#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include <sys/stat.h>
#include <dgcutils/GlobalConstants.h>
#include <alice/AliceConstants.h>
#include <rndf/RNDF.hh>
#include "analyzer_cmdline.h"

#define PI 3.14159265
#define STOP_VEL 0.15
#define MAX_DISTANCE 5

using namespace std;

void checkCollision(FILE *obstFile, FILE *logFile);
int readLogFile(FILE *logFile, double *timestep,
		double *x, double *y, double *yaw);
int readObstFile(FILE *obstFile, double *obstX, double *obstY, double *obstRad);
double checkDist(double obstX, double obstY, double obstRad, FILE *logFile);
void printResult(double dist);
void checkCollision(FILE *obstFile, FILE *logFile);
void checkStayInLane(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
		     vector<double> yaw, vector<double>& speed,
		     vector<GPSPoint*>& closestWaypoints);
void checkStop(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
	       vector<double> yaw, vector<double>& speed,
	       vector<GPSPoint*>& closestWaypoints);
void checkMission(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		  vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		  vector<GPSPoint*>& closestWaypoints, vector<Waypoint*>& checkpoints);
void getSegmentStat(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		    vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		    vector<GPSPoint*>& closestWaypoints);
bool parseState(char* fileName, vector<int>& timestamp,
		vector<double>& northing, vector<double>& easting,
		vector<double>& yaw, vector<double>& speed);
bool loadMDFFile(char* fileName, RNDF* rndf, vector<Waypoint*>& checkpoints, 
		 vector<double>& minSpeedLimits, vector<double>& maxSpeedLimits);
void parseCheckpoint(ifstream* file, RNDF* rndf, vector<Waypoint*>& checkpoints);
void parseSpeedLimit(ifstream* file, RNDF* rndf, 
		     vector<double>& minSpeedLimits, 
		     vector<double>& maxSpeedLimits);
double distPoint2Line(double px, double py, double l1x, double l1y, double l2x, 
		      double l2y);
double findMax(vector<double> vect, int& index);
void toLog(stringstream& mess);

/* file to log the analysis data */
FILE* logFile;
int DEBUG_LEVEL;
const double safeDist = 1.0; // 1 meter safe distance
const int BADTIMECOUNTER_MAXSIZE = 100;
double badTimes[BADTIMECOUNTER_MAXSIZE];
int badTimesCounter;

int main(int argc, char** argv)
{
  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);

  DEBUG_LEVEL = cmdline.debug_arg;
  if (cmdline.debug_given)
    DEBUG_LEVEL = 1;

  /* Log file */
  char logfilename[600];
  strcpy(logfilename, cmdline.state_logfile_arg);
  char* dot = strrchr( logfilename, '.' );
  if( dot ) *dot = 0;
  strcat( logfilename, ".analysis" );
  logFile = fopen(logfilename, "a");

  stringstream mess("");

  /* Figure out what RNDF and MDF we want to use */
  char RNDFFileName[500];
  char MDFFileName[500];
  string RNDFFileNameStr, MDFFileNameStr;
  struct stat buf;			/* status buffer for fstat */
  /* First see if the file exists in the current directory */
  ostringstream ossRNDF, ossMDF;
  ossRNDF << "./" << cmdline.rndf_arg;
  ossMDF << "./" << cmdline.mdf_arg;
  RNDFFileNameStr = ossRNDF.str();
  MDFFileNameStr = ossMDF.str();
  if (stat(RNDFFileNameStr.c_str(), &buf) == 0)
  {
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", RNDFFileNameStr.c_str() );    
  }
  else if (getenv("DGC_CONFIG_PATH"))
  {
    ostringstream oss1;
    oss1 << getenv("DGC_CONFIG_PATH") << "/" << cmdline.rndf_arg;
    RNDFFileNameStr = oss1.str();
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", RNDFFileNameStr.c_str() );
  }
  else
  {
    mess.str("");
    snprintf( RNDFFileName, sizeof(RNDFFileName), "%s", cmdline.rndf_arg );
    mess << "unknown configuration path: please set DGC_CONFIG_PATH\n";
    mess << "ERROR: cannot find RNDF file: " << RNDFFileName << "\n";
    toLog(mess);
    exit(1);
  }
  if (stat(MDFFileNameStr.c_str(), &buf) == 0)
  {
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", MDFFileNameStr.c_str() );    
  }
  else if (getenv("DGC_CONFIG_PATH"))
  {
    ostringstream oss2;
    oss2 << getenv("DGC_CONFIG_PATH") << "/" << cmdline.mdf_arg;
    MDFFileNameStr = oss2.str();
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", MDFFileNameStr.c_str() );
  }
  else
  {
    mess.str("");
    snprintf( MDFFileName, sizeof(MDFFileName), "%s", cmdline.mdf_arg );
    mess << "unknown configuration path: please set DGC_CONFIG_PATH\n";  
    mess << "ERROR: cannot find MDF file: " << MDFFileName << "\n";
    toLog(mess);
    exit(1);
  }

  mess.str("");
  mess << "\n" << "-------------------------------" << "\n\n";
  mess << "RNDF file: " << RNDFFileName << "\n";
  mess << "MDF file: " << MDFFileName << "\n";
  toLog(mess);

  /* Parse RNDF and MDF */
  RNDF* rndf = new RNDF();
  if (!rndf->loadFile(RNDFFileName))
  {
    mess.str("");
    mess << "ERROR:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program\n";
    toLog(mess);
    exit(1);
  }
  rndf->assignLaneDirection();

  vector<Waypoint*> checkpoints;
  vector<double> minSpeedLimits, maxSpeedLimits;
  minSpeedLimits.resize(rndf->getNumOfSegments() + rndf->getNumOfZones() + 1);
  maxSpeedLimits.resize(rndf->getNumOfSegments() + rndf->getNumOfZones() + 1);

  if (!loadMDFFile(MDFFileName, rndf, checkpoints, minSpeedLimits, maxSpeedLimits)) {
    mess.str("");
    mess << __FILE__ << ":" << __LINE__ << " ERROR: Unable to load MDF file " 
	 << MDFFileName << ", exiting program\n";
    toLog(mess);
    exit(1);
  }


  /* Parse state data */
  vector<int> timestamp;
  vector<double> northing, easting, yaw, speed;
  vector<GPSPoint*> closestWaypoints;
  if (!parseState(cmdline.state_logfile_arg, timestamp, northing, easting, yaw, speed)) {
    mess.str("");
    mess << __FILE__ << ":" << __LINE__ << " ERROR: Unable to parse state data " 
	 << cmdline.state_logfile_arg << ", exiting program\n";
    toLog(mess);
    exit(1);
  }
  mess.str("");
  mess << "state log file: " << cmdline.state_logfile_arg << "\n";
  toLog(mess);

  /* Analyze data */
  mess.str("");
  mess << "\n" << "-------------------------------" << "\n\n";
  toLog(mess);
  
  getSegmentStat(rndf, timestamp, northing, easting, yaw, speed, closestWaypoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  checkMission(rndf, timestamp, northing, easting, yaw, speed, 
	       closestWaypoints, checkpoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  checkStop(rndf, northing, easting, yaw, speed, closestWaypoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  checkStayInLane(rndf, northing, easting, yaw, speed, closestWaypoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  if (strcmp("noObs", cmdline.obs_logfile_arg) != 0) {
    FILE *obstFile = fopen(cmdline.obs_logfile_arg,"r"); // opens files
    FILE *stateFile  = fopen(cmdline.state_logfile_arg, "r");

    if (obstFile != NULL && stateFile != NULL)
      checkCollision(obstFile, stateFile);
    else if (obstFile == NULL) {
      mess.str("");
      mess << __FILE__ << ":" << __LINE__ << " ERROR: " << cmdline.state_logfile_arg 
	   << " does not exists\n";
      toLog(mess);
    }
    else {
      mess.str("");
      mess << __FILE__ << ":" << __LINE__ << " ERROR: fileName " << " file not found.\n";
      toLog(mess);
      return false;
    }
  }
  else {
    mess.str("");
    mess << "No obstacle\n";
    toLog(mess);
  }
    

  delete rndf;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkCollision(FILE *obstFile, FILE *logFile) {
  double obstX, obstY, obstRad, dist;
  int readLine, obstCounter = 1;
  rewind(obstFile);
  readLine = readObstFile(obstFile,&obstX,&obstY,&obstRad);
  while(readLine != EOF) {
    stringstream mess("");
    badTimesCounter = 0;
    mess << "[Collision check for obstacle #"
	 << obstCounter << " @ (" << fixed << setprecision(0)
	 << obstX << ", " << obstY << ")]\n";
    toLog(mess);
    dist = checkDist(obstX,obstY,obstRad,logFile);

    printResult(dist); // outputs results
    obstCounter++;
    readLine = readObstFile(obstFile,&obstX,&obstY,&obstRad);
  }
}


int readLogFile(FILE *logFile, double *timestep,
		double *x, double *y, double *yaw) {
  double speed;
  return fscanf(logFile, "%lf %lf %lf %lf %lf", timestep, x, y, yaw, &speed);
}

int readObstFile(FILE *obstFile, double *obstX, double *obstY, double *obstRad) {
  return fscanf(obstFile, "%lf %lf %lf", obstX, obstY, obstRad);
}

double checkDist(double obstX, double obstY, double obstRad, FILE *logFile) {
  // true = vehicle does not come within 1 m of obstacle
  double x, y, x1, y1, yaw, dist, theta;
  double timestep;
  double closestDist = 10000;
  int readLine = 1;
  bool tooClose = false;
  stringstream mess("");
  
  rewind(logFile);
  readLine = readLogFile(logFile,&timestep,&x,&y,&yaw);
  while(readLine != EOF) {
    theta = atan( (y - obstY) / (x - obstX) ); // finds closest
    if(x < obstX)
      theta += PI;
    y1 = obstY + (sin(theta) * obstRad);       // point to vehicle
    x1 = obstX + (cos(theta) * obstRad);
    
    /*if(obstY == 398940)
    {
      printf("theta = %0.3lf :: ",theta);
      printf("(%0.2lf,%0.2lf)\n",x1,y1);
      }*/
    if( (x1 < x && x < obstX) || (x1 > x && x > obstX) ||
	(y1 < y && y < obstY) || (y1 > y && y > obstY) ) // collision case
      dist = -1;
    else
      dist = sqrt( (x - x1)*(x - x1) + (y - y1)*(y - y1) );

    if( (dist >= safeDist &&  tooClose) ||
	(dist <  safeDist && !tooClose) ) { // gathers bad ranges
      tooClose = !tooClose;
      if (badTimesCounter >= BADTIMECOUNTER_MAXSIZE) {
	mess << __FILE__ << ":" << __LINE__
	     << " ERROR: badTimesCounter >= BADTIMECOUNTER_MAXSIZE\n";
	toLog(mess);
      }
      badTimes[badTimesCounter] = timestep;
      if(!tooClose)
	badTimes[badTimesCounter]--;
      badTimesCounter++;
    }
    if(dist < closestDist) // remembers closest dist to obstacle
      closestDist = dist;
    if (DEBUG_LEVEL > 0) {
      if(dist < 1) {
	mess.str("");
	mess << "distance is " << dist << " @ "
	     << timestep << "\n";
	toLog(mess);
      }
    }
    
    readLine = readLogFile(logFile,&timestep,&x,&y,&yaw);
  }
  if(tooClose){
    if (badTimesCounter >= BADTIMECOUNTER_MAXSIZE) {
      mess.str("");
      mess << __FILE__ << ":" << __LINE__
	   << " ERROR: badTimesCounter >= BADTIMECOUNTER_MAXSIZE\n";
      toLog(mess);
    }
    badTimes[badTimesCounter] = timestep;
    badTimesCounter++;
  }
  return closestDist;
}

void printResult(double dist) {
  stringstream mess("");
  int c;
  if(dist == -1) {
    mess << "   The vehicle hit the obstacle!  ";
    toLog(mess);
  }
  else {
    mess << "   The vehicle comes as close as "
	 << dist << " m from the obstacle.  ";
    toLog(mess);
  }
  if(dist >= safeDist) {
    mess.str("");
    mess << "That is safe!\n";
    toLog(mess);
  }
  else {
    mess.str("");
    mess << "That is unsafe!\n";
    mess << "   Bad timestep ranges:\n"; // lists bad time ranges
    toLog(mess);
    for(c = 0; c < badTimesCounter; c++) {
      if(c%2) { // odd
	if(badTimes[c] != badTimes[c-1]) {
	  mess.str("");
	  mess << " - " << badTimes[c] << "\n";
	  toLog(mess);
	}
	else {
	  mess.str("");
	  mess << "\n";
	  toLog(mess);
	}
      }
      else { // even
	mess.str("");
	mess << fixed << setprecision(0) << "   * " << badTimes[c];
	toLog(mess);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkStayInLane(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
		     vector<double> yaw, vector<double>& speed, 
		     vector<GPSPoint*>& closestWaypoints)
{
  if (closestWaypoints.size() == 0)
    return;
  stringstream mess("");
  mess << "Max distance from centerline:\n";
  toLog(mess);

  vector<double> distToCenterline;
  vector<GPSPoint*> prevWaypoints, nextWaypoints;
  int currentSegmentID = closestWaypoints[0]->getSegmentID();
  GPSPoint* currentWaypoint = closestWaypoints[0];
  GPSPoint* previousWaypoint = closestWaypoints[0];

  for (unsigned i = 0; i < closestWaypoints.size(); i++) {
    if (closestWaypoints[i]->getSegmentID() > rndf->getNumOfSegments())
      continue;
    if (closestWaypoints[i]->getSegmentID() != currentSegmentID  ||
	i == closestWaypoints.size() - 1) {
      int index;
      mess.str("");
      mess << "Segment " << currentSegmentID << ":\t" << findMax(distToCenterline, index);
      if (index >= 0) {
	mess << " (between waypoint " << prevWaypoints[index]->getSegmentID() << "."
	     << prevWaypoints[index]->getLaneID() << "."
	     << prevWaypoints[index]->getWaypointID() << " and " 
	     << nextWaypoints[index]->getSegmentID() << "."
	     << nextWaypoints[index]->getLaneID() << "."
	     << nextWaypoints[index]->getWaypointID() << ")\n";
	if (DEBUG_LEVEL > 0) {
	  mess << "\t\t" << distToCenterline.size() << "\t" << prevWaypoints.size() << "\t"
	       << nextWaypoints.size() << "\t" << index << "\n";
	}
      }
      else {
	mess << "\n";
      }
      toLog(mess);
      distToCenterline.clear();
      prevWaypoints.clear();
      nextWaypoints.clear();
      currentSegmentID = closestWaypoints[i]->getSegmentID();
    }
    double angle = atan2(closestWaypoints[i]->getEasting() - easting[i], 
			 closestWaypoints[i]->getNorthing() - northing[i]) - yaw[i];
    /*
    if (closestWaypoints[i]->getSegmentID() == 3 && closestWaypoints[i]->getLaneID() == 1 &&
	closestWaypoints[i]->getWaypointID() == 2)
      cout << "3.1.2:\t" << cos(angle) << endl;
    */
    if (cos(angle) > 0 && closestWaypoints[i]->getWaypointID() > 1) {
      GPSPoint* waypoint2 = rndf->getWaypoint(closestWaypoints[i]->getSegmentID(),
					      closestWaypoints[i]->getLaneID(),
					      closestWaypoints[i]->getWaypointID() - 1);
      double distTmp = distPoint2Line(northing[i], easting[i],
				      closestWaypoints[i]->getNorthing(),
				      closestWaypoints[i]->getEasting(),
				      waypoint2->getNorthing(),
				      waypoint2->getEasting());
      distToCenterline.push_back(distTmp);
      prevWaypoints.push_back(waypoint2);
      nextWaypoints.push_back(closestWaypoints[i]);
    }
    else if (cos(angle) <= 0 && closestWaypoints[i]->getWaypointID() < 
	     rndf->getLane(closestWaypoints[i]->getSegmentID(), 
			   closestWaypoints[i]->getLaneID())->getNumOfWaypoints()) {
      GPSPoint* waypoint2 = rndf->getWaypoint(closestWaypoints[i]->getSegmentID(),
					      closestWaypoints[i]->getLaneID(),
					      closestWaypoints[i]->getWaypointID() + 1);
      double distTmp = distPoint2Line(northing[i], easting[i],
				      closestWaypoints[i]->getNorthing(),
				      closestWaypoints[i]->getEasting(),
				      waypoint2->getNorthing(),
				      waypoint2->getEasting());
      distToCenterline.push_back(distTmp);
      prevWaypoints.push_back(closestWaypoints[i]);
      nextWaypoints.push_back(waypoint2);
    }
    else if (closestWaypoints[i] != previousWaypoint &&
	     closestWaypoints[i]->getSegmentID() == previousWaypoint->getSegmentID() &&
	     closestWaypoints[i]->getLaneID() == previousWaypoint->getLaneID()) {
      GPSPoint* waypoint2 = previousWaypoint;
      double distTmp = distPoint2Line(northing[i], easting[i],
				      closestWaypoints[i]->getNorthing(),
				      closestWaypoints[i]->getEasting(),
				      waypoint2->getNorthing(),
				      waypoint2->getEasting());
      distToCenterline.push_back(distTmp);
      prevWaypoints.push_back(waypoint2);
      nextWaypoints.push_back(closestWaypoints[i]);
    }

    if (closestWaypoints[i] != currentWaypoint) {
      previousWaypoint = currentWaypoint;
      currentWaypoint = closestWaypoints[i];
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkStop(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
	       vector<double> yaw, vector<double>& speed, 
	       vector<GPSPoint*>& closestWaypoints)
{
  if (closestWaypoints.size() == 0)
    return;

  stringstream mess("");
  mess << "Stop lines:\n";
  toLog(mess);
  unsigned i = 0;
  while ( i < closestWaypoints.size() ) {
    if (closestWaypoints[i]->getSegmentID() > rndf->getNumOfSegments()) {
      continue;
    }
    Waypoint* stopWaypoint = rndf->getWaypoint(closestWaypoints[i]->getSegmentID(),
					   closestWaypoints[i]->getLaneID(),
					   closestWaypoints[i]->getWaypointID());
    if (stopWaypoint == NULL) {
      mess.str("");
      mess << __FILE__ << ":" << __LINE__ << " ERROR: Can't find waypoint "
	   << closestWaypoints[i]->getSegmentID() << "."
	   << closestWaypoints[i]->getLaneID() << "."
	   << closestWaypoints[i]->getWaypointID() << "\n";
      toLog(mess);
      continue;
    }
    if (!stopWaypoint->isStopSign())
      i++;
    else {
      bool isStop = false;
      while (i < closestWaypoints.size() &&
	     closestWaypoints[i]->getSegmentID() == stopWaypoint->getSegmentID() &&
	     closestWaypoints[i]->getLaneID() == stopWaypoint->getLaneID() &&
	     closestWaypoints[i]->getWaypointID() == stopWaypoint->getWaypointID() ) {
	if (speed[i] < STOP_VEL && !isStop) {
	  isStop = true;
	  double angle = atan2(stopWaypoint->getEasting() - easting[i], 
			       stopWaypoint->getNorthing() - northing[i]) - yaw[i];
	  double distance = sqrt( pow(northing[i] - stopWaypoint->getNorthing(), 2) + 
				  pow(easting[i] - stopWaypoint->getEasting(), 2) );
	  mess.str("");
	  mess << "  Distance to " << stopWaypoint->getSegmentID() << "."
	       << stopWaypoint->getLaneID() << "."
	       << stopWaypoint->getWaypointID() << ":\t"
	       << distance * cos(angle) << " m\n";
	  toLog(mess);
	}
	i++;
      }
      if (!isStop) {
	mess.str("");
	mess << "ERROR: We did not stop at " << stopWaypoint->getSegmentID() << "."
	     << stopWaypoint->getLaneID() << "."
	     << stopWaypoint->getWaypointID() << "\n";
	toLog(mess);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkMission(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		  vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		  vector<GPSPoint*>& closestWaypoints, vector<Waypoint*>& checkpoints)
{
  stringstream mess("");
  vector<int> ckptsTime;
  if (timestamp.size() == 0) {
    if (checkpoints.size() != 0) {
      mess << "ERROR: Vehicle is stationary. No checkpoint is crossed\n";
      toLog(mess);
    }
    return;
  }
  unsigned currentCkptInd = 0;
  ckptsTime.push_back(timestamp[0]);
  unsigned i = 0;
  while (i < northing.size() && currentCkptInd < checkpoints.size()) {
    double ckptEasting = checkpoints[currentCkptInd]->getEasting();
    double ckptNorthing = checkpoints[currentCkptInd]->getNorthing();
    double angle = atan2(ckptEasting - easting[i], ckptNorthing - northing[i]) - yaw[i];
    double distance = sqrt( pow(northing[i] - ckptNorthing, 2) + 
			    pow(easting[i] - ckptEasting, 2) );
    if (closestWaypoints[i]->getSegmentID() == checkpoints[currentCkptInd]->getSegmentID() &&
	closestWaypoints[i]->getLaneID() == checkpoints[currentCkptInd]->getLaneID() &&
	closestWaypoints[i]->getWaypointID() == checkpoints[currentCkptInd]->getWaypointID() &&
	fabs(distance * cos(angle)) < 0.5 ) {
      if ( fabs(distance * sin(angle)) > VEHICLE_WIDTH/2 ) {
	mess.str("");
	mess << "WARNING: Make sure that Alice crossed checkpoint "
	     << currentCkptInd + 1 << " (The bumper is " << fabs(distance * sin(angle))
	     << " meters away from the checkpoint.)\n";
	toLog(mess);
      }
      currentCkptInd++;
      ckptsTime.push_back(timestamp[i]);
    }
    i++;
  }
  if (currentCkptInd == 0) {
    mess.str("");
    mess << "ERROR: No checkpoint is crossed\n";
    toLog(mess);
  }
  else if (currentCkptInd != checkpoints.size()) {
    mess.str("");
    mess << "ERROR: Last checkpoint crossed is " << currentCkptInd
	 << "(waypoint " << checkpoints[currentCkptInd-1]->getSegmentID()
	 << "." << checkpoints[currentCkptInd-1]->getLaneID()
	 << "." << checkpoints[currentCkptInd-1]->getWaypointID()
	 << ")\n";
    toLog(mess);
  }
  else {
    for (unsigned j = 1; j < ckptsTime.size(); j ++) {
      mess.str("");
      mess << "  From checkpoint " << j-1 << " to " << j << ":\t" 
	   << (ckptsTime[j] - ckptsTime[j-1]) * pow(10, -6)  << " seconds" << endl;
      toLog(mess);
    }
    mess.str("");
    mess << "\n" << "Time to complete mission:\t" 
	 << (ckptsTime[ckptsTime.size() - 1] - ckptsTime[0]) * pow(10, -6) 
	 << " seconds\n";
    toLog(mess);
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void getSegmentStat(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		    vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		    vector<GPSPoint*>& closestWaypoints)
{
  stringstream mess("");
  double distance, headingDiff, headingLaneDir;
  int currentSegmentID = 0;
  unsigned currentSegmentStartInd = 0;
  mess << "Stat:" << endl;
  toLog(mess);
  for (unsigned i = 0; i < northing.size(); i++) {
    double maxHeadingLaneDir = 45*PI/180;
    distance = 10*MAX_DISTANCE;
    GPSPoint* closestPoint;
    while (distance > MAX_DISTANCE && maxHeadingLaneDir <= 90*PI/180) {
      closestPoint = rndf->findClosestGPSPoint(northing[i], easting[i], 0, 0,
					       distance, true, yaw[i],
					       2*PI, 0, maxHeadingLaneDir, 
					       headingDiff, headingLaneDir);
      maxHeadingLaneDir += 5*PI/180;
    }
    if (DEBUG_LEVEL > 0) {
      mess.str("");
      mess << timestamp[i] << closestPoint->getSegmentID() << "." 
	   << closestPoint->getLaneID() << "."
	   << closestPoint->getWaypointID() << "\t" << distance << "\t" 
	   << "\t" << headingDiff*180/PI << "\n";
      toLog(mess);
    }
    closestWaypoints.push_back(closestPoint);
    if (i == 0)
      currentSegmentID = closestPoint->getSegmentID();
    if (currentSegmentID != closestPoint->getSegmentID() ||
	i == northing.size() - 1) {
      mess.str("");
      mess << "  Segment " << currentSegmentID << ":\t"
	   << (timestamp[i - 1] - timestamp[currentSegmentStartInd]) * pow(10, -6)
	   << " seconds\n";
      toLog(mess);
      currentSegmentStartInd = i;
      currentSegmentID = closestPoint->getSegmentID();
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool parseState(char* fileName, vector<int>& timestamp,
		vector<double>& northing, vector<double>& easting,
		vector<double>& yaw, vector<double>& speed)
{
  stringstream mess("");
  ifstream file;
  string line;
  string word;
  int t;
  double n, e, y, v;
  bool isPaused = true;

  file.open(fileName, ios::in);

  if(!file)
  {
    mess << __FILE__ << ":" << __LINE__ << " ERROR: fileName " << " file not found.\n";
    toLog(mess);
    return false;
  } 

  getline(file, line);
  
  while(!file.eof())
  { 
    istringstream lineStream(line, ios::in);

    lineStream >> word;
    t = atoi(word.c_str());
    lineStream >> word;
    n = atof(word.c_str());
    lineStream >> word;
    e = atof(word.c_str());
    lineStream >> word;
    y = atof(word.c_str());
    lineStream >> word;
    v = atof(word.c_str());

    if (v > STOP_VEL) {
      isPaused = false;
    }

    if (!isPaused) {
      timestamp.push_back(t);
      northing.push_back(n);
      easting.push_back(e);
      yaw.push_back(y);
      speed.push_back(v);
    }
    getline(file, line);
  }

  isPaused = true;
  int i = northing.size() - 1;
  while (isPaused && i > 0) {
    if (speed[i] > STOP_VEL) {
      isPaused = false;
    }
    if (isPaused) {
      timestamp.pop_back();
      northing.pop_back();
      easting.pop_back();
      yaw.pop_back();
      speed.pop_back();
    }
    i--;
  }

  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool loadMDFFile(char* fileName, RNDF* rndf, vector<Waypoint*>& checkpoints, 
	     vector<double>& minSpeedLimits, vector<double>& maxSpeedLimits)
{
  stringstream mess("");
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    mess << __FILE__ << ":" << __LINE__ << " ERROR: fileName " << " file not found.\n";
    toLog(mess);
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf, checkpoints);
    parseSpeedLimit(&file, rndf, minSpeedLimits, maxSpeedLimits);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void parseCheckpoint(ifstream* file, RNDF* rndf, vector<Waypoint*>& checkpoints)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      checkpoints.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void parseSpeedLimit(ifstream* file, RNDF* rndf, 
				   vector<double>& minSpeedLimits, 
				   vector<double>& maxSpeedLimits)
{
  stringstream mess("");
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits") {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits") {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9') {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= minSpeedLimits.size()) {
	minSpeedLimits[segmentID] = minSpeed;
	maxSpeedLimits[segmentID] = maxSpeed;
      }
      else {
	mess << __FILE__ << ":" << __LINE__ << " ERROR: got speed limit for segment " 
	     << segmentID << " while number of segments and zones = " 
	     << minSpeedLimits.size() << "\n";
	toLog(mess);
	int numOfSegments = minSpeedLimits.size();
	minSpeedLimits.resize(segmentID);
	maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= minSpeedLimits.size(); i++)
	{
	  minSpeedLimits[i] = 0;
	  maxSpeedLimits[i] = 0;
	}
	minSpeedLimits[segmentID] = minSpeed;
	maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double findMax(vector<double> vect, int& index)
{
  if (vect.size() == 0) {
    index = -1;
    return 0;
  }
  double ret = vect[0];
  index = 0;
  for (unsigned i = 1; i < vect.size(); i++) {
    if (vect[i] > ret) {
      ret = vect[i];
      index = i;
    }
  }
  return ret;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double distPoint2Line(double px, double py, double l1x, double l1y, double l2x, double l2y)
{
  return fabs((l2x - l1x)*(l1y - py) - (l1x - px)*(l2y - l1y))/
    sqrt(pow(l1x - l2x, 2) + pow(l1y - l2y, 2));
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void toLog(stringstream& mess)
{
  cout << mess.str();
  fprintf (logFile, mess.str().c_str());
}
