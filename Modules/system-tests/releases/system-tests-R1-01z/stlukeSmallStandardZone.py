#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '5.2.1'
scenario = 'stlukeSmallStandard'
rndf = '../../etc/routes-stluke/stluke_small_zone.rndf'
mdf = '../../etc/routes-stluke/stluke_small_zone.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

