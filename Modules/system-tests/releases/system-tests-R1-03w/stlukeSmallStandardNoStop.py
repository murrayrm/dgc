#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '3.1.1'
scenario = 'stlukeSmallStandard'
rndf = '../../etc/routes-stluke/stluke_small_intersection_nostop.rndf'
mdf = '../../etc/routes-stluke/stluke_small_standard.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )



