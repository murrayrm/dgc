#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroFieldDemoMDF6'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_adj.rndf'
mdf = '../../etc/routes-eltoro/Sept_2007_6.mdf'
#mdf = '../../etc/routes-eltoro/eltoro_field_demo2_zone.mdf'
obsFile = 'noObs'
sceneFunc = 'eltoro_buildingsandbushes'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )


