import os;
import sys;

def runSensingStack( ):
  apps = [ \
  ('ladarfeeder', '--sensor-id=SENSNET_LF_BUMPER_LADAR', 1, 'amy'), \
   ('ladarfeeder', '--sensor-id=SENSNET_RF_BUMPER_LADAR', 1, 'amy'), \
   ('ladarfeeder', '--sensor-id=SENSNET_MF_BUMPER_LADAR', 1, 'amy'), \
#  ('ladarfeeder', '--sensor-id=SENSNET_LF_ROOF_LADAR', 1, 'amy'), \
#  ('ladarfeeder', '--sensor-id=SENSNET_RF_ROOF_LADAR', 1, 'amy'), \
#  ('ladarfeeder', '--sensor-id=SENSNET_RIEGL', 1, 'amy'), \t
   ('template-ladarperceptor', '--send-debug', 1, 'amy'), \
#  ('ladar-car-perceptor', '', 1, 'amy'), \
#  ('ladar-obs-perceptor', '', 1, 'amy'), \
#  ('stereofeeder', '--sensor-id=SENSNET_RF_SHORT_STEREO', 1, 'fry'), \
#  ('stereofeeder', '--sensor-id=SENSNET_LF_SHORT_STEREO', 1, 'fry'), \
#  ('lineperceptor', '--sensor-id=SENSNET_RF_SHORT_STEREO --module-id=MODlinePerceptorRFShort', 1, 'fry'), \
#  ('lineperceptor', '--sensor-id=SENSNET_LF_SHORT_STEREO --module-id=MODlinePerceptorLFShort', 1, 'fry'), \
#  ('stereofeeder', '--sensor-id=SENSNET_MF_MEDIUM_STEREO', 1, 'zoidberg'), \
#  ('blobstereo', '--opengl', 1, 'zoidberg'), \
  ]
  return apps