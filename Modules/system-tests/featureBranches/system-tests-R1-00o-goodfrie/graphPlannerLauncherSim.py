#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'graphplanner'
rndf = '../../src/graph-planner/stluke_bigloop.rndf'
mdf = '../../src/graph-planner/stluke_bigloop.rndf'
obsFile = 'noObs'
sceneFunc = ''

apps = [ \
('asim', '--rndf=%(rndf)s --rndf-start=1.1.1 --gcdrive' % locals(), 2, 'localhost'), \
 ('graph-planner-viewer', '--rndf=%(rndf)s --goal=2 --loop=5' % locals(), 2 , 'localhost'), \
 ('gcfollower',  '', 2 , 'leela'), \
 ('gcdrive',  '--simulate', 2 , 'leela'), \
 ('planviewer',  '', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )


