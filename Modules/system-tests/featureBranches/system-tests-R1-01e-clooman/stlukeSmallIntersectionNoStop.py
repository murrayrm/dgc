#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '3.1.2'
scenario = 'stlukeSmallStandard'
rndf = '../../stluke_small_intersection_nostop.rndf'
mdf = '../../stluke_small_intersection_nostop.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

