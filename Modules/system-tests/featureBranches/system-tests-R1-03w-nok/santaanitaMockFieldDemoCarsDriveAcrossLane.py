#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '6.2.1'
scenario = 'santaanitaMockFieldDemoCarsDriveAcrossLane'
rndf = '../../etc/routes-santaanita/santaanita_mock_field_demo1.rndf'
mdf = '../../etc/routes-santaanita/santaanita_mock_field_demo_bigloop.mdf'
obsFile = 'NoObs'
sceneFunc = 'santaanita_cardrivesacrosslane'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



