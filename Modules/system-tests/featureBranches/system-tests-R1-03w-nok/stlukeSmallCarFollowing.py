#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSmallCarFollowing'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_straight.mdf'
obsFile = 'noObs'
sceneFunc = 'stluke_small_carfollowing'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )




