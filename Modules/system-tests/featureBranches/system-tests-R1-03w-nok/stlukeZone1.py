#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeZone1'
rndf = '../../etc/routes-stluke/stluke_zone.rndf'
mdf = '../../etc/routes-stluke/stluke_zone.mdf'
obsFile = 'stlukeZone1'
sceneFunc = 'stlukezone_1'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )




