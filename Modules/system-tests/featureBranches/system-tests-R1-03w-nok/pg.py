#! /usr/bin/python -i

import os;

def checkRndf(rndf):
  """Check that the file.rndf has a corresponding file.rndf.pg.
If not, build it with plangraph-builder.
Returns true if the .pg file exists or was made, and otherwise false.
  """
  
  isRndf = os.path.isfile(rndf)
  rndfpg = rndf + ".pg"
  isRndfpg = os.path.isfile(rndfpg)
  if isRndf:
    if not isRndfpg:
      print "I need to build a point graph file for that rndf:"
      cmd = "../../bin/Drun plangraph-builder --rndf=%(rndf)s" % locals()
      print cmd
      stat = os.popen(cmd).read().strip()
      print stat
      isRndfpg = os.path.isfile(rndfpg)
    else:
      print "Note: the plan graph is all ready."
  else:
    print "Error: no such route: %(rndf)s" % locals()

  return isRndf and isRndfpg

