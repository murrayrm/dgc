#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.5'
scenario = 'santaanitaPassBlock'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaPassBlock.obs'
sceneFunc = 'santaanita_sitevisit_passblock'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



