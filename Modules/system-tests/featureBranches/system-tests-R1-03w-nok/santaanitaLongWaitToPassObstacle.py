#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.5'
scenario = 'santaanitaLongWaitToPassObstacle'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaLongWaitToPassObstacle.obs'
sceneFunc = 'santaanita_sitevisit_longwaittopassobstacle'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


