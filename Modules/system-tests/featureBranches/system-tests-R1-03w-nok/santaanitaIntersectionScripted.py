#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'santaanitaIntersectionScripted'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_simpleloop.mdf'
obsFile = 'noObs'
sceneFunc = 'santaanita_sitevisit_intersection_scripted'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


