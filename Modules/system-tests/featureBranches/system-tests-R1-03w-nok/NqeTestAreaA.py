#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.1.2'
scenario = 'NqeTestAreaA'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_test2.mdf'
obsFile = 'NqeTestAreaA.obs'
sceneFunc = 'nqe_area_a_mod3'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


