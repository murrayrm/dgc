#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '6.2.2'
scenario = 'santaanitaMockFieldDemo1PartLaneBlock'
rndf = '../../etc/routes-santaanita/santaanita_mock_field_demo1.rndf'
mdf = '../../etc/routes-santaanita/santaanita_mock_field_demo1.mdf'
obsFile = 'santaanitaMockFieldDemo1PartLaneBlock.obs'
sceneFunc = 'santaanita_mock_field_demo1_passblock'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



