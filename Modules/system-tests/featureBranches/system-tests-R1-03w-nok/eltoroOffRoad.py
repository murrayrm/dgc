#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '23.1.2'
scenario = 'eltoroOffRoad'
rndf = '../../etc/routes-eltoro/eltoro_offroad.rndf'
mdf = '../../etc/routes-eltoro/eltoro_offroad.mdf'
obsFile = 'noObs'
sceneFunc = 'eltoro_buildingsandbushes'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



