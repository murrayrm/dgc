#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'santaanitaFakeIntersectionOrdering'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_right.mdf'
obsFile = 'santaanitaFakeIntersectionOrdering.obs'
sceneFunc = 'santaanita_sitevisit_fakeintersectionordering'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


