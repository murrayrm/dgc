#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSingleroadRoadBlockField'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'stlukeSingleroadRoadBlockObs.log'
sceneFunc = 'stluke_singleroad_roadblock'

apps = [ \
('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'bender'), \
 ('tplannerNoCSS-R2-02u-field', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'bender'), \
 ('s1planner',  '', 2 , 'bender'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

