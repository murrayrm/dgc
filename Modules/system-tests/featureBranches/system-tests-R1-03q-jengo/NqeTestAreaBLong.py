#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '40.1.1'
scenario = 'NqeTestAreaBLong'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_test4.mdf'
obsFile = 'NqeTestAreaBLong.obs'
sceneFunc = 'nqe_test4'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
