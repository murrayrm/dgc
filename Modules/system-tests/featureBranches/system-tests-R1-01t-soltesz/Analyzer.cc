#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include <sys/stat.h>
#include <dgcutils/GlobalConstants.h>
#include <dgcutils/findroute.h>
#include <alice/AliceConstants.h>
#include <rndf/RNDF.hh>
#include "analyzer_cmdline.h"

#define PI 3.14159265
#define STOP_VEL 0.15
#define MAX_DISTANCE 5
#define ALICE_WIDTH 2.1336

using namespace std;

/* Check obstacles */
void getDynObstFilenames(string str, vector<string> *obstFilenames);
double getDistance(double,double,double,double,double,double,double,double,double);
double getDistToObstTimestamp(FILE*,double,double,double,double,double,double,double);
void addBadTime(int);
void printResult(FILE*,double);
double lineNumToTimestamp(FILE*,int);
int readLogFile(FILE*,double*,double*,double*,double*);
int readDynObstFile(FILE*,double*,double*,double*,double*);
int readCircleObstFile(FILE*,double*,double*,double*);
int readBlockObstFile(FILE*,double*,double*,double*,double*,double*);
int parseObstLog(FILE*);
double getClosestDistToObst(FILE*,FILE*);
double lineNumToTimestamp(FILE*,int);

/* Check other things */
void checkStayInLane(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
		     vector<double> yaw, vector<double>& speed,
		     vector<GPSPoint*>& closestWaypoints);
void checkStop(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
	       vector<double> yaw, vector<double>& speed,
	       vector<GPSPoint*>& closestWaypoints);
void checkMission(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		  vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		  vector<GPSPoint*>& closestWaypoints, vector<Waypoint*>& checkpoints);
void getSegmentStat(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		    vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		    vector<GPSPoint*>& closestWaypoints);
void getOverallStat(vector<int>& timestamp, vector<double>& northing, 
		    vector<double>& easting, vector<double>& speed);
bool parseState(char* fileName, vector<int>& timestamp,
		vector<double>& northing, vector<double>& easting,
		vector<double>& yaw, vector<double>& speed);
bool loadMDFFile(char* fileName, RNDF* rndf, vector<Waypoint*>& checkpoints, 
		 vector<double>& minSpeedLimits, vector<double>& maxSpeedLimits);
void parseCheckpoint(ifstream* file, RNDF* rndf, vector<Waypoint*>& checkpoints);
void parseSpeedLimit(ifstream* file, RNDF* rndf, 
		     vector<double>& minSpeedLimits, 
		     vector<double>& maxSpeedLimits);
double distPoint2Line(double px, double py, double l1x, double l1y, double l2x, 
		      double l2y);
double findMax(vector<double> vect, int& index);
void toLog(stringstream& mess);

/* file to log the analysis data */
FILE* logFile;
int DEBUG_LEVEL;

double DYNAMIC_OBS_WIDTH = 3.25;
double DYNAMIC_OBS_LENGTH = 5.45;
const double safeDist = 1.0; // 1 meter safe distance
const int BADTIMES_MAX = 100;
int badTimes[BADTIMES_MAX]; // stores the line #s in the logFile which are bad
int badTimesCounter;
double earliestTime;


int main(int argc, char** argv)
{
  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);

  DEBUG_LEVEL = cmdline.debug_arg;
  if (cmdline.debug_given)
    DEBUG_LEVEL = 1;

  /* Log file */
  char logfilename[600];
  strcpy(logfilename, cmdline.state_logfile_arg);
  char* dot = strrchr( logfilename, '.' );
  if( dot ) *dot = 0;
  strcat( logfilename, ".analysis" );
  logFile = fopen(logfilename, "a");

  stringstream mess("");

  /* Figure out what RNDF and MDF we want to use */
  char* RNDFFileName = dgcFindRouteFile(cmdline.rndf_arg);
  char* MDFFileName = dgcFindRouteFile(cmdline.mdf_arg);

  mess.str("");
  mess << "\n" << "-------------------------------" << "\n\n";
  mess << "RNDF file: " << RNDFFileName << "\n";
  mess << "MDF file: " << MDFFileName << "\n";
  mess << "obs file: " << cmdline.obs_logfile_arg << "\n";
  toLog(mess);

  /* Parse RNDF and MDF */
  RNDF* rndf = new RNDF();
  if (!rndf->loadFile(RNDFFileName))
  {
    mess.str("");
    mess << "ERROR:  Unable to load RNDF file " << RNDFFileName
	 << ", exiting program\n";
    toLog(mess);
    exit(1);
  }
  rndf->assignLaneDirection();

  vector<Waypoint*> checkpoints;
  vector<double> minSpeedLimits, maxSpeedLimits;
  minSpeedLimits.resize(rndf->getNumOfSegments() + rndf->getNumOfZones() + 1);
  maxSpeedLimits.resize(rndf->getNumOfSegments() + rndf->getNumOfZones() + 1);

  if (!loadMDFFile(MDFFileName, rndf, checkpoints, minSpeedLimits, maxSpeedLimits)) {
    mess.str("");
    mess << __FILE__ << ":" << __LINE__ << " ERROR: Unable to load MDF file " 
	 << MDFFileName << ", exiting program\n";
    toLog(mess);
    exit(1);
  }


  /* Parse state data */
  vector<int> timestamp;
  vector<double> northing, easting, yaw, speed;
  vector<GPSPoint*> closestWaypoints;
  if (!parseState(cmdline.state_logfile_arg, timestamp, northing, easting, yaw, speed)) {
    mess.str("");
    mess << __FILE__ << ":" << __LINE__ << " ERROR: Unable to parse state data " 
	 << cmdline.state_logfile_arg << ", exiting program\n";
    toLog(mess);
    exit(1);
  }
  mess.str("");
  mess << "state log file: " << cmdline.state_logfile_arg << "\n";
  toLog(mess);

  /* Analyze data */
  mess.str("");
  mess << "\n" << "-------------------------------" << "\n\n";
  toLog(mess);

  /* get overall statistic */
  getOverallStat(timestamp, northing, easting, speed);
  
  /* get segment stat */
  getSegmentStat(rndf, timestamp, northing, easting, yaw, speed, closestWaypoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  /* check mission */
  checkMission(rndf, timestamp, northing, easting, yaw, speed, 
	       closestWaypoints, checkpoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  /* check stop */
  checkStop(rndf, northing, easting, yaw, speed, closestWaypoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  /* check stay in lane */
  checkStayInLane(rndf, northing, easting, yaw, speed, closestWaypoints);
  mess.str("");
  mess << "\n";
  toLog(mess);

  /* Check obs */
  if (strcmp("noObs", cmdline.obs_logfile_arg) != 0) {
    double dist, closestDist = 1e10;
    vector<string> obstFilenames;
    stringstream osobstFilenames;
    osobstFilenames << cmdline.obs_logfile_arg;
    getDynObstFilenames(osobstFilenames.str(), &obstFilenames);

    mess.str("");
    mess << "Obstacles:\n";
    toLog(mess);

    for (unsigned i = 0; i < obstFilenames.size(); i++) {
      mess.str("");
      mess << obstFilenames[i].c_str() << "\n";
      toLog(mess);

      FILE *obstFile = fopen(obstFilenames[i].c_str(),"r"); // opens files
      FILE *stateFile  = fopen(cmdline.state_logfile_arg, "r");

      if (obstFile != NULL && stateFile != NULL) {
	dist = getClosestDistToObst(stateFile, obstFile);
	if (dist < closestDist)
	  closestDist = dist;
	fclose(obstFile);
	fclose(stateFile);
      }
      else if (obstFile == NULL) {
	mess.str("");
	mess << __FILE__ << ":" << __LINE__ << " ERROR: " << cmdline.obs_logfile_arg
	     << " does not exists\n";
	toLog(mess);
      }
    }
    FILE *stateFile  = fopen(cmdline.state_logfile_arg, "r");
    printResult(stateFile,closestDist); // outputs results
    fclose(stateFile);
  }
  else {
    mess.str("");
    mess << "No obstacle\n";
    toLog(mess);
  }

  delete rndf;
  fclose(logFile);
  return 0;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void getDynObstFilenames(string str, vector<string> *obstFilenames) {
  string temp;
  int indexStart = 0, indexEnd = 0;
  while(indexEnd != -1) {
    indexEnd = str.find_first_of(' ',indexStart);
    temp = str.substr(indexStart,indexEnd - indexStart);
    (*obstFilenames).push_back(temp);
    indexStart = indexEnd + 1;
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double getDistance(double x, double y, double yaw,
		   double obstX, double obstY, double obstRad,
		   double obstWidth, double obstLength, double obstAngle) {
  double theta, dist;
  yaw = -1 * yaw + PI/2; // converts "northing-easting" to xy-plane
  //obstAngle = -1 * obstAngle + PI/2;
//==================== finds closest point on vehicle to obstacle =================
  if(y < obstY)
    y += sin(yaw) * ALICE_WIDTH / 2;
  else
    y -= sin(yaw) * ALICE_WIDTH / 2;
  if(x < obstX)
    x += cos(yaw) * ALICE_WIDTH / 2;
  else
    x -= cos(yaw) * ALICE_WIDTH / 2;
  
  theta = atan( (y-obstY) / (x-obstX) );
  if(x < obstX)
    theta += PI; // -90 < theta < 270
  //printf("\t\t\t%0.0lf degrees\t\t\t(%0.1lf,%0.1lf)\n", theta * 180 / PI,x,y);
//==================== finds closest point on obstacle to vehicle =================
  double obstCenterToAlice = sqrt( (x-obstX)*(x-obstX) + (y-obstY)*(y-obstY) );
  if(obstRad > 0) // circular obstacle
    dist = obstCenterToAlice - obstRad;
  else { // rectangular obstacle
    double relAngle = fabs( theta - obstAngle - PI/2 );

    double blockRatioAngle = atan(obstLength / obstWidth);
    double nonClosestSide = obstWidth, closestSide = obstLength;
    if(relAngle > blockRatioAngle) {
      closestSide = obstWidth;
      nonClosestSide = obstLength;
    } // finds obstacle's closest side and furthest side from Alice

    dist = (obstCenterToAlice * sin(relAngle)) - (nonClosestSide/2);
    if(cos(relAngle) * obstCenterToAlice > closestSide / 2) // closest pt = corner
      dist = dist / sin(relAngle);
  }
  return dist;
}

double getDistToObstTimestamp(FILE *logFile, double obstTimestamp,
			      double obstX, double obstY, double obstRad,
			      double obstWidth, double obstLength, double obstAngle) {
  double timestamp, x, y, yaw, dist;
  double closestDist = 1e10, closeTime = 5; // change this as needed
  int readLine = 1, lineNum = 1;
 
  rewind(logFile);
  readLine = readLogFile(logFile,&timestamp,&x,&y,&yaw);
  while(readLine != EOF) {
    if( obstTimestamp < 0 || // testing static obstacle or...
	(obstTimestamp > timestamp - closeTime) &&
	(obstTimestamp < timestamp + closeTime) ) { // ...timestamps are similar
      dist = getDistance(x,y,yaw,obstX,obstY,obstRad,obstWidth,obstLength,obstAngle);
      //printf("%0.2lf m @ time: %0.0lf\n",dist,timestamp);
      if(dist < safeDist) // gathers bad ranges
	addBadTime(lineNum);
      if(dist < closestDist)
	closestDist = dist;
    }
    readLine = readLogFile(logFile,&timestamp,&x,&y,&yaw);
    lineNum++;
  }
  return closestDist;
}

void addBadTime(int lineNum) {
  int c;
  //printf("*** %d is a bad line ***\n", lineNum);
  for(c = 0; c < badTimesCounter; c += 2) {
    if(lineNum >= badTimes[c] - 1 && lineNum <= badTimes[c+1] + 1) {
      // if lineNum is within or extends an existing range
      if(lineNum == badTimes[c] - 1)
	badTimes[c] = lineNum;
      if(lineNum == badTimes[c+1] + 1)
	badTimes[c+1] = lineNum;
      return;
    }
  }
  badTimes[badTimesCounter] = lineNum; // adds start of range...
  badTimesCounter++;
  badTimes[badTimesCounter] = lineNum; // ...and end of range
  badTimesCounter++; // badTimesCounter should always be even
}

void fixBadTimes() {
  int c,d;
  for(c = 0; c < badTimesCounter; c += 2) {
    for(d = c+2; d < badTimesCounter; d+= 2) {
      if(badTimes[d+1] > badTimes[c] && badTimes[d+1] < badTimes[c+1] &&
	 badTimes[d] < badTimes[c]) {
	badTimes[c] = badTimes[d];
	badTimes[d] = -1;
	badTimes[d+1] = -1;
      }
      if(badTimes[d] > badTimes[c] && badTimes[d] < badTimes[c+1] &&
	 badTimes[d+1] > badTimes[c+1]) {
	badTimes[c+1] = badTimes[d+1];
	badTimes[d] = -1;
	badTimes[d+1] = -1;
      }
    }
  }
}

void printResult(FILE *logFile, double dist) {
  int c;
  double timestamp, timestampSec;
  const double MICROSEC_TO_SEC = 1e-6;
  stringstream mess("");
  fixBadTimes(); // merges overlapping 'bad' ranges
  if(dist < 0)
    mess << "The vehicle hit one or more obstacle(s)!  ";
  else if (dist != 1e10)
    mess << "The vehicle comes as close as " << fixed << setprecision(2) 
	 << dist << " m from an obstacle.  ";
  if(dist >= safeDist && dist != 1e10)
    mess << "That is safe!\n";
  else if (dist != 1e10) {
    mess << "That is unsafe!\n";
    mess << "Approximated bad timestamp ranges:\n"; // lists bad time ranges
    for(c = 0; c < badTimesCounter; c++) {
      if(badTimes[c] != -1) { // ignores 'dead' ranges
	timestamp = lineNumToTimestamp(logFile,badTimes[c]);
	timestampSec = (timestamp - earliestTime) * MICROSEC_TO_SEC;
	//printf("%0.0lf\n",timestamp);
	if(c%2) { // odd
	  if(badTimes[c] == badTimes[c-1])
	    mess << "\n";
	  else
	    mess << fixed << setprecision(0) << " - " << timestampSec << " s ("
		 << timestamp << ")\n";
	} else // even
	  mess << " * " << fixed << setprecision(0) << timestampSec << " s ("
	       << timestamp << ")";
      }
    }
  }
  toLog(mess);
}

double lineNumToTimestamp(FILE *logFile, int lineNum) {
  double temp;
  int c;
  rewind(logFile);
  for(c = 0; c < lineNum; c++)
    fscanf(logFile,"%lf %*lf %*lf %*lf %*lf",&temp);
  return temp; // returns the lineNum'th line of the file
}

int readLogFile(FILE *logFile, double *timestamp,
		double *x, double *y, double *yaw) {
  double speed;
  int readLine;
  readLine = fscanf(logFile, "%lf %lf %lf %lf %lf", timestamp, y, x, yaw, &speed);
  // reads "timestamp northing easting yaw velocity"
  if(*timestamp < earliestTime) // finds the earliest timestamp
    earliestTime = *timestamp;
  return readLine;
}

int readDynObstFile(FILE *obstFile, double *obstTimestamp,
		    double *obstX, double *obstY, double *obstAngle) {
  return fscanf(obstFile, "%lf %lf %lf %lf", obstTimestamp, obstY, obstX, obstAngle);
  // reads "timestamp northing easting angle"
}

int readCircleObstFile(FILE *obstFile, double *obstX, double *obstY, double *obstRad) {
  return fscanf(obstFile, "%lf %lf %lf", obstY, obstX, obstRad);
  // reads "northing easting radius"
}

int readBlockObstFile(FILE *obstFile, double *obstX, double *obstY,
		      double *obstWidth, double *obstLength, double *obstAngle) {
  return fscanf(obstFile, "%lf %lf %lf %lf %lf",
		obstY, obstX, obstWidth, obstLength, obstAngle);
  // reads "northing easting width length angle"
}

void hitMsg(double dist) {
  if(dist == 1e10)
    printf("(An error has occured.  A log file likely didn't read correctly.)\n");
  else if(dist < 0)
    printf("(The vehicle hit this obstacle.)\n");
  else
    printf("(The vehicle comes as close as %0.2lf m from this obstacle.)\n", dist);
}

int parseObstLog(FILE *obstFile) {
  char str[100];
  double temp;
  int readLine, whitespaceCounter = 0, c = 0;

  readLine = fscanf(obstFile, "%lf", &temp);
  rewind(obstFile);
  if(readLine == EOF)
    whitespaceCounter = -1;
  else {
    fgets(str, 100, obstFile);
    cout << str << endl;
    while(str[c] != '\0' && str[c] != '\n') {
      if(str[c] == ' ') // whitespace counter
	whitespaceCounter++;
      c++;
    }
  }
  return whitespaceCounter;
}

double getClosestDistToObst(FILE *logFile, FILE *obstFile) {
  double obstTimestamp, obstX, obstY, dist;
  double obstRad = -1, obstWidth, obstLength, obstAngle;
  double closestDist = 1e10;
  int readLine, parseObstLogSwitch;

  rewind(obstFile);
  parseObstLogSwitch = parseObstLog(obstFile);
  rewind(obstFile);

  if(parseObstLogSwitch < 2 || parseObstLogSwitch > 4) // bad log file
    printf("(This log file is bad or empty.)\n");
  else {
    if(parseObstLogSwitch == 3) { // dynamic, block
      printf("* Dynamic block obstacle log detected. *\n");
      obstWidth = DYNAMIC_OBS_WIDTH; // for now, use fixed values for width/length
      obstLength = DYNAMIC_OBS_LENGTH;
      /*printf("Enter width (x-dimension): ");
      scanf("%lf", &obstWidth);
      printf("Enter length (y-dimension): ");
      scanf("%lf", &obstLength);*/
      readLine = readDynObstFile(obstFile,&obstTimestamp,&obstX,&obstY,&obstAngle);
      while(readLine != EOF) {
	dist = getDistToObstTimestamp(logFile,obstTimestamp,obstX,obstY,
				      obstRad,obstWidth,obstLength,obstAngle);	
	if(dist < closestDist)
	  closestDist = dist;
	readLine = readDynObstFile(obstFile,&obstTimestamp,&obstX,&obstY,&obstAngle);
      }
      hitMsg(closestDist);
    } else { // static
      if(parseObstLogSwitch == 2) { // static, circle
	printf("* Static circular obstacle log detected. *\n");
	readLine = readCircleObstFile(obstFile,&obstX,&obstY,&obstRad);
      } else { // static, block
	printf("* Static block obstacle log detected. *\n");
	readLine = readBlockObstFile(obstFile,&obstX,&obstY,
				     &obstWidth,&obstLength,&obstAngle);
      }
      while(readLine != EOF) {
	dist = getDistToObstTimestamp(logFile,-1,obstX,obstY,
				      obstRad,obstWidth,obstLength,obstAngle);
	hitMsg(dist);
	if(dist < closestDist)
	  closestDist = dist;
	if(parseObstLogSwitch == 2) // circle
	  readLine = readCircleObstFile(obstFile,&obstX,&obstY,&obstRad);
	else // block
	  readLine = readBlockObstFile(obstFile,&obstX,&obstY,
				       &obstWidth,&obstLength,&obstAngle);
      }
    }
  }
  return closestDist;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkStayInLane(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
		     vector<double> yaw, vector<double>& speed, 
		     vector<GPSPoint*>& closestWaypoints)
{
  if (closestWaypoints.size() == 0)
    return;
  stringstream mess("");
  mess << "Max distance from centerline:\n";
  toLog(mess);

  vector<double> distToCenterline;
  vector<GPSPoint*> prevWaypoints, nextWaypoints;
  int currentSegmentID = closestWaypoints[0]->getSegmentID();
  GPSPoint* currentWaypoint = closestWaypoints[0];
  GPSPoint* previousWaypoint = closestWaypoints[0];

  for (unsigned i = 0; i < closestWaypoints.size(); i++) {
    if (closestWaypoints[i]->getSegmentID() > rndf->getNumOfSegments())
      continue;
    if (closestWaypoints[i]->getSegmentID() != currentSegmentID  ||
	i == closestWaypoints.size() - 1) {
      int index;
      mess.str("");
      mess << "Segment " << currentSegmentID << ":\t" << fixed << setprecision(2) << findMax(distToCenterline, index);
      if (index >= 0) {
	mess << " m (between waypoint " << prevWaypoints[index]->getSegmentID() << "."
	     << prevWaypoints[index]->getLaneID() << "."
	     << prevWaypoints[index]->getWaypointID() << " and " 
	     << nextWaypoints[index]->getSegmentID() << "."
	     << nextWaypoints[index]->getLaneID() << "."
	     << nextWaypoints[index]->getWaypointID() << ")\n";
	if (DEBUG_LEVEL > 0) {
	  mess << "\t\t" << distToCenterline.size() << "\t" << prevWaypoints.size() << "\t"
	       << nextWaypoints.size() << "\t" << index << "\n";
	}
      }
      else {
	mess << "\n";
      }
      toLog(mess);
      distToCenterline.clear();
      prevWaypoints.clear();
      nextWaypoints.clear();
      currentSegmentID = closestWaypoints[i]->getSegmentID();
    }
    double angle = atan2(closestWaypoints[i]->getEasting() - easting[i], 
			 closestWaypoints[i]->getNorthing() - northing[i]) - yaw[i];
    /*
    if (closestWaypoints[i]->getSegmentID() == 3 && closestWaypoints[i]->getLaneID() == 1 &&
	closestWaypoints[i]->getWaypointID() == 2)
      cout << "3.1.2:\t" << cos(angle) << endl;
    */
    if (cos(angle) > 0 && closestWaypoints[i]->getWaypointID() > 1) {
      GPSPoint* waypoint2 = rndf->getWaypoint(closestWaypoints[i]->getSegmentID(),
					      closestWaypoints[i]->getLaneID(),
					      closestWaypoints[i]->getWaypointID() - 1);
      double distTmp = distPoint2Line(northing[i], easting[i],
				      closestWaypoints[i]->getNorthing(),
				      closestWaypoints[i]->getEasting(),
				      waypoint2->getNorthing(),
				      waypoint2->getEasting());
      distToCenterline.push_back(distTmp);
      prevWaypoints.push_back(waypoint2);
      nextWaypoints.push_back(closestWaypoints[i]);
    }
    else if (cos(angle) <= 0 && closestWaypoints[i]->getWaypointID() < 
	     rndf->getLane(closestWaypoints[i]->getSegmentID(), 
			   closestWaypoints[i]->getLaneID())->getNumOfWaypoints()) {
      GPSPoint* waypoint2 = rndf->getWaypoint(closestWaypoints[i]->getSegmentID(),
					      closestWaypoints[i]->getLaneID(),
					      closestWaypoints[i]->getWaypointID() + 1);
      double distTmp = distPoint2Line(northing[i], easting[i],
				      closestWaypoints[i]->getNorthing(),
				      closestWaypoints[i]->getEasting(),
				      waypoint2->getNorthing(),
				      waypoint2->getEasting());
      distToCenterline.push_back(distTmp);
      prevWaypoints.push_back(closestWaypoints[i]);
      nextWaypoints.push_back(waypoint2);
    }
    else if (closestWaypoints[i] != previousWaypoint &&
	     closestWaypoints[i]->getSegmentID() == previousWaypoint->getSegmentID() &&
	     closestWaypoints[i]->getLaneID() == previousWaypoint->getLaneID()) {
      GPSPoint* waypoint2 = previousWaypoint;
      double distTmp = distPoint2Line(northing[i], easting[i],
				      closestWaypoints[i]->getNorthing(),
				      closestWaypoints[i]->getEasting(),
				      waypoint2->getNorthing(),
				      waypoint2->getEasting());
      distToCenterline.push_back(distTmp);
      prevWaypoints.push_back(waypoint2);
      nextWaypoints.push_back(closestWaypoints[i]);
    }

    if (closestWaypoints[i] != currentWaypoint) {
      previousWaypoint = currentWaypoint;
      currentWaypoint = closestWaypoints[i];
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkStop(RNDF* rndf, vector<double>& northing, vector<double>& easting, 
	       vector<double> yaw, vector<double>& speed, 
	       vector<GPSPoint*>& closestWaypoints)
{
  if (closestWaypoints.size() == 0)
    return;

  stringstream mess("");
  mess << "Stop lines:\n";
  toLog(mess);
  unsigned i = 0;
  while ( i < closestWaypoints.size() ) {
    if (closestWaypoints[i]->getSegmentID() > rndf->getNumOfSegments()) {
      continue;
    }
    Waypoint* stopWaypoint = rndf->getWaypoint(closestWaypoints[i]->getSegmentID(),
					   closestWaypoints[i]->getLaneID(),
					   closestWaypoints[i]->getWaypointID());
    if (stopWaypoint == NULL) {
      mess.str("");
      mess << __FILE__ << ":" << __LINE__ << " ERROR: Can't find waypoint "
	   << closestWaypoints[i]->getSegmentID() << "."
	   << closestWaypoints[i]->getLaneID() << "."
	   << closestWaypoints[i]->getWaypointID() << "\n";
      toLog(mess);
      continue;
    }
    if (!stopWaypoint->isStopSign())
      i++;
    else {
      bool isStop = false;
      while (i < closestWaypoints.size() &&
	     closestWaypoints[i]->getSegmentID() == stopWaypoint->getSegmentID() &&
	     closestWaypoints[i]->getLaneID() == stopWaypoint->getLaneID() &&
	     closestWaypoints[i]->getWaypointID() == stopWaypoint->getWaypointID() ) {
	if (speed[i] < STOP_VEL && !isStop) {
	  isStop = true;
	  double angle = atan2(stopWaypoint->getEasting() - easting[i], 
			       stopWaypoint->getNorthing() - northing[i]) - yaw[i];
	  double distance = sqrt( pow(northing[i] - stopWaypoint->getNorthing(), 2) + 
				  pow(easting[i] - stopWaypoint->getEasting(), 2) );
	  mess.str("");
	  mess << "  Distance to " << stopWaypoint->getSegmentID() << "."
	       << stopWaypoint->getLaneID() << "."
	       << stopWaypoint->getWaypointID() << ":\t"
	       << fixed << setprecision(2) << distance * cos(angle) << " m\n";
	  toLog(mess);
	}
	i++;
      }
      if (!isStop) {
	mess.str("");
	mess << "ERROR: We did not stop at " << stopWaypoint->getSegmentID() << "."
	     << stopWaypoint->getLaneID() << "."
	     << stopWaypoint->getWaypointID() << "\n";
	toLog(mess);
      }
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void checkMission(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		  vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		  vector<GPSPoint*>& closestWaypoints, vector<Waypoint*>& checkpoints)
{
  stringstream mess("");
  vector<int> ckptsTime;
  if (timestamp.size() == 0) {
    if (checkpoints.size() != 0) {
      mess << "ERROR: Vehicle is stationary. No checkpoint is crossed\n";
      toLog(mess);
    }
    return;
  }
  unsigned currentCkptInd = 0;
  ckptsTime.push_back(timestamp[0]);
  unsigned i = 0;
  while (i < northing.size() && currentCkptInd < checkpoints.size()) {
    double ckptEasting = checkpoints[currentCkptInd]->getEasting();
    double ckptNorthing = checkpoints[currentCkptInd]->getNorthing();
    double angle = atan2(ckptEasting - easting[i], ckptNorthing - northing[i]) - yaw[i];
    double distance = sqrt( pow(northing[i] - ckptNorthing, 2) + 
			    pow(easting[i] - ckptEasting, 2) );
    if (closestWaypoints[i]->getSegmentID() == checkpoints[currentCkptInd]->getSegmentID() &&
	closestWaypoints[i]->getLaneID() == checkpoints[currentCkptInd]->getLaneID() &&
	closestWaypoints[i]->getWaypointID() == checkpoints[currentCkptInd]->getWaypointID() &&
	fabs(distance * cos(angle)) < 0.5 ) {
      if ( fabs(distance * sin(angle)) > VEHICLE_WIDTH/2 ) {
	mess.str("");
	mess << "WARNING: Make sure that Alice crossed checkpoint "
	     << currentCkptInd + 1 << " (The bumper is " << fabs(distance * sin(angle))
	     << " meters away from the checkpoint.)\n";
	toLog(mess);
      }
      currentCkptInd++;
      ckptsTime.push_back(timestamp[i]);
    }
    i++;
  }
  if (currentCkptInd == 0) {
    mess.str("");
    mess << "ERROR: No checkpoint is crossed\n";
    toLog(mess);
  }
  else if (currentCkptInd != checkpoints.size()) {
    mess.str("");
    mess << "ERROR: Last checkpoint crossed is " << currentCkptInd
	 << "(waypoint " << checkpoints[currentCkptInd-1]->getSegmentID()
	 << "." << checkpoints[currentCkptInd-1]->getLaneID()
	 << "." << checkpoints[currentCkptInd-1]->getWaypointID()
	 << ")\n";
    toLog(mess);
  }
  else {
    for (unsigned j = 1; j < ckptsTime.size(); j ++) {
      mess.str("");
      mess << "  From checkpoint " << j-1 << " to " << j << ":\t" 
	   << (ckptsTime[j] - ckptsTime[j-1]) * pow(10, -6)  << " seconds" << endl;
      toLog(mess);
    }
    mess.str("");
    mess << "\n" << "Time to complete mission:\t" 
	 << (ckptsTime[ckptsTime.size() - 1] - ckptsTime[0]) * pow(10, -6) 
	 << " seconds\n";
    toLog(mess);
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void getSegmentStat(RNDF* rndf, vector<int>& timestamp, vector<double>& northing, 
		    vector<double>& easting, vector<double> yaw, vector<double>& speed, 
		    vector<GPSPoint*>& closestWaypoints)
{
  stringstream mess("");
  double distance, headingDiff, headingLaneDir;
  int currentSegmentID = 0;
  unsigned currentSegmentStartInd = 0;
  double maxSpeed = 0;
  toLog(mess);
  for (unsigned i = 0; i < northing.size(); i++) {
    double maxHeadingLaneDir = 45*PI/180;
    distance = 10*MAX_DISTANCE;
    GPSPoint* closestPoint;
    while (distance > MAX_DISTANCE && maxHeadingLaneDir <= 90*PI/180) {
      closestPoint = rndf->findClosestGPSPoint(northing[i], easting[i], 0, 0,
					       distance, true, yaw[i],
					       2*PI, 0, maxHeadingLaneDir, 
					       headingDiff, headingLaneDir);
      maxHeadingLaneDir += 5*PI/180;
    }
    if (speed[i] > maxSpeed)
      maxSpeed = speed[i];

    if (DEBUG_LEVEL > 0) {
      mess.str("");
      mess << timestamp[i] << closestPoint->getSegmentID() << "." 
	   << closestPoint->getLaneID() << "."
	   << closestPoint->getWaypointID() << "\t" << distance << "\t" 
	   << "\t" << headingDiff*180/PI << "\n";
      toLog(mess);
    }
    closestWaypoints.push_back(closestPoint);
    if (i == 0)
      currentSegmentID = closestPoint->getSegmentID();
    if (currentSegmentID != closestPoint->getSegmentID() ||
	i == northing.size() - 1) {
      mess.str("");
      mess << "  Segment " << currentSegmentID << ":\t" << fixed
	   << setprecision(0) << (timestamp[i - 1] - timestamp[currentSegmentStartInd]) * pow(10, -6)
	   << " seconds\tmaxspeed: " << setprecision(2) << maxSpeed << " m/s\n";
      toLog(mess);
      currentSegmentStartInd = i;
      currentSegmentID = closestPoint->getSegmentID();
      maxSpeed = 0;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void getOverallStat(vector<int>& timestamp, vector<double>& northing, 
		    vector<double>& easting, vector<double>& speed)
{
  stringstream mess("");
  mess << "Stat: ";
  toLog(mess);

  double distance = 0;
  double sumSpeed = 0;
  double numSpeed = 0;
  double maxSpeed = 0;
  for (unsigned i = 0; i < northing.size()-1; i++) {
    distance += sqrt(pow(northing[i] - northing[i+1], 2) + pow(easting[i] - easting[i+1], 2));
    if (speed[i] > STOP_VEL) {
      sumSpeed += speed[i];
      numSpeed++;
    }
    if (speed[i] > maxSpeed)
      maxSpeed = speed[i];
  }

  mess.str("");
  mess << distance*0.000621371 << " mi, " 
       << sumSpeed*2.236936292/numSpeed << " mph (avg), " 
       << maxSpeed*2.236936292 << " mph (max)\n";
  toLog(mess);
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool parseState(char* fileName, vector<int>& timestamp,
		vector<double>& northing, vector<double>& easting,
		vector<double>& yaw, vector<double>& speed)
{
  stringstream mess("");
  ifstream file;
  string line;
  string word;
  int t;
  double n, e, y, v;
  bool isPaused = true;

  file.open(fileName, ios::in);

  if(!file)
  {
    mess << __FILE__ << ":" << __LINE__ << " ERROR: " << fileName << " file not found.\n";
    toLog(mess);
    return false;
  } 

  getline(file, line);
  
  while(!file.eof())
  { 
    istringstream lineStream(line, ios::in);

    lineStream >> word;
    t = atoi(word.c_str());
    lineStream >> word;
    n = atof(word.c_str());
    lineStream >> word;
    e = atof(word.c_str());
    lineStream >> word;
    y = atof(word.c_str());
    lineStream >> word;
    v = atof(word.c_str());

    if (v > STOP_VEL) {
      isPaused = false;
    }

    if (!isPaused) {
      timestamp.push_back(t);
      northing.push_back(n);
      easting.push_back(e);
      yaw.push_back(y);
      speed.push_back(v);
    }
    getline(file, line);
  }

  isPaused = true;
  int i = northing.size() - 1;
  while (isPaused && i > 0) {
    if (speed[i] > STOP_VEL) {
      isPaused = false;
    }
    if (isPaused) {
      timestamp.pop_back();
      northing.pop_back();
      easting.pop_back();
      yaw.pop_back();
      speed.pop_back();
    }
    i--;
  }

  file.close();
  return true;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
bool loadMDFFile(char* fileName, RNDF* rndf, vector<Waypoint*>& checkpoints, 
	     vector<double>& minSpeedLimits, vector<double>& maxSpeedLimits)
{
  stringstream mess("");
  ifstream file;
  string line;
  string word;

  file.open(fileName, ios::in);

  if(!file)
  {
    mess << __FILE__ << ":" << __LINE__ << " ERROR: fileName " << " file not found.\n";
    toLog(mess);
    return false;
  } 
  
  getline(file, line);
    
  istringstream lineStream(line, ios::in);
  
  lineStream >> word;

  if(word == "MDF_name")
  {
    parseCheckpoint(&file, rndf, checkpoints);
    parseSpeedLimit(&file, rndf, minSpeedLimits, maxSpeedLimits);
    file.close();
    return true;
  }
  else
  {
    file.close();
    return false;
  }
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void parseCheckpoint(ifstream* file, RNDF* rndf, vector<Waypoint*>& checkpoints)
{
  string line, word;
  char letter;
  int ckptID;  
  
  while(word != "end_checkpoints")
  {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    lineStream >> word;
    
    if(letter >= '0' && letter <= '9')
    {
      ckptID = atoi(word.c_str());
      checkpoints.push_back(rndf->getCheckpoint(ckptID));
    }
    else
    {
      continue;
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void parseSpeedLimit(ifstream* file, RNDF* rndf, 
				   vector<double>& minSpeedLimits, 
				   vector<double>& maxSpeedLimits)
{
  stringstream mess("");
  int segmentID;
  double minSpeed, maxSpeed;
  string line, word;
  char letter;

  while(word != "speed_limits") {    
    getline(*file, line);
    istringstream lineStream(line, ios::in);
    lineStream >> word;
  }

  
  while(word != "end_speed_limits") {    
    letter = file->peek();
    
    getline(*file, line);
    
    istringstream lineStream(line, ios::in);
    
    if(letter >= '0' && letter <= '9') {
      lineStream >> segmentID;
      lineStream >> minSpeed;
      lineStream >> maxSpeed;

      minSpeed = minSpeed * MPS_PER_MPH;
      maxSpeed = maxSpeed * MPS_PER_MPH;
      
      // setSpeedLimits(segmentID, minSpeed, maxSpeed, rndf);
      if ((unsigned)segmentID <= minSpeedLimits.size()) {
	minSpeedLimits[segmentID] = minSpeed;
	maxSpeedLimits[segmentID] = maxSpeed;
      }
      else {
	mess << __FILE__ << ":" << __LINE__ << " ERROR: got speed limit for segment " 
	     << segmentID << " while number of segments and zones = " 
	     << minSpeedLimits.size() << "\n";
	toLog(mess);
	int numOfSegments = minSpeedLimits.size();
	minSpeedLimits.resize(segmentID);
	maxSpeedLimits.resize(segmentID);
	for (unsigned i = numOfSegments; i <= minSpeedLimits.size(); i++)
	{
	  minSpeedLimits[i] = 0;
	  maxSpeedLimits[i] = 0;
	}
	minSpeedLimits[segmentID] = minSpeed;
	maxSpeedLimits[segmentID] = maxSpeed;
      }
    }
    else {
      lineStream >> word;
      continue;      
    }
  }
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double findMax(vector<double> vect, int& index)
{
  if (vect.size() == 0) {
    index = -1;
    return 0;
  }
  double ret = vect[0];
  index = 0;
  for (unsigned i = 1; i < vect.size(); i++) {
    if (vect[i] > ret) {
      ret = vect[i];
      index = i;
    }
  }
  return ret;
}


//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
double distPoint2Line(double px, double py, double l1x, double l1y, double l2x, double l2y)
{
  return fabs((l2x - l1x)*(l1y - py) - (l1x - px)*(l2y - l1y))/
    sqrt(pow(l1x - l2x, 2) + pow(l1y - l2y, 2));
}

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------
void toLog(stringstream& mess)
{
  cout << mess.str();
  fprintf (logFile, mess.str().c_str());
}
