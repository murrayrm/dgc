#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroFieldDemo2Run3'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo2_run3.mdf'
obsFile = 'eltoroFieldDemo2Run3.obs'
sceneFunc = 'eltoro_field_demo_2_run3'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
