#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'victorvilleAllSegs'
rndf = '../../etc/routes-victorville/victorville_full.rndf'
mdf = '../../etc/routes-victorville/victorville_allsegs.mdf'
obsFile = 'NoObs'
sceneFunc = 'victorville_allsegs'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )
