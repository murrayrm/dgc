#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSmallWaitToPassObstacle'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_rightleft.mdf'
obsFile = 'stlukeSmallWaitToPassObstacle.obs'
sceneFunc = 'stluke_small_waittopassobstacle'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )


