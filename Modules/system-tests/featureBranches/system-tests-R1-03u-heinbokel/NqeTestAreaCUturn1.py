#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '20.2.9'
scenario = 'NqeTestAreaCUturn1'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_areac_uturn1.mdf'
obsFile = 'NqeTestAreaC.obs'
sceneFunc = 'nqe_area_c_mod'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

