#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoro'
rndf = '../../etc/routes-eltoro/eltoro_field_demo1_short.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo_1.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

