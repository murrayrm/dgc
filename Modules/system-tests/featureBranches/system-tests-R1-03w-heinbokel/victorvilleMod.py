#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '59.1.1'
scenario = 'victorvilleMod'
rndf = '../../etc/routes-victorville/victorville_mod.rndf'
mdf = '../../etc/routes-victorville/victorville_mod_allsegs.mdf'
obsFile = 'NoObs'
sceneFunc = 'victorville_test'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

	