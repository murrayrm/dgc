#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroMerging'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_merging.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo2_intersection.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

