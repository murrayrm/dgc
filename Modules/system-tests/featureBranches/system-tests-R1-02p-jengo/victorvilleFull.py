#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'victorvilleFull'
rndf = '../../etc/routes-victorville/victorville_full.rndf'
mdf = '../../etc/routes-victorville/victorville_allsegs.mdf'
obsFile = 'NoObs'
sceneFunc = 'victorville_full'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
