#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'sinewave'
rddf = '../../etc/routes-stluke/stluke_sinewave.rddf'
traj = '../../etc/routes-stluke/stluke_sinewave.traj'
rndf = '../../src/graph-planner/stluke_bigloop.rndf'
mdf = '../../src/graph-planner/stluke_bigloop.rndf'
obsFile = 'noObs'
sceneFunc = ''

apps = [ \
('asim', '--rddf=%(rddf)s --no-pause --gcdrive' % locals(), 2, 'localhost'), \
 ('follower',  '--traj-file=%(traj)s' %locals(), 2 , 'localhost'), \
 ('gcdrive',  '--simulate --steer-speed-threshold=2 --pause-braking=0.9', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )


