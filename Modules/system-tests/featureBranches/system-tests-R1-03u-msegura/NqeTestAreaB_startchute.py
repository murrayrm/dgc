#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '40.1.1'
scenario = 'NqeTestAreaB_startchute'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_areaB_rmm1.mdf'
obsFile = 'NqeTestAreaB_startchute.obs'
sceneFunc = 'NqeTestAreaB_startchute'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

