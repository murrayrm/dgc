#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '17.1.2'
scenario = 'NqeTestAreaBZones'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_test_areab_zones.mdf'
obsFile = 'NqeTestAreaBZones.obs'
sceneFunc = 'nqe_test_areabzones'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

