#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.1.1'
scenario = 'NqeTestAreaA_original'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nae_areaA_rmm1.mdf'
obsFile = 'NqeTestAreaA_original.obs'
sceneFunc = 'NqeTestAreaA_original'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )
