#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.5'
scenario = 'santaanitaCarOppositeLane'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'noObs'
sceneFunc = 'santaanitaCarOppositeLane'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

