#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '4.1.1'
scenario = 'stlukeSmallPartLaneBlock'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_right.mdf'
obsFile = 'stlukeSmallPartLaneBlock.obs'
sceneFunc = 'stluke_small_partlaneblock'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )


