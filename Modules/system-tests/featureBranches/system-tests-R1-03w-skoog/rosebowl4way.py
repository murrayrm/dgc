#! /usr/bin/python -i
import sysTestLaunch

start = '3.1.1'
scenario = 'rosebowlLotf'
rndf = '../../etc/routes-rosebowl/rosebowl_4way.rndf'
mdf = '../../etc/routes-rosebowl/rosebowl_4way.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''

sysTestLaunch.runPlanners(start,scenario,obsFile,rndf,mdf,sceneFunc,world)

