#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '18.1.13'
scenario = 'stlukearea60MileNoObstacles'
rndf = '../../etc/routes-stlukearea/stluke_area.rndf'
mdf = '../../etc/routes-stlukearea/stluke_area.mdf'
obsFile = 'noObs'
sceneFunc = 'stlukearea60MileNoObstacles'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
