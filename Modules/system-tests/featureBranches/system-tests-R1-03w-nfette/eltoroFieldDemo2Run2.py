#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroFieldDemo2Run2'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo2_run2.mdf'
obsFile = 'noObs'
sceneFunc = 'eltoro_field_demo_2_run2'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


