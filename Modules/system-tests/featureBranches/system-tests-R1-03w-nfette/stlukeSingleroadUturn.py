#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.5'
scenario = 'stlukeSingleroadUturn'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



