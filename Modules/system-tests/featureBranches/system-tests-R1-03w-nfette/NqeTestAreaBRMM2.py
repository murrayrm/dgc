#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '32.1.1'
scenario = 'NqeTestAreaB'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_areaB_rmm2.mdf'
obsFile = 'NqeTestAreaB.obs'
sceneFunc = 'nqe_area_b'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



