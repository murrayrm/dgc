#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSmallZoneExtended'
rndf = '../../etc/routes-stluke/stluke_small_zone_extended.rndf'
mdf = '../../etc/routes-stluke/stluke_small_zone_extended.mdf'
obsFile = 'stlukeSmallZoneExtended.obs'
sceneFunc = 'stlukesmallzoneextended'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )




