#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoro_north_of_perim_parkinglot'
rndf = '../../etc/routes-eltoro/eltoro_field_demo_zone.rndf'
mdf = '../../etc/routes-eltoro/eltoro_field_demo_zone_2.mdf'
obsFile = 'NoObs'
sceneFunc = 'eltoro_field_demo_north_of_perim'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



