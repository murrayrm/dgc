#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '36.2.35'
scenario = 'victorvilleOffsetTStop'
rndf = '../../etc/routes-victorville/victorville_mod.rndf'
mdf = '../../etc/routes-victorville/victorville_offsettstop.mdf'
obsFile = 'NoObs'
sceneFunc = 'victorville_test'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


