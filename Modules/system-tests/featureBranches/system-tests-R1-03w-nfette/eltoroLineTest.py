#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroFieldDemoMDF1'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_adj.rndf'
mdf = '../../etc/routes-eltoro/lineTest.mdf'
obsFile = 'noObs'
sceneFunc = 'eltoro_buildingsandbushes'
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


