#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '3.1.1'
scenario = 'stlukeSmallStandard'
rndf = '../../etc/routes-stluke/stluke_small_distorted.rndf'
mdf = '../../etc/routes-stluke/stluke_small_standard.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = '../../etc/routes-stluke/stluke_small_standard.world'
tsfile = '../../etc/routes-stluke/stluke_small.ts'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )

