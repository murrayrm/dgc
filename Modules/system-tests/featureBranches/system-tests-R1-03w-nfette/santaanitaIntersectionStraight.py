#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.1'
scenario = 'santaanitaIntersectionStraight'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_straight.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )



