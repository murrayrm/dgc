#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'eltoroEstopTEst'
rndf = '../../etc/routes-eltoro/eltoro_estop_test.rndf'
mdf = '../../etc/routes-eltoro/eltoro_estop_test.mdf'
obsFile = 'noObs'
sceneFunc = ''
world = ''
tsfile = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world, tsfile )


