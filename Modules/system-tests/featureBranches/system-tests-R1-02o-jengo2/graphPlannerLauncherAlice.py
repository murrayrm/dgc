#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'graphplanner'
rndf = '../../src/graph-planner/stluke_bigloop.rndf'
mdf = '../../src/graph-planner/stluke_bigloop.mdf'
obsFile = 'noObs'
sceneFunc = ''

apps = [ \
('graph-planner-viewer', '--rndf=%(rndf)s --goal=2 --loop=5' % locals(), 2 , 'localhost'), \
 ('gcfollower',  '', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
 ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )