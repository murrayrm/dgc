#include <getopt.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <signal.h>
#include <math.h>
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "alice/AliceConstants.h"
#include "interfaces/sn_types.h"
#include "skynettalker/StateClient.hh"
#include "statelogger_cmdline.h"

class StateLogger : public CStateClient
{ 
  FILE* m_logFile;
  unsigned long long startTime;
  
public:
  StateLogger(char* fileName, int skynetKey) : 
    CSkynetContainer(SNstateSim, skynetKey), CStateClient(true)
  {
    m_logFile = fopen(fileName, "wt");
    if (m_logFile == NULL) {
      cerr << "Cannot open log file: " << fileName << endl;
      exit(1);
    }
    UpdateState();
    startTime = m_state.timestamp;
  }

  ~StateLogger()
  {
    fclose(m_logFile);
  }

  void listeningLoop()
  {
    UpdateState();
    unsigned timestamp = m_state.timestamp - startTime;
    double northingFront = m_state.utmNorthing + 
      DIST_REAR_AXLE_TO_FRONT*cos(m_state.utmYaw);
    double eastingFront = m_state.utmEasting + 
      DIST_REAR_AXLE_TO_FRONT*sin(m_state.utmYaw);
    double yaw = m_state.utmYaw;
    double vel = sqrt(pow(m_state.utmNorthVel, 2) + 
		      pow(m_state.utmEastVel, 2));
    fprintf(m_logFile, "%d\t%0.2lf\t%0.2lf\t%0.6lf\t%0.4lf\n",
	    timestamp, northingFront, eastingFront, yaw, vel);
  }
};

using std::cout;

volatile sig_atomic_t quit = 0;

void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 2) {
    abort();
  }
  quit++;
}

int main(int argc, char** argv)
{
  // catch CTRL-C and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  // and SIGTERM, i.e. when someone types "kill <pid>" or the like
  signal(SIGTERM, sigintHandler);

  // Parse command line options
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  // Figure out the skynet key
  int skynetKey = skynet_findkey(argc, argv);
  cout << "StateLogger: log file = " << cmdline.file_arg <<endl;
  StateLogger stateLogger = StateLogger(cmdline.file_arg, skynetKey);
  while (!quit) {
    stateLogger.listeningLoop();
    usleep(100000);
  }
}
