#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '3.1.1'
scenario = 'santaanitaSmallStandard'
rndf = '../../etc/routes-santaanita/santaanita_small.rndf'
mdf = '../../etc/routes-santaanita/santaanita_small_standard.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

