#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '7.1.4'
scenario = 'eltoroFieldDemo1'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_adj.rndf'
mdf = '../../etc/routes-eltoro/Sept_2007_4.mdf'
#mdf = '../../etc/routes-eltoro/eltoro_field_demo2_zone.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
