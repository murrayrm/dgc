import os;
import sys;
import time;
import re;


# here is a sample apps array
# testapps = [ \
# ('asim',  '--rndf=%(rndf)s' % locals(), 2, 'berlin'), \
#  ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p --nomap' % locals(), 2 , 'localhost'), \
#  ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'localhost'), \
#  ('rddfplanner',  '--use-endpoints --use-flat --verbose=4', 2 , 'localhost'), \
#  ('trajfollower',  '--use-local', 2 , 'localhost'), \
#  ('planviewer',  '--use-local', 2 , 'localhost'), \
# ]
#

def runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, bRunTrafsim ):
  packagePath = '../../'

  # please define the rndf and mdf functions from the calling script, as in the 
  # examples below
  # rndf = '../../etc/routes-stluke/tplanner_unittest.rndf'
  # mdf = '../../etc/routes-stluke/tplanner_unittest_rightleft.mdf'
  # sceneFunc = 'stluke_singleroad_partlaneblock'
  #if( not ('rndf' in globals() and 'mdf' in globals() and 'obsFile' in globals()) ):
    #print "you must specify the obsFile, rndf and mdf variables as global variables"
    #print globals()
    #sys.exit()
  if( not sceneFunc ):
    print "no sceneFunc defined, no obstacles will be placed on map"

  try:
    os.mkdir ( './logs' )
  except:
    print './logs folder already created. leaving' 
  logname = './logs/systemtest-%s-%s.log' % ( time.strftime( '%Y-%m-%d-%H-%M-%S' ), scenario ) 
  #logname = './logs/systemtest-%s-%s.log' % ( time.strftime( '%Y-%m-%d-%H-%M-%S' ), re.sub( '.*/', '', re.sub( r'\.mdf$', '', mdf) ) )
  print "log name: %s" % (logname)

  cwd = os.getcwd()
  print cwd
  appdict = {}
  for i in apps:
    (appname, params, sleeptime, host) = i
    appdict[appname] = [appname, '', '', 0]   #(appname, fullpath, pid)

  bFoundAll = True
  for root, dirs, files in os.walk('../../bin'):
    for name in files:
      if name in appdict.keys():
        #print 'found our target'
        #print os.path.join( root, name )
        lpath =  os.path.join( os.getcwd(), os.path.join(root,name) )
        if( os.path.islink(lpath) ):
          path =  os.readlink(lpath)  
          path = os.path.join(os.path.dirname(lpath), path)
          if( os.path.isfile(path) ):
            appdict[name][1] = lpath
          else:
            print 'found a broken link: %s -> %s ' % (lpath, path)
            bFoundAll = False
        else:
          appdict[name][1] = lpath
        print appdict[name]

  print 
  print 

  for i in appdict.keys():
    if appdict[i][1] == '':
      print 'could not find path for %s' % i
      bFoundAll = False 
  if not bFoundAll:
    print 'unavailable modules, exiting'
    sys.exit()


  for i in apps:
    (appname, params, sleeptime, host) = i
    path = appdict[appname][1]
    # pid = os.spawnle( os.P_NOWAIT, 'xterm -e "%(appname)s" %(params)s' % locals(), os.environ)
    sshcmd = ''
    if host != 'localhost':
      cmd = '''ssh -Y %(host)s "xterm -T %(appname)s -e '{ echo; source /etc/profile.dgc; cd "%(cwd)s"; "%(path)s" %(params)s; echo press enter to close; read; }' "  &  ''' % locals()
    else:
      cmd = '''xterm -T %(appname)s -e '{ echo; source /etc/profile.dgc; "%(path)s" %(params)s; echo press enter to close; read; }'  &  ''' % locals()
    print cmd
    pid = os.system( cmd );
    # pid = os.environ['last']
    # print pid
    os.system( 'sleep %(sleeptime)d' % locals() )


  print "starting the statelogger"
  os.system( 'xterm -e "{ %(packagePath)sbin/Drun statelogger --file=%(logname)s; echo press return to close; read; }" & ' % locals() )

  print "press enter to start the trafsim stack, or C-d or C-c to interrupt"
  blah = raw_input()
  runTrafsimStack(obsFile, rndf, mdf, sceneFunc, logname, bRunTrafsim, packagePath )



def runTrafsimStack( obsFile, rndf, mdf, sceneFunc, logname, bRunTrafsim, packagePath ):
  
            #'%(packagePath)s./bin/Drun python ../../lib/PYTHON/trafsim/GenericApp.py  ' % locals(), \
  stack = [ '%(packagePath)s./bin/Drun mapviewer ' % locals(), \
            '%(packagePath)s./bin/Drun planviewer2 --use-local ' % locals(), \
            '%(packagePath)s./bin/Drun mapper --rndf=%(rndf)s --noconsole ' % locals() ]
  if bRunTrafsim:
    tsfile = re.sub( r'\.rndf', '.ts', rndf )
    if 'sceneFunc' in locals () and sceneFunc:
      stack = stack + [ '%(packagePath)s./bin/Drun python -i %(packagePath)slib/PYTHON/trafsim/GenericApp.py --loadFile %(tsfile)s --loadFunction %(sceneFunc)s '  % locals() ]
    else:
      stack = stack + [ '%(packagePath)s./bin/Drun python -i %(packagePath)slib/PYTHON/trafsim/GenericApp.py --loadFile %(tsfile)s '  % locals() ]


  for i in stack:
    os.system( '''xterm -e "{ %s ; echo press return; read ; }" &''' % (i) )

  print "once testing is complete, close all windows, and press enter to analyze"
  blah = raw_input()
  os.system( '%(packagePath)sbin/Drun analyzer --state-logfile=%(logname)s --obs-logfile=%(obsFile)s --rndf=%(rndf)s --mdf=%(mdf)s' % locals() )

