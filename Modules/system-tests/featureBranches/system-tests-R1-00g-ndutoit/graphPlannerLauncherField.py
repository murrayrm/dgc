#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'graphplanner'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_right.mdf'
obsFile = 'noObs.log'
sceneFunc = ''

apps = [ \
('graph-planner-viewer', '--rndf=%(rndf)s --goal=2 --loop=1 --rate=2' % locals(), 2 , 'localhost'), \
 ('gcfollower',  '', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
 ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )