#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '10.2.1'
scenario = 'eltoroFieldDemoMDF1'
rndf = '../../etc/routes-eltoro/eltoro_field_demo2_adj_sparse.rndf'
mdf = '../../etc/routes-eltoro/Sept_2007_1.mdf'
obsFile = 'noObs'
sceneFunc = 'eltoro_buildingsandbushes'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
