#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSingleroadPartLaneBlockField'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'stlukeSingleroadPartLaneBlockObs.log'
sceneFunc = 'stluke_singleroad_partlaneblock'

apps = [ \
('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'localhost'), \
 ('rddfplanner',  '--use-endpoints --use-flat --verbose=2', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
# ('gcdrive',  '--simulate', 2 , 'leela'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

