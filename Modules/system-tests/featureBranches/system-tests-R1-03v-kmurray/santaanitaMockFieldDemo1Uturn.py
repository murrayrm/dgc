#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '6.2.2'
scenario = 'santaanitaMockFieldDemo1Uturn'
rndf = '../../etc/routes-santaanita/santaanita_mock_field_demo1.rndf'
mdf = '../../etc/routes-santaanita/santaanita_mock_field_demo1.mdf'
obsFile = 'santaanitaMockFieldDemo1Uturn.obs'
sceneFunc = 'santaanita_mock_field_demo1_Uturn'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

