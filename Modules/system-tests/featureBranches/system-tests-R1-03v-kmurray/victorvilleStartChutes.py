#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '47.1.1'
scenario = 'victorvilleTest'
rndf = '../../etc/routes-victorville/victorville_mod.rndf'
mdf = '../../etc/routes-victorville/victorville_startfinish.mdf'
obsFile = 'NoObs'
sceneFunc = 'victorville_test'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )
