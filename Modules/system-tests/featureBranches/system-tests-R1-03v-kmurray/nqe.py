#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

#start = '43.0.30'
start = '2.1.2'
scenario = 'nqe'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
#mdf = '../../etc/routes-victorville/victorville_startfinish.mdf'
obsFile = 'noObs'
sceneFunc = ''

mdf = sysTestLaunch.selectMDF()
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

