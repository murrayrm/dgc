#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'santaanitaIntersectionStraightSim'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_straight.mdf'
obsFile = 'noObs.log'
sceneFunc = ''

apps = [ \
('asim', '--no-pause --rndf=%(rndf)s --rndf-start=2.2.1 --gcdrive' % locals(), 2, 'localhost'), \
 ('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local --send-costmap' % locals(), 2 , 'localhost'), \
 ('s1planner',  '', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'localhost'), \
 ('gcdrive',  '--simulate', 2 , 'localhost'), \
# ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, True )

