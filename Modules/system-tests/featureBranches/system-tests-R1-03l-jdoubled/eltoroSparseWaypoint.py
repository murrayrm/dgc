#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '24.4.2'
scenario = 'eltoroSparseWaypoint'
rndf = '../../etc/routes-eltoro/eltoro_sparse_waypoint.rndf'
mdf = '../../etc/routes-eltoro/eltoro_sparse_waypoint.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
