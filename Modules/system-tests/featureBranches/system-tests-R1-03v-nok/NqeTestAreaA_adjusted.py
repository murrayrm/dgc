#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.1.1'
scenario = 'NqeTestAreaA_adjusted'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_areaA_rmm1.mdf'
obsFile = 'NqeTestAreaA_adjusted.obs'
sceneFunc = 'NqeTestAreaA_adjusted'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

