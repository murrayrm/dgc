#! /usr/bin/python -i

import os;
import sys;
import time;

import sysTestLaunch

scenario = 'followerCurvyPath'
rndf = '../../etc/routes-stluke/stluke_sinewave.rndf'
mdf = '../../etc/routes-stluke/stluke_sinewave.mdf'
obsFile = 'noObs'
sceneFunc = ''
lognameTimestamp = time.strftime( '%Y-%m-%d-%a-%H-%M' )

apps = [ \
('asim', '--no-pause --rndf=%(rndf)s --rndf-start=1.1.1 --gcdrive' % locals(), 2, 'localhost'), \
 ('follower', '--paramfile paramfile -f localSim --vis-lat-control --lo-alpha=1.7 --traj-file=../../etc/routes-stluke/stluke_sinewave.traj' % locals(), 2 , 'localhost'), \
 ('gcdrive',  '--simulate --steer-speed-threshold=2 --pause-braking=0.9', 2 , 'localhost'), \
 ('testMapper', '%(rndf)s 2' %locals(), 2, 'localhost'), \
 ('planviewer2',  '-l', 2 , 'localhost'), \
 ('mapviewer', '--recv-subgroup=-2', 2, 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False, lognameTimestamp )


