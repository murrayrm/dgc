#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '17.1.2'
scenario = 'NqeTestAreaB_zonetest'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-darpa/nqe_test_areab_zones.mdf'
obsFile = 'NqeTestAreaB_zonetest.obs'
sceneFunc = 'NqeTestAreaB_zonetest'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )

