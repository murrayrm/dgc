#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '6.1.1'
scenario = 'stlukeSmallFieldDemo2'
rndf = '../../etc/routes-stluke/stluke_small_zone_extended.rndf'
mdf = '../../etc/routes-stluke/stluke_small_zone_extended_zone9only.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

