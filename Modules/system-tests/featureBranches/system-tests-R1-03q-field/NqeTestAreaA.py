#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'NqeTestAreaA'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_test2.mdf'
obsFile = 'NqeTestAreaA.obs'
sceneFunc = 'nqe_area_a'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
