#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSingleroadFollowing'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'noObs'
sceneFunc = 'stluke_singleroad_following'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
