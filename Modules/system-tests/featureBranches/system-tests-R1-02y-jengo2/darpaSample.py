#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'darpaSample'
rndf = '../../etc/routes-darpa/darpa_sample.rndf'
mdf = '../../etc/routes-darpa/darpa_sample.mdf'
obsFile = 'darpaSample.obs'
sceneFunc = 'darpasample'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
