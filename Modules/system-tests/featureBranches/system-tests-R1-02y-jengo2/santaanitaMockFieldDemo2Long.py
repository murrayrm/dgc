#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '6.2.1'
scenario = 'santaanitaMockFieldDemo2Long'
rndf = '../../etc/routes-santaanita/santaanita_mock_field_demo2_zone.rndf'
mdf = '../../etc/routes-santaanita/santaanita_mock_field_demo2_long.mdf'
obsFile = 'santaanitaMockFieldDemo2Long.obs'
sceneFunc = 'santaanita_mock_field_demo2_long'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
