#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeComplexZones'
rndf = '../../etc/routes-stluke/stluke_complex.rndf'
mdf = '../../etc/routes-stluke/stluke_complex_zones.mdf'
obsFile = 'NoObs'
sceneFunc = 'stlukecomplex_zones'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
