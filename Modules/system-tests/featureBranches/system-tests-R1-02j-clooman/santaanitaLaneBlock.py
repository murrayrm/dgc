#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.2.5'
scenario = 'santaanitaLaneBlock'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaLaneBlock.obs'
sceneFunc = 'santaanita_sitevisit_block'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

