#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'onewayIntersectionRoadBlock'
rndf = '../../etc/routes-advanced/oneway_intersection.rndf'
mdf = '../../etc/routes-advanced/oneway_intersection_roadblock.mdf'
obsFile = 'onewayIntersectionRoadBlock.obs'
sceneFunc = 'onewayinter_roadblock'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
