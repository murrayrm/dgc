#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '19.1.1'
scenario = 'NqeTestAreaC'
rndf = '../../etc/routes-darpa/nqe.rndf'
mdf = '../../etc/routes-darpa/nqe_test3.mdf'
obsFile = 'NqeTestAreaC.obs'
sceneFunc = 'nqe_area_c_mod'

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)
