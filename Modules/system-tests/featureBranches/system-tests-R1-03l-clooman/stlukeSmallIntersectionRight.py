#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'stlukeSmallIntersectionRight'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_right.mdf'
obsFile = 'noObs'
sceneFunc = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc )

