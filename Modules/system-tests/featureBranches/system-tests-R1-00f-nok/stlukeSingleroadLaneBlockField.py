#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch


scenario = 'stlukeSingleroadLaneBlockField'
rndf = '../../etc/routes-stluke/stluke_singleroad.rndf'
mdf = '../../etc/routes-stluke/stluke_singleroad_testobs.mdf'
obsFile = 'stlukeSingleroadLaneBlockObs.log'
sceneFunc = 'stluke_singleroad_laneblock'

apps = [ \
('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'bender'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'bender'), \
 ('rddfplanner',  '--use-final --use-flat --verbose=2', 2 , 'bender'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
 ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

