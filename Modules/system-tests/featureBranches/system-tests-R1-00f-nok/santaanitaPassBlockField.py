#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'santaanitaPassBlockField'
rndf = '../../etc/routes-santaanita/santaanita_sitevisit.rndf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_testobs.mdf'
obsFile = 'santaanitaSiteVisitPassBlockObs.log'
sceneFunc = 'santaanita_sitevisit_passblock'

apps = [ \
('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'localhost'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'localhost'), \
 ('rddfplanner',  '--use-final --use-flat --verbose=2', 2 , 'localhost'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
# ('gcdrive',  '', 2 , 'leela'), \
# ('planviewer',  '--use-local', 2 , 'fry'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

