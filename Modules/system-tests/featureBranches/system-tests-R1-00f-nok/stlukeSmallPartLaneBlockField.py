#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'stlukeSmallIntersectionRightField'
rndf = '../../etc/routes-stluke/stluke_small.rndf'
mdf = '../../etc/routes-stluke/stluke_small_right.mdf'
obsFile = 'stlukeSmallPartLaneBlockObs.log'
sceneFunc = 'stluke_small_partlaneblock'

apps = [ \
('mplanner', '--rndf=%(rndf)s --mdf=%(mdf)s -l -p' % locals(), 2 , 'bender'), \
 ('tplannerNoCSS', '--rndf=%(rndf)s --disable-console --debug=1 --use-local' % locals(), 2 , 'bender'), \
 ('rddfplanner',  '--use-final --use-flat --verbose=2', 2 , 'bender'), \
 ('gcfollower',  '--use-local', 2 , 'leela'), \
 ('gcdrive',  '', 2 , 'leela'), \
 ('planviewer',  '--use-local', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

