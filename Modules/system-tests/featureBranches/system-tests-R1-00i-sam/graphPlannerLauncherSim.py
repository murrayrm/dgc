#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

scenario = 'graphplanner'
rndf = '../../src/graph-planner/stluke_bigloop.rndf'
mdf = '../../src/graph-planner/stluke_bigloop.rndf'
obsFile = 'noObs.log'
sceneFunc = ''

apps = [ \
('asim', '--no-pause --rndf=%(rndf)s --rndf-start=1.1.1 --gcdrive' % locals(), 2, 'localhost'), \
('graph-planner-viewer', '--rndf=%(rndf)s --goal=2 --loop=5' % locals(), 2 , 'localhost'), \
 ('trajfollower',  '--gcdrive --disable-trans', 2 , 'localhost'), \
 ('gcfollower',  '', 2 , 'localhost'), \
 ('gcdrive',  '--simulate', 2 , 'localhost'), \
# ('planviewer',  '', 2 , 'localhost'), \
]

sysTestLaunch.runApps( apps, scenario, obsFile, rndf, mdf, sceneFunc, False )

