#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '2.1.1'
scenario = 'nqeTestAreaACars'
rndf = '../../etc/routes-darpa/nqe_mod.rndf'
mdf = '../../etc/routes-victorville/nqe_practice1.mdf'
obsFile = ''
sceneFunc = 'nqe_test_area_a_cars'
world = ''

sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc, world )
