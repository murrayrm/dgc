/* astate.h - platform-independent state struct
 *
 * Lars Cremean
 * 6 Jan 05
 * 
 * This file defines platform-independent state structs intended for use by 
 * various code that constructs, sends or receives state information.
 */

#ifndef VEHICLESTATE_H
#define VEHICLESTATE_H

/** 
 * New, clean struct for vehicle state representation.  Originally
 * implemented as GetVehicleStateMsg in bob/vehlib/VState.hh, but
 * moved here because it is platform-independent.  All values use the
 * center of the rear axle as the origin of the vehicle frame unless
 * otherwise stated
 *
 * The weird ordering of members is designed to preserve binary
 * compatability with old code.
 */
typedef struct _VehicleState
{
  /** Timestamp represents UNIX-time. This is the value of microseconds since
   *  the epoch (1/1/1970) */
  unsigned long long timestamp;

  /** Position of the vehicle in the UTM frame (meters).  To convert
      these to a global cartesian frame, use: (x = northing, y =
      easting, z = altitude).  Note that the altitude is positive
      /downward/ and zero at sea-level. */
  double utmNorthing, utmEasting, utmAltitude;

  /** Position of the vehicle in a site-centric frame (meters).
      The coordinate frame is aligned with the UTM frame, but translated
      such that (0, 0, 0) corresponds to the initial vehicle location.
      Note that this is *not* the same as the local pose (defined below),
      since the local frame is not necessarily aligned with global north.
   */
  double siteNorthing, siteEasting;

  /** Linear velocity of the vehicle in the UTM frame (m/sec). */
  double utmNorthVel, utmEastVel, utmAltitudeVel;

  /** Component of linear acceleration in the Northing direction (m/s/s).  */
  double Acc_N_deprecated;

  /** Component of linear acceleration in the Easting direction (m/s/s).  */
  double Acc_E_deprecated;

  /** Component of linear acceleration in the downward direction (m/s/s).  */
  double Acc_D_deprecated;

  /** Rotation of the vehicle in the UTM frame (radians); these
      are Euler angles with yaw applied last.  Yaw can also be
      interpreted as a conventional compass heading, i.e., yaw = 0 if
      the vehicle is facing due north, yaw = pi/2 if the vehicle is
      facing due east, etc. */
  double utmRoll, utmPitch, utmYaw;

  /** Angular velocity of the vehicle in the UTM frame (radians/sec). */
  double utmRollRate, utmPitchRate, utmYawRate;

  /** Altitude of the vehicle in the site frame (m). */
  double siteAltitude;
  
  /** Rotation of the vehicle in site frame.  This is identical to the
      rotation in the UTM frame (see below) and is provided for
      consistency only. */
  double siteRoll, sitePitch, siteYaw;
    
  /** Confidence intervals on vehicle position in UTM frame.
      @todo What units are these measured in? */
  double utmNorthConfidence, utmEastConfidence, utmAltitudeConfidence;  

  // Old variables to preserve structure
  double rollConfidence;		//<! Confidence level in Roll
  double pitchConfidence;		//<! Confidence level in Pitch
  double yawConfidence;			//<! Confidence level in Yaw

  /** Vehicle ground speed (m/sec). */
  double vehSpeed;

  /** Vehicle UTM zone (e.g., 11S for Southern California) */
  int utmZone, utmLetter;
    
  /** Linear velocities in vehicle frame (m/sec). */
  double vehXVel, vehYVel, vehZVel;

  /** Angular velocities in vehicle frame (rad/sec). */
  double vehRollRate, vehPitchRate, vehYawRate;

  /** Position in local frame (m); +z is down. */
  double localX, localY, localZ;

  /** Rotation in local frame (radians); these are Euler angles with
      yaw applied last. */
  double localRoll, localPitch, localYaw;

  /** Linear velocity of the vehicle in the local frame (m/sec). */
  double localXVel, localYVel, localZVel;

  /** Angular velocity of the vehicle in the local frame (radians/sec). */
  double localRollRate, localPitchRate, localYawRate;
  
}  __attribute__((packed)) VehicleState;


#endif
