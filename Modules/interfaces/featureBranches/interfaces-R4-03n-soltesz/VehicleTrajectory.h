/*!
 * \file VehicleTrajectory.h
 * \brief Commanded vehicle trajectory interface
 * 
 * This header file describes the interface used to send trajectories
 * between a planner and the control system.
 */

#ifndef _TRAJ_H_
#define _TRAJ_H_

// TRAJ_MAX_LEN is defined 
//#define TRAJ_MAX_LEN  50000

#ifdef UNUSED
/*!
 * Format for an individual trajectory point.
 */
struct TrajPoint {
  double n, e, nd, ed, ndd, edd;
};
#endif

/*! 
 * This is the trajectory container class. The trajectory stored is a
 * list of Northing values, their derivatives, Easting values and
 * their derivatives. The number of derivatives stored is given by the
 * m_trajHeader.m_order member (order = maxderivativenumber+1). The
 * number of points stored is hard-limited by TRAJ_MAX_LEN. Setting
 * this high will only affect local memory allocation and will not
 * slow down anything that sends trajectories over the network, since
 * only m_numPoints points would be sent. The actual data is stored as
 * two one-dimensional arrays in m_pN, m_pE. Those arrays are indexes
 * by the INDEX macro.
*/

struct VehicleTrajectory {
  struct STrajHeader {
    int m_numPoints;		// number of points in this trajectory
    int m_order;        // max derivatives of each point+1 (x,xdot -> 2)
  } __attribute__((packed)) trajHeader;
  
  // actual trajectory: northing, easting and their derivatives
  // each array holds at most TRAJ_MAX_LEN*m_trajHeader.m_order points
  double  *pN;
  double  *pE;
};

#endif // _TRAJ_H_
