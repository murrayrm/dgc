/* Process Response struct
 *
 * David Trotz dctrotz@jpl.nasa.gov
 * 10 April 07
 *
 * This file defines various response data that a process might 
 * provide to the proctl module.
 */

#ifndef PROCESS_RESPONSE_H
#define PROCESS_RESPONSE_H

/* Be very careful here, I am using the basic assignment operator in the proctl module.
 * If you add a member here that uses dynamic memory allocation, the assignment will 
 * be undefined and strange things will happen.
 */

typedef struct _ProcessResponse
{
  // Originating Module
  int moduleId;
  
  // Time when message was constructed.
  uint64_t timestamp;

  // Total KB logged
  int logTotal;

} __attribute__((packed)) ProcessResponse;

#endif // PROCESS_RESPONSE_H
