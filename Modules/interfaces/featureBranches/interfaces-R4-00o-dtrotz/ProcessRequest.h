/* Process Request struct
 *
 * David Trotz dctrotz@jpl.nasa.gov
 * 10 April 07
 *
 * This file defines various response data that a process might 
 * provide to the proctl module.
 */

#ifndef PROCESS_REQUEST_H
#define PROCESS_REQUEST_H

typedef struct _ProcessRequest
{
  // Originating Module
  int moduleId;
  
  // Time when message was constructed.
  uint64_t timestamp;

  // Log state (1 enabled, 0 not enabled)
  int enableLog;

  // Log file name (empty string or null to ignore)
  char logFName[512];

  // Quit (1 quit, 0 ignore)
  int quit;

} __attribute__((packed)) ProcessRequest;

#endif // PROCESS_REQUEST_H
