#ifndef _STATEPRECISION_H_
#define _STATEPRECISION_H_

typedef struct VehiclePrecisionPlus
{
    
  unsigned long long timestamp;
    
  double utmNorthConfidence;
  double utmEastConfidence;
  double utmAltitudeConfidence;  
  double rollConfidence;	
  double pitchConfidence;	
  double yawConfidence;	
    
  double velNorthRMS;
  double velEastRMS;
  double velDownRMS;
    
  double errEllipMajor;
  double errEllipMinor;
  double errEllipAngle;

  template <class Archive>
  void serialize(Archive &ar,const unsigned int version)
    {
      ar & timestamp;
    
      ar & utmNorthConfidence;
      ar & utmEastConfidence;
      ar & utmAltitudeConfidence;  
      ar & rollConfidence;	
      ar & pitchConfidence;	
      ar & yawConfidence;	
    
      ar & velNorthRMS;
      ar & velEastRMS;
      ar & velDownRMS;
    
      ar & errEllipMajor;
      ar & errEllipMinor;
      ar & errEllipAngle;
    }

  
};
//__attribute__((packed)) VehiclePrecisionPlus; // this gives problem to Boost


#endif

