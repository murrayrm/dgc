/*!
 * \file ActuatorState.h
 * \brief Actuator state interface
 *
 * \author Tully Foote
 * \date 2004-05
 *
 * This interface returns the current status of all vehicle actuators
 * (including the engine).  It is primarily used by adrive and asim to
 * communicate the state of the actuators that they keep track
 * of/emulate.
 */

#ifndef ACTUATORSTATE_HH
#define ACTUATORSTATE_HH

/*! Define estop status outputs */
enum EstopStatus { 
  EstopDisable = 0, 			/*!< Disable vehicle operations  */
  EstopPause = 1, 			/*!< Bring vehicle to a smooth stop  */
  EstopRun = 2 				/*!< Autonomous operation allowed  */
};

/*! Define some standard actuation status types */
enum ActuatorStatus { ActuatorOff = 0, ActuatorOn = 1 };

typedef struct {
  // These are all in the [0,1] range
  //Steer
  int m_steerstatus; // 0 = off, 1 = on 
  double m_steerpos; // -1 to 1, left to right
  double m_steercmd;  //dito
  unsigned long long m_steer_update_time; // When the steering status was last updated

  //Gas
  int m_gasstatus; // 0 = off, 1 = on 
  double m_gaspos;
  double m_gascmd;  
  unsigned long long m_gas_update_time; // When the gas status was last updated

  //Brake
  int m_brakestatus; // 0 = off, 1 = on 
  double m_brakepos; // 0 to 1
  double m_brakecmd;  // 0 to 1
  double m_brakepressure; // 0 to 1
  unsigned long long m_brake_update_time; // When the brake status was last updated

  //Estop
  int m_estopstatus; // 0 = off, 1 = on 

  // Something is pausing us.  
  // Poll this if you don't care why we're stopped
  int m_estoppos; // 0 or 1 or 2
  // Darpa Pause
  int m_dstoppos; // 0 or 1
  // Adrive error pause
  int m_astoppos; // 0 or 1
  // Controled Pause from software
  int m_cstoppos; // 0 or 1
  unsigned long long m_estop_update_time; // When the estop status was last updated
  // Warning trying to return from pause
  int m_about_to_unpause;

  //Transmission
  int m_transstatus; // 0 = off, 1 = on 
  int m_transcmd; // 0 to 1
  int m_transpos; // 0 to 1
  unsigned long long m_trans_update_time; // When the transmission status was last updated

  //OBDII
  int m_obdiistatus; // 0 = off, 1 = on 
  /*! The RPM of the engine */
  double m_engineRPM;
  /*! The time since the engine started in seconds */
  int m_TimeSinceEngineStart;
  /*! Vehicle speed m/s */
  double m_VehicleWheelSpeed;
  /*! The temperature of the engine coolant. */

  double m_EngineCoolantTemp;
  /*! The wheel force in Newtons */
  double m_WheelForce;

  /*! The time the glow plug has been on??? in seconds */
  int m_GlowPlugLampTime;
  /*! The position of the accelerator petal UNITS?? */
  double m_ThrottlePosition;
  /*! The current gear ratio */
  double m_CurrentGearRatio;
  unsigned long long m_obdii_update_time; // When the obdii status was last updated

} ActuatorState;


#endif
