
/* Desc: Low-level logging interface for sensnet messages
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include "SensnetLog.h"


// Current log version
#define SENSNET_LOG_VERSION 0x0010


// Logging context.  
struct _SensnetLog
{
  // Log directory
  char logname[1024];

  // Index file
  FILE *indexFile;

  // Data file
  FILE *dataFile;

  // Current file count and offset within file
  int fileno, offset;
};


// Index entry.  This is written directly to the index file, so all
// types must be size-defined.
typedef struct
{
  uint32_t type;  
  uint64_t timestamp;
  uint32_t fileno, offset, len;
  
} __attribute__((packed))  SensnetLogIndex;


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Allocate object
SensnetLog *SensnetLogAlloc()
{
  SensnetLog *self;

  self = calloc(1, sizeof(SensnetLog));

  return self;
}


// Free object
void SensnetLogFree(SensnetLog *self)
{
  free(self);

  return;
}


// Open file for writing
int SensnetLogOpenWrite(SensnetLog *self, const char *logname, SensnetLogHeader *header)
{
  char filename[1024];

  // Copy the name
  strncpy(self->logname, logname, sizeof(self->logname) - 1);
  
  // Create directory
  if (mkdir(self->logname, 0x1ED) != 0) 
    return ERROR("unable to create: %s %s", self->logname, strerror(errno));

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.bin", self->logname);  
  self->indexFile = fopen(filename, "w");
  if (!self->indexFile)
    return ERROR("unable to open index file: %s %s", filename, strerror(errno));

  // Overwrite version
  header->version = SENSNET_LOG_VERSION;

  // Write the header
  if (fwrite(header, sizeof(*header), 1, self->indexFile) < 0)
    return ERROR("unable to write header");
      
  return 0;
}


// Open file for reading
int SensnetLogOpenRead(SensnetLog *self, const char *logname, SensnetLogHeader *header)
{
  char filename[1024];

  // Copy the name
  strncpy(self->logname, logname, sizeof(self->logname) - 1);

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.bin", self->logname);  
  self->indexFile = fopen(filename, "r");
  if (!self->indexFile)
    return ERROR("unable to open index file: %s %s", filename, strerror(errno));

  // Read the header
  if (fread(header, sizeof(*header), 1, self->indexFile) < 0)
    return ERROR("unable to read header");
  
  // Check the version
  if (header->version != SENSNET_LOG_VERSION)
    return ERROR("version mismatch: log = %X code = %X", header->version, SENSNET_LOG_VERSION);
  

  return 0;
}


// Close the log
int SensnetLogClose(SensnetLog *self)
{
  if (self->dataFile)
    fclose(self->dataFile);
  if (self->indexFile)
    fclose(self->indexFile);
  
  return 0;
}


// Write blob to the log
int SensnetLogWrite(SensnetLog *self, int type, uint64_t timestamp, int len, const void *blob)
{
  char filename[1024];
  SensnetLogIndex index;

  // See if there is room in the current data file.
  // If not, open a new one.
  if (self->offset + len > 0x40000000)
  {
    self->fileno += 1;
    self->offset -= 0x40000000;
    fclose(self->dataFile);
    self->dataFile = NULL;
  }

  // Create data file as needed
  if (!self->dataFile)
  {
    snprintf(filename, sizeof(filename), "%s/data-%02d.bin", self->logname, self->fileno);  
    self->dataFile = fopen(filename, "w");
    if (!self->dataFile)
      return ERROR("unable to open data file: %s %s", filename, strerror(errno));
  }

  // Write blob to data file
  // TODO: use non-buffered IO
  if (fwrite(blob, len, 1, self->dataFile) < 1)
    return ERROR("unable to write data: %s", strerror(errno));

  // Construct entry for index file
  index.type = type;
  index.timestamp = timestamp;
  index.fileno = self->fileno;
  index.offset = self->offset;
  index.len = len;

  // Write index 
  if (fwrite(&index, sizeof(index), 1, self->indexFile) < 1)
    return ERROR("unable to write index: %s", strerror(errno));

  self->offset += len;

  // Flush the data file
  fflush(self->indexFile);
  fflush(self->dataFile);
  fsync(fileno(self->dataFile));

  return 0;
}


// Read a blob from the log
int SensnetLogRead(SensnetLog *self, int *type, uint64_t *timestamp, int len, void *blob)
{
  char filename[1024];
  SensnetLogIndex index;
    
  // Check for eof
  if (feof(self->indexFile))
    return ERROR("end-of-file");

  // Read the index
  if (fread(&index, sizeof(index), 1, self->indexFile) < 1)
    return ERROR("unable to read index: %s", strerror(errno));

  // Make sure the lengths match
  if (index.len != len)
    return ERROR("mismatched lengths: file has %d, call has %d", index.len, len);
  
  // See if we are still using the current data file
  if (index.fileno != self->fileno)
  {
    fclose(self->dataFile);
    self->dataFile = NULL;
  }

  self->fileno = index.fileno;  
  self->offset = index.offset;
    
  // Open data file
  if (!self->dataFile)
  {
    snprintf(filename, sizeof(filename), "%s/data-%02d.bin", self->logname, self->fileno);  
    self->dataFile = fopen(filename, "r");
    if (!self->dataFile)
      return ERROR("unable to open data file: %s %s", filename, strerror(errno));
  }

  // See to right place in data file
  if (fseek(self->dataFile, self->offset, SEEK_SET) != 0)
    return ERROR("unable to seek to byte %d: %s", self->offset, strerror(errno));

  // Read the data blob
  if (fread(blob, index.len, 1, self->dataFile) < 1)
    return ERROR("unable to read data: %s", strerror(errno));

  if (type)
    *type = index.type;
  if (timestamp)
    *timestamp = index.timestamp;

  return 0;
}

