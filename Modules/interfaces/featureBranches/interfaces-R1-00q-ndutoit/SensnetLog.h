
/* Desc: Low-level logging interface for sensnet messages
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */

#ifndef SENSNET_LOG_H
#define SENSNET_LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>

  
/// @brief Header data for sensnet log.
typedef struct
{
  /// @internal Log file version number
  uint32_t version;
  
  /// Unix time for file generation (seconds since the Epoch)
  uint32_t timestamp;

  // Reserved
  uint32_t reserved[30];
  
} __attribute__((packed)) SensnetLogHeader;

  
/// @brief Sensnet log context (opaque).
typedef struct _SensnetLog SensnetLog;

  
/// @brief Allocate object
///
/// @return Returns a new log handle.
SensnetLog *SensnetLogAlloc();

/// @brief Free object
///
/// @param[in] self Log handle.
void SensnetLogFree(SensnetLog *self);

/// @brief Open log for writing
///
/// @param[in] self Log handle.
/// @param[in] logname Path and directory name for the new log.
/// @returns Returns 0 on success, non-zero on error.    
int SensnetLogOpenWrite(SensnetLog *self, const char *logname, SensnetLogHeader *header);

/// @brief Open log for reading
///
/// @param[in] self Log handle.
/// @param[in] logname Path and directory name for an existing log.
/// @returns Returns 0 on success, non-zero on error.  
int SensnetLogOpenRead(SensnetLog *self, const char *logname, SensnetLogHeader *header);

/// @brief Close the log
///  
/// @param[in] self Log handle.
int SensnetLogClose(SensnetLog *self);

/// @brief Seek to a particular time in the log.
///
/// @param[in] self Log handle.
/// @param[in] timestamp Blob timestamp (microseconds since epoch).
/// @returns Returns 0 on success, non-zero on error.  
int SensnetLogSeek(SensnetLog *self, uint64_t timestamp);
  
/// @brief Write a blob to the log
///
/// @param[in] self Log handle.
/// @param[in] Blob type.
/// @param[in] timestamp Blob timestamp (microseconds since epoch).
/// @param[in] len Blob length.
/// @param[in] blob Blob data.
/// @returns Returns 0 on success, non-zero on error.  
int SensnetLogWrite(SensnetLog *self, int type, uint64_t timestamp, int len, const void *blob);

/// @brief Read an image from the log
///
/// @param[in] self Log handle.
/// @param[out] Blob type.
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[in] len Blob length.
/// @param[out] blob Blob data.
/// @returns Returns 0 on success, non-zero on error.  
int SensnetLogRead(SensnetLog *self, int *type, uint64_t *timestamp, int len, void *blob);


#ifdef __cplusplus
}
#endif

#endif
