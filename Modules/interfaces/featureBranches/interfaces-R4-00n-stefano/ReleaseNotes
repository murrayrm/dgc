              Release Notes for "interfaces" module

Release R4-00n (Sun Mar 25 23:32:29 2007):
  Moved the state precision information to a C++ header file (StatePrecision.hh) as requested to avoid breaking older modules.
  
Release R4-00m (Sun Mar 25 18:45:06 2007):
  Had to comment out the conditional compilation for the C++ code as this 
  now conflicts with the C++ compiler.  Suggest the entire offending block
  be removed.

Release R4-00l (Sat Mar 24 17:03:36 2007):
  Fixed the syntax (this is a C file, not a C++ file).  The serialization
  code is still there, but conditionally compiles for C++ only.  Should
  probably be removed.

Release R4-00k (Fri Mar 23 17:47:29 2007):
   Added skynet type for state precision message. Added serialization 
   of VehiclePrecisionPlus struct.

Release R4-00j (Wed Mar 21 16:40:47 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R4-00i (Wed Mar 21 16:17:40 2007):
        Added another struct in VehicleState.h that contains info about the estimation precision. The precision information are asynchronous wrt the estimation, hence it is pointless to include them in the same struct. Hence I did not change the estimator interface.

Release R4-00h (Mon Mar 19 21:18:01 2007):
  Minor cleanup of bogus dependencies, plus adjustment of the ladar 
  and stereo blobs to permit high-speed logging.  Some API changes
  in both blobs. 

Release R4-00g (Sun Mar 18  8:42:46 2007):
	Minor update in preparation for field test.  StereoImageBlob.h
	needed to include vec3.h and pose3.h from the frames module. 

Release R4-00f (Fri Mar 16 13:06:15 2007):
	Added GcModuleInterfaces that is independent of gcmodule and
	contains definitions needed by SegGoals and Status." 

Release R4-00e (Fri Mar 16 10:37:56 2007):
	Added functions to get the inverse transformations from local ->
	vehicle -> sensor -> image frames in StereoImageBlob.h


Release R4-00d (Fri Mar 16  7:56:26 2007):
	Fixed the serialize() function in SegGoals.

Release R4-00c (Thu Mar 15 13:12:27 2007):
        Added the header file that provide the interface between tplanner
        and dplanner	 

Release R4-00b (Thu Mar 15 10:16:44 2007):
	Fix for StereoImageBlob accessor functions.

Release R4-00a (Mon Mar 12 22:58:01 2007):
	<* Pleaase insert a release notes entry here. If none, then please
	delete these lines *> 

Release R3-00d (Mon Mar 12  3:59:39 2007):
	reverting order of sensors in the enum so old logs will still work

Release R3-00c (Sun Mar 11 12:11:01 2007):
	Updated MapElementMsg.h to include plot_color and plot_value fields

Release R3-00b (Fri Mar  9 19:31:14 2007):
	Added message types SNocpObstacles and SNocpParams to sn_types.h

Release R3-00a (Fri Mar  9 16:48:11 2007):
	"Moved SegmentTypes into SegGoals struct to avoid conflict with some definition in mplanner. Added ReasonForFailure to SegGoalsStatus."

Release R2-00o (Fri Mar  9 16:38:01 2007):
	Minor changes to ObsMapMsg.h; I removed unnecessary header files and
	cmap row/column definitions.

Release R2-00n (Fri Mar  9 12:21:02 2007):
	Moved MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  
	MapElementMsg still lives in interfaces.  MapElementTalker has also
	been moved from skynettalker to map.  All users of the
	MapElementTalker will need to include map/MapElementTalker.hh and
	map/MapElement.hh instead of skynettalker/MapElementTalker and
	interfaces/MapElement.hh.

Release R2-00m (Tue Mar  6 23:34:06 2007):
  Support for long-range stereo.

Release R2-00l (Sat Mar  3 15:02:33 2007):
  Minor tweak.

Release R2-00k (Sat Mar  3 14:04:19 2007):
	Minor update to Makefile allow compilation of rddfPlanner in trunk

Release R2-00j (Sat Mar  3 13:52:13 2007):
Figured out how to send deltas using the CMapDeltaTalker class; this makes the CostMapMsg interface obsolete. These 
changes reflect that. 

Release R2-00i (Sat Mar  3 11:58:26 2007):
  Change the stereo blob structure to make it more future proof.  Old stereo
  log files will need to be converted to the new format.

Release R2-00g (Fri Mar  2 11:24:45 2007):
	added the CostMapMsg.h header file; this defines the message 
interface that tplanner will use to send map deltas to dplanner; 
originally cmap class was supposed to send deltas across the network and 
apply them but because of unsuccessful attempts to get the 
send/receive-deltas functions working, we've (temporarily) resorted to 
defining our own delta-message which this file is for; if in the event 
that jeremy gillula (or someone else) can help us figure out how to 
correctly use the send/receive deltas functions, this header file will not be 
necessary and can be removed. 

Release R2-00f (Thu Mar  1 19:32:06 2007):
	updated MapElement.hh to work with new frames point2 functions.

Release R2-00e (Thu Feb 22 10:56:18 2007):
  Added another stereo module id.

Release R2-00d (Wed Feb 21 23:39:10 2007):
  Added ObsMapMsg.hh for obstacle map deltas.

Release R2-00c (Wed Feb 21 19:53:54 2007):
Updated MapElement functionality.  

Release R2-00b (Wed Feb 21 13:08:14 2007):
	Added tplanner-dplanner interfaces to sn_types. Modified Status.hh to fix the conflicting problems.

Release R2-00a (Tue Feb 20 15:36:03 2007):
	Moved all the talkers (including StateClient) which made the interfaces module depend on the skynet module to the skynettalker module. Also 
added sn_types and enumstring (moved from the skynet module).

Release R1-00u (Sun Feb 18 21:28:34 2007):
	This release fixes a minor namespace issue in MapElement.hh

Release R1-00t (Thu Feb  8 19:28:10 2007):
	Added MapElement class definition in MapElement.hh which describes
	a single element of the map.   MapElementMsg.h defines a constant sized 
	struct used by the talker and skynet and shouldn't be used directly.   
	MapElementTalker defines a class inherited by other modules to implement 
	sending and/or receiving of MapElements. 
	testSendMapElement and testRecvMapElement show the simplest usage 
	of MapElementTalker.

Release R1-00s (Sun Feb  4 17:35:49 2007):
	Removed duplicated files.

Release R1-00r (Sat Feb  3 22:28:39 2007):
	Performance tweaks for sensnet data logger.

Release R1-00q (Fri Feb  2 23:52:34 2007):
	Added support for universal sensnet data logger.

Release R1-00p (Thu Feb  1 23:10:05 2007):
	Some fixes to Makefile.yam, mainly re-added SensnetTypes.h,
	StereoImageBlob.h, LadarRangeBlob.h, RoadLineMsg.h to the list
	of files to link in include/interfaces.

Release R1-00o (Thu Feb  1 11:37:37 2007):
	Added ifndef statement to ActuatorState to fix the conflicting 
declaration problems in trajfollower.

Release R1-00n (Thu Feb  1  2:22:39 2007):
	Added VehicleTrajectory needed by rddfplanner and trajfollower

Release R1-00m (Wed Jan 31 23:34:44 2007):
	Small changed to StereoRangeBlob.

Release R1-00l (Tue Jan 30  0:04:04 2007):
	Added RoadLine message.

Release R1-00k (Mon Jan 29 22:19:41 2007):
	StereoImgaeBlob.h and SensnetTypes.h disappeared again.  Adding 
	them and LadarRangeBlob.h back in.

Release R1-00j (Mon Jan 29 18:30:29 2007):
        Attempted to re-merge VehicleState changes.

Release R1-00i (Mon Jan 29  8:44:44 2007):
	Re-merged Andrew's changes.  This version has StereoImageBlog.h
	and SensnetTypes.h.  Also fixed a problem in Makefile.yam that
	was causing a bad link (which got in the way of proper releases)

Release R1-00h (Sun Jan 28 18:27:41 2007):
	Fixed bug in RDDFTalker by adding include <strstream>

Release R1-00g (Sun Jan 28 18:01:36 2007):
	Added Sensnet messages (from earlier branch).
	
Release R1-00f (Sat Jan 27 18:03:08 2007):
	Added RDDFTalker to libinterfaces.

Release R1-00e (Wed Jan 24 23:44:50 2007):
	Changed the Makefile so it only generates libinterfaces which 
also contains all the talkers.

Release R1-00d (Wed Jan 24 22:49:13 2007):
	Added interfaces between mplanner and tplanner to libinterfaces.
	Also created libtalker which currently contains SegGoalsTalker.

Release R1-00c (Wed Jan 24 22:05:52 2007):
	Created libinterfaces, with StateClient class as an entry.  Turning
	over control to Nok.

Release R1-00b (Sat Jan 13 20:41:29 2007):
	Added interfaces for vehicle state (VehicleState.hh), actuator state
	(ActuatorState.hh) and actuator commands (ActuatorCommand.hh).  All
	of these interfaces are compatible with the current interfaces used
	under dgc/trunk => you can interoperate with old code (consistent
	across R1-* releasese).

	Also copied over the SkynetContainer class definition so that
	we don't have to update everything in moving to YaM.  The 'asim'
	module uses this class, so it has been tested and working.  Note
	that SkynetContainer is just a shell around the skynet class.  As
	we upgrade Skynet, we can update this container class to make
	the changes transparent.

	Note that interfaces/adrive.h is not interfaces/ActuatorState.hh
	and interfaces/ActuatorCommand.hh.  To access the current state,
	you can use interfaces/VehicleState.hh directly or the (legacy)
	CSkynetContainer class.

Release R1-00a (Wed Jan 10 22:56:05 2007):
	Initial release.  Currently only contains the adrive interface
	definition, which is basically the structure definitions for the 
	data that is sent in adrive command and status packets.  This 
	change is essentially a move of some data from adrive to interfaces.
	If this works right, we should eventually be able to compile the
	simulator (asim) without having to compile the serial library...

	No talkers at this point (adrive sends the structure directly,
	without	any interverning code or classes).

Release R1-00 (Wed Jan 10 16:32:25 2007):
	Created.























































