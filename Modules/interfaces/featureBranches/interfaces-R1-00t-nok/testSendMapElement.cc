/**********************************************************
 **
 **  TESTMAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-02-08 19:03:32 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "interfaces/MapElementTalker.hh"
#include "interfaces/MapElement.hh"
#include "skynet/sn_types.h"


using namespace std;


class CTestSendMapElement : public CMapElementTalker{

public:
	/*! Constructor */
  CTestSendMapElement(int skynetKey)
		: CSkynetContainer(MODmapping,skynetKey)
	{}
      
  /*! Standard destructor */
  ~CTestSendMapElement() {}

};

int main(int argc, char **argv)
{
	int skynetKey = 0;
	int id = 1234;

	skynetKey = atoi(getenv("SKYNET_KEY"));	

	if(argc >1)
		id = atoi(argv[1]);
	if(argc >2)
		skynetKey = atoi(argv[2]);



	int bytesSent = 0;


	CTestSendMapElement testtalker(skynetKey);
	testtalker.initSendMapElement();
	MapElement obj;

	obj.id = id;

	bytesSent = testtalker.sendMapElement(&obj);

	cout << "bytes sent = " << bytesSent << endl;
	cout  << "object id = " << obj.id << endl;
	return(0);
}

