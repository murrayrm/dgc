/**********************************************************
 **
 **  TESTMAPELEMENTLISTENER.CC
 **
 **    Time-stamp: <2007-02-08 19:03:43 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "interfaces/MapElementTalker.hh"
#include "interfaces/MapElement.hh"
#include "skynet/sn_types.h"

using namespace std;


class CTestRecvMapElement : public CMapElementTalker{

public:
	/*! Constructor */
  CTestRecvMapElement(int skynetKey)
		: CSkynetContainer(MODmapping,skynetKey)
	{}
      
  /*! Standard destructor */
  ~CTestRecvMapElement() {}

};

int main(int argc, char **argv)
{

	int skynetKey = 0;

	skynetKey = atoi(getenv("SKYNET_KEY"));	

	if(argc >1)
		skynetKey = atoi(argv[1]);
		
	int bytesRecv = 0;



	CTestRecvMapElement testtalker(skynetKey);
	testtalker.initRecvMapElement();
	MapElement obj;
	
	cout << "Testing blocking read " << endl;


	bytesRecv = testtalker.recvMapElementBlock(&obj);
	cout << "bytes received = " << bytesRecv << endl;
	cout << "object id = " << obj.id << endl;
 



	while(1){
	
		bytesRecv = testtalker.recvMapElementNoBlock(&obj);
		if (bytesRecv>0){
			cout << "bytes received = " << bytesRecv << endl;
			cout << "object id = " << obj.id << endl;

		}else{
			cout << "Testing non-blocking read" << endl;
			sleep(1);			
		}
	}
	return(0);
}

