
#ifndef TPDPINTERFACE_HH_
#define TPDPINTERFACE_HH_

#define PARAMS_NUMBER 12
#define COND_NUMBER 6

#define LENGTH_IDX_P 0
#define WEELB_IDX_P 1
#define HCG_IDX_P 2
#define VMIN_IDX_P 3
#define VMAX_IDX_P 4
#define AMIN_IDX_P 5
#define AMAX_IDX_P 6
#define PHIMIN_IDX_P 7
#define PHIMAX_IDX_P 8
#define PHIDMIN_IDX_P 9
#define PHIDMAX_IDX_P 10
#define G_IDX_P 11 
#define EASTING_IDX_C 0
#define NORTHING_IDX_C 1
#define VELOCITY_IDX_C 2
#define HEADING_IDX_C 3
#define ACCELERATION_IDX_C 4
#define STEERING_IDX_C 5

#define MAX_OBSTACLES 5


/*! Structure that defines the dplanner status to be passed upwards to Tplanner */

struct DplannerStatus
{
    bool OCPsolved ;
    int solverFlag ;
    bool corridorGenerated ;
    int corridorFlag ;

    DplannerStatus()
    {
      OCPsolved = false;
      solverFlag = -1;
      corridorGenerated = false;
      corridorFlag = -1;
    }


    template <class Archive>
    void serialize(Archive &ar,const unsigned int version)
    {
        ar & OCPsolved ;   
        ar & solverFlag ;   
        ar & corridorGenerated ;
        ar & corridorFlag ;
    }


};

/* Structure defining the obstacles to be sent from Tplanner to Dplanner */

struct OCPobstacles 
{
    int ObstNumber ;
    double ObstCenter [MAX_OBSTACLES][2];
    double ObstHessian [MAX_OBSTACLES][2][2];

    OCPobstacles()
    {
        ObstNumber=0;
	for(int i=0;i<MAX_OBSTACLES;i++)
	{
            ObstCenter[i][0] = 0;
	    ObstCenter[i][1] = 0;
            ObstHessian[i][0][0] = 0;
            ObstHessian[i][0][1] = 0; 
	    ObstHessian[i][1][1] = 0; 
	    ObstHessian[i][1][0] = 0;

	}

    }

    OCPobstacles(const OCPobstacles& p)
    {
        
        ObstNumber = p.ObstNumber;
	for(int i=0;i<MAX_OBSTACLES;i++)
	{
            ObstCenter[i][0] =  p.ObstCenter[i][0]; 
	    ObstCenter[i][1] =  p.ObstCenter[i][1]; 
            ObstHessian[i][0][0] =  p.ObstHessian[i][0][0]; 
            ObstHessian[i][0][1] =  p.ObstHessian[i][0][1]; 
	    ObstHessian[i][1][1] =  p.ObstHessian[i][1][1]; 
	    ObstHessian[i][1][0] =  p.ObstHessian[i][1][0];

	}

    }


    template <class Archive>
    void serialize(Archive &ar,const unsigned int version)
    {
    
        ar & ObstNumber;
        ar & ObstCenter;
        ar & ObstHessian;
   
    }

};


/*! -  This enum is used to encapsulate the valid flags that specify the system mode.*/
enum Mode{md_FWD, md_REV};

/*! Structure defining the OCP params, the initial and final condition to be passed from Tplanner to Dplanner */

struct OCPparams
{
    int mode;
    double parameters [PARAMS_NUMBER];
    double initialConditionLB [COND_NUMBER];
    double initialConditionUB [COND_NUMBER];
    
    /*
    same structure as finalCondition
    */
       
    double finalConditionLB [COND_NUMBER];
    double finalConditionUB [COND_NUMBER];
 /*
     finalCondition[0] = Northing_min
     finalCondition[1] = Easting_min
     finalCondition[2]  = Heading_min
     finalCondition[3]  = Velocity_min (norm)
     finalCondition[4]  = Acceleration_min (norm)
     finalCondition[5]  = SteeringAngle_min 
      */
  
    OCPparams()
    {
    	//this is the fastest way to set everything to 0
        memset(parameters,'\0',PARAMS_NUMBER*sizeof(double));    
        memset(initialConditionLB,'\0',COND_NUMBER*sizeof(double));    
        memset(finalConditionLB,'\0',COND_NUMBER*sizeof(double));    
        memset(initialConditionUB,'\0',COND_NUMBER*sizeof(double));    
        memset(finalConditionUB,'\0',COND_NUMBER*sizeof(double));    
        //by default we drive fwd
        mode = md_FWD ;
        
        //setting parasmeters to the default values defined For Alice dynamics in dplanner
	    parameters[0]  = 5.43560 ;         // L
	    parameters[1]  = 2.13360 ;         // W
	    parameters[2]  = 1.06680 ;         // hcg
	    parameters[3]  = 0.00000 ;         // v_min
	    parameters[4]  = 1.40800 ;         // v_max
	    parameters[5]  =-3.00000 ;         // a_min
	    parameters[6]  = 0.98100 ;         // a_max
	    parameters[7]  =-0.44942 ;         // phi_min
	    parameters[8]  = 0.44942 ;         // phi_max
	    parameters[9]  =-1.30900 ;         // phid_min
	    parameters[10] = 1.30900 ;         // phid_max
	    parameters[11] = 9.81000 ;         // g
	    
    }
    OCPparams(const OCPparams& p)
    {
        for(int i=0;i<COND_NUMBER;i++)
        {
            initialConditionLB[i]=p.initialConditionLB[i];
            initialConditionUB[i]=p.initialConditionUB[i];
            finalConditionLB[i]=p.finalConditionLB[i];
            finalConditionUB[i]=p.finalConditionUB[i];
        }
         for(int i=0;i<PARAMS_NUMBER;i++)
        {
            parameters[i]=p.parameters[i];
        }
       mode = p.mode; 

    }
    template <class Archive>
    void serialize(Archive &ar,const unsigned int version)
    {
    
    ar & mode;
    ar & parameters;
    ar & initialConditionLB;
    ar & initialConditionUB;
    
    /*
    same structure as finalCondition
    */
       
    ar & finalConditionLB;
    ar & finalConditionUB;


    }
};

#endif
