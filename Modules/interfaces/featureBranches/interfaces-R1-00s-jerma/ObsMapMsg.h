/* 
 * Desc: Obstacle Map message type.
 * Date: 09 February 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/


#ifndef OBSMAP_MSG_H
#define OBSMAP_MSG_H

#include <stdint.h>
#include "VehicleState.h"
#include <frames/coords.hh>

// TODO: Debatable whether this needs to be here.  Why would we send
// cells that are empty or outside the map?
// -- may need to know which cells are empty or outside the map
//    simply for visualization purposes

enum CELL_TYPE{
  OUTSIDE_MAP = 0,
  EMPTY = 1,
  DATA = 2
};

struct ElevationData
{
  double meanElevation;
  double stdDev;
  NEDcoord UTMPoint;
  CELL_TYPE cellType; 
};

/// @brief Skynet message with obstacle map data.
typedef struct
{
  /// Skynet message type (must be SNfusionmapdelta)
  int msg_type;

  /// Vehicle state data
  VehicleState state;

  int numDeltas;

  // vector of map deltas
  ElevationData mapDeltaVec[500];
  
} __attribute__((packed)) ObsMapMsg;

#endif
