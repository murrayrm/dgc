/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef SEGGOALS_HH_
#define SEGGOALS_HH_

#include <iostream>
#include <string>
#include "interfaces/GcModuleInterfaces.hh"
using std::cout;
using std::stringstream;
using std::string;


struct SegGoals : public ITransmissive
{
  enum SegmentType{ ROAD_SEGMENT, PARKING_ZONE, INTERSECTION, PREZONE, UTURN, PAUSE,
		    END_OF_MISSION, UNKNOWN};
  SegGoals()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  void print()
  { 
    stringstream strout("");
    print( strout );
    cout << strout.str();
  }

  string toString() const 
  {
    stringstream strout("");
    print(strout);
    return strout.str();
  }

  void print(stringstream& strout) const
  {
    strout << "GOAL " << goalID << ":\t" << entrySegmentID << "." << entryLaneID << "."
         << entryWaypointID;
    strout << "\t -> \t";
    strout << exitSegmentID << "." << exitLaneID << "." << exitWaypointID;
    strout << "\t\t";
    switch(segment_type)
    {
    case ROAD_SEGMENT:
      strout << "ROAD_SEGMENT";
      break;
    case PARKING_ZONE:
      strout << "PARKING_ZONE";
      break;
    case INTERSECTION:
      strout << "INTERSECTION";
      break;
    case PREZONE:
      strout << "PREZONE";
      break;
    case UTURN:
      strout << "UTURN";
      break;
    case PAUSE:
      strout << "PAUSE";
      break;
    case END_OF_MISSION:
      strout << "END_OF_MISSION";
      break;
    default:
      strout << "UNKNOWN";
    }
    strout << "\tMin Speed: " << minSpeedLimit << " Max Speed: " << maxSpeedLimit;
    if (illegalPassingAllowed)
      strout << "\tIllegal passing allowed";
  }

  unsigned getDirectiveId()
  {
    return (unsigned) goalID;
  }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & segment_type;
    ar & goalID;
    ar & entrySegmentID;
    ar & entryLaneID;
    ar & entryWaypointID;
    ar & exitSegmentID;
    ar & exitLaneID;
    ar & exitWaypointID;
    ar & minSpeedLimit;
    ar & maxSpeedLimit;
    ar & illegalPassingAllowed;
    ar & stopAtExit;
    ar & isExitCheckpoint;
    ar & perf_level;
  }

  SegmentType segment_type;
  int goalID;
  int entrySegmentID;
  int entryLaneID;
  int entryWaypointID;
  int exitSegmentID;
  int exitLaneID;
  int exitWaypointID;
  double minSpeedLimit;
  double maxSpeedLimit;
  bool illegalPassingAllowed;
  bool stopAtExit;
  bool isExitCheckpoint;
  int perf_level;
};

#endif //SEGGOALS_HH_
