#include <iostream>
#include <limits>
#include <cmath>

#include "Polygon.hh"

// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

namespace bitmap
{
    using namespace std;

    void Polygon::setVertices(const vector<point2>& v, const vector<float>& cost)
    {
        assert(cost.size() == v.size());
        m_xv.resize(v.size());
        m_yv.resize(v.size());
        m_costVec.resize(cost.size());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            m_xv[i] = v[i].x;
            m_yv[i] = v[i].y;
            m_costVec[i] = cost[i];
        }
    }

    // the code is exactly the same as for point2
    void Polygon::setVertices(const vector<point2_uncertain>& v, const vector<float>& cost)
    {
        assert(cost.size() == v.size());
        m_xv.resize(v.size());
        m_yv.resize(v.size());
        m_costVec.resize(cost.size());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            m_xv[i] = v[i].x;
            m_yv[i] = v[i].y;
            m_costVec[i] = cost[i];
        }
    }

    void combineReplace(cost_t* dest, cost_t* src)
    {
        *dest = *src;
    }

    void combineMax(cost_t* dest, cost_t* src)
    {
        *dest = max(*dest, *src);
    }

    void combineMin(cost_t* dest, cost_t* src)
    {
        *dest = min(*dest, *src);
    }

    void combineAdd(cost_t* dest, cost_t* src)
    {
        *dest += *src;
    }

    void combineSub(cost_t* dest, cost_t* src)
    {
        *dest -= *src;
    }
    
    void combineMul(cost_t* dest, cost_t* src)
    {
        *dest *= *src;
    }

    void combineDiv(cost_t* dest, cost_t* src)
    {
        *dest /= *src; // if *src == 0, you'll get +inf, if this is fine, you decide!
    }

    void combineAvg(cost_t* dest, cost_t* src)
    {
        *dest = (*dest + *src)/2;
    }


    combine_f combineTable[] =
    {
        combineReplace,
        combineMax,
        combineMin,
        combineAdd,
        combineSub,
        combineMul,
        combineDiv,
        combineAvg,
        NULL /* just in case, used as a terminator */
    };

    cost_t fillLinear(cost_t x, cost_t a)
    {
        return a * x;
    }

    cost_t fillSquare(cost_t x, cost_t a)
    {
        return a * x*x;
    }

    cost_t fillExp(cost_t x, cost_t a)
    {
        return exp(a * x);
    }

    cost_t fillExp2(cost_t x, cost_t a)
    {
        return exp(a * x*x);
    }

    cost_t fillCos(cost_t x, cost_t a)
    {
        return cos(a * x);
    }

    cost_t fillSin(cost_t x, cost_t a)
    {
        return sin(a * x);
    }

    cost_t fillTan(cost_t x, cost_t a)
    {
        return tan(a * x);
    }

    cost_t fillAtan(cost_t x, cost_t a)
    {
        return atan(a * x);
    }

    fill_f fillTable[] =
    {
        NULL, /* FILL_NONE */
        fillLinear,
        fillSquare,
        fillExp,
        fillExp2,
        fillCos,
        fillSin,
        fillTan,
        fillAtan,
        NULL
    };
    
    /* Ok guys, this is some very old code ('99 I think) that used to run under dos
     * in the times when there were no accelerated graphic cards, to draw shaded
     * polygons on the screen.
     * I've completely rewritten the draw_polygon function, so it should be easily readable
     * and debuggable, but the draw_scanline function is still the same (is a pretty simple
     * function anyway). I'll need to change that soon anyway.
     * -- Daniele Tamino
     */

#if 0
    float clip_line_y(float max, float min, float *x1, float *y1, float *x2, float *y2)
    {
        float dist = 0;
        if(*y1 == *y2) {
            if (*y1 > max || *y1 < min)
                return -1;
            else
                return 0;
        }
        // if the verticies are in the wrong order, exchange them
        if(*y1 > *y2) {
            swap(y1, y2);
            swap(x1, x2);
        }
        // completely out of bounds
        if(*y2<=min) { *y1 = *y2 = min; return -1; }
        if(*y1>=max) { *y1 = *y2 = max+1; return -1; }

        /* (x2 - x1)*(y - y1) = (x - x1)*(y2 - y1) */

        /* intersection with y = min */
        if(*y1 < min) {
            /* (x2 - x1)*(min - y1) = (x - x1)*(y2 - y1) */
            /* (y2 - y1)*x = x1*(y2 - min) + x2*(min - y1) */
            float x = ((*x1)*((*y2) - min) + (*x2)*(min - (*y1))) / ((*y2) - (*y1));
            *x1 = x;
            *y1 = min;
        }
        /* intersection con y = max */
        if(*y2 > max) {
            /* (x2 - x1)*(max - y1) = (x - x1)*(y2 - y1) */
            /* (y2 - y1)*x = x1*(y2 - min) + x2*(min - y1) */
            float x = ((*x1)*((*y2) - max) + (*x2)*(max - (*y1))) / ((*y2) - (*y1));
            dist = (*y2)-max;
            *x2 = x;
            *y2 = max;
        }
        return dist;
    }
#endif

    /** Draws a scanline (horizontal straight line). The length of the scanline once
     * drawn will be (x2 - x1) cells, including the starting poing and excluding the
     * ending one. If x1 > x2, the endpoints will be exchanged.
     * @param map CostMap where to draw the scanline
     * @param x1 starting point on the row. It's NOT required that x1 < x2.
     * @param x2 ending point on the row.
     * @param val1 Desired value of cell at x1.
     * @param val2 Desired value of cell at x2.
     * @param row The row where all the drawing should happen.
     */
    static void draw_scanline(CostMap* map, int x1, int x2,
                              cost_t val1, cost_t val2, int row)
    {
        cost_t curVal;
        cost_t incVal;
  
        // we want x2 > x1
        if (x2 < x1) {
            swap(x2, x1);
            swap(val1, val2);
        }
        if (x1 > (map->getWidth() - 1) || x2 < 0) // completely out of bounds
            return;
        //if (x2 == x1)
        //    return;

        curVal = val1;
        incVal = (val2 - val1) / (x2 - x1);

        if (x1 < 0) {
            curVal += incVal*(-x1);
            x1 = 0;
        }
        if (x2 > (map->getWidth() - 1))
            x2 = map->getWidth() - 1;
        //if(x2 == x1)
        //    return;

        cost_t* adr = map->getRow(row) + x1;

        for(int x = x1; x <= x2; x++) {
            *adr = curVal;
            adr++;
            curVal += incVal;
        }
    }

    struct PolyEdge
    {
        float x;
        float incX;
        float c;
        float incC;
        float rowEnd;

        /// @param x1 x coordinate of first point (column)
        /// @param x2 x coordinate of second point (column)
        /// @param y1 y coordinate of first point (row)
        /// @param y2 y coordinate of secons point (row). Must be y2 < y1
        /// @param c1 Cost of the first vertex
        /// @param c2 Cost of the second vertex
        /// @param bottom Set to true if this is a bottom edge (i.e. the polygon
        /// lies over this edge).
        PolyEdge(float x1, float x2, float y1, float y2, float c1, float c2,
                 bool bottom)
        {
            //MSG("PolyEdge(" << x1 << ", " << x2 << ", " << y1 << ", " << y2 << ", "
            //    << c1 << ", " << c2 << ", " << bottom << ")");
            float preInc = y1 - int(y1 + 0.5);
            c = c1;
            if (y1 - y2 > 0.001)
            {
                incX = (x2 - x1) / (y1 - y2);
                incC = (c2 - c1) / (y1 - y2);
            }
            else
            {
                incX = 0; // avoid div-by-zero or by a very small number
                incC = 0;
            }

            x = x1 /* preInc * incX */;
            c = c1 /* + preInc * incC */;
            if (bottom) // this is to avoid having disconnected polygons
            {
                x += incX - 1;
                c += incC * (1 - 1/incX);
            }
            rowEnd = int(y2 + 0.5);
            //MSG("PolyEdge: x=" << x << ", incX=" << incX << ", c=" << c
            //    << ", incC=" << incC << ", rowEnd=" << rowEnd);
        }

        void next()
        {
            x += incX;
            c += incC;
        }

        bool done(int row)
        {
            return row < rowEnd;
        }
    };


    static inline int nextVert(int vert, int nVert)
    {
        return (vert + 1 >= nVert) ? 0 : (vert + 1);
    }

    static inline int prevVert(int vert, int nVert)
    {
        return (vert - 1 < 0) ? (nVert - 1) : (vert - 1);
    }

    static void draw_polygon(CostMap* map,
                             const vector<float>& vertX,
                             const vector<float>& vertY,
                             const vector<cost_t>& vertVal)
    {
        int nVert = int(vertX.size());
        assert(int(vertY.size()) == nVert);
        assert(int(vertVal.size()) == nVert);

        //MSG("draw_polygon BEGIN");

        float miny=numeric_limits<float>::max();
        float maxy=numeric_limits<float>::min();

        int bottom = 0;
        int row;

        // search for the vertex with the highes y value
        for (int i = 0; i < nVert; i++) {
            if (vertY[i] > maxy) {
                bottom = i;
                maxy = vertY[i];
            }
            if (vertY[i] < miny) {
                miny = vertY[i];
            }
        }
        if(vertY[bottom] < 0) return; // completely over the top boundary
        if(miny < 0) miny = 0; //bugfix (chance of infinite loop otherwise)

        int left = prevVert(bottom, nVert);
        int right = nextVert(bottom, nVert);
        bool cw = false;
        if (vertX[left]*vertY[right] - vertX[right]*vertY[left] > 0)
        {
            swap(left, right);
            cw = true; // clockwise vertex order
        }

        PolyEdge leftEdge(vertX[bottom],   vertX[left],
                          vertY[bottom],   vertY[left],
                          vertVal[bottom], vertVal[left],
                          vertX[bottom] > vertX[left]);
        PolyEdge rightEdge(vertX[bottom],   vertX[right],
                          vertY[bottom],   vertY[right],
                          vertVal[bottom], vertVal[right],
                          vertX[bottom] < vertX[right]);

        int minRow = int(miny + 0.5);
        for (row = int(vertY[bottom] + 0.5); row >= minRow; row--)
        {
            //MSG("row " << row << ":");
            if (leftEdge.done(row) && row > minRow)
            {
                //MSG("  next left edge");
                int last = left;
                if (cw)
                    left = nextVert(left, nVert); // want them in ccw
                else
                    left = prevVert(left, nVert);

                leftEdge = PolyEdge(vertX[last],   vertX[left],
                                    vertY[last],   vertY[left],
                                    vertVal[last], vertVal[left],
                                    vertX[last] > vertX[left]);
            }
            if (rightEdge.done(row) && row > minRow)
            {
                //MSG("  next right edge");
                int last = right;
                if (cw)
                    right = prevVert(right, nVert); // want them in ccw
                else
                    right = nextVert(right, nVert);

                 rightEdge = PolyEdge(vertX[last],   vertX[right],
                                      vertY[last],   vertY[right],
                                      vertVal[last], vertVal[right],
                                      vertX[last] < vertX[right]);
            }

            if (row < map->getHeight())
            {
                draw_scanline(map, int(leftEdge.x + 0.5), int(rightEdge.x + 0.5),
                              leftEdge.c, rightEdge.c, row);
            }
            leftEdge.next();
            rightEdge.next();
            //MSG("  leftEdge.x = " << leftEdge.x);
            //MSG("  rightEdge.x = " << rightEdge.x);
        }
        //MSG("draw_polygon END");
    }


#if 0
    /**
     * Draw a polygon
     */
    static void draw_polygon_old(CostMap* map,
                             const vector<float>& vertX,
                             const vector<float>& vertY,
                             const vector<cost_t>& vertVal)
    {
        int nVert = int(vertX.size());
        assert(int(vertY.size()) == nVert);
        assert(int(vertVal.size()) == nVert);

        float miny=numeric_limits<float>::max();
        float maxy=numeric_limits<float>::min();
        int cv1=0, cv2=0; // current vertex 1 (left) and 2 (right)
        float dist=0; // used for clipping
        float x, y, x1, y1, x2, y2, xx, yy; // don't ask me what all these means, I don't remember ;-) Daniele
        cost_t c, c1, c2; // costs (once they meant "color" ;-)
        float len1, len2;
        //fixed_t len1, len2;
        int v_prev, v_next; //tmp vars

        float cur_x_1, cur_x_2;
        //fixed_t cur_x_1, cur_x_2;
        cost_t cur_c_1, cur_c_2;
        float inc_x_1, inc_x_2;
        float pinc_x_1, pinc_x_2; // partial increment, to make sure polygon is connected
        //fixed_t inc_x_1, inc_x_2;
        cost_t inc_c_1, inc_c_2;

        // search for the vertex with the highes y value
        for (int i = 0; i < nVert; i++) {
            if (vertY[i] > maxy) {
                cv1 = i;
                maxy = vertY[i];
            }
            if (vertY[i] < miny) {
                miny = vertY[i];
            }
        }
        cv2 = cv1;
        if(vertY[cv1] < 0) return; // completely out of boundary
        if(miny < 0) miny = 0; //bugfix (chance of infinite loop otherwise)

        x = vertX[cv1];
        y = vertY[cv1];
        c = vertVal[cv1];
        xx = x;
        yy = y;
        v_prev = (cv1-1 < 0) ? (nVert - 1) : (cv1 - 1);
        v_next = (cv2 + 1 >= nVert) ? 0 : (cv2 + 1);
        x1 = vertX[v_prev];
        y1 = vertY[v_prev];
        x2 = vertX[v_next];
        y2 = vertY[v_next];
        c1 = vertVal[v_prev];
        c2 = vertVal[v_next];

        len1 = (y - y1 == 0) ? 1 : (y - y1);
        len2 = (y - y2 == 0) ? 1 : (y - y2);

        inc_x_1 = /* Dx / Dy */ (x1 - x) / len1;
        inc_x_2 = /* Dx / Dy */ (x2 - xx) / len2;
        inc_c_1 = /* Dc / Dy */ (c1 - c) / len1;
        inc_c_2 = /* Dc / Dy */ (c2 - c) / len2;

        // cut initial edges
        float clipY = map->getHeight();
        do
        {
            dist = clip_line_y(clipY-1, 0, &x1, &y1, &x, &y);
            if (1/*dist >= 0*/)
            {
                cur_c_1 = c + inc_c_1 * dist;
            }
            else
            {
                // next right edge
                cv1 = (cv1 - 1 < 0) ? (nVert - 1) : (cv1 - 1);
                x = vertX[cv1];
                c = vertVal[cv1];
                v_prev = (cv1 - 1 < 0) ? (nVert - 1) : (cv1 - 1);
                x1 = vertX[v_prev];
                y1 = vertY[v_prev];
                c1 = vertVal[v_prev];
                len1 = (y - y1 == 0) ? 1 : (y - y1);
                inc_x_1 = /* Dx / Dy */ (x1 - x) / len1;
                inc_c_1 = /* Dc / Dy */ (c1 - c) / len1;
            }
        } while (0 /*dist < 0*/);

        do
        {
            dist = clip_line_y(clipY-1, 0, &x2, &y2, &xx, &yy);
            if (1/*dist >= 0*/)
            {
                cur_c_2 = c + inc_c_2 * dist;
            }
            else
            {
                // next left edge
                cv2 = (cv2 + 1 >=nVert) ? 0 : (cv2 + 1);
                xx = vertX[cv2];
                c = vertVal[cv2];
                v_next = (cv2 + 1 >= nVert) ? 0 : (cv2 + 1);
                x2 = vertX[v_next];
                y2 = vertY[v_next];
                c2 = vertVal[v_next];
                len2 = y - y2 == 0 ? 1 : y - y2;
                inc_x_2 = /* Dx / Dy */ (x2 - xx) / len2;
                inc_c_2 = /* Dc / Dy */ (c2 - c) / len2;
            }
        } while(0/*dist  < 0*/);

        cur_x_1 = x;
        cur_x_2 = xx;

        if (inc_x_1 > 1 && cur_x_2 < cur_x_1) {
            pinc_x_1 = inc_x_1 - 1;
        } else if (inc_x_1 < -1 && cur_x_2 > cur_x_1) {
            pinc_x_1 = inc_x_1 + 1;
        } 
        if (inc_x_2 > 1 && cur_x_1 < cur_x_2) {
            pinc_x_2 = inc_x_2 - 1;
        } else if (inc_x_2 < -1 && cur_x_1 > cur_x_2) {
            pinc_x_2 = inc_x_2 + 1;
        } 

        while(y >= miny /* && y < map->getHeight()*/) {
            if(y <= y1) { // new right edge
                cv1 = (cv1 - 1 < 0) ? (nVert - 1) : (cv1 - 1);
                x = vertX[cv1];
                c = vertVal[cv1];
                v_prev = (cv1 - 1 < 0) ? (nVert - 1) : (cv1 - 1);
                x1 = vertX[v_prev];
                y1 = vertY[v_prev];
                c1 = vertVal[v_prev];
                len1 = (y - y1 == 0) ? 1 : (y - y1);
                inc_x_1 = /* Dx / Dy */ (x1 - x) / len1;
                inc_c_1 = /* Dc / Dy */ (c1 - c) / len1;
                cur_c_1 = c;
                cur_x_1 = x;
                pinc_x_1 = 0;
                if (inc_x_1 > 1 && cur_x_2 < cur_x_1) {
                    pinc_x_1 = inc_x_1 - 1;
                } else if (inc_x_1 < -1 && cur_x_2 > cur_x_1) {
                    pinc_x_1 = inc_x_1 + 1;
                } 
            }
            if(y <= y2) { // new left edge
                cv2 = (cv2 + 1 >=nVert) ? 0 : (cv2 + 1);
                xx = vertX[cv2];
                c = vertVal[cv2];
                v_next = (cv2 + 1 >= nVert) ? 0 : (cv2 + 1);
                x2 = vertX[v_next];
                y2 = vertY[v_next];
                c2 = vertVal[v_next];
                len2 = y - y2 == 0 ? 1 : y - y2;
                inc_x_2 = /* Dx / Dy */ (x2 - xx) / len2;
                inc_c_2 = /* Dc / Dy */ (c2 - c) / len2;
                cur_c_2 = c;
                cur_x_2 = xx;
                pinc_x_2 = 0;
                if (inc_x_2 > 1 && cur_x_1 < cur_x_2) {
                    pinc_x_2 = inc_x_2 - 1;
                } else if (inc_x_2 < -1 && cur_x_1 > cur_x_2) {
                    pinc_x_2 = inc_x_2 + 1;
                } 
            }

            if(y < map->getHeight()) {
                draw_scanline(map, int(cur_x_1 + pinc_x_1 + 0.5), int(cur_x_2 + pinc_x_2 + 0.5),
                              cur_c_1, cur_c_2, int(y + 0.5));
                //draw_scanline(map, int(cur_x_1 + 0.5), int(cur_x_2 + 0.5),
                //              cur_c_1, cur_c_2, int(y + 0.5));
            }
            cur_x_1 += inc_x_1;
            cur_x_2 += inc_x_2;
            cur_c_1 += inc_c_1;
            cur_c_2 += inc_c_2;
            y--;
        } //  end of for (each value of y)
    }
#endif

    /**
     * Draw the polygon on the specified cost map, interpreting the vertices
     * as they are in local frame.
     * The polygon will be correctly clipped if it's partly or completely
     * outside of the map.
     */
    void Polygon::draw(CostMap* bmp) const
    {
        unsigned int nVec = m_xv.size();
        vector<float> xVec(nVec);
        vector<float> yVec(nVec);
        for (unsigned int i = 0; i < nVec; i++)
        {
            bmp->toMapFrame(m_xv[i], m_yv[i], &xVec[i], &yVec[i]);
        }
        draw_polygon(bmp, xVec, yVec, m_costVec);
    }

    /**
     * Draw the polygon on the specified cost map, interpreting the vertices
     * as they are in map coordinates already (y = row, x = column).
     * The polygon will be correctly clipped if it's partly or completely
     * outside of the map.
     */
    void Polygon::drawMapCoords(CostMap* bmp) const
    {
        draw_polygon(bmp, m_xv, m_yv, m_costVec);

    }


};
