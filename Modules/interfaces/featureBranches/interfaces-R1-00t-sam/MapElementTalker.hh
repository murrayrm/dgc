/**********************************************************
 **
 **  CMAPELEMENTTALKER.HH
 **
 **    Time-stamp: <2007-02-20 21:09:10 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:05:08 2007
 **
 **
 **********************************************************
 **
 **  Implements a talker for a map element
 **
 **********************************************************/


#ifndef MAPELEMENTTALKER_HH
#define MAPELEMENTTALKER_HH

#include <iostream>
#include "skynet/sn_msg.hh"//"SkynetContainer.hh"
#include "MapElement.hh"
#include "MapElementMsg.h"
using namespace std;
//! Implements a talker for a map element
class CMapElementTalker
{
	
public:
	
	//! A Constructor 
	CMapElementTalker();
	
	//! A Destructor 
	~CMapElementTalker() {}

	int initSendMapElement(int snkey, modulename snname = MODmapping);
	int initRecvMapElement(int snkey, modulename snname = MODmapping);

	int sendMapElement(MapElement* pMapElement, int subgroup = 0);
		
	int recvMapElementBlock(MapElement* pMapElement,int subgroup = 0);
	int recvMapElementNoBlock(MapElement* pMapElement,int subgroup = 0);

private:
	int setMapElementMsgOut(MapElement* pMapElement);	
	int setMapElementMsgIn(MapElement* pMapElement);	
	
	int mapElementSendSocket;
	MapElementMsg mapElementMsgOut;

	int mapElementRecvSocket;
	MapElementMsg mapElementMsgIn;

	skynet * skynet_ptr;


};
#endif
