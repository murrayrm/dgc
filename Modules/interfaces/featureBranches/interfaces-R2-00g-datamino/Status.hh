/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef STATUS_HH_
#define STATUS_HH_


struct SegGoalsStatus
{
  enum Status{ ACCEPT, REJECT, COMPLETED, FAILED, READY_FOR_NEXT };
  SegGoalsStatus()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
    status = FAILED;
  }
  int goalID;
  Status status;
  int currentSegmentID;
  int currentLaneID;
  int lastWaypointID;
};
#endif //STATUS_HH_
