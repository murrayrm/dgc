/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef STATUS_HH_
#define STATUS_HH_

//#include "gcmodule/GcInterface.hh"
#include "interfaces/GcModuleInterfaces.hh"


class SegGoalsStatus : public GcInterfaceDirectiveStatus
{
public:
  enum Status{ ACCEPT, REJECT, COMPLETED, FAILED };
  enum ReasonForFailure{ OBSTACLES, KNOWLEDGE };

  int goalID;
  Status status;
  int currentSegmentID;
  int currentLaneID;
  int lastWaypointID;
  ReasonForFailure reason;

  SegGoalsStatus()
  {
    // initialize the status
    goalID = 0;
    status = FAILED;
    currentSegmentID = 0;
    currentLaneID = 0;
    lastWaypointID = 0;
  }

  virtual unsigned getDirectiveId()
  {
    return (unsigned) goalID;
  }
  
  virtual int getCustomStatus( ) 
  { 
    return status;
  }

  /*! Serialize function */
  friend class boost::serialization::access;
  private:
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar & goalID;
    ar & status;
    ar & currentSegmentID;
    ar & currentLaneID;
    ar & lastWaypointID;
    ar & reason;
  }

  std::string toString() const {
    stringstream s("");
    s << "GoalId: " << goalID << "Status: "  << status << endl;
    return s.str();
  }

};
#endif //STATUS_HH_
