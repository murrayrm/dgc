#ifndef _VEHICLECAPABILITY_H_
#define _VEHICLECAPABILITY_H_

/*! Vehicle Capability message interface
 *  Author:         Chris Schantz
 *  Date:           July 10th 2007
 *
 *  Desctiption:    These unsigned ints are filed in on a scale of 0-4
 *                  With 5 being full capability and 0 being no capability for the maneuver in question
 */

#include <sstream>
#include <string>

#define MAX_VEHICLE_CAPABILITY 4

typedef struct VehicleCapability
{
    
  unsigned long long timestamp;
    
  int intersectionRightTurn;
  int intersectionLeftTurn;
  int intersectionStraightTurn;
  
  int uturn;
  
  int nominalDriving;
  int nominalStopping;
  int nominalZoneRegionDriving;
  int nominalNewRegionDriving;

  VehicleCapability() {
    timestamp = 0;
    intersectionRightTurn = 0;
    intersectionLeftTurn = 0;
    intersectionStraightTurn = 0;
    uturn = 0;
    nominalDriving = 0;
    nominalStopping = 0;
    nominalZoneRegionDriving = 0;
    nominalNewRegionDriving = 0;
  }
       
  string toString() const 
  {
    stringstream s("");
    s << "Timestamp: " << timestamp << "\n intersectionRightTurn: "
      << intersectionRightTurn << "\n intersectionLeftTurn: " 
      << intersectionLeftTurn << "\n intersectionStraightTurn: "
      << intersectionStraightTurn << "\n uturn: "
      << uturn << "\n nominalDriving: "
      << nominalDriving << "\n nominalStopping: "
      << nominalStopping << "\n nominalZoneRegionDriving: "
      << nominalZoneRegionDriving << "\n nominalNewRegionDriving: "
      << nominalNewRegionDriving << "\n";
    return s.str();
  }
  
  template <class Archive>
  void serialize(Archive &ar,const unsigned int version)
  {
    ar & timestamp;
    
    ar & intersectionRightTurn;
    ar & intersectionLeftTurn;
    ar & intersectionStraightTurn;
    
    ar & uturn;
    
    ar & nominalDriving;
    ar & nominalStopping;
    ar & nominalZoneRegionDriving;
    ar & nominalNewRegionDriving;
    
  }




};

#endif

