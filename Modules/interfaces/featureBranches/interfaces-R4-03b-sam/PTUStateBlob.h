
/* 
 * Desc: SensNet PTU state blob
 * Date: 20 May 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/

#ifndef PTU_STATE_BLOB_H
#define PTU_STATE_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <math.h>
  
#include "VehicleState.h"

  
/// @brief Blob version number
#define PTU_STATE_BLOB_VERSION 0x01  

///
typedef struct _PTUStateBlob
{  
  /// Blob type (must be SENSNET_PTU_STATE_BLOB)
  int blobType;
  
  /// Version number (must be PTU_STATE_BLOB_VERSION)
  int32_t version;

  /// Sensor ID for originating sensor
  int sensorId;

  /// Scan id
  int scanId;

  /// Image timestamp
  uint64_t timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Tool-to-PTU transformation (homogeneous matrix); 
  //  --- the Tool in this case is the tool frame of the PTU
  //  --- the PTU in this case is the stationary frame of the PTU
  float tool2ptu[4][4];

  /// PTU-to-Tool transformation (homogeneous matrix);
  //  --- the tool in this case is the tool frame of the PTU
  //  --- the PTU in this case is the stationary frame of the PTU
  float ptu2tool[4][4];
  
  /// Vehicle-to-PTU transformation (homogeneous matrix);
  //  --- the PTU in this case is the stationary frame of the PTU
  float veh2ptu[4][4];

  /// PTU-to-Vehicle transformation (homogeneous matrix);
  //  --- the PTU in this case is the stationary frame of the PTU
  float ptu2veh[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix); 
  float veh2loc[4][4];

  /// Local-to-vehicle transformation (homogeneous matrix)
  float loc2veh[4][4];

  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];

  // Pan and tilt Angles 
  float currpan;
  float currtilt;
  float currpanspeed;
  float currtiltspeed;

  /// Padding
  uint8_t padding[1176];
  
} __attribute__((packed)) PTUStateBlob;


/// @brief Convert from Tool to PTU frame
static  __inline__
void PTUStateBlobToolToPTU(PTUStateBlob *self, float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->tool2ptu[0][0]*px + self->tool2ptu[0][1]*py +
        self->tool2ptu[0][2]*pz + self->tool2ptu[0][3];
  *qy = self->tool2ptu[1][0]*px + self->tool2ptu[1][1]*py +
        self->tool2ptu[1][2]*pz + self->tool2ptu[1][3];
  *qz = self->tool2ptu[2][0]*px + self->tool2ptu[2][1]*py +
        self->tool2ptu[2][2]*pz + self->tool2ptu[2][3];  
  return;
}


/// @brief Convert from PTU to Sensor frame
static  __inline__
void PTUStateBlobPTUToTool(PTUStateBlob *self, float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->ptu2tool[0][0]*px + self->ptu2tool[0][1]*py +
        self->ptu2tool[0][2]*pz + self->ptu2tool[0][3];
  *qy = self->ptu2tool[1][0]*px + self->ptu2tool[1][1]*py +
        self->ptu2tool[1][2]*pz + self->ptu2tool[1][3];
  *qz = self->ptu2tool[2][0]*px + self->ptu2tool[2][1]*py +
        self->ptu2tool[2][2]*pz + self->ptu2tool[2][3];  
  return;
}


/// @brief Convert from vehicle to PTU frame
static  __inline__
void PTUStateBlobVehicleToPTU(PTUStateBlob *self, float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->veh2ptu[0][0]*px + self->veh2ptu[0][1]*py +
        self->veh2ptu[0][2]*pz + self->veh2ptu[0][3];
  *qy = self->veh2ptu[1][0]*px + self->veh2ptu[1][1]*py +
        self->veh2ptu[1][2]*pz + self->veh2ptu[1][3];
  *qz = self->veh2ptu[2][0]*px + self->veh2ptu[2][1]*py +
        self->veh2ptu[2][2]*pz + self->veh2ptu[2][3];  
  return;
}


/// @brief Convert from PTU to vehicle frame
static  __inline__
void PTUStateBlobPTUToVehicle(PTUStateBlob *self, float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->ptu2veh[0][0]*px + self->ptu2veh[0][1]*py +
        self->ptu2veh[0][2]*pz + self->ptu2veh[0][3];
  *qy = self->ptu2veh[1][0]*px + self->ptu2veh[1][1]*py +
        self->ptu2veh[1][2]*pz + self->ptu2veh[1][3];
  *qz = self->ptu2veh[2][0]*px + self->ptu2veh[2][1]*py +
        self->ptu2veh[2][2]*pz + self->ptu2veh[2][3];  
  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__
void PTUStateBlobLocalToVehicle(PTUStateBlob *self, float px, float py, float pz,
                                  float *qx, float *qy, float *qz)
{
  *qx = self->loc2veh[0][0]*px + self->loc2veh[0][1]*py +
        self->loc2veh[0][2]*pz + self->loc2veh[0][3];
  *qy = self->loc2veh[1][0]*px + self->loc2veh[1][1]*py +
        self->loc2veh[1][2]*pz + self->loc2veh[1][3];
  *qz = self->loc2veh[2][0]*px + self->loc2veh[2][1]*py +
        self->loc2veh[2][2]*pz + self->loc2veh[2][3];  
  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__
void PTUStateBlobVehicleToLocal(PTUStateBlob *self, float px, float py, float pz,
                                  float *qx, float *qy, float *qz)
{
  *qx = self->veh2loc[0][0]*px + self->veh2loc[0][1]*py +
        self->veh2loc[0][2]*pz + self->veh2loc[0][3];
  *qy = self->veh2loc[1][0]*px + self->veh2loc[1][1]*py +
        self->veh2loc[1][2]*pz + self->veh2loc[1][3];
  *qz = self->veh2loc[2][0]*px + self->veh2loc[2][1]*py +
        self->veh2loc[2][2]*pz + self->veh2loc[2][3];  
  return;
}


#ifdef __cplusplus
}
#endif


#endif
