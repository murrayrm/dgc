/**********************************************************
 **
 **  MAPELEMENTMSG.H
 **
 **    Time-stamp: <2007-07-17 04:10:49 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb  6 18:45:42 2007
 **
 **
 **********************************************************
 **
 ** Struct representation of MapElement class
 ** this is statically sized and used for skynet 
 ** only.   
 **
 **********************************************************/


#ifndef MAPELEMENTMSG_H
#define MAPELEMENTMSG_H

#include "interfaces/VehicleState.h"

using namespace std;

#define MAP_ELEMENT_NUMPTS 2048
#define MAP_ELEMENT_ID_LENGTH 10
#define MAP_ELEMENT_LABEL_LENGTH 50
#define MAP_ELEMENT_LABEL_NUMLINES 3

typedef struct
{
  float x,y,z;
  float max_var, min_var, axis;

} __attribute__((packed)) point2_uncertain_struct;
    
  


typedef struct
{
  int subgroup;
  int numIds;
  int id[MAP_ELEMENT_ID_LENGTH]; 
  float conf;

  int type;
  float typeConf;

  point2_uncertain_struct position;

  point2_uncertain_struct center;

  float length;  
  float width;
  float orientation;

  float lengthVar;
  float widthVar;
  float orientationVar;

    
  float height;
  float heightVar;

  float elevation;
  float elevationVar;


  point2_uncertain_struct velocity;
  float timeStopped; //s
  float peakSpeed; // m/s
  float peakSpeedVar; // m/s
  float peakAccel; // m/s^2
  float peakAccelVar; // m/s^2
  int timesSeen;

  int frameType;
  uint64_t timestamp;
  VehicleState state;
  
  int plotColor;
  int plotValue;


  int numLines;
  char label[MAP_ELEMENT_LABEL_NUMLINES][MAP_ELEMENT_LABEL_LENGTH];


  point2_uncertain_struct geometryMin;
  point2_uncertain_struct geometryMax;

  int numPts;
  int geometryType;
  point2_uncertain_struct geometry[MAP_ELEMENT_NUMPTS];

 
 } __attribute__((packed)) MapElementMsg;

#endif
