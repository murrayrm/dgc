/*
    AliceFlat_Definitions.hh
 
	Melvin E. Flores and Mark B. Milam
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 07-Apr-2007.
*/

#ifndef AliceFlat_Definitions_HH
#define AliceFlat_Definitions_HH

/* Flat outputs: */
#define z1			 z[0]
#define z1d			 z[1]
#define z1dd		 z[2]
#define z1ddd		 z[3]
#define z2			 z[4]
#define z2d			 z[5]
#define z2dd		 z[6]
#define z2ddd		 z[7]
#define z3			 z[8]

/* States: */
#define x1			 x[0][0]
#define x1d			 x[0][1]
#define x1dd		 x[0][2]
#define x2			 x[1][0]
#define x2d			 x[1][1]
#define x2dd		 x[1][2]
#define x3			 x[2][0]
#define x3d			 x[2][1]
#define x4			 x[3][0]
#define x4d			 x[3][1]

/* Inputs: */
#define u1			 u[0][0]
#define u2			 u[1][0]
#define u2d			 u[1][1]

/* Parameters: */
#define p1			 Parameters[0]

#endif  /*AliceFlat_Definitions_HH*/