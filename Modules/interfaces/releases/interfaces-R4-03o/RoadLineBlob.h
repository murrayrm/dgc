/* 
 * Desc: SensNet road line data
 * Date: 28 Sep 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef ROAD_LINE_BLOB_H
#define ROAD_LINE_BLOB_H

#include <stdint.h>
#include "VehicleState.h"


/** @file

Blob containing info on the detected road lines.  The detection may be
performed by stereo or ladar.

*/


/// @brief Blob version number
#define ROAD_LINE_BLOB_VERSION 0x01

/// @brief Maximum number of lines in any message
#define ROAD_LINE_BLOB_MAX_LINES 64


/// @brief Types of detected lines
enum
{
  ROAD_LINE_BLOB_TYPE_UNKNOWN,
  ROAD_LINE_BLOB_TYPE_WHITE,
  ROAD_LINE_BLOB_TYPE_YELLOW,
};


/// @brief Description for a single line
typedef struct
{
  /// Line type (e.g., solid white)
  int type;
  
  /// Line start point (local frame)
  float ax, ay, az;

  /// Line end point (local frame)
  float bx, by, bz;
  
} __attribute__((packed)) RoadLine;



/// @brief Blob containing road line data.
typedef struct
{
  /// Blob type (must be SENSNET_ROAD_LINE_BLOB)
  int32_t blobType;

  /// Version number (must be ROAD_LINE_BLOB_VERSION)
  int32_t version;

  /// ID of the originating perceptor
  int32_t sensnetId;

  /// Unique frame id (increased monotonically)
  int32_t frameId;

  /// Frame timestamp
  uint64_t timestamp;

  /// Vehicle state data
  VehicleState state;

  /// List of detected lines.
  int numLines;
  RoadLine lines[ROAD_LINE_BLOB_MAX_LINES];
  
} __attribute__((packed)) RoadLineBlob;


#endif
