#include <iostream>
#include <limits>
#include <cmath>

#include "Polygon.hh"

// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

namespace bitmap
{
    using namespace std;

    void Polygon::setVertices(const vector<point2>& v, const vector<float>& cost)
    {
        assert(cost.size() == v.size());
        m_xv.resize(v.size());
        m_yv.resize(v.size());
        m_costVec.resize(cost.size());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            m_xv[i] = v[i].x;
            m_yv[i] = v[i].y;
            m_costVec[i] = cost[i];
        }
    }

    // the code is exactly the same as for point2
    void Polygon::setVertices(const vector<point2_uncertain>& v, const vector<float>& cost)
    {
        assert(cost.size() == v.size());
        m_xv.resize(v.size());
        m_yv.resize(v.size());
        m_costVec.resize(cost.size());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            m_xv[i] = v[i].x;
            m_yv[i] = v[i].y;
            m_costVec[i] = cost[i];
        }
    }

    void combineReplace(cost_t* dest, cost_t* src)
    {
        *dest = *src;
    }

    void combineMax(cost_t* dest, cost_t* src)
    {
        *dest = max(*dest, *src);
    }

    void combineMin(cost_t* dest, cost_t* src)
    {
        *dest = min(*dest, *src);
    }

    void combineAdd(cost_t* dest, cost_t* src)
    {
        *dest += *src;
    }

    void combineSub(cost_t* dest, cost_t* src)
    {
        *dest -= *src;
    }
    
    void combineMul(cost_t* dest, cost_t* src)
    {
        *dest *= *src;
    }

    void combineDiv(cost_t* dest, cost_t* src)
    {
        *dest /= *src; // if *src == 0, you'll get +inf, if this is fine, you decide!
    }

    void combineAvg(cost_t* dest, cost_t* src)
    {
        *dest = (*dest + *src)/2;
    }


    combine_f combineTable[] =
    {
        combineReplace,
        combineMax,
        combineMin,
        combineAdd,
        combineSub,
        combineMul,
        combineDiv,
        combineAvg,
        NULL /* just in case, used as a terminator */
    };

    cost_t fillLinear(cost_t x, cost_t a)
    {
        return a * x;
    }

    cost_t fillSquare(cost_t x, cost_t a)
    {
        return a * x*x;
    }

    cost_t fillExp(cost_t x, cost_t a)
    {
        return exp(a * x);
    }

    cost_t fillExp2(cost_t x, cost_t a)
    {
        return exp(a * x*x);
    }

    cost_t fillCos(cost_t x, cost_t a)
    {
        return cos(a * x);
    }

    cost_t fillSin(cost_t x, cost_t a)
    {
        return sin(a * x);
    }

    cost_t fillTan(cost_t x, cost_t a)
    {
        return tan(a * x);
    }

    cost_t fillAtan(cost_t x, cost_t a)
    {
        return atan(a * x);
    }

    fill_f fillTable[] =
    {
        NULL, /* FILL_NONE */
        fillLinear,
        fillSquare,
        fillExp,
        fillExp2,
        fillCos,
        fillSin,
        fillTan,
        fillAtan,
        NULL
    };
    
    /* Ok guys, this is some very old code ('99 I think) that used to run under dos
     * in the times when there were no accelerated graphic cards, to draw shaded
     * polygons on the screen.
     * I've completely rewritten the draw_polygon function, so it should be easily readable
     * and debuggable, but the draw_scanline function is still the same (is a pretty simple
     * function anyway). I'll need to change that soon anyway.
     * -- Daniele Tamino
     */

#if 0
    float clip_line_y(float max, float min, float *x1, float *y1, float *x2, float *y2)
    {
        float dist = 0;
        if(*y1 == *y2) {
            if (*y1 > max || *y1 < min)
                return -1;
            else
                return 0;
        }
        // if the verticies are in the wrong order, exchange them
        if(*y1 > *y2) {
            swap(y1, y2);
            swap(x1, x2);
        }
        // completely out of bounds
        if(*y2<=min) { *y1 = *y2 = min; return -1; }
        if(*y1>=max) { *y1 = *y2 = max+1; return -1; }

        /* (x2 - x1)*(y - y1) = (x - x1)*(y2 - y1) */

        /* intersection with y = min */
        if(*y1 < min) {
            /* (x2 - x1)*(min - y1) = (x - x1)*(y2 - y1) */
            /* (y2 - y1)*x = x1*(y2 - min) + x2*(min - y1) */
            float x = ((*x1)*((*y2) - min) + (*x2)*(min - (*y1))) / ((*y2) - (*y1));
            *x1 = x;
            *y1 = min;
        }
        /* intersection con y = max */
        if(*y2 > max) {
            /* (x2 - x1)*(max - y1) = (x - x1)*(y2 - y1) */
            /* (y2 - y1)*x = x1*(y2 - min) + x2*(min - y1) */
            float x = ((*x1)*((*y2) - max) + (*x2)*(max - (*y1))) / ((*y2) - (*y1));
            dist = (*y2)-max;
            *x2 = x;
            *y2 = max;
        }
        return dist;
    }
#endif

    /** Draws a scanline (horizontal straight line). The length of the scanline once
     * drawn will be (x2 - x1) cells, including the starting poing and excluding the
     * ending one. If x1 > x2, the endpoints will be exchanged.
     * @param map CostMap where to draw the scanline
     * @param x1 starting point on the row. It's NOT required that x1 < x2.
     * @param x2 ending point on the row.
     * @param val1 Desired value of cell at x1.
     * @param val2 Desired value of cell at x2.
     * @param row The row where all the drawing should happen.
     */
    static void draw_scanline(CostMap* map, int x1, int x2,
                              cost_t val1, cost_t val2, int row)
    {
        cost_t curVal;
        cost_t incVal;
  
        // we want x2 > x1
        if (x2 < x1) {
            swap(x2, x1);
            swap(val1, val2);
        }
        if (x1 > (map->getWidth() - 1) || x2 < 0) // completely out of bounds
            return;
        //if (x2 == x1)
        //    return;

        curVal = val1;
        incVal = (val2 - val1) / (x2 - x1);

        if (x1 < 0) {
            curVal += incVal*(-x1);
            x1 = 0;
        }
        if (x2 > (map->getWidth() - 1))
            x2 = map->getWidth() - 1;
        //if(x2 == x1)
        //    return;

        cost_t* adr = map->getRow(row) + x1;

        for(int x = x1; x <= x2; x++) {
            *adr = curVal;
            adr++;
            curVal += incVal;
        }
    }

#define USE_BRESENHAM_ALG 1
#if !USE_BRESENHAM_ALG
    struct PolyEdge
    {
        float x;
        float incX;
        float c;
        float incC;
        float rowEnd;

        /// @param x1 x coordinate of first point (column)
        /// @param x2 x coordinate of second point (column)
        /// @param y1 y coordinate of first point (row)
        /// @param y2 y coordinate of secons point (row). Must be y2 < y1
        /// @param c1 Cost of the first vertex
        /// @param c2 Cost of the second vertex
        /// @param bottom Set to true if this is a bottom edge (i.e. the polygon
        /// lies over this edge).
        void set(float x1, float x2, float y1, float y2, float c1, float c2, bool bottom)
        {
            //MSG("PolyEdge::set(" << x1 << ", " << x2 << ", " << y1 << ", " << y2 << ", "
            //    << c1 << ", " << c2 << ", " << bottom << ")");
            float preInc = y1 - int(y1 + 0.5);
            c = c1;
            if (y1 - y2 > 0.001)
            {
                incX = (x2 - x1) / (y1 - y2);
                incC = (c2 - c1) / (y1 - y2);
            }
            else
            {
                incX = 0; // avoid div-by-zero or by a very small number
                incC = 0;
            }

            x = x1 /* preInc * incX */;
            c = c1 /* + preInc * incC */;
            if (bottom) // this is to avoid having disconnected polygons
            {
                x += incX - 1;
                c += incC * (1 - 1/incX);
            }
            rowEnd = int(y2 + 0.5);
            //MSG("PolyEdge::set: x=" << x << ", incX=" << incX << ", c=" << c
            //    << ", incC=" << incC << ", rowEnd=" << rowEnd);
        }

        void next()
        {
            x += incX;
            c += incC;
        }

        bool done(int row)
        {
            return row < rowEnd;
        }
    };
#else
    /* Modified Bresenham's line drawing algorithm */

    struct PolyEdge
    {
        int x;
        float c;
        float incC;
        int rowEnd;

    private:
        bool bottom;
        
        int deltay;
        int deltax;
        int error;
        int incX;
        int stepX;
        
    public:

        /// @param x1 x coordinate of first point (column)
        /// @param x2 x coordinate of second point (column)
        /// @param y1 y coordinate of first point (row)
        /// @param y2 y coordinate of secons point (row). Must be y2 < y1
        /// @param c1 Cost of the first vertex
        /// @param c2 Cost of the second vertex
        /// @param left Set to true if this is a left edge
        void set(float x1, float x2, float y1, float y2, float c1, float c2, bool left)
        {
            //MSG("PolyEdge::set(" << x1 << ", " << x2 << ", " << y1 << ", " << y2 << ", "
            //    << c1 << ", " << c2 << ", " << left << ")");
            
/*          // this should never happen
            if (y1 < y2)
            {
                swap(x1, x2);
                swap(y1, y2);
                swap(c1, c2);
            }
*/
            c = c1;
            if (y1 - y2 > 0.001)
            {
                incC = (c2 - c1) / (y1 - y2);
            }
            else
            {
                incC = 0;
            }
            rowEnd = int(y2 + 0.5);
            if (rowEnd < 0)
            {
                rowEnd = 0;
            }

            bottom = (left && (x2 < x1)) || (!left && (x2 > x1));

            deltay = int(y1+0.5) - int(y2+0.5);
            deltax = abs(int(x2+0.5) - int(x1+0.5));
            error = -deltay / 2;

            if (deltay != 0) {
                incX = deltax / deltay;
                deltax = deltax % deltay;
                if (x2 < x1) {
                    incX = -incX;
                    stepX = -1;
                } else {
                    stepX = 1;
                }
            } else {
                incX = 0;
                stepX = 0;
            }

            x = int(x1 + 0.5);
            c = c1;
            if (bottom)
            {
                x += incX;
                c += incC;
            }

            //MSG("PolyEdge::set: x=" << x << ", incX=" << incX << ", c=" << c
            //    << ", incC=" << incC << ", rowEnd=" << rowEnd);
        }

        void next()
        {
            c += incC;
            if (!bottom)
                x += incX;

            error = error + deltax;
            if (error > 0)
            {
                x = x + stepX;
                error = error - deltay;
            }

            if (bottom)
                x += incX;
        }

        bool done(int row)
        {
            return row <= rowEnd;
        }

    };

#endif

    static inline int nextVert(int vert, int nVert)
    {
        return (vert + 1 >= nVert) ? 0 : (vert + 1);
    }

    static inline int prevVert(int vert, int nVert)
    {
        return (vert - 1 < 0) ? (nVert - 1) : (vert - 1);
    }

    static void draw_polygon(CostMap* map,
                             const vector<float>& vertX,
                             const vector<float>& vertY,
                             const vector<cost_t>& vertVal)
    {
        int nVert = int(vertX.size());
        assert(int(vertY.size()) == nVert);
        assert(int(vertVal.size()) == nVert);

        //MSG("draw_polygon BEGIN");

        float miny=numeric_limits<float>::max();
        float maxy=numeric_limits<float>::min();

        int bottom = 0;
        int row;

        // search for the vertex with the highes y value
        for (int i = 0; i < nVert; i++) {
            if (vertY[i] > maxy) {
                bottom = i;
                maxy = vertY[i];
            }
            if (vertY[i] < miny) {
                miny = vertY[i];
            }
        }
        if(vertY[bottom] < 0) return; // completely over the top boundary
        if(miny < 0) miny = 0; //bugfix (chance of infinite loop otherwise)

        int left = prevVert(bottom, nVert);
        int right = nextVert(bottom, nVert);
        bool cw = false;
        float dxL = vertX[left]  - vertX[bottom];
        float dyL = vertY[left]  - vertY[bottom];
        float dxR = vertX[right] - vertX[bottom];
        float dyR = vertY[right] - vertY[bottom];
        if (dxL*dyR - dyL*dxR < 0)
        {
            //MSG("clockwise order, swapping left and right");
            swap(left, right);
            cw = true; // clockwise vertex order
        }

        PolyEdge leftEdge, rightEdge;
        //PolyEdge leftEdge(map->getHeight(), map->getWidth());
        //PolyEdge rightEdge(map->getHeight(), map->getWidth());
        leftEdge.set(vertX[bottom],   vertX[left],
                     vertY[bottom],   vertY[left],
                     vertVal[bottom], vertVal[left],
                     /*vertX[bottom] > vertX[left]*/
                     true
            );
        rightEdge.set(vertX[bottom],   vertX[right],
                      vertY[bottom],   vertY[right],
                      vertVal[bottom], vertVal[right],
                      /*vertX[bottom] < vertX[right]*/
                      false
            );

        int minRow = int(miny + 0.5);
        for (row = int(vertY[bottom] + 0.5); row >= minRow; row--)
        {
            //MSG("row " << row << ":");
            if (row != minRow && leftEdge.done(row))
            {
                //MSG("  next left edge");
                int last = left;
                if (cw)
                    left = nextVert(left, nVert); // want them in ccw
                else
                    left = prevVert(left, nVert);

                leftEdge.set(vertX[last],   vertX[left],
                             vertY[last],   vertY[left],
                             vertVal[last], vertVal[left],
                             /*vertX[last] > vertX[left]*/
                             true
                    );
            }
            if (row != minRow && rightEdge.done(row))
            {
                //MSG("  next right edge");
                int last = right;
                if (cw)
                    right = prevVert(right, nVert); // want them in ccw
                else
                    right = nextVert(right, nVert);

                rightEdge.set(vertX[last],   vertX[right],
                              vertY[last],   vertY[right],
                              vertVal[last], vertVal[right],
                              /*vertX[last] < vertX[right]*/
                              false
                    );
            }

            if (row < map->getHeight())
            {
                //draw_scanline(map, int(leftEdge.x + 0.5), int(rightEdge.x + 0.5),
                //              leftEdge.c, rightEdge.c, row);
                draw_scanline(map, leftEdge.x, rightEdge.x,
                              leftEdge.c, rightEdge.c, row);
            }
            leftEdge.next();
            rightEdge.next();
            //MSG("  leftEdge.x = " << leftEdge.x);
            //MSG("  rightEdge.x = " << rightEdge.x);
        }
        //MSG("draw_polygon END");
    }

    /**
     * Draw the polygon on the specified cost map, interpreting the vertices
     * as they are in local frame.
     * The polygon will be correctly clipped if it's partly or completely
     * outside of the map.
     */
    void Polygon::draw(CostMap* bmp) const
    {
        unsigned int nVec = m_xv.size();
        vector<float> rowVec(nVec);
        vector<float> colVec(nVec);
        //MSG("DBG: draw(): translating coordinates (up left = "
        //    << bmp->getUpperLeft() << ", low right = " << bmp->getLowerRight() << ")");
        for (unsigned int i = 0; i < nVec; i++)
        {
            bmp->toMapFrame(m_xv[i], m_yv[i], &rowVec[i], &colVec[i]);
            //MSG("   (" << m_xv[i] << ", " << m_yv[i] << ") --> (" << colVec[i] << ", " << rowVec[i] << ")");
        }
        //MSG("DBG: draw() end");
        draw_polygon(bmp, colVec, rowVec, m_costVec);
    }

    /**
     * Draw the polygon on the specified cost map, interpreting the vertices
     * as they are in map coordinates already (y = row, x = column).
     * The polygon will be correctly clipped if it's partly or completely
     * outside of the map.
     */
    void Polygon::drawMapCoords(CostMap* bmp) const
    {
        draw_polygon(bmp, m_xv, m_yv, m_costVec);

    }


};
