//this file define the information relative to the vehicle that is preceding Alice
//as needed to do queuing and following
//S. Di Cairano jun-07

#ifndef LEADING_VEHICLE_INFO_HH
#define LEADING_VEHICLE_INFO_HH


// Threshold constants, to be defined
#define THRS_MIN 5.4356*2
#define THRS_MAX 5.4356*3
#define LV_OUT_OF_RANGE 20.0

/*! structure that defines the leading vehicle information that are sent by tplanner to gcfollower
 * for queuing and obstacle avoidance
 */

struct leadVehInfo
{
    double velocity ;
    double distance ;
    double separationDistance;

	leadVehInfo()
	{
         velocity = 5.0 ;
		 distance = LV_OUT_OF_RANGE ;
         separationDistance = THRS_MAX;
	}

    template <class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
        ar & velocity;
	    ar & distance;
        ar & separationDistance;
	}

};


#endif
