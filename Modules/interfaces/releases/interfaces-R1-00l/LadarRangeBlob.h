/* 
 * Desc: SensNet ladar blob
 * Date: 3 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef LADAR_RANGE_BLOB_H
#define LADAR_RANGE_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <math.h>
  
#include "VehicleState.h"

/** @file

@brief Ladar blob and some useful accessors.

*/

/// @brief Maximum image dimensions
#define SENSNET_LADAR_BLOB_MAX_POINTS 181
  

/// @brief Ladar scan data.
///
typedef struct _LadarRangeBlob
{  
  /// Blob type (must be SENSNET_LADAR_BLOB)
  int blob_type;

  /// Sensor ID for originating sensor
  int sensor_id;

  /// Scan id
  int scanid;

  /// Image timestamp
  uint64_t timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Sensor-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];

  /// Number of points
  int num_points;
  
  /// Range data
  float points[SENSNET_LADAR_BLOB_MAX_POINTS][2];
  
} __attribute__((packed)) LadarRangeBlob;


/// @brief Convert polar (bearing, range) to (x,y,z) in sensor frame.
static  __inline__ void sensnet_ladar_br_to_xyz(LadarRangeBlob *self,
                                                float pa, float pr,
                                                float *px, float *py, float *pz)
{
  *px = pr * cos(pa);
  *py = pr * sin(pa);
  *pz = 0;  
  return;
}


/// @brief Convert from sensor to vehicle frame
static  __inline__ void sensnet_ladar_sensor_to_vehicle(LadarRangeBlob *self,
                                                        float px, float py, float pz,
                                                        float *qx, float *qy, float *qz)
{
  *qx = self->sens2veh[0][0]*px + self->sens2veh[0][1]*py + self->sens2veh[0][2]*pz + self->sens2veh[0][3];
  *qy = self->sens2veh[1][0]*px + self->sens2veh[1][1]*py + self->sens2veh[1][2]*pz + self->sens2veh[1][3];
  *qz = self->sens2veh[2][0]*px + self->sens2veh[2][1]*py + self->sens2veh[2][2]*pz + self->sens2veh[2][3];
  
  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__ void sensnet_ladar_vehicle_to_local(LadarRangeBlob *self,
                                                       float px, float py, float pz,
                                                       float *qx, float *qy, float *qz)
{
  *qx = self->veh2loc[0][0]*px + self->veh2loc[0][1]*py + self->veh2loc[0][2]*pz + self->veh2loc[0][3];
  *qy = self->veh2loc[1][0]*px + self->veh2loc[1][1]*py + self->veh2loc[1][2]*pz + self->veh2loc[1][3];
  *qz = self->veh2loc[2][0]*px + self->veh2loc[2][1]*py + self->veh2loc[2][2]*pz + self->veh2loc[2][3];
  
  return;
}


#ifdef __cplusplus
}
#endif


#endif
