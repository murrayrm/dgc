/**********************************************************
 **
 **  MAPELEMENTMSG.H
 **
 **    Time-stamp: <2007-02-08 18:48:34 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb  6 18:45:42 2007
 **
 **
 **********************************************************
 **
 ** Struct representation of MapElement class
 ** this is statically sized and used for skynet 
 ** only.   
 **
 **********************************************************/


#ifndef MAPELEMENTMSG_H
#define MAPELEMENTMSG_H

#include "interfaces/VehicleState.h"
#include "frames/poscov2.hh"


#define MAP_ELEMENT_NUMPTS 16

typedef struct
{

	int id; 
	double conf;

	int type;
	double type_conf;

	poscov2 center;

	double length;  
	double width;
	double orientation;

	double length_var;
	double width_var;
	double orientation_var;

	int num_pts;
	poscov2 geometry[MAP_ELEMENT_NUMPTS];
		
	double height;
	double height_var;

	poscov2 velocity;
	int frame;
  VehicleState state;

 
 } __attribute__((packed)) MapElementMsg;

#endif
