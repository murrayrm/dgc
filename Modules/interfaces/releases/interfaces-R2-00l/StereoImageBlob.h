/* 
 * Desc: SensNet stereo image blob.
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef STEREO_IMAGE_BLOB_H
#define STEREO_IMAGE_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "VehicleState.h"

  
/** @file

@brief Stereo image data and some useful accessors.

*/


/// @brief Blob version number
#define STEREO_IMAGE_BLOB_VERSION 0x02  
  
/// @brief Maximum image dimensions
#define STEREO_IMAGE_BLOB_MAX_COLS 640

/// @brief Maximum image dimensions
#define STEREO_IMAGE_BLOB_MAX_ROWS 480
  

/// @brief Stereo image blob.
///
/// The blob contains rectified color and disparity images for the left camera.
typedef struct _StereoImageBlob
{  
  /// Blob type (must be SENSNET_STEREO_BLOB)
  int32_t blobType;

  /// Version number (must be STEREO_IMAGE_BLOB_VERSION)
  int32_t version;
  
  /// ID for originating sensor.
  int32_t sensorId;

  /// Unique frame id (increased monotonically)
  int32_t frameId;

  /// Image timestamp
  uint64_t timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Camera calibration data: (cx, cy) is the image center in pixels,
  /// (sx, sy) is the focal length in pixels.
  float cx, cy, sx, sy;
  
  // Stereo baseline (m).
  float baseline;
  
  /// Disparity scaling factor
  float dispScale;

  /// Camera-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];

  /// Camera gain (dB)
  float gain;

  /// Camera shutter speed (ms)
  float shutter;

  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];
  
  /// Image dimensions 
  int32_t cols, rows;

  /// Number of color channels (1 for mono, 3 for RGB)
  int32_t colorChannels;

  /// Size of color image data (8-bits/pixel)
  uint32_t colorSize;
  
  /// Color/grayscale image data
  uint8_t colorData[STEREO_IMAGE_BLOB_MAX_COLS * STEREO_IMAGE_BLOB_MAX_ROWS * 3];

  /// Size of disparity image (16-bits/pixel)
  uint32_t dispSize;

  /// Disparity data
  uint16_t dispData[STEREO_IMAGE_BLOB_MAX_COLS * STEREO_IMAGE_BLOB_MAX_ROWS * 1];
  
} __attribute__((packed)) StereoImageBlob;


/// @brief Get the disparity value at the given image location
static __inline__ 
uint16_t StereoImageBlobGetDisp(StereoImageBlob *self, int c, int r)
{
  return self->dispData[r * self->cols + c];
}


/// @brief Convert from image coords (col,row,disparity) to sensor coords (x,y,z).
static  __inline__ 
void StereoImageBlobImageToSensor(StereoImageBlob *self,
                                   int c, int r, uint16_t d,
                                   float *x, float *y, float *z)
{
  *z = self->sx / ((float) (int) d) * self->dispScale * self->baseline;
  *x = (c - self->cx) / self->sx * *z;
  *y = (r - self->cy) / self->sy * *z;
  return;
}


/// @brief Convert from sensor to vehicle frame
static  __inline__ void StereoImageBlobSensorToVehicle(StereoImageBlob *self,
                                                       float px, float py, float pz,
                                                       float *qx, float *qy, float *qz)
{
  *qx = self->sens2veh[0][0]*px + self->sens2veh[0][1]*py +
        self->sens2veh[0][2]*pz + self->sens2veh[0][3];
  *qy = self->sens2veh[1][0]*px + self->sens2veh[1][1]*py +
        self->sens2veh[1][2]*pz + self->sens2veh[1][3];
  *qz = self->sens2veh[2][0]*px + self->sens2veh[2][1]*py +
        self->sens2veh[2][2]*pz + self->sens2veh[2][3];  
  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__ void StereoImageBlobVehicleToLocal(StereoImageBlob *self,
                                                      float px, float py, float pz,
                                                      float *qx, float *qy, float *qz)
{
  *qx = self->veh2loc[0][0]*px + self->veh2loc[0][1]*py +
        self->veh2loc[0][2]*pz + self->veh2loc[0][3];
  *qy = self->veh2loc[1][0]*px + self->veh2loc[1][1]*py +
        self->veh2loc[1][2]*pz + self->veh2loc[1][3];
  *qz = self->veh2loc[2][0]*px + self->veh2loc[2][1]*py +
        self->veh2loc[2][2]*pz + self->veh2loc[2][3];  
  return;
}

#ifdef __cplusplus
}
#endif

#endif
