/* 
 * Desc: SensNet message types.
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef SENSNET_TYPES_H
#define SENSNET_TYPES_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <string.h>

/** @file

@brief Enumerations for all SensNet ids and blob types.

@todo Change naming convention to CamelCap.

*/

/// @brief Sensor IDs (unique identifier for each data source).
typedef enum _sensnet_id_t
{
  /// Null sensor
  SENSNET_NULL_SENSOR = 0,

  /// Virtual Skynet sensor (for reading skynet messages through sensnet).
  SENSNET_SKYNET_SENSOR = 1,

  /// Left-front bumper stereo 
  SENSNET_LF_SHORT_STEREO,

  /// Right-front short range stereo
  SENSNET_RF_SHORT_STEREO,

  /// Middle-front medium range stereo
  SENSNET_MF_MEDIUM_STEREO,

  /// Middle-front long range stereo
  SENSNET_MF_LONG_STEREO,

  /// Middle-front bumper ladar
  SENSNET_MF_BUMPER_LADAR,

  /// Left-front bumper ladar
  SENSNET_LF_BUMPER_LADAR,

  /// Right-front bumper ladar
  SENSNET_RF_BUMPER_LADAR,

  /// Left-front roof ladar
  SENSNET_LF_ROOF_LADAR,

  /// Right-front roof ladar
  SENSNET_RF_ROOF_LADAR,

  /// Middle-front roof ladar
  SENSNET_MF_ROOF_LADAR,

  /// End-of-list marker
  SENSNET_LAST_SENSOR
  
} sensnet_id_t;


// Macros for creating enum:string lookup tables.
// Based on code on the GnuCash mailing lists.
#define SENSNET_AS_STRING_CASE(name) case name: return #name;
#define SENSNET_FROM_STRING_CASE(name) if (strcmp(str, #name) == 0) return name;
#define SENSNET_DEFINE_LOOKUP(type, name, list)          \
   static const char* name ## _to_name(type n) __attribute__((unused));\
   static const char* name ## _to_name(type n) \
   {                                       \
     switch (n)                            \
     {                                     \
       list(SENSNET_AS_STRING_CASE)        \
       default: return "";                 \
     }                                     \
   }                                       \
   static type name ## _from_name(const char* str) __attribute__((unused));\
   static type name ## _from_name(const char* str) \
   {                                       \
     list(SENSNET_FROM_STRING_CASE)        \
     return (type)-1;                      \
   }                                       

// Lookup table for mapping ids to strings.
#define SENSORID_LIST(_)      \
  _(SENSNET_NULL_SENSOR)      \
  _(SENSNET_LF_SHORT_STEREO)  \
  _(SENSNET_RF_SHORT_STEREO)  \
  _(SENSNET_MF_MEDIUM_STEREO)  \
  _(SENSNET_MF_LONG_STEREO)    \
  _(SENSNET_MF_BUMPER_LADAR)  \
  _(SENSNET_LF_BUMPER_LADAR)  \
  _(SENSNET_RF_BUMPER_LADAR)  \
  _(SENSNET_LF_ROOF_LADAR)  \
  _(SENSNET_RF_ROOF_LADAR)  \
  _(SENSNET_LAST_SENSOR)
SENSNET_DEFINE_LOOKUP(sensnet_id_t, sensnet_id, SENSORID_LIST)


/// @brief Convert a sensor id enum to a string name.
static const char* sensnet_id_to_name(sensnet_id_t id) __attribute__((unused));

/// @brief Convert a string name to a sensor id enum.
static sensnet_id_t sensnet_id_from_name(const char *name) __attribute__((unused));


/// @brief Blob types.
typedef enum _sensnet_blobtype_t
{
  /// Dummy blob type; used for testing only.
  SENSNET_NULL_BLOB,
  
  /// Scan data from ladar.
  SENSNET_LADAR_BLOB,

  /// Combined color/disparity image from stereo.
  SENSNET_STEREO_BLOB
  
} sensnet_blobtype_t;


#ifdef __cplusplus
}
#endif

#endif
