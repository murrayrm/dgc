Release R4-01t (Wed May 30 15:24:47 2007):
	In sn_types.h added type for MODstereoObsPerceptorMedium, Long, and
	PTU.

Release R4-01s (Wed May 30 11:51:46 2007):
  Added ladar intensity values; should have no effect on existing code/logs.

Release R4-01r (Sat May 26 15:58:20 2007):
	Added a new command structure to interface with the pan-tilt 
unit (PTUCommand.h). This will most likely be modified in the coming 
week as the higher-level feeder/controller gets written. I also added a 
new SN type to handle the commands being sent to the PTU.

Release R4-01q (Wed May 23 14:48:48 2007):
	Added and created appropriate message types for the Pan-tilt unit to send pan and tilt angle values. 

Release R4-01p (Wed May 23 12:58:53 2007):
	Added function in StereoImageBlob.h to convert from image to local frames

Release R4-01o (Mon May 21 10:13:12 2007):
Added types for current and future perceptor modules

Release R4-01n (Wed May 16 12:00:11 2007):	
        updated sn_types.h and SensnetTypes.h to account for rear_ladar.

Release R4-01m (Mon May 14 10:31:37 2007):
	updated MapElementMsg.	Added max and min bound terms, cleaned up
	the bounding box representation, added a position field so that the
	center point can float more.  Added elevation term for objects with
	free space below, added time stamp of data collection

Release R4-01l (Sun May 13 23:10:16 2007):
	Created new skynet group for sending status of simulated actuators.
	To be used for updated version of asim and gcdrive that provides
	more realistic simulation capability.

Release R4-01k (Wed May  9  0:58:05 2007):
	Added skynet group for sending parameters for painting cost map

Release R4-01j (Tue May  8 13:16:48 2007):
	Removed Bitmap and Polygon, moved to the new bitmap module.

Release R4-01i (Tue May  8  1:49:43 2007):
	Changed the drawing code, now uses Bresenham's algorithm to enumerate
	pixels on the edges of polygons, and fixed a bunch of bugs on the
	Bitmap (CostMap) and Polygon classes (see ChangeLog).

Release R4-01h (Fri May  4 17:09:09 2007):
	Added the Polygon class, to be sent from the tplanner to the
	dplanner to describe how to draw the cost map, and the Bitmap class
	(ans yes, another CostMap class ...) to actually hold the map,
	where polygons can be drawn. Only linearly filled polygons can be
	drawn at the moment, and they are drawn one on top of the other
	(replacing values).

Release R4-01g (Fri May  4 15:05:17 2007):
  Added module id for graph-planner.

Release R4-01f (Mon Apr 30 12:07:01 2007):
	Changed MapElementMsg.hh data ordering to support variable length
	messages implemented in new map version

Release R4-01e (Fri Apr 27 22:51:13 2007):
  Minor tweaks for radar structure alignment.  Added vehicle speed to
  the VehicleState message using one of the deprecated members (back-wards 
  compatable with existing log files).

Release R4-01d (Fri Apr 27 14:20:05 2007):
	Added skynet types for Traj Planner Pause and End mission 
Release R4-01c (Thu Apr 26 10:54:42 2007):
   Structs to communicate corridor data through skynet added to TpDpInterface.hh .
Release R4-01b (Wed Apr 25 22:57:24 2007):
  Tweaks for ladar-obs-perceptor map interaction.

Release R4-01a (Wed Apr 25 15:24:09 2007):
	Added skynet types for CorridorGenerator 
Release R4-01 (Wed Apr 25 14:52:34 2007):
	Added skynet types for TrafficManager and CorridorGenerator modules
Release R4-00z (Wed Apr 25 10:13:58 2007):
	Yam is doing an in-place sync right now. If this goes through, then the changes I made were to a few files to make 
room for the radarfeeder. The files sn_types.h  and sensnet_types.h were changed to account for the new radar messages 
that are being sent. I also defined a RadarRangeBlob.h to define the radar message sent.

Release R4-00y (Tue Apr 24  9:54:23 2007):
   Astate status is now defined in StatePrecision.hh. Added skynet message for polytopic corridor.

Release R4-00x (Sat Apr 21 19:36:07 2007):
	Added MODroaLadarPerceptor

Release R4-00w (Sat Apr 21 10:51:18 2007):
	This release of the 'interfaces' module defines the EstopStatus type
	returned by the ActuationState and also includes new skynet message
	types for use with the GcModule version of adrive (currently called
	gcdrive).  

	Estop: The status can be EstopRun, EstopPause or EstopDisable.  This
	is defined in ActuationState.h, which is the structure that is used
	to return the estop status (as well as lots of other internal state
	information from adrive).

	sn_types.h: Two new message types are defined: SNadrive_command and
	SNadrive_response, which are used by the GcModule version of adrive
	for communication.  New types were used for backward compatibility
	and also so that gcdrive can be run in simulation mode (gcdrive
	--simulate), with asim then picking up the (old style) actuation
	commands.  I also marked some old skynet message types as
	deprecated, so that we can reuse these if we want.  The number and
	ordering of the skynet type list is unchanged.

Release R4-00v (Sat Apr 21  7:53:15 2007):
	Added skynet type for ROA and trajfollower communication

Release R4-00u (Thu Apr 19 19:51:31 2007):
    Inserted skynet message for Astate health.

Release R4-00t (Tue Apr 17 14:09:29 2007):
    Added definitions of Alice flat model.

Release R4-00s (Sun Apr 15 22:40:36 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R4-00r (Thu Apr 12 18:09:17 2007):
  Added message to report to tplanner the dplanner status in TpDpInterface.hh and a corresponding skynet message 
Release R4-00q (Wed Apr 11 21:34:38 2007):
  Moved SegGoals.hh, Status.hh and GcModuleInterfaces.hh to the gcinterfaces module.

Release R4-00p (Wed Apr 11 20:15:50 2007):
  Added an ostreamable interface to teh ITransmissive interface --> all directives and status 
  will need to implement a std::string toString() function so they can be logged.

Release R4-00o (Wed Apr  4 22:55:12 2007):
  Minor tweak to reduce cross-dependencies.

Release R4-00n (Sun Mar 25 23:32:29 2007):
  Moved the state precision information to a C++ header file (StatePrecision.hh) as requested to avoid breaking older modules.
  
Release R4-00m (Sun Mar 25 18:45:06 2007):
  Had to comment out the conditional compilation for the C++ code as this 
  now conflicts with the C++ compiler.  Suggest the entire offending block
  be removed.

Release R4-00l (Sat Mar 24 17:03:36 2007):
  Fixed the syntax (this is a C file, not a C++ file).  The serialization
  code is still there, but conditionally compiles for C++ only.  Should
  probably be removed.

Release R4-00k (Fri Mar 23 17:47:29 2007):
   Added skynet type for state precision message. Added serialization 
   of VehiclePrecisionPlus struct.

Release R4-00j (Wed Mar 21 16:40:47 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R4-00i (Wed Mar 21 16:17:40 2007):
        Added another struct in VehicleState.h that contains info about the estimation precision. The precision information are asynchronous wrt the estimation, hence it is pointless to include them in the same struct. Hence I did not change the estimator interface.

Release R4-00h (Mon Mar 19 21:18:01 2007):
  Minor cleanup of bogus dependencies, plus adjustment of the ladar 
  and stereo blobs to permit high-speed logging.  Some API changes
  in both blobs. 

Release R4-00g (Sun Mar 18  8:42:46 2007):
	Minor update in preparation for field test.  StereoImageBlob.h
	needed to include vec3.h and pose3.h from the frames module. 

Release R4-00f (Fri Mar 16 13:06:15 2007):
	Added GcModuleInterfaces that is independent of gcmodule and
	contains definitions needed by SegGoals and Status." 

Release R4-00e (Fri Mar 16 10:37:56 2007):
	Added functions to get the inverse transformations from local ->
	vehicle -> sensor -> image frames in StereoImageBlob.h


Release R4-00d (Fri Mar 16  7:56:26 2007):
	Fixed the serialize() function in SegGoals.

Release R4-00c (Thu Mar 15 13:12:27 2007):
        Added the header file that provide the interface between tplanner
        and dplanner	 

Release R4-00b (Thu Mar 15 10:16:44 2007):
	Fix for StereoImageBlob accessor functions.

Release R4-00a (Mon Mar 12 22:58:01 2007):
	<* Pleaase insert a release notes entry here. If none, then please
	delete these lines *> 

Release R3-00d (Mon Mar 12  3:59:39 2007):
	reverting order of sensors in the enum so old logs will still work

Release R3-00c (Sun Mar 11 12:11:01 2007):
	Updated MapElementMsg.h to include plot_color and plot_value fields

Release R3-00b (Fri Mar  9 19:31:14 2007):
	Added message types SNocpObstacles and SNocpParams to sn_types.h

Release R3-00a (Fri Mar  9 16:48:11 2007):
	"Moved SegmentTypes into SegGoals struct to avoid conflict with some definition in mplanner. Added ReasonForFailure to SegGoalsStatus."

Release R2-00o (Fri Mar  9 16:38:01 2007):
	Minor changes to ObsMapMsg.h; I removed unnecessary header files and
	cmap row/column definitions.

Release R2-00n (Fri Mar  9 12:21:02 2007):
	Moved MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  
	MapElementMsg still lives in interfaces.  MapElementTalker has also
	been moved from skynettalker to map.  All users of the
	MapElementTalker will need to include map/MapElementTalker.hh and
	map/MapElement.hh instead of skynettalker/MapElementTalker and
	interfaces/MapElement.hh.

Release R2-00m (Tue Mar  6 23:34:06 2007):
  Support for long-range stereo.

Release R2-00l (Sat Mar  3 15:02:33 2007):
  Minor tweak.

Release R2-00k (Sat Mar  3 14:04:19 2007):
	Minor update to Makefile allow compilation of rddfPlanner in trunk

Release R2-00j (Sat Mar  3 13:52:13 2007):
Figured out how to send deltas using the CMapDeltaTalker class; this makes the CostMapMsg interface obsolete. These 
changes reflect that. 

Release R2-00i (Sat Mar  3 11:58:26 2007):
  Change the stereo blob structure to make it more future proof.  Old stereo
  log files will need to be converted to the new format.

Release R2-00g (Fri Mar  2 11:24:45 2007):
	added the CostMapMsg.h header file; this defines the message 
interface that tplanner will use to send map deltas to dplanner; 
originally cmap class was supposed to send deltas across the network and 
apply them but because of unsuccessful attempts to get the 
send/receive-deltas functions working, we've (temporarily) resorted to 
defining our own delta-message which this file is for; if in the event 
that jeremy gillula (or someone else) can help us figure out how to 
correctly use the send/receive deltas functions, this header file will not be 
necessary and can be removed. 

Release R2-00f (Thu Mar  1 19:32:06 2007):
	updated MapElement.hh to work with new frames point2 functions.

Release R2-00e (Thu Feb 22 10:56:18 2007):
  Added another stereo module id.

Release R2-00d (Wed Feb 21 23:39:10 2007):
  Added ObsMapMsg.hh for obstacle map deltas.

Release R2-00c (Wed Feb 21 19:53:54 2007):
Updated MapElement functionality.  

Release R2-00b (Wed Feb 21 13:08:14 2007):
	Added tplanner-dplanner interfaces to sn_types. Modified Status.hh to fix the conflicting problems.

Release R2-00a (Tue Feb 20 15:36:03 2007):
	Moved all the talkers (including StateClient) which made the interfaces module depend on the skynet module to the skynettalker module. Also 
added sn_types and enumstring (moved from the skynet module).

Release R1-00u (Sun Feb 18 21:28:34 2007):
	This release fixes a minor namespace issue in MapElement.hh

Release R1-00t (Thu Feb  8 19:28:10 2007):
	Added MapElement class definition in MapElement.hh which describes
	a single element of the map.   MapElementMsg.h defines a constant sized 
	struct used by the talker and skynet and shouldn't be used directly.   
	MapElementTalker defines a class inherited by other modules to implement 
	sending and/or receiving of MapElements. 
	testSendMapElement and testRecvMapElement show the simplest usage 
	of MapElementTalker.

Release R1-00s (Sun Feb  4 17:35:49 2007):
	Removed duplicated files.

Release R1-00r (Sat Feb  3 22:28:39 2007):
	Performance tweaks for sensnet data logger.

Release R1-00q (Fri Feb  2 23:52:34 2007):
	Added support for universal sensnet data logger.

Release R1-00p (Thu Feb  1 23:10:05 2007):
	Some fixes to Makefile.yam, mainly re-added SensnetTypes.h,
	StereoImageBlob.h, LadarRangeBlob.h, RoadLineMsg.h to the list
	of files to link in include/interfaces.

Release R1-00o (Thu Feb  1 11:37:37 2007):
	Added ifndef statement to ActuatorState to fix the conflicting 
declaration problems in trajfollower.

Release R1-00n (Thu Feb  1  2:22:39 2007):
	Added VehicleTrajectory needed by rddfplanner and trajfollower

Release R1-00m (Wed Jan 31 23:34:44 2007):
	Small changed to StereoRangeBlob.

Release R1-00l (Tue Jan 30  0:04:04 2007):
	Added RoadLine message.

Release R1-00k (Mon Jan 29 22:19:41 2007):
	StereoImgaeBlob.h and SensnetTypes.h disappeared again.  Adding 
	them and LadarRangeBlob.h back in.

Release R1-00j (Mon Jan 29 18:30:29 2007):
        Attempted to re-merge VehicleState changes.

Release R1-00i (Mon Jan 29  8:44:44 2007):
	Re-merged Andrew's changes.  This version has StereoImageBlog.h
	and SensnetTypes.h.  Also fixed a problem in Makefile.yam that
	was causing a bad link (which got in the way of proper releases)

Release R1-00h (Sun Jan 28 18:27:41 2007):
	Fixed bug in RDDFTalker by adding include <strstream>

Release R1-00g (Sun Jan 28 18:01:36 2007):
	Added Sensnet messages (from earlier branch).
	
Release R1-00f (Sat Jan 27 18:03:08 2007):
	Added RDDFTalker to libinterfaces.

Release R1-00e (Wed Jan 24 23:44:50 2007):
	Changed the Makefile so it only generates libinterfaces which 
also contains all the talkers.

Release R1-00d (Wed Jan 24 22:49:13 2007):
	Added interfaces between mplanner and tplanner to libinterfaces.
	Also created libtalker which currently contains SegGoalsTalker.

Release R1-00c (Wed Jan 24 22:05:52 2007):
	Created libinterfaces, with StateClient class as an entry.  Turning
	over control to Nok.

Release R1-00b (Sat Jan 13 20:41:29 2007):
	Added interfaces for vehicle state (VehicleState.hh), actuator state
	(ActuatorState.hh) and actuator commands (ActuatorCommand.hh).  All
	of these interfaces are compatible with the current interfaces used
	under dgc/trunk => you can interoperate with old code (consistent
	across R1-* releasese).

	Also copied over the SkynetContainer class definition so that
	we don't have to update everything in moving to YaM.  The 'asim'
	module uses this class, so it has been tested and working.  Note
	that SkynetContainer is just a shell around the skynet class.  As
	we upgrade Skynet, we can update this container class to make
	the changes transparent.

	Note that interfaces/adrive.h is not interfaces/ActuatorState.hh
	and interfaces/ActuatorCommand.hh.  To access the current state,
	you can use interfaces/VehicleState.hh directly or the (legacy)
	CSkynetContainer class.

Release R1-00a (Wed Jan 10 22:56:05 2007):
	Initial release.  Currently only contains the adrive interface
	definition, which is basically the structure definitions for the 
	data that is sent in adrive command and status packets.  This 
	change is essentially a move of some data from adrive to interfaces.
	If this works right, we should eventually be able to compile the
	simulator (asim) without having to compile the serial library...

	No talkers at this point (adrive sends the structure directly,
	without	any interverning code or classes).

Release R1-00 (Wed Jan 10 16:32:25 2007):
	Created.































































































