/*!
 * \file interfaces.c
 * \brief Dummy file for interfaces library
 *
 * \author Richard Murray
 * \date 21 April 2007
 *
 * This file currently contains no code, but is here so that the
 * interfaces library gets created.  This is needed on some platforms
 * (eg, Mac OSX) to keep the interfaces library from being empty.
 */

void libinterface_dummy_function() {}
