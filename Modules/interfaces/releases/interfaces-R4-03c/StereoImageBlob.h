/* 
 * Desc: SensNet stereo image blob.
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef STEREO_IMAGE_BLOB_H
#define STEREO_IMAGE_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "VehicleState.h"
#include <alice/AliceConstants.h>
#include <stdio.h>
  
/** @file

@brief Stereo image data (left/right rectified images), with some useful accessors.

*/


/// @brief Blob version number
#define STEREO_IMAGE_BLOB_VERSION 0x04
  
/// @brief Maximum image dimensions
#define STEREO_IMAGE_BLOB_MAX_COLS 640

/// @brief Maximum image dimensions
#define STEREO_IMAGE_BLOB_MAX_ROWS 480
  

/// @brief Stereo Camera data
typedef struct _StereoImageCamera
{
  /// Camera calibration data: (cx, cy) is the image center in pixels,
  /// (sx, sy) is the focal length in pixels.
  float cx, cy, sx, sy;

  /// Camera-to-vehicle transformation (homogeneous matrix).
  float sens2veh[4][4];

  /// Vehicle-to-camera transformation (homogeneous matrix).
  float veh2sens[4][4];

  /// Camera gain setting (dB) 
  float gain;

  /// Camera shutter speed (ms)
  float shutter;

  /// Reserved for future use
  uint32_t reserved[16];
  
} __attribute__((packed)) StereoImageCamera;
  
  
/// @brief Stereo image blob.
///
/// Contains a rectified left/right stereo image pair and (optionally)
/// the left disparity image.  Disparity values are scaled up and
/// stored as 16-bit integers.
///
typedef struct _StereoImageBlob
{  
  /// Blob type (must be SENSNET_STEREO_IMAGE_BLOB)
  int32_t blobType;

  /// Version number (must be STEREO_IMAGE_BLOB_VERSION)
  int32_t version;
  
  /// ID for originating sensor.
  int32_t sensorId;

  /// Unique frame id (increased monotonically)
  int32_t frameId;

  /// Image timestamp
  uint64_t timestamp;

  /// Left camera parameters
  StereoImageCamera leftCamera;

  /// Right camera parameters
  StereoImageCamera rightCamera;

  /// Stereo baseline (m).
  float baseline;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];

  /// Local-to-vehicle transformation (homogeneous matrix)
  float loc2veh[4][4];

  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];
  
  /// Image dimensions 
  int32_t cols, rows;

  /// Number of channels in rectified image (1 = grayscale, 3 = RGB)
  int32_t channels;
    
  /// Offset to start of left rectified image data
  uint32_t leftOffset;

  /// Size of left image data 
  uint32_t leftSize;

  /// Offset to start of right rectified image data
  uint32_t rightOffset;

  /// Size of right image data
  uint32_t rightSize;

  /// Disparity scaling factor
  float dispScale;

  /// Offset to start of disparity data
  uint32_t dispOffset;

  /// Size of disparity data
  uint32_t dispSize;

  /// Extra padding to align with page boundaries
  uint8_t padding[460];

  /// Packed data for all images.  This is large enough to contain
  /// left and right rectified images (6 bytes/pixel) and the
  /// disparity image (2 bytes/pixel).
  uint8_t imageData[STEREO_IMAGE_BLOB_MAX_COLS * STEREO_IMAGE_BLOB_MAX_ROWS * 8];
  
} __attribute__((packed)) StereoImageBlob;


/// @brief Get the pixel pointer at the given location in the left image.
static __inline__ 
uint8_t *StereoImageBlobGetLeft(StereoImageBlob *self, int c, int r)
{
  return self->imageData + self->leftOffset + (r * self->cols + c) * self->channels;
}


/// @brief Get the pixel pointer at the given location in the right image.
static __inline__ 
uint8_t *StereoImageBlobGetRight(StereoImageBlob *self, int c, int r)
{
  return self->imageData + self->rightOffset + (r * self->cols + c) * self->channels;
}


/// @brief Get the pixel pointer at the given location in the disparity image.
///
/// To convert to true disparity, divide by dispScale.
static __inline__ 
uint16_t *StereoImageBlobGetDisp(StereoImageBlob *self, int c, int r)
{
  return (uint16_t*) (self->imageData + self->dispOffset) + r * self->cols + c;
}


/// @brief Convert from image frame (column, row, disparity) to sensor
/// frame (x, y, z).  
static  __inline__ 
void StereoImageBlobImageToSensor(StereoImageBlob *self, float c, float r, float d,
                                  float *x, float *y, float *z)
{   
	*z = self->leftCamera.sx / d * self->baseline;
	*x = (c - self->leftCamera.cx) / self->leftCamera.sx * *z;
	*y = (r - self->leftCamera.cy) / self->leftCamera.sy * *z;
  return;
}

/// @brief Convert from image frame (column, row, disparity) to sensor
/// frame (x, y, z).  
static  __inline__ 
void StereoImageBlobImageToSensorRight(StereoImageBlob *self, float c, float r, float d,
                                  float *x, float *y, float *z)
{   
	*z = self->rightCamera.sx / d * self->baseline;
	*x = (c - self->rightCamera.cx) / self->rightCamera.sx * *z;
	*y = (r - self->rightCamera.cy) / self->rightCamera.sy * *z;	
  return;
}


/// @brief Convert from left camera frame to vehicle frame
static  __inline__
void StereoImageBlobSensorToVehicle(StereoImageBlob *self,
                                    float px, float py, float pz,
                                    float *qx, float *qy, float *qz)
{
	*qx = self->leftCamera.sens2veh[0][0]*px + self->leftCamera.sens2veh[0][1]*py +
	    self->leftCamera.sens2veh[0][2]*pz + self->leftCamera.sens2veh[0][3];
	*qy = self->leftCamera.sens2veh[1][0]*px + self->leftCamera.sens2veh[1][1]*py +
	    self->leftCamera.sens2veh[1][2]*pz + self->leftCamera.sens2veh[1][3];
	*qz = self->leftCamera.sens2veh[2][0]*px + self->leftCamera.sens2veh[2][1]*py +
	    self->leftCamera.sens2veh[2][2]*pz + self->leftCamera.sens2veh[2][3];  
  return;
}


/// @brief Convert from right camera frame to vehicle frame
static  __inline__
void StereoImageBlobSensorToVehicleRight(StereoImageBlob *self,
                                    float px, float py, float pz,
					 float *qx, float *qy, float *qz)
{
	*qx = self->rightCamera.sens2veh[0][0]*px + self->rightCamera.sens2veh[0][1]*py +
	    self->rightCamera.sens2veh[0][2]*pz + self->rightCamera.sens2veh[0][3];
	*qy = self->rightCamera.sens2veh[1][0]*px + self->rightCamera.sens2veh[1][1]*py +
	    self->rightCamera.sens2veh[1][2]*pz + self->rightCamera.sens2veh[1][3];
	*qz = self->rightCamera.sens2veh[2][0]*px + self->rightCamera.sens2veh[2][1]*py +
	    self->rightCamera.sens2veh[2][2]*pz + self->rightCamera.sens2veh[2][3];  

  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__
void StereoImageBlobVehicleToLocal(StereoImageBlob *self,
                                   float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->veh2loc[0][0]*px + self->veh2loc[0][1]*py +
        self->veh2loc[0][2]*pz + self->veh2loc[0][3];
  *qy = self->veh2loc[1][0]*px + self->veh2loc[1][1]*py +
        self->veh2loc[1][2]*pz + self->veh2loc[1][3];
  *qz = self->veh2loc[2][0]*px + self->veh2loc[2][1]*py +
        self->veh2loc[2][2]*pz + self->veh2loc[2][3];  
  return;
}


/// @brief Convert from sensor coords (x,y,z) to image coords (col,row,disparity).
static  __inline__ 
void StereoImageBlobSensorToImage(StereoImageBlob *self,
                                  float x, float y, float z, 
                                  float *c, float *r, float *d)
{
	*d =  (self->leftCamera.sx / z  * self->baseline);
	*r =  (y * self->leftCamera.sy / z + self->leftCamera.cy);
	*c =  (x * self->leftCamera.sx / z + self->leftCamera.cx);
  return;
}


/// @brief Convert from sensor coords (x,y,z) to image coords (col,row,disparity).
static  __inline__ 
void StereoImageBlobSensorToImageRight(StereoImageBlob *self,
                                  float x, float y, float z, 
				       float *c, float *r, float *d)
                                   
{
	*d =  (self->rightCamera.sx / z  * self->baseline);
	*r =  (y * self->rightCamera.sy / z + self->rightCamera.cy);
	*c =  (x * self->rightCamera.sx / z + self->rightCamera.cx);
  return;
}


/// @brief Convert from vehicle to sensor frame
static __inline__
void StereoImageBlobVehicleToSensor(StereoImageBlob *self,
                                    float px, float py, float pz,
                                    float *qx, float *qy, float *qz)
{
	*qx = self->leftCamera.veh2sens[0][0]*px + self->leftCamera.veh2sens[0][1]*py +
	    self->leftCamera.veh2sens[0][2]*pz + self->leftCamera.veh2sens[0][3];
	*qy = self->leftCamera.veh2sens[1][0]*px + self->leftCamera.veh2sens[1][1]*py +
	    self->leftCamera.veh2sens[1][2]*pz + self->leftCamera.veh2sens[1][3];
	*qz = self->leftCamera.veh2sens[2][0]*px + self->leftCamera.veh2sens[2][1]*py +
	    self->leftCamera.veh2sens[2][2]*pz + self->leftCamera.veh2sens[2][3];  
  return;
}

/// @brief Convert from vehicle to sensor frame
static __inline__
void StereoImageBlobVehicleToSensorRight(StereoImageBlob *self,
                                    float px, float py, float pz,
					 float *qx, float *qy, float *qz)
{
	*qx = self->rightCamera.veh2sens[0][0]*px + self->rightCamera.veh2sens[0][1]*py +
	    self->rightCamera.veh2sens[0][2]*pz + self->rightCamera.veh2sens[0][3];
	*qy = self->rightCamera.veh2sens[1][0]*px + self->rightCamera.veh2sens[1][1]*py +
	    self->rightCamera.veh2sens[1][2]*pz + self->rightCamera.veh2sens[1][3];
	*qz = self->rightCamera.veh2sens[2][0]*px + self->rightCamera.veh2sens[2][1]*py +
	    self->rightCamera.veh2sens[2][2]*pz + self->rightCamera.veh2sens[2][3];  
  return;
}


/// @brief Convert from vehicle to sensor frame
static __inline__
void StereoImageBlobLocalToVehicle(StereoImageBlob *self,
                                   float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->loc2veh[0][0]*px + self->loc2veh[0][1]*py +
        self->loc2veh[0][2]*pz + self->loc2veh[0][3];
  *qy = self->loc2veh[1][0]*px + self->loc2veh[1][1]*py +
        self->loc2veh[1][2]*pz + self->loc2veh[1][3];
  *qz = self->loc2veh[2][0]*px + self->loc2veh[2][1]*py +
        self->loc2veh[2][2]*pz + self->loc2veh[2][3];  
  return;
}


/// @brief Convert from image to local frame
static __inline__
int StereoImageBlobImageToLocal(StereoImageBlob *self,
				 float pi, float pj, 
				 float *px, float *py, 
				 float *pz)
{
 
    //get disparity
    uint16_t d = *StereoImageBlobGetDisp(self, (int) pi, (int) pj);
    //check if valid disparity
    if (d == (uint16_t)0xFFFF || !d)
	return 0;

    float pd = (float) d;
    pd /= self->dispScale;
    // Compute point in the sensor frame
    StereoImageBlobImageToSensor(self, pi, pj, pd, px, py, pz);
    // Convert to vehicle frame
    StereoImageBlobSensorToVehicle(self, *px,*py, *pz, px, py, pz);
    // Convert to local frame
    StereoImageBlobVehicleToLocal(self, *px, *py, *pz, px, py, pz);      

    return 1;
}

/// @brief Convert from image to local frame
static __inline__
int StereoImageBlobImageToLocalRight(StereoImageBlob *self,
				 float pi, float pj, 
				 float *px, float *py, 
				 float *pz)
{
 
    //get disparity
    uint16_t d = *StereoImageBlobGetDisp(self, (int) pi, (int) pj);
    //check if valid disparity
    if (d == (uint16_t)0xFFFF || !d)
	return 0;

    float pd = (float) d;
    pd /= self->dispScale;
    // Compute point in the sensor frame
    StereoImageBlobImageToSensorRight(self, pi, pj, pd, px, py, pz);
    // Convert to vehicle frame
    StereoImageBlobSensorToVehicleRight(self, *px,*py, *pz, px, py, pz);
    // Convert to local frame
    StereoImageBlobVehicleToLocal(self, *px, *py, *pz, px, py, pz);      

    return 1;
}


/// @brief Convert from image to local frame assuming ground plane
static __inline__
int StereoImageBlobImageToLocalPlaneRight(StereoImageBlob *self,
					  float pi, float pj, 
					  float *px, float *py, 
					  float *pz)
{
    // Compute the ray/z-plane intersection in camera coordinates;
    // the matrix m specifies the camera pose relative to the plane.
    *px = (pi - self->rightCamera.cx) / self->rightCamera.sx;
    *py = (pj - self->rightCamera.cy) / self->rightCamera.sy;
    *pz = -(self->rightCamera.sens2veh[2][3] - VEHICLE_TIRE_RADIUS) / 
	(self->rightCamera.sens2veh[2][0] * *px + 
	 self->rightCamera.sens2veh[2][1] * *py + 
	 self->rightCamera.sens2veh[2][2]);

    if (*pz<0)
	return 0;


    // Convert to vehicle frame
    StereoImageBlobSensorToVehicleRight(self, *px,*py, *pz, px, py, pz);
    // Convert to local frame
    StereoImageBlobVehicleToLocal(self, *px, *py, *pz, px, py, pz);      
    
    return 1;

}


/// @brief Convert from image to local frame assuming ground plane
static __inline__
int StereoImageBlobImageToLocalPlane(StereoImageBlob *self,
					  float pi, float pj, 
					  float *px, float *py, 
					  float *pz)
{

    // Compute the ray/z-plane intersection in camera coordinates;
    // the matrix m specifies the camera pose relative to the plane.
    float x = (pi - self->leftCamera.cx) / self->leftCamera.sx;
    float y = (pj - self->leftCamera.cy) / self->leftCamera.sy;
    float z = 1 * self->leftCamera.sx;
    //get in vehicle frame
    StereoImageBlobSensorToVehicle(self, x, y, z, &x, &y, &z);
    fprintf(stderr, "x=%f, y=%f, z=%f\n", x, y, z);
    //now: p1=(x,y,z) & p2=(m03, m13, m23)
    float t = (VEHICLE_TIRE_RADIUS - 
	       self->leftCamera.sens2veh[2][3]) / z;
    *px = self->leftCamera.sens2veh[0][3] + t * x;
    *py = self->leftCamera.sens2veh[1][3] + t * y;
    *pz = VEHICLE_TIRE_RADIUS;
    
/*     *pz = -(self->leftCamera.sens2veh[2][3] - VEHICLE_TIRE_RADIUS) /  */
/* 	(self->leftCamera.sens2veh[2][0] * *px +  */
/* 	 self->leftCamera.sens2veh[2][1] * *py +  */
/* 	 self->leftCamera.sens2veh[2][2]); */
/*     if (*pz<0) */
/* 	return 0; */
/*     float t = (VEHICLE_TIRE_RADIUS - self->leftCamera.sens2veh[2][3]) / */
/* 	self->leftCamera.sens2veh[2][2]; */
/*     *py = self->leftCamera.sens2veh[0][3] + t * x * */
/* 	self->leftCamera.sens2veh[2][0]; */
/*     *px = self->leftCamera.sens2veh[1][3] + t * y * */
/* 	self->leftCamera.sens2veh[2][1]; */
/*     *pz = VEHICLE_TIRE_RADIUS; */
	

    fprintf(stderr, "px=%f, py=%f, pz=%f\n", *px, *py, *pz);

/*     // Convert to vehicle frame */
/*     StereoImageBlobSensorToVehicle(self, *px,*py, *pz, px, py, pz); */
/*     fprintf(stderr, "px=%f, py=%f, pz=%f\n", *px, *py, *pz); */

    // Convert to local frame
    StereoImageBlobVehicleToLocal(self, *px, *py, *pz, px, py, pz);      
    fprintf(stderr, "px=%f, py=%f, pz=%f\n", *px, *py, *pz);
    
    return 1;

}

#ifdef __cplusplus
}
#endif

#endif
