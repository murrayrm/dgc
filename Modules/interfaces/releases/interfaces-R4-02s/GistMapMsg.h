/* 
 * Desc: Gist Map message type
 * Date: 23 July 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/


#ifndef GISTMAP_MSG_H
#define GISTMAP_MSG_H

#include <stdint.h>

enum PlanningMode{
  LEFT_TURN,
  RIGHT_TURN,
  LEFT_LANE_CHANGE,
  RIGHT_LANE_CHANGE,
  FOLLOWING,
  U_TURN,
  CRUISE,
  NO_MODE,
};

#endif
