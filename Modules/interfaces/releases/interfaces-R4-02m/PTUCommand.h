#ifndef _PTUCOMMAND_H_
#define _PTUCOMMAND_H_


// Two ways to command the PTU:
// -- 1) issue a pose in local coordinates for the line of site
enum PTUCommandType{
  LINEOFSITE, //line of site
  RAW // raw pan-tilt commands
  };

typedef struct PTUCommand
{
  PTUCommandType type;
  float localx;
  float localy;
  float localz;
  float pan;
  float tilt;
  float panspeed; //these fields currently aren't being used
  float tiltspeed; //these fields currently aren't being used
};

#endif
