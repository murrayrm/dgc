#ifndef _VEHICLECAPABILITY_H_
#define _VEHICLECAPABILITY_H_

/*! Vehicle Capability message interface
 *  Author:         Chris Schantz
 *  Date:           July 10th 2007
 *
 *  Desctiption:    These unsigned ints are filed in on a scale of 0-5
 *                  With 5 being full capability and 0 being no capability for the maneuver in question
 */
 
typedef struct VehiclePrecisionPlus
{
    
  unsigned long long timestamp;
    
  unsigned int intersectionRightTurn;
  unsigned int intersectionLeftTurn;
  unsigned int intersectionStraightTurn;
  
  unsigned int uturn;
  
  unsigned int nominalDriving;
  unsigned int nominalStopping;
  unsigned int nominalZoneRegionDriving;
  unsigned int nominalNewRegionDriving;
  
  template <class Archive>
  void serialize(Archive &ar,const unsigned int version)
    {
      ar & timestamp;
    
      ar & intersectionRightTurn;
      ar & intersectionLeftTurn;
      ar & intersectionStraightTurn;
      
      ar & uturn;
      
      ar & nominalDriving;
      ar & nominalStopping;
      ar & nominalZoneRegionDriving;
      ar & nominalNewRegionDriving;
      
    }

};

#endif

