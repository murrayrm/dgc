
/* 
 * Desc: SensNet PTU state blob
 * Date: 20 May 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/

#ifndef PTU_STATE_BLOB_H
#define PTU_STATE_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <math.h>
  
#include "VehicleState.h"

  
/// @brief Blob version number
#define PTU_STATE_BLOB_VERSION 0x01  

///
typedef struct _PTUStateBlob
{  
  /// Blob type (must be SENSNET_PTU_STATE_BLOB)
  int blobType;
  
  /// Version number (must be PTU_STATE_BLOB_VERSION)
  int32_t version;

  /// Sensor ID for originating sensor
  int sensorId;

  /// Scan id
  int scanId;

  /// Image timestamp
  uint64_t timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Sensor-to-PTU transformation (homogeneous matrix); 
  //  --- the sensor in this case is the tool frame of the PTU
  //  --- the PTU in this case is the stationary frame of the PTU
  float sens2ptu[4][4];

  /// PTU-to-Sensor transformation (homogeneous matrix);
  //  --- the sensor in this case is the tool frame of the PTU
  //  --- the PTU in this case is the stationary frame of the PTU
  float ptu2sens[4][4];
  
  /// Vehicle-to-PTU transformation (homogeneous matrix);
  //  --- the PTU in this case is the stationary frame of the PTU
  float veh2ptu[4][4];

  /// PTU-to-Vehicle transformation (homogeneous matrix);
  //  --- the PTU in this case is the stationary frame of the PTU
  float ptu2veh[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix); 
  float veh2loc[4][4];

  /// Local-to-vehicle transformation (homogeneous matrix)
  float loc2veh[4][4];

  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];

  // Pan and tilt Angles 
  short currpan;
  short currtilt;
  short currpanspeed;
  short currtiltspeed;

  /// Padding
  uint8_t padding[220];
  
} __attribute__((packed)) PTUStateBlob;


#ifdef __cplusplus
}
#endif


#endif
