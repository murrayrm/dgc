/**********************************************************
 **
 **  MAPELEMENTMSG.H
 **
 **    Time-stamp: <2007-04-27 19:10:39 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb  6 18:45:42 2007
 **
 **
 **********************************************************
 **
 ** Struct representation of MapElement class
 ** this is statically sized and used for skynet 
 ** only.   
 **
 **********************************************************/


#ifndef MAPELEMENTMSG_H
#define MAPELEMENTMSG_H

#include "interfaces/VehicleState.h"

using namespace std;

#define MAP_ELEMENT_NUMPTS 2048
#define MAP_ELEMENT_ID_LENGTH 10
#define MAP_ELEMENT_LABEL_LENGTH 50
#define MAP_ELEMENT_LABEL_NUMLINES 5

typedef struct
{
  double x,y;
  double max_var, min_var, axis;

} __attribute__((packed)) point2_uncertain_struct;
    
  


typedef struct
{
  int subgroup;
  int num_ids;
  int id[MAP_ELEMENT_ID_LENGTH]; 
  double conf;

  int type;
  double type_conf;

  int center_type;
  point2_uncertain_struct center;

  double length;  
  double width;
  double orientation;

  double length_var;
  double width_var;
  double orientation_var;

    
  double height;
  double height_var;

  point2_uncertain_struct velocity;


  int frame_type;
  VehicleState state;
  
  int plot_color;
  int plot_value;


  int num_lines;
  char label[MAP_ELEMENT_LABEL_NUMLINES][MAP_ELEMENT_LABEL_LENGTH];

  int num_pts;
  int geometry_type;
  point2_uncertain_struct geometry[MAP_ELEMENT_NUMPTS];

 
 } __attribute__((packed)) MapElementMsg;

#endif
