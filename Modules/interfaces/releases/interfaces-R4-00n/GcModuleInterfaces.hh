
#ifndef __GC_MODULE_INTERFACES_HH__FFIJ87GY4HFTO734JDGH9327H4FRGH__
#define __GC_MODULE_INTERFACES_HH__FFIJ87GY4HFTO734JDGH9327H4FRGH__

/*!  Dirctives to be tranmitted from GcModule controller to another 
 * controllers arbiter should derive from this class.
 * They must implement the getDirectiveId(), and the templated serialize
 * function.
 */
#include <boost/archive/binary_oarchive.hpp>

class ITransmissive : public OStreamable {
  public:
    virtual ~ITransmissive() {} 
    virtual unsigned int getDirectiveId() = 0;
    // Cannot have a virtual templated function... 
    // Developers, you must implment this function(remove 'virtual'), 
    // the linker will remind you...
    //template<class ArchiveT>
    //virtual void serialize(ArchiveT, const unsigned int version ) = 0;
};

/*!
 * Status sent back from a GcModule's arbiter to the controlling controler
 * should derive from this class.  They should override the getDirectiveId 
 * (or properly set the id field) and must implement the serialize function
 * to transmit their data.  The serialize function must serialize the base 
 * as instructed below.
 */
class GcInterfaceDirectiveStatus : public ITransmissive
{
  public:
    // must implement this so we can auto-capture and classify 
    // status respnoses
    virtual int          getCustomStatus() = 0;

  public:
    enum DeliveryStatus { NONE, QUEUED, SENT, RECEIVED, ACKED } ;
  public:
    unsigned id;
    DeliveryStatus deliveryStat;

  public:
    GcInterfaceDirectiveStatus() : id(0), deliveryStat(NONE)
    {}
    virtual ~GcInterfaceDirectiveStatus() {}
    unsigned getDirectiveId( )
    {
      return id;
    }

  friend class boost::serialization::access;
  private:
    /* When overriding the serialize function you should make it private, 
     * and allow the boost:serialization::access class to be a friend.
     * You must also include the following line to serialize the parent
     * class:
     *   ar & boost::serialization::base_object<GcInterfaceDireciveStatus>(*this);
     * where GcInterfaceDirectiveStatus is the parent of your class (replace if 
     * subclass futher).
     */
    template< class ArchT >
      void serialize( ArchT & ar, const unsigned version )
      {
        // The base class in this case has no members, and it has no serialize 
        // function, so it cannot be added here.
        //ar & boost::serialization::base_object<ITransmissive>(*this);
        
        ar & id;
        ar & deliveryStat;
      }

};


#endif // __GC_MODULE_INTERFACES_HH__FFIJ87GY4HFTO734JDGH9327H4FRGH__

