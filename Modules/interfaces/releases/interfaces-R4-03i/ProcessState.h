/* Process control messages
 *
 * David Trotz dctrotz@jpl.nasa.gov
 * 10 April 07
 *
 * This file defines various process state messages that a process
 * might provide to the proctl module.
 */

#ifndef PROCESS_STATE_H
#define PROCESS_STATE_H

#include <stdint.h>

/** @file

@brief Messages for controlling and reporting process state (e.g.,
start, stop, logging, etc).

There are two message types:

  -# ProcessRequest messages are sent by the master controller to
     inform processs of key events.

  -# ProcessResponse messages are sent by processs to inform the
     master controller of their status.
     
  -# ProcessHealth messages are sent by the master controller for
  	 updates on overall health of the system.

Note that these are pass-by-value C structures; DO NOT use pointers or
C++ constructs.
     
*/

typedef struct _ProcessRequest
{
  // Originating module id
  int moduleId;
  
  // Time when message was constructed.
  uint64_t timestamp;

  // Quit (1 quit, 0 ignore)
  int quit;

  // Log state (1 enabled, 0 not enabled)
  int enableLog;

} __attribute__((packed)) ProcessRequest;


typedef struct _ProcessResponse
{
  // Originating module id
  int moduleId;
  
  // Time when message was constructed.
  uint64_t timestamp;

  // Size of current log (in kilobytes)
  int logSize;

  // Health Status
  int healthStatus;

} __attribute__((packed)) ProcessResponse;

typedef struct _ProcessHealth
{
	// Overall Health 
	int health;
	
} __attribute__((packed)) ProcessHealth;

#endif // PROCESS_STATE_H
