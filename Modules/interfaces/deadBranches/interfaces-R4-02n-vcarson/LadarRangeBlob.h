/* 
 * Desc: SensNet ladar blob
 * Date: 3 Dec 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef LADAR_RANGE_BLOB_H
#define LADAR_RANGE_BLOB_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <math.h>
  
#include "VehicleState.h"

/** @file

@brief Ladar blob and some useful accessors.

*/

  
/// @brief Blob version number
#define LADAR_BLOB_VERSION 0x04

/// @brief Maximum image dimensions
#define LADAR_BLOB_MAX_POINTS 201
  

/// @brief Ladar scan data.
///
typedef struct _LadarRangeBlob
{  
  /// Blob type (must be SENSNET_LADAR_BLOB)
  int blobType;
  
  /// Version number (must be LADAR_BLOB_VERSION)
  int32_t version;

  /// Sensor ID for originating sensor
  int sensorId;

  /// Scan id
  int scanId;

  /// Image timestamp
  uint64_t timestamp;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Sensor-to-vehicle transformation (homogeneous matrix)
  float sens2veh[4][4];
  
  /// Vehicle-to-sensor transformation (homogeneous matrix).
  float veh2sens[4][4];

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];

  /// Local-to-vehicle transformation (homogeneous matrix)
  float loc2veh[4][4];

  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];

  /// Number of points
  int numPoints;
  
  /// Range data
  float points[LADAR_BLOB_MAX_POINTS][2];

  /// Ladar intensity data
  uint8_t intensities[LADAR_BLOB_MAX_POINTS];
  
  /// Padding
  uint8_t padding[220 - 201];
  
} __attribute__((packed)) LadarRangeBlob;


/// @brief Convert scan value (bearing, range) to (x,y,z) in sensor frame.
static  __inline__
void LadarRangeBlobScanToSensor(LadarRangeBlob *self, float pb, float pr,
                                float *px, float *py, float *pz)
{
  *px = pr * cos(pb);
  *py = pr * sin(pb);
  *pz = 0;  
  return;
}


/// @brief Convert from sensor to vehicle frame
static  __inline__
void LadarRangeBlobSensorToVehicle(LadarRangeBlob *self, float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->sens2veh[0][0]*px + self->sens2veh[0][1]*py +
        self->sens2veh[0][2]*pz + self->sens2veh[0][3];
  *qy = self->sens2veh[1][0]*px + self->sens2veh[1][1]*py +
        self->sens2veh[1][2]*pz + self->sens2veh[1][3];
  *qz = self->sens2veh[2][0]*px + self->sens2veh[2][1]*py +
        self->sens2veh[2][2]*pz + self->sens2veh[2][3];  
  return;
}


/// @brief Convert from vehicle to local frame
static  __inline__
void LadarRangeBlobVehicleToLocal(LadarRangeBlob *self, float px, float py, float pz,
                                  float *qx, float *qy, float *qz)
{
  *qx = self->veh2loc[0][0]*px + self->veh2loc[0][1]*py +
        self->veh2loc[0][2]*pz + self->veh2loc[0][3];
  *qy = self->veh2loc[1][0]*px + self->veh2loc[1][1]*py +
        self->veh2loc[1][2]*pz + self->veh2loc[1][3];
  *qz = self->veh2loc[2][0]*px + self->veh2loc[2][1]*py +
        self->veh2loc[2][2]*pz + self->veh2loc[2][3];  
  return;
}


/// @brief Convert from vehicle to sensor frame
static  __inline__
void LadarRangeBlobVehicleToSensor(LadarRangeBlob *self, float px, float py, float pz,
                                   float *qx, float *qy, float *qz)
{
  *qx = self->veh2sens[0][0]*px + self->veh2sens[0][1]*py +
        self->veh2sens[0][2]*pz + self->veh2sens[0][3];
  *qy = self->veh2sens[1][0]*px + self->veh2sens[1][1]*py +
        self->veh2sens[1][2]*pz + self->veh2sens[1][3];
  *qz = self->veh2sens[2][0]*px + self->veh2sens[2][1]*py +
        self->veh2sens[2][2]*pz + self->veh2sens[2][3];  
  return;
}


/// @brief Convert from local to vehicle frame
static  __inline__
void LadarRangeBlobLocalToVehicle(LadarRangeBlob *self, float px, float py, float pz,
                                  float *qx, float *qy, float *qz)
{
  *qx = self->loc2veh[0][0]*px + self->loc2veh[0][1]*py +
        self->loc2veh[0][2]*pz + self->loc2veh[0][3];
  *qy = self->loc2veh[1][0]*px + self->loc2veh[1][1]*py +
        self->loc2veh[1][2]*pz + self->loc2veh[1][3];
  *qz = self->loc2veh[2][0]*px + self->loc2veh[2][1]*py +
        self->loc2veh[2][2]*pz + self->loc2veh[2][3];  
  return;
}


#ifdef __cplusplus
}
#endif


#endif
