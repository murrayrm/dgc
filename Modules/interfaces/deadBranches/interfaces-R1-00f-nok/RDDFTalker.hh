/*!**
 * Lars Cremean and Nok Wongpiromsarn
 * December 8, 2006
 */

#ifndef _RDDFTALKER_HH_
#define _RDDFTALKER_HH_

#include <unistd.h>
#include <list>
#include "SkynetContainer.hh"
#include "dgcutils/RDDF.hh"
#include <pthread.h>
#include "dgcutils/DGCutils.hh"
#include <strstream>

#include <string>
using namespace std;

/** The maximum size of the data that can be sent across skynet for 
 * sending an RDDF.  Units of bytes. */
#define RDDF_DATA_PACKAGE_SIZE 10000

class CRDDFTalker : virtual public CSkynetContainer
{
  pthread_mutex_t m_RDDFDataBufferMutex;
  pthread_mutex_t m_ReceivedRDDFMutex;
  
  /** character buffer that matches the RDDF format */
  char* m_pRDDFDataBuffer;
  
  RDDF m_receivedRddf;
  DGCcondition condNewRDDF;
  bool m_bRunGetRDDFThreads;

  /*! The function which continually updates the RDDF when any is received from
    the network */
  void getRDDFThread(void*);
  
protected:
  /*! The RDDF itself. This is the copy of the RDDF that's coming
  in. This is visible from the derived classes */
  RDDF m_newRddf;

public:
  CRDDFTalker();	// Default constructor. Do not run getRDDFThread.
  CRDDFTalker(bool runGetRDDFThread);
  CRDDFTalker(bool runGetRDDFThread, int rddfSocket);
  ~CRDDFTalker();
  
  bool SendRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex = NULL);
  
  bool RecvRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex = NULL, 
                string* pOutstring = NULL);
                
  /*! Check for new RDDF */
  bool NewRDDF();
  
  /*! Give me the latest RDDF (a copy) and reset new flag */
  void UpdateRDDF();
  
  /*! use conditional mutexes to wait until RDDF is received */
  void WaitForRDDF();
  
private:
  bool loadStream(istrstream& inputstream, RDDF* pRddf);
};

#endif // _RDDFTALKER_HH_
