/* 
 * Desc: Cost Map message type.
 * Date: 01 March 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/


#ifndef COSTMAP_MSG_H
#define COSTMAP_MSG_H

#include <stdint.h>
#include "interfaces/VehicleState.h"

struct CorrCost
{
  double cost;
  double Northing;
  double Easting;
  unsigned long long timestamp;
};


/// @brief Skynet message with obstacle map data.
typedef struct
{

  /// Vehicle state data
  VehicleState state;

  int numDeltas;

  // vector of map deltas
  CorrCost deltaBuffer[500*500];
  
} CostMapMsg;

#endif
