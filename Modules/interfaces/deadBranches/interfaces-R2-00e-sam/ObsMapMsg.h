/* 
 * Desc: Obstacle Map message type.
 * Date: 09 February 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/


#ifndef OBSMAP_MSG_H
#define OBSMAP_MSG_H

#include <stdint.h>
#include "VehicleState.h"

// TODO: remove all of these; they do not belong in an interface file
// and are C++ files to boot.
// REMOVE #include <vector>
// REMOVE #include <iostream>
// REMOVE #include  <iomanip>
// REMOVE #include <stdlib.h>

#include <frames/coords.hh> // SHOULD PROBABLY REMOVE THIS TOO

// TODO: this doesnt belong here.
// The message does not care what size the map is.
#define NUM_ROWS  500
#define NUM_COLS  500
#define RES_ROWS  0.40
#define RES_COLS  0.40


// TODO: Debatable whether this needs to be here.  Why would we send
// cells that are empty or outside the map?
enum CELL_TYPE{
  OUTSIDE_MAP = 0,
  EMPTY = 1,
  DATA = 2
};

struct ElevationData
{
  double meanElevation;
  double stdDev;
  NEDcoord UTMPoint;
  CELL_TYPE cellType; 
};

/// @brief Skynet message with obstacle map data.
typedef struct
{
  /// Skynet message type (must be SNfusionmapdelta)
  int msg_type;

  /// Vehicle state data
  VehicleState state;

  int numDeltas;

  // vector of map deltas
  ElevationData mapDeltaVec[500];
  
} __attribute__((packed)) ObsMapMsg;

#endif
