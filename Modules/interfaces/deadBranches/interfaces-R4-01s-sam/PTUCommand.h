#ifndef _PTUCOMMAND_H_
#define _PTUCOMMAND_H_

typedef struct PTUCommand
{
  float pan;
  float tilt;
  float panspeed;
  float tiltspeed;
};

#endif
