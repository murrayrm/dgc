/* astate.h - platform-independent state struct
 *
 * Lars Cremean
 * 6 Jan 05
 * 
 * This file defines platform-independent state structs intended for use by 
 * various code that constructs, sends or receives state information.
 */

#ifndef VEHICLESTATE_H
#define VEHICLESTATE_H

#ifdef __cplusplus
extern "C"
{
#endif


/** 
 * New, clean struct for vehicle state representation.  Originally
 * implemented as GetVehicleStateMsg in bob/vehlib/VState.hh, but
 * moved here because it is platform-independent.  All values use the
 * center of the rear axle as the origin of the vehicle frame unless
 * otherwise stated. 
 */
typedef struct _VehicleState
{
  /** Timestamp represents UNIX-time. This is the value of microseconds since
   *  the epoch (1/1/1970) */
  unsigned long long timestamp;
  
  /** Linear velocities in vehicle frame (m/sec). */
  double vehXVel, vehYVel, vehZVel;

  /** Angular velocities in vehicle frame (rad/sec). */
  double vehRollRate, vehPitchRate, vehYawRate;

  /** Position in local frame (m); +z is down. */
  double localX, localY, localZ;

  /** Rotation in local frame (radians); these are Euler angles with
      yaw applied last. */
  double localRoll, localPitch, localYaw;

  /** Linear velocity of the vehicle in the local frame (m/sec). */
  double localXVel, localYVel, localZVel;

  /** Angular velocity of the vehicle in the local frame (radians/sec). */
  double localRollRate, localPitchRate, localYawRate;

  /** Vehicle UTM zone (e.g., 11S for Southern California) */
  int utmZone, utmLetter;

  /** Position of the vehicle in the UTM frame (meters).  To convert
      these to a global cartesian frame, use: (x = northing, y =
      easting, z = altitude).  Note that the altitude is positive
      /downward/ and zero at sea-level. */
  double utmNorthing, utmEasting, utmAltitude;

  /** Rotation of the vehicle in the UTM frame (radians); these
      are Euler angles with yaw applied last.  Yaw can also be
      interpreted as a conventional compass heading, i.e., yaw = 0 if
      the vehicle is facing due north, yaw = pi/2 if the vehicle is
      facing due east, etc. */
  double utmRoll, utmPitch, utmYaw;

  /** Linear velocity of the vehicle in the UTM frame (m/sec). */
  double utmNorthVel, utmEastVel, utmAltitudeVel;

  /** Angular velocity of the vehicle in the UTM frame (radians/sec). */
  double utmRollRate, utmPitchRate, utmYawRate;

  /** Confidence intervals on vehicle position in UTM frame.
      What units are these measured in? */
  double utmNorthConfidence, utmEastConfidence, utmAltitudeConfidence;

  /** This is a unitless measure of the goodness of the GPS data.
      What is the range? */
  double gpsGamma;

  /* The following variables are deprecated */

  double GPS_Northing_deprecated;
  double GPS_Easting_deprecated;

  /** Component of linear acceleration in the Northing direction (m/s/s).  */
  double Acc_N_deprecated;

  /** Component of linear acceleration in the Easting direction (m/s/s).  */
  double Acc_E_deprecated;

  /** Component of linear acceleration in the downward direction (m/s/s).  */
  double Acc_D_deprecated;

  // Old variables to preserve stucture
  double raw_YawRate_deprecated;
  double RollAcc_deprecated, PitchAcc_deprecated, YawAcc_deprecated;
  
  /* DEPRECATED?
  double northConfidence;		//<! Confidence level in Northing
  double eastConfidence;		//<! Confidence level in Easting
  double altitudeConfidence;		//<! Confidence level in Altitude
  double rollConfidence;		//<! Confidence level in Roll
  double pitchConfidence;		//<! Confidence level in Pitch
  double yawConfidence;			//<! Confidence level in Yaw
  */

} __attribute__((packed)) VehicleState;

  
#ifdef __cplusplus
}
#endif

#endif
