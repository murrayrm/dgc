/* astate.h - platform-independent state struct
 *
 * Lars Cremean
 * 6 Jan 05
 * 
 * This file defines platform-independent state structs intended for use by 
 * various code that constructs, sends or receives state information.
 */

#ifndef VEHICLESTATE_HH
#define VEHICLESTATE_HH

/** 
 * New, clean struct for vehicle state representation.  Originally
 * implemented as GetVehicleStateMsg in bob/vehlib/VState.hh, but
 * moved here because it is platform-independent.  All values use the
 * center of the rear axle as the origin of the vehicle frame unless
 * otherwise stated. 
 */
struct VehicleState
{
  /** Timestamp represents UNIX-time. This is the value of microseconds since
   *  the epoch (1/1/1970) */
  unsigned long long Timestamp;

  /** Cartesian UTM Northing coordinate for UTM zone 11S (meters).
   * This is the X-axis in our coordinate system conventions, and
   * corresponds to latitude. */
  double Northing;

  /** Cartesian UTM Easting coordinate for UTM zone 11S (meters).
   * This is the Y-axis in our coordinate system conventions, and
   * corresponds to longitude. */
  double Easting;

  /** "Altitude" is the Z-axis corresponding to the global coordinate frame.  
   * It is positive /downward/ and zero at sea-level, in meters. */
  double Altitude;

  /* The following variables are depricated */
  double GPS_Northing_deprecated;
  double GPS_Easting_deprecated;

  /** Component of linear velocity in the Northing direction (m/s).  */
  double Vel_N;

  /** Component of linear velocity in the Easting direction (m/s).  */
  double Vel_E;

  /** Component of linear velocity in the downward direction (m/s).  */
  double Vel_D;

  /** Component of linear acceleration in the Northing direction (m/s/s).  */
  double Acc_N_deprecated;

  /** Component of linear acceleration in the Easting direction (m/s/s).  */
  double Acc_E_deprecated;

  /** Component of linear acceleration in the downward direction (m/s/s).  */
  double Acc_D_deprecated;

  /** Rotation of the vehicle about the X-axis, positive according the 
   * right-hand rule (positive roll is top-right) (radians).  */
  double Roll;

  /** Rotation of the vehicle about the vehicle Y-axis, positive according the 
   * right-hand rule (positive nose-up) (radians). */
  double Pitch;

  /** Rotation of the vehicle about the Z-axis, measured clockwise
   * from North.  i.e. East is Yaw=pi/2 (radians). Note that if you
   * orient your coordinate system with Northing as x and Easting as
   * y, this becomes the standard angle measurement that starts at the
   * x axis and goes counterclockwise.  */
  double Yaw;

  /** RollRate is d(Roll)/dt (rad/s). */
  double RollRate;

  /** PitchRate is d(Pitch)/dt (rad/s). */
  double PitchRate;

  /** YawRate is d(Yaw)/dt (rad/s). */
  double YawRate;

  // Old variables to preserve stucture
  double raw_YawRate_deprecated;
  double RollAcc_deprecated, PitchAcc_deprecated, YawAcc_deprecated;

  double northConfidence;		//<! Confidence level in Northing
  double eastConfidence;		//<! Confidence level in Easting
  double altitudeConfidence;		//<! Confidence level in Altitude
  double rollConfidence;		//<! Confidence level in Roll
  double pitchConfidence;		//<! Confidence level in Pitch
  double yawConfidence;			//<! Confidence level in Yaw

  //This is a unitless measure of the goodness of the GPS data
  double gamma;

  /** Position in local frame (m).  Note that +z is down. */
  double localX, localY, localZ;

  /** Attitide in local frame (radians).  These are Euler angles, with
      yaw applied last. */
  double localRoll, localPitch, localYaw;

  /** Linear velocities in vehicle frame (m/sec). */
  double vehicleVelX, vehicleVelY, vehicleVelZ;

  /** Angular velocities in vehicle frame (rad/sec). */
  double vehicleVelRoll, vehicleVelPitch, vehicleVelYaw;
}  __attribute__((packed)) ;;;

#endif
