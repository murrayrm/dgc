#ifndef ENUMSTRING_H
#define ENUMSTRING_H

#include <string.h>
/* Note:  the following is modified GnuCash code 
 * so I suppose it's technically covered by the GPL
 */
#define ENUM_BODY(name, value)             \
   name value ,
#define AS_STRING_CASE(name, value)        \
   case name: return #name;
#define FROM_STRING_CASE(name, value)      \
   if (strcmp(str, #name) == 0) {          \
     return name;                          \
   }
#define DEFINE_ENUM(name, list)            \
   typedef enum {                          \
     list(ENUM_BODY)                       \
   }name;                                  \
   static const char* name ## _asString(name n) __attribute__((unused));\
   static const char* name ## _asString(name n){    \
     switch (n) {                          \
       list(AS_STRING_CASE)                \
       default: return "";                 \
     }                                     \
   }                                       \
   static name name ## fromString(const char* str) __attribute__((unused));\
   static name name ## fromString(const char* str) {      \
     list(FROM_STRING_CASE)                \
     return (name)-1; /* assert? throw? */        \
   }                                       
/* USAGE EXAMPLE:
 
#define ENUM_LIST(_) \
  _(RED, =  0)  \
  _(GREEN, )    \
  _(BLUE,  )
DEFINE_ENUM(Color, ENUM_LIST)

int main() {
    Color c = GREEN;
    printf("%d\n",c);
    printf("%s\n", asString(c));
    printf("%d\n", fromString("BLUE"));
}
*/
#endif
