
/* Desc: Simple test for SensnetLog library
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "SensnetLog.h"


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Test blob data
typedef struct
{
  int data[1024];
  
} __attribute__((packed)) TestBlob;


uint64_t gettime()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (uint64_t) tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Write a dummy log.  The log is filled with random data.
int writeLog(const char *filename, int seed)
{
  SensnetLog *log;
  SensnetLogHeader header;
  TestBlob blob;
  int i, j;

  MSG("writing %s", filename);
  
  srand(seed);
    
  log = SensnetLogAlloc();
  assert(log);

  header.timestamp = gettime();

  if (SensnetLogOpenWrite(log, filename, &header) != 0)
    return -1;

  for (i = 0; i < 100000; i++)
  {
    // Generate random data
    for (j = 0; j < sizeof(blob.data) / sizeof(blob.data[0]); j++)
      blob.data[j] = rand();

    // Write data
    if (SensnetLogWrite(log, 0, gettime(), sizeof(blob), &blob) != 0)
      return -1;
  }
  
  SensnetLogClose(log);
  SensnetLogFree(log);
  
  return 0;
}


// Read a dummy log.  The log is filled with random data.
int readLog(const char *filename, int seed)
{
  SensnetLog *log;
  SensnetLogHeader header;
  TestBlob blob;
  int i, j;

  MSG("reading %s", filename);
  
  srand(seed);
    
  log = SensnetLogAlloc();
  assert(log);

  header.timestamp = gettime();

  if (SensnetLogOpenRead(log, filename, &header) != 0)
    return -1;

  for (i = 0; i < 100000; i++)
  {
    // Read data
    if (SensnetLogRead(log, NULL, NULL, sizeof(blob), &blob) != 0)
      return -1;

    // Check random data
    for (j = 0; j < sizeof(blob.data) / sizeof(blob.data[0]); j++)
    {
      if (blob.data[j] != rand())
        return ERROR("test failed; read/write files differs");
    }
  }
  
  SensnetLogClose(log);
  SensnetLogFree(log);
  
  return 0;
}



// Simple self-test.
int main(int argc, char **argv)
{
  char *filename;

  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  // Write out a random log
  if (writeLog(filename, 4567) != 0)
    return -1;

  // Read the log and make sure it is what we wrote
  if (readLog(filename, 4567) != 0)
    return -1;

  MSG("all tests passed");
  
  return 0;
}

