/* 
 * Desc: Road line message type.
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/


#ifndef ROADLINE_MSG_H
#define ROADLINE_MSG_H

#include <stdint.h>
#include "VehicleState.h"


/// @brief Skynet message with road line data.
///
typedef struct
{
  /// Skynet message type (must be SNroadLine)
  int msg_type;

  /// Image frame id (from the sensor).
  int frameid;

  /// Image timestamp (from the sensor).
  uint64_t timestamp;

  /// Vehicle state data
  VehicleState state;

  /// List of detected lines, represented by their end-points
  /// in the local frame.
  int num_lines;
  struct {float a[3], b[3];} lines[16];
  
} __attribute__((packed)) RoadLineMsg;


#endif
