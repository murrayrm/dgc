/**********************************************************
 **
 **  MAPELEMENTMSG.H
 **
 **    Time-stamp: <2007-05-13 12:41:45 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Feb  6 18:45:42 2007
 **
 **
 **********************************************************
 **
 ** Struct representation of MapElement class
 ** this is statically sized and used for skynet 
 ** only.   
 **
 **********************************************************/


#ifndef MAPELEMENTMSG_H
#define MAPELEMENTMSG_H

#include "interfaces/VehicleState.h"

using namespace std;

#define MAP_ELEMENT_NUMPTS 2048
#define MAP_ELEMENT_ID_LENGTH 10
#define MAP_ELEMENT_LABEL_LENGTH 50
#define MAP_ELEMENT_LABEL_NUMLINES 3

typedef struct
{
  double x,y;
  double max_var, min_var, axis;

} __attribute__((packed)) point2_uncertain_struct;
    
  


typedef struct
{
  int subgroup;
  int numIds;
  int id[MAP_ELEMENT_ID_LENGTH]; 
  double conf;

  int type;
  double typeConf;

  point2_uncertain_struct position;

  point2_uncertain_struct center;

  double length;  
  double width;
  double orientation;

  double lengthVar;
  double widthVar;
  double orientationVar;

    
  double height;
  double heightVar;

  double elevation;
  double elevationVar;


  point2_uncertain_struct velocity;
  double peakSpeed; // m/s
  double peakSpeedVar; // m/s
  double peakAccel; // m/s^2
  double peakAccelVar; // m/s^2
  

  int frameType;
  uint64_t timestamp;
  VehicleState state;
  
  int plotColor;
  int plotValue;


  int numLines;
  char label[MAP_ELEMENT_LABEL_NUMLINES][MAP_ELEMENT_LABEL_LENGTH];


  point2_uncertain_struct geometryMin;
  point2_uncertain_struct geometryMax;

  int numPts;
  int geometryType;
  point2_uncertain_struct geometry[MAP_ELEMENT_NUMPTS];

 
 } __attribute__((packed)) MapElementMsg;

#endif
