/**********************************************************
 **
 **  MAPELEMENT.HH
 **
 **    Time-stamp: <2007-02-08 18:49:18 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:44:48 2007
 **
 **
 **********************************************************
 **
 **  A class to describe a geometric map element
 **
 **********************************************************/


#ifndef MAPELEMENT_HH
#define MAPELEMENT_HH

#include <vector>
#include "interfaces/VehicleState.h"
#include "frames/poscov2.hh"



	
// A class to describe a geometric map element
class MapElement
{
	
public:
	
	// A Constructor 
	MapElement() {}
	
	// A Destructor 
	~MapElement() {}

	// Object id
	int id; 

	// Confidence that the object exists 
	double conf;

	// Object classification
	int type;
	// TODO: Need type list and enumeration

	
	// Type classification confidence bounds
	double type_conf;
	

	// Estimated object center position and covariance
	poscov2 center;
	//
	// poscov2 struct members (defined in frames/poscov2.hh)
	//
	// center.x                x position
	// center.y                y position
	// center.major_var        max variance
	// center.minor_var        min variance
	// center.axis             angle of max variance
	
	
	// Estimated bounding box parameter
	double length;  
	double width;
	double orientation;
	// Not meaningful for all objects need this, but can be useful
	

	// bounding box parameter variance
	double length_var;
	double width_var;
	double orientation_var;


	// Estimated object geometry 
	std::vector<poscov2> geometry;
	// geometry represents object contour
	// -list of ordered connected points for a line 
	// -list of ordered vertices for an obstacle
	// If the geometry is not specified, bounding box info only is used


	// Estimated object height
	double height;
	// height for road markings should be zero

	// Object height variance
	double height_var;
	

	// Object velocity
	poscov2 velocity;
	// The velocity x,y coordinates define a velocity vector
	// for static map elements x,y should be 0 

		
	// Frame of reference
	int frame;
	// All map element coordinates should be given in local frame, 
	// not GPS global or vehicle frame.
	// Shouldn't need this eventually.
	// TODO: define frame enumeration

  /// Vehicle state data when element was detected
  VehicleState state;
	// Not critical but could be useful initially for debugging

};
#endif
