#ifndef __POLYGON_HH__
#define __POLYGON_HH__

#include "Bitmap.hh"

namespace bitmap
{

    enum CombineFunc
    {
        COMB_REPLACE, /* just overwrite the old value with the new  */
        COMB_MAX,     /* take the maximum of the two values */
        COMB_MIN,     /* take the minimum of the two values */
        COMB_ADD,     /* sum the two values */
        COMB_MUL,     /* multiply the two values */
        COMB_SUB,     /* subtract the new value from the old one */
        COMB_DIV,     /* divide the old value by the new one
                         NOTE: if the new value is 0, you'll get a +inf (division by zero).
                         This may or may not be what you want ...
                      */
        COMB_AVG      /* take the average of the two values (that is, sum and divide by 2) */
    };

    /// pointer to function that combines two values, and stores the result in 'dest'.
    typedef void (*combine_f)(cost_t* dest, cost_t* src);

    extern combine_f combineTable[];

    /// The function used to calculate the value to put into the bitmap.
    /// If the selected function is f(x), the actual value is
    ///   b * f(x,a) + c
    /// where a, b and c are the constants defined in the Polygon class.
    enum FillFunc
    {
        FILL_NONE,    /* x, but don't use a, b and c, just copy the value (fastest) */
        FILL_LINEAR,  /* a * x */
        FILL_SQUARE,  /* a * x^2 */
        FILL_EXP,     /* exp(a * x) */
        FILL_EXP2,    /* exp(a * x^2) (gaussian, when a, b, c are set correctly) */
        FILL_COS,     /* cos(a * x) */
        FILL_SIN,     /* sin(a * x) */
        FILL_TAN,     /* tan(a * x) */
        FILL_ATAN     /* atan(a * x) */
    };

    /// pointer to function that calculates some mathematical function.
    typedef cost_t (*fill_f)(cost_t val, cost_t a);

    extern fill_f fillTable[];

    /**
     * A 2D convex polygon (polytope), that can be drawn on a single-channel Bitmap of type cost_.
     * Each vertex of the polygon has a value that will be interpolated linearly, then the
     * final value to be written on the bitmap will be a function of this value.
     */
    class Polygon
    {
        CombineFunc m_comb;
        FillFunc m_fill;
        cost_t m_fa, m_fb, m_fc; // constants used to calculate the value to fill with
        vector<float> m_xv;
        vector<float> m_yv;
        vector<float> m_costVec; // cost

    public:
        Polygon(CombineFunc cf = COMB_MAX, FillFunc fill = FILL_NONE,
                cost_t fa = 1, cost_t fb = 1, cost_t fc = 0)
            : m_comb(cf), m_fill(fill), m_fa(fa), m_fb(fb), m_fc(fc)
        { }

        // these can be uset to read and write the vertices
        vector<float>& getXv() { return m_xv; }
        vector<float>& getYv() { return m_yv; }
        vector<float>& getCostVec() { return m_costVec; }

        // read only versions
        const vector<float>& getXv() const { return m_xv; }
        const vector<float>& getYv() const { return m_yv; }
        const vector<float>& getCostVec() const { return m_costVec; }

        /// Same as:
        /// poly.getXv() = xv;
        /// poly.getYx() = yv;
        void setVertices(const vector<float>& xv, const vector<float>& yv,
                         const vector<float>& cost)
        {
            m_xv = xv;
            m_yv = yv;
            m_costVec = cost;
        }

        void setVertices(const vector<point2>& v, const vector<float>& cost);
        void setVertices(const vector<point2_uncertain>& v, const vector<float>& cost);

        void setVertices(const point2arr& v, const vector<float>& cost)
        {
            setVertices(v.arr, cost);
        }

        void setVertices(const point2arr_uncertain& v, const vector<float>& cost)
        {
            setVertices(v.arr, cost);
        }

        cost_t getA() const { return m_fa; }
        void setA(cost_t a) { m_fa = a; }

        cost_t getB() const { return m_fb; }
        void setB(cost_t b) { m_fb = b; }

        cost_t getC() const { return m_fc; }
        void setC(cost_t c) { m_fc = c; }

        /**
         * Draw the polygon on the specified cost map, interpreting the vertices
         * as they are in local frame.
         * The polygon will be correctly clipped if it's partly or completely
         * outside of the map.
         */
        void draw(CostMap* bmp) const;

        /**
         * Draw the polygon on the specified cost map, interpreting the vertices
         * as they are in map coordinates already (y = row, x = column).
         * The polygon will be correctly clipped if it's partly or completely
         * outside of the map.
         */
        void drawMapCoords(CostMap* bmp) const;

        /**
         * Make this class serializable with boost::serialization.
         */
        template<class Archive>
        void serialize(Archive &ar, const unsigned int /* version */)
        {
            // size: 4 + 4 + 3*4 + 3*4*nVert = 20 + 12*nVert bytes
            ar & m_comb;
            ar & m_fill;
            ar & m_fa & m_fb & m_fc;
            ar & m_xv;
            ar & m_yv;
            ar & m_costVec;
        }
    };

};

#endif
