/**********************************************************
 **
 **  MAPELEMENT.HH
 **
 **    Time-stamp: <2007-02-28 16:48:05 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:44:48 2007
 **
 **
 **********************************************************
 **
 **  A class to describe a geometric map element
 **
 **********************************************************/


#ifndef MAPELEMENT_HH
#define MAPELEMENT_HH

#include <vector>
#include <string>
#include <iostream>
#include "interfaces/VehicleState.h"
#include "frames/point2_uncertain.hh"

using namespace std;

enum MapElementGeometryType {
	GEOMETRY_UNDEF,	
	GEOMETRY_POINTS,
	GEOMETRY_ORDERED_POINTS,
	GEOMETRY_LINE,
	GEOMETRY_EDGE,
	GEOMETRY_POLY
};
enum MapElementCenterType {
	CENTER_UNDEF,	
	CENTER_POINT,
	CENTER_POSE,
	CENTER_BOUND_RADIUS,
	CENTER_BOUND_BOX,
	CENTER_BOUND_LINES
};
		
enum MapElementFrameType {
	FRAME_UNDEF,
	FRAME_LOCAL,
	FRAME_GLOBAL,
	FRAME_VEHICLE
};

enum MapElementType {
	ELEMENT_UNDEF,
	ELEMENT_CLEAR,

	ELEMENT_POINTS,
	ELEMENT_WAYPOINT,
	ELEMENT_CHECKPOINT,

	ELEMENT_LINE,
	ELEMENT_STOPLINE,
	ELEMENT_LANELINE,

	ELEMENT_PARKING_SPOT,
	ELEMENT_PERIMETER,

	ELEMENT_OBSTACLE,	
	ELEMENT_OBSTACLE_EDGE,
	//	ELEMENT_VEHICLE
};



// A class to describe a geometric map element
class MapElement
{
	
public:
	
	// A Constructor 
	MapElement() {clear();}
	
	// A Destructor 
	~MapElement() {}
	
	// Subgroup id. only changed and used by talker;
	int subgroup;

	// Object id
	vector<int> id; 

	// Confidence that the object exists 
	double conf;

	// Object classification
	MapElementType type;
	// TODO: Need type list and enumeration
	
	// Type classification confidence bounds
	double type_conf;
	

	MapElementCenterType center_type;
	// Estimated object center position and covariance
	point2_uncertain center;
	//
	// point2_uncertain struct members (defined in frames/point2_uncertain.hh)
	//
	// center.x                x position
	// center.y                y position
	// center.max_var        max variance
	// center.min_var        min variance
	// center.axis             angle of max variance
	
	
	// Estimated bounding box parameter
	double length;  
	double width;
	double orientation;
	// Not meaningful for all objects, but can be useful
	

	// bounding box parameter variance
	double length_var;
	double width_var;
	double orientation_var;

	MapElementGeometryType geometry_type;
	// Estimated object geometry 
	vector<point2_uncertain> geometry;
	// geometry represents object contour
	// -list of ordered connected points for a line 
	// -list of ordered vertices for an obstacle
	// If the geometry is not specified, bounding box info only is used


	// Estimated object height
	double height;
	// height for road markings should be zero

	// Object height variance
	double height_var;
	

	// Object velocity
	point2_uncertain velocity;
	// The velocity x,y coordinates define a velocity vector
	// for static map elements x,y should be 0 

		
	// Frame of reference
	MapElementFrameType frame_type;

	// All map element coordinates should be given in local frame, 
	// not GPS global or vehicle frame.
	// Shouldn't need this eventually.
	// TODO: define frame enumeration

  /// Vehicle state data when element was detected
  VehicleState state;

	// Text label
	// Not critical but could be useful initially for debugging
	vector<string> label;


	void clear(){
  subgroup = 0;
	id.clear();
	conf = 0;
	type = ELEMENT_UNDEF;
	type_conf = 0;
	center_type = CENTER_UNDEF;
	center.clear();
	length = -1;
	width = -1;
	orientation =0;
	length_var = 0;
	width_var = 0;
	orientation_var = 0;
	geometry_type = GEOMETRY_UNDEF;
	geometry.clear();
	height =  -1;
	height_var = 0;
	velocity.clear();
	frame_type = FRAME_UNDEF;
	memset(&state, 0, sizeof(state));
	label.clear();
}

	void set_circle_obs(vector<int>& ident, point2 cpt, double radius){
		id = ident;
	center.x = cpt.x;
	center.y = cpt.y;
	length = radius;
	width = radius;
	type = ELEMENT_OBSTACLE;
	center_type = CENTER_BOUND_RADIUS;
}

	void set_poly_obs(vector<int>& ident, vector<point2>& ptarr){
	id = ident;
	geometry.clear();
	for (int i = 0; i < (int)ptarr.size(); ++i){
		geometry.push_back(ptarr[i]);
	}
	
	type = ELEMENT_OBSTACLE;
	//center_type = CENTER_BOUND_BOX;
	geometry_type = GEOMETRY_POLY;
	set_center_from_geometry();
}
	void set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid ){
	id = ident;
	center.x = cpt.x;
	center.y = cpt.y;
	orientation = ang;
	length = len;
	width = wid;
	type = ELEMENT_OBSTACLE;
	center_type = CENTER_BOUND_BOX;
	geometry_type = GEOMETRY_UNDEF;
}
	void set_line(vector<int>& ident,  vector<point2>& ptarr){
	id = ident;
	geometry.clear();
	for (int i = 0; i < (int)ptarr.size(); ++i){
		geometry.push_back(ptarr[i]);
	}	
	type = ELEMENT_LINE;
	geometry_type = GEOMETRY_LINE;
	set_center_from_geometry();
}
	void set_stopline(vector<int>& ident, vector<point2>& ptarr){
	id = ident;
	geometry.clear();
	for (int i = 0; i < (int)ptarr.size(); ++i){
		geometry.push_back(ptarr[i]);
	}	
	type = ELEMENT_STOPLINE;
	geometry_type = GEOMETRY_LINE;
	set_center_from_geometry();
}
	void set_laneline(vector<int>& ident, vector<point2>& ptarr){
	id = ident;
	geometry.clear();
	for (int i = 0; i < (int)ptarr.size(); ++i){
		geometry.push_back(ptarr[i]);
	}	
	type = ELEMENT_LANELINE;
	geometry_type = GEOMETRY_LINE;
	set_center_from_geometry();

}
	void set_points(vector<int>& ident, vector<point2>& ptarr){
	id = ident;
	geometry.clear();
	for (int i = 0; i < (int)ptarr.size(); ++i){
		geometry.push_back(ptarr[i]);
	}	
	type = ELEMENT_POINTS;
	geometry_type = GEOMETRY_POINTS;
	set_center_from_geometry();

}
	void print() const
{
	unsigned int i;
	cout << "------Start Printing MapElement------" << endl;
	cout << "subgroup= " << subgroup << endl;
	cout << "id= " ;
	for (i = 0; i < id.size(); ++i){
		cout << id[i] << "  ";
	}
	cout << " conv= " << conf 
			 << " type= " << type
			 << " type_conf= " << type_conf << endl;
	cout << "center:  " ;
	cout << "center_type= " << center_type <<endl;
	center.print();
	cout << "length= " << length
			 << " width= " << width
			 << " orientation= " << orientation << endl;
	cout << "length_var= " << length_var
			 << " width_var= " << width_var
			 << " orientation_var= " << orientation_var << endl;
	cout << "geometry_type= " << geometry_type <<endl;
	for (i = 0; i < geometry.size(); ++i){
		cout << "geometry point " << i << ":  ";
		geometry[i].print();
	}
	cout <<"height= " << height 
			 << " height_var= " << height_var << endl;
	cout << "velocity:  " ;
	velocity.print();
	cout <<"frame= " << frame_type << endl;
	
	for (i = 0; i < label.size(); ++i){
		cout << "label line " << i << ":  " << label[i]<< endl;
	}
	cout << "------End Printing MapElement--------" << endl;
}
	void print_state() const {
	
	cout << "------Start Printing VehicleState------" << endl;
	cout << "timestamp= " << state.timestamp << endl;
	cout << "utmNorthing= " << state.utmNorthing 
			 << " utmEasting= " << state.utmEasting 
			 << " utmAltitude= " << state.utmAltitude 
			 << endl;

	cout << "GPS_Northing_deprecated= " << state.GPS_Northing_deprecated
			 << " GPS_Easting_deprecated= " << state.GPS_Easting_deprecated 
			 << endl;

	cout << "utmNorthVel= " << state.utmNorthVel 
			 << " utmEastVel= " << state.utmEastVel 
			 << " utmAltitudeVel= " << state.utmAltitudeVel 
			 << endl;

	cout << "Acc_N_deprecated= " << state.Acc_N_deprecated
			 << "Acc_E_deprecated= " << state.Acc_E_deprecated
			 << "Acc_D_deprecated= " << state.Acc_D_deprecated
			 << endl;

	cout << "utmRoll= " << state.utmRoll 
			 << " utmPitch= " << state.utmPitch 
			 << " utmYaw= " << state.utmYaw 
			 << endl;

	cout << "utmRollRate= " << state.utmRollRate 
			 << " utmPitchRate= " << state.utmPitchRate 
			 << " utmYawRate= " << state.utmYawRate 
			 << endl;

	cout << "raw_YawRate_deprecated= " << state.raw_YawRate_deprecated 
			 << " RollAcc_deprecated= " << state.RollAcc_deprecated 
			 << endl;
 
	
	cout << "PitchAcc_deprecated= " << state.PitchAcc_deprecated
			 << " YawAcc_deprecated= " << state.YawAcc_deprecated 
			 << endl;

	cout << "utmNorthConfidence= " << state.utmNorthConfidence  
			 << " utmEastConfidence= " << state.utmEastConfidence 
			 << " utmAltitudeConfidence= " <<  state.utmAltitudeConfidence  
			 << endl;

	cout << "rollConfidence= " << state.rollConfidence 
			 << " pitchConfidence= " << state.pitchConfidence
			 << " yawConfidence= " << state.yawConfidence
			 << endl;

	cout << "gpsGamma= " << state. gpsGamma
			 << " utmZone= " << state.utmZone 
			 << " utmLetter= " << state.utmLetter
			 << endl;

  cout << "vehXVel= " << state.vehXVel 
			 << " vehYVel= " << state.vehYVel 
			 << " vehZVel= " << state.vehZVel
			 << endl;
  
	cout << "vehRollRate= " << state.vehRollRate 
			 << " vehPitchRate= " << state.vehPitchRate 
			 << " vehYawRate= " << state.vehYawRate
			 << endl;


  cout << "localX= " << state.localX 
			 << " localY= " << state.localY 
			 << " localZ= " << state.localZ
			 << endl;

  cout << "localRoll= " << state.localRoll 
			 << " localPitch= " << state.localPitch 
			 << " localYaw= " << state.localYaw
			 << endl;

  cout << "localXVel= " << state.localXVel 
			 << " localYVel= " << state.localYVel 
			 << " localZVel= " << state.localZVel
			 << endl;

  cout << "localRollRate= " << state.localRollRate 
			 << " localPitchRate= " << state.localPitchRate 
			 << " localYawRate= " << state.localYawRate
			 << endl; 

	cout << "------End Printing VehicleState--------" << endl;
}

	void set_center_from_geometry(){
	int i;
	int size = (int)geometry.size();
	if (size==0)
		return;
	
	double fulldist = 0;
	double dist = 0;
	point2 dpt;
	double ratio = 1;
	point2 sum(0,0);
	int lowindex =0,highindex=0;

	
	switch (geometry_type) {
	case GEOMETRY_UNDEF:
	case GEOMETRY_POINTS:
		center = geometry[0];
		return;
	case GEOMETRY_ORDERED_POINTS:
	case GEOMETRY_LINE:
	case GEOMETRY_EDGE:
		for (i = 0; i < size-1; ++i){
			dpt = geometry[i+1]-geometry[i];
			fulldist = fulldist+dpt.norm();
		}
			
		for (i = 0; i < size-1; ++i){
			dpt = geometry[i+1]-geometry[i];
				
			if (dist < fulldist/2 &&
					dist+dpt.norm() >= fulldist/2){
				lowindex = i;
				highindex = i+1;
				
				break;
			}
			dist = dist+dpt.norm();
		}
		ratio = (fulldist/2-dist)/dpt.norm();
		center = geometry[lowindex]+dpt*ratio;
		return;
	case GEOMETRY_POLY:
		for (i = 0; i < size; ++i){
			sum = sum+geometry[i];
		}
		center = sum/size;
		return;
		
	}
}
};
#endif
