#ifndef _ADRIVE_SKYNET_INTERFACE_TYPES_H
#define _ADRIVE_SKYNET_INTERFACE_TYPES_H
/**
 * Basically, this header defines the actuator
 * and command type and has two possible fields.  

 *Say the desired effect is command the steering to .75.
 *create the struct in memory.  aka %drivecmd my_struct;
 *then set %my_actuator = steer;
 *set %my_command_type = set_position;
 *and give the value %number_arg = .75;

 *When skynet gets this message.  It will read the actuator and command type.  
 *And set_position knows to look at the numeric_arg as opposed to the string.  

 *This is for expandability.  Such as if settings need to be changed on some
 *actuaor.  set_parameter would look at the string_arg, and ignore the number. 
 **/


/*! This is the list of all the actuator types for which adrive will 
 * listen for commands, gas and brake individually are legacy. They will 
 * eventually go away. */

#define ADRIVE_ACTUATOR_LIST(_) \
  _(steer, = 0 ) \
  _(accel, ) \
  _(gas, ) \
  _(brake, ) \
  _(estop, ) \
  _(trans, )
DEFINE_ENUM( adrive_actuator_type_enum_t, ADRIVE_ACTUATOR_LIST )

/*! These are the command types for the actuators.  */

#define ACTUATOR_COMMAND_LIST(_) \
  /* _(get_position, )- need to set =0 if this is made the first entry */ \
  /* _(get_pressure, ) */ \
  _(set_position, =0) \
  /* _(set_parameter, ) */ \
  /* _(get_status, ) */ \
  /* _(get_error, ) */ \
  /* _(set_parameter, ) */ \
  _(set_velocity, ) \
  _(set_acceleration, )
DEFINE_ENUM( adrive_command_type_enum_t, ACTUATOR_COMMAND_LIST )  

/*! These are the return_types for adrive when it is returning a request 
 * for status. */
/* I am removing return functionality but leaving the code in place in 
 * case it wants to be reimplemented */
typedef enum
  {
    return_status,
    return_position,
    return_pressure,
    return_error
  } adrive_return_type_enum_t;  


/*! This is the structure of all adrive commands. Include this header and 
* sent all adrive commands in this struct.  */
typedef struct 
{
  adrive_actuator_type_enum_t my_actuator;
  adrive_command_type_enum_t my_command_type;
  double number_arg;
  char string_arg[25];
} drivecmd_t;

/*! This the struct in which adrive will return position requests.  Include
* this header and parse it directly out of this struct.  */
typedef struct
{
  adrive_actuator_type_enum_t my_actuator;
  adrive_return_type_enum_t my_return_type;
  double number_arg;
  char string_arg[25];
} drivecom_t;

typedef struct 
{
  // These are all in the [0,1] range
  //Steer
  int m_steerstatus; // 0 = off, 1 = on 
  double m_steerpos; // -1 to 1, left to right
  double m_steercmd;  //dito
  unsigned long long m_steer_update_time; // When the steering status was last updated

  //Gas
  int m_gasstatus; // 0 = off, 1 = on 
  double m_gaspos;
  double m_gascmd;  
  unsigned long long m_gas_update_time; // When the gas status was last updated

  //Brake
  int m_brakestatus; // 0 = off, 1 = on 
  double m_brakepos; // 0 to 1
  double m_brakecmd;  // 0 to 1
  double m_brakepressure; // 0 to 1
  unsigned long long m_brake_update_time; // When the brake status was last updated

  //Estop
  int m_estopstatus; // 0 = off, 1 = on 

  // Something is pausing us.  
  // Poll this if you don't care why we're stopped
  int m_estoppos; // 0 or 1 or 2
  // Darpa Pause
  int m_dstoppos; // 0 or 1
  // Adrive error pause
  int m_astoppos; // 0 or 1
  // Controled Pause from software
  int m_cstoppos; // 0 or 1
  unsigned long long m_estop_update_time; // When the estop status was last updated
  // Warning trying to return from pause
  int m_about_to_unpause;


  //Transmission
  int m_transstatus; // 0 = off, 1 = on 
  int m_transcmd; // 0 to 1
  int m_transpos; // 0 to 1
  unsigned long long m_trans_update_time; // When the transmission status was last updated

  //OBDII
  int m_obdiistatus; // 0 = off, 1 = on 
  /*! The RPM of the engine */
  double m_engineRPM;
  /*! The time since the engine started in seconds */
  int m_TimeSinceEngineStart;
  /*! Vehicle speed m/s */
  double m_VehicleWheelSpeed;
  /*! The temperature of the engine coolant. */

  double m_EngineCoolantTemp;
  /*! The wheel force in Newtons */
  double m_WheelForce;

  /*! The time the glow plug has been on??? in seconds */
  int m_GlowPlugLampTime;
  /*! The position of the accelerator petal UNITS?? */
  double m_ThrottlePosition;
  /*! The current gear ratio */
  double m_CurrentGearRatio;
  unsigned long long m_obdii_update_time; // When the obdii status was last updated

} ActuatorState;


#endif //_ADRIVE_SKYNET_INTERFACE_TYPES_H

