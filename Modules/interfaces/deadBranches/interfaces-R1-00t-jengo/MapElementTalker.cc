/**********************************************************
 **
 **  MAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-02-08 18:55:14 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:05:13 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapElementTalker.hh"
using namespace std;

int CMapElementTalker::initSendMapElement()
{
	mapElementSendSocket = this->m_skynet.get_send_sock(SNmapElement);
	return(0);
}

int CMapElementTalker::initRecvMapElement()
{
	mapElementRecvSocket= this->m_skynet.listen(SNmapElement,MODmapping);
	return(0);
}

int CMapElementTalker::sendMapElement(MapElement* pMapElement)
{
	setMapElementMsgOut(pMapElement);
	return(this->m_skynet.send_msg(this->mapElementSendSocket,
																 &this->mapElementMsgOut,
																 sizeof(this->mapElementMsgOut),0));
}


int CMapElementTalker::recvMapElementBlock(MapElement* pMapElement)
{
	int readval = this->m_skynet.get_msg(this->mapElementRecvSocket,
																		 &this->mapElementMsgIn,
																		 sizeof(this->mapElementMsgIn),0);
	setMapElementMsgIn(pMapElement);

	return readval;
}

int CMapElementTalker::recvMapElementNoBlock(MapElement* pMapElement)
{

	int readval = 0;
	
	if (this->m_skynet.is_msg(this->mapElementRecvSocket)){
		readval = this->m_skynet.get_msg(this->mapElementRecvSocket,
																		 &this->mapElementMsgIn,
																		 sizeof(this->mapElementMsgIn),0);
		setMapElementMsgIn(pMapElement);
	}
	return(readval);
}



int CMapElementTalker::setMapElementMsgOut(MapElement* pMapElement)
{
	this->mapElementMsgOut.id = pMapElement->id;
	this->mapElementMsgOut.conf = pMapElement->conf;
	this->mapElementMsgOut.type = pMapElement->type;
	this->mapElementMsgOut.type_conf = pMapElement->type_conf;
	this->mapElementMsgOut.center = pMapElement->center;
	
	this->mapElementMsgOut.length = pMapElement->length;
	this->mapElementMsgOut.width = pMapElement->width;
	this->mapElementMsgOut.orientation = pMapElement->orientation;
	this->mapElementMsgOut.length_var = pMapElement->length_var;
	this->mapElementMsgOut.width_var = pMapElement->width_var;
	this->mapElementMsgOut.orientation_var = pMapElement->orientation_var;

	if (pMapElement->geometry.size() > MAP_ELEMENT_NUMPTS){
		this->mapElementMsgOut.num_pts = MAP_ELEMENT_NUMPTS;
		cerr << "in MapElementTalker.cc, Trunkating Map Element geometry vector from "
				 << pMapElement->geometry.size() << " to " << MAP_ELEMENT_NUMPTS<< endl;
	} 
	else
		this->mapElementMsgOut.num_pts = pMapElement->geometry.size();

	for (int i = 0; i<mapElementMsgOut.num_pts;++i){
		this->mapElementMsgOut.geometry[i] = pMapElement->geometry[i];
	}
	this->mapElementMsgOut.height = pMapElement->height;
	this->mapElementMsgOut.height_var = pMapElement->height_var;

	this->mapElementMsgOut.velocity = pMapElement->velocity;
		 
	this->mapElementMsgOut.frame = pMapElement->frame;
	this->mapElementMsgOut.state = pMapElement->state;
	return 0;
}

int CMapElementTalker::setMapElementMsgIn(MapElement* pMapElement)
{
	pMapElement->id = this->mapElementMsgIn.id;
	pMapElement->conf = this->mapElementMsgIn.conf;
	pMapElement->type = this->mapElementMsgIn.type;
	pMapElement->type_conf = this->mapElementMsgIn.type_conf;
	pMapElement->center = this->mapElementMsgIn.center;
	
	pMapElement->length = this->mapElementMsgIn.length;
	pMapElement->width = this->mapElementMsgIn.width;
	pMapElement->orientation = this->mapElementMsgIn.orientation;
	pMapElement->length_var = this->mapElementMsgIn.length_var;
	pMapElement->width_var = this->mapElementMsgIn.width_var;
	pMapElement->orientation_var = this->mapElementMsgIn.orientation_var;

	pMapElement->geometry.clear();
	for (int i = 0; i<mapElementMsgIn.num_pts;++i){
		pMapElement->geometry.push_back(this->mapElementMsgIn.geometry[i]);
	}

	pMapElement->height = this->mapElementMsgIn.height;
	pMapElement->height_var = this->mapElementMsgIn.height_var;

	pMapElement->velocity = this->mapElementMsgIn.velocity;
		 
	pMapElement->frame = this->mapElementMsgIn.frame;
	pMapElement->state = this->mapElementMsgIn.state;
	return 0;
}
