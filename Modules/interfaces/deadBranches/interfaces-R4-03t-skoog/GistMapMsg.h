/* 
 * Desc: Gist Map message type
 * Date: 23 July 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/


#ifndef GISTMAP_MSG_H
#define GISTMAP_MSG_H

#include <stdint.h>

enum PlanningMode{
  INTERSECT_LEFT,
  INTERSECT_RIGHT,
  INTERSECT_STRAIGHT,
  BACKUP,
  DRIVE,
  STOP_OBS,
  ZONE, 	
};

#endif
