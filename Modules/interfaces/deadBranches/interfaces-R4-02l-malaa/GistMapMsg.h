/* 
 * Desc: Gist Map message type
 * Date: 23 July 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/


#ifndef GISTMAP_MSG_H
#define GISTMAP_MSG_H

#include <stdint.h>

struct GistSegment{
  float startPtx;
  float startPty;
  float startCost;
  float endPtx;
  float endPty;
  float endCost;
};

/// @brief Skynet message with gist map data.
typedef struct
{
  // vector of gist segments
  GistSegment gistVec[10];
 
} __attribute__((packed)) GistMapMsg;

#endif
