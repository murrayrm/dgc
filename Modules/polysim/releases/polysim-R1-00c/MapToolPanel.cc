/*
 *
 *  Author: Chris Schantz
 *  Date: August 23th 2007
 *
 *
 *  MapToolPanel.cc: The control pannel to hold the tools for adding stuff to and manupilating the map
 *
 */

#include "MapToolPanel.hh"

using namespace std;

MapToolPanel::MapToolPanel(MapViewer* view) : 
  Fl_Window(550, 200, 300, 100, "Tool Panel")
{
  viewer = view; // hold the pointer to the Map Viewer object, and though it the map.
  cout << "mapviewer address" << viewer << endl;
}

void MapToolPanel::initToolPanel()
{
  
  begin();
    newAlice = new Fl_Button( 10, 10, 80, 80, "New Alice");
    newAlice->callback(static_newAlice_cb, this);
    
    loadRNDF = new Fl_Button(110, 10, 80, 80, "Load RNDF");
    loadRNDF->callback(static_loadRNDF_cb, this);

    quitButton = new Fl_Button(210, 10, 80, 80, "Quit Sim");
    quitButton->callback(static_quitButton_cb, this);
  end();
  show();
}

MapToolPanel::~MapToolPanel()
{}

void MapToolPanel::static_newAlice_cb(Fl_Widget* w, void *v) // static method
{ 
  ((MapToolPanel*)v)->newAlice_cb();
}

void MapToolPanel::newAlice_cb() // normal method
{ 
  NewAlicePanel panel(viewer);
  
  while(panel.shown())
  { 
    Fl::wait(); 
  }
}

void MapToolPanel::static_quitButton_cb(Fl_Widget* w, void *v) // static method
{ 
  exit(0);
}

void MapToolPanel::static_loadRNDF_cb(Fl_Widget* w, void *v)
{
   ((MapToolPanel*)v)->loadRNDF_cb(); 
}

void MapToolPanel::loadRNDF_cb() 
{
  viewer->mapwin->cursorState = NORMAL;
  // Create the file chooser, and show it
  Fl_File_Chooser chooser("../../etc/",			    // directory
			              "*.rndf",			        // filter
			              Fl_File_Chooser::SINGLE, 	// chooser type
			              "Pick an RNDF");	        // title
  chooser.show();
  
  // Block until user picks something.
  while(chooser.shown())
  { 
    Fl::wait(); 
  }

  // User hit cancel?
  if ( chooser.value() == NULL )
  { 
    cout << "No RNDF chosen" << endl;
    return; 
  }
  
  string rndfPath = chooser.value();
  viewer->loadRNDF(rndfPath);
}



