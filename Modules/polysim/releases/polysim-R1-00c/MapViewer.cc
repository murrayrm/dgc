/**********************************************************
 **
 **  MAPVIEWER.CC
 **
 **    Time-stamp: <2007-06-07 10:53:28 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 23:03:09 2007
 **
 **
 **********************************************************
 **  
 **  2D map viewer
 **
 **********************************************************/


#include "MapViewer.hh"

using namespace std;

MapViewer::MapViewer(int skynet)
{
  skynetKey = skynet;
}

MapViewer::~MapViewer()
{
}

int MapViewer::init(int debug_level, bool show_ids, string rndfPath)
{
  aliceCollection.clear();
  mainwin = new Fl_Window(500,500,"Polysim: Multiple Alice Simulator");
  mapwin = new MapWindow(10,10, mainwin->w()-20, mainwin->h()-20);

  mapwin->debugLevel = debug_level;
  mapwin->showIDs = show_ids;
  
  mapwin->has_rndf = false;
  
  // Load up the rndf if given in command line
  if (rndfPath != "")
  {
    if (loadRNDF(rndfPath) == 0)
    {
      mapwin->has_rndf = true;
    } 
  }  
    
  mainwin->resizable(mapwin);

  mainwin->show();

  AliceInstance::idCounter = -2;

  return 0;
}

int MapViewer::loadRNDF(string rndf)
{
  Map mapdata;
  
  if (!mapdata.loadRNDF(rndf))
  {
    cout << "Error loading rndf: " << rndf << endl;
    return -1;
  }

  mapwin->localmap.clear();
  
  MapElement el;
  
  int numelements = mapdata.prior.data.size();
  cout << "elements = " << numelements << endl;  
  
  for (int i = 0 ; i < numelements; ++i)
  {
    mapdata.prior.getEl(el,i);
    add_element(el);
  }

  loadedRndf = rndf;

  return 0;
}

int MapViewer::add_element(MapElement& el)
{
  MapId thisid = el.id;
  MapId tmpid;
  
  if (el.type == ELEMENT_ALICE)
  {
    cout << "trying to add an element type = ELEMENT_ALICE to localmap, Alices belong in aliceList" << endl;
    return -1;
  }

  for (unsigned int i = 0; i < mapwin->localmap.size(); ++i)
  {
    tmpid = mapwin->localmap[i].id;
    if (tmpid == thisid)
    {
      if (el.type==ELEMENT_CLEAR)
      {
        mapwin->localmap.erase(mapwin->localmap.begin()+i);
      }
      else
      {
        mapwin->localmap[i] = el;
      }
      return 0;
    }
  }
  if (el.type != ELEMENT_CLEAR)
  {
    mapwin->localmap.push_back(el);
  }
  return 0;
}

int MapViewer::add_alice(MapElement& el)
{
  MapId thisid = el.id;
  MapId tmpid;

  if ((el.type != ELEMENT_ALICE) || (el.type==ELEMENT_CLEAR))
  {
    cout << "trying to add an element type != ELEMENT_ALICE to aliceList, non-Alices belong in localmap" << endl;
    return -1;
  }

  for (unsigned int i = 0; i < mapwin->aliceList.size(); ++i)
  {
    tmpid = mapwin->aliceList[i].id;
    if (tmpid == thisid)
    {
      if (el.type==ELEMENT_CLEAR)
      {
        mapwin->aliceList.erase(mapwin->aliceList.begin()+i);
      }
      else
      {
        mapwin->aliceList[i] = el;
      }
      return 0;
    }
  }
  if (el.type != ELEMENT_CLEAR)
  {
    mapwin->aliceList.push_back(el);
  }
  return 0;
}

void MapViewer::updateLiveElements()
{
  if (mapwin->newAliceInstance.readyFlag)
  {
    double tempYaw = atan2(mapwin->newAliceInstance.initNorthing - mapwin->newAliceInstance.headingNorthing,
                           mapwin->newAliceInstance.initEasting - mapwin->newAliceInstance.headingEasting);
    AliceInstance* tempAlice = new AliceInstance(mapwin->newAliceInstance.skynetKey, mapwin->newAliceInstance.initEasting,
                                                 mapwin->newAliceInstance.initNorthing, tempYaw + M_PI, loadedRndf, mapwin->newAliceInstance.mdf);
    aliceCollection.push_back(tempAlice);
    mapwin->newAliceInstance.readyFlag = false;
  }
  for (unsigned int i = 0; i < aliceCollection.size(); i++)
  {
    aliceCollection[i]->update(this);
  }
}

