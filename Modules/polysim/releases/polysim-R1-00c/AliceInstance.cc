/*!
 * \file VehiclePlan.cc
 * \brief Code for VehiclePlan class
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 */

#include <sstream>
#include <string>

#include "AliceInstance.hh"

int AliceInstance::idCounter = -2;


AliceInstance::AliceInstance(int skynetKey, double northing, double easting, double yaw, string rndf, string mdf)
{
  baseId = idCounter;
  idCounter--;
  cout << "skynet_key: " << skynetKey << endl;
  initNorthing = northing;
  initEasting = easting;
  initYaw = yaw;

  rndfPath = rndf;
  mdfPath = mdf;

  timeStopped = 0;
  lastRoundTimeStamp = DGCgettime();
  
  stringstream s("");
  s << "xterm -geometry 90x30 -e ./../../bin/i486-gentoo-linux/process-control --skynet-key=" << skynetKey << " --northing=" 
    << initNorthing << " --easting=" << initEasting << " --yaw=" << initYaw << " --rndf=" << rndf 
    << " --mdf=" << mdf << " --cfg=polysim.CFG  --otherProcs=otherProcsField.CFG --no-restart &";

  cout << s.str() << endl;

  system(s.str().c_str());
  
  server = skynet_connect(skynetKey, SNGui);
  pthread_create(&skynet_thread, NULL, skynet_main_thread, (void *) server);
  snVehStategroup = skynet_listen(server, SNstate, NULL, sizeof(VehicleState), NULL, NULL, NULL, 0);
  snActStategroup = skynet_listen(server, SNactuatorstate, NULL, sizeof(ActuatorState), NULL, NULL, NULL, 0);  
  
  stateMessagesFlowing = false;  //after first message recieved this will be true- prevents program hanging while planning stack starts up
  actStateMessagesFlowing = false;

  /* Create map element for steering spline */
  steerObj.setTypeSteerCmd();
  steerObj.setId(baseId,3);
  
  /* Create map element for commanded steering spline */
  steerCmdObj.setTypeSteer();
  steerCmdObj.setId(baseId,4);
  
  /* Create map element for alice */
  alice.setTypeAlice();
  alice.setId(baseId,2);

  alicePos = point2(initNorthing, initEasting);
  
  /* Create a traversed path(s) */
  travPath.push_back(alicePos);
  
  /* map element for traversed path */
  travPathObj.setTypeTravPath();
  travPathObjId = 0;
  travPathObj.setId(baseId,1,travPathObjId);

  //Ready out mapelement talker to send to the planning stack runnign this particular Alice
  mapTalker.initSendMapElement(skynetKey);
}

AliceInstance::~AliceInstance()
{
  idCounter++;
}



void AliceInstance::update(MapViewer* map)
{
  // update state only if we know messages have begun flowing (else it will block the program here)
  if (stateMessagesFlowing)
  {
    skynet_get_latest(snVehStategroup, (char*) &m_state, sizeof(VehicleState));
  } 
  else
  {
    m_state.localX = 0;
    m_state.localY = 0;
    m_state.localYaw = initYaw;
    if (skynet_peek(snVehStategroup, NULL))
    {
      stateMessagesFlowing = true;
    }
  }
  if (actStateMessagesFlowing)
  {
    skynet_get_latest(snActStategroup, (char*) &m_actuatorState, sizeof(ActuatorState));
  }
  else
  {
    m_actuatorState.m_steerpos = 0.0;
    m_actuatorState.m_steercmd = 0.0;
    if (skynet_peek(snActStategroup, NULL))
    {
      actStateMessagesFlowing = true;
    }
  }
  
  m_state.localX += initNorthing;
  m_state.localY += initEasting;

  if ((m_state.utmNorthVel == 0) && (m_state.utmEastVel == 0))
  {
    timeStopped += (DGCgettime() - lastRoundTimeStamp);
  }
  else if ((m_state.utmNorthVel != 0) || (m_state.utmEastVel != 0))
  {
    timeStopped = 0;
  }

  lastRoundTimeStamp = DGCgettime();
  
  // send alice object to mapviewer
  alice.setState(m_state);
  map->add_alice(alice);

  // update the steering spline
  steerSplineTemp = genAngleArcSpline(m_actuatorState.m_steerpos);
  steerObj.setGeometry(steerSplineTemp);
  
  map->add_element(steerObj);
  
  // update the commanded steering spline
  steerSplineTemp = genAngleArcSpline(m_actuatorState.m_steercmd);
  steerCmdObj.setGeometry(steerSplineTemp);
  
  map->add_element(steerCmdObj);
  
  // update the traveld path
  if (genTravPathSpline(travPath)) //will be true if we have moved far enough to be worth updating
  {
    travPathObj.setGeometry(travPath);
    map->add_element(travPathObj);
  }

  m_state.localX -= initNorthing;
  m_state.localY -= initEasting;

  broadcastMapElements(map);

}

void AliceInstance::broadcastMapElements(MapViewer* map)
{
  MapElement obj;
  double dfront = DIST_REAR_AXLE_TO_FRONT;
  double drear = DIST_REAR_TO_REAR_AXLE;
  double dside =VEHICLE_WIDTH/2;
  vector<point2> boundary;


  for (unsigned int i = 0; i < map->aliceCollection.size(); i++)
  {
    if (this == map->aliceCollection[i])  //don't report yourself...
    {
      continue;
    }

    AliceInstance* otherAlice = map->aliceCollection[i];
    obj.clear();
    obj.setId(otherAlice->baseId);
    boundary.clear();
    
    double vehicleX = otherAlice->m_state.localX + otherAlice->initEasting;
    double vehicleY = otherAlice->m_state.localY + otherAlice->initNorthing;
    double vehicleYaw = otherAlice->m_state.localYaw;
    double relativeX = vehicleX - (m_state.localX + initEasting);
    double relativeY = vehicleY - (m_state.localY + initNorthing);
    obj.velocity.x = otherAlice->m_state.utmNorthVel;
    obj.velocity.y = otherAlice->m_state.utmEastVel;
    obj.setState(m_state);
    obj.setTypeVehicle();
    obj.height = 1;
    obj.timeStopped = otherAlice->timeStopped;
    
    double co = cos(vehicleYaw);
    double so = sin(vehicleYaw);

    boundary.push_back(point2(relativeX+(dfront*co -dside*so), relativeY+(dfront*so +dside*co)));
    boundary.push_back(point2(relativeX+(-drear*co -dside*so), relativeY+(-drear*so +dside*co)));
    boundary.push_back(point2(relativeX+(-drear*co +dside*so), relativeY+(-drear*so -dside*co)));
    boundary.push_back(point2(relativeX+(dfront*co +dside*so), relativeY+(dfront*so -dside*co)));
    obj.setGeometry(boundary);
    
    //send to map viewer
    mapTalker.sendMapElement(&obj, -2);
    //send to mapper
    mapTalker.sendMapElement(&obj, 0);
  }
}

bool AliceInstance::genTravPathSpline(point2arr &travPath)
{
  // update the traversed path
  alicePos.x = m_state.localX;
  alicePos.y = m_state.localY;
  
  // want to space out the points so the vector isn't huge
  
  if ((alicePos - lastpoint).norm() >= 1)
  {
    lastpoint = alicePos;
    if (travPath.size()>200)
    {
      travPathObjId++;
      travPathObjId = (travPathObjId % 250); //limit total number of path segments
      travPathObj.setId(baseId,1,travPathObjId);
      lastpoint = travPath.back();
      travPath.clear();
      travPath.push_back(lastpoint);
    }
      
    travPath.push_back(alicePos);
    
    return true;    
  }
  return false;
}

point2arr AliceInstance::genAngleArcSpline(double theta)
{
  point2arr spline;
  spline.clear();
  double phi = theta;

  phi *= VEHICLE_MAX_AVG_STEER;
  
  if(fabs(phi) < 0.0001)
  {
    phi = 0.0001;
  }
  
  double turningRadius = VEHICLE_WHEELBASE / sin(phi);
  double cn, ce, angle, dangle;
  double lengthArc = VEHICLE_WHEELBASE * 0.5;
  double localYaw = m_state.localYaw;
  double screenyaw = localYaw;
  point2 frontWheels;
  frontWheels.x = m_state.localX + cos(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.y = m_state.localY + sin(screenyaw) *VEHICLE_WHEELBASE;
  
  // turningRadius has a sign and will make sure the following are correct
  cn = frontWheels.y + turningRadius*sin(M_PI/2.0 -localYaw - phi);
  ce = frontWheels.x  - turningRadius*cos(M_PI/2.0 -localYaw - phi);
  dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
  angle = atan2(frontWheels.x - ce, frontWheels.y - cn);
  
  spline.push_back (point2(frontWheels.x, frontWheels.y));
  
  for(int i=0; i<20; i++)
  {
    angle -= dangle;
    spline.push_back(point2(ce + fabs(turningRadius)*sin(angle),
                         cn + fabs(turningRadius)*cos(angle)));
  }
  return spline;
}
