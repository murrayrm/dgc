/*
 *  Multiple skynet key listener test program
 *  Author Chris Schantz
 */

#include <iostream>

#include "interfaces/sn_types.h"
#include "interfaces/VehicleState.h"
#include "skynettalker/SkynetTalker.hh"
#include "skynet/skynet.hh"

using namespace std;

int main(int argc, char *argv[])
{
  system("xterm -e ./../../bin/i486-gentoo-linux/asim --skynet-key=1001 &");
  system("xterm -e ./../../bin/i486-gentoo-linux/asim --skynet-key=1002 -N 20 -E 40 &");

  skynet_server_t *server1 = skynet_connect(1001, SNGui);
  skynet_server_t *server2 = skynet_connect(1002, SNGui);

  // Start thread to read and process messages
  pthread_t skynet_thread1;
  pthread_t skynet_thread2;
  pthread_create(&skynet_thread1, NULL, skynet_main_thread, (void *) server1);
  pthread_create(&skynet_thread2, NULL, skynet_main_thread, (void *) server2);

  skynet_group_t* sngroup1 = skynet_listen(server1, SNstate, NULL, sizeof(VehicleState), NULL, NULL, NULL, 0);
  skynet_group_t* sngroup2 = skynet_listen(server2, SNstate, NULL, sizeof(VehicleState), NULL, NULL, NULL, 0);

  VehicleState vehstate1;
  VehicleState vehstate2;

  cout << "vehstate1 address: " << &vehstate1 << endl;
  cout << "vehstate2 address: " << &vehstate2 << endl;

  //char vehstate1[sizeof(VehicleState)];
  //char vehstate2[sizeof(VehicleState)];

  while (1) 
  {
    skynet_get_message(sngroup1, (char*) &vehstate1, sizeof(VehicleState));
    skynet_get_message(sngroup2, (char*) &vehstate2, sizeof(VehicleState));
    cout << "vehstate1 address: " << &vehstate1 << endl;
    cout << "vehstate2 address: " << &vehstate2 << endl;
    cout << "On snKey 1001 Norhting: " << vehstate1.utmNorthing << " Easting: " << vehstate1.utmEasting << endl;
    cout << "On snKey 1002 Norhting: " << vehstate2.utmNorthing << " Easting: " << vehstate2.utmEasting << endl;
  }
}
