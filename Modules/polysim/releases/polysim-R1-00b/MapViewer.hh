/**********************************************************
 **
 **  MAPVIEWER.HH
 **
 **    Time-stamp: <2007-06-07 10:53:28 sam> 
 **    
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 23:03:09 2007
 **
 **    Revised for PolySim by Chris Schantz
 **    Date: August 19th 2007
 **
 **********************************************************
 **  
 **  2D map viewer
 **
 **********************************************************/

#ifndef MAPVIEWER_HH
#define MAPVIEWER_HH

#include <vector>
#include <iomanip>
#include <math.h>

#include "skynettalker/SkynetTalker.hh"
#include "interfaces/sn_types.h"
#include "map/Map.hh"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"


#include "MapWindow.hh"
#include "AliceInstance.hh"

class AliceInstance;

class MapViewer
{

public:
  MapViewer(int skynet); 
  
  ~MapViewer(); 
   
  int init(int debug_level, bool show_ids, string rndfPath);
  int add_element(MapElement& el);
  int add_alice(MapElement& el);
  int loadRNDF(string rndf);

  //void add_liveElement();

  string loadedRndf;

  void updateLiveElements();
    
  vector<AliceInstance*> aliceCollection;
  Fl_Window *mainwin;
  MapWindow *mapwin;
    
  int skynetKey;
};

#endif //MAPVIEWER_HH
