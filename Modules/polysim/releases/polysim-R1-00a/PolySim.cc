/*
 *
 *  Author: Chris Schantz
 *  Date: August 19th 2007
 *
 *  PolySim.cc: the main class for a quick but useful rewrite of trafsim
 *
 *  The idea is to leverage as much pre existing code as possible.  For example
 *    All simulated vehicles will be copies of Alice.  This has the added benefit
 *    multiplying the test time in simulation tremendously, plus each simulated car
 *    is going to be a lot smarter than any automatic scripded car- much more
 *    accurate traffic test.  I also want to incorporate sensor-sim functionaliuty
 *    directly, which is a much better way of doing it when there are multiple Alices
 *    present.  In addition I want to have some tools to help with obstical placement
 *    and the ability to create a vegetation obstical in fields and stuff.
 *
 */

#include <iomanip>
#include <iostream>
#include <string>
#include <boost/serialization/vector.hpp>

#include "map/Map.hh"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "bitmap/BitmapParams.hh"
#include "interfaces/sn_types.h"


#include "cmdline.h"
#include "MapViewer.hh"

// Handle idle callbacks
void main_idle(MapViewer *self)
{
  int bytesSent = 0;
  MapElement sendEl;
 
  if (self->mapwin->get_sendElement(sendEl))
  {
    bytesSent = self->sendMapElement(&sendEl,self->sendSubGroup);
  }
  else
  {
    usleep(1000);
  }
  return;
}


int main(int argc, char *argv[])
{
  int skynetKey;
  int sendSubGroup;
  
  gengetopt_args_info options;

  glutInit(&argc, argv);

  // Load options
  if (cmdline_parser(argc, argv, &options) < 0)
    return -1;
  
  // Fill out the send subgroup number
  if (options.send_subgroup_given)
    sendSubGroup = options.send_subgroup_arg;
  else
    sendSubGroup = -1;

   // Fill out the skynet key
  if (options.skynet_key_given)
    skynetKey = options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    skynetKey = 0;
  
  MapViewer mapviewer = MapViewer(skynetKey, sendSubGroup);
  
  mapviewer.init(options.debug_arg, options.show_ids_given);

  // Load up the rndf

  Map mapdata;
  
  if (!mapdata.loadRNDF(options.rndf_arg)){
    cout << "Error loading " << options.rndf_arg << endl;
    return 0;
  };
 
  MapElement el;
  
  int numelements = mapdata.prior.data.size();
  cout << "elements = " << numelements << endl;  
  
  for (int i = 0 ; i < numelements; ++i)
  {
	mapdata.prior.getEl(el,i);
    mapviewer.add_element(el);
  }
  
  mapviewer.mainwin->resizable(mapviewer.mapwin);

  Fl::add_idle((void (*) (void*)) main_idle, &mapviewer);
  mapviewer.mainwin->show();
  return(Fl::run());
}
    
