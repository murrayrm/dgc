/*
 *
 *  Author: Chris Schantz
 *  Date: August 23th 2007
 *
 *
 *  NewAlicePanel.cc:  The control pannel to hold the interface for giving a new skynet key and mdf to an new aliceInstance
 *
 */

#include "NewAlicePanel.hh"

using namespace std;

NewAlicePanel::NewAlicePanel(MapViewer* view) : 
  Fl_Window(210,170,"New Alice Information")
{
  viewer = view; // hold the pointer to the Map Viewer object, and though it the map.
  begin();
    browse = new Fl_Button( 130, 90, 70, 30, "Browse");
    browse->callback( static_browse_cb, this );
  
    placeAlice = new Fl_Button(10, 130, 70, 30, "Place");
    placeAlice->callback(static_placeAlice_cb, this);
  
    cancel = new Fl_Button(130, 130, 70, 30, "Cancel");
    cancel->callback(static_cancel_cb, this);

    inpSnKey = new Fl_Int_Input(100, 10, 100, 30, "Skynet Key:");
    inpMdf = new Fl_Input(50, 50, 150, 30, "MDF:");
    inpHost = new Fl_Input(60, 90, 119, 30, "Host:");    
  end();
  show();
}

NewAlicePanel::~NewAlicePanel()
{}

void NewAlicePanel::static_placeAlice_cb(Fl_Widget* w, void *v) // static method
{ 
  ((NewAlicePanel*)v)->placeAlice_cb();
}

void NewAlicePanel::placeAlice_cb() // normal method
{ 
  viewer->mapwin->newAliceInstance.skynetKey = atoi(inpSnKey->value());
  cout << "skynetKey: " << viewer->mapwin->newAliceInstance.skynetKey << endl;
  viewer->mapwin->newAliceInstance.mdf = inpMdf->value();
  cout << "mdf: " << viewer->mapwin->newAliceInstance.mdf << endl;
  viewer->mapwin->newAliceInstance.host = inpHost->value();
  cout << "host: " << viewer->mapwin->newAliceInstance.host << endl;
  viewer->mapwin->cursorState = ALICE_DROP;
  hide();
}

void NewAlicePanel::static_browse_cb(Fl_Widget* w, void *v)
{
   ((NewAlicePanel*)v)->browse_cb(); 
}

void NewAlicePanel::browse_cb() 
{
  // Create the file chooser, and show it
  Fl_File_Chooser chooser("../../etc/",			    // directory
			              "*.mdf",			        // filter
			              Fl_File_Chooser::SINGLE, 	// chooser type
			              "Pick an MDF");	        // title
  chooser.show();
  
  // Block until user picks something.
  while(chooser.shown())
  { 
    Fl::wait(); 
  }

  // User hit cancel?
  if ( chooser.value() == NULL )
  { 
    cout << "No MDF chosen" << endl;
    return; 
  }
  
  inpMdf->value(chooser.value());
}

void NewAlicePanel::static_cancel_cb(Fl_Widget* w, void *v) // static method
{ 
  ((NewAlicePanel*)v)->cancel_cb();
}

void NewAlicePanel::cancel_cb() // normal method
{ 
  viewer->mapwin->cursorState = NORMAL;
  hide();     
}




