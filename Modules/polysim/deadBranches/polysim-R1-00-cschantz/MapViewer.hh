/**********************************************************
 **
 **  MAPVIEWER.HH
 **
 **    Time-stamp: <2007-06-07 10:53:28 sam> 
 **    
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 23:03:09 2007
 **
 **    Revised for PolySim by Chris Schantz
 **    Date: August 19th 2007
 **
 **********************************************************
 **  
 **  2D map viewer
 **
 **********************************************************/

#ifndef MAPVIEWER_HH
#define MAPVIEWER_HH

#include "MapWindow.hh"

class MapViewer : public CMapElementTalker
{
public:
  MapViewer(int skynet, int sendGroup); 
  
  ~MapViewer(); 
   
  int init(int debug_level, bool show_ids);
  int add_element(MapElement& el);
    
  Fl_Window *mainwin;
  MapWindow *mapwin;
  bool ispaused;
  
  int skynetKey;
  int sendSubGroup;
};

#endif //MAPVIEWER_HH
