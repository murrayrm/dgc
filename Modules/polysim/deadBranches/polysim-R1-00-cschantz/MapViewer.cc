/**********************************************************
 **
 **  MAPVIEWER.CC
 **
 **    Time-stamp: <2007-06-07 10:53:28 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Feb  8 23:03:09 2007
 **
 **
 **********************************************************
 **  
 **  2D map viewer
 **
 **********************************************************/

#include <iomanip>

#include "map/MapElementTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "bitmap/BitmapParams.hh"
#include "interfaces/sn_types.h"
#include <boost/serialization/vector.hpp>

// Cmd-line handling
#include "MapWindow.hh"
#include "MapViewer.hh"
using namespace std;

using namespace bitmap;

MapViewer::MapViewer(int skynet, int sendGroup)
{
  skynetKey = skynet;
  sendSubGroup = sendGroup;
}

MapViewer::~MapViewer()
{
}


int MapViewer::init(int debug_level, bool show_ids)
{
  initSendMapElement(this->skynetKey);
  
  this->mainwin = new Fl_Window(500,500,"DGC Map Viewer");
  this->mapwin = new MapWindow(10,10, 
                               this->mainwin->w()-20, 
                               this->mainwin->h()-20);
  this->mapwin->debugLevel = debug_level;

  this->mapwin->showIDs = show_ids;
  // select colormap
    
  ispaused = false;
  return 0;
}

int MapViewer::add_element(MapElement& el)
{

  MapId thisid = el.id;
  MapId tmpid;

  for (unsigned int i = 0; i < mapwin->localmap.size(); ++i){
    tmpid = mapwin->localmap[i].id;
    if (tmpid == thisid){
      if (el.type==ELEMENT_CLEAR){
        mapwin->localmap.erase(mapwin->localmap.begin()+i);
      }else{
        mapwin->localmap[i] = el;
      }
      return 0;
    }
  }
  if(el.type != ELEMENT_CLEAR)
    mapwin->localmap.push_back(el);
  return 0;
}

