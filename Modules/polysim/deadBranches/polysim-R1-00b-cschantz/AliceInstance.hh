/*!
 * \file VehiclePlan.hh
 * \brief Class for planner stack viewer
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 * The PlanViewer class is used to recieve various types of data
 * messages and update the map display.  This has to be implemented as
 * a class for now because all of the talkers want to derive from
 * SkynetContainer.  This needs to be fixed, but will be done later.
 *
 */

#ifndef ALICEINSTANCE_HH
#define ALICEINSTANCE_HH

#include <boost/serialization/vector.hpp>

#include "skynet/skynet.hh"
#include "interfaces/VehicleState.h"
#include "interfaces/ActuatorState.h"
#include "interfaces/sn_types.h"
#include "trajutils/TrajTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "interfaces/TpDpInterface.hh"

#include "MapViewer.hh"

class MapViewer;

class AliceInstance
{
  //Talker to send map elements to individual planning stacks
  CMapElementTalker mapTalker;

  MapElement steerObj;
  MapElement steerCmdObj;
  MapElement alice;
  MapElement travPathObj;
  
  CTraj *planTrajData;
  pthread_mutex_t path;

  point2arr travPath;
  point2arr steerSplineTemp;  

  point2 emptyPoint;
  point2 alicePos;  
  point2 delta;
  point2 lastpoint;

  bool useLocal;
  double xTranslate, yTranslate;
  bool verbose;
  bool showTrav;
  bool stateMessagesFlowing;
  bool actStateMessagesFlowing;

  int baseId;
  int travPathObjId;

  double initNorthing; 
  double initEasting;
  double initYaw;

  unsigned long long timeStopped;
  unsigned long long lastRoundTimeStamp;  

  string rndfPath;
  string mdfPath;

  double initPhi;

  skynet_server_t* server;
  pthread_t skynet_thread;
  skynet_group_t* snVehStategroup;
  skynet_group_t* snActStategroup;

  VehicleState m_state;
  ActuatorState m_actuatorState;
  
public:
  
  AliceInstance(int skynetKey, double northing, double easting, double yaw, string rndf, string mdf);
  ~AliceInstance();

  void update(MapViewer* map);
  void broadcastMapElements(MapViewer* map);

  bool genTravPathSpline(point2arr &travPath);
  point2arr genAngleArcSpline(double angle);
  
  static int idCounter; 
};


#endif  //ALICEINSTANCE_HH
