/*
 *
 *  Author: Chris Schantz
 *  Date: August 26th 2007
 *
 *
 *  NewAlicePanel.hh: The control pannel to hold the interface for giving a new skynet key and mdf to an new aliceInstance
 *
 */

#ifndef NEWALICEPANEL_HH
#define NEWALICEPANEL_HH

#include <math.h>
#include <iostream>
#include <string>
#include <sstream>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_File_Chooser.H>

#include "MapViewer.hh"

class NewAlicePanel : public Fl_Window
{
  static void static_browse_cb(Fl_Widget* v, void *);
  void browse_cb();

  static void static_placeAlice_cb(Fl_Widget* v, void *);
  void placeAlice_cb();

  static void static_cancel_cb(Fl_Widget* v, void *);
  void cancel_cb();

  MapViewer* viewer;
  string mdfPath;
  
  public:
  
  NewAlicePanel(MapViewer* view);
  ~NewAlicePanel();

  Fl_Button* browse;
  Fl_Button* placeAlice;
  Fl_Button* cancel;
  Fl_Int_Input* inpSnKey;
  Fl_Input* inpMdf;
  Fl_Input* inpHost;

};



#endif //NEWALICEPANEL_HH

