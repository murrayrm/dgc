/*!
 * \file VehiclePlan.cc
 * \brief Code for VehiclePlan class
 *
 * \author Richard M. Murray
 * \date 4 March 2007
 *
 */

#include <sstream>
#include <string>

#include "AliceInstance.hh"

int AliceInstance::idCounter = -2;


AliceInstance::AliceInstance(int skynetKey, point2 delta, double northing, double easting, double yaw, string rndf, string mdf, string host)
{
  baseId = idCounter;
  idCounter--;
  cout << "skynet_key: " << skynetKey << endl;
  initNorthing = northing;
  initEasting = easting;
  initYaw = yaw;
  localToGlobalDelta = delta;
  hostName = "";
  hostName = hostName + host; 

  rndfPath = rndf;
  mdfPath = mdf;

  timeStopped = 0;
  lastRoundTimeStamp = DGCgettime();
  
  stringstream base("");
  stringstream asim("");
  stringstream mapper("");
  stringstream mplanner("");
  stringstream planner("");
  stringstream follower("");
  stringstream gcdrive("");
  stringstream planviewer2("");
  stringstream mapviewer("");
  if (hostName != "")
  {
    base << "ssh -Yf " << hostName << " \"export SPREAD_DAEMON=4803@192.168.1.21; source .bashrc; xterm -e ./alice-hardware-cschantz02/bin/i486-gentoo-linux/";
  }
  /*
  base << "process-control --skynet-key=" << skynetKey << " --northing=" 
    << initNorthing + localToGlobalDelta.x << " --easting=" << initEasting + localToGlobalDelta.y << " --yaw=" << initYaw << " --rndf=" << rndf 
    << " --mdf=" << mdf << " --cfg=polysim.CFG  --otherProcs=otherProcsField.CFG --no-restart \"";

  cout << base.str();

  system(base.str().c_str());
  

  */

  asim.precision(10);
  asim << base.str();
  asim << "asim --skynet-key=" << skynetKey << " -N " << initNorthing + localToGlobalDelta.x << " -E " << initEasting + localToGlobalDelta.y 
       << " -Y " << initYaw << " --gcdrive; echo press enter to close; read\"";

  cout << asim.str() << endl;

  mapper << base.str();
  mapper << "mapper --rndf=" << rndf << " --debug-subgroup=-2 --disable-line-fusion --skynet-key=" << skynetKey << " \"";
  
  cout << mapper.str() << endl;  

  mplanner << base.str();
  mplanner << "mplanner --rndf=" << rndf << " --mdf=" << mdf << " --skynet-key=" << skynetKey << " \"";
  
  cout << mplanner.str() << endl;
  
  planner << base.str();
  planner << "planner --rndf=" << rndf << " --mapper-disable-line-fusion --verbose=9 --skynet-key=" << skynetKey << " \"";
  
  cout << planner.str() << endl; 
  
  follower << base.str();
  follower << "follower --paramfile  alice-hardware-cschantz02/etc/follower/paramfile -f localSim --vis-lat-control --lo-alpha=1.7 --skynet-key=" << skynetKey << " \"";
  
  cout << follower.str() << endl;
  
  gcdrive << base.str();
  gcdrive << "gcdrive --simulate --steer-speed-threshold=2 --pause-braking=0.9 --skynet-key=" << skynetKey << " \"";
  
  cout << gcdrive.str() << endl;
  
  planviewer2 << base.str();
  planviewer2 << "planviewer2 -l --skynet-key=" << skynetKey << " \"";
  
  cout << planviewer2.str() << endl;
  
  mapviewer << base.str();
  mapviewer << "mapviewer --recv-subgroup=-2 --skynet-key=" << skynetKey << " \"";
  
  cout << mapviewer.str() << endl;
  
  /*
  s << "screen -d -m -S Process_Control_" << skynetKey << " ./../../bin/i486-gentoo-linux/process-control --skynet-key=" << skynetKey << " --northing=" 
    << initNorthing + localToGlobalDelta.x << " --easting=" << initEasting + localToGlobalDelta.y << " --yaw=" << initYaw << " --rndf=" << rndf 
    << " --mdf=" << mdf << " --cfg=polysim.CFG  --otherProcs=otherProcsField.CFG --no-restart";
  */


  /*s << "xterm -geometry 90x30 -e ./../../bin/i486-gentoo-linux/process-control --skynet-key=" << skynetKey << " --northing=" 
    << initNorthing + localToGlobalDelta.x << " --easting=" << initEasting + localToGlobalDelta.y << " --yaw=" << initYaw << " --rndf=" << rndf 
    << " --mdf=" << mdf << " --cfg=polysim.CFG  --otherProcs=otherProcsField.CFG --no-restart";
   
  if (hostName != "")
  {
    s <<" \"\"";
  }
  s << " &";
  
  cout << s.str() << endl;
    */
  cout.precision(10);
  cout << "northing: " << initNorthing << " easting: " << initEasting << endl;
  cout << "delta.x: " << localToGlobalDelta.x << " delta.y: " << localToGlobalDelta.y << endl;
 
  system(asim.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(mapper.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(mplanner.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(planner.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(follower.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(gcdrive.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(planviewer2.str().c_str());
  usleep(500000);  //keeps it from stacking the command and rejecting succedding forwarding requests.
  system(mapviewer.str().c_str());

  

  server = skynet_connect(skynetKey, SNGui);
  pthread_create(&skynet_thread, NULL, skynet_main_thread, (void *) server);
  snVehStategroup = skynet_listen(server, SNstate, NULL, sizeof(VehicleState), NULL, NULL, NULL, 0);
  snActStategroup = skynet_listen(server, SNactuatorstate, NULL, sizeof(ActuatorState), NULL, NULL, NULL, 0);  
  
  stateMessagesFlowing = false;  //after first message recieved this will be true- prevents program hanging while planning stack starts up
  actStateMessagesFlowing = false;

  /* Create map element for steering spline */
  steerObj.setTypeSteerCmd();
  steerObj.setId(baseId,3);
  
  /* Create map element for commanded steering spline */
  steerCmdObj.setTypeSteer();
  steerCmdObj.setId(baseId,4);
  
  /* Create map element for alice */
  alice.setTypeAlice();
  alice.setId(baseId,2);

  alicePos = point2(initNorthing, initEasting);
  
  /* Create a traversed path(s) */
  travPath.push_back(alicePos);
  
  /* map element for traversed path */
  travPathObj.setTypeTravPath();
  travPathObjId = 0;
  travPathObj.setId(baseId,1,travPathObjId);

  //Ready out mapelement talker to send to the planning stack runnign this particular Alice
  mapTalker.initSendMapElement(skynetKey);
}

AliceInstance::~AliceInstance()
{
  idCounter++;
}



void AliceInstance::update(MapViewer* map)
{
  // update state only if we know messages have begun flowing (else it will block the program here)
  if (stateMessagesFlowing)
  {
    skynet_get_latest(snVehStategroup, (char*) &m_state, sizeof(VehicleState));
  } 
  else
  {
    m_state.localX = 0;
    m_state.localY = 0;
    m_state.localYaw = initYaw;
    if (skynet_peek(snVehStategroup, NULL))
    {
      stateMessagesFlowing = true;
    }
  }
  if (actStateMessagesFlowing)
  {
    skynet_get_latest(snActStategroup, (char*) &m_actuatorState, sizeof(ActuatorState));
  }
  else
  {
    m_actuatorState.m_steerpos = 0.0;
    m_actuatorState.m_steercmd = 0.0;
    if (skynet_peek(snActStategroup, NULL))
    {
      actStateMessagesFlowing = true;
    }
  }
  
  m_state.localX += initNorthing;
  m_state.localY += initEasting;

  if ((m_state.utmNorthVel == 0) && (m_state.utmEastVel == 0))
  {
    timeStopped += (DGCgettime() - lastRoundTimeStamp);
  }
  else if ((m_state.utmNorthVel != 0) || (m_state.utmEastVel != 0))
  {
    timeStopped = 0;
  }

  lastRoundTimeStamp = DGCgettime();
  
  // send alice object to mapviewer
  alice.setState(m_state);
  map->add_alice(alice);

  // update the steering spline
  steerSplineTemp = genAngleArcSpline(m_actuatorState.m_steerpos);
  steerObj.setGeometry(steerSplineTemp);
  
  map->add_element(steerObj);
  
  // update the commanded steering spline
  steerSplineTemp = genAngleArcSpline(m_actuatorState.m_steercmd);
  steerCmdObj.setGeometry(steerSplineTemp);
  
  map->add_element(steerCmdObj);
  
  // update the traveld path
  if (genTravPathSpline(travPath)) //will be true if we have moved far enough to be worth updating
  {
    travPathObj.setGeometry(travPath);
    map->add_element(travPathObj);
  }

  m_state.localX -= initNorthing;
  m_state.localY -= initEasting;

  broadcastMapElements(map);

}

void AliceInstance::broadcastMapElements(MapViewer* map)
{
  MapElement obj;
  double dfront = DIST_REAR_AXLE_TO_FRONT;
  double drear = DIST_REAR_TO_REAR_AXLE;
  double dside = VEHICLE_WIDTH/2;
  vector<point2> boundary;


  for (unsigned int i = 0; i < map->aliceCollection.size(); i++)
  {
    if (this == map->aliceCollection[i])  //don't report yourself...
    {
      continue;
    }

    AliceInstance* otherAlice = map->aliceCollection[i];
    obj.clear();
    obj.setId(88,-1*otherAlice->baseId);
    boundary.clear();
    
    double vehicleX = otherAlice->m_state.localX - otherAlice->initEasting;
    double vehicleY = otherAlice->m_state.localY - otherAlice->initNorthing;
    double vehicleYaw = otherAlice->m_state.localYaw;
    double relativeX = vehicleX + initEasting;
    double relativeY = vehicleY + initNorthing;
    obj.velocity.x = otherAlice->m_state.utmNorthVel;
    obj.velocity.y = otherAlice->m_state.utmEastVel;
    obj.setState(m_state);
    obj.setTypeVehicle();
    obj.height = 1;
    obj.timeStopped = otherAlice->timeStopped;
    
    double co = cos(vehicleYaw);
    double so = sin(vehicleYaw);

    boundary.push_back(point2(relativeX+(dfront*co -dside*so), relativeY+(dfront*so +dside*co)));
    boundary.push_back(point2(relativeX+(-drear*co -dside*so), relativeY+(-drear*so +dside*co)));
    boundary.push_back(point2(relativeX+(-drear*co +dside*so), relativeY+(-drear*so -dside*co)));
    boundary.push_back(point2(relativeX+(dfront*co +dside*so), relativeY+(dfront*so -dside*co)));
    obj.setGeometry(boundary);
    
    //send to map viewer
    //mapTalker.sendMapElement(&obj, -2);
    //send to mapper
    mapTalker.sendMapElement(&obj, 0);
  }
}

bool AliceInstance::genTravPathSpline(point2arr &travPath)
{
  // update the traversed path
  alicePos.x = m_state.localX;
  alicePos.y = m_state.localY;
  
  // want to space out the points so the vector isn't huge
  
  if ((alicePos - lastpoint).norm() >= 1)
  {
    lastpoint = alicePos;
    if (travPath.size()>200)
    {
      travPathObjId++;
      travPathObjId = (travPathObjId % 250); //limit total number of path segments
      travPathObj.setId(baseId,1,travPathObjId);
      lastpoint = travPath.back();
      travPath.clear();
      travPath.push_back(lastpoint);
    }
      
    travPath.push_back(alicePos);
    
    return true;    
  }
  return false;
}

point2arr AliceInstance::genAngleArcSpline(double theta)
{
  point2arr spline;
  spline.clear();
  double phi = theta;

  phi *= VEHICLE_MAX_AVG_STEER;
  
  if(fabs(phi) < 0.0001)
  {
    phi = 0.0001;
  }
  
  double turningRadius = VEHICLE_WHEELBASE / sin(phi);
  double cn, ce, angle, dangle;
  double lengthArc = VEHICLE_WHEELBASE * 0.5;
  double localYaw = m_state.localYaw;
  double screenyaw = localYaw;
  point2 frontWheels;
  frontWheels.x = m_state.localX + cos(screenyaw) *VEHICLE_WHEELBASE;
  frontWheels.y = m_state.localY + sin(screenyaw) *VEHICLE_WHEELBASE;
  
  // turningRadius has a sign and will make sure the following are correct
  cn = frontWheels.y + turningRadius*sin(M_PI/2.0 -localYaw - phi);
  ce = frontWheels.x  - turningRadius*cos(M_PI/2.0 -localYaw - phi);
  dangle = 2.0 * M_PI * lengthArc / turningRadius / 20.0;
  angle = atan2(frontWheels.x - ce, frontWheels.y - cn);
  
  spline.push_back (point2(frontWheels.x, frontWheels.y));
  
  for(int i=0; i<20; i++)
  {
    angle -= dangle;
    spline.push_back(point2(ce + fabs(turningRadius)*sin(angle),
                         cn + fabs(turningRadius)*cos(angle)));
  }
  return spline;
}
