/*
 *
 *  Author: Chris Schantz
 *  Date: August 23th 2007
 *
 *
 *  MapToolPanel.hh: The control pannel to hold the tools for adding stuff to and manupilating the map
 *
 */

#ifndef MAPTOOLPANEL_HH
#define MAPTOOLPANEL_HH

#include <iostream>
#include <string>
#include <sstream>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_File_Chooser.H>

#include "map/Map.hh"
#include "map/MapElement.hh"
#include "frames/point2_uncertain.hh"

#include "MapViewer.hh"
#include "NewAlicePanel.hh"

class MapToolPanel : public Fl_Window
{
  static void static_newAlice_cb(Fl_Widget* v, void *);
  void newAlice_cb();

  static void static_loadRNDF_cb(Fl_Widget* v, void *);
  void loadRNDF_cb();
 
  static void static_quitButton_cb(Fl_Widget* v, void *);
  //void quitButton_cb();

  MapViewer* viewer;

  public:
  
  MapToolPanel(MapViewer* view);
  ~MapToolPanel();

  void initToolPanel();

  Fl_Button* newAlice;
  Fl_Button* loadRNDF;
  Fl_Button* quitButton;

};



#endif //MAPTOOLPANEL_HH

