/*
 *  DynamicPlanner.cc
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "DynamicPlanner.hh"

CDynamicPlanner:: CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents):CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
	m_i   = 0;
	m_j   = 0;
	m_k   = 0;
	m_ell = 0;
	
	m_Error = 0;
	m_Duration = 0.0;
	
	/* Create an RDDF object */
	m_RDDF = NULL; 				//new RDDF("rddf.dat");
	if(m_RDDF != NULL)
	{
	   cout << "RDDF object was created successfully" << endl;		
	}
		
	/* Create a Traj object (y,yd,ydd,x,xd,xdd) */
	m_Traj = new CTraj(3);
	if(m_Traj != NULL)
	{
	   cout << "Traj object was created successfully" << endl;		
	}
		
	m_CostMapPlus = new CMapPlus();
	if(m_CostMapPlus != NULL)
	{
	   cout << "Cost Map Plus object was created successfully" << endl;		
	}

	m_OCPComponents = OCPComponents;

	/* 
       Create Alice problems: Currently only one is present - Alice_Full
	   In the future there will be a set of Alice problems being created here. 
	*/ 
	m_apType = apt_Full;
  
	m_Alice_Full = new CAlice_Full(m_OCPComponents);
	if(m_Alice_Full != NULL)
	{
	   cout << "Alice OC problem definition was created successfully" << endl;	
	}

	m_ReconfigOCProblem = new CReconfigOCProblem(m_Alice_Full);
	if(m_ReconfigOCProblem != NULL)
	{
	   cout << "Reconfig OC Problem was created successfully" << endl;
	}		
    
	m_OCPSolver = new COCPSolver(m_ReconfigOCProblem);
	if(m_OCPSolver != NULL)
	{
	   cout << "OCP Solver was created successfully" << endl;
	}
    
	m_AlgGeom = new CAlgebraicGeometry();
	
	m_SendSocket = m_skynet.get_send_sock(SNtraj);
		
	m_NofWaypoints   = 0;
	
	m_x_start = 0;
	m_y_start = 0;
	m_v_start = 0;
		
	m_x_stop = 0;
	m_y_stop = 0;
	m_v_stop = 0;
	
	m_theta_start = 0;
    
    m_NofFlatOutputs = m_ReconfigOCProblem->getNofFlatOutputs();

    m_NofDimensionsOfFlatOutputs = new int[m_NofFlatOutputs];
    m_NofControlPoints           = new int[m_NofFlatOutputs];
    m_NofWeights                 = new int[m_NofFlatOutputs];
    
    m_NURBSDataX = nd_NofDimensionsOfFlatOutputs;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs);
    
    m_NURBSDataX = nd_NofControlPoints;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofControlPoints);
    
    m_NURBSDataX = nd_NofWeights;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofWeights);
    
	m_Weights       = new double*[m_NofFlatOutputs];
	m_ControlPoints = new double**[m_NofFlatOutputs];
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		m_ControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			m_ControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
		}
	}

    m_OrderOfPolynomials = new int[m_NofFlatOutputs];
    m_CurveSmoothness    = new int[m_NofFlatOutputs];
        
    m_NURBSDataX = nd_OrderOfPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_OrderOfPolynomials);
    
    m_NURBSDataX = nd_CurveSmoothness;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_CurveSmoothness);
    
    m_FunctionType = ft_Constraint;
    m_NofConstraintCollocPoints = m_ReconfigOCProblem->getNofCollocPoints(&m_FunctionType);
    
	m_NofStates     = m_ReconfigOCProblem->getNofStates();
	m_NofInputs     = m_ReconfigOCProblem->getNofInputs();
	m_NofParameters = m_ReconfigOCProblem->getNofParameters();
    
	m_NofDerivativesOfStates = new int[m_NofStates];
	m_NofDerivativesOfInputs = new int[m_NofInputs];
    
    m_ReconfigOCProblem->getSystemsInfo(&m_NofStates, m_NofDerivativesOfStates, &m_NofInputs, m_NofDerivativesOfInputs);
    
	m_LIC = new double[m_NofStates];
	m_LFC = new double[m_NofStates];
        
	m_Time   = new double[m_NofConstraintCollocPoints];
	m_States = new double**[m_NofConstraintCollocPoints];
	m_Inputs = new double**[m_NofConstraintCollocPoints];
	for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
	{
		m_States[m_i] = new double*[m_NofStates];
		for(m_j=0; m_j<m_NofStates; m_j++)
		{
			m_States[m_i][m_j] = new double[m_NofDerivativesOfStates[m_j]+1];
		}
		
		m_Inputs[m_i] = new double*[m_NofInputs];
		for(m_j=0; m_j<m_NofInputs; m_j++)
		{
			m_Inputs[m_i][m_j] = new double[m_NofDerivativesOfInputs[m_j]+1];
		}
		
	}	
	m_Parameters = new double[m_NofParameters];
}

CDynamicPlanner::~CDynamicPlanner(void)
{
	delete m_RDDF;
	delete m_Traj;
	delete m_CostMapPlus;

	if(m_AlgGeom != NULL)
	{
		delete m_AlgGeom;
	} 

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		delete[] m_Weights[m_i];
	}
	delete[] m_Weights;

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			delete[] m_ControlPoints[m_i][m_j];
		}
		delete[] m_ControlPoints[m_i];
	}
	delete[] m_ControlPoints;
	
	delete[] m_NofDimensionsOfFlatOutputs;
	delete[] m_NofControlPoints;
    delete[] m_NofWeights;
        
	for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
	{
		for(m_j=0; m_j<m_NofStates; m_j++)
        {
            delete[] m_States[m_i][m_j];
        }

		for(m_j=0; m_j<m_NofInputs; m_j++)
        {
			delete[] m_Inputs[m_i][m_j];
		}
		
		delete[] m_States[m_i];
		delete[] m_Inputs[m_i];
	}
	delete[] m_Time;
	delete[] m_States;
	delete[] m_Inputs;
    
    delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
	
	delete[] m_LIC;
	delete[] m_LFC;		
    
    delete m_OCPSolver;
    delete m_ReconfigOCProblem;	
    delete m_Alice_Full;
}

void CDynamicPlanner::ActiveLoop(void)
{
	/* Get the current RDDF from tlanner via RDDFTalker */
	cout << "about to receive an rddf ..."<< endl;
	WaitForRDDF();
	cout << "done waiting ..." << endl;
	UpdateRDDF();
	cout << "done updating ..." << endl;
	m_RDDF = &m_newRddf;
	cout << "done getting RDDF object ..."<< endl;
	cout.flush();
    
	/* Extract the data from the new RDDF file */
    m_NofWaypoints = m_RDDF->getNumTargetPoints();
    m_RDDFVector   = m_RDDF->getTargetPoints();
    
    cout << "No. of Waypoints = " << m_NofWaypoints << endl;
  	cout << "Printing RDDF ... " << endl;
  	m_RDDF->print();
    
	/* Get current state from astate */
	UpdateState();
	
	cout << endl;
	cout << "Current State: " << endl;
	cout << "t   = " << m_state.timestamp << endl;
	cout << "x   = " << m_state.utmEasting   << '\t' << "y   = " << m_state.utmNorthing  << '\t' << "z   = " << m_state.utmAltitude << endl;
	/*
     cout << "xd  = " << m_state.utmNorthVel     << '\t' << "yd  = " << m_state.utmEastVel     << '\t' << "zd  = " << m_state.utmAltitueVel    << endl;
     cout << "p   = " << m_state.utmRoll      << '\t' << "q   = " << m_state.utmPitch     << '\t' << "r   = " << m_state.utmYaw      << endl;
     cout << "pd  = " << m_state.utmRollRate  << '\t' << "qd  = " << m_state.utmPitchRate << '\t' << "rd  = " << m_state.utmYawRate  << endl;
     cout << endl;
     */
	cout.flush();
    
	/* Initialize the trajectory */
	m_Traj->startDataInput();
    
    /* Constuct a feasible region wrt trajectory constraints using the RDDF data being sent by tplanner */
    m_Error = ConstructFeasibleRegion(&m_NofWaypoints, m_RDDFVector, &m_ConvexSets);
    
    /* Construct an inner approximation of the feasible region by a set of overlapping polytopes */
    m_NofConvexSets = m_ConvexSets.getNofPolytopes();
    
    /* Polytope Specification: */
    m_FlatOutput           = 0;
    m_NofDimensions        = m_NofDimensionsOfFlatOutputs[m_FlatOutput]; 
    m_NofPolytopes         = m_NofConvexSets; /* a polytope per convex set given */
    m_NofPointsPerPolytope = m_OrderOfPolynomials[m_FlatOutput];
    m_NofSharingPoints     = m_CurveSmoothness[m_FlatOutput];
    m_NofTotalPoints       = m_NofControlPoints[m_FlatOutput];
    m_NofUnknownPoints     = (m_NofTotalPoints-2)*m_NofDimensions;

    m_InitialPoint = new double[m_NofDimensions];
    m_FinalPoint   = new double[m_NofDimensions];
    m_InitialGuess = new double[m_NofUnknownPoints];
   
    m_NLPProblem = new CNLPProblem(&m_NofConvexSets, &m_ConvexSets, &m_NofPolytopes, &m_NofPointsPerPolytope, &m_NofSharingPoints, m_InitialPoint, m_FinalPoint, m_InitialGuess);
    m_NLPSolver  = new CNLPSolver(m_NLPProblem);

    /* Determine Corridor */
    m_NLPSolver->SolveNLPProblem();   
    m_NLPProblem->getControlPoints(&m_NofDimensions, &m_NofControlPoints[m_FlatOutput], m_ControlPoints[m_FlatOutput]);

    delete[] m_InitialPoint;
    delete[] m_FinalPoint;
    delete[] m_InitialGuess;
    
    m_start_waypoint = 0;
    m_stop_waypoint  = m_NofWaypoints-1;
    
    m_x_start = m_RDDFVector[m_start_waypoint].Easting;
    m_y_start = m_RDDFVector[m_start_waypoint].Northing;
    m_v_start = m_RDDFVector[m_start_waypoint].maxSpeed;
    
    m_x_stop = m_RDDFVector[m_stop_waypoint].Easting;
    m_y_stop = m_RDDFVector[m_stop_waypoint].Northing;
    m_v_stop = m_RDDFVector[m_stop_waypoint].maxSpeed;
	
    m_theta_start = atan2(m_y_stop - m_y_start, m_x_stop - m_x_start);
    
    /* Modify the OC Problem */
    
    /* Set intial and final conditions */
    m_LIC[0] = m_x_start;
    m_LIC[1] = m_y_start;
    m_LIC[2] = m_v_start/2;
    m_LIC[3] = m_theta_start;
    m_LIC[4] = 0;
    m_LIC[5] = 0;    
    m_ConstraintType = ct_LinInitial;
    m_Error = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_NofStates, m_LIC);
    
    m_LFC[0] = m_x_stop;
    m_LFC[1] = m_y_stop;
    m_LFC[2] = m_v_start/2;
    m_LFC[3] = m_theta_start;
    m_LFC[4] = 0;
    m_LFC[5] = 0;
    m_ConstraintType = ct_LinFinal;
    m_Error = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_NofStates, m_LFC);
    
    /* Set Parameters */
    m_Parameters[0]  = 5.43560;		// L
    m_Parameters[1]  = 2.13360;		// W
    m_Parameters[2]  = 1.06680;		// hcg
    m_Parameters[3]  = 0.00000;		// v_min
    m_Parameters[4]  = 1.40800;		// v_max
    m_Parameters[5]  =-3.00000;		// a_min
    m_Parameters[6]  = 0.98100;		// a_max
    m_Parameters[7]  =-0.44942;		// phi_min
    m_Parameters[8]  = 0.44942;		// phi_max
    m_Parameters[9]  =-1.30900;		// phid_min
    m_Parameters[10] = 1.30900;		// phid_max
    m_Parameters[11] = 9.81000;		// g
    m_Parameters[12] = 0.35200;		// vi
    m_Parameters[13] = 0.35200;		// vf
    m_Parameters[14] = 0.00000;		// ai
    m_Parameters[15] = 0.00000;		// af		

    /*! Use to set the parameters at discrete instances of time, useful moving objects, updating new info at it becomes available */ 
    m_i = 0;
    m_t = 0.0;
    
    m_FunctionType = ft_Cost;
    m_Error = m_ReconfigOCProblem->setOCPParameters(&m_i, &m_t, &m_FunctionType, &m_NofParameters, (const double* const) m_Parameters);
    
    m_FunctionType = ft_Constraint;
    m_Error = m_ReconfigOCProblem->setOCPParameters(&m_i, &m_t, &m_FunctionType, &m_NofParameters, (const double* const) m_Parameters);
    
    //m_Alice_Full->getInitialGuess(m_start_waypoint, m_stop_waypoint, m_RDDFVector, m_Weights, m_ControlPoints);
				
    m_SolverInform = 0;
    m_OptimalCost  = 0;
    
    /* Solve OCProblem */
    cout << "----------- NURBSBasedOTG -----------" << endl;
    cout << "Solving Optimal Control Problem ..." << endl;
    m_InitialTime = clock();
    m_OCPSolver->SolveOCProblem(&m_SolverInform, &m_OptimalCost, m_Weights, m_ControlPoints);
    m_FinalTime   = clock();
    cout << "OCP Solver returned with the following flag: " << m_SolverInform << endl; 
    
    m_Duration = (double)(m_FinalTime-m_InitialTime)/CLOCKS_PER_SEC;
    cout << "Time NURBSBasedOTG took to solve the OCProblem  = "<< m_Duration << " secs" << endl;
    cout << "-------------------------------------" << endl;
    cout.flush();
    
    /* Recover Original System */
    m_ReconfigOCProblem->RecoverSystem((const double** const) m_Weights, (const double*** const) m_ControlPoints, m_Time, (double*** const) m_States,(double*** const) m_Inputs);
    cout << "Original system recovered successfully ..." << endl;
    
    /*
    cout << "Recovered System " << endl;
    for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
    {
        cout << m_Time[m_i] << '\t';
        for(m_j=0; m_j<m_NofStates; m_j++)
        {
            for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]+1; m_k++)
            {
                cout << m_States[m_i][m_j][m_k] << '\t';
            }
        }
         
        for(m_j=0; m_j<m_NofInputs; m_j++)
        {
            for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]+1; m_k++)
            {
                cout << m_Inputs[m_i][m_j][m_k] << '\t';
            }
        }
        cout << endl;
    }
    cout << endl;
    */
    
    /* Store the trajectory */
    for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
    {
        m_Traj->addPoint(m_States[m_i][1][0],m_States[m_i][1][1],m_States[m_i][1][2],m_States[m_i][0][0],m_States[m_i][0][1],m_States[m_i][0][2]);
    }
    
	/* Sending  trajectory */
    SendTraj(m_SendSocket, m_Traj);

    /* Free Resources */
    m_ConvexSets.clear();
    return;
}

void CDynamicPlanner::SwitchOCProblem(const AliceProblemType* const apType)
{
	m_apType = *apType;	
}
