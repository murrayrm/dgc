/* 
	AliceFlat.cc
	Melvin E. Flores
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 22-Apr-2007.
*/ 

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "AliceFlat.hh" 

CAliceFlat::CAliceFlat(void)
{
	m_i = 0;
	m_j = 0;
	m_k = 0;

	m_TimeInterval.t0 = 0;
	m_TimeInterval.tf = 1;

	m_ocpSizes.NofFlatOutputs                = 2;
	m_ocpSizes.HasICostFunction              = false;
	m_ocpSizes.HasTCostFunction              = false;
	m_ocpSizes.HasFCostFunction              = true;
	m_ocpSizes.NofILinConstraints            = 2;
	m_ocpSizes.HasVariableTLinConstraints    = false;
	m_ocpSizes.NofTLinConstraints            = 1;
	m_ocpSizes.NofFLinConstraints            = 2;
	m_ocpSizes.NofINLinConstraints           = 1;
	m_ocpSizes.HasVariableTNLinConstraints   = false;
	m_ocpSizes.NofTNLinConstraints           = 5;
	m_ocpSizes.NofFNLinConstraints           = 1;
	m_ocpSizes.NofStates                     = 4;
	m_ocpSizes.NofInputs                     = 2;
	m_ocpSizes.NofParameters                 = 1;
	m_ocpSizes.HasVariableParameters         = false;
	m_ocpSizes.NofReferences                 = 0;
	m_ocpSizes.HasVariableReferences         = false;
	m_ocpSizes.IntegrationType               = qt_Trapezoidal;
	m_ocpSizes.NofCostCollocPoints           = 21;
	m_ocpSizes.NofConstraintCollocPoints     = 31;
	m_ocpSizes.HasRecoverSystem              = true;
    m_ocpSizes.HasConstraintsOnWeights       = false;
    m_ocpSizes.HasConstraintsOnControlPoints = false;
    
	m_Parameters = new double[m_ocpSizes.NofParameters];
	m_Parameters[0] = 5.4356;

	m_NofVariables = 9;

	m_Ai = new double*[m_ocpSizes.NofILinConstraints];
	m_li = new double[m_ocpSizes.NofILinConstraints];
	m_ui = new double[m_ocpSizes.NofILinConstraints];
	for(m_i=0; m_i<m_ocpSizes.NofILinConstraints; m_i++)
	{
		m_Ai[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++)
		{
			m_Ai[m_i][m_j] = 0.0;
		}
	}

	m_Ai[0][0]= 1.0;
	m_Ai[1][4]= 1.0;

    /* Left Turn */
    m_li[0] =   3.0;
	m_li[1] = -15.0;

	m_ui[0] =   3.0;
	m_ui[1] = -15.0;

	m_At = new double**[m_ocpSizes.NofConstraintCollocPoints];
	m_lt = new double* [m_ocpSizes.NofConstraintCollocPoints];
	m_ut = new double* [m_ocpSizes.NofConstraintCollocPoints];
	for(m_i=0; m_i<m_ocpSizes.NofConstraintCollocPoints; m_i++)
	{
		m_At[m_i] = new double*[m_ocpSizes.NofTLinConstraints];
		m_lt[m_i] = new double [m_ocpSizes.NofTLinConstraints];
		m_ut[m_i] = new double [m_ocpSizes.NofTLinConstraints];
		for(m_j=0; m_j<m_ocpSizes.NofTLinConstraints; m_j++)
		{
			m_At[m_i][m_j] = new double[m_NofVariables];
			for(m_k=0; m_k<m_NofVariables; m_k++)
			{
				m_At[m_i][m_j][m_k] = 0.0;
			}
		}
	}

	for(m_k = 0; m_k < m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		m_At[m_k][0][8]= 1.0;
	}

	for(m_k = 0; m_k < m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		m_lt[m_k][0] =   0.01;  /* tf_min */
		m_ut[m_k][0] = 100.00;  /* tf_max */
	}

	m_Af = new double*[m_ocpSizes.NofFLinConstraints];
	m_lf = new double[m_ocpSizes.NofFLinConstraints];
	m_uf = new double[m_ocpSizes.NofFLinConstraints];
	for(m_i=0; m_i<m_ocpSizes.NofFLinConstraints; m_i++)
	{
		m_Af[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++)
		{
			m_Af[m_i][m_j] = 0.0;
		}
	}

	m_Af[0][0]= 1.0;
	m_Af[1][4]= 1.0;

    /* Left Turn */
	m_lf[0] = -7.0;
	m_lf[1] =  7.0;

	m_uf[0] = -7.0;
	m_uf[1] =  7.0;

	m_Li = new double[m_ocpSizes.NofINLinConstraints];
	m_Ui = new double[m_ocpSizes.NofINLinConstraints];

    /* Left Turn */
	m_Li[0] =  M_PI/2.0;                      /* thetai */
	m_Ui[0] =  M_PI/2.0;                      /* thetai */

	m_Lt = new double*[m_ocpSizes.NofConstraintCollocPoints];
	m_Ut = new double*[m_ocpSizes.NofConstraintCollocPoints];
	for(m_i=0; m_i<m_ocpSizes.NofConstraintCollocPoints; m_i++)
	{
		m_Lt[m_i] = new double[m_ocpSizes.NofTNLinConstraints];
		m_Ut[m_i] = new double[m_ocpSizes.NofTNLinConstraints];
	}

	for(m_k = 0; m_k < m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		m_Lt[m_k][0] =  0.1;          /* vmin */
		m_Lt[m_k][1] = -3;           /* amin */
		m_Lt[m_k][2] = -0.44942;     /* phimin */
		m_Lt[m_k][3] = -1.309;       /* phidmin */
		m_Lt[m_k][4] = -9.81;

		m_Ut[m_k][0] = 26.8224;      /* vmax */
		m_Ut[m_k][1] = 0.981;        /* amax */
		m_Ut[m_k][2] = 0.44942;      /* phimax */
		m_Ut[m_k][3] = 1.309;        /* phidmax */
		m_Ut[m_k][4] = 9.81;
	}

	m_Lf = new double[m_ocpSizes.NofFNLinConstraints];
	m_Uf = new double[m_ocpSizes.NofFNLinConstraints];

    /* Left Turn */
	m_Lf[0] = M_PI;               /* thetaf */
	m_Uf[0] = M_PI;               /* thetaf */

	m_NofDerivativesOfStates = new int[m_ocpSizes.NofStates];
	m_NofDerivativesOfInputs = new int[m_ocpSizes.NofInputs];

	m_NofDerivativesOfStates[0] = 3;
	m_NofDerivativesOfStates[1] = 3;
	m_NofDerivativesOfStates[2] = 2;
	m_NofDerivativesOfStates[3] = 2;

	m_NofDerivativesOfInputs[0] = 1;
	m_NofDerivativesOfInputs[1] = 2;
}

CAliceFlat::~CAliceFlat(void)
{
	delete[] m_Parameters;

	for(m_i=0; m_i<m_ocpSizes.NofILinConstraints; m_i++)
	{
		delete[] m_Ai[m_i];
	}
	delete[] m_Ai;
	delete[] m_li;
	delete[] m_ui;

	for(m_k=0; m_k<m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		for(m_i=0; m_i<m_ocpSizes.NofTLinConstraints; m_i++)
		{
			delete[] m_At[m_k][m_i];
		}
		delete[] m_At[m_k];
		delete[] m_lt[m_k];
		delete[] m_ut[m_k];
	}
	delete[] m_At;
	delete[] m_lt;
	delete[] m_ut;

	for(m_i=0; m_i<m_ocpSizes.NofFLinConstraints; m_i++)
	{
		delete[] m_Af[m_i];
	}
	delete[] m_Af;
	delete[] m_lf;
	delete[] m_uf;

	delete[] m_Li;
	delete[] m_Ui;

	for(m_k=0; m_k<m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		delete[] m_Lt[m_k];
		delete[] m_Ut[m_k];
	}
	delete[] m_Lt;
	delete[] m_Ut;

	delete[] m_Lf;
	delete[] m_Uf;

	delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
}

void CAliceFlat::getTimeInterval(TimeInterval* const timeInterval)
{
	timeInterval->t0 = m_TimeInterval.t0;
	timeInterval->tf = m_TimeInterval.tf;
}

void CAliceFlat::getOCPSizes(OCPSizes* const ocpSizes)
{
	ocpSizes->NofFlatOutputs            = m_ocpSizes.NofFlatOutputs;
	ocpSizes->HasICostFunction          = m_ocpSizes.HasICostFunction;
	ocpSizes->HasTCostFunction          = m_ocpSizes.HasTCostFunction;
	ocpSizes->HasFCostFunction          = m_ocpSizes.HasFCostFunction;
	ocpSizes->NofILinConstraints        = m_ocpSizes.NofILinConstraints;
	ocpSizes->NofTLinConstraints        = m_ocpSizes.NofTLinConstraints;
	ocpSizes->NofFLinConstraints        = m_ocpSizes.NofFLinConstraints;
	ocpSizes->NofINLinConstraints       = m_ocpSizes.NofINLinConstraints;
	ocpSizes->NofTNLinConstraints       = m_ocpSizes.NofTNLinConstraints;
	ocpSizes->NofFNLinConstraints       = m_ocpSizes.NofFNLinConstraints;
	ocpSizes->NofStates                 = m_ocpSizes.NofStates;
	ocpSizes->NofInputs                 = m_ocpSizes.NofInputs;
	ocpSizes->NofParameters             = m_ocpSizes.NofParameters;
	ocpSizes->HasVariableParameters     = m_ocpSizes.HasVariableParameters;
	ocpSizes->NofReferences             = m_ocpSizes.NofReferences;
	ocpSizes->HasVariableReferences     = m_ocpSizes.HasVariableReferences;
	ocpSizes->IntegrationType           = m_ocpSizes.IntegrationType;
	ocpSizes->NofIntegrationPoints      = m_ocpSizes.NofIntegrationPoints;
	ocpSizes->NofCostCollocPoints       = m_ocpSizes.NofCostCollocPoints;
	ocpSizes->NofConstraintCollocPoints = m_ocpSizes.NofConstraintCollocPoints;
	ocpSizes->HasRecoverSystem          = m_ocpSizes.HasRecoverSystem;
}

void CAliceFlat::getNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data)
{
	if(*dataType == dt_NofDimensionsOfFlatOutputs)
	{
		Data[0] = 2;
		Data[1] = 1;
	}
	else if(*dataType == dt_SolveForControlPoints)
	{
		Data[0] = 1;
        Data[1] = 1;

	}
	else if(*dataType == dt_SolveForWeights)
	{
		Data[0] = 0;
        Data[1] = 0;
	}
	else if(*dataType == dt_NofPolynomials)
	{
		Data[0] = 4;
		Data[1] = 1;
	}
	else if(*dataType == dt_DegreeOfPolynomials)
	{
		Data[0] = 6;
		Data[1] = 0;
	}
	else if(*dataType == dt_CurveSmoothness)
	{
		Data[0] = 3;
		Data[1] = -1;
	}
	else //if (*dataType == dt_MaxDerivatives)
	{
		Data[0] = 3;
		Data[1] = 0;
	}
}

void CAliceFlat::getBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints)
{
	for(m_i=0; m_i<*NofFlatOutputs; m_i++)
	{
		CreateTimeVector(timeInterval, &NofBreakPoints[m_i], BreakPoints[m_i]);
	}
}

void CAliceFlat::getCollocPoints(const FunctionType* const functionType, const TimeInterval* const timeInterval, const int* const NofCollocPoints, double* const CollocPoints)
{
	CreateTimeVector(timeInterval, NofCollocPoints, CollocPoints);
}

void CAliceFlat::getOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters)
{
	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		Parameters[m_i] = m_Parameters[m_i];
	}
}

void CAliceFlat::getOCPReferences(const int* const i, const double* const t, const int* const NofReferences, double* const References)
{

}

void CAliceFlat::getSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs)
{
	for(m_i = 0; m_i<*NofStates; m_i++)
	{
		NofDerivativesOfStates[m_i] = m_NofDerivativesOfStates[m_i];
	}

	for(m_i = 0; m_i<*NofInputs; m_i++)
	{
		NofDerivativesOfInputs[m_i] = m_NofDerivativesOfInputs[m_i];
	}
}

void CAliceFlat::RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u)
{

	*t = *tau*z3;

	{
		double t1 = 1/z3;
		double t3 = z3*z3;      
		double t4 = 1/t3;      
		double t8 = z1d*z1d;      
		double t9 = z2d*z2d;      
		double t10 = t8+t9;      
		double t11 = sqrt(t10);      
		double t17 = atan2(z2d,z1d);      

		x1   = z1;                                      // x
		x1d  = z1d*t1;        
		x1dd = z1dd*t4;      
		x2   = z2;                                      // y
		x2d  = z2d*t1;      
		x2dd = z2dd*t4;      
		x3   = t11*t1;                                  // v
		x3d  = ((z1d*z1dd+z2d*z2dd)/t11)*t4;      
		x4   = t17;                                     // theta
		x4d  = ((z1d*z2dd-z2d*z1dd)/t10);      
        
        x1dd = cos(x4)*x3d-sin(x4)*x3*x4d;
        x2dd = sin(x4)*x3d+cos(x4)*x3*x4d;
	}

	{
		double t4 = z1d*z1d;
		double t5 = z2d*z2d;      
		double t6 = t4+t5;      
		double t7 = sqrt(t6);      
		double t9 = ((z1d*z1dd+z2d*z2dd)/t7)*(1/(z3*z3));      
		double t12 = z1d*z2dd-z2d*z1dd;      
		double t13 = p1*t12;      
		double t15 = atan2(t13,t7*t6);      
		double t16 = t6*t6;      
		double t18 = p1*p1;      
		double t19 = t12*t12;      

		u1  = t9;                                                                              // a    
		u2  = t15;                                                                             // phi   
		u2d = (t6/(t16*t6+t18*t19)*(p1*t7*(-z2d*z1ddd+z1d*z2ddd)-3.0*t13*t9));      //phid
	}
}

void CAliceFlat::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{

}

void CAliceFlat::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
}

void CAliceFlat::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
	double tf_weight = 1.0;
	if(*mode == 0 || *mode == 2)
	{
		*Ff = tf_weight*z3;
	}

	if(*mode == 1 || *mode == 2)
	{

		DFf[8] = tf_weight*1.0;      
	}
}

void CAliceFlat::getLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A)
{
	if(*constraintType == ct_LinInitial)
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				A[m_j][m_k] = m_Ai[m_j][m_k];
			}
		}
	}
	else if(*constraintType == ct_LinTrajectory)
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				A[m_j][m_k] = m_At[*i][m_j][m_k];
			}
		}
	}
	else /*if(*constraintType == ct_LinFinal)*/
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				A[m_j][m_k] = m_Af[m_j][m_k];
			}
		}
	}
}

void CAliceFlat::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli)
{

	if(*mode == 0 || *mode == 2)
	{
		Cnli[0] = atan2(z2d,z1d);
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = 1/t1;      
		double t4 = z2d*z2d;      
		double t7 = 1/(t4*t2+1.0);      

		DCnli[0][1] = -z2d*t2*t7;      
		DCnli[0][5] = 1/z1d*t7;      
	}

	if(*mode == 99)
	{
		DCnli[0][1] = 1.0; 
		DCnli[0][5] = 1.0; 
	}

}

void CAliceFlat::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt)
{

	if(*mode == 0 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t10 = 1/t4;      
		double t11 = (z1d*z1dd+z2d*z2dd)*t10;      
		double t12 = z3*z3;      
		double t13 = 1/t12;      
		double t17 = z1d*z2dd-z2d*z1dd;      
		double t18 = p1*t17;      
		double t20 = atan2(t18,t4*t3);      
		double t21 = t3*t3;      
		double t23 = p1*p1;      
		double t24 = t17*t17;      

		Cnlt[0] = t4/z3;      
		Cnlt[1] = t11*t13;      
		Cnlt[2] = t20;      
		Cnlt[3] = t3/(t21*t3+t23*t24)*(p1*t4*(-z2d*z1ddd+z1d*z2ddd)-3.0*t18*t11);      
		Cnlt[4] = t17*t10*t13;      
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = 1/t4;      
		double t7 = t5/z3;      
		double t10 = z3*z3;      
		double t11 = 1/t10;      
		double t13 = z1dd*t5;      
		double t14 = t11*t13;      
		double t17 = z1d*z1dd+z2d*z2dd;      
		double t18 = t4*t3;      
		double t19 = 1/t18;      
		double t20 = t17*t19;      
		double t21 = t11*z1d;      
		double t24 = z1d*t5;      
		double t25 = t24*t11;      
		double t26 = z2dd*t5;      
		double t27 = t26*t11;      
		double t28 = t11*z2d;      
		double t31 = z2d*t5;      
		double t32 = t31*t11;      
		double t33 = t17*t5;      
		double t35 = 1/t10/z3;      
		double t38 = p1*z2dd;      
		double t42 = z1d*z2dd-z2d*z1dd;      
		double t43 = p1*t42;      
		double t44 = t3*t3;      
		double t46 = 1/t4/t44;      
		double t51 = p1*p1;      
		double t52 = t42*t42;      
		double t53 = t51*t52;      
		double t54 = t44*t3;      
		double t58 = 1/(1.0+t53/t54);      
		double t60 = p1*z2d;      
		double t61 = t19*t58;      
		double t63 = p1*z1dd;      
		double t70 = p1*z1d;      
		double t72 = t54+t53;      
		double t73 = 1/t72;      
		double t75 = p1*t4;      
		double t78 = -z2d*z1ddd+z1d*z2ddd;      
		double t82 = t75*t78-3.0*t43*t33;      
		double t85 = t72*t72;      
		double t87 = t3/t85;      
		double t90 = t51*t42;      
		double t96 = t3*t73;      
		double t97 = p1*t5;      
		double t111 = t87*t82;      
		double t120 = t18*t73;      
		double t154 = t42*t19;      

		DCnlt[0][1] = t7*z1d;      
		DCnlt[0][5] = t7*z2d;      
		DCnlt[0][8] = -t4*t11;      
		DCnlt[1][1] = t14-t20*t21;      
		DCnlt[1][2] = t25;      
		DCnlt[1][5] = t27-t20*t28;      
		DCnlt[1][6] = t32;      
		DCnlt[1][8] = -2.0*t33*t35;      
		DCnlt[2][1] = (t38*t19-3.0*t43*t46*z1d)*t58;      
		DCnlt[2][2] = -t60*t61;      
		DCnlt[2][5] = (-t63*t19-3.0*t43*t46*z2d)*t58;      
		DCnlt[2][6] = t70*t61;      
		DCnlt[3][1] = 2.0*z1d*t73*t82-t87*t82*(6.0*t44*z1d+2.0*t90*z2dd)+t96*(t97*t78*z1d+t75*z2ddd-3.0*t38*t33-3.0*t43*t13+3.0*t43*t20*z1d);      
		DCnlt[3][2] = 2.0*t111*t90*z2d+3.0*t96*(t60*t33-t43*t24);      
		DCnlt[3][3] = -t120*t60;      
		DCnlt[3][5] = 2.0*z2d*t73*t82-t87*t82*(6.0*t44*z2d-2.0*t90*z1dd)+t96*(t97*t78*z2d-t75*z1ddd+3.0*t63*t33-3.0*t43*t26+3.0*t43*t20*z2d);      
		DCnlt[3][6] = -2.0*t111*t90*z1d+3.0*t96*(-t70*t33-t43*t31);      
		DCnlt[3][7] = t120*t70;      
		DCnlt[4][1] = t27-t154*t21;      
		DCnlt[4][2] = -t32;      
		DCnlt[4][5] = -t14-t154*t28;      
		DCnlt[4][6] = t25;      
		DCnlt[4][8] = -2.0*t42*t5*t35;      
	}

	if(*mode == 99)
	{
		DCnlt[0][1] = 1.0; 
		DCnlt[0][5] = 1.0; 
		DCnlt[0][8] = 1.0; 
		DCnlt[1][1] = 1.0; 
		DCnlt[1][2] = 1.0; 
		DCnlt[1][5] = 1.0; 
		DCnlt[1][6] = 1.0; 
		DCnlt[1][8] = 1.0; 
		DCnlt[2][1] = 1.0; 
		DCnlt[2][2] = 1.0; 
		DCnlt[2][5] = 1.0; 
		DCnlt[2][6] = 1.0; 
		DCnlt[3][1] = 1.0; 
		DCnlt[3][2] = 1.0; 
		DCnlt[3][3] = 1.0; 
		DCnlt[3][5] = 1.0; 
		DCnlt[3][6] = 1.0; 
		DCnlt[3][7] = 1.0; 
		DCnlt[4][1] = 1.0; 
		DCnlt[4][2] = 1.0; 
		DCnlt[4][5] = 1.0; 
		DCnlt[4][6] = 1.0; 
		DCnlt[4][8] = 1.0; 
	}

}

void CAliceFlat::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf)
{
	if(*mode == 0 || *mode == 2)
	{
		Cnlf[0] = atan2(z2d,z1d);
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = 1/t1;      
		double t4 = z2d*z2d;      
		double t7 = 1/(t4*t2+1.0);      

		DCnlf[0][1] = -z2d*t2*t7;      
		DCnlf[0][5] = 1/z1d*t7;      
	}

	if(*mode == 99)
	{
		DCnlf[0][1] = 1.0; 
		DCnlf[0][5] = 1.0; 
	}

}

void CAliceFlat::getLUBounds(const ConstraintType* const constraintType, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, double* const LUBounds)
{

	if(*constraintType == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_li[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_ui[m_i];
			}
		}
	}

	if(*constraintType == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_lt[*i][m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_ut[*i][m_j];
			}
		}
	}

	if(*constraintType == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_lf[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_uf[m_i];
			}
		}
	}

	if(*constraintType == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_Li[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_Ui[m_i];
			}
		}
	}

	if(*constraintType == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Lt[*i][m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Ut[*i][m_j];
			}
		}
	}

	if(*constraintType == ct_NLinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_Lf[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_Uf[m_i];
			}
		}
	}

}

int CAliceFlat::setTimeInterval(const TimeInterval* const timeInterval)
{
	m_TimeInterval.t0 = timeInterval->t0;
	m_TimeInterval.tf = timeInterval->tf;
	return 0;
}

int CAliceFlat::setStates(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const NofStates, const double* const States)
{
	if(*constraintType == ct_LinInitial)
	{
        if(*boundsType == ubt_LowerBound)
        {
           m_li[0] = States[0];  /* xi */
           m_li[1] = States[1];  /* yi */
        }
        else
        {
          m_ui[0] = States[0];   /* xi */
          m_ui[1] = States[1];   /* yi */
        }
	}

	if(*constraintType == ct_LinTrajectory)
	{

	}

	if(*constraintType == ct_LinFinal)
	{
        if(*boundsType == ubt_LowerBound)
        {
           m_lf[0] = States[0];  /* xf */
           m_lf[1] = States[1];  /* yf */
        }
        else
        {
          m_uf[0] = States[0];   /* xf */
          m_uf[1] = States[1];   /* yi */
        }
	}

	if(*constraintType == ct_NLinInitial)
	{
        if(*boundsType == ubt_LowerBound)
        {
            m_Li[0] = States[3];               /* thetai */
        }
        else
        {
            m_Ui[0] = States[3];               /* thetaf */
        }
	}

	if(*constraintType == ct_NLinTrajectory)
	{

	}

	if(*constraintType == ct_NLinFinal)
	{
        if(*boundsType == ubt_LowerBound)
        {
            m_Lf[0] = States[3];               /* thetaf */
        }
        else
        {
            m_Uf[0] = States[3];               /* thetaf */
        }
    }

	return 0;
}

int CAliceFlat::setInputs(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const NofInputs, const double* const Inputs)
{
	if(*constraintType == ct_LinInitial)
	{

	}

	if(*constraintType == ct_LinTrajectory)
	{

	}

	if(*constraintType == ct_LinFinal)
	{

	}

	if(*constraintType == ct_NLinInitial)
	{

	}

	if(*constraintType == ct_NLinTrajectory)
	{

	}

	if(*constraintType == ct_NLinFinal)
	{

	}

	return 0;
}

int CAliceFlat::setOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, const double* const Parameters)
{

	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		m_Parameters[m_i] = Parameters[m_i];
	}

	return 0;
}

int CAliceFlat::setOCPReferences(const int* const i, const double* const t, const int* const NofReferences, const double* const References)
{

	return 0;
}

int CAliceFlat::setLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, const double** const A)
{
	if(*constraintType == ct_LinInitial)
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				m_Ai[m_i][m_j] = A[m_i][m_j];
			}
		}
	}
	else if(*constraintType == ct_LinTrajectory)
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				m_At[*i][m_j][m_k] = A[m_j][m_k];
			}
		}
	}
	else /*if(*constraintType == ct_LinFinal)*/
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				m_Af[m_j][m_k] = A[m_j][m_k];
			}
		}
	}
	return 0;
}

int CAliceFlat::setLUBounds(const ConstraintType* const constraintType, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, const double* const LUBounds)
{
	if(*constraintType == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_li[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_ui[m_i] = LUBounds[m_i];
			}
		}
	}

	if(*constraintType == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_lt[*i][m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_ut[*i][m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_lf[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_uf[m_i] = LUBounds[m_i];
			}
		}
	}

	if(*constraintType == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_Li[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_Ui[m_i] = LUBounds[m_i];
			}
		}
	}

	if(*constraintType == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Lt[*i][m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Ut[*i][m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_NLinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_Lf[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_Uf[m_i] = LUBounds[m_i];
			}
		}
	}

	return 0;
}

int CAliceFlat::setNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, const int* const Data)
{
	return 0;
}

void CAliceFlat::getWeightSizes(WeightSizes* const weightSizes)
{

}

void CAliceFlat::getLinearConstraintsOnWeights(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{

}

void CAliceFlat::InitialNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli,const int* const Offset)
{

}

void CAliceFlat::TrajectoryNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{

}

void CAliceFlat::FinalNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{

}

void CAliceFlat::getControlPointSizes(ControlPointSizes* const controlPointSizes)
{

}

void CAliceFlat::getLinearConstraintsOnControlPoints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{

}

void CAliceFlat::InitialNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset)
{

}

void CAliceFlat::TrajectoryNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{

}

void CAliceFlat::FinalNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{

}
