/* 
	DynamicPlanner_Utilities.cc
	DynamicPlanner
	
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-14-2006.
    Latest Modified on 3-20-2007.
*/ 

#define _USE_MATH_DEFINES

#ifdef __cplusplus
extern "C" {
#endif

#include "DynamicPlanner_Math.h"

#ifdef __cplusplus
}
#endif

#include <math.h>
#include <iostream>
#include <iomanip>
#include <time.h>

#include "DynamicPlanner_Utilities.hh"
#include "AlgGeom/AlgebraicGeometry.hh"

using namespace std;

int ConstructFeasibleRegion(const int* const NofWaypoints, 
                            RDDFVector *rddfvector, 
                            double*  const DataOffsets,  
                            double** const DataPoints, 
                            const int* const NofConvexSets,
                            CPolytope* const ConvexSets)
{
	int i   = 0;
    int j   = 0;
	int k   = 0;
	int ell = 0;
    int s   = 0;    
	
	int NofSides      = 6; /* How many points should be place in a circle */
	int NofDimensions = 2;

	double slope  = 0.0;
	double theta  = 0.0;
	double offset = 0.0;
    double th     = 0.0; 
       
    double delta_th      = 2.0*M_PI/(double)NofSides;
    double HalfNofPoints = (NofSides/2)+1;    
    
    int start_waypoint = 0;
    int stop_waypoint  = 0;
    
 	double x_start = 0.0;
	double y_start = 0.0;
	double r_start = 0.0;

	double x_stop = 0.0;
	double y_stop = 0.0;
       
    DataOffsets[0] = (*rddfvector)[0].Easting;
    DataOffsets[1] = (*rddfvector)[0].Northing;   
	for(i=1; i<*NofWaypoints; i++)
	{
	    if((*rddfvector)[i].Easting < DataOffsets[0])
	    {
	       DataOffsets[0] = (*rddfvector)[i].Easting;
	    }

	    if((*rddfvector)[i].Northing < DataOffsets[1])
	    {
	       DataOffsets[1] = (*rddfvector)[i].Northing;
	    }
	}

    int* NofPoints = new int[*NofConvexSets];
    for(i = 0; i < *NofConvexSets; i++)
    {
        NofPoints[i] = NofSides + 2;
    }    
    
    double*** Points = new double**[*NofConvexSets];
    for(i = 0; i < *NofConvexSets; i++)
    {
        Points[i] = new double*[NofDimensions];
        for(j = 0; j < NofDimensions; j++)
        {
            Points[i][j] = new double[NofPoints[i]];
        }
    }
     
    CAlgebraicGeometry* AlgGeom = new CAlgebraicGeometry(); 
    for(ell=0; ell<*NofConvexSets; ell++)
    {
        start_waypoint = ell;
        stop_waypoint  = ell + 1;
        
        x_start = (*rddfvector)[start_waypoint].Easting  - DataOffsets[0];
        y_start = (*rddfvector)[start_waypoint].Northing - DataOffsets[1];
        r_start = (*rddfvector)[start_waypoint].radius;
        
        x_stop = (*rddfvector)[stop_waypoint].Easting  - DataOffsets[0];
        y_stop = (*rddfvector)[stop_waypoint].Northing - DataOffsets[1];
        
        slope = (y_stop - y_start)/(x_stop - x_start);
        theta = atan2(y_stop - y_start,x_stop - x_start);

        k = 0;
        offset = M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][0][k] = x_start + r_start*cos(th);
            k = k + 1;
        }
        
        offset = -M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][0][k] = x_stop + r_start*cos(th);
            k = k + 1;
        }
        
        k = 0;
        offset = M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][1][k] = y_start + r_start*sin(th);
            k = k + 1;
        }
        
        offset = -M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][1][k] = y_stop + r_start*sin(th);
            k = k + 1;
        }     
    
        /* Construct the polytopes */			
        ConvexSets[ell] = new CPolytope(&NofDimensions, &NofPoints[ell], (const double** const) Points[ell]);        
        AlgGeom->VertexAndFacetEnumeration(&ConvexSets[ell]);		
        AlgGeom->ComputeVolume(&ConvexSets[ell]);
        AlgGeom->ComputeCentroid(&ConvexSets[ell]);
        AlgGeom->ComputeBoundingBox(&ConvexSets[ell]); 
   }
   
    for(ell=0; ell<*NofWaypoints; ell++)
    {
        DataPoints[0][ell] = (*rddfvector)[ell].Easting  - DataOffsets[0];
        DataPoints[1][ell] = (*rddfvector)[ell].Northing - DataOffsets[1];    
    }
    
   /* Restore Resources */
   delete AlgGeom;
    
   for(i = 0; i < *NofConvexSets; i++)
   {
       for(j = 0; j < NofDimensions; j++){delete[] Points[i][j];}
       delete[] Points[i];
   }
   delete[] Points;
   delete[] NofPoints;
    
   return 0;
}

int ConstructFeasibleRegion(const int* const NofWaypoints, 
                            const double** const RDDFData, 
                            double*  const DataOffsets, 
                            double** const DataPoints,
                            const int* const NofConvexSets,
                            CPolytope* const ConvexSets)
{
	int i   = 0;
    int j   = 0;
	int k   = 0;
	int ell = 0;
    int s   = 0;    
	
    ALGErrorType ErrorFlag = alg_NoError;
    
	int NofSides      = 6; /* How many points should be place in a circle */
	int NofDimensions = 2;

	double slope  = 0.0;
	double theta  = 0.0;
	double offset = 0.0;
    double th     = 0.0; 
       
    double delta_th      = 2.0*M_PI/(double)NofSides;
    double HalfNofPoints = (NofSides/2)+1;    
    
    int start_waypoint = 0;
    int stop_waypoint  = 0;
    
 	double x_start = 0.0;
	double y_start = 0.0;
	double r_start = 0.0;

	double x_stop = 0.0;
	double y_stop = 0.0;
   
    /* Determine the offset values to scale the RDDF data */           
    /* 
       This scaling will fail if the smallest values are zero. 
       Must write code to avoid this pitfall. 
    */

    DataOffsets[0] = 0.0;
	DataOffsets[1] = 0.0;

    /*
    DataOffsets[0] = RDDFData[0][1];
	DataOffsets[1] = RDDFData[0][2];
	for(i=1; i<*NofWaypoints; i++)
	{
	    if(RDDFData[i][1] < DataOffsets[0])
	    {
	       DataOffsets[0] = RDDFData[i][1];
	    }

	    if(RDDFData[i][2] < DataOffsets[1])
	    {
	       DataOffsets[1]= RDDFData[i][2];
	    }
	}
    */
    
    int* NofPoints = new int[*NofConvexSets];
    for(i = 0; i < *NofConvexSets; i++){NofPoints[i] = NofSides + 2;}    
    
    double*** Points = new double**[*NofConvexSets];
    for(i = 0; i < *NofConvexSets; i++)
    {
        Points[i] = new double*[NofDimensions];
        for(j = 0; j < NofDimensions; j++){Points[i][j] = new double[NofPoints[i]];}
    }
     
    CAlgebraicGeometry* AlgGeom = new CAlgebraicGeometry();    
    for(ell=0; ell<*NofConvexSets; ell++)
    {
        start_waypoint = ell;
        stop_waypoint  = ell + 1;
        
        x_start = RDDFData[start_waypoint][1] - DataOffsets[0];
        y_start = RDDFData[start_waypoint][2] - DataOffsets[1];
        r_start = RDDFData[start_waypoint][3];

        x_stop = RDDFData[stop_waypoint][1] - DataOffsets[0]; 
        y_stop = RDDFData[stop_waypoint][2] - DataOffsets[1];
      
        slope = (y_stop - y_start)/(x_stop - x_start);
        theta = atan2(y_stop - y_start,x_stop - x_start);
        
        k = 0;
        offset = M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][0][k] = x_start + r_start*cos(th);
            k = k + 1;
        }
        
        offset = -M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][0][k] = x_stop + r_start*cos(th);
            k = k + 1;
        }
        
        k = 0;
        offset = M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][1][k] = y_start + r_start*sin(th);
            k = k + 1;
        }
        
        offset = -M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][1][k] = y_stop + r_start*sin(th);
            k = k + 1;
        }     
		
	    /* Construct the polytopes */			
        ConvexSets[ell] = new CPolytope(&NofDimensions, &NofPoints[ell], (const double** const) Points[ell]);
        ErrorFlag = AlgGeom->FacetEnumeration(&ConvexSets[ell]);
        if(ErrorFlag == alg_NoError)
        {
            ErrorFlag =AlgGeom->VertexEnumeration(&ConvexSets[ell]);
            if(ErrorFlag == alg_NoError)
            {
                AlgGeom->ComputeVolume(&ConvexSets[ell]);
                AlgGeom->ComputeCentroid(&ConvexSets[ell]);
                AlgGeom->ComputeBoundingBox(&ConvexSets[ell]); 
            }
        }
    }

    for(j = 0; j < NofDimensions; j++)
    {    
        for(ell=0; ell<*NofWaypoints; ell++)
        {
            DataPoints[j][ell] = RDDFData[ell][j+1] - DataOffsets[j];   
        }
    }

    /* Restore Resources */        
    delete AlgGeom;

    for(i = 0; i < *NofConvexSets; i++)
    {
        for(j = 0; j < NofDimensions; j++){delete[] Points[i][j];}
        delete[] Points[i];
    }
    delete[] Points;
    delete[] NofPoints;
    
   	return 0;
}

