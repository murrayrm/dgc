/*
 *  DynamicPlanner_Main.cpp
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/21/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "cmdline.h"
#include "skynet/skynet.hh"
#include "NURBSBasedOTG/OTG_OCPComponents.hh" 
#include "DynamicPlanner.hh"

using namespace std;

/*! This is the entry point to the dplanner. */
int main(int argc, char* argv[])
{ 
	/* Process command line arguments */
	gengetopt_args_info cmdline;
	if(cmdline_parser(argc, argv, &cmdline) != 0)
	{
	   exit (1);
	}
    	
	/* Retrieve the skynet key */
	int SkynetKey = skynet_findkey(argc, argv);

        if(cmdline.verbose_flag)
	{
           cerr << "Connecting to skynet with KEY = " << SkynetKey<< endl;
        }
        bool WAIT_STATE = false;

	if(cmdline.verbose_flag)
	{
	   cout << "Dynamic Planner is starting..." << endl;
	}

        //CMyComponent* MyComponent = new CMyComponent();
          
	/*!Collect all the plug and play OC problem components */
	COCPComponents* OCPComponents = new COCPComponents();
    //OCPComponents->addOCPComponent((IOCPComponent*)MyComponent);
        
	/*! Create a instance of dplanner */
	CDynamicPlanner* DynamicPlanner;
	DynamicPlanner = new CDynamicPlanner(SkynetKey, WAIT_STATE, OCPComponents);
	DynamicPlanner->ActiveLoop();

	cout << "Dynamic Planner is ending." << endl;
	
    	/* Restore memory resources */
	delete DynamicPlanner;
	delete OCPComponents;

	return (int) 0;
} 

