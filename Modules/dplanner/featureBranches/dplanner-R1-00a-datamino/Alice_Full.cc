/* 
	Alice_Full.cpp
	DynamicPlanner
	
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-12-2006.
*/ 

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "NURBSBasedOTG/OTG_General.hh"
#include "Alice_Full.hh" 
#include "DynamicPlanner_Utilities.hh"

CAlice_Full::CAlice_Full(COCPComponents* OCPComponents)
{
	m_ocpComponents = OCPComponents;
	m_ocpComponentSizes	= new OCPComponentSizes();
	m_ocpComponents->getOCPComponentSizes(m_ocpComponentSizes);
	
	m_i = 0;
	m_j = 0;

    m_NofStates     =  4;
    m_NofInputs     =  2;
    m_NofParameters = 16;
    m_NofReferences =  0;

	m_NofFlatOutputs = m_NofStates + m_NofInputs;    
	m_Nurbs          = new NURBS[m_NofFlatOutputs];

	m_Nurbs[0].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[0].SolveForControlPoints      = 1;
	m_Nurbs[0].SolveForWeights            = 0;
	m_Nurbs[0].NofPolynomials             = 5;
	m_Nurbs[0].DegreeOfPolynomials        = 3;
	m_Nurbs[0].CurveSmoothness            = 2;
	m_Nurbs[0].MaxDerivatives             = 2;

	m_Nurbs[1].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[1].SolveForControlPoints      = 1;
	m_Nurbs[1].SolveForWeights            = 0;
	m_Nurbs[1].NofPolynomials             = 5;
	m_Nurbs[1].DegreeOfPolynomials        = 3;
	m_Nurbs[1].CurveSmoothness            = 2;
	m_Nurbs[1].MaxDerivatives             = 2;

	m_Nurbs[2].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[2].SolveForControlPoints      = 1;
	m_Nurbs[2].SolveForWeights            = 0;
	m_Nurbs[2].NofPolynomials             = 5;
	m_Nurbs[2].DegreeOfPolynomials        = 3;
	m_Nurbs[2].CurveSmoothness            = 2;
	m_Nurbs[2].MaxDerivatives             = 2;

	m_Nurbs[3].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[3].SolveForControlPoints      = 1;
	m_Nurbs[3].SolveForWeights            = 0;
	m_Nurbs[3].NofPolynomials             = 1;
	m_Nurbs[3].DegreeOfPolynomials        = 0;
	m_Nurbs[3].CurveSmoothness            =-1;
	m_Nurbs[3].MaxDerivatives             = 0;

	m_Nurbs[4].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[4].SolveForControlPoints      = 1;
	m_Nurbs[4].SolveForWeights            = 0;
	m_Nurbs[4].NofPolynomials             = 5;
	m_Nurbs[4].DegreeOfPolynomials        = 3;
	m_Nurbs[4].CurveSmoothness            = 2;
	m_Nurbs[4].MaxDerivatives             = 2;

	m_Nurbs[5].NofDimensionsOfFlatOutputs = 1;
	m_Nurbs[5].SolveForControlPoints      = 1;
	m_Nurbs[5].SolveForWeights            = 0;
	m_Nurbs[5].NofPolynomials             = 5;
	m_Nurbs[5].DegreeOfPolynomials        = 3;
	m_Nurbs[5].CurveSmoothness            = 2;
	m_Nurbs[5].MaxDerivatives             = 2;
	
	m_NurbsLib.CompleteNURBSInfo(m_NofFlatOutputs, m_Nurbs);

    m_NofVariables = 0;
    for(m_i = 0; m_i< m_NofFlatOutputs; m_i++)
    {
        m_NofVariables = m_NofVariables + m_Nurbs[m_i].NofDerivatives;
    }

	m_HasICostFunction    = false;
	m_HasTCostFunction    = true;
	m_HasFCostFunction    = true;
    
    m_NofILinConstraints  = m_NofStates + m_NofInputs;
	m_NofTLinConstraints  = 6;
	m_NofFLinConstraints  = m_NofStates + m_NofInputs;
	m_NofINLinConstraints = 1;
	m_NofTNLinConstraints = 6;
	m_NofFNLinConstraints = 1;
        
    m_timeInterval.t0 = 0;
	m_timeInterval.tf = 1;

	m_NofCostCollocPoints       = 21;
	m_NofConstraintCollocPoints = 31;
    m_IntegrationType           = qt_Simpsons38;
    m_NofIntegrationPoints      = m_NofCostCollocPoints + (m_NofCostCollocPoints-1)*((int) m_IntegrationType - 1);	  
    
    m_HasVariableParameters = false;
    m_HasVariableReferences = false;
    m_HasRecoverSystem      = true;
    	
	m_NofDerivativesOfStates = new int[m_NofStates];
	m_NofDerivativesOfStates[0] = 2;
	m_NofDerivativesOfStates[1] = 2;
	m_NofDerivativesOfStates[2] = 2;
	m_NofDerivativesOfStates[3] = 2;
	
	m_NofDerivativesOfInputs = new int[m_NofInputs];
	m_NofDerivativesOfInputs[0] = 2;
	m_NofDerivativesOfInputs[1] = 2;
    
    m_InitialStates    = new double[m_NofStates];
	m_InitialStates[0] = 398903.1;
	m_InitialStates[1] = 3781278;
	m_InitialStates[2] = 0;
	m_InitialStates[3] =-0.22348;
    
    m_InitialInputs    = new double[m_NofInputs];
	m_InitialInputs[0] = 0;
	m_InitialInputs[1] = 0;
    
    m_FinalStates    = new double[m_NofStates];
	m_FinalStates[0] = 398907.5;
	m_FinalStates[1] = 3781277;
	m_FinalStates[2] = 0;
	m_FinalStates[3] =-0.22348;
    
    m_FinalInputs    = new double[m_NofInputs];
	m_FinalInputs[4] = 0;
	m_FinalInputs[5] = 0;
   
    m_Parameters           = NULL;
    m_CostParameters       = NULL;
    m_ConstraintParameters = NULL;    
    if(m_NofParameters != 0)
    {
        if(m_HasVariableParameters)
        {
            m_CostParameters = new double*[m_NofIntegrationPoints];
            for(m_i = 0; m_i < m_NofIntegrationPoints; m_i++)
            {
                m_CostParameters[m_i] = new double[m_NofParameters];
            }
           
            m_ConstraintParameters = new double*[m_NofConstraintCollocPoints];
            for(m_i = 0; m_i < m_NofConstraintCollocPoints; m_i++)
            {
                m_ConstraintParameters[m_i] = new double[m_NofParameters];
            }
        }
        else
        {            
            m_Parameters = new double[m_NofParameters];
            
            m_Parameters[0]  = 5.43560;	// L
            m_Parameters[1]  = 2.13360;	// W
            m_Parameters[2]  = 1.06680;	// hcg
            m_Parameters[3]  = 0.00000;	// v_min
            m_Parameters[4]  = 1.40800;	// v_max
            m_Parameters[5]  =-3.00000;	// a_min
            m_Parameters[6]  = 0.98100;	// a_max
            m_Parameters[7]  =-0.44942;	// phi_min
            m_Parameters[8]  = 0.44942;	// phi_max
            m_Parameters[9]  =-1.30900;	// phid_min
            m_Parameters[10] = 1.30900;	// phid_max
            m_Parameters[11] = 9.81000;	// g
            m_Parameters[12] = 0.35200;	// vi
            m_Parameters[13] = 0.35200;	// vf
            m_Parameters[14] = 0.00000;	// ai
            m_Parameters[15] = 0.00000;	// af		
        }        
    }
    
    m_References     = NULL;
    m_CostReferences = NULL;
    if(m_NofReferences != 0)
    {
        if(m_HasVariableReferences)
        {
            m_CostReferences = new double*[m_NofIntegrationPoints];
            for(m_i = 0; m_i < m_NofIntegrationPoints; m_i++)
            {
                m_CostReferences[m_i] = new double[m_NofReferences];
            }
        }
        else
        {
            m_References = new double[m_NofReferences];
        }
    }
    
    m_Ai = NULL; m_li = NULL; m_ui = NULL;
    if(m_NofILinConstraints != 0)
    {
       m_Ai = new double*[m_NofILinConstraints];
       for(m_i=0; m_i<m_NofILinConstraints; m_i++)
       {
           m_Ai[m_i] = new double[m_NofVariables]; 
           for(m_j=0; m_j<m_NofVariables; m_j++)
           {
               m_Ai[m_i][m_j] = 0.0; 
           }
       }    
       m_Ai[0][0]  = 1.0;
       m_Ai[1][3]  = 1.0;
       m_Ai[2][6]  = 1.0;
       m_Ai[2][12] = -m_Parameters[12];
       m_Ai[3][9]  = 1.0;
       m_Ai[4][13] = 1.0;
       m_Ai[5][16] = 1.0;
       
       m_li = new double[m_NofILinConstraints];
       m_ui = new double[m_NofILinConstraints];
       
       m_j = 0;
       for(m_i=0; m_i<m_NofStates; m_i++)
       {
           m_li[m_j] = m_InitialStates[m_i];
           m_ui[m_j] = m_li[m_j];
           m_j = m_j + 1;
       }
       
       for(m_i=0; m_i<m_NofInputs; m_i++)
       {
           m_li[m_j] = m_InitialInputs[m_i];
           m_ui[m_j] = m_li[m_j]; 
           m_j = m_j + 1;                    
       }                
    }
        
    m_At = NULL; m_lt = NULL; m_ut = NULL;
    if(m_NofTLinConstraints != 0)
    {
       m_At = new double*[m_NofTLinConstraints];
       for(m_i=0; m_i<m_NofTLinConstraints; m_i++)
       {
           m_At[m_i] = new double[m_NofVariables]; 
           for(m_j=0; m_j<m_NofVariables; m_j++)
           {
               m_At[m_i][m_j] = 0.0; 
           }           
           m_At[0][6]  = 1.0;
           m_At[0][12] = -m_Parameters[3];
           m_At[1][6]  = 1.0;
           m_At[1][12] = -m_Parameters[4];
           m_At[2][16] = 1.0;
           m_At[3][12] = -m_Parameters[9];
           m_At[3][17] = 1.0;
           m_At[4][12] = -m_Parameters[10];
           m_At[4][17] = 1.0;
           m_At[5][12] = 1.0;
       }    
       m_lt = new double[m_NofTLinConstraints*m_NofConstraintCollocPoints];
       m_ut = new double[m_NofTLinConstraints*m_NofConstraintCollocPoints];       
       
       m_j = 0;                
       for(m_i = 0; m_i < m_NofConstraintCollocPoints; m_i++)
       {
           m_lt[m_j + 0] = 0;
           m_lt[m_j + 1] =-DBL_MAX;
           m_lt[m_j + 2] =-0.44942;
           m_lt[m_j + 3] = 0;
           m_lt[m_j + 4] =-DBL_MAX;
           m_lt[m_j + 5] = 0.01;
           
           m_ut[m_j + 0] = DBL_MAX;
           m_ut[m_j + 1] = 0;
           m_ut[m_j + 2] = 0.44942;
           m_ut[m_j + 3] = DBL_MAX;
           m_ut[m_j + 4] = 0;
           m_ut[m_j + 5] = 100;
           
           m_j = m_j + m_NofTLinConstraints;
       }                
    }
    
    m_Af = NULL; m_lf = NULL; m_uf = NULL;
    if(m_NofFLinConstraints != 0)
    {
       m_Af = new double*[m_NofFLinConstraints];
       for(m_i=0; m_i<m_NofFLinConstraints; m_i++)
       {
           m_Af[m_i] = new double[m_NofVariables]; 
           for(m_j=0; m_j<m_NofVariables; m_j++)
           {
               m_Af[m_i][m_j] = 0.0; 
           }                  
       }    
       m_Af[0][0]  = 1.0;
       m_Af[1][3]  = 1.0;
       m_Af[2][6]  = 1.0;
       m_Af[2][12] = -m_Parameters[13];
       m_Af[3][9]  = 1.0;
       m_Af[4][13] = 1.0;
       m_Af[5][16] = 1.0;
            
       m_lf = new double[m_NofFLinConstraints];
       m_uf = new double[m_NofFLinConstraints];       
       
       m_j = 0;
       for(m_i=0; m_i<m_NofStates; m_i++)
       {
           m_lf[m_j] = m_FinalStates[m_i];
           m_uf[m_j] = m_lf[m_j];
           m_j = m_j + 1;
       }
       
       for(m_i=0; m_i<m_NofInputs; m_i++)
       {
           m_lf[m_j] = m_FinalInputs[m_i];
           m_uf[m_j] = m_lf[m_j]; 
           m_j = m_j + 1;                    
       }                
    }
    
    if(m_NofINLinConstraints != 0)
    {
        m_Li = new double[m_NofINLinConstraints];
        m_Ui = new double[m_NofINLinConstraints];
        
        m_Li[0] = 0;
        m_Ui[0] = m_Li[0];
    }
    
    if(m_NofTNLinConstraints != 0)
    {
        m_Lt = new double[m_NofTNLinConstraints*m_NofConstraintCollocPoints];
        m_Ut = new double[m_NofTNLinConstraints*m_NofConstraintCollocPoints];
        
        m_j = 0;                
        for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
        {
            m_Lt[m_j + 0] = 0;
            m_Lt[m_j + 1] = 0;
            m_Lt[m_j + 2] = 0;
            m_Lt[m_j + 3] = 0;
            m_Lt[m_j + 4] =-3;
            m_Lt[m_j + 5] = 9.81;
            
            m_Ut[m_j + 0] = 0;
            m_Ut[m_j + 1] = 0;
            m_Ut[m_j + 2] = 0;
            m_Ut[m_j + 3] = 0;
            m_Ut[m_j + 4] = 0.981;
            m_Ut[m_j + 5] = 9.81;                
            
            m_j = m_j + m_NofTLinConstraints;
        }                
    }

    if(m_NofFNLinConstraints != 0)
    {
        m_Lf = new double[m_NofFNLinConstraints];
        m_Uf = new double[m_NofFNLinConstraints];
        
        m_Lf[0] = 0;
        m_Uf[0] = m_Lf[0];
    }    
}

CAlice_Full::~CAlice_Full(void)
{
	delete m_Nurbs;
        
    if(m_NofParameters != 0)
    {
        if(m_HasVariableParameters)
        {
            for(m_i = 0; m_i < m_NofIntegrationPoints; m_i++)
            {
                delete[] m_CostParameters[m_i];
            }           
            delete [] m_CostParameters;
            
            for(m_i = 0; m_i < m_NofConstraintCollocPoints; m_i++)
            {
                delete[] m_ConstraintParameters[m_i];
            }
            delete[] m_ConstraintParameters;
        }
        else
        {
            delete[] m_Parameters;
        }
    }
    
    if(m_NofReferences != 0)
    {
        if(m_HasVariableReferences)
        {
            for(m_i = 0; m_i < m_NofIntegrationPoints; m_i++)
            {
                delete[] m_CostReferences[m_i];
            }
            delete[] m_CostReferences;
        }
        else
        {
            delete[] m_References;
        }
    }
	
    delete[] m_InitialStates;
    delete[] m_InitialInputs;

    delete[] m_FinalStates;
    delete[] m_FinalInputs;
    
	delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
    
    if(m_NofILinConstraints != 0)
    {
       for(m_i=0; m_i<m_NofILinConstraints; m_i++)
       {
           delete[] m_Ai[m_i]; 
       }
       delete[] m_Ai;
       delete[] m_li;
       delete[] m_ui;
    }
    
    if(m_NofTLinConstraints != 0)
    {
       for(m_i=0; m_i<m_NofTLinConstraints; m_i++)
       {
           delete[] m_At[m_i];
       }    
       delete[] m_At;
       delete[] m_lt;
       delete[] m_ut;
    }
    
    if(m_NofFLinConstraints != 0)
    {
       for(m_i=0; m_i<m_NofFLinConstraints; m_i++)
       {
           delete[] m_Af[m_i];
       }    
       delete[] m_Af;
       delete[] m_lf;
       delete[] m_uf;
    }
    
    if(m_NofINLinConstraints != 0)
    {
       delete[] m_Li;
       delete[] m_Ui;
    }
    
    if(m_NofTNLinConstraints != 0)
    {
       delete[] m_Lt;
       delete[] m_Ut;
    }

    if(m_NofFNLinConstraints != 0)
    {
       delete[] m_Lf;
       delete[] m_Uf;
    }    
}

void CAlice_Full::getTimeInterval(TimeInterval* const timeInterval)
{
	timeInterval->t0 = m_timeInterval.t0;
	timeInterval->tf = m_timeInterval.tf;
}

void CAlice_Full::getOCPSizes(OCPSizes* const ocpSizes)
{
	ocpSizes->NofFlatOutputs			= m_NofFlatOutputs;	
	ocpSizes->HasICostFunction          = m_HasICostFunction;
	ocpSizes->HasTCostFunction          = m_HasTCostFunction;
	ocpSizes->HasFCostFunction          = m_HasFCostFunction;
	ocpSizes->NofILinConstraints        = m_NofILinConstraints;
	ocpSizes->NofTLinConstraints        = m_NofTLinConstraints;
	ocpSizes->NofFLinConstraints        = m_NofFLinConstraints;
	ocpSizes->NofINLinConstraints       = m_NofINLinConstraints;
	ocpSizes->NofTNLinConstraints       = m_NofTNLinConstraints;
	ocpSizes->NofFNLinConstraints       = m_NofFNLinConstraints;	
	ocpSizes->NofStates                 = m_NofStates;
	ocpSizes->NofInputs                 = m_NofInputs;
	ocpSizes->NofParameters             = m_NofParameters;
	ocpSizes->HasVariableParameters     = m_HasVariableParameters;
	ocpSizes->NofReferences             = m_NofReferences;
	ocpSizes->HasVariableReferences     = m_HasVariableReferences;
	ocpSizes->IntegrationType           = m_IntegrationType;
	ocpSizes->NofCostCollocPoints       = m_NofCostCollocPoints;
	ocpSizes->NofConstraintCollocPoints = m_NofConstraintCollocPoints;
	ocpSizes->HasRecoverSystem          = m_HasRecoverSystem;
	
	if(m_ocpComponentSizes->IsActive)
	{
		ocpSizes->HasICostFunction    = ocpSizes->HasICostFunction || m_ocpComponentSizes->HasICostFunction;
		ocpSizes->HasTCostFunction    = ocpSizes->HasTCostFunction || m_ocpComponentSizes->HasTCostFunction;
		ocpSizes->HasFCostFunction    = ocpSizes->HasFCostFunction || m_ocpComponentSizes->HasFCostFunction;
		ocpSizes->NofILinConstraints  = ocpSizes->NofILinConstraints + m_ocpComponentSizes->NofILinConstraints;
		ocpSizes->NofTLinConstraints  = ocpSizes->NofTLinConstraints + m_ocpComponentSizes->NofTLinConstraints;
		ocpSizes->NofFLinConstraints  = ocpSizes->NofFLinConstraints + m_ocpComponentSizes->NofFLinConstraints;
		ocpSizes->NofINLinConstraints = ocpSizes->NofINLinConstraints + m_ocpComponentSizes->NofINLinConstraints;
		ocpSizes->NofTNLinConstraints = ocpSizes->NofTNLinConstraints + m_ocpComponentSizes->NofTNLinConstraints;
		ocpSizes->NofFNLinConstraints = ocpSizes->NofFNLinConstraints + m_ocpComponentSizes->NofFNLinConstraints;
	}
}

void CAlice_Full::getNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data)
{
	if(*dataType == dt_NofDimensionsOfFlatOutputs)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].NofDimensionsOfFlatOutputs;
		}
	}
	else if(*dataType == dt_SolveForControlPoints)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].SolveForControlPoints;
		}
	}
	else if(*dataType == dt_SolveForWeights)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].SolveForWeights;
		}
	}
	else if(*dataType == dt_NofPolynomials)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].NofPolynomials;
		}
	}
	else if(*dataType == dt_DegreeOfPolynomials)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].DegreeOfPolynomials;
		}
	}
	else if(*dataType == dt_CurveSmoothness)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].CurveSmoothness;
		}
	}
	else //if (*dataType == dt_MaxDerivatives)
	{
		for(m_i=0; m_i<*NofFlatOutputs; m_i++)
		{
			Data[m_i] = m_Nurbs[m_i].MaxDerivatives;
		}
	}
}

void CAlice_Full::getBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints)
{
	for(m_i=0; m_i<*NofFlatOutputs; m_i++)
	{
		CreateTimeVector(timeInterval, &NofBreakPoints[m_i], BreakPoints[m_i]);
	}
}

void CAlice_Full::getCollocPoints(const FunctionType* const functionType, const TimeInterval* const timeInterval, const int* const NofCollocPoints, double* const CollocPoints)
{
	if(*functionType == ft_Cost)
	{
		CreateTimeVector(timeInterval, NofCollocPoints, CollocPoints);	
	}
	else /*if(*functionType == ft_Constraint)*/
	{
		CreateTimeVector(timeInterval, NofCollocPoints, CollocPoints);			
	}		
}

void CAlice_Full::getOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters)
{
	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		Parameters[m_i] = m_Parameters[m_i];
	}
}

void CAlice_Full::getOCPReferences(const int* const i, const double* const t, const int* const NofReferences, double* const References)
{

}

void CAlice_Full::getSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs)
{
	for(m_i=0; m_i< *NofStates; m_i++)
	{
		NofDerivativesOfStates[m_i] = m_NofDerivativesOfStates[m_i];
	}
	
	for(m_i=0; m_i< *NofInputs; m_i++)
	{
		NofDerivativesOfInputs[m_i] = m_NofDerivativesOfInputs[m_i];
	}
}

void CAlice_Full::RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u)
{

		*t = *tau*z5;

	{
		double t6 = z5*z5;
		double t9 = 0.3141592653589793E1*0.3141592653589793E1;      

		x1 = z1;      
		x2 = z2;      
		x3 = z3/z5;      
		x4 = 180.0*z4/0.3141592653589793E1;      
		x1d = z1d;      
		x2d = z2d;      
		x3d = z3d/t6;      
		x4d = 32400.0*z4d/t9;      
		x1dd = z1dd;      
		x2dd = z2dd;      
		x3dd = z3dd/t6/z5;      
		x4dd = 5832000.0*z4dd/t9/0.3141592653589793E1;      
	}

	{
		double t1 = z5*z5;
		double t7 = t1*t1;      
		double t10 = 0.3141592653589793E1*0.3141592653589793E1;      

		u1 = z6/t1;      
		u2 = 180.0*z7/0.3141592653589793E1;      
		u1d = z6d/(z5*t1);      
		u2d = 32400.0*z7d/z5;      
		u1dd = z6dd/t7;      
		u2dd = 5832000.0*(z7dd/t1)/t10/0.3141592653589793E1;      
	}
}

void CAlice_Full::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{
	if(m_HasICostFunction)
	{
	   if(*mode == 0 || *mode == 2)
	   {
	
	   }

	   if(*mode == 1 || *mode == 2)
	   {

	   }
	}
	
	if(m_ocpComponentSizes->IsActive)
	{
	   if(m_ocpComponentSizes->HasICostFunction)
	   {	
		  m_ocpComponents->InitialCostFunction(mode, state, i, t, NofVariables, z, NofReferences, References, NofParameters, Parameters, Fi, DFi);
	   }
	}		
}

void CAlice_Full::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
	if(m_HasTCostFunction)
	{
	   if(*mode == 0 || *mode == 2)
	   {
		  double t1 = z6*z6;
		  double t2 = z5*z5;      
		  double t3 = t2*t2;      
		  double t6 = z7*z7;      

		  *Ft  = t1/t3+t6;      
	   }

	   if(*mode == 1 || *mode == 2)
	   {
	      double t1 = z6*z6;
		  double t2 = z5*z5;      
		  double t3 = t2*t2;      

		  DFt[12] = -4.0*t1/t3/z5;      
		  DFt[13] = 2.0*z6/t3;      
		  DFt[16] = 2.0*z7;      
	   }
	}	
	
	if(m_ocpComponentSizes->IsActive)
	{
	   if(m_ocpComponentSizes->HasTCostFunction)
	   {	
		  m_ocpComponents->TrajectoryCostFunction(mode, state, i, t, NofVariables, z, NofReferences, References, NofParameters, Parameters, Ft, DFt);
	   }
	}			
}

void CAlice_Full::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
	if(m_HasTCostFunction)
	{
	   if(*mode == 0 || *mode == 2)
	   {
		  *Ff = z5;
	   }
		
	   if(*mode == 1 || *mode == 2)
	   {			
		  DFf[12] = 1.0;      
	   }		
	}
	
	if(m_ocpComponentSizes->IsActive)
	{
	   if(m_ocpComponentSizes->HasFCostFunction)
	   {	
		  m_ocpComponents->FinalCostFunction(mode, state, i, t, NofVariables, z, NofReferences, References, NofParameters, Parameters, Ff, DFf);
	   }
	}			
}

void CAlice_Full::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A)
{
	if(*ctype == ct_LinInitial)
	{
        m_Offset = 0;
		if(m_NofILinConstraints != 0)
		{	
           for(m_i=0; m_i<m_NofILinConstraints; m_i++)
           {
               for(m_j=0; m_j<m_NofVariables; m_j++)
               {
                   A[m_i][m_j] = m_Ai[m_i][m_j];
               }
           }
           m_Offset = m_Offset + m_NofILinConstraints;
		}
        
        if(m_ocpComponentSizes->IsActive) 
        {
           if(m_ocpComponentSizes->NofILinConstraints != 0)
           {
              m_ocpComponents->getLinearConstraints(ctype, i, t, NofParameters, Parameters, NofLConstraints, NofVariables, A, &m_Offset);              
           }
        }
	}	
	else if(*ctype == ct_LinTrajectory)
	{
        m_Offset = 0;
        if(m_NofTLinConstraints != 0)
        {
           for(m_i=0; m_i<m_NofTLinConstraints; m_i++)
           {
               for(m_j=0; m_j<m_NofVariables; m_j++)
               {
                   A[m_i][m_j] = m_At[m_i][m_j];
               }
           }        
           m_Offset = m_Offset + m_NofTLinConstraints;
        }
        
        if(m_ocpComponentSizes->IsActive) 
        {
           if(m_ocpComponentSizes->NofTLinConstraints != 0)
           {
              m_ocpComponents->getLinearConstraints(ctype, i, t, NofParameters, Parameters, NofLConstraints, NofVariables, A, &m_Offset);              
           }
        }                
	}
	else /*if(*ctype == ct_LinFinal)*/
	{
        m_Offset = 0;
        if(m_NofFLinConstraints != 0)
        {
           for(m_i=0; m_i<m_NofFLinConstraints; m_i++)
           {
               for(m_j=0; m_j<m_NofVariables; m_j++)
               {
                   A[m_i][m_j] = m_Af[m_i][m_j];
               }
           }  
           m_Offset = m_Offset + m_NofFLinConstraints;            
        }
        
        if(m_ocpComponentSizes->IsActive) 
        {
           if(m_ocpComponentSizes->NofFLinConstraints != 0)
           {
              m_ocpComponents->getLinearConstraints(ctype, i, t, NofParameters, Parameters, NofLConstraints, NofVariables, A, &m_Offset);              
           }
        }                        
	}	
}

void CAlice_Full::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli)
{
    if(m_NofINLinConstraints != 0)
    {
        m_Offset = 0;
        
        if(*mode == 0 || *mode == 2)
        {
            double t1 = z5*z5;
            
            Cnli[0]  = z6-p15*t1;      
        }
        
        if(*mode == 1 || *mode == 2)
        {
            DCnli[0][12] = -2.0*p15*z5;      
            DCnli[0][13] = 1.0;      
        }
        
        if(*mode == 99)
        {
            DCnli[0][12] = 1.0; 
            DCnli[0][13] = 1.0; 
        }
        
        m_Offset = m_Offset + m_NofINLinConstraints;
    }
    
    if(m_ocpComponentSizes->IsActive) 
    {
       if(m_ocpComponentSizes->NofINLinConstraints != 0)
       {
          m_ocpComponents->InitialNLConstraintFunction(mode, state, i,  t, NofVariables, z, NofParameters, Parameters, NofConstraints, Cnli, DCnli, &m_Offset);                 
       }
    }                        
}

void CAlice_Full::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt)
{
    if(m_NofTNLinConstraints != 0)
    {
        m_Offset = 0;

        if(*mode == 0 || *mode == 2)
        {
            double t1 = cos(z4);
            double t4 = sin(z4);      
            double t8 = 1/p1;      
            double t10 = tan(z7);      
            double t13 = z5*z5;      
            double t14 = 1/t13;      
            double t16 = z3*z3;      
            
            Cnlt[0] = z3*t1-z1d;      
            Cnlt[1] = z3*t4-z2d;      
            Cnlt[2] = z6-z3d;      
            Cnlt[3] = z3*t8*t10-z4d;      
            Cnlt[4] = z6*t14;      
            Cnlt[5] = t16*t14*t10*t8;      
        }
        
        if(*mode == 1 || *mode == 2)
        {
            double t1 = cos(z4);
            double t2 = sin(z4);      
            double t5 = tan(z7);      
            double t6 = 1/p1;      
            double t7 = t5*t6;      
            double t9 = t5*t5;      
            double t10 = 1.0+t9;      
            double t12 = z5*z5;      
            double t14 = 1/t12/z5;      
            double t17 = 1/t12;      
            double t21 = z3*z3;      
            
            DCnlt[0][1] = -1.0;      
            DCnlt[0][6] = t1;      
            DCnlt[0][9] = -z3*t2;      
            DCnlt[1][4] = -1.0;      
            DCnlt[1][6] = t2;      
            DCnlt[1][9] = z3*t1;      
            DCnlt[2][7] = -1.0;      
            DCnlt[2][13] = 1.0;      
            DCnlt[3][6] = t7;      
            DCnlt[3][10] = -1.0;      
            DCnlt[3][16] = t10*z3*t6;      
            DCnlt[4][12] = -2.0*z6*t14;      
            DCnlt[4][13] = t17;      
            DCnlt[5][6] = 2.0*z3*t17*t7;      
            DCnlt[5][12] = -2.0*t21*t14*t7;      
            DCnlt[5][16] = t21*t17*t10*t6;      
        }
        
        if(*mode == 99)
        {
            DCnlt[0][1] = 1.0; 
            DCnlt[0][6] = 1.0; 
            DCnlt[0][9] = 1.0; 
            DCnlt[1][4] = 1.0; 
            DCnlt[1][6] = 1.0; 
            DCnlt[1][9] = 1.0; 
            DCnlt[2][7] = 1.0; 
            DCnlt[2][13] = 1.0; 
            DCnlt[3][6] = 1.0; 
            DCnlt[3][10] = 1.0; 
            DCnlt[3][16] = 1.0; 
            DCnlt[4][12] = 1.0; 
            DCnlt[4][13] = 1.0; 
            DCnlt[5][6] = 1.0; 
            DCnlt[5][12] = 1.0; 
            DCnlt[5][16] = 1.0; 
        }
        
        m_Offset = m_Offset + m_NofTNLinConstraints;
    }
    
    if(m_ocpComponentSizes->IsActive) 
    {
       if(m_ocpComponentSizes->NofTNLinConstraints != 0)
       {
          m_ocpComponents->TrajectoryNLConstraintFunction(mode, state, i, t, NofVariables, z, NofParameters, Parameters, NofConstraints, Cnlt, DCnlt, &m_Offset);       
       }
    }                            
}

void CAlice_Full::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf)
{
    if(m_NofFNLinConstraints != 0)
    {
        m_Offset = 0;

        if(*mode == 0 || *mode == 2)
        {
            double t1 = z5*z5;
            
            Cnlf[0]  = z6-p16*t1;      
        }
        
        if(*mode == 1 || *mode == 2)
        {
            DCnlf[0][12] = -2.0*p16*z5;      
            DCnlf[0][13] = 1.0;      
        }
        
        if(*mode == 99)
        {
            DCnlf[0][12] = 1.0; 
            DCnlf[0][13] = 1.0; 
        }
        
        m_Offset = m_Offset + m_NofFNLinConstraints;
    }

    if(m_ocpComponentSizes->IsActive) 
    {
       if(m_ocpComponentSizes->NofFNLinConstraints != 0)
       {
          m_ocpComponents->FinalNLConstraintFunction(mode, state, i, t, NofVariables, z, NofParameters, Parameters, NofConstraints, Cnlf, DCnlf, &m_Offset);
       }
    }                        
}

void CAlice_Full::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds)
{
	if(*ctype == ct_LinInitial)
	{
        if(m_NofILinConstraints != 0)
        {   
            m_Offset = 0;            
        
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i=0; m_i<m_NofILinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_li[m_i];
                }                
            }
            else
            {
                for(m_i=0; m_i<m_NofILinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_ui[m_i];
                }                
            }
            
            m_Offset = m_Offset + m_NofILinConstraints;            
        }
        
        if(m_ocpComponentSizes->IsActive)
        {
            if(m_ocpComponentSizes->NofILinConstraints != 0)
            {
               m_ocpComponents->getLUBounds(ctype, ubtype, NofConstraintCollocPoints, NofConstraints, LUBounds, &m_Offset);  
            }     
        }        
	}

	if(*ctype == ct_LinTrajectory)
	{
        if(m_NofTLinConstraints != 0)
        {   
            m_Offset = 0;            
        
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i=0; m_i<(*NofConstraintCollocPoints)*m_NofTLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_lt[m_i];
                }                
            }
            else
            {
                for(m_i=0; m_i<(*NofConstraintCollocPoints)*m_NofTLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_ut[m_i];
                }                
            }
            
            m_Offset = m_Offset + (*NofConstraintCollocPoints)*m_NofTLinConstraints;  
        }
        
        if(m_ocpComponentSizes->IsActive)
        {
            if(m_ocpComponentSizes->NofTLinConstraints != 0)
            {
               m_ocpComponents->getLUBounds(ctype, ubtype, NofConstraintCollocPoints, NofConstraints, LUBounds, &m_Offset);  
            }     
        }
	}

	if(*ctype == ct_LinFinal)
	{
        if(m_NofFLinConstraints != 0)
        {   
            m_Offset = 0;            

            if(*ubtype == ubt_LowerBound)
            {
                for(m_i = 0; m_i < m_NofFLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_lf[m_i];
                }
            }
            else
            {
                for(m_i = 0; m_i < m_NofFLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_uf[m_i];
                }
            }
            
            m_Offset = m_Offset + m_NofFLinConstraints; 
        }
        
        if(m_ocpComponentSizes->IsActive)
        {
            if(m_ocpComponentSizes->NofFLinConstraints != 0)
            {
               m_ocpComponents->getLUBounds(ctype, ubtype, NofConstraintCollocPoints, NofConstraints, LUBounds, &m_Offset);  
            }     
        }       
	}

	if(*ctype == ct_NLinInitial)
	{
        if(m_NofINLinConstraints != 0)
        {   
            m_Offset = 0;            
    
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i=0; m_i<m_NofINLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_Li[m_i];
                }
            }
            else
            {
                for(m_i = 0; m_i < m_NofINLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_Ui[m_i];
                }
            }
            
            m_Offset = m_Offset + m_NofINLinConstraints; 
        }
        
        if(m_ocpComponentSizes->IsActive)
        {
            if(m_ocpComponentSizes->NofINLinConstraints != 0)
            {
               m_ocpComponents->getLUBounds(ctype, ubtype, NofConstraintCollocPoints, NofConstraints, LUBounds, &m_Offset);  
            }     
        }       
	}

	if(*ctype == ct_NLinTrajectory)
	{   
        if(m_NofTNLinConstraints != 0)
        {   
            m_Offset = 0;            

            if(*ubtype == ubt_LowerBound)
            {
                for(m_i = 0; m_i < (*NofConstraintCollocPoints)*m_NofTNLinConstraints; m_i++)
                {
                  LUBounds[m_i] = m_Lt[m_i];
                }                
            }
            else
            {
                for(m_i = 0; m_i < (*NofConstraintCollocPoints)*m_NofTNLinConstraints; m_i++)
                {
                  LUBounds[m_i] = m_Ut[m_i];
                }                         
            }
            
            m_Offset = m_Offset + (*NofConstraintCollocPoints)*m_NofTNLinConstraints; 
        }
        
        if(m_ocpComponentSizes->IsActive)
        {
            if(m_ocpComponentSizes->NofTNLinConstraints != 0)
            {
               m_ocpComponents->getLUBounds(ctype, ubtype, NofConstraintCollocPoints, NofConstraints, LUBounds, &m_Offset);  
            }     
        }               
	}

	if(*ctype == ct_NLinFinal)
	{
        if(m_NofFNLinConstraints != 0)
        {   
            m_Offset = 0;            
                
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i = 0; m_i < m_NofFNLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_Lf[m_i];
                }    
            }
            else
            {
                for(m_i = 0; m_i < m_NofFNLinConstraints; m_i++)
                {
                    LUBounds[m_i] = m_Uf[m_i];
                }    
            }
            
            m_Offset = m_Offset + m_NofFNLinConstraints;
        }
        
        if(m_ocpComponentSizes->IsActive)
        {
            if(m_ocpComponentSizes->NofFNLinConstraints != 0)
            {
               m_ocpComponents->getLUBounds(ctype, ubtype, NofConstraintCollocPoints, NofConstraints, LUBounds, &m_Offset);  
            }     
        }       
	}   
}

int CAlice_Full::setTimeInterval(const TimeInterval* const timeInterval)
{
    m_timeInterval.t0 = timeInterval->t0;
    m_timeInterval.tf = timeInterval->tf;
    return 0;
}

int CAlice_Full::setStates(const ConstraintType* const ctype, const int* const NofStates, const double* const x)
{
	if(*ctype == ct_LinInitial)
	{
       for(m_i=0; m_i<*NofStates; m_i++)
       {
           m_InitialStates[m_i] = x[m_i];
       } 
	}
	else if(*ctype == ct_LinTrajectory)
	{
    
	}
	else if(*ctype == ct_LinFinal)
	{
       for(m_i=0; m_i<*NofStates; m_i++)
       {
           m_FinalStates[m_i] = x[m_i];
       } 
	}
	else if(*ctype == ct_NLinInitial)
	{

	}
	else if(*ctype == ct_NLinTrajectory)
	{   

	}
    else /*(*ctype == ct_NLinFinal)*/
	{
    
	}       
    
    return 0;
}

int CAlice_Full::setInputs(const ConstraintType* const ctype, const int* const NofInputs, const double* const u)
{
	if(*ctype == ct_LinInitial)
	{
       for(m_i=0; m_i<*NofInputs; m_i++)
       {
           m_InitialInputs[m_i] = u[m_i];
       } 
	}
	else if(*ctype == ct_LinTrajectory)
	{
    
	}
	else if(*ctype == ct_LinFinal)
	{
       for(m_i=0; m_i<*NofInputs; m_i++)
       {
           m_FinalInputs[m_i] = u[m_i];
       } 
	}
	else if(*ctype == ct_NLinInitial)
	{

	}
	else if(*ctype == ct_NLinTrajectory)
	{   

	}
    else /*(*ctype == ct_NLinFinal)*/
	{
    
	}       
    
    return 0;
}

int CAlice_Full::setOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, const double* const Parameters)
{
    if(m_HasVariableParameters)
    {
        if(*functionType == ft_Cost)
        {
            for(m_j=0; m_j<*NofParameters; m_j++)
            {
                m_CostParameters[*i][m_j] = Parameters[m_j];
            } 
        }
        else
        {
            for(m_j=0; m_j<*NofParameters; m_j++)
            {
                m_ConstraintParameters[*i][m_j] = Parameters[m_j];
            } 
        }
    }
    else
    {                    
        for(m_j=0; m_j<*NofParameters; m_j++)
        {
            m_Parameters[m_j] = Parameters[m_j];
        }
    }        

    return 0;
}

int CAlice_Full::setOCPReferences(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofReferences, const double* const References)
{
    if(m_HasVariableReferences)
    {
        if(*functionType == ft_Cost)
        {
            for(m_j=0; m_j<*NofReferences; m_j++)
            {
                m_CostReferences[*i][m_j] = References[m_j];
            } 
        }
        else
        {

        }
    }
    else
    {                    
        for(m_j=0; m_j<*NofReferences; m_j++)
        {
            m_CostReferences[*i][m_j] = References[m_j];
        } 
    }        

    return 0;
}

int CAlice_Full::setLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, const double** const A)
{
	if(*ctype == ct_LinInitial)
	{
       for(m_i=0; m_i<m_NofILinConstraints; m_i++)
       {
           for(m_j=0; m_j<m_NofVariables; m_j++)
           {
               m_Ai[m_i][m_j] = A[m_i][m_j];
           }
       }
	}	
	else if(*ctype == ct_LinTrajectory)
	{
       for(m_i=0; m_i<m_NofTLinConstraints; m_i++)
       {
           for(m_j=0; m_j<m_NofVariables; m_j++)
           {
               m_At[m_i][m_j] = A[m_i][m_j];
           }
       }        
	}
	else /*if(*ctype == ct_LinFinal)*/
	{
       for(m_i=0; m_i<m_NofFLinConstraints; m_i++)
       {
           for(m_j=0; m_j<m_NofVariables; m_j++)
           {
               m_Af[m_i][m_j] = A[m_i][m_j];
           }
       }  
	}	

    return 0;
}

int CAlice_Full::setLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, const double* const LUBounds)
{
	if(*ctype == ct_LinInitial)
	{
        if(m_NofILinConstraints != 0)
        {   
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i=0; m_i<m_NofILinConstraints; m_i++)
                {
                    m_li[m_i] = LUBounds[m_i];
                }                
            }
            else
            {
                for(m_i=0; m_i<m_NofILinConstraints; m_i++)
                {
                    m_ui[m_i] = LUBounds[m_i];
                }                
            }
        }        
	}

	if(*ctype == ct_LinTrajectory)
	{
        if(m_NofTLinConstraints != 0)
        {   
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i=0; m_i<(*NofConstraintCollocPoints)*m_NofTLinConstraints; m_i++)
                {
                    m_lt[m_i] = LUBounds[m_i];
                }                
            }
            else
            {
                for(m_i=0; m_i<(*NofConstraintCollocPoints)*m_NofTLinConstraints; m_i++)
                {
                    m_ut[m_i] = LUBounds[m_i];
                }                
            }
        }        
	}

	if(*ctype == ct_LinFinal)
	{
        if(m_NofFLinConstraints != 0)
        {   
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i = 0; m_i < m_NofFLinConstraints; m_i++)
                {
                    m_lf[m_i] = LUBounds[m_i];
                }
            }
            else
            {
                for(m_i = 0; m_i < m_NofFLinConstraints; m_i++)
                {
                    m_uf[m_i] = LUBounds[m_i];
                }
            }
        }
	}

	if(*ctype == ct_NLinInitial)
	{
        if(m_NofINLinConstraints != 0)
        {   
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i=0; m_i<m_NofINLinConstraints; m_i++)
                {
                    m_Li[m_i] = LUBounds[m_i];
                }
            }
            else
            {
                for(m_i = 0; m_i < m_NofINLinConstraints; m_i++)
                {
                    m_Ui[m_i] = LUBounds[m_i];
                }
            }
        }        
	}

	if(*ctype == ct_NLinTrajectory)
	{   
        if(m_NofTNLinConstraints != 0)
        {   
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i = 0; m_i < (*NofConstraintCollocPoints)*m_NofTNLinConstraints; m_i++)
                {
                    m_Lt[m_i] = LUBounds[m_i];
                }                
            }
            else
            {
                for(m_i = 0; m_i < (*NofConstraintCollocPoints)*m_NofTNLinConstraints; m_i++)
                {
                    m_Ut[m_i] = LUBounds[m_i];
                }                         
            }
        }
	}

	if(*ctype == ct_NLinFinal)
	{
        if(m_NofFNLinConstraints != 0)
        {   
            if(*ubtype == ubt_LowerBound)
            {
                for(m_i = 0; m_i < m_NofFNLinConstraints; m_i++)
                {
                    m_Lf[m_i] = LUBounds[m_i];
                }    
            }
            else
            {
                for(m_i = 0; m_i < m_NofFNLinConstraints; m_i++)
                {
                    m_Uf[m_i] = LUBounds[m_i];
                }    
            }
        }        
	}   
    
    return 0;
}

/*
void CAlice_Full::getInitialGuess(int start_waypoint, int stop_waypoint, RDDFVector rddfvector, double** Weights, double*** ControlPoints)
{
	ConstructInitialGuess(m_NofFlatOutputs, m_Nurbs, &m_timeInterval, start_waypoint, stop_waypoint, rddfvector, Weights, ControlPoints);
}
*/
