/*
 *  DynamicPlanner.hh
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#ifndef DYNAMICPLANNER_HH
#define DYNAMICPLANNER_HH

#include "AlgGeom/Polytopes.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/NLPProblem.hh"
#include "AlgGeom/NLPSolver.hh"


#include "DynamicPlanner_Utilities.hh"
#include "Alice_Full.hh"
#include "NURBSBasedOTG/OTG_OCPComponents.hh"
#include "NURBSBasedOTG/OTG_ReconfigOCProblem.hh"
#include "NURBSBasedOTG/OTG_OCPSolver.hh"
#include "NURBSBasedOTG/OTG_Utilities.hh"

#include "interfaces/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "interfaces/RDDFTalker.hh"
#include "trajutils/CPath.hh"
#include "cmap/CMap.hh"
#include "cmap/CMapPlus.hh"
// #include "MapConstants.h"
#include "dgcutils/DGCutils.hh"

enum AliceProblemType{apt_Full, apt_Nominal, apt_BackingUp};

class CDynamicPlanner:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
    	CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents);
       ~CDynamicPlanner(void);

		void ActiveLoop(void);
		void SwitchOCProblem(const AliceProblemType* const apType);

    private:
		int m_i;
		int m_j;
		int m_k;
		int m_ell;
		
		int m_Error;
		
		RDDF*     m_RDDF;
		CTraj*    m_Traj;
		CMapPlus* m_CostMapPlus;

		RDDFVector m_RDDFVector;

		int m_SendSocket;
		int m_RDDFSocket;
		
   		AliceProblemType m_apType;

		clock_t m_InitialTime;
		clock_t m_FinalTime;
		double m_Duration;

		COCPComponents* m_OCPComponents;
		
	   	CAlice_Full*         m_Alice_Full;
        CReconfigOCProblem*  m_ReconfigOCProblem;
		COCPSolver*          m_OCPSolver;
		CAlgebraicGeometry*  m_AlgGeom;
	
        CNLPProblem* m_NLPProblem;
        CNLPSolver*  m_NLPSolver;
    
		int m_SolverInform;
		double m_OptimalCost;
				
        NURBSDataTypeX m_NURBSDataX;
        FunctionType   m_FunctionType;
        ConstraintType m_ConstraintType;
        
        int  m_FlatOutput;
		int  m_NofFlatOutputs;
		int* m_NofDimensionsOfFlatOutputs;
		int* m_NofControlPoints;
		int* m_NofWeights;
        int* m_OrderOfPolynomials;
        int* m_CurveSmoothness;
												
		double**  m_Weights;
		double*** m_ControlPoints;
		
		int m_NofConstraintCollocPoints;
		int m_NofStates;
		int m_NofInputs;
		int m_NofParameters;		
		
		int* m_NofDerivativesOfStates;
		int* m_NofDerivativesOfInputs;
		
		double*   m_Time;
		double*** m_States;
		double*** m_Inputs;
		double*   m_Parameters;
		
		int m_NofWaypoints;
		int m_start_waypoint;
		int m_stop_waypoint;
		
        int m_NofConvexSets;
        CPolytopes m_ConvexSets;

        int m_NofDimensions;
        int m_NofPolytopes;
        int m_NofPointsPerPolytope;
        int m_NofSharingPoints;
        int m_NofTotalPoints;
        int m_NofUnknownPoints;
        
        double* m_InitialPoint;
        double* m_FinalPoint;
        double* m_InitialGuess;
			
		double m_x_start;
		double m_y_start;
		double m_v_start;
		
		double m_x_stop;
		double m_y_stop;
		double m_v_stop;
	
		double m_theta_start;
		
		double* m_LIC;
		double* m_LFC;
        
        double m_t;
};

#endif /* DYNAMICPLANNER_HH*/
