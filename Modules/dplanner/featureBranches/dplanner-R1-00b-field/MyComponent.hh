/* 
	MyComponent.h
	Melvin E. Flores and Mark B. Milam
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 12-Mar-2007.
*/ 

#ifndef MyComponent_HH
#define MyComponent_HH

#include "NURBSBasedOTG/OTG_Interfaces.hh" 
#include "AliceFlat_Definitions.hh"

class CMyComponent: public IOCPComponent
{
	public:
	 CMyComponent(void);
	~CMyComponent(void);

	virtual void getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes);
	virtual void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
	virtual void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
	virtual void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);
	virtual void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
	virtual void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset);
	virtual void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
	virtual void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);
	virtual void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset);

	private:

		int m_i;
		int m_j;

		OCPComponentSizes m_ocpComponentSizes;

};
#endif /*MyComponent_HH*/


