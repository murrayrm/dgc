/*
 *  DynamicPlanner.hh
 *  DynamicPlanner
 *  Melvin E. Flores
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *  Created: Nov. 26, 2006.
 */

#ifndef DYNAMICPLANNER_HH
#define DYNAMICPLANNER_HH

/* Always place the OCPtSpecs.hh header file at the top, before all others */
#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "AliceFlat.hh"

#include "DynamicPlanner_Utilities.hh"

#ifdef __cplusplus
extern "C" {
#endif

#include "DynamicPlanner_Math.h"

#ifdef __cplusplus
}
#endif

#include "BSplineSurfaceMap/BS_CostComponent.hh"

#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/NLPProblem.hh"
#include "AlgGeom/NLPSolver.hh"

#include "NURBSBasedOTG/OTG_Interfaces.hh"
#include "NURBSBasedOTG/OTG_OCPComponents.hh"
#include "NURBSBasedOTG/OTG_ReconfigOCProblem.hh"
#include "NURBSBasedOTG/OTG_OCPSolver.hh"
#include "NURBSBasedOTG/OTG_Utilities.hh"

#include "skynettalker/StateClient.hh"
#include "trajutils/TrajTalker.hh"
#include "skynettalker/RDDFTalker.hh"
#include "trajutils/CPath.hh"
#include "dgcutils/DGCutils.hh"

enum AliceProblemType{apt_Full, apt_FlatForward, apt_FlatReverse};

class CDynamicPlanner:public CStateClient, public CTrajTalker, public CRDDFTalker
{
    public:
        CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents);
       ~CDynamicPlanner(void);

        void ActiveLoop(void);
        void SwitchOCProblem(const AliceProblemType* const apType);
        void getCurrentParameterization(void);
        void getParameterizationFromFile(void);
        void TranslateRDDFtoPolytopes(void);
        void ApproximateFeasibleRegion(void);
        void ConstructInitialGuess(void);

    private:
        int m_i;
        int m_j;
        int m_k;
        int m_ell;	
        int m_simIndex;
        	
        int m_Error;
		ALGErrorType m_ErrorFlag;
        
        RDDF*      m_RDDF;
        CTraj*     m_Traj;
        RDDFVector m_RDDFVector;
  
        OCPtSpecs* m_ocptSpecs;
        int  m_Verbose;
        bool m_useAstate;
        bool m_useCMap;
        bool m_useObstacles;
        bool m_useParameters;
        bool m_useRDDF;
        bool m_usePolyCorridor;
        
        bool m_IsSimulationMode;
        
        int m_SendSocket;
        
        AliceProblemType m_apType;

        clock_t m_InitialTime;
        clock_t m_FinalTime;
        double m_Duration;

        COCPComponents* m_OCPComponents;
	
        CAliceFlat*          m_AliceFlat;
        CReconfigOCProblem*  m_ReconfigOCProblem;
        COCPSolver*          m_OCPSolver;        
        CAlgebraicGeometry* m_AlgGeom;
        CNLPProblem*        m_NLPProblem;
        CNLPSolver*         m_NLPSolver;
	
        int m_SolverInform;
        double m_OptimalCost;
				
        NURBSDataType  m_NURBSData;        
        NURBSDataTypeX m_NURBSDataX;
        FunctionType   m_FunctionType;
        ConstraintType m_ConstraintType;
        BoundsType     m_BoundsType;
        
        OCPSizes m_ocpSizes;
        int  m_FlatOutput;
        int  m_NofFlatOutputs;
        int* m_NofDimensionsOfFlatOutputs;
        int* m_NofControlPoints;
        int* m_NofWeights;
        int* m_DegreeOfPolynomials;
        int* m_OrderOfPolynomials;
        int* m_CurveSmoothness;
        int* m_NofDerivatives;
												
        double**  m_Weights;
        double*** m_ControlPoints;
        
        TimeInterval m_tInterval;
        int m_NofCollocPointsToRecover;
        double* m_CollocPointsToRecover;
        int m_NofConstraintCollocPoints;
        int m_NofStates;
        int m_NofInputs;
        int m_NofParameters;		
		
        int     m_NofAllParams;
        double* m_AllParams;
        
        int* m_NofDerivativesOfStates;
        int* m_NofDerivativesOfInputs;
		
        double*   m_Time;
        double*** m_States;
        double*** m_Inputs;
        double*   m_Parameters;
		
        int m_NofWaypoints;
	    int m_RDDFDataSize;
	    double** m_RDDFData;

        int m_start_waypoint;
        int m_stop_waypoint;
		
        int m_NofConvexSets;
        CPolytope* m_ConvexSets;
        
        int m_NofPolytopes;
        CPolytope* m_Polytopes;
 
        int m_NofDimensions;
        int m_NofDataPoints;
        double* m_DataOffsets;
        double** m_DataPoints;
        
        double* m_InitialPoint;
        double* m_FinalPoint;
		
        double* m_InitialConditions;
        double* m_FinalConditions;

        double* m_InitialStates;
        double* m_InitialInputs;

        double* m_FinalStates;
        double* m_FinalInputs;
        
        int m_NofTNLinConstraints;
        double** m_Lt;
        double** m_Ut;
        
        PositionInterval* m_positionIntervals;
        int m_NofMetersPerPolynomial;
        int* m_SurfaceDegreeOfPolynomials;
        int* m_SurfaceCurveSmoothness;
        int* m_SurfaceMaxDerivatives;
        double m_Cost;
                
        CCostComponent* m_costComponent;
        double** m_SurfaceControlPoints;
        int*     m_NofSurfaceControlPoints;
                
        int*    m_NofCells;
        double* m_CellsSize;
        double* m_CenterPoint;
        double  m_BaseCost;

        double m_t;
};

#endif /* DYNAMICPLANNER_HH*/
