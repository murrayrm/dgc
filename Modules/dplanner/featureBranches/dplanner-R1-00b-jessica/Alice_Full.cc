/* 
	Alice_Full.cpp
	Melvin E. Flores
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 12-Mar-2007.
*/ 

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "Alice_Full.hh" 

CAlice_Full::CAlice_Full(void)
{
	m_i = 0;
	m_j = 0;

	m_TimeInterval.t0 = 0;
	m_TimeInterval.tf = 1;

	m_ocpSizes.NofFlatOutputs            = 6;
	m_ocpSizes.HasICostFunction          = false;
	m_ocpSizes.HasTCostFunction          = true;
	m_ocpSizes.HasFCostFunction          = false;
	m_ocpSizes.NofILinConstraints        = 4;
	m_ocpSizes.NofTLinConstraints        = 2;
	m_ocpSizes.NofFLinConstraints        = 3;
	m_ocpSizes.NofINLinConstraints       = 1;
	m_ocpSizes.NofTNLinConstraints       = 0;
	m_ocpSizes.NofFNLinConstraints       = 0;
	m_ocpSizes.NofStates                 = 4;
	m_ocpSizes.NofInputs                 = 2;
	m_ocpSizes.NofParameters             = 16;
	m_ocpSizes.HasVariableParameters     = false;
	m_ocpSizes.NofReferences             = 0;
	m_ocpSizes.HasVariableReferences     = false;
	m_ocpSizes.IntegrationType           = qt_Simpsons38;
	m_ocpSizes.NofIntegrationPoints      = 28;
	m_ocpSizes.NofCostCollocPoints       = 10;
	m_ocpSizes.NofConstraintCollocPoints = 51;
	m_ocpSizes.HasRecoverSystem          = true;

	m_Parameters = new double[m_ocpSizes.NofParameters];
	m_Parameters[0] = 5.4356;
	m_Parameters[1] = 2.1336;
	m_Parameters[2] = 1.0668;
	m_Parameters[3] = 0;       //0.1 m/s
	m_Parameters[4] = 1.408;
	m_Parameters[5] = -3;
	m_Parameters[6] = 0.981;
	m_Parameters[7] = -0.44942;
	m_Parameters[8] = 0.44942;
	m_Parameters[9] = -1.309;
	m_Parameters[10] = 1.309;
	m_Parameters[11] = 9.81;
	m_Parameters[12] = 0.352;
	m_Parameters[13] = 0.352;
	m_Parameters[14] = 0;
	m_Parameters[15] = 0;

	m_NofVariables = 19;

	m_Ai = new double*[m_ocpSizes.NofILinConstraints];
	m_li = new double[m_ocpSizes.NofILinConstraints];
	m_ui = new double[m_ocpSizes.NofILinConstraints];
	for(m_i=0; m_i<m_ocpSizes.NofILinConstraints; m_i++)
	{
		m_Ai[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++)
		{
			m_Ai[m_i][m_j] = 0.0;
		}
	}

	m_Ai[0][0]= 1.0;
	m_Ai[1][3]= 1.0;
	m_Ai[2][9]= 1.0;
	m_Ai[3][16]= 1.0;

	m_li[0] =398903.1;
	m_li[1] =3781278;
	m_li[2] =-0.22348;
	m_li[3] =0;         //0.1 m/s

	m_ui[0] =398903.1;
	m_ui[1] =3781278;
	m_ui[2] =-0.22348;
	m_ui[3] =0;         //0.1 m/s

	m_At = new double*[m_ocpSizes.NofTLinConstraints];
	m_lt = new double[m_ocpSizes.NofTLinConstraints*m_ocpSizes.NofConstraintCollocPoints];
	m_ut = new double[m_ocpSizes.NofTLinConstraints*m_ocpSizes.NofConstraintCollocPoints];
	for(m_i=0; m_i<m_ocpSizes.NofTLinConstraints; m_i++)
	{
		m_At[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++)
		{
			m_At[m_i][m_j] = 0.0;
		}
	}

	m_At[0][12]= 1.0;
	m_At[1][16]= 1.0;

	m_j = 0;
	for(m_i = 0; m_i < m_ocpSizes.NofConstraintCollocPoints; m_i++)
	{
		m_lt[m_j + 0] =0.01;
		m_lt[m_j + 1] =-0.44942;

		m_ut[m_j + 0] =100;
		m_ut[m_j + 1] =0.44942;

		m_j = m_j + m_ocpSizes.NofTLinConstraints;
	}


	m_Af = new double*[m_ocpSizes.NofFLinConstraints];
	m_lf = new double[m_ocpSizes.NofFLinConstraints];
	m_uf = new double[m_ocpSizes.NofFLinConstraints];
	for(m_i=0; m_i<m_ocpSizes.NofFLinConstraints; m_i++)
	{
		m_Af[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++)
		{
			m_Af[m_i][m_j] = 0.0;
		}
	}

	m_Af[0][0]= 1.0;
	m_Af[1][3]= 1.0;
	m_Af[2][9]= 1.0;

	m_lf[0] =398920.6;
	m_lf[1] =3781276;
	m_lf[2] =0.43241;

	m_uf[0] =398920.6;
	m_uf[1] =3781276;
	m_uf[2] =0.43241;

	m_Li = new double[m_ocpSizes.NofINLinConstraints];
	m_Ui = new double[m_ocpSizes.NofINLinConstraints];

	m_Li[0] =0.352;

	m_Ui[0] =0.352;
	m_NofDerivativesOfStates = new int[m_ocpSizes.NofStates];
	m_NofDerivativesOfInputs = new int[m_ocpSizes.NofInputs];

	m_NofDerivativesOfStates[0] = 3;
	m_NofDerivativesOfStates[1] = 3;
	m_NofDerivativesOfStates[2] = 3;
	m_NofDerivativesOfStates[3] = 3;

	m_NofDerivativesOfInputs[0] = 3;
	m_NofDerivativesOfInputs[1] = 3;

}

CAlice_Full::~CAlice_Full(void)
{
	delete[] m_Parameters;

	for(m_i=0; m_i<m_ocpSizes.NofILinConstraints; m_i++)
	{
		delete[] m_Ai[m_i];
	}
	delete[] m_Ai;
	delete[] m_li;
	delete[] m_ui;

	for(m_i=0; m_i<m_ocpSizes.NofTLinConstraints; m_i++)
	{
		delete[] m_At[m_i];
	}
	delete[] m_At;
	delete[] m_lt;
	delete[] m_ut;

	for(m_i=0; m_i<m_ocpSizes.NofFLinConstraints; m_i++)
	{
		delete[] m_Af[m_i];
	}
	delete[] m_Af;
	delete[] m_lf;
	delete[] m_uf;

	delete[] m_Li;
	delete[] m_Ui;

	delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;

}

void CAlice_Full::getTimeInterval(TimeInterval* const timeInterval)
{
	timeInterval->t0 = m_TimeInterval.t0;
	timeInterval->tf = m_TimeInterval.tf;
}

void CAlice_Full::getOCPSizes(OCPSizes* const ocpSizes)
{
	ocpSizes->NofFlatOutputs            = m_ocpSizes.NofFlatOutputs;
	ocpSizes->HasICostFunction          = m_ocpSizes.HasICostFunction;
	ocpSizes->HasTCostFunction          = m_ocpSizes.HasTCostFunction;
	ocpSizes->HasFCostFunction          = m_ocpSizes.HasFCostFunction;
	ocpSizes->NofILinConstraints        = m_ocpSizes.NofILinConstraints;
	ocpSizes->NofTLinConstraints        = m_ocpSizes.NofTLinConstraints;
	ocpSizes->NofFLinConstraints        = m_ocpSizes.NofFLinConstraints;
	ocpSizes->NofINLinConstraints       = m_ocpSizes.NofINLinConstraints;
	ocpSizes->NofTNLinConstraints       = m_ocpSizes.NofTNLinConstraints;
	ocpSizes->NofFNLinConstraints       = m_ocpSizes.NofFNLinConstraints;
	ocpSizes->NofStates                 = m_ocpSizes.NofStates;
	ocpSizes->NofInputs                 = m_ocpSizes.NofInputs;
	ocpSizes->NofParameters             = m_ocpSizes.NofParameters;
	ocpSizes->HasVariableParameters     = m_ocpSizes.HasVariableParameters;
	ocpSizes->NofReferences             = m_ocpSizes.NofReferences;
	ocpSizes->HasVariableReferences     = m_ocpSizes.HasVariableReferences;
	ocpSizes->IntegrationType           = m_ocpSizes.IntegrationType;
	ocpSizes->NofIntegrationPoints      = m_ocpSizes.NofIntegrationPoints;
	ocpSizes->NofCostCollocPoints       = m_ocpSizes.NofCostCollocPoints;
	ocpSizes->NofConstraintCollocPoints = m_ocpSizes.NofConstraintCollocPoints;
	ocpSizes->HasRecoverSystem          = m_ocpSizes.HasRecoverSystem;
}

void CAlice_Full::SetNofPolynomials(const int* const NofPolynomials)
{
    m_NofPolynomials = *NofPolynomials;
}

void CAlice_Full::getNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data)
{
	if(*dataType == dt_NofDimensionsOfFlatOutputs)
	{
		Data[0] = 2;
		Data[1] = 1;
		Data[2] = 1;
		Data[3] = 1;
		Data[4] = 1;
		Data[5] = 1;
	}
	else if(*dataType == dt_SolveForControlPoints)
	{
		Data[0] = 0;
		Data[1] = 1;
		Data[2] = 1;
		Data[3] = 1;
		Data[4] = 1;
		Data[5] = 1;
	}
	else if(*dataType == dt_SolveForWeights)
	{
		Data[0] = 1;
		Data[1] = 0;
		Data[2] = 0;
		Data[3] = 0;
		Data[4] = 0;
		Data[5] = 0;
	}
	else if(*dataType == dt_NofPolynomials)
	{
		Data[0] = m_NofPolynomials;
		Data[1] = m_NofPolynomials;
		Data[2] = m_NofPolynomials;
		Data[3] = 1;
		Data[4] = m_NofPolynomials;
		Data[5] = m_NofPolynomials;
	}
	else if(*dataType == dt_DegreeOfPolynomials)
	{
		Data[0] = 5;
		Data[1] = 5;
		Data[2] = 5;
		Data[3] = 0;
		Data[4] = 5;
		Data[5] = 5;
	}
	else if(*dataType == dt_CurveSmoothness)
	{
		Data[0] = 2;
		Data[1] = 2;
		Data[2] = 2;
		Data[3] = -1;
		Data[4] = 2;
		Data[5] = 2;
	}
	else //if (*dataType == dt_MaxDerivatives)
	{
		Data[0] = 2;
		Data[1] = 2;
		Data[2] = 2;
		Data[3] = 0;
		Data[4] = 2;
		Data[5] = 2;
	}
}

void CAlice_Full::getBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints)
{
	for(m_i=0; m_i<*NofFlatOutputs; m_i++)
	{
		CreateTimeVector(timeInterval, &NofBreakPoints[m_i], BreakPoints[m_i]);
	}
}

void CAlice_Full::getCollocPoints(const FunctionType* const functionType, const TimeInterval* const timeInterval, const int* const NofCollocPoints, double* const CollocPoints)
{
	CreateTimeVector(timeInterval, NofCollocPoints, CollocPoints);
}

void CAlice_Full::getOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters)
{
	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		Parameters[m_i] = m_Parameters[m_i];
	}
}

void CAlice_Full::getOCPReferences(const int* const i, const double* const t, const int* const NofReferences, double* const References)
{

}

void CAlice_Full::getSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs)
{
	for(m_i = 0; m_i<*NofStates; m_i++)
	{
		NofDerivativesOfStates[m_i] = m_NofDerivativesOfStates[m_i];
	}

	for(m_i = 0; m_i<*NofInputs; m_i++)
	{
		NofDerivativesOfInputs[m_i] = m_NofDerivativesOfInputs[m_i];
	}
}

void CAlice_Full::RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u)
{

	*t = *tau*z5;

	{
		x1 = z1;
		x1d = z1d/z5;      
		x1dd = z1dd/(z5*z5);      
		x2 = z2;      
		x2d = z2d/z5;      
		x2dd = z2dd/(z5*z5);      
		x3 = z3/z5;      
		x3d = z3d/(z5*z5);      
		x3dd = z3dd/(z5*z5*z5);      
		x4 = 180.0*z4/0.3141592653589793E1;      
		x4d = 32400.0*z4d/(0.3141592653589793E1*0.3141592653589793E1);      
		x4dd = 5832000.0*z4dd/(0.3141592653589793E1*0.3141592653589793E1*0.3141592653589793E1);      
	}

	{
		u1 = z6/(z5*z5);
		u1d = z6d/(z5*z5*z5*z5);      
		u1dd = z6dd/(z5*z5*z5*z5*z5*z5);      
		u2 = 180.0*z7/0.3141592653589793E1;      
		u2d = 32400.0*z7d/(0.3141592653589793E1*0.3141592653589793E1);      
		u2dd = 5832000.0*z7dd/(0.3141592653589793E1*0.3141592653589793E1*0.3141592653589793E1);      
	}

}

void CAlice_Full::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{

}

void CAlice_Full::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
	if(*mode == 0 || *mode == 2)
	{
		*Ft = z6*z6/(z5*z5*z5*z5)+z7*z7;
	}

	if(*mode == 1 || *mode == 2)
	{

		DFt[12] = -4.0*z6*z6/(z5*z5*z5*z5*z5);      
		DFt[13] = 2.0*z6/(z5*z5*z5*z5);      
		DFt[16] = 2.0*z7;      
	}

}

void CAlice_Full::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{

}

void CAlice_Full::getLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A)
{
	if(*constraintType == ct_LinInitial)
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				A[m_i][m_j] = m_Ai[m_i][m_j];
			}
		}
	}
	else if(*constraintType == ct_LinTrajectory)
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				A[m_i][m_j] = m_At[m_i][m_j];
			}
		}
	}
	else /*if(*constraintType == ct_LinFinal)*/
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				A[m_i][m_j] = m_Af[m_i][m_j];
			}
		}
	}
}

void CAlice_Full::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli)
{

	if(*mode == 0 || *mode == 2)
	{
		Cnli[0] = z3/z5;
	}

	if(*mode == 1 || *mode == 2)
	{
		DCnli[0][6] = 1/z5;      
		DCnli[0][12] = -z3/(z5*z5);      
	}

	if(*mode == 99)
	{
		DCnli[0][6] = 1.0; 
		DCnli[0][12] = 1.0; 
	}

}

void CAlice_Full::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt)
{

}

void CAlice_Full::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf)
{
}

void CAlice_Full::getLUBounds(const ConstraintType* const constraintType, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds)
{

	if(*constraintType == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_li[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_ui[m_i];
			}
		}
	}

	if(*constraintType == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<(*NofConstraints)*(*NofConstraintCollocPoints); m_i++)
			{
				LUBounds[m_i] = m_lt[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<(*NofConstraints)*(*NofConstraintCollocPoints); m_i++)
			{
				LUBounds[m_i] = m_ut[m_i];
			}
		}
	}

	if(*constraintType == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_lf[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_uf[m_i];
			}
		}
	}

	if(*constraintType == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_Li[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				LUBounds[m_i] = m_Ui[m_i];
			}
		}
	}

}

int CAlice_Full::setTimeInterval(const TimeInterval* const timeInterval)
{
	m_TimeInterval.t0 = timeInterval->t0;
	m_TimeInterval.tf = timeInterval->tf;
	return 0;
}

int CAlice_Full::setStates(const ConstraintType* const constraintType, const int* const NofStates, const double* const x)
{
	if(*constraintType == ct_LinInitial)
	{
        m_li[0] = x[0]; //x
	    m_li[1] = x[1]; //y
	    m_li[2] = x[3]; //theta

	    m_ui[0] = x[0];
	    m_ui[1] = x[1];
	    m_ui[2] = x[3];
	}

	if(*constraintType == ct_LinTrajectory)
	{

	}

	if(*constraintType == ct_LinFinal)
	{
        m_lf[0] = x[0]; //x
	    m_lf[1] = x[1]; //y
	    m_lf[2] = x[3]; //theta

	    m_uf[0] = x[0];
	    m_uf[1] = x[1];
	    m_uf[2] = x[3];
	}

	if(*constraintType == ct_NLinInitial)
	{

	}

	return 0;
}

int CAlice_Full::setInputs(const ConstraintType* const constraintType, const int* const NofInputs, const double* const u)
{
	if(*constraintType == ct_LinInitial)
	{
	    m_li[3] = u[1]; //phi

	    m_ui[3] = u[1]; //phi
	}

	if(*constraintType == ct_LinTrajectory)
	{

	}

	if(*constraintType == ct_LinFinal)
	{

	}

	if(*constraintType == ct_NLinInitial)
	{

	}

	return 0;
}

int CAlice_Full::setOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, const double* const Parameters)
{

	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		m_Parameters[m_i] = Parameters[m_i];
	}

	return 0;
}

int CAlice_Full::setOCPReferences(const int* const i, const double* const t, const int* const NofReferences, const double* const References)
{

	return 0;
}

int CAlice_Full::setLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, const double** const A)
{
	if(*constraintType == ct_LinInitial)
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				m_Ai[m_i][m_j] = A[m_i][m_j];
			}
		}
	}
	else if(*constraintType == ct_LinTrajectory)
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				m_At[m_i][m_j] = A[m_i][m_j];
			}
		}
	}
	else /*if(*constraintType == ct_LinFinal)*/
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				m_Af[m_i][m_j] = A[m_i][m_j];
			}
		}
	}
	return 0;
}

int CAlice_Full::setLUBounds(const ConstraintType* const constraintType, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, const double* const LUBounds)
{
	if(*constraintType == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_li[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_ui[m_i] = LUBounds[m_i];
			}
		}
	}

	if(*constraintType == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<(*NofConstraints)*(*NofConstraintCollocPoints); m_i++)
			{
				m_lt[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<(*NofConstraints)*(*NofConstraintCollocPoints); m_i++)
			{
				m_ut[m_i] = LUBounds[m_i];
			}
		}
	}

	if(*constraintType == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_lf[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_uf[m_i] = LUBounds[m_i];
			}
		}
	}

	if(*constraintType == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_Li[m_i] = LUBounds[m_i];
			}
		}
		else
		{
			for(m_i=0; m_i<*NofConstraints; m_i++)
			{
				m_Ui[m_i] = LUBounds[m_i];
			}
		}
	}

	return 0;
}

int CAlice_Full::setNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, const int* const Data)
{
    return 0;
}


