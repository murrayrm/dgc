/* 
	Alice_Full.h
	Melvin E. Flores
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 12-Mar-2007.
*/ 

#ifndef Alice_Full_HH
#define Alice_Full_HH

#include "NURBSBasedOTG/OTG_Interfaces.hh" 
#include "NURBSBasedOTG/OTG_Utilities.hh" 

/* Flat outputs: */
#define z1			 z[0]
#define z1d			 z[1]
#define z1dd		 z[2]
#define z2			 z[3]
#define z2d			 z[4]
#define z2dd		 z[5]
#define z3			 z[6]
#define z3d			 z[7]
#define z3dd		 z[8]
#define z4			 z[9]
#define z4d			 z[10]
#define z4dd		 z[11]
#define z5			 z[12]
#define z6			 z[13]
#define z6d			 z[14]
#define z6dd		 z[15]
#define z7			 z[16]
#define z7d			 z[17]
#define z7dd		 z[18]

/* States: */
#define x1			 x[0][0]
#define x1d			 x[0][1]
#define x1dd		 x[0][2]
#define x2			 x[1][0]
#define x2d			 x[1][1]
#define x2dd		 x[1][2]
#define x3			 x[2][0]
#define x3d			 x[2][1]
#define x3dd		 x[2][2]
#define x4			 x[3][0]
#define x4d			 x[3][1]
#define x4dd		 x[3][2]

/* Inputs: */
#define u1			 u[0][0]
#define u1d			 u[0][1]
#define u1dd		 u[0][2]
#define u2			 u[1][0]
#define u2d			 u[1][1]
#define u2dd		 u[1][2]

/* Parameters: */
#define p1			 Parameters[0]
#define p2			 Parameters[1]
#define p3			 Parameters[2]
#define p4			 Parameters[3]
#define p5			 Parameters[4]
#define p6			 Parameters[5]
#define p7			 Parameters[6]
#define p8			 Parameters[7]
#define p9			 Parameters[8]
#define p10			 Parameters[9]
#define p11			 Parameters[10]
#define p12			 Parameters[11]
#define p13			 Parameters[12]
#define p14			 Parameters[13]
#define p15			 Parameters[14]
#define p16			 Parameters[15]

class CAlice_Full: public IReconfigOCProblem
{
	public:
		 CAlice_Full(void);
		~CAlice_Full(void);

		void getTimeInterval(TimeInterval* const timeInterval);
		void getOCPSizes(OCPSizes* const ocpSizes);
		void getNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data);
		void getBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints);

		void getCollocPoints(const FunctionType* const functionType, const TimeInterval* const timeInterval, const int* const NofCollocPoints, double* const CollocPoints);

		void getOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters);
		void getOCPReferences(const int* const i, const double* const t, const int* const NofReferences, double* const References);

		void getSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs);
		void RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u);

		void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
		void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
		void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);

		void getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A);

		void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli);
		void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt);
		void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf);

		void getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds);

		int setTimeInterval(const TimeInterval* const timeInterval);

		int setStates(const ConstraintType* const ctype, const int* const NofStates, const double* const x);
		int setInputs(const ConstraintType* const ctype, const int* const NofInputs, const double* const u);

		int setOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, const double* const Parameters);
		int setOCPReferences(const int* const i, const double* const t, const int* const NofReferences, const double* const References);

		int setLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, const double** const A);
		int setLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, const double* const LUBounds);
		int setNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, const int* const Data);

        void SetNofPolynomials(const int* const NofPolynomials);

	private:

		int m_i;
		int m_j;

		TimeInterval m_TimeInterval;
		OCPSizes m_ocpSizes;

        int m_NofPolynomials;
		double* m_Parameters;
		int m_NofVariables;

		double** m_Ai;
		double*  m_li;
		double*  m_ui;

		double** m_At;
		double*  m_lt;
		double*  m_ut;

		double** m_Af;
		double*  m_lf;
		double*  m_uf;

		double*  m_Li;
		double*  m_Ui;

		int*  m_NofDerivativesOfStates;
		int*  m_NofDerivativesOfInputs;
};

#endif /*Alice_Full_HH*/


