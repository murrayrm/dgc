/* 
	AliceFlat.hh
	Melvin E. Flores
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 22-Apr-2007.
*/ 

#ifndef AliceFlat_HH
#define AliceFlat_HH

#include "NURBSBasedOTG/OTG_Interfaces.hh" 
#include "NURBSBasedOTG/OTG_Utilities.hh" 
#include "interfaces/AliceFlat_Definitions.hh"

class CAliceFlat: public IReconfigOCProblem
{
	public:
		 CAliceFlat(void);
		~CAliceFlat(void);

		void getTimeInterval(TimeInterval* const timeInterval);
		void getOCPSizes(OCPSizes* const ocpSizes);
		void getNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data);
		void getBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints);

		void getCollocPoints(const FunctionType* const functionType, const TimeInterval* const timeInterval, const int* const NofCollocPoints, double* const CollocPoints);

		void getOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters);
		void getOCPReferences(const int* const i, const double* const t, const int* const NofReferences, double* const References);

		void getSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs);
		void RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u);

		void InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi);
		void TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt);
		void FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf);

		void getLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A);

		void InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli);
		void TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt);
		void FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf);

		void getLUBounds(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const i, const double* const t, const int* const NofConstraints, double* const LUBounds);

		int setTimeInterval(const TimeInterval* const timeInterval);
		int setStates(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const NofStates, const double* const x);
		int setInputs(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const NofInputs, const double* const u);
		int setOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, const double* const Parameters);
		int setOCPReferences(const int* const i, const double* const t, const int* const NofReferences, const double* const References);
		int setLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, const double** const A);
		int setLUBounds(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const i, const double* const t, const int* const NofConstraints, const double* const LUBounds);
		int setNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, const int* const Data);

        void getWeightSizes(WeightSizes* const weightSizes);
        void getLinearConstraintsOnWeights(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
        void InitialNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset);
        void TrajectoryNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
        void FinalNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);

        void getControlPointSizes(ControlPointSizes* const controlPointSizes);
        void getLinearConstraintsOnControlPoints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset);
        void InitialNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset);
        void TrajectoryNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset);
        void FinalNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset);

	private:

		int m_i;
		int m_j;
		int m_k;

		TimeInterval m_TimeInterval;
		OCPSizes m_ocpSizes;

		double* m_Parameters;
		int m_NofVariables;

		double** m_Ai;
		double*  m_li;
		double*  m_ui;

		double*** m_At;
		double**  m_lt;
		double**  m_ut;

		double** m_Af;
		double*  m_lf;
		double*  m_uf;

		double*  m_Li;
		double*  m_Ui;

		double**  m_Lt;
		double**  m_Ut;

		double*  m_Lf;
		double*  m_Uf;

		int*  m_NofDerivativesOfStates;
		int*  m_NofDerivativesOfInputs;
};

#endif /*AliceFlat_HH*/
