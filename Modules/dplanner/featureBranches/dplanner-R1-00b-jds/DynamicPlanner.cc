/*
 *  DynamicPlanner.cc
 *  DynamicPlanner
 *  Melvin E. Flores
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *  Created: Nov. 26, 2006.
 */

#include <fstream.h> 
#include <math.h> 
#include "DynamicPlanner.hh"
#include "AlgGeom/AlgGeom_Utilities.hh"
#include "interfaces/sn_types.h"

CDynamicPlanner:: CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents):
CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
{
	m_i   = 0;
	m_j   = 0;
	m_k   = 0;
	m_ell = 0;
	
	m_Error = 0;
	m_Duration = 0.0;

	m_OCPComponents = OCPComponents;

    /* Create an RDDF object */
    m_RDDF = new RDDF();
    if(m_RDDF != NULL){cout << "RDDF object was created successfully" << endl;}
    
    /* Create a Traj object (y,yd,ydd,x,xd,xdd) */
    m_Traj = new CTraj(3);
    if(m_Traj != NULL){cout << "Traj object was created successfully" << endl;}
    
    m_Verbose         = 0;
    m_useAstate       = false;
    m_useCMap         = true;
    m_useObstacles    = false;
    m_useParameters   = true;
    m_useRDDF         = true;
    m_usePolyCorridor = false;
    
    m_ocptSpecs = new OCPtSpecs(SkynetKey, m_useAstate, m_Verbose , m_useCMap,   m_useObstacles, m_useParameters, m_useRDDF, m_usePolyCorridor);
    if(m_ocptSpecs != NULL){cout << "OCPtSpecs object was created successfully" << endl;}
    
    usleep(100000);
    
    m_NofDimensions = 2;   

    m_positionIntervals = new PositionInterval[m_NofDimensions];
    m_positionIntervals[0].p0 = -50.0;
    m_positionIntervals[0].pf =  50.0;
    
    m_positionIntervals[1].p0 = -50.0;
    m_positionIntervals[1].pf =  50.0;
    
    m_NofMetersPerPolynomial     = 2.0;
    m_SurfaceDegreeOfPolynomials = new int[m_NofDimensions];
    m_SurfaceCurveSmoothness     = new int[m_NofDimensions];
    m_SurfaceMaxDerivatives      = new int[m_NofDimensions];
    
    m_SurfaceDegreeOfPolynomials[0] = 5;
    m_SurfaceDegreeOfPolynomials[1] = 5;
    
    m_SurfaceCurveSmoothness[0] = 3;
    m_SurfaceCurveSmoothness[1] = 3;
    
    m_SurfaceMaxDerivatives[0] = 1;
    m_SurfaceMaxDerivatives[1] = 1;

    m_Cost = 200.0;
    m_costComponent = new CCostComponent(&m_NofDimensions, 
                                         m_positionIntervals,
                                         &m_NofMetersPerPolynomial,
                                         m_SurfaceDegreeOfPolynomials,
                                         m_SurfaceCurveSmoothness,
                                         m_SurfaceMaxDerivatives, 
                                         &m_Cost);    

    m_OCPComponents->addOCPComponent((IOCPComponent*)m_costComponent);    
    //m_OCPComponents->addOCPComponent((IOCPComponent*)m_ocptSpecs);
    
    m_SendSocket = m_skynet.get_send_sock(SNtraj);
    m_AlgGeom   = new CAlgebraicGeometry();

    /* Constructing OCProblem */
    m_apType = apt_FlatForward;
  
    m_AliceFlat = new CAliceFlat();
    if(m_AliceFlat != NULL)
    {
       cout << "Alice OC problem definition was created successfully" << endl;
    }
    
    /* create the Reconfigurable OCProblem */
    m_ReconfigOCProblem = new CReconfigOCProblem(m_AliceFlat, m_OCPComponents);
    if(m_ReconfigOCProblem != NULL)
    {
       cout << "Reconfig OC Problem was created successfully" << endl;
    }

    /* Get information about the parameterization of the OCProblem */
    m_NofFlatOutputs = m_ReconfigOCProblem->getNofFlatOutputs();
    
    m_NofDimensionsOfFlatOutputs = new int[m_NofFlatOutputs];
    m_DegreeOfPolynomials        = new int[m_NofFlatOutputs];
    m_OrderOfPolynomials         = new int[m_NofFlatOutputs];    
    m_CurveSmoothness            = new int[m_NofFlatOutputs];    
    m_NofDerivatives             = new int[m_NofFlatOutputs];    
    m_NofControlPoints           = new int[m_NofFlatOutputs];
    m_NofWeights                 = new int[m_NofFlatOutputs];

    m_NURBSDataX = nd_NofDimensionsOfFlatOutputs;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs);

    m_NURBSDataX = nd_DegreeOfPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_DegreeOfPolynomials);
 
    m_NURBSDataX = nd_OrderOfPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_OrderOfPolynomials);
   
    m_NURBSDataX = nd_CurveSmoothness;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_CurveSmoothness);

    m_NURBSDataX = nd_NofDerivatives;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofDerivatives);
    
    m_NURBSDataX = nd_NofControlPoints;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofControlPoints);
    
    m_NURBSDataX = nd_NofWeights;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofWeights);
    
	m_Weights       = new double* [m_NofFlatOutputs];
	m_ControlPoints = new double**[m_NofFlatOutputs];
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		for(m_j=0; m_j<m_NofWeights[m_i]; m_j++){m_Weights[m_i][m_j] = 1.0;}

		m_ControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
		    m_ControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++){m_ControlPoints[m_i][m_j][m_k] = 0.0;}
		}
	}

    m_InitialPoint = new double[m_NofDimensionsOfFlatOutputs[0]];
    m_FinalPoint   = new double[m_NofDimensionsOfFlatOutputs[0]];
    m_DataOffsets  = new double[m_NofDimensionsOfFlatOutputs[0]];  
                
    m_NofStates     = m_ReconfigOCProblem->getNofStates();
    m_NofInputs     = m_ReconfigOCProblem->getNofInputs();
    m_NofParameters = m_ReconfigOCProblem->getNofParameters();
    
    m_NofAllParams  = 16;
    m_AllParams     = new double[m_NofAllParams];    
    for(m_i=0; m_i<m_NofAllParams; m_i++){m_AllParams[m_i] = 0.0;}
    
    m_InitialConditions = new double[m_NofStates + m_NofInputs];
    m_FinalConditions   = new double[m_NofStates + m_NofInputs];
    for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
    {
        m_InitialConditions[m_i] = 0.0;
        m_FinalConditions[m_i]   = 0.0;
    }

    m_InitialStates = new double[m_NofStates];
    m_InitialInputs = new double[m_NofInputs];

    m_FinalStates = new double[m_NofStates];
    m_FinalInputs = new double[m_NofInputs];
    
    m_Parameters = new double[m_NofParameters];
    for(m_i=0; m_i<m_NofParameters; m_i++){m_Parameters[m_i] = 0.0;}    

    m_NofDerivativesOfStates = new int[m_NofStates];
    m_NofDerivativesOfInputs = new int[m_NofInputs];
    
    m_FunctionType = ft_Constraint;
    m_NofConstraintCollocPoints = m_ReconfigOCProblem->getNofCollocPoints(&m_FunctionType);
    
    m_ConstraintType = ct_NLinTrajectory;
    m_NofTNLinConstraints = m_ReconfigOCProblem->getNofConstraints(&m_ConstraintType);
    m_Lt = new double*[m_NofConstraintCollocPoints];
    m_Ut = new double*[m_NofConstraintCollocPoints];
    for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
    {
        m_Lt[m_i] = new double[m_NofTNLinConstraints];
        m_Ut[m_i] = new double[m_NofTNLinConstraints];
        for(m_j=0; m_j<m_NofTNLinConstraints; m_j++)
        {
            m_Lt[m_i][m_j] = 0.0;
            m_Ut[m_i][m_j] = 0.0;                
        }
    }

    m_ReconfigOCProblem->getSystemsInfo(&m_NofStates, m_NofDerivativesOfStates, &m_NofInputs, m_NofDerivativesOfInputs);
 
    m_NofCollocPointsToRecover = 100;
    m_CollocPointsToRecover    = new double[m_NofCollocPointsToRecover];
    m_ReconfigOCProblem->getTimeInterval(&m_tInterval);
    CreateTimeVector(&m_tInterval, &m_NofCollocPointsToRecover, m_CollocPointsToRecover);
    
	m_Time   = new double  [m_NofCollocPointsToRecover];
	m_States = new double**[m_NofCollocPointsToRecover];
	m_Inputs = new double**[m_NofCollocPointsToRecover];
	for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
	{
		m_States[m_i] = new double*[m_NofStates];
		for(m_j=0; m_j<m_NofStates; m_j++)
		{
			m_States[m_i][m_j] = new double[m_NofDerivativesOfStates[m_j]+1];
            for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]+1; m_k++){m_States[m_i][m_j][m_k] = 0.0;}
		}
		
		m_Inputs[m_i] = new double*[m_NofInputs];
		for(m_j=0; m_j<m_NofInputs; m_j++)
		{
			m_Inputs[m_i][m_j] = new double[m_NofDerivativesOfInputs[m_j]+1];
            for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]+1; m_k++){m_Inputs[m_i][m_j][m_k] = 0.0;}
		}
	}	
    
    m_NofSurfaceControlPoints = new int[m_NofDimensions];
    m_costComponent->getNofControlPoints(&m_NofDimensions, m_NofSurfaceControlPoints);
    
    m_SurfaceControlPoints = new double*[m_NofSurfaceControlPoints[0]];
    for(m_i=0; m_i<m_NofSurfaceControlPoints[0]; m_i++)
    {
        m_SurfaceControlPoints[m_i] = new double[m_NofSurfaceControlPoints[1]];
        for(m_j=0; m_j<m_NofSurfaceControlPoints[1]; m_j++){m_SurfaceControlPoints[m_i][m_j] = 0.0;}
    }
    
    m_NofWaypoints = 5;
    m_RDDFDataSize = 5;
    m_RDDFData = new double*[m_NofWaypoints];
    for(m_i=0; m_i<m_NofWaypoints; m_i++)
    {
        m_RDDFData[m_i] = new double[m_RDDFDataSize];
        for(m_j=0; m_j<m_RDDFDataSize; m_j++){m_RDDFData[m_i][m_j] = 0.0;}
    }
    
    m_IsSimulationMode = false;
}

CDynamicPlanner::~CDynamicPlanner(void)
{
    delete m_RDDF;
    delete m_Traj;
    
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++){delete[] m_Weights[m_i];}
	delete[] m_Weights;

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
	    for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++){delete[] m_ControlPoints[m_i][m_j];}
	    delete[] m_ControlPoints[m_i];
	}
	delete[] m_ControlPoints;    
	
	delete[] m_NofDimensionsOfFlatOutputs;
	delete[] m_DegreeOfPolynomials;
	delete[] m_CurveSmoothness;
	delete[] m_NofControlPoints;
    delete[] m_NofWeights;
        
    delete[] m_InitialPoint;
	delete[] m_FinalPoint;
    delete[] m_DataOffsets;  
        
 	delete[] m_InitialConditions;
	delete[] m_FinalConditions;	  
   
    delete[] m_InitialStates;
    delete[] m_InitialInputs;
    
    delete[] m_FinalStates;
    delete[] m_FinalInputs; 
           
    delete[] m_Parameters;
    delete[] m_AllParams;
           
	for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
	{
	    for(m_j=0; m_j<m_NofStates; m_j++){delete[] m_States[m_i][m_j];}
	    for(m_j=0; m_j<m_NofInputs; m_j++){delete[] m_Inputs[m_i][m_j];}
	    delete[] m_States[m_i];
	    delete[] m_Inputs[m_i];
	}
	delete[] m_Time;
	delete[] m_States;
	delete[] m_Inputs;
    
    delete[] m_CollocPointsToRecover;
    
    for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
    {
        delete[] m_Lt[m_i];
        delete[] m_Ut[m_i];
    }
    delete[] m_Lt;
    delete[] m_Ut;
        
    delete[] m_positionIntervals;
    delete[] m_SurfaceDegreeOfPolynomials;
    delete[] m_SurfaceCurveSmoothness;
    delete[] m_SurfaceMaxDerivatives;        

    for(m_i=0; m_i<m_NofSurfaceControlPoints[0]; m_i++)
    {
        delete[] m_SurfaceControlPoints[m_i];
    }
    delete[] m_SurfaceControlPoints;
    delete[] m_NofSurfaceControlPoints;    
        
    delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
    
    delete m_AlgGeom;
    delete m_ReconfigOCProblem;	
    delete m_AliceFlat;  
    delete m_costComponent;
    //delete m_NURBSPoly;
    
    for(m_i=0; m_i<m_NofWaypoints; m_i++){delete[] m_RDDFData[m_i];}
    delete[] m_RDDFData;
        
//    delete m_ocptSpecs;
    
    //for(m_i=0; m_i<m_NofDimensions; m_i++){delete[] m_DataPoints[m_i];}
    //delete[] m_DataPoints;
    
}

void CDynamicPlanner::getCurrentParameterization(void)
{
    /* Query OC Problem Specification for data being sent by tplanner */
    m_ocptSpecs->startSolve();

    /* Get the current RDDF from OCPtSpecs */	
    cout << "Requesting an RDDF ... " << endl;
    while (1) 
    {
        m_ocptSpecs->getRDDFcorridor(m_RDDF);
        cout << "Got it! " << endl;
        if((m_RDDF != NULL) && (m_RDDF->getNumTargetPoints() > 0))
	    {
            cout << "Extracting data from RDDF ... " << endl;
            // Extract the data from the new RDDF file 
            m_NofWaypoints = m_RDDF->getNumTargetPoints();
            m_RDDFVector   = *(new RDDFVector);
            m_RDDFVector   = m_RDDF->getTargetPoints();
            cout << "No. of Waypoints = " << m_NofWaypoints << endl;
            cout.flush();
            m_RDDF->print();
            cout.flush();
            break;
	    }
        else
	    {
            cerr << "Empty RDDF ..." << endl;
            continue;
	    }
    }
    
    m_BoundsType = ubt_LowerBound;
    
    cout << endl;
    cout << "Initial Conditions ... " << endl;
    m_ocptSpecs->getInitialCondition(&m_BoundsType, m_InitialConditions);
    for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
    {
        cout << fixed << setprecision(5) << m_InitialConditions[m_i] << endl;
    }

    cout << endl;
    cout << "Final Conditions ...  " << endl;
    m_ocptSpecs->getFinalCondition(&m_BoundsType, m_FinalConditions);
    for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
    {
        cout << fixed << setprecision(5) << m_FinalConditions[m_i] << endl;
    }

    cout << endl;
    cout << "Parameters ... " << endl;
    m_ocptSpecs->getParams(m_AllParams);    
    for(m_i=0; m_i<m_NofAllParams; m_i++)
    {
        cout << fixed << setprecision(5) << m_AllParams[m_i] << endl;
    }

    cout << endl;
    cout << "Polytopes ... " << endl;
    //m_ocptSpecs->getNofPolytopes();
    //m_ocptSpecs->getPolytopes();


    //m_ocptSpecs->getSurfaceControlPoints(m_xNofControlPoints, &m_xInterval, &m_yInterval, m_SurfaceControlPoints);
    /*
    for(m_i=0;m_i<m_xNofControlPoints;m_i++)
    {   
        for(m_j=0;m_j<m_yNofControlPoints;m_j++)
        {
          cout << " " << m_SurfaceControlPoints[m_i][m_j];
        }
        cout << endl;
    }
    */
    m_ocptSpecs->endSolve(); 

    m_ell = 0;
    for(m_i=0; m_i<m_NofStates; m_i++)
    {
        m_InitialStates[m_i] = m_InitialConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    for(m_i=0; m_i<m_NofInputs; m_i++)
    {
        m_InitialInputs[m_i] = m_InitialConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    m_ell = 0;
    for(m_i=0; m_i<m_NofStates; m_i++)
    {
        m_FinalStates[m_i] = m_FinalConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    for(m_i=0; m_i<m_NofInputs; m_i++)
    {
        m_FinalInputs[m_i] = m_FinalConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    m_Parameters[0] = m_AllParams[0];  //L
    for(m_k = 0; m_k < m_NofConstraintCollocPoints; m_k++)
	{
		m_Lt[m_k][0] = m_AllParams[3];     /* vmin */
		m_Lt[m_k][1] = m_AllParams[5];     /* amin */
		m_Lt[m_k][2] = m_AllParams[7];     /* phimin */
		m_Lt[m_k][3] = m_AllParams[9];     /* phidmin */
		m_Lt[m_k][4] = -(m_AllParams[11]*m_AllParams[1])/(2*m_AllParams[2]); /* roll-over */

		m_Ut[m_k][0] = m_AllParams[4];     /* vmax */
		m_Ut[m_k][1] = m_AllParams[6];     /* amax */
		m_Ut[m_k][2] = m_AllParams[8];     /* phimax */
		m_Ut[m_k][3] = m_AllParams[10];    /* phidmax */
		m_Ut[m_k][4] = (m_AllParams[11]*m_AllParams[1])/(2*m_AllParams[2]); /* roll-over */
	}

    m_costComponent->setSurfaceControlPoints(&m_NofDimensions, m_NofSurfaceControlPoints, (const double** const) m_SurfaceControlPoints);
}

void CDynamicPlanner::getParameterizationFromFile(void)
{
    m_InitialConditions[0] =   3.0;          /* xi     */
    m_InitialConditions[1] = -15.0;          /* yi     */
    m_InitialConditions[2] =   0.0;          /* vi     */
    m_InitialConditions[3] = (1.0/2.0)*M_PI; /* thetai */
    m_InitialConditions[4] =   0.0;          /* ai     */
    m_InitialConditions[5] =   0.0;          /* phii   */   
    
    m_FinalConditions[0] =   -7.0;          /* xf     */
    m_FinalConditions[1] =    7.0;          /* yf     */
    m_FinalConditions[2] =    0.0;          /* vf     */
    m_FinalConditions[3] =    M_PI;         /* thetaf */
    m_FinalConditions[4] =    0.0;          /* af     */
    m_FinalConditions[5] =    0.0;          /* phif   */              
                    
    m_InitialPoint[0] = m_InitialConditions[0];
    m_InitialPoint[1] = m_InitialConditions[1];

    m_FinalPoint[0]   = m_FinalConditions[0];
    m_FinalPoint[1]   = m_FinalConditions[1];
                    
    m_AllParams[0]  = 5.43560;		// L
    m_AllParams[1]  = 2.13360;		// W
    m_AllParams[2]  = 1.06680;		// hcg
    m_AllParams[3]  = 0.10000;		// v_min
    m_AllParams[4]  = 26.8224;		// v_max
    m_AllParams[5]  =-3.00000;		// a_min
    m_AllParams[6]  = 0.98100;		// a_max
    m_AllParams[7]  =-0.44942;		// phi_min
    m_AllParams[8]  = 0.44942;		// phi_max
    m_AllParams[9]  =-1.30900;		// phid_min
    m_AllParams[10] = 1.30900;		// phid_max
    m_AllParams[11] = 9.81000;		// g
    m_AllParams[12] = 0.00000;		// vi
    m_AllParams[13] = 0.00000;		// vf
    m_AllParams[14] = 0.00000;		// ai
    m_AllParams[15] = 0.00000;		// af	
    
    m_ell = 0;
    for(m_i=0; m_i<m_NofStates; m_i++)
    {
        m_InitialStates[m_i] = m_InitialConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    for(m_i=0; m_i<m_NofInputs; m_i++)
    {
        m_InitialInputs[m_i] = m_InitialConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    m_ell = 0;
    for(m_i=0; m_i<m_NofStates; m_i++)
    {
        m_FinalStates[m_i] = m_FinalConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    for(m_i=0; m_i<m_NofInputs; m_i++)
    {
        m_FinalInputs[m_i] = m_FinalConditions[m_ell];
        m_ell = m_ell + 1;
    }
    
    m_Parameters[0] = m_AllParams[0];  //L
    for(m_k = 0; m_k < m_NofConstraintCollocPoints; m_k++)
	{
		m_Lt[m_k][0] = m_AllParams[3];     /* vmin */
		m_Lt[m_k][1] = m_AllParams[5];     /* amin */
		m_Lt[m_k][2] = m_AllParams[7];     /* phimin */
		m_Lt[m_k][3] = m_AllParams[9];     /* phidmin */
		m_Lt[m_k][4] = -(m_AllParams[11]*m_AllParams[1])/(2*m_AllParams[2]); /* roll-over */

		m_Ut[m_k][0] = m_AllParams[4];     /* vmax */
		m_Ut[m_k][1] = m_AllParams[6];     /* amax */
		m_Ut[m_k][2] = m_AllParams[8];     /* phimax */
		m_Ut[m_k][3] = m_AllParams[10];    /* phidmax */
		m_Ut[m_k][4] = (m_AllParams[11]*m_AllParams[1])/(2*m_AllParams[2]); /* roll-over */
	}
    
    int i=0;
    int j=0;
    int m_NofPolytopes = 14;
    int* m_NofPoints = new int[m_NofPolytopes];
    for(i=0; i<m_NofPolytopes; i++){m_NofPoints[i] = 4;}       
    
    double*** Points   = new double**[m_NofPolytopes];
    for(i=0; i<m_NofPolytopes; i++)
    {
        Points[i] = new double*[m_NofDimensions];
        for(j=0; j<m_NofDimensions; j++){Points[i][j] = new double[m_NofPoints[i]];}
    }
    
    Points[0][0][0]  =  -5.25;  Points[0][0][1] =  -0.25;   Points[0][0][2] = -0.25;  Points[0][0][3] = -5.25;
    Points[0][1][0]  = -20.00;  Points[0][1][1] = -20.00;   Points[0][1][2] =  0.00;  Points[0][1][3] =  0.00;
    
    Points[1][0][0]  =  -0.25;  Points[1][0][1] =   0.25;   Points[1][0][2] =  0.25;  Points[1][0][3] = -0.25;
    Points[1][1][0]  = -20.00;  Points[1][1][1] = -20.00;   Points[1][1][2] =  0.00;  Points[1][1][3] =  0.00;
    
    Points[2][0][0]  =   0.25;  Points[2][0][1] =   5.25;   Points[2][0][2] = 5.25;   Points[2][0][3]  = 0.25;
    Points[2][1][0]  = -20.00;  Points[2][1][1] = -20.00;   Points[2][1][2] = 0.00;   Points[2][1][3]  = 0.00;
    
    Points[3][0][0]  =   5.25;  Points[3][0][1] =  25.25;   Points[3][0][2] = 25.25;  Points[3][0][3] =  5.25;
    Points[3][1][0]  =   0.00;  Points[3][1][1] =   0.00;   Points[3][1][2] =  5.00;  Points[3][1][3] =  5.00;
    
    Points[4][0][0]  =   5.25;  Points[4][0][1] = 25.25;    Points[4][0][2] = 25.25;  Points[4][0][3] = 5.25;
    Points[4][1][0]  =   5.00;  Points[4][1][1] =  5.00;    Points[4][1][2] =  5.50;  Points[4][1][3] = 5.50;
    
    Points[5][0][0]  =   5.25;  Points[5][0][1] = 25.25;    Points[5][0][2] = 25.25;  Points[5][0][3] =  5.25;
    Points[5][1][0]  =   5.50;  Points[5][1][1] =  5.50;    Points[5][1][2] = 10.50;  Points[5][1][3] = 10.50;                    
    
    Points[6][0][0]  =  -5.25;  Points[6][0][1] = -25.25;   Points[6][0][2] = -25.25; Points[6][0][3] = -5.25;
    Points[6][1][0]  =   0.00;  Points[6][1][1] =   0.00;   Points[6][1][2] =   5.00; Points[6][1][3] =  5.00;
    
    Points[7][0][0]  =  -5.25;  Points[7][0][1] = -25.25;   Points[7][0][2] = -25.25; Points[7][0][3] = -5.25;
    Points[7][1][0]  =   5.00;  Points[7][1][1] =   5.00;   Points[7][1][2] =   5.50; Points[7][1][3] =  5.50;
    
    Points[8][0][0]  =  -5.25;  Points[8][0][1] = -25.25;   Points[8][0][2] = -25.25; Points[8][0][3] = -5.25;
    Points[8][1][0]  =   5.50;  Points[8][1][1] =   5.50;   Points[8][1][2] =  10.50; Points[8][1][3] = 10.50;
    
    Points[9][0][0]  =  -5.25;  Points[9][0][1] =  -5.25;   Points[9][0][2] =  -0.25; Points[9][0][3] = -0.25;
    Points[9][1][0]  =  10.50;  Points[9][1][1] =  30.50;   Points[9][1][2] =  30.50; Points[9][1][3] = 10.50;
    
    Points[10][0][0] =  -0.25; Points[10][0][1] = -0.25;   Points[10][0][2] =  0.25; Points[10][0][3] =  0.25;
    Points[10][1][0] =  10.50; Points[10][1][1] = 30.50;   Points[10][1][2] = 30.50; Points[10][1][3] = 10.50;
    
    Points[11][0][0] =   0.25; Points[11][0][1] =  0.25;   Points[11][0][2] =  5.25; Points[11][0][3] =  5.25;
    Points[11][1][0] =  30.50; Points[11][1][1] = 10.50;   Points[11][1][2] = 10.50; Points[11][1][3] = 30.50;                            
    
    Points[12][0][0] =  -5.25; Points[12][0][1] = -5.25;   Points[12][0][2] =  5.25; Points[12][0][3] =  5.25;
    Points[12][1][0] =  10.50; Points[12][1][1] =  0.00;   Points[12][1][2] =  0.00; Points[12][1][3] = 10.50;  

    Points[13][0][0] =  -5.25; Points[13][0][1] = -5.25;   Points[13][0][2] = -0.25; Points[13][0][3] = -0.25;
    Points[13][1][0] =   0.00; Points[13][1][1] =  5.50;   Points[13][1][2] =  0.00; Points[13][1][3] =  5.50; 
                         
    CPolytope* Polytopes = new CPolytope[m_NofPolytopes];
    for(i=0; i<m_NofPolytopes; i++)
    {
        Polytopes[i] = new CPolytope(&m_NofDimensions, &m_NofPoints[i], (const double** const) Points[i]);
        cout << &Polytopes[i] << endl; 
    }    
     
    m_Cost = 170;
    m_costComponent->PaintPolytope(&Polytopes[0], &m_Cost);
 
    m_Cost = 200;
    m_costComponent->PaintPolytope(&Polytopes[1], &m_Cost);

    m_Cost = 5;
    m_costComponent->PaintPolytope(&Polytopes[2], &m_Cost);
 
    m_Cost = 5;
    m_costComponent->PaintPolytope(&Polytopes[3], &m_Cost);

    m_Cost = 200;
    m_costComponent->PaintPolytope(&Polytopes[4], &m_Cost);
 
    m_Cost = 170;
    m_costComponent->PaintPolytope(&Polytopes[5], &m_Cost);

    m_Cost = 170;
    m_costComponent->PaintPolytope(&Polytopes[6], &m_Cost);
 
    m_Cost = 200;
    m_costComponent->PaintPolytope(&Polytopes[7], &m_Cost);

    m_Cost = 5;
    m_costComponent->PaintPolytope(&Polytopes[8], &m_Cost);
 
    m_Cost = 170;
    m_costComponent->PaintPolytope(&Polytopes[9], &m_Cost);

    m_Cost = 200;
    m_costComponent->PaintPolytope(&Polytopes[10], &m_Cost);
 
    m_Cost = 5;
    m_costComponent->PaintPolytope(&Polytopes[11], &m_Cost);

    m_Cost = 5;
    m_costComponent->PaintPolytope(&Polytopes[12], &m_Cost);
        
    m_Cost = 170;
    m_costComponent->PaintPolytope(&Polytopes[13], &m_Cost);
    
	char* scFileName = "SurfaceControlPoints.dat";
    m_Error = m_costComponent->SaveSurfaceControlPoints(scFileName);
    
    //char* RDDFFileName = "test.rddf";
    //m_Error = ReadFile(RDDFFileName, &m_NofWaypoints, &m_RDDFDataSize, m_RDDFData);
          
    //char* sFileName = "SurfaceControlPointsQ.dat";
    //m_costComponent->setSurfaceControlPoints(sFileName, &m_NofDimensions, m_NofSurfaceControlPoints)    
}

void CDynamicPlanner::ActiveLoop(void)
{
    cout << "Setting up Optimal Control Problem ... " << endl;      
	m_OCPSolver = new COCPSolver(m_ReconfigOCProblem);
	if(m_OCPSolver != NULL)
	{
        cout << "OCP Solver was created successfully" << endl;
	}
    getParameterizationFromFile();
    
    while(!m_IsSimulationMode)    
    {
        //getCurrentParameterization();
        //TranslateRDDFtoPolytopes();
        //ApproximateFeasibleRegion();
        
        /*
        m_ConstraintType = ct_LinInitial;
        m_BoundsType     = ubt_LowerBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_InitialStates);
        
        m_ConstraintType = ct_LinInitial;
        m_BoundsType     = ubt_UpperBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_InitialStates);
        
        m_ConstraintType = ct_LinFinal;
        m_BoundsType     = ubt_LowerBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_FinalStates);
        
        m_ConstraintType = ct_LinFinal;
        m_BoundsType     = ubt_UpperBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_FinalStates);
        
        m_ConstraintType = ct_NLinInitial;
        m_BoundsType     = ubt_LowerBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_InitialStates);
        
        m_ConstraintType = ct_NLinInitial;
        m_BoundsType     = ubt_UpperBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_InitialStates);
        
        m_ConstraintType = ct_NLinFinal;
        m_BoundsType     = ubt_LowerBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_FinalStates);
        
        m_ConstraintType = ct_NLinFinal;
        m_BoundsType     = ubt_UpperBound;
        m_Error          = m_ReconfigOCProblem->setStates(&m_ConstraintType, &m_BoundsType, &m_NofStates, m_FinalStates);
        
        for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
        {
            m_FunctionType   = ft_Cost;
            m_Error          = m_ReconfigOCProblem->setOCPParameters(&m_FunctionType, &m_i, &m_NofParameters, m_Parameters);
            
            m_FunctionType   = ft_Constraint;
            m_Error          = m_ReconfigOCProblem->setOCPParameters(&m_FunctionType, &m_i, &m_NofParameters, m_Parameters);        
        }
        
        m_ConstraintType = ct_NLinTrajectory;
        for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
        {
            m_BoundsType = ubt_LowerBound;
            m_Error = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_i, &m_NofTNLinConstraints, m_Lt[m_i]);
            
            m_BoundsType = ubt_UpperBound;
            m_Error = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_i, &m_NofTNLinConstraints, m_Ut[m_i]);
        }
        
        cout << "Setting initial guess ... " << endl;
        for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
        {
            for(m_j=0; m_j<m_NofWeights[m_i]; m_j++){m_Weights[m_i][m_j] = 1.0;}
        }        
      */          
        m_positionIntervals[0].p0 = m_InitialPoint[0];
        m_positionIntervals[0].pf = m_FinalPoint[0];
        CreatePositionVector(&m_positionIntervals[0], &m_NofControlPoints[0], m_ControlPoints[0][0]);
        
        m_positionIntervals[1].p0 = m_InitialPoint[1];
        m_positionIntervals[1].pf = m_FinalPoint[1];
        CreatePositionVector(&m_positionIntervals[1], &m_NofControlPoints[0], m_ControlPoints[0][1]);
       
        m_ControlPoints[1][0][0] = 2;
           
        if(m_IsSimulationMode)
        {
            cout << "InitialGuess ... " << endl;
            cout << "Weights ... " << endl;
            for(m_k=0; m_k<m_NofFlatOutputs; m_k++)
            {
                for(m_j=0; m_j<m_NofWeights[m_k]; m_j++)
                {
                    cout << fixed << setprecision(7) << m_Weights[m_k][m_j] << "\t"; 
                }
                cout << endl;
            }
            
            cout << "Control Points ... " << endl;
            for(m_k=0; m_k<m_NofFlatOutputs; m_k++)
            {
                for(m_i=0; m_i<m_NofDimensionsOfFlatOutputs[m_k]; m_i++)
                {
                    for(m_j=0; m_j<m_NofControlPoints[m_k]; m_j++)
                    {
                        cout << fixed << setprecision(7) << m_ControlPoints[m_k][m_i][m_j] << "\t"; 
                    }
                    cout << endl;
                }
                cout << endl;
            }
        }
        
        m_SolverInform = 0;
        m_OptimalCost  = 0;
        
        /* Solve OCProblem */
        cout << "----------- NURBSBasedOTG -----------" << endl;
        cout << "Solving Optimal Control Problem ..." << endl;
        m_InitialTime = clock();
        m_OCPSolver->SolveOCProblem(&m_SolverInform, &m_OptimalCost, m_Weights, m_ControlPoints);
        m_FinalTime   = clock();
        cout << "OCP Solver returned with the following flag: " << m_SolverInform << endl; 
        char* sInform = m_OCPSolver->TranslateSolverInform(&m_SolverInform);
        cout << sInform << endl;
        delete sInform;
        
        m_Duration = (double)(m_FinalTime-m_InitialTime)/CLOCKS_PER_SEC;
        cout << "Time NURBSBasedOTG took to solve the OCProblem  = "<< m_Duration << " secs" << endl;
        cout << "-------------------------------------" << endl;
        cout.flush();
        
        if(m_SolverInform >= 1)
        {
            /* Recover Original System */
            m_ReconfigOCProblem->RecoverSystem((const double** const) m_Weights, (const double*** const) m_ControlPoints, &m_NofCollocPointsToRecover, m_CollocPointsToRecover, m_Time, (double*** const) m_States, (double*** const) m_Inputs);        
            cout << "Original system recovered successfully ..." << endl;
            
            m_IsSimulationMode = true;
            
            if(m_IsSimulationMode)
            {
                char* sSolutionFileName = "AliceFlat_OptimalSolution.dat";
                m_Error = OTG_WriteFile(sSolutionFileName, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs, m_NofWeights, (double** const) m_Weights, m_NofControlPoints, (double*** const) m_ControlPoints, false);
                cout << "Recovered System " << endl;
                cout << "States: " << endl;
                cout << "   x   " << '\t' << "   xd   " << '\t' << "   xdd   "<< '\t' << "   y   " << '\t' << "   yd   " <<  '\t' << "   ydd   " << '\t' << "   v   " << '\t'<< "   vd   " << '\t' << "   theta   " << '\t' << "   thetad   " << endl;
                for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
                {
                    cout << m_Time[m_i] << '\t';
                    for(m_j=0; m_j<m_NofStates; m_j++)
                    {
                        for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]; m_k++)
                        {
                            cout << m_States[m_i][m_j][m_k] << '\t';
                        }
                    }
                    cout << endl;
                }
                cout << endl;
                for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
                {
                    cout << sqrt(m_States[m_i][2][1]*m_States[m_i][2][1] + m_States[m_i][3][1]*m_States[m_i][3][1]*m_States[m_i][2][0]*m_States[m_i][2][0]) << '\t' <<
                    sqrt(m_States[m_i][0][2]*m_States[m_i][0][2] + m_States[m_i][1][2]*m_States[m_i][1][2]) << endl;
                }
                cout << endl;
                
                cout << "Inputs: " << endl;
                cout << "   a   " << '\t' << "   phi   " << '\t' << "   phid   " << endl;
                for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
                {
                    for(m_j=0; m_j<m_NofInputs; m_j++)
                    {
                        for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]; m_k++)
                        {
                            cout << m_Inputs[m_i][m_j][m_k] << '\t';
                        }
                    }
                    cout << endl;
                }
                cout << endl;
                
                /* Save file with optimal trajectory for dplanner. */
                char* sDplannerFileName = "Traj.dat";
                int   TrajRows = m_NofCollocPointsToRecover;
                int   TrajCols = m_NofDerivativesOfStates[0] + m_NofDerivativesOfStates[1] + 1;
                double** DplannerTraj = new double*[TrajRows];
                for(m_i=0; m_i<TrajRows; m_i++)
                {
                    DplannerTraj[m_i] = new double[TrajCols];
                }                
                
                for(m_i=0; m_i<TrajRows; m_i++)
                {
                    DplannerTraj[m_i][0] = m_Time[m_i];
                    DplannerTraj[m_i][1] = m_States[m_i][1][0];
                    DplannerTraj[m_i][2] = m_States[m_i][1][1];
                    DplannerTraj[m_i][3] = m_States[m_i][1][2];
                    DplannerTraj[m_i][4] = m_States[m_i][0][0];
                    DplannerTraj[m_i][5] = m_States[m_i][0][1];
                    DplannerTraj[m_i][6] = m_States[m_i][0][2];         
                }
                m_Error = OTG_WriteFile(sDplannerFileName, &TrajRows, &TrajCols, (const double** const) DplannerTraj, false); 
                for(m_i=0; m_i<TrajRows; m_i++){delete[] DplannerTraj[m_i];}        
                delete[] DplannerTraj; 
                
                /* Save file with optimal trajectory. */
                char* sTrajFileName = "Data.traj";
                TrajRows = m_NofCollocPointsToRecover;
                TrajCols = m_NofDerivativesOfStates[0] + m_NofDerivativesOfStates[1];
                double** TrajData = new double*[TrajRows];
                for(m_i=0; m_i<TrajRows; m_i++)
                {
                    TrajData[m_i] = new double[TrajCols];
                }
                
                for(m_i=0; m_i<TrajRows; m_i++)
                {
                    TrajData[m_i][0] = m_States[m_i][1][0];
                    TrajData[m_i][1] = m_States[m_i][1][1];
                    TrajData[m_i][2] = m_States[m_i][1][2];
                    TrajData[m_i][3] = m_States[m_i][0][0];
                    TrajData[m_i][4] = m_States[m_i][0][1];
                    TrajData[m_i][5] = m_States[m_i][0][2];         
                }
                
                m_Error = OTG_WriteFile(sTrajFileName, &TrajRows, &TrajCols, (const double** const) TrajData, false); 
                
                for(m_i=0; m_i<TrajRows; m_i++){delete[] TrajData[m_i];}        
                delete[] TrajData;       
            }
            
            /* Initialize the trajectory */
            m_Traj->startDataInput();
            
            /* Store the trajectory */
            for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
            {
                m_Traj->addPoint(m_States[m_i][1][0],m_States[m_i][1][1],m_States[m_i][1][2],m_States[m_i][0][0],m_States[m_i][0][1],m_States[m_i][0][2]);
            }
            
            /* Sending  trajectory */
            SendTraj(m_SendSocket, m_Traj);
        }
        else
        {
            /* Validate answer and return status to tplanner */
        }
    }
    /* Free Resources */
    delete m_OCPSolver;
}

void CDynamicPlanner::ApproximateFeasibleRegion(void)
{
    m_InitialPoint[0] = m_InitialStates[0];
    m_InitialPoint[1] = m_InitialStates[1];

    m_FinalPoint[0] = m_FinalStates[0];
    m_FinalPoint[1] = m_FinalStates[1];
    
    cout << "Constructing approximation of feasible region ... " << endl;
    m_FlatOutput = 0;
    int NofConvexSets = 6;
    m_NLPProblem = new CNLPProblem(&m_NofConvexSets,
                                   m_ConvexSets,
                                   &m_NofDimensions,
                                   &m_NofWaypoints,
                                   (const double** const) m_DataPoints, 
                                   m_InitialPoint,
                                   m_FinalPoint,
                                   &m_NofDimensionsOfFlatOutputs[m_FlatOutput],
                                   &m_DegreeOfPolynomials[m_FlatOutput],
                                   &m_CurveSmoothness[m_FlatOutput]);
                                                      
    m_NLPSolver = new CNLPSolver(m_NLPProblem);
    m_Error = m_NLPSolver->SolveNLPProblem(); 

    /* Here's an opportunity to guess again. How many times? It's up for grabs.*/
    int counter = 10;
    if(m_Error != 1)
    {
        counter = 0;
    }
    
    while(counter != 10)
    {
       m_NLPProblem->ResetInitialGuess();
       m_Error = m_NLPSolver->SolveNLPProblem();  
       if(m_Error == 1)
       {
          counter = 10;
       }
       else
       {
          counter++;
       }
    }
 
    /* Obtain the optimal solution */
    m_FlatOutput = 0;
    m_NLPProblem->getControlPoints(&m_NofDimensionsOfFlatOutputs[m_FlatOutput], 
                                   &m_NofControlPoints[m_FlatOutput], 
                                   (double** const) m_ControlPoints[m_FlatOutput]);
    
    m_NofPolytopes = m_NofConvexSets;  
    m_Polytopes    = new CPolytope[m_NofPolytopes]; 
    m_NLPProblem->getPolytopes(&m_NofPolytopes, m_Polytopes);

    cout << "Printing scaled approximating polytopes ..." << endl;                        
    for(m_i = 0; m_i < m_NofPolytopes; m_i++)
    {
        cout << &m_Polytopes[m_i] << endl;
	    cout << endl;
    }       
    delete[] m_Polytopes;
    
    /* Initial Guess */
    cout << "Constructing the Initial Guess ... " << endl;
    m_FlatOutput = 0;
    for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_FlatOutput]; m_j++)
    {
        for(m_k=0; m_k<m_NofControlPoints[m_FlatOutput]; m_k++)
        {
            m_ControlPoints[m_FlatOutput][m_j][m_k] = m_ControlPoints[m_FlatOutput][m_j][m_k];
        }
    }
    
    m_FlatOutput = 1;
    m_ControlPoints[m_FlatOutput][0][0] = 10;
    
    /*
    int maxWeight = 0;
    srand((unsigned)time(NULL));
    for(m_k=0; m_k<m_NofFlatOutputs; m_k++)
    {
        for(m_j=0; m_j<m_NofWeights[m_k]; m_j++)
        {
            m_Weights[m_k][m_j] = rand();
        }
        
        maxWeight = m_Weights[m_k][0]; 
        for(m_j=1; m_j<m_NofWeights[m_k]; m_j++)
        {
            if(m_Weights[m_k][m_j] > maxWeight)
            {
               maxWeight = m_Weights[m_k][m_j];
            }
        }
        
        for(m_j=0; m_j<m_NofWeights[m_k]; m_j++)
        {
            m_Weights[m_k][m_j] = m_Weights[m_k][m_j]/maxWeight;
        }
    }
    */
}

void CDynamicPlanner::TranslateRDDFtoPolytopes(void)
{
    /* Constuct a feasible region wrt trajectory constraints using the RDDF data being sent by tplanner */
    cout << "Translating RDDF into overlapping polytopes" << endl;
    
    m_NofDataPoints = m_NofWaypoints;
    m_DataPoints = new double*[m_NofDimensions];
    for(m_i=0; m_i<m_NofDimensions; m_i++)
    {
        m_DataPoints[m_i] = new double[m_NofDataPoints];
        for(m_j=0; m_j<m_NofDataPoints; m_j++){m_DataPoints[m_i][m_j] = 0.0;}
    }
    
    m_NofConvexSets = m_NofWaypoints - 1;
    m_ConvexSets    = new CPolytope[m_NofConvexSets];
    
    /*
    m_Error = ConstructFeasibleRegion(&m_NofWaypoints, 
                                      &m_RDDFVector, 
                                      m_DataOffsets, 
                                      m_DataPoints, 
                                      &m_NofConvexSets, 
                                      m_ConvexSets);
    */
                                      
    m_Error = ConstructFeasibleRegion(&m_NofWaypoints, 
                            (const double** const)m_RDDFData,
                            (double*  const )m_DataOffsets,
                            (double** const )m_DataPoints,
                            &m_NofConvexSets, 
			                m_ConvexSets);                                      
    
    cout << "Printing scaled overllapping polytopes ..." << endl;
    for(m_i = 0; m_i < m_NofConvexSets; m_i++)
    {
	    cout << "Polytope " << m_i + 1 << endl;
	    cout << &m_ConvexSets[m_i] << endl;
	    cout << endl;
    }
}

void CDynamicPlanner::ConstructInitialGuess(void)
{
    int j   = 0;
    int k   = 0;
    int ell = 0;
    int index = 0;
    
    int FlatOutput = 0;

	double* CollocPoints = new double[m_NofControlPoints[FlatOutput]];
    CreateTimeVector(&m_tInterval, &m_NofControlPoints[FlatOutput], CollocPoints);

    double*** zetas = new double**[m_NofControlPoints[FlatOutput]];
    for(j=0; j<m_NofControlPoints[FlatOutput]; j++)
    {
        zetas[j] = new double*[m_NofDimensionsOfFlatOutputs[FlatOutput]];
        for(k=0; k<m_NofDimensionsOfFlatOutputs[0]; k++){zetas[j][k] = new double[m_NofDerivatives[FlatOutput]];}
    }   

	int* Lefts          = new int[m_NofControlPoints[FlatOutput]];
	double*** BMatrices = new double**[m_NofControlPoints[FlatOutput]];
    for(j=0; j<m_NofControlPoints[FlatOutput]; j++)
	{
		BMatrices[j] = new double*[m_NofDerivatives[FlatOutput]];
        for(k=0; k<m_NofDerivatives[0]; k++){BMatrices[j][k] = new double[m_OrderOfPolynomials[FlatOutput]];}
    }

    m_ReconfigOCProblem->RecoverFlatOutput(m_Weights[FlatOutput], 
                                         (const double** const) m_ControlPoints[FlatOutput], 
                                         &m_NofControlPoints[FlatOutput], 
                                         CollocPoints, 
                                         Lefts, 
                                         (double*** const) BMatrices,
                                         &FlatOutput, 
                                         (double*** const)zetas);

    double* A = new double[m_NofControlPoints[FlatOutput]*m_NofControlPoints[FlatOutput]];
    for(j=0; j<m_NofControlPoints[FlatOutput]*m_NofControlPoints[FlatOutput]; j++){A[j] = 0.0;}

    index = 0;
 	for(j=0; j<m_NofControlPoints[FlatOutput]; j++)
	{
		for(ell = Lefts[j]-m_DegreeOfPolynomials[FlatOutput]; ell <= Lefts[j]; ell++)
		{
 			index = ell*m_NofControlPoints[FlatOutput] + j;
			A[index]  = BMatrices[j][0][ell-(Lefts[j]-m_DegreeOfPolynomials[FlatOutput])];
		}
	}

    int NofChars = 1;
	char* stype = new char[NofChars];
	stype[0] = 'N';
	int len_stype = (int) strlen(stype);

	int NofRowsOfA = m_NofControlPoints[FlatOutput];
	int NofColsOfA = m_NofControlPoints[FlatOutput];
	int LDA        = m_NofControlPoints[FlatOutput];

	int* PivotIndices = new int[NofColsOfA];
	int SolverInform;

    int NofRows = m_NofControlPoints[FlatOutput];
    int NofCols = 1;
    double* x = new double[NofRows];
    double* y = new double[NofRows];

    m_positionIntervals[0].p0 = m_InitialStates[0];
    m_positionIntervals[0].pf = m_FinalStates[0];
    CreatePositionVector(&m_positionIntervals[0], &m_NofControlPoints[0], x);
    
    m_positionIntervals[1].p0 = m_InitialStates[1];
    m_positionIntervals[1].pf = m_FinalStates[1];
    CreatePositionVector(&m_positionIntervals[1], &m_NofControlPoints[0], y);
    
    dgetrf_(&NofRowsOfA, &NofColsOfA, A, &LDA, PivotIndices, &SolverInform);
	dgetrs_(stype, &NofColsOfA, &NofCols, A, &NofRowsOfA, PivotIndices, x, &NofRows, &SolverInform, len_stype);
	dgetrs_(stype, &NofColsOfA, &NofCols, A, &NofRowsOfA, PivotIndices, y, &NofRows, &SolverInform, len_stype);

    for(k=0; k<m_NofControlPoints[FlatOutput]; k++){m_ControlPoints[FlatOutput][0][k] = x[k];}
    for(k=0; k<m_NofControlPoints[FlatOutput]; k++){m_ControlPoints[FlatOutput][1][k] = y[k];}
    
    m_ControlPoints[1][0][0] = 10;
    
    /* Restore Resources */
    delete[] CollocPoints;        

    for(j=0; j<m_NofControlPoints[FlatOutput]; j++)
    {
        for(k=0; k<m_NofDimensionsOfFlatOutputs[FlatOutput]; k++){delete[] zetas[j][k];}
        delete[] zetas[j];
    }   
    delete[] zetas;

	for(j=0; j< m_NofControlPoints[FlatOutput]; j++)
	{
		for(k=0; k<m_NofDerivatives[0]; k++){delete[] BMatrices[j][k];}			
		delete[] BMatrices[j];	
	}
	delete[] BMatrices;	
    delete[] Lefts;	

    delete[] PivotIndices; 
    delete[] A;
    delete[] x;
    delete[] y;
    delete[] stype;
}

void CDynamicPlanner::SwitchOCProblem(const AliceProblemType* const apType)
{
	m_apType = *apType;	
}

  /*  	m_Parameters[0]  = 5.43560;		// L
    	m_Parameters[1]  = 2.13360;		// W
    	m_Parameters[2]  = 1.06680;		// hcg
    	m_Parameters[3]  = 0.00000;		// v_min
    	m_Parameters[4]  = 1.40800;		// v_max
    	m_Parameters[5]  =-3.00000;		// a_min
    	m_Parameters[6]  = 0.98100;		// a_max
    	m_Parameters[7]  =-0.44942;		// phi_min
    	m_Parameters[8]  = 0.44942;		// phi_max
    	m_Parameters[9]  =-1.30900;		// phid_min
    	m_Parameters[10] = 1.30900;		// phid_max
    	m_Parameters[11] = 9.81000;		// g
    	m_Parameters[12] = 0.35200;		// vi
    	m_Parameters[13] = 0.35200;		// vf
    	m_Parameters[14] = 0.00000;		// ai
    	m_Parameters[15] = 0.00000;		// af		
  */
