/*	DynamicPlanner_Math.hh
	DynamicPlanner
	
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-14-2006.
*/

#ifndef DYNAMICPLANNER_MATH_HH
#define DYNAMICPLANNER_MATH_HH

/* NofRowsOfA >= max(1,NofColsOfA) */
void dgetrf_(int* NofRowsOfA, int* NofColsOfA, double* A, int* LDA, int* PivotIndices, int* info);

/* NofRowsOfA >= max(1,N) and NofRowsOfB >= max(1,N) */
void dgetrs_(char* Type, int* NofColsOfA, int* NofColsOfB, double* A, int* NofRowsOfA, int* PivotIndeces, double* B, int* NofRowsOfB, int* info, int LenOfType);

#endif DYNAMICPLANNER_MATH_HH
