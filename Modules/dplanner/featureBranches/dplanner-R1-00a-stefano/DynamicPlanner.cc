/*
 *  DynamicPlanner.cc
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include "DynamicPlanner.hh"
#include "AlgGeom/AlgGeom_Utilities.hh"
#ifdef InAlice
    #include "interfaces/sn_types.h"
#endif

#ifdef InAlice
    CDynamicPlanner:: CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents):CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE), CRDDFTalker(true)
#else
    CDynamicPlanner:: CDynamicPlanner(COCPComponents* OCPComponents)
#endif
{
	m_i   = 0;
	m_j   = 0;
	m_k   = 0;
	m_ell = 0;
	
	m_Error = 0;
	m_Duration = 0.0;

	m_OCPComponents = OCPComponents;

    #ifdef InAlice
        /* Create an RDDF object */
        m_RDDF = new RDDF();
        if(m_RDDF != NULL){cout << "RDDF object was created successfully" << endl;}
		
        /* Create a Traj object (y,yd,ydd,x,xd,xdd) */
        m_Traj = new CTraj(3);
        if(m_Traj != NULL){cout << "Traj object was created successfully" << endl;}
		
        m_CostMapPlus = new CMapPlus();
        if(m_CostMapPlus != NULL){cout << "Cost Map Plus object was created successfully" << endl;}

        m_ocptSpecs = new OCPtSpecs(SkynetKey, false, 3);
        m_OCPComponents->addOCPComponent((IOCPComponent*)m_ocptSpecs);

        m_SendSocket = m_skynet.get_send_sock(SNtraj);
    #endif

    /* Place OCProblem code here after the accessors are defined. */
    m_NofStates = 4;
    m_NofInputs = 2;
    m_NofParameters = 16;

    m_InitialConditions = new double[m_NofStates + m_NofInputs];
    m_FinalConditions   = new double[m_NofStates + m_NofInputs];
    for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
    {
        m_InitialConditions[m_i] = 0.0;
        m_FinalConditions[m_i]   = 0.0;
    }

    m_InitialStates = new double[m_NofStates];
    m_InitialInputs = new double[m_NofInputs];

    m_FinalStates = new double[m_NofStates];
    m_FinalInputs = new double[m_NofInputs];
    
    m_Parameters = new double[m_NofParameters];
    for(m_i=0; m_i<m_NofParameters; m_i++){m_Parameters[m_i] = 0.0;}
    
    m_NofDimensions = 2;
    m_DataOffsets   = new double[m_NofDimensions];  

    #ifndef InAlice
        /* This code is useful for debugging when not in Alice */
        m_NofWaypoints   = 3;
        m_RDDFDataSize   = 5;
        m_RDDFData = new double*[m_NofWaypoints];
        for(m_i=0; m_i<m_NofWaypoints; m_i++)
        {
            m_RDDFData[m_i] = new double[m_RDDFDataSize];
            for(m_j=0; m_j<m_RDDFDataSize; m_j++){m_RDDFData[m_i][m_j] = 0.0;}
        }
    #endif        

}

CDynamicPlanner::~CDynamicPlanner(void)
{
    #ifdef InAlice
        delete m_RDDF;
        delete m_Traj;
        delete m_CostMapPlus;
        delete m_ocptSpecs;
    #endif
    
    #ifndef InAlice
        for(m_i=0; m_i<m_NofWaypoints; m_i++)
        {
            delete[] m_RDDFData[m_i];
        }
        delete[] m_RDDFData;
    #endif
}

void CDynamicPlanner::ActiveLoop(void)
{
    #ifdef InAlice
        m_ocptSpecs->startSolve();

        /* Get the current RDDF from OCPtSpecs */	
        cout << "Requesting an RDDF ... " << endl;
        m_ocptSpecs->getRDDFcorridor(m_RDDF);
        cout << "Got it! " << endl;

        if((m_RDDF != NULL) && (m_RDDF->getNumTargetPoints() > 0))
        {
           cout << "Extracting data from RDDF ... " << endl;
          /* Extract the data from the new RDDF file */
           m_NofWaypoints = m_RDDF->getNumTargetPoints();
           m_RDDFVector   = m_RDDF->getTargetPoints();
           cout << "No. of Waypoints = " << m_NofWaypoints << endl;
           cout.flush();
           m_RDDF->print();
           cout.flush();
        }
        else
        {
          cerr << "Empty RDDF ..." << endl;
          exit(1);
        }

        /* Initialize the trajectory */
        m_Traj->startDataInput();
        
        /* Constuct a feasible region wrt trajectory constraints using the RDDF data being sent by tplanner */
        cout << "Translating RDDF into overlapping polytopes" << endl;
        m_DataPoints = new double*[m_NofDimensions];
        for(m_i=0; m_i<m_NofDimensions; m_i++)
        {
            m_DataPoints[m_i] = new double[m_NofWaypoints];
            for(m_j=0; m_j<m_NofWaypoints; m_j++){m_DataPoints[m_i][m_j] = 0.0;}
        }
        m_Error = ConstructFeasibleRegion(&m_NofWaypoints, m_RDDFVector, m_DataOffsets, m_DataPoints, &m_ConvexSets);
    #else   
        char* sFileName="./RDDFData1.dat";
        m_Error = OTG_ReadFile(sFileName, &m_NofWaypoints, &m_RDDFDataSize, m_RDDFData);
        
        cout << "Translating RDDF into overlapping polytopes ..." << endl;
        m_DataPoints = new double*[m_NofDimensions];
        for(m_i=0; m_i<m_NofDimensions; m_i++)
        {
            m_DataPoints[m_i] = new double[m_NofWaypoints];
            for(m_j=0; m_j<m_NofWaypoints; m_j++){m_DataPoints[m_i][m_j] = 0.0;}
        }    
        m_Error = ConstructFeasibleRegion(&m_NofWaypoints, (const double** const)m_RDDFData, m_DataOffsets, m_DataPoints, &m_ConvexSets);
    #endif
    
    cout << "Printing scaled overllapping polytopes ..." << endl;
	for(m_i = 0; m_i < m_ConvexSets.getNofPolytopes(); m_i++)
	{
	    cout << "Polytope " << m_i + 1 << endl;
        m_Polytope = m_ConvexSets.getPolytope(&m_i);
	    cout << m_Polytope << endl;
	    cout << endl;
	}
    
    /* Constructing OCProblem */
    /* -------------------------------------------------------------------*/
	/* 
       Create Alice problems: Currently only one is present - Alice_Full
	   In the future there will be a set of Alice problems being created here. 
	*/ 
	m_apType = apt_Full;
  
	m_Alice_Full = new CAlice_Full();
	if(m_Alice_Full != NULL){cout << "Alice OC problem definition was created successfully" << endl;}

    /* Modifying Alice_Full */
    m_NofConvexSets = m_ConvexSets.getNofPolytopes();
    m_Alice_Full->SetNofPolynomials(&m_NofConvexSets);

    #ifdef InAlice
        m_BoundsType = ubt_LowerBound;
        
        cout << endl;
        cout << "Initial Conditions ... " << endl;
        m_ocptSpecs->getInitialCondition(&m_BoundsType, m_InitialConditions);

        for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
        {
            cout << fixed << setprecision(5) << m_InitialConditions[m_i] << endl;
        }
        
        m_ell = 0;
        for(m_i=0; m_i<m_NofStates; m_i++)
        {
            m_InitialStates[m_i] = m_InitialConditions[m_ell];
            m_ell = m_ell + 1;
        }

        for(m_i=0; m_i<m_NofInputs; m_i++)
        {
            m_InitialInputs[m_i] = m_InitialConditions[m_ell];
            m_ell = m_ell + 1;
        }

        m_ConstraintType = ct_LinInitial;
        m_Error = m_Alice_Full->setStates(&m_ConstraintType, &m_NofStates, m_InitialStates);
        m_Error = m_Alice_Full->setInputs(&m_ConstraintType, &m_NofInputs, m_InitialInputs);

        cout << endl;
        cout << "Final Conditions ...  " << endl;
        m_ocptSpecs->getFinalCondition(&m_BoundsType, m_FinalConditions);
        for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
        {
            cout << fixed << setprecision(5) << m_FinalConditions[m_i] << endl;
        }

        m_ell = 0;
        for(m_i=0; m_i<m_NofStates; m_i++)
        {
            m_FinalStates[m_i] = m_FinalConditions[m_ell];
            m_ell = m_ell + 1;
        }

        for(m_i=0; m_i<m_NofInputs; m_i++)
        {
            m_FinalInputs[m_i] = m_FinalConditions[m_ell];
            m_ell = m_ell + 1;
        }

        m_ConstraintType = ct_LinFinal;
        m_Error = m_Alice_Full->setStates(&m_ConstraintType, &m_NofStates, m_FinalStates);
        m_Error = m_Alice_Full->setInputs(&m_ConstraintType, &m_NofInputs, m_FinalInputs);

        cout << endl;
        cout << "Parameters ... " << endl;
        m_ocptSpecs->getParams(m_Parameters);
        for(m_i=0; m_i<m_NofParameters; m_i++)
        {
            cout << fixed << setprecision(5) << m_Parameters[m_i] << endl;
        }
        m_FunctionType = ft_Constraint;
        m_i = 0; 
        double tmp = 0.0;
        m_Alice_Full->setOCPParameters(&m_i, &tmp, &m_FunctionType, &m_NofParameters, m_Parameters);
    #endif

	m_ReconfigOCProblem = new CReconfigOCProblem(m_Alice_Full, m_OCPComponents);
	if(m_ReconfigOCProblem != NULL)
	{
	   cout << "Reconfig OC Problem was created successfully" << endl;
	}		
    
	m_OCPSolver = new COCPSolver(m_ReconfigOCProblem);
	if(m_OCPSolver != NULL)
	{
	   cout << "OCP Solver was created successfully" << endl;
	}
	
    m_NofFlatOutputs = m_ReconfigOCProblem->getNofFlatOutputs();

    m_NofDimensionsOfFlatOutputs = new int[m_NofFlatOutputs];
    m_NofControlPoints           = new int[m_NofFlatOutputs];
    m_NofWeights                 = new int[m_NofFlatOutputs];
    
    m_NURBSDataX = nd_NofDimensionsOfFlatOutputs;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs);
    
    m_InitialPoint = new double[m_NofDimensionsOfFlatOutputs[0]];
	m_FinalPoint   = new double[m_NofDimensionsOfFlatOutputs[0]];

    m_NURBSDataX = nd_NofControlPoints;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofControlPoints);
    
    m_NURBSDataX = nd_NofWeights;
   	m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofWeights);
    
	m_Weights       = new double*[m_NofFlatOutputs];
	m_ControlPoints = new double**[m_NofFlatOutputs];
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		for(m_j=0; m_j<m_NofWeights[m_i]; m_j++){m_Weights[m_i][m_j] = 0.0;}

		m_ControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
			m_ControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++)
            {
                m_ControlPoints[m_i][m_j][m_k] = 0.0;
            }
		}
	}

    m_DegreeOfPolynomials = new int[m_NofFlatOutputs];
    m_CurveSmoothness     = new int[m_NofFlatOutputs];
        
    m_NURBSDataX = nd_DegreeOfPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_DegreeOfPolynomials);
    
    m_NURBSDataX = nd_CurveSmoothness;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_CurveSmoothness);
    
    m_FunctionType = ft_Constraint;
    m_NofConstraintCollocPoints = m_ReconfigOCProblem->getNofCollocPoints(&m_FunctionType);
    
	m_NofStates     = m_ReconfigOCProblem->getNofStates();
	m_NofInputs     = m_ReconfigOCProblem->getNofInputs();
	m_NofParameters = m_ReconfigOCProblem->getNofParameters();
    
	m_NofDerivativesOfStates = new int[m_NofStates];
	m_NofDerivativesOfInputs = new int[m_NofInputs];
    
    m_ReconfigOCProblem->getSystemsInfo(&m_NofStates, m_NofDerivativesOfStates, &m_NofInputs, m_NofDerivativesOfInputs);
 
	m_Time   = new double[m_NofConstraintCollocPoints];
	m_States = new double**[m_NofConstraintCollocPoints];
	m_Inputs = new double**[m_NofConstraintCollocPoints];
	for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
	{
		m_States[m_i] = new double*[m_NofStates];
		for(m_j=0; m_j<m_NofStates; m_j++)
		{
			m_States[m_i][m_j] = new double[m_NofDerivativesOfStates[m_j]+1];
            for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]+1; m_k++)
            {
                m_States[m_i][m_j][m_k] = 0.0;
            }
		}
		
		m_Inputs[m_i] = new double*[m_NofInputs];
		for(m_j=0; m_j<m_NofInputs; m_j++)
		{
			m_Inputs[m_i][m_j] = new double[m_NofDerivativesOfInputs[m_j]+1];
            for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]+1; m_k++)
            {
                m_Inputs[m_i][m_j][m_k] = 0.0;
            }
		}
	}	
    /* -------------------------------------------------------------------*/

    for(m_j = 0; m_j < m_NofDimensions; m_j++)
    {
        m_InitialPoint[m_j] = m_RDDFData[0][m_j+1]- m_DataOffsets[m_j];
        m_FinalPoint[m_j]   = m_RDDFData[m_NofWaypoints-1][m_j+1]-m_DataOffsets[m_j];
    }
    cout << "Constructing approximation of feasible region ... " << endl;
 	/* Construct an inner approximation of the feasible region by a set of overlapping polytopes */	
	m_FlatOutput = 0;
    
    m_NLPProblem = new CNLPProblem(&m_ConvexSets,
                                   (const double** const) m_DataPoints, 
                                   m_InitialPoint,
                                   m_FinalPoint,
                                   &m_NofDimensionsOfFlatOutputs[m_FlatOutput],
                                   &m_DegreeOfPolynomials[m_FlatOutput],
                                   &m_CurveSmoothness[m_FlatOutput]);
                                                      
    m_NLPSolver = new CNLPSolver(m_NLPProblem);
    m_Error = m_NLPSolver->SolveNLPProblem(); 
    
    cout << "Printing scaled approximating polytopes ..." << endl;
	for(m_i=0; m_i<m_Polytopes.getNofPolytopes(); m_i++)
	{
        m_Polytope = m_Polytopes.getPolytope(&m_i);
	    cout << m_Polytope << endl;
	    cout << endl;
	}

    cout << "Control Points ... " << endl;    
    for(m_i=0; m_i<m_NofDimensionsOfFlatOutputs[m_FlatOutput]; m_i++)
    {
        for(m_j=0; m_j<m_NofControlPoints[m_FlatOutput]; m_j++)
        {
            cout << m_ControlPoints[m_FlatOutput][m_i][m_j] << endl; 
        }
    }

    cout << "Setting up Optimal Control Problem ... " << endl;    

    /* Initial Guess */
    cout << "Constructing the Initial Guess ... " << endl;

    ConstructInitialGuess(m_ReconfigOCProblem, 
                          (double** const) m_Weights, 
                          (double*** const) m_ControlPoints, 
                          m_InitialConditions, 
                          m_FinalConditions);

    cout << "InitialGuess ... " << endl;
    cout << "Weights ... " << endl;
    for(m_k=0; m_k<m_NofFlatOutputs; m_k++)
    {
        for(m_j=0; m_j<m_NofWeights[m_k]; m_j++)
        {
            cout << fixed << setprecision(7) << m_Weights[m_k][m_j] << "\t"; 
        }
        cout << endl;
    }
 
    cout << "Control Points ... " << endl;
    for(m_k=0; m_k<m_NofFlatOutputs; m_k++)
    {
        for(m_i=0; m_i<m_NofDimensionsOfFlatOutputs[m_k]; m_i++)
        {
            for(m_j=0; m_j<m_NofControlPoints[m_k]; m_j++)
            {
                cout << fixed << setprecision(7) << m_ControlPoints[m_k][m_i][m_j] << "\t"; 
            }
            cout << endl;
        }
        cout << endl;
    }

    m_SolverInform = 0;
    m_OptimalCost  = 0;
    
    /* Solve OCProblem */
    cout << "----------- NURBSBasedOTG -----------" << endl;
    cout << "Solving Optimal Control Problem ..." << endl;
    m_InitialTime = clock();
    m_OCPSolver->SolveOCProblem(&m_SolverInform, &m_OptimalCost, m_Weights, m_ControlPoints);
    m_FinalTime   = clock();
    cout << "OCP Solver returned with the following flag: " << m_SolverInform << endl; 
    
    m_Duration = (double)(m_FinalTime-m_InitialTime)/CLOCKS_PER_SEC;
    cout << "Time NURBSBasedOTG took to solve the OCProblem  = "<< m_Duration << " secs" << endl;
    cout << "-------------------------------------" << endl;
    cout.flush();
    
    /* Recover Original System */
    m_ReconfigOCProblem->RecoverSystem((const double** const) m_Weights, (const double*** const) m_ControlPoints, m_Time, (double*** const) m_States,(double*** const) m_Inputs);
    cout << "Original system recovered successfully ..." << endl;

    cout << "Recovered System " << endl;
    for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
    {
        cout << m_Time[m_i] << '\t';
        for(m_j=0; m_j<m_NofStates; m_j++)
        {
            for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]+1; m_k++)
            {
                cout << m_States[m_i][m_j][m_k] << '\t';
            }
        }
         
        for(m_j=0; m_j<m_NofInputs; m_j++)
        {
            for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]+1; m_k++)
            {
                cout << m_Inputs[m_i][m_j][m_k] << '\t';
            }
        }
        cout << endl;
    }
    cout << endl;
    
#ifdef InAlice
    /* Store the trajectory */
    for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
    {
        m_Traj->addPoint(m_States[m_i][1][0],m_States[m_i][1][1],m_States[m_i][1][2],m_States[m_i][0][0],m_States[m_i][0][1],m_States[m_i][0][2]);
    }
    
	/* Sending  trajectory */
    SendTraj(m_SendSocket, m_Traj);


	//m_ocptSpecs->endSolve();
    /* Setup the Optimal Control Problem */
#endif

    /* Free Resources */
    /* -------------------------------------------------------------*/

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
	    delete[] m_Weights[m_i];
	}
	delete[] m_Weights;

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
	    for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
	    {
		    delete[] m_ControlPoints[m_i][m_j];
	    }
	    delete[] m_ControlPoints[m_i];
	}
	delete[] m_ControlPoints;
	
	delete[] m_NofDimensionsOfFlatOutputs;
	delete[] m_DegreeOfPolynomials;
	delete[] m_CurveSmoothness;
	delete[] m_NofControlPoints;
    delete[] m_NofWeights;
        
	for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
	{
	    for(m_j=0; m_j<m_NofStates; m_j++)
        {		
            delete[] m_States[m_i][m_j];
        }

	    for(m_j=0; m_j<m_NofInputs; m_j++)
        {
		    delete[] m_Inputs[m_i][m_j];
	    }
	    delete[] m_States[m_i];
	    delete[] m_Inputs[m_i];
	}
	delete[] m_Time;
	delete[] m_States;
	delete[] m_Inputs;
    
    delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
	
	delete [] m_InitialPoint;
	delete [] m_FinalPoint;

	delete[] m_InitialConditions;
	delete[] m_FinalConditions;		
    
    delete m_OCPSolver;
    delete m_ReconfigOCProblem;	
    delete m_Alice_Full;
    /* -------------------------------------------------------------*/

    /* Free Resources */
    m_ConvexSets.clear();
    m_Polytopes.clear();

}

void CDynamicPlanner::SwitchOCProblem(const AliceProblemType* const apType)
{
	m_apType = *apType;	
}

  /*  	m_Parameters[0]  = 5.43560;		// L
    	m_Parameters[1]  = 2.13360;		// W
    	m_Parameters[2]  = 1.06680;		// hcg
    	m_Parameters[3]  = 0.00000;		// v_min
    	m_Parameters[4]  = 1.40800;		// v_max
    	m_Parameters[5]  =-3.00000;		// a_min
    	m_Parameters[6]  = 0.98100;		// a_max
    	m_Parameters[7]  =-0.44942;		// phi_min
    	m_Parameters[8]  = 0.44942;		// phi_max
    	m_Parameters[9]  =-1.30900;		// phid_min
    	m_Parameters[10] = 1.30900;		// phid_max
    	m_Parameters[11] = 9.81000;		// g
    	m_Parameters[12] = 0.35200;		// vi
    	m_Parameters[13] = 0.35200;		// vf
    	m_Parameters[14] = 0.00000;		// ai
    	m_Parameters[15] = 0.00000;		// af		
  */
