/*
 *  DynamicPlanner.hh
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/26/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#ifndef DYNAMICPLANNER_HH
#define DYNAMICPLANNER_HH

#include "InAlice.hh"

#ifdef InAlice
    /* Always place the OCPtSpecs.hh header file at the top, before all others */
    #include "ocpspecs/OCPtSpecs.hh"
    #include "interfaces/TpDpInterface.hh"
#endif

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "AlgGeom/Polytopes.hh"
#include "AlgGeom/AlgebraicGeometry.hh"

#include "DynamicPlanner_Utilities.hh"
#include "Alice_Full.hh"
#include "NURBSBasedOTG/OTG_OCPComponents.hh"
#include "NURBSBasedOTG/OTG_ReconfigOCProblem.hh"
#include "NURBSBasedOTG/OTG_OCPSolver.hh"
#include "NURBSBasedOTG/OTG_Utilities.hh"

#ifdef InAlice
    #include "skynettalker/StateClient.hh"
    #include "trajutils/TrajTalker.hh"
    #include "skynettalker/RDDFTalker.hh"
    #include "trajutils/CPath.hh"
    #include "cmap/CMap.hh"
    #include "cmap/CMapPlus.hh"
    #include "dgcutils/DGCutils.hh"
#endif

enum AliceProblemType{apt_Full, apt_Nominal, apt_BackingUp};

#ifdef InAlice
class CDynamicPlanner:public CStateClient, public CTrajTalker, public CRDDFTalker
#else
class CDynamicPlanner
#endif
{
    public:
        #ifdef InAlice
            CDynamicPlanner(int SkynetKey, bool WAIT_STATE, COCPComponents* OCPComponents);
        #else
            CDynamicPlanner(COCPComponents* OCPComponents);
        #endif

       ~CDynamicPlanner(void);

        void ActiveLoop(void);
        void SwitchOCProblem(const AliceProblemType* const apType);

    private:
        int m_i;
        int m_j;
        int m_k;
        int m_ell;		
        int m_Error;
		
        #ifdef InAlice
            RDDF*      m_RDDF;
            CTraj*     m_Traj;
            CMapPlus*  m_CostMapPlus;
            RDDFVector m_RDDFVector;
            OCPtSpecs* m_ocptSpecs;
        #endif	

        int m_SendSocket;
        int m_RDDFSocket;
        
        AliceProblemType m_apType;

        clock_t m_InitialTime;
        clock_t m_FinalTime;
        double m_Duration;

        COCPComponents* m_OCPComponents;
	
        CAlice_Full*         m_Alice_Full;
        CReconfigOCProblem*  m_ReconfigOCProblem;
        COCPSolver*          m_OCPSolver;
        
        CNLPProblem* m_NLPProblem;
        CNLPSolver*  m_NLPSolver;
	
        int m_SolverInform;
        double m_OptimalCost;
				
        NURBSDataTypeX m_NURBSDataX;
        FunctionType   m_FunctionType;
        ConstraintType m_ConstraintType;
        BoundsType     m_BoundsType;
        
        int  m_FlatOutput;
        int  m_NofFlatOutputs;
        int* m_NofDimensionsOfFlatOutputs;
        int* m_NofControlPoints;
        int* m_NofWeights;
        int* m_DegreeOfPolynomials;
        int* m_CurveSmoothness;
												
        double**  m_Weights;
        double*** m_ControlPoints;
		
        int m_NofConstraintCollocPoints;
        int m_NofStates;
        int m_NofInputs;
        int m_NofParameters;		
		
        int* m_NofDerivativesOfStates;
        int* m_NofDerivativesOfInputs;
		
        double*   m_Time;
        double*** m_States;
        double*** m_Inputs;
        double*   m_Parameters;
		
        int m_NofWaypoints;
	    int m_RDDFDataSize;
	    double** m_RDDFData;

        int m_start_waypoint;
        int m_stop_waypoint;
		
        int m_NofConvexSets;
        CPolytopes m_ConvexSets;
        CPolytopes m_Polytopes;
        CPolytope* m_Polytope;
 
        int m_NofDimensions;
        double* m_DataOffsets;
        double** m_DataPoints;
        
        double* m_InitialPoint;
        double* m_FinalPoint;
		
        double* m_InitialConditions;
        double* m_FinalConditions;

        double* m_InitialStates;
        double* m_InitialInputs;

        double* m_FinalStates;
        double* m_FinalInputs;
        
        double m_t;
};

#endif /* DYNAMICPLANNER_HH*/
