/* 
	MyComponent.cpp
	Melvin E. Flores and Mark B. Milam
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 12-Mar-2007.
*/ 

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "MyComponent.hh" 

CMyComponent::CMyComponent(void)
{
	m_i = 0;
	m_j = 0;

	m_ocpComponentSizes.IsActive            = true;
	m_ocpComponentSizes.HasICostFunction    = false;
	m_ocpComponentSizes.HasTCostFunction    = false;
	m_ocpComponentSizes.HasFCostFunction    = false;
	m_ocpComponentSizes.NofILinConstraints  = 0;
	m_ocpComponentSizes.NofTLinConstraints  = 0;
	m_ocpComponentSizes.NofFLinConstraints  = 0;
	m_ocpComponentSizes.NofINLinConstraints = 0;
	m_ocpComponentSizes.NofTNLinConstraints = 8;
	m_ocpComponentSizes.NofFNLinConstraints = 0;
}

CMyComponent::~CMyComponent(void)
{

}

void CMyComponent::getOCPComponentSizes(OCPComponentSizes* const ocpComponentSizes)
{
	ocpComponentSizes->IsActive                  = m_ocpComponentSizes.IsActive;
	ocpComponentSizes->HasICostFunction          = m_ocpComponentSizes.HasICostFunction;
	ocpComponentSizes->HasTCostFunction          = m_ocpComponentSizes.HasTCostFunction;
	ocpComponentSizes->HasFCostFunction          = m_ocpComponentSizes.HasFCostFunction;
	ocpComponentSizes->NofILinConstraints        = m_ocpComponentSizes.NofILinConstraints;
	ocpComponentSizes->NofTLinConstraints        = m_ocpComponentSizes.NofTLinConstraints;
	ocpComponentSizes->NofFLinConstraints        = m_ocpComponentSizes.NofFLinConstraints;
	ocpComponentSizes->NofINLinConstraints       = m_ocpComponentSizes.NofINLinConstraints;
	ocpComponentSizes->NofTNLinConstraints       = m_ocpComponentSizes.NofTNLinConstraints;
	ocpComponentSizes->NofFNLinConstraints       = m_ocpComponentSizes.NofFNLinConstraints;
}

void CMyComponent::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{

}

void CMyComponent::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{

}

void CMyComponent::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{

}

void CMyComponent::getLinearConstraints(const ConstraintType* const ctype, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{
	
}

void CMyComponent::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset)
{

}

void CMyComponent::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{

	if(*mode == 0 || *mode == 2)
	{
		Cnlt[*Offset + 0] = z3/z5;
		Cnlt[*Offset + 1] = z6/(z5*z5);      
		Cnlt[*Offset + 2] = z7d/z5;      
		Cnlt[*Offset + 3] = z3*cos(z4)-z1d;      
		Cnlt[*Offset + 4] = z3*sin(z4)-z2d;      
		Cnlt[*Offset + 5] = z6-z3d;      
		Cnlt[*Offset + 6] = 0.1839723305614836*z3*tan(z7)-z4d;      
		Cnlt[*Offset + 7] = 0.1839723305614836*z3*z3/(z5*z5)*tan(z7);      
	}

	if(*mode == 1 || *mode == 2)
	{
		DCnlt[*Offset + 0][6] = 1/z5;      
		DCnlt[*Offset + 0][12] = -z3/(z5*z5);      
		DCnlt[*Offset + 1][12] = -2.0*z6/(z5*z5*z5);      
		DCnlt[*Offset + 1][13] = 1/(z5*z5);      
		DCnlt[*Offset + 2][12] = -z7d/(z5*z5);      
		DCnlt[*Offset + 2][17] = 1/z5;      
		DCnlt[*Offset + 3][1] = -1.0;      
		DCnlt[*Offset + 3][6] = cos(z4);      
		DCnlt[*Offset + 3][9] = -z3*sin(z4);      
		DCnlt[*Offset + 4][4] = -1.0;      
		DCnlt[*Offset + 4][6] = sin(z4);      
		DCnlt[*Offset + 4][9] = z3*cos(z4);      
		DCnlt[*Offset + 5][7] = -1.0;      
		DCnlt[*Offset + 5][13] = 1.0;      
		DCnlt[*Offset + 6][6] = 0.1839723305614836*tan(z7);      
		DCnlt[*Offset + 6][10] = -1.0;      
		DCnlt[*Offset + 6][16] = 0.1839723305614836*z3*(1.0+pow(tan(z7),2.0));      
		DCnlt[*Offset + 7][6] = 0.3679446611229671*z3/(z5*z5)*tan(z7);      
		DCnlt[*Offset + 7][12] = -0.3679446611229671*z3*z3/(z5*z5*z5)*tan(z7);      
		DCnlt[*Offset + 7][16] = 0.1839723305614836*z3*z3/(z5*z5)*(1.0+pow(tan(z7),2.0));      
	}

	if(*mode == 99)
	{
		 DCnlt[*Offset + 0][6] = 1.0; 
		 DCnlt[*Offset + 0][12] = 1.0; 
		 DCnlt[*Offset + 1][12] = 1.0; 
		 DCnlt[*Offset + 1][13] = 1.0; 
		 DCnlt[*Offset + 2][12] = 1.0; 
		 DCnlt[*Offset + 2][17] = 1.0; 
		 DCnlt[*Offset + 3][1] = 1.0; 
		 DCnlt[*Offset + 3][6] = 1.0; 
		 DCnlt[*Offset + 3][9] = 1.0; 
		 DCnlt[*Offset + 4][4] = 1.0; 
		 DCnlt[*Offset + 4][6] = 1.0; 
		 DCnlt[*Offset + 4][9] = 1.0; 
		 DCnlt[*Offset + 5][7] = 1.0; 
		 DCnlt[*Offset + 5][13] = 1.0; 
		 DCnlt[*Offset + 6][6] = 1.0; 
		 DCnlt[*Offset + 6][10] = 1.0; 
		 DCnlt[*Offset + 6][16] = 1.0; 
		 DCnlt[*Offset + 7][6] = 1.0; 
		 DCnlt[*Offset + 7][12] = 1.0; 
		 DCnlt[*Offset + 7][16] = 1.0; 
	}

}

void CMyComponent::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{

}

void CMyComponent::getLUBounds(const ConstraintType* const ctype, const BoundsType* const ubtype, const int* const NofConstraintCollocPoints, const int* const NofConstraints, double* const LUBounds, const int* const Offset)
{
	if(*ctype == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
            m_j = 0;
            for(m_i=0; m_i<*NofConstraintCollocPoints; m_i++)
            {
			    LUBounds[*Offset + m_j + 0] =0;
			    LUBounds[*Offset + m_j + 1] =-3;
			    LUBounds[*Offset + m_j + 2] =-1.309;
			    LUBounds[*Offset + m_j + 3] =0;
			    LUBounds[*Offset + m_j + 4] =0;
			    LUBounds[*Offset + m_j + 5] =0;
			    LUBounds[*Offset + m_j + 6] =0;
			    LUBounds[*Offset + m_j + 7] =-9.81;

                m_j = m_j + *NofConstraints;
            }
		}
		else
		{
            m_j = 0;
            for(m_i=0; m_i<*NofConstraintCollocPoints; m_i++)
            {
			    LUBounds[*Offset + m_j + 0] =1.408;
			    LUBounds[*Offset + m_j + 1] =0.981;
			    LUBounds[*Offset + m_j + 2] =-1.309;
			    LUBounds[*Offset + m_j + 3] =0;
			    LUBounds[*Offset + m_j + 4] =0;
			    LUBounds[*Offset + m_j + 5] =0;
			    LUBounds[*Offset + m_j + 6] =0;
			    LUBounds[*Offset + m_j + 7] =9.81;

                m_j = m_j + *NofConstraints;
            }
		}
	}

}


