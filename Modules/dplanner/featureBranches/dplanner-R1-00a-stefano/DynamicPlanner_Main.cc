/*
 *  DynamicPlanner_Main.cpp
 *  DynamicPlanner
 *
 *  Created by Melvin E. Flores on 11/21/06.
 *  Control and Dynamical Systems
 *	California Institute of Technology
 *
 *  Copyright 2006. All rights reserved.
 */

#include "InAlice.hh"

#ifndef InAlice
   // #include <conio.h>
#endif

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#ifdef InAlice
    #include "cmdline.h"
    #include "skynet/skynet.hh"
#endif

#include "NURBSBasedOTG/OTG_OCPComponents.hh" 

#ifndef NONGC
    #include "DynamicPlanner.hh"
#endif

#include "MyComponent.hh" 

using namespace std;

/*! This is the entry point to the dplanner. */
int main(int argc, char* argv[])
{ 
    #ifdef InAlice
	    /* Process command line arguments */
	    gengetopt_args_info cmdline;
	    if(cmdline_parser(argc, argv, &cmdline) != 0)
	    {
	    exit (1);
	    }
    	
	    /* Retrieve the skynet key */
	    int SkynetKey = skynet_findkey(argc, argv);

        if(cmdline.verbose_flag)
	    {
        cerr << "Connecting to skynet with KEY = " << SkynetKey<< endl;
        }
        bool WAIT_STATE = false;

	    if(cmdline.verbose_flag)
	    {
	       cout << "Dynamic Planner is starting..." << endl;
	    }
    #endif    	

    #ifndef NONGC
        CMyComponent* MyComponent = new CMyComponent();
          
	    /*!Collect all the plug and play OC problem components */
	    COCPComponents* OCPComponents = new COCPComponents();
        OCPComponents->addOCPComponent((IOCPComponent*)MyComponent);
        
	    /*! Create a instance of dplanner */
	    CDynamicPlanner* DynamicPlanner;
        #ifdef InAlice
	       DynamicPlanner = new CDynamicPlanner(SkynetKey, WAIT_STATE, OCPComponents);
        #else    
	       DynamicPlanner = new CDynamicPlanner(OCPComponents);
        #endif
	    DynamicPlanner->ActiveLoop();

	    cout << "Dynamic Planner is ending." << endl;
	
    	/* Restore memory resources */
	    delete DynamicPlanner;
	    delete OCPComponents;
    #endif

    #ifdef InAlice
    cout << "Press any key to continue... \n" << endl;
    //while(!_kbhit());
    #endif

	return (int) 0;
} 

