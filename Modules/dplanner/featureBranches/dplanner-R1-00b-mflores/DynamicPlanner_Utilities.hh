/* 
	DynamicPlanner_Utilities.hh
	DynamicPlanner
	
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-12-2006.
    Latest Modified on 3-20-2007.
*/ 

#ifndef DYNAMICPLANNER_UTILITIES_HH
#define DYNAMICPLANNER_UTILITIES_HH

#include <time.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "DynamicPlanner_Flags.hh"

#include "AlgGeom/Polytope.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/AlgGeom_Utilities.hh"

#include "NURBSBasedOTG/OTG_Interfaces.hh" 
#include "NURBSBasedOTG/OTG_NURBSBuilder.hh"
#include "NURBSBasedOTG/OTG_ReconfigOCProblem.hh"

#ifdef InAlice
    #include "dgcutils/RDDF.hh"
    
    int ConstructFeasibleRegion(const int* const NofWaypoints, 
                                RDDFVector *rddfvector,
                                double** const DataPoints, 
                                double*  const Headings,
                                const int* const NofConvexSets,
                                CPolytope* const ConvexSets);
#endif


int ConstructFeasibleRegion(const int* const NofWaypoints, 
                            const int* const RDDFDataSize,
                            const double** const RDDFData,
                            double** const DataPoints,
                            double*  const Headings,
                            const int* const NofConvexSets, 
			                CPolytope* const ConvexSets);


#endif /*DYNAMICPLANNER_UTILITIES_HH*/


