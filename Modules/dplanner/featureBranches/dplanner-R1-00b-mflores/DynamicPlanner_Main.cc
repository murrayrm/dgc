/*
 *  DynamicPlanner_Main.cc
 *  DynamicPlanner
 *
 *  Melvin E. Flores
 *  Control and Dynamical Systems
 *  California Institute of Technology
 *  May 30, 2007.
 *
 */
 
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "cmdline.h"

#ifdef InAlice
    #include "skynet/skynet.hh"
#endif

#include "DynamicPlanner.hh"

int main (int argc, char* argv[]) 
{
    int m_Error = 0;
    
    /* Process command line arguments */
	gengetopt_args_info cmdline;
	if(cmdline_parser(argc, argv, &cmdline) != 0){exit(1);}

    CDynamicPlanner* m_DynamicPlanner;
    #ifdef InAlice
        int SkynetKey = skynet_findkey(argc, argv);
        if(cmdline.verbose_flag){cerr << "Connecting to skynet with KEY = " << SkynetKey << endl;}
        bool WAIT_STATE = false;
        m_DynamicPlanner = new CDynamicPlanner(SkynetKey, WAIT_STATE);
    #else
        m_DynamicPlanner = new CDynamicPlanner();
        m_DynamicPlanner->setRDDFFileName("SanMarcos1.rddf");
    #endif
    
    if(cmdline.verbose_flag)
    {
        bool m_VerboseMode = true;
        m_DynamicPlanner->setVerboseMode(&m_VerboseMode);
    }

    if(cmdline.verbose_flag > 4)
    {
       bool m_DebugMode = true;
       m_DynamicPlanner->setDebugMode(&m_DebugMode);
    }
    
    int k=0;
    while( k <= 0)
    {
        if(cmdline.verbose_flag){cerr << "Solving OCProblem ... " << endl;} 
        m_Error = m_DynamicPlanner->SolveOCProblem();
        if(cmdline.verbose_flag){cerr << "Done! " << endl;} 
        //usleep(400000);
        k++;
    }
    
    /* Restore Resources */
    delete m_DynamicPlanner;
    
    return 0;
}
