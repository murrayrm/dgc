/*
 *  DynamicPlanner.hh
 *  DynamicPlanner
 *
 *  Melvin E. Flores
 *  Control and Dynamical Systems
 *  California Institute of Technology
 *  May 30, 2007.
 *
 */

#ifndef DYNAMICPLANNER_HH
#define DYNAMICPLANNER_HH

#include "DynamicPlanner_Flags.hh"

#ifdef InAlice
    /* Always place the OCPtSpecs.hh header file at the top, before all others. */
    #include "ocpspecs/OCPtSpecs.hh"
    #include "interfaces/TpDpInterface.hh"
#endif

#include <fstream>
#include <cfloat> 
#include <cmath>
#include <cstring>

#include "AliceFlat.hh"
#include "DynamicPlanner_Utilities.hh"

#ifdef __cplusplus
extern "C" {
#endif

#include "DynamicPlanner_Math.h"

#ifdef __cplusplus
}
#endif

#include "BSplineSurfaceMap/BS_CostComponent.hh"
#include "AlgGeom/NURBSPolyhedra.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/NLPProblem.hh"
#include "AlgGeom/NLPSolver.hh"

#include "NURBSBasedOTG/OTG_Interfaces.hh"
#include "NURBSBasedOTG/OTG_OCPComponents.hh"
#include "NURBSBasedOTG/OTG_ReconfigOCProblem.hh"
#include "NURBSBasedOTG/OTG_OCPSolver.hh"
#include "NURBSBasedOTG/OTG_Utilities.hh"
#include "NURBSBasedOTG/OTG_SNOPT.hh"

#ifdef InAlice
    #include "skynettalker/StateClient.hh"
    #include "trajutils/TrajTalker.hh"
    #include "skynettalker/RDDFTalker.hh"
    #include "trajutils/CPath.hh"
    #include "dgcutils/DGCutils.hh"
    #include "interfaces/sn_types.h"
#endif

#ifdef InAlice
    class CDynamicPlanner:public CStateClient, public CTrajTalker
#else
    class CDynamicPlanner
#endif
{
    public:
        #ifdef InAlice
            CDynamicPlanner(int SkynetKey, bool WAIT_STATE);
        #else
            CDynamicPlanner(void);
        #endif
       ~CDynamicPlanner(void);

        int  setRDDFFileName(const char* const RDDFFileName);
        void setDebugMode(const bool* const DebugMode);
        void setVerboseMode(const bool* const VerboseMode);

        int SolveOCProblem(void);
        
    private:
        int getRDDFData(void);
        int TranslateRDDFToOverlappingPolytopes(void);
        int getPolytopeContainers(void);
        int getNewOCProblem(void);
            
        int  getInitialGuess(void);
        int  getInitialVolumeGuess(void);
        int  ReconfigureOCProblem(void);
        void DetectStopMode(void);
        int  getOptimalTrajectory(void);
        int  printOptimalTrajectory(void);
        int  storeOptimalTrajectory(void);
        int  saveOptimalTrajectory(const char* const sFileName);
        int  setArtificialEndPoint(void);

        #ifdef InAlice
            int getNewOCPSpecs(void);
            int ReconstructPolytopes(void);
            int sendOptimalTrajectory(void);
            int sendStopTrajectory(void);
        #endif
        
        int m_i;
        int m_j;
        int m_k;
        int m_ell;
        
        int m_ErrorFlag;
        ALGErrorType alg_ErrorFlag;
        
        int    m_SolverInform;
        double m_OptimalCost;
        double m_InitialTime;
        double m_FinalTime;
        double m_Duration;
        
        bool m_DebugMode;
        bool m_VerboseMode;
        bool m_StopMode;
        bool m_UseVolumeGuess;
        bool m_ReusePreviousSolution;
        bool m_FirstProblem;

        NURBSDataType  m_NURBSData;        
        NURBSDataTypeX m_NURBSDataX;
        FunctionType   m_FunctionType;
        ConstraintType m_ConstraintType;
        BoundsType     m_BoundsType;
        ALGBoundsType  m_algBoundsType;
        
        SNOPTParams_int    m_params_int;
        SNOPTParams_double m_params_double;

        int m_DerivativeLevel;
        int m_MaxNofIterations;
        int m_MajorMaxNofIterations;
        int m_MinorMaxNofIterations;
        int m_MajorPrintLevel;
        int m_MinorPrintLevel;
        int m_VerifyLevel;
                        
        double m_FunctionPrecision;
        double m_FeasibleTolerance;
        double m_MajorFeasibilityTolerance;
        double m_MajorOptimalityTolerance;
        double m_MajorStepLimit;
        double m_MinorFeasibilityTolerance;
        
        COCPComponents*     m_OCPComponents;
        CCostComponent*     m_CostComponent;
        CAliceFlat*         m_AliceFlat;
        CReconfigOCProblem* m_ReconfigOCProblem;
        COCPSolver*         m_OCPSolver;
        CNURBSPolyhedra*    m_NURBSPoly;
        CAlgebraicGeometry* m_AlgGeom;
        CNLPProblem*        m_NLPProblem;
        CNLPSolver*         m_NLPSolver;
        
        int m_NofDimensions;
        
        PositionInterval* m_PositionIntervals;
        int    m_NofMetersPerPolynomial;
        int*   m_SurfaceDegreeOfPolynomials;
        int*   m_SurfaceCurveSmoothness;
        int*   m_SurfaceMaxDerivatives;
        double m_BaseCost;
        double m_PolytopeCost;
        
        int  m_FlatOutput;
        int  m_NofFlatOutputs;
        int* m_NofDimensionsOfFlatOutputs;
        int* m_NofPolynomials;
        int* m_DegreeOfPolynomials;
        int* m_OrderOfPolynomials;
        int* m_CurveSmoothness;
        int* m_NofDerivatives;
        int* m_NofWeights;
        int* m_NofControlPoints;
												
        double**  m_Weights;
        double*** m_ControlPoints;
        double*** m_LowerBoundsOnControlPoints;
        double*** m_UpperBoundsOnControlPoints;
        
        int m_NofStates;
        int m_NofInputs;
        int m_NofParameters;
        int m_NofAllParams;
        
        double* m_InitialConditionsLB;
        double* m_InitialConditionsUB;
        double* m_FinalConditionsLB;
        double* m_FinalConditionsUB;

        double* m_Parameters;
        
        double* m_InitialStates;
        double* m_InitialInputs;
        double* m_FinalStates;
        double* m_FinalInputs;
        double* m_AllParams;
        
        int      m_NofWayPoints;
        int      m_RDDFDataSize;
        double** m_RDDFData;
        
        char*    m_RDDFFileName;
        double** m_DataPoints;
        double*  m_Headings;
        
        int        m_NofMapPolytopes;
        CPolytope* m_MapPolytopes;
        CPolytope* m_ContainersOfMapPolytopes;
        
        bool       m_ChangePolytopeCoordinates;
        int        m_NofActivePolytopes;
        CPolytope* m_ActivePolytopes;
        
        int       m_NofActiveWayPoints;
        double**  m_ActiveWayPoints;
        
        int*      m_NofActivePoints;
        double*** m_ActivePoints;
                    
        double* m_InitialPoint;
        double* m_FinalPoint;    

        int      m_NofPoints;
        double** m_Points;
        double*  m_mins;
        double*  m_maxs;
        
        OCPSizes m_ocpSizes;
        int m_NofILinConstraints;
        int m_NofTLinConstraints;
        int m_NofFLinConstraints;
        int m_NofINLinConstraints;
        int m_NofTNLinConstraints;
        int m_NofFNLinConstraints;
        int m_NofCostCollocPoints;
        int m_NofConstraintCollocPoints;
        
        double* m_li;
        double* m_ui;
        
        double** m_lt;
        double** m_ut;
        
        double* m_lf;
        double* m_uf;

        double* m_Li;
        double* m_Ui;
        
        double** m_Lt;
        double** m_Ut;
        
        double* m_Lf;
        double* m_Uf;
        
        int* m_NofDerivativesOfStates;
        int* m_NofDerivativesOfInputs;
        
        int m_NofCollocPointsToRecover;
        double* m_CollocPointsToRecover;

        TimeInterval m_timeInterval;        
        double*   m_Time;
        double*** m_States;
        double*** m_Inputs;
        
        #ifdef InAlice
            RDDF*      m_RDDF;
            CTraj*     m_Traj;
            RDDFVector m_RDDFVector;
            OCPtSpecs* m_ocptSpecs;

            int  m_Verbose;
            bool m_useAstate;
            bool m_useCMap;
            bool m_useObstacles;
            bool m_useParameters;
            bool m_useRDDF;
            bool m_usePolyCorridor;        
            int m_SendSocket;       
        #endif
        
        /* for storing */
        bool m_StoreData;
        int m_TrajCounter;

        int       m_MaxNofTrajs;
        double*** m_TrajData;
        
        int* m_TrajNofPolytopes;
        CPolytope** m_TrajPolytopes;
        CPolytope** m_TrajModifiedConvexSets;
        CPolytope** m_TrajOverlappingConvexSets;
        
};

#endif /* DYNAMICPLANNER_HH*/