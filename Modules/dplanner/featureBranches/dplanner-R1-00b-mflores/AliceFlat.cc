/* 
	AliceFlat.cc
	Melvin E. Flores
	Northrop Grumman Space Technology
	Proprietary Level 1
	Redondo Beach, California
	Created on 07-May-2007.
*/ 

#include <float.h>
#include <stdlib.h>
#include <math.h>

#include "AliceFlat.hh" 

CAliceFlat::CAliceFlat(void)
{
	m_i = 0;
	m_j = 0;
	m_k = 0;

	m_TimeInterval.t0 = 0;
	m_TimeInterval.tf = 1;

	m_ocpSizes.NofFlatOutputs                = 2;
	m_ocpSizes.HasICostFunction              = false;
	m_ocpSizes.HasTCostFunction              = false;
	m_ocpSizes.HasFCostFunction              = true;
	m_ocpSizes.NofILinConstraints            = 2;
	m_ocpSizes.HasVariableTLinConstraints    = false;
	m_ocpSizes.NofTLinConstraints            = 3;
	m_ocpSizes.NofFLinConstraints            = 2;
	m_ocpSizes.NofINLinConstraints           = 1;
	m_ocpSizes.HasVariableTNLinConstraints   = false;
	m_ocpSizes.NofTNLinConstraints           = 5;
	m_ocpSizes.NofFNLinConstraints           = 1;
	m_ocpSizes.NofStates                     = 4;
	m_ocpSizes.NofInputs                     = 2;
	m_ocpSizes.NofParameters                 = 1;
	m_ocpSizes.HasVariableParameters         = false;
	m_ocpSizes.NofReferences                 = 0;
	m_ocpSizes.HasVariableReferences         = false;
	m_ocpSizes.IntegrationType               = qt_Simpsons38;
	m_ocpSizes.NofCostCollocPoints           = 11;
	m_ocpSizes.NofConstraintCollocPoints     = 31;
	m_ocpSizes.HasRecoverSystem              = true;
    m_ocpSizes.HasConstraintsOnWeights       = false;
    m_ocpSizes.HasConstraintsOnControlPoints = false;
    
	m_Parameters = new double[m_ocpSizes.NofParameters];
	m_Parameters[0] = 5.4356;

	m_NofVariables = 9;

	m_Ai = new double*[m_ocpSizes.NofILinConstraints];
	m_li = new double [m_ocpSizes.NofILinConstraints];
	m_ui = new double [m_ocpSizes.NofILinConstraints];
	for(m_i=0; m_i<m_ocpSizes.NofILinConstraints; m_i++)
	{
		m_Ai[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++){m_Ai[m_i][m_j] = 0.0;}
	}

	m_Ai[0][0]= 1.0;
	m_Ai[1][4]= 1.0;

    m_li[0] =  0.0;
	m_li[1] =  0.0;

	m_ui[0] =  0.0;
	m_ui[1] =  0.0; 
    
	m_At = new double**[m_ocpSizes.NofConstraintCollocPoints];
	m_lt = new double* [m_ocpSizes.NofConstraintCollocPoints];
	m_ut = new double* [m_ocpSizes.NofConstraintCollocPoints];
	for(m_i=0; m_i<m_ocpSizes.NofConstraintCollocPoints; m_i++)
	{
		m_At[m_i] = new double*[m_ocpSizes.NofTLinConstraints];
		m_lt[m_i] = new double [m_ocpSizes.NofTLinConstraints];
		m_ut[m_i] = new double [m_ocpSizes.NofTLinConstraints];
		for(m_j=0; m_j<m_ocpSizes.NofTLinConstraints; m_j++)
		{
			m_At[m_i][m_j] = new double[m_NofVariables];
			for(m_k=0; m_k<m_NofVariables; m_k++){m_At[m_i][m_j][m_k] = 0.0;}
		}
	}

	for(m_k = 0; m_k < m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		m_At[m_k][0][0]= 1.0;
		m_At[m_k][1][4]= 1.0;
		m_At[m_k][2][8]= 1.0;
	}

	for(m_k = 0; m_k < m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		m_lt[m_k][0] = 0.0;
		m_lt[m_k][1] = 0.0;
		m_lt[m_k][2] = 0.0;

		m_ut[m_k][0] = 0.0;
		m_ut[m_k][1] = 0.0;
		m_ut[m_k][2] = 0.0;            
	}

	m_Af = new double*[m_ocpSizes.NofFLinConstraints];
	m_lf = new double[m_ocpSizes.NofFLinConstraints];
	m_uf = new double[m_ocpSizes.NofFLinConstraints];
	for(m_i=0; m_i<m_ocpSizes.NofFLinConstraints; m_i++)
	{
		m_Af[m_i] = new double[m_NofVariables];
		for(m_j=0; m_j<m_NofVariables; m_j++){m_Af[m_i][m_j] = 0.0;}
	}

	m_Af[0][0]= 1.0;
	m_Af[1][4]= 1.0;
    
	m_lf[0] =  0.0;
	m_lf[1] =  0.0;

	m_uf[0] =  0.0;
	m_uf[1] =  0.0;
    
	m_Li = new double[m_ocpSizes.NofINLinConstraints];
	m_Ui = new double[m_ocpSizes.NofINLinConstraints];

	m_Li[0] = 0.0;
	m_Ui[0] = 0.0;
    
	m_Lt = new double*[m_ocpSizes.NofConstraintCollocPoints];
	m_Ut = new double*[m_ocpSizes.NofConstraintCollocPoints];
	for(m_i=0; m_i<m_ocpSizes.NofConstraintCollocPoints; m_i++)
	{
		m_Lt[m_i] = new double[m_ocpSizes.NofTNLinConstraints];
		m_Ut[m_i] = new double[m_ocpSizes.NofTNLinConstraints];
	}

	for(m_k = 0; m_k < m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		m_Lt[m_k][0] = 0.0;
		m_Lt[m_k][1] = 0.0;
		m_Lt[m_k][2] = 0.0;
		m_Lt[m_k][3] = 0.0;
		m_Lt[m_k][4] = 0.0;

		m_Ut[m_k][0] = 0.0;
		m_Ut[m_k][1] = 0.0;
		m_Ut[m_k][2] = 0.0;
		m_Ut[m_k][3] = 0.0;
		m_Ut[m_k][4] = 0.0;
	}

	m_Lf = new double[m_ocpSizes.NofFNLinConstraints];
	m_Uf = new double[m_ocpSizes.NofFNLinConstraints];

	m_Lf[0] = 0.0;
	m_Uf[0] = 0.0;
    
	m_NofDerivativesOfStates = new int[m_ocpSizes.NofStates];
	m_NofDerivativesOfInputs = new int[m_ocpSizes.NofInputs];

	m_NofDerivativesOfStates[0] = 3;
	m_NofDerivativesOfStates[1] = 3;
	m_NofDerivativesOfStates[2] = 2;
	m_NofDerivativesOfStates[3] = 2;

	m_NofDerivativesOfInputs[0] = 1;
	m_NofDerivativesOfInputs[1] = 2;
    
    m_NofPolynomials = new int[m_ocpSizes.NofFlatOutputs];
    m_NofPolynomials[0] = 1;
    m_NofPolynomials[1] = 1;
}

CAliceFlat::~CAliceFlat(void)
{
	delete[] m_Parameters;

	for(m_i=0; m_i<m_ocpSizes.NofILinConstraints; m_i++)
	{
		delete[] m_Ai[m_i];
	}
	delete[] m_Ai;
	delete[] m_li;
	delete[] m_ui;

	for(m_k=0; m_k<m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		for(m_i=0; m_i<m_ocpSizes.NofTLinConstraints; m_i++)
		{
			delete[] m_At[m_k][m_i];
		}
		delete[] m_At[m_k];
		delete[] m_lt[m_k];
		delete[] m_ut[m_k];
	}
	delete[] m_At;
	delete[] m_lt;
	delete[] m_ut;

	for(m_i=0; m_i<m_ocpSizes.NofFLinConstraints; m_i++)
	{
		delete[] m_Af[m_i];
	}
	delete[] m_Af;
	delete[] m_lf;
	delete[] m_uf;

	delete[] m_Li;
	delete[] m_Ui;

	for(m_k=0; m_k<m_ocpSizes.NofConstraintCollocPoints; m_k++)
	{
		delete[] m_Lt[m_k];
		delete[] m_Ut[m_k];
	}
	delete[] m_Lt;
	delete[] m_Ut;

	delete[] m_Lf;
	delete[] m_Uf;

	delete[] m_NofDerivativesOfStates;
	delete[] m_NofDerivativesOfInputs;
    
    delete[] m_NofPolynomials;

}

void CAliceFlat::getTimeInterval(TimeInterval* const timeInterval)
{
	timeInterval->t0 = m_TimeInterval.t0;
	timeInterval->tf = m_TimeInterval.tf;
}

void CAliceFlat::getOCPSizes(OCPSizes* const ocpSizes)
{
	ocpSizes->NofFlatOutputs                = m_ocpSizes.NofFlatOutputs;
	ocpSizes->HasICostFunction              = m_ocpSizes.HasICostFunction;
	ocpSizes->HasTCostFunction              = m_ocpSizes.HasTCostFunction;
	ocpSizes->HasFCostFunction              = m_ocpSizes.HasFCostFunction;
	ocpSizes->NofILinConstraints            = m_ocpSizes.NofILinConstraints;
	ocpSizes->NofTLinConstraints            = m_ocpSizes.NofTLinConstraints;
	ocpSizes->NofFLinConstraints            = m_ocpSizes.NofFLinConstraints;
	ocpSizes->NofINLinConstraints           = m_ocpSizes.NofINLinConstraints;
	ocpSizes->NofTNLinConstraints           = m_ocpSizes.NofTNLinConstraints;
	ocpSizes->NofFNLinConstraints           = m_ocpSizes.NofFNLinConstraints;
	ocpSizes->NofStates                     = m_ocpSizes.NofStates;
	ocpSizes->NofInputs                     = m_ocpSizes.NofInputs;
	ocpSizes->NofParameters                 = m_ocpSizes.NofParameters;
	ocpSizes->HasVariableParameters         = m_ocpSizes.HasVariableParameters;
	ocpSizes->NofReferences                 = m_ocpSizes.NofReferences;
	ocpSizes->HasVariableReferences         = m_ocpSizes.HasVariableReferences;
	ocpSizes->IntegrationType               = m_ocpSizes.IntegrationType;
	ocpSizes->NofCostCollocPoints           = m_ocpSizes.NofCostCollocPoints;
	ocpSizes->NofConstraintCollocPoints     = m_ocpSizes.NofConstraintCollocPoints;
	ocpSizes->HasRecoverSystem              = m_ocpSizes.HasRecoverSystem;
    ocpSizes->HasConstraintsOnWeights       = m_ocpSizes.HasConstraintsOnWeights;
    ocpSizes->HasConstraintsOnControlPoints = m_ocpSizes.HasConstraintsOnControlPoints;
}

void CAliceFlat::getNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, int* const Data)
{
	if(*dataType == dt_NofDimensionsOfFlatOutputs)
	{
		Data[0] = 2;
		Data[1] = 1;
	}
	else if(*dataType == dt_SolveForControlPoints)
	{
		Data[0] = 1;
		Data[1] = 1;
	}
	else if(*dataType == dt_SolveForWeights)
	{
		Data[0] = 0;
		Data[1] = 0;
	}
	else if(*dataType == dt_NofPolynomials)
	{
        for(m_i=0; m_i<*NofFlatOutputs; m_i++)
        {
            Data[m_i] = m_NofPolynomials[m_i];
        }
	}
	else if(*dataType == dt_DegreeOfPolynomials)
	{
		Data[0] = 7;
		Data[1] = 0;
	}
	else if(*dataType == dt_CurveSmoothness)
	{
		Data[0] =  3;
		Data[1] = -1;
	}
	else //if (*dataType == dt_MaxDerivatives)
	{
		Data[0] = 3;
		Data[1] = 0;
	}
}

void CAliceFlat::getBreakPoints(const TimeInterval* const timeInterval, const int* const NofFlatOutputs, const int* const NofBreakPoints, double** const BreakPoints)
{
	for(m_i=0; m_i<*NofFlatOutputs; m_i++)
	{
		CreateTimeVector(timeInterval, &NofBreakPoints[m_i], BreakPoints[m_i]);
	}
}

void CAliceFlat::getCollocPoints(const FunctionType* const functionType, const TimeInterval* const timeInterval, const int* const NofCollocPoints, double* const CollocPoints)
{
	CreateTimeVector(timeInterval, NofCollocPoints, CollocPoints);
}

void CAliceFlat::getOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, double* const Parameters)
{
	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		Parameters[m_i] = m_Parameters[m_i];
	}
}

void CAliceFlat::getOCPReferences(const int* const i, const double* const t, const int* const NofReferences, double* const References)
{

}

void CAliceFlat::getSystemsInfo(const int* const NofStates, int* const NofDerivativesOfStates, const int* const NofInputs, int* const NofDerivativesOfInputs)
{
	for(m_i = 0; m_i<*NofStates; m_i++)
	{
		NofDerivativesOfStates[m_i] = m_NofDerivativesOfStates[m_i];
	}

	for(m_i = 0; m_i<*NofInputs; m_i++)
	{
		NofDerivativesOfInputs[m_i] = m_NofDerivativesOfInputs[m_i];
	}
}

void CAliceFlat::RecoverSystem(const double* const tau, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, double* const t, const int* const NofStates, double** const x, const int* const NofInputs, double** const u)
{

	*t = *tau*z3;

	{
		double t1  = 1/z3;
		double t3  = z1d*z1d;      
		double t4  = z2d*z2d;      
		double t5  = t3+t4;      
		double t6  = 1/t5;      
		double t10 = z1d*z1dd+z2d*z2dd;      
		double t14 = z1d*z2dd-z2d*z1dd;      
		double t18 = z3*z3;      
		double t19 = 1/t18;      
		double t28 = sqrt(t5);      
		double t33 = atan2(z1d,z2d);      

		x1   = z1;      
		x1d  = z1d*t1;      
		x1dd = (z1d*t6*t10+z2d*t14*t6)*t19;      
		x2   = z2;      
		x2d  = z2d*t1;      
		x2dd = (z2d*t6*t10-z1d*t14*t6)*t19;      
		x3   = t28*t1;      
		x3d  = t10/t28*t19;      
		x4   = t33;      
		x4d  = t14*t6*t1;      
	}

	{
		double t3  = z1d*z1dd+z2d*z2dd;
		double t4  = z1d*z1d;      
		double t5  = z2d*z2d;      
		double t6  = t4+t5;      
		double t7  = sqrt(t6);      
		double t8  = 1/t7;      
		double t10 = z3*z3;      
		double t15 = z1d*z2dd-z2d*z1dd;      
		double t18 = atan2(p1*t15,t7*t6);      
		double t20 = t6*t6;      
		double t22 = p1*p1;      
		double t23 = t15*t15;      

		u1  = t3*t8/t10;      
		u2  = t18;      
		u2d = p1*t6/(t20*t6+t22*t23)*(3.0*t15*t3*t8-t7*(-z2d*z1ddd+z1d*z2ddd))/z3;      
	}

}

void CAliceFlat::InitialCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Fi, double* const DFi)
{

}

void CAliceFlat::TrajectoryCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ft, double* const DFt)
{
	if(*mode == 0 || *mode == 2)
	{
		double t3 = z1d*z1dd+z2d*z2dd;
		double t4 = t3*t3;      
		double t5 = z1d*z1d;      
		double t6 = z2d*z2d;      
		double t7 = t5+t6;      
		double t10 = z3*z3;      
		double t16 = z1d*z2dd-z2d*z1dd;      
		double t18 = sqrt(t7);      
		double t20 = atan2(p1*t16,t18*t7);      
		double t21 = t20*t20;      
		double t23 = p1*p1;      
		double t24 = t7*t7;      
		double t27 = t16*t16;      
		double t30 = pow(t24*t7+t23*t27,2.0);      
		double t40 = pow(3.0*t16*t3/t18-t18*(-z2d*z1ddd+z1d*z2ddd),2.0);      

		*Ft  = *Ft + t4/t7/t10/z3+z3*t21+t23*t24/t30*t40/z3;      
	}

	if(*mode == 1 || *mode == 2)
	{

		double t3 = z1d*z1dd+z2d*z2dd;
		double t4 = z1d*z1d;      
		double t5 = z2d*z2d;      
		double t6 = t4+t5;      
		double t7 = 1/t6;      
		double t8 = t3*t7;      
		double t9 = z3*z3;      
		double t11 = 1/t9/z3;      
		double t15 = t3*t3;      
		double t16 = t6*t6;      
		double t18 = t15/t16;      
		double t19 = t11*z1d;      
		double t24 = z1d*z2dd-z2d*z1dd;      
		double t25 = p1*t24;      
		double t26 = sqrt(t6);      
		double t27 = t26*t6;      
		double t28 = atan2(t25,t27);      
		double t29 = z3*t28;      
		double t31 = 1/t27;      
		double t33 = t26*t16;      
		double t34 = 1/t33;      
		double t39 = p1*p1;      
		double t40 = t24*t24;      
		double t41 = t39*t40;      
		double t42 = t16*t6;      
		double t46 = 1/(1.0+t41/t42);      
		double t51 = t42+t41;      
		double t52 = t51*t51;      
		double t53 = 1/t52;      
		double t54 = t39*t6*t53;      
		double t55 = 3.0*t24*t3;      
		double t56 = 1/t26;      
		double t60 = -z2d*z1ddd+z1d*z2ddd;      
		double t62 = t55*t56-t26*t60;      
		double t63 = t62*t62;      
		double t64 = 1/z3;      
		double t65 = t63*t64;      
		double t69 = t39*t16;      
		double t71 = 1/t52/t51;      
		double t72 = t69*t71;      
		double t75 = t39*t24;      
		double t82 = t69*t53;      
		double t83 = t62*t64;      
		double t89 = t31*z1d;      
		double t91 = t56*t60;      
		double t101 = t29*p1;      
		double t102 = z2d*t31;      
		double t106 = t39*t39;      
		double t108 = t106*t16*t71;      
		double t124 = t39*t33*t53;      
		double t131 = t11*z2d;      
		double t190 = t9*t9;      
		double t194 = t28*t28;      

		DFt[1] =  DFt[1] + 2.0*t8*t11*z1dd-2.0*t18*t19+2.0*t29*(p1*z2dd*t31-3.0*t25*t34*z1d)*t46+4.0*t54*t65*z1d-2.0*t72*t65*(6.0*t16*z1d+2.0*t75*z2dd)+2.0*t82*t83*(3.0*z2dd*t3*t56+3.0*t24*z1dd*t56-t55*t89-t91*z1d-t26*z2ddd);      
		DFt[2] =  DFt[2] +  2.0*t8*t19-2.0*t101*t102*t46+4.0*t108*t65*t24*z2d+2.0*t82*t83*(-3.0*z2d*t3*t56+3.0*t24*z1d*t56);      
		DFt[3] =  DFt[3] +  2.0*t124*t83*z2d;      
		DFt[5] =  DFt[5] + 2.0*t8*t11*z2dd-2.0*t18*t131+2.0*t29*(-p1*z1dd*t31-3.0*t25*t34*z2d)*t46+4.0*t54*t65*z2d-2.0*t72*t65*(6.0*t16*z2d-2.0*t75*z1dd)+2.0*t82*t83*(-3.0*z1dd*t3*t56+3.0*t24*z2dd*t56-t55*t102-t91*z2d+t26*z1ddd);      
		DFt[6] =  DFt[6] + 2.0*t8*t131+2.0*t101*t89*t46-4.0*t108*t65*t24*z1d+2.0*t82*t83*(3.0*z1d*t3*t56+3.0*t24*z2d*t56);      
		DFt[7] =  DFt[7] + -2.0*t124*t83*z1d;      
		DFt[8] =  DFt[8] + -3.0*t15*t7/t190+t194-t69*t53*t63/t9;      
	}
}

void CAliceFlat::FinalCostFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofReferences, const double* const References, const int* const NofParameters, const double* const Parameters, double* const Ff, double* const DFf)
{
    double tWeight = 1.0;
    
	if(*mode == 0 || *mode == 2)
	{
		*Ff = *Ff + z3*tWeight;
	}

	if(*mode == 1 || *mode == 2)
	{
		DFf[8] = DFf[8] + 1.0*tWeight;      
	}
}

void CAliceFlat::getLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A)
{
	if(*constraintType == ct_LinInitial)
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				A[m_j][m_k] = m_Ai[m_j][m_k];
			}
		}
	}
	else if(*constraintType == ct_LinTrajectory)
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				A[m_j][m_k] = m_At[*i][m_j][m_k];
			}
		}
	}
	else /*if(*constraintType == ct_LinFinal)*/
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				A[m_j][m_k] = m_Af[m_j][m_k];
			}
		}
	}
}

void CAliceFlat::InitialNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli)
{
	if(*mode == 0 || *mode == 2)
	{
		Cnli[0] = atan2(z1d,z2d);
	}

	if(*mode == 1 || *mode == 2)
	{
		double t2 = z1d*z1d;
		double t3 = z2d*z2d;      
		double t4 = 1/t3;      
		double t7 = 1/(1.0+t2*t4);      

		DCnli[0][1] = 1/z2d*t7;      
		DCnli[0][5] = -z1d*t4*t7;      
	}

	if(*mode == 99)
	{
		DCnli[0][1] = 1.0; 
		DCnli[0][5] = 1.0; 
	}
}

void CAliceFlat::TrajectoryNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt)
{
	if(*mode == 0 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = 1/z3;      
		double t9 = z1d*z1dd+z2d*z2dd;      
		double t10 = 1/t4;      
		double t12 = z3*z3;      
		double t13 = 1/t12;      
		double t17 = z1d*z2dd-z2d*z1dd;      
		double t20 = atan2(p1*t17,t4*t3);      
		double t22 = t3*t3;      
		double t24 = p1*p1;      
		double t25 = t17*t17;      

		Cnlt[0] = t4*t5;      
		Cnlt[1] = t9*t10*t13;      
		Cnlt[2] = t20;      
		Cnlt[3] = p1*t3/(t22*t3+t24*t25)*(3.0*t17*t9*t10-t4*(-z2d*z1ddd+z1d*z2ddd))*t5;      
		Cnlt[4] = t17*t10*t13;      
	}

	if(*mode == 1 || *mode == 2)
	{
		double t1 = z1d*z1d;
		double t2 = z2d*z2d;      
		double t3 = t1+t2;      
		double t4 = sqrt(t3);      
		double t5 = 1/t4;      
		double t6 = 1/z3;      
		double t7 = t5*t6;      
		double t10 = z3*z3;      
		double t11 = 1/t10;      
		double t14 = t11*z1dd*t5;      
		double t17 = z1d*z1dd+z2d*z2dd;      
		double t18 = t4*t3;      
		double t19 = 1/t18;      
		double t20 = t17*t19;      
		double t21 = t11*z1d;      
		double t25 = z1d*t5*t11;      
		double t27 = z2dd*t5*t11;      
		double t28 = t11*z2d;      
		double t32 = z2d*t5*t11;      
		double t35 = 1/t10/z3;      
		double t42 = z1d*z2dd-z2d*z1dd;      
		double t43 = p1*t42;      
		double t44 = t3*t3;      
		double t46 = 1/t4/t44;      
		double t51 = p1*p1;      
		double t52 = t42*t42;      
		double t53 = t52*t51;      
		double t54 = t44*t3;      
		double t58 = 1/(t53/t54+1.0);      
		double t60 = p1*z2d;      
		double t61 = t19*t58;      
		double t70 = p1*z1d;      
		double t72 = t54+t53;      
		double t73 = 1/t72;      
		double t74 = 3.0*t42*t17;      
		double t78 = -z2d*z1ddd+z1d*z2ddd;      
		double t80 = t74*t5-t4*t78;      
		double t81 = t73*t80;      
		double t82 = t81*t6;      
		double t85 = p1*t3;      
		double t86 = t72*t72;      
		double t87 = 1/t86;      
		double t88 = t85*t87;      
		double t89 = t80*t6;      
		double t92 = t51*t42;      
		double t105 = t5*t78;      
		double t115 = t51*p1*t3*t87;      
		double t130 = p1*t18;      
		double t176 = t42*t19;      

		DCnlt[0][1] = t7*z1d;      
		DCnlt[0][5] = t7*z2d;      
		DCnlt[0][8] = -t4*t11;      
		DCnlt[1][1] = t14-t20*t21;      
		DCnlt[1][2] = t25;      
		DCnlt[1][5] = t27-t20*t28;      
		DCnlt[1][6] = t32;      
		DCnlt[1][8] = -2.0*t17*t5*t35;      
		DCnlt[2][1] = (p1*z2dd*t19-3.0*t43*t46*z1d)*t58;      
		DCnlt[2][2] = -t60*t61;      
		DCnlt[2][5] = (-p1*z1dd*t19-3.0*t43*t46*z2d)*t58;      
		DCnlt[2][6] = t70*t61;      
		DCnlt[3][1] = 2.0*t70*t82-t88*t89*(6.0*t44*z1d+2.0*t92*z2dd)+t85*t73*(3.0*z2dd*t17*t5+3.0*t42*z1dd*t5-t74*t19*z1d-t105*z1d-t4*z2ddd)*t6;      
		DCnlt[3][2] = 2.0*t115*t89*t42*z2d+t85*t73*(-3.0*z2d*t17*t5+3.0*t42*z1d*t5)*t6;      
		DCnlt[3][3] = t130*t73*z2d*t6;      
		DCnlt[3][5] = 2.0*t60*t82-t88*t89*(6.0*t44*z2d-2.0*t92*z1dd)+t85*t73*(-3.0*z1dd*t17*t5+3.0*t42*z2dd*t5-t74*t19*z2d-t105*z2d+t4*z1ddd)*t6;      
		DCnlt[3][6] = -2.0*t115*t89*t42*z1d+t85*t73*(3.0*z1d*t17*t5+3.0*t42*z2d*t5)*t6;      
		DCnlt[3][7] = -t130*t73*z1d*t6;      
		DCnlt[3][8] = -t85*t81*t11;      
		DCnlt[4][1] = t27-t176*t21;      
		DCnlt[4][2] = -t32;      
		DCnlt[4][5] = -t14-t176*t28;      
		DCnlt[4][6] = t25;      
		DCnlt[4][8] = -2.0*t42*t5*t35;      
	}

	if(*mode == 99)
	{
		DCnlt[0][1] = 1.0; 
		DCnlt[0][5] = 1.0; 
		DCnlt[0][8] = 1.0; 
		DCnlt[1][1] = 1.0; 
		DCnlt[1][2] = 1.0; 
		DCnlt[1][5] = 1.0; 
		DCnlt[1][6] = 1.0; 
		DCnlt[1][8] = 1.0; 
		DCnlt[2][1] = 1.0; 
		DCnlt[2][2] = 1.0; 
		DCnlt[2][5] = 1.0; 
		DCnlt[2][6] = 1.0; 
		DCnlt[3][1] = 1.0; 
		DCnlt[3][2] = 1.0; 
		DCnlt[3][3] = 1.0; 
		DCnlt[3][5] = 1.0; 
		DCnlt[3][6] = 1.0; 
		DCnlt[3][7] = 1.0; 
		DCnlt[3][8] = 1.0; 
		DCnlt[4][1] = 1.0; 
		DCnlt[4][2] = 1.0; 
		DCnlt[4][5] = 1.0; 
		DCnlt[4][6] = 1.0; 
		DCnlt[4][8] = 1.0; 
	}
}

void CAliceFlat::FinalNLConstraintFunction(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofVariables, const double* const z, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf)
{
	if(*mode == 0 || *mode == 2)
	{
		Cnlf[0] = atan2(z1d,z2d);
	}

	if(*mode == 1 || *mode == 2)
	{
		double t2 = z1d*z1d;
		double t3 = z2d*z2d;      
		double t4 = 1/t3;      
		double t7 = 1/(t2*t4+1.0);      

		DCnlf[0][1] = 1/z2d*t7;      
		DCnlf[0][5] = -z1d*t4*t7;      
	}

	if(*mode == 99)
	{
		DCnlf[0][1] = 1.0; 
		DCnlf[0][5] = 1.0; 
	}
}

void CAliceFlat::getLUBounds(const ConstraintType* const constraintType, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, double* const LUBounds)
{
	if(*constraintType == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_li[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_ui[m_j];
			}
		}
	}

	if(*constraintType == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_lt[*i][m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_ut[*i][m_j];
			}
		}
	}

	if(*constraintType == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_lf[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_uf[m_j];
			}
		}
	}

	if(*constraintType == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Li[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Ui[m_j];
			}
		}
	}

	if(*constraintType == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Lt[*i][m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Ut[*i][m_j];
			}
		}
	}

	if(*constraintType == ct_NLinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Lf[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				LUBounds[m_j] = m_Uf[m_j];
			}
		}
	}
}

int CAliceFlat::setTimeInterval(const TimeInterval* const timeInterval)
{
	m_TimeInterval.t0 = timeInterval->t0;
	m_TimeInterval.tf = timeInterval->tf;
	return 0;
}

int CAliceFlat::setStates(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const NofStates, const double* const States)
{
	if(*constraintType == ct_LinInitial)
	{
        if(*boundsType == ubt_LowerBound)
        {
           m_li[0] = States[0];  /* Easting_i */
           m_li[1] = States[1];  /* Northing_i */
        }
        else
        {
          m_ui[0] = States[0];   /* Easting_i  */
          m_ui[1] = States[1];   /* Northing_i */
        }
	}

	if(*constraintType == ct_LinTrajectory)
	{

	}

	if(*constraintType == ct_LinFinal)
	{
        if(*boundsType == ubt_LowerBound)
        {
           m_lf[0] = States[0];  /* Easting_f */
           m_lf[1] = States[1];  /* Northing_f */
        }
        else
        {
          m_uf[0] = States[0];   /* Easting_f */
          m_uf[1] = States[1];   /* Northing_f */
        }
	}

	if(*constraintType == ct_NLinInitial)
	{
        if(*boundsType == ubt_LowerBound)
        {
            m_Li[0] = States[3];               /* Heading_i */
        }
        else
        {
            m_Ui[0] = States[3];               /* Heading_i */
        }
	}

	if(*constraintType == ct_NLinTrajectory)
	{

	}

	if(*constraintType == ct_NLinFinal)
	{
        if(*boundsType == ubt_LowerBound)
        {
            m_Lf[0] = States[3];               /* Heading_f */
        }
        else
        {
            m_Uf[0] = States[3];               /* Heading_f */
        }
    }

	return 0;
}

int CAliceFlat::setInputs(const ConstraintType* const constraintType, const BoundsType* const boundsType, const int* const NofInputs, const double* const Inputs)
{
	if(*constraintType == ct_LinInitial)
	{

	}

	if(*constraintType == ct_LinTrajectory)
	{

	}

	if(*constraintType == ct_LinFinal)
	{

	}

	if(*constraintType == ct_NLinInitial)
	{

	}

	if(*constraintType == ct_NLinTrajectory)
	{

	}

	if(*constraintType == ct_NLinFinal)
	{

	}

	return 0;
}

int CAliceFlat::setOCPParameters(const int* const i, const double* const t, const FunctionType* const functionType, const int* const NofParameters, const double* const Parameters)
{

	for(m_i=0; m_i<*NofParameters; m_i++)
	{
		m_Parameters[m_i] = Parameters[m_i];
	}

	return 0;
}

int CAliceFlat::setOCPReferences(const int* const i, const double* const t, const int* const NofReferences, const double* const References)
{

	return 0;
}

int CAliceFlat::setLinearConstraints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, const double** const A)
{
	if(*constraintType == ct_LinInitial)
	{
		for(m_i=0; m_i<*NofLConstraints; m_i++)
		{
			for(m_j=0; m_j<*NofVariables; m_j++)
			{
				m_Ai[m_i][m_j] = A[m_i][m_j];
			}
		}
	}
	else if(*constraintType == ct_LinTrajectory)
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				m_At[*i][m_j][m_k] = A[m_j][m_k];
			}
		}
	}
	else /*if(*constraintType == ct_LinFinal)*/
	{
		for(m_j=0; m_j<*NofLConstraints; m_j++)
		{
			for(m_k=0; m_k<*NofVariables; m_k++)
			{
				m_Af[m_j][m_k] = A[m_j][m_k];
			}
		}
	}
	return 0;
}

int CAliceFlat::setLUBounds(const ConstraintType* const constraintType, const BoundsType* const ubtype, const int* const i, const double* const t, const int* const NofConstraints, const double* const LUBounds)
{
	if(*constraintType == ct_LinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_li[m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_ui[m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_LinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_lt[*i][m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_ut[*i][m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_LinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_lf[m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_uf[m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_NLinInitial)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Li[m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Ui[m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_NLinTrajectory)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Lt[*i][m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Ut[*i][m_j] = LUBounds[m_j];
			}
		}
	}

	if(*constraintType == ct_NLinFinal)
	{
		if(*ubtype == ubt_LowerBound)
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Lf[m_j] = LUBounds[m_j];
			}
		}
		else
		{
			for(m_j=0; m_j<(*NofConstraints); m_j++)
			{
				m_Uf[m_j] = LUBounds[m_j];
			}
		}
	}

	return 0;
}

int CAliceFlat::setNURBSData(const NURBSDataType* const dataType, const int* const NofFlatOutputs, const int* const Data)
{
    if(*dataType == dt_NofPolynomials)
    {
        for(m_i=0; m_i<*NofFlatOutputs; m_i++)
        {
            m_NofPolynomials[m_i] = Data[m_i];
        }
        return 0;
    }
    else
    {
        return -1;
    }
}

void CAliceFlat::getWeightSizes(WeightSizes* const weightSizes)
{

}

void CAliceFlat::getLinearConstraintsOnWeights(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{

}

void CAliceFlat::InitialNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli,const int* const Offset)
{

}

void CAliceFlat::TrajectoryNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{

}

void CAliceFlat::FinalNLConstraintFunctionOnWeights(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofWeights, const double* const Weights, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{

}

void CAliceFlat::getControlPointSizes(ControlPointSizes* const controlPointSizes)
{

}

void CAliceFlat::getLinearConstraintsOnControlPoints(const ConstraintType* const constraintType, const int* const i, const double* const t, const int* const NofParameters, const double* const Parameters, const int* const NofLConstraints, const int* const NofVariables, double** const A, const int* const Offset)
{

}

void CAliceFlat::InitialNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnli, double** const DCnli, const int* const Offset)
{

}

void CAliceFlat::TrajectoryNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlt, double** const DCnlt, const int* const Offset)
{

}

void CAliceFlat::FinalNLConstraintFunctionOnControlPoints(int* const mode, const int* const state, const int* const i, const double* const t, const int* const NofControlPoints, const double* const ControlPoints, const int* const NofParameters, const double* const Parameters, const int* const NofConstraints, double* const Cnlf, double** const DCnlf, const int* const Offset)
{

}

