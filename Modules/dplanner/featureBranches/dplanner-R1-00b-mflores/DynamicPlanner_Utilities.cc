/* 
	DynamicPlanner_Utilities.cc
	DynamicPlanner
	
	Melvin E. Flores
	Control and Dynamical Systems
	California Institute of Technology
	Created on 12-14-2006.
    Latest Modified on 3-20-2007.
*/ 

#define _USE_MATH_DEFINES

#include <math.h>
#include <iostream>
#include <iomanip>
#include <time.h>

#include "DynamicPlanner_Utilities.hh"
#include "AlgGeom/AlgebraicGeometry.hh"

using namespace std;

#ifdef InAlice
    int ConstructFeasibleRegion(const int* const NofWaypoints, 
                                RDDFVector *rddfvector, 
                                double** const DataPoints, 
                                double*  const Headings,
                                const int* const NofConvexSets,
                                CPolytope* const ConvexSets)
    {
        int i   = 0;
        int j   = 0;
        int k   = 0;
        int ell = 0;
        int s   = 0;    
        
        ALGErrorType ErrorFlag = alg_NoError;
        
        int NofSides      = 6; /* How many points should be place in a circle */
        int NofDimensions = 2;

        double slope  = 0.0;
        double theta  = 0.0;
        double offset = 0.0;
        double th     = 0.0; 
           
        double delta_th      = 2.0*M_PI/(double)NofSides;
        double HalfNofPoints = (NofSides/2)+1;    
        
        int start_waypoint = 0;
        int stop_waypoint  = 0;
        
        double Easting_start  = 0.0;
        double Northing_start = 0.0;
        double Radius_start   = 0.0;

        double Easting_stop  = 0.0;
        double Northing_stop = 0.0;
           
        int* NofPoints = new int[*NofConvexSets];
        for(i = 0; i < *NofConvexSets; i++){NofPoints[i] = NofSides + 2;}    
        
        double*** Points = new double**[*NofConvexSets];
        for(i = 0; i < *NofConvexSets; i++)
        {
            Points[i] = new double*[NofDimensions];
            for(j = 0; j < NofDimensions; j++){Points[i][j] = new double[NofPoints[i]];}
        }
         
        CAlgebraicGeometry* AlgGeom = new CAlgebraicGeometry(); 
        
        for(ell=0; ell<*NofConvexSets; ell++)
        {
            start_waypoint = ell;
            stop_waypoint  = ell + 1;
            
            Easting_start  = (*rddfvector)[start_waypoint].Easting;
            Northing_start = (*rddfvector)[start_waypoint].Northing;
            Radius_start   = (*rddfvector)[start_waypoint].radius;
            
            Easting_stop  = (*rddfvector)[stop_waypoint].Easting;
            Northing_stop = (*rddfvector)[stop_waypoint].Northing;
            
            slope = (Northing_stop - Northing_start)/(Easting_stop - Easting_start);
            theta = atan2(Northing_stop - Northing_start, Easting_stop - Easting_start);

            k = 0;
            offset = M_PI_2 + theta;
            for(s=0; s<HalfNofPoints; s++)
            {
                th = offset + s*delta_th;
                Points[ell][0][k] = Easting_start + Radius_start*cos(th);
                k = k + 1;
            }
            
            offset = -M_PI_2 + theta;
            for(s=0; s<HalfNofPoints; s++)
            {
                th = offset + s*delta_th;
                Points[ell][0][k] = Easting_stop + Radius_start*cos(th);
                k = k + 1;
            }
            
            k = 0;
            offset = M_PI_2 + theta;
            for(s=0; s<HalfNofPoints; s++)
            {
                th = offset + s*delta_th;
                Points[ell][1][k] = Northing_start + Radius_start*sin(th);
                k = k + 1;
            }
            
            offset = -M_PI_2 + theta;
            for(s=0; s<HalfNofPoints; s++)
            {
                th = offset + s*delta_th;
                Points[ell][1][k] = Northing_stop + Radius_start*sin(th);
                k = k + 1;
            }     
            
            /* Construct the polytopes */			
            ConvexSets[ell] = new CPolytope(&NofDimensions, &NofPoints[ell], (const double** const) Points[ell]);
            ErrorFlag = AlgGeom->FacetEnumeration(&ConvexSets[ell]);
            if(ErrorFlag == alg_NoError)
            {
                ErrorFlag =AlgGeom->VertexEnumeration(&ConvexSets[ell]);
                if(ErrorFlag == alg_NoError)
                {
                    AlgGeom->ComputeVolume(&ConvexSets[ell]);
                    AlgGeom->ComputeCentroid(&ConvexSets[ell]);
                    AlgGeom->ComputeBoundingBox(&ConvexSets[ell]); 
                }
            }
            Headings[ell] = atan2(Easting_stop - Easting_start, Northing_stop - Northing_start);            
       }
       
        for(ell=0; ell<*NofWaypoints; ell++)
        {
            DataPoints[0][ell] = (*rddfvector)[ell].Easting;
            DataPoints[1][ell] = (*rddfvector)[ell].Northing;    
        }
        
       /* Restore Resources */
       delete AlgGeom;
        
       for(i = 0; i < *NofConvexSets; i++)
       {
           for(j = 0; j < NofDimensions; j++){delete[] Points[i][j];}
           delete[] Points[i];
       }
       delete[] Points;
       delete[] NofPoints;
        
       return 0;
    }

#endif

int ConstructFeasibleRegion(const int* const NofWayPoints, 
                            const int* const RDDFDataSize,
                            const double** const RDDFData, 
                            double** const DataPoints,
                            double*  const Headings,
                            const int* const NofConvexSets,
                            CPolytope* const ConvexSets)
{
	int i   = 0;
    int j   = 0;
	int k   = 0;
	int ell = 0;
    int s   = 0;    
	
    /*
    for(i=0; i<*NofWayPoints; i++)
    {
        for(j=0; j<*RDDFDataSize; j++)
        {
            cout << RDDFData[i][j] << '\t'; 
        }
        cout << endl;
    }
    cout << endl;
    */
    
    ALGErrorType ErrorFlag = alg_NoError;
    
	int NofSides      = 6; /* How many points should be place in a circle */
	int NofDimensions = 2;

	double slope  = 0.0;
	double theta  = 0.0;
	double offset = 0.0;
    double th     = 0.0; 
       
    double delta_th      = 2.0*M_PI/(double)NofSides;
    double HalfNofPoints = (NofSides/2)+1;    
    
    int start_waypoint = 0;
    int stop_waypoint  = 0;
    
 	double Easting_start  = 0.0;
	double Northing_start = 0.0;
	double Radius_start   = 0.0;

	double Easting_stop  = 0.0;
	double Northing_stop = 0.0;
   
    int* NofPoints = new int[*NofConvexSets];
    for(i = 0; i < *NofConvexSets; i++){NofPoints[i] = NofSides + 2;}    
    
    double*** Points = new double**[*NofConvexSets];
    for(i = 0; i < *NofConvexSets; i++)
    {
        Points[i] = new double*[NofDimensions];
        for(j = 0; j < NofDimensions; j++){Points[i][j] = new double[NofPoints[i]];}
    }
     
    CAlgebraicGeometry* AlgGeom = new CAlgebraicGeometry();    
    
    for(ell=0; ell<*NofConvexSets; ell++)
    {
        start_waypoint = ell;
        stop_waypoint  = ell + 1;
        
        Easting_start  = RDDFData[start_waypoint][1];
        Northing_start = RDDFData[start_waypoint][2];
        Radius_start   = RDDFData[start_waypoint][4];

        Easting_stop  = RDDFData[stop_waypoint][1]; 
        Northing_stop = RDDFData[stop_waypoint][2];
      
        slope = (Northing_stop - Northing_start)/(Easting_stop - Easting_start);
        theta = atan2(Northing_stop - Northing_start, Easting_stop - Easting_start);
        
        k = 0;
        offset = M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][0][k] = Easting_start + Radius_start*cos(th);
            k = k + 1;
        }
        
        offset = -M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][0][k] = Easting_stop + Radius_start*cos(th);
            k = k + 1;
        }
        
        k = 0;
        offset = M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][1][k] = Northing_start + Radius_start*sin(th);
            k = k + 1;
        }
        
        offset = -M_PI_2 + theta;
        for(s=0; s<HalfNofPoints; s++)
        {
            th = offset + s*delta_th;
            Points[ell][1][k] = Northing_stop + Radius_start*sin(th);
            k = k + 1;
        }     
		
	    /* Construct the polytopes */			
        ConvexSets[ell] = new CPolytope(&NofDimensions, &NofPoints[ell], (const double** const) Points[ell]);
        ErrorFlag = AlgGeom->FacetEnumeration(&ConvexSets[ell]);
        if(ErrorFlag == alg_NoError)
        {
            ErrorFlag =AlgGeom->VertexEnumeration(&ConvexSets[ell]);
            if(ErrorFlag == alg_NoError)
            {
                AlgGeom->ComputeVolume(&ConvexSets[ell]);
                AlgGeom->ComputeCentroid(&ConvexSets[ell]);
                AlgGeom->ComputeBoundingBox(&ConvexSets[ell]); 
            }
        }
        Headings[ell] = atan2(Easting_stop - Easting_start, Northing_stop - Northing_start);
    }

    for(ell=0; ell<*NofWayPoints; ell++)
    {
        DataPoints[0][ell] = RDDFData[ell][1];   
        DataPoints[1][ell] = RDDFData[ell][2];           
    }

    /* Restore Resources */        
    delete AlgGeom;

    for(i = 0; i < *NofConvexSets; i++)
    {
        for(j = 0; j < NofDimensions; j++){delete[] Points[i][j];}
        delete[] Points[i];
    }
    delete[] Points;
    delete[] NofPoints;
    
   	return 0;
}

