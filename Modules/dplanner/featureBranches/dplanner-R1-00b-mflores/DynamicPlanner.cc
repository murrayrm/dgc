/*
 *  DynamicPlanner.cc
 *  DynamicPlanner
 *
 *  Melvin E. Flores
 *  Control and Dynamical Systems
 *  California Institute of Technology
 *  May 30, 2007.
 *
 */

#include "DynamicPlanner.hh"

#ifdef InAlice
    CDynamicPlanner::CDynamicPlanner(int SkynetKey, bool WAIT_STATE):CSkynetContainer(MODdynamicplanner,SkynetKey),CStateClient(WAIT_STATE)
#else
    CDynamicPlanner::CDynamicPlanner(void)
#endif
{
    m_DebugMode                 = true;
    m_VerboseMode               = true;
    m_ChangePolytopeCoordinates = true;
    m_UseVolumeGuess            = true;
    m_ReusePreviousSolution     = true;
    m_FirstProblem              = true;
    m_StoreData                 = true;

    alg_ErrorFlag = alg_NoError;

    /* BSpline Surface Map */
    m_NofDimensions = 2;
    
    m_PositionIntervals = new PositionInterval[m_NofDimensions];
    m_PositionIntervals[0].p0 = -200.0;
    m_PositionIntervals[0].pf =  200.0;
    
    m_PositionIntervals[1].p0 = -200.0;
    m_PositionIntervals[1].pf =  200.0;
    
    m_NofMetersPerPolynomial     = 2.0;
    m_SurfaceDegreeOfPolynomials = new int[m_NofDimensions];
    m_SurfaceCurveSmoothness     = new int[m_NofDimensions];
    m_SurfaceMaxDerivatives      = new int[m_NofDimensions];
    
    m_SurfaceDegreeOfPolynomials[0] = 5;
    m_SurfaceDegreeOfPolynomials[1] = 5;
    
    m_SurfaceCurveSmoothness[0] = 3;
    m_SurfaceCurveSmoothness[1] = 3;
    
    m_SurfaceMaxDerivatives[0] = 1;
    m_SurfaceMaxDerivatives[1] = 1;

    m_BaseCost = 200.0;
    m_CostComponent = new CCostComponent(&m_NofDimensions, m_PositionIntervals, &m_NofMetersPerPolynomial, m_SurfaceDegreeOfPolynomials, m_SurfaceCurveSmoothness, m_SurfaceMaxDerivatives, &m_BaseCost);    

    m_PolytopeCost = 5.0;

    #ifdef InAlice  
        m_SendSocket = m_skynet.get_send_sock(SNtraj);
    #endif
    
    /* Create OCPComponents */
    m_OCPComponents  = new COCPComponents();
        
    /* add the BSpline Surface Map to the component */
    m_OCPComponents->addOCPComponent((IOCPComponent*)m_CostComponent);      
          
    /* Create an instance of the Alice Flat Problem */      
    m_AliceFlat = new CAliceFlat();
    
     /* create the Reconfigurable OCProblem */
    m_ReconfigOCProblem = new CReconfigOCProblem(m_AliceFlat,m_OCPComponents);

    /* Get information about the parameterization of the OCProblem */
    m_NofFlatOutputs = m_ReconfigOCProblem->getNofFlatOutputs();
    
    m_NofDimensionsOfFlatOutputs = new int[m_NofFlatOutputs];
    m_NofPolynomials             = new int[m_NofFlatOutputs];
    m_DegreeOfPolynomials        = new int[m_NofFlatOutputs];
    m_OrderOfPolynomials         = new int[m_NofFlatOutputs];    
    m_CurveSmoothness            = new int[m_NofFlatOutputs];    
    m_NofDerivatives             = new int[m_NofFlatOutputs];    
    m_NofWeights                 = new int[m_NofFlatOutputs];
    m_NofControlPoints           = new int[m_NofFlatOutputs];

    m_NURBSDataX = nd_NofDimensionsOfFlatOutputs;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofDimensionsOfFlatOutputs);

    m_NURBSDataX = nd_NofPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofPolynomials);

    m_NURBSDataX = nd_DegreeOfPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_DegreeOfPolynomials);
 
    m_NURBSDataX = nd_OrderOfPolynomials;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_OrderOfPolynomials);
   
    m_NURBSDataX = nd_CurveSmoothness;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_CurveSmoothness);

    m_NURBSDataX = nd_NofDerivatives;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofDerivatives);
    
    m_NURBSDataX = nd_NofControlPoints;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofControlPoints);
    
    m_NURBSDataX = nd_NofWeights;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofWeights);
        
	m_Weights                    = new double* [m_NofFlatOutputs];
	m_ControlPoints              = new double**[m_NofFlatOutputs];
	m_LowerBoundsOnControlPoints = new double**[m_NofFlatOutputs];
	m_UpperBoundsOnControlPoints = new double**[m_NofFlatOutputs];    
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		for(m_j=0; m_j<m_NofWeights[m_i]; m_j++){m_Weights[m_i][m_j] = 1.0;}

		m_ControlPoints[m_i]              = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
        m_LowerBoundsOnControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];
        m_UpperBoundsOnControlPoints[m_i] = new double*[m_NofDimensionsOfFlatOutputs[m_i]];        
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
            m_ControlPoints[m_i][m_j]              = new double[m_NofControlPoints[m_i]];
            m_LowerBoundsOnControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            m_UpperBoundsOnControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            
            for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++)
            {
                m_ControlPoints[m_i][m_j][m_k] = 0.0;
                m_LowerBoundsOnControlPoints[m_i][m_j][m_k] = -DBL_MAX;
                m_UpperBoundsOnControlPoints[m_i][m_j][m_k] =  DBL_MAX;
            }
		}
	}

    m_NofStates     = m_ReconfigOCProblem->getNofStates();
    m_NofInputs     = m_ReconfigOCProblem->getNofInputs();
    m_NofParameters = m_ReconfigOCProblem->getNofParameters();
    m_NofAllParams  = 16;

    m_InitialConditionsLB = new double[m_NofStates + m_NofInputs];
    m_InitialConditionsUB = new double[m_NofStates + m_NofInputs];
    m_FinalConditionsLB   = new double[m_NofStates + m_NofInputs];
    m_FinalConditionsUB   = new double[m_NofStates + m_NofInputs];

    for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++)
    {
        m_InitialConditionsLB[m_i] = -DBL_MAX;
        m_InitialConditionsUB[m_i] =  DBL_MAX;

        m_FinalConditionsLB[m_i] = -DBL_MAX;
        m_FinalConditionsUB[m_i] =  DBL_MAX;
    }
    
    m_Parameters = new double[m_NofParameters];
    for(m_i=0; m_i<m_NofParameters; m_i++){m_Parameters[m_i] = 0.0;}    

    m_InitialStates = new double[m_NofStates];
    m_InitialInputs = new double[m_NofInputs];

    m_FinalStates = new double[m_NofStates];
    m_FinalInputs = new double[m_NofInputs];
    
    m_AllParams = new double[m_NofAllParams];    
    for(m_i=0; m_i<m_NofAllParams; m_i++){m_AllParams[m_i] = 0.0;}
    
    m_RDDFFileName = NULL;
    m_RDDFData     = NULL;
    
    m_DataPoints   = NULL;
    m_Headings     = NULL;
    m_MapPolytopes = NULL;
    
    m_InitialPoint = new double[m_NofDimensionsOfFlatOutputs[0]];
    m_FinalPoint   = new double[m_NofDimensionsOfFlatOutputs[0]];
    
    m_mins = new double[m_NofDimensions];
    m_maxs = new double[m_NofDimensions];

    m_NofPoints = 4;
    m_Points    = new double*[m_NofDimensions];
    for(m_j=0; m_j<m_NofDimensions; m_j++){m_Points[m_j] = new double[m_NofPoints];}

    m_AlgGeom = new CAlgebraicGeometry();
        
    m_ReconfigOCProblem->getOCPSizes(&m_ocpSizes);
    
    m_NofILinConstraints  = m_ocpSizes.NofILinConstraints;
    m_NofTLinConstraints  = m_ocpSizes.NofTLinConstraints;
    m_NofFLinConstraints  = m_ocpSizes.NofFLinConstraints;
    m_NofINLinConstraints = m_ocpSizes.NofINLinConstraints;
    m_NofTNLinConstraints = m_ocpSizes.NofTNLinConstraints;
    m_NofFNLinConstraints = m_ocpSizes.NofFNLinConstraints;            

    m_NofCostCollocPoints       = m_ocpSizes.NofCostCollocPoints;
    m_NofConstraintCollocPoints = m_ocpSizes.NofConstraintCollocPoints;
    
    m_li = NULL; m_ui = NULL;
    if(m_NofILinConstraints != 0)
    {
        m_li = new double[m_NofILinConstraints];
        m_ui = new double[m_NofILinConstraints];
    }
    
    m_lt = NULL; m_ut = NULL;
    if(m_NofTLinConstraints != 0)
    {
        m_lt = new double*[m_NofConstraintCollocPoints];
        m_ut = new double*[m_NofConstraintCollocPoints];
        for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
        {
            m_lt[m_i] = new double[m_NofTLinConstraints];
            m_ut[m_i] = new double[m_NofTLinConstraints];
        }
    }

    m_lf = NULL; m_uf = NULL;
    if(m_NofFLinConstraints != 0)
    {
        m_lf = new double[m_NofFLinConstraints];
        m_uf = new double[m_NofFLinConstraints];
    }
    
    m_Li = NULL; m_Ui = NULL;
    if(m_NofINLinConstraints != 0)
    {
        m_Li = new double[m_NofINLinConstraints];
        m_Ui = new double[m_NofINLinConstraints];
    }

    m_Lt = NULL; m_Ut = NULL;
    if(m_NofTNLinConstraints != 0)
    {
        m_Lt = new double*[m_NofConstraintCollocPoints];
        m_Ut = new double*[m_NofConstraintCollocPoints];
        for(m_i=0; m_i<m_NofConstraintCollocPoints; m_i++)
        {
            m_Lt[m_i] = new double[m_NofTNLinConstraints];
            m_Ut[m_i] = new double[m_NofTNLinConstraints];
        }
    }
    
    m_Lf = NULL; m_Uf = NULL;
    if(m_NofFNLinConstraints != 0)
    {
        m_Lf = new double[m_NofFNLinConstraints];
        m_Uf = new double[m_NofFNLinConstraints];
    }
    
    m_NofDerivativesOfStates = new int[m_NofStates];
    m_NofDerivativesOfInputs = new int[m_NofInputs];

    m_ReconfigOCProblem->getSystemsInfo(&m_NofStates, m_NofDerivativesOfStates, &m_NofInputs, m_NofDerivativesOfInputs);
 
    m_NofCollocPointsToRecover = 100;
    m_CollocPointsToRecover    = new double[m_NofCollocPointsToRecover];
    m_ReconfigOCProblem->getTimeInterval(&m_timeInterval);
    CreateTimeVector(&m_timeInterval, &m_NofCollocPointsToRecover, m_CollocPointsToRecover);
    
	m_Time   = new double  [m_NofCollocPointsToRecover];
	m_States = new double**[m_NofCollocPointsToRecover];
	m_Inputs = new double**[m_NofCollocPointsToRecover];
	for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
	{
		m_States[m_i] = new double*[m_NofStates];
		for(m_j=0; m_j<m_NofStates; m_j++)
		{
			m_States[m_i][m_j] = new double[m_NofDerivativesOfStates[m_j]+1];
            for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]+1; m_k++){m_States[m_i][m_j][m_k] = 0.0;}
		}
		
		m_Inputs[m_i] = new double*[m_NofInputs];
		for(m_j=0; m_j<m_NofInputs; m_j++)
		{
			m_Inputs[m_i][m_j] = new double[m_NofDerivativesOfInputs[m_j]+1];
            for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]+1; m_k++){m_Inputs[m_i][m_j][m_k] = 0.0;}
		}
	}	
    
    #ifdef InAlice
        /* Create an RDDF object */
        m_RDDF = new RDDF();
        if(m_RDDF != NULL){cerr << "RDDF object was created successfully" << endl;}
        
        /* Create a Traj object (N,Nd,Ndd,E,Ed,Edd); Northing then Easting */
        m_Traj = new CTraj(3);
        if(m_Traj != NULL){cerr << "Traj object was created successfully" << endl;}
        
        m_Verbose         = 0;
        m_useAstate       = false;
        m_useCMap         = false;
        m_useObstacles    = false;
        m_useParameters   = true;
        m_useRDDF         = true;
        m_usePolyCorridor = false;
        
        m_ocptSpecs = new OCPtSpecs(SkynetKey, m_useAstate, m_Verbose , m_useCMap,   m_useObstacles, m_useParameters, m_useRDDF, m_usePolyCorridor);
        if(m_ocptSpecs != NULL){cerr << "OCPtSpecs object was created successfully" << endl;}        
        usleep(100000);
    #endif
    
    if(m_StoreData == true)
    {
       m_TrajCounter = 0; 
       m_MaxNofTrajs = 100; 
       m_TrajData = new double**[m_MaxNofTrajs];
       for(m_i=0; m_i< m_MaxNofTrajs; m_i++)
       {
           m_TrajData[m_i] = new double*[m_NofCollocPointsToRecover];
           for(m_j=0; m_j<m_NofCollocPointsToRecover; m_j++)
           {
               m_TrajData[m_i][m_j] = new double[m_NofDerivativesOfStates[0] + m_NofDerivativesOfStates[1] + 1];
           } 
       }
       m_TrajNofPolytopes          = new int[m_MaxNofTrajs];
       m_TrajPolytopes             = new CPolytope*[m_MaxNofTrajs];
       m_TrajModifiedConvexSets    = new CPolytope*[m_MaxNofTrajs];
       m_TrajOverlappingConvexSets = new CPolytope*[m_MaxNofTrajs];     
    }    
}

int CDynamicPlanner::storeOptimalTrajectory(void)
{
    if(m_TrajCounter < m_MaxNofTrajs)
    {
       for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
       {
           m_TrajData[m_TrajCounter][m_i][0] = m_Time[m_i];
           m_TrajData[m_TrajCounter][m_i][1] = m_States[m_i][0][0];
           m_TrajData[m_TrajCounter][m_i][2] = m_States[m_i][0][1];
           m_TrajData[m_TrajCounter][m_i][3] = m_States[m_i][0][2];
           m_TrajData[m_TrajCounter][m_i][4] = m_States[m_i][1][0];
           m_TrajData[m_TrajCounter][m_i][5] = m_States[m_i][1][1];
           m_TrajData[m_TrajCounter][m_i][6] = m_States[m_i][1][2];         
       }

       //m_TrajConvexSets[m_TrajCounter] = new CPolytope[m_TrajNofConvexSets] 
       
       if(m_UseVolumeGuess == true)
       {
          m_TrajNofPolytopes[m_TrajCounter] = m_NLPProblem->getNofPolytopes();
         
          m_TrajPolytopes[m_TrajCounter]             = new CPolytope[m_TrajNofPolytopes[m_TrajCounter]];
          m_TrajModifiedConvexSets[m_TrajCounter]    = new CPolytope[m_TrajNofPolytopes[m_TrajCounter]];
          m_TrajOverlappingConvexSets[m_TrajCounter] = new CPolytope[m_TrajNofPolytopes[m_TrajCounter]-1];
          
          m_ErrorFlag = m_NLPProblem->getPolytopes(&m_TrajNofPolytopes[m_TrajCounter], m_TrajPolytopes[m_TrajCounter]);
          for(m_i=0; m_i<m_TrajNofPolytopes[m_TrajCounter]; m_i++)
          {
              cout << &m_TrajPolytopes[m_TrajCounter][m_i] << endl;
          }
          
          m_ErrorFlag = m_NLPProblem->getModifiedConvexSets(&m_TrajNofPolytopes[m_TrajCounter], m_TrajModifiedConvexSets[m_TrajCounter]);
          for(m_i=0; m_i<m_TrajNofPolytopes[m_TrajCounter]; m_i++)
          {
              cout << &m_TrajModifiedConvexSets[m_TrajCounter][m_i] << endl;
          }
          
          int NofOverlappingConvexSets = m_TrajNofPolytopes[m_TrajCounter]-1;
          m_ErrorFlag = m_NLPProblem->getOverlappingConvexSets(&NofOverlappingConvexSets, m_TrajOverlappingConvexSets[m_TrajCounter]);
          
          for(m_i=0; m_i<m_TrajNofPolytopes[m_TrajCounter]-1; m_i++)
          {
              cout << &m_TrajOverlappingConvexSets[m_TrajCounter][m_i] << endl;
          }
          
       }
       else
       {
          m_TrajNofPolytopes[m_TrajCounter] = m_NURBSPoly->getNofPolytopes();
         
          m_TrajPolytopes[m_TrajCounter]             = new CPolytope[m_TrajNofPolytopes[m_TrajCounter]];
          m_TrajModifiedConvexSets[m_TrajCounter]    = new CPolytope[m_TrajNofPolytopes[m_TrajCounter]];
          m_TrajOverlappingConvexSets[m_TrajCounter] = new CPolytope[m_TrajNofPolytopes[m_TrajCounter]-1];

          m_ErrorFlag = m_NURBSPoly->getModifiedConvexSets(&m_TrajNofPolytopes[m_TrajCounter], m_TrajModifiedConvexSets[m_TrajCounter]);
          
          int NofOverlappingConvexSets = m_TrajNofPolytopes[m_TrajCounter]-1;
          m_ErrorFlag = m_NURBSPoly->getOverlappingConvexSets(&NofOverlappingConvexSets, m_TrajOverlappingConvexSets[m_TrajCounter]);
       }  

       m_TrajCounter++;
    }
    return 0;
}

void CDynamicPlanner::setDebugMode(const bool* const DebugMode)
{
    m_DebugMode = *DebugMode;
}

void CDynamicPlanner::setVerboseMode(const bool* const VerboseMode)
{
    m_VerboseMode = *VerboseMode;
}
  
int CDynamicPlanner::SolveOCProblem(void)
{
    if(m_VerboseMode == true){cerr << "Setting up optimal control problem solver ... " << endl;}      
    m_OCPSolver = new COCPSolver(m_ReconfigOCProblem);
    if(m_OCPSolver == NULL){return -1;}
    if(m_VerboseMode == true){cerr << "Done!" << endl;}

    #ifdef InAlice
        if(m_VerboseMode == true){cerr << "getting new OCP Specs ... " << endl;}
        m_ErrorFlag = getNewOCPSpecs();
        if(m_ErrorFlag != 0){return -1;}
        if(m_VerboseMode == true){cerr << "Done!" << endl;}

        if(m_useRDDF == true)
        {
            if(m_VerboseMode == true){cerr << "Translating RDDF into overlapping polytopes" << endl;}
            m_ErrorFlag = TranslateRDDFToOverlappingPolytopes(); 
            if(m_ErrorFlag != 0){return -1;}   
            if(m_VerboseMode == true){cerr << "Done!" << endl;}
        }
    #else
        if(m_VerboseMode == true){cerr << "Getting RDDF Data ... " << endl;}
        m_ErrorFlag = getRDDFData();
        if(m_ErrorFlag != 0){return -1;}
        if(m_VerboseMode == true){cerr << "Done!" << endl;}

        if(m_VerboseMode == true){cerr << "Translating RDDF into overlapping polytopes" << endl;}
        m_ErrorFlag = TranslateRDDFToOverlappingPolytopes(); 
        if(m_ErrorFlag != 0){return -1;}   
        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    #endif
        
    if(m_VerboseMode == true){cerr << "Getting new OC Problem..." << endl;}
    m_ErrorFlag = getNewOCProblem();
    if(m_ErrorFlag != 0){return -1;}   
    if(m_VerboseMode == true){cerr << "Done!" << endl;}

    if(m_VerboseMode == true){cerr << "Painting polytopes in cost map..." << endl;}
    for(m_k=0; m_k<m_NofMapPolytopes; m_k++){m_CostComponent->PaintPolytope(&m_MapPolytopes[m_k], &m_PolytopeCost);}
    if(m_VerboseMode == true){cerr << "Done!" << endl;}

    if(m_UseVolumeGuess == true)
    {
        if(m_VerboseMode == true){cerr << "Computing initial volume guess..." << endl;}
        m_ErrorFlag = getInitialVolumeGuess();
        if(m_ErrorFlag != 0){return -1;}   
        if(m_VerboseMode == true){cerr << "Done!" << endl;}    
    }
    else
    {
        if(m_VerboseMode == true){cerr << "Computing initial guess..." << endl;}
        m_ErrorFlag = getInitialGuess();
        if(m_ErrorFlag != 0){return -1;}   
        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }

    if(m_VerboseMode == true){cerr << "Reconfiguring OC Problem..." << endl;}
    m_ErrorFlag = ReconfigureOCProblem();
    if(m_ErrorFlag != 0){return -1;}   
    if(m_VerboseMode == true){cerr << "Done!" << endl;}

    if(m_VerboseMode == true){cerr << "Detecting stop mode..." << endl;}
    DetectStopMode();
    if(m_VerboseMode == true){cerr << "Done!" << endl;}
    
    if(m_StopMode == false)
    {
        m_SolverInform = 0;
        m_OptimalCost  = 0;
        
        m_params_int      = snopt_MajorPrintLevel;
        m_MajorPrintLevel = 10;
        m_ErrorFlag = m_OCPSolver->setParams_int(&m_params_int, &m_MajorPrintLevel);
        
        m_params_int      = snopt_MinorPrintLevel;
        m_MinorPrintLevel = 10;
        m_ErrorFlag = m_OCPSolver->setParams_int(&m_params_int, &m_MinorPrintLevel);
        
        m_params_int      = snopt_DerivativeLevel;
        m_DerivativeLevel = 3;    
        m_ErrorFlag = m_OCPSolver->setParams_int(&m_params_int, &m_DerivativeLevel);
        
        m_params_int       = snopt_IterationsLimit;
        m_MaxNofIterations = 100000;    
        m_ErrorFlag = m_OCPSolver->setParams_int(&m_params_int, &m_MaxNofIterations);
        
        /*    
        m_params_int            = snopt_MajorIterationsLimit;
        m_MajorMaxNofIterations = 900000;
        m_ErrorFlag = m_OCPSolver->setParams_int(&m_params_int, &m_MajorMaxNofIterations);
        
        m_params_int            = snopt_MinorIterationsLimit;
        m_MinorMaxNofIterations = 500000;
        m_ErrorFlag = m_OCPSolver->setParams_int(&m_params_int, &m_MinorMaxNofIterations);
        */
        
        if(m_VerboseMode == true)
        {
           cerr << "----------- NURBSBasedOTG -----------" << endl;
           cerr << "Solving Optimal Control Problem ...  " << endl;
        }
        
        m_InitialTime = clock();
        m_OCPSolver->SolveOCProblem(&m_SolverInform, &m_OptimalCost, m_Weights, m_ControlPoints);
        m_FinalTime   = clock();
       
        cerr << "OCP Solver returned with the following flag: " << m_SolverInform << endl; 
        char* sInform = m_OCPSolver->TranslateSolverInform(&m_SolverInform);
        cout << sInform << endl;
        delete sInform;
        
        m_Duration = (double)(m_FinalTime-m_InitialTime)/CLOCKS_PER_SEC;
        if(m_VerboseMode == true){cerr << "Time NURBSBasedOTG took to solve the OCProblem  = "<< fixed << setprecision(5) << m_Duration << " secs" << endl;}
        cout << "-------------------------------------" << endl;
        cout.flush();  
        
        if(m_SolverInform == 1)
        {
           if(m_VerboseMode == true){cerr << "Computing optimal trajectory ..." << endl;}
           m_ErrorFlag = getOptimalTrajectory(); 
           if(m_DebugMode == true){m_ErrorFlag = saveOptimalTrajectory("Traj.dat");}
           if(m_VerboseMode == true){cerr << "Done!" << endl;}
           
           if(m_DebugMode == true)
           {
              m_ErrorFlag = printOptimalTrajectory();
           }
           
           if(m_StoreData == true)
           {
              m_ErrorFlag = storeOptimalTrajectory();
           }
           
           #ifdef InAlice
                m_ErrorFlag = sendOptimalTrajectory();   
           #endif
        }
        else
        {
            /* Send a failure message to tplanner */
            #ifdef InAlice
                m_ErrorFlag = sendStopTrajectory();    
            #endif            
        }
        if(m_FirstProblem == true){m_FirstProblem == false;}
    }
    else
    {
        #ifdef InAlice
            m_ErrorFlag = sendStopTrajectory();    
        #endif
    }

    if(m_VerboseMode == true){cerr << "Erasing polytopes from cost map..." << endl;}
    for(m_k=0; m_k<m_NofMapPolytopes; m_k++){m_CostComponent->PaintPolytope(&m_ContainersOfMapPolytopes[m_k], &m_BaseCost);}

    if(m_UseVolumeGuess == true)
    {
       delete m_NLPProblem;
    }
    else
    {
       delete m_NURBSPoly; 
    }   
    delete m_OCPSolver;
    return 0;
}

int CDynamicPlanner::getOptimalTrajectory(void)
{
    /* Recover Original System */
    m_ReconfigOCProblem->RecoverSystem((const double** const)  m_Weights, 
                                       (const double*** const) m_ControlPoints, 
                                       &m_NofCollocPointsToRecover, 
                                       m_CollocPointsToRecover, 
                                       m_Time, 
                                       (double*** const) m_States, 
                                       (double*** const) m_Inputs);     
                                       
    if(m_VerboseMode==true){cerr << "Original system recovered successfully ..." << endl;}    
    return 0;                                   
}

int CDynamicPlanner::printOptimalTrajectory(void)
{                                       
    cerr << "Recovered System " << endl;
    cerr << endl;
    cerr << "States: " << endl;
    
    cerr << setw(16) << "t"      << '\t';
    cerr << setw(16) << "E"      << '\t';
    cerr << setw(16) << "Ed"     << '\t';
    cerr << setw(16) << "Edd"    << '\t';
    cerr << setw(16) << "N"      << '\t';
    cerr << setw(16) << "Nd"     << '\t';
    cerr << setw(16) << "Ndd"    << '\t';
    cerr << setw(16) << "v"      << '\t';
    cerr << setw(16) << "vd"     << '\t';
    cerr << setw(16) << "theta"  << '\t';
    cerr << setw(16) << "thetad" << endl; 
    
    for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
    {
        cerr << setw(16) << showpos << setprecision(12) << m_Time[m_i] << '\t';
        for(m_j=0; m_j<m_NofStates; m_j++)
        {
            for(m_k=0; m_k<m_NofDerivativesOfStates[m_j]; m_k++)
            {
                cerr << setw(16) << showpos << setprecision(12) << m_States[m_i][m_j][m_k] << '\t';
            }
        }
        cerr << endl;
    }
    cerr << endl;
    cerr << "Inputs: " << endl;
    cerr << setw(16) << "a"    << '\t';
    cerr << setw(16) << "phi"  << '\t';
    cerr << setw(16) << "phid" << endl;
    
    for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
    {
        for(m_j=0; m_j<m_NofInputs; m_j++)
        {
            for(m_k=0; m_k<m_NofDerivativesOfInputs[m_j]; m_k++)
            {
                cerr<< setw(16) << showpos << setprecision(12) << m_Inputs[m_i][m_j][m_k] << '\t';
            }
        }
        cerr << endl;
    }
    cerr << endl;    

    return 0;
}

int CDynamicPlanner::saveOptimalTrajectory(const char* const sFileName)
{
    int   TrajRows = m_NofCollocPointsToRecover;
    int   TrajCols = m_NofDerivativesOfStates[0] + m_NofDerivativesOfStates[1] + 1;
    double** DplannerTraj = new double*[TrajRows];
    for(m_i=0; m_i<TrajRows; m_i++){DplannerTraj[m_i] = new double[TrajCols];}                
    
    for(m_i=0; m_i<TrajRows; m_i++)
    {
        DplannerTraj[m_i][0] = m_Time[m_i];
        DplannerTraj[m_i][1] = m_States[m_i][0][0];
        DplannerTraj[m_i][2] = m_States[m_i][0][1];
        DplannerTraj[m_i][3] = m_States[m_i][0][2];
        DplannerTraj[m_i][4] = m_States[m_i][1][0];
        DplannerTraj[m_i][5] = m_States[m_i][1][1];
        DplannerTraj[m_i][6] = m_States[m_i][1][2];         
    }
    m_ErrorFlag = OTG_WriteFile(sFileName, &TrajRows, &TrajCols, (const double** const) DplannerTraj, false); 
    for(m_i=0; m_i<TrajRows; m_i++){delete[] DplannerTraj[m_i];}        
    delete[] DplannerTraj; 
    
    if(m_ErrorFlag != 0)
    {
       return -1; 
    }
    else
    {
       return 0;
    }
}

#ifdef InAlice
int CDynamicPlanner::sendOptimalTrajectory()
{
    /* Initialize the trajectory */
    m_Traj->startDataInput();
    
    /* Store the trajectory */
    for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
    {
        /* Northing ... then Easting ... */
        m_Traj->addPoint(m_States[m_i][1][0],m_States[m_i][1][1],m_States[m_i][1][2],m_States[m_i][0][0],m_States[m_i][0][1],m_States[m_i][0][2]);
    }
    
    /* Sending  trajectory */
    SendTraj(m_SendSocket, m_Traj);
    
    return 0;
}

int CDynamicPlanner::sendStopTrajectory()
{
    /* Initialize the trajectory */
    m_Traj->startDataInput();
    
    /* Store the trajectory */
    for(m_i=0; m_i<10; m_i++)
    {
        /* Northing ... then Easting ... */
        m_Traj->addPoint(m_InitialStates[1],0.0,0.0,m_InitialStates[0],0.0,0.0);
    }
    
    /* Sending  trajectory */
    SendTraj(m_SendSocket, m_Traj);
    
    return 0;
}
#endif

void CDynamicPlanner::DetectStopMode(void)
{
    /* If the distance between the initial and final condition is less than or equal to 1/2 a meter and the vf = 0 then stop */
    if((sqrt(pow(m_InitialConditionsLB[0]-m_FinalConditionsLB[0],2) + pow(m_InitialConditionsLB[1] - m_FinalConditionsLB[1],2)) <= 0.25) && (m_FinalConditionsLB[2] == 0.0))
    {
        m_StopMode = true; 
    }
    else
    {
        m_StopMode = false;
    }
    
    if(m_VerboseMode == true)
    {
        if(m_StopMode == true)
        {
            cerr << "Stop Mode = " << "true" << endl;
        }
        else
        {
            cerr << "Stop Mode = " << "false" << endl;
        }
    }
}

int CDynamicPlanner::getInitialVolumeGuess(void)
{
    m_FlatOutput = 0;
    m_NofPolynomials[m_FlatOutput] = m_NofMapPolytopes;
    
    m_NURBSData = dt_NofPolynomials;
    m_ErrorFlag = m_ReconfigOCProblem->setNURBSData(&m_NURBSData, &m_NofFlatOutputs, m_NofPolynomials);

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		delete[] m_Weights[m_i];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
            delete[] m_ControlPoints[m_i][m_j];
            delete[] m_LowerBoundsOnControlPoints[m_i][m_j];
            delete[] m_UpperBoundsOnControlPoints[m_i][m_j];
        }
    }

    m_NURBSDataX = nd_NofWeights;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofWeights);

    m_NURBSDataX = nd_NofControlPoints;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofControlPoints);
    
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		for(m_j=0; m_j<m_NofWeights[m_i]; m_j++){m_Weights[m_i][m_j] = 1.0;}

		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
            m_ControlPoints[m_i][m_j]              = new double[m_NofControlPoints[m_i]];
            m_LowerBoundsOnControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            m_UpperBoundsOnControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            
            for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++)
            {
                m_ControlPoints[m_i][m_j][m_k] = 0.0;
                m_LowerBoundsOnControlPoints[m_i][m_j][m_k] = -DBL_MAX;
                m_UpperBoundsOnControlPoints[m_i][m_j][m_k] =  DBL_MAX;
            }
		}
	}

	m_NLPProblem = new CNLPProblem(&m_NofMapPolytopes,
                                   m_MapPolytopes,
                                   &m_NofDimensions,
                                   &m_NofWayPoints, 
                                   (const double** const) m_DataPoints, 
                                   m_InitialPoint, 
                                   m_FinalPoint,
                                   &m_NofDimensionsOfFlatOutputs[m_FlatOutput],
                                   &m_DegreeOfPolynomials[m_FlatOutput], 
                                   &m_CurveSmoothness[m_FlatOutput]);
                                   
    // int NofCostEvaluations = 3;
    // m_NLPProblem->setNofCostEvaluations(&NofCostEvaluations);                                

    if(m_FirstProblem == false && m_ReusePreviousSolution == true)
    {
        /* The previous number of control points may not match the new number */
       //m_ErrorFlag = m_NLPProblem->setControlPoints(&m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (const double** const) m_ControlPoints[m_FlatOutput]);
    }
    
    m_NLPSolver = new CNLPSolver(m_NLPProblem);
    m_ErrorFlag = m_NLPSolver->SolveNLPProblem();
    
    //Here's an opportunity to guess again. How many times? It's up for grabs.
    int counter = 1;
    if(m_ErrorFlag != 1){counter = 0;}
    
    while(counter != 1)
    {
       m_NLPProblem->ResetInitialGuess();
       m_ErrorFlag = m_NLPSolver->SolveNLPProblem();  
       if(m_ErrorFlag == 1)
       {
          counter = 1;
       }
       else
       {
          counter++;
          if(counter > 10){counter = 1;}
       }
    }

    if(m_ErrorFlag == 1)
    {
        /* Get Lower and Upper Control Points and their initial guess*/  
        m_algBoundsType = alg_LowerBound;
        m_NLPProblem->getBoundsOnControlPoints(&m_algBoundsType, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (double** const) m_LowerBoundsOnControlPoints[m_FlatOutput]);
        
        m_algBoundsType = alg_UpperBound;
        m_NLPProblem->getBoundsOnControlPoints(&m_algBoundsType, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (double** const) m_UpperBoundsOnControlPoints[m_FlatOutput]);
        
        m_NLPProblem->getControlPoints(&m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (double** const) m_ControlPoints[m_FlatOutput]);
        
        if(m_DebugMode == true)
        {
            m_i = m_FlatOutput;
            for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
            {
                for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++)
                {
                    cerr << scientific << m_LowerBoundsOnControlPoints[m_i][m_j][m_k]  << " <= " << m_ControlPoints[m_i][m_j][m_k] <<  " <= "  << scientific << m_UpperBoundsOnControlPoints[m_i][m_j][m_k] << endl;
                }
            }
        }
    }
    
    delete m_NLPSolver;    
    return 0;
}

int CDynamicPlanner::getInitialGuess(void)
{
    m_FlatOutput = 0;
    m_NofPolynomials[m_FlatOutput] = m_NofMapPolytopes;
    
    m_NURBSData = dt_NofPolynomials;
    m_ErrorFlag = m_ReconfigOCProblem->setNURBSData(&m_NURBSData, &m_NofFlatOutputs, m_NofPolynomials);

	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		delete[] m_Weights[m_i];
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
            delete[] m_ControlPoints[m_i][m_j];
            delete[] m_LowerBoundsOnControlPoints[m_i][m_j];
            delete[] m_UpperBoundsOnControlPoints[m_i][m_j];
        }
    }

    m_NURBSDataX = nd_NofWeights;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofWeights);

    m_NURBSDataX = nd_NofControlPoints;
    m_ReconfigOCProblem->getNURBSData(&m_NURBSDataX, &m_NofFlatOutputs, m_NofControlPoints);
    
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		m_Weights[m_i] = new double[m_NofWeights[m_i]];
		for(m_j=0; m_j<m_NofWeights[m_i]; m_j++){m_Weights[m_i][m_j] = 1.0;}

		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
            m_ControlPoints[m_i][m_j]              = new double[m_NofControlPoints[m_i]];
            m_LowerBoundsOnControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            m_UpperBoundsOnControlPoints[m_i][m_j] = new double[m_NofControlPoints[m_i]];
            
            for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++)
            {
                m_ControlPoints[m_i][m_j][m_k] = 0.0;
                m_LowerBoundsOnControlPoints[m_i][m_j][m_k] = -DBL_MAX;
                m_UpperBoundsOnControlPoints[m_i][m_j][m_k] =  DBL_MAX;
            }
		}
	}

    m_NURBSPoly = new CNURBSPolyhedra(&m_NofMapPolytopes, m_MapPolytopes, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_DegreeOfPolynomials[m_FlatOutput], &m_CurveSmoothness[m_FlatOutput]);

    /* Pass the waypoints as the DataPoints */
    m_NURBSPoly->ConstructGuess(&m_NofDimensions, &m_NofWayPoints, (const double** const)m_DataPoints, m_InitialPoint, m_FinalPoint);

    /* Get Lower and Upper Control Points and their initial guess*/  
    m_algBoundsType = alg_LowerBound;
    m_NURBSPoly->getBoundsOnControlPoints(&m_algBoundsType, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (double** const) m_LowerBoundsOnControlPoints[m_FlatOutput]);

    m_algBoundsType = alg_UpperBound;
    m_NURBSPoly->getBoundsOnControlPoints(&m_algBoundsType, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (double** const) m_UpperBoundsOnControlPoints[m_FlatOutput]);
    
    m_NURBSPoly->getControlPoints(&m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (double** const) m_ControlPoints[m_FlatOutput]);

    if(m_DebugMode == true)
    {
        m_i = m_FlatOutput;
        for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
        {
            for(m_k=0; m_k<m_NofControlPoints[m_i]; m_k++)
            {
                cerr << scientific << m_LowerBoundsOnControlPoints[m_i][m_j][m_k]  << " <= " << m_ControlPoints[m_i][m_j][m_k] <<  " <= "  << scientific << m_UpperBoundsOnControlPoints[m_i][m_j][m_k] << endl;
            }
        }
    }
    delete m_NURBSPoly;
    
    return 0;
}

#ifdef InAlice
    int CDynamicPlanner::getNewOCPSpecs(void)
    {
        m_ocptSpecs->startSolve();
        
        if(m_useRDDF == true)
        {
           if(m_VerboseMode == true){cerr << "Requesting an RDDF ... " << endl;}
           while(1) 
           {
               m_ocptSpecs->getRDDFcorridor(m_RDDF);
               if(m_VerboseMode == true){cerr << "Got it! " << endl;}
               if((m_RDDF != NULL) && (m_RDDF->getNumTargetPoints() > 0))
               {
                   if(m_VerboseMode == true){cerr << "Extracting data from RDDF ... " << endl;}
                   m_NofWayPoints = m_RDDF->getNumTargetPoints();
                   m_RDDFVector   = *(new RDDFVector);
                   m_RDDFVector   = m_RDDF->getTargetPoints();
                   if(m_VerboseMode == true){cerr << "No. of Waypoints = " << m_NofWayPoints << endl;}
                   if(m_VerboseMode == true)
                   {
                      cout.flush();
                      m_RDDF->print();
                      cout.flush();
                   }
                   break;
               }
               else
               {
                   if(m_VerboseMode == true){cerr << "Empty RDDF ..." << endl;}
                   continue;
               }
           }
        }
        
        if(m_useParameters == true)
        {
           if(m_VerboseMode == true){cerr << "Initial Conditions ... " << endl;}
           m_BoundsType = ubt_LowerBound;
           m_ocptSpecs->getInitialCondition(&m_BoundsType, m_InitialConditionsLB);
            
           m_BoundsType = ubt_UpperBound;
           m_ocptSpecs->getInitialCondition(&m_BoundsType, m_InitialConditionsUB);
            
           if(m_VerboseMode == true)
           {
              for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++){cerr << fixed << setprecision(12) << m_InitialConditionsLB[m_i] << '\t' << fixed << setprecision(12) << m_InitialConditionsUB[m_i] << endl;}
           }
            
           if(m_VerboseMode == true){cerr << "Final Conditions ...  " << endl;}
           m_BoundsType = ubt_LowerBound;
           m_ocptSpecs->getFinalCondition(&m_BoundsType, m_FinalConditionsLB);
            
           m_BoundsType = ubt_UpperBound;
           m_ocptSpecs->getFinalCondition(&m_BoundsType, m_FinalConditionsUB);
            
           if(m_VerboseMode == true)
           {
              for(m_i=0; m_i<m_NofStates + m_NofInputs; m_i++){cerr << fixed << setprecision(12) << m_FinalConditionsLB[m_i] << '\t' << fixed << setprecision(12) << m_FinalConditionsUB[m_i] << endl;}
           }
            
           if(m_VerboseMode == true){cerr << "Parameters ... " << endl;}
           m_ocptSpecs->getParams(m_AllParams);    
            
           if(m_VerboseMode == true)
           {
              for(m_i=0; m_i<m_NofAllParams; m_i++){cerr << fixed << setprecision(12) << m_AllParams[m_i] << endl;}
           }
        }
        
        if(m_usePolyCorridor == true)
        {
            if(m_ChangePolytopeCoordinates == false)
            { 
                if(m_VerboseMode == true){cerr << "Polytopes ... " << endl;}
                m_NofMapPolytopes = m_ocptSpecs->getNofActivePolytopes();
                
                m_MapPolytopes    = new CPolytope[m_NofMapPolytopes];
                m_ocptSpecs->getActivePolytopes(m_MapPolytopes);
                
                if(m_VerboseMode == true){cerr << "Waypoints ... " << endl;}
                m_NofWayPoints = m_ocptSpecs->getNofActiveWaypoints();
                if(m_VerboseMode == true){cerr << "getNofActiveWaypoints returned : " << m_NofWayPoints << endl;}
                
                m_DataPoints = new double*[m_NofDimensions];
                for(m_i=0; m_i<m_NofDimensions; m_i++){m_DataPoints[m_i] = new double[m_NofWayPoints];}    
                
                m_ErrorFlag = m_ocptSpecs->getActiveWaypoints(m_DataPoints);
                if(m_VerboseMode == true){cerr << "getActiveWaypoints returned: " << m_ErrorFlag << endl;}
                
                if(m_VerboseMode == true)
                { 
                    for(m_i=0; m_i<m_NofDimensions; m_i++)
                    {
                        for(m_j=0; m_j<m_NofWayPoints; m_j++){cerr << m_DataPoints[m_i][m_j] << '\t';}
                        cerr << endl;
                    }
                }  
                
                if(m_DebugMode == true)
                {
                    for(m_k=0; m_k<m_NofMapPolytopes; m_k++)
                    {
                        cerr << "Polytope # " << m_k + 1 << endl;
                        cerr << &m_MapPolytopes[m_k] << endl;
                    }    
                }
                
                m_InitialPoint[0] = m_DataPoints[0][0];
                m_InitialPoint[1] = m_DataPoints[1][0];                                          
                
                m_FinalPoint[0]   = m_DataPoints[0][m_NofWayPoints-1];
                m_FinalPoint[1]   = m_DataPoints[1][m_NofWayPoints-1];           
                
                m_ErrorFlag = getPolytopeContainers();    
            }
            else
            {
                if(m_VerboseMode == true){cerr << "Polytopes ... " << endl;}
                m_NofActivePolytopes = m_ocptSpecs->getNofActivePolytopes();
                
                m_ActivePolytopes    = new CPolytope[m_NofActivePolytopes];
                m_ocptSpecs->getActivePolytopes(m_ActivePolytopes);
                
                if(m_VerboseMode == true){cerr << "Waypoints ... " << endl;}
                m_NofActiveWayPoints = m_ocptSpecs->getNofActiveWaypoints();
                if(m_VerboseMode == true){cerr << "getNofActiveWaypoints returned : " << m_NofActiveWayPoints << endl;}
                
                m_ActiveWayPoints = new double*[m_NofDimensions];
                for(m_i=0; m_i<m_NofDimensions; m_i++){m_ActiveWayPoints[m_i] = new double[m_NofActiveWayPoints];}    
                
                m_ErrorFlag = m_ocptSpecs->getActiveWaypoints(m_ActiveWayPoints);
                if(m_VerboseMode == true){cerr << "getActiveWaypoints returned: " << m_ErrorFlag << endl;}
                
                if(m_VerboseMode == true)
                { 
                    for(m_i=0; m_i<m_NofDimensions; m_i++)
                    {
                        for(m_j=0; m_j<m_NofActiveWayPoints; m_j++){cerr << m_ActiveWayPoints[m_i][m_j] << '\t';}
                        cerr << endl;
                    }
                }

                if(m_VerboseMode == true){cerr << "Reconstructing polytopes ... " << endl;}
                m_ErrorFlag = ReconstructPolytopes();
                if(m_VerboseMode == true){cerr << "Done!" << endl;}    
                
                if(m_DebugMode == true)
                {
                    for(m_k=0; m_k<m_NofMapPolytopes; m_k++)
                    {
                        cerr << "Polytope # " << m_k + 1 << endl;
                        cerr << &m_MapPolytopes[m_k] << endl;
                    }    
                }
                
                if(m_DataPoints != NULL)
                {
                    for(m_i=0; m_i<m_NofDimensions; m_i++){delete[] m_DataPoints[m_i];}    
                    delete[] m_DataPoints;
                }
                
                if(m_Headings != NULL){delete[] m_Headings;}
                
                m_NofWayPoints = m_NofActiveWayPoints;
                m_DataPoints = new double*[m_NofDimensions];
                for(m_i=0; m_i<m_NofDimensions; m_i++)
                {
                    m_DataPoints[m_i] = new double[m_NofWayPoints];
                    for(m_j=0; m_j<m_NofWayPoints; m_j++){m_DataPoints[m_i][m_j] = 0.0;}
                }    
                m_Headings = new double[m_NofWayPoints];
                for(m_j=0; m_j<m_NofWayPoints; m_j++){m_Headings[m_j] = 0.0;}                
                
                for(m_ell=0; m_ell<m_NofActiveWayPoints; m_ell++)
                {
                    m_DataPoints[0][m_ell] =  m_ActiveWayPoints[1][m_ell];
                    m_DataPoints[1][m_ell] =  m_ActiveWayPoints[0][m_ell];
                }

                for(m_ell=0; m_ell<m_NofActiveWayPoints-1; m_ell++)
                {
                    m_Headings[m_ell] = atan2(m_DataPoints[0][m_ell+1] - m_DataPoints[0][m_ell], m_DataPoints[1][m_ell+1] - m_DataPoints[1][m_ell]);
                }
                
                m_InitialPoint[0] = m_DataPoints[0][0];
                m_InitialPoint[1] = m_DataPoints[1][0];                                          
                
                m_FinalPoint[0]   = m_DataPoints[0][m_NofWayPoints-1];
                m_FinalPoint[1]   = m_DataPoints[1][m_NofWayPoints-1];           

                m_ErrorFlag = getPolytopeContainers();    
            }  
        }
        
        if(m_useCMap == true){}
        if(m_useObstacles == true){}
        
        m_ocptSpecs->endSolve();         
        
        return 0;
    }
    
    int CDynamicPlanner::ReconstructPolytopes(void)
    {
        m_NofActivePoints = new int[m_NofActivePolytopes];
        m_ActivePoints = new double**[m_NofActivePolytopes];
        for(m_i=0; m_i<m_NofActivePolytopes; m_i++)
        {
            m_NofActivePoints[m_i] = m_ActivePolytopes[m_i].getNofPoints();
            m_ActivePoints[m_i] = new double*[m_NofDimensions];
            for(m_j=0; m_j<m_NofDimensions; m_j++)
            {
                m_ActivePoints[m_i][m_j] = new double[m_NofActivePoints[m_i]];    
            }
            m_ActivePolytopes[m_i].getPoints(&m_NofDimensions, &m_NofActivePoints[m_i], (double** const)m_ActivePoints[m_i]);
        }

        m_NofMapPolytopes = m_NofActivePolytopes;
        m_MapPolytopes    = new CPolytope[m_NofMapPolytopes];
        for(m_i=0; m_i<m_NofMapPolytopes; m_i++)
        {
            m_MapPolytopes[m_i] = new CPolytope(&m_NofDimensions, &m_NofActivePoints[m_i], (const double** const)m_ActivePoints[m_i], &m_ChangePolytopeCoordinates);
            alg_ErrorFlag = m_AlgGeom->FacetEnumeration(&m_MapPolytopes[m_i]);
            if(alg_ErrorFlag == alg_NoError)
            {
                alg_ErrorFlag = m_AlgGeom->VertexEnumeration(&m_MapPolytopes[m_i]);
                if(alg_ErrorFlag == alg_NoError)
                {
                    m_AlgGeom->ComputeVolume(&m_MapPolytopes[m_i]);
                    m_AlgGeom->ComputeCentroid(&m_MapPolytopes[m_i]);
                    m_AlgGeom->ComputeBoundingBox(&m_MapPolytopes[m_i]); 
                }
            }
        }
        
        delete[] m_NofActivePoints;
        for(m_i=0; m_i<m_NofActivePolytopes; m_i++)
        {
            for(m_j=0; m_j<m_NofDimensions; m_j++){delete[] m_ActivePoints[m_i][m_j];}
            delete[] m_ActivePoints[m_i];
        }
        delete[] m_ActivePoints;
    
        return 0;
    }
    
#endif

int CDynamicPlanner::setArtificialEndPoint(void)
{
    m_FinalConditionsLB[0] = m_DataPoints[0][m_NofWayPoints-1];       // Easting_lb    
    m_FinalConditionsLB[1] = m_DataPoints[1][m_NofWayPoints-1];       // Northing_lb   
    m_FinalConditionsLB[2] =  0.10000;                                // v_lb           
    m_FinalConditionsLB[3] = m_Headings[m_NofWayPoints-2];            // Heading_lb    
    m_FinalConditionsLB[4] = -3.00000;                                // a_lb           
    m_FinalConditionsLB[5] = -0.44942;                                // phi_lb                       
    
    m_FinalConditionsUB[0] = m_DataPoints[0][m_NofWayPoints-1];       // Easting_ub    
    m_FinalConditionsUB[1] = m_DataPoints[1][m_NofWayPoints-1];       // Northing_ub   
    m_FinalConditionsUB[2] = 26.8224;                                 // v_ub           
    m_FinalConditionsUB[3] = m_Headings[m_NofWayPoints-2];            // Heading_ub    
    m_FinalConditionsUB[4] =  0.98100;                                // a_ub           
    m_FinalConditionsUB[5] =  0.44942;                                // phi_ub           
    
    return 0;
}

int CDynamicPlanner::getNewOCProblem(void)
{
    m_InitialConditionsLB[0] = m_DataPoints[0][0];                    // Easting_lb   
    m_InitialConditionsLB[1] = m_DataPoints[1][0];                    // Northing_lb   
    m_InitialConditionsLB[2] =  0.10000;                              // v_lb           
    m_InitialConditionsLB[3] = m_Headings[0];                         // Heading_lb    
    m_InitialConditionsLB[4] = -3.00000;                              // a_lb           
    m_InitialConditionsLB[5] = -0.44942;                              // phi_lb            
    
    m_InitialConditionsUB[0] = m_DataPoints[0][0];                    // Easting_ub    
    m_InitialConditionsUB[1] = m_DataPoints[1][0];                    // Northing_ub   
    m_InitialConditionsUB[2] = 26.8224;                               // v_ub           
    m_InitialConditionsUB[3] = m_Headings[0];                         // Heading_ub    
    m_InitialConditionsUB[4] =  0.98100;                              // a_ub           
    m_InitialConditionsUB[5] =  0.44942;                              // phi_ub            

    m_FinalConditionsLB[0] = m_DataPoints[0][m_NofWayPoints-1];       // Easting_lb    
    m_FinalConditionsLB[1] = m_DataPoints[1][m_NofWayPoints-1];       // Northing_lb   
    m_FinalConditionsLB[2] =  0.10000;                                // v_lb           
    m_FinalConditionsLB[3] = m_Headings[m_NofWayPoints-2];            // Heading_lb    
    m_FinalConditionsLB[4] = -3.00000;                                // a_lb           
    m_FinalConditionsLB[5] = -0.44942;                                // phi_lb                       
    
    m_FinalConditionsUB[0] = m_DataPoints[0][m_NofWayPoints-1];       // Easting_ub    
    m_FinalConditionsUB[1] = m_DataPoints[1][m_NofWayPoints-1];       // Northing_ub   
    m_FinalConditionsUB[2] = 26.8224;                                 // v_ub           
    m_FinalConditionsUB[3] = m_Headings[m_NofWayPoints-2];            // Heading_ub    
    m_FinalConditionsUB[4] =  0.98100;                                // a_ub           
    m_FinalConditionsUB[5] =  0.44942;                                // phi_ub                       
    
    m_AllParams[0]  =  5.43560;		// L
    m_AllParams[1]  =  2.13360;		// W
    m_AllParams[2]  =  1.06680;		// hcg
    m_AllParams[3]  =  0.10000;		// v_min
    m_AllParams[4]  = 26.82240;		// v_max
    m_AllParams[5]  = -3.00000;		// a_min
    m_AllParams[6]  =  0.98100;		// a_max
    m_AllParams[7]  = -0.44942;		// phi_min
    m_AllParams[8]  =  0.44942;		// phi_max
    m_AllParams[9]  = -1.30900;		// phid_min
    m_AllParams[10] =  1.30900;		// phid_max
    m_AllParams[11] =  9.81000;		// g
    m_AllParams[12] =  0.00000;		// vi
    m_AllParams[13] =  0.00000;		// vf
    m_AllParams[14] =  0.00000;		// ai
    m_AllParams[15] =  0.00000;		// af
   
    m_Parameters[0] = m_AllParams[0];  //L
    
    return 0;
}

int CDynamicPlanner::ReconfigureOCProblem(void)
{
    if(m_NofILinConstraints != 0)
    {
        m_li[0] = m_InitialConditionsLB[0];
        m_li[1] = m_InitialConditionsLB[1];
        
        m_ui[0] = m_InitialConditionsUB[0];
        m_ui[1] = m_InitialConditionsUB[1];
                
        if(m_VerboseMode == true){cerr << "Setting Initial Linear Constraints ..." << endl;}
        m_k = 0;
        m_ConstraintType = ct_LinInitial;
        
        m_BoundsType = ubt_LowerBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofILinConstraints, m_li);
        if(m_ErrorFlag != 0){return -1;}
        
        m_BoundsType = ubt_UpperBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofILinConstraints, m_ui);
        if(m_ErrorFlag != 0){return -1;}
        
        if(m_DebugMode == true)
        {
            for(m_i=0; m_i<m_NofILinConstraints; m_i++)
            {
                if(m_i==0)
                {
                    cerr << scientific << m_li[m_i] << " <= " << " E " << " <= " << scientific << m_ui[m_i] << endl;
                }
                else
                {
                    cerr << scientific << m_li[m_i] << " <= " << " N " << " <= " << scientific << m_ui[m_i] << endl;            
                }
            }
        }
        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }
    
    if(m_NofTLinConstraints != 0)
    {
        for(m_k = 0; m_k < m_NofConstraintCollocPoints; m_k++)
        {
            m_lt[m_k][0] = -DBL_MAX;
            m_lt[m_k][1] = -DBL_MAX;
            m_lt[m_k][2] =  0.01;
            
            m_ut[m_k][0] =  DBL_MAX; 
            m_ut[m_k][1] =  DBL_MAX; 
            m_ut[m_k][2] =  100.0; 
        }
    
        if(m_VerboseMode == true){cerr << "Setting Trajectory Linear Constraints ..." << endl;}

        m_ConstraintType = ct_LinTrajectory;
        for(m_k = 0; m_k < m_NofConstraintCollocPoints; m_k++)
        {
            m_BoundsType = ubt_LowerBound;
            m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofTLinConstraints, m_lt[m_k]);
            if(m_ErrorFlag != 0){return -1;}
            
            m_BoundsType = ubt_UpperBound;
            m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofTLinConstraints, m_ut[m_k]);
            if(m_ErrorFlag != 0){return -1;}

            if(m_DebugMode == true)
            {
                for(m_i=0; m_i<m_NofTLinConstraints; m_i++)
                {
                    if(m_i==0)
                    {
                        cerr << scientific << m_lt[m_k][m_i] << " <= " << " E " << " <= " << scientific << m_ut[m_k][m_i] << endl;
                    }
                    else if(m_i==1)
                    {
                        cerr << scientific << m_lt[m_k][m_i] << " <= " << " N " << " <= " << scientific << m_ut[m_k][m_i] << endl;            
                    }
                    else
                    {
                        cerr << scientific << m_lt[m_k][m_i] << " <= " << " tf " << " <= " << scientific << m_ut[m_k][m_i] << endl;                            
                    }
                }
            }
        }

        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }
    
    if(m_NofFLinConstraints != 0)
    {
        m_lf[0] = m_FinalConditionsLB[0];
        m_lf[1] = m_FinalConditionsLB[1]; 
        
        m_uf[0] = m_FinalConditionsUB[0];
        m_uf[1] = m_FinalConditionsUB[1];
            
        if(m_VerboseMode == true){cerr << "Setting Final Linear Constraints ..." << endl;}

        m_k = m_NofConstraintCollocPoints-1;    
        m_ConstraintType = ct_LinFinal;
        
        m_BoundsType = ubt_LowerBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofFLinConstraints, m_lf);
        if(m_ErrorFlag != 0){return -1;}

        m_BoundsType = ubt_UpperBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofFLinConstraints, m_uf);
        if(m_ErrorFlag != 0){return -1;}

        if(m_DebugMode == true)
        {
            for(m_i=0; m_i<m_NofFLinConstraints; m_i++)
            {
                if(m_i==0)
                {
                    cerr << scientific << m_lf[m_i] << " <= " << " E " << " <= " << scientific << m_uf[m_i] << endl;
                }
                else
                {
                    cerr << scientific << m_lf[m_i] << " <= " << " N " << " <= " << scientific << m_uf[m_i] << endl;            
                }
            }
        }
        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }
    
    if(m_NofINLinConstraints != 0)
    {
        m_Li[0] = m_InitialConditionsLB[3];
        m_Ui[0] = m_InitialConditionsUB[3];
        
        if(m_VerboseMode == true){cerr << "Setting Initial Nonlinear Constraints ..." << endl;}

        m_k = 0;
        m_ConstraintType = ct_NLinInitial;
        
        m_BoundsType = ubt_LowerBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofINLinConstraints, m_Li);
        if(m_ErrorFlag != 0){return -1;}
        
        m_BoundsType = ubt_UpperBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofINLinConstraints, m_Ui);
        if(m_ErrorFlag != 0){return -1;}

        if(m_DebugMode == true)
        {
            for(m_i=0; m_i<m_NofINLinConstraints; m_i++)
            {
                cerr << scientific << m_Li[m_i] << " <= " << " theta " << " <= " << scientific << m_Ui[m_i] << endl;
            }
        }

        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }
    
    if(m_NofTNLinConstraints != 0)
    {
        for(m_k = 0; m_k < m_NofConstraintCollocPoints; m_k++)
        {
            m_Lt[m_k][0] = m_AllParams[3];                                       // vmin 
            m_Lt[m_k][1] = m_AllParams[5];                                       // amin 
            m_Lt[m_k][2] = m_AllParams[7];                                       // phimin 
            m_Lt[m_k][3] = m_AllParams[9];                                       // phidmin 
            m_Lt[m_k][4] = -(m_AllParams[11]*m_AllParams[1])/(2*m_AllParams[2]); // roll-over
            
            m_Ut[m_k][0] = m_AllParams[4];                                       // vmax
            m_Ut[m_k][1] = m_AllParams[6];                                       // amax 
            m_Ut[m_k][2] = m_AllParams[8];                                       // phimax 
            m_Ut[m_k][3] = m_AllParams[10];                                      // phidmax 
            m_Ut[m_k][4] = (m_AllParams[11]*m_AllParams[1])/(2*m_AllParams[2]);  // roll-over 
        }
        
        if(m_VerboseMode == true){cerr << "Setting Trajectory Nonlinear Constraints ..." << endl;}
        m_ConstraintType = ct_NLinTrajectory;
        for(m_k = 0; m_k < m_NofConstraintCollocPoints; m_k++)
        {
            m_BoundsType = ubt_LowerBound;
            m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofTNLinConstraints, m_Lt[m_k]);
            if(m_ErrorFlag != 0){return -1;}

            m_BoundsType = ubt_UpperBound;
            m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofTNLinConstraints, m_Ut[m_k]);
            if(m_ErrorFlag != 0){return -1;}
            
            if(m_DebugMode == true)
            {
                for(m_i=0; m_i<m_NofTNLinConstraints; m_i++)
                {
                    if(m_i==0)
                    {
                        cerr << scientific << m_Lt[m_k][m_i] << " <= " << " v " << " <= " << scientific << m_Ut[m_k][m_i] << endl;
                    }
                    else if(m_i==1)
                    {
                        cerr << scientific << m_Lt[m_k][m_i] << " <= " << " a " << " <= " << scientific << m_Ut[m_k][m_i] << endl;            
                    }
                    else if(m_i==2)
                    {
                        cerr << scientific << m_Lt[m_k][m_i] << " <= " << " phi " << " <= " << scientific << m_Ut[m_k][m_i] << endl;                            
                    }
                    else if(m_i==3)
                    {
                        cerr << scientific << m_Lt[m_k][m_i] << " <= " << " phid " << " <= " << scientific << m_Ut[m_k][m_i] << endl;                            
                    }
                    else
                    {
                        cerr << scientific << m_Lt[m_k][m_i] << " <= " << " roll-over " << " <= " << scientific << m_Ut[m_k][m_i] << endl;                                                
                    }
                }
            }
        }
        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }
    
    if(m_NofFNLinConstraints != 0)
    {
        m_Lf[0] = m_FinalConditionsLB[3];
        m_Uf[0] = m_FinalConditionsUB[3];
    
        if(m_VerboseMode == true){cerr << "Setting Final Nonlinear Constraints ..." << endl;}
        m_k = m_NofConstraintCollocPoints-1;    
        m_ConstraintType = ct_NLinFinal;
        
        m_BoundsType = ubt_LowerBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofFNLinConstraints, m_Lf);
        if(m_ErrorFlag != 0){return -1;}

        m_BoundsType = ubt_UpperBound;
        m_ErrorFlag  = m_ReconfigOCProblem->setLUBounds(&m_ConstraintType, &m_BoundsType, &m_k, &m_NofFNLinConstraints, m_Uf);
        if(m_ErrorFlag != 0){return -1;}

        if(m_DebugMode == true)
        {
            for(m_i=0; m_i<m_NofFNLinConstraints; m_i++)
            {
                cerr << scientific << m_Lf[m_i] << " <= " << " theta " << " <= " << scientific << m_Uf[m_i] << endl;
            }
        }

        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    }

    for(m_k=0; m_k<m_NofCostCollocPoints; m_k++)
    {
        m_FunctionType   = ft_Cost;
        m_ErrorFlag      = m_ReconfigOCProblem->setOCPParameters(&m_FunctionType, &m_k, &m_NofParameters, m_Parameters);
        if(m_ErrorFlag != 0){return -1;}
    }

    for(m_k=0; m_k<m_NofConstraintCollocPoints; m_k++)
    {
        m_FunctionType   = ft_Constraint;
        m_ErrorFlag      = m_ReconfigOCProblem->setOCPParameters(&m_FunctionType, &m_k, &m_NofParameters, m_Parameters);        
        if(m_ErrorFlag != 0){return -1;}
    }

    if(m_VerboseMode == true){cerr << "Setting Bounds on control points ... " << endl;}

    m_FlatOutput = 0;
    m_BoundsType = ubt_LowerBound;
    m_ReconfigOCProblem->setBoundsOnControlPoints(&m_FlatOutput, &m_BoundsType, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (const double** const) m_LowerBoundsOnControlPoints[m_FlatOutput]);
    
    m_BoundsType = ubt_UpperBound;
    m_ReconfigOCProblem->setBoundsOnControlPoints(&m_FlatOutput, &m_BoundsType, &m_NofDimensionsOfFlatOutputs[m_FlatOutput], &m_NofControlPoints[m_FlatOutput], (const double** const) m_UpperBoundsOnControlPoints[m_FlatOutput]);
    
    m_ControlPoints[1][0][0] = 2.0;
            
    if(m_VerboseMode == true){cerr << "Done!" << endl;}

     m_OCPSolver->Reconfigure();

    return 0;
}

int CDynamicPlanner::TranslateRDDFToOverlappingPolytopes(void)
{
    if(m_DataPoints != NULL)
    {
       for(m_i=0; m_i<m_NofDimensions; m_i++){delete[] m_DataPoints[m_i];}    
       delete[] m_DataPoints;
    }
    
    if(m_Headings != NULL){delete[] m_Headings;}
    if(m_MapPolytopes != NULL){delete[] m_MapPolytopes;}
    if(m_ContainersOfMapPolytopes != NULL){delete[] m_ContainersOfMapPolytopes;}
    
    m_DataPoints = new double*[m_NofDimensions];
    for(m_i=0; m_i<m_NofDimensions; m_i++)
    {
        m_DataPoints[m_i] = new double[m_NofWayPoints];
        for(m_j=0; m_j<m_NofWayPoints; m_j++){m_DataPoints[m_i][m_j] = 0.0;}
    }    
    m_Headings = new double[m_NofWayPoints];
    for(m_j=0; m_j<m_NofWayPoints; m_j++){m_Headings[m_j] = 0.0;}
    
    m_NofMapPolytopes = m_NofWayPoints - 1;
    m_MapPolytopes    = new CPolytope[m_NofMapPolytopes];

    #ifdef InAlice
        m_ErrorFlag = ConstructFeasibleRegion(&m_NofWayPoints, &m_RDDFVector, (double** const)m_DataPoints, m_Headings, &m_NofMapPolytopes, m_MapPolytopes);    
    #else
        m_ErrorFlag = ConstructFeasibleRegion(&m_NofWayPoints, &m_RDDFDataSize, (const double** const)m_RDDFData, (double** const)m_DataPoints, m_Headings, &m_NofMapPolytopes, m_MapPolytopes);    
    #endif
                                         
    if(m_DebugMode == true)
    {
        for(m_k=0; m_k<m_NofMapPolytopes; m_k++)
        {
            cerr << "Polytope # " << m_k + 1 << endl;
            cerr << &m_MapPolytopes[m_k] << endl;
        }    
    }
     
    m_InitialPoint[0] = m_DataPoints[0][0];
    m_InitialPoint[1] = m_DataPoints[1][0];                                          

    m_FinalPoint[0]   = m_DataPoints[0][m_NofWayPoints-1];
    m_FinalPoint[1]   = m_DataPoints[1][m_NofWayPoints-1];

   /*  
    m_InitialPoint[0] = m_InitialConditionsLB[0];
    m_InitialPoint[1] = m_InitialConditionsLB[1];                                          

    m_FinalPoint[0]   = m_FinalConditionsLB[0];
    m_FinalPoint[1]   = m_FinalConditionsLB[1];
    */    
                                                                                                                                                                 
    m_ErrorFlag = getPolytopeContainers();    
    
    return 0;
}

int CDynamicPlanner::getPolytopeContainers(void)
{
    m_ContainersOfMapPolytopes = new CPolytope[m_NofMapPolytopes];
    for(m_k=0; m_k<m_NofMapPolytopes; m_k++)
    {
        m_MapPolytopes[m_k].getBoundingBox(&m_NofDimensions, m_mins, m_maxs);
        
        m_Points[0][0] = m_mins[0]; //xmin
        m_Points[1][0] = m_mins[1]; //ymin
        
        m_Points[0][1] = m_maxs[0]; //xmax
        m_Points[1][1] = m_mins[1]; //ymin
        
        m_Points[0][2] = m_maxs[0]; //xmax
        m_Points[1][2] = m_maxs[1]; //ymax
        
        m_Points[0][3] = m_mins[0]; //xmin
        m_Points[1][3] = m_maxs[1]; //ymax
        
        m_ContainersOfMapPolytopes[m_k] = new CPolytope(&m_NofDimensions, &m_NofPoints, (const double** const) m_Points);     
        m_ErrorFlag = m_AlgGeom->VertexAndFacetEnumeration(&m_ContainersOfMapPolytopes[m_k]);
        if(m_ErrorFlag == alg_NoError)
        {
            m_AlgGeom->ComputeCentroid   (&m_ContainersOfMapPolytopes[m_k]);
            m_AlgGeom->ComputeVolume     (&m_ContainersOfMapPolytopes[m_k]);
            m_AlgGeom->ComputeBoundingBox(&m_ContainersOfMapPolytopes[m_k]);    
        }
    }    

    if(m_DebugMode == true)
    {
        for(m_k=0; m_k<m_NofMapPolytopes; m_k++)
        {
            cerr << "Container # " << m_k + 1 << endl;
            cerr << &m_ContainersOfMapPolytopes[m_k] << endl;
        }
        cerr << endl;
    }
    
    return 0;
}

int CDynamicPlanner::getRDDFData(void)
{
    if(m_RDDFFileName == NULL){return -1;} // the file name variable has not been set.
    
    ifstream InReader;
    
    InReader.open(m_RDDFFileName, ios::in);
    if(!InReader.good()){return -2;}      // File could not be open.

    m_RDDFDataSize = 5;
    int    Waypoint;
    double Easting;
    double Northing;
    double Velocity;
    double Radius;

    m_NofWayPoints = 0;
    while(InReader >> Waypoint >> Easting >> Northing >> Velocity >> Radius){m_NofWayPoints ++;}
    InReader.close();

    if(m_RDDFData != NULL)
    {
       for(m_i=0; m_i<m_NofWayPoints; m_i++){delete[] m_RDDFData[m_i];}
       delete[] m_RDDFData;
    }

    m_RDDFData = new double*[m_NofWayPoints];
    for(m_i=0; m_i<m_NofWayPoints; m_i++){m_RDDFData[m_i] = new double[m_RDDFDataSize];}

    InReader.open(m_RDDFFileName, ios::in);
    if(!InReader.good()){return -1;} // File could not be open.
    
    m_i = 0;
    while(m_i >= 0 && m_i < m_NofWayPoints)
    {
        InReader >> m_RDDFData[m_i][0] >> m_RDDFData[m_i][1] >> m_RDDFData[m_i][2] >> m_RDDFData[m_i][3] >> m_RDDFData[m_i][4];
        m_i++;
    }
    InReader.close();
    
    if(m_DebugMode == true)
    {
        for(m_i=0; m_i<m_NofWayPoints; m_i++)
        {
            for(m_j=0; m_j<m_RDDFDataSize; m_j++){cout << m_RDDFData[m_i][m_j] << '\t';}
            cout << endl;
        }
        cout << endl;    
    }
    
    return 0;
}

int CDynamicPlanner::setRDDFFileName(const char* const RDDFFileName)
{
    if(RDDFFileName != NULL)
    {
       if(m_RDDFFileName != NULL)
       {
          delete[] m_RDDFFileName;
          m_RDDFFileName = NULL;
       }
       m_RDDFFileName = new char[(int)strlen(RDDFFileName) + 1];
       strcpy(m_RDDFFileName, RDDFFileName); 
       return 0;
    }
    else
    {
        return -1;
    }
}

CDynamicPlanner::~CDynamicPlanner(void)
{
    delete[] m_PositionIntervals;
    delete[] m_SurfaceDegreeOfPolynomials;
    delete[] m_SurfaceCurveSmoothness;
    delete[] m_SurfaceMaxDerivatives;
 
    delete m_CostComponent;
    delete m_AliceFlat;
    delete m_ReconfigOCProblem;
    delete m_AlgGeom;
    
	for(m_i=0; m_i<m_NofFlatOutputs; m_i++)
	{
		for(m_j=0; m_j<m_NofDimensionsOfFlatOutputs[m_i]; m_j++)
		{
            delete[] m_ControlPoints[m_i][m_j];
            delete[] m_LowerBoundsOnControlPoints[m_i][m_j];
            delete[] m_UpperBoundsOnControlPoints[m_i][m_j];
		}
		delete[] m_Weights[m_i];
		delete[] m_ControlPoints[m_i];
        delete[] m_LowerBoundsOnControlPoints[m_i];
        delete[] m_UpperBoundsOnControlPoints[m_i];        
	}
    
	delete[] m_Weights;
	delete[] m_ControlPoints;
	delete[] m_LowerBoundsOnControlPoints;
	delete[] m_UpperBoundsOnControlPoints;    
    
    delete[] m_NofDimensionsOfFlatOutputs;
    delete[] m_NofPolynomials;
    delete[] m_DegreeOfPolynomials;
    delete[] m_OrderOfPolynomials;    
    delete[] m_CurveSmoothness;    
    delete[] m_NofDerivatives;    
    delete[] m_NofWeights;
    delete[] m_NofControlPoints;    
    
    delete[] m_InitialConditionsLB;
    delete[] m_InitialConditionsUB;
    
    delete[] m_FinalConditionsLB;
    delete[] m_FinalConditionsUB;
    
    delete[] m_Parameters;
    
    delete[] m_InitialStates;
    delete[] m_InitialInputs;
    delete[] m_FinalStates;
    delete[] m_FinalInputs;
    delete[] m_AllParams;

    delete[] m_RDDFFileName;
    if(m_RDDFData != NULL)
    {
       for(m_i=0; m_i<m_NofWayPoints; m_i++){delete[] m_RDDFData[m_i];}
       delete[] m_RDDFData;
    }
       
    if(m_DataPoints != NULL)
    {
        for(m_i=0; m_i<m_NofDimensions; m_i++){delete[] m_DataPoints[m_i];}    
        delete[] m_DataPoints;
    }
    
    if(m_Headings != NULL){delete[] m_Headings;}
    if(m_MapPolytopes != NULL){delete[] m_MapPolytopes;}
    if(m_ContainersOfMapPolytopes != NULL){delete[] m_ContainersOfMapPolytopes;}
    
    delete[] m_InitialPoint;
    delete[] m_FinalPoint;
    
    delete[] m_mins;
    delete[] m_maxs;
    
    for(m_j=0; m_j<m_NofDimensions; m_j++){delete[] m_Points[m_j];}
    delete[] m_Points;
    
    if(m_NofILinConstraints != 0)
    {
       delete[] m_li;
       delete[] m_ui;
    }
    
    if(m_NofTLinConstraints != 0)
    {
       for(m_k=0; m_k<m_NofConstraintCollocPoints; m_k++)
       {
           delete[] m_lt[m_k];
           delete[] m_ut[m_k];
       }
       delete[] m_lt;
       delete[] m_ut;
    }

    if(m_NofFLinConstraints != 0)
    {
       delete[] m_lf;
       delete[] m_uf;
    }
    
    if(m_NofINLinConstraints != 0)
    {
       delete[] m_Li;
       delete[] m_Ui;
    }

    if(m_NofTNLinConstraints != 0)
    {
        for(m_k=0; m_k<m_NofConstraintCollocPoints; m_k++)
        {
            delete[] m_Lt[m_k];
            delete[] m_Ut[m_k];
        }
        delete[] m_Lt;
        delete[] m_Ut;
    }
    
    if(m_NofFNLinConstraints != 0)
    {
       delete[] m_Lf;
       delete[] m_Uf;
    }
    
    delete[] m_NofDerivativesOfStates;
    delete[] m_NofDerivativesOfInputs;
    delete[] m_CollocPointsToRecover;
    
	for(m_i=0; m_i<m_NofCollocPointsToRecover; m_i++)
	{
		for(m_j=0; m_j<m_NofStates; m_j++){delete[] m_States[m_i][m_j];}
		delete[] m_States[m_i];
		
		for(m_j=0; m_j<m_NofInputs; m_j++){delete[] m_Inputs[m_i][m_j];}
		delete[] m_Inputs[m_i];
	}	
    delete[] m_Time;
	delete[] m_States;
	delete[] m_Inputs;
    
    #ifdef InAlice
        delete m_RDDF;
        delete m_Traj;
        
        if(m_VerboseMode == true){cerr<< "Destroying OCPtSpecs ... " << endl;}
        delete m_ocptSpecs;
        if(m_VerboseMode == true){cerr << "Done!" << endl;}
    #endif

    if(m_StoreData == true)
    {
       for(m_i=0; m_i< m_MaxNofTrajs; m_i++)
       {
           for(m_j=0; m_j<m_NofCollocPointsToRecover; m_j++){delete[] m_TrajData[m_i][m_j];} 
           delete[] m_TrajData[m_i];
       }
       delete[] m_TrajData;

       delete[] m_TrajNofPolytopes;
       for(m_j=0; m_j<m_MaxNofTrajs; m_j++)
       {
           delete[] m_TrajPolytopes[m_j];
           delete[] m_TrajModifiedConvexSets[m_j];
           delete[] m_TrajOverlappingConvexSets[m_j];
       }
       delete[] m_TrajPolytopes;
       delete[] m_TrajModifiedConvexSets;
       delete[] m_TrajOverlappingConvexSets;         
    }    
}

