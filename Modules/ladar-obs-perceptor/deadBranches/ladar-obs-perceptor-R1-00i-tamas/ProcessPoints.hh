#include "LadarPerceptor.hh"
#include <frames/mat44.h>
#include <frames/quat.h>
#include <frames/pose3.h>
#include "nrc.h"

#ifndef PROCPOINTS_HH
#define PROCPOINTS_HH

// number of history points to fit line to (not used)
#define NUM_GS_POINTS 6
// maximum distance a groundstrike can be from ground
#define MAX_GS_DIST 0.2
// +- width rect to check groundstrike avg
#define GS_RECT 2

// ratio between roofmap and master map. integer.
#define ROOFMAP_FRAC 4
// roof ladar map size
#define ROOFMAP_SIZE (EMAP_SIZE/ROOFMAP_FRAC)
// left ladar freespace
#define RM_LEFT 0x1001
// right ladar freespace
#define RM_RIGHT 0x2002
// left ladar coverage
#define RMC_LEFT 0x0001
// right ladar coverage
#define RMC_RIGHT 0x0002

// amount to subtract from roof ladar range for groundstrike test
#define ROOF_LADAR_DELTA 1.25f
// distance to segment by for expanding foreground obstacles to avoid
// erroneous roof ladar groundstrike classification for narrow things
#define ROOF_LADAR_DILATE 10
// probability if we have no data
#define ROOF_NO_DATA 0.75f
// probability if in freespace
#define ROOF_FREESPACE 1.0f
// probability if outside of freespace, give it a softer edge
#define ROOF_OUTSIDE 0.2f
// dynamic groundstrike tracker values
#define GS_DYN_MAX 40
#define GS_DYN_INC 2
#define GS_DYN_DEC 1

// This is the per-point processor, used for just transforming points
// but also for doing the bulk of groundstrike filtering
class ProcessPoints
{
    // scan storage space
    LadarScanStore *scans;
    // map of road elevation
    EMap *roadMap;
    float local2road[4][4];
    float road2local[4][4];
    // map of roof ladar freespaces
    EMap *roofMap;
    float local2roof[4][4];
    float roof2local[4][4];
    // cumulative counter for groundstrike dynamics tracking
    int gsDynamics[NUM_LADARS][LADAR_BLOB_MAX_POINTS];

    void setRoofPoly(LadarScan *scan, float mat[4][4]);

public:
    ProcessPoints(LadarScanStore *scans)
    {
	memset(this,0,sizeof(*this));
	this->scans = scans;
	this->roadMap = new EMap(EMAP_SIZE);	
	this->roadMap->clear(0x0);
	this->roofMap = new EMap(ROOFMAP_SIZE);
	this->roofMap->clear(0x0);
    }

    ~ProcessPoints() 
    {
	delete this->roadMap;
	delete this->roofMap;
    }

    // Processes one scan, does everything that it needs to
    void ProcessScan(LadarScan *scan, float l2m[4][4]);

    // Update the internal road heightmap
    void UpdateRoadMap(EMap *rmap, float* l2m);

    EMap* getRoofMap()
    {
	return roofMap;
    }
};

#endif
