#include "LadarMap.hh"

void LadarMap::update()
{
    timer.startTime(5);
    for (int i=0;i<NUM_TRACK_LADARS;i++)
    {
	LadarScan *scan = scans->getPastScan(i,0);
	if (!scan->processed)
	    scan = scans->getPastScan(i,1);
	this->tracker[i].ProcessScan(scan);	
    }
    timer.printTime("Map update time",5);
    if ((DGCgettime()-this->lastDebugTime) > 100000)
    {
	this->lastDebugTime = DGCgettime();
	for (int i=0;i<NUM_TRACK_LADARS;i++)
	    this->tracker[i].sendDebug(this->debugTalker,this->debugSubgroup);
    }
    usleep(1);
}

void LadarMap::associate()
{
}
