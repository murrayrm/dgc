#include "LadarPerceptor.hh"
#include "emapViewer.hh"

// Default constructor
LadarPerceptor::LadarPerceptor()
{
    memset(this, 0, sizeof(*this));
    cmdline_parser_init(&this->options);    
    return;
}


// Default destructor
LadarPerceptor::~LadarPerceptor()
{
    cmdline_parser_free(&this->options);
    return;
}


// Parse the command line
int LadarPerceptor::parseCmdLine(int argc, char **argv)
{
    // Load options
    if (cmdline_parser(argc, argv, &this->options) < 0)
	return -1;
    
    // Fill out the spread name
    if (this->options.spread_daemon_given)
	this->spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
	this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
	return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
    
    // Fill out the skynet key
    if (this->options.skynet_key_given)
	this->skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
	this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
	this->skynetKey = 0;
    
    // Fill out module id
    this->moduleId = MODladarObsPerceptor;

    // Subgroups
    this->sendSubgroup = this->options.send_subgroup_arg;
    this->debugSubgroup = (this->options.send_debug_flag)?this->options.debug_subgroup_arg:0;
    
    // If the user gives us some log files on the command line, run in
    // replay mode.
    if (this->options.inputs_num > 0)
	this->mode = modeReplay;
    else
	this->mode = modeLive;
    
    return 0;
}


// Initialize sensnet
int LadarPerceptor::initSensnet()
{
    int i;
    
    // Create sensnet interface.  We do this in both live and replay
    // mode (in replay mode we use sensnet to write the map).
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
	return -1;
    
    // Subscribe to process state messages
    if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
	return ERROR("unable to join process group");
    
    if (this->mode == modeReplay)
    {
	// Create replay interface
	this->replay = sensnet_replay_alloc();
	assert(this->replay);    
	if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
	    return ERROR("unable to open log");
    }
    
    // Join ladar data groups
    for (i = 0; i < NUM_LADARS; i++)
    {
	if (this->sensnet)
	{
	    if (sensnet_join(this->sensnet, ladarSensorId[i],
			     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
		ERROR("unable to join %d", ladarSensorId[i]);
	}
    }
    
    return 0;
}


// Clean up sensnet
int LadarPerceptor::finiSensnet()
{
    int i;
    
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
    if (this->sensnet)
    {
	for (i = 0; i < NUM_LADARS; i++)
	{
	    sensnet_leave(this->sensnet, ladarSensorId[i], SENSNET_LADAR_BLOB);
	}
	sensnet_disconnect(this->sensnet);
	sensnet_free(this->sensnet);
	this->sensnet = NULL;
    }
    
    if (this->replay)
    {
	sensnet_replay_close(this->replay);
	sensnet_replay_free(this->replay);
	this->replay = NULL;
    }
    
    return 0;
}



// Initialize console display
int LadarPerceptor::initConsole()
{
    char *temp =
	//234567890123456789012345678901234567890123456789012345678901234567890123456789
	"LadarPerceptor $Revision$                                         \n"
	"                                                                           \n"
	"Skynet: %spread%                                                           \n"
	"                                                                           \n"
	"Ladar[0]: %ladar0%                                                         \n"
	"Ladar[1]: %ladar1%                                                         \n"
	"Ladar[2]: %ladar2%                                                         \n"
	"Ladar[3]: %ladar3%                                                         \n"
	"Ladar[4]: %ladar4%                                                         \n"
	"Ladar[5]: %ladar5%                                                         \n"
	"Ladar[6]: %ladar6%                                                         \n"
	"                                                                           \n"
	"Profile : %stats%                                                          \n"
	"                                                                           \n"
	"%stderr%                                                                   \n"
	"%stderr%                                                                   \n"
	"%stderr%                                                                   \n"
	"%stderr%                                                                   \n"
	"%stderr%                                                                   \n"
	"                                                                           \n"
	"[%QUIT%|%PAUSE%]                                                           \n";
    
    // Initialize console
    this->console = cotk_alloc();
    assert(this->console);
    
    // Set the console template
    cotk_bind_template(this->console, temp);
    
    // Bind buttons and toggles
    cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
		     (cotk_callback_t) onUserQuit, this);
    cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
		     (cotk_callback_t) onUserPause, this);
    
    // Initialize the display
    cotk_open(this->console,NULL);
    
    // Display some fixed values
    cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
		this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
    
    return 0;
}


// Finalize sparrow display
int LadarPerceptor::finiConsole()
{
    // Clean up the CLI
    if (this->console)
    {
	cotk_close(this->console);
	cotk_free(this->console);
	this->console = NULL;
    }
    
    return 0;
}


// Handle button callbacks
int LadarPerceptor::onUserQuit(cotk_t *console, LadarPerceptor *self, const char *token)
{
    MSG("user quit");
    self->quit = true;
    return 0;
}


// Handle button callbacks
int LadarPerceptor::onUserPause(cotk_t *console, LadarPerceptor *self, const char *token)
{
    self->pause = !self->pause;
    MSG("pause %s", (self->pause ? "on" : "off"));
    return 0;
}


// Initalize algorithms
int LadarPerceptor::init()
{
    // Initialize ladar data store
    this->scans = new LadarScanStore();
    this->pproc = new ProcessPoints(this->scans);
    // Create maps
    this->initMaps();
    this->ladarMap = new LadarMap(this->scans,this->smap,this->skynetKey,this->moduleId,this->sendSubgroup,this->debugSubgroup);
    // start the map update thread
    pthread_t thread;
    pthread_create(&thread,0,&LadarPerceptor::mapUpdateLoop,(void*)this);
    return 0;
}



// Finalize algorithms
int LadarPerceptor::fini()
{
    delete this->ladarMap;
    this->finiMaps();
    delete this->scans;
    delete this->pproc;
    return 0;
}


// Update the process state
int LadarPerceptor::updateProcessState()
{
    int blobId;
    ProcessRequest request;
    ProcessResponse response;
    
    // Send heart-beat message
    memset(&response, 0, sizeof(response));  
    response.moduleId = this->moduleId;
    response.timestamp = DGCgettime();
    response.logSize = 0;
    response.healthStatus = 2; //we're ok
    sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
		  this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
    
    // Read process request
    if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
		     &blobId, sizeof(request), &request) != 0)
	return 0;
    if (blobId < 0)
	return 0;
    
    // If we have request data, override the console values
    this->quit = request.quit;
    if (request.quit)
	MSG("remote quit request");
    
    return 0;
}

// Update the map with new range data
int LadarPerceptor::update()
{
    int i;
    int blobId, blobLen;
    LadarScan *scan;
    
    if (this->mode == modeLive)
    {
	// In live mode wait for new data, but timeout if
	// we dont get anything new for a while.
	if (sensnet_wait(this->sensnet, 200) != 0)
	    return 0;
    }
    else 
    {
	// In replay mode, advance the log to the next record.
	if (sensnet_replay_next(this->replay, 0) != 0)
	    return 0;
    }
    
    for (i = 0; i < NUM_LADARS; i++)
    {
	if (this->mode == modeLive)
	{
	    // Check the latest blob id
	    if (sensnet_peek(this->sensnet, ladarSensorId[i],
			     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
		continue;
	    
	    // Is this a new blob?
	    if (blobId == scans->getLastBlobId(i) || blobId == -1)
		continue;

	    // this is a new blob, get its index in the store
	    scan = scans->nextScan(i,blobId);
	    scan->processed = false;
	    
	    // New blob, so read it into our store
	    if (sensnet_read(this->sensnet, ladarSensorId[i],
			     SENSNET_LADAR_BLOB, &(scan->blobId), sizeof(LadarRangeBlob), &(scan->blob)) != 0)
		fprintf(stderr,"Error reading ladar %d/blob %d!\n",i,blobId);
	}
	else
	{ 
	    // Check the latest blob id
	    if (sensnet_replay_peek(this->replay, ladarSensorId[i],
				    SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
		continue;
	    
	    // Is this a new blob?
	    if (blobId == scans->getLastBlobId(i))
		continue;

	    // this is a new blob, get its index in the store
	    scan = scans->nextScan(i,blobId);
	    scan->processed = false;
	    
	    // Read new blob
	    if (sensnet_replay_read(this->replay, ladarSensorId[i],
				    SENSNET_LADAR_BLOB, &(scan->blobId), sizeof(LadarRangeBlob), &(scan->blob)) != 0)
		fprintf(stderr,"Error reading ladar %d/blob %d!\n",i,blobId);
	}

	scan->ladarId = i;
	scan->numPoints = scan->blob.numPoints;
	scan->timestamp = DGCgettime();

	if (scan->blob.timestamp > 0)
	    this->processScan(scan);
	else
	    printf("!!!!!!bad blob received from %d!!!!!!!!\n",i);

	if (this->console)
	{
	    // Update the console
	    char token[64];
	    snprintf(token, sizeof(token), "%%ladar%d%%", i);
	    cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
			sensnet_id_to_name(ladarSensorId[i]),
			scan->blob.scanId, fmod((double) scan->blob.timestamp * 1e-6, 10000));
	}
    }

    // Send off the data periodically
    if (DGCgettime() - this->sendTime > (uint64_t) 1000000/this->options.rate_arg)
    {
	if (this->options.show_maps_flag)
	    this->displayMaps();
	this->sendTime = DGCgettime();
    }
    
    return 0;
}

// Main program thread
int main(int argc, char **argv)
{
    LadarPerceptor *percept;
    
    percept = new LadarPerceptor();
    assert(percept);
    
    // Parse command line options
    if (percept->parseCmdLine(argc, argv) != 0)
	return -1;
    
    // Initialize sensnet
    if (percept->initSensnet() != 0)
	return -1;
    
    // Initialize cotk display
    if (!percept->options.disable_console_flag)
	if (percept->initConsole() != 0)
	    return -1;
    
    // Initialize maps and everything
    percept->init();

    //start emap viewer
    if (percept->options.show_maps_flag)
	startViewer(argc, argv, EMAP_SIZE);
    
    while (!percept->quit)
    {
	// Do heartbeat occasionally
	if (DGCgettime() - percept->processTime > 500000)
	{
	    percept->processTime = DGCgettime();
	    percept->updateProcessState();
	}
	
	// Update the console
	if (percept->console)
	    cotk_update(percept->console);

	// If we are paused, dont do anything
	if (percept->pause)
	{
	    usleep(100000);
	    continue;
	}
	// Get new data and update our perceptor
	if (percept->update() != 0)
	    ERROR("Error updating map");
    }
    
    // Clean up
    percept->fini();
    MSG("cleaned up map");
    percept->finiConsole();
    MSG("cleaned up console");
    percept->finiSensnet();
    MSG("cleaned up sensnet");
    delete percept;
    MSG("deleted perceptor");
    percept = NULL;

    MSG("waiting for threads to terminate");
    pthread_exit(NULL);

    return 0;
}

