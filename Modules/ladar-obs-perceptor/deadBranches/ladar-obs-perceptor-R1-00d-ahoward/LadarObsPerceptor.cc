
/* 
 * Desc: Obstacle/free space detector using bumper ladars.
 * Date: 17 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <float.h>

#include <ncurses.h>
#include <cotk/cotk.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/MapElementMsg.h>

// TODO REMOVE
#include <map/MapElement.hh>

#include "scanmap.h"
#include "cmdline.h"


class LadarObsPerceptor
{  
  public:
  
  // Constructor
  LadarObsPerceptor();

  // Destructor
  ~LadarObsPerceptor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  public:
  
  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, LadarObsPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, LadarObsPerceptor *self, const char *token);

  public:

  // Initalize map
  int init();

  // Finalize map
  int fini();
  
  // Update the process state
  int updateProcessState();

  // Update map with a single scan
  int updateMap(LadarRangeBlob *blob);

  // Send the free-space map
  int sendFreeMap();

  // Send the obstacle map
  int sendObsMap();

  public:
  
  // Check for new data and update maps
  int update();

  public:

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
   
  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet replay module
  sensnet_replay_t *replay;

  // Console interface
  cotk_t *console;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  // Individual ladar info
  struct LadarInfo
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Last blob id for this ladar
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  LadarInfo ladars[16];

  // Current state data
  VehicleState state;

  // Scan map
  scanmap_t *scanmap;

  // Time of last process update
  uint64_t processTime;
  
  // Time of last fusion
  uint64_t fusetime;

  // Number of scans that went into the last map
  int scanCount;

  // Performance stats
  uint64_t addCount, addTime;
  uint64_t fuseCount, fuseTime;
};



// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// More useful macros
#undef MIN
#undef MAX
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Default constructor
LadarObsPerceptor::LadarObsPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
LadarObsPerceptor::~LadarObsPerceptor()
{
  return;
}


// Parse the command line
int LadarObsPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  // Fill out module id
  this->moduleId = MODladarObsPerceptor;
  
  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
  else
    this->mode = modeLive;
  
  return 0;
}


// Initialize sensnet
int LadarObsPerceptor::initSensnet()
{
  int i;
  LadarInfo *ladar;

  // Create sensnet interface.  We do this in both live and replay
  // mode (in replay mode we use sensnet to write the map).
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  if (this->mode == modeReplay)
  {
    // Create replay interface
    this->replay = sensnet_replay_alloc();
    assert(this->replay);    
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  // Create list of ladars
  this->numLadars = 0;
  this->ladars[this->numLadars++].sensorId = SENSNET_RIEGL;
  this->ladars[this->numLadars++].sensorId = SENSNET_MF_BUMPER_LADAR;
  this->ladars[this->numLadars++].sensorId = SENSNET_LF_BUMPER_LADAR;
  this->ladars[this->numLadars++].sensorId = SENSNET_RF_BUMPER_LADAR;

  // Join ladar data groups
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = &this->ladars[i];
    ladar->blobId = -1;
    if (this->sensnet)
    {
      if (sensnet_join(this->sensnet, ladar->sensorId,  
                       SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
        ERROR("unable to join %d", ladar->sensorId);
    }
  }
     
  return 0;
}


// Clean up sensnet
int LadarObsPerceptor::finiSensnet()
{
  int i;
  LadarInfo *ladar;
  
  if (this->sensnet)
  {
    for (i = 0; i < this->numLadars; i++)
    {
      ladar = &this->ladars[i];
      sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
    }
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  
  if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}



// Initialize console display
int LadarObsPerceptor::initConsole()
{
  char *temp =
    //234567890123456789012345678901234567890123456789012345678901234567890123456789
    "LadarObsPerceptor $Revision$                                         \n"
    "                                                                           \n"
    "Skynet: %spread%                                                           \n"
    "                                                                           \n"
    "Ladar[0]: %ladar0%                                                         \n"
    "Ladar[1]: %ladar1%                                                         \n"
    "Ladar[2]: %ladar2%                                                         \n"
    "Ladar[3]: %ladar3%                                                         \n"
    "Ladar[4]: %ladar4%                                                         \n"
    "                                                                           \n"
    "Profile : %stats%                                                          \n"
    "                                                                           \n"
    "%stderr%                                                                   \n"
    "%stderr%                                                                   \n"
    "%stderr%                                                                   \n"
    "%stderr%                                                                   \n"
    "%stderr%                                                                   \n"
    "                                                                           \n"
    "[%QUIT%|%PAUSE%]                                                           \n";

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, temp);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int LadarObsPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int LadarObsPerceptor::onUserQuit(cotk_t *console, LadarObsPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarObsPerceptor::onUserPause(cotk_t *console, LadarObsPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Initalize algorithms
int LadarObsPerceptor::init()
{
  // Create map
  this->scanmap = scanmap_alloc();
  assert(this->scanmap);

  return 0;
}



// Finalize algorithms
int LadarObsPerceptor::fini()
{
  scanmap_free(this->scanmap);
  this->scanmap = NULL;

  return 0;
}


// Update the process state
int LadarObsPerceptor::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


// Update map with a single scan
int LadarObsPerceptor::updateMap(LadarRangeBlob *blob)
{
  uint64_t time;

  // Update the current state estimate
  if (blob->state.timestamp > this->state.timestamp)
    this->state = blob->state;

  // Clear the map, ready for new data
  if (this->scanmap->scan_count == 0)
    scanmap_clear(this->scanmap, &this->state);

  // TODO: the min/max bearing should be on for each individual ladar.
  // Update the map
  time = DGCgettime();
  scanmap_add_obs(this->scanmap, blob,
                  this->options.max_range_arg,
                  this->options.min_bearing_arg * M_PI/180,
                  this->options.max_bearing_arg * M_PI/180);
  if (this->options.enable_free_arg)
    scanmap_add_free(this->scanmap, blob);    
  this->addCount += 1;
  this->addTime += DGCgettime() - time;

  // See if we are ready to fuse the map
  if (DGCgettime() - this->fusetime > (uint64_t) 1000000/this->options.rate_arg)
  {
    this->fusetime = DGCgettime();

    MSG("fuse %lld scans %d ", this->fusetime/1000, this->scanmap->scan_count);
    
    // Fuse the map
    time = DGCgettime();
    scanmap_fuse(this->scanmap);
    this->fuseCount += 1;
    this->fuseTime += DGCgettime() - time;
    
    // Send off the data we have
    this->sendObsMap();
    if (this->options.enable_free_arg)
      this->sendFreeMap();

    // Record number of scans that went into this map
    this->scanCount = this->scanmap->scan_count;
    
    // Clear the map, ready for new data
    scanmap_clear(this->scanmap, &this->state);
  }

  if (this->console)
  {
    if (this->fuseCount > 0)
      cotk_printf(this->console, "%stats%", A_NORMAL, "add %dms fuse %dms scans %d    ",
                  (int) (this->addTime / this->addCount / 1000),
                  (int) (this->fuseTime / this->fuseCount / 1000),
                  this->scanCount);
  }
  
  return 0;
}


// Send the free-space map
int LadarObsPerceptor::sendFreeMap()
{
  int j;
  gpc_polygon *poly;
  gpc_vertex_list *contour;
  gpc_vertex *vertex;
  float m[4][4];
  float lx, ly;
  float ax, bx, ay, by;
  MapElementMsg msg = {0};
  int size;
  
  MSG("sending map");

  // TODO Assign unique id for free space polygon
  msg.numIds = 1;
  msg.id[0] = 6667;

  // Display colors
  msg.plotColor = MAP_COLOR_GREEN;
  msg.plotValue = 0xFF;

  // Object type
  msg.type = ELEMENT_OBSTACLE;
  msg.geometryType = GEOMETRY_POLY;

  // State data
  msg.frameType = FRAME_LOCAL;
  msg.state = this->state;

  msg.numLines = 1;
  strcpy(msg.label[0], "LadarFreeMap");

  poly = &this->scanmap->fused_space;
  mat44f_setf(m, this->scanmap->map2loc);
  ax = ay = +FLT_MAX;
  bx = by = -FLT_MAX;

  // Send the first contour only from the fused free space.
  // We cant cope with holes yet.
  if (poly->num_contours >= 1)
  {      
    contour = &poly->contour[0];

    for (j = 0; j < contour->num_vertices; j++)
    {
      vertex = &contour->vertex[j];

      // Compute coords in local frame
      lx = m[0][0]*vertex->x + m[0][1]*vertex->y + m[0][3];
      ly = m[1][0]*vertex->x + m[1][1]*vertex->y + m[1][3];

      // Update bounding box
      ax = MIN(ax, lx);
      bx = MAX(bx, lx);
      ay = MIN(ay, ly);
      by = MAX(by, ly);
          
      // Check for vertex list overflow
      if (msg.numPts >= (int) (sizeof(msg.geometry)/sizeof(msg.geometry[0])))
      {
        ERROR("overflow in free map element; ignoring scan");
        return 0;
      }

      // Copy vertex
      msg.geometry[msg.numPts].x = lx;
      msg.geometry[msg.numPts].y = ly;
      msg.numPts++;
    }
  }

  // Bounding box
  msg.center.x = (bx + ax) / 2;
  msg.center.y = (by + ay) / 2;
  msg.length = bx - ax;
  msg.width = by - ay;
  msg.orientation = 0;  
  msg.height = 1;

  // Compute message size
  size = sizeof(msg) - sizeof(msg.geometry);
  size += msg.numPts * sizeof(msg.geometry[0]);

  MSG("sending map size %d", size);
  
  // Ready to go, so write it
  assert(this->sensnet);
  sensnet_write(this->sensnet, SENSNET_METHOD_SKYNET, SENSNET_SKYNET_SENSOR,
                SNmapElement, 0, size, &msg);
     
  return 0;
}


// Send the obstacle map
int LadarObsPerceptor::sendObsMap()
{
  int i, j;
  float m[4][4];
  float mx, my;
  float lx, ly;
  float ax, bx, ay, by;
  scanmap_cell_t *cell;
  MapElementMsg msg = {0};
  int size;
  
  MSG("sending map");
  
  // TODO Assign unique id for obstacle points
  msg.numIds = 1;
  msg.id[0] = 6668; // MAGIC

  msg.plotColor = MAP_COLOR_RED;
  msg.plotValue = 0xFF;

  // Object type
  msg.type = ELEMENT_OBSTACLE;
  msg.geometryType = GEOMETRY_POINTS;

  // State data
  msg.frameType = FRAME_LOCAL;
  msg.state = this->state;

  msg.numLines = 1;
  strcpy(msg.label[0], "LadarObsMap");

  mat44f_setf(m, this->scanmap->map2loc);
  ax = ay = +FLT_MAX;
  bx = by = -FLT_MAX;

  for (j = 0; j < this->scanmap->map_size; j++)
  {
    for (i = 0; i < this->scanmap->map_size; i++)
    {
      cell = scanmap_cell_index(this->scanmap, i, j);
      assert(cell);

      if (cell->hits < 2)
        continue;

      // Compute coords in map frame
      scanmap_i2p(this->scanmap, i, j, &mx, &my);
      
      // Compute coords in local frame
      lx = m[0][0]*mx + m[0][1]*my + m[0][3];
      ly = m[1][0]*mx + m[1][1]*my + m[1][3];

      // Update bounding box
      ax = MIN(ax, lx);
      bx = MAX(bx, lx);
      ay = MIN(ay, ly);
      by = MAX(by, ly);
          
      // Check for vertex list overflow
      if (msg.numPts >= (int) (sizeof(msg.geometry)/sizeof(msg.geometry[0])))
      {
        ERROR("overflow in obstacle map element; ignoring scan");
        return 0;
      }

      // Copy vertex
      msg.geometry[msg.numPts].x = lx;
      msg.geometry[msg.numPts].y = ly;
      msg.numPts++;
    }
  }

  // Bounding box
  msg.center.x = (bx + ax) / 2;
  msg.center.y = (by + ay) / 2;
  msg.length = bx - ax;
  msg.width = by - ay;
  msg.orientation = 0;  
  msg.height = 1;

  // Compute message size
  size = sizeof(msg) - sizeof(msg.geometry);
  size += msg.numPts * sizeof(msg.geometry[0]);

  MSG("sending map points %d size %d ", msg.numPts, size);

  // Ready to go, so write it
  assert(this->sensnet);
  sensnet_write(this->sensnet, SENSNET_METHOD_SKYNET, SENSNET_SKYNET_SENSOR,
                SNmapElement, 0, size, &msg);
     
  return 0;
}



// Update the map with new range data
int LadarObsPerceptor::update()
{
  int i;
  LadarInfo *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;
  
  if (this->mode == modeLive)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else 
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }
  
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    if (this->mode == modeLive)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == ladar->blobId)
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else
    { 
      // Check the latest blob id
      if (sensnet_replay_peek(this->replay, ladar->sensorId,
                             SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == ladar->blobId)
        continue;

      // Read new blob
      if (sensnet_replay_read(this->replay, ladar->sensorId,
                              SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

    // We got new data, so update the map
    this->updateMap(&blob);

    if (this->console)
    {
      // Update the console
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
    }
  }

  return 0;
} 



// Main program thread
int main(int argc, char **argv)
{
  LadarObsPerceptor *percept;
  
  percept = new LadarObsPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  // Initialize map
  percept->init();

  while (!percept->quit)
  {
    // Do heartbeat occasionally
    if (DGCgettime() - percept->processTime > 500000)
    {
      percept->processTime = DGCgettime();
      percept->updateProcessState();
    }

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(100000);
      continue;
    }

    // Get new data and update our perceptor
    if (percept->update() != 0)
      break;
  }

  // Clean up
  percept->fini();
  percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}

