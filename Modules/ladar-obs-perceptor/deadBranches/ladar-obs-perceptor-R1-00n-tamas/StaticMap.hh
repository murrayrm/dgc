#include "LadarPerceptor.hh"
#include <frames/mat44.h>
#include <frames/vec3.h>

#ifndef _STATICMAP_HH
#define _STATICMAP_HH

using namespace std;

#define SMAP_CLEAR 40000
#define SMAP_INC 1200
#define SMAP_DEC 300
#define SMAP_DW 2
#define SMAP_TIME_DEC 300

#define SMAP_MIN_DIST 0.1f
#define SMAP_MAX_DIST 80
#define SMAP_NORETURN 45

// delta angle for freespace, 0.5 deg
#define SMAP_DA 0.00872665f
#define SMAP_DR 0.1f

#define SMAP_FG_DIST 2


class StaticMap
{
private:
    EMap *map;
    EMap *testmap;
    float local2map[4][4];
    float map2local[4][4];
public:
    StaticMap();
    ~StaticMap();
    
    EMap* getMap() {return this->map;}
    EMap* getTestMap() {return this->testmap;}
    void getMatrices(float l2m[4][4], float m2l[4][4])
    {
	memcpy(l2m,local2map,sizeof(int)*16);
	memcpy(m2l,map2local,sizeof(int)*16);
    }

    void shiftMap(int dx, int dy, float l2m[4][4]);
    void drawScan(LadarScan *scan);
    void timeDec();
};

#endif
