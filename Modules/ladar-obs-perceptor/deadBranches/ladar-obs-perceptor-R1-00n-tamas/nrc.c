#include <math.h>
#include "nrc.h"

static float sqrarg;
#define SQR(a) ((sqrarg=(a)) == 0.0 ? 0.0 : sqrarg*sqrarg)

extern void fit(float x[], float y[], int ndata, float *a, 
	 float *b, float *siga, float *sigb, float *chi2)
{
    int i;
    float t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss,sigdat;

    *b=0.0;

    for (i=0;i<ndata;i++) {
	sx += x[i];
	sy += y[i];
    }
    ss=ndata;
    sxoss=sx/ss;
    for (i=0;i<ndata;i++) {
	t=x[i]-sxoss;
	st2 += t*t;
	*b += t*y[i];
    }
    *b /= st2;
    *a=(sy-sx*(*b))/ss;
    *siga=sqrt((1.0+sx*sx/(ss*st2))/ss);
    *sigb=sqrt(1.0/st2);
    *chi2=0.0;
    for (i=0;i<ndata;i++)
	*chi2 += SQR(y[i]-(*a)-(*b)*x[i]);
    sigdat=sqrt((*chi2)/(ndata-2));
    *siga *= sigdat;
    *sigb *= sigdat;
}
