#include "LadarMap.hh"
#include "emapViewer.hh"

bool LadarTracker::ProcessScan(LadarScan *scan)
{
    if (scan->blobId <= lastProcId)
	return false;

    predict(scan->blobId - lastProcId);
    vector<LadarSegment> segments = segmentScan(scan);
    associateScan(scan, segments);
    pruneMap();
    updateTrackList();

    lastProcId = scan->blobId;
    lastScan = scan;
    return true;
}

// Segments a scan based on range and groundstrike discontinuity
vector<LadarSegment> LadarTracker::segmentScan(LadarScan *scan)
{
    vector<LadarSegment> segments;
    int newSegment[LADAR_BLOB_MAX_POINTS];
    double gsConf[LADAR_BLOB_MAX_POINTS];

    LadarSegment segment;

    segment.scan = scan;
    segment.blobId = scan->blobId;
    segment.start = 0;
    segment.end = 0;
    segment.flags = 0;

    double pr, segdist;

    newSegment[0] = 2;
    gsConf[0] = 0.5;

    // find all the breaks between segments
    for (int i=1; i<scan->numPoints; i++)
    {
	pr = MIN(scan->blob.points[i][RANGE],scan->blob.points[i-1][RANGE]);
	segdist = MAP_SEG_MIN + MAP_SEG_COEFF*pr;
	// there is a break in the scan, mark new segment
	if (vec3_mag(vec3_sub(scan->points[i-1],scan->points[i])) > segdist)
	    newSegment[i] = 2;
	else
	    newSegment[i] = 0;
	// groundstrike check, segment if gs discontinuity
	gsConf[i] = scan->gsRoofConf[i]*scan->gsRoadConf[i];
/*	if (fabs(gsConf[i]-gsConf[i-1]) > GS_GAP)
	newSegment[i]++;*/
    }

    // close any no-return gaps smaller than or equal to NR_GAP_POINTS
    // if distance is less than NR_GAP_DIST
    int nrCount=0;
    int nrStart=0;
    bool isnr = false;
    for (int i=1;i<scan->numPoints-1;i++)
    {
	if (scan->blob.points[i][RANGE] > MAX_LADAR_DIST)
	{
	    if (isnr) //continuing no-return seg
	    {
		nrCount++;
	    }
	    else // starting no-return segment
	    {
		nrCount=1;
		nrStart=i;
		isnr = true;
	    }
	}
	else
	{
	    if (isnr) // ending no-return segment
	    {
		double dist = vec3_mag(vec3_sub(
					   scan->points[i],scan->points[nrStart-1]));
		if (dist < NR_GAP_DIST && nrCount <= NR_GAP_POINTS)
		{
		    // close gap
		    for (int j=nrStart;j<=i+1;j++)
			newSegment[j] = 0;
		}
	    }
	    isnr = false;
	}
    }

    segment.left = segment.center = segment.min = segment.max = scan->points[0];
    double avgGs, maxGs;
    bool useSegment = true;
    int n=1;

    // compute segment properties and add to our list
    for (int i=1; i<scan->numPoints+1; i++)
    {
	if (newSegment[i]>0)
	{
	    segment.end = i-1;
	    segment.right = scan->points[i-1];
	    segment.center = vec3_scale((1.0/n),segment.center);	    
	    // only set foreground-ness if not groundstrike break
	    if (i < scan->numPoints)
		if (newSegment[i] > 1 && (scan->blob.points[i][RANGE] > scan->blob.points[i-1][RANGE]))
		    segment.flags |= FG_R;
	    avgGs /= n;
	    // throw away if we're sure it's a groundstrike
    
	    if (avgGs > GS_UPPER_THRESH)
		useSegment = false;
	    // otherwise use maximum gs value in segment to decide
	    else if (avgGs > GS_LOWER_THRESH && maxGs > GS_UPPER_THRESH)
		useSegment = false;

	    if (segment.flags == FG_N)
		useSegment = false;
	    
	    // add segment to list
	    if (useSegment)
		segments.push_back(segment);

	    if (i == scan->numPoints)
		break;
	    
	    // set new values
	    segment.left = segment.center = segment.min = segment.max = scan->points[i];
	    segment.start = i;
	    segment.flags = 0;
	    segment.reflective = false;
	    avgGs = maxGs = gsConf[i];
	    useSegment = true;
	    n = 1;
	    // check reflectivity
	    if (scan->blob.intensities[i] > 0)
		segment.reflective = true;
	    // throw away no-returns
	    if (scan->blob.points[i][RANGE] > MAX_LADAR_DIST)
		useSegment = false;
	    // foreground corner
	    if (newSegment[i] > 1 && (scan->blob.points[i-1][RANGE] > scan->blob.points[i][RANGE]))
		segment.flags |= FG_L;
	}
	else if (scan->blob.points[i][RANGE] > MAX_LADAR_DIST)
	{
	    // skip over no-returns, to close small gaps for cars
	    continue;
	}
	else
	{
	    segment.center = vec3_add(segment.center,scan->points[i]);
	    segment.min = vec3_min(segment.min,scan->points[i]);
	    segment.max = vec3_max(segment.max,scan->points[i]);
	    avgGs += gsConf[i];
	    n++;
	    // keep track of max groundstrike value
	    maxGs = MAX(gsConf[i],maxGs);
	    // check reflectivity
	    if (scan->blob.intensities[i] > 0)
		segment.reflective = true;
	}
    }

    return segments;
}

void LadarTracker::associateScan(LadarScan *scan, vector<LadarSegment> &segments)
{
    size_t i;
    double score, maxScore;
    int maxIndex;

    LadarScan *prevScan = scans->getPastScan(scan->ladarId,1);

    // first, try associating with previous associations
    if ((scan->blobId - prevScan->blobId) < 75)
    {
	int lastChecked = -1;

	for (i=0;i<segments.size();i++)
	{
	    maxScore = 0;
	    maxIndex = -1;
	    for (int j=segments[i].start;j<=segments[i].end;j++)
	    {
		if (prevScan->lastElementId[j] >= 0 && prevScan->lastElementId[j] != lastChecked)
		{
		    lastChecked = prevScan->lastElementId[j];
		    score = scoreSegment(segments[i], lastChecked);
		    if (score > maxScore)
		    {
			maxScore = score;
			maxIndex = lastChecked;
		    }
		}
	    }
	    // found a good association, so use it
	    if (maxIndex != -1)
	    {
		addToTrack(segments[i],maxIndex);
		segments[i].blobId = 0;
	    }
	}
    }

    // now go through and do a deep association if we didn't get some
    for (i=0;i<segments.size();i++)
    {
	// already associated?
	if (segments[i].blobId == 0)
	    continue;
	maxScore = 0;
	maxIndex = -1;
	// loop through all tracks and associate
	for (int j=0;j<highestTrack;j++)
	{
	    score = scoreSegment(segments[i], j);
	    if (score > maxScore)
	    {
		maxScore = score;
		maxIndex = j;
	    }
	}
	// found a good association, so use it
	if (maxIndex != -1)
	{
	    addToTrack(segments[i],maxIndex);
	    segments[i].blobId = 0;
	}
    }

    // and create new tracks for unassociated non-background segments
    // (this code should probably be improved some)
    for (i=0;i<segments.size();i++)
    {
	if (segments[i].blobId != 0 && segments[i].flags != FG_N)
	{
	    createTrack(segments[i]);
	}
    }
}

void LadarTracker::predict(int blobId)
{
    double dt;

    // loop through all the objects and update stuff
    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status <= 0)
	    continue;
	dt = (blobId-track[i].segment[0].blobId)*LADAR_DT;
	// update position with dt*vel
	track[i].nx = track[i].x[0] + dt*track[i].x[1];
	track[i].ny = track[i].y[0] + dt*track[i].y[1];
    }
}

void LadarTracker::pruneMap()
{
    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status == 0)
	    continue;
	track[i].status--;
	// check if its geometry still in storage
	if (track[i].segment[0].blobId != track[i].segment[0].scan->blobId)
	    deleteTrack(i);
	else if (track[i].status <= 0)
	    deleteTrack(i);
    }
}

inline double LadarTracker::scoreSegment(LadarSegment& segment, int index)
{
    // have we already associated with this track?
    if (segment.blobId == track[index].segment[0].blobId)
	return 0;
    if (track[index].status == 0)
	return 0;
    
    double distl,distr,distc,dist;
    distl = distr = distc = 2*TRACK_MAX_ASSOC;
    // find which corners they have in common
/*    int corners = (segment.flags & track[index].segment[0].flags);
    switch (corners)
    {
    case FG_B: //both corners
	distc = vec3_mag(vec3_sub(segment.center,track[index].segment[0].center));
	distl = vec3_mag(vec3_sub(segment.left,track[index].segment[0].left));
	distr = vec3_mag(vec3_sub(segment.right,track[index].segment[0].right));
	break;
    case FG_L: //left corner only
	distl = vec3_mag(vec3_sub(segment.left,track[index].segment[0].left));
	break;
    case FG_R: //right corner only
	distr = vec3_mag(vec3_sub(segment.right,track[index].segment[0].right));
	break;
    case FG_N: //no corners in common, return 0
	return 0;
    }
*/    
    // get the best association from any available corners
//    dist = MIN(MIN(distr,distl),distc);
    dist = vec3_mag(vec3_sub(segment.center, 
			     vec3_set(track[index].x[0],track[index].y[0],
				      segment.center.z)));
    if (dist > TRACK_MAX_ASSOC)
	return 0;
    // for now, just return linear score based on distance
    return (1.0-(dist/TRACK_MAX_ASSOC));
}

void LadarTracker::addToTrack(LadarSegment& segment, int index)
{
    if (track[index].status == 0)
    {
	fprintf(stderr,"Tried to add to dead track %d!\n",index);
	return;
    }
    if (track[index].segment[0].blobId == segment.blobId)
    {
	fprintf(stderr,"Tried to add to track %d twice!\n",index);
	return;
    }
    track[index].numAssoc++;
    int dp = abs((segment.end-segment.start)-(track[index].segment[0].end-track[index].segment[0].start));
    // only increment status if geo change is slow, else just hold
    if (dp <= TRACK_CHANGE_POINTS)
	track[index].status += 2; // 2 because we minus one each time
    else
	track[index].status++;

    if (track[index].status > TRACK_MAX_STATUS)
	track[index].status = TRACK_MAX_STATUS;
    // move old segments down one
    memmove(track[index].segment+1,track[index].segment,
	    sizeof(LadarSegment)*(NUM_PAST_SEGS-1));
    // add new segment
    track[index].segment[0] = segment;
    // update scan's lastElementId stuff
    for (int i=segment.start;i<=segment.end;i++)
	segment.scan->lastElementId[i] = index;
    // does the scan contain a reflective point?
    if (segment.reflective)
	track[index].numReflective++;

    // update our confidence values of this segment's car-ness
    updateCarConf(index);

    if (track[index].numAssoc == KF_INIT_WAIT)
    {
	// initialize Kalman Filter using averaged velocity as seed
	initKF(index);
    }
    else if (track[index].numAssoc > KF_INIT_WAIT)
    {
	// predict & update Kalman Filter
	predictKF(index);
	double range = vec3_mag(vec3_sub(segment.center,segment.scan->ladarPos));
	updateKF(index, segment.center.x, segment.center.y, range*KF_SIGMA_COEFF);
    }
    else
    {
	// update averaged velocity estimate (for KF init)
	updateAvg(index);
    }
}

void LadarTracker::initKF(int index)
{
    LadarSegment& firstSeg = track[index].segment[track[index].numAssoc-1];
    LadarSegment& lastSeg = track[index].segment[0];
    track[index].x[0] = lastSeg.center.x;
    track[index].y[0] = lastSeg.center.y;
    double dt = (lastSeg.blobId-firstSeg.blobId)*LADAR_DT;
    // compute velocity relative to original location
    double vx = (lastSeg.center.x-firstSeg.center.x)/dt;
    double vy = (lastSeg.center.y-firstSeg.center.y)/dt;
    vx = track[index].velocity.x;
    vy = track[index].velocity.y;
    // limit velocity
    vx = MIN(vx,KF_MAX_VEL);
    vy = MIN(vy,KF_MAX_VEL);
    vx = MAX(vx,-KF_MAX_VEL);
    vy = MAX(vy,-KF_MAX_VEL);
    track[index].x[1] = vx;
    track[index].y[1] = vy;

    track[index].Px[0][0] = KF_COV_INIT;
    track[index].Px[1][0] = 0;
    track[index].Px[0][1] = 0;
    track[index].Px[1][1] = KF_COV_INIT;
    track[index].Py[0][0] = KF_COV_INIT;
    track[index].Py[1][0] = 0;
    track[index].Py[0][1] = 0;
    track[index].Py[1][1] = KF_COV_INIT;
}

void LadarTracker::predictKF(int index)
{
    double dt = LADAR_DT * (track[index].segment[0].blobId - track[index].segment[1].blobId);
    double A[2][2] = {{1, dt},{0, 1}};
    double At[2][2] = {{1, 0},{dt, 1}};
    double tmp[2][2];

    // update state
    track[index].x[0] += dt*track[index].x[1];
    track[index].y[0] += dt*track[index].y[1];
    // update state covariance matrices
    mat22d_mul(tmp,track[index].Px,At);
    mat22d_mul(track[index].Px,A,tmp);
    mat22d_mul(tmp,track[index].Py,At);
    mat22d_mul(track[index].Py,A,tmp);
    // add process noise
    track[index].Px[1][1] += KF_PROC_NOISE;
    track[index].Py[1][1] += KF_PROC_NOISE;    
}

void LadarTracker::updateKF(int index, double nx, double ny, double sigma)
{
    double K[2];
    double R = sigma*sigma;
    double d = track[index].Px[0][0]+R;
    // shortcut to avoid computation
    double fac = R/d;
    // Kalman gain matrix
    K[0] = track[index].Px[0][0]/d;
    K[1] = track[index].Px[1][0]/d;
    // update covariance matrix
    track[index].Px[0][0] *= fac;
    track[index].Px[1][0] *= fac;
    track[index].Px[0][1] *= fac;
    track[index].Px[1][1] *= fac;
    // update state with new data
    track[index].x[1] += K[1]*(nx-track[index].x[0]);
    track[index].x[0] += K[0]*(nx-track[index].x[0]);

    d = track[index].Py[0][0]+R;
    // shortcut to avoid computation
    fac = R/d;
    // Kalman gain matrix
    K[0] = track[index].Py[0][0]/d;
    K[1] = track[index].Py[1][0]/d;
    // update covariance matrix
    track[index].Py[0][0] *= fac;
    track[index].Py[1][0] *= fac;
    track[index].Py[0][1] *= fac;
    track[index].Py[1][1] *= fac;
    // update state with new data
    track[index].y[1] += K[1]*(ny-track[index].y[0]);
    track[index].y[0] += K[0]*(ny-track[index].y[0]);
}

void LadarTracker::updateAvg(int index)
{
    LadarSegment& segment = track[index].segment[0];
    double dt = LADAR_DT * (segment.blobId - track[index].segment[1].blobId);
    vec3_t vel;
    // number of associated segments that we want to look at
    int n = (track[index].numAssoc > TRACK_VEL_SEGS)?TRACK_VEL_SEGS:track[index].numAssoc;
    double dxavg,dyavg;
    // compute average dx's, dy's
    dxavg = (segment.center.x-track[index].segment[n-1].center.x)/n;
    dyavg = (segment.center.y-track[index].segment[n-1].center.y)/n;
    // compute variances in dx, dy
    double varx=0, vary=0;
    double d;
    for (int i=0;i<n-1;i++)
    {
	d = fabs((track[index].segment[i].center.x - track[index].segment[i+1].center.x) - dxavg);
	varx += d*d;
	d = fabs((track[index].segment[i].center.y - track[index].segment[i+1].center.y) - dyavg);
	vary += d*d;
    }
    // yeah, i changed it to std. dev but left the variable names. sue me.
    varx = sqrt(varx)/sqrt(n);
    vary = sqrt(vary)/sqrt(n);
    double var = sqrt(varx*varx + vary*vary);
    // scale down things with high variance
    if (var > TRACK_MAX_VAR)
	var = 0.5; //TRACK_MAX_VAR;
    else
	var = 1;
    // scale down things with rapid geometry change
    if (abs((segment.end-segment.start)-(track[index].segment[n-1].end-
					 track[index].segment[n-1].start))
	> TRACK_CHANGE_POINTS)
	var *= 0.5;
    // now scale velocity by opposite of variance
//    var = 1 - var/TRACK_MAX_VAR;
    vel = vec3_sub(segment.center,track[index].segment[n-1].center);
    vel = vec3_scale(var/dt,vel);
    // limit vel to be reasonable
    vel.x = MIN(vel.x,TRACK_MAX_VEL);
    vel.y = MIN(vel.y,TRACK_MAX_VEL);
    vel.x = MAX(vel.x,-TRACK_MAX_VEL);
    vel.y = MAX(vel.y,-TRACK_MAX_VEL);
    // update averaged velocity
    track[index].velocity.x = vel.x; //track[index].velocity.x;//*(1-TRACK_VEL_WEIGHT) + vel.x*TRACK_VEL_WEIGHT;
    track[index].velocity.y = vel.y; //track[index].velocity.y;//*(1-TRACK_VEL_WEIGHT) + vel.y*TRACK_VEL_WEIGHT;
    track[index].x[0] = segment.center.x;
    track[index].y[0] = segment.center.y;
    track[index].x[1] = track[index].velocity.x;
    track[index].y[1] = track[index].velocity.y;
}

void LadarTracker::updateCarConf(int index)
{
    LadarSegment& segment = track[index].segment[0];
    track[index].carConf = 0;
    // check size of segment (rough calculation)
    double size = vec3_mag(vec3_sub(segment.left,segment.right));
    if (size < CCONF_MAX_SIZE)
        track[index].carConf |= CAR_SIZE;
//    else
//        track[index].carConf += CCONF_SIZE_INC;

    // reflectivity
    if (segment.reflective)
        track[index].carConf |= CAR_REFL;

    // corner computation (basically, noise to remove bushes/trees)
    vec3_t v,w;
    double c1,c2,dist;
    int nCorners = 0;
    for (int i=segment.start+1;i<segment.end-1;i++)
    {
	// can't do if we have a no-return
	if ((segment.scan->blob.points[i-1][RANGE] > MAX_LADAR_DIST) ||
	    (segment.scan->blob.points[i][RANGE] > MAX_LADAR_DIST) ||
	    (segment.scan->blob.points[i+1][RANGE] > MAX_LADAR_DIST))
	    continue;
	// compute distance
	v = vec3_unit(vec3_sub(segment.scan->points[i+1],segment.scan->points[i]));
	w = vec3_unit(vec3_sub(segment.scan->points[i],segment.scan->points[i-1]));
	//v.z = w.z = 0;
	//c1 = vec3_dot(w,v);
	//c2 = vec3_dot(v,v);
	//v = vec3_scale((c1/c2),v);
	//dist = vec3_mag(vec3_sub(v,w));
	//if (dist > CCONF_CORNER_DIST)
	//    nCorners++;
	if (vec3_dot(v,w) < 0)
	    nCorners++;
    }
    if (nCorners <= CCONF_CORNER_LOW)
	track[index].carConf |= CAR_CORNERS;
    //track[index].carConf += CCONF_CORNER_INC;
//    if (nCorners >= CCONF_CORNER_HIGH)
//	track[index].carConf -= CCONF_CORNER_DEC;
    
/*    if (track[index].carConf < 0)
	track[index].carConf = 0;
    if (track[index].carConf > CCONF_MAX)
    rack[index].carConf = CCONF_MAX;*/
}

void LadarTracker::createTrack(LadarSegment& segment)
{
    if (emptyTrack == -1)
    {
	fprintf(stderr,"Tried to create too many tracks!\n");
	return;
    }
    if (track[emptyTrack].status != 0)
    {
	fprintf(stderr,"Tried to overwrite existing track %d with status %d\n",
		emptyTrack, track[emptyTrack].status);
	return;
    }

    track[emptyTrack].status = TRACK_INIT_STATUS;
    track[emptyTrack].numAssoc = 1;
    track[emptyTrack].segment[0] = segment;
    track[emptyTrack].velocity.x = track[emptyTrack].velocity.y = 0;
    track[emptyTrack].x[0] = segment.center.x;
    track[emptyTrack].y[0] = segment.center.y;
    track[emptyTrack].x[1] = track[emptyTrack].y[1] = 0;
    track[emptyTrack].numReflective = (segment.reflective)?1:0;
    track[emptyTrack].carConf = 0;
    updateCarConf(emptyTrack);
    if (emptyTrack > highestTrack)
	highestTrack = emptyTrack;
    emptyTrack = track[emptyTrack].nextEmptyId;
    numTracks++;
}

void LadarTracker::deleteTrack(int index)
{
    track[index].status = 0;
    numTracks--;
}

void LadarTracker::updateTrackList()
{
    int lastEmpty = 0;
    emptyTrack = -1;
    // update list of empty tracks
    for (int i=0;i<MAX_TRACKS;i++)
    {
	if (track[i].status == 0)
	{
	    // set this->emptyTrack to first empty track
	    if (emptyTrack < 0)
		emptyTrack = i;
	    track[lastEmpty].nextEmptyId = i;
	    lastEmpty = i;
	}
	else
	{
	    // set highest active track
	    highestTrack = i;
	}
    }
    // set last empty track thing
    track[MAX_TRACKS-1].nextEmptyId = -1;
}

void LadarTracker::clearTracks()
{
    memset(this->track,0,sizeof(TrackedSegment)*MAX_TRACKS);
}

void LadarTracker::sendDebug(CMapElementTalker& talker, int group)
{
    MapElement el;
    MapElement elVel;
    
    el.setFrameTypeLocal();
    el.setState(scans->getPastScan(ladarId,0)->blob.state);

    elVel.setFrameTypeLocal();
    elVel.plotColor = MAP_COLOR_RED;

    int lastBlobId = scans->getPastScan(ladarId,0)->blobId;
    
    // display points n stuff for temps
    for (int i=0;i<highestTrack+10;i++)
    {
	el.setId(this->ladarId,i);
	elVel.setId(this->ladarId,i+MAX_TRACKS);
	if (track[i].status <= 0)
	{
	    el.setTypeClear();
	    elVel.setTypeClear();
	    talker.sendMapElement(&el,group);
	    talker.sendMapElement(&elVel,group);
	    continue;
	}
	el.setTypeLine();
	elVel.setTypeLine();

	if (track[i].status < 10)
	    continue;//el.plotColor = MAP_COLOR_YELLOW;
	if (track[i].segment[0].start==track[i].segment[0].end)
	    continue;
//	track[i].carConf = 10;
//	updateCarConf(i);

//	if (track[i].status > 10)
	el.plotColor = MAP_COLOR_GREEN;
	if (track[i].status < 30)
	    el.plotColor = MAP_COLOR_YELLOW;
	// unless:
//	if (track[i].segment[0].blobId != lastBlobId)
//	    el.plotColor = MAP_COLOR_RED;
	
	if (track[i].carConf == 0)
	    el.plotColor = MAP_COLOR_RED;

	point2arr ptarr = getSegmentPoints(track[i].segment[0]);
	el.setGeometry(ptarr);
	el.velocity = point2(track[i].x[1],track[i].y[1]);
	el.conf = track[i].status;
	el.typeConf = track[i].carConf;

	point2 pt;
	pt.set(track[i].x[0],track[i].y[0]);
	ptarr.clear();
	ptarr.push_back(pt);
//	ptarr.push_back(pt + 10*point2(sqrt(track[i].Px[0][0]),sqrt(track[i].Py[0][0])));
	ptarr.push_back(pt + point2(track[i].x[1],track[i].y[1]));
	elVel.setGeometry(ptarr);
	talker.sendMapElement(&el,group);
	talker.sendMapElement(&elVel,group);
    }
}

point2arr LadarTracker::getSegmentPoints(LadarSegment& segment)
{
    point2arr ptarr;
  if (segment.scan->blobId != segment.blobId)
    {
      fprintf(stderr,"Inaccessible geometry requested!\n");
      int n = 0;
      //      int f = 1 / n;
      return ptarr;
    }
    for (int i=segment.start;i<=segment.end;i++)
    {
	if (segment.scan->blob.points[i][RANGE] < MAX_LADAR_DIST)
	    ptarr.push_back(point2(segment.scan->points[i].x,segment.scan->points[i].y));
    }
  if (segment.scan->blobId != segment.blobId)
    {
      fprintf(stderr,"Inaccessible geometry requested!\n");
      ptarr.clear();
    }
    return ptarr;
}
