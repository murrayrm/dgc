#include "ProcessPoints.hh"

void ProcessPoints::ProcessScan(LadarScan *scan, float l2m[4][4])
{
    float mat[4][4];
    float sx,sy,sz;

    float x[NUM_GS_POINTS];
    float y[NUM_GS_POINTS];

    mat44f_mul(mat,scan->blob.veh2loc,scan->blob.sens2veh);

    // First just go through and transform points, why not
    for (int i=0;i<scan->numPoints;i++)
    {
	LadarRangeBlobScanToSensor(&(scan->blob), scan->blob.points[i][ANGLE],
				   scan->blob.points[i][RANGE], &sx, &sy, &sz);
	// sz is zero, always, so can ignore
	scan->points[i].x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
	scan->points[i].y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
	scan->points[i].z = mat[2][0]*sx + mat[2][1]*sy + mat[2][3];

	scan->lastElementId[i] = -1;
    }

    // Make sure we account for the ladar's center as well
    scan->ladarPos.x = mat[0][3];
    scan->ladarPos.y = mat[1][3];
    scan->ladarPos.z = mat[2][3];

    // a roof ladar
    if (scan->ladarId == 4 || scan->ladarId == 5)
    {
	// update local/roof transform
	memcpy(this->local2roof,l2m,sizeof(float)*16);
	// scale for roofmap
	local2roof[0][0] /= ROOFMAP_FRAC;
	local2roof[0][1] /= ROOFMAP_FRAC;
	local2roof[1][0] /= ROOFMAP_FRAC;
	local2roof[1][1] /= ROOFMAP_FRAC;
	local2roof[0][3] /= ROOFMAP_FRAC;
	local2roof[1][3] /= ROOFMAP_FRAC;
	mat44f_inv(this->roof2local,this->local2roof);
	// get to drawing in pixel coords
	mat44f_mul(mat,this->local2roof,mat);
	setRoofPoly(scan,mat);
    }

    if (scan->ladarId > 3) // not bumper ladar, dun care 'bout groundstrikes
	return;

//    LadarScan *pscan[NUM_GS_POINTS];
//    for (int i=0;i<NUM_GS_POINTS;i++)
//	pscan[i] = scans->getPastScan(scan->ladarId, i);

    //float a,b,siga,sigb,chi2;
    double zavg, zdiff;
    int mx, my;
    emap_t* roofData = roofMap->getData();
    emap_t roofVal;
    LadarScan *pscan = scans->getPastScan(scan->ladarId,1);
    quat_t rot = quat_from_rpy(scan->blob.state.localRoll-pscan->blob.state.localRoll,
			       scan->blob.state.localPitch-pscan->blob.state.localPitch,
			       scan->blob.state.localYaw-pscan->blob.state.localYaw);
    vec3_t vehDelta = vec3_rotate(rot,vec3_set(0,0,-1));
    vehDelta = vec3_sub(vehDelta,vec3_set(0,0,-1));
    vec3_t vehCenter = vec3_set(scan->blob.state.localX,scan->blob.state.localY,
				scan->blob.state.localZ);
    double dynDot;

    // scan is now populated, can start looking at groundstrikes
    for (int i=0;i<scan->numPoints;i++)
    {
/*	for (int j=0;j<NUM_GS_POINTS;j++)
	{
	    x[j] = pscan[j]->blob.points[i][RANGE];
	    y[j] = pscan[j]->points[i].z;
	}
	fit(x,y,NUM_GS_POINTS,&a,&b,&siga,&sigb,&chi2);
	scan->gsDynConf[i] = chi2;
	if (b > 0.2)
	    scan->gsDynConf[i] = 1;
	if (chi2 < 0)
	    scan->gsDynConf[i] = 1;
	if (siga > 0.01)
	scan->gsDynConf[i] = 1;*/

	//// Compute dynamic confidence
	// check if we're rolling/pitching towards or away from ground
	dynDot = vec3_dot(vehDelta,vec3_sub(scan->points[i],vehCenter));
	// and multiply by negative change in range
	dynDot *= -(scan->blob.points[i][RANGE] - pscan->blob.points[i][RANGE]);
	if (dynDot > 0)
	{
	    if (gsDynamics[scan->ladarId][i] < GS_DYN_MAX)
		gsDynamics[scan->ladarId][i] += GS_DYN_INC;
	}
	else
	{
	    if (gsDynamics[scan->ladarId][i] >= GS_DYN_DEC)
		gsDynamics[scan->ladarId][i] -= GS_DYN_DEC;
	}
	// now set confidence based on this tracked value
	scan->gsDynConf[i] = (gsDynamics[scan->ladarId][i]/(double)GS_DYN_MAX);

	//// Compute road confidence
	mx = (int)(scan->points[i].x*local2road[0][0] + local2road[0][3]);
	my = (int)(scan->points[i].y*local2road[1][1] + local2road[1][3]);

	// find avg height
	this->roadMap->avgRectangleNoZero(mx-GS_RECT,my-GS_RECT,mx+GS_RECT,my+GS_RECT,RMAP_CLEAR, RMAP_NOTROAD,zavg);
	// no data?
	if (zavg == 0)
	{
	    scan->gsRoadConf[i] = 0;
	} else { // have data
	    // transform back and find difference
	    zavg = road2local[2][2]*zavg + road2local[2][3];
	    // get height difference
	    zdiff = fabs(zavg-scan->points[i].z);
	    if (zdiff < MAX_GS_DIST) {
	      scan->gsRoadConf[i] = 1.0 - zdiff / MAX_GS_DIST;
	      //this is just plain wrong...
	      //scan->gsRoadConf[i] = (float)(zavg/MAX_GS_DIST);
	    } else {
		scan->gsRoadConf[i] = 0; 
	    }
	}

	//// Compute roof confidence
	mx = (int)(scan->points[i].x*local2roof[0][0] + local2roof[0][3]);
	my = (int)(scan->points[i].y*local2roof[1][1] + local2roof[1][3]);
	// make sure we are on map
	if (mx >= 0 && mx < ROOFMAP_SIZE && my >= 0 && my < ROOFMAP_SIZE)
	    roofVal = roofData[my*ROOFMAP_SIZE+mx];
	else
	    roofVal = 0;

	if (roofVal > 2) // inside coverage and freespace
	    scan->gsRoofConf[i] = ROOF_FREESPACE;
	else if (roofVal > 0) // inside coverage, but not fs
	    scan->gsRoofConf[i] = ROOF_OUTSIDE;
	else // outside coverage, no data
	    scan->gsRoofConf[i] = ROOF_NO_DATA;
    }
}

void ProcessPoints::setRoofPoly(LadarScan *scan,float mat[4][4])
{
    float sx,sy,sz;
    float lastpr,curpr,nextpr;

    emap_t color = (scan->ladarId == 4)?RMC_LEFT:RMC_RIGHT;
    Point2 pts[LADAR_BLOB_MAX_POINTS+1];
    
    // make coverage polygon
    pts[0].x = mat[0][3];
    pts[0].y = mat[1][3];
    // three representative points
    LadarRangeBlobScanToSensor(&(scan->blob), -M_PI/2, 60, &sx, &sy, &sz);
    pts[1].x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
    pts[1].y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
    LadarRangeBlobScanToSensor(&(scan->blob), 0, 60, &sx, &sy, &sz);
    pts[2].x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
    pts[2].y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
    LadarRangeBlobScanToSensor(&(scan->blob), M_PI/2, 60, &sx, &sy, &sz);
    pts[3].x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
    pts[3].y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];

    // erase ladar from map
    this->roofMap->eraseand(color);
    // draw coverage polygon
    this->roofMap->drawPolygon(4,pts,color);

    // make main polygon
    lastpr = scan->blob.points[0][RANGE];
    curpr = scan->blob.points[1][RANGE];
    for (int i=1;i<scan->numPoints-1;i++)
    {
	// dilate foreground objects
	nextpr = scan->blob.points[i+1][RANGE];
	if ((curpr-lastpr)>ROOF_LADAR_DILATE)
	    curpr = lastpr;
	else if ((curpr-nextpr)>ROOF_LADAR_DILATE)
	    curpr = nextpr;
	curpr -= ROOF_LADAR_DELTA;
	LadarRangeBlobScanToSensor(&(scan->blob), scan->blob.points[i][ANGLE],
				   curpr, &sx, &sy, &sz);
	pts[i].x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
	pts[i].y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
	lastpr = scan->blob.points[i][RANGE];
	curpr = nextpr;
    }
    // draw polygon
    color = (scan->ladarId == 4)?RM_LEFT:RM_RIGHT;
    this->roofMap->drawPolygon(scan->numPoints-1,pts,color);
}

void ProcessPoints::UpdateRoadMap(EMap *rmap, float* l2m)
{
    this->roadMap->copyFrom(rmap);
    memcpy(this->local2road,l2m,sizeof(float)*16);
    mat44f_inv(this->road2local,this->local2road);
}
