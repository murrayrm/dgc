#include "LadarPerceptor.hh"
#include <frames/mat44.h>
#include <frames/vec3.h>

#ifndef _ROADMAP_HH
#define _ROADMAP_HH

using namespace std;

#define RMAP_CLEAR 0xFFFF
#define RMAP_ROAD 0x8000
#define RMAP_NOTROAD 0xFFFE
#define RMAP_INVALID 100000

// vertical meters per emap_t unit
#define RMAP_VRES 0.02f
#define RMAP_VCENTER 0x8000

#define GROUND_LADARS 2

#define MAX_ROAD_GAP_PTU 0.14
#define MAX_ROAD_GAP_RIEGL 0.005

#define MIN_SEG_POINTS_PTU 20
#define MIN_SEG_POINTS_RIEGL 35

#define MAX_BUMP_WIDTH 6
#define MAX_BUMP_HEIGHT 0.10

#define GAP_WIDTH_RIEGL 6
#define GAP_WIDTH_PTU 5

// maximum slope, 30 cm/m
#define MAX_ROAD_SLOPE 0.3f

#define GP_SIZE 1
#define MAX_TRI_SIZE 10

class RoadMap
{
private:
    EMap *map;
    int shiftX, shiftY; //for drawing between consecutive scans
    int lastScan[GROUND_LADARS][LADAR_BLOB_MAX_POINTS][2];
    float local2map[4][4];
    float map2local[4][4];
    float mapOffsetZ;

public:
    RoadMap();
    ~RoadMap();

    EMap* getMap() 
    {
	return this->map;
    }

    float* getLocal2Map()
    {
	return &(local2map[0][0]);
    }

    void setOffsetZ(float offset)
    {
	this->mapOffsetZ = offset;
    }

    void shiftMap(int dx, int dy, float l2m[4][4]);
    void drawScan(LadarScan *scan, LadarScan *scan1, LadarScan *scan2);
};

#endif
