
/* 
 * Desc: Ladar obstacle viewer utility
 * Date: 17 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>

#include "scanmap.h"

#include "cmdline.h"


class App
{
  public:

  // Default constructor
  App();

  // Destructor
  ~App();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Speed callback
  static void onSpeed(Fl_Widget *w, int option);
  
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, App *self);

  // Handle idle callbacks
  static void onIdle(App *self);

  public:

  // Switch to the vehicle frame
  void pushFrameVehicle(VehicleState state);

  // Revert to previous frame
  void popFrame();

  // Draw a set of axes
  void drawAxes(float size);

  // Draw the robot
  void drawAlice();

  // Draw free space
  void predrawFree(gpc_polygon *poly);

  // Draw the obstacle map
  void predrawObs(scanmap_t *scanmap);

  public:

  // Initialize the perceptor
  int init();

  // Finalize the perceptor
  int fini();

  // Update the perceptor
  int update();
  
  public:

  // Command-line options
  struct gengetopt_args_info options;

  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;

  // Should we advance a single step?
  bool step;
  
  // Should we quit?
  bool quit;

  public:
  
  // Log replay module
  sensnet_replay_t *replay;

  // Current state data
  VehicleState state;
  
  // Current blob data
  LadarRangeBlob blob;

  // Fused scan map
  scanmap_t *scanmap;

  // Starting time for current fuse window
  uint64_t fusetime;

  // Display lists
  int freeList, obsList;
};



// Commands
enum
{
  APP_ACTION_PAUSE = 0x1000,
  APP_ACTION_STEP,
};


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Default constructor
App::App()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
App::~App()
{
  return;
}


// Parse command-line options
int App::parseCmdLine(int argc, char **argv)
{
  // Run parser
  if (cmdline_parser(argc, argv, &this->options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }

  // Make sure we have a log file to replay
  if (this->options.inputs_num < 1)
  {
    cmdline_parser_print_help();
    return -1;
  }

  return 0;
}


// Initialize stuff
int App::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) App::onExit},
      {0},
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) App::onAction, (void*) APP_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Single step", '.', (Fl_Callback*) App::onAction, (void*) APP_ACTION_STEP},
      {0},
      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Ladar Blob Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows - 30, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  this->worldwin->set_clip(10, 1000);

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);
  
  return 0;
}


// Finalize stuff
int App::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void App::onAction(Fl_Widget *w, int option)
{
  App *self;  

  self = (App*) w->user_data();
  if (option == APP_ACTION_PAUSE)
    self->pause = !self->pause;
  if (option == APP_ACTION_STEP)
    self->step = true;
  
  return;
}



// Handle menu callbacks
void App::onExit(Fl_Widget *w, int option)
{
  App *self;

  self = (App*) w->user_data();
  self->quit = true;

  return;
}


// Handle draw callbacks
void App::onDraw(Fl_Glv_Window *win, App *self)
{
  glPushMatrix();
  glTranslatef(-self->state.localX, -self->state.localY, -self->state.localZ);
  
  // Switch to vehicle frame to draw axes
  self->pushFrameVehicle(self->state);
  self->drawAxes(1.0);
  self->drawAlice();
  self->popFrame();

  glCallList(self->freeList);
  glCallList(self->obsList);
  
  glPopMatrix();
  
  return;
}


// Switch to the vehicle frame
void App::pushFrameVehicle(VehicleState state)
{
  pose3_t pose;
  pose.pos = vec3_set(state.localX, state.localY, state.localZ);
  pose.rot = quat_from_rpy(state.localRoll, state.localPitch, state.localYaw);

  float m[4][4];
  pose3_to_mat44f(pose, m);  

  // Transpose to column-major order for GL
  int i, j;
  float t[16];
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);
  
  return;
}


// Revert to previous frame
void App::popFrame()
{
  glPopMatrix();  
  return;
}


// Draw a set of axes
void App::drawAxes(float size)
{
  // Show camera origin
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(size, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, size, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, size);
  glEnd();

  return;
}


// Draw the robot
void App::drawAlice()
{
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
    
  return;
}


// Draw the free-space
void App::predrawFree(gpc_polygon *poly)
{
  int i, j;
  gpc_vertex_list *contour;
  gpc_vertex *vertex;
  float m[4][4];
  float lx, ly, lz;
  
  if (this->freeList == 0)
    this->freeList = glGenLists(1);
  glNewList(this->freeList, GL_COMPILE);

  mat44f_setf(m, scanmap->map2loc);

  glColor3f(0, 0, 1);
  for (i = 0; i < poly->num_contours; i++)
  {      
    contour = &poly->contour[i];

    glBegin(GL_LINE_LOOP);
    for (j = 0; j < contour->num_vertices; j++)
    {
      vertex = &contour->vertex[j];
      lx = m[0][0]*vertex->x + m[0][1]*vertex->y + m[0][3];
      ly = m[1][0]*vertex->x + m[1][1]*vertex->y + m[1][3];
      lz = m[2][0]*vertex->x + m[2][1]*vertex->y + m[2][3];
      glVertex3f(lx, ly, lz);
    }
    glEnd();
  }

  glEndList();
    
  return;
}


// Draw the obstacle map
void App::predrawObs(scanmap_t *scanmap)
{
  int i, j;
  float mx, my;
  float lx, ly, lz;
  float m[4][4];
  scanmap_cell_t *cell;

  if (this->obsList == 0)
    this->obsList = glGenLists(1);
  glNewList(this->obsList, GL_COMPILE);

  mat44f_setf(m, scanmap->map2loc);

  glColor3f(1, 0, 0);
  glBegin(GL_QUADS);
  
  for (j = 0; j < scanmap->map_size; j++)
  {
    for (i = 0; i < scanmap->map_size; i++)
    {
      cell = scanmap_cell_index(scanmap, i, j);
      if (cell->hits < 2)  // MAGIC
        continue;

      /*
        if (cell->hits == 1)
        glColor3f(1, 0, 1);
        else
        glColor3f(1, 0, 0);
      */

      //int k;
      //k = 256 * cell->hits / 8;
      //glColor3ub(k, 255 - k, 0);
        
      scanmap_i2p(scanmap, i, j, &mx, &my);

      lx = m[0][0]*mx + m[0][1]*my + m[0][3];
      ly = m[1][0]*mx + m[1][1]*my + m[1][3];
      lz = m[2][0]*mx + m[2][1]*my + m[2][3];

      glVertex3f(lx - scanmap->map_scale/2, ly - scanmap->map_scale/2, lz);
      glVertex3f(lx + scanmap->map_scale/2, ly - scanmap->map_scale/2, lz);
      glVertex3f(lx + scanmap->map_scale/2, ly + scanmap->map_scale/2, lz);
      glVertex3f(lx - scanmap->map_scale/2, ly + scanmap->map_scale/2, lz);
    }
  }

  glEnd();

  glEndList();
  
  return;
}


// Handle idle callbacks
void App::onIdle(App *self)
{  
  if (!self->pause || self->step)
  {  
    // Update perceptor
    if (self->update() != 0)
      self->quit = true;

    // Redraw the display
    self->worldwin->redraw();
    
    // Single step mode: we've moved one step, so clear flag
    if (self->step)
      self->step = false;
  }
  else
  {
    // Sleepy bye-bye
    usleep(0);
  }
  
  return;
}


// Initialize the perceptor
int App::init()
{
  /*
  // Open the log
  sensnet_log_header_t header;
  this->log = sensnet_log_alloc();
  assert(this->options.inputs_num > 0);
  if (sensnet_log_open_read(this->log, this->options.inputs[0], &header) != 0)
    return ERROR("unable to open log %s", this->options.inputs[0]);
  */

  /* REMOVE
  // TESTING
  // Seek to start location
  uint64_t timestamp;
  if (sensnet_log_peek(this->log, &timestamp, NULL, NULL, NULL, NULL) != 0)
    return ERROR("peek failed");
  timestamp += 118 * 1000000;
  if (sensnet_log_seek(this->log, timestamp) != 0)
    return ERROR("seek failed on timestamp %lld", timestamp);
  */

  this->replay = sensnet_replay_alloc();
  if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
    return ERROR("unable to open one or more log files");
  
  // Create map
  this->scanmap = scanmap_alloc();
  assert(this->scanmap);
  
  return 0;
}


// Finalize the perceptor
int App::fini()
{
  scanmap_free(this->scanmap);
  this->scanmap = NULL;

  if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;  
}


// Update the perceptor
int App::update()
{
  int sensor_id, blob_type, blob_id;
  uint64_t time;

  assert(this->replay);
  
  // Read data from the logs
  sensnet_replay_next(this->replay, 0);
  sensnet_replay_query(this->replay, NULL, &sensor_id, &blob_type);
  if (sensnet_replay_read(this->replay, sensor_id, blob_type,
                          &blob_id, sizeof(this->blob), &this->blob) != 0)
    return -1;  

  // Ignore bogus state values
  if (this->blob.state.timestamp < 1)
    return 0;

  // Record the vehicle state
  if (this->blob.state.timestamp > this->state.timestamp)
    this->state = this->blob.state;
  
  printf("blob %d %d %lld\n", sensor_id, blob_id, this->blob.timestamp);
  
  // If we have accumulated enough data, time to fuse the scans
  if (this->blob.timestamp - this->fusetime > (uint64_t) 1000000/this->options.rate_arg)
  {
    MSG("fusing");
    this->fusetime = this->blob.timestamp;
    
    // Fuse maps
    time = DGCgettime();
    scanmap_fuse(this->scanmap);
    MSG("fuse %d", (int) (DGCgettime() - time));

    // Update display lists
    this->predrawFree(&this->scanmap->fused_space);
    this->predrawObs(this->scanmap);

    // Refresh display
    this->mainwin->redraw();
    
    time = DGCgettime();
    scanmap_clear(this->scanmap, &this->state);
    MSG("clear  %d", (int) (DGCgettime() - time));
  }

  // Update the map
  //time = DGCgettime();
  scanmap_add_obs(this->scanmap, &this->blob,
                  this->options.max_range_arg,
                  this->options.min_bearing_arg * M_PI/180,
                  this->options.max_bearing_arg * M_PI/180);
  //MSG("update %d", (int) (DGCgettime() - time));
     
  return 0;
}




int main(int argc, char *argv[])
{
  App *app;

  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new App();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  if (app->init() != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) App::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
