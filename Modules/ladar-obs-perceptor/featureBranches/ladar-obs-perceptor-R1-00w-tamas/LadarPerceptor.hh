#define NUM_POINTS 200
#define NUM_LADARS 4

#define ANGLE 0
#define RANGE 1

#ifndef _LADARPERCEPTOR_HH
#define _LADARPERCEPTOR_HH

#include <assert.h>
#include <float.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <unistd.h>


#include <ncurses.h>
#include <cotk/cotk.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/LadarRangeBlob.h>

#include <interfaces/MapElementMsg.h>
#include <map/MapElement.hh>

#include "LadarCar.hh"

#include "cmdline.h"
#include "Log.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

class LadarPerceptor
{  
public:
    
    // Constructor
    LadarPerceptor();
    
    // Destructor
    ~LadarPerceptor();
    
public:
    
    // Parse the command line
    int parseCmdLine(int argc, char **argv);
    
    // Initialize sensnet
    int initSensnet();
    
    // Clean up sensnet
    int finiSensnet();
    
public:
    
    // Initialize console display
    int initConsole();
    
    // Finalize console display
    int finiConsole();
    
    // Console button callback
    static int onUserQuit(cotk_t *console, LadarPerceptor *self, const char *token);
    
    // Console button callback
    static int onUserPause(cotk_t *console, LadarPerceptor *self, const char *token);
    
public:
    
    // Initalize map
    int init();
    
    // Finalize map
    int fini();
    
    // Update the process state
    int updateProcessState();

public:
    
    // Check for new data and update maps
    int update();

    // update current scan
    void updateScan(LadarRangeBlob *blob);
    
public:
    
    // Program options
    gengetopt_args_info options;
    
    // Spread settings
    char *spreadDaemon;
    int skynetKey;
    modulename moduleId;
    int debugSubgroup;
    
    // Operation mode
    enum {modeLive, modeReplay} mode;
    
    // Sensnet module
    sensnet_t *sensnet;
    
    // Sensnet replay module
    sensnet_replay_t *replay;
    
    //logging options
    bool logging;
    int logLevel;

    // Console interface
    cotk_t *console;
    
    // Should we quit?
    bool quit;
    
    // Should we pause?
    bool pause;
        
    // Current state data
    VehicleState state;

    //*** ladar stuff ***
    struct LadarData
    {
	sensnet_id_t id;
	int blobId;
	LadarRangeBlob blob;
	bool newData;
    };

    LadarData ladarData[NUM_LADARS];

    // Anomalies caught
    int anomalyCount;

    // Time of last process update
    uint64_t processTime;
    
    // Time of last outgoing message
    uint64_t sendTime;
    
    // Performance stats
    uint64_t addCount, addTime;

    // *******************************************
    // *** Perceptor-specific stuffs goes here ***
    // *******************************************

    LadarCarTracker *tracker;
    LadarScan *curScan;

    // the source ladar and point id
    // for each of our mapped points
    int sourceLadar[NUM_POINTS];
    int sourceId[NUM_POINTS];
    // whether we have mapped them yet or not
    bool sourceMapped;
};

#endif
