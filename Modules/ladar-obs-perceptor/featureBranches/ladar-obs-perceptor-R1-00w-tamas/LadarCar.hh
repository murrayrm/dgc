#include "LadarPerceptor.hh"
#include <frames/mat44.h>
#include <frames/quat.h>
#include <frames/pose3.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

#ifndef LADARCAR_HH
#define LADARCAR_HH

struct LadarScan
{
    uint64_t timestamp; // when it was sent
    float range[NUM_POINTS]; // range of points
    float angle[NUM_POINTS]; // angle
    vec3_t pos[NUM_POINTS]; // position in local frame
    int numPoints; // the number of points we have
    int segment[NUM_POINTS]; // the segment id of this scan
    vec3_t ladarPos; // position of this ladar in veh. frame
    VehicleState state; // state of alice for this scan
    int ladarId; // some identifying number
};

struct CarShape
{
    vec3_t start;
    vec3_t end;
    vec3_t corner; // the closest corner of the shape
    double angle; // orientation of the first segment, pointing towards corner

    int startind; // index of start
    int endind; // index of end

    bool startvis; // is corner a obscured?
    bool endvis; // is corner b obscured?

    double fit; // quality of fit
};

struct TrackedCar
{
    int status; // integer status, 'aliveness'
    int nextEmptyId; // index of next empty track

    // previous time we updated (local time)
    uint64_t lastUpdate;
    // status we had last time we updated
    int lastUpdateStatus;
    // last point that was assoc. with corner (0-3)
    int lastCorner;
    // corner points
    double corner[4][2];
    // predicted corner points
    double predcorner[4][2];

    // information for KF initialization
    uint64_t firstSeen;
    
    // Kalman filtering stuff
    // position
    double x[2];
    double y[2];
    // size
    double width;
    double length;
    // orientation
    double angle[2];
    // and covariance matrices
    double Px[2][2];
    double Py[2][2];
    double Pw;
    double Pl;
    double Pa[2][2];
    // predicted position/angle
    double nx;
    double ny;
    double na;
    // predicted angular velocities
    double nvx;
    double nvy;
};

//****** Finding defines

// No-return distance of ladars (m)
#define MAX_LADAR_DIST 80.0
// Minimum segmentation distance (m)
#define MAP_SEG_MIN 1.5
// Coefficient based on distance to add to above value
#define MAP_SEG_COEFF 0.02
// Max number of points to skip over when merging
#define NR_GAP_POINTS 4
// And max dist between end points (m)
#define NR_GAP_DIST 0.7
// Fitting threshold to immediately discard stuff (avg. m^2)
#define FIT_THRESH 0.25
// Initial angle step to use for optimization (rad)
#define INIT_DA 0.4
// Number of iterations
#define ANGLE_ITER 4
// Limit for small maxdist (useful, huh?)
#define SMALL_MAXDIST 0.1

//****** Tracking defines

// Number of tracks
#define NUM_TRACKS 500
// Time between ladar scans
#define LADAR_DT 1.0/75.0
// Time to remove status (in uS); just over LADAR_DT
#define MAX_TIME 15000
// Initial status value
#define INIT_STATUS 5
// Maximum status value
#define MAX_STATUS 200
// Maximum distance to associate over
#define MAX_ASSOC_DIST 1
// Instant reject distance
#define REJECT_DIST 6

/*** Kalman filter stuff ***/
// for velocity/orientation
#define KF_VEL_NOISE 0.05
#define KF_ANG_NOISE 0.1
#define KF_VEL_INIT 0.3
#define KF_ANG_INIT 0.3
// for width/height
#define KF2_PROC_NOISE 0.2
#define KF2_COV_INIT 0.5
// coeffs
#define KF_ANG_SIGMA 15
#define KF_MAX_VSIGMA 2
// time until velocity sigma reaches max
#define KF_VEL_INIT_TIME 2
// minimum width/length dimension
#define MIN_WIDTH 0.5

// debug rate to send at (= 10 Hz)
#define SEND_DEBUG_RATE 100000

// This is the main class that does car finding and tracking.
class LadarCarTracker
{
    CMapElementTalker debugTalker;
    int debugSubgroup;
    int moduleId;
    int skynetKey;
    bool sendDebug;
    uint64_t lastDebugTime;
    TrackedCar track[NUM_TRACKS];
    int emptyTrack; // id of next empty track
    int highestTrack; // upper limit of loop

public:
    LadarCarTracker(modulename module, int skynetKey, int debugSubgroup)
    {
	this->skynetKey = skynetKey;
	this->moduleId = (int)module;
	this->debugSubgroup = debugSubgroup;
	this->lastDebugTime = DGCgettime();
	this->sendDebug = (debugSubgroup != 1);
	if (this->sendDebug)
	    this->debugTalker.initSendMapElement(skynetKey,module);
	memset(track,0,NUM_TRACKS*sizeof(TrackedCar));
	emptyTrack = 0;
	highestTrack = 0;
	CleanTracks();
    }

    ~LadarCarTracker()
    {
    }

    // main function, does everything it needs to with a new scan
    void ProcessScan(LadarScan *scan);

private:

/************ CAR FINDING ************/
    
    // split the scan up into discontinuous segments
    void SegmentScan(LadarScan *scan);

    // find possible cars in current scan
    vector<CarShape> FindCars(LadarScan *scan);

    // fit car-shape to scan data
    double OptimizeFit(LadarScan *scan, CarShape& car, int maxind);

    // test a potential fit angle
    double TestFit(LadarScan *scan, CarShape car);

/************ CAR TRACKING ************/

    // incorporate a new processed scan
    void Update(vector<CarShape> cars);

    // update a track with a scan
    void UpdateTrack(int id, CarShape car);

    // add a new track
    int AddTrack(CarShape car);

    // remove a track
    void RemoveTrack(int id);

    // clean up track list and stuff
    void CleanTracks();

    // initialize KF
    void InitKF(int id);

    // predict single var KF
    void PredictKF(double dt, double noise,
		   double &val, double &var);

    // predict 2-var KF
    void PredictKF2(double dt, double noise,
		    double val[2], double mvar[2][2]);

    // update single var KF
    void UpdateKF(double dt, double dv, double sigma,
		  double &val, double &var);

    // update 2-var KF
    void UpdateKF2(double dt, double dv, double sigma,
		   double val[2], double mvar[2][2]);

    // predict all new stuffs one timestep
    void Predict();

    // predict a single car by dt
    void PredictCar(int id, double dt);

    // find best-fit track
    int ClosestTrack(CarShape car);

    // set the corners on a track
    void SetCorners(int id);

    // overload
    void SetCorners(TrackedCar& t);

    // overload
    void SetPredictCorners(TrackedCar& t);

/************ DEBUG STUFFS ************/

    // send car shape information
    void SendCarShapes(vector<CarShape>& cars, int ladarId);

    // send debug information
    void SendDebug(LadarScan *scan);

    // send tracks
    void SendTracks();
};

static inline void mat22d_mul(double c[2][2],double a[2][2], double b[2][2])
{
    c[0][0] = a[0][0]*b[0][0] + a[0][1]*b[1][0];
    c[1][0] = a[1][0]*b[0][0] + a[1][1]*b[1][0];
    c[0][1] = a[0][0]*b[0][1] + a[0][1]*b[1][1];
    c[1][1] = a[1][0]*b[0][1] + a[1][1]*b[1][1];
}

#endif
