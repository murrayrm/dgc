#include "LadarCar.hh"

#define MIN(a,b) ((a)<(b))?(a):(b)
#define MAX(a,b) ((a)>(b))?(a):(b)
#define DIST(x1,y1,x2,y2) sqrt(((x2)-(x1))*((x2)-(x1))+((y2)-(y1))*((y2)-(y1)))

void LadarCarTracker::Update(vector<CarShape> cars)
{
    uint64_t time = DGCgettime();
    uint64_t dt;
    // subtract from all tracks' status
    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status <= 0)
	    continue;
	dt = time - track[i].lastUpdate;
	// been idle too long?
	if (dt > MAX_TIME)
	{
	    track[i].status = track[i].lastUpdateStatus - (int)(dt/MAX_TIME);
	    if (track[i].status <= 0)
	    {
		// goodbye
		RemoveTrack(i);
		return;
	    }
	}
    }

    Predict();

    // associate
    for (size_t i=0; i<cars.size(); i++)
    {
	int nearest = ClosestTrack(cars[i]);
	if (nearest == -1)
	    AddTrack(cars[i]);
	else
	    UpdateTrack(nearest,cars[i]);
    }

    CleanTracks();
}

int LadarCarTracker::AddTrack(CarShape car)
{
    if (emptyTrack == -1)
    {
	printf("Too many tracks!\n");
	return NUM_TRACKS-1;
    }
    int id = emptyTrack;
    track[id].status = INIT_STATUS;
    track[id].lastUpdateStatus = INIT_STATUS;
    // initial position and angle hear
    vec3_t pos = vec3_scale(0.5,vec3_add(car.start,car.end));
    double angle = atan2(car.corner.y - car.start.y,
			 car.corner.x - car.start.x);
    if (angle < 0)
	angle += M_PI;
    // set our values
    track[id].lastCorner = 0;
    track[id].x[0] = pos.x;
    track[id].y[0] = pos.y;
    track[id].x[1] = 0;
    track[id].y[1] = 0;
    track[id].width = vec3_mag(vec3_sub(car.corner,car.start));
    track[id].length = vec3_mag(vec3_sub(car.corner,car.end));
    track[id].angle[0] = angle;
    track[id].angle[1] = 0;
    track[id].nx = pos.x;
    track[id].ny = pos.y;
    track[id].na = angle;

    SetCorners(id);

    track[id].lastUpdate = track[id].firstSeen = DGCgettime();

    // and init our basic KF
    InitKF(id);
    
    if (id > highestTrack)
	highestTrack = id;
    emptyTrack = track[id].nextEmptyId;
    return id;
}

void LadarCarTracker::UpdateTrack(int id, CarShape car)
{
    vec3_t pos = vec3_scale(0.5,vec3_add(car.start,car.end));
    double angle = atan2(car.corner.y - car.start.y,
			 car.corner.x - car.start.x);
    if (angle < 0)
	angle += M_PI;
    
    double mindist = 100;
    int minindex = track[id].lastCorner;
    double dist;
    // find which point on original car is closest to new corner
    for (int i=0;i<4;i++)
    {
	dist = sqrt((car.corner.x - track[id].corner[i][0])*
		    (car.corner.x - track[id].corner[i][0])+
		    (car.corner.y - track[id].corner[i][1])*
		    (car.corner.y - track[id].corner[i][1]));
	if (dist < mindist)
	{
	    mindist = dist;
	    minindex = i;
	}
    }

    // now we go through and perform a good ol' prediction
    double dt = (DGCgettime() - track[id].lastUpdate)*0.000001;
    track[id].lastUpdate = DGCgettime();
    if (dt > 1) // was paused
	dt = 0;

    PredictKF(dt, KF2_PROC_NOISE, track[id].width, track[id].Pw);
    PredictKF(dt, KF2_PROC_NOISE, track[id].length, track[id].Pl);
    PredictKF2(dt, KF_VEL_NOISE, track[id].x, track[id].Px);
    PredictKF2(dt, KF_VEL_NOISE, track[id].y, track[id].Py);
    PredictKF2(dt, KF_VEL_NOISE, track[id].angle, track[id].Pa);

    // need to recompute corners because they will have shifted
    SetCorners(id);

    // now, dx and dy will be difference between prediction and measurement
    double dx = car.corner.x - track[id].corner[minindex][0];
    double dy = car.corner.y - track[id].corner[minindex][1];
    // except if we're matching bad corners
    // make sure centers are closer than center-corner
    double mdist = DIST(pos.x,pos.y,track[id].x[0],track[id].y[0]);
    double cdist = DIST(track[id].corner[minindex][0],track[id].corner[minindex][1],
			track[id].x[0],track[id].y[0]);
    if (mdist > cdist) //centers farther than radius
    {
	// use center-center offset
	dx = pos.x - track[id].x[0];
	dy = pos.y - track[id].y[0];
    }


    // now get width and length adjustments
    // ...adjacent corners
    int c1 = (minindex==0)?3:minindex-1;
    int c2 = (minindex==3)?0:minindex+1;
    // find closest corner matchup
    double dist1 = DIST(car.start.x,car.start.y,
			track[id].corner[c1][0],track[id].corner[c1][1]);
    dist1 += DIST(car.end.x,car.end.y,
			track[id].corner[c2][0],track[id].corner[c2][1]);
    double dist2 = DIST(car.start.x,car.start.y,
			track[id].corner[c2][0],track[id].corner[c2][1]);
    dist2 += DIST(car.end.x,car.end.y,
			track[id].corner[c1][0],track[id].corner[c1][1]);

    // which corners are closest?
    // if dist1<dist2, s->e is "clockwise"
//    bool cornerChoice = (dist1 < dist2);
    // thus, if cornerChoice is true and c is odd,
    // s->c is length; if cc true and c even, s->c width
    // if cc false and c odd, s->c width, etc...
    double dw=0;
    double dl=0;
    double dws=3;
    double dls=3;
    
    double da = angle - track[id].angle[0];
    // da is now in the range -PI to PI
//    if ((dist1 < dist2) != ((minindex&2)==0))
    if (fabs(da) < 0.25*M_PI || fabs(da) > 0.75*M_PI)
    {
	dw = track[id].width - vec3_mag(vec3_sub(car.corner,car.start));
	dl = track[id].length - vec3_mag(vec3_sub(car.corner,car.end));
	if (!car.startvis)
	{
	    dw = 0;
	    dws = 100;
	}
	if (!car.endvis)
	{
	    dl = 0;
	    dls = 100;
	}
    }
    else
    {
	dl = track[id].length - vec3_mag(vec3_sub(car.corner,car.start));
	dw = track[id].width - vec3_mag(vec3_sub(car.corner,car.end));
	if (!car.endvis)
	{
	    dw = 0;
	    dws = 100;
	}
	if (!car.startvis)
	{
	    dl = 0;
	    dls = 100;
	}
    }
    
    // if we are trying to shrink it
    if (dw > 0)
	dws *= 20;
    if (dl > 0)
	dls *= 20;

    if (da < 0)
	da += 2*M_PI;
    // limit da to (0,90)
    da = fmod(da, M_PI/2);
    // and if it's the other way, make it negative
    if (da > M_PI/4)
	da -= M_PI/2;

    // scale sigma linearly from 0 to max with time
    double age = 0.000001 * (DGCgettime() - track[id].firstSeen);
    if (age > KF_VEL_INIT_TIME)
	age = KF_VEL_INIT_TIME;
    double vsigma = (age/KF_VEL_INIT_TIME)*KF_MAX_VSIGMA;
    UpdateKF2(dt, dx, vsigma, track[id].x, track[id].Px);
    UpdateKF2(dt, dy, vsigma, track[id].y, track[id].Py);
    UpdateKF2(dt, da, car.fit*KF_ANG_SIGMA, track[id].angle, track[id].Pa);
    if (car.startvis && car.endvis)
    {
	UpdateKF(dt, -dw, dws, track[id].width, track[id].Pw);
	UpdateKF(dt, -dl, dls, track[id].length, track[id].Pl);
    }

    if (track[id].width < MIN_WIDTH)
	track[id].width = MIN_WIDTH;
    if (track[id].length < MIN_WIDTH)
	track[id].length = MIN_WIDTH;

    // and update to the new corner locations
    SetCorners(id);

    // check angle limits
    if (track[id].angle[0] < 0)
	track[id].angle[0] += M_PI;
    if (track[id].angle[0] > M_PI)
	track[id].angle[0] -= M_PI;

    track[id].status += 3;
    if (track[id].status > MAX_STATUS)
	track[id].status = MAX_STATUS;
    track[id].lastUpdateStatus = track[id].status;

    // and update dum-dum predictions
    track[id].nx = track[id].x[0];
    track[id].ny = track[id].y[0];
    track[id].na = track[id].angle[0];
}

int LadarCarTracker::ClosestTrack(CarShape car)
{
    double minscore = MAX_ASSOC_DIST;
    int minind = -1;
    double score,dist,dist1;

    double x = 0.5*(car.start.x + car.end.x);
    double y = 0.5*(car.start.y + car.end.y);

    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status <= 0)
	    continue;
	dist = DIST(x,y,track[i].nx,track[i].ny);

	if (dist > REJECT_DIST)
	    continue;
	score = dist;
	// find nearest corner-corner distance
	for (int j=0;j<4;j++)
	{
	    dist = DIST(car.corner.x,car.corner.y,track[i].corner[j][0],track[i].corner[j][1]);
	    dist1 = DIST(car.corner.x,car.corner.y,track[i].predcorner[j][0],track[i].predcorner[j][1]);
	    //dist = MIN(dist,dist1);

	    score = MIN(score,dist);
	}
	if (score < minscore)
	{
	    minind = i;
	    minscore = score;
	}
    }

    return minind;
}

inline void LadarCarTracker::PredictKF(double dt, double noise, double &val, double &var)
{
    // state doesn't need to be updated, since it's constant
    // so we just add noise to var (i think?)
    var += noise;
}

inline void LadarCarTracker::PredictKF2(double dt, double noise, double val[2], double mvar[2][2])
{
    double A[2][2] = {{1, dt},{0,1}};
    double At[2][2] = {{1,0},{dt,1}};
    double tmp[2][2];

    // update state
    val[0] += dt*val[1];
    // update state covariance matrices
    mat22d_mul(tmp,mvar,At);
    mat22d_mul(mvar,A,tmp);
    // add process noise
    mvar[1][1] += noise;
}

inline void LadarCarTracker::UpdateKF(double dt, double dv, double sigma, double &val, double &var)
{
    double K;
    double R = sigma*sigma;
    double d = var + R;
    // same thing as 2-var?
    double fac = R/d;
    // Kalman gain
    K = var / d;
    // update variance
    var *= fac;
    // update state
    val += K*dv;
}

inline void LadarCarTracker::UpdateKF2(double dt, double dv, double sigma, double val[2], double mvar[2][2])
{
    double K[2];
    double R = sigma*sigma;
    double d = mvar[0][0] + R;
    // shortcut thingy
    double fac = R/d;
    // Kalman gain matrix
    K[0] = mvar[0][0]/d;
    K[1] = mvar[1][0]/d;
    // update covariance matrix
    mvar[0][0] *= fac;
    mvar[1][0] *= fac;
    mvar[0][1] *= fac;
    mvar[1][1] *= fac;
    // update state with new data
    val[1] += K[1]*dv;
    val[0] += K[0]*dv;
}

void LadarCarTracker::InitKF(int id)
{
    // fundamentally just initialize the covariance stuff
    track[id].Px[0][0] = KF_VEL_INIT;
    track[id].Px[1][1] = KF_VEL_INIT;
    track[id].Px[1][0] = 0;
    track[id].Px[0][1] = 0;

    track[id].Py[0][0] = KF_VEL_INIT;
    track[id].Py[1][1] = KF_VEL_INIT;
    track[id].Py[1][0] = 0;
    track[id].Py[0][1] = 0;

    track[id].Pa[0][0] = KF_ANG_INIT;
    track[id].Pa[1][1] = KF_ANG_INIT;
    track[id].Pa[1][0] = 0;
    track[id].Pa[0][1] = 0;

    track[id].Pw = track[id].Pl = KF2_COV_INIT;
}

void LadarCarTracker::Predict()
{
    double dt = LADAR_DT;

    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status <= 0)
	    continue;
	dt = 0.000001*(DGCgettime() - track[i].lastUpdate);
	// set predicted values using proper dynamics prediciton
	PredictCar(i,dt);
//	track[i].nx = track[i].x[0];
//	track[i].ny = track[i].y[0];
//	track[i].na = track[i].angle[0];
	// and set the predicted corner location
	SetPredictCorners(track[i]);
    }
}

void LadarCarTracker::PredictCar(int id, double dt)
{
    double angle = track[id].angle[0];
    // first, get our velocity parallel to direction
    double vel1 = cos(angle)*track[id].x[1] + sin(angle)*track[id].y[1];
    double vel2 = -cos(angle)*track[id].y[1] + sin(angle)*track[id].x[1];
    double vel = vel1;

    if (fabs(vel2) > fabs(vel1))
    {
	vel = vel2;
	// point angle along velocity
        angle -= M_PI/2;
    }
    angle -= M_PI/2;
    // radius of turn
    double R = vel/track[id].angle[1];
    double fang = angle+track[id].angle[1]*dt;
    double dx = R*cos(fang) - R*cos(angle);
    double dy = R*sin(fang) - R*sin(angle);

    track[id].na = fang + M_PI/2;
    // get graphical display right
    if (fabs(vel2) > fabs(vel1))
	track[id].na += M_PI/2;
    
    track[id].nx = track[id].x[0] + dx;
    track[id].ny = track[id].y[0] + dy;

    // set predicted velocities along our direction
    // (mostly for graphical display)
    if (vel1 > vel2)
    {
	track[id].nvx = vel1*cos(track[id].na);
	track[id].nvy = vel1*sin(track[id].na);
    }
    else
    {
	track[id].nvx = vel2*cos(track[id].na+M_PI/2);
	track[id].nvy = vel2*sin(track[id].na+M_PI/2);
    }
}

void LadarCarTracker::SetCorners(int id)
{
    SetCorners(track[id]);
}

void LadarCarTracker::SetCorners(TrackedCar& t)
{
    double dx1 = 0.5*cos(t.angle[0])*t.width;
    double dy1 = 0.5*sin(t.angle[0])*t.width;
    double dx2 = 0.5*cos(t.angle[0]-(M_PI/2))*t.length;
    double dy2 = 0.5*sin(t.angle[0]-(M_PI/2))*t.length;
    double x = t.x[0];
    double y = t.y[0];

    t.corner[0][0] = x+dx1+dx2;
    t.corner[0][1] = y+dy1+dy2;
    t.corner[1][0] = x+dx1-dx2;
    t.corner[1][1] = y+dy1-dy2;
    t.corner[2][0] = x-dx1-dx2;
    t.corner[2][1] = y-dy1-dy2;
    t.corner[3][0] = x-dx1+dx2;
    t.corner[3][1] = y-dy1+dy2;
}

void LadarCarTracker::SetPredictCorners(TrackedCar& t)
{
    double dx1 = 0.5*cos(t.na)*t.width;
    double dy1 = 0.5*sin(t.na)*t.width;
    double dx2 = 0.5*cos(t.na-(M_PI/2))*t.length;
    double dy2 = 0.5*sin(t.na-(M_PI/2))*t.length;
    double x = t.nx;
    double y = t.ny;

    t.predcorner[0][0] = x+dx1+dx2;
    t.predcorner[0][1] = y+dy1+dy2;
    t.predcorner[1][0] = x+dx1-dx2;
    t.predcorner[1][1] = y+dy1-dy2;
    t.predcorner[2][0] = x-dx1-dx2;
    t.predcorner[2][1] = y-dy1-dy2;
    t.predcorner[3][0] = x-dx1+dx2;
    t.predcorner[3][1] = y-dy1+dy2;
}

void LadarCarTracker::RemoveTrack(int id)
{
    track[id].status = -1;
}

void LadarCarTracker::CleanTracks()
{
    int lastEmpty = 0;
    emptyTrack = -1;
    // update list of empty tracks
    for (int i=0;i<NUM_TRACKS;i++)
    {	
	if (track[i].status == 0)
	{
	    // set this->emptyTrack to first empty track
	    if (emptyTrack < 0)
		emptyTrack = i;
	    track[lastEmpty].nextEmptyId = i;
	    lastEmpty = i;
	}
	else
	{
	    // set highest active track
	    highestTrack = i;
	}
    }
    // set last empty track thing
    track[NUM_TRACKS-1].nextEmptyId = -1;
}

void LadarCarTracker::SendTracks()
{
    MapElement el;
    MapElement elPred;
    MapElement elClear;

    //el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_GREEN;

    elPred.setTypeObstacle();
    elPred.height = 1;
    elPred.plotColor = MAP_COLOR_GREEN;
    // set the state to something approximate, who cares anyway?
//    el.setState(scan->state);
//    el.velocity = point2(0,0);

    elClear.setTypeClear();

//    el.setTypePoints();
    point2 mid, d1, d2;
    uint64_t time = DGCgettime();

    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status < 0)
	{
	    elClear.setTypeClear();
	    elClear.setId(this->moduleId,i+1000);
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	    elClear.setId(this->moduleId,i+5000);
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	    track[i].status = 0;
	}
	if (track[i].status <= 0)
	    continue;
	el.setTypeObstacle();
	elPred.setTypeObstacle();

	point2arr geo;

	for (int j=0;j<4;j++)
	    geo.push_back(point2(track[i].predcorner[j][0],
				 track[i].predcorner[j][1])); 
	
	elPred.setColor(MAP_COLOR_ORANGE);
	elPred.setGeometry(geo);

	geo.clear();	

	for (int j=0;j<4;j++)
	    geo.push_back(point2(track[i].corner[j][0],
				 track[i].corner[j][1]));

	if (track[i].status < MAX_STATUS/2)
	    el.setColor(MAP_COLOR_BLUE);
	else
	    el.setColor(MAP_COLOR_GREEN,(100*track[i].status)/MAX_STATUS);

	el.setGeometry(geo);
	
	el.setId(this->moduleId,i+1000);
	el.velocity = point2(track[i].nvx,track[i].nvy);

	elPred.setId(this->moduleId,i+5000);
	elPred.velocity = point2(track[i].nvx,track[i].nvy);
	if ((time - track[i].lastUpdate) < 100000)
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	else
	    this->debugTalker.sendMapElement(&elPred,debugSubgroup);
    }
}
