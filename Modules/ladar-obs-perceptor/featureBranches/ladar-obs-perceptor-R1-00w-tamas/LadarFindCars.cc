#include "LadarCar.hh"

#define MIN(a,b) ((a)<(b))?(a):(b)
#define MAX(a,b) ((a)>(b))?(a):(b)

void LadarCarTracker::ProcessScan(LadarScan *scan)
{
    SegmentScan(scan);
    vector<CarShape> cars = FindCars(scan);
    Update(cars);
    if (this->sendDebug)// && (DGCgettime() - this->lastDebugTime) > SEND_DEBUG_RATE)
    {
	this->lastDebugTime = DGCgettime();
	SendTracks();
	SendCarShapes(cars,scan->ladarId);
	SendDebug(scan);
    }
}

void LadarCarTracker::SegmentScan(LadarScan *scan)
{
    double pr, segdist;

    scan->segment[0] = 0;

    // find all the breaks between segments
    for (int i=1; i<scan->numPoints; i++)
    {
	pr = MIN(scan->range[i],scan->range[i-1]);
	segdist = MAP_SEG_MIN + MAP_SEG_COEFF*pr;
	// there is a break in the scan, mark new segment
	if (vec3_mag(vec3_sub(scan->pos[i-1],scan->pos[i])) > segdist)
	    scan->segment[i] = i;
	else
	    scan->segment[i] = scan->segment[i-1];
    }

    // close any no-return gaps smaller than or equal to NR_GAP_POINTS
    // if distance is less than NR_GAP_DIST
    int nrCount=0;
    int nrStart=0;
    bool isnr = false;
    for (int i=1;i<scan->numPoints-1;i++)
    {
	if (scan->range[i] > MAX_LADAR_DIST)
	{
	    if (isnr) //continuing no-return seg
	    {
		nrCount++;
	    }
	    else // starting no-return segment
	    {
		nrCount=1;
		nrStart=i;
		isnr = true;
	    }
	}
	else
	{
	    if (isnr) // ending no-return segment
	    {
		double dist = vec3_mag(vec3_sub(
					   scan->pos[i],scan->pos[nrStart-1]));
		if (dist < NR_GAP_DIST && nrCount <= NR_GAP_POINTS)
		{
		    // close gap
		    for (int j=nrStart;j<=i;j++)
			scan->segment[j] = scan->segment[j-1];
		}
	    }
	    isnr = false;
	}
    }
}

vector<CarShape> LadarCarTracker::FindCars(LadarScan *scan)
{
    vector<CarShape> cars;
    int start, end;
    CarShape curCar;

    start = 0;
    for (int i=1;i<scan->numPoints;i++)
    {
	// still part of same segment
	if (scan->segment[i] == start)
	    continue;
	// so we have a new segment
	end = i-1;
	// no-returns?
	if (scan->range[start] > MAX_LADAR_DIST
	    || scan->range[end] > MAX_LADAR_DIST)
	{
	    start = i;
	    continue;
	}	    
	if (end == start) // one point
	{
	    // TODO: DEAL WITH THIS
	}
	else if (end - start == 1) // two points
	{
	    curCar.startind = start;
	    curCar.endind = end;
	    curCar.start = scan->pos[start];
	    curCar.end = scan->pos[end];
	    curCar.corner = curCar.end;
	    curCar.startvis = true;
	    curCar.endvis = true;
	    curCar.angle = atan2(curCar.end.y-curCar.start.y,
				 curCar.end.x-curCar.start.x);

	    // check corner visibility, short-circuit prevents memory snafu
	    if (start == 0 || scan->range[start-1] < scan->range[start])
		curCar.startvis = false;
	    if (end == scan->numPoints - 1 || scan->range[end+1] < scan->range[end])
		curCar.endvis = false;

	    curCar.fit = 0.5;
	    
	    cars.push_back(curCar);
	}
	else // have enough points dur
	{
	    // get perpendicular vector
	    vec3_t v = vec3_sub(scan->pos[end],scan->pos[start]);
	    v.z = -v.x; v.x = v.y; v.y = v.z; v.z = 0;
	    v = vec3_unit(v);
	    // find farthest point from centerline
	    double maxdist = 0;
	    int maxind = start+1;
	    double dist;
	    for (int j=start+1;j<end;j++)
	    {
		if (scan->range[j]>MAX_LADAR_DIST)
		    continue;
		dist = fabs(vec3_dot(v,vec3_sub(scan->pos[j],scan->pos[start])));
		if (dist > maxdist)
		{
		    maxdist = dist;
		    maxind = j;
		}
	    }

	    curCar.startind = start;
	    curCar.endind = end;
	    curCar.start = scan->pos[start];
	    curCar.end = scan->pos[end];
	    curCar.startvis = true;
	    curCar.endvis = true;

	    // check corner visibility, short-circuit prevents memory snafu
	    if (start == 0 || scan->range[start-1] < scan->range[start])
		curCar.startvis = false;
	    if (end == scan->numPoints - 1 || scan->range[end+1] < scan->range[end])
		curCar.endvis = false;

	    // really narrow slice
	    if (maxdist < SMALL_MAXDIST)
	    {
		if (curCar.endvis)
		    maxind = end;
		else
		    maxind = start;
	    }
	    // make sure our corner is closest, within reason
	    if (maxdist > 5*SMALL_MAXDIST &&
		scan->range[maxind] > scan->range[start] && 
		scan->range[maxind] > scan->range[end])
	    {
		start = i;
		continue;
	    }

	    // add car if the fit is good enough
	    if (OptimizeFit(scan, curCar, maxind) < FIT_THRESH)
	    {
		// scale fit value to (0,1)
		curCar.fit = curCar.fit / FIT_THRESH;
		cars.push_back(curCar);
	    }
	}
	start = i;
    }

    return cars;
}

double LadarCarTracker::OptimizeFit(LadarScan *scan, CarShape& car, int maxind)
{
    double fit, fitup, fitdown;
    int start = car.startind;
    int end = car.endind;

    // the radius of our constraining sphere
    double dist = 0.5*vec3_mag(vec3_sub(scan->pos[end],scan->pos[start]));
    vec3_t mid = vec3_scale(0.5,vec3_add(car.start,car.end));

    // get angle, adjust for side
    car.angle = atan2(scan->pos[maxind].y-mid.y,
			 scan->pos[maxind].x-mid.x);

    double angle = car.angle;
    double da = INIT_DA;

    car.corner = mid;
    car.corner.x += cos(car.angle)*dist;
    car.corner.y += sin(car.angle)*dist;

    fit = TestFit(scan, car);
    
    for (int n=0;n<ANGLE_ITER;n++)
    {
	car.angle = angle + da;
	car.corner = mid;
	car.corner.x += cos(car.angle)*dist;
	car.corner.y += sin(car.angle)*dist;
	fitup = TestFit(scan, car);

	car.angle = angle - da;
	car.corner = mid;
	car.corner.x += cos(car.angle)*dist;
	car.corner.y += sin(car.angle)*dist;
	fitdown = TestFit(scan, car);

	if (fitup < fit)
	{
	    angle = angle + da;
	    fit = fitup;
	}

	if (fitdown < fit)
	{
	    angle = angle - da;
	    fit = fitdown;
	}

	da = 0.5*da;
    }

    car.fit = fit;

    return fit;
}

double LadarCarTracker::TestFit(LadarScan *scan, CarShape car)
{
    int start = car.startind;
    int end = car.endind;
    vec3_t corner = car.corner;

    vec3_t v1 = vec3_sub(scan->pos[start],corner);
    vec3_t v2 = vec3_sub(scan->pos[end],corner);

    double l1 = vec3_mag(v1);
    double l2 = vec3_mag(v2);

    v1 = vec3_unit(v1);
    v2 = vec3_unit(v2);

    double d1, d2;
    double d;
    double total=0;
    int count=0;

    for (int i=start+1;i<end;i++)
    {
	if (scan->range[i] > MAX_LADAR_DIST)
	    continue;
	d1 = vec3_dot(v1,vec3_sub(scan->pos[i],corner));
	d2 = vec3_dot(v2,vec3_sub(scan->pos[i],corner));
	d = 100;
	if (d1 < l1 && d1 > 0) // one side within range
	    d = d2;
        if (d2 < l2 && d2 > 0) // other side within range
	    d = MIN(d,d1);
	if (d == 100) // neither side, check corners
	{
	    d = vec3_mag(vec3_sub(scan->pos[i],corner));
	    d1 = vec3_mag(vec3_sub(scan->pos[i],scan->pos[start]));
	    d2 = vec3_mag(vec3_sub(scan->pos[i],scan->pos[end]));
	    d = MIN(d, d1);
	    d = MIN(d, d2);
	}
	// now, d is closest distance to car-shape
	total += d*d;
	count++;
    }

    total = total/(count+2);

    return total;
}

void LadarCarTracker::SendCarShapes(vector<CarShape>& cars, int ladarId)
{
    MapElement el;
    MapElement elClear;

    //el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_RED;
    // set the state to something approximate, who cares anyway?
//    el.setState(scan->state);
    el.velocity = point2(0,0);

    elClear.setTypeClear();

//    el.setTypePoints();

    for (size_t i=0;i<cars.size();i++)
    {
	el.setTypeObstacle();
	point2arr geo;
	geo.push_back(point2(cars[i].start.x,cars[i].start.y));
	geo.push_back(point2(cars[i].corner.x,cars[i].corner.y));
	geo.push_back(point2(cars[i].end.x,cars[i].end.y));
	el.setGeometry(geo);
	el.setId(this->moduleId,i+100,ladarId);
	this->debugTalker.sendMapElement(&el,debugSubgroup);
    }
}

void LadarCarTracker::SendDebug(LadarScan *scan)
{
    MapElement el;
    MapElement elClear;

    //el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_YELLOW;
    // set the state to something approximate, who cares anyway?
    el.setState(scan->state);
    el.velocity = point2(0,0);
    el.setTypeLine();

    elClear.setTypeClear();

//    el.setTypePoints();

//    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	//el.setTypeObstacle();
	point2arr geo;
	for (int i=0;i<scan->numPoints;i++)
	    geo.push_back(point2(scan->pos[i].x,scan->pos[i].y));
	//geo = geo.simplify(0.3);
	el.setGeometry(geo);
	el.setId(this->moduleId,0,scan->ladarId);
	this->debugTalker.sendMapElement(&el,debugSubgroup);
    }
}
