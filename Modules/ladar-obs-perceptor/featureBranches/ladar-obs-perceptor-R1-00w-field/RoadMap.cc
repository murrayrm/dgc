#include "RoadMap.hh"

RoadMap::RoadMap()
{
    memset(this,0,sizeof(*this));
    this->gsmap = new EMap(EMAP_SIZE);
    this->ptumap = new EMap(EMAP_SIZE);
    this->gsmap->clear(RMAP_CLEAR);
    this->ptumap->clear(RMAP_CLEAR);

    for (int k=0;k<GROUND_LADARS;k++)
	for (int i=0; i<LADAR_BLOB_MAX_POINTS;i++)
	    lastScan[k][i][0] = RMAP_NOTROAD;
}

RoadMap::~RoadMap()
{
    delete this->gsmap;
    delete this->ptumap;
}

void RoadMap::shiftMap(int dx, int dy, float l2m[4][4])
{
    // set our transform matrices
    memcpy(local2map,l2m,16*sizeof(float));
    // adjust for height, make negative so plus is up
    local2map[2][2] = -1.0f/RMAP_VRES;
    local2map[2][3] = (mapOffsetZ/RMAP_VRES) + RMAP_VCENTER;
    mat44f_inv(map2local,local2map);
    // and shift/clear the map
    this->gsmap->shiftClear(dx,dy,RMAP_CLEAR);
    this->ptumap->shiftClear(dx,dy,RMAP_CLEAR);
    shiftX += dx;
    shiftY += dy;
}

void RoadMap::drawScan2(LadarScan *scan, LadarScanStore *store)
{
  //  fprintf(stderr, "\n RECEIVED NEW SCAN! \n");
  unsigned long long starttime, endtime;
  DGCgettime(starttime);
    LadarRangeBlob *blob = &(scan->blob);
    //ONLY use ptu ladar - not riegl yet
    if (blob->sensorId != SENSNET_PTU_LADAR)
    {
      return;
    }


    float mat[4][4];
    emap_t height[LADAR_BLOB_MAX_POINTS];
    int iscan[LADAR_BLOB_MAX_POINTS][2];
    vec3_t* pos = scan->points;

    LadarScan *pastScan;
    pastScan = store->getPastScan(scan->ladarId, 3);
    LadarRangeBlob *oldblob = &(scan->blob);
    vec3_t* oldpos = pastScan->points;
    int oldscan[LADAR_BLOB_MAX_POINTS][2];

  // convert points
    for (int i=0;i<blob->numPoints;i++)
    {
	// convert to map frame
	iscan[i][0] = (int)(local2map[0][0]*pos[i].x+local2map[0][3]);
	iscan[i][1] = (int)(local2map[1][1]*pos[i].y+local2map[1][3]);
	height[i] = (emap_t)(local2map[2][2]*pos[i].z+local2map[2][3]);

	oldscan[i][0] = (int)(local2map[0][0]*oldpos[i].x+local2map[0][3]);
	oldscan[i][1] = (int)(local2map[1][1]*oldpos[i].y+local2map[1][3]);
    }


    //first try -> just propagate changes out from ptumap
    for(int i=0; i<blob->numPoints; i++)
    {
        this->ptumap->drawminRectangle(iscan[i][0]-GP_SIZE,iscan[i][1]-GP_SIZE,
			iscan[i][0]+GP_SIZE,iscan[i][1]+GP_SIZE, height[i]);
	//	updateGSMap(iscan[i][0], iscan[i][1], GP_SIZE, height[i]);
    }
    
    /** 

    // second try -> use discontinuities for segmentation, then check
    //               these for 3-d flatness; want to use old scan so 
    //               will be amidst painted area
    double diffs[oldblob->numPoints-1];
    bool newseg = true; //whether we're dealing w/ a new segment
    vec3_t segStart;
    segStart = vec3_set(0.0,0.0,0.0);
    vec3_t segEnd;
    int startIndex = 0;
    int numSegPts = 0;
    double length;
    bool gs, tempgs;
    for(int i=1;i<oldblob->numPoints-1;i++) 
      {
	diffs[i] = oldpos[i+1].z - oldpos[i].z;

	if(fabs(diffs[i]) < GAP_DIST_ROAD) 
	{
	  segEnd = vec3_set(oldpos[i].x,oldpos[i].y,0.0);
	  numSegPts++;

	} else {
	  if(false == newseg) // if this *used* to be a segment, check it
	    {
	      length = vec3_mag(vec3_sub(segStart, segEnd));
	    }
	  gs = true;
	  for(int j=1; j<numSegPts; j++) 
	    {
	      tempgs = checkGradient(oldscan[i][0], oldscan[i][1], GP_SIZE);
	      gs = gs && tempgs;
	    }
	  //if long enough AND passed gradient test
	  if(gs && (length > MIN_ROAD_WIDTH)) {
	  for(int j=1; j<numSegPts; j++) 
	    {
              this->gsmap->drawminRectangle(i-GP_SIZE, j-GP_SIZE, i+GP_SIZE, j+GP_SIZE, height[i]);

	    }
	  }
	}

	if(newseg == true) {
	  segStart = vec3_set(oldpos[i].x, oldpos[i].y, 0.0);
	  numSegPts = 1;
	  startIndex = i;

	  newseg = false;
	}

      }


    **/

    DGCgettime(endtime);
    //    fprintf(stderr,"elapsed time: %llu \n", endtime - starttime);
}

void RoadMap::updateGSMap(int x, int y, int margin, emap_t height) 
{
  //  fprintf(stderr, "called updateGSMap for %d %d \n", x, y);

  //note -> checks four extra cells (corners)
  int x_i, x_f, y_i, y_f;
  x_i = MAX(0,x-2*margin);
  x_f = MIN(x+2*margin, EMAP_SIZE);
  y_i = MAX(0,y-2*margin);
  y_f = MIN(y+2*margin, EMAP_SIZE);  
  bool gs;
  for(int i=x_i; i<=x_f; i++) {
    for(int j=y_i; j<=y_f; j++) {
      gs = checkGradient(i,j, margin);
      if(gs) {
        this->gsmap->drawminRectangle(i-margin, j-margin, i+margin, j+margin, height);
      }
    }
  }

}

//return true if gradient on PTU ladar map is consistent w/ ground
bool RoadMap::checkGradient(int x, int y, int margin)
{
  //the 4 directions we check for continuity...
  bool x_pos, x_neg, y_pos, y_neg;
  double dx_neg, dx_pos, dy_neg, dy_pos;
  bool gs;

  //how far to the side to look...
  int cells = 2;
 
  emap_t dat1, dat2;
  dat1 =  *(this->ptumap->getData()+y*EMAP_SIZE+x);
  if(dat1 == RMAP_CLEAR) {
    return false;
  }

  //doesn't yet handle no data...
  if(x <= margin + cells) {
    x_neg = true;
  } else {
    dat2 =  *(this->ptumap->getData()+y*EMAP_SIZE+x-margin-cells);
    dx_neg =dat1 - dat2;
    if(dat2 == RMAP_CLEAR) {
      x_neg = false;
    } else {
      x_neg = RMAP_VRES*fabs(dx_neg) < MAX_ROAD_SLOPE/((cells+margin)*EMAP_RES);
    }
  }
  if(x >= EMAP_SIZE - margin - cells) {
    x_pos = true;
  } else {
    dat2 =  *(this->ptumap->getData()+y*EMAP_SIZE+x+margin+cells);
    dx_pos =dat1 - dat2;
    if(dat2 == RMAP_CLEAR) {
      x_pos = false;
    } else {
      x_pos = RMAP_VRES*fabs(dx_pos) < MAX_ROAD_SLOPE/((cells+margin)*EMAP_RES);
    }
  }

  if(y <= margin + cells) {
    y_neg = true;
  } else {
    dat2 = *(this->ptumap->getData()+(y-margin-cells)*EMAP_SIZE+x);
    dy_neg =dat1 - dat2;
    if(dat2 == RMAP_CLEAR) {
      y_neg = false; 
    } else {
      y_neg = RMAP_VRES*fabs(dy_neg) < MAX_ROAD_SLOPE/((cells+margin)*EMAP_RES);
    }
  }
  if(y <= EMAP_SIZE - margin - cells) {
    y_pos = true;
  } else {
    dat2 = *(this->ptumap->getData()+(y+margin+cells)*EMAP_SIZE+x);
    dy_pos = dat1 - dat2;
    if(dat2 == RMAP_CLEAR) {
      y_pos = false;
    } else {    
      y_pos = RMAP_VRES*fabs(dy_pos) < MAX_ROAD_SLOPE/((cells+margin)*EMAP_RES);
    }
  }

  gs = x_pos && x_neg && y_pos && y_neg;

  //  fprintf(stderr, "checking pt %d, %d. x+ = %f, x- = %f, y+ = %f, y- = %f, gs = %d \n", x, y, dx_pos, dx_neg, dy_pos, dy_neg, gs);

  return gs;
 
}

void RoadMap::drawScan(LadarScan *scan, LadarScanStore *store)
{

  drawScan2(scan, store);

    float mat[4][4];

    int iscan[LADAR_BLOB_MAX_POINTS][2];
    int oldscan[LADAR_BLOB_MAX_POINTS][2];
    uint8_t road[LADAR_BLOB_MAX_POINTS];
    emap_t height[LADAR_BLOB_MAX_POINTS];

    int index = 0;
    int curStart, curEnd;

    bool wasRoad;
    bool isRoad;
    vec3_t segStart;
    vec3_t segEnd;

    int MIN_SEG_POINTS=0;
    double MAX_ROAD_GAP=0;
    int GAP_WIDTH=0;

    LadarRangeBlob *blob = &(scan->blob);
    LadarScan *pastScan1;
    LadarScan *pastScan2;
    pastScan1 = store->getPastScan(scan->ladarId, 1);
    pastScan2 = store->getPastScan(scan->ladarId, 2);

    LadarScan *oldScan;
    oldScan = store->getPastScan(scan->ladarId,3);

    //    vec3_t* pos = scan->points;
    vec3_t* oldpos = oldScan->points;
    vec3_t* pos = oldScan->points;

    if (blob->sensorId == SENSNET_RIEGL)
    {
	index = 1;
	MIN_SEG_POINTS = MIN_SEG_POINTS_RIEGL;
	MAX_ROAD_GAP = MAX_ROAD_GAP_RIEGL;
	GAP_WIDTH = GAP_WIDTH_RIEGL;
    } else if (blob->sensorId == SENSNET_PTU_LADAR) {
	index = 0;
	MIN_SEG_POINTS = MIN_SEG_POINTS_PTU;
	MAX_ROAD_GAP = MAX_ROAD_GAP_PTU;
	GAP_WIDTH = GAP_WIDTH_PTU;
    } else if (blob->sensorId == SENSNET_MF_BUMPER_LADAR) {
	index = 0;
	MIN_SEG_POINTS = MIN_SEG_POINTS_PTU;
	MAX_ROAD_GAP = MAX_ROAD_GAP_PTU;
	GAP_WIDTH = GAP_WIDTH_PTU;


    } else {
	fprintf(stderr,"invalid ladar ID for roadmap\n");
	return;
    }

    //debugging output for tuning of groundstrike parameters
    vec3_t r1, r2; //vectors from point1 to point2
    double meanHypot1 = 0;
    double meanHypot2 = 0;
    double meanZ1 = 0;
    double meanZ2 = 0;
    for(int i=0; i < scan->numPoints; i++) {
      r1 = vec3_sub(scan->points[i], pastScan1->points[i]);
      r2 = vec3_sub(scan->points[i], pastScan2->points[i]);
      meanHypot1 += vec3_mag(r1);
      meanHypot2 += vec3_mag(r2);
      meanZ1 += fabs(r1.z);
      meanZ2 += fabs(r2.z);
    }
    Log::getStream(7)<<endl;
    if(blob->sensorId == SENSNET_RIEGL) {
      Log::getStream(7)<<"for Riegl, avg delta: ";
    } else if(blob->sensorId == SENSNET_PTU_LADAR) {
      Log::getStream(7)<<"for PTU, avg delta: ";
    }
    meanHypot1 /= scan->numPoints;
    meanHypot2 /= scan->numPoints;
    meanZ1 /= scan->numPoints;
    meanZ2 /= scan->numPoints;
    Log::getStream(7)<<meanHypot1<<", "<<meanHypot2<<endl;
    Log::getStream(7)<<"and avg dz = "<<meanZ1<<", "<<meanZ2<<endl;


    // zero stuff
    memset(road,0,LADAR_BLOB_MAX_POINTS*sizeof(uint8_t));

    // set up local coordinate conversion matrix
    mat44f_mul(mat, blob->veh2loc, blob->sens2veh);

    // convert points
    for (int i=0;i<blob->numPoints;i++)
    {
	// convert to map frame
	iscan[i][0] = (int)(local2map[0][0]*pos[i].x+local2map[0][3]);
	iscan[i][1] = (int)(local2map[1][1]*pos[i].y+local2map[1][3]);

	oldscan[i][0] = (int)(local2map[0][0]*oldpos[i].x+local2map[0][3]);
	oldscan[i][1] = (int)(local2map[1][1]*oldpos[i].y+local2map[1][3]);


	height[i] = (emap_t)(local2map[2][2]*pos[i].z+local2map[2][3]);

	// update last scan
	lastScan[index][i][0] += shiftX;
	lastScan[index][i][1] += shiftY;
    }

    // segment based on lines connecting endpoints GAP_WIDTH apart
    // and point-line distance along the way
    wasRoad = false;
    for (int i=GAP_WIDTH;i<blob->numPoints;i++)
    {
         vec3_t r = vec3_sub(pos[i],pos[i-GAP_WIDTH]);
      // vec3_t r = vec3_sub(iscan[i],iscan[i-GAP_WIDTH]);
	double rr = vec3_mag(r);
	double d=0;
	double dd=0;


	isRoad = true;

	double slope = 0;
	int numNoReturns = 0;

	bool gs = false;
	int numgs = 0;

	for (int j=i-GAP_WIDTH+1;j<i;j++)
	{
	  //	    dd = vec3_mag(vec3_cross(r,vec3_sub(pos[i-GAP_WIDTH],pos[j])))/rr;
	    dd = vec3_mag(vec3_cross(r,vec3_sub(pos[i-GAP_WIDTH],pos[j])))/rr;
	    d += dd*dd;

	    if(blob->points[j][1] > 50.0)
	      numNoReturns++;

	    //check dz/dx and dz/dy before calling this a gradient
	    gs = checkGradient(iscan[j][0], iscan[j][1], GP_SIZE);
	    if(gs)
	      numgs++;

	}

	d = d/(GAP_WIDTH-1);

	// 'discontinuity' check; average and single
	if(d > MAX_ROAD_GAP) {
  	  isRoad = false;
	}
	double dz = pos[i].z - pos[i-i].z;
	if(fabs(dz) > MAX_DZ_ROAD_GAP) {
	  //	  isRoad = false;
	}

	if(numgs < GAP_WIDTH - 2) {
	  if(isRoad)
	    //  	    fprintf(stderr, "rejected segment due to gradient  %d \n", numgs);
	  isRoad = false;

	} else {
	  //	  fprintf(stderr, "accepted segment due to gradient  %d %d\n", numgs, numNoReturns);
	}

	/**
	// slope check perp to scan
        slope += fabs((oldpos[i].z -pos[i].z) / vec3_mag(vec3_sub(pos[i],oldpos[i])));
	if(slope > MAX_ROAD_PERP_SLOPE) {
	  isRoad = false;
	  //	  Log::getStream(7)<<"ruled out road seg based on perp slope"<<endl;
	}
	**/

	/**

	//check that scan is moving across ground
	double dist = vec3_mag(vec3_sub(pos[i],oldpos[i]));
	if(dist < .02) {
	  isRoad = false;
	  //	  Log::getStream(7)<<"ruled out road seg based on too small dist"<<endl;
	}

	/**

	//check if this is a no-return segment
	if((numNoReturns > 0) || (blob->points[i][1] > 50.0)) {
	  isRoad = false;
	  //	  Log::getStream(7)<<"ruled out road seg based on no-return"<<endl;
	}


	/**
	// slope check along scan
	if (fabs(r.z/sqrt(r.x*r.x + r.y*r.y)) > MAX_ROAD_SLOPE)
	    isRoad = false;
	**/


	if (isRoad)
	{
	    if (wasRoad) //increment endpoint
	    {
		curEnd = i;
		segEnd = pos[i];
	    }
	    else //just started road, set start and end
	    {
		curStart = i-GAP_WIDTH;
		curEnd = i;
		segStart = pos[i-GAP_WIDTH];
		segEnd = pos[i];
	    }
	}
	else if (wasRoad) //road just ended
	{
	    // set relevant road areas to 1
	  if ((curEnd-curStart >= MIN_SEG_POINTS) && (vec3_mag(vec3_sub(segEnd, segStart)) > MIN_ROAD_SEG_LENGTH)) {
	    for(int k = curStart; k < curEnd-curStart+1; k++)
	      oldScan->road[k] = 1;
            memset(road+curStart,1,(curEnd-curStart+1));
	  } else {
	    //	    Log::getStream(7)<<"not sufficiently long segment to call road"<<endl;
	  }
	}
	wasRoad = isRoad;
    }
    // don't forget about last segment
    if (isRoad)
	memset(road+curStart,1,(curEnd-curStart+1));

    // now, draw segments
    int numpts = MIN(blob->numPoints, oldScan->numPoints);
    char tmp[10];
    string str;
    string oldstr;
    str.clear();
    oldstr.clear();
    //    for (int i=0;i<numpts-2;i++)
    for(int i=0;i<blob->numPoints-2;i++)
    {
      sprintf(tmp, "%d", road[i]);
      str.append(tmp);
      sprintf(tmp, "%d", oldScan->road[i]);
      oldstr.append(tmp);
	if (road[i] == 0)
	{
	  //	    this->ptumap->drawminRectangle(iscan[i][0]-GP_SIZE,iscan[i][1]-GP_SIZE,
	  //					iscan[i][0]+GP_SIZE,iscan[i][1]+GP_SIZE, RMAP_NOTROAD);
	    iscan[i][0] = RMAP_INVALID;
	    continue;
	} else {
	  //	  fprintf(stderr, "drawing pts \n");
	      this->gsmap->drawminRectangle(iscan[i][0]-GP_SIZE,iscan[i][1]-GP_SIZE,
			iscan[i][0]+GP_SIZE,iscan[i][1]+GP_SIZE, height[i]);
	}
    }       

    Log::getStream(6)<<"for blob "<<scan->blobId<<" gs classification was: "<<endl<<str<<endl<<oldstr<<endl;

    if (road[blob->numPoints-1]==0)
	iscan[blob->numPoints-1][0] = RMAP_INVALID;

    memcpy(&(lastScan[index][0][0]),iscan,sizeof(int)*2*LADAR_BLOB_MAX_POINTS);
    shiftX = shiftY = 0;
}
