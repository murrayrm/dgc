s              Release Notes for "ladar-obs-perceptor" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "ladar-obs-perceptor" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "ladar-obs-perceptor" module can be found in
the ChangeLog file.

Release R1-00u (Wed Oct 24  4:02:49 2007):
	Changed association some to work better with cars, and tweaked classification some more. 
I'm going to be out in the field all day to see if this version performs well enough to use, 
otherwise probably will be reverted to 00s.

Release R1-00t (Tue Oct 23  7:43:36 2007):
	Updated geometry generation code so it isn't as jagged and ugly. Also fixed 
some other bugs; hopefully there won't be a static obstacle sitting on top of a 
dynamic one when we're trying to follow a car. Classification looks pretty good, 
but some work still needs to be done with association.

Release R1-00s (Sun Oct 21  1:31:47 2007):
	Hardly a release; some very minor changes. Just getting those out of the way before 
making some larger changes to the geometry handling to make the obstacles not look like crap.

Release R1-00r (Thu Oct 18 22:00:23 2007):
	Finally tracked down (no pun intended) dynamic obstacle classification 
problem. Tosin is now classified as dynamic just about immediately; the catch is 
that bushes and stuff might occasionally be classified as dynamic as well (but only 
for 0.5 seconds, and in those 0.5 seconds their velocity will be limited so 
prediction doesn't flip out). If there are any problems due to over-classification 
of dynamic obstacles, let me know immediately.
	I have not been able to reproduce the freespace segfault issue that was 
appearing in the field with the previous release; my hunch is that a vector is 
trying to allocate too much data because it goes into an infinite loop, so I 
added a check to stop that from happening, hopefully preventing a segfault or at 
least causing some different error so that the source can be tracked down better. 
If it happens again, please please take a screenshot of the map?

** In case it does happen, I have added a --disable-freespace flag that 
turns off all freespace-sending-related things, so the other fixes aren't for 
nothing.

	Still quite a few things left to fix, this is just to help tomorrow's 
testing.
	

Release R1-00q (Tue Oct 16  2:35:07 2007):
	Fixed a bug with the freespace polygon finding code.

Release R1-00p (Fri Oct 12  1:33:21 2007):
	Added sending of freespace element for intersection handling. Currently sends 
~2x a second with ~200 points; if this is a problem, this could be easily reduced.

Release R1-00o (Thu Oct  4  6:12:30 2007):
	Improvements in tracking: is now much more sensitive to cars and 
can typically pick them up at the full range of the ladars and track 
them well (though there are still some occasional snafus); it also no 
longer sends the static occupancy map obstacle belonging to a car so 
that planner won't slam on the brakes while we're following. There are 
more tracked objects and a few more false positives for car 
classification, but they typically disappear quickly.
	Other changes: added the ability to shrink obstacles in the 
occupancy map, via the command-line argument --obs-shrink=<int>. <int> 
is the number of pixels to shrink by (yeah, it's a quick hack), and 
shouldn't be much more than 2 or else you start losing obstacles (the 
extra 15 cm shouldn't have to matter). Also, now uses the upper bumper 
ladars for tracking as well.

Release R1-00n (Fri Sep 28 16:49:44 2007):
	Merge. And stuff. And some more tracking and groundstrike stuff.

Release R1-00m (Sun Sep 30  3:45:05 2007):
	First release of groundstrike filtering...not perfectly tuned, but seems to work reasonably. Changes:
	*check for no-returns before drawing into road-map
	*static map now checks against groundstrike prob before 
	painting points into map
	*'p' now removes red outlines as well 4-paned view now works
	(removed extraneous mutex locking)

	

Release R1-00l (Fri Sep 28 16:25:20 2007):
	adding back logging functionality (copied from planner)

Release R1-00k (Fri Sep 28 12:32:42 2007):
	Reverted a few changes for yesterday's testing, and added some 
more tracking features.

Release R1-00i-tamas (Thu Sep 27  1:21:25 2007):
	Just merging with Laura's changes.

Release R1-00i (Wed Sep 26 15:27:50 2007):
	NOT ready for use - I'm releasing Tamas's initial changes so I can work in my own branch. 

Release R1-00h (Wed Jun 13 18:15:39 2007):
  Increased map size for longer range perception.

Release R1-00g (Fri Jun  1  0:01:15 2007):
  Added all the necessary files this time; d'oh!

Release R1-00f (Thu May 31 22:23:48 2007):
  This is a major re-write of the obstacle perceptor to support data
  from the roof ladars.  The algorithms roof and bumper ladars are quite
  different.  For the bumper ladars, we look for ladar hits and assume they
  are obstacles (there is some code to limit the effect of ground strikes
  but these still generate false alarms).  For the roof ladars, we look
  for discontinuities in the scan due to stuff sticking up from the ground.
  Hits and discontinuities from multiple ladars and multiple scans are 
  fused into a simple grid map, with a simple exponential decay factor
  to force the map to forget about older data.  Obstacles are thresholded
  from the map and packaged into a MapElement message at periodic intervals.

Release R1-00e (Fri May 18 19:31:08 2007):
  Minor field tweaks.

Release R1-00d (Thu May 17  9:09:07 2007):
  Minor updates to support modified map API.

Release R1-00c (Sun May  6 14:16:29 2007):
  Added process control messages and fixed the fusion time criteria to
  handle out-of-order blob timestamps.

Release R1-00b (Thu Apr 26  0:34:40 2007):
  Better fusion criteria based on elapsed time.

Release R1-00a (Thu Apr 19 23:12:37 2007):
  Very basic but fully functional obstacle and free-space detector using
  the bumper ladars.

Release R1-00 (Tue Apr 17 17:31:13 2007):
	Created.





















