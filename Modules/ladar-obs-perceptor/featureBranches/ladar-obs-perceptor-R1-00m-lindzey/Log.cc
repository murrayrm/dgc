/*!
 * \file Log.cc
 * \brief Source code for logging in the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "Log.hh"

#include <math.h>

/** Initialize verbose level to maximum */
int Log::verbose_level = 9; 
NullStream Log::nStream;
bool Log::generic_logging = false;
ofstream Log::genericFileStream;
int Log::getVerboseLevel()
{
    return verbose_level;
}

void Log::setVerboseLevel(int level)
{
    verbose_level = level;
}

void Log::setGenericLogFile(const char *filename)
{
  genericFileStream.open(filename);
  if (genericFileStream.is_open()) {
    generic_logging = true;
    genericFileStream << "ladar-obs-perceptor log file [START]" << endl << endl;
  }
}

ostream& Log::getStream(int level)
{
    if (verbose_level >= level) {
      if (generic_logging) {
          return genericFileStream;
      } else {
        return nStream;
      }
    } else {
      return nStream;
    }
}
