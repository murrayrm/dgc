#include "LadarMap.hh"

void LadarMap::update()
{
    if (scans == 0)
    {
	usleep(10000);
	return;
    }

    for (int i=0;i<NUM_TRACK_LADARS;i++)
    {
	LadarScan *scan = scans->getPastScan(i,0);
	if (!scan->processed)
	    scan = scans->getPastScan(i,1);
	this->tracker[i].ProcessScan(scan);	
    }

    updateTracks();

    // send debug info
    if ((DGCgettime()-this->lastDebugTime) > MAP_DEBUG_RATE)
    {
	this->lastDebugTime = DGCgettime();	
	if (false)//(this->sendDebug)
	    for (int i=0;i<NUM_TRACK_LADARS;i++)
		this->tracker[i].sendDebug(this->debugTalker,this->debugSubgroup);
    }
    // send dyn obs
    if ((DGCgettime()-this->lastTrackTime) > MAP_TRACK_RATE)
    {
	sendTracks();
	this->lastTrackTime = DGCgettime();
    }
    // update obs map
    if ((DGCgettime()-this->lastStaticTime) > MAP_STATIC_RATE)
    {
	this->lastStaticTime = DGCgettime();
	this->staticMap->copyFrom(smap->getMap());
	this->smap->getMatrices(local2map,map2local);
	updateStatic();
	sendStatic();	
    }
    // do freespace
    if (this->sendFS &&
	(DGCgettime()-this->lastFreespaceTime) > MAP_FS_RATE)
    {
	this->lastFreespaceTime = DGCgettime();
	this->staticMap->copyFrom(smap->getMap());
	this->smap->getMatrices(local2map,map2local);
	// threshold properly
	this->staticMap->invthreshold(MAP_FS_THRESH);
	point2arr fs = this->getFreespace();
	this->sendFreespace(fs);
    }
    usleep(1);
}

void LadarMap::updateTracks()
{
    int lastBlob[NUM_TRACK_LADARS];
    for (int i=0;i<NUM_TRACK_LADARS;i++)
	lastBlob[i] = tracker[i].getBlobId();

    for (int i=0;i<MAP_MAX_TRACKS;i++)
	track[i].numSegments = 0;

    double maxscore,d;
    int index;

    // associate every track in every ladar (it's not that many)
    for (int i=0;i<NUM_TRACK_LADARS;i++)
    {
	for (int j=0;j<tracker[i].getHighestTrack();j++)
	{
	    TrackedSegment& ts = tracker[i].getTrack(j);
	    if (ts.status <= 0)
		continue;
	    // make sure geometry exists also
	    if (ts.segment[0].blobId != ts.segment[0].scan->blobId)
	      {
		ts.lastMapTrack = -1;
		ts.status = -1;
      		printf("invalid scan -----------------\n");
	        continue;
	      }
	    // check prev. association
	    if (ts.lastMapTrack != -1 && track[ts.lastMapTrack].status > 0)
	    {
		d = track[ts.lastMapTrack].pos.dist(point2(ts.x[0],ts.y[0]));
		if (d < MAP_ASSOC_DIST)
		{
		    addSegment(ts.lastMapTrack,i,j);
		    continue; //still good
		}
	    }
	    // none found, check for new ones
	    index = -1;
	    maxscore = 0;
	    for (int k=0;k<MAP_MAX_TRACKS;k++)
	    {
		if (track[k].status <= 0)
		    continue;
		d = getScore(k,ts);
		if (d > maxscore)
		{
		    index = k;
		    maxscore = d;
		}
	    }
	    // found one, make sure one of our statuses is high-ish
	    if (index > -1 && (ts.status > MAP_SEG_MINSTATUS || 
		    track[index].status > MAP_LIVE_THRESH))
	    {
		ts.lastMapTrack = index;
		addSegment(index,i,j);
	    }
	    else // didn't find one, create?
	    {
		testCreateTrack(ts,i,j);
	    }
	}
    }

    int freshest[NUM_TRACK_LADARS];

    // now every track should be created/associated... we need to do confidences
    // and update geometries
    int n;
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status <= 0)
	    continue;
	track[i].geometry.clear();
	// find freshest blob for each ladar
	memset(freshest,0,sizeof(int)*NUM_TRACK_LADARS);
	for (int j=0;j<track[i].numSegments;j++)
	{
	    int ladarId = track[i].segment[j][0];
	    int blobId = tracker[ladarId].getTrack(track[i].segment[j][1]).segment[0].blobId;
	    if (blobId > freshest[ladarId])
		freshest[ladarId] = blobId;	    
	}
	track[i].status--;
	// shift old positions
	memmove(track[i].pastpos+1,track[i].pastpos,sizeof(point2)*(MAP_PAST_POS-1));
	track[i].pastpos[0] = track[i].pos;
	track[i].numPastPos++;
	track[i].pos = point2();
	track[i].vel = point2();
	double totalweight = 0;
	double weight;
	double tvel;
	point2 minvel=point2(1000,1000);
	point2 maxvel=point2(-1000,-1000);
	for (int j=0;j<track[i].numSegments;j++)
	{
	    int ladarId = track[i].segment[j][0];
	    TrackedSegment& ts = tracker[ladarId].getTrack(track[i].segment[j][1]);
	    // don't use blob if we have a newer one from same ladar
	    if (ts.segment[0].blobId < freshest[ladarId])
		continue;
	    // increment if fresh
//	    if (ts.segment[0].blobId == lastBlob[ladarId])
//		track[i].status++;
	    // also inc for foreground
	    if (ts.segment[0].flags == FG_B)
		track[i].status++;

	    // increment if high confidence, dec if not
	    if (ts.status > MAP_STATUS_THRESH)
		track[i].status++;
	    else
		track[i].status--;

	    // inc like crazy for reflectivity
	    if (ts.numReflective > 0)
		track[i].status += 3;

	    // car confidence metrics
	    if (!(ts.carConf & CAR_SIZE))
		track[i].status-=3;
	    if (!(ts.carConf & CAR_CORNERS))
		track[i].status--;
	    
	    // find min and max vel
	    if (ts.x[1] < minvel.x)
		minvel.x = ts.x[1];
	    else if (ts.x[1] > maxvel.x)
		maxvel.x = ts.x[1];
	    if (ts.y[1] < minvel.y)
		minvel.y = ts.y[1];
	    else if (ts.y[1] > maxvel.y)
		maxvel.y = ts.y[1];	  		

	    n = (ts.segment[0].end-ts.segment[0].start+1);
	    // scale by velocity magnitude, weight smaller ones more heavily
	    tvel = sqrt(point2(ts.x[1],ts.y[1]).norm());
	    if (tvel > 0)
		weight = n*ts.status/tvel;
	    else
		weight = n*ts.status;
	    // add pos,vel weighted by # points & conf
	    track[i].pos = track[i].pos + weight*point2(ts.x[0],ts.y[0]);
	    track[i].vel = track[i].vel + weight*point2(ts.x[1],ts.y[1]);
	    totalweight += weight;
	    // update geometry, why not
	    point2arr pts = tracker[ladarId].getSegmentPoints(ts.segment[0]);
	    for (size_t k=0;k<pts.size();k++)
		track[i].geometry.push_back(pts[k]);
	}
	track[i].pos = track[i].pos / totalweight;
	track[i].vel = track[i].vel / totalweight;
	// if (very!) high velocity variability, decrement
	if ((maxvel.x-minvel.x)*(maxvel.y-minvel.y) > MAP_MAX_VAREA)
	    track[i].status -= 0;

	if (track[i].status > MAP_MAX_STATUS)
	    track[i].status = MAP_MAX_STATUS;

	// check if we're moving, after all that crap
	checkMoving(i);
/*	if (isMoving(i))
	{
	    track[i].seenMoving = true;
	    track[i].lastMoving = DGCgettime();
	    }*/

	if (track[i].status <= 0)
	    removeTrack(i);
    }
}

void LadarMap::sendTracks()
{
    MapElement el;
    MapElement elVel;
    MapElement elClear;

    el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_YELLOW;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    elClear.setTypeClear();

    elVel.setFrameTypeLocal();
    elVel.plotColor = MAP_COLOR_RED;
    elVel.setTypeLine();

    el.setTypePoints();

    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status < 0)
	{	    
	    // send a clear
	    elClear.setId(this->moduleId,i+MAP_MAX_STATIC);
	    this->mapTalker.sendMapElement(&elClear,sendSubgroup);
	    if (this->sendDebug)
	    {
		this->debugTalker.sendMapElement(&elClear,debugSubgroup);
		elClear.setId(this->moduleId,i+MAP_MAX_STATIC+MAP_MAX_TRACKS);
		this->debugTalker.sendMapElement(&elClear,debugSubgroup);
	    }
	    track[i].status = 0;
	}
	else if (track[i].status > 0)
	{
	    if (track[i].geometry.size() == 0)
		continue;

	    if (track[i].seenMoving)
		el.setTypeVehicle();
	    else if (track[i].status > MAP_LIVE_THRESH)
		el.setTypeObstacle();
	    else
	      continue;

	    if (track[i].lastMoving > 0)
		el.timeStopped = 0.000001*(int)(DGCgettime()-track[i].lastMoving);

	    el.velocity = track[i].vel;
	    // decrease velocity if we just saw it
	    if (0.000001*(int)(DGCgettime()-track[i].firstSeen) < MAP_DYN_TIME)
		el.velocity = el.velocity*0.1;

	    point2arr geo = makeGeometry(i);
	    el.setGeometry(geo);
	    el.setId(this->moduleId,i+MAP_MAX_STATIC);

	    this->mapTalker.sendMapElement(&el,sendSubgroup);

	    if (this->sendDebug)
	    {
	      //el.plotValue = MIN((track[i].status*100)/MAP_MAX_STATUS,100);		
		if (track[i].seenMoving)
		    el.plotColor = MAP_COLOR_GREEN;
		else
		    el.plotColor = MAP_COLOR_YELLOW;
		el.typeConf = track[i].status;
		this->debugTalker.sendMapElement(&el,debugSubgroup);
		elVel.setId(this->moduleId,i+MAP_MAX_STATIC+MAP_MAX_TRACKS);
		point2arr geo;
		geo.push_back(track[i].pos);
		geo.push_back(track[i].pos+el.velocity);
		elVel.setGeometry(geo);
		if (!track[i].seenMoving)
		{
		    elClear.setId(this->moduleId,i+MAP_MAX_STATIC+MAP_MAX_TRACKS);
		    this->debugTalker.sendMapElement(&elClear,debugSubgroup);
		}
		else
		{
		    this->debugTalker.sendMapElement(&elVel,debugSubgroup);
		}
	    }
	    /*	    el.setId(this->moduleId+1,i);
	    el.setGeometry(obj[i].centroid);
	    el.setType(ELEMENT_POINTS);
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	    el.setTypeObstacle();*/
	}
    }
/*    el.clear();
    el.setTypeClear();
    for (;i<lastStaticCount;i++)
    {
	el.setId(this->moduleId,i);
	this->mapTalker.sendMapElement(&el,sendSubgroup);
	if (this->sendDebug)
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
    }
    lastStaticCount = staticObs.size();*/
}

bool LadarMap::checkMoving(int index)
{
    // no newbies
    if (track[index].status < MAP_LIVE_THRESH)
	return false;
    point2 min,max;
    track[index].geometry.bounds2(min,max);
    // no large moving things
    if (min.dist(max) > MAP_IGNORE_DIST*1.6)
    {
	track[index].seenMoving = false;
	return false;
    }

    bool moving = (track[index].vel.norm() > MAP_MIN_VEL);
    bool displaced = (track[index].firstpos.dist(track[index].pos) > MAP_MIN_DELTA);
    // change algorithm based on time seen
    double timeSeen = (DGCgettime() - track[index].firstSeen)/1000000.0;
    if (timeSeen > MAP_DYN_TIME)
    {
	// only set as moving if both conditions are satisfied
	track[index].seenMoving = track[index].seenMoving || 
	    (moving && displaced);
	// and unset as moving if we haven't actually moved
	if (!displaced)
	    track[index].seenMoving = false;
    }
    else
    {
	// send as moving even if displacement not met
	if (moving)
	    track[index].seenMoving = true;
    }

    if (moving)
	track[index].lastMoving = DGCgettime();
	
    return track[index].seenMoving;
}

void LadarMap::testCreateTrack(TrackedSegment& ts, int ladarId, int trackId)
{
    ts.lastMapTrack = -1;
    if (ts.status < MAP_SEG_MINSTATUS)
	return;
    // only create a track if we really, really want to
//    if (!(ts.carConf & CAR_SIZE))
//	return;
    // no single points
    if (ts.segment[0].start == ts.segment[0].end)
	return;
    // no invalid geometries
    if (ts.segment[0].scan->blobId != ts.segment[0].blobId)
	return;
    // except for now, just create it, who gives a crap
    if (ts.segment[0].flags == FG_B || ts.status > MAP_STATUS_THRESH)
    {
	int id = addTrack(ladarId, trackId);
	track[id].pos = point2(ts.x[0],ts.y[0]);
	track[id].firstpos = track[id].pos;
	track[id].vel = point2(ts.x[1],ts.y[1]);
	track[id].geometry = tracker[ladarId].getSegmentPoints(ts.segment[0]);
	ts.lastMapTrack = id;
    }
}

void LadarMap::updateStatic()
{
    // Threshold, dilate, and erode... ta-da
    staticMap->threshold(MAP_THRESH_VAL);
    staticMap->dilate(MAP_DE_VAL);
    staticMap->erode(MAP_DE_VAL+this->obsShrink);
    // Now, make the map into vectors n stuff
    vector<point2arr> poly = vectorizeMap();
    // and now associate
    for (size_t i=0;i<poly.size();i++)
    {
	point2 centroid = getCentroid(poly[i]);
	double mindist = 100000;
	int minindex = -1;
	for (int j=0;j<MAP_MAX_STATIC;j++)
	{
	    if (obj[j].status != 1)
		continue;
	    // find closest other centroid
	    double dist = centroid.dist(obj[j].centroid);
	    if (dist < mindist)
	    {
		mindist = dist;
		minindex = j;
	    }
	}
	if (minindex >= 0 && mindist < MAP_MAX_ASSOC)
	{
	    updateStatic(minindex,poly[i].simplify(MAP_SIMPLIFY_DIST));
	}
	else
	{
	    addStatic(poly[i].simplify(MAP_SIMPLIFY_DIST));
	}
    }
    // alright, we've either associated or added what we need to, now prune some
    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status == 1) //wasn't associated, remove
	    removeStatic(i);
	else if (obj[i].status == 2)
	    obj[i].status = 1; //prepare for next cycle
    }
    // now, quick check to make sure we aren't sending a static obs
    // along with a dynamic one
    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status < 1)
	    continue;
	for (int j=0;j<MAP_MAX_TRACKS;j++)
	{
	    if (track[j].status < 1 || !track[j].seenMoving)
		continue;
	    if (track[j].pos.dist(obj[i].centroid) > MAP_MAX_DYNCDIST)
		continue;
	    if (track[j].geometry.size() == 0)
		continue;
	    // close enough to do poly-poly check
	    if (track[j].geometry.get_min_dist_poly(obj[i].geometry) < MAP_MAX_DYNSDIST)
	    {
		removeStatic(i);
		break;
	    }
	}
    }
}

void LadarMap::sendStatic()
{
    MapElement el;
    MapElement elClear;

    el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_PURPLE;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    elClear.setTypeClear();

    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status == -1)
	{	    
	    // send a clear
	    elClear.setId(this->moduleId,i);
	    this->mapTalker.sendMapElement(&elClear,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&elClear,debugSubgroup);
	    obj[i].status = 0;
	}
	else if (obj[i].status == 1)
	{
	    el.setGeometry(obj[i].geometry);
	    el.setId(this->moduleId,i);
	    this->mapTalker.sendMapElement(&el,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&el,debugSubgroup);
	    /*	    el.setId(this->moduleId+1,i);
	    el.setGeometry(obj[i].centroid);
	    el.setType(ELEMENT_POINTS);
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	    el.setTypeObstacle();*/
	}
    }
/*    el.clear();
    el.setTypeClear();
    for (;i<lastStaticCount;i++)
    {
	el.setId(this->moduleId,i);
	this->mapTalker.sendMapElement(&el,sendSubgroup);
	if (this->sendDebug)
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
    }
    lastStaticCount = staticObs.size();*/
}

void LadarMap::sendFreespace(point2arr& pts)
{
    MapElement el;

    if (pts.size() == 0)
	return;

    el.setFrameTypeLocal();
    el.setTypeFreeSpace();
    el.height = 1;
    el.plotColor = MAP_COLOR_RED;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    el.setGeometry(pts);
    el.setId(this->moduleId,MAP_MAX_TRACKS + MAP_MAX_STATIC + 100);
    this->mapTalker.sendMapElement(&el,sendSubgroup);
    if (this->sendDebug)
	this->debugTalker.sendMapElement(&el,debugSubgroup);
}

double LadarMap::getScore(int index, TrackedSegment& ts)
{
    if (track[index].pos.dist(point2(ts.x[0],ts.y[0])) > MAP_ASSOC_DIST)
	return 0;
    double mindist=MAP_ASSOC_DIST;
    double d;
    // find nearest pair of points, yeah i know it's a hack, oh well
    for (size_t i=0;i<track[index].geometry.size();i++)
    {
	for (int j=ts.segment[0].start;j<=ts.segment[0].end;j++)
	{
	    d = track[index].geometry[i].dist(point2(ts.segment[0].scan->points[j].x,
						     ts.segment[0].scan->points[j].y));
	    mindist = MIN(mindist,d);
	}
    }
    if (mindist > MAP_MAX_PPDIST)
	return 0;
    // return a score where higher is better
    return 1000-mindist;
}

void LadarMap::addStatic(point2arr geo)
{
    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status == 0)
	{
	    obj[i].status = 2;
	    obj[i].geometry = geo;
	    obj[i].centroid = getCentroid(geo);
	    return;
	}
    }
}

void LadarMap::removeStatic(int id)
{
    obj[id].status = -1;
}

void LadarMap::updateStatic(int id, point2arr geo)
{
    obj[id].geometry = geo;
    obj[id].centroid = getCentroid(geo);
    obj[id].status = 2;
}

int LadarMap::addTrack(int ladarId, int trackId)
{
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status == 0)
	{
	    track[i].status = MAP_INIT_STATUS;
	    track[i].segment[0][0] = ladarId;
	    track[i].segment[0][1] = trackId;
	    track[i].numSegments = 1;
	    track[i].seenMoving = false;
	    track[i].lastMoving = 0;
	    track[i].numPastPos = 0;
	    track[i].firstSeen = DGCgettime();
	    
	    return i;
	}
    }
    fprintf(stderr,"Could not craete track!");
    return 0;
}

void LadarMap::removeTrack(int index)
{
    track[index].status = -1;
}

void LadarMap::addSegment(int index, int ladarId, int trackId)
{
    int id = track[index].numSegments;
    track[index].segment[id][0] = ladarId;
    track[index].segment[id][1] = trackId;
    track[index].numSegments++;
}

void LadarMap::removeSegment(int index, int id)
{
    // want to remove a certain el from the track
    memmove(track[index].segment[id],track[index].segment[id+1],sizeof(int)*2*(MAP_TRACK_SEGS-id-1));
    track[index].numSegments--;
}

point2arr LadarMap::makeGeometry(int index)
{
  LadarScan *scan = scans->getPastScan(0,0);
  if (!scan->processed)
    scan = scans->getPastScan(0,1);
  point2 obsdir = track[index].pos - point2(scan->ladarPos.x,scan->ladarPos.y);
  point2 perp(obsdir.y,-obsdir.x);
  for (size_t i=0;i<track[index].geometry.size();i++)
    track[index].geometry[i].z = perp.dot(track[index].geometry[i]-track[index].pos);
  qsort(&(track[index].geometry[0]),track[index].geometry.size(),sizeof(point2),&qcmp);
  point2arr geometry = track[index].geometry.simplify(MAP_SIMPLIFY_DIST);
  if (geometry.size() <= 2)
    return geometry;
  for (size_t i = geometry.size()-2; i>0; i--)
    if (perp.cross(geometry[i]-track[index].pos) > 0)
      geometry.push_back(geometry[i]);
  return geometry;
}

inline point2 LadarMap::getCentroid(point2arr& ptarr)
{
    // find centroid by center-of-mass of a closed poly
    double A=0;
    for (size_t i=0;i<ptarr.size()-1;i++)
	A += (ptarr[i].x*ptarr[i+1].y - ptarr[i+1].x*ptarr[i].y);
    A += ptarr.back().x*ptarr[0].y - ptarr[0].x*ptarr.back().y;
    A *= 0.5;
    point2 c = point2(0,0);
    for (size_t i=0;i<ptarr.size()-1;i++)
    {
	c.x += (ptarr[i].x+ptarr[i+1].x)*(ptarr[i].x*ptarr[i+1].y-ptarr[i+1].x*ptarr[i].y);
	c.y += (ptarr[i].y+ptarr[i+1].y)*(ptarr[i].x*ptarr[i+1].y-ptarr[i+1].x*ptarr[i].y);
    }
    c.x += (ptarr.back().x+ptarr[0].x)*(ptarr.back().x*ptarr[0].y-ptarr[0].x*ptarr.back().y);
    c.y += (ptarr.back().y+ptarr[0].y)*(ptarr.back().x*ptarr[0].y-ptarr[0].x*ptarr.back().y);
    c.x *= (1.0/(6*A));
    c.y *= (1.0/(6*A));
    return c;
}

point2arr LadarMap::getFreespace()
{
    int width = staticMap->getWidth();
    emap_t *pix = staticMap->getData();
    emap_t *curPix;
    int curDir;
    int lastDir;
    point2arr freespace;
    point2 alicepos;

    int n = 0;
    
    alicepos = makePoint(width/2 + width*width/2);

    // set edges of map to 0 to avoid edge issues
    for (int i=0;i<width;i++)
    {
	*(pix+i) = 0;
	*(pix+i*width) = 0;
	*(pix+i*width+width-1) = 0;
	*(pix+width*width-i-1) = 0;
    }

    //start searching at alice's location (center of map)
    pix = staticMap->getData() + width/2 + (width/2)*width;

    // loop until edge of freespace poly reached
    for (int i=0;i<width/2-1;i++)
    {
	// found the edge of polygon, start looping through
	if (*pix == 0xFFFF && *(pix+1) == 0x0)
	{
	    // get starting index
	    int ind = (pix-staticMap->getData());
	    freespace.clear();
	    freespace.push_back(makePoint(ind));
	    n = 1;
	    curPix = pix;
	    *pix = 1; // set start location
	    curDir = moveDir(curPix,1,width);
	    lastDir = 0;
	    while (*curPix > 1)
	    {
		ind += curDir;
		if (curDir != lastDir)
		{
		    freespace.push_back(makePoint(ind));
		    n++;
		    // sanity check
		    if (n > 50000)
		    {
			fprintf(stderr,"Freespace poly too big!\n");
			freespace.clear();
			return freespace;
		    }
		}
		// mark that we have been here
		(*curPix)--;
		lastDir = curDir;
		curDir = moveDir(curPix,lastDir,width);
	    }
	    // forget it if we don't know
	    if (freespace.size() < 3)
	    {
		pix++;
		continue;
	    }
	    // make sure our starting point is inside of the poly
	    if (!freespace.is_inside_poly(alicepos))
	    {
		pix++;
		continue;
	    }

	    // we found a poly containing alice's location
	    return freespace.simplify(MAP_SIMPLIFY_DIST*5);
	}
	pix++;
    }

    // no poly found (this should not happen?)
    freespace.clear();
    return freespace;
}

vector<point2arr> LadarMap::vectorizeMap()
{
    int width = staticMap->getWidth();
    emap_t *pix = staticMap->getData();
    emap_t *end = pix + width*width;
    emap_t *curPix;
    int curDir;
    int lastDir;
    vector<point2arr> polys;

    // set edges of map to 0 to avoid edge issues
    for (int i=0;i<width;i++)
    {
	*(pix+i) = 0;
	*(pix+i*width) = 0;
	*(pix+i*width+width-1) = 0;
	*(pix+width*width-i-1) = 0;
    }

    // loop through map
    while (pix < end)
    {
	if (*pix == 0)
	{
	    pix++;
	    continue;
	}
	// found a polygon, start looping through
	else if (*pix == 0xFFFF && *(pix-1) == 0) // new polygon, == 0 to avoid doing donuts
	{
	    // get starting index
	    int ind = (pix-staticMap->getData());
	    point2arr ptarr;
	    ptarr.push_back(makePoint(ind));
	    curPix = pix;
	    *pix = 1; // set start location
	    curDir = moveDir(curPix,1,width);
	    lastDir = 0;
	    // eliminate wrong dir polys
	    if (curDir < 0)
	    {
		*curPix = 0; // skip loop
		ptarr.clear(); // don't add
	    }
	    while (*curPix > 1)
	    {
		ind += curDir;
		if (curDir != lastDir)
		    ptarr.push_back(makePoint(ind));
		// mark that we have been here
		(*curPix)--;
		lastDir = curDir;
		curDir = moveDir(curPix,lastDir,width);
	    }
	    if (ptarr.size() > 1)
	    {
		polys.push_back(ptarr);
	    }
	    else if (ptarr.size() == 1)
	    {
		// give it a tiny bit of size, just in case
		ptarr.push_back(ptarr[0] + point2(0.1,0));
		ptarr.push_back(ptarr[0] + point2(0,0.1));
		polys.push_back(ptarr);
	    }

	    // now move to end of poly
	    while (*pix != 0)
		pix++;
	}
	else
	{
	    while (*pix != 0) // have already done this poly
		pix++;
	}
    }

    return polys;
}

inline point2 LadarMap::makePoint(int ind)
{
    int x = (ind & (EMAP_SIZE-1));
    int y = (ind >> EMAP_BITS);
    return point2(x*map2local[0][0]+map2local[0][3],y*map2local[1][1]+map2local[1][3]);
}

inline int LadarMap::moveDir(emap_t* &pix, int lastDir, int width)
{
    emap_t *lastPix=pix;
    switch (lastDir)
    {
    case -EMAP_SIZE:
	pix += width - 1;
	lastPix = pix + 1;
	goto bl;
    case -EMAP_SIZE+1:
	pix--;
	lastPix = pix + width;
	goto lt;
    case 1:
	pix -= (width+1);
	lastPix = pix + width;
	goto ul;
    case EMAP_SIZE+1:
	pix -= width;
	lastPix = pix - 1;
	goto up;
    case EMAP_SIZE:
	pix -= (width-1);
	lastPix = pix - 1;
	goto ur;
    case EMAP_SIZE-1:
	pix ++;
	lastPix = pix - width;
	goto rt;
    case -1:
	pix += width + 1;
	lastPix = pix - width;
	goto br;
    case -EMAP_SIZE-1:
	pix += width;
	lastPix = pix + 1;
	goto bt;	
    }
up:
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;
ur:
    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;
rt:
    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;
br:
    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;
bt:
    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;
bl:
    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;
lt:
    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;
ul:
    if (*lastPix == 0 && *pix > 0)
	return -width-1;
    lastPix = pix;
    pix++;
// now for wrap-around calls
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;

    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;

    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;

    return 0;
}

