
/* 
 * Desc: Ladar scan map
 * Date: 17 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SCANMAP_H
#define SCANMAP_H

#include <interfaces/LadarRangeBlob.h>
#include "gpc.h"

#if defined __cplusplus
extern "C"
{
#endif


/** @file

@brief Fused ladar scans into a simple map.

*/


/// @brief Data for each individual ladar
typedef struct
{
  /// Number of scans in the current map
  int scan_count;

  /// Combined free-space polygon
  gpc_polygon space;
  
} scanmap_ladar_t;

  
/// @brief Map cell data
typedef struct
{
  uint16_t hits;
  uint16_t scan;  
} scanmap_cell_t;
  

/// @brief Map data
typedef struct 
{
  // Ladar data
  scanmap_ladar_t ladars[64];
  
  // Fused free-space polygon
  gpc_polygon fused_space;

  // Map scale (m/cell)
  float map_scale;

  // Map size in each dimension
  int map_size;

  // Transform from local to map frame
  float loc2map[4][4];

  // Transform from grid to local frame
  float map2loc[4][4];

  // Map data
  scanmap_cell_t *cells;

  // Scan counter; used to limit the each cell to one hit per scan.
  int scan_count;
  
} scanmap_t;


  
/// @brief Allocate new display
scanmap_t *scanmap_alloc();

/// @brief Free display
void scanmap_free(scanmap_t *self);

/// @brief Clear the map.
int scanmap_clear(scanmap_t *self, const VehicleState *state);

/// @brief Add a ladar scan to the map
int scanmap_add(scanmap_t *self, LadarRangeBlob *blob);

// Update the free-space polygon
int scanmap_add_free(scanmap_t *self, LadarRangeBlob *blob);

// Update the obstacle map
int scanmap_add_obs(scanmap_t *self, LadarRangeBlob *blob);

/// @brief Construct the final fused map
int scanmap_fuse(scanmap_t *self);


/// @brief Transfrom from map position to map index
static __inline__ void scanmap_p2i(scanmap_t *self, float px, float py, int *mi, int *mj)
{
  *mi = (int) (px/self->map_scale + 10000000) - 10000000 + self->map_size/2;
  *mj = (int) (py/self->map_scale + 10000000) - 10000000 + self->map_size/2;
  return;
}


/// @brief Transfrom from map index to map position
static __inline__ void scanmap_i2p(scanmap_t *self, int mi, int mj, float *px, float *py)
{
  *px = (mi - self->map_size/2 + 0.5) * self->map_scale;
  *py = (mj - self->map_size/2 + 0.5) * self->map_scale;
  return;
}


/// @brief Return the cell at the given map index
static __inline__ scanmap_cell_t *scanmap_cell_index(scanmap_t *self, int mi, int mj)
{
  if (mi < 0 || mi >= self->map_size)
    return NULL;
  if (mj < 0 || mj >= self->map_size)
    return NULL;
  return self->cells + mi + mj * self->map_size;
}

  
/// @brief Return the cell at the given map position
static __inline__ scanmap_cell_t *scanmap_cell_pos(scanmap_t *self, float px, float py)
{
  int mi, mj;
  scanmap_p2i(self, px, py, &mi, &mj);
  return scanmap_cell_index(self, mi, mj);
}

  
#if defined __cplusplus
}
#endif

#endif
