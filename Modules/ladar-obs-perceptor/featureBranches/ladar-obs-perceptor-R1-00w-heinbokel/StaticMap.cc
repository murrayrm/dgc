#include "StaticMap.hh"


StaticMap::StaticMap(LadarScanStore *store)
{
    memset(this,0,sizeof(*this));
    this->map = new EMap(EMAP_SIZE);
    this->map->clear(SMAP_CLEAR);
    this->testmap = new EMap(EMAP_SIZE);
    this->testmap->clear(SMAP_CLEAR);
    this->scans = store;
}

StaticMap::~StaticMap()
{
    delete this->map;
    delete this->testmap;
}

void StaticMap::shiftMap(int dx, int dy, float l2m[4][4])
{
    // set our transform matrices
    memcpy(local2map,l2m,16*sizeof(float));
    mat44f_inv(map2local,local2map);
    // and shift/clear the map
    this->map->shiftClear(dx,dy,SMAP_CLEAR);
    this->testmap->shiftClear(dx,dy, SMAP_CLEAR);
}

void StaticMap::drawScan(LadarScan *scan)
{
    float sx, sy, sz;
    float mat[4][4];
    Point2 ladarpoints[LADAR_BLOB_MAX_POINTS*2 + 1];
    Point2 *curPoint = ladarpoints;
    LadarRangeBlob *blob = &(scan->blob);

    mat44f_mul(mat, blob->veh2loc, blob->sens2veh);
    mat44f_mul(mat, local2map, mat);

    LadarRangeBlobScanToSensor(blob, 0.0, 0.0, &sx, &sy, &sz);

    curPoint->x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
    curPoint->y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
    curPoint++;

    float pr;

//    char tmp[16];
//    string str;
//    str.clear();
    // find last scan used for the map
    LadarScan *lastProc;
    for (int i=0;i<NUM_SCANS;i++)
    {
	lastProc = scans->getPastScan(scan->ladarId,i);
	if (lastProc->usedForUpdate)
	    break;
    }

    for (int i=0; i<blob->numPoints; i++)
    {
	// Limit very large range values
	if (blob->points[i][RANGE] > SMAP_MAX_DIST)
	    pr = SMAP_NORETURN;
	else
	    pr = blob->points[i][RANGE];

	//create free-space polygon
	LadarRangeBlobScanToSensor(blob, blob->points[i][ANGLE]-SMAP_DA,pr-SMAP_DR, &sx, &sy, &sz);
	curPoint->x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
	curPoint->y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
	curPoint++;

	LadarRangeBlobScanToSensor(blob, blob->points[i][ANGLE]+SMAP_DA,pr-SMAP_DR, &sx, &sy, &sz);
	curPoint->x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
	curPoint->y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
	curPoint++;

	if (pr == SMAP_NORETURN)
	    continue;
	if (pr < SMAP_MIN_DIST)
	    continue;

	LadarRangeBlobScanToSensor(blob, blob->points[i][ANGLE], pr, &sx, &sy, &sz);

	// if we previously saw a dyn. obs here, erase it
	if (lastProc->isDynamic[i])
	{
	    int mx = (int)(local2map[0][0]*lastProc->points[i].x + local2map[0][3]);
	    int my = (int)(local2map[1][1]*lastProc->points[i].y + local2map[1][3]);
	    int bs = SMAP_DW + 1;
	    this->map->decRectangle(mx-bs,my-bs,mx+bs,my+bs,2*SMAP_INC);
//	    this->testmap->drawRectangle(mx-bs,my-bs,mx+bs,my+bs,63000);
	}

	int tx = (int)(mat[0][0]*sx + mat[0][1]*sy + mat[0][3]);
	int ty = (int)(mat[1][0]*sx + mat[1][1]*sy + mat[1][3]);

	// not no-return, so draw in point itself if it isn't a groundstrike
	double gsConf = scan->gsRoadConf[i];//*scan->gsRoofConf[i];
	if (gsConf > 0.8) {
//	  sprintf(tmp,"%1.2f,",gsConf);
//	  str.append(tmp);
	    //if it's a groundstrike, draw it in the test map
	    this->testmap->drawRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,1000);
	} else {
	    this->map->incRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,SMAP_INC);
	}
    }
//    Log::getStream(6)<<"for blob "<<scan->blobId<<" gs probs were: "<<str<<endl;
    //free-space decrement
    this->map->decPolygon(blob->numPoints*2+1, ladarpoints, SMAP_DEC);
}

// clear based on constant time; fades to "no info" (fade out old objects)
void StaticMap::timeDec()
{
    this->map->moveToCenter(SMAP_CLEAR,SMAP_TIME_DEC);
}
