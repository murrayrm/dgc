
/* 
 * Desc: Ladar obstacle map
 * Date: 30 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <frames/pose3.h>
#include <frames/mat44.h>

#include "obsmap.h"


// Compute residual from local mean elevation
float obsmap_mean_residual(obsmap_t *self, int oi,
                         int numPoints, float points[LADAR_BLOB_MAX_POINTS][5]);

// Compute a local linear fit.
float obsmap_linear_residual(obsmap_t *self, int oi,
                           int numPoints, float points[LADAR_BLOB_MAX_POINTS][5]);


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Allocate new object
obsmap_t *obsmap_alloc()
{
  obsmap_t *self;

  // MAGIC
  self = calloc(1, sizeof(obsmap_t));
  self->scale = 0.20; // TESTING 0.40; 
  self->size = 320; // TESTING 196;  
  self->cells = calloc(self->size * self->size, sizeof(self->cells[0]));
  self->roof_decay_const = 0.1;
  self->bumper_decay_const = 2.0; 
 
  return self;
}


// Free object
void obsmap_free(obsmap_t *self)
{
  free(self->cells);
  free(self);
  
  return;
}


// Clear the map
int obsmap_clear(obsmap_t *self, const VehicleState *state)
{
  pose3_t pose;
    
  // Construct map pose from the local pose
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);
  self->pose = pose;

  // Construct local<>map transforms
  pose3_to_mat44f(pose, self->trans_lm);
  mat44f_inv(self->trans_ml, self->trans_lm);

  // Clear the obstacle map
  memset(self->cells, 0, self->size * self->size * sizeof(self->cells[0]));
  
  return 0;
}


// Move the map center but keep the data
int obsmap_move(obsmap_t *self, const VehicleState *state)
{
  pose3_t pose, dpose;
  int mi, mj, si, sj, di, dj;
  obsmap_cell_t *src, *dst;

  // Construct local pose
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);

  // Compute the number of cells to move
  dpose = pose3_mul(pose3_inv(self->pose), pose);
  mi = (int) (dpose.pos.x / self->scale);
  mj = (int) (dpose.pos.y / self->scale);

  // Dont do anything unless we have moved a significant distance
  if (abs(mi) < 4 && abs(mj) < 4) // MAGIC
    return 0;
  
  // Compute the new map pose
  dpose.pos.x = mi * self->scale;
  dpose.pos.y = mj * self->scale;
  dpose.rot = quat_ident();
  self->pose = pose3_mul(self->pose, dpose);

  // Construct local<>map transforms
  pose3_to_mat44f(self->pose, self->trans_lm);
  mat44f_inv(self->trans_ml, self->trans_lm);

  if (mi >= 0 && mj >= 0)
  {
    for (dj = 0; dj < self->size; dj++)
    {
      sj = dj + mj;            
      for (di = 0; di < self->size; di++)
      {
        si = di + mi;
        src = obsmap_cell_index(self, si, sj);
        dst = obsmap_cell_index(self, di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else if (mi >= 0 && mj < 0)
  {
    for (dj = self->size - 1; dj >= 0; dj--)
    {
      sj = dj + mj;            
      for (di = 0; di < self->size; di++)
      {
        si = di + mi;
        src = obsmap_cell_index(self, si, sj);
        dst = obsmap_cell_index(self, di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else if (mi < 0 && mj >= 0)
  {
    for (dj = 0; dj < self->size; dj++)
    {
      sj = dj + mj;            
      for (di = self->size - 1; di >= 0; di--)
      {
        si = di + mi;
        src = obsmap_cell_index(self, si, sj);
        dst = obsmap_cell_index(self, di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else if (mi < 0 && mj < 0)
  {
    for (dj = self->size - 1; dj >= 0; dj--)
    {
      sj = dj + mj;            
      for (di = self->size - 1; di >= 0; di--)
      {
        si = di + mi;
        src = obsmap_cell_index(self, si, sj);
        dst = obsmap_cell_index(self, di, dj);
        if (src && dst)
          memcpy(dst, src, sizeof(*dst));
        else
          memset(dst, 0, sizeof(*dst));
      }
    }    
  }
  else
  {
    assert(false);
  }
  
  return 0;
}


// Decay the map
int obsmap_decay(obsmap_t *self, uint64_t time)
{
  int i, j;
  obsmap_cell_t *cell;
  float da, db;

  if (self->decay_time == 0)
    self->decay_time = time;  
  if (time - self->decay_time < 100000)
    return 0;

  // Compute the decay factor based on the elapsed time.
  da = 1 - (self->roof_decay_const * (time - self->decay_time) * 1e-6);
  db = 1 - (self->bumper_decay_const * (time - self->decay_time) * 1e-6);
  //MSG("df %f %f", (time - self->decay_time) * 1e-6, df);
  
  // Adjust all of the cells
  for (j = 0; j < self->size; j++)
  {
    for (i = 0; i < self->size; i++)
    {
      cell = obsmap_cell_index(self, i, j);
      cell->mn *= da;
      cell->mz *= da;
      cell->res *= da;
      cell->bumper *= db;
    }
  }

  self->decay_time = time;
  
  return 0;
}


// Add a bumper ladar scan
int obsmap_add_bumper(obsmap_t *self, LadarRangeBlob *blob, float max_range)
{
  int i;
  float pb, pr;
  float sx, sy, sz;
  float mx, my, mz;
  float m[4][4];
  obsmap_cell_t *cell;
  
  // Compute transform from sensor frame to map frame
  mat44f_setf(m, self->trans_ml);
  mat44f_mul(m, m, blob->veh2loc);
  mat44f_mul(m, m, blob->sens2veh);

  // Unpack the points into x,y values
  for (i = 0; i < blob->numPoints; i++)
  {
    pb = blob->points[i][0];
    pr = blob->points[i][1];
        
    // Discard bogus range values
    if (pr < 0.1) // MAGIC
      continue;
    if (pr > max_range)
      continue;

    // Compute point in sensor frame
    LadarRangeBlobScanToSensor(blob, pb, pr, &sx, &sy, &sz);
        
    // Compute point in map frame
    mx = m[0][0]*sx + m[0][1]*sy + m[0][2]*sz + m[0][3];
    my = m[1][0]*sx + m[1][1]*sy + m[1][2]*sz + m[1][3];
    mz = m[2][0]*sx + m[2][1]*sy + m[2][2]*sz + m[2][3];

    // Get cell at end point
    cell = obsmap_cell_pos(self, mx, my);
    if (!cell)
      continue;

    // Update cell stats.  We only increment the hit count once for
    // each cell, so the hit count tells us how many scans hit this
    // cell (as opposed to getting lots of hits in one scan).
    if (cell->scan_count == self->scan_count)
      continue;

    cell->scan_count = self->scan_count;
    cell->bumper += 1;
  }

  self->scan_count += 1;
    
  return 0;
}


// Sort comparison function for ladar points.
// This sorts on the y-value in the sensor frame.
int obsmap_sortfn(float a[5], float b[5])
{
  //MSG("sort %f %f", a[1], b[1]);
  if (a[1] < b[1])
    return -1;
  if (a[1] > b[1])
    return +1;  
  return 0;
}


// Add a roof ladar scan
int obsmap_add_roof(obsmap_t *self, LadarRangeBlob *blob, float max_range)
{
  int i;
  float pb, pr;
  float sx, sy, sz;
  float vx, vy, vz;
  float lx, ly, lz;  
  float mx, my, mz;
  float m[4][4];
  obsmap_cell_t *cell;
  float res;
  int numPoints;
  float points[LADAR_BLOB_MAX_POINTS][5];

  // Compute transform from sensor frame to map frame
  mat44f_setf(m, self->trans_ml);
  mat44f_mul(m, m, blob->veh2loc);
  mat44f_mul(m, m, blob->sens2veh);

  // Unpack the points into x,y values
  numPoints = 0;
  for (i = 0; i < blob->numPoints; i++)
  {
    pb = blob->points[i][0];
    pr = blob->points[i][1];
        
    // Discard bogus range values
    if (pr < 0.1) // MAGIC
      continue;
    if (pr > max_range)
      continue;

    // Convert from bearing/range to point in local frame
    LadarRangeBlobScanToSensor(blob, pb, pr, &sx, &sy, &sz);
    LadarRangeBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);
    LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);

    // Store position in sensor and local frames.
    assert(numPoints < LADAR_BLOB_MAX_POINTS);
    points[numPoints][0] = sx;
    points[numPoints][1] = sy;
    points[numPoints][2] = lx;
    points[numPoints][3] = ly;
    points[numPoints][4] = lz;
    numPoints++;
  }

  // Sort the points by y-value (across the scan)
  qsort(points, numPoints, sizeof(points[0]), (void*) obsmap_sortfn);
    
  for (i = 0; i < numPoints; i++)
  {
    sx = points[i][0];
    sy = points[i][1];
    sz = 0;
    
    // Compute point in map frame
    mx = m[0][0]*sx + m[0][1]*sy + m[0][2]*sz + m[0][3];
    my = m[1][0]*sx + m[1][1]*sy + m[1][2]*sz + m[1][3];
    mz = m[2][0]*sx + m[2][1]*sy + m[2][2]*sz + m[2][3];

    // Get cell at end point
    cell = obsmap_cell_pos(self, mx, my);
    if (!cell)
      continue;

    // Compute residual from the local mean elevation.
    res = obsmap_mean_residual(self, i, numPoints, points);

    // Using fused Z is BAD BAD BAD; for display purposes only.
    cell->mn += 1;
    cell->mz += mz;
    cell->res += res;

    //printf("%f %f %f ", lx, ly, lz);
    //printf("%f\n", res);
  }
  //printf("\n\n");

  self->scan_count += 1;
  
  return 0;
}


// Compute residual from the local mean elevation, where "local" means
// neighboring points in the scan.  This works well for narrow poles
// and traffic cones on flat surfaces.  It will also generate high
// residuals on slopes.
float obsmap_mean_residual(obsmap_t *self, int oi,
                         int numPoints, float points[LADAR_BLOB_MAX_POINTS][5])
{
  int pi;
  float oy, oz, sy, lz;
  float mn, mz;
  float res;
  float win;

  win = 2.0/2;  // MAGIC

  oy = points[oi][1];
  oz = points[oi][4];
  
  mn = 0;
  mz = 0;
  
  // Search backward from the key point
  for (pi = oi - 1; pi >= 0; pi--)
  {
    sy = points[pi][1];
    lz = points[pi][4];

    if (sy - oy < -win)
      break;
    
    mn += 1;
    mz += lz;
  }

  // Search forward from the key point
  for (pi = oi + 1; pi < numPoints; pi++)
  {
    sy = points[pi][1];
    lz = points[pi][4];

    if (sy - oy < +win)
      break;
    
    mn += 1;
    mz += lz;
  }

  if (mn < 3) // MAGIC
    return 0;

  mz = mz / mn;

  // Construct residual such that it is positive for stuff sticking
  // up, then discard negative values (this test doesnt work well for
  // negative obstacles).
  res = mz - oz;
  if (res < 0)
    res = 0;
  
  return res;
}


// Compute a local linear fit
float obsmap_linear_fit(obsmap_t *self, int oi,
                      int numPoints, float points[LADAR_BLOB_MAX_POINTS][5])
{
  float win;
  int pi;
  float ox, oy;
  float px, py;
  float mn, mx, my, mxx, mxy;
  float b0, b1;

  win = 2.0/2; // MAGIC

  // This differs from the usual regression because we have x as a
  // function of y.  To make the math look more familiar, we swap
  // the x,y coordinates when reading the point data.

  ox = points[oi][1];
  oy = points[oi][0];
    
  mn = 0;
  mx = 0;
  my = 0;
  mxx = 0;
  mxy = 0;
  
  // Search backward from the key point
  for (pi = oi - 1; pi >= 0; pi--)
  {
    px = points[pi][1];
    py = points[pi][0];

    if (py - oy < -win)
      break;
    
    mn += 1;
    mx += px;    
    my += py;
    mxx += px * px;
    mxy += px * py;
  }

  // Search forward from the key point
  for (pi = oi + 1; pi < numPoints; pi++)
  {
    px = points[pi][1];
    py = points[pi][0];

    if (py - oy > +win)
      break;

    mn += 1;
    mx += px;    
    my += py;
    mxx += px * px;
    mxy += px * py;
  }

  if (mn < 10) // MAGIC
    return 0;
  
  mx /= mn;
  my /= mn;
  mxx /= mn;
  mxy /= mn;
  
  // Compute line parameters for y = b1 * x + b0
  b1 = (mxy - mx * my) / (mxx - mx * mx);
  b0 = my - b1 * mx;

  //printf("b %f %f\n", b0, b1);

  // Compute predicted value for the key point
  py = ox * b1 + b0;

  return fabs(py - oy);
}
