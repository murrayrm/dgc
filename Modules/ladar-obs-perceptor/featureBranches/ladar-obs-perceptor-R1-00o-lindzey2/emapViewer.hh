#include <pthread.h>
#include <emap/emap.hh>

void setYaw(double yaw);
void setEMap(int layer, EMap *emap);
void setEMap(EMap *emap, emap_t min, emap_t max, double dh, double yaw);
pthread_t startViewer(int argc, char **argv, int size);
void EVsetPoints(vector<point2arr> points);
