#define EMAP_BITS 9

#define EMAP_SIZE (1<<EMAP_BITS)   // pixel size of our map
#define EMAP_RES 0.20f   // m/pixel

#undef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#undef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))

#define ANGLE 0
#define RANGE 1

#ifndef _LADARPERCEPTOR_HH
#define _LADARPERCEPTOR_HH

#include <assert.h>
#include <float.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <unistd.h>


#include <ncurses.h>
#include <cotk/cotk.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>
#include <interfaces/LadarRangeBlob.h>

#include <interfaces/MapElementMsg.h>
#include <map/MapElement.hh>
#include <emap/emap.hh>

#include "LadarPerceptorTypes.hh"
#include "LadarMap.hh"
#include "StaticMap.hh"
#include "RoadMap.hh"
#include "ProcessPoints.hh"

#include "RWLock.hh"
#include "cmdline.h"
#include "Log.hh"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

// timing defines, microseconds
#define ROAD_UPDATE_TIME 100000
#define STATIC_DEC_TIME  100000

class LadarPerceptor
{  
public:
    
    // Constructor
    LadarPerceptor();
    
    // Destructor
    ~LadarPerceptor();
    
public:
    
    // Parse the command line
    int parseCmdLine(int argc, char **argv);
    
    // Initialize sensnet
    int initSensnet();
    
    // Clean up sensnet
    int finiSensnet();
    
public:
    
    // Initialize console display
    int initConsole();
    
    // Finalize console display
    int finiConsole();
    
    // Console button callback
    static int onUserQuit(cotk_t *console, LadarPerceptor *self, const char *token);
    
    // Console button callback
    static int onUserPause(cotk_t *console, LadarPerceptor *self, const char *token);
    
public:
    
    // Initalize map
    int init();
    
    // Finalize map
    int fini();
    
    // Update the process state
    int updateProcessState();

public:
    // Create our maps
    void initMaps();

    // Destroy maps
    void finiMaps();

    // Process a single blob
    void processScan(LadarScan *scan);

    // Update all inter-dependent maps that need updating
    void updateMaps();

    // Shift all of the maps
    void shiftMaps(VehicleState &state);

    // Send maps to emapViewer
    void displayMaps();

    // The separate map update loop, is static for pthread stuffs
    static void* mapUpdateLoop(void *self);
    
public:
    
    // Check for new data and update maps
    int update();
    
public:
    
    // Program options
    gengetopt_args_info options;
    
    // Spread settings
    char *spreadDaemon;
    int skynetKey;
    modulename moduleId;
    int sendSubgroup, debugSubgroup;
    
    // Operation mode
    enum {modeLive, modeReplay} mode;
    
    // Sensnet module
    sensnet_t *sensnet;
    
    // Sensnet replay module
    sensnet_replay_t *replay;
    
    //logging options
    bool logging;
    int logLevel;

    // Console interface
    cotk_t *console;
    
    // Should we quit?
    bool quit;
    
    // Should we pause?
    bool pause;
        
    // Current state data
    VehicleState state;

    // Map position info
    float local2map[4][4];
    double mapErrorX;
    double mapErrorY;
    double mapOffsetZ;

    // Anomalies caught
    int anomalyCount;

    // Update timing info
    uint64_t lastRoadUpdate;
    uint64_t lastStaticDec;

    // Time of last process update
    uint64_t processTime;
    
    // Time of last outgoing message
    uint64_t sendTime;
    
    // Performance stats
    uint64_t addCount, addTime;

    // *******************************************
    // ******** Perceptor classes go here ********
    // *******************************************

    // The storage unit for ladar scans
    LadarScanStore *scans;

    // The scan-based tracking map
    LadarMap *ladarMap;

    // Point processing/groundstrike filtering class
    ProcessPoints *pproc;

    // Our road map
    RoadMap *rmap;

  //road map for center front ladar
    RoadMap *rmap_m;

    // Static map
    StaticMap *smap;
};

// More useful macros
#undef MIN
#undef MAX
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#endif
