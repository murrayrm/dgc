#include "LadarMap.hh"

void LadarMap::update()
{
    timer.startTime(5);
    for (int i=0;i<1;i++)
    {
	LadarScan *scan = scans->getPastScan(i,0);
	if (!scan->processed && scan->blobId > 0) {
	  //	  Log::getStream(1)<<"lm ln 10 calling getPastScan w/ blobID "<<scan->blobId<<endl;
	    scan = scans->getPastScan(i,1);
	}
	this->tracker[i].ProcessScan(scan);	
    }
    //    timer.printTime("Map update time",5);
    if ((DGCgettime()-this->lastDebugTime) > 100000)
    {
	this->lastDebugTime = DGCgettime();
	for (int i=0;i<1;i++)
	    this->tracker[i].sendDebug(this->debugTalker,this->debugSubgroup);
    }
    usleep(1);
}

void LadarMap::associate()
{
}
