#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include "LadarPerceptor.hh"

#ifndef LADARMAP_HH
#define LADARMAP_HH

enum
{
    FG_N = 0x0000, // background
    FG_L = 0x0001, // left foreground
    FG_R = 0x0002, // right foreground
    FG_B = 0x0003  // both foreground
};

// Contains information about a segment we're interested in
struct LadarSegment
{
    LadarScan *scan; // the  scan this segment belongs to   
    int blobId; // blob id of original scan, to make sure it's the same
    int start; // the starting point of this scan
    int end; // the end of this scan
    int flags; // which ends are in foreground, etc
    vec3_t center; // center of mass of segment
    vec3_t left; // left corner of segment
    vec3_t right; // right corner
    vec3_t min;
    vec3_t max;
};

#define NUM_PAST_SEGS 10

// A tracked segment object in a single ladar
struct TrackedSegment
{
    // status - confidence of existence stuff
    int status;
    // associated segments, [0] is most recent
    LadarSegment segment[NUM_PAST_SEGS];
    // number of segments we have associated to date
    int numAssoc;
    // next empty track for linked list of empty tracks
    int nextEmptyId;

    Point2 velocity;
    // this is where filtering stuff goes
};

// Some define constants
// Minimum segmentation distance (m)
#define MAP_SEG_MIN 1.5
// Coefficient based on distance to add to above value
#define MAP_SEG_COEFF 0.01
// Maximum ladar return distance (beyond is no-return)
#define MAX_LADAR_DIST 80.0
// Upper groundstrike threshold - above this, we are certain
// a segment is a groundstrike
#define GS_UPPER_THRESH 0.6
// Lower groundstrike threshold - certain it isn't one
#define GS_LOWER_THRESH 0.05
// Groundstrike gap.. segment if our delta gs conf is larger than this
#define GS_GAP 0.8
// No-return gap, to smooth out gaps of no-returns that are
// otherwise not segmented. Number of points.
#define NR_GAP_POINTS 2
// Maximum distance allowable for NR_GAPS, for range
#define NR_GAP_DIST 0.5

// Tracking association distance
#define TRACK_MAX_ASSOC 0.2
// Maximum change in segment length allowed
// confidence of existence for track to be considered live
#define TRACK_LIVE_THRESH 8
// initial status points we give track
#define TRACK_INIT_STATUS 5
// maximum status a track can have
#define TRACK_MAX_STATUS 60
// weight we use when combining new vel with old
#define TRACK_VEL_WEIGHT 0.02
// maximum velocity we consider reasonable
#define TRACK_MAX_VEL 40

// time between ladar scans
#define LADAR_DT 1.0/75.0

#define MAX_TRACKS 1000
#define NUM_TRACK_LADARS 6


// A tracker for an individual ladar
class LadarTracker
{
    LadarScanStore *scans; // Scan storage
    int ladarId; // our ladar's id
    TrackedSegment track[MAX_TRACKS]; // Tracked segments
    int emptyTrack; // index of lowest empty track
    int numTracks; // number of active tracks
    int highestTrack; // index of highest alive track
    int lastProcId; // id of last processed blob

    vector<LadarSegment> segmentScan(LadarScan *scan);
    void associateScan(LadarScan *scan, vector<LadarSegment> &segments);
    void pruneMap();
    point2arr getSegmentPoints(LadarSegment& segment);

    void createTrack(LadarSegment& segment);
    void deleteTrack(int index);
    void addToTrack(LadarSegment& segment, int index);
    void updateTrackList();
    double scoreSegment(LadarSegment& segment, int index);

public:
    LadarTracker() {memset(this,0,sizeof(*this));}
    ~LadarTracker() {}
    void setStore(LadarScanStore *store) {this->scans = store;}

    void ProcessScan(LadarScan *scan);
    void sendDebug(CMapElementTalker& talker, int group);
    void clearTracks();
};

// The map
class LadarMap
{
    int skynetKey;
    LadarScanStore *scans; // The scan storage depot
    EMap* staticMap; // The processed occupancy (static) map
    LadarTracker tracker[NUM_TRACK_LADARS]; // the trackers

    CMapElementTalker mapTalker;
    CMapElementTalker debugTalker;
    int sendSubgroup;
    int debugSubgroup;
    bool sendDebug;
    uint64_t lastDebugTime;
    // Tracked objects
//    vector<LadarObject> objects;

public:
    LadarMap(LadarScanStore *store, int skynetKey, modulename module, int sendSubgroup, int debugSubgroup)
    {
	memset(this,0,sizeof(*this));
	this->skynetKey = skynetKey;
	this->scans = store;
	this->staticMap = new EMap(EMAP_SIZE);
	for (int i=0;i<NUM_TRACK_LADARS;i++)
	    tracker[i].setStore(store);
	this->sendSubgroup = sendSubgroup;
	this->debugSubgroup = debugSubgroup;
	this->mapTalker.initSendMapElement(skynetKey,module);
	this->sendDebug = (debugSubgroup < 0);
	if (this->sendDebug)
	    this->debugTalker.initSendMapElement(skynetKey,module);
    }

    ~LadarMap()
    {
	delete staticMap;
    }

    void update();
    void associate();
};

static inline vec3_t vec3_min(vec3_t a, vec3_t b)
{
    return vec3_set(MIN(a.x,b.x),MIN(a.y,b.y),MIN(a.z,b.z));
}

static inline vec3_t vec3_max(vec3_t a, vec3_t b)
{
    return vec3_set(MAX(a.x,b.x),MAX(a.y,b.y),MAX(a.z,b.z));
}

#endif
