#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include "LadarPerceptor.hh"
#include "StaticMap.hh"

#ifndef LADARMAP_HH
#define LADARMAP_HH

enum
{
    FG_N = 0x0000, // background
    FG_L = 0x0001, // left foreground
    FG_R = 0x0002, // right foreground
    FG_B = 0x0003  // both foreground
};

enum
{
    CAR_SIZE    = 0x0001, // small enough to be car
    CAR_CORNERS = 0x0002, // not too 'rough'
    CAR_REFL    = 0x0004  // was reflective
};

// Contains information about a segment we're interested in
struct LadarSegment
{
    LadarScan *scan; // the  scan this segment belongs to   
    int blobId; // blob id of original scan, to make sure it's the same
    int start; // the starting point of this scan
    int end; // the end of this scan
    int flags; // which ends are in foreground, etc
    vec3_t center; // center of mass of segment
    vec3_t left; // left corner of segment
    vec3_t right; // right corner
    vec3_t min;
    vec3_t max;
    bool reflective; // whether we contain a reflective point
};

#define NUM_PAST_SEGS 10

// A tracked segment object in a single ladar
struct TrackedSegment
{
    // status - confidence of existence stuff
    int status;
    // associated segments, [0] is most recent
    LadarSegment segment[NUM_PAST_SEGS];
    // number of segments we have associated to date
    int numAssoc;
    // confidence that this belongs to a car
    int carConf;
    // next empty track for linked list of empty tracks
    int nextEmptyId;
    // number of reflective segments associated with this
    int numReflective;

    // last map track we were associated with
    int lastMapTrack;

    // recursively averaged velocity until KF kicks in
    Point2 velocity;
    // Kalman filtering variables
    // last known state
    double x[2];
    double y[2];
    // predicted position
    double nx;
    double ny;
    // covariance matrix
    double Px[2][2];
    double Py[2][2];
};

// Some define constants
// Minimum segmentation distance (m)
#define MAP_SEG_MIN 1.5
// Coefficient based on distance to add to above value
#define MAP_SEG_COEFF 0.02
// Maximum ladar return distance (beyond is no-return)
#define MAX_LADAR_DIST 80.0
// Upper groundstrike threshold - above this, we are certain
// a segment is a groundstrike
#define GS_UPPER_THRESH 0.6
// Lower groundstrike threshold - certain it isn't one
#define GS_LOWER_THRESH 0.05
// Groundstrike gap.. segment if our delta gs conf is larger than this
#define GS_GAP 0.8
// No-return gap, to smooth out gaps of no-returns that are
// otherwise not segmented. Number of points.
#define NR_GAP_POINTS 2
// Maximum distance allowable for NR_GAPS, for range
#define NR_GAP_DIST 0.75

// Tracking association distance
#define TRACK_MAX_ASSOC 1.0
// Maximum change in segment length allowed
// confidence of existence for track to be considered live
//#define TRACK_LIVE_THRESH 8
// initial status points we give track
#define TRACK_INIT_STATUS 5
// maximum status a track can have
#define TRACK_MAX_STATUS 60
// weight we use when combining new vel with old
#define TRACK_VEL_WEIGHT 0.04
// number of segments to look at velocity variance with
#define TRACK_VEL_SEGS 6
// maximum variance allowed in velocity stuffs
#define TRACK_MAX_VAR 0.3
// maximum velocity we consider reasonable
#define TRACK_MAX_VEL 20
// max number of points the track can change by to inc status
#define TRACK_CHANGE_POINTS 3

// filter process noise
#define KF_PROC_NOISE 0.02
// initial covariance matrix value
#define KF_COV_INIT 0.3
// number of segments we wait for until starting KF
#define KF_INIT_WAIT 3
// the coefficient between ladar range and uncertainty
#define KF_SIGMA_COEFF 0.02
// maximum velocity allowed when initializing KF
#define KF_MAX_VEL 15

// This is how we decide whether something looks like a car
// maximum car confidence value
#define CCONF_MAX 80
// corner distance threshold (m)
#define CCONF_CORNER_DIST 0.15
// increment for this many or fewer corners
#define CCONF_CORNER_LOW 2
// decrement for this many or more corners
#define CCONF_CORNER_HIGH 4
// increment for corners
#define CCONF_CORNER_INC 2
// decrement for high noise, many corners
#define CCONF_CORNER_DEC 4
// increment for positive intensity return (no decrement)
#define CCONF_REFLECT_INC 8
// size threshold (m)
#define CCONF_MAX_SIZE 6
// good size increment
#define CCONF_SIZE_INC 1
// bad size decrement
#define CCONF_SIZE_DEC 4

// time between ladar scans
#define LADAR_DT 1.0/75.0

#define MAX_TRACKS 1000
#define NUM_TRACK_LADARS 6


// A tracker for an individual ladar
class LadarTracker
{
    LadarScanStore *scans; // Scan storage
    int ladarId; // our ladar's id
    TrackedSegment track[MAX_TRACKS]; // Tracked segments
    int emptyTrack; // index of lowest empty track
    int numTracks; // number of active tracks
    int highestTrack; // index of highest alive track
    LadarScan *lastScan; // pointer to last proc scan
    int lastProcId; // id of last processed blob

    vector<LadarSegment> segmentScan(LadarScan *scan);
    void associateScan(LadarScan *scan, vector<LadarSegment> &segments);
    void predict(int delta);
    void pruneMap();

    void createTrack(LadarSegment& segment);
    void deleteTrack(int index);
    void addToTrack(LadarSegment& segment, int index);
    void initKF(int index);
    void predictKF(int index);
    void updateKF(int index, double nx, double ny, double R);
    void updateAvg(int index);
    void updateCarConf(int index);
    void updateTrackList();
    double scoreSegment(LadarSegment& segment, int index);

public:
    LadarTracker() {}
    ~LadarTracker() {}
    void init(LadarScanStore *store,int ladar) 
    {
	this->scans = store;
	this->ladarId = ladar;
	updateTrackList();
	for (int i=0;i<MAX_TRACKS;i++)
	    track[i].lastMapTrack = -1;
    }

    bool ProcessScan(LadarScan *scan);
    void sendDebug(CMapElementTalker& talker, int group);
    void clearTracks();

    int getBlobId() { return lastProcId; }
    LadarScan *getLastScan() { return lastScan; }
    TrackedSegment& getTrack(int id) { return track[id]; }
    int getHighestTrack() { return highestTrack; }
    point2arr getSegmentPoints(LadarSegment& segment);
};

// An untracked object in the LadarMap, from the StaticMap
struct StaticObject
{
    // our status/confidence/TBD
    int status;
    // our latest static map geometry
    point2arr geometry;
    // the center of our geometry (by area/mass)
    point2 centroid;
};

#define MAP_TRACK_SEGS 30
#define MAP_PAST_POS 200

// A tracked object based on raw ladar data
struct TrackedObject
{
    // confidence/status
    int status;
    // associated tracks [id][ladarId,index]
    int segment[MAP_TRACK_SEGS][2];
    // number of associated segs/tracks
    int numSegments;
    // KF position & vel
    //double state[4];
    point2 pos;
    point2 vel;
    // position history
    point2 pastpos[MAP_PAST_POS];
    // number of past positions
    int numPastPos;
    // latest geometry
    point2arr geometry;
    // dynamics stuff
    bool seenMoving;
    uint64_t lastMoving;
};

// The rate at which we send debug elements (us)
#define MAP_DEBUG_RATE 1000000
// The rate at which we update our static map (us)
#define MAP_STATIC_RATE 100000
// The rate at which we send tracks
#define MAP_TRACK_RATE 50000
// The thresholding value we apply to the map
#define MAP_THRESH_VAL 45000
// The number of pixels we dilate/erode by
#define MAP_DE_VAL 4
// The distance we simplify static obs with
#define MAP_SIMPLIFY_DIST 0.2
// Maximum number of static objs to keep
#define MAP_MAX_STATIC 200
// Maximum association distance for static obs stuff
#define MAP_MAX_ASSOC 1.0
// Maximum number of dynamic tracks
#define MAP_MAX_TRACKS 300

// Minimum status of associated tracks
#define MAP_SEG_MINSTATUS 10
// Maximum association distance for tracks
#define MAP_ASSOC_DIST 3
// Maximum point-point distance for association
#define MAP_MAX_PPDIST 1
// Maximum velocity screwiness area
#define MAP_MAX_VAREA 25
// Ignore distance - gt and two tracks not assoc.
#define MAP_IGNORE_DIST 6
// Initial status we start with
#define MAP_INIT_STATUS 5
// Maximum status we can have
#define MAP_MAX_STATUS 200
// Status threshold thingy for segment
#define MAP_STATUS_THRESH 35
// Live threshold for tracks
#define MAP_LIVE_THRESH 5
// Minimum velocity to be considered moving
#define MAP_MIN_VEL 2.0
// Minimum amount centroid has to move by before being moving
#define MAP_MIN_DELTA 2
// Maximum centroid distance to consider for static obs removal due to dyn track
#define MAP_MAX_DYNCDIST 5
// Maximum distance to remove static obs
#define MAP_MAX_DYNSDIST 1

// The map
class LadarMap
{
    int skynetKey;
    LadarScanStore *scans; // The scan storage depot
    StaticMap* smap; // Pointer to shared static occupancy map
    EMap* staticMap; // The processed occupancy (static) map
    float local2map[4][4]; // staticMap transforms
    float map2local[4][4];
    LadarTracker tracker[NUM_TRACK_LADARS]; // the trackers

    CMapElementTalker mapTalker;
    CMapElementTalker debugTalker;
    int sendSubgroup;
    int debugSubgroup;
    int moduleId;
    bool sendDebug;

    int obsShrink;

    uint64_t lastDebugTime;
    uint64_t lastStaticTime;
    uint64_t lastTrackTime;
    // Untracked objects from static map
    StaticObject obj[MAP_MAX_STATIC];
    // Tracked objects from trackers
    TrackedObject track[MAP_MAX_TRACKS];

    point2arr makeGeometry(int index);
    point2 getCentroid(point2arr& ptarr);
    vector<point2arr> vectorizeMap();
    point2 makePoint(int ind);
    int moveDir(emap_t* &pix, int lastDir, int width);
    void updateStatic();
    void sendStatic();
    void updateTracks();
    void sendTracks();

    double getScore(int index, TrackedSegment& ts);

    void addStatic(point2arr geo);
    void removeStatic(int id);
    void updateStatic(int id, point2arr geo);

    bool isMoving(int index);
    void testCreateTrack(TrackedSegment& ts, int ladarId, int trackId);
    int addTrack(int ladarId, int trackId);
    void addSegment(int index, int ladarId, int trackId);
    void removeTrack(int index);
    void removeSegment(int index, int id);

public:
    LadarMap(LadarScanStore *store, StaticMap *statmap, int skynetKey, modulename module, int sendSubgroup, int debugSubgroup)
    {
	memset(this,0,sizeof(*this));
	this->skynetKey = skynetKey;
	this->scans = store;
	this->smap = statmap;
	this->staticMap = new EMap(EMAP_SIZE);
	for (int i=0;i<NUM_TRACK_LADARS;i++)
	    tracker[i].init(store,i);
	this->sendSubgroup = sendSubgroup;
	this->debugSubgroup = debugSubgroup;
	this->mapTalker.initSendMapElement(skynetKey,module);
	this->moduleId = (int)module;
	this->sendDebug = (debugSubgroup < 0);
	if (this->sendDebug)
	    this->debugTalker.initSendMapElement(skynetKey,module);
    }

    ~LadarMap()
    {
	delete staticMap;
    }

    void setObsShrink(int shrink) {this->obsShrink=shrink;}

    void update();
};

static inline vec3_t vec3_min(vec3_t a, vec3_t b)
{
    return vec3_set(MIN(a.x,b.x),MIN(a.y,b.y),MIN(a.z,b.z));
}

static inline vec3_t vec3_max(vec3_t a, vec3_t b)
{
    return vec3_set(MAX(a.x,b.x),MAX(a.y,b.y),MAX(a.z,b.z));
}

static inline void mat22d_mul(double c[2][2],double a[2][2], double b[2][2])
{
    c[0][0] = a[0][0]*b[0][0] + a[0][1]*b[1][0];
    c[1][0] = a[1][0]*b[0][0] + a[1][1]*b[1][0];
    c[0][1] = a[0][0]*b[0][1] + a[0][1]*b[1][1];
    c[1][1] = a[1][0]*b[0][1] + a[1][1]*b[1][1];
}

static int qcmp(const void *a, const void *b)
{
  const point2* pt1 = (point2*)a;
  const point2* pt2 = (point2*)b;
  return (pt1->z > pt2->z)?1:-1;
}

#endif
