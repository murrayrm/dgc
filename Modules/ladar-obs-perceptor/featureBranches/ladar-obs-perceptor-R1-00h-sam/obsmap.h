
/* 
 * Desc: Ladar obstacle map
 * Date: 30 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef OBSMAP_H
#define OBSMAP_H

#include <frames/pose3.h>
#include <interfaces/LadarRangeBlob.h>

#if defined __cplusplus
extern "C"
{
#endif


/** @file

@brief Fuse ladar scans into an elevation map.

*/

  
/// @brief Map cell data
typedef struct
{
  // Elevation moments (un-normalized)
  float mn, mz;

  // Residual from local filter (un-normalized)
  float res;

  // Bumper ladar hit count
  float bumper;
  
  // Scan count of last update
  int scan_count;
  
} obsmap_cell_t;
  

/// @brief Elevation map object
typedef struct 
{  
  // Map scale (m/cell)
  float scale;

  // Map size in each dimension
  int size;

  // Map pose
  pose3_t pose;

  // Decay data constant (1/s)
  float roof_decay_const;

  // Bumper data constant (1/s)
  float bumper_decay_const;

  // Number of scans processed
  int scan_count;
  
  // Timestamp of last decay
  uint64_t decay_time;
  
  // Transform from local to map frame
  float trans_ml[4][4];

  // Transform from map to local frame
  float trans_lm[4][4];

  // Map data
  obsmap_cell_t *cells;
  
} obsmap_t;


  
/// @brief Allocate new display
obsmap_t *obsmap_alloc();

/// @brief Free display
void obsmap_free(obsmap_t *self);

/// @brief Clear the map and center it on the current local pose.
int obsmap_clear(obsmap_t *self, const VehicleState *state);

/// @brief Move the map center but keep the data
int obsmap_move(obsmap_t *self, const VehicleState *state);

/// @brief Decay the map.
int obsmap_decay(obsmap_t *self, uint64_t time);

/// @brief Add a bumper ladar scan.
int obsmap_add_bumper(obsmap_t *self, LadarRangeBlob *blob, float max_range);

/// @brief Add a roof ladar scan.
int obsmap_add_roof(obsmap_t *self, LadarRangeBlob *blob, float max_range);


/// @brief Transfrom from map position to map index
static __inline__ void obsmap_m2i(obsmap_t *self, float px, float py, int *mi, int *mj)
{
  *mi = (int) (px/self->scale + 10000000) - 10000000 + self->size/2;
  *mj = (int) (py/self->scale + 10000000) - 10000000 + self->size/2;
  return;
}


/// @brief Transfrom from map index to map position
static __inline__ void obsmap_i2m(obsmap_t *self, int mi, int mj, float *px, float *py)
{
  *px = (mi - self->size/2 + 0.5) * self->scale;
  *py = (mj - self->size/2 + 0.5) * self->scale;
  return;
}


/// @brief Return the cell at the given map index
static __inline__ obsmap_cell_t *obsmap_cell_index(obsmap_t *self, int mi, int mj)
{
  if (mi < 0 || mi >= self->size)
    return NULL;
  if (mj < 0 || mj >= self->size)
    return NULL;
  return self->cells + mi + mj * self->size;
}

  
/// @brief Return the cell at the given map position
static __inline__ obsmap_cell_t *obsmap_cell_pos(obsmap_t *self, float px, float py)
{
  int mi, mj;
  obsmap_m2i(self, px, py, &mi, &mj);
  return obsmap_cell_index(self, mi, mj);
}

  
#if defined __cplusplus
}
#endif

#endif
