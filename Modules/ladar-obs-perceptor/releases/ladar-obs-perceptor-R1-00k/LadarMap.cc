#include "LadarMap.hh"

void LadarMap::update()
{
    timer.startTime(5);
    for (int i=0;i<NUM_TRACK_LADARS;i++)
    {
	LadarScan *scan = scans->getPastScan(i,0);
	if (!scan->processed)
	    scan = scans->getPastScan(i,1);
	this->tracker[i].ProcessScan(scan);	
    }
    timer.printTime("Map update time",5);
    // send debug info
    if ((DGCgettime()-this->lastDebugTime) > MAP_DEBUG_RATE)
    {
	this->lastDebugTime = DGCgettime();
	//	for (int i=0;i<NUM_TRACK_LADARS;i++)
	int i = DGCgettime() % NUM_TRACK_LADARS;
	if (this->sendDebug)
	  this->tracker[i].sendDebug(this->debugTalker,this->debugSubgroup);
    }
    // update obs map
    if ((DGCgettime()-this->lastStaticTime) > MAP_STATIC_RATE)
    {
	this->lastStaticTime = DGCgettime();
	this->staticMap->copyFrom(smap->getMap());
	this->smap->getMatrices(local2map,map2local);
	updateStatic();
	sendStatic();
    }
    usleep(1);
}

void LadarMap::updateStatic()
{
    // Threshold, dilate, and erode... ta-da
    staticMap->threshold(MAP_THRESH_VAL);
    staticMap->dilate(MAP_DE_VAL);
    staticMap->erode(MAP_DE_VAL);
    // Now, make the map into vectors n stuff
    vector<point2arr> poly = vectorizeMap();
    // and now associate
    for (size_t i=0;i<poly.size();i++)
    {
	point2 centroid = getCentroid(poly[i]);
	double mindist = 100000;
	int minindex = -1;
	for (int j=0;j<MAP_MAX_TRACKS;j++)
	{
	    if (obj[j].status != 1)
		continue;
	    // find closest other centroid
	    double dist = centroid.dist(obj[j].centroid);
	    if (dist < mindist)
	    {
		mindist = dist;
		minindex = j;
	    }
	}
	if (minindex >= 0 && mindist < MAP_MAX_ASSOC)
	{
	    updateTrack(minindex,poly[i].simplify(MAP_SIMPLIFY_DIST));
	}
	else
	{
	    addTrack(poly[i].simplify(MAP_SIMPLIFY_DIST));
	}
    }
    // alright, we've either associated or added what we need to, now prune some
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (obj[i].status == 1) //wasn't associated, remove
	    removeTrack(i);
	else if (obj[i].status == 2)
	    obj[i].status = 1; //prepare for next cycle
    }
    // simplify all of the polygons
//    for (size_t i=0;i<poly.size();i++)
//	this->staticObs.push_back(poly[i].simplify(MAP_SIMPLIFY_DIST));
}

void LadarMap::sendStatic()
{
    MapElement el;
    MapElement elClear;

    el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_PURPLE;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    elClear.setTypeClear();

    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (obj[i].status == -1)
	{	    
	    // send a clear
	    elClear.setId(this->moduleId,i);
	    this->mapTalker.sendMapElement(&elClear,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&elClear,debugSubgroup);
	    obj[i].status = 0;
	}
	else if (obj[i].status == 1)
	{
	    el.setGeometry(obj[i].geometry);
	    el.setId(this->moduleId,i);
	    this->mapTalker.sendMapElement(&el,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&el,debugSubgroup);
	    /*	    el.setId(this->moduleId+1,i);
	    el.setGeometry(obj[i].centroid);
	    el.setType(ELEMENT_POINTS);
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	    el.setTypeObstacle();*/
	}
    }
/*    el.clear();
    el.setTypeClear();
    for (;i<lastStaticCount;i++)
    {
	el.setId(this->moduleId,i);
	this->mapTalker.sendMapElement(&el,sendSubgroup);
	if (this->sendDebug)
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
    }
    lastStaticCount = staticObs.size();*/
}

void LadarMap::addTrack(point2arr geo)
{
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (obj[i].status == 0)
	{
	    obj[i].status = 2;
	    obj[i].geometry = geo;
	    obj[i].centroid = getCentroid(geo);
	    return;
	}
    }
}

void LadarMap::removeTrack(int id)
{
    obj[id].status = -1;
}

void LadarMap::updateTrack(int id, point2arr geo)
{
    obj[id].geometry = geo;
    obj[id].centroid = getCentroid(geo);
    obj[id].status = 2;
}

void LadarMap::associate()
{
}



inline point2 LadarMap::getCentroid(point2arr& ptarr)
{
    double A=0;
    for (size_t i=0;i<ptarr.size()-1;i++)
	A += (ptarr[i].x*ptarr[i+1].y - ptarr[i+1].x*ptarr[i].y);
    A += ptarr.back().x*ptarr[0].y - ptarr[0].x*ptarr.back().y;
    A *= 0.5;
    point2 c = point2(0,0);
    for (size_t i=0;i<ptarr.size()-1;i++)
    {
	c.x += (ptarr[i].x+ptarr[i+1].x)*(ptarr[i].x*ptarr[i+1].y-ptarr[i+1].x*ptarr[i].y);
	c.y += (ptarr[i].y+ptarr[i+1].y)*(ptarr[i].x*ptarr[i+1].y-ptarr[i+1].x*ptarr[i].y);
    }
    c.x += (ptarr.back().x+ptarr[0].x)*(ptarr.back().x*ptarr[0].y-ptarr[0].x*ptarr.back().y);
    c.y += (ptarr.back().y+ptarr[0].y)*(ptarr.back().x*ptarr[0].y-ptarr[0].x*ptarr.back().y);
    c.x *= (1.0/(6*A));
    c.y *= (1.0/(6*A));
    return c;
}

vector<point2arr> LadarMap::vectorizeMap()
{
    int width = staticMap->getWidth();
    emap_t *pix = staticMap->getData();
    emap_t *end = pix + width*width;
    emap_t *curPix;
    int curDir;
    int lastDir;
    vector<point2arr> polys;

    // set edges of map to 0 to avoid edge issues
    for (int i=0;i<width;i++)
    {
	*(pix+i) = 0;
	*(pix+i*width) = 0;
	*(pix+i*width+width-1) = 0;
	*(pix+width*width-i-1) = 0;
    }

    // loop through map
    while (pix < end)
    {
	if (*pix == 0)
	{
	    pix++;
	    continue;
	}
	// found a polygon, start looping through
	else if (*pix == 0xFFFF && *(pix-1) == 0) // new polygon, == 0 to avoid doing donuts
	{
	    // get starting index
	    int ind = (pix-staticMap->getData());
	    point2arr ptarr;
	    ptarr.push_back(makePoint(ind));
	    curPix = pix;
	    *pix = 1; // set start location
	    curDir = moveDir(curPix,1,width);
	    lastDir = 0;
	    while (*curPix > 1)
	    {
		ind += curDir;
		if (curDir != lastDir)
		    ptarr.push_back(makePoint(ind));
		// mark that we have been here
		(*curPix)--;
		lastDir = curDir;
		curDir = moveDir(curPix,lastDir,width);
	    }
	    if (ptarr.size() > 1)
	    {
		polys.push_back(ptarr);
	    }
	    else // give it a tiny bit of size, just in case
	    {
		ptarr.push_back(ptarr[0] + point2(0.1,0));
		ptarr.push_back(ptarr[0] + point2(0,0.1));
		polys.push_back(ptarr);
	    }

	    // now move to end of poly
	    while (*pix != 0)
		pix++;
	}
	else
	{
	    while (*pix != 0) // have already done this poly
		pix++;
	}
    }

    return polys;
}

inline point2 LadarMap::makePoint(int ind)
{
    int x = (ind & (EMAP_SIZE-1));
    int y = (ind >> EMAP_BITS);
    return point2(x*map2local[0][0]+map2local[0][3],y*map2local[1][1]+map2local[1][3]);
}

inline int LadarMap::moveDir(emap_t* &pix, int lastDir, int width)
{
    emap_t *lastPix=pix;
    switch (lastDir)
    {
    case -EMAP_SIZE:
	pix += width - 1;
	lastPix = pix + 1;
	goto bl;
    case -EMAP_SIZE+1:
	pix--;
	lastPix = pix + width;
	goto lt;
    case 1:
	pix -= (width+1);
	lastPix = pix + width;
	goto ul;
    case EMAP_SIZE+1:
	pix -= width;
	lastPix = pix - 1;
	goto up;
    case EMAP_SIZE:
	pix -= (width-1);
	lastPix = pix - 1;
	goto ur;
    case EMAP_SIZE-1:
	pix ++;
	lastPix = pix - width;
	goto rt;
    case -1:
	pix += width + 1;
	lastPix = pix - width;
	goto br;
    case -EMAP_SIZE-1:
	pix += width;
	lastPix = pix + 1;
	goto bt;	
    }
up:
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;
ur:
    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;
rt:
    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;
br:
    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;
bt:
    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;
bl:
    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;
lt:
    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;
ul:
    if (*lastPix == 0 && *pix > 0)
	return -width-1;
    lastPix = pix;
    pix++;
// now for wrap-around calls
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;

    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;

    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;

    return 0;
}

