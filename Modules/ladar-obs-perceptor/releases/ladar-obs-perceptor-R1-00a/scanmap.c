
/* 
 * Desc: Ladar scan map
 * Date: 17 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <frames/pose3.h>
#include <frames/mat44.h>

#include "scanmap.h"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Allocate new object
scanmap_t *scanmap_alloc()
{
  scanmap_t *self;

  self = calloc(1, sizeof(scanmap_t));
  self->map_scale = 0.20;
  self->map_size = 512;  
  self->cells = calloc(self->map_size * self->map_size, sizeof(self->cells[0]));
 
  return self;
}


// Free object
void scanmap_free(scanmap_t *self)
{
  free(self->cells);
  free(self);
  
  return;
}


// Clear the map
int scanmap_clear(scanmap_t *self, const VehicleState *state)
{
  int i;
  pose3_t pose;
  scanmap_ladar_t *ladar;
  
  // Construct transform from local-to-grid frame, using the vehicle
  // local pose as the starting point
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);
  pose3_to_mat44f(pose, self->map2loc);
  mat44f_inv(self->loc2map, self->map2loc);

  // Clear individual ladar polygons
  for (i = 0; i < sizeof(self->ladars)/sizeof(self->ladars[0]); i++)
  {
    ladar = &self->ladars[i];
    ladar->scan_count = 0;
    gpc_free_polygon(&ladar->space);
  }
  
  // Clear the free-space polygon
  gpc_free_polygon(&self->fused_space);

  // Clear the obstacle map
  memset(self->cells, 0, self->map_size * self->map_size * sizeof(self->cells[0]));
  
  return 0;
}


// Add a ladar scan to the map
int scanmap_add(scanmap_t *self, LadarRangeBlob *blob)
{
  scanmap_add_free(self, blob);
  scanmap_add_obs(self, blob);
  
  return 0;
}


// Update the free-space polygon
int scanmap_add_free(scanmap_t *self, LadarRangeBlob *blob)
{
  int i;
  float pb, pr;
  float sx, sy, sz;
  float vx, vy, vz;
  float lx, ly, lz;  
  float mx, my;
  float m[4][4];
  gpc_vertex_list contour = {0};
  gpc_polygon src = {0};
  gpc_polygon dst = {0};
  scanmap_ladar_t *ladar;

  mat44f_setf(m, self->loc2map);
    
  // Create empty contour
  contour.vertex = calloc(blob->numPoints + 2, sizeof(contour.vertex[0]));  

  for (i = -1; i <= blob->numPoints; i++)
  {
    if (i < 0 || i >= blob->numPoints)
    {
      pb = 0;
      pr = 0;
    }
    else
    {
      pb = blob->points[i][0];
      pr = blob->points[i][1];
    }

    // Clip the range when looking for free space.  We dont really
    // believe the very long readings, since the ladar is probably not
    // detecting the return.
    if (pr > 30)
      pr = 30;
    
    // Convert from bearing/range to point in local frame
    LadarRangeBlobScanToSensor(blob, pb, pr, &sx, &sy, &sz);
    LadarRangeBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);
    LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);

    // Compute point in map frame
    mx = m[0][0]*lx + m[0][1]*ly + m[0][2]*lz + m[0][3];
    my = m[1][0]*lx + m[1][1]*ly + m[1][2]*lz + m[1][3];

    // Update free-space contour
    contour.vertex[contour.num_vertices].x = mx;
    contour.vertex[contour.num_vertices].y = my;
    contour.num_vertices += 1;

    //printf("%d %f %f\n", i, vx, vy);
    //printf("%d %f %f\n", i, pb, pr);
  }
  //printf("\n\n");

  // Create polgon from the scan contour
  gpc_add_contour(&src, &contour, false);
  free(contour.vertex);

  // Get current free-space polygon for this ladar
  assert(blob->sensorId >= 0);
  assert(blob->sensorId < sizeof(self->ladars)/sizeof(self->ladars[0]));
  ladar = &self->ladars[blob->sensorId];
  
  // Take the intersection of all the 
  if (ladar->scan_count == 0)
    gpc_polygon_clip(GPC_UNION, &ladar->space, &src, &dst);
  else
    gpc_polygon_clip(GPC_INT, &ladar->space, &src, &dst);
  
  // Clean up
  gpc_free_polygon(&src);

  // Keep destination polygon
  gpc_free_polygon(&ladar->space);
  ladar->space = dst;
  ladar->scan_count += 1;
      
  return 0;
}


// Update the obstacle map
int scanmap_add_obs(scanmap_t *self, LadarRangeBlob *blob)
{
  int i;
  float pb, pr;
  float sx, sy, sz;
  float vx, vy, vz;
  float lx, ly, lz;  
  float mx, my;
  float m[4][4];
  scanmap_cell_t *cell;

  self->scan_count++;
    
  mat44f_setf(m, self->loc2map);

  for (i = 0; i < blob->numPoints; i++)
  {
    pb = blob->points[i][0];
    pr = blob->points[i][1];

    // Discard bogus range values
    if (pr <= 0)
      continue;
    if (pr > 80.0) // MAGIC
      continue;
        
    // Convert from bearing/range to point in local frame
    LadarRangeBlobScanToSensor(blob, pb, pr, &sx, &sy, &sz);
    LadarRangeBlobSensorToVehicle(blob, sx, sy, sz, &vx, &vy, &vz);
    LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);
          
    // Compute point in map frame
    mx = m[0][0]*lx + m[0][1]*ly + m[0][2]*lz + m[0][3];
    my = m[1][0]*lx + m[1][1]*ly + m[1][2]*lz + m[1][3];

    // Get cell at end point
    cell = scanmap_cell_pos(self, mx, my);
    if (!cell)
      continue;

    // Update cell stats.  We only increment the hit count once for
    // each cell, so the hit count tells us how many scans hit this
    // cell (as opposed to getting lots of hits in one scan).
    if (cell->scan != self->scan_count)
    {
      cell->hits += 1;
      cell->scan = self->scan_count;
    }
  }
  
  return 0;
}


// See if the map is ready to fuse
bool scanmap_is_done(scanmap_t *self, int min_scans, int max_scans)
{
  int i;
  int num, min, max;
  scanmap_ladar_t *ladar;

  num = min = max = 0;
  
  // Check how many ladars seem to be active, and how many of those
  // have at least the requsite number of scans.
  for (i = 0; i < sizeof(self->ladars)/sizeof(self->ladars[0]); i++)
  {
    ladar = &self->ladars[i];
    if (ladar->scan_count > 0)
      num += 1;
    if (ladar->scan_count >= min_scans)
      min += 1;
    if (ladar->scan_count >= max_scans)
      max += 1;
  }

  if (min == num)
  {
    MSG("%d/%d ladars have %d scans", min, num, min_scans);    
    return true;
  }
  if (max > 0)
  {
    MSG("%d/%d ladars have %d scans", min, num, min_scans);    
    MSG("%d ladars have %d scans", max, max_scans);
    return true;
  }
  
  return false;
}


// Compute fused map data
int scanmap_fuse(scanmap_t *self)
{
  int i;
  scanmap_ladar_t *ladar;
  gpc_polygon dst;
  
  // Take the union of the free-space polygons for all ladars
  for (i = 0; i < sizeof(self->ladars)/sizeof(self->ladars[0]); i++)
  {
    ladar = &self->ladars[i];
    if (ladar->space.num_contours == 0)
      continue;
    memset(&dst, 0, sizeof(dst));    
    gpc_polygon_clip(GPC_UNION, &self->fused_space, &ladar->space, &dst);
    gpc_free_polygon(&self->fused_space);
    self->fused_space = dst;
  }
  
  return 0;
}
