#ifndef _RWLOCK_HH
#define _RWLOCK_HH

// Wrapper for a pthread read/write lock object
#include <pthread.h>
#include <sched.h>
#include <stdio.h>

#include <dgcutils/DGCutils.hh>

class RWLock
{
private:
    pthread_rwlock_t lock;

public:    
    RWLock()
    {
	if (!DGCcreateRWLock(&lock))
	    fprintf(stderr,"Failed to initialize r/w lock!\n");
    }

    ~RWLock()
    {
	if (!DGCdeleteRWLock(&lock))
	    fprintf(stderr,"Failed to destroy r/w lock!\n");
    }

    void LockRead()
    {
	DGCreadlockRWLock(&lock);
    }

    void LockWrite()
    {
	DGCwritelockRWLock(&lock);
    }

    void Unlock()
    {
	DGCunlockRWLock(&lock);
    }
};

#endif
