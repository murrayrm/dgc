
/* 
 * Desc: Ladar elevation map
 * Date: 30 May 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef EMAP_H
#define EMAP_H

#include <frames/pose3.h>
#include <interfaces/LadarRangeBlob.h>

#if defined __cplusplus
extern "C"
{
#endif


/** @file

@brief Fuse ladar scans into an elevation map.

*/

  
/// @brief Map cell data
typedef struct
{
  // Elevation moments (un-normalized)
  float mn, mz;

  // Residual from local filter (un-normalized)
  float res;
  
} emap_cell_t;
  

/// @brief Elevation map object
typedef struct 
{  
  // Map scale (m/cell)
  float scale;

  // Map size in each dimension
  int size;

  // Map pose
  pose3_t pose;

  // Decay constant (1/s)
  float decay_const;
  
  // Timestamp of last decay
  uint64_t decay_time;
  
  // Transform from local to map frame
  float trans_ml[4][4];

  // Transform from map to local frame
  float trans_lm[4][4];

  // Map data
  emap_cell_t *cells;
  
} emap_t;


  
/// @brief Allocate new display
emap_t *emap_alloc();

/// @brief Free display
void emap_free(emap_t *self);

/// @brief Clear the map and center it on the current local pose.
int emap_clear(emap_t *self, const VehicleState *state);

/// @brief Move the map center but keep the data
int emap_move(emap_t *self, const VehicleState *state);

/// @brief Decay the map.
int emap_decay(emap_t *self, uint64_t time);

/// @brief Add a ladar range blob. 
int emap_add_blob(emap_t *self, LadarRangeBlob *blob);


/// @brief Transfrom from map position to map index
static __inline__ void emap_m2i(emap_t *self, float px, float py, int *mi, int *mj)
{
  *mi = (int) (px/self->scale + 10000000) - 10000000 + self->size/2;
  *mj = (int) (py/self->scale + 10000000) - 10000000 + self->size/2;
  return;
}


/// @brief Transfrom from map index to map position
static __inline__ void emap_i2m(emap_t *self, int mi, int mj, float *px, float *py)
{
  *px = (mi - self->size/2 + 0.5) * self->scale;
  *py = (mj - self->size/2 + 0.5) * self->scale;
  return;
}


/// @brief Return the cell at the given map index
static __inline__ emap_cell_t *emap_cell_index(emap_t *self, int mi, int mj)
{
  if (mi < 0 || mi >= self->size)
    return NULL;
  if (mj < 0 || mj >= self->size)
    return NULL;
  return self->cells + mi + mj * self->size;
}

  
/// @brief Return the cell at the given map position
static __inline__ emap_cell_t *emap_cell_pos(emap_t *self, float px, float py)
{
  int mi, mj;
  emap_m2i(self, px, py, &mi, &mj);
  return emap_cell_index(self, mi, mj);
}

  
#if defined __cplusplus
}
#endif

#endif
