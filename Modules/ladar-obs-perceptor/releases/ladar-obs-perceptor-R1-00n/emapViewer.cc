/*!
 * \file emapViewer.cc
 * \brief A basic viewer for emap class
 * \author Tamas Szalay
 *
 * This file will need to be modified to suit your needs!
 * Call startViewer(EMap *emap) to create window and start
 * viewing thread, and call SetEMap(EMap *map) to set the
 * viewer's visible emap.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define GL_GLEXT_PROTOTYPES
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <GL/glext.h>
#endif

#include <sys/types.h>
#include <string.h>

#include <dgcutils/DGCutils.hh>
#include <alice/AliceConstants.h>
#include <emap/emap.hh>
#include <emap/emapWrapper.hh>

#include "LadarPerceptor.hh"

using namespace std;

//OpenGL variables
int drag_x0, drag_y0;
int lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;

// Initial value, will be set when thread is started
int WINDOW_SIZE=500;
const int BOT_SIZE=100;
const int BOT_BORDER=15;

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
EMap *map;
EMap *map_orig; // Unscaled map, if you need it
emap_t curMin, curMax; // The current minimum and maximum values of map
double dHeight; // The change in height from curMin to curMax
double aliceYaw; // Our yaw
char strDisplay[64]; //On-screen display string
int dispMode=0;
int curLayer=1;
int threshLim=45000;

bool showPoly = false;

EMap *maps[4]; // our various maps

// Display on bottom
bool botVisible = false;
float *botPos; // positions of points
int botCount=0; // # of points to draw
float botRange;

static vector<point2arr> curPoints;

void EVsetPoints(vector<point2arr> points)
{
    curPoints = points;
}

void drawString (char *s)
{
    unsigned int i;
    for (i = 0; i < strlen (s); i++)
	glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
}

void makePolygons();

void setEMap(int layer, EMap *emap)
{
    if (!maps[layer])
	return;
    DGClockMutex(&mutex1);
    maps[layer]->copyFrom(emap);
    if (layer == 1)
      { 
	if (showPoly) {
	  timer.startTime();
	  map->copyFrom(maps[1]);
	  map->threshold(threshLim);
	  map->dilate(5);
	  map->erode(3);
	  makePolygons();
	  map->copyFrom(maps[curLayer]);
	  timer.printTime("Polygon segment time");

	} else {
	  curPoints.clear();
	}
    } 
    if (layer == 2)
    {
//	maps[2]->threshold(0);
    }
    if (curLayer == 0)
    {
//	map->erode(4);
    }
    if (layer == curLayer)
	map->copyFrom(maps[curLayer]);
    DGCunlockMutex(&mutex1);
}

void setEMap(EMap *emap, emap_t min, emap_t max, double dh, double yaw)
{
    if (!map)
	return;
    DGClockMutex(&mutex1);
    map->copyFrom(emap);
    map_orig->copyFrom(emap);
    map->scale(min, max);
    curMin = min;
    curMax = max;
    dHeight = dh;
    aliceYaw = yaw+M_PI/2;
    DGCunlockMutex(&mutex1);

    if (dispMode==1)
    {
	DGClockMutex(&mutex1);
	map->threshold(0xFFFF-0x2000);
	DGCunlockMutex(&mutex1);
    }

    glutPostRedisplay();
}

void setYaw(double yaw)
{
    aliceYaw = yaw-M_PI/2;
}

void drawPoints(void)
{
    for (size_t i=0;i<curPoints.size();i++)
    {
	glColor3f(1,0,0);
	glBegin(GL_LINE_LOOP);
	for (size_t j=0;j<curPoints[i].size();j++)
	{
	    glVertex2d(curPoints[i][j].x,curPoints[i][j].y);
	}
	glEnd();
    }
    return;
    glPointSize(4);
    glBegin(GL_POINTS);
    for (size_t i=0;i<curPoints.size();i++)
    {
	glVertex2d(curPoints[i][0].x,curPoints[i][0].y);
	glVertex2d(curPoints[i].back().x,curPoints[i].back().y);
    }
    glEnd();
}

void drawEMaps(void)
{

    glWindowPos2i(0,WINDOW_SIZE);
    glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_SHORT, maps[2]->getData());


    glWindowPos2i(WINDOW_SIZE,WINDOW_SIZE);
    glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_SHORT, maps[0]->getData());

    glWindowPos2i(0,2*WINDOW_SIZE);
    glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_SHORT, maps[1]->getData());


    glWindowPos2i(WINDOW_SIZE,2*WINDOW_SIZE);
    glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_SHORT, maps[3]->getData());

    // draw Alice
    float verts[4][2];
    verts[0][0] = -VEHICLE_WIDTH/2;
    verts[1][0] = -VEHICLE_WIDTH/2;
    verts[2][0] = VEHICLE_WIDTH/2;
    verts[3][0] = VEHICLE_WIDTH/2;
    verts[0][1] = DIST_REAR_AXLE_TO_FRONT;
    verts[1][1] = -DIST_REAR_TO_REAR_AXLE;
    verts[2][1] = DIST_REAR_AXLE_TO_FRONT;
    verts[3][1] = -DIST_REAR_TO_REAR_AXLE;

    // now rotate it
    for (int i=0;i<4;i++)
    {
	float tx  = cos(aliceYaw)*verts[i][0] - sin(aliceYaw)*verts[i][1];
	float ty = sin(aliceYaw)*verts[i][0] + cos(aliceYaw)*verts[i][1];
	verts[i][0] = EMAP_SIZE/2 + tx/EMAP_RES;
	verts[i][1] = EMAP_SIZE + EMAP_SIZE/2 + ty/EMAP_RES;
    }

    //now draw it!
    glColor3f(1,1,1);
    glBegin(GL_TRIANGLE_STRIP);
    for (int i=0;i<4;i++)
	glVertex2f(verts[i][0],verts[i][1]);
    glEnd();

}

void drawEMap(void)
{
    int dy = botVisible?BOT_SIZE:0;
    glWindowPos2i(0,WINDOW_SIZE+dy);

    if (sizeof(emap_t) == 1)
	glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_BYTE, map->getData());
    else if (sizeof(emap_t) == 2)
	glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_SHORT, map->getData());
    else if (sizeof(emap_t) == 4)
	glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_INT, map->getData());

    glWindowPos2i(10,10+dy);
    glColor3f(1.0f,0.6f,0.6f); // Laura wanted it to be pink, ok?
    if (lbuttondown)
    {
	glBegin(GL_LINE_LOOP);
	glVertex2i(drag_x0,drag_y0);
	glVertex2i(drag_x0,lasty);
	glVertex2i(lastx,lasty);
	glVertex2i(lastx,drag_y0);	
	glEnd();
    }
    if (curLayer == 1)
	sprintf(strDisplay,"Threshold: %d", threshLim);
    drawString(strDisplay);

    if (rbuttondown)
    {
	glBegin(GL_LINES);
	glVertex2i(drag_x0,drag_y0);
	glVertex2i(lastx,lasty);
	glEnd();
    }

    if (botVisible)
    {
	glColor3f(1,1,1);
	glBegin(GL_QUADS);
	glVertex2i(0,WINDOW_SIZE);
	glVertex2i(WINDOW_SIZE,WINDOW_SIZE);
	glVertex2i(WINDOW_SIZE,WINDOW_SIZE+BOT_SIZE);
	glVertex2i(0,WINDOW_SIZE+BOT_SIZE);
	glEnd();
	glColor3f(0,0,0);
	glWindowPos2i(0,0);
	char range[16];
	sprintf(range,"%3.2f cm",100*botRange);
	drawString(range);
	glColor3f(0.3,0.3,1);
	glBegin(GL_LINE_STRIP);
	if (botCount > 1)
	{
	    float x,y;
	    for (int i=0;i<botCount;i++)
	    {
		if (botPos[i] < 0)
		    continue;
		x = i*(WINDOW_SIZE-2*BOT_BORDER)/(float)(botCount-1);
		x += BOT_BORDER;
		y = -botPos[i] + WINDOW_SIZE +BOT_SIZE - BOT_BORDER;
		glVertex2f(x,y);
	    }
	}
	glEnd();
    }
        // draw Alice
    float verts[4][2];
    verts[0][0] = -VEHICLE_WIDTH/2;
    verts[1][0] = -VEHICLE_WIDTH/2;
    verts[2][0] = VEHICLE_WIDTH/2;
    verts[3][0] = VEHICLE_WIDTH/2;
    verts[0][1] = DIST_REAR_AXLE_TO_FRONT;
    verts[1][1] = -DIST_REAR_TO_REAR_AXLE;
    verts[2][1] = DIST_REAR_AXLE_TO_FRONT;
    verts[3][1] = -DIST_REAR_TO_REAR_AXLE;

    // now rotate it
    for (int i=0;i<4;i++)
    {
	float tx  = cos(aliceYaw)*verts[i][0] - sin(aliceYaw)*verts[i][1];
	float ty = sin(aliceYaw)*verts[i][0] + cos(aliceYaw)*verts[i][1];
	verts[i][0] = EMAP_SIZE/2 + tx/EMAP_RES;
	verts[i][1] = EMAP_SIZE/2 + ty/EMAP_RES;
    }

    //now draw it!
    glColor3f(0.4f,0.7f,0.4f);
    glBegin(GL_TRIANGLE_STRIP);
    for (int i=0;i<4;i++)
	glVertex2f(verts[i][0],verts[i][1]);
    glEnd();
}

void showBot()
{
    botVisible = true;
    glutReshapeWindow(WINDOW_SIZE,WINDOW_SIZE+BOT_SIZE);
}

void hideBot()
{
    botVisible = false;
    glutReshapeWindow(WINDOW_SIZE,WINDOW_SIZE);
}

void computeGradients()
{
    const int cellSize = 8;
    const int cellCount = WINDOW_SIZE/cellSize;

    DGClockMutex(&mutex1);

    emap_t min,max;
    int cx, cy;
    map->copyFrom(map_orig);
    for (int x=0;x<cellCount;x++)
    {
	for (int y=0;y<cellCount;y++)
	{
	    cx = x*cellSize;
	    cy = y*cellSize;
	    map->minmaxRectangle(cx,cy,cx+cellSize-1,cy+cellSize-1,min,max);
	    if (min > max)
		max = min;
	    map->drawRectangle(cx,cy,cx+cellSize-1,cy+cellSize-1,(max-min));
	}
    }
    map->minmaxRectangle(0,0,WINDOW_SIZE-1,WINDOW_SIZE-1,min,max);
    map->scale(0,max/4);
    DGCunlockMutex(&mutex1);
}

inline int getDir(int dir,int width)
{
    switch (dir)
    {
    case 1:
	return -width;
    case 2:
	return -width+1;
    case 3:
	return 1;
    case 4:
	return width+1;
    case 5:
	return width;
    case 6:
	return width-1;
    case 7:
	return -1;
    case 0:
	return -width-1;
    default:
	return 0;
    }
}

inline int moveDir(emap_t* &pix, int lastDir, int width)
{
    emap_t *lastPix=pix;
    switch (lastDir)
    {
    case -EMAP_SIZE:
	pix += width - 1;
	lastPix = pix + 1;
	goto bl;
    case -EMAP_SIZE+1:
	pix--;
	lastPix = pix + width;
	goto lt;
    case 1:
	pix -= (width+1);
	lastPix = pix + width;
	goto ul;
    case EMAP_SIZE+1:
	pix -= width;
	lastPix = pix - 1;
	goto up;
    case EMAP_SIZE:
	pix -= (width-1);
	lastPix = pix - 1;
	goto ur;
    case EMAP_SIZE-1:
	pix ++;
	lastPix = pix - width;
	goto rt;
    case -1:
	pix += width + 1;
	lastPix = pix - width;
	goto br;
    case -EMAP_SIZE-1:
	pix += width;
	lastPix = pix + 1;
	goto bt;	
    }
up:
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;
ur:
    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;
rt:
    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;
br:
    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;
bt:
    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;
bl:
    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;
lt:
    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;
ul:
    if (*lastPix == 0 && *pix > 0)
	return -width-1;
    lastPix = pix;
    pix++;
// now for wrap-around calls
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;

    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;

    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;

    return 0;
}

point2 makePoint(int i, int dir, int width)
{
    int y = i/width;
    int x = i-(y*width);
    switch(dir)
    {
    case 0:
	return point2(x-0.5,y);
    case 1:
	return point2(x-0.5,y-0.5);
    case 2:
	return point2(x,y-0.5);
    case 3:
	return point2(x+0.5,y-0.5);
    case 4:
	return point2(x+0.5,y);
    case 5:
	return point2(x+0.5,y+0.5);
    case 6:
	return point2(x,y+0.5);
    case 7:
	return point2(x-0.5,y+0.5);
    default:
	return point2(x,y);
    }
}

void makePolygons()
{
//    DGClockMutex(&mutex1);
    int width = EMAP_SIZE;
    curPoints.clear();
    emap_t *pix = map->getData();
    emap_t *end = pix + width*width;
    emap_t *curPix;
    int curDir;
    int lastDir;
    // set edges of map to 0 to avoid edge issues
    for (int i=0;i<width;i++)
    {
	*(pix+i) = 0;
	*(pix+i*width) = 0;
	*(pix+i*width+width-1) = 0;
	*(pix+width*width-i-1) = 0;
    }

    // loop through map
    while (pix < end)
    {
	if (*pix == 0)
	{
	    pix++;
	    continue;
	}
	// found a polygon, start looping through
	else if (*pix == 0xFFFF && *(pix-1) == 0) // new polygon, == 0 to avoid doing donuts
	{
	    int ind = (pix-map->getData());
	    point2arr ptarr;
	    ptarr.push_back(makePoint(ind,-1,width));
	    curPix = pix;
	    *pix = 1; // set start location
	    curDir = moveDir(curPix,1,width);
	    lastDir = 0;
	    while (*curPix > 1)
	    {
		ind += curDir;
		if (curDir != lastDir)
		    ptarr.push_back(makePoint(ind,-1,width));
		// mark that we have been here
		(*curPix)--;
		lastDir = curDir;
		curDir = moveDir(curPix,lastDir,width);
	    }
//	    printf("Found poly of size %d\n",(int)ptarr.size());
	    if (ptarr.size() > 3)
		curPoints.push_back(ptarr);
	    else
		printf("Tried to make poly of size %d\n",(int)ptarr.size());
	    // now move to end of poly
	    while (*pix != 0)
		pix++;
	}
	else
	{
	    while (*pix != 0) // have already done this poly
		pix++;
	}
    }
    printf("Poly count: %d\n",(int)curPoints.size());
//    DGCunlockMutex(&mutex1);
}

void display(void)
{
    DGClockMutex(&mutex1);
    if (dispMode == 2)
	drawEMaps();
    else
	drawEMap();
    drawPoints();
    DGCunlockMutex(&mutex1);
    glFlush();
    usleep(50000);
    glutPostRedisplay();
}

void idle (void) 
{
    //replot data
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    lastx = x;
    lasty = y;
    if(button == GLUT_LEFT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	  drag_x0 = x;
	  drag_y0 = y;
	}
	else
	{
	    lbuttondown = false;
	    if (drag_x0 >= x || drag_y0 >= y)
	      return;
	    if (x >= map_orig->getWidth() || y >= map_orig->getWidth())
	      return;
	    
	    emap_t min=-1;
	    emap_t max=0;
	    maps[curLayer]->minmaxRectangle(drag_x0,drag_y0,x,y,min,max);
	    
	    DGClockMutex(&mutex1);
	    map->copyFrom(maps[curLayer]);
	    map->scale(min,max);
	    DGCunlockMutex(&mutex1);
	    //float fac = dHeight / (float)(curMax - curMin);
	    sprintf(strDisplay,"sel. range: %d,%d\n", min,max);
	}
    }   
    if(button == GLUT_RIGHT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	    rbuttondown = true;
	    drag_x0 = x;
	    drag_y0 = y;
	}
	else
	{
	    showBot();
	    rbuttondown = false;
	    if (x < 0 || y < 0 || x >= WINDOW_SIZE || y >= WINDOW_SIZE)
		return;

	    if (curMax == curMin)
		curMax++;
	    float fac = dHeight / (float)(curMax - curMin);
	    emap_t pos[WINDOW_SIZE];
	    DGClockMutex(&mutex1);
	    map_orig->getLine(drag_x0,drag_y0,x,y,pos,botCount);
	    DGCunlockMutex(&mutex1);
	    fprintf(stderr,"%d\n",botCount);
	    float posMin=1e10, posMax=0;
	    for (int i=0;i<botCount;i++)
	    {
		botPos[i] = pos[i]*fac;
		if (botPos[i] < posMin && pos[i] != 0)
		    posMin = botPos[i];
		else if (botPos[i] > posMax)
		    posMax = botPos[i];
	    }
	    if (posMin > posMax) //no points found
	    {
		botCount = 0;
		return;
	    }
	    if (posMin == posMax)
		posMax = posMax + 1;

	    for (int i=0;i<botCount;i++)
	    {
		if (pos[i] == 0)
		{
		    botPos[i] = -1;
		    continue;
		}
		botPos[i] = (BOT_SIZE-2*BOT_BORDER)*(botPos[i] - posMin)/(posMax-posMin);
	    }

	    botRange = posMax-posMin;
	}
    }
}

void drawPoly()
{
    int n=360;
    point2arr pts;
    for (int i=0;i<n;i++)
    {
	double ang = (M_PI*i)/n+1.57;
	double rad = (i&16)?100:200;
	pts.push_back(point2(rad*cos(ang),rad*sin(ang)));
    }

    float mat[4][4];
    EMapWrapper::getTransform(map,200,0,2,mat);
    Point2 *pt = EMapWrapper::makePoly(pts,mat);

/*    pt[0].x = 100; pt[0].y = 100;
    pt[1].x = 200; pt[1].y = 100;
    pt[2].x = 100; pt[2].y = 200;
    n = 3;*/

    uint64_t start = DGCgettime();
    for (int i=0;i<1000;i++)
	map->incPolygon(n, pt, 0x0010);
    printf("Poly time: %d us/poly \n",(int)(DGCgettime()-start)/(1000));

//    map->clear(0);

    start = DGCgettime();
    for (int i=0;i<1000;i++)
	map->incTriangle(100,100,200,100,100,200,0x0010);
    printf("Triangle time: %d us/poly \n",(int)(DGCgettime()-start)/(1000));

    free(pt);
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case '1':
	curLayer = 0;
	map->copyFrom(maps[curLayer]);
	break;
    case '2':
	curLayer = 1;
	map->copyFrom(maps[curLayer]);
	break;
    case '3':
	curLayer = 2;
	map->copyFrom(maps[curLayer]);
	break;
    case '4':
	curLayer = 3;
	map->copyFrom(maps[curLayer]);
	break;
    case '=':
    case '+':
	threshLim += 100;
	break;
    case '-':
	threshLim -= 100;
	break;
    case 'p':
	showPoly = !showPoly;
	break;
    case 'm':
	map->manhattan(); 
	map->normalize();
	break;
    case 'e':
	map->erode(1);
	break;
    case 'd':
	map->dilate(1);
	break;
    case 'h':
	hideBot();
	break;
    case 'g':
	computeGradients();
	break;
    case 't':
	map->threshold(threshLim);
	if (dispMode==1)
	    dispMode=0;
	else
	    dispMode=1;
	break;
    case 'f':
	if (dispMode==2)
	{
	    dispMode=0;
	    glutReshapeWindow(WINDOW_SIZE,WINDOW_SIZE);
	    fprintf(stderr, "changed to single pane window \n");
	}
	else
	{
	    dispMode=2;
	    glutReshapeWindow(WINDOW_SIZE*2,WINDOW_SIZE*2);
	    fprintf(stderr, "changed to 4-paned window \n");
	}
	break;
    case 'r': 
	DGClockMutex(&mutex1);
	map->copyFrom(map_orig);
	if (curMax > curMin)
	    map->scale(curMin,curMax);
	else
	    map->normalize();
	DGCunlockMutex(&mutex1);
	break;
    case 'q': 
	break;
    }
}

void motion(int x, int y)
{
    lastx = x;
    lasty = y;
    if (lbuttondown)
    {
    }
    
    if (rbuttondown)
    {
    }
}

// Disable resizing
void resize(int w, int h)
{
    // Set transform matrix to be the pixel transform
    glLoadIdentity();
    glViewport (0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glOrtho(0,w,h,0,-1,1);

    int dw = (dispMode==2)?WINDOW_SIZE*2:WINDOW_SIZE;

    if (w != dw || (h != dw && h != dw+BOT_SIZE))
	glutReshapeWindow(dw,dw);
}

void * mainLoop(void*)
{
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE);
    glutCreateWindow("EMap Viewer");  
    glClearColor (0.0,0.0,0.0,0.0); //clear the screen to black
    glShadeModel(GL_FLAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, sizeof(emap_t));
    glPixelZoom(1, -1);

    map = new EMap(WINDOW_SIZE);
    map_orig = new EMap(WINDOW_SIZE);
    for (int i=0;i<4;i++)
	maps[i] = new EMap(WINDOW_SIZE);
    DGCcreateMutex(&mutex1);
    botPos = new float[WINDOW_SIZE];
    
    /* set callback functions */
    glutDisplayFunc(display); 
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);
    glutReshapeFunc(resize);

    //========================
    // initialize Map
    //========================
    glutMainLoop();
    // The above function never returns anyway

    //glutDestroyWindow(win);
    DGClockMutex(&mutex1);
    delete map;
    map = NULL;
    DGCunlockMutex(&mutex1);
    DGCdeleteMutex(&mutex1);

    return 0;
}

pthread_t startViewer(int argc, char **argv, int size)
{
    WINDOW_SIZE = size;
    glutInit(&argc, argv);
    pthread_t thread;
    pthread_create(&thread, 0, &mainLoop, 0);    
    return thread;
}
