#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <frames/vec3.h>
#include <dgcutils/DGCutils.hh>
#include "Log.hh"

struct LadarScan
{
    int numPoints; // # of points in scan
    int blobId; // the blob id of this scan
    int ladarId; // the id (not sensor id) of ladar
    LadarRangeBlob blob; // original ladar blob
    vec3_t points[LADAR_BLOB_MAX_POINTS]; // transformed blob points
    int lastElementId[LADAR_BLOB_MAX_POINTS]; // last el we associated with
    vec3_t ladarPos; // position of the ladar
    bool processed; // has this scan been processed?
    uint64_t timestamp; // time we received it (blob timestamp is send time)
    

    // -- groundstrike information --
    uint8_t road[LADAR_BLOB_MAX_POINTS]; //only for sweeping ladars
    float gsDynConf[LADAR_BLOB_MAX_POINTS]; // based on points' behavior
    float gsRoadConf[LADAR_BLOB_MAX_POINTS]; // based on distance to road
    float gsRoofConf[LADAR_BLOB_MAX_POINTS]; // based on roof ladar visibility
};

// number of scans to store from each ladar
#define NUM_SCANS 15
#define NUM_LADARS 8

static sensnet_id_t ladarSensorId[NUM_LADARS] = {
    SENSNET_MF_BUMPER_LADAR,
    SENSNET_LF_BUMPER_LADAR,
    SENSNET_RF_BUMPER_LADAR,
    SENSNET_REAR_BUMPER_LADAR,
    SENSNET_LF_ROOF_LADAR,
    SENSNET_RF_ROOF_LADAR,
    SENSNET_PTU_LADAR,
    SENSNET_RIEGL };

class LadarScanStore
{
    LadarScan scans[NUM_LADARS][NUM_SCANS];
    int curIndex[NUM_LADARS]; // index of latest blob
    int lastBlobId[NUM_LADARS]; // id of latest blob
    int firstBlobId[NUM_LADARS]; // id of oldest blob

public:
    LadarScanStore()
    {
	memset(this,0,sizeof(*this));
    }

    ~LadarScanStore()
    {
    }

    // return a scan if we have it, else return false
    bool getScan(LadarScan* &scan, int ladarId, int blobId)
    {
	// bad ladar
	if (ladarId >= NUM_LADARS || ladarId < 0)
	    return false;
	// bad scan req - even if we have the scan (because of a gap), pretend we don't
	if ((lastBlobId[ladarId]-blobId) >= NUM_SCANS || blobId > lastBlobId[ladarId])
	    return false;
	// we *should* have the scan, but might have missed it
	int index = curIndex[ladarId];
	for (int i=0; i<NUM_SCANS; i++)
	{
	    if (scans[ladarId][index].blobId == blobId)
	    {
		scan = &(scans[ladarId][index]);
		return true;
	    }
	    index--;
	    if (index < 0)
		index += NUM_SCANS;
	}
	// we don't have the scan, in which case we really shouldn't be checking for it
	fprintf(stderr,"Error: tried to read missed scan %d from ladar %d\n",ladarId,blobId);
    }

    LadarScan* getPastScan(int ladarId, int numPast)
    {
	int index = curIndex[ladarId] - numPast;
	if (index < 0)
	    index += NUM_SCANS;
	if (scans[ladarId][index].blobId == 0 || numPast >= NUM_SCANS) {
	    index = curIndex[ladarId];
	}
	return &(scans[ladarId][index]);
    }

    LadarScan* nextScan(int ladarId, int blobId)
    {
	if (ladarId >= NUM_LADARS || ladarId < 0)
	    return NULL;
	// increment write index
	curIndex[ladarId]++;
	if (curIndex[ladarId] == NUM_SCANS)
	    curIndex[ladarId] = 0;	    

	// set first blob id
	if (curIndex[ladarId] == NUM_SCANS-1)
	    firstBlobId[ladarId] = scans[ladarId][0].blobId;
	else
	    firstBlobId[ladarId] = scans[ladarId][curIndex[ladarId]+1].blobId;

	lastBlobId[ladarId] = blobId;
	// return pointer to scan
	return &(scans[ladarId][curIndex[ladarId]]);
    }

    int getLastBlobId(int ladarId)
    {
	return lastBlobId[ladarId];
    }
};

#define NUM_TIMERS 16

// Some helper functions for timing/profiling
class LadarTimer
{
    uint64_t start[NUM_TIMERS];
    uint64_t starttime;
    double totalTime[NUM_TIMERS];
    int ticks[NUM_TIMERS];

public:
    LadarTimer() {}
    ~LadarTimer() {}

    void startTime(int timer)
    {
	start[timer] = DGCgettime();
    }

    void startTime()
    {
	starttime = DGCgettime();
    }

    int endTime(int timer)
    {
	int time = (int)(DGCgettime() - start[timer]);
	totalTime[timer] += time;
	ticks[timer]++;
	return time;
    }

    int endTime()
    {
	return (int)(DGCgettime()-starttime);
    }

    void printTime(char *str, int timer)
    {
	//printf("%s: %d us\n",str,endTime(timer));
      //      Log::getStream(9)<<str<<": "<<endTime(timer)<<"us"<<endl;
    }

    void printTime(char *str)
    {
	//printf("%s: %d us\n",str,endTime());
      //      Log::getStream(9)<<str<<": "<<endTime()<<"us"<<endl;
    }

    double getAvg(int timer)
    {
	if (ticks[timer] == 0)
	    return 0;
	return totalTime[timer]/ticks[timer];
    }
};

// our static timer
static LadarTimer timer;
