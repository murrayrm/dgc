#include "LadarMap.hh"

void LadarMap::update()
{
    if (scans == 0)
    {
	usleep(10000);
	return;
    }

    for (int i=0;i<NUM_TRACK_LADARS;i++)
    {
	LadarScan *scan = scans->getPastScan(i,0);
	if (!scan->processed)
	    scan = scans->getPastScan(i,1);
	this->tracker[i].ProcessScan(scan);	
    }

    uint64_t time = DGCgettime();
    updateTracks();
    this->updateCount++;
//    printf("Track update time: %d\n",(int)(DGCgettime()-time)/1000);

    // send debug info
    if ((DGCgettime()-this->lastDebugTime) > MAP_DEBUG_RATE)
    {
	this->lastDebugTime = DGCgettime();	
	if (false)//(this->sendDebug)
	    for (int i=0;i<NUM_TRACK_LADARS;i++)
		this->tracker[i].sendDebug(this->debugTalker,this->debugSubgroup);
    }
    // send dyn obs
    if ((DGCgettime()-this->lastTrackTime) > MAP_TRACK_RATE)
    {
	this->lastTrackTime = DGCgettime();
	sendTracks();
	//fprintf(stderr,"Track send time: %d\n",(int)(DGCgettime()-this->lastTrackTime)/1000);
    }
    // update obs map
    if ((DGCgettime()-this->lastStaticTime) > MAP_STATIC_RATE)
    {
	this->staticMap->copyFrom(smap->getMap());
	this->smap->getMatrices(local2map,map2local);
	this->lastStaticTime = DGCgettime();
	updateStatic();
	sendStatic();
	//fprintf(stderr,"Stat time:   %d\n",(int)(DGCgettime()-this->lastStaticTime)/1000);
    }
    if (DGCgettime()-this->lastSecond > 1000000)
    {
	printf("Update count in last second:    %d\n",this->updateCount);
	this->lastSecond = DGCgettime();
	this->updateCount = 0;
    }
    // do freespace
    // DISABLED because planner does not support it sufficiently for the race
/*    if (this->sendFS &&
	(DGCgettime()-this->lastFreespaceTime) > MAP_FS_RATE)
    {
	this->lastFreespaceTime = DGCgettime();
	this->staticMap->copyFrom(smap->getMap());
	this->smap->getMatrices(local2map,map2local);
	// threshold properly
	this->staticMap->invthreshold(MAP_FS_THRESH);
	point2arr fs = this->getFreespace();
	this->sendFreespace(fs);
	}*/
    usleep(1);
}

void LadarMap::updateTracks()
{
    LinkedSegment segs[MAX_NUM_NSEGS];
    int lastBlob[NUM_TRACK_LADARS];
    
    for (int i=0;i<NUM_TRACK_LADARS;i++)
	lastBlob[i] = tracker[i].getBlobId();

    // pre-process the new segments (associating 'tween them)
    int numFresh = processNewSegments(segs);

    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	track[i].updated = false;
//	track[i].numSegments = 0;
    }

//    double maxscore,d;
//    int index;

    // associate every track in every ladar (it's not that many)
    // newness: only associate the latest tracks
/*    for (int i=0;i<NUM_TRACK_LADARS;i++)
    {
	for (int j=0;j<tracker[i].getHighestTrack();j++)
	{
	    TrackedSegment& ts = tracker[i].getTrack(j);
	    if (ts.status <= 0)
		continue;
	    // only consider latest tracks
	    if (ts.segment[0].blobId < lastBlob[i])
		continue;
	    // make sure geometry exists also
	    if (ts.segment[0].blobId != ts.segment[0].scan->blobId)
	    {
		ts.lastMapTrack = -1;
		ts.status = -1;
		fprintf(stderr,"-----invalid scan-----\n");
	        continue;
	    }
	    // check prev. association
	    if (ts.lastMapTrack != -1 && track[ts.lastMapTrack].status > 0)
	    {
		d = track[ts.lastMapTrack].pos.dist(point2(ts.x[0],ts.y[0]));
		if (d < MAP_ASSOC_DIST)
		{
		    addSegment(ts.lastMapTrack,i,j);
		    continue; //still good
		}
	    }
	    // none found, check for new ones
	    index = -1;
	    maxscore = 0;
	    for (int k=0;k<MAP_MAX_TRACKS;k++)
	    {
		if (track[k].status <= 0)
		    continue;
		d = getScore(k,ts);
		if (d > maxscore)
		{
		    index = k;
		    maxscore = d;
		}
	    }
	    // found one, make sure one of our statuses is high-ish
	    if (index > -1 && (ts.status > MAP_SEG_MINSTATUS || 
		    track[index].status > MAP_LIVE_THRESH))
	    {
		ts.lastMapTrack = index;
		addSegment(index,i,j);
	    }
	    else // didn't find one, create?
	    {
		testCreateTrack(ts,i,j);
	    }
	}
	}*/

    int kn=0;
    // check every found LinkedSegment chain against each track
    for (int i=0;i<numFresh;i++)
    {
	// already associated
	if (segs[i].assoc)
	    continue;
	// get the min/max of the LinkedSegment
	vec3_t bmin = segs[i].segment->min;
	vec3_t bmax = segs[i].segment->max;
	int cur = segs[i].nextSegment;
	segs[i].assoc = true;
	while (cur != i)
	{	    
	    bmin = vec3_min(bmin,segs[cur].segment->min);
	    bmax = vec3_max(bmax,segs[cur].segment->max);
	    segs[cur].assoc = true;
	    cur = segs[cur].nextSegment;
	}
	// and now do the associations
	double score;
	double maxscore = 0;
	int maxindex = -1;
	for (int j=0;j<MAP_MAX_TRACKS;j++)
	{
	    if (track[j].status <= 0)
		continue;
	    //   if (track[j].updated)
	    //continue;
	    //if (lastBlob[track[j].segments[0][0]] == track[j]
	    score = getLinkedScore(segs,i,j,bmin,bmax);
	    if (score > maxscore)
	    {
		maxscore = score;
		maxindex = j;
	    }
	}
	// did we find one?
	if (maxindex >= 0)
	{
	    updateTrack(segs,i,maxindex,bmin,bmax);
	}
	else
	{
	    if (testCreateTrack(segs,i,bmin,bmax))
		kn++;
	}
    }

//    printf("Num added: %d\n",kn);

    // now remove every unassociated track (harsh, i know)
    for (int i=0;i<MAP_MAX_TRACKS;i++)
	if (!track[i].updated && track[i].status > 0)
	    removeTrack(i);

    // now every track should be created/associated... we need to do confidences
    // and update geometries
    int n;
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status <= 0)
	    continue;
	if (track[i].numSegments == 0)
	{
	    removeTrack(i);
	    continue;
	}
	    
//	track[i].geometry.clear();
	track[i].status--;
	// shift old positions
	memmove(track[i].pastpos+1,track[i].pastpos,sizeof(point2)*(MAP_PAST_POS-1));
	track[i].pastpos[0] = track[i].pos;
	track[i].numPastPos++;
	track[i].pos = point2();
	track[i].vel = point2();
	double totalweight = 0;
	double weight;
	double tvel;
	point2 minvel=point2(1000,1000);
	point2 maxvel=point2(-1000,-1000);
	for (int j=0;j<track[i].numSegments;j++)
	{	    
//	    int ladarId = track[i].segment[j][0];
//	    TrackedSegment& ts = tracker[ladarId].getTrack(track[i].segment[j][1]);
	    
	    TrackedSegment* ts = track[i].tracked[j];
	    LadarSegment* seg = ts->segment;
	    // also inc for foreground
	    if (seg->flags == FG_B)
		track[i].status++;

	    // increment if high confidence, dec if not
	    if (ts->status > MAP_STATUS_THRESH)
		track[i].status++;
//	    else
//		track[i].status--;

	    // inc like crazy for reflectivity
	    if (ts->numReflective > 0)
		track[i].status += 3;

	    // car confidence metrics
	    if (!(ts->carConf & CAR_SIZE))
		track[i].status-=3;
	    if (!(ts->carConf & CAR_CORNERS))
		track[i].status--;
	    
	    // find min and max vel
	    if (ts->x[1] < minvel.x)
		minvel.x = ts->x[1];
	    else if (ts->x[1] > maxvel.x)
		maxvel.x = ts->x[1];
	    if (ts->y[1] < minvel.y)
		minvel.y = ts->y[1];
	    else if (ts->y[1] > maxvel.y)
		maxvel.y = ts->y[1];	  		

	    n = (seg->end-seg->start+1);
	    // scale by velocity magnitude, weight smaller ones more heavily
	    tvel = sqrt(point2(ts->x[1],ts->y[1]).norm());
	    if (tvel > 0)
		weight = n*ts->status/tvel;
	    else
		weight = n*ts->status;
	    // add pos,vel weighted by # points & conf
	    track[i].pos = track[i].pos + weight*point2(ts->x[0],ts->y[0]);
	    track[i].vel = track[i].vel + weight*point2(ts->x[1],ts->y[1]);
	    totalweight += weight;
	    // update geometry, why not
//	    point2arr pts = tracker[ladarId].getSegmentPoints(ts.segment[0]);
//	    for (size_t k=0;k<pts.size();k++)
//		track[i].geometry.push_back(pts[k]);
	}
	track[i].pos = track[i].pos / totalweight;
	track[i].vel = track[i].vel / totalweight;
	// if (very!) high velocity variability, decrement
	if ((maxvel.x-minvel.x)*(maxvel.y-minvel.y) > MAP_MAX_VAREA)
	    track[i].status -= 1;

	if (track[i].status > MAP_MAX_STATUS)
	    track[i].status = MAP_MAX_STATUS;

	// do motion checks and stuff
	checkMoving(i);

	if (track[i].status <= 0)
	    removeTrack(i);
    }

    // remove dynamic stuff from static map
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status <= 0)
	    continue;
	//if (!track[i].seenMoving)
	//    continue;
	for (int j=0;j<track[i].numSegments;j++)
	{
//	    int ladarId = track[i].segment[j][0];
//	    TrackedSegment& ts = tracker[ladarId].getTrack(track[i].segment[j][1]);
	    LadarSegment* seg = track[i].tracked[j]->segment;
	    seg->scan->usedForUpdate = true;
	    for (int k=seg->start;k<=seg->end;k++)
		seg->scan->isDynamic[k] = true;
	}
    }	
}

void LadarMap::sendTracks()
{
    MapElement el;
    MapElement elVel;
    MapElement elClear;

    el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_YELLOW;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    elClear.setTypeClear();

    el.setTypePoints();

    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status < 0)
	{	    
	    // send a clear
	    elClear.setId(this->moduleId,i+MAP_MAX_STATIC);
	    this->mapTalker.sendMapElement(&elClear,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&elClear,debugSubgroup);
	    track[i].status = 0;
	}
	else if (track[i].status > 0)
	{
	    if (track[i].numSegments == 0)
		continue;

	    el.plotValue = 100;

	    if (track[i].seenMoving)
		el.setTypeVehicle();
	    else if (track[i].status > MAP_LIVE_THRESH)
		el.setTypeObstacle();
	    else
	    {
		el.setTypeClear();
//		el.setTypeObstacle();
		//	el.plotValue = 50;
	    }

	    if (track[i].lastMoving > 0)
		el.timeStopped = 0.000001*(int)(DGCgettime()-track[i].lastMoving);

	    el.velocity = track[i].vel;
	    // decrease velocity if we just saw it
	    if (0.000001*(int)(DGCgettime()-track[i].firstSeen) < MAP_DYN_TIME)
		el.velocity = el.velocity*0.5;

	    if (track[i].seenMoving)
		el.plotColor = MAP_COLOR_GREEN;
	    else
		el.plotColor = MAP_COLOR_YELLOW;

	    point2arr geo = makeGeometry(i);
	    el.setGeometry(geo);
	    el.setId(this->moduleId,i+MAP_MAX_STATIC);

	    this->mapTalker.sendMapElement(&el,sendSubgroup);

	    if (this->sendDebug)
	    {
		el.typeConf = track[i].status;
		this->debugTalker.sendMapElement(&el,debugSubgroup);
	    }
	}
    }
}

bool LadarMap::checkMoving(int index)
{
    // no newbies
    if (track[index].status < MAP_LIVE_THRESH)
	return false;
//    point2 min,max;
    //track[index].geometry.bounds2(min,max);
    // no large moving things
    if (vec3_mag(vec3_sub(track[index].min,track[index].max)) > 
	MAP_IGNORE_DIST*1.5)
//    if (min.dist(max) > MAP_IGNORE_DIST*1.6)
    {
	track[index].seenMoving = false;
	return false;
    }

    bool moving = (track[index].vel.norm() > MAP_MIN_VEL);
   bool displaced = (track[index].firstpos.dist(track[index].pos) > MAP_MIN_DELTA);
    // change algorithm based on time seen
    double timeSeen = (DGCgettime() - track[index].firstSeen)/1000000.0;
    if (timeSeen > MAP_DYN_TIME)
    {
	// only set as moving if both conditions are satisfied
	track[index].seenMoving = track[index].seenMoving || 
	    (moving && displaced);
	// and unset as moving if we haven't actually moved
	if (!displaced)
	    track[index].seenMoving = false;
    }
    else
    {
	// send as moving even if displacement not met
	bool littleDisp = (track[index].firstpos.dist(track[index].pos) > MAP_MIN_IDELTA);
	if (moving && littleDisp)
	    track[index].seenMoving = true;
    }

    if (moving)
	track[index].lastMoving = DGCgettime();
	
    return track[index].seenMoving;
}

void LadarMap::updateTrack(LinkedSegment segs[MAX_NUM_NSEGS],int curSeg,int index,vec3_t min, vec3_t max)
{
    int cur = segs[curSeg].nextSegment;
    int numSegs = 0;
    while (true)
    {
	track[index].tracked[numSegs] = segs[cur].track;
	numSegs++;
	if (cur == curSeg || numSegs == MAP_TRACK_SEGS)
	    break;
	cur = segs[cur].nextSegment;
    }
    track[index].numSegments = numSegs;
    track[index].min = min;
    track[index].max = max;
    track[index].updated = true;
}

void LadarMap::testCreateTrack(TrackedSegment& ts, int ladarId, int trackId)
{
    ts.lastMapTrack = -1;
    if (ts.status < MAP_SEG_MINSTATUS)
	return;
    // only create a track if we really, really want to
//    if (!(ts.carConf & CAR_SIZE))
//	return;
    // no single points
    if (ts.segment[0].start == ts.segment[0].end)
	return;
    // no invalid geometries
    if (ts.segment[0].scan->blobId != ts.segment[0].blobId)
	return;
    // except for now, just create it, who gives a crap
    if (ts.segment[0].flags == FG_B || ts.status > MAP_STATUS_THRESH)
    {
	int id = addTrack(ladarId, trackId);
	track[id].pos = point2(ts.x[0],ts.y[0]);
	track[id].firstpos = track[id].pos;
	track[id].vel = point2(ts.x[1],ts.y[1]);
//	track[id].geometry = tracker[ladarId].getSegmentPoints(ts.segment[0]);
	ts.lastMapTrack = id;
    }
}

bool LadarMap::testCreateTrack(LinkedSegment segs[MAX_NUM_NSEGS],int curSeg,vec3_t min,vec3_t max)
{
    int cur = segs[curSeg].nextSegment;
    // make sure some of our segments satisfy our criteria and that
    // none violate it
    bool isValid = false;
    bool mustCreate = false;
    int count=1;
    while (true)
    {
	if (segs[cur].track->status >= MAP_SEG_MINSTATUS
	    && segs[cur].segment->start != segs[cur].segment->end)
	    isValid = true;
	if (segs[cur].segment->flags == FG_B)
	    mustCreate = true;
	if (segs[cur].track->status > MAP_STATUS_THRESH)
	    mustCreate = true;
	if (cur == curSeg)
	    break;
	cur = segs[cur].nextSegment;
	count++;
    }
//    if (count > 1)
//	isValid=true;
    if (!isValid)
	return false;
    // make the track
    if (mustCreate)
    {
	int id = addTrack(segs[curSeg].ladarId,segs[curSeg].trackId);
	updateTrack(segs,curSeg,id,min,max);
	TrackedSegment* ts = segs[curSeg].track;
	track[id].pos = point2(ts->x[0],ts->y[0]);
	track[id].firstpos = track[id].pos;
	track[id].vel = point2(ts->x[1],ts->y[1]);
//	track[id].geometry = tracker[segs[curSeg].ladarId].getSegmentPoints(ts.segment[0]);
	ts->lastMapTrack = id;
	return true;
    }
    return false;
}

int LadarMap::processNewSegments(LinkedSegment segments[MAX_NUM_NSEGS])
{
    int lastBlob[NUM_TRACK_LADARS];

    for (int i=0;i<NUM_TRACK_LADARS;i++)
	lastBlob[i] = tracker[i].getBlobId();

    int numFresh = 0;

    // go through and find all fresh segments
    for (int ladarId=0;ladarId<NUM_TRACK_LADARS;ladarId++)
    {
	for (int trackId=0;trackId<tracker[ladarId].getHighestTrack();trackId++)
	{
	    TrackedSegment& ts = tracker[ladarId].getTrack(trackId);
	    if (ts.status <= 0)
		continue;
	    // only consider latest tracks
	    if (ts.segment[0].blobId < lastBlob[ladarId])
		continue;
	    // make sure geometry exists also
	    if (ts.segment[0].blobId != ts.segment[0].scan->blobId)
	    {
		ts.lastMapTrack = -1;
		ts.status = -1;
		fprintf(stderr,"-----invalid scan-----\n");
	        continue;
	    }
	    // now add it to our list
	    if (numFresh < MAX_NUM_NSEGS)
	    {
		segments[numFresh].ladarId = ladarId;
		segments[numFresh].trackId = trackId;
		segments[numFresh].track = &tracker[ladarId].getTrack(trackId);
		segments[numFresh].segment = &(ts.segment[0]);
		// set linked list to point to itself (always makes a loop)
		segments[numFresh].prevSegment = numFresh;
		segments[numFresh].nextSegment = numFresh;
		segments[numFresh].minId = numFresh;
		segments[numFresh].assoc = false;
		numFresh++;
	    }
	}
    }

    // now do dat durn darn association
    for (int i=0;i<numFresh;i++)
    {
	for (int j=0;j<i;j++)
	{
	    // if they are already part of the same loop, continue
	    if (segments[i].minId == segments[j].minId)
		continue;
	    // if they're close, join them
	    if (closerThan(segments[i].segment,segments[j].segment,MAP_MAX_PPDIST))
	    {
		// set minimum ids of higher loop to that of lower loop
		int min = MIN(segments[i].minId,segments[j].minId);
		int cur = (segments[i].minId>segments[j].minId)?i:j;
		while (segments[cur].minId != min)
		{
		    segments[cur].minId = min;
		    cur = segments[cur].nextSegment;
		}
		// and join the two loops
		int a = segments[j].prevSegment;
		int b = segments[i].nextSegment;
		segments[i].nextSegment = j;
		segments[j].prevSegment = i;
		segments[a].nextSegment = b;
		segments[b].prevSegment = a;
	    }
	}
    }

    return numFresh;

    // tentatively, just send this shizz
    int numStatic = 0;
    for (int i=0;i<numFresh;i++)
    {
	if (segments[i].assoc)
	    continue;
	point2arr geo;
	// loop through all the tracks in this loop
	int cur = segments[i].nextSegment;
	while (true)
	{
	    point2arr pts = tracker[segments[cur].ladarId].getSegmentPoints(*(segments[cur].segment));
	    for (size_t k=0;k<pts.size();k++)
		geo.push_back(pts[k]);
	    segments[cur].assoc = true;
	    if (cur == i)
		break;
	    cur = segments[cur].nextSegment;
	}
	obj[numStatic].status = 1;
	obj[numStatic].geometry = geo;
	obj[numStatic].centroid = point2();
	numStatic++;
    }
//    for (int i=numStatic;i<MAP_MAX_STATIC;i++)
//	obj[i].status=-1;

    for (int i=0;i<numFresh;i++)
	segments[i].assoc = false;

    return numFresh;
}

double LadarMap::getLinkedScore(LinkedSegment segs[MAX_NUM_NSEGS], int curSeg, int index, vec3_t min, vec3_t max)
{
    // first, do basic bounding box check
    vec3_t bmin = vec3_max(min,track[index].min);
    vec3_t bmax = vec3_min(max,track[index].max);
    // now check if bbox is farther apart in either direction than dist
    if (bmin.x - bmax.x > MAP_MAX_PPDIST || bmin.y - bmax.y > MAP_MAX_PPDIST)
	return 0;

    // loop through dem segments
    int cur = segs[curSeg].nextSegment;
    double score = 0;
    while (true)
    {
	for (int i=0;i<track[index].numSegments;i++)
	{
	    score += getScore(track[index].tracked[i]->segment, 
			      segs[cur].segment, MAP_MAX_PPDIST);
	}
	if (cur == curSeg)
	    break;
	cur = segs[cur].nextSegment;
    }
    return score;
}

void LadarMap::updateStatic()
{
    // Threshold, dilate, and erode... ta-da
    staticMap->threshold(MAP_THRESH_VAL);
    staticMap->dilate(MAP_DE_VAL);
    staticMap->erode(MAP_DE_VAL+this->obsShrink);
    // Now, make the map into vectors n stuff
    vector<point2arr> poly = vectorizeMap();
    // and now associate
    for (size_t i=0;i<poly.size();i++)
    {
	point2 centroid = getCentroid(poly[i]);
	double mindist = 100000;
	int minindex = -1;
	for (int j=0;j<MAP_MAX_STATIC;j++)
	{
	    if (obj[j].status != 1)
		continue;
	    // find closest other centroid
	    double dist = centroid.dist(obj[j].centroid);
	    if (dist < mindist)
	    {
		mindist = dist;
		minindex = j;
	    }
	}
	if (minindex >= 0 && mindist < MAP_MAX_ASSOC)
	{
	    updateStatic(minindex,poly[i].simplify(MAP_SIMPLIFY_DIST));
	}
	else
	{
	    addStatic(poly[i].simplify(MAP_SIMPLIFY_DIST));
	}
    }
    // alright, we've either associated or added what we need to, now prune some
    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status == 1) //wasn't associated, remove
	    removeStatic(i);
	else if (obj[i].status == 2)
	    obj[i].status = 1; //prepare for next cycle
    }
    return;
    // now, quick check to make sure we aren't sending a static obs
    // along with a dynamic one
/*    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status < 1)
	    continue;
	for (int j=0;j<MAP_MAX_TRACKS;j++)
	{
	    if (track[j].status < 1 || !track[j].seenMoving)
		continue;
	    if (track[j].pos.dist(obj[i].centroid) > MAP_MAX_DYNCDIST)
		continue;
	    if (track[j].geometry.size() == 0)
		continue;
	    // close enough to do poly-poly check
	    if (track[j].geometry.get_min_dist_poly(obj[i].geometry) < MAP_MAX_DYNSDIST)
	    {
		removeStatic(i);
		break;
	    }
	}
	}*/
}

void LadarMap::sendStatic()
{
    MapElement el;
    MapElement elClear;

    el.setFrameTypeLocal();
    el.setTypeObstacle();
    el.height = 1;
    el.plotColor = MAP_COLOR_PURPLE;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    elClear.setTypeClear();

    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status == -1)
	{	    
	    // send a clear
	    elClear.setId(this->moduleId,i);
	    this->mapTalker.sendMapElement(&elClear,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&elClear,debugSubgroup);
	    obj[i].status = 0;
	}
	else if (obj[i].status == 1)
	{
	    el.setGeometry(obj[i].geometry);
	    el.setId(this->moduleId,i);
	    this->mapTalker.sendMapElement(&el,sendSubgroup);
	    if (this->sendDebug)
		this->debugTalker.sendMapElement(&el,debugSubgroup);
	    /*	    el.setId(this->moduleId+1,i);
	    el.setGeometry(obj[i].centroid);
	    el.setType(ELEMENT_POINTS);
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
	    el.setTypeObstacle();*/
	}
    }
/*    el.clear();
    el.setTypeClear();
    for (;i<lastStaticCount;i++)
    {
	el.setId(this->moduleId,i);
	this->mapTalker.sendMapElement(&el,sendSubgroup);
	if (this->sendDebug)
	    this->debugTalker.sendMapElement(&el,debugSubgroup);
    }
    lastStaticCount = staticObs.size();*/
}

void LadarMap::sendFreespace(point2arr& pts)
{
    MapElement el;

    if (pts.size() == 0)
	return;

    el.setFrameTypeLocal();
    el.setTypeFreeSpace();
    el.height = 1;
    el.plotColor = MAP_COLOR_RED;
    // set the state to something approximate, who cares anyway?
    el.setState(scans->getPastScan(0,0)->blob.state);
    el.velocity = point2(0,0);

    el.setGeometry(pts);
    el.setId(this->moduleId,MAP_MAX_TRACKS + MAP_MAX_STATIC + 100);
    this->mapTalker.sendMapElement(&el,sendSubgroup);
    if (this->sendDebug)
	this->debugTalker.sendMapElement(&el,debugSubgroup);
}

bool LadarMap::closerThan(LadarSegment *seg1, LadarSegment *seg2, double dist)
{
    // first, do basic bounding box check
    vec3_t bmin = vec3_max(seg1->min,seg2->min);
    vec3_t bmax = vec3_min(seg1->max,seg2->max);
    // now check if bbox is farther apart in either direction than dist
    if (bmin.x - bmax.x > dist || bmin.y - bmax.y > dist)
	return false;
    // get two points pointer arrays
    vec3_t *pts1 = seg1->scan->points;
    vec3_t *pts2 = seg2->scan->points;
    // find nearest pair of points, yeah i know it's a hack, oh well
    for (int i=seg1->start;i<=seg1->end;i++)
	for (int j=seg2->start;j<=seg2->end;j++)
	    if (vec3_mag(vec3_sub(pts1[i],pts2[j])) < dist)
		return true;
    // no pair of points are closer than dist
    return false;
}

double LadarMap::getScore(LadarSegment *seg1, LadarSegment *seg2, double dist)
{
    vec3_t bmin = vec3_max(seg1->min,seg2->min);
    vec3_t bmax = vec3_min(seg1->max,seg2->max);
    // now check if bbox is farther apart in either direction than dist
    if (bmin.x - bmax.x > dist || bmin.y - bmax.y > dist)
	return 0;
    // get two points pointer arrays
    vec3_t *pts1 = seg1->scan->points;
    vec3_t *pts2 = seg2->scan->points;
    // add up score based on p-p dist
    double score = 0;
    double d;
    for (int i=seg1->start;i<=seg1->end;i++)
    {
	for (int j=seg2->start;j<=seg2->end;j++)
	{
	    d = vec3_mag(vec3_sub(pts1[i],pts2[j]));	    
	    if (d < dist)
		score += (dist - d)/dist;
	}
    }
    return score;
}

double LadarMap::getScore(int index, TrackedSegment& ts)
{
    if (track[index].pos.dist(point2(ts.x[0],ts.y[0])) > MAP_ASSOC_DIST)
	return 0;
    double mindist=MAP_ASSOC_DIST;
    double d;
/*    // find nearest pair of points, yeah i know it's a hack, oh well
//    for (size_t i=0;i<track[index].geometry.size();i++)
    {
	for (int j=ts.segment[0].start;j<=ts.segment[0].end;j++)
	{
//	    d = track[index].geometry[i].dist(point2(ts.segment[0].scan->points[j].x,
//						     ts.segment[0].scan->points[j].y));
	    d = 1;
	    mindist = MIN(mindist,d);
	}
    }
    if (mindist > MAP_MAX_PPDIST)
    return 0;*/
    // return a score where higher is better
    return 1000-mindist;
}

void LadarMap::addStatic(point2arr geo)
{
    for (int i=0;i<MAP_MAX_STATIC;i++)
    {
	if (obj[i].status == 0)
	{
	    obj[i].status = 2;
	    obj[i].geometry = geo;
	    obj[i].centroid = getCentroid(geo);
	    return;
	}
    }
}

void LadarMap::removeStatic(int id)
{
    obj[id].status = -1;
}

void LadarMap::updateStatic(int id, point2arr geo)
{
    obj[id].geometry = geo;
    obj[id].centroid = getCentroid(geo);
    obj[id].status = 2;
}

int LadarMap::addTrack(int ladarId, int trackId)
{
    for (int i=0;i<MAP_MAX_TRACKS;i++)
    {
	if (track[i].status <= 0)
	{
	    track[i].status = MAP_INIT_STATUS;
//	    track[i].segment[0][0] = ladarId;
//	    track[i].segment[0][1] = trackId;
	    track[i].numSegments = 0;
	    track[i].seenMoving = false;
	    track[i].lastMoving = 0;
	    track[i].numPastPos = 0;
	    track[i].firstSeen = DGCgettime();
	    
	    return i;
	}
    }
    fprintf(stderr,"Could not craete track!");
    return 0;
}

void LadarMap::removeTrack(int index)
{
    track[index].status = -1;
}

void LadarMap::addSegment(int index, int ladarId, int trackId)
{
//    int id = track[index].numSegments;
//    track[index].segment[id][0] = ladarId;
//    track[index].segment[id][1] = trackId;
//    track[index].numSegments++;
}

void LadarMap::removeSegment(int index, int id)
{
    // want to remove a certain el from the track
//    memmove(track[index].segment[id],track[index].segment[id+1],sizeof(int)*2*(MAP_TRACK_SEGS-id-1));
//    track[index].numSegments--;
}

int qcmp(const void *a, const void *b)
{
  const metaPoint* pt1 = (metaPoint*)a;
  const metaPoint* pt2 = (metaPoint*)b;
  return (pt1->perp > pt2->perp)?1:-1;
}

point2arr LadarMap::makeGeometry(int index)
{
    LadarScan *scan = scans->getPastScan(0,0);
    if (!scan->processed)
	scan = scans->getPastScan(0,1);
    point2 alicePos = point2(scan->ladarPos.x,scan->ladarPos.y);
    point2 obsdir = track[index].pos - alicePos;
    point2 perp(obsdir.y,-obsdir.x);
/*    for (size_t i=0;i<track[index].geometry.size();i++)
	track[index].geometry[i].z = perp.dot(track[index].geometry[i]-track[index].pos);
    // points are now sorted by perpendicular direction
    qsort(&(track[index].geometry[0]),track[index].geometry.size(),sizeof(point2),&qcmp);
    point2arr geometry = track[index].geometry.simplify(MAP_SIMPLIFY_DIST);
    if (geometry.size() <= 2)
	return geometry;
    for (size_t i = geometry.size()-2; i>0; i--)
	if (perp.cross(geometry[i]-track[index].pos) > 0)
	    geometry.push_back(geometry[i]);
	    return geometry;*/
    int nSegs = track[index].numSegments;
    LadarSegment* seg[nSegs];
    vector<metaPoint> mpts;

    // make a vector of "metapoints"
    for (int i=0;i<nSegs;i++)
    {
//	int ladarId = track[index].segment[i][0];
//	TrackedSegment& ts = tracker[ladarId].getTrack(track[index].segment[i][1]);
	seg[i] = track[index].tracked[i]->segment;
	if (seg[i]->blobId != seg[i]->scan->blobId)
	    fprintf(stderr,"Oh noes you try to get scan too late!");
	for (int j=seg[i]->start;j<=seg[i]->end;j++)
	{
	    metaPoint mpt;
	    mpt.pt.x = seg[i]->scan->points[j].x;
	    mpt.pt.y = seg[i]->scan->points[j].y;
	    mpt.pt.z = seg[i]->scan->points[j].z;
	    mpt.perp = perp.dot(mpt.pt-track[index].pos);
	    mpt.seg = i;
	    // make sure it isn't a no-return
	    if (seg[i]->scan->blob.points[j][RANGE] < MAX_LADAR_DIST)
		mpts.push_back(mpt);
	}

    }
    // sort our metapoints
    qsort(&(mpts[0]),mpts.size(),sizeof(metaPoint),&qcmp);
    point2arr geometry;
    double cross1,cross2;
    line2 dline;
    point2 dpt;
    point2 dpoint, cpt;
    for (size_t i=0;i<mpts.size();i++)
    {
	metaPoint cur = mpts[i];
	// add current point
	geometry.push_back(cur.pt);
	// find next point belonging to same scan
	int nextPt = -1;
	for (size_t j=i+1;j<mpts.size();j++)
	{
	    if (mpts[j].seg == cur.seg)
	    {
		nextPt = j;
		break;
	    }
	}
	// no point found ==> go to next point
	if (nextPt == -1)
	    continue;
	if (nextPt == i+1)
	    continue;
	dpt = (mpts[nextPt].pt - cur.pt)*100;
	// get delta line
	line2 dline = line2(cur.pt-dpt,mpts[nextPt].pt+dpt);
	// see how many we can skip
	int j;
	for (j=i+1;j<nextPt;j++)
	{
	    // if we have a closer point n stuff
	    if (!dline.is_intersect(alicePos,mpts[j].pt))
		break;
	}
	// either we stopped at a point j or we just want to go to nextPt
	// yes, i know messing with loop vars is evil.
	i = j-1;
    }

    if (geometry.size() > 5)
	geometry = geometry.simplify(MAP_SIMPLIFY_DIST);

    size_t s = geometry.size();
    point2 delta = (track[index].pos-alicePos);
    delta.normalize();
    delta = delta * MAP_PROJ_DIST;
    // now add back-face points
    for (int i=s-1;i>=0;i--)
    {
	point2 pt = geometry[i] + delta;
	geometry.push_back(pt);
    }

    return geometry;
}

inline point2 LadarMap::getCentroid(point2arr& ptarr)
{
    // find centroid by center-of-mass of a closed poly
    double A=0;
    for (size_t i=0;i<ptarr.size()-1;i++)
	A += (ptarr[i].x*ptarr[i+1].y - ptarr[i+1].x*ptarr[i].y);
    A += ptarr.back().x*ptarr[0].y - ptarr[0].x*ptarr.back().y;
    A *= 0.5;
    point2 c = point2(0,0);
    for (size_t i=0;i<ptarr.size()-1;i++)
    {
	c.x += (ptarr[i].x+ptarr[i+1].x)*(ptarr[i].x*ptarr[i+1].y-ptarr[i+1].x*ptarr[i].y);
	c.y += (ptarr[i].y+ptarr[i+1].y)*(ptarr[i].x*ptarr[i+1].y-ptarr[i+1].x*ptarr[i].y);
    }
    c.x += (ptarr.back().x+ptarr[0].x)*(ptarr.back().x*ptarr[0].y-ptarr[0].x*ptarr.back().y);
    c.y += (ptarr.back().y+ptarr[0].y)*(ptarr.back().x*ptarr[0].y-ptarr[0].x*ptarr.back().y);
    c.x *= (1.0/(6*A));
    c.y *= (1.0/(6*A));
    return c;
}

point2arr LadarMap::getFreespace()
{
    int width = staticMap->getWidth();
    emap_t *pix = staticMap->getData();
    emap_t *curPix;
    int curDir;
    int lastDir;
    point2arr freespace;
    point2 alicepos;

    int n = 0;
    
    alicepos = makePoint(width/2 + width*width/2);

    // set edges of map to 0 to avoid edge issues
    for (int i=0;i<width;i++)
    {
	*(pix+i) = 0;
	*(pix+i*width) = 0;
	*(pix+i*width+width-1) = 0;
	*(pix+width*width-i-1) = 0;
    }

    //start searching at alice's location (center of map)
    pix = staticMap->getData() + width/2 + (width/2)*width;

    // loop until edge of freespace poly reached
    for (int i=0;i<width/2-1;i++)
    {
	// found the edge of polygon, start looping through
	if (*pix == 0xFFFF && *(pix+1) == 0x0)
	{
	    // get starting index
	    int ind = (pix-staticMap->getData());
	    freespace.clear();
	    freespace.push_back(makePoint(ind));
	    n = 1;
	    curPix = pix;
	    *pix = 1; // set start location
	    curDir = moveDir(curPix,1,width);
	    lastDir = 0;
	    while (*curPix > 1)
	    {
		ind += curDir;
		if (curDir != lastDir)
		{
		    freespace.push_back(makePoint(ind));
		    n++;
		    // sanity check
		    if (n > 50000)
		    {
			fprintf(stderr,"Freespace poly too big!\n");
			freespace.clear();
			return freespace;
		    }
		}
		// mark that we have been here
		(*curPix)--;
		lastDir = curDir;
		curDir = moveDir(curPix,lastDir,width);
	    }
	    // forget it if we don't know
	    if (freespace.size() < 3)
	    {
		pix++;
		continue;
	    }
	    // make sure our starting point is inside of the poly
	    if (!freespace.is_inside_poly(alicepos))
	    {
		pix++;
		continue;
	    }

	    // we found a poly containing alice's location
	    return freespace.simplify(MAP_SIMPLIFY_DIST*5);
	}
	pix++;
    }

    // no poly found (this should not happen?)
    freespace.clear();
    return freespace;
}

vector<point2arr> LadarMap::vectorizeMap()
{
    int width = staticMap->getWidth();
    emap_t *pix = staticMap->getData();
    emap_t *end = pix + width*width;
    emap_t *curPix;
    int curDir;
    int lastDir;
    vector<point2arr> polys;

    // set edges of map to 0 to avoid edge issues
    for (int i=0;i<width;i++)
    {
	*(pix+i) = 0;
	*(pix+i*width) = 0;
	*(pix+i*width+width-1) = 0;
	*(pix+width*width-i-1) = 0;
    }

    // loop through map
    while (pix < end)
    {
	if (*pix == 0)
	{
	    pix++;
	    continue;
	}
	// found a polygon, start looping through
	else if (*pix == 0xFFFF && *(pix-1) == 0) // new polygon, == 0 to avoid doing donuts
	{
	    // get starting index
	    int ind = (pix-staticMap->getData());
	    point2arr ptarr;
	    ptarr.push_back(makePoint(ind));
	    curPix = pix;
	    *pix = 1; // set start location
	    curDir = moveDir(curPix,1,width);
	    lastDir = 0;
	    // eliminate wrong dir polys
	    if (curDir < 0)
	    {
		*curPix = 0; // skip loop
		ptarr.clear(); // don't add
	    }
	    while (*curPix > 1)
	    {
		ind += curDir;
		if (curDir != lastDir)
		    ptarr.push_back(makePoint(ind));
		// mark that we have been here
		(*curPix)--;
		lastDir = curDir;
		curDir = moveDir(curPix,lastDir,width);
	    }
	    if (ptarr.size() > 1)
	    {
		polys.push_back(ptarr);
	    }
	    else if (ptarr.size() == 1)
	    {
		// give it a tiny bit of size, just in case
		ptarr.push_back(ptarr[0] + point2(0.1,0));
		ptarr.push_back(ptarr[0] + point2(0,0.1));
		polys.push_back(ptarr);
	    }

	    // now move to end of poly
	    while (*pix != 0)
		pix++;
	}
	else
	{
	    while (*pix != 0) // have already done this poly
		pix++;
	}
    }

    return polys;
}

inline point2 LadarMap::makePoint(int ind)
{
    int x = (ind & (EMAP_SIZE-1));
    int y = (ind >> EMAP_BITS);
    return point2(x*map2local[0][0]+map2local[0][3],y*map2local[1][1]+map2local[1][3]);
}

inline int LadarMap::moveDir(emap_t* &pix, int lastDir, int width)
{
    emap_t *lastPix=pix;
    switch (lastDir)
    {
    case -EMAP_SIZE:
	pix += width - 1;
	lastPix = pix + 1;
	goto bl;
    case -EMAP_SIZE+1:
	pix--;
	lastPix = pix + width;
	goto lt;
    case 1:
	pix -= (width+1);
	lastPix = pix + width;
	goto ul;
    case EMAP_SIZE+1:
	pix -= width;
	lastPix = pix - 1;
	goto up;
    case EMAP_SIZE:
	pix -= (width-1);
	lastPix = pix - 1;
	goto ur;
    case EMAP_SIZE-1:
	pix ++;
	lastPix = pix - width;
	goto rt;
    case -1:
	pix += width + 1;
	lastPix = pix - width;
	goto br;
    case -EMAP_SIZE-1:
	pix += width;
	lastPix = pix + 1;
	goto bt;	
    }
up:
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;
ur:
    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;
rt:
    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;
br:
    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;
bt:
    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;
bl:
    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;
lt:
    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;
ul:
    if (*lastPix == 0 && *pix > 0)
	return -width-1;
    lastPix = pix;
    pix++;
// now for wrap-around calls
    if (*lastPix == 0 && *pix > 0)
	return -width;
    lastPix = pix;
    pix++;

    if (*lastPix == 0 && *pix > 0)
	return -width+1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return 1;
    lastPix = pix;
    pix += width;

    if (*lastPix == 0 && *pix > 0)
	return width+1;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width;
    lastPix = pix;
    pix--;

    if (*lastPix == 0 && *pix > 0)
	return width-1;
    lastPix = pix;
    pix -= width;

    if (*lastPix == 0 && *pix > 0)
	return -1;
    lastPix = pix;
    pix -= width;

    return 0;
}

