#ifdef __cplusplus
extern "C" {
#endif
void fit(float x[], float y[], int ndata, float *a, float *b, float *siga, float *sigb, float *chi2);
#ifdef __cplusplus
}
#endif
