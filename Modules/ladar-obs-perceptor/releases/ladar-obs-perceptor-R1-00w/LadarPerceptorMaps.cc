#include "LadarPerceptor.hh"
#include "emapViewer.hh"

void LadarPerceptor::initMaps()
{
    this->rmap = new RoadMap();
    this->smap = new StaticMap(this->scans);
    mapErrorX = 0;
    mapErrorY = 0;
    mapOffsetZ = 0;
}

void LadarPerceptor::finiMaps()
{
    delete this->rmap;
    delete this->smap;
}

void* LadarPerceptor::mapUpdateLoop(void *self)
{
    LadarPerceptor *me = (LadarPerceptor*)self;
    while (!me->quit)
    {
	me->ladarMap->update();
    }
    return NULL;
}

void LadarPerceptor::processScan(LadarScan *scan)
{
    timer.startTime(10);
    // transform scan and compute groundstrike probs
    pproc->ProcessScan(scan,this->local2map);
    scan->processed = true;

    if (scan->ladarId < 4)
	timer.printTime("Processing time",10);
    else if (scan->ladarId < 6)
	timer.printTime("Roof processing time",10);

    // update map position if we need to
    if (scan->blob.state.timestamp > this->state.timestamp)
	shiftMaps(scan->blob.state);
    updateMaps();

    timer.startTime();

    switch (scan->blob.sensorId)
    {
    case SENSNET_PTU_LADAR:
    case SENSNET_RIEGL:
	rmap->drawScan(scan, scans);
	timer.printTime("RoadMap time");
	break;

    case SENSNET_MF_BUMPER_LADAR:
    case SENSNET_LF_BUMPER_LADAR:
    case SENSNET_RF_BUMPER_LADAR:
    case SENSNET_REAR_BUMPER_LADAR:
    case SENSNET_LF_ROOF_LADAR:
    case SENSNET_RF_ROOF_LADAR:
	smap->drawScan(scan);
	timer.printTime("StaticMap time");
	break;
    }
}

void LadarPerceptor::updateMaps()
{
    if ((DGCgettime() - lastRoadUpdate) > ROAD_UPDATE_TIME)
    {
	pproc->UpdateRoadMap(rmap->getGSMap(),rmap->getLocal2Map());	
	lastRoadUpdate = DGCgettime();
    }

    if ((DGCgettime() - lastStaticDec) > STATIC_DEC_TIME)
    {
	smap->timeDec();
	lastStaticDec = DGCgettime();
    }
}

void LadarPerceptor::shiftMaps(VehicleState &state)
{
    if (this->state.timestamp == 0)
    {
	this->state = state;
	this->mapOffsetZ = state.localZ;
	rmap->setOffsetZ(state.localZ);
    }

    mapErrorX += (state.localX - this->state.localX);
    mapErrorY += (state.localY - this->state.localY);

    int dx = -(int)(mapErrorX / EMAP_RES);
    int dy = -(int)(mapErrorY / EMAP_RES);

    mapErrorX += dx*EMAP_RES;
    mapErrorY += dy*EMAP_RES;

    float m[4][4];
    memset(m,0,sizeof(float)*16);
    float scl = (float)(1.0/EMAP_RES);
    m[0][0] = m[1][1] = scl;
    m[2][2] = m[3][3] = 1;
    m[0][3] = -(state.localX-mapErrorX)/EMAP_RES + EMAP_SIZE/2;
    m[1][3] = -(state.localY-mapErrorY)/EMAP_RES + EMAP_SIZE/2;

    memcpy(this->local2map,m,sizeof(float)*16);
    
    // shift each map
    this->rmap->shiftMap(dx,dy,m);
    this->smap->shiftMap(dx,dy,m);

    // add in a nice little jumping correction here, dzdt in m/s
    double dzdt = fabs(this->state.localZ-state.localZ);
    if (dzdt > 0.1) //magic
    {
	this->mapOffsetZ += (state.localZ - this->state.localZ);
	this->anomalyCount++;
	this->rmap->setOffsetZ(mapOffsetZ);
	MSG("anomaly count: %d", this->anomalyCount);
    }

    this->state = state;
}

void LadarPerceptor::displayMaps()
{
    setEMap(0,rmap->getPTUMap());
    setEMap(3,rmap->getGSMap());
    setEMap(1,smap->getMap());
    setEMap(2,smap->getTestMap());
    setYaw(this->state.localYaw);
//    smap->getTestMap()->clear(0x8000);

/*    int w1 = EMAP_SIZE;
    int w2 = ROOFMAP_SIZE;
    emap_t* src = pproc->getRoofMap()->getData();
    emap_t* dst = smap->getTestMap()->getData();
    for (int x=0;x<w2;x++)
	for (int y=0;y<w2;y++)
	    dst[y*w1+x] = src[y*w2+x];
	   setEMap(2,smap->getTestMap());*/
}
