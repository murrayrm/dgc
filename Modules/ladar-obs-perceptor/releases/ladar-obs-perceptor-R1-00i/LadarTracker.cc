#include "LadarMap.hh"
#include "emapViewer.hh"

void LadarTracker::ProcessScan(LadarScan *scan)
{
    if (scan->blobId <= lastProcId)
	return;
    lastProcId = scan->blobId;

    vector<LadarSegment> segments = segmentScan(scan);
    associateScan(scan, segments);
    pruneMap();
    updateTrackList();
}

// Segments a scan based on range and groundstrike discontinuity
vector<LadarSegment> LadarTracker::segmentScan(LadarScan *scan)
{
    vector<LadarSegment> segments;
    int newSegment[LADAR_BLOB_MAX_POINTS];
    double gsConf[LADAR_BLOB_MAX_POINTS];

    LadarSegment segment;

    segment.scan = scan;
    segment.blobId = scan->blobId;
    segment.start = 0;
    segment.end = 0;
    segment.flags = 0;

    double pr, segdist;

    newSegment[0] = 2;
    gsConf[0] = 0.5;

    // find all the breaks between segments
    for (int i=1; i<scan->numPoints; i++)
    {
	pr = MIN(scan->blob.points[i][RANGE],scan->blob.points[i-1][RANGE]);
	segdist = MAP_SEG_MIN + MAP_SEG_COEFF*pr;
	// there is a break in the scan, mark new segment
	if (vec3_mag(vec3_sub(scan->points[i-1],scan->points[i])) > segdist)
	    newSegment[i] = 2;
	else
	    newSegment[i] = 0;
	// groundstrike check, segment if gs discontinuity
	gsConf[i] = scan->gsRoofConf[i]*scan->gsRoadConf[i];
/*	if (fabs(gsConf[i]-gsConf[i-1]) > GS_GAP)
	newSegment[i]++;*/
    }

    segment.left = segment.center = segment.min = segment.max = scan->points[0];
    double avgGs, maxGs;
    bool useSegment = true;
    int n;

    // compute segment properties and add to our list
    for (int i=1; i<scan->numPoints+1; i++)
    {
	if (newSegment[i]>0)
	{
	    segment.end = i-1;
	    n = (segment.end-segment.start+1);
	    segment.right = scan->points[i-1];
	    segment.center = vec3_scale((1.0/n),segment.center);	    
	    // only set foreground-ness if not groundstrike break
	    if (i < scan->numPoints)
		if (newSegment[i] > 1 && (scan->blob.points[i][RANGE] > scan->blob.points[i-1][RANGE]))
		    segment.flags |= FG_R;
	    avgGs /= n;
	    // throw away if we're sure it's a groundstrike
    
	    if (avgGs > GS_UPPER_THRESH)
		useSegment = false;
	    // otherwise use maximum gs value in segment to decide
	    else if (avgGs > GS_LOWER_THRESH && maxGs > GS_UPPER_THRESH)
		useSegment = false;

	    if (segment.flags == FG_N)
		useSegment = false;
	    
	    // add segment to list
	    if (useSegment)
		segments.push_back(segment);

	    if (i == scan->numPoints)
		break;
	    
	    // set new values
	    segment.left = segment.center = segment.min = segment.max = scan->points[i];
	    segment.start = i;
	    segment.flags = 0;
	    avgGs = maxGs = gsConf[i];
	    useSegment = true;
	    // throw away no-returns
	    if (scan->blob.points[i][RANGE] > MAX_LADAR_DIST)
		useSegment = false;

	    if (newSegment[i] > 1 && (scan->blob.points[i-1][RANGE] > scan->blob.points[i][RANGE]))
		segment.flags |= FG_L;
	}
	else
	{
	    segment.center = vec3_add(segment.center,scan->points[i]);
	    segment.min = vec3_min(segment.min,scan->points[i]);
	    segment.max = vec3_max(segment.max,scan->points[i]);
	    avgGs += gsConf[i];

	    // keep track of max groundstrike value
	    maxGs = MAX(gsConf[i],maxGs);
	}
    }

    return segments;
}

void LadarTracker::associateScan(LadarScan *scan, vector<LadarSegment> &segments)
{
    size_t i;
    double score, maxScore;
    int maxIndex;

    LadarScan *prevScan = scans->getPastScan(scan->ladarId,1);

    // first, try associating with previous associations
    if ((scan->blobId - prevScan->blobId) < 75)
    {
	int lastChecked = -1;

	for (i=0;i<segments.size();i++)
	{
	    maxScore = 0;
	    maxIndex = -1;
	    for (int j=segments[i].start;j<=segments[i].end;j++)
	    {
		if (prevScan->lastElementId[j] >= 0 && prevScan->lastElementId[j] != lastChecked)
		{
		    lastChecked = prevScan->lastElementId[j];
		    score = scoreSegment(segments[i], lastChecked);
		    if (score > maxScore)
		    {
			maxScore = score;
			maxIndex = lastChecked;
		    }
		}
	    }
	    // found a good association, so use it
	    if (maxIndex != -1)
	    {
		addToTrack(segments[i],maxIndex);
		segments[i].blobId = 0;
	    }
	}
    }

    // now go through and do a deep association if we didn't get some
    for (i=0;i<segments.size();i++)
    {
	// already associated?
	if (segments[i].blobId == 0)
	    continue;
	maxScore = 0;
	maxIndex = -1;
	// loop through all tracks and associate
	for (int j=0;j<highestTrack;j++)
	{
	    score = scoreSegment(segments[i], j);
	    if (score > maxScore)
	    {
		maxScore = score;
		maxIndex = j;
	    }
	}
	// found a good association, so use it
	if (maxIndex != -1)
	{
	    addToTrack(segments[i],maxIndex);
	    segments[i].blobId = 0;
	}
    }

    // and create new tracks for unassociated non-background segments
    // (this code should probably be improved some)
    for (i=0;i<segments.size();i++)
    {
	if (segments[i].blobId != 0 && segments[i].flags != FG_N)
	{
	    createTrack(segments[i]);
	}
    }
}

void LadarTracker::pruneMap()
{
    for (int i=0;i<highestTrack;i++)
    {
	if (track[i].status == 0)
	    continue;
	track[i].status--;
	// check if its geometry still in storage
	if (track[i].segment[0].blobId != track[i].segment[0].scan->blobId)
	    deleteTrack(i);
	else if (track[i].status <= 0)
	    deleteTrack(i);
    }
}

inline double LadarTracker::scoreSegment(LadarSegment& segment, int index)
{
    // have we already associated with this track?
    if (segment.blobId == track[index].segment[0].blobId)
	return 0;
    if (track[index].status == 0)
	return 0;
    
    double distl,distr,distc,dist;
    distl = distr = distc = 2*TRACK_MAX_ASSOC;
    // find which corners they have in common
    int corners = (segment.flags & track[index].segment[0].flags);
    switch (corners)
    {
    case FG_B: //both corners
	distc = vec3_mag(vec3_sub(segment.center,track[index].segment[0].center));
	distl = vec3_mag(vec3_sub(segment.left,track[index].segment[0].left));
	distr = vec3_mag(vec3_sub(segment.right,track[index].segment[0].right));
	break;
    case FG_L: //left corner only
	distl = vec3_mag(vec3_sub(segment.left,track[index].segment[0].left));
	break;
    case FG_R: //right corner only
	distr = vec3_mag(vec3_sub(segment.right,track[index].segment[0].right));
	break;
    case FG_N: //no corners in common, return 0
	return 0;
    }
    
    // get the best association from any available corners
    dist = MIN(MIN(distr,distl),distc);
    if (dist > TRACK_MAX_ASSOC)
	return 0;
    // for now, just return linear score based on distance
    return (1.0-(dist/TRACK_MAX_ASSOC));
}

void LadarTracker::addToTrack(LadarSegment& segment, int index)
{
    if (track[index].status == 0)
    {
	fprintf(stderr,"Tried to add to dead track %d!\n",index);
	return;
    }
    if (track[index].segment[0].blobId == segment.blobId)
    {
	fprintf(stderr,"Tried to add to track %d twice!\n",index);
	return;
    }
    track[index].numAssoc++;
    track[index].status += 2; // 2 because we minus one each time
    if (track[index].status > TRACK_MAX_STATUS)
	track[index].status = TRACK_MAX_STATUS;
    // move old segments down one
    memmove(track[index].segment+1,track[index].segment,
	    sizeof(LadarSegment)*(NUM_PAST_SEGS-1));
    // add new segment
    track[index].segment[0] = segment;
    // update scan's lastElementId stuff
    for (int i=segment.start;i<=segment.end;i++)
	segment.scan->lastElementId[i] = index;
    // tracking update stuff would go here
    double dt = LADAR_DT * (segment.blobId - track[index].segment[1].blobId);
    int corners = (segment.flags & track[index].segment[1].flags);
    vec3_t vel;
    switch (corners)
    {
    case FG_B:
	vel = vec3_sub(segment.center,track[index].segment[1].center);
	break;
    case FG_L:
	vel = vec3_sub(segment.left,track[index].segment[1].left);
	break;
    case FG_R:
	vel = vec3_sub(segment.right,track[index].segment[1].right);
	break;
    case FG_N:
	fprintf(stderr,"Associated track %d with no corners in common!\n",index);
	return;
    }
    vel = vec3_scale(1.0/dt,vel);
    // limit vel to be reasonable
    vel.x = MIN(vel.x,TRACK_MAX_VEL);
    vel.y = MIN(vel.y,TRACK_MAX_VEL);
    vel.x = MAX(vel.x,-TRACK_MAX_VEL);
    vel.y = MAX(vel.y,-TRACK_MAX_VEL);

    track[index].velocity.x = track[index].velocity.x*(1-TRACK_VEL_WEIGHT) + vel.x*TRACK_VEL_WEIGHT;
    track[index].velocity.y = track[index].velocity.y*(1-TRACK_VEL_WEIGHT) + vel.y*TRACK_VEL_WEIGHT;
}

void LadarTracker::createTrack(LadarSegment& segment)
{
    if (emptyTrack == -1)
    {
	fprintf(stderr,"Tried to create too many tracks!\n");
	return;
    }
    if (track[emptyTrack].status != 0)
    {
	fprintf(stderr,"Tried to overwrite existing track %d\n",emptyTrack);
	return;
    }

    track[emptyTrack].status = TRACK_INIT_STATUS;
    track[emptyTrack].numAssoc = 1;
    track[emptyTrack].segment[0] = segment;
    track[emptyTrack].velocity.x = track[emptyTrack].velocity.y = 0;
    if (emptyTrack > highestTrack)
	highestTrack = emptyTrack;
    emptyTrack = track[emptyTrack].nextEmptyId;
    numTracks++;
}

void LadarTracker::deleteTrack(int index)
{
    track[index].status = 0;
    numTracks--;
}

void LadarTracker::updateTrackList()
{
    int lastEmpty = 0;
    emptyTrack = -1;
    // update list of empty tracks
    for (int i=0;i<MAX_TRACKS;i++)
    {
	if (track[i].status == 0)
	{
	    // set this->emptyTrack to first empty track
	    if (emptyTrack < 0)
		emptyTrack = i;
	    track[lastEmpty].nextEmptyId = i;
	    lastEmpty = i;
	}
	else
	{
	    // set highest active track
	    highestTrack = i;
	}
    }
    // set last empty track thing
    track[MAX_TRACKS-1].nextEmptyId = -1;
}

void LadarTracker::clearTracks()
{
    memset(this->track,0,sizeof(TrackedSegment)*MAX_TRACKS);
}

void LadarTracker::sendDebug(CMapElementTalker& talker, int group)
{
    MapElement el;
    MapElement elVel;
    
    el.setFrameTypeLocal();
    el.setState(scans->getPastScan(ladarId,0)->blob.state);

    elVel.setFrameTypeLocal();
    elVel.plotColor = MAP_COLOR_RED;
    
    // display points n stuff for temps
    for (int i=0;i<highestTrack;i++)
    {
	el.setId(this->ladarId,i);
	elVel.setId(this->ladarId,i+MAX_TRACKS);
	if (track[i].status <= 0)
	{
	    el.setTypeClear();
	    elVel.setTypeClear();
	    talker.sendMapElement(&el,group);
	    talker.sendMapElement(&elVel,group);
	    continue;
	}
	el.setTypeLine();
	elVel.setTypeLine();

	if (track[i].status < 30)
	    el.plotColor = MAP_COLOR_YELLOW;
	else
	    el.plotColor = MAP_COLOR_GREEN;

	point2arr ptarr = getSegmentPoints(track[i].segment[0]);
	el.setGeometry(ptarr);

	point2 pt;
	switch(track[i].segment[0].flags)
	{
	case FG_B:
	    pt = point2(track[i].segment[0].center.x,track[i].segment[0].center.y);
	    break;
	case FG_R:
	    pt = point2(track[i].segment[0].right.x,track[i].segment[0].right.y);
	    break;
	case FG_L:
	    pt = point2(track[i].segment[0].left.x,track[i].segment[0].left.y);
	    break;
	}
	ptarr.clear();
	ptarr.push_back(pt);
	ptarr.push_back(pt + point2(track[i].velocity.x,track[i].velocity.y));
	elVel.setGeometry(ptarr);
	talker.sendMapElement(&el,group);
	talker.sendMapElement(&elVel,group);
    }
}

point2arr LadarTracker::getSegmentPoints(LadarSegment& segment)
{
    point2arr ptarr;
    for (int i=segment.start;i<=segment.end;i++)
	ptarr.push_back(point2(segment.scan->points[i].x,segment.scan->points[i].y));
    return ptarr;
}
