#include "StaticMap.hh"


StaticMap::StaticMap()
{
    memset(this,0,sizeof(*this));
    this->map = new EMap(EMAP_SIZE);
    this->map->clear(SMAP_CLEAR);
    this->testmap = new EMap(EMAP_SIZE);
    this->testmap->clear(0x0);
}

StaticMap::~StaticMap()
{
    delete this->map;
    delete this->testmap;
}

void StaticMap::shiftMap(int dx, int dy, float l2m[4][4])
{
    // set our transform matrices
    memcpy(local2map,l2m,16*sizeof(float));
    mat44f_inv(map2local,local2map);
    // and shift/clear the map
    this->map->shiftClear(dx,dy,SMAP_CLEAR);
    this->testmap->shiftClear(dx,dy,0x0);
}

void StaticMap::drawScan(LadarScan *scan)
{
    float sx, sy, sz;
    float mat[4][4];
    Point2 ladarpoints[LADAR_BLOB_MAX_POINTS*2 + 1];
    Point2 *curPoint = ladarpoints;
    LadarRangeBlob *blob = &(scan->blob);

    mat44f_mul(mat, blob->veh2loc, blob->sens2veh);
    mat44f_mul(mat, local2map, mat);

    LadarRangeBlobScanToSensor(blob, 0.0, 0.0, &sx, &sy, &sz);

    curPoint->x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
    curPoint->y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
    curPoint++;

    // erase last ladar scan
//    if (scan->ladarId < 6)
//	testmap->andimg(~((emap_t)(1<<scan->ladarId)));

    float pr;

    for (int i=0; i<blob->numPoints; i++)
    {
	// Limit very large range values
	if (blob->points[i][RANGE] > SMAP_MAX_DIST)
	    pr = SMAP_NORETURN;
	else
	    pr = blob->points[i][RANGE];

	LadarRangeBlobScanToSensor(blob, blob->points[i][ANGLE]-SMAP_DA,pr-SMAP_DR, &sx, &sy, &sz);
	curPoint->x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
	curPoint->y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
	curPoint++;

	LadarRangeBlobScanToSensor(blob, blob->points[i][ANGLE]+SMAP_DA,pr-SMAP_DR, &sx, &sy, &sz);
	curPoint->x = mat[0][0]*sx + mat[0][1]*sy + mat[0][3];
	curPoint->y = mat[1][0]*sx + mat[1][1]*sy + mat[1][3];
	curPoint++;

	// no-return
	if (pr == SMAP_NORETURN)
	    continue;
	if (pr < SMAP_MIN_DIST)
	    continue;
	
	LadarRangeBlobScanToSensor(blob, blob->points[i][ANGLE], pr, &sx, &sy, &sz);

	int tx = (int)(mat[0][0]*sx + mat[0][1]*sy + mat[0][3]);
	int ty = (int)(mat[1][0]*sx + mat[1][1]*sy + mat[1][3]);

	// not no-return, so draw in point itself
	this->map->incRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,SMAP_INC);
//	val = 65535 - MIN(val,65535);
	if (scan->ladarId < 4)
	{
	    int val = (int)(scan->gsRoadConf[i] * scan->gsRoofConf[i] * 60000);
//	    int val = (int)(scan->gsDynConf[i] * 60000);
	    // draw new scan
	    emap_t eval = (1<<scan->ladarId);
//	    this->testmap->orRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,eval);
	    this->testmap->drawRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,(emap_t)val);
	}
//	this->map->incRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,0xFFFF);
	// hack to see lampposts/small obs from farther away
	/*if (i > 0 && i < blob->numPoints-1)
	{
	    float pr = blob->points[i][RANGE];
	    if ((blob->points[i-1][RANGE] - pr)>SMAP_FG_DIST &&
		(blob->points[i+1][RANGE] - pr)>SMAP_FG_DIST)
		this->map->incRectangle(tx-SMAP_DW,ty-SMAP_DW,tx+SMAP_DW,ty+SMAP_DW,SMAP_INC);
		}*/
    }

    this->map->decPolygon(blob->numPoints*2+1, ladarpoints, SMAP_DEC);
}

// clear based on constant time; fades to "no info" (fade out old objects)
void StaticMap::timeDec()
{
    this->map->moveToCenter(SMAP_CLEAR,SMAP_TIME_DEC);
}
