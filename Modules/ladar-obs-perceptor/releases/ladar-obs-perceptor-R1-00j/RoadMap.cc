#include "RoadMap.hh"

RoadMap::RoadMap()
{
    memset(this,0,sizeof(*this));
    this->map = new EMap(EMAP_SIZE);
    this->map->clear(RMAP_CLEAR);
    for (int k=0;k<GROUND_LADARS;k++)
	for (int i=0; i<LADAR_BLOB_MAX_POINTS;i++)
	    lastScan[k][i][0] = RMAP_NOTROAD;
}

RoadMap::~RoadMap()
{
    delete this->map;
}

void RoadMap::shiftMap(int dx, int dy, float l2m[4][4])
{
    // set our transform matrices
    memcpy(local2map,l2m,16*sizeof(float));
    // adjust for height, make negative so plus is up
    local2map[2][2] = -1.0f/RMAP_VRES;
    local2map[2][3] = (mapOffsetZ/RMAP_VRES) + RMAP_VCENTER;
    mat44f_inv(map2local,local2map);
    // and shift/clear the map
    this->map->shiftClear(dx,dy,RMAP_CLEAR);
    shiftX += dx;
    shiftY += dy;
}

//void RoadMap::drawScan(LadarScan *scan)
void RoadMap::drawScan(LadarScan *scan, LadarScan *scan1, LadarScan *scan2)
{
    float mat[4][4];

    int iscan[LADAR_BLOB_MAX_POINTS][2];
    uint8_t road[LADAR_BLOB_MAX_POINTS];
    emap_t height[LADAR_BLOB_MAX_POINTS];

    int index = 0;
    int curStart, curEnd;

    bool wasRoad;
    bool isRoad;

    int MIN_SEG_POINTS=0;
    double MAX_ROAD_GAP=0;
    int GAP_WIDTH=0;

    LadarRangeBlob *blob = &(scan->blob);
    vec3_t* pos = scan->points;

    if (blob->sensorId == SENSNET_PTU_LADAR)
    {
	index = 0;
	MIN_SEG_POINTS = MIN_SEG_POINTS_PTU;
	MAX_ROAD_GAP = MAX_ROAD_GAP_PTU;
	GAP_WIDTH = GAP_WIDTH_PTU;
    }
    else if (blob->sensorId == SENSNET_RIEGL)
    {
	index = 1;
	MIN_SEG_POINTS = MIN_SEG_POINTS_RIEGL;
	MAX_ROAD_GAP = MAX_ROAD_GAP_RIEGL;
	GAP_WIDTH = GAP_WIDTH_RIEGL;
    }
    else
    {
	fprintf(stderr,"invalid ladar ID for roadmap\n");
	return;
    }

    // zero stuff
    memset(road,0,LADAR_BLOB_MAX_POINTS);

    // set up local coordinate conversion matrix
    mat44f_mul(mat, blob->veh2loc, blob->sens2veh);

    // convert points
    for (int i=0;i<blob->numPoints;i++)
    {
	// convert to map frame
	iscan[i][0] = (int)(local2map[0][0]*pos[i].x+local2map[0][3]);
	iscan[i][1] = (int)(local2map[1][1]*pos[i].y+local2map[1][3]);
	height[i] = (emap_t)(local2map[2][2]*pos[i].z+local2map[2][3]);

	// update last scan
	lastScan[index][i][0] += shiftX;
	lastScan[index][i][1] += shiftY;
    }

    // segment based on lines connecting endpoints GAP_WIDTH apart
    // and point-line distance along the way
    wasRoad = false;
    for (int i=GAP_WIDTH;i<blob->numPoints;i++)
    {
	vec3_t r = vec3_sub(pos[i],pos[i-GAP_WIDTH]);
	double rr = vec3_mag(r);
	double d=0;
	double dd=0;
	isRoad = true;

	for (int j=i-GAP_WIDTH+1;j<i;j++)
	{
	    dd = vec3_mag(vec3_cross(r,vec3_sub(pos[i-GAP_WIDTH],pos[j])))/rr;
	    d += dd*dd;
	}

	d = d/(GAP_WIDTH-1);
	isRoad = (d < MAX_ROAD_GAP);
	if (fabs(r.z/sqrt(r.x*r.x + r.y*r.y)) > MAX_ROAD_SLOPE)
	    isRoad = false;
	if (isRoad)
	{
	    if (wasRoad) //increment endpoint
	    {
		curEnd = i;
	    }
	    else //just started road, set start and end
	    {
		curStart = i-GAP_WIDTH;
		curEnd = i;
	    }
	}
	else if (wasRoad) //road just ended
	{
	    // set relevant road areas to 1
	    if (curEnd-curStart >= MIN_SEG_POINTS)
		memset(road+curStart,1,(curEnd-curStart+1));
	}
	wasRoad = isRoad;
    }
    // don't forget about last segment
    if (isRoad)
	memset(road+curStart,1,(curEnd-curStart+1));

    // now, draw segments
    for (int i=0;i<blob->numPoints-1;i++)
    {
	if (road[i] == 0)
	{
	    this->map->drawminRectangle(iscan[i][0]-GP_SIZE,iscan[i][1]-GP_SIZE,
					iscan[i][0]+GP_SIZE,iscan[i][1]+GP_SIZE, RMAP_NOTROAD);
	    iscan[i][0] = RMAP_INVALID;
	    continue;
	}
	bool useTriangles = (lastScan[index][i][0] != RMAP_INVALID);
	useTriangles = useTriangles && (lastScan[index][i+1][0] != RMAP_INVALID);
	useTriangles = useTriangles && (road[i+1] != 0);
	if (useTriangles)
	{
	    int minx = MIN(MIN(iscan[i][0],iscan[i+1][0]),
			   MIN(lastScan[index][i][0],lastScan[index][i+1][0]));
	    int maxx = MAX(MAX(iscan[i][0],iscan[i+1][0]),
			   MAX(lastScan[index][i][0],lastScan[index][i+1][0]));
	    int miny = MIN(MIN(iscan[i][1],iscan[i+1][1]),
			   MIN(lastScan[index][i][1],lastScan[index][i+1][1]));
	    int maxy = MAX(MAX(iscan[i][1],iscan[i+1][1]),
			   MAX(lastScan[index][i][1],lastScan[index][i+1][1]));
	    useTriangles = useTriangles && ((maxx-minx)<MAX_TRI_SIZE);
	    useTriangles = useTriangles && ((maxy-miny)<MAX_TRI_SIZE);
	}
	if (useTriangles)
	{
	    this->map->minTriangle(iscan[i][0],iscan[i][1],iscan[i+1][0],iscan[i+1][1],
				   lastScan[index][i][0],lastScan[index][i][1],height[i]);
	    this->map->minTriangle(lastScan[index][i+1][0],lastScan[index][i+1][1],iscan[i+1][0],iscan[i+1][1],
				   lastScan[index][i][0],lastScan[index][i][1],height[i]);
	}
	else if (false)
	{
	      this->map->drawminRectangle(iscan[i][0]-GP_SIZE,iscan[i][1]-GP_SIZE,
					iscan[i][0]+GP_SIZE,iscan[i][1]+GP_SIZE, height[i]);
	}
    }
    if (road[blob->numPoints-1]==0)
	iscan[blob->numPoints-1][0] = RMAP_INVALID;

    memcpy(&(lastScan[index][0][0]),iscan,sizeof(int)*2*LADAR_BLOB_MAX_POINTS);
    shiftX = shiftY = 0;
}
