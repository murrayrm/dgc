#include "emapWrapper.hh"

using namespace std;

//! Constructor -- currently does nothing
EMapWrapper::EMapWrapper()
{
  return;
}

//! destructor -- currently does nothing
EMapWrapper::~EMapWrapper()
{
  return;
}


//! initializes the emap and some other variables
void EMapWrapper::initMap(EMap* m_emap, double m_localX, double m_localY, double m_rowRes, double m_colRes, emap_t m_initValue)
{
  // make alice at center of map  
  this->lastLocalX = m_localX;
  this->lastLocalY = m_localY;
  this->rowRes = m_rowRes;
  this->colRes = m_colRes;
  this->size = m_emap->getWidth();
  this->clearMap(m_emap, m_initValue);

  return;
}

//! clears the emap
void EMapWrapper::clearMap(EMap* m_emap, emap_t m_clearValue)
{
  if(m_clearValue==0)
    {
      m_emap->clear(0);
    }
  else
    {
      int width;
      width = m_emap->getWidth();
      m_emap->clear(0);
      m_emap->drawRectangle(0, 0, width-1, width-1, m_clearValue);
    }
  return;
}

//! updates the map to the new vehicle location
void EMapWrapper::updateVehicleLoc(EMap* m_emap, double m_localX, double m_localY, emap_t m_clearValue)
{
  // this gets kind of tricky since local frame has x as northing, y as easting
  // whereas the window frame has x right and y down;
  double dx, dy;
  int m_dx, m_dy;
  int sign_dx, sign_dy;
  bool shiftMap = false;

  dx = m_localY - this->lastLocalY;
  dy = -1*(m_localX - this->lastLocalX);
  
  if(dx >=0)
    sign_dx = 1;
  else
    sign_dx = -1;

  if(dy >=0)
    sign_dy = 1;
  else
    sign_dy = -1;

  m_dx = sign_dx*(long int)floor(fabs(dx)/(this->colRes));
  m_dy = sign_dy*(long int)floor(fabs(dy)/(this->rowRes));

  if(m_dx != 0)
    {
      this->lastLocalY = m_localY;
      shiftMap = true;
    }

  if(m_dy != 0)
    {
      this->lastLocalX = m_localX;
      shiftMap = true;
    }

  // if alice is moving by (m_dx, m_dy) then the map needs to shift by (-m_dx, -m_dy) to stay put  
  if(shiftMap)    
    m_emap->shiftClear(-m_dx, -m_dy, m_clearValue);
    
  return;

}

//! converts the UTM coordinates to window coordinates
void EMapWrapper::UTM2Win(EMap* m_emap, double m_localX, double m_localY, int* m_winRow, int* m_winCol)
{
  this->size = m_emap->getWidth();

  // first find where the point is relative to the center of the map (which is alice's loc)
  double dx_rta, dy_rta; //relative to alice
  double dx_rtw, dy_rtw; //relative to window origin (top left corner)
  int row, col;
  
  dx_rta = m_localX- lastLocalX;
  dy_rta = m_localY - lastLocalY;

  dx_rtw = dy_rta + ((double)this->size)*this->colRes/2;
  dy_rtw = -dx_rta + ((double)this->size)*this->rowRes/2;

  // now convert to window coordinates
  row = (long int)floor(dy_rtw/this->rowRes);
  col = (long int)floor(dx_rtw/this->colRes);
  
  // if within window bounds, then set to appropriate value
  if(row >= 0 && row <= this->size-1 &&
     col >= 0 && col <= this->size-1)
    {
      *m_winRow = row;
      *m_winCol = col;
    }
  else
    {
      *m_winRow = -1;
      *m_winCol = -1;
    }
  
  return;

}

//! converts the window coordinates to UTM coordinates
void EMapWrapper::Win2UTM(EMap* m_emap, int m_winRow, int m_winCol, double* m_localX, double* m_localY)
{
  this->size = m_emap->getWidth();

  // first find the cell in UTM coordinates relative to window frame
  double dx_rtw, dy_rtw; //relative to window
  double dx_rta, dy_rta; //relative to Alice
  
  dx_rtw = (double)m_winCol*this->colRes;
  dy_rtw = (double)m_winRow*this->rowRes;

  // now find where the point is relative to the center of the map (which is alice's location)
  dx_rta = -1*dy_rtw + ((double)this->size)*this->rowRes/2;
  dy_rta = dx_rtw - ((double)this->size)*this->colRes/2;

  // now assign the value
  *m_localX = this->lastLocalX + dx_rta;
  *m_localY = this->lastLocalY + dy_rta;
  
  return;  
}

//! gets the data at the appropriate location specified in window coordinates
int EMapWrapper::getDataWin(EMap* m_emap, int m_winRow, int m_winCol, emap_t* m_val)
{
  this->size = m_emap->getWidth();

  if(m_winRow < 0 || m_winRow > this->size-1)
    return(-1);
  else if(m_winCol < 0 || m_winCol > this->size-1)
    return(-1);
  else
    *m_val = *(m_emap->getData() + m_winRow*this->size + m_winCol);

  return(0);

}

//! sets the data in the window coordinates
void EMapWrapper::setDataWin(EMap* m_emap, int m_winRow, int m_winCol, emap_t m_val)
{
  this->size = m_emap->getWidth();

  emap_t *p_val;

  //have the pointer point to the appropriate memory address location
  p_val = (m_emap->getData() + m_winRow*this->size + m_winCol); 
  
  //now dereference
  *p_val = m_val;

  return;
}

//! gets the data at the appropriate location specified in UTM coordinates
int EMapWrapper::getDataUTM(EMap* m_emap, double m_localX, double m_localY, emap_t* m_val)
{
  int m_winRow, m_winCol;
  this->size = m_emap->getWidth();

  this->UTM2Win(m_emap, m_localX, m_localY, &m_winRow, &m_winCol);

  if(m_winRow < 0 || m_winRow > this->size-1)
    return(-1);
  else if(m_winCol < 0 || m_winCol > this->size-1)
    return(-1);
  else
    *m_val = *(m_emap->getData() + m_winRow*this->size + m_winCol);

  return(0);
}

//! sets the data in UTM coordinates
void EMapWrapper::setDataUTM(EMap* m_emap, double m_localX, double m_localY, emap_t m_val)
{
  int m_winRow, m_winCol;
  this->UTM2Win(m_emap, m_localX, m_localY, &m_winRow, &m_winCol);
  this->setDataWin(m_emap, m_winRow, m_winCol, m_val);
  
  return;
}

//! fill area enclosed by triangle to desired value with vertices specified in window coordinates
void EMapWrapper::fillTriangle(EMap* m_emap, int x1, int y1, int x2, int y2, int x3, int y3, emap_t m_val)
{
  m_emap->fillTriangle(x1, y1, x2, y2, x3, y3, m_val);
  return;
}

//! checks whether given UTM point is inside the map
bool EMapWrapper::isInsideMap(EMap* m_emap, double m_localX, double m_localY)
{
  bool inside;
  int m_winRow, m_winCol;
  
  this->UTM2Win(m_emap, m_localX, m_localY, &m_winRow, &m_winCol);
  if(m_winRow<0 || m_winCol <0)
    inside = false;
  else
    inside = true;

  return inside; 
}

