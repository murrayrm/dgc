#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>

#ifndef _EMAP_HH_
#define _EMAP_HH_

//! The map type used by emap, MUST BE UNSIGNED
typedef uint16_t emap_t;

inline
int int_max(int a, int b) {
    b = a-b;
    a -= b & (b>>31);
    return a;
}

inline
int int_min(int a, int b) {
    b = b-a;
    a += b & (b>>31);
    return a;
}

inline
int int_abs(int a) {
    return a - ((a+a) & (a>>31));
}

inline
int int_sgn(int a) {
    return 0x01|(a>>31);
}

//! 2D point struct for Polygon drawing
struct Point2 {
    double x, y;
};

class EMap
{
    //! For fixed point computations, FRAC is number of fraction bits to use.
    static const int FRAC = 12;
    static const int HFRAC = 1<<(FRAC-1);

public:
    EMap(int size);
    ~EMap();

    void clear(emap_t val);
    void copyFrom(EMap *src);

    void drawPolygon(int nvert, Point2 *point, emap_t g);
    void drawminPolygon(int nvert, Point2 *point, emap_t g);
    void drawmaxPolygon(int nvert, Point2 *point, emap_t g);
    emap_t drawgetmaxPolygon(int nvert, Point2 *point, emap_t g);
    void incPolygon(int nvert, Point2 *point, emap_t val);
    void decPolygon(int nvert, Point2 *point, emap_t val);
    double avgPolygon(int nvert, Point2 *point);
    double fracPolygon(int nvert, Point2 *point, emap_t thresh);
    
    void drawRectangle(int x1, int y1, int x2, int y2, emap_t g);
    void drawminRectangle(int x1, int y1, int x2, int y2, emap_t g);
    void drawmaxRectangle(int x1, int y1, int x2, int y2, emap_t g);
    void incRectangle(int x1, int y1, int x2, int y2, emap_t g);
    void decRectangle(int x1, int y1, int x2, int y2, emap_t g);
    void andRectangle(int x1, int y1, int x2, int y2, emap_t g);
    void orRectangle(int x1, int y1, int x2, int y2, emap_t g);

    void shadeTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g1, emap_t g2);
    void compTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g1, emap_t g2);
    void fillTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g);
    void minTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g);
    void maxTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g);
    void incTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t dc);
    void decTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t dc);

    void shadeLine(int x1, int y1, int x2, int y2, emap_t g1, emap_t g2);
    void compLine(int x1, int y1, int x2, int y2, emap_t g1, emap_t g2);
    void drawLine(int x1, int y1, int x2, int y2, emap_t g);
    void minLine(int x1, int y1, int x2, int y2, emap_t g);
    void maxLine(int x1, int y1, int x2, int y2, emap_t g);
    void getLine(int x1, int y1, int x2, int y2, emap_t *pts, int &count);

    void normalize();
    void scale(emap_t min, emap_t max);

    void moveToCenter(emap_t center, emap_t val);

    void threshold(emap_t val);
    void invthreshold(emap_t val);
    void threshlow(emap_t val);
    void threshhigh(emap_t val);
    void replace(emap_t val,emap_t nval);
    void erase(emap_t val);
    void eraseand(emap_t val);
    void andimg(emap_t val);
    void orimg(emap_t val);
    void increment(emap_t val);
    void decrement(emap_t val);

    void manhattan();
    void invmanhattan();
    void erode(int k);
    void dilate(int k);

    void minmaxRectangle(int x1, int y1, int x2, int y2, emap_t &min, emap_t &max);
    void avgRectangle(int x1, int y1, int x2, int y2, double &avg);
    void avgRectangleNoZero(int x1, int y1, int x2, int y2, double zeroVal1, double zeroVal2, double &avg);

    void shiftFill(int dx, int dy);
    void shiftClear(int dx, int dy, emap_t val);

    emap_t* getData() {return img;}
    int getWidth() {return width;}

private:
    emap_t *img;
    int width;

    void shiftLeft(int dx);
    void shiftRight(int dx);
    void shiftUp(int dy);
    void shiftDown(int dy);

    void fillLeft(int dx);
    void fillRight(int dx);
    void fillUp(int dy);
    void fillDown(int dy);

    void clearLeft(int dx, emap_t val);
    void clearRight(int dx, emap_t val);
    void clearUp(int dy, emap_t val);
    void clearDown(int dy, emap_t val);
};
#endif
