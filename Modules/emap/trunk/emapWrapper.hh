#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <emap/emap.hh>
#include <frames/point2.hh>
#include <iostream>

/*! \brief 
 *  This is a wrapper around the basic emap class functions. In order to use it, you need to have 
 *  a wrapper for each emap that you're using since the private variables are dependent on the emap
 *  used to initialize it. Generally, any emapWrapper object should be initialized with the map 
 *  first before making any other function calls. You can initialize it by  calling the "initMap" 
 *  function. After that, all other functions can be used. 
 */

using namespace std;

class EMapWrapper
{
public: 
  // Default constructor
  EMapWrapper();

  // Default destructor
  ~EMapWrapper();

  
  //! initializes the emap to the given vehicle location placed at the center of the map 
  void initMap(EMap* m_emap, double m_localX, double m_localY, double m_rowRes, double m_colRes, emap_t m_initValue);
    
  //! clears the emap
  void clearMap(EMap* m_emap, emap_t m_clearValue);

  //! updates the map to the new vehicle location
  void updateVehicleLoc(EMap* m_emap, double m_localX, double m_localY, emap_t m_clearValue);

  //! converts the UTM coordinates to window coordinates
  void UTM2Win(EMap* m_emap, double m_localX, double m_localY, int* m_winRow, int* m_winCol);

  //! converts the window coordinates to UTM coordinates
  void Win2UTM(EMap* m_emap, int m_winRow, int m_winCol, double* m_localX, double* m_localY);

  //! gets the data at the appropriate location specified in window coordinates
  int getDataWin(EMap* m_emap, int m_winRow, int m_winCol, emap_t *m_val);

  //! sets the data in the window coordinates
  void setDataWin(EMap* m_emap, int m_winRow, int m_winCol, emap_t m_val);

  //! gets the data at the appropriate location specified in UTM coordinates
  int getDataUTM(EMap* m_emap, double m_localX, double m_localY, emap_t *m_val);

  //! sets the data in UTM coordinates
  void setDataUTM(EMap* m_emap, double m_localX, double m_localY, emap_t m_val);

  //! fill area enclosed by triangle to desired value; vertices specified in window coordinates
  void fillTriangle(EMap* m_emap, int x1, int y1, int x2, int y2, int x3, int y3, emap_t m_val);

  //! checks whether given UTM point is inside the map
  bool isInsideMap(EMap* m_emap, double m_localX, double m_localY);

  // Some static helper functions
  //! Returns the local->map transform matrix
  static void getTransform(EMap *map, double lx, double ly, double res, float mat[4][4]);

  //! Converts a point2arr to emap polygon format
  static Point2* makePoly(point2arr& poly);

  //! Converts & transforms point2arr to polygon format
  static Point2* makePoly(point2arr& poly, float mat[4][4]);

private:

  //! last known vehicle state
  double lastLocalX;  
  double lastLocalY;
  
  //! row resolution of the map in meters
  double rowRes;

  //! column resolution of the map in meters
  double colRes;

  //! size of the map in number of cells specified by one side of the square map
  int size;

};
