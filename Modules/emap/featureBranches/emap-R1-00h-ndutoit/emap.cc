/*!
 * \file emap.cc
 * \brief A lightweight map class with fast drawing functions
 *
 * \author Tamas Szalay
 * \date 15 Aug 2007
 */

#include "emap.hh"

// I'm so going to hell for this
// but I can't think of any other fast way to do this

// This macro draws scans from y=start to y=end-1, using
// a pixel-filling function specified with func.
#define DRAW_SCANS(ystart,yend,func) \
for (int y=(ystart);y<(yend);y++) \
{ \
    xstart = (xstartF >> FRAC); \
    xend = (xendF >> FRAC); \
    ccurF = cstartF; \
    if (xstart < 0) \
    { \
	ccurF += (dcdxF*-xstart); \
	xstart = 0; \
    } \
    if (xend>=stride) xend=stride-1; \
    for (curPixel=curRow+xstart;curPixel<=curRow+xend;curPixel++) \
    { \
        func \
	ccurF += dcdxF; \
    } \
    xstartF += dxdy1F; \
    xendF += dxdy2F; \
    cstartF += dcdy1F; \
    curRow += stride; \
}

// Same as DRAW_SCANS, but no shading
#define FILL_SCANS(ystart,yend,func) \
for (int y=(ystart);y<(yend);y++) \
{ \
    xstart = (xstartF >> FRAC); \
    xend = (xendF >> FRAC); \
    if (xstart < 0) xstart = 0; \
    if (xend>=stride) xend=stride-1; \
    for (curPixel=curRow+xstart;curPixel<=curRow+xend;curPixel++) \
    { \
        func \
    } \
    xstartF += dxdy1F; \
    xendF += dxdy2F; \
    curRow += stride; \
}


// This macro sets up the drawing functions:
// it creates the color gradients, sorts the y-coordinates
// in ascending order, and computes the slopes of the lines
// (and checks if the poly is "flipped", which side of the p1-p3
// line the middle vertex lies on)
#define DRAW_START \
int stride = this->width; \
emap_t g3 = g2; \
int dcdxF; \
{ \
    int x0 = (x2 + x3) >> 1; \
    int y0 = (y2 + y3) >> 1; \
    x0 -= x1; \
    y0 -= y1; \
    int z0F = (g2 - g1) << FRAC; \
    int rs = (x0*x0 + y0*y0); \
    if (rs == 0) \
	return; \
    dcdxF = x0 * (z0F / rs); \
} \
/* First, sort vertices by y-coordinate, y1 smallest */ \
if (y1 > y2) \
{ \
    int t = x1; \
    x1 = x2; \
    x2 = t; \
    t = y1; \
    y1 = y2; \
    y2 = t; \
    emap_t g = g1; \
    g1 = g2; \
    g2 = g; \
} \
/* Now, y1 < y2 */ \
if (y1 > y3) \
{ \
    int t = x1; \
    x1 = x3; \
    x3 = t; \
    t = y1; \
    y1 = y3; \
    y3 = t; \
    emap_t g = g1; \
    g1 = g3; \
    g3 = g; \
} \
/* y1 is smallest */ \
if (y2 > y3) \
{ \
    int t = x2; \
    x2 = x3; \
    x3 = t; \
    t = y2; \
    y2 = y3; \
    y3 = t; \
    emap_t g = g2; \
    g2 = g3; \
    g3 = g; \
} \
/* In correct order now */ \
/* Compute deltas, make sure no divide by zero*/ \
int dy1 = y3-y2; \
if (dy1 == 0) \
    dy1 = 1; \
int dy2 = y2-y1; \
if (dy2 == 0) \
    dy2 = 1; \
int dy3 = y3-y1; \
if (dy3 == 0) \
    dy3 = 1; \
emap_t *curRow = this->img + stride * y1; \
emap_t *curPixel; \
int dxdy1F = ((x3 - x1)<<FRAC)/dy3; \
int dxdy2F = ((x2 - x1)<<FRAC)/dy2; \
int dcdy1F = ((g3 - g1)<<FRAC)/dy3; \
int dcdy2F = ((g2 - g1)<<FRAC)/dy2; \
bool flipped = false; \
if (dxdy1F > dxdy2F) \
{ \
    int t = dxdy1F; \
    dxdy1F = dxdy2F; \
    dxdy2F = t; \
    dcdy1F = dcdy2F; \
    flipped = true; \
} \
int xstartF = x1<<FRAC; \
int xendF = x1<<FRAC; \
xstartF += HFRAC; \
xendF += HFRAC; \
int cstartF = g1 << FRAC; \
cstartF += HFRAC; \
int ccurF, xstart, xend; 


// This macro checks the extreme cases first (no polygon drawn at all)
// and then checks if any of the vertices are above the image, compensating
// accordingly by incrementing relevant variables.
// If either y2 or y3 are below the bottom of the screen, it correctly limits
// the range we draw.
#define DRAW_CLIP_1 \
if (y1 >= stride || y3 < 0) \
    return; \
if (y2 > stride) \
    y2 = stride; \
if (y3 > stride) \
    y3 = stride-1; \
if (y1 < 0) \
{ \
    int dyt; \
    if (y2 < 0) \
    { \
	dyt = y2 - y1; \
	y1 = y2; \
    } \
    else \
    { \
	dyt = -y1; \
	y1 = 0; \
    } \
    xstartF += (dxdy1F*dyt); \
    xendF += (dxdy2F*dyt); \
    cstartF += (dcdy1F*dyt); \
    curRow += (stride*dyt); \
}

// Computes the relevant variables for
// the bottom half of our triangle
#define DRAW_MID \
if (flipped) \
{ \
    dxdy1F = ((x3 - x2)<<FRAC)/dy1; \
    xstartF = x2<<FRAC; \
    xstartF += HFRAC; \
    dcdy1F = ((g3 - g2)<<FRAC)/dy1; \
    cstartF = g2 << FRAC; \
    cstartF += HFRAC; \
} \
else \
{ \
    dxdy2F = ((x3 - x2)<<FRAC)/dy1; \
    xendF = x2<<FRAC; \
    xendF += HFRAC; \
}

// One special case we need to handle here
// if we are still above image area
#define DRAW_CLIP_2 \
if (y2 < 0) \
{ \
    int dyt = -y2; \
    xstartF += (dxdy1F*dyt); \
    xendF += (dxdy2F*dyt); \
    cstartF += (dcdy1F*dyt); \
    curRow += (stride*dyt); \
}

// Same as DRAW_START, except no coloring
#define FILL_START \
int stride = this->width; \
/* First, sort vertices by y-coordinate, y1 smallest */ \
if (y1 > y2) \
{ \
    int t = x1; \
    x1 = x2; \
    x2 = t; \
    t = y1; \
    y1 = y2; \
    y2 = t; \
} \
/* Now, y1 < y2 */ \
if (y1 > y3) \
{ \
    int t = x1; \
    x1 = x3; \
    x3 = t; \
    t = y1; \
    y1 = y3; \
    y3 = t; \
} \
/* y1 is smallest */ \
if (y2 > y3) \
{ \
    int t = x2; \
    x2 = x3; \
    x3 = t; \
    t = y2; \
    y2 = y3; \
    y3 = t; \
} \
/* In correct order now */ \
/* Compute deltas, make sure no divide by zero*/ \
int dy1 = y3-y2; \
if (dy1 == 0) \
    dy1 = 1; \
int dy2 = y2-y1; \
if (dy2 == 0) \
    dy2 = 1; \
int dy3 = y3-y1; \
if (dy3 == 0) \
    dy3 = 1; \
emap_t *curRow = this->img + stride * y1; \
emap_t *curPixel; \
int dxdy1F = ((x3 - x1)<<FRAC)/dy3; \
int dxdy2F = ((x2 - x1)<<FRAC)/dy2; \
bool flipped = false; \
if (dxdy1F > dxdy2F) \
{ \
    int t = dxdy1F; \
    dxdy1F = dxdy2F; \
    dxdy2F = t; \
    flipped = true; \
} \
int xstartF = x1<<FRAC; \
int xendF = x1<<FRAC; \
xstartF += HFRAC; \
xendF += HFRAC; \
int xstart, xend;

// Same as DRAW_CLIP_1, without coloring
#define FILL_CLIP_1 \
if (y1 >= stride || y3 < 0) \
    return; \
if (y2 > stride) \
    y2 = stride; \
if (y3 > stride) \
    y3 = stride-1; \
if (y1 < 0) \
{ \
    int dyt; \
    if (y2 < 0) \
    { \
	dyt = y2 - y1; \
	y1 = y2; \
    } \
    else \
    { \
	dyt = -y1; \
	y1 = 0; \
    } \
    xstartF += (dxdy1F*dyt); \
    xendF += (dxdy2F*dyt); \
    curRow += (stride*dyt); \
}

// DRAW_MID, but without shading
#define FILL_MID \
if (flipped) \
{ \
    dxdy1F = ((x3 - x2)<<FRAC)/dy1; \
    xstartF = x2<<FRAC; \
    xstartF += HFRAC; \
} \
else \
{ \
    dxdy2F = ((x3 - x2)<<FRAC)/dy1; \
    xendF = x2<<FRAC; \
    xendF += HFRAC; \
}

// DRAW_CLIP_2, without shading
#define FILL_CLIP_2 \
if (y2 < 0) \
{ \
    int dyt = -y2; \
    xstartF += (dxdy1F*dyt); \
    xendF += (dxdy2F*dyt); \
    curRow += (stride*dyt); \
}

#define LINE_DRAW(func) \
int i,dx,sdx,dxabs,dyabs,x,y; \
int stride = this->width; \
/* makes bounds checking much easier */ \
if (y1 > y2) \
{ \
    int t = y1; \
    y1 = y2; \
    y2 = t; \
    t = x1; \
    x1 = x2; \
    x2 = t; \
    emap_t g = g1; \
    g1 = g2; \
    g2 = g; \
} \
if (y2 < 0) \
    return; \
dx=x2-x1;      /* the horizontal distance of the line */ \
dxabs=int_abs(dx); \
sdx=int_sgn(dx); \
dyabs= y2-y1; \
x=dyabs>>1; \
y=dxabs>>1; \
emap_t *curPixel = this->img + x1 + y1 * stride; \
int size = stride*stride; \
emap_t *imgEnd = this->img + size; \
*curPixel = g1; \
if (dxabs>=dyabs) /* the line is more horizontal than vertical */ \
{ \
    if (dxabs == 0) \
	return; \
    int dcdx = ((g2-g1)<<FRAC)/dxabs; \
    int ccurF = (g1 << FRAC)+HFRAC; \
    for(i=0;i<dxabs;i++) \
    { \
	y+=dyabs; \
	if (y>=dxabs) \
	{ \
	    y-=dxabs; \
	    curPixel += stride; \
	} \
	curPixel += sdx; \
	ccurF += dcdx; \
	if (curPixel >= this->img && curPixel < imgEnd) \
	{ \
	    func; \
	} \
    } \
} \
else /* the line is more vertical than horizontal */ \
{ \
    int dcdx = ((g2-g1)<<FRAC)/dyabs; \
    int ccurF = g1 << FRAC; \
    if (y2 >= stride) \
	y2 = stride-1; \
    for(i=y1;i<=y2;i++) \
    { \
	x+=dxabs; \
	if (x>=dyabs) \
	{ \
	    x-=dyabs; \
	    curPixel += sdx; \
	} \
	curPixel += stride; \
	ccurF += dcdx; \
	if (i>0) \
	{ \
	    func \
	} \
    } \
}

#define LINE_FILL(func) \
int i,dx,sdx,dxabs,dyabs,x,y; \
int stride = this->width; \
/* makes bounds checking much easier */ \
if (y1 > y2) \
{ \
    int t = y1; \
    y1 = y2; \
    y2 = t; \
    t = x1; \
    x1 = x2; \
    x2 = t; \
} \
if (y2 < 0) \
    return; \
dx=x2-x1;  /* the horizontal distance of the line */ \
dxabs=int_abs(dx); \
sdx=int_sgn(dx); \
dyabs= y2-y1; \
x=dyabs>>1; \
y=dxabs>>1; \
emap_t *curPixel = this->img + x1 + y1 * stride; \
int size = stride*stride; \
emap_t *imgEnd = this->img + size; \
if (curPixel >= this->img && curPixel < imgEnd) \
{ \
    func; \
} \
if (dxabs>=dyabs) /* the line is more horizontal than vertical */ \
{ \
    if (dxabs == 0) \
	return; \
    for(i=0;i<dxabs;i++) \
    { \
	y+=dyabs; \
	if (y>=dxabs) \
	{ \
	    y-=dxabs; \
	    curPixel += stride; \
	} \
	curPixel += sdx; \
	if (curPixel >= this->img && curPixel < imgEnd) \
	{ \
	    func; \
	} \
    } \
} \
else /* the line is more vertical than horizontal */ \
{ \
    if (y2 >= stride) \
	y2 = stride-1; \
    for(i=y1;i<=y2;i++) \
    { \
	x+=dxabs; \
	if (x>=dyabs) \
	{ \
	    x-=dyabs; \
	    curPixel += sdx; \
	} \
	curPixel += stride; \
	if (i>0) \
	{ \
            func \
	} \
    } \
}


#define START_RECT \
int stride = this->width; \
if (x1 > x2) \
{ \
    int t = x1; \
    x1 = x2; \
    x2 = t; \
} \
if (y1 > y2) \
{ \
    int t = y1; \
    y1 = y2; \
    y2 = t; \
} \
if ((y2 | x2) < 0) \
    return; \
if ((x1 >= stride) || (y1 >= stride)) \
    return; \
x1 = int_max(x1, 0); \
y1 = int_max(y1, 0); \
x2 = int_min(x2, stride-1); \
y2 = int_min(y2, stride-1); \
emap_t *curStart = this->img + stride*y1 + x1; \
emap_t *curPixel;




#define SHADE_FUNC \
*curPixel = (ccurF>>FRAC);

#define COMP_FUNC \
if (*curPixel > (ccurF>>FRAC)) \
    *curPixel = 0;

#define FILL_FUNC \
*curPixel = g;

#define MIN_FUNC \
if (g < *curPixel)\
*curPixel = g;

#define MAX_FUNC \
if (g > *curPixel)\
*curPixel = g;

// This function is hax. dc becomes "negative" by the time you reach here
// so that I can make sure it doesn't wrap around. *curPixel < (-5) means
// that if you add 5 to it it won't wrap around, and then I subtract -5
// from it. Neat, huh?
#define INC_FUNC \
if (*curPixel < dc) \
    *curPixel -= dc;

#define DEC_FUNC \
if (*curPixel > dc) \
    *curPixel -= dc;

// For returning the values of a line
#define GET_FUNC \
pts[count] = *curPixel; \
count++;

#undef MIN
#undef MAX
#define MIN(a,b) ((a)<(b))?(a):(b)
#define MAX(a,b) ((a)>(b))?(a):(b)


/*! Initialize with a fixed size (size being the width or height) */
EMap::EMap(int size)
{
    width = size;
    img = (emap_t*)malloc(size*size*sizeof(emap_t));
}

EMap::~EMap()
{
    free(img);
}

/*! Clear to a certain value */
void EMap::clear(emap_t val)
{
    emap_t *ptr = this->img;
    emap_t *imgEnd = ptr+this->width*this->width;
    for (;ptr<imgEnd;ptr++)
	*ptr = val;
}

/*! Copies map image from another EMap */
void EMap::copyFrom(EMap *src)
{
    if (src->getWidth() != this->width)
	return;
    memcpy(this->img, src->getData(), (this->width)*(this->width)*sizeof(emap_t));
}

/*! Finds min and max, and calls scale; map will be rescaled to range from 0 to max. value of emap_t */
void EMap::normalize()
{
    emap_t min = (emap_t)(-1);
    emap_t max = 0;

    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
    {
	if (*pix > 0 && *pix < min)
	    min = *pix;
	if (*pix > max)
	    max = *pix;
    }
    this->scale(min, max);
}

/*! Scales values from min to max to be 0 to maximum of emap_t, setting x<min to 0 and x>max to max(emap_t) */
void EMap::scale(emap_t min, emap_t max)
{
    if (min == max)
	return;

    // does fixed point scaling
    uint32_t scale = (uint32_t)((emap_t)(-1));
    scale <<= FRAC;
    scale /= (max-min);

    emap_t *pixEnd = this->img + (this->width*this->width);

    for (emap_t *pix = this->img; pix < pixEnd; pix++)
    {
	if (*pix <= min)
	{
	    *pix = 0;
	    continue;
	}
	if (*pix > max)
	{
	    *pix = -1;
	    continue;
	}
	*pix = (emap_t)(((uint32_t)(*pix-min)*scale)>>FRAC);
    }
}

/*! Shifts each pixel towards center by val. */
void EMap::moveToCenter(emap_t center, emap_t val)
{
    emap_t nval = -val;
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	*pix += (*pix>=center)?nval:val;
}

/*! Thresholds by value, setting above to max and below/equal to 0. */
void EMap::threshold(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	*pix = (*pix>val)?(emap_t)(-1):0;
}

/*! Thresholds by value, setting above to 0 and below/equal to max. */
void EMap::invthreshold(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	*pix = (*pix>val)?0:(emap_t)(-1);
}

/*! Thresholds by value, leaving above/equal as is and setting below to 0. */
void EMap::threshlow(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix<val)
	    *pix=0;
}

/*! Thresholds by value, setting above/equal to max and leaving below as is. */
void EMap::threshhigh(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix >= val)
	    *pix = (emap_t)(-1);
}

/*! Replaces val with nval */
void EMap::replace(emap_t val, emap_t nval)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix == val)
	    *pix = nval;
}

/*! Sets all vals to 0. */
void EMap::erase(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix == val)
	    *pix = 0;
}

/*! Sets anything that ands with val to 0. */
void EMap::eraseand(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix & val)
	    *pix = 0;
}

/*! Ands entire image with val. */
void EMap::andimg(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	*pix = (*pix & val);
}

/*! Ors entire image with val. */
void EMap::orimg(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	*pix = (*pix | val);
}

/*! Increments entire image */
void EMap::increment(emap_t val)
{
    val = -val;
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix < val) 
	    *pix -= val;
}

/*! Decrements entire image */
void EMap::decrement(emap_t val)
{
    emap_t *pixEnd = this->img + (this->width*this->width);
    for (emap_t *pix = this->img; pix < pixEnd; pix++)
	if (*pix >= val) 
	    *pix -= val;
}

/*! Fills the image with each pixel containing the distance to the nearest pixel */
// probably copyright Stephen Ostermiller
void EMap::manhattan()
{
    int width = this->width; //local register
    emap_t *pix = this->img;
    // traverse from top left to bottom right
    for (int i=0; i<width; i++){
        for (int j=0; j<width; j++){
            if (*pix > 0){
                // first pass and pixel was on, it gets a zero
                *pix = 0;
            } else {
                // pixel was off
                // It is at most the sum of the lengths of the array
                // away from a pixel that is on
                *pix = (width<<1);
                // or one more than the pixel to the north
                if (i>0) *pix = MIN(*pix, *(pix-width)+1);
                // or one more than the pixel to the west
                if (j>0) *pix = MIN(*pix, *(pix-1)+1);
            }
	    pix++;
        }
    }
    // traverse from bottom right to top left
    for (int i=0; i<width; i++){
        for (int j=0; j<width; j++){
	    pix--;
            // either what we had on the first pass
            // or one more than the pixel to the south
            if (i>0) *pix = MIN(*pix, *(pix+width)+1);
            // or one more than the pixel to the east
            if (j>0) *pix = MIN(*pix, *(pix+1)+1);
        }
    }
}

/*! Fills the image with each pixel containing the distance to the nearest off pixel */
// probably copyright Stephen Ostermiller
void EMap::invmanhattan()
{
    int width = this->width; //local register
    emap_t *pix = this->img;
    // traverse from top left to bottom right
    for (int i=0; i<width; i++){
        for (int j=0; j<width; j++){
	    // opposite of manhattan(): consider off pixels to be on pixels
            if (*pix > 0){
                // pixel was off
                // It is at most the sum of the lengths of the array
                // away from a pixel that is on
                *pix = (width<<1);
                // or one more than the pixel to the north
                if (i>0) *pix = MIN(*pix, *(pix-width)+1);
                // or one more than the pixel to the west
                if (j>0) *pix = MIN(*pix, *(pix-1)+1);
            }
	    pix++;
        }
    }
    // traverse from bottom right to top left
    for (int i=0; i<width; i++){
        for (int j=0; j<width; j++){
	    pix--;
            // either what we had on the first pass
            // or one more than the pixel to the south
            if (i>0) *pix = MIN(*pix, *(pix+width)+1);
            // or one more than the pixel to the east
            if (j>0) *pix = MIN(*pix, *(pix+1)+1);
        }
    }
}

/*! Dilates the image by k */
void EMap::dilate(int k)
{
    manhattan();
    invthreshold((emap_t)k);
}

/*! Erodes the image by k */
void EMap::erode(int k)
{
    invmanhattan();
    threshold((emap_t)k);
}

/*! Shifts entire map dx columns to the left, leaving gap on the right. */
void EMap::shiftLeft(int dx)
{
    memmove(this->img, this->img+dx, this->width*this->width*sizeof(emap_t)-dx);
}

/*! Shifts entire map dx columns to the right, leaving gap on the left. */
void EMap::shiftRight(int dx)
{
    memmove(this->img+dx, this->img, this->width*this->width*sizeof(emap_t)-dx);
}

/*! Shifts entire map dy columns up, leaving gap on the bottom. */
void EMap::shiftUp(int dy)
{
    int width = this->width;
    memmove(this->img, this->img+width*dy, (width-dy)*width*sizeof(emap_t));
}

/*! Shifts entire map dy columns down, leaving gap on the top. */
void EMap::shiftDown(int dy)
{
    int width = this->width;
    memmove(this->img+width*dy, this->img, (width-dy)*width*sizeof(emap_t));
}

/*! NOTE: This fills dx columns on the RIGHT side of the map, propagating the
 * rightmost valid column. This is so that shiftLeft(dx) and fillLeft(dx) can 
 * be called in sequence. */
void EMap::fillLeft(int dx)
{
    int width = this->width;
    emap_t *ptr_read = this->img + (width - dx - 1);
    emap_t *ptr_write;
    for (int i=0; i<width; i++)
    {
	ptr_write = ptr_read+1;
	for (int j=0; j<dx; j++)
	{
	    *ptr_write = *ptr_read;
	    ptr_write++;
	}
	ptr_read += width;
    }
}

/*! NOTE: This fills dx columns on the LEFT side of the map, propagating the
 * leftmost valid column. This is so that shiftRight(dx) and fillRight(dx) can 
 * be called in sequence. */
void EMap::fillRight(int dx)
{
    int width = this->width;
    emap_t *ptr_read = this->img + dx;
    emap_t *ptr_write;
    for (int i=0; i<width; i++)
    {
	ptr_write = ptr_read-1;
	for (int j=0; j<dx; j++)
	{
	    *ptr_write = *ptr_read;
	    ptr_write--;
	}
	ptr_read += width;
    }
}

/*! NOTE: This fills dy rows on the BOTTOM of the map, propagating the
 * bottom-most valid row. This is so that shiftUp(dy) and fillUp(dy) can 
 * be called in sequence. */
void EMap::fillUp(int dy)
{
    int width = this->width;
    emap_t *ptr_read = this->img + width*(width-dy-1);
    for (int i=1;i<=dy;i++)
	memcpy(ptr_read+(i*width), ptr_read, width*sizeof(emap_t));
}

/*! NOTE: This fills dy rows on the TOP of the map, propagating the
 * top-most valid row. This is so that shiftDown(dy) and fillDown(dy) can 
 * be called in sequence. */
void EMap::fillDown(int dy)
{
    int width = this->width;
    emap_t *ptr_read = this->img + width*dy;
    for (int i=0;i<dy;i++)
	memcpy(this->img + (i*width), ptr_read, width*sizeof(emap_t));
}

/*! Clears dx columns on the RIGHT side of the map to val. See fill*
 * for explanation. */
void EMap::clearLeft(int dx, emap_t val)
{
    int width = this->width;
    emap_t *ptr_read = this->img + (width - dx);
    emap_t *ptr_write;
    for (int i=0; i<width; i++)
    {
	ptr_write = ptr_read;
	for (int j=0; j<dx; j++)
	{
	    *ptr_write = val;
	    ptr_write++;
	}
	ptr_read += width;
    }
}

/*! Clears dx columns on the LEFT side of the map to val. See fill*
 * for explanation. */
void EMap::clearRight(int dx, emap_t val)
{
    int width = this->width;
    emap_t *ptr_read = this->img + dx - 1;
    emap_t *ptr_write;
    for (int i=0; i<width; i++)
    {
	ptr_write = ptr_read;
	for (int j=0; j<dx; j++)
	{
	    *ptr_write = val;
	    ptr_write--;
	}
	ptr_read += width;
    }
}

/*! Clears dy rows on the BOTTOM of the map to val. See fill*
 * for explanation. */
void EMap::clearUp(int dy, emap_t val)
{
    emap_t *ptr_write = this->img + this->width*(this->width-dy);
    for (int i=0;i<dy*width;i++)
    {
	*ptr_write = val;
	ptr_write++;
    }
}

/*! Clears dy rows on the TOP of the map to val. See fill*
 * for explanation. */
void EMap::clearDown(int dy, emap_t val)
{
    emap_t *ptr_write = this->img;
    for (int i=0;i<dy*width;i++)
    {
	*ptr_write = val;
	ptr_write++;
    }
}

/*! Shifts map by dx and dy rows and calls proper fill functions, also
 * does bounds checking. dx and dy can be positive (right, down) or
 * negative. */
void EMap::shiftFill(int dx, int dy)
{
    if (int_abs(dx) >= this->width-3)
	return;
    if (int_abs(dy) >= this->width-3)
	return;
    if (dx < 0)
    {
	dx = -dx;
	shiftLeft(dx);
	fillLeft(dx);
    }
    else if (dx > 0)
    {
	shiftRight(dx);
	fillRight(dx);
    }
    if (dy < 0)
    {
	dy = -dy;
	shiftUp(dy);
	fillUp(dy);
    }
    else if (dy > 0)
    {
	shiftDown(dy);
	fillDown(dy);
    }
}

/*! Shifts map by dx and dy rows and calls proper clear functions, also
 * does bounds checking. dx and dy can be positive (right, down) or
 * negative. */
void EMap::shiftClear(int dx, int dy, emap_t val)
{
    if (int_abs(dx) >= this->width-3)
	return;
    if (int_abs(dy) >= this->width-3)
	return;
    if (dx < 0)
    {
	dx = -dx;
	shiftLeft(dx);
	clearLeft(dx, val);
    }
    else if (dx > 0)
    {
	shiftRight(dx);
	clearRight(dx, val);
    }
    if (dy < 0)
    {
	dy = -dy;
	shiftUp(dy);
	clearUp(dy, val);
    }
    else if (dy > 0)
    {
	shiftDown(dy);
	clearDown(dy, val);
    }
}

/************ NEW DRAWING FUNCTIONS ***************/

//! Draw a shaded triangle on screen
void EMap::shadeTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g1, emap_t g2)
{
    // if we are trying to draw a degenerate polygon, draw line instead
    // looks way better, algorithm deals poorly with such points
    if (((x2- x3) | (y2 - y3))==0)
    {
	shadeLine(x1, y1, x2, y2, g1, g2);
	return;
    }

    DRAW_START;
    DRAW_CLIP_1;
    DRAW_SCANS(y1,y2,SHADE_FUNC);
    DRAW_MID;
    DRAW_CLIP_2;
    DRAW_SCANS(y2,y3+1,SHADE_FUNC);
}

//! Compare/clip all pixels above triangle color, sort of z-buffer test
void EMap::compTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g1, emap_t g2)
{
    // if we are trying to draw a degenerate polygon, draw line instead
    // looks way better, algorithm deals poorly with such points
    if (((x2- x3) | (y2 - y3))==0)
    {
	compLine(x1, y1, x2, y2, g1, g2);
	return;
    }

    DRAW_START;
    DRAW_CLIP_1;
    DRAW_SCANS(y1,y2,COMP_FUNC);
    DRAW_MID;
    DRAW_CLIP_2;
    DRAW_SCANS(y2,y3+1,COMP_FUNC);
}

//! Fill all triangle pixels to a single color
void EMap::fillTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g)
{
    // if we are trying to draw a degenerate polygon, draw line instead
    // looks way better, algorithm deals poorly with such points
    if (((x2- x3) | (y2 - y3))==0)
    {
	drawLine(x1, y1, x2, y2, g);
	return;
    }

    FILL_START;
    FILL_CLIP_1;
    FILL_SCANS(y1,y2,FILL_FUNC);
    FILL_MID;
    FILL_CLIP_2;
    FILL_SCANS(y2,y3+1,FILL_FUNC);
}

//! Fill all triangle pixels to the min of the current and g
void EMap::minTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g)
{
    // if we are trying to draw a degenerate polygon, draw line instead
    // looks way better, algorithm deals poorly with such points
    if (((x2- x3) | (y2 - y3))==0)
    {
	minLine(x1, y1, x2, y2, g);
	return;
    }

    FILL_START;
    FILL_CLIP_1;
    FILL_SCANS(y1,y2,MIN_FUNC);
    FILL_MID;
    FILL_CLIP_2;
    FILL_SCANS(y2,y3+1,FILL_FUNC);
}

//! Fill all triangle pixels to the max of the current and g
void EMap::maxTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t g)
{
    // if we are trying to draw a degenerate polygon, draw line instead
    // looks way better, algorithm deals poorly with such points
    if (((x2- x3) | (y2 - y3))==0)
    {
	maxLine(x1, y1, x2, y2, g);
	return;
    }

    FILL_START;
    FILL_CLIP_1;
    FILL_SCANS(y1,y2,MAX_FUNC);
    FILL_MID;
    FILL_CLIP_2;
    FILL_SCANS(y2,y3+1,FILL_FUNC);
}

//! Increment each triangle pixel by dc, without wrapping around
void EMap::incTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t dc)
{
    dc = -dc; //see the INC_FUNC definition up top
    FILL_START;
    FILL_CLIP_1;
    FILL_SCANS(y1,y2,INC_FUNC);
    FILL_MID;
    FILL_CLIP_2;
    FILL_SCANS(y2,y3+1,INC_FUNC);
}

//! Decrement each triangle pixel by dc, without wrapping around
void EMap::decTriangle(int x1, int y1, int x2, int y2, int x3, int y3, emap_t dc)
{
    FILL_START;
    FILL_CLIP_1;
    FILL_SCANS(y1,y2,DEC_FUNC);
    FILL_MID;
    FILL_CLIP_2;
    FILL_SCANS(y2,y3+1,DEC_FUNC);
}

//! Draw a smooth shaded line from (x1,y1,g1) to (x2,y2,g2)
void EMap::shadeLine(int x1, int y1, int x2, int y2, emap_t g1, emap_t g2)
{
    LINE_DRAW(SHADE_FUNC);
}

/*! Compute smooth shading color between two coordinates, and clip if current
 * pixel is larger than this value. */
void EMap::compLine(int x1, int y1, int x2, int y2, emap_t g1, emap_t g2)
{
    LINE_DRAW(COMP_FUNC);
}

//! Draws a line of solid color.
void EMap::drawLine(int x1, int y1, int x2, int y2, emap_t g)
{
    LINE_FILL(FILL_FUNC);
}

//! Draws a line of solid color/min.
void EMap::minLine(int x1, int y1, int x2, int y2, emap_t g)
{
    LINE_FILL(MIN_FUNC);
}

//! Draws a line of solid color/max.
void EMap::maxLine(int x1, int y1, int x2, int y2, emap_t g)
{
    LINE_FILL(MAX_FUNC);
}

//! Populates output array with values of line. Maximum of width points.
void EMap::getLine(int x1, int y1, int x2, int y2, emap_t *pts, int &count)
{
    count=0;
    LINE_FILL(GET_FUNC);
}

//! Draws a rectangle of solid color.
void EMap::drawRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    *curPixel = g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Draws a rectangle of solid color, filling in lesser of two values.
void EMap::drawminRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (g < *curPixel)
		*curPixel = g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Draws a rectangle of solid color, filling in greater of two values.
void EMap::drawmaxRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (g > *curPixel)
		*curPixel = g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Increments a rectangle by a certain value.
void EMap::incRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    g = -g;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (*curPixel<g)
		*curPixel -= g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Increments a rectangle by a certain value.
void EMap::decRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (*curPixel>g)
		*curPixel -= g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Ands a rectangle by a certain value.
void EMap::andRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    *curPixel &= g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Ors a rectangle by a certain value.
void EMap::orRectangle(int x1, int y1, int x2, int y2, emap_t g)
{
    START_RECT;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    *curPixel |= g;
	    curPixel++;
	}
	curStart += stride;
    }
}

//! Finds the minimum and maximum values in a rectangle (ignoring zeros!)
void EMap::minmaxRectangle(int x1, int y1, int x2, int y2, emap_t &min, emap_t &max)
{
    START_RECT;

    min = -1;
    max = 0;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (*curPixel > 0 && *curPixel < min)
		min = *curPixel;
	    else if (*curPixel > max)
		max = *curPixel;
	    curPixel++;
	}
	curStart += stride;
    }
}

/*! Finds the average value in a rectangle (including zeros).
 * This function can overflow, so don't find the average value of
 * a very large, completely white image... please? */
void EMap::avgRectangle(int x1, int y1, int x2, int y2, double &avg)
{
    START_RECT;

    uint32_t total=0;
    int numPix = 0;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (*curPixel > 0 && *curPixel < (emap_t)(-1))
	    {
		total += *curPixel;
		numPix++;
	    }
	    curPixel++;
	}
	curStart += stride;
    }

    if (numPix > 0)
	avg = ((double)total)/numPix;
    else
	avg = 0;
}

/*! Finds the average value in a rectangle,
 * excluding the input zero values 
 * (added for use by the RoadMap, which has 
 * a clear value, and a not-road value, neither of
 * which should be counted....) */
void EMap::avgRectangleNoZero(int x1, int y1, int x2, int y2, double zeroVal1, double zeroVal2, double &avg)
{
    START_RECT;

    uint32_t total=0;
    int numPix = 0;

    for (int y=y1; y<= y2; y++)
    {
	curPixel = curStart;
	for (int x=x1; x<=x2; x++)
	{
	    if (*curPixel > 0 && *curPixel < (emap_t)(-1) && *curPixel != zeroVal1 && *curPixel != zeroVal2)
	    {
		total += *curPixel;
		numPix++;
	    }
	    curPixel++;
	}
	curStart += stride;
    }

    if (numPix > 0)
	avg = ((double)total)/numPix;
    else
	avg = 0;
}
