/*!
 * \file emapViewer.cc
 * \brief A basic viewer for emap class
 * \author Tamas Szalay
 *
 * This file will need to be modified to suit your needs!
 * Call startViewer(argc, argv, EMap *emap) to create window and start
 * viewing thread, and call SetEMap(EMap *map) to set the
 * viewer's visible emap.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define GL_GLEXT_PROTOTYPES
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <GL/glext.h>
#endif

#include <sys/types.h>
#include <string.h>

#include <dgcutils/DGCutils.hh>
#include <emap/emap.hh>
#include <frames/point2.hh>

using namespace std;

//OpenGL variables
int drag_x0, drag_y0;
int lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;

// Initial value, will be set when thread is started
int WINDOW_SIZE=500;

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
EMap *map;
EMap *map_orig; // Unscaled map, if you need it
emap_t curMin, curMax; // The current minimum and maximum values of map
char strDisplay[64]; //On-screen display string
point2arr points;
float ptcolor[3];


void drawString (char *s)
{
    unsigned int i;
    for (i = 0; i < strlen (s); i++)
	glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
}

void setPoints(point2arr pts, float color[3])
{
    points = pts;
    memcpy(ptcolor,color,sizeof(float)*3);
}

void drawPoints()
{
    glColor3f(ptcolor[0],ptcolor[1],ptcolor[2]);
    glBegin(GL_POINTS);
    for (int i=0;i<(int)points.size();i++)
	glVertex2d(points[i].x,points[i].y);
    glEnd();
}

void setEMap(EMap *emap, emap_t min, emap_t max)
{
    if (!map)
	return;
    DGClockMutex(&mutex1);
    map->copyFrom(emap);
    map_orig->copyFrom(emap);
    if (max > min)
	map->scale(min, max);
    else
	map->normalize();
    curMin = min;
    curMax = max;
    DGCunlockMutex(&mutex1);
    glutPostRedisplay();
}

void drawEMap(void)
{
    glWindowPos2i(0,WINDOW_SIZE);
    DGClockMutex(&mutex1);
    if (sizeof(emap_t) == 1)
	glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_BYTE, map->getData());
    else if (sizeof(emap_t) == 2)
	glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_SHORT, map->getData());
    else if (sizeof(emap_t) == 4)
	glDrawPixels(WINDOW_SIZE, WINDOW_SIZE, GL_LUMINANCE, GL_UNSIGNED_INT, map->getData());
    glWindowPos2i(10,WINDOW_SIZE);
    glColor3f(1.0f,0.6f,0.6f); // Laura wanted it to be pink, ok?
    if (lbuttondown)
    {
	glBegin(GL_LINE_LOOP);
	glVertex2i(drag_x0,drag_y0);
	glVertex2i(drag_x0,lasty);
	glVertex2i(lastx,lasty);
	glVertex2i(lastx,drag_y0);	
	glEnd();
    }
    drawString(strDisplay);
    DGCunlockMutex(&mutex1);
}

void display(void)
{
    drawEMap();
    drawPoints();
    glFlush();
    usleep(50000);
    glutPostRedisplay();
}

void idle (void) 
{
    //replot data
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    lastx = x;
    lasty = y;
    if(button == GLUT_LEFT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	  // Your code here!
	  drag_x0 = x;
	  drag_y0 = y;
	}
	else
	{
	    lbuttondown = false;
	    if (drag_x0 >= x || drag_y0 >= y)
		return;
	    if (x >= map_orig->getWidth() || y >= map_orig->getWidth())
		return;
	    // Your code here!
	}
    }   
    if(button == GLUT_RIGHT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	    rbuttondown = true;
	}
	else
	{
	    rbuttondown = false;
	}
    }
}

void motion(int x, int y)
{
    lastx = x;
    lasty = y;
    if (lbuttondown)
    {
    }
    
    if (rbuttondown)
    {     
    }
}

// Disable resizing
void resize(int w, int h)
{
    if (w != WINDOW_SIZE || h != WINDOW_SIZE)
	glutReshapeWindow(WINDOW_SIZE,WINDOW_SIZE);
}

void * mainLoop(void*)
{
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE);
    int win = glutCreateWindow("EMap Viewer");  
    glClearColor (0.0,0.0,0.0,0.0); //clear the screen to black
    glShadeModel(GL_FLAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, sizeof(emap_t));
    glPixelZoom(1,-1);

    // Set transform matrix to be the pixel transform
    glLoadIdentity();
    glViewport (0, 0, WINDOW_SIZE, WINDOW_SIZE);
    glMatrixMode(GL_PROJECTION);
    glOrtho(0,WINDOW_SIZE,WINDOW_SIZE,0,-1,1);

    map = new EMap(WINDOW_SIZE);
    map_orig = new EMap(WINDOW_SIZE);
    DGCcreateMutex(&mutex1);
    
    /* set callback functions */
    glutDisplayFunc(display); 
    glutMouseFunc(mouse);
    glutMotionFunc(motion);    
    glutIdleFunc(idle);
    glutReshapeFunc(resize);

    //========================
    // initialize Map
    //========================
    glutMainLoop();
    // The above function never returns anyway

    //glutDestroyWindow(win);
    DGClockMutex(&mutex1);
    delete map;
    map = NULL;
    DGCunlockMutex(&mutex1);
    DGCdeleteMutex(&mutex1);

    return 0;
}

pthread_t startViewer(int argc, char **argv, int size)
{
    WINDOW_SIZE = size;
    glutInit(&argc, argv);
    pthread_t thread;
    pthread_create(&thread, 0, &mainLoop, 0);    
    return thread;
}
