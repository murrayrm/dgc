/*
 * Concave Polygon Scan Conversion
 * by Paul Heckbert
 * from "Graphics Gems", Academic Press, 1990
 */

/*
 * concave: scan convert nvert-sided concave non-simple polygon with vertices at
 * (point[i].x, point[i].y) for i in [0..nvert-1] within the window win by
 * calling spanproc for each visible span of pixels.
 * Polygon can be clockwise or counterclockwise.
 * Algorithm does uniform point sampling at pixel centers.
 * Inside-outside test done by Jordan's rule: a point is considered inside if
 * an emanating ray intersects the polygon an odd number of times.
 * drawproc should fill in pixels from xl to xr inclusive on scanline y,
 * e.g:
 *	drawproc(y, xl, xr)
 *	int y, xl, xr;
 *	{
 *	    int x;
 *	    for (x=xl; x<=xr; x++)
 *		pixel_write(x, y, pixelvalue);
 *	}
 *
 *  Paul Heckbert	30 June 81, 18 Dec 89
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "emap.hh"

struct Edge {	/* a polygon edge */
    double x;	/* x coordinate of edge's intersection with current scanline */
    double dx;	/* change in x with respect to y */
    int i;	/* edge number: edge i goes from pt[i] to pt[i+1] */
};

struct PolyInfo { /* info for poly drawing */
    int n;	  /* number of vertices */
    Point2 *pt;	  /* vertices */
    int nact;	  /* number of active edges */
    Edge *active; /* active edge list:edges crossing scanline y */
};

/* comparison routines for qsort */
static int compare_point(const void *u, const void *v) {return (*(Point2**)u)->y <= (*(Point2**)v)->y ? -1 : 1;}
static int compare_active(const void *u, const void *v) {return ((Edge*)u)->x <= ((Edge*)v)->x ? -1 : 1;}
void cinsert(PolyInfo &pi, int i, int y);
void cdelete(PolyInfo &pi, int i);

#define DRAW_POLY(func) \
    int k, y0, y1, y, i, j, xl, xr; \
    int width = this->width-1;\
    emap_t *pix;\
    emap_t *pixEnd;\
    PolyInfo pi;\
    pi.n = nvert;\
    pi.pt = point;\
    pi.active = (Edge*)malloc(pi.n*sizeof(Edge));\
    Point2 **ind = (Point2**)malloc(pi.n*sizeof(Point2*)); \
\
    for (i=0;i<pi.n;i++) /*set indices, for sort*/\
        ind[i]=pi.pt+i;\
\
    qsort(ind, pi.n, sizeof(Point2*), compare_point); /* sort indices */\
\
    pi.nact = 0;			/* start with empty active list */\
    k = 0;				/* ind[k] is next vertex to process */\
    y0 = (int)ceil(ind[0]->y-.5);		/* ymin of polygon */\
    if (y0 < 0) y0 = 0;\
    y1 = (int)floor(ind[pi.n-1]->y-.5);	/* ymax of polygon */\
    if (y1 > width) y1 = width;\
    for (y=y0; y<=y1; y++) {		/* step through scanlines */\
	/* scanline y is at y+.5 in continuous coordinates */\
\
	/* check vertices between previous scanline and current one, if any */\
	for (; k<pi.n && ind[k]->y<=y+.5; k++) {\
	    /* to simplify, if pt.y=y+.5, pretend it's above */\
	    /* invariant: y-.5 < pt[i].y <= y+.5 */\
	    i = ind[k]-pi.pt;	\
	    /*\
	     * insert or delete edges before and after vertex i (i-1 to i,\
	     * and i to i+1) from active list if they cross scanline y\
	     */\
	    j = i>0 ? i-1 : pi.n-1;	/* vertex previous to i */\
	    if (pi.pt[j].y <= y-.5)	/* old edge, remove from active list */\
		cdelete(pi, j);\
	    else if (pi.pt[j].y > y+.5)	/* new edge, add to active list */\
		cinsert(pi, j, y);\
	    j = i<pi.n-1 ? i+1 : 0;	/* vertex next after i */\
	    if (pi.pt[j].y <= y-.5)	/* old edge, remove from active list */\
		cdelete(pi, i);\
	    else if (pi.pt[j].y > y+.5)	/* new edge, add to active list */\
		cinsert(pi, i, y);\
	}\
\
	/* sort active edge list by active[j].x */\
	qsort(pi.active, pi.nact, sizeof(Edge), compare_active);\
\
	/* draw horizontal segments for scanline y */\
	for (j=0; j<pi.nact; j+=2) {	/* draw horizontal segments */\
	    /* span 'tween j & j+1 is inside, span tween j+1 & j+2 is outside */\
	    xl = (int)ceil(pi.active[j].x-.5);		/* left end of span */\
	    if (xl<0) xl = 0;\
	    xr = (int)floor(pi.active[j+1].x-.5);	/* right end of span */\
	    if (xr>width) xr = width;\
	    if (xl<=xr)\
	    {\
		func\
	    }\
	    pi.active[j].x += pi.active[j].dx;	 /* increment edge coords */\
	    pi.active[j+1].x += pi.active[j+1].dx;\
	}\
    }\
    free(pi.active);\
    free(ind);

inline void cdelete(PolyInfo &pi, int i)	/* remove edge i from active list */
{
    int j;

    for (j=0; j<pi.nact && pi.active[j].i!=i; j++);
    if (j>=pi.nact) return;	/* edge not in active list; happens at win->y0*/
    pi.nact--;
    bcopy(&pi.active[j+1], &pi.active[j], (pi.nact-j)*sizeof(Edge));
}

inline void cinsert(PolyInfo &pi, int i, int y)	/* append edge i to end of active list */
{
    int j;
    double dx;
    Point2 *p, *q;

    j = i<pi.n-1 ? i+1 : 0;
    if (pi.pt[i].y < pi.pt[j].y) {p = &pi.pt[i]; q = &pi.pt[j];}
    else		   {p = &pi.pt[j]; q = &pi.pt[i];}
    /* initialize x position at intersection of edge with scanline y */
    pi.active[pi.nact].dx = dx = (q->x-p->x)/(q->y-p->y);
    pi.active[pi.nact].x = dx*(y+.5-p->y)+p->x;
    pi.active[pi.nact].i = i;
    pi.nact++;
}

#define POLY_DRAW \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
*pix = g;

#define POLY_MIN \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
if (g < *pix)\
*pix = g;

#define POLY_MAX \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
if (g > *pix)\
*pix = g;

#define POLY_DRAWMAX \
pix = this->img + (y*this->width);\
pixEnd = pix + xr;\
pix += xl; \
for (;pix<=pixEnd;pix++)\
{\
if (*pix > max)\
max = *pix;\
*pix = g;\
}

#define POLY_AVG \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
total += *pix;\
area += (xr-xl)+1;

#define POLY_FRAC \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
if (*pix>thresh)\
total++;\
area += (xr-xl)+1;

#define POLY_INC \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
if (*pix < val) *pix -= val;

#define POLY_DEC \
pix = this->img + (y*this->width);\
pixEnd = pix + xr; \
pix += xl; \
for (;pix<=pixEnd;pix++)\
if (*pix > val) *pix -= val;

/********************************
         HERE ARE POLY FUNCS
*********************************/

void EMap::drawPolygon(int nvert, Point2 *point, emap_t g)
{
    if (nvert<3)
	return;

    DRAW_POLY(POLY_DRAW);
}

void EMap::drawminPolygon(int nvert, Point2 *point, emap_t g)
{
    if (nvert<3)
	return;

    DRAW_POLY(POLY_MIN);
}

void EMap::drawmaxPolygon(int nvert, Point2 *point, emap_t g)
{
    if (nvert<3)
	return;

    DRAW_POLY(POLY_MAX);
}

emap_t EMap::drawgetmaxPolygon(int nvert, Point2 *point, emap_t g)
{
    emap_t max = 0;
    if (nvert<3)
	return 0;

    DRAW_POLY(POLY_DRAW);

    return max;
}

double EMap::avgPolygon(int nvert, Point2 *point)
{
    uint64_t total = 0;
    int area = 0;

    if (nvert<3)
	return 0;

    DRAW_POLY(POLY_AVG);

    if (area==0)
	return 0;

    return ((double)(total))/area;
}

double EMap::fracPolygon(int nvert, Point2 *point, emap_t thresh)
{
    int total = 0;
    int area = 0;

    if (nvert<3)
	return 0;

    DRAW_POLY(POLY_FRAC);

    if (area==0)
	return 0;

    return ((double)(total))/area;
}

void EMap::incPolygon(int nvert, Point2 *point, emap_t val)
{
    if (nvert<3)
	return;

    val = -val;

    DRAW_POLY(POLY_INC);
}

void EMap::decPolygon(int nvert, Point2 *point, emap_t val)
{
    if (nvert<3)
	return;

    DRAW_POLY(POLY_DEC);
}
