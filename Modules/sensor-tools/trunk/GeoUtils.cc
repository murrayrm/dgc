#include "GeoUtils.hh"

#include <fstream>
#include <sstream>

// Load and average a set of scans from a file into sensor frame
int LoadAvgScans(const char *path, const char *name, tvert3 **scans)
{
    char filename[150];
    sprintf(filename, "%s/%s",path,name);
    FILE *file = fopen(filename,"r");
    if (!file)
    {
	fprintf(stderr,"ERROR: could not load file %s\n", filename);
	return -1;
    }
    int n=0;
    int count=0;
    float val[MAX_POINTS][2];
    double total[MAX_POINTS];

    while (!feof(file))
    {
	fread(&n,sizeof(int),1,file);
	fread(val,sizeof(float),n*2,file);
	for (int i=0; i<n; i++)
	    total[i] += val[i][1];
	count ++;
    }
    fclose(file);
    printf("Read %d scans of %d points\n",count,n);

    tvert3 *verts = new tvert3[n];

    for (int i=0;i<n;i++)
    {
	total[i] *= 1.0/count;
	verts[i].x = total[i]*cos(val[i][0]);
	verts[i].y = total[i]*sin(val[i][0]);
	verts[i].z = 0;
	verts[i].index = 0;
    }

    *scans = verts;

    return n;
}

// Load PTU Ladar scans into PTU frame
int LoadPTUScans(const char *path, tvert3 **scans, int maxpts)
{
    // first, set up ladar->ptu transform
    float ladar2tool[4][4];
    float tool2ptu[4][4];
    float ptu2veh[4][4];
    float ladar2veh[4][4];
    float ladar2ptu[4][4];

    setMatrix(LoadSensor(path,"SENSNET_PTU_LADAR"),ladar2tool);
    setMatrix(LoadSensor(path,"SENSNET_MF_PTU"),ptu2veh);

    char filename[150];

    sprintf(filename,"%s/SENSNET_PTU_LADAR",path);
    FILE *lfile = fopen(filename, "r");
    sprintf(filename,"%s/SENSNET_MF_PTU",path);
    FILE *pfile = fopen(filename, "r");

    if (lfile == NULL || pfile == NULL)
    {
	fprintf(stderr, "Error opening PTU files!\n");
	return -1;
    }

    vector<ptudata> ptuvals;
    ptudata pd;

    // load & count PTU blobs

    while (!feof(pfile))
    {
	if (fread(&pd,sizeof(pd),1,pfile))
	    ptuvals.push_back(pd);
    }

    fclose(pfile);

    // allocate points

    int n = ptuvals.size();
    int c;
    fread(&c,sizeof(int),1,lfile);
    fseek(lfile,0,SEEK_SET);

    int count = n*c;

    tvert3 *points;

    // use existing array, if given
    if (*scans != NULL && maxpts > 0 && maxpts <= count)
        points = *scans;
    else
	points = (tvert3*)malloc(count*sizeof(tvert3));

    tvert3 *pstart = points;

    printf("Read %d PTU values\n",n);

    vector<ptudata>::iterator pdi = ptuvals.begin();

    vertrpy ptu;
    ptu.x=ptu.y=ptu.z=ptu.roll=0;

    // load & transform points

    float val[c][2];

    for (int i=0;i<n;i++)
    {
	fread(&c,sizeof(int),1,lfile);
	fread(val,sizeof(float),c*2,lfile);

	// account for ptu state
	ptu.yaw = pdi->pan*M_PI/180;
	ptu.pitch = pdi->tilt*M_PI/180;

	setMatrix(ptu,tool2ptu);

	mat44f_mul(ladar2ptu, tool2ptu, ladar2tool);
	mat44f_mul(ladar2veh, ptu2veh, ladar2ptu);

	// transform points
	for (int j=0; j<c;j++)
	{
	    pstart->x = val[j][1]*cos(val[j][0]);
	    pstart->y = val[j][1]*sin(val[j][0]);
	    pstart->z = 0;
	    mulMatrix(pstart, ladar2veh);
	    pstart++;
	}

	pdi++;
    }

    fclose(lfile);

    *scans = points;

    return count;
}

// Load vehicle-sensor transform from .CFG
vertrpy LoadSensor(const char *path, const char *name)
{
    vertrpy sensor;
    char filename[150];
    char *ptr;

    sprintf(filename,"%s/%s.CFG",path,name);
    FILE *file = fopen(filename, "r");

    if (!file)
    {
	fprintf(stderr,"Could not read file %s\n",filename);
	return sensor;
    }
    while (!feof(file))
    {
	fgets(filename, 150, file);
	if (strlen(filename) < 8)
	    continue;
	if (strncmp(filename,"sens-pos",8) == 0)
	{
	    ptr = strchr(filename, '"');
	    if (ptr != NULL)
		sscanf(ptr+1,"%f,%f,%f",&sensor.x,&sensor.y,&sensor.z);
	}
	else if (strncmp(filename,"sens-rot",8) == 0)
	{
	    ptr = strchr(filename, '"');
	    if (ptr != NULL)
		sscanf(ptr+1,"%f,%f,%f",
		       &sensor.roll,&sensor.pitch,&sensor.yaw);
	}
    }
    
    return sensor;
}

// Write info to sensor .CFG
void SaveSensor(const char *path, const char *name, vertrpy sensor)
{
    char filename[150];
    char line[150];

    sprintf(filename,"%s/%s.CFG",path,name);

    ifstream src(filename);
    ostringstream buf(filename);
    
    if (!src.is_open())
    {
	fprintf(stderr,"Could not read file %s\n",filename);
	return;
    }

    while (src.good())
    {
	src.getline(line, 150);
	if (strncmp(line,"sens-pos",8) == 0)
	    buf << "sens-pos = \"" << sensor.x << ", " << sensor.y
		<< ", " << sensor.z << "\"" << endl;
	else if (strncmp(line,"sens-rot",8) == 0)
	    buf << "sens-rot = \"" << sensor.roll << ", " << sensor.pitch
		<< ", " << sensor.yaw << "\"" << endl;
	else
	    buf << line << endl;
    }
    src.close();

    ofstream dst(filename);
    if (!dst.good())
    {
	fprintf(stderr,"Could not write to sensor file!\n");
	return;
    }
    dst << buf.str();
    dst.close();
}

void CopyFile(const char *src, const char *dest)
{
    ifstream fsrc(src);
    ofstream fdest(dest);
    if (!fsrc.good() || !fdest.good())
    {
	fprintf(stderr, "File copy %s -> %s failed\n", src, dest);
	return;
    }
    fdest << fsrc.rdbuf();
    fsrc.close();
    fdest.close();
}

void CopyFile(const char *path, const char *src, const char *dest)
{
    char filename[150];
    sprintf(filename,"%s/%s",path,src);
    ifstream fsrc(filename);
    sprintf(filename,"%s/%s",path,dest);
    ofstream fdest(filename);
    if (!fsrc.good() || !fdest.good())
    {
	fprintf(stderr, "File copy %s -> %s failed\n",src,dest);
	return;
    }
    fdest << fsrc.rdbuf();
    fsrc.close();
    fdest.close();
}

// Transform points by offset & RPY
void TransformPoints(tvert3 *points, int count, vertrpy sensor)
{
    float mat[4][4];
    setMatrix(sensor,mat);
    for (int i=0;i<count;i++)
    {
	mulMatrix(points,mat);
	points++;
    }
}

int ReducePoints(tvert3 *points, int count, int newcount)
{
    int step = count/newcount;
    tvert3 *pread = points;
    tvert3 *pwrite = points;
    for (int i=0;i<newcount;i++)
    {
	*pwrite = *pread;
	pread += step;
	pwrite++;
    }

    return newcount;
}

int CopyReducePoints(tvert3 *points, int count, int newcount, tvert3 **newpoints)
{
    *newpoints = (tvert3*)malloc(newcount*sizeof(tvert3));

    int step = count/newcount;
    tvert3 *pread = points;
    tvert3 *pwrite = *newpoints;
    for (int i=0;i<newcount;i++)
    {
	*pwrite = *pread;
	pread += step;
	pwrite++;
    }

    return newcount;
}

float PlaneFit(vector<tvert3*> points, float plane[4])
{
    int n = points.size();
    if (n < 3)
    {
	printf("Attempting to fit less than three points!\n");
	return -1;
    }

    int size = n;

    if (n==3)
	size++;

    // create all necessary matrices
    float *a[size];
    float w[4];
    float *v[4];

    float *amem = (float*)malloc(4*size*sizeof(float));
    float *vmem = (float*)malloc(4*4*sizeof(float));

    // set matrix a
    for (int i=0;i<n;i++)
    {
	a[i] = amem + 4*i;
	a[i][0] = points[i]->x;
	a[i][1] = points[i]->y;
	a[i][2] = points[i]->z;
	a[i][3] = 1;
    }

    for (int i=0;i<4;i++)
	v[i] = vmem + 4*i;

    if (n==3)
    {
	a[3] = amem + 4*3;
	for (int i=0;i<4;i++)
	    a[3][i] = 0;
    }

    int m=0;
    float min = 100000;

    if (dsvd(a, size, 4, w, v) != 0)
    {
	for (int i=0;i<4;i++)
	{
	    if (w[i]<min)
	    {
		m = i;
		min = w[i];
	    }
	}
	for (int i=0;i<4;i++)
	    plane[i] = v[i][m];

    }

    free(amem);
    free(vmem);

    return min;
}

void PlaneGetSlice(float plane[4], tvert3 point, float width, tvert3 points[4])
{
    tvert3 n = tvert3(plane[0],plane[1],plane[2]);
    float sc = len3(n);
    n = scale3(n,1.0f/sc);
    // find a point on the plane
    tvert3 p = scale3(n, -plane[3]/sc);
    float dist = dot3(p-point, n);
//    printf("d: %f | dist: %f\n", plane[3]/sc, dist);
    // move point onto plane
    point = point + scale3(n, dist);

    tvert3 vec(1,0,0);
    tvert3 u = scale3(norm3(cross3(n,vec)),width);
    tvert3 v = cross3(n,u);

    points[0] = point+u+v;
    points[1] = point+u-v;
    points[2] = point-u+v;
    points[3] = point-u-v;
}

// Compute tree indices and quicksort points
void BuildTree(tvert3 *points, int count)
{
    // First, go through computing indices
    tvert3 *vert = points;

    printf("Computing indices... ");

    for (int i=0; i<count; i++)
    {
	vert->index = getIndex(vert);
	vert++;
    }
    printf("done.\n");
    printf("Sorting... ");
    // now, sort the tree
    quickPart(points, points, points + count - 1);
    printf("done.\n");
}

void quickPart(tvert3 *points, tvert3 *start, tvert3 *end)
{
    if (start < end)
    {
	tvert3 pivot = *end;
	tvert3 *i = start;
	tvert3 *j = end;
	while (i != j)
	{
	    if (i->index <= pivot.index)
	    {
		i++;
	    }
	    else
	    {
		*j = *i;
		*i = *(j-1);
		j--;
	    }
	}
	*j = pivot;
	if (pivot.index != -1) // all lower points have equal index
	    quickPart(points, start, j-1);
	quickPart(points, j+1, end);
    }
}

vector<tvert3*> GetNearbyPoints(tvert3 point, tvert3 *points, int count, float radius)
{
    int i=0;
    vector<tvert3*> verts;
    point.index = getIndex(&point);

    if (point.index == -1)
	return verts;

    // split up index into coords
    int x=0, y=0, z=0;
    int ind = point.index;
    int mask = 1;
    for (i=0; i<TREE_DEPTH;i++)
    {
	z |= (mask&ind);
	y |= (mask&(ind>>1));
	x |= (mask&(ind>>2));
	mask <<= 1;
	ind >>= 2;
    }

    // side length of a single cell
    float cellrad = TREE_SIZE / (float)(1 << TREE_DEPTH);
    int lim = (int)round(radius/cellrad) + 1;
    tvert3 *pt;

    // search nearby cells for close points
    for (int ix=x-lim; ix<=x+lim; ix++)
    {
	for (int iy=y-lim; iy<=y+lim; iy++)
	{
	    for (int iz=z-lim; iz<=z+lim; iz++)
	    {
		ind = makeIndex(ix,iy,iz);
		pt = getCellPoint(ind,points,count);
		if (pt == NULL)
		    continue;
		while (pt < (points+count) && pt->index == ind)
		{
		    if (dist(pt,&point) < radius)
			verts.push_back(pt);
		    pt++;
		}
	    }
	}
    }

    return verts;
}

vector<tvert3*> GetNearbyPointsLin(tvert3 point, tvert3 *points, int count, float radius)
{
    float xmin, xmax, ymin, ymax, zmin, zmax;
    xmin = point.x - radius;
    xmax = point.x + radius;
    ymin = point.y - radius;
    ymax = point.y + radius;
    zmin = point.z - radius;
    zmax = point.z + radius;
    vector<tvert3*> pts;

    for (int i=0; i<count; i++)
    {
	if (points->x > xmin && points->x < xmax &&
	    points->y > ymin && points->y < ymax &&
	    points->z > zmin && points->z < zmax &&
	    dist(&point,points) <= radius)
	    pts.push_back(points);
	points++;
    }

    return pts;
}

tvert3 * getCellPoint(int index, tvert3 *points, int count)
{
    int low = 0;
    int high = count - 1;
    int mid;
    while (low < high)
    {
	mid = (low + high)/2;
	if (points[mid].index < index)
	    low = mid + 1;
	else
	    high = mid;
    }
    if (low >= count || points[low].index != index)
	return NULL; //not found

    tvert3 *pt = points + low;
    while (pt >= points)
    {
	// just passed beginning of list
	if (pt->index != index)
	    return pt+1;
	pt--;
    }
    return NULL;
}

inline int getIndex(tvert3 *vert)
{
    float x,y,z,td;
    int ind, j;

    td = 0.5f*TREE_SIZE;
    if (vert->x > td || vert->x < -td || 
	vert->y > td || vert->y < -td || 
	vert->z > td || vert->z < -td)
    {
	return -1;
    }
    x = vert->x;
    y = vert->y;
    z = vert->z;
    ind = 0;
    for (j=0;j<TREE_DEPTH;j++)
    {
	td *= 0.5f;
	ind <<= 3;
	
	if (x > 0) 
	{
	    ind |= 4;
	    x -= td;
	}
	else
	{
	    x += td;
	}
	if (y > 0)
	{
	    ind |= 2;
	    y -= td;
	}
	else
	{
	    y += td;
	}
	if (z > 0)
	{
	    ind |= 1;
	    z -= td;
	}
	else
	{
	    z += td;
	}
    }

    return ind;
}

inline int makeIndex(int x, int y, int z)
{
    int ind = 0;
    x <<= 2             ;
    y <<= 1;
    int mask = 1;

    for (int i=0;i<TREE_DEPTH;i++)
    {
	ind |= (mask&z);
	ind |= ((mask<<1)&y);
	ind |= ((mask<<2)&x);
	mask <<=3;
	x <<= 2;
	y <<= 2;
	z <<= 2;
    }

    return ind;
}

inline void setMatrix(vertrpy sensor, float matrix[4][4])
{
    pose3_t pose;
    pose.pos = vec3_set(sensor.x, sensor.y, sensor.z);
    pose.rot = quat_from_rpy(sensor.roll, sensor.pitch, sensor.yaw);
    pose3_to_mat44f(pose, matrix);
}

inline void mulMatrix(tvert3 *vert, float matrix[4][4])
{
    tvert3 outvert;
    outvert.x = matrix[0][0]*vert->x + matrix[0][1]*vert->y +
        matrix[0][2]*vert->z + matrix[0][3];
    outvert.y = matrix[1][0]*vert->x + matrix[1][1]*vert->y +
        matrix[1][2]*vert->z + matrix[1][3];
    outvert.z = matrix[2][0]*vert->x + matrix[2][1]*vert->y +
        matrix[2][2]*vert->z + matrix[2][3];  
    outvert.index = vert->index;
    *vert = outvert;
}

inline float dist(tvert3 *p1, tvert3 *p2)
{
    return sqrt((p1->x-p2->x)*(p1->x-p2->x) + 
		(p1->y-p2->y)*(p1->y-p2->y) + 
		(p1->z-p2->z)*(p1->z-p2->z));
}

inline tvert3 cross3(tvert3 a, tvert3 b)
{
    tvert3 v;
    v.x = (a.y*b.z-a.z*b.y);
    v.y = (a.z*b.x-a.x*b.z);
    v.z = (a.x*b.y-a.y*b.x);
    return v;
}

inline float dot3(tvert3 a, tvert3 b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline tvert3 norm3(tvert3 n)
{
    float l = len3(n);
    if (l > 0.00001f)
	return scale3(n,1.0f/l);
    else
	return n;
}

inline tvert3 scale3(tvert3 v, float s)
{
    v.x *= s; v.y *= s; v.z *= s;
    return v;
}

inline float len3(tvert3 v)
{
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

float PlaneDist(tvert3 v, float plane[4])
{
    // make normal vector
    tvert3 n = tvert3(plane[0],plane[1],plane[2]);
    float sc = len3(n);
    n = scale3(n,1.0f/sc);
    // find a point on the plane
    tvert3 p = scale3(n, -plane[3]/sc);
    return dot3(p-v, n);
}

tvert3 PlaneNormal(float plane[4])
{
    return norm3(tvert3(plane[0],plane[1],plane[2]));
}
