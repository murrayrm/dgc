/**********************************************************
 *
 * Captures ladar points from sensnet, as commanded, storing
 * them to a file. Also commands pan-tilt unit.
 *
 **********************************************************/

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/PTUStateBlob.h>
#include <interfaces/PTUCommand.h>
#include <dgcutils/DGCutils.hh>
#include <dgcutils/cfgfile.h>
#include <frames/pose3.h>
#include <frames/coords.hh>


#ifndef CAPTUREPOINTS_HH
#define CAPTUREPOINTS_HH

#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <sys/stat.h>

using namespace std;

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define PTU_ANG_THRESH 0.1f //degrees
#define PTU_VEL_THRESH 0.01f //deg/sec

class CapturePoints
{  
public:
    // Constructor
    CapturePoints();
    
    // Destructor
    ~CapturePoints();
        
    // Update message stats
    int update();

    // Prints active ladars to cout
    void printActive();

    // Begin recording points
    void startCapture();

    // End recording points
    void stopCapture();

    // Return path to log associated with object
    char *getLogPath();

    // Commands the PTU to a position, optionally blocking
    // until we reach the position
    int commandPTU(float pan, float tilt, float panspeed,
		    float tiltspeed, bool blocking);
    
private:
    // Initialize sensnet
    int initSensnet();
    
    // Clean up sensnet
    int finiSensnet();

    // Open /tmp/logs/ladar-calib/* for write
    void openLogs();

    // Close logs
    void closeLogs();

    // Write a single scan to our log files
    void saveScan(int id, LadarRangeBlob *blob);

    // Are we capturing?
    bool capturing;

    char logpath[100];

    // Individual ladar info
    struct LadarInfo
    {
	sensnet_id_t sensorId; // Sensor id
	int blobId; // Last blob id for this ladar
	int blobCount; // Number of blobs recvd
	FILE *log; // Address of log file
    };

    int numLadars;
    LadarInfo ladars[16];

    LadarInfo ptu;
    PTUStateBlob ptublob;

    // Spread settings
    char *spreadDaemon;
    int skynetKey;
    modulename moduleId;
    
    // Sensnet module
    sensnet_t *sensnet;
};
#endif


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

