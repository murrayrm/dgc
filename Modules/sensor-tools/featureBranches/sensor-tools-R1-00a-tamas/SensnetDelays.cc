/**********************************************************
 **
 **  SENSNETDELAYS.CC
 **
 **    Time-stamp: <2007-08-17 11:47:54 tamas> 
 **
 **    Author: Tamas Szalay
 **    Created: Wed Aug  17 09:15:25 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "SensnetDelays.hh"

// Default constructor
SensnetDelays::SensnetDelays()
{
    memset(this, 0, sizeof(*this));
    return;
}


// Default destructor
SensnetDelays::~SensnetDelays()
{
}


// Parse the command line
int SensnetDelays::parseCmdLine(int argc, char **argv)
{
    // Load options
    if (cmdline_parser(argc, argv, &this->options) < 0)
	return -1;
    
    // Fill out the spread name
    if (this->options.spread_daemon_given)
	this->spreadDaemon = this->options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
	this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
	return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
    
    // Fill out the skynet key
    if (this->options.skynet_key_given)
	this->skynetKey = this->options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
	this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
	this->skynetKey = 0;

    if (this->options.recv_subgroup_given)
        this->recvSubGroup = this->options.recv_subgroup_arg;

    if (this->options.force_delay_given)
        this->forceDelay = this->options.force_delay_arg;

    // Fill out module id
    this->moduleId = modulenamefromString(this->options.module_id_arg);
    if (this->moduleId <= 0)
	return ERROR("invalid module id: %s", this->options.module_id_arg);
    
    // If the user gives us some log files on the command line, run in
    // replay mode.
/*  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
    else
    this->mode = modeLive;*/
    
    return 0;
}



// Initialize sensnet
int SensnetDelays::initSensnet()
{
    int i=0;
    
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
	return -1;
    
    // Set up sensors
    msgInfo[i++].sensorId = SENSNET_SKYNET_SENSOR; // For AState messages
    msgInfo[i++].sensorId = SENSNET_MF_PTU;
    msgInfo[i++].sensorId = SENSNET_MF_BUMPER_LADAR;
    msgInfo[i++].sensorId = SENSNET_RF_BUMPER_LADAR;
    msgInfo[i++].sensorId = SENSNET_LF_BUMPER_LADAR;
    msgInfo[i++].sensorId = SENSNET_RF_ROOF_LADAR;
    msgInfo[i++].sensorId = SENSNET_LF_ROOF_LADAR;
    msgInfo[i++].sensorId = SENSNET_REAR_BUMPER_LADAR;
    msgInfo[i++].sensorId = SENSNET_PTU_LADAR;
    msgInfo[i++].sensorId = SENSNET_RIEGL;
    msgInfo[i++].sensorId = SENSNET_RADAR;
    msgInfo[i++].sensorId = SENSNET_PTU_RADAR;

    this->mapTalker = new CMapElementTalker();
    this->mapTalker->initRecvMapElement(this->skynetKey,this->recvSubGroup);

    // Join vehicle state
    if (sensnet_join(this->sensnet, msgInfo[0].sensorId,
		     SNstate, sizeof(VehicleState)) != 0)
	return ERROR("unable to join %d", msgInfo[0].sensorId);

    // Join PTU group
    if (sensnet_join(this->sensnet, msgInfo[1].sensorId,
		     SENSNET_PTU_STATE_BLOB, sizeof(PTUStateBlob)) != 0)
	return ERROR("unable to join %d", msgInfo[1].sensorId);

    // Join Ladar groups
    for (i = 2; i < 10; i++)
    {
	if (sensnet_join(this->sensnet, msgInfo[i].sensorId,
			 SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
	    return ERROR("unable to join %d", msgInfo[i].sensorId);
    }

    // Join radar groups
    for (i = 10; i < 12; i++)
    {
	if (sensnet_join(this->sensnet, msgInfo[i].sensorId,
			 SENSNET_RADAR_BLOB, sizeof(RadarRangeBlob)) != 0)
	    return ERROR("unable to join %d", msgInfo[i].sensorId);
    }
    
    return 0;
}


// Clean up sensnet
int SensnetDelays::finiSensnet()
{
    int i=0;
    if (this->sensnet)
    {
	sensnet_leave(this->sensnet, msgInfo[0].sensorId, SNstate);
	sensnet_leave(this->sensnet, msgInfo[1].sensorId, SENSNET_PTU_STATE_BLOB);
	for (i = 2; i < 10; i++)
	    sensnet_leave(this->sensnet, msgInfo[i].sensorId, SENSNET_LADAR_BLOB);
	for (i = 10; i < 12; i++)
	    sensnet_leave(this->sensnet, msgInfo[i].sensorId, SENSNET_RADAR_BLOB);

	sensnet_free(this->sensnet);
	this->sensnet = NULL;
    }
    
    return 0;
}


// Update the map with new range data
int SensnetDelays::update()
{
    int i;
    int blobId, blobLen;
    LadarRangeBlob lblob;
    RadarRangeBlob rblob;
    PTUStateBlob pblob;
    VehicleState vstate;
    
//    if (sensnet_wait(this->sensnet, 200) != 0)
//	return 0;
    i = 0;
    if (sensnet_peek(this->sensnet, msgInfo[i].sensorId, SNstate, &blobId, &blobLen) == 0)
    	if (blobId > msgInfo[i].lastMsgId)
	    if (sensnet_read(this->sensnet, msgInfo[i].sensorId, SNstate, 
			     &blobId, sizeof(vstate), &vstate) == 0)
		updateMsg(i,blobId,vstate.timestamp,vstate.timestamp);
    i = 1;
    // read off PTU State
    if (sensnet_peek(this->sensnet, msgInfo[i].sensorId, SENSNET_PTU_STATE_BLOB, &blobId, &blobLen) == 0)
    	if (blobId > msgInfo[i].lastMsgId)
	    if (sensnet_read(this->sensnet, msgInfo[i].sensorId, SENSNET_PTU_STATE_BLOB, 
			     &blobId, sizeof(pblob), &pblob) == 0)
		updateMsg(i,blobId,pblob.timestamp,pblob.state.timestamp);

    for (i = 2; i < 10; i++)
    {
	if (sensnet_peek(this->sensnet, msgInfo[i].sensorId, SENSNET_LADAR_BLOB, &blobId, &blobLen) == 0)
	    if (blobId > msgInfo[i].lastMsgId)
		if (sensnet_read(this->sensnet, msgInfo[i].sensorId, SENSNET_LADAR_BLOB, 
				 &blobId, sizeof(lblob), &lblob) == 0)
		    updateMsg(i,blobId,lblob.timestamp,lblob.state.timestamp);
    }

    for (i = 10; i < 12; i++)
    {
	if (sensnet_peek(this->sensnet, msgInfo[i].sensorId, SENSNET_RADAR_BLOB, &blobId, &blobLen) == 0)
	    if (blobId > msgInfo[i].lastMsgId)
		if (sensnet_read(this->sensnet, msgInfo[i].sensorId, SENSNET_RADAR_BLOB, 
				 &blobId, sizeof(rblob), &rblob) == 0)
		    updateMsg(i,blobId,rblob.timestamp,rblob.state.timestamp);
    }

    // get mapelements
    while (mapTalker->recvMapElementNoBlock(&sensedEl,this->recvSubGroup) > 0) {
        mapElInfo.msgCount++;
	mapElInfo.lastMsgTime = sensedEl.timestamp;
        sensedEl.timestamp = DGCgettime();
    }

    if ((DGCgettime() - this->lastSecond) > 1000000)
    {
	updateSecond();
	this->lastSecond += 1000000;
	this->updateRes = this->updateTicks;
	this->updateTicks = 0;
    }

    if ((DGCgettime() - this->lastConsole) > 100000)
    {
	updateConsole();
	this->lastConsole = DGCgettime();
    }

    if (this->forceDelay > 0)
      usleep(this->forceDelay);

    this->updateTicks++;
    
    return 0;
}

void SensnetDelays::updateMsg(int id, int blobId, uint64_t timestamp, uint64_t vsTimestamp)
{
    int delay = (int)(DGCgettime() - timestamp)/1000;
    msgInfo[id].latencyThisSecond += delay;

    msgInfo[id].msgThisSecond++;
    msgInfo[id].msgCount++;

    if (this->options.use_recv_flag)
	timestamp = DGCgettime();

    if (timestamp > msgInfo[id].lastMsgTime)
    {
	delay = (int)(timestamp - msgInfo[id].lastMsgTime)/1000;
    }
    else
    {
	delay = -(int)(msgInfo[id].lastMsgTime-timestamp)/1000;
	ERROR("%d: Blobs arrived out of order in sensor %-20s\n", blobId,
	      sensnet_id_to_name(msgInfo[id].sensorId));
    }

    delay = delay / (blobId - msgInfo[id].lastMsgId);

    msgInfo[id].delayThisSecond += delay;

    if (msgInfo[id].firstId == 0) // our very first blob!
    {
	msgInfo[id].firstId = blobId;
	msgInfo[id].firstTime = timestamp;
    }

    msgInfo[id].devSqThisSecond += (delay-msgInfo[id].lastDelay)*(delay-msgInfo[id].lastDelay);

    if (delay > msgInfo[id].maxDelayThisSecond)
	msgInfo[id].maxDelayThisSecond = delay;
    else if (delay < msgInfo[id].minDelayThisSecond)
	msgInfo[id].minDelayThisSecond = delay;

    if (vsTimestamp > msgInfo[id].lastVSTime) // new veh state message
    {
	msgInfo[id].vsThisSecond++;
	msgInfo[id].lastVSTime = vsTimestamp;
    }

    if (timestamp>vsTimestamp)
    {
	msgInfo[id].vsDelayThisSecond += (int)(timestamp-vsTimestamp)/1000;
    }
    else
    {
	msgInfo[id].vsDelayThisSecond += (int)(vsTimestamp-timestamp)/1000;
	//ERROR("%d: vehicle state delay < 0 in sensor %-20s\n", blobId,
//	      sensnet_id_to_name(msgInfo[id].sensorId));
    }

    msgInfo[id].lastMsgTime = timestamp;
    msgInfo[id].lastMsgId = blobId;
}

void SensnetDelays::updateSecond()
{
    for (int i=0;i<MSG_COUNT;i++)
    {
	if (msgInfo[i].msgThisSecond == 0)
	    continue;

	int n = msgInfo[i].msgThisSecond;
	
	msgInfo[i].lastCount = msgInfo[i].msgThisSecond;
	msgInfo[i].lastDelay = msgInfo[i].delayThisSecond / n;
	msgInfo[i].lastVariance = (msgInfo[i].maxDelayThisSecond - msgInfo[i].minDelayThisSecond) / 2;
	msgInfo[i].lastVsCount = msgInfo[i].vsThisSecond;
	msgInfo[i].lastVsDelay = msgInfo[i].vsDelayThisSecond / n;
	msgInfo[i].lastLatency = msgInfo[i].latencyThisSecond / n;

	msgInfo[i].msgThisSecond = 0;
	msgInfo[i].devSqThisSecond = 0;
	msgInfo[i].latencyThisSecond = 0;
	msgInfo[i].delayThisSecond = 0;
	msgInfo[i].vsThisSecond = 0;
	msgInfo[i].vsDelayThisSecond = 0;
	msgInfo[i].maxDelayThisSecond = -10000;
	msgInfo[i].minDelayThisSecond = 10000;
    }
}

void SensnetDelays::updateConsole()
{
    if (!this->console)
	return;
    
    char token[64];

    for (int i=0;i<MSG_COUNT;i++)
    {
	snprintf(token, sizeof(token), "%%sens%d%%", i);
	if (msgInfo[i].msgCount == 0)
	{
	    cotk_printf(this->console, token, A_NORMAL, 
			"%-25s: No messages received!",
			sensnet_id_to_name(msgInfo[i].sensorId));
	    continue;
	}

	int nMissed = (msgInfo[i].lastMsgId - msgInfo[i].firstId) - msgInfo[i].msgCount;
	
	cotk_printf(this->console, token, A_NORMAL, 
		    "%-25s: %7d tot %5d missed %3d Hz | Spacing:%3d +- %3d ms | Latency:%3d ms | VS:%3d del. %3d ms                  ",
		    sensnet_id_to_name(msgInfo[i].sensorId), msgInfo[i].msgCount, nMissed, msgInfo[i].lastCount,
		    msgInfo[i].lastDelay, msgInfo[i].lastVariance, msgInfo[i].lastLatency,
		    msgInfo[i].lastVsCount, msgInfo[i].lastVsDelay);
    }
    cotk_printf(this->console, "%mapel%", A_NORMAL, 
		"MapElements received: %d", this->mapElInfo.msgCount);
    cotk_printf(this->console, "%res%", A_NORMAL, "%6d Hz", this->updateRes);
}


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"SensnetDelays                                                              \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"%sens0%                                                                    \n"
"%sens1%                                                                    \n"
"%sens2%                                                                    \n"
"%sens3%                                                                    \n"
"%sens4%                                                                    \n"
"%sens5%                                                                    \n"
"%sens6%                                                                    \n"
"%sens7%                                                                    \n"
"%sens8%                                                                    \n"
"%sens9%                                                                    \n"
"%sens10%                                                                   \n"
"%sens11%                                                                   \n"
"%sens12%                                                                   \n"
"                                                                           \n"
"%mapel%                                                                    \n"
"                                                                           \n"
"   %res%                                                                   \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int SensnetDelays::initConsole()
{
    // Initialize console
    this->console = cotk_alloc();
    assert(this->console);
    
    // Set the console template
    cotk_bind_template(this->console, ::consoleTemplate);
    
    // Bind buttons and toggles
    cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
		     (cotk_callback_t) onUserQuit, this);
    cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
		     (cotk_callback_t) onUserPause, this);
    
    // Initialize the display
    cotk_open(this->console,NULL);
    
    // Display some fixed values
    cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
		this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
    
    return 0;
}


// Finalize sparrow display
int SensnetDelays::finiConsole()
{
    // Clean up the CLI
    if (this->console)
    {
	cotk_close(this->console);
	cotk_free(this->console);
	this->console = NULL;
    }
    
    return 0;
}


// Handle button callbacks
int SensnetDelays::onUserQuit(cotk_t *console, SensnetDelays *self, const char *token)
{
    MSG("user quit");
    self->quit = true;
    return 0;
}


// Handle button callbacks
int SensnetDelays::onUserPause(cotk_t *console, SensnetDelays *self, const char *token)
{
    self->pause = !self->pause;
    MSG("pause %s", (self->pause ? "on" : "off"));
    return 0;
}


// Main program thread
int main(int argc, char **argv)
{
    SensnetDelays *delays;
    
    delays = new SensnetDelays();
    assert(delays);
    
    // Parse command line options
    if (delays->parseCmdLine(argc, argv) != 0)
	return -1;
    
    // Initialize sensnet
    if (delays->initSensnet() != 0)
	return -1;
    
    // Initialize cotk display
    if (!delays->options.disable_console_flag)
	if (delays->initConsole() != 0)
	    return -1;
    
    
    fprintf(stderr, "entering main thread of SensnetDelays \n");

    delays->lastSecond = DGCgettime();
    
    while (!delays->quit)
    {
	// Update the console
	if (delays->console)
	    cotk_update(delays->console);
	
	// If we are paused, dont do anything
	if (delays->pause)
	{
	    usleep(50000);
	    continue;
	}
	
	// Get new data and update our perceptor
	if (delays->update() != 0)
	    break;
    }
    
    if (delays->console)
	delays->finiConsole();
    delays->finiSensnet();
    
    MSG("exited cleanly");
    
    return 0;
}

