#ifndef _GEOUTILS_HH
#define _GEOUTILS_HH

#include <stdio.h>
#include <cmath>
#include <cstring>
#include <iostream>

#include <vector>

#include <frames/pose3.h>
#include <frames/mat44.h>

#include "svd.h"

using namespace std;

struct tvert3
{
    float x;
    float y;
    float z;
    int index; // the cell coordinate
    
    tvert3()
    { x = y = z = index = 0; }

    tvert3(float x, float y, float z)
    { this->x = x; this->y = y; this->z = z;}

    tvert3 operator+ (const tvert3& v)
    { return tvert3(v.x+x,v.y+y,v.z+z); }

    tvert3 operator- (const tvert3& v)
    { return tvert3(x-v.x,y-v.y,z-v.z); }
};

struct vertrpy
{
    float x;
    float y;
    float z;
    float roll;
    float pitch;
    float yaw;
};

struct ptudata
{
    float pan;
    float tilt;
    float panspeed;
    float tiltspeed;
};

// the max number of ladar points
#define MAX_POINTS 250

// depth of our tree (not really a tree)
#define TREE_DEPTH 8
// size of our tree (in each dimension)
#define TREE_SIZE 50

// Load and average a set of scans from a file into sensor frame
int LoadAvgScans(const char *path, const char *name, tvert3 **scans);

// Load PTU Ladar scans into PTU frame
int LoadPTUScans(const char *path, tvert3 **scans, int maxpts = 0);

// Load vehicle-sensor transform from .CFG
vertrpy LoadSensor(const char *path, const char *name);

// Write vehicle-sensor transform to .CFG
void SaveSensor(const char *path, const char *name, vertrpy sensor);

// Copy a file from one to another using full paths
void CopyFile(const char *src, const char *dest);

// Copy a file from one to another in a given directory
void CopyFile(const char *path, const char *src, const char *dest);

// Transform points by offset & RPY
void TransformPoints(tvert3 *points, int count, vertrpy sensor);

// Reduce points by removing points along the way
int ReducePoints(tvert3 *points, int count, int newcount);

// Reduce points by removing points along the way into a new array
int CopyReducePoints(tvert3 *points, int count, int newcount, tvert3 **newpoints);

// Perform least-squares fit on plane
float PlaneFit(vector<tvert3*> points, float plane[4]);

// Returns a small square from the plane centered at point
void PlaneGetSlice(float plane[4], tvert3 point, float width, tvert3 points[4]);

// Foo barr
void BuildTree(tvert3 *points, int count);

// Quicksort stuff
void quickPart(tvert3 *points, tvert3 *start, tvert3 *end);

// Find points within a radius using a tree
vector<tvert3*> GetNearbyPoints(tvert3 point, tvert3 *points, int count, float radius, float *nearest = NULL);

// Linearly find points within a radius, if we don't have a tree
vector<tvert3*> GetNearbyPointsLin(tvert3 point, tvert3 *points, int count, float radius);

// Get first point in a cell
tvert3 * getCellPoint(int index, tvert3 *points, int count);

// Get tree index
int getIndex(tvert3 *point);

// Make tree index from partial x,y,z indices
int makeIndex(int x, int y, int z);

// tvert3 distance
float dist(tvert3 *p1, tvert3 *p2);

// set transform matrix
void setMatrix(vertrpy sensor, float matrix[4][4]);

// multiply tvert3 by matrix
void mulMatrix(tvert3 *vert, float matrix[4][4]);

// cross product
tvert3 cross3(tvert3 a, tvert3 b);

// dot product
float dot3(tvert3 a, tvert3 b);

// normalize
tvert3 norm3(tvert3 n);

// scale
tvert3 scale3(tvert3 v, float s);

// length
float len3(tvert3 v);

// get distance from vertex to plane
float PlaneDist(tvert3 v, float plane[4]);

// get normal vector of plane
tvert3 PlaneNormal(float plane[4]);

#endif
