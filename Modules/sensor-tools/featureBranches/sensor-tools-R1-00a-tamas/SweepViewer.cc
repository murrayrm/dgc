#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define GL_GLEXT_PROTOTYPES
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <GL/glext.h>
#endif

#include <sys/types.h>
#include <string.h>
#include <vector>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>

#include "GeoUtils.hh"

using namespace std;

char *currPath;

int drag_x0, drag_y0;
int lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;

int width, height; //window size

float camx; //camera angle x in rad
float camy; //camera angle y in rad
float camdist; //camera distance
bool camcenter; //centered on origin?

bool fullWorld; //draw full world?
tvert3 *redworldpoints;
int redworldcount;
tvert3 *fullworldpoints;
int fullworldcount;

#define NUM_LADARS 7
tvert3 *ladars[NUM_LADARS];
int ladarcount[NUM_LADARS];
bool showladar[NUM_LADARS];
char *ladarname[NUM_LADARS] = {"SENSNET_MF_BUMPER_LADAR",
			       "SENSNET_LF_BUMPER_LADAR",
			       "SENSNET_RF_BUMPER_LADAR",
			       "SENSNET_LF_ROOF_LADAR",
			       "SENSNET_RF_ROOF_LADAR",
			       "SENSNET_REAR_BUMPER_LADAR",
			       "SENSNET_RIEGL"};

bool editMode;
int editLadar;
vertrpy editSensor;
float editFac;
tvert3 editPoints[4]; //ladar display

tvert3 testpoint;
float testrad;

// Save ROI to ground or ladar file?
bool groundROI;
int curROI;

bool drawGround;

// camera pan rate
#define CAM_RATE 0.01f
// camera zoom rate
#define CAM_ZOOM 0.1f
// max camera Y
#define CAM_YLIM 1.5f

void drawString (char *s)
{
    unsigned int i;
    for (i = 0; i < strlen (s); i++)
	glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
}

void drawVertices(tvert3 * verts, int count)
{
    glVertexPointer(3, GL_FLOAT, sizeof(tvert3), verts);
    glDrawArrays(GL_POINTS,0,count);
}

void drawVertices(vector<tvert3*> verts)
{
    glBegin(GL_POINTS);
    vector<tvert3*>::iterator itr;
    for (itr = verts.begin(); itr != verts.end(); itr++)
	glVertex3f((*itr)->x,(*itr)->y,(*itr)->z);
    glEnd();
}

void drawLadars()
{
    for (int i=0; i<NUM_LADARS;i++)
    {
	if (!showladar[i])
	    continue;	
	glColor4f(0.6f,0.6f,1,1);
	glPointSize(3);
	glBegin(GL_POINTS);
	for (int j=0;j<ladarcount[i];j++)
	    glVertex3f(ladars[i][j].x,ladars[i][j].y,ladars[i][j].z);
	glEnd();
    }
}

void drawEdit()
{
    // draw scan points
    glColor4f(1.0f,0.2f,0.2f,1);
    glPointSize(3);
    glBegin(GL_POINTS);
    for (int j=0;j<ladarcount[editLadar];j++)
	glVertex3f(ladars[editLadar][j].x,ladars[editLadar][j].y,ladars[editLadar][j].z);
    glEnd();
    // draw ladar
    glBegin(GL_LINES);
    for (int i=1;i<4;i++)
    {
	glVertex3f(editPoints[0].x,editPoints[0].y,editPoints[0].z);
	glVertex3f(editPoints[i].x,editPoints[i].y,editPoints[i].z);
    }
    glEnd();
    // draw strings
    glWindowPos2i(10,20);
    char str[100];
    sprintf(str,"Editing: %s | Factor: %f",ladarname[editLadar],editFac);
    drawString(str);
}

void drawWorld(void)
{
    glColor3f(1,1,1);
    glPointSize(1);
    if (fullWorld)
	drawVertices(fullworldpoints,fullworldcount);
    else
	drawVertices(redworldpoints,redworldcount);

    glColor3f(1,0,0);
    glPointSize(2);
    vector<tvert3*> nearpts = GetNearbyPoints(testpoint, fullworldpoints, fullworldcount, testrad);
    drawVertices(nearpts);

    glColor4f(0.5f,0.5f,1,0.5f);
    float plane[4];
    tvert3 ppoints[4];
    if (nearpts.size() > 2)
    {
	PlaneFit(nearpts, plane);
	PlaneGetSlice(plane, testpoint, 0.2f, ppoints);
	glVertexPointer(3, GL_FLOAT, sizeof(tvert3), ppoints);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	glBegin(GL_LINES);
	glVertex3f(testpoint.x,testpoint.y,testpoint.z);
	glVertex3f(testpoint.x+plane[0],testpoint.y+plane[1],testpoint.z+plane[2]);
	glEnd();
    }

    glColor3f(0,1,0);
    glPointSize(6);
    glBegin(GL_POINTS);
    glVertex3f(testpoint.x,testpoint.y,testpoint.z);
    glEnd();

    drawLadars();
    if (editMode)
    {
	drawEdit();
    }
    else
    {
	glWindowPos2i(10,20);
	if (groundROI)
	    drawString("ROI Mode: Ground");
	else
	    drawString("ROI Mode: Ladar");
	glWindowPos2i(10,10);
	char str[100];
	sprintf(str,"Test Point: %f %f %f",testpoint.x,testpoint.y,testpoint.z);
	drawString(str);
    }

    if (drawGround)
    {
	glColor4f(0.4f,1,0.4f,0.4f);
	glBegin(GL_TRIANGLE_STRIP);
	glVertex3f(-5,-5,VEHICLE_TIRE_RADIUS);
	glVertex3f(5,-5,VEHICLE_TIRE_RADIUS);
	glVertex3f(-5,5,VEHICLE_TIRE_RADIUS);
	glVertex3f(5,5,VEHICLE_TIRE_RADIUS);
	glEnd();
    }
}

void setCamera(void)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    if (camcenter)
	gluLookAt(cos(-camy)*cos(-camx)*camdist,cos(-camy)*sin(-camx)*camdist,sin(-camy)*camdist,
		  0, 0, 0, 0, 0, -1); //negatives for our screwy coord system
    else
	gluLookAt(cos(-camy)*cos(-camx)*camdist+testpoint.x,cos(-camy)*sin(-camx)*camdist+testpoint.y,
		  sin(-camy)*camdist+testpoint.z, testpoint.x, testpoint.y, testpoint.z, 
		  0, 0, -1); //negatives for our screwy coord system
}

void saveROI()
{
    char filename[150];
    if (groundROI)
	sprintf(filename,"%s/GROI",currPath);
    else
	sprintf(filename,"%s/LROI",currPath);
    FILE *file = fopen(filename,"a");
    if (file == NULL)
	return;
    fprintf(file,"%f %f %f %f\n",testpoint.x,testpoint.y,testpoint.z,testrad);
    fclose(file);
}

void nextROI()
{
    char filename[150];
    if (groundROI)
	sprintf(filename,"%s/GROI",currPath);
    else
	sprintf(filename,"%s/LROI",currPath);
    vector<tvert3> roiPt;
    vector<float> roiRad;
    ifstream roi(filename);
    tvert3 pt;
    float rad;
    if (!roi.good())
	return;
    while (roi.good())
    {
	rad = -1;
	roi >> pt.x >> pt.y >> pt.z >> rad;
	if (rad > 0) //we actually read
	{
	    roiPt.push_back(pt);
	    roiRad.push_back(rad);
	}
    }
    curROI %= roiPt.size();
    testpoint = roiPt[curROI];
    testrad = roiRad[curROI];
    curROI++;
}

void doEdit(unsigned char key)
{
    vertrpy sensor = editSensor;
    float ds = 0.01f*editFac;
    switch(key)
    {
    case 'w':
	sensor.x += ds;
	break;
    case 's':
	sensor.x -= ds;
	break;
    case 'a':
	sensor.y += ds;
	break;
    case 'd':
	sensor.y -= ds;
	break;
    case 'q':
	sensor.z += ds;
	break;
    case 'e':
	sensor.z -= ds;
	break;
    case '8':
	sensor.pitch += ds;
	break;
    case '2':
	sensor.pitch -= ds;
	break;
    case '6':
	sensor.yaw += ds;
	break;
    case '4':
	sensor.yaw -= ds;
	break;
    case '3':
	sensor.roll += ds;
	break;
    case '1':
	sensor.roll -= ds;
	break;
    case '+':
	editFac *= 1.1f;
	break;
    case '-':
	editFac /= 1.1f;
	break;
    case 'z':
	SaveSensor(currPath,ladarname[editLadar],editSensor);
	editMode = false;
	break;
    case 'x':
	editMode = false;
	sensor = LoadSensor(currPath,ladarname[editLadar]);
	break;
    }
    editSensor = sensor;
    ladarcount[editLadar] = LoadAvgScans(currPath,ladarname[editLadar],ladars+editLadar);
    TransformPoints(ladars[editLadar],ladarcount[editLadar],editSensor);
    editPoints[0] = tvert3(0,0,0);
    editPoints[1] = tvert3(0.1f,0,0);
    editPoints[2] = tvert3(0,0.1f,0);
    editPoints[3] = tvert3(0,0,0.1f);
    TransformPoints(editPoints,4,editSensor);
}

void startEdit()
{
    for (int i=0;i<NUM_LADARS;i++)
    {
	if (showladar[i])
	{
	    editLadar = i;
	    editSensor = LoadSensor(currPath,ladarname[i]);
	    editFac = 1.0f;
	    editMode = true;
	    showladar[i] = false;
	    doEdit(0);
	    return;
	}
    }
}

void display(void)
{
    //static int n=0;
    glClear(GL_COLOR_BUFFER_BIT);
    setCamera();
    drawWorld();
    glWindowPos2i(0,10);
/*    char foo[10];
    sprintf(foo,"%d",n);
    drawString(foo);
    n++;*/
    glutSwapBuffers();
    glutPostRedisplay();
}

void idle (void) 
{
    //replot data
//    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    lastx = x;
    lasty = y;
    if(button == GLUT_LEFT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	  drag_x0 = x;
	  drag_y0 = y;
	}
	else
	{
	    lbuttondown = false;
	}
    }   
    if(button == GLUT_RIGHT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	    rbuttondown = true;
	    drag_x0 = x;
	    drag_y0 = y;
	}
	else
	{
	    rbuttondown = false;
	}
    }
}

void keyboard(unsigned char key, int x, int y)
{
    if (editMode)
    {
	doEdit(key);
	return;
    }

    float d = testrad*0.5f;
    switch(key)
    {
    case '2':
	testpoint.x -= d;
	break;
    case '8':
	testpoint.x += d;
	break;
    case '4':
	testpoint.y -= d;
	break;
    case '6':
	testpoint.y += d;
	break;
    case '7':
	testpoint.z -= d;
	break;
    case '9':
	testpoint.z += d;
	break;
    case '+':
	testrad += 0.01f;
	break;
    case '-':
	testrad -= 0.01f;
	break;
    case '!':
	showladar[0] = !showladar[0];
	break;
    case '@':
	showladar[1] = !showladar[1];
	break;
    case '#':
	showladar[2] = !showladar[2];
	break;
    case '$':
	showladar[3] = !showladar[3];
	break;
    case '%':
	showladar[4] = !showladar[4];
	break;
    case '^':
	showladar[5] = !showladar[5];
	break;
    case '&':
	showladar[6] = !showladar[6];
	break;
    case 'g':
	drawGround = !drawGround;
	break;
    case 'r':
	nextROI();
	break;
    case 'c':
	camcenter = !camcenter;
	break;
    case 'S':
	saveROI();
	break;
    case 'R':
	fullWorld = !fullWorld;
	break;
    case 'E':
	startEdit();
	break;
    }
}

void motion(int x, int y)
{
    if (rbuttondown)
    {
	if (lbuttondown)
	{
	    if (camdist > (lasty-y)*CAM_ZOOM)
		camdist += (y-lasty)*CAM_ZOOM;
	}
	else
	{
	    camx += (lastx-x)*CAM_RATE;
	    camy += (y-lasty)*CAM_RATE;
	    if (camy > CAM_YLIM)
		camy = CAM_YLIM;
	    if (camy < -CAM_YLIM)
		camy = -CAM_YLIM;
	}
    }

    lastx = x;
    lasty = y;
    glutPostRedisplay();
}

// Disable resizing
void resize(int w, int h)
{
    width = w;
    height = h;
    glViewport (0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, width/(double)height, 0.01, 100);
}

void initWorld(char *path)
{
    fullworldcount = LoadPTUScans(path, &fullworldpoints);
    BuildTree(fullworldpoints,fullworldcount);
    // clip invalid values
    while (fullworldpoints->index == -1)
    {
	fullworldpoints++;
	fullworldcount--;
    }

    redworldcount = CopyReducePoints(fullworldpoints, fullworldcount, 80000, &redworldpoints);

    printf("using %d points\n",fullworldcount);

    vertrpy sensor = LoadSensor(path,"SENSNET_MF_PTU");
    testpoint.x = sensor.x;
    testpoint.y = sensor.y;
    testpoint.z = sensor.z;

/*    ladarcount[0] = LoadAvgScans(path,"SENSNET_MF_BUMPER_LADAR",ladars+0);
    ladarcount[1] = LoadAvgScans(path,"SENSNET_LF_BUMPER_LADAR",ladars+1);
    ladarcount[2] = LoadAvgScans(path,"SENSNET_RF_BUMPER_LADAR",ladars+2);
    ladarcount[3] = LoadAvgScans(path,"SENSNET_LF_ROOF_LADAR",ladars+3);
    ladarcount[4] = LoadAvgScans(path,"SENSNET_RF_ROOF_LADAR",ladars+4);
    ladarcount[5] = LoadAvgScans(path,"SENSNET_REAR_BUMPER_LADAR",ladars+5);
    ladarcount[6] = LoadAvgScans(path,"SENSNET_RIEGL",ladars+6);*/

    for (int i=0;i<NUM_LADARS;i++)
    {
	ladarcount[i] = LoadAvgScans(path,ladarname[i],ladars+i);
	if (ladarcount[i]>0)
	    TransformPoints(ladars[i],ladarcount[i],LoadSensor(path,ladarname[i]));
	showladar[i] = false;
    }
}

int main(int argc, char ** argv)
{
    if (argc < 2)
    {
	fprintf(stderr, "usage: %s path [sensor1 sensor2 ...]\n", argv[0]);
	return 0;
    }

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(600, 600);
    glutCreateWindow("Sweep Viewer");  
    glClearColor (0.0,0.0,0.0,0.0); //clear the screen to black

    // stuff for vertex arrays
    glEnableClientState(GL_VERTEX_ARRAY);
    //can also do this for vertices and colors
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    camx = camy = 0;
    camdist = 20;
    camcenter = false;
    fullWorld = false;
    testrad = 0.1f;
    editMode = false;
    drawGround = false;
    groundROI = false;
    curROI = 0;

    currPath = argv[1];

    initWorld(currPath);

    /* set callback functions */
    glutDisplayFunc(display); 
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);
    glutReshapeFunc(resize);

    //========================
    // initialize Map
    //========================
    glutMainLoop();
    // The above function never returns anyway

    return 0;
}
