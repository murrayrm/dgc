/**********************************************************
 **
 **  SENSNETELAYS.HH
 **
 **    Time-stamp: <2007-08-17 13:42:15 tamas> 
 **
 **    Author: Tamas Szalay
 **    Created: Fri Aug  17
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef SENSNETDELAYS_HH
#define SENSNETDELAYS_HH

using namespace std;

#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/RadarRangeBlob.h>
#include <interfaces/PTUStateBlob.h>
#include <dgcutils/DGCutils.hh>
#include <map/MapElementTalker.hh>

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"


#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

// Structs for timing and such
struct MessageStatus
{
    // general statistics
    sensnet_id_t sensorId; // associated sensor id
    int msgCount; // total messages received
    int firstId; // id of first blob received
    uint64_t firstTime; // time of first message
    
    // per-second measurements
    int msgThisSecond; // # recvd this second
    int devSqThisSecond; // cumulative squared deviations
    int latencyThisSecond; // cumulative latency
    int delayThisSecond; // cumulative delay

    int minDelayThisSecond;
    int maxDelayThisSecond;

    // associated vehicle state stats
    int vsThisSecond; // # of new veh states
    int vsDelayThisSecond; // cumulative delay

    uint64_t lastVSTime; // time of previous vs update

    // others
    uint64_t lastMsgTime; // time of previous message
    int lastMsgId; // id of previous message
    
    // last second display values
    int lastDelay;
    int lastVariance;
    int lastLatency;
    int lastCount;
    int lastVsCount;
    int lastVsDelay;
};

/*
00 - STATE
01 - MF_PTU
02 - MF_BUMPER_LADAR
03 - RF_BUMPER_LADAR
04 - LF_BUMPER_LADAR
05 - RF_ROOF_LADAR
06 - LF_ROOF_LADAR
07 - REAR_BUMPER_LADAR
08 - PTU_LADAR
09 - RIEGL
10 - RADAR
11 - PTU_RADAR
*/

#define MSG_COUNT 12

class SensnetDelays
{
  
public:
    // Constructor
    SensnetDelays();
    
    // Destructor
    ~SensnetDelays();
    
    // Parse the command line
    int parseCmdLine(int argc, char **argv);
    
    // Initialize sensnet
    int initSensnet();
    
    // Clean up sensnet
    int finiSensnet();
    
    // Update message stats
    int update();

    // Update a single message
    void updateMsg(int id, int blobId, uint64_t timestamp, uint64_t vsTimestamp);

    // Per-second updates
    void updateSecond();

    // Update console display
    void updateConsole();
    
    // Initialize console display
    int initConsole();
    
    // Finalize console display
    int finiConsole();
    
    // Console button callback
    static int onUserQuit(cotk_t *console, SensnetDelays *self, const char *token);
    
    // Console button callback
    static int onUserPause(cotk_t *console, SensnetDelays *self, const char *token);
    
    // Program options
    gengetopt_args_info options;
    
    // Spread settings
    char *spreadDaemon;
    int skynetKey;
    modulename moduleId;
    
    // Sensnet module
    sensnet_t *sensnet;
    
    // Console interface
    cotk_t *console;
    
    // Should we quit?
    bool quit;
    
    // Should we pause?
    bool pause;  

    // Forced delay time, us
    int forceDelay;

    // Receive subgroup
    int recvSubGroup;

    // Map El. talker
    CMapElementTalker * mapTalker;

    // Map El.
    MapElement sensedEl;

    // Map Element status
    MessageStatus mapElInfo;
    
    // Message constants
    MessageStatus msgInfo[MSG_COUNT];
    
    // Last "second" tick time
    uint64_t lastSecond;

    // Last console update
    uint64_t lastConsole;


    // Update stats
    int updateTicks;
    int updateRes;
};
#endif


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

