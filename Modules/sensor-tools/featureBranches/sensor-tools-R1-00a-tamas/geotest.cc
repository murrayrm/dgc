/*!
 * \file emapViewer.cc
 * \brief A basic viewer for emap class
 * \author Tamas Szalay
 *
 * This file will need to be modified to suit your needs!
 * Call startViewer(argc, argv, EMap *emap) to create window and start
 * viewing thread, and call SetEMap(EMap *map) to set the
 * viewer's visible emap.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cmath>

#define GL_GLEXT_PROTOTYPES
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <GL/glext.h>
#endif

#include <sys/types.h>
#include <string.h>

#include <dgcutils/DGCutils.hh>
#include <emap/emap.hh>
#include <frames/point2.hh>
#include <map/MapElement.hh>
#include <frames/point2_uncertain.hh>

//#include "dphull2.cc"

using namespace std; 

//OpenGL variables
int drag_x0, drag_y0;
int lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;

// Initial value, will be set when thread is started
int WINDOW_SIZE=500;

char strDisplay[64]; //On-screen display string
point2arr points;
point2arr poly;
point2arr poly2;
point2arr poly_simp;
vector<line2> lines;
int tol;


void drawString (char *s)
{
    unsigned int i;
    for (i = 0; i < strlen (s); i++)
	glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
}

void drawPoints()
{
    glColor3f(1,1,0);
    glBegin(GL_LINE_LOOP);
    for (int i=0;i<(int)poly.size();i++)
	glVertex2d(poly[i].x,poly[i].y); 
    glEnd();
    
    glColor3f(0,1,1);
    glBegin(GL_LINE_LOOP);
    for (int i=0;i<(int)poly2.size();i++)
	glVertex2d(poly2[i].x,poly2[i].y); 
    glEnd();

    glColor3f(0.5f,0.5f,0);
    glBegin(GL_LINE_LOOP);
    for (int i=0;i<(int)poly_simp.size();i++)
	glVertex2d(poly_simp[i].x,poly_simp[i].y); 
    glEnd();

    point2 min1, min2, max1, max2;
    poly.bounds2(min1, max1);
    poly2.bounds2(min2, max2);

    glColor3f(0.3f,0.3f,0.3f);
    glBegin(GL_LINE_LOOP);
    glVertex2d(min1.x,min1.y);
    glVertex2d(max1.x,min1.y);
    glVertex2d(max1.x,max1.y);
    glVertex2d(min1.x,max1.y);
    glEnd();
    glBegin(GL_LINE_LOOP);
    glVertex2d(min2.x,min2.y);
    glVertex2d(max2.x,min2.y);
    glVertex2d(max2.x,max2.y);
    glVertex2d(min2.x,max2.y);
    glEnd();
    min1.set(max(min1.x,min2.x),max(min1.y,min2.y));
    max1.set(min(max1.x,max2.x),min(max1.y,max2.y));
    glColor3f(1,0,0);
    glBegin(GL_LINE_LOOP);
    glVertex2d(min1.x,min1.y);
    glVertex2d(max1.x,min1.y);
    glVertex2d(max1.x,max1.y);
    glVertex2d(min1.x,max1.y);
    glEnd();

    glColor3f(0,1,0);
    glBegin(GL_LINES);
    for (int i=0;i<(int)lines.size();i++)
    {
	glVertex2d(lines[i].ptA.x,lines[i].ptA.y);
	glVertex2d(lines[i].ptB.x,lines[i].ptB.y);
    }
    glEnd();

    if (poly.is_inside_poly(point2(lastx,lasty)))
    {    
	glPointSize(10);
	glColor3f(0,1,1);
	glBegin(GL_POINTS);
	glVertex2d(lastx,lasty);
	glEnd();
    }

    glPointSize(5);
    glColor3f(1,0.5f,0.5f);
    glBegin(GL_POINTS);
    for (int i=0;i<(int)lines.size();i++)
    {
	point2 p = lines[i].project_bound(point2(lastx,lasty));	
	glVertex2d(p.x,p.y);
    }
    glEnd();

    glPointSize(7);
    glColor3f(1,0,0);
    glBegin(GL_POINTS);
    for (int i=0;i<(int)points.size();i++)
	glVertex2d(points[i].x,points[i].y);
    glEnd();

    glColor3f(1,1,1);
    glWindowPos2i(0,30);
    char disp[100];
    sprintf(disp,"Simplified poly: %d/%d (%d), tol=%d",poly_simp.size(),poly.size(),poly2.size(),tol);
    drawString(disp);

    MapElement el1, el2;
    el1.setGeometry(poly);
    el1.setTypeObstacle();
    el2.setGeometry(poly2);
    el2.setTypeObstacle();

    glWindowPos2i(0,16);
    bool coll;
    uint64_t start = DGCgettime(); 
    for (int i=0;i<1000;i++) 
	coll=poly.is_poly_overlap(poly2,min1,max1);
    sprintf(disp,"Time: %4.2f microsecs, val=%s",(DGCgettime()-start)/1000.0f,coll?"true":"false");
    drawString(disp); 

    glWindowPos2i(0,2);
    start = DGCgettime();
    for (int i=0;i<1000;i++)
	coll=poly2.is_poly_overlap(poly,min1,max1);
    sprintf(disp,"Time 2: %4.2f microsecs, val=%s",(DGCgettime()-start)/1000.0f,coll?"true":"false");
    drawString(disp); 

    glWindowPos2i(0,45);
    start = DGCgettime(); 
    for (int i=0;i<1000;i++) 
	coll=el1.isOverlap(el2);
    sprintf(disp,"Time 3: %4.2f microsecs, val=%s",(DGCgettime()-start)/1000.0f,coll?"true":"false");
    drawString(disp); 
}

void display(void)
{
    glColor3f(0,0,0);
    glClear(GL_COLOR_BUFFER_BIT);
    drawPoints();
    glFlush();
    usleep(50000);
    glutPostRedisplay();
}

void idle (void) 
{
    //replot data
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
    lastx = x;
    lasty = y;
    if(button == GLUT_LEFT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	  // Your code here!
	  drag_x0 = x;
	  drag_y0 = y;
	}
	else
	{
	    lbuttondown = false;
	    //if (x == drag_x0 && y == drag_y0)
	    {
		poly2.push_back(point2(x,y));
	    }
	    // else
	    {
		line2 l(point2(x,y),point2(drag_x0,drag_y0));
		for (int i=0;i<(int)lines.size();i++)
		    if (lines[i].is_intersect(l))
			points.push_back(lines[i].intersect(l));
		lines.push_back(l);
		point2 pmin,pmax;
		poly.bounds2(pmin,pmax);
		if (poly.line_rect_intersect(l.ptA,l.ptB,pmin,pmax))
		{
		    points.push_back(l.ptA);
		    points.push_back(l.ptB);
		}
	    }
	    // Your code here!
	}
    }   
    if(button == GLUT_RIGHT_BUTTON)
    {
	if(state == GLUT_DOWN)
	{
	    rbuttondown = true;
	}
	else
	{
	    rbuttondown = false;
	    poly.push_back(point2(x,y));
	}
    }
}

void motion(int x, int y)
{
    lastx = x;
    lasty = y;
    if (lbuttondown)
    {
    }
    
    if (rbuttondown)
    {     
    }
}

void keyboard(unsigned char key, int x, int y)
{
    point2 p;
    vector<point2arr> ptarr;
    switch (key)
    {
    case 'c':
	points.clear();
	poly.clear();
	poly_simp.clear();
	poly2.clear();
	lines.clear();
	break;
    case '+':
    case '=':
	tol++;
	poly_simp = poly.simplify(tol);
	break;
    case '-':
	tol--;
	poly_simp = poly.simplify(tol);
	break;
    case 'w':
	p.y = -1;
	break;
    case 's':
	p.y = 1;
	break;
    case 'a':
	p.x = -1;
	break;
    case 'd':
	p.x = 1;
	break;
    case 'i':
	poly.get_poly_intersection(poly2,ptarr);
	if (ptarr.size() > 0)
	    poly_simp = ptarr[0];
	break;
    case 'u':
	poly.get_poly_union(poly2,ptarr);
	if (ptarr.size() > 0)
	    poly_simp = ptarr[0];
	break;
    }
    for (size_t i=0; i<poly2.size(); i++)
	poly2[i] = poly2[i] + p;
}

// Disable resizing
void resize(int w, int h)
{
    if (w != WINDOW_SIZE || h != WINDOW_SIZE)
	glutReshapeWindow(WINDOW_SIZE,WINDOW_SIZE);
}

void * mainLoop(void)
{
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE);
    glutCreateWindow("EMap Viewer");  
    glClearColor (0.0,0.0,0.0,0.0); //clear the screen to black
    glShadeModel(GL_FLAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, sizeof(emap_t));
    glPixelZoom(1,-1);

    // Set transform matrix to be the pixel transform
    glLoadIdentity();
    glViewport (0, 0, WINDOW_SIZE, WINDOW_SIZE);
    glMatrixMode(GL_PROJECTION);
    glOrtho(0,WINDOW_SIZE,WINDOW_SIZE,0,-1,1);

    /* set callback functions */
    glutDisplayFunc(display); 
    glutMouseFunc(mouse);
    glutPassiveMotionFunc(motion);    
    glutIdleFunc(idle);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyboard);

    //========================
    // initialize Map
    //========================
    glutMainLoop();
    // The above function never returns anyway

    //glutDestroyWindow(win);
    return 0;
}

void speedTest()
{
    uint64_t start;
    point2arr arr;
    for (int i=0;i<360;i++)
    {
	point2 p;
	p.x = cos(i*(M_PI/180));//+5;
	p.y = sin(i*(M_PI/180));//+5;
	arr.push_back(p);
    }
    point2arr arr2;
    arr2.push_back(point2(-0.1,-0.1));
    arr2.push_back(point2(0.1,-0.1));
    arr2.push_back(point2(0.1,0.1));
    arr2.push_back(point2(0.1,-0.1));
    start = DGCgettime();
    for (int i=0;i<1000;i++)
	arr.is_inside_poly(point2(0,0));
    printf("Time: %d ms\n",(int)(DGCgettime()-start)/1000);
    start = DGCgettime();
    for (int i=0;i<1000;i++) 
	arr.is_intersect(arr2);
    printf("Time: %d ms\n",(int)(DGCgettime()-start)/1000);
    vector<point2arr> arrs;
    start = DGCgettime();
    for (int i=0;i<1000;i++)
    {
	arrs.clear();
	arr.get_poly_intersection(arr2,arrs);
    }
    printf("Time: %d ms\n",(int)(DGCgettime()-start)/1000);
    start = DGCgettime();
    for (int i=0;i<1000;i++)
	arr.is_poly_overlap(arr2);
    printf("Time: %d ms\n",(int)(DGCgettime()-start)/1000);
}

int main(int argc, char **argv)
{
    WINDOW_SIZE = 800;
    tol = 30;
    speedTest();
    glutInit(&argc, argv);
    mainLoop();
    return 0; 
}


/*int main(int argc, char ** argv)
{
    int n = 100;
    if (argc > 1)
	n = atoi(argv[1]);
    point2arr poly;
    point2arr pout;

    for (int i=0;i<n;i++)
	poly.push_back(point2(i*10,rand()%10));

    uint64_t tstart = DGCgettime();
    poly_simplify(3.5f, poly, pout);
    cout << pout.size() << " points in " << (DGCgettime()-tstart)/1000 << " ms " << endl;
    return 0;
}*/
