#include "CapturePoints.hh"

// Default constructor
CapturePoints::CapturePoints()
{
    memset(this, 0, sizeof(*this));

    // Get spread daemon
    if (getenv("SPREAD_DAEMON"))
	this->spreadDaemon = getenv("SPREAD_DAEMON");
    else
	ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
    
    // Fill out the skynet key
    if (getenv("SKYNET_KEY"))
	this->skynetKey = atoi(getenv("SKYNET_KEY"));
    else
	this->skynetKey = 0;
    
    // Fill out module id
    this->moduleId = MODladarPolyObsPerceptor;

    this->ladars[this->numLadars++].sensorId = SENSNET_PTU_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_MF_BUMPER_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_LF_BUMPER_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_RF_BUMPER_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_REAR_BUMPER_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_LF_ROOF_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_RF_ROOF_LADAR;
    this->ladars[this->numLadars++].sensorId = SENSNET_RIEGL;

    this->ptu.sensorId = SENSNET_MF_PTU;

    this->capturing = false;
    
    initSensnet();
    openLogs();
}


// Default destructor
CapturePoints::~CapturePoints()
{
    closeLogs();
    finiSensnet();
}

// Initialize sensnet
int CapturePoints::initSensnet()
{
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
	return -1;

    if (sensnet_join(this->sensnet, ptu.sensorId,
		     SENSNET_PTU_STATE_BLOB, sizeof(PTUStateBlob)) != 0)
	return ERROR("unable to join %d", ptu.sensorId);
    
    // Join ladar groups
    for (int i = 0; i < this->numLadars; i++)
    {
	if (sensnet_join(this->sensnet, ladars[i].sensorId,
			 SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
	    return ERROR("unable to join %d", ladars[i].sensorId);
    }

    return 0;
}


// Clean up sensnet
int CapturePoints::finiSensnet()
{
    if (this->sensnet)
    {
	sensnet_leave(this->sensnet, this->ptu.sensorId, SENSNET_PTU_STATE_BLOB);
	for (int i = 0; i < this->numLadars; i++)
	    sensnet_leave(this->sensnet, this->ladars[i].sensorId, SENSNET_LADAR_BLOB);

	sensnet_free(this->sensnet);
	this->sensnet = NULL;
    }
    
    return 0;
}

void CapturePoints::openLogs()
{
    char filename[150];

    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);

    mkdir("/tmp/logs/ladar-calib/",
	  S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);

    sprintf(this->logpath,
	    "/tmp/logs/ladar-calib/%04d-%02d-%02d-%02d-%02d-%02d/",
	    local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
	    local->tm_hour, local->tm_min, local->tm_sec);

    mkdir(this->logpath,
	  S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
    
    for (int i=0;i<this->numLadars;i++)
    {
	sprintf(filename,"%s%s",
		this->logpath,sensnet_id_to_name(this->ladars[i].sensorId));
	this->ladars[i].log = fopen(filename,"w");
	if (!this->ladars[i].log)
	    ERROR("Could not open %s",filename);
    }

    sprintf(filename,"%s%s",
	    this->logpath,sensnet_id_to_name(this->ptu.sensorId));
    this->ptu.log = fopen(filename,"w");
    if (!this->ptu.log)
	ERROR("Could not open %s",filename);

    printf("Opened %s for data capture\n",this->logpath);

    FILE *fsrc, *fdest;

    printf("Copying sensor config files... ");

    char *configPath = dgcFindConfigDir("PTUcontroller");
    sprintf(filename,"%s/SENSNET_MF_PTU.CFG",configPath);

    fsrc = fopen(filename,"rb");
    if (fsrc)
    {
	sprintf(filename,"%sSENSNET_MF_PTU.CFG",this->logpath);
	fdest = fopen(filename,"wb");
	if (fdest)
	{
	    while (!feof(fsrc))
		fputc(fgetc(fsrc),fdest);
	    fclose(fdest);
	}
	fclose(fsrc);	
    }

    configPath = dgcFindConfigDir("ladarfeeder");

    for (int i=0;i<this->numLadars;i++)
    {
	sprintf(filename,"%s/%s.CFG",configPath,sensnet_id_to_name(this->ladars[i].sensorId));
	fsrc = fopen(filename,"rb");
	if (fsrc)
	{
	    sprintf(filename,"%s%s.CFG",this->logpath,sensnet_id_to_name(this->ladars[i].sensorId));
	    fdest = fopen(filename,"wb");
	    if (fdest)
	    {
		while (!feof(fsrc))
		    fputc(fgetc(fsrc),fdest);
		fclose(fdest);
	    }
	    fclose(fsrc);	
	}
    }

    printf("done.\n");
}

void CapturePoints::closeLogs()
{
    if (this->ptu.log)
	fclose(this->ptu.log);
    for (int i=0;i<this->numLadars;i++)
	if (this->ladars[i].log)
	    fclose(this->ladars[i].log);
}

char* CapturePoints::getLogPath()
{
    return logpath;
}

void CapturePoints::saveScan(int id, LadarRangeBlob *blob)
{
    if (!this->capturing)
	return;

    // write numPoints
    fwrite(&blob->numPoints,sizeof(int),1,this->ladars[id].log);
    // write points (angle and range)
    fwrite(blob->points,sizeof(float),2*blob->numPoints,this->ladars[id].log);

    // write PTU state data also
    if (blob->sensorId == SENSNET_PTU_LADAR)
    {
	if (blob->reserved[0]+blob->reserved[1]) // we received pan/tilt data
	{
	    fwrite(blob->reserved,sizeof(float),4,this->ptu.log);
	}
	else
	{
	    fwrite(&(this->ptublob.currpan),sizeof(float),4,this->ptu.log);
	    printf("No embedded PTU messages in ladar blob!\n");
	}
    }
}

void CapturePoints::printActive()
{
    this->update();
    printf("Active sensors:\n");
    for (int i=0; i<this->numLadars;i++)
	if (this->ladars[i].blobCount > 0)
	    printf("%s\n",sensnet_id_to_name(this->ladars[i].sensorId));
    if (this->ptu.blobCount > 0)
	printf("%s\n",sensnet_id_to_name(this->ptu.sensorId));
    else
	printf("Warning: no PTU messages received!\n");
}

void CapturePoints::startCapture()
{
    this->capturing = true;
    printf("Capture started\n");
}

void CapturePoints::stopCapture()
{
    this->capturing = false;
    printf("Capture paused\n");
}

int CapturePoints::commandPTU(float pan, float tilt, float panspeed, float tiltspeed, bool blocking)
{
    PTUCommand pc;

    pc.type = RAW;
    pc.pan = pan;
    pc.tilt = tilt;
    pc.panspeed = panspeed;
    pc.tiltspeed = tiltspeed;

    if (this->sensnet)
    {
	if (sensnet_write(this->sensnet, SENSNET_METHOD_SKYNET,
			   SENSNET_SKYNET_SENSOR,SNptuCommand,0,
			   sizeof(PTUCommand), &pc) == 0)
	    printf("PTU command sent: %3.2f %3.2f %3.2f %3.2f\n",
		   pan, tilt, panspeed, tiltspeed);
	else
	    return ERROR("Could not send PTU command!");
    }

    if (!blocking)
	return 0;

    printf("Waiting for PTU to reach commanded position... ");
    
    update();

    // wait for PTU to reach commanded position
    while (blocking)
    {
	bool isDone = (fabs(this->ptublob.currpanspeed) < PTU_VEL_THRESH);
	isDone = isDone && (fabs(this->ptublob.currtiltspeed) < PTU_VEL_THRESH);
	isDone = isDone && (fabs(this->ptublob.currtilt - tilt) < PTU_ANG_THRESH);
	isDone = isDone && (fabs(this->ptublob.currpan - pan) < PTU_ANG_THRESH);
	if (isDone)
	    break;
	// get new PTU state
	this->update();
    }

    printf("done.\n");

    return 0;
}

// Update the map with new range data
int CapturePoints::update()
{
    int blobId, blobLen;
    LadarRangeBlob lblob;
    
//    if (sensnet_wait(this->sensnet, 200) != 0)
//	return 0;

    // read off PTU State
    if (sensnet_peek(this->sensnet, this->ptu.sensorId, SENSNET_PTU_STATE_BLOB, &blobId, &blobLen) == 0)
    {
    	if (blobId > this->ptu.blobId)
	{
	    if (!sensnet_read(this->sensnet, this->ptu.sensorId, SENSNET_PTU_STATE_BLOB, 
			     &blobId, sizeof(PTUStateBlob), &this->ptublob) == 0)
		return ERROR("Could not read PTU state");
	    this->ptu.blobCount++;
	    this->ptu.blobId = blobId;
	    // do what we need to update/capture/etc
	}
    }

    for (int i = 0; i < this->numLadars; i++)
    {
	if (sensnet_peek(this->sensnet, this->ladars[i].sensorId, SENSNET_LADAR_BLOB, &blobId, &blobLen) == 0)
	{
	    if (blobId > this->ladars[i].blobId)
	    {
		if (!sensnet_read(this->sensnet, this->ladars[i].sensorId, SENSNET_LADAR_BLOB, 
				 &blobId, sizeof(lblob), &lblob) == 0)
		    return ERROR("Error reading ladar blob");
		this->ladars[i].blobCount++;
		this->ladars[i].blobId = blobId;
		saveScan(i,&lblob);
	    }
	}
    }

    return 0;
}

/*    if ((DGCgettime() - this->lastSecond) > 1000000)
    {
	updateSecond();
	this->lastSecond += 1000000;
	this->updateRes = this->updateTicks;
	this->updateTicks = 0;
    }

    if ((DGCgettime() - this->lastConsole) > 100000)
    {
	updateConsole();
	this->lastConsole = DGCgettime();
    }

    this->updateTicks++;
    
    return 0;
}

// Main program thread
int main(int argc, char **argv)
{
    CapturePoints *delays;
    
    delays = new CapturePoints();
    assert(delays);
    
    // Parse command line options
    if (delays->parseCmdLine(argc, argv) != 0)
	return -1;
    
    // Initialize sensnet
    if (delays->initSensnet() != 0)
	return -1;
    
    // Initialize cotk display
    if (!delays->options.disable_console_flag)
	if (delays->initConsole() != 0)
	    return -1;
    
    
    fprintf(stderr, "entering main thread of CapturePoints \n");

    delays->lastSecond = DGCgettime();
    
    while (!delays->quit)
    {
	// Update the console
	if (delays->console)
	    cotk_update(delays->console);
	
	// If we are paused, dont do anything
	if (delays->pause)
	{
	    usleep(50000);
	    continue;
	}
	
	// Get new data and update our perceptor
	if (delays->update() != 0)
	    break;
    }
    
    if (delays->console)
	delays->finiConsole();
    delays->finiSensnet();
    
    MSG("exited cleanly");
    
    return 0;
}

*/
