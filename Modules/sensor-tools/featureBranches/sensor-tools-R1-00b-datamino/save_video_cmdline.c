/*
  File autogenerated by gengetopt version 2.16
  generated with the following command:
  gengetopt -i save_video_cmdline.ggo -F save_video_cmdline -u --conf-parser 

  The developers of gengetopt consider the fixed text that goes in all
  gengetopt output files to be in the public domain:
  we make no copyright claims on it.
*/

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "getopt.h"

#include "save_video_cmdline.h"

static
void clear_given (struct gengetopt_args_info *args_info);
static
void clear_args (struct gengetopt_args_info *args_info);

static int
cmdline_parser_internal (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required, const char *additional_error);

struct line_list
{
  char * string_arg;
  struct line_list * next;
};

static struct line_list *cmd_line_list = 0;
static struct line_list *cmd_line_list_tmp = 0;


static char *
gengetopt_strdup (const char *s);

static
void clear_given (struct gengetopt_args_info *args_info)
{
  args_info->help_given = 0 ;
  args_info->version_given = 0 ;
  args_info->spread_daemon_given = 0 ;
  args_info->skynet_key_given = 0 ;
  args_info->module_id_given = 0 ;
  args_info->sensor_id_given = 0 ;
  args_info->output_given = 0 ;
  args_info->log_path_given = 0 ;
  args_info->width_given = 0 ;
  args_info->height_given = 0 ;
  args_info->channels_given = 0 ;
  args_info->divider_given = 0 ;
  args_info->format_given = 0 ;
}

static
void clear_args (struct gengetopt_args_info *args_info)
{
  args_info->spread_daemon_arg = NULL;
  args_info->spread_daemon_orig = NULL;
  args_info->skynet_key_arg = 0;
  args_info->skynet_key_orig = NULL;
  args_info->module_id_arg = gengetopt_strdup ("MODvideoRecorder");
  args_info->module_id_orig = NULL;
  args_info->sensor_id_arg = gengetopt_strdup ("SENSNET_MF_MEDIUM_STEREO");
  args_info->sensor_id_orig = NULL;
  args_info->output_arg = gengetopt_strdup ("video");
  args_info->output_orig = NULL;
  args_info->log_path_arg = gengetopt_strdup (".");
  args_info->log_path_orig = NULL;
  args_info->width_arg = 320;
  args_info->width_orig = NULL;
  args_info->height_arg = 240;
  args_info->height_orig = NULL;
  args_info->channels_arg = 1;
  args_info->channels_orig = NULL;
  args_info->divider_arg = 2;
  args_info->divider_orig = NULL;
  args_info->format_arg = gengetopt_strdup ("jpg");
  args_info->format_orig = NULL;
  
}

void
cmdline_parser_print_version (void)
{
  printf ("%s %s\n", CMDLINE_PARSER_PACKAGE, CMDLINE_PARSER_VERSION);
}

void
cmdline_parser_print_help (void)
{
  cmdline_parser_print_version ();
  printf("\n%s\n", "Listen to sensnet StereoImageBlob and save a reduced-quality video to disk. The \noutput is in the form of a directory containing each frame as a separate file.");
  printf("\nUsage: Video Saver perceptor [OPTIONS]... [FILES]...\n\n");
  printf("%s\n","  -h, --help                  Print help and exit");
  printf("%s\n","  -V, --version               Print version and exit");
  printf("%s\n","\nBasic options:");
  printf("%s\n","      --spread-daemon=STRING  Spread daemon");
  printf("%s\n","  -S, --skynet-key=INT        Skynet key  (default=`0')");
  printf("%s\n","      --module-id=STRING      Module id  (default=`MODvideoRecorder')");
  printf("%s\n","      --sensor-id=STRING      Sensor id (should be a camera!)  \n                                (default=`SENSNET_MF_MEDIUM_STEREO')");
  printf("%s\n","  -o, --output=STRING         Output directory name. A timestamp will be added \n                                after the specified name  (default=`video')");
  printf("%s\n","      --log-path=STRING       Base directory, created if needed. Output will be \n                                in <logs>/<output>/img-*  (default=`.')");
  printf("%s\n","  -w, --width=INT             Width of the output images  (default=`320')");
  printf("%s\n","  -H, --height=INT            Width of the output images  (default=`240')");
  printf("%s\n","  -c, --channels=INT          Number of channels of the output images (3 = \n                                color, 1 = grayscale)  (default=`1')");
  printf("%s\n","  -d, --divider=INT           Framerate divider, i.e., save one frame every \n                                this number of frames  (default=`2')");
  printf("%s\n","  -f, --format=STRING         Output image format (file extension), can be any \n                                format supported by opencv  (default=`jpg')");
  
}

void
cmdline_parser_init (struct gengetopt_args_info *args_info)
{
  clear_given (args_info);
  clear_args (args_info);

  args_info->inputs = NULL;
  args_info->inputs_num = 0;
}

static void
cmdline_parser_release (struct gengetopt_args_info *args_info)
{
  
  unsigned int i;
  if (args_info->spread_daemon_arg)
    {
      free (args_info->spread_daemon_arg); /* free previous argument */
      args_info->spread_daemon_arg = 0;
    }
  if (args_info->spread_daemon_orig)
    {
      free (args_info->spread_daemon_orig); /* free previous argument */
      args_info->spread_daemon_orig = 0;
    }
  if (args_info->skynet_key_orig)
    {
      free (args_info->skynet_key_orig); /* free previous argument */
      args_info->skynet_key_orig = 0;
    }
  if (args_info->module_id_arg)
    {
      free (args_info->module_id_arg); /* free previous argument */
      args_info->module_id_arg = 0;
    }
  if (args_info->module_id_orig)
    {
      free (args_info->module_id_orig); /* free previous argument */
      args_info->module_id_orig = 0;
    }
  if (args_info->sensor_id_arg)
    {
      free (args_info->sensor_id_arg); /* free previous argument */
      args_info->sensor_id_arg = 0;
    }
  if (args_info->sensor_id_orig)
    {
      free (args_info->sensor_id_orig); /* free previous argument */
      args_info->sensor_id_orig = 0;
    }
  if (args_info->output_arg)
    {
      free (args_info->output_arg); /* free previous argument */
      args_info->output_arg = 0;
    }
  if (args_info->output_orig)
    {
      free (args_info->output_orig); /* free previous argument */
      args_info->output_orig = 0;
    }
  if (args_info->log_path_arg)
    {
      free (args_info->log_path_arg); /* free previous argument */
      args_info->log_path_arg = 0;
    }
  if (args_info->log_path_orig)
    {
      free (args_info->log_path_orig); /* free previous argument */
      args_info->log_path_orig = 0;
    }
  if (args_info->width_orig)
    {
      free (args_info->width_orig); /* free previous argument */
      args_info->width_orig = 0;
    }
  if (args_info->height_orig)
    {
      free (args_info->height_orig); /* free previous argument */
      args_info->height_orig = 0;
    }
  if (args_info->channels_orig)
    {
      free (args_info->channels_orig); /* free previous argument */
      args_info->channels_orig = 0;
    }
  if (args_info->divider_orig)
    {
      free (args_info->divider_orig); /* free previous argument */
      args_info->divider_orig = 0;
    }
  if (args_info->format_arg)
    {
      free (args_info->format_arg); /* free previous argument */
      args_info->format_arg = 0;
    }
  if (args_info->format_orig)
    {
      free (args_info->format_orig); /* free previous argument */
      args_info->format_orig = 0;
    }
  
  for (i = 0; i < args_info->inputs_num; ++i)
    free (args_info->inputs [i]);
  
  if (args_info->inputs_num)
    free (args_info->inputs);
  
  clear_given (args_info);
}

int
cmdline_parser_file_save(const char *filename, struct gengetopt_args_info *args_info)
{
  FILE *outfile;
  int i = 0;

  outfile = fopen(filename, "w");

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot open file for writing: %s\n", CMDLINE_PARSER_PACKAGE, filename);
      return EXIT_FAILURE;
    }

  if (args_info->help_given) {
    fprintf(outfile, "%s\n", "help");
  }
  if (args_info->version_given) {
    fprintf(outfile, "%s\n", "version");
  }
  if (args_info->spread_daemon_given) {
    if (args_info->spread_daemon_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "spread-daemon", args_info->spread_daemon_orig);
    } else {
      fprintf(outfile, "%s\n", "spread-daemon");
    }
  }
  if (args_info->skynet_key_given) {
    if (args_info->skynet_key_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "skynet-key", args_info->skynet_key_orig);
    } else {
      fprintf(outfile, "%s\n", "skynet-key");
    }
  }
  if (args_info->module_id_given) {
    if (args_info->module_id_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "module-id", args_info->module_id_orig);
    } else {
      fprintf(outfile, "%s\n", "module-id");
    }
  }
  if (args_info->sensor_id_given) {
    if (args_info->sensor_id_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "sensor-id", args_info->sensor_id_orig);
    } else {
      fprintf(outfile, "%s\n", "sensor-id");
    }
  }
  if (args_info->output_given) {
    if (args_info->output_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "output", args_info->output_orig);
    } else {
      fprintf(outfile, "%s\n", "output");
    }
  }
  if (args_info->log_path_given) {
    if (args_info->log_path_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "log-path", args_info->log_path_orig);
    } else {
      fprintf(outfile, "%s\n", "log-path");
    }
  }
  if (args_info->width_given) {
    if (args_info->width_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "width", args_info->width_orig);
    } else {
      fprintf(outfile, "%s\n", "width");
    }
  }
  if (args_info->height_given) {
    if (args_info->height_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "height", args_info->height_orig);
    } else {
      fprintf(outfile, "%s\n", "height");
    }
  }
  if (args_info->channels_given) {
    if (args_info->channels_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "channels", args_info->channels_orig);
    } else {
      fprintf(outfile, "%s\n", "channels");
    }
  }
  if (args_info->divider_given) {
    if (args_info->divider_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "divider", args_info->divider_orig);
    } else {
      fprintf(outfile, "%s\n", "divider");
    }
  }
  if (args_info->format_given) {
    if (args_info->format_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "format", args_info->format_orig);
    } else {
      fprintf(outfile, "%s\n", "format");
    }
  }
  
  fclose (outfile);

  i = EXIT_SUCCESS;
  return i;
}

void
cmdline_parser_free (struct gengetopt_args_info *args_info)
{
  cmdline_parser_release (args_info);
  if (cmd_line_list)
    {
      /* free the list of a previous call */
      while (cmd_line_list) {
        cmd_line_list_tmp = cmd_line_list;
        cmd_line_list = cmd_line_list->next;
        free (cmd_line_list_tmp->string_arg);
        free (cmd_line_list_tmp);
      }
    }
}


/* gengetopt_strdup() */
/* strdup.c replacement of strdup, which is not standard */
char *
gengetopt_strdup (const char *s)
{
  char *result = NULL;
  if (!s)
    return result;

  result = (char*)malloc(strlen(s) + 1);
  if (result == (char*)0)
    return (char*)0;
  strcpy(result, s);
  return result;
}

int
cmdline_parser (int argc, char * const *argv, struct gengetopt_args_info *args_info)
{
  return cmdline_parser2 (argc, argv, args_info, 0, 1, 1);
}

int
cmdline_parser2 (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required)
{
  int result;

  result = cmdline_parser_internal (argc, argv, args_info, override, initialize, check_required, NULL);

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser_required (struct gengetopt_args_info *args_info, const char *prog_name)
{
  return EXIT_SUCCESS;
}

int
cmdline_parser_internal (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required, const char *additional_error)
{
  int c;	/* Character of the parsed option.  */

  int error = 0;
  struct gengetopt_args_info local_args_info;

  if (initialize)
    cmdline_parser_init (args_info);

  cmdline_parser_init (&local_args_info);

  optarg = 0;
  optind = 0;
  opterr = 1;
  optopt = '?';

  while (1)
    {
      int option_index = 0;
      char *stop_char;

      static struct option long_options[] = {
        { "help",	0, NULL, 'h' },
        { "version",	0, NULL, 'V' },
        { "spread-daemon",	1, NULL, 0 },
        { "skynet-key",	1, NULL, 'S' },
        { "module-id",	1, NULL, 0 },
        { "sensor-id",	1, NULL, 0 },
        { "output",	1, NULL, 'o' },
        { "log-path",	1, NULL, 0 },
        { "width",	1, NULL, 'w' },
        { "height",	1, NULL, 'H' },
        { "channels",	1, NULL, 'c' },
        { "divider",	1, NULL, 'd' },
        { "format",	1, NULL, 'f' },
        { NULL,	0, NULL, 0 }
      };

      stop_char = 0;
      c = getopt_long (argc, argv, "hVS:o:w:H:c:d:f:", long_options, &option_index);

      if (c == -1) break;	/* Exit from `while (1)' loop.  */

      switch (c)
        {
        case 'h':	/* Print help and exit.  */
          cmdline_parser_print_help ();
          cmdline_parser_free (&local_args_info);
          exit (EXIT_SUCCESS);

        case 'V':	/* Print version and exit.  */
          cmdline_parser_print_version ();
          cmdline_parser_free (&local_args_info);
          exit (EXIT_SUCCESS);

        case 'S':	/* Skynet key.  */
          if (local_args_info.skynet_key_given)
            {
              fprintf (stderr, "%s: `--skynet-key' (`-S') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->skynet_key_given && ! override)
            continue;
          local_args_info.skynet_key_given = 1;
          args_info->skynet_key_given = 1;
          args_info->skynet_key_arg = strtol (optarg, &stop_char, 0);
          if (!(stop_char && *stop_char == '\0')) {
            fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
            goto failure;
          }
          if (args_info->skynet_key_orig)
            free (args_info->skynet_key_orig); /* free previous string */
          args_info->skynet_key_orig = gengetopt_strdup (optarg);
          break;

        case 'o':	/* Output directory name. A timestamp will be added after the specified name.  */
          if (local_args_info.output_given)
            {
              fprintf (stderr, "%s: `--output' (`-o') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->output_given && ! override)
            continue;
          local_args_info.output_given = 1;
          args_info->output_given = 1;
          if (args_info->output_arg)
            free (args_info->output_arg); /* free previous string */
          args_info->output_arg = gengetopt_strdup (optarg);
          if (args_info->output_orig)
            free (args_info->output_orig); /* free previous string */
          args_info->output_orig = gengetopt_strdup (optarg);
          break;

        case 'w':	/* Width of the output images.  */
          if (local_args_info.width_given)
            {
              fprintf (stderr, "%s: `--width' (`-w') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->width_given && ! override)
            continue;
          local_args_info.width_given = 1;
          args_info->width_given = 1;
          args_info->width_arg = strtol (optarg, &stop_char, 0);
          if (!(stop_char && *stop_char == '\0')) {
            fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
            goto failure;
          }
          if (args_info->width_orig)
            free (args_info->width_orig); /* free previous string */
          args_info->width_orig = gengetopt_strdup (optarg);
          break;

        case 'H':	/* Width of the output images.  */
          if (local_args_info.height_given)
            {
              fprintf (stderr, "%s: `--height' (`-H') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->height_given && ! override)
            continue;
          local_args_info.height_given = 1;
          args_info->height_given = 1;
          args_info->height_arg = strtol (optarg, &stop_char, 0);
          if (!(stop_char && *stop_char == '\0')) {
            fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
            goto failure;
          }
          if (args_info->height_orig)
            free (args_info->height_orig); /* free previous string */
          args_info->height_orig = gengetopt_strdup (optarg);
          break;

        case 'c':	/* Number of channels of the output images (3 = color, 1 = grayscale).  */
          if (local_args_info.channels_given)
            {
              fprintf (stderr, "%s: `--channels' (`-c') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->channels_given && ! override)
            continue;
          local_args_info.channels_given = 1;
          args_info->channels_given = 1;
          args_info->channels_arg = strtol (optarg, &stop_char, 0);
          if (!(stop_char && *stop_char == '\0')) {
            fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
            goto failure;
          }
          if (args_info->channels_orig)
            free (args_info->channels_orig); /* free previous string */
          args_info->channels_orig = gengetopt_strdup (optarg);
          break;

        case 'd':	/* Framerate divider, i.e., save one frame every this number of frames.  */
          if (local_args_info.divider_given)
            {
              fprintf (stderr, "%s: `--divider' (`-d') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->divider_given && ! override)
            continue;
          local_args_info.divider_given = 1;
          args_info->divider_given = 1;
          args_info->divider_arg = strtol (optarg, &stop_char, 0);
          if (!(stop_char && *stop_char == '\0')) {
            fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
            goto failure;
          }
          if (args_info->divider_orig)
            free (args_info->divider_orig); /* free previous string */
          args_info->divider_orig = gengetopt_strdup (optarg);
          break;

        case 'f':	/* Output image format (file extension), can be any format supported by opencv.  */
          if (local_args_info.format_given)
            {
              fprintf (stderr, "%s: `--format' (`-f') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->format_given && ! override)
            continue;
          local_args_info.format_given = 1;
          args_info->format_given = 1;
          if (args_info->format_arg)
            free (args_info->format_arg); /* free previous string */
          args_info->format_arg = gengetopt_strdup (optarg);
          if (args_info->format_orig)
            free (args_info->format_orig); /* free previous string */
          args_info->format_orig = gengetopt_strdup (optarg);
          break;


        case 0:	/* Long option with no short option */
          /* Spread daemon.  */
          if (strcmp (long_options[option_index].name, "spread-daemon") == 0)
          {
            if (local_args_info.spread_daemon_given)
              {
                fprintf (stderr, "%s: `--spread-daemon' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->spread_daemon_given && ! override)
              continue;
            local_args_info.spread_daemon_given = 1;
            args_info->spread_daemon_given = 1;
            if (args_info->spread_daemon_arg)
              free (args_info->spread_daemon_arg); /* free previous string */
            args_info->spread_daemon_arg = gengetopt_strdup (optarg);
            if (args_info->spread_daemon_orig)
              free (args_info->spread_daemon_orig); /* free previous string */
            args_info->spread_daemon_orig = gengetopt_strdup (optarg);
          }
          /* Module id.  */
          else if (strcmp (long_options[option_index].name, "module-id") == 0)
          {
            if (local_args_info.module_id_given)
              {
                fprintf (stderr, "%s: `--module-id' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->module_id_given && ! override)
              continue;
            local_args_info.module_id_given = 1;
            args_info->module_id_given = 1;
            if (args_info->module_id_arg)
              free (args_info->module_id_arg); /* free previous string */
            args_info->module_id_arg = gengetopt_strdup (optarg);
            if (args_info->module_id_orig)
              free (args_info->module_id_orig); /* free previous string */
            args_info->module_id_orig = gengetopt_strdup (optarg);
          }
          /* Sensor id (should be a camera!).  */
          else if (strcmp (long_options[option_index].name, "sensor-id") == 0)
          {
            if (local_args_info.sensor_id_given)
              {
                fprintf (stderr, "%s: `--sensor-id' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->sensor_id_given && ! override)
              continue;
            local_args_info.sensor_id_given = 1;
            args_info->sensor_id_given = 1;
            if (args_info->sensor_id_arg)
              free (args_info->sensor_id_arg); /* free previous string */
            args_info->sensor_id_arg = gengetopt_strdup (optarg);
            if (args_info->sensor_id_orig)
              free (args_info->sensor_id_orig); /* free previous string */
            args_info->sensor_id_orig = gengetopt_strdup (optarg);
          }
          /* Base directory, created if needed. Output will be in <logs>/<output>/img-*.  */
          else if (strcmp (long_options[option_index].name, "log-path") == 0)
          {
            if (local_args_info.log_path_given)
              {
                fprintf (stderr, "%s: `--log-path' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->log_path_given && ! override)
              continue;
            local_args_info.log_path_given = 1;
            args_info->log_path_given = 1;
            if (args_info->log_path_arg)
              free (args_info->log_path_arg); /* free previous string */
            args_info->log_path_arg = gengetopt_strdup (optarg);
            if (args_info->log_path_orig)
              free (args_info->log_path_orig); /* free previous string */
            args_info->log_path_orig = gengetopt_strdup (optarg);
          }
          
          break;
        case '?':	/* Invalid option.  */
          /* `getopt_long' already printed an error message.  */
          goto failure;

        default:	/* bug: option not considered.  */
          fprintf (stderr, "%s: option unknown: %c%s\n", CMDLINE_PARSER_PACKAGE, c, (additional_error ? additional_error : ""));
          abort ();
        } /* switch */
    } /* while */




  cmdline_parser_release (&local_args_info);

  if ( error )
    return (EXIT_FAILURE);

  if (optind < argc)
    {
      int i = 0 ;

      args_info->inputs_num = argc - optind ;
      args_info->inputs =
        (char **)(malloc ((args_info->inputs_num)*sizeof(char *))) ;
      while (optind < argc)
        args_info->inputs[ i++ ] = gengetopt_strdup (argv[optind++]) ;
    }

  return 0;

failure:
  
  cmdline_parser_release (&local_args_info);
  return (EXIT_FAILURE);
}

#ifndef CONFIG_FILE_LINE_SIZE
#define CONFIG_FILE_LINE_SIZE 2048
#endif
#define ADDITIONAL_ERROR " in configuration file "

#define CONFIG_FILE_LINE_BUFFER_SIZE (CONFIG_FILE_LINE_SIZE+3)
/* 3 is for "--" and "=" */

char my_argv[CONFIG_FILE_LINE_BUFFER_SIZE+1];

int
cmdline_parser_configfile (char * const filename, struct gengetopt_args_info *args_info, int override, int initialize, int check_required)
{
  FILE* file;
  char linebuf[CONFIG_FILE_LINE_SIZE];
  int line_num = 0;
  int i, result, equal;
  char *fopt, *farg;
  char *str_index;
  size_t len, next_token;
  char delimiter;
  int my_argc = 0;
  char **my_argv_arg;
  char *additional_error;

  /* store the program name */
  cmd_line_list_tmp = (struct line_list *) malloc (sizeof (struct line_list));
  cmd_line_list_tmp->next = cmd_line_list;
  cmd_line_list = cmd_line_list_tmp;
  cmd_line_list->string_arg = gengetopt_strdup (CMDLINE_PARSER_PACKAGE);

  if ((file = fopen(filename, "r")) == NULL)
    {
      fprintf (stderr, "%s: Error opening configuration file '%s'\n",
               CMDLINE_PARSER_PACKAGE, filename);
      result = EXIT_FAILURE;
      goto conf_failure;
    }

  while ((fgets(linebuf, CONFIG_FILE_LINE_SIZE, file)) != NULL)
    {
      ++line_num;
      my_argv[0] = '\0';
      len = strlen(linebuf);
      if (len > (CONFIG_FILE_LINE_BUFFER_SIZE-1))
        {
          fprintf (stderr, "%s:%s:%d: Line too long in configuration file\n",
                   CMDLINE_PARSER_PACKAGE, filename, line_num);
          result = EXIT_FAILURE;
          goto conf_failure;
        }

      /* find first non-whitespace character in the line */
      next_token = strspn ( linebuf, " \t\r\n");
      str_index  = linebuf + next_token;

      if ( str_index[0] == '\0' || str_index[0] == '#')
        continue; /* empty line or comment line is skipped */

      fopt = str_index;

      /* truncate fopt at the end of the first non-valid character */
      next_token = strcspn (fopt, " \t\r\n=");

      if (fopt[next_token] == '\0') /* the line is over */
        {
          farg  = NULL;
          equal = 0;
          goto noarg;
        }

      /* remember if equal sign is present */
      equal = (fopt[next_token] == '=');
      fopt[next_token++] = '\0';

      /* advance pointers to the next token after the end of fopt */
      next_token += strspn (fopt + next_token, " \t\r\n");
      /* check for the presence of equal sign, and if so, skip it */
      if ( !equal )
        if ((equal = (fopt[next_token] == '=')))
          {
            next_token++;
            next_token += strspn (fopt + next_token, " \t\r\n");
          }
      str_index  += next_token;

      /* find argument */
      farg = str_index;
      if ( farg[0] == '\"' || farg[0] == '\'' )
        { /* quoted argument */
          str_index = strchr (++farg, str_index[0] ); /* skip opening quote */
          if (! str_index)
            {
              fprintf
                (stderr,
                 "%s:%s:%d: unterminated string in configuration file\n",
                 CMDLINE_PARSER_PACKAGE, filename, line_num);
              result = EXIT_FAILURE;
              goto conf_failure;
            }
        }
      else
        { /* read up the remaining part up to a delimiter */
          next_token = strcspn (farg, " \t\r\n#\'\"");
          str_index += next_token;
        }

      /* truncate farg at the delimiter and store it for further check */
      delimiter = *str_index, *str_index++ = '\0';

      /* everything but comment is illegal at the end of line */
      if (delimiter != '\0' && delimiter != '#')
        {
          str_index += strspn(str_index, " \t\r\n");
          if (*str_index != '\0' && *str_index != '#')
            {
              fprintf
                (stderr,
                 "%s:%s:%d: malformed string in configuration file\n",
                 CMDLINE_PARSER_PACKAGE, filename, line_num);
              result = EXIT_FAILURE;
              goto conf_failure;
            }
        }

    noarg:
      ++my_argc;
      len = strlen(fopt);

      strcat (my_argv, len > 1 ? "--" : "-");
      strcat (my_argv, fopt);
      if (len > 1 && ((farg &&*farg) || equal))
          strcat (my_argv, "=");
      if (farg && *farg)
          strcat (my_argv, farg);

      cmd_line_list_tmp = (struct line_list *) malloc (sizeof (struct line_list));
      cmd_line_list_tmp->next = cmd_line_list;
      cmd_line_list = cmd_line_list_tmp;
      cmd_line_list->string_arg = gengetopt_strdup(my_argv);
    } /* while */

  ++my_argc; /* for program name */
  my_argv_arg = (char **) malloc((my_argc+1) * sizeof(char *));
  cmd_line_list_tmp = cmd_line_list;
  for (i = my_argc - 1; i >= 0; --i) {
    my_argv_arg[i] = cmd_line_list_tmp->string_arg;
    cmd_line_list_tmp = cmd_line_list_tmp->next;
  }
  my_argv_arg[my_argc] = 0;

  additional_error = (char *)malloc(strlen(filename) + strlen(ADDITIONAL_ERROR) + 1);
  strcpy (additional_error, ADDITIONAL_ERROR);
  strcat (additional_error, filename);
  result =
    cmdline_parser_internal (my_argc, my_argv_arg, args_info, override, initialize, check_required, additional_error);

  free (additional_error);
  free (my_argv_arg);

conf_failure:
  if (file)
    fclose(file);
  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}
