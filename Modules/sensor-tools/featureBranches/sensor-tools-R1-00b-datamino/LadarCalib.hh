#ifndef _LADARCALIB_HH
#define _LADARCALIB_HH

#include "CapturePoints.hh"
#include "GeoUtils.hh"

// Yes/No continue query
bool waitContinue(string str = "Continue? (y/n): ");

// Captures a full 360 view around alice
string FullSweep();

// Captures a good view of the ground plane
string GroundSweep();

// Calibrates all ladars
void LadarCalib(string path);

// Calibrates PTU ladar to ground plane
void PTUCalib(string path);

// Calibrates PTU ladar to PTU by minimizing plane depths
void PTULadarCalib(string path);

#endif
