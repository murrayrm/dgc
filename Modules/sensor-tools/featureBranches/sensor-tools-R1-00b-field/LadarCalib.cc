#include <fstream>
#include "LadarCalib.hh"
#include <alice/AliceConstants.h>
#include <frames/quat.h>

bool waitContinue(string str)
{
    cout << str;
    char s;
    cin >> s;
    return (s=='y');
}

string FullSweep()
{
    CapturePoints capture;
    string s(capture.getLogPath());

    sleep(4);
    capture.printActive();

    if (!waitContinue())
	return s;

    float degmax = 38;
    float degmin = -75;
    float pan=-120;
    float delta=60;
    float dsmall=0.33f;
    int stepcount=5;
    

    capture.commandPTU(pan,degmax,10,10,true);
    sleep(1);    
    capture.startCapture();
    for (int i=0;i<stepcount;i++)
    {
	capture.commandPTU(pan,degmin,1,1,true);
	pan += dsmall;
	capture.commandPTU(pan,degmin,1,1,true);
	capture.commandPTU(pan,degmax,1,1,true);
	pan += dsmall + delta;
	if (pan < 150)
	    capture.commandPTU(pan,degmax,5,5,true);
    }
    capture.stopCapture();

    return s;
}

string GroundSweep()
{
    CapturePoints capture;
    string s(capture.getLogPath());

    sleep(4);
    capture.printActive();

    if (!waitContinue())
	return s;

    float degmax = 0;
    float degmin = -75;
    float pan=-120;
    float delta=60;
    float dsmall=0.33f;
    int stepcount=5;
    

    capture.commandPTU(pan,degmax,10,10,true);
    sleep(1);    
    capture.startCapture();
    for (int i=0;i<stepcount;i++)
    {
	capture.commandPTU(pan,degmin,1,1,true);
	pan += dsmall;
	capture.commandPTU(pan,degmin,1,1,true);
	capture.commandPTU(pan,degmax,1,1,true);
	pan += dsmall + delta;
	if (pan < 150)
	    capture.commandPTU(pan,degmax,5,5,true);
    }
    capture.stopCapture();
    return s;
}

void LadarCalib(string path)
{
}


void PTULadarCalib(string path)
{
    const char *cpath = path.c_str();
    vertrpy ptu_iter = LoadSensor(cpath, "SENSNET_PTU_LADAR");
    vertrpy ptu_cur = ptu_iter;
    // backup original config
    CopyFile(cpath,"SENSNET_PTU_LADAR.CFG","SENSNET_PTU_LADAR.BAK");

    vector<tvert3> roiPt;
    vector<float> roiRad;

    // Load ROI info
    ifstream roi((path + "ROI").c_str());
    tvert3 pt;
    float rad;
    while (roi.good())
    {
	rad = -1;
	roi >> pt.x >> pt.y >> pt.z >> rad;
	if (rad > 0) //we actually read
	{
	    roiPt.push_back(pt);
	    roiRad.push_back(rad);
	    printf("ROI Point: %f %f %f %f\n",pt.x,pt.y,pt.z,rad);
	}
    }

    float curmin=0,curscore=0;
    vertrpy ptu_min = ptu_iter;
    tvert3 *points;
    // Load scans to allocate memory
    int count = LoadPTUScans(cpath, &points);
    float plane[4];

    for (int t=0;t<(int)roiRad.size();t++)
	curmin += PlaneFit(GetNearbyPointsLin(roiPt[t],points,count,roiRad[t]),plane);

    float delta[6] = {0.001f, 0.001f, 0.001f, 0.001f, 0.001f, 0.001f};

    // begin iterations
    while (true)
    {
	printf("Value before iter: %f %f %f %f %f %f: %f\n",ptu_iter.x,ptu_iter.y,ptu_iter.z,
	       ptu_iter.roll,ptu_iter.pitch,ptu_iter.yaw,curmin);
	// loop through each variable, test each change
	float *var;
	for (int i=0;i<6;i++)
	{
	    // test each possible setting
	    for (int j=-1;j<=1;j++)
	    {
		printf("Testing %d, %d...\n",i,j);
		ptu_cur = ptu_iter;
		var = (float*)(&ptu_cur) + i;
		*var += j*delta[i];
		// save settings and reload
		SaveSensor(cpath,"SENSNET_PTU_LADAR",ptu_cur);
		LoadPTUScans(cpath, &points, count);
		// compute score
		curscore = 0;
		for (int t=0;t<(int)roiRad.size();t++)
		    curscore += PlaneFit(GetNearbyPointsLin(roiPt[t],points,count,roiRad[t]),plane);
		if (curscore < curmin)
		{
		    ptu_min = ptu_cur;
		    curmin = curscore;
		}
	    }
	}
	ptu_iter = ptu_min;
	printf("Value after iter: %f %f %f %f %f %f: %f\n",ptu_iter.x,ptu_iter.y,ptu_iter.z,
	       ptu_iter.roll,ptu_iter.pitch,ptu_iter.yaw,curmin);
	if (!waitContinue())
	    break;
    }
    SaveSensor(cpath,"SENSNET_PTU_LADAR",ptu_iter);
    if (!waitContinue("Keep new settings? (y/n): "))
    {
	CopyFile(cpath,"SENSNET_PTU_LADAR.BAK","SENSNET_PTU_LADAR.CFG");
	printf("Reverted to original config.\n");
    }
}

void PTUCalib(string path)
{
    const char *cpath = path.c_str();
    // backup original config
    CopyFile(cpath,"SENSNET_MF_PTU.CFG","SENSNET_MF_PTU.BAK");
    vertrpy ptu = LoadSensor(cpath,"SENSNET_MF_PTU");

    vector<tvert3> roiPt;
    vector<float> roiRad;

    // Load ROI info
    ifstream roi((path + "ROI").c_str());
    tvert3 pt;
    float rad;
    while (roi.good())
    {
	rad = -1;
	roi >> pt.x >> pt.y >> pt.z >> rad;
	if (rad > 0) //we actually read
	{
	    roiPt.push_back(pt);
	    roiRad.push_back(rad);
	    printf("ROI Point: %f %f %f %f\n",pt.x,pt.y,pt.z,rad);
	}
    }

    tvert3 *points;
    // Load scans to allocate memory
    int count = LoadPTUScans(cpath, &points);
    float plane[4];

    // collect all of our nearby points
    vector<tvert3*> planepts;
    for (int t=0;t<(int)roiRad.size();t++)
    {
	vector<tvert3*> temp = GetNearbyPointsLin(roiPt[t],points,count,roiRad[t]);
	planepts.insert(planepts.end(),temp.begin(),temp.end());
    }

    printf("Initial PTU position: %f %f %f | %f %f %f\n",ptu.x,ptu.y,ptu.z,
	   ptu.roll,ptu.pitch,ptu.yaw);

    printf("Fitting ground plane to %d points...\n", (int)planepts.size());
    PlaneFit(planepts,plane);
    ptu.z = -fabs(PlaneDist(tvert3(ptu.x,ptu.y,ptu.z),plane))+VEHICLE_TIRE_RADIUS;

    tvert3 pnorm = PlaneNormal(plane);
    if (pnorm.z > 0) //pointing wrong way!
	pnorm = scale3(pnorm,-1);
    tvert3 axis = norm3(cross3(pnorm,tvert3(0,0,-1)));
    double theta = acos(dot3(pnorm,tvert3(0,0,-1)));

    quat_t ptuquat = quat_from_rpy(ptu.roll,ptu.pitch,ptu.yaw);
    quat_t rotquat = quat_set(cos(0.5*theta),axis.x*sin(0.5*theta),
			      axis.y*sin(0.5*theta),axis.z*sin(theta/2));
    quat_t qout = quat_mul(ptuquat,rotquat);
    double vals[3];
    quat_to_rpy(qout,vals,vals+1,vals+2);
    ptu.roll = (float)vals[0];
    ptu.pitch = (float)vals[1];
    ptu.yaw = (float)vals[2];

    printf("Final PTU position: %f %f %f | %f %f %f\n",ptu.x,ptu.y,ptu.z,
	   ptu.roll,ptu.pitch,ptu.yaw);
    if (waitContinue("Accept final position? (y/n): "))
	SaveSensor(cpath,"SENSNET_MF_PTU",ptu);
}

int main(int argc, char ** argv)
{
    cout << "LADAR calibration utility by Tamas Szalay" << endl;
    int opt = 0;
    string lastpath;

    while (true)
    {
	cout << endl << "Please enter your option: " << endl;
	cout << "0 - Quit" << endl;
	cout << "1 - Capture full sweep" << endl;
	cout << "2 - Capture ground sweep" << endl;
	cout << "3 - Calibrate PTU LADAR" << endl;
	cout << "4 - Calibrate PTU to ground" << endl;
	cout << "5 - Calibrate other LADARs" << endl;
	cout << endl << "Option: ";
	cin >> opt;
	// query path?
	if (opt > 2)
	{
	    if (lastpath == "" || !waitContinue("Use path '" + lastpath + 
						"' ? (y/n): "))
	    {
		cout << "Enter path: /tmp/logs/ladar-calib/";
		cin >> lastpath;
		lastpath = "/tmp/logs/ladar-calib/" + lastpath;
	    }
	}
	switch (opt)
	{
	case 0:
	    return 0;
	    break;
	case 1:
	    lastpath = FullSweep();
	    break;
	case 2:
	    lastpath = GroundSweep();
	    break;
	case 3:
	    PTULadarCalib(lastpath);
	    break;
	case 4:
	    PTUCalib(lastpath);
	    break;
	case 5:
	    LadarCalib(lastpath);
	    break;
	default:
	    cout << "Unrecognized option." << endl;
	    break;
	}
    }
    return 0;
}
