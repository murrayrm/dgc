/* 
 * Desc: Simple perceptor that listens for image data and writes a 
 * reduced version of that on disc (smaller scale, monochromatic, slower
 * framerate).
 * Date: 25 October 2007
 * Author: Daniele Tamino (based on sensnet_test.c by Andrew Howard)
 */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>

// opencv headers
#define CV_NO_BACKWARD_COMPATIBILITY // this avoids a lot of warnings
#include <cv.h>
#include <highgui.h>

// DGC headers
#include <interfaces/StereoImageBlob.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>

#include "save_video_cmdline.h"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Get time in microseconds
static uint64_t gettime()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}


volatile sig_atomic_t quit = 0;

void sigintHandler(int sig)
{
    (void) sig;
    // if the user presses CTRL-C three or more times, and the program still
    // doesn't terminate, abort
    if (quit > 2) {
        abort();
    }
    quit++;
}

/*
 * This function is used, when the debugging flag is on, to force OpenCV to abort and
 * generate a core dump in case of error.
 */
int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                            const char* file_name, int line, void* userdata)
{
    cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
    abort();
}


int main(int argc, char *argv[])
{
    int sensorId, blobType, blobId;
    sensnet_t *sensnet;
    int blobSize;
    char *blob;
    StereoImageBlob* sib = NULL;

    uint64_t totalTime = 0;
    const int avgCount = 8;

    char filename[1024];
    char outdir[1024];
    char timestr[64];

    const char *spreadDaemon;
    modulename moduleId;
    int skynetKey;

    struct gengetopt_args_info options;
    int outcols, outrows;
    int outchan; // 1 = mono, 3 = color
    int divider; // frame rate divider

    IplImage imgInObj;
    IplImage* imgTmp = NULL;
    IplImage *imgTmp2 = NULL;
    int count = 0, outcount = 0;

    cvRedirectError(abortOnCvError, NULL, NULL);

    // catch CTRL-C and exit cleanly, if possible
    signal(SIGINT, sigintHandler);
    // and SIGTERM, i.e. when someone types "kill <pid>" or the like
    signal(SIGTERM, sigintHandler);
    // when a terminal (or screen session) gets killed , we receive SIGHUP
    signal(SIGHUP, sigintHandler);

    // Load options
    if (cmdline_parser(argc, argv, &options) < 0)
	return -1;
    
    // Fill out the spread daemon name
    if (options.spread_daemon_given)
	spreadDaemon = options.spread_daemon_arg;
    else if (getenv("SPREAD_DAEMON"))
	spreadDaemon = getenv("SPREAD_DAEMON");
    else
	return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
    
    // Fill out the skynet key
    if (options.skynet_key_given)
	skynetKey = options.skynet_key_arg;
    else if (getenv("SKYNET_KEY"))
	skynetKey = atoi(getenv("SKYNET_KEY"));
    else
	skynetKey = 0;
    
    // Fill out module id
    moduleId = modulenamefromString(options.module_id_arg);
    if (moduleId <= 0)
	return ERROR("invalid module id: %s", options.module_id_arg);

    // remove trailing /, if any
    int len = strlen(options.logs_arg);
    if (options.logs_arg[len - 1] == '/')
        options.logs_arg[len - 1] = '\0';

    // check that <logs> exists and is a directory
    struct stat st;
    if (stat(options.logs_arg, &st) < 0) {
        return ERROR("Cannot stat %s: %s", options.logs_arg, strerror(errno));
    } else if (!S_ISDIR(st.st_mode)) {
        return ERROR("%s exists, but is not a directory", options.logs_arg);
    }

    // format the output directory name, adding a timestamp
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    snprintf(outdir, sizeof(outdir), "%s/%s-%s",
             options.logs_arg, options.output_arg, timestr);
    
    // remove trailing /, if any
    len = strlen(outdir);
    if (outdir[len - 1] == '/')
        outdir[len - 1] = '\0';

    // check that <logs>/<output> doesn't exists already
    if (stat(outdir, &st) < 0) {
        if (errno == ENOENT) {
            // good, create it
            if (mkdir(outdir, 0775) < 0)
                return ERROR("Cannot create %s: %s", outdir, strerror(errno));
        } else {
            return ERROR("Cannot stat %s: %s", outdir, strerror(errno));
        }
    } else {
        if (!S_ISDIR(st.st_mode))
            return ERROR("%s exists, but is not a directory", outdir);
        else
            return ERROR("%s exists, refusing to mix two logs", outdir);
    }

    outcols = options.width_arg;
    outrows = options.height_arg;
    outchan = options.channels_arg;
    divider = options.divider_arg;

    // imgTmp is initialized later (need to know the input # of channels)
    imgTmp2 = cvCreateImage(cvSize(outcols, outrows), IPL_DEPTH_8U, outchan);

    blobSize = sizeof(StereoImageBlob);

    sensorId = sensnet_id_from_name(options.sensor_id_arg);
    if (sensorId <= SENSNET_NULL_SENSOR)
        return ERROR("invalid sensor id: %s", options.sensor_id_arg);

    blobType = SENSNET_STEREO_IMAGE_BLOB;
    blob = malloc(blobSize);

    // Connect to sensnet
    sensnet = sensnet_alloc();
    if (sensnet_connect(sensnet, spreadDaemon, skynetKey, MODsupercon) < 0)
        return ERROR("Cannot connect to sensnet (sensnet_connect failed)!!!");

    // Join an existing group
    if (sensnet_join(sensnet, sensorId, blobType, blobSize) < 0)
        return ERROR("Cannot join group (sensnet_join failed)!!!");
    
    MSG("waiting");

    uint64_t startTs = 0;

    while (!quit)
    {
        int ret = sensnet_wait(sensnet, 100);
        if (ret == ETIMEDOUT)
            continue;
        if (ret != 0)
            break;

        uint64_t time = gettime();

        if (sensnet_read(sensnet, sensorId, blobType, &blobId, blobSize, blob) < 0)
            break;

        if (count++ % divider != 0) {
            MSG("received blob with id %d, skipped", blobId);
            continue;
        } else {
            // TODO: check if we're paused, and if so, don't save!
        }
        
        sib = (StereoImageBlob*) blob;
        assert(sib->version >= 4);
        if (sib->version != 4)
            MSG("Received stereo blob with version >= 4, continuing anyway");
      
        if (imgTmp == NULL)
        {
            // one time initialization, hoping that all blobs will have the same # of channels
            imgTmp = cvCreateImage(cvSize(outcols, outrows), IPL_DEPTH_8U, sib->channels);
        }
    
        IplImage* imgIn = cvInitImageHeader(&imgInObj, cvSize(sib->cols, sib->rows),
                                            IPL_DEPTH_8U, sib->channels, IPL_ORIGIN_TL, 4);
        cvSetData(imgIn, sib->imageData + sib->leftOffset, sib->cols * sib->channels);

        IplImage* img = imgIn;

        if (sib->rows != outrows || sib->cols != outcols)
        {
            cvResize(img, imgTmp, CV_INTER_AREA); // experiment with CV_INTER_{LINEAR,AREA,CUBIC} too
            img = imgTmp;
        }

        if (sib->channels != outchan)
        {
            cvCvtColor(img, imgTmp2, CV_RGB2GRAY);
            img = imgTmp2;
        }

        if (outchan == 3)
            cvCvtColor(img, img, CV_RGB2BGR);
            

        if (startTs == 0)
            startTs = sib->timestamp;


        // add a count to the filename
        //snprintf(filename, sizeof(filename), "%s/img%010d.%s",
        //         outdir, outcount, options.format_arg);

        // uncomment this to use relative timestamps
        //long timePassed = (long) (sib->timestamp - startTs);
        //snprintf(filename, sizeof(filename), "%s/img%020ld.%s",
        //         outdir, timePassed, options.format_arg);

        // uncomment this to use absolute timestamps
        //snprintf(filename, sizeof(filename), "%s/img%020lld.%s",
        //         outdir, sib->timestamp, options.format_arg);

        // just do both, count and timestamp!
        snprintf(filename, sizeof(filename), "%s/img-%010lld-%06d.%s",
                 outdir, sib->timestamp, outcount, options.format_arg);

        // save the image in the specified format
        cvSaveImage(filename, img);

        time = gettime() - time;

        MSG("received blob with id %d, saved %s", blobId, filename);

        totalTime += time;
        outcount++;
        if (outcount % avgCount == 0) {
            MSG("Average loop time (read, convert and save): %.10f ms",
                (double) totalTime / avgCount * 1e-3);
            totalTime = 0;
        }
    }
  
    if (imgTmp)
        cvReleaseImage(&imgTmp);
    if (imgTmp2)
        cvReleaseImage(&imgTmp2);
  
    if (sensnet_leave(sensnet, sensorId, blobType) < 0)
        return -1;
  
    sensnet_disconnect(sensnet);
    sensnet_free(sensnet);
  
    free(blob);

    MSG("exiting cleanly");
    
    return 0;
}
