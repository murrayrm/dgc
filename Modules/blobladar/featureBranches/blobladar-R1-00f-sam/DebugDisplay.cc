/**
 * File: DebugDisplay.cc
 * Description:
 *   Handles all the openGL display functions for
 *   blobladar. Most of the code is directly from
 *   Jeremy's demo code
 * Last Changed: March 9
 **/

#include "DebugDisplay.hh"

#define BLOB_RED        0
#define BLOB_ORANGE     1
#define BLOB_YELLOW     2
#define BLOB_GREEN      3
#define BLOB_BLUE       4
#define BLOB_BLACK      5

//mutex for changing any of the ladar_xxx vectors
pthread_mutex_t mutex1;

//whether to post redisplay
int flag = 0;

//data structures for plotting
vector<ladar_point> ladar_points;
vector<ladar_object> ladar_objects;
vector<ladar_line> ladar_lines;
vector<ladar_car> ladar_cars;

//OpenGL display variables
float xrot = 0, yrot = 0, angle=0.0;
float  lastx,  lasty;
bool  lbuttondown = false;
float  xpos = 0,  ypos = 0,  zpos = 0;

double colorLookup[6][3];

const int GRIDLINES = 1;

/**
 * Function that takes care of the actual plotting.
 * called every iteration of the main loop
 **/
void display(void);

/**
 * The following were written by Jeremy, to handle
 * human interface, etc. 
 * they're the private functions for the display class
 **/
void keyboard(unsigned char key, int x, int y);
void resize(int w, int h);
void idle(void);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void motionPassive(int x, int y);


LindzeyDisplay::LindzeyDisplay(int *argc, char **argv)
{
  glEnable(GL_DEPTH_TEST);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);

  //initializing color lookup
  //red
  colorLookup[BLOB_RED][0] = 1.0f;
  colorLookup[BLOB_RED][1] = 0.0f;
  colorLookup[BLOB_RED][2] = 0.0f;
  //orange
  colorLookup[BLOB_ORANGE][0] = 1.0f;
  colorLookup[BLOB_ORANGE][1] = 0.5f;
  colorLookup[BLOB_ORANGE][2] = 0.0f;
  //yellow
  colorLookup[BLOB_YELLOW][0] = 1.0f;
  colorLookup[BLOB_YELLOW][1] = 1.0f;
  colorLookup[BLOB_YELLOW][2] = 0.0f;
  //green
  colorLookup[BLOB_GREEN][0] = 0.0f;
  colorLookup[BLOB_GREEN][1] = 1.0f;
  colorLookup[BLOB_GREEN][2] = 0.0f;
  //blue
  colorLookup[BLOB_BLUE][0] = 0.0f;
  colorLookup[BLOB_BLUE][1] = 0.0f;
  colorLookup[BLOB_BLUE][2] = 1.0f; 
  //black
  colorLookup[BLOB_BLACK][0] = 1.0f;
  colorLookup[BLOB_BLACK][1] = 1.0f;
  colorLookup[BLOB_BLACK][2] = 1.0f;

  arg1 = argc;
  arg2 = argv;

}

/**
 * Default Destructor
 **/
LindzeyDisplay::~LindzeyDisplay()
{
  return;
}

/**
 * 
 **/
void LindzeyDisplay::clearObjects()
{
  ladar_objects.clear();
  ladar_lines.clear();
  ladar_cars.clear();
  return;
}

void LindzeyDisplay::addCar(double *corners)
{
  ladar_car temp_car;

  temp_car.rear_r_x = SCALING*corners[0];
  temp_car.rear_r_y = SCALING*corners[1];
  temp_car.rear_l_x = SCALING*corners[2];
  temp_car.rear_l_y = SCALING*corners[3];
  temp_car.front_r_x = SCALING*corners[4];
  temp_car.front_r_y = SCALING*corners[5];
  temp_car.front_l_x = SCALING*corners[6];
  temp_car.front_l_y = SCALING*corners[7];

  pthread_mutex_lock(&mutex1);
  ladar_cars.push_back(temp_car);
  pthread_mutex_unlock(&mutex1);
  return;

}

void LindzeyDisplay::addObject(double x, double y, double r, int age)
{
  ladar_object temp_object;
  temp_object.x = SCALING*x;
  temp_object.y = SCALING*y;
  temp_object.r = SCALING*r;
  temp_object.color = min(NUM_COLORS,(int)floor(age/AGE_STEP));
  pthread_mutex_lock(&mutex1);
  ladar_objects.push_back(temp_object);
  pthread_mutex_unlock(&mutex1);
  return;
}

void LindzeyDisplay::sendLines(int numlines, double *x1, double *x2, double *y1, double *y2)
{

  flag = 1;
  ladar_line temp_line;
  pthread_mutex_lock(&mutex1);
  ladar_lines.clear();
  for(int i=0; i<numlines;i++)
  {
    temp_line.x1 = SCALING*x1[i];
    temp_line.x2 = SCALING*x2[i];
    temp_line.y1 = SCALING*y1[i];
    temp_line.y2 = SCALING*y2[i];
    temp_line.color = 5;
    ladar_lines.push_back(temp_line);
  }
  pthread_mutex_unlock(&mutex1);
 
 return;

}
void LindzeyDisplay::sendLine(double x1, double x2, double y1, double y2, int color)
{

  flag = 1;
  ladar_line temp_line;
  temp_line.x1 = SCALING*x1;
  temp_line.x2 = SCALING*x2;
  temp_line.y1 = SCALING*y1;
  temp_line.y2 = SCALING*y2;
  temp_line.color = color;
  pthread_mutex_lock(&mutex1);
  ladar_lines.push_back(temp_line);
  pthread_mutex_unlock(&mutex1);
 
 return;

}

void LindzeyDisplay::sendScan(int numPts, double *xpts, double *ypts, double *zpts)
{
  ladar_point temp_point;
  pthread_mutex_lock(&mutex1);

  //receiving a new scan, discard old points
  ladar_points.clear(); 
  for(int i=0; i<numPts; i++)
  {
    temp_point.x = SCALING*xpts[i];
    temp_point.y = SCALING*ypts[i];
    temp_point.z = SCALING*zpts[i];

    ladar_points.push_back(temp_point);
  }
  //  fprintf(stderr, "received %d points \n", numPts);
  pthread_mutex_unlock(&mutex1);

  return;
}


/**
 * intializes openGL functions copied from main() 
 * loop of jeremy's demo code 
 **/
void LindzeyDisplay::mainOpenGL()
{

    glutInit(arg1, arg2);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(800, 600);
    glutCreateWindow("BlobLadar Display");

    //glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
    glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
    glColor3f(1.0, 1.0, 1.0);

    glEnable(GL_DEPTH_TEST);

    /* set callback functions */
    glutDisplayFunc(display); 

    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutPassiveMotionFunc(motionPassive);

    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);
    glutIdleFunc(idle);

    glutMainLoop();
	 
    return;

}


void display(void)
{
  glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
  //  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer
  glLoadIdentity();

  //camera position: at x,y,z, looking at x,y,z, up x,y,z
  gluLookAt (0.0, 0.0, 30.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

  glRotatef(xrot,1.0,0.0,0.0); //rotate our camera on the x-axis (left and right)
  glRotatef(yrot,0.0,1.0,0.0); //rotate our camera on the y-axis (up and down)
  glTranslated(- xpos,- ypos,- zpos); //translate the screen to the position of our camera

  glEnable (GL_DEPTH_TEST); //enable the depth testing

  //====================
  //draw fine grid
  //====================
  if(GRIDLINES)
    {
      //vertical lines
      glLineWidth (0.5);
      glColor3f (0.0F, 0.0F, 0.0F);
      glBegin (GL_LINES);
      for (int i= XMIN; i < XMAX+1; i=i+2)
	{
	  glVertex3f (i, ZMIN,0);
	  glVertex3f (i, ZMAX,0);
	};
      glEnd ();
      
      //horizontal lines
      glLineWidth (0.5);
      glColor3f (0.0F, 0.0F, 0.0F);
      glBegin (GL_LINES);
      for (int i= ZMIN; i < ZMAX+1; i=i+2)
	{
	  glVertex3f ( XMIN, i, 0);
	  glVertex3f ( XMAX, i, 0);
	};
      glEnd ();
    }
  
  //====================
  //draw coordinate axes
  //====================

  glLineWidth (1);
  glColor3f (0.0F, 0.0F, 1.0F);
  glBegin(GL_LINES);
  glVertex3f(XMIN,0,0);
  glVertex3f(XMAX,0,0);
  glVertex3f(0,ZMAX, 0);
  glVertex3f(0,ZMIN, 0);
  glEnd();
  

  //flag keeps track of whether we need to replot data
  if(flag==1)
    {

      //============START MUTEX LOCK=============
      pthread_mutex_lock(&mutex1);

      int tempcolor;

      //********DRAWING LADAR SCAN***********/
      glColor3f(0.5F, 0.0F, 0.5F);
      glBegin(GL_POINTS);
      for(unsigned int n = 0; n < ladar_points.size(); n++)
	{
	  glVertex3f(ladar_points[n].x,ladar_points[n].y,0);
	}
      glEnd();
        
      /***********DRAWING SEGMENTATIONS******/
      glColor3f(1.0F, 0.0F, 0.0F); 
      glLineWidth(2);
      glBegin(GL_LINES);
      for(unsigned int n=0; n<ladar_lines.size(); n++)
	{
          tempcolor = ladar_lines[n].color;
	  //FIXME: why can't i use the lookup table here??
	  glColor3f(0.0f, 0.0f, 0.0f);
	  //	glColor3f(colorLookup[tempcolor][0], colorLookup[tempcolor][1], colorLookup[tempcolor][2]);
	  glVertex3f(ladar_lines[n].x1,ladar_lines[n].y1,0);
	  glVertex3f(ladar_lines[n].x2,ladar_lines[n].y2,0);

	}
      glEnd();

      /***********PLOTTING OBJECTS************/

      glBegin(GL_QUADS);

      double tempx, tempy, tempr;
      for(unsigned int n=0; n<ladar_objects.size();n++)
      {
	tempx = ladar_objects[n].x;
	tempy = ladar_objects[n].y;
	tempr = ladar_objects[n].r;
	tempcolor = ladar_objects[n].color;
	glColor3f(colorLookup[tempcolor][0], colorLookup[tempcolor][1], colorLookup[tempcolor][2]);
	glVertex3f(tempx,tempy+tempr,0);
	glVertex3f(tempx+tempr, tempy, 0);
	glVertex3f(tempx,tempy-tempr,0);
	glVertex3f(tempx-tempr, tempy, 0);

      }
      glEnd();

      /************* DRAWS CARS*********/
      //drawing a black box for each car
      glBegin(GL_QUADS);

      double rlx, rly, rrx, rry, flx, fly, frx, fry;
      for(unsigned int n=0; n<ladar_cars.size();n++)
      {
	rlx = ladar_cars[n].rear_l_x;
	rly = ladar_cars[n].rear_l_y;
	rrx = ladar_cars[n].rear_r_x;
	rry = ladar_cars[n].rear_r_y;
        flx = ladar_cars[n].front_l_x;
	fly = ladar_cars[n].front_l_y;
	frx = ladar_cars[n].front_r_x;
	fry = ladar_cars[n].front_r_y;

	glColor3f(colorLookup[BLOB_BLACK][0], colorLookup[BLOB_BLACK][1], colorLookup[BLOB_BLACK][2]);
	glVertex3f(rlx, rly, 0);
	glVertex3f(rrx, rry, 0);
	glVertex3f(frx, fry, 0);
	glVertex3f(flx, fly, 0);
      }
      glEnd();

      //colored corners for each car
      glBegin(GL_POINTS);
      for(unsigned int n = 0; n < ladar_cars.size(); n++)
	{
	  glVertex3f(ladar_cars[n].front_l_x,ladar_cars[n].front_l_y,BLOB_RED);
	  glVertex3f(ladar_cars[n].front_r_x,ladar_cars[n].front_r_y,BLOB_ORANGE);
	  glVertex3f(ladar_cars[n].rear_l_x,ladar_cars[n].rear_l_y,BLOB_GREEN);
	  glVertex3f(ladar_cars[n].rear_r_y,ladar_cars[n].rear_r_y,BLOB_BLUE);
	}
      glEnd();


      //============STOP MUTEX LOCK==============
      pthread_mutex_unlock(&mutex1);
    }
  
  glutSwapBuffers();
  
}

void idle(void)
{
  /* Check flag variable */

  //============START MUTEX LOCK=============
  pthread_mutex_lock(&mutex1);

  if(flag==1)
	glutPostRedisplay();
			  
  //============STOP  MUTEX LOCK=============
  pthread_mutex_unlock(&mutex1);

}


void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	   lbuttondown = true;
	}
      else
	{
	   lbuttondown = false;
	}
    }
}

void motion(int x, int y)
{
  if ( lbuttondown)
    {
      float diffx=(x- lastx)/10; //check the difference between the current x and the last x position
      float diffy=(y- lasty)/10; //check the difference between the current y and the last y position  
       lastx=x; //set  lastx to the current x position
       lasty=y; //set  lasty to the current y position
      xrot += diffy; //set the xrot to xrot with the addition of the difference in the y position
      yrot += diffx; //set the xrot to yrot with the addition of the difference in the x position
      glRotatef(xrot,1.0,0.0,0.0); //rotate our camera on teh x-axis (left and right)
      glRotatef(yrot,0.0,1.0,0.0); //rotate our camera on the y-axis (up and down)
      display();
    }

  
}

void motionPassive(int x, int y)
{
   lastx = x;
   lasty = y;
}
void keyboard(unsigned char key, int x, int y)
{
  /* This time the controls are:
     
  "a": move left
  "d": move right
  "w": move forward
  "s": move back
  "t": toggle depth-testing
  
  */
  
  
  if (key=='2')
    {
      float xrotrad, yrotrad;
      yrotrad = (yrot / 180 * M_PI);
      xrotrad = (xrot / 180 * M_PI);
       xpos += float(sin(yrotrad)/10) ;
       zpos -= float(cos(yrotrad)/10) ;
       ypos -= float(sin(xrotrad)/10) ;
    }
  
  if (key=='8')
    {
      float xrotrad, yrotrad;
      yrotrad = (yrot / 180 * M_PI);
      xrotrad = (xrot / 180 * M_PI);
       xpos -= float(sin(yrotrad)/10);
       zpos += float(cos(yrotrad)/10) ;
       ypos += float(sin(xrotrad)/10);
    }
  
  if (key=='4')
    {
      float yrotrad;
      yrotrad = (yrot / 180 * M_PI);
       xpos += float(cos(yrotrad)/10);
       zpos += float(sin(yrotrad)/10);
    }
  
  if (key=='6')
    {
      float yrotrad;
      yrotrad = (yrot / 180 * M_PI);
       xpos -= float(cos(yrotrad)/10);
       zpos -= float(sin(yrotrad)/10);
    }


  if (key==27)
    {
      exit(0);
    }

}



void resize(int w, int h)
{
	//if (height == 0) height = 1;
	flag = 1;
	glViewport (0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
	glMatrixMode (GL_PROJECTION); //set the matrix to projection
	glLoadIdentity ();
	gluPerspective (60, (GLfloat)w / (GLfloat)h, 1.0, 1000.0); //set the perspective (angle of sight, width, height, , depth)
	glMatrixMode (GL_MODELVIEW); //set the matrix back to model

}
