/**
 * File: BlobLadar.hh
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 9
 **/

using namespace std;

#include "DebugDisplay.hh"
#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>
#include <frames/pose3.h>
#include <frames/coords.hh>
#include "Matrix.hh"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

//openGL support
#include <GL/glut.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include "dgcutils/DGCutils.hh" 
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "interfaces/MapElementMsg.h"

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

#define PI 3.14159265
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define MAXTRAVEL 2 //max dist obj can travel btwn frames

#define MAXNUMCARS 50 //max num cars we can track
#define MAXNUMOBJS 100

#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define NUMSCANPOINTS 181
#define SCALINGFACTOR 4 //used in determining 
                 //discontinuities for segmentation

//#warning "this should be replaced by some probabilistic calculation involving covariances of KF"
#define CARMARGIN 1.0 //if it's this close to outline of 
   //a car, classify it as part
#define RADIALOFFSET 1.0 //how far back to put additional points

#define MINSPEED 2.0   //min velocity to bother sending out
#define MAXSPEED 20.0 //max velocity 
#define MINDIMENSION 2 //we're assuming this is the minimum car dimension 
                       //used when we can only see 1 side...
#define OBJECTTIMEOUT 100 //how many loops before we delete an object
#define CARTIMEOUT 300 //how many loops before we delete a car

//for accessing the rawData array
#define ANGLE 0
#define RANGE 1


/**
 * This struct is how we store all the information about a car that we ever use
**/
struct carRepresentation{

  //unique ID # for this car (used by mapping...)
  int ID;

  //for storing the car's corner point, and the endpoints of segments
  double Y,dy1,dy2;
  double X,dx1,dx2;
  double len1, len2;

  //for storing the Car's history (mapper & prediction may want this)
  double lastY[10];
  double lastX[10];
  int histPointer; //this (mod 10) is the spot we're at in the history

  //the last scan we saw the car at, and how many times
  int lastSeen;
  int timesSeen;

  //whether this end is in foreground or background
  // >0 means in fg, < 0 means in bg, =0 means scan edge
  double dStart, dEnd;

  //whether we've actually seen front/back of car 
  //(useful for updating cars)
  int seenFront, seenBack;

  //whether we actually fit a corner to the data, 
  //or added one based on where it should be
  int cornerSeen;

  //matrices for KF estimates.
  // note that N and E are treated independently
  Matrix Xmu;
  Matrix Xmu_hat;
  Matrix Xsigma;
  Matrix Xsigma_hat;

  Matrix Ymu;
  Matrix Ymu_hat;
  Matrix Ysigma;
  Matrix Ysigma_hat;

};



/**
 * If we're tracking objects rather than cars, this struct is how we keep
 * track of all relevant information
 **/
struct objectRepresentation {
  //the center of mass of this object

  double X,Y;
  int ID;

  //histories of the points, ranges and angles for this car
  vector<vector<point2> > scans;
  vector<vector<double> > ranges;
  vector<vector<double> > angles;

  //how many times we've seen it, and when the last one was
  int timesSeen;
  int lastSeen;

  //start and end distances (+ means car is in foreground, 
  //0 means at edge of scan, - means car is in background)
  double dStart, dEnd; 

  //matrices for KF estimates...

  Matrix Xmu;
  Matrix Xmu_hat;
  Matrix Xsigma;
  Matrix Xsigma_hat;

  Matrix Ymu;
  Matrix Ymu_hat;
  Matrix Ysigma;
  Matrix Ysigma_hat;


};

struct lineSegmentFit {
  double a;
  double b;
  double x1;
  double x2;
  double y1;
  double y2;

};

struct myState {
  double X;
  double Y;
  double Z;
};

// LADAR blob perceptor class
class BlobLadar : public CMapElementTalker
{
  public:

  // Constructor
  BlobLadar();

  // Destructor
  ~BlobLadar();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Update the map
  int update();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, BlobLadar *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, BlobLadar *self, const char *token);


  //useful function to calculate dist between 2 pts
  double dist(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
  }


  /*************TRACKING FUNCTIONS****************/


  /**
   * Initializes all tracking variables:
   *   matrices for KF calculations
   *   stack of indices for objects & cars
   *   sets up logfile
   *   sets counters to 0
   **/
  void trackerInit();

  /**
   * deals with incoming range data from feeder
   * (called by main loop when new scan arrives)
   * calls the sequence of fxns that perform 
   *   operations on the new data
   **/
  void processScan(int ladarID);

  /**
   * segments a single scan, using the discontinuity 
   * requirement does not yet impose convex requirement.
   * TODO: add Convex requirement, or better threshold
   **/
  void segmentScan();

  /**
   * We represent objects by keeping track of their 
   * 'center of mass'. This function turns segmented 
   * objects into a point, and keeps track of their 
   * corresponding points/ranges/angles
   * (uses the objectRepresentation struct)
   **/
  void createObjects();

  /**
   * Updates old object with data from new object:
   *  - changes center, updates occlusion characteristics,
   *  - adds scan/range/angle points to the vectors
   *  - updates timesSeen and lastSeen
   **/
  void updateObject(objectRepresentation* oldObj, objectRepresentation* newObj);

  /**
   * Adds new object to array
   *  - updates numObjects
   *  - gets index of next free space in object array
   *  - copies all data from oldObject to that free space
   **/
  void addObject(objectRepresentation* newObj);


  /**
   * tracks objects through frames (using their centers)
   **/
  //#warning "this needs to take into account the predicted positions, not the last-seen positions!!"
  void classifyObjects();

  /**
   * Checks if objects appear to be moving - if so,
   * turn them into a car
   **/
  void checkMotion();

  /**
   * Sends all tracked objects to mapper AND this
   * module's debugging display
   * TODO: add remove msg, vel, uncertainty
   **/
  void sendObjects();

  /**
   * Sends all tracked cars to mapper AND this
   * module's debugging display
   * TODO: add remove msg, vel, uncertainty
   **/
  void sendCars();

  //removes any objects that haven't been seen recently
  void cleanObjects();

  //removes any cars that haven't been seen recently
  void cleanCars();

  //TODO: implement this for objs AND cars
  /**
   * this function checks each car against every other, removing
   * any potential duplicates.
   *
   * next functionality to be added is checking last-updated time...
   * if we haven't seen an MO for some time, we don't care about it
   * needs to be made fancier, but this is a field-test hack
   **/
  //  void cleanCars();

  /**
   * This function takes the points stored in the vector 
   * points, and uses them to update the representation 
   * Car. 
   * Currently, this update is VERY incomplete, simply 
   * replacing the car representation stored in Car 
   * with the one that comes from processing the points.
   *  
   * TODO:        
   * At some point (soon) need to fix this, to deal with 
   * only partial matches, and to actually update the 
   * data, rather than replacing it 
   * (see Sam's thesis)
   **/
  void updateCar(carRepresentation* Car, objectRepresentation* newObj);

  /**
   * we have an obj and checkMotion says that it's moving,
   * so we need to turn it into a car
   * 
   * Removes object at given array index, fits a car 
   *  shape to most recently observed scan, and adds 
   *  the new car to the car array
   **/
  void obj2car(int index); 

  /**
   * takes in a list of points, and fits a rectangle 
   * to them. The fit will be an array, consisting 
   * of the fit parameters
   * [alpha, rho, a,b, alpha2, rho2, a2, b2] 
   **/
  //#warning "this should take in vector<NEcoord>, so conversions don't have to happen before"
  int fitCar(double* Npoints,double*Epoints, int numPoints, carRepresentation* fit, double vN, double vE);

  /**
   * removes object at given index, where index
   * is index to the usedObjs array (NOT the index
   * to the array of objects)
   **/
  void removeObject(int index);

  /**
   * removes car at given index
   **/
  void removeCar(int index);

  /**
   * fits a line (n = a + b*x) to an array of points
   * for now, tries to minimise the perpendicularsquared error.
   * 
   * returns a, b and endpoints
   **/
  double fitLine(double* Xpoints, double* Ypoints, int numPoints, lineSegmentFit* fit);

  /**
   * fits a line (alpha, rho, a, b) to given array of 
   * points, given that it must be perpendicular to the 
   * line of given slope
   **/
  //#warning: "do I want to allow a range around theta1 at some point??"
  double fitLine2(double* Xpoints, double* Ypoints, int numPoints, lineSegmentFit* fit, double slope);


  /**
   * uses the most recent measurements (if they exist) 
   * to update the KF
   * */
  void KFupdate();

  /**
   * calculates mu_hat and sigma_hat, to be used in 
   * matching objects w/ new scans.
   **/
  void KFpredict();


  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  FILE * logfile;

  // Sensnet module
  sensnet_t *sensnet;
  LindzeyDisplay *myDisplay;
  LindzeyDisplay *fitDisplay;

  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  int useDisplay;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //how many times we've looped in the main program
  int loopCount;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];


  /************ defines from trackMO ***************/

  //angle, range data
  double rawData[NUMSCANPOINTS][2];
  //differences in RANGES between consecutive scans
  double diffs[NUMSCANPOINTS-1];
  //x,y,z data (in local frame)
  double xyzData[NUMSCANPOINTS+1][3];
  //x,y,z data in local frame, for points 1m radially
  //behind the actual return
  double depthData[NUMSCANPOINTS+1][3];

  //vehicle state at time of scan, used for figuring 
  //which corner to add
  myState currState;
  myState ladarState;

  // Matrices for Kalman Filter calculations
  Matrix A;
  Matrix C;
  Matrix I;
  Matrix R;
  Matrix Q;

  //variables for the maptalker stuff
  int useMapTalker; //FIXME: this should be a cmd line option to turn off
  int subgroup; //FIXME: check w/ sam what this value should be
  int moduleMapID, moduleObjectID, moduleCarID; 

  //first and last index of each object, as found 
  //by segmentScan()
  int posSegments[NUMSCANPOINTS][2]; 

  //number of pts in ith object, again as found 
  //by segmentScan()
  int sizeSegments[NUMSCANPOINTS]; 

  int numSegments; //number of objects found by segmentScan
  int numFrames; //how many frames we've processed
  int numObjects;

   //the stack to keep track of used/unused object indices
  vector<int> openObjs; 
  vector<int> usedObjs;
  vector<int>::iterator objIterator;
  objectRepresentation newObjects[MAXNUMOBJS];
  objectRepresentation objects[MAXNUMOBJS];

  //how many objects we've turned into cars
  // used to give each car unique ID
  int numCars; 

  //the stack to keep track of used/unused car indices
  vector<int> openCars;
  vector<int> usedCars;
  vector<int>::iterator carIterator;
  carRepresentation cars[MAXNUMCARS]; //Trying to define an array of car structs


  //stuff for display

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


