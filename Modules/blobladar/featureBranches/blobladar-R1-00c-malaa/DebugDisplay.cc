#include "DebugDisplay.hh"

  pthread_mutex_t grr_mutex1;// = PTHREAD_MUTEX_INITIALIZER;
int grr_flag = 0;
vector<ladar_point> ladar_points;
vector<ladar_object> ladar_objects;
  vector<ladar_line> ladar_lines;


//int mess_recvd = 0;

//OpenGL variables
float grr_xrot = 0, grr_yrot = 0, grr_angle=0.0;
float grr_lastx, grr_lasty;
bool grr_lbuttondown = false;
float grr_xpos = 0, grr_ypos = 0, grr_zpos = 0;

const int GRIDLINES = 0;


void display(void);
void keyboard(unsigned char key, int x, int y);
void resize(int w, int h);
void idle(void);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void motionPassive(int x, int y);


LindzeyDisplay::LindzeyDisplay(int *argc, char **argv)
{
  glEnable(GL_DEPTH_TEST);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);

  arg1 = argc;
  arg2 = argv;
  //  grr_state.N = 0;
  //  grr_state.E = 0;
  //  grr_mutex1 = PTHREAD_MUTEX_INITIALIZER;

}

LindzeyDisplay::~LindzeyDisplay()
{
  return;
}

void LindzeyDisplay::clearObjects()
{
  ladar_objects.clear();
  return;
}

void LindzeyDisplay::addObject(double x, double y, double r)
{
  ladar_object temp_object;
  temp_object.x = SCALING*x;
  temp_object.y = SCALING*y;
  temp_object.r = SCALING*r;
  pthread_mutex_lock(&grr_mutex1);
  ladar_objects.push_back(temp_object);
  pthread_mutex_unlock(&grr_mutex1);
  return;
}

void LindzeyDisplay::sendLines(int numlines, double *x1, double *x2, double *y1, double *y2)
{

  grr_flag = 1;
  ladar_line temp_line;
  pthread_mutex_lock(&grr_mutex1);
  ladar_lines.clear();
  for(int i=0; i<numlines;i++)
  {
    temp_line.x1 = SCALING*x1[i];
    temp_line.x2 = SCALING*x2[i];
    temp_line.y1 = SCALING*y1[i];
    temp_line.y2 = SCALING*y2[i];
    ladar_lines.push_back(temp_line);
  }
  pthread_mutex_unlock(&grr_mutex1);
 
 return;

}
void LindzeyDisplay::sendLine(double x1, double x2, double y1, double y2)
{

  grr_flag = 1;
  ladar_line temp_line;
  temp_line.x1 = x1;
  temp_line.x2 = x2;
  temp_line.y1 = y1;
  temp_line.y2 = y2;

  pthread_mutex_lock(&grr_mutex1);
  ladar_lines.push_back(temp_line);
  pthread_mutex_unlock(&grr_mutex1);
 
 return;

}

void LindzeyDisplay::sendScan(int numPts, double *xpts, double *ypts, double *zpts)
{
  ladar_point temp_point;
  pthread_mutex_lock(&grr_mutex1);

  //receiving a new scan, discard old points
  ladar_points.clear(); 
  for(int i=0; i<numPts; i++)
  {
    temp_point.x = SCALING*xpts[i];
    temp_point.y = SCALING*ypts[i];
    temp_point.z = SCALING*zpts[i];

    ladar_points.push_back(temp_point);
  }
  //  fprintf(stderr, "received %d points \n", numPts);
  pthread_mutex_unlock(&grr_mutex1);

  return;
}

void LindzeyDisplay::sendPoint(double x, double y)
{

  //  fprintf(stderr, "received point! %f, %f \n", x, y);

  ladar_point temp_point;
  temp_point.x = SCALING*x;
  temp_point.y = SCALING*y;

  pthread_mutex_lock(&grr_mutex1);
  //  ladar_points.clear();
  ladar_points.push_back(temp_point);
  //  fprintf(stderr, "num ladarpoints: %d \n", ladar_points.size());
  pthread_mutex_unlock(&grr_mutex1);

  grr_flag = 1;
 
 return;
}


//intializes openGL functions
//copied from main() loop of jeremy's
//demo code
void LindzeyDisplay::mainOpenGL()
{
  //int main(int argc, char **argv)
  //    int iret1;
    //    pthread_t thread1;

    //Spread stuff
    //    Initialize_spread();

    //    iret1 = pthread_create( &thread1, NULL, Read_message, NULL);

    glutInit(arg1, arg2);
    //    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Laura's OpenGL Program");

    //glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
    glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
    glColor3f(1.0, 1.0, 1.0);

    glEnable(GL_DEPTH_TEST);

    /* set callback functions */
    glutDisplayFunc(display); 

    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutPassiveMotionFunc(motionPassive);

    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);
    glutIdleFunc(idle);

    glutMainLoop();
	 
    return;

}


void display(void)
{
  glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
  //  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer
  glLoadIdentity();
  //  gluLookAt (0.0, 20, 20.0, 0.0, -0.5, -1.0, 0.0, 1.0, 0.0); //camera position, x,y,z, looking at x,y,z, Up Positions of the cameraa
  gluLookAt (0.0, 0.0, 30.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0); //camera position, x,y,z, looking at x,y,z, Up Positions of the cameraa

  glRotatef(grr_xrot,1.0,0.0,0.0); //rotate our camera on teh x-axis (left and right)
  glRotatef(grr_yrot,0.0,1.0,0.0); //rotate our camera on the y-axis (up and down)
  glTranslated(-grr_xpos,-grr_ypos,-grr_zpos); //translate the screen to the position of our camera

  glEnable (GL_DEPTH_TEST); //enable the depth testing


  //====================
  /* draw the floor */
  //====================
  /**  
  glBegin(GL_QUADS);
  glColor3f(0.85f, 0.85f, 0.85f);
  glVertex3f(XMIN, ZMIN, 0.0);
  glVertex3f(XMIN, ZMAX, 0.0);
  glVertex3f(XMAX, ZMAX, 0.0);
  glVertex3f(XMAX, ZMIN, 0.0);		
  glEnd();
  **/

  //====================
  //draw fine grid
  //====================
  if(GRIDLINES)
    {
      //vertical lines
      glLineWidth (1.5);
      glColor3f (0.0F, 0.0F, 0.0F);
      glBegin (GL_LINES);
      for (int i= XMIN; i < XMAX+1; i++)
	{
	  glVertex3f (i, 0, ZMIN);
	  glVertex3f (i, 0, ZMAX);
	};
      glEnd ();
      
      //horizontal lines
      glLineWidth (1.5);
      glColor3f (0.0F, 0.0F, 0.0F);
      glBegin (GL_LINES);
      for (int i= ZMIN; i < ZMAX+1; i++)
	{
	  glVertex3f ( XMIN, 0, i);
	  glVertex3f ( XMAX, 0, i);
	};
      glEnd ();
    }
  
  //====================
  //draw coordinate axes
  //====================

  glLineWidth (1);
  glColor3f (0.0F, 0.0F, 1.0F);
  glBegin(GL_LINES);
  glVertex3f(XMIN,0,0);
  glVertex3f(XMAX,0,0);
  glVertex3f(0,ZMAX, 0);
  glVertex3f(0,ZMIN, 0);
  glEnd();
  
  
  if(grr_flag==1)// && mess_recvd==1)
    {



      //============START MUTEX LOCK=============
      pthread_mutex_lock(&grr_mutex1);
      
      //DO ALL YOUR PLOTTING IN HERE!!!!!

      //draw alice state
      /**
      glPushMatrix();
      glColor3f (0.0, 1.0, 0.0);
      //coordinate frame is (x,z,-y)
      //      glTranslatef(grr_state.N, 0.25, -grr_state.E);
      glutSolidSphere(0.40f,10,10); //wheel radius is 0.40
      glPopMatrix();
      **/

      //printing the ladar points
      glColor3f(0.5F, 0.0F, 0.5F);
      glBegin(GL_POINTS);
      //      fprintf(stderr, "size of ladar_points: %d \n", ladar_points.size());
      for(unsigned int n = 0; n < ladar_points.size(); n++)
	{
	  glVertex3f(ladar_points[n].x,ladar_points[n].y,0);
	  //	  fprintf(stderr, "pt. %d \n", n);
	}
      glEnd();
        
      //plotting the segmentations
      glColor3f(1.0F, 0.0F, 0.0F); 
      glLineWidth(2);
      glBegin(GL_LINES);
      //      fprintf(stderr, "size of ladar_lines: %d \n", ladar_lines.size());
      for(unsigned int n=0; n<ladar_lines.size(); n++)
	{
	  glVertex3f(ladar_lines[n].x1,ladar_lines[n].y1,0);
	  glVertex3f(ladar_lines[n].x2,ladar_lines[n].y2,0);

	}
      glEnd();

      //plotting the "object"
      glColor3f(0.0F, 1.0F, 0.0F);
      glBegin(GL_QUADS);
      double tempx, tempy, tempr;
      for(unsigned int n=0; n<ladar_objects.size();n++)
      {
	tempx = ladar_objects[n].x;
	tempy = ladar_objects[n].y;
	tempr = ladar_objects[n].r;
	glVertex3f(tempx,tempy+tempr,0);
	glVertex3f(tempx+tempr, tempy, 0);
	glVertex3f(tempx,tempy-tempr,0);
	glVertex3f(tempx-tempr, tempy, 0);

      }
      glEnd();



      //============STOP MUTEX LOCK==============
      pthread_mutex_unlock(&grr_mutex1);
    }
  
  glutSwapBuffers();
  
  
}

void idle(void)
{
  /* Check flag variable */

  //============START MUTEX LOCK=============
  pthread_mutex_lock(&grr_mutex1);

  if(grr_flag==1)
	glutPostRedisplay();
			  
  //============STOP  MUTEX LOCK=============
  pthread_mutex_unlock(&grr_mutex1);

}


void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  grr_lbuttondown = true;
	}
      else
	{
	  grr_lbuttondown = false;
	}
    }
}

void motion(int x, int y)
{
  if (grr_lbuttondown)
    {
      float diffx=(x-grr_lastx)/10; //check the difference between the current x and the last x position
      float diffy=(y-grr_lasty)/10; //check the difference between the current y and the last y position  
      grr_lastx=x; //set grr_lastx to the current x position
      grr_lasty=y; //set grr_lasty to the current y position
      grr_xrot += diffy; //set the xrot to xrot with the addition of the difference in the y position
      grr_yrot += diffx; //set the grr_xrot to grr_yrot with the addition of the difference in the x position
      glRotatef(grr_xrot,1.0,0.0,0.0); //rotate our camera on teh x-axis (left and right)
      glRotatef(grr_yrot,0.0,1.0,0.0); //rotate our camera on the y-axis (up and down)
      display();
    }

  
}

void motionPassive(int x, int y)
{
  grr_lastx = x;
  grr_lasty = y;
}
void keyboard(unsigned char key, int x, int y)
{
  /* This time the controls are:
     
  "a": move left
  "d": move right
  "w": move forward
  "s": move back
  "t": toggle depth-testing
  
  */
  
  
  if (key=='2')
    {
      float xrotrad, yrotrad;
      yrotrad = (grr_yrot / 180 * M_PI);
      xrotrad = (grr_xrot / 180 * M_PI);
      grr_xpos += float(sin(yrotrad)/10) ;
      grr_zpos -= float(cos(yrotrad)/10) ;
      grr_ypos -= float(sin(xrotrad)/10) ;
    }
  
  if (key=='8')
    {
      float xrotrad, yrotrad;
      yrotrad = (grr_yrot / 180 * M_PI);
      xrotrad = (grr_xrot / 180 * M_PI);
      grr_xpos -= float(sin(yrotrad)/10);
      grr_zpos += float(cos(yrotrad)/10) ;
      grr_ypos += float(sin(xrotrad)/10);
    }
  
  if (key=='4')
    {
      float yrotrad;
      yrotrad = (grr_yrot / 180 * M_PI);
      grr_xpos += float(cos(yrotrad)/10);
      grr_zpos += float(sin(yrotrad)/10);
    }
  
  if (key=='6')
    {
      float yrotrad;
      yrotrad = (grr_yrot / 180 * M_PI);
      grr_xpos -= float(cos(yrotrad)/10);
      grr_zpos -= float(sin(yrotrad)/10);
    }


  if (key==27)
    {
      exit(0);
    }

}



void resize(int w, int h)
{
	//if (height == 0) height = 1;
	grr_flag = 1;
	glViewport (0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
	glMatrixMode (GL_PROJECTION); //set the matrix to projection
	glLoadIdentity ();
	gluPerspective (60, (GLfloat)w / (GLfloat)h, 1.0, 1000.0); //set the perspective (angle of sight, width, height, , depth)
	glMatrixMode (GL_MODELVIEW); //set the matrix back to model

}
