/**
 * File: DebugDisplay.hh
 * Description:
 *   Handles all the openGL display functions for
 *   blobladar. Most of the code is directly from
 *   Jeremy's demo code
 * Last Changed: March 9
 **/

using namespace std;

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sp.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <vector>

//scaling factor for m so image fits on display
#define SCALING          1.0

#define XMIN  		-20
#define XMAX   		 20
#define ZMIN  		-20
#define ZMAX   		 20
#define MAX_MESSLEN      102400

#define NUM_COLORS       4
#define AGE_STEP         25

struct ladar_point {
  double x;
  double y;
  double z;
};

struct ladar_line {
  double x1;
  double x2;
  double y1;
  double y2;
  int color;
};

struct ladar_object {
  double x;
  double y;
  double r;
  int color;
};

struct ladar_car {
  double rear_r_x;
  double rear_r_y;
  double rear_l_x;
  double rear_l_y;

  double front_r_x;
  double front_r_y;
  double front_l_x;
  double front_l_y;

  int color;

};

class LindzeyDisplay
{
public:
  LindzeyDisplay(int *argc, char **argv);
  ~LindzeyDisplay();
 
  /**
   * The main loop - called from another thread to 
   *  start all the plotting functionality.
   *  All of the following functions are called from
   *  BlobLadar.cc or BlobTracking.cc
   **/
  void mainOpenGL();

  /**
   * empties the object, line and car vectors
   **/
  void clearObjects();

  /** 
   * Adds an object to the plotted vector
   * Inputs: center (x,y) coords, radius, age
   * Action: object will be defined by x,y +- radius,
   *   and object's color will depend on age
   **/
  void addObject(double x, double y, double r, int age);

  /** 
   * Adds a car to the plotted vector
   * Inputs: corner (x,y) coords 
   * Action: car will be rectangle defined by the corners,
   *   which will be plotted in diff colors
   **/
  void addCar(double *corners, int color=5);

  /**
   * Adds a new scan to the points to be plotted
   * Inputs: size of scan, arrays of x,y&z pts 
   * Action: points will be plotted in black
   */
  void sendScan(int numPts, double *xpts, double *ypts, double *zpts);

  /**
   * adds a new line to the plotted lines
   * Inputs: line endpoints, color
   **/
  void sendLine(double x1, double x2, double y1, double y2, int color=5);

  /**
   * adds new lines to the plotted lines
   * Inputs: number of lines, arrays of line endpoints
   **/
  void sendLines(int numlines, double *x1, double *x2, double *y1, double *y2);

private:

  int *arg1;
  char **arg2;

};
