/*
 * This is all the code for trakcing
 * blobs in ladar data.
 * Most of it comes directly from Laura's
 * summer project, at least for now
 */ 

#include "BlobLadar.hh"
using namespace std;

void BlobLadar::foo()
{
  //  fprintf(stderr, "foo! \n");
  return;
}

void BlobLadar::trackerInit()
{
  numFrames = 0;
  numObjects = 0;

  useMapTalker = 1;
  subgroup = 0;

  moduleMapID = -1;  fprintf(stderr, "initializing send map element w/ snkey: %d \n", skynetKey);
  initSendMapElement(skynetKey);

  logfile = fopen("BlobLadar.log","w");
  //  fprintf(logfile, "\n ******** NEW LOG STARTS HERE ******* \n \n");


  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    //    fprintf(stderr, "pushing back %d \n", i);
    openObjs.push_back(i);
    openCars.push_back(i);
  }

  //setting up matrics for KF calculations
  //#warning "these need to be tuned!"
  double dt = .0133; 
  double sa = .05; //std dev of system noise -- tune!
  double sz = .1; //std dev of measurement noise -- should be fxn of range/theta!!!

  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);



  //  setupLogs();
  //  cout<<"trackerInit(...) Finished"<<endl;

  return;
}

void BlobLadar::processScan()
{
  numFrames++;

  segmentScan(); 
  prepObjects();
  classifyObjects();

  checkMotion();
  sendObjects();

  cleanObjects();
  KFupdate();
  KFpredict();

  return;
}

void BlobLadar::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][1] - rawData[i][1];
  }

  int tempNum = 0; //number objects segmented from curr scan
  int currSize = 1; //size of current object

  // This is the actual segmentation functionality...
  // the above was just setting up the data we need

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];
  //for a while, there was a problem with segfaulting
  //at sumE += utmData....
  //I *think* that this fixed it, but not positive
  //(haven't been able to recreate it)
  tempSize[0] = 0;

  double distThresh;
  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    if(rawData[i][1] > 80.0) {
      distThresh = 0;
    } else {
    //threshold should be scaled based on distance from ladar
    distThresh = max(.10,rawData[i][1]*.0175*SCALINGFACTOR);
    //    cout<<"range: "<<rawData[i]<<"   threshold: "<<distThresh<<endl;
    }
    //if the distance is too large to be the same object
    //note that this distance is the distance btwn point i and pt i+1
    if(fabs(diffs[i]) > distThresh) {
 
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;

    } else { //this point belongs to current object
      
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points

  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  int origin;
  numSegments = 0;
    fprintf(logfile, "segment sizes: ");
  for(int i=0; i<=tempNum; i++) {
    st = tempPos[i][0];
    en = tempPos[i][1];
    origin=0; 
    //    cout<<"rawData: ";
    for(int j=st; j<=en; j++) {
      //      cout<<rawData[j]<<", ";
      if(rawData[j][1] > 80) {
	origin=1;
      } else {
	//	cout<<'('<<newPoints[j].N<<' '<<newPoints[j].E<<"), ";
      }
    }
    //    cout<<endl;
    //checking that this isn't a group of no-return points

    if(origin==0 && tempSize[i]>=MINNUMPOINTS) {
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      int foo = tempSize[i];
      fprintf(logfile," %d , ",foo);
      numSegments++;
    }

  }
    fprintf(logfile,"\n");
  //TODO: add convex hull requirement

  //now, trying to output the segmentation, to check that it's correct
  //TODO: output this to gui (sensviewer?)

  //now, send the segments to the gui
  double x1[numSegments];
  double x2[numSegments];
  double y1[numSegments];
  double y2[numSegments];
  int tmpindex;

  //  fprintf(stderr, "num segments: %d \n", numSegments);
  for(int i=0; i<numSegments;i++) {
    //      m_outputSegments<<posSegments[i][0]<<' '<<posSegments[i][1]<<' ';
      //FIXME: this only works for alice at the origin.
      //need to change this to be alices's state
      x1[i]=0;
      y1[i]=0;
      tmpindex = posSegments[i][1];
      x2[i]=utmData[tmpindex][0];
      y2[i]=utmData[tmpindex][1];
  }
  if(useDisplay) {
    //FIXME: this doens't work, since ladar isn't at origin
    //    myDisplay->sendLines(numSegments, x1, x2, y1, y2);
  }

  /**
  for(int i=numSegments; i<NUMSCANPOINTS; i++) {
    m_outputSegments<<"-1 -1 ";
  }
  m_outputSegments<<endl;
  **/

  //  fprintf(stderr, "we found %d segments \n", numSegments);


  return;
}


/**
 * This code goes through all the new segments, picks 
 * out the ones that correspond to objects that we care 
 * about (based on number of points in object, and that 
 * the object isn't a group of no-return points) 
 * It then updates the candidates array with the 
 * appropriate information
**/
void BlobLadar::prepObjects() {

  //  cout<<"entering prepObjects()"<<endl;
  //  cout<<"STARTING prepObjects()"<<endl;

  //TODO: change this. candidates is a confusing var name 
  numCandidates = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  for(int i=0; i<numSegments; i++) {

    int objectsize = sizeSegments[i];

    if(objectsize >= MINNUMPOINTS) {

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      //if dStart or dEnd > 0, that means that that end of the object 
      //is in the foreground, <0 means the object next to it is in the 
      //foreground, and 0 means that it's at the edge of a scan
      //#warning "check that ranges don't get set to 0 for no-return values before we get here"
      if(objStart == 0) {
	dStart = 0;
      } else {
	dStart = -diffs[objStart - 1];
      }
      if(objEnd == NUMSCANPOINTS - 1) {
	dEnd = 0;
      } else {
	dEnd = diffs[objEnd];
      }

      //want to fit center to object here, and put 
      // it in candidates array....

      //first, check that it's not a group of no-return points
      if(rawData[objStart][1] < 80.0 && rawData[objEnd][1] < 80.0 ) {

	double cenN, cenE;
	double sumN = 0;
	double sumE = 0;

	//TODO: check this 
	//currently have x from sensnet_ladar_vehicle_to_local
	//as the northing value
          fprintf(logfile, "object size, object start: %d, %d \n", objectsize, objStart);
	for(int j=0;j<objectsize;j++) {
	  //FIXME: sometimes segfaults here. WHY???

	  sumN += utmData[j+objStart][0];
	  sumE += utmData[j+objStart][1];
	}

	cenN = sumN / objectsize;
	cenE = sumE / objectsize;

	candidates[numCandidates].N = cenN;
	candidates[numCandidates].E = cenE; 
	//	cout<<"in prep objects, new object's N is: "<<candidates[numCandidates].N<<endl;

	candidates[numCandidates].dStart = dStart;
	candidates[numCandidates].dEnd = dEnd; 

	vector<NEcoord> tmppts;
	vector<double> rngs;
	vector<double> angs;

	NEcoord tmp;
	double foo, bar;

	//#warning "keeping this many points may be a problem"
	for(int m=0;m<objectsize;m++) {
	  tmp.N = utmData[m+objStart][0];
	  tmp.E = utmData[m+objStart][1];
	  tmppts.push_back(tmp);
	  foo = rawData[m+objStart][1];
	  bar = rawData[m+objStart][0];
	  rngs.push_back(foo);
	  angs.push_back(bar);
	}


	//TODO: these cause segfaults - don't need now,
	//but leaving in because for more sophisticated
	//object representations, will want this data
	candidates[numCandidates].scans.push_back(tmppts);
	candidates[numCandidates].ranges.push_back(rngs);
	candidates[numCandidates].angles.push_back(angs);

	numCandidates++; //we've found a new object

      } //end ranges condition
    }   //end num points condition
  }     //end cycling through segments
  //    fprintf(stderr, "we have %d new objects \n", numCandidates);
}   //end of prepObjects()



/**
 * This function assumes that the scan has been segmented, and 
 * classifies the new objects into: already-seen object, already-seen
 * car, and new object. Then, it calls the appropriate function to update
 * the representations.
 **/
void BlobLadar::classifyObjects() 
{
  //  cout<<"STARTING trackObjects()"<<endl;
  double distTraveled;

  int taken[numCandidates];
  for(int i=0;i<numCandidates;i++)
    taken[i] = 0;

  double oldN, oldE;
  double newN, newE;

  /**
   * First, we cycle through all previously seen objects to see if the new 
   * object matches one of them
   **/
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int k = usedObjs.at(i);
    //    oldN = objects[k].Nmu.getelem(0,0);
    //    oldE = objects[k].Emu.getelem(0,0);
    oldN = objects[k].N;
    oldE = objects[k].E;

    for(int j=0; j<numCandidates; j++) {
      if(taken[j] == 0) { //if new object hasn't already been classified
	newN = candidates[j].N;
	newE = candidates[j].E;
	distTraveled = dist(oldN, oldE, newN, newE);

	if(distTraveled < MAXTRAVEL) { //if they match
	  //	  cout<<"new object matched old object...updating!"<<endl;
	  taken[j] = 1;
	  //	  fprintf(stderr,"updating object #%d \n",k);
	  updateObject(&objects[k], &candidates[j]);

	} //end checking distance traveled
      } //end checking of newObj is taken
    } //end cycling through new objects
  } //end cycling through old objects



  /**
   * Next, we cycle through all previously seen cars to see if obj matches
   * This functionality goes AFTER matching newobjs to oldobjs cuz we don't want 
   * to match an already seen lamppost in the car's path to the car...
   **/
  double Nmin, Nmax, Emin, Emax;
  double dn1, de1, dn2, de2;
  int foo = 0;
  for(unsigned int i=0; i<usedCars.size(); i++) {
    int k = usedCars.at(i);

    oldN = cars[k].Nmu_hat.getelem(0,0);
    oldE = cars[k].Emu_hat.getelem(0,0);
    //    oldN = cars[k].n;
    //    oldE = cars[k].e;
    dn1 = cars[k].dn1;
    de1 = cars[k].de1;
    dn2 = cars[k].dn2;
    de2 = cars[k].de2;

    Nmin = min(oldN, min(oldN+dn1, min(oldN+dn2, oldN+dn1+dn2))) - CARMARGIN;
    Nmax = max(oldN, max(oldN+dn1, max(oldN+dn2, oldN+dn1+dn2))) + CARMARGIN;
    Emin = min(oldE, min(oldE+de1, min(oldE+de2, oldE+de1+de2))) - CARMARGIN;
    Emax = max(oldE, max(oldE+de1, max(oldE+de2, oldE+de1+de2))) + CARMARGIN;


    for(int j=0; j<numCandidates; j++) {
      if(taken[j] == 0) { //this obj has not already been claimed
	newN = candidates[j].N;
	newE = candidates[j].E;
	//check if this obj matches car
	if(newN < Nmax && newN > Nmin && newE < Emax && newE > Emin) {
	  //the obj and car should be associated
	 
	  taken[j] = 1;
	  fprintf(stderr, "new obj matched car # %d -- updating!! \n", k);
	  //	  cout<<"center points for object: "<<newN<<' '<<newE<<endl;
	  //	  cout<<setprecision(8)<<"before update, N and E for slot "<<k<<" are: "<<cars[k].n<<' '<<cars[k].e<<endl;
	  updateCar(&cars[k], &candidates[j]);
	  foo++;
	  //	  cout<<setprecision(8)<<"after update, N and E for slot "<<k<<" are: "<<cars[k].n<<' '<<cars[k].e<<endl;
	}  //end of checking if newObj and car match
      }   //end of checking if obj already claimed
    }    //end cycling thru newObjs
  }     //end cycling thru numCars




  /**
   * Finally, we take care of the new objects that don't match anything we've
   * seen before
   **/
  int numNew = 0;
  for(int i=0; i<numCandidates; i++) {
    if(taken[i] == 0) { //this is a lonely, unwanted new object
      //      cout<<"new object didn't match anything -- creating new obj!"<<endl;
      //      fprintf(stderr,"adding new object! \n");
      addObject(&candidates[i]);
      numNew++;
    } //end checking if object already classified
  } //end cycling through numCandidates
  fprintf(stderr, "number new objects: %d, num old: %d \n", numNew, numCandidates-numNew);

}

/**
 * This function is called when it is determined that a newobject 
 * matches an old one. As such, it simply replaces the center position, 
 * and adds the latest scan/ranges data
**/
void BlobLadar::updateObject(objectRepresentation* oldObj, objectRepresentation* newObj) {

  oldObj->N = newObj->N;
  oldObj->E = newObj->E;

  //  fprintf(stderr,"updated object at: %f, %f \n", newObj->N, newObj->E);

  oldObj->dStart = newObj->dStart;
  oldObj->dEnd = newObj->dEnd;

  vector<NEcoord> newscans(newObj->scans.back());
  vector<double> newranges(newObj->ranges.back());
  vector<double> newangles(newObj->angles.back());

  //  fprintf(stderr,"updating object \n");
  oldObj->scans.push_back(newscans);
  oldObj->ranges.push_back(newranges);
  oldObj->angles.push_back(newangles);

  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;
  //TODO: add KF stuff here!!

}


/**
 * This function is called when it is determined that a new object
 * has no match in already seen objects or cars
 * It's job is to add an object to the object array, if there is room.
 **/
void BlobLadar::addObject(objectRepresentation* newObj) {
  numObjects = numObjects + 1;
  //  fprintf(stderr, "entering add object, w/ object # %d \n", numObjects);
  if(usedObjs.size() < MAXNUMCARS) {

    int temp;
    temp = openObjs.back();
    openObjs.pop_back();
    usedObjs.push_back(temp);

    objects[temp].ID = numObjects;
    objects[temp].N = newObj->N;
    objects[temp].E = newObj->E;
    objects[temp].dStart = newObj->dStart;
    objects[temp].dEnd = newObj->dEnd;

    objects[temp].timesSeen = 1;
    objects[temp].lastSeen = numFrames;

    //transferring vectors of scan/range/angle history
    //    fprintf(stderr,"adding new object %d \n", numObjects);
    vector<NEcoord> newscans(newObj->scans.back());
    vector<double> newranges(newObj->ranges.back());
    vector<double> newangles(newObj->angles.back());

    objects[temp].scans.push_back(newscans);
    objects[temp].ranges.push_back(newranges);
    objects[temp].angles.push_back(newangles);
 
    //initizlizing KF variables
    // currently, no KF being done
    objects[temp].Nmu.resetSize(2,1);
    objects[temp].Emu.resetSize(2,1);
    objects[temp].Nsigma.resetSize(2,2);
    objects[temp].Esigma.resetSize(2,2);
    objects[temp].Nmu_hat.resetSize(2,1);
    objects[temp].Emu_hat.resetSize(2,1);
    objects[temp].Nsigma_hat.resetSize(2,2);
    objects[temp].Esigma_hat.resetSize(2,2);

    double NMelems[] = {objects[temp].N, 0};
    double EMelems[] = {objects[temp].E, 0};
    double NSelems[] = {1,0,0,1};
    double ESelems[] = {1,0,0,1};
    //    cout<<"when adding new object, N is: "<<objects[temp].N<<endl;
    objects[temp].Nmu.setelems(NMelems);
    objects[temp].Nsigma.setelems(NSelems);
    objects[temp].Emu.setelems(EMelems);
    objects[temp].Esigma.setelems(ESelems);
    //    cout<<" and Nmu is: "<<objects[temp].Nmu.getelem(0,0)<<endl;
    //    */

  } else {
    cerr<<"trying to add obj, no space in buffer"<<endl;

  } //end checking that there's enough room to add new object
} //end addObject


//TODO: use actual struct/message type that sam defines
//TODO: actually calculate radius of object
void BlobLadar::sendObjects() {
  //  cout<<"entering sendObjects()"<<endl;
  //  cout<<"number of cars: "<<usedCars.size()<<endl;

  //first, we clear the object list
  if(useDisplay) {
  myDisplay->clearObjects();
  }


    MapElement el;
    vector <point2> objectPoints;

  objectMessage obstacle;

  double tempN, tempE, dN, dE, radius;

  fprintf(stderr, "number of objects: %d \n", usedObjs.size());
  int sent = 0;
  for(unsigned int k=0; k<usedObjs.size(); k++) {
    int j = usedObjs.at(k);
  
    int age = objects[j].timesSeen;
    sent++;
    obstacle.ID = objects[j].ID;

    tempE = objects[j].Emu.getelem(0,0);
    tempN = objects[j].Nmu.getelem(0,0);
    radius = 0.50;
    dN = objects[j].Nmu.getelem(1,0);
    dE = objects[j].Emu.getelem(1,0);
    //    obstacle.center.E = objects[j].E;
    //    obstacle.center.N = objects[j].N;
    //    obstacle.radius = 0.50;

    if(useDisplay) {
      myDisplay->addObject(tempN, tempE, radius, age);
      //      fprintf(stderr, "trying to plot line: %f, %f, %f, %f \n ", tempN, tempE, dN, dE);
      //      fprintf(logfile, "looking for color %d  \n", 5);
      myDisplay->sendLine(tempN, tempN + dN, tempE, tempE + dE, 5);
    }


    fprintf(stderr, "trying to create and send map element \n");
    if(useMapTalker) {
      objectPoints.clear();
      vector <NEcoord> lastScan(objects[j].scans.back());
      point2 tmppt;
      vector <int> id;
      //fill in the points
      fprintf(stderr, "first point: %f, %f \n", lastScan.at(0).N, lastScan.at(0).E);
      for(unsigned int m=0; m < lastScan.size(); m++) {
	tmppt = point2(lastScan.at(m).N, lastScan.at(m).E);
	objectPoints.push_back(tmppt);
      }
   
      //set the originating module and blob ID numbers
      id.clear();
      id.push_back(moduleMapID);
      id.push_back(objects[j].ID);

      el.clear();
      el.set_poly_obs(id, objectPoints);
      fprintf(stderr, "object ID: %d, object num %d \n", objects[j].ID, j);
      int bytesSent = sendMapElement(&el, 0);
      fprintf(stderr, "sent map element! bytesSent = %d \n", bytesSent);
    }

    //    fprintf(stderr, "trying to send object %d \n", j);
    //    int msgSize = sizeof(movingObstacle);
    //    m_skynet.send_msg(m_send_socket, &obstacle, msgSize, 0);

  } // end checking each car


  fprintf(stderr, "number of cars: %d \n", usedCars.size());
  sent = 0;
  for(unsigned int k=0; k<usedCars.size(); k++) {
    int j = usedCars.at(k);
  
    sent++;
    obstacle.ID = cars[j].ID;


    tempE = cars[j].Emu.getelem(0,0);
    tempN = cars[j].Nmu.getelem(0,0);
    radius = 1.0;
    dN = cars[j].Nmu.getelem(1,0);
    dE = cars[j].Emu.getelem(1,0);

    //    obstacle.center.E = cars[j].lastE[(ptr-1)%10];
    //    obstacle.center.N = cars[j].lastN[(ptr-1)%10];
    //    obstacle.radius = 1.0;

    if(useDisplay) {
      myDisplay->addObject(tempN, tempE, radius, cars[j].timesSeen);
      //      myDisplay->sendLine(tempN, tempE, tempN + dN, tempE+dE, BLOB_BLACK);
    }

    //    fprintf(stderr, "trying to send object %d \n", j);
    //    int msgSize = sizeof(movingObstacle);
    //    m_skynet.send_msg(m_send_socket, &obstacle, msgSize, 0);

  } // end checking each car

  //  cout<<"exiting sendObjects()"<<endl;
  return;
}



//#warning "may want to break this up into diff functions (build model, evaluate model)"
//this is code from probMoving, buildModel and probModel
//#warning "may want to add more checks before checking every object's motion, every time"
void BlobLadar::checkMotion() {

  for(unsigned int i = 0; i< usedObjs.size(); i++) {
    int index = usedObjs.at(i);
    // cout<<"ENTERING checkMotion() with index = "<<index<<setprecision(10)<<endl;
    if(objects[index].timesSeen >= 20) {
  
      double velN, velE, vel;
      velN = objects[index].Nmu.getelem(1,0);
      velE = objects[index].Emu.getelem(1,0);
      vel = sqrt(velN*velN+velE*velE);
      if(vel > MINSPEED) {
	//	fprintf(stderr, "THIS OBJ IS MOVING!!! \n ");
	//	obj2car(i);
      }
    }
  }
} //end checkMotion



//#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
void BlobLadar::obj2car(int index) {
  //  cout<<"trying to turn an object into a car"<<endl;
  if(usedCars.size() < MAXNUMCARS-1) { 
    int i = usedObjs.at(index);
  vector<NEcoord> points(objects[i].scans.back());

  int temp;
  temp = openCars.back();
  openCars.pop_back();
  usedCars.push_back(temp);

 int numPoints = points.size();
 //  cout<<"this has "<<numPoints<<" points..."<<endl;

  double pts[2*numPoints];
  for(int i=0; i<numPoints; i++) {
    pts[2*i] = points.at(i).E;
    pts[2*i+1] = points.at(i).N;
  }

  int corner;
  corner = fitCar(pts,numPoints,&cars[temp],objects[i].Nmu.getelem(1,0), objects[i].Emu.getelem(1,0)); 

  cars[temp].Nmu.resetSize(2,1);
  cars[temp].Emu.resetSize(2,1);
  cars[temp].Nsigma.resetSize(2,2);
  cars[temp].Esigma.resetSize(2,2);
  cars[temp].Nmu_hat.resetSize(2,1);
  cars[temp].Emu_hat.resetSize(2,1);
  cars[temp].Nsigma_hat.resetSize(2,2);
  cars[temp].Esigma_hat.resetSize(2,2);

  double NMelems[] = {cars[temp].n, objects[i].Nmu.getelem(1,0)};
  double EMelems[] = {cars[temp].e, objects[i].Emu.getelem(1,0)};
  double NSelems[] = {0,0,0,0};
  double ESelems[] = {0,0,0,0};

  //	  cout<<"setting matrix mu-naught to: "<<cars[temp].N<<" and "<<cars[temp].E<<endl;

  cars[temp].Nmu.setelems(NMelems);
  cars[temp].Nsigma.setelems(NSelems);
  cars[temp].Emu.setelems(EMelems);
  cars[temp].Esigma.setelems(ESelems);

  cars[temp].ID = numCars;
  
  numCars++; //we've found a car

  removeObject(index);

  } else {
    cerr<<"not enough space in cars array!!"<<endl;
  }
  //  cout<<"exiting obj2car()"<<endl;
}

/**
 * function: fitCar
 * input: takes in set of NE points that have been grouped together as an object,
 *   and finds the two lines that fit them best (think rectangle)
 * output: parameters describing this rectangle
 **/

//#warning "should add idea of uncertainty to position of car!"
int BlobLadar::fitCar(double* points, int numPoints, carRepresentation* newCar, double vN, double vE) {
  int corner = 0;
  //  cout<<"entering fitCar with "<<numPoints<<" points"<<endl;

  double minerror, MSE1, MSE2;
  int breakpoint;
  //calculating w/out corner, or new best fit
  double temp1[6];
  double temp2[6];
  //calculating corner
  double temp3[6];
  double temp4[6];

  //returns the fit in temp 1 w/ format {a, b, n1, e1, n2, e2}
  minerror = fitLine(points, numPoints, temp1);

  //figuring which corner to add (see p. 60 for docs)
  //want to get points relative to Alice's position
  double relPoints[2*numPoints];
  for(int i=0; i<numPoints; i++) {
    relPoints[2*i] = points[2*i] - currState[1];
    relPoints[2*i+1] = points[2*i+1] - currState[0];
  }

  //  fitLine(relPoints, numPoints, temp3);
  //now, adding a seg that points away from alice (see p.88)
  double th;
  th = atan2(temp1[4]-temp1[2], temp1[5]-temp1[3]); //slope of line
  double n,e;
  //#warning "the signs on these are going to be incorrect in some cases..."
  n = temp1[2] - MINDIMENSION * cos(th);
  e = temp1[3] - MINDIMENSION * sin(th);
  //creates the second line segment, in global coords
  //#warning "need to set a & b properly!!"
  //#warning "if we actually use this, then  need to change dn2, de2 to reflect that..."
  temp2[0] = 0;
  temp2[1] = 0;
  temp2[2] = temp1[2];
  temp2[3] = temp1[3];
  temp2[4] = n;
  temp2[5] = e;

  //now, temp1 and temp2 are the line segments, if no corner is found
  //need to see if there's a better fit, w/ a corner

  //now, find min error, but requiring each leg to have at 
  //least 2 points, and each leg contains the breakpoint
  for(int i=2; i<numPoints-2; i++) {
    double points1[2*i];
    double points2[2*(numPoints-i+1)];

    for(int j=0; j<2*i; j++) {
      points1[j] = points[j];
    }
    for(int j=0; j<2*(numPoints-i+1); j++) {
      points2[j] = points[j+2*(i-1)];
    }

    MSE1 = fitLine(points1, i, temp3);
    MSE2 = fitLine2(points2, numPoints-i+1, temp4, temp3[1]);

    //    cout<<"breaking at point "<<i<<" w/ minerror, MSE1, MSE2: "<<minerror<<' '<<MSE1<<' '<<MSE2<<endl;

    //if we've reached a new minimum error, this is our current best-fit, and
    //should be copied to temp1 & temp2
   if(minerror > (MSE1+ MSE2)) {
     //     cout<<"new min error!!!"<<endl;
      minerror = MSE1 + MSE2;
      breakpoint = i;
      memcpy(temp1, temp3, 6*sizeof(double));
      memcpy(temp2, temp4, 6*sizeof(double));
      corner = 1;
    }
  }

  //  cout<<"corner posns for seg1: "<<temp1[2]<<' '<<temp1[3]<<' '<<temp1[4]<<' '<<temp1[5]<<endl;
  //  cout<<"corner posns for seg2: "<<temp2[2]<<' '<<temp2[3]<<' '<<temp2[4]<<' '<<temp2[5]<<endl;



  //now, need to get these two segments into the format of a car
  //  cout<<"tryign to orient deltas properly...vels are: "<<vN<<' '<<vE<<endl;
  //calculating their intersection (to get the N,E points we define car as)
  double ni, ei; //northing and easting intersections
  double dn1, de1, dn2, de2; //deltas for segs
  if(temp2[0] ==0 && temp2[1] == 0) {
    ni = temp1[2];
    ei = temp1[3];
  } else {
    ei = (temp1[0] - temp2[0])/(temp1[1] - temp2[1]);
    ni = temp1[0] + temp1[1]*ei;
  }
  dn1 = temp1[4] - ni; //seg 1 deltas
  de1 = temp1[5] - ei;
  dn2 = temp2[4] - ni; //seg 2 deltas
  de2 = temp2[5] - ei;
  //  cout<<"initial corner pos and deltas: "<<ni<<' '<<ei<<' '<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
  //  cout<<"input velocity: "<<vN<<' '<<vE<<endl;
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;



  //now, making sure the corner is in the right position (using vE and vN) see p.85 - 87
  double thv, th1, th2; //velocity and segment angles
  double tn, te; //temp holders for switching values
  thv = atan2(vN,vE);
  th1 = atan2(dn1, de1);
  th2 = atan2(dn2, de2);
  //  cout<<"angles (vel, 1, 2): "<<thv<<' '<<th1<<' '<<th2<<endl;

  //putting ni,ei at back bumper
  if( sin(thv-th1) < sin(thv-th2)) { //(dn1, de1) is parallel direction of velocity
    if(cos(thv-th1) > 0) { //they're oriented the same
      //      cout<<"//all is good...(n,e) is at back, (dn1, de1) is pointed along velocity"<<endl;
    } else {
      //      cout<<" //oriented opposite, so need to flip d1"<<endl;
      ni = ni + dn1;
      ei = ei + de1;
      dn1 = -dn1;
      de1 = -de1;
    }
  } else {
    //    cout<<"//(dn2, de2) is parallel to velocity...need to switch d1 and d2"<<endl;
    tn = dn1;
    te = de1;
    if(cos(thv-th2) > 0) { //same orientation
      //      cout<<"correct d2 orientation"<<endl;
      // ni,ei are fine, only need to switch 
      dn1 = dn2;
      de1 = de2;
      dn2 = tn;
      de2 = te;
    } else { //opp orientation
      //      cout<<"need to switch d2 orientation"<<endl;
      ni = ni+dn2;
      ei = ei+dn2;
      dn1 = -dn2;
      de1 = -de2;
      dn2 = tn;
      de2 = te;
    }
  }

  /**
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;
  **/

  //putting ni,ei at RIGHT side of back bumper (i.e. th2 and thv+PI/2 are in same dir)
  th2 = atan2(dn2, de2); //need to recalculate...
  if(cos(thv + PI/2 - th2) > 0) { //they're in same direction
    //everything is set up properly
  } else { //need to switch
    ni = ni+dn2;
    ei = ei+de2;
    dn2 = -dn2;
    de2 = -de2;
  }

  //WHEW. car is set up correctly
  //  cout<<"fit a car, w/ params: "<<ni<<' '<<ei<<' '<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
  /**
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;
  **/


  return corner;
} //end fitCar

//removes object by putting its index back on openObjs, and removing 
//the index from usedObjs
void BlobLadar::removeObject(int index) {
  openObjs.push_back(usedObjs.at(index));
  objIterator = usedObjs.begin();
  objIterator+=index;
  usedObjs.erase(objIterator);

}

//FIXME: this gives bad results when line has infinite slope
double BlobLadar::fitLine(double* points, int numPoints, double* fit) {

  //  cout<<"entering fitLine"<<endl;

  double a,b; //vars for least-squares fit

  //compute mean e and n coordinates
  double e, n;
  double eerr, nerr, therr, err;

  double ssee = 0;
  double ssnn = 0;
  double ssen = 0;

  double ebar = 0;
  double nbar = 0;
  //  cout<<"entered fit line"<<endl;

  for(int i=0;i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];
//  cout<<n<<' '<<e<<' ';

    ebar += e;
    nbar += n;

    ssee += e*e;
    ssnn += n*n;
    ssen += e*n;

  }
//  cout<<endl;

  ebar = ebar/numPoints; 
  nbar = nbar/numPoints;

  //  cout<<"mean n,e "<<nbar<<", "<<ebar<<endl;

  ssee = ssee - numPoints*ebar*ebar;
  ssnn = ssnn - numPoints*nbar*nbar;
  ssen = ssen - numPoints*ebar*nbar;

  b = ssen/ssee;
  a = nbar - b*ebar;

  //now, need to have some measure of the error
  //p. 59 explains this calculation
  double SE = 0;
  for(int i=0; i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];
    nerr = n - a - b*e;
    eerr = e - (n-a)/b;
    therr = atan2(nerr,eerr);
    err = fabs(eerr*sin(therr));
    SE += err*err;
  }

  //vars for chosen representation
  double theta, rho;
  double e1, n1, e2, n2,  ei, ni;
  if(a<0) { //they're both pos, or both negative (see p. 36 of notebook)
    theta = atan2(b,1)+3*PI/2; //angle of line perpendicular to one with slope b
  }  else {
    theta = atan2(b,1)+PI/2;
  }

  //now, need to find intersection pt of y = a+b*e and y=-(1/b)*e ... 
  //this is at x = -a*b/(b*b+1), and y = a/(b*b+1);
  ei = a/(tan(theta)-b);
  ni = a+a*b/(tan(theta)-b);
  rho = sqrt(ei*ei+ni*ni);


  //technically, r1 and r2 should be the 1st and last entry in the array 
  //projected onto the line defined by theta and rho....
  //for now, I'm going to be lazy, and just define it as the distance
//#warning "HACK!!"
  e1 = points[0];
  n1 = points[1];
  e2 = points[2*numPoints-2];
  n2 = points[2*numPoints-1];
  //  cout<<"and, endpoints of the segment: "<<n1<<' '<<e1<<' '<<n2<<' '<<e2<<endl;

  //need to project these points onto the line (theta, rho) see p.55
  double ai, bi, ci, di; //parameters for calculating intersect
  double pn1, pe1, pn2, pe2; //projected points
  ai = n1 - e1 * tan(theta);
  bi = tan(theta);
  ci = rho / cos(theta - PI/2);
  di = tan(PI/2 + theta);

  pe1 = (ai - ci)/(di - bi);
  pn1 = ai + bi*pe1;

  ai = n2 - e2 * tan(theta);

  pe2 = (ai - ci)/(di - bi);
  pn2 = ai + bi*pe2;

  //  cout<<"projected points: "<<pn1<<' '<<pe1<<' '<<pn2<<' '<<pe2<<endl;

  fit[0] = a;
  fit[1] = b;
  fit[2] = pn1;
  fit[3] = pe1;
  fit[4] = pn2;
  fit[5] = pe2;

  return SE;
}


/** 
 *Function: uses the method of least-squares to fit a line of the form b = a + b * e
 * to the input points, given that it must be perpendicular to line w/ slope given
 * Then, transforms that to a line of form [alpha, rho, r1, r2]
 * uses eqns found on mathworld.wolfram.com
 * returns: the r-squared value of the fit, and changes the data in fit
 **/

double BlobLadar::fitLine2(double* points, int numPoints, double* fit, double slope) {

  //  cout<<"entering fitline2"<<endl;

  double a,b; //vars for least-squares fit

  //compute mean e and y coordinates
  double eerr, nerr, err;
  double e, n;

  double ebar = 0;
  double nbar = 0;
  for(int i=0;i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];

    ebar += e;
    nbar += n;
  }

  ebar = ebar/numPoints; 
  nbar = nbar/numPoints;

  //b must be perpendicular to line described by theta (i.e. parallel to theta)
  b = -1/slope; 
  a = nbar - b*ebar;

  //now, need to have some measure of the error
  double SE = 0;
  for(int i=0; i<numPoints;i++) {
    e = points[2*i];
    n = points[2*i + 1];
    nerr = n - a - b*e;
    eerr = e - (n-a)/b;
    err = sqrt(nerr*nerr + eerr*eerr);
    SE += err*err;
  }

  //vars for chosen representation
  double theta, rho;
  double e1, n1, e2, n2,  ei, ni;
  if(a<0) { //they're both pos, or both negative (see p. 36 of notebook)
    theta = atan2(b,1)+3*PI/2; //angle of line perpendicular to one with slope b
  }  else {
    theta = atan2(b,1)+PI/2;
  }

  //now, need to find intersection pt of y = a+b*x and y=-(1/b)*x ... 
  //this is at x = -a*b/(b*b+1), and y = a/(b*b+1);
  ei = a/(tan(theta)-b);
  ni = a+a*b/(tan(theta)-b);
  rho = sqrt(ei*ei+ni*ni);

  //technically, r1 and r2 should be the 1st and last entry in the array 
  //projected onto the line defined by theta and rho....
  //for now, I'm going to be lazy, and just define it as the distance
//#warning "HACK!!"
  e1 = points[0];
  n1 = points[1];
  e2 = points[2*numPoints-2];
  n2 = points[2*numPoints-1];

  //need to project these points onto the line (theta, rho) see p.55
  double ai, bi, ci, di; //parameters for calculating intersect
  double pn1, pe1, pn2, pe2; //projected points
  ai = n1 - e1 * tan(theta);
  bi = tan(theta);
  ci = rho / cos(theta - PI/2);
  di = tan(PI/2 + theta);

  pe1 = (ai - ci)/(di - bi);
  pn1 = ai + bi*pe1;

  ai = n2 - e2 * tan(theta);

  pe2 = (ai - ci)/(di - bi);
  pn2 = ai + bi*pe2;

  fit[0] = a;
  fit[1] = b;
  fit[2] = pn1;
  fit[3] = pe1;
  fit[4] = pn2;
  fit[5] = pe2;

  return SE;
}


void BlobLadar::KFupdate() {

  //  cout<<"entering KFupdate"<<endl;

  Matrix K1;
  K1 = Matrix(2,2);
  Matrix K2;
  K2 = Matrix(2,2);
  Matrix Nerr;
  Nerr = Matrix(1,1);
  Matrix Eerr;
  Eerr = Matrix(1,1);
  Matrix tmp;
  tmp = Matrix(1,1);
  Matrix tmp2;
  tmp2 = Matrix(1,1);
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int j = usedObjs.at(i);
    //    cout<<"last seen: "<<objects[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(objects[j].timesSeen > 1) {
      if(objects[j].lastSeen == numFrames) {
        double grr[] = {objects[j].N};
        tmp.setelems(grr);
	//	cout<<"Northing: "<<objects[j].N<<" and, predicted northing: "<<objects[j].Nmu_hat.getelem(0,0)<<endl;
        Nerr = tmp - C*objects[j].Nmu_hat;
	//	cout<<"Nerror: "<<Nerr.getelem(0,0)<<' ';
        K1 = objects[j].Nsigma_hat * C.transpose() * (C*objects[j].Nsigma_hat*C.transpose() + Q).inverse();
        objects[j].Nmu = objects[j].Nmu_hat + K1 * Nerr;
        objects[j].Nsigma = (I - K1*C) * objects[j].Nsigma_hat;

        double grr2[] = {objects[j].E};
        tmp2.setelems(grr2);

        Eerr = tmp2 - C*objects[j].Emu_hat;
	//	cout<<" and, Eerror: "<<Eerr.getelem(0,0)<<endl;
        K2 = objects[j].Esigma_hat * C.transpose() * (C*objects[j].Esigma_hat*C.transpose() + Q).inverse();
        objects[j].Emu = objects[j].Emu_hat + K2 * Eerr;
        objects[j].Esigma = (I - K2*C) * objects[j].Esigma_hat;
	//	cout<<"new vel (actually calculated) "<<objects[j].Nmu.getelem(1,0)<<' '<<objects[j].Emu.getelem(1,0)<<endl;

      } else {

	objects[j].Nmu = objects[j].Nmu_hat;
	objects[j].Nsigma = objects[j].Nsigma_hat;
	objects[j].Emu = objects[j].Emu_hat;
	objects[j].Esigma = objects[j].Esigma_hat;

      }

    }
  }


  for(unsigned int i=0; i<usedCars.size(); i++) {
    //    cout<<"entering loop w/ i = "<<i<<endl;
    int j = usedCars.at(i);
    //    cout<<"last seen: "<<cars[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(cars[j].timesSeen > 1) {
      if(cars[j].lastSeen == numFrames) {

	double grr[] = {cars[j].n};
	tmp.setelems(grr);

	//	cout<<"line 2239"<<endl;
        Nerr = tmp - C*cars[j].Nmu_hat;
	//	cout<<"Nerr for car: "<<Nerr.getelem(0,0)<<endl;
        K1 = cars[j].Nsigma_hat * C.transpose() * (C*cars[j].Nsigma_hat*C.transpose() + Q).inverse();
	//	cout<<"line 2242"<<endl;
        cars[j].Nmu = cars[j].Nmu_hat + K1 * Nerr;
	//	cout<<"new n,ndot: "<<cars[j].Nmu.getelem(0,0)<<' '<<cars[j].Nmu.getelem(1,0)<<endl;
        cars[j].Nsigma = (I - K1*C) * cars[j].Nsigma_hat;

	//	cout<<"ln 2246"<<endl;
	double grr2[] = {cars[j].e};
	tmp2.setelems(grr2);

	//	cout<<"ln 2250"<<endl;
        Eerr = tmp2 - C*cars[j].Emu_hat;
        K2 = cars[j].Esigma_hat * C.transpose() * (C*cars[j].Esigma_hat*C.transpose() + Q).inverse();
	//	cout<<"ln 2253"<<endl;
        cars[j].Emu = cars[j].Emu_hat + K2 * Eerr;
        cars[j].Esigma = (I - K2*C) * cars[j].Esigma_hat;

      } else {
	//	cout<<"ln 2258"<<endl;
	cars[j].Nmu = cars[j].Nmu_hat;
	cars[j].Nsigma = cars[j].Nsigma_hat;
	//	cout<<"ln 2261"<<endl;
	cars[j].Emu = cars[j].Emu_hat;
	//	cout<<"ln 2263"<<endl;
	cars[j].Esigma = cars[j].Esigma_hat;
	//	cout<<"ln 2264"<<endl;
      }
    }
  }

  //  cout<<"exiting KFupdate"<<endl;
}
 
void BlobLadar::KFpredict() {

  //  cout<<"entering KFpredict()"<<endl;

  for(unsigned int i=0; i<usedObjs.size() ; i++) {
    int j = usedObjs.at(i);

    objects[j].Nmu_hat =    A*objects[j].Nmu;
    objects[j].Nsigma_hat = A*objects[j].Nsigma*A.transpose() + R;
    objects[j].Emu_hat =    A*objects[j].Emu;
    objects[j].Esigma_hat = A*objects[j].Esigma*A.transpose() + R;

  } //end cycling through numObjects


  //and, objcars

 for(unsigned int i=0; i<usedCars.size(); i++) {
    int j = usedCars.at(i);

    //    cout<<"ln 2300"<<endl;
    cars[j].Nmu_hat =    A*cars[j].Nmu;
    cars[j].Nsigma_hat = A*cars[j].Nsigma*A.transpose() + R;
    //    cout<<"ln2303"<<endl;
    cars[j].Emu_hat =    A*cars[j].Emu;
    cars[j].Esigma_hat = A*cars[j].Esigma*A.transpose() + R;
 } //end cycling through numCars

} //end KFpredict()


void BlobLadar::cleanObjects()
{
  int j; 
  int diff;
  for(unsigned int i=0; i<usedObjs.size(); i++)
  {
    j = usedObjs.at(i);
    diff = numFrames - objects[j].lastSeen;
    //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
    if(diff > OBJECTTIMEOUT) {
      fprintf(stderr, "tryng to remove object # %d! \n", j);
      removeObject(i);
    }
  }

}


/**
 * this function is called when it is determined that a new object
 * matches an already seen car.
 **/
//#warning "FIXME: this update is very incomplete -- only replaces old data, doesn't merge at all"
//#warning "At this point, simply replaces Car w/ car formed by Points...add in sam's update rules"
void BlobLadar::updateCar(carRepresentation* Car, objectRepresentation* newObj) {

  //  cout<<"entering updateCar"<<endl;
  vector<NEcoord> points(newObj->scans.back());
  double dStart = newObj->dStart;
  double dEnd = newObj->dEnd;

  int numPoints = points.size();

  double pts[2*numPoints];
  for(int i=0; i<numPoints; i++) {
    pts[2*i] = points.at(i).E;
    pts[2*i+1] = points.at(i).N;
  }
  //  cout<<"points in object: "<<pts[0]<<' '<<pts[1]<<' '<<pts[2*numPoints-2]<<' '<<pts[2*numPoints-1]<<endl;
  //  cout<<"center of object: "<<newObj->N<<' '<<newObj->E<<endl;
  carRepresentation newCar;
  //  cout<<"updating car #"<<Car->ID<<" at "<<Car->n<<' '<<Car->e<<endl;
  fitCar(pts,numPoints,&newCar, Car->Nmu.getelem(1,0), Car->Emu.getelem(1,0)); 

  //  cout<<"Params for old car: "<<Car->n<<' '<<Car->de1<<' '<<Car->dn1<<' '<<Car->e<<' '<<Car->dn2<<' '<<Car->de2<<endl;
  //  cout<<"Params for new car: "<<newCar.n<<' '<<newCar.de1<<' '<<newCar.dn1<<' '<<newCar.e<<' '<<newCar.dn2<<' '<<newCar.de2<<endl;

  /**
   * depending on how much of the car we see in the old measurement, update
   * the old car (if we see the corner, that carries more information than 
   * seeing only part of a segment. see p. 86-87
   * we determine how much of car we saw by occlusions
   **/
  int sawFront = 0; //whether dStart&dEnd say we actually saw the boundary of car
  int sawBack = 0;

  //need angles to front/back of car (rel to alice), to determine how they 
  //correspond to dStart&dEnd. will be in range of 0-PI (how ladar works)
  double th1, th2;
  //#warning "for here (and elsewhere) will need to add yaw when using real data"
  double foo1, foo2, bar1, bar2;
  foo1 = Car->n - currState[0];
  foo2 = Car->e - currState[1];
  bar1 = Car->n + Car->dn1 - currState[0];
  bar2 = Car->e + Car->de1 - currState[1];
  //  cout<<"deltas: "<<foo1<<' '<<foo2<<' '<<bar1<<' '<<bar2<<endl;
  th1 = atan2(Car->n - currState[0], Car->e - currState[1]);
  th2 = atan2(Car->n + Car->dn1 - currState[0], Car->e + Car->de1 - currState[1]);
  //  th1 = atan2(Car->e - currState[1], Car->n - currState[0]);
  //  th2 = atan2(Car->e + Car->de1 - currState[1], Car->n + Car->dn1 - currState[0]);

  //  cout<<"thetas: "<<th1<<' '<<th2<<endl;
  if(th1 < th2) { // n,e (back bumper) correspond to start, and d1 (front bumper) correspond to end
    //  if(1==1) { // for now, we're guaranteed that back == start
    if(dStart >= 0) { //start was in foreground
      sawBack = 1;
    }
    if(dEnd >= 0) { //end was in foreground
      sawFront = 1;
    }
  } else { // back bumper corresponds to end
    if(dStart >= 0) {
      sawFront = 1;
    }
    if(dEnd >= 0) {
      sawBack = 1;
    }
  }

  //now that we know whether to trust start/end coords, can calculate updates
  if(sawBack == 1) {
    if(sawFront == 1) {
//      cout<<" //have full information on this car, can entirely replace"<<endl;
      //#warning "replacing isn't the best option here...better to update d's to take into account ALL information"
    double dn, de;
    dn = newCar.n + newCar.dn1 - Car->n - Car->dn1;
    de = newCar.e + newCar.de1 - Car->e - Car->de1;
    Car->n = Car->n + dn;
    Car->e = Car->e + de;
    //#warning "for some reason, we think we've seen the back when we havent...thus, only using deltas here"
    //      Car->n = newCar.n;
    //      Car->e = newCar.e;
      //      Car->dn1 = newCar.dn1;
      //      Car->de1 = newCar.de1;
      //      Car->dn2 = newCar.dn2;
      //      Car->de2 = newCar.de2;
    } else {
      //    cout<<" //only know back bumper, only update position"<<endl;
      Car->n = newCar.n;
      Car->e = newCar.e;
    }
  } else if (sawFront == 1) {
    //    cout<<"//only know front bumper, only update deltas"<<endl;
    double dn, de;
    dn = newCar.n + newCar.dn1 - Car->n - Car->dn1;
    de = newCar.e + newCar.de1 - Car->e - Car->de1;
    Car->n = Car->n + dn;
    Car->e = Car->e + de;
  }

  //if we were able to update car, update history/log of when seen
  if(sawFront + sawBack > 0) {
    //setting up history vector...
    int histptr = Car->histPointer;

    //  cout<<"setting ptr to "<<histptr<<" plus 1"<<endl;
    Car->histPointer = histptr + 1;
    Car->lastE[histptr%10] = Car->e;
    Car->lastN[histptr%10] = Car->n;

    Car->timesSeen = Car->timesSeen + 1;
    Car->lastSeen = numFrames;
  } else {
    //    cout<<"tried to update car, not enough information"<<endl<<endl;
  }
} //end updateCar

