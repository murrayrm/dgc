using namespace std;

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sp.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <vector>

//scaling factor for m so image fits on display
#define SCALING          .3

#define XMIN  		-20
#define XMAX   		 20
#define ZMIN  		-20
#define ZMAX   		 20
#define MAX_MESSLEN      102400

#define BLOB_RED        0
#define BLOB_ORANGE     1
#define BLOB_YELLOW     2
#define BLOB_GREEN      3
#define BLOB_BLUE       4
#define BLOB_BLACK      5

struct State {
  double N;
  double E;
};

struct ladar_point {
  double x;
  double y;
  double z;
};

struct ladar_line {
  double x1;
  double x2;
  double y1;
  double y2;
  int color;
};

struct ladar_object {
  double x;
  double y;
  double r;
  int color;
};

class LindzeyDisplay
{
public:
  LindzeyDisplay(int *argc, char **argv);
  ~LindzeyDisplay();
 

  void mainOpenGL();
  void clearObjects();
  void addObject(double x, double y, double r, int age);
  void sendPoint(double x, double y);
  void sendScan(int numPts, double *xpts, double *ypts, double *zpts);
  void sendLine(double x1, double x2, double y1, double y2, int color=5);
  void sendLines(int numlines, double *x1, double *x2, double *y1, double *y2);
private:
State state;
  int *arg1;
  char **arg2;

  //for each ladar, holds ladar position relative to alice's origin
  double ladarPos[16][3];



};
