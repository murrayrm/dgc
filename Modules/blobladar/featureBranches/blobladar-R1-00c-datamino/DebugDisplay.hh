using namespace std;

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sp.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include <vector>

//scaling factor for m so image fits on display
#define SCALING          .2

#define XMIN  		-20
#define XMAX   		 20
#define ZMIN  		-20
#define ZMAX   		 20
#define MAX_MESSLEN      102400

struct State {
  double N;
  double E;
};

struct ladar_point {
  double x;
  double y;
  double z;
};

struct ladar_line {
  double x1;
  double x2;
  double y1;
  double y2;
};

struct ladar_object {
  double x;
  double y;
  double r;
};

class LindzeyDisplay
{
public:
  LindzeyDisplay(int *argc, char **argv);
  ~LindzeyDisplay();
 

  void mainOpenGL();
  void clearObjects();
  void addObject(double x, double y, double r);
  void sendPoint(double x, double y);
  void sendScan(int numPts, double *xpts, double *ypts, double *zpts);
  void sendLine(double x1, double x2, double y1, double y2);
  void sendLines(int numlines, double *x1, double *x2, double *y1, double *y2);
private:
State grr_state;
  int *arg1;
  char **arg2;



};
