/*
 * This is all the code for trakcing
 * blobs in ladar data.
 * Most of it comes directly from Laura's
 * summer project, at least for now
 */ 

#include "BlobLadar.hh"
using namespace std;

void BlobLadar::foo()
{
  //  fprintf(stderr, "foo! \n");
  return;
}

void BlobLadar::trackerInit()
{
  numFrames = 0;
  numObjects = 0;
  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    //    fprintf(stderr, "pushing back %d \n", i);
    openObjs.push_back(i);
  }
  //TODO: add KF stuff (

  //  setupLogs();
  //  cout<<"trackerInit(...) Finished"<<endl;

  return;
}

void BlobLadar::processScan()
{
  numFrames++;

  segmentScan(); 
  prepObjects();
  classifyObjects();
  sendObjects();
  return;
}

void BlobLadar::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][1] - rawData[i][1];
  }

  int tempNum = 0; //number objects segmented from curr scan
  int currSize = 1; //size of current object

  // This is the actual segmentation functionality...
  // the above was just setting up the data we need

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  double distThresh;
  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    if(rawData[i][1] > 80.0) {
      distThresh = 0;
    } else {
    //threshold should be scaled based on distance from ladar
    distThresh = max(.10,rawData[i][1]*.0175*SCALINGFACTOR);
    //    cout<<"range: "<<rawData[i]<<"   threshold: "<<distThresh<<endl;
    }
    //if the distance is too large to be the same object
    //note that this distance is the distance btwn point i and pt i+1
    if(fabs(diffs[i]) > distThresh) {
 
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;

    } else { //this point belongs to current object
      
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points

  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  int origin;
  numSegments = 0;
  for(int i=0; i<=tempNum; i++) {
    st = tempPos[i][0];
    en = tempPos[i][1];
    origin=0; 
    //    cout<<"rawData: ";
    for(int j=st; j<=en; j++) {
      //      cout<<rawData[j]<<", ";
      if(rawData[j][1] > 80) {
	origin=1;
      } else {
	//	cout<<'('<<newPoints[j].N<<' '<<newPoints[j].E<<"), ";
      }
    }
    //    cout<<endl;
    //checking that this isn't a group of no-return points
    if(origin==0 && tempSize[i]>=MINNUMPOINTS) {
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      numSegments++;
    }
  }

  //TODO: add convex hull requirement

  //now, trying to output the segmentation, to check that it's correct
  //TODO: output this to gui (sensviewer?)

  //now, send the segments to the gui
  double x1[numSegments];
  double x2[numSegments];
  double y1[numSegments];
  double y2[numSegments];
  int tmpindex;

  //  fprintf(stderr, "num segments: %d \n", numSegments);
  for(int i=0; i<numSegments;i++) {
    //      m_outputSegments<<posSegments[i][0]<<' '<<posSegments[i][1]<<' ';
      //FIXME: this only works for alice at the origin.
      //need to change this to be alices's state
      x1[i]=0;
      y1[i]=0;
      tmpindex = posSegments[i][1];
      x2[i]=utmData[tmpindex][0];
      y2[i]=utmData[tmpindex][1];
  }
  myDisplay->sendLines(numSegments, x1, x2, y1, y2);

  /**
  for(int i=numSegments; i<NUMSCANPOINTS; i++) {
    m_outputSegments<<"-1 -1 ";
  }
  m_outputSegments<<endl;
  **/

  //  fprintf(stderr, "we found %d segments \n", numSegments);


  return;
}


/**
 * This code goes through all the new segments, picks 
 * out the ones that correspond to objects that we care 
 * about (based on number of points in object, and that 
 * the object isn't a group of no-return points) 
 * It then updates the candidates array with the 
 * appropriate information
**/
void BlobLadar::prepObjects() {

  //  cout<<"entering prepObjects()"<<endl;
  //  cout<<"STARTING prepObjects()"<<endl;

  //TODO: change this. candidates is a confusing var name 
  numCandidates = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  for(int i=0; i<numSegments; i++) {

    int objectsize = sizeSegments[i];

    if(objectsize >= MINNUMPOINTS) {

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      //if dStart or dEnd > 0, that means that that end of the object 
      //is in the foreground, <0 means the object next to it is in the 
      //foreground, and 0 means that it's at the edge of a scan
      //#warning "check that ranges don't get set to 0 for no-return values before we get here"
      if(objStart == 0) {
	dStart = 0;
      } else {
	dStart = -diffs[objStart - 1];
      }
      if(objEnd == NUMSCANPOINTS - 1) {
	dEnd = 0;
      } else {
	dEnd = diffs[objEnd];
      }

      //want to fit center to object here, and put 
      // it in candidates array....

      //first, check that it's not a group of no-return points
      if(rawData[objStart][1] < 80.0 && rawData[objEnd][1] < 80.0 ) {

	double cenN, cenE;
	double sumN = 0;
	double sumE = 0;

	//TODO: check this 
	//currently have x from sensnet_ladar_vehicle_to_local
	//as the northing value
	for(int j=0;j<objectsize;j++) {
	  sumE += utmData[j+objStart][1];
	  sumN += utmData[j+objStart][0];
	}

	cenN = sumN / objectsize;
	cenE = sumE / objectsize;

	candidates[numCandidates].N = cenN;
	candidates[numCandidates].E = cenE; 
	//	cout<<"in prep objects, new object's N is: "<<candidates[numCandidates].N<<endl;

	candidates[numCandidates].dStart = dStart;
	candidates[numCandidates].dEnd = dEnd; 

	vector<NEcoord> tmppts;
	vector<double> rngs;
	vector<double> angs;

	NEcoord tmp;
	double foo, bar;

	//#warning "keeping this many points may be a problem"
	for(int m=0;m<objectsize;m++) {
	  tmp.N = utmData[m+objStart][0];
	  tmp.E = utmData[m+objStart][1];
	  tmppts.push_back(tmp);
	  foo = rawData[m+objStart][1];
	  bar = rawData[m+objStart][0];
	  rngs.push_back(foo);
	  angs.push_back(bar);
	}


	//TODO: these cause segfaults - don't need now,
	//but leaving in because for more sophisticated
	//object representations, will want this data
	candidates[numCandidates].scans.push_back(tmppts);
	candidates[numCandidates].ranges.push_back(rngs);
	candidates[numCandidates].angles.push_back(angs);

	numCandidates++; //we've found a new object

      } //end ranges condition
    }   //end num points condition
  }     //end cycling through segments
  //    fprintf(stderr, "we have %d new objects \n", numCandidates);
}   //end of prepObjects()



/**
 * This function assumes that the scan has been segmented, and 
 * classifies the new objects into: already-seen object, already-seen
 * car, and new object. Then, it calls the appropriate function to update
 * the representations.
 **/
void BlobLadar::classifyObjects() 
{
  //  cout<<"STARTING trackObjects()"<<endl;
  double distTraveled;

  int taken[numCandidates];
  for(int i=0;i<numCandidates;i++)
    taken[i] = 0;

  double oldN, oldE;
  double newN, newE;

  /**
   * First, we cycle through all previously seen objects to see if the new 
   * object matches one of them
   **/
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int k = usedObjs.at(i);
    oldN = objects[k].N;
    oldE = objects[k].E;

    for(int j=0; j<numCandidates; j++) {
      if(taken[j] == 0) { //if new object hasn't already been classified
	newN = candidates[j].N;
	newE = candidates[j].E;
	distTraveled = dist(oldN, oldE, newN, newE);

	if(distTraveled < MAXTRAVEL) { //if they match
	  //	  cout<<"new object matched old object...updating!"<<endl;
	  taken[j] = 1;
	  //	  fprintf(stderr,"updating object #%d \n",k);
	  updateObject(&objects[k], &candidates[j]);

	} //end checking distance traveled
      } //end checking of newObj is taken
    } //end cycling through new objects
  } //end cycling through old objects


  /**
   * Finally, we take care of the new objects that don't match anything we've
   * seen before
   **/
  for(int i=0; i<numCandidates; i++) {
    if(taken[i] == 0) { //this is a lonely, unwanted new object
      //      cout<<"new object didn't match anything -- creating new obj!"<<endl;
      //      fprintf(stderr,"adding new object! \n");
      addObject(&candidates[i]);
    } //end checking if object already classified
  } //end cycling through numCandidates

}

/**
 * This function is called when it is determined that a newobject 
 * matches an old one. As such, it simply replaces the center position, 
 * and adds the latest scan/ranges data
**/
void BlobLadar::updateObject(objectRepresentation* oldObj, objectRepresentation* newObj) {

  oldObj->N = newObj->N;
  oldObj->E = newObj->E;

  //  fprintf(stderr,"updated object at: %f, %f \n", newObj->N, newObj->E);

  oldObj->dStart = newObj->dStart;
  oldObj->dEnd = newObj->dEnd;

  vector<NEcoord> newscans(newObj->scans.back());
  vector<double> newranges(newObj->ranges.back());
  vector<double> newangles(newObj->angles.back());

  //  fprintf(stderr,"updating object \n");
  oldObj->scans.push_back(newscans);
  oldObj->ranges.push_back(newranges);
  oldObj->angles.push_back(newangles);

  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;
  //TODO: add KF stuff here!!

}


/**
 * This function is called when it is determined that a new object
 * has no match in already seen objects or cars
 * It's job is to add an object to the object array, if there is room.
 **/
void BlobLadar::addObject(objectRepresentation* newObj) {
  numObjects = numObjects + 1;
  //  fprintf(stderr, "entering add object, w/ object # %d \n", numObjects);
  if(usedObjs.size() < MAXNUMCARS) {

    int temp;
    temp = openObjs.back();
    openObjs.pop_back();
    usedObjs.push_back(temp);

    objects[temp].ID = numObjects;
    objects[temp].N = newObj->N;
    objects[temp].E = newObj->E;
    objects[temp].dStart = newObj->dStart;
    objects[temp].dEnd = newObj->dEnd;

    objects[temp].timesSeen = 1;
    objects[temp].lastSeen = numFrames;

    //transferring vectors of scan/range/angle history
    //    fprintf(stderr,"adding new object %d \n", numObjects);
    vector<NEcoord> newscans(newObj->scans.back());
    vector<double> newranges(newObj->ranges.back());
    vector<double> newangles(newObj->angles.back());

    objects[temp].scans.push_back(newscans);
    objects[temp].ranges.push_back(newranges);
    objects[temp].angles.push_back(newangles);
 
    //initizlizing KF variables
    /** currently, no KF being done
    objects[temp].Nmu.resetSize(2,1);
    objects[temp].Emu.resetSize(2,1);
    objects[temp].Nsigma.resetSize(2,2);
    objects[temp].Esigma.resetSize(2,2);
    objects[temp].Nmu_hat.resetSize(2,1);
    objects[temp].Emu_hat.resetSize(2,1);
    objects[temp].Nsigma_hat.resetSize(2,2);
    objects[temp].Esigma_hat.resetSize(2,2);

    double NMelems[] = {objects[temp].N, 0};
    double EMelems[] = {objects[temp].E, 0};
    double NSelems[] = {1,0,0,1};
    double ESelems[] = {1,0,0,1};
    //    cout<<"when adding new object, N is: "<<objects[temp].N<<endl;
    objects[temp].Nmu.setelems(NMelems);
    objects[temp].Nsigma.setelems(NSelems);
    objects[temp].Emu.setelems(EMelems);
    objects[temp].Esigma.setelems(ESelems);
    //    cout<<" and Nmu is: "<<objects[temp].Nmu.getelem(0,0)<<endl;
    */

  } else {
    cerr<<"trying to add obj, no space in buffer"<<endl;

  } //end checking that there's enough room to add new object
} //end addObject


//TODO: use actual struct/message type that sam defines
//TODO: actually calculate radius of object
void BlobLadar::sendObjects() {
  //  cout<<"entering sendObjects()"<<endl;
  //  cout<<"number of cars: "<<usedCars.size()<<endl;

  //first, we clear the object list
  myDisplay->clearObjects();


  objectMessage obstacle;

  int sent = 0;
  for(unsigned int k=0; k<usedObjs.size(); k++) {
    int j = usedObjs.at(k);

    sent++;
    obstacle.ID = objects[j].ID;


    obstacle.center.N = objects[j].N;
    obstacle.center.E = objects[j].E;
    obstacle.radius = 3.0;

    myDisplay->addObject(objects[j].N, objects[j].E, obstacle.radius);

    //    fprintf(stderr, "trying to send object %d \n", j);
    //    int msgSize = sizeof(movingObstacle);
    //    m_skynet.send_msg(m_send_socket, &obstacle, msgSize, 0);

  } // end checking each car

  //  cout<<"exiting sendObjects()"<<endl;
  return;
}
