/**
 * File: BlobTracking.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 11
 **/

#include "BlobLadar.hh"
using namespace std;

void BlobLadar::trackerInit()
{
  numFrames = 0;
  numObjects = 0;

  useMapTalker = 1;
  subgroup = 0;

  moduleMapID = -1;  
  initSendMapElement(skynetKey);

  logfile = fopen("BlobLadar.log","w");

  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    openObjs.push_back(i);
    openCars.push_back(i);
  }

  //setting up matrics for KF calculations
  //#warning "these need to be tuned!"
  double dt = .0133; 
  double sa = .05; //std dev of system noise -- tune!
  double sz = .1; //std dev of measurement noise -- should be fxn of range/theta!!!

  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);

  return;
}

void BlobLadar::processScan(int ladarID)
{
  //  fprintf(stderr, "processScan called for ladar # %d \n", ladarID);
  numFrames++;

  //first, we clear the object list. 
  if(useDisplay) {
    myDisplay->clearObjects();
  }

  segmentScan(); 
  createObjects();
  classifyObjects();

  checkMotion();

  sendObjects();
  sendCars();

  cleanObjects();
  //  cleanCars();

  KFupdate();
  KFpredict();

  return;
}

void BlobLadar::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][RANGE] - rawData[i][RANGE];
  }

  //number objects segmented from curr scan
  int tempNum = 0; 
  //size of current object
  int currSize = 1; 

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  tempSize[0] = 0;

  double distThresh;
  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    if(rawData[i][RANGE] > 80.0) {
      distThresh = 0;
    } else {
      //threshold scaled based on range
      distThresh = max(.10,rawData[i][RANGE]*.0175*SCALINGFACTOR);
    }
  
    //if the distance is too large to be the same object
    //note that this distance is the distance btwn point i and pt i+1
    if(fabs(diffs[i]) > distThresh) {
 
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;

    } else { //this point belongs to current object
      
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points

  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  int origin;
  numSegments = 0;
  fprintf(logfile, "segment sizes: ");

  // checking if each group of points is at the origin
  for(int i=0; i<=tempNum; i++) {
    st = tempPos[i][0];
    en = tempPos[i][1];
    origin=0; 

    for(int j=st; j<=en; j++) 
    {
      if(rawData[j][RANGE] > 80) 
      {
	origin=1;
      } 
    }

    //checking that this isn't a group of no-return points
    if(origin==0 && tempSize[i]>=MINNUMPOINTS) {
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      int foo = tempSize[i];
      fprintf(logfile," %d , ",foo);
      numSegments++;
    }

  }
    fprintf(logfile,"\n");
  //TODO: add convex hull requirement

  // send the segments to the gui
  double n1[numSegments];
  double n2[numSegments];
  double e1[numSegments];
  double e2[numSegments];
  int tmpindex;

  for(int i=0; i<numSegments;i++) {
      //FIXME: this only works for ladar at the origin.
      //need to change this to be ladar's position
      n1[i]=ladarState.N;
      e1[i]=ladarState.E;
      tmpindex = posSegments[i][1];
      n2[i]=utmData[tmpindex][NORTH];
      e2[i]=utmData[tmpindex][EAST];
  }
  if(useDisplay) {
    // sending the segmentation lines
    //    myDisplay->sendLines(numSegments, n1, n2, e1, e2);
  }

  return;
}


/**
 * This code goes through all the new segments
 * and creates new objects for them. 
 * The checking for min number points and no-return
 * points was done in segment scan
 **/
void BlobLadar::createObjects() {

  int tempCounter = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  for(int i=0; i<numSegments; i++) 
  {

    int objectsize = sizeSegments[i];

    objStart = posSegments[i][0];
    objEnd = posSegments[i][1];

    //if dStart or dEnd > 0, that means that that end of the object 
    //is in the foreground, <0 means the object next to it is in the 
    //foreground, and 0 means that it's at the edge of a scan
    //#warning "check that ranges don't get set to 0 for no-return values before we get here"
    if(objStart == 0) {
  	dStart = 0;
    } else {
  	dStart = -diffs[objStart - 1];
    }
    if(objEnd == NUMSCANPOINTS - 1) {
  	dEnd = 0;
    } else {
  	dEnd = diffs[objEnd];
    }

    //want to fit center to object here, and put 
    // it in newObjects array....


    double cenN, cenE;
    double sumN = 0;
    double sumE = 0;

    //TODO: check this 
    //currently have x from sensnet_ladar_vehicle_to_local
    //as the northing value
    fprintf(logfile, "object size, object start: %d, %d \n", objectsize, objStart);
    for(int j=0;j<objectsize;j++) 
    {
      //FIXME: sometimes segfaults here. WHY???

      sumN += utmData[j+objStart][NORTH];
      sumE += utmData[j+objStart][EAST];
    }

    cenN = sumN / objectsize;
    cenE = sumE / objectsize;

    newObjects[tempCounter].N = cenN;
    newObjects[tempCounter].E = cenE; 
    
    newObjects[tempCounter].dStart = dStart;
    newObjects[tempCounter].dEnd = dEnd; 

    vector<NEcoord> tmppts;
    vector<double> rngs;
    vector<double> angs;

    NEcoord tmp;
    double foo, bar;

    //#warning "keeping this many points may be a problem"
    for(int m=0;m<objectsize;m++) {
      tmp.N = utmData[m+objStart][0];
      tmp.E = utmData[m+objStart][1];
      tmppts.push_back(tmp);
      foo = rawData[m+objStart][1];
      bar = rawData[m+objStart][0];
      rngs.push_back(foo);
      angs.push_back(bar);
    }

    newObjects[tempCounter].scans.push_back(tmppts);
    newObjects[tempCounter].ranges.push_back(rngs);
    newObjects[tempCounter].angles.push_back(angs);

    tempCounter++; //we've added a new object
  
  }     //end cycling through segments

}   //end of prepObjects()



/**
 * This function assumes that the scan has been segmented,
 * and classifies the new objects into: 
 *    already-seen object
 *    already-seen car
 *    new object 
 * Then, it calls the appropriate functions to update
 * the representations.
 **/
void BlobLadar::classifyObjects() 
{

  double distTraveled;

  //whether this segment has already been assigned
  //WARNING: leads to screwy behaviour if for some reason
  //   we have multiple "objects" corresponding to the same
  //   physical object
  int taken[numSegments];
  for(int i=0;i<numSegments;i++)
    taken[i] = 0;

  double oldN, oldE;
  double newN, newE;

  /**
   * First, we cycle through all previously seen objects 
   * to see if the new object matches one of them
   **/
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int k = usedObjs.at(i);
    oldN = objects[k].Nmu.getelem(0,0);
    oldE = objects[k].Emu.getelem(0,0);
    //        oldN = objects[k].N;
    //  oldE = objects[k].E;

    for(int j=0; j<numSegments; j++) {
      if(taken[j] == 0) { //if new object hasn't already been classified
	newN = newObjects[j].N;
	newE = newObjects[j].E;
	distTraveled = dist(oldN, oldE, newN, newE);

	if(distTraveled < MAXTRAVEL) { //if they match
	  //	  cout<<"new object matched old object...updating!"<<endl;
	  taken[j] = 1;
	  //	  fprintf(stderr,"updating object #%d \n",k);
	  updateObject(&objects[k], &newObjects[j]);

	} //end checking distance traveled
      } //end checking of newObj is taken
    } //end cycling through new objects
  } //end cycling through old objects



  /**
   * Next, we cycle through all previously seen cars 
   * to see if obj matches. This functionality goes 
   * AFTER matching newobjs to oldobjs cuz we don't want 
   * to match an already seen lamppost in the car's path 
   * to the car...
   **/
  double Nmin, Nmax, Emin, Emax;
  double dn1, de1, dn2, de2;
  int foo = 0;
  for(unsigned int i=0; i<usedCars.size(); i++) {
    int k = usedCars.at(i);

    oldN = cars[k].Nmu_hat.getelem(0,0);
    oldE = cars[k].Emu_hat.getelem(0,0);
    //    oldN = cars[k].N;
    //    oldE = cars[k].E;
    dn1 = cars[k].dn1;
    de1 = cars[k].de1;
    dn2 = cars[k].dn2;
    de2 = cars[k].de2;

    //creating bounding box for potential matches
    Nmin = min(oldN, min(oldN+dn1, min(oldN+dn2, oldN+dn1+dn2))) - CARMARGIN;
    Nmax = max(oldN, max(oldN+dn1, max(oldN+dn2, oldN+dn1+dn2))) + CARMARGIN;
    Emin = min(oldE, min(oldE+de1, min(oldE+de2, oldE+de1+de2))) - CARMARGIN;
    Emax = max(oldE, max(oldE+de1, max(oldE+de2, oldE+de1+de2))) + CARMARGIN;


    for(int j=0; j<numSegments; j++) {
      if(taken[j] == 0) { //this obj has not already been claimed
	newN = newObjects[j].N;
	newE = newObjects[j].E;
	//check if this obj matches car
	if(newN < Nmax && newN > Nmin && newE < Emax && newE > Emin) {
	  //the obj and car should be associated
	 
	  taken[j] = 1;
	  fprintf(stderr, "new obj matched car # %d -- updating!! \n", k);
	  updateCar(&cars[k], &newObjects[j]);
	  foo++;
	}  //end of checking if newObj and car match
      }   //end of checking if obj already claimed
    }    //end cycling thru newObjs
  }     //end cycling thru numCars




  /**
   * Finally, we take care of the new objects that don't match anything we've
   * seen before
   **/
  int numNew = 0;
  for(int i=0; i<numSegments; i++) {
    if(taken[i] == 0) { 
      addObject(&newObjects[i]);
      numNew++;
    } //end checking if object already classified
  } //end cycling through numSegments
  //  fprintf(stderr, "number new objects: %d, num old: %d \n", numNew, numSegments-numNew);

}

/**
 * This function is called when it is determined that a newobject 
 * matches an old one. As such, it simply replaces the center position, 
 * and adds the latest scan/ranges data
**/
void BlobLadar::updateObject(objectRepresentation* oldObj, objectRepresentation* newObj) {

  oldObj->N = newObj->N;
  oldObj->E = newObj->E;

  //  fprintf(stderr,"updated object at: %f, %f \n", newObj->N, newObj->E);

  oldObj->dStart = newObj->dStart;
  oldObj->dEnd = newObj->dEnd;

  vector<NEcoord> newscans(newObj->scans.back());
  vector<double> newranges(newObj->ranges.back());
  vector<double> newangles(newObj->angles.back());

  //  fprintf(stderr,"updating object \n");
  oldObj->scans.push_back(newscans);
  oldObj->ranges.push_back(newranges);
  oldObj->angles.push_back(newangles);

  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;

}


/**
 * This function is called when it is determined that a new object
 * has no match in already seen objects or cars
 * It's job is to add an object to the object array, if there is room.
 **/
void BlobLadar::addObject(objectRepresentation* newObj) {
  numObjects = numObjects + 1;
  //  fprintf(stderr, "entering add object, w/ object # %d \n", numObjects);
  if(usedObjs.size() < MAXNUMCARS) {

    int newIndex;
    newIndex = openObjs.back();
    openObjs.pop_back();
    usedObjs.push_back(newIndex);

    objects[newIndex].ID = numObjects;
    objects[newIndex].N = newObj->N;
    objects[newIndex].E = newObj->E;
    objects[newIndex].dStart = newObj->dStart;
    objects[newIndex].dEnd = newObj->dEnd;

    objects[newIndex].timesSeen = 1;
    objects[newIndex].lastSeen = numFrames;

    //transferring vectors of scan/range/angle history
    vector<NEcoord> newscans(newObj->scans.back());
    vector<double> newranges(newObj->ranges.back());
    vector<double> newangles(newObj->angles.back());

    objects[newIndex].scans.push_back(newscans);
    objects[newIndex].ranges.push_back(newranges);
    objects[newIndex].angles.push_back(newangles);
 
    //initizlizing KF variables
    objects[newIndex].Nmu.resetSize(2,1);
    objects[newIndex].Emu.resetSize(2,1);
    objects[newIndex].Nsigma.resetSize(2,2);
    objects[newIndex].Esigma.resetSize(2,2);
    objects[newIndex].Nmu_hat.resetSize(2,1);
    objects[newIndex].Emu_hat.resetSize(2,1);
    objects[newIndex].Nsigma_hat.resetSize(2,2);
    objects[newIndex].Esigma_hat.resetSize(2,2);

    double NMelems[] = {objects[newIndex].N, 0};
    double EMelems[] = {objects[newIndex].E, 0};
    double NSelems[] = {1,0,0,1};
    double ESelems[] = {1,0,0,1};

    objects[newIndex].Nmu.setelems(NMelems);
    objects[newIndex].Nsigma.setelems(NSelems);
    objects[newIndex].Emu.setelems(EMelems);
    objects[newIndex].Esigma.setelems(ESelems);

  } else {

    fprintf(stderr,"trying to add obj, no space in buffer \n");

  } //end checking that there's enough room to add new object
} //end addObject


//TODO: use actual struct/message type that sam defines
//TODO: actually calculate radius of object
void BlobLadar::sendObjects() {


  MapElement el;
  vector <point2> objectPoints;

  double tempN, tempE, tempN2, tempE2, dN, dE, radius;
  
  //  fprintf(stderr, "number of objects: %d \n", usedObjs.size());
  int sent = 0;
  for(unsigned int k=0; k<usedObjs.size(); k++) {
    int j = usedObjs.at(k);
  
    int age = objects[j].timesSeen;
    sent++;

    tempE = objects[j].Emu.getelem(0,0);
    tempN = objects[j].Nmu.getelem(0,0);
    tempE2 = objects[j].E;
    tempN2 = objects[j].N;

    radius = 0.50;
    dN = objects[j].Nmu.getelem(1,0);
    dE = objects[j].Emu.getelem(1,0);

    if(useDisplay) {
      myDisplay->addObject(tempN, tempE, radius, age);
      //      fprintf(stderr, "trying to plot line: %f, %f, %f, %f \n ", tempN, tempE, dN, dE);
      //      fprintf(logfile, "looking for color %d  \n", 5);
      myDisplay->sendLine(tempN, tempN + dN, tempE, tempE + dE, 5);
      // to check diff btwn estimated and measured pos
      //      myDisplay->sendLine(tempN, tempN2, tempE, tempE2, 0);
    }


    //    fprintf(stderr, "trying to create and send map element \n");
    if(useMapTalker) {
      objectPoints.clear();
      vector <NEcoord> lastScan(objects[j].scans.back());
      point2 tmppt;
      vector <int> id;
      //fill in the points
      //      fprintf(stderr, "first point: %f, %f \n", lastScan.at(0).N, lastScan.at(0).E);
      for(unsigned int m=0; m < lastScan.size(); m++) {
	tmppt = point2(lastScan.at(m).N, lastScan.at(m).E);
	objectPoints.push_back(tmppt);
      }
   
      //set the originating module and blob ID numbers
      id.clear();
      id.push_back(moduleMapID);
      id.push_back(objects[j].ID);

      el.clear();
      el.set_poly_obs(id, objectPoints);
      //      fprintf(stderr, "object ID: %d, object num %d \n", objects[j].ID, j);
      int bytesSent = sendMapElement(&el, 0);
      //      fprintf(stderr, "sent map element! bytesSent = %d \n", bytesSent);
    }

  } // end checking each object
  return;
}

void BlobLadar::sendCars()
{

  //  fprintf(stderr, "number of cars: %d \n", usedCars.size());
  int sent = 0;
  double tempE, tempN, dN, dE;
  MapElement el;
  vector <point2> cornerPoints;

  for(unsigned int k=0; k<usedCars.size(); k++) {
    int j = usedCars.at(k);
  
    sent++;

    double corners[8];

    corners[0] = 1.0;
    //TODO: check that I've got the corners properly labeled
    corners[0] = cars[j].N;
    corners[1] = cars[j].E;
    corners[2] = cars[j].N+cars[j].dn1;
    corners[3] = cars[j].E+cars[j].de1;
    corners[4] = cars[j].N+cars[j].dn2;
    corners[5] = cars[j].E+cars[j].de2;
    corners[6] = cars[j].N+cars[j].dn1+cars[j].dn2;
    corners[7] = cars[j].E+cars[j].de1+cars[j].de2;

    tempE = cars[j].Emu.getelem(0,0);
    tempN = cars[j].Nmu.getelem(0,0);

    dN = cars[j].Nmu.getelem(1,0);
    dE = cars[j].Emu.getelem(1,0);

    if(useDisplay) {
      //      myDisplay->addCar(corners);
      //      myDisplay->sendLine(tempN, tempN + dN, tempE, tempE+dE, 5);
    }

    //TODO: special message for car types??
    if(useMapTalker) {
      cornerPoints.clear();
      point2 tmppt;
      vector <int> id;

      //fill in the corners
      tmppt = point2(corners[0], corners[1]);
      cornerPoints.push_back(tmppt);
      tmppt = point2(corners[4], corners[5]);
      cornerPoints.push_back(tmppt);
      tmppt = point2(corners[6], corners[7]);
      cornerPoints.push_back(tmppt);
      tmppt = point2(corners[2], corners[3]);
      cornerPoints.push_back(tmppt);

      //set the originating module and blob ID numbers
      id.clear();
      id.push_back(moduleMapID);
      id.push_back(cars[j].ID);

      el.clear();
      el.set_poly_obs(id, cornerPoints);
      int bytesSent = sendMapElement(&el, 0);

    } // end sending to map
  } // end cycling through each car

  return;
}


//#warning "may want to add more checks before checking every object's motion, every time"
void BlobLadar::checkMotion() {

  for(unsigned int i = 0; i< usedObjs.size(); i++) {
    int index = usedObjs.at(i);

    if(objects[index].timesSeen >= 20) {
  
      double velN, velE, vel;
      velN = objects[index].Nmu.getelem(1,0);
      velE = objects[index].Emu.getelem(1,0);
      vel = sqrt(velN*velN+velE*velE);
      if(vel > MINSPEED) {
	//	fprintf(stderr, "THIS OBJ IS MOVING!!! \n ");
	//	obj2car(i);

      }
    }
  }
} //end checkMotion



//#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
void BlobLadar::obj2car(int index) {
  //  cout<<"trying to turn an object into a car"<<endl;
  if(usedCars.size() < MAXNUMCARS-1) { 
    int i = usedObjs.at(index);
  vector<NEcoord> points(objects[i].scans.back());

  int temp;
  temp = openCars.back();
  openCars.pop_back();
  usedCars.push_back(temp);

 int numPoints = points.size();
 //  cout<<"this has "<<numPoints<<" points..."<<endl;

  double Epts[numPoints];
  double Npts[numPoints];
  for(int i=0; i<numPoints; i++) {
    Epts[i] = points.at(i).E;
    Npts[i] = points.at(i).N;
  }

  int corner;
  myDisplay->addObject(Npts[0],Epts[0],.5,200);
  corner = fitCar(Npts,Epts,numPoints,&cars[temp],objects[i].Nmu.getelem(1,0), objects[i].Emu.getelem(1,0)); 
  //  fprintf(stderr, "fit car! corner: %f, %f \n", cars[temp].N, cars[temp].E);
  cars[temp].Nmu.resetSize(2,1);
  cars[temp].Emu.resetSize(2,1);
  cars[temp].Nsigma.resetSize(2,2);
  cars[temp].Esigma.resetSize(2,2);
  cars[temp].Nmu_hat.resetSize(2,1);
  cars[temp].Emu_hat.resetSize(2,1);
  cars[temp].Nsigma_hat.resetSize(2,2);
  cars[temp].Esigma_hat.resetSize(2,2);

  double NMelems[] = {cars[temp].N, objects[i].Nmu.getelem(1,0)};
  double EMelems[] = {cars[temp].E, objects[i].Emu.getelem(1,0)};
  double NSelems[] = {0,0,0,0};
  double ESelems[] = {0,0,0,0};

  //	  cout<<"setting matrix mu-naught to: "<<cars[temp].N<<" and "<<cars[temp].E<<endl;

  cars[temp].Nmu.setelems(NMelems);
  cars[temp].Nsigma.setelems(NSelems);
  cars[temp].Emu.setelems(EMelems);
  cars[temp].Esigma.setelems(ESelems);

  cars[temp].ID = numCars;
  
  numCars++; //we've found a car

  removeObject(index);

  } else {
    cerr<<"not enough space in cars array!!"<<endl;
  }
  //  cout<<"exiting obj2car()"<<endl;
}

/**
 * function: fitCar
 * input: takes in set of NE points that have been grouped together as an object,
 *   and finds the two lines that fit them best (think rectangle)
 * output: parameters describing this rectangle
 **/

//#warning "should add idea of uncertainty to position of car!"
int BlobLadar::fitCar(double* Npoints, double* Epoints, int numPoints, carRepresentation* newCar, double vN, double vE) {
  int corner = 0;
  //  cout<<"entering fitCar with "<<numPoints<<" points"<<endl;
  fprintf(stderr, "entering fitCar w/ endpoints: %f, %f, %f, %f \n", Npoints[0], Epoints[0], Npoints[numPoints-1], Epoints[numPoints-1]);
  double minerror, MSE1, MSE2;
  int breakpoint;
  //calculating w/out corner, or new best fit
  lineSegmentFit seg1;
  lineSegmentFit seg2;
  lineSegmentFit tempSeg1;
  lineSegmentFit tempSeg2;

  //returns the fit in temp 1 w/ format {a, b, n1, e1, n2, e2}
  minerror = fitLine(Npoints, Epoints, numPoints, &seg1);

  //now, temp1 and temp2 are the line segments, if no corner is found
  //need to see if there's a better fit, w/ a corner

  //now, find min error, but requiring each leg to have at 
  //least 2 points, and each leg contains the breakpoint
  //TODO: need to try each breakpoint twice, giving 
  //  each seg fair shot at first slope
  for(int i=2; i<numPoints-2; i++) {
    double Npoints1[i];
    double Epoints1[i];
    double Npoints2[numPoints-i+1];
    double Epoints2[numPoints-i+1];

    for(int j=0; j<i; j++) {
      Npoints1[j] = Npoints[j];
      Epoints1[j] = Epoints[j];
    }
    for(int j=0; j<(numPoints-i+1); j++) {
      Npoints2[j] = Npoints[j+2*(i-1)];
      Epoints2[j] = Epoints[j+2*(i-1)];
    }
    //FIXME: this isn't really mse...is this best way?
    MSE1 = fitLine(Npoints1, Epoints1, i, &tempSeg1);
    MSE2 = fitLine2(Npoints2, Epoints2, numPoints-i+1, &tempSeg2, tempSeg1.b);

    //    cout<<"breaking at point "<<i<<" w/ minerror, MSE1, MSE2: "<<minerror<<' '<<MSE1<<' '<<MSE2<<endl;

    //if we've reached a new minimum error, this is our 
    //current best-fit, and
    //should be copied to temp1 & temp2
    //note that this isn't really mean squared error...
    //  it's MSE*numPoints
    /**
   if(minerror > MSE1 + MSE2)
   {
      minerror = MSE1 + MSE2;
      breakpoint = i;
      memcpy(temp1, temp3, 6*sizeof(double));
      memcpy(temp2, temp4, 6*sizeof(double));
      corner = 1;
    }
    **/
  }

  //FIXME: is this really necessary?? 

  //if we didn't see a corner:
  if(corner == 0)
  {

    //want to get points relative to Alice's position
    double relE, relN;
    double n, e; //new n,e pts 
    double thA; //angle from alice to point
    double thC; //angle from start to end of car
    double th;
    relN = seg1.n1 - currState.N;
    relE = seg1.e1 - currState.E;
    fprintf(stderr, "relN, relE: %f, %f: \n", relN, relE);
    thA = atan2(relN, relE);
    thC = atan2((seg1.n1 - seg1.n2), \
		(seg1.e1 - seg1.e2));
    th = (PI/2) + thC;
    fprintf(stderr, "thA, thC, th: %f, %f, %f \n", thA, thC, th);
    e = seg1.e1 + MINDIMENSION * cos(th);
    n = seg1.n1 + MINDIMENSION * sin(th);

    seg2.a = 0;
    seg2.b = 0;
    seg2.e1 = seg1.e1;
    seg2.n1 = seg1.n1;
    seg2.e2 = e;
    seg2.n2 = n;
  }


  //now, need to get these two segments into the format of a car
  //  cout<<"tryign to orient deltas properly...vels are: "<<vN<<' '<<vE<<endl;
  //calculating their intersection (to get the N,E points we define car as)
  double ni, ei; //northing and easting intersections
  double dn1, de1, dn2, de2; //deltas for segs
  //  fprintf(stderr, "seg1: %f, %f, %f, %f, %f, %f \n",seg1.a, seg1.b, seg1.n1, seg1.e1, seg1.n2, seg1.e2);
  //  fprintf(stderr, "seg2: %f, %f, %f, %f, %f, %f \n",seg2.a, seg2.b, seg2.n1, seg2.e1, seg2.n2, seg2.e2);
  //  fprintf(stderr, "corner = %d \n", corner);
  if(corner == 0 )
  { //corner is start of seg
    fprintf(stderr, "corner is at start of seg \n");
    ni = seg1.n1;
    ei = seg1.e1;
  } else { //corner is intersect of the lines
    ei = (seg1.a - seg2.a)/(seg1.b - seg2.b);
    ni = seg1.a + ei*seg1.b;
  }

  fprintf(stderr, "ni, ei, d1, d2: %f, %f, %f, %f, %f, %f \n", ni, ei, dn1, de1, dn2, de2);
//TODO: check this for seg1
  dn1 = seg1.n2 - ni; //seg 1 deltas
  de1 = seg1.e2 - ei;
  dn2 = seg2.n2 - ni; //seg 2 deltas
  de2 = seg2.e2 - ei;
  //  cout<<"initial corner pos and deltas: "<<ni<<' '<<ei<<' '<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
  //  cout<<"input velocity: "<<vN<<' '<<vE<<endl;
  newCar->N = ni; //corner coords
  newCar->E = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;

  /**

  //now, making sure the corner is in the right position (using vE and vN) see p.85 - 87
  double thv, th1, th2; //velocity and segment angles
  double tn, te; //temp holders for switching values
  thv = atan2(vN,vE);
  th1 = atan2(dn1, de1);
  th2 = atan2(dn2, de2);
  //  cout<<"angles (vel, 1, 2): "<<thv<<' '<<th1<<' '<<th2<<endl;

  //putting ni,ei at back bumper
  if( sin(thv-th1) < sin(thv-th2)) { //(dn1, de1) is parallel direction of velocity
    if(cos(thv-th1) > 0) { //they're oriented the same
      //      cout<<"//all is good...(n,e) is at back, (dn1, de1) is pointed along velocity"<<endl;
    } else {
      //      cout<<" //oriented opposite, so need to flip d1"<<endl;
      ni = ni + dn1;
      ei = ei + de1;
      dn1 = -dn1;
      de1 = -de1;
    }
  } else {
    //    cout<<"//(dn2, de2) is parallel to velocity...need to switch d1 and d2"<<endl;
    tn = dn1;
    te = de1;
    if(cos(thv-th2) > 0) { //same orientation
      //      cout<<"correct d2 orientation"<<endl;
      // ni,ei are fine, only need to switch 
      dn1 = dn2;
      de1 = de2;
      dn2 = tn;
      de2 = te;
    } else { //opp orientation
      //      cout<<"need to switch d2 orientation"<<endl;
      ni = ni+dn2;
      ei = ei+dn2;
      dn1 = -dn2;
      de1 = -de2;
      dn2 = tn;
      de2 = te;
    }
  }
  **/
  /**
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;
  **/
  /**
  //putting ni,ei at RIGHT side of back bumper (i.e. th2 and thv+PI/2 are in same dir)
  th2 = atan2(dn2, de2); //need to recalculate...
  if(cos(thv + PI/2 - th2) > 0) { //they're in same direction
    //everything is set up properly
  } else { //need to switch
    ni = ni+dn2;
    ei = ei+de2;
    dn2 = -dn2;
    de2 = -de2;
  }
  **/
  //WHEW. car is set up correctly
  //  cout<<"fit a car, w/ params: "<<ni<<' '<<ei<<' '<<dn1<<' '<<de1<<' '<<dn2<<' '<<de2<<endl;
  /**
  newCar->n = ni; //corner coords
  newCar->e = ei;
  newCar->dn1 = dn1; //seg 1 deltas
  newCar->de1 = de1;
  newCar->dn2 = dn2; //seg 2 deltas
  newCar->de2 = de2;
  **/


  //debugging - plotting the points, the lines, etc:
  double zpts[numPoints];
  for(int i=0; i< numPoints; i++)
  {
    zpts[i] = 0;
  }
  fprintf(stderr, "first point: %f, %f \n", Npoints[0], Epoints[0]);
  fprintf(stderr, "corner: %f, %f, %f, %f, %f, %f\n", ni, ei, dn1, de1, dn2, de2);
  //  fitDisplay->sendScan(numPoints, Npoints, Epoints, zpts);
  //  fitDisplay->sendLine(seg1.n1,seg1.n2, seg1.e1, seg1.e2);
  myDisplay->sendLine(ni, ni+dn1, ei, ei+de1, 0);
  myDisplay->sendLine(ni, ni+dn2, ei, ei+de2, 4);


  return corner;
} //end fitCar

//removes object by putting its index back on openObjs, and removing 
//the index from usedObjs
//TODO: send message to map saying this obj has been removed
void BlobLadar::removeObject(int index) {
  openObjs.push_back(usedObjs.at(index));
  objIterator = usedObjs.begin();
  objIterator+=index;
  usedObjs.erase(objIterator);

}
//removes car by putting it's index on openCars
void BlobLadar::removeCar(int index) {
  openCars.push_back(usedCars.at(index));
  carIterator = usedCars.begin();
  carIterator+=index;
  usedCars.erase(carIterator);

}

//FIXME: this gives bad results when line has infinite slope
double BlobLadar::fitLine(double* Npoints, double* Epoints, int numPoints, lineSegmentFit* fit) {

  //  cout<<"entering fitLine"<<endl;

  double a,b,MSE; //final vars for least-squares fit
  //fit gives 2 solns, will need to test them
  double a1, a2, b1, b2; 
  //intermediate value
  double B;


  //compute mean e and n coordinates
  double e, n;
  double nerr;
  double ebar, nbar;

  double ssee = 0;
  double ssnn = 0;
  double ssen = 0;

  double sume = 0;
  double sumn = 0;


  for(int i=0;i<numPoints;i++) {
    e = Epoints[i];
    n = Npoints[i];

    sume += e;
    sumn += n;

    ssee += e*e;
    ssnn += n*n;
    ssen += e*n;

  }

  ebar = sume/numPoints; 
  nbar = sumn/numPoints;

  B = .5*((ssnn - numPoints*nbar*nbar)     \
	  -(ssee - numPoints*ebar*ebar))  \
    /(numPoints*ebar*nbar - ssen);

  b1 = -B + sqrt(1+B*B);
  a1 = nbar - b1*ebar;

  b2 = -B - sqrt(1+B*B);
  a2 = nbar - b2*ebar;

  //  fitDisplay->sendLine(-4, 4, a1-b1*4, a1+b1*4,5);
  //  fitDisplay->sendLine(-8, 8, a2-b2*8, a2+b2*8,5);

  //now, need to have some measure of the error
  double SE1 = 0;
  double SE2 = 0;
  double err;
  for(int i=0; i<numPoints;i++) 
  {
    e = Epoints[i];
    n = Npoints[i];

    nerr = n - a1 - b1*e;
    err = nerr / sqrt(1+b1*b1);
    SE1 += err*err;

    nerr = n - a2 - b2*e;
    err = nerr / sqrt(1+b2*b2);
    SE2 += err*err;
  }

  if(SE1 < SE2)
  {
    a = a1;
    b = b1;
    MSE = SE1/numPoints;
  } else {
    a = a2;
    b = b2;
    MSE = SE2/numPoints;
  }

  //projecting ladar returns onto the line
  double dn1, dn2, de1, de2;
  double  newn1, newn2, newe1, newe2;
  double hyp, th, e1, n1, e2, n2;
  e1 = Epoints[0];
  n1 = Npoints[0];
  de1 = e1 - (n1 - a)/b;
  dn1 = n1 - (a +  b*e1);
  //  fprintf(stderr, "n1, e1, dn1, de1: %f, %f, %f, %f \n", n1, e1, dn1, de1); 
  th = atan2(dn1, de1);
  hyp = de1*sin(th);
  //  fprintf(stderr, "th, and hyp: %f, %f \n", th, hyp);
  newn1 = n1 + hyp*cos(th);
  newe1 = e1 + hyp*sin(th);

  e2 = Epoints[numPoints-1];
  n2 = Npoints[numPoints-1];
  de2 = e2 - (n2 - a)/b;
  dn2 = a +  b*e2 - n2;
  //  fprintf(stderr, "n2, e2, dn2, de2: %f, %f, %f, %f \n", n2, e2, dn2, de2); 
  th = atan2(dn2, de2);
  hyp = de2*sin(th);
  //  fprintf(stderr, "th, and hyp: %f, %f \n", th, hyp);
  newn2 = n2 + hyp*cos(th);
  newe2 = e2 + hyp*sin(th);

  fit->a = a;
  fit->b = b;
  fit->e1 = newe1;
  fit->n1 = newn1;
  fit->e2 = newe2;
  fit->n2 = newn2;

  return numPoints*MSE;
}


/** 
 *Function: uses the method of least-squares to fit a line of the form b = a + b * e
 * to the input points, given that it must be perpendicular to line w/ slope given
 * Then, transforms that to a line of form [alpha, rho, r1, r2]
 * uses eqns found on mathworld.wolfram.com
 * returns: the r-squared value of the fit, and changes the data in fit
 **/

double BlobLadar::fitLine2(double* Npoints, double* Epoints, int numPoints, lineSegmentFit* fit, double slope) {

  double a,b, MSE; //vars for least-squares fit

  //compute mean e and y coordinates
  double eerr, nerr, err;
  double e, n;

  double ebar = 0;
  double nbar = 0;
  for(int i=0;i<numPoints;i++) {
    e = Epoints[i];
    n = Npoints[i];

    ebar += e;
    nbar += n;
  }

  ebar = ebar/numPoints; 
  nbar = nbar/numPoints;

  //b must be perpendicular to line described by theta (i.e. parallel to theta)
  b = -1/slope; 
  a = nbar - b*ebar;

  //now, need to have some measure of the error
  double SE = 0;
  for(int i=0; i<numPoints;i++) {
    e = Epoints[i];
    n = Npoints[i];
    nerr = n - a - b*e;
    eerr = e - (n-a)/b;
    err = sqrt(nerr*nerr + eerr*eerr);
    SE += err*err;
  }


  //projecting ladar returns onto the line
  double dn1, dn2, de1, de2;
  double  newn1, newn2, newe1, newe2;
  double hyp, th, e1, n1, e2, n2;
  e1 = Epoints[0];
  n1 = Npoints[0];
  de1 = e1 - (n1 - a)/b;
  dn1 = a +  b*e1 - n1;
  th = atan2(dn1, de1);
  hyp = de1*cos(th);
  //  fprintf(stderr, "two calcs of hyp: %f, %f \n", de1*cos(th), dn1*sin(th));
  newn1 = n1 + hyp*sin(th);
  newe1 = e1 + hyp*cos(th);

  e2 = Epoints[numPoints-1];
  n2 = Npoints[numPoints-1];
  de2 = e2 - (n2 - a)/b;
  dn2 = a +  b*e2 - n2;
  th = atan2(dn2, de2);
  hyp = de2*cos(th);
  //  fprintf(stderr, "two calcs of hyp: %f, %f \n", de2*cos(th), dn2*sin(th));
  newn2 = n2 + hyp*sin(th);
  newe2 = e2 + hyp*cos(th);

  fit->a = a;
  fit->b = b;
  fit->e1 = newe1;
  fit->n1 = newn1;
  fit->e2 = newe2;
  fit->n2 = newn2;

  MSE = SE/numPoints;
  return SE;
}


void BlobLadar::KFupdate() {

  //  cout<<"entering KFupdate"<<endl;

  Matrix K1;
  K1 = Matrix(2,2);
  Matrix K2;
  K2 = Matrix(2,2);
  Matrix Nerr;
  Nerr = Matrix(1,1);
  Matrix Eerr;
  Eerr = Matrix(1,1);
  Matrix tmp;
  tmp = Matrix(1,1);
  Matrix tmp2;
  tmp2 = Matrix(1,1);
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int j = usedObjs.at(i);
    //    cout<<"last seen: "<<objects[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(objects[j].timesSeen > 1) {
      if(objects[j].lastSeen == numFrames) {
        double grr[] = {objects[j].N};
        tmp.setelems(grr);
	//	cout<<"Northing: "<<objects[j].N<<" and, predicted northing: "<<objects[j].Nmu_hat.getelem(0,0)<<endl;
        Nerr = tmp - C*objects[j].Nmu_hat;
	//	cout<<"Nerror: "<<Nerr.getelem(0,0)<<' ';
        K1 = objects[j].Nsigma_hat * C.transpose() * (C*objects[j].Nsigma_hat*C.transpose() + Q).inverse();
        objects[j].Nmu = objects[j].Nmu_hat + K1 * Nerr;
        objects[j].Nsigma = (I - K1*C) * objects[j].Nsigma_hat;

        double grr2[] = {objects[j].E};
        tmp2.setelems(grr2);

        Eerr = tmp2 - C*objects[j].Emu_hat;
	//	cout<<" and, Eerror: "<<Eerr.getelem(0,0)<<endl;
        K2 = objects[j].Esigma_hat * C.transpose() * (C*objects[j].Esigma_hat*C.transpose() + Q).inverse();
        objects[j].Emu = objects[j].Emu_hat + K2 * Eerr;
        objects[j].Esigma = (I - K2*C) * objects[j].Esigma_hat;
	//	cout<<"new vel (actually calculated) "<<objects[j].Nmu.getelem(1,0)<<' '<<objects[j].Emu.getelem(1,0)<<endl;

      } else {

	objects[j].Nmu = objects[j].Nmu_hat;
	objects[j].Nsigma = objects[j].Nsigma_hat;
	objects[j].Emu = objects[j].Emu_hat;
	objects[j].Esigma = objects[j].Esigma_hat;

      }

    }
  }


  for(unsigned int i=0; i<usedCars.size(); i++) {
    //    cout<<"entering loop w/ i = "<<i<<endl;
    int j = usedCars.at(i);
    //    cout<<"last seen: "<<cars[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(cars[j].timesSeen > 1) {
      if(cars[j].lastSeen == numFrames) {

	double grr[] = {cars[j].N};
	tmp.setelems(grr);

	//	cout<<"line 2239"<<endl;
        Nerr = tmp - C*cars[j].Nmu_hat;
	//	cout<<"Nerr for car: "<<Nerr.getelem(0,0)<<endl;
        K1 = cars[j].Nsigma_hat * C.transpose() * (C*cars[j].Nsigma_hat*C.transpose() + Q).inverse();
	//	cout<<"line 2242"<<endl;
        cars[j].Nmu = cars[j].Nmu_hat + K1 * Nerr;
	//	cout<<"new n,ndot: "<<cars[j].Nmu.getelem(0,0)<<' '<<cars[j].Nmu.getelem(1,0)<<endl;
        cars[j].Nsigma = (I - K1*C) * cars[j].Nsigma_hat;

	//	cout<<"ln 2246"<<endl;
	double grr2[] = {cars[j].E};
	tmp2.setelems(grr2);

	//	cout<<"ln 2250"<<endl;
        Eerr = tmp2 - C*cars[j].Emu_hat;
        K2 = cars[j].Esigma_hat * C.transpose() * (C*cars[j].Esigma_hat*C.transpose() + Q).inverse();
	//	cout<<"ln 2253"<<endl;
        cars[j].Emu = cars[j].Emu_hat + K2 * Eerr;
        cars[j].Esigma = (I - K2*C) * cars[j].Esigma_hat;

      } else {
	//	cout<<"ln 2258"<<endl;
	cars[j].Nmu = cars[j].Nmu_hat;
	cars[j].Nsigma = cars[j].Nsigma_hat;
	//	cout<<"ln 2261"<<endl;
	cars[j].Emu = cars[j].Emu_hat;
	//	cout<<"ln 2263"<<endl;
	cars[j].Esigma = cars[j].Esigma_hat;
	//	cout<<"ln 2264"<<endl;
      }
    }
  }

  //  cout<<"exiting KFupdate"<<endl;
}
 
void BlobLadar::KFpredict() {

  //  cout<<"entering KFpredict()"<<endl;

  for(unsigned int i=0; i<usedObjs.size() ; i++) {
    int j = usedObjs.at(i);

    objects[j].Nmu_hat =    A*objects[j].Nmu;
    objects[j].Nsigma_hat = A*objects[j].Nsigma*A.transpose() + R;
    objects[j].Emu_hat =    A*objects[j].Emu;
    objects[j].Esigma_hat = A*objects[j].Esigma*A.transpose() + R;

  } //end cycling through numObjects


  //and, objcars

 for(unsigned int i=0; i<usedCars.size(); i++) {
    int j = usedCars.at(i);

    //    cout<<"ln 2300"<<endl;
    cars[j].Nmu_hat =    A*cars[j].Nmu;
    cars[j].Nsigma_hat = A*cars[j].Nsigma*A.transpose() + R;
    //    cout<<"ln2303"<<endl;
    cars[j].Emu_hat =    A*cars[j].Emu;
    cars[j].Esigma_hat = A*cars[j].Esigma*A.transpose() + R;
 } //end cycling through numCars

} //end KFpredict()


void BlobLadar::cleanObjects()
{
  int j; 
  int diff;
  for(unsigned int i=0; i<usedObjs.size(); i++)
  {
    j = usedObjs.at(i);
    diff = numFrames - objects[j].lastSeen;
    //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
    if(diff > OBJECTTIMEOUT) {
      fprintf(stderr, "tryng to remove object # %d! \n", j);
      removeObject(i);
    }
  }

}

//TODO: add a free-space condition for removing cars
void BlobLadar::cleanCars()
{
  int j; 
  int diff;
  for(unsigned int i=0; i<usedCars.size(); i++)
  {
    j = usedCars.at(i);
    diff = numFrames - cars[j].lastSeen;
    //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
    if(diff > CARTIMEOUT) {
      fprintf(stderr, "tryng to remove car # %d! \n", j);
      removeCar(i);
    }
  }

}


/**
 * this function is called when it is determined that a new object
 * matches an already seen car.
 **/
//#warning "FIXME: this update is very incomplete -- only replaces old data, doesn't merge at all"
//#warning "At this point, simply replaces Car w/ car formed by Points...add in sam's update rules"
void BlobLadar::updateCar(carRepresentation* Car, objectRepresentation* newObj) {

  //  cout<<"entering updateCar"<<endl;
  vector<NEcoord> points(newObj->scans.back());
  double dStart = newObj->dStart;
  double dEnd = newObj->dEnd;

  int numPoints = points.size();

  double npts[numPoints];
  double epts[numPoints];
  for(int i=0; i<numPoints; i++) {
    epts[i] = points.at(i).E;
    npts[i] = points.at(i).N;
  }
  //  cout<<"points in object: "<<pts[0]<<' '<<pts[1]<<' '<<pts[2*numPoints-2]<<' '<<pts[2*numPoints-1]<<endl;
  //  cout<<"center of object: "<<newObj->N<<' '<<newObj->E<<endl;
  carRepresentation newCar;
  //  cout<<"updating car #"<<Car->ID<<" at "<<Car->n<<' '<<Car->e<<endl;
  fitCar(npts, epts, numPoints,&newCar, Car->Nmu.getelem(1,0), Car->Emu.getelem(1,0)); 

  //  cout<<"Params for old car: "<<Car->n<<' '<<Car->de1<<' '<<Car->dn1<<' '<<Car->e<<' '<<Car->dn2<<' '<<Car->de2<<endl;
  //  cout<<"Params for new car: "<<newCar.N<<' '<<newCar.de1<<' '<<newCar.dn1<<' '<<newCar.e<<' '<<newCar.dn2<<' '<<newCar.de2<<endl;

  /**
   * depending on how much of the car we see in the old measurement, update
   * the old car (if we see the corner, that carries more information than 
   * seeing only part of a segment. see p. 86-87
   * we determine how much of car we saw by occlusions
   **/
  int sawFront = 0; //whether dStart&dEnd say we actually saw the boundary of car
  int sawBack = 0;

  //need angles to front/back of car (rel to alice), to determine how they 
  //correspond to dStart&dEnd. will be in range of 0-PI (how ladar works)
  double th1, th2;
  //#warning "for here (and elsewhere) will need to add yaw when using real data"
  double foo1, foo2, bar1, bar2;
  foo1 = Car->N - currState.N;
  foo2 = Car->E - currState.E;
  bar1 = Car->N + Car->dn1 - currState.N;
  bar2 = Car->E + Car->de1 - currState.E;
  //  cout<<"deltas: "<<foo1<<' '<<foo2<<' '<<bar1<<' '<<bar2<<endl;
  th1 = atan2(Car->N - currState.N, Car->E - currState.E);
  th2 = atan2(Car->N + Car->dn1 - currState.N, Car->E + Car->de1 - currState.E);
  //  th1 = atan2(Car->e - currState[1], Car->n - currState[0]);
  //  th2 = atan2(Car->e + Car->de1 - currState[1], Car->n + Car->dn1 - currState[0]);

  //  cout<<"thetas: "<<th1<<' '<<th2<<endl;
  if(th1 < th2) { // n,e (back bumper) correspond to start, and d1 (front bumper) correspond to end
    //  if(1==1) { // for now, we're guaranteed that back == start
    if(dStart >= 0) { //start was in foreground
      sawBack = 1;
    }
    if(dEnd >= 0) { //end was in foreground
      sawFront = 1;
    }
  } else { // back bumper corresponds to end
    if(dStart >= 0) {
      sawFront = 1;
    }
    if(dEnd >= 0) {
      sawBack = 1;
    }
  }

  //now that we know whether to trust start/end coords, can calculate updates
  if(sawBack == 1) {
    if(sawFront == 1) {
//      cout<<" //have full information on this car, can entirely replace"<<endl;
      //#warning "replacing isn't the best option here...better to update d's to take into account ALL information"
    double dn, de;
    dn = newCar.N + newCar.dn1 - Car->N - Car->dn1;
    de = newCar.E + newCar.de1 - Car->E - Car->de1;
    Car->N = Car->N + dn;
    Car->E = Car->E + de;
    //#warning "for some reason, we think we've seen the back when we havent...thus, only using deltas here"
    //      Car->n = newCar.n;
    //      Car->e = newCar.e;
      //      Car->dn1 = newCar.dn1;
      //      Car->de1 = newCar.de1;
      //      Car->dn2 = newCar.dn2;
      //      Car->de2 = newCar.de2;
    } else {
      //    cout<<" //only know back bumper, only update position"<<endl;
      Car->N = newCar.N;
      Car->E = newCar.E;
    }
  } else if (sawFront == 1) {
    //    cout<<"//only know front bumper, only update deltas"<<endl;
    double dn, de;
    dn = newCar.N + newCar.dn1 - Car->N - Car->dn1;
    de = newCar.E + newCar.de1 - Car->E - Car->de1;
    Car->N = Car->N + dn;
    Car->E = Car->E + de;
  }

  //if we were able to update car, update history/log of when seen
  if(sawFront + sawBack > 0) {
    //setting up history vector...
    int histptr = Car->histPointer;

    //  cout<<"setting ptr to "<<histptr<<" plus 1"<<endl;
    Car->histPointer = histptr + 1;
    Car->lastE[histptr%10] = Car->E;
    Car->lastN[histptr%10] = Car->N;

    Car->timesSeen = Car->timesSeen + 1;
    Car->lastSeen = numFrames;
  } else {
    //    cout<<"tried to update car, not enough information"<<endl<<endl;
  }
} //end updateCar

