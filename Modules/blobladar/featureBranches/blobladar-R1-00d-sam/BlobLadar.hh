using namespace std;
#include "DebugDisplay.hh"
#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>
#include <frames/pose3.h>
#include <frames/coords.hh>
#include "Matrix.hh"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

#include "dgcutils/DGCutils.hh" 

//openGL support
#include <GL/glut.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include "skynettalker/MapElementTalker.hh"
#include "interfaces/MapElement.hh"
#include "interfaces/MapElementMsg.h"

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

//openCV graphics
//#include <cv.h>
#include "cv.h"
#include "highgui.h"
//#include <highgui.h>

#define PI 3.14159265
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define MAXTRAVEL 1.5 //max dist obj can travel btwn frames
#define MAXNUMCARS 50 //max num cars we can track
#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define NUMSCANPOINTS 181
#define SCALINGFACTOR 4 //used in determining 
           //discontinuities for segmentation
#define CARMARGIN 1.0 //if it's this close to a car, classify it as part

#define MINSPEED 4   //min velocity to bother sending out
#define MAXSPEED 20.0 //max velocity 
#define MINDIMENSION 2 //we're assuming this is the minimum car dimension 
                       //used when we can only see 1 side...
#define OBJECTTIMEOUT 100 //how many loops before we delete an object



/**
 * This struct is how we store all the information about a car that we ever use
**/
struct carRepresentation{

  //unique ID # for this car (used by mapping...)
  int ID;

  //for storing the car's corner point, and the endpoints of segments
  double e,de1,de2;
  double n,dn1,dn2;
  double len1, len2;

  //for storing the Car's history (mapper & prediction may want this)
  double lastE[10];
  double lastN[10];
  int histPointer; //this (mod 10) is the spot we're at in the history

  //the last scan we saw the car at, and how many times
  int lastSeen;
  int timesSeen;

  //points at which the segment starts and ends...useful for debugging, and
  //necessary for updateCar (at least for now...once we no longer use
  //trackCars for initial association, we can get rid of it, i think)
  int segStart, segEnd; 
  double dStart, dEnd;

  //whether we've actually seen front/back of car (useful for updating cars)
  int seenFront, seenBack;

  //whether we actually fit a corner to the data, or added one for our purposes
  int cornerSeen;

  //matrices for KF estimates...

  Matrix Nmu;
  Matrix Nmu_hat;
  Matrix Nsigma;
  Matrix Nsigma_hat;

  Matrix Emu;
  Matrix Emu_hat;
  Matrix Esigma;
  Matrix Esigma_hat;

};



/**
 * If we're tracking objects rather than cars, this struct is how we keep
 * track of all relevant information
 **/
struct objectRepresentation {
  //the center of mass of this object

  double N,E;
  int ID;

  //histories of the points, ranges and angles for this car
  vector<vector<NEcoord> > scans;
  vector<vector<double> > ranges;
  vector<vector<double> > angles;

  //how many times we've seen it, and when the last one was
  int timesSeen;
  int lastSeen;

  //start and end distances (+ means car is in foreground, 
  //0 means at edge of scan, - means car is in background)
  double dStart, dEnd; 

  //matrices for KF estimates...

  Matrix Nmu;
  Matrix Nmu_hat;
  Matrix Nsigma;
  Matrix Nsigma_hat;

  Matrix Emu;
  Matrix Emu_hat;
  Matrix Esigma;
  Matrix Esigma_hat;


};

struct objectMessage
{
  int ID;

  NEcoord center;
  double radius;


};

// Line perceptor module
class BlobLadar : public CMapElementTalker
{
  public:

  // Constructor
  BlobLadar();

  // Destructor
  ~BlobLadar();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);



  public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  public:

  // Update the map
  int update();

  public:

  // Initialize console display
  int initConsole();

  //int mainOpenGL(int argc, char** argv);

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, BlobLadar *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, BlobLadar *self, const char *token);

  //tracking functions
  void foo();
  void trackerInit();
  void processScan();
  void segmentScan();
  void prepObjects();
  double dist(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
  }
  void updateObject(objectRepresentation* oldObj, objectRepresentation* newObj);
  void addObject(objectRepresentation* newObj);
  void classifyObjects();
  void checkMotion();
  void sendObjects();
  //removes any objects that haven't been seen recently
  void cleanObjects();


  /**
   * This function takes the points stored in the vector points, and uses
   * them to update the representation Car. 
   * Currently, this update is VERY incomplete, simply replacing the car
   * representation stored in Car with the one that comes from processing points.
   *         
   * At some point (soon) need to fix this, to deal with only partial matches,
   * and to actually update the data, rather than replace it 
   * (see Sam's thesis)
   **/
  void updateCar(carRepresentation* Car, objectRepresentation* newObj);


  /**
   * we have an obj and checkMotion says that its moving...
   * so we need to turn it into a car
   **/
  void obj2car(int index); 

  /**
   * takes in a list of points, and fits a rectangle to them
   * fit will be an array, consisting of the fit parameters
   * [alpha, rho, a,b, alpha2, rho2, a2, b2] 
   **/
  //#warning "this should take in vector<NEcoord>, so conversions don't have to happen before"
  int fitCar(double* points, int numPoints, carRepresentation* fit, double vN, double vE);

  //removes object at index
  void removeObject(int index);
  /**
   * fits a line (alpha,rho,a,b) to an array of points
   * for now, tries to minimise the total squared error
   **/
  double fitLine(double* points, int numPoints, double* fit);

  /**
   * fits a line (alpha, rho, a, b) to given array of points, given 
   * that it must be perpendicular to the line described by theta1
   **/
  //#warning: "do I want to allow a range around theta1 at some point??"
  double fitLine2(double* points, int numPoints, double* fit, double theta1);


  /**
   * uses the most recent measurements (if they exist) to update the KF
   * */
  void KFupdate();

  /**
   * calculates mu_hat and sigma_hat, to be used in matching objects w/ 
   * new scans.
   **/
  void KFpredict();


  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  FILE * logfile;

  // Sensnet module
  sensnet_t *sensnet;
  LindzeyDisplay *myDisplay;
  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  int useDisplay;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //how many times we've looped in the main program
  int loopCount;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];

  /************ defines from trackMO ***************/


  double rawData[NUMSCANPOINTS][2];
  double diffs[NUMSCANPOINTS-1];
  double utmData[NUMSCANPOINTS+1][3];
  double currState[3];
  objectRepresentation candidates[MAXNUMCARS];
  objectRepresentation objects[MAXNUMCARS];

  //  Matrix A = new Matrix(2,2);
  Matrix A;//(2,2);
  Matrix C;//(2,1);
  Matrix I;//(2,2);
  Matrix R;//(2,2);
  Matrix Q;//(1,1);

  //variables for the maptalker stuff
  int useMapTalker; //FIXME: this should be a cmd line option to turn off
  int subgroup; //FIXME: check w/ sam what this value should be
  int moduleMapID;

  //first and last index of each object, as found 
  //by segmentScan()
  int posSegments[NUMSCANPOINTS][2]; 
  //number of pts in ith object, again as found 
  //by segmentScan()
  int sizeSegments[NUMSCANPOINTS]; 
  int numCandidates; //number of objects okayed by prepobjects
  int numSegments; //number of objects found by segmentScan
  int numFrames; //how many frames we've processed
  int numObjects;
   //the stack to keep track of objects
  vector<int> openObjs; 
  vector<int> usedObjs;
  vector<int>::iterator objIterator;


  int numCars; //how many objects we've turned into cars; used to index the cars
  //the stack to keep track of cars
  vector<int> openCars;
  vector<int> usedCars;
  vector<int>::iterator carIterator;
  carRepresentation cars[MAXNUMCARS]; //Trying to define an array of car structs


  //stuff for display

};

//TODO: shouldn't use defines here (static const?)
#define MAXNUMOBJS 50





// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


