#include "DebugDisplay.hh"

  pthread_mutex_t mutex1;// = PTHREAD_MUTEX_INITIALIZER;
int flag = 0;
vector<ladar_point> ladar_points;
vector<ladar_object> ladar_objects;
  vector<ladar_line> ladar_lines;


//int mess_recvd = 0;

//OpenGL variables
float xrot = 0, yrot = 0, angle=0.0;
float  lastx,  lasty;
bool  lbuttondown = false;
float  xpos = 0,  ypos = 0,  zpos = 0;

double colorLookup[6][3];

const int GRIDLINES = 0;


void display(void);
void keyboard(unsigned char key, int x, int y);
void resize(int w, int h);
void idle(void);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void motionPassive(int x, int y);


LindzeyDisplay::LindzeyDisplay(int *argc, char **argv)
{
  glEnable(GL_DEPTH_TEST);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);

  //FIXME: these should be looked up from appropriate files
  //lf bumper is ladar 3
  ladarPos[3][0] = 4.426;
  ladarPos[3][1] = 1.144;
  ladarPos[3][2] = -.822;
  //mf bumper is ladar 4 (??)

  //rf bumper is ladar 5
  ladarPos[5][0] = 4.426;
  ladarPos[5][1] = -1.144;
  ladarPos[5][2] = -.822;

  //initializing color lookup
  //red
  colorLookup[0][0] = 1.0f;
  colorLookup[0][1] = 0.0f;
  colorLookup[0][2] = 0.0f;
  //orange
  colorLookup[1][0] = 1.0f;
  colorLookup[1][1] = 0.5f;
  colorLookup[1][2] = 0.0f;
  //yellow
  colorLookup[2][0] = 1.0f;
  colorLookup[2][1] = 1.0f;
  colorLookup[2][2] = 0.0f;
  //green
  colorLookup[3][0] = 0.0f;
  colorLookup[3][1] = 1.0f;
  colorLookup[3][2] = 0.0f;
  //blue
  colorLookup[4][0] = 0.0f;
  colorLookup[4][1] = 0.0f;
  colorLookup[4][2] = 1.0f; 
  //black
  colorLookup[5][0] = 1.0f;
  colorLookup[5][1] = 1.0f;
  colorLookup[5][2] = 1.0f;

  arg1 = argc;
  arg2 = argv;
  //  state.N = 0;
  //  state.E = 0;
  //  mutex1 = PTHREAD_MUTEX_INITIALIZER;

}

LindzeyDisplay::~LindzeyDisplay()
{
  return;
}

void LindzeyDisplay::clearObjects()
{
  ladar_objects.clear();
  ladar_lines.clear();
  return;
}

void LindzeyDisplay::addObject(double x, double y, double r, int age)
{
  ladar_object temp_object;
  temp_object.x = SCALING*x;
  temp_object.y = SCALING*y;
  temp_object.r = SCALING*r;
  temp_object.color = min(4,(int)floor(age/25));
  pthread_mutex_lock(&mutex1);
  ladar_objects.push_back(temp_object);
  pthread_mutex_unlock(&mutex1);
  return;
}

void LindzeyDisplay::sendLines(int numlines, double *x1, double *x2, double *y1, double *y2)
{

  flag = 1;
  ladar_line temp_line;
  pthread_mutex_lock(&mutex1);
  ladar_lines.clear();
  for(int i=0; i<numlines;i++)
  {
    temp_line.x1 = SCALING*x1[i];
    temp_line.x2 = SCALING*x2[i];
    temp_line.y1 = SCALING*y1[i];
    temp_line.y2 = SCALING*y2[i];
    ladar_lines.push_back(temp_line);
  }
  pthread_mutex_unlock(&mutex1);
 
 return;

}
void LindzeyDisplay::sendLine(double x1, double x2, double y1, double y2, int color)
{

  flag = 1;
  ladar_line temp_line;
  temp_line.x1 = SCALING*x1;
  temp_line.x2 = SCALING*x2;
  temp_line.y1 = SCALING*y1;
  temp_line.y2 = SCALING*y2;

  pthread_mutex_lock(&mutex1);
  ladar_lines.push_back(temp_line);
  pthread_mutex_unlock(&mutex1);
 
 return;

}

void LindzeyDisplay::sendScan(int numPts, double *xpts, double *ypts, double *zpts)
{
  ladar_point temp_point;
  pthread_mutex_lock(&mutex1);

  //receiving a new scan, discard old points
  ladar_points.clear(); 
  for(int i=0; i<numPts; i++)
  {
    temp_point.x = SCALING*xpts[i];
    temp_point.y = SCALING*ypts[i];
    temp_point.z = SCALING*zpts[i];

    ladar_points.push_back(temp_point);
  }
  //  fprintf(stderr, "received %d points \n", numPts);
  pthread_mutex_unlock(&mutex1);

  return;
}

void LindzeyDisplay::sendPoint(double x, double y)
{

  //  fprintf(stderr, "received point! %f, %f \n", x, y);

  ladar_point temp_point;
  temp_point.x = SCALING*x;
  temp_point.y = SCALING*y;

  pthread_mutex_lock(&mutex1);
  //  ladar_points.clear();
  ladar_points.push_back(temp_point);
  //  fprintf(stderr, "num ladarpoints: %d \n", ladar_points.size());
  pthread_mutex_unlock(&mutex1);

  flag = 1;
 
 return;
}


//intializes openGL functions
//copied from main() loop of jeremy's
//demo code
void LindzeyDisplay::mainOpenGL()
{
  //int main(int argc, char **argv)
  //    int iret1;
    //    pthread_t thread1;

    //Spread stuff
    //    Initialize_spread();

    //    iret1 = pthread_create( &thread1, NULL, Read_message, NULL);

    glutInit(arg1, arg2);
    //    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowPosition(40, 40);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Laura's OpenGL Program");

    //glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
    glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
    glColor3f(1.0, 1.0, 1.0);

    glEnable(GL_DEPTH_TEST);

    /* set callback functions */
    glutDisplayFunc(display); 

    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutPassiveMotionFunc(motionPassive);

    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);
    glutIdleFunc(idle);

    glutMainLoop();
	 
    return;

}


void display(void)
{
  glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
  //  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer
  glLoadIdentity();
  //  gluLookAt (0.0, 20, 20.0, 0.0, -0.5, -1.0, 0.0, 1.0, 0.0); //camera position, x,y,z, looking at x,y,z, Up Positions of the cameraa
  gluLookAt (0.0, 0.0, 30.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0); //camera position, x,y,z, looking at x,y,z, Up Positions of the cameraa

  glRotatef(xrot,1.0,0.0,0.0); //rotate our camera on teh x-axis (left and right)
  glRotatef(yrot,0.0,1.0,0.0); //rotate our camera on the y-axis (up and down)
  glTranslated(- xpos,- ypos,- zpos); //translate the screen to the position of our camera

  glEnable (GL_DEPTH_TEST); //enable the depth testing


  //====================
  /* draw the floor */
  //====================
  /**  
  glBegin(GL_QUADS);
  glColor3f(0.85f, 0.85f, 0.85f);
  glVertex3f(XMIN, ZMIN, 0.0);
  glVertex3f(XMIN, ZMAX, 0.0);
  glVertex3f(XMAX, ZMAX, 0.0);
  glVertex3f(XMAX, ZMIN, 0.0);		
  glEnd();
  **/

  //====================
  //draw fine grid
  //====================
  if(GRIDLINES)
    {
      //vertical lines
      glLineWidth (1.5);
      glColor3f (0.0F, 0.0F, 0.0F);
      glBegin (GL_LINES);
      for (int i= XMIN; i < XMAX+1; i++)
	{
	  glVertex3f (i, 0, ZMIN);
	  glVertex3f (i, 0, ZMAX);
	};
      glEnd ();
      
      //horizontal lines
      glLineWidth (1.5);
      glColor3f (0.0F, 0.0F, 0.0F);
      glBegin (GL_LINES);
      for (int i= ZMIN; i < ZMAX+1; i++)
	{
	  glVertex3f ( XMIN, 0, i);
	  glVertex3f ( XMAX, 0, i);
	};
      glEnd ();
    }
  
  //====================
  //draw coordinate axes
  //====================

  glLineWidth (1);
  glColor3f (0.0F, 0.0F, 1.0F);
  glBegin(GL_LINES);
  glVertex3f(XMIN,0,0);
  glVertex3f(XMAX,0,0);
  glVertex3f(0,ZMAX, 0);
  glVertex3f(0,ZMIN, 0);
  glEnd();
  
  
  if(flag==1)// && mess_recvd==1)
    {



      //============START MUTEX LOCK=============
      pthread_mutex_lock(&mutex1);
      
      //DO ALL YOUR PLOTTING IN HERE!!!!!

      //draw alice state
      /**
      glPushMatrix();
      glColor3f (0.0, 1.0, 0.0);
      //coordinate frame is (x,z,-y)
      //      glTranslatef(state.N, 0.25, -state.E);
      glutSolidSphere(0.40f,10,10); //wheel radius is 0.40
      glPopMatrix();
      **/
      int tempcolor;
      //printing the ladar points
      glColor3f(0.5F, 0.0F, 0.5F);
      glBegin(GL_POINTS);
      //      fprintf(stderr, "size of ladar_points: %d \n", ladar_points.size());
      for(unsigned int n = 0; n < ladar_points.size(); n++)
	{
	  glVertex3f(ladar_points[n].x,ladar_points[n].y,0);
	  //	  fprintf(stderr, "pt. %d \n", n);
	}
      glEnd();
        
      //plotting the segmentations
      glColor3f(1.0F, 0.0F, 0.0F); 
      glLineWidth(2);
      glBegin(GL_LINES);
      //      fprintf(stderr, "size of ladar_lines: %d \n", ladar_lines.size());
      for(unsigned int n=0; n<ladar_lines.size(); n++)
	{
          tempcolor = ladar_lines[n].color;
	  //FIXME: why can't i use the lookup table here??
	  glColor3f(0.0f, 0.0f, 0.0f);

	  glVertex3f(ladar_lines[n].x1,ladar_lines[n].y1,0);
	  glVertex3f(ladar_lines[n].x2,ladar_lines[n].y2,0);

	}
      glEnd();

      //plotting the "object"
      glColor3f(0.0F, 1.0F, 0.0F);
      glBegin(GL_QUADS);
      double tempx, tempy, tempr;

      for(unsigned int n=0; n<ladar_objects.size();n++)
      {
	tempx = ladar_objects[n].x;
	tempy = ladar_objects[n].y;
	tempr = ladar_objects[n].r;
	tempcolor = ladar_objects[n].color;
	glColor3f(colorLookup[tempcolor][0], colorLookup[tempcolor][1], colorLookup[tempcolor][2]);
	glVertex3f(tempx,tempy+tempr,0);
	glVertex3f(tempx+tempr, tempy, 0);
	glVertex3f(tempx,tempy-tempr,0);
	glVertex3f(tempx-tempr, tempy, 0);

      }
      glEnd();



      //============STOP MUTEX LOCK==============
      pthread_mutex_unlock(&mutex1);
    }
  
  glutSwapBuffers();
  
  
}

void idle(void)
{
  /* Check flag variable */

  //============START MUTEX LOCK=============
  pthread_mutex_lock(&mutex1);

  if(flag==1)
	glutPostRedisplay();
			  
  //============STOP  MUTEX LOCK=============
  pthread_mutex_unlock(&mutex1);

}


void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	   lbuttondown = true;
	}
      else
	{
	   lbuttondown = false;
	}
    }
}

void motion(int x, int y)
{
  if ( lbuttondown)
    {
      float diffx=(x- lastx)/10; //check the difference between the current x and the last x position
      float diffy=(y- lasty)/10; //check the difference between the current y and the last y position  
       lastx=x; //set  lastx to the current x position
       lasty=y; //set  lasty to the current y position
      xrot += diffy; //set the xrot to xrot with the addition of the difference in the y position
      yrot += diffx; //set the xrot to yrot with the addition of the difference in the x position
      glRotatef(xrot,1.0,0.0,0.0); //rotate our camera on teh x-axis (left and right)
      glRotatef(yrot,0.0,1.0,0.0); //rotate our camera on the y-axis (up and down)
      display();
    }

  
}

void motionPassive(int x, int y)
{
   lastx = x;
   lasty = y;
}
void keyboard(unsigned char key, int x, int y)
{
  /* This time the controls are:
     
  "a": move left
  "d": move right
  "w": move forward
  "s": move back
  "t": toggle depth-testing
  
  */
  
  
  if (key=='2')
    {
      float xrotrad, yrotrad;
      yrotrad = (yrot / 180 * M_PI);
      xrotrad = (xrot / 180 * M_PI);
       xpos += float(sin(yrotrad)/10) ;
       zpos -= float(cos(yrotrad)/10) ;
       ypos -= float(sin(xrotrad)/10) ;
    }
  
  if (key=='8')
    {
      float xrotrad, yrotrad;
      yrotrad = (yrot / 180 * M_PI);
      xrotrad = (xrot / 180 * M_PI);
       xpos -= float(sin(yrotrad)/10);
       zpos += float(cos(yrotrad)/10) ;
       ypos += float(sin(xrotrad)/10);
    }
  
  if (key=='4')
    {
      float yrotrad;
      yrotrad = (yrot / 180 * M_PI);
       xpos += float(cos(yrotrad)/10);
       zpos += float(sin(yrotrad)/10);
    }
  
  if (key=='6')
    {
      float yrotrad;
      yrotrad = (yrot / 180 * M_PI);
       xpos -= float(cos(yrotrad)/10);
       zpos -= float(sin(yrotrad)/10);
    }


  if (key==27)
    {
      exit(0);
    }

}



void resize(int w, int h)
{
	//if (height == 0) height = 1;
	flag = 1;
	glViewport (0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
	glMatrixMode (GL_PROJECTION); //set the matrix to projection
	glLoadIdentity ();
	gluPerspective (60, (GLfloat)w / (GLfloat)h, 1.0, 1000.0); //set the perspective (angle of sight, width, height, , depth)
	glMatrixMode (GL_MODELVIEW); //set the matrix back to model

}
