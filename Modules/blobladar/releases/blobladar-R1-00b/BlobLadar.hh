#include <string>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/coords.hh>

// Sensnet/Skynet support
#include <skynet/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

#define PI 3.14159265
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define MAXNUMCARS 50 //max num cars we can track
#define MINNUMPOINTS 3 //min # pts to consider object a 'car'
#define NUMSCANPOINTS 181
#define SCALINGFACTOR 4 //used in determining 
           //discontinuities for segmentation

using namespace std;


/**
 * If we're tracking objects rather than cars, this struct is how we keep
 * track of all relevant information
 **/
struct objectRepresentation {
  //the center of mass of this object

  double N,E;

  //histories of the points, ranges and angles for this car
  vector<vector<NEcoord> > scans;
  vector<vector<double> > ranges;
  vector<vector<double> > angles;

  //how many times we've seen it, and when the last one was
  int timesSeen;
  int lastSeen;

  //start and end distances (+ means car is in foreground, 
  //0 means at edge of scan, - means car is in background)
  double dStart, dEnd; 

};



// Line perceptor module
class ObsPerceptor
{
  public:

  // Constructor
  ObsPerceptor();

  // Destructor
  ~ObsPerceptor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);



  public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  public:

  // Update the map
  int update();

  public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, ObsPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, ObsPerceptor *self, const char *token);

  //tracking functions
  void foo();
  void trackerInit();
  void processScan();
  void segmentScan();
  void prepObjects();

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  // Sensnet module
  sensnet_t *sensnet;

  // Console interface
  cotk_t *console;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];

  /************ defines from trackMO ***************/


  double rawData[NUMSCANPOINTS][2];
  double diffs[NUMSCANPOINTS-1];
  double utmData[NUMSCANPOINTS][3];

  objectRepresentation candidates[MAXNUMCARS];
  objectRepresentation objects[MAXNUMCARS];

  //first and last index of each object, as found 
  //by segmentScan()
  int posSegments[NUMSCANPOINTS][2]; 
  //number of pts in ith object, again as found 
  //by segmentScan()
  int sizeSegments[NUMSCANPOINTS]; 
  int numCandidates; //number of objects okayed by prepobjects
  int numSegments; //number of objects found by segmentScan
  int numFrames; //how many frames we've processed

   //the stack to keep track of objects
  vector<int> openObjs; 
  vector<int> usedObjs;
  vector<int>::iterator objIterator;


};

//TODO: shouldn't use defines here (static const?)
#define MAXNUMOBJS 50





// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


