/**
 * File: BlobLadar.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 9
 **/

#include "BlobLadar.hh"

// Default constructor
BlobLadar::BlobLadar()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
BlobLadar::~BlobLadar()
{
  fclose(logfile);
}


// Parse the command line
int BlobLadar::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // See which mode we are running in (sensnet or log)
  if (this->options.inputs_num == 0)
    this->mode = modeLive;
  else
    this->mode = modeReplay;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
      
  trackerInit();
  return 0;
}



// Initialize sensnet
int BlobLadar::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet itnerface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // If replaying log files, now is the time to open them
  if (this->mode == modeReplay)
    if (sensnet_open_replay(this->sensnet, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");

  // Default ladar set
  numSensorIds = 0;
  sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (sensnet_join(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob), 5) != 0)
      return ERROR("unable to join %d", ladar->sensorId);
  }
  
  return 0;
}


// Clean up sensnet
int BlobLadar::finiSensnet()
{
  int i;
  Ladar *ladar;

  if(this->mode == modeReplay)
    sensnet_close_replay(this->sensnet);

  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;
    sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}


// Update the map with new range data
int BlobLadar::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  usleep(100000);
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    // Check the latest blob id
    if (sensnet_peek(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
      break;

    // Is this a new blob?
    if (blobId == ladar->blobId)
      continue;
    ladar->blobId = blobId;

    // If this is a new blob, read it
    if (sensnet_read(this->sensnet, ladar->sensorId,
                     SENSNET_LADAR_BLOB, blobId, blobLen, &blob) != 0)
      break;

    //testing - checking that I can access the data and creating template for future usage =)
    //sensor id
    //    fprintf(stderr,"sensorID: %d \n", blob.sensor_id);

    //scan id
    //    fprintf(stderr, "scanID: %d \n", blob.scanid);

    //timestamp
    //    fprintf(stderr, "timestamp: %f \n", blob.timestamp);

    //state
    //    fprintf(stderr, "Vehicle state (N, E, timestamp): %f, %f, %f \n", blob.state.utmNorthing, blob.state.utmEasting, blob.state.timestamp);

    //number of points
    //    fprintf(stderr, "num Points: %d \n", blob.num_points);

    //raw ranges
    //    fprintf(stderr, "first and last raw ranges: %f, %f \n", blob.points[0][1], blob.points[180][1]);

    //transformed to sensor frame
    float sfx, sfy, sfz; //sensor frame vars
    sensnet_ladar_br_to_xyz(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);
    //    fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);

    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    sensnet_ladar_sensor_to_vehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);

    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
    sensnet_ladar_vehicle_to_local(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);

    //coyping the data to a arrays accessible by any 
    //function in the program. Seems inefficient....
    double xpts[NUMSCANPOINTS];
    double ypts[NUMSCANPOINTS];
    double zpts[NUMSCANPOINTS];

    int numReturns = 0;
    for(int j=0; j < NUMSCANPOINTS; j++) 
    {
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      sensnet_ladar_br_to_xyz(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      sensnet_ladar_sensor_to_vehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      sensnet_ladar_vehicle_to_local(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      xyzData[j][0] = lfx;
      xyzData[j][1] = lfy;
      xyzData[j][2] = lfz;

      //if this isn't a no-return point, send to display
      if(rawData[j][1] < 80) {
        xpts[numReturns]=lfx;
        ypts[numReturns]=lfy;
        zpts[numReturns]=lfz;
        numReturns++;
      }

    }

    for(int j=0; j < NUMSCANPOINTS; j++) 
    {
      sensnet_ladar_br_to_xyz(&blob, blob.points[j][ANGLE], blob.points[j][RANGE] + RADIALOFFSET,&sfx, &sfy, &sfz);
      sensnet_ladar_sensor_to_vehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      sensnet_ladar_vehicle_to_local(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      depthData[j][0] = lfx;
      depthData[j][1] = lfy;
      depthData[j][2] = lfz;

    }




    //for plotting the segmentation
    sensnet_ladar_br_to_xyz(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
    sensnet_ladar_sensor_to_vehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    sensnet_ladar_vehicle_to_local(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    ladarState.X = lfx;
    ladarState.Y = lfy;
    ladarState.Z = lfz;

    currState.X = blob.state.utmNorthing;
    currState.Y = blob.state.utmEasting;
    currState.Z = blob.state.utmAltitude;
    //TODO: check if this runs fast enough to not be limiting factor
    //    fprintf(stderr, "calling processScan() \n");
    if(useDisplay) {
      myDisplay->sendScan(numReturns, xpts, ypts, zpts);
    }

    //call all the tracking functions
    processScan(blob.sensor_id);

    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%ladar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(ladar->sensorId),
                  blob.scanid, fmod((double) blob.timestamp * 1e-6, 10000));
    }
  } //end cycling through ladars

  return 0;
} //end update()


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"BlobLadar $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int BlobLadar::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int BlobLadar::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int BlobLadar::onUserQuit(cotk_t *console, BlobLadar *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int BlobLadar::onUserPause(cotk_t *console, BlobLadar *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  BlobLadar *percept;
  
  percept = new BlobLadar();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  // Initialize openGL display
  if (percept->options.disable_display_flag)
    percept->useDisplay = 0;
  else
    percept->useDisplay = 1;

  if(percept->useDisplay) {
    percept->myDisplay = new LindzeyDisplay(&argc, argv);
    DGCstartMemberFunctionThread(percept->myDisplay, &LindzeyDisplay::mainOpenGL);
    //    percept->fitDisplay = new LindzeyDisplay(&argc, argv);
    //    DGCstartMemberFunctionThread(percept->fitDisplay, &LindzeyDisplay::mainOpenGL);
  }


  fprintf(stderr, "entering main thread of BlobLadar \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Wait for new data
    if (sensnet_step(percept->sensnet, 100) != 0)
    {
      break;
    } else {
      //      fprintf(stderr, "should have new data! \n");
    }

    // Update the map
    if (percept->update() != 0)
    {	
      break;
    } else {
      //      fprintf(stderr, "should be updating the map! \n");
    }

  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



