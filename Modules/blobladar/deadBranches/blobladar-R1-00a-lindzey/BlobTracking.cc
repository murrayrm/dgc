/*
 * This is all the code for trakcing
 * blobs in ladar data.
 * Most of it comes directly from Laura's
 * summer project, at least for now
 */ 

#include "BlobLadar.hh"

void ObsPerceptor::foo()
{
  fprintf(stderr, "foo! \n");
  return;
}

void ObsPerceptor::trackerInit()
{
  numFrames = 0;

  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    //    fprintf(stderr, "pushing back %d \n", i);
    openObjs.push_back(i);
  }
  //TODO: add KF stuff (

  //  setupLogs();
  //  cout<<"trackerInit(...) Finished"<<endl;
  return;
}

void ObsPerceptor::processScan()
{
  numFrames++;
  segmentScan(); 
  prepObjects();

  return;
}

void ObsPerceptor::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][1] - rawData[i][1];
  }

  int tempNum = 0; //number objects segmented from curr scan
  int currSize = 1; //size of current object

  // This is the actual segmentation functionality...
  // the above was just setting up the data we need

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  double distThresh;
  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    if(rawData[i][1] > 80.0) {
      distThresh = 0;
    } else {
    //threshold should be scaled based on distance from ladar
    distThresh = max(.10,rawData[i][1]*.0175*SCALINGFACTOR);
    //    cout<<"range: "<<rawData[i]<<"   threshold: "<<distThresh<<endl;
    }
    //if the distance is too large to be the same object
    //note that this distance is the distance btwn point i and pt i+1
    if(fabs(diffs[i]) > distThresh) {
 
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;

    } else { //this point belongs to current object
      
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points

  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  int origin;
  numSegments = 0;
  for(int i=0; i<=tempNum; i++) {
    st = tempPos[i][0];
    en = tempPos[i][1];
    origin=0; 
    //    cout<<"rawData: ";
    for(int j=st; j<=en; j++) {
      //      cout<<rawData[j]<<", ";
      if(rawData[j][1] > 80) {
	origin=1;
      } else {
	//	cout<<'('<<newPoints[j].N<<' '<<newPoints[j].E<<"), ";
      }
    }
    //    cout<<endl;
    //checking that this isn't a group of no-return points
    if(origin==0 && tempSize[i]>=MINNUMPOINTS) {
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      numSegments++;
    }
  }

  //TODO: add convex hull requirement

  //now, trying to output the segmentation, to check that it's correct
  //TODO: output this to gui (sensviewer?)
  /**
  for(int i=0; i<numSegments;i++) {
      m_outputSegments<<posSegments[i][0]<<' '<<posSegments[i][1]<<' ';
  }
  for(int i=numSegments; i<NUMSCANPOINTS; i++) {
    m_outputSegments<<"-1 -1 ";
  }
  m_outputSegments<<endl;
  **/

  fprintf(stderr, "we found %d segments \n", numSegments);


  return;
}


/**
 * This code goes through all the new segments, picks 
 * out the ones that correspond to objects that we care 
 * about (based on number of points in object, and that 
 * the object isn't a group of no-return points) 
 * It then updates the candidates array with the 
 * appropriate information
**/
void ObsPerceptor::prepObjects() {

  //  cout<<"entering prepObjects()"<<endl;
  //  cout<<"STARTING prepObjects()"<<endl;

  //TODO: change this. candidates is a confusing var name 
  numCandidates = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  for(int i=0; i<numSegments; i++) {

    int objectsize = sizeSegments[i];

    if(objectsize >= MINNUMPOINTS) {

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      //if dStart or dEnd > 0, that means that that end of the object 
      //is in the foreground, <0 means the object next to it is in the 
      //foreground, and 0 means that it's at the edge of a scan
#warning "check that ranges don't get set to 0 for no-return values before we get here"
      if(objStart == 0) {
	dStart = 0;
      } else {
	dStart = -diffs[objStart - 1];
      }
      if(objEnd == NUMSCANPOINTS - 1) {
	dEnd = 0;
      } else {
	dEnd = diffs[objEnd];
      }

      //want to fit center to object here, and put 
      // it in candidates array....

      //first, check that it's not a group of no-return points
      if(rawData[objStart][1] < 80.0 && rawData[objEnd][1] < 80.0 ) {

	double cenN, cenE;
	double sumN = 0;
	double sumE = 0;

	//TODO: check this 
	//currently have x from sensnet_ladar_vehicle_to_local
	//as the northing value
	for(int j=0;j<objectsize;j++) {
	  sumE += utmData[j+objStart][1];
	  sumN += utmData[j+objStart][0];
	}

	cenN = sumN / objectsize;
	cenE = sumE / objectsize;

	candidates[numCandidates].N = cenN;
	candidates[numCandidates].E = cenE; 
	//	cout<<"in prep objects, new object's N is: "<<candidates[numCandidates].N<<endl;

	candidates[numCandidates].dStart = dStart;
	candidates[numCandidates].dEnd = dEnd; 

	vector<NEcoord> tmppts;
	vector<double> rngs;
	vector<double> angs;

	NEcoord tmp;
	double foo, bar;

	//#warning "keeping this many points may be a problem"
	for(int m=0;m<objectsize;m++) {
	  tmp.N = utmData[m+objStart][0];
	  tmp.E = utmData[m+objStart][1];
	  tmppts.push_back(tmp);
	  foo = rawData[m+objStart][1];
	  bar = rawData[m+objStart][0];
	  rngs.push_back(foo);
	  angs.push_back(bar);
	}


	//TODO: these cause segfaults - don't need now,
	//but leaving in because for more sophisticated
	//object representations, will want this data
	//	candidates[numCandidates].scans.push_back(tmppts);
	//	candidates[numCandidates].ranges.push_back(rngs);
	//	candidates[numCandidates].angles.push_back(angs);

	numCandidates++; //we've found a new object

      } //end ranges condition
    }   //end num points condition
  }     //end cycling through segments
  fprintf(stderr, "we have %d new objects \n", numCandidates);
}   //end of prepObjects()
