/**
 * File: BlobTracking.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 11
 **/

#include "BlobLadar.hh"
using namespace std;

void BlobLadar::trackerInit()
{
  numFrames = 0;
  numObjects = 0;

  useMapTalker = 1;
  subgroup = 0;

  moduleMapID = -1;  
  initSendMapElement(skynetKey);

  logfile = fopen("BlobLadar.log","w");

  //initializes the stack of open positions in the car array
  for(int i=(MAXNUMOBJS-1);i>=0; i--){
    openObjs.push_back(i);
    openCars.push_back(i);
  }
  fprintf(stderr,"open/closed obj&car sizes: %d %d %d %d \n", openObjs.size(), usedObjs.size(), openCars.size(), usedCars.size());

  //setting up matrics for KF calculations
  //#warning "these need to be tuned!"
  double dt = .0133; 
  double sa = .05; //std dev of system noise -- tune!
  double sz = .1; //std dev of measurement noise -- should be fxn of range/theta!!!

  A.resetSize(2,2);
  double Aelems[] = {1,dt,0,1};
  A.setelems(Aelems);

  C.resetSize(1,2);
  double Celems[] = {1,0};
  C.setelems(Celems);

  I.resetSize(2,2);
  double Ielems[] = {1,0,0,1};
  I.setelems(Ielems);

  R.resetSize(2,2);
  double Relems[] = {0,0,0,sa};
  R.setelems(Relems);

  Q.resetSize(1,1);
  double Qelems[] = {sz};
  Q.setelems(Qelems);

  return;
}

void BlobLadar::processScan(int ladarID)
{
  //  fprintf(stderr, "processScan called for ladar # %d \n", ladarID);
  numFrames++;

  //first, we clear the object list. 
  if(useDisplay) {
    myDisplay->clearObjects();
  }

  segmentScan(); 
  createObjects();
  classifyObjects();

  checkMotion();

  sendObjects();
  //  sendCars();

  cleanObjects();
  //  cleanCars();

  //  KFupdate();
  //  KFpredict();

  return;
}

void BlobLadar::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = rawData[i+1][RANGE] - rawData[i][RANGE];
  }

  //number objects segmented from curr scan
  int tempNum = 0; 
  //size of current object
  int currSize = 1; 

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  tempSize[0] = 0;

  double distThresh;
  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    if(rawData[i][RANGE] > 80.0) {
      distThresh = 0;
    } else {
      //threshold scaled based on range
      distThresh = max(.10,rawData[i][RANGE]*.0175*SCALINGFACTOR);
    }
  
    //if the distance is too large to be the same object
    //note that this distance is the distance btwn point i and pt i+1
    if(fabs(diffs[i]) > distThresh) {
 
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;

    } else { //this point belongs to current object
      
      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points

  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  int origin;
  numSegments = 0;
  fprintf(logfile, "segment sizes: ");

  // checking if each group of points is at the origin
  for(int i=0; i<=tempNum; i++) {
    st = tempPos[i][0];
    en = tempPos[i][1];
    origin=0; 

    for(int j=st; j<=en; j++) 
    {
      if(rawData[j][RANGE] > 80) 
      {
	origin=1;
      } 
    }

    //checking that this isn't a group of no-return points
    if(origin==0 && tempSize[i]>=MINNUMPOINTS) {
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      int foo = tempSize[i];
      fprintf(logfile," %d , ",foo);
      numSegments++;
    }

  }
    fprintf(logfile,"\n");
  //TODO: add convex hull requirement

  // send the segments to the gui
  double x1[numSegments];
  double x2[numSegments];
  double y1[numSegments];
  double y2[numSegments];
  int tmpindex;

  for(int i=0; i<numSegments;i++) {
    //FIXME: this only works for ladar at the origin.
    //need to change this to be ladar's position
    x1[i]=ladarState.X;
    y1[i]=ladarState.Y;
    tmpindex = posSegments[i][1];
    x2[i]=xyzData[tmpindex][0];
    y2[i]=xyzData[tmpindex][1];
  }
  if(useDisplay) {
    // sending the segmentation lines
    //    myDisplay->sendLines(numSegments, x1, x2, y1, y2);
  }

  return;
}


/**
 * This code goes through all the new segments
 * and creates new objects for them. 
 * The checking for min number points and no-return
 * points was done in segment scan
 **/
void BlobLadar::createObjects() {

  int tempCounter = 0;

  int objStart, objEnd;
  double dStart, dEnd;

  for(int i=0; i<numSegments; i++) 
  {

    int objectsize = sizeSegments[i];

    objStart = posSegments[i][0];
    objEnd = posSegments[i][1];

    //if dStart or dEnd > 0, that means that that end of the object 
    //is in the foreground, <0 means the object next to it is in the 
    //foreground, and 0 means that it's at the edge of a scan
    //#warning "check that ranges don't get set to 0 for no-return values before we get here"
    if(objStart == 0) {
  	dStart = 0;
    } else {
  	dStart = -diffs[objStart - 1];
    }
    if(objEnd == NUMSCANPOINTS - 1) {
  	dEnd = 0;
    } else {
  	dEnd = diffs[objEnd];
    }

    //want to fit center to object here, and put 
    // it in newObjects array....

    double cenX, cenY;
    double sumX = 0;
    double sumY = 0;

    //TODO: check this 
    //currently have x from sensnet_ladar_vehicle_to_local
    //as the northing value
    fprintf(logfile, "object size, object start: %d, %d \n", objectsize, objStart);
    for(int j=0;j<objectsize;j++) 
    {
      //FIXME: sometimes segfaults here. WHY???

      sumX += xyzData[j+objStart][0];
      sumY += xyzData[j+objStart][1];
    }

    cenX = sumX / objectsize;
    cenY = sumY / objectsize;

    newObjects[tempCounter].X = cenX;
    newObjects[tempCounter].Y = cenY; 
    
    newObjects[tempCounter].dStart = dStart;
    newObjects[tempCounter].dEnd = dEnd; 

    vector<point2> tmppts;
    vector<double> rngs;
    vector<double> angs;

    point2 tmp;
    double foo, bar;

    //#warning "keeping this many points may be a problem"
    for(int m=0;m<objectsize;m++) {
      tmp.x = xyzData[m+objStart][0];
      tmp.y = xyzData[m+objStart][1];
      tmppts.push_back(tmp);
      foo = rawData[m+objStart][1];
      bar = rawData[m+objStart][0];
      rngs.push_back(foo);
      angs.push_back(bar);
    }

    newObjects[tempCounter].scans.push_back(tmppts);
    newObjects[tempCounter].ranges.push_back(rngs);
    newObjects[tempCounter].angles.push_back(angs);

    tempCounter++; //we've added a new object
  
  }     //end cycling through segments

}   //end of prepObjects()



/**
 * This function assumes that the scan has been segmented,
 * and classifies the new objects into: 
 *    already-seen object
 *    already-seen car
 *    new object 
 * Then, it calls the appropriate functions to update
 * the representations.
 **/
void BlobLadar::classifyObjects() 
{

  double distTraveled;

  //whether this segment has already been assigned
  //WARNING: leads to screwy behaviour if for some reason
  //   we have multiple "objects" corresponding to the same
  //   physical object
  int taken[numSegments];
  for(int i=0;i<numSegments;i++) {
    taken[i] = 0;
  }
  double oldX, oldY;
  double newX, newY;

  /**
   * First, we cycle through all previously seen objects 
   * to see if the new object matches one of them
   **/
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int k = usedObjs.at(i);
    //    oldX = objects[k].Xmu.getelem(0,0);
    //    oldY = objects[k].Ymu.getelem(0,0);
    oldX = objects[k].X;
    oldY = objects[k].Y;
    fprintf(stderr, "old obj position: %f %f \n", oldX, oldY);

    for(int j=0; j<numSegments; j++) {
      if(taken[j] == 0) { //if new object hasn't already been classified
	newX = newObjects[j].X;
	newY = newObjects[j].Y;
	distTraveled = dist(oldX, oldY, newX, newY);

	if(distTraveled < MAXTRAVEL) { //if they match
	  //	  cout<<"new object matched old object...updating!"<<endl;
	  taken[j] = 1;
	  fprintf(stderr,"updating object #%d \n",k);
	  updateObject(&objects[k], &newObjects[j]);

	} //end checking distance traveled
      } //end checking of newObj is taken
    } //end cycling through new objects
  } //end cycling through old objects



  /**
   * Next, we cycle through all previously seen cars 
   * to see if obj matches. This functionality goes 
   * AFTER matching newobjs to oldobjs cuz we don't want 
   * to match an already seen lamppost in the car's path 
   * to the car...
   **/

  double Xmin, Xmax, Ymin, Ymax;
  double dx1, dy1, dx2, dy2;
  int foo = 0;
  fprintf(stderr,"open/closed obj&car sizes: %u %u %u %u \n", openObjs.size(), usedObjs.size(), openCars.size(), usedCars.size());
  for(unsigned int i=0; i<usedCars.size(); i++) {

    int k = usedCars.at(i);

    oldX = cars[k].Xmu_hat.getelem(0,0);
    oldY = cars[k].Ymu_hat.getelem(0,0);
    //    oldX = cars[k].N;
    //    oldY = cars[k].E;
    dx1 = cars[k].dx1;
    dy1 = cars[k].dy1;
    dx2 = cars[k].dx2;
    dy2 = cars[k].dy2;

    //creating bounding box for potential matches
    Xmin = min(oldX, min(oldX+dx1, min(oldX+dx2, oldX+dx1+dx2))) - CARMARGIN;
    Xmax = max(oldX, max(oldX+dx1, max(oldX+dx2, oldX+dx1+dx2))) + CARMARGIN;
    Ymin = min(oldY, min(oldY+dy1, min(oldY+dy2, oldY+dy1+dy2))) - CARMARGIN;
    Ymax = max(oldY, max(oldY+dy1, max(oldY+dy2, oldY+dy1+dy2))) + CARMARGIN;


    for(int j=0; j<numSegments; j++) {
      if(taken[j] == 0) { //this obj has not already been claimed
	newX = newObjects[j].X;
	newY = newObjects[j].Y;
	//check if this obj matches car
	if(newX < Xmax && newX > Xmin && newY < Ymax && newY > Ymin) {
	  //the obj and car should be associated
	 
	  taken[j] = 1;
	  fprintf(stderr, "new obj matched car # %d -- updating!! \n", k);
	  updateCar(&cars[k], &newObjects[j]);
	  foo++;
	}  //end of checking if newObj and car match
      }   //end of checking if obj already claimed
    }    //end cycling thru newObjs
  }     //end cycling thru numCars




  /**
   * Finally, we take care of the new objects that don't match anything we've
   * seen before
   **/
  int numNew = 0;
  for(int i=0; i<numSegments; i++) {
    if(taken[i] == 0) { 
      newX = newObjects[i].X;
      newY = newObjects[i].Y;

      fprintf(stderr, "no match for new obj at position: %f %f \n", newX, newY);
      addObject(&newObjects[i]);
      numNew++;
    } //end checking if object already classified
  } //end cycling through numSegments
  //  fprintf(stderr, "number new objects: %d, num old: %d \n", numNew, numSegments-numNew);

}

/**
 * This function is called when it is determined that a newobject 
 * matches an old one. As such, it simply replaces the center position, 
 * and adds the latest scan/ranges data
**/
void BlobLadar::updateObject(objectRepresentation* oldObj, objectRepresentation* newObj) {

  oldObj->X = newObj->X;
  oldObj->Y = newObj->Y;

  //  fprintf(stderr,"updated object at: %f, %f \n", newObj->N, newObj->E);

  oldObj->dStart = newObj->dStart;
  oldObj->dEnd = newObj->dEnd;

  vector<point2> newscans(newObj->scans.back());
  vector<double> newranges(newObj->ranges.back());
  vector<double> newangles(newObj->angles.back());

  //  fprintf(stderr,"updating object \n");
  oldObj->scans.push_back(newscans);
  oldObj->ranges.push_back(newranges);
  oldObj->angles.push_back(newangles);

  oldObj->timesSeen = oldObj->timesSeen + 1;
  oldObj->lastSeen = numFrames;

}


/**
 * This function is called when it is determined that a new object
 * has no match in already seen objects or cars
 * It's job is to add an object to the object array, if there is room.
 **/
void BlobLadar::addObject(objectRepresentation* newObj) {
  numObjects = numObjects + 1;
  //  fprintf(stderr, "entering add object, w/ object # %d \n", numObjects);
  if(usedObjs.size() < MAXNUMOBJS) {

    int newIndex;
    newIndex = openObjs.back();
    openObjs.pop_back();
    usedObjs.push_back(newIndex);

    objects[newIndex].ID = numObjects;
    objects[newIndex].X = newObj->X;
    objects[newIndex].Y = newObj->Y;
    objects[newIndex].dStart = newObj->dStart;
    objects[newIndex].dEnd = newObj->dEnd;

    objects[newIndex].timesSeen = 1;
    objects[newIndex].lastSeen = numFrames;

    //transferring vectors of scan/range/angle history
    vector<point2> newscans(newObj->scans.back());
    vector<double> newranges(newObj->ranges.back());
    vector<double> newangles(newObj->angles.back());

    objects[newIndex].scans.push_back(newscans);
    objects[newIndex].ranges.push_back(newranges);
    objects[newIndex].angles.push_back(newangles);
 
    //initizlizing KF variables
    objects[newIndex].Xmu.resetSize(2,1);
    objects[newIndex].Ymu.resetSize(2,1);
    objects[newIndex].Xsigma.resetSize(2,2);
    objects[newIndex].Ysigma.resetSize(2,2);
    objects[newIndex].Xmu_hat.resetSize(2,1);
    objects[newIndex].Ymu_hat.resetSize(2,1);
    objects[newIndex].Xsigma_hat.resetSize(2,2);
    objects[newIndex].Ysigma_hat.resetSize(2,2);

    double XMelems[] = {objects[newIndex].X, 0};
    double YMelems[] = {objects[newIndex].Y, 0};
    double XSelems[] = {1,0,0,1};
    double YSelems[] = {1,0,0,1};

    objects[newIndex].Xmu.setelems(XMelems);
    objects[newIndex].Xsigma.setelems(XSelems);
    objects[newIndex].Ymu.setelems(YMelems);
    objects[newIndex].Ysigma.setelems(YSelems);

  } else {

    fprintf(stderr,"trying to add obj, no space in buffer \n");

  } //end checking that there's enough room to add new object
} //end addObject


//TODO: use actual struct/message type that sam defines
//TODO: actually calculate radius of object
void BlobLadar::sendObjects() {


  MapElement el;
  vector <point2> objectPoints;

  double tempX, tempY, tempX2, tempY2, dX, dY, radius;
  
  //  fprintf(stderr, "number of objects: %d \n", usedObjs.size());
  int sent = 0;
  for(unsigned int k=0; k<usedObjs.size(); k++) {
    int j = usedObjs.at(k);
  
    int age = objects[j].timesSeen;
    sent++;

    tempY = objects[j].Ymu.getelem(0,0);
    tempX = objects[j].Xmu.getelem(0,0);
    tempY2 = objects[j].Y;
    tempX2 = objects[j].X;

    radius = 0.50;
    dX = objects[j].Xmu.getelem(1,0);
    dY = objects[j].Ymu.getelem(1,0);

    if(useDisplay) {
      myDisplay->addObject(tempX, tempY, radius, age);
      //      fprintf(stderr, "trying to plot line: %f, %f, %f, %f \n ", tempX, tempY, dX, dY);
      //      fprintf(logfile, "looking for color %d  \n", 5);
      myDisplay->sendLine(tempX, tempX + dX, tempY, tempY + dY, 5);
      // to check diff btwn estimated and measured pos
      //      myDisplay->sendLine(tempX, tempX2, tempY, tempY2, 0);
    }


    //    fprintf(stderr, "trying to create and send map element \n");
    if(useMapTalker) {
      objectPoints.clear();
      vector <point2> lastScan(objects[j].scans.back());
      point2 tmppt;
      vector <int> id;
      //fill in the points
      //      fprintf(stderr, "first point: %f, %f \n", lastScan.at(0).N, lastScan.at(0).E);
      for(unsigned int m=0; m < lastScan.size(); m++) {
	tmppt = point2(lastScan.at(m).x, lastScan.at(m).y);
	objectPoints.push_back(tmppt);
      }
   
      //set the originating module and blob ID numbers
      id.clear();
      id.push_back(moduleMapID);
      id.push_back(objects[j].ID);

      el.clear();
      el.set_poly_obs(id, objectPoints);
      //      fprintf(stderr, "object ID: %d, object num %d \n", objects[j].ID, j);
      int bytesSent = sendMapElement(&el, 0);
      //      fprintf(stderr, "sent map element! bytesSent = %d \n", bytesSent);
    }

  } // end checking each object
  return;
}

void BlobLadar::sendCars()
{

  //  fprintf(stderr, "number of cars: %d \n", usedCars.size());
  int sent = 0;
  double tempY, tempX, dX, dY;
  MapElement el;
  vector <point2> cornerPoints;

  for(unsigned int k=0; k<usedCars.size(); k++) {
    int j = usedCars.at(k);
  
    sent++;

    double corners[8];

    corners[0] = 1.0;
    //TODO: check that I've got the corners properly labeled
    corners[0] = cars[j].X;
    corners[1] = cars[j].Y;
    corners[2] = cars[j].X+cars[j].dx1;
    corners[3] = cars[j].Y+cars[j].dy1;
    corners[4] = cars[j].X+cars[j].dx2;
    corners[5] = cars[j].Y+cars[j].dy2;
    corners[6] = cars[j].X+cars[j].dx1+cars[j].dx2;
    corners[7] = cars[j].Y+cars[j].dy1+cars[j].dy2;

    tempY = cars[j].Ymu.getelem(0,0);
    tempX = cars[j].Xmu.getelem(0,0);

    dX = cars[j].Xmu.getelem(1,0);
    dY = cars[j].Ymu.getelem(1,0);

    if(useDisplay) {
      //      myDisplay->addCar(corners);
      //      myDisplay->sendLine(tempX, tempX + dX, tempY, tempY+dY, 5);
    }

    //TODO: special message for car types??
    if(useMapTalker) {
      cornerPoints.clear();
      point2 tmppt;
      vector <int> id;

      //fill in the corners
      tmppt = point2(corners[0], corners[1]);
      cornerPoints.push_back(tmppt);
      tmppt = point2(corners[4], corners[5]);
      cornerPoints.push_back(tmppt);
      tmppt = point2(corners[6], corners[7]);
      cornerPoints.push_back(tmppt);
      tmppt = point2(corners[2], corners[3]);
      cornerPoints.push_back(tmppt);

      //set the originating module and blob ID numbers
      id.clear();
      id.push_back(moduleMapID);
      id.push_back(cars[j].ID);

      el.clear();
      el.set_poly_obs(id, cornerPoints);
      int bytesSent = sendMapElement(&el, 0);

    } // end sending to map
  } // end cycling through each car

  return;
}


//#warning "may want to add more checks before checking every object's motion, every time"
void BlobLadar::checkMotion() {

  for(unsigned int i = 0; i< usedObjs.size(); i++) {
    int index = usedObjs.at(i);

    if(objects[index].timesSeen >= 20) {
  
      double velX, velY, vel;
      velX = objects[index].Xmu.getelem(1,0);
      velY = objects[index].Ymu.getelem(1,0);
      vel = sqrt(velX*velX+velY*velY);
      if(vel > MINSPEED) {
	//	fprintf(stderr, "THIS OBJ IS MOVING!!! \n ");
	//	obj2car(i);

      }
    }
  }
} //end checkMotion



//#warning "this code should remove the object at index, in addition to making a new car...possibly replace with addCar(&object), and removeObject(index)"
void BlobLadar::obj2car(int index) {
  //  cout<<"trying to turn an object into a car"<<endl;
  if(usedCars.size() < MAXNUMCARS-1) { 
    int i = usedObjs.at(index);
  vector<point2> points(objects[i].scans.back());

  int temp;
  temp = openCars.back();
  openCars.pop_back();
  usedCars.push_back(temp);

 int numPoints = points.size();
 //  cout<<"this has "<<numPoints<<" points..."<<endl;

  double Ypts[numPoints];
  double Xpts[numPoints];
  for(int i=0; i<numPoints; i++) {
    Ypts[i] = points.at(i).y;
    Xpts[i] = points.at(i).x;
  }

  int corner;
  myDisplay->addObject(Xpts[0],Ypts[0],.5,200);
  corner = fitCar(Xpts,Ypts,numPoints,&cars[temp],objects[i].Xmu.getelem(1,0), objects[i].Ymu.getelem(1,0)); 
  //  fprintf(stderr, "fit car! corner: %f, %f \n", cars[temp].N, cars[temp].E);
  cars[temp].Xmu.resetSize(2,1);
  cars[temp].Ymu.resetSize(2,1);
  cars[temp].Xsigma.resetSize(2,2);
  cars[temp].Ysigma.resetSize(2,2);
  cars[temp].Xmu_hat.resetSize(2,1);
  cars[temp].Ymu_hat.resetSize(2,1);
  cars[temp].Xsigma_hat.resetSize(2,2);
  cars[temp].Ysigma_hat.resetSize(2,2);

  double XMelems[] = {cars[temp].X, objects[i].Xmu.getelem(1,0)};
  double YMelems[] = {cars[temp].Y, objects[i].Ymu.getelem(1,0)};
  double XSelems[] = {0,0,0,0};
  double YSelems[] = {0,0,0,0};

  //	  cout<<"setting matrix mu-naught to: "<<cars[temp].N<<" and "<<cars[temp].E<<endl;

  cars[temp].Xmu.setelems(XMelems);
  cars[temp].Xsigma.setelems(XSelems);
  cars[temp].Ymu.setelems(YMelems);
  cars[temp].Ysigma.setelems(YSelems);

  cars[temp].ID = numCars;
  
  numCars++; //we've found a car

  removeObject(index);

  } else {
    cerr<<"not enough space in cars array!!"<<endl;
  }
  //  cout<<"exiting obj2car()"<<endl;
}

/**
 * function: fitCar
 * input: takes in set of NE points that have been grouped together as an object,
 *   and finds the two lines that fit them best (think rectangle)
 * output: parameters describing this rectangle
 **/

//#warning "should add idea of uncertainty to position of car!"
int BlobLadar::fitCar(double* Xpoints, double* Ypoints, int numPoints, carRepresentation* newCar, double vN, double vE) {
  int corner = 0;
  //  cout<<"entering fitCar with "<<numPoints<<" points"<<endl;
  fprintf(stderr, "entering fitCar w/ endpoints: %f, %f, %f, %f \n", Xpoints[0], Ypoints[0], Xpoints[numPoints-1], Ypoints[numPoints-1]);
  double minerror, MSE1, MSE2;
  int breakpoint;
  //calculating w/out corner, or new best fit
  lineSegmentFit seg1;
  lineSegmentFit seg2;
  lineSegmentFit tempSeg1;
  lineSegmentFit tempSeg2;

  //returns the fit in temp 1 w/ format {a, b, x1, y1, x2, y2}
  minerror = fitLine(Xpoints, Ypoints, numPoints, &seg1);

  //now, temp1 and temp2 are the line segments, if no corner is found
  //need to see if there's a better fit, w/ a corner

  //now, find min error, but requiring each leg to have at 
  //least 2 points, and each leg contains the breakpoint
  //TODO: need to try each breakpoint twice, giving 
  //  each seg fair shot at first slope
  for(int i=2; i<numPoints-2; i++) {
    double Xpoints1[i];
    double Ypoints1[i];
    double Xpoints2[numPoints-i+1];
    double Ypoints2[numPoints-i+1];

    for(int j=0; j<i; j++) {
      Xpoints1[j] = Xpoints[j];
      Ypoints1[j] = Ypoints[j];
    }
    for(int j=0; j<(numPoints-i+1); j++) {
      Xpoints2[j] = Xpoints[j+2*(i-1)];
      Ypoints2[j] = Ypoints[j+2*(i-1)];
    }
    //FIXME: this isn't really mse...is this best way?
    MSE1 = fitLine(Xpoints1, Ypoints1, i, &tempSeg1);
    MSE2 = fitLine2(Xpoints2, Ypoints2, numPoints-i+1, &tempSeg2, tempSeg1.b);

    //    cout<<"breaking at point "<<i<<" w/ minerror, MSE1, MSE2: "<<minerror<<' '<<MSE1<<' '<<MSE2<<endl;

    //if we've reached a new minimum error, this is our 
    //current best-fit, and
    //should be copied to temp1 & temp2
    //note that this isn't really mean squared error...
    //  it's MSE*numPoints
    /**
   if(minerror > MSE1 + MSE2)
   {
      minerror = MSE1 + MSE2;
      breakpoint = i;
      memcpy(temp1, temp3, 6*sizeof(double));
      memcpy(temp2, temp4, 6*sizeof(double));
      corner = 1;
    }
    **/
  }

  //FIXME: is this really necessary?? 

  //if we didn't see a corner:
  if(corner == 0)
  {

    //want to get points relative to Alice's position
    double x,y; //new n,e pts 
    double thC; //angle from start to end of car
    double th;
    thC = atan2((seg1.x1 - seg1.x2), \
		(seg1.y1 - seg1.y2));
    th = (PI/2) + thC;
    y = seg1.y1 + MINDIMENSION * cos(th);
    x = seg1.x1 + MINDIMENSION * sin(th);

    seg2.a = 0;
    seg2.b = 0;
    seg2.y1 = seg1.y1;
    seg2.x1 = seg1.x1;
    seg2.y2 = y;
    seg2.x2 = x;
  }


  //now, need to get these two segments into the format of a car //  cout<<"tryign to orient deltas properly...vels are: "<<vN<<' '<<vE<<endl;
  //calculating their intersection (to get the N,E points we define car as)
  double xi, yi; //northing and easting intersections
  double dx1, dy1, dx2, dy2; //deltas for segs
  //  fprintf(stderr, "seg1: %f, %f, %f, %f, %f, %f \n",seg1.a, seg1.b, seg1.x1, seg1.y1, seg1.x2, seg1.y2);
  //  fprintf(stderr, "seg2: %f, %f, %f, %f, %f, %f \n",seg2.a, seg2.b, seg2.x1, seg2.y1, seg2.x2, seg2.y2);
  //  fprintf(stderr, "corner = %d \n", corner);
  if(corner == 0 )
  { //corner is start of seg
    fprintf(stderr, "corner is at start of seg \n");
    xi = seg1.x1;
    yi = seg1.y1;
  } else { //corner is intersect of the lines
    yi = (seg1.a - seg2.a)/(seg1.b - seg2.b);
    xi = seg1.a + yi*seg1.b;
  }

  fprintf(stderr, "xi, yi, d1, d2: %f, %f, %f, %f, %f, %f \n", xi, yi, dx1, dy1, dx2, dy2);
//TODO: check this for seg1
  dx1 = seg1.x2 - xi; //seg 1 deltas
  dy1 = seg1.y2 - yi;
  dx2 = seg2.x2 - xi; //seg 2 deltas
  dy2 = seg2.y2 - yi;
  //  cout<<"ixitial corner pos and deltas: "<<xi<<' '<<yi<<' '<<dx1<<' '<<dy1<<' '<<dy2<<' '<<dy2<<endl;
  //  cout<<"input velocity: "<<vN<<' '<<vE<<endl;
  newCar->X = xi; //corner coords
  newCar->Y = yi;
  newCar->dx1 = dx1; //seg 1 deltas
  newCar->dy1 = dy1;
  newCar->dx2 = dx2; //seg 2 deltas
  newCar->dy2 = dy2;

  /**

  //now, making sure the corner is in the right position (using vE and vN) see p.85 - 87
  double thv, th1, th2; //velocity and segment angles
  double tn, te; //temp holders for switching values
  thv = atan2(vN,vE);
  th1 = atan2(dx1, dy1);
  th2 = atan2(dx2, dy2);
  //  cout<<"angles (vel, 1, 2): "<<thv<<' '<<th1<<' '<<th2<<endl;

  //putting xi,yi at back bumper
  if( sin(thv-th1) < sin(thv-th2)) { //(dx1, dy1) is parallel direction of velocity
    if(cos(thv-th1) > 0) { //they're oriented the same
      //      cout<<"//all is good...(n,e) is at back, (dx1, dy1) is pointed along velocity"<<endl;
    } else {
      //      cout<<" //oriented opposite, so need to flip d1"<<endl;
      xi = xi + dx1;
      yi = yi + dy1;
      dx1 = -dx1;
      dy1 = -dy1;
    }
  } else {
    //    cout<<"//(dx2, dy2) is parallel to velocity...need to switch d1 and d2"<<endl;
    tn = dx1;
    te = dy1;
    if(cos(thv-th2) > 0) { //same orientation
      //      cout<<"correct d2 orientation"<<endl;
      // xi,yi are fine, only need to switch 
      dx1 = dx2;
      dy1 = dy2;
      dx2 = tn;
      dy2 = te;
    } else { //opp orientation
      //      cout<<"need to switch d2 orientation"<<endl;
      xi = xi+dx2;
      yi = yi+dy2;
      dx1 = -dx2;
      dy1 = -dy2;
      dx2 = tn;
      dy2 = te;
    }
  }
  **/
  /**
  newCar->n = xi; //corner coords
  newCar->e = yi;
  newCar->dx1 = dx1; //seg 1 deltas
  newCar->dy1 = dy1;
  newCar->dx2 = dx2; //seg 2 deltas
  newCar->dy2 = dy2;
  **/
  /**
  //putting xi,yi at RIGHT side of back bumper (i.e. th2 and thv+PI/2 are in same dir)
  th2 = atan2(dx2, dy2); //need to recalculate...
  if(cos(thv + PI/2 - th2) > 0) { //they're in same direction
    //everything is set up properly
  } else { //need to switch
    xi = xi+dx2;
    yi = yi+dy2;
    dx2 = -dx2;
    dy2 = -dy2;
  }
  **/
  //WHEW. car is set up correctly
  //  cout<<"fit a car, w/ params: "<<xi<<' '<<yi<<' '<<dx1<<' '<<dy1<<' '<<dy2<<' '<<dy2<<endl;
  /**
  newCar->n = xi; //corner coords
  newCar->e = yi;
  newCar->dx1 = dx1; //seg 1 deltas
  newCar->dy1 = dy1;
  newCar->dx2 = dx2; //seg 2 deltas
  newCar->dy2 = dy2;
  **/


  //debugging - plotting the points, the lines, etc:
  double zpts[numPoints];
  for(int i=0; i< numPoints; i++)
  {
    zpts[i] = 0;
  }
  fprintf(stderr, "first point: %f, %f \n", Xpoints[0], Ypoints[0]);
  fprintf(stderr, "corner: %f, %f, %f, %f, %f, %f\n", xi, yi, dx1, dy1, dx2, dy2);
  //  fitDisplay->sendScan(numPoints, Xpoints, Ypoints, zpts);
  //  fitDisplay->sendLine(seg1.x1,seg1.x2, seg1.y1, seg1.y2);
  myDisplay->sendLine(xi, xi+dx1, yi, yi+dy1, 0);
  myDisplay->sendLine(xi, xi+dx2, yi, yi+dy2, 4);


  return corner;
} //end fitCar

//removes object by putting its index back on openObjs, and removing 
//the index from usedObjs
//TODO: send message to map saying this obj has been removed
void BlobLadar::removeObject(int index) {


  MapElement el;
  vector <int> id;
  id.clear();
  id.push_back(moduleMapID);
  int j = usedObjs.at(index); 
  id.push_back(objects[j].ID);
  el.clear();
  el.set_clear(id);
  int bytesSent = sendMapElement(&el, 0);

  openObjs.push_back(usedObjs.at(index));
  objIterator = usedObjs.begin();
  objIterator+=index;
  usedObjs.erase(objIterator);


}
//removes car by putting it's index on openCars
void BlobLadar::removeCar(int index) {

  MapElement el;
  vector <int> id;
  id.clear();
  id.push_back(moduleMapID);
  int j = usedCars.at(index); 
  id.push_back(cars[j].ID);
  el.clear();
  el.set_clear(id);
  int bytesSent = sendMapElement(&el, 0);


  openCars.push_back(usedCars.at(index));
  carIterator = usedCars.begin();
  carIterator+=index;
  usedCars.erase(carIterator);

}

//FIXME: this gives bad results when line has infinite slope
double BlobLadar::fitLine(double* Xpoints, double* Ypoints, int numPoints, lineSegmentFit* fit) {

  //  cout<<"entering fitLine"<<endl;

  double a,b,MSE; //final vars for least-squares fit
  //fit gives 2 solns, will need to test them
  double a1, a2, b1, b2; 
  //intermediate value
  double B;


  //compute mean e and n coordinates
  double x,y;
  double xerr;
  double ybar, xbar;

  double ssyy = 0;
  double ssxx = 0;
  double ssxy = 0;

  double sumy = 0;
  double sumx = 0;


  for(int i=0;i<numPoints;i++) {
    y = Ypoints[i];
    x = Xpoints[i];

    sumy += y;
    sumx += x;

    ssyy += y*y;
    ssxx += x*x;
    ssxy += y*x;

  }

  ybar = sumy/numPoints; 
  xbar = sumx/numPoints;

  B = .5*((ssxx - numPoints*xbar*xbar)     \
	  -(ssyy - numPoints*ybar*ybar))  \
    /(numPoints*ybar*xbar - ssxy);

  b1 = -B + sqrt(1+B*B);
  a1 = xbar - b1*ybar;

  b2 = -B - sqrt(1+B*B);
  a2 = xbar - b2*ybar;

  //  fitDisplay->sendLine(-4, 4, a1-b1*4, a1+b1*4,5);
  //  fitDisplay->sendLine(-8, 8, a2-b2*8, a2+b2*8,5);

  //now, need to have some measure of the error
  double SE1 = 0;
  double SE2 = 0;
  double err;
  for(int i=0; i<numPoints;i++) 
  {
    y = Ypoints[i];
    x = Xpoints[i];

    xerr = x - a1 - b1*y;
    err = xerr / sqrt(1+b1*b1);
    SE1 += err*err;

    xerr = x - a2 - b2*y;
    err = xerr / sqrt(1+b2*b2);
    SE2 += err*err;
  }

  if(SE1 < SE2)
  {
    a = a1;
    b = b1;
    MSE = SE1/numPoints;
  } else {
    a = a2;
    b = b2;
    MSE = SE2/numPoints;
  }

  //projecting ladar returns onto the line
  double dx1, dx2, dy1, dy2;
  double  newx1, newx2, newy1, newy2;
  double hyp, th, y1, x1, y2, x2;
  y1 = Ypoints[0];
  x1 = Xpoints[0];
  dy1 = y1 - (x1 - a)/b;
  dx1 = x1 - (a +  b*y1);
  //  fprintf(stderr, "x1, y1, dx1, dy1: %f, %f, %f, %f \n", x1, y1, dx1, dy1); 
  th = atan2(dx1, dy1);
  hyp = dy1*sin(th);
  //  fprintf(stderr, "th, and hyp: %f, %f \n", th, hyp);
  newx1 = x1 + hyp*cos(th);
  newy1 = y1 + hyp*sin(th);

  y2 = Ypoints[numPoints-1];
  x2 = Xpoints[numPoints-1];
  dx2 = y2 - (x2 - a)/b;
  dy2 = a +  b*y2 - x2;
  //  fprintf(stderr, "x2, y2, dx2, dy2: %f, %f, %f, %f \n", x2, y2, dx2, dy2); 
  th = atan2(dx2, dy2);
  hyp = dy2*sin(th);
  //  fprintf(stderr, "th, and hyp: %f, %f \n", th, hyp);
  newx2 = x2 + hyp*cos(th);
  newy2 = y2 + hyp*sin(th);

  fit->a = a;
  fit->b = b;
  fit->y1 = newy1;
  fit->x1 = newx1;
  fit->y2 = newy2;
  fit->x2 = newx2;

  return numPoints*MSE;
}


/** 
 *Function: uses the method of least-squares to fit a line of the form b = a + b * e
 * to the input points, given that it must be perpendicular to line w/ slope given
 * Then, transforms that to a line of form [alpha, rho, r1, r2]
 * uses eqns found on mathworld.wolfram.com
 * returns: the r-squared value of the fit, and changes the data in fit
 **/

double BlobLadar::fitLine2(double* Xpoints, double* Ypoints, int numPoints, lineSegmentFit* fit, double slope) {

  double a,b, MSE; //vars for least-squares fit

  //compute mean e and y coordinates
  double eerr, xerr, err;
  double x,y;

  double ybar = 0;
  double xbar = 0;
  for(int i=0;i<numPoints;i++) {
    y = Ypoints[i];
    x = Xpoints[i];

    ybar += y;
    xbar += x;
  }

  ybar = ybar/numPoints; 
  xbar = xbar/numPoints;

  //b must be perpendicular to line described by theta (i.e. parallel to theta)
  b = -1/slope; 
  a = xbar - b*ybar;

  //now, need to have some measure of the error
  double SE = 0;
  for(int i=0; i<numPoints;i++) {
    y = Ypoints[i];
    x = Xpoints[i];
    xerr = x - a - b*y;
    eerr = y - (x-a)/b;
    err = sqrt(xerr*xerr + eerr*eerr);
    SE += err*err;
  }


  //projecting ladar returns onto the line
  double dx1, dx2, dy1, dy2;
  double  newx1, newx2, newy1, newy2;
  double hyp, th, y1, x1, y2, x2;
  y1 = Ypoints[0];
  x1 = Xpoints[0];
  dy1 = y1 - (x1 - a)/b;
  dx1 = a +  b*y1 - x1;
  th = atan2(dx1, dy1);
  hyp = dy1*cos(th);
  //  fprintf(stderr, "two calcs of hyp: %f, %f \n", dy1*cos(th), dx1*sin(th));
  newx1 = x1 + hyp*sin(th);
  newy1 = y1 + hyp*cos(th);

  y2 = Ypoints[numPoints-1];
  x2 = Xpoints[numPoints-1];
  dy2 = y2 - (x2 - a)/b;
  dx2 = a +  b*y2 - x2;
  th = atan2(dx2, dy2);
  hyp = dx2*cos(th);
  //  fprintf(stderr, "two calcs of hyp: %f, %f \n", dy2*cos(th), dy2*sin(th));
  newx2 = x2 + hyp*sin(th);
  newy2 = y2 + hyp*cos(th);

  fit->a = a;
  fit->b = b;
  fit->y1 = newy1;
  fit->x1 = newx1;
  fit->y2 = newy2;
  fit->x2 = newx2;

  MSE = SE/numPoints;
  return SE;
}


void BlobLadar::KFupdate() {

  //  cout<<"entering KFupdate"<<endl;

  Matrix K1;
  K1 = Matrix(2,2);
  Matrix K2;
  K2 = Matrix(2,2);
  Matrix Xerr;
  Xerr = Matrix(1,1);
  Matrix Yerr;
  Yerr = Matrix(1,1);
  Matrix tmp;
  tmp = Matrix(1,1);
  Matrix tmp2;
  tmp2 = Matrix(1,1);
  for(unsigned int i=0; i<usedObjs.size(); i++) {
    int j = usedObjs.at(i);
    //    cout<<"last seen: "<<objects[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(objects[j].timesSeen > 1) {
      if(objects[j].lastSeen == numFrames) {
        double grr[] = {objects[j].X};
        tmp.setelems(grr);
	//	cout<<"Northing: "<<objects[j].N<<" and, predicted northing: "<<objects[j].Xmu_hat.getelem(0,0)<<endl;
        Xerr = tmp - C*objects[j].Xmu_hat;
	//	cout<<"Xerror: "<<Xerr.getelem(0,0)<<' ';
        K1 = objects[j].Xsigma_hat * C.transpose() * (C*objects[j].Xsigma_hat*C.transpose() + Q).inverse();
        objects[j].Xmu = objects[j].Xmu_hat + K1 * Xerr;
        objects[j].Xsigma = (I - K1*C) * objects[j].Xsigma_hat;

        double grr2[] = {objects[j].Y};
        tmp2.setelems(grr2);

        Yerr = tmp2 - C*objects[j].Ymu_hat;
	//	cout<<" and, Yerror: "<<Yerr.getelem(0,0)<<endl;
        K2 = objects[j].Ysigma_hat * C.transpose() * (C*objects[j].Ysigma_hat*C.transpose() + Q).inverse();
        objects[j].Ymu = objects[j].Ymu_hat + K2 * Yerr;
        objects[j].Ysigma = (I - K2*C) * objects[j].Ysigma_hat;
	//	cout<<"new vel (actually calculated) "<<objects[j].Xmu.getelem(1,0)<<' '<<objects[j].Ymu.getelem(1,0)<<endl;

      } else {

	objects[j].Xmu = objects[j].Xmu_hat;
	objects[j].Xsigma = objects[j].Xsigma_hat;
	objects[j].Ymu = objects[j].Ymu_hat;
	objects[j].Ysigma = objects[j].Ysigma_hat;

      }

    }
  }


  for(unsigned int i=0; i<usedCars.size(); i++) {
    //    cout<<"entering loop w/ i = "<<i<<endl;
    int j = usedCars.at(i);
    //    cout<<"last seen: "<<cars[j].lastSeen<<"  and curr frame: "<<numFrames<<endl;
    if(cars[j].timesSeen > 1) {
      if(cars[j].lastSeen == numFrames) {

	double grr[] = {cars[j].X};
	tmp.setelems(grr);

	//	cout<<"line 2239"<<endl;
        Xerr = tmp - C*cars[j].Xmu_hat;
	//	cout<<"Xerr for car: "<<Xerr.getelem(0,0)<<endl;
        K1 = cars[j].Xsigma_hat * C.transpose() * (C*cars[j].Xsigma_hat*C.transpose() + Q).inverse();
	//	cout<<"line 2242"<<endl;
        cars[j].Xmu = cars[j].Xmu_hat + K1 * Xerr;
	//	cout<<"new n,ndot: "<<cars[j].Xmu.getelem(0,0)<<' '<<cars[j].Xmu.getelem(1,0)<<endl;
        cars[j].Xsigma = (I - K1*C) * cars[j].Xsigma_hat;

	//	cout<<"ln 2246"<<endl;
	double grr2[] = {cars[j].Y};
	tmp2.setelems(grr2);

	//	cout<<"ln 2250"<<endl;
        Yerr = tmp2 - C*cars[j].Ymu_hat;
        K2 = cars[j].Ysigma_hat * C.transpose() * (C*cars[j].Ysigma_hat*C.transpose() + Q).inverse();
	//	cout<<"ln 2253"<<endl;
        cars[j].Ymu = cars[j].Ymu_hat + K2 * Yerr;
        cars[j].Ysigma = (I - K2*C) * cars[j].Ysigma_hat;

      } else {
	//	cout<<"ln 2258"<<endl;
	cars[j].Xmu = cars[j].Xmu_hat;
	cars[j].Xsigma = cars[j].Xsigma_hat;
	//	cout<<"ln 2261"<<endl;
	cars[j].Ymu = cars[j].Ymu_hat;
	//	cout<<"ln 2263"<<endl;
	cars[j].Ysigma = cars[j].Ysigma_hat;
	//	cout<<"ln 2264"<<endl;
      }
    }
  }

  //  cout<<"exiting KFupdate"<<endl;
}
 
void BlobLadar::KFpredict() {

  //  cout<<"entering KFpredict()"<<endl;

  for(unsigned int i=0; i<usedObjs.size() ; i++) {
    int j = usedObjs.at(i);

    objects[j].Xmu_hat =    A*objects[j].Xmu;
    objects[j].Xsigma_hat = A*objects[j].Xsigma*A.transpose() + R;
    objects[j].Ymu_hat =    A*objects[j].Ymu;
    objects[j].Ysigma_hat = A*objects[j].Ysigma*A.transpose() + R;

  } //end cycling through numObjects


  //and, objcars

 for(unsigned int i=0; i<usedCars.size(); i++) {
    int j = usedCars.at(i);

    //    cout<<"ln 2300"<<endl;
    cars[j].Xmu_hat =    A*cars[j].Xmu;
    cars[j].Xsigma_hat = A*cars[j].Xsigma*A.transpose() + R;
    //    cout<<"ln2303"<<endl;
    cars[j].Ymu_hat =    A*cars[j].Ymu;
    cars[j].Ysigma_hat = A*cars[j].Ysigma*A.transpose() + R;
 } //end cycling through numCars

} //end KFpredict()


void BlobLadar::cleanObjects()
{
  int j; 
  int diff;
  for(unsigned int i=0; i<usedObjs.size(); i++)
  {
    j = usedObjs.at(i);
    diff = numFrames - objects[j].lastSeen;
    //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
    if(diff > OBJECTTIMEOUT) {
      fprintf(stderr, "tryng to remove object # %d! \n", j);
      removeObject(i);
    }
  }

}

//TODO: add a free-space condition for removing cars
void BlobLadar::cleanCars()
{
  int j; 
  int diff;
  for(unsigned int i=0; i<usedCars.size(); i++)
  {
    j = usedCars.at(i);
    diff = numFrames - cars[j].lastSeen;
    //    fprintf(stderr, "current frame: %d, last seen: %d \n", numFrames, objects[j].lastSeen);
    if(diff > CARTIMEOUT) {
      fprintf(stderr, "tryng to remove car # %d! \n", j);
      removeCar(i);
    }
  }

}


/**
 * this function is called when it is determined that a new object
 * matches an already seen car.
 **/
//#warning "FIXME: this update is very incomplete -- only replaces old data, doesn't merge at all"
//#warning "At this point, simply replaces Car w/ car formed by Points...add in sam's update rules"
void BlobLadar::updateCar(carRepresentation* Car, objectRepresentation* newObj) {

  //  cout<<"entering updateCar"<<endl;
  vector<point2> points(newObj->scans.back());
  double dStart = newObj->dStart;
  double dEnd = newObj->dEnd;
  double dx, dy;
  int numPoints = points.size();

  double xpts[numPoints];
  double ypts[numPoints];
  for(int i=0; i<numPoints; i++) {
    ypts[i] = points.at(i).y;
    xpts[i] = points.at(i).x;
  }
  //  cout<<"points in object: "<<pts[0]<<' '<<pts[1]<<' '<<pts[2*numPoints-2]<<' '<<pts[2*numPoints-1]<<endl;
  //  cout<<"center of object: "<<newObj->N<<' '<<newObj->E<<endl;
  carRepresentation newCar;
  //  cout<<"updating car #"<<Car->ID<<" at "<<Car->n<<' '<<Car->e<<endl;
  fitCar(xpts, ypts, numPoints,&newCar, Car->Xmu.getelem(1,0), Car->Ymu.getelem(1,0)); 

  //  cout<<"Params for old car: "<<Car->n<<' '<<Car->dy1<<' '<<Car->dx1<<' '<<Car->e<<' '<<Car->dy2<<' '<<Car->dy2<<endl;
  //  cout<<"Params for new car: "<<newCar.N<<' '<<newCar.dy1<<' '<<newCar.dx1<<' '<<newCar.e<<' '<<newCar.dy2<<' '<<newCar.dy2<<endl;

  /**
   * depending on how much of the car we see in the old measurement, update
   * the old car (if we see the corner, that carries more information than 
   * seeing only part of a segment. see p. 86-87
   * we determine how much of car we saw by occlusions
   **/
  int sawFront = 0; //whether dStart&dEnd say we actually saw the boundary of car
  int sawBack = 0;

  //need angles to front/back of car (rel to alice), to determine how they 
  //correspond to dStart&dEnd. will be in range of 0-PI (how ladar works)
  double th1, th2;
  //#warning "for here (and elsewhere) will need to add yaw when using real data"
  double foo1, foo2, bar1, bar2;
  foo1 = Car->X - currState.X;
  foo2 = Car->Y - currState.Y;
  bar1 = Car->X + Car->dx1 - currState.X;
  bar2 = Car->Y + Car->dy1 - currState.Y;
  //  cout<<"deltas: "<<foo1<<' '<<foo2<<' '<<bar1<<' '<<bar2<<endl;
  th1 = atan2(Car->X - currState.X, Car->Y - currState.Y);
  th2 = atan2(Car->X + Car->dx1 - currState.X, Car->Y + Car->dy1 - currState.Y);
  //  th1 = atan2(Car->e - currState[1], Car->n - currState[0]);
  //  th2 = atan2(Car->e + Car->dy1 - currState[1], Car->n + Car->dx1 - currState[0]);

  //  cout<<"thetas: "<<th1<<' '<<th2<<endl;
  if(th1 < th2) { // n,e (back bumper) correspond to start, and d1 (front bumper) correspond to end
    //  if(1==1) { // for now, we're guaranteed that back == start
    if(dStart >= 0) { //start was in foreground
      sawBack = 1;
    }
    if(dEnd >= 0) { //end was in foreground
      sawFront = 1;
    }
  } else { // back bumper corresponds to end
    if(dStart >= 0) {
      sawFront = 1;
    }
    if(dEnd >= 0) {
      sawBack = 1;
    }
  }

  //now that we know whether to trust start/end coords, can calculate updates
  if(sawBack == 1) {
    if(sawFront == 1) {
//      cout<<" //have full information on this car, can entirely replace"<<endl;
      //#warning "replacing isn't the best option here...better to update d's to take into account ALL information" double dx, dy;
    dx = newCar.X + newCar.dx1 - Car->X - Car->dx1;
    dy = newCar.Y + newCar.dy1 - Car->Y - Car->dy1;
    Car->X = Car->X + dx;
    Car->Y = Car->Y + dy;
    //#warniNg "for some reason, we think we've seen the back when we havent...thus, only using dyltas here"
    //      Car->n = newCar.n;
    //      Car->e = newCar.e;
      //      Car->dx1 = newCar.dx1;
      //      Car->dy1 = newCar.dy1;
      //      Car->dx2 = newCar.dx2;
      //      Car->dy2 = newCar.dy2;
    } else {
      //    cout<<" //only know back bumper, only update position"<<endl;
      Car->X = newCar.X;
      Car->Y = newCar.Y;
    }
  } else if (sawFront == 1) {
    //    cout<<"//only know front bumper, only update deltas"<<endl;
    double dx, dy;
    dx = newCar.X + newCar.dx1 - Car->X - Car->dx1;
    dy = newCar.Y + newCar.dy1 - Car->Y - Car->dy1;
    Car->X = Car->X + dx;
    Car->Y = Car->Y + dy;
  }

  //if we were able to update car, update history/log of when seen
  if(sawFront + sawBack > 0) {
    //setting up history vector...
    int histptr = Car->histPointer;

    //  cout<<"setting ptr to "<<histptr<<" plus 1"<<endl;
    Car->histPointer = histptr + 1;
    Car->lastY[histptr%10] = Car->Y;
    Car->lastX[histptr%10] = Car->X;

    Car->timesSeen = Car->timesSeen + 1;
    Car->lastSeen = numFrames;
  } else {
    //    cout<<"tried to update car, not enough information"<<endl<<endl;
  }
} //end updateCar

