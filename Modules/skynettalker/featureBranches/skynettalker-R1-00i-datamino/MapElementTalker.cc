/**********************************************************
 **
 **  MAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-02-26 15:01:50 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:05:13 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapElementTalker.hh"
using namespace std;

CMapElementTalker::CMapElementTalker()
{
	skynet_ptr = NULL;

}

int CMapElementTalker::initSendMapElement(	int snkey,
	modulename snname)
{
	if (!skynet_ptr)
		skynet_ptr = new skynet(snname, snkey);

	mapElementSendSocket = this->skynet_ptr->get_send_sock(SNmapElement);
	return(0);
}

int CMapElementTalker::initRecvMapElement(int snkey, modulename snname)
{
	if (!skynet_ptr)
		skynet_ptr = new skynet(snname, snkey);

	mapElementRecvSocket= this->skynet_ptr->listen(SNmapElement,snname);
	return(0);
}

int CMapElementTalker::sendMapElement(MapElement* pMapElement, int subgroup)
{
	if (!skynet_ptr){
		cerr << "in CMapElementTalker - Need to initialize talker" << endl;
		return 0;
	}
	pMapElement->subgroup = subgroup;
	setMapElementMsgOut(pMapElement);
	return(this->skynet_ptr->send_msg(this->mapElementSendSocket,
																 &this->mapElementMsgOut,
																 sizeof(this->mapElementMsgOut),0));
}


int CMapElementTalker::recvMapElementBlock(MapElement* pMapElement, int subgroup)
{

	if (!skynet_ptr){
		cerr << "in CMapElementTalker - Need to initialize talker" << endl;
		return 0;
	}
	int readval = 0;
	while(1){
		readval = this->skynet_ptr->get_msg(this->mapElementRecvSocket,
																		 &this->mapElementMsgIn,
																		 sizeof(this->mapElementMsgIn),0);
		
		if (this->mapElementMsgIn.subgroup == subgroup)
			break;
	}

	setMapElementMsgIn(pMapElement);

	return readval;
}

int CMapElementTalker::recvMapElementNoBlock(MapElement* pMapElement, int subgroup)
{
	if (!skynet_ptr){
		cerr << "in CMapElementTalker - Need to initialize talker" << endl;
		return 0;
	}


	int readval = 0;
	int tmpreadval = 0;
	while(1){
		if (this->skynet_ptr->is_msg(this->mapElementRecvSocket)){
			tmpreadval = this->skynet_ptr->get_msg(this->mapElementRecvSocket,
																				&this->mapElementMsgIn,
																				sizeof(this->mapElementMsgIn),0);
			
			if (this->mapElementMsgIn.subgroup ==subgroup){
				setMapElementMsgIn(pMapElement);
				readval = tmpreadval;
				break;
			}

		} else { // no messages waiting
			break;
		}
	}
	return(readval);
}



int CMapElementTalker::setMapElementMsgOut(MapElement* pMapElement)
{
	int i,j;

	//--------------------------------------------------
	// truncate id if needed
	//--------------------------------------------------
	if (pMapElement->id.size() > MAP_ELEMENT_ID_LENGTH){
		this->mapElementMsgOut.num_ids = MAP_ELEMENT_ID_LENGTH;
		cerr << "in MapElementTalker.cc, Truncating Map Element id length from "
				 << pMapElement->id.size() << " to " << MAP_ELEMENT_ID_LENGTH<< endl;
	} 
	else
		this->mapElementMsgOut.num_ids = pMapElement->id.size();

	for (i = 0; i<mapElementMsgOut.num_ids;++i){
		this->mapElementMsgOut.id[i] = pMapElement->id[i];
	}


	this->mapElementMsgOut.subgroup = pMapElement->subgroup;
	this->mapElementMsgOut.conf = pMapElement->conf;
	this->mapElementMsgOut.type = pMapElement->type;
	this->mapElementMsgOut.type_conf = pMapElement->type_conf;
	this->mapElementMsgOut.center_type = pMapElement->center_type;
this->mapElementMsgOut.center.x = pMapElement->center.x;
	this->mapElementMsgOut.center.y = pMapElement->center.y;
	this->mapElementMsgOut.center.min_var = pMapElement->center.min_var;
	this->mapElementMsgOut.center.max_var = pMapElement->center.max_var;
	this->mapElementMsgOut.center.axis = pMapElement->center.axis;
	
	this->mapElementMsgOut.length = pMapElement->length;
	this->mapElementMsgOut.width = pMapElement->width;
	this->mapElementMsgOut.orientation = pMapElement->orientation;
	this->mapElementMsgOut.length_var = pMapElement->length_var;
	this->mapElementMsgOut.width_var = pMapElement->width_var;
	this->mapElementMsgOut.orientation_var = pMapElement->orientation_var;


	this->mapElementMsgOut.geometry_type = pMapElement->geometry_type;
	//--------------------------------------------------
	// truncate geometry if needed
	//--------------------------------------------------
	if (pMapElement->geometry.size() > MAP_ELEMENT_NUMPTS){
		this->mapElementMsgOut.num_pts = MAP_ELEMENT_NUMPTS;
		cerr << "in MapElementTalker.cc, Truncating Map Element geometry vector length from "
				 << pMapElement->geometry.size() << " to " << MAP_ELEMENT_NUMPTS<< endl;
	} 
	else
		this->mapElementMsgOut.num_pts = pMapElement->geometry.size();

	for (i = 0; i<mapElementMsgOut.num_pts;++i){
		this->mapElementMsgOut.geometry[i].x = pMapElement->geometry[i].x;
		this->mapElementMsgOut.geometry[i].y = pMapElement->geometry[i].y;
		this->mapElementMsgOut.geometry[i].max_var = pMapElement->geometry[i].max_var;
		this->mapElementMsgOut.geometry[i].min_var = pMapElement->geometry[i].min_var;
		this->mapElementMsgOut.geometry[i].axis = pMapElement->geometry[i].axis;

	}
	this->mapElementMsgOut.height = pMapElement->height;
	this->mapElementMsgOut.height_var = pMapElement->height_var;

	this->mapElementMsgOut.velocity.x = pMapElement->velocity.x;
	this->mapElementMsgOut.velocity.y = pMapElement->velocity.y;
	this->mapElementMsgOut.velocity.max_var = pMapElement->velocity.max_var;
	this->mapElementMsgOut.velocity.min_var = pMapElement->velocity.min_var;
	this->mapElementMsgOut.velocity.axis = pMapElement->velocity.axis;
		 
	this->mapElementMsgOut.frame_type = pMapElement->frame_type;
	this->mapElementMsgOut.state = pMapElement->state;


	//--------------------------------------------------
	// truncate label if needed
	//--------------------------------------------------
	if (pMapElement->label.size() > MAP_ELEMENT_LABEL_NUMLINES){
		this->mapElementMsgOut.num_lines = MAP_ELEMENT_LABEL_NUMLINES;
		cerr << "in MapElementTalker.cc, Truncating Map Element label vector length from "
				 << pMapElement->label.size() << " to " 
				 << MAP_ELEMENT_LABEL_NUMLINES<< endl;
	} 
	else
		this->mapElementMsgOut.num_lines = pMapElement->label.size();

	///cout << "label size = " << pMapElement->label.size() << endl;
	for (i = 0; i<mapElementMsgOut.num_lines;++i){
		///cout << " out line " << i << " = " << pMapElement->label[i] << endl;
		this->mapElementMsgOut.label[i][MAP_ELEMENT_LABEL_LENGTH-1] = '\0';
		for (j = 0; j<(MAP_ELEMENT_LABEL_LENGTH-1); ++j){
			if (j>=(int)pMapElement->label[i].size()){
				this->mapElementMsgOut.label[i][j] = '\0';
				break;
			}
			this->mapElementMsgOut.label[i][j] = pMapElement->label[i][j];
		}
		
	}
		
	return 0;
}

int CMapElementTalker::setMapElementMsgIn(MapElement* pMapElement)
{
	pMapElement->id.clear();
	for (int i = 0; i<mapElementMsgIn.num_ids;++i){
		//cout << i;
		pMapElement->id.push_back(this->mapElementMsgIn.id[i]);
	}

	pMapElement->subgroup = this->mapElementMsgIn.subgroup;
	pMapElement->conf = this->mapElementMsgIn.conf;
	pMapElement->type = this->mapElementMsgIn.type;
	pMapElement->type_conf = this->mapElementMsgIn.type_conf;

	pMapElement->center_type = this->mapElementMsgIn.center_type;
	pMapElement->center.x = this->mapElementMsgIn.center.x;
	pMapElement->center.y = this->mapElementMsgIn.center.y;
	pMapElement->center.max_var = this->mapElementMsgIn.center.max_var;
	pMapElement->center.min_var = this->mapElementMsgIn.center.min_var;
	pMapElement->center.axis = this->mapElementMsgIn.center.axis;
	
	pMapElement->length = this->mapElementMsgIn.length;
	pMapElement->width = this->mapElementMsgIn.width;
	pMapElement->orientation = this->mapElementMsgIn.orientation;
	pMapElement->length_var = this->mapElementMsgIn.length_var;
	pMapElement->width_var = this->mapElementMsgIn.width_var;
	pMapElement->orientation_var = this->mapElementMsgIn.orientation_var;

	pMapElement->geometry_type = this->mapElementMsgIn.geometry_type;
	pMapElement->geometry.clear();
	point2_uncertain tmppt;
	for (int i = 0; i<mapElementMsgIn.num_pts;++i){
		tmppt.x = this->mapElementMsgIn.geometry[i].x;
		tmppt.y = this->mapElementMsgIn.geometry[i].y;
		tmppt.max_var = this->mapElementMsgIn.geometry[i].max_var;
		tmppt.min_var = this->mapElementMsgIn.geometry[i].min_var;
		tmppt.axis = this->mapElementMsgIn.geometry[i].axis;

		pMapElement->geometry.push_back(tmppt);
	}

	pMapElement->height = this->mapElementMsgIn.height;
	pMapElement->height_var = this->mapElementMsgIn.height_var;

	pMapElement->velocity.x = this->mapElementMsgIn.velocity.x;
	pMapElement->velocity.y = this->mapElementMsgIn.velocity.y;
	pMapElement->velocity.max_var = this->mapElementMsgIn.velocity.max_var;
	pMapElement->velocity.min_var = this->mapElementMsgIn.velocity.min_var;
	pMapElement->velocity.axis = this->mapElementMsgIn.velocity.axis;
		 
	pMapElement->frame_type = this->mapElementMsgIn.frame_type;
	pMapElement->state = this->mapElementMsgIn.state;

	pMapElement->label.clear();
	///cout << "label recv " << mapElementMsgIn.num_lines <<endl;
	//	string tmprecv ;
	for (int i = 0; i<mapElementMsgIn.num_lines;++i){
		///cout << " in line " << i << " = " << this->mapElementMsgIn.label[i] << endl;
		//tmprecv = this->mapElementMsgIn.label[i];
		//pMapElement->label.push_back(tmprecv);
		pMapElement->label.push_back(this->mapElementMsgIn.label[i]);
	}

	return 0;
}
