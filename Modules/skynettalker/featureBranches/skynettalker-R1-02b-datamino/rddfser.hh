/*!**
 * Daniele Tamino (using code from Nok's RDDFTalker)
 * Boost serialization routines for the RDDF class. This is just a proof-of-concept,
 * this code is bad and must be rewritten to be used in real programs.
 * February 7, 2007
 */

// C++ STL headers
// Note: the classes i/ostrstream are deprecated, but provides a functionality that's
// not available in the new i/ostringstream classes!!! IMHO, it should't have been
// deprecated in the first place, but ISO has more decision power than me ... --datamino
#include <strstream>
// boost library headers
#include <boost/serialization/split_free.hpp>

// DGC headers
#include "dgcutils/RDDF.hh"

/** The maximum size of the data that can be sent across skynet for 
 * sending an RDDF.  Units of bytes. */
#define RDDF_DATA_PACKAGE_SIZE 10000

bool loadRDDFStream(istrstream& inputstream, RDDF* pRddf);

namespace boost {
    namespace serialization {

        template<class Archive>
        void load(Archive & ar, RDDF & rddf, unsigned int /*version*/)
        {
            char * buf = new char[RDDF_DATA_PACKAGE_SIZE];
            ar >> make_binary_object(buf, RDDF_DATA_PACKAGE_SIZE);
            
            istrstream recvstream(buf, RDDF_DATA_PACKAGE_SIZE);
            loadRDDFStream(recvstream, &rddf);
            
            //printf("[%s:%d] \n", __FILE__, __LINE__);
            //cout << recvstream.str() << endl;
            
        }

        template<class Archive>
        void save(Archive & ar, const RDDF & rddf, unsigned int /*version*/)
        {
            char * buf = new char[RDDF_DATA_PACKAGE_SIZE];
            // Initialize
            char tmp[100] = "\0";
            buf[0] = '\0';
            RDDFVector wp = rddf.getTargetPoints();
            
            // Print out the RDDF so we can see what is going on
            for (int i=0; i<rddf.getNumTargetPoints(); i++)
            {
                /*
                  sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
                  wp[i].number, wp[i].latitude, wp[i].longitude, 
                  wp[i].radius/METERS_PER_FOOT, wp[i].maxSpeed/MPS_PER_MPH);
                */  
                
                //Note that radius is in meters and maxSpeed is in MPH
                sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
                        wp[i].number, wp[i].latitude, wp[i].longitude, 
                        wp[i].radius, wp[i].maxSpeed);
                
                strncat(buf, tmp, strlen(tmp));
                
                //cout << tmp;
            }
            
            //printf("\n[%s:%d] \n", __FILE__, __LINE__);
            //cout << buf;
            
            ar << make_binary_object(buf, RDDF_DATA_PACKAGE_SIZE);
        }
    }
}

BOOST_SERIALIZATION_SPLIT_FREE(RDDF)
BOOST_CLASS_VERSION(RDDF, 1)
