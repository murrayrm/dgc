/*!**
 * Daniele Tamino
 * February 6, 2007
 */


#include <getopt.h>
// C++ STL headers
#include <iostream>

// local headers
#include "SkynetTalker.hh"
#include "dgcutils/RDDF.hh"
#include "dgcutils/ggis.h"
#include "rddfser.hh"

class SkynetTalkerTestRecv
{
    SkynetTalker<RDDF> talker;
    RDDF m_receivedRddf;
    
public:
    SkynetTalkerTestRecv(int sn_key) :
        talker(sn_key, SNrddf, MODrddftalkertestrecv, true)
    {
        cout << " listening!" << endl;
        while(true)
	{
            if (talker.hasNewMessage())
            {
                RDDF rddf;
                cout << '[' <<  __FILE__ << ':' << __LINE__ << ']' << endl;
                cout << "about to receive an rddf...\n";
                talker.receive(&rddf);
                cout << " received an rddf! here it is:" << endl;
                rddf.print();
                cout << " done!" << endl;
            } else {
                cout << "waiting ... " << endl;
                talker.waitForMessage();
            }
        }
    }
};

int main(int argc, char *argv[])
{
	int key = (argc > 1) ? atoi(argv[1]) : 0;
        cout << "about to listen...";
	SkynetTalkerTestRecv test(key);
        return 0;
}
