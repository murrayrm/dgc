/**
 * Daniele Tamino, Vanessa Carson
 * February 2, 2007
 */

#ifndef SKYNETTALKER_HH_
#define SKYNETTALKER_HH_

// system includes
#include <pthread.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <sstream>
// boost serialization includes
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
// boost iostream includes
#include <boost/iostreams/stream.hpp>
// local (dgc) includes
#include "skynet/skynet.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

// check that noone else is using a DBG or MSG macro
#ifdef DBG
#error "Sorry, this file #defines DBG too (and #undefs it at the end)!"
#error "please #undef DBG it if not needed afterwards, or #define it after including this file"
#endif
#ifdef MSG
#error "Sorry, this file #defines MSG too (and #undefs it at the end)!"
#error "please #undef MSG if not needed afterwards, or #define it after including this file"
#endif

#ifndef NDEBUG
#define NDEBUG 1 // set to 1 to disable debugging messages
#endif
#if !NDEBUG
#  define DBG(arg) (cerr << "DBG: " << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
#else
#  define DBG(arg)
#endif
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)


namespace skynettalker {

    // creates a boost::iostream Source and Sink to read/write to a buffer
    // from within a stream
    namespace boostio = boost::iostreams;

    /* Use boost::iostreams library to create two new standard-compatible iostreams, to read
       and write to a memory buffer, and resize it when it's too small.
       See the tutorial: http://www.boost.org/libs/iostreams/doc/index.html
   
       NOTE: std::istrstream and std::ostrstream offer a similar functionality, but are deprecated
       in favor of istringstream and ostringstream, and these lack the feature of streaming to and
       from a memory buffer, instead of a std::string.
    */

    /*! This 'sink' class writes to the specified memory buffer, and resizes it up when it's
     * too small. This is later used to instantiate a boost::iostreams::stream<SkynetBufferSink>
     * that is, a stream compatible with the standard library, that behaves exactly like e.g.
     * cout, cerr or ofstream.
     */
    class SkynetBufferSource {
	// don't use references, e.g. "char*& m_buffer; int& m_size" boost don't like them
	// (and don't ask me why, I've warned you, ask boost developers)
	// maybe as something to do with the auto-generated copy constructor
	char** m_buffer;
	int* m_size;
	int m_pos;
	//pthread_mutex_t& m_mtx;
    public:
	typedef boostio::source_tag category; // tell boost this is a source
	typedef char char_type; // tell boost the "char" we use
    
	SkynetBufferSource(char** buffer, int* size/*, pthread_mutex_t mtx*/) 
	    : m_buffer(buffer), m_size(size), m_pos(0)/*, m_mtx(mtx)*/
	{ }
    
	/*! Reads n bytes from 's'.
	 * \param s The buffer with the data to write.
	 * \param n The number of bytes to copy.
	 * see http://www.boost.org/libs/iostreams/doc/tutorial/container_source.html
	 */
	streamsize read(char_type* s, streamsize n);
    
	/*! \return The current position in the buffer, e.g. how many bytes were read so far.
	 */
	int getPos() const {
	    return m_pos;
	}
    };

    /*! This 'source' class reads from the specified memory buffer. This is later used to instantiate
     * a boost::iostreams::stream<SkynetBufferSink>, that is, a stream compatible with the standard
     * library, that behaves exactly like e.g. cin or ifstream.
     */
    class SkynetBufferSink {
	// don't use references, e.g. "char*& m_buffer; int& m_size" boost don't like them
	// (and don't ask me why, I've warned you, ask boost developers)
	// maybe as something to do with the auto-generated copy constructor
	char** m_buffer;
	int* m_size;
	int m_pos;
	//pthread_mutex_t& m_mtx;
    public:
	typedef boostio::sink_tag category;
	typedef char char_type;

	/*! Create an object using the specified buffer and size.
	 * \param buffer Pointer to the buffer address.
	 * \param size Pointer to the buffer size.
	 */
	SkynetBufferSink(char** buffer, int* size/*, pthread_mutex_t mtx*/) 
	    : m_buffer(buffer), m_size(size), m_pos(0)/*, m_mtx(mtx)*/
	{ }

	/*! Writes n bytes into 's'.
	 * \param s The buffer where the data has to be copied.
	 * \param n The number of bytes to copy.
	 * see http://www.boost.org/libs/iostreams/doc/tutorial/container_sink.html
	 */
	streamsize write(const char_type* s, streamsize n);

	/*! \return The current position in the buffer, e.g. how many bytes were written so far.
	 */
	int getPos() const {
	    return m_pos;
	}

    };

    /*! This class is used to serialize messages (using boost::serialize) and then send them
     * via skynet to everyone who's listening to that particular type of message.
     * It's a helper to make skynet more Object Oriented, and to ease writing modules that
     * uses it.
     */
    template<typename Message,
	     typename IAr = boost::archive::binary_iarchive,
	     typename OAr = boost::archive::binary_oarchive>
    class SkynetTalker
    {

    public:
	/*! Constructor.
	 * \param snkey Skynet key to use.
	 * \param type Type of the messages to be sent or received.
	 * \param modulename The name of this module.
	 */
	SkynetTalker(int snkey, sn_msg type, modulename name);
	~SkynetTalker();
  
	/*! Send a message through skynet.
	 * \param message The message object that we want to send.
	 * \param pMutex If not NULL, this mutex will be locked before sending and unlocked
	 * after all the data has been sent succesfully.
	 * \return true if everything went well, false otherwise
	 */
	bool send(const Message* message, pthread_mutex_t* pMutex = NULL);
	/*! Receive a message from skynet. If no message is available, this method blocks
	 * and waits for a message (like waitForMessages()).
	 * \param pMutex If not NULL, this is locked while reading the message and unlocked
	 * after that (but it's not locked while waiting for messages).
	 */
	bool receive(Message* msgReceive, pthread_mutex_t* pMutex = NULL);

	/*! Checks if there is a new message available
	 * \returns true if a message is available, false otherwise.
	 */
	bool hasNewMessage();

	/*! Wait until a message is available.
	 */
	void waitForMessage();

    private:

	skynet m_skynet;

	pthread_mutex_t m_dataBufferMutex;

	int m_bufSize; // size in bytes of m_buffer
	char * m_buffer; // buffer used to send and receive messages
	int m_sendsock;
	int m_recvsock; 

	modulename m_modname;
	sn_msg m_msgtype;

	// resize m_buffer to the given size, or allocate if == NULL. If size == 0,
	// free buffer and set m_buffer = NULL.
	void resizeBuffer(int size);
  
	int getRecvSock();
	int getSendSock();
    };

    /** Resize a buffer. 'buffer' is a pointer to the buffer, 'size' a pointer to its size (these two are both
     * input and output parameters).
     * @param buffer: Pointer to the buffer to be resized
     * @param size: pointer to the current size
     * @param newSize: size of the new buffer to be allocated.
     * @param length: length of content currently in buffer (<= *size). This number of
     * bytes will be copied to the new buffer.
     */
    void resizeBuffer(char** buffer, int* size, int newSize, int length = 0);

    //////////////////////////////////////////////////////////////////////////////////
    /// IMPLEMENTATION OF CLASS SkynetTalker, in the .hh file, because it's a template
    /// Note to self: a lot of this code could actually be moved to the .cc file with
    /// some little tricks, if needed.
    //////////////////////////////////////////////////////////////////////////////////

    template<typename Message, typename IAr, typename OAr>
    SkynetTalker<Message, IAr, OAr>::SkynetTalker(int snkey, sn_msg type, modulename name)
	: m_skynet(name, snkey/*, status*/), 
	  m_bufSize(0),
	  m_buffer(NULL),
	  m_sendsock(-1),
	  m_recvsock(-1),
	  m_modname(name),
	  m_msgtype(type)
    {
	DGCcreateMutex(&m_dataBufferMutex);
	// m_buffer, m_bufSize, m_sendsock, m_recvsock will be initialized the first time
	// they are used.
    }

    template<typename Message, typename IAr, typename OAr>
    SkynetTalker<Message, IAr, OAr>::~SkynetTalker()
    {
	resizeBuffer(0);
	DGCdeleteMutex(&m_dataBufferMutex);
    }


    template<typename Message, typename IAr, typename OAr>
    int SkynetTalker<Message, IAr, OAr>::getRecvSock()
    {
	if (m_recvsock < 0) {
	    DBG("SkynetTalker::getRecvSock(): opening receive socket (listening)");
	    m_recvsock = m_skynet.listen(m_msgtype, m_modname);
	    if (m_recvsock < 0) {
		MSG("SkynetTalker::getRecvSock(): *** error opening receive socket");
	    }
	}
	return m_recvsock;
    }

    template<typename Message, typename IAr, typename OAr>
    int SkynetTalker<Message, IAr, OAr>::getSendSock()
    {
	if (m_sendsock < 0) {
	    DBG("SkynetTalker::getSendSock(): opening send socket");
	    m_sendsock = m_skynet.get_send_sock(m_msgtype);
	    if (m_sendsock < 0) { // at the moment, this never happens
		MSG("SkynetTalker::getSendSock(): *** error opening send socket");
	    }
	}
	return m_sendsock;
    }

    template<typename Message, typename IAr, typename OAr>
    void SkynetTalker<Message, IAr, OAr>::resizeBuffer(int size)
    {
	skynettalker::resizeBuffer(&m_buffer, &m_bufSize, size);
    }


    template<typename Message, typename IAr, typename OAr>
    bool SkynetTalker<Message, IAr, OAr>::send(const Message* msg, pthread_mutex_t* pMutex)
    {
	int bytesToSend;
	int bytesSent;
	bool ret = true;

	DBG("SkynetTalker::send() BEGIN");


	DGClockMutex(&m_dataBufferMutex);
 
	if(pMutex != NULL) 
	    {
		DGClockMutex(pMutex);
	    }

	// Serialize the message to be sent 
	try {
	    SkynetBufferSink sink(&m_buffer, &m_bufSize);
	    boostio::stream<SkynetBufferSink> os(sink);
	    OAr ar(os);
	    ar << *msg;
	    os.flush(); // without this not all data is written in the buffer!
	    bytesToSend = os->getPos();
	    DBG("SkynetTalker::send(): sending a " << bytesToSend << " bytes message");

	    bytesSent = m_skynet.send_msg(getSendSock(), const_cast<char*>(m_buffer), bytesToSend, 0);

	    if(bytesSent != bytesToSend)
		{
		    cerr << "SkynetTalker::send() sent " << bytesSent
			 << " bytes while expected to send " << bytesToSend << " bytes" << endl;
		    ret = false;
		}
	} catch( ... ) {
	    // FIXME: catch the correct type of exception and print a more detailed error message
	    // Serialization is the only thing that can throw, anyway
	    MSG("*** exception caught serializing message ***");
	    ret = false;
	}


	DGCunlockMutex(&m_dataBufferMutex);
	if(pMutex != NULL)
	    DGCunlockMutex(pMutex);

	DBG("SkynetTalker::send() END returning " << ret);

	return true;
    }


    template<typename Message, typename IAr, typename OAr>
    bool SkynetTalker<Message, IAr, OAr>::receive(Message* msgReceive, pthread_mutex_t* pMutex)
    {
	int bytesReceived;
	bool ret = true;

	DBG("SkynetTalker::receive(): BEGIN");

	int recvsock = getRecvSock();
	if (recvsock < 0)
	    {
		return false;
	    }

	if (!hasNewMessage()) {
	    DBG("SkynetTalker::receive(): no new message, waiting");
	    waitForMessage();
	}
  
	// Build the mutex list. We want to protect the data buffer
	int numMutices = 1;
	pthread_mutex_t* ppMutices[2];
	ppMutices[0] = &m_dataBufferMutex;
	if(pMutex != NULL)
	    {
		ppMutices[1] = pMutex;
		numMutices++;
	    }

	int requiredSize = m_skynet.get_msg_size(recvsock);
  
	DBG("SkynetTalker::receive(): skynet::get_msg_size() returned " << requiredSize);
	if (requiredSize <= 0)
	    {
		// we know there should be a message available, unless another thread read it between
		// the hasNewMessage() call and here.
		// TODO: better synchronize this method
		DBG("SkynetTalker::receive(): returning false. END");
		return false;
	    }

	DBG("SkynetTalker::receive(): m_bufSize = " << m_bufSize);
	if (requiredSize > m_bufSize)
	    {
		resizeBuffer(requiredSize);
	    }
  
	bytesReceived = m_skynet.get_msg(recvsock, m_buffer, m_bufSize,
					 0, ppMutices, false, numMutices);

	try {
	    SkynetBufferSource src(&m_buffer, &m_bufSize);
	    boostio::stream<SkynetBufferSource> is(src);
	    IAr ar(is);
	    ar >> *msgReceive;
	} catch ( ...) {
	    // FIXME: catch the correct type of exception and print a more detailed error message
	    MSG("*** exception caught deserializing message ***");
	    ret = false;
	}
	if(pMutex != NULL)
	    DGCunlockMutex(pMutex);
	DGCunlockMutex(&m_dataBufferMutex);
  
	DBG(" SkynetTalker::receive() END returning " << ret);
  
	return ret;
    }

    template<typename Message, typename IAr, typename OAr>
    bool SkynetTalker<Message, IAr, OAr>::hasNewMessage()
    {
	int recvsock = getRecvSock();
	if (recvsock < 0)
	    {
		return false;
	    }
	return m_skynet.is_msg(recvsock);
    }

    template<typename Message, typename IAr, typename OAr>
    void SkynetTalker<Message, IAr, OAr>::waitForMessage()
    {
	int recvsock = getRecvSock();
	if (recvsock >= 0)
	    {
		m_skynet.sn_select(recvsock);
	    }
    }

} // end namespace skynettalker

// current code assume that SkynetTalker is in the global namespace
// so this line does the trick. Eventually, could be removed.
using skynettalker::SkynetTalker;

// try to avoid causing problems to other people trying to use this names
// for their own purposes
#undef DBG
#undef MSG

#endif //  SKYNETTALKER_HH_
