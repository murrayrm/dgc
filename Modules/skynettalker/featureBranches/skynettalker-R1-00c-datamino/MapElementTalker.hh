/**********************************************************
 **
 **  CMAPELEMENTTALKER.HH
 **
 **    Time-stamp: <2007-02-08 18:16:50 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:05:08 2007
 **
 **
 **********************************************************
 **
 **  Implements a talker for a map element
 **
 **********************************************************/


#ifndef MAPELEMENTTALKER_HH
#define MAPELEMENTTALKER_HH

#include <iostream>
#include "skynet/SkynetContainer.hh"
#include "interfaces/MapElement.hh"
#include "interfaces/MapElementMsg.h"
using namespace std;
//! Implements a talker for a map element
class CMapElementTalker : virtual public CSkynetContainer
{
	
public:
	
	//! A Constructor 
	CMapElementTalker() {}
	
	//! A Destructor 
	~CMapElementTalker() {}

	int initSendMapElement();
	int initRecvMapElement();

	int sendMapElement(MapElement* pMapElement);
		
	int recvMapElementBlock(MapElement* pMapElement);
	int recvMapElementNoBlock(MapElement* pMapElement);

private:
	int setMapElementMsgOut(MapElement* pMapElement);	
	int setMapElementMsgIn(MapElement* pMapElement);	
	
	int mapElementSendSocket;
	MapElementMsg mapElementMsgOut;

	int mapElementRecvSocket;
	MapElementMsg mapElementMsgIn;

};
#endif
