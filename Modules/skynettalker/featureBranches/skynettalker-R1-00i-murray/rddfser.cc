/*!**
 * Daniele Tamino (using code from Nok's RDDFTalker)
 * Boost serialization routines for the RDDF class. This is just a proof-of-concept,
 * this code is bad and must be rewritten to be used in real programs.
 * February 7, 2007
 */

// standard C++ headers
#include <strstream>

// boost library headers
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/split_free.hpp>
#include <boost/serialization/binary_object.hpp>

// DGC headers
#include "dgcutils/RDDF.hh"
#include "dgcutils/ggis.h"

#include "rddfser.hh"

/** The maximum size of the data that can be sent across skynet for 
 * sending an RDDF.  Units of bytes. */
#define RDDF_DATA_PACKAGE_SIZE 10000

using namespace boost::serialization;

#define MAX_SKYNET_RDDF_WAYPOINTS 100

bool loadRDDFStream(istrstream& inputstream, RDDF* pRddf)
{
  string line;
  RDDFData eachWaypoint;
  bool on_first_line = true;

  if(!inputstream)
  {
    cerr << "RDDF couldn't read from a stream\n";
    return false;
  }

  //printf("[%s:%d] ", __FILE__, __LINE__);
  //cout << "inputstream.str() = " << endl;
  //cout << inputstream.str();

  GisCoordLatLon latlon;
  GisCoordUTM utm;

  for(int i=0; i<MAX_SKYNET_RDDF_WAYPOINTS; i++)
  {
    // Number
    getline(inputstream,line,RDDF_DELIMITER);    
    
    // First input on a line should be checked for empty lines.
    if(line.empty()) 
    {
      cout << "crap!  the line is empty\n" ; 
      continue;
    }

    // NOTE: Waypoints are numbered starting at 0 for RDDFs 
    // that are sent across skynet.
    eachWaypoint.number = atoi(line.c_str());
	
    if(pRddf->getNumTargetPoints() > eachWaypoint.number)
    {
      break;
    }
    
    on_first_line = false;

    // Latitude
    getline(inputstream,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(inputstream,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL))
    {
      cerr << "Error converting coordinate, numTargetPoints = " 
	   << pRddf->getNumTargetPoints() << " eachWaypoint.number = " 
	   << eachWaypoint.number << " to utm\n";
      cout << latlon.latitude << " " << latlon.longitude << endl;
      return false;
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Preserve the location in original coordinates.    
    eachWaypoint.latitude = latlon.latitude;
    eachWaypoint.longitude = latlon.longitude;

    // Boundary offset and radius
    getline(inputstream,line,RDDF_DELIMITER);
    eachWaypoint.radius = atof(line.c_str()); //*METERS_PER_FOOT -- comment this out because the radius is already in meters for RDDFs that are sent across skynet.

    // Speed limit
    getline(inputstream,line,'\n');
    eachWaypoint.maxSpeed = atof(line.c_str()); //*MPS_PER_MPH -- comment this out because the radius is already in meters for RDDFs that are sent across skynet.

    pRddf->addDataPoint(eachWaypoint);
  }

  //printf("[%s:%d] \n", __FILE__, __LINE__);
  //print();

  // debug
  // cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  
  return true;
}
