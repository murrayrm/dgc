/*!**
 * Daniele Tamino
 * Vanessa Carson
 * December 8, 2006
 */

#include "SkynetTalker.hh"
#include "dgcutils/DGCutils.hh"
#include <iostream>
#include <sstream>

#include <boost/iostreams/stream.hpp>

using namespace std;

#define NDEBUG 1 // comment this to enable debug messages
#ifndef NDEBUG
#  define DBG(arg) (cerr << "DBG: " << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
#else
#  define DBG(arg)
#endif
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)

// creates a boost::iostream Source and Sink to read/write to a buffer
// from within a stream
namespace boostio = boost::iostreams;

namespace { // anonymous namespace -- do not export this classes outside

    /* Resize a buffer. 'buffer' is a pointer to the buffer, 'size' a pointer to its size (these two are both
       input and output parameters).
       newSize: size of the new buffer to be allocated.
       length: length of content currently in buffer (<= *size). This number of
       bytes will be copied to the new buffer.
     */
    void resizeBuffer(char** buffer, int* size, int newSize, int length = 0)
    {
        DBG("resizeBuffer(): BEGIN resizing buffer (" << *size << " -> " << newSize << ")");
        char* newBuf = NULL;
        if (newSize != 0)
        {
            newBuf = new char[newSize];
            if (newBuf == NULL)
            {
                    cerr << "OUT OF MEMORY!!!!!" << endl;
                    exit(-1); // uhmmmm .. not a graceful exit ...
            }
            else
            {
                if (*buffer != NULL)
                {
                    int numCopy = min(length, newSize);
                    if (numCopy > 0)
                    {
                        memcpy(newBuf, *buffer, numCopy);
                    }
                    delete[] *buffer;
                }
                *buffer = newBuf;
                *size = newSize;
            }
        }
        DBG("resizeBuffer(): END");
    }

    /* Use boost::iostreams library to create two new standard-compatible iostreams, to read
       and write to a memory buffer, and resize it when it's too small.
       See the tutorial: http://www.boost.org/libs/iostreams/doc/index.html
    
       NOTE: std::istrstream and std::ostrstream offer a similar functionality, but are deprecated
       in favor of istringstream and ostringstream, and these lack the feature of streaming to and
       from a memory buffer, instead of a std::string.
    */
    
    /*! This 'sink' class writes to the specified memory buffer, and resizes it up when it's
     * too small. This is later used to instantiate a boost::iostreams::stream<SkynetBufferSink>
     * that is, a stream compatible with the standard library, that behaves exactly like e.g.
     * cout, cerr or ofstream.
     */
    class SkynetBufferSource {
        // don't use references, e.g. "char*& m_buffer; int& m_size" boost don't like them
        // (and don't ask me why, I've warned you, ask boost developers)
        // maybe as something to do with the auto-generated copy constructor
        char** m_buffer;
        int* m_size;
        int m_pos;
        //pthread_mutex_t& m_mtx;
    public:
        typedef boostio::source_tag category; // tell boost this is a source
        typedef char char_type; // tell boost the "char" we use

        SkynetBufferSource(char** buffer, int* size/*, pthread_mutex_t mtx*/) 
            : m_buffer(buffer), m_size(size), m_pos(0)/*, m_mtx(mtx)*/
            { }

        /*! Reads n bytes from 's'.
         * \param s The buffer with the data to write.
         * \param n The number of bytes to copy.
         * see http://www.boost.org/libs/iostreams/doc/tutorial/container_source.html
         */
        streamsize read(char_type* s, streamsize n)
        {
            DBG("SkynetBufferSource::read(): BEGIN");
            streamsize max = static_cast<streamsize>(*m_size - m_pos);
            streamsize result = min(n, max);
            if (result != 0) {
                //pthread_mutex_lock(&m_mtx);
                memcpy(s, *m_buffer + m_pos, result);
                //pthread_mutex_unlock(&m_mtx);
                m_pos += result;
            } else {
                result = -1; // EOF
            }
            DBG("SkynetBufferSource::read(): END returning " << result);
            return result;
        }

        /*! \return The current position in the buffer, e.g. how many bytes were read so far.
         */
        int getPos() const {
            return m_pos;
        }
    };

    /*! This 'source' class reads from the specified memory buffer. This is later used to instantiate
     * a boost::iostreams::stream<SkynetBufferSink>, that is, a stream compatible with the standard
     * library, that behaves exactly like e.g. cin or ifstream.
     */
    class SkynetBufferSink {
        // don't use references, e.g. "char*& m_buffer; int& m_size" boost don't like them
        // (and don't ask me why, I've warned you, ask boost developers)
        // maybe as something to do with the auto-generated copy constructor
        char** m_buffer;
        int* m_size;
        int m_pos;
        //pthread_mutex_t& m_mtx;
    public:
        typedef boostio::sink_tag category;
        typedef char char_type;

        /*! Create an object using the specified buffer and size.
         * \param buffer Pointer to the buffer address.
         * \param size Pointer to the buffer size.
         */
        SkynetBufferSink(char** buffer, int* size/*, pthread_mutex_t mtx*/) 
            : m_buffer(buffer), m_size(size), m_pos(0)/*, m_mtx(mtx)*/
            { }

        /*! Writes n bytes into 's'.
         * \param s The buffer where the data has to be copied.
         * \param n The number of bytes to copy.
         * see http://www.boost.org/libs/iostreams/doc/tutorial/container_sink.html
         */
        streamsize write(const char_type* s, streamsize n)
        {
            DBG("SkynetBufferSink::write(): BEGIN");
            //pthread_mutex_lock(&m_mtx);
            // check if the buffer is big enough
            if (m_pos + n > *m_size) {
                int newSize = m_pos + n;
                resizeBuffer(m_buffer, m_size, newSize, m_pos);
            }            
            memcpy(*m_buffer + m_pos, s, n);
            //pthread_mutex_unlock(&m_mtx);
            m_pos += n;
            DBG("SkynetBufferSink::write(): END returning " << n);
            return n;
        }

        /*! \return The current position in the buffer, e.g. how many bytes were written so far.
         */
        int getPos() const {
            return m_pos;
        }

    };

} // end anonymous namespace


/*****************************************************************************\
 *                                                                            *
 * class SkynetTalker                                                         *
 *                                                                            *
\*****************************************************************************/

template<typename Message, typename IAr, typename OAr>
SkynetTalker<Message, IAr, OAr>::SkynetTalker(int snkey, sn_msg type, modulename name)
    : m_skynet(name, snkey/*, status*/), 
      m_bufSize(0),
      m_buffer(NULL),
      m_sendsock(-1),
      m_recvsock(-1),
      m_modname(name),
      m_msgtype(type)
{
  DGCcreateMutex(&m_dataBufferMutex);
  // m_buffer, m_bufSize, m_sendsock, m_recvsock will be initialized the first time
  // they are used.
}

template<typename Message, typename IAr, typename OAr>
SkynetTalker<Message, IAr, OAr>::~SkynetTalker()
{
  resizeBuffer(0);
  DGCdeleteMutex(&m_dataBufferMutex);
}


template<typename Message, typename IAr, typename OAr>
int SkynetTalker<Message, IAr, OAr>::getRecvSock()
{
  if (m_recvsock < 0) {
      DBG("SkynetTalker::getRecvSock(): opening receive socket (listening)");
      m_recvsock = m_skynet.listen(m_msgtype, m_modname);
      if (m_recvsock < 0) {
          MSG("SkynetTalker::getRecvSock(): *** error opening receive socket");
      }
  }
  return m_recvsock;
}

template<typename Message, typename IAr, typename OAr>
int SkynetTalker<Message, IAr, OAr>::getSendSock()
{
  if (m_sendsock < 0) {
      DBG("SkynetTalker::getSendSock(): opening send socket");
      m_sendsock = m_skynet.get_send_sock(m_msgtype);
      if (m_sendsock < 0) { // at the moment, this never happens
          MSG("SkynetTalker::getSendSock(): *** error opening send socket");
      }
  }
  return m_sendsock;
}

template<typename Message, typename IAr, typename OAr>
void SkynetTalker<Message, IAr, OAr>::resizeBuffer(int size)
{
  ::resizeBuffer(&m_buffer, &m_bufSize, size);
}


template<typename Message, typename IAr, typename OAr>
bool SkynetTalker<Message, IAr, OAr>::send(const Message* msg, pthread_mutex_t* pMutex)
{
  int bytesToSend;
  int bytesSent;
  bool ret = true;

  DBG("SkynetTalker::send() BEGIN");


  DGClockMutex(&m_dataBufferMutex);
 
  if(pMutex != NULL) 
  {
    DGClockMutex(pMutex);
  }

  // Serialize the message to be sent 
  try {
      SkynetBufferSink sink(&m_buffer, &m_bufSize);
      boostio::stream<SkynetBufferSink> os(sink);
      OAr ar(os);
      ar << *msg;
      os.flush(); // without this not all data is written in the buffer!
      bytesToSend = os->getPos();
      DBG("SkynetTalker::send(): sending a " << bytesToSend << " bytes message");

      bytesSent = m_skynet.send_msg(getSendSock(), const_cast<char*>(m_buffer), bytesToSend, 0);

      if(bytesSent != bytesToSend)
      {
          cerr << "SkynetTalker::send() sent " << bytesSent
               << " bytes while expected to send " << bytesToSend << " bytes" << endl;
          ret = false;
      }
  } catch( ... ) {
      // FIXME: catch the correct type of exception and print a more detailed error message
      // Serialization is the only thing that can throw, anyway
      MSG("*** exception caught serializing message ***");
      ret = false;
  }


  DGCunlockMutex(&m_dataBufferMutex);
  if(pMutex != NULL)
    DGCunlockMutex(pMutex);

  DBG("SkynetTalker::send() END returning " << ret);

  return true;
}


template<typename Message, typename IAr, typename OAr>
bool SkynetTalker<Message, IAr, OAr>::receive(Message* msgReceive, pthread_mutex_t* pMutex)
{
  int bytesReceived;
  bool ret = true;

  DBG("SkynetTalker::receive(): BEGIN");

  int recvsock = getRecvSock();
  if (recvsock < 0)
  {
      return false;
  }

  if (!hasNewMessage()) {
      DBG("SkynetTalker::receive(): no new message, waiting");
      waitForMessage();
  }
  
  // Build the mutex list. We want to protect the data buffer
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_dataBufferMutex;
  if(pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  int requiredSize = m_skynet.get_msg_size(recvsock);
  
  DBG("SkynetTalker::receive(): skynet::get_msg_size() returned " << requiredSize);
  if (requiredSize <= 0)
  {
    // we know there should be a message available, unless another thread read it between
    // the hasNewMessage() call and here.
    // TODO: better synchronize this method
    DBG("SkynetTalker::receive(): returning false. END");
    return false;
  }

  DBG("SkynetTalker::receive(): m_bufSize = " << m_bufSize);
  if (requiredSize > m_bufSize)
  {
    resizeBuffer(requiredSize);
  }
  
  bytesReceived = m_skynet.get_msg(recvsock, m_buffer, m_bufSize,
                                   0, ppMutices, false, numMutices);

  try {
      SkynetBufferSource src(&m_buffer, &m_bufSize);
      boostio::stream<SkynetBufferSource> is(src);
      IAr ar(is);
      ar >> *msgReceive;
  } catch ( ...) {
      // FIXME: catch the correct type of exception and print a more detailed error message
      MSG("*** exception caught deserializing message ***");
      ret = false;
  }
  if(pMutex != NULL)
      DGCunlockMutex(pMutex);
  DGCunlockMutex(&m_dataBufferMutex);
  
  DBG(" SkynetTalker::receive() END returning " << ret);
  
  return ret;
}

template<typename Message, typename IAr, typename OAr>
bool SkynetTalker<Message, IAr, OAr>::hasNewMessage()
{
    int recvsock = getRecvSock();
    if (recvsock < 0)
    {
        return false;
    }
    return m_skynet.is_msg(recvsock);
}

template<typename Message, typename IAr, typename OAr>
void SkynetTalker<Message, IAr, OAr>::waitForMessage()
{
    int recvsock = getRecvSock();
    if (recvsock >= 0)
    {
        m_skynet.sn_select(recvsock);
    }
}
