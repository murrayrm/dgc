/*!**
 * Lars Cremean and Nok Wongpiromsarn
 * December 8, 2006
 *
 * This is a simple test program that allows you to send out RDDFs via
 * skynet.  The default behavior is to read the file test.rddf from
 * the current directory and send out that RDDF, followed by 5 RDDFs
 * in which points are added to the end of the original set.
 *
 * Usage: test_RDDFTalkerSend [--rddf file] [--skynet-key key]
 */

#include "RDDFTalker.hh"
#include "dgcutils/ggis.h"
#include "interfaces/sn_types.h"
#include "skynet/skynet.hh"
#include "RDDFTalker_cmdline.h"

class CRDDFTalkerTestSend : public CRDDFTalker
{
public:
  CRDDFTalkerTestSend(int sn_key, char *rddffile)
    : CSkynetContainer(MODrddftalkertestsend, sn_key)
  {
    int i;
    RDDF rddf(rddffile);	// read from an RDDF file
    rddf.print();		// print out the RDDF file

    RDDFVector waypoints = rddf.getTargetPoints();

    cout << "about to get_send_sock...";
    int rddfSocket = m_skynet.get_send_sock(SNrddf);
    cout << " get_send_sock returned" << endl;
		
    for(i=1; i< 6; i++) {
      cout << "about to send an rddf ("<<i+1<<" of 5) with " <<
	rddf.getNumTargetPoints() << " points" << endl;
      SendRDDF(rddfSocket, &rddf);
      cout << " sent an rddf!" << endl;
      flush(cout);

      // Test sending different sizes of RDDFs
      RDDFData wp;
      wp.number = rddf.getNumTargetPoints();
      cout << "generating waypoint " << wp.number << endl;

      // Add a waypoint onto the end of the current RDDF
      GisCoordUTM utm; GisCoordLatLon latlon;
      utm.zone = 11; utm.letter = 'S';
      wp.Northing = utm.n = rddf.getWaypointNorthing(wp.number-1);
      wp.Easting = utm.e = rddf.getWaypointEasting(wp.number-1) + i;
      gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);

      // Convert to a waypoint and add
      wp.latitude = latlon.latitude;
      wp.longitude = latlon.longitude;
      wp.radius = 20;
      wp.maxSpeed = 10+i;
      fprintf(stdout, "  WP %d: %g %g %g %g\n",
	      wp.number, wp.latitude, wp.longitude, wp.radius, wp.maxSpeed);

      rddf.addDataPoint(wp);

      sleep(2);
    }
  }
};

int main(int argc, char *argv[])
{
  // Parse command line options
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  int snkey = skynet_findkey(argc, argv);

  /* Make sure that we got an input */
  if (cmdline.inputs_num != 1) {
    fprintf(stderr, "Usage: %s [-S key] rddf-file\n", argv[0]);
    exit(1);
  }

  CRDDFTalkerTestSend test(snkey, cmdline.inputs[0]);
}
