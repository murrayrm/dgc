/*!**
 * Daniele Tamino
 * February 6, 2007
 */

#include <getopt.h>
// C++ STL headers
#include <iostream>

// local headers
#include "SkynetTalker.hh"
#include "dgcutils/RDDF.hh"
#include "dgcutils/ggis.h"
#include "rddfser.hh"

class SkynetTalkerTestSend
{
    SkynetTalker<RDDF> talker;
public:
  SkynetTalkerTestSend(int sn_key, char *rddffile)
    : talker(sn_key, SNrddf, MODrddftalkertestsend)
  {
    int i;
    RDDF rddf(rddffile); // read from an RDDF file

    RDDFVector waypoints = rddf.getTargetPoints();

    for(i = 0; i < 5; i++) {
      if (i > 0) { // the first RDDF is the unchanged one
        sleep(2);
        // Test sending different sizes of RDDFs
        RDDFData wp;
        int wpidx = rddf.getNumTargetPoints();
        wp.number = wpidx + 1;
        cout << "generating waypoint " << wp.number << endl;
        
        // Add a waypoint onto the end of the current RDDF
        GisCoordUTM utm; GisCoordLatLon latlon;
        utm.zone = 11; utm.letter = 'S';
        wp.Northing = utm.n = rddf.getWaypointNorthing(wpidx-1);
        wp.Easting = utm.e = rddf.getWaypointEasting(wpidx-1) + i + 1;
        gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);

        // Convert to a waypoint and add
        wp.latitude = latlon.latitude;
        wp.longitude = latlon.longitude;
        wp.radius = 20;
        wp.maxSpeed = 10+i+1;
        fprintf(stdout, "  WP %d: %g %g %g %g\n",
                wp.number, wp.latitude, wp.longitude, wp.radius, wp.maxSpeed);

        rddf.addDataPoint(wp);
      }

      cout << "about to send an rddf (" << i+1 << " of 5) with "
           << rddf.getNumTargetPoints() << " points:" << endl;
      rddf.print();
      talker.send(&rddf);
      cout << " sent an rddf!" << endl;
    }
  }
};

// Command line options
enum {OPT_NONE, OPT_HELP, OPT_RDDF, NUM_OPTS};
static struct option long_options[] = {
  // first: long option (--option) string
  // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
  // third: if pointer, set variable to value of fourth argument
  //        if NULL, getopt_long returns fourth argument
  {"help", 0, NULL, OPT_HELP},
  {"rddf", 1, NULL, OPT_RDDF},
  {NULL, 0, NULL, 0}
};

char *usage_string = "\
Usage: follow [options]\n\
  -h, --help        print usage information\n\
  --rddf FILENAME   use FILENAME as the RDDF\n\
";

int main(int argc, char *argv[])
{
  int errflg = 0, ch, option_index;
  char *rddffile = "test.rddf";

  // Parse command line options
  while (!errflg &&
	 (ch = getopt_long_only(argc, argv, "",
				long_options, &option_index)) != -1) {
    switch (ch) {
    case '?':
      fprintf(stderr, usage_string);
      ++errflg;
      break;

    case OPT_RDDF:
      if (optarg != NULL) rddffile=optarg;
      break;

    default:
      if(ch!=0) {
	printf("Unknown option %d!\n", ch);
	fprintf(stderr, usage_string);
	exit(1);
      }
    }
  }

  // Check to see if there were any errors
  if (errflg) { exit(1); }


  extern int optind;
  int snkey = (argc > optind) ? atoi(argv[optind]) : 0;
  SkynetTalkerTestSend test(snkey, rddffile);
  return 0;
}
