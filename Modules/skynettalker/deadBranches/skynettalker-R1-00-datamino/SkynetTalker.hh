/*!**
 * Daniele Tamino, Vanessa Carson and Nok Wongpiromsarn
 * February 2, 2007
 */

#ifndef SKYNETTALKER_HH_
#define SKYNETTALKER_HH_

// system includes
#include <pthread.h>
#include <unistd.h>
#include <string>
// boost serialization includes
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
// local (dgc) includes
//#include "interfaces/SkynetContainer.hh"
#include "skynet/skynet.hh"

template<typename Message,
         typename IAr = boost::archive::binary_iarchive,
         typename OAr = boost::archive::binary_oarchive>
class SkynetTalker
{

public:
  SkynetTalker(int snkey, sn_msg mbox, modulename name /*, int* status=NULL */);
  ~SkynetTalker();
  
  /*! Send a message through skynet.
   * \param message The message object that we want to send.
   * \param pMutex If not NULL, this mutex will be locked before sending and unlocked
   * after all the data has been sent succesfully.
   * \return true if everything went well, false otherwise
   */
#warning "TODO: use a better error checking method (detailed error codes or exceptions)"
  bool send(const Message* message, pthread_mutex_t* pMutex = NULL);
  /*! Receive a message from skynet.
   * \param pMutex FIXME: don't know really well ... --datamino
   */
#warning "TODO: use a better error checking method (detailed error codes or exceptions)"
  bool receive(Message* msgReceive, pthread_mutex_t* pMutex = NULL);

  /*! Checks if there is a new message available
   * \returns true if a message is available, false otherwise.
   */
  bool hasNewMessage();

  /*! Wait until a message is available or more than 'timeout' seconds are passed.
   * \param timeout Maximum number of seconds to wait for, or wait forever if timeout = -1.
   */
  void waitForMessage(double timeout = -1);

private:

  skynet m_skynet;

  pthread_mutex_t m_dataBufferMutex;

  int m_bufSize; // size in bytes of m_buffer
  char * m_buffer; // buffer used to send and receive messages
  int m_recvsock; 
  int m_sendsock;

  modulename m_modname;
  sn_msg m_msgtype;

  // resize m_buffer to the given size, or allocate if == NULL. If size == 0,
  // free buffer and set m_buffer = NULL.
  void resizeBuffer(int size);
};

#endif //  SKYNETTALKER_HH_
