/*!**
 * Daniele Tamino
 * Vanessa Carson
 * December 8, 2006
 */

#include "SkynetTalker.hh"
#include "dgcutils/DGCutils.hh"
#include <iostream>
#include <sstream>
//#include <strstream>

using namespace std;

#ifndef NDEBUG

#define DBG() (cerr << "DBG: " << __FILE__ << ':' << __LINE__ << ':')

#else

// implement DBG() << something; as a NO-OP
struct dev_null_stream { };
dev_null_stream& DBG() { return dev_null_stream(); }
template<typename T> dev_null_stream& operator<<(dev_null_stream& s, T discarded) {
    (void) discarded; return s;
}

#endif

template<typename Message, typename IAr, typename OAr>
SkynetTalker<Message, IAr, OAr>::SkynetTalker(int snkey, sn_msg type, modulename name /*, int* status=NULL */)
    : m_skynet(name, snkey/*, status*/), 
      m_buffer(NULL),
      m_modname(name),
      m_msgtype(type)
{
  DGCcreateMutex(&m_dataBufferMutex);
  
  // FIXME: is there any side effect having both a send and receive sockek
  // opened on every module???

#warning "TODO: do some error checking!!!"

  // listen to the specified mailbox
  m_recvsock = m_skynet.listen(type, name);
  // get a send socket
  m_sendsock = m_skynet.get_send_sock(type);

  // m_buffer will be allocated to the correct size the first time receive() is called
 }

template<typename Message, typename IAr, typename OAr>
SkynetTalker<Message, IAr, OAr>::~SkynetTalker()
{
  resizeBuffer(0);
  DGCdeleteMutex(&m_dataBufferMutex);
}

template<typename Message, typename IAr, typename OAr>
void SkynetTalker<Message, IAr, OAr>::resizeBuffer(int size)
{
  if (m_buffer != NULL) {
    delete[] m_buffer;
  }
  m_bufSize = size;
  if (size > 0) {
    m_buffer = new char[m_bufSize];
    if (m_buffer == NULL) {
      cerr << "OUT OF MEMORY!!!!!" << endl;
      exit(-1); // uhmmmm .. not a graceful exit ...
    }
  } else {
    m_buffer = NULL;
  }
}


template<typename Message, typename IAr, typename OAr>
bool SkynetTalker<Message, IAr, OAr>::send(const Message* msg, pthread_mutex_t* pMutex)
{
  int bytesToSend;
  int bytesSent;

  DBG() << " SkynetTalker::send() BEGIN" << endl;

  DGClockMutex(&m_dataBufferMutex);
 
  if(pMutex != NULL) 
  {
    DGClockMutex(pMutex);
  }

  //Serialize the message sent 
#warning "FIXME: use a better stream than istringstream" 
  ostringstream os;
  OAr ar(os);
#warning "FIXME: boost throws exceptions in case of errors, catch them!"
  ar << *msg;
  const char* buf = os.str().data();
  bytesToSend = os.str().length();

  bytesSent = m_skynet.send_msg(m_sendsock, const_cast<char*>(buf), bytesToSend, 0);

  if(bytesSent != bytesToSend)
  {
    cerr << "SkynetTalker::send() sent " << bytesSent
         << " bytes while expected to send " << bytesToSend << " bytes" << endl;
    return false;
  }

  DGCunlockMutex(&m_dataBufferMutex);
  if(pMutex != NULL)
    DGCunlockMutex(pMutex);

  DBG() << " SkynetTalker::send() END" << endl;

  return true;
}


template<typename Message, typename IAr, typename OAr>
bool SkynetTalker<Message, IAr, OAr>::receive(Message* msgReceive, pthread_mutex_t* pMutex)
{
  int bytesReceived;

  DBG() << " SkynetTalker::receive(): BEGIN" << endl;

  if (!hasNewMessage()) {
    DBG() << " SkynetTalker::receive(): no new message, returning false. END" << endl;
    return false;
  }
  
  // Build the mutex list. We want to protect the data buffer
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_dataBufferMutex;
  if(pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

#warning "TODO: I've added a method skynet::get_msg_size(). REMEMBER TO MAKE A SKYNET RELEASE!!!"

  int requiredSize = m_skynet.get_msg_size(m_recvsock);
  
  if (requiredSize <= 0)
  {
    // we know there should be a message available, unless another thread read it between
    // the hasNewMessage() call and here.
    // Synchronize this method or let the caller do the synchronization?
    DBG() << "SkynetTalker::receive(): skynet::get_msg_size() returned " << requiredSize
          << ", returning false. END" << endl;
    return false;
  }

  if (requiredSize < m_bufSize)
  {
    resizeBuffer(requiredSize);
  }
  
  bytesReceived = m_skynet.get_msg(m_recvsock, m_buffer, m_bufSize,
                                   0, ppMutices, false, numMutices);

  //Deserialize   
  //IAr ar(istrstream(m_buffer, bytesReceived));
#warning "FIXME: use a better stream than istringstream"  
  // Need to copy byte by byte the buffer in a string otherwise istringstream
  // searches for the terminator '\0' and eventually segfaults.
  // this is BAD, must find an alternative to istringstream (or crete one!)
  string tmp(bytesReceived, '\0');
  for (int i = 0; i < bytesReceived; i++)
  {
      tmp[i] = m_buffer[i];
  }
  istringstream is(tmp);
  IAr ar(is);
#warning "FIXME: boost throws exceptions in case of errors, catch them!"
  ar >> *msgReceive;

  if(pMutex != NULL)
    DGCunlockMutex(pMutex);
  DGCunlockMutex(&m_dataBufferMutex);
  
  DBG() << " SkynetTalker::receive() END" << endl;

  return true;
}

template<typename Message, typename IAr, typename OAr>
bool SkynetTalker<Message, IAr, OAr>::hasNewMessage()
{
    return m_skynet.is_msg(m_recvsock);
}

template<typename Message, typename IAr, typename OAr>
void SkynetTalker<Message, IAr, OAr>::waitForMessage(double timeout)
{
  // any chance to use the timeout without bad hacks? is it useful/needed?
  m_skynet.sn_select(m_recvsock);
}
