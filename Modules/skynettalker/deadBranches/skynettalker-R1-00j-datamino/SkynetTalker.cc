/*!**
 * Daniele Tamino
 * Vanessa Carson
 * December 8, 2006
 */

#include "SkynetTalker.hh"

#if !NDEBUG
#  define DBG(arg) (cerr << "DBG: " << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
#else
#  define DBG(arg)
#endif
#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)

namespace skynettalker {

    /* Resize a buffer. 'buffer' is a pointer to the buffer, 'size' a pointer to its size (these two are both
       input and output parameters).
       newSize: size of the new buffer to be allocated.
       length: length of content currently in buffer (<= *size). This number of
       bytes will be copied to the new buffer.
    */
    void resizeBuffer(char** buffer, int* size, int newSize, int length)
    {
	DBG("resizeBuffer(): BEGIN resizing buffer (" << *size << " -> " << newSize << ")");
	char* newBuf = NULL;
	if (newSize != 0)
	    {
		newBuf = new char[newSize];
		if (newBuf == NULL)
		    {
			cerr << "OUT OF MEMORY!!!!!" << endl;
			exit(-1); // uhmmmm .. not a graceful exit ...
		    }
		else
		    {
			if (*buffer != NULL)
			    {
				int numCopy = min(length, newSize);
				if (numCopy > 0)
				    {
					memcpy(newBuf, *buffer, numCopy);
				    }
				delete[] *buffer;
			    }
			*buffer = newBuf;
			*size = newSize;
		    }
	    }
	DBG("resizeBuffer(): END");
    }

    streamsize SkynetBufferSource::read(char_type* s, streamsize n)
    {
	DBG("SkynetBufferSource::read(): BEGIN");
	streamsize max = static_cast<streamsize>(*m_size - m_pos);
	streamsize result = min(n, max);
	if (result != 0) {
	    //pthread_mutex_lock(&m_mtx);
	    memcpy(s, *m_buffer + m_pos, result);
	    //pthread_mutex_unlock(&m_mtx);
	    m_pos += result;
	} else {
	    result = -1; // EOF
	}
	DBG("SkynetBufferSource::read(): END returning " << result);
	return result;
    }
    
    streamsize SkynetBufferSink::write(const char_type* s, streamsize n)
    {
	DBG("SkynetBufferSink::write(): BEGIN");
	//pthread_mutex_lock(&m_mtx);
	// check if the buffer is big enough
	if (m_pos + n > *m_size) {
	    int newSize = m_pos + n;
	    resizeBuffer(m_buffer, m_size, newSize, m_pos);
	}            
	memcpy(*m_buffer + m_pos, s, n);
	//pthread_mutex_unlock(&m_mtx);
	m_pos += n;
	DBG("SkynetBufferSink::write(): END returning " << n);
	return n;
    }

} // end namespace skynettalker
