/*!**
 * Lars Cremean and Nok Wongpiromsarn
 * December 8, 2006
 *
 * This is a simple test program that allows you to send out RDDFs via
 * skynet.  The default behavior is to read the file test.rddf from
 * the current directory and send out that RDDF, followed by 5 RDDFs
 * in which points are added to the end of the original set.
 *
 * Usage: test_RDDFTalkerSend [--rddf file] skynet_key
 */

#include "RDDFTalker.hh"
#include "dgcutils/ggis.h"
#include "interfaces/sn_types.h"
#include "getopt.h"

class CRDDFTalkerTestSend : public CRDDFTalker
{
public:
  CRDDFTalkerTestSend(int sn_key, char *rddffile)
    : CSkynetContainer(MODrddftalkertestsend, sn_key)
  {
    int i;
    RDDF rddf(rddffile); // read from an RDDF file

    RDDFVector waypoints = rddf.getTargetPoints();

#ifdef UNUSED
    for(i=0; i<rddf.getNumTargetPoints(); i++) {
      waypoints[i].display();
    }
#endif

    cout << "about to get_send_sock...";
    int rddfSocket = m_skynet.get_send_sock(SNrddf);
    cout << " get_send_sock returned" << endl;
		
    for(i=1; i< 6; i++) {
      cout << "about to send an rddf ("<<i+1<<" of 5) with " <<
	rddf.getNumTargetPoints() << " points" << endl;
      SendRDDF(rddfSocket, &rddf);
      cout << " sent an rddf!" << endl;
      flush(cout);

      // Test sending different sizes of RDDFs
      RDDFData wp;
      wp.number = rddf.getNumTargetPoints();
      cout << "generating waypoint " << wp.number << endl;

      // Add a waypoint onto the end of the current RDDF
      GisCoordUTM utm; GisCoordLatLon latlon;
      utm.zone = 11; utm.letter = 'S';
      wp.Northing = utm.n = rddf.getWaypointNorthing(wp.number-1);
      wp.Easting = utm.e = rddf.getWaypointEasting(wp.number-1) + i;
      gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);

      // Convert to a waypoint and add
      wp.latitude = latlon.latitude;
      wp.longitude = latlon.longitude;
      wp.radius = 20;
      wp.maxSpeed = 10+i;
      fprintf(stdout, "  WP %d: %g %g %g %g\n",
	      wp.number, wp.latitude, wp.longitude, wp.radius, wp.maxSpeed);

      rddf.addDataPoint(wp);

      sleep(2);
    }
  }
};

// Command line options
enum {OPT_NONE, OPT_HELP, OPT_RDDF, NUM_OPTS};
static struct option long_options[] = {
  // first: long option (--option) string
  // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
  // third: if pointer, set variable to value of fourth argument
  //        if NULL, getopt_long returns fourth argument
  {"help", 0, NULL, OPT_HELP},
  {"rddf", 1, NULL, OPT_RDDF},
  {NULL, 0, NULL, 0}
};

char *usage_string = "\
Usage: follow [options]\n\
  -h, --help        print usage information\n\
  --rddf FILENAME   use FILENAME as the RDDF\n\
";

int main(int argc, char *argv[])
{
  int errflg = 0, ch, option_index;
  char *rddffile = "test.rddf";

  // Parse command line options
  while (!errflg &&
	 (ch = getopt_long_only(argc, argv, "",
				long_options, &option_index)) != -1) {
    switch (ch) {
    case '?':
      fprintf(stderr, usage_string);
      ++errflg;
      break;

    case OPT_RDDF:
      if (optarg != NULL) rddffile=optarg;
      break;

    default:
      if(ch!=0) {
	printf("Unknown option %d!\n", ch);
	fprintf(stderr, usage_string);
	exit(1);
      }
    }
  }

  // Check to see if there were any errors
  if (errflg) { exit(1); }


  extern int optind;
  int snkey = (argc > optind) ? atoi(argv[optind]) : 0;
  CRDDFTalkerTestSend test(snkey, rddffile);
}
