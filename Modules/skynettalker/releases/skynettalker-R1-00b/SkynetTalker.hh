/*!**
 * Daniele Tamino, Vanessa Carson and Nok Wongpiromsarn
 * February 2, 2007
 */

#ifndef SKYNETTALKER_HH_
#define SKYNETTALKER_HH_

// system includes
#include <pthread.h>
#include <unistd.h>
#include <string>
// boost serialization includes
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
// local (dgc) includes
//#include "interfaces/SkynetContainer.hh"
#include "skynet/skynet.hh"

/*! This class is used to serialize messages (using boost::serialize) and then send them
 * via skynet to everyone who's listening to that particular type of message.
 * It's a helper to make skynet more Object Oriented, and to ease writing modules that
 * uses it.
 */
template<typename Message,
         typename IAr = boost::archive::binary_iarchive,
         typename OAr = boost::archive::binary_oarchive>
class SkynetTalker
{

public:
    /*! Constructor.
     * \param snkey Skynet key to use.
     * \param type Type of the messages to be sent or received.
     * \param modulename The name of this module.
     */
  SkynetTalker(int snkey, sn_msg type, modulename name);
  ~SkynetTalker();
  
  /*! Send a message through skynet.
   * \param message The message object that we want to send.
   * \param pMutex If not NULL, this mutex will be locked before sending and unlocked
   * after all the data has been sent succesfully.
   * \return true if everything went well, false otherwise
   */
#warning "TODO: use a better error checking method (detailed error codes or exceptions)"
  bool send(const Message* message, pthread_mutex_t* pMutex = NULL);
  /*! Receive a message from skynet. If no message is available, this method blocks
   * and waits for a message (like waitForMessages()).
   * \param pMutex If not NULL, this is locked while reading the message and unlocked
   * after that (but it's not locked while waiting for messages).
   */
#warning "TODO: use a better error checking method (detailed error codes or exceptions)"
  bool receive(Message* msgReceive, pthread_mutex_t* pMutex = NULL);

  /*! Checks if there is a new message available
   * \returns true if a message is available, false otherwise.
   */
  bool hasNewMessage();

  /*! Wait until a message is available.
   */
  void waitForMessage();

private:

  skynet m_skynet;

  pthread_mutex_t m_dataBufferMutex;

  int m_bufSize; // size in bytes of m_buffer
  char * m_buffer; // buffer used to send and receive messages
  int m_sendsock;
  int m_recvsock; 

  modulename m_modname;
  sn_msg m_msgtype;

  // resize m_buffer to the given size, or allocate if == NULL. If size == 0,
  // free buffer and set m_buffer = NULL.
  void resizeBuffer(int size);
  
  int getRecvSock();
  int getSendSock();
};

#endif //  SKYNETTALKER_HH_
