/*!**
 * Lars Cremean and Nok Wongpiromsarn
 * December 8, 2006
 *
 * The CRDDFTalker class provides a mechanism for sending and receiving
 * RDDFs across skynet.
 *
 * Someone should add some comments on the basic mechanism that is
 * used, epsecially the use of streams in the receive function, which
 * I don't understand (RMM, 9 Dec 06).
 */

#include <iostream>
#include <strstream>
#include <iomanip>
#include "RDDFTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/ggis.h"

#define MAX_SKYNET_RDDF_WAYPOINTS 100
using namespace std;

/******************************************************************************/
// Basic constructor: no threads
CRDDFTalker::CRDDFTalker() : m_receivedRddf(), m_newRddf()
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  m_bRunGetRDDFThreads = false;
  DGCcreateMutex(&m_RDDFDataBufferMutex);
}

/******************************************************************************/
// Basic constructor with option to start a talker thread using rddfSocket
CRDDFTalker::CRDDFTalker(bool runGetRDDFThread) : m_receivedRddf(), m_newRddf()
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  m_bRunGetRDDFThreads = runGetRDDFThread;
  DGCcreateMutex(&m_RDDFDataBufferMutex);
  if (m_bRunGetRDDFThreads)
  {
  	int rddfSocket = m_skynet.listen(SNrddf, MODtrafficplanner);
  	DGCcreateMutex(&m_ReceivedRDDFMutex);
  	DGCcreateCondition(&condNewRDDF);
  	condNewRDDF.bCond = false;
  	DGCstartMemberFunctionThreadWithArg(this, &CRDDFTalker::getRDDFThread, (void*)rddfSocket);
  }
}

/******************************************************************************/
// Constructor for talking across a specific socket
CRDDFTalker::CRDDFTalker(bool runGetRDDFThread, int socket) : m_receivedRddf(), m_newRddf()
{
  m_pRDDFDataBuffer = new char[RDDF_DATA_PACKAGE_SIZE];
  m_bRunGetRDDFThreads = runGetRDDFThread;
  DGCcreateMutex(&m_RDDFDataBufferMutex);
  DGCcreateMutex(&m_ReceivedRDDFMutex);
  DGCcreateCondition(&condNewRDDF);
  condNewRDDF.bCond = false;
  if (m_bRunGetRDDFThreads)
  	DGCstartMemberFunctionThreadWithArg(this, &CRDDFTalker::getRDDFThread, (void*)socket);
}

/******************************************************************************/
CRDDFTalker::~CRDDFTalker()
{
  delete [] m_pRDDFDataBuffer;
  DGCdeleteMutex(&m_RDDFDataBufferMutex);
  if (m_bRunGetRDDFThreads)
  {
  	DGCdeleteMutex(&m_ReceivedRDDFMutex);
  	DGCdeleteCondition(&condNewRDDF);
  }
}

/******************************************************************************/
// Send an RDDF accross skynet
bool CRDDFTalker::SendRDDF(int rddfSocket, RDDF* pRddf, pthread_mutex_t* pMutex)
{
  int bytesToSend;
  int bytesSent;
  char* pBuffer = m_pRDDFDataBuffer;

  // Initialize
  char tmp[100] = "\0";
  pBuffer[0] = '\0';
  RDDFVector wp = pRddf->getTargetPoints();

  // Print out the RDDF so we can see what is going on
  for (int i=0; i<pRddf->getNumTargetPoints(); i++)
  {
  	/*
    sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
      wp[i].number, wp[i].latitude, wp[i].longitude, 
      wp[i].radius/METERS_PER_FOOT, wp[i].maxSpeed/MPS_PER_MPH);
    */  
    
    //Note that radius is in meters and maxSpeed is in MPH
    sprintf(tmp, "%d,%.8f,%.8f,%.3f,%.3f\n", 
      wp[i].number, wp[i].latitude, wp[i].longitude, 
      wp[i].radius, wp[i].maxSpeed);
  
    strncat(pBuffer, tmp, strlen(tmp));

    //cout << tmp;
  }

  //printf("\n[%s:%d] \n", __FILE__, __LINE__);
  //cout << pBuffer;

  // Lock mutexes so that we can access the RDDF data    
  DGClockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGClockMutex(pMutex);
  }

  // Send the RDDF accross skynet
  bytesToSend = RDDF_DATA_PACKAGE_SIZE;
  bytesSent = m_skynet.send_msg(rddfSocket, m_pRDDFDataBuffer, bytesToSend, 0);

  // Unlock mutexes
  DGCunlockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGCunlockMutex(pMutex);
  }

  // Check for any errors and print a message
  if (bytesSent != bytesToSend)
  {
    cerr << "CRDDFTalker::SendRDDF(): sent " << bytesSent << 
      " bytes while expected to send " << bytesToSend << " bytes" << endl;

    return false;
  }
  
  return true;
}


/******************************************************************************/
// Receive an RDDF accross skynet
bool CRDDFTalker::RecvRDDF(int rddfSocket, RDDF* pRddf, 
    pthread_mutex_t* pMutex, string* pOutstring)
{
  int bytesToReceive;
  int bytesReceived;
  char* pBuffer = m_pRDDFDataBuffer;

  // Build the mutex list. We want to protect the data buffer
  int numMutices = 1;
  pthread_mutex_t* ppMutices[2];
  ppMutices[0] = &m_RDDFDataBufferMutex;
  if (pMutex != NULL)
  {
    ppMutices[1] = pMutex;
    numMutices++;
  }

  // Get the RDDF data from skynet. We want to receive the whole segGoals, locking
  // the requesting the mutices to be locked and NOT unlocked at the end. This
  // is done because this data is still accessed afterwards and we manually
  // unlock the mutices later.
  bytesToReceive = RDDF_DATA_PACKAGE_SIZE;
  
  bytesReceived = m_skynet.get_msg(rddfSocket, pBuffer, bytesToReceive, 
                                   0, ppMutices, false, numMutices);

  // Check to make sure we received everything correctly
  if (bytesReceived != bytesToReceive)
  {
    cerr << "CRDDFTalker::RecvRDDF(): received" << bytesReceived << 
      "bytes while expected to receive " << bytesToReceive << "bytes" << endl;

    DGCunlockMutex(&m_RDDFDataBufferMutex);
    if (pMutex != NULL)
    {
      DGCunlockMutex(pMutex);
    } 
    return false;
  }

  /*! Not sure what pOutstring does; can someone add a comment? !*/
  if (pOutstring != NULL)
  {
    pOutstring->append((char*)&bytesReceived, sizeof(bytesReceived));
    pOutstring->append(m_pRDDFDataBuffer, bytesReceived);
  }

  //printf("Raw characters received: ");
  //for (int i=0; i<RDDF_DATA_PACKAGE_SIZE; i++) 
  //{ 
  //  printf("%c",pBuffer[i]); 
  //}
  //printf("\n");

  /* Load the character array into an istream and call loadStream */
  istrstream recvstream(pBuffer, RDDF_DATA_PACKAGE_SIZE);
  
  //printf("[%s:%d] \n", __FILE__, __LINE__);
  //cout << recvstream.str() << endl;

  bool rddfReceived = loadStream(recvstream, pRddf);

  DGCunlockMutex(&m_RDDFDataBufferMutex);
  if (pMutex != NULL)
  {
    DGCunlockMutex(pMutex);
  }

  return rddfReceived;
}


/******************************************************************************/
bool CRDDFTalker::NewRDDF()
{
  return condNewRDDF.bCond;
}

/******************************************************************************/
void CRDDFTalker::UpdateRDDF()
{
  DGClockMutex(&m_ReceivedRDDFMutex);
  m_newRddf = m_receivedRddf;
  DGCunlockMutex(&m_ReceivedRDDFMutex);
  condNewRDDF.bCond = false;
}

/******************************************************************************/
void CRDDFTalker::WaitForRDDF()
{
  DGCWaitForConditionTrue(condNewRDDF);
  UpdateRDDF();
}

/******************************************************************************/
void CRDDFTalker::getRDDFThread(void* pArg)
{
  int rddfSocket = (int)pArg;
  while(true)
  {
  	RDDF rddf;
  	bool rddfReceived = RecvRDDF(rddfSocket, &rddf);
  	if (rddfReceived)
  	{
  	  DGClockMutex(&m_ReceivedRDDFMutex);
  	  m_receivedRddf = rddf;
  	  DGCunlockMutex(&m_ReceivedRDDFMutex);
	  DGCSetConditionTrue(condNewRDDF);
  	}
  	usleep(100000);
  }	
}

/******************************************************************************/
bool CRDDFTalker::loadStream(istrstream& inputstream, RDDF* pRddf)
{
  string line;
  RDDFData eachWaypoint;
  bool on_first_line = true;

  if(!inputstream)
  {
    cerr << "RDDF couldn't read from a stream\n";
    return false;
  }

  //printf("[%s:%d] ", __FILE__, __LINE__);
  //cout << "inputstream.str() = " << endl;
  //cout << inputstream.str();

  GisCoordLatLon latlon;
  GisCoordUTM utm;

  for(int i=0; i<MAX_SKYNET_RDDF_WAYPOINTS; i++)
  {
    // Number
    getline(inputstream,line,RDDF_DELIMITER);    
    
    // First input on a line should be checked for empty lines.
    if(line.empty()) 
    {
      cout << "crap!  the line is empty\n" ; 
      continue;
    }

    // NOTE: Waypoints are numbered starting at 0 for RDDFs 
    // that are sent across skynet.
    eachWaypoint.number = atoi(line.c_str());
	
    if(pRddf->getNumTargetPoints() > eachWaypoint.number)
    {
      break;
    }
    
    on_first_line = false;

    // Latitude
    getline(inputstream,line,RDDF_DELIMITER);
    latlon.latitude = atof(line.c_str());
    
    // Longitude
    getline(inputstream,line,RDDF_DELIMITER);
    latlon.longitude = atof(line.c_str());

    // Converting to UTM
    if(!gis_coord_latlon_to_utm(&latlon, &utm, GEODETIC_MODEL))
    {
      cerr << "Error converting coordinate, numTargetPoints = " 
	   << pRddf->getNumTargetPoints() << " eachWaypoint.number = " 
	   << eachWaypoint.number << " to utm\n";
      cout << latlon.latitude << " " << latlon.longitude << endl;
      return false;
    }
    eachWaypoint.Northing = utm.n;
    eachWaypoint.Easting = utm.e;

    // Preserve the location in original coordinates.    
    eachWaypoint.latitude = latlon.latitude;
    eachWaypoint.longitude = latlon.longitude;

    // Boundary offset and radius
    getline(inputstream,line,RDDF_DELIMITER);
    eachWaypoint.radius = atof(line.c_str()); //*METERS_PER_FOOT -- comment this out because the radius is already in meters for RDDFs that are sent across skynet.

    // Speed limit
    getline(inputstream,line,'\n');
    eachWaypoint.maxSpeed = atof(line.c_str()); //*MPS_PER_MPH -- comment this out because the radius is already in meters for RDDFs that are sent across skynet.

    pRddf->addDataPoint(eachWaypoint);
  }

  //printf("[%s:%d] \n", __FILE__, __LINE__);
  //print();

  // debug
  // cout << "RDDF: " << numTargetPoints << " target points read !!!" << endl;
  
  return true;
}
