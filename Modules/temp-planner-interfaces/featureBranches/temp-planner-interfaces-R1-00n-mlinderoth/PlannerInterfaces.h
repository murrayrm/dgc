/**********************************************************
 **
 **  PLANNERINTERFACES.H
 **
 **    Author: Noel du Toit
 **    Created: Tue Jul 10 08:38:32 2007
 **
 **********************************************************
 ** The interfaces between the executable planner module, 
 ** planner, and its libraries (GraphUpdater, PathPlanner, 
 ** LogicPlanner, VelPlanner) are defined here.
 **********************************************************/

#ifndef PLANNERINTERFACES_H
#define PLANNERINTERFACES_H

#include "Graph.hh"
#include "GraphPath.hh"

#include <gcinterfaces/SegGoals.hh>

// GRAPH
typedef Graph Graph_t;

// STATE PROBLEM
/// States used in the finite state machine
typedef enum {DRIVE, STOP_INT, STOP_OBS, UTURN, PAUSE} FSM_state_t;
/// Flags that guide the update of the graph
typedef enum {PASS, NO_PASS, ZONE} FSM_flag_t;

/// Defines the state problem that is returned by the logic planner
typedef struct {
  FSM_state_t state;
  double probability;
  FSM_flag_t flag;
} StateProblem_t;

// ERRORS
#define LP_OK              0
#define GU_OK              0
#define PP_OK              0
#define VP_OK              0
#define LP_MAP_INCOMPLETE  1
#define LP_FAIL_MPLANNER   2
#define GU_MAP_INCOMPLETE  4
#define PP_COLLISION       8
#define PP_NOPATH_LEN      16
#define PP_NOPATH_COST     32
#define VP_UTURN_FINISHED  64
#define PP_NONODEFOUND     128

typedef uint32_t Err_t;

// PATH
typedef struct GraphPath Path_t;

// COST
typedef int Cost_t;

// VEL PLANNER PARAMS
typedef struct {
  double minSpeed;
  double maxSpeed;
} Vel_params_t;

// PATH PLANNER PARAMS
typedef struct {
  FSM_flag_t flag;
  bool planFromCurrPos;
} Path_params_t;

// LOGIC PLANNER PARAMS
typedef struct {
  SegGoals::SegmentType segment_type;
  SegGoals seg_goal;
} Logic_params_t;

// PREDICTION INTERFACE
typedef int Prediction_t;

#endif
