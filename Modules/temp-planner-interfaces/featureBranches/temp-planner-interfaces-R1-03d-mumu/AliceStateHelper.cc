/*!
 * \file AliceStateHelper.cc
 * \brief Source code for some wrapper functions around the vehicle struct (m_state)
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#include <math.h>
#include "alice/AliceConstants.h"
#include "AliceStateHelper.hh"


/* Legacy of TPlanner, we might not need this after all */
LaneLabel AliceStateHelper::m_desiredLaneLabel;

/**
 * @brief Gets the front axle position
 */
point2 AliceStateHelper::getPositionFrontAxle(VehicleState &vehState)
{
  double length = DIST_REAR_AXLE_TO_FRONT - DIST_FRONT_AXLE_TO_FRONT;
  double easting = length*sin(vehState.utmYaw);
  double northing = length*cos(vehState.utmYaw);
  return point2 (vehState.siteNorthing + northing, vehState.siteEasting + easting);
}

/**
 * @brief Gets the front bumper position
 */
point2 AliceStateHelper::getPositionFrontBumper(VehicleState &vehState)
{
  double length = DIST_REAR_AXLE_TO_FRONT;
  double easting = length*sin(vehState.utmYaw);
  double northing = length*cos(vehState.utmYaw);
  return point2 (vehState.siteNorthing + northing, vehState.siteEasting + easting);
}

/**
 * @brief Gets the rear axle position
 */
point2 AliceStateHelper::getPositionRearAxle(VehicleState &vehState)
{
  return point2 (vehState.siteNorthing, vehState.siteEasting);
}

/**
 * @brief Gets the rear axle pose
 */
pose2f_t AliceStateHelper::getPoseRearAxle(VehicleState &vehState)
{
  pose2f_t pose;
  pose.pos.x = vehState.siteNorthing;
  pose.pos.y = vehState.siteEasting;
  pose.rot = vehState.siteYaw;
  return pose;
}

/**
 * @brief Gets the rear bumper position
 */
point2 AliceStateHelper::getPositionRearBumper(VehicleState &vehState)
{
  double length = DIST_REAR_TO_REAR_AXLE;
  double easting = -length*sin(vehState.utmYaw);
  double northing = -length*cos(vehState.utmYaw);
  return point2 (vehState.siteNorthing + northing, vehState.siteEasting + easting);
}

/**
 * @brief Gets the current acceleration
 */
double AliceStateHelper::getAccelerationMag(VehicleState &vehState)
{
  return sqrt(pow(vehState.Acc_N_deprecated,2) + pow(vehState.Acc_E_deprecated, 2));
}

/**
 * @brief Gets the current speed
 */
double AliceStateHelper::getVelocityMag(VehicleState &vehState)
{
  return sqrt(pow(vehState.utmNorthVel,2) + pow(vehState.utmEastVel, 2));
}

/**
 * @brief Translates to global coordinates
 */
point2 AliceStateHelper::convertToGlobal(point2 gloToLocalDelta, point2 localPoint)
{
		return point2(gloToLocalDelta + localPoint);
}

/**
 * @brief Gets the vehicle heading
 */
double AliceStateHelper::getHeading(VehicleState &vehState)
{
  return vehState.utmYaw;
}

/**
 * @brief Gets the desired lane
 */
LaneLabel AliceStateHelper::getDesiredLaneLabel()
{
    return m_desiredLaneLabel;
}

/**
 * @brief Sets the desired lane
 */
void AliceStateHelper::setDesiredLaneLabel(LaneLabel lane)
{
    m_desiredLaneLabel = lane;
}
