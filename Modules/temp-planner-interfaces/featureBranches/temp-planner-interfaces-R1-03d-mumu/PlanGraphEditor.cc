/* 
 * Desc: PlanGraph editor utility
 * Date: 25 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <float.h>

#include <GL/glut.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/DGCutils.hh>

#include "PlanGraphEditor.hh"

 
// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, "%s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Useful macros
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Default constructor
PlanGraphEditor::PlanGraphEditor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Destructor
PlanGraphEditor::~PlanGraphEditor()
{
  return;
}


// Initialize stuff
int PlanGraphEditor::initGUI(int cols, int rows)
{
  // The menu
  Fl_Menu_Item menuitems[] =
    {
      {"&File", 0, 0, 0, FL_SUBMENU},    
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) PlanGraphEditor::onExit},
      {0},

      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"Pause", ' ', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) CMD_ACTION_PAUSE, FL_MENU_TOGGLE},
      {"Reload", 'u', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) CMD_ACTION_RELOAD},
      {0},
      
      {"&View", 0, 0, 0, FL_SUBMENU},

      {"Aerial", 'a', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_VIEW_AERIAL + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},      
      {"RNDF", 'r', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_VIEW_RNDF + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Road", 'o', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_VIEW_ROAD + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      {"Graph", 'g', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_VIEW_PLAN  + CMD_VIEW_FIRST), FL_MENU_TOGGLE | FL_MENU_VALUE},
      
      // Graph display properties
      {"Plan Graph", 0, 0, 0, 0},
      {"Node info", FL_CTRL + 'n', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_GRAPH_NODES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      //{"Vehicle", FL_CTRL + 'v', (Fl_Callback*) PlanGraphEditor::onAction,
      // (void*) (CMD_GRAPH_VEHICLE + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Lane changes", FL_CTRL + 'l', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_GRAPH_LANE_CHANGES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},
      {"Rail changes", FL_CTRL + 'r', (Fl_Callback*) PlanGraphEditor::onAction,
       (void*) (CMD_GRAPH_RAIL_CHANGES + CMD_GRAPH_FIRST), FL_MENU_TOGGLE},      
      {0},

      {0},
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows + 30, "DGC Plan Graph Editor");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new Fl_Glv_Window(0, 30, cols, rows, this, (Fl_Callback*) onDraw);

  this->mainwin->end();

  // Make world window resizable  
  this->mainwin->resizable(this->worldwin);

  // Set the initial POV
  this->worldwin->set_hfov(40.0);
  this->worldwin->set_clip(4, 2000); 
  this->worldwin->set_lookat(-0.1, 0, -80, 0, 0, 0, 0, 0, -1);
    
  if (this->rndf && this->cmdline.start_given)
  {
    int segmentId, laneId, waypointId;
    RNDFGraphWaypoint *wp;
    if (sscanf(this->cmdline.start_arg, "%d.%d.%d", &segmentId, &laneId, &waypointId) == 3)
    {
      wp = this->rndf->getWaypoint(segmentId, laneId, waypointId);
      this->worldwin->set_lookat(wp->px - 0.1, wp->py, -80, wp->px, wp->py, 0, 0, 0, -1);
    }
  }

  // Set consistent menu state
  this->viewLayers = 0;
  this->viewLayers |= (1 << CMD_VIEW_AERIAL) | (1 << CMD_VIEW_RNDF);
  this->viewLayers |= (1 << CMD_VIEW_ROAD) | (1 << CMD_VIEW_PLAN);

  this->planProps = 0;

  this->rndfList = 0;
  this->roadList = 0;
  this->planList = 0;
  
  this->dirty = true;
  
  return 0;
}


// Finalize stuff
int PlanGraphEditor::finiGUI()
{  
  return 0;
}


// Handle menu callbacks
void PlanGraphEditor::onExit(Fl_Widget *w, int option)
{
  PlanGraphEditor *self;

  self = (PlanGraphEditor*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void PlanGraphEditor::onAction(Fl_Widget *w, int option)
{
  PlanGraphEditor *self;  

  self = (PlanGraphEditor*) w->user_data();
  if (option == CMD_ACTION_PAUSE)
    self->pause = !self->pause;

  // Check for changes in the selected display viewLayers
  if (option >= CMD_VIEW_FIRST && option < CMD_VIEW_LAST)
  {
    self->viewLayers = (self->viewLayers ^ (1 << (option - CMD_VIEW_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  // Check for changes in the selected graph properties
  if (option >= CMD_GRAPH_FIRST && option < CMD_GRAPH_LAST)
  {
    self->planProps = (self->planProps ^ (1 << (option - CMD_GRAPH_FIRST)));
    self->dirty = true;
    self->worldwin->redraw();
  }

  return;
}


// Handle draw callbacks
void PlanGraphEditor::onDraw(Fl_Glv_Window *win, PlanGraphEditor *self)
{  
  self->roiPos.x = win->at_x;
  self->roiPos.y = win->at_y;
      
  // Predraw if necessary
  if (self->dirty)
  {
    // Draw the grid
    self->drawMisc.predrawGrid(self->roiPos.x, self->roiPos.y, 10.0);

    // Predraw the static RNDF if necessary
    if (self->rndf)
    {
      if (self->rndfList == 0)
        self->rndfList = self->rndf->predraw();
    }

    // Draw the road graph
    if (self->road)
    {
      if (self->viewLayers & (1 << CMD_VIEW_ROAD))
        self->roadList = self->road->predraw(self->roiPos.x, self->roiPos.y, self->roiSize);
    }
    
    // Draw the plan graph
    if (self->plan)
    {
      if (self->viewLayers & (1 << CMD_VIEW_PLAN))
      {
        bool showNodes;
        uint16_t include, exclude;
      
        // Select the type of nodes to display
        include = PLAN_GRAPH_NODE_LANE | PLAN_GRAPH_NODE_TURN | PLAN_GRAPH_NODE_ZONE;
        exclude = PLAN_GRAPH_NODE_ONCOMING;

        if (self->planProps & (1 << CMD_GRAPH_LANE_CHANGES))
        {
          include |= PLAN_GRAPH_NODE_LANE_CHANGE;
          exclude &= ~PLAN_GRAPH_NODE_ONCOMING;
        }
        if (self->planProps & (1 << CMD_GRAPH_RAIL_CHANGES))
          include |= PLAN_GRAPH_NODE_RAIL_CHANGE;

        showNodes = ((self->planProps & (1 << CMD_GRAPH_NODES)) != 0);
  
        // Draw the graph structure
        self->planList = self->plan->predrawStruct(self->roiPos.x, self->roiPos.y, self->roiSize,
                                                   include, exclude, showNodes, !showNodes);
      }
    }
      
    self->dirty = false;
  }    
  
  // Switch to site frame
  glPushMatrix();

  // Draw the grid
  glPushMatrix();
  glTranslatef(self->roiPos.x, self->roiPos.y, 0);
  glCallList(self->drawMisc.gridList);
  glPopMatrix();

   
  if (true)
  {
    // Draw the aerial image
    glPushMatrix();
    glTranslatef(0, 0, 0.1);
    if (self->viewLayers & (1 << CMD_VIEW_AERIAL))
      self->drawAerial.draw(self->roiPos.x, self->roiPos.y);
    glPopMatrix();

    glPushMatrix();

    // Draw the ROI
    glColor3f(0, 0, 1);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUADS);
    glVertex2f(self->roiPos.x - self->roiSize/2, self->roiPos.y - self->roiSize/2);
    glVertex2f(self->roiPos.x + self->roiSize/2, self->roiPos.y - self->roiSize/2);
    glVertex2f(self->roiPos.x + self->roiSize/2, self->roiPos.y + self->roiSize/2);
    glVertex2f(self->roiPos.x - self->roiSize/2, self->roiPos.y + self->roiSize/2);
    glEnd();

    // Draw the RNDF 
    if (self->viewLayers & (1 << CMD_VIEW_RNDF))
      glCallList(self->rndfList);  

    // Draw the road graph
    if (self->viewLayers & (1 << CMD_VIEW_ROAD))
      glCallList(self->roadList);

    // Draw the plan graph
    if (self->viewLayers & (1 << CMD_VIEW_PLAN))
      glCallList(self->planList);

    glPopMatrix();
  }

  glPopMatrix();
  
  return;
}

// Handle idle callbacks
void PlanGraphEditor::onIdle(PlanGraphEditor *self)
{
  if (!self->pause)
  {
    self->update();
    self->worldwin->redraw();
  }
  usleep(0);
  
  return;
}


// Parse command-line options
int PlanGraphEditor::parseCmdLine(int argc, char **argv)
{
  plangraph_editor_cmdline_init(&this->cmdline);
      
  // Run parser
  if (plangraph_editor_cmdline(argc, argv, &this->cmdline) != 0)
  {
    plangraph_editor_cmdline_print_help();
    return -1;
  }

  // Load configuration file, if given; settings in the config file do not
  // override the command line options.
  if (this->cmdline.inputs_num > 0)
  {
    if (plangraph_editor_cmdline_configfile(this->cmdline.inputs[0], &this->cmdline,
                                        false, false, false) != 0)
    {
      plangraph_editor_cmdline_print_help();
      return -1;
    }
  }

  // Do some checks
  if (!this->cmdline.rndf_given)
    return ERROR("rndf must be specified");
  
  return 0;
}


// Initialize the planner
int PlanGraphEditor::init()
{
  // Create the graph
  this->plan = new PlanGraph();
  
  // Set up some convenience pointers
  this->rndf = NULL;
  this->road = NULL;
  this->plan = NULL;
  this->reloadRoad();
  this->reloadPlan();

  // Create the graph builder
  // TODO this->builder = new PlanGraphBuilder(this->plan);

  this->roiSize = 512;

  // Load up imagery
  assert(this->rndf);
  this->drawAerial.load("/dgc/aerial-images/", this->rndf->siteN, this->rndf->siteE);
  
  return 0;
}


// Finalize the planner
int PlanGraphEditor::fini()
{  
  // Clean up
  delete this->builder;
  this->builder = NULL;
  delete this->plan;
  this->plan = NULL;
  
  return 0;  
}


// Update 
int PlanGraphEditor::update()
{
  // Re-load the road
  this->reloadRoad();
  
  // Update the ROI
  if (this->plan)
    this->plan->updateROI(this->roiPos.x, this->roiPos.y, this->roiSize);

  this->dirty = true; 
  
  return 0;
}

   
// Re-load the road graph
int PlanGraphEditor::reloadRoad()
{
  if (this->road)
    delete this->road;
  this->road = new RoadGraph();
  this->rndf = &this->road->rndf;
  
  // Re-load the road graph
  this->road->load(this->cmdline.rndf_arg, this->cmdline.tweaks_arg);
  
  return 0;
}


// Re-load the plan graph
int PlanGraphEditor::reloadPlan()
{
  char filename[1024];
  
  if (this->plan)
    delete this->plan;
  this->plan = new PlanGraph();

  // Load the graph
  snprintf(filename, sizeof(filename), "%s.pg", this->cmdline.rndf_arg);  
  if (this->plan->load(filename) != 0)
    return ERROR("unable to load %s", filename);

  return 0;
}


int main(int argc, char *argv[])
{
  PlanGraphEditor *app;
 
  // Initialize GLUT calls
  glutInit(&argc, argv);

  app = new PlanGraphEditor();

  // Parse cmd line
  if (app->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initilize the app
  if (app->init() != 0)
    return -1;

  // Initialize gui
  if (app->initGUI(1024, 768) != 0)
    return -1;

  // Idle callback
  Fl::add_idle((void (*) (void*)) PlanGraphEditor::onIdle, app);
    
  // Run
  app->mainwin->show();
  while (!app->quit)
    Fl::wait();

  MSG("exiting");
  
  // Clean up
  app->fini();
  app->finiGUI();
  delete app;
 
  MSG("exited cleanly");
  
  return 0;
}
