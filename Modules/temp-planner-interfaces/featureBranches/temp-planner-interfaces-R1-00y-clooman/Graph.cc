
/* 
 * Desc: Trajectory graph data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <vector>

#include "Graph.hh"
#include "Quadtree.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Default constructor
Graph_t::Graph_t(int maxNodes, int maxArcs)
{
  this->numNodes = 0;
  this->maxNodes = maxNodes;
  this->nodes = new GraphNode[this->maxNodes];

  this->numArcs = 0;
  this->maxArcs = maxArcs;
  this->arcs = new GraphArc[this->maxArcs];

  this->numStaticNodes = 0;
  this->numStaticArcs = 0;

  this->quadtree = NULL;
  this->is_quadtree_initialized = false;

  return;
}

// Destructor
Graph_t::~Graph_t()
{
  delete this->quadtree;
  delete [] this->arcs;
  delete [] this->nodes;
  
  return;
}

// Get the number of nodes
int Graph_t::getNodeCount()
{
  return this->numNodes;
}

// Get a node from an index
GraphNode *Graph_t::getNode(int index)
{
  if (index < 0 || index >= this->numNodes)
    return NULL;
  return this->nodes + index;
}

// Create a new node
GraphNode *Graph_t::createNode()
{
  GraphNode *node;

  if (this->numNodes >= this->maxNodes)
    return NULL;

  // Create the node
  node = this->nodes + this->numNodes;
  memset(node, 0, sizeof(*node));  
  node->index = this->numNodes;
  node->quadtree = NULL;
  node->outFirst = NULL;
  node->outLast = NULL;
  node->inFirst = NULL;
  node->inLast = NULL;
  node->startNode = NULL;
  node->endNode = NULL;
  node->leftNode = NULL;
  node->rightNode = NULL;
  node->railId = 0;
  this->numNodes++;
  
  return node;
}

// Get the number of arcs
int Graph_t::getArcCount()
{
  return this->numArcs;
}

// Get an arc from a node and arc index
GraphArc *Graph_t::getArc(int index)
{
  // Make sure the arc exists
  if (index < 0 || index >= this->numArcs)
    return NULL;
  return this->arcs + index;
}

// Create an arc between two nodes
GraphArc *Graph_t::createArc(int indexa, int indexb)
{
  GraphNode *nodeA, *nodeB;
  GraphArc *arc;
  double dist;

  // Make sure both nodes exist
  nodeA = this->getNode(indexa);
  if (!nodeA)
    return NULL;
  nodeB = this->getNode(indexb);
  if (!nodeB)
    return NULL;

  // Check for an existing arc between these nodes
  for (arc = nodeA->outFirst; arc != NULL; arc = arc->outNext)
  {
    if (arc->nodeB == nodeB)
      return arc;
  }

  if (this->numArcs >= this->maxArcs)
    return NULL;

  // Create a new arc
  arc = this->arcs + this->numArcs;
  memset(arc, 0, sizeof(*arc));  
  arc->index = this->numArcs++;
  arc->nodeA = nodeA;
  arc->nodeB = nodeB;

  // Set the default plan distance, in mm
  dist = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
  arc->planDist = (int) (dist * 1000);
  // MSG("arc %d %d, %d", indexa, indexb, arc->planDist);
  assert(arc->planDist >= 0);

  // Update the linked list for outgoing arcs from nodeA
  this->insertOutArc(nodeA, arc);

  // Update the linked list for incoming arcs from nodeB
  this->insertInArc(nodeB, arc);

  // MSG("arc %d %d : %d %d", indexa, indexb,
  //    this->numOutArcs[indexa],
  //    this->numInArcs[indexb]);
  
  return arc;
}

// Insert arc into list of outgoing arcs for this node
int Graph_t::insertOutArc(GraphNode *node, GraphArc *arc)
{
  arc->outPrev = node->outLast;
  if (!node->outFirst)
  {
    node->outFirst = arc;
    node->outLast = arc;
  }
  else if (node->outLast)
  {
    node->outLast->outNext = arc;
    node->outLast = arc;
  }
  
  return 0;
}

// Remove arc from list of outgoing arcs for this node
int Graph_t::removeOutArc(GraphNode *node, GraphArc *arc)
{
  if (arc->outPrev)
    arc->outPrev->outNext = arc->outNext;
  if (arc->outNext)
    arc->outNext->outPrev = arc->outPrev;

  if (node->outFirst == arc)
    node->outFirst = arc->outNext;
  if (node->outLast == arc)
    node->outLast = arc->outPrev;

  return 0;
}

// Insert arc into list of ingoing arcs for this node
int Graph_t::insertInArc(GraphNode *node, GraphArc *arc)
{
  arc->inPrev = node->inLast;
  if (!node->inFirst)
  {
    node->inFirst = arc;
    node->inLast = arc;
  }
  else if (node->inLast)
  {
    node->inLast->inNext = arc;
    node->inLast = arc;
  }
  
  return 0;
}

// Remove arc from list of ingoing arcs for this node
int Graph_t::removeInArc(GraphNode *node, GraphArc *arc)
{
  if (arc->inPrev)
    arc->inPrev->inNext = arc->inNext;
  if (arc->inNext)
    arc->inNext->inPrev = arc->inPrev;

  if (node->inFirst == arc)
    node->inFirst = arc->inNext;
  if (node->inLast == arc)
    node->inLast = arc->inPrev;

  return 0;
}

// Get a node from an RNDF id
GraphNode *Graph_t::getNodeFromRndfId(int segmentId, int laneId, int waypointId)
{
  int i;
  GraphNode *node;

  for (i = 0; i < this->numNodes; i++)
  {
    node = this->nodes + i;
    if (node->isWaypoint &&
        node->segmentId == segmentId &&
        node->laneId == laneId &&
        node->waypointId == waypointId)
      return node;
  }
  
  return NULL;
}

// Get a node from a checkpoint id  
GraphNode *Graph_t::getNodeFromCheckpointId(int checkpointId)
{
  int i;
  GraphNode *node;
  for (i = 0; i < this->numNodes; i++)
  {
    node = this->nodes + i;
    if (node->isCheckpoint && node->checkpointId == checkpointId)
      return node;
  }  
  return NULL;
}

// Get the next node for lane changing
GraphNode *Graph_t::getNextChangeNode(const GraphNode *node)
{
  GraphArc *arc;

  for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
  {
    if (arc->type != GRAPH_ARC_NODE) continue;

    if (arc->nodeB->type == GRAPH_NODE_CHANGE)
      return arc->nodeB;
  }

  return NULL; 
}

// Get the next node for lane following
GraphNode *Graph_t::getNextLaneNode(const GraphNode *node)
{
  GraphArc *arc;
  
  for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
  {
    if (arc->type != GRAPH_ARC_NODE) continue;

    if (arc->nodeB->type == GRAPH_NODE_LANE)
      return arc->nodeB;
  }
  
  return NULL;
}

// Get the previous node for lane changing
GraphNode *Graph_t::getPrevChangeNode(const GraphNode *node)
{
  GraphArc *arc;

  for (arc = node->inFirst; arc != NULL; arc = arc->inNext)
  {
    if (arc->type != GRAPH_ARC_NODE) continue;

    if (arc->nodeA->type == GRAPH_NODE_CHANGE)
      return arc->nodeA;
  }
  
  return NULL;
}

// Get the previous node for lane following
GraphNode *Graph_t::getPrevLaneNode(const GraphNode *node)
{
  GraphArc *arc;

  for (arc = node->inFirst; arc != NULL; arc = arc->inNext)
  {
    if (arc->type != GRAPH_ARC_NODE) continue;

    if (arc->nodeA->type == GRAPH_NODE_LANE)
      return arc->nodeA;
  }
  
  return NULL;
}

GraphNode *Graph_t::getNearestNode(vec3_t pos, int typeMask, double maxDist) {
  return getNearestNode(pos, typeMask, maxDist, 0);
}

// Get the nearest node for the given type mask
GraphNode *Graph_t::getNearestNode(vec3_t pos, int typeMask, double maxDist, int direction)
{
  GraphNode *node, *minNode;
  double dx, dy, dD, minD;
  minD = INFINITY;
  minNode = NULL;

  if (typeMask & GRAPH_NODE_VOLATILE == typeMask) {
    for (int i = numStaticNodes; i < this->numNodes; i++) {
      node = this->nodes + i;

      dx = node->pose.pos.x - pos.x;
      dy = node->pose.pos.y - pos.y;
      dD = dx * dx + dy * dy;

      if (dD < minD) {
        minD = dD;
        minNode = node;
      }
    }
  } else if (this->is_quadtree_initialized) {
    vector<GraphNode *> nodes;
    double x_min, x_max, y_min, y_max;

    x_min = pos.x - maxDist;
    x_max = pos.x + maxDist;
    y_min = pos.y - maxDist;
    y_max = pos.y + maxDist;
    this->quadtree->get_all_nodes(nodes, x_min, x_max, y_min, y_max);

    for (unsigned int i=0; i<nodes.size(); i++) {
      node = nodes[i];
      if ((node->type & typeMask) == 0) continue;
      if (direction != 0 && node->direction != direction) continue;

      dx = node->pose.pos.x - pos.x;
      dy = node->pose.pos.y - pos.y;
      dD = dx * dx + dy * dy;
  
      if (dD < minD) {
        minD = dD;
        minNode = node;
      }
    }
    if (typeMask & GRAPH_NODE_VOLATILE) {
      for (int i = this->numStaticNodes; i < this->numNodes; i++) {
        node = this->nodes + i;

        dx = node->pose.pos.x - pos.x;
        dy = node->pose.pos.y - pos.y;
        dD = dx * dx + dy * dy;

        if (dD < minD) {
          minD = dD;
          minNode = node;
        }
      }
    }
  } else {
    for (int i = 0; i < this->numNodes; i++) {
      node = this->nodes + i;
      if ((node->type & typeMask) == 0) continue;
      if (direction != 0 && node->direction != direction) continue;

      dx = node->pose.pos.x - pos.x;
      dy = node->pose.pos.y - pos.y;
      dD = dx * dx + dy * dy;

      if (dD < minD) {
        minD = dD;
        minNode = node;
      }
    }
  }
  
  return minNode;
}

// Freeze the current graph.
void Graph_t::freezeStatic()
{
  this->numStaticNodes = this->numNodes;
  this->numStaticArcs = this->numArcs;
  
  return;
}

// Get the number of static nodes.
int Graph_t::getStaticNodeCount()
{
  return this->numStaticNodes;
}


// Clear volatile components.
void Graph_t::clearVolatile()
{
  GraphNode *node;
  GraphArc *arc;

  // Remove any volatile arcs by clipping them from the linked lists.
  while (this->numArcs > this->numStaticArcs) {
    arc = this->arcs + this->numArcs - 1;
    this->removeOutArc(arc->nodeA, arc);
    this->removeInArc(arc->nodeB, arc);
    this->numArcs -= 1;
  }

  // Remove volatile nodes one at a time
  while (this->numNodes > this->numStaticNodes) {
    node = this->nodes + this->numNodes - 1;
    if (node->quadtree) {
      node->quadtree->remove_node(node);
    }
    this->numNodes -= 1;
  }
  
  return;
}


