#include "Quadtree.hh"

#include <assert.h>
#include <iostream>
#include <sstream>
#include <fstream>

Quadtree::Quadtree()
{
  x_min =  1000000;
  x_max = -1000000;
  y_min =  1000000;
  y_max = -1000000;
  has_child = false;
  nodes.clear();
}

Quadtree::Quadtree(double x_min, double x_max, double y_min, double y_max)
{
  this->x_min = x_min;
  this->x_max = x_max;
  this->y_min = y_min;
  this->y_max = y_max;
  has_child = false;
  nodes.clear();
}

Quadtree::~Quadtree()
{
  if (has_child) {
    for (int i=0; i<4; i++) {
      delete childs[i];
    }
  }
  nodes.clear();
}

void Quadtree::create_tree(Graph_t *graph)
{
  GraphNode *node;
  double x, y;

  /* destroy childs */
  /* if (has_child) { 
    for (int i=0; i<4; i++) {
      delete childs[i];
    }
  } */
  nodes.clear();

  /* Create root quad */
  for (int i=0; i<graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    x = node->pose.pos.x;
    y = node->pose.pos.y;
    if (x < x_min) x_min = x;
    if (y < y_min) y_min = y;
    if (x > x_max) x_max = x;
    if (y > y_max) y_max = y;
  }
  x_max += 100; // To account for GPS shifts
  y_max += 100;
  x_min -= 100;
  y_min -= 100;

  /* Populate the tree */
  for (int i=0; i<graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    add_node(node);
  }
}

Quadtree * Quadtree::find_leaf_quad(double x, double y)
{
  if (!has_child) return this;
  return childs[find_quad_child(x,y)]->find_leaf_quad(x,y);
}

unsigned int Quadtree::find_quad_child(double x, double y)
{
  int num;

  if (y < HALF_Y) num = 0;
  else num = 2;
  if (x >= HALF_X) num++;

  return num;
}

void Quadtree::add_node(GraphNode *node)
{
  double x = node->pose.pos.x;
  double y = node->pose.pos.y;
  add_node(node, x, y);
}

void Quadtree::add_node(GraphNode *node, double nx, double ny)
{
  double x, y;

  if (nx < x_min || ny < y_min || nx >= x_max || ny >= y_max) return;
  Quadtree *quad = find_leaf_quad(nx,ny);

  quad->nodes.push_back(node);
  if (quad->nodes.size() <= MAX_NODES_IN_QUAD+1) {
    return;
  }

  /* There are too many nodes in that quad, divide up the space */
  quad->childs[0] = new Quadtree(quad->x_min, HALF_QUAD_X(quad), quad->y_min, HALF_QUAD_Y(quad));
  quad->childs[1] = new Quadtree(HALF_QUAD_X(quad), quad->x_max, quad->y_min, HALF_QUAD_Y(quad));
  quad->childs[2] = new Quadtree(quad->x_min, HALF_QUAD_X(quad), HALF_QUAD_Y(quad), quad->y_max);
  quad->childs[3] = new Quadtree(HALF_QUAD_X(quad), quad->x_max, HALF_QUAD_Y(quad), quad->y_max);
  quad->has_child = true;

  /* re-divide the nodes among the childs */
  Quadtree *tmp_quad;
  for (unsigned int i=0; i<quad->nodes.size(); i++) {
    if (quad->nodes[i] == node) {
      tmp_quad = quad->childs[quad->find_quad_child(nx, ny)];
      tmp_quad->add_node(node);
    } else {
      x = quad->nodes[i]->pose.pos.x;
      y = quad->nodes[i]->pose.pos.y;
      tmp_quad = quad->childs[quad->find_quad_child(x, y)];
      tmp_quad->add_node(quad->nodes[i]);
    }
  }

  /* clear the nodes list */
  quad->nodes.clear();
}

void Quadtree::remove_node(GraphNode *node)
{
  double x = node->pose.pos.x;
  double y = node->pose.pos.y;
  Quadtree *quad = find_leaf_quad(x,y);

  // find the node in the vector
  for (unsigned int i = 0; i < quad->nodes.size(); i++) {
    if (quad->nodes[i] == node) {
      quad->nodes.erase(quad->nodes.begin() + i);
      break;
    }
  }

  // don't bother rearranging the quad tree (erasing is not an action that it taken too often)
}

void Quadtree::move_node(GraphNode *node, double nx, double ny)
{
  double x = node->pose.pos.x;
  double y = node->pose.pos.y;
  Quadtree *quad = find_leaf_quad(x,y);

  /* nothing to do, the node is still in the same quad */
  if (nx >= quad->x_min && ny >= quad->y_min && nx < quad->x_max && ny < quad->y_max) return;

  /* could be more efficient by looking at the parent */
  quad->remove_node(node);
  add_node(node, nx, ny);
}

bool Quadtree::is_inside(double x_min, double x_max, double y_min, double y_max)
{
  if (x_max < this->x_min) return false;
  if (y_max < this->y_min) return false;
  if (x_min > this->x_max) return false;
  if (y_min > this->y_max) return false;
  return true;
}

/**
 * Make sure that nodes is empty before calling this function
 */
void Quadtree::get_all_nodes(vector<GraphNode *> &nodes, double x_min, double x_max, double y_min, double y_max)
{
  if (!is_inside(x_min, x_max, y_min, y_max))
    return;

  if (!has_child) {
    for (unsigned int i = 0; i<this->nodes.size(); i++) {
        nodes.push_back(this->nodes[i]);
    }
    return;
  }

  for (unsigned int i=0; i<4; i++) {
    childs[i]->get_all_nodes(nodes, x_min, x_max, y_min, y_max);
  }
}

void Quadtree::print()
{
  if (has_child) {
    for (unsigned int i=0; i<4; i++) {
      childs[i]->print();
    }
    return;
  }

  ofstream file_out;
  file_out.open("plot_quad.m", ios_base::app);

  ostringstream nodes_x, nodes_y;
  nodes_x << "nodes_x = [ ";
  nodes_y << "nodes_y = [ ";
  for (unsigned int i = 0; i<nodes.size(); i++) {
    nodes_x << nodes[i]->pose.pos.x << " ";
    nodes_y << nodes[i]->pose.pos.y << " ";
  }
  nodes_x << " ];" << endl;
  nodes_y << " ];" << endl;

  file_out << nodes_x.str() << nodes_y.str();
  file_out << "plot(nodes_x, nodes_y, 'b.'); hold on;" << endl;
  file_out << "x = [ " << x_min << " " << x_max << " " << x_max << " " << x_min << " " << x_min << " ];" << endl;
  file_out << "y = [ " << y_min << " " << y_min << " " << y_max << " " << y_max << " " << y_min << " ];" << endl;
  file_out << "plot(x, y, 'k-'); hold on;" << endl << endl;
  file_out.close();
}
