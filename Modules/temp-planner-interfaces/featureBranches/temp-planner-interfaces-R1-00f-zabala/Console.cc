/*!
 * \file Console.cc
 * \brief Source code for the console of the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "Console.hh"
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdarg.h>
#include "AliceStateHelper.hh"
#include "frames/point2.hh"
#include "map/Map.hh"

/* Properties of state machine window */
#define FSM_WIN_X      0
#define FSM_WIN_Y      1
#define FSM_WIN_HEIGHT 6
#define FSM_WIN_WIDTH  40

/* Properties of the corridor window */
#define TRAJ_WIN_X      FSM_WIN_X
#define TRAJ_WIN_Y      (FSM_WIN_Y + FSM_WIN_HEIGHT)
#define TRAJ_WIN_HEIGHT 9
#define TRAJ_WIN_WIDTH  FSM_WIN_WIDTH

/* Properties of the state window */
#define STATE_WIN_X      (FSM_WIN_X + FSM_WIN_WIDTH)
#define STATE_WIN_Y      FSM_WIN_Y
#define STATE_WIN_HEIGHT 12
#define STATE_WIN_WIDTH  40

/* Properties of the inter window */
#define INTER_WIN_X      (FSM_WIN_X + FSM_WIN_WIDTH)
#define INTER_WIN_Y      (STATE_WIN_Y + STATE_WIN_HEIGHT)
#define INTER_WIN_HEIGHT 8
#define INTER_WIN_WIDTH  40

/* Properties of the message window */
#define MSG_WIN_X      FSM_WIN_X
#define MSG_WIN_Y      (INTER_WIN_Y + INTER_WIN_HEIGHT)
#define MSG_WIN_HEIGHT 8
#define MSG_WIN_WIDTH  (STATE_WIN_X - FSM_WIN_X + STATE_WIN_WIDTH)
#define MAX_MSG        (MSG_WIN_HEIGHT - 2)
#define MSG_LENGTH     (MSG_WIN_WIDTH-3)

WINDOW *Console::fsm_win = NULL;
WINDOW *Console::traj_win = NULL;
WINDOW *Console::state_win = NULL;
WINDOW *Console::inter_win = NULL;
WINDOW *Console::msg_win = NULL;
char Console::messages[MAX_MSG][MSG_LENGTH];
bool Console::initialized = false;

#define FSM_WIN_ID    1
#define TRAJ_WIN_ID   2
#define STATE_WIN_ID  4
#define INTER_WIN_ID  8
#define MSG_WIN_ID   16
#define STD_WIN_ID   32

pthread_mutex_t Console::refresh_mutex;
pthread_cond_t Console::refresh_notifier;
uint32_t Console::refresh_win = 0;

/**
 * @brief Initializes the console display
 */
void Console::init()
{
  initialized = true;

  initscr();
  noecho();
  cbreak();
  curs_set(0);
  refresh();
    
  attron(A_BOLD);
  mvprintw(0, 0, " Planner running at");
  attroff(A_BOLD);

  /* Create a window for the state machines */
  fsm_win = newwin(FSM_WIN_HEIGHT, FSM_WIN_WIDTH, FSM_WIN_Y, FSM_WIN_X);
  wborder(fsm_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(fsm_win);
  mvwprintw(fsm_win, 0, 2, " State Machines ");
  mvwprintw(fsm_win, 2, 2, "FSM State: ");
  mvwprintw(fsm_win, 3, 2, "FSM Flag: ");
  wrefresh(fsm_win);

  /* Create a window for the Trajectory */
  traj_win = newwin(TRAJ_WIN_HEIGHT, TRAJ_WIN_WIDTH, TRAJ_WIN_Y, TRAJ_WIN_X);
  wborder(traj_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(traj_win);
  mvwprintw(traj_win, 0, 2, " Trajectory ");
  attron(A_UNDERLINE);
  mvwprintw(traj_win, 2, 11, "Initial");
  mvwprintw(traj_win, 2, 23, "Final");
  attroff(A_UNDERLINE);
  mvwprintw(traj_win, 3, 9, "X");
  mvwprintw(traj_win, 4, 9, "Y");
  mvwprintw(traj_win, 5, 2, "Velocity");
  mvwprintw(traj_win, 6, 3, "Heading");
  wrefresh(traj_win);

  /* Create a window for the state */
  state_win = newwin(STATE_WIN_HEIGHT, STATE_WIN_WIDTH, STATE_WIN_Y, STATE_WIN_X);
  wborder(state_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(state_win);
  mvwprintw(state_win, 0, 2, " Alice State ");
  mvwprintw(state_win, 2, 2, "Position: ");
  mvwprintw(state_win, 3, 2, "Velocity: ");
  mvwprintw(state_win, 5, 2, "Desired Lane: ");
  mvwprintw(state_win, 6, 2, "Turning: ");
  mvwprintw(state_win, 8, 2, "Last Wpt: ");
  mvwprintw(state_win, 9, 2, "Next Wpt: ");
  wrefresh(state_win);

  /* Create a window for the intersection */
  inter_win = newwin(INTER_WIN_HEIGHT, INTER_WIN_WIDTH, INTER_WIN_Y, INTER_WIN_X);
  wborder(inter_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(inter_win);
  mvwprintw(inter_win, 0, 2, " Intersection ");
  mvwprintw(inter_win, 2, 2, "Safety flag: ");
  mvwprintw(inter_win, 3, 2, "Cab mode flag: ");
  mvwprintw(inter_win, 4, 2, "Veh with precedence: ");
  mvwprintw(inter_win, 5, 2, "Status: ");
  wrefresh(inter_win);

  /* Create a window for the messages */
  msg_win = newwin(MSG_WIN_HEIGHT, MSG_WIN_WIDTH, MSG_WIN_Y, MSG_WIN_X);
  wborder(msg_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(msg_win);
  mvwprintw(msg_win, 0, 2, " Messages ");
  wrefresh(msg_win);
  for (int i=0; i<MAX_MSG; i++) {
    messages[i][0] = '\0';
  }

  /* Create the condition variable */
  pthread_mutex_init(&refresh_mutex, NULL);
  pthread_cond_init(&refresh_notifier, NULL);
}

/**
 * @brief Refreshes the console display
 */
void Console::refresh()
{
  if (!initialized) return;

  touchwin(stdscr);
  touchwin(fsm_win);
  touchwin(traj_win);
  touchwin(msg_win);
  touchwin(state_win);
  touchwin(inter_win);
  wrefresh(stdscr);
  wrefresh(fsm_win);
  wrefresh(traj_win);
  wrefresh(state_win);
  wrefresh(inter_win);
  wrefresh(msg_win);
}

/**
 * @brief Frees memory
 */
void Console::destroy()
{
  if (!initialized) return;

  delwin(fsm_win);
  delwin(traj_win);
  delwin(state_win);
  delwin(msg_win);
  endwin();

  pthread_cond_destroy(&refresh_notifier);
  pthread_mutex_destroy(&refresh_mutex);
}

/**
 * @brief Displays the cycle time and make an average
 */
void Console::updateRate(double time) {
  static double total_time;
  static unsigned int cnt = 1;

  if (!initialized) return;
  total_time += time;

  attron(A_BOLD);
  mvprintw(0, 0, " Planner running at %.3f (%.3f)     ", time, total_time/(double)cnt);
  attroff(A_BOLD);

  cnt++;

  /* Sets the window the refreshing thread has to refresh and signal */
  refresh_win |= STD_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays the trajectory information
 */
void Console::updateTrajectory(CTraj *traj)
{
  int size = traj->getNumPoints();

  if (!initialized) return;
  if (size < 1) return;

  point2 initial(traj->getNorthing(0),traj->getEasting(0));
  point2 final(traj->getNorthing(size-1),traj->getEasting(size-1));

  double ivel = traj->getSpeed(0);
  double fvel = traj->getSpeed(size-1);

  double iheading = 0;
  double fheading = 0;

  mvwprintw(traj_win, 3, 11, "%7.2f", initial.x); /* Initial - X */
  mvwprintw(traj_win, 4, 11, "%7.2f", initial.y); /* Initial - Y */
  mvwprintw(traj_win, 5, 11, "%7.2f", ivel);      /* Initial - Velocity */
  mvwprintw(traj_win, 6, 11, "%7.2f", iheading);  /* Initial - Heading */
  mvwprintw(traj_win, 3, 23, "%7.2f", final.x);   /* Final - X */
  mvwprintw(traj_win, 4, 23, "%7.2f", final.y);   /* Final - Y */
  mvwprintw(traj_win, 5, 23, "%7.2f", fvel);      /* Final - Velocity */
  mvwprintw(traj_win, 6, 23, "%7.2f", fheading);  /* Final - Heading */

  refresh_win |= TRAJ_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays Alice state
 */
void Console::updateState(VehicleState &vehState)
{
  if (!initialized) return;

  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double vel = AliceStateHelper::getVelocityMag(vehState);
  LaneLabel lane = AliceStateHelper::getDesiredLaneLabel();

  stringstream str;
  str << lane;

  mvwprintw(state_win, 2, 16, "[ %7.2f, %7.2f ]", pos.x, pos.y); /* Position */
  mvwprintw(state_win, 3, 16, "%.2f", vel);                      /* Velocity */
  mvwprintw(state_win, 5, 16, "%s", str.str().c_str());          /* Desired Lane */
  
  refresh_win |= STATE_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays intersection information
 */
void Console::updateInter(bool SafetyFlag, bool CabmodeFlag, int CountPrecedence, string Status)
{
  if (!initialized) return;
  
  mvwprintw(inter_win, 2, 24, "%i", SafetyFlag ); /* Safety flag set? */
  mvwprintw(inter_win, 3, 24, "%i", CabmodeFlag ); /* Cabmode flag set? */
  mvwprintw(inter_win, 4, 24, "%i", CountPrecedence ); /* Vehicles with Precedence */
  mvwprintw(inter_win, 5, 24, "%s", Status.c_str() ); /* Is it legal to go? */
  
  refresh_win |= INTER_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays last completed waypoint
 */
void Console::updateLastWpt(PointLabel last)
{
  if (!initialized) return;

  mvwprintw(state_win, 8, 16, "%d.%d.%d      ", last.segment, last.lane, last.point);
  
  refresh_win |= STATE_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays the next waypoint
 */
void Console::updateNextWpt(PointLabel next)
{
  if (!initialized) return;

  mvwprintw(state_win, 9, 16, "%d.%d.%d      ", next.segment, next.lane, next.point);
  
  refresh_win |= STATE_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays turning signal
 */
void Console::updateTurning(int direction)
{
  if (!initialized) return;

  if (direction == -1)
    mvwprintw(state_win, 7, 16, "<--");                  /* Turning */
  else if (direction == 1)
    mvwprintw(state_win, 7, 16, "-->");
  else
    mvwprintw(state_win, 7, 16, "   ");
  
  refresh_win |= STATE_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays FSM state
 */
void Console::updateFSMState(string state)
{
  if (!initialized) return;

  mvwprintw(fsm_win, 2, 17, "%-21s", state.c_str());       /* FSM State */
  
  refresh_win |= FSM_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Displays FSM flag
 */
void Console::updateFSMFlag(string flag)
{
  if (!initialized) return;

  mvwprintw(fsm_win, 3, 17, "%-21s", flag.c_str());      /* FSM flag */
  
  refresh_win |= FSM_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

/**
 * @brief Adds and displays the message
 */
void Console::addMessage(char *format, ...)
{
  static char msg[MSG_LENGTH];

  if (!initialized) return;

  va_list valist;
  va_start(valist, format);
  vsnprintf(msg, MSG_LENGTH, format, valist);
  va_end(valist);

  for (int i = 0; i < MAX_MSG; i++) {
    strncpy(messages[i], messages[i+1], MSG_LENGTH-1);
    messages[i][MSG_LENGTH-1] = '\0';
  }
  strncpy(messages[MAX_MSG-1], msg, MSG_LENGTH-1);
  messages[MAX_MSG-1][MSG_LENGTH-1] = '\0';
  for (int i=0; i<MAX_MSG; i++) {
    mvwprintw(msg_win, 1+i, 2, "%-76s", messages[i]);
  }
  
  refresh_win |= MSG_WIN_ID;
  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_signal(&refresh_notifier);
  pthread_mutex_unlock(&refresh_mutex);
}

void Console::refresh_window()
{
  if (!initialized) return;

  if (refresh_win & FSM_WIN_ID)
    wrefresh(fsm_win);
  if (refresh_win & TRAJ_WIN_ID)
    wrefresh(traj_win);
  if (refresh_win & STATE_WIN_ID)
    wrefresh(state_win);
  if (refresh_win & INTER_WIN_ID)
    wrefresh(inter_win);
  if (refresh_win & MSG_WIN_ID)
    wrefresh(msg_win);
  if (refresh_win & STD_WIN_ID)
    wrefresh(stdscr);
}

void Console::thread_refresh()
{
  if (!initialized) {
    sleep(5);
    return;
  }

  pthread_mutex_lock(&refresh_mutex);
  pthread_cond_wait(&refresh_notifier, &refresh_mutex);
  pthread_mutex_unlock(&refresh_mutex);
  refresh_window();
}
