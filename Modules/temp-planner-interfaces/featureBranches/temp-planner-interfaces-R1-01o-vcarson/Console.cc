/*!
 * \file Console.cc
 * \brief Source code for the console of the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "Console.hh"
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdarg.h>
#include "AliceStateHelper.hh"
#include "frames/point2.hh"
#include "map/Map.hh"


bool Console::initialized = false;

// variables for Trajectory
double Console::traj_x=0.0;
double Console::traj_y=0.0;
double Console::traj_vel=0.0;
double Console::traj_f_x=0.0;
double Console::traj_f_y=0.0;
double Console::traj_f_vel=0.0;

// variables for Prediction
int Console::pred_numbers=0;

// variables for Alice State
double Console::state_velocity=0.0;

// variables for Alice State
double Console::distToStop=INFINITY;

// variables for messages
char Console::messages[MAX_MSG][MSG_LENGTH];

// variables for Rates
double Console::rate1;
double Console::rate2;

CSparrowHawk* Console::display = &SparrowHawk();

// variables for keyboard commands
bool Console::resetIntersectionFlag = false;
bool Console::togglePredictionFlag = false;
bool Console::resetStateFlag = false;
bool Console::toggleIntersectionSafetyFlag = false;
bool Console::toggleIntersectionCabmodeFlag = false;

/**
 * @brief Initializes the console display
 */
void Console::init()
{
  display->add_page(plannertable, "planner");

  // bind variables for Rate
  display->rebind("rate1", &rate1);
  display->set_readonly("rate1");
  display->rebind("rate2", &rate2);
  display->set_readonly("rate2");

   // bind variables for Trajectory
  display->rebind("traj_x", &traj_x);
  display->set_readonly("traj_x");
  display->rebind("traj_y", &traj_y);
  display->set_readonly("traj_y");
  display->rebind("traj_vel", &traj_vel);
  display->set_readonly("traj_vel");
  display->rebind("traj_f_x", &traj_f_x);
  display->set_readonly("traj_f_x");
  display->rebind("traj_f_y", &traj_f_y);
  display->set_readonly("traj_f_y");
  display->rebind("traj_f_vel", &traj_f_vel);
  display->set_readonly("traj_f_vel");

   // bind variables for Alice Status
  display->rebind("state_velocity", &state_velocity);
  display->set_readonly("state_velocity");

   // bind variables for Prediction
  display->rebind("pred_obst", &pred_numbers);
  display->set_readonly("pred_obst");

   // bind variables for Prediction
  display->rebind("dist_stop", &distToStop);
  display->set_readonly("dist_stop");

  // assign keys
  display->set_keymap((int)'i', &resetIntersection);
  display->set_keymap((int)'p', &togglePrediction);
  display->set_keymap((int)'s', &resetState);
  display->set_keymap((int)'y', &toggleIntersectionSafety);
  display->set_keymap((int)'c', &toggleIntersectionCabmode);

  initialized = true;

  for (unsigned int i=0; i<MAX_MSG; i++)
    strncpy(messages[i]," ",MSG_LENGTH);
}

/**
 * @brief Frees memory
 */
void Console::destroy()
{
}

// sets and query resetIntersectionFlag
void Console::resetIntersection()
{
  resetIntersectionFlag = true;
}

bool Console::queryResetIntersectionFlag()
{
  bool flag = resetIntersectionFlag;
  resetIntersectionFlag = false;
  return flag;
}

// sets and query togglePredictionFlag
void Console::togglePrediction()
{
  togglePredictionFlag = true;
}

bool Console::queryTogglePredictionFlag()
{
  bool flag = togglePredictionFlag;
  togglePredictionFlag = false;
  return flag;
}

// sets and query resetStateFlag
void Console::resetState()
{
  resetStateFlag = true;
}

bool Console::queryResetStateFlag()
{
  bool flag = resetStateFlag;
  resetStateFlag = false;
  return flag;
}

// sets and query toggleIntersectionSafetyFlag
void Console::toggleIntersectionSafety()
{
  toggleIntersectionSafetyFlag = true;
}

bool Console::queryToggleIntersectionSafety()
{
  bool flag = toggleIntersectionSafetyFlag;
  toggleIntersectionSafetyFlag = false;
  return flag;
}

// sets and query toggleIntersectionCabmodeFlag
void Console::toggleIntersectionCabmode()
{
  toggleIntersectionCabmodeFlag = true;
}

bool Console::queryToggleIntersectionCabmode()
{
  bool flag = toggleIntersectionCabmodeFlag;
  toggleIntersectionCabmodeFlag = false;
  return flag;
}

/**
 * @brief Displays the cycle time and make an average
 */
void Console::updateRate(double time) {
  static double total_time;
  static unsigned int cnt = 1;

  if (!initialized) return;
  total_time += time;

  rate1 = time;
  rate2 = total_time/(double)cnt;

  cnt++;
}

/**
 * @brief Displays the trajectory information
 */
void Console::updateTrajectory(CTraj *traj)
{
  int size = traj->getNumPoints();

  if (!initialized) return;
  if (size < 1) return;

  traj_x = traj->getNorthing(0);
  traj_y = traj->getEasting(0);
  traj_vel = traj->getSpeed(0);

  traj_f_x = traj->getNorthing(size-1);
  traj_f_y = traj->getEasting(size-1);
  traj_f_vel = traj->getSpeed(size-1);
}

/**
 * @brief Displays Alice state
 */
void Console::updateState(VehicleState &vehState)
{
  if (!initialized) return;

  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);

  stringstream p;

  char c[20];

  sprintf(c,"[ %7.2f, %7.2f ]", pos.x, pos.y);

  display->set_string("state_position", c);
  state_velocity = AliceStateHelper::getVelocityMag(vehState);
}

/**
 * @brief Displays intersection information
 */
void Console::updateInter(bool SafetyFlag, bool CabmodeFlag, int CountPrecedence, string Status)
{
  string inter_safety;
  string inter_cabmode;

  if (!initialized) return;
  
  if (SafetyFlag)
    inter_safety = "SET         ";
  else
    inter_safety = "DEACTIVATED";

  if (CabmodeFlag)
    inter_cabmode = "SET        ";
  else
    inter_cabmode = "DEACTIVATED";

  stringstream cp;
  cp<<CountPrecedence;

  display->set_string("inter_safety", inter_safety.c_str());
  display->set_string("inter_cabmode", inter_cabmode.c_str());
  display->set_string("inter_veh", cp.str().c_str());
  display->set_string("inter_status", Status.c_str());
}

void Console::updateStopline(double dist)
{
  distToStop = dist;
}

/**
 * @brief Displays Prediction information
 */
void Console::updatePred(string Status, int numberObstacles)
{
  if (!initialized) return;
  
  display->set_string("pred_status",Status.c_str());
  pred_numbers = numberObstacles;
}

/**
 * @brief Displays last completed waypoint
 */
void Console::updateLastWpt(PointLabel last)
{
  if (!initialized) return;

  stringstream s;
  s<<last.segment<<"."<<last.lane<<"."<<last.point;

  char c[15];
  sprintf(c,"%-10s",s.str().c_str());

  display->set_string("state_last_wpt", c);
}

/**
 * @brief Displays the next waypoint
 */
void Console::updateNextWpt(PointLabel next)
{
  if (!initialized) return;

  stringstream s;
  s<<next.segment<<"."<<next.lane<<"."<<next.point;

  char c[15];
  sprintf(c,"%-10s",s.str().c_str());

  display->set_string("state_next_wpt", c);
}

/**
 * @brief Displays turning signal
 */
void Console::updateTurning(int direction)
{
  if (!initialized) return;

  string state_turning;

  if (direction == -1)
    state_turning = "<--";                  /* Turning */
  else if (direction == 1)
    state_turning = "-->";
  else
    state_turning = "   ";

  display->set_string("state_turning", state_turning.c_str());
  //  display->set_string("state_turning", state_turning.c_str());
}

/**
 * @brief Displays FSM state
 */
void Console::updateFSMState(string state)
{
  if (!initialized) return;

  char c[20];
  sprintf(c,"%-10s",state.c_str());
  
  display->set_string("FSM_state", c);
}

/**
 * @brief Displays FSM flag
 */
void Console::updateFSMFlag(string flag)
{
  if (!initialized) return;

  char c[20];
  sprintf(c,"%-10s",flag.c_str());

  display->set_string("FSM_flag", c);
}

/**
 * @brief Adds and displays the message
 */
void Console::addMessage(char *format, ...)
{
  static char msg[MSG_LENGTH];

  if (!initialized) return;

   va_list valist;
   va_start(valist, format);
   vsnprintf(msg, MSG_LENGTH, format, valist);
   va_end(valist);

   for (int i = 0; i < MAX_MSG; i++) {
     strncpy(messages[i], messages[i+1], MSG_LENGTH-1);
     messages[i][MSG_LENGTH-1] = '\0';
   }
   strncpy(messages[MAX_MSG-1], msg, MSG_LENGTH-1);
   messages[MAX_MSG-1][MSG_LENGTH-1] = '\0';

   char c[100];
   sprintf(c,"%-77s",messages[0]);
   display->set_string("message1",c);

   sprintf(c,"%-77s",messages[1]);
   display->set_string("message2",c);

   sprintf(c,"%-77s",messages[2]);
   display->set_string("message3",c);

   sprintf(c,"%-77s",messages[3]);
   display->set_string("message4",c);

   sprintf(c,"%-77s",messages[4]);
   display->set_string("message5",c);

   sprintf(c,"%-77s",messages[5]);
   display->set_string("message6",c);
}

