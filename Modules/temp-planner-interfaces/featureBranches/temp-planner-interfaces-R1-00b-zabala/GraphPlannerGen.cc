
/* 
 * Desc: Graph-based planner; functions for graph generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <errno.h>
#include <float.h>

#include <frames/pose3.h>
#include <frames/mat44.h>
#include <rndf/RNDF.hh>
#include <trajutils/maneuver.h>

#include "GraphPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// TODO move to an interolation library 
// Compute Catmull-Rom spline interpolation.
// Based on code from: http://www.lighthouse3d.com/opengl/maths/index.php?catmullrom.
double catmullRomSpline(double x, double v0, double v1, double v2, double v3)
{
  //const double M11 = 0.0;
  const double M12 = 1.0;
  //const double M13 = 0.0;
  //const double M14 = 0.0;
  const double M21 =-0.5;
  //const double M22 = 0.0;
  const double M23 = 0.5;
  //const double M24 = 0.0;
  const double M31 = 1.0;
  const double M32 =-2.5;
  const double M33 = 2.0;
  const double M34 =-0.5;
  const double M41 =-0.5;
  const double M42 = 1.5;
  const double M43 =-1.5;
  const double M44 = 0.5;
  
  double c1,c2,c3,c4;

	c1 =  	      M12*v1;
	c2 = M21*v0          + M23*v2;
	c3 = M31*v0 + M32*v1 + M33*v2 + M34*v3;
	c4 = M41*v0 + M42*v1 + M43*v2 + M44*v3;

	return (((c4*x + c3)*x +c2)*x + c1);
}


// TODO move to an interpolation library
// Generate the interpolated value for a cardinal spline.
float cardinalSpline(float c, float t, float p0, float p1, float p2, float p3)
{
  float m1, m2;
  float ha, hb, hc, hd;

  m1 = (1 - c) * (p2 - p0) / 2;
  m2 = (1 - c) * (p3 - p1) / 2;

  ha =  2*t*t*t - 3*t*t + 1;
  hb =    t*t*t - 2*t*t + t;
  hc = -2*t*t*t + 3*t*t;
  hd =    t*t*t -   t*t;
  
  return ha * p1 + hb * m1 + hc * p2 + hd * m2;  
}


// Generate lane nodes
int GraphPlanner::genLanes(std::RNDF *rndf)
{
  int sn, ln;
  std::Segment *segment;
  std::Lane *lane;

  // Iterate through segments and lanes, adding intermediate
  // nodes between waypoints in the same lane.
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];

      // Generate nodes for driving with traffic
      if (this->genLane(lane, +1) != 0)
        return -1;
      if (this->genLaneTangents(lane) != 0)
        return -1;

      // Generate nodes for driving against traffic
      if (this->genLane(lane, -1) != 0)
        return -1;
      if (this->genLaneTangents(lane) != 0)
        return -1;
    }
  }

  return 0;
}


// Generate a lane
int GraphPlanner::genLane(std::Lane *lane, int direction)
{
  int wn;
  int size;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wa, *wb, *wc, *wd, *we, *wf;
  GraphNode *node;
  GraphArc *arc;
  double stepSize;
  int i, numSteps;
  double dx, dy, dm, s;
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseC, poseD, poseS;    

  // MAGIC
  // Spacing for interpolated lane nodes
  stepSize = 1.0;

  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, 1000); // MAGIC
  
  // List of all waypoints in the lane
  waypoints = lane->getAllWaypoints();
  size = (int) waypoints.size();
  
  for (wn = 0; wn < size; wn++)
  {
    // Pick out six waypoints for interpolation.  The lane nodes will
    // be generated between c and d.
    wa = wb = wc = wd = we = wf = NULL;
    if (direction > 0)
    {
      if (wn - 2 >= 0)
        wa = waypoints[wn - 2];
      if (wn - 1 >= 0)
        wb = waypoints[wn - 1];
      if (true)
        wc = waypoints[wn + 0];
      if (wn + 1 < (int) waypoints.size())
        wd = waypoints[wn + 1];
      if (wn + 2 < (int) waypoints.size())
        we = waypoints[wn + 2];
      if (wn + 3 < (int) waypoints.size())
        wf = waypoints[wn + 3];
    }
    else if (direction < 0)
    {
      if (wn - 2 >= 0)
        wa = waypoints[size - 1 - (wn - 2)];
      if (wn - 1 >= 0)
        wb = waypoints[size - 1 - (wn - 1)];
      if (true)
        wc = waypoints[size - 1 - (wn + 0)];
      if (wn + 1 < (int) waypoints.size())
        wd = waypoints[size - 1 - (wn + 1)];
      if (wn + 2 < (int) waypoints.size())
        we = waypoints[size - 1 - (wn + 2)];
      if (wn + 3 < (int) waypoints.size())
        wf = waypoints[size - 1 - (wn + 3)];
    }
    else
    {
      assert(false);
    }

    // Create a node for waypoint c
    node = this->graph->createNode();
    if (!node)
      return ERROR("unable to create node; try increasing maxNodes");
    assert(node);
    node->type = GRAPH_NODE_LANE;
    node->direction = direction;
    node->isWaypoint = (direction > 0);
    node->isEntry = wc->isEntry();
    node->isExit = wc->isExit();
    node->isStop = wc->isStopSign();
    node->isCheckpoint = wc->isCheckpoint();        
    node->segmentId = wc->getSegmentID();
    node->laneId = wc->getLaneID();
    node->waypointId = wc->getWaypointID();
    node->checkpointId = wc->getCheckpointID();
    node->laneLength = stepSize * 1.35; // MAGIC HACK
    node->laneWidth = lane->getLaneWidth() * 0.0254 * 12; // Width is in feet, urgh
    node->laneRadius = sqrtf(pow(node->laneLength,2) + pow(node->laneWidth,2)) / 2;
    node->pose.pos = vec3_set(wc->getNorthing(), wc->getEasting(), 0);

    // Add to spatial index
    this->graph->setNodePose(node, node->pose);
      
    // Create an arc from the previous node to the new node
    if (wb)
    {
      arc = this->graph->createArc(node->index - 1, node->index);
      if (!arc)
        return ERROR("unable to create arc; try increasing maxArcs");
    }

    // Stop here if d is the last waypoint in the lane.
    if (!wd)
      continue;

    // Distance between nodes
    dx = wd->getNorthing() - wc->getNorthing();
    dy = wd->getEasting() - wc->getEasting();
    dm = sqrt(dx*dx + dy*dy);

    // Number of intermediate nodes
    numSteps = (int) ceil(dm / stepSize);

    if (true && wa && wb && we && wf)
    {
      double tba, tcb, tdc, tdb;
      double ted, tfe, tec;

      tba = atan2(wb->getEasting() - wa->getEasting(),
                  wb->getNorthing() - wa->getNorthing());
      tcb = atan2(wc->getEasting() - wb->getEasting(),
                  wc->getNorthing() - wb->getNorthing());
      tdc = atan2(wd->getEasting() - wc->getEasting(),
                  wd->getNorthing() - wc->getNorthing());
      tdb = atan2(wd->getEasting() - wb->getEasting(),
                  wd->getNorthing() - wb->getNorthing());

      ted = atan2(we->getEasting() - wd->getEasting(),
                  we->getNorthing() - wd->getNorthing());
      tfe = atan2(wf->getEasting() - we->getEasting(),
                  wf->getNorthing() - we->getNorthing());
      tec = atan2(we->getEasting() - wc->getEasting(),
                  we->getNorthing() - wc->getNorthing());
      
      // Initial vehicle pose
      poseC.x = wc->getNorthing();
      poseC.y = wc->getEasting();
      poseC.theta = tdb;
 
      // Final vehicle pose
      poseD.x = wd->getNorthing();
      poseD.y = wd->getEasting();
      poseD.theta = tec;

      /*
        if (acos(cos(tcb - tba)) < 15 * M_PI/180 ||
        acos(cos(tfe - ted)) < 15 * M_PI/180) // MAGIC
        {
        poseC.theta = tcb;
        poseD.theta = ted;        
        }
      */

      // UTTER HACK FOR SITE VISIT
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 10)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 10)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 11)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 11)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 10)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 10)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 11)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 11)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 16)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 16)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 17)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 17)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 16)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 16)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 17)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 17)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 22)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 22)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 23)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 23)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 22)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 22)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 23)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 23)
          poseD.theta = tdc;
      }

      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 18)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 18)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 19)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 19)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 18)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 18)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 19)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 19)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 12)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 12)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 13)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 13)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 12)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 12)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 13)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 13)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 6)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 6)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 7)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 7)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 6)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 6)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 7)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 7)
          poseD.theta = tdc;
      }

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseC, &poseD);
    }
    else if (wb && we)
    {      
      // Initial vehicle pose
      poseC.x = wc->getNorthing();
      poseC.y = wc->getEasting();
      poseC.theta = atan2(wd->getEasting() - wb->getEasting(),
                          wd->getNorthing() - wb->getNorthing());

      // Final vehicle pose
      poseD.x = wd->getNorthing();
      poseD.y = wd->getEasting();
      poseD.theta = atan2(we->getEasting() - wc->getEasting(),
                          we->getNorthing() - wc->getNorthing());      

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseC, &poseD);
    }
    else
    {
      mp = NULL;
    }

    // Step along the maneuver
    for (i = 1; i < numSteps; i++)
    {
      s = (double) i / numSteps;  

      if (mp)
      {
        // Get the vehicle configuration (including steer angle) at this step.
        poseS = maneuver_evaluate_pose(vp, mp, s);
      }
      else
      {
        // Linear interpolation
        poseS.x = s * dx + wc->getNorthing();
        poseS.y = s * dy + wc->getEasting();
      }

      //MSG("pose %d %f %f %f %f", i, s, poseS.x, poseS.y, acos(cos(poseS.theta)));
              
      // Create a interpolated node between waypoints b and c
      node = this->graph->createNode();
      if (!node)
        return ERROR("unable to create node; try increasing maxNodes");      
      node->type = GRAPH_NODE_LANE;
      node->direction = direction;
      node->segmentId = wc->getSegmentID();
      node->laneId = wc->getLaneID();
      node->laneLength = stepSize * 1.35; // MAGIC HACK
      node->laneWidth = lane->getLaneWidth() * 0.0254 * 12; // Width in in feet, urgh
      node->laneRadius = sqrtf(pow(node->laneLength,2) + pow(node->laneWidth,2)) / 2;
      node->pose.pos.x = poseS.x;
      node->pose.pos.y = poseS.y;
      node->pose.pos.z = 0;

      // Add to spatial index
      this->graph->setNodePose(node, node->pose);

      // Create an arc frome the previous node to the new node
      arc = this->graph->createArc(node->index - 1, node->index);
      if (!arc)
        return ERROR("unable to create arc; try increasing maxArcs");      
    }

    if (mp)
    {
      maneuver_free(mp);
      mp = NULL;
    }    
  }

  // Clean up
  free(vp);

  return 0;
}


// Generate lane tangents
int GraphPlanner::genLaneTangents(std::Lane *lane)
{
  int i;
  GraphNode *na, *nb, *nc;
  double th;

  for (i = 0; i < this->graph->getNodeCount(); i++)
  {
    nb = this->graph->getNode(i);
    if (nb->laneId != lane->getLaneID())
      continue;

    na = this->graph->getPrevLaneNode(nb);
    nc = this->graph->getNextLaneNode(nb);

    // Crude tangent calculation across nearby points.
    if (na && nc)
    {
      th = atan2(nc->pose.pos.y - na->pose.pos.y,
                 nc->pose.pos.x - na->pose.pos.x);
    }
    else if (na)
    {
      th = atan2(nb->pose.pos.y - na->pose.pos.y,
                 nb->pose.pos.x - na->pose.pos.x);
    }
    else if (nc)
    {
      th = atan2(nc->pose.pos.y - nb->pose.pos.y,
                 nc->pose.pos.x - nb->pose.pos.x);
    }
    else
      assert(false);

    // Set the node orientation
    nb->pose.rot = quat_from_rpy(0, 0, th);

    // Construct transforms
    pose3_to_mat44d(nb->pose, nb->transGN);
    mat44d_inv(nb->transNG, nb->transGN);
  }

  return 0;
}


// Generate turn maneuvers
int GraphPlanner::genTurns(std::RNDF *rndf)
{
  int sn, ln, wn, vn;
  std::Segment *segmentA, *segmentB;
  std::Lane *laneA, *laneB;
  std::Waypoint *wpA;
  std::GPSPoint *wpB;
  GraphNode *nodeA, *nodeB;
  
  // Iterate through waypoints in the RNDF
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segmentA = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segmentA->getAllLanes().size(); ln++)
    {
      laneA = segmentA->getAllLanes()[ln];
      for (wn = 0; wn < (int) laneA->getAllWaypoints().size(); wn++)
      {
        wpA = laneA->getAllWaypoints()[wn];

        for (vn = 0; vn < (int) wpA->getEntryPoints().size(); vn++)
        {
          wpB = wpA->getEntryPoints()[vn];
          
          segmentB = rndf->getSegment(wpB->getSegmentID());

          // TODO: handle zones
          if (!segmentB)
            continue;          
          assert(segmentB);
          
          laneB = segmentB->getLane(wpB->getLaneID());
          assert(laneB);

          MSG("turn %d.%d.%d %d.%d.%d",
              wpA->getSegmentID(), wpA->getLaneID(), wpA->getWaypointID(),
              wpB->getSegmentID(), wpB->getLaneID(), wpB->getWaypointID());
          
          nodeA = this->graph->getNodeFromRndfId(wpA->getSegmentID(),
                                                 wpA->getLaneID(),
                                                 wpA->getWaypointID());
          assert(nodeA);

          nodeB = this->graph->getNodeFromRndfId(wpB->getSegmentID(),
                                                 wpB->getLaneID(),
                                                 wpB->getWaypointID());

          // Skip turns from zones
          if (!nodeB)
            continue;

          // Try generating a feasible maneuver.  If it works, we are
          // done for this turn.
          if (this->genManeuver(GRAPH_NODE_TURN, nodeA, nodeB, this->kin.maxSteer) == 0)
            continue;
          
          // Generate a tight-turn maneuver (which may not be
          // feasible).  This allows the robot to take the turn by
          // veering outside the lane.
          this->genManeuver(GRAPH_NODE_TURN, nodeA, nodeB, this->kin.maxTurn);

          /* REMOVE?
          // Generate a wide-turn maneuver by increasing the turn radius.
          // We are done as soon as we can find a feasible maneuver.
          while (true)
          {
            if (this->genManeuver(GRAPH_NODE_TURN, nodeA, nodeB, this->kin.maxSteer) == 0)
              break;
            nodeA = this->graph->getPrevLaneNode(nodeA);
            if (!nodeA)
              break;
            nodeB = this->graph->getNextLaneNode(nodeB);
            if (!nodeB)
              break;
          }
          */
        }
      }
    }
  }
  
  return 0;
}


// Generate lane-change maneuvers
int GraphPlanner::genChanges(std::RNDF *rndf)
{
  int numNodes;
  int na, nb;
  GraphNode *nodeA, *nodeB;
  vec3_t pa, pb;
  double dm;
  
  numNodes = this->graph->getNodeCount();

  // Consider all pairs of lane nodes
  for (nb = 0; nb < numNodes; nb++)
  {    
    for (na = 0; na < numNodes; na++)
    {
      nodeA = this->graph->getNode(na);
      nodeB = this->graph->getNode(nb);

      // Consider lane nodes only
      if (!(nodeA->type == GRAPH_NODE_LANE && nodeB->type == GRAPH_NODE_LANE))
        continue;

      // Look for nodes that are the same segment, but different lanes.
      if (nodeA->segmentId != nodeB->segmentId)
        continue;
      if (nodeA->laneId == nodeB->laneId)
        continue;

      // Space out the lane-changes so we dont overload the graph
      if (!(nodeA->index % 4) == 0) // MAGIC
        continue;

      // Nodes must have similar alignment
      pa = vec3_rotate(nodeA->pose.rot, vec3_set(1, 0, 0));
      pb = vec3_rotate(nodeB->pose.rot, vec3_set(1, 0, 0));
      if (acos(vec3_dot(pa, pb)) > 45 * M_PI/180) // MAGIC
        continue;
      
      // Nodes must be close, but not too close
      dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
      if (!(dm > 15 && dm < 16)) // MAGIC HACK
        continue;

      // The destination must be ahead of the source
      pb = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
      if (pb.x < 0)
        continue;

      //MSG("change %d %d.%d %d %d.%d %d",
      //    na, nodeA->segmentId, nodeA->laneId,
      //    nb, nodeB->segmentId, nodeB->laneId,
      //    this->graph->getNodeCount());
      
      // Generate the lane-change maneuver.  It is generally ok
      // if we cannot make a particular maneuver here.
      if (this->genManeuver(GRAPH_NODE_CHANGE, nodeA, nodeB, this->kin.maxSteer) == ENOMEM)
        return -1;
    }
  }
    
  return 0;
}


// Generate K-turn maneuvers
int GraphPlanner::genKTurns()
{
  int na, nb;
  double dm;
  pose3_t pose;
  int numNodes;
  GraphNode *nodeA, *nodeB, *nodeC;

  // Create nodes for the turn extrema
  numNodes = this->graph->getNodeCount();
  for (na = 0; na < numNodes; na++)
  {
    nodeA = this->graph->getNode(na);

    if (nodeA->type != GRAPH_NODE_LANE)
      continue;
    if (nodeA->direction < +1)
      continue;

    // Space out the k-turns so we dont overload the graph
    //if (!(nodeA->index % 5 == 0)) // MAGIC
    //  continue;

    for (nb = 0; nb < numNodes; nb++)
    {
      nodeB = this->graph->getNode(nb);

      if (nodeB->type != GRAPH_NODE_LANE)
        continue;
      if (nodeB->direction > -1)
        continue;

      // Must be from the same segment and lane
      if (nodeA->segmentId != nodeB->segmentId)
        continue;
      if (nodeA->laneId != nodeB->laneId)
        continue;

      // Nodes must have the correct separation
      dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
      if (!(dm > 20 && dm < 21)) // MAGIC
        continue;
      
      //MSG("change %d %d.%d %d %d.%d %d %d",
      //    na, nodeA->segmentId, nodeA->laneId,
      //    nb, nodeB->segmentId, nodeB->laneId,
      //    this->graph->getNodeCount(), this->graph->getArcCount());

      nodeC = this->graph->createNode();
      if (!nodeC)
        return ERROR("unable to create node; try increasing maxNodes");

      // Create a node at an angle to the original node
      assert(nodeC);
      nodeC->type = GRAPH_NODE_KTURN;
      nodeC->direction = 0;
      nodeC->segmentId = nodeA->segmentId;
      nodeC->laneId = nodeA->laneId;
      nodeC->waypointId = 0;
      nodeC->checkpointId = 0;
      nodeC->laneWidth = 0;   
      pose.pos = vec3_set(dm/2, -dm/2, 0);
      pose.rot = quat_from_rpy(0, 0, -M_PI/2);
      nodeC->pose = pose3_mul(nodeA->pose, pose);

      // Generate the lane-change maneuver.  It is generally ok
      // if we cannot make a particular maneuver here.
      if (this->genManeuver(GRAPH_NODE_CHANGE, nodeA, nodeC, this->kin.maxSteer) == ENOMEM)
        return -1;

      // Generate the lane-change maneuver.  It is generally ok
      // if we cannot make a particular maneuver here.
      if (this->genManeuver(GRAPH_NODE_CHANGE, nodeB, nodeC, this->kin.maxSteer) == ENOMEM)
        return -1;
      
      break;
    }
  }

  return 0;
}


// Generate or update the vehicle node and associated maneuvers.
// Unlike the rest of the graph (which is initialized once), this must
// be done continuously.
int GraphPlanner::genVehicleSubGraph(const VehicleState *vehicleState,
                                     const ActuatorState *actuatorState)
{
  int i;
  GraphNode *nodeA, *nodeB;
  vec3_t p;
  double dm;

  // Clear the volatile portion of the graph
  this->graph->clearVolatile();
    
  // Create vehicle node 
  nodeA = this->graph->createNode();
  if (!nodeA)
    return ERROR("unable to create node; try increasing maxNodes");
  nodeA->type = GRAPH_NODE_VOLATILE;    

  // Remember the node for later use.  
  this->vehicleNode = nodeA;

  // Set the node pose
  nodeA->pose.pos = vec3_set(vehicleState->utmNorthing,
                             vehicleState->utmEasting,
                             0);
  nodeA->pose.rot = quat_from_rpy(vehicleState->utmRoll,
                                  vehicleState->utmPitch,
                                  vehicleState->utmYaw);

  // Construct transforms
  pose3_to_mat44d(nodeA->pose, nodeA->transGN);
  mat44d_inv(nodeA->transNG, nodeA->transGN);

  // Compute the current steering angle for maneuver generation
  nodeA->steerAngle = actuatorState->m_steerpos * this->kin.maxSteer;

  // Set RNDF info
  this->genVolatileNode(nodeA);

  int err;
  int numFeasible = 0;
  int numUnfeasible = 0;
  
  // Find destination nodes on the static graph.
  //
  // TODO: This should really be checking things like whether or not
  // lanes are adjacent, and also doing a quick rejection of unfeasible
  // destinations (e.g., on distance and minimum turning radius).    
  for (i = 0; i < this->graph->getNodeCount(); i++)
  {
    nodeB = this->graph->getNode(i);
    if (nodeB == nodeA)
      continue;

    // Must be lane/turn nodes 
    if (!(nodeB->type == GRAPH_NODE_LANE ||
          nodeB->type == GRAPH_NODE_TURN ||
          nodeB->type == GRAPH_NODE_KTURN))
      continue;
          
    // Nodes must be reasonably close, but not too close.
    dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
    if (dm < 2 || dm > 20) // MAGIC
      continue;

    // The destination must be ahead of the source
    p = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
    if (p.x < 0)
      continue;

    /* REMOVE (makes a very marginal difference)
    // Do a quick check to remove obviously unfeasable maneuvers, based
    // on the vehicle turning circle.  This may not be necessary.
    if (true)
    {
    double turnRadius;
    vec3_t pa, pb;
    
    // Vehicle turning radius
    turnRadius = this->kin.wheelBase / tan(this->kin.maxSteer);

    // Final position in the initial frame
    pb = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
  
    // See if the final position lies inside the right turning circle;
    // if so, this position is unreachable in a single turn.
    pa = vec3_set(0, +turnRadius, 0);
    if (vec3_mag(vec3_sub(pb, pa)) < turnRadius)
    continue;

    // See if the final position lies inside the left turning circle;
    // if so, this position is unreachable in a single turn.
    pa = vec3_set(0, -turnRadius, 0);
    if (vec3_mag(vec3_sub(pb, pa)) < turnRadius)
    continue;
    }
    */

    if (true)
    {
      // Generate the maneuver from A to B
      err = this->genManeuver(GRAPH_NODE_VOLATILE, nodeA, nodeB, this->kin.maxSteer);
      if (err == ENOMEM)
        break;
      if (err == 0)
        numFeasible += 1;
      else
        numUnfeasible += 1;
    }

    if (this->cons.enableReverse)
    {
      // Generate the maneuver from B to A (for driving in reverse)
      err = this->genManeuver(GRAPH_NODE_VOLATILE, nodeB, nodeA, this->kin.maxSteer);
      if (err == ENOMEM)
        break;
      if (err == 0)
        numFeasible += 1;
      else
        numUnfeasible += 1;
    }
  }

  //MSG("feasable ratio %d : %d (%0.f%%)",
  //    numFeasible, numUnfeasible, 100.0 * numFeasible / (numFeasible + numUnfeasible));
  
  return 0;
}


// Generate a maneuver linking two nodes
int GraphPlanner::genManeuver(int nodeType, GraphNode *nodeA, GraphNode *nodeB, double maxSteer)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseB;
  VehicleConfiguration configA, config;
  double roll, pitch, yaw;
  double stepSize, dm, s;
  int i, numSteps;
  double theta;
  bool feasible;
  GraphNode *src, *dst;
  GraphArc *arc;

  assert(nodeA);
  assert(nodeB);
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  // Final vehicle pose
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.theta = yaw;

  //MSG("man %f %f : %f %f", poseA.x, poseA.y, poseB.x, poseB.y);

  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, maxSteer);
  
  // Create maneuver object
  mp = maneuver_config2pose(vp, &configA, &poseB);

  // Some maneuvers may not be possible, so trap this.
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // MAGIC
  stepSize = 1.0;
  
  // Distance between nodes
  dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / stepSize);

  // Dont do very, very short maneuvers.
  if (numSteps < 1)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  //MSG("%f %f %f : %f %f %f",
  //    poseA.x, poseA.y, poseA.theta, poseB.x, poseB.y, poseB.theta);

  // Check for unfeasable maneuvers. Note the iteration includes the final
  // step in the trajectory (we check 0 to 1 inclusive), to trap the case
  // where the final step has a huge change in theta.
  feasible = true;
  theta = configA.theta;
  for (i = 1; i < numSteps + 1; i++) 
  {
    s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    //fprintf(stdout, "config %d %f %f %f %f %f\n",
    //        i, s, config.x, config.y, config.theta, config.phi);
    
    // Discard turns outside the steering limit
    if (fabs(config.phi) > vp->steerlimit)
      feasible = false;

    // Discard big changes in yaw that occur too fast to be
    // caught by the steering limit test at a course step size.
    if (acos(cos(config.theta - theta)) > 45 * M_PI/180) // MAGIC
      feasible = false;

    theta = config.theta;
  }

  if (!feasible)
  {
    maneuver_free(mp);
    free(vp);
    return -1;      
  }

  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a maneuver node
    dst = this->graph->createNode();
    if (!dst)
    {
      ERROR("unable to create node; try increasing maxNodes");
      return ENOMEM;
    }

    // Fill out basic node data
    dst->type = nodeType;
    dst->direction = +1;
    dst->pose.pos = vec3_set(config.x, config.y, 0);
    dst->pose.rot = quat_from_rpy(0, 0, config.theta);
    dst->steerAngle = config.phi;
    pose3_to_mat44d(dst->pose, dst->transGN);
    mat44d_inv(dst->transNG, dst->transGN);

    // If we are creating turns, create the lane boundaries
    // and update the spatial index.
    if (nodeType == GRAPH_NODE_TURN)
    {
      dst->laneLength = stepSize * 1.35; // MAGIC HACK
      dst->laneWidth = (1 - s) * nodeA->laneWidth + s * nodeB->laneWidth;
      dst->laneRadius = sqrtf(pow(dst->laneLength,2) + pow(dst->laneWidth,2)) / 2;
      this->graph->setNodePose(dst, dst->pose);
    }
    
    // If we are generating a lane change, set the
    // RNDF data and center-line distance.
    if (nodeType == GRAPH_NODE_CHANGE)
    {
      this->genChangeNode(dst);
      assert(dst->segmentId == nodeA->segmentId && dst->segmentId == nodeB->segmentId);
      assert(dst->laneId == nodeA->laneId || dst->laneId == nodeB->laneId);
    }
      
    // If we are generating volatile, set the RNDF data and
    // center-line distance.
    if (nodeType == GRAPH_NODE_VOLATILE)
      this->genVolatileNode(dst);
    
    // Create an arc from the previous node to the new node
    arc = this->graph->createArc(src->index, dst->index);
    if (!arc)
    {
      ERROR("unable to create arc; try increasing maxArcs");
      return ENOMEM;
    }  

    src = dst;
  }

  // Create an arc to the final node
  dst = nodeB;
  arc = this->graph->createArc(src->index, dst->index);
  if (!arc)
  {
    ERROR("unable to create arc; try increasing maxArcs");
    return ENOMEM;
  }
  
  maneuver_free(mp);
  free(vp);
  
  return 0;
}


// Set the RNDF data on a change node
int GraphPlanner::genChangeNode(GraphNode *node)
{
  GraphNode *lanel;
  double ax, ay, bx, by;
      
  // Get the nearest lane.
  lanel = this->graph->getNearestNode(node->pose.pos, GRAPH_NODE_LANE, 10); // MAGIC
  assert(lanel);

  // Compute node position relative to lane element
  ax = node->pose.pos.x;
  ay = node->pose.pos.y;
  bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
  by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

  // Record distance from lane centerline
  node->centerDist = sqrtf(bx * bx + by * by);
  node->segmentId = lanel->segmentId;
  node->laneId = lanel->laneId;      

  return 0;
}


// Set the RNDF data on a volatile node
int GraphPlanner::genVolatileNode(GraphNode *node)
{
  GraphNode *lanel;
  double ax, ay, bx, by;

  // Get the nearest lane or turn element.
  lanel = this->graph->getNearestNode(node->pose.pos,
                                      GRAPH_NODE_LANE | GRAPH_NODE_TURN, 10); // MAGIC
  
  if (!lanel)
    return 0;
  
  // Compute node position relative to lane element
  ax = node->pose.pos.x;
  ay = node->pose.pos.y;
  bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
  by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

  // Record distance from lane centerline
  node->centerDist = sqrtf(bx * bx + by * by);

  // See if the node is in the lane in a strict euclidean sense
  if (bx > -lanel->laneLength/2 && bx < +lanel->laneLength/2 &&
      by > -lanel->laneWidth/2 && by < +lanel->laneWidth/2)
  {
    // Assign RNDF data
    node->segmentId = lanel->segmentId;
    node->laneId = lanel->laneId;
    node->isEntry = lanel->isEntry;
    node->isExit = lanel->isExit;
    node->isStop = lanel->isStop;
  }

  return 0;
}



