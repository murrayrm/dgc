
/* 
 * Desc: Update the collision status of nodes in the graph.
 * Date: 28 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <math.h>

#include <alice/AliceConstants.h>
#include "PlanGraphStatus.hh"


// Useful macros
#define MIN(a, b) ((a) < (b) ? (a) : (b))


// Constructor
PlanGraphStatus::PlanGraphStatus(PlanGraph *graph)
{
  this->graph = graph;
  this->lineSpacing = 0.5;

  // Hard limits on Alice's size
  this->aliceInnerFront = DIST_REAR_AXLE_TO_FRONT;
  this->aliceInnerRear  = DIST_REAR_TO_REAR_AXLE;
  this->aliceInnerSide = VEHICLE_WIDTH/2;

  // Soft limts
  this->setOuterBuffer(1.0, 1.0, 0.5);

  return;
}


// Destructor
PlanGraphStatus::~PlanGraphStatus()
{
  return;
}


// Set the inner bounding box.
void PlanGraphStatus::setInner(float front, float rear, float side)
{
  this->aliceInnerFront = front;
  this->aliceInnerRear = rear;
  this->aliceInnerSide = side;
  return;
}


// Set the outer bounding box by specifying the buffer size.
void PlanGraphStatus::setOuterBuffer(float front, float rear, float side)
{
  this->aliceOuterFront = this->aliceInnerFront + front;
  this->aliceOuterRear = this->aliceInnerRear + rear;
  this->aliceOuterSide = this->aliceInnerSide + side;
  this->aliceOuterDiag = sqrtf(pow(this->aliceOuterFront + this->aliceOuterRear, 2) +
                               pow(this->aliceOuterSide + this->aliceOuterSide, 2));  
  return;
}


// Check for any nodes in the obstacle bounding box (site frame)
bool PlanGraphStatus::checkBox(float ox, float oy, float sx, float sy, PlanGraphQuad *quad)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;

  if (quad == NULL)
  {
    // Expand the bounding box to include any possible collision with Alice
    sx += 2*this->aliceOuterDiag;
    sy += 2*this->aliceOuterDiag;
    quad = this->graph->root;
  }
  
  // Check for intersection between the quad and the box.
  if (!quad->hasIntersection(ox, oy, sx, sy))
    return false;

  // Check our children
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;    
    if (this->checkBox(ox, oy, sx, sy, leaf))
      return true;
  }

  // Check our static nodes to see if any fall inside the bounding box
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    if (node->pose.pos.x < ox - sx/2)
      continue;
    if (node->pose.pos.x > ox + sx/2)
      continue;
    if (node->pose.pos.y < oy - sy/2)
      continue;
    if (node->pose.pos.y > oy + sy/2)
      continue;
    return true;
  }

  return false;
}


// Update the status values along a line
void PlanGraphStatus::updateLine(vec2f_t pa, vec2f_t pb, bool obs, bool car, bool pred)
{
  int i, numSteps;
  float length;
  float px, py, dx, dy;
  
  length = vec2f_mag(vec2f_sub(pb, pa));
  numSteps = (int) (ceil(length / this->lineSpacing)) + 1;
  assert(numSteps > 0);

  // Compute line parameters
  px = pa.x;
  py = pa.y;
  dx = (pb.x - pa.x) / numSteps;
  dy = (pb.y - pa.y) / numSteps;

  // Walk the line
  for (i = 0; i < numSteps; i++)
  {
    //printf("line %d %f %f\n", i, px, py);    
    this->updateQuad(px, py, obs, car, pred, this->graph->root);
    px += dx;
    py += dy;
  }
  
  return;
}


// Update the status values for intersecting nodes
void PlanGraphStatus::updateQuad(float ox, float oy, bool obs, bool car, bool pred, PlanGraphQuad *quad)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  float px, py;
  float m[3][3];

  // REMOVE
  //if (pred)
  //{
  //  if (!quad->hasIntersection(ox, oy, 2.0, 2.0)) // MAGIC 
  //    return;
  //}

  // See of the quad can contain any node that overlaps this point.
  if (!quad->hasIntersection(ox, oy, 2*this->aliceOuterDiag, 2*this->aliceOuterDiag))
    return;

  // Check our children
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;    
    this->updateQuad(ox, oy, obs, car, pred, leaf);
  }

  // Check our nodes.
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];

    // Compute transform from site frame to node frame.  If the value
    // has never been set, do it now.
    if (node->status.transTime == 0)
    {
      pose2f_to_mat33f(pose2f_inv(node->pose), m);
      memcpy(node->status.trans[0], m[0], sizeof(m[0]));
      memcpy(node->status.trans[1], m[1], sizeof(m[1]));
      node->status.transTime = graph->getStatusTime();
    }

    // Transform point into the node frame.
    px = node->status.trans[0][0]*ox + node->status.trans[0][1]*oy + node->status.trans[0][2];
    py = node->status.trans[1][0]*ox + node->status.trans[1][1]*oy + node->status.trans[1][2];

    // REMOVE 
    //if (pred && (px > 1.0 || px < -1.0 || py > 1.0 || py < -1.0))
    //  continue;

    // Update obstacle distances
    if (obs) 
    {
      if (!graph->isStatusFresh(node->status.obsDistTime))
      {
        // TESTING
        //if (px > 0 && px < this->aliceInnerFront)
        //  node->status.obsSideDist = fabsf(py) - this->aliceInnerSide;
        //else
        //  node->status.obsSideDist = 10.0;
        // TESTING
        //node->status.obsFrontDist = this->aliceOuterFront - this->aliceInnerFront;
        //node->status.obsRearDist = this->aliceOuterRear - this->aliceInnerRear;
        node->status.obsFrontDist = 100.0;
        node->status.obsRearDist = 100.0;
        node->status.obsSideDist = 100.0;
        node->status.obsDistTime = graph->getStatusTime();
      }
      if (py > -this->aliceInnerSide && py < +this->aliceInnerSide)
      {
        if (px > 0)
          node->status.obsFrontDist = MIN(node->status.obsFrontDist, +px - this->aliceInnerFront);
        else
          node->status.obsRearDist = MIN(node->status.obsRearDist, -px - this->aliceInnerRear);
      }
      if (px > 0 && px < this->aliceInnerFront) // TESTING
        node->status.obsSideDist = MIN(node->status.obsSideDist, fabsf(py) - this->aliceInnerSide);
    }

    // Ignore points outside the Alice box
    if (px > +this->aliceOuterFront)
      continue;
    if (px < -this->aliceOuterRear)
      continue;
    if (py > +this->aliceOuterSide)
      continue;
    if (py < -this->aliceOuterSide)
      continue;

    // Update the node
    if (obs)
    {
      node->status.obsCollision = true;
      node->status.obsTime = graph->getStatusTime();
    }
    if (car)
    {
      node->status.carCollision = true;
      node->status.carTime = graph->getStatusTime();
    }
  }    
  
  return;
}
