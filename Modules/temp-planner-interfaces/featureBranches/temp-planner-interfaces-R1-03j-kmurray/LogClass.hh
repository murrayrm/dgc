/*!
 * \file LogClass.hh
 * \brief Log-functions as a class, not as static functions
 *
 * \author Christian Looman
 * \date 11 October 2007
 *
 * \ingroup planner
 *
 */

#ifndef LOGCLASS_HH_
#define LOGCLASS_HH_

#include <iostream>
#include <ostream>
#include <stdio.h>
#include <string>
#include <sstream>
#include <fstream>

#include "Log.hh"

using namespace std;

class CLog {
public:
  CLog(int level);
  virtual ~CLog();
  int getVerboseLevel();
  void setVerboseLevel(int level);
  ostream& getStream(int level);
  void setGenericLogFile(const char *filename);

private:
  int verbose_level;
  ofstream genericFileStream;
  NullStream nStream;
};

#endif                          /*LOGCLASS_HH_ */
