
/* 
 * Desc: Generates a dense graph from an RNDF file.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <alice/AliceConstants.h>
//#include <dgcutils/DGCutils.hh>
#include <trajutils/maneuver.h>

#include "PlanGraphBuilder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
PlanGraphBuilder::PlanGraphBuilder(PlanGraph *graph)
{
  this->graph = graph;
  
  this->wheelBase = VEHICLE_WHEELBASE;
  this->maxSteer  = VEHICLE_MAX_AVG_STEER;

  this->railCount = 5;
  this->railSpacing = 0.75;
     
  this->spacing = 1.0; // MAGIC 
  
  return;
}


// Destructor
PlanGraphBuilder::~PlanGraphBuilder()
{  
  return;
}


// Set generation options.
int PlanGraphBuilder::setOptions(float spacing, int railCount, float railSpacing)
{
  this->spacing = spacing;
  this->road.spacing = spacing;
  this->railCount = railCount;
  this->railSpacing = railSpacing;
  
  return 0;
}
 
// Build from RNDF file
int PlanGraphBuilder::build(char *filename)
{
  // Load the reference RNDF into the plan graph
  if (this->graph->rndf.load(filename) != 0)
    return -1;
  
  // Build the road graph; we will use this to guide construction.
  if (this->road.load(filename) != 0)
    return -1;
  
  // Generate the dense graph
  if (this->genGraph() != 0)
    return -1;
  
  return 0;
}


// Load from PG file
int PlanGraphBuilder::load(char *filename)
{
  // Load directly
  if (graph->load(filename) != 0)
    return -1;

  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);
  
  return 0;
}


// Generate the dense graph
int PlanGraphBuilder::genGraph()
{
  int i, w;
  
  MSG("min quad size %d, min node size %d",
      sizeof(PlanGraphQuad), sizeof(PlanGraphNode));
        
  // Generate lanes/rails
  w = (this->railCount - 1) / 2;
  for (i = -w; i <= w; i++)
    this->genRails(i, i * this->railSpacing);
  
  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);
  
  // Generate turns
  this->genTurns();

  MSG("pass 2: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);
      
  // Generating oncoming rails.  For now, just make one of these.
  this->genOncomingRails(0, 0);

  // Generate zones
  this->genZones();

  MSG("pass 3: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  // Generate feasible lane/rail changes.  Only the feasibility info,
  // not the full maneuver, is added to the graph.
  this->genFeasibleChanges();
    
  MSG("final: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  return 0;
}


// Generate rails in lanes
int PlanGraphBuilder::genRails(int railId, float offset)
{
  int i, j;
  pose2f_t pose;
  RoadGraphNode *rnode, *rnodeA, *rnodeB;
  PlanGraphNode *node, *nodeA, *nodeB;
  
  // Add all of the nodes from the road graph.
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnode = this->road.nodes + i;

    // Reset user data so we dont get confused.
    rnode->data = NULL;

    // Dont generate intersections at this time
    if (rnode->flags.isTurn)
      continue;

    // On zone boundaries, generate nodes for the entry/exit points only
    if (rnode->flags.isZonePerimeter && !(rnode->flags.isEntry || rnode->flags.isExit))
      continue;
    
    // Apply a lateral offset to the node in the road graph
    pose.pos.x = rnode->pose.pos.x - offset*sin(rnode->pose.rot);
    pose.pos.y = rnode->pose.pos.y + offset*cos(rnode->pose.rot);
    pose.rot = rnode->pose.rot;

    // Create the new node
    node = this->graph->allocNode(pose.pos.x, pose.pos.y);
    if (!node)
      return ERROR("unable to allocate node %d", this->graph->nodeCount);

    node->pose.rot = pose.rot;
    node->steerAngle = 0;
    node->segmentId = rnode->segmentId;
    node->laneId = rnode->laneId;
    node->waypointId = rnode->waypointId;
    node->interId = rnode->interId;
    node->railId = railId;
    node->flags.isLane = true;
    node->flags.isStop = rnode->flags.isStop;
    node->flags.isExit = rnode->flags.isExit;
    node->flags.isEntry = rnode->flags.isEntry;
    node->flags.isZonePerimeter = rnode->flags.isZonePerimeter;
    node->flags.isZoneParking = rnode->flags.isZoneParking;
    node->flags.isZone = node->flags.isZonePerimeter || node->flags.isZoneParking;
    node->flags.isOncoming = false;    
    node->prevWaypoint = rnode->prevWaypoint;
    node->nextWaypoint = rnode->nextWaypoint;
    
    // Store a pointer to the plan node in the road node; we will use
    // this in a moment to build arcs.
    rnode->data = (void*) node;
  }

  // Add in feasable arcs along lanes
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnodeA = this->road.nodes + i;
    if (!rnodeA->data)
      continue;

    for (j = 0; j < rnodeA->numNext; j++)
    { 
      rnodeB = rnodeA->next[j];
      if (!rnodeB->data)
        continue;
 
      nodeA = (PlanGraphNode*) rnodeA->data;
      assert(nodeA);
      nodeB = (PlanGraphNode*) rnodeB->data;
      assert(nodeB);

      if (nodeA->flags.isZonePerimeter && nodeB->flags.isZonePerimeter)
        continue;

      /* TESTING TODO
      if (!this->checkFeasibleStep(nodeA->pose, nodeB->pose))
      {
        MSG("infeasible lane %d.%d.%d.%d to %d.%d.%d.%d",
            nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
            nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);
        continue;
      }
      */

      this->graph->insertArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Generate the rails in oncoming lanes
int PlanGraphBuilder::genOncomingRails(int railId, float offset)
{
  int i, j;
  pose2f_t pose;
  RoadGraphNode *rnode, *rnodeA, *rnodeB;
  PlanGraphNode *node, *nodeA, *nodeB;
  
  // Add all of the nodes from the road graph.
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnode = this->road.nodes + i;

    // Reset user data so we dont get confused.
    rnode->data = NULL;

    // Dont generate intersections at this time
    if (rnode->flags.isTurn)
      continue;

    // On zone boundaries, generate nodes for the entry/exit points only
    if (rnode->flags.isZonePerimeter && !(rnode->flags.isEntry || rnode->flags.isExit))
      continue;
    
    // Apply a lateral offset to the node in the road graph
    pose.pos.x = rnode->pose.pos.x - (0.10 + offset)*sin(rnode->pose.rot);
    pose.pos.y = rnode->pose.pos.y + (0.10 + offset)*cos(rnode->pose.rot);
    pose.rot = rnode->pose.rot + M_PI;
    pose.rot = atan2f(sin(pose.rot), cos(pose.rot));

    // Create the new node
    node = this->graph->allocNode(pose.pos.x, pose.pos.y);
    if (!node)
      return ERROR("unable to allocate node %d", this->graph->nodeCount);

    node->pose.rot = pose.rot;
    node->segmentId = rnode->segmentId;
    node->laneId = rnode->laneId;
    node->waypointId = rnode->waypointId;
    node->interId = rnode->interId;
    node->railId = railId;
    node->flags.isLane = true;
    node->flags.isStop = rnode->flags.isStop;
    node->flags.isExit = rnode->flags.isExit;
    node->flags.isEntry = rnode->flags.isEntry;
    node->flags.isZonePerimeter = rnode->flags.isZonePerimeter;
    node->flags.isZoneParking = rnode->flags.isZoneParking;
    node->flags.isZone = node->flags.isZonePerimeter || node->flags.isZoneParking;
    node->flags.isOncoming = true;    
    node->prevWaypoint = rnode->prevWaypoint;
    node->nextWaypoint = rnode->nextWaypoint;
    
    // Store a pointer to the plan node in the road node; we will use
    // this in a moment to build arcs.
    rnode->data = (void*) node;
  }

  // Add in feasable arcs along lanes
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnodeA = this->road.nodes + i;
    if (!rnodeA->data)
      continue;

    for (j = 0; j < rnodeA->numPrev; j++)
    { 
      rnodeB = rnodeA->prev[j];
      if (!rnodeB->data)
        continue;
 
      nodeA = (PlanGraphNode*) rnodeA->data;
      assert(nodeA);
      nodeB = (PlanGraphNode*) rnodeB->data;
      assert(nodeB);

      if (nodeA->flags.isZonePerimeter && nodeB->flags.isZonePerimeter)
        continue;

      /* TESTING TODO
      if (!this->checkFeasibleStep(nodeA->pose, nodeB->pose))
      {
        MSG("infeasible lane %d.%d.%d.%d to %d.%d.%d.%d",
            nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
            nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);
        continue;
      }
      */

      this->graph->insertArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Generate turn maneuvers through intersections
int PlanGraphBuilder::genTurns()
{
  int i, j;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wpA, *wpB;

  rndf = &this->road.rndf;

  // Use the RNDF graph to find matching entry/exit points.
  // We may generate multiple turns for each entry/exit pair.
  for (i = 0; i < rndf->numWaypoints; i++)
  {
    wpA = rndf->waypoints + i;
    if (!wpA->flags.isExit)
      continue;
    for (j = 0; j < wpA->numNext; j++)
    {
      wpB = wpA->next[j];
      if (!wpB->flags.isEntry)
        continue;
      if (wpA->flags.isZonePerimeter && wpB->flags.isZonePerimeter)
        continue;
      this->genTurnWaypoints(wpA, wpB);
    }    
  }
  
  return 0;
}


// Generate turn manuevers for the given waypoints
int PlanGraphBuilder::genTurnWaypoints(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB)
{
  int i, j;
  float size;
  bool feasible;
  PlanGraphNodeList nodesA, nodesB;
  PlanGraphNode *nodeA, *nodeB;

  MSG("gen turns from %d.%d.%d to %d.%d.%d",
      wpA->segmentId, wpA->laneId, wpA->waypointId,
      wpB->segmentId, wpB->laneId, wpB->waypointId);

  // Compute a size that is sure to capture all the rails.
  size = 16 + 4 * this->railCount * this->railSpacing;

  // Get all the nodes in the vicinity of the entry/exit waypoints.
  this->graph->getRegion(&nodesA, wpA->px, wpA->py, size, size);
  this->graph->getRegion(&nodesB, wpB->px, wpB->py, size, size);

  assert(nodesA.size() > 0);
  assert(nodesB.size() > 0);
  
  feasible = false;
  
  // Look through the lists to find the correct entry/exit nodes,
  // (there may be multiple matches if there are multiple rails), then
  // generate the turn maneuver between these nodes.
  for (i = 0; i < (int) nodesA.size(); i++)
  {
    nodeA = nodesA[i];

    if (nodeA->segmentId != wpA->segmentId)
      continue;
    if (nodeA->laneId != wpA->laneId)
      continue;
    if (nodeA->waypointId != wpA->waypointId)
      continue;
    if (!nodeA->flags.isExit)
      continue;
    
    for (j = 0; j < (int) nodesB.size(); j++)
    {
      nodeB = nodesB[j];
          
      if (nodeB->segmentId != wpB->segmentId)
        continue;
      if (nodeB->laneId != wpB->laneId)
        continue;
      if (nodeB->waypointId != wpB->waypointId)
        continue;
      if (!nodeB->flags.isEntry)
        continue;

      // TESTING
      if (nodeA->railId != nodeB->railId)
        continue;
      
      if (this->genTurnNodes(nodeA, nodeB) == 0)
        feasible = true;
    }
  }

  // Where any of these maneuvers feasible?
  if (!feasible)
  {
    MSG("infeasible turn %d.%d.%d to %d.%d.%d",
        wpA->segmentId, wpA->laneId, wpA->waypointId, 
        wpB->segmentId, wpB->laneId, wpB->waypointId);
    //assert(false);
  }

  return 0;
}


// Generate a turn maneuver between the given nodes
int PlanGraphBuilder::genTurnNodes(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  int i, steps;
  bool feasible;
  PlanGraphNode *exitNode = nodeA;
  PlanGraphNode *entryNode = nodeB;
  PlanGraphNode::Flags flags = {0};
  
  // MSG("gen turns from %+d to %+d (nodeA isStop = %s)", nodeA->railId, nodeB->railId, (exitNode->flags.isStop)?"true":"false");

  // Set the node flags for these maneuvers
  flags.isTurn = 1;

  // Max number of steps to take along the entry/exit rails to broaden
  // the turn.
  steps = (int) ((DIST_REAR_AXLE_TO_FRONT-0.5) / this->spacing);   // MAGIC

  feasible = false;
  for (i = 0; i < steps; i++)
  {
    //MSG("attempting %d.%d.%d.%d to %d.%d.%d.%d",
    //	nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
    //  nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);

    // See if this turn is feasible.
    if (this->checkFeasibleManeuver(nodeA->pose, nodeB->pose))
    {
      // See if this turn stays on the road
      if (true)
      {
        // Insert this manuever
        flags.isInfeasible = false;
        this->insertManeuver(nodeA, nodeB, flags);
        feasible = true;

        // Set waypointId value
        nodeA->waypointId = exitNode->waypointId;
        nodeB->waypointId = entryNode->waypointId;

        // Set stop values
        nodeA->flags.isStop = exitNode->flags.isStop;
        nodeA->status.stopDist = vec2f_mag(vec2f_sub(nodeA->pose.pos, exitNode->pose.pos));

        // If a feasible turn was found, just break
        //if (!flags.isInfeasible)
        //  break;
      }
    }

    // TESTING
    // Try a wider turn by stepping along both the entry and exit rails (but dont bypass waypoints).
    if (nodeA->numPrev == 1) // && nodeA->prev[0]->nextWaypoint->waypointId == exitNode->waypointId)
      nodeA = nodeA->prev[0];
    if (nodeB->numNext == 1) // && nodeB->next[0]->prevWaypoint->waypointId == entryNode->waypointId)
      nodeB = nodeB->next[0];
  }

  if (!feasible)
  {
    MSG("infeasible turn %d.%d.%d.%d %+d to %d.%d.%d.%d %+d",
        nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId, nodeA->railId,
        nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId, nodeB->railId);

    // Insert this manuever
    flags.isInfeasible = true;
    this->insertManeuver(nodeA, nodeB, flags);

    // Set waypointId value
    nodeA->waypointId = exitNode->waypointId;
    nodeB->waypointId = entryNode->waypointId;

    // Set stop values
    nodeA->flags.isStop = exitNode->flags.isStop;
    nodeA->status.stopDist = vec2f_mag(vec2f_sub(nodeA->pose.pos, exitNode->pose.pos));

    return -1;
  }

  return 0;
}


// Generate basic connectivity into zones
int PlanGraphBuilder::genZones()
{
  int i, j;
  PlanGraphNode *nodeA, *nodeB;
  PlanGraphNode::Flags flags = {0};
  PlanGraphNodeList nodes;
  
  // Get all the nodes (should not be too many at this point)
  this->graph->getRegion(&nodes, 0, 0, 100000, 100000);
  
  // Find entry/exit nodes on zone perimeters
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeA = nodes[i];
    
    if (!nodeA->flags.isZonePerimeter)
      continue;
    if (!(nodeA->flags.isEntry || nodeA->flags.isExit))
      continue;

    if (nodeA->flags.isEntry)
      MSG("zone entry %d.%d.%d",
          nodeA->segmentId, nodeA->laneId, nodeA->waypointId);
    if (nodeA->flags.isExit)
      MSG("zone exit %d.%d.%d",
          nodeA->segmentId, nodeA->laneId, nodeA->waypointId);

    // Find parking spots
    for (j = 0; j < (int) nodes.size(); j++)
    {
      nodeB = nodes[j];

      if (!nodeB->flags.isZoneParking)
        continue;
      if (nodeB->segmentId != nodeA->segmentId)
        continue;
      
      // TESTING
      if (nodeA->railId != nodeB->railId)
        continue;
      
      MSG("zone parking %d.%d.%d",
          nodeB->segmentId, nodeB->laneId, nodeB->waypointId);
      
      // Set the flags
      flags.isZone = true;
      
      // Generate the (possibly unfeasible) maneuver from entry point
      // to waypoint.
      if (nodeA->flags.isEntry)
      {
        flags.isInfeasible = !this->checkFeasibleManeuver(nodeA->pose, nodeB->pose);
        this->insertManeuver(nodeA, nodeB, flags);
      }
      else
      {
        flags.isInfeasible = !this->checkFeasibleManeuver(nodeB->pose, nodeA->pose);
        this->insertManeuver(nodeB, nodeA, flags);
      }
    }
  }

  return 0;
}


// Generate feasible rail-change maneuvers (recursive)
int PlanGraphBuilder::genFeasibleChanges()
{
  int i, j;
  float spacing, minDist, maxDist, maxSide;
  PlanGraphNode *nodeA, *nodeB;
  vec2f_t pos;
  uint8_t include, exclude;
  PlanGraphNodeList nodes;

  // MAGIC
  maxSide = 5.0;
  minDist = 5.0;
  maxDist = 40.0;
  spacing = this->road.spacing;

  // Get all the nodes; there should not be too many at this point
  include = PLAN_GRAPH_NODE_LANE;
  exclude = PLAN_GRAPH_NODE_NONE;
  this->graph->getRegion(&nodes, 0, 0, FLT_MAX, FLT_MAX, include, exclude);
  
  // Consider all nodes 
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeA = nodes[i];

    if (i % 100 == 0)
      MSG("generating changes for %d/%d", i, nodes.size());

    // MAGIC
    if (nodeA->interId % 10 != 0)
      continue;
    
    for (j = 0; j < (int) nodes.size(); j++)
    {
      nodeB = nodes[j];

      // MAGIC
      if (nodeB->interId % 10 != 0)
        continue;

      // Dont make changes between segments
      if (nodeB->segmentId != nodeA->segmentId)
        continue;

      // Compute end-point in frame of first node
      pos = vec2f_transform(pose2f_inv(nodeA->pose), nodeB->pose.pos);

      // Dont make changes that are too near or too far.
      if (pos.x < minDist || pos.x > maxDist)
        continue;
      if (fabs(pos.y) > maxDist)
        continue;

      // If changing rails in or between regular lanes...
      if (nodeA->flags.isOncoming == false && nodeB->flags.isOncoming == false)
      {        
        // Dont make changes to/from the same lane and rail
        if (nodeB->laneId == nodeA->laneId && nodeB->railId == nodeA->railId)
          continue;

        // Move at most one lane at a time
        if (abs(nodeB->laneId - nodeA->laneId) > 1)
          continue;

        // If changing within a lane, shift left/right one rail only
        if (nodeB->laneId == nodeA->laneId && abs(nodeB->railId - nodeA->railId) > 1)
          continue;

        // If changing between lanes, shift only to the same rail in the
        // other lane.
        if (nodeB->laneId != nodeA->laneId && nodeB->railId != nodeA->railId)
          continue;

        // Dont make large lateral deviations
        if (fabs(pos.y) > maxSide) 
          continue;
      }

      // If changing rails within or between oncoming lanes...
      else if (nodeA->flags.isOncoming == true && nodeB->flags.isOncoming == true)
      {
        continue;
      }

      // If changing rails into or out of oncoming lanes...
      else
      {
        // Dont make changes to/from the same lane and rail
        if (nodeB->laneId == nodeA->laneId && nodeB->railId == nodeA->railId)
          continue;
      }

      // Dont cross stoplines
      if (nodeA->nextWaypoint)
      {
        if (nodeA->nextWaypoint->flags.isStop || nodeA->nextWaypoint->flags.isExit)
        {
          PlanGraphNode *stopNode;
          vec2f_t dA, dB;
          stopNode = this->graph->getWaypoint(nodeA->nextWaypoint->segmentId,
                                              nodeA->nextWaypoint->laneId,
                                              nodeA->nextWaypoint->waypointId);
          assert(stopNode != NULL);
          dA = vec2f_transform(pose2f_inv(stopNode->pose), nodeA->pose.pos);
          dB = vec2f_transform(pose2f_inv(stopNode->pose), nodeB->pose.pos);
          if (dA.x * dB.x < 0.0)
            continue;
        }
      }

      // Do a feasibility check based on vehicle kinematics
      if (!this->checkFeasibleManeuver(nodeA->pose, nodeB->pose))
        continue;

      //MSG("building %d.%d.%d.%d:%+d %d to %d.%d.%d.%d:%+d %d",
      //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId,
      //    nodeA->interId, nodeA->railId, nodeA->flags.isOncoming,
      //    nodeB->segmentId, nodeB->laneId, nodeB->waypointId,
      //    nodeB->interId, nodeB->railId, nodeB->flags.isOncoming);
      //MSG("pose %f %f %f  to %f %f %f",
      //    nodeA->pose.pos.x, nodeA->pose.pos.y, nodeA->pose.rot,
      //    nodeB->pose.pos.x, nodeB->pose.pos.y, nodeB->pose.rot);

      // TODO
      // Check if the maneuver is within the road boundaries

      // Insert a feasible arc; this will get expanded only if the
      // node is in the ROI.
      this->graph->insertFeasibleArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Check for feasibility of an extended maneuver
bool PlanGraphBuilder::checkFeasibleManeuver(pose2f_t poseA, pose2f_t poseB)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB, config;
  pose2f_t src, dst;
  double  dm, s;
  int i, numSteps;
  float spacing;

  // Step size
  spacing = this->spacing;

  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(poseB.pos, poseA.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);

  // Check for feasibility along the entire maneuver, including the
  // final step.
  src.pos = vec2f_set(configA.x, configA.y);
  src.rot = configA.theta;
  for (i = 1; i < numSteps + 1; i++)
  {
    s = (double) i / numSteps;  
    config = maneuver_evaluate_pose(vp, mp, s);    
    dst.pos = vec2f_set(config.x, config.y);
    dst.rot = config.theta;    
    if (!this->checkFeasibleStep(src, dst))
      break;
    src = dst;
  }

  // Clean up
  maneuver_free(mp);
  free(vp);

  // Not feasible if we did not get to the end
  if (i < numSteps + 1)
    return false;
  
  return true;
}


// Do check for unfeasible maneuvers using the vehicle turning circle.
bool PlanGraphBuilder::checkFeasibleStep(pose2f_t poseA, pose2f_t poseB)
{
  float turnRadius;
  pose2f_t pose;
  float x0, y0, dx, dy, s, t;
    
  // Vehicle turning radius
  turnRadius = this->wheelBase / tan(this->maxSteer);
  
  // Final pose in the initial frame
  pose = pose2f_mul(pose2f_inv(poseA), poseB);

  // Compute equation of line at right angles to current pose.
  // x = t dx + x0.
  // y = t dy + y0.
  x0 = pose.pos.x;
  y0 = pose.pos.y;
  dx = -sin(pose.rot);
  dy = +cos(pose.rot);

  // Trap straight-line case to avoid div-by-zero.
  if (fabs(dx) < 1e-6)
    return true;
  
  // Find intercept with x = 0; the values t and s specify the radii
  // of two turning circles.
  t = -x0 / dx;
  s = t * dy + y0;

  //MSG("check %f %f : %f %f : %f %f : %f", x0, y0, dx, dy, t, s, turnRadius);

  if (fabs(t) < turnRadius)
    return false;
  if (fabs(s) < turnRadius)
    return false;
  if (s * t < 0)
    return false;
        
  return true;
}


// Insert a maneuver between the given nodes.
int PlanGraphBuilder::insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, PlanGraphNode::Flags flags)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB;
  VehicleConfiguration config;
  double  dm, s;
  int i, numSteps;
  PlanGraphNode *src, *dst;
  float spacing;

  assert(nodeA);
  assert(nodeB);

  // Node spacing
  spacing = this->spacing;
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = nodeA->pose.rot;

  // Final vehicle configuration
  configB.x = nodeB->pose.pos.x;
  configB.y = nodeB->pose.pos.y;
  configB.theta = nodeB->pose.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);

  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);
  
  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a  node
    dst = this->graph->allocNode(config.x, config.y);
    assert(dst);

    // Fill out basic node data
    dst->pose.pos = vec2f_set((float) config.x, (float) config.y);
    dst->pose.rot = config.theta;
    dst->steerAngle = config.phi;
    
    // Fill out nominal RNDF data.  These are not very meaningful.
    if (i < numSteps / 2)
    {
      dst->segmentId = nodeA->segmentId;
      dst->laneId = nodeA->laneId;
      dst->waypointId = nodeA->waypointId;
    }
    else
    {
      dst->segmentId = nodeB->segmentId;
      dst->laneId = nodeB->laneId;
      dst->waypointId = nodeB->waypointId;
    }

    // Set the interpolant
    dst->interId = i;

    // Set the flags
    dst->flags = flags;

    // Record the final destination for this maneuver.  This help us
    // later when we want to delete the maneuver.
    dst->currentDest = nodeB;

    // Fill out waypoint pointers
    dst->prevWaypoint = nodeA->prevWaypoint;
    dst->nextWaypoint = nodeB->nextWaypoint;
    
    // Create an arc from the previous node to the new node
    this->graph->insertArc(src, dst);

    src = dst;
  }
  dst = nodeB;

  // Create an arc to the final node
  this->graph->insertArc(src, dst);
  
  maneuver_free(mp);
  free(vp);

  return 0;
}
