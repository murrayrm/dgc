/**********************************************************
 **
 **  PLANNERINTERFACES.H
 **
 **    Author: Noel du Toit
 **    Created: Tue Jul 10 08:38:32 2007
 **
 **********************************************************
 ** The interfaces between the executable planner module, 
 ** planner, and its libraries (GraphUpdater, PathPlanner, 
 ** LogicPlanner, VelPlanner) are defined here.
 **********************************************************/

#ifndef PLANNERINTERFACES_H
#define PLANNERINTERFACES_H

#include "PlanGraph.hh"
#include "PlanGraphPath.hh"
#include <queue>

#include <cspecs/CSpecs.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <gcinterfaces/SegGoals.hh>
#include <map/Map.hh>

using namespace std;


// STATE PROBLEM
/// States used in the finite state machine
typedef enum {DRIVE, STOP_INT, STOP_OBS, BACKUP, UTURN, PAUSE} FSM_state_t;
/// Flags that guide the update of the graph
typedef enum {PASS, NO_PASS, ZONE} FSM_flag_t;
/// Regions that we need to be able to plan through
typedef enum {ZONE_REGION, ROAD_REGION, INTERSECTION} FSM_region_t;
/// Planners that we want to use
typedef enum {RAIL_PLANNER, S1PLANNER, DPLANNER, CIRCLE_PLANNER} FSM_planner_t;
/// How do we deal with obstacles? Can be safe, aggressive, and not safe (extreme)
typedef enum {OBSTACLE_SAFETY, OBSTACLE_AGGRESSIVE, OBSTACLE_BARE} FSM_obstacle_t;

/// Defines the state problem that is returned by the logic planner
typedef struct {
  FSM_state_t state;
  double probability;
  FSM_flag_t flag;
  FSM_region_t region;
  FSM_planner_t planner;
  FSM_obstacle_t obstacle;
} StateProblem_t;

// ERRORS
#define LP_OK                   0
#define GU_OK                   0
#define PP_OK                   0
#define VP_OK                   0
#define PLANNER_OK              0
#define S1PLANNER_OK            0
#define CIRCLE_PLANNER_OK       0
#define LP_MAP_INCOMPLETE       1
#define LP_FAIL_MPLANNER_LANE_BLOCKED        2
#define GU_MAP_INCOMPLETE       4
#define PP_COLLISION            8
#define PP_NOPATH_LEN          16
#define PP_NOPATH_COST         32
#define VP_UTURN_FINISHED      64
#define VP_BACKUP_FINISHED    128
#define PP_NONODEFOUND        256
#define P_EXTRACT_SUBPATH_ERROR     512
#define S1PLANNER_FAILED      1024
#define CIRCLE_PLANNER_FAILED 2048
#define GU_UPDATE_FROM_MAP_ERROR    4096
#define LP_FAIL_MPLANNER_ROAD_BLOCKED 8192
#define LP_FAIL_MPLANNER_LOST       16384
#define S1PLANNER_START_BLOCKED		32768
#define S1PLANNER_END_BLOCKED		65536
#define S1PLANNER_COST_TOO_HIGH		131072
#define VP_PARKING_SPOT_FINISHED  262144

// the value of the cost which will result in a
// S1PLANNER_COST_TOO_HIGH error getting reported
#define S1PLANNER_HIGH_COST_VALUE	3000

typedef uint32_t Err_t;

// PATH
// REMOVE typedef struct GraphPath Path_t;

// COST
typedef int Cost_t;

// VEL PLANNER PARAMS
typedef struct {
  double minSpeed;
  double maxSpeed;
} Vel_params_t;

// PATH PLANNER PARAMS
typedef struct {
  FSM_flag_t flag;
  bool planFromCurrPos;
  int zoneId;
  int spotId; // 0 is perimeter, >0 is spot numbers
  int exitId;
  double velMin;
  double velMax;
  bool readPathPlanParams;
} Path_params_t;

// LOGIC PLANNER PARAMS
typedef struct {
  SegGoals::SegmentType segment_type;
  SegGoals seg_goal;
  deque<SegGoals>* seg_goal_queue;
  PlanGraph* m_graph;
  PlanGraphPath* m_path;
  bool planFromCurrPos;
  int m_estop;
} Logic_params_t;

typedef struct {
  PointLabel WayPoint;
  MapElement element;
  double velocity;
  double distance;
  double eta;
  LaneLabel lane;
  bool precedence;
  bool closest;
  bool checkedQueuing;
  bool updated;
  unsigned long long lastUpdated;
} PrecedenceList_t;

// PREDICTION INTERFACE
typedef int Prediction_t;

typedef CSpecs CSpecs_t;


#endif
