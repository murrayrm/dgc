
/* 
 * Desc: Generates a dense graph from an RNDF file.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <alice/AliceConstants.h>
//#include <dgcutils/DGCutils.hh>
#include <trajutils/maneuver.h>

#include "PlanGraphBuilder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Useful macros
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Constructor
PlanGraphBuilder::PlanGraphBuilder(PlanGraph *graph)
{
  this->graph = graph;
  
  this->wheelBase = VEHICLE_WHEELBASE;
  this->maxSteer  = VEHICLE_MAX_AVG_STEER;

  this->railCount = 5;
  this->railSpacing = 0.75;
     
  this->spacing = 1.0; // MAGIC 
  
  return;
}


// Destructor
PlanGraphBuilder::~PlanGraphBuilder()
{  
  return;
}


// Set generation options.
int PlanGraphBuilder::setOptions(float spacing, int railCount, float railSpacing)
{
  this->spacing = spacing;
  this->road.spacing = spacing;
  this->railCount = railCount;
  this->railSpacing = railSpacing;
  
  return 0;
}
 
// Build from RNDF file
int PlanGraphBuilder::build(char *rndfFilename, const char *tweaksFilename)
{
  // Load the reference RNDF into the plan graph
  if (this->graph->rndf.load(rndfFilename) != 0)
    return -1;
  
  // Build the road graph; we will use this to guide construction.
  if (this->road.load(rndfFilename, tweaksFilename) != 0)
    return -1;
  
  // Generate the dense graph
  if (this->genGraph() != 0)
    return -1;
  
  return 0;
}


// Load from PG file
int PlanGraphBuilder::load(char *filename)
{
  // Load directly
  if (graph->load(filename) != 0)
    return -1;

  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);
  
  return 0;
}


// Generate the dense graph
int PlanGraphBuilder::genGraph()
{
  int i, w;
  
  MSG("min quad size %d, min node size %d",
      (int) sizeof(PlanGraphQuad), (int) sizeof(PlanGraphNode));
        
  // Generate lanes/rails
  w = (this->railCount - 1) / 2;
  for (i = -w; i <= w; i++)
    this->genRails(i);
  
  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);
  
  // Generate turns
  this->genTurns();

  MSG("pass 2: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  // Generating oncoming rails.
  for (i = -w; i <= w; i++)  
    this->genOncomingRails(i);

  // Generate zones
  this->genZones();

  MSG("pass 3: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  // Generate feasible lane/rail changes.  Only the feasibility info,
  // not the full maneuver, is added to the graph.
  this->genChanges();
    
  MSG("final: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  return 0;
}


// Generate rails in lanes
int PlanGraphBuilder::genRails(int railId)
{
  int i, j;
  pose2f_t pose;
  float width, offset;
  RoadGraphNode *rnode, *rnodeA, *rnodeB;
  PlanGraphNode *node, *nodeA, *nodeB;
  
  // Add all of the nodes from the road graph.
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnode = this->road.nodes + i;

    // Reset user data so we dont get confused.
    rnode->data = NULL;

    // Dont generate intersections at this time
    if (rnode->flags.isTurn)
      continue;

    // Generate nodes for zone entry/exit points, plus zone parking spots.
    if (rnode->flags.isZonePerimeter && !(rnode->flags.isEntry || rnode->flags.isExit))
      continue;

    // Pick an offset based on the current lane width
    width = (rnode->laneWidth - VEHICLE_WIDTH - 0.0) / (this->railCount - 1);
    width = MAX(width, this->railSpacing);    
    offset = railId * width;

    //MSG("foo %d %f %f %f", railId, width, rnode->laneWidth, VEHICLE_WIDTH);
    
    // Apply a lateral offset to the node in the road graph
    pose.pos.x = rnode->pose.pos.x - offset*sin(rnode->pose.rot);
    pose.pos.y = rnode->pose.pos.y + offset*cos(rnode->pose.rot);
    pose.rot = rnode->pose.rot;

    // Create the new node
    node = this->graph->allocNode(pose.pos.x, pose.pos.y);
    if (!node)
      return ERROR("unable to allocate node %d", this->graph->nodeCount);

    node->pose.rot = pose.rot;
    node->steerAngle = 0;
    node->segmentId = rnode->segmentId;
    node->laneId = rnode->laneId;
    node->waypointId = (rnode->interId == 0 ? rnode->waypointId : 0);
    node->interId = rnode->interId;
    node->railId = railId;
    node->flags.isLane = !(rnode->flags.isZonePerimeter || rnode->flags.isZoneParking);
    node->flags.isStop = rnode->flags.isStop;
    node->flags.isExit = rnode->flags.isExit;
    node->flags.isEntry = rnode->flags.isEntry;
    node->flags.isZonePerimeter = rnode->flags.isZonePerimeter;
    node->flags.isZoneParking = rnode->flags.isZoneParking;
    node->flags.isZone = rnode->flags.isZonePerimeter || rnode->flags.isZoneParking;
    node->flags.isOncoming = false;    
    node->prevWaypoint = rnode->prevWaypoint;
    node->nextWaypoint = rnode->nextWaypoint;
    
    // Store a pointer to the plan node in the road node; we will use
    // this in a moment to build arcs.
    rnode->data = (void*) node;
  }

  // Add in feasable arcs along lanes
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnodeA = this->road.nodes + i;
    if (!rnodeA->data)
      continue;

    for (j = 0; j < rnodeA->numNext; j++)
    { 
      rnodeB = rnodeA->next[j];
      if (!rnodeB->data)
        continue;
 
      nodeA = (PlanGraphNode*) rnodeA->data;
      assert(nodeA);
      nodeB = (PlanGraphNode*) rnodeB->data;
      assert(nodeB);

      if (nodeA->flags.isZonePerimeter && nodeB->flags.isZonePerimeter)
        continue;

      /* TODO
      if (!this->checkFeasibleStep(nodeA->pose, nodeB->pose))
      {
        MSG("infeasible lane %d.%d.%d.%d to %d.%d.%d.%d",
            nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
            nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);
        continue;
      }
      */

      this->graph->insertArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Generate the rails in oncoming lanes
int PlanGraphBuilder::genOncomingRails(int railId)
{
  int i, j;
  pose2f_t pose;
  float width, offset;
  RoadGraphNode *rnode, *rnodeA, *rnodeB;
  PlanGraphNode *node, *nodeA, *nodeB;
  
  // Add all of the nodes from the road graph.
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnode = this->road.nodes + i;

    // Reset user data so we dont get confused.
    rnode->data = NULL;

    // Dont generate oncoming nodes through intersections
    if (rnode->flags.isTurn)
      continue;

    // Dont generate oncoming nodes in zones
    if (rnode->flags.isZonePerimeter || rnode->flags.isZoneParking)
      continue;

    // On zone boundaries, generate nodes for the entry/exit points only
    if (rnode->flags.isZonePerimeter && !(rnode->flags.isEntry || rnode->flags.isExit))
      continue;

    // Pick an offset based on the current lane width
    width = (rnode->laneWidth - VEHICLE_WIDTH - 1.0) / (this->railCount - 1);
    width = MAX(width, this->railSpacing);    
    offset = -railId * width/2;

    // Apply a lateral offset to the node in the road graph
    pose.pos.x = rnode->pose.pos.x - (0.10 + offset)*sin(rnode->pose.rot);
    pose.pos.y = rnode->pose.pos.y + (0.10 + offset)*cos(rnode->pose.rot);
    pose.rot = rnode->pose.rot + M_PI;
    pose.rot = atan2f(sin(pose.rot), cos(pose.rot));

    // Create the new node
    node = this->graph->allocNode(pose.pos.x, pose.pos.y);
    if (!node)
      return ERROR("unable to allocate node %d", this->graph->nodeCount);

    node->pose.rot = pose.rot;
    node->segmentId = rnode->segmentId;
    node->laneId = rnode->laneId;
    node->waypointId = rnode->waypointId;
    node->interId = rnode->interId;
    node->railId = railId;
    node->flags.isLane = !(rnode->flags.isZonePerimeter || rnode->flags.isZoneParking);
    node->flags.isStop = rnode->flags.isStop;
    node->flags.isExit = rnode->flags.isExit;
    node->flags.isEntry = rnode->flags.isEntry;
    node->flags.isZonePerimeter = rnode->flags.isZonePerimeter;
    node->flags.isZoneParking = rnode->flags.isZoneParking;
    node->flags.isZone = node->flags.isZonePerimeter || node->flags.isZoneParking;
    node->flags.isOncoming = true;    
    node->prevWaypoint = rnode->prevWaypoint;
    node->nextWaypoint = rnode->nextWaypoint;
    
    // Store a pointer to the plan node in the road node; we will use
    // this in a moment to build arcs.
    rnode->data = (void*) node;
  }

  // Add in feasable arcs along lanes
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnodeA = this->road.nodes + i;
    if (!rnodeA->data)
      continue;

    for (j = 0; j < rnodeA->numPrev; j++)
    { 
      rnodeB = rnodeA->prev[j];
      if (!rnodeB->data)
        continue;
 
      nodeA = (PlanGraphNode*) rnodeA->data;
      assert(nodeA);
      nodeB = (PlanGraphNode*) rnodeB->data;
      assert(nodeB);

      if (nodeA->flags.isZonePerimeter && nodeB->flags.isZonePerimeter)
        continue;

      /* TODO
      if (!this->checkFeasibleStep(nodeA->pose, nodeB->pose))
      {
        MSG("infeasible lane %d.%d.%d.%d to %d.%d.%d.%d",
            nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
            nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);
        continue;
      }
      */

      this->graph->insertArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Generate turn maneuvers through intersections
int PlanGraphBuilder::genTurns()
{
  int i, j;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wpA, *wpB;

  rndf = &this->road.rndf;

  // Use the RNDF graph to find matching entry/exit points.
  // We may generate multiple turns for each entry/exit pair.
  for (i = 0; i < rndf->numWaypoints; i++)
  {
    wpA = rndf->waypoints + i;
    if (!wpA->flags.isExit)
      continue;
    for (j = 0; j < wpA->numNext; j++)
    {
      wpB = wpA->next[j];
      if (!wpB->flags.isEntry)
        continue;
      if (wpA->flags.isZonePerimeter && wpB->flags.isZonePerimeter)
        continue;
      this->genTurnWaypoints(wpA, wpB);
    }    
  }
  
  return 0;
}


// Generate turn manuevers for the given waypoints
int PlanGraphBuilder::genTurnWaypoints(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB)
{
  int i, j;
  float size;
  bool feasible;
  PlanGraphNodeList nodesA, nodesB;
  PlanGraphNode *nodeA, *nodeB;

  // Compute a size that is sure to capture all the rails.
  size = 1.0 + this->railCount * this->railSpacing; 

  // Get all the nodes in the vicinity of the entry/exit waypoints.
  this->graph->getRegion(&nodesA, wpA->px, wpA->py, size, size);
  this->graph->getRegion(&nodesB, wpB->px, wpB->py, size, size);

  assert(nodesA.size() > 0);
  assert(nodesB.size() > 0);

  feasible = false;
  
  // Look through the lists to find the correct entry/exit nodes,
  // (there may be multiple matches if there are multiple rails), then
  // generate the turn maneuver between these nodes.
  for (i = 0; i < (int) nodesA.size(); i++)
  {
    nodeA = nodesA[i];

    if (nodeA->segmentId != wpA->segmentId)
      continue;
    if (nodeA->laneId != wpA->laneId)
      continue;
    if (nodeA->waypointId != wpA->waypointId)
      continue;
    if (!nodeA->flags.isExit)
      continue;
    
    for (j = 0; j < (int) nodesB.size(); j++)
    {
      nodeB = nodesB[j];
          
      if (nodeB->segmentId != wpB->segmentId)
        continue;
      if (nodeB->laneId != wpB->laneId)
        continue;
      if (nodeB->waypointId != wpB->waypointId)
        continue;
      if (!nodeB->flags.isEntry)
        continue;

      // Only generate to/from the same rail
      if (nodeA->railId != nodeB->railId)
        continue;

      //MSG("trying %d %d %d.%d.%d.%d to %d.%d.%d.%d", i, j,
      //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
      //    nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);

      if (this->genTurnNodes(nodeA, nodeB) == 0)
        feasible = true;
    }
  }

  // Where any of these maneuvers feasible?
  //if (!feasible)
  //{
  //  MSG("infeasible turn %d.%d.%d to %d.%d.%d", 
  //      wpA->segmentId, wpA->laneId, wpA->waypointId, 
  //      wpB->segmentId, wpB->laneId, wpB->waypointId);
  //}

  return 0;
}


// Generate a turn maneuver between the given nodes
int PlanGraphBuilder::genTurnNodes(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry)
{
  bool feasible;

  feasible = false;

  // Generate a nominal turn (may or may not be feasible)
  if (true)
    feasible = this->genNominalTurn(nodeExit, nodeEntry);
   
  // Should have a feasible turn at this point; if not the fall-back
  // is to insert a feasible maneuver.
  if (true) // Always generate the infeasible !feasible)
  {
    feasible = this->checkFeasible(nodeExit->pose, nodeEntry->pose);
    PlanGraphNode::Flags flags = {0};
    flags.isTurn = true;
    flags.isInfeasible = !feasible;
    this->insertManeuver(nodeExit, nodeEntry, flags);
  }

  // Generate two-part (wide) turns; start from a vehicle-length
  // behind the stop line and move forward until we can find a
  // reasonable turn.
  if (!feasible)
  {
    MSG("infeasible turn %d.%d.%d %+d to %d.%d.%d %+d",
        nodeExit->segmentId, nodeExit->laneId, nodeExit->waypointId, nodeExit->railId,
        nodeEntry->segmentId, nodeEntry->laneId, nodeEntry->waypointId, nodeEntry->railId);
  }

  return 0;
}


// Generate a nominal turn
bool PlanGraphBuilder::genNominalTurn(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry)
{
  bool feasible;
  int i, steps;
  PlanGraphNode *nodeA, *nodeB, *nodeAA, *nodeBB;
  float curve, minCurve;
  
  feasible = false;
  
  // Max number of steps to take along the entry/exit rails to
  // broaden or shorten the turn.
  steps = (int) (4.0 / this->spacing);   // MAGIC

  minCurve = FLT_MAX;
  nodeAA = nodeA = nodeExit;
  nodeBB = nodeB = nodeEntry;

  // Try different exit points
  for (i = 0; i <= steps; i++)
  {
    // Find the maneuver with the smallest maximum curvature
    curve = this->checkCurvature(nodeA->pose, nodeB->pose);
    if (curve < minCurve)
    {
      minCurve = curve;
      nodeAA = nodeA;
      nodeBB = nodeB;
    }
    
    if (nodeA->numPrev > 0)
      if (nodeA->prev[0]->flags.isLane)
        if (nodeA->prev[0]->interId != 0 || nodeA->prev[0]->waypointId == nodeEntry->waypointId)
          nodeA = nodeA->prev[0];

    if (nodeB->numNext > 0)
      if (nodeB->next[0]->flags.isLane)
        if (nodeB->next[0]->interId != 0 || nodeB->next[0]->waypointId == nodeExit->waypointId)
          nodeB = nodeB->next[0];
  }

  // Set the node flags for these maneuvers
  PlanGraphNode::Flags flags = {0};
  flags.isTurn = 1;

  // Check for feasibility
  feasible = this->checkFeasible(nodeA->pose, nodeB->pose);
      
  // Set the node flags for these maneuvers
  flags.isTurn = 1;
  flags.isInfeasible = false; // !feasible;

  // Insert this manuever
  this->insertManeuver(nodeAA, nodeBB, flags);

  // If we have inserted a turn between A and B, we need
  // to mark these nodes and exit/entry points.
  nodeAA->flags.isExit = nodeExit->flags.isExit;
  // REMOVE nodeA->flags.isStop = nodeExit->flags.isStop;
  nodeBB->flags.isEntry = nodeEntry->flags.isEntry;

  return feasible;
}


// Generate a one-part turn
bool PlanGraphBuilder::genOneTurn(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry)
{
  bool found, feasible;
  int i, j, k, steps;
  PlanGraphNode *nodeA, *nodeB;
  pose2f_t poseA, poseB;
  float curve, minCurve;
  PlanGraphNode *nodeAA, *nodeBB;
  pose2f_t poseAA, poseBB;
  
  feasible = false;
  
  // Max number of steps to take along the entry/exit rails to
  // broaden or shorten the turn.
  steps = (int) (4.0 / this->spacing);   // MAGIC

  minCurve = FLT_MAX;
  nodeAA = nodeExit;
  poseAA = nodeAA->pose;
  nodeBB = NULL;
  poseBB = nodeBB->pose;

  // Try different exit points
  for (j = -steps; j <= +steps; j++) // TESTING
  {
    nodeA = nodeExit;
    poseA = nodeA->pose;
    found = true;
    if (j < 0)
    {      
      // Try to find a node in the graph
      found = false;
      for (k = 0; k < -j; k++)
      {
        if (nodeA->numPrev > 0)
        {
          if (nodeA->prev[0]->interId != 0 || nodeA->prev[0]->waypointId == nodeEntry->waypointId)
          {
            if (nodeA->prev[0]->flags.isLane)
            {
              nodeA = nodeA->prev[0];
              poseA = nodeA->pose;
              found = true;
            }
          }
        }
      }
    }
    else if (j > 0)
    {
      // Try to find a node in the graph
      found = false;
      for (k = 0; k < +j; k++)
      {
        if (nodeA->numNext > 0)
        {
          if (nodeA->next[0]->interId != 0 || nodeA->next[0]->waypointId == nodeEntry->waypointId)
          {
            if (nodeA->next[0]->flags.isLane)
            {
              nodeA = nodeA->next[0];
              poseA = nodeA->pose;
              found = true;
            }
          }
        }
      }
    }

    // If not found, we may have to create one
    if (!found)
    {
      nodeA = NULL;
      poseA.pos = vec2f_transform(nodeExit->pose, vec2f_set(j * this->spacing, 0));
    }

    // Try different entry points
    //for (i = -steps; i <= +steps; i++)
    i = 0; // TESTING
    {
      found = true;
      nodeB = nodeEntry;
      poseB = nodeB->pose;
      if (i < 0)
      {
        found = false;
        for (k = 0; k < -i; k++)
        {
          if (nodeB->numPrev > 0)
          {
            if (nodeB->prev[0]->interId != 0 || nodeB->prev[0]->waypointId == nodeEntry->waypointId)
            {
              if (nodeB->prev[0]->flags.isLane)
              {
                nodeB = nodeB->prev[0];
                poseB = nodeB->pose;
                found = true;
              }
            }
          }
        }
      }
      else if (i > 0)
      {
        found = false;
        for (k = 0; k < +i; k++)
        {
          if (nodeB->numNext > 0)
          {
            if (nodeB->next[0]->interId != 0 || nodeB->next[0]->waypointId == nodeEntry->waypointId)
            {
              if (nodeB->next[0]->flags.isLane)
              {
                nodeB = nodeB->next[0];
                poseB = nodeB->pose;
                found = true;
              }
            }
          }
        }
      }

      // If not found, we may have to create one
      if (!found)
      {
        nodeB = NULL;
        poseB.pos = vec2f_transform(nodeEntry->pose, vec2f_set(i * this->spacing, 0));
      }

      // Find the maneuver with the smallest maximum curvature
      curve = this->checkCurvature(poseA, poseB);
      if (curve < minCurve)
      {
        minCurve = curve;
        nodeAA = nodeA;
        poseAA = poseA;
        nodeBB = nodeB;
        poseBB = poseB;
      }
    }
  }

  if (!nodeAA)
    MSG("need intermediate exit %d.%d.%d", nodeExit->segmentId, nodeExit->laneId, nodeExit->waypointId);
  if (!nodeBB)
    MSG("need intermediate entry %d.%d.%d", nodeEntry->segmentId, nodeEntry->laneId, nodeEntry->waypointId);
  
  //if (nodeAA && nodeBB)
  if (minCurve < FLT_MAX)
  {
    // Set the node flags for these maneuvers
    PlanGraphNode::Flags flags = {0};
    flags.isTurn = 1;

    if (!nodeAA)
    {
      // Create the intermediate node
      nodeAA = this->graph->allocNode(poseAA.pos.x, poseAA.pos.y);
      nodeAA->pose.rot = poseAA.rot;
      nodeAA->segmentId = nodeExit->segmentId;
      nodeAA->laneId = nodeExit->laneId;
      nodeAA->flags = flags;
      nodeAA->prevWaypoint = nodeExit->prevWaypoint;
      nodeAA->nextWaypoint = nodeEntry->nextWaypoint;
      this->insertManeuver(nodeExit, nodeAA, flags);
    }
    
    if (!nodeBB)
    {
      // Create the intermediate node
      nodeBB = this->graph->allocNode(poseAA.pos.x, poseAA.pos.y);
      nodeBB->pose.rot = poseAA.rot;
      nodeBB->segmentId = nodeEntry->segmentId;
      nodeBB->laneId = nodeEntry->laneId;
      nodeBB->flags = flags;
      nodeBB->prevWaypoint = nodeExit->prevWaypoint;
      nodeBB->nextWaypoint = nodeEntry->nextWaypoint;
      this->insertManeuver(nodeBB, nodeEntry, flags);
    }

    // Check for feasibility
    feasible = this->checkFeasible(nodeAA->pose, nodeBB->pose);
      
    // Set the node flags for these maneuvers
    flags.isTurn = 1;
    flags.isInfeasible = !feasible;

    // Insert this manuever
    this->insertManeuver(nodeAA, nodeBB, flags);

    // If we have inserted a turn between A and B, we need
    // to mark these nodes and exit/entry points.
    nodeAA->flags.isExit = nodeExit->flags.isExit;
    //nodeAA->flags.isStop = nodeExit->flags.isStop;
    nodeBB->flags.isEntry = nodeEntry->flags.isEntry;
  }

  return feasible;
}



// Generate a two-part wide turn; this will not connect the given
// nodes, but will attempt to connect the rails.
int PlanGraphBuilder::genWideTurn(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry,
                                  float exitDist, float maxTurnRadius)
{
  int i, steps;
  bool foundC;
  pose2f_t poseB = {{0}, 0};
  float minDist;
  PlanGraphNode *nodeA, *nodeB, *nodeC;
  
  // Pick a starting node based on the distance to the exit node.
  // TODO: allow us to walk forward as well, which implies creating
  // another intermediate node.
  nodeA = nodeExit;
  steps = (int) (exitDist / this->spacing);
  for (i = 0; i < steps; i++)
  {
    if (nodeA->numPrev > 0)
      if (nodeA->prev[0]->interId != 0 || nodeA->prev[0]->waypointId == nodeEntry->waypointId)
        nodeA = nodeA->prev[0];
  }

  // The tightest initial turn that lines us up with the entry point.
  steps = 100;
  minDist = FLT_MAX;
  for (i = 0; i < steps; i++)
  {
    vec2f_t pos;
    float minTurnRadius, turnRadius, px, py, s, c;

    // Vehicle turning radius
    minTurnRadius = this->wheelBase / tan(this->maxSteer);

    // Try turns with different radii, ranging from the min to the max
    // turn radius.
    turnRadius = (float) i / steps * (maxTurnRadius - minTurnRadius) + minTurnRadius;

    // Turning left
    if (sinf(nodeEntry->pose.rot - nodeA->pose.rot) < 0)
      turnRadius *= -1;

    // Compute the position of the intermediate point on the
    // tightest turning circle in the frame of nodeA.
    //s = sinf(nodeEntry->pose.rot - nodeA->pose.rot);
    //c = cosf(nodeEntry->pose.rot - nodeA->pose.rot);
    s = sinf(nodeEntry->pose.rot)*cosf(nodeA->pose.rot)
      - cosf(nodeEntry->pose.rot)*sinf(nodeA->pose.rot);
    c = cosf(nodeEntry->pose.rot)*cosf(nodeA->pose.rot)
      + sinf(nodeEntry->pose.rot)*sinf(nodeA->pose.rot);
    px = turnRadius * s;
    py = turnRadius * (1 - c);

    //MSG("turn %f %f %f", turnRadius, px, py);

    // Transform into entry node frame
    pos = vec2f_transform(nodeA->pose, vec2f_set(px, py));
    pos = vec2f_transform(pose2f_inv(nodeEntry->pose), pos);
    
    // Record the pose that gets us closest to the entry node
    if (fabs(pos.y) < minDist)
    {
      minDist = fabs(pos.y);
      poseB.pos = vec2f_transform(nodeA->pose, vec2f_set(px, py));
      poseB.rot = nodeEntry->pose.rot;
    }
  }

  // Walk along the exit rail till we can get back into our lane.  We
  // *must* find a node that is in the correct portion of the lane
  // (previous waypoint equals the entry point), otherwise the
  // cooridor logic in the planner will get confused.
  foundC = false;
  nodeC = nodeEntry;
  steps = (int) (32.0 / this->spacing);   // MAGIC 64
  for (i = 0; i < steps; i++)
  {
    if (this->checkFeasible(poseB, nodeC->pose))
    {
      foundC = true;
      break;
    }
    if (nodeC->numNext == 0)
      break;
    nodeC = nodeC->next[0];
    if (nodeC->prevWaypoint != nodeEntry->prevWaypoint)
      break;
  }
  if (!foundC)
    return -1;

  //MSG("unable to get back into lane %d.%d.%d.%d to %d.%d.%d.%d",
  //    nodeExit->segmentId, nodeExit->laneId, nodeExit->waypointId, nodeExit->railId,
  //    nodeEntry->segmentId, nodeEntry->laneId, nodeEntry->waypointId, nodeEntry->railId);
  
  // Set the node flags for these maneuvers
  PlanGraphNode::Flags flags = {0};
  flags.isTurn = 1;

  // Create the intermediate node
  nodeB = this->graph->allocNode(poseB.pos.x, poseB.pos.y);
  nodeB->pose.rot = poseB.rot;
  nodeB->segmentId = nodeEntry->segmentId;
  nodeB->laneId = nodeEntry->laneId;
  nodeB->flags = flags;
  nodeB->prevWaypoint = nodeExit->prevWaypoint;
  nodeB->nextWaypoint = nodeEntry->nextWaypoint;

  // Join up the nodes
  this->insertManeuver(nodeA, nodeB, flags);
  this->insertManeuver(nodeB, nodeC, flags);

  // If we have inserted a turn between A and B, we need
  // to mark these nodes and exit/entry points.
  nodeA->flags.isExit = nodeExit->flags.isExit;
  nodeA->flags.isStop = nodeExit->flags.isStop;
  nodeC->flags.isEntry = nodeEntry->flags.isEntry;

  return 0;
}


// Generate basic connectivity into zones
int PlanGraphBuilder::genZones()
{
  int i, j;
  PlanGraphNode *nodeA, *nodeB;
  PlanGraphNode::Flags flags = {0};
  PlanGraphNodeList nodes;
  
  // Get all the nodes (should not be too many at this point)
  this->graph->getRegion(&nodes, 0, 0, 100000, 100000);

  // Generate turn from entries to exits
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeA = nodes[i];
    
    if (!nodeA->flags.isZonePerimeter)
      continue;
    if (!nodeA->flags.isEntry)
      continue;
    if (nodeA->flags.isOncoming)
      continue;

    for (j = 0; j < (int) nodes.size(); j++)
    {
      nodeB = nodes[j];

      if (!nodeB->flags.isZonePerimeter)
        continue;
      if (!nodeB->flags.isExit)
        continue;
      if (nodeB->flags.isOncoming)
        continue;
      if (nodeB->segmentId != nodeA->segmentId)
        continue;
      if (nodeB->railId != nodeA->railId)
        continue;

      // Insert the maneuver
      flags.isZone = true;
      flags.isInfeasible = !this->checkFeasible(nodeA->pose, nodeB->pose);
      this->insertManeuver(nodeA, nodeB, flags);
    }
  }

  // Generate "turns" from entry points to parking spots
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeA = nodes[i];
    
    if (!nodeA->flags.isZonePerimeter)
      continue;
    if (!nodeA->flags.isEntry)
      continue;
    if (nodeA->flags.isOncoming)
      continue;

    //MSG("zone entry %d.%d.%d",
    //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId);
    
    // Find parking spots
    for (j = 0; j < (int) nodes.size(); j++)
    {
      nodeB = nodes[j];

      if (!nodeB->flags.isZoneParking)
        continue;
      if (nodeB->segmentId != nodeA->segmentId)
        continue;
      if (nodeB->waypointId != 1)
        continue;
      if (nodeB->railId != nodeA->railId)
        continue;
      
      //MSG("zone parking %d.%d.%d",
      //    nodeB->segmentId, nodeB->laneId, nodeB->waypointId);
      
      // Create maneuver
      flags.isZone = true;      
      flags.isInfeasible = !this->checkFeasible(nodeA->pose, nodeB->pose);
      this->insertManeuver(nodeA, nodeB, flags);
    }
  }
  
  return 0;
}


// Generate feasible rail-change maneuvers (recursive)
int PlanGraphBuilder::genChanges()
{
  int i, j;
  int step;
  float spacing;
  PlanGraphNode *nodeA, *nodeB;
  uint8_t include, exclude;
  PlanGraphNodeList nodes;

  // MAGIC
  spacing = this->spacing;
  step = (int) ceil(8.0 / spacing);

  // Get all the nodes; there should not be too many at this point
  include = PLAN_GRAPH_NODE_LANE;
  exclude = PLAN_GRAPH_NODE_NONE;
  this->graph->getRegion(&nodes, 0, 0, FLT_MAX, FLT_MAX, include, exclude);
  
  // Consider all nodes 
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeA = nodes[i];

    if (i % 1000 == 0)
    {
      fprintf(stderr, "generating changes for %d/%d\r", i, (int) nodes.size());
      fflush(stderr);
    }

    // MAGIC
    if (nodeA->interId % step != 0)
      continue;
    
    for (j = 0; j < (int) nodes.size(); j++)
    {
      nodeB = nodes[j];

      // MAGIC
      if (nodeB->interId % step != 0)
        continue;

      // Check for legality
      if (!this->checkLegal(nodeA, nodeB))
        continue;
      
      // Do a feasibility check based on vehicle kinematics
      if (!this->checkFeasible(nodeA->pose, nodeB->pose))
        continue;

      //MSG("building %d.%d.%d.%d:%+d %d to %d.%d.%d.%d:%+d %d",
      //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId,
      //    nodeA->interId, nodeA->railId, nodeA->flags.isOncoming,
      //    nodeB->segmentId, nodeB->laneId, nodeB->waypointId,
      //    nodeB->interId, nodeB->railId, nodeB->flags.isOncoming);
      //MSG("pose %f %f %f  to %f %f %f",
      //    nodeA->pose.pos.x, nodeA->pose.pos.y, nodeA->pose.rot,
      //    nodeB->pose.pos.x, nodeB->pose.pos.y, nodeB->pose.rot);

      // Insert a feasible arc; this will get expanded only if the
      // node is in the ROI.
      this->graph->insertFeasibleArc(nodeA, nodeB);
    }
  }

  fprintf(stderr, "\n");
  
  return 0;
}


// Check for the legality of a lane/rail change
bool PlanGraphBuilder::checkLegal(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  //PlanGraphNode *node;
  float minDist, maxDist, maxSide;
  pose2f_t pose;

  // MAGIC
  // Limit the distance over which we make the changes.  The side
  // limit helps prevent us from cutting corners by doing a rail
  // change.
  maxSide = 10.0; 
  minDist = 5.0;
  maxDist = 40.0;

  // Dont make changes between segments
  if (nodeB->segmentId != nodeA->segmentId)
    return false;

  // Compute end-pose in frame of first node
  pose = pose2f_mul(pose2f_inv(nodeA->pose), nodeB->pose);

  // Dont make changes that are too near or too far.
  if (pose.pos.x < minDist || pose.pos.x > maxDist)
    return false;
  if (fabsf(pose.pos.y) > maxDist)
    return false;
  if (cosf(pose.rot) < cosf(20*M_PI/180)) // MAGIC
    return false;

  // If changing rails in or between regular lanes...
  if (nodeA->flags.isOncoming == false && nodeB->flags.isOncoming == false)
  {        
    // Dont make changes to/from the same lane and rail
    if (nodeB->laneId == nodeA->laneId && nodeB->railId == nodeA->railId)
      return false;

    // Move at most one lane at a time
    if (abs(nodeB->laneId - nodeA->laneId) > 1)
      return false;

    // If changing within a lane, shift left/right one rail only
    if (nodeB->laneId == nodeA->laneId && abs(nodeB->railId - nodeA->railId) > 1)
      return false;

    // If changing between lanes, shift only to the same rail in the
    // other lane.
    if (nodeB->laneId != nodeA->laneId && nodeB->railId != nodeA->railId)
      return false;

    // Dont make large lateral deviations
    if (fabs(pose.pos.y) > maxSide) 
      return false;
  }

  // If changing rails within or between oncoming lanes...
  else if (nodeA->flags.isOncoming == true && nodeB->flags.isOncoming == true)
  {
    return false;
  }

  // If changing rails into or out of oncoming lanes...
  else
  {
    // Dont make changes to/from the same lane and rail
    if (nodeB->laneId == nodeA->laneId && nodeB->railId == nodeA->railId)
      return false;

    // Move at most one lane at a time
    if (abs(nodeB->laneId - nodeA->laneId) > 1)
      return false;

    // Dont move to different rails
    if (nodeB->railId != nodeA->railId)
      return false;
  }

  /* TESTING
  // Walk along the initial rail; if we get to an entry/exit before we
  // go past nodeB, this change goes through an intersection and is
  // therefore illegal.
  node = nodeA;
  while (true)
  {
    if (node->numNext == 0)
      return false;
    if (node->flags.isExit || node->flags.isEntry)
      return false;
    pos = vec2f_transform(pose2f_inv(node->pose), nodeB->pose.pos);
    if (pos.x < 0)
      break;
    node = node->next[0];
  }
  */

  return true;
}


// Check for feasibility of an extended maneuver
bool PlanGraphBuilder::checkFeasible(pose2f_t poseA, pose2f_t poseB)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB, config;
  pose2f_t src, dst;
  double  dm, s;
  int i, numSteps;
  float spacing;

  // Step size
  spacing = this->spacing;

  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(poseB.pos, poseA.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing) + 10; // TESTING
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);
  
  // Check for feasibility along the entire maneuver, including the
  // final step.
  src.pos = vec2f_set(configA.x, configA.y);
  src.rot = configA.theta;
  for (i = 1; i < numSteps + 1; i++)
  {
    s = (double) i / numSteps;  
    config = maneuver_evaluate_pose(vp, mp, s);    
    dst.pos = vec2f_set(config.x, config.y);
    dst.rot = config.theta;    
    if (!this->checkFeasibleStep(src, dst))
      break;
    src = dst;
  }

  // Clean up
  maneuver_free(mp);
  free(vp);

  // Not feasible if we did not get to the end
  if (i < numSteps + 1)
    return false;
  
  return true;
}


// Do check for unfeasible maneuvers using the vehicle turning circle.
bool PlanGraphBuilder::checkFeasibleStep(pose2f_t poseA, pose2f_t poseB)
{
  float turnRadius;
  pose2f_t pose;
  float x0, y0, dx, dy, s, t;
    
  // Vehicle turning radius
  turnRadius = this->wheelBase / tan(this->maxSteer); 
  
  // Final pose in the initial frame
  pose = pose2f_mul(pose2f_inv(poseA), poseB);

  // Compute equation of line at right angles to current pose.
  // x = t dx + x0.
  // y = t dy + y0.
  x0 = pose.pos.x;
  y0 = pose.pos.y;
  dx = -sin(pose.rot);
  dy = +cos(pose.rot);

  // Trap straight-line case to avoid div-by-zero.
  if (fabsf(dx) < 1e-12)
  {
    if (pose.pos.x < 0)
      return false;
    // TODO FIX breaks rail change generation
    // if (fabsf(pose.pos.y) > 1e-12)
    //  return false;
    return true;
  }
  
  // Find intercept with x = 0; the values t and s specify the radii
  // of two turning circles.
  t = -x0 / dx;
  s = t * dy + y0;

  //MSG("check %f %f : %f %f : %f %f : %f", x0, y0, dx, dy, t, s, turnRadius);

  if (fabsf(t) < turnRadius)
    return false;
  if (fabsf(s) < turnRadius)
    return false;
  if (s * t < 0)
    return false;
        
  return true;
}


// Find the max curvature of an extended maneuver
float PlanGraphBuilder::checkCurvature(pose2f_t poseA, pose2f_t poseB)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB, config;
  pose2f_t src, dst;
  double  dm, s;
  int i, numSteps;
  float spacing, curve, maxCurve;

  // Step size
  spacing = this->spacing * 0.1; // MAGIC

  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(poseB.pos, poseA.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing) + 10; // MAGIC
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);

  maxCurve = 0;
  
  // Check for feasibility along the entire maneuver, including the
  // final step.
  src.pos = vec2f_set(configA.x, configA.y);
  src.rot = configA.theta;
  for (i = 1; i < numSteps + 1; i++)
  {
    s = (double) i / numSteps;  
    config = maneuver_evaluate_pose(vp, mp, s);    
    dst.pos = vec2f_set(config.x, config.y);
    dst.rot = config.theta;    
    curve = this->checkCurvatureStep(src, dst);
    if (curve > maxCurve)
      maxCurve = curve;
    src = dst;
  }

  // Clean up
  maneuver_free(mp);
  free(vp);
  
  return maxCurve;
}


// Get the curvature at a step
float PlanGraphBuilder::checkCurvatureStep(pose2f_t poseA, pose2f_t poseB)
{
  float turnRadius;
  pose2f_t pose;
  float x0, y0, dx, dy, s, t;
    
  // Vehicle turning radius
  turnRadius = this->wheelBase / tan(this->maxSteer); 
  
  // Final pose in the initial frame
  pose = pose2f_mul(pose2f_inv(poseA), poseB);

  // Compute equation of line at right angles to current pose.
  // x = t dx + x0.
  // y = t dy + y0.
  x0 = pose.pos.x;
  y0 = pose.pos.y;
  dx = -sin(pose.rot);
  dy = +cos(pose.rot);

  // Trap straight-line case to avoid div-by-zero.
  if (fabsf(dx) < 1e-12)
    return 0;
  
  // Find intercept with x = 0; the values t and s specify the radii
  // of two turning circles.
  t = -x0 / dx;
  s = t * dy + y0;

  // Curvature is reciprocal
  t = 1 / fabsf(t);
  s = 1 / fabsf(s);

  //MSG("check %f %f : %f %f : %f %f : %f", x0, y0, dx, dy, t, s, turnRadius);

  return MAX(t, s);
}


// Insert a maneuver between the given nodes.
int PlanGraphBuilder::insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, PlanGraphNode::Flags flags)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB;
  VehicleConfiguration config;
  double  dm, s;
  int i, numSteps;
  PlanGraphNode *src, *dst;
  float spacing;

  assert(nodeA);
  assert(nodeB);

  // Node spacing
  spacing = this->spacing;
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = nodeA->pose.rot;

  // Final vehicle configuration
  configB.x = nodeB->pose.pos.x;
  configB.y = nodeB->pose.pos.y;
  configB.theta = nodeB->pose.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);

  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);
  
  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a  node
    dst = this->graph->allocNode(config.x, config.y);
    assert(dst);

    // Fill out basic node data
    dst->pose.pos = vec2f_set((float) config.x, (float) config.y);
    dst->pose.rot = config.theta;
    dst->steerAngle = config.phi;
    
    // Fill out nominal RNDF data.  These are not very meaningful.
    if (i < numSteps / 2)
    {
      dst->segmentId = nodeA->segmentId;
      dst->laneId = nodeA->laneId;
      dst->waypointId = 0;
    }
    else
    {
      dst->segmentId = nodeB->segmentId;
      dst->laneId = nodeB->laneId;
      dst->waypointId = 0;
    }

    // Set the interpolant
    dst->interId = i;

    // Set the flags
    dst->flags = flags;

    // Record the final destination for this maneuver.  This help us
    // later when we want to delete the maneuver.
    // REMOVE dst->currentDest = nodeB;

    // Fill out waypoint pointers
    dst->prevWaypoint = nodeA->prevWaypoint;
    dst->nextWaypoint = nodeB->nextWaypoint;
    
    // Create an arc from the previous node to the new node
    this->graph->insertArc(src, dst);

    src = dst;
  }
  dst = nodeB;

  // Create an arc to the final node
  this->graph->insertArc(src, dst);
  
  maneuver_free(mp);
  free(vp);

  return 0;
}
