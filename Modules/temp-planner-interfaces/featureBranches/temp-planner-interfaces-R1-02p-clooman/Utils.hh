#ifndef UTILS_HH_
#define UTILS_HH_

#include <frames/point2.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <gcinterfaces/SegGoals.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/ActuatorState.h>
#include <trajutils/traj.hh>
#include <map/MapElementTalker.hh>

class Utils {

public: 
  static int init();
  static void destroy();

  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static LaneLabel getCurrentLane(VehicleState &vehState, Graph_t *graph);
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(VehicleState &vehState, Graph_t* graph, Map *map, Path_t* m_path, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal);
  static double getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(point2 &pos, Map *map, LaneLabel &lane);
  static double getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getAngleInRange(double angle);
  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement **me, VehicleState &vehState, Map *map, LaneLabel &lane); 
  static double monitorAliceSpeed(VehicleState &vehState, int m_estop);
  static void updateStoppedMapElements(Map *map);
  static uint64_t getTime();
  static int reverseProjection(point2 &out, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static int distToProjectedPoint(point2 &out, double &distance, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static void displayGraph(int sendSubgroup, Graph_t* graph, VehicleState vehState);
  static void printErrorInBinary(Err_t error);
  static void printCSpecs(CSpecs_t* cSpecs);
  static void displayPath(int sendSubgroup, Path_t* path, MapElementColorType colorType = MAP_COLOR_DARK_GREEN, MapId mapId = 12500);
  static void printPath(Path_t *path);
  static void printTraj(CTraj* traj);
  static void displayTraj(int sendSubgroup, CTraj* traj);

private :
  static CMapElementTalker m_meTalker;

};

#endif /*UTILS_HH_*/


