/*!
 * \file AliceStateHelper.hh
 * \brief Header for some wrapper functions around the vehicle struct (m_state)
 *
 * \author Noel du Toit
 * \date 10 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef ALICESTATEHELPER_HH_
#define ALICESTATEHELPER_HH_

#include "frames/pose2.h"
#include "frames/point2.hh"
#include "interfaces/VehicleState.h"
#include "map/Map.hh"


class AliceStateHelper {

public:
  static point2 getPositionFrontAxle(VehicleState &vehState);
  static point2 getPositionFrontBumper(VehicleState &vehState);
  static point2 getPositionRearAxle(VehicleState &vehState);
  static pose2f_t getPoseRearAxle(VehicleState &vehState); 
  static point2 getPositionRearBumper(VehicleState &vehState); 
  static double getVelocityMag(VehicleState &vehState); 
  static double getAccelerationMag(VehicleState &vehState);
  static point2 convertToGlobal(point2 gloToLocalDelta, point2 localPoint);
  static double getHeading(VehicleState &vehState);
  static LaneLabel getDesiredLaneLabel();
  static void setDesiredLaneLabel(LaneLabel lane);

private:
  /*! Desired Lane Label */
  static LaneLabel m_desiredLaneLabel;
};
#endif /*ALICESTATEHELPER_HH_*/
