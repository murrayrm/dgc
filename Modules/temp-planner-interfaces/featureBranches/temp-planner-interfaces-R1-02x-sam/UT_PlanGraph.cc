
/* 
 * Desc: Test plan graph builder
 * Date: 5 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include <stdlib.h>
#include <dgcutils/DGCutils.hh>
#include "PlanGraph.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Test reading/writing to file
int testSerialization(PlanGraph *src)
{
  uint64_t time;
  char *filename;
  PlanGraph dstA, dstB;
  
  // Write the graph to a file
  filename = "./plan_graph_test_a.pg";
  MSG("saving graph to %s", filename);
  time = DGCgettime();
  if (src->save(filename) != 0)
    return ERROR("unable to save graph");
  time = DGCgettime() - time;
  MSG("saving %.3f ms", (double) time * 1e-3);

  // Load a new graph from the file
  MSG("loading graph from %s", filename);
  time = DGCgettime();
  if (dstA.load(filename) != 0)
    return ERROR("unable to load destination graph");
  time = DGCgettime() - time;
  MSG("loading %.3f ms", (double) time * 1e-3);

  // Do some sanity checking
  MSG("quads %d/%d %d/%d nodes %d/%d %d/%d",
      src->quadCount, dstA.quadCount, src->quadBytes, dstA.quadBytes,
      src->nodeCount, dstA.nodeCount, src->nodeBytes, dstA.nodeBytes);

  filename = "./plan_graph_test_b.pg";
  
  // Save the graph another file
  MSG("saving graph to %s", filename);
  if (dstA.save(filename) != 0)
    return ERROR("unable to save destination graph");
  
  // These two files should be identical.  A necessary but not
  // sufficient condition for validity.
  system("diff --binary ./plan_graph_test_a.pg ./plan_graph_test_b.pg");

  // Load a new graph from the file
  MSG("loading graph from %s", filename);
  time = DGCgettime();
  if (dstB.load(filename) != 0)
    return ERROR("unable to load destination graph");
  time = DGCgettime() - time;
  MSG("loading %.3f ms", (double) time * 1e-3);

  // Do some sanity checking
  MSG("quads %d/%d %d/%d nodes %d/%d %d/%d",
      dstA.quadCount, dstB.quadCount, dstA.quadBytes, dstB.quadBytes,
      dstA.nodeCount, dstB.nodeCount, dstA.nodeBytes, dstB.nodeBytes);

  return 0;
}


// Test the ROI update speed.  Random walk to evaluate typical speeds.
int testROI(PlanGraph *graph)
{
  int i;
  uint64_t time;
  float px, py, size;

  px = 0;
  py = 0;
  size = 128;

  // Test initialization
  time = DGCgettime();  
  graph->updateROI(px, py, size);
  time = DGCgettime() - time;
  MSG("initialize ROI: %.3f ms/call", (double) time * 1e-3);

  // Test update
  time = DGCgettime();  
  for (i = 0; i < 20; i++)
  {
    px += (rand() % 2 - 0.5) * graph->quadScale * 4;
    py += (rand() % 2 - 0.5) * graph->quadScale * 4;
    MSG("ROI %.3f %.3f %.0f", px, py, size);
    graph->updateROI(px, py, size);
  }
  time = DGCgettime() - time;
  MSG("update ROI: %.3f ms/call size %.0f", (double) time * 1e-3 / i, size);

  return 0;
}  



int main(int argc, char **argv)
{
  char *filename;
  PlanGraph graph;
      
  filename = NULL;
  
  if (argc > 1)
    filename = argv[1];

  if (!filename)
    return fprintf(stderr, "usage: %s <FILE.RNDF.PG>\n", argv[0]);

  // Load the compiled graph file
  MSG("loading %s", filename);
  if (graph.load(filename) != 0)
    return -1;

  // Test reading/writing to file
  testSerialization(&graph);

  // Test ROI updates
  testROI(&graph);
  
  return 0;
}
