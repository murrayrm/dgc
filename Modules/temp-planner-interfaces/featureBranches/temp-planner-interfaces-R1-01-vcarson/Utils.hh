#ifndef UTILS_HH_
#define UTILS_HH_

#include <frames/point2.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <gcinterfaces/SegGoals.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <trajutils/maneuver.h>

class Utils {

public: 
  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(VehicleState &vehState, Graph_t* graph, Map *map, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal);
  static double getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(point2 &pos, Map *map, LaneLabel &lane);  static double getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getAngleInRange(double angle);
  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement **me, VehicleState &vehState, Map *map, LaneLabel &lane); 
  /*!\Given a point and path return the closest node to that point and the coordinates of the projection  */
  static Err_t projectOnPath(point2 pos, double heading, Path_t* path, GraphNode& graphNode, point2& projPoint);
  static double monitorAliceSpeed(VehicleState &vehState);
  static void updateStoppedMapElements(Map *map);
  static uint64_t getTime();

private :
};

#endif /*UTILS_HH_*/


