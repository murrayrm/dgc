
/* 
 * Desc: Graph-based planner; functions for local trajectory generation
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>

#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <trajutils/maneuver.h>
#include <trajutils/man2traj.hh>
#include <trajutils/traj.hh>

#include "TrajPlanner.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Additional limits
#define UINT64_MAX	18446744073709551615ULL

// Useful macros
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))


// Default constructor
TrajPlanner::TrajPlanner(Graph* graph)
{
  this->graph = graph;

  this->speeds.maxLaneSpeed = 4.0;
  this->speeds.maxTurnSpeed = 2.0;
  this->speeds.maxNearSpeed = 1.0;
  this->speeds.maxOffRoadSpeed = 1.0;
  this->speeds.maxStopSpeed = 0.5;
  this->speeds.maxAccel = +0.50;
  this->speeds.maxDecel = -0.50;
  this->speeds.deltaSpeed = 0.25;
  this->speeds.stopDist = 5;
  this->speeds.stopTol = 2.2;
  this->speeds.stopTime = 5.0;
  this->speeds.signalDist = 30.0;

  // Internal state variables
  this->stopState = 0;
  this->stopTime = 0;

  this->signalState = 0;
  this->signalTimestamp = 0;

  // HACK: get the nodes with stop signs (precedence calculation)
  if (true)
  {
    int i;
    GraphNode *node;
    this->numStopNodes = 0;
    for (i = 0; i < graph->getStaticNodeCount(); i++)
    {
      node = graph->getNode(i);
      if (node->isStop && node->isExit)
      {
        assert((size_t) this->numStopNodes < sizeof(this->stopNodes)/sizeof(this->stopNodes[0]));
        this->stopNodes[this->numStopNodes++] = node;
      }
    }
  }
  
  return;
}


// Destructor
TrajPlanner::~TrajPlanner()
{
  return;
}


// Get the vehicle kinematics
int TrajPlanner::getKinematics(TrajPlannerKinematics *kin)
{
  *kin = this->kin;
  return 0;
}


// Current vehicle kinematic values.
int TrajPlanner::setKinematics(const TrajPlannerKinematics *kin)
{
  this->kin = *kin;
  return 0;
}


// Get the speed limits.
int TrajPlanner::getSpeeds(TrajPlannerSpeeds *speeds)
{
  *speeds = this->speeds;
  return 0;
}


// Set the speed limits.
int TrajPlanner::setSpeeds(const TrajPlannerSpeeds *speeds)
{
  this->speeds = *speeds;
  return 0;
}


// Initialize speed limits from an MDF file.
int TrajPlanner::loadMDF(const char *filename)
{
  MSG("loading %s", filename);
  if (this->mdf.loadFile(filename) != 0)
    return ERROR("failed loading MDF");
  
  return 0;
}


// Generate the speed profile
int TrajPlanner::genSpeedProfile(GraphPath *path,
                                 const VehicleState *vehState,
                                 const ActuatorState *actState)
{
  int i;
  double currentSpeed;
  double stopDist, dist;
  
  // Get the vehicle speed.
  currentSpeed = vehState->vehSpeed;
  
  // If there is no path, there is nothing to do
  if (path->pathLen <= 0)
    return 0;

  // Zeroth pass: walk along the path and construct the distance
  // profile.
  path->dists[0] = 0;
  for (i = 1; i < path->pathLen; i++) 
  {
    GraphNode *nodeA, *nodeB;
    double dx, dy, dd;
    nodeA = path->path[i - 1];
    nodeB = path->path[i + 0];
    dx = nodeB->pose.pos.x - nodeA->pose.pos.x;
    dy = nodeB->pose.pos.y - nodeA->pose.pos.y;
    dd = sqrt(dx * dx + dy * dy);
    assert(dd > 0);
    path->dists[i] = path->dists[i - 1] + dd;
  }

  // First pass: walk along the path and construct the nominal speed profile.
  // We give a small initial acceleration to get Alice moving.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    node = path->path[i];
    if (i == 0)
      path->speeds[i] = MIN(currentSpeed + this->speeds.deltaSpeed, this->speeds.maxLaneSpeed);
    else
      path->speeds[i] = this->speeds.maxLaneSpeed;
  }
  
  // Stop if there is a collision on this path.  TODO: this should
  // produce a soft stop.
  if (path->collideObs >= GRAPH_COLLIDE_MAP_INNER && path->goalDist < 100) // MAGIC
  {
    MSG("path has collision; hard stop");
    for (i = 0; i < path->pathLen; i++)
      path->speeds[i] = 0;
  }

  // Third pass: walk along the path and construct the max speed
  // profile.
  for (i = 0; i < path->pathLen; i++) 
  {
    GraphNode *node;
    double speed;
      
    node = path->path[i];
    speed = path->speeds[i];

    // Check for corridor/lane speeds
    if (node->type != GRAPH_NODE_TURN && node->segmentId > 0)
    {
      // If driving in a segment, consider the maximum speed for this
      // segment.  The miminum speed is currently ignored.
      double minSpeed = 0;
      double maxSpeed = 10;
      // this->mdf.getSpeedLimits(node->segmentId, &minSpeed, &maxSpeed);
      if (maxSpeed > 0)
        speed = MIN(speed, maxSpeed);
    }
    else
    {
      // Are we driving through an intersection?  If so, use the
      // intersection speed.
      speed = MIN(speed, this->speeds.maxTurnSpeed);
    }

    // Check for collisions with obstacles.  
    if (node->collideObs == GRAPH_COLLIDE_MAP_INNER)
      speed = 0;
    else if (node->collideObs == GRAPH_COLLIDE_MAP_OUTER)
      speed = MIN(speed, this->speeds.maxNearSpeed);

    // If the path is collides with obstacles (i.e., we can't reach the goal),
    // we might as well stop early.  This causes a soft stop.
    if (path->collideObs >= GRAPH_COLLIDE_MAP_OUTER && node->collideObs > 0)
      speed = 0;

    // Check for collisions with cars.
    if (node->collideCar == GRAPH_COLLIDE_MAP_INNER ||
        node->collideCar == GRAPH_COLLIDE_LANE_INNER)
      speed = 0;
    else if (node->collideCar > 0)
      speed = MIN(speed, this->speeds.maxNearSpeed);

    path->speeds[i] = speed;
  }
  
  // Find the first stop sign.  
  stopDist = DBL_MAX;
  for (i = 0; i < path->pathLen; i++)
  {
    GraphNode *node;
    node = path->path[i];
    if (node->isExit && node->isStop)
    {
      stopDist = path->dists[i] - this->speeds.stopDist;
      break;
    }
  }

  if (stopDist < DBL_MAX)
  {    
    double carDist;
    uint64_t giveWayTime;

    // Check to see if there are any cars already in the intersection
    carDist = DBL_MAX;
    for (i = 0; i < path->pathLen; i++)
    {
      GraphNode *node;
      node = path->path[i];
      if (node->collideCar > 0)
      {
        carDist = path->dists[i];
        break;
      }
    }
    
    // Check the intersection precendence to see if we must give way.
    // This is a total HACK right now, and only works for one intersection.
    giveWayTime = UINT64_MAX;
    for (i = 0; i < this->numStopNodes; i++)
    {
      GraphNode *node;
      node = this->stopNodes[i];
      if (node->carInLane && node->carTime < giveWayTime)
        giveWayTime = node->carTime;
    }
    
    // Update the state machine depending on where we are with respect
    // to the stopping region.  States: 0 = not in region, 1 = stopped
    // in region, 2 = starting in region.  We also stay stopped at the
    // stop sign if there is a car waiting at one of the stop signs
    // (and arrived before we stopped).
    dist = fabs(stopDist - path->dists[0]);
    if (dist > this->speeds.stopTol/2)
    {
      this->stopState = 0;
    }
    else if (this->stopState == 0 && currentSpeed < 0.30) // MAGIC
    {
      this->stopState = 1;
      this->stopTime = vehState->timestamp;
    }
    else if (this->stopState == 1 &&
             this->stopTime < giveWayTime && carDist > 20.0 && // MAGIC
             vehState->timestamp - this->stopTime > this->speeds.stopTime * 1000000)
    {
      this->stopState = 2;
    }

    // If we are stopping or stopped, set all nodes in the stopping
    // region to zero velocity.
    if (this->stopState < 2)
    {
      for (i = 0; i < path->pathLen; i++)
      {
        dist = fabs(stopDist - path->dists[i]);
        if (dist < this->speeds.stopTol/2)
          path->speeds[i] = 0;
      }
    }

    MSG("stop state %d dist %.1f time %.1f giveway %.1f cardist %.1f",
        this->stopState, stopDist,
        (this->stopState == 1 ? (vehState->timestamp - this->stopTime) * 1e-6 : -1),
        (giveWayTime < UINT64_MAX ? (vehState->timestamp - giveWayTime) * 1e-6 : -1),
        (carDist < DBL_MAX ? carDist : -1));
  }
   
  // Smooth out the accelerations
  for (i = 0; i < path->pathLen - 1; i++)
  {
    double d0, d1, s0, s1, acc, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i + 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i + 1];

    assert(d1 > d0);
    
    // Compute acceleration; if it is high, trim the speed
    acc = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (acc > this->speeds.maxAccel)
    {
      acc = this->speeds.maxAccel;
      speed = sqrt(2 * acc * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, acc, speed);
      assert(speed <= s1);
      path->speeds[i + 1] = MIN(path->speeds[i + 1], speed);
    }
  }

  // Smooth out the deccelerations
  for (i = path->pathLen - 1; i > 0; i--)
  {
    double d0, d1, s0, s1, dec, speed;
    
    d0 = path->dists[i + 0];
    d1 = path->dists[i - 1];    
    s0 = path->speeds[i + 0];    
    s1 = path->speeds[i - 1];

    assert(d1 < d0);
    
    // Compute acceleration; if it is high, trim the speed
    dec = (s1 * s1 - s0 * s0) / (d1 - d0) / 2;
    if (dec < this->speeds.maxDecel)
    {
      dec = this->speeds.maxDecel;
      speed = sqrt(2 * dec * (d1 - d0) + s0 * s0);
      //MSG("%f %f %f   %f %f", d1 - d0, s1 - s0, s1, dec, speed);
      assert(speed <= s1);
      path->speeds[i - 1] = MIN(path->speeds[i - 1], speed);
    }
  }

  return 0;
}


// Generate a vehicle trajectory.
int TrajPlanner::genTraj(const GraphPath *path,
                         const VehicleState *vehState,
                         const ActuatorState *actState,                           
                         CTraj *traj)
{
  int i, numSteps, numSpeeds;
  double roll, pitch, yaw;
  Vehicle *vp;
  Maneuver *maneuver;
  GraphNode *nodeA, *nodeB;
  double speeds[2], dists[2];
  VehicleConfiguration configA;  
  Pose2D poseA, poseB;
  double stepSize;

  // Spacing between trajectory points
  stepSize = 0.05; // MAGIC

  // Make sure we have a path
  if (path->pathLen < 2)
  {
    MSG("no valid destination, stopping vehicle");
    traj->startDataInput();
    traj->addPoint(vehState->utmNorthing, 0, 0,
                   vehState->utmEasting, 0, 0);
    return 0;
  }

  assert(path->pathLen >= 1);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->kin.wheelBase, this->kin.maxSteer);
      
  traj->startDataInput();

  // Start with the vehicle node
  nodeA = path->path[0];
  speeds[0] = path->speeds[0];
  dists[0] = path->dists[0];

  // Look for the first non-volatile node; this is the destination for the
  // first maneuver
  nodeB = NULL;
  for (i = 1; i < path->pathLen; i++)
  {
    nodeB = path->path[i];
    speeds[1] = path->speeds[i];
    dists[1] = path->dists[i];
    if (nodeB->type != GRAPH_NODE_VOLATILE)
      break;
  }
  assert(nodeB != NULL);

  // Compute the number of speeds to consider along this maneuver.
  numSpeeds = i + 1;
    
  // Compute the number of interpolation steps, based on the path
  // distance between the two nodes.
  numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

  // Use the initial configuration given by the state.
  // This causes oscillations.
  //configA.x = vehState->utmNorthing;
  //configA.y = vehState->utmEasting;
  //configA.theta = vehState->utmYaw;
  //configA.phi = actState->m_steerpos * this->kin.maxSteer;
  //speedA = vehState->vehSpeed;

  // Use the initial configuration given by the node.
  // This produces stable behavior at low speeds
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  poseB.theta = yaw;
    
  // Generate a trajectory from the maneuver.
  maneuver = maneuver_config2pose(vp, &configA, &poseB);
  maneuver_profile_single(vp, maneuver, numSpeeds, path->speeds, numSteps, traj);
  maneuver_free(maneuver);
  
  // Create the rest of the maneuvers
  for (; i < path->pathLen - 1; i++) 
  {
    nodeA = path->path[i + 0];
    nodeB = path->path[i + 1];

    speeds[0] = path->speeds[i + 0];
    speeds[1] = path->speeds[i + 1];

    dists[0] = path->dists[i + 0];
    dists[1] = path->dists[i + 1];
          
    // Compute the number of interpolation steps, based on the path
    // distance between the two nodes.
    numSteps = (int) ((dists[1] - dists[0]) / stepSize) + 1;

    // For non-vehicle nodes, use the node pose and assume zero-steering angle
    quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
    poseA.x = nodeA->pose.pos.x;
    poseA.y = nodeA->pose.pos.y;
    poseA.theta = yaw;

    // Final pose on node B
    quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
    poseB.x = nodeB->pose.pos.x;
    poseB.y = nodeB->pose.pos.y;
    poseB.theta = yaw;

    // Generate a trajectory from the maneuver list.
    maneuver = maneuver_pose2pose(vp, &poseA, &poseB);
    maneuver_profile_generate(vp, 1, &maneuver, speeds, numSteps, traj);
    maneuver_free(maneuver);
 
    // Dont plan too far into the future
    if (dists[1] > 20.0) // MAGIC
      break;
  }

  return 0;
}


// Compute turn signal state.
int TrajPlanner::genTurnSignal(const GraphPath *path,
                               const VehicleState *vehState,
                               int *signal)
{
  int i;
  GraphNode *node, *nodeA, *nodeB, *nodeC;
  
  if (path->pathLen <= 0)
  {
    *signal = this->signalState = 0;
    return 0;
  }

  // Current node
  nodeA = path->path[0];  
  if (nodeA->segmentId == 0)
  {
    // If we are outside a lane, do nothing
    this->signalTimestamp = vehState->timestamp;
    *signal = this->signalState;
    return 0;
  }

  // Reset the signal after some time interval
  if ((vehState->timestamp - this->signalTimestamp) * 1e-6 > 2.0) // MAGIC
    this->signalState = 0;

  // See when we next leave our lane.
  nodeB = NULL;
  for (i = 1; i < path->pathLen; i++)
  {
    node = path->path[i];
    if (path->dists[i] - path->dists[0] > this->speeds.signalDist)
      break;
    if (node->segmentId != nodeA->segmentId || node->laneId != nodeA->laneId)
    {
      //MSG("nodeB %f", path->dists[i]);
      nodeB = node;
      break;
    }
  }  

  // See when we subsequently re-enter a lane.
  nodeC = NULL;
  if (nodeB)
  {
    for (; i < path->pathLen; i++)
    {
      node = path->path[i];
      if (node->segmentId != nodeB->segmentId || node->laneId != nodeB->laneId)
      {
        //MSG("nodeC %f", path->dists[i]);
        nodeC = node;
        break;
      }
    }
  }

  if (nodeB && nodeA->segmentId == nodeB->segmentId)
  {
    double px, py, qx, qy;

    // This must be a lane change
    px = nodeB->pose.pos.x;
    py = nodeB->pose.pos.y;     
    qx = nodeA->transNG[0][0] * px + nodeA->transNG[0][1] * py + nodeA->transNG[0][3];
    qy = nodeA->transNG[1][0] * px + nodeA->transNG[1][1] * py + nodeA->transNG[1][3];

    if (qy < -1.0) // MAGIC
    {
      this->signalState = -1;
      this->signalTimestamp = vehState->timestamp;
    }
    else if (qy > +1.0) // MAGIC
    {
      this->signalState = +1;
      this->signalTimestamp = vehState->timestamp;
    }
  }
  else if (nodeB && nodeC)
  {
    double roll, pitch, yawB, yawC, theta;

    // This must be a turn
    quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yawB);
    quat_to_rpy(nodeC->pose.rot, &roll, &pitch, &yawC);
    theta = atan2(sin(yawC - yawB), cos(yawC - yawB));
      
    if (theta < -20 * M_PI/180) // MAGIC
    {
      this->signalState = -1;
      this->signalTimestamp = vehState->timestamp;
    }
    else if (theta > +20 * M_PI/180) // MAGIC
    {
      this->signalState = +1;
      this->signalTimestamp = vehState->timestamp;
    }
  }

  *signal = this->signalState;
  return 0;
}
