#ifndef GRAPHUTILS_HH_
#define GRAPHUTILS_HH_

// Forward declarations
namespace std
{
  class Lane;
  class RNDF;
}
class Map;
class MapElement;


class GraphUtils {

public: 
  
  // Generate lane nodes
  int genLanes(std::RNDF *rndf);

  // Generate a lane, driving either with traffic (direction = +1) or
  // against traffic (direction = -1).
  int genLane(std::Lane *lane, int direction);

  // Generate lane tangents
  int genLaneTangents(std::Lane *lane);
  
  // Generate turn manuevers
  int genTurns(std::RNDF *rndf);

  // Generate lane-change manuevers
  int genChanges(std::RNDF *rndf);

  // Generate K-turn maneuvers
  int genKTurns();

  // Generate a maneuver linking two nodes
  int genManeuver(int nodeType, GraphNode *nodeA, GraphNode *nodeB, double maxSteer);

  // Set the RNDF data on a change node
  int genChangeNode(GraphNode *node);

  // Set the RNDF data on a volatile node
  int genVolatileNode(GraphNode *node);

  // Initialize the graph from an RNDF file.
  int loadRndf(const char *filename);
  
};

#endif /*GRAPHUTILS_HH_*/
