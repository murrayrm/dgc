
/* 
 * Desc: Update the collision status of nodes in the graph.
 * Date: 28 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_STATUS_HH
#define PLAN_GRAPH_STATUS_HH


/** @file

TODO

*/

#include <stdio.h>
#include "PlanGraph.hh"


/// @brief Update graph status
class PlanGraphStatus
{
  public:

  /// @brief Constructor
  PlanGraphStatus(PlanGraph *graph);

  /// @brief Destructor
  virtual ~PlanGraphStatus();
  
  private:

  // Hide the copy constructor
  PlanGraphStatus(const PlanGraphStatus &that);

  public:

  // Set the inner bounding box.
  void setInner(float front, float rear, float side);

  // Set the outer bounding box by specifying the buffer size.
  void setOuterBuffer(float front, float rear, float side);

  // Check for any nodes in the obstacle bounding box (site frame)
  // @param[in] ox,oy Center of the box.
  // @param[in] sx, sy Dimensions of the box.
  bool checkBox(float ox, float oy, float sx, float sy, PlanGraphQuad *quad = NULL);

  // Update the status values along a line
  void updateLine(vec2f_t pa, vec2f_t pb, bool obs, bool car = false, bool pred = false);

  private:
  
  // Update the status values for intersecting nodes
  void updateQuad(float px, float py, bool obs, bool car, bool pred, PlanGraphQuad *quad = NULL);

  public:

  // The graph to update
  PlanGraph *graph;
  
  // Spacing for points along a line
  float lineSpacing;
  
  // Size of Alice for c-space tests
  float aliceInnerFront, aliceInnerRear, aliceInnerSide;
  float aliceOuterFront, aliceOuterRear, aliceOuterSide;

  // Maximum diagonal of Alice outer bounding box
  float aliceOuterDiag;
};

#endif

