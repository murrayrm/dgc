/*!
 * \file Console.hh
 * \brief Console header for the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef _CONSOLE_H
#define _CONSOLE_H

#define MAX_MSG        6
#define MSG_LENGTH     77

#include <ncurses.h>
#include <sstream>
#include <fstream>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>
#include <map/MapPrior.hh>
#include <pthread.h>
#include "sparrowhawk/SparrowHawk.hh"
#include "plannerDisplay.h"
#include "intersectionDisplay.h"
#include "debugDisplay.h"

class Console {

public:
  static void init();
  static void destroy();
  
  static void updateRate(double time);
  static void updateTrajectory(CTraj *traj);
  static void updateState(VehicleState &vehState);
  static void updateInter(bool SafetyFlag, bool CabmodeFlag, int CountPrecedence, string Status, PointLabel wp);
  static void updateInterPage2(vector<PrecedenceList_t> obstaclesPrecedence, vector<MapElement> obstaclesClearance);
  static void updatePred(string Status, int numberObstacles);
  static void updateLastWpt(PointLabel last);
  static void updateNextWpt(PointLabel next);
  static void updateTurning(int direction);
  static void updateFSMFlag(string flag);
  static void updateFSMState(string state);
  static void updateStopline(double dist);
  static void addMessage(char *format, ...);
  static void resetIntersection();
  static bool queryResetIntersectionFlag();
  static void togglePrediction();
  static bool queryTogglePredictionFlag();
  static void resetState();
  static bool queryResetStateFlag();
  static void toggleIntersectionSafety();
  static bool queryToggleIntersectionSafety();
  static void toggleIntersectionCabmode();
  static bool queryToggleIntersectionCabmode();
  static void updateMapSize(int size);
  static void increaseObstSize();
  static void increaseClearSize();

  static CSparrowHawk* display;

private:
  static bool initialized;

  // variables for Rate
  static double rate1;
  static double rate2;

  // variables for Trajectory
  static double traj_x;
  static double traj_y;
  static double traj_vel;
  static double traj_head;
  static double traj_f_x;
  static double traj_f_y;
  static double traj_f_vel;
  static double traj_f_head;

  // variables for Prediction
  static int pred_numbers;

  // variables for Prediction
  static double distToStop;

  // variables for Alice State
  static double state_velocity;

  // variables for messages
  static char messages[MAX_MSG][MSG_LENGTH];

  // flags associated to keyboard commands in the console
  static bool resetIntersectionFlag;
  static bool togglePredictionFlag;
  static bool resetStateFlag;
  static bool toggleIntersectionSafetyFlag;
  static bool toggleIntersectionCabmodeFlag;

  // debug map information
  static int mapSize;
  static int obstSize;
  static int clearSize;
};

/* Pointer to SparrowHawk accessible to all who include this file*/

/*! Stream used for all message printing in follower */
extern stringstream printStream;

/*! Function used to print messages either in the terminal or SparrowHawk window */
void printMessage();

#endif
