/*!
 * \file CmdArgs.cc
 * \brief Source for the command line arguments object that gets passed around in the planner stack
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "CmdArgs.hh"

int CmdArgs::sn_key = 0;
bool CmdArgs::debug = false;
bool CmdArgs::use_RNDF = false;
string CmdArgs::RNDF_file = "";
int CmdArgs::verbose_level = 0;
bool CmdArgs::logging = false;
bool CmdArgs::console = false;
bool CmdArgs::lane_cost = false;
string CmdArgs::log_path = "";
string CmdArgs::log_filename = "tplanner.log";
int CmdArgs::log_level = 0;
bool CmdArgs::closed_loop = false;
bool CmdArgs::noprediction = false;
bool CmdArgs::stepbystep_load = false;
bool CmdArgs::load_graph_files = false;
