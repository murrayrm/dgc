/**********************************************************
 **
 **  PLANNERINTERFACES.H
 **
 **    Author: Noel du Toit
 **    Created: Tue Jul 10 08:38:32 2007
 **
 **********************************************************
 ** The interfaces between the executable planner module, 
 ** planner, and its libraries (GraphUpdater, PathPlanner, 
 ** LogicPlanner, VelPlanner) are defined here.
 **********************************************************/

#ifndef PLANNERINTERFACES_H
#define PLANNERINTERFACES_H

#include "PlanGraph.hh"
#include "PlanGraphPath.hh"
#include <sstream>
#include <queue>

#include <cspecs/CSpecs.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <gcinterfaces/SegGoals.hh>
#include <map/Map.hh>

using namespace std;


/******************************************************************
* State problem enum definitions and a way to make them printable
* For states used in the finite state machine(s)
*******************************************************************/
// Convert command to string, helping macro.
#define MAP_TO_STRING(A) #A, 	
// List all items, helping macro
#define LIST_ITEMS(A) A,		

/********** Define State Enum **********/ 
#define FOR_ALL_STATES(STATE)\
	STATE(DRIVE) STATE(STOP_INT) STATE(STOP_OBS) STATE(BACKUP) STATE(UTURN) STATE(PAUSE)
typedef enum  {
	FOR_ALL_STATES(LIST_ITEMS) STATES_COUNT
} FSM_state_t;
const char *const stateString[STATES_COUNT] = {
	FOR_ALL_STATES(MAP_TO_STRING)
};

/********** Define Flag Enum **********/
// Flags that guide the update of the graph
#define FOR_ALL_FLAGS(FLAG) FLAG(PASS) FLAG(NO_PASS) FLAG(OFFROAD) FLAG(PASS_REV)
typedef enum  {
	FOR_ALL_FLAGS(LIST_ITEMS) FLAGS_COUNT
} FSM_flag_t;
const char *const flagString[FLAGS_COUNT] = {
	FOR_ALL_FLAGS(MAP_TO_STRING)
};

/********** Define Region Enum **********/
// Regions that we need to be able to plan through
#define FOR_ALL_REGIONS(REGION) REGION(ZONE_REGION) REGION(ROAD_REGION) REGION(INTERSECTION) 
#define DECLARE_REGIONS(A) A,
typedef enum  {
	FOR_ALL_REGIONS(DECLARE_REGIONS) REGIONS_COUNT
} FSM_region_t;
const char *const regionString[REGIONS_COUNT] = {
	FOR_ALL_REGIONS(MAP_TO_STRING)
};

/********** Define Planner Enum **********/
// Planners that we want to use
#define FOR_ALL_PLANNERS(PLANNER)\
	PLANNER(RAIL_PLANNER) PLANNER(S1PLANNER) PLANNER(DPLANNER)\
	PLANNER(CIRCLE_PLANNER) PLANNER(RRT_PLANNER) 
typedef enum  {
	FOR_ALL_PLANNERS(LIST_ITEMS) PLANNERS_COUNT
} FSM_planner_t;
const char *const plannerString[PLANNERS_COUNT] = {
	FOR_ALL_PLANNERS(MAP_TO_STRING)
};

/********** Define Obstacle Enum **********/
/// How do we deal with obstacles? Can be safe, aggressive, and not safe (extreme)
#define FOR_ALL_OBSTACLES(OBSTACLE)\
	OBSTACLE(OBSTACLE_SAFETY) OBSTACLE(OBSTACLE_AGGRESSIVE) OBSTACLE(OBSTACLE_BARE) 
typedef enum  {
	FOR_ALL_OBSTACLES(LIST_ITEMS) OBSTACLES_COUNT
} FSM_obstacle_t;
const char *const obstacleString[OBSTACLES_COUNT] = {
	FOR_ALL_OBSTACLES(MAP_TO_STRING)
};

/********** Define State Problem Structure **********/
/// Defines the state problem that is returned by the logic planner
typedef struct {
  FSM_state_t state;
  double probability;
  FSM_flag_t flag;
  FSM_region_t region;
  FSM_planner_t planner;
  FSM_obstacle_t obstacle;
} StateProblem_t;



/********** Printable enumeration representation of Planner internal errors **********/

// A dedicated error type
typedef uint32_t Err_t;

// This is all errors in one linear enum list, printable. 
// Needs a function to use with the power 2-defines above
#define FOR_ALL_ERRORS(ERR)\
	ERR(PLANNERS_OK) 					ERR(LP_MAP_INCOMPLETE)\
	ERR(LP_FAIL_MPLANNER_LANE_BLOCKED) 	ERR(GU_MAP_INCOMPLETE)\
	ERR(PP_COLLISION_SAFETY) 			ERR(PP_NOPATH_LEN)\
	ERR(PP_NOPATH_COST) 				ERR(VP_UTURN_FINISHED)\
	ERR(VP_BACKUP_FINISHED) 			ERR(PP_NONODEFOUND)\
	ERR(P_EXTRACT_SUBPATH_ERROR) 		ERR(S1PLANNER_FAILED)\
	ERR(CIRCLE_PLANNER_FAILED) 			ERR(GU_UPDATE_FROM_MAP_ERROR)\
	ERR(LP_FAIL_MPLANNER_ROAD_BLOCKED)  ERR(LP_FAIL_MPLANNER_LOST)\
	ERR(S1PLANNER_START_BLOCKED) 		ERR(S1PLANNER_END_BLOCKED)\
	ERR(S1PLANNER_COST_TOO_HIGH)  		ERR(VP_PARKING_SPOT_FINISHED)\
	ERR(PP_COLLISION_AGGRESSIVE) 		ERR(PP_COLLISION_BARE)\
	ERR(S1PLANNER_PATH_INCOMPLETE)		ERR(PP_COLLISION_BACKUP)
typedef enum {
	FOR_ALL_ERRORS(LIST_ITEMS) ERRORS_COUNT
} Error_t;
const char *const errorString[ERRORS_COUNT] = {
	FOR_ALL_ERRORS(MAP_TO_STRING)
};

// Magic function to convert error codes to a string
// This function IS used by other modules, whatever the compiler tells you!
static const string plannerErrorsToString(Err_t error) {
	stringstream strout("");

	// Extract all powers of 2 from error
	int i=0;	// Loop counter
	int j=0;	// Number of errors counter
	while (error || i>=ERRORS_COUNT) {	// Still any information left? or more than there is elements in string vector.
		// Mask out bit 0 only
		if (error & 0x01) {
			// If set, we have an error on place i
			// Print the enumeration of this error
			strout << errorString[i+1];	// With an ending space and correct index offset
			strout << ", ";
			j++;	// Error # just increased!
		}
		// Shift error bitwise one step right (shift out LSB)
		error >>= 1;
		i++;	// Count loops
	}
	// Insert total error count at the end of the string
	strout <<"("<<j<<")";
	return strout.str();	// Return the accumulated string
}

// These are defined on the power of 2 so that they can be 
// combined and easily masked in one 32-bit variable
#define LP_OK      						0		// 0
#define GU_OK                   		0		// 0
#define PP_OK                  			0		// 0	
#define VP_OK                  			0		// 0
#define PLANNER_OK             			0		// 0
#define S1PLANNER_OK            		0		// 0
#define CIRCLE_PLANNER_OK       		0		// 0
#define LP_MAP_INCOMPLETE      			1		// (1<<0)	// 2^0
#define LP_FAIL_MPLANNER_LANE_BLOCKED   2		// (1<<1)	// 2^1
#define GU_MAP_INCOMPLETE       		4		// (1<<2)	// 2^2
#define PP_COLLISION_SAFETY     		8		// (1<<3)	// 2^3
#define PP_NOPATH_LEN          			16		// (1<<4)	// 2^4
#define PP_NOPATH_COST         			32		// (1<<5)	// 2^5
#define VP_UTURN_FINISHED      			64		// (1<<6)	// 2^6
#define VP_BACKUP_FINISHED    			128		// (1<<7)	// 2^7
#define PP_NONODEFOUND        			256		// (1<<8)	// 2^8
#define P_EXTRACT_SUBPATH_ERROR   		512		// (1<<9)	// 2^9
#define S1PLANNER_FAILED          		1024	// (1<<10)	// 2^10
#define CIRCLE_PLANNER_FAILED     		2048	// (1<<11)	// 2^11
#define GU_UPDATE_FROM_MAP_ERROR  		4096	// (1<<12)	// 2^12
#define LP_FAIL_MPLANNER_ROAD_BLOCKED 	8192	// (1<<13)	// 2^13
#define LP_FAIL_MPLANNER_LOST     		16384	// (1<<14)	// 2^14
#define S1PLANNER_START_BLOCKED			32768	// (1<<15)	// 2^15
#define S1PLANNER_END_BLOCKED		  	65536	// (1<<16)	// 2^16
#define S1PLANNER_COST_TOO_HIGH			131072	// (1<<17)	// 2^17
#define VP_PARKING_SPOT_FINISHED  		262144	// (1<<18)	// 2^18
#define PP_COLLISION_AGGRESSIVE   		524288	// (1<<19)	// 2^19
#define PP_COLLISION_BARE         		1048576	// (1<<20)	// 2^20
#define S1PLANNER_PATH_INCOMPLETE		2097152	// (1<<21)	// 2^21
#define PP_COLLISION_ASTERN				4194304	// (1<<22)	// 2^22

// the threshold of the cost which will result in a
// S1PLANNER_COST_TOO_HIGH error getting reported
// This is not an error code as the ones above!
#define S1PLANNER_HIGH_COST_VALUE	500




// PATH
// REMOVE typedef struct GraphPath Path_t;

// COST
typedef int Cost_t;

// VEL PLANNER PARAMS
typedef struct {
  double minSpeed;
  double maxSpeed;
} Vel_params_t;

// PATH PLANNER PARAMS
typedef struct {
  FSM_flag_t flag;
  bool planFromCurrPos;
  int zoneId;
  int spotId; // 0 is perimeter, >0 is spot numbers
  int exitId;
  double velMin;
  double velMax;
  bool readPathPlanParams;
} Path_params_t;

// LOGIC PLANNER PARAMS
typedef struct {
  SegGoals::SegmentType segment_type;
  SegGoals seg_goal;
  deque<SegGoals>* seg_goal_queue;
  PlanGraph* m_graph;
  PlanGraphPath* m_path;
  bool planFromCurrPos;
  int m_estop;
  bool readConfig;
  bool evasion;
  vector<PointLabel> mergingWaypoints;
  int transpos;
} Logic_params_t;

typedef struct {
  PointLabel waypoint;
  MapElement element;
  double velocity;
  double distance;
  double eta;
  LaneLabel lane;
  bool precedence;
  bool closest;
  bool checkedQueuing;
  bool updated;
  unsigned long long lastUpdated;
} precedenceList_t;

// PREDICTION INTERFACE
typedef int Prediction_t;

typedef CSpecs CSpecs_t;


#endif
