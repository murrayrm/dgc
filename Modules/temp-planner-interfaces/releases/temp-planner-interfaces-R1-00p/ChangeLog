Wed Aug  8 11:18:47 2007	Christian Looman (clooman)

	* version R1-00p
	BUGS:  
	FILES: PlannerInterfaces.h(32583)
	commit after merging branches

	FILES: PlannerInterfaces.h(32575)
	Changed logicParamtT so that LogicPlanner receives the whole
	segment goal queue

Mon Aug  6 20:25:03 2007	Sven Gowal (sgowal)

	* version R1-00o
	BUGS:  
	FILES: Graph.cc(32376), Graph.hh(32376)
	updated to enable unloading in Graph-updater

Sat Aug  4  3:32:18 2007	Magnus Linderoth (mlinderoth)

	* version R1-00n
	BUGS:  
	FILES: GraphPath.hh(31906)
	Added acceleration and curvature fields to GraphPath

	FILES: PlannerInterfaces.h(31907)
	Defined Vel_params_t

Fri Aug  3 20:13:28 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: CmdArgs.cc(31749), CmdArgs.hh(31749)
	added update_from_map flag

	FILES: Graph.hh(31840)
	Added node type for lane changes with broken white line

Fri Aug  3  0:15:16 2007	Sven Gowal (sgowal)

	* version R1-00l
	BUGS:  
	New files: Quadtree.cc Quadtree.hh
	FILES: CmdArgs.cc(31444), CmdArgs.hh(31444)
	Added --step-by-step-loading argument

	FILES: CmdArgs.cc(31623), CmdArgs.hh(31623), Graph.cc(31623),
		Graph.hh(31623)
	Now loading from files possible

	FILES: Graph.cc(31372), Graph.hh(31372), Makefile.yam(31372)
	Using quadtree for spacial indexing

Tue Jul 31 19:53:05 2007	Sven Gowal (sgowal)

	* version R1-00k
	BUGS:  
	FILES: Graph.cc(31237), Graph.hh(31237)
	merged

	FILES: Graph.cc(30955), Graph.hh(30955)
	Overloaded getNearestNode function

	FILES: Graph.cc(31190), Graph.hh(31190)
	Modifying getNearestNode function

	FILES: Graph.cc(31192), Graph.hh(31192)
	Added getNext/PrevChangeNode

	FILES: Graph.cc(31227)
	merged

	FILES: Graph.hh(30899)
	Added a GRAPH_ARC_CHANGE type

	FILES: Graph.hh(30901)
	Changed arc type to int

Tue Jul 31 15:06:59 2007	Jessica Gonzalez (jengo)

	* version R1-00j
	BUGS:  
	FILES: Graph.cc(31115)
	increased size of graph to enable loading larger rndfs

Tue Jul 31 13:32:12 2007	Christian Looman (clooman)

	* version R1-00i
	BUGS:  
	FILES: CmdArgs.cc(31070), CmdArgs.hh(31070)
	Changed cmdArgs for command line argument noprediction

Tue Jul 31 11:17:12 2007	Christian Looman (clooman)

	* version R1-00h
	BUGS:  
	FILES: Console.cc(31021), Console.hh(31021)
	added output for Prediction

Fri Jul 27 16:26:14 2007	Noel duToit (ndutoit)

	* version R1-00g
	BUGS:  
	New files: AliceStateHelper.cc AliceStateHelper.hh CmdArgs.cc
		CmdArgs.hh Console.cc Console.hh Log.cc Log.hh
	FILES: Graph.hh(30369)
	Added a type to the arcs

	FILES: Graph.hh(30437)
	Added ignored member to GraphArc

	FILES: Makefile.yam(30374)
	Moved the common files in planner over from planner itself to this
	interfaces library.

	FILES: Makefile.yam(30401)
	Fixed

	FILES: PlannerInterfaces.h(30293)
	started enforcing continuity between plans. Added bool
	planFromCurrPos to the path-planner params.

	FILES: PlannerInterfaces.h(30356)
	Added an error for logic-planner

	FILES: PlannerInterfaces.h(30479)
	added error for path planner to deal with the case where we cannot
	find a close node on the path to plan from.

	FILES: PlannerInterfaces.h(30499)
	merged

Mon Jul 23 19:33:30 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	Deleted files: GraphUtils.cc GraphUtils.hh
Thu Jul 19 18:49:33 2007	Sven Gowal (sgowal)

	* version R1-00e
	BUGS:  
	FILES: Graph.cc(29546), Graph.hh(29546), Makefile.yam(29546),
		PlannerInterfaces.h(29546)
	Modified Path_params_t

	FILES: Graph.hh(29572)
	Added a velocity variable to the node. This is the velocity along
	the lane that is associated with the car at that node.

	FILES: PlannerInterfaces.h(29622)
	Added more error flags in path planner.

	FILES: Graph.cc(29405), Graph.hh(29405)
	updated Graph.cc

	FILES: Graph.cc(29481), Graph.hh(29481)
	added the collideGrownObs and railId to the graph node structure.

Wed Jul 18 12:24:55 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: PlannerInterfaces.h(29403)
	Changed Logic_param_t

	FILES: PlannerInterfaces.h(29414)
	Modified Err_t

Mon Jul 16 20:45:13 2007	Noel duToit (ndutoit)

	* version R1-00c
	BUGS:  
	FILES: Graph.hh(29206), GraphPlanner.cc(29206),
		GraphPlanner.hh(29206), GraphPlannerGen.cc(29206)
	Moved the vehicleNode, which is the volatile node associated with
	our current position, out of the graph-planner and into the graph.

Mon Jul 16 13:59:44 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	New files: GraphUtils.cc GraphUtils.hh
	FILES: GraphPlanner.cc(29105), GraphPlanner.hh(29105)
	created GraphUtils to hold load/gen functions currently at
	GraphPlanner

	FILES: GraphPlanner.cc(29153), GraphPlanner.hh(29153),
		PlannerInterfaces.h(29153), TrajPlanner.cc(29153)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:04:51 2007	Sven Gowal (sgowal)

	* version R1-00a
	BUGS:  
	New files: Graph.cc Graph.hh GraphPath.hh GraphPlanner.cc
		GraphPlanner.hh GraphPlannerGen.cc PlannerInterfaces.h
		TrajPlanner.cc TrajPlanner.hh
	FILES: Makefile.yam(28759)
	These are the required graph planner files to generate a graph,
	plan a path, and plan the velocity. It also contains the planner
	interfaces file that define additional interfaces. This builds a
	library which is now used in the planner and planner libraries.

Tue Jul 10 18:21:16 2007	Noel duToit (ndutoit)

	* version R1-00
	Created temp-planner-interfaces module.






















