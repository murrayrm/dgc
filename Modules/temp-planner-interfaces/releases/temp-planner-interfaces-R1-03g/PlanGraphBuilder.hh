
/* 
 * Desc: Build a dense graph from an RNDF file.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_BUILDER_HH
#define PLAN_GRAPH_BUILDER_HH


/** @file

@brief Generate a graph from an RNDF file.  This class also manages
pre-compiled graph files.

*/


// Dependencies
#include "rndf/RoadGraph.hh"
#include "PlanGraph.hh"



/// @brief Generate a graph from an RNDF file.  This class also
/// manages pre-compiled graph files.
class PlanGraphBuilder
{
  public:

  /// @brief Constructor
  PlanGraphBuilder(PlanGraph *graph);

  /// @brief Destructor
  virtual ~PlanGraphBuilder();
  
  private:

  // Hide the copy constructor
  PlanGraphBuilder(const PlanGraphBuilder &that);
  
  public:

  /// @brief Set the vehicle kinematics.
  ///
  /// @param[in] wheelbase Distance between front and rear wheels (m).
  /// @param[in] maxSteer Maximum steering angle (radians).  
  int setKinematics(float wheelBase, float maxSteer);

  /// @brief Set generation options.
  ///
  /// @param[in] spacing Distance between adjacent nodes (m).
  /// @param[in] railCount Number of rails in each lane.
  /// @param[in] railSpacing Distance between adjacent rails (m).
  int setOptions(float spacing, int railCount, float railSpacing);
  
  /// @brief Build graph from RNDF file.
  ///
  /// @param[in] filename RNDF file name.  
  int build(char *rndfFilename, const char *tweaksFilename);

  /// @brief Load from pre-generated PG file.
  ///
  /// @param[in] filename PG file name.
  int load(char *filename);

  private:

  // Generate the graph
  int genGraph();

  // Generate rails in lanes.
  int genRails(int railId);
  
  // Generate rails in oncomgin lanes.
  int genOncomingRails(int railId);

  // Generate turn maneuvers through intersections
  int genTurns();

  // Generate turn manuevers for the given waypoints
  int genTurnWaypoints(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB);

  // Generate a turn maneuver between the given nodes
  int genTurnNodes(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  // Generate a nominal turn
  bool genNominalTurn(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry);

  // Generate a one-part turn
  bool genOneTurn(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry);

  // Generate a two-part wide turn; this will not connect the given
  // nodes, but will attempt to connect the rails.
  int genWideTurn(PlanGraphNode *nodeExit, PlanGraphNode *nodeEntry,
                  float exitDist, float maxTurnFactor);

  // Generate basic connectivity into zones
  int genZones();

  // Generate rail/lane change maneuvers.  Only the feasibility
  // info, not the full maneuver, is added to the graph.
  int genChanges();

  // Check for the legality of a lane/rail change
  bool checkLegal(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  // Check for feasibility of an extended maneuver.
  bool checkFeasible(pose2f_t poseA, pose2f_t poseB);

  // Check feasibility of a small maneuver.
  bool checkFeasibleStep(pose2f_t poseA, pose2f_t poseB);

  // Find the max curvature of an extended maneuver
  float checkCurvature(pose2f_t poseA, pose2f_t poseB);
  
  // Get the curvature at a step
  float checkCurvatureStep(pose2f_t poseA, pose2f_t poseB);
  
  // Insert a maneuver between the given nodes.
  int insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, PlanGraphNode::Flags flags);

  public:

  // Current graph we are building.
  PlanGraph *graph;

  // Road graph used during construction
  RoadGraph road;

  private:
  
  // Vehicle kinematic properties for feasibility checks.
  float wheelBase, maxSteer;

  // Number of rails for each lane
  int railCount;

  // Distance between rails
  float railSpacing;
  
  // Step size (node spacing) for maneuvers and feasibility checks.
  float spacing;
};

#endif

