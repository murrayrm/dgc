
/* 
 * Desc: Build instantaenous vehicle subgraphs.
 * Date: 11 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_UPDATER_HH
#define PLAN_GRAPH_UPDATER_HH


/** @file

@brief Generate a graph from an RNDF file.  This class also manages
pre-compiled graph files.

*/


// Dependencies
#include "PlanGraph.hh"


/// @brief Generate a graph from an RNDF file.  This class also
/// manages pre-compiled graph files.
class PlanGraphUpdater
{
  public:

  /// @brief Constructor
  PlanGraphUpdater(PlanGraph *graph);

  /// @brief Destructor
  virtual ~PlanGraphUpdater();
  
  private:

  // Hide the copy constructor
  PlanGraphUpdater(const PlanGraphUpdater &that);
  
  public:

  /// @brief Set the vehicle kinematics.
  ///
  /// @param[in] wheelbase Distance between front and rear wheels (m).
  /// @param[in] maxSteer Maximum steering angle (radians).  
  int setKinematics(float wheelBase, float maxSteer);

  /// @brief Build/update instantaneous maneuvers from the current
  /// vehicle pose.
  PlanGraphNode *updateVehicleSubGraph(pose2f_t pose, float steerAngle);

  private:
  
  // Check for feasibility of an extended maneuver.
  bool checkFeasibleManeuver(pose2f_t poseA, float steerAngle, pose2f_t poseB);

  // Check feasibility of a small maneuver.
  bool checkFeasibleStep(pose2f_t poseA, pose2f_t poseB);

  // Insert a maneuver between the given nodes.
  int insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB);
  
  /// Allocate a volatile node
  PlanGraphNode *allocNode(float px, float py);

  /// Free all volatile noes at once
  void freeNodes();

  private:

  // Current graph we are working on.
  PlanGraph *graph;
  
  public:
  
  // Vehicle kinematic properties for feasibility checks.
  float wheelBase, maxSteer;

  // Step size (node spacing) for maneuvers and feasibility checks.
  float spacing;

  // Size of the region-of-interest for generating manuevers.
  float roiSize;
  
  // Flat list of nodes we created (a list of pointers; the actual
  // storage is maintained by the quad-tree).  This list is used for
  // fast deletion of nodes.
  int numNodes, maxNodes;
  PlanGraphNode **nodes;
};

#endif

