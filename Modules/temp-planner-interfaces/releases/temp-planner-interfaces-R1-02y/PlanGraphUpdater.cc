
/* 
 * Desc: Build instantaenous vehicle subgraphs.
 * Date: 11 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <alice/AliceConstants.h>
//#include <dgcutils/DGCutils.hh>
#include <trajutils/maneuver.h>

#include "PlanGraphUpdater.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
PlanGraphUpdater::PlanGraphUpdater(PlanGraph *graph)
{
  this->graph = graph;
  
  this->wheelBase = VEHICLE_WHEELBASE;
  this->maxSteer  = VEHICLE_MAX_AVG_STEER;     

  this->spacing = 0.5; // MAGIC 
  this->roiSize = 20.0; // MAGIC

  this->numNodes = 0;
  this->maxNodes = 0;
  this->nodes = NULL;

  return;
}


// Destructor
PlanGraphUpdater::~PlanGraphUpdater()
{  
  if (this->nodes)
    free(this->nodes);
  this->nodes = NULL;

  return;
}


// Build/update instantaneous maneuvers from the current vehicle pose.
PlanGraphNode *PlanGraphUpdater::updateVehicleSubGraph(pose2f_t pose, float steerAngle)
{
  int i;
  vec2f_t pos;
  uint16_t include, exclude;
  PlanGraphNode *nodeA, *nodeB;
  PlanGraphNodeList nodes;
  
  // Clear existing stuff
  this->freeNodes();

  // Create vehicle node 
  nodeA = this->allocNode(pose.pos.x, pose.pos.y);
  assert(nodeA);
  nodeA->pose.rot = pose.rot;
  nodeA->steerAngle = steerAngle;
  
  // Get all nodes in the vicinity, excepting lane and rail changes.
  include = PLAN_GRAPH_NODE_ALL;
  exclude = PLAN_GRAPH_NODE_RAIL_CHANGE | PLAN_GRAPH_NODE_LANE_CHANGE;
  this->graph->getRegion(&nodes, pose.pos.x, pose.pos.y, 2*(this->roiSize), 2*(this->roiSize),
                         include, exclude);
    
  // Try out these nodes
  nodeB = NULL;
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeB = (PlanGraphNode*) nodes[i];

    // Compute end-point in frame of first node
    pos = vec2f_transform(pose2f_inv(nodeA->pose), nodeB->pose.pos);

    // Dont make go to nodes that are too near.
    if (pos.x < 2.0) // MAGIC
      continue;

    // Dont make unfeasible manuevers
    if (!this->checkFeasibleManeuver(nodeA->pose, nodeA->steerAngle, nodeB->pose))
      continue;
    
    // Looks ok; add this one 
    this->insertManeuver(nodeA, nodeB);
  }
  
  return nodeA;
}


// Check for feasibility of an extended maneuver
bool PlanGraphUpdater::checkFeasibleManeuver(pose2f_t poseA, float steerAngle, pose2f_t poseB)
{
  Vehicle *vp;
  Maneuver *mp;  
  VehicleConfiguration configA, config;
  Pose2D configB;
  pose2f_t src, dst;
  double  dm, s;
  int i, numSteps;
  float spacing;

  // Step size
  spacing = 2*(this->spacing);

  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;
  configA.phi = steerAngle;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(poseB.pos, poseA.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_config2pose(vp, &configA, &configB);
  assert(mp);

  // Check for feasibility along the entire maneuver, including the
  // final step.
  src.pos = vec2f_set(configA.x, configA.y);
  src.rot = configA.theta;
  for (i = 1; i < numSteps + 1; i++)
  {
    s = (double) i / numSteps;  
    config = maneuver_evaluate_configuration(vp, mp, s);    
    dst.pos = vec2f_set(config.x, config.y);
    dst.rot = config.theta;    
    if (!this->checkFeasibleStep(src, dst))
      break;
    src = dst;
  }

  // Clean up
  maneuver_free(mp);
  free(vp);

  // Not feasible if we did not get to the end
  if (i < numSteps + 1)
    return false;
  
  return true;
}


// Do check for unfeasible maneuvers using the vehicle turning circle.
bool PlanGraphUpdater::checkFeasibleStep(pose2f_t poseA, pose2f_t poseB)
{
  float turnRadius;
  pose2f_t pose;
  float x0, y0, dx, dy, s, t;
    
  // Vehicle turning radius
  turnRadius = this->wheelBase / tan(this->maxSteer);
  
  // Final pose in the initial frame
  pose = pose2f_mul(pose2f_inv(poseA), poseB);

  // Compute equation of line at right angles to current pose.
  // x = t dx + x0.
  // y = t dy + y0.
  x0 = pose.pos.x;
  y0 = pose.pos.y;
  dx = -sin(pose.rot);
  dy = +cos(pose.rot);

  // Trap straight-line case to avoid div-by-zero.
  if (fabs(dx) < 1e-6)
    return true;
  
  // Find intercept with x = 0; the values t and s specify the radii
  // of two turning circles.
  t = -x0 / dx;
  s = t * dy + y0;

  //MSG("check %f %f : %f %f : %f %f : %f", x0, y0, dx, dy, t, s, turnRadius);

  if (fabs(t) < turnRadius)
    return false;
  if (fabs(s) < turnRadius)
    return false;
  if (s * t < 0)
    return false;
        
  return true;
}


// Insert a maneuver between the given nodes.
int PlanGraphUpdater::insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  Vehicle *vp;
  Maneuver *mp;  
  VehicleConfiguration configA, config;
  Pose2D configB;
  double  dm, s;
  int i, numSteps;
  PlanGraphNode *src, *dst;
  float spacing;

  assert(nodeA);
  assert(nodeB);

  // Node spacing
  spacing = this->spacing;
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = nodeA->pose.rot;
  configA.phi = nodeA->steerAngle;
  
  // Final vehicle configuration
  configB.x = nodeB->pose.pos.x;
  configB.y = nodeB->pose.pos.y;
  configB.theta = nodeB->pose.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);

  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_config2pose(vp, &configA, &configB);
  assert(mp);
  
  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a  node
    dst = this->allocNode(config.x, config.y);
    assert(dst);

    // Fill out basic node data
    dst->pose.pos = vec2f_set((float) config.x, (float) config.y);
    dst->pose.rot = config.theta;
    dst->steerAngle = config.phi;
    
    // Set the interpolant
    dst->interId = i;

    // Flags
    dst->flags.isVehicle = true;
    
    // Fill out waypoint pointers
    dst->prevWaypoint = nodeA->prevWaypoint;
    dst->nextWaypoint = nodeB->nextWaypoint;
        
    // Create an arc from the previous node to the new node
    this->graph->insertArc(src, dst);

    src = dst;
  }
  dst = nodeB;

  // Create an arc to the final node
  this->graph->insertArc(src, dst);
  
  maneuver_free(mp);
  free(vp);

  return 0;
}


// Allocate a  node
PlanGraphNode *PlanGraphUpdater::allocNode(float px, float py)
{
  PlanGraphNode *node;
  
  // Allocate node in the usual fashion
  node = this->graph->allocNode(px, py);
  node->flags.isVolatile = true;

  // Make room in our list of volatile nodes
  if (this->numNodes >= this->maxNodes)
  {
    this->maxNodes += 512;
    this->nodes = (PlanGraphNode**)
      realloc(this->nodes, this->maxNodes * sizeof(PlanGraphNode*));
    assert(this->nodes);
  }

  // Add node to list
  this->nodes[this->numNodes++] = node;

  return node;
}


// Free all  noes at once
void PlanGraphUpdater::freeNodes()
{
  int i;
  PlanGraphNode *node;
  
  // Free nodes in reverse order on the unsubstantiated belief that
  // this will be faster
  for (i = this->numNodes - 1; i >= 0; i--)
  {
    node = this->nodes[i];
    this->graph->freeNode(node);
  }
  this->numNodes = 0;
  
  return;
}

