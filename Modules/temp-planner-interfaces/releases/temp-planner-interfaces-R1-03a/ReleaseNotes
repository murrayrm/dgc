              Release Notes for "temp-planner-interfaces" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "temp-planner-interfaces" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "temp-planner-interfaces" module can be found in
the ChangeLog file.

Release R1-03a (Sat Oct 20 12:18:10 2007):
	* Added ROA Override info in the Console
	* Updated updatePathStoplines. Populates distance along the path and takes into account Alice current position
	* Improved stopping at stop lines
	* Fixed possible segfault in LogClass.cc
	* Added distinct color for visualizing infeasible mans
	* Drawing the gradient based on the distance to the obstacle (for node that are not colliding)
	* Fixed turn generator that was smashing waypoint ids, and set waypointID for non-waypoints to zero
	* Added reference count for protecting paths
	* Changed magic number
	* Added test for points in ROI
	* Added the obsDistTime and carDistTime fields to the PlanGraphNode
	* Somewhat improved turn generation
	* Tweaks to off-road graph
	* Modified off-road arc generation
	* Added the flag OFFROAD
	* Added PASS_REV flag which allows rail planner to pass and reverse
	* changed parameter for s1planner (nothing else effected)
	* Add readConfig to logic planner params
	* Removed warning
	* Fixed bug in updatePathStopline
	* uncommenting the line which allows the displayTraj function to actually, well, display a traj.
	* improved updatePathToStoplines and fixed core dump
	* Fixed another bug in updatePathStoplines ...
	* fixed stupid bug
	* Added options for the fused perceptor

Release R1-03 (Tue Oct 16 21:27:02 2007):
	* Implemented cmd args for sparse and planner config file.
	* Fixed waypoint function to get nearest
	* Removed unneeded ROI fields
	* Added off-road flag
	* restrict lane changes to 1 lane only in the case where we are
	changing into oncoming lanes. Increased the maxSide dist, and
	create a change man every 8 m.
	* changed PlanGraphPath and added stop line information * new
	getDistToStopline function
	* new structure due to row intersections
	* Added support for off-road driving
	* Added generation for reverse maneuvers
	* fixed bug in getDistanceToStopline
	* Now handles row intersections and places information into path structure
	* removed debug information

Release R1-02z (Mon Oct 15 23:33:21 2007):
	* implemented cmd args for using fused perceptor, visualization 
level
	* added stopped duration to console
	* Added parking man completion for internal goal complete 
checking
	* generate stopnodes close to stopline.
	* modified getstopline fcn to use new graph structure.

Release R1-02y (Mon Oct 15  7:38:58 2007):
	Major release for new rail planner implementation. See changelog for details.

Release R1-02x (Thu Oct 11  0:27:36 2007):
  Major release, with PlanGraph now fully integrated.

Release R1-02w (Tue Oct  9 16:26:54 2007):
  Added new PlanGraph class to replace the existing Graph class.  Also
  added a utility for building plan graphs from RNDFs and saving to a file.

Release R1-02v (Sat Oct  6 16:45:20 2007):
	* Modified output from intersection, setprecision = 2 works correct now
	* Removed the old graph-planner code. added some display functions to assist in debugging the graph.
	* added more display fcns to debug the graph.
	* displayGraph also displays the volatile nodes
	* More output to log files while debugging
notes entry here. If none, then please delete these lines *>

Release R1-02u (Sat Oct  6 14:03:48 2007):
changed map obstacle access function in Utils.cc to work if map
	returns elements in site frame or in local frame.

Release R1-02t (Fri Oct  5  9:16:36 2007):
	changed prediction output in console

Release R1-02s (Wed Oct  3 18:37:01 2007):
	Added cmdline option --no-failure-handling

Release R1-02r (Wed Oct  3 16:43:20 2007):
	Outputs FailureHandler index instead of the FailureHandler counter to the console

Release R1-02q (Tue Oct  2 15:40:40 2007):
	Modified AliceStateHelper to use site frame coordinates
	Quadtree now uses pose_x, pose_y

Release R1-02p (Tue Oct  2 11:57:54 2007):
	Moved display and print functions to Utils.

Release R1-02o (Tue Oct  2  8:09:47 2007):
	SAFETYFLAG and CABMODE no longer displayed on intersection page. Added new message field to intersection page.

Release R1-02n (Tue Oct  2  0:40:20 2007):
  * Minor name-change on the graph for compatability with the site frame.

Release R1-02m (Sun Sep 30 23:29:49 2007):
  * Added in a couple of command line options from Noel's release (hacked-uturn, hacked-backup).

Release R1-02l (Sun Sep 30 21:58:07 2007):
  * Moved command line options to this module; suggest we deprecate CmdArgs.
  * The global-graph offset now lives int Graph::pos.

Release R1-02k (Sun Sep 30 18:46:32 2007):
	Added options to the list of errors for s1planner to report. these options are start and end blocked (by an obstacle) and
	cost too high (to be used when we fail to zone and don't want to go off of the road). also in this file
	is a #define statement setting what cost will be considered too high and will trigger the cost too high error to be returned.

Release R1-02j (Sun Sep 30 13:53:36 2007):
	Added use-hacked-uturn and --use-hacked-backup flags. These are off by default, meaning that these maneuvers
	are handled by s1planner by default. s1planner needs more debugging though, so for now these flags should be 
	given.

Release R1-02i (Sat Sep 29 17:51:30 2007):
	Added fault handling information to planner console.  The "FH" field indicates if fault 
handling was enabled.  The "Failures" field indicates the number of faults that have occured.

Release R1-02h (Sat Sep 29 12:17:35 2007):
	Fix from Sven: in getCurrentLane function, check that the node is not NULL so we don't segfault. If the node is NULL, return laneId = 0, segmentId = 0.

Release R1-02g (Sat Sep 29 10:56:39 2007):
	Specify the type of failure to mplanner.

Release R1-02f (Fri Sep 28 16:07:47 2007):
	Added functionality to request reread of path-planner parameters from file (by hitting 'u')

Release R1-02e (Fri Sep 28 16:00:05 2007):
	monitorAliceSpeed takes into account when Alice is in Estop pause/disabled

Release R1-02d (Thu Sep 27 16:51:22 2007):
	Changed the way the average cycle time gets computed.

Release R1-02c (Thu Sep 27 10:12:05 2007):
	Made getNodeFromRndfId and getCurrentLane faster.
	Reduced the GraphNode size further to 254 bytes.

Release R1-02b (Wed Sep 26 21:47:38 2007):
	Changed logicParams to accept parameters sent from logic-planner to planner; fault handling needs that
	Also added output from intersection to console that got removed from the console due to more important information on
	the first page of the console

Release R1-02a (Wed Sep 26 20:49:19 2007):
	Implemented cmdline args to specify updating the lane information (via cost) on the graph (--update-lanes) and 
	to update stopline locations (--update-stoplines).

Release R1-02 (Wed Sep 26 10:37:46 2007):
	Reduced GraphNode size from 444 bytes to 262 bytes, and GraphArc size from 40 bytes to 36 bytes.

Release R1-01z (Tue Sep 25 11:01:35 2007):
	Added the show_costmap field to cmd args. When given, the cost map is sent to the map viewer.

Release R1-01y (Sun Sep 23 15:18:17 2007):
	Added the use-s1planner and use-circle-planner arguments.

Release R1-01x (Fri Sep 21 16:04:03 2007):
	Added region, planner and obstacle field to state problem to fascilitate fault handling.
	Added errors to the list.

Release R1-01w (Fri Sep 21  5:02:52 2007):
	Added value to CmdArgs for command line option to enable use of
	RNDF frame --use-rndf-frame.

Release R1-01v (Tue Sep 18 18:58:22 2007):
	Outputs information about merging into console

Release R1-01u (Mon Sep 17 20:52:43 2007):
	Fixed output bug in intersection console and outputs the time that Alice waits at the intersection.

Release R1-01t (Fri Sep 14  5:55:55 2007):
	Uses MapElement::isObstacle() and MapElement::isVehicle() now

Release R1-01s (Thu Sep 13 17:35:33 2007):
	Output information about planner's map to console on third page 
called "debug"

Release R1-01r (Tue Sep 11 21:38:04 2007):
	Fixed bug in Console that caused seg faults in planner at certain intersections

Release R1-01q (Tue Sep 11 13:42:28 2007):
	Fixed minor bug to improve the console

Release R1-01p (Tue Sep 11 13:07:26 2007):
	Added new page in the console. This page contains information about obstacles that affect the Intersection Handling.

Release R1-01o (Tue Sep  4 20:24:45 2007):
	Added use-dplanner option to CmdArgs. 

Release R1-01n (Tue Sep  4  4:15:59 2007):
	Added mapper_line_fusion_compat option to CmdArgs.

Release R1-01m (Sun Sep  2 17:27:39 2007):
	Propagating mapper/map changes

Release R1-01l (Sun Sep  2 11:02:26 2007):
	in getDistToStop: if no correspondig SegGoal was found to the stopline, it searches along the path to 
find a matching entryWaypoint and exitWaypoint. The INTERSECTION_TYPE will be fake (only important for setting 
correct turn signals)

Release R1-01k (Thu Aug 30 17:45:42 2007):
	Added a function in Utils that calculates the distance along a path to a point that is projected onto the path.

Release R1-01j (Thu Aug 30 17:11:34 2007):
	Outputs the distance to stopline in the planner console and 
	prediction got its own frame back. The new planner window will 
	be sligthly bigger

Release R1-01i (Thu Aug 30 14:13:47 2007):
	Optimized getDistToStopline

Release R1-01h (Wed Aug 29 19:02:17 2007):
	Added an argument to reverseProjection that specifies the distance to extend the path if the point can not 
projected on the path. 

Release R1-01g (Wed Aug 29 11:20:23 2007):
	Moved reverseProjection to Utils. 

Release R1-01f (Wed Aug 29  2:40:44 2007):
	Added the PathDirection to the nodes.
	Added a type field to the Graph to specify a type of uturn.

Release R1-01e (Tue Aug 28 18:53:21 2007):
	in getDistToStop, do not check for distance to segGoals anymore. The interaction with the graph did not work properly.

Release R1-01d (Tue Aug 28 12:14:43 2007):
	Updated the distToStopLine function. It compares the SegGoals vs. the current path. The closer stopline will be 
	returned.

Release R1-01c (Tue Aug 28  9:29:28 2007):
	fixing argument (int -> bool)

Release R1-01b (Mon Aug 27 16:27:30 2007):
	adding option to run groundstrike filtering in internal map

Release R1-01a (Mon Aug 27 14:34:46 2007):
	Removed unused fields in GraphNode

Release R1-01 (Mon Aug 27  9:21:33 2007):
	Added files that were not added in prev release. 

Release R1-00z (Mon Aug 27  8:56:16 2007):
	Created a Utils file for general utility function for the planning stack.  These functions were in logic-planner LogicUtils previously. 
Release R1-00y (Tue Aug 21  6:00:15 2007):
	Changed the name of the Graph class to Graph_t.  The class name had
	been typedefed to Graph_t already so most interfaces won't change. 
	This was necessary to have the option to run mapper inside planner
	because of a naming conflict.

Release R1-00x (Mon Aug 20 21:25:40 2007):
	Fixed minor bug in Console. Now it looks nice again

Release R1-00w (Mon Aug 20 19:22:16 2007):
	Added distToStop field in node (to prepare to stopline updates)

Release R1-00v (Wed Aug 15 17:44:09 2007):
	Added enable-line-fusion in CmdArgs

Release R1-00u (Tue Aug 14 18:17:22 2007):
	Switched over to the cspecs interface to the zone planners.

Release R1-00t (Tue Aug 14 10:52:23 2007):
	Added multiple rail management

Release R1-00s (Tue Aug 14 12:04:52 2007):
	Added functionality to build a graph for zones.	
	Added fields the path-params for zone handling.
	Implemented a temporary interface for the zone corridor specification.

Release R1-00r (Tue Aug 14  9:21:59 2007):
	Adjusted Makefile to work with new console

Release R1-00q (Wed Aug  8 15:29:16 2007):
	Added VP_BACKUP_FINISHED to handle completion of the backing up maneuver

Release R1-00p (Wed Aug  8 11:15:25 2007):
	Added segment goal queue to logicParam

Release R1-00o (Mon Aug  6 20:25:08 2007):
	Trying to make unloading of segment work

Release R1-00n (Sat Aug  4  3:32:24 2007):
	Added acceleration and curvature fields to GraphPath
	Defined Vehicle_params_t

Release R1-00m (Fri Aug  3 20:13:34 2007):
	Added node type for lane changes with broken white line
	Added --update-from-map in CmdArgs

Release R1-00l (Fri Aug  3  0:15:25 2007):
	Added --step-by-step-loading in CmdArgs
	Added --load-graph-files in CmdArgs
	Now only using the quadtree for spacial indexing

Release R1-00k (Tue Jul 31 19:34:46 2007):
	Overloaded getNearestNode function
        Modifying getNearestNode function
        Added getNext/PrevChangeNode

Release R1-00j (Tue Jul 31 15:07:03 2007):
	Increased size of graph to enable loading larger RNDFs

Release R1-00i (Tue Jul 31 13:32:17 2007):
	Modified CmdArgs structure and added command line argument noprediction.

Release R1-00h (Tue Jul 31 11:17:17 2007):
	Changed the Console to add output from the prediction module. It shows the status whether any possible collision was predicted
	and how many obstacles are currently predicted.

Release R1-00g (Fri Jul 27 16:26:20 2007):
	Added the following errors to the planner interfaces: path 
planner cannot find node on path when planning using the previous 
path.
	Added flag to tell if logic planner completed the uturn.
	Moved common planner files from planner to the interfaces.
	Changed arcs to allow multi-resolution graph searches.

Release R1-00f (Mon Jul 23 19:33:32 2007):
	Deleted unused files

Release R1-00e (Wed Jul 18 16:19:39 2007):
	Added railId and collideGrownObs into the graph nodes in the graph files. 
	The idea behind the nodes is that you can have multiple rails associated
	with each lane. For now the rails are initialized to 1.

Release R1-00d (Wed Jul 18 12:24:58 2007):
	Modified the Err_t typedef (this should not affect the planner libraries)
	Added the current SegGoals to the Logic_param_t structure

Release R1-00c (Mon Jul 16 20:45:19 2007):
	Moved the vehicle nodes from the graph planner to the graph.

Release R1-00b (Mon Jul 16 13:59:51 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:04:55 2007):
	Added basic functionality in order to start coding

Release R1-00 (Tue Jul 10 18:21:16 2007):
	Created.





































































































