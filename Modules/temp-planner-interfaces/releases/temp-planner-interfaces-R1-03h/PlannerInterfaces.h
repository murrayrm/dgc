/**********************************************************
 **
 **  PLANNERINTERFACES.H
 **
 **    Author: Noel du Toit
 **    Created: Tue Jul 10 08:38:32 2007
 **
 **********************************************************
 ** The interfaces between the executable planner module, 
 ** planner, and its libraries (GraphUpdater, PathPlanner, 
 ** LogicPlanner, VelPlanner) are defined here.
 **********************************************************/

#ifndef PLANNERINTERFACES_H
#define PLANNERINTERFACES_H

#include "PlanGraph.hh"
#include "PlanGraphPath.hh"
#include <queue>

#include <cspecs/CSpecs.hh>
#include <bitmap/Polygon.hh>
#include <bitmap/BitmapParams.hh>
#include <gcinterfaces/SegGoals.hh>
#include <map/Map.hh>

using namespace std;


/******************************************************************
* State problem enum definitions and a way to make them printable
* For states used in the finit state machine(s)
*******************************************************************/
// Convert command to string, helping macro.
#define MAP_TO_STRING(A) #A,

/********** Define State Enum **********/ 
#define FOR_ALL_STATES(STATE)\
	STATE(DRIVE) STATE(STOP_INT) STATE(STOP_OBS) STATE(BACKUP) STATE(UTURN) STATE(PAUSE)
#define DECLARE_STATES(A) A,
typedef enum  {
	FOR_ALL_STATES(DECLARE_STATES) STATES_COUNT
} FSM_state_t;
const char *const stateString[STATES_COUNT] = {
	FOR_ALL_STATES(MAP_TO_STRING)
};

/********** Define Flag Enum **********/
// Flags that guide the update of the graph
#define FOR_ALL_FLAGS(FLAG) FLAG(PASS) FLAG(NO_PASS) FLAG(OFFROAD) FLAG(PASS_REV)
#define DECLARE_FLAGS(A) A,
typedef enum  {
	FOR_ALL_FLAGS(DECLARE_FLAGS) FLAGS_COUNT
} FSM_flag_t;
//#define MAP_FLAGS_TO_STRING(A) #A,
const char *const flagString[FLAGS_COUNT] = {
	FOR_ALL_FLAGS(MAP_TO_STRING)
};

/********** Define Region Enum **********/
// Regions that we need to be able to plan through
#define FOR_ALL_REGIONS(REGION) REGION(ZONE_REGION) REGION(ROAD_REGION) REGION(INTERSECTION) 
#define DECLARE_REGIONS(A) A,
typedef enum  {
	FOR_ALL_REGIONS(DECLARE_REGIONS) REGIONS_COUNT
} FSM_region_t;
//#define MAP_REGIONS_TO_STRING(A) #A,
const char *const regionString[REGIONS_COUNT] = {
	FOR_ALL_REGIONS(MAP_TO_STRING)
};

/********** Define Planner Enum **********/
// Planners that we want to use
#define FOR_ALL_PLANNERS(PLANNER)\
	PLANNER(RAIL_PLANNER) PLANNER(S1PLANNER) PLANNER(DPLANNER)\
	PLANNER(CIRCLE_PLANNER) PLANNER(RRT_PLANNER) 
#define DECLARE_PLANNERS(A) A,
typedef enum  {
	FOR_ALL_PLANNERS(DECLARE_PLANNERS) PLANNERS_COUNT
} FSM_planner_t;
//#define MAP_PLANNERS_TO_STRING(A) #A,
const char *const plannerString[PLANNERS_COUNT] = {
	FOR_ALL_PLANNERS(MAP_TO_STRING)
};

/********** Define Obstacle Enum **********/
/// How do we deal with obstacles? Can be safe, aggressive, and not safe (extreme)
#define FOR_ALL_OBSTACLES(OBSTACLE)\
	OBSTACLE(OBSTACLE_SAFETY) OBSTACLE(OBSTACLE_AGGRESSIVE) OBSTACLE(OBSTACLE_BARE) 
#define DECLARE_OBSTACLES(A) A,
typedef enum  {
	FOR_ALL_OBSTACLES(DECLARE_OBSTACLES) OBSTACLES_COUNT
} FSM_obstacle_t;
//#define MAP_OBSTACLES_TO_STRING(A) #A,
const char *const obstacleString[OBSTACLES_COUNT] = {
	FOR_ALL_OBSTACLES(MAP_TO_STRING)
};

/********** Define State Problem Structure **********/
/// Defines the state problem that is returned by the logic planner
typedef struct {
  FSM_state_t state;
  double probability;
  FSM_flag_t flag;
  FSM_region_t region;
  FSM_planner_t planner;
  FSM_obstacle_t obstacle;
} StateProblem_t;

// ERRORS
#define LP_OK                   0
#define GU_OK                   0
#define PP_OK                   0
#define VP_OK                   0
#define PLANNER_OK              0
#define S1PLANNER_OK            0
#define CIRCLE_PLANNER_OK       0
#define LP_MAP_INCOMPLETE       1
#define LP_FAIL_MPLANNER_LANE_BLOCKED        2
#define GU_MAP_INCOMPLETE       4
#define PP_COLLISION_SAFETY     8
#define PP_NOPATH_LEN          16
#define PP_NOPATH_COST         32
#define VP_UTURN_FINISHED      64
#define VP_BACKUP_FINISHED    128
#define PP_NONODEFOUND        256
#define P_EXTRACT_SUBPATH_ERROR   512
#define S1PLANNER_FAILED          1024
#define CIRCLE_PLANNER_FAILED     2048
#define GU_UPDATE_FROM_MAP_ERROR  4096
#define LP_FAIL_MPLANNER_ROAD_BLOCKED 8192
#define LP_FAIL_MPLANNER_LOST     16384
#define S1PLANNER_START_BLOCKED		32768
#define S1PLANNER_END_BLOCKED		  65536
#define S1PLANNER_COST_TOO_HIGH		131072
#define VP_PARKING_SPOT_FINISHED  262144
#define PP_COLLISION_AGGRESSIVE   524288
#define PP_COLLISION_BARE         1048576
#define S1PLANNER_PATH_INCOMPLETE	2097152

// the value of the cost which will result in a
// S1PLANNER_COST_TOO_HIGH error getting reported
#define S1PLANNER_HIGH_COST_VALUE	500

typedef uint32_t Err_t;

// PATH
// REMOVE typedef struct GraphPath Path_t;

// COST
typedef int Cost_t;

// VEL PLANNER PARAMS
typedef struct {
  double minSpeed;
  double maxSpeed;
} Vel_params_t;

// PATH PLANNER PARAMS
typedef struct {
  FSM_flag_t flag;
  bool planFromCurrPos;
  int zoneId;
  int spotId; // 0 is perimeter, >0 is spot numbers
  int exitId;
  double velMin;
  double velMax;
  bool readPathPlanParams;
} Path_params_t;

// LOGIC PLANNER PARAMS
typedef struct {
  SegGoals::SegmentType segment_type;
  SegGoals seg_goal;
  deque<SegGoals>* seg_goal_queue;
  PlanGraph* m_graph;
  PlanGraphPath* m_path;
  bool planFromCurrPos;
  int m_estop;
  bool readConfig;
  bool evasion;
  vector<PointLabel> mergingWaypoints;
  int transpos;
} Logic_params_t;

typedef struct {
  PointLabel waypoint;
  MapElement element;
  double velocity;
  double distance;
  double eta;
  LaneLabel lane;
  bool precedence;
  bool closest;
  bool checkedQueuing;
  bool updated;
  unsigned long long lastUpdated;
} precedenceList_t;

// PREDICTION INTERFACE
typedef int Prediction_t;

typedef CSpecs CSpecs_t;


#endif
