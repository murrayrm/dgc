
/* 
 * Desc: Build instantaenous vehicle subgraphs.
 * Date: 11 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_UPDATER_HH
#define PLAN_GRAPH_UPDATER_HH


/** @file

@brief Generate a graph from an RNDF file.  This class also manages
pre-compiled graph files.

*/


// Dependencies
#include <trajutils/maneuver.h>
#include "PlanGraph.hh"


/// @brief Generate a graph from an RNDF file.  This class also
/// manages pre-compiled graph files.
class PlanGraphUpdater
{
  public:

  /// @brief Constructor
  PlanGraphUpdater(PlanGraph *graph);

  /// @brief Destructor
  virtual ~PlanGraphUpdater();
  
  private:

  // Hide the copy constructor
  PlanGraphUpdater(const PlanGraphUpdater &that);
  
  public:

  /// @brief Set the vehicle kinematics.
  ///
  /// @param[in] wheelbase Distance between front and rear wheels (m).
  /// @param[in] maxSteer Maximum steering angle (radians).  
  int setKinematics(float wheelBase, float maxSteer);
  
  /// @brief Set the modes for graph generation
  int setMode(bool allowOncoming, bool allowReverse, bool allowOffRoad);
  
  /// @brief Build/update instantaneous maneuvers from the current
  /// vehicle pose.
  PlanGraphNode *update(pose2f_t pose, float steerAngle, PlanGraphNode *goalNode = NULL);

  private:

  // Generate maneuvers that bring us back to the road
  int genOnRoad(PlanGraphNode *nodeA);

  // Generate maneuvers that take us cross country to the goal
  int genOffRoad(PlanGraphNode *nodeA, PlanGraphNode *nodeC);

  // Check for feasibility of an extended maneuver.
  bool isFeasible(Maneuver *mp, pose2f_t poseA, float steerA, pose2f_t poseB);

  // Check feasibility of a single step.
  bool isFeasibleStep(pose2f_t poseA, pose2f_t poseB);

  // Insert a maneuver between the given nodes.
  int insertManeuver(Maneuver *mp, PlanGraphNode *nodeA, PlanGraphNode *nodeB, bool isOffRoad);

  // Create a maneuver
  Maneuver *allocManeuver(pose2f_t poseA, float steerA, pose2f_t poseB);

  // Free a maneuver
  void freeManeuver(Maneuver *mp);

  /// Allocate a volatile node
  PlanGraphNode *allocNode(float px, float py);

  /// Free all volatile noes at once
  void freeNodes();

  private:

  // Current graph we are working on.
  PlanGraph *graph;
  
  public:
  
  // Vehicle kinematic properties for feasibility checks.
  float wheelBase, maxSteer;

  // Step size (node spacing) for maneuvers and feasibility checks.
  float spacing;

  // Size of the region-of-interest for generating manuevers.
  float roiSize;

  // Dimensions of the region for off-road planner
  float frontDist, frontWidth, frontSpacing;
  float rearDist, rearWidth, rearSpacing;

  // Operating mode
  bool allowOncoming, allowReverse, allowOffRoad;
  
  // Vehicle properties description, for maneuver generation
  Vehicle *vp;
  
  // Flat list of nodes we created (a list of pointers; the actual
  // storage is maintained by the quad-tree).  This list is used for
  // fast deletion of nodes.
  int numNodes, maxNodes;
  PlanGraphNode **nodes;
};

#endif

