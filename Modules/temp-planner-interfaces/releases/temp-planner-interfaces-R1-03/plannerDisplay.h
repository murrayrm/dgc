/*
 * plannerDisplay.dd - SparrowHawk display for planner
 *
 * Christian Looman
 * 09 August 2007
 *
 */

extern long long sparrowhawk;

#define D(x)    (sparrowhawk)



/* Object ID's (offset into table) */


#ifdef __cplusplus
extern "C" {
#endif

/* Allocate space for buffer storage */
static char plannerbuf[104];

static DD_IDENT plannertable[] = {
{1, 1, (void *)("Planner runs at"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 24, (void *)("("), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 31, (void *)(")"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("+- (S)tate Machine ------------------+- Alice State ------------------------+"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("| FSM State:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 38, (void *)("| Position:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("| FSM Flag:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 38, (void *)("| Velocity:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 1, (void *)("| FSM Planner:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 38, (void *)("| Turning:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 1, (void *)("| FSM Region:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 38, (void *)("| Last Wpt:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("| FSM Obstacle:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 38, (void *)("| Next Wpt:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 1, (void *)("| FH Enabled:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 38, (void *)("| Stopped:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 1, (void *)("| Fail index:"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 1, (void *)("| Hit (U) to reload params from file +- (I)ntersections and (P)rediction ---+"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 1, (void *)("+- Trajectory -----------------------+ Prediction Status    :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 11, (void *)("Initital     Final"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 10, (void *)("X"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 38, (void *)("| Start Waypoint"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 61, (void *)(":"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 10, (void *)("Y"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 38, (void *)("| Distance to Stopline :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{18, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 1, (void *)("| Velocity"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 38, (void *)("| Intersection Status  :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{19, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 38, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{20, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{21, 1, (void *)("+- Messages -------------------------+--------------------------------------+"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{22, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{22, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{23, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{23, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{24, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{24, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{25, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{25, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{26, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{26, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{27, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{27, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{28, 1, (void *)("+---------------------------------------------------------------------------+"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{1, 17, (void *)(&(D(rate1))), dd_double, "%5.2f", plannerbuf+0, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(rate1)", -1},
{1, 25, (void *)(&(D(rate2))), dd_double, "%5.2f", plannerbuf+8, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(rate2)", -1},
{4, 17, (void *)(D(FSM_state)), dd_string, "%s(10)", (char *)(sizeof(D(FSM_state))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(FSM_state)", -1},
{5, 17, (void *)(D(FSM_flag)), dd_string, "%10s", (char *)(sizeof(D(FSM_flag))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(FSM_flag)", -1},
{6, 17, (void *)(D(FSM_planner)), dd_string, "%10s", (char *)(sizeof(D(FSM_planner))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(FSM_planner)", -1},
{7, 17, (void *)(D(FSM_region)), dd_string, "%10s", (char *)(sizeof(D(FSM_region))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(FSM_region)", -1},
{8, 17, (void *)(D(FSM_obstacle)), dd_string, "%10s", (char *)(sizeof(D(FSM_obstacle))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(FSM_obstacle)", -1},
{9, 17, (void *)(&(D(FAIL_on))), dd_short, "%i", plannerbuf+16, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(FAIL_on)", -1},
{10, 17, (void *)(&(D(FAIL_count))), dd_short, "%i", plannerbuf+24, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(FAIL_count)", -1},
{17, 12, (void *)(&(D(traj_x))), dd_double, "%5.2f", plannerbuf+32, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(traj_x)", -1},
{18, 12, (void *)(&(D(traj_y))), dd_double, "%5.2f", plannerbuf+40, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(traj_y)", -1},
{19, 12, (void *)(&(D(traj_vel))), dd_double, "%5.2f", plannerbuf+48, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(traj_vel)", -1},
{17, 24, (void *)(&(D(traj_f_x))), dd_double, "%5.2f", plannerbuf+56, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(traj_f_x)", -1},
{18, 24, (void *)(&(D(traj_f_y))), dd_double, "%5.2f", plannerbuf+64, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(traj_f_y)", -1},
{19, 24, (void *)(&(D(traj_f_vel))), dd_double, "%5.2f", plannerbuf+72, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(traj_f_vel)", -1},
{14, 63, (void *)(D(pred_status)), dd_string, "%s", (char *)(sizeof(D(pred_status))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(pred_status)", -1},
{4, 54, (void *)(D(state_position)), dd_string, "%s", (char *)(sizeof(D(state_position))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(state_position)", -1},
{5, 54, (void *)(&(D(state_velocity))), dd_double, "%5.2f", plannerbuf+80, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(state_velocity)", -1},
{6, 54, (void *)(D(state_turning)), dd_string, "%10s5", (char *)(sizeof(D(state_turning))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(state_turning)", -1},
{7, 54, (void *)(D(state_last_wpt)), dd_string, "%10s", (char *)(sizeof(D(state_last_wpt))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(state_last_wpt)", -1},
{8, 54, (void *)(D(state_next_wpt)), dd_string, "%10s", (char *)(sizeof(D(state_next_wpt))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(state_next_wpt)", -1},
{9, 54, (void *)(&(D(stopped))), dd_double, "%5.2f", plannerbuf+88, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(stopped)", -1},
{17, 63, (void *)(D(inter_wp)), dd_string, "%10s", (char *)(sizeof(D(inter_wp))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(inter_wp)", -1},
{19, 63, (void *)(D(inter_status)), dd_string, "%10s", (char *)(sizeof(D(inter_status))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(inter_status)", -1},
{18, 63, (void *)(&(D(dist_stop))), dd_double, "%5.2f", plannerbuf+96, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(dist_stop)", -1},
{22, 3, (void *)(D(message1)), dd_string, "%70s", (char *)(sizeof(D(message1))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(message1)", -1},
{23, 3, (void *)(D(message2)), dd_string, "%70s", (char *)(sizeof(D(message2))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(message2)", -1},
{24, 3, (void *)(D(message3)), dd_string, "%70s", (char *)(sizeof(D(message3))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(message3)", -1},
{25, 3, (void *)(D(message4)), dd_string, "%70s", (char *)(sizeof(D(message4))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(message4)", -1},
{26, 3, (void *)(D(message5)), dd_string, "%70s", (char *)(sizeof(D(message5))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(message5)", -1},
{27, 3, (void *)(D(message6)), dd_string, "%70s", (char *)(sizeof(D(message6))), 1, dd_nilcbk, (long)(long) 0, 0, 0, String, "D(message6)", -1},
DD_End};

#ifdef __cplusplus
}
#endif

