
/* 
 * Desc: Build instantaenous vehicle subgraphs.
 * Date: 11 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <alice/AliceConstants.h>
//#include <dgcutils/DGCutils.hh>


#include "PlanGraphUpdater.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
PlanGraphUpdater::PlanGraphUpdater(PlanGraph *graph)
{
  this->graph = graph;
  
  this->wheelBase = VEHICLE_WHEELBASE;
  this->maxSteer  = VEHICLE_MAX_AVG_STEER;     

  this->spacing = 1.0; // MAGIC 
  this->roiSize = 40.0; // MAGIC

  this->frontDist = 25;
  this->frontWidth = 15;
  this->frontSpacing = 1.0;
  
  this->rearDist = 20;
  this->rearWidth = 15;
  this->rearSpacing = 1.0;
      
  this->numNodes = 0;
  this->maxNodes = 0;
  this->nodes = NULL;

  // Vehicle properties
  this->vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);

  return;
}


// Destructor
PlanGraphUpdater::~PlanGraphUpdater()
{
  free(this->vp);
  
  if (this->nodes)
    free(this->nodes);
  this->nodes = NULL;

  return;
}


// Set the vehicle kinematics.
int PlanGraphUpdater::setKinematics(float wheelBase, float maxSteer)
{
  this->wheelBase = wheelBase;
  this->maxSteer = maxSteer;

  // Re-generate vehicle properties
  assert(this->vp);
  free(this->vp);
  this->vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  return 0;
}


// Set the modes for graph generation
int PlanGraphUpdater::setMode(bool allowOncoming, bool allowReverse, bool allowOffRoad)
{
  this->allowOncoming = allowOncoming;
  this->allowReverse = allowReverse;
  this->allowOffRoad = allowOffRoad;
  
  return 0;
}


// Build/update instantaneous maneuvers from the current vehicle pose.
PlanGraphNode *PlanGraphUpdater::update(pose2f_t pose, float steerAngle, PlanGraphNode *goalNode)
{
  PlanGraphNode *nodeA;
  
  // Clear existing stuff
  this->freeNodes();

  // Create vehicle node 
  nodeA = this->allocNode(pose.pos.x, pose.pos.y);
  assert(nodeA);
  nodeA->pose.rot = pose.rot;
  nodeA->steerAngle = steerAngle;

  // Generate maneuvers that join us back onto the graph
  if (true)
    this->genOnRoad(nodeA);

  // Generate maneuvers that join us directly to the goal
  if (this->allowOffRoad && goalNode)
    this->genOffRoad(nodeA, goalNode);
  
  return nodeA;
}


// Generate maneuvers that bring us back to the road
int PlanGraphUpdater::genOnRoad(PlanGraphNode *nodeA)
{
  int i;
  vec2f_t pos;
  uint16_t include, exclude;
  PlanGraphNode *nodeB;
  PlanGraphNodeList nodes;
  Maneuver *mp;
  
  // Get all nodes in the vicinity, excepting lane and rail changes.
  include = PLAN_GRAPH_NODE_ALL;
  exclude = PLAN_GRAPH_NODE_RAIL_CHANGE | PLAN_GRAPH_NODE_LANE_CHANGE;
  if (!this->allowOncoming)
    exclude |= PLAN_GRAPH_NODE_ONCOMING;  
  this->graph->getRegion(&nodes, nodeA->pose.pos.x, nodeA->pose.pos.y,
                         this->roiSize, this->roiSize, include, exclude);
    
  // Try out forward maneuvers to these nodes
  nodeB = NULL;
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeB = (PlanGraphNode*) nodes[i];

    // Compute end-point in frame of first node
    pos = vec2f_transform(pose2f_inv(nodeA->pose), nodeB->pose.pos);

    // Dont make go to nodes that are too near.
    if (pos.x < 2.0) // MAGIC
      continue;

    // Create the maneuver
    mp = this->allocManeuver(nodeA->pose, nodeA->steerAngle, nodeB->pose);
    
    // Dont make unfeasible maneuvers
    if (this->isFeasible(mp, nodeA->pose, nodeA->steerAngle, nodeB->pose))
    {
      // Looks ok; add this one 
      this->insertManeuver(mp, nodeA, nodeB, false);
    }

    // Clean up
    this->freeManeuver(mp);
  }

  // If there is no reversing allowed, we are done.
  if (!this->allowReverse)
    return 0;

  // Try out reversing maneuvers to these nodes
  nodeB = NULL;
  for (i = 0; i < (int) nodes.size(); i++)
  {
    nodeB = (PlanGraphNode*) nodes[i];

    // When driving backwards, restrict ourselves somewhat.  This
    // probably needs a better check to make sure we dont reverse
    // through an intersection.
    if (nodeB->flags.isTurn)
      continue;
    if (nodeB->flags.isOncoming)
      continue;
    
    // Compute end-point in frame of first node
    pos = vec2f_transform(pose2f_inv(nodeB->pose), nodeA->pose.pos);

    // Dont make go to nodes that are too near.
    if (pos.x < 2.0) // MAGIC
      continue;

    // Create the maneuver
    mp = this->allocManeuver(nodeB->pose, 0, nodeA->pose); // TODO nodeA->steerAngle
    
    // Dont make unfeasible maneuvers
    if (this->isFeasible(mp, nodeB->pose, 0, nodeA->pose)) // TODO nodeA->steerAngle
    {
      // Looks ok; add this one 
      this->insertManeuver(mp, nodeB, nodeA, false);
    }

    // Clean up
    this->freeManeuver(mp);
  }

  return 0;
}


// Generate maneuvers that take us cross country to the goal
int PlanGraphUpdater::genOffRoad(PlanGraphNode *nodeA, PlanGraphNode *nodeC)
{
  pose2f_t poseA, poseB, poseC;
  float steerAngle;
  PlanGraphNode *nodeB;
  float sx, sy, spacing;
  int i, n;
  Maneuver *mpA, *mpB;
  
  // MAGIC
  spacing = 0.5;

  steerAngle = nodeA->steerAngle;
  poseA = nodeA->pose;
  poseC = nodeC->pose;
      
  // Generate a solution that goes straight to the goal
  mpA = this->allocManeuver(poseA, steerAngle, poseC);
  if (this->isFeasible(mpA, poseA, steerAngle, poseC))
    this->insertManeuver(mpA, nodeA, nodeC, true);
  this->freeManeuver(mpA);

  // Control parameters for the front family of solutions
  sx = this->frontDist;
  sy = this->frontWidth/2;
  n = (int) ceil(sy / this->frontSpacing);
  spacing = sy / n;

  // Generate a family of solutions in front of the vehicle, then link them
  // up to the goal.
  for (i = -n; i <= +n; i++)
  {
    // Pick a target pose in the vehicle frame
    poseB.pos.x = sx;
    poseB.pos.y = i * spacing;
    poseB.rot = 0;

    // Convert to site frame
    poseB = pose2f_mul(poseA, poseB);

    // Must be feasible from A to B
    mpA = this->allocManeuver(poseA, steerAngle, poseB);
    if (this->isFeasible(mpA, poseA, steerAngle, poseB))
    {
      // Must be feasible from B to C
      mpB = this->allocManeuver(poseB, 0, poseC);
      if (this->isFeasible(mpB, poseB, 0, poseC))
      {
        // Looks ok; add this one
        nodeB = this->allocNode(poseB.pos.x, poseB.pos.y);
        nodeB->pose.rot = poseB.rot;    
        this->insertManeuver(mpA, nodeA, nodeB, true);
        this->insertManeuver(mpB, nodeB, nodeC, true);
      }
      this->freeManeuver(mpB);
    }
    this->freeManeuver(mpA);
  }

  // If there is no reversing allowed, we are done.
  if (!this->allowReverse)
    return 0;
  
  // Control parameters for the rear family of solutions
  sx = -this->rearDist;
  sy = +this->rearWidth/2;
  n = (int) ceil(sy / this->rearSpacing);
  spacing = sy / n;

  // Generate a family of solutions to the rear of the vehicle, then
  // link them up to the goal.
  for (i = -n; i <= +n; i++)
  {
    // Pick a target pose in the vehicle frame
    poseB.pos.x = sx;
    poseB.pos.y = i * spacing;
    poseB.rot = 0;
    
    // Convert to site frame
    poseB = pose2f_mul(poseA, poseB);

    // Must be feasible from B to A (we will drive this in reverse)
    mpA = this->allocManeuver(poseB, 0, poseA); // TODO steerAngle
    if (this->isFeasible(mpA, poseB, 0, poseA)) // TODO steerAngle
    {
      // Must be feasible from B to C
      mpB = this->allocManeuver(poseB, 0, poseC);
      if (this->isFeasible(mpB, poseB, 0, poseC))
      {
        // Looks ok; add this one
        nodeB = this->allocNode(poseB.pos.x, poseB.pos.y);
        nodeB->pose.rot = poseB.rot;    
        this->insertManeuver(mpA, nodeB, nodeA, true);
        this->insertManeuver(mpB, nodeB, nodeC, true);
      }
      this->freeManeuver(mpB);
    }
    this->freeManeuver(mpA);
  }

  return 0;
}


// Check for feasibility of an extended maneuver
bool PlanGraphUpdater::isFeasible(Maneuver *mp, pose2f_t poseA, float steerA, pose2f_t poseB)
{
  VehicleConfiguration configA, config;
  Pose2D configB;
  pose2f_t src, dst;
  double  dm, s;
  int i, numSteps;
  float spacing;

  // TODO vary the resolution for better speed.
  
  // Step size
  spacing = this->spacing;

  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;
  configA.phi = steerA;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(poseB.pos, poseA.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);

  // Check for feasibility along the entire maneuver, including the
  // final step.
  src.pos = vec2f_set(configA.x, configA.y);
  src.rot = configA.theta;
  for (i = 1; i < numSteps + 1; i++)
  {
    s = (double) i / numSteps;  
    config = maneuver_evaluate_configuration(this->vp, mp, s);    
    dst.pos = vec2f_set(config.x, config.y);
    dst.rot = config.theta;    
    if (!this->isFeasibleStep(src, dst))
      break;
    src = dst;
  }

  // Not feasible if we did not get to the end
  if (i < numSteps + 1)
    return false;
  
  return true;
}


// Do check for unfeasible maneuvers using the vehicle turning circle.
bool PlanGraphUpdater::isFeasibleStep(pose2f_t poseA, pose2f_t poseB)
{
  float turnRadius;
  pose2f_t pose;
  float x0, y0, dx, dy, s, t;
    
  // Vehicle turning radius
  turnRadius = this->wheelBase / tan(this->maxSteer);
  
  // Final pose in the initial frame
  pose = pose2f_mul(pose2f_inv(poseA), poseB);

  // Compute equation of line at right angles to current pose.
  // x = t dx + x0.
  // y = t dy + y0.
  x0 = pose.pos.x;
  y0 = pose.pos.y;
  dx = -sin(pose.rot);
  dy = +cos(pose.rot);

  // Trap straight-line case to avoid div-by-zero.
  if (fabs(dx) < 1e-6)
    return true;
  
  // Find intercept with x = 0; the values t and s specify the radii
  // of two turning circles.
  t = -x0 / dx;
  s = t * dy + y0;

  //MSG("check %f %f : %f %f : %f %f : %f", x0, y0, dx, dy, t, s, turnRadius);

  if (fabs(t) < turnRadius)
    return false;
  if (fabs(s) < turnRadius)
    return false;
  if (s * t < 0)
    return false;
        
  return true;
}


// Insert a maneuver between the given nodes.
int PlanGraphUpdater::insertManeuver(Maneuver *mp,
                                     PlanGraphNode *nodeA, PlanGraphNode *nodeB, bool isOffRoad)
{
  VehicleConfiguration configA, config;
  Pose2D configB;
  double  dm, s;
  int i, numSteps;
  PlanGraphNode *src, *dst;
  float spacing;
  
  assert(nodeA);
  assert(nodeB);

  // TODO vary the resolution for better speed.
  
  // Node spacing
  spacing = this->spacing;
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = nodeA->pose.rot;
  configA.phi = nodeA->steerAngle;
  
  // Final vehicle configuration
  configB.x = nodeB->pose.pos.x;
  configB.y = nodeB->pose.pos.y;
  configB.theta = nodeB->pose.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);
  
  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a  node
    dst = this->allocNode(config.x, config.y);
    assert(dst);

    // Fill out basic node data
    dst->pose.pos = vec2f_set((float) config.x, (float) config.y);
    dst->pose.rot = config.theta;
    dst->steerAngle = config.phi;
    
    // Set the interpolant
    dst->interId = i;

    // Flags
    dst->flags.isOffRoad = isOffRoad;
    
    // Fill out waypoint pointers
    dst->prevWaypoint = nodeA->prevWaypoint;
    dst->nextWaypoint = nodeB->nextWaypoint;
        
    // Create an arc from the previous node to the new node
    this->graph->insertArc(src, dst);

    src = dst;
  }
  dst = nodeB;

  // Create an arc to the final node
  this->graph->insertArc(src, dst);

  return 0;
}


// Create a maneuver
Maneuver *PlanGraphUpdater::allocManeuver(pose2f_t poseA, float steerA, pose2f_t poseB)
{
  Maneuver *mp;  
  VehicleConfiguration configA;
  Pose2D configB;
  
  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;
  configA.phi = steerA;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Create maneuver object
  mp = maneuver_config2pose(this->vp, &configA, &configB);
  assert(mp);

  return mp;
}


// Free a maneuver
void PlanGraphUpdater::freeManeuver(Maneuver *mp)
{
  maneuver_free(mp);
  return;
}


// Allocate a  node
PlanGraphNode *PlanGraphUpdater::allocNode(float px, float py)
{
  PlanGraphNode *node;
  
  // Allocate node in the usual fashion
  node = this->graph->allocNode(px, py);

  node->flags.isVolatile = true;
  node->flags.isVehicle = true;

  // Make room in our list of volatile nodes
  if (this->numNodes >= this->maxNodes)
  {
    this->maxNodes += 512;
    this->nodes = (PlanGraphNode**)
      realloc(this->nodes, this->maxNodes * sizeof(PlanGraphNode*));
    assert(this->nodes);
  }

  // Add node to list
  this->nodes[this->numNodes++] = node;

  return node;
}


// Free all  noes at once
void PlanGraphUpdater::freeNodes()
{
  int i;
  PlanGraphNode *node;
  
  // Free nodes in reverse order on the unsubstantiated belief that
  // this will be faster
  for (i = this->numNodes - 1; i >= 0; i--)
  {
    node = this->nodes[i];
    this->graph->freeNode(node);
  }
  this->numNodes = 0;
  
  return;
}

