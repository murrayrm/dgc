
/* 
 * Desc: Path data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_PATH_HH
#define PLAN_GRAPH_PATH_HH


// Dependencies
#include "PlanGraph.hh"


// Maximum path length
#define PLAN_GRAPH_PATH_MAX_NODES 65535

/// Is the vehicle supposed to be driving forwards or backwards?
enum PlanGraphPathDirection
{
  PLAN_GRAPH_PATH_FWD,
  PLAN_GRAPH_PATH_REV,
};


/// @brief Class describing a planned path.
struct PlanGraphPath
{
  // Is this path valid?
  bool valid;
  
  // Does this path have a collision with a static obstacle?
  int collideObs;

  // Does this path have a collision with a non-static car?
  int collideCar;

  // Does this path have a stop?
  int hasStop;

  // distance to stop line
  double stopDistAlongPath;

  // distance to stop line
  double stopDistNode;

  // index of closest node to stop line within path
  int stopIndex;

  // Does this path have a ROW intersection with a left turn?
  int hasRow;

  // Waypoint of ROW intersection
  RNDFGraphWaypoint* rowWaypoint;

  // Waypoint of stop line
  RNDFGraphWaypoint* stopWaypoint;

  // What is the total distance along this path?
  float dist;

  // Number of elements in the path
  int pathLen;
 
  // List of graph nodes in the path
  PlanGraphNode *nodes[PLAN_GRAPH_PATH_MAX_NODES];

  // Driving direction at each node
  PlanGraphPathDirection directions[PLAN_GRAPH_PATH_MAX_NODES];
  
  // Distance profile along the path
  float dists[PLAN_GRAPH_PATH_MAX_NODES];
  
  // Speed profile along the path
  float speeds[PLAN_GRAPH_PATH_MAX_NODES + 1];

  // Acceleration profile along the path
  float accelerations[PLAN_GRAPH_PATH_MAX_NODES];

  // Curvature profile along the path
  float curvatures[PLAN_GRAPH_PATH_MAX_NODES];
};


#endif
