/*!
 * \file CmdArgs.cc
 * \brief Header for the command line arguments object that gets passed around in the planner stack
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef CMDARGS_HH_
#define CMDARGS_HH_

#include <string>

using namespace std;

class CmdArgs {
public:
  static int sn_key;
  static bool debug;
  static bool use_RNDF;
  static string RNDF_file;
  static int verbose_level;
  static bool logging;
  static bool console;
  static bool lane_cost;
  static string log_path;
  static string log_filename;
  static int log_level;
  static int visualization_level;
  static bool mod_log_planner;
  static bool lt_planner;
  static bool closed_loop;
  static bool noprediction;
  static bool stepbystep_load;
  static bool load_graph_files;
  static bool update_from_map;
  static bool disable_fused_perceptor;
  static bool update_lanes;
  static bool update_stoplines;
  static bool mapper_use_internal;
  static int mapper_debug_subgroup;
  static bool mapper_disable_obs_fusion;
  static bool mapper_disable_line_fusion;
  static bool mapper_line_fusion_compat;
  static int mapper_decay_thresh;
  static bool mapper_enable_groundstrike_filtering;
  static bool use_dplanner; 
  static bool use_s1planner; 
  static bool use_circle_planner; 
  static bool use_hacked_uturn;
  static bool use_hacked_backup;
  static bool show_costmap;
	static bool use_rndf_frame;
  static char* costmap_config;
  static char* path_plan_config;
  static char* planner_config;
  static bool no_failure_handling;
  static bool use_prob_planner;
  static double prob_conf_bound; //use for prob_planner position uncertainty; 68%: 1, 80%: 1.282, 90%: 1.645
  static int sparse;
  static bool use_rrt_planner;
};

#endif
