
/* 
 * Desc: Generates a dense graph for planning.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#if USE_GL
#include <GL/glut.h>
#endif

#include <alice/AliceConstants.h>
#include <trajutils/maneuver.h>
#include <temp-planner-interfaces/Log.hh>

#include "PlanGraph.hh"


// Magic number for saved files
#define PLAN_GRAPH_MAGIC_NUMBER 0x1337

// File format version number
#define PLAN_GRAPH_VERSION 0x0006


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Mask comparison macro.
// Cast the flags to an appropriately size integer and "and" with the mask.
#define CHECK_MASK(mask, flags) ( ((*((uint16_t*) &(flags))) & (mask)) != 0)


// Constructor
PlanGraph::PlanGraph()
{
  this->vehicleNode = NULL;
  
  // MAGIC
  this->quadScale = 1.0; 
    
  // Create the root quad
  this->root = (PlanGraphQuad*) calloc(1, sizeof(PlanGraphQuad));
  this->root->px = 0;
  this->root->py = 0;
  this->root->size = 8192; // MAGIC
  
  this->quadCount = 1;
  this->quadBytes = 0;
  this->nodeCount = 0;
  this->nodeBytes = 0;

  this->wheelBase = VEHICLE_WHEELBASE;
  this->maxSteer  = VEHICLE_MAX_AVG_STEER;

  this->roiIndex = 0;
  this->roiSpacing = 1.0; // MAGIC

  this->statusTime = 0;
  this->statusMaxAge = 2000000;
  
  this->structList = 0;
  this->statusList = 0;
  
  return;
}


// Destructor
PlanGraph::~PlanGraph()
{  
  // Clean out the quad-tree; this also deletes the nodes
  assert(this->root);
  this->freeQuad(this->root);
  this->root = NULL;

  return;
}


// Get the canonical node corresponding to the given checkpoint ID
PlanGraphNode *PlanGraph::getCheckpoint(int checkpointId)
{
  RNDFGraphWaypoint* wp;
  wp = this->rndf.getCheckpoint(checkpointId);
  if (!wp)
    return NULL;
  return this->getWaypoint(wp->segmentId, wp->laneId, wp->waypointId);
}


// Get the canonical node corresponding to the given RNDF ID
PlanGraphNode *PlanGraph::getWaypoint(int segmentId, int laneId, int waypointId)
{
  int i;
  float dist, minDist;
  RNDFGraphWaypoint* wp;  
  PlanGraphNode *node, *minNode;
  PlanGraphNodeList nodes;

  // Loop up in the underlying RNDF.  This is faster.
  wp = this->rndf.getWaypoint(segmentId, laneId, waypointId);
  if (!wp)
    return NULL;

  // Now look up by position.
  this->getRegion(&nodes, wp->px, wp->py, 0.1, 0.1);

  // Find the closest matching node.
  minNode = NULL;
  minDist = FLT_MAX;
  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];
    if (node->segmentId == segmentId && node->laneId == laneId && node->waypointId == waypointId)
    {
      dist = pow(wp->px - node->pose.pos.x, 2) + pow(wp->py - node->pose.pos.y, 2);
      if (dist < minDist)
      {
        minDist = dist;
        minNode = node;
      }
    }
  }
  
  return minNode;
}


// Get the node nearest the given position
PlanGraphNode *PlanGraph::getNearestPos(float px, float py, float maxDist,
                                        uint16_t includeFlags, uint16_t excludeFlags)
{
  int i;
  PlanGraphNode *node, *minNode;
  PlanGraphNodeList nodes;
  float dist, minDist;

  // Get all the nodes in the requested region.
  this->getRegion(&nodes, px, py, 2*maxDist, 2*maxDist, includeFlags, excludeFlags);

  // Get the nearest node (Euclidean distance) in this list
  minDist = maxDist;
  minNode = NULL;
  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];    
    dist = vec2f_mag(vec2f_sub(node->pose.pos, vec2f_set(px, py)));
    if (dist <= minDist)
    {
      minDist = dist;
      minNode = node;
    }
  }      

  // This may be null if there is nothing within the specified distance.
  return minNode;
}


// Get all nodes in a square region.
int PlanGraph::getRegion(PlanGraphNodeList *nodes, float px, float py, float sx, float sy,
                         uint16_t includeFlags, uint16_t excludeFlags)
{
  nodes->clear();
  if (this->root)
    this->getRegionQuad(nodes, this->root, px, py, sx, sy, includeFlags, excludeFlags);
    
  return 0;
}


// Get all nodes in a square region (recursive form)
int PlanGraph::getRegionQuad(PlanGraphNodeList *nodes, PlanGraphQuad *quad,
                             float px, float py, float sx, float sy,
                             uint16_t includeFlags, uint16_t excludeFlags)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  
  // Check for overlap between this quad and the requested region.  If
  // there is no overlap, stop now.
  if (!quad->hasIntersection(px, py, sx, sy))
    return 0;

  // Recurse through the quad tree
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->getRegionQuad(nodes, leaf, px, py, sx, sy, includeFlags, excludeFlags);
  }

  // Add our nodes
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    if (!CHECK_MASK(includeFlags, node->flags))
      continue;
    if (CHECK_MASK(excludeFlags, node->flags))
      continue;
    nodes->push_back(node);
  }
  
  return 0;
}


// Update the region of interest
int PlanGraph::updateROI(float px, float py, float size)
{
  // Check for changes before updating the ROI
  if (this->roiIndex > 0 &&
      fabs(px - this->roiPx) < this->quadScale &&
      fabs(py - this->roiPy) < this->quadScale &&
      fabs(size - this->roiSize) < this->quadScale)
    return 0;
  
  // Update the ROI index; we use this to figure out which nodes are
  // in the new ROI, and which are in the old ROI.  Note that there
  // could be some weirdness if the index rolls around to zero, but
  // this will take thousands of hours under normal usage.
  this->roiIndex++;
  assert(this->roiIndex > 0);

  // Expand all nodes in the new ROI.  This may timeout, in which case
  // only some nodes are expanded.  These will be expanded on
  // subsequent calls to updateROI.
  this->expandROI(this->root, px, py, size, this->roiIndex);

  // Contract all nodes in the old ROI that are not in the new ROI.
  // We should not call this on the first time through.
  if (this->roiIndex > 1)
    this->contractROI(this->root, this->roiPx, this->roiPy, this->roiSize, this->roiIndex);
  
  // Keep track of the ROI so we can correctly contract it next time
  // through.  Only do this if the process completes within the timeout.
  this->roiPx = px;
  this->roiPy = py;
  this->roiSize = size;

  //MSG("ROI nodes %d (%.3fMb)",
  //    this->nodeCount, this->nodeBytes * 1e-6);

  return 0;
}


// Test a point to see if it lies in the ROI
bool PlanGraph::hasPointROI(float px, float py)
{
  if (px < this->roiPx - this->roiSize/2)
    return false;
  if (px >= this->roiPx + this->roiSize/2)
    return false;
  if (py < this->roiPy - this->roiSize/2)
    return false;
  if (py >= this->roiPy + this->roiSize/2)
    return false;
  return true;
}


// Expand nodes in the ROI
int PlanGraph::expandROI(PlanGraphQuad *quad, float px, float py, float size, uint32_t index)
{
  int i, j;
  PlanGraphQuad *leaf;
  PlanGraphNode *nodeA, *nodeB;
  
  // If this quad has no intersection with the ROI, skip it.
  if (!quad->hasIntersection(px, py, size, size))
    return 0;

  // Recurse into leaves
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->expandROI(leaf, px, py, size, index);
  }

  // Look for expandable nodes in this quad.
  for (i = 0; i < quad->numNodes; i++)
  {
    nodeA = quad->nodes[i];

    // If node is already expanded, update the index only.
    if (nodeA->roiIndex > 0)
    {
      nodeA->roiIndex = index;
      continue;
    }

    // Expand all feasible arcs for this node in to feasible
    // maneuvers.
    for (j = 0; j < nodeA->numDest; j++)
    {
      nodeB = nodeA->dest[j];
      this->insertManeuverROI(nodeA, nodeB);
    }
    nodeA->roiIndex = index;
  }

  return 0;
}


// Contract nodes in the ROI
int PlanGraph::contractROI(PlanGraphQuad *quad, float px, float py, float size, uint32_t index)
{
  int i, j;
  PlanGraphQuad *leaf;
  PlanGraphNode *nodeA, *nodeB;

  // If this quad has no intersection with the ROI, skip it.
  if (!quad->hasIntersection(px, py, size, size))
    return 0;

  // Recurse into leaves
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->contractROI(leaf, px, py, size, index);
  }
  
  // Look for contractable nodes in this quad (i.e., if they are not
  // in the current ROI).
  for (i = 0; i < quad->numNodes; i++)
  {
    nodeA = quad->nodes[i];
        
    // If node is not expanded, skip it.  
    if (nodeA->roiIndex == 0)
      continue;

    // If the node is expanded, but belongs in the ROI, skip it.
    if (nodeA->roiIndex == index)
      continue;

    // Contract feasible maneuvers for this node.
    for (j = 0; j < nodeA->numDest; j++)
    {
      nodeB = nodeA->dest[j];
      this->removeManeuverROI(nodeA, nodeB);
    }
    nodeA->roiIndex = 0;
  }

  return 0;
}


// Insert a maneuver between the given nodes.
int PlanGraph::insertManeuverROI(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB, config;
  double  dm, s;
  int i, numSteps;
  PlanGraphNode *src, *dst;
  float spacing;

  assert(nodeA);
  assert(nodeB);

  // Node spacing
  spacing = this->roiSpacing;
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = nodeA->pose.rot;

  // Final vehicle configuration
  configB.x = nodeB->pose.pos.x;
  configB.y = nodeB->pose.pos.y;
  configB.theta = nodeB->pose.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);

  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);
  
  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_pose(vp, mp, s);    
    
    // Create a maneuver node
    dst = this->allocNode(config.x, config.y);
    assert(dst);

    // Fill out basic node data
    dst->pose.pos = vec2f_set((float) config.x, (float) config.y);
    dst->pose.rot = config.theta;

    // Fill out nominal RNDF data.  These are not very meaningful.
    if (i < numSteps / 2)
    {
      dst->segmentId = nodeA->segmentId;
      dst->laneId = nodeA->laneId;
      dst->waypointId = nodeA->waypointId;
    }
    else
    {
      dst->segmentId = nodeB->segmentId;
      dst->laneId = nodeB->laneId;
      dst->waypointId = nodeB->waypointId;
    }
    
    // Set the interpolant
    dst->interId = i;
    
    // Set flags 
    if (nodeA->laneId != nodeB->laneId)
      dst->flags.isLaneChange = true;
    if (nodeA->railId != nodeB->railId)
      dst->flags.isRailChange = true;
    dst->flags.isOncoming = nodeB->flags.isOncoming;
    dst->flags.isVolatile = true;

    // Use the id of the rail we are going to.  Helps the
    // planner keep Alice on the center rail.
    dst->railId = nodeB->railId;      

    // Record the final destination for this maneuver.  This help us
    // later when we want to delete the maneuver.
    dst->roiDest = nodeB;

    // Fill out waypoint pointers
    dst->prevWaypoint = nodeA->prevWaypoint;
    dst->nextWaypoint = nodeB->nextWaypoint;
    
    // Create an arc from the previous node to the new node
    this->insertArc(src, dst);

    src = dst;
  }
  dst = nodeB;

  // Create an arc to the final node
  this->insertArc(src, dst);
  
  maneuver_free(mp);
  free(vp);

  return 0;
}


// Remove a maneuver between the given nodes.
int PlanGraph::removeManeuverROI(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  int i;
  PlanGraphNode *src, *dst;

  // Walk along the route between nodeA and nodeB, deleting the nodes
  // and arcs as we go.
  src = nodeA;
  while (src != nodeB)
  {
    dst = NULL;
    for (i = 0; i < src->numNext; i++)
    {
      if (src->next[i] == nodeB || src->next[i]->roiDest == nodeB)
      {
        dst = src->next[i];
        break;
      }
    }
    assert(dst);

    // Delete arc and node
    this->removeArc(src, dst);
    if (src != nodeA)
      this->freeNode(src);
    
    src = dst;
  }

  return 0;
}


// Allocate new node
PlanGraphNode *PlanGraph::allocNode(float px, float py)
{
  PlanGraphNode *node;
  PlanGraphQuad *quad;

  node = (PlanGraphNode*) calloc(1, sizeof(PlanGraphNode));
  assert(node);
  node->refCount = 1;

  this->nodeCount += 1;
  this->nodeBytes += sizeof(*node);

  node->nodeId = this->nodeCount;
  node->pose.pos.x = px;
  node->pose.pos.y = py;
  node->pose.rot = 0;

  // Get a quad to put the node in
  quad = this->allocQuad(this->root, px, py);
  assert(quad);
  
  // Make sure there is space in the quad to store the new node
  // pointer.
  if (quad->numNodes >= quad->maxNodes)
  {
    this->quadBytes -= quad->maxNodes * sizeof(quad->nodes[0]);
    quad->maxNodes = quad->numNodes + 1;
    quad->nodes = (PlanGraphNode**) realloc(quad->nodes, quad->maxNodes*sizeof(quad->nodes[0]));
    this->quadBytes += quad->maxNodes * sizeof(quad->nodes[0]);
  }

  // Add node to the quad
  quad->nodes[quad->numNodes++] = node;
    
  return node;
}


// Free a node
void PlanGraph::freeNode(PlanGraphNode *node)
{
  int i;
  PlanGraphQuad *quad;

  // Decrement the reference count; dont delete unless the reference
  // count is zero.
  node->refCount--;
  assert(node->refCount >= 0);
  if (node->refCount > 0)
    return;
  
  // Get the quad we are in
  quad = this->getNearestQuad(this->root, node->pose.pos.x, node->pose.pos.y);
  assert(quad);

  // Remove node from the quad
  for (i = 0; i < quad->numNodes; i++)
    if (quad->nodes[i] == node)
      break;
  for (; i < quad->numNodes - 1; i++)
    quad->nodes[i] = quad->nodes[i + 1];
  quad->numNodes--;

  // Shrink quad storage if there is nothing left
  if (quad->numNodes == 0)
  {
    free(quad->nodes);
    quad->nodes = NULL;
    quad->maxNodes = 0;
  }

  // Remove our arcs
  while (node->numNext > 0)
    this->removeArc(node, node->next[0]);
  while (node->numPrev > 0)
    this->removeArc(node->prev[0], node);
  while (node->numDest > 0)
    this->removeFeasibleArc(node, node->dest[0]);
  
  // All our arcs should be gone by now, so free the storage
  assert(node->numNext == 0);
  free(node->next);
  assert(node->numPrev == 0);
  free(node->prev);
  assert(node->numDest == 0);
  free(node->dest);

  // Done with this node
  this->nodeCount -= 1;
  this->nodeBytes -= sizeof(*node);
  free(node);

  return;
}


// Fix a volatile node in memory (so it wont get freed).
void PlanGraph::fixNode(PlanGraphNode *node)
{
  node->refCount++;
  return;
}


// Unfix a volatile node in memory (so that it may be freed).
void PlanGraph::unfixNode(PlanGraphNode *node)
{
  this->freeNode(node);
  return;
}


// Insert an arc
int PlanGraph::insertArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  // Expand the next list in node A
  if (nodeA->maxNext >= nodeA->maxNext)
  {
    this->nodeBytes -= nodeA->maxNext*sizeof(nodeA->next[0]);
    nodeA->maxNext += 1; 
    nodeA->next = (PlanGraphNode**) realloc(nodeA->next, nodeA->maxNext*sizeof(nodeA->next[0]));
    this->nodeBytes += nodeA->maxNext*sizeof(nodeA->next[0]);
  }

  // Insert arc in node A
  nodeA->next[nodeA->numNext++] = nodeB;
    
  // Expact the prev list in node B
  if (nodeB->maxPrev >= nodeB->maxPrev)
  {
    this->nodeBytes -= nodeB->maxPrev*sizeof(nodeB->prev[0]);
    nodeB->maxPrev += 1;
    nodeB->prev = (PlanGraphNode**) realloc(nodeB->prev, nodeB->maxPrev*sizeof(nodeB->prev[0]));
    this->nodeBytes += nodeB->maxPrev*sizeof(nodeB->prev[0]);
  }

  // Insert arc in node B
  nodeB->prev[nodeB->numPrev++] = nodeA;

  return 0;
}


// Remove an arc
int PlanGraph::removeArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  int i;
  PlanGraphNode *node;
  
  // Remove from the next nodes for nodeA
  for (i = 0; i < nodeA->numNext; i++)
  {
    node = nodeA->next[i];
    if (node == nodeB)
      break;
  }
  for (; i + 1 < nodeA->numNext; i++)
    nodeA->next[i] = nodeA->next[i + 1];
  nodeA->numNext--;

  // Shrink the list 
  if (nodeA->numNext < nodeA->maxNext) 
  {
    this->nodeBytes -= nodeA->maxNext*sizeof(nodeA->next[0]);
    nodeA->maxNext -= 1;
    nodeA->next = (PlanGraphNode**) realloc(nodeA->next, nodeA->maxNext*sizeof(nodeA->next[0]));
    this->nodeBytes += nodeA->maxNext*sizeof(nodeA->next[0]);
  }

  // Remove from the prev nodes for nodeB
  for (i = 0; i < nodeB->numPrev; i++)
  {
    node = nodeB->prev[i];
    if (node == nodeA)
      break;
  }
  for (; i + 1 < nodeB->numPrev; i++)
    nodeB->prev[i] = nodeB->prev[i + 1];
  nodeB->numPrev--;

  // Shrink the list 
  if (nodeB->numPrev < nodeB->maxPrev)
  {
    this->nodeBytes -= nodeB->maxPrev*sizeof(nodeB->prev[0]);
    nodeB->maxPrev -= 1; 
    nodeB->prev = (PlanGraphNode**) realloc(nodeB->prev, nodeB->maxPrev*sizeof(nodeB->prev[0]));
    this->nodeBytes += nodeB->maxPrev*sizeof(nodeB->prev[0]);
  }

  return 0;
}

  
// Insert a new feasible arc.  
int PlanGraph::insertFeasibleArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  // Make room in node A
  if (nodeA->maxDest >= nodeA->maxDest)
  {
    this->nodeBytes -= nodeA->maxDest*sizeof(nodeA->dest[0]);
    nodeA->maxDest += 1;
    nodeA->dest = (PlanGraphNode**) realloc(nodeA->dest, nodeA->maxDest*sizeof(nodeA->dest[0]));
    this->nodeBytes += nodeA->maxDest*sizeof(nodeA->dest[0]);
  }

  // Insert arcs
  nodeA->dest[nodeA->numDest++] = nodeB;

  return 0;
}


// Remove a feasible arc
int PlanGraph::removeFeasibleArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  int i;
  PlanGraphNode *node;
  
  // Remove from the feasible nodes for nodeA
  for (i = 0; i < nodeA->numDest; i++)
  {
    node = nodeA->dest[i];
    if (node == nodeB)
      break;
  }
  for (; i + 1 < nodeA->numDest; i++)
    nodeA->dest[i] = nodeA->dest[i + 1];
  nodeA->numDest--;

  // Shrink the list 
  if (nodeA->numDest < nodeA->maxDest - 8)  // MAGIC
  {
    this->nodeBytes -= nodeA->maxDest*sizeof(nodeA->dest[0]);
    nodeA->maxDest -= 8;
    nodeA->dest = (PlanGraphNode**) realloc(nodeA->dest, nodeA->maxDest*sizeof(nodeA->dest[0]));
    this->nodeBytes += nodeA->maxDest*sizeof(nodeA->dest[0]);
  }

  return 0;
}


// Allocate a new quad
PlanGraphQuad *PlanGraph::allocQuad(PlanGraphQuad *quad, float px, float py)
{
  int li;
  float qx, qy, size;
  PlanGraphQuad *leaf;

  // See if the point falls within this quad; if not, give up.
  if (!quad->hasPoint(px, py))
    return NULL;

  // If we have not arrived at a terminal leaf...
  if (quad->size > this->quadScale)
  {
    // See which leaf this point belongs to
    li = 2 * (py < quad->py) + (px < quad->px);    
    leaf = quad->leaves[li];

    // If we need to recurse, but there is no leaf, create one now
    if (!leaf)
    {
      size = quad->size / 2;
      if (px < quad->px)
        qx = quad->px - size/2;
      else
        qx = quad->px + size/2;
      if (py < quad->py)
        qy = quad->py - size/2;
      else
        qy = quad->py + size/2;

      leaf = (PlanGraphQuad*) calloc(1, sizeof(PlanGraphQuad));
      assert(leaf);
      this->quadCount += 1;
      this->quadBytes += sizeof(*leaf);

      leaf->px = qx;
      leaf->py = qy;
      leaf->size = size;
      quad->leaves[li] = leaf;
    }

    // Recurse
    return this->allocQuad(leaf, px, py);
  }

  // If we have arrived at a terminal leaf, then we are done
  return quad;
}


// Free a quad and all the nodes in it
void PlanGraph::freeQuad(PlanGraphQuad *quad)
{
  int i;
  PlanGraphQuad *leaf;
  
  // Free all of our nodes
  while (quad->numNodes > 0)
    this->freeNode(quad->nodes[0]);

  // Free node storage in the quad
  free(quad->nodes);
  quad->nodes = NULL;
  quad->numNodes = 0;
  quad->maxNodes = 0;

  // Recurse
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->freeQuad(leaf);
    quad->leaves[i] = NULL;
  }

  // Finally, free ourselves
  free(quad);
  
  return;
}


// Get the quad nearest the given position
PlanGraphQuad *PlanGraph::getNearestQuad(PlanGraphQuad *quad, float px, float py)
{
  int i;
  PlanGraphQuad *leaf;
  
  // See if the point falls within this quad; if not, give up.
  if (!quad->hasPoint(px, py))
    return NULL;

  // Check our leaves
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (!leaf)
      continue;
    leaf = this->getNearestQuad(leaf, px, py);
    if (leaf)
      return leaf;
  }

  // We dont have leaves, so it must be us
  return quad;
}


// Macros for reading and writing
#define FWRITE(file, var) fwrite(var, sizeof(*var), 1, file)
#define FREAD(file, var) fread(var, sizeof(*var), 1, file)


// Save to a binary file
int PlanGraph::save(const char *filename)
{
  FILE *file;
  uint32_t magic, version;
  uint32_t reserved[64] = {0};
  
  if (!this->root)
    return ERROR("graph is empty; not saving");

  file = fopen(filename, "w");
  if (!file)
    return ERROR("unable to open %s : %s", filename, strerror(errno));
  
  // Save header data
  magic = PLAN_GRAPH_MAGIC_NUMBER;
  version = PLAN_GRAPH_VERSION;
  FWRITE(file, &magic);
  FWRITE(file, &version);
  FWRITE(file, reserved);

  // Save the RNDF first.
  this->rndf.writeFile(file);
  
  // Save recursively.  This is done it two passes (nodes then arcs)
  // to simplify loading.
  this->writeQuadNodes(file, this->root);
  this->writeQuadArcs(file, this->root);

  fclose(file);
  
  return 0;
}


// Load from a binary file
int PlanGraph::load(const char *filename)
{
  FILE *file;
  uint32_t magic, version;
  uint32_t reserved[64];
  
  if (!this->root)
    return ERROR("graph is empty; not saving");

  file = fopen(filename, "r");
  if (!file)
    return ERROR("unable to open %s : %s", filename, strerror(errno));
  
  // Load header data
  FREAD(file, &magic);
  FREAD(file, &version);
  FREAD(file, reserved);

  // Check for consistency
  if (magic != PLAN_GRAPH_MAGIC_NUMBER)
  {
    fclose(file);
    return ERROR("file has incorrect magic number");
  }
  if (version != PLAN_GRAPH_VERSION)
  {
    fclose(file);
    return ERROR("file has incorrect version number %X", version);
  }

  // Load the underlying RNDF first so that we can set up the links
  // correctly.
  this->rndf.readFile(file);
  
  // Load file recusively.  This is done in two passes to allow us
  // to correctly reconstruct the arcs once all the nodes have been created.
  this->readQuadNodes(file, this->root);
  this->readQuadArcs(file, this->root);

  fclose(file);
  
  return 0;
}


// Write quad nodes (recursive form)
int PlanGraph::writeQuadNodes(FILE *file, PlanGraphQuad *quad)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  uint32_t exists;
      
  // Write quad header data
  FWRITE(file, &quad->px);
  FWRITE(file, &quad->py);
  FWRITE(file, &quad->size);

  // Recurse into the leaves unconditionally to either save the quad
  // or a marker indicated that it is no defined.
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    exists = (leaf != NULL);    
    FWRITE(file, &exists);
    if (exists)
      this->writeQuadNodes(file, leaf);
  }

  // Write nodes for this quad
  FWRITE(file, &quad->numNodes);
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    
    FWRITE(file, &node->nodeId);
    FWRITE(file, &node->pose);
    FWRITE(file, &node->steerAngle);
    FWRITE(file, &node->segmentId);
    FWRITE(file, &node->laneId);
    FWRITE(file, &node->waypointId);
    FWRITE(file, &node->interId);
    FWRITE(file, &node->railId);
    FWRITE(file, &node->flags);

    FWRITE(file, &node->nextWaypoint->segmentId);
    FWRITE(file, &node->nextWaypoint->laneId);
    FWRITE(file, &node->nextWaypoint->waypointId);
    FWRITE(file, &node->prevWaypoint->segmentId);
    FWRITE(file, &node->prevWaypoint->laneId);
    FWRITE(file, &node->prevWaypoint->waypointId);
  }

  return 0;
}


// Write quad arcs (recursive form)
int PlanGraph::writeQuadArcs(FILE *file, PlanGraphQuad *quad)
{
  int i, j;
  PlanGraphQuad *leaf;
  PlanGraphNode *node, *next, *prev, *dest;
      
  // Recurse into the leaves unconditionally to either save the quad
  // or a marker indicated that it is no defined.
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->writeQuadArcs(file, leaf);
  }

  // Write nodes for this quad
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];

    // Write outgoing arcs for this node
    FWRITE(file, &node->numNext);
    for (j = 0; j < node->numNext; j++)
    {
      next = node->next[j];
      FWRITE(file, &next->nodeId);
      FWRITE(file, &next->pose.pos.x);
      FWRITE(file, &next->pose.pos.y);
    }

    // Write incoming arcs for this node
    FWRITE(file, &node->numPrev);
    for (j = 0; j < node->numPrev; j++)
    {
      prev = node->prev[j];
      FWRITE(file, &prev->nodeId);
      FWRITE(file, &prev->pose.pos.x);
      FWRITE(file, &prev->pose.pos.y);
    }

    // Write feasible arcs for this node
    FWRITE(file, &node->numDest);
    for (j = 0; j < node->numDest; j++)
    {
      dest = node->dest[j];
      FWRITE(file, &dest->nodeId);
      FWRITE(file, &dest->pose.pos.x);
      FWRITE(file, &dest->pose.pos.y);
    }
  }

  return 0;
}


// Read a quad (recursive form)
int PlanGraph::readQuadNodes(FILE *file, PlanGraphQuad *quad)
{
  int i;
  PlanGraphQuad *leaf;
  PlanGraphNode *node;
  uint32_t exists;
  
  // Read header data
  FREAD(file, &quad->px);
  FREAD(file, &quad->py);
  FREAD(file, &quad->size);

  // Recurse into the leaves and create those that are in the file.
  for (i = 0; i < 4; i++)
  {
    FREAD(file, &exists);
    if (exists)
    {
      leaf = (PlanGraphQuad*) calloc(1, sizeof(PlanGraphQuad));
      assert(leaf);
      this->quadCount += 1;
      this->quadBytes += sizeof(*leaf);
      quad->leaves[i] = leaf;
      this->readQuadNodes(file, leaf);
    }
  }

  // Load the nodes for this quad
  FREAD(file, &quad->numNodes);
  quad->maxNodes = quad->numNodes;
  quad->nodes = (PlanGraphNode**) calloc(quad->maxNodes, sizeof(quad->nodes[0]));
  assert(quad->nodes);
  this->quadBytes += quad->maxNodes * sizeof(quad->nodes[0]);
  for (i = 0; i < quad->numNodes; i++)
  {
    node = (PlanGraphNode*) calloc(1, sizeof(PlanGraphNode));
    assert(node);
    node->refCount = 1;
    
    this->nodeCount += 1;
    this->nodeBytes += sizeof(*node);        
    quad->nodes[i] = node;

    FREAD(file, &node->nodeId);
    FREAD(file, &node->pose);
    FREAD(file, &node->steerAngle);
    FREAD(file, &node->segmentId);
    FREAD(file, &node->laneId);
    FREAD(file, &node->waypointId);
    FREAD(file, &node->interId);
    FREAD(file, &node->railId);
    FREAD(file, &node->flags);

    int segmentId, laneId, waypointId;
    
    FREAD(file, &segmentId);
    FREAD(file, &laneId);
    FREAD(file, &waypointId);
    node->nextWaypoint = this->rndf.getWaypoint(segmentId, laneId, waypointId);
    assert(node->nextWaypoint);
    
    FREAD(file, &segmentId);
    FREAD(file, &laneId);
    FREAD(file, &waypointId);
    node->prevWaypoint = this->rndf.getWaypoint(segmentId, laneId, waypointId);
    assert(node->prevWaypoint);
  }
    
  return 0;
}


// Read quad arcs (recursive form)
int PlanGraph::readQuadArcs(FILE *file, PlanGraphQuad *quad)
{
  int i, j;
  PlanGraphQuad *leaf;
  PlanGraphNode *node, *next, *prev, *dest;
  uint32_t id;
  float px, py;
  
  // Recurse into the leaves and read those that are in the file.
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->readQuadArcs(file, leaf);
  }

  // Load the nodes for this quad
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    
    // Read outgoing nodes
    FREAD(file, &node->numNext);
    node->maxNext = node->numNext;
    node->next = (PlanGraphNode**) calloc(node->maxNext, sizeof(node->next[0]));
    assert(node->next);
    this->nodeBytes += node->maxNext * sizeof(node->next[0]);
    for (j = 0; j < node->numNext; j++)
    {
      FREAD(file, &id);
      FREAD(file, &px);
      FREAD(file, &py);
      next = this->getNodeId(id, px, py);
      assert(next);
      node->next[j] = next;
    }

    // Read incoming nodes
    FREAD(file, &node->numPrev);
    node->maxPrev = node->numPrev;
    node->prev = (PlanGraphNode**) calloc(node->maxPrev, sizeof(node->prev[0]));
    assert(node->prev);
    this->nodeBytes += node->maxPrev * sizeof(node->prev[0]);
    for (j = 0; j < node->numPrev; j++)
    {
      FREAD(file, &id);
      FREAD(file, &px);
      FREAD(file, &py);
      prev = this->getNodeId(id, px, py);
      assert(prev);
      node->prev[j] = prev;
    }
  
    // Read feasible arcs
    FREAD(file, &node->numDest);
    node->maxDest = node->numDest;
    node->dest = (PlanGraphNode**) calloc(node->maxDest, sizeof(node->dest[0]));
    assert(node->dest);
    this->nodeBytes += node->maxDest * sizeof(node->dest[0]);
    for (j = 0; j < node->numDest; j++)
    {
      FREAD(file, &id);
      FREAD(file, &px);
      FREAD(file, &py);
      dest = this->getNodeId(id, px, py);
      assert(dest);
      node->dest[j] = dest;
    }
  }
    
  return 0;
}


// Get a node by id and position
PlanGraphNode *PlanGraph::getNodeId(uint32_t nodeId, float px, float py)
{
  int i;
  PlanGraphQuad *quad;
  PlanGraphNode *node;

  // Find the corresponding leaf in the quad-tree.
  quad = this->getNearestQuad(this->root, px, py);
  if (!quad)
    return NULL;

  // The node we are looking for should be in here somewhere.
  for (i = 0; i < quad->numNodes; i++)
  {
    node = quad->nodes[i];
    if (node->nodeId == nodeId)
      return node;
  }

  return NULL;
}


#if USE_GL

// Draw a text box
void drawText(float size, const char *text)
{
  int i, count;
  float sx, sy;

  // Compute the scale factors in x/y directions, based on the 'M'
  // character.
  sx = glutStrokeWidth(GLUT_STROKE_MONO_ROMAN, 'M');
  sy = sx * 1.3;
    
  glPushMatrix();
  glScalef(size / sx, size / sy, 1);
  glTranslatef(0, -sy, 0);

  glPushMatrix();
    
  for (i = 0, count = 0; i < (int) strlen(text); i++, count++)
  {
    if (text[i] == '\n')
    {
      glPopMatrix();
      glTranslatef(0, -sy, 0);
      glPushMatrix();
      count = 0;      
    }
    else
    {
      glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, text[i]);
    }
  }

  glPopMatrix();
  glPopMatrix();

  return;
}


// Predraw the road graph centered around some point
int PlanGraph::predrawStruct(float px, float py, float size,
                             uint16_t includeFlags, uint16_t excludeFlags)
{
  int i, j;
  PlanGraphNode *node, *next;
  PlanGraphNodeList nodes;

  // Dont do anything if we have already drawn a similar region
  if (this->structList > 0 && fabs(size - this->structListSize) < 0.01 &&
      fabs(px - this->structListPx) < 0.01 && fabs(py - this->structListPy) < 0.01 &&
      includeFlags == this->structInclude && excludeFlags == this->structExclude)
    return this->structList;

  this->structInclude = includeFlags;
  this->structExclude = excludeFlags;
  this->structListPx = px;
  this->structListPy = py;
  this->structListSize = size;
  
  // Create display list
  if (this->structList == 0)
    this->structList = glGenLists(1);
  glNewList(this->structList, GL_COMPILE);

  // Get the nodes in the region; this is not as fast as recursion
  // through the tree, but is it simpler.
  this->getRegion(&nodes, px, py, size, size, includeFlags, excludeFlags);

  if (false)
  {
    // Draw nodes
    for (i = 0; i < (int) nodes.size(); i++)
    {
      node = nodes[i];      

      glBegin(GL_LINES);
      glColor3f(1, 0, 0);
      glVertex2f(node->pose.pos.x, node->pose.pos.y);
      glColor3f(0, 1, 0);
      glVertex2f(node->pose.pos.x + 0.20 * cos(node->pose.rot),
                 node->pose.pos.y + 0.20 * sin(node->pose.rot));
      glEnd();
    
      if (true)
      {
        // Draw a rectnagle with text
        char text[256];
        snprintf(text, sizeof(text), "%d.%d.%d",
                 node->segmentId, node->laneId, node->waypointId);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glLineWidth(1.0);
        glPushMatrix();      
        glTranslatef(node->pose.pos.x, node->pose.pos.y, 0);
        glRotatef(node->pose.rot * 180/M_PI, 0, 0, 1);
        glRotatef(180, 0, 1, 0);
        glBegin(GL_QUADS);
        glVertex2f(-0.5, -0.5);
        glVertex2f(+0.5, -0.5);
        glVertex2f(+0.5, +0.5);
        glVertex2f(-0.5, +0.5);
        glEnd();
        drawText(0.05, text);
        glPopMatrix();
      }
    }
  }

  if (true) 
  {
    // Draw some important waypoints (stops, entry/exit)
    glPointSize(5);
    glBegin(GL_POINTS);
    for (i = 0; i < (int) nodes.size(); i++)
    {
      node = nodes[i];
      if (node->flags.isStop)
        glColor3f(1, 0, 0);
      else if (node->flags.isExit)
        glColor3f(1, 0.5, 0);
      else if (node->flags.isEntry)
        glColor3f(0, 1, 0);
      else
        continue;
      glVertex2f(node->pose.pos.x, node->pose.pos.y);
    }
    glEnd();
    
    // Draw arcs
    glBegin(GL_LINES);
    for (i = 0; i < (int) nodes.size(); i++)
    {
      node = nodes[i];

      for (j = 0; j < node->numNext; j++)
      {
        next = node->next[j];        

        if (!CHECK_MASK(includeFlags, next->flags))
          continue;
        if (CHECK_MASK(excludeFlags, next->flags))
          continue;

        if (node->flags.isOncoming || next->flags.isOncoming)
          glColor3f(1, 0, 0);
        else if (node->flags.isInfeasible)
          glColor3f(0.5, 0, 0);
        else if (node->flags.isLaneChange || next->flags.isLaneChange)
          glColor3f(0.5, 0.5, 0.5);
        else if (node->flags.isLane)
          glColor3f(1, 1, 1);
        else if (node->flags.isTurn)
          glColor3f(0, 0, 1);
        else
          glColor3f(0.7, 0.7, 0.7);
        
        glVertex2f(node->pose.pos.x, node->pose.pos.y);
        glVertex2f(next->pose.pos.x, next->pose.pos.y);
      } 
    }
    glEnd();
  }

  glEndList();
    
  return this->structList;
}


// Predraw the status information
int PlanGraph::predrawStatus(float px, float py, float size,
                             uint16_t includeFlags, uint16_t excludeFlags)
{
  int i;
  PlanGraphNode *node;
  PlanGraphNodeList nodes;
  
  // Create display list
  if (this->statusList == 0)
    this->statusList = glGenLists(1);
  glNewList(this->statusList, GL_COMPILE);

  // Get the nodes in the region; this is not as fast as recursion
  // through the tree, but is it simpler.
  this->getRegion(&nodes, px, py, size, size, includeFlags, excludeFlags);

  // Draw nodes
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  for (i = 0; i < (int) nodes.size(); i++)
  {
    node = nodes[i];

    // Pick a node color based on the current status
    if (node->status.obsCollision && this->isStatusFresh(node->status.obsTime))
    {
      glColor3f(1, 1, 0);
      if (node->status.obsFrontDist < 3.0)
        glColor3f(1, 1, node->status.obsFrontDist/3.0);
    }
    else if (node->status.carCollision && this->isStatusFresh(node->status.carTime))
    {
      glColor3f(1, 0.5, 0);
      if (node->status.carSideDist < 3.0)
        glColor3f(1, 0.5, node->status.carSideDist/3.0);
    }
    else
      continue;

    glPushMatrix();
    glTranslatef(node->pose.pos.x, node->pose.pos.y, -0.1);
    glRotatef(node->pose.rot*180/M_PI, 0, 0, 1);    
    
    glBegin(GL_POLYGON);
    glVertex2f(0, -0.25);
    glVertex2f(0, +0.25);
    glVertex2f(+1, 0);
    glEnd();
    
    glPopMatrix();
  }

  glEndList();
    
  return this->statusList;
}


#endif
