/*!
 * \file Console.hh
 * \brief Console header for the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#ifndef _CONSOLE_H
#define _CONSOLE_H

#define MAX_MSG        6
#define MSG_LENGTH     77

#include <ncurses.h>
#include <sstream>
#include <fstream>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>
#include <map/MapPrior.hh>
#include <pthread.h>
#include "sparrowhawk/SparrowHawk.hh"

class Console {

public:
  static void init();
  static void destroy();
  
  static void updateRate(double time);
  static void updateROA(bool ROA);
  static void updateTrajectory(CTraj *traj);
  static void updateState(VehicleState &vehState);
  static void updateInter(int CountPrecedence, string Status, PointLabel wp, double interTimer);
  static void updateInterPage2(vector<precedenceList_t> obstaclesPrecedence, vector<MapElement> obstaclesClearance, vector<MapElement> obstaclesMerging);
  static void updatePrediction(bool running);
  static void updateLastWpt(PointLabel last);
  static void updateNextWpt(PointLabel next);
  static void updateTurning(int direction);
  static void updateFSMFlag(string flag);
  static void updateFSMState(string state);
  static void updateFSMRegion(string region);
  static void updateFSMPlanner(string planner);
  static void updateFSMObstacle(string obstacle);
  static void updateStopline(double dist);
  static void addMessage(char *format, ...);
  static void addInterMessage(string message);
  static void resetIntersection();
  static bool queryResetIntersectionFlag();
  static void togglePrediction();
  static bool queryTogglePredictionFlag();
  static void toggleFailureHandling();
  static bool queryToggleFailureHandling();
  static void setFailureIndex(int failureIndex);
  static void resetState();
  static bool queryResetStateFlag();
  static void updateMapSize(int size);
  static void increaseObstSize();
  static void increaseClearSize();
  static void readPathPlanParams();
  static bool queryReadPathPlanParamsFlag();
  static void updateStoppedDuration(double stopped_duration);
  static void updateFinalCond(double x, double y);

  static CSparrowHawk* display;

private:
  static bool initialized;

  // variables for Rate
  static double rate1;
  static double rate2;

  // variables for ROA
  static int ROA;

  // variables for Trajectory
  static double traj_x;
  static double traj_y;
  static double traj_vel;
  static double fc_x;
  static double fc_y;

  // variables for Prediction
  static int pred_numbers;

  // variables for Failure Handling
  static int fail_on;
  static int fail_count;

  // variables for intersections
  static double distToStop;
  static double interTimer;

  // variables for Alice State
  static double state_velocity;
  static double stopped_duration;
  
  // variables for messages
  static char messages[MAX_MSG][MSG_LENGTH];

  // flags associated to keyboard commands in the console
  static bool resetIntersectionFlag;
  static bool togglePredictionFlag;
  static bool resetStateFlag;
  static bool readPathPlanParamsFlag;

  // debug map information
  static int mapSize;
  static int obstSize;
  static int clearSize;
};

/* Pointer to SparrowHawk accessible to all who include this file*/

/*! Stream used for all message printing in follower */
extern stringstream printStream;

/*! Function used to print messages either in the terminal or SparrowHawk window */
void printMessage();

#endif
