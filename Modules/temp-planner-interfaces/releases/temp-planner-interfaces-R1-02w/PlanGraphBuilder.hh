
/* 
 * Desc: Build a dense graph from an RNDF file.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_BUILDER_HH
#define PLAN_GRAPH_BUILDER_HH


/** @file

@brief Generate a graph from an RNDF file.  This class also manages
pre-compiled graph files.

*/


// Dependencies
#include "rndf/RoadGraph.hh"
#include "PlanGraph.hh"



/// @brief Generate a graph from an RNDF file.  This class also
/// manages pre-compiled graph files.
class PlanGraphBuilder
{
  public:

  /// @brief Constructor
  PlanGraphBuilder(PlanGraph *graph);

  /// @brief Destructor
  virtual ~PlanGraphBuilder();
  
  private:

  // Hide the copy constructor
  PlanGraphBuilder(const PlanGraphBuilder &that);
  
  public:

  /// @brief Set the vehicle kinematics.
  ///
  /// @param[in] wheelbase Distance between front and rear wheels (m).
  /// @param[in] maxSteer Maximum steering angle (radians).  
  int setKinematics(float wheelBase, float maxSteer);
  
  /// @brief Build graph from RNDF file.
  ///
  /// @param[in] filename RNDF file name.  
  int build(char *filename);

  /// @brief Load from pre-generated PG file.
  ///
  /// @param[in] filename PG file name.
  int load(char *filename);

  private:

  // Generate the graph
  int genGraph();

  // Generate the initial graph from the road graph
  int genLanes(int railId, float offset);

  // Generate turn maneuvers through intersections
  int genTurns();

  // Generate turn manuevers for the given waypoints
  int genTurnWaypoints(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB);

  // Generate a turn maneuver between the given nodes
  int genTurnNodes(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  // Generate feasible rail-change maneuvers.  Only the feasibility
  // info, not the full maneuver, is added to the graph.
  int genFeasibleChanges(PlanGraphQuad *quad = NULL);
  
  // Check for feasibility of an extended maneuver.
  bool checkFeasibleManeuver(pose2f_t poseA, pose2f_t poseB);

  // Check feasibility of a small maneuver.
  bool checkFeasibleStep(pose2f_t poseA, pose2f_t poseB);

  public:
  
  /// @brief Update the region of interest.
  ///
  /// This expands all nodes in the new ROI, and contracts any nodes
  /// in the old ROI.
  /// TODO: add a speed or curvature limit to control which nodes
  /// get expanded.
  ///
  /// @param[in] px,py Center of the ROI (site frame).
  /// @param[in] size Size of the ROI.
  int updateROI(float px, float py, float size);

  private:
  
  // Expand nodes in the ROI
  int expandROI(PlanGraphQuad *quad, float px, float py, float size, uint32_t index);

  // Contract nodes in the ROI
  int contractROI(PlanGraphQuad *quad, float px, float py, float size, uint32_t index);

  // Insert a maneuver between the given nodes.
  int insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, bool roi);

  // Remove a maneuver between the given nodes.
  int removeManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, bool roi);

  public:

  // Current graph we are building.
  PlanGraph *graph;
  
  private:

  // Road graph used during construction
  RoadGraph road;
  
  // Vehicle kinematic properties for feasibility checks.
  float wheelBase, maxSteer;

  // Number of rails for each lane
  int railCount;

  // Distance between rails
  float railSpacing;
  
  // Step size (node spacing) for maneuvers and feasibility checks.
  float spacing;

  // Current ROI index
  uint32_t roiIndex;

  // Current ROI region
  float roiPx, roiPy, roiSize;
};

#endif

