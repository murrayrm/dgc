
/* 
 * Desc: Generates a dense graph for planning.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_HH
#define PLAN_GRAPH_HH


/** @file

@brief PlanGraph maintains a dense graph of interpolated vehicle
configurations (poses) based on the RNDF data.  This graph is used
for planning.

*/

#include <stdio.h>
#include <vector>
#include "frames/pose2.h"
#include "rndf/RNDFGraph.hh"


/// @brief Data for each node in the graph (a vehicle configuration).
struct PlanGraphNode
{
  /// Unique node identifier 
  uint32_t nodeId;
  
  /// Pose in site frame
  pose2f_t pose;

  /// Nominal RNDF identifier.  Note that laneId is zero for zone
  /// perimeters.
  uint16_t segmentId, laneId, waypointId;

  /// Interpolation id (0 if the node is on top of a waypoint).
  uint16_t interId;

  /// Rail id (0 for the nominal center line).
  int8_t railId;

  /// Bit field of flags.  We make it a structure for saving and loading.
  struct
  {  
    /// Is this node at a stop line?
    uint8_t isStop : 1;
  
    /// Is this node an exit point?
    uint8_t isExit : 1;

    /// Is this node an entry point?
    uint8_t isEntry : 1;
        
    /// Is this node in an intersection?
    uint8_t isTurn : 1;

    /// Is this node part of a lane-change?
    uint8_t isChange : 1;

    /// Is this node on the wrong side of the road?
    uint8_t isOncoming : 1;

  } flags;

  // Corresponding waypoints in the RNDF.  The next and previous
  // waypoint will be the same if the node corresponds exactly to an
  // RNDF waypoint.
  RNDFGraphWaypoint *nextWaypoint, *prevWaypoint;
  
  // Next nodes(s) in the graph
  int16_t numNext, maxNext;
  PlanGraphNode **next;

  // Previous nodes(s) in the graph
  int16_t numPrev, maxPrev;
  PlanGraphNode **prev;

  // ROI index is non-zero if the node is in the region of interest
  // and expanded.  This needs to be a counter to enable incremental
  // expansion and contraction.
  uint32_t roiIndex;
  
  // Feasible destination nodes (i.e., distant nodes to which there
  // are feasible maneuvers).
  int16_t numDest, maxDest;
  PlanGraphNode **dest;

  // If this is an expanded node (generated in the ROI only), this
  // field points to the current destination node.  An optimization
  // for removing stuff from the ROI.
  PlanGraphNode *currentDest;

  // Pointer for user data
  void *data;
};


/// @brief A dynamically allocated list of nodes, use for querying
/// regions of the quad-tree.
typedef std::vector<PlanGraphNode*> PlanGraphNodeList;


/// @brief Quad-tree node.
///
/// This must be simple structure (with no functions) to allow fast
/// memory allocation and initialization (calloc).
struct PlanGraphQuad
{
  // Quad center
  float px, py;

  // Quad size
  float size;

  // Leaves from this quad
  PlanGraphQuad *leaves[4];

  // Nodes in this quad
  int numNodes, maxNodes;
  PlanGraphNode **nodes;
  
  // ROI nodes in this quad
  int numRoiNodes, maxRoiNodes;
  PlanGraphNode **roiNodes;

  /// Check if quad contains the given point.
  bool hasPoint(float px, float py)
    {
      if (px > this->px + this->size/2)
        return false;
      if (px < this->px - this->size/2)
        return false;
      if (py > this->py + this->size/2)
        return false;
      if (py < this->py - this->size/2)
        return false;
      return true;
    };


  /// Check for overlap between a quad and the requested region.  
  bool hasIntersection(float px, float py, float size)
    {
      if (px - size/2 > this->px + this->size/2)
        return false;
      if (px + size/2 < this->px - this->size/2)
        return false;
      if (py - size/2 > this->py + this->size/2)
        return false;
      if (py + size/2 < this->py - this->size/2)
        return false;
      return true;
    };
};


/// @brief PlanGraph maintains a dense graph of interpolated vehicle
/// configurations (poses) based on the RNDF data.  This graph is used
/// for planning.
///
/// The graph is defined in the site frame (identical to UTM apart
/// from a translation).
///
class PlanGraph
{
  public:

  /// @brief Constructor
  PlanGraph();

  /// @brief Destructor
  virtual ~PlanGraph();
  
  private:

  // Hide the copy constructor
  PlanGraph(const PlanGraph &that);
  
  public:

  /// @brief Get the canonical node corresponding to the given checkpoint ID
  PlanGraphNode *getCheckpoint(int checkpointId);

  /// @brief Get the canonical node corresponding to the given RNDF ID
  PlanGraphNode *getWaypoint(int segmentId, int laneId, int waypointId);

  /// @brief Get the node nearest the given position
  PlanGraphNode *getNearestPos(float px, float py);

  /// @brief Get all nodes in a square region.
  ///
  /// This function gets all nodes in overlapping quads, so the region
  /// will be slightly expanded.
  ///
  /// @param[out] nodes Node list to be filled (this is an STL vector).
  /// @param[in] px,py Center of the square.
  /// @param[in] size  Size (width and height) of the region.
  int getRegion(PlanGraphNodeList *nodes, float px, float py, float size);  
  
  private:
  
  // Get all nodes in a square region (recursive form)
  int getRegionQuad(PlanGraphNodeList *nodes, PlanGraphQuad *quad, float px, float py, float size);
  
  public:
  
  /// Allocate a node
  PlanGraphNode *allocNode(float px, float py);

  /// Free a node
  void freeNode(PlanGraphNode *node);

  /// Allocate a node in the ROI
  PlanGraphNode *allocRoiNode(float px, float py);

  /// Free a node in the ROI
  void freeRoiNode(PlanGraphNode *node);

  /// Insert an arc
  int insertArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  /// Remove an arc
  int removeArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);
  
  /// Insert a feasible arc
  int insertFeasibleArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  /// Remove a feasible arc
  int removeFeasibleArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  // Allocate a new quad or return an existing one for the given point.
  PlanGraphQuad *allocQuad(PlanGraphQuad *quad, float px, float py);

  /// Free a quad and all the nodes in it
  void freeQuad(PlanGraphQuad *quad);

  /// Get the quad nearest the given position
  PlanGraphQuad *getNearestQuad(PlanGraphQuad *quad, float px, float py);

  public:
  
  // Save to a binary file
  int save(const char *filename);

  // Load from a binary file
  int load(const char *filename);
  
  private:
  
  // Write a quad (recursive form)
  int writeQuadNodes(FILE *file, PlanGraphQuad *quad);

  // Write quad arcs (recursive form)
  int writeQuadArcs(FILE *file, PlanGraphQuad *quad);

  // Read quad nodes (recursive form)
  int readQuadNodes(FILE *file, PlanGraphQuad *quad);

  // Read quad arcs (recursive form)
  int readQuadArcs(FILE *file, PlanGraphQuad *quad);

  // Get a node by id and position
  PlanGraphNode *getNodeId(uint32_t nodeId, float px, float py);

  public:

  // Underlying RNDF
  RNDFGraph rndf;

  // Minimum linear dimension for quad tree elements.
  float quadScale;
  
  // Root of the quad-tree
  PlanGraphQuad *root;

  // Stats on memory usage
  int quadCount, quadBytes, nodeCount, nodeBytes;
    
#if USE_GL
  public:

  /// Predraw the plan graph centered around some point
  /// @param[in] px,py Center point of region of interest (site frame).
  /// @param[in] size Size of region of interest (m).
  /// @return On success, returns the display list.
  int predraw(float px, float py, float size);

  private:

  // Recursively predraw the given quad
  int predrawQuad(PlanGraphQuad *quad, float px, float py, float size);
    
  // GL display list
  int planList;

  // Last configuration drawn
  float planListPx, planListPy, planListSize;
  
#endif
};

#endif

