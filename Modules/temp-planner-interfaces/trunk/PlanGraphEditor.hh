/* 
 * Desc: PlanGraph editor utility
 * Date: 25 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_EDITOR_HH
#define PLAN_GRAPH_EDITOR_HH


#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <dgcutils/Fl_Glv_Window.H>

#include <rndf/DrawMisc.hh>
#include <rndf/DrawAerial.hh>
#include <rndf/RNDFGraph.hh>
#include <rndf/RoadGraph.hh>
#include <temp-planner-interfaces/PlanGraph.hh>
#include <temp-planner-interfaces/PlanGraphBuilder.hh>

#include "plangraph_editor_cmdline.h"


class PlanGraphEditor
{
  public:

  // Default constructor
  PlanGraphEditor();

  // Destructor
  ~PlanGraphEditor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  // Initialize GUI
  int initGUI(int cols, int rows);

  // Finalize GUI
  int finiGUI();
 
  public:
    
  // Exit callback
  static void onExit(Fl_Widget *w, int option);

  // Action callback
  static void onAction(Fl_Widget *w, int option);

  // Handle draw callbacks
  static void onDraw(Fl_Glv_Window *win, PlanGraphEditor *self);

  // Handle idle callbacks
  static void onIdle(PlanGraphEditor *self);

  public:

  // Initialize the viewer
  int init();

  // Finalize the viewer
  int fini();

  // Re-load config file
  int reload();

  // Update the viewer
  int update();

  // Re-load the road graph
  int reloadRoad();

  // Re-load the plan graph
  int reloadPlan();
  
  public:

  // MENU options
  enum
  {
    CMD_ACTION_PAUSE = 0x1000,
    CMD_ACTION_RELOAD = 0x1001,

    CMD_VIEW_FIRST  = 0x2000,
    CMD_VIEW_AERIAL = 0x00,
    CMD_VIEW_RNDF   = 0x01,
    CMD_VIEW_ROAD   = 0x02,
    CMD_VIEW_PLAN   = 0x03,
    CMD_VIEW_LAST   = 0x20FF,
    
    CMD_GRAPH_FIRST         = 0x2100,
    CMD_GRAPH_NODES         = 0x00,
    CMD_GRAPH_RAIL_CHANGES  = 0x01,
    CMD_GRAPH_LANE_CHANGES  = 0x02,
    CMD_GRAPH_LAST          = 0x21FF,
  };
  
  // Top-level window
  Fl_Window *mainwin;

  // Top menu bar
  Fl_Menu_Bar *menubar;
  
  // 3D window
  Fl_Glv_Window *worldwin;

  // Should we pause?
  bool pause;
  
  // Should we quit?
  bool quit;
  
  // Command-line options
  struct plangraph_editor_cmdline cmdline;
  
  // Graph generator
  PlanGraphBuilder *builder;

  // The RNDF graph
  RNDFGraph *rndf;
  
  // The road graph
  RoadGraph *road;

  // The plan graph
  PlanGraph *plan;

  // Center of the ROI
  vec2f_t roiPos;
  
  // Size of the ROI
  float roiSize;
  
  // Are the display lists dirty?
  bool dirty;

  // Has the initial ROI position been set?
  bool initialROI;
  
  // Which layers are we viewing?  This is a bit-mask, with each bit
  // position denoting a seperate layer that is on or off.
  int viewLayers;

  // Which graph properties are enabled?  A bit-mask.
  int planProps;

  // Drawing tools
  DrawMisc drawMisc;
  DrawAerial drawAerial;
  
  // Display lists 
  GLuint gridList, rndfList, roadList, planList;
};

#endif
