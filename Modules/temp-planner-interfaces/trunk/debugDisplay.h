/*
 * debugDisplay.dd - SparrowHawk display for planner
 *
 * Christian Looman
 * 13 September 2007
 *
 */

extern long long sparrowhawk;

#define D(x)    (sparrowhawk)



/* Object ID's (offset into table) */


#ifdef __cplusplus
extern "C" {
#endif

/* Allocate space for buffer storage */
static char debugbuf[20];

static DD_IDENT debugtable[] = {
{1, 1, (void *)("+- Debug information -------------------------------------------------------+"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{2, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 1, (void *)("| Planner map size     :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 1, (void *)("| Obstacles received   :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{4, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 1, (void *)("| ClearObst received   :"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{5, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{6, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{7, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{8, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{9, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{10, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{11, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{12, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{13, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{14, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{15, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 1, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{16, 77, (void *)("|"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{17, 1, (void *)("+---------------------------------------------------------------------------+"), dd_label, "NULL", (char *) NULL, 0, dd_nilcbk, (long)(long) 0, 0, 0, Label, "", -1},
{3, 26, (void *)(&(D(mapsize))), dd_short, "%i", debugbuf+0, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(mapsize)", -1},
{4, 26, (void *)(&(D(obstsize))), dd_short, "%i", debugbuf+8, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(obstsize)", -1},
{5, 26, (void *)(&(D(clearsize))), dd_short, "%i", debugbuf+16, 1, dd_nilcbk, (long)(long) 0, 0, 0, Data, "D(clearsize)", -1},
DD_End};

#ifdef __cplusplus
}
#endif

