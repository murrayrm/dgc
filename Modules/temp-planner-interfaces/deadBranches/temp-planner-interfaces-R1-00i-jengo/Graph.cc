
/* 
 * Desc: Trajectory graph data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#include "Graph.hh"


// Error handling
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Default constructor
Graph::Graph(int maxNodes, int maxArcs)
{
  this->numNodes = 0;
  this->maxNodes = maxNodes;
  this->nodes = new GraphNode[this->maxNodes];

  this->numArcs = 0;
  this->maxArcs = maxArcs;
  this->arcs = new GraphArc[this->maxArcs];

  this->numStaticNodes = 0;
  this->numStaticArcs = 0;

  this->gridReady = false;
  this->gridRes = 4.0; // MAGIC
  this->gridSx = (int) (3000 / this->gridRes); // MAGIC
  this->gridSy = (int) (3000 / this->gridRes); // MAGIC
  this->gridOx = 0;
  this->gridOy = 0;
  this->grid = (GraphCell*) calloc(this->gridSx * this->gridSy, sizeof(this->grid[0]));
  
  for (int i=0; i<maxNodes; i++){
    this->nodes[i].railId = 1;
  }
  return;
}


// Destructor
Graph::~Graph()
{
  free(this->grid);
  delete [] this->arcs;
  delete [] this->nodes;
  
  return;
}


// Get the number of nodes
int Graph::getNodeCount()
{
  return this->numNodes;
}


// Get a node from an index
GraphNode *Graph::getNode(int index)
{
  if (index < 0 || index >= this->numNodes)
    return NULL;
  return this->nodes + index;
}


// Create a new node
GraphNode *Graph::createNode()
{
  GraphNode *node;

  if (this->numNodes >= this->maxNodes)
    return NULL;

  // Create the node
  node = this->nodes + this->numNodes;
  memset(node, 0, sizeof(*node));  
  node->index = this->numNodes;
  this->numNodes++;
  
  return node;
}



// Get the number of arcs
int Graph::getArcCount()
{
  return this->numArcs;
}


// Get an arc from a node and arc index
GraphArc *Graph::getArc(int index)
{
  // Make sure the arc exists
  if (index < 0 || index >= this->numArcs)
    return NULL;
  return this->arcs + index;
}


// Create an arc between two nodes
GraphArc *Graph::createArc(int indexa, int indexb)
{
  GraphNode *nodeA, *nodeB;
  GraphArc *arc;
  double dist;

  // Make sure both nodes exist
  nodeA = this->getNode(indexa);
  if (!nodeA)
    return NULL;
  nodeB = this->getNode(indexb);
  if (!nodeB)
    return NULL;

  // Check for an existing arc between these nodes
  for (arc = nodeA->outFirst; arc != NULL; arc = arc->outNext)
  {
    if (arc->nodeB == nodeB)
      return arc;
  }

  if (this->numArcs >= this->maxArcs)
    return NULL;

  // Create a new arc
  arc = this->arcs + this->numArcs;
  memset(arc, 0, sizeof(*arc));  
  arc->index = this->numArcs++;
  arc->nodeA = nodeA;
  arc->nodeB = nodeB;

  // Set the default plan distance, in mm
  dist = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
  arc->planDist = (int) (dist * 1000);
  assert(arc->planDist > 0);

  // Update the linked list for outgoing arcs from nodeA
  this->insertOutArc(nodeA, arc);

  // Update the linked list for incoming arcs from nodeB
  this->insertInArc(nodeB, arc);

  //MSG("arc %d %d : %d %d", indexa, indexb,
  //    this->numOutArcs[indexa],
  //    this->numInArcs[indexb]);
  
  return arc;
}


// Insert arc into list of outgoing arcs for this node
int Graph::insertOutArc(GraphNode *node, GraphArc *arc)
{
  arc->outPrev = node->outLast;
  if (!node->outFirst)
  {
    node->outFirst = arc;
    node->outLast = arc;
  }
  else if (node->outLast)
  {
    node->outLast->outNext = arc;
    node->outLast = arc;
  }
  
  return 0;
}


// Remove arc from list of outgoing arcs for this node
int Graph::removeOutArc(GraphNode *node, GraphArc *arc)
{
  if (arc->outPrev)
    arc->outPrev->outNext = arc->outNext;
  if (arc->outNext)
    arc->outNext->outPrev = arc->outPrev;

  if (node->outFirst == arc)
    node->outFirst = arc->outNext;
  if (node->outLast == arc)
    node->outLast = arc->outPrev;

  return 0;
}


// Insert arc into list of ingoing arcs for this node
int Graph::insertInArc(GraphNode *node, GraphArc *arc)
{
  arc->inPrev = node->inLast;
  if (!node->inFirst)
  {
    node->inFirst = arc;
    node->inLast = arc;
  }
  else if (node->inLast)
  {
    node->inLast->inNext = arc;
    node->inLast = arc;
  }
  
  return 0;
}


// Remove arc from list of ingoing arcs for this node
int Graph::removeInArc(GraphNode *node, GraphArc *arc)
{
  if (arc->inPrev)
    arc->inPrev->inNext = arc->inNext;
  if (arc->inNext)
    arc->inNext->inPrev = arc->inPrev;

  if (node->inFirst == arc)
    node->inFirst = arc->inNext;
  if (node->inLast == arc)
    node->inLast = arc->inPrev;

  return 0;
}



// Get a node from an RNDF id
GraphNode *Graph::getNodeFromRndfId(int segmentId, int laneId, int waypointId)
{
  int i;
  GraphNode *node;

  for (i = 0; i < this->numNodes; i++)
  {
    node = this->nodes + i;
    if (node->isWaypoint &&
        node->segmentId == segmentId &&
        node->laneId == laneId &&
        node->waypointId == waypointId)
      return node;
  }
  
  return NULL;
}


// Get a node from a checkpoint id  
GraphNode *Graph::getNodeFromCheckpointId(int checkpointId)
{
  int i;
  GraphNode *node;
  for (i = 0; i < this->numNodes; i++)
  {
    node = this->nodes + i;
    if (node->isCheckpoint && node->checkpointId == checkpointId)
      return node;
  }  
  return NULL;
}


// Get the next node for lane following
GraphNode *Graph::getNextLaneNode(const GraphNode *node)
{
  GraphArc *arc;
  
  for (arc = node->outFirst; arc != NULL; arc = arc->outNext)
  {
    if (arc->nodeB->type == GRAPH_NODE_LANE)
      return arc->nodeB;
  }
  
  return NULL;
}


// Get the previous node for lane following
GraphNode *Graph::getPrevLaneNode(const GraphNode *node)
{
  GraphArc *arc;

  for (arc = node->inFirst; arc != NULL; arc = arc->inNext)
  {
    if (arc->nodeA->type == GRAPH_NODE_LANE)
      return arc->nodeA;
  }
  
  return NULL;
}


// Insert a node into a cell list
int Graph::insertNodeIntoCell(GraphCell *cell, GraphNode *node)
{
  // Update the linked list for this cell
  node->cellPrev = cell->cellLast;
  if (!cell->cellFirst)
  {
    cell->cellFirst = node;
    cell->cellLast = node;
  }
  else if (cell->cellLast)
  {
    cell->cellLast->cellNext = node;
    cell->cellLast = node;
  }
  
  return 0;
}


// Remove a node from a cell list
int Graph::removeNodeFromCell(GraphCell *cell, GraphNode *node)
{
  if (node->cellPrev)
    node->cellPrev->cellNext = node->cellNext;
  if (node->cellNext)
    node->cellNext->cellPrev = node->cellPrev;

  if (cell->cellFirst == node)
    cell->cellFirst = node->cellNext;
  if (cell->cellLast == node)
    cell->cellLast = node->cellPrev;

  node->cellNext = NULL;
  node->cellPrev = NULL;
    
  return 0;
}
                              
  
// Update the node pose
int Graph::setNodePose(GraphNode *node, pose3_t pose)
{
  int gx, gy;
  GraphCell *cell;
 
  /* Sven's hack */
  // Initialize the grid offset if not already set
  if (!this->gridReady)
  {
    this->gridOx = (int) (pose.pos.x / this->gridRes) - this->gridSx / 2;
    this->gridOy = (int) (pose.pos.y / this->gridRes) - this->gridSy / 2;
    this->gridReady = true;
  }

  // Remove node from the cell it is in now
  if (node->cell)
  {
    this->removeNodeFromCell(node->cell, node);
    node->cell = NULL;
  }
    
  // Get the cell at the current location
  gx = (int) (pose.pos.x / this->gridRes) - this->gridOx;
  gy = (int) (pose.pos.y / this->gridRes) - this->gridOy;
  assert(gx >= 0 && gx < this->gridSx);
  assert(gy >= 0 && gy < this->gridSy);  
  cell = this->grid + (gx + gy * this->gridSx);

  // Add node to this new cell
  node->cell = cell;
  this->insertNodeIntoCell(node->cell, node);
  
  return 0;
}


// Get the nearest node for the given type mask
GraphNode *Graph::getNearestNode(vec3_t pos, int typeMask, double maxDist)
{
  int ox, oy, nx, ny, gx, gy, rad;
  GraphCell *cell;
  GraphNode *node, *minNode;
  double dx, dy, dD, minD;

  minD = DBL_MAX;
  minNode = NULL;

  // Find the cell at the given position
  ox = (int) (pos.x / this->gridRes) - this->gridOx;
  oy = (int) (pos.y / this->gridRes) - this->gridOy;

  // Search radius, in grid cells
  rad = (int) (maxDist / this->gridRes + 0.5);

  // Search in the neighborhood of this cell
  for (ny = -rad; ny <= +rad; ny++)
  {
    for (nx = -rad; nx <= +rad; nx++)
    {
      gx = ox + nx;
      gy = oy + ny;
      if (gx < 0 || gx >= this->gridSx)
        continue;
      if (gy < 0 || gy >= this->gridSy)
        continue;
      cell = this->grid + (gx + gy * this->gridSx);

      // Look at all nodes in this cell
      for (node = cell->cellFirst; node != NULL; node = node->cellNext)
      {
        if ((node->type & typeMask) == 0)
          continue;

        dx = node->pose.pos.x - pos.x;
        dy = node->pose.pos.y - pos.y;
        dD = dx * dx + dy * dy;

        if (dD < minD)
        {
          minD = dD;
          minNode = node;
        }        
      }      
    }
  }
  
  return minNode;
}



// Freeze the current graph.
void Graph::freezeStatic()
{
  this->numStaticNodes = this->numNodes;
  this->numStaticArcs = this->numArcs;
  
  return;
}


// Get the number of static nodes.
int Graph::getStaticNodeCount()
{
  return this->numStaticNodes;
}


// Clear volatile components.
void Graph::clearVolatile()
{
  GraphNode *node;
  GraphArc *arc;

  // Remove any volatile arcs by clipping them from the linked lists.
  while (this->numArcs > this->numStaticArcs)
  {
    arc = this->arcs + this->numArcs - 1;
    this->removeOutArc(arc->nodeA, arc);
    this->removeInArc(arc->nodeB, arc);
    this->numArcs -= 1;
  }

  // Remove volatile nodes one at a time
  while (this->numNodes > this->numStaticNodes)
  {
    node = this->nodes + this->numNodes - 1;
    if (node->cell)
    {
      this->removeNodeFromCell(node->cell, node);
      node->cell = NULL;
    }
    this->numNodes -= 1;
  }
  

  /* REMOVE
     int i;
     GraphArc *arc;
  
     // Destroy any volatile arcs.  We have do this explitly, since there
     // may be arcs between static and volatile nodes.  For the sake of
     // speed, we simply clip them out of the linked lists.
     for (i = this->numStaticArcs; i < this->numArcs; i++)
     {
     arc = &this->arcs[i];

     if (arc->outPrev)
     arc->outPrev->outNext = arc->outNext;
     if (arc->outNext)
     arc->outNext->outPrev = arc->outPrev;

     if (arc->nodeA->outFirst == arc)
     arc->nodeA->outFirst = arc->outNext;
     if (arc->nodeA->outLast == arc)
     arc->nodeA->outLast = arc->outPrev;
    
     if (arc->inPrev)
     arc->inPrev->inNext = arc->inNext;
     if (arc->inNext)
     arc->inNext->inPrev = arc->inPrev;
    
     if (arc->nodeB->inFirst == arc)
     arc->nodeB->inFirst = arc->inNext;
     if (arc->nodeB->inLast == arc)
     arc->nodeB->inLast = arc->inPrev;
     }
  
     this->numNodes = this->numStaticNodes;
     this->numArcs = this->numStaticArcs;
  */
  
  return;
}


