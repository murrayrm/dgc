
/* 
 * Desc: Trajectory graph data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_HH
#define GRAPH_HH

#include <stdint.h>
#include <frames/pose3.h>
#include <vector>

class Quadtree;

// Foward declarations
struct GraphNode;
struct GraphArc;

// Maximum value for the plan cost
#define GRAPH_PLAN_COST_MAX 0x7FFFFFFF

// Types for nodes
enum GraphNodeType
{
  GRAPH_NODE_LANE = 0x01,
  GRAPH_NODE_TURN = 0x02,
  GRAPH_NODE_CHANGE = 0x04,
  GRAPH_NODE_KTURN = 0x08,
  GRAPH_NODE_VOLATILE = 0x10,
  GRAPH_NODE_LOCAL_CHANGE = 0x20,
  GRAPH_NODE_ZONE = 0x40
};

/// @brief Enumerated list of different collision types.
enum
{
  GRAPH_COLLIDE_NONE = 0,
  GRAPH_COLLIDE_LANE_OUTER,
  GRAPH_COLLIDE_LANE_INNER,
  GRAPH_COLLIDE_MAP_OUTER,
  GRAPH_COLLIDE_MAP_INNER,
};

enum GraphArcType
{
  GRAPH_ARC_WAYPOINT = 1,
  GRAPH_ARC_NODE     = 2,
};

enum PathDirection
{
  GRAPH_PATH_FWD,
  GRAPH_PATH_REV,
};

/// @brief Class for storing a single node in the graph.
struct GraphNode
{
  // Node type
  uint8_t type;
  
  // Unique node index
  int index;

  // Start and end of the linked list of outward arcs
  GraphArc *outFirst, *outLast;

  // Start and end of the linked list of inwards arcs
  GraphArc *inFirst, *inLast;

  // For maneuvers node (start and end node of the maneuver)
  GraphNode *startNode, *endNode;

  // For lane node (left and right nodes, for rails)
  GraphNode *leftNode, *rightNode;
  
  // Direction (with traffic +1 or against traffic -1)
  signed char direction;

  // Path direction that we want to drive (fwd or rev)
  PathDirection pathDir;

  // Segment id
  uint8_t segmentId;
  
  // Lane id
  uint8_t laneId;

  // Rail id
  signed char railId;

  // Waypoint id (waypoints only)
  uint16_t waypointId;

  // Dimensions of the lane element (width across the lane).
  float laneWidth;

  // Distance to center-line of lane (volatile nodes only)
  float centerDist;
  
  // Node pose 
  pose3_t pose;

  // Transform from global to node frame.  This is an optimization
  // for configuration space computations.
  float transNG[4][4];

  // Transform from node to global frame.  This is an optimization
  // for configuration space computations.
  float transGN[4][4];

  // Steering angle
  float steerAngle;

  // How fast is the car driving along the lane (>0 means in lane dir)
  float carVel;

  // Plan cost
  int planCost;

  // If isStop is true, distToStop is set to be the distance from the node to the actual stopline
  float distToStop;

  // Is this a waypoint?
  bool isWaypoint :1;
  
  // Is this an entry point?
  bool isEntry :1;

  // Is this an exit point?
  bool isExit :1;

  // Is this a stop line?
  bool isStop :1;

  // Is this node off the road?
  bool offRoad :1;

  // Does this node collide with an static obstacle?
  uint8_t collideObs :1;

  // Does this node collide with a non-static car?
  uint8_t collideCar :1;


} __attribute__((packed));


/// @brief Class for storing a single arc in the graph.
struct GraphArc
{
  // Unique arc index
  int index;
    
  // Nodes joined by this arc
  GraphNode *nodeA, *nodeB;

  // Prev/next arcs in the outwards arcs for nodeA
  GraphArc *outPrev, *outNext;

  // Prev/next arcs in the inwards arcs for nodeB
  GraphArc *inPrev, *inNext;
  
  // Distance between nodes, in mm (an optimization for planning)
  int planDist;

  // Type of the arc for multi-resolution
  uint8_t type;

  // Was this are ignored in the backward search
  bool ignored;
};

/// @brief Class for storing trajectory graphs.
class Graph_t
{
public:
  
  /// Default constructor
  Graph_t(int maxNodes, int maxArcs);

  /// Destructor
  virtual ~Graph_t();

public:

  int type;

  // Get the number of nodes
  int getNodeCount();
  
  // Get a node from an node index
  GraphNode *getNode(int index);

  // Create a new node
  GraphNode *createNode();

  // Get the number of arcs
  int getArcCount();
  
  // Get an arc from an arc index
  GraphArc *getArc(int index);

  // Create an arc between two nodes
  GraphArc *createArc(int indexa, int indexb);
  
public:

  // Insert arc into list of outgoing arcs for this node
  int insertOutArc(GraphNode *node, GraphArc *arc);

  // Remove arc from list of outgoing arcs for this node
  int removeOutArc(GraphNode *node, GraphArc *arc);

  // Insert arc into list of ingoing arcs for this node
  int insertInArc(GraphNode *node, GraphArc *arc);

  // Remove arc from list of ingoing arcs for this node
  int removeInArc(GraphNode *node, GraphArc *arc);

public:

  // Flat list of nodes
  int numNodes, maxNodes;
  GraphNode *nodes;

  // Flat list of arcs
  int numArcs, maxArcs;
  GraphArc *arcs;

public:

  /// @brief Get the next node for lane following
  GraphNode *getNextChangeNode(const GraphNode *node);

  /// @brief Get the previous node for lane following
  GraphNode *getPrevChangeNode(const GraphNode *node);

  /// @brief Get the next node for lane following
  GraphNode *getNextLaneNode(const GraphNode *node);

  /// @brief Get the previous node for lane following
  GraphNode *getPrevLaneNode(const GraphNode *node);

  /// @brief Get a node from an RNDF id
  GraphNode *getNodeFromRndfId(int segmentId, int laneId, int waypointId);

public:
  
  /// @brief Get the nearest node for the given type mask using spatial indexing.
  GraphNode *getNearestNode(vec3_t pos, int typeMask, double maxDist);

  // @brief Get the nearest node with the given direction
  GraphNode *getNearestNode(vec3_t pos, int typeMask, double maxDist, int direction);
  
  // Quadtree for fast spatial indexing
  bool is_quadtree_initialized;
  Quadtree *quadtree;

public:

  /// @brief Freeze the current graph.
  ///
  /// Call this function when all the static components have been added to
  /// the graph.  Any future additions will be volatile, meaning they can
  /// be erased with the ::clearVolatile function.
  void freezeStatic();
  
  /// @brief Get the number of static nodes.
  int getStaticNodeCount();

  /// @brief Clear volatile components.
  ///
  /// Remove any elements added since ::freezeStatic was called.
  void clearVolatile();

private:

  // Size of the static part of the graph (and the start of the
  // volatile part).
  int numStaticNodes, numStaticArcs;
  std::vector<GraphNode *> waypoints;

public:
  // Vehicle node in graph
  GraphNode *vehicleNode;  

};

#endif
