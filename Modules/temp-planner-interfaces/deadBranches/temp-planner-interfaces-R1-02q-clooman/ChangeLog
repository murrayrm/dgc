Wed Oct  3 16:43:13 2007	Christian Looman (clooman)

	* version R1-02q-clooman
	BUGS:  
	FILES: Console.cc(42606), Console.hh(42606),
		plannerDisplay.dd(42606), plannerDisplay.h(42606)
	FailureHandler counter replaced by FailureHandler index

Tue Oct  2 15:43:40 2007	Sven Gowal (sgowal)

	* version R1-02q
	BUGS:  
	FILES: AliceStateHelper.cc(42295), AliceStateHelper.hh(42295),
		Graph.cc(42295), Graph.hh(42295), Quadtree.cc(42295),
		Utils.cc(42295)
	Merged

	FILES: AliceStateHelper.cc(42288), AliceStateHelper.hh(42288),
		Graph.cc(42288), Graph.hh(42288), Quadtree.cc(42288),
		Utils.cc(42288)
	Modified AliceStateHelper to return site frame coordinates

Tue Oct  2 11:57:48 2007	Noel duToit (ndutoit)

	* version R1-02p
	BUGS:  
	FILES: Makefile.yam(42218), Utils.cc(42218), Utils.hh(42218)
	Moved the display and print functions from the respective libraries
	into Utils. This should help with the unit-test dependencies.

Tue Oct  2  8:12:10 2007	Christian Looman (clooman)

	* version R1-02o
	BUGS:  
	FILES: Console.cc(42162), Console.hh(42162),
		intersectionDisplay.dd(42162)
	commit after merge

	FILES: Console.cc(42155), Console.hh(42155),
		intersectionDisplay.dd(42155)
	changed intersection display. SAFETY and CABMODE FLAG are not
	displayed any longer. new message field on intersection page added

Tue Oct  2  0:40:16 2007	Andrew Howard (ahoward)

	* version R1-02n
	BUGS:  
	FILES: Graph.hh(42125)
	Slight mod for site frame compatability

Sun Sep 30 23:29:46 2007	Andrew Howard (ahoward)

	* version R1-02m
	BUGS:  
	FILES: planner_cmdline.ggo(42004)
	Added Noel's recent options

Sun Sep 30 22:26:53 2007	Andrew Howard (ahoward)

	* version R1-02l
	BUGS:  
	New files: planner_cmdline.ggo
	FILES: Graph.hh(41984), Makefile.yam(41984)
	Merged branches

	New files: planner_cmdline.ggo
	FILES: Graph.hh(41610), Makefile.yam(41610)
	Move command line options to this module (so they can be shared by
	other planner modules).

	FILES: Graph.hh(41925)
	Tweaks

Sun Sep 30 18:46:27 2007	Kenny Oslund (kennyo)

	* version R1-02k
	BUGS:  
	FILES: PlannerInterfaces.h(41929)
	added errors for s1planner to report- start blocked goal blocked
	path cost too high

Sun Sep 30 13:53:33 2007	Noel duToit (ndutoit)

	* version R1-02j
	BUGS:  
	FILES: CmdArgs.cc(41858), CmdArgs.hh(41858)
	added use-hacked-uturn and use-hacked-backup to cmdargs.

Sat Sep 29 17:51:22 2007	vcarson (vcarson)

	* version R1-02i
	BUGS:  
	FILES: CmdArgs.cc(41595), CmdArgs.hh(41595), Console.cc(41595),
		Console.hh(41595), plannerDisplay.dd(41595),
		plannerDisplay.h(41595)
	Added failure handling info to the planner console. 

Sat Sep 29 12:17:32 2007	Nok Wongpiromsarn (nok)

	* version R1-02h
	BUGS:  
	FILES: Utils.cc(41474)
	Fix from Sven, in getCurrentLane function, check that the node is not NULL so we don't segfault. If the node is NULL, return laneId = 0, segmentId = 0.

Sat Sep 29 10:56:36 2007	Nok Wongpiromsarn (nok)

	* version R1-02g
	BUGS:  
	FILES: PlannerInterfaces.h(41305)
	GU_UPDATE_FROM_MAP_ERROR is fixed to 4096. (It was 5096 earlier.)
	Specify the type of failure to mplanner.

Fri Sep 28 16:38:27 2007	Noel duToit (ndutoit)

	* version R1-02f
	BUGS:  
	New files: ConfigFile.cc ConfigFile.hh
	FILES: CmdArgs.cc(41179), CmdArgs.hh(41179), Console.cc(41179),
		Console.hh(41179), Makefile.yam(41179),
		PlannerInterfaces.h(41179), plannerDisplay.dd(41179),
		plannerDisplay.h(41179)
	merging with the latest release.

	New files: ConfigFile.cc ConfigFile.hh
	FILES: CmdArgs.cc(41121), CmdArgs.hh(41121), Makefile.yam(41121)
	Added the config files for path-planner to the CmdArgs. Added
	ConfigFile.* that is a useful tool to parse config files.

	FILES: Console.cc(41147), Console.hh(41147),
		PlannerInterfaces.h(41147), plannerDisplay.dd(41147),
		plannerDisplay.h(41147)
	added a bound key (u) to the console to specify when we want to
	reread path planner params from file.

Fri Sep 28 15:59:58 2007	Christian Looman (clooman)

	* version R1-02e
	BUGS:  
	FILES: PlannerInterfaces.h(41143), Utils.cc(41143), Utils.hh(41143)
	monitorAliceSpeed takes into account when Estop is paused/disabled

Thu Sep 27 16:51:17 2007	Sven Gowal (sgowal)

	* version R1-02d
	BUGS:  
	FILES: Console.cc(40886), Graph.hh(40886)
	Changed the way the mean average gets computed.

Thu Sep 27 10:13:56 2007	Sven Gowal (sgowal)

	* version R1-02c
	BUGS:  
	FILES: Graph.cc(40786), Graph.hh(40786), Utils.cc(40786),
		Utils.hh(40786)
	Merged

	FILES: Graph.cc(40779), Graph.hh(40779), Utils.cc(40779),
		Utils.hh(40779)
	Made getNodeFromRndfId and getCurrentLane faster. Reduce the
	GraphNode size further to 254 bytes.

Wed Sep 26 21:50:54 2007	Christian Looman (clooman)

	* version R1-02b
	BUGS:  
	FILES: PlannerInterfaces.h(40649), intersectionDisplay.dd(40649),
		plannerDisplay.dd(40649), plannerDisplay.h(40649)
	Commit after merge

	FILES: PlannerInterfaces.h(40640), intersectionDisplay.dd(40640),
		plannerDisplay.dd(40640), plannerDisplay.h(40640)
	More information from intersection (SAFETY FLAG and CAB MODE)
	Changed structure of logicParams to return values back to planner

Wed Sep 26 20:49:14 2007	Noel duToit (ndutoit)

	* version R1-02a
	BUGS:  
	FILES: CmdArgs.cc(40574), CmdArgs.hh(40574)
	Added the cmdline args for updating lane info from map and stopline
	info separately.

	FILES: PlannerInterfaces.h(40580)
	added a graph-updater error for failing to update based on map info

Wed Sep 26 10:37:38 2007	Sven Gowal (sgowal)

	* version R1-02
	BUGS:  
	FILES: Graph.cc(40446), Graph.hh(40446), Quadtree.cc(40446),
		Utils.cc(40446)
	Reduced GraphNode size from 444 bytes to 262 bytes, and GraphArc
	size from 40 bytes to 36 bytes.

Tue Sep 25 11:01:30 2007	Noel duToit (ndutoit)

	* version R1-01z
	BUGS:  
	FILES: CmdArgs.cc(40245), CmdArgs.hh(40245)
	Added the show_costmap field to the cmd args to specify whether or
	not the cost map is sent to map viewer.

Sun Sep 23 15:18:14 2007	Noel duToit (ndutoit)

	* version R1-01y
	BUGS:  
	FILES: CmdArgs.cc(40060), CmdArgs.hh(40060)
	Added the --use-s1planner and --use-circle-planner to the cmd args.

Fri Sep 21 16:37:27 2007	Noel duToit (ndutoit)

	* version R1-01x
	BUGS:  
	FILES: Console.cc(39884), Console.hh(39884),
		PlannerInterfaces.h(39884), plannerDisplay.dd(39884),
		plannerDisplay.h(39884)
	merged with the latest branch

	FILES: Console.cc(39856), Console.hh(39856),
		PlannerInterfaces.h(39856), plannerDisplay.dd(39856),
		plannerDisplay.h(39856)
	Added some additional fields to the state problem to fascilitate
	fault handling. Updated the console to display this information.
	Added some errors for the circle planner, s1planner and for
	extracting the subpath.

	FILES: PlannerInterfaces.h(39604)
	Added region, planner, and obstacle flags to the state problem.

Fri Sep 21  5:02:46 2007	Sam Pfister (sam)

	* version R1-01w
	BUGS:  
	FILES: CmdArgs.cc(39701), CmdArgs.hh(39701)
	Added value to CmdArgs for command line option to enable use of
	RNDF frame --use-rndf-frame.

Tue Sep 18 18:58:16 2007	Christian Looman (clooman)

	* version R1-01v
	BUGS:  
	FILES: Console.cc(39378), Console.hh(39378),
		intersectionDisplay.dd(39378)
	Output information about merging into Console

Mon Sep 17 20:52:38 2007	Christian Looman (clooman)

	* version R1-01u
	BUGS:  
	FILES: Console.cc(39232), Console.hh(39232),
		intersectionDisplay.dd(39232)
	Outputs the time that Alice is waiting at the intersection in the
	console

Fri Sep 14  5:55:52 2007	Christian Looman (clooman)

	* version R1-01t
	BUGS:  
	FILES: Utils.cc(38781)
	Changed to isObstacle() and isVehicle()

Thu Sep 13 17:35:26 2007	Christian Looman (clooman)

	* version R1-01s
	BUGS:  
	New files: debugDisplay.dd debugDisplay.h
	FILES: Console.cc(38641), Console.hh(38641), Makefile.yam(38641),
		PlannerInterfaces.h(38641)
	* added 3rd page to Console * ouput number of elements in
	tplanner's map

Tue Sep 11 21:37:56 2007	Christian Looman (clooman)

	* version R1-01r
	BUGS:  
	FILES: Console.cc(38441), Console.hh(38441),
		intersectionDisplay.dd(38441), plannerDisplay.dd(38441),
		plannerDisplay.h(38441)
	Fixed segfault in Console

Tue Sep 11 13:42:22 2007	Christian Looman (clooman)

	* version R1-01q
	BUGS:  
	FILES: Console.cc(38341), intersectionDisplay.dd(38341)
	Fixed small "beauty"-bug in the console

Tue Sep 11 13:07:16 2007	Christian Looman (clooman)

	* version R1-01p
	BUGS:  
	New files: intersectionDisplay.dd
	FILES: Console.cc(38313), Console.hh(38313), Makefile.yam(38313),
		PlannerInterfaces.h(38313), plannerDisplay.dd(38313),
		plannerDisplay.h(38313)
	Added intersectionDisplay to output information about obstacles
	that IntersectionHandling looks at

Tue Sep  4 20:24:40 2007	vcarson (vcarson)

	* version R1-01o
	BUGS:  
	FILES: CmdArgs.cc(37541), CmdArgs.hh(37541)
	Added use-dplanner option. 

Tue Sep  4  4:15:53 2007	datamino (datamino)

	* version R1-01n
	BUGS:  
	FILES: CmdArgs.cc(37340), CmdArgs.hh(37338)
	Added mapper_line_fusion_compat option

Sun Sep  2 17:27:36 2007	Sven Gowal (sgowal)

	* version R1-01m
	BUGS:  
	FILES: Utils.cc(37175)
	Propagating map/mapper changes

Sun Sep  2 11:02:22 2007	Christian Looman (clooman)

	* version R1-01l
	BUGS:  
	FILES: Utils.cc(37058)
	in getDistToStop: if no corresponding SegGoal was found, search
	along the path and create SegGoals with fake INTERSECTION_TYPE

Thu Aug 30 17:48:41 2007	Noel duToit (ndutoit)

	* version R1-01k
	BUGS:  
	FILES: Utils.cc(36648), Utils.hh(36648)
	merged with latest release

	FILES: Utils.cc(36499), Utils.hh(36499)
	added a function that calculates the distance from the beginning on
	an array to a point that is projected onto the path.

	FILES: Utils.cc(36639)
	Updated the distToProjectionPoint function to return the correct
	distance when the projection is not on the path.

Thu Aug 30 17:11:28 2007	Christian Looman (clooman)

	* version R1-01j
	BUGS:  
	FILES: Console.cc(36607), Console.hh(36607),
		plannerDisplay.dd(36607), plannerDisplay.h(36607)
	Changed Display. Outputs distance to stopline and more information
	about prediction. Also, the display will be larger.

Thu Aug 30 14:13:42 2007	Sven Gowal (sgowal)

	* version R1-01i
	BUGS:  
	FILES: Utils.cc(36522)
	Optimized getDistToStopline

Wed Aug 29 19:02:13 2007	vcarson (vcarson)

	* version R1-01h
	BUGS:  
	FILES: Utils.cc(36371), Utils.hh(36371)
	Changed reverseProjection to extend the path by the argument given
	if the point passed can not be projected.  

Wed Aug 29 11:20:18 2007	vcarson (vcarson)

	* version R1-01g
	BUGS:  
	FILES: Utils.cc(36199), Utils.hh(36199)
	Moved reverseProject to Utils. 

Wed Aug 29  2:40:42 2007	Noel duToit (ndutoit)

	* version R1-01f
	BUGS:  
	FILES: Graph.hh(36106)
	added a path direction field to the graph node to specify whether
	we are driving fwd or rev.

	FILES: Graph.hh(36124)
	commiting changes for Noel to grab

Tue Aug 28 18:53:16 2007	Christian Looman (clooman)

	* version R1-01e
	BUGS:  
	FILES: Utils.cc(36040), Utils.hh(36040)
	Fixed getDistToStop

Tue Aug 28 12:17:50 2007	Christian Looman (clooman)

	* version R1-01d
	BUGS:  
	FILES: PlannerInterfaces.h(35895), Utils.cc(35895), Utils.hh(35895)
	commit after merge

	FILES: PlannerInterfaces.h(35888), Utils.cc(35888), Utils.hh(35888)
	Updated the distToStop function. It now compares the SegGoals vs.
	the path. The closer stopline will be returned.

Tue Aug 28  9:29:23 2007	Laura Lindzey (lindzey)

	* version R1-01c
	BUGS:  
	FILES: CmdArgs.cc(35858), CmdArgs.hh(35858)
	fixing argument (int -> bool)

Mon Aug 27 16:27:27 2007	Laura Lindzey (lindzey)

	* version R1-01b
	BUGS:  
	FILES: CmdArgs.hh(35727)
	adding option to run groundstrike filtering in internal map

Mon Aug 27 14:34:41 2007	Sven Gowal (sgowal)

	* version R1-01a
	BUGS:  
	FILES: Graph.cc(35694), Graph.hh(35694)
	Removed unused fields in GraphNode

Mon Aug 27  9:21:31 2007	vcarson (vcarson)

	* version R1-01
	BUGS:  
	New files: Utils.cc Utils.hh
Mon Aug 27  8:56:13 2007	vcarson (vcarson)

	* version R1-00z
	BUGS:  
	FILES: Makefile.yam(35600)
	Added Utils class to replace LogicUtils in logic-planner. 

Tue Aug 21  6:00:04 2007	Sam Pfister (sam)

	* version R1-00y
	BUGS:  
	FILES: CmdArgs.cc(34540), CmdArgs.hh(34540), Graph.cc(34540),
		Graph.hh(34540), GraphPlanner.cc(34540),
		GraphPlanner.hh(34540), PlannerInterfaces.h(34540)
	Changed the name of the Graph class to Graph_t.  The class name had
	been typedefed to Graph_t already so most interfaces won't change. 
	This was necessary to have the option to run mapper inside planner
	because of a naming conflict.

Mon Aug 20 21:27:30 2007	Christian Looman (clooman)

	* version R1-00x
	BUGS:  
	FILES: Console.cc(34428)
	commit after merge

	FILES: Console.cc(34421)
	Fixed minor bugs in Console

Mon Aug 20 19:22:11 2007	Sven Gowal (sgowal)

	* version R1-00w
	BUGS:  
	FILES: Graph.hh(34371), PlannerInterfaces.h(34371)
	Added distToStop field in node

Wed Aug 15 17:44:04 2007	Sven Gowal (sgowal)

	* version R1-00v
	BUGS:  
	FILES: CmdArgs.cc(33750), CmdArgs.hh(33750)
	Added enable-line-fusion argument

Tue Aug 14 18:25:39 2007	Noel duToit (ndutoit)

	* version R1-00u
	BUGS:  
	FILES: Makefile.yam(33543), PlannerInterfaces.h(33543)
	merged with the latest branch

	FILES: Makefile.yam(33513), PlannerInterfaces.h(33513)
	updated the zone-planner interface to now use the cspecs object.

Tue Aug 14 17:26:53 2007	Sven Gowal (sgowal)

	* version R1-00t
	BUGS:  
	FILES: Graph.cc(33414), Graph.hh(33414)
	merged

	FILES: Graph.cc(33338), Graph.hh(33338)
	Added rail management

Tue Aug 14 12:04:41 2007	Noel duToit (ndutoit)

	* version R1-00s
	BUGS:  
	FILES: Graph.hh(33357), Makefile.yam(33357),
		PlannerInterfaces.h(33357)
	Implemented a temporary interface to the zone planners. Added zone
	graph nodes and some zone info to the path-params struct. Updated
	makefile accordingly.

Tue Aug 14  9:39:33 2007	Christian Looman (clooman)

	* version R1-00r
	BUGS:  
	New files: plannerDisplay.dd plannerDisplay.h
	FILES: Console.cc(33283), Console.hh(33283), Makefile.yam(33283),
		PlannerInterfaces.h(33283)
	Commit after merge

	New files: plannerDisplay.dd plannerDisplay.h
	FILES: Console.cc(33230), Console.hh(33230), Makefile.yam(33230),
		PlannerInterfaces.h(33230)
	Integrated new Console and assign keyboard commands

	FILES: PlannerInterfaces.h(33256)
	Updated console to work with 24 lines only

Thu Aug  9 12:37:41 2007	Sven Gowal (sgowal)

	* version R1-00q
	BUGS:  
	FILES: PlannerInterfaces.h(32792)
	Added VP_BACKUP_FINISHED

Wed Aug  8 11:18:47 2007	Christian Looman (clooman)

	* version R1-00p
	BUGS:  
	FILES: PlannerInterfaces.h(32583)
	commit after merging branches

	FILES: PlannerInterfaces.h(32575)
	Changed logicParamtT so that LogicPlanner receives the whole
	segment goal queue

Mon Aug  6 20:25:03 2007	Sven Gowal (sgowal)

	* version R1-00o
	BUGS:  
	FILES: Graph.cc(32376), Graph.hh(32376)
	updated to enable unloading in Graph-updater

Sat Aug  4  3:32:18 2007	Magnus Linderoth (mlinderoth)

	* version R1-00n
	BUGS:  
	FILES: GraphPath.hh(31906)
	Added acceleration and curvature fields to GraphPath

	FILES: PlannerInterfaces.h(31907)
	Defined Vel_params_t

Fri Aug  3 20:13:28 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: CmdArgs.cc(31749), CmdArgs.hh(31749)
	added update_from_map flag

	FILES: Graph.hh(31840)
	Added node type for lane changes with broken white line

Fri Aug  3  0:15:16 2007	Sven Gowal (sgowal)

	* version R1-00l
	BUGS:  
	New files: Quadtree.cc Quadtree.hh
	FILES: CmdArgs.cc(31444), CmdArgs.hh(31444)
	Added --step-by-step-loading argument

	FILES: CmdArgs.cc(31623), CmdArgs.hh(31623), Graph.cc(31623),
		Graph.hh(31623)
	Now loading from files possible

	FILES: Graph.cc(31372), Graph.hh(31372), Makefile.yam(31372)
	Using quadtree for spacial indexing

Tue Jul 31 19:53:05 2007	Sven Gowal (sgowal)

	* version R1-00k
	BUGS:  
	FILES: Graph.cc(31237), Graph.hh(31237)
	merged

	FILES: Graph.cc(30955), Graph.hh(30955)
	Overloaded getNearestNode function

	FILES: Graph.cc(31190), Graph.hh(31190)
	Modifying getNearestNode function

	FILES: Graph.cc(31192), Graph.hh(31192)
	Added getNext/PrevChangeNode

	FILES: Graph.cc(31227)
	merged

	FILES: Graph.hh(30899)
	Added a GRAPH_ARC_CHANGE type

	FILES: Graph.hh(30901)
	Changed arc type to int

Tue Jul 31 15:06:59 2007	Jessica Gonzalez (jengo)

	* version R1-00j
	BUGS:  
	FILES: Graph.cc(31115)
	increased size of graph to enable loading larger rndfs

Tue Jul 31 13:32:12 2007	Christian Looman (clooman)

	* version R1-00i
	BUGS:  
	FILES: CmdArgs.cc(31070), CmdArgs.hh(31070)
	Changed cmdArgs for command line argument noprediction

Tue Jul 31 11:17:12 2007	Christian Looman (clooman)

	* version R1-00h
	BUGS:  
	FILES: Console.cc(31021), Console.hh(31021)
	added output for Prediction

Fri Jul 27 16:26:14 2007	Noel duToit (ndutoit)

	* version R1-00g
	BUGS:  
	New files: AliceStateHelper.cc AliceStateHelper.hh CmdArgs.cc
		CmdArgs.hh Console.cc Console.hh Log.cc Log.hh
	FILES: Graph.hh(30369)
	Added a type to the arcs

	FILES: Graph.hh(30437)
	Added ignored member to GraphArc

	FILES: Makefile.yam(30374)
	Moved the common files in planner over from planner itself to this
	interfaces library.

	FILES: Makefile.yam(30401)
	Fixed

	FILES: PlannerInterfaces.h(30293)
	started enforcing continuity between plans. Added bool
	planFromCurrPos to the path-planner params.

	FILES: PlannerInterfaces.h(30356)
	Added an error for logic-planner

	FILES: PlannerInterfaces.h(30479)
	added error for path planner to deal with the case where we cannot
	find a close node on the path to plan from.

	FILES: PlannerInterfaces.h(30499)
	merged

Mon Jul 23 19:33:30 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	Deleted files: GraphUtils.cc GraphUtils.hh
Thu Jul 19 18:49:33 2007	Sven Gowal (sgowal)

	* version R1-00e
	BUGS:  
	FILES: Graph.cc(29546), Graph.hh(29546), Makefile.yam(29546),
		PlannerInterfaces.h(29546)
	Modified Path_params_t

	FILES: Graph.hh(29572)
	Added a velocity variable to the node. This is the velocity along
	the lane that is associated with the car at that node.

	FILES: PlannerInterfaces.h(29622)
	Added more error flags in path planner.

	FILES: Graph.cc(29405), Graph.hh(29405)
	updated Graph.cc

	FILES: Graph.cc(29481), Graph.hh(29481)
	added the collideGrownObs and railId to the graph node structure.

Wed Jul 18 12:24:55 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: PlannerInterfaces.h(29403)
	Changed Logic_param_t

	FILES: PlannerInterfaces.h(29414)
	Modified Err_t

Mon Jul 16 20:45:13 2007	Noel duToit (ndutoit)

	* version R1-00c
	BUGS:  
	FILES: Graph.hh(29206), GraphPlanner.cc(29206),
		GraphPlanner.hh(29206), GraphPlannerGen.cc(29206)
	Moved the vehicleNode, which is the volatile node associated with
	our current position, out of the graph-planner and into the graph.

Mon Jul 16 13:59:44 2007	Francisco zabala (zabala)

	* version R1-00b
	BUGS:  
	New files: GraphUtils.cc GraphUtils.hh
	FILES: GraphPlanner.cc(29105), GraphPlanner.hh(29105)
	created GraphUtils to hold load/gen functions currently at
	GraphPlanner

	FILES: GraphPlanner.cc(29153), GraphPlanner.hh(29153),
		PlannerInterfaces.h(29153), TrajPlanner.cc(29153)
	Changed Graph_t to be Graph (instead of GraphPlanner)

Fri Jul 13 16:04:51 2007	Sven Gowal (sgowal)

	* version R1-00a
	BUGS:  
	New files: Graph.cc Graph.hh GraphPath.hh GraphPlanner.cc
		GraphPlanner.hh GraphPlannerGen.cc PlannerInterfaces.h
		TrajPlanner.cc TrajPlanner.hh
	FILES: Makefile.yam(28759)
	These are the required graph planner files to generate a graph,
	plan a path, and plan the velocity. It also contains the planner
	interfaces file that define additional interfaces. This builds a
	library which is now used in the planner and planner libraries.

Tue Jul 10 18:21:16 2007	Noel duToit (ndutoit)

	* version R1-00
	Created temp-planner-interfaces module.









































































































