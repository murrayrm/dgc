#
# This is the source file for managing traffic planner command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Noel duToit, 2007-07-14
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings

package "planner"
purpose "Planner is responsible for taking in mission information and the local map, and generating a planned trajectory."
version "1.0"

option "rndf" - "initialize local map from the specified RNDF file" string default="" no
option "mdf" - "load MDF file over which to plan missions" string default="" no
option "disable-console" D "disable the ncurses console" flag off
option "skynet-key" S "skynet key" int default="0" no
option "verbose" v "turn on verbose error messages" int default="0" no argoptional
option "log-level" - "set the verbose level of the gcmodule logger" int default="0" no argoptional
option "log" L "turn on logging" flag off
option "log-path" - "path to the log files" string default="" no
option "closed-loop" - "recompute path from current pos" flag off
option "noprediction" - "do not use prediction" flag off
option "step-by-step-loading" - "loading RNDF information as needed into the graph" flag off
option "load-graph-files" - "loading RNDF information from pre-generated graph files" flag off

option "rate" - "Maximum planning rate in Hz" float default="10" no

# these options are for choosing between s1planner or dplanner for zone regions 
option "use-dplanner" - "use dplanner for zone planning" flag off
option "use-s1planner" - "use s1planner for zone planning" flag off
option "use-circle-planner" - "use circle-planner for zone planning" flag off
option "show-costmap" - "show the costmap on MapViewer" flag off
option "use-hacked-uturn" - "use the hacked uturn maneuver" flag off
option "use-hacked-backup" - "use the hacked backup maneuver" flag off

# these options are needed for line fusion
option "update-from-map" - "updates the graph information from the map" flag off
option "update-lanes" - "updates the graph information about lanes from the map" flag off
option "update-stoplines" - "updates the graph information about stoplines from the map" flag off

# these options are for reading config/params files 
option "costmap-config" - "configuration file for painting cost map" string default="costmap.config" no
option "path-plan-config" - "configuration file for path planning costs" string default="path_plan.config" no

# these options are for the internal mapper structure
option "mapper-use-internal" - "uses internal mapper instead of standalone executable" flag off
option "mapper-debug-subgroup" -
  "Set mapper debugging subgroup id. If subgroup>=0 no debugging info is sent" int default="-2" no
option "mapper-disable-obs-fusion" -
  "Disable fusion of obstacles in mapper" flag off
option "mapper-disable-line-fusion" - "Don't update the map with the sensed lane lines" flag off
option "mapper-line-fusion-compat" - "Use old (tested) line fusion behavior, i.e. update the map in Map, not mapper main loop" flag off
option "mapper-decay-thresh" -
	"Time threshold for removing un-updated elements of the map" int default="0" no 
option "mapper-enable-groundstrike-filtering" -
  "Whether to generate a road map and check incoming ladar-car-perceptor obstacles against it" flag off

option "use-rndf-frame" -
	"Set this flag to use the new RNDF frame which enables road sensor based localization to the RNDF" flag off

# options that are not currently being used and will likely be remove in the future
option "debug" d "specify the amount of debug messages" int default="0" no argoptional
option "use-local" - "use local coordinates" flag off
option "nowait" - "do not wait for state to fill, plan from vehicle state" flag off


