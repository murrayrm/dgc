/*!
 * \file Console.cc
 * \brief Source code for the console of the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "Console.hh"
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdarg.h>
#include "AliceStateHelper.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include <iomanip>
#include <sstream>
#include "Log.hh"


bool Console::initialized = false;

// variables for Trajectory
double Console::traj_x=0.0;
double Console::traj_y=0.0;
double Console::traj_vel=0.0;
double Console::traj_f_x=0.0;
double Console::traj_f_y=0.0;
double Console::traj_f_vel=0.0;

// variables for Prediction
int Console::pred_numbers=0;

// variables for Alice State
double Console::state_velocity=0.0;

// variables for Alice State
double Console::distToStop=INFINITY;
double Console::interTimer=INFINITY;

// variables for messages
char Console::messages[MAX_MSG][MSG_LENGTH];

// variables for Rates
double Console::rate1;
double Console::rate2;

CSparrowHawk* Console::display = &SparrowHawk();

// variables for keyboard commands
bool Console::resetIntersectionFlag = false;
bool Console::togglePredictionFlag = false;
bool Console::resetStateFlag = false;
bool Console::toggleIntersectionSafetyFlag = false;
bool Console::toggleIntersectionCabmodeFlag = false;

// variables for map debug information
int Console::mapSize = 0;
int Console::obstSize = 0;
int Console::clearSize = 0;


/**
 * @brief Initializes the console display
 */
void Console::init()
{
  display->add_page(plannertable, "planner");
  display->add_page(intersectiontable, "intersection");
  display->add_page(debugtable, "debug");

  // bind variables for Rate
  display->rebind("rate1", &rate1);
  display->set_readonly("rate1");
  display->rebind("rate2", &rate2);
  display->set_readonly("rate2");

   // bind variables for Trajectory
  display->rebind("traj_x", &traj_x);
  display->set_readonly("traj_x");
  display->rebind("traj_y", &traj_y);
  display->set_readonly("traj_y");
  display->rebind("traj_vel", &traj_vel);
  display->set_readonly("traj_vel");
  display->rebind("traj_f_x", &traj_f_x);
  display->set_readonly("traj_f_x");
  display->rebind("traj_f_y", &traj_f_y);
  display->set_readonly("traj_f_y");
  display->rebind("traj_f_vel", &traj_f_vel);
  display->set_readonly("traj_f_vel");

   // bind variables for Alice Status
  display->rebind("state_velocity", &state_velocity);
  display->set_readonly("state_velocity");

   // bind variables for Prediction
  display->rebind("pred_obst", &pred_numbers);
  display->set_readonly("pred_obst");

   // bind variables for Prediction
  display->rebind("dist_stop", &distToStop);
  display->set_readonly("dist_stop");
  display->rebind("dist_stop2", &distToStop);
  display->set_readonly("dist_stop2");

  display->rebind("inter_timer", &interTimer);
  display->set_readonly("inter_timer");

  display->rebind("mapsize", &mapSize);
  display->set_readonly("mapsize");

  display->rebind("obstsize", &obstSize);
  display->set_readonly("obstsize");

  display->rebind("clearsize", &clearSize);
  display->set_readonly("clearsize");

  // assign keys
  display->set_keymap((int)'i', &resetIntersection);
  display->set_keymap((int)'p', &togglePrediction);
  display->set_keymap((int)'s', &resetState);
  display->set_keymap((int)'y', &toggleIntersectionSafety);
  display->set_keymap((int)'c', &toggleIntersectionCabmode);

  initialized = true;

  for (unsigned int i=0; i<MAX_MSG; i++)
     while (strlen(messages[i])<MSG_LENGTH)
       strncat(messages[i]," ",1);
}

/**
 * @brief Frees memory
 */
void Console::destroy()
{
}

// sets and query resetIntersectionFlag
void Console::resetIntersection()
{
  resetIntersectionFlag = true;
}

bool Console::queryResetIntersectionFlag()
{
  bool flag = resetIntersectionFlag;
  resetIntersectionFlag = false;
  return flag;
}

// sets and query togglePredictionFlag
void Console::togglePrediction()
{
  togglePredictionFlag = true;
}

bool Console::queryTogglePredictionFlag()
{
  bool flag = togglePredictionFlag;
  togglePredictionFlag = false;
  return flag;
}

// sets and query resetStateFlag
void Console::resetState()
{
  resetStateFlag = true;
}

bool Console::queryResetStateFlag()
{
  bool flag = resetStateFlag;
  resetStateFlag = false;
  return flag;
}

// sets and query toggleIntersectionSafetyFlag
void Console::toggleIntersectionSafety()
{
  toggleIntersectionSafetyFlag = true;
}

bool Console::queryToggleIntersectionSafety()
{
  bool flag = toggleIntersectionSafetyFlag;
  toggleIntersectionSafetyFlag = false;
  return flag;
}

// sets and query toggleIntersectionCabmodeFlag
void Console::toggleIntersectionCabmode()
{
  toggleIntersectionCabmodeFlag = true;
}

bool Console::queryToggleIntersectionCabmode()
{
  bool flag = toggleIntersectionCabmodeFlag;
  toggleIntersectionCabmodeFlag = false;
  return flag;
}

/**
 * @brief Displays the cycle time and make an average
 */
void Console::updateRate(double time) {
  static double total_time;
  static unsigned int cnt = 1;

  if (!initialized) return;
  total_time += time;

  rate1 = time;
  rate2 = total_time/(double)cnt;

  cnt++;
}

/**
 * @brief Displays the trajectory information
 */
void Console::updateTrajectory(CTraj *traj)
{
  int size = traj->getNumPoints();

  if (!initialized) return;
  if (size < 1) return;

  traj_x = traj->getNorthing(0);
  traj_y = traj->getEasting(0);
  traj_vel = traj->getSpeed(0);

  traj_f_x = traj->getNorthing(size-1);
  traj_f_y = traj->getEasting(size-1);
  traj_f_vel = traj->getSpeed(size-1);
}

/**
 * @brief Displays Alice state
 */
void Console::updateState(VehicleState &vehState)
{
  if (!initialized) return;

  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);

  stringstream p;

  char c[20];

  sprintf(c,"[ %7.2f, %7.2f ]", pos.x, pos.y);

  display->set_string("state_position", c);
  state_velocity = AliceStateHelper::getVelocityMag(vehState);
}

/**
 * @brief Displays intersection information
 */
void Console::updateInter(bool SafetyFlag, bool CabmodeFlag, int CountPrecedence, string Status, PointLabel wp, double timer)
{
  string inter_safety;
  string inter_cabmode;

  if (!initialized) return;
  
  if (SafetyFlag)
    inter_safety = "SET         ";
  else
    inter_safety = "DEACTIVATED";

  if (CabmodeFlag)
    inter_cabmode = "SET        ";
  else
    inter_cabmode = "DEACTIVATED";

  stringstream cp;
  cp<<CountPrecedence;

  stringstream s;
  s<<wp;
  char c[50];
  snprintf(c, sizeof(c), "%s", s.str().c_str());

  display->set_string("inter_safety", inter_safety.c_str());
  display->set_string("inter_cabmode", inter_cabmode.c_str());
  display->set_string("inter_status", Status.c_str());
  display->set_string("inter_status2", Status.c_str());
  display->set_string("inter_wp", c);
  display->set_string("inter_wp2", c);
  
  interTimer = timer;
}

void Console::updateInterPage2(vector<PrecedenceList_t> obstaclesPrecedence, vector<MapElement> obstaclesClearance, vector<MapElement> obstaclesMerging)
{
   // create list with vehicles having precedence
   for (unsigned int i=0; i<obstaclesPrecedence.size(); i++) {
     stringstream s;
     s<<std::setprecision(2)<<"ID="<<obstaclesPrecedence[i].element.id<<" T="<<obstaclesPrecedence[i].element.type<<" P= ["<<obstaclesPrecedence[i].element.position.x<<", "<<obstaclesPrecedence[i].element.position.y<<"] V="<<obstaclesPrecedence[i].velocity<<" D="<<obstaclesPrecedence[i].distance<<" ETA="<<obstaclesPrecedence[i].eta<<" PREC="<<obstaclesPrecedence[i].precedence<<" L="<<obstaclesPrecedence[i].lane;;

     char c[70];
     snprintf(c,sizeof(c),"%s",s.str().c_str());
     while (strlen(c)<70)
       strncat(c," ",1);

     if (i==0)
       display->set_string("precedence1", c);
     else if (i==1)
       display->set_string("precedence2", c);
     else if (i==2)
       display->set_string("precedence3", c);
     else if (i==3)
       display->set_string("precedence4", c);
     else if (i==4)
       display->set_string("precedence5", c);
   }

   for (unsigned int i=obstaclesPrecedence.size(); i<5; i++) {
     char c[70]="";
     while (strlen(c)<70)
       strncat(c," ",1);
     if (i==0)
       display->set_string("precedence1", c);
     else if (i==1)
       display->set_string("precedence2", c);
     else if (i==2)
       display->set_string("precedence3", c);
     else if (i==3)
       display->set_string("precedence4", c);
     else if (i==4)
       display->set_string("precedence5", c);
   }

   // create list with vehicles blocking intersection
   for (unsigned int i=0; i<obstaclesClearance.size(); i++) {
     stringstream s;
     double v = sqrt(pow(obstaclesClearance[i].velocity.x,2) + pow(obstaclesClearance[i].velocity.y,2));
     s<<std::setprecision(2)<<"ID="<<obstaclesClearance[i].id<<" T="<<obstaclesClearance[i].type<<" P= ["<<obstaclesClearance[i].position.x<<", "<<obstaclesClearance[i].position.y<<"] V= "<<v<<" Stopped="<<obstaclesClearance[i].timeStopped;
     char c[70];
     snprintf(c,sizeof(c),"%s",s.str().c_str());
     while (strlen(c)<70)
       strncat(c," ",1);
     if (i==0)
       display->set_string("clearance1", c);
     else if (i==1)
       display->set_string("clearance2", c);
     else if (i==2)
       display->set_string("clearance3", c);
     else if (i==3)
       display->set_string("clearance4", c);
     else if (i==4)
       display->set_string("clearance5", c);
   }

   for (unsigned int i=obstaclesClearance.size(); i<5; i++) {
     char c[70]="";
     while (strlen(c)<70)
       strncat(c," ",1);

     if (i==0)
       display->set_string("clearance1", c);
     else if (i==1)
       display->set_string("clearance2", c);
     else if (i==2)
       display->set_string("clearance3", c);
     else if (i==3)
       display->set_string("clearance4", c);
     else if (i==4)
       display->set_string("clearance5", c);
   }

   for (unsigned int i=0; i<obstaclesMerging.size(); i++) {
     stringstream s;
     double v = sqrt(pow(obstaclesMerging[i].velocity.x,2) + pow(obstaclesMerging[i].velocity.y,2));
     s<<std::setprecision(2)<<"ID="<<obstaclesMerging[i].id<<" T="<<obstaclesMerging[i].type<<" P= ["<<obstaclesMerging[i].position.x<<", "<<obstaclesMerging[i].position.y<<"] V= "<<v;
     char c[70];
     snprintf(c,sizeof(c),"%s",s.str().c_str());
     while (strlen(c)<70)
       strncat(c," ",1);
     if (i==0)
       display->set_string("merging1", c);
     else if (i==1)
       display->set_string("merging2", c);
     else if (i==2)
       display->set_string("merging3", c);
     else if (i==3)
       display->set_string("merging4", c);
     else if (i==4)
       display->set_string("merging5", c);
   }

   for (unsigned int i=obstaclesMerging.size(); i<1; i++) {
     char c[70]="";
     while (strlen(c)<70)
       strncat(c," ",1);

     if (i==0)
       display->set_string("merging1", c);
     else if (i==1)
       display->set_string("merging2", c);
     else if (i==2)
       display->set_string("merging3", c);
     else if (i==3)
       display->set_string("merging4", c);
     else if (i==4)
       display->set_string("merging5", c);
   }
}

void Console::updateStopline(double dist)
{
  distToStop = dist;
}

/**
 * @brief Displays Prediction information
 */
void Console::updatePred(string Status, int numberObstacles)
{
  if (!initialized) return;
  
  display->set_string("pred_status",Status.c_str());
  pred_numbers = numberObstacles;
}

/**
 * @brief Displays last completed waypoint
 */
void Console::updateLastWpt(PointLabel last)
{
  if (!initialized) return;

  stringstream s;
  s<<last.segment<<"."<<last.lane<<"."<<last.point;

  char c[15];
  sprintf(c,"%-10s",s.str().c_str());

  display->set_string("state_last_wpt", c);
}

/**
 * @brief Displays the next waypoint
 */
void Console::updateNextWpt(PointLabel next)
{
  if (!initialized) return;

  stringstream s;
  s<<next.segment<<"."<<next.lane<<"."<<next.point;

  char c[15];
  sprintf(c,"%-10s",s.str().c_str());

  display->set_string("state_next_wpt", c);
}

/**
 * @brief Displays turning signal
 */
void Console::updateTurning(int direction)
{
  if (!initialized) return;

  string state_turning;

  if (direction == -1)
    state_turning = "<--";                  /* Turning */
  else if (direction == 1)
    state_turning = "-->";
  else
    state_turning = "   ";

  display->set_string("state_turning", state_turning.c_str());
  //  display->set_string("state_turning", state_turning.c_str());
}

/**
 * @brief Displays FSM state
 */
void Console::updateFSMState(string state)
{
  if (!initialized) return;

  char c[20];
  snprintf(c,sizeof(c),"%-20s",state.c_str());
  display->set_string("FSM_state", c);
}

/**
 * @brief Displays FSM flag
 */
void Console::updateFSMFlag(string flag)
{
  if (!initialized) return;

  char c[20];
  snprintf(c,sizeof(c),"%-20s",flag.c_str());
  display->set_string("FSM_flag", c);
}

/**
 * @brief Displays FSM region
 */
void Console::updateFSMRegion(string region)
{
  if (!initialized) return;

  char c[20];
  snprintf(c,sizeof(c),"%-20s",region.c_str());
  display->set_string("FSM_region", c);
}

/**
 * @brief Displays FSM planner
 */
void Console::updateFSMPlanner(string planner)
{
  if (!initialized) return;

  char c[20];
  snprintf(c,sizeof(c),"%-20s",planner.c_str());
  display->set_string("FSM_planner", c);
}

/**
 * @brief Displays FSM obstacle
 */
void Console::updateFSMObstacle(string obstacle)
{
  if (!initialized) return;

  char c[20];
  snprintf(c,sizeof(c),"%-20s",obstacle.c_str());
  display->set_string("FSM_obstacle", c);
}

/**
 * @brief Adds and displays the message
 */
void Console::addMessage(char *format, ...)
{
  static char msg[MSG_LENGTH];

  if (!initialized) return;

   va_list valist;
   va_start(valist, format);
   vsnprintf(msg, MSG_LENGTH, format, valist);
   va_end(valist);

   for (int i = 0; i < MAX_MSG; i++) {
     strncpy(messages[i], messages[i+1], MSG_LENGTH-1);
     messages[i][MSG_LENGTH-1] = '\0';
   }
   strncpy(messages[MAX_MSG-1], msg, MSG_LENGTH-1);
   messages[MAX_MSG-1][MSG_LENGTH-1] = '\0';

   char c[100];
   snprintf(c,sizeof(c),"%-77s",messages[0]);
   display->set_string("message1",c);

   snprintf(c,sizeof(c),"%-77s",messages[1]);
   display->set_string("message2",c);

   snprintf(c,sizeof(c),"%-77s",messages[2]);
   display->set_string("message3",c);

   snprintf(c,sizeof(c),"%-77s",messages[3]);
   display->set_string("message4",c);
   snprintf(c,sizeof(c),"%-77s",messages[4]);
   display->set_string("message5",c);

   snprintf(c,sizeof(c),"%-77s",messages[5]);
   display->set_string("message6",c);
}

void Console::updateMapSize(int size)
{
  mapSize = size;
}

void Console::increaseObstSize()
{
  obstSize ++;
}

void Console::increaseClearSize()
{
  clearSize++;
}
