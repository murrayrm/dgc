#ifndef UTILS_HH_
#define UTILS_HH_

#include <interfaces/ActuatorState.h>
#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>

#include <map/MapElementTalker.hh>

class Utils {

public: 
  static int init();
  static void destroy();

  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static LaneLabel getCurrentLane(VehicleState &vehState, PlanGraph *graph);
  static LaneLabel getCurrentLaneAnyDir(VehicleState &vehState, PlanGraph *graph);
  static LaneLabel getDesiredLane(VehicleState &vehState, PlanGraph *graph);
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(VehicleState &vehState, PlanGraph* graph, Map *map, PlanGraphPath* m_path, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal);
  static double getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(point2 &pos, Map *map, LaneLabel &lane);
  static double getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getAngleInRange(double angle);
  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement **me, VehicleState &vehState, Map *map, LaneLabel &lane); 
  static double monitorAliceSpeed(VehicleState &vehState, int m_estop);
  static void updateStoppedMapElements(Map *map);
  static uint64_t getTime();
  static int reverseProjection(point2 &out, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static int distToProjectedPoint(point2 &out, double &distance, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static void displayGraph(int sendSubgroup, PlanGraph* graph, VehicleState vehState);
  static void printErrorInBinary(Err_t error);
  static void printCSpecs(CSpecs_t* cSpecs);
  static void printPath(PlanGraphPath *path);
  static void printTraj(CTraj* traj);
  static void printCurrPose(pose2f_t pose);
  public:

  // Set the site frame to local frame transform for display functions
  static void setSiteToLocal(const VehicleState *vehState);
  
  static void displayPath(int sendSubgroup, PlanGraphPath* path, MapElementColorType colorType = MAP_COLOR_DARK_GREEN, MapId mapId = 12500);

  static void displayTraj(int sendSubgroup, CTraj* traj);
  static void displayLine(int sendSubgroup, point2arr line, MapElementColorType colorType = MAP_COLOR_PINK, MapId mapId = 13000);
  static void displayPoints(int sendSubgroup, point2arr points, MapElementColorType colorType = MAP_COLOR_PURPLE, MapId mapId = 13500);
  static void displayGraphHeadings(int sendSubgroup, PlanGraph* graph, VehicleState vehState, double bound = 25);

private :
  
  // Transform from site to local frame
  static pose2 m_transLS;

  // Talker for the map
  static CMapElementTalker m_meTalker;

};

#endif /*UTILS_HH_*/


