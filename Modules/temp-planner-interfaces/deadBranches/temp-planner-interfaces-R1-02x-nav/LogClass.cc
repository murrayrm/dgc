/*!
 * \file Log.cc
 * \brief Source code for logging in the planning stack as a whole
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "LogClass.hh"
#include "CmdArgs.hh"

#include <math.h>

CLog::CLog(int level)
{
  verbose_level = level;
}


CLog::~CLog()
{
}

int CLog::getVerboseLevel()
{
    return verbose_level;
}

void CLog::setVerboseLevel(int level)
{
    verbose_level = level;
}

void CLog::setGenericLogFile(const char *filename)
{
  genericFileStream.open(filename);
  if (genericFileStream.is_open()) {
    genericFileStream << "%Intersection Log File Start" << endl << endl;
  }
}

ostream& CLog::getStream(int level)
{
  if (verbose_level >= level)
    return genericFileStream;
}
