
/* 
 * Desc: Graph-based planner
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_PLANNER_HH
#define GRAPH_PLANNER_HH


// Forward declarations
namespace std
{
  class Lane;
  class RNDF;
}
class Map;
class MapElement;


#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>

#include "Graph.hh"
#include "GraphPath.hh"


/// @brief Vehicle kinematic properties.
struct GraphPlannerKinematics
{
  /// Distance between front and rear wheels (m).
  double wheelBase;

  /// Maximum steering angle (degrees).  This is the largest angle
  /// that the lowel-level will command.  This value is also used to
  /// scale the measured steering position in ActuatorState (which is
  /// in the domain [-1, +1]) to a steering angle.
  double maxSteer;

  /// Maximum allowed steering angle for generating turning maneuvers.
  /// This can be higher than maxSteer, in which case the vehicle will
  /// attempt turns that are beyond its actual steering capability (and
  /// swing wide as a result).
  double maxTurn;
};


/// @brief Plan constraints.
///
/// These are the weights used to construct plans.  A negative value
/// inidicates a hard constraint.
struct GraphPlannerConstraints
{
  /// Enable driving in reverse.
  bool enableReverse;

  /// Cost multiplier for driving off the lane center-line.
  int centerCost;
  
  /// Cost for driving in an on-coming lane (and risking a head-on
  /// crash). This value should be much larger than laneCost.
  int headOnCost;

  /// Cost for changing lanes.
  int changeCost;  

  /// Cost for drivig off-road
  int offRoadCost;

  /// Cost for driving in reverse.
  int reverseCost;

  /// Cost for driving through a static obstacle.  Can be set to -1 to
  /// denote a hard constraint.
  int obsCost;

  /// Cost for driving through another vehicle. This can be set
  /// to zero to produce queuing behavior at intersections (the
  /// trajectory generation step will set the velocity profile
  /// such that we dont crash into other vehicles).
  int carCost;
};


/// @brief Graph-based planner
class GraphPlanner
{
  public:

  // Default constructor
  GraphPlanner(Graph *graph);

  // Destructor
  virtual ~GraphPlanner();

  private:

  // Global plan graph
  Graph *graph;
  
  public:

  /// @brief Get the vehicle kinematics
  ///
  /// @param[out] kin Current vehicle kinematic values.
  int getKinematics(GraphPlannerKinematics *kin);

  /// @brief Set the vehicle kinematics
  ///
  /// @param[int] kin Current vehicle kinematic values.
  int setKinematics(const GraphPlannerKinematics *kin);

  private:

  // Vehicle kinermatic properties
  GraphPlannerKinematics kin;

  public:
  
  // Initialize the graph from an RNDF file.
  int loadRndf(const char *filename);
  
  private:

  // TODO: graph generation could be moved to a seperate class.
  
  // Generate lane nodes
  int genLanes(std::RNDF *rndf);

  // Generate a lane, driving either with traffic (direction = +1) or
  // against traffic (direction = -1).
  int genLane(std::Lane *lane, int direction);

  // Generate lane tangents
  int genLaneTangents(std::Lane *lane);
  
  // Generate turn manuevers
  int genTurns(std::RNDF *rndf);

  // Generate lane-change manuevers
  int genChanges(std::RNDF *rndf);

  // Generate K-turn maneuvers
  int genKTurns();

  // Generate a maneuver linking two nodes
  int genManeuver(int nodeType, GraphNode *nodeA, GraphNode *nodeB, double maxSteer);

  // Set the RNDF data on a change node
  int genChangeNode(GraphNode *node);

  // Set the RNDF data on a volatile node
  int genVolatileNode(GraphNode *node);
  
  public:

  /// @brief Update the vehicle node and associated maneuvers.
  ///
  /// Unlike the rest of the graph, this must be done dynamically.
  int genVehicleSubGraph(const VehicleState *vehicleState, const ActuatorState *actuatorState);
  
  public:

  /// @brief Get the plan constraints
  ///
  /// @param[out] cons Current plan constraints. 
  int getConstraints(GraphPlannerConstraints *cons);

  /// @brief Set the plan constraints
  ///
  /// @param[in] cons Current plan constraints. 
  int setConstraints(const GraphPlannerConstraints *cons);

  /// @brief Construct a plan for reaching the given checkpoint
  int makePlan(int checkId);
  /// @brief Construct a plan for reaching the given pose
  int makePlan(pose3_t pose);
  /// @brief Construct a plan for reaching the given node
  int makePlan(GraphNode *node);

  /// @brief Construct path.
  ///
  /// @param[out] path The planned path, including some meta-data.
  int makePath(GraphPath *path);

  private:

  // Push node onto the priority queue
  int pushNode(GraphNode *node, int planCost);
    
  // Pop node from the priority queue
  GraphNode *popNode();

  // Plan constraints
  GraphPlannerConstraints cons;
  
  // Priority queue for Dijkstra
  int queueLen, queueMax;
  GraphNode **queue;

  // TODO: make private
  public:

  // Current segment/lane (both are set to zero if the
  // vehicle is in an intersection).
  int segmentId, laneId;
  
  // Vehicle node in graph
  GraphNode *vehicleNode;  
};



#endif
