#include "Utils.hh"
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <math.h>
#include <map/MapElement.hh>
#include <frames/pose3.h>
#include <cspecs/CSpecs.hh>
#include <dgcutils/DGCutils.hh>

#define EXTRA_WIDTH 1.5

pose2 Utils::m_transLS;
CMapElementTalker Utils::m_meTalker;
bool Utils::m_ignoreObstacle;

int Utils::init()
{
  // Initialize the map element talker
  m_meTalker.initSendMapElement(CmdArgs::sn_key);

  return 0;
}

void Utils::destroy()
{
  return;
}

bool Utils::isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane)
{
  point2arr lb, rb;
  map->getLaneBounds(lb, rb, lane);

  double ldist = me.dist(lb, GEOMETRY_LINE);
  double rdist = me.dist(rb, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

bool Utils::isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound)
{
  double ldist = me.dist(leftBound, GEOMETRY_LINE);
  double rdist = me.dist(rightBound, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

LaneLabel Utils::getCurrentLane(VehicleState &vehState, Map *map)
{
  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  LaneLabel lane;
  map->getLane(lane, currPos);
  return lane;
}

LaneLabel Utils::getCurrentLane(VehicleState &vehState, PlanGraph *graph)
{
  LaneLabel lane;

  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  PlanGraphNode *node = graph->getNearestPos(currPos.x, currPos.y, 5.0, 
                                             PLAN_GRAPH_NODE_LANE, PLAN_GRAPH_NODE_ONCOMING);
  if (node) {
    lane.segment = node->segmentId;
    lane.lane = node->laneId;
  } else {
    lane.segment = 0;
    lane.lane = 0;
  }
  return lane;
}

LaneLabel Utils::getCurrentLaneAnyDir(VehicleState &vehState, PlanGraph *graph)
{
  LaneLabel lane;

  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  PlanGraphNode *node = graph->getNearestPos(currPos.x, currPos.y, 5.0, 
                                             PLAN_GRAPH_NODE_LANE, PLAN_GRAPH_NODE_NONE);
  if (node) {
    lane.segment = node->segmentId;
    lane.lane = node->laneId;
  } else {
    lane.segment = 0;
    lane.lane = 0;
  }
  return lane;
}

LaneLabel Utils::getDesiredLane(VehicleState &vehState, PlanGraph *graph)
{
  LaneLabel lane;

  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  PlanGraphNode *node = graph->vehicleNode;
  if (node) {
    lane.segment = node->segmentId;
    lane.lane = node->laneId;
  } else {
    node = graph->getNearestPos(currPos.x, currPos.y, 1.0, 
                                PLAN_GRAPH_NODE_LANE, PLAN_GRAPH_NODE_ONCOMING);
    if (node) {
      lane.segment = node->segmentId;
      lane.lane = node->laneId;
    } else {
      lane.segment = 0;
      lane.lane = 0;
    }
  }
  return lane;
}

bool Utils::isReverse(VehicleState &vehState, Map *map, LaneLabel &lane)
{
  double currlane_angle;
  point2 pt;

  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearaxle = AliceStateHelper::getPositionRearAxle(vehState);

  map->getHeading(currlane_angle, pt, lane, alice_rearaxle);
  double diff_angle = fabs(getAngleInRange(alice_angle-currlane_angle));
  
  return (diff_angle > M_PI/2);
}

#define MAX_NODES 40

double Utils::getDistToStopline(PlanGraphPath* m_path)
{
  if (m_path->hasStop) {
    return m_path->stopDistAlongPath;
  }
  else
    return INFINITY;
}

double Utils::getDistToStopline(PlanGraphPath* m_path, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal)
{
  // initialize SegGoal
  segGoal.entrySegmentID = 0;
  segGoal.entryLaneID = 0;
  segGoal.entryWaypointID = 0;
  segGoal.exitSegmentID = 0;
  segGoal.exitLaneID = 0;
  segGoal.exitWaypointID = 0;

  
  if (m_path->hasStop) {

    // try finding corresponding seg goal
    for (unsigned int j=0; j<seg_goal_queue->size(); j++) {
      SegGoals *goal =  &((*(seg_goal_queue))[j]);
      if (goal->segment_type != SegGoals::INTERSECTION) continue;

      if (goal->entrySegmentID == m_path->stopWaypoint->segmentId && goal->entryLaneID == m_path->stopWaypoint->laneId && goal->entryWaypointID == m_path->stopWaypoint->waypointId) {
        segGoal=*goal;
        return m_path->stopDistAlongPath;
      }
    }

    if (m_path->stopIndex == -1) {
      Log::getStream(1)<<"getDistToStopline: Did not find corresponding stop line. Also no m_path->stopIndex available."<<endl;
      return m_path->stopDistAlongPath;
    }

    // if no corresponding seg goal is found, fake it
    RNDFGraphWaypoint *entry, *exit;
    entry = m_path->nodes[m_path->stopIndex]->nextWaypoint;

    if (entry == NULL)
      return INFINITY;
    // If there is no corresponding stopline found in seggoals, return waypoint of stop node and fake the Intersection Type
    for (int k= m_path->stopIndex; k < m_path->pathLen; k++) {      
      PlanGraphNode* node3 = m_path->nodes[k];
      
      if (node3 == NULL)
        continue;
      
      exit = node3->prevWaypoint;
      if (exit == NULL)
        continue;
  
      if (exit->waypointId != 0 && entry->segmentId!=exit->segmentId && entry->laneId!=exit->laneId && entry->waypointId!=exit->waypointId) {
        segGoal.entrySegmentID = m_path->stopWaypoint->segmentId;
        segGoal.entryLaneID = m_path->stopWaypoint->laneId;
        segGoal.entryWaypointID = m_path->stopWaypoint->waypointId;
        
        segGoal.exitSegmentID = exit->segmentId;
        segGoal.exitLaneID = exit->laneId;
        segGoal.exitWaypointID = exit->waypointId;
        segGoal.segment_type = SegGoals::INTERSECTION;
        segGoal.intersection_type = SegGoals::INTERSECTION_STRAIGHT;
        Log::getStream(3)<<"getDistToStop did not find corresponding SegGoal. Send entry="<<segGoal.entrySegmentID<<"."<<segGoal.entryLaneID<<"."<<segGoal.entryWaypointID<<" & exit="<<segGoal.exitSegmentID<<"."<<segGoal.exitLaneID<<"."<<segGoal.exitWaypointID<<" instead."<<endl;
        return m_path->stopDistAlongPath;
      }
    }
  }

  return INFINITY;
}


// double Utils::getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal)
// {
//   // Get current lane
//   LaneLabel current_lane(seg_goal.exitSegmentID, seg_goal.exitLaneID);

//   return getDistToStopline(vehState, map, current_lane);
// }

double Utils::getDistToStopline(point2 &pos, Map *map, LaneLabel &current_lane)
{
  // Get all stoplines on that lane
  vector<PointLabel> stoplines;
  map->getLaneStopLines(stoplines, current_lane);

  // Get center line
  point2arr centerline;
  map->getLaneCenterLine(centerline, current_lane);

  // extend centerline in case that stop line is at the beginning/end of centerline
  point2arr temp, extendCenterline;
  map->extendLine(temp, centerline, 20);
  map->extendLine(extendCenterline, temp, -20);


  // Find the closest stopline on the lane on front of use
  point2 stop_position;
  double dist, min_dist = -1;
  for (unsigned int i=0; i<stoplines.size(); i++) {
    map->getWaypoint(stop_position, stoplines[i]);
    map->getDistAlongLine(dist, extendCenterline, stop_position, pos);
    if (dist > -1.0 && (dist < min_dist || min_dist == -1)) {
      min_dist = dist;
    }
  }

  return min_dist;
}

// double Utils::getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &current_lane)
// {
//   // Set position
//   point2 currFrontPos;
//   currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
//   return getDistToStopline(currFrontPos, map, current_lane);
// }


double Utils::getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal)
{
  double dist;
  LaneLabel current_lane;
  map->getLane(current_lane, AliceStateHelper::getPositionFrontBumper(vehState));

  LaneLabel waypoint_lane(seg_goal.entrySegmentID, seg_goal.entryLaneID);

  if (current_lane == waypoint_lane) {
    point2arr centerline;
    map->getLaneCenterLine(centerline, waypoint_lane);
    point2 entry_position;
    PointLabel entryWaypoint = PointLabel(seg_goal.entrySegmentID, seg_goal.entryLaneID, seg_goal.entryWaypointID);
    map->getWaypoint(entry_position, entryWaypoint);

    map->getDistAlongLine(dist, centerline, entry_position, AliceStateHelper::getPositionFrontBumper(vehState));
  }

  return dist;
}

double Utils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

double Utils::getNearestObsInLane(MapElement ** me, VehicleState &vehState, Map *map, LaneLabel &lane)
{
  vector<MapElement> obstacles;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  bool is_reverse = isReverse(vehState, map, lane);

  int obsErr = map->getObsInLane(obstacles, lane);
  if (obsErr < 1) //no obstacles
    return -1;

  int obs_index = -1;
  double min_dist = INFINITY;
  double dist;
  point2 obs_pt;
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  for (unsigned int i=0; i<obstacles.size(); i++) {
    for (unsigned int j=0; j<obstacles[i].geometry.size(); j++) {
      obs_pt.set(obstacles[i].geometry[j]);
      map->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
      if (is_reverse) dist = -dist;
      if (dist > 0.0 && dist < min_dist) {
        min_dist = dist;
        obs_index = i;
      }
    }
  }

  if (min_dist == INFINITY)
    return -1;

  if (me != NULL) {
    *me = &obstacles[obs_index];
  }

  return min_dist; 
}

double Utils::getNearestObsOnPath(PlanGraphPath* path, PlanGraph* graph, VehicleState vehState)
{
  double distance = INFINITY;
  int i = 0;
  if ( (path->pathLen == 0) || (path==NULL) ) {
    //    Console::addMessage("distant to closest obs on path (no path) = %3.2f", distance);
    //    Log::getStream(1) << "distant to closest obs on path (no path) = " << distance << endl;
    return distance;
  }
  Console::addMessage("Collision on path? %d  Ignore obstacle? %d", path->collideObs, m_ignoreObstacle);
  // if no collision return inf
  if ( !(path->collideObs) ) {
    //    Console::addMessage("distant to closest obs on path (no collision) = %3.2f", distance);
    // Log::getStream(1) << "distant to closest obs on path (no collision) = " << distance << endl;
    return distance;
  } else {
    // set dist to zero and start calculating
    distance = 0.0;
  }

  // do we want to ignore some of the obstacle?
  if (m_ignoreObstacle) {
    // step along the path and ignore the first obstacle
    for (i = 0; i<path->pathLen; i++) { 
      // step out when the obstacle status of the node changes (i.e., the status is not fresh anymore
      if (!graph->isObsCollision(path->nodes[i]))
        break;
          
      // update the distance that we have stepped along this path
      if (i != 0)
        distance += vec2f_mag(vec2f_sub(path->nodes[i]->pose.pos, path->nodes[i-1]->pose.pos));
    }
  }
  
  // max distance that we want to check?
  double distToStopObs = MAX(pow(AliceStateHelper::getVelocityMag(vehState),2)/(2*0.5),30);

  // how far along that path are we?
  double distAlongPath = 0.0;
  if (path->pathLen>1) {
    point2arr pathArr;
    point2 tmpPt;
    for (int j=0; j<path->pathLen; j++) {
      tmpPt.set(path->nodes[j]->pose.pos.x, path->nodes[j]->pose.pos.y);
      pathArr.push_back(tmpPt);
    }
    Utils::distToProjectedPoint(tmpPt, distAlongPath, AliceStateHelper::getPositionRearAxle(vehState), 
				AliceStateHelper::getHeading(vehState), pathArr, 10, 10);
  }

  // get the distance to the first obstacle node
  for (; i<path->pathLen; i++) {
    // update the distance that we have stepped along this path
    if (i != 0)
      distance += vec2f_mag(vec2f_sub(path->nodes[i]->pose.pos, path->nodes[i-1]->pose.pos));

    // if this distance is greater that what we are interested in then step out of loop
    if (distance > distToStopObs) {
      //      Console::addMessage("distant to closest obs on path (too far) = %3.2f", INFINITY);
      //      Log::getStream(1) << "distant to closest obs on path (too far) = " << INFINITY << endl;
      return INFINITY;
    }

    if ( graph->isObsCollision(path->nodes[i]) )
      break;
  }
  //  Console::addMessage("distant to closest obs on path (stepped through) = %3.2f at %d", (distance - DIST_REAR_AXLE_TO_FRONT - distAlongPath), i);
  //  Log::getStream(1) << "distant to closest obs on path (stepped through) = " << (distance - DIST_REAR_AXLE_TO_FRONT - distAlongPath) << " at " << i << endl;
  return distance - distAlongPath;
}

int Utils::getNearestObsNodeOnPath(PlanGraphPath* path, PlanGraph* graph)
{
  int i = 0;
  if ( (path->pathLen == 0) || (path==NULL) ) {
    return -1;
  }
  Console::addMessage("Collision on path? %d  Ignore obstacle? %d", path->collideObs, m_ignoreObstacle);
  // if no collision return inf
  if ( !(path->collideObs) ) {
    return -1;
  }

  // do we want to ignore some of the obstacle?
  if (m_ignoreObstacle) {
    // step along the path and ignore the first obstacle
    for (i = 0; i<path->pathLen; i++) { 
      // step out when the obstacle status of the node changes (i.e., the status is not fresh anymore
      if (!graph->isObsCollision(path->nodes[i]) )
        break;
    }
  }
  
  // get the index of the first obstacle node
  for (; i<path->pathLen; i++) {
    if ( graph->isObsCollision(path->nodes[i]) )
      break;
  }

  // assign the index
  if (i == path->pathLen)
	// we got to the end without an obstacle - possible since we may ignore some obstacles
	return -1;
  else
	return i;
}

double Utils::getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane)
{
  return getNearestObsInLane(NULL, vehState, map, lane);
}

double Utils::monitorAliceSpeed(VehicleState &vehState, int m_estop)
{
  static bool first_time_stopped = true;
  static uint64_t stopped_since = 0;
  double current_velocity = AliceStateHelper::getVelocityMag(vehState);

  if (m_estop != EstopRun) {
    first_time_stopped = true;
    return 0.0;
  }

  if (current_velocity < 0.2) {
    if (first_time_stopped) {
      stopped_since = getTime();
      first_time_stopped = false;
    }
    return (double)(getTime()-stopped_since)/1000000.0;
  } else {
    first_time_stopped = true;
    return 0.0;
  }
  return 0.0;
}

void Utils::updateStoppedMapElements(Map *map)
{
  MapElement mapEl;
  point2 point;
  LaneLabel obs_lane;
  double dist;

  for (int j = 0; j < (int)map->usedIndices.size(); j++) {
    map->getFusedEl(mapEl,j);

    if (!mapEl.isVehicle()) continue;
    if (mapEl.timeStopped < 10.0) continue;

    // Check if obstacle is close to an intersection
    point.set(mapEl.center);
    map->getLane(obs_lane, point);
    dist = getDistToStopline(point, map, obs_lane);
    if (dist < 30) {
      // We have at most 4 roads going into an intersection so each vehicle
      // at the stopline has to wait at most 30 seconds
      if (mapEl.timeStopped > dist*40.0/(1.5*VEHICLE_LENGTH)) // MAGIC
        mapEl.type = ELEMENT_OBSTACLE;
    } else {
      mapEl.type = ELEMENT_OBSTACLE;
    }
  }
}
 

uint64_t Utils::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

int Utils::reverseProjection(point2 &out, point2 pt, double yaw, point2arr on, double max_dist, double extension)
{
  bool swapped;

  /* for all segments on the point2arr, find the intersection */
  point2 pt1, pt2, tmpPt;
  for (unsigned int i=0; i<on.size()-1; i++) {
    swapped = false;

    pt1.set(on[i]-pt);
    pt2.set(on[i+1]-pt);

    pt1 = pt1.rot(-yaw);
    pt2 = pt2.rot(-yaw);

    /* Rotate pt1 and pt2 to the pt/yaw coords */
    if (pt1.x > pt2.x) {
      swapped = true;
      tmpPt.set(pt2);
      pt2.set(pt1);
      pt1.set(tmpPt);
    } else if (pt1.x == pt2.x) {
      if (0 != pt1.x) continue;
      else {
        if (pt1.y > pt2.x) {
          tmpPt.set(pt2);
          pt2.set(pt1);
          pt1.set(tmpPt);
        }
        if ((0 < pt1.y) && (fabs(pt1.y) <= max_dist)) {
          pt1.rot(yaw); out.set(pt1 + pt);
          return 0;
        } else if ((0 > pt2.y) && (fabs(pt2.y) <= max_dist)) {
          pt2.rot(yaw); out.set(pt2+pt);
          return 0;
        } else if ((0 <= pt2.y) && (0 >= pt1.y)) {
          out.set(pt);
          return 0;
        }
        continue;
      }
    }

    /* Check for intersection */
    if (i == 0) {
      if (swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (i == on.size()-2) {
      if (!swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (0 < pt1.x || 0 > pt2.x) continue;

    double a = (pt2.y-pt1.y)/(pt2.x-pt1.x);
    double b = pt1.y - a*pt1.x;
    out.x = 0;
    out.y = b;
    if (fabs(b) > max_dist) continue;
    out = out.rot(yaw);
    out.set(out + pt);
    return 0;
  }
  return -1;
}

int Utils::distToProjectedPoint(point2 &out, double &distance, point2 pt, double yaw, point2arr on, double max_dist, double extension)
{
  if (on.size() < 1)
    return -1;

  bool swapped;
  distance = 0.0;
  
  /* for all segments on the point2arr, find the intersection */
  point2 pt1, pt2, tmpPt;
  for (unsigned int i=0; i<on.size()-1; i++) {
    swapped = false;

    pt1.set(on[i]-pt);
    pt2.set(on[i+1]-pt);

    pt1 = pt1.rot(-yaw);
    pt2 = pt2.rot(-yaw);

    /* calculate the current dist */
    distance += pt1.dist(pt2);

    /* Rotate pt1 and pt2 to the pt/yaw coords */
    if (pt1.x > pt2.x) {
      swapped = true;
      tmpPt.set(pt2);
      pt2.set(pt1);
      pt1.set(tmpPt);
    } else if (pt1.x == pt2.x) {
      if (0 != pt1.x) continue;
      else {
        if (pt1.y > pt2.x) {
          tmpPt.set(pt2);
          pt2.set(pt1);
          pt1.set(tmpPt);
        }
        if ((0 < pt1.y) && (fabs(pt1.y) <= max_dist)) {
          pt1.rot(yaw); out.set(pt1 + pt);
          return 0;
        } else if ((0 > pt2.y) && (fabs(pt2.y) <= max_dist)) {
          pt2.rot(yaw); out.set(pt2+pt);
          return 0;
        } else if ((0 <= pt2.y) && (0 >= pt1.y)) {
          out.set(pt);
          return 0;
        }
        continue;
      }
    }

    /* Check for intersection */
    if (i == 0) {
      if (swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (i == on.size()-2) {
      if (!swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (0 < pt1.x || 0 > pt2.x) continue;

    double a = (pt2.y-pt1.y)/(pt2.x-pt1.x);
    double b = pt1.y - a*pt1.x;
    out.x = 0;
    out.y = b;
    if (fabs(b) > max_dist) continue;
    /* If we used the extension, it is little more tricky */
    if (0 < pt1.x || 0 > pt2.x) {
      if (i == 0) {
        if (swapped) distance -= pt1.dist(out);
        else distance -= pt2.dist(out);
      } else {
	if (swapped) distance += pt1.dist(out);
	else distance += pt2.dist(out);
      }
    /* Substract the extra distance we added previously */
    } else {
      if (swapped) distance -= pt1.dist(out);
      else distance -= pt2.dist(out);
    }
    out = out.rot(yaw);
    out.set(out + pt);
    return 0;
  }
  return -1;
}


void Utils::printErrorInBinary(Err_t error, int log_level)
{
  //  int bit;
  Log::getStream(log_level) << "error = " << error;
  Log::getStream(log_level) << " and in Binary (reversed!!) = ";
  
  int i = 0;
  while ((int)error >= pow(2,i)) {
    //    bit = (((int)error >> i) & 1);((int)error >> i) & 1;
    Log::getStream(log_level) << (((int)error >> i) & 1);
    i++;
  }
  Log::getStream(log_level) << endl;

  return;

}

void Utils::printCSpecs(CSpecs_t* cSpecs, int log_level)
{
  Log::getStream(log_level) << "cSpecs: " << endl;
  Log::getStream(log_level) << "Initial state = " << cSpecs->getStartingState() << endl;
  Log::getStream(log_level) << "Initial controls = " << cSpecs->getStartingControls() << endl;
  Log::getStream(log_level) << "Final state = " << cSpecs->getFinalState() << endl;
  Log::getStream(log_level) << "Final controls = " << cSpecs->getFinalControls() << endl;
  Log::getStream(log_level) << "Perimeter = " << cSpecs->getBoundingPolygon() << endl;

}

void Utils::printTraj(CTraj* traj, int log_level)
{
//   Log::getStream(log_level) << "Trajectory (x, y, vel, acc, theta) = " << endl;
//   for (int i=0; i<traj->getNumPoints(); i++) {
//     Log::getStream(log_level) << traj->getNorthing(i) << " " << traj->getEasting(i) << " " << sqrt( pow(traj->getNorthingDiff(i, 1),2) + pow(traj->getEastingDiff(i, 1),2) ) << " " << sqrt( pow(traj->getNorthingDiff(i, 2),2) + pow(traj->getEastingDiff(i, 2),2) ) << " " << atan2(traj->getEastingDiff(i,1), traj->getNorthingDiff(i,1)) << endl;
//     }
  return;
}

void Utils::printPath(PlanGraphPath* path, int log_level)
{
  Log::getStream(log_level) << "PathPlanner::print - Printing the path" << endl;
  for (int i=0; i<path->pathLen; i++) {
    if (path->nodes[i]) {
      Log::getStream(log_level) << "Path node " << i << " = (" << path->nodes[i]->pose.pos.x << "," << path->nodes[i]->pose.pos.y << "), heading = " << path->nodes[i]->pose.rot << " direction = " << path->directions[i] << endl;
// BOGUS FIX <<  " isStop = " << path->nodes[i]->flags.isStop <<endl;
    }
  }
}




// Set the site frame to local frame transform
void Utils::setSiteToLocal(const VehicleState *vehState)
{
  pose3_t transSV, transLV, transLS;
  double m[4][4];
    
  // Construct transform from vehicle to site frame
  transSV.pos = vec3_set(vehState->siteNorthing, vehState->siteEasting, vehState->siteAltitude);
  transSV.rot = quat_from_rpy(vehState->siteRoll, vehState->sitePitch, vehState->siteYaw);

  // Construct transform from vehicle to local frame
  transLV.pos = vec3_set(vehState->localX, vehState->localY, vehState->localZ);
  transLV.rot = quat_from_rpy(vehState->localRoll, vehState->localPitch, vehState->localYaw);
  
  // Construct transform from site to vehicle frame
  transLS = pose3_mul(transLV, pose3_inv(transSV));
  pose3_to_mat44d(transLS, m);

  m_transLS.x = m[0][3];
  m_transLS.y = m[1][3];
  m_transLS.ang = atan2(m[1][0], m[0][0]);
    
  return;
}


void Utils::displayGraph(int sendSubgroup, PlanGraph* graph, VehicleState vehState, int vis_level)
{
  if (vis_level > CmdArgs::visualization_level) return;

  if (graph->nodeCount == 0) return;

  MapElement me;
    
  // DISPLAY THE GRAPH
  point2 point, vehPos;
  vector<point2> free_points;
  vector<point2> obs_points8;
  vector<point2> obs_points6;
  vector<point2> obs_points4;
  vector<point2> obs_points2;
  vector<point2> obs_points0;
  vector<point2> car_points;
  vector<point2> volatile_points;
  vector<point2arr> zone_points;
  vector<point2> stop_points;
  vector<PlanGraphNode *> nodes;
  MapId free_mapId, obs8_mapId, obs6_mapId, obs4_mapId, obs2_mapId, obs0_mapId, car_mapId, stop_mapId, volatile_mapId;
  int zone_mapId;
  PlanGraphNode* node;
    
  double dotProd, dist;
  double heading = vehState.localYaw;

  point2arr points;

  vehPos.set(vehState.siteNorthing, vehState.siteEasting);
  free_mapId = 10000;
  obs8_mapId = 10001;
  obs6_mapId = 10006;
  obs4_mapId = 10007;
  obs2_mapId = 10008;
  obs0_mapId = 10009;
  car_mapId = 10002;
  zone_mapId = 10003;
  stop_mapId = 10004;
  volatile_mapId = 10005;
    
  graph->getRegion(&nodes, vehState.siteNorthing, vehState.siteEasting, 50, 50); // MAGIC
  for (unsigned int i=0; i<nodes.size(); i++) {
    node = nodes[i];
    if ( !node->flags.isLane && !node->flags.isTurn && !node->flags.isZone)
      continue;
	if ( node->flags.isOncoming)
	  continue;
    point.set(node->pose.pos.x, node->pose.pos.y);
    dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
    dist = point.dist(vehPos);
    if (dotProd>-10 && dist<25) {
      if (node->flags.isStop)
        stop_points.push_back(point);
      else if ( (node->status.obsCollision)  && (graph->isStatusFresh(node->status.obsTime)) )
      {
	if(node->status.objConf > 0.8)
	  obs_points8.push_back(point);
	else if(node->status.objConf > 0.6)
	  obs_points6.push_back(point);
	else if(node->status.objConf > 0.4)
	  obs_points4.push_back(point);
	else if(node->status.objConf > 0.2)
	  obs_points2.push_back(point);
	else
	  obs_points0.push_back(point);
      }
      else if ( (node->status.carCollision) && (graph->isStatusFresh(node->status.carTime)) )
        car_points.push_back(point);
      else if (node->flags.isZone) {
        if (node->interId % 10 == 0) {
          points.push_back(point);         
          point.set(point.x+1*cos(node->pose.rot), point.y+1*sin(node->pose.rot));          
          points.push_back(point);
          zone_points.push_back(points);
          points.clear();
        }
      } else if (free_points.size() < 500) {
        free_points.push_back(point);
      }
    }
  }
  nodes.clear();

  /* TODO
  // Get the volatile nodes
  for (int i=graph->getStaticNodeCount(); i < graph->getNodeCount(); i++) { 
  node = graph->getNode(i);
  if ( node->type != GRAPH_NODE_VOLATILE ) continue;
  point.set(node->pose.pos.x, node->pose.pos.y);
  volatile_points.push_back(point);
  }
  */

  // Send volatile nodes
  me.setId(volatile_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_GREEN, 100);
  me.setGeometry(volatile_points);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  // Send free nodes
  me.setId(free_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_LIGHT_BLUE, 100);
  me.setGeometry(free_points);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  // Send nodes occupied by an obstacle  --for various confidence levels
  me.setId(obs8_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_YELLOW, 100);
  me.setGeometry(obs_points8);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  me.setId(obs6_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_DARK_YELLOW, 100);
  me.setGeometry(obs_points6);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  me.setId(obs4_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_DARK_GREEN, 100);
  me.setGeometry(obs_points4);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  me.setId(obs2_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_PURPLE, 100);
  me.setGeometry(obs_points2);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  me.setId(obs0_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_PINK, 100);
  me.setGeometry(obs_points0);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  

  // Send nodes occupied by a car
  me.setId(car_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_ORANGE,100);
  me.setGeometry(car_points);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);

  // Send zone nodes (to display heading info too)
  for (int i=0; i<(int)zone_points.size(); i++) {
    zone_mapId += i;
    me.setId(zone_mapId);
    me.setTypeLine();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(zone_points[i]);
    me.rotate(m_transLS.ang);
    me.translate(m_transLS.x, m_transLS.y);     
    m_meTalker.sendMapElement(&me,sendSubgroup);
  }

  // Send nodes with stopline
  me.setId(stop_mapId);
  me.setTypePoints();
  me.setColor(MAP_COLOR_RED,100);
  me.setGeometry(stop_points);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);
}


void Utils::displayPath(int sendSubgroup, PlanGraphPath* path, int vis_level, MapElementColorType colorType, MapId mapId)
{
  if (vis_level > CmdArgs::visualization_level) return;

  if (path->pathLen == 0 || path->nodes[0] == NULL) return;

  point2 point;
  vector<point2> points;
  point2 start, end;
  PlanGraphNode* node;
  MapElement me;
  
  if (path->pathLen > 0) {
    for (int i=0; i<path->pathLen; i++) {
      node = path->nodes[i];
      if (!node) break;
      point.set(node->pose.pos.x,node->pose.pos.y);
      points.push_back(point);
    }
    me.setId(mapId);
    me.setTypeLine();
    if (path->nodes[0] && path->directions[0] == PLAN_GRAPH_PATH_REV)
      me.setColor(MAP_COLOR_YELLOW,100);
    else 
      me.setColor(colorType, 100);
    me.setGeometry(points);
    me.rotate(m_transLS.ang);
    me.translate(m_transLS.x, m_transLS.y);     
    m_meTalker.sendMapElement(&me,sendSubgroup);
  }
}

void Utils::displayTraj(int sendSubgroup, CTraj* traj, int vis_level)
{
  if (vis_level > CmdArgs::visualization_level) return;

  MapElement me;
  static int counter=140000;
  point2 point;
  vector<point2> points;
  MapId mapId(counter);
    
  //mapId = counter;
  for (int i=0; i<traj->getNumPoints(); i++) {
    point.set(traj->getNorthing(i),traj->getEasting(i));
    points.push_back(point);
  }
  me.setId(mapId);
  me.setTypePlanningTraj();
  me.setGeometry(points);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);
  m_meTalker.sendMapElement(&me,sendSubgroup);
}


void Utils::displayLine(int sendSubgroup, point2arr line, int vis_level, MapElementColorType colorType, MapId mapId)
{
  if (vis_level > CmdArgs::visualization_level) return;

  MapElement me;
  me.setId(mapId);
  me.setTypeLine();
  me.setGeometry(line);
  me.setColor(colorType);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);     
  m_meTalker.sendMapElement(&me,sendSubgroup);
}


void Utils::displayPoints(int sendSubgroup, point2arr points, int vis_level, MapElementColorType colorType, MapId mapId)
{
  if (vis_level > CmdArgs::visualization_level) return;
  MapElement me;
  me.setId(mapId);
  me.setTypePoints();
  me.setGeometry(points);
  me.setColor(colorType);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);
  m_meTalker.sendMapElement(&me,sendSubgroup);
}

void Utils::displayCircle(int sendSubgroup, point2 pt, double radius, int vis_level, MapElementColorType colorType, MapId mapId)
{
  if (vis_level > CmdArgs::visualization_level) return;
  MapElement me;
  me.setId(mapId);
  me.setTypePoints();
  me.setGeometry(pt, radius);
  me.setPosition(pt);
  me.setColor(colorType);
  me.rotate(m_transLS.ang);
  me.translate(m_transLS.x, m_transLS.y);
  m_meTalker.sendMapElement(&me,sendSubgroup);
}

void Utils::displayGraphHeadings(int sendSubgroup, PlanGraph* graph, VehicleState vehState, int vis_level, double bound)
{
  if (vis_level > CmdArgs::visualization_level) return;

  if (graph->nodeCount == 0) return;

  MapElement me;
  me.setTypeLine();
  me.setColor(MAP_COLOR_LIGHT_BLUE,100);
  
  // DISPLAY THE GRAPH
  point2 point, vehPos;
  int mapId = 14000;
  PlanGraphNode* node;
  vector<PlanGraphNode *> nodes;
  point2 tmpPt1, tmpPt2;
  point2arr tmpPtArr;
  
  double dotProd, dist;
  double heading = vehState.localYaw;
  
  vehPos.set(vehState.localX, vehState.localY);
  
  graph->getRegion(&nodes, vehState.siteNorthing, vehState.siteEasting, 2 * bound, 2 * bound);
  for (unsigned int i=0; i<nodes.size(); i++) {
    node = nodes[i];
    if (!node->flags.isLane && !node->flags.isTurn && !node->flags.isZone) continue;
    if (node->flags.isOncoming) continue;
    tmpPt1.set(node->pose.pos.x, node->pose.pos.y);
    dotProd = (tmpPt1.x-vehPos.x)*cos(heading) + (tmpPt1.y-vehPos.y)*sin(heading);
    dist = tmpPt1.dist(vehPos);
    if (dotProd>-10 && dist<bound) {
      tmpPt2.set(tmpPt1.x+cos(node->pose.rot), tmpPt1.y+sin(node->pose.rot));
      tmpPtArr.push_back(tmpPt1);
      tmpPtArr.push_back(tmpPt2);
      
      me.setId(mapId);
      me.setGeometry(tmpPtArr);
      me.rotate(m_transLS.ang);
      me.translate(m_transLS.x, m_transLS.y);     
      m_meTalker.sendMapElement(&me,sendSubgroup);
      
      mapId++;
      tmpPtArr.clear();
    }
  }
}

void Utils::displayElement(int sendSubgroup, MapElement me, int vis_level)
{
  if (vis_level > CmdArgs::visualization_level) return;

  m_meTalker.sendMapElement(&me,sendSubgroup);
}


void Utils::displayCollision(int sendSubgroup, point2 p, double size)
{
  MapElement me;

  me.setId(MODtrafficplanner,400);
  me.setTypePredictedVehicle();
  me.setColor(MAP_COLOR_RED);
  me.setGeometry(p, size);
  me.setPosition(p);

  m_meTalker.sendMapElement(&me,sendSubgroup);
}

void Utils::displayClearCollision(int sendSubgroup)
{
  MapElement me;

  me.setId(MODtrafficplanner,400);
  me.setTypeClear();

  m_meTalker.sendMapElement(&me,sendSubgroup);
}

void Utils::displayPrediction(int sendSubgroup, point2arr poly, MapElementColorType colorType, MapId mapId)
{
  MapElement me;
  me.setId(mapId);
  me.setTypePredictedVehicle();
  me.setGeometry(poly);
  me.setColor(colorType);

  m_meTalker.sendMapElement(&me,sendSubgroup);
}

void Utils::displayClear(int sendSubgroup, int vis_level, MapId mapId)
{
  if (vis_level > CmdArgs::visualization_level) return;

  MapElement me;
  me.setId(mapId);
  me.setTypeClear();
  m_meTalker.sendMapElement(&me,sendSubgroup);
}

void Utils::displayPlannerStatus(string region, string state, string planner, string flag)
{
  MapElement debugEl;
  debugEl.clear();
  debugEl.setTypeDebug();
  debugEl.setId(-1,3); // second number revers to the the row
  debugEl.setLabel(0,string("Region: ")+region);
  debugEl.setLabel(1,string("State: ")+state);
  debugEl.setLabel(2,string("Planner: ")+planner);
  m_meTalker.sendMapElement(&debugEl,-2);

  debugEl.clear();
  debugEl.setTypeDebug();
  debugEl.setId(-1,2); // second number revers to the the row
  debugEl.setLabel(0,string("Flag: ")+flag);
  m_meTalker.sendMapElement(&debugEl,-2);
}

void Utils::displayIntersectionState(string intersection)
{
  MapElement debugEl;
  debugEl.clear();
  debugEl.setTypeDebug();
  debugEl.setId(-1,1); // second number revers to the the row
  debugEl.setLabel(0,string("Intersection state: ")+intersection);
  m_meTalker.sendMapElement(&debugEl,-2);
}

void Utils::printCurrPose(pose2f_t pose, int log_level)
{
  Log::getStream(log_level) << "Initial pos = (" << pose.pos.x << "," << pose.pos.y << ") and heading = " << pose.rot << endl;
}


int Utils::updatePathStoplines(PlanGraphPath *path, Map *map, PlanGraph *graph, VehicleState vehState)
{
  unsigned long long time1,time2,timeS,timeE;
  double regionX = 0.0;
  static double avgTime = 0.0;
  static int avgCounter = 0;

  DGCgettime(timeS);
  PlanGraphNode *node;
  RNDFGraphWaypoint *wp;
  PointLabel stopline;
  point2 stopPt, projectedStopPt;
  point2arr pathArray;

  if (path->pathLen == 0)
  {
    path->hasStop = false;
    return 0;
  }

  pathArray.clear();
  point2 alice=AliceStateHelper::getPositionFrontBumper(vehState);
  double minX = alice.x;
  double minY = alice.y;
  double maxX = alice.x;
  double maxY = alice.y;

  // create point2arr out of node
  for (int l=0; l<path->pathLen; l++) {
    node = path->nodes[l];
    if (node == NULL) return -1;

    if (node->pose.pos.x<minX)
      minX = node->pose.pos.x;
    if (node->pose.pos.x>maxX)
      maxX = node->pose.pos.x;
    if (node->pose.pos.y<minY)
      minY = node->pose.pos.y;
    if (node->pose.pos.y>maxY)
      maxY = node->pose.pos.y;
   
    pathArray.push_back(point2(node->pose.pos.x, node->pose.pos.y));
    if (path->dists[l] > 70.0) break;
  }

  // initialize and assume that there is no stop line
  path->hasStop = false;

  // find all stop nodes within a 5x5m square
  PlanGraphNodeList listStopnodes;
  DGCgettime(time1);
  graph->getRegion(&listStopnodes, (maxX+minX)/2.0, (maxY+minY)/2.0, maxX-minX+10.0, maxY-minY+10.0, PLAN_GRAPH_NODE_STOP, PLAN_GRAPH_NODE_ONCOMING);
  DGCgettime(time2);
  regionX+=(time2-time1)/1000.0;

  // step through all found stop nodes and find those with similar heading
  double distStopline = INFINITY;
  double distRow = INFINITY;
  int index = -1;
  point2 p2;
  for (unsigned int l=0; l<listStopnodes.size(); l++) {
    // check heading
    double headingDiff;
    
    addAngles(headingDiff, AliceStateHelper::getHeading(vehState), -listStopnodes[l]->pose.rot);
    
    // if not within heading of Alice, discard
    if (headingDiff > M_PI/4 || headingDiff < -M_PI/4)
        continue;
    
    // and find closest stop line
    double dist;
    distToProjectedPoint(p2, dist, point2(listStopnodes[l]->pose.pos.x, listStopnodes[l]->pose.pos.y), listStopnodes[l]->pose.rot, pathArray, 10.0, 0.0);

    if (dist<70.0 && dist<distStopline) {
      distStopline = dist;
      index = l;
    }
  }
  
  bool hasRow = path->hasRow;
  if (hasRow) {
    PlanGraphNode* tempNode = graph->getWaypoint(path->rowWaypoint->segmentId, path->rowWaypoint->laneId,path->rowWaypoint->waypointId);
    distToProjectedPoint(p2, distRow, point2(path->rowWaypoint->px, path->rowWaypoint->py), tempNode->pose.rot, pathArray, 10.0, 0.0);
    if (distRow>70.0)
      hasRow = false;
  }

  // if ROW intersection, check whether a real stop line is closer
  if (index!=-1 && hasRow) {
    if (distStopline < distRow)
      wp = listStopnodes[index]->nextWaypoint;
    else
      wp = path->rowWaypoint;
  }
  else if (index == -1 && hasRow)
    wp = path->rowWaypoint;
  else if (index != -1 && !hasRow)
    wp = listStopnodes[index]->nextWaypoint;
  else
    wp = NULL;

  if (wp != NULL) {
    path->stopWaypoint = wp;
    
    stopPt = point2(wp->px, wp->py);
    // if update_stoplines through sensing is true, obtain sensed stop line. Otherwise use position of waypoint
    if (CmdArgs::update_stoplines && !(path->hasRow && wp == path->rowWaypoint)) {
      stopline.segment = wp->segmentId;
      stopline.lane    = wp->laneId;
      stopline.point   = wp->waypointId;
      // Get the actual stopline position from the map
      point2 tempStopPt;
      Log::getStream(1)<<"Sense stop line for "<<stopline<<"... Status ";
      int status = map->getStopLineSensed(tempStopPt, stopline);
      Log::getStream(1)<<status<<endl;
      if (status!=-1)
        stopPt = tempStopPt;
    }

    // get stop node. This is only needed to obtain rotation of stop line
    PlanGraphNode* stopNode = graph->getWaypoint(wp->segmentId, wp->laneId,wp->waypointId);

    // display the found stop point
    displayCircle(-2, stopPt, 1.2, 1, MAP_COLOR_RED, 1000);
    
    // get projected stop line in path and output it to mapviewer
    distToProjectedPoint(projectedStopPt, path->stopDistAlongPath, stopPt, stopNode->pose.rot, pathArray, 10.0, 10.0);
    displayCircle(-2, projectedStopPt, 1.1, 1, MAP_COLOR_YELLOW, 1001);
    
    // get Alice position along path
    point2 AlicePt;
    double distAlice;
    distToProjectedPoint(AlicePt, distAlice, AliceStateHelper::getPositionFrontBumper(vehState), AliceStateHelper::getHeading(vehState), pathArray, 10.0, 0);
    
    // get closest node to stop line; if not found, use index = 0
    int indexClosest = 0;
    for (int l=0; l<path->pathLen; l++) {
      node = path->nodes[l];
      if (node == NULL) return -1;
      
      if (path->dists[l] > path->stopDistAlongPath)
        break;
      indexClosest = l;
    }
    path->stopIndex = indexClosest;
    
    // compute distance between closest node and stop line itself
    double distNode;
    point2 p2;
    distToProjectedPoint(p2, distNode, point2( path->nodes[indexClosest]->pose.pos.x, path->nodes[indexClosest]->pose.pos.y), path->nodes[indexClosest]->pose.rot, pathArray, 10.0, 0.0);
    displayCircle(-2, point2( path->nodes[indexClosest]->pose.pos.x,  path->nodes[indexClosest]->pose.pos.y), 1.0, 1, MAP_COLOR_GREEN, 1002);
    
    path->stopDistNode = path->stopDistAlongPath - distNode;
    
    // adjust distAlongPath
    path->stopDistAlongPath -= distAlice;
    
    if (path->stopDistAlongPath<70.0)
      path->hasStop = true;
  }

  if (!path->hasStop) {
    displayClear(-2, 1, 1000);
    displayClear(-2, 1, 1001);
    displayClear(-2, 1, 1002);
  }

  DGCgettime(timeE);
  Log::getStream(1)<<"Performance: getRegion "<<regionX/1000.0<<endl;
  Log::getStream(1)<<"Performance: percente "<<regionX / ((timeE - timeS)/1000.0)<<endl;
  Log::getStream(1)<<"Performance: Total "<<(timeE - timeS)/1000.0<<endl;
  avgTime+=(timeE - timeS)/1000.0;
  avgCounter++;
  Log::getStream(1)<<"Performance: Average "<<avgTime/float(avgCounter)<<endl;

  return 0;
}

int Utils::updatePathLaneId(PlanGraphPath *path, PlanGraph *graph)
{
  PlanGraphNode *node;

  for (int l=0; l<path->pathLen; l++) {
    node = path->nodes[l];
    if (node == NULL) return -1;
    if (path->dists[l] > 70.0) break;

    // if segmentId == 0, need to update lane information
    if (node->segmentId == 0) {
      PlanGraphNode *laneNode = graph->getNearestPos(node->pose.pos.x, node->pose.pos.y, 20.0, PLAN_GRAPH_NODE_LANE, PLAN_GRAPH_NODE_ONCOMING);

      if (laneNode == NULL)
        return -1;

      node->segmentId = laneNode->segmentId;
      node->laneId = laneNode->laneId;
      node->waypointId = laneNode->waypointId;
    }
  }

  return 0;
}


int Utils::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

void Utils::setIgnoreMode(bool ignore)
{
  m_ignoreObstacle = ignore;
  return;
}
