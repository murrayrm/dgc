#ifndef GRAPHUTILS_HH_
#define GRAPHUTILS_HH_

#include <assert.h>
#include <float.h>
#include <string.h>

#include <frames/pose3.h>
#include <rndf/RNDF.hh>
#include "GraphUtils.hh"

// Common macros
#define MIN(a, b) ((a) <= (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

// Load an RNDF and initialize the graph.
int GraphPlanner::loadRndf(const char *filename)
{
  std::RNDF *rndf;
    
  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);

  // Load RNDF from file
  if (!rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);

  if (rndf->getNumOfSegments() <= 0)
    return ERROR("RNDF has no segments");
  
  // Create lane nodes
  if (this->genLanes(rndf) != 0)
    return -1;      

  // Create turn maneuvers
  if (this->genTurns(rndf) != 0)
    return -1;

  // Create lane-change nodes
  if (this->genChanges(rndf) != 0)
    return -1;

  // Create k-turn nodes
  //if (this->genKTurns() != 0)
  //  return -1;

  // Fix the static portion of the map so we can add volatile bits
  // for the vehicle.
  this->graph->freezeStatic();

  // Clean up
  delete rndf;

  MSG("created %d nodes %d arcs",
      this->graph->getNodeCount(), this->graph->getArcCount());
  
  return 0;
}

#endif /*GRAPHUTILS_HH_*/
