
/* 
 * Desc: Generates a dense graph for planning.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PLAN_GRAPH_HH
#define PLAN_GRAPH_HH


/** @file

@brief PlanGraph maintains a dense graph of interpolated vehicle
configurations (poses) based on the RNDF data.  This graph is used
for planning.

*/

#include <stdio.h>
#include <vector>
#include "frames/pose2.h"
#include "rndf/RNDFGraph.hh"



/// @brief Data for each node in the graph (a vehicle configuration).
struct PlanGraphNode
{
  /// Reference counter
  int16_t refCount;
  
  /// Unique node identifier 
  uint32_t nodeId;
  
  /// Pose in site frame
  pose2f_t pose;

  /// Nominal RNDF identifier.  Note that laneId is zero for zone
  /// perimeters.
  uint16_t segmentId, laneId, waypointId;

  /// Interpolation id (0 if the node is on top of a waypoint).
  uint16_t interId;

  /// Rail id (0 for the nominal center line).
  int8_t railId;

  /// Persistent flags (we make this bit-field a structure for easy
  /// saving and loading). Dont change the order of the bits here
  /// without also changing the type mask enumeration below.
  struct Flags
  {  
    /// Is this node at a stop line?
    uint8_t isStop : 1;
  
    /// Is this node an exit point?
    uint8_t isExit : 1;

    /// Is this node an entry point?
    uint8_t isEntry : 1;

    /// Is this node in a zone perimeter?
    uint8_t isZonePerimeter : 1;

    /// Is this node in a zone parking spot?
    uint8_t isZoneParking : 1;

    /// Is this node in a zone at all (includes perimeters, parking
    /// spots and interpolated nodes).
    uint8_t isZone : 1;

    /// Is this node in a lane?
    uint8_t isLane : 1;
    
    /// Is this node in an intersection?
    uint8_t isTurn : 1;

    /// Is this node part of a rail-change?
    uint8_t isRailChange : 1;

    /// Is this node part of a lane-change?
    uint8_t isLaneChange : 1;

    /// Is this node on the wrong side of the road?
    uint8_t isOncoming : 1;

    /// Is this node on an infeasible trajectory?  Non-feasible
    /// trajectories are included for long-range planning purposes,
    /// but should not be used in the near field.
    uint8_t isInfeasible : 1;

    /// Is this a volatile node?  Volatile nodes may be deleted by ROI and
    /// other operations, so pointers to these nodes should not be stored.
    uint8_t isVolatile : 1;

    /// Is this part of the vehicle sub-graph (i.e., dynamically generated
    /// maneuvers rooted on the current vehicle pose.
    uint8_t isVehicle : 1;

    /// Is this node off the road (can be true for vehicle nodes only).
    uint8_t isOffRoad : 1;
    
    // Reserved space for future flags
    uint8_t isReserved : 1;
    
  } flags;
  
  /// Current angle of the steering wheel (volatile nodes only)
  float steerAngle;
  
  // Corresponding waypoints in the RNDF.  The next and previous
  // waypoint will be the same if the node corresponds exactly to an
  // RNDF waypoint.
  RNDFGraphWaypoint *nextWaypoint, *prevWaypoint;
  
  // Next nodes(s) in the graph
  int16_t numNext, maxNext;
  PlanGraphNode **next;

  // Previous nodes(s) in the graph
  int16_t numPrev, maxPrev;
  PlanGraphNode **prev;

  // ROI index is non-zero if the node is in the region of interest
  // and expanded.  This needs to be a counter to enable incremental
  // expansion and contraction.
  uint32_t roiIndex;
  
  // Feasible destination nodes (i.e., distant nodes to which there
  // are feasible maneuvers).
  int16_t numDest, maxDest;
  PlanGraphNode **dest;

  // If this is an expanded node (generated in the ROI only), this
  // field points to the current destination node.  An optimization
  // for removing stuff from the ROI.
  PlanGraphNode *roiDest;
  
  /// Status information (collisions, etc).
  struct
  {
    /// Time when we last computed the site-to-node transform (used for
    /// obstacle and car collision detection).
    uint64_t transTime;

    /// The site-to-node transform (used for
    /// obstacle and car collision detection).
    float trans[2][3];

    /// Time (usec) when we last updated the obstacle collision field.
    uint64_t obsTime;

    /// Does this configuration collide with an obstacle?
    /// This flag indicates that there is at least one obstacle
    /// inside the current bounding box applied to this configuration.
    uint8_t obsCollision;

    /// Time (usec) when we last updated the distance fields (this is
    /// independent from obsTime).  The distance fields can be filled
    /// even if no collision occurs.
    uint64_t obsDistTime;
    
    /// Distance to the nearest obstacle to the front and rear of the
    /// vehicle.  Used for driving us out of collisions.
    float obsFrontDist, obsRearDist;

    /// Distance to the nearest obstacle on either side of the vehicle.
    /// This is used for pushing Alice away from glancing collisions
    /// and controlling vehicle speed in narrow areas.
    float obsSideDist;

    /// Time (usec) when we last updated the car collision field.
    uint64_t carTime;

    /// Does this configuration collide with a car?
    uint8_t carCollision;

    /// Time (usec) when we last updated the center-lane distance fields.
    uint64_t centerTime;

    /// Distance to the center of the lane, based on sensed lines.
    float centerLineDist;

    /// Distance to the center of the lane, based on sensed roughness.
    float centerRoughDist;

    /// Sensed distance to the next stop line.
    float stopDist;

  } status;
  
  /// Pointer for user data
  void *data1, *data2;
};


/// Type mask for querying nodes.  The bit ordering mst match the
/// flags structure, which is a bit naughty, since bit ordering on
/// bit-fields can vary between targets.
enum
{
  PLAN_GRAPH_NODE_STOP            = 0x0001,
  PLAN_GRAPH_NODE_EXIT            = 0x0002,
  PLAN_GRAPH_NODE_ENTRY           = 0x0004,
  PLAN_GRAPH_NODE_ZONE_PERIMETER  = 0x0008,
  PLAN_GRAPH_NODE_ZONE_PARKING    = 0x0010,
  PLAN_GRAPH_NODE_ZONE            = 0x0020,
  PLAN_GRAPH_NODE_LANE            = 0x0040,
  PLAN_GRAPH_NODE_TURN            = 0x0080,
  PLAN_GRAPH_NODE_RAIL_CHANGE     = 0x0100,
  PLAN_GRAPH_NODE_LANE_CHANGE     = 0x0200,
  PLAN_GRAPH_NODE_ONCOMING        = 0x0400,
  PLAN_GRAPH_NODE_INFEASIBLE      = 0x0800,
  PLAN_GRAPH_NODE_VOLATILE        = 0x1000,
  PLAN_GRAPH_NODE_VEHICLE         = 0x2000,
  PLAN_GRAPH_NODE_OFFROAD         = 0x4000,    
  PLAN_GRAPH_NODE_NONE            = 0x0000,
  PLAN_GRAPH_NODE_ALL             = 0xFFFF,
};



/// @brief A dynamically allocated list of nodes, use for querying
/// regions of the quad-tree.
typedef std::vector<PlanGraphNode*> PlanGraphNodeList;


/// @brief Quad-tree node.
///
/// This must be simple structure (with no functions) to allow fast
/// memory allocation and initialization (calloc).
struct PlanGraphQuad
{
  // Quad center
  float px, py;

  // Quad size
  float size;

  // Leaves from this quad
  PlanGraphQuad *leaves[4];

  // Nodes in this quad
  int numNodes, maxNodes;
  PlanGraphNode **nodes;

  /// Check if quad contains the given point.
  bool hasPoint(float px, float py)
    {
      if (px >= this->px + this->size/2)
        return false;
      if (px < this->px - this->size/2)
        return false;
      if (py >= this->py + this->size/2)
        return false;
      if (py < this->py - this->size/2)
        return false;
      return true;
    };


  /// Check for overlap between a quad and the requested region.
  /// @param[in] px,py Center of the rectangle.
  /// @param[in] sx,sy Width and height of the rectangle.
  bool hasIntersection(float px, float py, float sx, float sy)
    {
      if (px - sx/2 >= this->px + this->size/2)
        return false;
      if (px + sx/2 < this->px - this->size/2)
        return false;
      if (py - sy/2 >= this->py + this->size/2)
        return false;
      if (py + sy/2 < this->py - this->size/2)
        return false;
      return true;
    };
};


/// @brief PlanGraph maintains a dense graph of interpolated vehicle
/// configurations (poses) based on the RNDF data.  This graph is used
/// for planning.
///
/// The graph is defined in the site frame (identical to UTM apart
/// from a translation).
///
class PlanGraph
{
  public:

  /// @brief Constructor
  PlanGraph();

  /// @brief Destructor
  virtual ~PlanGraph();
  
  private:

  // Hide the copy constructor
  PlanGraph(const PlanGraph &that);
  
  public:

  /// @brief Get the canonical node corresponding to the given checkpoint ID
  PlanGraphNode *getCheckpoint(int checkpointId);

  /// @brief Get the canonical node corresponding to the given RNDF ID
  PlanGraphNode *getWaypoint(int segmentId, int laneId, int waypointId);

  /// @brief Get the node nearest the given position
  ///
  /// @param[in] px,py Test position.
  /// @param[in] maxDist Only return nodes within this distance to the test position.
  /// @param[in] includeFlags Mask indicating which nodes should be included.
  /// @param[in] excludeFlags Mask indicating which nodes should be excluded. 
  PlanGraphNode *getNearestPos(float px, float py, float maxDist,
                               uint16_t includeFlags = PLAN_GRAPH_NODE_ALL,
                               uint16_t excludeFlags = PLAN_GRAPH_NODE_NONE);

  /// @brief Get all nodes in a rectangular region.
  ///
  /// This function gets all nodes in overlapping quads, so the region
  /// will be slightly expanded.
  ///
  /// @param[out] nodes Node list to be filled (this is an STL vector).
  /// @param[in] px,py Center of the square.
  /// @param[in] sx,sy Size (width and height) of the region.
  /// @param[in] includeFlags Mask indicating which nodes should be included.
  /// @param[in] excludeFlags Mask indicating which nodes should be excluded. 
  int getRegion(PlanGraphNodeList *nodes, float px, float py, float sx, float sy,
                uint16_t includeFlags = PLAN_GRAPH_NODE_ALL,
                uint16_t excludeFlags = PLAN_GRAPH_NODE_NONE);

  private:
  
  // Get all nodes in a square region (recursive form)
  int getRegionQuad(PlanGraphNodeList *nodes, PlanGraphQuad *quad,                    
                    float px, float py, float sx, float sy,
                    uint16_t includeFlags, uint16_t excludeFlags);

  public:
  
  /// @brief Update the region of interest.
  ///
  /// This expands all nodes in the new ROI, and contracts any nodes
  /// in the old ROI.
  ///
  /// @param[in] px,py Center of the ROI (site frame).
  /// @param[in] size Size of the ROI.
  int updateROI(float px, float py, float size);

  /// @brief Test a point to see if it lies in the ROI
  bool hasPointROI(float px, float py);
  
  private:
  
  // Expand nodes in the ROI
  int expandROI(PlanGraphQuad *quad, float px, float py, float size, uint32_t index);

  // Contract nodes in the ROI
  int contractROI(PlanGraphQuad *quad, float px, float py, float size, uint32_t index);

  // Insert a maneuver between the given nodes.
  int insertManeuverROI(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  // Remove a maneuver between the given nodes.
  int removeManeuverROI(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  private:

  // ROI node spacing
  float roiSpacing;
  
  // Current ROI index
  uint32_t roiIndex;

  // Current ROI region
  float roiPx, roiPy, roiSize;

  // Vehicle kinematic properties for feasibility checks.
  float wheelBase, maxSteer;

  public:

  /// @brief Update the status timestamp (usec); this will be used for
  /// future calls to freshness functions.
  void setStatusTime(uint64_t statusTime) {this->statusTime = statusTime;}

  /// @brief Get the current status timestamp (usec).
  uint64_t getStatusTime(void) {return this->statusTime;}
    
  /// @brief Is the status information in the given node current?
  bool isStatusFresh(uint64_t statusTime)
    {
      return ((this->statusTime - statusTime) < this->statusMaxAge);
    }

  /// @brief Status helper: is this node in collision with an obstacle?
  bool isObsCollision(PlanGraphNode *node)
    {
      if (!isStatusFresh(node->status.obsTime))
        return false;
      return node->status.obsCollision;
    }

  /// @brief Status helper: is this node in collision with a car?
  bool isCarCollision(PlanGraphNode *node)
    {
      if (!isStatusFresh(node->status.carTime))
        return false;
      return node->status.carCollision;
    }

  private:

  // Time (usec) when we last freshened the the status information.
  uint64_t statusTime;

  public:

  // TODO add setter
  /// Maximuma age of valid data
  uint64_t statusMaxAge;
  
  public:
  
  /// Allocate a node
  PlanGraphNode *allocNode(float px, float py);

  /// Free a node
  void freeNode(PlanGraphNode *node);

  /// Fix a volatile node in memory (so it wont get freed).
  void fixNode(PlanGraphNode *node);

  /// Unfix a volatile node in memory (so that it may be freed).
  void unfixNode(PlanGraphNode *node);
  
  /// Insert an arc
  int insertArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  /// Remove an arc
  int removeArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);
  
  /// Insert a feasible arc
  int insertFeasibleArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  /// Remove a feasible arc
  int removeFeasibleArc(PlanGraphNode *nodeA, PlanGraphNode *nodeB);

  // Allocate a new quad or return an existing one for the given point.
  PlanGraphQuad *allocQuad(PlanGraphQuad *quad, float px, float py);

  /// Free a quad and all the nodes in it
  void freeQuad(PlanGraphQuad *quad);

  /// Get the quad nearest the given position
  PlanGraphQuad *getNearestQuad(PlanGraphQuad *quad, float px, float py);

  public:
  
  // Save to a binary file
  int save(const char *filename);

  // Load from a binary file
  int load(const char *filename);
  
  private:
  
  // Write a quad (recursive form)
  int writeQuadNodes(FILE *file, PlanGraphQuad *quad);

  // Write quad arcs (recursive form)
  int writeQuadArcs(FILE *file, PlanGraphQuad *quad);

  // Read quad nodes (recursive form)
  int readQuadNodes(FILE *file, PlanGraphQuad *quad);

  // Read quad arcs (recursive form)
  int readQuadArcs(FILE *file, PlanGraphQuad *quad);

  // Get a node by id and position
  PlanGraphNode *getNodeId(uint32_t nodeId, float px, float py);

  public:

  // Underlying RNDF
  RNDFGraph rndf;

  // Pointer to the node that corresponds to the current vehicle
  // configuration.  This may be null.
  PlanGraphNode *vehicleNode;
  
  // Root of the quad-tree
  PlanGraphQuad *root;
  
  // Minimum linear dimension for quad tree elements.
  float quadScale;

  // Stats on memory usage
  int quadCount, quadBytes, nodeCount, nodeBytes;

  public:

  /// Predraw the plan graph centered around some point
  /// @param[in] px,py Center point of region of interest (site frame).
  /// @param[in] size Size of region of interest (m).
  /// @param[in] includeFlags Mask indicating which nodes should be included.
  /// @param[in] excludeFlags Mask indicating which nodes should be excluded.
  /// @return On success, returns the display list.
  int predrawStruct(float px, float py, float size, uint16_t includeFlags, uint16_t excludeFlags,
                    bool showNodes = false, bool showArcs = true);
  
  /// Predraw the node status information.
  /// @param[in] px,py Center point of region of interest (site frame).
  /// @param[in] size Size of region of interest (m).
  /// @param[in] includeFlags Mask indicating which nodes should be included.
  /// @param[in] excludeFlags Mask indicating which nodes should be excluded. 
  /// @return On success, returns the display list.
  int predrawStatus(float px, float py, float size, uint16_t includeFlags, uint16_t excludeFlags);

  private:
    
  // GL display list
  int structList, statusList;

  // Settings for the last structure list we predrew.
  uint16_t structInclude, structExclude;
  float structListPx, structListPy, structListSize;
};

#endif

