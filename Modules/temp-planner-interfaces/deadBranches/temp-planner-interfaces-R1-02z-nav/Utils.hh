#ifndef UTILS_HH_
#define UTILS_HH_

#include <interfaces/ActuatorState.h>
#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>

#include <map/MapElementTalker.hh>

class Utils {

public: 
  static int init();
  static void destroy();

  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static LaneLabel getCurrentLane(VehicleState &vehState, PlanGraph *graph);
  static LaneLabel getCurrentLaneAnyDir(VehicleState &vehState, PlanGraph *graph);
  static LaneLabel getDesiredLane(VehicleState &vehState, PlanGraph *graph);
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
//   static double getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal);
//  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(point2 &pos, Map *map, LaneLabel &lane);
  static double getDistToStopline(PlanGraphPath* m_path, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal);
  static double getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getAngleInRange(double angle);
  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement **me, VehicleState &vehState, Map *map, LaneLabel &lane); 
  static double monitorAliceSpeed(VehicleState &vehState, int m_estop);
  static void updateStoppedMapElements(Map *map);
  static uint64_t getTime();
  static int reverseProjection(point2 &out, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static int distToProjectedPoint(point2 &out, double &distance, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static void displayGraph(int sendSubgroup, PlanGraph* graph, VehicleState vehState, int vis_level = 2);
  static void printErrorInBinary(Err_t error, int log_level = 1);
  static void printCSpecs(CSpecs_t* cSpecs, int log_level = 5);
  static void printPath(PlanGraphPath *path, int log_level = 8);
  static void printTraj(CTraj* traj, int log_level = 8);
  static void printCurrPose(pose2f_t pose, int log_level = 3);
  public:

  // Set the site frame to local frame transform for display functions
  static void setSiteToLocal(const VehicleState *vehState);
  
  static void displayPath(int sendSubgroup, PlanGraphPath* path, int vis_level = 2, MapElementColorType colorType = MAP_COLOR_DARK_GREEN, MapId mapId = 12500);

  static void displayTraj(int sendSubgroup, CTraj* traj, int vis_level = 2);
  static void displayLine(int sendSubgroup, point2arr line, int vis_level = 2, MapElementColorType colorType = MAP_COLOR_PINK, MapId mapId = 13000);
  static void displayPoints(int sendSubgroup, point2arr points, int vis_level = 2, MapElementColorType colorType = MAP_COLOR_PURPLE, MapId mapId = 13500);
  static void displayCircle(int sendSubgroup, point2 pt, double radius, int vis_level, MapElementColorType colorType, MapId mapId);
  static void displayGraphHeadings(int sendSubgroup, PlanGraph* graph, VehicleState vehState, int vis_level = 2, double bound = 25);
  static void displayElement(int sendSubgroup, MapElement me, int vis_level = 2);
  static void displayCollision(int sendSubgroup, point2 p, double size);
  static void displayClearCollision(int sendSubgroup);
  static void displayPrediction(int sendSubgroup, point2arr poly, MapElementColorType colorType, MapId mapId);
  static void displayClear(int sendSubgroup, int vis_level = 2, MapId mapId = 13000);

  static int updatePathStoplines(PlanGraphPath *path, Map *map, PlanGraph *graph);
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);
private :
  
  // Transform from site to local frame
  static pose2 m_transLS;

  // Talker for the map
  static CMapElementTalker m_meTalker;

};

#endif /*UTILS_HH_*/


