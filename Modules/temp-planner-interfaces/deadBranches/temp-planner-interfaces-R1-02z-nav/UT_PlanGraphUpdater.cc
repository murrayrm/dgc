
/* 
 * Desc: Test plan graph updater
 * Date: 11 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include <stdlib.h>
#include <dgcutils/DGCutils.hh>
#include <alice/AliceConstants.h>

#include "PlanGraphUpdater.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



// Test the vehicle sub-graph generation
int testVehicle(PlanGraphUpdater *updater, PlanGraph *graph)
{
  int i, j;
  int nodeCount;
  uint64_t time;
  pose2f_t pose;
  float steerAngle;
  RNDFGraphWaypoint *wp;

  nodeCount = 0;
  time = DGCgettime();
  for (j = 0; j < 100; j++)
  {
    // Pick a random point from the RNDF
    i = rand() % graph->rndf.numWaypoints;
    wp = graph->rndf.waypoints + i;
    
    // Pick a random pose near the waypoint
    pose.pos.x = wp->px;
    pose.pos.y = wp->py;
    pose.pos.x += (((float) rand())/RAND_MAX * 2 - 1) * 10;
    pose.pos.y += (((float) rand())/RAND_MAX * 2 - 1) * 10;
    pose.rot = (((float) rand())/RAND_MAX * 2 - 1) * M_PI;
    steerAngle = (((float) rand())/RAND_MAX * 2 - 1) * VEHICLE_MAX_AVG_STEER;
    
    // Update sub-graph
    updater->update(pose, steerAngle);

    MSG("wp %d.%d.%d config %.3f %.3f %.3f %.3f generated %d nodes",
        wp->segmentId, wp->laneId, wp->waypointId,
        pose.pos.x, pose.pos.y, pose.rot, steerAngle,
        updater->numNodes);

    if (updater->numNodes > 1)
      nodeCount += updater->numNodes;
  }
  time = DGCgettime() - time;
  MSG("%.3f ms/call %.0f nodes/call", (double) time * 1e-3 / j, (float) nodeCount / j);
  
  return 0;
}


int main(int argc, char **argv)
{
  char *filename;
  PlanGraph graph;
  PlanGraphUpdater updater(&graph);
      
  filename = NULL;  
  if (argc > 1)
    filename = argv[1];

  if (!filename)
    return fprintf(stderr, "usage: %s <FILE.RNDF.PG>\n", argv[0]);

  // Load the compiled graph file
  MSG("loading %s", filename);
  if (graph.load(filename) != 0)
    return -1;

  // Test vehicle sub-graph generation
  testVehicle(&updater, &graph);
  
  return 0;
}
