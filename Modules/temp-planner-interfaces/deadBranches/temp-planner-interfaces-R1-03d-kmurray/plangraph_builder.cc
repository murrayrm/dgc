
/* 
 * Desc: Utility for pre-building plan graphs.
 * Date: 7 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <stdio.h>
#include <stdlib.h>
#include <dgcutils/DGCutils.hh>

#include "plangraph_builder_cmdline.h"
#include "PlanGraphBuilder.hh"



// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)



int main(int argc, char **argv)
{
  struct cmdline options;
  char pgFilename[1024];
  PlanGraph graph;
  PlanGraphBuilder builder(&graph);

  // Parse the command line
  if (cmdline_parser(argc, argv, &options) != 0)
  {
    cmdline_parser_print_help();
    return -1;
  }
  if (!options.rndf_given)
  {
    fprintf(stderr, "please specify an RNDF file to load\n");
    return -1;
  }
  
  // Set the options
  builder.setOptions(options.spacing_arg, options.rail_count_arg, options.rail_spacing_arg);
  
  // Load the file
  MSG("building %s %s", options.rndf_arg, options.tweaks_arg);
  if (builder.build(options.rndf_arg, options.tweaks_arg) != 0)
    return -1;

  // Save the build graph
  snprintf(pgFilename, sizeof(pgFilename), "%s.pg", options.rndf_arg);
  MSG("saving graph to %s", pgFilename);
  if (graph.save(pgFilename) != 0)
    return ERROR("unable to save graph");

  // Done
  cmdline_parser_free(&options);
  
  return 0;
}
