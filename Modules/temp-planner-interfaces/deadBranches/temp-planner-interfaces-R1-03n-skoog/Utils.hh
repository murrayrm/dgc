#ifndef UTILS_HH_
#define UTILS_HH_

#include <interfaces/ActuatorState.h>
#include <interfaces/VehicleState.h>
#include <trajutils/traj.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>

#include <map/MapElementTalker.hh>

class Utils {

public: 
  static int init();
  static void destroy();

  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);

  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static LaneLabel getCurrentLane(VehicleState &vehState, PlanGraph *graph);
  static LaneLabel getCurrentLaneAnyDir(VehicleState &vehState, PlanGraph *graph);
  static LaneLabel getDesiredLane(VehicleState &vehState, PlanGraph *graph);

  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
  // set whether or not we are currently ignoring an obstacle
  static void setIgnoreMode(bool ignore);

  static double getDistToStopline(point2 &pos, Map *map, LaneLabel &lane);
  static double getDistToStopline(PlanGraphPath* m_path, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal);
  static double getDistToStopline(PlanGraphPath* m_path);
  static float  getDistToIntersection(deque<SegGoals>* seg_goal_queue, Map *map, VehicleState &vehState);
  
  static double getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static point2 getExitPoint(VehicleState &vehState, Map *map, SegGoals &seg_goal);

  static double getAngleInRange(double angle);

  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  // Ambiguous 'getNearestObsInLane'
  static double getNearestObsInLane(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement **me, VehicleState &vehState, Map *map, LaneLabel &lane); 
  // calculate the distance along the path to nearest obstacle along the path. returns value from front/rear bumper
  static double getNearestObsOnPath(PlanGraphPath* path, PlanGraph* graph, VehicleState vehState);
  // get the node index for the first obstacle on the path
  static int getNearestObsNodeOnPath(PlanGraphPath* path, PlanGraph* graph);

  static double monitorAliceSpeed(VehicleState &vehState, int m_estop);

  static void updateStoppedMapElements(Map *map);
  static uint64_t getTime();

  static int reverseProjection(point2 &out, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);
  static int distToProjectedPoint(point2 &out, double &distance, point2 pt, double yaw, point2arr on, double max_dist, double extension = 0);

  static void printErrorInBinary(Err_t error, int log_level = 1);
  static void printCSpecs(CSpecs_t* cSpecs, int log_level = 5);
  static void printPath(PlanGraphPath *path, int log_level = 9);
  static void printTraj(CTraj* traj, int log_level = 8);
  static void printCurrPose(pose2f_t pose, int log_level = 3);
  public:

  // Set the site frame to local frame transform for display functions
  static void setSiteToLocal(const VehicleState *vehState);

  static void displayGraph(int sendSubgroup, PlanGraph* graph, VehicleState vehState, int vis_level = 2);
  static void displayPath(int sendSubgroup, PlanGraphPath* path, int vis_level = 2, MapElementColorType colorType = MAP_COLOR_DARK_GREEN, MapId mapId = 12500);
  static void displayTraj(int sendSubgroup, CTraj* traj, int vis_level = 2);
  static void displayLine(int sendSubgroup, point2arr line, int vis_level = 2, MapElementColorType colorType = MAP_COLOR_PINK, MapId mapId = 13000);
  static void displayPoints(int sendSubgroup, point2arr points, int vis_level = 2, MapElementColorType colorType = MAP_COLOR_PURPLE, MapId mapId = 13500);
  static void displayCircle(int sendSubgroup, point2 pt, double radius, int vis_level, MapElementColorType colorType, MapId mapId);
  static void displayGraphHeadings(int sendSubgroup, PlanGraph* graph, VehicleState vehState, int vis_level = 2, double bound = 25);
  static void displayElement(int sendSubgroup, MapElement me, int vis_level = 2);
  static void displayCollision(int sendSubgroup, point2 p, double size);
  static void displayClearCollision(int sendSubgroup);
  static void displayPrediction(int sendSubgroup, point2arr poly, MapElementColorType colorType, MapId mapId);
  static void displayClear(int sendSubgroup, int vis_level = 2, MapId mapId = 13000);
  static void displayPlannerStatus(string region, string state, string planner, string flag);
  static void displayPlannerStatus(string region, string state, string planner, string flag, string obstacle);
  static void displayPlannerState(string state);
  static void displayIntersectionState(string intersection);

  static int updatePathStoplines(PlanGraphPath *path, Map *map, PlanGraph *graph, VehicleState vehState);
  static int updatePathLaneId(PlanGraphPath *path, PlanGraph *graph);
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);


private :
  
  // Transform from site to local frame
  static pose2 m_transLS;

  // Talker for the map
  static CMapElementTalker m_meTalker;

  // Do we ignore the obstacle we are on?
  static bool m_ignoreObstacle;
  
};

#endif /*UTILS_HH_*/


