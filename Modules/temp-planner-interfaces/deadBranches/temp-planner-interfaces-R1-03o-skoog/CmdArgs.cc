/*!
 * \file CmdArgs.cc
 * \brief Source for the command line arguments object that gets passed around in the planner stack
 *
 * \author Sven Gowal
 * \date 1 July 2007
 *
 * \ingroup planner
 *
 */

#include "CmdArgs.hh"

int CmdArgs::sn_key = 0;
bool CmdArgs::debug = false;
bool CmdArgs::use_RNDF = false;
string CmdArgs::RNDF_file = "";
int CmdArgs::verbose_level = 0;
bool CmdArgs::logging = false;
bool CmdArgs::console = false;
bool CmdArgs::lane_cost = false;
string CmdArgs::log_path = "";
string CmdArgs::log_filename = "tplanner.log";
int CmdArgs::log_level = 0;
int CmdArgs::visualization_level = 2;
bool CmdArgs::mod_log_planner = false;
bool CmdArgs::lt_planner = false;
bool CmdArgs::closed_loop = false;
bool CmdArgs::noprediction = false;
bool CmdArgs::stepbystep_load = false;
bool CmdArgs::load_graph_files = false;
bool CmdArgs::update_from_map = false;
bool CmdArgs::disable_fused_perceptor = false;
bool CmdArgs::update_lanes = false;
bool CmdArgs::update_stoplines = false;
bool CmdArgs::mapper_use_internal = false;
int CmdArgs::mapper_debug_subgroup = -2;
bool CmdArgs::mapper_disable_obs_fusion = false;
bool CmdArgs::mapper_disable_line_fusion = false;
bool CmdArgs::mapper_line_fusion_compat = false;
int CmdArgs::mapper_decay_thresh = 0;
bool CmdArgs::mapper_enable_groundstrike_filtering = false;
bool CmdArgs::mapper_enable_sense_segment = false;
bool CmdArgs::use_dplanner = false;
bool CmdArgs::use_s1planner = false;
bool CmdArgs::use_circle_planner = false;
bool CmdArgs::show_costmap = false;
bool CmdArgs::use_rndf_frame = false;
char* CmdArgs::costmap_config = NULL;
char* CmdArgs::path_plan_config = NULL;
char* CmdArgs::planner_config = NULL;
bool CmdArgs::no_failure_handling = false;
bool CmdArgs::use_hacked_uturn = false;
bool CmdArgs::use_hacked_backup = false;
bool CmdArgs::use_prob_planner = false;
double CmdArgs::prob_conf_bound = 1.645; //use for prob_planner position uncertainty; 68%: 1, 80%: 1.282, 90%: 1.645
int CmdArgs::sparse = 0;
bool CmdArgs::use_rrt_planner = false;
