#------------------------------------------------------------------------------
#	DO NOT CHANGE OR MOVE THE LINES BELOW
#
# Include a file that provides much common functionality
# be sure to include this *after* setting the MODULE_* variables
# These lines should be the first thing in the makefile
#------------------------------------------------------------------------------
ifndef YAM_ROOT
  include ../../etc/SiteDefs/mkHome/shared/overall.mk
else
  include $(YAM_ROOT)/etc/SiteDefs/mkHome/shared/overall.mk
endif

#------------------------------------------------------------------------------
#	START OF MODULE SPECIFIC CUSTOMIZATION (below)
#------------------------------------------------------------------------------
#
# Uncomment and define the variables as appropriate
#
#------------------------------------------------------------------------------
# specify any module specific variable definitions here
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
# specify what to build. Librares should be listed in the PROJ_LIBS
# variable, while binaries go into the PROJ_BINS variable.
#
# If building a library, the name should NOT contain the suffix such 
# as ".a" or ".so".
#
# If PROJ_LIBS and PROJ_BINS are left blank, no source files will be 
# compiled, but the module may still contain scripts or public header 
# files. 
#------------------------------------------------------------------------------
PROJ_LIBS 			:= libtemp-planner-interfaces libplangraph 
PROJ_BINS 			:= UT_PlanGraph UT_PlanGraphUpdater plangraph-builder plangraph-editor
# PROJ_INTERNAL_BINS 		:= 


#------------------------------------------------------------------------------
# specify public .h, .a, .so, etc. files by filling in *_LINKS variables
# symbolic links to these files get created in top-level directories
#------------------------------------------------------------------------------
# BIN_LINKS			:= 
# LIB_MODULE_LINKS 		:=
INC_MODULE_LINKS := PlannerInterfaces.h Console.hh \
										AliceStateHelper.hh Log.hh LogClass.hh CmdArgs.hh Quadtree.hh Utils.hh\
						 				plannerDisplay.h intersectionDisplay.h debugDisplay.h ConfigFile.hh \
                    planner_cmdline.h \
                    PlanGraph.hh PlanGraphPath.hh	PlanGraphBuilder.hh PlanGraphUpdater.hh \
										PlanGraphStatus.hh Graph.hh
# REMOVE Graph.hh GraphPath.hh 

# ETC_MODULE_LINKS 	 	:=

#------------------------------------------------------------------------------
# specify source code to compile (must end in .c, or .cc)
# for source files to be compiled for each PROJ_LIBS and PROJ_BINS
# value listed above
#------------------------------------------------------------------------------

CC_SRC-libtemp-planner-interfaces	:= planner_cmdline.c
CPLUSPLUS_SRC-libtemp-planner-interfaces := \
						CmdArgs.cc \
						Console.cc \
						Log.cc \
						AliceStateHelper.cc \
						Utils.cc \
						ConfigFile.cc \
						LogClass.cc
# REMOVE Graph.cc Quadtree.cc 

#------------------------------------------------------------------------------
# compilation flags unique to the individual targets
#------------------------------------------------------------------------------
# CFLAGS-<tgt> 			:=
# F77FLAGS-<tgt> 		:=

#------------------------------------------------------------------------------
# when building binary executables and shared libraries, list any extra
# libraries that must be linked in and any -L options needed to find them.
# set the LINKER-<tgt> flag to "$(CPLUPLUS)" to use the C++ linker
#------------------------------------------------------------------------------
LIBS-libtemp-planner-interfaces	:= -lrndf -lmdf -lmap -ltrajutils -llapack \
	 				   -linterfaces -lframes -lsparrowhawk -lsparrow -lcspecs
#						-lbitmap `pkg-config --libs opencv` -lGL -lGLU
LINKER-libtemp-planner-interfaces			:= $(CPLUSPLUS)

# PlanGraph library in two flavours : with and without OpenGL support
FLAVORS-libplangraph 		     	:= plain gl
FLAVOR_EXT-libplangraph-plain	:= -NONE-
FLAVOR_EXT-libplangraph-gl 		:= -gl
CPLUSPLUS_SRC-libplangraph	 	:= PlanGraph.cc PlanGraphBuilder.cc PlanGraphUpdater.cc PlanGraphStatus.cc
ifeq ($(FLAVOR),gl)
  CFLAGS-libplangraph  	    	:= -DUSE_GL
endif

# Unit test for the plan graph
CFLAGS-UT_PlanGraph         := 
CPLUSPLUS_SRC-UT_PlanGraph  := UT_PlanGraph.cc 
LIBS-UT_PlanGraph           := -lplangraph 
LIBS-UT_PlanGraph           += -lrndf -ldgcutils -ltrajutils -llapack -lpthread
LINKER-UT_PlanGraph         := $(CPLUSPLUS)

# Unit test for the plan graph updater
CFLAGS-UT_PlanGraphUpdater         := 
CPLUSPLUS_SRC-UT_PlanGraphUpdater  := UT_PlanGraphUpdater.cc 
LIBS-UT_PlanGraphUpdater           := -lplangraph 
LIBS-UT_PlanGraphUpdater           += -lrndf -ldgcutils -ltrajutils -llapack -lpthread
LINKER-UT_PlanGraphUpdater         := $(CPLUSPLUS)

# Utility for plan graph generation
CPLUSPLUS_SRC-plangraph-builder 	:= plangraph_builder.cc
CC_SRC-plangraph-builder 				:= plangraph_builder_cmdline.c
LIBS-plangraph-builder          	:= -lplangraph 
LIBS-plangraph-builder	 	       	+= -lrndf -ldgcutils -ltrajutils -llapack -lpthread
LINKER-plangraph-builder        	:= $(CPLUSPLUS)

# Command line options for builder
plangraph_builder.cc: plangraph_builder_cmdline.h
plangraph_builder_cmdline.h plangraph_builder_cmdline.c: plangraph_builder_cmdline.ggo
	gengetopt -i plangraph_builder_cmdline.ggo -F plangraph_builder_cmdline -a cmdline -u 

# GUI for editing/building plan graphs
CFLAGS-plangraph-editor					:= `fltk-config --cflags`
CPLUSPLUS_SRC-plangraph-editor 	:= PlanGraphEditor.cc
CC_SRC-plangraph-editor 				:= plangraph_editor_cmdline.c
LIBS-plangraph-editor          	:= -lplangraph-gl -ldraw -lrndf-gl 
LIBS-plangraph-editor	 	       	+= -ldgcutils -ltrajutils -llapack -lpthread
LIBS-plangraph-editor						+= -ldgc-fltk `fltk-config --ldflags --use-gl --use-glut` -lglut
LINKER-plangraph-editor        	:= $(CPLUSPLUS)

#Flag for probabilistic planning

# Command line options 
PlanGraphEditor.cc : plangraph_editor_cmdline.h
plangraph_editor_cmdline.h plangraph_editor_cmdline.c: plangraph_editor_cmdline.ggo
	gengetopt -i plangraph_editor_cmdline.ggo -F plangraph_editor_cmdline -a plangraph_editor_cmdline -f plangraph_editor_cmdline -u -C

# Do post-processing (needed for OS X)
bins-module :: $(YAM_TARGET)/plangraph-editor
	fltk-config --post $<



#------------------------------------------------------------------------------
# augment flags used when compiling C and C++ source code
#------------------------------------------------------------------------------
MODULE_COMPILE_FLAGS 		:= -Wall -g -O3 `pkg-config --cflags opencv`

#------------------------------------------------------------------------------
# additional compiler flags to use for dependency information generations
# if left undefined, then all the CFLAGS-<tgt> values are used to set this
# variable
#------------------------------------------------------------------------------
# MODULE_DEPENDS_FLAGS 		:=

#------------------------------------------------------------------------------
# specify information for building Doxygen documentation
# set DOXYGEN_DOCS to "true" to turn on documentation generation
# set DOXYGEN_TAGFILES to other module names for link generation
#------------------------------------------------------------------------------
# DOXYGEN_DOCS 			:= true
# DOXYGEN_TAGFILES 		:= 

#------------------------------------------------------------------------------
# Add any additional rules specific to the module
#------------------------------------------------------------------------------
# regtest-module::
#	@$(YAM_ROOT)/bin/Drun -fep - oeltest -d test

# Rules for making sparrow dynamic displays
CDD = $(YAM_ROOT)/bin/$(YAM_TARGET)/cdd
%.h: %.dd;	$(CDD) -o $*.h $*.dd

# Sparrow dependenciesfor this file
$(YAM_TARGET)/Console.o: plannerDisplay.h
plannerDisplay.h: plannerDisplay.dd

$(YAM_TARGET)/Console.o: intersectionDisplay.h
intersectionDisplay.h: intersectionDisplay.dd

$(YAM_TARGET)/Console.o: debugDisplay.h
debugDisplay.h: debugDisplay.dd

# Shared command line options
planner_cmdline.c planner_cmdline.h: planner_cmdline.ggo
	gengetopt -i planner_cmdline.ggo --conf-parser -f planner_cmdline -a planner_options -F planner_cmdline


#------------------------------------------------------------------------------
# Add module specific clean rule if necessary
#------------------------------------------------------------------------------
# clean-module::

#------------------------------------------------------------------------------
#	END OF MODULE SPECIFIC CUSTOMIZATION (below)
#------------------------------------------------------------------------------
#	DO NOT CHANGE OR MOVE THE LINE BELOW
#
# include the "stdrules.mk" file that provides much common functionality.
#------------------------------------------------------------------------------
include $(YAM_ROOT)/etc/SiteDefs/makefile-yam-tail.mk


###########################################################################
# Makefile.yam for "temp-planner-interfaces" module
#
# Makefile.yam is the top-level Makefile for the module, and is the one a
# developer should invoke directly to rebuild a single module.
# Invoke it while in the module's checked out src directory.
#
# Makefile.yam - specify source files, public header files and libraries,
#
# This Makefile is used by YAM scripts to build and link a module.
# It should have targets for:
#     yam-mklinks links depends libs bins clean
# even if some are no-ops.
#
# See http://dartslab.jpl.nasa.gov/cgi/dshell-fom.cgi?file=661
#	for more information and Makefile.yam examples
#
# When invoked from the YAM scripts, this Makefile is passed values for
# YAM_NATIVE, YAM_ROOT, YAM_SITE, and YAM_TARGET variables.
#
# Use etc/SiteDefs/Makefile.yam-common to take advantage of some common
# functionality.
#
#       PROJ            - what to build, either the name of a binary executable
#                         or a library (in which case $(PROJ) should start with
#                         "lib" and NOT contain a suffix such as .a or .so).
#                         if left blank, then no source files are compiled.
#			  Links for the libraries automatically get exported 
#			  into the top-level lib/YAM_TARGET, while those for 
#			  binaries get exported to bin/YAM_TARGET.
#                         but the module can contain scripts and header files
#       FLAVORS-<tgt>   - list of "flavors" of the module to build
#			  for the specified binary/library.
#                         If FLAVORS is set to "FOO BAR", then each source
#                         file gets compiled twice into the FOO & BAR
#                         sub-directories of YAM_TARGET. Libraries and binaries
#                         files have "-FOO" or "-BAR" appended to
#                         them. The suffix to use can be set by explicitly setting
#			  the FLAVOR_EXT-<tgt>-<flavor> to the value of the 
#			  desired suffix. No suffix is used if the "-NONE-"
#			  suffix is specified. 
#                         Makefile.yam, and append to CC_SRC (etc.) as
#                         appropriate for that flavor.
#       CC_SRC-<tgt>          - list of .c files to compile for the target
#       CPLUSPLUS_SRC-<tgt>   - list of .cc files to compile for the target
#       MODULE_COMPILE_FLAGS - augments standard C pre-processor flags (-I, -D)
#       MODULE_DEPENDS_FLAGS - flags to use for dependency information 
#       LIBS-<tgt>      - list of libraries to link in for shared libraries
#                         and binary executables for the target
#       LINKER-<tgt>    - set to "CPLUSPLUS" to use the C++ linker
#       CFLAGS-<tgt>    - compilation flags specific to the target
#	DOXYGEN_DOCS    - if "true" then Doxygen docs are generated
#       DOXYGEN_TAGFILES    - names of other modules to create links for in the
#			      Doxygen documentation
#
# The following variables are used by the yam-mklinks and yam-rmlinks rules 
# to export and deleted links for the module to the higher level directories.
#
# BIN_LINKS             - will set up links under ../../bin/
# BIN_MODULE_LINKS      - will set up links under ../../bin/<module>/
#
# Additional available variables can be obtained by replacing "BIN" with
# either of "INC", "ETC", "LIB", "BIN", or "DOC"
#
# Additional variables for target specific links are
#
# BIN_TARGET_LINKS      - will set up links under ../../bin/$(YAM_TARGET)/
# BIN_sparc-sunos5_LINKS - will set up linkvs under ../../bin/sparc-sunos5/
#
# Additional available variables can be obtained by replacing "BIN" with "LIB"
#
#
# You may also augment the default rules by specifiying them in this file.
# Since the "::" versions of the rules are used the effect is to append to
# rather than to replace the default rule.
#
# By default, a module is assumed to be support all the known targets.
# An optional .supported.mk file can be created in the top level module 
# directory to restrict the list of supported targets for the module.
# If you need to disable certain targets for this module then create
# a .supported.mk file in the module's directory and add lines so that 
# the the following variables are set appropriately:
#
#       MODULE_SUPPORTED_TARGETS
#       MODULE_UNSUPPORTED_TARGETS
#       MODULE_SUPPORTED_OS
#       MODULE_UNSUPPORTED_OS 
#
# Also an optional .directives.mk file can be created in the top level 
# module directory to pass on module specific directives to the site-config 
# files.
#
#
