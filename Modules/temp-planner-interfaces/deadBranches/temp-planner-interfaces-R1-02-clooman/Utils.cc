#include "Utils.hh"
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>
#include <math.h>
#include <map/MapElementTalker.hh>
#include <frames/vec3.h>

#define EXTRA_WIDTH 1.5

bool Utils::isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane)
{
  point2arr lb, rb;
  map->getLaneBounds(lb, rb, lane);

  double ldist = me.dist(lb, GEOMETRY_LINE);
  double rdist = me.dist(rb, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

bool Utils::isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound)
{
  double ldist = me.dist(leftBound, GEOMETRY_LINE);
  double rdist = me.dist(rightBound, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

LaneLabel Utils::getCurrentLane(VehicleState &vehState, Map *map)
{
  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  LaneLabel lane;
  map->getLane(lane, currPos);
  return lane;
}

bool Utils::isReverse(VehicleState &vehState, Map *map, LaneLabel &lane)
{
  double currlane_angle;
  point2 pt;

  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearaxle = AliceStateHelper::getPositionRearAxle(vehState);

  map->getHeading(currlane_angle, pt, lane, alice_rearaxle);
  double diff_angle = fabs(getAngleInRange(alice_angle-currlane_angle));
  
  return (diff_angle > M_PI/2);
}


double Utils::getDistToStopline(VehicleState &vehState, Graph_t* graph, Map *map, Path_t* m_path, deque<SegGoals>* seg_goal_queue, SegGoals& segGoal)
{
  point2arr path;
  // create point2arr out of path
  for (int i= 0; i < m_path->pathLen; i++) {
    if (!m_path->path[i]) return INFINITY;
    point2 p=point2(m_path->path[i]->pose.pos.x, m_path->path[i]->pose.pos.y);
    path.push_back(p);
  }

  // compute Alice distance to first path point
  double dist_alice;
  point2 alice = AliceStateHelper::getPositionFrontBumper(vehState);
  map->getDistAlongLine(dist_alice, path, alice);

  double dist_path = INFINITY;

  SegGoals goal_queue;

  for (int i= 0; i < m_path->pathLen; i++) {      
    GraphNode* node = m_path->path[i];

    if (node == NULL)
      return INFINITY;

    // Get current lane, only get the distance of the first stopline in the seggoals
    if (node->isStop) {
      double d3;
      point2 p3 = point2(node->pose.pos.x, node->pose.pos.y);
      map->getDistAlongLine(d3, path, p3);
      dist_path = d3 - dist_alice + node->distToStop;
      Log::getStream(9) << "DistToStop: " << node->distToStop << endl;
      Log::getStream(9) << "Stopline Coord: " << p3 << endl;

      // try finding corresponding segGoal
      for (unsigned int j=0; j<seg_goal_queue->size(); j++) {
        SegGoals *goal =  &((*(seg_goal_queue))[j]);
        if (goal->segment_type != SegGoals::INTERSECTION) continue;

        // Get position of stopline
        GraphNode* node2 = graph->getNodeFromRndfId(goal->entrySegmentID, goal->entryLaneID, goal->entryWaypointID);
        if (node2 == NULL) continue;

        vec3_t v = { node2->pose.pos.x - node->pose.pos.x, node2->pose.pos.y - node->pose.pos.y, 0 };
        if (vec3_mag(v) < 5.0) {
          segGoal = *goal;
          return dist_path;
        }
      }

      // If there is no corresponding stopline found in seggoals, return waypoint of stop node and fake the Intersection Type
      for (int k= i+1; k < m_path->pathLen; k++) {      
        GraphNode* node3 = m_path->path[k];

        if (node3 == NULL)
          continue;

        if (node3->waypointId != 0) {
          segGoal.entrySegmentID = node->segmentId;
          segGoal.entryLaneID = node->laneId;
          segGoal.entryWaypointID = node->waypointId;
          segGoal.exitSegmentID = node3->segmentId;
          segGoal.exitLaneID = node3->laneId;
          segGoal.exitWaypointID = node3->waypointId;
          segGoal.segment_type = SegGoals::INTERSECTION;
          segGoal.intersection_type = SegGoals::INTERSECTION_STRAIGHT;
          Log::getStream(1)<<"getDistToStop did not find corresponding SegGoal. Send "<<segGoal.entrySegmentID<<"."<<segGoal.entryLaneID<<"."<<segGoal.entryWaypointID<<" instead."<<endl;
          return dist_path;
        }
      }
    }
  }

  segGoal.entrySegmentID = 0;
  segGoal.entryLaneID = 0;
  segGoal.entryWaypointID = 0;
  segGoal.exitSegmentID = 0;
  segGoal.exitLaneID = 0;
  segGoal.exitWaypointID = 0;
  return dist_path;
}

double Utils::getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal)
{
  // Get current lane
  LaneLabel current_lane(seg_goal.exitSegmentID, seg_goal.exitLaneID);

  return getDistToStopline(vehState, map, current_lane);
}

double Utils::getDistToStopline(point2 &pos, Map *map, LaneLabel &current_lane)
{
  // Get all stoplines on that lane
  vector<PointLabel> stoplines;
  map->getLaneStopLines(stoplines, current_lane);

  // Get center line
  point2arr centerline;
  map->getLaneCenterLine(centerline, current_lane);

  // extend centerline in case that stop line is at the beginning/end of centerline
  point2arr temp, extendCenterline;
  map->extendLine(temp, centerline, 20);
  map->extendLine(extendCenterline, temp, -20);


  // Find the closest stopline on the lane on front of use
  point2 stop_position;
  double dist, min_dist = -1;
  for (unsigned int i=0; i<stoplines.size(); i++) {
    map->getWaypoint(stop_position, stoplines[i]);
    map->getDistAlongLine(dist, extendCenterline, stop_position, pos);
    if (dist > -1.0 && (dist < min_dist || min_dist == -1)) {
      min_dist = dist;
    }
  }

  return min_dist;
}

double Utils::getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &current_lane)
{
  // Set position
  point2 currFrontPos;
  currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  return getDistToStopline(currFrontPos, map, current_lane);
}


double Utils::getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal)
{
  double dist;
  LaneLabel current_lane;
  map->getLane(current_lane, AliceStateHelper::getPositionFrontBumper(vehState));

  LaneLabel waypoint_lane(seg_goal.entrySegmentID, seg_goal.entryLaneID);

  if (current_lane == waypoint_lane) {
    point2arr centerline;
    map->getLaneCenterLine(centerline, waypoint_lane);
    point2 entry_position;
    PointLabel entryWaypoint = PointLabel(seg_goal.entrySegmentID, seg_goal.entryLaneID, seg_goal.entryWaypointID);
    map->getWaypoint(entry_position, entryWaypoint);

    map->getDistAlongLine(dist, centerline, entry_position, AliceStateHelper::getPositionFrontBumper(vehState));
  }

  return dist;
}

double Utils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

double Utils::getNearestObsInLane(MapElement ** me, VehicleState &vehState, Map *map, LaneLabel &lane)
{
  vector<MapElement> obstacles;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  bool is_reverse = isReverse(vehState, map, lane);

  int obsErr = map->getObsInLane(obstacles, lane);
  if (obsErr < 1) //no obstacles
    return -1;

  int obs_index = -1;
  double min_dist = INFINITY;
  double dist;
  point2 obs_pt;
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  for (unsigned int i=0; i<obstacles.size(); i++) {
    for (unsigned int j=0; j<obstacles[i].geometry.size(); j++) {
      obs_pt.set(obstacles[i].geometry[j]);
      map->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
      if (is_reverse) dist = -dist;
      if (dist > 0.0 && dist < min_dist) {
        min_dist = dist;
        obs_index = i;
      }
    }
  }

  if (min_dist == INFINITY)
    return -1;

  if (me != NULL) {
    *me = &obstacles[obs_index];
  }

  return min_dist; 
}

double Utils::getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane)
{
  return getNearestObsInLane(NULL, vehState, map, lane);
}

double Utils::monitorAliceSpeed(VehicleState &vehState)
{
  static bool first_time_stopped = true;
  static uint64_t stopped_since = 0;
  double current_velocity = AliceStateHelper::getVelocityMag(vehState);

  if (current_velocity < 0.2) {
    if (first_time_stopped) {
      stopped_since = getTime();
      first_time_stopped = false;
    }
    return (double)(getTime()-stopped_since)/1000000.0;
  } else {
    first_time_stopped = true;
    return 0.0;
  }
  return 0.0;
}

void Utils::updateStoppedMapElements(Map *map)
{
  MapElement *mapEl;
  point2 point;
  LaneLabel obs_lane;
  double dist;

  for (int j = 0; j < (int)map->usedIndices.size(); j++) {
    mapEl = &map->newData[map->usedIndices.at(j)].mergedMapElement;

    if (!mapEl->isVehicle()) continue;
    if (mapEl->timeStopped < 10.0) continue;

    // Check if obstacle is close to an intersection
    point.set(mapEl->center);
    map->getLane(obs_lane, point);
    dist = getDistToStopline(point, map, obs_lane);
    if (dist < 30) {
      // We have at most 4 roads going into an intersection so each vehicle
      // at the stopline has to wait at most 30 seconds
      if (mapEl->timeStopped > dist*40.0/(1.5*VEHICLE_LENGTH)) // MAGIC
        mapEl->type = ELEMENT_OBSTACLE;
    } else {
      mapEl->type = ELEMENT_OBSTACLE;
    }
  }
}
 

uint64_t Utils::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

int Utils::reverseProjection(point2 &out, point2 pt, double yaw, point2arr on, double max_dist, double extension)
{
  bool swapped;

  /* for all segments on the point2arr, find the intersection */
  point2 pt1, pt2, tmpPt;
  for (unsigned int i=0; i<on.size()-1; i++) {
    swapped = false;

    pt1.set(on[i]-pt);
    pt2.set(on[i+1]-pt);

    pt1 = pt1.rot(-yaw);
    pt2 = pt2.rot(-yaw);

    /* Rotate pt1 and pt2 to the pt/yaw coords */
    if (pt1.x > pt2.x) {
      swapped = true;
      tmpPt.set(pt2);
      pt2.set(pt1);
      pt1.set(tmpPt);
    } else if (pt1.x == pt2.x) {
      if (0 != pt1.x) continue;
      else {
        if (pt1.y > pt2.x) {
          tmpPt.set(pt2);
          pt2.set(pt1);
          pt1.set(tmpPt);
        }
        if ((0 < pt1.y) && (fabs(pt1.y) <= max_dist)) {
          pt1.rot(yaw); out.set(pt1 + pt);
          return 0;
        } else if ((0 > pt2.y) && (fabs(pt2.y) <= max_dist)) {
          pt2.rot(yaw); out.set(pt2+pt);
          return 0;
        } else if ((0 <= pt2.y) && (0 >= pt1.y)) {
          out.set(pt);
          return 0;
        }
        continue;
      }
    }

    /* Check for intersection */
    if (i == 0) {
      if (swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (i == on.size()-2) {
      if (!swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (0 < pt1.x || 0 > pt2.x) continue;

    double a = (pt2.y-pt1.y)/(pt2.x-pt1.x);
    double b = pt1.y - a*pt1.x;
    out.x = 0;
    out.y = b;
    if (fabs(b) > max_dist) continue;
    out = out.rot(yaw);
    out.set(out + pt);
    return 0;
  }
  return -1;
}

int Utils::distToProjectedPoint(point2 &out, double &distance, point2 pt, double yaw, point2arr on, double max_dist, double extension)
{
  if (on.size() < 1)
    return -1;

  bool swapped;
  distance = 0.0;
  
  /* for all segments on the point2arr, find the intersection */
  point2 pt1, pt2, tmpPt;
  for (unsigned int i=0; i<on.size()-1; i++) {
    swapped = false;

    pt1.set(on[i]-pt);
    pt2.set(on[i+1]-pt);

    pt1 = pt1.rot(-yaw);
    pt2 = pt2.rot(-yaw);

    /* calculate the current dist */
    distance += pt1.dist(pt2);

    /* Rotate pt1 and pt2 to the pt/yaw coords */
    if (pt1.x > pt2.x) {
      swapped = true;
      tmpPt.set(pt2);
      pt2.set(pt1);
      pt1.set(tmpPt);
    } else if (pt1.x == pt2.x) {
      if (0 != pt1.x) continue;
      else {
        if (pt1.y > pt2.x) {
          tmpPt.set(pt2);
          pt2.set(pt1);
          pt1.set(tmpPt);
        }
        if ((0 < pt1.y) && (fabs(pt1.y) <= max_dist)) {
          pt1.rot(yaw); out.set(pt1 + pt);
          return 0;
        } else if ((0 > pt2.y) && (fabs(pt2.y) <= max_dist)) {
          pt2.rot(yaw); out.set(pt2+pt);
          return 0;
        } else if ((0 <= pt2.y) && (0 >= pt1.y)) {
          out.set(pt);
          return 0;
        }
        continue;
      }
    }

    /* Check for intersection */
    if (i == 0) {
      if (swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (i == on.size()-2) {
      if (!swapped) {
        if (0 < pt1.x || 0 > pt2.x + extension) continue;
      } else {
        if (0 < pt1.x - extension || 0 > pt2.x) continue;
      }
    } else if (0 < pt1.x || 0 > pt2.x) continue;

    double a = (pt2.y-pt1.y)/(pt2.x-pt1.x);
    double b = pt1.y - a*pt1.x;
    out.x = 0;
    out.y = b;
    if (fabs(b) > max_dist) continue;
    /* If we used the extension, it is little more tricky */
    if (0 < pt1.x || 0 > pt2.x) {
      if (i == 0) {
        if (swapped) distance -= pt1.dist(out);
        else distance -= pt2.dist(out);
      } else {
	if (swapped) distance += pt1.dist(out);
	else distance += pt2.dist(out);
      }
    /* Substract the extra distance we added previously */
    } else {
      if (swapped) distance -= pt1.dist(out);
      else distance -= pt2.dist(out);
    }
    out = out.rot(yaw);
    out.set(out + pt);
    return 0;
  }
  return -1;
}



