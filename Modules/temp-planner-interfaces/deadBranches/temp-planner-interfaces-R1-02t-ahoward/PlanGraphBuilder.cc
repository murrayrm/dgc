
/* 
 * Desc: Generates a dense graph from an RNDF file.
 * Date: 4 Oct 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <alice/AliceConstants.h>
//#include <dgcutils/DGCutils.hh>
#include <trajutils/maneuver.h>

#include "PlanGraphBuilder.hh"


// Error handling
#define MSG(fmt, ...) \
  fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)


// Constructor
PlanGraphBuilder::PlanGraphBuilder(PlanGraph *graph)
{
  this->graph = graph;
  
  this->wheelBase = VEHICLE_WHEELBASE;
  this->maxSteer  = VEHICLE_MAX_AVG_STEER;

  this->railCount = 5;
  this->railSpacing = 0.75;
     
  this->spacing = 1.0; // MAGIC
  
  this->roiIndex = 0;
  
  return;
}


// Destructor
PlanGraphBuilder::~PlanGraphBuilder()
{  
  return;
}

 
// Build from RNDF file
int PlanGraphBuilder::build(char *filename)
{
  // Load the reference RNDF into the plan graph
  if (this->graph->rndf.load(filename) != 0)
    return -1;
  
  // Build the road graph; we will use this to guide construction.
  if (this->road.load(filename) != 0)
    return -1;
  
  // Generate the dense graph
  if (this->genGraph() != 0)
    return -1;
  
  return 0;
}


// Load from PG file
int PlanGraphBuilder::load(char *filename)
{
  // Load directly
  if (graph->load(filename) != 0)
    return -1;

  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);
  
  return 0;
}


// Generate the dense graph
int PlanGraphBuilder::genGraph()
{
  int i, w;
  
  MSG("min quad size %d, min node size %d",
      sizeof(PlanGraphQuad), sizeof(PlanGraphNode));
        
  // Generate lanes/rails
  w = (this->railCount - 1) / 2;
  for (i = -w; i <= w; i++)
    this->genLanes(i, i * this->railSpacing);

  MSG("pass 1: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  // Generate turns
  this->genTurns();

  MSG("pass 2: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  // Generate feasible lane/rail changes.  Only the feasibility info,
  // not the full maneuver, is added to the graph.
  this->genFeasibleChanges();
  
  MSG("final: quads %d (%.3fMb) nodes %d (%.3fMb)",
      this->graph->quadCount, this->graph->quadBytes * 1e-6,
      this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  return 0;
}


// Generate the initial graph from the road graph
int PlanGraphBuilder::genLanes(int railId, float offset)
{
  int i, j;
  pose2f_t pose;
  RoadGraphNode *rnode, *rnodeA, *rnodeB;
  PlanGraphNode *node, *nodeA, *nodeB;
  
  // Add all of the nodes from the road graph.
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnode = this->road.nodes + i;

    // Reset user data so we dont get confused.
    rnode->data = NULL;
    
    // Skip zone boundaries
    if (rnode->laneId == 0)
      continue;

    // Dont generate intersections at this time
    if (rnode->flags.isTurn)
      continue;
    
    // Apply a lateral offset to the node in the road graph
    pose.pos.x = rnode->pose.pos.x - offset*sin(rnode->pose.rot);
    pose.pos.y = rnode->pose.pos.y + offset*cos(rnode->pose.rot);
    pose.rot = rnode->pose.rot;

    // Create the new node
    node = this->graph->allocNode(pose.pos.x, pose.pos.y);
    if (!node)
      return ERROR("unable to allocate node %d", this->graph->nodeCount);

    node->pose.rot = rnode->pose.rot;
    node->segmentId = rnode->segmentId;
    node->laneId = rnode->laneId;
    node->waypointId = rnode->waypointId;
    node->interId = rnode->interId;
    node->railId = railId;
    node->flags.isStop = rnode->flags.isStop;
    node->flags.isExit = rnode->flags.isExit;
    node->flags.isEntry = rnode->flags.isEntry;
    node->prevWaypoint = rnode->prevWaypoint;
    node->nextWaypoint = rnode->nextWaypoint;
    
    // Store a pointer to the plan node in the road node; we will use
    // this later to build arcs.
    rnode->data = (void*) node;
  }

  // Add in feasable arcs along lanes
  for (i = 0; i < this->road.numNodes; i++)
  {
    rnodeA = this->road.nodes + i;
    if (!rnodeA->data)
      continue;

    for (j = 0; j < rnodeA->numNext; j++)
    { 
      rnodeB = rnodeA->next[j];
      if (!rnodeB->data)
        continue;
 
      nodeA = (PlanGraphNode*) rnodeA->data;
      assert(nodeA);
      nodeB = (PlanGraphNode*) rnodeB->data;
      assert(nodeB);

      /* TESTING TODO
      if (!this->checkFeasibleStep(nodeA->pose, nodeB->pose))
      {
        MSG("infeasible lane %d.%d.%d.%d to %d.%d.%d.%d",
            nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId,
            nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId);
        continue;
      }
      */

      this->graph->insertArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Generate turn maneuvers through intersections
int PlanGraphBuilder::genTurns()
{
  int i, j;
  RNDFGraph *rndf;
  RNDFGraphWaypoint *wpA, *wpB;

  rndf = &this->road.rndf;

  // Use the RNDF graph to find matching entry/exit points.
  // We may generate multiple turns for each entry/exit pair.
  for (i = 0; i < rndf->numWaypoints; i++)
  {
    wpA = rndf->waypoints + i;
    if (!wpA->flags.isExit)
      continue;
    for (j = 0; j < wpA->numNext; j++)
    {
      wpB = wpA->next[j];
      if (!wpB->flags.isEntry)
        continue;
      this->genTurnWaypoints(wpA, wpB);
    }    
  }
  
  return 0;
}


// Generate turn manuevers for the given waypoints
int PlanGraphBuilder::genTurnWaypoints(RNDFGraphWaypoint *wpA, RNDFGraphWaypoint *wpB)
{
  int i, j;
  float size;
  bool feasible;
  PlanGraphNodeList nodesA, nodesB;
  PlanGraphNode *nodeA, *nodeB;

  //MSG("gen turns from %d.%d.%d to %d.%d.%d",
  //    wpA->segmentId, wpA->laneId, wpA->waypointId,
  //    wpB->segmentId, wpB->laneId, wpB->waypointId);

  // Compute a size that is sure to capture all the rails.
  size = 16 + 4 * this->railCount * this->railSpacing;

  // Get all the nodes in the vicinity of the entry/exit waypoints.
  this->graph->getRegion(&nodesA, wpA->px, wpA->py, size);
  this->graph->getRegion(&nodesB, wpB->px, wpB->py, size);

  assert(nodesA.size() > 0);
  assert(nodesB.size() > 0);
  
  feasible = false;
  
  // Look through the lists to find the correct entry/exit nodes,
  // (there may be multiple matches if there are multiple rails), then
  // generate the turn maneuver between these nodes.
  for (i = 0; i < (int) nodesA.size(); i++)
  {
    nodeA = nodesA[i];

    if (nodeA->segmentId != wpA->segmentId)
      continue;
    if (nodeA->laneId != wpA->laneId)
      continue;
    if (nodeA->waypointId != wpA->waypointId)
      continue;
    if (!nodeA->flags.isExit)
      continue;
    
    for (j = 0; j < (int) nodesB.size(); j++)
    {
      nodeB = nodesB[j];
          
      if (nodeB->segmentId != wpB->segmentId)
        continue;
      if (nodeB->laneId != wpB->laneId)
        continue;
      if (nodeB->waypointId != wpB->waypointId)
        continue;
      if (!nodeB->flags.isEntry)
        continue;

      // TESTING
      if (nodeA->railId != nodeB->railId)
        continue;
      
      if (this->genTurnNodes(nodeA, nodeB) == 0)
        feasible = true;
    }
  }

  // Where any of these maneuvers feasible?
  if (!feasible)
  {
    MSG("infeasible turn %d.%d.%d to %d.%d.%d",
        wpA->segmentId, wpA->laneId, wpA->waypointId, 
        wpB->segmentId, wpB->laneId, wpB->waypointId);
    assert(false);
  }

  return 0;
}


// Generate a turn maneuver between the given nodes
int PlanGraphBuilder::genTurnNodes(PlanGraphNode *nodeA, PlanGraphNode *nodeB)
{
  int i, steps;
  bool feasible;
  
  //MSG("gen turns from %+d to %+d", nodeA->railId, nodeB->railId);

  // Max number of steps to take along the entry/exit rails to broaden
  // the turn.
  steps = (int) (16.0 / this->spacing);   // MAGIC

  feasible = false;
  for (i = 0; i < steps; i++)
  {
    // TESTING
    // See if this turn is feasible.
    //if (this->checkFeasibleManeuver(nodeA->pose, nodeB->pose))
    {
      // See if this turn stays on the road
      if (true)
      {
        // Insert this manuever
        this->insertManeuver(nodeA, nodeB, false);
        feasible = true;
        break;
      }
    }

    // Try a wider turn by stepping along both the entry and exit rails.
    if (nodeA->numPrev == 1)
      nodeA = nodeA->prev[0];
    if (nodeB->numNext == 1)
      nodeB = nodeB->next[0];
  }

  if (!feasible)
  {
    //MSG("infeasible turn %d.%d.%d.%d %+d to %d.%d.%d.%d %+d",
    //    nodeA->segmentId, nodeA->laneId, nodeA->waypointId, nodeA->interId, nodeA->railId,
    //    nodeB->segmentId, nodeB->laneId, nodeB->waypointId, nodeB->interId, nodeB->railId);
    return -1;
  }

  return 0;
}


// Generate feasible rail-change maneuvers (recursive)
int PlanGraphBuilder::genFeasibleChanges(PlanGraphQuad *quad)
{
  int i, j;
  float size, spacing;
  PlanGraphQuad *leaf;
  PlanGraphNodeList nodes;    
  PlanGraphNode *nodeA, *nodeB;
  vec2f_t pos;
    
  // MAGIC
  size = 64;
  spacing = this->road.spacing;

  if (quad == NULL)
    quad = this->graph->root;

  if ((int) (quad->size / this->graph->quadScale) == 32)
    MSG("changes for quad %.0f %.0f (%.0f)", quad->px, quad->py, quad->size);

  // Recurse into leaves 
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->genFeasibleChanges(leaf);
  }
  if (quad->numNodes == 0)
    return 0;
  
  // Consider all nodes in the quad.
  for (i = 0; i < quad->numNodes; i++)
  {
    nodeA = quad->nodes[i];

    // MAGIC
    if (nodeA->interId % 10 != 0)
      continue;
    
    // Get nodes in some local region
    this->graph->getRegion(&nodes, nodeA->pose.pos.x, nodeA->pose.pos.y, size);

    for (j = 0; j < (int) nodes.size(); j++)
    {
      nodeB = nodes[j];
      
      // MAGIC
      if (nodeB->interId % 10 != 0)
        continue;

      // Dont make changes between segments
      if (nodeB->segmentId != nodeA->segmentId)
        continue;
      
      // Dont make changes to/from the same lane and rail
      if (nodeB->laneId == nodeA->laneId &&
          nodeB->railId == nodeA->railId)
        continue;

      // Move at most one lane at a time
      if (abs(nodeB->laneId - nodeA->laneId) > 1)
        continue;

      // If changing within a lane, shift left/right one rail only
      if (nodeB->laneId == nodeA->laneId &&
          abs(nodeB->railId - nodeA->railId) > 1)
        continue;

      // If changing between lanes, shift only to the same rail in the
      // other lane.
      if (nodeB->laneId != nodeA->laneId &&
          nodeB->railId != nodeA->railId)
        continue;

      // Compute end-point in frame of first node
      pos = vec2f_transform(pose2f_inv(nodeA->pose), nodeB->pose.pos);

      // HACK
      // Dont make changes that are too far or too near.
      if (pos.x < 5.0 || pos.x > 30.0) // MAGIC
        continue;

      // HACK
      // Dont make large lateral deviations
      if (fabs(pos.y) > 5.0) // MAGIC
        continue;

      //MSG("building %d.%d.%d:%+d to %d.%d.%d:%+d",
      //    nodeA->segmentId, nodeA->laneId, nodeA->interId, nodeA->railId,
      //    nodeB->segmentId, nodeB->laneId, nodeB->interId, nodeB->railId);

      // Do a feasibility check based on vehicle kinematics
      if (!this->checkFeasibleManeuver(nodeA->pose, nodeB->pose))
        continue;

      // TODO
      // Check if the maneuver is within the road boundaries

      // Insert a feasible arc; this will get expanded only if the
      // node is in the ROI.
      this->graph->insertFeasibleArc(nodeA, nodeB);
    }
  }
  
  return 0;
}


// Check for feasibility of an extended maneuver
bool PlanGraphBuilder::checkFeasibleManeuver(pose2f_t poseA, pose2f_t poseB)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB, config;
  pose2f_t src, dst;
  double  dm, s;
  int i, numSteps;
  float spacing;

  // Step size
  spacing = this->spacing;

  // Initial vehicle pose
  configA.x = poseA.pos.x;
  configA.y = poseA.pos.y;
  configA.theta = poseA.rot;

  // Final vehicle configuration
  configB.x = poseB.pos.x;
  configB.y = poseB.pos.y;
  configB.theta = poseB.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(poseB.pos, poseA.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);
  
  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);

  // Check for feasibility along the entire maneuver, including the
  // final step.
  src.pos = vec2f_set(configA.x, configA.y);
  src.rot = configA.theta;
  for (i = 1; i < numSteps + 1; i++)
  {
    s = (double) i / numSteps;  
    config = maneuver_evaluate_pose(vp, mp, s);    
    dst.pos = vec2f_set(config.x, config.y);
    dst.rot = config.theta;    
    if (!this->checkFeasibleStep(src, dst))
      break;
    src = dst;
  }

  // Clean up
  maneuver_free(mp);
  free(vp);

  // Not feasible if we did not get to the end
  if (i < numSteps + 1)
    return false;
  
  return true;
}


// Do check for unfeasible maneuvers using the vehicle turning circle.
bool PlanGraphBuilder::checkFeasibleStep(pose2f_t poseA, pose2f_t poseB)
{
  float turnRadius;
  pose2f_t pose;
  float x0, y0, dx, dy, s, t;
    
  // Vehicle turning radius
  turnRadius = this->wheelBase / tan(this->maxSteer);
  
  // Final pose in the initial frame
  pose = pose2f_mul(pose2f_inv(poseA), poseB);

  // Compute equation of line at right angles to current pose.
  // x = t dx + x0.
  // y = t dy + y0.
  x0 = pose.pos.x;
  y0 = pose.pos.y;
  dx = -sin(pose.rot);
  dy = +cos(pose.rot);

  // Trap straight-line case to avoid div-by-zero.
  if (fabs(dx) < 1e-6)
    return true;
  
  // Find intercept with x = 0; the values t and s specify the radii
  // of two turning circles.
  t = -x0 / dx;
  s = t * dy + y0;

  //MSG("check %f %f : %f %f : %f %f : %f", x0, y0, dx, dy, t, s, turnRadius);

  if (fabs(t) < turnRadius)
    return false;
  if (fabs(s) < turnRadius)
    return false;
  if (s * t < 0)
    return false;
        
  return true;
}




// Update the region of interest
int PlanGraphBuilder::updateROI(float px, float py, float size)
{
  // Check for changes before updating the ROI
  if (this->roiIndex > 0 &&
      fabs(px - this->roiPx) < this->graph->quadScale &&
      fabs(py - this->roiPy) < this->graph->quadScale &&
      fabs(size - this->roiSize) < this->graph->quadScale)
    return 0;
  
  // Update the ROI index; we use this to figure out which nodes are
  // in the new ROI, and which are in the old ROI.  Note that there
  // could be some weirdness if the index rolls around to zero, but
  // this will take thousands of hours under normal usage.
  this->roiIndex++;
  assert(this->roiIndex > 0);

  // Expand all nodes in the new ROI.  This may timeout, in which case
  // only some nodes are expanded.  These will be expanded on
  // subsequent calls to updateROI.
  this->expandROI(this->graph->root, px, py, size, this->roiIndex);

  // Contract all nodes in the old ROI that are not in the new ROI.
  // We should not call this on the first time through.
  if (this->roiIndex > 1)
    this->contractROI(this->graph->root, this->roiPx, this->roiPy, this->roiSize, this->roiIndex);
  
  // Keep track of the ROI so we can correctly contract it next time
  // through.  Only do this if the process completes within the timeout.
  this->roiPx = px;
  this->roiPy = py;
  this->roiSize = size;

  //MSG("ROI nodes %d (%.3fMb)",
  //    this->graph->nodeCount, this->graph->nodeBytes * 1e-6);

  return 0;
}


// Expand nodes in the ROI
int PlanGraphBuilder::expandROI(PlanGraphQuad *quad,
                                float px, float py, float size, uint32_t index)
{
  int i, j;
  PlanGraphQuad *leaf;
  PlanGraphNode *nodeA, *nodeB;
  
  // If this quad has no intersection with the ROI, skip it.
  if (!quad->hasIntersection(px, py, size))
    return 0;

  // Recurse into leaves
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->expandROI(leaf, px, py, size, index);
  }

  // Look for expandable nodes in this quad.
  for (i = 0; i < quad->numNodes; i++)
  {
    nodeA = quad->nodes[i];

    // If node is already expanded, update the index only.
    if (nodeA->roiIndex > 0)
    {
      nodeA->roiIndex = index;
      continue;
    }

    // Expand all feasible arcs for this node in to feasible
    // maneuvers.
    for (j = 0; j < nodeA->numDest; j++)
    {
      nodeB = nodeA->dest[j];
      this->insertManeuver(nodeA, nodeB, true);
    }
    nodeA->roiIndex = index;
  }

  return 0;
}


// Contract nodes in the ROI
int PlanGraphBuilder::contractROI(PlanGraphQuad *quad,
                                  float px, float py, float size, uint32_t index)
{
  int i, j;
  PlanGraphQuad *leaf;
  PlanGraphNode *nodeA, *nodeB;

  // If this quad has no intersection with the ROI, skip it.
  if (!quad->hasIntersection(px, py, size))
    return 0;

  // Recurse into leaves
  for (i = 0; i < 4; i++)
  {
    leaf = quad->leaves[i];
    if (leaf)
      this->contractROI(leaf, px, py, size, index);
  }
  
  // Look for contractable nodes in this quad (i.e., if they are not
  // in the current ROI).
  for (i = 0; i < quad->numNodes; i++)
  {
    nodeA = quad->nodes[i];
        
    // If node is not expanded, skip it.  
    if (nodeA->roiIndex == 0)
      continue;

    // If the node is expanded, but belongs in the ROI, skip it.
    if (nodeA->roiIndex == index)
      continue;

    // Contract feasible maneuvers for this node.
    for (j = 0; j < nodeA->numDest; j++)
    {
      nodeB = nodeA->dest[j];
      this->removeManeuver(nodeA, nodeB, true);
    }
    nodeA->roiIndex = 0;
  }

  return 0;
}


// Insert a feasible maneuver between the given nodes.
int PlanGraphBuilder::insertManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, bool roi)
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D configA, configB, config;
  double  dm, s;
  int i, numSteps;
  PlanGraphNode *src, *dst;
  float spacing;

  assert(nodeA);
  assert(nodeB);

  // Node spacing
  spacing = this->spacing;
  
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  configA.theta = nodeA->pose.rot;

  // Final vehicle configuration
  configB.x = nodeB->pose.pos.x;
  configB.y = nodeB->pose.pos.y;
  configB.theta = nodeB->pose.rot;
  
  // Distance between nodes
  dm = vec2f_mag(vec2f_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / spacing);

  // Vehicle properties
  vp = maneuver_create_vehicle(this->wheelBase, this->maxSteer);
  
  // Create maneuver object
  mp = maneuver_pose2pose(vp, &configA, &configB);
  assert(mp);
  
  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_pose(vp, mp, s);    
    
    // Create a maneuver node
    if (roi)
      dst = this->graph->allocRoiNode(config.x, config.y);
    else
      dst = this->graph->allocNode(config.x, config.y);
    assert(dst);

    // Fill out basic node data
    dst->pose.pos = vec2f_set((float) config.x, (float) config.y);
    dst->pose.rot = config.theta;

    // Fill out nominal RNDF data.  TODO should we use the nodeA or nodeB?
    dst->segmentId = nodeA->segmentId;
    dst->laneId = nodeA->laneId;
    dst->waypointId = nodeA->waypointId;
    dst->interId = i;

    // Record the final destination for this maneuver.  This help us
    // later when we want to delete the maneuver.
    dst->currentDest = nodeB;

    // Fill out waypoint pointers
    dst->prevWaypoint = nodeA->prevWaypoint;
    dst->nextWaypoint = nodeB->nextWaypoint;
    
    // Create an arc from the previous node to the new node
    this->graph->insertArc(src, dst);

    src = dst;
  }
  dst = nodeB;

  // Create an arc to the final node
  this->graph->insertArc(src, dst);
  
  maneuver_free(mp);
  free(vp);

  return 0;
}


// Remove a maneuver between the given nodes.
int PlanGraphBuilder::removeManeuver(PlanGraphNode *nodeA, PlanGraphNode *nodeB, bool roi)
{
  int i;
  PlanGraphNode *src, *dst;

  // Walk along the route between nodeA and nodeB, deleting the nodes
  // and arcs as we go.
  src = nodeA;
  while (src != nodeB)
  {
    dst = NULL;
    for (i = 0; i < src->numNext; i++)
    {
      if (src->next[i] == nodeB || src->next[i]->currentDest == nodeB)
      {
        dst = src->next[i];
        break;
      }
    }
    assert(dst);

    // Delete arc and node
    this->graph->removeArc(src, dst);
    if (src != nodeA)
    {
      if (roi)
        this->graph->freeRoiNode(src);
      else
        this->graph->freeNode(src);
    }
    
    src = dst;
  }

  return 0;
}
