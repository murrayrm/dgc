
/* 
 * Desc: Path data structure
 * Date: 29 April 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef GRAPH_PATH_HH
#define GRAPH_PATH_HH

#include "Graph.hh"


// Maximum path length
#define GRAPH_PATH_MAX_NODES 65535


/// @brief Class describing a planned path.
struct GraphPath
{
  // Is this path valid?
  bool valid;
  
  // Does this path have a collision with a static obstacle?
  int collideObs;

  // Does this path have a collision with a non-static car?
  int collideCar;

  // What is the distance to the goal along this path?
  double goalDist;
  
  // List of graph nodes in the path
  int pathLen;
  GraphNode *path[GRAPH_PATH_MAX_NODES];

  // Distance profile along the path
  double dists[GRAPH_PATH_MAX_NODES];
  
  // Speed profile along the path
  double speeds[GRAPH_PATH_MAX_NODES + 1];
};



#endif
