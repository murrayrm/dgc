#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include <math.h>
#include <iostream>

int main(int argc, char *argv[]) {

  int skynetKey = 0;
  int subgroup = -12;
  
  skynetKey = atoi(getenv("SKYNET_KEY"));  

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);

  _VehicleState state;
  state.localYaw = -.785;

  MapElement el;
  el.setState(state);
  el.setTypeObstacle();
  el.plotColor = MAP_COLOR_RED;
  double increment = .1;
  double orientation = M_PI/2;
  bool refresh;
  if (argc>1) {
    orientation = atoi(argv[1]);
    refresh = false;
  }
  refresh = true;
  cout<<"increment: "<<increment;

  point2 position(10,0);
  double lx = 2;
  double ly = 5;

  el.timeStopped = 0;

  bool car = true;
  bool circle = !car;

  double circleIncr = cos(M_PI/4);
  double circleRadius = 15;

  vector<point2> circlePoints;
  
  circlePoints.push_back(point2(circleRadius,0));
  circlePoints.push_back(point2(circleRadius*circleIncr,circleRadius*circleIncr));
  circlePoints.push_back(point2(0,circleRadius));
  circlePoints.push_back(point2(-circleRadius*circleIncr,circleRadius*circleIncr));
  circlePoints.push_back(point2(-circleRadius,0));
  circlePoints.push_back(point2(-circleRadius*circleIncr,-circleRadius*circleIncr));
  circlePoints.push_back(point2(0,-circleRadius));
  circlePoints.push_back(point2(circleRadius*circleIncr,-circleRadius*circleIncr));

  vector<point2>::iterator circleIter = circlePoints.begin();
  int circleCounter = 0;

  while (circleIter != circlePoints.end()) {
    cout<<*circleIter<<endl;
    circleIter++;
  }
  circleIter = circlePoints.begin();
  cout<<" AHA!"<<endl;

  // move the car
  while(1) {

    if (car)
      usleep(100000);
    else if (circle)
      usleep(1000000);

    if (refresh) {
      if (car) {
	position.x += increment;
	position.y = cos(position.x);
	orientation = acos(position.y);
      }
      else if (circle) {
	position = *circleIter;
	circleIter++;
	if (circleIter == circlePoints.end())
	  circleIter = circlePoints.begin();
      }
    }

    el.velocity = point2(1.5,.3);

    cout<<"("<<position.x<<","<<position.y<<")"<<endl;

    // return the four corners of the car
    std::vector<point2> boundary;
    
    double x = position.x;
    double y = position.y;
   
    if (circle) {

      double a = cos(M_PI/4);
      double r = 3;
      
      boundary.push_back(point2(r+x,0+y));
      boundary.push_back(point2(r*a+x,r*a+y));
      boundary.push_back(point2(0+x,r+y));
      boundary.push_back(point2(-r*a+x,r*a+y));
      boundary.push_back(point2(-r+x,0+y));
      boundary.push_back(point2(-r*a+x,-r*a+y));
      boundary.push_back(point2(0+x,-r+y));
      boundary.push_back(point2(r*a+x,-r*a+y));
    }
    
    else if (car) {
      
      double t = orientation;
      
      // put in the points at origin
      boundary.push_back(point2(.5*lx,.5*ly));
      boundary.push_back(point2(0,.7*ly));
      boundary.push_back(point2(-.5*lx,.5*ly));
      boundary.push_back(point2(-.5*lx,-.5*ly));
      boundary.push_back(point2(0,-.7*ly));
      boundary.push_back(point2(.5*lx,-.5*ly));
      
      double oldY, oldX;
      // rotate points and translate
      for (std::vector<point2>::iterator iter = boundary.begin();
	   iter != boundary.end(); iter++) {
	oldX = iter->x;
	oldY = iter->y;
	iter->x = oldX*cos(t) - oldY*sin(t) +x;
	iter->y = oldX*sin(t) + oldY*cos(t) +y;
      }
    
    }
    el.setGeometry(boundary);
    maptalker.sendMapElement(&el,subgroup);

  el.setColor(MAP_COLOR_GREY);
  el.geometryType =GEOMETRY_POINTS;
  el.setId(15);
  maptalker.sendMapElement(&el, -2);

  }

  return 0;
}
