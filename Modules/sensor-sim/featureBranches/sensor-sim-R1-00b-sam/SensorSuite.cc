#include "SensorSuite.hh"
#include <math.h>

SensorSuite::SensorSuite() 
{
}

SensorSuite::~SensorSuite()
{

}


void SensorSuite::initialize(int skynetKey, int recvSubgroup, int sendSubgroup, bool debug)
{
  // set up the MapElementTalker
  this->skynetKey = skynetKey;
  this->recvSubgroup = recvSubgroup;
  this->sendSubgroup = sendSubgroup;
  maptalker.initRecvMapElement(skynetKey,recvSubgroup);

  // set up all the sensors:

  // RIEGL: front middle bumper
  // currently not in use, so disabled by default
  Ladar riegl;
  riegl.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		   65,
		   -M_PI/4,
		   M_PI/4,
		   MODladarCarPerceptorRiegl,
		   skynetKey,
		   sendSubgroup,
		   debug);
  ladars.insert( make_pair( "MODladarCarPerceptorRiegl", riegl) );

  // LADARs

  // middle ladar, inside bumper
  Ladar MFBumper;
  MFBumper.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		      35,
		      -M_PI/2,
		      M_PI/2,
		      MODladarCarPerceptorCenter,
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( "MODladarCarPerceptorCenter", MFBumper) );

  // right front ladar, inside bumper and on top of bumper
  Ladar RFBumper;
  RFBumper.initialize(point2(DIST_REAR_AXLE_TO_FRONT,-VEHICLE_WIDTH/2),
		      35,
		      M_PI,
		      0,
		      MODladarCarPerceptorRight,
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( "MODladarCarPerceptorRight", RFBumper) );
  Ladar RFRoof;
  RFRoof.initialize(point2(DIST_REAR_AXLE_TO_FRONT,-VEHICLE_WIDTH/2),
		    35,
		    M_PI,
		    0,
		    MODladarCarPerceptorRoofRight,
		    skynetKey,
		    sendSubgroup,
		    debug);
  ladars.insert( make_pair( "MODladarCarPerceptorRoofRight", RFRoof) );

  // left front ladar, inside bumper and on top of bumper
  Ladar LFBumper;
  LFBumper.initialize(point2(DIST_REAR_AXLE_TO_FRONT,VEHICLE_WIDTH/2),
		      35,
		      0,
		      M_PI,
		      MODladarCarPerceptorLeft,
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( "MODladarCarPerceptorLeft", LFBumper) );
  Ladar LFRoof;
  LFRoof.initialize(point2(DIST_REAR_AXLE_TO_FRONT,VEHICLE_WIDTH/2),
		    35,
		    0,
		    M_PI,
		    MODladarCarPerceptorRoofLeft,
		    skynetKey,
		    sendSubgroup,
		    debug);
  ladars.insert( make_pair( "MODladarCarPerceptorRoofLeft", LFRoof) );
  
  // stereo cameras

  // medium range stereo, front of alice
  StereoCam MFMedium;
  MFMedium.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		      35,
		      -M_PI/5,
		      M_PI/5,
		      MODstereoObsPerceptorMedium,
		      skynetKey,
		      sendSubgroup,
		      debug);
  cameras.insert( make_pair( "MODstereoObsPerceptorMedium", MFMedium) );

  // long range stereo, front of alice
  // MODstereoObsPerceptorLong
  StereoCam MFLong;
  MFLong.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		    70,
		    -M_PI/8,
		    M_PI/8,
		    MODstereoObsPerceptorLong,
		    skynetKey,
		    sendSubgroup,
		    debug);
  cameras.insert( make_pair( "MODstereoObsPerceptorLong", MFLong) );

  // bumblebees
  // short range stereo, left side front
  StereoCam LFShort;
  LFShort.initialize(point2(DIST_REAR_AXLE_TO_FRONT,VEHICLE_WIDTH/2),
		     3,
		     -M_PI/8,
		     M_PI/8,
		     MODstereoFeederLFShort,
		     skynetKey,
		     sendSubgroup,
		     debug);
  cameras.insert( make_pair( "MODstereoFeederLFShort", LFShort) );
  
  // short range stereo, right side front
  StereoCam RFShort;
  RFShort.initialize(point2(DIST_REAR_AXLE_TO_FRONT,-VEHICLE_WIDTH/2),
		     3,
		     -M_PI/8,
		     M_PI/8,
		     MODstereoFeederRFShort,
		     skynetKey,
		     sendSubgroup,
		     debug);
  cameras.insert( make_pair( "MODstereoFeederRFShort", RFShort) );

  // RADAR and PTU
  // not currently implemented since there are currently changes being made

}


void SensorSuite::updateElems()
{
  MapElement el;

  // get a new map element
  int bytesRecv = 0;
  bytesRecv = maptalker.recvMapElementBlock(&el,recvSubgroup);
  //  cout << "bytes received = " << bytesRecv << endl;
  //  cout << "received el :" << el<< endl; 

  // iterate over the sensors and see of the mapelement falls under any of them
  // (make sure to skip non-working sensors)
  for (std::map<std::string, Ladar>::iterator it = ladars.begin();
       it != ladars.end(); it++) {
    if (it->second.isWorking) {
      it->second.simulate(el);
    }
  }
  for (std::map<std::string, StereoCam>::iterator it = cameras.begin();
       it != cameras.end(); it++) {
    if (it->second.isWorking) {
      it->second.simulate(el);
    }
  }

}

void SensorSuite::disableSensor(std::string sensorName)
{

  if (sensorName == "ALLLADARS") {
    cout<<"Disabling all LADARs"<<endl;
    for (map<std::string,Ladar>::iterator it = ladars.begin();
	 it != ladars.end(); it++) {
      it->second.isWorking = false;
    }
    return;
  }

  if (sensorName == "ALLSTEREO") {
    cout<<"Disabling all stereo cameras"<<endl;
    for (map<std::string,StereoCam>::iterator it = cameras.begin();
	 it != cameras.end(); it++) {
      it->second.isWorking = false;
    }
    return;
  }

  // disable the sensor
  map<std::string,Ladar>::iterator it1 = ladars.find(sensorName);
  if (it1 != ladars.end()) {
    cout<<"Disabling "<<sensorName<<endl;
    it1->second.isWorking = false;
  }
  map<std::string,StereoCam>::iterator it2 = cameras.find(sensorName);
  if (it2 != cameras.end())  {
    cout<<"Disabling "<<sensorName<<endl;
    it2->second.isWorking = false;
  }

  // TODO: send a message to the health-monitor

}

void SensorSuite::enableSensor(std::string sensorName)
{

  if (sensorName == "ALLLADARS") {
    cout<<"Enabling all LADARs"<<endl;
    for (map<std::string,Ladar>::iterator it = ladars.begin();
	 it != ladars.end(); it++) {
      it->second.isWorking = true;
    }
    return;
  }

  if (sensorName == "ALLSTEREO") {
    cout<<"Enabling all stereo cameras"<<endl;
    for (map<std::string,StereoCam>::iterator it = cameras.begin();
	 it != cameras.end(); it++) {
      it->second.isWorking = true;
    }
    return;
  }

  // enable the sensor
  map<std::string,Ladar>::iterator it1 = ladars.find(sensorName);
  if (it1 != ladars.end())  {
    cout<<"Enabling "<<sensorName<<endl;
    it1->second.isWorking = true;
  }
  map<std::string,StereoCam>::iterator it2 = cameras.find(sensorName);
  if (it2 != cameras.end())  {
    cout<<"Enabling "<<sensorName<<endl;
    it2->second.isWorking = true;
  }

  // TODO: send a message to the health-monitor

}
