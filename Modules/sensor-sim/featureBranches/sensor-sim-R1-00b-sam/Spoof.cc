/*!Spoof.cc
 * Author: Pete Trautman
 * Last revision: May 7 2007
 * */

#include "Spoof.hh"

using namespace std;

//ctors and dtors
Spoof::Spoof(int DOtype,double sigma,point2 pos,point2 vel,double badReturnRate)
{
   gsl_rng_env_setup();
   m_T = gsl_rng_default;
   m_r = gsl_rng_alloc (m_T);

   m_noReturn.x = 0;
   m_noReturn.y = 0;

   m_badReturnRate = badReturnRate;
   m_DOtype = DOtype;
   m_sensedSigma = sigma;
   m_dynObsPos = pos;
   m_dynObsVel = vel;
   m_ang = -atan(m_dynObsVel.x/m_dynObsVel.y);
}  

Spoof::~Spoof() 
{
}

//real functions
 
point2 Spoof::getTrueDynObsPos()
{
  return m_dynObsPos;
}

point2 Spoof::getTrueDynObsVel()
{
  return m_dynObsVel;
}

point2 Spoof::getSenseDynObsPos()
{
  double u;
  u = gsl_rng_uniform(m_r);

  m_Noise.x = gsl_ran_gaussian(m_r,m_sensedSigma);
  m_Noise.y = gsl_ran_gaussian(m_r,m_sensedSigma); 
   if(u>m_badReturnRate) 
    {
     return m_dynObsPos + m_Noise;
    }
   else
    {
      //return m_dynObsPos + 20*m_Noise;
      return m_noReturn;
    } 
}

point2 Spoof::getSenseDynObsVel()
{
  double u;
  u = gsl_rng_uniform(m_r);
  
  m_Noise.x = gsl_ran_gaussian(m_r,m_sensedSigma);
  m_Noise.y = gsl_ran_gaussian(m_r,m_sensedSigma); 
     if(u>m_badReturnRate) 
    {
     return m_dynObsVel + m_Noise;
    }
   else
    {
      //return m_dynObsVel + 20*m_Noise;
      return m_noReturn;
    } 
}


point2 Spoof::getStatObsPos()
{
  return m_statObsPos;
}

double Spoof::getdynObsAng()
{
  return m_ang;
}

void Spoof::dynObs(double deltaT)
{
  if(m_DOtype == 1) //constant velocity
  {
   m_Noise.x = gsl_ran_gaussian(m_r,m_sensedSigma);
   m_Noise.y = gsl_ran_gaussian(m_r,m_sensedSigma);    
    
   m_dynObsPos=m_dynObsPos+m_dynObsVel*deltaT;
    
   m_ang = -atan(m_dynObsVel.x/m_dynObsVel.y);
    
   //u = gsl_rng_uniform(m_r);
   //m_Noise.x = m_Noise.x/1;
   //u = gsl_rng_uniform(m_r);
   //m_Noise.y = u/1;
   //m_dynObstacleVel=m_dynObstacleVel+m_Noise*deltaT;
  }
  
  
}

void Spoof::statObs()
{
   
}
 



















