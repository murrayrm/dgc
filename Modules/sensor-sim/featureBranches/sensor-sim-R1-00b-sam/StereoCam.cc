#include "StereoCam.hh"

StereoCam::StereoCam()
{
}

StereoCam::~StereoCam()
{
}

bool StereoCam::isCorrectObjType(MapElementType type)
{

  // pretty much anything can be seen by a stereocamera,
  // but the perceptors on long and medium do not
  // pick up road lines

  // check if it's a road line
  if (type == ELEMENT_LINE ||
      type == ELEMENT_STOPLINE ||
      type == ELEMENT_LANELINE ||
      type == ELEMENT_PARKING_SPOT ||
      type == ELEMENT_PERIMETER) {

    // bumblebees
    if (groupID == MODstereoFeederLFShort &&
	groupID == MODstereoFeederRFShort)
      return true;
    
    // long and medium
    else 
      return false;
  }

  // all other elements:

  // medium and long
  if (groupID != MODstereoFeederLFShort &&
      groupID != MODstereoFeederRFShort)
    return true;

  // bumblebees
  return false;

}

void StereoCam::addNoise(MapElement& elem)
{
  // TODO: fill in noise fns
}
