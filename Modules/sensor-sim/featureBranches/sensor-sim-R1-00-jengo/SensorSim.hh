#ifndef __SENSORSIM_HH__
#define __SENSORSIM_HH__

#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "Sensor.hh"
#include "Object.hh"
#include "skynettalker/StateClient.hh"

class SensorSim : public CMapElementTalker, public CStateClient {

public:

  /// Constructor
  SensorSim(int skynetKey);
  
  /// Destructor
  ~SensorSim();

  /****** SIM LOOP FUNCTIONS *******/

  /// Update all the Objects (ie MapElements)
  void updateElems();

  /**** OTHER FUNCTIONS *****/
  
  /// Deletes an Object from the objectMap
  void deleteObj(MapId id);

  /// Add an object to the objectMap
  void addObj(Object obj);

private:

  /// Keep track of the current alice state
  MapElement alice;

  /// All of the mapelements we're keeping track of
  /// Keep the map id separate so we can search for objects better
  map<MapId, Objects> objectMap;
  

};

#endif
