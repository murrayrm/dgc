#ifndef __LADAR_HH__
#define __LADAR_HH__

#include "Sensor.hh"
#include <map>

class Ladar : public Sensor {

public:

  /// Constructor currently does nothing                                                                                                                    
  Ladar();

  /// Destructor currently does nothing                                                                                                                     
  ~Ladar();

  /**** FUNCTIONS ***/

  /// Checks if the given map element can be sensed by
  /// this particular perceptor
  virtual bool isCorrectObjType(MapElementType type);

  /// If necessary, modifies the object (adds noise, just gets the
  /// object's edge, etc), then sends it on to mapper
  virtual void simulate(MapElement elem, bool addNoiseToElem, bool probGeo);

  /// Sets certain parameters from the command line or a config file
  virtual void initParams(gengetopt_args_info* cmdline);

private:

  /// how often to generate "ghost" objects for cars
  /// ghosts are when an object changes id (to simulate the perceptor losing track)
  int carGhostFrequency;
  int MIN_CAR_GHOST_FREQ;
  double RANGE_CAR_GHOST_FREQ;

  /// how often to generate "ghost" objects for cars that are being labeled
  /// as obstacles for whatever reason.
  int carObsGhostFrequency;
  int MIN_CAR_OBS_GHOST_FREQ;
  double RANGE_CAR_OBS_GHOST_FREQ;

  /// how often to genereate "ghost" objects for obstacles. This random 
  /// number also doubles as noise for the # of times we must see an obstacle
  /// before we change the classification from ladarObstacle to Obstacle
  int obsGhostFrequency;
  int MIN_OBS_GHOST_FREQ;
  double RANGE_OBS_GHOST_FREQ;

  /// how often to lose track of an obstacle
  int dropoutFrequency;
  int MIN_DROPOUT_FREQ;
  double RANGE_DROPOUT_FREQ;

  /// noise to add to the velocity
  double velocityNoise;
  double MIN_VEL_NOISE;
  double RANGE_VEL_NOISE;

  /// The minimum velocity where perceptor can track vehicle
  double VEHICLE_MIN_VELOCITY;
  /// the maximum velocity where perceptor can track vehicle
  double VEHICLE_MAX_VELOCITY;
  /// The minimum amount of times we have to see a slow moving vehicle
  /// before we track it as a vehicle
  int VEHICLE_MIN_TRACKING;

  // count how many times we've gone through the addnoise loop
  int loopcounter;

};

#endif
