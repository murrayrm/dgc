#ifndef __SENSORSUITE_HH__
#define __SENSORSUITE_HH__

#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "Sensor.hh"
#include "Ladar.hh"
#include "StereoCam.hh"
#include "alice/AliceConstants.h"
#include "cotk/cotk.h"
#include <ncurses.h>
#include <string>
#include <map>

const int UPDATE_FREQ = 10;

/*! The SensorSuite keeps track of Alice's sensors. It intializes them,
 * checks for new map elements and iterates over them. It also has functions for
 * disabling and enabling sensors 
 *
 * Note on coordinate frames: The Sensors and SensorSuite do all of their
 * computation in Vehicle frame (with +z up, which is against convention,
 * and origin at the center of rear axle). All points are converted to this 
 * frame before calculations are done.
 */
class SensorSuite {

public:

  /// Constructor
  SensorSuite();
  
  /// Destructor
  ~SensorSuite();

  /// Initializes all the necessary skynet threads with a given skynet key
  void initialize(int skynetKey, int recvSubgroup, int sendSubgroup, int debug=0);

  /// Initialize noise parameters from command line or config file
  void initSensorParams(gengetopt_args_info* cmdline);

  /****** SIM LOOP FUNCTIONS *******/

  /// Listens for new mapelements and sends them to appropriate sensors to be
  /// processesd and passed on to the mapper
  void updateElems();

  /**** OTHER FUNCTIONS *****/

  /// Disables a given sensor. If a sensor is disabled, it no longer forwards 
  /// MapElements that it senses. This function also sends out error messages
  /// to the HealthMonitor
  /// NOTE: you can also give the parameter "ALLLADARS" or "ALLSTEREO" for
  /// sensorName in order to disable a group of sensors
  void disableSensor(std::string sensorName);

  /// Enables a given sensor. If a sensor is disabled, it no longer forwards 
  /// MapElements that it senses. This function also sends out messages
  /// to the HealthMonitor
  /// NOTE: you can also give the parameter "ALLLADARS" or "ALLSTEREO" for
  /// sensorName in order to enable a group of sensors
  void enableSensor(std::string sensorName);

  /// Set the range for a given sensor
  /// \param range The range, in meters
  /// \param name the name of the sensor
  void setRange(double range, std::string sensorname);


  /***** COTK DISPLAY *****/

  /// Initialize console display
  int initConsole();

  /// Finalize console display
  int finiConsole();

  /// Updates the console display. returns false if quit button is pressed,
  /// true otherwise
  bool updateConsole();

  /// Console text display
  cotk_t *console;

  /******* PARAMETERS **********/

  /// whether or not to add noise to incoming elements
  bool addNoise;

  ///whether or not to create uncertain geometries for prob-planning
  bool prob_planning;

  /// Whether or not to send groundstrikes
  bool enableGroundstrikes;

private:

  /// cotk function
  //  void sigintHandler(int);

  /// Console button callback
  static int onUserQuit(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserStartAll(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserStopAll(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserKillAll(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserLogAll(cotk_t *console, SensorSuite *self, const char *token);
  //static int onUserViewVisualizers(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserViewLoggers(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserStartModule(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserKillModule(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserStopModule(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserLogModule(cotk_t *console, SensorSuite *self, const char *token);
  static int onUserViewModule(cotk_t *console, SensorSuite *self, const char *token);

  /// Gets the index associated with a given token i.e. %START12% returns 12
  static int getTokenIndex(const char *token);

  /// Update window status on the screen
  void updateWindowStatus(int index, bool open);

  /// Generates an array of groundstrikes of various lengths and geometries
  void generateGroundstrikes();

  /// Keep track of the current alice state
  MapElement alice;

  /// All of the mapelements we're keeping track of
  /// Keep the map id separate so we can search for objects better
  //std::map<MapId, MapElemWrapper> elems;

  /// All of the sensors in the current suite (ladars)
  /// First is the name of the sensor (mostly for debugging purposes),
  /// Second is the actual Sensor
  std::map<std::string, Ladar> ladars;

  /// All of the sensors in the current suite (stereo cameras)
  /// First is the name of the sensor (mostly for debugging purposes),
  /// Second is the actual Sensor
  std::map<std::string, StereoCam> cameras;


  /// skynet key
  int skynetKey;
  /// Subgroup for receiving mapelements
  int recvSubgroup;
  /// Subgroup for sending mapelements
  int sendSubgroup;

  /// Map element talker
  CMapElementTalker maptalker;

  /// Groundstrikes
  map<int, vector<point2> > groundstrikes;
  /// min length for groundstrikes
  int minGroundstrike;
  /// max length for groundstrikes
  int maxGroundstrike;
  /// the current groundstrike
  MapElement gstrike;
  /// the current groundstrikes
  vector<int> gstrikeIndices;

  // how many loops we've run
  int loopnum;
  // whether or not to update the sensor ranges
  bool updateranges;

  /// the level of debug message
  int debug;
};

#endif
