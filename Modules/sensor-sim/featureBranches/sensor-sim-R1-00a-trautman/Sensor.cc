#include "Sensor.hh"
#include <math.h>

Sensor::Sensor()
{
  // set not working until the sensor is initialized
  isWorking = false;
  debugCount = 0;
}

Sensor::~Sensor()
{
}

void Sensor::initialize(point2 position, double range, double angleBegin, double angleEnd, int groupID, int skynetKey, int sendSubgroup, bool debug)
{
  isWorking = true;

  this->position = position;
  this->range = range;
  this->angleBegin = angleBegin;
  this->angleEnd = angleEnd;
  this->groupID = groupID;
  this->skynetKey = skynetKey;
  this->sendSubgroup = sendSubgroup;
  this->debug = debug;

  maptalker.initSendMapElement(skynetKey);

  // use the above info to form a polygon that outlines the sensor's range

  point2 circlept;
  // angle increments are 2pi/16
  int circlenumpts = 8/M_PI * (angleEnd - angleBegin);
  if (circlenumpts < 0)
    circlenumpts *= -1;
  double a = angleBegin;
  // set the origin point
  sensorBounds.push_back(position);
  // set the points on the arc
  for (int i=0; i <= circlenumpts; i++){
    a = angleBegin + i*M_PI/8;
    circlept.set(range*cos(a),range*sin(a));
    sensorBounds.push_back(position+circlept);
  }
  sensorBounds.push_back(position);
  
  if (debug) {
    cout<<"% initializing "<<groupID<<endl;
    cout<<"A = [";
    for (vector<point2>::iterator it = sensorBounds.begin();
	 it != sensorBounds.end(); it++) {
      cout<<it->x<<" "<<it->y<<";"<<endl;
    }
    cout<<"]"<<endl;
  }
}


void Sensor::simulate(MapElement elem)
{

  currAlicePos = point2(elem.state.localX,-elem.state.localY);
  currAliceYaw = elem.state.localYaw;

  // every 10 iterations, update the sensors boundaries
  debugCount++;
  if (debugCount == 10) {
    debugCount = 0;
    currSensorBounds.clear();
    for (vector<point2>::iterator it = sensorBounds.begin();
	 it != sensorBounds.end(); it++) {
      // rotate and translate points
      point2 oldpt = *it;
      point2 newpt;
      newpt.x = oldpt.x*cos(currAliceYaw) - oldpt.y*sin(currAliceYaw) + currAlicePos.x;
      newpt.y = oldpt.x*sin(currAliceYaw) + oldpt.y*cos(currAliceYaw) - currAlicePos.y;
      currSensorBounds.push_back(newpt);
    }
    if (debug) {
      MapElement tempEl;
      tempEl.setTypePolytope();
      tempEl.setId(groupID+123);
      tempEl.setGeometry(currSensorBounds);
      maptalker.sendMapElement(&tempEl,-2);
    }
  }

  if (debug) cout<<"Element is of type "<<elem.type<<endl;

  // check for "clear" elements
  if (elem.type == ELEMENT_CLEAR) {
    if (debug) cout<<"clear element message from trafsim"<<endl;

    // clear from our list of tracked objects
    for (vector<MapId>::iterator it = currObsIDs.begin();
	 it != currObsIDs.end(); it++) {
      if (*it == elem.id) {
	currObsIDs.erase(it);
	if (debug) cout<<"Removing "<<elem.id<<" from list"<<endl;
	break; 
      }
    }
        
    // then just pass it on to the mapper
    appendID(elem);
    maptalker.sendMapElement(&elem,sendSubgroup);
  }

  // check to see if the element is the correct type for the sensor
  if (isCorrectObjType(elem.type)) {
    if (debug) cout<<"Element is the correct type"<<endl;

    // see if this element is already being tracked
    vector<MapId>::iterator IDiterator = currObsIDs.begin();
    while (IDiterator != currObsIDs.end()) {
      if (*IDiterator == elem.id)
	break;
      IDiterator++;
    }

    // ROAD LINES: we probably won't see the center point of
    // these objects, but we may see parts of them, so need to
    // check all of the points.
    if (elem.type ==  ELEMENT_LINE || elem.type == ELEMENT_STOPLINE ||
	elem.type == ELEMENT_LANELINE || elem.type == ELEMENT_PARKING_SPOT ||
	elem.type == ELEMENT_PERIMETER) {

      // check if any points are in range
      point2arr tempGeometry;
      for (vector<point2_uncertain>::iterator it = elem.geometry.arr.begin();
	   it != elem.geometry.arr.end(); it++) {

	if (isInRange(*it))
	  tempGeometry.push_back(*it);
      }
      
      // some points are in range
      if (tempGeometry.size() != 0) {
	if (debug) cout<<"Lane line sensed"<<endl;
	
	// add the element id to our list of IDs if it isn't already there
	if (IDiterator == currObsIDs.end()) {
	  currObsIDs.push_back(elem.id);
	  if (debug) cout<<"Adding "<<elem.id<<" to list"<<endl;
	}

	// add noise and send the line
	elem.setGeometry(tempGeometry);
	elem.setColor(MAP_COLOR_GREY);
	addNoise(elem);
	appendID(elem);
	maptalker.sendMapElement(&elem,sendSubgroup);
	
      }

      // if the element was in range on last iteration, but
      // isn't anymore, then we need to send a "clear" message
      // to the mapper
      else if (IDiterator != currObsIDs.end()) {
	currObsIDs.erase(IDiterator);
	elem.setTypeClear();
	appendID(elem);
	maptalker.sendMapElement(&elem,sendSubgroup);
	if (debug) cout<<"Removing "<<elem.id<<" from list"<<endl;
      }

      // the element isn't in range and we aren't already tracking it
      return;
    }

    // ALL OTHER OBJECTS:
    // check to see if the element is in range
    else if (isInRange(elem.position)) {
      if (debug) cout<<"is in range"<<endl;
      
      if (debug) cout<<"IS SENSED"<<endl;

      // add the element id to our list of IDs if it isn't already there
      if (IDiterator == currObsIDs.end()) {
	currObsIDs.push_back(elem.id);
	if (debug) cout<<"Adding "<<elem.id<<" to list"<<endl;
      }

      // add noise to the element
      addNoise(elem);

      // send the element to the mapper: 
      // first append the group ID to the front of the elem's ID
      appendID(elem);

      // now send element
      maptalker.sendMapElement(&elem,sendSubgroup);

    }

    // not in range, check if we need to send "clear" message
    else if (IDiterator != currObsIDs.end()) {
      currObsIDs.erase(IDiterator);
      elem.setTypeClear();
      appendID(elem);
      maptalker.sendMapElement(&elem,sendSubgroup);
      if (debug) cout<<"Removing "<<elem.id<<" from list"<<endl;
    }
  }
}

bool Sensor::isInRange(point2 pt)
{

  /*
  // rotate and translate point into vehicle frame
  point2 oldpt = pt;
  pt.x = oldpt.x*cos(-currAliceYaw) - oldpt.y*sin(-currAliceYaw) - currAlicePos.x;
  pt.y = oldpt.x*sin(-currAliceYaw) + oldpt.y*cos(-currAliceYaw) + currAlicePos.y;
  */
  if (debug) cout<<"checking if point in range: pt: "<<pt.x<<","<<pt.y<<endl;

  int counter = 0;
  int N = currSensorBounds.size();
  double xintersection;
  point2 p1, p2;

  p1 = currSensorBounds.front();
  for (int i = 1; i <= N; i++) {
    p2 = currSensorBounds.at(i % N);
    if (pt.y > min(p1.y,p2.y)) {
      if (pt.y <= max(p1.y,p2.y)) {
        if (pt.x <= max(p1.x,p2.x)) {
          if (p1.y != p2.y) {
            xintersection = (pt.y-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)+p1.x;
            if (p1.x == p2.x || pt.x <= xintersection)
              counter++;
          }
        }
      }
    }
    p1 = p2;
  }

  if (counter % 2 == 0)
    return false;
  else
    return true;

}

void Sensor::appendID(MapElement& elem)
{
  vector<int> newID;
  newID.push_back(groupID);
  newID.insert(newID.end(),elem.id.dat.begin(),elem.id.dat.end());
  elem.setId(newID);
}
