#ifndef __SENSOR_HH__
#define __SENSOR_HH__

#include "frames/point2_uncertain.hh"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
//#include "MapElemWrapper.hh"
#include <string>

/*! An abstract class*/
class Sensor {

public:

  /****** CONSTRUCTORS ******/

  /// The default constructor does nothing
  Sensor();
  
  /// Does nothing
  virtual ~Sensor();

  /// Initializes the sensor
  /// \param position the position of hte sensor, in vehicle frame (origin at alice back bumper, +z up)
  /// \param range The range of the sensor, in meters, from alice's front bumper
  /// \param angleBegin the beginning of the sensor's range, in radians
  /// \param angleEnd the end of the sensor's range, in radians
  /// \param groupIDStr the ID of the sensor, as defined in sn_types.h (for example MODladarFeederRiegl)
  virtual void initialize(point2 position, 
			  double range, 
			  double angleBegin, 
			  double angleEnd, 
			  int groupID,
			  int skynetKey,
			  int sendSubgroup,
			  bool debug);

  /***** FUNCTIONS ****/

  /// This is the primary Sensor function. It takes in a mapelement and,
  /// if it is within the sensor range, adds noise to it and sends it
  /// on to the mapper
  virtual void simulate(MapElement elem);

  /***** PUBLIC MEMBERS *****/

  /// Whether or not the sensor is currently working
  /// If a sensor is not working, then it will not forward any mapelements
  bool isWorking;

  /// The sensor's mapelementtalker group ID. this is used to distinguish
  /// between different sensors, so for example if a map element is sent
  /// several times there can be several instances of it
  /// It is unique to the sensor and defined in interfaces/sn_types.h
  int groupID;

protected:

  /// Checks to see if the MapElement is the correct object type for the 
  /// sensor. For example, road lines can be seen by stereocameras but
  /// not by the ladars
  virtual bool isCorrectObjType(MapElementType type) = 0;

  /// \return true of the point is within the sensor's range
  /// \return false otherwise
  virtual bool isInRange(point2 pt);

  /// returns the max of two numbers
  double max(double x, double y) { return (x > y ? x : y); }
  
  /// returns the min of two numbers
  double min(double x, double y) { return (x < y ? x : y); }

  /// Adds noise to all the elements the sensor is keeping track of
  /// The noise characteristics are sensor-specific, but may include
  /// gaussian noise in the position vector, drop-outs, modifications to
  /// the geometry, etc.
  /// If sensor is "down", it will just set the elements to "invisible",
  /// meaning that they will not be sent on to the map.
  virtual void addNoise(MapElement& elem) = 0;

  /// Appends the map element's ID with this sensor's group ID
  /// (puts the group ID as the first number in the array)
  void appendID(MapElement& elem);

  /// Position relative to center of Alice's rear axle (in vehicle frame)
  point2 position;
  /// The range of the sensor, in meters
  double range;
  /// The angle of the sensor, in radians, in vehicle frame 
  double angleBegin;
  /// The angle of the sensor, in radians, in vehicle frame 
  double angleEnd;

  /// The boundary defining the sensor's coverage
  /// This is in vehicle frame, so the points must be converted
  /// to local before doing any calculations
  std::vector<point2> sensorBounds;

  /// The current sensor bounds, in abs local coords
  std::vector<point2> currSensorBounds;

  /// The current alice position
  point2 currAlicePos;
  /// The current alice orientation
  double currAliceYaw;

  /// skynet key
  int skynetKey;
  /// Which subgroup to send map elements on
  int sendSubgroup;
  /// Map element talker for sending map elements
  CMapElementTalker maptalker;

  /// List of element IDs that a sensor is currently tracking.
  /// This is used when an object moves out of range, so that
  /// we can send a "clear" message to mapper
  vector<MapId> currObsIDs;

  /// Whether or not debugging messages are turned on
  bool debug;
  
  /// temporary int for debugging
  int debugCount;
};

#endif
