#ifndef __SENSORSUITE_HH__
#define __SENSORSUITE_HH__

#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "Sensor.hh"
#include "Ladar.hh"
#include "StereoCam.hh"
#include "alice/AliceConstants.h"
#include <string>
#include <map>

/*! The SensorSuite keeps track of Alice's sensors. It intializes them,
 * checks for new map elements and iterates over them. It also has functions for
 * disabling and enabling sensors 
 *
 * Note on coordinate frames: The Sensors and SensorSuite do all of their
 * computation in Vehicle frame (with +z up, which is against convention,
 * and origin at the center of rear axle). All points are converted to this 
 * frame before calculations are done.
 */
class SensorSuite {

public:

  /// Constructor
  SensorSuite();
  
  /// Destructor
  ~SensorSuite();

  /// Initializes all the necessary skynet threads with a given skynet key
  void initialize(int skynetKey, int recvSubgroup, int sendSubgroup, int debug=0);

  /****** SIM LOOP FUNCTIONS *******/

  /// Listens for new mapelements and sends them to appropriate sensors to be
  /// processesd and passed on to the mapper
  void updateElems();

  /**** OTHER FUNCTIONS *****/

  /// Disables a given sensor. If a sensor is disabled, it no longer forwards 
  /// MapElements that it senses. This function also sends out error messages
  /// to the HealthMonitor
  /// NOTE: you can also give the parameter "ALLLADARS" or "ALLSTEREO" for
  /// sensorName in order to disable a group of sensors
  void disableSensor(std::string sensorName);

  /// Enables a given sensor. If a sensor is disabled, it no longer forwards 
  /// MapElements that it senses. This function also sends out messages
  /// to the HealthMonitor
  /// NOTE: you can also give the parameter "ALLLADARS" or "ALLSTEREO" for
  /// sensorName in order to enable a group of sensors
  void enableSensor(std::string sensorName);

  /// Set the range for a given sensor
  /// \param range The range, in meters
  /// \param name the name of the sensor
  void setRange(double range, std::string sensorname);
  
private:

  /// Keep track of the current alice state
  MapElement alice;

  /// All of the mapelements we're keeping track of
  /// Keep the map id separate so we can search for objects better
  //std::map<MapId, MapElemWrapper> elems;

  /// All of the sensors in the current suite (ladars)
  /// First is the name of the sensor (mostly for debugging purposes),
  /// Second is the actual Sensor
  std::map<std::string, Ladar> ladars;

  /// All of the sensors in the current suite (stereo cameras)
  /// First is the name of the sensor (mostly for debugging purposes),
  /// Second is the actual Sensor
  std::map<std::string, StereoCam> cameras;


  /// skynet key
  int skynetKey;
  /// Subgroup for receiving mapelements
  int recvSubgroup;
  /// Subgroup for sending mapelements
  int sendSubgroup;

  /// Map element talker
  CMapElementTalker maptalker;

};

#endif
