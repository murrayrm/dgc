#include "Ladar.hh"
#include <math.h>
#include <time.h>

Ladar::Ladar()
{

  minCarGhostFreq = 50;
  rangeCarGhostFreq = 40;
  carGhostFrequency = minCarGhostFreq + rangeCarGhostFreq/2;

  minCarObsGhostFreq = 10;
  rangeCarObsGhostFreq = 30;
  carObsGhostFrequency = minCarObsGhostFreq + rangeCarObsGhostFreq/2;

  obsGhostFrequency = 60;
  minObsGhostFreq = 40;
  rangeObsGhostFreq = minObsGhostFreq + rangeObsGhostFreq/2;

  minDropoutFreq = 20;
  rangeDropoutFreq = 200;
  dropoutFrequency = minDropoutFreq + rangeDropoutFreq/2;

  minVelNoise = .05;
  rangeVelNoise = .5;
  velocityNoise = minVelNoise + rangeVelNoise/2;

  loopcounter = 0;

  // seed the rand function
  srand((unsigned)time(0)); 

}

Ladar::~Ladar()
{
}

bool Ladar::isCorrectObjType(MapElementType type)
{
  // only obstacles can be seen by a ladar                                                                                                                  
  if (type == ELEMENT_OBSTACLE ||
      type == ELEMENT_OBSTACLE_EDGE ||
      type == ELEMENT_VEHICLE ||
      type == ELEMENT_POLY ||
      type == ELEMENT_OBSTACLE_LADAR) {
    return true;
  }
  else
    return false;
}

bool Ladar::addNoise(MapElement& elem)
{
  //  cout<<"Ladar::addNoise"<<endl;

  loopcounter++;
  if (loopcounter == 1500)
    loopcounter = 0;

  if (loopcounter % dropoutFrequency == 0) {
    // get a new dropout frequency
    dropoutFrequency = minDropoutFreq + int(rangeDropoutFreq*rand()/(RAND_MAX + 1.0)); 
    //    cout<<"dropout freq: "<<dropoutFrequency<<endl;
    return false;
  }

  // get new velocity noise
  velocityNoise = minVelNoise + rangeVelNoise*rand()/(RAND_MAX + 1.0); 
  //  cout<<"velocity noise: "<<velocityNoise<<endl;

  // ladarCarPerceptor
  if (groupID == MODladarCarPerceptorLeft ||
      groupID == MODladarCarPerceptorCenter ||
      groupID == MODladarCarPerceptorRight ||
      groupID == MODladarCarPerceptorRear ||
      groupID == MODladarCarPerceptorRoofLeft ||
      groupID == MODladarCarPerceptorRoofRight ||
      groupID == MODladarCarPerceptorRiegl ) {
      
    double elemVelocity = sqrt(pow(elem.velocity.x,2)+pow(elem.velocity.y,2));

    // check if the element is moving. if so, it's probably a vehicle.
    if (elemVelocity > 0) {

      // if it is moving too fast, however, then perceptor will lose track
      // of it and we send it as obstacle
      if (elemVelocity	< THRESHOLD_VELOCITY - velocityNoise) {

	elem.setTypeVehicle();
	elem.height=2;
	elem.plotColor = MAP_COLOR_GREY;
	
	// every once in a while, change the id to simulate losing track of vehicle
	if (loopcounter % carGhostFrequency == 0) {
	  // make a ghost
	  elem.setId(groupID,numElements); 
	  incrementElems();
	  // get a new random number
	  carGhostFrequency = minCarGhostFreq + int(rangeCarGhostFreq*rand()/(RAND_MAX + 1.0)); 
	  //	  cout<<"car ghost freq "<<carGhostFrequency<<endl;
	}
	
	/*
	// change the orientation to be based on velocity
	// TODO: fix this so it actually works
	cout<<"changing orientation... maybe"<<endl;
	double oldOrientation = elem.orientation;
	if (elem.velocity.x == 0)
	elem.orientation = M_PI/2;
	else 
	elem.orientation = atan2(elem.velocity.y, elem.velocity.x);
	cout<<"old orientation "<<oldOrientation<<endl;
	cout<<"new orientation "<<elem.orientation<<endl;
	if (fabs(elem.orientation-oldOrientation)>.5) {
	cout<<" REALLY changing orientation"<<endl;

	vector<point2> newBoundary;
	// put in the points at origin
	if (elem.width > elem.length) {
	double temp = elem.width;
	elem.width = elem.length;
	elem.length = temp;
	cout<<"**************** SWITcHING***"<<endl;
	}
	newBoundary.push_back(point2(.5*elem.width,.5*elem.length));
	newBoundary.push_back(point2(-.5*elem.width,.5*elem.length));
	newBoundary.push_back(point2(-.5*elem.width,-.5*elem.length));
	newBoundary.push_back(point2(.5*elem.width,-.5*elem.length));

	// rotate points and translate
	double ca = cos(elem.orientation);
	double sa = sin(elem.orientation);
	double oldY, oldX;
	for (std::vector<point2>::iterator iter = newBoundary.begin();
	iter != newBoundary.end(); iter++) {
	oldX = iter->x;
	oldY = iter->y;
	iter->x = oldX*ca - oldY*sa +elem.center.x;
	iter->y = oldX*sa + oldY*ca +elem.center.y;
	}
	elem.setGeometry(newBoundary);
	}
	*/
	return true;
    }
      
      // break it apart into obstacles
      else {

	//	  cout<<"break into obstacles"<<endl;

	// get the visible edge of the car
	vector<point2_uncertain> newBoundary =  getEdge(elem.geometry);
	
	// add some noise - make line wavy
	// TODO: fill this in possibly

	// grow the edge 
	growEdge(newBoundary);
	  
	// store it in the ghost array
	elem.setGeometry(newBoundary);
	elem.setTypeLadarObstacle();
	elem.plotColor = MAP_COLOR_PINK;

	// leave ghosts far more often for this one
	if (loopcounter % carObsGhostFrequency == 0) {
	  // make a ghost
	  elem.setId(groupID,numElements);
	  incrementElems();
	  // get a new random number
	  obsGhostFrequency = minCarObsGhostFreq + int(rangeCarObsGhostFreq*rand()/(RAND_MAX + 1.0)); 
	  //	  cout<<"car obs ghost freq "<<carObsGhostFrequency<<endl;
	}
	
	return true;
      }
     
    }
    
    // obstacle
    else {

      //      cout<<"obstacle"<<endl;

      // get the visible edge of the obstacle
      vector<point2_uncertain> newBoundary = getEdge(elem.geometry);
      
      // add some noise - make line wavy
      // TODO: fill this in possibly

      // grow the edge 
      growEdge(newBoundary);

      // modify type and color and store boundary
      elem.setGeometry(newBoundary);
      elem.setTypeLadarObstacle();
      elem.plotColor = MAP_COLOR_PINK;

      if (loopcounter % obsGhostFrequency == 0) {
	// make a ghost
	elem.setId(groupID,numElements);
	incrementElems();
	// get a new random number
	obsGhostFrequency = minObsGhostFreq + int(rangeObsGhostFreq*rand()/(RAND_MAX + 1.0)); 
	//	cout<<"obs ghost freq "<<obsGhostFrequency<<endl;
      }
      
      return true;
    }
    // shouldn't ever get here
    return false;
  } // end ladar car perceptors

    // ladarRoadPerceptor
  else if (groupID == MODladarRoadPerceptor) {
    // TODO: fill this in if needed
    return true;
  }

  return false;

}


vector<point2_uncertain> Ladar::getEdge(point2arr_uncertain& array)
{
  //  cout<<"Ladar::getEdge"<<endl;

  // iterate through the array n/2 times, eliminating the point furthest
  // away each time

  double maxDist = 0;
  double tempDist;
  vector<point2_uncertain>::iterator maxPt; 
  vector<point2_uncertain> newBoundary = array.arr;

  for (int i = 0; i < array.size()/2-1; i++) {

    maxDist = 0;

    for (vector<point2_uncertain>::iterator it = newBoundary.begin();
	 it != newBoundary.end(); it++) {

      tempDist = it->dist(currAlicePos);
      if (tempDist > maxDist) {
	maxDist = tempDist;
	maxPt = it;
      }
    }

    // delete point furthest away
    newBoundary.erase(maxPt);

  } 

  return newBoundary;

}

void Ladar::makeNoisyEdge(vector<point2_uncertain>& arr)
{

}

void Ladar::growEdge(vector<point2_uncertain>& arr)
{

  //note: not exactly correct but whatever
  double slope;
  double dx, dy;
  int size = arr.size();
  // note: going backwards so we create a polygon properly
  vector<point2_uncertain>::iterator it = arr.end();
  it--; // move to last elem in array
      
  for (int i = 0; i < size-1; i++) {

    if ((it->x - currAlicePos.x) != 0) {

      slope = (it->y - currAlicePos.y) / (it->x - currAlicePos.x);

      dx = sqrt((OBJECT_DEPTH*OBJECT_DEPTH)/(slope*slope + 1));
      dy = slope*dx;
      arr.push_back(point2(it->x + dx, it->y + dy));
    }
    it--;    
  }

}
