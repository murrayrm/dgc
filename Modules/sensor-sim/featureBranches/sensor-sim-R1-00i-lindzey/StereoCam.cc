#include "StereoCam.hh"
#include <cassert>

StereoCam::StereoCam()
{
}

StereoCam::~StereoCam()
{
}

void StereoCam::initParams(gengetopt_args_info* cmdline)
{

}

bool StereoCam::isCorrectObjType(MapElementType type)
{

  // pretty much anything can be seen by a stereocamera,
  // but the perceptors on long and medium do not
  // pick up road lines

  // check if it's a road line
  if (type < 11) {

    // line perceptors
    if (groupID == MODlinePerceptorLFShort ||
	groupID == MODlinePerceptorRFShort ||
	groupID == MODlinePerceptorMFMedium) {
      return true;
      //cout<<"forwarding lane line on "<<name<<endl;
    }
    
    // obs perceptors
    else 
      return false;
  }

  // all other elements:

  // obs perceptors
  if (groupID == MODstereoObsPerceptorMedium ||
      groupID == MODstereoObsPerceptorLong)
    return true;

  // line percetpors
  return false;

}

void StereoCam::simulate(MapElement elem, bool addNoiseToElem)
{
  // ROAD LINES
  if (elem.type < 11) {

    vector<point2> newBoundary = isInRange(elem.geometry,false);

    if (newBoundary.size() != 0) {

      if (debug==3) cout<<"Lane line sensed"<<endl;

      // add to our list if it isn't already there
      map<MapId,MapElement>::iterator elemiter = addElem(elem);

      // from here on, refer only to element id

      // add noise
      bool sendElement = true; // whether or not to send the element on
      if (addNoiseToElem) {
	// TODO: fill this in
      }

      elemiter->second.setGeometry(newBoundary);
      if (sendElement)
	  maptalker.sendMapElement(&(elemiter->second),sendSubgroup);	
    
      else  // we're dropping the message
	  deleteElem(elemiter->first);

    }

    else // object not in sensor range
      deleteElem(elem.id);

    // done with line objects
    return;
  }

  // ALL OTHER OBJECTS:
  vector<point2> newBoundary = isInRange(elem.geometry,addNoiseToElem);

  if (newBoundary.size() != 0) {
      
    if (debug==3) cout<<"is in range"<<endl;
    if (debug==3) cout<<"IS SENSED"<<endl;

    // add to our list if it isn't already there
    map<MapId,MapElement>::iterator elemiter = addElem(elem);
      
    // from here on, refer only to element id

    // add noise and send the objec
    bool sendElement = true; // whether or not to send the element on
    if (addNoiseToElem) {
      // TODO: fill this in

      // make into a blob
      
      // depending on dist from alice, elongate blob
      
      // add position noise, depending on dist from alice
      
      // add some random dropouts, depending on location in sensor range
      
      elem.setTypeStereoObstacle();
    }
    
    if (sendElement)
      maptalker.sendMapElement(&(elemiter->second),sendSubgroup);    
    else  // we're dropping the message
	deleteElem(elemiter->first);

  }

  else // not in sensor range
    deleteElem(elem.id);
}


