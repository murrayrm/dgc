/*! Spoof.hh
 * Pete Trautman
 * April 18 2007
 *
 * Modified by Jessica Austin
 * July 2007
 */

#ifndef SPOOF_HH
#define SPOOF_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_statistics.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
#include <cv.h>
#include <highgui.h>
#include <time.h>
//open cv stuff

#include "dgcutils/DGCutils.hh"

// Other modules/def's
#include "map/Map.hh"
//#include <cotk/cotk.h>
//#include <ncurses.h>

/*! Spoof class
 * This is the main class for spoofing data: the primary function here is to simulate data with high false alarm rates, such as befits our current perceptors.
 * \brief Main class for spoof function  
 */ 
class Spoof
{ 
public:

  //ctor & dtor
  Spoof(int DOtype,double sigma,point2 pos,point2 vel,double badReturnRate);
  
  ~Spoof();

  //get functions
  point2 getTrueDynObsPos();

  point2 getTrueDynObsVel();

  point2 getSenseDynObsPos();

  point2 getSenseDynObsVel();

  point2 getStatObsPos();

  double getdynObsAng();
  
  //real functions
  void dynObs(double deltaT);

  void statObs();
	
private:

  double m_badReturnRate;

  point2 m_noReturn;

  double m_ang;

  point2 m_Noise;

  point2 m_dynObsPos;

  point2 m_dynObsVel;

  point2 m_dynObsPosData;

  point2 m_dynObsVelData;

  point2 m_statObsPos;

  point2 m_statObsPosData;

  double m_sensedSigma;

  int m_DOtype;

  const gsl_rng_type* m_T; //rng seed stuff
   
  gsl_rng* m_r; //rnd seed stuff

};

#endif  // SPOOF_HH

