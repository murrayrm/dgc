have std::map objects with mapelements paired with a struct that stores location, whether or not they're getting sent on, etc

MapElemWrapper
Stores MapElement
stores information about the object, like whether or not they're getting sent on
Can eventually hold multiple map elements that have variations of the original object in order to spoof multiple sensors and ghost objects
Functions for distance to alice, stuff like that

Sensor
has noise characteristics (from Spoof)
is associated with a physical block of space in vehicle frame
takes in an obstacle's absolute state, returns a noisy state
can "remove" an obstacle if the sensor is "down"
//can modify confidence levels based on sensor status and distance away (note: confidence levels will be modified by the trackers, not this spoofing module)
has a list of the mapelements it can "see" (as pointers)

Main class
stores map of objects (map<MapId ID, MapElement>)
queries Sensors to see if they're holding certain MapElements (if no sensors claim a mapelement, then the element is set to invisible)
stores alice's state, from asim
stores array of Sensors
iterates over the Sensors, which iterate over the MapElements 
brings sensors up and down
has functions for sending messages to health monitor and such
has function for placing map elements in a sensor area? possibly break up the area around
alice into a grid to facilitate this

List of different sensors:
Riegl - 90 deg, 65m, off front bumper
SICK - 180 deg, 35m, 1 off front bumper, 2 ea on ea side, 1 on back
Mid-range stereo - 45 deg, 35m, off front
Long-range stereo - 45 deg, 70m, off front



get a mapelement from the talker
see if it is in a given sensor
if it is, the sensor returns true, then runs its addnoise function, then changes the mapid (so it is unique for each sensor) and sends it on
does this for as many sensors as necessary
if all sensors return false, we knwo that that mapelement is out of range (this doesn't matter so much since we're not actually storing them anyway)