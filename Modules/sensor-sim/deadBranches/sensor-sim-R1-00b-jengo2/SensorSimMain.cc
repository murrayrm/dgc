#include "SensorSuite.hh"
#include "cmdline.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

  // get command line opts
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  // get skynet key
  int skynet_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if ( pSkynetkey == NULL || cmdline.skynet_key_given) {
    skynet_key = cmdline.skynet_key_arg;
  } 
  else {
    skynet_key = atoi(pSkynetkey);
  }
  cout << "Constructing skynet with KEY = " << skynet_key << endl;

  int debug = cmdline.debug_arg;
  cout<<"debug level "<<debug<<endl;

  // get the sending and receiving subgroups
  int recvSubgroup = cmdline.recv_subgroup_arg;
  int sendSubgroup = cmdline.send_subgroup_arg;
  cout<<"Receiving on subgroup "<<recvSubgroup<<endl;
  cout<<"Sending on subgroup "<<sendSubgroup<<endl;

  // create new SensorSuite and initialize with the skynet key
  SensorSuite sensors;
  sensors.initialize(skynet_key,recvSubgroup,sendSubgroup,debug);

  // check to see if any of the sensors are disabled
  if (cmdline.disableAllLadar_flag)
    sensors.disableSensor("ALLLADARS");
  if (cmdline.disableAllStereo_flag)
    sensors.disableSensor("ALLSTEREO");
  if (cmdline.MODladarCarPerceptorRiegl_flag)
    sensors.disableSensor("MODladarCarPerceptorRiegl");
  if (cmdline.MODladarCarPerceptorLeft_flag)
    sensors.disableSensor("MODladarCarPerceptorLeft");
  if (cmdline.MODladarCarPerceptorRight_flag)
    sensors.disableSensor("MODladarCarPerceptorRight");
  if (cmdline.MODladarCarPerceptorRoofLeft_flag)
    sensors.disableSensor("MODladarCarPerceptorRoofLeft");
  if (cmdline.MODladarCarPerceptorRoofRight_flag)
    sensors.disableSensor("MODladarCarPerceptorRoofRight");
  if (cmdline.MODladarCarPerceptorCenter_flag)
    sensors.disableSensor("MODladarCarPerceptorCenter");
  if (cmdline.MODstereoObsPerceptorMedium_flag)
    sensors.disableSensor("MODstereoObsPerceptorMedium");
  if (cmdline.MODlinePerceptorMFMedium_flag)
    sensors.disableSensor("MODlinePerceptorMFMedium");
  if (cmdline.MODstereoObsPerceptorLong_flag)
    sensors.disableSensor("MODstereoObsPerceptorLong");
  if (cmdline.MODstereoFeederLFShort_flag)
    sensors.disableSensor("MODstereoFeederLFShort");
  if (cmdline.MODstereoFeederRFShort_flag)
    sensors.disableSensor("MODstereoFeederRFShort");

  // modify ranges if necessary
  if (cmdline.range_linePercepMF_given)
    sensors.setRange(cmdline.range_linePercepMF_arg,"MODlinePerceptorMFMedium");

  // run the simulation loop
  while (1) {
    sensors.updateElems();
  }

  return 0;
}
