#include "Sensor.hh"
#include <math.h>

Sensor::Sensor()
{
  // set not working until the sensor is initialized
  isWorking = false;
  numElements = 0;
  maxElementsUsed = false;
}

Sensor::~Sensor()
{
  //  cout<<"Clearing all elements"<<endl;
  // send clear message for all elements we're keeping track of
  for ( map<MapId,MapElement>::iterator it = elements.begin();
	it != elements.end(); it++) {
    deleteElem(it->first);
  }
}

void Sensor::initialize(point2 position, 
			double range, 
			double angleBegin, 
			double angleEnd, 
			int groupID, 
			string name,
			int skynetKey, 
			int sendSubgroup, 
			int debug)
{
  isWorking = true;

  this->position = position;
  this->range = range;
  this->angleBegin = angleBegin;
  this->angleEnd = angleEnd;
  this->groupID = groupID;
  this->name = name;
  this->skynetKey = skynetKey;
  this->sendSubgroup = sendSubgroup;
  this->debug = debug;

  maptalker.initSendMapElement(skynetKey);

  // use the above info to form a polygon that outlines the sensor's range
  calcCoverage();
  
  if (debug==3) {
    cout<<"% initializing "<<name<<" ("<<groupID<<")"<<endl;
    cout<<"A = [";
    for (vector<point2>::iterator it = sensorBounds.begin();
	 it != sensorBounds.end(); it++) {
      cout<<it->x<<" "<<it->y<<";"<<endl;
    }
    cout<<"]"<<endl;
  }
}

void Sensor::calcCoverage()
{
  sensorBounds.clear();

  point2 circlept;
  // angle increments are 2pi/16
  int circlenumpts = 8/M_PI * (angleEnd - angleBegin);
  if (circlenumpts < 0)
    circlenumpts *= -1;
  double a = angleBegin;
  // set the origin point
  sensorBounds.push_back(position);
  // set the points on the arc
  for (int i=0; i <= circlenumpts; i++){
    a = angleBegin + i*M_PI/8;
    circlept.set(range*cos(a),-range*sin(a));
    sensorBounds.push_back(position+circlept);
  }
  sensorBounds.push_back(position);

}

/// Calculates the new sensor range based on alice's position
void Sensor::updateCoverage(point2 alicePos, double aliceYaw)
{

  //  cout<<"Sensor::updateCoverage"<<endl;

  currAlicePos = alicePos;
  currAliceYaw = aliceYaw;

  //  update the sensors boundaries
  currSensorBounds.clear();
  double ca = cos(currAliceYaw);
  double sa = sin(currAliceYaw);
  for (vector<point2>::iterator it = sensorBounds.begin();
       it != sensorBounds.end(); it++) {
    // rotate and translate points
    point2 oldpt = *it;
    point2 newpt;
    newpt.x = oldpt.x*ca - oldpt.y*sa + currAlicePos.x;
    newpt.y = oldpt.x*sa + oldpt.y*ca - currAlicePos.y;
    currSensorBounds.push_back(newpt);
  }

  if (debug==1) { // send outline of sensor ranges to mapviewer -- VERY intensive
    MapElement tempEl;
    tempEl.setTypePolytope();
    tempEl.setId(groupID+123);
    tempEl.setGeometry(currSensorBounds);
    maptalker.sendMapElement(&tempEl,-2);
  }
  
}

vector<point2> Sensor::isInRange(point2arr boundary, bool returnEdge)
{
  vector<point2> newBoundary;
  // get the position of the sensor in local coords (need to convert from vehicle frame to local frame)
  point2 pos;
  pos.x = currAlicePos.x + position.x*cos(currAliceYaw) - position.y*sin(currAliceYaw);
  pos.y = currAlicePos.y + position.x*sin(currAliceYaw) + position.y*cos(currAliceYaw);
  // the min and max sensor angle (defined counterclockwise)
  double minAngle = angleBegin - currAliceYaw;
  double maxAngle = angleEnd - currAliceYaw;
  // make sure all angles are between -pi and pi
  while (minAngle > M_PI)
    minAngle -= 2*M_PI;
  while (minAngle < -M_PI)
    minAngle += 2*M_PI;
  while (maxAngle > M_PI)
    maxAngle -= 2*M_PI;
  while (maxAngle < -M_PI)
    maxAngle += 2*M_PI;

  // check each point in object boundary
  for (vector<point2>::iterator it = boundary.arr.begin();
       it != boundary.arr.end(); it++) {

    // check distance
    if (pos.dist(*it) < range) {
     
      // now check angle (remember +z is down)
      double elemAngle = atan2(-1*(it->y-pos.y),it->x-pos.x); 

      if (maxAngle<0 && minAngle>0) { // necessary b/c angles range from -pi->pi, not 0->2pi
	if ((elemAngle>minAngle && elemAngle<M_PI) 
	    || (elemAngle>-M_PI && elemAngle<maxAngle)) {
	  newBoundary.push_back(*it);
	}
      }
      else if (elemAngle>minAngle && elemAngle<maxAngle) {
	newBoundary.push_back(*it);
      }
     
    }

  }

  // grab the edge we can see
  if (returnEdge && newBoundary.size() > 0) {

    double maxDist = 0;
    double tempDist;
    vector<point2>::iterator maxPt; 
    double arraySize = newBoundary.size();

    // erase half the points that are furthest away
    for (int i = 0; i < arraySize/2; i++) {

      maxDist = 0;

      for (vector<point2>::iterator it = newBoundary.begin();
	   it != newBoundary.end(); it++) {
	
	tempDist = it->dist(pos);
	if (tempDist > maxDist) {
	  maxDist = tempDist;
	  maxPt = it;
	}
      }

      // delete point furthest away
      if (i != arraySize/2-1) // don't delete the last max pt.. we'll need it later
	newBoundary.erase(maxPt);
      
    } 

    // points may be in weird order, so reorder them starting with point 
    // furthest away
    if (maxPt == newBoundary.begin() || maxPt == (newBoundary.end()-1)) {
      // first grow the edge
      //      growEdge(newBoundary);
      return newBoundary;
    }

    vector<point2> newNewBoundary;
    for (int i = 0; i < newBoundary.size(); i++) {
      newNewBoundary.push_back(*maxPt);
      maxPt++;
      if (maxPt == newBoundary.end())
	maxPt = newBoundary.begin();
    }
    //    growEdge(newNewBoundary);
    return newNewBoundary;
  }

  return newBoundary;
}

void Sensor::growEdge(vector<point2>& arr)
{

  //note: not exactly correct but whatever
  double slope;
  double dx, dy;
  int size = arr.size();
  // note: going backwards so we create a polygon properly
  vector<point2>::iterator it = arr.end();
  it--; // move to last elem in array
      
  for (int i = 0; i < size-1; i++) {

    if ((it->x - currAlicePos.x) != 0) {

      slope = (it->y - currAlicePos.y) / fabs(it->x - currAlicePos.x);

      dx = sqrt((OBJECT_DEPTH*OBJECT_DEPTH)/(slope*slope + 1));
      dy = slope*dx;
      arr.push_back(point2(it->x + dx, it->y + dy));
    }
    it--;    
  }

}


void Sensor::incrementElems()
{

  // if we've already used this id before, make sure we've sent
  // a clear message to mapper
  if (maxElementsUsed) {
    MapElement tempElem;
    tempElem.setId(groupID,numElements);
    tempElem.setTypeClear();
    maptalker.sendMapElement(&tempElem,sendSubgroup);
    if (debug==2) cout<<groupID<<": clearing element"<<endl;
  }

  numElements++;
  if (debug==2) cout<<groupID<<": numElements: "<<numElements<<endl;

  if (numElements>=MAX_NUM_ELEMENTS) {
    numElements = 0;
    maxElementsUsed = true;
  }
}

map<MapId,MapElement>::iterator Sensor::addElem(MapElement& elem)
{

  //  cout<<"Sensor::addElem"<<endl;

  // add to the map of elements (note that a map will not insert
  // an object if an object with that same key is already there)
  pair< map<MapId,MapElement>::iterator, bool > inserted = 
    elements.insert( make_pair(elem.id,elem) );

  // if the addition was successful, change the element id
  // to conform with perceptor standards
  if (inserted.second) {

    MapElement* storedElem = &(inserted.first->second);
    // append the sensor's id and change the element id 
    // to be unique and in between 0 and MAX_NUM_ELEMENTS
    storedElem->setId(groupID,numElements);
    incrementElems();

    if (debug==2) cout<<"Adding "<<storedElem->id<<" to list"<<endl;
  }
  
  
  // if already in list, update the element
  else {
    MapElement* storedElem = &(inserted.first->second);
    storedElem->setGeometry(elem.geometry);
    storedElem->state = elem.state;
    storedElem->velocity = elem.velocity;
    storedElem->timeStopped = elem.timeStopped;
  }

  return inserted.first;

}

void Sensor::deleteElem(MapId id)
{

  // first check if its in our list of tracked objects
  // if it's there, them remove it and forward message to mapper
  // if it isn't in our list, do nothing

  map<MapId,MapElement>::iterator it = elements.find(id);
  
  if (it != elements.end()) {

    if (debug==2) cout<<"Removing "<<it->second.id<<" from list"<<endl;

    // forward clear message
    it->second.setTypeClear();
    maptalker.sendMapElement(&(it->second),sendSubgroup);

    // delete from our list
    elements.erase(it);

  }

}

void Sensor::setRange(double range)
{
  this->range = range;
  calcCoverage();
}
