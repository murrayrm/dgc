#ifndef __STEREOCAM_HH__
#define __STEREOCAM_HH__

#include "Sensor.hh"

class StereoCam : public Sensor {

public:

  /// Constructor currently does nothing                                                                                                                     
  StereoCam();

  /// Destructor currently does nothing                                                                                                                      
  ~StereoCam();

  /**** FUNCTIONS ***/

  virtual bool isCorrectObjType(MapElementType type);

  virtual void simulate(MapElement elem, bool addNoiseToElem, bool probGeo);

  /// Sets certain parameters from the command line or a config file
  virtual void initParams(gengetopt_args_info* cmdline);

};

#endif
