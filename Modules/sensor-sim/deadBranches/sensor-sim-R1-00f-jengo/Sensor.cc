#include "Sensor.hh"
#include <math.h>

Sensor::Sensor()
{
  // set not working until the sensor is initialized
  isWorking = false;
  numElements = 0;
  maxElementsUsed = false;
}

Sensor::~Sensor()
{
  //  cout<<"Clearing all elements"<<endl;
  // send clear message for all elements we're keeping track of
  for ( map<MapId,MapElement>::iterator it = elements.begin();
	it != elements.end(); it++) {
    deleteElem(it->first);
  }
}

void Sensor::initialize(point2 position, 
			double range, 
			double angleBegin, 
			double angleEnd, 
			int groupID, 
			string name,
			int skynetKey, 
			int sendSubgroup, 
			int debug)
{
  isWorking = true;

  this->position = position;
  this->range = range;
  this->angleBegin = angleBegin;
  this->angleEnd = angleEnd;
  this->groupID = groupID;
  this->name = name;
  this->skynetKey = skynetKey;
  this->sendSubgroup = sendSubgroup;
  this->debug = debug;

  maptalker.initSendMapElement(skynetKey);

  // use the above info to form a polygon that outlines the sensor's range
  calcCoverage();
  
  if (debug) {
    cout<<"% initializing "<<name<<" ("<<groupID<<")"<<endl;
    cout<<"A = [";
    for (vector<point2>::iterator it = sensorBounds.begin();
	 it != sensorBounds.end(); it++) {
      cout<<it->x<<" "<<it->y<<";"<<endl;
    }
    cout<<"]"<<endl;
  }
}


void Sensor::simulate(MapElement elem, bool addNoiseToElem)
{

  // ROAD LINES: we probably won't see the center point of
  // these objects, but we may see parts of them, so need to
  // check all of the points.
  if (elem.type < 11) {

    // check if any points are in range
    point2arr tempGeometry;
    for (vector<point2_uncertain>::iterator it = elem.geometry.arr.begin();
	 it != elem.geometry.arr.end(); it++) {

      if (isInRange(*it))
	tempGeometry.push_back(*it);
    }
      
    // some points are in range
    if (tempGeometry.size() != 0) {
      if (debug==3) cout<<"Lane line sensed"<<endl;

      // add to our list if it isn't already there
      map<MapId,MapElement>::iterator elemiter = addElem(elem);

      // from here on, refer only to element id
      // add noise and send the line
      elemiter->second.setGeometry(tempGeometry);
      if (addNoiseToElem) {
	if (addNoise(elemiter->second)) {
	  maptalker.sendMapElement(&(elemiter->second),sendSubgroup);	
	}
	else { // we're dropping the message
	  deleteElem(elemiter->first);
	}
      }
      else { // just send the element 
	//	  cout<<"not adding noise, just sending"<<endl;
	maptalker.sendMapElement(&(elemiter->second),sendSubgroup);	
      }
    }

    else 
      deleteElem(elem.id);

    // done with line objects
    return;
  }

  // ALL OTHER OBJECTS:
  // check to see if the element is in range
  else if (isInRange(elem.position)) {
      
    if (debug==3) cout<<"is in range"<<endl;
    if (debug==3) cout<<"IS SENSED"<<endl;

    // add to our list if it isn't already there
    map<MapId,MapElement>::iterator elemiter = addElem(elem);
      
    // from here on, refer only to element id
    // add noise and send the object
    if (addNoiseToElem) {
      if (addNoise(elemiter->second))
	maptalker.sendMapElement(&(elemiter->second),sendSubgroup);    
      else { // we're dropping the message
	deleteElem(elemiter->first);
      }
    }
    else { // just send the element
      //	cout<<"not adding noise, just sending"<<endl;
      maptalker.sendMapElement(&(elemiter->second),sendSubgroup);	
    }
  }

  else
    deleteElem(elem.id);

}

void Sensor::calcCoverage()
{
  sensorBounds.clear();

  point2 circlept;
  // angle increments are 2pi/16
  int circlenumpts = 8/M_PI * (angleEnd - angleBegin);
  if (circlenumpts < 0)
    circlenumpts *= -1;
  double a = angleBegin;
  // set the origin point
  sensorBounds.push_back(position);
  // set the points on the arc
  for (int i=0; i <= circlenumpts; i++){
    a = angleBegin + i*M_PI/8;
    circlept.set(range*cos(a),range*sin(a));
    sensorBounds.push_back(position+circlept);
  }
  sensorBounds.push_back(position);

}

/// Calculates the new sensor range based on alice's position
void Sensor::updateCoverage(point2 alicePos, double aliceYaw)
{

  //  cout<<"Sensor::updateCoverage"<<endl;

  currAlicePos = alicePos;
  currAliceYaw = aliceYaw;

  //  update the sensors boundaries
  currSensorBounds.clear();
  double ca = cos(currAliceYaw);
  double sa = sin(currAliceYaw);
  for (vector<point2>::iterator it = sensorBounds.begin();
       it != sensorBounds.end(); it++) {
    // rotate and translate points
    point2 oldpt = *it;
    point2 newpt;
    newpt.x = oldpt.x*ca - oldpt.y*sa + currAlicePos.x;
    newpt.y = oldpt.x*sa + oldpt.y*ca - currAlicePos.y;
    currSensorBounds.push_back(newpt);
  }

  if (debug==1) { // send outline of sensor ranges to mapviewer -- VERY intensive
    MapElement tempEl;
    tempEl.setTypePolytope();
    tempEl.setId(groupID+123);
    tempEl.setGeometry(currSensorBounds);
    maptalker.sendMapElement(&tempEl,-2);
  }
  
}

bool Sensor::isInRange(point2 pt)
{

  if (debug==3) cout<<"checking if point in range: pt: "<<pt.x<<","<<pt.y<<endl;

  // do a simple check for distance first
  currAlicePos.y *= -1;
  if (currAlicePos.dist(pt) > range)
    return false;
  currAlicePos.y *= -1;

  int counter = 0;
  int N = currSensorBounds.size();
  double xintersection;
  point2 p1, p2;

  p1 = currSensorBounds.front();
  for (int i = 1; i <= N; i++) {
    p2 = currSensorBounds.at(i % N);
    if (pt.y > min(p1.y,p2.y)) {
      if (pt.y <= max(p1.y,p2.y)) {
        if (pt.x <= max(p1.x,p2.x)) {
          if (p1.y != p2.y) {
            xintersection = (pt.y-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)+p1.x;
            if (p1.x == p2.x || pt.x <= xintersection)
              counter++;
          }
        }
      }
    }
    p1 = p2;
  }

  if (counter % 2 == 0)
    return false;
  else
    return true;

}

void Sensor::incrementElems()
{

  // if we've already used this id before, make sure we've sent
  // a clear message to mapper
  if (maxElementsUsed) {
    MapElement tempElem;
    tempElem.setId(groupID,numElements);
    tempElem.setTypeClear();
    maptalker.sendMapElement(&tempElem,sendSubgroup);
    cout<<"clearing element"<<endl;
  }

  numElements++;
  cout<<"numElements: "<<numElements<<endl;

  if (numElements>=MAX_NUM_ELEMENTS) {
    numElements = 0;
    maxElementsUsed = true;
  }
}

map<MapId,MapElement>::iterator Sensor::addElem(MapElement& elem)
{

  //  cout<<"Sensor::addElem"<<endl;

  // add to the map of elements (note that a map will not insert
  // an object if an object with that same key is already there)
  pair< map<MapId,MapElement>::iterator, bool > inserted = 
    elements.insert( make_pair(elem.id,elem) );

  // if the addition was successful, change the element id
  // to conform with perceptor standards
  if (inserted.second) {

    MapElement* storedElem = &(inserted.first->second);
    // append the sensor's id and change the element id 
    // to be unique and in between 0 and MAX_NUM_ELEMENTS
    storedElem->setId(groupID,numElements);
    incrementElems();

    if (debug) cout<<"Adding "<<storedElem->id<<" to list"<<endl;
  }
  
  
  // if already in list, update the element
  else {
    MapElement* storedElem = &(inserted.first->second);
    storedElem->setGeometry(elem.geometry);
    storedElem->state = elem.state;
    storedElem->velocity = elem.velocity;
    storedElem->timeStopped = elem.timeStopped;
  }

  return inserted.first;

}

void Sensor::deleteElem(MapId id)
{

  // first check if its in our list of tracked objects
  // if it's there, them remove it and forward message to mapper
  // if it isn't in our list, do nothing

  map<MapId,MapElement>::iterator it = elements.find(id);
  
  if (it != elements.end()) {

    if (debug) cout<<"Removing "<<it->second.id<<" from list"<<endl;

    // forward clear message
    it->second.setTypeClear();
    maptalker.sendMapElement(&(it->second),sendSubgroup);

    // delete from our list
    elements.erase(it);

  }

}

void Sensor::setRange(double range)
{
  this->range = range;
  calcCoverage();
}
