#include "Ladar.hh"

Ladar::Ladar()
{
}

Ladar::~Ladar()
{
}

bool Ladar::isCorrectObjType(MapElementType type)
{
  // only obstacles can be seen by a ladar                                                                                                                  
  if (type == ELEMENT_OBSTACLE ||
      type == ELEMENT_OBSTACLE_EDGE ||
      type == ELEMENT_VEHICLE ||
      type == ELEMENT_POLY) {
    return true;
  }
  else
    return false;
}

void Ladar::addNoise(MapElement& elem)
{
  // TODO: add in noise calculations
}
