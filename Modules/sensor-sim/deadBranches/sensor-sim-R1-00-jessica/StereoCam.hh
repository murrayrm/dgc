#ifndef __STEREOCAM_HH__
#define __STEREOCAM_HH__

#include "Sensor.hh"

class StereoCam : public Sensor {

public:

  /// Constructor currently does nothing                                                                                                                     
  StereoCam();

  /// Destructor currently does nothing                                                                                                                      
  ~StereoCam();

  /**** FUNCTIONS ***/

  virtual bool isCorrectObjType(MapElementType type);

  virtual void addNoise(MapElement& elem);

};

#endif
