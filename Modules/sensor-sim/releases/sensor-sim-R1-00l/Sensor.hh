#ifndef __SENSOR_HH__
#define __SENSOR_HH__

#include "frames/point2_uncertain.hh"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "cmdline.h"
//#include "MapElemWrapper.hh"
#include <string>
#include <map>

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

/// this number comes from the perceptor design--they only keep track
/// of a certain number of elements
const int MAX_NUM_ELEMENTS = 500;

/// How much to "grow" object edges
const double OBJECT_DEPTH = 1;

/*! An abstract class*/
class Sensor {

public:

  /****** CONSTRUCTORS ******/

  /// The default constructor does nothing
  Sensor();
  
  /// Does nothing
  virtual ~Sensor();

  /// Initializes the sensor
  /// \param position the position of hte sensor, in vehicle frame (origin at alice back bumper, +z up)
  /// \param range The range of the sensor, in meters, from alice's front bumper
  /// \param angleBegin the beginning of the sensor's range, in radians
  /// \param angleEnd the end of the sensor's range, in radians
  /// \param groupID the ID of the sensor, as defined in sn_types.h (for example MODladarPerceptorRiegl)
  /// \param name The name of the sensor (should be the same as groupID)
  /// \param skynetKey the skynet key
  /// \param sendSubgroup The subgroup to send out on
  /// \param debug The debug level (0-3)
  virtual void initialize(point2 position, 
			  double range, 
			  double angleBegin, 
			  double angleEnd, 
			  int groupID,
			  string name,
			  int skynetKey,
			  int sendSubgroup,
			  int debug);

  /// Sets certain parameters from the command line or a config file
  virtual void initParams(gengetopt_args_info* cmdline) = 0;

  /***** FUNCTIONS ****/

  /// This is the primary Sensor function. It takes in a mapelement and,
  /// if it is within the sensor range, adds noise to it and sends it
  /// on to the mapper
  virtual void simulate(MapElement elem, bool addNoiseToElem) = 0;

  /// Sets the sensor range
  virtual void setRange(double range);

  /// Calculates the new sensor range based on alice's position
  virtual void updateCoverage(point2 alicePos, double aliceYaw);

  /// Checks to see if the MapElement is the correct object type for the 
  /// sensor. For example, road lines can be seen by stereocameras but
  /// not by the ladars
  virtual bool isCorrectObjType(MapElementType type) = 0;

  /// If a clear element message is sent from trafsim, this function will
  /// delete that element from any sensor lists, and then forward on the clear
  /// message
  virtual void deleteElem(MapId id);

  /***** PUBLIC MEMBERS *****/

  /// Whether or not the sensor is currently working
  /// If a sensor is not working, then it will not forward any mapelements
  bool isWorking;

  /// The sensor's mapelementtalker group ID. this is used to distinguish
  /// between different sensors, so for example if a map element is sent
  /// several times there can be several instances of it
  /// It is unique to the sensor and defined in interfaces/sn_types.h
  int groupID;

  /// The name of the sensor. In string form, it is mostly useful
  /// for output/debugging purposes
  string name;

  /// The current alice position
  point2 currAlicePos;
  /// The current alice orientation
  double currAliceYaw;

protected:

  /// Calculates the sensor coverage
  virtual void calcCoverage();

  /// Checks if an element is in the sensor's range. If it isn't, this function
  /// will return an empty array. If the element is in range and returnEdge is false,
  /// it will simply return the points in the boundary within range. If it is in range
  /// and returnEdge is true, it will get the visible edge of the obstacle, grow it
  /// by a fixed amound, and then return that
  virtual vector<point2> isInRange(point2arr boundary, bool returnEdge);

  /// Grows the visible edge of the car. currently buggy and not used
  void growEdge(vector<point2>& arr);

  /// adds noise to an object's position by modifying each element in
  /// the geometry array
  void addPosNoise(vector<point2>& arr);

  /// Increments the internal count of map elements
  virtual void incrementElems();

  /// Adds an element to the map of tracked objects
  /// If the element is already being tracked, this fuction will update
  /// its state data
  virtual map<MapId,MapElement>::iterator addElem(MapElement& elem);

  /// returns the max of two numbers
  double max(double x, double y) { return (x > y ? x : y); }
  
  /// returns the min of two numbers
  double min(double x, double y) { return (x < y ? x : y); }

  /// Adds noise to all the elements the sensor is keeping track of
  /// The noise characteristics are sensor-specific, but may include
  /// gaussian noise in the position vector, drop-outs, modifications to
  /// the geometry, etc.
  /// If sensor is "down", it will just set the elements to "invisible",
  /// meaning that they will not be sent on to the map.
  //  virtual bool addNoise(MapElement& elem) = 0;

  /// Position relative to center of Alice's rear axle (in vehicle frame)
  point2 position;
  /// The range of the sensor, in meters
  double range;
  /// The angle of the sensor, in radians, in vehicle frame 
  double angleBegin;
  /// The angle of the sensor, in radians, in vehicle frame 
  double angleEnd;

  /// The boundary defining the sensor's coverage
  /// This is in vehicle frame, so the points must be converted
  /// to local before doing any calculations
  std::vector<point2> sensorBounds;

  /// The current sensor bounds, in abs local coords
  std::vector<point2> currSensorBounds;

  /// The list of elements this sensor is currently tracking. Note that the MapId
  /// refers to the original element's id, and does not change, while the MapElement's
  /// actual id can change based on the sensor, noise, etc
  map<MapId, MapElement> elements;

  /// skynet key
  int skynetKey;
  /// Which subgroup to send map elements on
  int sendSubgroup;
  /// Map element talker for sending map elements
  CMapElementTalker maptalker;

  /// List of element IDs that a sensor is currently tracking.
  /// This is used when an object moves out of range, so that
  /// we can send a "clear" message to mapper
  vector<MapId> currObsIDs;

  /// Whether or not debugging messages are turned on
  int debug;
  
  /// number of elements we've cycled through
  int numElements;  

  /// whether or not we've looped through all 500 ids for objects
  bool maxElementsUsed;

  // NOISE //

  /// noise to add to obstacle position
  double obsPositionNoise;
  double MIN_OBS_POS_NOISE;
  double RANGE_OBS_POS_NOISE;

  
};

#endif
