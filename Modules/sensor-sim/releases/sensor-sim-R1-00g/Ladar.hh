#ifndef __LADAR_HH__
#define __LADAR_HH__

#include "Sensor.hh"
#include <map>

const double THRESHOLD_VELOCITY = 1.7;
const double OBJECT_DEPTH = 1;

class Ladar : public Sensor {

public:

  /// Constructor currently does nothing                                                                                                                    
  Ladar();

  /// Destructor currently does nothing                                                                                                                     
  ~Ladar();

  /**** FUNCTIONS ***/

  /// Checks if the given map element can be sensed by
  /// this particular perceptor
  virtual bool isCorrectObjType(MapElementType type);

  /// Adds noise based on the perceptor's characteristics
  virtual bool addNoise(MapElement& elem);

private:

  /// returns the visible edge of the car
  vector<point2_uncertain> getEdge(point2arr_uncertain& array);

  /// makes the edges of an obstacle noisy
  void makeNoisyEdge(vector<point2_uncertain>& arr);

  /// Grows the visible edge of the car
  void growEdge(vector<point2_uncertain>& arr);

  /// how often to generate "ghost" objects for cars
  /// ghosts are when an object changes id (to simulate the perceptor losing track)
  int carGhostFrequency;
  int minCarGhostFreq;
  double rangeCarGhostFreq;

  int carObsGhostFrequency;
  int minCarObsGhostFreq;
  double rangeCarObsGhostFreq;

  /// how often to genereate "ghost" objects for obstacles
  int obsGhostFrequency;
  int minObsGhostFreq;
  double rangeObsGhostFreq;

  /// how often to drop messages
  int dropoutFrequency;
  int minDropoutFreq;
  double rangeDropoutFreq;

  /// noise to add to the velocity
  double velocityNoise;
  double minVelNoise;
  double rangeVelNoise;

  // count how many times we've gone through the addnoise loop
  int loopcounter;

};

#endif
