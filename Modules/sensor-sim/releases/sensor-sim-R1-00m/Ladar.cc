#include "Ladar.hh"
#include <math.h>
#include <time.h>

Ladar::Ladar()
{
  loopcounter = 0;

  // seed the rand function
  srand((unsigned)time(0)); 

}

Ladar::~Ladar()
{
}

void Ladar::initParams(gengetopt_args_info* cmdline)
{

  MIN_CAR_GHOST_FREQ = cmdline->minCarGhost_arg;
  RANGE_CAR_GHOST_FREQ = cmdline->rangeCarGhost_arg;
  carGhostFrequency = MIN_CAR_GHOST_FREQ + RANGE_CAR_GHOST_FREQ/2;

  MIN_CAR_OBS_GHOST_FREQ = cmdline->minCarObsGhost_arg;
  RANGE_CAR_OBS_GHOST_FREQ = cmdline->rangeCarObsGhost_arg;
  carObsGhostFrequency = MIN_CAR_OBS_GHOST_FREQ + RANGE_CAR_OBS_GHOST_FREQ/2;

  MIN_OBS_GHOST_FREQ = cmdline->minObsGhost_arg;
  RANGE_OBS_GHOST_FREQ = cmdline->rangeObsGhost_arg;
  obsGhostFrequency = MIN_OBS_GHOST_FREQ + RANGE_OBS_GHOST_FREQ/2;

  MIN_DROPOUT_FREQ = cmdline->minDropout_arg;
  RANGE_DROPOUT_FREQ = cmdline->rangeDropout_arg;
  dropoutFrequency = MIN_DROPOUT_FREQ + RANGE_DROPOUT_FREQ/2;

  MIN_VEL_NOISE = cmdline->minVelNoise_arg;
  RANGE_VEL_NOISE = cmdline->rangeVelNoise_arg;
  velocityNoise = MIN_VEL_NOISE + RANGE_VEL_NOISE/2;

  MIN_OBS_POS_NOISE = cmdline->minObsPosNoise_arg;
  RANGE_OBS_POS_NOISE = cmdline->rangeObsPosNoise_arg;
  obsPositionNoise = MIN_OBS_POS_NOISE + RANGE_OBS_POS_NOISE/2;

  VEHICLE_MIN_VELOCITY = cmdline->minVehicleVel_arg;
  VEHICLE_MAX_VELOCITY = cmdline->maxVehicleVel_arg;
  VEHICLE_MIN_TRACKING = cmdline->minTrackVeh_arg;

}

bool Ladar::isCorrectObjType(MapElementType type)
{
  // only obstacles can be seen by a ladar                                                                                                                  
  if (type >= 11)
    return true;
  else
    return false;
}

void Ladar::simulate(MapElement elem, bool addNoiseToElem)
{

  /******* CHECK IF ELEM IS IN RANGE **********/

  // check to see if the element is in range
  vector<point2> newBoundary = isInRange(elem.geometry,addNoiseToElem);

  if (newBoundary.size() != 0) {
      
    if (debug==3) cout<<"is in range"<<endl;
    if (debug==3) cout<<"IS SENSED"<<endl;

    // add to our list if it isn't already there
    map<MapId,MapElement>::iterator elemiter = addElem(elem);
      
    // from here on, refer only to element id

    loopcounter++;
    if (loopcounter == 1500)
      loopcounter = 0;
    
    elemiter->second.timesSeen ++;

    /************ ADD NOISE **********/

    // add noise
    if (addNoiseToElem) {

      MapElement *elem = &elemiter->second;

      // every once in a while this sensor has a dropout
      if (loopcounter % dropoutFrequency == 0) {
	// get a new dropout frequency
	dropoutFrequency = MIN_DROPOUT_FREQ + int(RANGE_DROPOUT_FREQ*rand()/(RAND_MAX + 1.0)); 
	//    cout<<"dropout freq: "<<dropoutFrequency<<endl;
	// dont send the element
	deleteElem(elemiter->first);
	return;
      }

      if (elem->timesSeen < 10) {
	// haven't had time to classify it yet
	addPosNoise(newBoundary);
	elem->setGeometry(newBoundary);
	elem->setTypeLadarObstacle();
	elem->plotColor = MAP_COLOR_PINK;
	maptalker.sendMapElement(&(elemiter->second),sendSubgroup);    
	return;
      }

      // get new velocity noise
      velocityNoise = MIN_VEL_NOISE + RANGE_VEL_NOISE*rand()/(RAND_MAX + 1.0); 
      //  cout<<"velocity noise: "<<velocityNoise<<endl;
      double elemVelocity = sqrt(pow(elem->velocity.x,2)+pow(elem->velocity.y,2));

      // check whether we have a vehicle or obstacle (don't trust the map 
      // element type because we could have changed that earlier)
      if (elemVelocity > 0 || elem->timeStopped > 0)
	elem->setTypeVehicle();
      else 
	elem->setTypeObstacle();

      // now the noise we add will depend on whether it's an obstacle or vehicle

      /*********** VEHICLE NOISE ***************/
      if (elem->type == ELEMENT_VEHICLE) {

	// if car is moving too quickly, it's an obs
	if (elemVelocity > VEHICLE_MAX_VELOCITY - velocityNoise) {

	  // set the boundary to just be the edge
	  addPosNoise(newBoundary);
	  elem->setGeometry(newBoundary);
	  elem->setTypeLadarObstacle();
	  elem->plotColor = MAP_COLOR_PINK;

	  // leave ghosts far more often for this one
	  if (loopcounter % carObsGhostFrequency == 0) {
	    // make a ghost
	    elem->setId(groupID,numElements);
	    incrementElems();
	    // get a new random number
	    obsGhostFrequency = MIN_CAR_OBS_GHOST_FREQ + int(RANGE_CAR_OBS_GHOST_FREQ*rand()/(RAND_MAX + 1.0)); 
	    //	  cout<<"car obs ghost freq "<<carObsGhostFrequency<<endl;
	  } 

	}

	// if car is moving too slowly and we haven't been tracking it, it's an obs
	else if (((elemVelocity < VEHICLE_MIN_VELOCITY + velocityNoise)
		 && elem->timesSeen < VEHICLE_MIN_TRACKING) ||
		 elemVelocity < VEHICLE_MIN_VELOCITY/2 + velocityNoise) {

	  // set the boundary to just be the edge
	  addPosNoise(newBoundary);
	  elem->setGeometry(newBoundary);
	  elem->setTypeLadarObstacle();
	  elem->plotColor = MAP_COLOR_PINK;
	  
	  // don't send ghosts for this

	}

	// else it's just vehicle
	else {

	  elem->height = 2;
	  elem->plotColor = MAP_COLOR_GREY;

	  // every once in a while, change the id to simulate losing track of vehicle
	  if (loopcounter % carGhostFrequency == 0) {
	    // make a ghost
	    elem->setId(groupID,numElements); 
	    incrementElems();
	    // get a new random number
	    carGhostFrequency = MIN_CAR_GHOST_FREQ + int(RANGE_CAR_GHOST_FREQ*rand()/(RAND_MAX + 1.0)); 
	    //	  cout<<"car ghost freq "<<carGhostFrequency<<endl;
	  }

	}

      }

      /********* OBSTACLE NOISE ***********/
      else if (elem->type == ELEMENT_OBSTACLE) {

	// set the boundary to just be the edge
	addPosNoise(newBoundary);
	elem->setGeometry(newBoundary);

	// don't track as obstacle until we've seen it > 200 times
	if (elem->timesSeen < 100 + obsGhostFrequency) {
	  elem->setTypeLadarObstacle();
	  elem->plotColor = MAP_COLOR_PINK;
	}
	else 
	  elem->plotColor = MAP_COLOR_YELLOW;

	// every once in a while, send a ghost
	if (loopcounter % obsGhostFrequency == 0) {
	  // make a ghost
	  elem->setId(groupID,numElements);
	  incrementElems();
	  // get a new random number
	  obsGhostFrequency = MIN_OBS_GHOST_FREQ 
	    + int(RANGE_OBS_GHOST_FREQ*rand()/(RAND_MAX + 1.0)); 
	  //	cout<<"obs ghost freq "<<obsGhostFrequency<<endl;
	}
	
      }  
    } // end add noise

    else { // hack to fix bug hack that i don't understand
      if (elemiter->second.type == ELEMENT_VEHICLE)
	elemiter->second.setTypeVehicle();
      else if (elemiter->second.type == ELEMENT_OBSTACLE)
	elemiter->second.setTypeObstacle();
    }

    /********* SEND THE ELEMENT *********/
    maptalker.sendMapElement(&(elemiter->second),sendSubgroup);    
    
  }

  else // not in range
    deleteElem(elem.id);

}
