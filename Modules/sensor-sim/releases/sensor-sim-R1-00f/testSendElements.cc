#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include <math.h>
#include <iostream>

int main(int argc, char *argv[]) {

  int skynetKey = 0;
  int subgroup = -12;
  
  skynetKey = atoi(getenv("SKYNET_KEY"));  

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);

  MapElement el;
  el.setTypeVehicle();
  el.plotColor = MAP_COLOR_RED;
  double increment = .1;
  double orientation = M_PI/2;
  bool refresh;
  if (argc>1) {
    orientation = atoi(argv[1]);
    refresh = false;
  }
  cout<<"increment: "<<increment;

  point2 position(10,0);
  double lx = 2;
  double ly = 5;

  el.timeStopped = 0;

  // move the car
  while(1) {

    usleep(100000);
    if (refresh) {
      position.x += increment;
      position.y = cos(position.x);
      orientation = acos(position.y);
    }

    el.velocity = point2(1.5,.3);

    cout<<"("<<position.x<<","<<position.y<<")"<<endl;

    // return the four corners of the car
    std::vector<point2> boundary;
    
    double x = position.x;
    double y = position.y;
    double t = orientation;
    

    // put in the points at origin
    boundary.push_back(point2(.5*lx,.5*ly));
    boundary.push_back(point2(0,.7*ly));
    boundary.push_back(point2(-.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,-.5*ly));
    boundary.push_back(point2(0,-.7*ly));
    boundary.push_back(point2(.5*lx,-.5*ly));

    double oldY, oldX;
    // rotate points and translate
    for (std::vector<point2>::iterator iter = boundary.begin();
	 iter != boundary.end(); iter++) {
      oldX = iter->x;
      oldY = iter->y;
      iter->x = oldX*cos(t) - oldY*sin(t) +x;
      iter->y = oldX*sin(t) + oldY*cos(t) +y;
    }

    el.setGeometry(boundary);
    maptalker.sendMapElement(&el,subgroup);

  el.setColor(MAP_COLOR_GREY);
  el.geometryType =GEOMETRY_POINTS;
  el.setId(15);
  maptalker.sendMapElement(&el, -2);

  }

  return 0;
}
