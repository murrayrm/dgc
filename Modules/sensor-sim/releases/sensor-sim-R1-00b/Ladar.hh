#ifndef __LADAR_HH__
#define __LADAR_HH__

#include "Sensor.hh"

class Ladar : public Sensor {

public:

  /// Constructor currently does nothing                                                                                                                    
  Ladar();

  /// Destructor currently does nothing                                                                                                                     
  ~Ladar();

  /**** FUNCTIONS ***/

  virtual bool isCorrectObjType(MapElementType type);

  virtual void addNoise(MapElement& elem);

};

#endif
