#include "StereoCam.hh"

StereoCam::StereoCam()
{
}

StereoCam::~StereoCam()
{
}

bool StereoCam::isCorrectObjType(MapElementType type)
{

  // pretty much anything can be seen by a stereocamera,
  // but the perceptors on long and medium do not
  // pick up road lines

  // check if it's a road line
  if (type == ELEMENT_LINE ||
      type == ELEMENT_STOPLINE ||
      type == ELEMENT_LANELINE ||
      type == ELEMENT_PARKING_SPOT ||
      type == ELEMENT_PERIMETER) {

    // bumblebees
    if (groupID == MODlinePerceptorLFShort ||
	groupID == MODlinePerceptorRFShort ||
	groupID == MODlinePerceptorMFMedium) {
      return true;
      //cout<<"forwarding lane line on "<<name<<endl;
    }
    
    // long and medium
    else 
      return false;
  }

  // all other elements:

  // TODO: fix this so it's properly inclusive
  // medium and long
  if (groupID != MODlinePerceptorLFShort &&
      groupID != MODlinePerceptorRFShort)
    return true;

  // bumblebees
  return false;

}

bool StereoCam::addNoise(MapElement& elem)
{

  // Lines
  if (groupID == MODlinePerceptorRFShort ||
      groupID == MODlinePerceptorLFShort ||
      groupID == MODlinePerceptorMFMedium ||
      groupID == MODlinePerceptorMFLong) {
    
    elem.setColor(MAP_COLOR_GREY);
    
  }
  // Obstacles
  else if (groupID == MODstereoObsPerceptorMedium ||
	   groupID == MODstereoObsPerceptorLong) {
    
    // make into a blob
    
    // depending on dist from alice, elongate blob
    
    // add position noise, depending on dist from alice
    
    // add some random dropouts, depending on location in sensor range

    elem.setTypeStereoObstacle();
  
  }
  
  return true;

}

