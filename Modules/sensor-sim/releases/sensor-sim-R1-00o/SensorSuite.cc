#include "SensorSuite.hh"
#include <math.h>
#include <assert.h>
#include <time.h>

//volatile sig_atomic_t quit = 0;
int quitConsole;

SensorSuite::SensorSuite() 
{
  loopnum = 0;
  updateranges = false;
  addNoise = true;
  prob_planning = false;
  enableGroundstrikes = false;
  
  // seed the rand function
  srand((unsigned)time(0)); 
}

SensorSuite::~SensorSuite()
{
}


void SensorSuite::initialize(int skynetKey, int recvSubgroup, int sendSubgroup, int debug)
{
  // set up the MapElementTalker
  this->skynetKey = skynetKey;
  this->recvSubgroup = recvSubgroup;
  this->sendSubgroup = sendSubgroup;
  maptalker.initRecvMapElement(skynetKey,recvSubgroup);

  this->debug = debug;

  // set up all the sensors:

  // RIEGL: front middle bumper
  // currently not in use, so disabled by default
  Ladar riegl;
  riegl.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		   65,
		   -M_PI/4,
		   M_PI/4,
		   MODladarCarPerceptorRiegl,
		   "MODladarCarPerceptorRiegl",
		   skynetKey,
		   sendSubgroup,
		   debug);
  ladars.insert( make_pair( riegl.name , riegl) );

  // LADARs

  // middle ladar, inside bumper
  Ladar MFBumper;
  MFBumper.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		      35,
		      -M_PI/2,
		      M_PI/2,
		      MODladarCarPerceptorCenter,
		      "MODladarCarPerceptorCenter",
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( MFBumper.name, MFBumper) );

  // right front ladar, inside bumper and on top of bumper
  Ladar RFBumper;
  RFBumper.initialize(point2(DIST_REAR_AXLE_TO_FRONT,VEHICLE_WIDTH/2),
		      35,
		      -M_PI,
		      0,
		      MODladarCarPerceptorRight,
		      "MODladarCarPerceptorRight",
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( RFBumper.name, RFBumper) );
  Ladar RFRoof;
  RFRoof.initialize(point2(DIST_REAR_AXLE_TO_FRONT,VEHICLE_WIDTH/2),
		    35,
		    -M_PI,
		    0,
		    MODladarCarPerceptorRoofRight,
		    "MODladarCarPerceptorRoofRight",
		    skynetKey,
		    sendSubgroup,
		    debug);
  ladars.insert( make_pair( RFRoof.name, RFRoof) );

  // left front ladar, inside bumper and on top of bumper
  Ladar LFBumper;
  LFBumper.initialize(point2(DIST_REAR_AXLE_TO_FRONT,-VEHICLE_WIDTH/2),
		      35,
		      0,
		      M_PI,
		      MODladarCarPerceptorLeft,
		      "MODladarCarPerceptorLeft",
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( LFBumper.name, LFBumper) );
  Ladar LFRoof;
  LFRoof.initialize(point2(DIST_REAR_AXLE_TO_FRONT,-VEHICLE_WIDTH/2),
		    35,
		    0,
		    M_PI,
		    MODladarCarPerceptorRoofLeft,
		    "MODladarCarPerceptorRoofLeft",
		    skynetKey,
		    sendSubgroup,
		    debug);
  ladars.insert( make_pair( LFRoof.name, LFRoof) );
  
  // rear ladar
  Ladar rearladar;
  rearladar.initialize(point2(-DIST_REAR_TO_REAR_AXLE,0),
		      35,
		      M_PI/2,
		      -M_PI/2,
		      MODladarCarPerceptorRear,
		      "MODladarCarPerceptorRear",
		      skynetKey,
		      sendSubgroup,
		      debug);
  ladars.insert( make_pair( rearladar.name, rearladar) );

  // stereo cameras

  // medium range stereo, front of alice
  // first one is for obstacles
  StereoCam MFMedium;
  MFMedium.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		      35,
		      -M_PI/5,
		      M_PI/5,
		      MODstereoObsPerceptorMedium,
		      "MODstereoObsPerceptorMedium",
		      skynetKey,
		      sendSubgroup,
		      debug);
  cameras.insert( make_pair( MFMedium.name, MFMedium) );

  // second is for lines
  StereoCam MFMediumLine;
  MFMediumLine.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
			  15,
			  -M_PI/3,
			  M_PI/3,
			  MODlinePerceptorMFMedium,
			  "MODlinePerceptorMFMedium",
			  skynetKey,
			  sendSubgroup,
			  debug);
  cameras.insert( make_pair( MFMediumLine.name, MFMediumLine) );

  // long range stereo, front of alice
  // MODstereoObsPerceptorLong
  StereoCam MFLong;
  MFLong.initialize(point2(DIST_REAR_AXLE_TO_FRONT,0),
		    70,
		    -M_PI/8,
		    M_PI/8,
		    MODstereoObsPerceptorLong,
		    "MODstereoObsPerceptorLong",
		    skynetKey,
		    sendSubgroup,
		    debug);
  cameras.insert( make_pair( MFLong.name, MFLong) );

  // bumblebees
  // short range stereo, left side front
  StereoCam LFShort;
  LFShort.initialize(point2(DIST_REAR_AXLE_TO_FRONT,VEHICLE_WIDTH/2),
		     3,
		     -M_PI/8,
		     M_PI/8,
		     MODlinePerceptorLFShort,
		     "MODlinePerceptorLFShort",
		     skynetKey,
		     sendSubgroup,
		     debug);
  cameras.insert( make_pair( LFShort.name, LFShort) );
  
  // short range stereo, right side front
  StereoCam RFShort;
  RFShort.initialize(point2(DIST_REAR_AXLE_TO_FRONT,-VEHICLE_WIDTH/2),
		     3,
		     -M_PI/8,
		     M_PI/8,
		     MODlinePerceptorRFShort,
		     "MODlinePerceptorRFShort",
		     skynetKey,
		     sendSubgroup,
		     debug);
  cameras.insert( make_pair( RFShort.name, RFShort) );

  // RADAR and PTU
  // not currently implemented since there are currently changes being made

  // GROUNDSTRIKES: initialized below

}

void SensorSuite::initSensorParams(gengetopt_args_info* cmdline)
{
  for (std::map<std::string, Ladar>::iterator it = ladars.begin();
       it != ladars.end(); it++) {
    it->second.initParams(cmdline);
  }
  for (std::map<std::string, StereoCam>::iterator it = cameras.begin();
       it != cameras.end(); it++) {
    it->second.initParams(cmdline);
  }

  // also setup groundstrikes
  minGroundstrike = cmdline->minGroundstrike_arg;
  maxGroundstrike = cmdline->maxGroundstrike_arg;
  generateGroundstrikes();

  gstrike.setTypeLadarObstacle();
  gstrike.setId(17);

}

void SensorSuite::updateElems()
{
  MapElement el;

  // get a new map element
  int bytesRecv = 0;
  bytesRecv = maptalker.recvMapElementBlock(&el,recvSubgroup);


  // note: y coord is negative for some calculations b/c +z is down
  point2 alicepos = point2(el.state.localX,-el.state.localY);
  point2 actualalicepos = point2(alicepos.x,-alicepos.y);
  double aliceyaw = el.state.localYaw;
  double aliceyawrate = el.state.utmYawRate;

  // this way of looking at sensor coverage is deprecated. however i left
  // it in because it's useful for debugging. set --debug=1 to see the sensor
  // ranges visualized
  if (loopnum % UPDATE_FREQ == 0) {

    if (debug==1) { // note: this should only be used for debugging now
      for (std::map<std::string, Ladar>::iterator it = ladars.begin();
	   it != ladars.end(); it++) {
	if (it->second.isWorking)
	  it->second.updateCoverage(alicepos,aliceyaw);
      }
      for (std::map<std::string, StereoCam>::iterator it = cameras.begin();
	   it != cameras.end(); it++) {
	if (it->second.isWorking)
	  it->second.updateCoverage(alicepos,aliceyaw);
      }
    }
  }
    
  // do a quick check to make sure the object is in alice's range (note
  // that this check does not apply to lines since the center point of/
  // a very long line could be far from alice but we still see some of it)
  if (actualalicepos.dist(el.position) < 100 || el.type <11) {

    // iterate over the sensors and see of the mapelement falls under any of them
    // (make sure to skip non-working sensors)
    for (std::map<std::string, Ladar>::iterator it = ladars.begin();
	 it != ladars.end(); it++) {
      
      it->second.currAlicePos = actualalicepos;
      it->second.currAliceYaw = aliceyaw;

      if (it->second.isWorking && it->second.isCorrectObjType(el.type)) {

	// if the element is a clear message, make sure it gets deleted from each sensor
	if (el.type == ELEMENT_CLEAR)
	  it->second.deleteElem(el.id);
	// now add noise and do whatever else
	else
        {
	   it->second.simulate(el,addNoise, prob_planning);
	}

      }
    }
    for (std::map<std::string, StereoCam>::iterator it = cameras.begin();
	 it != cameras.end(); it++) {

      it->second.currAlicePos = actualalicepos;
      it->second.currAliceYaw = aliceyaw;

      if (it->second.isWorking && it->second.isCorrectObjType(el.type)) {

	if (el.type == ELEMENT_CLEAR)
	  it->second.deleteElem(el.id);
	else
	  it->second.simulate(el,addNoise, prob_planning);
	  
      }
    }
  }

  // send groundstrikes
  if (enableGroundstrikes && loopnum%10 == 0 && fabs(aliceyawrate)>.05) {

    if (loopnum%100 == 0) {
      gstrikeIndices.clear();

      double yawratemultiplier = 20;
      int numgstrikes = fabs(aliceyawrate)*yawratemultiplier;
      if (numgstrikes > (maxGroundstrike-minGroundstrike-1))
	numgstrikes = maxGroundstrike-minGroundstrike-1;
      else if (numgstrikes == 0)
	numgstrikes = 1;
      for (int k = 0; k <= numgstrikes; k++)
	gstrikeIndices.push_back(minGroundstrike + (maxGroundstrike-minGroundstrike)*(rand()/(RAND_MAX + 1.0)));

    }

    for (vector<int>::iterator iter = gstrikeIndices.begin();
	 iter != gstrikeIndices.end(); iter++) {
    
      vector<point2> arr = groundstrikes.find(*iter)->second;

      double posNoiseX = 10*(rand()/(RAND_MAX+1.0));
      double posNoiseY = 2*(rand()/(RAND_MAX+1.0));      

      // place groundstrikes on outside of turn
      point2 posInVehFrame;
      if (aliceyawrate<0) // turning left
	posInVehFrame = point2(10+posNoiseX,15+posNoiseY);
      else // turning right
	posInVehFrame = point2(15+posNoiseX,-15+posNoiseY);
      
      // rotate and translate the groundstrike (always same orientation as alice)
      double ca = cos(aliceyaw);
      double sa = sin(aliceyaw);
      double x = actualalicepos.x + posInVehFrame.x*ca - posInVehFrame.y*sa;
      double y = actualalicepos.y + posInVehFrame.x*sa + posInVehFrame.y*ca;
      for (vector<point2>::iterator giter = arr.begin();
	   giter != arr.end(); giter++) {
	*giter = point2(giter->x*ca - giter->y*sa + x,
			giter->x*sa + giter->y*ca + y);
      }
      gstrike.setId(*iter);
      gstrike.setGeometry(arr);
      
      for (std::map<std::string, Ladar>::iterator it = ladars.begin();
	   it != ladars.end(); it++) {
	if (it->second.isWorking) {
	  it->second.simulate(gstrike,false, prob_planning);
	}
      }
    }
  }

  loopnum++;
  if (loopnum > 500)
    loopnum = 0;
}


void SensorSuite::disableSensor(std::string sensorName)
{

  if (sensorName == "ALLLADARS") {
    cout<<"Disabling all LADARs"<<endl;
    for (map<std::string,Ladar>::iterator it = ladars.begin();
	 it != ladars.end(); it++) {
      it->second.isWorking = false;
    }
    return;
  }

  if (sensorName == "ALLSTEREO") {
    cout<<"Disabling all stereo cameras"<<endl;
    for (map<std::string,StereoCam>::iterator it = cameras.begin();
	 it != cameras.end(); it++) {
      it->second.isWorking = false;
    }
    return;
  }

  // disable the sensor
  map<std::string,Ladar>::iterator it1 = ladars.find(sensorName);
  if (it1 != ladars.end()) {
    cout<<sensorName<<" ("<<it1->second.groupID<<")\tDisabled"<<endl;
    it1->second.isWorking = false;
  }
  map<std::string,StereoCam>::iterator it2 = cameras.find(sensorName);
  if (it2 != cameras.end())  {
    cout<<sensorName<<" ("<<it2->second.groupID<<")\tDisabled"<<endl;
    it2->second.isWorking = false;
  }

  // TODO: send a message to the health-monitor

}

void SensorSuite::enableSensor(std::string sensorName)
{

  if (sensorName == "ALLLADARS") {
    cout<<"Enabling all LADARs"<<endl;
    for (map<std::string,Ladar>::iterator it = ladars.begin();
	 it != ladars.end(); it++) {
      it->second.isWorking = true;
    }
    return;
  }

  if (sensorName == "ALLSTEREO") {
    cout<<"Enabling all stereo cameras"<<endl;
    for (map<std::string,StereoCam>::iterator it = cameras.begin();
	 it != cameras.end(); it++) {
      it->second.isWorking = true;
    }
    return;
  }

  // enable the sensor
  map<std::string,Ladar>::iterator it1 = ladars.find(sensorName);
  if (it1 != ladars.end())  {
    cout<<sensorName<<"\tEnabled"<<endl;
    it1->second.isWorking = true;
    return;
  }
  map<std::string,StereoCam>::iterator it2 = cameras.find(sensorName);
  if (it2 != cameras.end())  {
    cout<<sensorName<<"\tEnabled"<<endl;
    it2->second.isWorking = true;
    return;
  }

  // TODO: send a message to the health-monitor

}

void SensorSuite::setRange(double range, std::string sensorName)
{

  map<std::string,Ladar>::iterator it1 = ladars.find(sensorName);
  if (it1 != ladars.end())  {
    cout<<"Setting range on "<<sensorName<<" to "<<range<<endl;
    it1->second.setRange(range);
    return;
  }
  map<std::string,StereoCam>::iterator it2 = cameras.find(sensorName);
  if (it2 != cameras.end())  {
    cout<<"Setting range on "<<sensorName<<" to "<<range<<endl;
    it2->second.setRange(range);
    return;
  }
  
  cout<<"setRange: sensor "<<sensorName<<" not found"<<endl;

}

void SensorSuite::generateGroundstrikes()
{

  double objectDepth = 1;
  double xIncr = .3;
  double minNoise = -.25;
  double rangeNoise = .5;
  
  // generate for various lengths
  for (int i = minGroundstrike; i <= maxGroundstrike; i++) {

    vector<point2> arr;
    double x, y, noise;
    x = 0;
    y = 0;
    noise = 0;

    // go over front edge
    while (x<=i) {
      arr.push_back(point2(x,y));
    
      noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);
      x += xIncr + noise;
      y += noise;
    }
    arr.push_back(point2(x,y));
 
    // now make back edge
    vector<point2>::iterator it = arr.end();
    it--;
    int size = arr.size();
    for (int k = 0; k < size -1; k++) {
      arr.push_back(point2(it->x,it->y+objectDepth));
      it--;
    }

    groundstrikes.insert( make_pair(i,arr) );

    if (debug==3) {
      cout<<"A"<<i<<" = [";
      for (vector<point2>::iterator it2 = arr.begin();
	   it2 != arr.end(); it2++) {
	cout<<it2->x<<" "<<it2->y<<";"<<endl;
      }
      cout<<"]"<<endl;
    }
  }

}

// Initialize console display
int SensorSuite::initConsole()
{
  const char *header =
    "-----------------------------------------------------------------------------------------\n"
    " SensorSim $Revision$ \n"
    "-----------------------------------------------------------------------------------------\n"
    "                                                                                         \n"
    " Process Name                  Logsize  Health  Lat                        %OVHEALTH%    \n"
    "-----------------------------------------------------------------------------------------\n";

  const char *body = 
    " %%_desc_%02d%%                   %%_status_%02d%%           [%%STR%02d%%|%%SP%02d%%|%%KL%02d%%]\n";

  const char *footer = 
    "                                                                                         \n"
    "-----------------------------------------------------------------------------------------\n"
    "                                                                                         \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    "                                                                                         \n"
    "                                                                                         \n"
    "                                                                                         \n"
    "-----------------------------------------------------------------------------------------\n"
    "[%QUIT%]                     [%START_ALL%|%STOP_ALL%|%KILL_ALL%|%LOG_ALL%|%VIEW_LOGGERS%]\n";

  int i;
  char temp[8192];
  char buf[1024];
  //  ProcessData *proc;
  int numSensors = cameras.size() + ladars.size();

  // Initialize the console template
  memset(temp, 0, sizeof(temp)); 
  strcpy(temp, header);

  // Loop through the processes and create corresponding entries in
  // the template.
  for (i = 0; i < numSensors; i++)
    {
      //      proc = &this->procs[i];    
      snprintf(buf,sizeof(buf), body, i, i, i, i, i, i, i);
      strcat(temp, buf);
    }

  strcat(temp, footer);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, temp);
  
  for(i = 0; i < numSensors; i++)
    {
      snprintf(buf,sizeof(buf),"%%STR%02d%%",i);
      cotk_bind_button(this->console, buf, " START ", "",
		       (cotk_callback_t) onUserStartModule, this);
      snprintf(buf,sizeof(buf),"%%SP%02d%%",i);
      cotk_bind_button(this->console, buf, " STOP ", "",
		       (cotk_callback_t) onUserStopModule, this);
      snprintf(buf,sizeof(buf),"%%KL%02d%%",i);
      cotk_bind_button(this->console, buf, " KILL ", "",
		       (cotk_callback_t) onUserKillModule, this);
    }
  
  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_button(this->console, "%START_ALL%", " START ALL ", "Ss",
                   (cotk_callback_t) onUserStartAll, this);
  cotk_bind_button(this->console, "%STOP_ALL%", " STOP ALL ", "Tt",
                   (cotk_callback_t) onUserStopAll, this);
  cotk_bind_button(this->console, "%KILL_ALL%", " KILL ALL ", "Kk",
                   (cotk_callback_t) onUserKillAll, this);

  // Initialize the display
  if (cotk_open(this->console, NULL) != 0)
    return -1;

  // Create colors
  cotk_set_color_pair(this->console, 1, COLOR_RED, -1);
  
  // Display some fixed values
  int count = 0;
  for (map<string,Ladar>::iterator it = ladars.begin();
       it != ladars.end(); it++)
    {
      snprintf(buf,sizeof(buf),"%%_desc_%02d%%",count);
      cotk_printf(console, buf, A_NORMAL, it->second.name.c_str());
      count++;
    }
  for (map<string,StereoCam>::iterator it = cameras.begin();
       it != cameras.end(); it++)
    {
      snprintf(buf,sizeof(buf),"%%_desc_%02d%%",count);
      cotk_printf(console, buf, A_NORMAL, it->second.name.c_str());
      count++;
    }

  quitConsole = 0;

  //cotk_printf(this->console, "%log_name%", A_NORMAL, this->logName);

  return 0;
}


// Finalize console display
int SensorSuite::finiConsole()
{
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}

bool SensorSuite::updateConsole()
{
  if (quitConsole)
    return false;

  cotk_update(console);

  return true;

}

// Gets the index associated with a given token i.e. %START12% returns 12
int SensorSuite::getTokenIndex(const char *token)
{  
  char tid[8];
  int j = 0;
  
  for(int i = 0; i < (int) strlen(token); i++)
    {
      if(isdigit(token[i]))
	{
	  while(isdigit(token[i]))
	    tid[j++] = token[i++];
	  tid[j++] = 0;
	  break;         
	}
    }

  return atoi(tid);
}

// Handle button callbacks
int SensorSuite::onUserQuit(cotk_t *console, SensorSuite *self, const char *token)
{
  MSG("user quit");
  quitConsole = 1;
  return 0;
}


// Handle button callbacks
int SensorSuite::onUserStartAll(cotk_t *console, SensorSuite *self, const char *token)
{
  MSG("user start all");

  return 0;
}


// Handle button callbacks
int SensorSuite::onUserStopAll(cotk_t *console, SensorSuite *self, const char *token)
{
  MSG("user stop all");
  return 0;
}


// Handle button callbacks
int SensorSuite::onUserKillAll(cotk_t *console, SensorSuite *self, const char *token)
{
  MSG("user kill all");

  return 0;
}


// Handle button callbacks
int SensorSuite::onUserStartModule(cotk_t *console, SensorSuite *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user start module-id:");// %s", self->procs[tid].name);
  //  self->startProcess(tid);
  return 0;
}


// Handle button callbacks
int SensorSuite::onUserStopModule(cotk_t *console, SensorSuite *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user stop module-id:");// %s", self->procs[tid].name);
  //  self->sendQuitRequest(tid);
  return 0;
}

// Handle button callbacks
int SensorSuite::onUserKillModule(cotk_t *console, SensorSuite *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user kill module-id:");// %s", self->procs[tid].name);
  //  self->killProcess(tid);
  return 0;
}



void SensorSuite::updateWindowStatus(int index, bool open)
{
  char token[16];
  
  // get the correct token to change for this index
  snprintf(token, sizeof(token), "%%s%02d%%", index);

  if(open)
    cotk_printf(this->console, token, A_NORMAL, "OPEN");
  else
    cotk_printf(this->console, token, A_NORMAL, "    ");
}

