#include "SensorSuite.hh"
#include "cmdline.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>


int main(int argc, char **argv) {

  // get gengetopt opts
  gengetopt_args_info cmdline;

  // get config file from command line
  if (cmdline_parser2(argc, argv, &cmdline, 1, 1, 0) != 0)
    exit (1);

  cout<<"-------------------------------------"<<endl;

  cout<<"Loading config file ... "<<endl;

  string configfile = cmdline.config_file_arg;

  // load the config file
  if (cmdline_parser_configfile(cmdline.config_file_arg, &cmdline, 0, 1, 0) != 0)
    exit(1);
  else {
    if (configfile == cmdline.config_file_arg)
      cout<<" Loaded '"<<configfile<<"' (default)"<<endl;
    else 
      cout<<" Loaded '"<<configfile<<"' from command line"<<endl;
  }

  cout<<"Loading command line opts ...\n (note this will override config file options)"<<endl;

  // then load command line (this will override a config file)
  if (cmdline_parser2(argc, argv, &cmdline, 1, 0, 0) != 0) 
    exit (1);

  cout<<"-------------------------------------"<<endl;

  // get skynet key
  int skynet_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if ( pSkynetkey == NULL || cmdline.skynet_key_given) {
    skynet_key = cmdline.skynet_key_arg;
  } 
  else {
    skynet_key = atoi(pSkynetkey);
  }
  cout << "Constructing skynet with KEY = " << skynet_key << endl;

  // get the sending and receiving subgroups
  int recvSubgroup = cmdline.recv_subgroup_arg;
  int sendSubgroup = cmdline.send_subgroup_arg;
  cout<<"Receiving on subgroup "<<recvSubgroup<<endl;
  cout<<"Sending on subgroup "<<sendSubgroup<<endl;

  cout<<"-------------------------------------"<<endl;

  int debug = cmdline.debug_arg;
  cout<<"debug level "<<debug<<endl;

  // check if we want a display
  bool showDisplay = cmdline.enable_display_arg;
  if (!showDisplay)
    cout<<"disabling cotk display."<<endl;

  // create new SensorSuite and initialize with the skynet key
  SensorSuite sensors;
  sensors.initialize(skynet_key,recvSubgroup,sendSubgroup,debug);

  cout<<"-------------------------------------"<<endl;

  // see if we're adding noise
  sensors.addNoise = cmdline.add_noise_arg;
  if (cmdline.add_noise_arg)
    cout<<"Adding noise to MapElements"<<endl;
  else
    cout<<"NOT adding noise to MapElements"<<endl;

  // check for groundstrikes
  sensors.enableGroundstrikes = cmdline.enable_groundstrikes_arg;
  if (sensors.enableGroundstrikes)
    cout<<"Sending groundstrikes"<<endl;
  else
    cout<<"NOT sending groundstrikes"<<endl;

  cout<<"-------------------------------------"<<endl;

  // check to see if any of the sensors are disabled
  if (!cmdline.MODladarCarPerceptorRiegl_arg) 
    sensors.disableSensor("MODladarCarPerceptorRiegl");
  else
    cout<<"MODladarCarPerceptorRiegl ("<<MODladarCarPerceptorRiegl<<")\tEnabled"<<endl;
  if (!cmdline.MODladarCarPerceptorLeft_arg)
    sensors.disableSensor("MODladarCarPerceptorLeft");
  else 
    cout<<"MODladarCarPerceptorLeft ("<<MODladarCarPerceptorLeft<<")\tEnabled"<<endl;
  if (!cmdline.MODladarCarPerceptorRight_arg)
    sensors.disableSensor("MODladarCarPerceptorRight");
  else 
    cout<<"MODladarCarPerceptorRight ("<<MODladarCarPerceptorRight<<")\tEnabled"<<endl;
  if (!cmdline.MODladarCarPerceptorRoofLeft_arg)
    sensors.disableSensor("MODladarCarPerceptorRoofLeft");
  else 
    cout<<"MODladarCarPerceptorRoofLeft ("<<MODladarCarPerceptorRoofLeft<<")\tEnabled"<<endl;
  if (!cmdline.MODladarCarPerceptorRoofRight_arg)
    sensors.disableSensor("MODladarCarPerceptorRoofRight");
  else 
    cout<<"MODladarCarPerceptorRoofRight ("<<MODladarCarPerceptorRoofRight<<")\tEnabled"<<endl;
  if (!cmdline.MODladarCarPerceptorCenter_arg)
    sensors.disableSensor("MODladarCarPerceptorCenter");
  else 
    cout<<"MODladarCarPerceptorCenter ("<<MODladarCarPerceptorCenter<<")\tEnabled"<<endl;
  if (!cmdline.MODladarCarPerceptorRear_arg)
    sensors.disableSensor("MODladarCarPerceptorRear");
  else 
    cout<<"MODladarCarPerceptorRear ("<<MODladarCarPerceptorRear<<")\tEnabled"<<endl;
  if (!cmdline.MODstereoObsPerceptorMedium_arg)
    sensors.disableSensor("MODstereoObsPerceptorMedium");
  else 
    cout<<"MODstereoObsPerceptorMedium ("<<MODstereoObsPerceptorMedium<<")\tEnabled"<<endl;
  if (!cmdline.MODlinePerceptorMFMedium_arg)
    sensors.disableSensor("MODlinePerceptorMFMedium");
  else 
    cout<<"MODlinePerceptorMFMedium ("<<MODlinePerceptorMFMedium<<")\tEnabled"<<endl;
  if (!cmdline.MODstereoObsPerceptorLong_arg)
    sensors.disableSensor("MODstereoObsPerceptorLong");
  else 
    cout<<"MODstereoObsPerceptorLong ("<<MODstereoObsPerceptorLong<<")\tEnabled"<<endl;
  if (!cmdline.MODlinePerceptorLFShort_arg)
    sensors.disableSensor("MODlinePerceptorLFShort");
  else 
    cout<<"MODlinePerceptorLFShort ("<<MODlinePerceptorLFShort<<")\tEnabled"<<endl;
  if (!cmdline.MODlinePerceptorRFShort_arg)
    sensors.disableSensor("MODlinePerceptorRFShort");
  else 
    cout<<"MODlinePerceptorRFShort ("<<MODlinePerceptorRFShort<<")\tEnabled"<<endl;

  cout<<"-------------------------------------"<<endl;

  // modify ranges if necessary
  sensors.setRange(cmdline.range_linePercepMedium_arg,"MODlinePerceptorMFMedium");

  cout<<"-------------------------------------"<<endl;

  // init sensors from command line
  sensors.initSensorParams(&cmdline);

  // start the console
  if (showDisplay)
    sensors.initConsole();

  cout<<"Listening for map elements ..."<<endl;

  // run the simulation loop
  bool quitLoop = false;
  while (!quitLoop) {

    sensors.updateElems();

    if (showDisplay)
      quitLoop = !sensors.updateConsole();
  }

  if (showDisplay)
    sensors.finiConsole();

  return 0;
}

