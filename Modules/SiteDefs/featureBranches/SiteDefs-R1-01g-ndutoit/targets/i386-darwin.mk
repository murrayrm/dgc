########################################################################
#
#      !!!!!! EDIT & CUSTOMIZE THIS FILE !!!!!!
# 
########################################################################
#
# Defines site independent, but target specific build variables
#
# This file is included by overall.mk

CC_COMPILE_FLAGS += -DMACOSX -I/sw/include
CPLUSPLUS_COMPILE_FLAGS += -DMACOSX -I/sw/include
CPLUSPLUS_STATIC_LINK_FLAGS += -L/sw/lib  -bind_at_load
AR_FLAGS += -s
