########################################################################
#
#      !!!!!! EDIT & CUSTOMIZE THIS FILE !!!!!!
# 
########################################################################
#
# Defines build flags common to all the sites. The site specific
# files can override these settings.
# This file is typically included by the site specific site.local file.

# C compiler settings
export CC=gcc
export CPLUSPLUS=g++

export CC_DEPEND_FLAG = -MM
export CPLUSPLUS_DEPEND_FLAG = $(CC_DEPEND_FLAG)

export CC_WARNINGS = -Wall
export CPLUSPLUS_WARNINGS = -Wall

export CC_OPTIMIZATION = -g 
export CPLUSPLUS_OPTIMIZATION = -g

# Doxygen definitions
HAVE_DOXYGEN = true
DOXYGEN = doxygen
DOXYGEN_CONFIG = Doxyfile
DOXYGEN_OUTPUT_DIRECTORY = doc

#===============================================================

WWW_DOCS_SUBDIR := DGCdocs
DOXYGEN_CONFIG = /mnt/dgc-lab/yam/www/$(WWW_DOCS_SUBDIR)/Doxyfile-generic

export TAGFILES_EXPANDED = $(foreach file, $(DOXYGEN_TAGFILES), $(YAM_ROOT)/doc/$(file)/doxy-$(file).tag=$(YAM_ROOT)/src/$(file)/doc/doxygen/html )

WWW_ROOT = /mnt/dgc-lab/yam/www
export DOXYGEN_DOCS_DIR := $(WWW_ROOT)/$(WWW_DOCS_SUBDIR)
export DOXYGEN_IMAGE_DIR := $(WWW_ROOT)/images/doxygen
export WWW_URL = http://gc.caltech.edu/yam/$(WWW_DOCS_SUBDIR)

INSTALLDOX_STR = $(foreach file, $(DOXYGEN_TAGFILES), -l doxy-$(file).tag@$(WWW_URL)/modules/$(file)/html )
export DOXYGEN_OUTPUT_DIRECTORY ?= $(YAM_ROOT)/src/$(MODULE_NAME)/doc/doxygen

