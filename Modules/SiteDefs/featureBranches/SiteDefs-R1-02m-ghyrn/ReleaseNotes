              Release Notes for "SiteDefs" module

Release R1-02m (Tue May 15 16:25:52 2007):
	Added new 'remote' site that can be used when compiling off of the
	gclab or gcfield clusters.  This is intended for laptop users.

	Also changed the default compile host to be the local host you are
	running on, not paris (which doesn't have any memory any more) or
	other machines in the lab.  This should keep from slowing down other
	people's machines.

Release R1-02l (Sat May 12 22:07:40 2007):
	Updated i386-darwin target to include X11R6 paths.  This allows
	compiling against gl and glu without having to mess with
	Makefile.yam.

	Changed the path to doxygen files to /dgc/yam (from
	/mnt/dgc-lab/yam, which is not portable).

	Note to OS X users: This release allows you put files required for
	compilation of the dgc software in the directory /dgc/yam and
	everything should work.  The files required (including compiled
	versions of the OTG library are available in /dgc/macosx on gclab).

Release R1-02k (Fri May  4 10:05:12 2007):
	Changed the background compilation to use paris instead of rome.
	Rome has two screens on it, so it is used more often than paris
	(which only has one).

Release R1-02j (Sat Apr 28 18:39:06 2007):
	Changed background compilation to use rome instead of gclab.  This
	should help keep the load average on gclab down, which will speed up
	file access.

Release R1-02h (Sat Apr 21 10:21:05 2007):
	Updated i386-darwin target (Mac OS X) to use libtool instead of
	ranlib.  This fixes problems with out of date indexes in library
	files.  No change to anyone not using OS X.

Release R1-02g (Wed Apr 11 22:51:17 2007):
	Support for generation of doxygen documentation.  To 
	generate the Doxygen tag file run "ymk docs" within a module. To 
	install the documentation run "ymk install-doxygen-docs".

Release R1-02b (Mon Feb  5  7:10:49 2007):
	This release fixes a small bug that Laura discovered in the
	shared.mk file. I had a typo that caused dependencies to not get
	properly generated (and spurious .o files to pop up as well).  

Release R1-02a (Sat Feb  3 22:22:30 2007):
	This release of the SiteDefs module sets some additional flags for
	C/C++ compilation.  First, the -MM flag is set for computing the
	dependencies (this was not working before).  Second, the -Wall and
	-g flags are now turned on by default, to aid in debugging.  To
	reset these, you can set the CPLUSPLUS_WARNINGS variable or the
	CPLUSPLUS_OPTIMIZATION variable in your Makefile.yam file.

	Since this formally requires a recompile in order to take effect,
	bumping the revision number (R1-01g -> R1-02a).

Release R1-01g (Sat Jan 27 17:42:09 2007):
	This release of the SiteDefs module installs the files necessary
	for Drun and also enables doxygen to be used.  There is still
	some tuning to be done for doxygen, but if you create a 'Doxygen'
	file in your source code directory and type 'ymk docs', then
	you should get compiled output.

	TBD: need to make sure the doxygen output is going to the right
	place and figure out how to do cross module linking.

Release R1-01f (Mon Jan 22 21:59:47 2007):
	This release of SiteDefs properly sets up the makefile to use
	gcc for C source (isntead of g++).

Release R1-01e (Sat Jan 13 20:01:51 2007):
	This release of SiteDefs includes support for OS X, identifed
	by yamNative.sh as i386-darwin.  The default config file on gc 
	includes this target as part of the 'lab' site.  No change in
	usage for non-OSX targets.

	This version has been tested on OSX by compiling the simulator
	and then running this with (old) versions of trajFollow and the
	gui to verify proper operation.

Release R1-01d (Wed Jan 10 23:06:17 2007):
	Added compiler definitions for all sites (lab, alice).  This
	version has been tested by compiling adrive against skynet,
	sparrow, dgcutils and interfaces.

Release R1-01c (Mon Jan  8 13:49:44 2007):

Release R1-01b (Mon Jan  8 13:43:40 2007):
	Added rules for detecting RHEL4, gentoo

Release R1-00 (Sat Jan  6 16:32:07 2007):
	Created.





