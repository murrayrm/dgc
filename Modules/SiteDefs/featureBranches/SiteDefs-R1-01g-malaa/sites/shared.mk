########################################################################
#
#      !!!!!! EDIT & CUSTOMIZE THIS FILE !!!!!!
# 
########################################################################
#
# Defines build flags common to all the sites. The site specific
# files can override these settings.
# This file is typically included by the site specific site.local file.

# the following are examples of settings. Customize appropriately.
export CC=gcc
export CPLUSPLUS=g++

# Doxygen definitions
HAVE_DOXYGEN = true
DOXYGEN = doxygen
DOXYGEN_CONFIG = Doxyfile
DOXYGEN_OUTPUT_DIRECTORY = doc
