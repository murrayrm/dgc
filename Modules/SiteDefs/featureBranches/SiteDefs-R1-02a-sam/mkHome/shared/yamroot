#!/bin/sh
#
# yamroot - script to determine YAM_ROOT starting from a directory
#           or executable, by looking for YAM.config in parent directories

# check command line arguments
dir=""
if [ "$#" = "0" ]; then
    dir=`pwd`
elif [ "$#" = "1" ]; then
    if test -d $1; then
        dir=$1

        # prepend current working directory if necessary
        if [ `echo $dir | sed s@\^/@@` = "$dir" ]; then
            dir="`pwd`/$dir"
        fi
    else
        dir=`locate $1`
        if [ $? -ne 0 ]; then
            echo "Error: could not locate $1"
            exit 1
        fi
    fi
else
    echo "usage: `basename $0` [ <directory> | <executable> ]"
    exit 2
fi

# see if environment variable is already set
if [ "X$YAM_ROOT" != "X" ]; then
    echo $YAM_ROOT
    exit 0
fi

# see if leading /tmp_mnt or /export should be stripped from path
strip_tmp=0;
if [ "$YAM_NATIVE" = "sparc-sunos4" ]; then
    strip_tmp=1;
fi

# search for YAM.config
ndir=$dir
while [ "$ndir" != "/" ]; do
    if test -f "$ndir/YAM.config"; then
        # clean up directory name
        cd $ndir
        if [ $? -ne 0 ]; then echo "$ndir: not found"; exit 1; fi
        if [ $strip_tmp = 1 ]; then
          ndir=`pwd | sed -e 's@^/tmp_mnt@@' -e 's@^/export@@'`
	else
          ndir=`pwd`
	fi
        # all done
        echo $ndir
        exit 0
    else
        ndir=`dirname $ndir`
    fi
done

# no YAM.config was found
echo "Error: no YAM.config was found in or above $dir"
exit 1
