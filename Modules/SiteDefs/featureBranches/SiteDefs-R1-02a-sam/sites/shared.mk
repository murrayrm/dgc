########################################################################
#
#      !!!!!! EDIT & CUSTOMIZE THIS FILE !!!!!!
# 
########################################################################
#
# Defines build flags common to all the sites. The site specific
# files can override these settings.
# This file is typically included by the site specific site.local file.

# C compiler settings
export CC=gcc
export CPLUSPLUS=g++

export CC_DEPEND_FLAG = -MM
export CPLUSPLUS_DEPENDFLAG = $(CC_DEPEND_FLAG)

export CC_WARNINGS = -Wall
export CPLUSPLUS_WARNINGS = -Wall

export CC_OPTIMIZATION = -g 
export CPLUSPLUS_OPTIMIZATION = -g

# Doxygen definitions
HAVE_DOXYGEN = true
DOXYGEN = doxygen
DOXYGEN_CONFIG = Doxyfile
DOXYGEN_OUTPUT_DIRECTORY = doc
