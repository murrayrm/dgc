# This file is included by the top-level Makefile. It is meant to contain
# rules and definitions unique for a YaM installation

# rules for both work and link modules
YAM_LINKMOD_RULES += install-doxygen-docs setup-doxygen-docs

# rules for only work modules
#YAM_BUILD_RULES += links depends libs libsso bins
#YAM_WORKMOD_RULES += $(YAM_BUILD_RULES) clean docs
YAM_WORKMOD_RULES += moddeps doxfiles

# define the "strip" command 
ifeq ($(YAM_NATIVE),i486-linux)
  STRIP = strip -s
else
  STRIP = strip
endif

#=======================================================
# set environment variables for running regtest regression tests
# regression test output file
export OELTEST_REPORT = $(YAM_ROOT)/report.regtest
# concatenate all the module regression test reports
export APPEND_OELTEST_REPORT = 1

#=======================================================
supp-map::
	@$(MAKE) -i --no-print-directory QUIET=1 RULE=$@ \
		$(foreach t, $(YAM_TARGETS), allmods-rule-target-$(t) )


regtest:: regtest-clean

regtest:: 
        ifneq ($(YAM_TARGET),$(REGRESSION_TEST_YAM_TARGET))
	  @echo ""
	  @echo "    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	  @echo "    ! WARNING: The regression tests are meant to be !"
	  @echo "    !          run on '$(REGRESSION_TEST_YAM_TARGET)' platforms -  !"
	  @echo "    !          NOT the '$(YAM_TARGET)' platform        !"
	  @echo "    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	  @echo ""
        endif

regtest-clean:
	rm -f $(OELTEST_REPORT)

# a generic help message
help::
	@echo ""
	@echo "This top-level makefile provides a convenient way to loop "
	@echo "  thru all the modules in the sandbox and build the target rule"
	@echo "  for all of them. The available rules are:"
	@echo ""
	@echo "      mklinks: Export links for all link/work modules"
	@echo "      rmlinks: Remove exported links for all link/work modules"
	@echo "        links: Build the 'links' rule for all work modules"
	@echo "      depends: Build the 'depends' rule for all work modules"
	@echo "         docs: Build the 'docs' rule for all work modules"
	@echo "         libs: Build the 'libs' rule for all work modules"
	@echo "       libsso: Build the 'libsso' rule for all work modules"
	@echo "         bins: Build the 'bins' rule for all work modules"
	@echo "        clean: Build the 'clean' rule for all work modules"
	@echo "      regtest: Run available rgression tests all work modules"
	@echo "     supp-map: Build the 'supp-map' rule for all work modules"
	@echo ""
	@echo "          all: Build the 'all' rule for all work modules"
	@echo "        build: Build the 'build' rule for all work modules"
	@echo ""
	@echo "The list of link/work modules is normally derived from the "
	@echo "WORK_MODULES and LINK_MODULES settings in YAM.config."
	@echo "However to use only a subset of the modules you can set"
	@echo "the 'MODULES' variable to the list of modules as part of the"
	@echo "make command line. The corresponding sublist of link and work"
	@echo "modules will be extracted and used."
	@echo ""
	@echo "The following 'alltgt' rules additionally allow the building"
	@echo "  of rules for all the modules for all supported targets."
	@echo "  The ALLTGT variable can be used to restrict the 'alltgt' targets."
	@echo ""
	@echo "       alltgt-<xxx>: Build the 'xxx' rule for all modules"
	@echo ""
	@echo "Other available utility rules are: "
	@echo ""
	@echo "          help: Generate this message"
	@echo "   clean-links: Delete the top-level bin, doc, etc, include, lib directories"
	@echo "        config: Returns the branch names for all work modules"
	@echo "    cvs-update: Runs 'cvs update' for all the work modules"
	@echo "      cvscheck: Runs 'cvscheck' for all the work modules"
	@echo " release-diffs: Runs 'yam diff' for all the work modules"
	@echo "       rshtest: Check for access to the remote build hosts"
	@echo ""
	@echo "Recognized Unix targets: $(unix_targets)"
	@echo "Recognized VxWorks targets: $(vx_targets)"
	@echo ""
