#!/bin/sh
#
# getYamNative - script to determine YAM_NATIVE.

if [ "X$YAM_NATIVE" = "X" ]; then
    os=`uname -s`
    osver=`uname -r`
    case $os in
	SunOS)
	    case $osver in
		4*) YAM_NATIVE=sparc-sunos4 ;;
		5.6) YAM_NATIVE=sparc-sunos5.6 ;;
		5.7) YAM_NATIVE=sparc-sunos5.7 ;;
		5.8) YAM_NATIVE=sparc-sunos5.8 ;;
		5.9) YAM_NATIVE=sparc-sunos5.9 ;;
		5*) YAM_NATIVE=sparc-sunos5 ;;
		*) YAM_NATIVE=unknown ; echo "The \"$os\" host OS is not supported." ;;
	    esac
	    ;;
	Linux)
            # Try to sort out the flavor of linux that we are using
            # If possible, just use the osver string
	    case $osver in
		*-gentoo-*) YAM_NATIVE=i486-gentoo-linux ;;
		*) YAM_NATIVE=unknown ;;
	    esac

  	    # Additioanl checks to see if we can figure things out
   	    if [ -e /etc/fedora-release ]; then
	        # Some flavor of fedora linux; look at release file for details
		YAM_NATIVE=i486-rh9-linux
		str=`cat /etc/fedora-release | sed -n -e 's/Fedora Core release //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		case $str in
		    1) YAM_NATIVE=i486-fedora1-linux ;;
		    2) YAM_NATIVE=i486-fedora2-linux ;;
		    '') case $str2 in
			'') YAM_NATIVE=UNKNOWN_LINUX;;
		        esac ;;
	            *) YAM_NATIVE=i486-fedora${str}-linux
                esac

	    elif [ -e /etc/redhat-release ]; then
	        # Some flavor of Red Hat linux; look at release file for details
		str=`cat /etc/redhat-release | sed -n -e 's/Red Hat Linux release //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		str2=`cat /etc/redhat-release | sed -n -e 's/Red Hat Enterprise Linux AS release //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		case $str in
		    72*) YAM_NATIVE=i486-linux ;;
		    73*) YAM_NATIVE=i486-linux ;;
		    '') case $str2 in
			'') YAM_NATIVE=UNKNOWN_LINUX;;
		         4) YAM_NATIVE=i486-rhel4-linux;;
		         *) YAM_NATIVE=i486-rh9-linux
	                esac ;;
		    *) YAM_NATIVE=i486-rh${str}-linux
	        esac
	    elif [ -d /usr/portage ]; then
	        # GENTOO!!!!
		YAM_NATIVE=i486-gentoo-linux
	    else
		if [ -e /etc/SuSE-release ]; then
		    str=`cat /etc/SuSE-release | sed -n -e 's/SuSE Linux //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		    case $str in
		        90*) YAM_NATIVE=i486-rh9-linux ;;
		        *) YAM_NATIVE=i486-suse${str}-linux
  		    esac
		fi
	    fi
	    ;;
    HP-UX)
      case $osver in
        A.09*) YAM_NATIVE=hppa-hpux9 ;;
        B.10*) YAM_NATIVE=hppa-hpux10 ;;
        *) YAM_NATIVE=unknown ; echo "The \"$os\" host OS is not supported." ;;
      esac
      ;;

    Darwin)
      YAM_NATIVE=`uname -m`-darwin
      ;;
  esac
fi

echo $YAM_NATIVE
