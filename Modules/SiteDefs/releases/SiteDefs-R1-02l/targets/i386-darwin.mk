########################################################################
#
#      !!!!!! EDIT & CUSTOMIZE THIS FILE !!!!!!
# 
########################################################################
#
# Defines site independent, but target specific build variables
#
# This file is included by overall.mk

CC_COMPILE_FLAGS += -DMACOSX -I/sw/include -I/usr/X11R6/include
CPLUSPLUS_COMPILE_FLAGS += -DMACOSX -I/sw/include -I/usr/X11R6/include
CPLUSPLUS_STATIC_LINK_FLAGS += -L/sw/lib  -L/usr/X11R6/lib -bind_at_load

AR = libtool
AR_FLAGS = -static -o
