#!/bin/sh
#
# getYamNative - script to determine YAM_NATIVE.

if [ "X$YAM_NATIVE" = "X" ]; then
  os=`uname -s`
  osver=`uname -r`
  case $os in
    SunOS)
      case $osver in
        4*) YAM_NATIVE=sparc-sunos4 ;;
        5.6) YAM_NATIVE=sparc-sunos5.6 ;;
        5.7) YAM_NATIVE=sparc-sunos5.7 ;;
        5.8) YAM_NATIVE=sparc-sunos5.8 ;;
        5.9) YAM_NATIVE=sparc-sunos5.9 ;;
        5*) YAM_NATIVE=sparc-sunos5 ;;
        *) YAM_NATIVE=unknown ; echo "The \"$os\" host OS is not supported." ;;
      esac
      ;;
    IRIX64)
      case $osver in
        6.5*) YAM_NATIVE=mips-irix6.5 ;;
        6*) YAM_NATIVE=mips-irix5 ;;
        *) YAM_NATIVE=unknown ; echo "The \"$os\" host OS is not supported." ;;
      esac
      ;;
    CYGWIN_NT-5.0)
       YAM_NATIVE=i486-cygwin
      ;;
    Linux)
		if [ -e /etc/fedora-release ]; then
		   YAM_NATIVE=i486-rh9-linux
		   str=`cat /etc/fedora-release | sed -n -e 's/Fedora Core release //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		   case $str in
			1) YAM_NATIVE=i486-fedora1-linux ;;
			2) YAM_NATIVE=i486-fedora2-linux ;;
			'') case $str2 in
			      '') YAM_NATIVE=UNKNOWN_LINUX;;
			    esac ;;
			*) YAM_NATIVE=i486-fedora${str}-linux
		   esac
		elif [ -e /etc/redhat-release ]; then
 	    #      str=`cat /etc/redhat-release | `sed -n -e 's/^.*-> \(.*\)/\1/p'`
		   str=`cat /etc/redhat-release | sed -n -e 's/Red Hat Linux release //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		   str2=`cat /etc/redhat-release | sed -n -e 's/Red Hat Enterprise Linux WS release //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
		   case $str in
			72*) YAM_NATIVE=i486-linux ;;
			73*) YAM_NATIVE=i486-linux ;;
			'') case $str2 in
			      '') YAM_NATIVE=UNKNOWN_LINUX;;
			       *) YAM_NATIVE=i486-rh9-linux
			    esac ;;
			*) YAM_NATIVE=i486-rh${str}-linux
		   esac
	        else
			if [ -e /etc/SuSE-release ]; then
			str=`cat /etc/SuSE-release | sed -n -e 's/SuSE Linux //p' | sed -n -e 's/ \(.*\)//p' | sed -e 's/\.//'`
			case $str in
#				73*) YAM_NATIVE=i486-suse-linux ;;
# SuSE 9.0 can use Redhat 9
				90*) YAM_NATIVE=i486-rh9-linux ;;
				*) YAM_NATIVE=i486-suse${str}-linux
			esac

			else
				YAM_NATIVE=UNKNOWN_LINUX
			fi
	    fi
      ;;
    HP-UX)
      case $osver in
        A.09*) YAM_NATIVE=hppa-hpux9 ;;
        B.10*) YAM_NATIVE=hppa-hpux10 ;;
        *) YAM_NATIVE=unknown ; echo "The \"$os\" host OS is not supported." ;;
      esac
      ;;
  esac
fi

echo $YAM_NATIVE












