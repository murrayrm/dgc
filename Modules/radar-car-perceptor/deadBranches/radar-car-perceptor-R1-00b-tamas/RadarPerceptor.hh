/*!
 *
 * \file RadarPerceptor.hh
 * \brief Header file for Radar Car Perceptor
 * 
 * \author Tamas Szalay
 * \date 13 Jul 2007
 * 
 *     (shamelessly ripped from Sam's template-ladarperceptor) 
 *********************************************************/


#ifndef RADARCARPERCEPTOR_H
#define RADARCARPERCEPTOR_H

using namespace std;

#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>
#include <frames/pose3.h>
#include <frames/coords.hh>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

//openGL support
//#include <GL/glut.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/RadarRangeBlob.h>
#include <interfaces/ProcessState.h>
#include "dgcutils/DGCutils.hh" 
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "interfaces/MapElementMsg.h"

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"


#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

//#define MAXTRAVEL 2 //max dist obj can travel btwn frames

//#define MAXNUMCARS 50 //max num cars we can track
//#define MAXNUMOBJS 100

#define VEL_THRESHOLD 0.5   // velocities above this are cars, below are obstacles

#define MAPEL_POINTS 8 //number of points in a map element polygon
#define MAPEL_SIZE 1.0 //radius of element

//for accessing the rawData array
//#define ANGLE 0
//#define RANGE 1

//maximum number of radars
#define MAX_RADARS 2

//minimum threshold to consider object an element
#define MIN_CREDIBILITY 8
//credibility at which probability of existence is 50%
#define MID_CREDIBILITY 18
//credibility at which we are certain it exists
#define MAX_CREDIBILITY 40
//(actual maximum credibility is 63)


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class RadarPerceptor :public CMapElementTalker
{
  
public:
   // Constructor
  RadarPerceptor();

  // Destructor
  ~RadarPerceptor();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Update the map
  int update();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Update the process state
  int updateProcessState();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, RadarPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, RadarPerceptor *self, const char *token);

 // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
 
  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet replay module
  sensnet_replay_t *replay;
 
  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  int useDisplay;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //how many times we've looped in the main program
  int loopCount;

  //one second ago, for cap rate
  uint64_t lastTickTime;

  //number of updates this second
  int capsThisSec;

  // Health status
  int healthStatus;

  // Individual radar data
  struct Radar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numRadars;
  Radar radars[MAX_RADARS];

  bool send_debug;
  /************ defines from trackMO ***************/

  //angle, range data
  //  double rawData[NUMSCANPOINTS][2];
  //differences in RANGES between consecutive scans
  //double diffs[NUMSCANPOINTS-1];
  //x,y,z data (in local frame)
  //double xyzData[NUMSCANPOINTS+1][3];
  //x,y,z data in local frame, for points 1m radially
  //behind the actual return
  //double depthData[NUMSCANPOINTS+1][3];


  int subgroup; //FIXME: check w/ sam what this value should be


  //stuff for display
  
};
#endif


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


