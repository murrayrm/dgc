/*!
 * \file RadarPerceptor.cc
 * \brief Car perceptor for radar unit
 *
 * \author Tamas Szalay
 * \date 13 Jul 2007
 *
 *   (shamelessly ripped from Sam's template-ladarperceptor)
 *********************************************************
 * The radar data is read through sensnet and essentially
 * forwarded to the map. No internal tracking or history data
 * is stored between scans, since the radar tracks internally
 *    (this may change in the future)
 *********************************************************/

#include "RadarPerceptor.hh"

// Default constructor
RadarPerceptor::RadarPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
RadarPerceptor::~RadarPerceptor()
{
}


// Parse the command line
int RadarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
  else
    this->mode = modeLive;
  
  return 0;
}



// Initialize sensnet
int RadarPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[MAX_RADARS];
  Radar *radar;

  if (this->mode == modeLive)
  {
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return -1;
  }
  else
  {
    // Create replay interface
    this->replay = sensnet_replay_alloc();
    assert(this->replay);    
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  numSensorIds = 0;
  sensorIds[numSensorIds++] = SENSNET_RADAR;

  // Initialize radar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numRadars < sizeof(this->radars) / sizeof(this->radars[0]));
    radar = this->radars + this->numRadars++;

    // Initialize radar data
    radar->sensorId = sensorIds[i];

    // Join the radar data group
    if (this->sensnet)
    {
      if (sensnet_join(this->sensnet, radar->sensorId,
                       SENSNET_RADAR_BLOB, sizeof(RadarRangeBlob)) != 0)
        return ERROR("unable to join %d", radar->sensorId);
    }
  }

  initSendMapElement(skynetKey);

  return 0;
}


// Clean up sensnet
int RadarPerceptor::finiSensnet()
{
  if (this->sensnet)
  {
    int i;
    Radar *radar;
    for (i = 0; i < this->numRadars; i++)
    {
      radar = this->radars + i;
      sensnet_leave(this->sensnet, radar->sensorId, SENSNET_RADAR_BLOB);
    }
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Update the map with new range data
int RadarPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Radar *radar;
  int blobId, blobLen;
  RadarRangeBlob blob;

  // Why is this necessary? AH
  // usleep(100000);

  if (this->sensnet)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else if (this->replay)
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }
  
  for (i = 0; i < this->numRadars; i++)
  {
    radar = this->radars + i;

    if (this->sensnet)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, radar->sensorId,
                       SENSNET_RADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == radar->blobId || blobId <0 )
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, radar->sensorId,
                       SENSNET_RADAR_BLOB, &radar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else if (this->replay)
    {
      // Advance log one step
      if (sensnet_replay_next(this->replay, 0) != 0)
        return -1;
 
      // Read new blob
      if (sensnet_replay_read(this->replay, radar->sensorId,
                              SENSNET_RADAR_BLOB, &radar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

    fprintf(stderr, "got data %s %d %d %lld\n",
            sensnet_id_to_name(radar->sensorId), radar->blobId,
            blob.scanId, blob.timestamp);
    
    RadarRangeBlobObject *obj;
    float pa, pr, px, py, pz;
    float vx, vy, vz;
    float ax, ay, az, bx, by, bz;

    int trackCount = 0;

    // Only consider track data for now, ignore targets
    for (int j=0; j< RADAR_BLOB_MAX_TRACKS; j++) 
    {
      obj = &blob.tracks[j];
      
      pa = obj->yaw;
      pr = obj->range;

      // Get points in sensor frame
      RadarRangeBlobScanToSensor(&blob, pa, pr, &px, &py, &pz);

      // Transform them to vehicle frame
      RadarRangeBlobSensorToVehicle(&blob, px, py, pz, &vx, &vy, &vz);

      // Track location in local frame
      RadarRangeBlobVehicleToLocal(&blob, vx, vy, vz, &ax, &ay, &az);

      // Velocity vector in the local frame
      // Trick adapted from sensviewer
      vx += obj->velocity;
      RadarRangeBlobVehicleToLocal(&blob, vx, vy, vz, &bx, &by, &bz);
      // Then subtract out actual target position
      bx -= ax;
      by -= ay;
      bz -= az;

      MapElement el;
      point2arr ptarr;
      point2 p;

      // Create polygon centered at origin
      for (int k=0; k<MAPEL_POINTS; k++) {
	p.x = ax+ cos((2*M_PI*k)/(MAPEL_POINTS-1))*MAPEL_SIZE;
	p.y = ay+sin((2*M_PI*k)/(MAPEL_POINTS-1))*MAPEL_SIZE;
	ptarr.push_back(p);
      }

      el.clear();
      el.setId(MODradarObsPerceptor,i,j);

      //el.setPosition(ax,ay);
      el.velocity = point2(vx, vy);
      // TODO: Use velocity here
      
      if (obj->status == 0 || obj->credibility <= MIN_CREDIBILITY) {
	el.setTypeClear();
      }
      else {
	char token[64];
	snprintf(token, sizeof(token), "%%track%d%%", trackCount);
	cotk_printf(this->console, token, A_NORMAL, "Track %d Credibility: %d  ",
                  trackCount, obj->credibility);
	trackCount++;
	if (options.set_type_vehicle_flag && fabs(obj->velocity)>VEL_THRESHOLD)
	  el.setTypeVehicle();
	else
	  el.setTypeObstacle();
      }
       
      el.setState(blob.state);
      el.height = 1;

      el.setGeometry(ptarr);
      el.plotColor = MAP_COLOR_YELLOW;
      el.setFrameTypeLocal();
      
      sendMapElement(&el,0);      
      if (send_debug){
        sendMapElement(&el,-2);      
      }
    }

    for (int j=trackCount;j<RADAR_BLOB_MAX_TRACKS;j++) {
      char token[64];
      snprintf(token, sizeof(token), "%%track%d%%", j);
      cotk_printf(this->console, token, A_NORMAL, "                          ");
    }

    //transformed to sensor frame
    /*    float sfx, sfy, sfz; //sensor frame vars
    RadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1], &sfx, &sfy, &sfz);
    //    fprintf(stderr, "middle range transformed to sensor frame (x,y,z): %f, %f, %f \n", sfx, sfy, sfz);

    //transformed to vehicle frame
    float vfx, vfy, vfz; //vehicle frame vars
    LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    //    fprintf(stderr, "middle range transformed to vehicle frame (x,y,z): %f, %f, %f \n", vfx, vfy, vfz);

    //transformed to local frame
    float lfx, lfy, lfz; //local frame vars
    LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
    //    fprintf(stderr, "middle range transformed to local frame (x,y,z): %f, %f, %f \n", lfx, lfy, lfz);

    //coyping the data to a arrays accessible by any 
    //function in the program. Seems inefficient....
    double xpts[NUMSCANPOINTS];
    double ypts[NUMSCANPOINTS];
    double zpts[NUMSCANPOINTS];

    int numReturns = 0;
    point2arr ptarr;
    //    point2arr ptarrextra;
    ptarr.clear();
    // ptarrextra.clear();
    point2 pt;

    int edgeoffset = 6;
      
    for(int j=edgeoffset; j < NUMSCANPOINTS-edgeoffset; j++) 
    {
      rawData[j][ANGLE] = blob.points[j][ANGLE];
      rawData[j][RANGE] = blob.points[j][RANGE];
      LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
      xyzData[j][0] = lfx;
      xyzData[j][1] = lfy;
      xyzData[j][2] = lfz;

     
      //if this isn't a no-return point, send to display
      if(rawData[j][1] < 80 && rawData[j][1]>0)
      {

        if (ladar->sensorId == SENSNET_MF_BUMPER_LADAR){
          if (j<60 || j>(NUMSCANPOINTS-60)){
            if (fabs(lfy) >2){
              continue;
            }
          }
        }
        
        //SAM
        pt.set(lfx,lfy);


        ptarr.push_back(pt);

        //SAM

        xpts[numReturns]=lfx;
        ypts[numReturns]=lfy;
        zpts[numReturns]=lfz;
        numReturns++;
      }

    }

    //for plotting the segmentation
    LadarRangeBlobScanToSensor(&blob, 0.0, 0.0, &sfx, &sfy, &sfz);
    LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
    LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
   


    //SAM
    MapElement el;
    point2arr tmpptarr;
    vector<point2arr> ptarrarr;

    ptarrarr=  ptarr.split(2);
    point2 tmppt1,tmppt2, newpt, dpt;
    point2 origin(lfx,lfy);
    double offset;
    int thissize;
7
    int sendsize = ptarrarr.size();
    for (int k=0;k<sendsize;++k){
      tmpptarr = ptarrarr[k];
        
        continue;
      }
        
      el.height=tmpptarr.size(); 

      

      //      tmpptarr=tmpptarr.get_bound_box();      
      thissize = tmpptarr.size();
      
      tmppt1 = tmpptarr[0];
      tmppt2 = tmpptarr[thissize-1];
      
      offset = 2;
      if (tmppt1.dist(tmppt2) <offset)
        offset = tmppt1.dist(tmppt2);

      // cout << "offset = " << offset <<endl;
      if (offset<.1){
        offset = .1;
      }

        dpt = tmppt1-origin;
        
        dpt = dpt/dpt.norm();
        dpt = dpt*offset;
        newpt = tmppt1+dpt;
        tmpptarr.insert(0,newpt);
        
        
        dpt = tmppt2-origin;
        dpt = dpt/dpt.norm();
        dpt = dpt*offset;
        newpt = tmppt2+dpt;
        tmpptarr.push_back(newpt);
      
      
        


    }
    if (sendsize<lastSentSize[i]){
      for (int k=sendsize;k<lastSentSize[i]+1;++k){
        el.clear();
        el.setId(100,i,k);
        el.setTypeClear();
        sendMapElement(&el,0);      
        if(send_debug){
          sendMapElement(&el,-2);
        }      
        
      }
    }
    lastSentSize[i] = ptarrarr.size();*/

//     for (unsigned k = ptarrarr.size();k<elcount[i];k++){
//       el.clear();
//       el.type=ELEMENT_CLEAR;
//       el.set_id(-i-10,(int)k);
//       sendMapElement(&el,0);      
//       sendMapElement(&el,-2);      
      
//     }
    
   
//     elcount[i]=ptarrarr.size();
    //TODO: check if this runs fast enough to not be limiting factor
    //    fprintf(stderr, "calling processScan() \n");

    if (this->console)
    {
      char token[64];
      snprintf(token, sizeof(token), "%%radar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(radar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%tracks%", A_NORMAL, "%d", trackCount);
    }
  } //end cycling through radars

  return 0;
} //end update()


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"radar-car-perceptor $Revision$                                             \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Radar[0]: %radar0%                                                         \n"
"Radar[1]: %radar1%                                                         \n"
"                                                                           \n"
"Track count: %tracks%                                                      \n"
"%track0%                                                                   \n"
"%track1%                                                                   \n"
"%track2%                                                                   \n"
"%track3%                                                                   \n"
"%track4%                                                                   \n"
"%track5%                                                                   \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int RadarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int RadarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int RadarPerceptor::onUserQuit(cotk_t *console, RadarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RadarPerceptor::onUserPause(cotk_t *console, RadarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  RadarPerceptor *percept;
  
  percept = new RadarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  percept->send_debug = percept->options.send_debug_flag;
  if (percept->send_debug){
    MSG("Sending data to debugging channel -2");
  }else{
    MSG("Debugging map element messages off");
  }

  MSG("entering main thread of RadarPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Get new data and update our perceptor
    if (percept->update() != 0)
      break;
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}

