/*!
 * \file RadarPerceptor.cc
 * \brief Car perceptor for radar unit
 *
 * \author Tamas Szalay
 * \date 13 Jul 2007
 *
 *   (shamelessly ripped from Sam's template-ladarperceptor)
 *********************************************************
 * The radar data is read through sensnet and essentially
 * forwarded to the map. No internal tracking or history data
 * is stored between scans, since the radar tracks internally
 *    (this may change in the future)
 *********************************************************/

#include "RadarPerceptor.hh"

// Default constructor
RadarPerceptor::RadarPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
RadarPerceptor::~RadarPerceptor()
{
}


// Parse the command line
int RadarPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
  else
    this->mode = modeLive;
  
  return 0;
}


// Initialize sensnet
int RadarPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[MAX_RADARS];
  Radar *radar;

  if (this->mode == modeLive)
  {
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return -1;
  }
  else
  {
    // Create replay interface
    this->replay = sensnet_replay_alloc();
    assert(this->replay);    
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  numSensorIds = 0;
  sensorIds[numSensorIds++] = SENSNET_RADAR;
  sensorIds[numSensorIds++] = SENSNET_PTU_RADAR;

  // Initialize radar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numRadars < sizeof(this->radars) / sizeof(this->radars[0]));
    radar = this->radars + this->numRadars++;

    // Initialize radar data
    radar->sensorId = sensorIds[i];

    // Join the radar data group
    if (this->sensnet)
    {
      if (sensnet_join(this->sensnet, radar->sensorId,
                       SENSNET_RADAR_BLOB, sizeof(RadarRangeBlob)) != 0)
        return ERROR("unable to join %d", radar->sensorId);
    }
  }

  initSendMapElement(skynetKey);

  return 0;
}


// Clean up sensnet
int RadarPerceptor::finiSensnet()
{
  sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  if (this->sensnet)
  {
    int i;
    Radar *radar;
    for (i = 0; i < this->numRadars; i++)
    {
      radar = this->radars + i;
      sensnet_leave(this->sensnet, radar->sensorId, SENSNET_RADAR_BLOB);
    }
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Update the map with new range data
int RadarPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Radar *radar;
  int blobId, blobLen;
  RadarRangeBlob blob;
  char token[32];

  // Why is this necessary? AH
  // usleep(100000);

  if (this->sensnet)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else if (this->replay)
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }
  
  for (i = 0; i < this->numRadars; i++)
  {
    radar = this->radars + i;

    if (this->sensnet)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, radar->sensorId,
                       SENSNET_RADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == radar->blobId || blobId <0 )
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, radar->sensorId,
                       SENSNET_RADAR_BLOB, &radar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else if (this->replay)
    {
      // Advance log one step
      if (sensnet_replay_next(this->replay, 0) != 0)
        return -1;
 
      // Read new blob
      if (sensnet_replay_read(this->replay, radar->sensorId,
                              SENSNET_RADAR_BLOB, &radar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

    fprintf(stderr, "got data %s %d %d %lld\n",
            sensnet_id_to_name(radar->sensorId), radar->blobId,
            blob.scanId, blob.timestamp);
    
    RadarRangeBlobObject *obj;
    float pa, pr, px, py, pz;
    float vx, vy, vz;
    float ax, ay, az, bx, by, bz;
    bool isDead;

    int trackCount = 0;

    MapElement el;
    MapElement elVel;
    point2arr ptarr;
    point2 p;
    
    el.setFrameTypeLocal();
    el.setState(blob.state);
    el.height = 1;

    elVel.setFrameTypeLocal();
    elVel.plotColor = MAP_COLOR_RED;

    // Only consider track data for now, ignore targets
    for (int j=0; j< RADAR_BLOB_MAX_TRACKS; j++) 
    {
      obj = &blob.tracks[j];
      
      pa = obj->yaw;
      pr = obj->range;

      // Get points in sensor frame
      RadarRangeBlobScanToSensor(&blob, pa, pr, &px, &py, &pz);

      // Transform them to vehicle frame
      RadarRangeBlobSensorToVehicle(&blob, px, py, pz, &vx, &vy, &vz);

      // Track location in local frame
      RadarRangeBlobVehicleToLocal(&blob, vx, vy, vz, &ax, &ay, &az);

      // Velocity vector added in the vehicle frame
      // Trick adapted from sensviewer
      vx += obj->velocity;
      RadarRangeBlobVehicleToLocal(&blob, vx, vy, vz, &bx, &by, &bz);
      // Then subtract out actual target position
      bx -= ax;
      by -= ay;
      bz -= az;
      // Finally, adjust for Alice's own velocity
      bx += blob.state.localXVel;
      by += blob.state.localYVel;
      bz += blob.state.localZVel;

      el.velocity = point2(bx, by);
      el.plotColor = MAP_COLOR_YELLOW;

      isDead = (obj->status == 0 || obj->credibility <= MIN_CREDIBILITY);
      if (!this->options.use_stationary_flag) {
	isDead = (isDead || (obj->status & 0x08)); // no stationaries
      }
      if (obj->status & 0x08) { //make stationaries green
	el.plotColor = MAP_COLOR_GREEN;
      }

      // Move polygon to track's position
      if (!isDead) {
	ptarr.clear();
	for (int k=0; k<MAPEL_POINTS; k++) {
	  p.x = ax+cos((2*M_PI*k)/(MAPEL_POINTS-1))*MAPEL_SIZE;
	  p.y = ay+sin((2*M_PI*k)/(MAPEL_POINTS-1))*MAPEL_SIZE;
	  ptarr.push_back(p);
	}
	el.setGeometry(ptarr);
      }
      
      // various criteria for being alive

      if (isDead) {
	el.setTypeClear();
	elVel.setTypeClear();
      }
      else {
	snprintf(token, sizeof(token), "%%track%d%%", trackCount);
	cotk_printf(this->console, token, A_NORMAL, "Track %d Credibility: %d  ",
                  trackCount, obj->credibility);
	trackCount++;
	if (this->options.set_type_vehicle_flag && fabs(obj->velocity)>VEL_THRESHOLD)
	  el.setTypeVehicle();
	else
	  el.setTypeRadarObstacle();

        if (!mapID[i][j]) {//we were just dead
	  mapID[i][j] = nextMapID;
	  nextMapID++;
	}
	elVel.setTypePoly();
      }

      el.setId(MODradarObsPerceptor,i,mapID[i][j]);
      elVel.setId(MODradarObsPerceptor,i+10,mapID[i][j]);
       
      // set confidence of existence, which goes from 0 to 0.5 as credibility goes from
      // MIN_ to MID_, and 0.5 -> 1 as cred goes from MID_ to MAX_ (and is just 1 if
      // cred > MAX_)
      if (obj->credibility > MIN_CREDIBILITY && obj->credibility < MID_CREDIBILITY)
	el.conf = (0.5*(obj->credibility - MIN_CREDIBILITY))/(MID_CREDIBILITY-MIN_CREDIBILITY);
      else if (obj->credibility >= MID_CREDIBILITY && obj->credibility < MAX_CREDIBILITY)
	el.conf = 0.5 + (0.5*(obj->credibility-MID_CREDIBILITY))/(MAX_CREDIBILITY-MID_CREDIBILITY);
      else if (obj->credibility > MAX_CREDIBILITY)
	el.conf = 1;

      if (send_debug) {
	elVel.geometry.clear();
	p.x = ax;
	p.y = ay;
	elVel.geometry.push_back(p);
	p.x = ax+bx;
	p.y = ay+by;
	elVel.geometry.push_back(p);
      }
      
      // check if we need to send
      if (mapID[i][j]) {
	sendMapElement(&el,0);
	if (send_debug){
	  sendMapElement(&el,-7);
	  sendMapElement(&elVel,-7);
	}
	if (isDead) //means we just sent a "clear" message
	  // and can now remove our living status
	  mapID[i][j] = 0;
      }
    }

    for (int j=trackCount;j<RADAR_BLOB_MAX_TRACKS;j++) {
      snprintf(token, sizeof(token), "%%track%d%%", j);
      cotk_printf(this->console, token, A_NORMAL, "                          ");
    }

    //TODO: check if this runs fast enough to not be limiting factor
    //    fprintf(stderr, "calling processScan() \n");

    if (this->console)
    {
      snprintf(token, sizeof(token), "%%radar%d%%", i);
      cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                  sensnet_id_to_name(radar->sensorId),
                  blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%tracks%", A_NORMAL, "%d", trackCount);
    }
  } //end cycling through radars
  
  usleep(0);

  return 0;
} //end update()

// Update the process state
int RadarPerceptor::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.healthStatus = this->healthStatus;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"radar-car-perceptor $Revision$                                             \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Radar[0]: %radar0%                                                         \n"
"Radar[1]: %radar1%                                                         \n"
"Capture Rate: %caprate%                                                    \n"
"Track count: %tracks%                                                      \n"
"%track0%                                                                   \n"
"%track1%                                                                   \n"
"%track2%                                                                   \n"
"%track3%                                                                   \n"
"%track4%                                                                   \n"
"%track5%                                                                   \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int RadarPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int RadarPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int RadarPerceptor::onUserQuit(cotk_t *console, RadarPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RadarPerceptor::onUserPause(cotk_t *console, RadarPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  RadarPerceptor *percept;
  
  percept = new RadarPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  percept->send_debug = percept->options.send_debug_flag;
  if (percept->send_debug){
    MSG("Sending data to debugging channel -2");
  }else{
    MSG("Debugging map element messages off");
  }

  MSG("entering main thread of RadarPerceptor \n");

  percept->loopCount =0;
  percept->lastTickTime = DGCgettime();

  percept->nextMapID = 1;
  
  while (!percept->quit)
  {
    percept->loopCount++;
    percept->capsThisSec++;
    if (DGCgettime()-percept->lastTickTime > 1000000) {
      percept->lastTickTime = DGCgettime();
      cotk_printf(percept->console, "%caprate%", A_NORMAL, "%d", percept->capsThisSec);
      percept->capsThisSec = 0;
    }

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }
    
    if ((percept->capsThisSec % 4) == 0)
      percept->updateProcessState();

    // Get new data and update our perceptor
    if (percept->update() != 0) {
      ERROR("Update failed\n");
      usleep(100000);
    }
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");

  return 0;
}

