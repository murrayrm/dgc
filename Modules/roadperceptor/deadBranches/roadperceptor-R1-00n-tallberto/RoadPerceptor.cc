
/* 
 * Desc: Road perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard, Humberto Pereira
 * CVS: $Id$
*/

//! Unix headers
#include <signal.h>
#include <sys/stat.h>
#include <sys/times.h>

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

//! Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineMsg.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>
#include <dgcutils/DGCutils.hh>


//! CLI support
#include <cotk/cotk.h>

//! Cmd-line handling
#include "cmdline.h"

//! Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"

#include "RoadPerceptor.hh"

int ROIstartX = 50;
int ROIstartY = 50;
int ROIwidth = 540;
int ROIheight = 300;

int ROIhorizonY = 175;

int DEBUG_LINES = 0;

int lanesPreviousSize = 0;
int lanesOutput = 0;

float SizeTol=2; 
float AngleTol=10;
float DistanceTol=12;

static const uint64_t HEARTBEAT_TIME = 2000000L;

//! Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

//! Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


//! Default constructor
RoadPerceptor::RoadPerceptor() {
  memset(this, 0, sizeof(*this));
  
  return;
}


//! Default destructor
RoadPerceptor::~RoadPerceptor() {
  destroyImages();
  return;
}

//! allocate memory for the images
void RoadPerceptor::initImages() {
  //! Stage Images
  
  imBuffer = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  
  imCO0 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCO1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO3 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imC = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);
  imCProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imWhite = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imYellow = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);

  //! Scale
  ROIstartX = ROIstartX * (this->stereoBlob.cols / 640);
  ROIwidth  = ROIwidth  * (this->stereoBlob.cols / 640);

  ROIstartY = ROIstartY * (this->stereoBlob.rows / 480);
  ROIheight = ROIheight * (this->stereoBlob.rows / 480);
  ROIhorizonY = ROIhorizonY * (this->stereoBlob.rows / 480);


  //! Apply Regions Of Interest
  cvGetSubRect(imCO0, imCO0, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imCO1, imCO1, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imCO2, imCO2, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer,  imBuffer,  cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer1, imBuffer1, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer2, imBuffer2, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imProcess, imProcess, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imWhite,  imWhite,  cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imYellow, imYellow, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));

  return;

}

//! deallocate memory for the image
void RoadPerceptor::destroyImages() {
  cvReleaseMat(&imBuffer1);
  cvReleaseMat(&imBuffer2);
  cvReleaseMat(&imBuffer);

  cvReleaseMat(&imC);
  cvReleaseMat(&imCO0);
  cvReleaseMat(&imCO1);
  cvReleaseMat(&imCO2);
  cvReleaseMat(&imCO3);

  cvReleaseMat(&imCProcess);
  cvReleaseMat(&imCOriginal);
  cvReleaseMat(&imProcess);
  cvReleaseMat(&imOriginal);

  cvReleaseMat(&imWhite);
  cvReleaseMat(&imYellow);

  return;

}

void RoadPerceptor::handleProcessControl() {

  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message every HEARTBEAT_TIME usecs
  if (DGCgettime() - this->beatTime > HEARTBEAT_TIME) {
    memset(&response, 0, sizeof(response));  
    response.moduleId = this->moduleId;
    response.timestamp = DGCgettime();
    response.logSize = 0;
    sensnet_write(sensnet, SENSNET_METHOD_CHUNK, this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
    this->beatTime = 0;
  }
            
  // Read process request     
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest, &blobId, sizeof(request), &request) != 0)
    return;
  if (blobId < 0)
    return;
            
  if (request.quit)
    this->quit = true;
  
}

//! Parse the command line
int RoadPerceptor::parseCmdLine(int argc, char **argv) {
  // Default configuration path
  char defaultConfigPath[256];
  //  char filename[256];

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

    // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(defaultConfigPath, sizeof(defaultConfigPath),
             "%s/lineperceptor", getenv("DGC_CONFIG_PATH"));
  else
    return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
  {
      //check if replay or blob mode
      if (strstr(this->options.inputs[0], ".blob") != 0)
	  this->mode = modeBlob;
      else
	  this->mode = modeReplay;
  }
  else
    this->mode = modeLive;

  //show option
  if (this->options.show_given)
      this->show = this->options.show_flag;
  else
      this->show = false;
 
  //step option
  if (this->options.step_given)
      this->step = this->options.step_flag;
  else
      this->step = false;

  //export option
  if (this->options.save_given)
      this->save = this->options.save_flag;
  else
      this->save = false;

  //export option
  if (this->options.save_debug_given)
      this->save_debug = this->options.save_debug_flag;
  else
      this->save_debug = false;

  //debug option
  if (this->options.debug_given)
      this->debug = this->options.debug_flag;
  else
      this->debug = false;
  DEBUG_LINES = this->debug;

  //goto option
  if (this->options.goto_given)
      this->gotoFrame = this->options.goto_arg;
  else
      this->gotoFrame = -1;

  //read the stop line perceptor conf
  /*  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "StopLinePerceptor.conf");
  mcvInitStopLinePerceptorConf(filename, &this->stopLineConf);

  //read the lane(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "LanePerceptor.conf");
  mcvInitStopLinePerceptorConf(filename, &this->laneConf);
  
  //read camera info
  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "CameraInfo.conf");
  mcvInitCameraInfo(filename, &this->cameraInfo);    
  */
  //tracking option
  if (this->options.track_stoplines_given)
      this->trackStopLines = this->options.track_stoplines_flag;
  else
      this->trackStopLines = false;
  this->numPreFrames = 0;
  this->numPostFrames = 0;

  //detections
  if (this->options.no_stoplines_given)
      this->noStoplines = this->options.no_stoplines_flag;
  else
      this->noStoplines = false;
  if (this->options.no_lanes_given)
      this->noLanes = this->options.no_lanes_flag;
  else
      this->noLanes = false;


  
  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoadPerceptor $Revision$                                                   \n"
"                                                                           \n"
"Mode: %mode%                                                               \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"Stats: Avg Rate\tEdge\tHough\tLine Assoc\n"
"Stats: %laneStats%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%SHOW%|%FAKE%|%SKIP%|%DEBUG%|%EXPORT%|%STEP%]              \n";

// Initialize console display
int RoadPerceptor::initConsole() {
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%SHOW%", " SHOW ", "Ss",
		   (cotk_callback_t) onUserShow, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);

  //button to skip through the log files
  cotk_bind_button(this->console, "%SKIP%", " SKIP ", "Kk",
                   (cotk_callback_t) onUserSkip, this);

  //button to enable/disable debug
  cotk_bind_button(this->console, "%DEBUG%", " DEBUG ", "Dd",
                   (cotk_callback_t) onUserDebug, this);

  //button to enable/disable stepping
  cotk_bind_button(this->console, "%STEP%", " STEP ", "Tt",
                   (cotk_callback_t) onUserStep, this);

  //button to export current frame
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);

    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%mode%", A_NORMAL, "%s", 
	      this->mode==modeReplay ? "Replay" : this->mode==modeBlob ? "Blob" : "Normal");


  return 0;
}


// Finalize sparrow display
int RoadPerceptor::finiConsole() {
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int RoadPerceptor::initSensnet() {

  // Start sensnet, even if we are in replay mode, since we may wish
  // to send line messages.
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  if (this->mode == modeLive) {
    // Join stereo group for live stereo data
    if (sensnet_join(this->sensnet, this->sensorId,
                     SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
      return -1;
  }
  else if (this->mode == modeReplay) {
    // Open replay of exisiting log
    this->replay = sensnet_replay_alloc();
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs)!=0)
      return -1;
  }


  //init sending to mapp
  mapElementTalker.initSendMapElement(this->skynetKey);

  return 0;
}


// Clean up sensnet
int RoadPerceptor::finiSensnet() {
  if (this->sensnet) {
    sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }

  if (this->replay) {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Read the current image
int RoadPerceptor::readImage() {
    //int blobId;

  // Live mode
  if (this->mode == modeLive) {
//     // Take a peek at the latest data
//     if (sensnet_peek(this->sensnet, this->sensorId,
//                      SENSNET_STEREO_IMAGE_BLOB, &blobId, NULL) != 0)
//       return -1;
  
//     // Do we have new data?
//     if (blobId < 0 || blobId == this->blobId)
//       return -1;

    //use blocking wait for new data to arrive
    if( sensnet_wait(this->sensnet, 500) != 0)
	return -1;      
    
    // Read the new blob
    if (sensnet_read(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                     &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;
  }

  // Replay mode
  else if (this->mode == modeReplay) {
    // Advance log one step
    if (sensnet_replay_next(this->replay, 0) != 0)
      return -1;
    
    // Read the new blob
    if (sensnet_replay_read(this->replay, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                            &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;

    MSG("read blob %d", this->blobId);
  }

  //modeBlob
  else if (this->mode == modeBlob) {
      //check if already read it
      if(this->stereoBlob.frameId!=0)
	  return -1;
      //load the blob from the blob file
      FILE *file;
      if ((file=fopen(this->options.inputs[0], "r")) == 0)
	  return -1;
      if (fread(&this->stereoBlob, sizeof(this->stereoBlob), 1, file)!= 1)
	  return -1;
      fclose(file);
      MSG("read blob: %d", this->stereoBlob.frameId);
  }

  // Update the display
  if (this->console) {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
}


//! Write the current stop lines
int RoadPerceptor::writeLines() {
  
  return 0;
}    


//! Handle button callbacks
int RoadPerceptor::onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token) {
  MSG("user quit");
  self->quit = true;
  return 0;
}


//! Handle button callbacks
int RoadPerceptor::onUserPause(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



//! Handle fake stop button
int RoadPerceptor::onUserFake(cotk_t *console, RoadPerceptor *self, const char *token) {
  float px, py, pz;
    
  MSG("creating fake stop line data");

  self->lineMsg.num_lines = 0;
      
  // MAGIC
  px = VEHICLE_LENGTH + 3;
  py = -2;
  pz = VEHICLE_TIRE_RADIUS;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);

  self->lineMsg.lines[0].a[0] = px;
  self->lineMsg.lines[0].a[1] = py;
  self->lineMsg.lines[0].a[2] = pz;

  px = VEHICLE_LENGTH + 3;
  py = +2;
  pz = 0.5;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);
      
  self->lineMsg.lines[0].b[0] = px;
  self->lineMsg.lines[0].b[1] = py;
  self->lineMsg.lines[0].b[2] = pz;
  
  self->lineMsg.num_lines = 1;
  
  self->totalFakes += 1;

	self->writeLines();
	self->lineMsg.num_lines = 0;

	return 0;
}


int RoadPerceptor::onUserShow(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->show = !self->show;
  MSG("show %s", (self->show ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserSkip(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->skip = !self->skip;
  MSG("skip %s", (self->skip ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserDebug(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->debug = !self->debug;
  DEBUG_LINES = self->debug;
  MSG("debug %s", (self->debug ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserStep(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->step = !self->step;
  MSG("step %s", (self->step ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserExport(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->exportImage();
  return 0;
}



//! export current image in stereo blob
int RoadPerceptor::exportImage() {
    CvMat im3 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
		      this->stereoBlob.channels==1 ? CV_8UC1 : CV_8UC3, 
		      this->stereoBlob.imageData + this->stereoBlob.leftOffset);
    
    char str[255];
    sprintf(str, "im-%08d.png", this->stereoBlob.frameId);
    cvSaveImage(str, &im3);
    MSG("Written file: %s", str);

    //write the whole stereoblob structure
    sprintf(str, "blob-%08d.blob", this->stereoBlob.frameId);
    FILE *f;
    if( (f = fopen(str, "w")) == 0)
	return -1;
    if( fwrite(&this->stereoBlob, sizeof(this->stereoBlob), 1, f) != 1)
	return -1;
    fclose(f);
    MSG("Wrote blob: %s", str);

    return 0;
}


//! initialize the kalman filter structure
void RoadPerceptor::initStopLineTrack() {

}

//! destroy the track
void RoadPerceptor::destroyStopLineTrack() {
    if (this->stopLineTrackCreated) {
	//release the current track
	cvReleaseKalman(&this->kalman);
	//reset
	this->stopLineTrackCreated = false;
	//msg
	MSG("Track destroyed");
    }
}

//track the detected stop line
void RoadPerceptor::updateStopLineTrack() {

}

//! Paint ROI on original image
void RoadPerceptor::paintROI() {
      CvPoint point1,point2;
      point1.x = ROIstartX;
      point1.y = ROIstartY;
      point2.x = ROIstartX + ROIwidth;
      point2.y = ROIstartY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
      point1.x = point2.x;
      point1.y = ROIstartY + ROIheight;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);  
      point2.x = ROIstartX;
      point2.y = point1.y;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8); 
      point1.x = ROIstartX;
      point1.y = ROIstartY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);  
      point1.x = ROIstartX;
      point1.y = ROIhorizonY;
      point2.x = ROIstartX + ROIwidth;
      point2.y = ROIhorizonY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
}

//! Find stop lines
int RoadPerceptor::findLines() {
    int i,j;
    uint8_t *pix;

    vector<Line> lanes;
    MapElement mE;

    //! get pointer to image data
    pix = StereoImageBlobGetLeft(&this->stereoBlob, 0, 0);
   
    CvMat rawim;

    //! check number of channels of input image
    //! grayscale image
    if (this->stereoBlob.channels==1) {
	//! get image
        rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1, pix);
		
    }
    //! RGB image
    else {
	//! Get raw image. its in RGB
	rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3, pix);

      	cvCopy(&rawim, imCOriginal, NULL);
	cvCopy(&rawim, imCProcess, NULL); 

	//! Convert to single channel
	cvCvtColor(imCOriginal,imOriginal,CV_RGB2GRAY);
	
	cvGetSubRect(imCOriginal, imC, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));

	//! get grayscale img
	cvCvtColor(imC,imProcess,CV_RGB2GRAY);
	if (this->debug) SHOW_IMAGE(imProcess, "0. GrayScale", 10);

	//! Split channels to R+G+B
	cvSplit(imC, imCO0, imCO1, imCO2, NULL); 	    
	
	//! WHITE CHANNEL on RGB
	//! hypothesis:  min(R,G,B)>200 && dist(min(R,G,B),max(R,G,B)) < 20 
	cvMin(imCO0,imCO1,imBuffer);
	cvMin(imBuffer,imCO2,imBuffer1);
	
	cvMax(imCO0,imCO1,imBuffer);
	cvMax(imBuffer,imCO2,imBuffer2);
	
	cvAbsDiff(imBuffer1,imBuffer2,imBuffer2);
	cvThreshold(imBuffer2,imBuffer2,20,1,CV_THRESH_BINARY_INV);
	
	cvMul(imBuffer1,imBuffer2,imBuffer1,1);
	cvThreshold(imBuffer1,imWhite,200,255,CV_THRESH_TOZERO);

	if (this->debug) SHOW_IMAGE(imWhite, "1. White Channel", 10);

	//! YELLO-BOOST CHANNEL
	//! hypothesis: R+G-B
	cvAddWeighted(imCO0,0.6, imCO1, 0.4, 0, imBuffer);
	cvSub(imBuffer,imCO2,imBuffer2,NULL);

	if (this->debug) SHOW_IMAGE(imBuffer2, "2. YellowBoost Channel", 10);

	//! Greyscale + Yellow boost 
	cvAdd(imProcess,imBuffer2,imProcess, NULL);

	if (this->debug) SHOW_IMAGE(imProcess, "3. Gr+Y Channel",10);

	//! WHITE CHANNEL ON RGrY: R+(GreyScale+Yellow)
	//! hypothesis:  max(R,Gr+Y)>220 && dist(min(R,Gr+Y),max(R,Gr+Y))<20 
	cvMin(imCO0,imProcess,imBuffer1);
	cvMax(imCO0,imProcess,imBuffer2);

	cvAbsDiff(imBuffer1,imBuffer2,imBuffer2);
	cvThreshold(imBuffer2,imBuffer,20,1,CV_THRESH_BINARY_INV);

	cvMul(imBuffer,imBuffer1,imBuffer2,1);
	cvThreshold(imBuffer1,imYellow,230,255,CV_THRESH_TOZERO);
	
	if (this->debug) SHOW_IMAGE(imYellow, "4. Yellow Channel (White on R+G+Y)", 10);
	
	cvMax(imWhite, imYellow, imProcess);
	cvSmooth(imProcess, imProcess, CV_GAUSSIAN, 3, 3, 0);
	
	if (this->debug) SHOW_IMAGE(imProcess, "5. Pre-Processed Image", 10);
	
    }

    MSG("Processing frame#%d", this->stereoBlob.frameId);

    if(!this->skip) {
	//! used for statistics
	static long numLaneFrames=0;
	static double totalLaneTime=0.0, totalEdgeTime=0.0, totalHoughTime=0.0, totalLineTime=0.0;
	double laneTime, edgeTime, houghTime, lineTime;

	//! get lanes
	if (!this->noLanes) {
	  //! Search for lines
	  int64 startTime, endEdgeTime, endHoughTime, endTime;    
	  startTime = cvGetTickCount();

	  cvCanny(imYellow, imProcess, 50, 300, 3);
	    
	  if (this->debug) SHOW_IMAGE(imProcess, "6. Edges", 10);

	  endEdgeTime = cvGetTickCount();

	  CvMemStorage* storage = cvCreateMemStorage(0);
	  CvSeq* lines = 0;
	    
	  //! Probabilistic Hough Transform to get lines
	  lines = cvHoughLines2(imProcess, storage, CV_HOUGH_PROBABILISTIC, 1, 1*CV_PI/180, 40, 20, 3);
	      
	  for (i=0; i<lines->total; i++) {
	    Line thisLine;
	    CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
	    thisLine.startPoint.x = line[0].x + ROIstartX;
	    thisLine.startPoint.y = line[0].y + ROIhorizonY;
            thisLine.endPoint.x = line[1].x + ROIstartX;
            thisLine.endPoint.y = line[1].y + ROIhorizonY;
            thisLine.score = 0;
            thisLine.color = OTHER;
            lanes.push_back(thisLine);
	  }

	  endHoughTime = cvGetTickCount();
	   
	  //! line matching 
	  for( i = 0; i < lines->total; i++ ) {
      
	    //! ignore this line if it has been considered by a parallel one
	    if (lanes[i].score == 3) continue;
   	
	    CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);	
	    CvPoint* line2;	
		
	    int hasPair=0;
       
	    //! check if there is a double line
	    for (j=i+1; j< lines->total; j++) {
	      line2 = (CvPoint*) cvGetSeqElem(lines,j);	
	      hasPair = checkDouble((CvPoint) line[0],(CvPoint) line[1],(CvPoint) line2[0],(CvPoint) line2[1],SizeTol,AngleTol,DistanceTol);
	      if (hasPair) break;
	    }
    	
	    //! this could be prettier, checking all and not one possible pair for each line and grouping closer pairs
	    //! lines with pairs  
	    if (hasPair) {
	      lanes[i].color = getLineColorBetween(lanes[i],lanes[j]); 
	      lanes[j].color = lanes[i].color;
	      
	      if( distance((CvPoint) line[0], (CvPoint) line[1]) > distance((CvPoint) line2[0], (CvPoint) line2[1]) ) {
		lanes[i].score=4;
		lanes[j].score=3;
	      } else {
		lanes[i].score=3;
		lanes[j].score=4;
	      }
      
	      //! if line is big enough, let's still take it on caution
	    } else if (distance((CvPoint) line[0], (CvPoint) line[1]) > 30) {
	      lanes[i].color = getLineColor(lanes[i], int(round(DistanceTol/2)));
	      lanes[i].score=2;
	    } else {
	      //! remaining lines are considered unimportant
	      lanes[i].score=1;
	    }
	  }

	  endTime = cvGetTickCount();

	  cvReleaseMemStorage(& storage);

	  //! stats
	  numLaneFrames++;

	  laneTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
	  edgeTime = 0.001*(endEdgeTime-startTime)/cvGetTickFrequency();
	  houghTime= 0.001*(endHoughTime-endEdgeTime)/cvGetTickFrequency();
	  lineTime = 0.001*(endTime-endHoughTime)/cvGetTickFrequency();

	  totalLaneTime += laneTime;
	  totalEdgeTime += edgeTime;
	  totalHoughTime += houghTime;
	  totalLineTime += lineTime;

	  //! display
	  if (this->console)
	    cotk_printf(this->console, "%laneStats%", A_NORMAL, 
			"%.2fHz\t%.2fms\t%.2fms\t%.2fms", numLaneFrames/totalLaneTime*1000, totalEdgeTime/numLaneFrames, totalHoughTime/numLaneFrames, totalLineTime/numLaneFrames);
	  else
	    MSG("Lane line takes %.2fms\tavg %.2fms\t%.2fHz", laneTime,
		totalLaneTime/numLaneFrames, numLaneFrames/totalLaneTime*1000);
	}
    } 
      
    //! save image with detected lines
    if(this->save_debug) {
    
    }

    //! show detected lines
    if (this->show) {    
      int key=cvWaitKey( this->step ? 0 : 10);
      if (key=='e' || key=='E')
	  this->exportImage();
      else if (key == 'q' || key == 'Q')
	  this->onUserQuit(console, this, "");
      else if (key == 'p' || key == 'P')
	  this->onUserPause(console, this, "");
      else if (key == 't' || key=='T')
	  this->onUserStep(console, this, "");
//       else if (key == 'k' || key == 'K') //skip	 
// 	  return 0;
    }
    //not showing, so export the image
    if (this->save) {
      // this->exportImage();
    }
    
    for (i = 0; i<(int)lanes.size(); i++) {
      
      if (this->show) {
	CvPoint point1,point2;
	point1.x = int(lanes[i].startPoint.x);
	point1.y = int(lanes[i].startPoint.y);
	point2.x = int(lanes[i].endPoint.x);
	point2.y = int(lanes[i].endPoint.y);

	if (lanes[i].score <2 ) cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8 );
	if (lanes[i].color == GREEN) cvLine(imCProcess, point1, point2, CV_RGB(0,255,0), 1, 8 );
	if (lanes[i].color == WHITE) cvLine(imCProcess, point1, point2, CV_RGB(255,255,255), 1, 8 );
	if (lanes[i].color == YELLOW) cvLine(imCProcess, point1, point2, CV_RGB(255,255,0), 1, 8 );
	if (lanes[i].color == OTHER) cvLine(imCProcess, point1, point2, CV_RGB(255,0,255), 1, 8 );
      
	if (lanes[i].color == WHITE || lanes[i].color == YELLOW) {
	  point1.x ++;
	  point1.y ++;
	  point2.x ++;
	  point2.y ++;
	  cvLine(imCProcess, point1, point2, CV_RGB(0,0,255), 1, 8 );
	  point1.x -=2;
	  point1.y -=2;
	  point2.x -=2;
	  point2.y -=2;
	  cvLine(imCProcess, point1, point2, CV_RGB(0,0,255), 1, 8 );
	}
      }

      //! lanes that are not supposed to be output must be cleaned
      if (lanes[i].color == GREEN || lanes[i].color == OTHER || lanes[i].score <2 || lanes[i].score == 3) {
	cleanMapElement(mE, i);
	continue;
      }
      
      //! if for some reason the line was not accepted in localFrame, clean it
      if (imageLineToMapElement(lanes[i], mE, i)==-1) {
	cleanMapElement(mE, i);
	continue;
      }
           

    }

    if (this->show) {
     
      paintROI();
      SHOW_IMAGE(imCProcess, "7. Original Image with superimposed lines", 10);

    }

    if (int(lanes.size()) < lanesPreviousSize) {
      for (i=lanes.size(); i < lanesPreviousSize; i++) {
	cleanMapElement(mE, i);
      }
    }

    MSG("%d lines output out of %d lines", lanesOutput, int(lanes.size()));
    lanesOutput = 0;

    lanesPreviousSize = lanes.size();
    lanes.clear();
  
    return 0;
}

//! Returns the line color in between two lines
int RoadPerceptor::getLineColorBetween(const Line &line1, const Line &line2) {

  Line lineBetween;
  float dist;

  // get distance between lines in the extreme closer to viewer
  if (line1.startPoint.y > line1.endPoint.y) {
    dist = minDistBetweenPointAndLine(cvPointFrom32f(line1.startPoint), cvPointFrom32f(line2.startPoint), cvPointFrom32f(line2.endPoint)); 
  } else {
    dist = minDistBetweenPointAndLine(cvPointFrom32f(line1.endPoint), cvPointFrom32f(line2.startPoint), cvPointFrom32f(line2.endPoint)); 
  }

  if (distance(line1.startPoint.x, line1.startPoint.y, line2.startPoint.x, line2.startPoint.y) < distance(line1.startPoint.x, line1.startPoint.y, line2.endPoint.x, line2.endPoint.y)) {
    
    lineBetween.startPoint.x = (line1.startPoint.x + line2.startPoint.x)/2;
    lineBetween.startPoint.y = (line1.startPoint.y + line2.startPoint.y)/2;
    lineBetween.endPoint.x = (line1.endPoint.x + line2.endPoint.x)/2;
    lineBetween.endPoint.y = (line1.endPoint.y + line2.endPoint.y)/2 ;

  } else {

    lineBetween.startPoint.x = (line1.startPoint.x + line2.endPoint.x)/2;
    lineBetween.startPoint.y = (line1.startPoint.y + line2.endPoint.y)/2;
    lineBetween.endPoint.x = (line1.endPoint.x + line2.startPoint.x)/2;
    lineBetween.endPoint.y = (line1.endPoint.y + line2.startPoint.y)/2 ;

  }

  return getLineColor(lineBetween, int(round(DistanceTol/2)));
}

//! Returns the line color 
int RoadPerceptor::getLineColor(const Line &imageLine, int sideColorCheckDistance) {

  //! point in yellow channel, average for self, left and right
  CvScalar p_y, a_y=cvScalar(0.0), a_y_l=cvScalar(0.0), a_y_r=cvScalar(0.0);
  //! point in white channel, average for self, left and right
  CvScalar p_w, a_w=cvScalar(0.0), a_w_l=cvScalar(0.0), a_w_r=cvScalar(0.0);

  int i=0, size=0;
  int x=0, y=0, x_l, y_l, x_r, y_r;

  size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));

  for (i=0; i<size; i++) {
    x = int(imageLine.startPoint.x + i*(imageLine.endPoint.x-imageLine.startPoint.x)/size);
    y = int(imageLine.startPoint.y + i*(imageLine.endPoint.y-imageLine.startPoint.y)/size);


    // in the middle
    x=x-ROIstartX;
    y=y-ROIhorizonY;
    if (x<0) x=0;
    if (y<0) y=0;

    p_w = cvGet2D(imWhite, y, x);
    p_y = cvGet2D(imYellow, y, x);

    a_w.val[0] = a_w.val[0] + p_w.val[0];
    a_y.val[0] = a_y.val[0] + p_y.val[0];
    
    // on the left
    x_l = x - sideColorCheckDistance;
    y_l = y - sideColorCheckDistance;
    if (x_l < 0) x_l = 0;
    if (y_l < 0) y_l = 0;

    p_w = cvGet2D(imWhite, y_l, x_l);
    p_y = cvGet2D(imYellow, y_l, x_l);
    a_w_l.val[0] = a_w_l.val[0] + p_w.val[0];
    a_y_l.val[0] = a_y_l.val[0] + p_y.val[0];
    
    // on the right
    x_r = x + sideColorCheckDistance;
    y_r = y + sideColorCheckDistance;
    if (x_r >= ROIwidth) x_r = ROIwidth -1;
    if (y_r >= ROIheight-(ROIhorizonY-ROIstartY)) y_r = ROIheight-(ROIhorizonY-ROIstartY) -1;   

    p_w = cvGet2D(imWhite, y_r, x_r);   
    p_y = cvGet2D(imYellow, y_r, x_r);
    a_w_r.val[0] = a_w_r.val[0] + p_w.val[0];
    a_y_r.val[0] = a_y_r.val[0] + p_y.val[0];
    
  }

  a_y.val[0] = round(a_y.val[0]/size);     // Y
  a_y_l.val[0] = round(a_y_l.val[0]/size); // Y
  a_y_r.val[0] = round(a_y_r.val[0]/size); // Y

  a_w.val[0] = round(a_w.val[0]/size);     // W
  a_w_l.val[0] = round(a_w_l.val[0]/size); // W
  a_w_r.val[0] = round(a_w_r.val[0]/size); // W

  if ((a_w.val[0] > a_w_l.val[0] + 20) && (a_w.val[0] > a_w_r.val[0] + 20)) {
    return WHITE;
  } else if ((a_y.val[0] > a_y_l.val[0] + 20) && (a_y.val[0] > a_y_r.val[0] + 20)){
    return YELLOW;
  }

  return OTHER;
}

float min3(float a, float b, float c) {
  if (min(a,b)==a && min(a,c)==a) return a; 
  if (min(a,b)==b && min(b,c)==b) return b; 
  return c;
}

//! extend this line
int RoadPerceptor::extendLocalLine(const Line &imageLine, float pi, float pj, float pi2, float pj2, float &px, float &py, float &px2, float &py2) {
  
  float sizeRatio;
  float wingSize;
  
  float lineSize;
  float sinTeta;
  float cosTeta;
  float direction;

  lineSize = sqrt(pow(px2-px,2)+pow(py2-py,2));
  sinTeta = fabs(py2-py)/lineSize;
  cosTeta = fabs(px2-px)/lineSize;

  //! Start wing
  if (imageLine.startPoint.x != pi || imageLine.startPoint.y != pj) {
    sizeRatio = sqrt(pow(pi-imageLine.startPoint.x,2)+pow(pj-imageLine.startPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;
    direction = (px-px2)/fabs(px-px2);
    px = px + direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py = py + direction*wingSize*sinTeta;
  }

  //! end wing
  if (imageLine.endPoint.x != pi2 || imageLine.endPoint.y != pj2) {
    sizeRatio = sqrt(pow(pi2-imageLine.endPoint.x,2)+pow(pj2-imageLine.endPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;

    direction = (px-px2)/fabs(px-px2);
    px = px - direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py = py - direction*wingSize*sinTeta;
    
  }

  return 1;
}

//! finds point near to provided, over line, that has good disparity
int RoadPerceptor::getBestNearestPoint(const Line &imageLine, float & pi, float & pj, float  & pd, int direction) {

  uint16_t testDisp;
  float px, py, pz;

  int size=0, i=0;

  float dist=0;
  float beginX=0, beginY=0;
  float endX=0, endY=0;
  
  size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));

    if (direction == 1) {
      beginX = imageLine.startPoint.x;
      beginY = imageLine.startPoint.y;
      endX = imageLine.endPoint.x;
      endY = imageLine.endPoint.y;
    } else {
      beginX = imageLine.endPoint.x;
      beginY = imageLine.endPoint.y;
      endX = imageLine.startPoint.x;
      endY = imageLine.startPoint.y;
    }
    
    //! try to get disparity from other points in line
    for (i=0; i<size; i++) {
      pi = beginX + i*direction*(imageLine.endPoint.x-imageLine.startPoint.x)/size;
      pj = beginY + i*direction*(imageLine.endPoint.y-imageLine.startPoint.y)/size;

     //! we just walked the whole line, don't check this point
      if (pi == endX && pj == endY) return 0;

      testDisp = (*StereoImageBlobGetDisp(&this->stereoBlob, (int) pi, (int) pj));
      
      //! good disparity, lets stick with this point
      if (testDisp>0 && testDisp != 65535) {
	pd = (float) testDisp;
	pd /= this->stereoBlob.dispScale;
	imageToLocalPoint(pi, pj, pd, px, py, pz);
	
	//! check if the point is on alice or too far away (lousy but valid disparity)
	dist = sqrt(pow(this->stereoBlob.state.localX-px,2)+pow(this->stereoBlob.state.localY-py,2));
	
	//! only consider valid disparity points
	if (dist > 1 && dist < 60) {
	  return 1;
	}
      }
    }

    return 0;
}

//! Convert image frame point to local point
void RoadPerceptor::imageToLocalPoint(float pi, float pj, float pd, float &px, float &py, float &pz) {

    //! Compute point in the sensor frame
    StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);
    //! Convert to vehicle frame
    StereoImageBlobSensorToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);
    //! Convert to local frame
    StereoImageBlobVehicleToLocal(&this->stereoBlob, px, py, pz, &px, &py, &pz);

}


//! cleans exceding mapElements
int RoadPerceptor::cleanMapElement(MapElement &mE,int index) {
  mE.clear();
  mE.setId(this->moduleId, index);

  this->mapElementTalker.sendMapElement(&mE, 0);

  return 1;
}


//! converts from a line in image coordinates to a line in local coordinates
int RoadPerceptor::imageLineToMapElement(const Line &imageLine, MapElement &mE, int index) {
    float pi, pj, pi2, pj2, pd;
    float px, py, pz, px2, py2, pz2;

    point2arr points;

    mE.clear();

    //! start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    
    //! check if it's a good point or there is a good point nearby
    if (!getBestNearestPoint(imageLine, pi, pj, pd, 1)) return 0;

    //! get the local frame coordinates for the best nearest point
    imageToLocalPoint(pi, pj, pd, px, py, pz);

    // fix the line taking into account we only used the segment between best nearest points
    // extendLocalLine(imageLine,pi,pj,pi2,pj2,px,py,px2,py2);

    // to high, probably an obstacle. TODO: better model for obstacles
    if (pz>0.5) return -1;
    
    //end point
    pi2 = imageLine.endPoint.x;
    pj2 = imageLine.endPoint.y;

    if (!getBestNearestPoint(imageLine, pi2, pj2, pd, -1)) return -1;

    // only one point on the line, discard it.
    if (pi == pi2 && pj == pj2) return -1;

    imageToLocalPoint(pi2, pj2, pd, px2, py2, pz2);
    
    //! to high, probably an obstacle
    if (pz2>0.5) return -1;

    //! get real line-end points
    extendLocalLine(imageLine, pi, pj, pi2, pj2, px, py, px2, py2);

    //! short lines don't get displayed
    if (distance(px,py,px2,py2)<0.4) return -1;

    points.push_back(point2(px, py));
    points.push_back(point2(px2,py2));

    mE.setId(this->moduleId, index);  
    mE.setGeometry(points);

    mE.height=0;
    if (imageLine.color == GREEN) mE.plotColor = MAP_COLOR_GREEN;
    if (imageLine.color == WHITE) mE.plotColor = MAP_COLOR_GREY;
    if (imageLine.color == YELLOW) mE.plotColor = MAP_COLOR_YELLOW;
    if (imageLine.color == OTHER) mE.plotColor = MAP_COLOR_ORANGE;
 
    mE.type = ELEMENT_LANELINE;
    mE.geometryType = GEOMETRY_LINE;
    mE.state = this->stereoBlob.state;
    
    this->mapElementTalker.sendMapElement(&mE, 0); 
    
    lanesOutput ++;
    
    points.clear();
    mE.clear();
    return 1;
}

/*---------------------------------------------*/
//! HELPER FUNCTIONS
/*---------------------------------------------*/
float RoadPerceptor::getLineAngle(CvPoint start, CvPoint end) {
	float angle=0.0;
	
	//! starting point always has lower or equal X coordinate
	if (start.x == end.x) {
		angle = 90.0;
	} else {
		angle = 180/CV_PI * atan((float) (end.y-start.y)/(end.x-start.x));
	}

	return angle;
}

float RoadPerceptor::distance(CvPoint start, CvPoint end) {
	return sqrt(pow((double) (start.x-end.x),2)+pow((double) (start.y-end.y),2));
}

float RoadPerceptor::distance(float startx,float starty, float endx, float endy) {
	return sqrt(pow((double) (startx-endx),2)+pow((double) (starty-endy),2));
}

int RoadPerceptor::checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol) {
		
	//! size tolerance trial
	float size1 = distance(start1, end1);
	float size2 = distance(start2, end2);
	if (max(size1,size2)/min(size1,size2) > sizeTol && min(size1,size2)>20) {
		return 0;
	}

	//! angle tolerance trial
	if (!parallel(start1, end1, start2, end2, angleTol)) {
		return 0;
	} 

	//! line convergence and alignment	
	if (!lineMatching(start1, end1, start2, end2, distanceTol)) {
		return 0;
	}

	// All trials passed. brillo
		
	return 1;
}

int RoadPerceptor::parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol) {
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	float angleDiff = fabs(angle1-angle2);
	if (!(angleDiff<=angleTol || (180.0-angleDiff)<=angleTol)) return 0;

	return 1;
	
}

int RoadPerceptor::lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol) {
	/*
	What we know: 
	a. Points in a line are ordered by lower X.
	   a1. start1.x > end1.x
	   a2. start2.x > end2.x
	b. line1 and line2 are almost parallel (this is after the angle tolerance trial)
	*/

	//! extreme 1 and 2 distance
	//! closer points at extremes
	float dE1, dE2;
	//! the bigger and smaller of dE1 and dE2
	float dMax, dMin;

	//! angles from -90 exclusive to +90 inclusive
	//float angle1 = getLineAngle(start1, end1);
	//float angle2 = getLineAngle(start2, end2);
	
	/*	//! they are practically parallel. if the diference of angles is >90, we have a /\ or \/ tricky situation
	if (fabs(angle1-angle2) > 90) {
			dE1 = distance(start1,end2);
			dE2 = distance(start2,end1);	
	} else {
	//! if not, then just calculate normal distances
			dE1 = distance(start1,start2);
			dE2 = distance(end1,end2);	
	}
	
	dMax = dE1 >= dE2 ? dE1 : dE2;
	dMin = dMax == dE1 ? dE2 : dE1;
	*/

	if (distance(start1,start2) <  distance(start1,end2)) {
	  //! lines may be offset. we know start1 and start2 are closer. determine minimum distance between each of them and the respective other line.
	  dE1 = min( minDistBetweenPointAndLine(start2,start1,end1),minDistBetweenPointAndLine(start1,start2,end2));
 	  dE2 = min( minDistBetweenPointAndLine(end2,start1,end1),minDistBetweenPointAndLine(end1,start2,end2));
	  	  
	} else {
	  //! lines may be offset. we know start1 and end2 are closer. determine minimum distance between each of them and the respective other line.
	  dE1 = min( minDistBetweenPointAndLine(end2,start1,end1),minDistBetweenPointAndLine(start1,start2,end2));
 	  dE2 = min( minDistBetweenPointAndLine(end1,start1,end1),minDistBetweenPointAndLine(start2,start2,end2));
	}

	dMax = dE1 >= dE2 ? dE1 : dE2;
	dMin = dMax == dE1 ? dE2 : dE1;

	if (dMax > distanceTol || dMax < 3 ) return 0;
		
	return 1;
}

float RoadPerceptor::minDistBetweenPointAndLine(CvPoint point, CvPoint start, CvPoint end) {
  float d=0,d_n, size=0;
  int i;

  size = distance(start,end);
  d = distance(start,point);
  for (i=0; i<size; i++) {
    d_n = distance(start.x+i*(end.x-start.x)/size, start.y+i*(end.y-start.y)/size, point.x, point.y);
    if (d_n>d) {
      return d;
    }
    d=d_n;
  }
  return d;

}

/*---------------------------------------------*/
//! MAIN FUNCTION
/*---------------------------------------------*/

// Main program thread
int main(int argc, char **argv) {
  RoadPerceptor *percept;


  percept = new RoadPerceptor();
  assert(percept);

  //  memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  // Get a single image for initializing the images
  while (percept->readImage() != 0) {
    percept->readImage();
  }
  // Initialize images
  percept->initImages();

  while (!percept->quit) {
    //delay for a while
    if (percept->replay) {    
	//usleep(500);
    }

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // Run process control
    percept->handleProcessControl();

    // If we are paused, dont do anything
    if (percept->pause) {
      usleep(0);
      continue;
    }

    // Check for new data
    if (percept->readImage() == 0) {

	//check if goto frame given
	if (percept->gotoFrame!=-1 && percept->stereoBlob.frameId < percept->gotoFrame)
	    continue;
	
	// Detect lines
	percept->findLines();  

    }
  }

  percept->finiConsole();
  percept->finiSensnet();
  percept->destroyStopLineTrack();

  MSG("exited cleanly");
  
  return 0;
}



