#ifndef ROAD_PERCEPTOR_HH
#define ROAD_PERCEPTOR_HH


/**
 * \file RoadPerceptor.hh
 * \author Humberto Pereira and Mohamed Aly and Andrew Howard
 * \date Thu 26 Jul, 2007
 *
 */


#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

//! Sensnet Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/ProcessState.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>

//! CLI support
#include <cotk/cotk.h>

//! Cmd-line handling
#include "cmdline.h"

//! Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"

#define WHITE 1
#define YELLOW 2
#define OTHER 0
#define GREEN 3

//! line structure with start and end points
typedef struct Line
{
  FLOAT_POINT2D startPoint;
  FLOAT_POINT2D endPoint;
  int score; 
  /* 
  0.not processed 
  1.unimportant 
  2.big isolated line 
  3.smaller line of pair. 
  4.bigger lineof pair
  */
  int color;
  
  float startPointz,endPointz ;
  float preStartX,preStartY,preStartZ,preEndX,preEndY,preEndZ;
  
  CvKalman* kalman_pointer;

  int ID; 
   
  int track;

  int postFrames;

  double startMaxVar;
  double endMaxVar;
  double startMinVar;
  double endMinVar;
  double startAxis;
  double endAxis;

  unsigned long long time;
  float localXVel,localYVel,localZVel, localX, localY, localZ, deltaX, deltaY, deltaZ;
}Line;

vector<Line> oldLines;
//! return minimum of 3 values
float min3(float a, float b, float c);

//! Line perceptor module
class RoadPerceptor
{
public:
    
  //! Constructor
  RoadPerceptor();
    
  //! Destructor
  ~RoadPerceptor();
    
public:
  
  //! Set parameters
  int readConfig();
  
  //! Parse the command line
  int parseCmdLine(int argc, char **argv);
    
public:
    
  //! Initialize sensnet
  int initSensnet();
    
  //! Clean up sensnet
  int finiSensnet();
    

  //! Start Image headers 
  void initImages();

  //! Kill Image headers
  void destroyImages();

  //! Read the current image
  int readImage();
    
  //! Write the current lines
  int writeLines();

  int number;


  //these variables get set in the config-file.

  float R, Q, minDistLocal, minDist,minDist2, deg, deg2, deg3, speed;
  double MAX_ALLOWED_DISTANCE, MinLineMetersKalman, MinLineMeters;
  int POST_FRAMES, EdgeYellowLinking, EdgeYellowStrong, EdgeWhiteLinking, EdgeWhiteStrong, HoughMaxGap, HoughMinLinePixels, HoughMinAcc, DLDistanceTol, DLAngleTol, DLSizeTol ; 


 public:

  //! Find stop lines
  int findLines();


  //! export current image
  int exportImage();

  //! export function image
  int exportImage(char *function, const CvMat *imSave);

  //! export function image
  int exportImage(char *dir, char *function, const CvMat *imSave);

  public:

  //! Initialize sparrow display
  int initConsole();

  //! Finalize sparrow display
  int finiConsole();
  
  //! Console button callback
  static int onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserPause(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserFake(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserShow(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserSkip(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserDebug(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserStep(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Console button callback
  static int onUserExport(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserLines(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserCovmup(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserCovmdown(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserCovpup(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserCovpdown(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserLinemodel(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserNoupdate(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Consolo button callback
  static int onUserUpdates(cotk_t *console, RoadPerceptor *self, const char *token);

  //! Send heartbeat and manage process control requests
  void handleProcessControl();

private :

  //! Line binary pixel accumulator 
  float lineAccumulator(const Line &imageLine, const CvMat *imBin);

  //! Line binary pixel variance accumulator
  float lineAccumulatorVariance(const Line &imageLine, const CvMat *imBin);

  //! Get line color
  int getLineColor(const Line &imageLine, int sideColorCheckDistance);

  //! Get Line color between two lines
  int getLineColorBetween(const Line &line1, const Line &line2);

  //! Finds point near pi, pj, over provided line, that has good disparity
  int getBestNearestPoint(const Line &imageLine, float &pi, float &pj, float &pd, int direction);

  //! Finds point near pi, pj, over provided line, that has good disparity
  int getBestNearestPointLocal(const Line &lane, float &pi, float &pj, float &pk, int direction);

  //! From image to Local coordinates
  void imageToLocalPoint(float pi, float pj, float pd, float &px, float &py, float &pz);

  //! Convert lines from image frame to local frame and put into the roadline message
  int imageLineToMapElement(const Line &imageLine, MapElement &mE, int index);

  //! Put lines into the roadline message
  int localLineToMapElementKalman(const Line &localLine, MapElement &mE, int index);

  //! extend this line
  int extendLocalLine(const Line &imageLine, float pi, float pj, float pi2, float pj2, float &px, float &py, float &px2, float &py2);

  //! Cleans exceding map Elements
  int cleanMapElement(MapElement &mE,int index);

  //! Cleans exceding map Elements
  int cleanMapElementKalman(MapElement &mE,int index);

  //! Write detected lines to sensnet.
  int writeSensnet(vector<Line> *lines);

  //! Write detected lines to sensnet.
  int writeSensnetKalman(vector<Line> *lines);

private:

  //! paint ROI on original image
  void paintROI();

  //! determines line angle
  float getLineAngle(int startX, int startY, int endX, int endY);

  //! determines line angle
  float getLineAngle(CvPoint start, CvPoint end);
 
  //! determines line angle in local frame
  float getLineAngle(float startX, float startY, float endX, float endY);

  //! calculate distance between points
  float distance(CvPoint start, CvPoint end);
  
  //! calculate distance between points
  float distance(float startx,float starty, float endx, float endy);
  
  //! calculate distance between points
  int checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol);

  //! check if lines are parallel, within certain thresholds
  int parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol);

  //! do line matching
  int lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol);

  //! determine minimum distance between a point and a line
  float minDistBetweenPointAndLine(CvPoint point, CvPoint start, CvPoint end);

  //! merge new lines together
  vector<Line> mergeLines(vector<Line> lanes);

  //! merge new lines together
  vector<Line> mergeLocalLines(vector<Line> lanes);

  int transformationImageToLocal(const Line &imageLine, Line &localLine);

  vector<Line> transformationLocalToImage(vector<Line> lanesIn);

  Line simpleLineModel2(Line thisOldLine);

public:
  
  //! Program options
  gengetopt_args_info options;

  //! Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
   
  //! Operating mode
  enum {modeLive, modeReplay, modeBlob} mode;
  
  //! Source sensor id
  sensnet_id_t sensorId;

  //! Sensnet module
  sensnet_t *sensnet;

  //! Sensnet log replay module
  sensnet_replay_t *replay;

  //! Console display
  cotk_t *console;

  //! Should we quit?
  bool quit;
    //! Should we pause?
  bool pause;
  //! should we show the detected line?
  bool show;
  //! skip thourgh log?
  bool skip;
  //! goto option given?
  int gotoFrame;
  //! should we step through shown images by pressing a key or fall through
  bool step;
  //! should we export every frame to an image
  bool save;
  //! should we export debug images (detected and tracked lines)
  bool save_debug;
  //! should we show debug images
  bool debug;
  //! should we show debug lines
  bool lines;
  //! should we track?
  bool kalman;
  //! Current blob id
  int blobId;
  //! larger covm?
  bool covmup;
  //! smaller covm?
  bool covmdown;
  //! larger covp?
  bool covpup;
  //! larger covp?
  bool covpdown;
  //! show line model messages?
  bool linemodel; 
  //! update? 
  bool noupdate;
  //! update start as well? 
  bool updates;
  //! Current (incoming) stereo blob
  StereoImageBlob stereoBlob;

  // Diagnostics on stop lines
  int totalStops, totalFakes;
    
  //line score
  float lineScore;

  //data association
  vector<Line> dataAssociation(vector<Line> *newLines);

  // init Kalman to start tracking line
  Line initLineTrack(Line newLine); 

  // stop tracking and deallocate Kalman filter structure
  void destroyLineTrack(Line oldLine);

  // predict next location of old line
  Line predictLineTrack(Line oldLine);

  // update tracking of the detected line
  Line updateLineTrack(Line newLine, Line oldLine);

  //! map element talker to send to map
  CMapElementTalker mapElementTalker; 
  
  //! map element to send the stop line
  MapElement mapElement;

private:
    
  CvMat *imBuffer,*imBuffer1, *imBuffer2, *imOriginal, *imProcess, *imCOriginal, *imCProcess, *imCProcessTrack;
  CvMat *imCO0, *imCO1, *imCO2, *imCO3, *imC;
  CvMat *imWhite, *imYellow;

  uint64_t beatTime;
};



#endif /*LINE_PERCEPTOR_HH*/

