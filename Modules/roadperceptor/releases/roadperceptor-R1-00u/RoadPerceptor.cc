
/* 
 * Desc: Road perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard, Humberto Pereira
 * CVS: $Id$
*/

//! Unix headers
#include <signal.h>
#include <sys/stat.h>
#include <sys/times.h>

#include <iostream>
#include <fstream>

#include <iostream>
#include <fstream>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

//! Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineBlob.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>
#include <dgcutils/DGCutils.hh>

//! CLI support
#include <cotk/cotk.h>

//! Cmd-line handling
#include "cmdline.h"

//! Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"

#include "RoadPerceptor.hh"

int ROIstartX = 50;
int ROIstartY = 50;
int ROIwidth = 540;
int ROIheight = 300;

int ROIhorizonY = 175;

int DEBUG_LINES = 0;

int lanesPreviousSize = 0;
int lanesOutput = 0;

// double line parameters 
float DLSizeTol =3.0; 
float DLAngleTol =10.0;
float DLDistanceTol =12.0;

// hough transform parameters
int HoughMinAcc = 60;
int HoughMinLinePixels = 20;
int HoughMaxGap = 2;

float MinLineMeters = 0.4;

static const uint64_t HEARTBEAT_TIME = 2000000L;

//! Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

//! Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


//! Default constructor
RoadPerceptor::RoadPerceptor() {
  memset(this, 0, sizeof(*this));
  
  return;
}


//! Default destructor
RoadPerceptor::~RoadPerceptor() {
  destroyImages();
  return;
}

//! allocate memory for the images
void RoadPerceptor::initImages() {
  //! Stage Images
  
  imBuffer = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  
  imCO0 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCO1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO3 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imC = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);
  imCProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imWhite = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imYellow = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);

  //! Scale
  ROIstartX = ROIstartX * (this->stereoBlob.cols / 640);
  ROIwidth  = ROIwidth  * (this->stereoBlob.cols / 640);

  ROIstartY = ROIstartY * (this->stereoBlob.rows / 480);
  ROIheight = ROIheight * (this->stereoBlob.rows / 480);
  ROIhorizonY = ROIhorizonY * (this->stereoBlob.rows / 480);


  //! Apply Regions Of Interest
  cvGetSubRect(imCO0, imCO0, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imCO1, imCO1, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imCO2, imCO2, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer,  imBuffer,  cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer1, imBuffer1, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer2, imBuffer2, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imProcess, imProcess, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imWhite,  imWhite,  cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imYellow, imYellow, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));

  return;

}

//! deallocate memory for the image
void RoadPerceptor::destroyImages() {
  cvReleaseMat(&imBuffer1);
  cvReleaseMat(&imBuffer2);
  cvReleaseMat(&imBuffer);

  cvReleaseMat(&imC);
  cvReleaseMat(&imCO0);
  cvReleaseMat(&imCO1);
  cvReleaseMat(&imCO2);
  cvReleaseMat(&imCO3);

  cvReleaseMat(&imCProcess);
  cvReleaseMat(&imCOriginal);
  cvReleaseMat(&imProcess);
  cvReleaseMat(&imOriginal);

  cvReleaseMat(&imWhite);
  cvReleaseMat(&imYellow);

  return;

}

void RoadPerceptor::handleProcessControl() {

  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message every HEARTBEAT_TIME usecs
  if (DGCgettime() - this->beatTime > HEARTBEAT_TIME) {
    memset(&response, 0, sizeof(response));  
    response.moduleId = this->moduleId;
    response.timestamp = DGCgettime();
    response.logSize = 0;
    sensnet_write(sensnet, SENSNET_METHOD_CHUNK, this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
    this->beatTime = 0;
  }
            
  // Read process request     
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest, &blobId, sizeof(request), &request) != 0)
    return;
  if (blobId < 0)
    return;
            
  if (request.quit)
    this->quit = true;
  
}

//! Parse the command line
int RoadPerceptor::parseCmdLine(int argc, char **argv) {
  // REMOVE
  // Default configuration path
  //char defaultConfigPath[256];
  //  char filename[256];

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
  {
      //check if replay or blob mode
      if (strstr(this->options.inputs[0], ".blob") != 0)
	  this->mode = modeBlob;
      else
	  this->mode = modeReplay;
  }
  else
    this->mode = modeLive;

  //show option
  if (this->options.show_given)
      this->show = this->options.show_flag;
  else
      this->show = false;
 
  //step option
  if (this->options.step_given)
      this->step = this->options.step_flag;
  else
      this->step = false;

  //export option
  if (this->options.save_given)
      this->save = this->options.save_flag;
  else
      this->save = false;

  //export option
  if (this->options.save_debug_given)
      this->save_debug = this->options.save_debug_flag;
  else
      this->save_debug = false;

  //debug option
  if (this->options.debug_given)
      this->debug = this->options.debug_flag;
  else
      this->debug = false;
  DEBUG_LINES = this->debug;

  //goto option
  if (this->options.goto_given)
      this->gotoFrame = this->options.goto_arg;
  else
      this->gotoFrame = -1;

  this->lines = false;

  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoadPerceptor $Revision$                                                   \n"
"                                                                           \n"
"Mode: %mode%                                                               \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"Stats: Avg Rate\tPreProc\tEdge\tHough\tLine Assoc\n"
"Stats: %laneStats%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%SHOW%|%FAKE%|%SKIP%|%DEBUG%|%EXPORT%|%STEP%|%LINES%]      \n";

// Initialize console display
int RoadPerceptor::initConsole() {
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%SHOW%", " SHOW ", "Ss",
		   (cotk_callback_t) onUserShow, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);

  //button to skip through the log files
  cotk_bind_button(this->console, "%SKIP%", " SKIP ", "Kk",
                   (cotk_callback_t) onUserSkip, this);

  //button to enable/disable debug
  cotk_bind_button(this->console, "%DEBUG%", " DEBUG ", "Dd",
                   (cotk_callback_t) onUserDebug, this);

  //button to enable/disable stepping
  cotk_bind_button(this->console, "%STEP%", " STEP ", "Tt",
                   (cotk_callback_t) onUserStep, this);

  //button to export current frame
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);

  //button to print current lines
  cotk_bind_button(this->console, "%LINES%", " LINES ", "Ll",
                   (cotk_callback_t) onUserLines, this);
   
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%mode%", A_NORMAL, "%s", 
	      this->mode==modeReplay ? "Replay" : this->mode==modeBlob ? "Blob" : "Normal");


  return 0;
}


// Finalize sparrow display
int RoadPerceptor::finiConsole() {
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int RoadPerceptor::initSensnet() {

  // Start sensnet, even if we are in replay mode, since we may wish
  // to send line messages.
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  if (this->mode == modeLive) {
    // Join stereo group for live stereo data
    if (sensnet_join(this->sensnet, this->sensorId,
                     SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
      return -1;
  }
  else if (this->mode == modeReplay) {
    // Open replay of exisiting log
    this->replay = sensnet_replay_alloc();
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs)!=0)
      return -1;
  }

  //init sending to mapp
  mapElementTalker.initSendMapElement(this->skynetKey);

  return 0;
}


// Clean up sensnet
int RoadPerceptor::finiSensnet() {
  if (this->sensnet) {
    sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }

  if (this->replay) {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Read the current image
int RoadPerceptor::readImage() {
    //int blobId;

  // Live mode
  if (this->mode == modeLive) {

    //use blocking wait for new data to arrive
    if( sensnet_wait(this->sensnet, 500) != 0)
	return -1;      
    
    // Read the new blob
    if (sensnet_read(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                     &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;
  }

  // Replay mode
  else if (this->mode == modeReplay) {
    // Advance log one step
    if (sensnet_replay_next(this->replay, 0) != 0)
      return -1;
    
    // Read the new blob
    if (sensnet_replay_read(this->replay, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                            &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;

    MSG("read blob %d", this->blobId);
  }

  //modeBlob
  else if (this->mode == modeBlob) {
      //check if already read it
      if(this->stereoBlob.frameId!=0)
	  return -1;
      //load the blob from the blob file
      FILE *file;
      if ((file=fopen(this->options.inputs[0], "r")) == 0)
	  return -1;
      if (fread(&this->stereoBlob, sizeof(this->stereoBlob), 1, file)!= 1)
	  return -1;
      fclose(file);
      MSG("read blob: %d", this->stereoBlob.frameId);
  }

  // Update the display
  if (this->console) {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
} 


//! Handle button callbacks
int RoadPerceptor::onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token) {
  MSG("user quit");
  self->quit = true;
  return 0;
}


//! Handle button callbacks
int RoadPerceptor::onUserPause(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



//! Handle fake stop button
int RoadPerceptor::onUserFake(cotk_t *console, RoadPerceptor *self, const char *token) {

  return 0;
  
}


int RoadPerceptor::onUserShow(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->show = !self->show;
  MSG("show %s", (self->show ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserSkip(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->skip = !self->skip;
  MSG("skip %s", (self->skip ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserDebug(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->debug = !self->debug;
  DEBUG_LINES = self->debug;
  MSG("debug %s", (self->debug ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserStep(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->step = !self->step;
  MSG("step %s", (self->step ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserExport(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->exportImage();
  return 0;
}

int RoadPerceptor::onUserLines(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->lines = !self->lines;
  MSG("lines %s", (self->lines ? "on" : "off"));
  return 0;
}

//! export process image for an original stereo blob
int RoadPerceptor::exportImage(char *function, const CvMat *imSave) {
  exportImage("",function, imSave);
  return 0;
}

//! export process image for an original stereo blob
int RoadPerceptor::exportImage(char *dir, char *function, const CvMat *imSave) {

    char str[255];

    if (function == "") function = "sample";
    if (dir == "") dir = "output";

    sprintf(str, "%s/im-%s-%08d.png", dir, function, this->stereoBlob.frameId);
    MSG("Saving file: %s", str);
    cvSaveImage(str, imSave);
    MSG("Written file: %s", str);

    return 0;
}

//! export current image in stereo blob
int RoadPerceptor::exportImage() {
    CvMat im3 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
		      this->stereoBlob.channels==1 ? CV_8UC1 : CV_8UC3, 
		      this->stereoBlob.imageData + this->stereoBlob.leftOffset);
    
    char str[255];
    sprintf(str, "im-%08d.png", this->stereoBlob.frameId);
    cvSaveImage(str, &im3);

    MSG("Written file: %s", str);

    //write the whole stereoblob structure
    sprintf(str, "blob-%08d.blob", this->stereoBlob.frameId);
    FILE *f;
    if( (f = fopen(str, "w")) == 0)
	return -1;
    if( fwrite(&this->stereoBlob, sizeof(this->stereoBlob), 1, f) != 1)
	return -1;
    fclose(f);
    MSG("Wrote blob: %s", str);

    return 0;
}

//! Paint ROI on original image
void RoadPerceptor::paintROI() {
      CvPoint point1,point2;
      point1.x = ROIstartX;
      point1.y = ROIstartY;
      point2.x = ROIstartX + ROIwidth;
      point2.y = ROIstartY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
      point1.x = point2.x;
      point1.y = ROIstartY + ROIheight;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);  
      point2.x = ROIstartX;
      point2.y = point1.y;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8); 
      point1.x = ROIstartX;
      point1.y = ROIstartY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);  
      point1.x = ROIstartX;
      point1.y = ROIhorizonY;
      point2.x = ROIstartX + ROIwidth;
      point2.y = ROIhorizonY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
}

//! Find stop lines
int RoadPerceptor::findLines() {
  int i,j;
  uint8_t *pix;

  vector<Line> lanes;
  MapElement mE;

  //! get pointer to image data
  pix = StereoImageBlobGetLeft(&this->stereoBlob, 0, 0);
   
  CvMat rawim;

  //! used for statistics
  static long numLaneFrames=0;
  static double totalLaneTime=0.0, totalPreprocessingTime=0.0, totalEdgeTime=0.0, totalHoughTime=0.0, totalLineTime=0.0;
  double laneTime, preprocessingTime, edgeTime, houghTime, lineTime;

  //! Search for lines
  int64 startTime, endPreprocessingTime, endEdgeTime, endHoughTime, endTime;    
  startTime = cvGetTickCount();

  //! check number of channels of input image
  //! grayscale image
  if (this->stereoBlob.channels==1) {
    //! get image
    rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1, pix);
    
  } else {
    //! Get raw image. its in RGB
    rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3, pix);

    cvCopy(&rawim, imCOriginal, NULL);
    cvCopy(&rawim, imCProcess, NULL); 

    //! Convert to single channel
    cvCvtColor(imCOriginal,imOriginal,CV_RGB2GRAY);
	
    cvGetSubRect(imCOriginal, imC, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));

    //! get grayscale img
    cvCvtColor(imC,imProcess,CV_RGB2GRAY);

    if (this->debug) SHOW_IMAGE(imProcess, "0.GrayScale", 10);
    if (this->save_debug) this->exportImage("0.Grayscale", imProcess);

    //! Split channels to R+G+B
    cvSplit(imC, imCO0, imCO1, imCO2, NULL); 	    	
    
    //! WHITE CHANNEL on RGB
    //! hypothesis:  min(R,G,B)>200 && dist(min(R,G,B),max(R,G,B)) < 20 
    cvMin(imCO0,imCO1,imBuffer);
    cvMin(imBuffer,imCO2,imBuffer1);
	
    cvMax(imCO0,imCO1,imBuffer);
    cvMax(imBuffer,imCO2,imBuffer2);
	
    cvAbsDiff(imBuffer1,imBuffer2,imBuffer2);
    cvThreshold(imBuffer2,imBuffer2,20,1,CV_THRESH_BINARY_INV);
	
    cvMul(imBuffer1,imBuffer2,imBuffer1,1);
    cvThreshold(imBuffer1,imWhite,200,255,CV_THRESH_TOZERO);

    cvMorphologyEx(imWhite, imWhite, imBuffer, NULL, CV_MOP_CLOSE, 1);
    cvSmooth(imWhite, imWhite, CV_GAUSSIAN,3,3,0);
    
    if (this->debug) SHOW_IMAGE(imWhite, "1.White Channel", 10);
    if (this->save_debug) this->exportImage("1.White", imWhite);

    //! YELLO-BOOST CHANNEL
    //! hypothesis: R+G-B
    cvAddWeighted(imCO0,0.6, imCO1, 0.4, 0, imBuffer);
    cvSub(imBuffer,imCO2,imBuffer2,NULL);

    cvMorphologyEx(imYellow, imYellow, imBuffer, NULL, CV_MOP_CLOSE, 1);
    cvSmooth(imBuffer2,imYellow,CV_GAUSSIAN,3,3,0);

    if (this->debug) SHOW_IMAGE(imBuffer2, "2.YellowBoost Channel", 10);
    if (this->save_debug) this->exportImage("2.YellowBoost", imBuffer2);

    //! Add Yellow boost 
    cvAdd(imProcess,imBuffer2,imProcess,NULL);
 
    if (this->debug) SHOW_IMAGE(imProcess, "3.Gr+Y Channel",10);
    if (this->save_debug) this->exportImage("3.GrAndY", imProcess);

    //! WHITE CHANNEL ON RY: R+(GreyScale+Yellow)
    //! hypothesis:  max(R,Gr+Y)>220 && dist(min(R,Gr+Y),max(R,Gr+Y))<20 
    /*cvMin(imCO0,imProcess,imBuffer1);
    cvMax(imCO0,imProcess,imBuffer2);

    cvAbsDiff(imBuffer1,imBuffer2,imBuffer2);
    cvThreshold(imBuffer2,imBuffer,20,1,CV_THRESH_BINARY_INV);

    cvMul(imBuffer,imBuffer1,imBuffer2,1);
    */
    // cvThreshold(imBuffer2,imYellow,40,255,CV_THRESH_BINARY);
    
    if (this->debug) SHOW_IMAGE(imYellow, "4.Yellow Channel (White on R+Gr+Y)", 10);
    if (this->save_debug) this->exportImage("4.Yellow", imYellow);

    if (this->debug) SHOW_IMAGE(imProcess, "5.Pre-Processed Image", 10);
    if (this->save_debug) this->exportImage("5.PreProcessed", imProcess);
	
  }

  MSG("Processing frame#%d", this->stereoBlob.frameId);

  if(!this->skip) {
    
    endPreprocessingTime = cvGetTickCount();
    
    cvCanny(imYellow, imBuffer2, 50, 100, 3);
    cvCanny(imWhite, imBuffer1, 50, 250, 3);

    cvMax(imBuffer1,imBuffer2,imProcess);

    if (this->debug) SHOW_IMAGE(imProcess, "6.Edges", 10);
    if (this->save_debug) this->exportImage("6.Edges", imProcess);

    endEdgeTime = cvGetTickCount();

    CvMemStorage* storage = cvCreateMemStorage(0);
    CvSeq* lines = 0;

    //! Probabilistic Hough Transform to get lines
    lines = cvHoughLines2(imProcess, storage, CV_HOUGH_PROBABILISTIC, 1, 1*CV_PI/180, HoughMinAcc, HoughMinLinePixels, HoughMaxGap);

    for (i=0; i<lines->total; i++) {
      Line thisLine;
      CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
      thisLine.startPoint.x = line[0].x + ROIstartX;
      thisLine.startPoint.y = line[0].y + ROIhorizonY;
      thisLine.endPoint.x = line[1].x + ROIstartX;
      thisLine.endPoint.y = line[1].y + ROIhorizonY;
      thisLine.score = 0;
      thisLine.color = OTHER;
      lanes.push_back(thisLine);
    }

    endHoughTime = cvGetTickCount();

    //! line matching 
    for( i = 0; i < lines->total; i++ ) {
      //! ignore this line if it has been considered by a parallel one
      if ((lanes[i].score == 3 || lanes[i].score == 4) && (lanes[i].color == WHITE || lanes[i].color == YELLOW)) continue;

      CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);	
      CvPoint* line2;  

      int hasPair=0;

      //! check if there is a double line and get the best possible
      for (j=i+1; j< lines->total; j++) {

        line2 = (CvPoint*) cvGetSeqElem(lines,j);	
	hasPair = checkDouble((CvPoint) line[0],(CvPoint) line[1],(CvPoint) line2[0],(CvPoint) line2[1], DLSizeTol, DLAngleTol, DLDistanceTol);	

	//! find the best pair
	if (hasPair > 0) {
	  int color = getLineColorBetween(lanes[i],lanes[j]);
	 
	  lanes[i].color = color; 
	  if (lanes[j].color != WHITE && lanes[j].color != YELLOW ) lanes[j].color = color;

	  if( distance((CvPoint) line[0], (CvPoint) line[1]) > distance((CvPoint) line2[0], (CvPoint) line2[1]) ) {
	    lanes[i].score=4;
	    if (lanes[j].score!=4) lanes[j].score=3;
	  } else {
	    if (lanes[i].score!=4)  lanes[i].score=3;
	    lanes[j].score=4;
	  }

	  //! if line is big enough, let's still take it on caution
	} else if (distance((CvPoint) line[0], (CvPoint) line[1]) > 40) {
	  lanes[i].color = getLineColor(lanes[i], 7);
	  lanes[i].score = 2;
	} else {
	  //! remaining lines are considered unimportant
	  lanes[i].score=1;
	}
	
      }
    }
    
    endTime = cvGetTickCount();
    
    //! stats
    numLaneFrames++;

    laneTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
    preprocessingTime = 0.001*(endPreprocessingTime-startTime)/cvGetTickFrequency();
    edgeTime = 0.001*(endEdgeTime-endPreprocessingTime)/cvGetTickFrequency();
    houghTime= 0.001*(endHoughTime-endEdgeTime)/cvGetTickFrequency();
    lineTime = 0.001*(endTime-endHoughTime)/cvGetTickFrequency();

    totalLaneTime += laneTime; 
    totalPreprocessingTime += preprocessingTime;
    totalEdgeTime += edgeTime;
    totalHoughTime += houghTime;
    totalLineTime += lineTime;

    //! display
    if (this->console) {
          cotk_printf(this->console, "%laneStats%", A_NORMAL, "%.2fHz\t%.2fms\t%.2fms\t%.2fms\t%.2fms", numLaneFrames/totalLaneTime*1000, totalPreprocessingTime/numLaneFrames, totalEdgeTime/numLaneFrames, totalHoughTime/numLaneFrames, totalLineTime/numLaneFrames);
    } else {
      MSG("Lane line takes %.2fms\tavg %.2fms\t%.2fHz", laneTime,totalLaneTime/numLaneFrames, numLaneFrames/totalLaneTime*1000);
    }

  } 


  //! show detected lines
  if (this->show) {    
    int key=cvWaitKey( this->step ? 0 : 10);
    if (key=='e' || key=='E')
      this->exportImage();
    else if (key == 'q' || key == 'Q')
      this->onUserQuit(console, this, "");
    else if (key == 'p' || key == 'P')
      this->onUserPause(console, this, "");
    else if (key == 't' || key=='T')
      this->onUserStep(console, this, "");
//       else if (key == 'k' || key == 'K') //skip	 
// 	  return 0;
  }

  //not showing, so export the image
  if (this->save) {
    // this->exportImage();
  }
    
  for (i = 0; i<(int)lanes.size(); i++) {

    //! display lines with blue nearby line-markers
    if (this->show || this->save_debug || this->lines) {
      CvPoint point1,point2;
      point1.x = int(lanes[i].startPoint.x);
      point1.y = int(lanes[i].startPoint.y);
      point2.x = int(lanes[i].endPoint.x);
      point2.y = int(lanes[i].endPoint.y);

      if (this->show || this->save_debug) {
	if (lanes[i].score <2 ) cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8 );
	if (lanes[i].color == GREEN) cvLine(imCProcess, point1, point2, CV_RGB(0,255,0), 1, 8 );
	if (lanes[i].color == WHITE) cvLine(imCProcess, point1, point2, CV_RGB(255,255,255), 1, 8 );
	if (lanes[i].color == YELLOW) cvLine(imCProcess, point1, point2, CV_RGB(255,255,0), 1, 8 );
	if (lanes[i].color == OTHER) cvLine(imCProcess, point1, point2, CV_RGB(255,0,255), 1, 8 );
      }

      char *color;
      //! print line stats
      if (this->lines) {
      
	switch (lanes[i].color) {
	case GREEN:
	  color = "green";
	  break;	
	case WHITE:
	  color = "white";
	  break;
	case YELLOW:
	  color = "yellow";
	  break;
	case OTHER:
	  color = "other";
	  break;
	default: 
	  color = "unkown";
	}
	MSG("line %d: (%d,%d)->(%d,%d) %s score:%d", i, point1.x, point1.y, point2.x, point2.y, color, lanes[i].score);
      }
 
      if (this->show || this->save_debug) {
	if (lanes[i].color == WHITE || lanes[i].color == YELLOW) {
	  point1.x ++;
	  point1.y ++;
	  point2.x ++;
	  point2.y ++;
	  cvLine(imCProcess, point1, point2, CV_RGB(0,0,255), 1, 8 );
	  point1.x -=2;
	  point1.y -=2;
	  point2.x -=2;
	  point2.y -=2;
	  cvLine(imCProcess, point1, point2, CV_RGB(0,0,255), 1, 8 );
	}
      }
    }

    //! lanes that are not supposed to be output must be cleaned
    if (lanes[i].color == GREEN || lanes[i].color == OTHER || lanes[i].score <2) {
      cleanMapElement(mE, i);
      continue;
    }
    
    //! if for some reason the line was not accepted in localFrame, clean i
    if (imageLineToMapElement(lanes[i], mE, i)==-1) {
      cleanMapElement(mE, i);
      continue;
    }
           
    // Send lines as map elements
    imageLineToMapElement(lanes[i], mE, i);
  }

  // Send lines as sensnet blob
  this->writeSensnet(&lanes);

  if (this->show || this->save_debug) {
    paintROI();
    if (this->show) SHOW_IMAGE(imCProcess, "7.Original Image with superimposed lines", 10);
    if (this->save_debug) this->exportImage("7.Lines", imCProcess);
  }
  
  if (int(lanes.size()) < lanesPreviousSize) {
    for (i=lanes.size(); i < lanesPreviousSize; i++) {
      cleanMapElement(mE, i);
    }
  }

  MSG("%d lines output out of %d lines", lanesOutput, int(lanes.size()));
  lanesOutput = 0;
  
  lanesPreviousSize = lanes.size();
  lanes.clear();
  
  return 0;
}

//! return the accumulator of a line on an image
float RoadPerceptor::lineAccumulator(const Line &imageLine, const CvMat *imBin) {
  int x, y, i;
  int size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));
  CvScalar p=cvScalar(0.0), a=cvScalar(0.0);
    
  for (i=0; i<size; i++) {
    x = int(imageLine.startPoint.x + i*(imageLine.endPoint.x-imageLine.startPoint.x)/size);
    y = int(imageLine.startPoint.y + i*(imageLine.endPoint.y-imageLine.startPoint.y)/size);
    
    // in the middle
    x=x-ROIstartX;
    y=y-ROIhorizonY;

    if (x<0) x=0;
    if (y<0) y=0;
    if (x >= ROIwidth) x = ROIwidth -1;
    if (y >= ROIheight-(ROIhorizonY-ROIstartY)) y = ROIheight-(ROIhorizonY-ROIstartY) -1;   

    p = cvGet2D(imBin, y, x);
    a.val[0] += p.val[0];
  }

  a.val[0] = round(a.val[0]/size);   // average
  return a.val[0];
}

//! Returns the line color in between two lines
int RoadPerceptor::getLineColorBetween(const Line &line1, const Line &line2) {

  Line lineBetween;
  Line side1;
  Line side2;

  //! center of line1, 2 and lineBetween
  float m1x, m1y, m2x, m2y, mBx, mBy;

  //! point in yellow channel: average for self, left and right
  float a_y, a_y_l, a_y_r;
  //! point in white channel: average for self, left and right
  float a_w, a_w_l, a_w_r; 

  if (distance(line1.startPoint.x, line1.startPoint.y, line2.startPoint.x, line2.startPoint.y) < distance(line1.startPoint.x, line1.startPoint.y, line2.endPoint.x, line2.endPoint.y)) {
    
    lineBetween.startPoint.x = (line1.startPoint.x + line2.startPoint.x)/2;
    lineBetween.startPoint.y = (line1.startPoint.y + line2.startPoint.y)/2;
    lineBetween.endPoint.x = (line1.endPoint.x + line2.endPoint.x)/2;
    lineBetween.endPoint.y = (line1.endPoint.y + line2.endPoint.y)/2 ;

  } else {
    lineBetween.startPoint.x = (line1.startPoint.x + line2.endPoint.x)/2;
    lineBetween.startPoint.y = (line1.startPoint.y + line2.endPoint.y)/2;
    lineBetween.endPoint.x = (line1.endPoint.x + line2.startPoint.x)/2;
    lineBetween.endPoint.y = (line1.endPoint.y + line2.startPoint.y)/2 ;
  }

  m1x = 0.5*(line1.startPoint.x + line1.endPoint.x);
  m1y = 0.5*(line1.startPoint.y + line1.endPoint.y);
  m2x = 0.5*(line2.startPoint.x + line2.endPoint.x);
  m2y = 0.5*(line2.startPoint.y + line2.endPoint.y);
  mBx = 0.5*(lineBetween.startPoint.x + lineBetween.endPoint.x);
  mBy = 0.5*(lineBetween.startPoint.y + lineBetween.endPoint.y);


  side1.startPoint.x = line1.startPoint.x + (m1x-mBx);
  side1.startPoint.y = line1.startPoint.y + (m1y-mBy);
  side1.endPoint.x = line1.endPoint.x + (m1x-mBx);
  side1.endPoint.y = line1.endPoint.y + (m1y-mBy);

  side2.startPoint.x = line2.startPoint.x + (m2x-mBx);
  side2.startPoint.y = line2.startPoint.y + (m2y-mBy);
  side2.endPoint.x = line2.endPoint.x + (m2x-mBx);
  side2.endPoint.y = line2.endPoint.y + (m2y-mBy);

  a_y = lineAccumulator(lineBetween, imYellow); // Y middle
  a_w = lineAccumulator(lineBetween, imWhite);  // W middle

  a_y_l = lineAccumulator(side1, imYellow); // Y left
  a_w_l = lineAccumulator(side2, imWhite);  // W left

  a_y_r = lineAccumulator(side2, imYellow); // Y right
  a_w_r = lineAccumulator(side2, imWhite);  // W right

  MSG("%f %f %f", a_y_l, a_y, a_y_r);
  if ((a_w > a_w_l + 20) && (a_w > a_w_r + 20)) {
    return WHITE;
  } 
  if ((a_y > a_y_l + 15) && (a_y > a_y_r + 15)){
    return YELLOW;
  }

  return OTHER;

}

//! Returns the line color 
int RoadPerceptor::getLineColor(const Line &imageLine, int sideColorCheckDistance) {

  Line leftLine, rightLine;

  //! point in yellow channel: average for self, left and right
  float a_y, a_y_l, a_y_r;
  //! point in white channel: average for self, left and right
  float a_w, a_w_l, a_w_r; 

  int xExtend=0, yExtend=0;

  float ang = getLineAngle(int(imageLine.startPoint.x), int(imageLine.startPoint.y), int(imageLine.endPoint.x), int(imageLine.endPoint.y));

  
  if (ang < -60) {
    xExtend = sideColorCheckDistance;
    yExtend = 0;
  } else if (ang < -30 ) {
    xExtend = sideColorCheckDistance;
    yExtend = -sideColorCheckDistance;
  } else if (ang < 0) {
    xExtend = 0;
    yExtend = - sideColorCheckDistance;
  } else if (ang < 30) {
    xExtend = 0;
    yExtend = sideColorCheckDistance;
  } else if (ang < 60) {
    xExtend = sideColorCheckDistance;
    yExtend = sideColorCheckDistance;
  } else {
    xExtend = sideColorCheckDistance;
    yExtend = 0;
  }

  a_y = lineAccumulator(imageLine, imYellow); // Y middle
  a_w = lineAccumulator(imageLine, imWhite);  // W middle

  leftLine.startPoint.x = imageLine.startPoint.x - xExtend;
  leftLine.endPoint.x = imageLine.endPoint.x - xExtend;
  leftLine.startPoint.y = imageLine.startPoint.y - yExtend;
  leftLine.endPoint.y = imageLine.endPoint.y - yExtend;

  a_y_l = lineAccumulator(leftLine, imYellow); // Y left
  a_w_l = lineAccumulator(leftLine, imWhite);  // W left

  rightLine.startPoint.x = imageLine.startPoint.x + xExtend;
  rightLine.endPoint.x = imageLine.endPoint.x + xExtend;
  rightLine.startPoint.y = imageLine.startPoint.y + yExtend;
  rightLine.endPoint.y = imageLine.endPoint.y + yExtend;
 
  a_y_r = lineAccumulator(rightLine, imYellow); // Y right
  a_w_r = lineAccumulator(rightLine, imWhite);  // W right

  if ((a_w > a_w_l + 30) && (a_w > a_w_r + 30)) {
    return WHITE;
  } 
  if ((a_y > a_y_l + 20) && (a_y > a_y_r + 20)){
    return YELLOW;
  }

  return OTHER;
}

float min3(float a, float b, float c) {
  if (min(a,b)==a && min(a,c)==a) return a; 
  if (min(a,b)==b && min(b,c)==b) return b; 
  return c;
}

//! extend this line
int RoadPerceptor::extendLocalLine(const Line &imageLine, float pi, float pj, float pi2, float pj2, float &px, float &py, float &px2, float &py2) {
  
  float sizeRatio;
  float wingSize;
  
  float lineSize;
  float sinTeta;
  float cosTeta;
  float direction;

  lineSize = sqrt(pow(px2-px,2)+pow(py2-py,2));
  sinTeta = fabs(py2-py)/lineSize;
  cosTeta = fabs(px2-px)/lineSize;

  //! Start wing
  if (imageLine.startPoint.x != pi || imageLine.startPoint.y != pj) {
    sizeRatio = sqrt(pow(pi-imageLine.startPoint.x,2)+pow(pj-imageLine.startPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;
    direction = (px-px2)/fabs(px-px2);
    px = px + direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py = py + direction*wingSize*sinTeta;
  }

  //! end wing
  if (imageLine.endPoint.x != pi2 || imageLine.endPoint.y != pj2) {
    sizeRatio = sqrt(pow(pi2-imageLine.endPoint.x,2)+pow(pj2-imageLine.endPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;

    direction = (px-px2)/fabs(px-px2);
    px2 = px2 - direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py2 = py2 - direction*wingSize*sinTeta;
    
  }

  return 1;
}

//! finds point near to provided, over line, that has good disparity
int RoadPerceptor::getBestNearestPoint(const Line &imageLine, float & pi, float & pj, float  & pd, int direction) {

  uint16_t testDisp;
  float px, py, pz;

  int size=0, i=0;

  float dist=0;
  float beginX=0, beginY=0;
  float endX=0, endY=0;
  
  size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));

    if (direction == 1) {
      beginX = imageLine.startPoint.x;
      beginY = imageLine.startPoint.y;
      endX = imageLine.endPoint.x;
      endY = imageLine.endPoint.y;
    } else {
      beginX = imageLine.endPoint.x;
      beginY = imageLine.endPoint.y;
      endX = imageLine.startPoint.x;
      endY = imageLine.startPoint.y;
    }
    
    //! try to get disparity from other points in line
    for (i=0; i<size; i++) {
      pi = beginX + i*direction*(imageLine.endPoint.x-imageLine.startPoint.x)/size;
      pj = beginY + i*direction*(imageLine.endPoint.y-imageLine.startPoint.y)/size;

     //! we just walked the whole line, don't check this point
      if (pi == endX && pj == endY) return 0;

      testDisp = (*StereoImageBlobGetDisp(&this->stereoBlob, (int) pi, (int) pj));
      
      //! good disparity, lets stick with this point
      if (testDisp>0 && testDisp != 65535) {
	pd = (float) testDisp;
	pd /= this->stereoBlob.dispScale;
	imageToLocalPoint(pi, pj, pd, px, py, pz);
	
	//! check if the point is on alice or too far away (lousy but valid disparity)
	dist = sqrt(pow(this->stereoBlob.state.localX-px,2)+pow(this->stereoBlob.state.localY-py,2));
	
	//! only consider valid disparity points
	if (dist > 1 && dist < 60) {
	  return 1;
	}
      }
    }

    return 0;
}

//! Convert image frame point to local point
void RoadPerceptor::imageToLocalPoint(float pi, float pj, float pd, float &px, float &py, float &pz) {

  float sx, sy, sz;
  float vx, vy, vz;
  
  //! Compute point in the sensor frame
  StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &sx, &sy, &sz);
  //! Convert to vehicle frame
  StereoImageBlobSensorToVehicle(&this->stereoBlob, sx, sy, sz, &vx, &vy, &vz);
  //! Convert to local frame
  StereoImageBlobVehicleToLocal(&this->stereoBlob, vx, vy, vz, &px, &py, &pz);

  return;
}


//! cleans exceding mapElements
int RoadPerceptor::cleanMapElement(MapElement &mE,int index) {
  mE.clear();
  mE.setId(this->moduleId, index);

  this->mapElementTalker.sendMapElement(&mE, 0);

  return 1;
}


//! converts from a line in image coordinates to a line in local coordinates
int RoadPerceptor::imageLineToMapElement(const Line &imageLine, MapElement &mE, int index) {
    float pi, pj, pi2, pj2, pd;
    float px, py, pz, px2, py2, pz2;

    point2arr points;

    mE.clear();

    //! start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    
    //! check if it's a good point or there is a good point nearby
    if (!getBestNearestPoint(imageLine, pi, pj, pd, 1)) return 0;

    //! get the local frame coordinates for the best nearest point
    imageToLocalPoint(pi, pj, pd, px, py, pz);

    // to high, probably an obstacle. TODO: better model for obstacles
    //if (pz>0.5) return -1;
    
    //end point
    pi2 = imageLine.endPoint.x;
    pj2 = imageLine.endPoint.y;

    if (!getBestNearestPoint(imageLine, pi2, pj2, pd, -1)) return -1;

    // only one point on the line, discard it.
    if (pi == pi2 && pj == pj2) return -1;

    imageToLocalPoint(pi2, pj2, pd, px2, py2, pz2);
    
    //! to high, probably an obstacle
    //if (pz2>0.5) return -1;

    //! get real line-end points
    float dist1, dist2;
    dist1 = distance(px,py,px2,py2);
    extendLocalLine(imageLine, pi, pj, pi2, pj2, px, py, px2, py2);
    dist2 = distance(px,py,px2,py2);
    
    if (this->debug) MSG("line grew %d",int(round(100*(dist2-dist1)/dist1)));
    
    //! short lines don't get displayed
    if (distance(px,py,px2,py2)< MinLineMeters) return -1;

    points.push_back(point2(px, py));
    points.push_back(point2(px2,py2));

    mE.setId(this->moduleId, index);  
    mE.setGeometry(points);

    mE.height=0;
    if (imageLine.color == GREEN) mE.plotColor = MAP_COLOR_GREEN;
    if (imageLine.color == WHITE) mE.plotColor = MAP_COLOR_GREY;
    if (imageLine.color == YELLOW) mE.plotColor = MAP_COLOR_YELLOW;
    if (imageLine.color == OTHER) mE.plotColor = MAP_COLOR_ORANGE;
 
    mE.type = ELEMENT_LANELINE;
    mE.geometryType = GEOMETRY_LINE;
    mE.state = this->stereoBlob.state;
    
    this->mapElementTalker.sendMapElement(&mE, 0); 
    
    lanesOutput ++;
    
    points.clear();
    mE.clear();
    return 1;
}


//! Write detected lines to sensnet
int RoadPerceptor::writeSensnet(vector<Line> *lines)
{
  int i;
  RoadLineBlob blob;
  Line imageLine;
  float pi, pj, pi2, pj2, pd;
  float px, py, pz, px2, py2, pz2;
  int blobSize;

  // Set up blob header
  blob.blobType = SENSNET_ROAD_LINE_BLOB;
  blob.version = ROAD_LINE_BLOB_VERSION;
  blob.sensnetId = SENSNET_STEREO_LINE_PERCEPTOR;
  blob.frameId = this->stereoBlob.frameId;
  blob.timestamp = this->stereoBlob.timestamp;
  blob.state = this->stereoBlob.state;
  blob.numLines = 0;
  
  for (i = 0; i < (int) lines->size(); i++)
  {
    imageLine = (*lines)[i];
    
    //! start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    
    //! check if it's a good point or there is a good point nearby
    if (!getBestNearestPoint(imageLine, pi, pj, pd, 1))
      continue;

    //! get the local frame coordinates for the best nearest point
    imageToLocalPoint(pi, pj, pd, px, py, pz);
    
    //end point
    pi2 = imageLine.endPoint.x;
    pj2 = imageLine.endPoint.y;

    if (!getBestNearestPoint(imageLine, pi2, pj2, pd, -1))
      continue;

    // only one point on the line, discard it.
    if (pi == pi2 && pj == pj2)
      continue;

    imageToLocalPoint(pi2, pj2, pd, px2, py2, pz2);
    
    //! to high, probably an obstacle
    //if (pz2>0.5)
    //  continue;

    // get real line-end points
    extendLocalLine(imageLine, pi, pj, pi2, pj2, px, py, px2, py2);
    
    // Copy line data
    if (blob.numLines >= ROAD_LINE_BLOB_MAX_LINES)
      break;
    blob.lines[blob.numLines].type = ROAD_LINE_BLOB_TYPE_UNKNOWN;
    blob.lines[blob.numLines].ax = px;
    blob.lines[blob.numLines].ay = py;
    blob.lines[blob.numLines].az = pz;
    blob.lines[blob.numLines].bx = px2;
    blob.lines[blob.numLines].by = py2;
    blob.lines[blob.numLines].bz = pz2;
    blob.numLines++;
  }

  // Compute the size of the blob that was actually used
  blobSize = sizeof(blob) - sizeof(blob.lines) + blob.numLines*sizeof(blob.lines[0]);

  // Publish the data
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK,
                    blob.sensnetId, blob.blobType, blob.frameId, blobSize, &blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}


/*---------------------------------------------*/
//! HELPER FUNCTIONS
/*---------------------------------------------*/

float RoadPerceptor::getLineAngle(int startX, int startY, int endX, int endY) {
  CvPoint start;
  CvPoint end;
  
  return getLineAngle(start,end);

}

float RoadPerceptor::getLineAngle(CvPoint start, CvPoint end) {
  float angle=0.0;
	
  //! starting point always has lower or equal X coordinate
  if (start.x == end.x) {
    angle = 90.0;
  } else {
    angle = 180/CV_PI * atan((float) (end.y-start.y)/(end.x-start.x));
  }
  
  //! Y grows downwards
  return -angle;
}

float RoadPerceptor::distance(CvPoint start, CvPoint end) {
	return sqrt(pow((double) (start.x-end.x),2)+pow((double) (start.y-end.y),2));
}

float RoadPerceptor::distance(float startx,float starty, float endx, float endy) {
	return sqrt(pow((double) (startx-endx),2)+pow((double) (starty-endy),2));
}

int RoadPerceptor::checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol) {
	//! size tolerance trial
	float size1 = distance(start1, end1);
	float size2 = distance(start2, end2);
	if (max(size1,size2)/min(size1,size2) > sizeTol || min(size1,size2) < 20) {
	  return -1;
	}

	//! angle tolerance trial
	if (!parallel(start1, end1, start2, end2, angleTol)) {
	  return -2;
	} 

	//! line convergence and alignment	
	if (!lineMatching(start1, end1, start2, end2, distanceTol)) {
	  return -3;
	}

	// All trials passed. brillo
		
	return 1;
}

int RoadPerceptor::parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol) {
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	float angleDiff = fabs(angle1-angle2);
	if (!(angleDiff<=angleTol || (180.0-angleDiff)<=angleTol)) return 0;

	return 1;
	
}

int RoadPerceptor::lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol) {
	/*
	What we know: 
	a. Points in a line are ordered by lower X.
	   a1. start1.x > end1.x
	   a2. start2.x > end2.x
	b. line1 and line2 are almost parallel (this is after the angle tolerance trial)
	*/

	//! extreme 1 and 2 distance
	//! closer points at extremes
	float dE1, dE2;
	//! the bigger and smaller of dE1 and dE2
	float dMax, dMin;

	if (distance(start1,start2) <  distance(start1,end2)) {
	  //! lines may be offset. we know start1 and start2 are closer. determine minimum distance between each of them and the respective other line.
	  dE1 = min( minDistBetweenPointAndLine(start2,start1,end1),minDistBetweenPointAndLine(start1,start2,end2));
 	  dE2 = min( minDistBetweenPointAndLine(end2,start1,end1),minDistBetweenPointAndLine(end1,start2,end2));
	  	  
	} else {
	  //! lines may be offset. we know start1 and end2 are closer. determine minimum distance between each of them and the respective other line.
	  dE1 = min( minDistBetweenPointAndLine(end2,start1,end1),minDistBetweenPointAndLine(start1,start2,end2));
 	  dE2 = min( minDistBetweenPointAndLine(end1,start1,end1),minDistBetweenPointAndLine(start2,start2,end2));
	}

	dMax = dE1 >= dE2 ? dE1 : dE2;
	dMin = dMax == dE1 ? dE2 : dE1;

	if (dMax > distanceTol || dMax < 0.5 ) return 0;
		
	return 1;
}

float RoadPerceptor::minDistBetweenPointAndLine(CvPoint point, CvPoint start, CvPoint end) {
  float d=0,d_n, size=0;
  int i;

  size = distance(start,end);
  d = distance(start,point);
  for (i=0; i<size; i++) {
    d_n = distance(start.x+i*(end.x-start.x)/size, start.y+i*(end.y-start.y)/size, point.x, point.y);
    if (d_n>d) {
      return d;
    }
    d=d_n;
  }
  return d;

}

/*---------------------------------------------*/
//! MAIN FUNCTION
/*---------------------------------------------*/

// Main program thread
int main(int argc, char **argv) {
  RoadPerceptor *percept;


  percept = new RoadPerceptor();
  assert(percept);

  //  memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  // Get a single image for initializing the images
  while (percept->readImage() != 0) {
    percept->readImage();
  }
  // Initialize images
  percept->initImages();

  while (!percept->quit) {
    //delay for a while
    if (percept->replay) {    
	//usleep(500);
    }

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // Run process control
    percept->handleProcessControl();

    // If we are paused, dont do anything
    if (percept->pause) {
      usleep(0);
      continue;
    }

    // Check for new data
    if (percept->readImage() == 0) {

	//check if goto frame given
	if (percept->gotoFrame!=-1 && percept->stereoBlob.frameId < percept->gotoFrame)
	    continue;
	
	// Detect lines
	percept->findLines();  

    }
  }

  percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



