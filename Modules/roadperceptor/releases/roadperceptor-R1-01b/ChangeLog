Tue Aug 12 20:50:07 2008	Mariette Annergren (annergren)

	* version R1-01b
	BUGS:  
	FILES: RoadPerceptor.cc(48602)
	1: Changed Canny threshold to the El Toro environment, meaning they
	are more sensitive. 2: Implemented the mergelines in local frame.
	3: Changed the imageLineToMapElement back to its original version,
	so if running with kalman flag off, you get the original
	RoadPerceptor. 

Mon Aug 11 23:11:36 2008	Mariette Annergren (annergren)

	* version R1-01a
	BUGS:  
	FILES: RoadPerceptor.cc(48524)
	1: Changed the transImageToLocal. It now only uses
	getBestNearestPoint on the startPoint. 2: The Canny thresholds are
	set to new sensitive values adjusted for El Toro. 3: The mapElement
	receives the covariances if the line has been updated, zero if it
	has just been initalised and some made up values if no kalman.
	TODO: 1: Change the mergeLines to local frame. 2: Set a config-file
	with the Canny thresholds. PROBLEM: No spurious lines detected so
	far. Lines disappear some times due to a bad disparity at the end
	point. 

	FILES: RoadPerceptor.cc(48529)
	1: Transformations; Have done a modification of an earlier version
	of the transformations, now only the point that is closest to Alice
	uses getBestNearestPoint.

	FILES: RoadPerceptor.cc(48538), RoadPerceptor.hh(48538)
	1: Divided imageLineToMapElement into two functions,
	imageLineToMapElement and localLineToMapElementKalman. The first
	one takes care of the lines when no kalman-flag, it uses the same
	transformations as transImageToLocal. The second one takes care of
	the lines when kalman-flag is on. It takes the lines directly after
	dataAssociation and sends them as map elements, meaning there is no
	unnecessary transformations back and forth as before. 2: The Canny
	thresholds are set to St. Luke.

	FILES: RoadPerceptor.cc(48561), RoadPerceptor.hh(48561)
	1: Have changed the thresholds for Canny again, it is till for St.
	Luke. 2: Have made a function for merging lines in local frame
	before it gets sent to localToMapElementKalman but it is for now
	commented out. 3: Changed the minimum length for displayment of
	lines as map elements, 0.4 -> 0.6.

Mon Aug 11  9:57:10 2008	Mariette Annergren (annergren)

	* version R1-01
	BUGS:  
	FILES: RoadPerceptor.cc(48219)
	Changed the made up covariances to the real ones obtained from the
	Kalman filter structure. These are as, before sent, further within
	the map element.

	FILES: RoadPerceptor.cc(48243), RoadPerceptor.hh(48243)
	1: The transformations has been changed, only the point closest to
	Alice use the getBestNearestPoint, meanning an angle check has been
	implemented on all the new detected lines. 2: The same condition
	for transformed lines before dataAssociation is stated once more
	after it to ensure that no "bad" lines comes from the
	update/correct algorithm. 3: The Kalman structure has been
	re-arranged. The first prediction occurs in updateLineTrack, not in
	initLineTrack, meaning that the prediction and updating steps has
	switch places. 4: Map elements yet again gets fake values, will fix
	this in next svn-commit. 5: Changed the condition on line 1337,
	&&->||. 6: POST_FRAMES has been set to 0, so that the behaviour of
	that would not get confused with something else, will fix this in
	next svn-commit.

	FILES: RoadPerceptor.cc(48329), RoadPerceptor.hh(48329)
	1: Changed the transformations from image to local frame and vice
	versa. They are now functions defined in RoadPerceptor and have
	been moved into dataAssociation. This was done to avoid drifting of
	lines due to their coordinates being stored in the local frame,
	this frame changes over time. 2: The map element receives the
	covariances from the Kalman structure. If a line has not passed
	through the structure the covariances are set to zero. 3: Have not
	yet gotten the post frames to work. It is still set to zero. 4: The
	flag "updates" does no longer do what it is said to do. Do not use
	it for now. 5: Need to check the color filter. The edge detection
	missclassify new painted yellow lines as white.

	FILES: RoadPerceptor.cc(48333), RoadPerceptor.hh(48333)
	1: Fixed "updates"-flag. If on it does not update the end point
	that is closest to Alice, if off it updates both end points.

	FILES: RoadPerceptor.cc(48334), RoadPerceptor.hh(48334)
	1: The simple line model is now a function in RoadPerceptor. Input:
	old line, output: deltaX, deltaY, deltaZ. 2: Post frames are still
	set to zero.

	FILES: RoadPerceptor.cc(48335), RoadPerceptor.hh(48335)
	1: The prediction part of updateLineTrack is now a function of its
	own, predictLineTrack. 2: simpleLineModel and predictLineTrack is
	now used on every single old line for every new frame, this to
	prevent from using velocities that are old. 

	FILES: RoadPerceptor.cc(48384), RoadPerceptor.hh(48384)
	Problem: Transformations do not work. It is not yet possible to use
	postFrames or update the same line for several frames. *Kalman
	structure is now setting the updated line to the predicted one. *A
	prediction is made in every frame to ensure correct speed and
	coordinates. *simpleLineModel is calculating the deltas as it
	should.

	FILES: RoadPerceptor.cc(48475), RoadPerceptor.hh(48475)
	Problem: Transformations are still not working. Q and R are both
	set to 10e-3 transformationImageToLocal uses getNearestPoint and
	imageToLocalPoint

	FILES: RoadPerceptor.cc(48492)
	Problem: Transformations. Changed: Thresholds in Canny edge
	detection to match the environment at El Toro. Threshold in
	mergeLines.

Wed Jul 23 21:44:17 2008	Mariette Annergren (annergren)

	* version R1-00z
	BUGS:  
	FILES: RoadPerceptor.cc(48074)
	1: Changed condition for z to 20 from 0.5 at line 1150 (To avoid
	lines NOT entering dataAssociation). 2: Changed the console
	template so the second button row gets displayed. 3: Fixed bug by
	adding an if-statement line 1432.

Tue Jul 22  2:57:04 2008	Mariette Annergren (annergren)

	* version R1-00y
	BUGS:  
	FILES: Makefile.yam(47933), RoadPerceptor.cc(47933),
		RoadPerceptor.hh(47933), cmdline.c(47933),
		cmdline.ggo(47933), cmdline.h(47933)
	Fixed merging

	FILES: Makefile.yam(47912), RoadPerceptor.cc(47912),
		RoadPerceptor.hh(47912), cmdline.c(47912),
		cmdline.ggo(47912), cmdline.h(47912)
	New implementations: kalman, merging, inheritance and post frames.
	These can be reached with -kalman.

Thu Oct 25 21:48:38 2007	Humberto Pereira (tallberto)

	* version R1-00x
	BUGS:  
	FILES: RoadPerceptor.cc(46776)
	fixed bug of previous release that would send no lines

Thu Oct 25  2:26:58 2007	Humberto Pereira (tallberto)

	* version R1-00w
	BUGS:  
	FILES: RoadPerceptor.cc(46522), RoadPerceptor.hh(46522)
	added variance filter to *possibly* reduce spurry lines

Thu Oct 18 16:55:41 2007	Humberto Pereira (tallberto)

	* version R1-00v
	BUGS:  
	FILES: RoadPerceptor.cc(45364)
	performance tune

Thu Oct 18  3:51:05 2007	Humberto Pereira (tallberto)

	* version R1-00u
	BUGS:  
	FILES: RoadPerceptor.cc(45265)
	line intensity tunning

Wed Oct 17 19:47:04 2007	Humberto Pereira (tallberto)

	* version R1-00t
	BUGS:  
	FILES: RoadPerceptor.cc(45189)
	separated Edge detecor on White and Yellow channels, to assign
	different params to each. should get more lines even in dim light,
	specially in El Toro.

Tue Oct 16 16:54:49 2007	Humberto Pereira (tallberto)

	* version R1-00s
	BUGS:  
	FILES: RoadPerceptor.cc(44999)
	more logging options. code clean

Sat Oct 13 21:07:27 2007	Humberto Pereira (tallberto)

	* version R1-00r
	BUGS:  
	FILES: RoadPerceptor.cc(43589)
	changed cvSmooth to cvClose operation

	FILES: RoadPerceptor.cc(44438)
	better model for paralell lines that works better and
	FASTER.corrected one bug in log outputs.

Sat Oct  6 21:53:56 2007	Humberto Pereira (tallberto)

	* version R1-00q
	BUGS:  
	FILES: RoadPerceptor.cc(42909), RoadPerceptor.hh(42909)
	added option to export images from every single frame and process.
	crashes if output directory is not present

	FILES: RoadPerceptor.cc(42916), RoadPerceptor.hh(42916)
	cleaned or removed all previous tracking code

	FILES: RoadPerceptor.cc(42917), RoadPerceptor.hh(42917)
	added debug code for lines. a small misclassification on some right
	lines persists

	FILES: RoadPerceptor.cc(42937)
	small fix

	FILES: RoadPerceptor.cc(42945), RoadPerceptor.hh(42945)
	small changes in color tracking. found the bug causing some right
	lines not being output. not corrected yet

	FILES: RoadPerceptor.cc(43083), RoadPerceptor.hh(43083)
	the bug with right lanes not getting detected is due to the angle
	calculations not having any particular correct order of points from
	openCV

	FILES: RoadPerceptor.cc(43364)
	fixed bug in lineExtend and all bugs in paralel colors.

Sun Sep 30 14:37:24 2007	Humberto Pereira (tallberto)

	* version R1-00p
	BUGS:  
	FILES: RoadPerceptor.cc(41896), RoadPerceptor.hh(41896)
	merging with andrews changes on sending RoadLine Blob

	FILES: RoadPerceptor.cc(40170)
	change in yellow channel compensation

	FILES: RoadPerceptor.cc(40596)
	time statistics

	FILES: RoadPerceptor.cc(40911)
	fixes in mapElement cleaning. function cleaning.

	FILES: RoadPerceptor.cc(41232), RoadPerceptor.hh(41232)
	added code for rescalling of the image in case *someone* changes
	the resolution or the size of the stereoBlobs. line color more
	robust with white channel comparizon. changed yellow boost to:
	yellow OR grayscale, which reduces artifact glare.

	FILES: RoadPerceptor.cc(41284), RoadPerceptor.hh(41284)
	small change in yellow channel

	FILES: RoadPerceptor.cc(41311)
	changed image sources for edge detection: White Channel + Yellow
	Channel (White Channel on RGreyY, not on RGB). Adjusted Hough
	Transform parameters for line detection on such edge image(and
	works on segmented lines too). Lines that are on the WHite channel
	are taggeed white, the remaining are tested on the Yellow Channel
	(that includes white). Added minimum distance for parallel lines (3
	pixels). some more changes that I don't remember.

Fri Sep 28 18:35:22 2007	abhoward (abhoward)

	* version R1-00o
	BUGS:  
	FILES: RoadPerceptor.cc(40598)
	Minor tweak for config path test

	FILES: RoadPerceptor.cc(41059), RoadPerceptor.hh(41059)
	Added support for writing road line blobs

	FILES: RoadPerceptor.cc(41231)
	Removed some debug messages

Fri Sep 21 16:22:57 2007	Humberto Pereira (tallberto)

	* version R1-00n
	BUGS:  
	FILES: RoadPerceptor.cc(39873), RoadPerceptor.hh(39873)
	better lane state reporting on console

	FILES: RoadPerceptor.cc(39874)
	fixed all warnings

Fri Sep 21 15:02:23 2007	Humberto Pereira (tallberto)

	* version R1-00m
	BUGS:  
	FILES: Makefile.yam(39677), RoadPerceptor.cc(39677),
		RoadPerceptor.hh(39677)
	added heartbeat and process control compatibility

	FILES: RoadPerceptor.cc(39845)
	fixed small bug with health reporting

Tue Sep 18 16:57:34 2007	Humberto Pereira (tallberto)

	* version R1-00l
	BUGS:  
	FILES: RoadPerceptor.cc(39363), RoadPerceptor.hh(39363)
	merge with old changes

	FILES: RoadPerceptor.cc(39345), RoadPerceptor.hh(39345)
	correct memory leaks, crash bugs. added lines for better
	visualisation. added horizon line to reduce number of detect lines
	and improve performance.

Sat Sep 15  0:58:06 2007	Laura Lindzey (lindzey)

	* version R1-00k
	BUGS:  
	FILES: RoadPerceptor.cc(38893), cmdline.c(38893),
		cmdline.ggo(38893), cmdline.h(38893)
	changing default module id in .ggo file to be MODroadPerceptor 
	when sending elements to the map, first int in the ID is now
	MODroadPerceptor. 

Sun Sep  2  3:30:31 2007	Humberto Pereira (tallberto)

	* version R1-00j
	BUGS:  
	FILES: RoadPerceptor.cc(37030), RoadPerceptor.hh(37030)
	implemented much better model for double parallel lines, than can
	overlap partially, be inverted in coordinates, have mixed
	configurations and segments.

	FILES: RoadPerceptor.cc(37031)
	new color scheme implemented, gets better double lines but needs
	tweaking in color

Sat Sep  1 15:21:21 2007	Humberto Pereira (tallberto)

	* version R1-00i
	BUGS:  
	FILES: RoadPerceptor.cc(36988), RoadPerceptor.hh(36988)
	Calculate color in between lines for double lines. Single line
	color matching according to Andrews distribution scheme not yet
	fully working.

Thu Aug 30 15:59:40 2007	Humberto Pereira (tallberto)

	* version R1-00h
	BUGS:  
	Deleted files: CameraInfo.conf CameraInfoOpt.c CameraInfoOpt.ggo
		CameraInfoOpt.h InversePerspectiveMapping.cc
		InversePerspectiveMapping.hh StopLinePerceptor.cc
		StopLinePerceptor.conf StopLinePerceptor.hh
		StopLinePerceptorOpt.c StopLinePerceptorOpt.ggo
		StopLinePerceptorOpt.h StopLinePerceptorTest.cc ranker.h
		test.sh
	FILES: Makefile.yam(35003), RoadPerceptor.cc(35003),
		RoadPerceptor.hh(35003)
	code cleaning, deleted old files added by Mohamed/Andrew.still
	works.

	FILES: RoadPerceptor.cc(34745), RoadPerceptor.hh(34745)
	lines cleaning. fixed bug where long lines got skewed. 

	FILES: RoadPerceptor.cc(34987), RoadPerceptor.hh(34987)
	fixed bugs related to color channels. They look swapped but really
	arent.

	FILES: RoadPerceptor.cc(35345)
	great yellow channel computed for color boost.

	FILES: RoadPerceptor.cc(35468), RoadPerceptor.hh(35468)
	yellow tinkering, try to fix memory leak failed

	FILES: RoadPerceptor.cc(36368), RoadPerceptor.hh(36368)
	double line check. memory allocation bug corrected. green line
	filtering.

	FILES: RoadPerceptor.cc(36592), RoadPerceptor.hh(36592)
	added white and yellow channels, color selection throught simple
	distance algorithm. change that, RGB is not linear. many bugs
	correct with extending lines, double lines, green fltering.

Thu Aug 16 17:11:22 2007	Humberto Pereira (tallberto)

	* version R1-00g
	BUGS:  
	FILES: RoadPerceptor.cc(33506), cmdline.ggo(33506)
	fixed channels bug. output for lines seems to be better. test it.

	FILES: RoadPerceptor.cc(33954)
	fixed method for getting disparity from inner points of lines whose
	extremes don't have it (good disparity)

Fri Aug 10  9:57:50 2007	Humberto Pereira (tallberto)

	* version R1-00f
	BUGS:  
	FILES: InversePerspectiveMapping.hh(32913),
		RoadPerceptor.cc(32913), RoadPerceptor.hh(32913)
	changes in export to mapviewer

	FILES: Makefile.yam(32632), RoadPerceptor.cc(32632, 32657),
		RoadPerceptor.hh(32632)
	fixed jpl vision includes. To not use them anymore.

	FILES: RoadPerceptor.cc(32658)
	fixed color swapping bug

Sun Aug  5 13:05:47 2007	Humberto Pereira (tallberto)

	* version R1-00e
	BUGS:  
	FILES: README(32060)
	making changes to the release notes

	FILES: README(32060)
	making changes to the release notes

Sat Aug  4 23:11:18 2007	Humberto Pereira (tallberto)

	* version R1-00d
	BUGS:  
	New files: CameraInfo.conf CameraInfoOpt.c CameraInfoOpt.ggo
		CameraInfoOpt.h InversePerspectiveMapping.cc
		InversePerspectiveMapping.hh LanePerceptor.conf
		RoadPerceptor.hh StopLinePerceptor.cc
		StopLinePerceptor.conf StopLinePerceptor.hh
		StopLinePerceptorOpt.c StopLinePerceptorOpt.ggo
		StopLinePerceptorOpt.h StopLinePerceptorTest.cc logReader.c
		mcv.cc mcv.hh ranker.h test.sh
	FILES: Makefile.yam(31395), README(31395), RoadPerceptor.cc(31395),
		cmdline.c(31395), cmdline.ggo(31395), cmdline.h(31395)
	LinePerceptor copied to RoadPerceptor and changes made to classes

	FILES: RoadPerceptor.cc(31805)
	3 stages ready. data association is next

Tue Mar 20 16:22:54 2007	Sam Pfister (sam)

	* version R1-00c
	BUGS: 
	FILES: RoadPerceptor.cc(18760)
	changed SENSNET_STEREO_BLOB to SENSNET_STEREO_IMAGE_BLOB in
	RoadPerceptor.cc to get it to compile.

Wed Feb 21 22:17:00 2007	Sam Pfister (sam)

	* version R1-00b
	BUGS: 
	FILES: RoadPerceptor.cc(15395)
	updated to work with new skynet release

Thu Feb  8 17:23:15 2007	Mohamed Aly (malaa)

	* version R1-00a
	BUGS: 
	New files: RoadPerceptor.cc cmdline.c cmdline.ggo cmdline.h
	FILES: Makefile.yam(14791)
	A template for raodperceptor

Thu Feb  8 15:23:26 2007	Mohamed Aly (malaa)

	* version R1-00
	Created roadperceptor module.




































