/* 
 * Desc: Road perceptor with tracking run-time 
 * Date: summer 2008
 * Author: Mariette Annergren (tracking), Andrew Howard (original), Humberto Pereira (original)
 * CVS: $Id$
*/

//! Unix headers
#include <signal.h>
#include <sys/stat.h>
#include <sys/times.h>

#include <iostream>
#include <fstream>

#include <iostream>
#include <fstream>
#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

//! Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineBlob.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>
#include <dgcutils/DGCutils.hh>

//! CLI support
#include <cotk/cotk.h>

//! Cmd-line handling
#include "cmdline.h"

//! Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"

#include "RoadPerceptor.hh"

//! parameters for region of interest
int ROIstartX = 50;
int ROIstartY = 50;
int ROIwidth = 540;
int ROIheight = 300;

int ROIhorizonY = 175;

int DEBUG_LINES = 0;

int lanesPreviousSize = 0;
int lanesOutput = 0;

// double line parameters 
float DLSizeTol =3.0; 
float DLAngleTol =10.0;
float DLDistanceTol =12.0;

// hough transform parameters

//minimum quality the line must have
int HoughMinAcc = 60;
//minimum linelength
int HoughMinLinePixels = 20;
//maximum gap between two line segments for them to be treated as one
int HoughMaxGap = 2;

// edge detection parameters
/*
El-Toro (sensitive): 50 100 50 250
St-Luke (conservative): 80 250 80 250
*/
int EdgeYellowLinking = 50; 
int EdgeYellowStrong = 100;
int EdgeWhiteLinking = 50;
int EdgeWhiteStrong = 250;


//! minimum length for line to be displayed on map
float MinLineMeters = 0.4;

static const uint64_t HEARTBEAT_TIME = 2000000L;

//! Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

//! Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


//! Default constructor
RoadPerceptor::RoadPerceptor() {
  memset(this, 0, sizeof(*this));
  
  return;
}


//! Default destructor
RoadPerceptor::~RoadPerceptor() {
  destroyImages();
  return;
}

//! allocate memory for the images
void RoadPerceptor::initImages() {
  //! Stage Images
  
  imBuffer = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  
  imCO0 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCO1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO3 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imC = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);
  imCProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);
  imCProcessTrack = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imWhite = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imYellow = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);

  //! Scale
  ROIstartX = ROIstartX * (this->stereoBlob.cols / 640);
  ROIwidth  = ROIwidth  * (this->stereoBlob.cols / 640);

  ROIstartY = ROIstartY * (this->stereoBlob.rows / 480);
  ROIheight = ROIheight * (this->stereoBlob.rows / 480);
  ROIhorizonY = ROIhorizonY * (this->stereoBlob.rows / 480);


  //! Apply Regions Of Interest
  cvGetSubRect(imCO0, imCO0, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imCO1, imCO1, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imCO2, imCO2, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer,  imBuffer,  cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer1, imBuffer1, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imBuffer2, imBuffer2, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imProcess, imProcess, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imWhite,  imWhite,  cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));
  cvGetSubRect(imYellow, imYellow, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));

  return;

}

//! deallocate memory for the image
void RoadPerceptor::destroyImages() {
  cvReleaseMat(&imBuffer1);
  cvReleaseMat(&imBuffer2);
  cvReleaseMat(&imBuffer);

  cvReleaseMat(&imC);
  cvReleaseMat(&imCO0);
  cvReleaseMat(&imCO1);
  cvReleaseMat(&imCO2);
  cvReleaseMat(&imCO3);

  cvReleaseMat(&imCProcess);
  cvReleaseMat(&imCProcessTrack);
  cvReleaseMat(&imCOriginal);
  cvReleaseMat(&imProcess);
  cvReleaseMat(&imOriginal);

  cvReleaseMat(&imWhite);
  cvReleaseMat(&imYellow);

  return;

}

void RoadPerceptor::handleProcessControl() {

  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message every HEARTBEAT_TIME usecs
  if (DGCgettime() - this->beatTime > HEARTBEAT_TIME) {
    memset(&response, 0, sizeof(response));  
    response.moduleId = this->moduleId;
    response.timestamp = DGCgettime();
    response.logSize = 0;
    sensnet_write(sensnet, SENSNET_METHOD_CHUNK, this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
    this->beatTime = 0;
  }
            
  // Read process request     
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest, &blobId, sizeof(request), &request) != 0)
    return;
  if (blobId < 0)
    return;
            
  if (request.quit)
    this->quit = true;
  
}

//! Parse the command line
int RoadPerceptor::parseCmdLine(int argc, char **argv) {

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
  {
      //check if replay or blob mode
      if (strstr(this->options.inputs[0], ".blob") != 0)
	  this->mode = modeBlob;
      else
	  this->mode = modeReplay;
  }
  else
    this->mode = modeLive;

  //show option
  if (this->options.show_given)
      this->show = this->options.show_flag;
  else
      this->show = false;
 
  //step option
  if (this->options.step_given)
      this->step = this->options.step_flag;
  else
      this->step = false;

  //export option
  if (this->options.save_given)
      this->save = this->options.save_flag;
  else
      this->save = false;

  //export option
  if (this->options.save_debug_given)
      this->save_debug = this->options.save_debug_flag;
  else
      this->save_debug = false;

  //debug option
  if (this->options.debug_given)
      this->debug = this->options.debug_flag;
  else
      this->debug = false;
  DEBUG_LINES = this->debug;

  //goto option
  if (this->options.goto_given)
      this->gotoFrame = this->options.goto_arg;
  else
      this->gotoFrame = -1;

  this->lines = false;

  // kalman options
  if (this->options.kalman_given)
    this->kalman = this->options.kalman_flag;
  else
    this->kalman = false;

  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoadPerceptor $Revision$                                                   \n"
"                                                                           \n"
"Mode: %mode%                                                               \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"Stats: Avg Rate\tPreProc\tEdge\tHough\tLine Assoc\n"
"Stats: %laneStats%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%SHOW%|%FAKE%|%SKIP%|%DEBUG%|%EXPORT%|%STEP%|%LINES%]      \n"
"[%COVM+%|%COVM-%|%COVP+%|%COVP-%|%LINEMODEL%|%NOUPDATE%|%UPDATES%]         \n";

// Initialize console display
int RoadPerceptor::initConsole() {
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%SHOW%", " SHOW ", "Ss",
		   (cotk_callback_t) onUserShow, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);

  //button to skip through the log files
  cotk_bind_button(this->console, "%SKIP%", " SKIP ", "Kk",
                   (cotk_callback_t) onUserSkip, this);

  //button to enable/disable debug
  cotk_bind_button(this->console, "%DEBUG%", " DEBUG ", "Dd",
                   (cotk_callback_t) onUserDebug, this);

  //button to enable/disable stepping
  cotk_bind_button(this->console, "%STEP%", " STEP ", "Tt",
                   (cotk_callback_t) onUserStep, this);

  //button to export current frame
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);

  //button to print current lines
  cotk_bind_button(this->console, "%LINES%", " LINES ", "Ll",
                   (cotk_callback_t) onUserLines, this);
   
  //button to multiply cov m with 10
  cotk_bind_button(this->console, "%COVM+%", " COVM+ ", "M+",
                   (cotk_callback_t) onUserCovmup, this);

  //button to divide cov m with 10
  cotk_bind_button(this->console, "%COVM-%", " COVM- ", "M-",
                   (cotk_callback_t) onUserCovmdown, this);

  //button to multiply cov p with 10
  cotk_bind_button(this->console, "%COVP+%", " COVP+ ", "P+",
		  (cotk_callback_t) onUserCovpup, this);

  //button to divide cov p with 10
  cotk_bind_button(this->console, "%COVP-%", " COVP- ", "P-",
                   (cotk_callback_t) onUserCovpdown, this);

  //button to show line model messages
  cotk_bind_button(this->console, "%LINEMODEL%", " LINEMODEL ", "Lm",
                   (cotk_callback_t) onUserLinemodel, this);  

  //button to disable/enable update
  cotk_bind_button(this->console, "%NOUPDATE%", " NOUPDATE ", "Nu",
                   (cotk_callback_t) onUserNoupdate, this); 

  //button to disable/enable update
  cotk_bind_button(this->console, "%UPDATES%", " UPDATES ", "Ud",
                   (cotk_callback_t) onUserUpdates, this);               

  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%mode%", A_NORMAL, "%s", 
	      this->mode==modeReplay ? "Replay" : this->mode==modeBlob ? "Blob" : "Normal");


  return 0;
}


// Finalize sparrow display
int RoadPerceptor::finiConsole() {
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int RoadPerceptor::initSensnet() {

  // Start sensnet, even if we are in replay mode, since we may wish
  // to send line messages.
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  if (this->mode == modeLive) {
    // Join stereo group for live stereo data
    if (sensnet_join(this->sensnet, this->sensorId,
                     SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
      return -1;
  }
  else if (this->mode == modeReplay) {
    // Open replay of exisiting log
    this->replay = sensnet_replay_alloc();
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs)!=0)
      return -1;
  }

  //init sending to mapp
  mapElementTalker.initSendMapElement(this->skynetKey);

  return 0;
}


// Clean up sensnet
int RoadPerceptor::finiSensnet() {
  if (this->sensnet) {
    sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }

  if (this->replay) {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Read the current image
int RoadPerceptor::readImage() {
    //int blobId;

  // Live mode
  if (this->mode == modeLive) {

    //use blocking wait for new data to arrive
    if( sensnet_wait(this->sensnet, 500) != 0)
	return -1;      
    
    // Read the new blob
    if (sensnet_read(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                     &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;
  }

  // Replay mode
  else if (this->mode == modeReplay) {
    // Advance log one step
    if (sensnet_replay_next(this->replay, 0) != 0)
      return -1;
    
    // Read the new blob
    if (sensnet_replay_read(this->replay, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                            &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;

    MSG("read blob %d", this->blobId);
  }

  //modeBlob
  else if (this->mode == modeBlob) {
      //check if already read it
      if(this->stereoBlob.frameId!=0)
	  return -1;
      //load the blob from the blob file
      FILE *file;
      if ((file=fopen(this->options.inputs[0], "r")) == 0)
	  return -1;
      if (fread(&this->stereoBlob, sizeof(this->stereoBlob), 1, file)!= 1)
	  return -1;
      fclose(file);
      MSG("read blob: %d", this->stereoBlob.frameId);
  }

  // Update the display
  if (this->console) {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
} 


//! Handle button callbacks
int RoadPerceptor::onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token) {
  MSG("user quit");
  self->quit = true;
  return 0;
}


//! Handle button callbacks
int RoadPerceptor::onUserPause(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->pause = !self->pause;
  //  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



//! Handle fake stop button
int RoadPerceptor::onUserFake(cotk_t *console, RoadPerceptor *self, const char *token) {

  return 0;
  
}


int RoadPerceptor::onUserShow(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->show = !self->show;
  MSG("show %s", (self->show ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserSkip(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->skip = !self->skip;
  MSG("skip %s", (self->skip ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserDebug(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->debug = !self->debug;
  DEBUG_LINES = self->debug;
  MSG("debug %s", (self->debug ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserStep(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->step = !self->step;
  MSG("step %s", (self->step ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserExport(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->exportImage();
  return 0;
}

int RoadPerceptor::onUserLines(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->lines = !self->lines;
  MSG("lines %s", (self->lines ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserLinemodel(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->linemodel = !self->linemodel;
  MSG("linemodel debug %s", (self->linemodel ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserUpdates(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->updates = !self->updates;
  MSG("update start %s", (self->updates ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserNoupdate(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->noupdate = !self->noupdate;
  MSG("noupdate %s", (self->noupdate ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserCovmup(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->covmup=true;
  self->covmdown=false;
  self->covpup=false;
  self->covpdown=false;

  return 0;
}
int RoadPerceptor::onUserCovmdown(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->covmdown=true;
  self->covmup=false;
  self->covpup=false;
  self->covpdown=false;
  return 0;
}
int RoadPerceptor::onUserCovpup(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->covpup=true;
  self->covmup=false;
  self->covmdown=false;
  self->covpdown=false;
  return 0;
}
int RoadPerceptor::onUserCovpdown(cotk_t *console, RoadPerceptor *self, const char *token) {
  self->covpdown=true;
  self->covmup=false;
  self->covmdown=false;
  self->covpup=false;
  return 0;
}
//! export process image for an original stereo blob
int RoadPerceptor::exportImage(char *function, const CvMat *imSave) {
  exportImage("",function, imSave);
  return 0;
}

//! export process image for an original stereo blob
int RoadPerceptor::exportImage(char *dir, char *function, const CvMat *imSave) {

    char str[255];

    if (function == "") function = "sample";
    if (dir == "") dir = "output";

    sprintf(str, "%s/im-%s-%08d.png", dir, function, this->stereoBlob.frameId);
    MSG("Saving file: %s", str);
    cvSaveImage(str, imSave);
    MSG("Written file: %s", str);

    return 0;
}

//! export current image in stereo blob
int RoadPerceptor::exportImage() {
    CvMat im3 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
		      this->stereoBlob.channels==1 ? CV_8UC1 : CV_8UC3, 
		      this->stereoBlob.imageData + this->stereoBlob.leftOffset);
    
    char str[255];
    sprintf(str, "im-%08d.png", this->stereoBlob.frameId);
    cvSaveImage(str, &im3);

    MSG("Written file: %s", str);

    //write the whole stereoblob structure
    sprintf(str, "blob-%08d.blob", this->stereoBlob.frameId);
    FILE *f;
    if( (f = fopen(str, "w")) == 0)
	return -1;
    if( fwrite(&this->stereoBlob, sizeof(this->stereoBlob), 1, f) != 1)
	return -1;
    fclose(f);
    MSG("Wrote blob: %s", str);

    return 0;
}

//! Paint ROI on original image
void RoadPerceptor::paintROI() {
      CvPoint point1,point2;
      point1.x = ROIstartX;
      point1.y = ROIstartY;
      point2.x = ROIstartX + ROIwidth;
      point2.y = ROIstartY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
      cvLine(imCProcessTrack, point1, point2, CV_RGB(255,0,0), 1, 8);
      point1.x = point2.x;
      point1.y = ROIstartY + ROIheight;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8); 
      cvLine(imCProcessTrack, point1, point2, CV_RGB(255,0,0), 1, 8);
      point2.x = ROIstartX;
      point2.y = point1.y;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
      cvLine(imCProcessTrack, point1, point2, CV_RGB(255,0,0), 1, 8); 
      point1.x = ROIstartX;
      point1.y = ROIstartY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8); 
      cvLine(imCProcessTrack, point1, point2, CV_RGB(255,0,0), 1, 8); 
      point1.x = ROIstartX;
      point1.y = ROIhorizonY;
      point2.x = ROIstartX + ROIwidth;
      point2.y = ROIhorizonY;
      cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8);
      cvLine(imCProcessTrack, point1, point2, CV_RGB(255,0,0), 1, 8);
}

/*--------------------------------------------------------------------------------------------*/
/* THE FUNCTION THAT DETECTS LINES IN THE IMAGE FRAME*/

int RoadPerceptor::findLines() {
  int i,j;
  uint8_t *pix;
  vector<Line> lanesCorrected;
  vector<Line> lanesIn, lanesOut;
  vector<Line> lanes;
  MapElement mE;

  //! get pointer to image data
  pix = StereoImageBlobGetLeft(&this->stereoBlob, 0, 0);
   
  CvMat rawim;

  //! used for statistics
  static long numLaneFrames=0;
  static double totalLaneTime=0.0, totalPreprocessingTime=0.0, totalEdgeTime=0.0, totalHoughTime=0.0, totalLineTime=0.0;
  double laneTime, preprocessingTime, edgeTime, houghTime, lineTime;

  //! Search for lines
  int64 startTime, endPreprocessingTime, endEdgeTime, endHoughTime, endTime;    
  startTime = cvGetTickCount();

  //! check number of channels of input image
  //! if # channels 1 it is a grayscale image
  if (this->stereoBlob.channels==1) {
    //! get image and then go directly to edge extraction (Canny)
    rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1, pix);
    
  } else {
    //! if #channels!=1 it is an rgb image, pre-process and then go to edge extraction (Canny)
    rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3, pix);

    //! copy rawimage to imCOriginal, imCProcess and imCProcessTrack
    cvCopy(&rawim, imCOriginal, NULL);
    cvCopy(&rawim, imCProcess, NULL); 
    cvCopy(&rawim, imCProcessTrack, NULL); 

    //! Convert to single channel (imOriginal is the grayscale version of imCOriginal)
    cvCvtColor(imCOriginal,imOriginal,CV_RGB2GRAY);

    //! gets the region of interest from imCOriginal copied to imC	
    cvGetSubRect(imCOriginal, imC, cvRect(ROIstartX, ROIhorizonY, ROIwidth, ROIheight-(ROIhorizonY-ROIstartY)));

   //! convert to single channel (imProcess is the grayscale version of imC)
    cvCvtColor(imC,imProcess,CV_RGB2GRAY);

    if (this->debug) SHOW_IMAGE(imProcess, "0.GrayScale", 10);
    if (this->save_debug) this->exportImage("0.Grayscale", imProcess);

    //! split channels in imC to R:imCO0, G:imCO1 and B:imCO2
    cvSplit(imC, imCO0, imCO1, imCO2, NULL); 	    	
    
    //! WHITE CHANNEL on RGB
    //! hypothesis:  min(R,G,B)>200 && dist(min(R,G,B),max(R,G,B)) < 20 
    //! min of R:imCO0 and G:imCO1 is stored in imBuffer
    cvMin(imCO0,imCO1,imBuffer);
    //! min of imBuffer and B:imCO2 is stored in imBuffer1
    cvMin(imBuffer,imCO2,imBuffer1);
    //! max of R:imCO0 and G:imCO1 is stored in imBuffer 	
    cvMax(imCO0,imCO1,imBuffer);
    //! max of imBuffer and B:imCO2 is stored in imBuffer2
    cvMax(imBuffer,imCO2,imBuffer2);

   //! calculate dist(min(R,G,B),max(R,G,B) and save the result in imBuffer2 	
    cvAbsDiff(imBuffer1,imBuffer2,imBuffer2);
    //! remove intensity values larger than threshold
    cvThreshold(imBuffer2,imBuffer2,20,1,CV_THRESH_BINARY_INV);
	
    //! multiply imBuffer1 (min) with imBuffer2 (acc. dist.) and save the result in imBuffer1	
    cvMul(imBuffer1,imBuffer2,imBuffer1,1);
    //! remove intensity values less than threshold (200), store result in imWhite
    cvThreshold(imBuffer1,imWhite,200,255,CV_THRESH_TOZERO);

    cvMorphologyEx(imWhite, imWhite, imBuffer, NULL, CV_MOP_CLOSE, 1);
    cvSmooth(imWhite, imWhite, CV_GAUSSIAN,3,3,0);
    
    if (this->debug) SHOW_IMAGE(imWhite, "1.White Channel", 10);
    if (this->save_debug) this->exportImage("1.White", imWhite);

    //! YELLO-BOOST CHANNEL
    //! hypothesis: yellow= 0.6*red+0.4*green-blue

    //! save 0.6*red+0.4*green in imBuffer 
    cvAddWeighted(imCO0,0.6, imCO1, 0.4, 0, imBuffer);
    //! subtract B:imCO2 from imBuffer and save the result in imBuffer2
    cvSub(imBuffer,imCO2,imBuffer2,NULL);

    cvMorphologyEx(imYellow, imYellow, imBuffer, NULL, CV_MOP_CLOSE, 1);
    cvSmooth(imBuffer2,imYellow,CV_GAUSSIAN,3,3,0);

    if (this->debug) SHOW_IMAGE(imBuffer2, "2.YellowBoost Channel", 10);
    if (this->save_debug) this->exportImage("2.YellowBoost", imBuffer2);


    cvCopy(imProcess, imBuffer1, NULL);
    //! add Yellow boost by adding the yellow imBuffer2 to the grayscale imProcess 
    cvCopy(imProcess, imBuffer1, NULL);
    cvAdd(imProcess,imBuffer2,imProcess,NULL);
 
    if (this->debug) SHOW_IMAGE(imProcess, "3.Gr+Y Channel",10);
    if (this->save_debug) this->exportImage("3.GrAndY", imProcess);

    //! WHITE CHANNEL ON RY: R+(GreyScale+Yellow)
    //! hypothesis:  max(R,Gr+Y)>220 && dist(min(R,Gr+Y),max(R,Gr+Y))<20 

//     cvMin(imCO0,imProcess,imBuffer1);
//     cvMax(imCO0,imProcess,imBuffer2);

//     cvAbsDiff(imBuffer1,imBuffer2,imBuffer2);
//     cvThreshold(imBuffer2,imBuffer,20,1,CV_THRESH_BINARY_INV);

//     cvMul(imBuffer,imBuffer1,imBuffer2,1);
    
    // cvThreshold(imBuffer2,imYellow,40,255,CV_THRESH_BINARY);

    
    if (this->debug) SHOW_IMAGE(imYellow, "4.Yellow Channel (White on R+Gr+Y)", 10);
    if (this->save_debug) this->exportImage("4.Yellow", imYellow);

    if (this->debug) SHOW_IMAGE(imProcess, "5.Pre-Processed Image", 10);
    if (this->save_debug) this->exportImage("5.PreProcessed", imProcess);
	
  }

  MSG("Processing frame#%d", this->stereoBlob.frameId);

  if(!this->skip) {

    
    endPreprocessingTime = cvGetTickCount();
    
    cvCanny(imYellow, imBuffer2, EdgeYellowLinking, EdgeYellowStrong, 3);
    cvCanny(imWhite, imBuffer1, EdgeWhiteLinking, EdgeWhiteStrong, 3);
 
    cvMax(imBuffer1,imBuffer2,imProcess);

    
    endPreprocessingTime = cvGetTickCount();

    //! implement Canny edge detector 
    //! 1 arg:src image, 2 arg:dst image, 3 arg:low threshold to get linking edges
    //! 4 arg:high threshold to get strong edges, 5 arg:aperture parameter for Sobel operator    
    cvCanny(imYellow, imBuffer2, EdgeYellowLinking, EdgeYellowStrong, 3);
    cvCanny(imWhite, imBuffer1, EdgeWhiteLinking, EdgeWhiteStrong, 3);
 
    cvMax(imBuffer1,imBuffer2,imProcess);

    if (this->debug) SHOW_IMAGE(imProcess, "6.Edges", 10);
    if (this->save_debug) this->exportImage("6.Edges", imProcess);

    endEdgeTime = cvGetTickCount();

    CvMemStorage* storage = cvCreateMemStorage(0);
    CvSeq* lines = 0;

    //! return a sequence of line segments defined by their start and end points
    //! 1 arg:src, 2 arg:dst image, 3 arg:method, 4/5 arg:resulotion for rho/theta, 
    //! 6 arg:minimum quality, 7 arg:minimum line length, 8 arg:max gap for line 
    lines = cvHoughLines2(imProcess, storage, CV_HOUGH_PROBABILISTIC, 1, 1*CV_PI/180, HoughMinAcc, HoughMinLinePixels, HoughMaxGap);

    for (i=0; i<lines->total; i++) {
      Line thisLine;
      CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
      thisLine.startPoint.x = line[0].x + ROIstartX;
      thisLine.startPoint.y = line[0].y + ROIhorizonY;
      thisLine.endPoint.x = line[1].x + ROIstartX;
      thisLine.endPoint.y = line[1].y + ROIhorizonY;
      thisLine.score = 0;
      thisLine.color = OTHER;
      lanes.push_back(thisLine);
    }

    endHoughTime = cvGetTickCount();

    //! line matching 
    for( i = 0; i < lines->total; i++ ) {
      //! ignore this line if it has been considered by a parallel one
      //! meaning if it has been given a score>2 or a color other than OTHER
      if ((lanes[i].score == 3 || lanes[i].score == 4) && (lanes[i].color == WHITE || lanes[i].color == YELLOW)) continue;
      
      //! set line to the current i-line
      CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);	

      //! initialize necessary parameters
      CvPoint* line2;  
      int hasPair=0;

      //! check if there is a double line and get the best possible
      for (j=i+1; j< lines->total; j++) {

        //! set line2 to the current j-line
        line2 = (CvPoint*) cvGetSeqElem(lines,j);
        //! check if current i and j lines form a pair, if so hasPair returns a positive number		
	hasPair = checkDouble((CvPoint) line[0],(CvPoint) line[1],(CvPoint) line2[0],(CvPoint) line2[1], DLSizeTol, DLAngleTol, DLDistanceTol);	

	//! assign score and color to the lines that are considered a pair
	if (hasPair > 0) {

          //! return color of the line in the middle of the pair
	  int color = getLineColorBetween(lanes[i],lanes[j]);

          //! assign the color in the middle to i-line	 
	  lanes[i].color = color; 

          //! assign the color in the middle to j-line if not already WHITE or YELOW
	  if (lanes[j].color != WHITE && lanes[j].color != YELLOW ) lanes[j].color = color;

          //! the largest line of the two is assigned score=4 the other score=3 
	  if( distance((CvPoint) line[0], (CvPoint) line[1]) > distance((CvPoint) line2[0], (CvPoint) line2[1]) ) {
	    lanes[i].score=4;
	    if (lanes[j].score!=4) lanes[j].score=3;
	  } else {
	    if (lanes[i].score!=4)  lanes[i].score=3;
	    lanes[j].score=4;
	  }

	  //! if line does not form a pair with anyone but is big enough assign score=2 and color
	} else if (distance((CvPoint) line[0], (CvPoint) line[1]) > 40) {
	  lanes[i].color = getLineColor(lanes[i], 7);
	  lanes[i].score = 2;
	} else {
	  //! if hasPair<0 and size<40 line is considered unimportant
	  //! meaning it is assigned score 1 to show that it has been considered
	  lanes[i].score=1;
	}
	
      }
      lanes[i].postFrames=0;// to avoid unconditional jump in dataAssociation

      lanes[i].startMaxVar = 0;// set value, if it will not get reset in update
      lanes[i].startMinVar = 0;
      lanes[i].startAxis = 0;
      lanes[i].endMaxVar = 0;
      lanes[i].endMinVar = 0;
      lanes[i].endAxis = 0;
    }

    /* ALL THE LINES DETECTED ARE STORED IN LANES */
    /*-----------------------------------------------------------------------------------------------*/

    endTime = cvGetTickCount();
    
    //! stats
    numLaneFrames++;

    laneTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
    preprocessingTime = 0.001*(endPreprocessingTime-startTime)/cvGetTickFrequency();
    edgeTime = 0.001*(endEdgeTime-endPreprocessingTime)/cvGetTickFrequency();
    houghTime= 0.001*(endHoughTime-endEdgeTime)/cvGetTickFrequency();
    lineTime = 0.001*(endTime-endHoughTime)/cvGetTickFrequency();

    totalLaneTime += laneTime; 
    totalPreprocessingTime += preprocessingTime;
    totalEdgeTime += edgeTime;
    totalHoughTime += houghTime;
    totalLineTime += lineTime;

    //! display
    if (this->console) {
          cotk_printf(this->console, "%laneStats%", A_NORMAL, "%.2fHz\t%.2fms\t%.2fms\t%.2fms\t%.2fms", numLaneFrames/totalLaneTime*1000, totalPreprocessingTime/numLaneFrames, totalEdgeTime/numLaneFrames, totalHoughTime/numLaneFrames, totalLineTime/numLaneFrames);
    } else {
      MSG("Lane line takes %.2fms\tavg %.2fms\t%.2fHz", laneTime,totalLaneTime/numLaneFrames, numLaneFrames/totalLaneTime*1000);
    }

  } 


  //! show detected lines
  if (this->show) {    
    int key=cvWaitKey( this->step ? 0 : 10);
    if (key=='e' || key=='E')
      this->exportImage();
    else if (key == 'q' || key == 'Q')
      this->onUserQuit(console, this, "");
    else if (key == 'p' || key == 'P')
      this->onUserPause(console, this, "");
    else if (key == 't' || key=='T')
      this->onUserStep(console, this, "");
//       else if (key == 'k' || key == 'K') //skip	 
// 	  return 0;
  }

  //not showing, so export the image
  if (this->save) {
    // this->exportImage();
  }
    
  for (i = 0; i<(int)lanes.size(); i++) {

    //! display lines with blue nearby line-markers
    if (this->show || this->save_debug || this->lines) {
      CvPoint point1,point2;
      point1.x = int(lanes[i].startPoint.x);
      point1.y = int(lanes[i].startPoint.y);
      point2.x = int(lanes[i].endPoint.x);
      point2.y = int(lanes[i].endPoint.y);

      if (this->show || this->save_debug) {
	//	if (lanes[i].score <2 ) cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8 );
	//	if (lanes[i].color == GREEN) cvLine(imCProcess, point1, point2, CV_RGB(0,255,0), 1, 8 );
	if (lanes[i].color == WHITE) cvLine(imCProcess, point1, point2, CV_RGB(255,255,255), 1, 8 );
	if (lanes[i].color == YELLOW) cvLine(imCProcess, point1, point2, CV_RGB(255,255,0), 1, 8 );
	//	if (lanes[i].color == OTHER) cvLine(imCProcess, point1, point2, CV_RGB(255,0,255), 1, 8 );
      }

      char *color;
      //! print line stats
      if (this->lines) {
      
	switch (lanes[i].color) {
	case GREEN:
	  color = "green";
	  break;	
	case WHITE:
	  color = "white";
	  break;
	case YELLOW:
	  color = "yellow";
	  break;
	case OTHER:
	  color = "other";
	  break;
	default: 
	  color = "unkown";
	}
	MSG("line %d: (%d,%d)->(%d,%d) %s score:%d", i, point1.x, point1.y, point2.x, point2.y, color, lanes[i].score);
      }
 
      if (this->show || this->save_debug) {
	if (lanes[i].color == WHITE || lanes[i].color == YELLOW) {
	  point1.x ++;
	  point1.y ++;
	  point2.x ++;
	  point2.y ++;
	  cvLine(imCProcess, point1, point2, CV_RGB(0,0,255), 1, 8 );
	  point1.x -=2;
	  point1.y -=2;
	  point2.x -=2;
	  point2.y -=2;
	  cvLine(imCProcess, point1, point2, CV_RGB(0,0,255), 1, 8 );

	}
      }
    }

    /*--------------------------------------------------------------------------------------------------*/
    /* ONLY SEND THESE LINES IF NO KALMAN FILTER APPLIED */

    if (!this->kalman){

    //! lanes that are not supposed to be output must be cleaned
    if (lanes[i].color == GREEN || lanes[i].color == OTHER || lanes[i].score < 3) {
      cleanMapElement(mE, i);
      continue;
    }
    
    //! send lines as map elements
    //! if for some reason the line was not accepted in localFrame, clean i
    if (imageLineToMapElement(lanes[i], mE, i)==-1) {
      cleanMapElement(mE, i);
      continue;
    }
    }
           
  }

  if (!this->kalman){
  // Send lines as sensnet blob
  this->writeSensnet(&lanes);

  if (this->show || this->save_debug) {
    paintROI();
    if (this->show) SHOW_IMAGE(imCProcess, "7.Original Image with superimposed lines", 10);
    if (this->save_debug) this->exportImage("7.Lines", imCProcess);
  }
  
  if (int(lanes.size()) < lanesPreviousSize) {
    for (i=lanes.size(); i < lanesPreviousSize; i++) {
      cleanMapElement(mE, i);
    }
  }

  lanesOutput = 0;
  
  lanesPreviousSize = lanes.size();
  lanes.clear();
  }

  /*---------------------------------------------------------------------------------------------------*/
  /* THIS IS DONE IF KALMAN FILTER IS APPLIED*/

 if (this->kalman){

   //! MERGING: All the new lines detected get sent into mergeLines
   //! where lines that are sufficiently close together get merged into one line.
   lanes=mergeLines(lanes);

   //MSG("1: Xs: %.2f, Ys: %.2f, Xe: %.2f, Ye: %.2f", lanes[0].startPoint.x, lanes[0].startPoint.x, lanes[0].startPoint.x,

   lanes=transformationImageToLocal(lanes);

   //! TRACKING
   lanesCorrected = dataAssociation(&lanes);//(&lanesIn); 

   //lanesCorrected=lanes; //DEBUGGING PURPOSE 

   lanesCorrected=transformationLocalToImage(lanesCorrected);
 
   //MSG("Last trans.");

   //! MERGING: All the new lines detected get sent into mergeLines
   //! where lines that are sufficiently close together get merged into one line.
   lanesCorrected=mergeLines(lanesCorrected);

  /*-------------------------------------------------------------------------------------------------------*/
  
  for (i = 0; i<(int)lanesCorrected.size(); i++) {

    if (this->show || this->save_debug || this->lines) {
      CvPoint point1,point2;
      point1.x = int(lanesCorrected[i].startPoint.x);
      point1.y = int(lanesCorrected[i].startPoint.y);
      point2.x = int(lanesCorrected[i].endPoint.x);
      point2.y = int(lanesCorrected[i].endPoint.y);

      if (this->show || this->save_debug) {
	//	if (lanesCorrected[i].score <2 ) cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8 );
	//	if (lanesCorrected[i].color == GREEN) cvLine(imCProcessTrack, point1, point2, CV_RGB(0,255,0), 1, 8 );
	if (lanesCorrected[i].color == WHITE) cvLine(imCProcessTrack, point1, point2, CV_RGB(255,255,255), 1, 8 );
	if (lanesCorrected[i].color == YELLOW) cvLine(imCProcessTrack, point1, point2, CV_RGB(255,255,0), 1, 8 );
	//	if (lanesCorrected[i].color == OTHER) cvLine(imCProcessTrack, point1, point2, CV_RGB(255,0,255), 1, 8 );
      }

      char *color;
      //! print line stats
      if (this->lines) {
      
	switch (lanesCorrected[i].color) {
	case GREEN:
	  color = "green";
	  break;	
	case WHITE:
	  color = "white";
	  break;
	case YELLOW:
	  color = "yellow";
	  break;
	case OTHER:
	  color = "other";
	  break;
	default: 
	  color = "unkown";
	}
	MSG("line %d: (%d,%d)->(%d,%d) %s score:%d", i, point1.x, point1.y, point2.x, point2.y, color, lanesCorrected[i].score);
      }
      //! add blue line markers around white and yellow lines
      if (this->show || this->save_debug) {
	if (lanesCorrected[i].color == WHITE || lanesCorrected[i].color == YELLOW && lanesCorrected[i].score>1) {//added score
	  point1.x ++;
	  point1.y ++;
	  point2.x ++;
	  point2.y ++;
	  cvLine(imCProcessTrack, point1, point2, CV_RGB(0,0,255), 1, 8 );
	  point1.x -=2;
	  point1.y -=2;
	  point2.x -=2;
	  point2.y -=2;
	  cvLine(imCProcessTrack, point1, point2, CV_RGB(0,0,255), 1, 8 );
	}
      }
    }

    //! lanes that are not supposed to be output must be cleaned
    if (lanesCorrected[i].color == GREEN || lanesCorrected[i].color == OTHER || lanesCorrected[i].score < 2) {
      cleanMapElement(mE, i);
      continue;
    }
    
    //! send lines as map elements
    //! if for some reason the line was not accepted in localFrame, clean i
    if (imageLineToMapElement(lanesCorrected[i], mE, i)==-1) {
      cleanMapElement(mE, i);
      continue;
    }         
  }

  // Send lines as sensnet blob
  this->writeSensnet(&lanesCorrected);

  if (this->show || this->save_debug) {
    paintROI();
    if (this->show) SHOW_IMAGE(imCProcess, "7.Original Image with superimposed lines", 10);
    if (this->save_debug) this->exportImage("7.Lines", imCProcess);
    if (this->show) SHOW_IMAGE(imCProcessTrack, "7.Original Image with tracked lines", 10);
    if (this->save_debug) this->exportImage("7. Tracked Lines", imCProcessTrack);
  }
  
  
  if (int(lanesCorrected.size()) < lanesPreviousSize) {
    for (i=lanesCorrected.size(); i < lanesPreviousSize; i++) {
      cleanMapElement(mE, i);
    }
  }

  // MSG("%d lines output out of %d lines", lanesOutput, int(lanesCorrected.size()));
  lanesOutput = 0;
  
  lanesPreviousSize = lanesCorrected.size();
  lanesCorrected.clear();
  lanes.clear();
 }
  return 0;
}

/*--------------------------------------------------------------------------------------------------------*/
/* ALL THE FUNCTIONS NECESSARY FOR TRACKING */

//! data association (oldLines is a class)
//! input: new detected lines (newLines), output: corrected lines (oldLines which is a class)
vector<Line> RoadPerceptor::dataAssociation(vector<Line> *newLines_pointer)
{


  //! Enable alterning covariances during run and display of the values each iteration even though no update	  
  if(this->covmup==true){this->R=this->R*10;this->covmup=false;}
  if(this->covmdown==true){this->R=this->R/10;this->covmdown=false;}
  if(this->covpup==true){this->Q=this->Q*10;this->covpup=false;}
  if(this->covpdown==true){this->Q=this->Q/10;this->covpdown=false;}
  MSG("COVM: %e, COVP: %e",this->R, this->Q);

   //! initializes necessary parameters 
  vector<Line> oldLinesTemp, oldLinesTemp1, newLines; 
  int  numberNewLines, numberOldLines,j,i;
  float distBetweenStart,distBetweenEnd, minDistanceBetween, minDistance;
  Line newLine, newOldLine,oldLine, oldLineTemp, thisOldLine;
  double MAX_ALLOWED_DISTANCE=0.1;
  int POST_FRAMES=3;

  //! set numberNewLines to the number of new detected lines (size of vector) 
  numberNewLines = (int) (*newLines_pointer).size();

  //! set numberOldLines to the number of old lines from the previous iteration (size of vector) 
  numberOldLines = (int) oldLines.size();  

  /*----------------------------------------------------------------------------------------------------*/
  //! if no old lines exists, like in the very first iteration
  if (numberOldLines == 0){
  
   for (j = 0; j < numberNewLines; j++) {

     //! unvectorize it, and set it to a Line
     newLine = (*newLines_pointer)[j];
     //! init track
     //! input:new measurement, output:oldLine
     newOldLine = initLineTrack(newLine);
     newOldLine.track = 1;
     newOldLine.postFrames=0;
     oldLines.push_back(newOldLine);

   }
  
  }
  /*-----------------------------------------------------------------------------------------------------*/

  else{

//     MSG("1:XS:%.2f,YS:%.2f,ZS:%.2f,XE:%.2f,YE:%.2f,ZE:%.2f, size:%d",oldLines[0].startPoint.x,oldLines[0].startPoint.y,oldLines[0].startPointz,oldLines[0].endPoint.x,oldLines[0].endPoint.y,oldLines[0].endPointz, (int) oldLines.size());

//     MSG("2:XS:%.2f,YS:%.2f,ZS:%.2f,XE:%.2f,YE:%.2f,ZE:%.2f, size:%d",oldLines[0].startPoint.x,oldLines[0].startPoint.y,oldLines[0].startPointz,oldLines[0].endPoint.x,oldLines[0].endPoint.y,oldLines[0].endPointz,(int) oldLines.size());

    //! these two for-loops are to check if one old line is missing a new line,
    //! meaning tracking should be stopped and this oldLine shall no longer be stored as an oldLine
     for (i = 0; i < numberOldLines; i++) {

       CvPoint pointstart, pointend, start, end;

       //! needs to be initialized/reset before the next for-loop
       minDistance = 20000;

       //! unvectorize it, and set it to a Line
       oldLine = oldLines[i];

       //! calculate the change of old line since last frame
       oldLine = simpleLineModel(oldLine);

       //! predict old line's current location
       oldLine = predictLineTrack(oldLine);

       start.x = (int) oldLine.startPoint.x;
       start.y = (int) oldLine.startPoint.y;
       //! start z is stored in [2]
       end.x = (int) oldLine.endPoint.x;
       end.y = (int) oldLine.endPoint.y;
       //! end z is stored in [5]

       for (j = 0; j < numberNewLines; j++) {
       

         //! unvectorize it, and set it to a Line
         newLine = (*newLines_pointer)[j];
	    
         pointstart.x = (int) newLine.startPoint.x;
	 pointstart.y = (int) newLine.startPoint.y;
         pointend.x = (int) newLine.endPoint.x;
	 pointend.y = (int) newLine.endPoint.y;

	 //! a distance check is made only for lines which have approx. the same angle (getLineAngle in local frame)	  
	 float angle1 = getLineAngle(newLine.startPoint.x, newLine.startPoint.y, newLine.endPoint.x, newLine.endPoint.y);
	 float angle2 = getLineAngle(oldLine.startPoint.x, oldLine.startPoint.y, oldLine.endPoint.x ,oldLine.endPoint.y );
	 
	 if (angle1 > angle2-15 && angle1 < angle2+15){ 

	   distBetweenStart = minDistBetweenPointAndLine(pointstart, start, end);

	   distBetweenEnd = minDistBetweenPointAndLine(pointend, start, end);

	   
	   //! the minimum found distance is stored as minDistanceBetween
	   if (distBetweenStart < distBetweenEnd){

	     minDistanceBetween = distBetweenStart;
	   }
	   else {
	     minDistanceBetween = distBetweenEnd;
	   }
      
	   //! only the minimum of all the distances gets saved
	   if (minDistanceBetween < minDistance) {

	     minDistance = minDistanceBetween;
	   }
	 }
       }

       //noupdate = true; //! debugging purpose
       if (noupdate == true){minDistance = 2000000;}

       //! if minDistance is OK, the line get sent further through dataAssociation
       if(minDistance < MAX_ALLOWED_DISTANCE){

	oldLine.postFrames = 0;
	oldLinesTemp.push_back(oldLine);}

       else{
	 //! if minDistance is not OK but the number of post frames (the number of frames from latest detection) are
	 //! the line still gets displayed, but not sent further in dataAssociation
	  if(oldLine.postFrames < POST_FRAMES){
 
	   //! predict old line's current location
	   oldLine.postFrames++;
	   oldLinesTemp1.push_back(oldLine);}
       }
	 
     }

     /*-------------------------------------------------------------------------------------------------------*/

     //! set numberOldLines to the new number of the old lines from the previous iteration 
     numberOldLines = (int) oldLinesTemp.size();

     //! clear the vector oldLines so that it does not keep growing for every iteration
     oldLines.clear();

     //! store the old lines that was not detected in this frame but which had OK post frames in oldLines
     for(i = 0; i < (int) oldLinesTemp1.size(); i++){oldLines.push_back(oldLinesTemp1[i]);}

     //! check which new line belongs to which old line, an old line can only be updated with respect to one new line
     vector<int> index;
     bool skip = false;
     int k;
     int jMin;
 
     CvPoint pointstart, pointend, start, end;

     for(i = 0; i < numberNewLines; i++){

       minDistance = 2000;

       newLine = (*newLines_pointer)[i];

       pointstart.x = (int) newLine.startPoint.x;
       pointstart.y = (int) newLine.startPoint.y;
       pointend.x = (int) newLine.endPoint.x;
       pointend.y = (int) newLine.endPoint.y;

       for(j = 0; j < numberOldLines; j++){

	 for(k = 0; k < (int) index.size(); k++){

	   if(i == index[k]){skip = true; break;}}
	 

	 if(skip == true){skip = false; continue;} 
	 
	 oldLineTemp = oldLinesTemp[j];

	 start.x = (int) oldLineTemp.startPoint.x;
	 start.y = (int) oldLineTemp.startPoint.y;
	 end.x = (int) oldLineTemp.endPoint.x;
	 end.y = (int) oldLineTemp.endPoint.y;
	 
	 //! distance check is only made for those lines which have approx. the same angle (getLineAngle in local frame)
	 float angle1 = getLineAngle(newLine.startPoint.x, newLine.startPoint.y, newLine.endPoint.x, newLine.endPoint.y);
	 float angle2 = getLineAngle(oldLineTemp.startPoint.x, oldLineTemp.startPoint.y, oldLineTemp.endPoint.x, oldLineTemp.endPoint.y);
	 
	 jMin = 10000;

	 if (angle1 > angle2-15 && angle1 < angle2+15){ 

	  distBetweenStart = minDistBetweenPointAndLine(pointstart, start, end);

	  distBetweenEnd = minDistBetweenPointAndLine(pointend, start, end);

	  if (distBetweenStart > distBetweenEnd){minDistanceBetween = distBetweenEnd;}
	  else{minDistanceBetween = distBetweenStart;}
	 
	  if (minDistanceBetween < minDistance){ minDistance = minDistanceBetween; jMin = j; thisOldLine = oldLineTemp;}
	}	 
       }

       index.push_back(jMin);
   
       //! if this condition is fulfilled the old line should get updated with respect to the new one
       //! the updated line should both get saved to the next iteration and
       //! displayed on image, sent as mapelement and sent over sensnet 
       if (minDistance < MAX_ALLOWED_DISTANCE){
	 //! update track
	 //! 1 arg: new measurement, 2 arg: old measurement
	 //! it returns a new old line with the Kalman structure stored on it
	 newOldLine = updateLineTrack(newLine, thisOldLine);

	 if (thisOldLine.color == OTHER){newOldLine.color = newLine.color;}
	 else{newOldLine.color = thisOldLine.color;}
	 if (newLine.score > thisOldLine.score){newOldLine.score = newLine.score;}
	 else{newOldLine.score = thisOldLine.score;} 
	 newOldLine.ID = thisOldLine.ID;
	 newOldLine.track = 1;
	 newOldLine.postFrames=0;             
	 oldLines.push_back(newOldLine);

       }

       //! if not it should get tracking initialised 
       else {
	 //! init track
	 //! input:new measurement, output:oldLine
	 newOldLine = initLineTrack(newLine);
	 newOldLine.track = 1;
	 newOldLine.postFrames=0; 
	 oldLines.push_back(newOldLine);

        }
         
     }
 
  }
  return oldLines;

}

//! init Kalman to start tracking line
Line RoadPerceptor::initLineTrack(Line newLine)
  //! model used to track lines in local frames
  //! states: start and end points (x,y)
  //! x[k+1]= A*x[k] + B*u[k] + w[k] i.e. A=I, B=I
  //! z[k] = H*x[k] + v[k] i.e. H=I
{
  //! allocate memory for Kalman filter structure (cvCreateKalman returns a pointer)
  //! 1 arg:dim of state vector, 2 arg:dim of measurement vector, 3 arg: dim of the control vector  (=0, do not control lines)
  this->number++;
  newLine.ID = this->number;

  newLine.kalman_pointer = cvCreateKalman(6, 6, 6);

  //! set the trans. matrix (denoted A above) to the identity matrix
  cvSetIdentity(newLine.kalman_pointer->transition_matrix, cvRealScalar(1));

  //! set the meas. matrix (denoted H above) to the identity matrix
  cvSetIdentity(newLine.kalman_pointer->measurement_matrix, cvRealScalar(1));

  //! set the control matrix (denoted B above) to the identity matrix
  cvSetIdentity(newLine.kalman_pointer->control_matrix, cvRealScalar(1));

  //! set process noise: p(w)~N(0,Q)
  //! dia. values are now set to 1e-1, must be imperically evaluated 
  cvSetIdentity(newLine.kalman_pointer->process_noise_cov, cvRealScalar(this->Q));

  //! set measurement noise: p(v)~N(0,R)
  //! dia. values are now set to 1e-5, must be imperically evaluated
  cvSetIdentity(newLine.kalman_pointer->measurement_noise_cov, cvRealScalar(this->R));

  //! set initial error estimate covariance matrix P'(k):
  //! P'(k)=A*P(k-1)*At+Q
  //! dia. values are now set to 1, must be imperically evaluated 
  cvSetIdentity(newLine.kalman_pointer->error_cov_pre, cvRealScalar(1));

  //! the following will be used in predictLineTrack, when predicting new location of line
  uint64_t newTime;
  newTime = this->stereoBlob.timestamp;
  newLine.time = newTime;

  newLine.localXVel = (float) this->stereoBlob.state.localXVel;
  newLine.localYVel = (float) this->stereoBlob.state.localYVel;
  newLine.localZVel = (float) this->stereoBlob.state.localZVel;

  newLine.startMaxVar = 0;// set value, if it will not get reset in update
  newLine.startMinVar = 0;
  newLine.startAxis = 0;
  newLine.endMaxVar = 0;
  newLine.endMinVar = 0;
  newLine.endAxis = 0;

  return newLine;
}

//! stop tracking and deallocate Kalman filter structure
//! cvReleaseKalman releases the structure cvKalman and all underlying matrices
void RoadPerceptor::destroyLineTrack(Line oldLine){
 
  cvReleaseKalman(&(oldLine.kalman_pointer));
  oldLine.track = 0;
 return;
}

Line RoadPerceptor::predictLineTrack(Line oldLine){

       //! necessary parameters get defined 
       CvMat control;
       float deltaX; float deltaY; float deltaZ;

       deltaX=oldLine.deltaX; deltaY=oldLine.deltaY; deltaZ=oldLine.deltaZ;

       //! can choose between two control vectors
       float control0[] = {deltaX,deltaY,deltaZ,deltaX,deltaY,deltaZ};
       float control1[] = {0.00,0.00,0.00,deltaX,deltaY,deltaZ};

       if (updates == true){control = cvMat(6, 1, CV_32FC1, control1);}
       else{control = cvMat(6, 1, CV_32FC1, control0);}

       //! estimate subsequent model state
       //! cvKalmanPredict estimates the subsequent stochatic model state
       //! by its current state and stores it in kalman->state_pre
       cvKalmanPredict(oldLine.kalman_pointer, &control);

       if (updates == true){

	 if (distance(oldLine.startPoint.x,oldLine.startPoint.y,this->stereoBlob.state.localX,this->stereoBlob.state.localY) < distance(oldLine.endPoint.x,oldLine.endPoint.y,this->stereoBlob.state.localX,this->stereoBlob.state.localY)){

	   oldLine.kalman_pointer->state_pre->data.fl[0] = oldLine.startPoint.x;
	   oldLine.kalman_pointer->state_pre->data.fl[1] = oldLine.startPoint.y;
	   oldLine.kalman_pointer->state_pre->data.fl[2] = oldLine.startPointz;
	   oldLine.kalman_pointer->state_pre->data.fl[3] = deltaX + oldLine.endPoint.x;
	   oldLine.kalman_pointer->state_pre->data.fl[4] = deltaY + oldLine.endPoint.y;
	   oldLine.kalman_pointer->state_pre->data.fl[5] = deltaZ + oldLine.endPointz;

	 }else{

	   oldLine.kalman_pointer->state_pre->data.fl[0] = deltaX + oldLine.startPoint.x;
	   oldLine.kalman_pointer->state_pre->data.fl[1] = deltaY + oldLine.startPoint.y;
	   oldLine.kalman_pointer->state_pre->data.fl[2] = deltaZ + oldLine.startPointz;
	   oldLine.kalman_pointer->state_pre->data.fl[3] = oldLine.endPoint.x;
	   oldLine.kalman_pointer->state_pre->data.fl[4] = oldLine.endPoint.y;
	   oldLine.kalman_pointer->state_pre->data.fl[5] = oldLine.endPointz;
	 }

       }else{

	 oldLine.kalman_pointer->state_pre->data.fl[0] = deltaX + oldLine.startPoint.x;
	 oldLine.kalman_pointer->state_pre->data.fl[1] = deltaY + oldLine.startPoint.y;
	 oldLine.kalman_pointer->state_pre->data.fl[2] = deltaZ + oldLine.startPointz;
	 oldLine.kalman_pointer->state_pre->data.fl[3] = deltaX + oldLine.endPoint.x;
	 oldLine.kalman_pointer->state_pre->data.fl[4] = deltaY + oldLine.endPoint.y;
	 oldLine.kalman_pointer->state_pre->data.fl[5] = deltaZ + oldLine.endPointz;
       }

//        float anglePre = getLineAngle(oldLine.startPoint.x,oldLine.startPoint.y, oldLine.endPoint.x, oldLine.endPoint.y);
//        float anglePost = getLineAngle(oldLine.kalman_pointer->state_pre->data.fl[0],oldLine.kalman_pointer->state_pre->data.fl[1],oldLine.kalman_pointer->state_pre->data.fl[3],oldLine.kalman_pointer->state_pre->data.fl[4]);
//        MSG("anglePre: %.2f, anglePost: %.2f", anglePre, anglePost);

//        MSG("PRE:Xs: %.2f,Ys: %.2f,Zs: %.2f",oldLine.startPoint.x,oldLine.startPoint.y,oldLine.startPointz);
//        MSG("PRE:Xe: %.2f,Ye: %.2f,Ze: %.2f",oldLine.endPoint.x,oldLine.endPoint.y,oldLine.endPointz);

       oldLine.startPoint.x = oldLine.kalman_pointer->state_pre->data.fl[0];
       oldLine.startPoint.y = oldLine.kalman_pointer->state_pre->data.fl[1];
       oldLine.startPointz = oldLine.kalman_pointer->state_pre->data.fl[2];
       oldLine.endPoint.x = oldLine.kalman_pointer->state_pre->data.fl[3];
       oldLine.endPoint.y = oldLine.kalman_pointer->state_pre->data.fl[4];
       oldLine.endPointz = oldLine.kalman_pointer->state_pre->data.fl[5];

//        MSG("POST:Xs: %.2f,Ys: %.2f,Zs: %.2f",oldLine.startPoint.x,oldLine.startPoint.y,oldLine.startPointz);
//        MSG("POST:Xe: %.2f,Ye: %.2f,Ze: %.2f",oldLine.endPoint.x,oldLine.endPoint.y,oldLine.endPointz);
       return oldLine;

}

//! update tracking of the detected line
Line RoadPerceptor::updateLineTrack(Line newLine, Line thisOldLine)

{
  if (linemodel == true) {MSG(" OLD: start x: %.4f, start y: %.4f, start z: %.4f, end x: %.4f, end y: %.4f, end z: %.4f",thisOldLine.startPoint.x,thisOldLine.startPoint.y,thisOldLine.startPointz,thisOldLine.endPoint.x,thisOldLine.endPoint.y,thisOldLine.endPointz );MSG(" NEW: start x: %.4f, start y: %.4f, start z: %.4f, end x: %.4f, end y: %.4f, end z: %.4f",newLine.startPoint.x,newLine.startPoint.y,newLine.startPointz,newLine.endPoint.x,newLine.endPoint.y,newLine.endPointz ); MSG(" PRE: start x: %.4f, start y: %.4f, start z: %.4f, end x: %.4f, end y: %.4f, end z: %.4f",thisOldLine.kalman_pointer->state_pre->data.fl[0],thisOldLine.kalman_pointer->state_pre->data.fl[1],thisOldLine.kalman_pointer->state_pre->data.fl[2],thisOldLine.kalman_pointer->state_pre->data.fl[3],thisOldLine.kalman_pointer->state_pre->data.fl[4],thisOldLine.kalman_pointer->state_pre->data.fl[5]);}

  CvMat z;

  //! get the measurement values y(k)
  //! meaning get the start and end points of the new line
  float z0[] = {newLine.startPoint.x, newLine.startPoint.y, newLine.startPointz,newLine.endPoint.x, newLine.endPoint.y, newLine.endPointz};

  //! save the measurements as a cvMat
  z = cvMat(6, 1, CV_32FC1, z0);

  //! the cvKalmanCorrect adjusts stochastic model state
  //! on the basis of the given measurement of the model state
  //! it stores the adjusted states at kalman->state_post
  //! meaning it merge measurement and prediction in a suitable way
  cvKalmanCorrect(thisOldLine.kalman_pointer, &z);

  //! set corrected line to contain the corrected data,
  //! this will be used as oldLines in the next iteration
  thisOldLine.startPoint.x = thisOldLine.kalman_pointer->state_post->data.fl[0];//this should all be things
  thisOldLine.startPoint.y = thisOldLine.kalman_pointer->state_post->data.fl[1];
  thisOldLine.startPointz = thisOldLine.kalman_pointer->state_post->data.fl[2];
  thisOldLine.endPoint.x = thisOldLine.kalman_pointer->state_post->data.fl[3];
  thisOldLine.endPoint.y = thisOldLine.kalman_pointer->state_post->data.fl[4];
  thisOldLine.endPointz = thisOldLine.kalman_pointer->state_post->data.fl[5];

  //! set the max cov, min cov and axis for the start point.
  if ( thisOldLine.kalman_pointer->error_cov_post->data.fl[0] > thisOldLine.kalman_pointer->error_cov_post->data.fl[1]){
    thisOldLine.startMaxVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[0];
    thisOldLine.startMinVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[1];
    thisOldLine.startAxis = 0;}
  else {
    thisOldLine.startMaxVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[1];
    thisOldLine.startMinVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[0];
    thisOldLine.startAxis = CV_PI/2;}

  //! set the max cov, min cov and axis for the end point.
  if ( thisOldLine.kalman_pointer->error_cov_post->data.fl[3] > thisOldLine.kalman_pointer->error_cov_post->data.fl[4]){
    thisOldLine.startMaxVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[3];
    thisOldLine.startMinVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[4];
    thisOldLine.startAxis = 0;}
  else {
    thisOldLine.startMaxVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[4];
    thisOldLine.startMinVar = thisOldLine.kalman_pointer->error_cov_post->data.fl[3];
    thisOldLine.startAxis = CV_PI/2;}     

  if (linemodel == true) {MSG(" UP: start x: %.4f, start y: %.4f,start z: %.4f, end x: %.4f, end y: %.4f, end z: %.4f",thisOldLine.startPoint.x,thisOldLine.startPoint.y,thisOldLine.startPointz,thisOldLine.endPoint.x,thisOldLine.endPoint.y,thisOldLine.endPointz );}
    
  return thisOldLine;
}
  
/* ALL THE FUNCTIONS NECESSARY FOR TRACKING */
/*------------------------------------------------------------------------------------------------------------------*/


//! return the accumulator of a line on an image
float RoadPerceptor::lineAccumulator(const Line &imageLine, const CvMat *imBin) {
  int x, y, i;
  int size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));
  CvScalar p=cvScalar(0.0), a=cvScalar(0.0);
    
  for (i=0; i<size; i++) {
    x = int(imageLine.startPoint.x + i*(imageLine.endPoint.x-imageLine.startPoint.x)/size);
    y = int(imageLine.startPoint.y + i*(imageLine.endPoint.y-imageLine.startPoint.y)/size);
    
    // in the middle
    x=x-ROIstartX;
    y=y-ROIhorizonY;

    if (x<0) x=0;
    if (y<0) y=0;
    if (x >= ROIwidth) x = ROIwidth -1;
    if (y >= ROIheight-(ROIhorizonY-ROIstartY)) y = ROIheight-(ROIhorizonY-ROIstartY) -1;   

    p = cvGet2D(imBin, y, x);
    a.val[0] += p.val[0];
  }

  a.val[0] = round(a.val[0]/size);   // average
  return a.val[0];
}

float RoadPerceptor::lineAccumulatorVariance(const Line &imageLine, const CvMat *imBin) {
  int x, y, i;
  int size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));
  CvScalar p=cvScalar(0.0), a=cvScalar(0.0), v=cvScalar(0.0);
    
  a.val[0] = lineAccumulator(imageLine, imBin);

  for (i=0; i<size; i++) {
    x = int(imageLine.startPoint.x + i*(imageLine.endPoint.x-imageLine.startPoint.x)/size);
    y = int(imageLine.startPoint.y + i*(imageLine.endPoint.y-imageLine.startPoint.y)/size);
    
    // in the middle
    x=x-ROIstartX;
    y=y-ROIhorizonY;

    if (x<0) x=0;
    if (y<0) y=0;
    if (x >= ROIwidth) x = ROIwidth -1;
    if (y >= ROIheight-(ROIhorizonY-ROIstartY)) y = ROIheight-(ROIhorizonY-ROIstartY) -1;   

    p = cvGet2D(imBin, y, x);
    v.val[0] += pow(a.val[0] - p.val[0],2);
  }

  v.val[0] = round(v.val[0]/size);   // variance
  return v.val[0];
}

//! Returns the line color in between two lines
int RoadPerceptor::getLineColorBetween(const Line &line1, const Line &line2) {

  Line lineBetween;
  Line side1;
  Line side2;

  //! center of line1, 2 and lineBetween
  float m1x, m1y, m2x, m2y, mBx, mBy;

  //! point in yellow channel: average for self, left and right
  float a_y, a_y_l, a_y_r;
  //! point in white channel: average for self, left and right
  float a_w, a_w_l, a_w_r; 

  if (distance(line1.startPoint.x, line1.startPoint.y, line2.startPoint.x, line2.startPoint.y) < distance(line1.startPoint.x, line1.startPoint.y, line2.endPoint.x, line2.endPoint.y)) {
    
    lineBetween.startPoint.x = (line1.startPoint.x + line2.startPoint.x)/2;
    lineBetween.startPoint.y = (line1.startPoint.y + line2.startPoint.y)/2;
    lineBetween.endPoint.x = (line1.endPoint.x + line2.endPoint.x)/2;
    lineBetween.endPoint.y = (line1.endPoint.y + line2.endPoint.y)/2 ;

  } else {
    lineBetween.startPoint.x = (line1.startPoint.x + line2.endPoint.x)/2;
    lineBetween.startPoint.y = (line1.startPoint.y + line2.endPoint.y)/2;
    lineBetween.endPoint.x = (line1.endPoint.x + line2.startPoint.x)/2;
    lineBetween.endPoint.y = (line1.endPoint.y + line2.startPoint.y)/2 ;
  }

  m1x = 0.5*(line1.startPoint.x + line1.endPoint.x);
  m1y = 0.5*(line1.startPoint.y + line1.endPoint.y);
  m2x = 0.5*(line2.startPoint.x + line2.endPoint.x);
  m2y = 0.5*(line2.startPoint.y + line2.endPoint.y);
  mBx = 0.5*(lineBetween.startPoint.x + lineBetween.endPoint.x);
  mBy = 0.5*(lineBetween.startPoint.y + lineBetween.endPoint.y);


  side1.startPoint.x = line1.startPoint.x + (m1x-mBx);
  side1.startPoint.y = line1.startPoint.y + (m1y-mBy);
  side1.endPoint.x = line1.endPoint.x + (m1x-mBx);
  side1.endPoint.y = line1.endPoint.y + (m1y-mBy);

  side2.startPoint.x = line2.startPoint.x + (m2x-mBx);
  side2.startPoint.y = line2.startPoint.y + (m2y-mBy);
  side2.endPoint.x = line2.endPoint.x + (m2x-mBx);
  side2.endPoint.y = line2.endPoint.y + (m2y-mBy);

  a_y = lineAccumulator(lineBetween, imYellow); // Y middle
  a_w = lineAccumulator(lineBetween, imWhite);  // W middle

  a_y_l = lineAccumulator(side1, imYellow); // Y left
  a_w_l = lineAccumulator(side1, imWhite);  // W left

  a_y_r = lineAccumulator(side2, imYellow); // Y right
  a_w_r = lineAccumulator(side2, imWhite);  // W right
//   MSG("a_w %.2f,a_w_l %.2f,a_w_r %.2f", a_w, a_w_l, a_w_r);
//   MSG("a_y %.2f,a_y_l %.2f,a_y_r %.2f", a_y, a_y_l, a_y_r);
  
  if ((a_w > a_w_l +20) && (a_w > a_w_r + 20)) {
    // float var= lineAccumulatorVariance(lineBetween, imWhite);
    // MSG("white var:%f",var);
    // if (var > 1000) return GREEN; 
    return WHITE;
  } 
  if ((a_y > a_y_l + 15) && (a_y > a_y_r + 15)){
    // float var= lineAccumulatorVariance(lineBetween, imYellow);
    // MSG("yellow var:%f",var);
    // if (var > 1000) return GREEN;
    return YELLOW;
  }
  
  return OTHER;

}

//! Returns the line color 
int RoadPerceptor::getLineColor(const Line &imageLine, int sideColorCheckDistance) {

  Line leftLine, rightLine;

  //! point in yellow channel: average for self, left and right
  float a_y, a_y_l, a_y_r;
  //! point in white channel: average for self, left and right
  float a_w, a_w_l, a_w_r; 

  int xExtend=0, yExtend=0;

  float ang = getLineAngle(int(imageLine.startPoint.x), int(imageLine.startPoint.y), int(imageLine.endPoint.x), int(imageLine.endPoint.y));

  
  if (ang < -60) {
    xExtend = sideColorCheckDistance;
    yExtend = 0;
  } else if (ang < -30 ) {
    xExtend = sideColorCheckDistance;
    yExtend = -sideColorCheckDistance;
  } else if (ang < 0) {
    xExtend = 0;
    yExtend = - sideColorCheckDistance;
  } else if (ang < 30) {
    xExtend = 0;
    yExtend = sideColorCheckDistance;
  } else if (ang < 60) {
    xExtend = sideColorCheckDistance;
    yExtend = sideColorCheckDistance;
  } else {
    xExtend = sideColorCheckDistance;
    yExtend = 0;
  }

  a_y = lineAccumulator(imageLine, imYellow); // Y middle
  a_w = lineAccumulator(imageLine, imWhite);  // W middle

  leftLine.startPoint.x = imageLine.startPoint.x - xExtend;
  leftLine.endPoint.x = imageLine.endPoint.x - xExtend;
  leftLine.startPoint.y = imageLine.startPoint.y - yExtend;
  leftLine.endPoint.y = imageLine.endPoint.y - yExtend;

  a_y_l = lineAccumulator(leftLine, imYellow); // Y left
  a_w_l = lineAccumulator(leftLine, imWhite);  // W left

  rightLine.startPoint.x = imageLine.startPoint.x + xExtend;
  rightLine.endPoint.x = imageLine.endPoint.x + xExtend;
  rightLine.startPoint.y = imageLine.startPoint.y + yExtend;
  rightLine.endPoint.y = imageLine.endPoint.y + yExtend;
 
  a_y_r = lineAccumulator(rightLine, imYellow); // Y right
  a_w_r = lineAccumulator(rightLine, imWhite);  // W right

  if ((a_w > a_w_l + 30) && (a_w > a_w_r + 30)) {
    return WHITE;
  } 
  if ((a_y > a_y_l + 20) && (a_y > a_y_r + 20)){
    return YELLOW;
  }

  return OTHER;
}

float min3(float a, float b, float c) {
  if (min(a,b)==a && min(a,c)==a) return a; 
  if (min(a,b)==b && min(b,c)==b) return b; 
  return c;
}

//! extend this line
int RoadPerceptor::extendLocalLine(const Line &imageLine, float pi, float pj, float pi2, float pj2, float &px, float &py, float &px2, float &py2) {
  
  float sizeRatio;
  float wingSize;
  
  float lineSize;
  float sinTeta;
  float cosTeta;
  float direction;

  lineSize = sqrt(pow(px2-px,2)+pow(py2-py,2));
  sinTeta = fabs(py2-py)/lineSize;
  cosTeta = fabs(px2-px)/lineSize;

  //! Start wing
  if (imageLine.startPoint.x != pi || imageLine.startPoint.y != pj) {
    sizeRatio = sqrt(pow(pi-imageLine.startPoint.x,2)+pow(pj-imageLine.startPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;
    direction = (px-px2)/fabs(px-px2);
    px = px + direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py = py + direction*wingSize*sinTeta;
  }

  //! end wing
  if (imageLine.endPoint.x != pi2 || imageLine.endPoint.y != pj2) {
    sizeRatio = sqrt(pow(pi2-imageLine.endPoint.x,2)+pow(pj2-imageLine.endPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;

    direction = (px-px2)/fabs(px-px2);
    px2 = px2 - direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py2 = py2 - direction*wingSize*sinTeta;
    
  }

  return 1;
}

//! finds point near to provided, over line, that has good disparity
int RoadPerceptor::getBestNearestPoint(const Line &imageLine, float & pi, float & pj, float  & pd, int direction) {

  uint16_t testDisp;
  float px, py, pz;

  int size=0, i=0;

  float dist=0;
  float beginX=0, beginY=0;
  float endX=0, endY=0;
  
  size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));

    if (direction == 1) {
      beginX = imageLine.startPoint.x;
      beginY = imageLine.startPoint.y;
      endX = imageLine.endPoint.x;
      endY = imageLine.endPoint.y;
    } else {
      beginX = imageLine.endPoint.x;
      beginY = imageLine.endPoint.y;
      endX = imageLine.startPoint.x;
      endY = imageLine.startPoint.y;
    }
    
    //! try to get disparity from other points in line
    for (i=0; i<size; i++) {
      pi = beginX + i*direction*(imageLine.endPoint.x-imageLine.startPoint.x)/size;
      pj = beginY + i*direction*(imageLine.endPoint.y-imageLine.startPoint.y)/size;

     //! we just walked the whole line, don't check this point
      if (pi == endX && pj == endY) return 0;

      testDisp = (*StereoImageBlobGetDisp(&this->stereoBlob, (int) pi, (int) pj));
      
      //! good disparity, lets stick with this point
      if (testDisp>0 && testDisp != 65535) {
	pd = (float) testDisp;
	pd /= this->stereoBlob.dispScale;
	imageToLocalPoint(pi, pj, pd, px, py, pz);
	
	//! check if the point is on alice or too far away (lousy but valid disparity)
	dist = sqrt(pow(this->stereoBlob.state.localX-px,2)+pow(this->stereoBlob.state.localY-py,2));
	
	//! only consider valid disparity points
	if (dist > 1 && dist < 60) {
	  return 1;
	}
      }
    }

    return 0;
}

//! Convert image frame point to local point
void RoadPerceptor::imageToLocalPoint(float pi, float pj, float pd, float &px, float &py, float &pz) {

  float sx, sy, sz;
  float vx, vy, vz;
  
  //! Compute point in the sensor frame
  StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &sx, &sy, &sz);
  //! Convert to vehicle frame
  StereoImageBlobSensorToVehicle(&this->stereoBlob, sx, sy, sz, &vx, &vy, &vz);
  //! Convert to local frame
  StereoImageBlobVehicleToLocal(&this->stereoBlob, vx, vy, vz, &px, &py, &pz);

  return;
}


//! cleans exceding mapElements
int RoadPerceptor::cleanMapElement(MapElement &mE,int index) {
  mE.clear();
  mE.setId(this->moduleId, index);

  this->mapElementTalker.sendMapElement(&mE, 0);

  return 1;
}


//! converts from a line in image coordinates to a line in local coordinates
int RoadPerceptor::imageLineToMapElement(const Line &imageLine, MapElement &mE, int index) {
    float pi, pj, pi2, pj2, pd;
    float px, py, pz, px2, py2, pz2;
    double max_var, min_var, axis,max_var2, min_var2, axis2;

    
//     max_var=imageLine.startMaxVar; min_var=imageLine.startMinVar; axis=imageLine.startAxis; max_var2=imageLine.endMaxVar; min_var2=imageLine.endMinVar; axis2=imageLine.endAxis;
    max_var=0.2; min_var=0.1; axis=0.3; max_var2=0.2; min_var2=0.1; axis2=0.3;


    point2arr_uncertain points; 

    mE.clear();

    //! start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    
    //! check if it's a good point or there is a good point nearby
    if (!getBestNearestPoint(imageLine, pi, pj, pd, 1)) return -1;

    //! get the local frame coordinates for the best nearest point
    imageToLocalPoint(pi, pj, pd, px, py, pz);
    
    //end point
    pi2 = imageLine.endPoint.x;
    pj2 = imageLine.endPoint.y;

    if (!getBestNearestPoint(imageLine, pi2, pj2, pd, -1)) return -1;

    // only one point on the line, discard it.
    if (pi == pi2 && pj == pj2) return -1;

    imageToLocalPoint(pi2, pj2, pd, px2, py2, pz2);
    
    //! get real line-end points
    float dist1, dist2;
    dist1 = distance(px,py,px2,py2);
    extendLocalLine(imageLine, pi, pj, pi2, pj2, px, py, px2, py2);
    dist2 = distance(px,py,px2,py2);
    
    if (this->debug) MSG("line grew %d",int(round(100*(dist2-dist1)/dist1)));
    
    //! short lines don't get displayed
    if (distance(px,py,px2,py2)< MinLineMeters) return -1;

    points.push_back(point2_uncertain(px, py, max_var, min_var, axis));
    points.push_back(point2_uncertain(px2,py2, max_var2, min_var2, axis2));

    mE.setId(this->moduleId, index);  
    mE.setGeometry(points);

    mE.height=0;
    if (imageLine.color == GREEN) mE.plotColor = MAP_COLOR_GREEN;
    if (imageLine.color == WHITE) mE.plotColor = MAP_COLOR_GREY;
    if (imageLine.color == YELLOW) mE.plotColor = MAP_COLOR_YELLOW;
    if (imageLine.color == OTHER) mE.plotColor = MAP_COLOR_ORANGE;
 
    mE.type = ELEMENT_LANELINE;
    mE.geometryType = GEOMETRY_LINE;
    mE.state = this->stereoBlob.state;
    
    this->mapElementTalker.sendMapElement(&mE, 0); 
    
    lanesOutput ++;
    
    points.clear();
    mE.clear();
    return 1;
}


//! Write detected lines to sensnet
int RoadPerceptor::writeSensnet(vector<Line> *lines)
{
  int i;
  RoadLineBlob blob;
  Line imageLine;
  float pi, pj, pi2, pj2, pd;
  float px, py, pz, px2, py2, pz2;
  int blobSize;

  // Set up blob header
  blob.blobType = SENSNET_ROAD_LINE_BLOB;
  blob.version = ROAD_LINE_BLOB_VERSION;
  blob.sensnetId = SENSNET_STEREO_LINE_PERCEPTOR;
  blob.frameId = this->stereoBlob.frameId;
  blob.timestamp = this->stereoBlob.timestamp;
  blob.state = this->stereoBlob.state;
  blob.numLines = 0;
  
  for (i = 0; i < (int) lines->size(); i++)
  {
    imageLine = (*lines)[i];
    
    //! start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    
    //! check if it's a good point or there is a good point nearby
    if (!getBestNearestPoint(imageLine, pi, pj, pd, 1))
      continue;

    //! get the local frame coordinates for the best nearest point
    imageToLocalPoint(pi, pj, pd, px, py, pz);
    
    //end point
    pi2 = imageLine.endPoint.x;
    pj2 = imageLine.endPoint.y;

    if (!getBestNearestPoint(imageLine, pi2, pj2, pd, -1))
      continue;

    // only one point on the line, discard it.
    if (pi == pi2 && pj == pj2)
      continue;

    imageToLocalPoint(pi2, pj2, pd, px2, py2, pz2);
    
    // get real line-end points
    extendLocalLine(imageLine, pi, pj, pi2, pj2, px, py, px2, py2);
    
    // Copy line data
    if (blob.numLines >= ROAD_LINE_BLOB_MAX_LINES)
      break;
    blob.lines[blob.numLines].type = ROAD_LINE_BLOB_TYPE_UNKNOWN;
    blob.lines[blob.numLines].ax = px;
    blob.lines[blob.numLines].ay = py;
    blob.lines[blob.numLines].az = pz;
    blob.lines[blob.numLines].bx = px2;
    blob.lines[blob.numLines].by = py2;
    blob.lines[blob.numLines].bz = pz2;
    blob.numLines++;
  }

  // Compute the size of the blob that was actually used
  blobSize = sizeof(blob) - sizeof(blob.lines) + blob.numLines*sizeof(blob.lines[0]);

  // Publish the data
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK,
                    blob.sensnetId, blob.blobType, blob.frameId, blobSize, &blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}

float RoadPerceptor::getLineAngle(int startX, int startY, int endX, int endY) {
  CvPoint start;
  CvPoint end;
  start.x=startX;
  start.y=startY;
  end.x=endX;
  end.y=endY;
  return getLineAngle(start,end);

}

float RoadPerceptor::getLineAngle(CvPoint start, CvPoint end) {
  float angle=0.0;
	
  //! starting point always has lower or equal X coordinate
  if (start.x == end.x) {
    angle = 90.0;
  } else {
    angle = 180/CV_PI * atan((float) (end.y-start.y)/(end.x-start.x));
  }
  
  //! Y grows downwards
  return -angle;
}

float RoadPerceptor::getLineAngle(float startX, float startY, float endX, float endY) {
  float angle=0.0;
  float PI = 3.141592654;
	
  //! starting point always has lower or equal X coordinate
  if (startX == endX) {
    angle = 90.0;
  } else {
    angle = 180/PI * atan((float) (endY-startY)/(endX-startX));
  }

  return angle;
}

float RoadPerceptor::distance(CvPoint start, CvPoint end) {
	return sqrt(pow((double) (start.x-end.x),2)+pow((double) (start.y-end.y),2));
}

float RoadPerceptor::distance(float startx,float starty, float endx, float endy) {
	return sqrt(pow((double) (startx-endx),2)+pow((double) (starty-endy),2));
}

int RoadPerceptor::checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol) {
	//! size tolerance trial
	float size1 = distance(start1, end1);
	float size2 = distance(start2, end2);
	if (max(size1,size2)/min(size1,size2) > sizeTol || min(size1,size2) < 20) {
	  return -1;
	}

	//! angle tolerance trial
	if (!parallel(start1, end1, start2, end2, angleTol)) {
	  return -2;
	} 

	//! line convergence and alignment	
	if (!lineMatching(start1, end1, start2, end2, distanceTol)) {
	  return -3;
	}

	// All trials passed. brillo
		
	return 1;
}

int RoadPerceptor::parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol) {
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	float angleDiff = fabs(angle1-angle2);
	if (!(angleDiff<=angleTol || (180.0-angleDiff)<=angleTol)) return 0;

	return 1;
	
}

int RoadPerceptor::lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol) {
	/*
	What we know: 
	a. Points in a line are ordered by lower X.
	   a1. start1.x > end1.x
	   a2. start2.x > end2.x
	b. line1 and line2 are almost parallel (this is after the angle tolerance trial)
	*/

	//! extreme 1 and 2 distance
	//! closer points at extremes
	float dE1, dE2;
	//! the bigger and smaller of dE1 and dE2
	float dMax, dMin;

	if (distance(start1,start2) <  distance(start1,end2)) {
	  //! lines may be offset. we know start1 and start2 are closer. determine minimum distance between each of them and the respective other line.
	  dE1 = min( minDistBetweenPointAndLine(start2,start1,end1),minDistBetweenPointAndLine(start1,start2,end2));
 	  dE2 = min( minDistBetweenPointAndLine(end2,start1,end1),minDistBetweenPointAndLine(end1,start2,end2));
	  	  
	} else {
	  //! lines may be offset. we know start1 and end2 are closer. determine minimum distance between each of them and the respective other line.
	  dE1 = min( minDistBetweenPointAndLine(end2,start1,end1),minDistBetweenPointAndLine(start1,start2,end2));
 	  dE2 = min( minDistBetweenPointAndLine(end1,start2,end2),minDistBetweenPointAndLine(start2,start1,end1));
	}

	dMax = dE1 >= dE2 ? dE1 : dE2;
	dMin = dMax == dE1 ? dE2 : dE1;

	if (dMax > distanceTol || dMax < 0.5 ) return 0;
		
	return 1;
}

float RoadPerceptor::minDistBetweenPointAndLine(CvPoint point, CvPoint start, CvPoint end) {
  float d=0,d_n, size=0;
  int i;

  size = distance(start,end);
  d = distance(start,point);
  for (i=0; i<size; i++) {
    d_n = distance(start.x+i*(end.x-start.x)/size, start.y+i*(end.y-start.y)/size, point.x, point.y);
    if (d_n>d) {
      return d;
    }
    d=d_n;
  }
  return d;

}

vector<Line> RoadPerceptor::mergeLines(vector<Line> lanes){

   vector<Line> newLanes;
   vector<int> index;
   bool skip1=false;
   bool skip2=false;
   int k, i, j;
   Line newLine, newLine2;
   CvPoint pointstart, pointend, start, end;

   for(i=0; i<(int)lanes.size(); i++){

     for(k=0; k<(int)index.size(); k++){
       if(i==index[k]){skip1=true; break;}}

     if(skip1==true){skip1=false; continue;}

     newLine=lanes[i];

     for(j=i+1; j<(int)lanes.size(); j++){

       newLine2=lanes[j];

       for(k=0; k<(int)index.size(); k++){
	 if(i==index[k]){skip2=true; break;}}

       if(skip2==true){skip2=false; continue;}

       float angle1=getLineAngle((int) newLine.startPoint.x, (int) newLine.startPoint.y , (int)newLine.endPoint.x, (int) newLine.endPoint.y );

       float angle2=getLineAngle((int) newLine2.startPoint.x, (int) newLine2.startPoint.y , (int) newLine2.endPoint.x, (int) newLine2.endPoint.y );

       if (angle1 > -10 && angle1 <10 && angle2 > -10 && angle2 < 10){

	   pointstart.x=(int) newLine.startPoint.x;
	   pointstart.y=(int) newLine.startPoint.y;
	   pointend.x=(int) newLine.endPoint.x;
	   pointend.y=(int) newLine.endPoint.y;
	   start.x=(int) newLine2.startPoint.x ;
	   start.y=(int) newLine2.startPoint.y;
	   end.x=(int) newLine2.endPoint.x;
	   end.y=(int) newLine2.endPoint.y;

	   float distBetweenStart=minDistBetweenPointAndLine(pointstart, start, end);

	   float distBetweenEnd=minDistBetweenPointAndLine(pointend, start, end);

	   if ((distBetweenStart<15 || distBetweenEnd<15) && ((start.y-pointstart.y)*(start.y-pointstart.y)<30 && (end.y-pointend.y)*(end.y-pointend.y)<30 && (start.y-pointend.y)*(start.y-pointend.y)<30 && (end.y-pointstart.y)*(end.y-pointstart.y)<30 )){

	     if (newLine.startPoint.x>newLine2.startPoint.x){newLine.startPoint=newLine2.startPoint;}
	     if (newLine.endPoint.x<newLine2.endPoint.x){newLine.endPoint=newLine2.endPoint;}
	     if (newLine.score<newLine2.score){newLine.score=newLine2.score;}
	     if (newLine.color==OTHER){newLine.color=newLine2.color;}
	     index.push_back(j);
	   }
       }

       else{
	 if (angle1 > angle2-5 && angle1 < angle2+5 ){

	   pointstart.x=(int) newLine.startPoint.x;
	   pointstart.y=(int) newLine.startPoint.y;
	   pointend.x=(int) newLine.endPoint.x;
	   pointend.y=(int) newLine.endPoint.y;
	   start.x=(int) newLine2.startPoint.x ;
	   start.y=(int) newLine2.startPoint.y;
	   end.x=(int) newLine2.endPoint.x;
	   end.y=(int) newLine2.endPoint.y;

	  
	   float distBetweenStart=minDistBetweenPointAndLine(pointstart, start, end);

	   float distBetweenEnd=minDistBetweenPointAndLine(pointend, start, end);

	   if (distBetweenStart<35 || distBetweenEnd<35){

	     if (newLine.startPoint.x>newLine2.startPoint.x){newLine.startPoint=newLine2.startPoint;}
	     if (newLine.endPoint.x<newLine2.endPoint.x){newLine.endPoint=newLine2.endPoint;}
	     if (newLine.score<newLine2.score){newLine.score=newLine2.score;}
	     if (newLine.color==OTHER){newLine.color=newLine2.color;}
	     index.push_back(j);
	   }
	 }
       }
     }
     newLanes.push_back(newLine);
   }

   lanes.clear();
   lanes=newLanes;
   return lanes;
}

vector<Line> RoadPerceptor::transformationImageToLocal(vector<Line> lanesIn){
 

 int i;
  vector<Line> lanes;
  Line lane;

  //! the lines get transformed from image frame to local frame (origin = way point 1 1 1)
    for(i=0; i<(int)lanesIn.size(); i++){

      lane = lanesIn[i];

      float pi=0, pj=0, pd=0, pi2=0, pj2=0, pd2=0, px=0, py=0, pz=0, px2=0, py2=0, pz2=0;
	
      //! transforming start point (use functions defined in stereoImageBlob)
      pi = lane.startPoint.x;
      pj = lane.startPoint.y;

      //! check if it's a good point or there is a good point nearby
      if (getBestNearestPoint(lane, pi, pj, pd, 1)){

	//! get the local frame coordinates for the best nearest point
	imageToLocalPoint(pi, pj, pd, px, py, pz);}

      lane.startPoint.x = px;
      lane.startPoint.y = py;     
      lane.startPointz = pz;
     
      //! transforming start point (use functions defined in stereoImageBlob)
      pi2 = lane.endPoint.x;
      pj2 = lane.endPoint.y;

      //! check if it's a good point or there is a good point nearby
      if (getBestNearestPoint(lane, pi2, pj2, pd2, -1)){

	//! get the local frame coordinates for the best nearest point
	imageToLocalPoint(pi2, pj2, pd2, px2, py2, pz2);}
     
      lane.endPoint.x = px2;
      lane.endPoint.y = py2;
      lane.endPointz = pz2;

      //! if either the start or end point are zero then the transformation was bad and no true value was assigned
      //! meaning, these should not get sent further 
      if ((lane.startPoint.x!=0 && lane.startPoint.y!=0 && lane.startPointz!=0) && ( lane.endPoint.x!=0 && lane.endPoint.y!=0 && lane.endPointz!=0)){

	//! all the transformed lines get stored in lanesIn (lines are in local frame)
	lanes.push_back( lane );
      }
    } 
     lanesIn.clear();
     lanesIn=lanes;
  
     return lanesIn;
}
   
vector<Line> RoadPerceptor::transformationLocalToImage(vector<Line> lanesIn){

      int i;
      vector<Line> lanes;
      Line lane;

      //! the lines get transformed from local frame to image frame   
      for(i=0; i<(int) lanesIn.size(); i++){

	lane=lanesIn[i];

	float qx=0, qy=0, qz=0, vx=0, vy=0, vz=0, tx=0, ty=0, tz=0, px=0, py=0, pz=0,qx2=0, qy2=0, qz2=0, vx2=0, vy2=0, vz2=0, tx2=0, ty2=0, tz2=0, px2=0, py2=0, pz2=0;

	//! check that none of the coordinates have gone "bad" when updated
	if (lane.startPoint.x!=0 && lane.startPoint.y!=0 && lane.startPointz!=0 && lane.endPoint.x!=0 && lane.endPoint.y!=0 && lane.endPointz!=0){


	  //! transforming start point (use functions defined in stereoImageBlob)
	  px=lane.startPoint.x;
	  py=lane.startPoint.y;
	  pz=lane.startPointz; 
	  StereoImageBlobLocalToVehicle(&this->stereoBlob, px, py, pz, &qx, &qy, &qz);
	  StereoImageBlobVehicleToSensor(&this->stereoBlob, qx, qy, qz, &vx, &vy, &vz);
	  StereoImageBlobSensorToImage(&this->stereoBlob, vx, vy, vz, &tx, &ty, &tz);
	  lane.startPoint.x = tx;
	  lane.startPoint.y = ty;
	  lane.startPointz = tz;//disparity

	  //! transforming end point (use functions defined in stereoImageBlob)
	  px2=lane.endPoint.x;
	  py2=lane.endPoint.y;
	  pz2=lane.endPointz;       
	  StereoImageBlobLocalToVehicle(&this->stereoBlob, px2, py2, pz2, &qx2, &qy2, &qz2);
	  StereoImageBlobVehicleToSensor(&this->stereoBlob, qx2, qy2, qz2, &vx2, &vy2, &vz2);
	  StereoImageBlobSensorToImage(&this->stereoBlob, vx2, vy2, vz2, &tx2, &ty2, &tz2);
	  lane.endPoint.x = tx2;
	  lane.endPoint.y = ty2;
	  lane.endPointz = tz2;//disparity

	  //! ensure that the start point really has a smaller x than the end point, otherwise make it so
	  if (lane.startPoint.x>lane.endPoint.x){
	    lane.startPoint.x= tx2;
	    lane.startPoint.y = ty2;
	    lane.startPointz = tz2;
	    lane.endPoint.x = tx;
	    lane.endPoint.y = ty;
	    lane.endPointz = tz;}

	  //! a check is made that the points have good disparity and are within the boundaries of the image frame
	  if(tz>0 && tz2>0 && tz!= 65535 && tz2!= 65535 && lane.startPoint.x>0 && lane.startPoint.y>0 && lane.endPoint.x>0 && lane.endPoint.y>0 && lane.startPoint.x<640 && lane.startPoint.y<480 && lane.endPoint.x<640 && lane.endPoint.y<480){

	    //MSG("LanesInImage: Xs: %.2f, Ys: %.2f, Xe: %.2f, Ye: %.2f",lanesIn[i].startPoint.x,lanesIn[i].startPoint.y, lanesIn[i].endPoint.x, lanesIn[i].startPoint.y); 

	    //! all the lines from dataAssociation get stored in lanesCorrected (lines are in image frame) 
	    lanes.push_back( lane);}
	}
      }

      lanesIn.clear();
      lanesIn=lanes;

      return lanesIn;
}
Line RoadPerceptor::simpleLineModel(Line thisOldLine){

     float deltaTime, angle, deltaX, deltaY, deltaZ;
     uint64_t newTime;
     float PI = 3.141592654;
     float deg =0.005;
     float speed = 0.001;

     //! get the time that is used in the prediction step
     newTime = this->stereoBlob.timestamp;
     deltaTime = (float)(newTime-thisOldLine.time)/1000000;

     if(linemodel == true){MSG("deltatime %.4f", deltaTime);}

     //! get the speed that Alice had when this line got init or updated last
     float aliceVelX = thisOldLine.localXVel;
     float aliceVelY = thisOldLine.localYVel;
     float aliceVelZ = thisOldLine.localZVel; 

     angle = getLineAngle(thisOldLine.startPoint.x,thisOldLine.startPoint.y, thisOldLine.endPoint.x, thisOldLine.endPoint.y);  
     if (linemodel == true){MSG("angle: %.4f, XS: %.2f,YS: %.2f,XS: %.2f,YS: %.2f", angle,thisOldLine.startPoint.x,thisOldLine.startPoint.y, thisOldLine.endPoint.x, thisOldLine.endPoint.y);}

     if ((aliceVelX>-speed && aliceVelX<speed) && (aliceVelY<-speed || aliceVelY>speed)){
             
       if (angle > -deg && angle < deg ){deltaY = 0; deltaX = 0;}
       else if (angle > 90-deg && angle < 90+deg){deltaY = aliceVelY*deltaTime; deltaX = 0;}
       else {angle = (PI/180)*angle; deltaY = aliceVelY*deltaTime; deltaX = aliceVelY*deltaTime/tan(angle);}

     }

     if ((aliceVelY > -speed && aliceVelY < speed) && (aliceVelX < -speed || aliceVelX > speed)){

       if (angle > -deg && angle < deg){deltaY = 0; deltaX = aliceVelX*deltaTime;}
       else if (angle > 90-deg && angle < 90+deg){deltaY = 0; deltaX =0;}
       else {angle = (PI/180)*angle; deltaX = aliceVelX*deltaTime; deltaY = aliceVelX*deltaTime*tan(angle);}
    
     }

     if ((aliceVelX > -speed && aliceVelX < speed) && (aliceVelY > -speed && aliceVelY < speed)){deltaX = 0; deltaY = 0;}

     if ((aliceVelX < -speed || aliceVelX > speed) && (aliceVelY < -speed || aliceVelY > speed)){

	 if (angle > -deg && angle < deg){deltaX = aliceVelX*deltaTime; deltaY = 0;}
	 else if (angle > 90-deg && angle < 90+deg){deltaX = 0; deltaY = aliceVelY*deltaTime;}
	 else {
	   angle= (PI/180)*angle;
	   if (aliceVelX*aliceVelX > aliceVelY*aliceVelY) {deltaY = aliceVelY*deltaTime; deltaX = aliceVelY*deltaTime/tan(angle);}
	   else{deltaX = aliceVelX*deltaTime; deltaY = aliceVelX*deltaTime*tan(angle);}
	 }
     }

     if (aliceVelZ > -speed && aliceVelZ < speed){deltaZ = 0;}
     else{ deltaZ = aliceVelZ*deltaTime;}

     if (linemodel == true){MSG("delta x: %.4f, delta y: %.4f, delta z: %.4f", deltaX, deltaY, deltaZ); MSG("vel x: %.4f, vel y: %.4f, vel z: %.4f", thisOldLine.localXVel, thisOldLine.localYVel, thisOldLine.localZVel);}

     //deltaX=0; deltaY=0; deltaZ=0;//debugging purposes

     thisOldLine.localXVel = (float) this->stereoBlob.state.localXVel;
     thisOldLine.localYVel = (float) this->stereoBlob.state.localYVel;
     thisOldLine.localZVel = (float) this->stereoBlob.state.localZVel;
     thisOldLine.deltaX = deltaX;
     thisOldLine.deltaY = deltaY;
     thisOldLine.deltaZ = deltaZ;

     if (linemodel==true){
     MSG("NEW:vel x: %.4f, vel y: %.4f, vel z: %.4f", thisOldLine.localXVel, thisOldLine.localYVel, thisOldLine.localZVel);}
     thisOldLine.time=newTime;

     return thisOldLine;
}  

   

/*---------------------------------------------*/
//! MAIN FUNCTION
/*---------------------------------------------*/

// Main program thread
int main(int argc, char **argv) {
  RoadPerceptor *percept;


  percept = new RoadPerceptor();
  assert(percept);

  percept->number = 0;  
  percept->Q = 10e-3;
  percept->R = 10e-3;

  //unsigned long long oldTime;
  double oldTime;
  oldTime=DGCgettime()/1000000;

  //  memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  // Get a single image for initializing the images
  while (percept->readImage() != 0) {
    percept->readImage();
  }
  // Initialize images
  percept->initImages();

  while (!percept->quit) {
    //delay for a while
    if (percept->replay) {    
	//usleep(500);
    }

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // Run process control
    percept->handleProcessControl();

    // If we are paused, dont do anything
    if (percept->pause) {
      usleep(0);
      continue;
    }

    // Check for new data
    if (percept->readImage() == 0) {

	//check if goto frame given
	if (percept->gotoFrame!=-1 && percept->stereoBlob.frameId < percept->gotoFrame)
	    continue;

	// Detect lines
	percept->findLines();  

    }
  }

  percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}
