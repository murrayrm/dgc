
/* 
 * Desc: Road perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard, Humberto Pereira
 * CVS: $Id$
*/


#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineMsg.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>


// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"

#include "RoadPerceptor.hh"


//0: end points in local frame
//1: r-theta in local frame

int ROIstartX = 50;
int ROIstartY = 100;
int ROIwidth = 540;
int ROIheight = 200; 

int DEBUG_LINES = 0;

int lanesPreviousSize = 0;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
RoadPerceptor::RoadPerceptor()
{
  memset(this, 0, sizeof(*this));
  

  return;
}


// Default destructor
RoadPerceptor::~RoadPerceptor()
{
  destroyImages();
  return;
}

// allocate memory for the images
void RoadPerceptor::initImages() {
  // Stage Images
  
  imBuffer = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imBuffer2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  
  imCO0 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCO1 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO2 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imCO3 = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imC = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  imOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
  imProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1); 
  imCOriginal = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);
  imCProcess = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);

  // Apply Regions Of Interest
  cvGetSubRect(imCO0, imCO0, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));
  cvGetSubRect(imCO1, imCO1, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));
  cvGetSubRect(imCO2, imCO2, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));
  cvGetSubRect(imBuffer, imBuffer, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));
  cvGetSubRect(imBuffer1, imBuffer1, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));
  cvGetSubRect(imBuffer2, imBuffer2, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));

  return;

}

// deallocate memory for the image
void RoadPerceptor::destroyImages() {
  cvReleaseMat(&imBuffer1);
  cvReleaseMat(&imBuffer2);
  cvReleaseMat(&imBuffer);

  cvReleaseMat(&imC);
  cvReleaseMat(&imCO0);
  cvReleaseMat(&imCO1);
  cvReleaseMat(&imCO2);
  cvReleaseMat(&imCO3);

  cvReleaseMat(&imCProcess);
  cvReleaseMat(&imCOriginal);
  cvReleaseMat(&imProcess);
  cvReleaseMat(&imOriginal);

  return;

}

// Parse the command line
int RoadPerceptor::parseCmdLine(int argc, char **argv)
{
  // Default configuration path
  char defaultConfigPath[256];
  //  char filename[256];

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

    // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(defaultConfigPath, sizeof(defaultConfigPath),
             "%s/lineperceptor", getenv("DGC_CONFIG_PATH"));
  else
    return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
  {
      //check if replay or blob mode
      if (strstr(this->options.inputs[0], ".blob") != 0)
	  this->mode = modeBlob;
      else
	  this->mode = modeReplay;
  }
  else
    this->mode = modeLive;

  //show option
  if (this->options.show_given)
      this->show = this->options.show_flag;
  else
      this->show = false;
 
  //step option
  if (this->options.step_given)
      this->step = this->options.step_flag;
  else
      this->step = false;

  //export option
  if (this->options.save_given)
      this->save = this->options.save_flag;
  else
      this->save = false;

  //export option
  if (this->options.save_debug_given)
      this->save_debug = this->options.save_debug_flag;
  else
      this->save_debug = false;

  //debug option
  if (this->options.debug_given)
      this->debug = this->options.debug_flag;
  else
      this->debug = false;
  DEBUG_LINES = this->debug;

  //goto option
  if (this->options.goto_given)
      this->gotoFrame = this->options.goto_arg;
  else
      this->gotoFrame = -1;

  //read the stop line perceptor conf
  /*  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "StopLinePerceptor.conf");
  mcvInitStopLinePerceptorConf(filename, &this->stopLineConf);

  //read the lane perceptor conf
  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "LanePerceptor.conf");
  mcvInitStopLinePerceptorConf(filename, &this->laneConf);
  
  //read camera info
  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "CameraInfo.conf");
  mcvInitCameraInfo(filename, &this->cameraInfo);    
  */
  //tracking option
  if (this->options.track_stoplines_given)
      this->trackStopLines = this->options.track_stoplines_flag;
  else
      this->trackStopLines = false;
  this->numPreFrames = 0;
  this->numPostFrames = 0;

  //detections
  if (this->options.no_stoplines_given)
      this->noStoplines = this->options.no_stoplines_flag;
  else
      this->noStoplines = false;
  if (this->options.no_lanes_given)
      this->noLanes = this->options.no_lanes_flag;
  else
      this->noLanes = false;


  
  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoadPerceptor $Revision$                                                   \n"
"                                                                           \n"
"Mode: %mode%                                                               \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"Stop lines: %stoplineStats%                                                \n"
"Lane lines: %laneStats%                                                    \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%SHOW%|%FAKE%|%SKIP%|%DEBUG%|%EXPORT%|%STEP%]              \n";

// Initialize console display
int RoadPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%SHOW%", " SHOW ", "Ss",
		   (cotk_callback_t) onUserShow, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);

  //button to skip through the log files
  cotk_bind_button(this->console, "%SKIP%", " SKIP ", "Kk",
                   (cotk_callback_t) onUserSkip, this);

  //button to enable/disable debug
  cotk_bind_button(this->console, "%DEBUG%", " DEBUG ", "Dd",
                   (cotk_callback_t) onUserDebug, this);

  //button to enable/disable stepping
  cotk_bind_button(this->console, "%STEP%", " STEP ", "Tt",
                   (cotk_callback_t) onUserStep, this);

  //button to export current frame
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);

    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%mode%", A_NORMAL, "%s", 
	      this->mode==modeReplay ? "Replay" : this->mode==modeBlob ? "Blob" : "Normal");


  return 0;
}


// Finalize sparrow display
int RoadPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int RoadPerceptor::initSensnet()
{
  // Start sensnet, even if we are in replay mode, since we may wish
  // to send line messages.
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  if (this->mode == modeLive)
  {
    // Join stereo group for live stereo data
    if (sensnet_join(this->sensnet, this->sensorId,
                     SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
      return -1;
  }
  else if (this->mode == modeReplay)
  {
    // Open replay of exisiting log
    this->replay = sensnet_replay_alloc();
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs)!=0)
      return -1;
  }


  //init sending to mapp
  mapElementTalker.initSendMapElement(this->skynetKey);

  return 0;
}


// Clean up sensnet
int RoadPerceptor::finiSensnet()
{
  if (this->sensnet)
  {
    sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }

  if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Read the current image
int RoadPerceptor::readImage()
{
    //int blobId;

  // Live mode
  if (this->mode == modeLive)
  {
//     // Take a peek at the latest data
//     if (sensnet_peek(this->sensnet, this->sensorId,
//                      SENSNET_STEREO_IMAGE_BLOB, &blobId, NULL) != 0)
//       return -1;
  
//     // Do we have new data?
//     if (blobId < 0 || blobId == this->blobId)
//       return -1;

    //use blocking wait for new data to arrive
    if( sensnet_wait(this->sensnet, 500) != 0)
	return -1;      
    
    // Read the new blob
    if (sensnet_read(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                     &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;
  }

  // Replay mode
  else if (this->mode == modeReplay)
  {
    // Advance log one step
    if (sensnet_replay_next(this->replay, 0) != 0)
      return -1;
    
    // Read the new blob
    if (sensnet_replay_read(this->replay, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                            &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;

    MSG("read blob %d", this->blobId);
  }

  //modeBlob
  else if (this->mode == modeBlob)
  {
      //check if already read it
      if(this->stereoBlob.frameId!=0)
	  return -1;
      //load the blob from the blob file
      FILE *file;
      if ((file=fopen(this->options.inputs[0], "r")) == 0)
	  return -1;
      if (fread(&this->stereoBlob, sizeof(this->stereoBlob), 1, file)!= 1)
	  return -1;
      fclose(file);
      MSG("read blob: %d", this->stereoBlob.frameId);
  }

  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
}


// Write the current stop lines
int RoadPerceptor::writeLines()
{
  
  return 0;
}    


// Handle button callbacks
int RoadPerceptor::onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RoadPerceptor::onUserPause(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



// Handle fake stop button
int RoadPerceptor::onUserFake(cotk_t *console, RoadPerceptor *self, const char *token)
{
  float px, py, pz;
    
  MSG("creating fake stop line data");

  self->lineMsg.num_lines = 0;
      
  // MAGIC
  px = VEHICLE_LENGTH + 3;
  py = -2;
  pz = VEHICLE_TIRE_RADIUS;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);

  self->lineMsg.lines[0].a[0] = px;
  self->lineMsg.lines[0].a[1] = py;
  self->lineMsg.lines[0].a[2] = pz;

  px = VEHICLE_LENGTH + 3;
  py = +2;
  pz = 0.5;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);
      
  self->lineMsg.lines[0].b[0] = px;
  self->lineMsg.lines[0].b[1] = py;
  self->lineMsg.lines[0].b[2] = pz;
  
  self->lineMsg.num_lines = 1;
  
  self->totalFakes += 1;

	self->writeLines();
	self->lineMsg.num_lines = 0;

	return 0;
}


int RoadPerceptor::onUserShow(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->show = !self->show;
  MSG("show %s", (self->show ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserSkip(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->skip = !self->skip;
  MSG("skip %s", (self->skip ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserDebug(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->debug = !self->debug;
  DEBUG_LINES = self->debug;
  MSG("debug %s", (self->debug ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserStep(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->step = !self->step;
  MSG("step %s", (self->step ? "on" : "off"));
  return 0;
}

int RoadPerceptor::onUserExport(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->exportImage();
  return 0;
}



// export current image in stereo blob
int RoadPerceptor::exportImage()
{
    CvMat im3 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
		      this->stereoBlob.channels==1 ? CV_8UC1 : CV_8UC3, 
		      this->stereoBlob.imageData + this->stereoBlob.leftOffset);
    
    char str[255];
    sprintf(str, "im-%08d.png", this->stereoBlob.frameId);
    cvSaveImage(str, &im3);
    MSG("Written file: %s", str);

    //write the whole stereoblob structure
    sprintf(str, "blob-%08d.blob", this->stereoBlob.frameId);
    FILE *f;
    if( (f = fopen(str, "w")) == 0)
	return -1;
    if( fwrite(&this->stereoBlob, sizeof(this->stereoBlob), 1, f) != 1)
	return -1;
    fclose(f);
    MSG("Wrote blob: %s", str);

    return 0;
}


//initialize the kalman filter structure
void RoadPerceptor::initStopLineTrack()
{

}

//destroy the track
void RoadPerceptor::destroyStopLineTrack()
{
    if (this->stopLineTrackCreated)
    {
	//release the current track
	cvReleaseKalman(&this->kalman);
	//reset
	this->stopLineTrackCreated = false;
	//msg
	MSG("Track destroyed");
    }
}

//track the detected stop line
void RoadPerceptor::updateStopLineTrack()
{

}


// Find stop lines
int RoadPerceptor::findLines() 
{
    int i,j;
    uint8_t *pix;

    vector<Line> lanes;
    MapElement mE;

    //get pointer to image data
    pix = StereoImageBlobGetLeft(&this->stereoBlob, 0, 0);
   
    CvMat rawim;

    //check number of channels of input image
    //grayscale image
    if (this->stereoBlob.channels==1)
    {
	//get image
        rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1, pix);
		
    }
    //RGB image
    else {
	//get raw image. its in RGB
	rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3, pix);

      	cvCopy(&rawim, imCOriginal, NULL);
	cvCopy(&rawim, imCProcess, NULL); 

	//convert to single channel
	cvCvtColor(imCOriginal,imOriginal,CV_RGB2GRAY);
	
	cvGetSubRect(imCOriginal, imC, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));
	cvGetSubRect(imOriginal, imProcess, cvRect(ROIstartX, ROIstartY, ROIwidth, ROIheight));

	// split channels to R+G+B
	cvSplit(imC, imCO0, imCO1, imCO2, NULL);
 
	// smooth green channel
	/*	
	if (this->debug) SHOW_IMAGE(imC, "before green Smoothing", 10);
	cvSmooth(imCO0,imCO0,CV_GAUSSIAN,3,0,1);
	cvMerge(imCO0,imCO1,imCO2,NULL,imC);
	if (this->debug) SHOW_IMAGE(imC, "After Green Smoothing", 10);
	*/

	//CvScalar one = cvRealScalar(255);
	//cvSubRS(imCO2,one,imBuffer);
	// boost yellow channel
	
	cvMin(imCO0,imCO1,imBuffer);
	cvMin(imBuffer,imCO2,imBuffer1);
	if (this->debug) SHOW_IMAGE(imBuffer1, "minimum of RGB", 10);

	cvAddWeighted(imCO0,0.80, imCO1, 0.2, 0, imBuffer);
	cvSub(imBuffer,imCO2,imBuffer2,NULL);
	//cvConvertScale(imBuffer2,imBuffer,2.0,0);

	if (this->debug) SHOW_IMAGE(imBuffer2, "Yellow channel", 10);

	 /*	cvAddWeighted(imCO0,0.5, imCO1, 0.5, 0, imBuffer);
	 cvSub(imBuffer,imCO2,imBuffer,NULL);
	 if (this->debug) SHOW_IMAGE(imBuffer, "Yellow channel 50-50", 10);
	
	 */

	// Add Yellow boost 
	cvAdd(imProcess,imBuffer2,imProcess,NULL);
	if (this->debug) SHOW_IMAGE(imProcess, "After GreenSmooth and YellowBoost",10);

    }

    MSG("Processing frame#%d", this->stereoBlob.frameId);

    if(!this->skip)
    {
	//used for statistics
	static long numLaneFrames=0;
	static double totalLaneTime=0.0;
	double laneTime;

	//get lanes
	if (!this->noLanes)
	{
	    // Search for lines
	    int64 startTime, endTime;    
	    startTime = cvGetTickCount();

	    // TODO: add option
	    if (this->debug) {
	      SHOW_IMAGE(imCO0, "Red / Hue channel", 10);
	      SHOW_IMAGE(imCO1, "Green / Saturation channel", 10);
	      SHOW_IMAGE(imCO2, "Blue / Value channel", 10);
	    }
	    
	    cvCanny(imProcess, imProcess, 80, 280, 3);
	    
	    if (this->show) SHOW_IMAGE(imProcess, "edges", 10);

	    CvMemStorage* storage = cvCreateMemStorage(0);
	    CvSeq* lines = 0;
	    
	    // Probabilistic Hough Transform to get lines
	    lines = cvHoughLines2(imProcess, storage, CV_HOUGH_PROBABILISTIC, 1, 1*CV_PI/180, 80, 20, 3);
	    
	  
	    /*	    // printlines
	    for( i = 0; i < lines->total; i++ ) {
	      CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i); 
	      line[0].x +=ROIstartX; //hardcoded LIMITS FOR THE IMAGE
	      line[0].y +=ROIstartY;
	      line[1].x +=ROIstartX;
	      line[1].y +=ROIstartY;
	      if (this->show) cvLine(imCProcess, line[0], line[1], CV_RGB(255,0,0), 1, 8 );
	   
	    }
	    */
	   if (this->show) SHOW_IMAGE(imCProcess, "all Hough lines", 10);
 
	    endTime = cvGetTickCount();
 
	    for (i=0; i<lines->total; i++) {
	      Line thisLine;
	      CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);
	      thisLine.startPoint.x = line[0].x + ROIstartX;
	      thisLine.startPoint.y = line[0].y + ROIstartY;
	      thisLine.endPoint.x = line[1].x + ROIstartX;
	      thisLine.endPoint.y = line[1].y + ROIstartY;
	      thisLine.score = 0;
	      thisLine.source = 2; // yellow channel
	      thisLine.color = getLineColor(thisLine);
	      lanes.push_back(thisLine);

	    }
	   
    for( i = 0; i < lines->total; i++ ) {
    //! ignore this line if it has been considered by a parallel one
    if (lanes[i].score == 3) continue;
   	
    CvPoint* line = (CvPoint*) cvGetSeqElem(lines,i);	
    CvPoint* line2;	
		
    int hasPair=0;
       
    // check if there is a double line
    for (j=i+1; j< lines->total; j++) {
      line2 = (CvPoint*) cvGetSeqElem(lines,j);	
      hasPair = checkDouble((CvPoint) line[0],(CvPoint) line[1],(CvPoint) line2[0],(CvPoint) line2[1],1.4,4.0,15);
      if (hasPair) break;
    }
    	
    //! this could be prettier, checking all and not one possible pair for each line and grouping closer pairs
    //! lines with pairs  
    if (hasPair) { 
      if( distance((CvPoint) line[0], (CvPoint) line[1]) > distance((CvPoint) line2[0], (CvPoint) line2[1]) ) {
	lanes[i].score=4;
    	lanes[j].score=3;
      } else {
	lanes[i].score=3;
    	lanes[j].score=4;
      }
      
      //! if line is big enough, let's still take it on caution
      } else if (distance((CvPoint) line[0], (CvPoint) line[1]) > 100) {
	lanes[i].score=2;
      } else {
    	//! remaining lines are considered unimportant
	lanes[i].score=1;
      }
  }

	    cvReleaseMemStorage(& storage);

	    //stats
	    numLaneFrames++;
	    laneTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
	    totalLaneTime += laneTime;

	    //display
	    if (this->console)
		cotk_printf(this->console, "%laneStats%", A_NORMAL, 
			    "%.2fms\tAvg:%.2fms\t%.2fHz", laneTime, 
			    totalLaneTime/numLaneFrames, 
			    numLaneFrames/totalLaneTime*1000);
	    else
		MSG("Lane line takes %.2fms\tavg %.2fms\t%.2fHz", laneTime,
		    totalLaneTime/numLaneFrames, numLaneFrames/totalLaneTime*1000);

	}
    } 
      
    //save image with detected lines
    if(this->save_debug)
    {
    
    }
    //show detected lines
    if (this->show)
    {    
      int key=cvWaitKey( this->step ? 0 : 10);
      if (key=='e' || key=='E')
	  this->exportImage();
      else if (key == 'q' || key == 'Q')
	  this->onUserQuit(console, this, "");
      else if (key == 'p' || key == 'P')
	  this->onUserPause(console, this, "");
      else if (key == 't' || key=='T')
	  this->onUserStep(console, this, "");
//       else if (key == 'k' || key == 'K') //skip	 
// 	  return 0;
    }
    //not showing, so export the image
    if (this->save)
    {
      // this->exportImage();
    }
    
    
  //put detected lines in message    
  MSG("Found %d lines", (int)lanes.size());

  for (i = 0; i<(int)lanes.size(); i++) {
        
  CvPoint point1,point2;
  point1.x = lanes[i].startPoint.x;
  point1.y = lanes[i].startPoint.y;
  point2.x = lanes[i].endPoint.x;
  point2.y = lanes[i].endPoint.y;

  if (this->show) {
    if (lanes[i].score <2 ) cvLine(imCProcess, point1, point2, CV_RGB(255,0,0), 1, 8 );
    if (lanes[i].color == GREEN) cvLine(imCProcess, point1, point2, CV_RGB(0,255,0), 1, 8 );
    if (lanes[i].color == WHITE) cvLine(imCProcess, point1, point2, CV_RGB(255,255,255), 1, 8 );
    if (lanes[i].color == YELLOW) cvLine(imCProcess, point1, point2, CV_RGB(255,255,0), 1, 8 );
    if (lanes[i].color == OTHER) cvLine(imCProcess, point1, point2, CV_RGB(255,0,255), 1, 8 );

  }

  // lanes that are not supposed to be output must be cleaned
  if (lanes[i].color == GREEN || lanes[i].color == OTHER || lanes[i].score <2 || lanes[i].score == 3) {
    cleanMapElement(mE, i);
    continue;
  }

  imageLineToMapElement(lanes[i], mE, i);

  }

  if (this->show) SHOW_IMAGE(imCProcess, "all Hough lines", 10);


  if (lanes.size()<lanesPreviousSize) {
    for (i=lanes.size(); i<lanesPreviousSize; i++) {
      cleanMapElement(mE, i);
    }
  }
  
  lanesPreviousSize = lanes.size();

  lanes.clear();
  
  return 0;
}

// Returns the line color 
int RoadPerceptor::getLineColor(const Line &imageLine) {
  CvScalar p,a=cvScalarAll(0.0);
  int i=0, size=0;
  int x=0, y=0;
  float distG, distY, distW;

  size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));

  for (i=0; i<size; i++) {
    x = (int) imageLine.startPoint.x + i*(imageLine.endPoint.x-imageLine.startPoint.x)/size;
    y = (int) imageLine.startPoint.y + i*(imageLine.endPoint.y-imageLine.startPoint.y)/size;
   
    p=cvGet2D(imCOriginal,y,x);

    a.val[0] = a.val[0] + p.val[0];
    a.val[1] = a.val[1] + p.val[1];
    a.val[2] = a.val[2] + p.val[2];

  }

  a.val[0] = round(a.val[0]/size);
  a.val[1] = round(a.val[1]/size);
  a.val[2] = round(a.val[2]/size);

  distG = sqrt(pow(a.val[0],2)+pow(a.val[1]-255,2)+pow(a.val[2],2));
  distW = sqrt(pow(a.val[0]-255,2)+pow(a.val[1]-255,2)+pow(a.val[2]-255,2));
  distY = sqrt(pow(a.val[0]-255,2)+pow(a.val[1]-255,2)+pow(a.val[2],2));

  if (min3(distG, distW, distY) == distG) return GREEN;
  if (min3(distG, distW, distY) == distW) return WHITE;
  if (min3(distG, distW, distY) == distY) return YELLOW;
    

    // MSG("R=%f, G=%f, R=%f\n",a.val[0],a.val[1],a.val[2]);
  return OTHER;
}

float min3(float a, float b, float c) {
  if (min(a,b)==a && min(a,c)==a) return a; 
  if (min(a,b)==b && min(b,c)==b) return b; 
  return c;
}

// extend this line
int RoadPerceptor::extendLocalLine(const Line &imageLine, float pi, float pj, float pi2, float pj2, float &px, float &py, float &px2, float &py2) {
  
  float sizeRatio;
  float wingSize;
  
  float lineSize;
  float sinTeta;
  float cosTeta;
  float direction;

  lineSize = sqrt(pow(px2-px,2)+pow(py2-py,2));
  sinTeta = fabs(py2-py)/lineSize;
  cosTeta = fabs(px2-px)/lineSize;

  // start wing
  if (imageLine.startPoint.x != pi || imageLine.startPoint.y != pj) {
    sizeRatio = sqrt(pow(pi-imageLine.startPoint.x,2)+pow(pj-imageLine.startPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;
    direction = (px-px2)/fabs(px-px2);
    px = px + direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py = py + direction*wingSize*sinTeta;
  }

  // end wing
  if (imageLine.endPoint.x != pi2 || imageLine.endPoint.y != pj2) {
    sizeRatio = sqrt(pow(pi2-imageLine.endPoint.x,2)+pow(pj2-imageLine.endPoint.y,2)) / sqrt(pow(imageLine.endPoint.x-imageLine.startPoint.x,2)+pow(imageLine.endPoint.y-imageLine.startPoint.y,2));
  
    wingSize = sizeRatio * lineSize;

    direction = (px-px2)/fabs(px-px2);
    px = px - direction*wingSize*cosTeta;
    direction = (py-py2)/fabs(py-py2);
    py = py - direction*wingSize*sinTeta;
    
  }

  return 1;
}

// finds point near to provided, over line, that has good disparity
int RoadPerceptor::getBestNearestPoint(const Line &imageLine, float & pi, float & pj, float  & pd, int direction) {

  uint16_t testDisp;
  float px, py, pz;

  int size=0, i=0;

  float dist=0;
  float beginX=0, beginY=0;
  float endX=0, endY=0;
  
  size = (int) sqrt(pow(imageLine.endPoint.y-imageLine.startPoint.y,2)+pow(imageLine.endPoint.x-imageLine.startPoint.x,2));

    if (direction == 1) {
      beginX = imageLine.startPoint.x;
      beginY = imageLine.startPoint.y;
      endX = imageLine.endPoint.x;
      endY = imageLine.endPoint.y;
    } else {
      beginX = imageLine.endPoint.x;
      beginY = imageLine.endPoint.y;
      endX = imageLine.startPoint.x;
      endY = imageLine.startPoint.y;
    }
    
    // try to get disparity from other points in line
    for (i=0; i<size; i++) {
      pi = beginX + i*direction*(imageLine.endPoint.x-imageLine.startPoint.x)/size;
      pj = beginY + i*direction*(imageLine.endPoint.y-imageLine.startPoint.y)/size;

     // we just walked the whole line, don't check this point
      if (pi == endX && pj == endY) return 0;

      testDisp = (*StereoImageBlobGetDisp(&this->stereoBlob, (int) pi, (int) pj));
      
      // good disparity, lets stick with this point
      if (testDisp>0 && testDisp != 65535) {
	pd = (float) testDisp;
	pd /= this->stereoBlob.dispScale;
	imageToLocalPoint(pi, pj, pd, px, py, pz);
	
	// check if the point is on alice or too far away (lousy but valid disparity)
	dist = sqrt(pow(this->stereoBlob.state.localX-px,2)+pow(this->stereoBlob.state.localY-py,2));
	
	// TODO: UN-HARDCODE THESE LIMITS
	if (dist > 1 && dist < 60) {
	  return 1;
	}
      }
    }

    return 0;
}

void RoadPerceptor::imageToLocalPoint(float pi, float pj, float pd, float &px, float &py, float &pz) {

    // Compute point in the sensor frame
    StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);
    // Convert to vehicle frame
    StereoImageBlobSensorToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);
    // Convert to local frame
    StereoImageBlobVehicleToLocal(&this->stereoBlob, px, py, pz, &px, &py, &pz);

}


//cleans exceding mapElements
int RoadPerceptor::cleanMapElement(MapElement &mE,int index) {
  mE.clear();
  mE.setId(index);

  this->mapElementTalker.sendMapElement(&mE, 0);

  return 1;
}


//converts from a line in image coordinates to a line in local coordinates
int RoadPerceptor::imageLineToMapElement(const Line &imageLine, MapElement &mE, int index) {
    float pi, pj, pi2, pj2, pd;
    float px, py, pz, px2, py2, pz2;

    point2arr points;

    mE.clear();

    //start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    
    // check if it's a good point or there is a good point nearby
    if (!getBestNearestPoint(imageLine, pi, pj, pd, 1)) return 0;

    // get the local frame coordinates for the best nearest point
    imageToLocalPoint(pi, pj, pd, px, py, pz);

    // fix the line taking into account we only used the segment between best nearest points
    // extendLocalLine(imageLine,pi,pj,pi2,pj2,px,py,px2,py2);

    // to high, probably an obstacle. TODO: better model for obstacles
    if (pz>0.5) return 0;
    
    //end point
    pi2 = imageLine.endPoint.x;
    pj2 = imageLine.endPoint.y;

    if (!getBestNearestPoint(imageLine, pi2, pj2, pd, -1)) return 0;

    // only one point on the line, discard it.
    if (pi == pi2 && pj == pj2) return 0;

    imageToLocalPoint(pi2, pj2, pd, px2, py2, pz2);
    
    // to high, probably an obstacle
    if (pz2>0.5) return 0;

    // get real line-end points
    extendLocalLine(imageLine, pi, pj, pi2, pj2, px, py, px2, py2);

    if (distance(px,py,px2,py2)<0.5) return 0;

    points.push_back(point2(px, py));
    points.push_back(point2(px2,py2));

    mE.setId(index);  
    mE.setGeometry(points);

    mE.height=0;
    if (imageLine.color == GREEN) mE.plotColor = MAP_COLOR_GREEN;
    if (imageLine.color == WHITE) mE.plotColor = MAP_COLOR_GREY;
    if (imageLine.color == YELLOW) mE.plotColor = MAP_COLOR_YELLOW;
    if (imageLine.color == OTHER) mE.plotColor = MAP_COLOR_ORANGE;
 
    mE.type = ELEMENT_LINE;
    mE.geometryType = GEOMETRY_LINE;
    mE.state = this->stereoBlob.state;
    
    this->mapElementTalker.sendMapElement(&mE, 0); 
    
    points.clear();
    mE.clear();
    return 0;
}

/*---------------------------------------------*/
//! HELPER FUNCTIONS
/*---------------------------------------------*/


float RoadPerceptor::getLineAngle(CvPoint start, CvPoint end) {
	float angle=0.0;
	
	//! starting point always has lower or equal X coordinate
	if (start.x == end.x) {
		angle = 90.0;
	} else {
		angle = 180/CV_PI * atan((float) (end.y-start.y)/(end.x-start.x));
	}

	return angle;
}

float RoadPerceptor::distance(CvPoint start, CvPoint end) {
	return sqrt(pow((double) (start.x-end.x),2)+pow((double) (start.y-end.y),2));
}

float RoadPerceptor::distance(float startx,float starty, float endx, float endy) {
	return sqrt(pow((double) (startx-endx),2)+pow((double) (starty-endy),2));
}

int RoadPerceptor::checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol) {
		
	//! size tolerance trial
	float size1 = distance(start1, end1);
	float size2 = distance(start2, end2);
	if ((size1 > size2 ? size1/size2 : size2/size1) > sizeTol) {
		return 0;
	}

	//! angle tolerance trial
	if (!parallel(start1, end1, start2, end2, angleTol)) {
		return 0;
	} 

	//! line convergence and alignment	
	if (!lineMatching(start1, end1, start2, end2, distanceTol)) {
		return 0;
	}

	// All trials passed. brillo
		
	return 1;
}

int RoadPerceptor::parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol) {
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	float angleDiff = fabs(angle1-angle2);
	if (!(angleDiff<=angleTol || (180.0-angleDiff)<=angleTol)) return 0;

	return 1;
	
}

int RoadPerceptor::lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol) {
	/*
		What we know: 
			a. Points in a line are ordered by lower X.
			a1. start1.x > end1.x
			a2. start2.x > end2.x
			b. line1 and line2 are almost parallel (this is after the angle tolerance trial)
	*/

	//! extreme 1 and 2 distance
	//! closer points at extremes
	float dE1, dE2;
	//! the bigger and smaller of dE1 and dE2
	float dMax, dMin;
	
	//! angles from -90 exclusive to +90 inclusive
	float angle1 = getLineAngle(start1, end1);
	float angle2 = getLineAngle(start2, end2);
	
	//! they are practically parallel. if the diference of angles is >90, we have a /\ or \/ tricky situation
	if (fabs(angle1-angle2) > 90) {
			dE1 = distance(start1,end2);
			dE2 = distance(start2,end1);	
	} else {
	//! if not, then just calculate normal distances
			dE1 = distance(start1,start2);
			dE2 = distance(end1,end2);	
	}
	
	dMax = dE1 >= dE2 ? dE1 : dE2;
	dMin = dMax == dE1 ? dE2 : dE1;

	if (dMin > distanceTol)	return 0;
	
	
	/*!
		There is another method that calculates the matching lengths of two parallel lines.
		It is somewhat more accurate in predicting some broken lines, but is still buggy
		For road lines it is not very important.
		The code is in deprecated_utils folder
	*/
	
	return 1;
}


/*---------------------------------------------*/
//! MAIN FUNCTION
/*---------------------------------------------*/

// Main program thread
int main(int argc, char **argv) {
  RoadPerceptor *percept;


  percept = new RoadPerceptor();
  assert(percept);

  //  memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  // Get a single image for initializing the images
  while (percept->readImage() != 0) {
    percept->readImage();
  }
  // Initialize images
  percept->initImages();

  while (!percept->quit) {
    //delay for a while
    if (percept->replay) {    
	//usleep(500);
    }

    // Update the console
    if (percept->console)
      cotk_update(percept->console);
    
    // If we are paused, dont do anything
    if (percept->pause) {
      usleep(0);
      continue;
    }

    // Check for new data

    if (percept->readImage() == 0) {

	//check if goto frame given
	if (percept->gotoFrame!=-1 && percept->stereoBlob.frameId < percept->gotoFrame)
	    continue;
	
	// Detect lines
	percept->findLines();  

    }
  }

  percept->finiConsole();
  percept->finiSensnet();
  percept->destroyStopLineTrack();

  MSG("exited cleanly");
  
  return 0;
}



