              Release Notes for "lineperceptor" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "lineperceptor" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "lineperceptor" module can be found in
the ChangeLog file.

Release R1-01c (Thu Aug 28 22:32:09 2008):
	1: Implemented a simpleLineModel2, one major change is the compensation for drifting. 2: Made it 
possible to show two maps; one for kalman-element (sub-group=-5) and one for non-kalman-element 
(sub-group=-4). For this purpose localLineToMapElementKalman has been changed, a cleanMapElementKalman has 
been implemented and the calls for the map-functions have been changed a bit. 3: transformationImageToLocal 
has been changed. It is now able to display a horizontal line in the correct angle. This is not the case in 
the original version. 4: Thresholds have been changed in merging-functions, map-functions and Canny 
algorithm. 5: Sub-group=-4 is supposed to display what the original roadperceptor was showing in its map. 
This must be checked again, since the result is not what was expected. 

Release R1-01b (Tue Aug 12 20:50:11 2008):
	1: Set the Canny thresholds to El Toro-sensitive. 2: 
Implemented the mergeLines in local frame. 3: Changed the 
imageLineToMapElement back to its original version so that if run with 
kalman flag off you get the original RoadPerceptor. TODO: 1: Some 
stop lines get displayed in an offset angle still. 2: Some yellow lines 
still get detected as white even though the change of the Canny 
thresholds helped some.

Release R1-01a (Mon Aug 11 23:11:41 2008):
	1: imageLineToMapElement has been divided into two versions, 
localImageLineToMapElementKalman and imageLineToMapElement. The first one gets called right 
after dataAssociation, this way no unnecessary transformation back and forth between frames 
have to be done. The second gets used if the kalman flag is off. The transformation in this 
one has been changed in the same way as in transImageToLocal. 2: The transformations in 
transImageToLocal have been changed. The function getBestNearestPoint is only used on the 
point closest to Alice. 3: The Canny thresholds are set for the St. Luke environment. 4: Min 
length for a map element to get displayed is changed from 0.4 m to 0.6 m. 5: A function that 
will merge lines in local frame before they get sent to localImageLineToMapElementKalman has 
been constructed but is here commented out. 6: The mapper recieves the kalman covariances if 
the line has been updated, otherwise it gets zeros. If the kalman flag is off, it gets 
constant 0.1. TODO: 1: merge of local lines 2: config-file for thresholds  etc. 3: Figure out 
why stop lines get displayed in an offset angle some times. 

Release R1-01 (Mon Aug 11  9:57:15 2008):
	1: cvPrediction has been changed to a "manual" code. 2: 
updateLineTrack has been divided into two parts. One that updates and on 
that predicts. The prediction step is performed on every old line in 
every frame, to ensure correct speed. 3: The simple line model has been 
made a function that gets called before the prediction step. 4: The 
button updates has the oppsite effect, on: just end point gets updated, 
off: both end points gets updated. 5: In this version the map element 
gets made up values for the covariances.

Release R1-00z (Wed Jul 23 21:44:21 2008):
	1: Lines did sometimes not show in kalman mode at St Luke. 
Changed the condition on z from 0.5 to 20 at line 1150. Helped when 
simulating with the same data. 2: Fixed a bug (unconditional jump) by adding an 
if-statement at line 1432. 3: Changed the console template to enable display 
of the last row when in Alice.

Release R1-00y (Tue Jul 22  2:15:21 2008):
	New implemtations: kalman structure with a simple line model, 
merging of sufficiently close lines, a system of post frames for display 
of no longer detected lines, inheritance of color, score, ID from an old 
line to a new one.

Release R1-00x (Thu Oct 25 21:48:41 2007):
	fixed bug of previous release that would send no lines


Release R1-00w (Thu Oct 25  2:27:02 2007):
	added an extra function to sort out lines according to their color variance, if normal classification isn't enough for the race.

Release R1-00v (Thu Oct 18 16:55:45 2007):
	small variable changes for speed bumps.

Release R1-00u (Thu Oct 18  3:51:08 2007):
	line intensity tunning.

Release R1-00t (Wed Oct 17 19:47:08 2007):
	separated Edge detecor on White and Yellow channels, to assign different params and sensibiltiies to each. should get more lines even in dim light,specially in El Toro where yellow paint is not reflective.

Release R1-00s (Tue Oct 16 16:54:53 2007):
	more logging options. small display bug corrected. more performance statistics.

Release R1-00r (Sat Oct 13 21:07:30 2007):
	changed cvSmooth to cvClose operation. 	better model for paralell lines that works better and FASTER.corrected one bug in log outputs.

Release R1-00q (Sat Oct  6 21:54:01 2007):
	Double lines: fixed color detected on right lines. Fixed a previously unkown bug where line oculd be shortened instead of lengthened!

Release R1-00p (Sat Sep 29 18:33:53 2007):
		changed image sources for edge detection: White Channel + Yellow
	Channel (White Channel on RGreyY, not on RGB). Adjusted Hough
	Transform parameters for line detection on such edge image(and
	works on segmented lines too). Lines that are on the WHite channel
	are taggeed white, the remaining are tested on the Yellow Channel
	(that includes white). Added minimum distance for parallel lines (3
	pixels). some more changes that I don't remember.
		added code for rescalling of the image in case *someone* changes
	the resolution or the size of the stereoBlobs. line color more
	robust with white channel comparizon. changed yellow boost to:
	yellow OR grayscale, which reduces artifact glare.


Release R1-00o (Fri Sep 28 18:35:38 2007):
  Added support for senset RoadLineBlob.

Release R1-00n (Fri Sep 21 16:23:01 2007):
	fixed all warnings, improved line reporting on console for performance diagnose.

Release R1-00m (Fri Sep 21 15:02:29 2007):
	added heartbeat and process control compatibility. small code change with line fitting.

Release R1-00l (Tue Sep 18 16:41:33 2007):
	correct all memory leaks, crash bugs. added lines for better
	visualisation. added horizon line to reduce number of detect lines
	and improve performance.

Release R1-00k (Sat Sep 15  0:58:13 2007):
	changing default module id in .ggo file to be MODroadPerceptor 
	when sending elements to the map 

	first int in the ID is now MODroadPerceptor: this is needed because module id is the only way mapper can appropriately sort clears into line/obstacles.



Release R1-00j (Sun Sep  2  3:30:36 2007):
	Improved double line association. New color matching scheme.

Release R1-00i (Sat Sep  1 15:21:25 2007):
	Calculate color in between lines for double lines.

Release R1-00h (Thu Aug 30 15:59:46 2007):
	Corrected all bugs related to mapElement handling. added yellow and white channels to get all lines at once, which still isnt perfect. colors are calculated with a simple distance to the Prototype yellow, green and white of RGB.line extending is being done with the ratio between lines in imageFrame and localFrame.

Release R1-00g (Thu Aug 16 17:11:33 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00f (Fri Aug 10  9:58:08 2007):
	* fixed color swapping bug
	* fixed jpl vision includes. To not use them anymore.
	* changes in export to mapviewer

Release R1-00e (Sun Aug  5 13:00:18 2007):
	Fixing the release notes so that they aren't confusing anymore. They had been accidentally copied over from line perceptor.

Release R1-00d (Sat Aug  4 23:11:28 2007):
	Making a release for humberto; line perception using the mid-range stereo-pair


Release R1-00c (Tue Mar 20 16:22:58 2007):
       changed SENSNET_STEREO_BLOB to SENSNET_STEREO_IMAGE_BLOB in
       RoadPerceptor.cc to get it to compile.


Release R1-00b (Wed Feb 21 22:17:03 2007):
updated to work with new skynet release


Release R1-00a (Thu Feb  8 17:23:18 2007):
       Template for roadperceptor module. Builds and runs, and has replay functionality.











































