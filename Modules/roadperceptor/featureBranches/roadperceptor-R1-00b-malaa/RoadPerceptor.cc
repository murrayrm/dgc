
/* 
 * Desc: Road perceptor run-time 
 * Date: 8 February 2007
 * Author: Andrew Howard, Mohamed Aly, ...
 * CVS: $Id$
*/

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
//include message type to send to mapper
//#include <interfaces/RoadLineMsg.h>

// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"


// Line perceptor module
class RoadPerceptor
{
  public:

  // Constructor
  RoadPerceptor();

  // Destructor
  ~RoadPerceptor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Read the current image
  int readImage();

  // Write the current stop lines
  int writeLines();

  public:

  // Find road
  int findRoad();

  public:

  // Initialize sparrow display
  int initConsole();

  // Finalize sparrow display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  //static int onUserFake(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserShow(cotk_t *console, RoadPerceptor *self, const char *token);

  /* TODO  
  // Sparrow callbacks; occurs in sparrow thread
  static int onUserShow(long);
  */

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

    //replay options
    bool replay; 
    char *logFile;

  // Source sensor id
  sensnet_id_t sensorId;

  // Sensnet module
  sensnet_t *sensnet;

  // Console display
  cotk_t *console;

  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  // Current blob id
  int blobId;
  
  // Current (incoming) stereo blob
  StereoImageBlob stereoBlob;

  // Current (outgoing) line message
  //RoadLineMsg lineMsg;

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
RoadPerceptor::RoadPerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
RoadPerceptor::~RoadPerceptor()
{
  return;
}


// Parse the command line
int RoadPerceptor::parseCmdLine(int argc, char **argv)
{
  // Default configuration path
  char defaultConfigPath[256];
  //char filename[256];

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

    // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(defaultConfigPath, sizeof(defaultConfigPath),
             "%s/lineperceptor", getenv("DGC_CONFIG_PATH"));
  else
    return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  //Fill out replay options
  if (this->options.replay_given)
      this->replay = (bool) this->options.replay_flag;
  else
      this->replay = false;
  //check if sim, then require the logfile
  if (this->replay)
  {
      if (this->options.log_file_given)
	  this->logFile = this->options.log_file_arg;
      else
	  return ERROR("Must specify a valid --log-file if specified --replay option");
  }

  
  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"RoadPerceptor $Revision$                                                   \n"
"                                                                           \n"
"Mode: %mode%                                                               \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";

// Initialize console display
int RoadPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  //cotk_bind_toggle(this->console, "%SHOW%", " SHOW ", "Ss", (cotk_callback_t) onUserShow, this);
  //cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff", (cotk_callback_t) onUserFake, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%mode%", A_NORMAL, "%s", this->replay ? "Replay" : "Normal");

  return 0;
}


// Finalize sparrow display
int RoadPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int RoadPerceptor::initSensnet()
{
  // Start sensnet
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);


  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return -1;
  
  // Join stereo group
  if (sensnet_join(this->sensnet, this->sensorId,
		   SENSNET_STEREO_BLOB, sizeof(StereoImageBlob), 5) != 0)
      return -1;

  //simulated run, so open the log file specified
  if (this->replay)
  {
      //open the log file
      if (sensnet_open_replay(this->sensnet, 1, &this->logFile)!=0)
	  return -1;
  }

  return 0;
}


// Clean up sensnet
int RoadPerceptor::finiSensnet()
{
    sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB);

  //replay
  if (this->replay)
  {
      sensnet_close_replay(this->sensnet);
  }
  sensnet_free(this->sensnet);
  
  return 0;
}


// Read the current image
int RoadPerceptor::readImage()
{
  int blobId, blobLen;

  //replay mode
  if (this->replay)
  {
      sensnet_wait(this->sensnet, 100);
  }

  // Take a peek at the latest data
  if (sensnet_peek(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB, &blobId, &blobLen) != 0)
      return -1;
  
  // Do we have new data?
  if (blobId < 0 || blobId == this->blobId)
      return -1;
  this->blobId = blobId;

  // Read the new blob
  if (sensnet_read(this->sensnet, this->sensorId,
		   SENSNET_STEREO_BLOB, blobId, blobLen, &this->stereoBlob) != 0)
      return -1;

  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
}



// Write the current stop lines
int RoadPerceptor::writeLines()
{
//   if (this->lineMsg.num_lines == 0)
//     return 0;
        
//   // Prepare the roadline message
//   //
//   this->lineMsg.msg_type = SNroadLine;
//   this->lineMsg.frameid = this->stereoBlob.frameId;
//   this->lineMsg.timestamp = this->stereoBlob.timestamp;
//   this->lineMsg.state = this->stereoBlob.state;

//   // Send line data
//   sensnet_write(this->sensnet, SENSNET_SKYNET_SENSOR,
//                 this->lineMsg.msg_type, 0, sizeof(this->lineMsg), &this->lineMsg);
  
// 	memset(&this->lineMsg, 0, sizeof(this->lineMsg));
  return 0;
}    


// Handle button callbacks
int RoadPerceptor::onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int RoadPerceptor::onUserPause(cotk_t *console, RoadPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



// // Handle fake stop button
// int RoadPerceptor::onUserFake(cotk_t *console, RoadPerceptor *self, const char *token)
// {
//   float px, py, pz;
    
//   MSG("creating fake stop line data");

//   self->lineMsg.num_lines = 0;
      
//   // MAGIC
//   px = VEHICLE_LENGTH + 3;
//   py = -2;
//   pz = VEHICLE_TIRE_RADIUS;

//   // Convert to local frame      
//   StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);

//   self->lineMsg.lines[0].a[0] = px;
//   self->lineMsg.lines[0].a[1] = py;
//   self->lineMsg.lines[0].a[2] = pz;

//   px = VEHICLE_LENGTH + 3;
//   py = +2;
//   pz = 0.5;

//   // Convert to local frame      
//   StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);
      
//   self->lineMsg.lines[0].b[0] = px;
//   self->lineMsg.lines[0].b[1] = py;
//   self->lineMsg.lines[0].b[2] = pz;
  
//   self->lineMsg.num_lines = 1;
  
//   self->totalFakes += 1;

// 	self->writeLines();
// 	self->lineMsg.num_lines = 0;

// 	return 0;
// }


// int RoadPerceptor::onUserShow(cotk_t *console, RoadPerceptor *self, const char *token)
// {
//   self->show = !self->show;
//   MSG("show %s", (self->show ? "on" : "off"));
//   return 0;
// }


// Find road
int RoadPerceptor::findRoad()
{
//   int i, j;
//   uint8_t *pix;
//   CvMat *image;

//   // Create float matrix for holding image
//   image = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, FLOAT_MAT_TYPE);
  
//   // Copy data into float matrix.
//   // TODO this only works properly for mono images.
//   pix = this->stereoBlob.colorData;  
//   for (j = 0; j < this->stereoBlob.rows; j++)
//   {
//       for (i = 0; i < this->stereoBlob.cols; i++)
//       {
// 	  cvmSet(image, j, i, (float) pix[0] / 255);
// 	  pix += this->stereoBlob.colorChannels;
//       }
//   }

  // Search for roads

    
  //free
  //cvReleaseMat(&image);
  
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  RoadPerceptor *percept;


  percept = new RoadPerceptor();
  assert(percept);

  //memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  while (!percept->quit)
  {

    // Update the console
    if (percept->console)
      cotk_update(percept->console);
    
    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Check for new data

    if (percept->readImage() == 0)
    {
      // Detect road
      percept->findRoad();

      //percept->writeLines(); 
      //memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 
    }

    // Write message
  

    // Update the display
    if (percept->console)
    {
	// cotk_printf(percept->console, "%stops%", A_NORMAL, "%d (%d fakes)",
        //          percept->totalStops, percept->totalFakes);
    }
  }

  percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



