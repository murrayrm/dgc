#ifndef ROAD_PERCEPTOR_HH
#define ROAD_PERCEPTOR_HH


/**
 * \file RoadPerceptor.hh
 * \author Humberto Pereira and Mohamed Aly and Andrew Howard
 * \date Thu 26 Jul, 2007
 *
 */


#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineMsg.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>

// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"

#define WHITE 1
#define YELLOW 2
#define OTHER 0
#define GREEN 3

///line structure with start and end points
typedef struct Line
{
  FLOAT_POINT2D startPoint;
  FLOAT_POINT2D endPoint;
  int score; 
  /* 
  0.not processed 
  1.unimportant 
  2.big isolated line 
  3.smaller line of pair. 
  4.bigger lineof pair
  */
  int color;
  /* CHANNEL
  1. white channel
  2. yellow channel
  */
  int source;
 
}Line;

// return minimum of 3 values
float min3(float a, float b, float c);

// Line perceptor module
class RoadPerceptor
{
public:
    
    // Constructor
    RoadPerceptor();
    
    // Destructor
    ~RoadPerceptor();
    
public:
    
    // Parse the command line
    int parseCmdLine(int argc, char **argv);
    
public:
    
  // Initialize sensnet
  int initSensnet();
    
  // Clean up sensnet
  int finiSensnet();
    

  // Start Image headers 
  void initImages();

  // Kill Image headers
  void destroyImages();

  // Read the current image
  int readImage();
    
  // Write the current lines
  int writeLines();

 public:

  // Find stop lines
  int findLines();


  //export current image
  int exportImage();


  public:

  // Initialize sparrow display
  int initConsole();

  // Finalize sparrow display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserFake(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserShow(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserSkip(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserDebug(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserStep(cotk_t *console, RoadPerceptor *self, const char *token);

  // Console button callback
  static int onUserExport(cotk_t *console, RoadPerceptor *self, const char *token);

private :

  // Get line color
  int getLineColor(const Line &imageLine);
    
  // Finds point near pi, pj, over provided line, that has good disparity
  int getBestNearestPoint(const Line &imageLine, float &pi, float &pj, float &pd, int direction);

  // From image to Local coordinates
  void imageToLocalPoint(float pi, float pj, float pd, float &px, float &py, float &pz);

  // Convert lines from image frame to local frame and put into the roadline message
  int imageLineToMapElement(const Line &imageLine, MapElement &mE, int index);

  // extend this line
  int extendLocalLine(const Line &imageLine, float pi, float pj, float pi2, float pj2, float &px, float &py, float &px2, float &py2);

  // Cleans exceding map Elements
  int cleanMapElement(MapElement &mE,int index);


private:

float getLineAngle(CvPoint start, CvPoint end);
float distance(CvPoint start, CvPoint end);
float distance(float startx,float starty, float endx, float endy);
int checkDouble(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float sizeTol, float angleTol, float distanceTol);
int parallel(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float angleTol);
int lineMatching(CvPoint start1, CvPoint end1, CvPoint start2, CvPoint end2, float distanceTol);

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
   
  // Operating mode
  enum {modeLive, modeReplay, modeBlob} mode;
  
  // Source sensor id
  sensnet_id_t sensorId;

  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet log replay module
  sensnet_replay_t *replay;

  // Console display
  cotk_t *console;

  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //should we show the detected line?
  bool show;

  //skip thourgh log?
  bool skip;

  //goto option given?
  int gotoFrame;

  //should we step through shown images by pressing a key or fall through
  bool step;

  //should we export every frame to an image
  bool save;

  //should we export debug images (detected and tracked lines)
  bool save_debug;

  //should we show debug images
  bool debug;

  // Current blob id
  int blobId;
  
  // Current (incoming) stereo blob
  StereoImageBlob stereoBlob;

    // Current (outgoing) line message
    RoadLineMsg lineMsg;
    RoadLineMsg laneMsg;

  // Diagnostics on stop lines
  int totalStops, totalFakes;
    
  //line score
  float lineScore;


    //tracking lines
    //
    //track or not
    bool trackStopLines;
    //if there's a line currently being tracked
    bool stopLineTrackCreated;
    //number of frames with measurements before starting the track
    int numPreFrames;
    //number of frames without measurements before destroying the track
    int numPostFrames;
    //kalman structure
    CvKalman* kalman;
    //initialize track
    void initStopLineTrack();
    //destroy track
    void destroyStopLineTrack();
    //track stop lines
    void updateStopLineTrack();
    //tracking model
    int stopLineTrackingModel;

    //detections
    bool noStoplines;
    bool noLanes;

    //map element talker to send to map
    CMapElementTalker mapElementTalker; 
    //map element to send the stop line
    MapElement mapElement;

private:
    
  CvMat *imBuffer,*imBuffer1, *imBuffer2, *imOriginal, *imProcess, *imCOriginal, *imCProcess;
  CvMat *imCO0, *imCO1, *imCO2, *imCO3, *imC;

};



#endif /*LINE_PERCEPTOR_HH*/
