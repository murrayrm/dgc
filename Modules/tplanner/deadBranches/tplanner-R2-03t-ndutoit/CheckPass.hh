/*! CheckPass.hh
 * Christian Looman
 * May 09 2007
 */

#ifndef CHECKPASS_HH
#define CHECKPASS_HH
#define AHEAD 0
#define BEHIND 1

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
#include <cv.h>
#include <highgui.h>
#include <time.h>
//open cv stuff

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "state/AliceStateHelper.hh"
#include "ControlState.hh"
#include "ControlStateFactory.hh"
#include "cmdline.h"


// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

class ListToA
{
public:
  ListToA() {}
  ~ListToA() {}

  PointLabel WayPoint;
  MapId Id;
  double velocity;
  time_t time;
  double distance;
  double eta;
  bool updated;
};

class CheckPass
{ 
public:
  // Functions for passing static obstacle with dyn. oncoming traffic
  static double getProbability(VehicleState vehState,Map* localMap);
  static void checkForObstacles(VehicleState vehState, Map* localMap, LaneLabel lane,int searchDirection, double& distance_obstacle,double& vel_obstacle, double& size_obstacle);

  // Functions for dyn. obstacles at intersections
  static void resetIntersection(Map* localMap);
  static bool checkIntersection(VehicleState vehState,Map* localMap,ControlState * cstate);
  static void createIntersection(VehicleState vehState,Map* localMap);
  static void populateWayPoints(Map* localMap, vector<PointLabel>& WayPointEntries, PointLabel InitialWayPointEntry);
  static void findStoplines(Map* localMap);
  static void checkExistenceObstacles(Map* localMap,ControlState * cstate);
  static bool checkClearance(Map* localMap);
  static bool checkPrecedence();

  static bool init;

private:
  // Variables for checking the intersection
  static vector<ListToA> VehicleList;
  static bool ClearToGo,LegalToGo,IntersectionCreated;
  static vector<PointLabel> WayPoint_WithStop,WayPoint_NoStop,WayPointEntries;
  static PointLabel StopLine_alice,StopLine_reverse;
  static double distance_stopline_alice,distance_reverse;

  static const double T_CREATE_PREC_LIST=10.0;
  static const double CREATE_PREC_LIST=10.0;
  static const double DISTANCE_STOPLINE_OBSTACLE=0.5;
  static const double DISTANCE_STOPLINE_ALICE_OFFSET=0.5;
  static const double VELOCITY_OBSTACLE_THRESHOLD=0.2;
  static const double DISTANCE_OBSTACLE_THRESHOLD=6;
  static const double VELOCITY_ALICE_EPS=0.1;
  static const double ETA_EPS=0.5;
  static const double DISTANCE_STOPLINE_OBSTACLE_EPS=0.5;

  static CMapElementTalker testMap;

  static const int sendChannel=-5;
  static const int skynetKey=1302;
  static MapElement alice;
};

#endif  // CHECKPASS_HH

