#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "TrafficPlanner.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "iostream"
       
using namespace std;             


int main(int argc, char **argv)              
{
  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
   
  // Populate cmdLineArgs struct
  CmdLineArgs cmdLineArgs;
  cmdLineArgs.sn_key = skynet_findkey(argc, argv);  
  cmdLineArgs.debugLevel = cmdline.debug_arg;
  cmdLineArgs.verboseLevel = cmdline.verbose_arg; 
  cmdLineArgs.no_console = cmdline.disable_console_flag;
  cmdLineArgs.waitForStateFill = !cmdline.nowait_flag;
  cmdLineArgs.log = cmdline.log_flag;
  cmdLineArgs.send_costmap = cmdline.send_costmap_flag;
  cmdLineArgs.use_local = cmdline.use_local_flag;
  cmdLineArgs.rndf_given = cmdline.rndf_given;
  cmdLineArgs.debug = cmdline.debug_given;
  cmdLineArgs.verbose = cmdline.verbose_given;

  if (cmdLineArgs.no_console){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << cmdLineArgs.sn_key << endl;
    cout << "Debug level = " << cmdLineArgs.debugLevel << endl;
    cout << "Verbose level = " << cmdLineArgs.verboseLevel << endl;
  }

  // Initialize Traffic Planner Class
  CTrafficPlanner* pTrafficPlanner = new CTrafficPlanner(cmdLineArgs);

  // Initialize the map with rndf if given
  if (cmdLineArgs.rndf_given){
    string RNDFfilename = cmdline.rndf_arg;
    cout << "RNDF Filename in = "  << RNDFfilename << endl;
    if (!pTrafficPlanner->loadRNDF(RNDFfilename)) {
      cerr << "in TrafficPlannerMain: error loading rndf file" << endl;
      return 0; 
    }  
		 
  }     

  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getLocalMapThread);
  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getDPlannerStatusThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getSegGoalsThread); 
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getStaticCostMapRequestThread);
      
        
  sleep(1);
  pTrafficPlanner->TPlanningLoop();
  return 0;
}
     
 

 
