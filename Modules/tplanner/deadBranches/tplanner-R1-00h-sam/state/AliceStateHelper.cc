#include "AliceStateHelper.hh"
#include <math.h>


point2 AliceStateHelper::getPositionFrontAxle(VehicleState vehState)
{
  double length = DIST_REAR_AXLE_TO_FRONT - DIST_FRONT_AXLE_TO_FRONT;
  double easting = length*sin(vehState.localYaw);
  double northing = length*cos(vehState.localYaw);

  return point2 (vehState.localX + northing, vehState.localY + easting);
}

point2 AliceStateHelper::getPositionFrontBumper(VehicleState vehState)
{
  double length = DIST_REAR_AXLE_TO_FRONT;
  //cout << "current alice yaw = " << vehState.localYaw << endl;
  
  double easting = length*sin(vehState.localYaw);
  double northing = length*cos(vehState.localYaw);

  return point2 (vehState.localX + northing, vehState.localY + easting);
}

point2 AliceStateHelper::getPositionRearAxle(VehicleState vehState)
{
  return point2 (vehState.localX, vehState.localY);
}

point2 AliceStateHelper::getPositionRearBumper(VehicleState vehState)
{

  double length = DIST_REAR_TO_REAR_AXLE;
  double easting = length*sin(vehState.localYaw);
  double northing = length*cos(vehState.localYaw);

  return point2 (vehState.localX - northing, vehState.localY + easting);

}


double AliceStateHelper::getVelocityMag(VehicleState vehState)
{
  return sqrt(pow(vehState.utmNorthVel,2) + pow(vehState.utmEastVel, 2));
}

point2 AliceStateHelper::convertToGlobal(point2 gloToLocalDelta, point2 localPoint)
{

		return point2(gloToLocalDelta + localPoint);

}
