#include "StoppedToPassing_LC.hh"

StoppedToPassing_LC::StoppedToPassing_LC(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

StoppedToPassing_LC::StoppedToPassing_LC()
{

}

StoppedToPassing_LC::~StoppedToPassing_LC()
{

}

double StoppedToPassing_LC::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToPassing_LC::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      {
        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel exitWayptLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        point2 exit;
        if (map->getWaypoint(exit, exitWayptLabel) != 0) {
            m_prob = 0;
            break;
        }
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        double distance = currFrontPos.dist(exit);
        // bool dist_ok = distance > delta_p;
        bool dist_ok = true;

        LaneLabel currLane;
	int laneErr = map->getLane(currLane, currFrontPos);
	if (laneErr == -1)
	  currLane = LaneLabel(seg.entrySegmentID, seg.entryLaneID);

        string lt, rt;

        map->getLeftBoundType(lt, currLane);
        map->getRightBoundType(rt, currLane);

	LaneLabel lane_left, lane_right;
	map->getNeighborLane(lane_left, currLane, -1);
	map->getNeighborLane(lane_right, currLane, 1);

	bool sameDirLeft = false;
	bool sameDirRight = false;
	vector<LaneLabel> sameDirLanes;
	int dirErr = map->getSameDirLanes(sameDirLanes, currLane);
	for (unsigned int i=0; i<sameDirLanes.size(); i++) {
	  if (sameDirLanes[i] == lane_left)
	    sameDirLeft = true;
	  if (sameDirLanes[i] == lane_right)
	    sameDirRight = true;
	}

        LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

	//	LaneLabel lane_left = TrafficUtils::getAdjacentLane(map, currFrontPos,false);
	//	LaneLabel lane_right = TrafficUtils::getAdjacentLane(map,currFrontPos, true);

	bool passing_allowed = false;

	if (lane_left.lane>0 && (lt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
          laneChange->setLaneChangeID(lane_left.lane);
	  laneChange->setReverse(sameDirLeft?0:1);
          passing_allowed = true;
	  cout << "Can change lanes left to lane " << lane_left << endl;
	  cout << "Same direction left: " << sameDirLeft << endl;
	}

        if (lane_right.lane>0 && (rt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
          laneChange->setLaneChangeID(lane_right.lane);
	  laneChange->setReverse(sameDirRight?0:1);
          passing_allowed = true;
	  cout << "Can change lanes right to lane " << lane_right << endl;
	  cout << "Same direction right: " << sameDirRight << endl;
	}

        if (passing_allowed) {
            m_prob = 1;
	    if (m_verbose || m_debug)
	      cout << "Transitioning to Passing1..." << endl;
        }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "StoppedToPassing_LC.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
