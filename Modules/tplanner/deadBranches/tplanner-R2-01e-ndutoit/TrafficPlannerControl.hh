#ifndef TRAFFICPLANNERCONTROL_HH_
#define TRAFFICPLANNERCONTROL_HH_

#include "Corridor.hh"
#include "Conflict.hh"
#include "mapping/Segment.hh"
#include "TrafficStateEstimator.hh"
#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "ControlStateFactory.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"
#include "interfaces/VehicleState.h"

class TrafficPlannerControl {


public: 


  TrafficPlannerControl();
  TrafficPlannerControl(bool debug, bool verbose, bool log);
  
  ~TrafficPlannerControl();
  
	// int controlLoop(Corridor& corr,list<SegGoals> currSegGoals, Map* localMap, VehicleState vehState);

  /**   Given the traffic state passed, give the most likely control state to maintain. */
  ControlState* determineControlState(ControlState *currControlState, TrafficState *currTrafficState,PlanningHorizon currPlanHorizon, Map* localMap, VehicleState currVehState);

  /** 
   *  Given the corridor passed, determine the conflicts that may arise with respect to moving obstacles.  
   */
  Conflict evaluateDynamicConflicts(Corridor m_currCorridor, ControlState *controlState);
  

  void determinePlanningHorizon(PlanningHorizon &currPlanHorizon, TrafficState *currTrafficState, list<SegGoals> currSegGoals) ;
  /** Choose best corridor */
  void evaluateCorridors();

  //ControlState getCurrentControlState();
  //ControlStateFactory::ControlStateType getCurrentControlStateType();
  ControlState* chooseMostProbableControlState(ControlState* controlState,std::vector<StateTransition*> transitions);   

  
private : 
  bool m_verbose;
  bool m_debug;
  bool m_log;

  /** State databases */ 
  StateGraph m_controlGraph;
 
  std::vector<Conflict> m_conflicts;
  TrafficStateEstimator* m_estimator;
};

#endif /*TRAFFICPLANNERCONTROL_HH_*/
