#include "CorridorUtils.hh"
#include <cmath>
#include <fstream>

#define DISTANCE_TO_OBSTACLE 10

int CorridorUtils::makeCorridorLane(Corridor & corr, point2 currFrontPos, Map * localMap, bool isReverse)
{
  int error = 0;
  point2arr leftBound, rightBound;
  double totRange = 50;
  double backRange = 5;
  int getBoundsErr = 0;
  LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();

  if (isReverse)
    getBoundsErr = localMap->getBoundsReverse(leftBound, rightBound, desiredLane, currFrontPos, totRange);
  else {
    getBoundsErr = localMap->getBounds(leftBound, rightBound, desiredLane, currFrontPos, totRange, backRange);
  }

  if (getBoundsErr < 0) {
    cerr << "ERROR: LaneKeeping.cc: boundary read from map error" << endl;
    error+=1;
  }
  
  if (leftBound.size() != rightBound.size()){
    cerr << "WARNING: LaneKeeping.cc: leftBound.size != rightBound.size, trying to fix this" << endl;
	//Align them boundaries
    TrafficUtils::alignBoundaries(leftBound, rightBound);
    }

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);
  
  return error;
}


int CorridorUtils::makeCorridorIntersection(Corridor & corr, point2 currFrontPos, Map* localMap, SegGoals currSegment)
{
  int error = 0;
  point2arr leftBound, rightBound;
  // Set of boundary points due to intersection lane
  PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
  
  double totRange = 80;
  double backRange = 40;
  int interBoundErr = localMap->getTransitionBounds(leftBound, rightBound, ptLabelIn, ptLabelOut, currFrontPos, totRange, backRange);
  if (interBoundErr != 0) {
    cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
    error+=1;
  }

  if (leftBound.size() != rightBound.size()){
    cerr << "WARNING: LaneKeeping.cc: leftBound.size != rightBound.size, trying to fix this" << endl;
    //Align them boundaries
    TrafficUtils::alignBoundaries(leftBound, rightBound);
  }

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);
  
  return error;

}

int CorridorUtils::makeCorridorZonePause(Corridor & corr, VehicleState vehState)
{
  point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  double corrHalfWidth = 3;
  double theta = AliceStateHelper::getHeading(vehState);
  point2arr leftBound, rightBound;
  point2 temppt, exitWaypt;
  
  /* Calculate the shortest distance to come to a stop*/
  double velIn = AliceStateHelper::getVelocityMag(vehState);
  double distToStop = -pow(velIn,2)/(-2*VEHICLE_MAX_DECEL); 
  
  exitWaypt.x = cos(theta)*2*distToStop + currFrontPos.x; 
  exitWaypt.y = sin(theta)*2*distToStop + currFrontPos.y; 
  
  // Left Boundary
  // pt 1 on left boundary
  temppt.x = currRearPos.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = currRearPos.y + corrHalfWidth * sin(theta + M_PI / 2);
  leftBound.push_back(temppt);
  
  // pt 2 on left boundary
  temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
  leftBound.push_back(temppt);
  
  // Right Boundary
  // pt 1 on right boundary
  temppt.x = currRearPos.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = currRearPos.y + corrHalfWidth * sin(theta - M_PI / 2);
  rightBound.push_back(temppt);
  
  // pt 2 on right boundary
  temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
  rightBound.push_back(temppt);
  
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);
  
  return 0;


}

int CorridorUtils::makeCorridorZone(Corridor & corr, point2 currRearPos, Map* localMap, SegGoals currSegment, SegGoals nextSegment)
{
  int error=0;
  point2arr leftBound, leftBound1, rightBound, rightBound1;
  point2arr tmpLB, tmpRB;
  point2 exitWaypt;

  PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
  int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
  if (wayptErr != 0) {
    cerr << "LaneKeeping.cc: waypt read from map error" << endl;
    error+=1;
  }

  // Create a corridor of LaneWidth from current position to entry point
  double corrHalfWidth = 3;
  double theta = atan2(exitWaypt.y - currRearPos.y, exitWaypt.x - currRearPos.x);  
  // Add the lane boundaries for the segment beyond the zone region
  double totRange=20, backRange=0;
  LaneLabel nextSegmentLane(nextSegment.entrySegmentID, nextSegment.entryLaneID);
  int getBoundsErr = localMap->getBounds(tmpLB, tmpRB, nextSegmentLane, exitWaypt, totRange, backRange);
  int index = TrafficUtils::getClosestPtIndex(tmpLB, exitWaypt);
  for (int ii = index; ii < (int) tmpLB.size(); ii++) {
    leftBound1.push_back(tmpLB[ii]);
  }
  index = TrafficUtils::getClosestPtIndex(tmpRB, exitWaypt);
  for (int ii = index; ii < (int) tmpRB.size(); ii++) {
    rightBound1.push_back(tmpRB[ii]);
  }
  // add pt at current location to drive to where we want to go
  point2 temppt;
  temppt.x = currRearPos.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = currRearPos.y + corrHalfWidth * sin(theta + M_PI / 2);
  rightBound.push_back(temppt);
  temppt.x = currRearPos.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = currRearPos.y + corrHalfWidth * sin(theta - M_PI / 2);
  leftBound.push_back(temppt);
  leftBound.arr.insert(leftBound.arr.end(), leftBound1.arr.begin(), leftBound1.arr.end());
  rightBound.arr.insert(rightBound.arr.end(), rightBound1.arr.begin(), rightBound1.arr.end());

  if (getBoundsErr < 0) {
    cerr << "ERROR: LaneKeeping.cc: boundary read from map error" << endl;
    error+=1;
  }
  
  if (leftBound.size() != rightBound.size()){
    cerr << "WARNING: LaneKeeping.cc: leftBound.size != rightBound.size, trying to fix this" << endl;
	//Align them boundaries
    TrafficUtils::alignBoundaries(leftBound1, rightBound1);
  }

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);
  
  return error;

}


int CorridorUtils::makeCorridorLaneChange(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, LaneLabel destLane, point2 startPos, int reverse)
{
  int error =0;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  cout << "Making LaneChange corridor " << endl;
  cout << "Reverse value: " << reverse << endl;
    
  point2arr leftBoundCur, rightBoundCur, leftBoundDest, rightBoundDest;
  SegGoals currSegment = planHoriz.getSegGoal(0);

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);

  //get current lane label
  LaneLabel currLane;
  int laneErr= localMap->getLane(currLane, currRearPos);
  error += laneErr;

  if (laneErr == -1) //map read error
    currLane = destLane;
  if (currLane == destLane) {
    //return makeCorridorLane(corr, vehState, traffState, planHoriz, localMap, 50,(reverse == 1), destLane);
    // NOEL: changed this function, but should return the same
    error += makeCorridorLane(corr, currFrontPos, localMap, (reverse==1));
    return error;
  }
  double laneChangeTime = 6; //seconds
  double laneChangeLength = currSegment.maxSpeedLimit * laneChangeTime;

  //is there an obstacle?
  // SVEN: double obs = localMap->getObstacleDist(startPos, 0);
  double obs = TrafficUtils::getNearestObsDist(localMap, vehState, currLane);
  if (obs > 0) {
    cout << "Adjusting lane change for obstacle in " << obs << " meters" << endl;
    if (laneChangeLength > (obs - 10))
      laneChangeLength = (obs-10);
    if (laneChangeLength < 8)
      laneChangeLength = 8;
  }

  cout << "laneChangeLength = " << laneChangeLength << endl;

  int boundErr = 0;
  int boundErr1 = 0;

  //current lane boundary
  if (reverse == -1)
    boundErr = localMap->getBoundsReverse(leftBoundCur,rightBoundCur,currLane,startPos,laneChangeLength+30);
  else
    boundErr = localMap->getBounds(leftBoundCur,rightBoundCur,currLane,startPos,laneChangeLength+30);

  error+=boundErr;  
  if (boundErr!=0){
    cerr << "ERROR: CorridorUtils.cc: current lane read from map error" << endl;
    return error;
  }     

  //lane change boundary
  if (reverse == 1)
    boundErr1 = localMap->getBoundsReverse(leftBoundDest,rightBoundDest,destLane,startPos,laneChangeLength+30);
  else
    boundErr1 = localMap->getBounds(leftBoundDest,rightBoundDest,destLane,startPos,laneChangeLength+30);

  error+=boundErr1;  
  if (boundErr1!=0){
    cerr << "ERROR: CorridorUtils.cc: destination lane read from map error" << endl;
    return error;
  }

  point2arr nearBound(rightBoundCur);
  point2arr midBound(leftBoundCur);
  point2arr farBound1(leftBoundDest);
  bool leftChange = true;

  //are we changing lanes to the right?
  if (leftBoundDest[0].dist(rightBoundCur[0]) < rightBoundDest[0].dist(leftBoundCur[0]))
  {
    nearBound = leftBoundCur;
    midBound = rightBoundCur;
    farBound1 = rightBoundDest;
    leftChange = false;
  }

  point2arr farBound = farBound1;

  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;

  //  for (int i=0;i<midBound.size();i++)
  //  farBound.push_back(farBound1.project(midBound[i]));
  TrafficUtils::alignBoundaries(midBound,farBound);

  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;

  TrafficUtils::alignBoundaries(midBound,farBound);
  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;

  //find start of lane change
  int indNear = TrafficUtils::insertProjPtInBoundary(nearBound, startPos);
  indNear = TrafficUtils::getClosestPtIndex(nearBound, startPos);
  int indMid = TrafficUtils::insertProjPtInBoundary(midBound, startPos);
  indMid = TrafficUtils::getClosestPtIndex(midBound, startPos);
  int indFar = TrafficUtils::insertProjPtInBoundary(farBound, midBound[indMid]);
  indFar = TrafficUtils::getClosestPtIndex(farBound, startPos);

  cout << "ind(Near,Mid,Far): " << indNear << " " << indMid << " " << indFar << endl;


  //add points at important distance, to give lane change maneuver some width
  double insertDist = 5;

  TrafficUtils::insertPtAtDistance(nearBound,indNear,laneChangeLength-0.1);
  TrafficUtils::insertPtAtDistance(midBound,indMid,laneChangeLength-0.1);
  TrafficUtils::insertPtAtDistance(farBound,indFar,laneChangeLength-0.1);

  TrafficUtils::insertPtAtDistance(nearBound,indNear,insertDist);
  TrafficUtils::insertPtAtDistance(midBound,indMid,insertDist);
  TrafficUtils::insertPtAtDistance(farBound,indFar,insertDist);

  TrafficUtils::insertPtAtDistance(nearBound,indNear,laneChangeLength+insertDist);
  TrafficUtils::insertPtAtDistance(midBound,indMid,laneChangeLength+insertDist);
  TrafficUtils::insertPtAtDistance(farBound,indFar,laneChangeLength+insertDist);

  //we hope that indNear == indMid

  //the cumulative distance from the starting position
  double lcDist = 0;//startPos.dist(nearBound[indNear]);

  srand(time(0));
  //double foo = (double)(rand() & 1);

  //weighted average transition between the two lanes
  //iterate from lane change start to current location
  for (int i=indMid; i<midBound.size()-1; i++)
  {
    double alpha = lcDist / laneChangeLength;
    double alpha2 = (lcDist - insertDist) / laneChangeLength;
    lcDist += midBound[i].dist(midBound[i+1]);

    //    alpha = foo;
    //    alpha2 = foo;

    if (alpha2 < 0) alpha2 = 0;
    if (alpha > 1) alpha = 1;
    if (alpha2 > 1) alpha2 = 1;

    nearBound[i] = (alpha2*midBound[i] + (1-alpha2)*nearBound[i]);
    midBound[i] = (alpha*farBound[i] + (1-alpha)*midBound[i]);
  }

  nearBound.back() = midBound.back();
  midBound.back() = farBound.back();

  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;
  //  cout << "Near Back: " << nearBound.back() << ", Mid Back: " << midBound.back() << endl;

  if (leftChange) {
    corr.addPolyline(midBound);
    corr.addPolyline(nearBound);
  }
  else {
    corr.addPolyline(nearBound);
    corr.addPolyline(midBound);
  }


  FC_finalPos = 0.5*(nearBound.back() + midBound.back());
  double heading;
  localMap->getHeading(heading,FC_finalPos);

  if (reverse == 1) {
    heading += M_PI;
    if (heading > (2*M_PI))
      heading -= (2*M_PI);
  }

  // Specify the ocpParams final conditions - lower bounds
  // Do we want to define this position based on the corridor?
  FC_velMin = 0;
  FC_headingMin = heading;
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = currSegment.maxSpeedLimit;
  FC_headingMax = heading;
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
  
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  return error;
}



int CorridorUtils::setFinalCondLK(Corridor &corr, point2 currFrontPos, Map* localMap, SegGoals segmentGoal, bool isReverse)
{
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;
  double distanceToExitPt, distanceToCorrExitPt, heading;
  int error = 0;
  point2 exitPt, corrExitPt;
  point2arr centerline;
  vector<point2arr> polylines = corr.getPolylines();

  PointLabel exitPtLabel(segmentGoal.exitSegmentID, segmentGoal.exitLaneID, segmentGoal.exitWaypointID);
  localMap->getWaypoint(exitPt, exitPtLabel);
  
  corrExitPt.set((polylines[0].back().x+polylines[1].back().x)/2,(polylines[0].back().y+polylines[1].back().y)/2);
  LaneLabel laneLabel(exitPtLabel.segment, exitPtLabel.lane);
  localMap->getLaneCenterLine(centerline, laneLabel);
  localMap->getDistAlongLine(distanceToExitPt, centerline, exitPt, currFrontPos);
  localMap->getDistAlongLine(distanceToCorrExitPt, centerline, corrExitPt, currFrontPos);
  
  cout << "distToExit = " << distanceToExitPt << " and distToCorrExit = " << distanceToCorrExitPt << endl;
  if (!isReverse) {
    if (segmentGoal.stopAtExit) {
      if (distanceToExitPt<0) {
        cout << "ERROR: Dist to Exit Pt < 0, so something went wrong. Taking exit as corrExitPt" << endl;
        FC_finalPos.set(corrExitPt);
        localMap->getHeading(heading, FC_finalPos);
        error += 1;
      } else if (distanceToCorrExitPt<0) {
        cout << "ERROR: Dist to Corr Exit Pt < 0, so something went wrong. Taking exit as exitPt" << endl;
        FC_finalPos.set(exitPt);
        localMap->getHeading(heading, exitPtLabel);
        error += 1;
      } else {
        if (distanceToExitPt<=distanceToCorrExitPt){
          cout << "using exit pt for FC_pos"  << endl;
          FC_finalPos.set(exitPt);
          localMap->getHeading(heading, exitPtLabel);
        } else {
          FC_finalPos.set(corrExitPt);
          cout << "using last pt in range for FC_pos"  << endl;
          localMap->getHeading(heading, FC_finalPos);
        }
      }
    } else {
      FC_finalPos.set(corrExitPt);
      cout << "using last pt in range for FC_pos"  << endl;
      localMap->getHeading(heading, FC_finalPos);
    }
  } else {
    FC_finalPos.set(corrExitPt);
    cout << "using last pt in range for FC_pos"  << endl;
    localMap->getHeading(heading, FC_finalPos);
  }
  
  FC_velMin = 0;
  
  if (isReverse)
    heading += M_PI;
  
  FC_headingMin = heading;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = segmentGoal.maxSpeedLimit;
  FC_headingMax = heading;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
 
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  return error;

}

int CorridorUtils::setFinalCondStop(Corridor & corr, Map* localMap, SegGoals segmentGoal)
{
  int error=0;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;
  
  point2 exitPt;
  double heading;
  PointLabel exitPtLabel(segmentGoal.exitSegmentID, segmentGoal.exitLaneID, segmentGoal.exitWaypointID);

  localMap->getWaypoint(exitPt, exitPtLabel);
  localMap->getHeading(heading, exitPtLabel);
  
  // Move back point so that we plan for the rear axle
  double easting = -DIST_REAR_AXLE_TO_FRONT*sin(heading);
  double northing = -DIST_REAR_AXLE_TO_FRONT*cos(heading);
  FC_finalPos.x = exitPt.x+northing;
  FC_finalPos.y = exitPt.y+easting;

  FC_velMin = 0;
  FC_headingMin = heading;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  FC_velMax = 0;
  FC_headingMax = heading;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
 
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  return error;


}


int CorridorUtils::setFinalCondStopped(Corridor & corr, VehicleState vehState)
{
  int error=0;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;

  point2 FC_finalPos = AliceStateHelper::getPositionRearAxle(vehState);
  double heading = AliceStateHelper::getHeading(vehState);

  FC_velMin = 0;
  FC_headingMin = heading;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  FC_velMax = 0;
  FC_headingMax = heading;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
 
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  return error;

}


int CorridorUtils::setFinalCondPause(Corridor & corr, VehicleState vehState, Map* localMap)
{
  int error=0;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;
  point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  double heading, velIn;

  /* Calculate the shortest distance to come to a stop*/
  velIn = AliceStateHelper::getVelocityMag(vehState);
  cout << "Pause: curr velocity = " << velIn << endl;
  if (velIn>0.2) {
    double distToStop = -pow(velIn,2)/(-2*VEHICLE_MAX_DECEL); 
    cout << "Pause: distToStop = " << distToStop << endl;
    LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
    localMap->getLaneCenterPoint(FC_finalPos, desiredLane, currFrontPos, distToStop);
    localMap->getHeading(heading, FC_finalPos);
  } else {
    cout << "Pause: vel < 0.2 so final pos = curr pos to invoke stopped condition" << endl;
    FC_finalPos = currRearPos;
    heading = AliceStateHelper::getHeading(vehState);
  }

  FC_velMin = 0;
  FC_headingMin = heading;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  FC_velMax = 0;
  FC_headingMax = heading;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
 
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  return error;

}


int CorridorUtils::setFinalCondLaneChange(Corridor &corr, point2 currFrontPos, Map* localMap, SegGoals segmentGoal, bool isReverse)
{
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;
  double heading;
  int error = 0;
  point2 corrExitPt;
  vector<point2arr> polylines = corr.getPolylines();

  corrExitPt.set((polylines[0].back().x+polylines[1].back().x)/2,(polylines[0].back().y+polylines[1].back().y)/2);
  FC_finalPos.set(corrExitPt);
  localMap->getHeading(heading, FC_finalPos);
  
  FC_velMin = 0;
  if (isReverse)
    heading += M_PI;
  FC_headingMin = heading;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = segmentGoal.maxSpeedLimit;
  FC_headingMax = heading;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
 
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  return error;

}

int CorridorUtils::setFinalCondObstacles(Corridor& corr, VehicleState vehState, Map* localMap, double separationDist)
{
  int error = 0;
  double heading;
  point2 exitPt;
  MapElement obstacle;
  point2 FCLB, FCUB, FC_pos;
  LaneLabel currLaneLabel;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
  LaneLabel desiredLaneLabel = AliceStateHelper::getDesiredLaneLabel();
  error= localMap->getLane(currLaneLabel, currFrontPos);
  double aliceLength = DIST_REAR_AXLE_TO_FRONT;
  double distToObs = TrafficUtils::getNearestObsInLane(obstacle, localMap, vehState, desiredLaneLabel);
  double currVel = AliceStateHelper::getVelocityMag(vehState);
  MapElementType obstacleType = TrafficUtils::getObsType(obstacle);
  corr.getOCPfinalPos(FCLB, FCUB);
  FC_pos = (FCLB+FCUB)/2;

  cout << "adjusting final position for obstacles with separationDist = " << separationDist << " and distance to obstacles = " << distToObs << endl;
  if ((distToObs<40)&&(distToObs>0)) {
    if (ELEMENT_OBSTACLE==obstacleType) {
      if (TrafficUtils::isLaneBlocked(localMap, vehState, FC_pos, desiredLaneLabel)) {
        cout << "Lane Blocked" << endl;
        if (distToObs > separationDist) { 
          cout << "distToObs>separationDist: enough time/space to stop" << endl;
          TrafficUtils::getNearestObsPoint(exitPt, localMap, vehState, desiredLaneLabel, separationDist+aliceLength);
          localMap->getHeading(heading, exitPt);
        } else if (distToObs<=separationDist) {
          cout << "WARNING: distToObs<=separationDist: TOO CLOSE Stopping NOW "<< endl;
          /* Calculate the shortest distance to come to a stop*/
          if (currVel>0.2) {
            double distToStop = -pow(currVel,2)/(-2*VEHICLE_MAX_DECEL);
            cout << "vel > 0.2, Stopping in " << distToStop << endl;
            heading = AliceStateHelper::getHeading(vehState);
            exitPt.x = currRearPos.x + distToStop*cos(heading);
            exitPt.y = currRearPos.y + distToStop*sin(heading);
          } else {
            cout << "vel <= 0.2, Stopping in 0" << endl;
            exitPt = currRearPos;
            heading = AliceStateHelper::getHeading(vehState);
          }
        }
      } else {
        cout << "Partial lane block - do nothing" << endl;
        return error;
      }
    } else if (ELEMENT_VEHICLE == obstacleType) {
      // if velocity of vehicle is less than some threshold, then just come to a stop
      double obsVel = TrafficUtils::getObstacleVelocityMag(obstacle);
      if ((obsVel<0.2)&&(distToObs<separationDist+2)) {
        cout << "vehicleVel < 0.2 and distToObs<separationDist+2 so come to a stop" << endl;
        if (distToObs>separationDist) {
          cout << "distToObs>separationDist: enough time/space to stop in " << separationDist << endl;
          TrafficUtils::getNearestObsPoint(exitPt, localMap, vehState, desiredLaneLabel, separationDist+aliceLength);
          localMap->getHeading(heading, exitPt);
        } else if (distToObs>0) {
          cout << "distToObs<=separationDist: TOO CLOSE: Stopping NOW" << endl;
          /* Calculate the shortest distance to come to a stop*/
          if (currVel>0.2) {
            double distToStop = -pow(currVel,2)/(-2*VEHICLE_MAX_DECEL);
            cout << "vel > 0.2, Stopping in " << distToStop << endl;
            heading = AliceStateHelper::getHeading(vehState);
            exitPt.x = currRearPos.x + distToStop*cos(heading);
            exitPt.y = currRearPos.y + distToStop*sin(heading);
          } else {
            cout << "vel <= 0.2, Stopping in 0" << endl;
            exitPt = currRearPos;
            heading = AliceStateHelper::getHeading(vehState);
          }
        }
      }
    } else {
      cerr<<"WARNING: Corridor.adjustCorrForObs(): did not recognize the obstacle type." << endl;
      error+=1;
    }
    
    corr.setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
    corr.setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
    corr.setOCPfinalCondLB(HEADING_IDX_C, heading);
    corr.setOCPfinalCondUB(HEADING_IDX_C, heading);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, 0);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, 0);
  }  
  return error;
}

int CorridorUtils::adjustVelocityForObstacles(Corridor& corr, Map* localMap, VehicleState vehState, SegGoals currSegment)
{
  int error = 0;
  double velMin, velMax;
  MapElement obstacle;
  LaneLabel currLaneLabel;
  point2 FCLB, FCUB, FC_pos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
  LaneLabel desiredLaneLabel = AliceStateHelper::getDesiredLaneLabel();
  error= localMap->getLane(currLaneLabel, currFrontPos);

  //  if (currLaneLabel != desiredLaneLabel){
  //   cout << "WARNING: in CorridorUtils:setFinalCondObstacles(): current lane != desired lane for some reason. Going to use desired lane." << endl;
  // }

  double distToObs = TrafficUtils::getNearestObsInLane(obstacle, localMap, vehState, desiredLaneLabel);
  double separationDist = corr.getSeparationDist();
  MapElementType obstacleType = TrafficUtils::getObsType(obstacle);
  corr.getOCPfinalPos(FCLB, FCUB);
  FC_pos = (FCLB+FCUB)/2;


  cout << "Adjusting velocity to deal with obstacles" << endl;
  if ((distToObs<40)&&(distToObs>0)) {
    if (ELEMENT_OBSTACLE==obstacleType) {
      if (TrafficUtils::isLaneBlocked(localMap, vehState, FC_pos, desiredLaneLabel)) 
        {
          cout << "Static: Lane block: do nothing" << endl;
          velMin = 0;
          velMax = currSegment.maxSpeedLimit;
        } else {
          // just want to adjust the max velocity to drive at
          cout << "Static: Partial block: slowing down" << endl;
          velMin = 0;
          velMax = 2;
        }
    } else if (ELEMENT_VEHICLE == obstacleType) {
      // if velocity of vehicle is less than some threshold, then just come to a stop
      double obstVel = TrafficUtils::getObstacleVelocityMag(obstacle);
      if (distToObs>separationDist+10) {
        cout << "Dynamic: distToObs>separationDist+10 so do nothing" << endl;
        velMin = 0;
        velMax = currSegment.maxSpeedLimit;
      } else if (distToObs>separationDist+5) {
        cout << "Dynamic: separationDist+5<distToObs<separationDist+10 so matching speeds" << endl;
        velMin = 0;
        velMax = TrafficUtils::getObstacleVelocityMag(obstacle); // match the velocity
      } else if (distToObs>separationDist) {
        cout << "Dynamic: separationDist<distToObs<separationDist+5 so driving at 80% of obstacle's speed"<< endl;
        velMin = 0;
        velMax = 0.8*TrafficUtils::getObstacleVelocityMag(obstacle);
      } else {
        cout << "Dynamic: distToObs<separationDist TOO CLOSE, slowing down" << endl;
        velMin = 0;
        velMax = 0.6*TrafficUtils::getObstacleVelocityMag(obstacle);
      }
    } else {
      cerr<<"WARNING: CorridorUtils.adjustCorrForObs(): did not recognize the obstacle type." << endl;
      error+=1;
    }
    corr.setVelocityLimits(velMin, velMax);
  }
  return error;
}


int CorridorUtils::setVelForCorridor(Corridor& corr, TrafficState *currTrafState, ControlState *currControlState, PlanningHorizon planHoriz)
{
  int error = 0;
  SegGoals currSegGoal = planHoriz.getSegGoal(0);
  double velMin, velMax;

  switch (currControlState->getType()) 
    {
    case ControlStateFactory::LANE_KEEPING:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          velMin = 0;
          velMax = 2;
        } else {
          velMin = currSegGoal.maxSpeedLimit;
          velMax = currSegGoal.maxSpeedLimit;
        }
      }
      break;

    case ControlStateFactory::LANE_CHANGE:
      {
        velMin = 0;
        velMax = 2;
      }
      break;
      
    case ControlStateFactory::STOP:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          velMin = 0;
          velMax = 2;
        } else {
          velMin = 0;
          velMax = currSegGoal.maxSpeedLimit;
        }
      }
      break;
    case ControlStateFactory::PAUSE:
      {
          velMin = 0;
          velMax = 2;
      }
      break;
    case ControlStateFactory::STOPPED:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          velMin = 0;
          velMax = 2;
        } else {
          velMin = 0;
          velMax = 2;
        }
      }
      break;

    case ControlStateFactory::CREEP:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          velMin = 0;
          velMax = 1;
        } else {
          velMin = 0;
          velMax = 1;
        }
      }
      break;

    case ControlStateFactory::UTURN:
      {
        velMin = 0;
        velMax = 1;
      }
      break;


    default:
      
      cerr << "CorridorUtils::setVelProfile: Undefined control state" << endl;        
    }
  corr.setVelocityLimits(velMin, velMax);
   
  return error;
}
