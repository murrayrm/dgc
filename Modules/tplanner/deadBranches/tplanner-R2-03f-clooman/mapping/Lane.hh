#ifndef LANE_HH_
#define LANE_HH_

#include <vector>
#include "Line.hh"

class Lane {


//TODO: question where do we take into account direction of lane ? 
//TODO: question lane number should be wrt to road not the lane itself 

public:
  	Lane(int lineId, std::vector<Line> laneBoudary);
	~Lane();
	std::vector<Line> getLaneBoundaries();
	int getLaneDirection();
	int getId();

private:
	int m_id;
	std::vector<Line> m_laneBoundary;
	int m_laneDirection;

};



#endif /*LANE_HH_*/
