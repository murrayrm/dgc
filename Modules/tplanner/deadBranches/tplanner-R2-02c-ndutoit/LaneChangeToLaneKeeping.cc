#include "LaneChangeToLaneKeeping.hh"

LaneChangeToLaneKeeping::LaneChangeToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

LaneChangeToLaneKeeping::LaneChangeToLaneKeeping()
{

}

LaneChangeToLaneKeeping::~LaneChangeToLaneKeeping()
{

}

double LaneChangeToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in LaneChangeToLaneKeeping::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        int laneID = planHorizon.getSegGoal(0).exitLaneID; // This should be a preferredLaneID instead of exitLaneID

        if (map->getLaneID(AliceStateHelper::getPositionRearBumper(vehState)) == laneID) {
            m_prob = 1;
        }
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
        
        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "LaneChangeToLaneKeeping.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
