#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"

#include "RoadRegion.hh"
#include "ZoneRegion.hh"
#include "ApproachInterSafety.hh"
#include "IntersectionStop.hh"

#include "ZoneToRoadRegion.hh"
#include "RoadToRoadRegion.hh"
#include "RoadToApproachInterSafety.hh"
#include "ApproachInterSafeToInterStop.hh"
#include "InterStopToRoadRegion.hh"

TrafficStateFactory::TrafficStateFactory()
{

}

TrafficStateFactory::TrafficStateFactory(bool debug, bool verbose, bool log)
{
    m_debug = debug;
    m_verbose = verbose;
    m_log = log;
    
    // need to set these variables in TrafficState too
    TrafficState::setOutputParams(debug, verbose, log);
    TrafficStateTransition::setOutputParams(debug, verbose, log);
}


TrafficStateFactory::~TrafficStateFactory()
{

}

StateGraph TrafficStateFactory::createTrafficStates()
{

    vector < StateTransition * >trans;

    int stateId = 0;            // TODO: fix this nonsense (Noel or Vanessa, please explain)

    // Traffic States 
    ZoneRegion *zoneRegion = new ZoneRegion(++stateId, ZONE_REGION);
    RoadRegion *roadRegion = new RoadRegion(++stateId, ROAD_REGION);
    ApproachInterSafety *approachInterSafe = new ApproachInterSafety(++stateId, APPROACH_INTER_SAFETY);
    IntersectionStop *interStop = new IntersectionStop(++stateId, INTERSECTION_STOP);

    // Traffic State Transitions
    ZoneToRoadRegion *zoneRoadTrans = new ZoneToRoadRegion(zoneRegion, roadRegion);
    RoadToRoadRegion *roadRoadTrans = new RoadToRoadRegion(roadRegion, roadRegion);
    RoadToApproachInterSafety *roadApproachTrans = new RoadToApproachInterSafety(roadRegion, approachInterSafe);
    ApproachInterSafeToInterStop *interSafeStopTrans = new ApproachInterSafeToInterStop(approachInterSafe, interStop);
    InterStopToRoadRegion *interStopRoadTrans = new InterStopToRoadRegion(interStop, roadRegion);

    trans.push_back(zoneRoadTrans);
    trans.push_back(roadRoadTrans);
    trans.push_back(roadApproachTrans);
    trans.push_back(interSafeStopTrans);
    trans.push_back(interStopRoadTrans);

    return StateGraph(trans);
}


int TrafficStateFactory::getNextId()
{
    return 0;
}


void TrafficStateFactory::print(int type)
{
    cout << "Traffic State Type: ";
    switch (type) {
    case TrafficStateFactory::ROAD_REGION:
        cout << "ROAD REGION" << endl;
        break;
    case TrafficStateFactory::ZONE_REGION:
        cout << "ZONE REGION" << endl;
        break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
        cout << "APPROACH INTERSECTION SAFETY" << endl;
        break;
    case TrafficStateFactory::INTERSECTION_STOP:
        cout << "INTERSECTION STOP" << endl;
        break;
    default:
        break;
    };
}


string *TrafficStateFactory::printString(int type)
{
    string *tstate_type;
    switch (type) {
    case TrafficStateFactory::ROAD_REGION:
        tstate_type = new string("ROAD REGION");
        break;
    case TrafficStateFactory::ZONE_REGION:
        tstate_type = new string("ZONE REGION");
        break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
        tstate_type = new string("APPROACH INTERSECTION SAFETY");
        break;
    case TrafficStateFactory::INTERSECTION_STOP:
        tstate_type = new string("INTERSECTION STOP");
        break;
    default:
        tstate_type = new string("INVALID");
        cerr << "TrafficStateFactory::printString: state type not recognized" << endl;
        break;
    };
    return tstate_type;
}
