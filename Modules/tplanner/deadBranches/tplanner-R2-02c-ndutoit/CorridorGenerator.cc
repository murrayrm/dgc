#include "CorridorGenerator.hh"

CorridorGenerator::CorridorGenerator(int skynetKey)
  : GcModule("CorridorGenerator", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_latestID(0)
    , m_currentID(0)

{

  /*!\param GcInterface variable */
  m_traffCorrInterface = new TrafficCorridorInterface(skynetKey, this);
  m_traffCorrInterfaceNF = m_traffCorrInterface->getNorthface();

  /*!\param GcInterface variable */
  m_corrTrajPlannerInterface = new CorrTrajPlannerInterface(skynetKey, this);
  m_corrTrajPlannerSF = m_corrTrajPlannerInterface->getSouthface();

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = CorrGenControlStatus::READY_FOR_NEXT;

}

CorridorGenerator::CorridorGenerator(int skynetKey, bool debug, bool verbose, bool log)
  : GcModule("CorridorGenerator", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_log(log)
    , m_latestID(0)
    , m_currentID(0)
{
  
  /*!\param GcInterface variable */
  m_traffCorrInterface = new TrafficCorridorInterface(skynetKey, this);
  m_traffCorrInterfaceNF = m_traffCorrInterface->getNorthface();

  /*!\param GcInterface variable */
  m_corrTrajPlannerInterface = new CorrTrajPlannerInterface(skynetKey, this);
  m_corrTrajPlannerSF = m_corrTrajPlannerInterface->getSouthface();

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = CorrGenControlStatus::READY_FOR_NEXT;

}

CorridorGenerator::~CorridorGenerator()
{

  delete m_corrTrajPlannerInterface;
  delete m_corrTrajPlannerSF;
  delete m_traffCorrInterface;
  delete m_traffCorrInterfaceNF;
}

void CorridorGenerator::CorridorGeneratorLoop(void)
{

}

void CorridorGenerator::arbitrate(ControlStatus* cs, MergedDirective* md) {

  CorrGenControlStatus *controlStatus =
    dynamic_cast<CorrGenControlStatus *>(cs);
  CorrGenMergedDirective *mergedDirective =
    dynamic_cast<CorrGenMergedDirective *>(md);

  CorrGenControlStatus corrGenControlStatus; 

  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == CorrGenControlStatus::COMPLETED ||
      controlStatus->status == CorrGenControlStatus::FAILED) {
    
    CorridorCreateStatus response;
    response.id = controlStatus->id;
    
    if (controlStatus->status == CorrGenControlStatus::COMPLETED) {
      response.status = CorridorCreateStatus::COMPLETED;
      
    } else {
      response.status = CorridorCreateStatus::FAILED;
      //response.reason = controlStatus->reason;
    }

    cerr << "CorrGenControl: sending response (" 
	 << response.id << ") - "
	 << (response.status == CorridorCreateStatus::COMPLETED ? "completed" : "other")
	 << endl;
    m_traffCorrInterfaceNF->sendResponse( &response );
  }

  // Compute the next mergedDirective
  if (m_traffCorrInterfaceNF->haveNewDirective()) {
    CorridorCreate newDirective;
    m_traffCorrInterfaceNF->getNewDirective( &newDirective );

    cerr << "CorrGenControl: have new directive (" 
	 << newDirective.m_Id << ")" << endl;

    // TODO: Determine if you want to reject this directive. This
    // includes checking that the id of newDirective is unique.
    bool reject = false;

    // Update the merged directive if newDirective is not rejected.
    if (!reject) {
      mergedDirective->id = newDirective.m_Id;
    } else {
      // TODO: Default extension
    }
  }
}

void CorridorGenerator::control(ControlStatus* cs, MergedDirective* md) {

  CorrGenControlStatus* controlStatus =
    dynamic_cast<CorrGenControlStatus *>(cs);
  CorrGenMergedDirective* mergedDirective =
    dynamic_cast<CorrGenMergedDirective *>(md);

  double number_arg = mergedDirective->number_arg;

  if (mergedDirective->id != m_currentID) {

    //m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);
    //RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
    //SendRDDF(rddfSocket,rddf);
    //corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state.localX, m_state.localY, localMap);      
    //CDeltaList* deltaList = NULL;
    //unsigned long long timestamp;
    //DGCgettime(timestamp);
    //deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
    //SendMapdelta(staticCostMapSocket, deltaList);
    //m_costMap->resetDelta<double>(m_costLayerID);
                      // send the parameters
    //corridor.convertOCPtoGlobal(m_gloToLocalDelta);
    //bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
    //if (!sendOCPparamsErr){
    // cerr << "TrafficPlanner: problem with send OCP parameters" << endl;
    // cout << "TrafficPlanner: problem with send OCP parameters" << endl;
    //}
    cerr << "Sending command " << number_arg << " to Trajectory Planner (" 
	 << mergedDirective->id << ")" << endl;
    m_currentID = mergedDirective->id;
    controlStatus->id = m_currentID;
    controlStatus->status = CorrGenControlStatus::COMPLETED;

  } else {
    /* If no new directive, we are still executing the old one */
    controlStatus->status = CorrGenControlStatus::READY_FOR_NEXT;
  }
}

Conflict CorridorGenerator::evaluateDynamicConflicts(Corridor m_currCorridor, ControlState *controlState) 
{
  Conflict conflict;
  return conflict;
}

void CorridorGenerator::evaluateCorridors() 
{
  //return m_currCorridor;
}

