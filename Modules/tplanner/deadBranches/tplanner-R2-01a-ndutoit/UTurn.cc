#include "UTurn.hh"


UTurn::UTurn(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
{
  m_stage = 1; // want to go into stage one first
  m_firstRun = true;
}

UTurn::~UTurn()
{
}

int UTurn::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{

  cout << "In UTurn.cc::determine corridor" << endl;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  // TODO: when I receive a uturn, I do not (at the moment) get any entry info. Thus, I either have to remember this info, or Nok has to send it. 
  cout << " ==============================" << endl;
  cout << "HARD CODING SEGMENT ENTRY INFO FOR NOW. THIS NEEDS TO CHANGE!!!!!" << endl;
  currSegment.entrySegmentID = 1;
  currSegment.entryLaneID = 1;
  currSegment.entryWaypointID = 1;
  cout << " ==============================" << endl;

  double velIn, velOut, acc;
  vector<double> velProfile;

  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;

  point2 IC_pos;
  double IC_velMin, IC_velMax;

  point2 FC_pos, tmpPt;
  point2arr leftBound, rightBound;
  //  point2arr tmpLeftBound, tmpRightBound;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
  double currVel = AliceStateHelper::getVelocityMag(vehState);
  point2 aliceInitPos = getInitialPosition();
  int index;

  // Step 1: Get bounds (from obstacle and back to 30 m
  // from the control state transition, we have our position when we transition into uturn. All other maneuvers relative to this?
  LaneLabel laneLabel_curr(currSegment.entrySegmentID, currSegment.entryLaneID);
  // for now just deal with two lanes (until sam has this wrapped up)
  int otherLane;
  if (currSegment.entryLaneID == 1) {
    otherLane =  currSegment.entryLaneID + 1;
  } else {
    otherLane =  currSegment.entryLaneID - 1;
  }
  LaneLabel laneLabel_other(currSegment.entrySegmentID, otherLane);
  if (m_firstRun){
    m_obsPos = localMap->getObstaclePoint(currFrontPos, 0);
    m_rearBound = localMap->getObstaclePoint(currFrontPos, 30);
    localMap->getLaneCenterPoint(m_ptStage1, laneLabel_curr, aliceInitPos, -18.0);
    localMap->getLaneRightPoint(m_ptStage2, laneLabel_other, aliceInitPos, 7.0);
    localMap->getLaneCenterPoint(m_ptStage3, laneLabel_curr, aliceInitPos, 5.0);
    localMap->getLaneCenterPoint(m_ptStage4, laneLabel_other, aliceInitPos, 20.0);
    m_firstRun = false;
  }

  //int getBoundsErr = localMap->getRightBound(tmpRightBound, laneLabel_curr);
  //if (getBoundsErr!=0){
  //   cerr << "LaneKeeping.cc: Right boundary (current lane) read from map error" << endl;
  //  return (getBoundsErr);
  //}  
  //point2arr tmp2LeftBound;
  //getBoundsErr = localMap->getRightBound(tmp2LeftBound, laneLabel_other);
  //if (getBoundsErr!=0){
  //  cerr << "LaneKeeping.cc: Right boundary (other lane) read from map error" << endl;
  //  return (getBoundsErr);
  //}  
  // invert the order on the left bound pts
  //for (int ii=(int)tmp2LeftBound.size()-1; ii>=0; ii--) {
  //  tmpLeftBound.push_back(tmp2LeftBound[ii]);
  //}
  //if (0) {
  //  cout << "Original boundaries" << endl;
   // for (int ii=0; ii<(int)tmpRightBound.size(); ii++) {
  //    cout << "RB[" << ii << "] = " << tmpRightBound[ii] << endl;
  //  }
  //  for (int ii=0; ii<(int)tmpLeftBound.size(); ii++) {
  //    cout << "LB[" << ii << "] = " << tmpLeftBound[ii] << endl;
  //  }
  //}
  cout << "obstacle pos = " << m_obsPos << endl;
  cout << "rear bound pos = " << m_rearBound << endl;
  //  // insert a pt on the rddf corresponding to 30m before the obs
  // TrafficUtils::insertProjPtInBoundary(tmpLeftBound, m_rearBound);
  // TrafficUtils::insertProjPtInBoundary(tmpRightBound, m_rearBound);
  // cut the boundaries before this point
  //index = TrafficUtils::getClosestPtIndex(tmpLeftBound, m_rearBound);
  
  localMap->getLaneRightPoint(tmpPt, laneLabel_other, m_rearBound, 0);
  leftBound.push_back(tmpPt);
  //  index = TrafficUtils::getClosestPtIndex(tmpRightBound, m_rearBound);
  localMap->getLaneRightPoint(tmpPt, laneLabel_curr, m_rearBound, 0);
  rightBound.push_back(tmpPt);

  // insert a pt on the rddf corresponding to the obstacle
  //  TrafficUtils::insertProjPtInBoundary(tmpLeftBound, m_obsPos);
  // TrafficUtils::insertProjPtInBoundary(tmpRightBound, m_obsPos);
  // truncate the boundaries after this point
  //index = TrafficUtils::getClosestPtIndex(tmpLeftBound, m_obsPos);
  //leftBound.push_back(tmpLeftBound.arr[index]);
  //index = TrafficUtils::getClosestPtIndex(tmpRightBound, m_obsPos);
  // rightBound.push_back(tmpRightBound.arr[index]);
  localMap->getLaneRightPoint(tmpPt, laneLabel_other, m_obsPos, 0);
  leftBound.push_back(tmpPt);
  //  index = TrafficUtils::getClosestPtIndex(tmpRightBound, m_rearBound);
  localMap->getLaneRightPoint(tmpPt, laneLabel_curr, m_obsPos, 0);
  rightBound.push_back(tmpPt);


  if (0){
    cout << "final boundaries" << endl;
    //print the boundaries
    for (int ii=0; ii<(int)rightBound.size(); ii++) {
      cout << "RB[" << ii << "] = " << rightBound[ii] << endl;
    }
    for (int ii=0; ii<(int)leftBound.size(); ii++) {
      cout << "LB[" << ii << "] = " << leftBound[ii] << endl;
    }
  }


  // state machine to deal with u-turn
  //===================================================
  //  cout << "IN UTURN: STAGE HARDCODED!!! NEEDS TO BE TAKEN OUT" << endl;
  //m_stage = 3;


  double heading, laneDir, orient;
  double compDist;
  double dotProd;
  double stoppedVel = 0.02;
  PointLabel nextPtLabel, ptLabel;

  switch(m_stage)
  {
    
  case 1: // stage 1: reverse until we are at pt in current lane, 25 m from obs
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 1: reverse" << endl;
    corr.setOCPmode(1);

    // get all the relevant points for the u-turn
    // specify the ocpParams final conditions - lower bounds
    // final pos should be 25 m from the obstacle
    FC_pos = m_ptStage1;
    FC_velMin =  0;
    localMap->getHeading(laneDir, FC_pos);
    cout << "laneDir = " << laneDir << endl;
    heading = laneDir;
    cout << "heading = " << heading << endl;
    orient = laneDir - M_PI;
    cout << "orient = " << orient << endl;
    // check if we are done with this reverse and we are stopped before switching to stage 2
    
    dotProd = (-FC_pos.x+currRearPos.x)*cos(orient) + (-FC_pos.y+currRearPos.y)*sin(orient);
    cout << "in uturn: dot product  = " << dotProd << endl;
    // specify some vel profile
    compDist = 1.5;
    if (dotProd>-compDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 2;
      velOut = 2;
      acc = 0;
    }
    FC_headingMin = fmod(heading,2*M_PI); 
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(heading, 2*M_PI);
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    
    // Initial cond's
    IC_pos = currRearPos;

    if((dotProd>-compDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      cout << "UTURN: SWITCHING TO STAGE 2!!!" << endl;
      m_stage = 2;
    }
    break;

  case 2: // stage 2: drive fwd and turn 90 degrees, at dist 8m from obstacle
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 2: fwd" << endl;
    corr.setOCPmode(0);

    // specify the ocpParams final conditions - lower bounds
    // final pos should be  17m from the obstacle, and facing out of the lane
    tmpPt = m_ptStage2;
    TrafficUtils::insertProjPtInBoundary(leftBound, tmpPt);
    index = TrafficUtils::getClosestPtIndex(leftBound, tmpPt);
    FC_pos = leftBound.arr[index];
    FC_velMin =  0;
    // offset this pt a little away from the lane so that it returns something (a map bug)
    // TODO: this is a temporary fix -
    tmpPt = (FC_pos+tmpPt)/2;
    //    localMap->getHeading(laneDir, FC_pos); // lane dir is the orientation of the lane
    localMap->getHeading(laneDir, tmpPt); // lane dir is the orientation of the lane
    cout << "laneDir = " << laneDir << endl;
    heading = laneDir + M_PI/2; // heading is the heading for the final cond
    cout << "heading = " << heading << endl;
    orient = laneDir + M_PI/2; // orientation is the orientation of the unit vector for the complete test
    cout << "orient = " << orient << endl;
    // check if we are done with this reverse and we are stopped before switching to stage 2
    
    dotProd = (-FC_pos.x+currFrontPos.x)*cos(orient) + (-FC_pos.y+currFrontPos.y)*sin(orient);
    cout << "in uturn: dot product  = " << dotProd << endl;
    // specify some vel profile
    compDist = 0.5;
    if (dotProd>-compDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 2;
      velOut = 2;
      acc = 0;
    }

    FC_headingMin = fmod(heading, 2*M_PI);
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(heading, 2*M_PI);
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

    // Initial cond's
    IC_pos = currFrontPos;

    if((dotProd>-compDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      cout << "UTURN: SWITCHING TO STAGE 3!!!" << endl;
      m_stage = 3;
    }

    break;

  case 3: // stage 3: Reverse until 8 m from obstacle
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 3: reverse" << endl;
    corr.setOCPmode(1);
    cout << "Not yet implemented" << endl;


    // get all the relevant points for the u-turn
    // specify the ocpParams final conditions - lower bounds
    // final pos should be 25 m from the obstacle
    FC_pos = m_ptStage3;
    FC_velMin =  0;
    localMap->getHeading(laneDir, FC_pos);
    cout << "laneDir = " << laneDir << endl;
    heading = laneDir - M_PI;
    cout << "heading = " << heading << endl;
    orient = laneDir;
    cout << "orient = " << orient << endl;
    
    dotProd = (-FC_pos.x+currRearPos.x)*cos(orient) + (-FC_pos.y+currRearPos.y)*sin(orient);
    cout << "in uturn: dot product  = " << dotProd << endl;
    // specify some vel profile
    compDist = 1.5;
    if (dotProd>-compDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 2;
      velOut = 2;
      acc = 0;
    }
    FC_headingMin = fmod(heading,2*M_PI); 
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(heading, 2*M_PI);
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    
    // Initial cond's
    IC_pos = currRearPos;

    if((dotProd>-compDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      cout << "UTURN: SWITCHING TO STAGE 2!!!" << endl;
      m_stage = 4;
    }

    break;

  case 4: // stage 4: drive fwd to exit pt in other lane
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 4: fwd" << endl;
    corr.setOCPmode(0);

    // specify the ocpParams final conditions - lower bounds
    // final pos should be 30m from the obstacle in the other lane
    // TODO: fix this so that we exit in the middle of the other lane, but for now just exit in the middle of the lanes
    cout << "UTURN: STAGE 4, THIS EXIT PT SPEC IS A HACK AND NEEDS TO BE FIXED" << endl;
    // THIS POINT NEEDS TO BE PROJECTED INTO THE OTHER LANE
    tmpPt = m_ptStage4;
    TrafficUtils::insertProjPtInBoundary(leftBound, tmpPt);
    index = TrafficUtils::getClosestPtIndex(leftBound, tmpPt);
    FC_pos = leftBound.arr[index];
    TrafficUtils::insertProjPtInBoundary(rightBound, tmpPt);
    index = TrafficUtils::getClosestPtIndex(rightBound, tmpPt);
    FC_pos = (FC_pos + rightBound.arr[index])/2;

    FC_velMin =  0;
    // offset this pt a little away from the lane so that it returns something (a map bug)
    localMap->getHeading(laneDir, FC_pos); // lane dir is the orientation of the lane
    cout << "laneDir = " << laneDir << endl;
    heading = laneDir; // heading is the heading for the final cond
    cout << "heading = " << heading << endl;
    orient = laneDir; // orientation is the orientation of the unit vector for the complete test
    cout << "orient = " << orient << endl;
    // check if we are done with this reverse and we are stopped before switching to stage 2
    
    dotProd = (-FC_pos.x+currFrontPos.x)*cos(orient) + (-FC_pos.y+currFrontPos.y)*sin(orient);
    cout << "in uturn: dot product  = " << dotProd << endl;
    // specify some vel profile
    compDist = 0.5;
    if (dotProd>-compDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 2;
      velOut = 2;
      acc = 0;
    }

    FC_headingMin = fmod(heading, 2*M_PI);
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(heading, 2*M_PI);
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

    // Initial cond's
    IC_pos = currFrontPos;

    if((dotProd>-compDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      cout << "UTURN: SWITCHING TO STAGE 3!!!" << endl;
      m_stage = 5;
    }
      
    break;


  case 5: // stage 4: drive fwd to exit pt in other lane
    // This should just be normal lane keeping from here on out


    
    // TODO: in check for complete should set m_firstRun = true and m_stage = 1
    cout << "NEED TO RESET SOME PARAMS IN UTURN IN THE TRANSITION" << endl;
    break;



  default:
    break;
  } // switch

  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);
  
  // Set the velocity profile - need to think this through some
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);
  
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_pos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_pos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_pos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_pos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  
  corr.setOCPinitialCondLB(EASTING_IDX_C, IC_pos.y);
  corr.setOCPinitialCondLB(NORTHING_IDX_C, IC_pos.x);
  corr.setOCPinitialCondUB(EASTING_IDX_C, IC_pos.y);
  corr.setOCPinitialCondUB(NORTHING_IDX_C, IC_pos.x);
 
  return 0;


}
