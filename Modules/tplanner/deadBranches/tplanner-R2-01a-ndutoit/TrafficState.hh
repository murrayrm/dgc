#ifndef TRAFFICSTATE_HH_
#define TRAFFICSTATE_HH_

#include "TrafficStateFactory.hh"
#include "state/State.hh"

class TrafficState : public State 
{

public: 

  TrafficState();
  TrafficState(int stateId, TrafficStateFactory::TrafficStateType type);
  ~TrafficState();
  //TrafficStateFactory::TrafficStateType getType();
  
	
private: 
  //int m_stateId;
  //TrafficStateFactory::TrafficStateType m_type;

};

#endif /*TRAFFICSTATE_HH_*/
