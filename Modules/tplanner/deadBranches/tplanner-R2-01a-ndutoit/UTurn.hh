#ifndef UTURN_HH_
#define UTURN_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class UTurn : public ControlState {

public: 

  UTurn(int stateId, ControlStateFactory::ControlStateType type);
  ~UTurn();
  virtual int determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* map);


private:
  bool m_firstRun;
  int m_stage;
  point2 m_obsPos;
  point2 m_rearBound;
  point2 m_ptStage1;
  point2 m_ptStage2;
  point2 m_ptStage3;
  point2 m_ptStage4;

};
#endif /*UTURN_HH_*/
