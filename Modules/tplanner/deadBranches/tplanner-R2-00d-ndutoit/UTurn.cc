#include "UTurn.hh"


UTurn::UTurn(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
{
  m_stage = 1; // want to go into stage one first
  m_firstRun = true;
}

UTurn::~UTurn()
{
}

int UTurn::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{

  cout << "In UTurn.cc::determine corridor" << endl;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  vector<double> velProfile;

  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;

  point2 IC_pos;
  double IC_velMin, IC_velMax;

  point2 FC_pos;
  point2arr leftBound, rightBound;
  point2arr tmpLeftBound, tmpRightBound;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
  double currVel = AliceStateHelper::getVelocityMag(vehState);

  int index;


  // from the control state transition, we have our position when we transition into uturn. All other maneuvers relative to this?

  if (m_firstRun){
    m_obsPos = localMap->getObstaclePoint(currFrontPos, 0);
    m_rearBound = localMap->getObstaclePoint(currFrontPos, 30);
    m_ptStage1 = localMap->getObstaclePoint(currFrontPos, 28);
    m_ptStage2 = localMap->getObstaclePoint(currFrontPos, 17);
    m_ptStage3 = localMap->getObstaclePoint(currFrontPos, 8);
    m_ptStage4 = localMap->getObstaclePoint(currFrontPos, 30);
    m_firstRun = false;
  }

  // Step 1: Get bounds (from obstacle and back to 30 m
  LaneLabel laneLabel_curr(currSegment.entrySegmentID, currSegment.entryLaneID);
  int getBoundsErr = localMap->getRightBound(tmpRightBound, laneLabel_curr);
  if (getBoundsErr!=0){
    cerr << "LaneKeeping.cc: Right boundary (current lane) read from map error" << endl;
    return (getBoundsErr);
  }  
  // for now just deal with two lanes (until sam has this wrapped up)
  int otherLane;
  if (currSegment.entryLaneID == 1) {
    otherLane =  currSegment.entryLaneID + 1;
  } else {
    otherLane =  currSegment.entryLaneID - 1;
  }
  LaneLabel laneLabel_other(currSegment.entrySegmentID, otherLane);
  point2arr tmp2LeftBound;
  getBoundsErr = localMap->getRightBound(tmp2LeftBound, laneLabel_other);
  if (getBoundsErr!=0){
    cerr << "LaneKeeping.cc: Right boundary (other lane) read from map error" << endl;
    return (getBoundsErr);
  }  
  // invert the order on the left bound pts
  for (int ii=(int)tmp2LeftBound.size()-1; ii>=0; ii--) {
    tmpLeftBound.push_back(tmp2LeftBound[ii]);
  }
  if (0) {
    cout << "Original boundaries" << endl;
    for (int ii=0; ii<(int)tmpRightBound.size(); ii++) {
      cout << "RB[" << ii << "] = " << tmpRightBound[ii] << endl;
    }
    for (int ii=0; ii<(int)tmpLeftBound.size(); ii++) {
      cout << "LB[" << ii << "] = " << tmpLeftBound[ii] << endl;
    }
  }
  cout << "obstacle pos = " << m_obsPos << endl;
  cout << "rear bound pos = " << m_rearBound << endl;
  // insert a pt on the rddf corresponding to 30m before the obs
  TrafficUtils::insertProjPtInBoundary(tmpLeftBound, m_rearBound);
  TrafficUtils::insertProjPtInBoundary(tmpRightBound, m_rearBound);
  // cut the boundaries before this point
  index = TrafficUtils::getClosestPtIndex(tmpLeftBound, m_rearBound);
  leftBound.push_back(tmpLeftBound.arr[index]);
  index = TrafficUtils::getClosestPtIndex(tmpRightBound, m_rearBound);
  rightBound.push_back(tmpRightBound.arr[index]);

  // insert a pt on the rddf corresponding to the obstacle
  TrafficUtils::insertProjPtInBoundary(tmpLeftBound, m_obsPos);
  TrafficUtils::insertProjPtInBoundary(tmpRightBound, m_obsPos);
  // truncate the boundaries after this point
  index = TrafficUtils::getClosestPtIndex(tmpLeftBound, m_obsPos);
  leftBound.push_back(tmpLeftBound.arr[index]);
  index = TrafficUtils::getClosestPtIndex(tmpRightBound, m_obsPos);
  rightBound.push_back(tmpRightBound.arr[index]);

  if (0){
    cout << "final boundaries" << endl;
    //print the boundaries
    for (int ii=0; ii<(int)rightBound.size(); ii++) {
      cout << "RB[" << ii << "] = " << rightBound[ii] << endl;
    }
    for (int ii=0; ii<(int)leftBound.size(); ii++) {
      cout << "LB[" << ii << "] = " << leftBound[ii] << endl;
    }
  }


  // state machine to deal with u-turn
  //===================================================
  //cout << "IN UTURN: STAGE HARDCODED!!! NEEDS TO BE TAKEN OUT" << endl;
  //m_stage = 2;


  double heading, laneDir, orient;
  double compDist;
  double dotProd;
  double stoppedVel = 0.02;
  PointLabel nextPtLabel, ptLabel;
  point2 tmpPt;

  switch(m_stage)
  {
    
  case 1: // stage 1: reverse until we are at pt in current lane, 25 m from obs
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 1: reverse" << endl;
    corr.setOCPmode(1);

    // get all the relevant points for the u-turn
    // specify the ocpParams final conditions - lower bounds
    // final pos should be 25 m from the obstacle
    FC_pos = m_ptStage1;
    FC_velMin =  0;
    localMap->getHeading(laneDir, FC_pos);
    cout << "laneDir = " << laneDir << endl;
    heading = laneDir;
    orient = laneDir - M_PI;
    cout << "orient = " << orient << endl;
    // check if we are done with this reverse and we are stopped before switching to stage 2
    
    dotProd = (-FC_pos.x+currRearPos.x)*cos(orient) + (-FC_pos.y+currRearPos.y)*sin(orient);
    cout << "in uturn: dot product  = " << dotProd << endl;
    // specify some vel profile
    compDist = 1.5;
    if (dotProd>-compDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 2;
      velOut = 2;
      acc = 0;
    }
    FC_headingMin = fmod(heading,2*M_PI); 
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(heading, 2*M_PI);
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    
    // Initial cond's
    IC_pos = currRearPos;

    if((dotProd>-compDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      cout << "UTURN: SWITCHING TO STAGE 2!!!" << endl;
      m_stage = 2;
    }
    break;

  case 2: // stage 2: drive fwd and turn 90 degrees, at dist 8m from obstacle
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 2: fwd" << endl;
    corr.setOCPmode(0);

    // specify the ocpParams final conditions - lower bounds
    // final pos should be  17m from the obstacle, and facing out of the lane
    tmpPt = m_ptStage2;
    TrafficUtils::insertProjPtInBoundary(leftBound, tmpPt);
    index = TrafficUtils::getClosestPtIndex(leftBound, tmpPt);
    FC_pos = leftBound.arr[index];
    FC_velMin =  0;
    // offset this pt a little away from the lane so that it returns something (a map bug)
    // TODO: this is a temporary fix -
    tmpPt = (FC_pos+tmpPt)/2;
    //    localMap->getHeading(laneDir, FC_pos); // lane dir is the orientation of the lane
    localMap->getHeading(laneDir, tmpPt); // lane dir is the orientation of the lane
    cout << "laneDir = " << laneDir << endl;
    heading = laneDir + M_PI/2; // heading is the heading for the final cond
    cout << "heading = " << heading << endl;
    orient = laneDir + M_PI/2; // orientation is the orientation of the unit vector for the complete test
    cout << "orient = " << orient << endl;
    // check if we are done with this reverse and we are stopped before switching to stage 2
    
    dotProd = (-FC_pos.x+currFrontPos.x)*cos(orient) + (-FC_pos.y+currFrontPos.y)*sin(orient);
    cout << "in uturn: dot product  = " << dotProd << endl;
    // specify some vel profile
    compDist = 0.5;
    if (dotProd>-compDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 2;
      velOut = 2;
      acc = 0;
    }

    FC_headingMin = fmod(heading, 2*M_PI);
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(heading, 2*M_PI);
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

    // Initial cond's
    IC_pos = currFrontPos;

    if((dotProd>-compDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      cout << "UTURN: SWITCHING TO STAGE 3!!!" << endl;
      m_stage = 3;
    }

    break;

  case 3: // stage 3: Reverse until 8 m from obstacle
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    cout << "stage 3: reverse" << endl;
    corr.setOCPmode(1);
    cout << "Not yet implemented" << endl;

    /*   
    // specify the ocpParams final conditions - lower bounds
    // final pos should be 25 m from the obstacle
    FC_pos = localMap->getObstaclePoint(currFrontPos, 28);
    FC_velMin =  0;
    nextPtLabel = localMap->getNextPointID(currFrontPos);
    nextPt = nextPtLabel.point-1;
    ptLabel = PointLabel(nextPtLabel.segment, nextPtLabel.lane, nextPt);
    localMap->getHeading(tmp_heading, FC_finalPos);
    cout << "tmp_heading = " << tmp_heading << endl;
    FC_headingMin = fmod(tmp_heading + M_PI - M_PI/6, M_PI); // want to rotate through 180 degrees for reversing
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = fmod(tmp_heading + M_PI + M_PI/6, 2*M_PI); // want to rotate through 180 degrees for reversing
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    
    // Initial cond's
    IC_pos = currRearPos;

    // check if we are done with this reverse and we are stopped before switching to stage 2
    dotProd = (-FC_pos.x+currRearPos.x)*cos(heading) + (-FC_pos.y+currRearPos.y)*sin(heading);
    cout << "in uturn: dot product  = " << dotProd << endl;

    // specify some vel profile
    if (dotProd>-completeDist) {
      velIn = 0;
      velOut = 0;
      acc = 0;
    } else {
      velIn = 1;
      velOut = 1;
      acc = 0;
    }

    if((dotProd>-completeDist)&&(currVel<stoppedVel)) {
      // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
      stage = 4;
    }
    */
    break;




  case 4: // stage 4: drive fwd to exit pt in other lane
      
    break;


  case 5: // stage 4: drive fwd to exit pt in other lane

    //in check for complete should set m_firstRun = true and m_stage = 1
    
    break;



  default:
    break;
  } // switch

  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);
  
  // Set the velocity profile - need to think this through some
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);
  
  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_pos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_pos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_pos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_pos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  
  corr.setOCPinitialCondLB(EASTING_IDX_C, IC_pos.y);
  corr.setOCPinitialCondLB(NORTHING_IDX_C, IC_pos.x);
  corr.setOCPinitialCondUB(EASTING_IDX_C, IC_pos.y);
  corr.setOCPinitialCondUB(NORTHING_IDX_C, IC_pos.x);
 
  return 0;


}
