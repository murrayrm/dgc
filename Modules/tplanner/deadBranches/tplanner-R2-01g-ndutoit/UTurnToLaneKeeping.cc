#include "UTurnToLaneKeeping.hh"
#include "UTurn.hh"

UTurnToLaneKeeping::UTurnToLaneKeeping(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
{
  m_distToTrans = 1;
}

UTurnToLaneKeeping::UTurnToLaneKeeping()
{

}

UTurnToLaneKeeping::~UTurnToLaneKeeping()
{

}

double UTurnToLaneKeeping::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon,Map* localMap, VehicleState vehState)
{
  UTurn* cState = static_cast<UTurn*>(controlState);    
  SegGoals currSegGoal = planHorizon.getSegGoal(0);

  cout << "in UTurnToLaneKeeping::meetTransitionConditions" <<endl;
	point2 exitWayPt;
  PointLabel ptLabel = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
  cout << "exit label" << endl;
	point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
	point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);


  //TODO: I took this if out so that we always transition out of the uturn, no matter what traffic state we are in
  //if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    // use getStopline() that gives next stopline in current lane
    localMap->getWaypoint(exitWayPt, ptLabel);
    double angle;
    localMap->getHeading(angle, ptLabel);
    cout << "heading = " << angle << endl;
    double dotProd = (-exitWayPt.x+currFrontPos.x)*cos(angle) + (-exitWayPt.y+currFrontPos.y)*sin(angle);
    cout << "in UTurnToLaneKeeping: dot product  = " << dotProd << endl;
    
    if ((dotProd > -m_distToTrans)||(!(SegGoals::UTURN == currSegGoal.segment_type))) {
	    m_probability = 1;
      cState->resetParameters();
	  } else { 
      m_probability = 0; 
    }

    //}




  setUncertainty(m_probability);
  return m_probability;
} 


