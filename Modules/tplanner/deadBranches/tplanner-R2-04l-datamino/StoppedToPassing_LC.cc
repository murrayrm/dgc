#include "StoppedToPassing_LC.hh"

StoppedToPassing_LC::StoppedToPassing_LC(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

StoppedToPassing_LC::StoppedToPassing_LC()
{

}

StoppedToPassing_LC::~StoppedToPassing_LC()
{

}

double StoppedToPassing_LC::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToPassing_LC::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      {
        SegGoals seg = planHorizon.getSegGoal(0);
        LaneLabel desiredLane;

        bool dist_ok = true;
        point2 stopLinePos;
        LaneLabel currLane = AliceStateHelper::getDesiredLaneLabel();
        PointLabel label = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
        int stopLineErr = map->getNextStopline(stopLinePos, label);
        if (stopLineErr != -1) {
          cout << "We have a stopline!" << endl;
          point2 obsPt;
          double dist_stopline;
          point2arr centerline;
          map->getLaneCenterLine(centerline, currLane);
          TrafficUtils::getNearestObsFarthermostPoint(obsPt, map, vehState, currLane, 0);
          cout << "Obstacle point " << obsPt << " - Stopline point " << stopLinePos << endl;
          map->getDistAlongLine(dist_stopline, centerline, stopLinePos, obsPt);
          cout << "Distance is " << dist_stopline << endl;
          if (dist_stopline > 0.0)
            dist_ok = dist_stopline > 20;
        }

        string lt, rt;

        map->getLeftBoundType(lt, currLane);
        map->getRightBoundType(rt, currLane);

        LaneLabel lane_left, lane_right;
        map->getNeighborLane(lane_left, currLane, -1);
        map->getNeighborLane(lane_right, currLane, 1);

        bool sameDirLeft = false;
        bool sameDirRight = false;
        vector<LaneLabel> sameDirLanes;
        map->getSameDirLanes(sameDirLanes, currLane);
        for (unsigned int i=0; i<sameDirLanes.size(); i++) {
          if (sameDirLanes[i] == lane_left)
            sameDirLeft = true;
          if (sameDirLanes[i] == lane_right)
            sameDirRight = true;
        }

        bool passing_allowed = false;

        LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

        if (lane_left.lane>0 && (lt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
	      desiredLane = lane_left;
	      laneChange->setReverse(sameDirLeft?0:1);
          passing_allowed = true;
	      cout << "Can change lanes left to lane " << lane_left << endl;
	      cout << "Same direction left: " << sameDirLeft << endl;
	    }

        if (lane_right.lane>0 && (rt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
	      desiredLane = lane_right;
	      laneChange->setReverse(sameDirRight?0:1);
          passing_allowed = true;
	      cout << "Can change lanes right to lane " << lane_right << endl;
	      cout << "Same direction right: " << sameDirRight << endl;
	   }

	   CheckPass cpass;
	   double p=cpass.getProbability(vehState,map);
	   cout << "Probability for passing..." << p << endl;

        if (dist_ok && passing_allowed && p>=.70) {
	      AliceStateHelper::setDesiredLaneLabel(desiredLane);
          m_prob = 1;
	    if (m_verbose || m_debug)
	      cout << "Transitioning to Passing1..." << endl;
        }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "StoppedToPassing_LC.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
