#include <iostream>
#include <frames/point2.hh>
#include <bitmap/BitmapParams.hh>
#include <skynettalker/SkynetTalker.hh>
#include <ocpspecs/CostMapEstimator.hh>
#include <highgui.h>
#include "Corridor.hh"
#include <boost/serialization/vector.hpp>

using namespace std;

int main(int argc,char** argv)
{
  int skynetKey = skynet_findkey(argc, argv);
  cout << "Skynet key: " << skynetKey << endl;
  /* Receiver */
  CostMap cmap;
  CostMapEstimator cmapEst(skynetKey, true, false, 2);

  while (true) {
    bool cmapRecv = cmapEst.updateMap(&cmap);
  }

}
