#ifndef PASSING2_LANECHANGETOLANEKEEPING_HH_
#define PASSING2_LANECHANGETOLANEKEEPING_HH_

#include "ControlStateTransition.hh"
#include "LaneChange.hh"

class Passing2_LaneChangeToLaneKeeping:public ControlStateTransition {

public:
  Passing2_LaneChangeToLaneKeeping(ControlState *state1, ControlState *state2);
  Passing2_LaneChangeToLaneKeeping();
  ~Passing2_LaneChangeToLaneKeeping();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
