#ifndef STOPPEDTOLANEKEEPING_HH_
#define STOPPEDTOLANEKEEPING_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class StoppedToLaneKeeping : public ControlStateTransition {


public: 

  StoppedToLaneKeeping(ControlState* state1, ControlState* state2);

  StoppedToLaneKeeping();

  ~StoppedToLaneKeeping();

  double meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon, Map* map, VehicleState vehState);



private: 

};
#endif /*STOPPEDTOLANEKEEPING_HH_*/
