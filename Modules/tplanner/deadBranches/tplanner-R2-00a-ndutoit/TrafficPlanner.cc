/*!TrafficPlanner.cc
 * Author: Noel duToit
 * Last revision: Feb 24 2007
 * */

#include "TrafficPlanner.hh"
//#include "TrafficStateFactory.hh"
//#include "ControlStateFactory.hh"
#include "ZoneRegion.hh"
#include "Stopped.hh"

using namespace std;

//#define MAX_DELTA_SIZE 100000

CTrafficPlanner::CTrafficPlanner(int skynetKey, bool bWaitForStateFill, bool recvLocalMap, bool recvSegmentGoals, int debugLevel, int verboseLevel)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(bWaitForStateFill)
  , m_OCPparamsTalker(skynetKey, SNocpParams, MODtrafficplanner)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = skynetKey;
  m_recvLocalMap = recvLocalMap;
  m_recvSegmentGoals = recvSegmentGoals;
  //  receiveSegmentGoals = true;
  //if (debugLevel>0)
  m_debug = true;
  //else m_debug = false;
  //if (verboseLevel>0)
  m_verbose = true;
  //else m_verbose = false;

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);
  //DGCcreateMutex(&m_LocalMapRecvMutex);
  //DGCcreateMutex(&m_ObstacleMutex);
  // dplanner status
  //DGCcreateMutex(&m_DPlannerStatusMutex);
  // segment goals
  DGCcreateMutex(&m_SegGoalsMutex);
  //cost map
  DGCcreateMutex(&m_CostMapMutex);

 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions

  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Skynet listen sockets
  // Local Map
  //localMapSocket = m_skynet.listen(SNtrafficLocalMap, MODmapping);

  // Segment Goals
  segGoalsSocket = m_skynet.listen(SNsegGoals, MODmissionplanner);

  // dplanner status
  // TODO: get socket for this message

  // dplanner static cost map request
  //requestFullStaticCostMapSocket = m_skynet.listen(SNtplannerStaticCostMapRequest, MODdynamicplanner);
  requestFullStaticCostMapSocket = m_skynet.listen(SNtplannerStaticCostMapRequest, MODtrafficplanner);
  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(skynetKey);
  initSendMapElement(skynetKey);
  if(!m_recvLocalMap){
    //bool loadMapFromRNDF =  loadRNDF(string filename) {return localmap.loadRNDF(filename);
  }
  m_localMap = new Map();
  localMap = new Map();

  // Segment Goals
  if(segGoalsSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;

  // cost map
  // cost map parameters
  double northing = 0.0;
  double easting = 0.0;
  m_numRows = 500;
  m_numCols = 500;
  double resRows = 0.4;
  double resCols = 0.4;
  int cmap_verboselevel = 0;

  // initialize the cost map using the configuration parameters
  m_costMap.initMap(northing, easting, m_numRows, m_numCols, resRows, resCols, cmap_verboselevel);

  //============================================
  // ADD LAYERS TO MAP:
  // 1) 1st layer will be cost
  // 2) 2nd layer will be cell condition (either dirty or clean)
  //============================================
  m_costLayerID = m_costMap.addLayer<double>(-1, -2, false); //nodataval, outsidemapval, uses deltas
  m_conditionLayerID = m_costMap.addLayer<int>(0,-1,false); 

  // initialize the test map using the configuration parameters
  m_testMap.initMap(northing, easting, m_numRows, m_numCols, resRows, resCols, cmap_verboselevel);
  // add a layer to the test map
  m_testLayerID = m_testMap.addLayer<double>(-1, -2, false);
  
  // Initialize the control and traffic states for startup

  m_tfac = TrafficStateFactory();
  m_cfac = ControlStateFactory();

  m_trafStateEst = new TrafficStateEstimator();
  m_currTrafficState = new ZoneRegion(0,TrafficStateFactory::ZONE_REGION);
  cout<<"FIRST GET TYPE ON CURRENT TRAFFIC STATE "<<endl;
  m_tfac.print(m_currTrafficState->getType());
  m_currControlState =  new Stopped(0,ControlStateFactory::STOPPED);
  
  m_tplannerControl =  new TrafficPlannerControl();
  //m_currControlState = m_tplannerControl->getCurrentControlState(); //TODO overkill FIX 
  cout<<"SECOND GET TYPE ON CURRENT TRAFFIC STATE "<<endl;
  m_tfac.print(m_currTrafficState->getType());
 


}

CTrafficPlanner::~CTrafficPlanner() 
{
  // delete pointers
  delete m_localMap;
  delete localMap;
  //delete m_dplannerStatus;

  // delete mutexes
  //DGCdeleteMutex(&m_LocalMapMutex);
  //DGCdeleteMutex(&m_ObstacleMutex);
  //DGCdeleteMutex(&m_DPlannerStatusMutex);
  DGCdeleteMutex(&m_SegGoalsMutex);
  DGCdeleteMutex(&m_CostMapMutex);
  //delete conditions

  delete m_trafStateEst;
  delete m_tplannerControl;

}

void CTrafficPlanner::getLocalMapThread()
{
  //if (recvLocalMap)
  //{
  //TODO: Put Sam's code here
  //}
  //else
  //{
  //TODO: initialize map object from the rndf and use that to plan
  //}
}


void CTrafficPlanner::getDPlannerStatusThread()
{
  // TODO: Update this function
  // The skynet socket for receiving dplanner status
  //  int dplannerStatusSocket = m_skynet.listen(SNdplannerStatus, MODdynamicplanner);
  //  DPlannerStatus* dplannerStatus = new DPlannerStatus();
  //  if(dplannerStatusSocket < 0)
  //    cerr << "TrafficPlanner::getDPlannerStatusThread(): skynet listen returned error" << endl;

  //  while(true)
  //  {
  //    bool dPlannerStatusReceived = RecvDPlannerStatus(dplannerStatusSocket, dplannerStatus);
  //    /* YOU NEED TO FIGURE OUT WHAT TO DO HERE */
  //    if (dPlannerStatusReceived)
  //    {
  //      DGClockMutex(&m_DPlannerStatusMutex);
  //      m_dplannerStatus = dplannerStatus;
  //      DGCunlockMutex(&m_DPlannerStatusMutex);      
  //    }
  //  }
}

void CTrafficPlanner::getSegGoalsThread()
{
  if(m_recvSegmentGoals)
    {
      SegGoals* segGoals = new SegGoals();
      while(true)
        {
          bool segGoalsReceived = RecvSegGoals(segGoalsSocket, segGoals);
          if (segGoalsReceived)
            {
              DGClockMutex(&m_SegGoalsMutex);
              m_segGoals.push_back(*segGoals);
              DGCunlockMutex(&m_SegGoalsMutex);
              cout << "seggoal received " << endl;            
            }
        }
    }
  else
    {
      //SegGoals* segGoals = new SegGoals();
      //TODO: fake segment goals here
    }
}
  
void CTrafficPlanner::getStaticCostMapRequestThread()
{
  // TODO: Put Jeremy's code here
}


void CTrafficPlanner::TPlanningLoop(void)
{

  cout << "Traffic State BEGIN LOOP..." << endl;
  m_tfac.print(m_currTrafficState->getType());

  cout << "beginning of planning loop" << endl;
  // Get send skynet sockets
  int segGoalsStatusSocket = m_skynet.get_send_sock(SNtplannerStatus);
  int rddfSocket = m_skynet.get_send_sock(SNrddf);
  int staticCostMapSocket = m_skynet.get_send_sock(SNtplannerStaticCostMap);

  // Confirm that we have joined seggoals group
  if(segGoalsStatusSocket < 0)
    cerr << "TPlanningLoop(): skynet get_send_sock returned error" << endl;
  SegGoalsStatus* segGoalsStatus = new SegGoalsStatus();
  segGoalsStatus->goalID = 0;
  segGoalsStatus->status = SegGoalsStatus::COMPLETED;
  bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
  if (!statusSent)
    {
      cout << "Error sending segGoals status to mplanner" << endl;
      cerr << "TPlanningLoop(): Error sending segGoals status to mplanner" << endl;
    } else 
      cout << "Successfully sent initial status" <<  endl;
  
  cout << "=================================================" << endl;
  cout << "in TrafficPlanner.cc line 239, added by Sam 03-08-07" << endl;
  cout << " initial state update" << endl;
  UpdateState(); // this gives m_state
  
  cout << " map prior delta = "<< localMap->prior.delta << endl;
  point2 statedelta(m_state.utmNorthing-m_state.localX,
                    m_state.utmEasting-m_state.localY);
  cout << " state delta = "<< statedelta << endl;
  cout << " delta difference = "<< statedelta-localMap->prior.delta << endl;
  point2 axledelta(DIST_REAR_AXLE_TO_FRONT,0);
  axledelta = axledelta.rot(m_state.localYaw);
  cout << " axle delta = " << axledelta << endl;
  
  //  localMap->prior.delta = statedelta+axledelta;
  localMap->prior.delta = statedelta;
  m_localMap->prior.delta = statedelta;
  
  cout << " New map prior delta = "<< localMap->prior.delta << endl;
  cout << "=================================================" << endl;
  sleep(2);
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // OUTER LOOP - runs continuously
  bool outerLoopFlag = true;
  if ((m_verbose) || (m_debug))
    {
      cout << "Entering outer planning loop..." << endl;
    }

  while(outerLoopFlag)
    {
      DGClockMutex(&m_SegGoalsMutex);
      unsigned numSegGoals = m_segGoals.size();
      DGCunlockMutex(&m_SegGoalsMutex);
    
      //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
      // robust startup
      while (numSegGoals == 0)
        {
          cout << "Waiting for segment goals" <<  endl;
          sleep(1);
          //segGoalsStatus->goalID = 0;
          //segGoalsStatus->status = SegGoalsStatus::COMPLETED;
          // Request seggoals from mission planner
          //bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
          DGClockMutex(&m_SegGoalsMutex);
          numSegGoals = m_segGoals.size();
          DGCunlockMutex(&m_SegGoalsMutex);
          cout << "numseggoals = " << numSegGoals << endl;
        }
      //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    
      //Read list of segment goals
      DGClockMutex(&m_SegGoalsMutex);
      list<SegGoals> segGoals = m_segGoals;
      m_segGoals.pop_front(); // discard the first goal
      DGCunlockMutex(&m_SegGoalsMutex);
      //    segGoals.front().print();
    
      //cout <<"segGoals.size =  " << segGoals.size() << endl;
    
      if (numSegGoals > 0) // DONT START PLANNING BEFORE I HAVE SOME GOALS
        {
          //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
          // INNER LOOP - planning loop
          if ((m_verbose) || (m_debug))
            {
              cout << "Entering inner planning loop..." << endl;
            }
          while(true) // inner loop
            {  
        
              // fix map and vehicle state for this planning cycle
              //DGClockMutex(&m_LocalMapMutex);
              ///localMap = m_localMap;
              //DGCunlockMutex(&m_LocalMapMutex);
              // TODO: use map updated in thread
              // set the global to local transformation
              m_gloToLocalDelta = localMap->prior.delta;
        
              cout << endl;
              cout << endl;
              cout << "updating state" << endl;
              UpdateState(); // this gives m_state
              cout << "current velocity = " << AliceStateHelper::getVelocityMag(m_state) << endl;
        
              if (!((segGoals.front().segment_type == SegGoals::END_OF_MISSION))) 
                {
                  // if not and end of MISSION, update traffic and control states
                  // Get waypoint label for exit point from current segment
                  PointLabel exitWayptLabel(segGoals.front().exitSegmentID, segGoals.front().exitLaneID, segGoals.front().exitWaypointID);
                  cout<< "Exit waypoint = " <<exitWayptLabel<<endl;
          
                  cout << "Traffic State BEFORE determineTrafficState()..." << endl;
                  m_tfac.print(m_currTrafficState->getType());
          
                  //  int lastType = m_currTrafficState->getType();
                  m_currTrafficState = m_trafStateEst->determineTrafficState(m_currTrafficState,localMap, m_state, exitWayptLabel);
                  //  m_currTrafficState=m_trafStateEst->getCurrentTrafficState();
          
                  cout << "Traffic State AFTER determineTrafficState()" << endl;
                  //    m_tfac.print(m_trafStateEst->getCurrentTrafficStateType());
                  m_tfac.print(m_currTrafficState->getType());
          
                  cout << "Determine Planning Horizion()" << endl;
                  m_tplannerControl->determinePlanningHorizon(m_currPlanHorizon,m_currTrafficState, segGoals);
          

                  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                  // run the control loop, which should return the corridor object to be sent to dplanner
          
                  // first see if we have completed a mission
                  cout<< "Checking if complete , exit waypoint "<< exitWayptLabel<<endl;
          
                  if (isComplete(exitWayptLabel, m_state))
                    {
                      cout << "GOAL " << segGoals.front().goalID << " COMPLETED!!!" << endl;
                      // send status to mplanner
                      segGoalsStatus->status = SegGoalsStatus::COMPLETED;
                      //stayInLoopFlag = false;
                      segGoalsStatus->goalID = segGoals.front().goalID;
                      bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
                      if (statusSent)
                        cout << "Successfully sent status to mplanner" << endl;
                      //if segment is completed
                      break;
                    }
          
                  cout << "Control State BEFORE determineControlState()..." << endl;
                  m_cfac.print(m_currControlState->getType());  
          
                  m_currControlState = m_tplannerControl->determineControlState(m_currControlState, m_currTrafficState, m_currPlanHorizon, localMap, m_state);
          
                  cout << "Control State AFTER determineControlState()..." << endl;
                  m_cfac.print(m_currControlState->getType());
                  cout << "distance into control state = " << m_currControlState->calcDistFromInitPos(m_state) << endl;
          
                  // determine the corridor
                  // initialize the ocpSpecs - initial conditions set to our current pos here. Also, mode initialized as fwd here - need to explicitly set only when going into reverse
                  corridor.initializeOCPparams(m_state, m_currPlanHorizon.getSegGoal(0).minSpeedLimit, m_currPlanHorizon.getSegGoal(0).maxSpeedLimit);

                  cout << "determine the corridor" << endl;

                  // ocpParams finalconditions get set inside this function
                  // also, the mode gets set (if necessary) in this function
                  m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);

                  cout << endl;
                  cout << "About to getRddfCorridor() " << endl;
                  RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);

                  // TODO: sort obstacles for hard and soft constraint implementation

                  // calculate the cost map based on the obstacles

          
                  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                  // Send this corridor to dplanner (rddf)
                  SendRDDF(rddfSocket,rddf);

                  // send the cost map to dplanner

                  // send the parameters
                  corridor.convertOCPtoGlobal(m_gloToLocalDelta);
                  bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
                  if (!sendOCPparamsErr){
                    cerr << "TrafficPlanner: problem with send OCP parameters" << endl;
                    cout << "TrafficPlanner: problem with send OCP parameters" << endl;
                  }

                } else 
                  { // now at the end of the mission
                    // do not update the traffic and control states, but use the previous control state to update the corridor
                    m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);
                    cout << "End of mission, bring alice to a complete stop (for now)" << endl;
                    // set the velocity to zero
                    vector<double> velProfile;
                    velProfile.push_back(0); // entry pt
                    velProfile.push_back(0); // exit pt
                    corridor.setVelProfile(velProfile);
                    corridor.setDesiredAcc(0);
          
                    RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
          
                    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // Send this corridor to dplanner
                    SendRDDF(rddfSocket,rddf);
          
                    cout << "GOAL " << segGoals.front().goalID << " COMPLETED!!!" << endl;
                    // send status to mplanner
                    segGoalsStatus->status = SegGoalsStatus::COMPLETED;
                    //stayInLoopFlag = false;
                    segGoalsStatus->goalID = segGoals.front().goalID;
                    bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
                    if (statusSent)
                      cout << "Signalled mplanner that MISSION COMPLETE!!" << endl;
          
                    // TODO: what does tplanner do while waiting for a new mission?
                    outerLoopFlag = false; // exit the outer loop
                    break;
                  }
        
              corridor.clear();
              usleep(100000); 
            } //WHILE STAY IN LOOP
      
        } // END: if (numOfSegGoals>0)
      else 
        {
          if (m_debug)
            {
              cout << "No segment goals yet ...  still waiting." << endl;
              cout << "SHOULD NOT GET HERE!!" << endl;
            }
          outerLoopFlag = true;
        }
      usleep(100000);
    }//WHILE STAY IN OUTER LOOP
}




bool CTrafficPlanner::isComplete(PointLabel exitWayptLabel, VehicleState vehState)
{
  double completeDist = 1;
  bool completed;
  point2 exitWaypt, currPos;
  int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
  currPos = AliceStateHelper::getPositionFrontBumper(vehState);
  double distance = exitWaypt.dist(currPos); 
  cout << exitWaypt << currPos << distance << endl;
  if(distance<=completeDist)
    {
      completed = true;
    }
  else 
    {
      completed = false;
    }
  return completed;
}

  
