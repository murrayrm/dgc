#include "SlowDown.hh"


SlowDown::SlowDown(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId, type)
  , m_desDecc(-0.5)
{
}

SlowDown::~SlowDown()
{
}

int SlowDown::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
  point2arr leftBound, rightBound;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;

  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2arr leftBound1, rightBound1;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);





  if(TrafficStateFactory::ROAD_REGION == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    double range = 50;
    int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
    if (getBoundsErr!=0){
      cerr << "LaneKeeping.cc: boundary read from map error" << endl;
      return (getBoundsErr);
    }  

    // int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    //int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    //if (rightBoundErr!=0){
    //  cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
    //  return (rightBoundErr);
    //}     
    //if (leftBoundErr!=0){
    //  cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
    //  return (leftBoundErr); 
    // }

    // insert a pt at the obstacle, and a distance of 10 m (m_distToObs from LaneKeepingToSlowDown) and with zero offset
    point2 tmppt = localMap->getObstaclePoint(currFrontPos, 10);
    int errInsLeftProj = TrafficUtils::insertProjPtInBoundary(leftBound1, tmppt);
    int errInsRightProj = TrafficUtils::insertProjPtInBoundary(rightBound1, tmppt);
    tmppt = localMap->getObstaclePoint(currFrontPos, 0);
    errInsLeftProj = TrafficUtils::insertProjPtInBoundary(leftBound1, tmppt);
    errInsRightProj = TrafficUtils::insertProjPtInBoundary(rightBound1, tmppt);


    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int err = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
    int index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
        leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
        rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // here I want to specify the acc, and calculate the distance function
    // TODO: think through the vel profile specification some more.
    double initVel = getInitialVelocity();
    cout << "slow down initial velocity = " << initVel << endl;
    double cStateLength = -pow(initVel,2)/(2*m_desDecc);
    cout << "slowdown corridor length = " << cStateLength << endl;
    double distIntoControlState = calcDistFromInitPos(vehState);
    //velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState;
    velIn = initVel + ((0-initVel)/cStateLength)*distIntoControlState;
    cout << "velIn = " << velIn << endl;
    velOut = 0;
    acc = m_desDecc;

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];






  } else if (TrafficStateFactory::APPROACH_INTER_SAFETY == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    cout << "Slowdown ais" << endl;
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    double range = 50;
    int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
    if (getBoundsErr!=0){
      cerr << "LaneKeeping.cc: boundary read from map error" << endl;
      return (getBoundsErr);
    }  

    //int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    //int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    //if (rightBoundErr!=0){
    //  cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
    //  return (rightBoundErr);
    //}     
    //if (leftBoundErr!=0){
    //  cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
    //  return (leftBoundErr);
   //}
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
        leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
        rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // here I want to specify the acc, and calculate the distance function
    // TODO: think through the vel profile specification some more.
    double initVel = getInitialVelocity();
    cout << "slow down initial velocity = " << initVel << endl;
    double cStateLength = -pow(initVel,2)/(2*m_desDecc);
    cout << "slowdown corridor length = " << cStateLength << endl;    
    double distIntoControlState = calcDistFromInitPos(vehState);
    //velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState;
    velIn = initVel + ((0-initVel)/cStateLength)*distIntoControlState;
    velOut = 0;
    acc = m_desDecc;

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  }
  
  cout << "leftBound.size()" << leftBound.size() << endl;
  cout << "rightBound.size()" << rightBound.size() << endl;
  cout << "velocity profile: in = " << velIn << " and out = " << velOut << endl;
  cout << "specified acceleration = " << acc <<  endl;
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector<double> velProfile;
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  //        cout << "printing FC's" << endl;
  //  for (int ii=0; ii<6 ; ii++) {
  //    cout << "FCLB[" << ii << "] = " << corr.getOCPparams().finalConditionLB[ii] << endl;
  //  }
  //  for (int ii=0; ii<6 ; ii++) {
  //    cout << "FCLB[" << ii << "] = " << corr.getOCPparams().finalConditionUB[ii] << endl;
  //   }

  return 0;
}
