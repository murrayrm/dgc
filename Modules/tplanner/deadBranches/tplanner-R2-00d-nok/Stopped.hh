#ifndef STOPPED_HH_
#define STOPPED_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class Stopped : public ControlState {

public: 

  Stopped(int stateId, ControlStateFactory::ControlStateType type);
  ~Stopped();
  virtual int determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* map);

  bool getUTurnBool();
  bool getChangeLaneLeftBool();
  bool getChangeLaneRightBool();
  void resetConditions();

private:
  bool m_uturn;
  bool m_changelane_left;
  bool m_changelane_right;

};
#endif /*STOPPED_HH_*/
