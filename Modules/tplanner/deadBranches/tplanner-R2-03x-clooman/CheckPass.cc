#include "CheckPass.hh"

using namespace std;

void CheckPass::checkForObstacles(VehicleState vehState, Map* localMap, LaneLabel lane,int searchDirection, double& distance_obstacle,double& vel_obstacle, double& size_obstacle)
{
  // Calculate Alice position of Front Bumper
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);

  // Get Alice's lane information
  LaneLabel lane_alice;
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);
  localMap->getLane(lane_alice,position_alice);
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  vector<MapElement> obstacle;
  localMap->getObsInLane(obstacle,lane);
  distance_obstacle=INFINITY;
  point2 position_obstacle;
  int id_obstacle=-1;
  bool blockLane;

  double distance_temp;
  // Loop through all obstacles found in lane and find the one with the lowest distance
  for (unsigned int i=0;i<obstacle.size();i++)
    {
      if (obstacle[i].type!=ELEMENT_OBSTACLE && obstacle[i].type!=ELEMENT_VEHICLE) continue;

      // Transform from uncertain to certain position
      position_obstacle.set(obstacle[i].position);
      localMap->getDistAlongLine(distance_temp,centerline_alice,position_obstacle,position_alice);

      // Check whether obstacle block lane.
      blockLane=TrafficUtils::isObstacleBlockingLane(obstacle[i],localMap,lane);
      cout<<"Lane is partially blocked..."<<blockLane<<endl;

      // Find lowest distance. If obstacle doesn't block lane, consider it as it is not present
      if (((searchDirection==AHEAD && distance_temp>0) || (searchDirection==BEHIND && distance_temp<0)) && fabs(distance_temp)<distance_obstacle && blockLane)
	{
	  distance_obstacle=fabs(distance_temp);
	  id_obstacle=i;
	}
    }
  size_obstacle=6;
  vel_obstacle=0;

  if (id_obstacle!=-1)
    {
      if (obstacle[id_obstacle].length>obstacle[id_obstacle].width)
	size_obstacle=obstacle[id_obstacle].length;
      else
	size_obstacle=obstacle[id_obstacle].width;

      vel_obstacle=sqrt(pow(obstacle[id_obstacle].velocity.x,2)+pow(obstacle[id_obstacle].velocity.y,2));

      // The distance needs to be corrected to obtain the bumper2bumper distance
      distance_obstacle=distance_obstacle-size_obstacle/2;
    }
  else
    distance_obstacle=-1;
}


double CheckPass::getProbability(VehicleState vehState,Map* localMap)
{
  const int SAFETY_FACTOR=20;
  const double size_alice=VEHICLE_LENGTH;

  // COMPUTE ALICE'S VALUES
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);

  // Calculate Velocity over Ground
  double vel_alice=sqrt(vehState.utmNorthVel*vehState.utmNorthVel + vehState.utmEastVel*vehState.utmEastVel);
  // Get Alice's lane
  LaneLabel lane_alice;
  point2arr centerline_alice;
  localMap->getLane(lane_alice,position_alice);

  // Get general lane information whether to turn legally or illegaly
  vector<LaneLabel> SameDirLanes;
  bool LegalTurn;
  localMap->getSameDirLanes(SameDirLanes,lane_alice);
  if (SameDirLanes.size()>1)
    {
      LegalTurn=true;
      Log::getStream(9)<<"Legal passing possible...true"<<endl;
    }
  else
    {
      Log::getStream(9)<<"Legal passing possible...false"<<endl;
      LegalTurn=false;
    }

  // If illegal turn is necessary, check for oncoming traffic
  if (!LegalTurn)
    {

      // COMPUTE OBSTACLE1'S VALUES
      double distance_obstacle1;
      double vel_obstacle1;
      double size_obstacle1;
      checkForObstacles(vehState,localMap,lane_alice,AHEAD,distance_obstacle1,vel_obstacle1,size_obstacle1);
      // if no obstacle was found, return probability of .70
      if (distance_obstacle1==-1) return .70;

      // COMPUTE OBSTACLE2'S VALUES, this is the obstacle in the opposite lane
      double distance_obstacle2;
      double vel_obstacle2;
      double size_obstacle2;
      LaneLabel lane_opposite;
      localMap->getNeighborLane(lane_opposite,lane_alice,-1);
      checkForObstacles(vehState,localMap,lane_opposite,AHEAD,distance_obstacle2,vel_obstacle2,size_obstacle2);
      // if no obstacle was found, return probability of .70
      if (distance_obstacle2==-1) return .70;

      double vel_difference=vel_alice-vel_obstacle1;
      // If obstacle in front of Alice is faster than Alice, return 0; this case should never happen as Alice
      // should not pass moving obstacles
      if (vel_difference<0) return 0;

      // the necessary time to pass a vehicle has two components:
      // 1) time to pass obstacle and be max 3*length of Alice ahead to make a safe lanechange back into own lane
      // 2) Uncertainties
      double corridor_alice=distance_obstacle1+size_obstacle1+3*size_alice;
      double t_1 = sqrt(2*corridor_alice/(0.7*VEHICLE_MAX_ACCEL));
      double t_2 = 0.5;
      double t_necessary=t_1+t_2;

      // The length of the obstacle's corridor is computed by computing the distance that the obstacle drives in the time
      // that Alice needs to pass it
      double corridor_obstacle2=vel_obstacle2*t_necessary;

      // Compute how much of the corridor on the opposite lane is left when Alice would pass the obstacle
      double corridor_left=distance_obstacle2-corridor_obstacle2-corridor_alice;
      // Output some results
      Log::getStream(9)<<"Vehicle Speed..."<<vel_alice<<endl;
      Log::getStream(9)<<"Position Alice..."<<position_alice<<endl;
      Log::getStream(9)<<"Obstacle Distance own lane..."<<distance_obstacle1<<endl;
      Log::getStream(9)<<"Obstacle velocity own lane..."<<vel_obstacle1<<endl;
      Log::getStream(9)<<"Obstacle Distance opposite lane..."<<distance_obstacle2<<endl;
      Log::getStream(9)<<"Obstacle velocity opposite lane..."<<vel_obstacle2<<endl;
      Log::getStream(9)<<"necessary passing time..."<<t_necessary<<endl;
      Log::getStream(9)<<"Corridor length Alice..."<<corridor_alice<<endl;
      Log::getStream(9)<<"Corridor length obstacle2..."<<corridor_obstacle2<<endl;
      Log::getStream(9)<<"Corridor left..."<<corridor_left<<endl;

      if (corridor_left<0)
	{
	  return 0;
	}
      else
	{
	  double ratio=(corridor_left/size_alice*SAFETY_FACTOR)/100;
	  Log::getStream(9)<<"Ratio..."<<ratio<<endl;
	  return (1 < ratio)?1:ratio;
	}
    } 
  // Check for traffic on neighbor lanes check whether lanes are free
  else
    {
       for (unsigned int i=0;i<SameDirLanes.size();i++)
 	{
  	  if (SameDirLanes[i].lane!=lane_alice.lane)
  	    {
	      // COMPUTE OBSTACLE1'S VALUES
	      double distance_obstacle1;
	      double vel_obstacle1;
	      double size_obstacle1;
	      checkForObstacles(vehState,localMap,SameDirLanes[i],BEHIND,distance_obstacle1,vel_obstacle1,size_obstacle1);
	      // if no obstacle was found, return probability of .70
	      if (distance_obstacle1==-1) return .70;

	      if ((vel_obstacle1>0) && (distance_obstacle1/vel_obstacle1)<5)
		return 0;
	      else if ((vel_obstacle1>0) && (distance_obstacle1/vel_obstacle1)>10)
		return .70;
	      else
		return 1;

	      // Output some results
	      Log::getStream(9)<<"Vehicle Speed..."<<vel_alice<<endl;
	      Log::getStream(9)<<"Position Alice..."<<position_alice<<endl;
	      Log::getStream(9)<<"Obstacle Distance neighbor lane..."<<distance_obstacle1<<endl;
	      Log::getStream(9)<<"Obstacle velocity neighbor lane..."<<vel_obstacle1<<endl;
  	    }
 	}
    }
  return -1;
 }









