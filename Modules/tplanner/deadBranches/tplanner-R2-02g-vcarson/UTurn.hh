#ifndef UTURN_HH_
#define UTURN_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class UTurn:public ControlState {

  public:

    UTurn(int stateId, ControlStateFactory::ControlStateType type, int stage);
    ~UTurn();
    virtual int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
    static void resetParameters();
    
    /* We have to define those static because they are shared among all the UTurn instances
       A fix for this would be to save the initial vehicule state rather than the initial position
       and also save the initial lane segment */
    static point2 m_obsPos;
    static point2 m_rearBound;
    static bool m_firstRun;
    static point2 m_ptStage1;
    static point2 m_ptStage2;
    static point2 m_ptStage3;
    static point2 m_ptStage4;

  ControlState* newCopy() { return (ControlState*) new UTurn(*this); }
  private:
  
    int m_stage;

};

#endif                          /*UTURN_HH_ */
