#ifndef STOPPEDTOPASSING_LC_HH_
#define STOPPEDTOPASSING_LC_HH_

#include "ControlStateTransition.hh"

class StoppedToPassing_LC:public ControlStateTransition {

public:
  StoppedToPassing_LC(ControlState *state1, ControlState *state2);
  StoppedToPassing_LC();
  ~StoppedToPassing_LC();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
