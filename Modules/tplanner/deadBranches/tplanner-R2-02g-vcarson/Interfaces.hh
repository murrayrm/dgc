#ifndef TRAFFICPLANNERINTERNALINTERFACES_HH
#define TRAFFICPLANNERINTERNALINTERFACES_HH

#include "gcinterfaces/GcModuleInterfaces.hh"
#include "TrafficState.hh" 
#include "ControlState.hh" 
#include "PlanningHorizon.hh" 
#include "interfaces/VehicleState.h"

enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };


struct TrafficManagerResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() const { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};


struct CorrGenResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() const { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct CorridorGen : public GcTransmissive
{
  enum DirectiveName{CREATE, PAUSE, END_OF_MISSION};

  unsigned int ID;

  /* id of the corresponding mplanner goal id */ //TODO we may need a list here based on the planning horizon 
  vector<unsigned int> m_segGoalIds;

  DirectiveName name;

  /* Parameters */

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;

  TrafficState* traffState; 
  ControlState* controlState; 
  PlanningHorizon planHorizon; 

  CorridorGen()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~CorridorGen()
  {
    delete traffState; 
    delete controlState; 
  }

  unsigned int getDirectiveId() const {
    return ID;
  }

  string toString() const {
    return "CorridorGen";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & ID;
  }

};

struct CorridorGenStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ INVALID_MAP_BDRY };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId()const { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct CorridorCreate : public GcTransmissive
{
  enum DirectiveName{CREATE, PAUSE, END_OF_MISSION};

  unsigned int ID;

  /* id of the corresponding mplanner goal id */ //TODO we may need a list here based on the planning horizon 
  vector<unsigned int> m_segGoalIds;

  DirectiveName name;

  /* Parameters */

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;

  TrafficState* trafficState; 
  ControlState* controlState; 
  PlanningHorizon planHorizon; 
  VehicleState vehState; 

   CorridorCreate()
   :ID(666)
   , exitSegmentID(666) 
   , exitLaneID(666)
   , exitWaypointID(666)  
   , trafficState(0)
   , controlState(0)
  {

  }

  CorridorCreate(const CorridorCreate &cc)
  {
  
    ID = cc.ID;
    name = cc.name;
    m_segGoalIds = cc.m_segGoalIds; 
    exitSegmentID = cc.exitSegmentID; 
    exitLaneID = cc.exitLaneID;
    exitWaypointID = cc.exitWaypointID; 
    cout<<"CORRCREATE = cc.exitLaneID "<<cc.exitLaneID;
    cout<<"CORRCREATE = cc.exitWaypointID "<<cc.exitWaypointID;
    cout<<"CORRCREATE = cc.trafficState "<<cc.trafficState;
    if (0 != cc.trafficState) {
      trafficState =  cc.trafficState->newCopy(); 
    }else {trafficState = 0;}
    cout<<"CORRCREATE " <<cc.controlState<<endl; 
    cout<<"CORRCREATE " <<cc.toString()<<endl; 
    if (0 != cc.controlState) {
      controlState = cc.controlState->newCopy();
    } else {controlState = 0;}
    planHorizon = PlanningHorizon(cc.planHorizon); 
    vehState = cc.vehState;
  }

  CorridorCreate& operator=(const CorridorCreate &cc)
  {
    ID = cc.ID;
    name = cc.name;
    m_segGoalIds = cc.m_segGoalIds; 
    exitSegmentID = cc.exitSegmentID; 
    exitLaneID = cc.exitLaneID;
    exitWaypointID = cc.exitWaypointID;
    cout<<"CORRCREATE - assignment op trafficState " <<cc.trafficState<<endl; 
    cout<<"CORRCREATE - assignment op controlState " <<cc.controlState<<endl; 
    if (0 == trafficState) {
      trafficState =  cc.trafficState?cc.trafficState->newCopy():0; 
    } else if( cc.trafficState ) {
      *trafficState = *cc.trafficState; 
    }
    if (0 == controlState) { 
      controlState = cc.controlState?cc.controlState->newCopy():0; 
    } else if(cc.controlState) {
      *controlState = *cc.controlState; 
    }
    planHorizon = PlanningHorizon(cc.planHorizon); 
    vehState = cc.vehState;
    return (*this);
  }

  ~CorridorCreate()
  {
    if (0 != trafficState) {
      delete trafficState; 
    }
    if (0 != controlState) {
      delete controlState; 
    }
  }

  unsigned int getDirectiveId() const {
    return ID;
  }

  string toString() const {
    return "CorridorCreate";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & ID;
    ar & exitSegmentID;
    ar & exitLaneID;
    ar & exitWaypointID;
  }

};

struct CorridorCreateStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ INVALID_MAP_BDRY };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId()const  { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryCreate : public GcTransmissive
{

  enum Directive{CREATE, PAUSE, END_OF_MISSION};

  unsigned int id;

  Directive name; 

  // Parameters

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;

  TrajectoryCreate()
    : id(0)
    , exitSegmentID(0) 
    , exitLaneID(0) 
    , exitWaypointID(0)

  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryCreate()
  {
  }

  unsigned int getDirectiveId() const {
    return id;
  }

  string toString() const {
    return "TrajectoryCreate";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & id;
    ar & exitSegmentID;
    ar & exitLaneID;
    ar & exitWaypointID;
  }

};

struct TrajectoryCreateStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId()const { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryPause : public GcTransmissive
{
  unsigned int ID;

  // Parameters
  PointLabel exitPtLabel;

  TrajectoryPause()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryPause()
  {
  }

  unsigned int getDirectiveId() const {
    return ID;
  }

  string toString() const {
    return "TrajectoryPause";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & ID;
  }

};

struct TrajectoryPauseStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() const { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryEndMission : public GcTransmissive
{
  unsigned int ID;

  // Parameters
  PointLabel exitPtLabel;

  TrajectoryEndMission()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryEndMission()
  {
  }

  unsigned int getDirectiveId() const {
    return ID;
  }

  string toString() const {
    return "TrajectoryEndMission";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & boost::serialization::base_object<GcTransmissive>(*this);
    ar & ID;
  }

};

struct TrajectoryEndMissionStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId()const { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

#endif //TRAFFICPLANNERINTERNALINTERFACES_HH
