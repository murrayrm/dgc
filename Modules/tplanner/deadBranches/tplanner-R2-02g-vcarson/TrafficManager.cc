#include "TrafficManager.hh"

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile)
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_snKey(skynetKey)
    , m_uniqueMergedDirID(0)
    , m_prevMergedDirectiveID(0)
    , m_isEndMission(false)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_logData(log)
    , m_isInit(false)
    , m_latestID(0)
    , m_currentID(0)
{

  m_controlGraph = ControlStateFactory::createControlStates();

  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionTrafficInterface::generateNorthface(skynetKey, this);

  if (true == log) {
    m_logger = new Log();
    m_logger->init("TRAFFMGR", true);
  }

  if (useRNDF) {
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, true, RNDFFile);
  } else { //TODO FIX this si not cool 
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, false, "");
  }
  this->setLogLevel(9);
  this->addLogfile("TrafficManager.log");
  m_currContrState = ControlStateFactory::getInitialState();
}

TrafficManager::~TrafficManager()
{

  MissionTrafficInterface::releaseNorthface(m_missTraffInterfaceNF);
  //TODO close file before deleting
  delete m_logFile;
  delete m_logger;
  TrafficStateEst::Destroy();
}

void TrafficManager::TrafficPlanningLoop(void)
{
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective );
      control(&m_controlStatus, &m_mergedDirective );
      usleep(100000);
    }
}

void TrafficManager::arbitrate(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus *controlStatus = dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective *mergedDirective = dynamic_cast<TrafficManagerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus; 

  bool meetsPlanHorizReqs = false;
  PointLabel exitWayptLabel; 


  if (!m_isInit) {
    SegGoalsStatus segGoalsStatus;
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    
    cout << "Sending intializing status goal " << segGoalsStatus.goalID << endl;
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(5);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      cout << "Sending status goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(5);
    }
    cout << "Successfully notified mission planner that I'm joining the group" <<  endl;
    m_isInit = true;
  }
  

  cout<<"TRAFFMGR.arbitrate()"<<endl; 
  /* If we are executing, then do not try to get another directive */
  //  if (controlStatus->status == TrafficManagerControlStatus::EXECUTING) {
  //if (m_logData)
  //  {
  //	fprintf( m_logFile, "  Control status: goalID = %d\t status = %d\n", controlStatus->ID, controlStatus->status );
  //  }
  // return;
  //} //end EXECUTING
  
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == TrafficManagerControlStatus::COMPLETED ||
      controlStatus->status == TrafficManagerControlStatus::FAILED) {
    
    if (controlStatus->status == TrafficManagerControlStatus::COMPLETED) {  
      for (unsigned int i=0; i < controlStatus->getSegGoalsSize(); i++) {
	segGoalsStatus.status = SegGoalsStatus::COMPLETED;
	segGoalsStatus.goalID = controlStatus->segGoalsIDs.front();
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
	controlStatus->segGoalsIDs.pop_front();
      }
      /* Now we must pop the previously merged dir off the queue, we can assume it's the front of the queue */
      if (controlStatus->ID == m_prevMergedDirective.id) {
	//m_contrDirectiveQ.pop_front();
	/* Do something to continue */
      } else {
	/* We have a problem, we didn't get a status back for this id so we won't mind resending in control */
      }

    } else if (controlStatus->status == TrafficManagerControlStatus::FAILED)  {
      
      for (unsigned int i=0; i < controlStatus->getSegGoalsSize(); i++) {
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	segGoalsStatus.goalID = controlStatus->segGoalsIDs.front();
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
	controlStatus->segGoalsIDs.pop_front();
      }     
   
      /* Flush out the NF queue and send failure status */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
	SegGoals newDirective;
	m_missTraffInterfaceNF->getNewDirective( &newDirective );
	segGoalsStatus.goalID = newDirective.goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      }
  
      /* Flush out the accumulating segGoals control queue and send failure status */
      while (m_accSegGoalsQ.size() > 0) {
	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	m_accSegGoalsQ.pop_front();
      }
    }
  }

  /* Wait for Route Planner to send a new directive */
  usleep(500000);

  /* Get all the new directives and put it in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    cout<<"TRFMGR - arbitrate has new directive "<<endl;
    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);

    cout<<"TRFMGR - arbitrate has new directive goalID "<<newDirective.goalID<<endl;
    cout<<"TRFMGR - arbitrate has new directive segment ID "<<newDirective.exitSegmentID<<endl;
    cout<<"TRFMGR - arbitrate has new directive lane ID "<<newDirective.exitLaneID<<endl;
    cout<<"TRFMGR - arbitrate has new directive waypoint ID "<<newDirective.exitWaypointID<<endl;

    if (SegGoals::PAUSE == newDirective.segment_type) {
      cout<<"TRFMGR - arbitrate new directive is PAUSE "<<endl;
      SegGoalsStatus segGoalsStatus;

      mergedDirective->id = getNextUniqueMergedDirID();

      mergedDirective->segType = SegGoals::PAUSE;
      mergedDirective->segGoalsIDs.push_back(newDirective.goalID);
      mergedDirective->exitSegmentID = newDirective.exitSegmentID;
      mergedDirective->exitLaneID = newDirective.exitLaneID;
      mergedDirective->exitWaypointID = newDirective.exitWaypointID;


      /* First flush the queue where the seg goals are accumulating */
      while (m_accSegGoalsQ.size()> 0) {
	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	m_accSegGoalsQ.pop_front();
      }

    } else if (SegGoals::PAUSE != newDirective.segment_type) {

      cout<<"TRFMGR - arbitrate new directive is not PAUSE "<<endl;
      

      /* Push back onto m_accSegGoalsQ */
      if (m_accSegGoalsQ.size() > 0) {
	cout<<"TRFMGR - m_accSegGoalsQ is > 0 "<<endl;
	if (newDirective.goalID > m_accSegGoalsQ.back().goalID) {
	  cout<<"TRFMGR - goalID > accSegGoalsQ.back().goalID "<<endl;
	  m_accSegGoalsQ.push_back(newDirective);
	} else {
	  cout<<"TRFMGR - already  received a goal with ID" << newDirective.goalID<<endl; 
	  cout<<"TRFMGR - sent REJECTED status to Route Planner" << endl;
	  segGoalsStatus.goalID = newDirective.goalID; 
	  segGoalsStatus.status = SegGoalsStatus::REJECTED;
	  m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	}
      } else {
	m_accSegGoalsQ.push_back(newDirective);
	cout<<"TRFMGR - pushed back new directive on accSegGoalsQ accSegGoalsQ "<<endl;
      }
      

      cout<<"TRAFMGR: about to determine TrafficState"<<endl; 
 
     /* Determine traffic state */
      m_currTraffState = m_traffStateEst->determineTrafficState(exitWayptLabel);

      cout<<"TRAFMGR: trafficState is "<<m_currTraffState->toString()<<endl; 

      /* Update the current planning horizon and populate m_planHoriz */
      meetsPlanHorizReqs = determinePlanningHorizon(m_accSegGoalsQ);      

      cout<< "TRAFMGR: determined planning horizon"<<endl;

      /* Do not create a new merged directive until we have the correct planning horizon */
      if(meetsPlanHorizReqs) { /* if we had enough segGoals to be able to plan properly */
	m_currMergedPlanHoriz = m_currPlanHorizon;  

	cout<<"TRAFMGR: meets planning horizon requirements"<<endl;

	for (int i = 0; i < m_currPlanHorizon.getSize(); i++) {
	  mergedDirective->id = getNextUniqueMergedDirID();
	  cout<<"TRFMGR - size of m_currPlanHoriz "<<m_currPlanHorizon.getSize()<<endl;
	  mergedDirective->segGoalsIDs.push_back(m_currPlanHorizon.getSegGoal(i).goalID);
	}

	/*TODO take out print */
	for (int i = 0; i < m_currPlanHorizon.getSize(); i++) {
	  cout<<"TRFMGR.arbitrate()- m_currPlanHoriz goalID"<<m_currPlanHorizon.getSegGoal(i).goalID;
	  cout<<"TRFMGR.arbitrate() - m_currPlanhoriz, segment ID"<<m_currPlanHorizon.getSegGoal(i).exitSegmentID<<endl;
	  cout<<"TRFMGR.arbitrate() - m_currPlanhoriz, lane ID"<<m_currPlanHorizon.getSegGoal(i).exitLaneID<<endl;
	  cout<<"TRFMGR.arbitrate() - m_currPlanhoriz, waypoint ID"<<m_currPlanHorizon.getSegGoal(i).exitWaypointID<<endl;
	  mergedDirective->segGoalsIDs.push_back(m_currPlanHorizon.getSegGoal(i).goalID);
	}

	/* want to flush the the queue where we are accumulating seggoals */
	m_accSegGoalsQ.clear();
      } else { /* If the plan horiz is not met  */

      }
    }
  }
  //TODO implement if the queue gets too large
}

void TrafficManager::control(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus* controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective* mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);
  
  PointLabel endMissionWayptLabel, exitWayptLabel;
  Map* localMap = m_traffStateEst->getMapAtLastEst();
  
  cout<<"TRAFFMGR.control() mergedDirective type  "<<mergedDirective->segType<<endl; 
  cout<<"TRAFFMGR.control() mergedDirective ID  "<<mergedDirective->id<<endl; 
  if (SegGoals::PAUSE == mergedDirective->segType) {
    cout<<"TRAFFMGR.control() Got PAUSE directive"<<endl;

    m_prevMergedDirective.id = mergedDirective->id;
    
    /* Get into the PAUSE control action, once we get stauts back of successful PAUSE, then we reinitialize the control action*/
    m_currContrState =  ControlStateFactory::getPauseState();

    cout<<"TRAFFMGR.control(): control state "<<m_currContrState->toString()<<endl; 

    /* NOW send Pause to PLANNER */

  } else {
    
    if (m_prevMergedDirectiveID < mergedDirective->id) {
      if ((SegGoals::END_OF_MISSION != mergedDirective->segType) 
	  && (SegGoals::PAUSE != mergedDirective->segType))  {
	
	cout<<"TRAFFMGR: Got Dir other than  PAUSE directive"<<endl;
	
	/* Update the current control action */
	m_currContrState = determineControlAction();
	
	cout<<"TRAFFMGR: determined control action "<< m_currContrState->toString()<<endl;

	/* Set the previous merged directive member */
	m_prevMergedDirectiveID = mergedDirective->id;
	
	m_currCorr.initializeOCPparams(m_traffStateEst->getUpdatedVehState(), 
				     m_currMergedPlanHoriz.getSegGoal(0).minSpeedLimit, 
				     m_currMergedPlanHoriz.getSegGoal(0).maxSpeedLimit);

	/* Determine the corridor */
	cout<<"CORRGEN,control(): control state "<<m_currContrState->toString()<<endl; 

	VehicleState vehState = m_traffStateEst->getVehStateAtLastEst();
	Map* localMap = m_traffStateEst->getMapAtLastEst();
	m_currContrState->determineCorridor(m_currCorr, vehState, m_currTraffState, m_currMergedPlanHoriz, m_traffStateEst->getUpdatedMap());

	/* Set the velocity profile */
	m_currCorr.setVelProfile(localMap, vehState, m_currTraffState, m_currContrState, m_currMergedPlanHoriz);

	/* Get the global to local delta */
	m_gloToLocalDelta = m_traffStateEst->getUpdatedMap()->prior.delta;

	/* Now prepare parameters for sending */
	
	/* Convert to RDDF */
	RDDF* rddf = m_currCorr.getRddfCorridor(m_gloToLocalDelta);
	
	/* Convert to polytope representation */
	m_currCorr.generatePolyCorridor();
	m_currCorr.printPolyCorridor();

	/* Now cost map? */

	//corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state.localX, m_state.localY, localMap);      
	//CDeltaList* deltaList = NULL;
	//unsigned long long timestamp;
	//DGCgettime(timestamp);
	//deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
	//m_costMap->resetDelta<double>(m_costLayerID);
	m_currCorr.convertOCPtoGlobal(m_gloToLocalDelta);

	/* Send all parameters to trajectory planner */
	m_currCorr.sendPolyCorridor();
	//bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
	//SendMapdelta(staticCostMapSocket, deltaList);


      } else if (SegGoals::END_OF_MISSION == mergedDirective->segType) {
	m_isEndMission = true;

	/* TODO: Get the correct way point */
	/* Get the exit waypoint from the seg goal preceding end of mission */
	//endMissionWayptLabel = PointLabel(m_contrDirectiveQ.front().exitSegmentID, 
	//			  m_contrDirectiveQ.front().exitLaneID,  
	//				  m_contrDirectiveQ.front().exitWaypointID);
	//cc.exitPtLabel = endMissionWayptLabel; 
      } 
      
    }          
    if (m_logData)
      {

      }      
  }
}

    
ControlState* TrafficManager::determineControlAction() 
{
  ControlState* controlState;

  /* Get the vehicle state and the map at the time of the last traffic state estimation */
  VehicleState vehState = m_traffStateEst->getVehStateAtLastEst();
  Map* localMap = m_traffStateEst->getMapAtLastEst();

  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(m_currContrState); 

  unsigned int i = 0;
  ControlStateTransition* transition;


  /*TODO take out print */
  for (int i = 0; i < m_currPlanHorizon.getSize(); i++) {
    cout<<"TRFMGR.control()- m_currPlanHoriz goalID"<<m_currPlanHorizon.getSegGoal(i).goalID;
    cout<<"TRFMGR.control() - m_currPlanhoriz, entry segment ID"<<m_currPlanHorizon.getSegGoal(i).entrySegmentID<<endl;
    cout<<"TRFMGR.control() - m_currPlanhoriz, entry lane ID"<<m_currPlanHorizon.getSegGoal(i).entryLaneID<<endl;
    cout<<"TRFMGR.control() - m_currPlanhoriz, entry waypoint ID"<<m_currPlanHorizon.getSegGoal(i).entryWaypointID<<endl;
    cout<<"TRFMGR.control() - m_currPlanhoriz, exit segment ID"<<m_currPlanHorizon.getSegGoal(i).exitSegmentID<<endl;
    cout<<"TRFMGR.control() - m_currPlanhoriz, exit lane ID"<<m_currPlanHorizon.getSegGoal(i).exitLaneID<<endl;
    cout<<"TRFMGR.control() - m_currPlanhoriz, exit waypoint ID"<<m_currPlanHorizon.getSegGoal(i).exitWaypointID<<endl;
  }

  for (int i=0; i < transitions.size(); i++) 
{		
  transition = static_cast<ControlStateTransition*> (transitions[i]);
  transition->meetTransitionConditions(m_currContrState, m_currTraffState, m_currPlanHorizon, localMap, vehState);
}
  
  controlState = chooseMostProbableControlState(m_currContrState, transitions);
  
  if (m_currContrState->getType() == controlState->getType())
    {
      controlState = m_currContrState;
    } else {
      VehicleState currVehState = m_traffStateEst->getVehStateAtLastEst();
      controlState->setInitialPosition(currVehState);
      controlState->setInitialVelocity(currVehState);
      controlState->setInitialTime();
    }
  
  return controlState;
}


ControlState* TrafficManager::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
  ControlState* currControlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficManager::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
      currControlState = controlState; 
    }
  } else { 
    if ((m_verbose) || (m_debug)){  
      cout << "in TrafficManager::chooseMostProbable... received transitions of size " << transitions.size() << endl;
    }
  }
  
  return currControlState;
}


bool TrafficManager::determinePlanningHorizon(deque<SegGoals> currSegGoals) 
{
  TrafficStateFactory::TrafficStateType tStateType = 
    (TrafficStateFactory::TrafficStateType) m_currTraffState->getType();
  
  cout<<"About to clear planning horizon"<<endl; 

  m_currPlanHorizon.clear();
  
  bool meetsPlanHorizonReqs = false; 

  if (currSegGoals.size() > 0 ) {
    /* We need to make sure the currSegGoals have the correct size or else we will seg fault */
    switch(tStateType) {
    case TrafficStateFactory::ZONE_REGION:// want to plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
      }
      break;
    case TrafficStateFactory::ROAD_REGION:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
      }
      break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
      }
      break;
    case TrafficStateFactory::INTERSECTION_STOP:
      // plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	m_currPlanHorizon.addGoal(currSegGoals.front()); 		
	currSegGoals.pop_front();
      }
      break;
    default:
      break;
    };
    
    if ((m_verbose) || (m_debug)){  
      cout<<"SIZE OF CURRPLANHORIZON GOALS " <<  m_currPlanHorizon.getSize()<< endl;
      cout<<"Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
      cout<<"Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
    }
  }   
  return meetsPlanHorizonReqs;
}

unsigned int TrafficManager::getNextUniqueMergedDirID() {
  return ++m_uniqueMergedDirID; 
}


