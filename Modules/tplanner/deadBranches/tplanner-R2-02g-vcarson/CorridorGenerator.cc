#include "CorridorGenerator.hh"

CorridorGenerator::CorridorGenerator(int skynetKey, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile)
  : GcModule("CorridorGenerator", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_snKey(skynetKey)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_log(log)
    , m_latestID(0)
    , m_currentID(0)
    , m_isEndMission(false)
{

  m_traffCorrInterfaceNF = TrafficCorridorInterface::generateNorthface(skynetKey, this);

  m_corrTrajPlannerSF = CorrTrajPlannerInterface::generateSouthface(skynetKey, this);

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = CorrGenControlStatus::READY_FOR_NEXT;

  if (useRNDF) {
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, true, RNDFFile);
  } else { //TODO FIX this si not cool 
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, false, "");
  }
  this->setLogLevel(9);
  this->addLogfile("CorridorGenerator.log");

}

CorridorGenerator::~CorridorGenerator()
{
  TrafficCorridorInterface::releaseNorthface(m_traffCorrInterfaceNF);
  CorrTrajPlannerInterface::releaseSouthface(m_corrTrajPlannerSF);
  TrafficStateEst::Destroy();
}

void CorridorGenerator::CorridorGeneratorLoop(void)
{
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective);
      control(&m_controlStatus, &m_mergedDirective );
      usleep(100000);
    }
}

void CorridorGenerator::arbitrate(ControlStatus* cs, MergedDirective* md) {

  CorrGenControlStatus *controlStatus =
    dynamic_cast<CorrGenControlStatus *>(cs);
  CorrGenMergedDirective *mergedDirective =
    dynamic_cast<CorrGenMergedDirective *>(md);

  CorrGenControlStatus corrControlStatus; 

  CorridorCreateStatus corrCreateStatus; 

  PointLabel exitWayptLabel; 

  /* If we are executing, then do not try to get another directive */
  //  if (controlStatus->status == TrafficManagerControlStatus::EXECUTING) {
  //if (m_logData)
  //  {
  //	fprintf( m_logFile, "  Control status: goalID = %d\t status = %d\n", controlStatus->ID, controlStatus->status );
  //  }
  // return;
  //} //end EXECUTING
  
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == CorrGenControlStatus::COMPLETED ||
      controlStatus->status == CorrGenControlStatus::FAILED) {
    
    if (controlStatus->status == CorrGenControlStatus::COMPLETED) {  
      corrCreateStatus.status = CorridorCreateStatus::COMPLETED;
      corrCreateStatus.id = controlStatus->ID;
      m_traffCorrInterfaceNF->sendResponse( &corrCreateStatus );
      
      /* Now we must pop the previously merged dir off the queue, we can assume it's the front of the queue */
      if (controlStatus->ID == m_prevMergedDirective.ID) {
	/* Do something to continue */
      } else {
	/* We have a problem, we didn't get a status back for this id so we won't mind resending in control */
      }
      
    } else if (controlStatus->status == CorrGenControlStatus::FAILED) {
	corrCreateStatus.status = CorridorCreateStatus::FAILED;
	corrCreateStatus.id = controlStatus->ID;
	m_traffCorrInterfaceNF->sendResponse( &corrCreateStatus );
      }     
   
      /* Flush out the NF queue and send failure status */
      while (m_traffCorrInterfaceNF->haveNewDirective()) {
	CorridorCreate newDirective;
	m_traffCorrInterfaceNF->getNewDirective( &newDirective );
	corrCreateStatus.ID = newDirective.ID;
	corrCreateStatus.status = CorridorCreateStatus::FAILED;
	m_traffCorrInterfaceNF->sendResponse( &corrCreateStatus );
      }
  }

  /* Wait for Route Planner to send a new directive */
  usleep(500000);
  
  /* Get all the new directives and put it in the queue */
  while (m_traffCorrInterfaceNF->haveNewDirective()) {
     cout<<"CORRGEN - arbitrate has new directive "<<endl;
    CorridorCreate newDirective;
    m_traffCorrInterfaceNF->getNewDirective(&newDirective);
    
    cout<<"CORRGEN - newDirective name "<<newDirective.name<<endl;
    cout<<"CORRGEN - newDirective ID "<<newDirective.ID<<endl;

    if (CorridorCreate::PAUSE == newDirective.name) {
      cout<<"CORRGEN - arbitrate PAUSE directive "<<endl;
      mergedDirective->ID = newDirective.ID;
      mergedDirective->name = CorrGenMergedDirective::PAUSE;

    } else if (CorridorCreate::CREATE == newDirective.name) {
       cout<<"CORRGEN - arbitrate CREATE directive "<<endl;
       mergedDirective->ID = newDirective.ID;
       mergedDirective->name = CorrGenMergedDirective::CREATE;
       mergedDirective->controlState = newDirective.controlState; 
	cout<<"CORRGEN: newDirective.controlState "<<newDirective.controlState<<endl;
       mergedDirective->trafficState = newDirective.trafficState; 
       mergedDirective->planHorizon = newDirective.planHorizon; 
       mergedDirective->vehState = newDirective.vehState;
 
    } else if (CorridorCreate::END_OF_MISSION == newDirective.name) {
      cout<<"CORRGEN - arbitrate END OF MISSION directive "<<endl;
      mergedDirective->ID = newDirective.ID;
      mergedDirective->name = CorrGenMergedDirective::END_OF_MISSION;
      mergedDirective->exitSegmentID = newDirective.exitSegmentID; 
      mergedDirective->exitLaneID = newDirective.exitLaneID; 
      mergedDirective->exitWaypointID = newDirective.exitWaypointID; 
    }
  }
}

void CorridorGenerator::control(ControlStatus* cs, MergedDirective* md) {

  CorrGenControlStatus* controlStatus =
    dynamic_cast<CorrGenControlStatus *>(cs);
  CorrGenMergedDirective* mergedDirective =
    dynamic_cast<CorrGenMergedDirective *>(md);

  PointLabel endMissionWayptLabel, exitWayptLabel;
  Map* localMap = m_traffStateEst->getMapAtLastEst();
  TrafficState* trafficState = m_traffStateEst->getCurrentTrafficState();

  TrajectoryCreate tc; 
  TrajectoryCreateStatus tcStatus; 

  /* First check to see if the previous directive has been completed */
  if (m_corrTrajPlannerSF->isStatus(CorridorCreateStatus::COMPLETED, m_prevMergedDirective.ID)) {

      controlStatus->status = CorrGenControlStatus::COMPLETED;
      controlStatus->ID = m_prevMergedDirective.ID;
  } 
  
  if (CorrGenMergedDirective::PAUSE == mergedDirective->name) {
    
    /* If there is only one merged Directive then get it and send a corridor pause directive */
    tc.name = TrajectoryCreate::PAUSE;
    tc.id = mergedDirective->ID; 
    m_prevMergedDirective.ID = mergedDirective->ID;
    
    m_corrTrajPlannerSF->sendDirective(&tc);

    m_logger->logString("CORRGEN: Sent PAUSE directive");

    
  } else {
    
    /* Check to see if the last directive was met */
    if (m_prevMergedDirectiveID < mergedDirective->ID) {
      if ((CorrGenMergedDirective::END_OF_MISSION != mergedDirective->name) 
	  && (CorrGenMergedDirective::PAUSE != mergedDirective->name))  {
	
	/* Update the current control action */

	ControlState* controlState = mergedDirective->controlState; 
	TrafficState* trafficState = mergedDirective->trafficState; 
	PlanningHorizon planHoriz = mergedDirective->planHorizon;
	VehicleState vehState = mergedDirective->vehState;
	Corridor corridor; 
	Map* localMap = m_traffStateEst->getUpdatedMap();
	
	/* TODO check on the order of these operations and vehicle state  */

	/* Initialize OCP parameters */
	corridor.initializeOCPparams(m_traffStateEst->getUpdatedVehState(), 
				     planHoriz.getSegGoal(0).minSpeedLimit, 
				     planHoriz.getSegGoal(0).maxSpeedLimit);

	/* Determine the corridor */
	cout<<"CORRGEN,control(): control state "<<controlState->toString()<<endl; 

	controlState->determineCorridor(corridor, vehState, trafficState, planHoriz, m_traffStateEst->getUpdatedMap());

	/* Set the velocity profile */
	corridor.setVelProfile(localMap, vehState, trafficState, controlState, planHoriz);

	/* Get the global to local delta */
	m_gloToLocalDelta = m_traffStateEst->getUpdatedMap()->prior.delta;

	/* exitPtLabel corresponds to the exit way point of the segGoal determined in determinePlanningHorizon */
	m_exitWayptPrevDir = PointLabel(planHoriz.getCurrentExitSegment()
				    , planHoriz.getCurrentExitLane(), planHoriz.getCurrentExitWaypoint());
	
	/* Set the previous merged directive member */
	m_prevMergedDirectiveID = mergedDirective->ID;


	/* Now prepare parameters for sending */
	
	/* Convert to RDDF */
	RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
	
	/* Convert to polytope representation */
	corridor.generatePolyCorridor();
	corridor.printPolyCorridor();

	/* Now cost map? */

	//corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state.localX, m_state.localY, localMap);      
	//CDeltaList* deltaList = NULL;
	//unsigned long long timestamp;
	//DGCgettime(timestamp);
	//deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
	//m_costMap->resetDelta<double>(m_costLayerID);
	corridor.convertOCPtoGlobal(m_gloToLocalDelta);

	/* Send all parameters to trajectory planner */
	corridor.sendPolyCorridor();
	//bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
	//SendMapdelta(staticCostMapSocket, deltaList);

	/* Now we want to send merged directive */
	tc.name = TrajectoryCreate::CREATE;
	tc.id =  m_prevMergedDirectiveID;
	      
	tc.exitSegmentID = mergedDirective->exitSegmentID ; 
	tc.exitLaneID = mergedDirective->exitLaneID ; 
	tc.exitWaypointID = mergedDirective->exitWaypointID ; 

	m_corrTrajPlannerSF->sendDirective(&tc);

	m_logger->logString("CORRGEN: Sent directive:");

      } else if (CorrGenMergedDirective::END_OF_MISSION == mergedDirective->name) {

	m_isEndMission = true;
	tc.name = TrajectoryCreate::END_OF_MISSION;
	tc.id = mergedDirective->ID;
	
	m_logger->logString("CORRGEN: Sent END_OF_MISSION directive.");

	/* TODO: Get the correct way point */
	/* Get the exit waypoint from the seg goal preceding end of mission */
	//endMissionWayptLabel = PointLabel(m_contrDirectiveQ.front().exitSegmentID, 
	//			  m_contrDirectiveQ.front().exitLaneID,  
	//				  m_contrDirectiveQ.front().exitWaypointID);
	//cc.exitPtLabel = endMissionWayptLabel; 
      } 
    }
  }
}


Conflict CorridorGenerator::evaluateDynamicConflicts(Corridor m_currCorridor, ControlState *controlState) 
{
  Conflict conflict;
  return conflict;
}

void CorridorGenerator::evaluateCorridors() 
{
  //return m_currCorridor;
}



//m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);
//RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
//SendRDDF(rddfSocket,rddf);
//corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state.localX, m_state.localY, localMap);      
//CDeltaList* deltaList = NULL;
//unsigned long long timestamp;
//DGCgettime(timestamp);
//deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
//SendMapdelta(staticCostMapSocket, deltaList);
//m_costMap->resetDelta<double>(m_costLayerID);
// send the parameters
//corridor.convertOCPtoGlobal(m_gloToLocalDelta);
//bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
//if (!sendOCPparamsErr){
// cerr << "TrafficPlanner: problem with send OCP parameters" << endl;
// cout << "TrafficPlanner: problem with send OCP parameters" << endl;
//}

