#include "Creep.hh"

Creep::Creep(int stateId, ControlStateFactory::ControlStateType type)
:ControlState(stateId, type)
{
}

Creep::~Creep()
{
}

int Creep::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    if ((m_verbose) || (m_debug)) {
        cout << "In Creep::determineCorridor " << endl;
    }
    
    point2arr leftBound, rightBound, leftBound1, rightBound1;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    double velIn, velOut, acc;
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 FC_finalPos;

    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

    switch (traffState->getType()) {
    
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "Creep::determineCorridor inside ROAD_REGION" << endl;
        }
        
        // We want to plan for current segment only
        // TODO: want to use function that does not specify specific lane
        LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
        if (getBoundsErr != 0) {
            cerr << "Creep.cc: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        /* int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
        int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
        if (rightBoundErr!=0){
            cerr << "Creep.cc: right boundary read from map error" << endl;
            return (rightBoundErr);
        }     
        if (leftBoundErr!=0){
            cerr << "Creep.cc: left boundary read from map error" << endl;
            return (leftBoundErr); 
        } */
        
        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }
        
        // Specify the velocity profile
        velIn = 1;
        velOut = 1;
        acc = 0;

        // specify the ocpParams final conditions - lower bounds
        // want to define this position based on the corridor?
        PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        
        // The only difference between a Road Region and Approach Intersection Safety is here
        int FCPos_Err = 0;
        FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
        
        if (FCPos_Err != 0) {
            cerr << "Creep.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FCPos_Err);
        }
        FC_velMin = 0;

        double heading;
        localMap->getHeading(heading,exitPtLabel);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;
        
    default:
    
        cerr << "Creep.cc: Undefined Traffic state" << endl;
        
    }
    
    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // Set the velocity profile - need to think this through some
    vector < double >velProfile;
    velProfile.push_back(velIn);        // entry pt
    velProfile.push_back(velOut);       // exit pt
    corr.setVelProfile(velProfile);
    corr.setDesiredAcc(acc);

    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    return 0;
}


ControlState* Creep::newCopy() {
  return new Creep(*this);
}
