#ifndef CONTROLSTATEFACTORY_HH_
#define CONTROLSTATEFACTORY_HH_


#include "state/StateGraph.hh"

class ControlStateFactory {


public: 

  ControlStateFactory();
  ~ControlStateFactory();
  static typedef enum { LANE_KEEPING, SLOW_DOWN, STOP, STOPPED, UTURN} ControlStateType;   
  static StateGraph createControlStates();
  static int getNextId();
  static void print(int type);

private:

  int m_stateId;
  
};

#endif /*CONTROLSTATEFACTORY_HH_*/
