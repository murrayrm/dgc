#ifndef TRAFFICUTILS_HH_
#define TRAFFICUTILS_HH_

#include "mapping/Location.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include "state/AliceStateHelper.hh"


class TrafficUtils {

public: 

  static double calculateDistance(Location loc1, Location loc2);
  static double calculateDistance(point2 pt1, point2 pt2);

  static int getClosestPtIndex(point2arr boundary, point2 point);
  static int insertProjPtInBoundary(point2arr& boundary, point2 point);
  static int insertPtAtDistance(point2arr& boundary, int index, double distance);
  static int addAngles(double &angle_out, double angle_in1, double angle_in2);
  static double getNearestObsInLane(MapElement & me, Map * map, VehicleState vehState, LaneLabel lane);
  static double getNearestObsInOppositeLane(MapElement & me, Map * map, VehicleState vehState, LaneLabel lane);
  static double getNearestObsDist(Map * map, VehicleState vehState, LaneLabel lane);
  static double getNearestObsPoint(point2&, Map*, VehicleState, LaneLabel lane, double);
  static double getNearestObsFarthermostPoint(point2 &pt, Map * map, VehicleState vehState, LaneLabel lane, double offset);
  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isLaneBlocked(Map *map, VehicleState vehState, point2 finalPt, LaneLabel lane);
  static MapElementType getObsType(MapElement & me);
  static bool isObsStatic(MapElement & me);
  static int alignBoundaries(point2arr& bound1, point2arr& bound2);
  static double getObstacleVelocityMag(MapElement el);
  static double getObstacleVelocityMagUncert(MapElement el);
  static MapElement* getMapElement(Map* locMap, MapId id);
  static int getLaneSide(Map* localMap, LaneLabel currLane, LaneLabel desiredLane);

private :

};

#endif /*TRAFFICUTILS_HH_*/




