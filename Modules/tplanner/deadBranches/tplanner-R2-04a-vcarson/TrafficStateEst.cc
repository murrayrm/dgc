#include "TrafficStateEst.hh"
#include "ZoneRegion.hh"
#include "interfaces/sn_types.h"

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile) 
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(waitForStateFill)
  , m_verbose(verbose)
  , m_debug(debug)
  , m_log(log)
  , m_followVehTalker(skynetKey, SNleadVehicleInfo, MODtrafficplanner)   
  , m_useRNDF(useRNDF)
  , m_RNDFFile(RNDFFile)
  , m_queueing(false)
  , m_followObsPresent(false)
{
  m_trafficGraph = TrafficStateFactory::createTrafficStates();  
  m_currTraffState = TrafficStateFactory::getInitialState();  

  
  DGCcreateMutex(&m_localMapUpMutex);
  DGCcreateMutex(&m_followObsDataMutex);

  if (useRNDF){
    cout<<"I am using the RNDF"<<endl;
    if (!RNDFFile.empty()){
      cout<<"RNDF is not empty "<<endl;
      m_mapElemTalker.initRecvMapElement(skynetKey, 1);
      m_mapElemTalker.initSendMapElement(skynetKey);
    } else {
      cout<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    cout<<"TFEST: not using RNDF"<<endl;
  }

  m_localUpdateMap = new Map();
  m_localMap = new Map();

  loadRNDF(RNDFFile);

  UpdateState();
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;
  DGClockMutex(&m_localMapUpMutex);
  m_localUpdateMap->prior.delta = statedelta;
  DGCunlockMutex(&m_localMapUpMutex);
 
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_currTraffState; 
}


TrafficStateEst* TrafficStateEst::Instance(int skynetKey, bool waitForStateFill, bool debug, 
					   bool verbose, bool log, bool useRNDF, string RNDFfilename)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(skynetKey, waitForStateFill, debug, verbose, log, useRNDF, RNDFfilename);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

TrafficState* TrafficStateEst::determineTrafficState(PointLabel exitPtLabel) 
{
  //TODO given sensing info determineTrafficState...
  //Given current traffic state, query graph get  possible transitions list  
  //After a query to the map object, vehicle state, determine possible transitions 
  //for all possible transitions calculate the probability of meeting the transition conditions 
  //VehicleState vehState; 
  UpdateState();
  m_currVehState = m_state;
  m_vehStateAtLastEst = m_state;

  // getLocalGlobalMapUpdate();

  std::vector<StateTransition*> transitions = m_trafficGraph.getOutStateTransitions(m_currTraffState);
 
  TrafficStateTransition* transition;

  unsigned int i = 0;

  for (i=0; i < transitions.size(); i++) 
    {
      transition = static_cast<TrafficStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(m_currTraffState, m_localMap, m_currVehState, exitPtLabel);
    }
  m_currTraffState = chooseMostProbableTrafficState(m_currTraffState, transitions);
  return m_currTraffState;
}

TrafficState* TrafficStateEst::getCurrentTrafficState() 
{
  return m_currTraffState; 
}

void TrafficStateEst::updateVehState() 
{
  UpdateState();
  m_currVehState = m_state; 
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}


VehicleState TrafficStateEst::getUpdatedVehState() 
{
  UpdateState();
  m_updatedVehState = m_state; 
  return m_updatedVehState;
}

TrafficState * TrafficStateEst::chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions) 
{
  TrafficState *currTrafficState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  TrafficStateTransition* trans; 
  
  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;

  cout<<"Traffic Transitions size "<<transitions.size()<<endl;

  if (transitions.size() > 0) {
    for (i=0; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }

    cout<<"Traffic ProbOneTrans size "<<probOneTrans.size()<<endl;
    if (probOneTrans.size() > 0) {
      trans = static_cast<TrafficStateTransition*> (probOneTrans.front());
      currTrafficState = trans->getTrafficStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        //cout<<"There are no TrafficStateTransitions with probability 1"<<endl;
      }
      currTrafficState = trafState; 
    }
  } else {
    /* TO DO Fix */
  }
 
  return currTrafficState;
}


void TrafficStateEst::getLocalMapUpdate()
{

  MapElement recvEl;
  int bytesRecv;
  cout<<"In local map update ..."<<endl; 
  while (true) {
    bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);
    
    if (bytesRecv>0){
      DGClockMutex(&m_localMapUpMutex);
      m_localUpdateMap->addEl(recvEl);
      DGCunlockMutex(&m_localMapUpMutex);
    }else {
      cout << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
	   << bytesRecv << endl;
      usleep(100);
    }
  }
}

void TrafficStateEst::updateMap() {

  DGClockMutex(&m_localMapUpMutex);
  m_localMap = m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  m_gloToLocalDelta = m_localMap->prior.delta;

}

Map* TrafficStateEst::getUpdatedMap() {

  DGClockMutex(&m_localMapUpMutex);
  m_updatedMap = m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  return m_updatedMap;

}

Map* TrafficStateEst::getMap() {
  return m_localMap;
}

bool TrafficStateEst::loadRNDF(string filename) {
  
  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);

}

void TrafficStateEst::getFollowObstacleUpdate() {

  MapElement me; 
  double distObst = 0, velMagObst =0;  
  point2 obsPos; 
  double currVel; 
  double AliceLength = DIST_REAR_AXLE_TO_FRONT; 
  double separationDist = 4*AliceLength;
  double baseVel = 4.4704;  // 10 miles/hour converted to m/s 

  while (true) {
    distObst = TrafficUtils::getNearestObsInLane(me,getUpdatedMap(), getUpdatedVehState(), 
						 AliceStateHelper::getDesiredLaneLabel());
 
    
    velMagObst = TrafficUtils::getObstacleVelocityMag(me);
    
    TrafficUtils::getNearestObsPoint(obsPos,getUpdatedMap(), 
    				     getUpdatedVehState(), 
    				     AliceStateHelper::getDesiredLaneLabel(), 0);
      
    // Calculate the separation distance
    currVel = AliceStateHelper::getVelocityMag(getUpdatedVehState());
    switch (m_currTraffState->getType())
    {    
      case TrafficStateFactory::ZONE_REGION:
      case TrafficStateFactory::APPROACH_INTER_SAFETY:
      case TrafficStateFactory::INTERSECTION_STOP:
        {
          //m_separationDist=2;
          separationDist=AliceLength;
        }
        break;
      case TrafficStateFactory::ROAD_REGION:
        {
          separationDist = AliceLength*(1+currVel/baseVel);
        }
        break;
      default:
      {
        separationDist = 4*AliceLength;
      }
    }


    if (m_currFollowObs.size() > 1) {
      m_currFollowObs.pop();
      m_currFollowObs.pop();
      m_currFollowObs.pop();
    }

    m_currFollowObs.push(velMagObst);
    m_currFollowObs.push(distObst);
    m_currFollowObs.push(separationDist);

    DGClockMutex(&m_followObsDataMutex);    

    /* We want to keep the size of the vector to be relatively small, ie over seconds */
    if(m_followObsData.size() >= 100) {
      m_followObsData.pop_front(); //get rid of old data 
    }
    if (distObst != -1) { 
      m_followObsPresent = true; 
      m_followObsData.push_back(obsPos);
    } else {
      m_followObsPresent = false; 
    }
    //cout<<"OBS POS pushed back "<<obsPos<<endl;
    DGCunlockMutex(&m_followObsDataMutex);

    sendFollowObstacle();
    usleep(100000);
  }
}

void TrafficStateEst::sendFollowObstacle() {

  leadVehInfo followVeh; 

  followVeh.velocity = m_currFollowObs.front();
  followVeh.distance = m_currFollowObs.front();
  followVeh.separationDistance = m_currFollowObs.front();
  m_followVehTalker.send(&followVeh);
}

void TrafficStateEst::updateQueueing() {

  if (isObstacleMoving()) {
    m_queueing = true;
  } else if (!isObstacleMoving()) {
    m_queueing = false; 
  }
}

bool TrafficStateEst::isQueueing() {
  return m_queueing; 
}

bool TrafficStateEst::isObstacleMoving() {

  double distThreshold = 0.025;

  bool isMoving = false; 

  if (m_followObsPresent) {


    DGClockMutex(&m_followObsDataMutex);
    m_followObsDataSnap = m_followObsData; 
    //    for(int i =0; i < m_followObsData.size();i++) {
    //      cout<<"m_followObsData["<<i<<"]="<<m_followObsData[i]<<endl;
    //    }
    DGCunlockMutex(&m_followObsDataMutex);
    
    int indexLastObsData = sizeOfFollowObsData() - 1;
    
    if (indexLastObsData > 0) {
      for (int i=indexLastObsData; i >= 0; i=i-5) {
	//cout<<"Distance between points "<<m_followObsDataSnap[indexLastObsData].dist(m_followObsDataSnap[i])<<endl;
        //	cout<<"Beg, point "<<m_followObsDataSnap[indexLastObsData]<<"end point "<<m_followObsDataSnap[i]<<endl;
	if (m_followObsDataSnap[indexLastObsData].dist(m_followObsDataSnap[i]) >= distThreshold) {
	  isMoving=true; 
	  break;
	}
      }  
    }
    m_followObsDataSnap.clear();
   
  }
  return isMoving;
}

int TrafficStateEst::sizeOfFollowObsData() {
  return m_followObsDataSnap.size();
}

