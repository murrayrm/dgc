#ifndef STOPPEDTOCREEP_HH_
#define STOPPEDTOCREEP_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class StoppedToCreep:public ControlStateTransition {

  public:

    StoppedToCreep(ControlState * state1, ControlState * state2);
    StoppedToCreep();
    ~StoppedToCreep();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

};

#endif                          /*STOPPEDTOCREEP_HH_ */
