#include "IntersectionHandling.hh"
#include "TrafficManager.hh"
#include <sstream>

using namespace std;

bool IntersectionHandling::init=false;
vector<ListToA> IntersectionHandling::VehicleList;
vector<ListBlockedObstacles> IntersectionHandling::BlockedObstacles;
bool IntersectionHandling::ClearToGo,IntersectionHandling::LegalToGo,IntersectionHandling::IntersectionCreated;
vector<PointLabel> IntersectionHandling::WayPoint_WithStop,IntersectionHandling::WayPoint_NoStop,IntersectionHandling::WayPointEntries;
PointLabel IntersectionHandling::StopLine_alice,IntersectionHandling::StopLine_reverse;
double IntersectionHandling::distance_stopline_alice,IntersectionHandling::distance_reverse;
CMapElementTalker IntersectionHandling::testMap;
MapElement IntersectionHandling::alice;

void IntersectionHandling::resetIntersection(Map* localMap)
{
  LegalToGo=false;
  ClearToGo=false;
  IntersectionCreated=false;
  VehicleList.clear();
  BlockedObstacles.clear();
  init=true;

  // Initialize Debug-Map
  testMap.initSendMapElement(TrafficManager::getSkynetKey());

  // Read RNDF from real map and pass it to Debug-Map
  //  MapElement el;
  //  for (unsigned int k=0; k<localMap->prior.data.size(); k++)
  //    {
  //      localMap->prior.getEl(el,k);
  //      testMap.sendMapElement(&el,sendChannel);
  //    }
}

bool IntersectionHandling::checkIntersection(VehicleState vehState,Map* localMap, SegGoals currSegment, ControlState * cstate)
{
  // Send Alice on map
  MapElement alice;
  alice.set_alice(vehState);
  testMap.sendMapElement(&alice,sendChannel);

  // Create intersection if it didn't happen yet
  if (!IntersectionCreated)
    createIntersection(vehState,localMap);
  
  if (IntersectionCreated)
    {
      // Check for current status of other obstacled.
      checkExistenceObstacles(localMap,cstate);

      // Alice needs to be in control state STOPPED, then check for Precedence
      if (cstate->getType()==ControlStateFactory::STOPPED)
	{
	  LegalToGo=checkPrecedence();
	  Log::getStream(7)<<"Alice stopped. Check Precedence..."<<LegalToGo<<endl;
	}
      else
	Log::getStream(7)<<"Alice didn't stop yet. Not legal to go!"<<endl;

      // After it is legal to go, check whether it's clear to go
      if (LegalToGo && !ClearToGo)
	  ClearToGo=checkClearance(localMap,currSegment);
    }
  else
    {
      // Output some debugging information
      Log::getStream(7)<<"Error: Couldn't build Intersection. But give ok for Legal&Clear to go."<<endl;
      Log::getStream(7)<<"Next Stopline Alice..."<<StopLine_alice<<", distance "<<distance_stopline_alice<<endl;

      LegalToGo=true;
      ClearToGo=true;
    }

  Log::getStream(1)<<"Intersection legal to go..."<<LegalToGo<<endl;
  Log::getStream(1)<<"Intersection clear to go..."<<ClearToGo<<endl;
 
  if (LegalToGo && ClearToGo)
    return true;
  else
    return false;
}

void IntersectionHandling::createIntersection(VehicleState vehState,Map* localMap)
{
  vector<PointLabel> WayPoint;
  vector<PointLabel> StopLines;
  point2 p;
  double distance_temp;

  // Obtain Alice's position
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);
  LaneLabel lane_alice;
  localMap->getLane(lane_alice,position_alice);

  // get Alice's centerline
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  if (localMap->getLaneStopLines(StopLines,lane_alice)!=0)
    {
      distance_stopline_alice=INFINITY;

      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      for (unsigned int i=0; i<StopLines.size(); i++)
	{
	  localMap->getWaypoint(p,StopLines[i]);
	  localMap->getDistAlongLine(distance_temp,centerline_alice,p,position_alice);

	  if (distance_temp>0 && distance_temp<distance_stopline_alice)
 	  {
	    distance_stopline_alice=distance_temp;
	    StopLine_alice=StopLines[i];
	    IntersectionCreated=true;
	  }
	}

      if (distance_stopline_alice<INFINITY)
	{
	  // get all entry points of the intersection
	  populateWayPoints(localMap,StopLine_alice);
	  findStoplines(localMap);
	}
    }
}

void IntersectionHandling::populateWayPoints(Map* localMap, PointLabel InitialWayPointEntry)
{
  vector<PointLabel> WayPointExits, WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++)
    {
      localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++)
	{
	  bool found=false;
	  for (unsigned int k=0; k<WayPointEntries.size(); k++)
	    {
	      if (WayPoint[j]==WayPointEntries[k])
		{
		  found=true;
		  break;
		}
	    }

	  // If not, add it to list and call function recursivly
	  if (!found)
	    {
	      WayPointEntries.push_back(WayPoint[j]);
	      populateWayPoints(localMap,WayPoint[j]);
	    }
	}
    }

  // Send Waypoints to map
  MapElement me;
  point2 p;
  int status;
  for (unsigned k=0; k<WayPointEntries.size(); k++)
    {
      me.setId(k+5000);
      me.setTypeWayPoints();
      localMap->getWayPoint(p,WayPointEntries[k]);
      me.setPosition(p);
      // little circle
      me.setGeometry(p,1.0);
      status=testMap.sendMapElement(&me,sendChannel);
    }
}

void IntersectionHandling::findStoplines(Map* localMap)
{
  MapElement me;
  point2 p;
  
  for (unsigned int i=0; i<WayPointEntries.size(); i++)
    {
      if (localMap->isStopLine(WayPointEntries[i]))
	{
	  // Store information in vector
	  WayPoint_WithStop.push_back(WayPointEntries[i]);

	  // Send stopline to map, color red
	  me.setId(i+6000);
	  me.setTypeStopLine();
	  me.setColor(MAP_COLOR_RED);
	  localMap->getWayPoint(p,WayPointEntries[i]);
	  me.setGeometry(p,1,1);
	  me.setPosition(p);
	  testMap.sendMapElement(&me,sendChannel);
	}
      else
	{
	  // Store information in vector
	  WayPoint_NoStop.push_back(WayPointEntries[i]);	

	  // Even so there is no stopline, send a stopline with color green to the map
	  me.setId(i+6000);
	  me.setTypeStopLine();
	  me.setColor(MAP_COLOR_GREEN);
	  localMap->getWayPoint(p,WayPointEntries[i]);
	  me.setPosition(p);
	  testMap.sendMapElement(&me,sendChannel);
	}
    }
}

void IntersectionHandling::checkExistenceObstacles(Map* localMap,ControlState * cstate)
{
  point2 p,position_obstacle;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry;
  ListToA list_temp;
  int foundindex;
  vector<ListToA> tempVehicleList;
  int CounterObstacle=0;
  MapElement obstacleMap;
  
  // Reset updated-flag
  for (unsigned int k=0; k<VehicleList.size(); k++)
    VehicleList[k].updated=false;

  // Look for obstacles at all stoplines
  for (unsigned int i=0; i<WayPoint_WithStop.size(); i++)
    {
      localMap->getWayPoint(p,WayPoint_WithStop[i]);
      localMap->getLane(lane,p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);

      CounterObstacle=0;

      // Loop through all obstacles found in lane
      for (unsigned int j=0;j<obstacle.size();j++)
	{
	  if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE)
	      continue;

	  distance_geometry=INFINITY;
	  // Get distance from stopline to closest point of obstacle
	  for (unsigned l=0; l<obstacle[j].geometry.size();l++)
	    {
	      position_obstacle.set(obstacle[j].geometry[l]);
	      localMap->getDistAlongLine(distance_temp, centerline, p, position_obstacle);
	      if (distance_temp > 0.0 && distance_temp < distance_geometry)
		distance_geometry = distance_temp;
	    }

	  // Looks within 30m of stopline
	  if (distance_geometry>0 && distance_geometry<30)
	    {
	      // Send obstacle to map
	      obstacle[j].setColor(MAP_COLOR_GREY,100);
	      testMap.sendMapElement(&obstacle[j],sendChannel);

	      CounterObstacle++;
	      // Look whether vehicle already exists
	      foundindex=-1;
	      for (unsigned int k=0; k<VehicleList.size(); k++)
		{
		  if (VehicleList[k].element.id==obstacle[j].id)
		    {
		      VehicleList[k].updated=true;
		      foundindex=k;
		    }
		}

	      // Don't add or update information when Alice stopped. We only wait until existing vehicles with ETA<epsilon are leaving intersection
	      // If vehicle is new, then add it to list
	      if (foundindex==-1 && cstate->getType()!=ControlStateFactory::STOPPED)
		{
		  if (distance_temp>0)
		    {
		      // Create list object
		      list_temp.element=obstacle[j];
		      list_temp.WayPoint=WayPoint_WithStop[i];
		      list_temp.velocity=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));
		      list_temp.distance=distance_geometry;
		      list_temp.updated=true;
		      time(&list_temp.time);
		      
		      // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<.5m/s
		      if (list_temp.velocity>=VELOCITY_OBSTACLE_THRESHOLD)
			list_temp.eta=list_temp.distance/list_temp.velocity;
		      else if (list_temp.distance>=DISTANCE_OBSTACLE_THRESHOLD)
			list_temp.eta=INFINITY;
		      else
			list_temp.eta=0;

		      
		      VehicleList.push_back(list_temp);
		      Log::getStream(7)<<"Obstacle "<<obstacle[j].id<<" stored"<<endl;
		    }
		}
	      // Otherwise update vehicle's state
	      else if (cstate->getType()!=ControlStateFactory::STOPPED)
		{
		  time_t t,t2;
		  double delta_t,v,v2,a;
		  
		  time(&t);
		  v=VehicleList[foundindex].velocity;
		  
		  t2=VehicleList[foundindex].time;
		  
		  v2=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));;
		  delta_t=difftime(t2,t);
		  
		  // Update VehicleList
		  VehicleList[foundindex].element=obstacle[j];
		  VehicleList[foundindex].velocity=v2;
		  VehicleList[foundindex].distance=distance_geometry;
		  VehicleList[foundindex].updated=true;
		  time(&VehicleList[foundindex].time);
		  
		  int debug=-1;
		  // s=0.5 * a * t * t
		  if ((t2-t)!=0)
		    a=(v2-v)/(t2-t);
		  else
		    a=0;
		  
		  if (a!=0)
		    {
		      if ((VehicleList[foundindex].distance-DISTANCE_STOPLINE_OBSTACLE)>=0)
			{
			  VehicleList[foundindex].eta=sqrt(2*(VehicleList[foundindex].distance-DISTANCE_STOPLINE_OBSTACLE)/fabs(a));
			  debug=1;
			}
		      else if (distance_temp!=0)
			{
			  VehicleList[foundindex].eta=sqrt(2*VehicleList[foundindex].distance/fabs(a));
			  debug=2;
			}
		      else
			{
			  VehicleList[foundindex].eta=0;
			  debug=3;
			}
		    }
		  else if (v2>=VELOCITY_OBSTACLE_THRESHOLD)
		    if ((distance_temp-DISTANCE_STOPLINE_OBSTACLE)>=0)
		      {
			VehicleList[foundindex].eta=(distance_temp-DISTANCE_STOPLINE_OBSTACLE)/v2;
			debug=4;
		      }
		    else if (distance_temp!=0)
		      {
			VehicleList[foundindex].eta=distance_temp/v2;
			debug=5;
		      }
		    else
		      {
			VehicleList[foundindex].eta=0;
			debug=6;
		      }
		  else if (VehicleList[foundindex].distance>=DISTANCE_OBSTACLE_THRESHOLD)
		    {
		      VehicleList[foundindex].eta=INFINITY;
		      debug=7;
		    }
		  else if (VehicleList[foundindex].distance<DISTANCE_OBSTACLE_THRESHOLD && VehicleList[foundindex].eta==0)
		    {
		      VehicleList[foundindex].eta=0;
		      debug=10;
		    }
		  else
		    {
		      VehicleList[foundindex].eta=INFINITY;
		      debug=8;
		    }
		}
	    }
	}
      Log::getStream(7)<<"Looking at waypoint..."<<WayPoint_WithStop[i]<<". Number of vehicles found "<<CounterObstacle<<endl;
    }

  // Clean up VehicleList
  for (unsigned int k=0; k<VehicleList.size(); k++)
    {
      if (VehicleList[k].updated==false)
	{
	  // Clear element out of map
	  obstacleMap.setId(VehicleList[k].element.id);
	  obstacleMap.setTypeClear();
	  testMap.sendMapElement(&obstacleMap,sendChannel);

	  Log::getStream(7)<<"Remove obstacle at WayPoint..."<<VehicleList[k].WayPoint<<"... ObstacleID..."<<VehicleList[k].element.id<<endl;
	}
      else
	tempVehicleList.push_back(VehicleList[k]);
    }
  VehicleList=tempVehicleList;

  // Output all vehicles that are at the intersection currently
  for (unsigned int m=0;m<VehicleList.size();m++)
    {
      Log::getStream(9)<<"--------------------------------"<<endl;
      Log::getStream(9)<<"ToA["<<m<<"] Id..."<<VehicleList[m].element.id<<endl;
      Log::getStream(9)<<"ToA["<<m<<"] WayPoint..."<<VehicleList[m].WayPoint<<endl;
      Log::getStream(9)<<"ToA["<<m<<"] distance..."<<VehicleList[m].distance<<endl;
      Log::getStream(9)<<"ToA["<<m<<"] velocity..."<<VehicleList[m].velocity<<endl;
      Log::getStream(9)<<"ToA["<<m<<"] ETA..."<<VehicleList[m].eta<<endl;
    }
}

bool IntersectionHandling::checkPrecedence()
{
  int counter=0;
  MapElement me;

  // All Vehicles with a very small ETA have precedence
  for (unsigned int i=0; i<VehicleList.size(); i++)
      if (VehicleList[i].eta<ETA_EPS)
	{
	  VehicleList[i].element.setColor(MAP_COLOR_ORANGE,100);
	  testMap.sendMapElement(&VehicleList[i].element,sendChannel);

	  counter++;
	}
  // Otherwise return true
  if (counter>0)
    return false;
  else
    return true;
}

bool IntersectionHandling::checkClearance(Map* localMap,SegGoals currSegment)
{
  point2arr centerline;
  vector<PointLabel> WayPointExits;
  point2 p_entry,p_exit,position_obstacle;
  vector<MapElement> obstacles;
  int counter=0;
  bool found=false;
  ListBlockedObstacles tempBlockObstacle;
  vector<ListBlockedObstacles> tempList;
  point2arr leftBound, rightBound;
  MapElement me;
  LaneLabel lane_exit;
  double distance_exit;

  // reset status of all obstacles blocking the intersection
  for (unsigned k=0; k<BlockedObstacles.size(); k++)
    BlockedObstacles[k].updated=false;

//******************************

 //********************************

  int idcounter=3000;
  // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
  for (unsigned i=0; i<WayPointEntries.size(); i++)
    {
      localMap->getWaypoint(p_entry,WayPointEntries[i]);
      //      localMap->getLane(lane_entry,p_entry);

      // get all exits for this entry
      localMap->getWayPointExits(WayPointExits,WayPointEntries[i]);
      for (unsigned j=0;j<WayPointExits.size(); j++)
	{
	  // Create TransitionBound between this WayPointEntry and WayPointExit
	  localMap->getTransitionBounds(leftBound, rightBound, WayPointEntries[i], WayPointExits[j], p_entry, 0, 0);

	  // Get all obstacles within these bounds
	  localMap->getObsInBounds(obstacles, leftBound, rightBound);

	  // paint TransitionBounds
	  stringstream s;

	  s.str();

	  idcounter++;
	  me.setId(idcounter);
	  me.setTypeLine();
	  me.setColor(MAP_COLOR_GREEN,100);
	  me.setGeometry(leftBound);
	  me.label.push_back("str");
	  testMap.sendMapElement(&me,sendChannel);
	  cout<<"BoundID..."<<idcounter<<" "<<WayPointEntries[i]<<" -> "<<WayPointExits[j]<<" left"<<endl;

	  idcounter++;
	  me.setId(idcounter);
	  me.setGeometry(rightBound);
	  me.label.push_back("str");
	  testMap.sendMapElement(&me,sendChannel);
	  cout<<"BoundID..."<<idcounter<<" "<<WayPointEntries[i]<<" -> "<<WayPointExits[j]<<" right"<<endl;
	  
	  for (unsigned int k=0; k<obstacles.size(); k++)
	    {
	      MapId fakeid=obstacles[k].id;
	      if (fakeid.dat[1]>10000) continue;
	      counter++;
	      cout<<"CHRISTIAN FOUND OBSTACLE WITHIN BOUNDS..."<<WayPointEntries[i]<<"->"<<WayPointExits[j]<<endl;
	      cout<<"MATCHING BOUND IDs..."<<idcounter-1<<"..."<<idcounter-2<<endl;
	      
	      // send obstacle that blocks the lane	
	      obstacles[k].setColor(MAP_COLOR_RED,100);
	      testMap.sendMapElement(&obstacles[k],sendChannel);
	      Log::getStream(7)<<"Intersection...blocked by obstacle "<<obstacles[k].id<<endl;

	      found=false;
	      // if obstacle is found in list, set updated=true
	      for (unsigned l=0; l<BlockedObstacles.size(); l++)
		{
		  if (obstacles[k].id==BlockedObstacles[l].obstacle.id)
		    {
		      BlockedObstacles[l].updated=true;
		      found=true;
		    }
		}
	      // otherwise add it to list
	      if (!found)
		{
		  tempBlockObstacle.obstacle=obstacles[k];
		  tempBlockObstacle.updated=true;
		  BlockedObstacles.push_back(tempBlockObstacle);
		}
	    }
	}
    }

  // Check whether ExitWaypoint is clear to go
   double distance_geometry;
   int obstacle_index;
   point2 p;
   PointLabel WayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

   localMap->getWaypoint(p_exit,WayPointExit);
   localMap->getLane(lane_exit,p_exit);
   localMap->getLaneCenterLine(centerline,lane_exit);
   localMap->getObsInLane(obstacles,lane_exit);
   for (unsigned int k=0; k<obstacles.size(); k++)
     {
       position_obstacle.set(obstacles[k].position);
       localMap->getDistAlongLine(distance_exit,centerline,p_exit,position_obstacle);

       // Only if obstacle blocks lane completly
       bool blockLane=TrafficUtils::isObstacleBlockingLane(obstacles[k],localMap,lane_exit);
       if (blockLane)
 	{
 	  distance_geometry=INFINITY;
 	  // Get distance from stopline to closest point of obstacle
 	  for (unsigned l=0; l<obstacles[k].geometry.size();l++)
 	    {
 	      position_obstacle.set(obstacles[k].geometry[l]);
 	      localMap->getDistAlongLine(distance_exit, centerline, position_obstacle, p_exit);
	      
 	      if (distance_exit > 0.0 && distance_exit < distance_geometry)
 		{
 		  obstacle_index=k;
 		  distance_geometry = distance_exit;
 		}
 	    }

 	  // If an obstacle is within 10m of the Intersection WaypointExit, consider intersection as blocked!!
 	  if (distance_geometry<DISTANCE_OBSTACLE_WAYPOINTEXIT)
 	    {
 	      counter++;
	      cout<<"CHRISTIAN FOUND OBSTACLE BEHIND WAYPOINT"<<endl;
 	      bool found=false;
 	      //check whether this obstacle already exists
 	      for (unsigned m=0; m<BlockedObstacles.size(); m++)
 		{
 		  if (BlockedObstacles[m].obstacle.id==obstacles[obstacle_index].id)
 		    {
 		      BlockedObstacles[m].updated=true;
 		      found=true;
 		    }
 		}

 	      if (!found)
 		{
 		  tempBlockObstacle.obstacle=obstacles[obstacle_index];
 		  tempBlockObstacle.updated=true;
 		  BlockedObstacles.push_back(tempBlockObstacle);
 		}

 	      obstacles[obstacle_index].setColor(MAP_COLOR_RED,100);
 	      testMap.sendMapElement(&obstacles[obstacle_index],sendChannel);
 	    }
 	}
     }

  // Clear out obstacles that disappear from map; this doesnt affect Alice and is for debugging/mapping purposes only
  for (unsigned m=0;m<BlockedObstacles.size(); m++)
    {
      if (BlockedObstacles[m].updated)
	tempList.push_back(BlockedObstacles[m]);
      else
	{
	  // clear obstacle out of map
	  BlockedObstacles[m].obstacle.setTypeClear();
	  testMap.sendMapElement(&BlockedObstacles[m].obstacle,sendChannel);
	}
    }
  BlockedObstacles=tempList;

  if (counter>0) return false;
  else
    return true;
}









