#include "StoppedToLaneKeeping.hh"
#include <math.h>
#include <list>

StoppedToLaneKeeping::StoppedToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

StoppedToLaneKeeping::StoppedToLaneKeeping()
{

}

StoppedToLaneKeeping::~StoppedToLaneKeeping()
{

}

double StoppedToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToLaneKeeping::meetTransitionConditions" << endl;
    }
    
    switch (trafficState->getType()) {
    
     case TrafficStateFactory::ZONE_REGION:
     {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        bool clear = true;
        if (clear) {
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToLaneKeeping: ZONE_REGION = CLEAR" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    case TrafficStateFactory::ROAD_REGION:
    {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        bool clear = false;
        if (clear) {
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToLaneKeeping: ROAD_REGION = CLEAR" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        sleep(3);
        if ((m_verbose) || (m_debug)) {
            cout << "checking to see if intersection is clear" << endl;
        }
        
        bool intersectionClear = true;
        // Check if it is our turn
        // TODO: DO THIS PROPERLY
        bool ourTurn = true;

        if ((ourTurn) && (intersectionClear)) {
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToLaneKeeping: INTERSECTION_STOP = CLEAR and OURTURN" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    default:
    
        m_probability = 0;
        cerr << "StoppedToLaneKeeping.cc: Undefined Traffic state" << endl;
    
    }
    
    if ((m_verbose) || (m_debug)) {
        cout << "probability = " << m_probability << endl;
    }
    setUncertainty(m_probability);
    
    return m_probability;
}
