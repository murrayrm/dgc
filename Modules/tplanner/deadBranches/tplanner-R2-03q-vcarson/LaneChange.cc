#include "LaneChange.hh"
#include "CorridorGen.hh"

LaneChange::LaneChange(int stateId, ControlStateFactory::ControlStateType type)
:ControlState(stateId, type)
{
  isReverse = 0;
}

LaneChange::~LaneChange()
{
}

void LaneChange::setReverse(int reverse)
{
  isReverse = reverse;
}

int LaneChange::getReverse()
{
  return isReverse;
}

int LaneChange::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  if ((m_verbose) || (m_debug)) {
    cout << "In LaneChange::determineCorridor " << endl;
  }
  
  int error = 0;
  SegGoals currSegment = planHoriz.getSegGoal(0);

  LaneLabel destLane = AliceStateHelper::getDesiredLaneLabel();
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  switch (traffState->getType()) {
  case TrafficStateFactory::ROAD_REGION:
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      cout << "LaneChange corridor: destination lane: " << destLane << endl;
      point2 initPos = this->getInitialPosition();
      error += CorridorGen::makeCorridorLaneChange(corr, vehState, traffState, planHoriz, localMap, destLane, initPos, isReverse);
      
      //      error += CorridorGen::setFinalCondLaneChange(corr, currFrontPos, localMap, currSegment, isReverse);
      if (error != 0) {
        cerr << "LaneChange corridor generation failed: " << error << endl;
      }
    }
    break;
    
  default:
    {
      cerr << "LaneChange.cc: Undefined for this Traffic state" << endl;
      error += 1;
    }
  }
  
  return error;
}

