#ifndef TRAFFICMANAGER_HH_
#define TRAFFICMANAGER_HH_

#include <fstream>
#include "Corridor.hh"
#include "Conflict.hh"
#include "mapping/Segment.hh"
#include "TrafficStateEst.hh"
#include "ControlState.hh"
#include "ControlStateFactory.hh"
#include "TrafficState.hh"
#include "ControlStateTransition.hh"
#include "state/StateGraph.hh"
#include "interfaces/VehicleState.h"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "gcinterfaces/AdriveCommand.hh"
#include "Interfaces.hh"
#include "Log.hh"
//TODO take out 
#include "skynettalker/RDDFTalker.hh"
#include "skynettalker/SkynetTalker.hh"

extern int DEBUG_LEVEL;

/*! Input interface from which SegGoals are received from the Route Planner */

typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MissionTrafficInterface;


class TrafficManagerControlStatus : public ControlStatus
{
public:
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };

  /* The id of the merged directive that this control status corresponds to. */
  unsigned int ID; 
  Status status;
  TrafficManagerResponse::ReasonForFailure reason;
  bool wasPaused;
};

class TrafficManagerMergedDirective : public MergedDirective
{
public:
 
  TrafficManagerMergedDirective(){};
  ~TrafficManagerMergedDirective(){};

  list<int> segGoalsIDs; 
  SegGoals::SegmentType segType; 
  SegGoals::IntersectionType interType; 

  int exitSegmentID; 
  int exitLaneID; 
  int exitWaypointID;
 
};


struct TrafficManagerMergedDirResp
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  TrafficManagerMergedDirResp()
    :status(QUEUED)
  {
  }

  Status status;
  CorridorGenStatus::ReasonForFailure reason;
};


class TrafficManager : public GcModule, public CRDDFTalker {

public: 
  
  /*! Constructor */

  TrafficManager(int skynetKey, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile, 
		 bool console, char* configFile);

  /*! Destructor */
  virtual ~TrafficManager();

  /*! This is the function that continually runs the planner in a loop */
  void TrafficPlanningLoop(void);

  /*! This is a function that is used to load an RNDF in place of a map object for estimation */
  
private :
  
  /*! Arbitration for the traffic planner control module. It computes the next
    merged directive based on the directives from mission control
    and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the traffic planner control module. It computes and sends
    directives to all its controlled modules based on the 
    merged directive and outputs the control status
    based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);
  
  //TODO: move all methods below to PRIVATE  
  /*! Given the traffic state passed, give the most likely control state to maintain. */
  ControlState* determineControlAction();
    
  /*! 
   * Determine the planning horizon, given the current traffic state. 
   */
  bool determinePlanningHorizon(deque<SegGoals> currSegGoals);
      
  /*! Choose the most probable control action. */
  ControlState* chooseMostProbableControlState(ControlState* controlState,std::vector<StateTransition*> transitions);   

  /*! Returns a unique ID for merged directives */
  unsigned int getNextUniqueMergedDirID(); 

  /*! Returns whether or not the goal is completed when there is an exit waypoint involved */
  bool isGoalComplete(PointLabel exitWayptLabel);

  /*! Returns whether or not the goal is completed when we don't check for exit waypoint */
  bool isGoalComplete();

  /*! Returns the time of day */
  uint64_t getTime();

  /*! Initialize parameters for painting cost map from config file */
  void readConfigFile(char* file);

  /*! Determine whether the turn signals need to be turned on */
  void determineSignaling(SegGoals segGoals);

  /*! Turn the signals on by sending a message to adrive */
  void turnSignalsOn(SegGoals segGoals);

  /*! Turn the signals off by sending a message to adrive */
  void turnSignalsOff();

  /*! Send the turn signal command */
  void sendTurnSignalCommand();

  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  unsigned int m_uniqueMergedDirID; 
  /*!\param control status sent from control to arbiter */
  CorridorGenStatus m_corrCreateStatus;

  /*!\param merged directive sent from arbiter to control */
  TrafficManagerMergedDirective m_mergedDirective; 

  /*!\param merged directive sent from arbiter to control */
  TrafficManagerMergedDirective m_prevMergedDirective; 
  
  /*!\param goalID of control directive that corresponds to the completion of
   * m_prevMergedDirective */
  unsigned int m_prevMergedDirectiveID;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_currMergedDirective */
  unsigned int m_currMergedDirectiveID;

  /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<TrafficManagerMergedDirective> m_contrDirectiveQ;

  /*!\param directives currently stored until planning horizon reqs are met */
  deque<SegGoals> m_accSegGoalsQ;


  /*!\Planning Horizon used during determinePlanningHorizon  */
  PlanningHorizon m_currPlanHorizon;

  /*!\Current Planning Horizon  */
  PlanningHorizon m_currMergedPlanHoriz;

  /*!\Current Seg Goal end point  */
  //PointLabel m_currGoalEndpoint;

  /*!\Previous Seg Goal end point  */
  PointLabel m_prevGoalEndpoint;

  /*!\Current SegGoals  */
  SegGoals m_currSegGoals;

  /*!\Current SegGoals exitwaypoint */
  PointLabel m_currSegGoalsExitWaypt;
  
  /*!\Previous SegGoals  */
  SegGoals m_prevSegGoals;

  /*!\Current number of segments   */
  unsigned int m_numSegs;

  /*!\param the goal corresponding to the end of mission */
  SegGoals m_endMissionGoal; 

  /*!\param the goal corresponding to the end of mission */
  bool m_isEndMission; 

  /*!\param control status sent from control to arbiter */
  TrafficManagerControlStatus m_controlStatus;

  /*!\param GcInterface variable */
  MissionTrafficInterface::Northface* m_missTraffInterfaceNF;

  /*!\param GcInterface variable */
  AdriveCommand::Southface* m_traffAdriveInterfaceSF;

  /*!\param corridor  (because of CSS bug)  */
  Corridor m_currCorr;
  
  /*! Global to local delta (because of CSS bug) */
  point2 m_gloToLocalDelta;
  

  bool m_verbose;
  bool m_debug;
  
  /*!\param m_logData is true when data is to be logged */
  bool m_logData;

  bool m_isInit;

  FILE* m_logFile;

  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*!\Control State */
  StateGraph m_controlGraph;

  /*!\Singleton intance of the Traffic State Estimator */
  TrafficStateEst* m_traffStateEst;

  /*!\Current Traffic State */
  TrafficState* m_currTraffState; 

  /*!\Current Control State  */
  ControlState* m_currContrState; 

  /*!\Current Logger  */
  Log* m_logger;

  /*!\Rddf socket number  */
  int m_rddfSocket;
  
  /*!\OCP Params talker  */
  SkynetTalker <OCPparams> m_OCPparamsTalker;  

  /*!\Rddf socket number  */
  bool m_completed;

  /*!\Bitmap talker  */
  SkynetTalker< BitmapParams > m_polyTalker;

  /*!\Parameters for painting cost map */
  BitmapParams m_bmparams;
  PolygonParams m_polygonParams;

  /*!\Start time of the executing goal */
  uint64_t m_startTimeGoal;

  /*!\Console display */
  bool m_console;

  /*!\Is turn signal on */
  bool m_isTurnSignalOn;

  /*!\Turn signal */
  double m_turnSignal;

  /*!\Adrive Directive  */
  AdriveDirective m_adriveDir; 

  /*!\Adrive Directive ID  */
  int  m_turnSignalDirId;

  void display_console();

};

#endif /*TRAFFICMANAGER_HH_*/
