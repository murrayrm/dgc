#ifndef ROAD_HH_
#define ROAD_HH_


#include "IntersectionLink.hh"
#include "Lane.hh"
#include "Line.hh"
#include <vector>

class Road {

public:
	//TODO get rid of default constructor and int param constructor
	Road(int roadId);
	Road();
   	Road(int roadId, std::vector<Line> roadBoundary);
	Road(int roadId, std::vector<Lane> lanes, std::vector<Line> roadBoundary);
	Road(int roadId, std::vector<Lane> lanes, std::vector<Line> roadBoundary, std::vector<RoadLink> roadLinks);
	~Road();
  	bool hasStopLine();
  	Line* getStopLine();
   	std::vector<Line> getRoadBoundary();
   	std::vector<Lane> getLanes();
	std::vector<RoadLink> getRoadLinks();
	void setLanes(std::vector<Lane> lanes);
	void setStopLine(Line stopLine);
	int getId();
  
private: 

	void assignLaneNumbers();

	int m_id;	
	std::vector<Lane> m_lanes;
	std::vector<Line> m_roadBoundary;
	std::vector<RoadLink> m_roadLinks;
	std::vector<IntersectionLink> m_interLinks;
   
   };
   
#endif /*ROAD_HH_*/
