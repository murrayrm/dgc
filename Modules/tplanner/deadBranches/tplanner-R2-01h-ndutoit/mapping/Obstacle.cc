#include "Obstacle.hh"



Obstacle::Obstacle(int obsId, GeometricConstraints& constraints, 
		   std::vector<double> pos, std::vector<double> vel, double velMag, 
		   std::vector<double> posUncert, std::vector<double> velUncert,
		   double velMagUncert, bool exists, double existsUncert, bool traversable,
		   double traversUncert)
  : m_id(obsId)
  , m_geometry(constraints)
  , m_pos(pos)
  , m_vel(vel)
  , m_velMag(velMag)
  , m_posUncert(posUncert)
  , m_velUncert(velUncert)
  , m_velMagUncert(velMagUncert)
  , m_exists(exists)
  , m_existsUncert(existsUncert)
  , m_traversable(traversable) 
  , m_traversUncert(traversUncert)
  , m_decay(0)
{

}
   	
   	
Obstacle::Obstacle(int obsId)
  : m_id(obsId)
{
   		
}
   	
   	
int Obstacle::getId() {
  return m_id;
}

std::vector<double> Obstacle::getPosition(){  

  return m_pos;
}


std::vector<double> Obstacle::getPositionUncertainty(){  
  return m_posUncert;
}

std::vector<double> Obstacle::getVelocityVector(){  
  return m_vel; 
}

std::vector<double> Obstacle::getVelocityVectorUncertainty(){  
  return m_velUncert;
}

double Obstacle::getVelocityMagnitude(){  
  return m_velMag;
}

double Obstacle::getVelocityMagnitudeUncertainty(){  
  return 	m_velMagUncert;
}

GeometricConstraints Obstacle::getGeometry(){  
  return m_geometry;
}

GeometricConstraints Obstacle::getGeometryProjectionToRoad(){  
  GeometricConstraints projToRoad; 
  //TODO
  //calculate the convex hull in a plane 
  return projToRoad; 	
}

bool Obstacle::exists(){  
  return m_exists; 
}
bool Obstacle::isTraversable(){  
  return m_traversable; 
}	
double Obstacle::existenceUncertainty(){  
  return m_existsUncert; 

}
double Obstacle::traversabilityUncertainty(){  
  return m_traversUncert;
}

double Obstacle::getDecay(){
  return m_decay;
}

void Obstacle::setDecay(double decay) {
  m_decay = decay;	

}
   

