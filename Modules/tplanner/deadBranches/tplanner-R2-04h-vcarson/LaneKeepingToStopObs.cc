#include "LaneKeepingToStopObs.hh"
#include "LaneKeeping.hh"

LaneKeepingToStopObs::LaneKeepingToStopObs(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
  , m_minDistToObs(25)
{

}

LaneKeepingToStopObs::LaneKeepingToStopObs()
{

}

LaneKeepingToStopObs::~LaneKeepingToStopObs()
{

}

double LaneKeepingToStopObs::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

  cout << "in LaneKeepingToStopObs::meetTransitionConditions - this is the one with obstacles" << endl;


    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      // check if there is an obstacle to worry about at all
      MapElement obstacle;
      LaneLabel desiredLaneLabel = AliceStateHelper::getDesiredLaneLabel();
      double distToObs = TrafficUtils::getNearestObsInLane(obstacle, map, vehState, desiredLaneLabel);
      if ((distToObs>=0)&&(distToObs<40) && TrafficUtils::isObsStatic(obstacle)){
        cout << "--------- THINKS THERE IS AN OBSTACLE" << endl;
        SegGoals seg = planHorizon.getSegGoal(0);

        // check if we are queueing
        bool isQueue = controlState->getIsQueueing();
        // check if passing is allowed
        bool passing_allowed = seg.illegalPassingAllowed;
        // check if lane blocked
        // TODO: think through what the correct final condition is that should be used here
        PointLabel exitPtLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        //      point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        point2 exitPt, finalPt;
        map->getWayPoint(exitPt, exitPtLabel);

        finalPt.set(exitPt);
        bool lane_blocked = TrafficUtils::isLaneBlocked(map, vehState, finalPt, desiredLaneLabel);
        
        if (!passing_allowed && !isQueue && lane_blocked) {
          m_prob = 1;
        }
      } else {
        m_prob = 0;
      }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
      
      m_prob = 0;
      break;
      
    default:
      
      m_prob = 0;
      cerr << "LaneKeepingToStopObs.cc with obstacles: Undefined Traffic state" << endl;
      
    }

    setUncertainty(m_prob);
    
    return m_prob;
}
