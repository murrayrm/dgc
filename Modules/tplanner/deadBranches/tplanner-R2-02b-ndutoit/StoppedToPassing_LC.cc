#include "StoppedToPassing_LC.hh"
#include <math.h>
#include <list>

StoppedToPassing_LC::StoppedToPassing_LC(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

StoppedToPassing_LC::StoppedToPassing_LC()
{

}

StoppedToPassing_LC::~StoppedToPassing_LC()
{

}

double StoppedToPassing_LC::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToPassing_LC::meetTransitionConditions" << endl;
    }
    
    switch (trafficState->getType()) {
    
     case TrafficStateFactory::ZONE_REGION:
     {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        bool clear = true;
        if (clear) {
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToPassing_LC: ZONE_REGION = CLEAR" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    case TrafficStateFactory::ROAD_REGION:
    {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        bool clear = false;
        if (clear) {
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToPassing_LC: ROAD_REGION = CLEAR" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        sleep(3);
        if ((m_verbose) || (m_debug)) {
            cout << "checking to see if intersection is clear" << endl;
        }
        
        bool intersectionClear = true;
        // Check if it is our turn
        // TODO: DO THIS PROPERLY
        bool ourTurn = true;

        if ((ourTurn) && (intersectionClear)) {
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToPassing_LC: INTERSECTION_STOP = CLEAR and OURTURN" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    default:
    
        m_probability = 0;
        cerr << "StoppedToPassing_LC.cc: Undefined Traffic state" << endl;
    
    }
    
    if ((m_verbose) || (m_debug)) {
        cout << "probability = " << m_probability << endl;
    }
    setUncertainty(m_probability);
    
    return m_probability;
}
