#include "Intersection_LaneKeepingToStop.hh"
#include <math.h>
#include <list>

/**
 * This class handles the transition from LaneKeepingToStop when approaching an intersection
 * In case of an obstacle, the standard LaneKeepingToStop is used!!!
 */

Intersection_LaneKeepingToStop::Intersection_LaneKeepingToStop(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
{

}

Intersection_LaneKeepingToStop::Intersection_LaneKeepingToStop()
{

}

Intersection_LaneKeepingToStop::~Intersection_LaneKeepingToStop()
{

}

double Intersection_LaneKeepingToStop::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon horiz, Map * localMap, VehicleState vehState)
{

    switch (trafficState->getType()) {
    
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        
        if ((m_verbose) || (m_debug)) {
            cout << "in LaneKeepingToSlowDown: APPROACH_INTER_SAFETY" << endl;
        }
        
        double currVel = 0;
        double delta = 0;
        double dstop = 0;
        int stopLineErr = -1;
        point2 stopLinePos, currFrontPos;
        PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
        currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        currVel = AliceStateHelper::getVelocityMag(vehState);
        delta = -pow(currVel, 2) / (2 * m_desiredDecel);
        // Use getStopline() call based on our current pos here
        stopLineErr = localMap->getNextStopline(stopLinePos, label);
        // Or should we use: stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
        dstop = stopLinePos.dist(currFrontPos);
        cout << "distance to stop = " << dstop << endl;
        if (dstop <= delta) {
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;

    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
   
        m_probability = 0;
        break;
    
    default:
    
        m_probability = 0;
        cerr << "LaneKeepingToSlowDown.cc: Undefined Traffic state" << endl;
    
    }

    setUncertainty(m_probability);
    
    return m_probability;
}
