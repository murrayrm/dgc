#include "ZoneToRoadRegion.hh"
//#include "TrafficPlanner.hh"
#include <math.h>


ZoneToRoadRegion::ZoneToRoadRegion(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distZoneExit(1)
{

}

ZoneToRoadRegion::~ZoneToRoadRegion()
{

}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    if ((m_verbose) || (m_debug)) {
        cout << "in ZoneToRoadRegion::meetTransitionConditions(), SHOULD NOT BE HERE " << endl;
    }
    return 0.0;
}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel pointLabel)
{
  cout<<"IN ZONETOROADREGION meetTransitionConditions"<<endl;

    if ((m_verbose) || (m_debug)) {
        cout << "in ZoneToRoadRegion::meetTransitionConditions() " << endl;
    }
    
    // This is some distance to some waypoint
    point2 exitWaypt;
    point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);
    localMap->getWaypoint(exitWaypt, pointLabel);

    cout<<"ZONETOROAD EXIT WAYPT: "<<exitWaypt<<endl;

    double angle;
    localMap->getHeading(angle, pointLabel);

    cout << "heading = " << angle << endl;

    if ((m_verbose) || (m_debug)) {
        cout << "heading = " << angle << endl;
    }
    double dotProd = (-exitWaypt.x + currPos.x) * cos(angle) + (-exitWaypt.y + currPos.y) * sin(angle);

     cout << "in iscomplete(): dot product  = " << dotProd << endl;

    if ((m_verbose) || (m_debug)) {
        cout << "in iscomplete(): dot product  = " << dotProd << endl;
    }

    double AliceHeading = AliceStateHelper::getHeading(vehState);
    double absDist = exitWaypt.dist(currPos);
    double headingDiff = fmod(angle-AliceHeading,M_PI);

    if ((dotProd > -m_distZoneExit) && (fabs(headingDiff)<M_PI/12) && (absDist<10)) {
        m_trafTransProb = 1;
        LaneLabel lane(pointLabel.segment, pointLabel.lane);
        AliceStateHelper::setDesiredLaneLabel(lane);
    } else
        m_trafTransProb = 0;
    setUncertainty(m_trafTransProb);
    
    return m_trafTransProb;
}
