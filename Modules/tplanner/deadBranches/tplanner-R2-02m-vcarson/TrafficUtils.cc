#include "TrafficUtils.hh"
#include <math.h>

#define EPS 0.1


double TrafficUtils::calculateDistance(Location loc1, Location loc2) 
{
	return sqrt(pow(loc1.getNorthing() - loc2.getNorthing(),2) +	
			  pow(loc1.getEasting() - loc2.getEasting(),2));

}


double TrafficUtils::calculateDistance(point2 pt1, point2 pt2) 
{
  return sqrt(pow(pt1.x -pt2.x, 2) + pow(pt1.y -pt2.y, 2)); 

}

int TrafficUtils::getClosestPtIndex(point2arr boundary, point2 point)
{
  // to specified point x,y
	int index;
  int lengthBound = boundary.size();
  double distance = 1000000.0;
  
  // closest point on Boundary
  for (int ii = 0; ii<lengthBound; ii++)
  {
    double distance_temp = point.dist(boundary[ii]);
    if (distance_temp<distance)
    {
      distance=distance_temp;
      index=ii;
    }
  }
  return index;
}

int TrafficUtils::insertPtAtDistance(point2arr& boundary, int index, double dist)
{
  double curDist = 0;
  if (dist == 0) {
    return boundary.insert((unsigned int)index, boundary[index]);
  }
  if (index >= boundary.size() || index < 0) {
    cerr << "insertPtAtDistance:Index out of range" << endl;
    return -1;
  }

  //  cerr << "in insertPtAtDistance: " << index << ", " << dist << endl;
  
  if (dist > 0) {
    for (int i=index; i<boundary.size()-1; i++)
    {
      curDist += boundary[i].dist(boundary[i+1]);
      if (curDist > dist) //point lies between i, i+1
      {
        double d = boundary[i].dist(boundary[i+1]);
        double fac = (curDist - dist) / d;
        if (fac > 1) fac = 1;
        point2 pt = (fac*boundary[i] + (1-fac)*boundary[i+1]);
        return boundary.insert((unsigned int)(i+1),pt);
      }
    }
    //point lies past end
    point2 delta = boundary.back() - boundary[boundary.size()-2];
    double dLeft = dist - curDist;
    delta.normalize();
    delta = delta*dLeft;
    point2 pt = boundary.back() + delta;
    boundary.push_back(pt);
    return boundary.size() - 1;
  }
  else if (dist < 0) {
    for (int i=index; i>0; i--)
    {
      curDist += boundary[i].dist(boundary[i-1]);
      if (curDist > dist) //point lies between i, i-1
      {
        double d = boundary[i].dist(boundary[i-1]);
        double fac = (curDist - dist) / d;
        if (fac > 1) fac = 1;
        point2 pt = (fac*boundary[i] + (1-fac)*boundary[i-1]);
        return boundary.insert((unsigned int)i,pt);
      }
    }
    point2 delta = boundary[0] - boundary[1];
    double dLeft = dist - curDist;
    delta.normalize();
    delta = delta*dLeft;
    point2 pt = boundary[0] + delta;
    return boundary.insert(0,pt);
  }
  return -1;
}

int TrafficUtils::getAdjacentLane(Map * map, point2 pos, bool right)
{
  int laneID = map->getLaneID(pos);
  LaneLabel label(map->getSegmentID(pos),laneID);
  //add centerpoint->right or left point vector to r or l pt
  point2 cpt;
  int ret = map->getLaneCenterPoint(cpt, label, pos, 0);
  if (ret != 0)
    cerr << "getAdjacentLane: error retrieving center point" << endl;
  point2 ept;
  if (right) {
    ret = map->getLaneRightPoint(ept, label, pos, 0);
    if (ret != 0)
      cerr << "getAdjacentLane: error retrieving edge point" << endl;
  }
  else {
    ret = map->getLaneLeftPoint(ept, label, pos, 0);
    if (ret != 0)
      cerr << "getAdjacentLane: error retrieving edge point" << endl;
  }
  ept = ept - pos;
  ept = pos + 2*ept;
  return map->getLaneID(ept);
}

int TrafficUtils::insertProjPtInBoundary(point2arr& boundary, point2 point)
{
  int index, insertIndex;
  point2arr dataToFit;
  int lengthBound = (int)boundary.size();
  // find the index of the closest pt on the boundary
  index = getClosestPtIndex(boundary, point);
  // cout << "index of closest point is: " << index << endl;

  if (index > 0 && index+1 < lengthBound){
      dataToFit.push_back(boundary[index-1]);
      dataToFit.push_back(boundary[index]);
      dataToFit.push_back(boundary[index+1]);
  } else if (index == 0 && index+1 < lengthBound) {
      dataToFit.push_back(boundary[index]);
      dataToFit.push_back(boundary[index+1]);
  } else if (index > 0 && index+1 == lengthBound) {
      dataToFit.push_back(boundary[index-1]);
      dataToFit.push_back(boundary[index]);
  } else {// case where only one point is given
    cout << "TrafficUtils::insertPtOnBoundary() only one pt on boundary - cannot fit a line to insert the closest point" << endl;
    return -1;
  }

  // then find best approximation to the point on the line
  // transform into coordinate frame at begin pt of line x-axis along line
  int ptsToFit = (int)dataToFit.size();
  double theta = atan2(dataToFit[ptsToFit-1].y - dataToFit[0].y, dataToFit[ptsToFit-1].x - dataToFit[0].x); 
  double R11 = cos(theta);
  double R12 = -sin(theta);
  double R21 = sin(theta);
  double R22 = cos(theta);
  point2 d_vec = boundary[index];
  point2 posXYPt_line, posBDPt_line, posBDPt_local;
  posXYPt_line.x = R11*(point.x-d_vec.x)+R21*(point.y-d_vec.y);
  posXYPt_line.y = 0;  // this does not matter - is not used
  // find projection of point onto x-axis, which is aligned with line
  posBDPt_line.x = posXYPt_line.x;
  //cout << "projected pt x = " << posBDPt_line.x << endl;
  posBDPt_line.y = 0;
  // transform new point back to local frame
  posBDPt_local.x = d_vec.x + R11*posBDPt_line.x + R12*posBDPt_line.y;
  posBDPt_local.y = d_vec.y + R21*posBDPt_line.x + R22*posBDPt_line.y;

  //  cout << "posDPt_line.x = " << posBDPt_line.x << endl;
  if (posBDPt_line.x > EPS) {
    insertIndex = index+1;
    //  cout << "insert index = " << insertIndex << endl;
    boundary.insert((unsigned int)insertIndex, posBDPt_local);
  } else if (posBDPt_line.x<-EPS) {
    insertIndex = index;
    //  cout << "insert index = " << insertIndex << endl;
    boundary.insert((unsigned int)insertIndex, posBDPt_local);
  }
  return 0;
}

// Adds two angles and adjust the sum according to the PI/-PI convention
int TrafficUtils::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}
