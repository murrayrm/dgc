#ifndef LANECHANGETOLANEKEEPING_HH_
#define LANECHANGETOLANEKEEPING_HH_

#include "ControlStateTransition.hh"
#include "LaneKeeping.hh"

class LaneChangeToLaneKeeping:public ControlStateTransition {

public:
  LaneChangeToLaneKeeping(ControlState *state1, ControlState *state2);
  LaneChangeToLaneKeeping();
  ~LaneChangeToLaneKeeping();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
