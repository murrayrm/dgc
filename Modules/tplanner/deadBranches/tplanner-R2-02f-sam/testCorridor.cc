#include <iostream>
#include "Corridor.hh"
#include "frames/point2.hh"

int main(int argc,char** argv)
{
  Corridor corr = Corridor();

  point2arr leftBound = point2arr();
  point2 pt=point2(0.0,0.0);
  leftBound.push_back(pt);
  pt=point2(3.0,0.0);
  leftBound.push_back(pt);
  pt=point2(6.0,2.0);
  leftBound.push_back(pt);
  pt=point2(9.0,2.0);
  leftBound.push_back(pt);
  pt=point2(14.0,6.0);
  leftBound.push_back(pt);
  pt=point2(9.0,10.0);
  leftBound.push_back(pt);
  pt=point2(5.0,10.0);
  leftBound.push_back(pt);


  corr.addPolyline(leftBound);

  point2arr rightBound = point2arr();
  pt=point2(0.0,2.0);
  rightBound.push_back(pt);
  pt=point2(3.0,2.0);
  rightBound.push_back(pt);
  pt=point2(6.0,4.0);
  rightBound.push_back(pt);
  pt=point2(9.0,4.0);
  rightBound.push_back(pt);
  pt=point2(12.0,6.0);
  rightBound.push_back(pt);
  pt=point2(9.0,8.0);
  rightBound.push_back(pt);
  pt=point2(5.0,8.0);
  rightBound.push_back(pt);
  corr.addPolyline(rightBound);
  corr.generatePolyCorridor(); 
  cout << " Printing the polytopic corridor, now. " << endl; 
  corr.printPolyCorridor();
  
  leftBound.clear();
  pt=point2(0.0,0.0);
  leftBound.push_back(pt);
  pt=point2(3.0,0.0);
  leftBound.push_back(pt);
  pt=point2(6.0,2.0);
  leftBound.push_back(pt);
  pt=point2(9.0,2.0);
  leftBound.push_back(pt);
  pt=point2(14.0,6.0);
  leftBound.push_back(pt);
  pt=point2(9.0,10.0);
  leftBound.push_back(pt);
  pt=point2(5.0,10.0);
//  leftBound.push_back(pt);

  corr.generatePolyCorridor(); 
  cout << " Printing the polytopic corridor, now. " << endl; 
  corr.printPolyCorridor();
  corr.sendPolyCorridor();
  sleep(20);

  Corridor corr2 = Corridor();
  corr2.addPolyline(leftBound);

  rightBound.clear();
  pt=point2(0.0,2.0);
  rightBound.push_back(pt);
  pt=point2(3.0,2.0);
  rightBound.push_back(pt);
  pt=point2(6.0,4.0);
  rightBound.push_back(pt);
  pt=point2(9.0,4.0);
  rightBound.push_back(pt);
  pt=point2(12.0,6.0);
  rightBound.push_back(pt);
  pt=point2(9.0,8.0);
  rightBound.push_back(pt);
  pt=point2(5.0,8.0);
//  rightBound.push_back(pt);
  corr2.addPolyline(rightBound);
  corr2.generatePolyCorridor(); 
  cout << " Printing the polytopic corridor  2 , now. " << endl; 
  corr2.printPolyCorridor();
  corr2.sendPolyCorridor();

}
