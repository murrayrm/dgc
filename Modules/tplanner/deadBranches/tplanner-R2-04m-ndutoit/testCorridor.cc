#include <iostream>
#include "Corridor.hh"
#include "frames/point2.hh"

int main(int argc,char** argv)
{
  Corridor corr = Corridor();

  point2arr leftBound = point2arr();
  point2 pt=point2(-2.5215,-44.6711);
  leftBound.push_back(pt);
  pt=point2(-2.5299,-48.3088);
  leftBound.push_back(pt);

  corr.addPolyline(leftBound);

  point2arr rightBound = point2arr();
  pt=point2(1.1277,-48.3142);
  rightBound.push_back(pt);
  pt=point2(1.1361,-44.67957);
  rightBound.push_back(pt);
  
  corr.addPolyline(rightBound);
  corr.generatePolyCorridor(); 
  cout << " Printing the polytopic corridor, now. " << endl; 
  corr.printPolyCorridor();
  corr.sendPolyCorridor();
  
  return 0;  
  leftBound.clear();
  pt=point2(0.0,0.0);
  leftBound.push_back(pt);
  pt=point2(3.0,0.0);
  leftBound.push_back(pt);
  pt=point2(6.0,2.0);
  leftBound.push_back(pt);
  pt=point2(9.0,2.0);
  leftBound.push_back(pt);
  pt=point2(14.0,6.0);
  leftBound.push_back(pt);
  pt=point2(9.0,10.0);
  leftBound.push_back(pt);
  pt=point2(5.0,10.0);
//  leftBound.push_back(pt);

  corr.generatePolyCorridor(); 
  cout << " Printing the polytopic corridor, now. " << endl; 
  corr.printPolyCorridor();
  corr.sendPolyCorridor();
  sleep(3);

  Corridor corr2 = Corridor();
  corr2.addPolyline(leftBound);

  rightBound.clear();
  pt=point2(0.0,2.0);
  rightBound.push_back(pt);
  pt=point2(3.0,2.0);
  rightBound.push_back(pt);
  pt=point2(6.0,4.0);
  rightBound.push_back(pt);
  pt=point2(9.0,4.0);
  rightBound.push_back(pt);
  pt=point2(12.0,6.0);
  rightBound.push_back(pt);
  pt=point2(9.0,8.0);
  rightBound.push_back(pt);
  pt=point2(5.0,8.0);
//  rightBound.push_back(pt);
  corr2.addPolyline(rightBound);
  int c=0;
  while(c<10)
  { c++;
  corr2.generatePolyCorridor(); 
  cout << " Printing the polytopic corridor  2 , now. " << endl; 
  corr2.printPolyCorridor();
  corr2.sendPolyCorridor();
  sleep(1);
}
}
