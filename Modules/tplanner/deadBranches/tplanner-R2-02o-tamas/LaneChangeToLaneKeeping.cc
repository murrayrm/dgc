#include "LaneChangeToLaneKeeping.hh"
#include "LaneChange.hh"

LaneChangeToLaneKeeping::LaneChangeToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

LaneChangeToLaneKeeping::LaneChangeToLaneKeeping()
{

}

LaneChangeToLaneKeeping::~LaneChangeToLaneKeeping()
{

}

double LaneChangeToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in LaneChangeToLaneKeeping::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        LaneChange *laneChange = dynamic_cast<LaneChange*>(controlState);
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

        int laneID = laneChange->getLaneChangeID();
	LaneLabel currLane;
	int laneErr = map->getLane(currLane, AliceStateHelper::getPositionRearBumper(vehState));
	if (laneErr != 0)
	  break;

        if (currLane.lane == laneID) {
          m_prob = 1;
	  LaneKeeping *laneKeeping = dynamic_cast<LaneKeeping*>(this->getControlStateTo());
	  if (laneKeeping == 0)
	    cerr << "Could not get LaneKeeping object..." << endl;
	  //make sure LaneKeeping knows about whether it is going in reverse
	  laneKeeping->setReverse((laneChange->getReverse() == 1));
        }
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
        
        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "LaneChangeToLaneKeeping.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
