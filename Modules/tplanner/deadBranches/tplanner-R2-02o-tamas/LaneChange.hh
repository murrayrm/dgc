#ifndef LANECHANGE_HH_
#define LANECHANGE_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"

class LaneChange:public ControlState {

  public:

    LaneChange(int stateId, ControlStateFactory::ControlStateType type);
    ~LaneChange();
    virtual int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
    void setLaneChangeID(int destLane);
    int getLaneChangeID();
  void setReverse(int reverse);
  int getReverse();

private:
  int desiredLaneID;
  int isReverse; //stores what lane is reverse
  //1: destination, 0: none, -1: initial
};

#endif                          /*LANECHANGE_HH_ */
