#include "Pause.hh"

Pause::Pause(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
, m_desDecc(-0.5)
{

}

Pause::~Pause()
{

}

int Pause::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    /* This is not in LaneKeeping + not initialized by used! */
    /* double IC_velMin, IC_velMax; */

  int errors = 0;
    point2arr leftBound, rightBound, leftBound1, rightBound1;
    double velIn;

    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 FC_finalPos;
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
    double heading;

    bool foundTraffState = true; 

    cout<<"PAUSE.determineCorridor - traffState type "<<traffState->toString();

    switch (traffState->getType()) {
    
    case TrafficStateFactory::ZONE_REGION:
    {
      

	/* Compute the exit waypoint because it doesn't exist for a PAUSE*/

        double corrHalfWidth = 3;
        double theta = AliceStateHelper::getHeading(vehState);

        point2 temppt, exitWaypt;

        /* Calculate the shortest distance to come to a stop*/
        velIn = AliceStateHelper::getVelocityMag(vehState);
        double distToStop = -pow(velIn,2)/(-2*VEHICLE_MAX_DECEL); 

        exitWaypt.x = cos(theta)*distToStop + currFrontPos.x; 
        exitWaypt.y = sin(theta)*distToStop + currFrontPos.y; 

        // Left Boundary
        // pt 1 on left boundary
        temppt.x = currRearPos.x + corrHalfWidth * cos(theta + M_PI / 2);
        temppt.y = currRearPos.y + corrHalfWidth * sin(theta + M_PI / 2);
        leftBound.push_back(temppt);
	
        // pt 2 on left boundary
        temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
        temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
        leftBound.push_back(temppt);

        // Right Boundary
        // pt 1 on right boundary
        temppt.x = currRearPos.x + corrHalfWidth * cos(theta - M_PI / 2);
        temppt.y = currRearPos.y + corrHalfWidth * sin(theta - M_PI / 2);
        rightBound.push_back(temppt);

        // pt 2 on right boundary
        temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
        temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
        rightBound.push_back(temppt);


        // TODO: need to think through pause condition in the Zone Region
        FC_finalPos = currFrontPos;
        FC_velMin = 0;

        // Specify the ocpParams final conditions - lower bounds
        FC_headingMin = theta;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // Specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = theta;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
    break;

    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
     
      SegGoals currSegment = planHoriz.getSegGoal(0);
        // Want to plan for current segment only
        // TODO: Want to use function that does not specify specific lane
        // LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, desiredLane,  currFrontPos, range);
        if (getBoundsErr != 0) {
            cerr << "LaneKeeping.cc: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }

        // Set final conditions
	
        /* Calculate the shortest distance to come to a stop*/
        velIn = AliceStateHelper::getVelocityMag(vehState);
        cout << "Pause: curr velocity = " << velIn << endl;
        if (velIn>0.2) {
          double distToStop = -pow(velIn,2)/(-2*VEHICLE_MAX_DECEL); 
          cout << "Pause: distToStop = " << distToStop << endl;
          localMap->getLaneCenterPoint(FC_finalPos, desiredLane, currFrontPos, distToStop); 
        } else {
          cout << "Pause: vel < 0.1 so final pos = curr pos to invoke stopped condition" << endl;
          FC_finalPos = currFrontPos;
        }
        
        FC_velMin = 0;
        localMap->getHeading(heading,FC_finalPos);

        // Specify the ocpParams final conditions - lower bounds
        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // Specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
      SegGoals currSegment = planHoriz.getSegGoal(0);
      // Want to plan over two segments
      SegGoals nextSegGoal = planHoriz.getSegGoal(1);
      point2arr leftBound1, rightBound1, leftBound2, rightBound2;
      
      // Set of boundary points due to intersection lane
      PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
      if ((m_verbose) || (m_debug)) {
        cout << "intersection entry waypt id = " << ptLabelIn << endl;
      }
      
      PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
      if ((m_verbose) || (m_debug)) {
        cout << "intersection exit waypt id = " << ptLabelOut << endl;
      }
      
      double range = 50;
      int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
      if (interBoundErr != 0) {
        cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
        return (interBoundErr);
      }
      
      leftBound.set(leftBound1);
      rightBound.set(rightBound1);
      
      // Specify the ocpParams final conditions - lower bounds
      // Want to define this position based on the corridor?
      FC_finalPos = currFrontPos;
      FC_velMin = 0;
      localMap->getHeading(heading,FC_finalPos);
      
      FC_headingMin = heading;  // unbounded
      FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
      FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
      
      // Specify the ocpParams final conditions - upper bounds
      FC_velMax = 0;
      FC_headingMax = heading;   // unbounded
      FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
      FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
    break;        
    default:    
      cerr << "Pause.cc: Undefined Traffic state" << endl;
      foundTraffState = false; 
      break;

    }

    if ((m_verbose) || (m_debug)) {
      cout << "velocity profile: in = " << velIn << endl;
    }
    
    if (foundTraffState) {
      // Move back point so that we plan for the rear axle
      double easting = -DIST_REAR_AXLE_TO_FRONT*sin(heading);
      double northing = -DIST_REAR_AXLE_TO_FRONT*cos(heading);
      FC_finalPos.x = FC_finalPos.x+northing;
      FC_finalPos.y = FC_finalPos.y+easting;

      
      // Assign lines to corridor variables
      corr.addPolyline(leftBound);
      corr.addPolyline(rightBound);

      // set the final conditions in the ocpspecs
      corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
      corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
      corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
      corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
      corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
      corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
      // specify the ocpParams final conditions - upper bounds
      corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
      corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
      corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
      corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
      corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
      corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

      /* corr.setOCPinitialCondLB(VELOCITY_IDX_C, IC_velMin);
	 corr.setOCPinitialCondUB(VELOCITY_IDX_C, IC_velMax); */
    } else {
      ++errors;
      cout<<"PAUSE, NO CORRIDOR CREATED"<<endl;
    }
    return errors;
}
