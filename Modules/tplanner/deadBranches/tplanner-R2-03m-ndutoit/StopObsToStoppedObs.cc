#include "StopObsToStoppedObs.hh"
#include <math.h>
#include <list>

StopObsToStoppedObs::StopObsToStoppedObs(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
, m_boundStoppedVel(0.25)
{

}

StopObsToStoppedObs::StopObsToStoppedObs()
{

}

StopObsToStoppedObs::~StopObsToStoppedObs()
{

}

double StopObsToStoppedObs::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StopObsToStoppedObs::meetTransitionConditions" << endl;
    }
    
    double vel = 0;

    switch (trafficState->getType()) {

    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        vel = AliceStateHelper::getVelocityMag(vehState);

        if (vel <= m_boundStoppedVel) {
            if ((m_verbose) || (m_debug)) {
                cout << "STOPPED(OBS)!!" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    default:
    
        m_probability = 0;
        cerr << "StopObsToStoppedObs.cc: Undefined Traffic state" << endl;
    
    }
    
    setUncertainty(m_probability);
    return m_probability;
}
