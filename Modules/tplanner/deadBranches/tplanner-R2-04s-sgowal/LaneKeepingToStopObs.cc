#include "LaneKeepingToStopObs.hh"
#include "LaneKeeping.hh"

LaneKeepingToStopObs::LaneKeepingToStopObs(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
  , m_desiredDecel(-0.5)
  , m_minDistToObs(25)
{

}

LaneKeepingToStopObs::LaneKeepingToStopObs()
{

}

LaneKeepingToStopObs::~LaneKeepingToStopObs()
{

}

double LaneKeepingToStopObs::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

  cout << "in LaneKeepingToStopObs::meetTransitionConditions - this is the one with obstacles" << endl;


    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {

      // check if there is an obstacle to worry about at all
      MapElement obstacle;
      LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
      double distToObs = TrafficUtils::getNearestObsInLane(obstacle, map, vehState, desiredLane);

      if ((distToObs>=0) && (distToObs<40)) {

        cout << "--------- THINKS THERE IS AN OBSTACLE" << endl;

        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel exitPtLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        point2 exitPt;
        map->getWayPoint(exitPt, exitPtLabel);

        // check if we are queueing
        bool isQueue = controlState->getIsQueueing();

        // check if passing is allowed
        string lt, rt;
        LaneLabel label(seg.entrySegmentID, seg.entryLaneID);
        map->getLeftBoundType(lt, label);
        map->getRightBoundType(rt, label);
        bool passing_allowed = seg.illegalPassingAllowed || lt.compare("broken_white")==0 || rt.compare("broken_white")==0;

        /* Get the final point (reproduces CorridorUtils code for LaneKeeping)
         * TODO: Think about information sharing between Corridor and transitions */
        LaneKeeping *lk = dynamic_cast<LaneKeeping*>(getControlStateFrom());
        bool isReverse = lk->getReverse();
        point2arr lb, rb;
        double totRange = 50;
        double backRange = 5;
        if (isReverse)
          map->getBoundsReverse(lb, rb, desiredLane, currFrontPos, totRange);
        else
          map->getBounds(lb, rb, desiredLane, currFrontPos, totRange, backRange);

        point2 finalPt;
        point2 corrExitPt;
        if (lb.size() > 0  && rb.size() > 0)
          corrExitPt.set((lb.back().x+rb.back().x)/2,(lb.back().y+rb.back().y)/2);
        else corrExitPt.set(exitPt);

        point2arr centerline;
        map->getLaneCenterLine(centerline, desiredLane);
        double distanceToExitPt;
        map->getDistAlongLine(distanceToExitPt, centerline, exitPt, currFrontPos);
        double distanceToCorrExitPt;
        map->getDistAlongLine(distanceToCorrExitPt, centerline, corrExitPt, currFrontPos);

        cout << "LK_to_StopObs: distToExit = " << distanceToExitPt << ", distToCorrExit = " << distanceToCorrExitPt << endl;

        /* Same as CorridorUtils */
        if (!isReverse) {
          if (seg.stopAtExit) {
            if (distanceToExitPt<0) {
              finalPt.set(corrExitPt);
            } else if (distanceToCorrExitPt<0) {
              finalPt.set(exitPt);
            } else {
              if (distanceToExitPt<=distanceToCorrExitPt){
                finalPt.set(exitPt);
              } else {
                finalPt.set(corrExitPt);
              }
            }
          } else {
            finalPt.set(corrExitPt);
          }
        } else {
          finalPt.set(corrExitPt);
        }

        // Check for blockage
        bool lane_blocked = TrafficUtils::isLaneBlocked(map, vehState, finalPt, desiredLane);

        if (!passing_allowed && !isQueue && lane_blocked) {
          m_prob = 1;
        }
      } else {
        m_prob = 0;
      }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
      
      m_prob = 0;
      break;
      
    default:
      
      m_prob = 0;
      cerr << "LaneKeepingToStopObs.cc with obstacles: Undefined Traffic state" << endl;
      
    }

    setUncertainty(m_prob);
    
    return m_prob;
}
