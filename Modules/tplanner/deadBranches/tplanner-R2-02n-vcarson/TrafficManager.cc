#include "TrafficManager.hh"

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile) 
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
  , CSkynetContainer(MODtrafficplanner, skynetKey) 
    , m_snKey(skynetKey)
    , m_prevMergedDirectiveID(0)
    , m_currMergedDirectiveID(0)
    , m_isEndMission(false)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_logData(log)
    , m_isInit(false)
    , m_latestID(0)
    , m_currentID(0)
    , m_OCPparamsTalker(skynetKey, SNocpParams, MODtrafficplanner)
    , m_completed(false)
{

  m_controlGraph = ControlStateFactory::createControlStates();

  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionTrafficInterface::generateNorthface(skynetKey, this);

  if (true == log) {
    m_logger = new Log();
    m_logger->init("TRAFFMGR", true);
  }

  if (useRNDF) {
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, true, RNDFFile);
  } else { //TODO FIX this si not cool 
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, false, "");
  }

  this->setLogLevel(9);
  this->addLogfile("TrafficManager.log");
  m_currTraffState = TrafficStateFactory::getInitialState();
  m_currContrState = ControlStateFactory::getInitialState();
  m_prevGoalEndpoint = PointLabel(0,0,0);
  DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getLocalMapUpdate);
  
  /* TODO this needs to be removed change */
  m_rddfSocket = m_skynet.get_send_sock(SNrddf);
}

TrafficManager::~TrafficManager()
{

  MissionTrafficInterface::releaseNorthface(m_missTraffInterfaceNF);
  //TODO close file before deleting
  delete m_logFile;
  delete m_logger;
  TrafficStateEst::Destroy();
}

void TrafficManager::TrafficPlanningLoop(void)
{
  cout<<"Entered traffplanning loop"<<endl;
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective );
      control(&m_controlStatus, &m_mergedDirective );
      usleep(100000);
    }
}

void TrafficManager::arbitrate(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus *controlStatus = dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective *mergedDirective = dynamic_cast<TrafficManagerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus; 

  bool meetsPlanHorizReqs = false;

  if (!m_isInit) {
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    
    cout << "Sending intializing status goal " << segGoalsStatus.goalID << endl;
    cout<<"Response ID for initialiazing goal "<<segGoalsStatus.goalID<<endl;
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(5);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      cout << "Sending status goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(5);
    }
    cout << "Successfully notified mission planner that I'm joining the group" <<  endl;
    m_isInit = true;
  }
  
  cout<<"TRAFFMGR.arbitrate()"<<endl; 
  
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == TrafficManagerControlStatus::COMPLETED ||
      controlStatus->status == TrafficManagerControlStatus::FAILED) {
    
    if (controlStatus->status == TrafficManagerControlStatus::COMPLETED) {  
	segGoalsStatus.status = SegGoalsStatus::COMPLETED;
	segGoalsStatus.goalID = controlStatus->ID;
	if (controlStatus->wasPaused){
	  m_currContrState=ControlStateFactory::getInitialState();
	}
	cout<<"Response ID, COMPLETED "<<segGoalsStatus.goalID<<endl;
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
	if (m_accSegGoalsQ.size() > 0 ){ 
	  m_accSegGoalsQ.pop_front();
	} else { cout<<"FAILED - TRFMGR.m_accSegGoals.size()==0 "<<endl;}
    } else if (controlStatus->status == TrafficManagerControlStatus::FAILED)  {
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      segGoalsStatus.goalID = controlStatus->ID;
      cout<<"Response ID, FAILED "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
       
   
      /* Flush out the NF queue and send failure status */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
	SegGoals newDirective;
	m_missTraffInterfaceNF->getNewDirective( &newDirective );
	segGoalsStatus.goalID = newDirective.goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	cout<<"Response ID, FAILED "<<segGoalsStatus.goalID<<endl;
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      }
      
      /* Flush out the accumulating segGoals control queue and send failure status */
      while (m_accSegGoalsQ.size() > 0) {
	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	cout<<"Response ID, FAILED "<<segGoalsStatus.goalID<<endl;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	m_accSegGoalsQ.pop_front();
      }
    }
  }

  /* Wait for Route Planner to send a new directive */
  usleep(500000);

  /* Get all the new directives and put it in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);
    
    cout<<"TRFMGR - arbitrate has new directive ID "<<newDirective.goalID<<endl;
    cout<<"TRFMGR - m_accSegGoalsQ.size "<<m_accSegGoalsQ.size()<<endl;

    if ((m_accSegGoalsQ.size()>0) && (newDirective.goalID <= m_accSegGoalsQ.back().goalID)) {
      cout<<"continues..."<<endl;
      continue;
      
    }
    m_accSegGoalsQ.push_back(newDirective);

    cout<<"TRFMGR - newDIrective type "<<newDirective.segment_type<<endl;

    if (SegGoals::PAUSE == newDirective.segment_type) {
      cout<<"TRFMGR - arbitrate new directive is PAUSE "<<endl;
      SegGoalsStatus segGoalsStatus;
      
      /* First flush the queue where the seg goals are accumulating */
      while (SegGoals::PAUSE != m_accSegGoalsQ.front().segment_type) {
	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	cout<<"Response ID PAUSE,FAILED "<<segGoalsStatus.goalID<<endl;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	m_accSegGoalsQ.pop_front();
      }
      break; 
    }
  }

  if (m_accSegGoalsQ.size() > 0) {

    SegGoals newGoal =  m_accSegGoalsQ.front();
  
    if (SegGoals::PAUSE == newGoal.segment_type) {
      mergedDirective->segType = newGoal.segment_type;
      mergedDirective->id = newGoal.goalID;
      m_currSegGoals = newGoal; 
    } else if (SegGoals::UNKNOWN == newGoal.segment_type) {
      segGoalsStatus.goalID = newGoal.goalID;
      mergedDirective->segType = newGoal.segment_type;
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      cout<<"Response ID,UNKNOWN "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      cout<<"TRFMGR.arbitrate()- SegGoals of type UNKNOWN"<<endl;
         
    } else if (SegGoals::END_OF_MISSION == newGoal.segment_type) {
	mergedDirective->id = newGoal.goalID; 
	mergedDirective->segType = newGoal.segment_type; 
	m_currSegGoals = newGoal;

    } else {


      PointLabel exitWayptLabel(newGoal.exitSegmentID, newGoal.exitLaneID, newGoal.exitWaypointID);

      cout<<"TRFMGR.arbitrate()- SegGoals of type OTHER"<<endl;
   
      /* Determine traffic state */
      m_currTraffState = m_traffStateEst->determineTrafficState(exitWayptLabel);

      cout<<"TRAFMGR: trafficState is "<<m_currTraffState->toString()<<endl; 

      /* Update the current planning horizon and populate m_planHoriz */
      meetsPlanHorizReqs = determinePlanningHorizon(m_accSegGoalsQ);      

      /* Do not create a new merged directive until we have the correct planning horizon */

      if(meetsPlanHorizReqs) { /* if we had enough segGoals to be able to plan properly */

	m_currMergedPlanHoriz = m_currPlanHorizon;  

	cout<<"TRAFMGR: meets planning horizon requirements"<<endl;

	mergedDirective->id = newGoal.goalID; 
	mergedDirective->segType = newGoal.segment_type;
	m_currSegGoals = newGoal;

      } else { /* If the plan horiz is not met  */
	cout<<"TRFMGR - Plan Horizon is not met"<<endl;
      }
    }
  } else {
    cout<<"Waiting for MISSION PLANNER goal "<<endl;
  }  
}


void TrafficManager::control(ControlStatus* cs, MergedDirective* md) {

  static int corrCounter = 0;

  TrafficManagerControlStatus* controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  
  TrafficManagerMergedDirective* mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);

  PointLabel endMissionWayptLabel, exitWayptLabel;

  Map* localMap = m_traffStateEst->getMapAtLastEst();

  CorridorCreate cc;

  int errors = 0;

  cout<<"TRFMGR - in control()"<<endl;

  if ((mergedDirective->segType >= SegGoals::ROAD_SEGMENT) 
      && (mergedDirective->segType < SegGoals::UNKNOWN)) {
    
    VehicleState vehState = m_traffStateEst->getVehStateAtLastEst();
    //    Map* localMap = m_traffStateEst->getMapAtLastEst();

    /* Check to see what the status of the last merged directive is */
    if (SegGoals::UNKNOWN != m_currSegGoals.segment_type) {      
      if (SegGoals::PAUSE != m_currSegGoals.segment_type) {
	PointLabel currGoalEndpoint = PointLabel(m_currSegGoals.exitSegmentID,
						 m_currSegGoals.exitLaneID,
						 m_currSegGoals.exitWaypointID);
	m_completed = isGoalComplete(currGoalEndpoint, m_traffStateEst->getUpdatedVehState());
	controlStatus->wasPaused = false;
      } else if (SegGoals::END_OF_MISSION == m_currSegGoals.segment_type) {
	m_completed = true;
      } else {
	m_completed = isGoalComplete(m_traffStateEst->getUpdatedVehState());	  
	controlStatus->wasPaused = true;
      }
      
      if (m_completed) {
	cout << "GOAL " << m_currSegGoals.goalID << " COMPLETED!!!" << endl;  
	controlStatus->ID = m_currSegGoals.goalID; 
	controlStatus->status = TrafficManagerControlStatus::COMPLETED;      
	m_currSegGoals.segment_type = SegGoals::UNKNOWN;
      }
    }

    if (SegGoals::PAUSE == mergedDirective->segType) {

      cout<<"TRAFFMGR.control() Got PAUSE directive"<<endl;
      
      /* If there is only one merged Directive then get it and send a corridor pause directive */
      cc.name = CorridorCreate::PAUSE;
      cc.ID = mergedDirective->id; 
      
      m_currMergedDirectiveID = mergedDirective->id;
      
      /* Get into the PAUSE control action, once we get stauts back of successful PAUSE, then we reinitialize the control action*/
      m_currContrState =  ControlStateFactory::getPauseState();
      
      /* Determine PAUSE corridor */
      errors += m_currContrState->determineCorridor(m_currCorr, vehState, m_currTraffState, m_currMergedPlanHoriz, m_traffStateEst->getUpdatedMap());
      if (0 == errors) {
	m_currCorr.setVelProfile(localMap, vehState, m_currTraffState, m_currContrState, m_currMergedPlanHoriz);
      } else {

	cout<<"PAUSE or END rcvd, did not send corridor, vel profile"<<endl; 
      }
      
    } else if (SegGoals::END_OF_MISSION == mergedDirective->segType) {

      m_isEndMission = true;

      m_currContrState->determineCorridor(m_currCorr, vehState, m_currTraffState, m_currMergedPlanHoriz, m_traffStateEst->getUpdatedMap());
      /* Set the velocity to zero */
      vector<double> velProfile;
      velProfile.push_back(0); // entry pt
      velProfile.push_back(0); // exit pt
      m_currCorr.setVelProfile(velProfile);
      m_currCorr.setDesiredAcc(0);

      double FC_velMin =  0;
      double FC_velMax =  0;
      m_currCorr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
      m_currCorr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
          
    } else {

      if(!m_completed) {
	if ((SegGoals::END_OF_MISSION != mergedDirective->segType) 
	    && (SegGoals::PAUSE != mergedDirective->segType))  {
	  
	  cout<<"In corridor create loop "<< ++corrCounter<<endl;
	  
	  /* Update the current control action */
	  m_currContrState = determineControlAction();
	  
	  /* Set the previous merged directive member */
	  m_prevMergedDirectiveID = mergedDirective->id;
	  
	  //m_prevSegGoals = m_currSegGoals; 
	  
	  m_currCorr.initializeOCPparams(m_traffStateEst->getUpdatedVehState(), 
					 m_currMergedPlanHoriz.getSegGoal(0).minSpeedLimit, 
					 m_currMergedPlanHoriz.getSegGoal(0).maxSpeedLimit);
	  
	  /* Determine the corridor */	  
	  m_currContrState->determineCorridor(m_currCorr, vehState, m_currTraffState, m_currMergedPlanHoriz, m_traffStateEst->getUpdatedMap());
	  
	  m_currCorr.printPolylines();
	  
	  /* Set the velocity profile */
	  m_currCorr.setVelProfile(localMap, vehState, m_currTraffState, m_currContrState, m_currMergedPlanHoriz);
	  
	  /* Get the global to local delta */
	  m_gloToLocalDelta = m_traffStateEst->getUpdatedMap()->prior.delta;
	  
	  /* Print the velocity profile */
	  //m_currCorr.printVelProfile();
	  m_currCorr.adjustFCPosForRearAxle();
	  //m_currCorr.printICFC();
	  
	  /* Now cost map? */
	  controlStatus->ID = m_prevMergedDirectiveID; 
	  controlStatus->status = TrafficManagerControlStatus::EXECUTING;
	}
      }
    }
  
    cout<<"TRFMGR,control(): control state "<<m_currContrState->toString()<<endl; 

    if (0 == errors) {
      /* Now prepare parameters for sending */  
      /* Convert to RDDF */
      RDDF* rddf = m_currCorr.getRddfCorridor(m_gloToLocalDelta);
      
      /* Send RDDF */
      SendRDDF(m_rddfSocket,rddf);
      
      /* Convert to polytope representation */
      //m_currCorr.generatePolyCorridor();
      //m_currCorr.printPolyCorridor();
      //m_currCorr.sendPolyCorridor();
      
      if (!m_OCPparamsTalker.send(&m_currCorr.getOCPparams())){
	cout<< "TrafficPlanner: problem with send OCP parameters" << endl;
      }  
      delete rddf; 
      m_currCorr.clear();
    } else { cout<<"TRFMGR - failed with errors "<<endl;} 
  } else {
      cout<<"TRFMGR - SegGoals UNKNOWN or of invalid type"<<endl;
    }
}
    
ControlState* TrafficManager::determineControlAction() {
  ControlState* controlState;

  /* Get the vehicle state and the map at the time of the last traffic state estimation */
  VehicleState vehState = m_traffStateEst->getVehStateAtLastEst();
  Map* localMap = m_traffStateEst->getMapAtLastEst();

  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(m_currContrState); 

  unsigned int i = 0;
  ControlStateTransition* transition;

  for (int i=0; i < transitions.size(); i++) 
    {		
      transition = static_cast<ControlStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(m_currContrState, m_currTraffState, m_currPlanHorizon, localMap, vehState);
    }
   
  /* Save old initial conditions - TODO clean */
  VehicleState oldVehState, currVehState;
  point2 pos = m_currContrState->getInitialPosition();
  oldVehState.localX = pos.x;
  oldVehState.localY = pos.y;

  currVehState = m_traffStateEst->getUpdatedVehState();
  controlState = chooseMostProbableControlState(m_currContrState, transitions);

 
  if (m_currContrState->getType() != controlState->getType()) {
      controlState->setInitialPosition(currVehState);
      controlState->setInitialVelocity(currVehState);
      controlState->setInitialTime();
  } else if (m_currContrState->getStateId() != controlState->getStateId()) {
      controlState->setInitialPosition(oldVehState);
      controlState->setInitialVelocity(oldVehState);
      controlState->setInitialTime();
  }

  return controlState;
}


ControlState* TrafficManager::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
  ControlState* currControlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficManager::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        //cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
      currControlState = controlState; 
    }
  } else { 
    currControlState = controlState; 
  }
  
  return currControlState;
}


bool TrafficManager::determinePlanningHorizon(deque<SegGoals> currSegGoals) 
{
  TrafficStateFactory::TrafficStateType tStateType = 
    (TrafficStateFactory::TrafficStateType) m_currTraffState->getType();
  
  cout<<"TRFMGR: Size of currSegGoals "<<currSegGoals.size()<<endl;

  m_currPlanHorizon.clear();
  
  bool meetsPlanHorizonReqs = false; 

  if (currSegGoals.size() > 0 ) {
    /* We need to make sure the currSegGoals have the correct size or else we will seg fault */
    switch(tStateType) {
    case TrafficStateFactory::ZONE_REGION:// want to plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	cout<<"TRFMGR: ZONE_REGION added goals "<<endl;
	cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
	cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    case TrafficStateFactory::ROAD_REGION:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	cout<<"TRFMGR: ROAD_REGION added goals "<<endl;
	cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
	cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	cout<<"TRFMGR: APPROACH_INTER_SAFETY added goals "<<endl;
	cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
	cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    case TrafficStateFactory::INTERSECTION_STOP:
      // plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	m_currPlanHorizon.addGoal(currSegGoals.front()); 		
	currSegGoals.pop_front();
	cout<<"TRFMGR: INTERSECTION STOP added goals "<<endl;
	cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
	cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    default:
      break;
    };
  }   
  return meetsPlanHorizonReqs;
}

unsigned int TrafficManager::getNextUniqueMergedDirID() {
  return ++m_uniqueMergedDirID; 
}


bool TrafficManager::isGoalComplete(PointLabel exitWayptLabel, VehicleState vehState)
{
  double completeDist = 1;
  bool completed = false;
  double angle, dotProd, AliceHeading, absDist, headingDiff, currVel; 

  if (SegGoals::PAUSE == m_currSegGoals.segment_type) {

    if (AliceStateHelper::getVelocityMag(vehState) < 0.1) {
      completed = true;
    } else {
      completed = false; 
      cout<<"ALICE not at a stop yet..."<<endl;
    }

  } else {

    point2 exitWaypt, currPos;
  
    m_traffStateEst->getUpdatedMap()->getWaypoint(exitWaypt, exitWayptLabel);

    currPos = AliceStateHelper::getPositionFrontBumper(vehState);

    m_traffStateEst->getUpdatedMap()->getHeading(angle, exitWayptLabel);

    cout<<"TRFMGR::isGoalComplete "<<endl;
    cout<<"TRFMGR exit waypoint label "<<exitWayptLabel<<endl;
    cout<<"TRFMGR currPos "<<currPos<<endl;
    cout<<"TRFMGR exit waypoint "<<exitWaypt<<endl;

    dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

    if ((m_verbose)||(m_debug)){
      cout << "in iscomplete(): dot product  = " << dotProd << endl;
    }

    AliceHeading = AliceStateHelper::getHeading(vehState);
    absDist = exitWaypt.dist(currPos);
    headingDiff = fmod(angle-AliceHeading,M_PI);

    if((dotProd>-completeDist) && (fabs(headingDiff)<M_PI/12) && (absDist<10)) {
      /* when we get within 1 m of the hyperplane defined by the exit pt, goal complete */
      completed = true;
      cout<<"TRFMGR::isGoalComplete COMPLETED "<<endl; 
    }
    else {
      completed = false;
      cout<<"TRFMGR::isGoalComplete NOT COMPLETE "<<endl; 
  }
  }
  return completed;
}


bool TrafficManager::isGoalComplete(VehicleState vehState)
{
  return (AliceStateHelper::getVelocityMag(vehState) < 0.1);

}
