#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"

#include "TrafficState.hh"
#include "RoadRegion.hh"
#include "ZoneRegion.hh"
#include "ApproachInterSafety.hh"
#include "IntersectionStop.hh"

#include "ZoneToRoadRegion.hh"
#include "RoadToRoadRegion.hh"
#include "RoadToApproachInterSafety.hh"
#include "ApproachInterSafeToInterStop.hh"
#include "InterStopToRoadRegion.hh"

#include "Log.hh"

TrafficState *TrafficStateFactory::m_initState;

TrafficStateFactory::TrafficStateFactory()
{

}

TrafficStateFactory::TrafficStateFactory(bool debug, bool verbose, bool log)
{
    m_debug = debug;
    m_verbose = verbose;
    m_log = log;
    
    // need to set these variables in TrafficState too
    TrafficState::setOutputParams(debug, verbose, log);
    TrafficStateTransition::setOutputParams(debug, verbose, log);
}

TrafficStateFactory::TrafficStateFactory(CmdLineArgs cLArgs)
{
    m_debug = cLArgs.debug;
    m_verbose = cLArgs.verbose;
    m_log = cLArgs.log;
    
    // need to set these variables in TrafficState too
    TrafficState::setOutputParams(m_debug, m_verbose, m_log);
    TrafficStateTransition::setOutputParams(m_debug, m_verbose, m_log);
}


TrafficStateFactory::~TrafficStateFactory()
{

}

StateGraph TrafficStateFactory::createTrafficStates()
{

    vector < StateTransition * >trans;

    int stateId = 0;            // TODO: fix this nonsense (Noel or Vanessa, please explain)

    // Traffic States 
    ZoneRegion *zoneRegion = new ZoneRegion(++stateId, ZONE_REGION);
    m_initState = zoneRegion;
    RoadRegion *roadRegion = new RoadRegion(++stateId, ROAD_REGION);
    ApproachInterSafety *approachInterSafe = new ApproachInterSafety(++stateId, APPROACH_INTER_SAFETY);
    IntersectionStop *interStop = new IntersectionStop(++stateId, INTERSECTION_STOP);

    // Traffic State Transitions
    ZoneToRoadRegion *zoneRoadTrans = new ZoneToRoadRegion(zoneRegion, roadRegion);
    RoadToRoadRegion *roadRoadTrans = new RoadToRoadRegion(roadRegion, roadRegion);
    RoadToApproachInterSafety *roadApproachTrans = new RoadToApproachInterSafety(roadRegion, approachInterSafe);
    ApproachInterSafeToInterStop *interSafeStopTrans = new ApproachInterSafeToInterStop(approachInterSafe, interStop);
    InterStopToRoadRegion *interStopRoadTrans = new InterStopToRoadRegion(interStop, roadRegion);

    trans.push_back(zoneRoadTrans);
    trans.push_back(roadRoadTrans);
    trans.push_back(roadApproachTrans);
    trans.push_back(interSafeStopTrans);
    trans.push_back(interStopRoadTrans);

    return StateGraph(trans);
}

TrafficState* TrafficStateFactory::getInitialState()
{
  return m_initState;
}


int TrafficStateFactory::getNextId()
{
    return 0;
}


void TrafficStateFactory::print(int type)
{
    Log::getStream(1) << "Traffic State Type: ";
    switch (type) {
    case TrafficStateFactory::ROAD_REGION:
        Log::getStream(1) << "ROAD REGION" << endl;
        break;
    case TrafficStateFactory::ZONE_REGION:
        Log::getStream(1) << "ZONE REGION" << endl;
        break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
        Log::getStream(1) << "APPROACH INTERSECTION SAFETY" << endl;
        break;
    case TrafficStateFactory::INTERSECTION_STOP:
        Log::getStream(1) << "INTERSECTION STOP" << endl;
        break;
    default:
        Log::getStream(1) << "INVALID" << endl;
        break;
    };
}


string TrafficStateFactory::printString(int type)
{
    string tstate_type;
    switch (type) {
    case TrafficStateFactory::ROAD_REGION:
        tstate_type = string("ROAD REGION");
        break;
    case TrafficStateFactory::ZONE_REGION:
        tstate_type = string("ZONE REGION");
        break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
        tstate_type = string("APPROACH INTER SAFETY");
        break;
    case TrafficStateFactory::INTERSECTION_STOP:
        tstate_type = string("INTERSECTION STOP");
        break;
    default:
        tstate_type = string("INVALID");
        break;
    };
    return tstate_type;
}
