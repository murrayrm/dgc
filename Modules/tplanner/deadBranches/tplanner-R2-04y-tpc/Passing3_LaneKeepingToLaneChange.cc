#include "Passing3_LaneKeepingToLaneChange.hh"
#include "Log.hh"

Passing3_LaneKeepingToLaneChange::Passing3_LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing3_LaneKeepingToLaneChange::Passing3_LaneKeepingToLaneChange()
{

}

Passing3_LaneKeepingToLaneChange::~Passing3_LaneKeepingToLaneChange()
{

}

double Passing3_LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        Log::getStream(1) << "in Passing3_LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        // Set up some useful vars
        point2 rearPos = AliceStateHelper::getPositionRearBumper(vehState);
        LaneLabel destLane(planHorizon.getSegGoal(0).exitSegmentID, planHorizon.getSegGoal(0).exitLaneID);
	// Find center position of destination lane
        point2 destCenterPos; 
	int ret = map->getLaneCenterPoint(destCenterPos,destLane,rearPos,0);
        if (ret != 0) break;
        double delta_o = 30; // the obstacle distance in meters
	cerr << "in Passing3... " << endl;
        // SVEN: double distToObs = map->getObstacleDist(destCenterPos, 0);
        LaneLabel gotoLane = LaneLabel(planHorizon.getSegGoal(0).exitSegmentID,planHorizon.getSegGoal(0).exitLaneID);
        double distToObs = TrafficUtils::getNearestObsDist(map, vehState, gotoLane);
	cerr << "dist2obs: " << distToObs << endl;
	//either no obstacle or far-away obstacle
	bool obstacle_passed = (distToObs < 0) || (distToObs > delta_o);

	LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

	LaneLabel currLane;
	int laneErr = map->getLane(currLane, rearPos);
	if (laneErr == -1)
	  obstacle_passed=false;

        if ((planHorizon.getSegGoal(0).exitLaneID != currLane.lane) && obstacle_passed) {
	  AliceStateHelper::setDesiredLaneLabel(gotoLane);
	  //are we going in lane reverse?
	  LaneKeeping *laneKeeping = dynamic_cast<LaneKeeping*>(controlState);
	  laneChange->setReverse(laneKeeping->getReverse()?-1:0);
          m_prob = 1;
        }
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing3_LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
