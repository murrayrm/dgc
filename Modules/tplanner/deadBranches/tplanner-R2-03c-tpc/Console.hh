/*
 *  Console.hh
 *  Console
 *
 *  Created by Sven Gowal on 24.05.07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

class Console {

  public:
    static void init();
    static void destroy();
 
    static void erase_screen();
    static int cprintf(char *format, ...);
    static int cprintf(int row, int col, char *format, ...);
    static void set_row(int);
    static void set_col(int);

  private:
    static int row;
    static int col;
};
