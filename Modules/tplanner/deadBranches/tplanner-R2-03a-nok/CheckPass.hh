/*! CheckPass.hh
 * Christian Looman
 * May 09 2007
 */

#ifndef CHECKPASS_HH
#define CHECKPASS_HH
#define AHEAD 0
#define BEHIND 1

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
#include <cv.h>
#include <highgui.h>
#include <time.h>
//open cv stuff

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "state/AliceStateHelper.hh"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

/*! ParticleFilter class
 * This is the main class for map prediction where the information from 
 * mapping is predicted forward in time so that tplanner can make a decisions. This function inherits from StateClient and LocalMapTalker
 * \brief Main class for map prediction function  
 */ 

class ListToA
{
public:
  ListToA() {}
  ~ListToA() {}

  PointLabel WayPoint;
  MapId Id;
  double velocity;
  double time;
  double distance;
};

class CheckPass
{ 
public:


  /*! Contstructor */
  CheckPass();
  
   /*! Standard destructor */
  ~CheckPass();


  // Functions for passing static obstacle with dyn. oncoming traffic
  double getProbability(VehicleState vehState,Map* localMap);
  void checkForObstacles(VehicleState vehState, Map* localMap, LaneLabel lane,int searchDirection, double& distance_obstacle,double& vel_obstacle, double& size_obstacle);

  // Functions for dyn. obstacles at intersections
  bool checkIntersection(VehicleState vehState,Map* localMap);
  void populateWayPoints(Map* localMap, vector<PointLabel>& WayPointEntries, PointLabel InitialWayPointEntry);
  void findStoplines(Map* localMap, vector<PointLabel>& WayPoint, vector<PointLabel>& WayPoint_NoStop, vector<PointLabel>& WayPoint_WithStop);
  void createListTimeOfArrival(Map* localMap, vector<PointLabel>& WayPoint_WithStop, double distance);
  void checkExistenceObstacles(Map* localMap);
  bool checkClearance(Map* localMap, vector<PointLabel>& WayPointEntries,double distance_uncertain);
  void resetIntersection();

  // Variables for checking the intersection
  vector<ListToA> PrecedenceList;
  bool ListCreated,ClearToGo,LegalToGo,once;

private:

  
};

#endif  // CHECKPASS_HH

