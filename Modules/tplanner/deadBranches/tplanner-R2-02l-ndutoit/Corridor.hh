#ifndef CORRIDOR_HH_
#define CORRIDOR_HH_

class ControlState;

#include "dgcutils/RDDF.hh"
#include "mapping/GeometricConstraints.hh"
#include "Conflict.hh"
#include "frames/point2.hh"
#include <stdio.h>
#include <vector>
#include <math.h>
#include "dgcutils/ggis.h"
#include "state/AliceStateHelper.hh"
#include "map/Map.hh"
#include "interfaces/TpDpInterface.hh"
#include "cmap/CMapPlus.hh"
#include "bitmap/Polygon.hh"
#include "bitmap/BitmapParams.hh"
#include "TrafficState.hh"
//#include "ControlState.hh"
#include "PlanningHorizon.hh"
#include "ControlStateFactory.hh"
#include "TrafficUtils.hh"
#include <boost/serialization/vector.hpp>
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/Polytope.hh"
#include "CmdLineArgs.hh"

#define EPS 0.05
#define BIGNUMBER 1000000000.0f 

#undef PI
#define PI 3.1416
#define EXPFACT 1
#define EXPPERC 0.25

using namespace bitmap;

class Corridor {

public :  		

  Corridor();
  // These constructors are old, should use the one with the cmdlineargs
  Corridor(bool debug, bool verbose, bool log);
  Corridor(int sn_key, bool debug, bool verbose, bool log);
  // This is the constructor to use...
  Corridor(CmdLineArgs cLArgs);
  Corridor(const Corridor& corr);
  ~Corridor();

  GeometricConstraints* getGeometricConstraints();
  RDDF* getRddfCorridor(point2 gloToLocalDelta);

  int getControlStateId();
  void addPolyline(point2arr);
  vector<point2arr> getPolylines();
  void setDesiredAcc(double desAcc);
  void setAlicePrevPos(point2 alicePos);
  //  void updateDistFromConStateBegin(point2 alicePrevPos);
  vector<double> getVelProfile();
  double getDesiredAcc();
  void clear();

  void setVelProfile(vector<double> velProfile);
  void setVelProfile(Map *localMap, VehicleState vehState, TrafficState *currTrafState, ControlState *currControlState, PlanningHorizon planHoriz);
  void setSpeedProfileAcc(point2 initPos, double initSpeed, double finalSpeed, double desiredAcc);
  void setSpeedProfileDist(point2 initPos, double initSpeed, double finalSpeed, point2 finalPos);
  double getSpeedAtPoint(point2 evalPoint);
  vector<double> returnSpeedProfile();
  double getAccAtPoint(point2 evalPoint);
  double getSpeedAtNode(int node);
  double getAccAtNode(int node);


  void initializeOCPparams(VehicleState vehState, double vmin, double vmax);
  OCPparams getOCPparams();
  void setOCPfinalCondLB(int index, double cond);
  void setOCPfinalCondUB(int index, double cond);
  void setOCPinitialCondLB(int index, double cond);
  void setOCPinitialCondUB(int index, double cond);
  void convertOCPtoGlobal(point2 gloToLocalDelta);
  void setOCPmode(int mode);
  void adjustFCPosForRearAxle();

  void getOCPinitialPos(point2 &IC_posLB, point2 &IC_posUB);

  void paintLaneCostCenterLine(CMapPlus* map, int costLayer, int tempLayer, 
			       point2arr& leftbound, point2arr& rightbound);
  void convertMapElementToCost(CMapPlus* map, int costLayer, int tempLayer, 
			       MapElement& el);
  void getCostMap(CMapPlus* map, int costLayer, int tempLayer,  
		  VehicleState &state, Map* lmap);

  void convertLaneToPolygon(vector<Polygon>& polygons, point2arr& leftbound, 
			    point2arr& rightbound, float bval, float cval);
  void convertMapElementToPolygon(vector<Polygon>& polygons, MapElement& el,
				  float height2cost);
  void getBitmapParams(BitmapParams& bmparams, VehicleState &state, Map* lmap);

  void printICFC();
  void printVelProfile();
  void generatePolyCorridor();
  void printPolyCorridor(void);
  void sendPolyCorridor(void);

private: 
  bool m_verbose;
  bool m_debug;
  bool m_log;
  bool m_use_local;

  RDDF* convertToRddf(point2 gloToLocalDelta);  
  RDDF* convertToRddf_old(point2 gloToLocalDelta);  
  // These are internal functions for convertToRddf()
  void closept(double lnseg[4], double pt[2], double cpt[2]);
  void midpt(double pt1[2], double pt2[2], double mpt[2]);
  void radius(double mpt[2], double pt1[2], double pt2[2], double &radius);

  GeometricConstraints* m_geometricConstraints;
  
  Conflict* m_conflict;
  
  vector<point2arr> m_polylines; 

  vector<double> m_speedProfile;
  vector<double> m_accProfile;

  int m_controlStateId;

  vector<double> m_velProfile; // [V_entry V_exit]
  double m_desiredAcc;
  double m_distFromConStateBegin;
  point2 m_alicePrevPos;
  OCPparams m_ocpParams;
  CPolytope* m_polyCorridor;
  int m_nPolytopes;
  SkynetTalker<sendCorr> m_polyCorridorTalker;

};

#endif /*CORRIDOR_HH_*/
