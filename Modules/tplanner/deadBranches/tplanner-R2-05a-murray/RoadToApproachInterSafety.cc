#include "RoadToApproachInterSafety.hh"
#include <math.h>
#include "Log.hh"

RoadToApproachInterSafety::RoadToApproachInterSafety(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distSafeStop(30)
{

}

RoadToApproachInterSafety::~RoadToApproachInterSafety()
{

}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    Log::getStream(1) << "in RoadToApproachInterSafety::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
    return 0.0;
}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel ptLabel)
{
    Log::getStream(1) << "in RoadToApproachInterSafety::meetTransitionConditions() " << endl;
    
    double distance = 0;
    int stopLineErr = -1;
    point2 stopLinePos;
    /* PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint()); */
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

    // Use getNextStopline() call which gives the next stopline in my lane
    stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);
    if (stopLineErr!=-1){
      LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
      LaneLabel laneLabel(ptLabel.segment, ptLabel.lane);

      if (!(laneLabel == desiredLane)) {
        m_trafTransProb = 0;
      } else {
        point2arr centerline;
        localMap->getLaneCenterLine(centerline, laneLabel);
        localMap->getDistAlongLine(distance, centerline, stopLinePos, currFrontPos);
        Log::getStream(1) << "distance to stopline = " << distance << endl;
      
        if ((distance <= m_distSafeStop))
          m_trafTransProb = 1;
        else
          m_trafTransProb = 0;
      }     
    } else {
      m_trafTransProb = 0;
    }
    setUncertainty(m_trafTransProb);
    return m_trafTransProb;
}
