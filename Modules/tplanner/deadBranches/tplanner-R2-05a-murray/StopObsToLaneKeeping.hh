#ifndef STOPOBSTOLANEKEEPING_HH_
#define STOPOBSTOLANEKEEPING_HH_

#include "ControlStateTransition.hh"

class StopObsToLaneKeeping:public ControlStateTransition {

  public:


    StopObsToLaneKeeping(ControlState * state1, ControlState * state2);
    StopObsToLaneKeeping();
    ~StopObsToLaneKeeping();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:

    double m_prob;
    double m_desiredDecel;
    double m_distToObs;
    double m_minDistToObs;
};

#endif                          /*LANEKEEPINGTOSTOPOBS_HH_ */
