#include "TrafficState.hh"
bool TrafficState::m_verbose = false;
bool TrafficState::m_debug = false;
bool TrafficState::m_log = false;


TrafficState::TrafficState(int stateId, TrafficStateFactory::TrafficStateType type)
: State(stateId, type)
{
	m_stateID = stateId;
	m_type = type;
}

TrafficState::TrafficState()
{ 

}


TrafficState::~TrafficState()
{ 

}

void TrafficState::setOutputParams(bool debug, bool verbose, bool log)
{
  m_verbose = verbose;
  m_debug = debug;
  m_log = log;
}

TrafficStateFactory::TrafficStateType TrafficState::getType()
{
    return m_type;
}

int TrafficState::getStateID()
{
    return m_stateID;
}
