#include "TrafficManager.hh"
#include "Console.hh"


int TrafficManager::m_snKey = 0;

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile, 
			       bool console, bool laneCost, char* configFile) 
  : CSkynetContainer(MODtrafficplanner, skynetKey) 
  , GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 1000)
  , m_prevMergedDirectiveID(0)
  , m_currMergedDirectiveID(0)
  , m_isEndMission(false)
  , m_currCorr(skynetKey, debug, verbose, log, laneCost)
  , m_verbose(verbose)
  , m_debug(debug)
  , m_logData(log)
  , m_isInit(false)
  , m_latestID(0)
  , m_currentID(0)
  , m_followVehTalker(skynetKey, SNleadVehicleInfo, MODtrafficplanner)   
  , m_OCPparamsTalker(skynetKey, SNocpParams, MODtrafficplanner)
  , m_completed(false)
  , m_polyTalker(skynetKey, SNbitmapParams, MODtrafficplanner)
  , m_isTurnSignalOn(false)
  , m_turnSignalDirId(0)
  , m_turnedSignalBecauseOfLaneChange(false)
{
  /* Initialize skynet key */
  m_snKey = skynetKey;

  m_controlGraph = ControlStateFactory::createControlStates();

  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionTrafficInterface::generateNorthface(skynetKey, this);

  /*!\param GcInterface variable */
  m_traffAdriveInterfaceSF = AdriveCommand::generateSouthface(skynetKey, this);

  if (true == log) {
    m_logger = new Log();
    m_logger->init("TRAFFMGR", true);
  }

  m_console = console;
  if (console) {
    Console::init();
    display_console();
    debug = false;
    verbose = false;
  }

  if (useRNDF) {
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, true, RNDFFile);
  } else { //TODO FIX this si not cool 
    m_traffStateEst = TrafficStateEst::Instance(skynetKey, true, debug, verbose, log, false, "");
  }

  this->setLogLevel(9);
  this->addLogfile("TrafficManager.log");
  m_currTraffState = TrafficStateFactory::getInitialState();
  m_currContrState = ControlStateFactory::getInitialState();
  m_prevGoalEndpoint = PointLabel(0,0,0);
  DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getLocalMapUpdate);
  
  //Initialize parameters for painting cost map
  readConfigFile(configFile);

  /* TODO this needs to be removed change */
  m_rddfSocket = m_skynet.get_send_sock(SNrddf);

  /* Starting the thread for car following */
  DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getFollowObstacleUpdate);

  /* Starting the thread for car following */
  //DGCstartMemberFunctionThread(m_traffStateEst, &TrafficStateEst::getOppositeLaneObstacleUpdate);

}

TrafficManager::~TrafficManager()
{

  MissionTrafficInterface::releaseNorthface(m_missTraffInterfaceNF);
  //TODO close file before deleting
  delete m_logFile;
  delete m_logger;
  TrafficStateEst::Destroy();
}

void TrafficManager::TrafficPlanningLoop(void)
{
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective );
      control(&m_controlStatus, &m_mergedDirective );
    }
}

void TrafficManager::arbitrate(ControlStatus* cs, MergedDirective* md) {

  // Display time
  static uint64_t old_time = getTime();
  uint64_t new_time = getTime();
  printf("Time consumed = %.6f seconds\n", (new_time - old_time)/(double)1000000);
  old_time = getTime();

  TrafficManagerControlStatus *controlStatus = dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective *mergedDirective = dynamic_cast<TrafficManagerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus; 

  bool meetsPlanHorizReqs = false;

  if (!m_isInit) {
    segGoalsStatus.goalID = 0;
    segGoalsStatus.status = SegGoalsStatus::COMPLETED;
    
    m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    sleep(5);
    
    while (!m_missTraffInterfaceNF->haveNewDirective()) {    
      cout << "Sending status goal " << segGoalsStatus.goalID << endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
      sleep(5);
    }
    m_isInit = true;
  }

  /* This is while it is executing and there is a timeout happening */

  m_traffStateEst->updateMap();
  m_traffStateEst->updateVehState();
  m_traffStateEst->updateQueueing();
  //m_traffStateEst->updateVehiclesInOppositeLane();
  //cout<<"VEH OPP LANE beg planning " << m_traffStateEst->isVehiclePresentInOppositeLane()<<endl;

  if (TrafficManagerControlStatus::EXECUTING == controlStatus->status) {
    cout << endl << endl;
    cout<<"TRFMGR:  Executing Goal ID :"<<controlStatus->ID<<endl; 
    /* We need a timeout here */
    //   if ((getTime() - m_startTimeGoal) > 7000000) {

    //    cout<<"GOAL FAILED (timeout) ID "<<controlStatus->ID;
    //    segGoalsStatus.status = SegGoalsStatus::FAILED;
    // segGoalsStatus.goalID = controlStatus->ID;
    // cout<<"GOAL ID, FAILED "<<segGoalsStatus.goalID<<endl;
    // m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
       
    /* Flush out the NF queue and send failure status */
    // while (m_missTraffInterfaceNF->haveNewDirective()) {
    //SegGoals newDirective;
    //m_missTraffInterfaceNF->getNewDirective( &newDirective );
    //segGoalsStatus.goalID = newDirective.goalID;
    //segGoalsStatus.status = SegGoalsStatus::FAILED;
    //cout<<"GOAL ID, FAILED "<<segGoalsStatus.goalID<<endl;
    //m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
    // }
      
    /* Flush out the accumulating segGoals control queue and send failure status */
    //while (m_accSegGoalsQ.size() > 0) {
    //	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
    //	segGoalsStatus.status = SegGoalsStatus::FAILED;
    //	cout<<"GOAL ID, FAILED "<<segGoalsStatus.goalID<<endl;
    //	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
    //	m_accSegGoalsQ.pop_front();
    // }    
    //}
  }
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == TrafficManagerControlStatus::COMPLETED ||
      controlStatus->status == TrafficManagerControlStatus::FAILED) {
    
    if (controlStatus->status == TrafficManagerControlStatus::COMPLETED) {  
      segGoalsStatus.status = SegGoalsStatus::COMPLETED;
      segGoalsStatus.goalID = controlStatus->ID;

      if (controlStatus->wasPaused){
        m_currContrState=ControlStateFactory::getInitialState();
      }
      cout<<"GOAL ID COMPLETED "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );

      /* SVEN: WE DONT WANT TO POP IF WE ARE COMPLETING A PREVIOUS GOAL */
      if (m_accSegGoalsQ.size() > 0 && m_accSegGoalsQ.front().goalID == segGoalsStatus.goalID){ 
        m_accSegGoalsQ.pop_front();
      } else { cout<<"TRFMGR - waiting for more goals: m_accSegGoals.size()==0 "<<endl;}
    } else if (controlStatus->status == TrafficManagerControlStatus::FAILED)  {
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      segGoalsStatus.goalID = controlStatus->ID;
      //m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
   
      /* Flush out the NF queue and send failure status */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
        SegGoals newDirective;
        m_missTraffInterfaceNF->getNewDirective( &newDirective );
        segGoalsStatus.goalID = newDirective.goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
	cout<<"GOAL ID, FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      }
      
      /* Flush out the accumulating segGoals control queue and send failure status */
      while (m_accSegGoalsQ.size() > 0) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
	cout<<"GOAL ID, FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
        m_accSegGoalsQ.pop_front();
      }

      //FIX to wait for the next seg goal so we don't immediately UTurn if we haven't received newgols
      usleep(1000000); 
    }
    cout << endl << endl;
  }

  /* Wait for Route Planner to send a new directive */
  usleep(500000);

  /* Get all the new directives and put it in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {

    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);
    
    cout<<"TRFMGR- new dir ID "<<newDirective.goalID<<",type "<<newDirective.segment_type;
    cout<<",intersection type "<<newDirective.intersection_type<<", illegalPassingAllowed "<<newDirective.illegalPassingAllowed<<endl;

    if ((m_accSegGoalsQ.size()>0) && (newDirective.goalID <= m_accSegGoalsQ.back().goalID)) {
      cout<<"continues..."<<endl;
      continue;
    }
    m_accSegGoalsQ.push_back(newDirective);

    if (SegGoals::PAUSE == newDirective.segment_type) {
      SegGoalsStatus segGoalsStatus;
      
      /* First flush the queue where the seg goals are accumulating */
      while (SegGoals::PAUSE != m_accSegGoalsQ.front().segment_type) {
        segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
        segGoalsStatus.status = SegGoalsStatus::FAILED;
        controlStatus->status = TrafficManagerControlStatus::FAILED;
        cout<<"GOAL ID PAUSE,FAILED "<<segGoalsStatus.goalID<<endl;
        m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
        m_accSegGoalsQ.pop_front();
      }
      break; 
    }
  }

  /* SVEN: ADDED THE EXECUTING */
  if (m_accSegGoalsQ.size() == 0 || TrafficManagerControlStatus::EXECUTING == controlStatus->status) {
    if ((SegGoals::PAUSE != m_currSegGoals.segment_type) || (SegGoals::END_OF_MISSION != m_currSegGoals.segment_type)) {

      PointLabel exitWayptLabel(m_currSegGoals.exitSegmentID, 
				m_currSegGoals.exitLaneID, 
				m_currSegGoals.exitWaypointID);

      cout<<"Exit WAYPOINT label "<<exitWayptLabel<<endl; 
      
      /* Determine traffic state */
      m_currTraffState = m_traffStateEst->determineTrafficState(exitWayptLabel);      
      cout<<"TRAFMGR: trafficState  when accseggoalssize ==0 or executing "<<m_currTraffState->toString()<<endl; 
      determineSignaling(m_currSegGoals);
      cout<<"TURN SIGNAL IS "<<m_turnSignal<<endl;
    }
  }

  if (m_accSegGoalsQ.size() > 0 && TrafficManagerControlStatus::EXECUTING != controlStatus->status) {

    SegGoals newGoal =  m_accSegGoalsQ.front();

    if (SegGoals::PAUSE == newGoal.segment_type) {
      mergedDirective->segType = newGoal.segment_type;
      mergedDirective->id = newGoal.goalID;
      m_currSegGoals = newGoal; 
    } else if (SegGoals::UNKNOWN == newGoal.segment_type) {
      segGoalsStatus.goalID = newGoal.goalID;
      mergedDirective->segType = newGoal.segment_type;
      segGoalsStatus.status = SegGoalsStatus::FAILED;
      cout<<"GOAL ID,UNKNOWN "<<segGoalsStatus.goalID<<endl;
      m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
         
    } else if (SegGoals::END_OF_MISSION == newGoal.segment_type) {
      mergedDirective->id = newGoal.goalID; 
      mergedDirective->segType = newGoal.segment_type; 
      m_currSegGoals = newGoal;

    } else {


      PointLabel exitWayptLabel(newGoal.exitSegmentID, newGoal.exitLaneID, newGoal.exitWaypointID);

      cout<<"Exit WAYPOINT label "<<exitWayptLabel<<endl; 
      
      /* Determine traffic state */
      m_currTraffState = m_traffStateEst->determineTrafficState(exitWayptLabel);
      
      cout<<"TRAFMGR: trafficState in else "<<m_currTraffState->toString()<<endl; 

      /* If we are approaching an intersection (approx 100 m away), and we are about to make a right or left turn 
       then turn on the signals */
      determineSignaling(newGoal); 
      cout<<"TURN SIGNAL IS "<<m_turnSignal<<endl;

      /* Update the current planning horizon and populate m_planHoriz */
      meetsPlanHorizReqs = determinePlanningHorizon(m_accSegGoalsQ);      
      
      /* Do not create a new merged directive until we have the correct planning horizon */
      if(meetsPlanHorizReqs) { /* if we had enough segGoals to be able to plan properly */

        m_currMergedPlanHoriz = m_currPlanHorizon;  

        cout<<"TRAFMGR: meets planning horizon"<<endl;

        mergedDirective->id = newGoal.goalID; 
        mergedDirective->segType = newGoal.segment_type;
        m_currSegGoals = newGoal;

      } else { /* If the plan horiz is not met  */
        cout<<"TRFMGR - Plan Horizon is not met"<<endl;
      }
    }
  } else {
    cout<<"Waiting for MISSION PLANNER goal "<<endl;
  }
}


void TrafficManager::control(ControlStatus* cs, MergedDirective* md) {


  TrafficManagerControlStatus* controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  
  TrafficManagerMergedDirective* mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);

  PointLabel endMissionWayptLabel, exitWayptLabel;

  int errors = 0;

  /* Check to see what the status of the last merged directive is */
  if ((mergedDirective->segType >= SegGoals::ROAD_SEGMENT) 
      && (mergedDirective->segType < SegGoals::UNKNOWN)) {
    
    switch(mergedDirective->segType) {
    case SegGoals::PAUSE:
      m_completed = isGoalComplete();	  
      controlStatus->wasPaused = true;
      cout<<"PAUSE m_completed = "<<m_completed<<endl;
      break;
    case SegGoals::END_OF_MISSION:
      m_completed = true;
      controlStatus->wasPaused = false;
      cout<<"END_OF MISSION completed"<<endl;
      break;
    default:
      PointLabel currGoalEndpoint = PointLabel(m_currSegGoals.exitSegmentID,
                                               m_currSegGoals.exitLaneID,
                                               m_currSegGoals.exitWaypointID);
      m_completed = isGoalComplete(currGoalEndpoint);
      controlStatus->wasPaused = false;
    } 
 
    if (m_completed) {
      controlStatus->ID = m_currSegGoals.goalID; 
      controlStatus->status = TrafficManagerControlStatus::COMPLETED;      
      m_currSegGoals.segment_type = SegGoals::UNKNOWN;

    }  
  
    if ((SegGoals::PAUSE == mergedDirective->segType) || 
        (SegGoals::END_OF_MISSION == mergedDirective->segType))  {
    
      m_currMergedDirectiveID = mergedDirective->id;
      
      /* Get into the PAUSE control action, once we get stauts back of successful PAUSE, then we reinitialize the control action*/
      m_currContrState =  ControlStateFactory::getPauseState();
      
      /* Determine PAUSE corridor */
      errors += m_currContrState->determineCorridor(m_currCorr, m_traffStateEst->getVehState(), m_currTraffState, m_currMergedPlanHoriz, m_traffStateEst->getMap());
      if (0 == errors) {
        bool queueing = false;
        m_currContrState->setIsQueueing(queueing);
        m_currCorr.calculateSeparationDist(m_traffStateEst->getVehState(), m_currTraffState);
        CorridorUtils::setVelForCorridor(m_currCorr, m_currTraffState, m_currContrState, m_currMergedPlanHoriz);
        CorridorUtils::adjustVelocityForObstacles(m_currCorr, m_traffStateEst->getMap(), m_traffStateEst->getVehState(), m_currSegGoals, queueing);
        CorridorUtils::setFinalCondObstacles(m_currCorr, m_currContrState, m_traffStateEst->getVehState(), m_traffStateEst->getMap(), m_currCorr.getSeparationDist(), queueing);
        //m_currCorr.setVelProfile(m_traffStateEst->getMap(), m_traffStateEst->getVehState(), m_currTraffState, m_currContrState, m_currMergedPlanHoriz);
        m_currCorr.adjustFCPosForRearAxle();
      } else {
        cout<<"PAUSE or END rcvd, did not send corridor, vel profile"<<endl; 
      }      
    } else {

      if(!m_completed) {
        bool queueing = m_traffStateEst->isQueueing();
        m_currContrState->setIsQueueing(queueing);
        cout<< "QUEUEING: " << queueing <<endl;
	//cout<<"VEH IN OPP LANE :"<<m_traffStateEst->isVehiclePresentInOppositeLane()<<endl;

	LaneLabel currLane = AliceStateHelper::getDesiredLaneLabel(); 

        /* Update the current control action */
        m_currContrState = determineControlAction();

	LaneLabel nextLane = AliceStateHelper::getDesiredLaneLabel();
 
        /* Update if vehicle present in opposite lane in the control state for use */
        //m_currContrState->setIsVehPresentInOppositeLane(m_traffStateEst->isVehiclePresentInOppositeLane());

        determineSignaling(currLane, nextLane);

        /* No matter what, we only allow illegal passing for one planning cycle */
        resetIllegalPassing();
	  
        /* Set the previous merged directive member */
        m_prevMergedDirectiveID = mergedDirective->id;
	  
        m_currCorr.initializeOCPparams(m_traffStateEst->getVehState(), 
                                       m_currMergedPlanHoriz.getSegGoal(0).minSpeedLimit, 
                                       m_currMergedPlanHoriz.getSegGoal(0).maxSpeedLimit);
	  
        /* Reset Failure for the control state before determining corridor */
        m_currContrState->resetFailure(); 

        /* Determine the corridor */	  
        m_currContrState->determineCorridor(m_currCorr, m_traffStateEst->getVehState(), m_currTraffState, m_currMergedPlanHoriz, m_traffStateEst->getMap());
	  
        if (m_currContrState->hasFailed()) { // Lane blocked situation
          cout<<"TRFMGR - FAILURE!!"<<endl;
          controlStatus->ID = m_prevMergedDirectiveID; 
          controlStatus->status = TrafficManagerControlStatus::FAILED;
          ++errors;

        } else {
          
          /* Set the velocity profile */
          m_currCorr.calculateSeparationDist(m_traffStateEst->getVehState(), m_currTraffState);
          CorridorUtils::setVelForCorridor(m_currCorr, m_currTraffState, m_currContrState, m_currMergedPlanHoriz);
          CorridorUtils::adjustVelocityForObstacles(m_currCorr, m_traffStateEst->getMap(), m_traffStateEst->getVehState(), m_currSegGoals, queueing);
          CorridorUtils::setFinalCondObstacles(m_currCorr, m_currContrState, m_traffStateEst->getVehState(), m_traffStateEst->getMap(), m_currCorr.getSeparationDist(), queueing);
          //m_currCorr.setVelProfile(m_traffStateEst->getMap(), m_traffStateEst->getVehState(), m_currTraffState, m_currContrState, m_currMergedPlanHoriz);	    

          /* Get the global to local delta */
          m_gloToLocalDelta = m_traffStateEst->getMap()->prior.delta;
          
          /* Print the velocity profile */	
          m_currCorr.adjustFCPosForRearAxle();
          controlStatus->ID = m_prevMergedDirectiveID; 
          controlStatus->status = TrafficManagerControlStatus::EXECUTING;
		    
        } //if(m_currContrState->hasFailed()) else
      } //if(!m_completed)
    } //if ((mergedDirective->segType >= SegGoals::ROAD_SEGMENT) .. else 


    cout<<"TRFMGR,control(): control state "<<m_currContrState->toString()<<"errors: "<<errors<<endl; 

    if ((0 == errors) && (!m_completed))  {
      /* Now prepare parameters for sending */  
      /* Convert to RDDF */
      RDDF* rddf = m_currCorr.getRddfCorridor(m_gloToLocalDelta);
      
      /* Send RDDF */
      SendRDDF(m_rddfSocket,rddf);
      /* Convert to polytope representation */
      m_currCorr.generatePolyCorridor();
      m_currCorr.sendPolyCorridor();
      VehicleState vehState = m_traffStateEst->getVehState(); 
      m_currCorr.getBitmapParams(m_bmparams, m_polygonParams, m_currContrState, vehState,m_traffStateEst->getMap(),m_gloToLocalDelta);      
      m_polyTalker.send(&m_bmparams);
      m_currCorr.printSeparationDist();
      m_currCorr.printSpeedLimits();
      m_currCorr.printPolylineCorr();
      m_currCorr.printICFC();
      if (!m_OCPparamsTalker.send(&m_currCorr.getOCPparams())){
        cout<< "TrafficPlanner: problem with send OCP parameters" << endl;
      } 
      delete rddf; 
      m_currCorr.clear();
      m_startTimeGoal = getTime();
    } else if (0 != errors) { 
      cout<<"TRFMGR - failed with errors "<<endl;
    } 
  } else {
    cout<<"TRFMGR - SegGoals UNKNOWN or of invalid type"<<endl;
  }
}
    
ControlState* TrafficManager::determineControlAction() {
  ControlState* controlState;

  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(m_currContrState); 

  ControlStateTransition* transition;

  for (unsigned int i=0; i < transitions.size(); i++) 
    {		
      transition = static_cast<ControlStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(m_currContrState, m_currTraffState, m_currPlanHorizon, m_traffStateEst->getMap(), m_traffStateEst->getVehState());
    }
   
  /* Save old initial conditions - TODO clean */
  VehicleState oldVehState;
  point2 pos = m_currContrState->getInitialPosition();
  oldVehState.localX = pos.x;
  oldVehState.localY = pos.y;

  controlState = chooseMostProbableControlState(m_currContrState, transitions);

  if (m_currContrState->getType() != controlState->getType()) {
    controlState->setInitialPosition(m_traffStateEst->getVehState());
    controlState->setInitialVelocity(m_traffStateEst->getVehState());
    controlState->setInitialTime();
  } else if (m_currContrState->getStateId() != controlState->getStateId()) {
    controlState->setInitialPosition(oldVehState);
    controlState->setInitialVelocity(oldVehState);
    controlState->setInitialTime();
  }

  return controlState;
}


ControlState* TrafficManager::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
  ControlState* currControlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficManager::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
        transProb = transitions[i]->getUncertainty();
        if(1 == transProb) {
          probOneTrans.push_back(transitions[i]);
        } else if ( m_probThresh < transProb) {
          probAboveThresh.push_back(transitions[i]);		
        } else if ( m_probThresh >= transProb) {
          probBelowThresh.push_back(transitions[i]);		
        }
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        //cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
      currControlState = controlState; 
    }
  } else { 
    currControlState = controlState; 
  }
  
  return currControlState;
}


bool TrafficManager::determinePlanningHorizon(deque<SegGoals> currSegGoals) 
{
  TrafficStateFactory::TrafficStateType tStateType = 
    (TrafficStateFactory::TrafficStateType) m_currTraffState->getType();
  
  //cout<<"TRFMGR: Size of currSegGoals "<<currSegGoals.size()<<endl;

  m_currPlanHorizon.clear();
  
  bool meetsPlanHorizonReqs = false; 

  if (currSegGoals.size() > 0 ) {
    /* We need to make sure the currSegGoals have the correct size or else we will seg fault */
    switch(tStateType) {
    case TrafficStateFactory::ZONE_REGION:// want to plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
        meetsPlanHorizonReqs = true;
        m_currPlanHorizon.addGoal(currSegGoals.front()); 
        currSegGoals.pop_front();
        m_currPlanHorizon.addGoal(currSegGoals.front()); 
        currSegGoals.pop_front();
        //cout<<"TRFMGR: ZONE_REGION added goals "<<endl;
        //cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
        //cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    case TrafficStateFactory::ROAD_REGION:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
        meetsPlanHorizonReqs = true;
        m_currPlanHorizon.addGoal(currSegGoals.front()); 
        currSegGoals.pop_front();
        //cout<<"TRFMGR: ROAD_REGION added goals "<<endl;
        //cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
        //cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
        meetsPlanHorizonReqs = true;
        m_currPlanHorizon.addGoal(currSegGoals.front()); 
        currSegGoals.pop_front();
        //cout<<"TRFMGR: APPROACH_INTER_SAFETY added goals "<<endl;
        //cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
        //cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    case TrafficStateFactory::INTERSECTION_STOP:
      // plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
        meetsPlanHorizonReqs = true;
        m_currPlanHorizon.addGoal(currSegGoals.front()); 
        currSegGoals.pop_front();
        m_currPlanHorizon.addGoal(currSegGoals.front()); 		
        currSegGoals.pop_front();
        //cout<<"TRFMGR: INTERSECTION STOP added goals "<<endl;
        //cout<<"TRFMGR: Exit lane ID"<< m_currPlanHorizon.getSegGoal(0).exitLaneID<<endl; 
        //cout<<"TRFMGR: Entry lane ID"<< m_currPlanHorizon.getSegGoal(0).entryLaneID<<endl; 
      }
      break;
    default:
      break;
    };
  }   
  return meetsPlanHorizonReqs;
}

unsigned int TrafficManager::getNextUniqueMergedDirID() {
  return ++m_uniqueMergedDirID; 
}


bool TrafficManager::isGoalComplete(PointLabel exitWayptLabel)
{
  double completeDist = 1;
  bool completed = false;
  double angle, dotProd, AliceHeading, absDist, headingDiff; 

  bool isComplete = m_currContrState->isComplete();

  if (!isComplete) {
    if (SegGoals::PAUSE == m_currSegGoals.segment_type) {

      if (AliceStateHelper::getVelocityMag(m_traffStateEst->getVehState()) < 0.1) {
	completed = true;
      } else {
	completed = false; 
	//cout<<"ALICE not at a stop yet..."<<endl;
      }
      
    } else {
      
      point2 exitWaypt, currPos;
  
      m_traffStateEst->getMap()->getWaypoint(exitWaypt, exitWayptLabel);

      currPos = AliceStateHelper::getPositionFrontBumper(m_traffStateEst->getVehState());

      m_traffStateEst->getMap()->getHeading(angle, exitWayptLabel);

      cout<<"TRFMGR::isGoalComplete "<<endl;
      cout<<"TRFMGR exit waypoint label "<<exitWayptLabel<<endl;
      cout<<"TRFMGR currPos "<<currPos<<endl;
      cout<<"TRFMGR exit waypoint "<<exitWaypt<<endl;

      dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

      if ((m_verbose)||(m_debug)){
	cout << "in iscomplete(): dot product  = " << dotProd << endl;
      }

      AliceHeading = AliceStateHelper::getHeading(m_traffStateEst->getVehState());
      absDist = exitWaypt.dist(currPos);

      double anglePiMinusPi;

      TrafficUtils::addAngles(anglePiMinusPi,angle,-AliceHeading);
      headingDiff = fmod(anglePiMinusPi,M_PI);

      if((dotProd>-completeDist) && (fabs(headingDiff)<M_PI/12) && (absDist<10)) {
	/* when we get within 1 m of the hyperplane defined by the exit pt, goal complete */
	completed = true;
	//      cout<<"TRFMGR::isGoalComplete COMPLETED "<<endl; 
      }
      else {
	completed = false;
	//    cout<<"TRFMGR::isGoalComplete NOT COMPLETE "<<endl; 
      }
    }
  } else { 
    completed = true;
  }
  return completed;
}


bool TrafficManager::isGoalComplete()
{
  return (AliceStateHelper::getVelocityMag(m_traffStateEst->getVehState()) < 0.1);

}

uint64_t TrafficManager::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}

void TrafficManager::display_console()
{
  Console::erase_screen();
  Console::cprintf(5, 5, "   The console is not yet available");
  Console::cprintf(6, 5, "Use the --disable-console flag for now\n\n");
}


void TrafficManager::readConfigFile(char* file)
{
  ifstream infile(file);
  if (infile.fail()) {
    // check with the user on whether we should continue
    cerr << "costmap painter: couldn't open config file '" << file
	 << "'; continue (y/n)?";

    // Anything except y or Y will cause us to abort; otherwise return
    if (tolower(fgetc(stdin)) != 'y') 
      exit(-1);
    else
      return;
  }
  string param;
  while ( !infile.eof() ) {
    infile >> param;
    if ( "resX" == param )
      infile >> m_bmparams.resX;
    else if ( "resY" == param )
      infile >> m_bmparams.resY;
    else if ( "width" == param )
      infile >> m_bmparams.width;
    else if ( "height" == param )
      infile >> m_bmparams.height;
    else if ( "baseVal" == param )
      infile >> m_bmparams.baseVal;
    else if ( "outOfBounds" == param )
      infile >> m_bmparams.outOfBounds;
    else if ( "centerlaneVal" == param )
      infile >> m_polygonParams.centerlaneVal;
    else if ( "obsCost" == param )
      infile >> m_polygonParams.obsCost;
  }
}


void TrafficManager::determineSignaling(SegGoals segGoals) {

  cout<<"TRFMGR: about to determine whether we want to turnsigs on" <<endl; 

  if  (!m_turnedSignalBecauseOfLaneChange) {
    if (TrafficStateFactory::APPROACH_INTER_SAFETY == m_currTraffState->getType()) {
     
      for (unsigned int i= 0; i < m_accSegGoalsQ.size(); i++) {      
	SegGoals newGoal =  m_accSegGoalsQ[i];
	cout<<"newGoal segment type "<<newGoal.segment_type<<endl;
	cout<<"newGoal intersection type "<<newGoal.intersection_type<<endl;
       
	if (SegGoals::INTERSECTION == newGoal.segment_type && 
	    SegGoals::INTERSECTION_STRAIGHT != newGoal.intersection_type) { 
	  turnSignalsOn(newGoal);
	  m_isTurnSignalOn = true;
	  break;
	}
      }
    } else if (TrafficStateFactory::ROAD_REGION == m_currTraffState->getType()) {
      turnSignalsOff();
      m_isTurnSignalOn = false; 
    } 
  }
}


void TrafficManager::determineSignaling(LaneLabel currLane, LaneLabel nextLane) {

  cout<<"TRFMGR: about to determine whether we want to turnsigs on" <<endl; 
  if (ControlStateFactory::LANE_CHANGE == m_currContrState->getType() && !m_turnedSignalBecauseOfLaneChange) {
    m_turnSignal = TrafficUtils::getLaneSide(m_traffStateEst->getMap(), currLane, nextLane);
    m_turnedSignalBecauseOfLaneChange = true; 
  } else if (ControlStateFactory::LANE_CHANGE != m_currContrState->getType() 
	     && m_turnedSignalBecauseOfLaneChange) {
    m_turnSignal = 0;
    m_turnedSignalBecauseOfLaneChange = false;
  }
  sendTurnSignalCommand();
}

void TrafficManager::turnSignalsOn(SegGoals segGoals) {

  cout<<"TRFMGR: turnSignalsOn " <<endl; 
  /* Send the turn signal to GcDrive if we are turning or lane changing */
  if (SegGoals::INTERSECTION_LEFT == segGoals.intersection_type){ 
    m_turnSignal = -1;
    cout<<"INTERSECTION_LEFT"<<endl;
  } else {
    cout<<"INTERSECTION_RIGHT"<<endl;
    m_turnSignal = 1;
  }
  sendTurnSignalCommand();
}   

void TrafficManager::turnSignalsOff() {

    m_turnSignal = 0;
    sendTurnSignalCommand();
    cout<<"TRFMGR: turnSignalsOff sent command" <<endl; 

}   
 
void TrafficManager::sendTurnSignalCommand() {

  m_adriveDir.id = ++m_turnSignalDirId;
  m_adriveDir.actuator = TurnSignal; 
  m_adriveDir.command = SetPosition; 
  m_adriveDir.arg = m_turnSignal;
  m_traffAdriveInterfaceSF->sendDirective(&m_adriveDir);
  cout<<"SENT ADRIVE turn signal directive "<<m_adriveDir.arg<<endl;
  
}   

void TrafficManager::resetIllegalPassing(void)
{
  m_currPlanHorizon.resetIllegalPassing();
  m_currMergedPlanHoriz.resetIllegalPassing();
  m_currSegGoals.illegalPassingAllowed = false;
}

int TrafficManager::getSkynetKey()
{
  return m_snKey;
}
