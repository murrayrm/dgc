/*! CheckPass.hh
 * Christian Looman
 * May 09 2007
 */

#ifndef CHECKPASS_HH
#define CHECKPASS_HH
#define AHEAD 0
#define BEHIND 1

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
#include <cv.h>
#include <highgui.h>
#include <time.h>
//open cv stuff

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "state/AliceStateHelper.hh"
#include "ControlState.hh"
#include "ControlStateFactory.hh"
#include "cmdline.h"
#include "Log.hh"


// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

class CheckPass
{ 
public:
  // Functions for passing static obstacle with dyn. oncoming traffic
  static double getProbability(VehicleState vehState,Map* localMap);
  static void checkForObstacles(VehicleState vehState, Map* localMap, LaneLabel lane,int searchDirection, double& distance_obstacle,double& vel_obstacle, double& size_obstacle);
};

#endif  // CHECKPASS_HH

