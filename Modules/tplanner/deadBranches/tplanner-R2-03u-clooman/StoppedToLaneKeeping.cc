#include "StoppedToLaneKeeping.hh"
#include <math.h>
#include <list>

StoppedToLaneKeeping::StoppedToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{
}

StoppedToLaneKeeping::StoppedToLaneKeeping()
{

}

StoppedToLaneKeeping::~StoppedToLaneKeeping()
{

}

double StoppedToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToLaneKeeping::meetTransitionConditions" << endl;
    }

    switch (trafficState->getType()) {
    
     case TrafficStateFactory::ZONE_REGION:
     {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
         if (!IntersectionHandling::init)
 	  IntersectionHandling::resetIntersection(map);
         bool clear = IntersectionHandling::checkIntersection(vehState,map,controlState);

        if (clear) {
	  // After successfull passing, reset intersection
	  IntersectionHandling::resetIntersection(map);
            if ((m_verbose) || (m_debug)) {
                cout << "in stoppedToLaneKeeping: ZONE_REGION = CLEAR" << endl;
            }
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;
        
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      {
        m_probability = 0;
        cerr << "In StoppedToLaneKeeping::meetTransitionsConditions: SHOULD NOT BE HERE as this transition is not defined for RR or AIS" << endl;
      }
      break;
          
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        // Check if the intersection is clear
        // TODO: DO THIS PROPERLY
        sleep(5);
        if ((m_verbose) || (m_debug)) {
            cout << "checking to see if intersection is clear" << endl;
        }
        
        // Check if it is our turn
        // TODO: DO THIS PROPERLY
         if (!IntersectionHandling::init)
 	  IntersectionHandling::resetIntersection(map);
         bool clear = IntersectionHandling::checkIntersection(vehState,map,controlState);

        if (clear) {
	  // After successfull passing, reset intersection
	  IntersectionHandling::resetIntersection(map);
          if ((m_verbose) || (m_debug)) {
            cout << "in stoppedToLaneKeeping: INTERSECTION_STOP = CLEAR and OURTURN" << endl;
          }
          m_probability = 1;
        } else {
          m_probability = 0;
        }
    }
        break;
        
    default:
    
        m_probability = 0;
        cerr << "StoppedToLaneKeeping.cc: Undefined Traffic state" << endl;
    
    }
    
    if ((m_verbose) || (m_debug)) {
        cout << "probability = " << m_probability << endl;
    }
    setUncertainty(m_probability);
    
    return m_probability;
}
