#ifndef STOPPED_HH_
#define STOPPED_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class Stopped:public ControlState {

  public:

    static typedef enum { OBSTACLE, INTERSECTION } StoppedType;

    Stopped(int stateId, ControlStateFactory::ControlStateType type, StoppedType stage);
    ~Stopped();
    int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);

    bool getUTurnBool();
    bool getChangeLaneLeftBool();
    bool getChangeLaneRightBool();
    void resetConditions();

  ControlState* newCopy() { return (ControlState*) new Stopped(*this); }
  private:
  
    bool m_uturn;
    bool m_changelane_left;
    bool m_changelane_right;
    StoppedType m_stage;

};

#endif                          /*STOPPED_HH_ */
