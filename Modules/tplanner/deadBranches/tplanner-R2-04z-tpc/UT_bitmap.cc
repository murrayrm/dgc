#include <iostream>
#include <frames/point2.hh>
#include <bitmap/BitmapParams.hh>
#include <skynettalker/SkynetTalker.hh>
#include <ocpspecs/CostMapEstimator.hh>
#include <highgui.h>
#include "Corridor.hh"
#include "UTurn.hh"
#include "LaneKeeping.hh"
#include "RoadRegion.hh"
#include <boost/serialization/vector.hpp>

using namespace std;

int main(int argc,char** argv)
{
  int skynetKey = skynet_findkey(argc, argv);
  cout << "Skynet key: " << skynetKey << endl;
  /* Receiver */
  CostMap cmap;
  CostMapEstimator cmapEst(skynetKey, true, false, 2);
  //ControlState* controlState = new UTurn(0, ControlStateFactory::UTURN, 1);
  ControlState* controlState = new LaneKeeping(0, ControlStateFactory::LANE_KEEPING, LaneKeeping::NOMINAL);
  TrafficState* trafficState = new RoadRegion(0, TrafficStateFactory::ROAD_REGION);

  /* Sender */
  SkynetTalker< BitmapParams > polyTalker(skynetKey, SNbitmapParams, 
					  MODtrafficplanner);

  Map lmap;
  BitmapParams bmparams;
  PolygonParams polygonParams;

  //Parameters
  bmparams.width = 800;
  bmparams.height = 800;
  bmparams.resX = 0.1;
  bmparams.resY = 0.1;
  bmparams.baseVal = 100;
  polygonParams.centerlaneVal = 0;
  polygonParams.obsCost = 1000;

  VehicleState state;
  state.localX = 70;
  state.localY = -20;
  Corridor corr = Corridor();

  point2arr leftBound = point2arr();
  point2 pt=point2(68.2617, 13.158);
  leftBound.push_back(pt);
  pt=point2(75.5468, -6.46463);
  leftBound.push_back(pt);
  pt=point2(83.064, -26.2927);
  leftBound.push_back(pt);
  pt=point2(88.1976, -40.6156);
  leftBound.push_back(pt);
  pt=point2(95.1599, -59.4611);
  leftBound.push_back(pt);
  corr.addPolyline(leftBound);

  point2arr rightBound = point2arr();
  pt=point2(72.5161, 14.8349);
  rightBound.push_back(pt);
  pt=point2(79.8274, -4.85859);
  rightBound.push_back(pt);
  pt=point2(87.3539, -24.7109);
  rightBound.push_back(pt);
  pt=point2(99.446, -57.8697);
  corr.addPolyline(rightBound);

  /*
  point2arr obs;
  pt=point2(4,5);
  obs.push_back(pt);
  pt=point2(4,7);
  obs.push_back(pt);
  pt=point2(7,7);
  obs.push_back(pt);
  pt=point2(7,3);
  obs.push_back(pt);
  MapElement mapElement;
  mapElement.setTypeObstacle();
  mapElement.frameType = FRAME_LOCAL;
  mapElement.setId(1);
  mapElement.setGeometry(obs);
  mapElement.height = 0.2;
  lmap.addEl(mapElement);
  */

  vector<MapId> vect;
  corr.getBitmapParams(bmparams, polygonParams, controlState, trafficState, state, &lmap, vect, point2(0,0));
  polyTalker.send(&bmparams);
  usleep(100000);
  bool cmapRecv = cmapEst.updateMap(&cmap);
  if (!cmapRecv) {
    cerr << "ERROR: No cmap received" << endl;
  }

  cout << "Press any key to quit" << endl;
  cvWaitKey(0);
  delete controlState;

}
