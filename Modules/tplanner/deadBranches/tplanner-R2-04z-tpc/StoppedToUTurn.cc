#include "StoppedToUTurn.hh"
#include <math.h>
#include <list>
#include "Log.hh"

StoppedToUTurn::StoppedToUTurn(ControlState * state1, ControlState * state2)
:  ControlStateTransition(state1, state2)
   , m_velBound(0.1)
{
}

StoppedToUTurn::StoppedToUTurn()
{

}

StoppedToUTurn::~StoppedToUTurn()
{

}

double StoppedToUTurn::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{
    Log::getStream(1) << "in StoppedToUTurn::meetTransitionConditions" << endl;
      
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      if (SegGoals::UTURN == planHorizon.getSegGoal(0).segment_type) {
            m_probability = 1;
        } else {
            m_probability = 0;
        }
        Log::getStream(1) << "stopped to uturn m_probability = " << m_probability << endl;
    }
        break;
        
    default:

        m_probability = 0;
    
    }
    setUncertainty(m_probability);
    
    return m_probability;
}
