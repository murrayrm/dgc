#include "Corridor.hh"
#include "state/AliceStateHelper.hh"
#include "ControlState.hh"

GisCoordLatLon latlon;
GisCoordUTM utm;

Corridor::Corridor()   
  : m_nPolytopes(1)
  , m_polyCorridorTalker(0,SNpolyCorridor,MODdynamicplanner) 
 
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_polyCorridor = new CPolytope[m_nPolytopes];
  
}

Corridor::Corridor(bool debug, bool verbose, bool log)  
  : m_nPolytopes(1)
  , m_polyCorridorTalker(0,SNpolyCorridor,MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = verbose;
  m_debug = debug;
  
}

Corridor::Corridor(int skynetKey, bool debug, bool verbose, bool log)  
  : m_nPolytopes(1)
  , m_polyCorridorTalker(skynetKey,SNpolyCorridor,MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = verbose;
  m_debug = debug;
  
}

Corridor::~Corridor() 
{
  //TODO why this is getting called at construction
  //delete[] m_polyCorridor; 
}

  
GeometricConstraints* Corridor::getGeometricConstraints() 
{
	return new GeometricConstraints(); 
}

  
int Corridor::getControlStateId() {

	return m_controlStateId;
  
}

void Corridor::addPolyline(point2arr line)
{
  m_polylines.push_back(line);
	//cout << "m_polylines.size = " << m_polylines.size() << endl;
}

vector<point2arr> Corridor::getPolylines()
{
  return m_polylines;

}


void Corridor::setVelProfile(vector<double> velProfile)
{
  m_velProfile = velProfile;
}

vector<double> Corridor::returnSpeedProfile()
{
  return m_speedProfile;
}

void Corridor::setDesiredAcc(double desAcc)
{
  m_desiredAcc = desAcc;
}

void Corridor::setAlicePrevPos(point2 alicePos)
{
  m_alicePrevPos = alicePos;
}

vector<double> Corridor::getVelProfile()
{
  return m_velProfile;
}

double Corridor::getDesiredAcc()
{
  return m_desiredAcc;
}


void Corridor::clear() 
{
  m_polylines.clear();

}



void Corridor::setSpeedProfileAcc(point2 initPos, double initSpeed, double finalSpeed, double desiredAcc)
{
  return;
}

void Corridor::setSpeedProfileDist(point2 initPos, double initSpeed, double finalSpeed, point2 finalPos)
{
  double dist_stop = 0;
  double dist = 0;
  point2 tmpPt, temppt1, temppt2;
  
  point2arr leftBound;
  leftBound = m_polylines[0];
  point2arr rightBound; 
  rightBound = m_polylines[1];
  point2arr centerline;

  m_speedProfile.clear();
  m_accProfile.clear();

  // need to insert a point on the boundary at the distance along the corridor to plan vel profile from
  TrafficUtils::insertProjPtInBoundary(leftBound, initPos);
  TrafficUtils::insertProjPtInBoundary(rightBound, initPos);
  // update the m_polytopes
  m_polylines.clear();
  m_polylines.push_back(leftBound);
  m_polylines.push_back(rightBound);

  //get the center line of the corridor
  for (int ii=0; ii<(int)leftBound.size(); ii++){
    tmpPt = (leftBound[ii]+rightBound[ii])/2;
    centerline.push_back(tmpPt);
  }
  
  //  cout << "centerline = " << centerline << endl;

  // get index of the boundary pt closest to initPos
  int index = TrafficUtils::getClosestPtIndex(centerline, initPos);
  //cout << "index = " << index << endl;
  // set the velocity up to that point as v_in;
  for (int ii=0; ii<=index; ii++) {
    m_speedProfile.push_back(initSpeed);
    m_accProfile.push_back(0);
  }

  //cout << "Speed profile 1 = " << m_speedProfile << endl;
  // calculate the distance to the final pos, from the init pos along the centerline
  for (int ii=index+1; ii<(int)centerline.size(); ii++) {
    // cout << "ii = " << ii << endl;
    dist_stop += centerline[ii-1].dist(centerline[ii]);
  }

  // cout << "dist to stop = " << dist_stop << endl;
  // calculate the desired acc based on this distance
  double acc_req = (finalSpeed*finalSpeed - initSpeed*initSpeed)/2/dist_stop;

  //cout << "acc req = " << acc_req << endl;

  //interpolate the rest of the way to the stopping location
  double tmp;
  for (int ii=index+1; ii<(int)centerline.size(); ii++) {
    //cout << "ii = " << ii << endl;
    dist += centerline[ii-1].dist(centerline[ii]);
    //cout << "dist = " << dist << endl;
    tmp = (finalSpeed-initSpeed)/dist_stop*dist + initSpeed;
    // make sure that we do not go below initSpeed
    if (tmp>finalSpeed)
      m_speedProfile.push_back(tmp);
    else
      m_speedProfile.push_back(finalSpeed);

    m_accProfile.push_back(acc_req);
  }

  return;
}

double Corridor::getSpeedAtNode(int node){
  if(node >= m_speedProfile.size()){
    return m_speedProfile.back();
   }
   else{
     return m_speedProfile[node];
   }
}

double Corridor::getAccAtNode(int node){
  if(node >= m_accProfile.size()){
    return m_accProfile.back();
  }
  else{
    return m_accProfile[node];
  }
}


double Corridor::getSpeedAtPoint(point2 evalPoint){

  return -1.0;
}

double Corridor::getAccAtPoint(point2 evalPoint){

  return -1.0;
}


void Corridor::setVelProfile(Map *localMap, VehicleState vehState, TrafficState *currTrafState, ControlState *currControlState, PlanningHorizon planHoriz)
{
  SegGoals currSegGoal = planHoriz.getSegGoal(0);
  PointLabel exitPointID(currSegGoal.exitSegmentID, currSegGoal.exitLaneID, currSegGoal.exitLaneID);
  PointLabel entryPointID(currSegGoal.entrySegmentID, currSegGoal.entryLaneID, currSegGoal.entryLaneID);
  point2 entryPt, exitPt;
  point2 currFrontPos  = AliceStateHelper::getPositionFrontBumper(vehState);
  double vel_in, vel_out;
  bool obsPresent = false;
  point2 obsPos;

  double dec_des = -0.5;

  // Check if there are any obstacles that we need to deal with
  double dist2Obs = localMap->getObstacleDist(currFrontPos, 0);
  if (dist2Obs!=-1) {
    // only deal with obstacles within 30m
    if (dist2Obs<30) {
      cout << "Obstacle less than 30m away!" << endl;
      obsPresent = true;
      obsPos = localMap->getObstaclePoint(currFrontPos,0);
    }
  }

  switch (currControlState->getType()) 
    {
    case ControlStateFactory::LANE_KEEPING:
      {
	// do not do obstacle adjustment for LaneChange
	if (obsPresent){
          // calculate a vel profile based on desired decceleration
          // TODO: set global variable for some of these things, like the stopping 10m vefore the obstacle, stopping 1m in front of the line, desired deceleration, etc
          if (dist2Obs<10)
            vel_in = 0; //stop now!
          else
            vel_in = sqrt(-2*dec_des*(dist2Obs-10));
          
          if (vel_in>currSegGoal.maxSpeedLimit) { 
              vel_in = currSegGoal.maxSpeedLimit;
          }
          vel_out = 0;
          
          // plan to 10 m before the obstacle
          exitPt = localMap->getObstaclePoint(currFrontPos,10);
          // need to update the final condition
          setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondLB(VELOCITY_IDX_C, 0);
          setOCPfinalCondUB(VELOCITY_IDX_C, 0);
        }
      }
    case ControlStateFactory::LANE_CHANGE:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = currSegGoal.maxSpeedLimit;
          vel_out = currSegGoal.maxSpeedLimit;
        }
        else {
          // Nominal driving
          localMap->getWaypoint(exitPt, exitPointID);
          entryPt = currFrontPos;
          vel_in = currSegGoal.maxSpeedLimit;
          vel_out = currSegGoal.maxSpeedLimit;
        }          
      }
      break;
      
    case ControlStateFactory::STOP:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0;
          vel_out = 0;
        }
        else {
          localMap->getNextStopline(exitPt, exitPointID);
          // want to use the pos and vel when we switch into this state to calculate
          // desired initial velocty to ensure that we follow the correct vel profile
          point2 initPos = currControlState->getInitialPosition();
          // TODO: replace with distance to stopline along lane function
          double distInit2Stop = exitPt.dist(initPos);
          cout << "distance Init to stop = " << distInit2Stop << endl;
          // TODO: need to replace this with a function that returns dist along lane
          double distFromEntry = initPos.dist(currFrontPos);
          cout << "distance from entry into STOP = " << distFromEntry << endl;
          double v_init = currControlState->getInitialVelocity();
          cout << "initial velocity = " << v_init << endl;
          vel_in = (0-v_init)/distInit2Stop*distFromEntry + v_init;
          cout << "vel_in = " << vel_in << endl;
          if (vel_in<0){
            vel_in = 0;
            cout << "vel_in < 0, set to " << vel_in << endl;
          }
          
          vel_out = 0;
          //TODO: Need to make the entry point here the entry waypoint
          entryPt = currFrontPos;
        }

        if (obsPresent){
          cout << "--------------THINKS THERE IS AN OBSTACLE in corridor::setVelProfile()" << endl;
          // calculate a vel profile based on desired decceleration
          // TODO: set global variable for some of these things, like the stopping 10m vefore the obstacle, stopping 1m in front of the line, desired deceleration, etc
          if (dist2Obs<10 && dist2Obs>0)
            vel_in = 0; //stop now!
          else
            vel_in = sqrt(-2*dec_des*(dist2Obs-10));

          if (vel_in>currSegGoal.maxSpeedLimit) { 
            vel_in = currSegGoal.maxSpeedLimit;
          }
          vel_out = 0;
          
          // plan to 10 m before the obstacle
          exitPt = localMap->getObstaclePoint(currFrontPos,10);
          // need to update the final condition
          setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondLB(VELOCITY_IDX_C, 0);
          setOCPfinalCondUB(VELOCITY_IDX_C, 0);
          
        }


      }
      break;
      
    case ControlStateFactory::STOPPED:
      {
        // Nominal driving
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0;
          vel_out = 0;
        }
        else {
          localMap->getWaypoint(exitPt, exitPointID);
          entryPt = currFrontPos;
          vel_in = 0;
          vel_out = 0;
        }

      }
      break;

    case ControlStateFactory::CREEP:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0;
          vel_out = 0;
        }
        else {
          // Nominal driving
          localMap->getWaypoint(exitPt, exitPointID);
          entryPt = currFrontPos;
          vel_in = 1;
          vel_out = 1;
        }

        if (obsPresent){
          // calculate a vel profile based on desired decceleration
          // TODO: set global variable for some of these things, like the stopping 10m vefore the obstacle, stopping 1m in front of the line, desired deceleration, etc
          if (dist2Obs<10)
            vel_in = 0; //stop now!

          vel_out = 0;
          
          // plan to 10 m before the obstacle
          exitPt = localMap->getObstaclePoint(currFrontPos,10);
          // need to update the final condition
          setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondLB(VELOCITY_IDX_C, 0);
          setOCPfinalCondUB(VELOCITY_IDX_C, 0);
          
        }


      }
      break;

    case ControlStateFactory::UTURN:
      {
        // Nominal driving
        point2arr leftBound(m_polylines[0]);
        point2arr rightBound(m_polylines[1]);
        exitPt = (leftBound[1]+rightBound[1])/2;
        entryPt = (leftBound[0]+rightBound[0])/2;
        vel_in = 2;
        vel_out = 2;
      }
      break;


    default:
      
      cerr << "Corridor::setVelProfile: Undefined control state" << endl;        
    }

  setSpeedProfileDist(entryPt, vel_in, vel_out, exitPt);
  
  return;
}



 
//void Corridor::updateDistFromConStateBegin(point2 alicePrevPos)
//{
//  point2 aliceCurrPos = AliceStateHelper::getPositionFrontBumber();
//  m_distFromConStateBegin = m_distFromConStateBegin + 
//    sqrt(pow(aliceCurrPos.x - alicePrevPos.x,2) + pow(aliceCurrPos.y - alicePrevPos.y,2));
//}

void Corridor::initializeOCPparams(VehicleState vehState, double vmin, double vmax)
{
  m_ocpParams = OCPparams();
  
  // set min and max speed to curr segment min/max speed (from mdf)
  m_ocpParams.parameters[VMIN_IDX_P] = vmin;
  m_ocpParams.parameters[VMAX_IDX_P] = vmax;


  // populate the initial conditions - lower bound
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).y;
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).x;
  m_ocpParams.initialConditionLB[VELOCITY_IDX_C] = AliceStateHelper::getVelocityMag(vehState);
  m_ocpParams.initialConditionLB[HEADING_IDX_C] = vehState.utmYaw;
  m_ocpParams.initialConditionLB[ACCELERATION_IDX_C] = AliceStateHelper::getAccelerationMag(vehState);
  // TODO: get the current steering angle
  m_ocpParams.initialConditionLB[STEERING_IDX_C] = 0;

  // populate the initial conditions - upper bound
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).y;
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).x;
  m_ocpParams.initialConditionUB[VELOCITY_IDX_C] = AliceStateHelper::getVelocityMag(vehState);
  m_ocpParams.initialConditionUB[HEADING_IDX_C] = vehState.utmYaw;
  m_ocpParams.initialConditionUB[ACCELERATION_IDX_C] = AliceStateHelper::getAccelerationMag(vehState);
  // TODO: get the current steering angle
  m_ocpParams.initialConditionUB[STEERING_IDX_C] = 0;

  // initialize to fwd mode by default
  m_ocpParams.mode = (int)md_FWD;
}

void Corridor::getOCPinitialPos(point2 &IC_posLB, point2 &IC_posUB)
{
  IC_posLB.x = m_ocpParams.initialConditionLB[NORTHING_IDX_C];
  IC_posLB.y = m_ocpParams.initialConditionLB[EASTING_IDX_C];
  IC_posUB.x = m_ocpParams.initialConditionUB[NORTHING_IDX_C];
  IC_posUB.y = m_ocpParams.initialConditionUB[EASTING_IDX_C];
  return;
}

void Corridor::convertOCPtoGlobal(point2 gloToLocalDelta)
{
  point2 tmpPt, gloTmpPt;
  tmpPt.x = m_ocpParams.initialConditionLB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.initialConditionLB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.initialConditionUB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.initialConditionUB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.finalConditionLB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.finalConditionLB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.finalConditionUB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.finalConditionUB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = gloTmpPt.y;

  if ((m_verbose) || (m_debug)){ 
    for (int ii=0; ii<6 ; ii++) {
      cout << "ICLB[" << ii << "] = " << m_ocpParams.initialConditionLB[ii] << endl;
      cout << "FCLB[" << ii << "] = " << m_ocpParams.finalConditionLB[ii] << " and FCUB[" << ii << "] = " << m_ocpParams.finalConditionUB[ii] << endl;
    }
  }
}

void Corridor::setOCPmode(int mode)
{
  m_ocpParams.mode = mode;
}

void Corridor::setOCPfinalCondLB(int index, double cond)
{
  m_ocpParams.finalConditionLB[index] = cond;
}

void Corridor::adjustFCPosForRearAxle()
{
  return;
  // TODO: will need to fix this heading if I decide to specify a non-symm heading range
  double length = -DIST_REAR_AXLE_TO_FRONT;
  double heading = m_ocpParams.finalConditionUB[HEADING_IDX_C];
  double easting = length*sin(heading);
  double northing = length*cos(heading);

  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = m_ocpParams.finalConditionUB[NORTHING_IDX_C] + northing ;
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = m_ocpParams.finalConditionUB[EASTING_IDX_C] + easting ;
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = m_ocpParams.finalConditionLB[NORTHING_IDX_C] + northing ;
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = m_ocpParams.finalConditionLB[EASTING_IDX_C] + easting ;
}

void Corridor::setOCPfinalCondUB(int index, double cond)
{
  m_ocpParams.finalConditionUB[index] = cond;
}


void Corridor::setOCPinitialCondLB(int index, double cond)
{
  m_ocpParams.initialConditionLB[index] = cond;
}


void Corridor::setOCPinitialCondUB(int index, double cond)
{
  m_ocpParams.initialConditionUB[index] = cond;
}

void Corridor::printICFC()
{
    for (int ii=0; ii<6 ; ii++) {
      cout << "ICLB[" << ii << "] = " << m_ocpParams.initialConditionLB[ii] << " and ICUB[" << ii << "] = " << m_ocpParams.initialConditionUB[ii] << endl;
      cout << "FCLB[" << ii << "] = " << m_ocpParams.finalConditionLB[ii] << " and FCUB[" << ii << "] = " << m_ocpParams.finalConditionUB[ii] << endl;
    }
    return;
}

void Corridor::printVelProfile()
{
  cout << "Velocity profile = " << m_speedProfile << endl;
}


OCPparams Corridor::getOCPparams()
{
  return m_ocpParams;
}


RDDF* Corridor::convertToRddf(point2 gloToLocalDelta)
{
  // For this I am assuming that the corridor boundaries are specified in ordered pairs
  //  cout << "in new convertToRddf()" << endl;
  RDDFData rddfData;
	RDDF* rddf = new RDDF();

  point2arr leftBound(m_polylines[0]);
  point2arr rightBound(m_polylines[1]);
  point2 tmpPt, tmpPt_glo;
  double radius;

  if (leftBound.size()==1){
    cerr << "ERROR convRDDF: only one data point on boundaries" << endl;
    return rddf;
  }

  int rddfNum = 1;
  for (int ii=0; ii<(int)leftBound.size(); ii++) {
    //cout << "ii = " << ii << endl;
    tmpPt = (leftBound[ii]+rightBound[ii])/2;
    radius = tmpPt.dist(leftBound[ii]);
    
    //tmpPt_glo = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
    //utm.n = tmpPt_glo.x;
    //utm.e = tmpPt_glo.y;
    //gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
    rddfData.number = rddfNum;
    rddfData.Northing = tmpPt.x;
    rddfData.Easting = tmpPt.y;
    rddfData.maxSpeed = m_speedProfile[ii];
    rddfData.radius = radius;
    //    rddfData.latitude = latlon.latitude;
    //rddfData.longitude = latlon.longitude;
    //rddfData.latitude = 0;
    //rddfData.longitude = 0;
    rddf->addDataPoint(rddfData);
    rddfNum++;
  }
  if ((m_verbose) || (m_debug)){
    cout<<"CORRIDOR.convertRddfCorridor: OK"<<endl;
  }

	return rddf;
}


RDDF* Corridor::getRddfCorridor(point2 gloToLocalDelta)
{

	return convertToRddf(gloToLocalDelta);

}
void Corridor::paintLaneCostCenterLine(CMapPlus* map, int costLayer, int tempLayer, point2arr& leftbound, point2arr &rightbound)
{
  //for now, we just assume a straight corridor and xval refers to the center line x-value

  /*  
      point2arr ptarr = el->geometry;
  
      int ptarrsize= ptarr.size();
      point2 pt = ptarr[0];

      double x = pt.x;
  */

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // assume a known lane-width of 3m on each side
  // THIS SHOULD BE MOVED
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  double laneWidth=10;

  int n_laneWidth;
  int n_laneHalf;
  int n_left, n_right;

  int num_map_rows;
  int num_map_cols;

  double cost;
  double colRes, rowRes;
  int rowWinCenter; 
  int col_Win_temp;
  int check_inside;

  colRes = map->getResCols();
  rowRes = map->getResRows();
     
  num_map_rows = map->getNumRows();
  num_map_cols = map->getNumCols();

  //n_laneWidth =(int)(ceil(laneWidth/rowRes));
  //n_laneHalf = (int)(floor(n_laneWidth/2));

  //printf("n_right: %d  n_left: %d\n", n_right, n_left);
  
  //r is row
  //c is column
  point2 lproj,rproj;
  point2 ldpt, rdpt;
  point2 pt;
 
  double lcost;
  double rcost;
  double x, y;
  int val;
  int lside, rside;
  int lindex, rindex;
  int baseval = 100;
  double dval = 20;
  cout << "TRYING TO PAINT THE CORRIDOR-------------------------------" <<endl;


  
  // cout << "leftbound post offset= "  << leftbound << endl << endl;
  // cout << "rightbound post offset= "  << rightbound << endl << endl;
  



  for(int c=0; c<num_map_cols; c++)
    {
      for(int r=0; r<num_map_rows; r++)
        {
          val = map->Win2UTM(r,c,&x,&y);
          //          if ((c<10)&&(r<10)){
          //  map->setDataWin<double>(tempLayer,r,c,54.3);
            //    cout << "c=" <<c << " r=" <<r <<endl;
            //   cout << "x=" <<x << " y=" <<y <<endl;

          //}


          
          pt.x =x;
          pt.y =y;

          leftbound.get_side(pt,lside,lindex);
          rightbound.get_side(pt,rside,rindex);

 
          if (rside!=lside){
            if (lindex==0 && rindex==0){
              //              cout << "plootting " << endl;
              //             if (lindex>0 &&rindex>0 &&lindex<(double)(leftbound.size()-1) 
              //             && rindex< (double) (rightbound.size()-1)){
              lproj = leftbound.project(pt);
              rproj = rightbound.project(pt);
              ldpt = lproj-pt;
              rdpt = rproj-pt;
              lcost = dval/(ldpt.norm()+dval/baseval);
              rcost = dval/(rdpt.norm()+dval/baseval);
              
              if (lcost>rcost)
                map->setDataWin<double>(tempLayer,r,c,lcost);
              else
                map->setDataWin<double>(tempLayer,r,c,rcost);
            }
            
          }
        }
    }
}


void Corridor::convertMapElementToCost(CMapPlus* map, int costLayer, int tempLayer, MapElement& el)
{
  double x,y,radius; 
  double left_bound, right_bound; 
  double top_bound, bottom_bound; 
  double row_res, col_res; 
  int num_cell_cols; 
  int num_cell_rows; 

  double cost; 
  double offset;
  int step_size;
  double COST_DELTA = 1;
  double base_value = 200;

  double old_value;

  //here we update costs on the tempLayer
  if(el.height>0)
    {       
      x = el.center.x; 
      y = el.center.y; 
      radius = el.length;       
      
      row_res = map->getResRows();
      col_res = map->getResCols();     
      
      step_size = (int)floor(radius/row_res);

      for(int k=0; k<step_size; k++)
        {
          offset = radius/step_size;

          radius = radius - k*offset;

          left_bound = x-radius; 
          right_bound = x+radius;
	  
          bottom_bound = y-radius;   
          top_bound = y+radius; 
	  
          num_cell_cols = (int)(ceil( (right_bound-left_bound)/col_res ));
          num_cell_rows = (int)(ceil( (top_bound-bottom_bound)/row_res ));
	  	  
          for(int j=0; j<num_cell_rows; j++)
            {
              for(int i=0; i<num_cell_cols; i++)
                {
                  old_value = map->getDataUTM<double>(tempLayer,left_bound+i*col_res,bottom_bound+j*row_res);
                  if(old_value>0)
                    {
                      cost = old_value + COST_DELTA;
		      
                      if(k==0)
                        {
                          cost = cost + base_value;
                        }
		      
                      map->setDataUTM<double>(tempLayer,left_bound+i*col_res,bottom_bound+j*row_res,cost);
                    }
                }
            }
	  
        }
    }

}

void Corridor::getCostMap(CMapPlus* map, int costLayer, int tempLayer, VehicleState &state, Map* lmap)
{
  //=============================
  //tempLayer vs costLayer
  // - templayer is a layer in the map that is continually filled with new objects from sam's mapper
  //   so more deltas get registered than there should be
  // - costlayer is the map that's actually sent and should have the correct number of deltas 
  //   (more efficient, basically)
  //=============================


  //=============================
  //UPDATE VEHICLE LOCATION FIRST
  //=============================
  map->updateVehicleLoc(state.localX, state.localY);
  
  //==========================================
  //THIS IS WHERE YOU FILL THE MAP WITH VALUES
  //==========================================
  //--eventually this will come from mapper


  /*
    for(int i=200; i<250; i++)
    {
    for(int j =200; j<250; j++)
    {
	  map->setDataWin_Delta<double>(costLayer,i,j,20);
    }
    }
  */  
  
  map->clearLayer(tempLayer);
  
  //NOEL: I took out this code to specify the boundaries determined earlier

  //  MapElement leftbound;
  // MapElement rightbound;
  // LaneLabel llabel;
  //point2 statept(state.localX,state.localY);

  //llabel.segment = lmap->getSegmentID(statept);
  // llabel.lane = lmap->getLaneID(statept);
  
  // point2arr leftptarr;
  //leftptarr =rightbound.geometry;
  //point2arr rightptarr;
  //rightptarr = leftbound.geometry;;
  //************************************************************
  point2arr leftptarr, rightptarr;
  leftptarr = m_polylines[0];
  rightptarr = m_polylines[1];
  //  cout << "leftptarr = " << leftptarr << endl;
  // cout << "rightptarr = " << rightptarr << endl;

  //int val = lmap->getBounds(leftptarr,rightptarr,llabel,statept,60);
  //cout << "############Left label = " << llabel << endl;
  double yaw = state.localYaw;

  if ((int)leftptarr.size()<2){
    //  paintLaneCostCenterLine(map, costLayer, tempLayer,yaw);
    cerr << "Only 1 pt on the boundaries: could not paint the corridor." << endl;
    cerr << "Only 1 pt on the boundaries: could not paint the corridor." << endl;
  } else {  
    cout << "painting corridor" << endl;

    //--------------------------------------------------
    // paint the corridor cost into the map
    //--------------------------------------------------
    paintLaneCostCenterLine(map, costLayer, tempLayer,rightptarr, leftptarr);
  }


  //--------------------------------------------------
  // draw the obstacles into the map
  //--------------------------------------------------
  for(unsigned int i=0; i<lmap->data.size(); i++)
    { 
      convertMapElementToCost(map, costLayer, tempLayer, lmap->data[i]); 
    }  
 
  //now we look at each cell in the tempLayer and compare it with the costLayer; 
  //if the compared cells differ, then update the costLayer
  int num_map_cols;
  int num_map_rows;
 
  double val_tempLayer;
  double val_costLayer;

  num_map_rows = map->getNumRows();
  num_map_cols = map->getNumCols();
  
  for(int i=0; i<num_map_cols; i++)
    {
      for(int j=0; j<num_map_rows; j++)
        {
          val_tempLayer = map->getDataWin<double>(tempLayer,i,j);
          val_costLayer = map->getDataWin<double>(costLayer,i,j);	  
	  
          if(val_tempLayer!=val_costLayer)
            {
              map->setDataWin_Delta<double>(costLayer,i,j,val_tempLayer);
              //map->setDataWin<double>(costLayer,i,j,val_tempLayer);
            }
          
        }
    }
  
  
}






RDDF* Corridor::convertToRddf_old(point2 gloToLocalDelta)
{

  RDDFData rddfData;
	RDDF* rddf = new RDDF();
  // This code is inherited from the old tplanner - was created by Julia Braman

  int i, j, k, l1, l2 ;
  vector<double> rddfx, rddfy, rddfr;

  //TODO: Is this laneWidth really necessary?
  double laneWidth = 12;

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% FIND CLOSEST SEGMENTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  bool error = false ;

  cerr << "Polylines size: " << m_polylines.size() << endl;

  point2arr line1 = m_polylines[0];
  point2arr line2 = m_polylines[1];

  int numline1seg = line1.size()-1;
  int numline2seg = line2.size()-1;
  int numline1pts = numline1seg+1;
  int numline2pts = numline2seg+1;

  double l1p[numline1pts][2], l2p[numline2pts][2] ;
  for( i = 0 ; i < numline1pts ; i++ ) {
    l1p[i][0] = line1[i].x;
    l1p[i][1] = line1[i].y;
		//		printf("l1p[%d] (x,y) = (%3.6f,%3.6f)\n", i, l1p[i][0], l1p[i][1]);
  }

  for( i = 0; i < numline2pts ; i++ ) {
    l2p[i][0] = line2[i].x;
    l2p[i][1] = line2[i].y;
		//printf("l2p[%d] (x,y) = (%3.6f,%3.6f)\n", i, l2p[i][0], l2p[i][1]);  
	}
	
  // Check that end points of two lines are not the same (full lane obstacle)
  if( (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1]) ){
    error = true ;
    cerr << "ERROR convRDDF: (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1])" << endl;
  }

  double mpt[2] = {0, 0} ;
  double r = 0 ;

  // Line 1
  double min1, min2, dist, xdiff, ydiff ;
  int min1num, min2num;
  bool skip = false ;
  double a[2], b[2], maga, magb, adotb, th, check[4], check1[4], check2[4] ;
  double cpt1[2], cpt2[2];
  double cpt[2], pnt[2] ;
  double rddfln1[numline1pts][3], rddfln2[numline2pts][3] ;

	// Line 1
  for( i = 0 ; i < numline1pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0; j < numline2pts; j++ ) {
      xdiff = l1p[i][0] - l2p[j][0] ;
      ydiff = l1p[i][1] - l2p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
        if( dist < min2 ) {
          min1 = min2 ;
          min1num = min2num ;
          min2 = dist ;
          min2num = j ;
        } else {
          min1 = dist ;
          min1num = j ;
        }
      }
    }
    skip = false ;
    if( (i == 0) || (i == (numline1pts-1))) {
      if( i == 0 ) {
        a[0] = l1p[0][0] - l2p[0][0] ;
        a[1] = l1p[0][1] - l2p[0][1] ;
        b[0] = l2p[1][0] - l2p[0][0] ;
        b[1] = l2p[1][1] - l2p[0][1] ;
        maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
        magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
        adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
        a[0] = l1p[numline1pts-1][0] - l2p[numline2pts-1][0] ;
        a[1] = l1p[numline1pts-1][1] - l2p[numline2pts-1][1] ;
        b[0] = l2p[numline2pts-2][0] - l2p[numline2pts-1][0] ;
        b[1] = l2p[numline2pts-2][1] - l2p[numline2pts-1][1] ;
        maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
        magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
        adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
        skip = true ;
    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l1p[i][0] ;
      pnt[1] = l1p[i][1] ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num)) {
        //fprintf( stderr, "min1num = %i, minnum2 = %i\n" , min1num, min2num) ;
        if( min1num < min2num ) {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 )
              check[k] = l2p[min1num][k] ;
            else
              check[k] = l2p[min1num+1][k-2];
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) 
              check[k] = l2p[min2num][k] ;
            else
              check[k] = l2p[min2num+1][k-2] ;
          }
        }
        closept( check, pnt, cpt ) ;
        midpt( cpt, pnt, mpt ) ;
        radius( mpt, pnt, cpt, r ) ;
      } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
        if( min1num < min2num ) {
          for( k = 0 ; k < 4 ; k++ ) {
            if ( k < 2 ) {
              check1[k] = l2p[min1num][k] ;
              check2[k] = l2p[min2num-1][k] ;
            } else {
              check1[k] = l2p[min1num+1][k-2] ;
              check2[k] = l2p[min2num][k-2] ;
            }
          }
        } else { 
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) {
              check1[k] = l2p[min2num][k] ;
              check2[k] = l2p[min1num-1][k] ;
            } else {
              check1[k] = l2p[min2num+1][k-2] ;
              check2[k] = l2p[min1num][k-2] ;
            }
          }
        }
        closept(check1, pnt, cpt1) ;
        closept(check2, pnt, cpt2) ;
        double xdiff1 = cpt1[0] - pnt[0] ;
        double ydiff1 = cpt1[1] - pnt[1] ;
        double xdiff2 = cpt2[0] - pnt[0];
        double ydiff2 = cpt2[1] - pnt[1] ;
        double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
        double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
        if( dist1 < dist2 ) {
          cpt[0] = cpt1[0] ;
          cpt[1] = cpt1[1] ;
        } else {
          cpt[0] = cpt2[0] ;
          cpt[1] = cpt2[1] ;
        }
        midpt( cpt, pnt, mpt ) ;
        radius( mpt, pnt, cpt, r ) ;
      } else {
        if( min1num < (numline2pts-1) ) {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) 
              check1[k] = l2p[min1num][k] ;
            else
              check1[k] = l2p[min1num+1][k-2] ;
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 )
              check1[k] = l2p[min1num-1][k] ;
            else 
              check1[k] = l2p[min1num][k-2] ;
          }
        }
        if( min2num < (numline2pts-1) ) {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) 
              check2[k] = l2p[min2num][k] ;
            else
              check2[k] = l2p[min2num+1][k-2] ;
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 )
              check2[k] = l2p[min2num-1][k] ;
            else 
              check2[k] = l2p[min2num][k-2] ;
          }
        }
        closept(check1, pnt, cpt1) ;
        closept(check2, pnt, cpt2) ;
        double xdiff1 = cpt1[0] - pnt[0] ;
        double ydiff1 = cpt1[1] - pnt[1] ;
        double xdiff2 = cpt2[0] - pnt[0];
        double ydiff2 = cpt2[1] - pnt[1] ;
        double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
        double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
        if( dist1 < dist2 ){
          cpt[0] = cpt1[0] ;
          cpt[1] = cpt1[1] ;
        } else {
          cpt[0] = cpt2[0] ;
          cpt[1] = cpt2[1] ;
        }
        midpt( cpt, pnt, mpt ) ;
        radius( mpt, pnt, cpt, r ) ;
      }
    }
    if( r > laneWidth / 2 )
      r = laneWidth / 2 ;

    rddfln1[i][0] = mpt[0] ;
    rddfln1[i][1] = mpt[1] ;
    rddfln1[i][2] = r ;
    //printf("rddfln1[%d] (x,y,r) = (%3.6f,%3.6f,%3.6f)\n", i, rddfln1[i][0], rddfln1[i][1], rddfln1[i][2]);
  }
	

  // Line 2
  for( i = 0 ; i < numline2pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0 ; j < numline1pts ; j++ ) {
      xdiff = l2p[i][0] - l1p[j][0] ;
      ydiff = l2p[i][1] - l1p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
        if( dist < min2 ) {
          min1 = min2 ;
          min1num = min2num ;
          min2 = dist ;
          min2num = j ;
        } else {
          min1 = dist ;
          min1num = j ;
        }
      }
    }
    skip = false ;
    if( (i == 0) || (i == numline2pts-1) ) {
      if( i == 0 ) { 
        a[0] = l2p[0][0] - l1p[0][0] ;
        a[1] = l2p[0][1] - l1p[0][1] ;
        b[0] = l1p[1][0] - l1p[0][0] ;
        b[1] = l1p[1][1] - l1p[0][1] ;
        maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
        magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
        adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
        a[0] = l2p[numline2pts-1][0] - l1p[numline1pts-1][0] ;
        a[1] = l2p[numline2pts-1][1] - l1p[numline1pts-1][1] ;
        b[0] = l1p[numline1pts-2][0] - l1p[numline1pts-1][0] ;
        b[1] = l1p[numline1pts-2][1] - l1p[numline1pts-1][1] ;
        maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
        magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
        adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
        skip = true ;
    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l2p[i][0] ;
      pnt[1] = l2p[i][1] ;
      //    fprintf( stderr, "min1 = %i, min2 = %i\n", min1num, min2num ) ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num) ) {
        if( min1num < min2num ) {
          for( k = 0 ; k < 4 ; k++ ){
            if( k < 2 )
              check[k] = l1p[min1num][k] ;
            else
              check[k] = l1p[min1num+1][k-2];
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 )
              check[k] = l1p[min2num][k] ;
            else
              check[k] = l1p[min2num+1][k-2];
          }
        }
        closept( check, pnt, cpt ) ;
        midpt( cpt, pnt, mpt ) ;
        radius( mpt, pnt, cpt, r ) ;
      } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
        if( min1num < min2num) {
          for( k = 0 ; k < 4 ; k++ ) {
            if ( k < 2 ) {
              check1[k] = l1p[min1num][k] ;
              check2[k] = l1p[min2num-1][k] ;
            } else {
              check1[k] = l1p[min1num+1][k-2] ;
              check2[k] = l1p[min2num][k-2] ;
            }
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if ( k < 2 ) {
              check1[k] = l1p[min2num][k] ;
              check2[k] = l1p[min1num-1][k] ;
            } else {
              check1[k] = l1p[min2num+1][k-2] ;
              check2[k] = l1p[min1num][k-2] ;
            }
          }
        }
        closept(check1, pnt, cpt1) ;
        closept(check2, pnt, cpt2) ;
        double xdiff1 = cpt1[0] - pnt[0] ;
        double ydiff1 = cpt1[1] - pnt[1] ;
        double xdiff2 = cpt2[0] - pnt[0];
        double ydiff2 = cpt2[1] - pnt[1] ;
        double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
        double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
        if( dist1 < dist2 ) {
          cpt[0] = cpt1[0] ;
          cpt[1] = cpt1[1] ;
        } else {
          cpt[0] = cpt2[0] ;
          cpt[1] = cpt2[1] ;
        }
        midpt( cpt, pnt, mpt ) ;
        radius( mpt, pnt, cpt, r ) ;
      } else {
        if( min1num < numline1pts-1 ) {
          for( k = 0 ; k < 4 ; k++ ){
            if( k < 2 ) 
              check1[k] = l1p[min1num][k] ;
            else
              check1[k] = l1p[min1num+1][k-2] ;
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) 
              check1[k] = l1p[min1num-1][k] ;
            else
              check1[k] = l1p[min1num][k-2] ;
          }
        }
        if( min2num < numline1pts-1 ) {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) 
              check2[k] = l1p[min2num][k] ;
            else
              check2[k] = l1p[min2num+1][k-2] ;
          }
        } else {
          for( k = 0 ; k < 4 ; k++ ) {
            if( k < 2 ) 
              check2[k] = l1p[min2num-1][k] ;
            else
              check2[k] = l1p[min2num][k-2] ;
          }
        }
        closept(check1, pnt, cpt1) ;
        closept(check2, pnt, cpt2) ;
        double xdiff1 = cpt1[0] - pnt[0] ;
        double ydiff1 = cpt1[1] - pnt[1] ;
        double xdiff2 = cpt2[0] - pnt[0];
        double ydiff2 = cpt2[1] - pnt[1] ;
        double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
        double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
        if( dist1 < dist2 ) {
          cpt[0] = cpt1[0] ;
          cpt[1] = cpt1[1] ;
        } else {
          cpt[0] = cpt2[0] ;
          cpt[1] = cpt2[1] ;
        }
        //       fprintf( stderr, "3: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
        midpt( cpt, pnt, mpt ) ;
        radius( mpt, pnt, cpt, r ) ;
      }
    }
    if( r > laneWidth / 2)
      r = laneWidth / 2 ;

    rddfln2[i][0] = mpt[0] ;
    rddfln2[i][1] = mpt[1] ;
    rddfln2[i][2] = r ;
    
    //printf("rddfln2[%d] (x,y,r) = (%3.6f,%3.6f, %3.6f)\n", i, rddfln2[i][0], rddfln2[i][1], rddfln2[i][2]);
  }

	
  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% SORT RDDF POINTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  l1 = 0 ;
  l2 = 0 ;
  double xdiff1, ydiff1, dist1, xdiff2, ydiff2, dist2 ;
  while( (l1 < numline1pts) || (l2 < numline2pts) ) {
    if( (rddfln1[l1][0] == 0) && (rddfln1[l1][1] == 0) && (rddfln1[l1][2] == 0))
      l1++;
    if( (rddfln2[l2][0] == 0) && (rddfln2[l2][1] == 0) && (rddfln2[l2][2] == 0))
      l2++ ;
        
    if( l1 < numline1pts ) {			
			if( (rddfln1[l1][0] == 0) && (rddfln1[l1][1] == 0) && (rddfln1[l1][2] == 0)) {
				l1++;
			}
      xdiff1 = rddfln1[l1][0] - rddfln1[0][0] ;
      ydiff1 = rddfln1[l1][1] - rddfln1[0][1] ;
      dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
    } else {
			dist1 = BIGNUMBER;
		}
    if( l2 < numline2pts ) {
			if( (rddfln2[l2][0] == 0) && (rddfln2[l2][1] == 0) && (rddfln2[l2][2] == 0)) {
        l2++;
			}
      xdiff2 = rddfln2[l2][0] - rddfln2[0][0] ;
      ydiff2 = rddfln2[l2][1] - rddfln2[0][1] ;
      dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
    } else {
			dist2 = BIGNUMBER;
		}


    if( (l1 < numline1pts) && (dist1 <= dist2) || (l2 >= numline2pts) ){
      rddfx.push_back(rddfln1[l1][0]);
      rddfy.push_back(rddfln1[l1][1]);
      rddfr.push_back(rddfln1[l1][2]);
      l1++ ;
    } 
    else if ( l2 < numline2pts ){
      rddfx.push_back(rddfln2[l2][0]);
      rddfy.push_back(rddfln2[l2][1]);
      rddfr.push_back(rddfln2[l2][2]);
      l2++ ;
    }
  } 

  
  double previousNorthing = rddfx[0];
  double previousEasting = rddfy[0];
  double previousRadius = rddfr[0];
  double distFromBegin = 0;
  int rddfNum=1;

  // variables required to calculate the velocity profile
  double rddfSpeed;
  vector<double> velProfile = getVelProfile();
  double desAcc = getDesiredAcc();
  point2 tmpPt;

	double corrLength;
	if ((desAcc==0)||(velProfile[0]-velProfile[1]==0)){
		corrLength = 1; // This value does not matter since we are driving at constant velocity
	} else {
		corrLength = (pow(velProfile[1] ,2) - pow(velProfile[0],2))/(2*desAcc);
	}

  for (int ii = 0; ii < (int)rddfx.size(); ii++){

		if (ii==0){
      // Velocity profile
			rddfSpeed = velProfile[0] + ((velProfile[1]-velProfile[0])/corrLength)*distFromBegin;
      tmpPt.set(rddfx[ii],rddfy[ii]);
      rddfSpeed = getSpeedAtPoint(tmpPt);

      if(rddfSpeed < velProfile[1]) // do not go below the end velocity
        {
          rddfSpeed = velProfile[1];
        }
      point2 tempPtLoc(rddfx[ii], rddfy[ii]);
      point2 tempPtGlo = AliceStateHelper::convertToGlobal(gloToLocalDelta, tempPtLoc);
		
      utm.n = tempPtGlo.x;
      utm.e = tempPtGlo.y;
      gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
      // TODO: Velocity profile
      rddfData.number = rddfNum;
      rddfData.Northing = utm.n;
      rddfData.Easting = utm.e;
      rddfData.maxSpeed = rddfSpeed;
      rddfData.radius = rddfr[ii];
      rddfData.latitude = latlon.latitude;
      rddfData.longitude = latlon.longitude;

      if (rddfx.size()==1){
        cerr << "ERROR convRDDF: only one data point in RDDFVector." << endl;
      }
      
      rddf->addDataPoint(rddfData);
      rddfNum++;

    } 
    else if ((sqrt(pow(rddfx[ii]-previousNorthing,2)+pow(rddfy[ii]-previousEasting,2))>5*EPS) ||
             ((sqrt(pow(rddfx[ii]-previousNorthing,2)+pow(rddfy[ii]-previousEasting,2))<=5*EPS) 
              && (fabs(previousRadius-rddfr[ii])>5*EPS))){ // this gets rid of rddf's that are too close together

      // Velocity profile
      rddfSpeed = velProfile[0] + ((velProfile[1]-velProfile[0])/corrLength)*distFromBegin;
      if(rddfSpeed < velProfile[1]) // do not go below the end velocity
        {
          rddfSpeed = velProfile[1];
        }

			point2 tempPtLoc(rddfx[ii], rddfy[ii]);
			point2 tempPtGlo = AliceStateHelper::convertToGlobal(gloToLocalDelta, tempPtLoc);
			
      utm.n = tempPtGlo.x;
      utm.e = tempPtGlo.y;
      gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
      // TODO: Velocity profile
      rddfData.number = rddfNum;
      rddfData.Northing = utm.n;
      rddfData.Easting = utm.e;
      rddfData.maxSpeed = rddfSpeed;
      rddfData.radius = rddfr[ii];
      rddfData.latitude = latlon.latitude;
      rddfData.longitude = latlon.longitude;
      


      rddf->addDataPoint(rddfData);
      rddfNum++;
    }
    distFromBegin = distFromBegin + 
      sqrt(pow(rddfx[ii]-previousNorthing,2) + pow(rddfy[ii]-previousEasting,2));

    previousNorthing = rddfx[ii];
    previousEasting = rddfy[ii];
    previousRadius = rddfr[ii];

  } 

  if ((m_verbose) || (m_debug)){
    cout<<"CORRIDOR.convertRddfCorridor: OK"<<endl;
  }
	return rddf;
  //TODO other conversions
} 
 
 
void Corridor::closept( double lnseg[4], double pt[2], double cpt[2] ) 
{ 
  double min[2], max[2], a[2], b1[2], b2[2] ;
  double maga, magb, adotb1, adotb2, minth, maxth, distmin, distln, distmax ;

  min[0] = lnseg[0] ;
  min[1] = lnseg[1] ;
  max[0] = lnseg[2] ;
  max[1] = lnseg[3] ;

  a[0] = pt[0] - min[0] ;
  a[1] = pt[1] - min[1] ;
  b1[0] = max[0] - min[0] ;
  b1[1] = max[1] - min[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  magb = sqrt( b1[0] * b1[0] + b1[1] * b1[1] ) ;
  adotb1 = a[0] * b1[0] + a[1] * b1[1] ;
  minth = acos(adotb1/(maga*magb)) ;
  distmin = maga ;
  distln = magb ;

  a[0] = pt[0] - max[0] ;
  a[1] = pt[1] - max[1] ;
  b2[0] = min[0] - max[0] ;
  b2[1] = min[1] - max[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  adotb2 = a[0] * b2[0] + a[1] * b2[1] ;
  maxth = acos(adotb2/(maga*magb)) ;
  distmax = maga ;

  double mdist1, a1, mdist2, a2, check, apt1[2], apt2[2] ;
  if( (minth <= PI/2) && (maxth <= PI/2 ) && (minth > 0) && (maxth > 0)) {
    mdist1 = distmin * sin(minth) ;
    a1 = mdist1 / tan(minth) ;
    mdist2 = distmax * sin(maxth) ;
    a2 = mdist2 / tan(maxth) ;
    check = fabs( a1 + a2 - distln ) ;
    apt1[0] = ( a1 / distln ) * b1[0] + min[0] ;
    apt1[1] = ( a1 / distln ) * b1[1] + min[1] ;
    apt2[0] = ( a2 / distln ) * b2[0] + max[0] ;
    apt2[1] = ( a2 / distln ) * b2[1] + max[1] ;
    if( check < 0.1 ){ 
      cpt[0] = ( apt1[0] + apt2[0] ) / 2 ;
      cpt[1] = ( apt1[1] + apt2[1] ) / 2 ;
    } else {
      if( mdist1 < mdist2 ) {
        cpt[0] = apt1[0] ;
        cpt[1] = apt1[1] ;
      } else { 
        cpt[0] = apt2[0] ;
        cpt[1] = apt2[1] ;
      }
    }
  } else {
    if( distmin < distmax ) {
      cpt[0] = min[0] ;
      cpt[1] = min[1] ;
    } else {
      cpt[0] = max[0] ;
      cpt[1] = max[1] ;
    }
  }
}

void Corridor::midpt( double pt1[2], double pt2[2], double mpt[2] ) 
{ 
  mpt[0] = (pt1[0] + pt2[0]) / 2 ;
  mpt[1] = (pt1[1] + pt2[1]) / 2 ;
}

void Corridor::radius( double mpt[2], double pt1[2], double pt2[2], double &r ) 
{
  double xdiff, ydiff, dist1, dist2;

  xdiff = (mpt[0] - pt1[0]) ;
  ydiff = (mpt[1] - pt1[1]) ;
  dist1 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  xdiff = (mpt[0] - pt2[0]) ;
  ydiff = (mpt[1] - pt2[1]) ;
  dist2 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  if( dist1 <= dist2 )
    r = dist1 ;
  else
    r = dist2 ;
}


/*!Function that generates the polyhedral corridor
 * If you think this is a mess:
 * "Welcome to Computational Geometry in pure C++"
 *
 * S Di Cairano Apr-07
 */


void Corridor::generatePolyCorridor()
{   
   
    int Ngates=m_polylines[0].size();
    CPolytope** rawPoly;
	rawPoly=new CPolytope*[Ngates-1];
    point2arr leftBound = m_polylines[0];
    point2arr rightBound = m_polylines[1];

    cout<<"About to delete m_polyCorridor"<<endl; 

    if (0 != m_polyCorridor) {
      delete[] m_polyCorridor;  //clearing the previous corridor
    }
	m_nPolytopes = Ngates-1;
	m_polyCorridor = new CPolytope [m_nPolytopes]; //creating the empty corridor
	int NofDim = 2;
	CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
	int NofVtx = 4;
	double*** vertices;
	vertices = new double**[Ngates-1];
	for(int k=0; k<Ngates-1;k++)
	{
	   vertices[k] = new double*[NofDim];
	   for(int i=0;i<NofDim;i++)
	   {
	       vertices[k][i] = new double[NofVtx];
	   }
	}
	for(int i=0;i<Ngates-1;i++)
	{
        vertices[i][0][1]=leftBound[i].x;
        vertices[i][1][1]=leftBound[i].y;
        vertices[i][0][2]=leftBound[i+1].x;
        vertices[i][1][2]=leftBound[i+1].y;
        vertices[i][0][3]=rightBound[i+1].x;
        vertices[i][1][3]=rightBound[i+1].y;
        vertices[i][0][0]=rightBound[i].x;
        vertices[i][1][0]=rightBound[i].y;
	    rawPoly[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
	
	    algGeom->FacetEnumeration(rawPoly[i]);
	    algGeom->VertexEnumeration(rawPoly[i]); //vertex reduction
        if(rawPoly[i]->getNofVertices() != 4)
		   cerr << "The gates: " << i << "," << i+1 << " are degenerate " << endl;

	}
      
	cout << "Raw Polytopes built." << endl;
	for(int i=0;i<Ngates-1;i++)
	{   
	    double** verticesFinal;
		int NofVtxFinal = rawPoly[i]->getNofPoints();
	    double** verticesIntPrev;
        int NofVtxIntPrev = 0;
    	double** verticesIntNext;
        int NofVtxIntNext = 0;
	    
	    if(i>0)//expand backwards
		{
            int Nrows=rawPoly[i]->getNofRows(); 
            int Ncols=rawPoly[i]->getNofColumns();
		    double** Amat;
		    double* bvect;
		    bvect = new double[Nrows];
		    Amat= new double*[Nrows];
		    for(int j=0;j<Nrows;j++)
		        Amat[j] = new double[Ncols];
        
		    rawPoly[i]->getA(&Nrows,&Ncols,Amat);
		    rawPoly[i]->getb(&Nrows,bvect);
 /*  search for the row that defines the border to be exapnaded. 
  *  It is such that Ai*x1=bi, Ai*x2=bi, hence Ai*(x1+x2)-2bi=0, which is  argmax_i Ai(x1+x2)-2*bi		                  
 */
            double myVect[NofDim];
            myVect[0] = leftBound[i].x+rightBound[i].x;
            myVect[1] = leftBound[i].y+rightBound[i].y;
			double expNorm = 0.5*sqrt(pow((leftBound[i].x-leftBound[i-1].x),2)+pow((leftBound[i].y-leftBound[i-1].y),2))+
			          0.5* sqrt(pow((rightBound[i].x-rightBound[i-1].x),2)+pow((rightBound[i].y-rightBound[i-1].y),2));
			int expIdx=0;
			double myMax=-1;
			for(int j=0;j<Nrows;j++)
			{
			    double temp = 0;
			    for(int h=0;h<Ncols;h++)
			        temp+=Amat[j][h]*myVect[h];
				temp-=2*bvect[j];
                if(temp>=myMax)
			    {
			        myMax=temp;
				    expIdx=j;      //index of the row to be expanded
                }
		    }
			double normARow=0;
			for(int j=0;j<Ncols;j++) //compute the norm of the row to be expanded to normalize
			{
                normARow+=Amat[expIdx][j]*Amat[expIdx][j];				

			}
			bvect[expIdx]+=(EXPPERC*expNorm)*normARow;    // expansion
            int NrowsPrev=rawPoly[i-1]->getNofRows(); 
            int NcolsPrev=rawPoly[i-1]->getNofColumns();
		    double** AmatPrev;
		    double* bvectPrev;
		    bvectPrev = new double[NrowsPrev];
		    AmatPrev = new double*[NrowsPrev];
		    for(int j=0;j<NrowsPrev;j++)
		        AmatPrev[j] = new double[NcolsPrev];
		    rawPoly[i-1]->getA(&NrowsPrev,&NcolsPrev,AmatPrev);
		    rawPoly[i-1]->getb(&NrowsPrev,bvectPrev);
            
			double** Aint;
            double* bint;
			bint = new double[Nrows+NrowsPrev];
			Aint = new double*[Nrows+NrowsPrev];
            for(int j=0;j<Nrows;j++)
			{
			    Aint[j] = Amat[j];   //I use the memory which has been already allocated
			    bint[j] = bvect[j];
            }
            for(int j=Nrows;j<(NrowsPrev+Nrows);j++)
			{
			    Aint[j] = AmatPrev[j-Nrows];   //I use the memory which has been already allocated
			    bint[j] = bvectPrev[j-Nrows];
            }
			int NrowsIntPrev =Nrows+NrowsPrev;
			int NcolsIntPrev =Ncols;
			
			NofVtxIntPrev = 4;
	        verticesIntPrev = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
	            verticesIntPrev[j] = new double[NofVtxIntPrev];

            algGeom->VertexEnumeration(&NrowsIntPrev,&NcolsIntPrev,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntPrev, verticesIntPrev);
			for(int j=0;j<NofDim;j++)
                delete[] verticesIntPrev[j];
	        delete[] verticesIntPrev;

	        verticesIntPrev = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
	            verticesIntPrev[j] = new double[NofVtxIntPrev];
		
            algGeom->VertexEnumeration(&NrowsIntPrev,&NcolsIntPrev,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntPrev, verticesIntPrev);
	       
    //        delete[] bint;       //this uses memory allocated by others. Hence I deallocate it using others
            delete[] bvectPrev;
            delete[] bvect;
            delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
		    for(int j=0;j<Nrows;j++)
			    delete[] Amat[j];
            delete[] Amat;
            for(int j=0;j<NrowsPrev;j++)
                delete[] AmatPrev[j];
            delete[] AmatPrev;
	    } //end if (not the last)
	 
	    if(i<Ngates-2)//expand forward
		{
            int Nrows=rawPoly[i]->getNofRows(); 
            int Ncols=rawPoly[i]->getNofColumns();
		    double** Amat;
		    double* bvect;
		    bvect = new double[Nrows];
		    Amat= new double*[Nrows];
		    for(int j=0;j<Nrows;j++)
		        Amat[j] = new double[Ncols];
        
		    rawPoly[i]->getA(&Nrows,&Ncols,Amat);
		    rawPoly[i]->getb(&Nrows,bvect);
 /*  search for the row that defines the border to be exapnaded. 
  *  It is such that Ai*x1=bi, Ai*x2=bi, hence Ai*(x1+x2)-2bi=0, which is  argmax_i Ai(x1+x2)-2*bi		                  
 */
            double myVect[NofDim];
            myVect[0] = leftBound[i+1].x+rightBound[i+1].x;
            myVect[1] = leftBound[i+1].y+rightBound[i+1].y;
			int expIdx=0;
			double myMax=-1;
			double expNorm = 0.5*sqrt(pow((leftBound[i].x-leftBound[i+1].x),2)+pow((leftBound[i].y-leftBound[i+1].y),2))+
			          0.5* sqrt(pow((rightBound[i].x-rightBound[i+1].x),2)+pow((rightBound[i].y-rightBound[i+1].y),2));
			for(int j=0;j<Nrows;j++)
			{
			    double temp = 0;
			    for(int h=0;h<Ncols;h++)
			        temp+=Amat[j][h]*myVect[h];
				temp-=2*bvect[j]; 
                if(temp>=myMax)
			    {
			        myMax=temp;
				    expIdx=j;      //index of the row to be expanded
                }
		    }
			double normARow=0;
			for(int j=0;j<Ncols;j++) //compute the norm of the row to be expanded to normalize
			{
                normARow+=Amat[expIdx][j]*Amat[expIdx][j];				

			}

			bvect[expIdx]+=(EXPPERC*expNorm)*normARow;    // expansion
            int NrowsNext=rawPoly[i+1]->getNofRows(); 
            int NcolsNext=rawPoly[i+1]->getNofColumns();
		    double** AmatNext;
		    double* bvectNext;
		    bvectNext = new double[NrowsNext];
		    AmatNext= new double*[NrowsNext];
		    for(int j=0;j<NrowsNext;j++)
		        AmatNext[j] = new double[NcolsNext];
		    rawPoly[i+1]->getA(&NrowsNext,&NcolsNext,AmatNext);
		    rawPoly[i+1]->getb(&NrowsNext,bvectNext);
            
			double** Aint;
            double* bint;
			bint = new double[Nrows+NrowsNext];
			Aint = new double*[Nrows+NrowsNext];
            for(int j=0;j<Nrows;j++)
			{
			    Aint[j] = Amat[j];   //I use the memory which has been already allocated
			    bint[j] = bvect[j];
            }
            for(int j=Nrows;j<(NrowsNext+Nrows);j++)
			{
			    Aint[j] = AmatNext[j-Nrows];   //I reuse the memory which has been already allocated
			    bint[j] = bvectNext[j-Nrows];
            }
			int NrowsIntNext =Nrows+NrowsNext;
			int NcolsIntNext=Ncols;
			
			NofVtxIntNext= 4;
	        verticesIntNext = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
	            verticesIntNext[j] = new double[NofVtxIntNext];
            algGeom->VertexEnumeration(&NrowsIntNext,&NcolsIntNext,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntNext, verticesIntNext);
			for(int j=0;j<NofDim;j++)
                delete[] verticesIntNext[j];
	        delete[] verticesIntNext;

	        verticesIntNext = new double*[NofDim];
	        for(int j=0;j<NofDim;j++)
	            verticesIntNext[j] = new double[NofVtxIntNext];
		
            algGeom->VertexEnumeration(&NrowsIntNext,&NcolsIntNext,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntNext, verticesIntNext);
	       
//            delete[] bint;     //this uses memory allocated by others. Hence I deallocate it using others
            delete[] bvectNext;
            delete[] bvect;
            delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
		    for(int j=0;j<Nrows;j++)
			    delete[] Amat[j];
            delete[] Amat;
            for(int j=0;j<NrowsNext;j++)
                delete[] AmatNext[j];
            delete[] AmatNext;

	    } //end if (not the last)

        //building the overlapping polytopes
        if(i>0)
		    NofVtxFinal+=NofVtxIntPrev;
		if(i<Ngates-2)
		    NofVtxFinal+=NofVtxIntNext;
        verticesFinal = new double*[NofDim];
		for(int h=0;h<NofDim;h++)
            verticesFinal[h] = new double[NofVtxFinal];
		for(int j=0;j<NofVtxFinal;j++)
		{
            verticesFinal[0][j]=0.0;
            verticesFinal[1][j]=0.0;
		}
		for(int j=0;j<NofVtx;j++)
		{
            verticesFinal[0][j]=vertices[i][0][j];
            verticesFinal[1][j]=vertices[i][1][j];
		}
		if(i>0)
        {          
		    for(int j=0;j<NofVtxIntPrev;j++)
		    {
                verticesFinal[0][j+NofVtx]=verticesIntPrev[0][j];
                verticesFinal[1][j+NofVtx]=verticesIntPrev[1][j];
		    }
     
		}
		if(i<Ngates-2)
        {          
		    for(int j=0;j<NofVtxIntNext;j++)
		    {
                verticesFinal[0][j+NofVtx+NofVtxIntPrev]=verticesIntNext[0][j];
                verticesFinal[1][j+NofVtx+NofVtxIntPrev]=verticesIntNext[1][j];
		    }
     
		}
		m_polyCorridor[i]  =  new CPolytope( &NofDim, &NofVtxFinal, (const double**)  verticesFinal ); //Ask Melvin: is this Correct?
		algGeom->FacetEnumeration(&m_polyCorridor[i]);
		algGeom->VertexEnumeration(&m_polyCorridor[i]); //vertex reduction
		cout << &m_polyCorridor[i] << endl;
	
        for(int j=0;j<NofDim;j++)
        {  
            delete[] verticesFinal[j];
            if(i<Ngates-2)
			    delete[] verticesIntNext[j];
            if(i>0)
			    delete[] verticesIntPrev[j];
        }

    } //end for (all the polytopes)

    for(int i=0; i<Ngates-1; i++)
	{
	    for(int j=0; j<NofDim; j++)
	        delete[] vertices[i][j];
	    delete[] vertices[i];
    }
	delete[] vertices;
    for(int i=0; i<Ngates-1; i++)
	{
	    delete rawPoly[i];
    }
    delete[] rawPoly;
	delete algGeom;
}

void Corridor::printPolyCorridor(void)
{
    cout << "Number of Polytopes : " << m_nPolytopes << endl;
    for(int i=0;i<m_nPolytopes;i++)
	   cout << " Polytope " << i << endl << &m_polyCorridor[i] << endl;

}

/*!Function that sends  the polyhedral corridor
 * to reduce the network load we extract the polytopes vertices and we send those.
 * the resulting message is a vector of vector of 2D points. The external vector represents
 * the list of polytopes, which are lists (the internal vectors) of vertices, which are 2-d arrays of double.
 *
 * S Di Cairano Apr-07
 */
void Corridor::sendPolyCorridor(void)
{
    sendCorr corrVertices;
    sendPoly polyVertices;
    for(int i=0;i<m_nPolytopes; i++)
	{
        double** vertices;
		int NofDim =  m_polyCorridor[i].getNofDimensions();
		int NofVtx = m_polyCorridor[i].getNofVertices();
		vertices = new double*[NofDim];
		for(int j=0;j<NofDim;j++)
            vertices[j]=new double[NofVtx];
        m_polyCorridor[i].getVertices(&NofDim, &NofVtx, vertices);
		
		for(int j=0;j<NofVtx;j++)
		{
		    polyVertices.push_back( sendPoint(vertices[0][j],  vertices[1][j]));
        }
	    corrVertices.push_back(polyVertices);
       
        polyVertices.clear(); //clearing the current polytope
		for(int j=0;j<NofDim;j++)
            delete[] vertices[j]; //clearing the current vertices
	    delete[] vertices;

	}
   m_polyCorridorTalker.send(&corrVertices);    
}

