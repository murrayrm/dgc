#include "StoppedToUTurn.hh"
#include <math.h>
#include <list>


StoppedToUTurn::StoppedToUTurn(ControlState * state1, ControlState * state2)
:  ControlStateTransition(state1, state2)
   , m_velBound(0.1)
{
}

StoppedToUTurn::StoppedToUTurn()
{

}

StoppedToUTurn::~StoppedToUTurn()
{

}

double StoppedToUTurn::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{
  cout << "in StoppedToUTurn::meetTransitionConditions" << endl;
      
    // cState was not used because the resetConditions was commented!
    /* Stopped *cState = static_cast < Stopped * >(controlState); */

    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      //double currVel = AliceStateHelper::getVelocityMag(vehState);
      //if ((SegGoals::UTURN == planHorizon.getSegGoal(0).segment_type)&&(currVel<m_velBound)) {
      if (SegGoals::UTURN == planHorizon.getSegGoal(0).segment_type) {
            m_probability = 1;
            /* cState->resetConditions(); */
        } else {
            m_probability = 0;
        }
        cout << "stopped to uturn m_probability = " << m_probability << endl;
    }
        break;
        
    default:
    
        m_probability = 0;
        cerr << "StoppedToUTurn.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_probability);
    
    return m_probability;
}
