#ifndef CLEARREGION_HH_
#define CLEARREGION_HH_


#include "TrafficState.hh"

class ClearRegion {


public: 

ClearRegion();
~ClearRegion();
void setCurrentSubState(TrafficState* trafficState);
TrafficState* getCurrentSubState();

private: 

TrafficState* m_currentState; 
TrafficState* m_currentSubState;

};

#endif /*CLEARREGION_HH_*/
