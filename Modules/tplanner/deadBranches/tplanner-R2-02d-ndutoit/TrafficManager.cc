#include "TrafficManager.hh"

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log, FILE* logFile)
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_logData(log)
    , m_logFile(logFile)
    , m_latestID(0)
    , m_currentID(0)
{
  m_controlGraph = ControlStateFactory::createControlStates();

  m_estimator = new TrafficStateEstimator(debug, verbose, log);

  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionTrafficInterface::generateNorthface(skynetKey, this);

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = TrafficManagerControlStatus::READY_FOR_NEXT;

  m_traffCorrInterfaceSF = TraffCorridorInterface::generateSouthface(skynetKey, this);

}

TrafficManager::~TrafficManager()
{

  MissionTrafficInterface::releaseNorthface(m_missTraffInterfaceNF);
  TraffCorridorInterface::releaseSouthface(m_traffCorrInterfaceSF);
  //TODO close file before deleting
  delete m_logFile;
  delete m_estimator; 
}

void TrafficManager::TrafficPlanningLoop(void)
{
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective );
      control(&m_controlStatus, &m_mergedDirective );
      usleep(100000);
    }
}


void TrafficManager::arbitrate(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus *controlStatus = dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective *mergedDirective = dynamic_cast<TrafficManagerMergedDirective *>(md);

  RoutePlannerDirectiveResponse* directiveResp = &(controlStatus->rpcs); 
  SegGoalsStatus segGoalsStatus; 

  /* If we are executing, then do not try to get another directive */
  if (controlStatus->status == RoutePlannerDirectiveResponse::EXECUTING) {
    if (m_logData)
      {
	fprintf( m_logFile, "  Control status: goalID = %d\t status = %d\n", controlStatus->id, controlStatus->status );
      }
    return;
  } //end EXECUTING
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == TrafficManagerControlStatus::COMPLETED ||
      controlStatus->status == TrafficManagerControlStatus::FAILED) {
    
    SegGoalsStatus response;
    response.goalID = controlStatus->id;
    
    if (controlStatus->status == TrafficManagerControlStatus::COMPLETED) {
      response.status = SegGoalsStatus::COMPLETED;
      
    } else {
      response.status = SegGoalsStatus::FAILED;
      //response.reason = controlStatus->reason;
    }

    cerr << "TrafficManagerControl: sending response (" 
	 << response.id << ") - "
	 << (response.status == SegGoalsStatus::COMPLETED ? "completed" : "other")
	 << endl;
    m_missTraffInterfaceNF->sendResponse( &response );
  } //end if COMPLETED or FAILED

  /* Compute the next mergedDirective, this is just a pass through now */
  if (m_missTraffInterfaceNF->haveNewDirective()) {
    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective( &newDirective );

    cerr << "TrafficManagerControl: have new directive (" 
	 << newDirective.goalID << ")" << endl;

    //    SegGoals::SegmentType segType = newDirective.segment_type;

    //    if (SegGoals::SegmentType::PAUSE == segType)
    // {
	//empty the m_segmentTypes vector and create a PAUSE directive 
	m_contrDirectiveQ.clear();
	m_contrDirectiveQ.push_back(newDirective);
	//      } 
    // TODO: Determine if you want to reject this directive. This
    // includes checking that the id of newDirective is unique.
    bool reject = false;

    // Update the merged directive if newDirective is not rejected.
    if (!reject) {
      m_contrDirectiveQ.push_back(newDirective);

    } else {
      // TODO: Default extension
    }
  }// end NorthFace has new directive
}

void TrafficManager::control(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus* controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective* mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);
  
  int mergedDirSize = m_contrDirectiveQ.size();
  if (1 == mergedDirSize)
    {
      //You now want to execute these directives 

    } else if (3 == mergedDirSize ) {

      /* If no new directive, we are still executing the old one */
      controlStatus->status = TrafficManagerControlStatus::READY_FOR_NEXT; 
      return; 
    }
  
  double number_arg = mergedDirective->number_arg;

  if (mergedDirective->id != m_currentID) {
    cerr << "Sending command " << number_arg << " to Corridor Control (" 
	 << mergedDirective->id << ")" << endl;
    m_currentID = mergedDirective->id;
    controlStatus->id = m_currentID;
    controlStatus->status = TrafficManagerControlStatus::COMPLETED;

  } 
}


ControlState* TrafficManager::determineControlState(ControlState  *currControlState, TrafficState *currTrafficState, PlanningHorizon currPlanHorizon, Map* localMap, VehicleState currVehState) 
{
  ControlState* controlState; 

  // m_currSegGoals = currSegGoals; 
  //  m_localMap = localMap; 
  //  m_currVehState = vehState;

  
  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(currControlState); 

  //  determinePlanningHorizon();	
  unsigned int i = 0;
  ControlStateTransition* transition;

  for (i; i < transitions.size(); i++) 
    {		
      transition = static_cast<ControlStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(currControlState, currTrafficState, currPlanHorizon, localMap, currVehState);
    }
  
  controlState = chooseMostProbableControlState(currControlState, transitions);
  
  if (currControlState->getType() == controlState->getType())
    {
      controlState = currControlState;
    } else {
      controlState->setInitialPosition(currVehState);
      controlState->setInitialVelocity(currVehState);
      controlState->setInitialTime();
    }
  
  return controlState; 
  //  m_currStateType = (ControlStateFactory::ControlStateType) m_currControlState.getType(); //FIX we may not need this 
}


// ControlState TrafficManager::getCurrentControlState() 
// {
//   return m_currControlState;
// }

// ControlStateFactory::ControlStateType TrafficManager::getCurrentControlStateType() 
// {
//   return m_currStateType;
// }


ControlState* TrafficManager::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
	ControlState* currControlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficManager::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
				transProb = transitions[i]->getUncertainty();
				if(1 == transProb) {
					probOneTrans.push_back(transitions[i]);
				} else if ( m_probThresh < transProb) {
					probAboveThresh.push_back(transitions[i]);		
				} else if ( m_probThresh >= transProb) {
					probBelowThresh.push_back(transitions[i]);		
				}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
			currControlState = controlState; 
		}
  } else { 
    if ((m_verbose) || (m_debug)){  
      cout << "in TrafficManager::chooseMostProbable... received transitions of size " << transitions.size() << endl;
    }
  }
	

  return currControlState;
}

void TrafficManager::determinePlanningHorizon(PlanningHorizon & currPlanHorizon, TrafficState * currTrafficState, list<SegGoals> currSegGoals) 
{
  //cout << "in TrafficManager::determinePlanningHorizon:" << endl;
  //cout << "currSegGoals size = " << currSegGoals.size() <<endl;
  
  //cout << "curr type = " << currTrafficState->getType() << endl;
  
  TrafficStateFactory::TrafficStateType tStateType = (TrafficStateFactory::TrafficStateType) currTrafficState->getType();
  
  //cout << "tStateType =  "<< tStateType <<endl;
  
  currPlanHorizon.clear();
  
  switch(tStateType) {
  case TrafficStateFactory::ZONE_REGION:		// want to plan over two segments

    currPlanHorizon.addGoal(currSegGoals.front()); 
    currSegGoals.pop_front();
    currPlanHorizon.addGoal(currSegGoals.front()); 		
    break;
  case TrafficStateFactory::ROAD_REGION:
    // plan over one segment
    currPlanHorizon.addGoal(currSegGoals.front()); 
    break;
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    // plan over one segment
    currPlanHorizon.addGoal(currSegGoals.front()); 
    break;
  case TrafficStateFactory::INTERSECTION_STOP:
    // plan over two segments
    currPlanHorizon.addGoal(currSegGoals.front()); 
    currSegGoals.pop_front();
    currPlanHorizon.addGoal(currSegGoals.front()); 		
    break;
  default:
    break;
  };
  if ((m_verbose) || (m_debug)){  
    cout<<"SIZE OF CURRPLANHORIZON GOALS " <<  currPlanHorizon.getSize()<< endl;
  } 

  //m_currPlanHorizon = subSegGoals;
  
}

