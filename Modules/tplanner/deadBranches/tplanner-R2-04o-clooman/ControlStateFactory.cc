#include "ControlStateFactory.hh"
#include "ControlStateTransition.hh"


#include "ControlState.hh"
#include "LaneKeeping.hh"
#include "Stop.hh"
#include "Stopped.hh"
#include "UTurn.hh"
#include "LaneChange.hh"
#include "Creep.hh"

#include "LaneKeepingToStop.hh"
#include "LaneKeepingToLaneChange.hh"
#include "StopToStopped.hh"
#include "StopObsToStoppedObs.hh"
#include "StoppedToLaneKeeping.hh"
#include "StoppedToUTurn.hh"
#include "UTurnToLaneKeeping.hh"
#include "UTurnToUTurn.hh"
#include "LaneChangeToLaneKeeping.hh"
#include "Passing1_LaneKeepingToLaneChange.hh"
#include "Passing3_LaneKeepingToLaneChange.hh"
#include "StoppedToPassing_LC.hh"
#include "LaneKeepingToStopObs.hh"
#include "Passing_LaneKeepingToLaneKeeping.hh"
#include "CreepToStop.hh"
#include "StoppedToCreep.hh"
#include "Pause.hh"
#include "StopObsToLaneKeeping.hh"

ControlState* ControlStateFactory::m_initState;
ControlState* ControlStateFactory::m_pauseState;

ControlStateFactory::ControlStateFactory()
{

}

ControlStateFactory::ControlStateFactory(bool debug, bool verbose, bool log)
{
    m_debug = debug;
    m_verbose = verbose;
    m_log = log;
	
    // We need to set these variables in ControlState and ControlStateTransition too
    ControlState::setOutputParams(debug, verbose, log);
    ControlStateTransition::setOutputParams(debug, verbose, log);
}


ControlStateFactory::ControlStateFactory(CmdLineArgs cLArgs)
{
    m_debug = cLArgs.debug;
    m_verbose = cLArgs.verbose;
    m_log = cLArgs.log;
	
    // We need to set these variables in ControlState and ControlStateTransition too
    ControlState::setOutputParams(m_debug, m_verbose, m_log);
    ControlStateTransition::setOutputParams(m_debug, m_verbose, m_log);
}


ControlStateFactory::~ControlStateFactory()
{
  delete m_initState; 
  delete m_pauseState; 
}

StateGraph ControlStateFactory::createControlStates()
{

    vector < StateTransition * >trans;

    int stateId = 0;            // TODO fix this nonsense

    // Control States

    // Pause state 
    Pause *pause = new Pause(++stateId, PAUSE);
    m_pauseState = pause; 

    // Road region
    LaneKeeping *laneKeeping = new LaneKeeping(++stateId, LANE_KEEPING);
    m_initState = laneKeeping;
    LaneChange *laneChange = new LaneChange(++stateId, LANE_CHANGE);
    LaneChange *passing1_LC = new LaneChange(++stateId, LANE_CHANGE);
    LaneKeeping *passing2_LK = new LaneKeeping(++stateId, LANE_KEEPING);
    LaneChange *passing3_LC = new LaneChange(++stateId, LANE_CHANGE);
    Stop *stop = new Stop(++stateId, STOP);
    Stopped *stopped = new Stopped(++stateId, STOPPED, Stopped::INTERSECTION);
    Creep *creep = new Creep(++stateId, CREEP);

    Stop *stopObs = new Stop(++stateId, STOP);
    Stopped *stoppedObs = new Stopped(++stateId, STOPPED, Stopped::OBSTACLE);
    UTurn *uTurn1 = new UTurn(++stateId, UTURN, 1);
    UTurn *uTurn2 = new UTurn(++stateId, UTURN, 2);
    UTurn *uTurn3 = new UTurn(++stateId, UTURN, 3);
    UTurn *uTurn4 = new UTurn(++stateId, UTURN, 4);
    UTurn *uTurn5 = new UTurn(++stateId, UTURN, 5);

    // Control State Transitions
    // Nominal driving
    // LaneKeepingToStop *laneKeepStopTrans = new LaneKeepingToStop(laneKeeping, stop);
    LaneKeepingToLaneChange *laneKeepLaneChangeTrans = new LaneKeepingToLaneChange(laneKeeping, laneChange);
    LaneChangeToLaneKeeping *laneChangeLaneKeepTrans = new LaneChangeToLaneKeeping(laneChange, laneKeeping);
    // Nominal UTurn
    StoppedToUTurn *laneKeepUTurnTrans = new StoppedToUTurn(laneKeeping, uTurn1);

    // Obstacle handling
    UTurnToUTurn *uturnUTurnTrans1 = new UTurnToUTurn(uTurn1, uTurn2, 1);
    UTurnToUTurn *uturnUTurnTrans2 = new UTurnToUTurn(uTurn2, uTurn3, 2);
    UTurnToUTurn *uturnUTurnTrans3 = new UTurnToUTurn(uTurn3, uTurn4, 3);
    UTurnToUTurn *uturnUTurnTrans4 = new UTurnToUTurn(uTurn4, uTurn5, 4);
    UTurnToLaneKeeping *uTurnLaneKeepTrans = new UTurnToLaneKeeping(uTurn5, laneKeeping);

    Passing1_LaneKeepingToLaneChange *laneKeepPassing1Trans = new Passing1_LaneKeepingToLaneChange(laneKeeping, passing1_LC);
    LaneChangeToLaneKeeping *passing1Passing2Trans = new LaneChangeToLaneKeeping(passing1_LC, passing2_LK);
    Passing3_LaneKeepingToLaneChange *passing2Passing3Trans = new Passing3_LaneKeepingToLaneChange(passing2_LK, passing3_LC);
    LaneChangeToLaneKeeping *passing3LaneKeepTrans = new LaneChangeToLaneKeeping(passing3_LC, laneKeeping);
    // Obstacle handling while passing
    Passing_LaneKeepingToLaneKeeping *passingLaneKeepLaneKeepTrans = new Passing_LaneKeepingToLaneKeeping(passing2_LK, laneKeeping);

    LaneKeepingToStopObs *laneKeepStopObsTrans = new LaneKeepingToStopObs(laneKeeping, stopObs);

    // Handling of wrong positive
    StopObsToLaneKeeping *stopToLK = new StopObsToLaneKeeping(stopObs, laneKeeping);

    // intersection handling
    LaneKeepingToStop *laneKeepStopTrans = new LaneKeepingToStop(laneKeeping, stop);
    StoppedToLaneKeeping *stoppedLaneKeepTrans = new StoppedToLaneKeeping(stopped, laneKeeping);
    StopToStopped *stopStoppedTrans = new StopToStopped(stop, stopped);
    StoppedToCreep *stoppedCreepTrans = new StoppedToCreep(stopped, creep);
    CreepToStop *creepStopTrans = new CreepToStop(creep, stop);

    StopObsToStoppedObs *stopStoppedObsTrans = new StopObsToStoppedObs(stopObs, stoppedObs);
    StoppedToUTurn *stoppedObsUTurnTrans = new StoppedToUTurn(stoppedObs, uTurn1);
    StoppedToPassing_LC *stoppedObsPassing1Trans = new StoppedToPassing_LC(stoppedObs, passing1_LC);

    // populate transitions list
    trans.push_back(laneKeepStopTrans);
    trans.push_back(laneKeepLaneChangeTrans);
    trans.push_back(laneChangeLaneKeepTrans);
    trans.push_back(stopStoppedTrans);
    trans.push_back(stoppedLaneKeepTrans);
    trans.push_back(stoppedObsPassing1Trans);
    trans.push_back(uturnUTurnTrans1);
    trans.push_back(uturnUTurnTrans2);
    trans.push_back(uturnUTurnTrans3);
    trans.push_back(uturnUTurnTrans4);
    trans.push_back(uTurnLaneKeepTrans);
    trans.push_back(laneKeepPassing1Trans);
    trans.push_back(passing1Passing2Trans);
    trans.push_back(passing2Passing3Trans);
    trans.push_back(passing3LaneKeepTrans);
    trans.push_back(passingLaneKeepLaneKeepTrans);
    trans.push_back(stoppedCreepTrans);
    trans.push_back(creepStopTrans);
    trans.push_back(stopStoppedObsTrans);
    trans.push_back(stoppedObsUTurnTrans);
    trans.push_back(laneKeepStopObsTrans);
    trans.push_back(laneKeepUTurnTrans);
    trans.push_back(stopToLK);

    return StateGraph(trans);
}

ControlState* ControlStateFactory::getInitialState()
{
    return m_initState;
}

ControlState* ControlStateFactory::getPauseState()
{
    return m_pauseState;
}

int ControlStateFactory::getNextId()
{
    return 0;
}


void ControlStateFactory::print(int type)
{
    cout << "Control State Type: ";
    switch (type) {
    case ControlStateFactory::LANE_KEEPING:
        cout << "LANE KEEPING" << endl;
        break;
    case ControlStateFactory::LANE_CHANGE:
        cout << "LANE_CHANGE" << endl;
        break;
    case ControlStateFactory::STOP:
        cout << "STOP" << endl;
        break;
    case ControlStateFactory::STOPPED:
        cout << "STOPPED" << endl;
        break;
    /*****************************
    Diferentiating between stop,stopped for Intersection or Obstacle
    case ControlStateFactory::STOPOBS:
        cout << "STOPOBS" << endl;
        break;
    case ControlStateFactory::STOPPEDOBS:
        cout << "STOPPEDOBS" << endl;
        break;
    *****************************/
    case ControlStateFactory::UTURN:
        cout << "UTURN" << endl;
        break;
    case ControlStateFactory::CREEP:
        cout << "CREEP" << endl;
        break;
    default:
        cout << "ControlStateFactory: Control state not defined in print function" << endl;
        break;
    };
}

string *ControlStateFactory::printString(int type)
{
    string *cstate_type;
    switch (type) {
    case ControlStateFactory::LANE_KEEPING:
        cstate_type = new string("LANE KEEPING");
        break;
    case ControlStateFactory::LANE_CHANGE:
        cstate_type = new string("LANE CHANGE");
        break;
    case ControlStateFactory::STOP:
        cstate_type = new string("STOP");
        break;
    case ControlStateFactory::STOPPED:
        cstate_type = new string("STOPPED");
        break;
    /*****************************
    Diferentiating between stop,stopped for Intersection or Obstacle
     case ControlStateFactory::STOPOBS:
        cstate_type = new string("STOPOBS");
        break;
    case ControlStateFactory::STOPPEDOBS:
        cstate_type = new string("STOPPEDOBS");
        break;
    *****************************/
    case ControlStateFactory::UTURN:
        cstate_type = new string("UTURN");
        break;
    case ControlStateFactory::CREEP:
        cstate_type = new string("CREEP");
        break;
    default:
        cstate_type = new string("INVALID");
        cerr << "TrafficStateFactory::printString: state type not recognized" << endl;
        break;
    };
    return cstate_type;
}

