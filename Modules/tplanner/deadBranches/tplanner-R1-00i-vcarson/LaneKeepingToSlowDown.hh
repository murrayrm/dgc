#ifndef LANEKEEPINGTOSLOWDOWN_HH_
#define LANEKEEPINGTOSLOWDOWN_HH_

#include "ControlStateTransition.hh"


class LaneKeepingToSlowDown : public ControlStateTransition

{

public: 


LaneKeepingToSlowDown(ControlState* state1, ControlState* state2);

LaneKeepingToSlowDown();

~LaneKeepingToSlowDown();

double meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon, Map* map,VehicleState vehState);

private: 

double m_probability;
double m_desiredDecel;

};
#endif /*LANEKEEPINGTOSLOWDOWN_HH_*/
