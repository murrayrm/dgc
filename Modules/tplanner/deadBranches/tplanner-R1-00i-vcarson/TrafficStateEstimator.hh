#ifndef TRAFFICSTATEESTIMATOR_HH_
#define TRAFFICSTATEESTIMATOR_HH_


#include "map/Map.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"
#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"
#include "interfaces/VehicleState.h"

class TrafficStateEstimator {

public: 

  TrafficStateEstimator();

  ~TrafficStateEstimator();

  /** 
   *  Determine traffic state with respect to the segment that needs to be accomplished given the most recent sensed data. 
   */
  TrafficState* determineTrafficState(TrafficState *currTrafficState, Map* map, VehicleState vehState, PointLabel ptLabel);

	//	void initTrafficState(TrafficState& currTrafficState);
	//  TrafficState getCurrentTrafficState();
	//  TrafficStateFactory::TrafficStateType getCurrentTrafficStateType(); 

private : 

  void buildTrafficStateGraph();
  TrafficState* chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions);

	//  TrafficState* m_currTrafficState; //TODO maybe we don't need this 
	// TrafficStateFactory::TrafficStateType m_currStateType;

  StateGraph m_trafficGraph;
  VehicleState m_currVehState;
  double m_probThresh;

};

#endif /*TRAFFICSTATEESTIMATOR_HH_*/
