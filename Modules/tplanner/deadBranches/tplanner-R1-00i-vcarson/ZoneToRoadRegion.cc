#include "ZoneToRoadRegion.hh"
#include <math.h>


ZoneToRoadRegion::ZoneToRoadRegion(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distZoneExit(1)
{
}

ZoneToRoadRegion::~ZoneToRoadRegion()
{

}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  cout << "in ZoneToRoadRegion::meetTransitionConditions(), SHOULD NOT BE HERE " << endl;
  return 0.0;
} 


double ZoneToRoadRegion::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel pointLabel)
{
  cout << "in ZoneToRoadRegion::meetTransitionConditions() " << endl;
  //this is some distance to some waypoint
  double distance = 0;
  point2 wayPtCoord; 

  point2 frontBumperPos =  AliceStateHelper::getPositionFrontBumper(vehState);
  localMap->getWaypoint(wayPtCoord,pointLabel);
  distance = TrafficUtils::calculateDistance(wayPtCoord,frontBumperPos);
  cout << "distance = " << distance << " m_distZoneExit = " << m_distZoneExit << endl;
  if (distance <= m_distZoneExit) {
    m_trafTransProb = 1;	
  } else
    m_trafTransProb = 0;
  //	cout << "IN ZoneToRoadRegion.cc SETTING PROB TO 1 NO MATTER WHAT" << endl;
  //prob = 1;
  setUncertainty(m_trafTransProb);
  return m_trafTransProb;
} 

