#include "Passing1_LaneKeepingToLaneChange.hh"

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange()
{

}

Passing1_LaneKeepingToLaneChange::~Passing1_LaneKeepingToLaneChange()
{

}

double Passing1_LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in Passing1_LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        double delta_p = 50; // 50 meters passing manouver
        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel exitWayptLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        point2 exit;
        if (map->getWaypoint(exit, exitWayptLabel) != 0) {
	  cout << "aborting pass, something about an exit waypt" << endl;
            m_prob = 0;
	    //            break;
        }
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        double distance = currFrontPos.dist(exit);
        // bool dist_ok = distance > delta_p;
        bool dist_ok = true;

        LaneLabel currLane;
	int laneErr = map->getLane(currLane, currFrontPos);
	if (laneErr == -1)
	  currLane = LaneLabel(seg.entrySegmentID, seg.entryLaneID);

        double delta_o = 30; // the obstacle distance in meters
	//        double distToObs = TrafficUtils::nearestObsInLane(map,vehState);
	double distToObs = map->getObstacleDist(currFrontPos,0);
	bool obstacle_present = (distToObs > 0) && (distToObs < delta_o);

        string lt, rt;

        map->getLeftBoundType(lt, currLane);
        map->getRightBoundType(rt, currLane);

        bool passing_allowed = false;

        LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

	//	LaneLabel lane_left = TrafficUtils::getAdjacentLane(map, currFrontPos,false);
	//	LaneLabel lane_right = TrafficUtils::getAdjacentLane(map,currFrontPos, true);
	LaneLabel lane_left, lane_right;
	map->getNeighborLane(lane_left, currLane, -1);
	map->getNeighborLane(lane_right, currLane, 1);

	bool sameDirLeft = false;
	bool sameDirRight = false;
	vector<LaneLabel> sameDirLanes;
	int dirErr = map->getSameDirLanes(sameDirLanes, currLane);
	for (unsigned int i=0; i<sameDirLanes.size(); i++) {
	  if (sameDirLanes[i] == lane_left)
	    sameDirLeft = true;
	  if (sameDirLanes[i] == lane_right)
	    sameDirRight = true;
	}

        if (lane_left.lane>0 && (lt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
          laneChange->setLaneChangeID(lane_left.lane);
	  laneChange->setReverse(sameDirLeft?0:1);
          passing_allowed = true;
	  cout << "Can change lanes left to lane " << lane_left << endl;
	  cout << "Same direction left: " << sameDirLeft << endl;
	}

        if (lane_right.lane>0 && (rt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
          laneChange->setLaneChangeID(lane_right.lane);
	  laneChange->setReverse(sameDirRight?0:1);
          passing_allowed = true;
	  cout << "Can change lanes right to lane " << lane_right << endl;
	  cout << "Same direction right: " << sameDirRight << endl;
	}

        if (m_verbose || m_debug)
	  cout << "obs_pres:" << obstacle_present << " dist_ok:" << dist_ok << " allowed:" << passing_allowed << endl;

        if (obstacle_present && dist_ok && passing_allowed) {
            m_prob = 1;
	    if (m_verbose || m_debug)
	      cout << "Transitioning to Passing1..." << endl;
        }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing1_LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
