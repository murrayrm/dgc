#ifndef STOP_HH_
#define STOP_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>
#include "CheckPass.hh"
#include "CorridorUtils.hh"

class Stop:public ControlState {

  public:

    Stop(int stateId, ControlStateFactory::ControlStateType type);
    ~Stop();
    int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);

  ControlState* newCopy() { return (ControlState*) new Stop(*this); }
  private:
    double m_desDecc;
    
};

#endif                          /*STOP_HH_ */
