#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "TrafficManager.hh"
#include "CorridorGenerator.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "iostream"
#include <sys/time.h>
#include <sys/stat.h>
      
using namespace std;             
     
int main(int argc, char **argv)              
{

  gengetopt_args_info cmdline;


  int debugLevel = 0, verboseLevel = 0, sn_key = 0;
  bool recvLocalMap = false; 
  bool recvSegmentGoals = false;
  bool enableLogging = false;
  string RNDFfilename;
  /* Figure out where logged data go */
  string logFileName;
  FILE* logFile;

  sn_key = skynet_findkey(argc, argv);
  debugLevel = cmdline.debug_arg;
  verboseLevel = cmdline.verbose_arg; 
 
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
    
  if (!cmdline.nomap_given)
  {
    recvLocalMap = true;
  } 
      
  if (!cmdline.nogoals_given)
  {
    recvSegmentGoals = true;
  } 

  // Initialize the map with rndf if given
  if (cmdline.rndf_given){
    RNDFfilename = cmdline.rndf_arg;
    cout << "RNDF Filename in = "  << RNDFfilename << endl;
    //Load the RNDF in the constructor of TrafficManager 
  }     

  if(true == (enableLogging = cmdline.enable_logging_given))
  {
    string tmpMDFname;
  
    ostringstream oss;
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    oss << timestr << "-tplanner-" << RNDFfilename << ".log";
    logFileName = oss.str();
    string suffix = "";
    
    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    logFile = fopen(logFileName.c_str(), "w");
    if (logFile == NULL)
    {
      cerr << "Cannot open log file: " << logFile << endl;
      exit(1);
    }

  }

  if (cmdline.disable_console_flag){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << sn_key << endl;
    cout  << "debug level = " << debugLevel << endl;
    cout << "verbose level = " << verboseLevel << endl;
  }

  //The idea is if there is no map given, then we won't have an RNDF file name 
  //  TrafficManager* traffManager = new TrafficManager(sn_key, !cmdline.nowait_given, 
  //   noSparrow, enableLogging,
  //   logFile, debugLevel, verboseLevel);

  // CorridorGenerator* corrGen = new CorridorGenerator(sn_key, !cmdline.nowait_given, 
  //   noSparrow, RNDFFileName, MDFFileName, !cmdline.nomap_given, cmdline.enable_logging_given,
  //    logFile, debugLevel, verboseLevel);

  //traffManager->Start();

  //corrGen->Start();

  //while (!traffManager->IsStopped())
  // {
  //   sleep(1);
  //  }
  
  if(cmdline.enable_logging_given)
    {
      pclose(logFile);
    }
  
  // DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getSegGoalsThread); 
      
  
  // I have had issues where some of the threads are not up yet by the time the planning loop starts,
  // and I do not know why that is, but I have added a sleep here to make sure that all the threads 
  // are running. Want something more robust here?!
  //#warning "make threads more independent during startup"
  sleep(1);

  return 0;
}

