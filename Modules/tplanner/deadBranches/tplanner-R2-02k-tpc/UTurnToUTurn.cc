#include "UTurnToUTurn.hh"
#include "UTurn.hh"

UTurnToUTurn::UTurnToUTurn(ControlState * state1, ControlState * state2, int stage)
: ControlStateTransition(state1, state2)
{
    m_stage = stage;
}

UTurnToUTurn::UTurnToUTurn()
{

}

UTurnToUTurn::~UTurnToUTurn()
{

}

double UTurnToUTurn::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * localMap, VehicleState vehState)
{

    cout << "in UTurnToUTurn::meetTransitionConditions" << endl;
    PointLabel ptLabel = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
    point2 exitWayPt;
    localMap->getWaypoint(exitWayPt, ptLabel);
    double angle;
    localMap->getHeading(angle, ptLabel);
    double orient;
    double laneDir;
    point2 FC_pos;
        
    m_probability = 0;
    
    switch (m_stage) {
    
    case 1:
      {
        FC_pos = UTurn::m_ptStage1;

        localMap->getHeading(laneDir, FC_pos);
        orient = laneDir - M_PI;
        double dotProd = (-FC_pos.x + currRearPos.x) * cos(orient) + (-FC_pos.y + currRearPos.y) * sin(orient);
        double compDist = 1.5;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
            // When we get within 1 m of the hyperplane defined by the exit pt, goal complete
            if ((m_verbose) || (m_debug)) {
                cout << "UTURN: SWITCHING TO STAGE 2 or 4!!!" << endl;
            }
            m_probability = 1;
        }
      }
        break;

       
    case 2:
      {
        FC_pos = UTurn::m_ptStage2;
        localMap->getHeading(laneDir, FC_pos);
        orient = laneDir - M_PI/2;
        double dotProd = (-FC_pos.x + currFrontPos.x) * cos(orient) + (-FC_pos.y + currFrontPos.y) * sin(orient);
        double compDist = 1;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && ((m_stage == 4) || (currVel < stoppedVel))) {
          // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
          if ((m_verbose) || (m_debug)) {
            cout << "UTURN: SWITCHING TO STAGE 3 or 5!!!" << endl;
          }
          m_probability = 1;
        }
      }
      break;
      
    case 3:
      {
        FC_pos = UTurn::m_ptStage3;
        localMap->getHeading(laneDir, FC_pos);
        orient = laneDir;
        double dotProd = (-FC_pos.x + currRearPos.x) * cos(orient) + (-FC_pos.y + currRearPos.y) * sin(orient);
        double compDist = 1.5;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          // When we get within 1 m of the hyperplane defined by the exit pt, goal complete
          if ((m_verbose) || (m_debug)) {
            cout << "UTURN: SWITCHING TO STAGE 2 or 4!!!" << endl;
          }
          m_probability = 1;
        }
        
      }
      break;

    case 4:
      {
        FC_pos = UTurn::m_ptStage4;
        localMap->getHeading(laneDir, FC_pos);
        orient = laneDir;
        double dotProd = (-FC_pos.x + currFrontPos.x) * cos(orient) + (-FC_pos.y + currFrontPos.y) * sin(orient);
        double compDist = 1;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && ((m_stage == 4) || (currVel < stoppedVel))) {
          // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
          if ((m_verbose) || (m_debug)) {
            cout << "UTURN: SWITCHING TO STAGE 3 or 5!!!" << endl;
          }
          m_probability = 1;
        }
      }
      break;
      
    default:
      break;
      
    }
    
    cout << "UTurn (" << m_stage << ") = " << m_probability << endl;
    setUncertainty(m_probability);
    return m_probability;
}
