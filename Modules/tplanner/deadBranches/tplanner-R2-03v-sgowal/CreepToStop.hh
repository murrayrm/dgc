#ifndef CREEPTOSTOP_HH_
#define CREEPTOSTOP_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class CreepToStop:public ControlStateTransition {

  public:

    CreepToStop(ControlState * state1, ControlState * state2);
    CreepToStop();
    ~CreepToStop();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

};

#endif                          /*STOPPEDTOCREEP_HH_ */
