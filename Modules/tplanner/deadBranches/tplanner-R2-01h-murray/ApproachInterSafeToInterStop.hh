#ifndef APPROACHINTERSAFETOINTERSTOP_HH_
#define APPROACHINTERSAFETOINTERSTOP_HH_


#include "TrafficStateTransition.hh"
#include "TrafficUtils.hh"


class ApproachInterSafeToInterStop : public TrafficStateTransition
{

public: 

ApproachInterSafeToInterStop(TrafficState* state1, TrafficState* state2);
~ApproachInterSafeToInterStop();

double meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState);

	double meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel ptLabel);

private: 

double m_distToStop; //in meters

};
#endif /*APPROACHINTERSAFETOINTERSTOP_HH_*/

