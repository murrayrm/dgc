#ifndef TRAFFICSTATEFACTORY_HH_
#define TRAFFICSTATEFACTORY_HH_

#include <string>
#include "state/StateGraph.hh"


class TrafficStateFactory {


public: 

  TrafficStateFactory();
  TrafficStateFactory(bool debug, bool verbose, bool log);
 ~TrafficStateFactory();
  static typedef enum {ROAD_REGION, ZONE_REGION, APPROACH_INTER_SAFETY, INTERSECTION_STOP } TrafficStateType; 
  static StateGraph createTrafficStates();
  static int getNextId();
  //void print(TrafficStateFactory::TrafficStateType type);

  void print(int type);
  string* printString(int type);
private:
  int m_stateId;
  bool m_debug, m_verbose, m_log;
  
};

#endif /*TRAFFICSTATEFACTORY_HH_*/
