#ifndef LANEKEEPINGTOSTOP_HH_
#define LANEKEEPINGTOSTOP_HH_

#include "ControlStateTransition.hh"

class LaneKeepingToStop: public ControlStateTransition {

  public:


    LaneKeepingToStop(ControlState * state1, ControlState * state2);
    LaneKeepingToStop();
    ~LaneKeepingToStop();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:

    double m_probability;
    double m_desiredDecel;
    
};

#endif                          /*LANEKEEPINGTOSTOP_HH_ */
