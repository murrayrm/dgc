#include "LaneKeepingToStopObs.hh"

LaneKeepingToStopObs::LaneKeepingToStopObs(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
  , m_minDistToObs(25)
{

}

LaneKeepingToStopObs::LaneKeepingToStopObs()
{

}

LaneKeepingToStopObs::~LaneKeepingToStopObs()
{

}

double LaneKeepingToStopObs::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in LaneKeepingToStopObs::meetTransitionConditions - this is the one with obstacles" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      SegGoals seg = planHorizon.getSegGoal(0);
      PointLabel exitPtLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
      point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
      point2 exitPt;
      map->getWayPoint(exitPt, exitPtLabel);
      
      /* deal with obstacle */
      double delta_o = -pow(AliceStateHelper::getVelocityMag(vehState), 2) / (2 * m_desiredDecel);
      LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
      double dist2Obs = TrafficUtils::getNearestObsDist(map, vehState, desiredLane);
      bool obstacle_present = (((dist2Obs < delta_o) && (dist2Obs>0)) || ((dist2Obs < m_minDistToObs) && (dist2Obs>0)));
      
      if (obstacle_present) {
        cout << "--------------THINK THERE IS OBSTACLE PRESENT" << endl;

        MapElement obstacle;
        TrafficUtils::getNearestObsInLane(obstacle, map, vehState, desiredLane);
        bool is_static = TrafficUtils::isObsStatic(obstacle);
        
        // TODO: want to make sure that we transition to stopObs state
        string lt, rt;
        LaneLabel label(seg.entrySegmentID, seg.entryLaneID);
        map->getLeftBoundType(lt, label);
        map->getRightBoundType(rt, label);
        bool passing_allowed = seg.illegalPassingAllowed || lt.compare("broken_white")==0 || rt.compare("broken_white")==0;

        // Check for blockage
        bool lane_blocked = TrafficUtils::isLaneBlocked(map, vehState, exitPt, desiredLane);
        
        if (!passing_allowed && is_static && lane_blocked) {
          m_prob = 1;
        }
      }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
      
      m_prob = 0;
      break;
      
    default:
      
      m_prob = 0;
      cerr << "LaneKeepingToStopObs.cc with obstacles: Undefined Traffic state" << endl;
      
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
