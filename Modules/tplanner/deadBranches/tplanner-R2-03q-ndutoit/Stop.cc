#include "Stop.hh"

Stop::Stop(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
, m_desDecc(-0.5)
{

}

Stop::~Stop()
{

}

int Stop::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    if ((m_verbose) || (m_debug)) {
        cout << "In Stop::determineCorridor " << endl;
    }
    
    int error = 0;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
        
    switch (traffState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        if (!CheckPass::init)
          CheckPass::resetIntersection();
        CheckPass::checkIntersection(vehState,localMap,this);

        bool isReverse = false;
        CorridorUtils::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
 
        // specify the ocpParams final conditions - lower bounds
        if (traffState->getType() == TrafficStateFactory::ROAD_REGION) {
          error += CorridorUtils::setFinalCondLK(corr, currFrontPos, localMap, currSegment, isReverse);
        } else if (traffState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY) {
          error += CorridorUtils::setFinalCondStop(corr, localMap, currSegment);
        }

    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
      SegGoals nextSegment = planHoriz.getSegGoal(1);
      bool isReverse = false;
      error += CorridorUtils::makeCorridorIntersection(corr, currFrontPos, localMap, currSegment);

      // If we are still coming up to the stop line, I want the FC to 
      // be the stopline, otherwise the FC should be our current location
      double heading;
      PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
      localMap->getHeading(heading, ptLabelIn);
      point2 tmpPt;
      localMap->getNextStopline(tmpPt, ptLabelIn);
      double dotProd = (-tmpPt.x+currFrontPos.x)*cos(heading) + (-tmpPt.y+currFrontPos.y)*sin(heading);
      if (dotProd<0) { 
        // This is the case where we are still approaching the intersection
        error += CorridorUtils::setFinalCondStopped(corr, vehState);

      } else {
        // This is the case where we want to stop inside the intersection
        error += CorridorUtils::setFinalCondStopped(corr, vehState);
      }

    }
    break;
        
    default:
      {    
        cerr << "Stop.cc: Undefined Traffic state" << endl;
        error += 1;
      }
    }

    return 0;
}
