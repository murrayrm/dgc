#include "LaneKeepingToLaneChange.hh"

LaneKeepingToLaneChange::LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

LaneKeepingToLaneChange::LaneKeepingToLaneChange()
{

}

LaneKeepingToLaneChange::~LaneKeepingToLaneChange()
{

}

double LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      if (planHorizon.getSegGoal(0).exitLaneID != map->getLaneID(AliceStateHelper::getPositionFrontBumper(vehState))) {
        LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0) {      
          cerr << "Error retrieving LaneChange destination" << endl;
	  break;
	}

	laneChange->setLaneChangeID(planHorizon.getSegGoal(0).exitLaneID);
        m_prob = 1;
      }
      m_prob=0;
    }
    
        break;

    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
    	
	    m_prob = 0;
	    break;

    default:
    
        m_prob = 0;
        cerr << "LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
