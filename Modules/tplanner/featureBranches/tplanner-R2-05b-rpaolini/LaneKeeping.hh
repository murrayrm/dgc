#ifndef LANEKEEPING_HH_
#define LANEKEEPING_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"
#include "CorridorUtils.hh"

class LaneKeeping:public ControlState {

  public:

    static typedef enum { NOMINAL, PASSING } LaneKeepingType;

    LaneKeeping(int stateId, ControlStateFactory::ControlStateType type, LaneKeepingType);
    ~LaneKeeping();
    bool isPassing();
    int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
    void setReverse(bool reverse) { isReverse = reverse; }
    bool getReverse() { return isReverse; }

  private:
    bool isReverse;
    LaneKeepingType m_lanekeeping_type;
    ControlState* newCopy() {
      return  (ControlState*) new LaneKeeping(*this);
    }
};

#endif                          /*LANEKEEPING_HH_ */
