#include "Passing1_LaneKeepingToLaneChange.hh"

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange()
{

}

Passing1_LaneKeepingToLaneChange::~Passing1_LaneKeepingToLaneChange()
{

}

double Passing1_LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in Passing1_LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        double delta_p = 50; // 50 meters passing manouver
        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel exitWayptLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        point2 exit;
        if (map->getWaypoint(exit, exitWayptLabel) != 0) {
            m_prob = 0;
            break;
        }
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        double distance = currFrontPos.dist(exit);
        bool dist_ok = distance > delta_p;

        double delta_o = 30; // the obstacle distance in meters
        double distToObs = map->getObstacleDist(AliceStateHelper::getPositionFrontBumper(vehState), 0);
	bool obstacle_present = (distToObs > 0) && (distToObs < delta_o);

        string lt, rt;
        LaneLabel label(seg.entrySegmentID, seg.entryLaneID);
        map->getLeftBoundType(lt, label);
        map->getRightBoundType(rt, label);
        bool passing_allowed = seg.illegalPassingAllowed;

        LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

	int lane_left = TrafficUtils::getAdjacentLane(map, currFrontPos,false);
	int lane_right = TrafficUtils::getAdjacentLane(map,currFrontPos, true);

        if (lane_left>0 && (lt.compare("broken_white") == 0 || passing_allowed)) {
          laneChange->setLaneChangeID(lane_left);
          passing_allowed = true;
	  cout << "Can change lanes left..." << endl;
	}

        if (lane_right>0 && (rt.compare("broken_white") == 0 || passing_allowed)) {
          laneChange->setLaneChangeID(lane_right);
          passing_allowed = true;
	  cout << "Can change lanes left..." << endl;
	}

        if (m_verbose || m_debug)
	  cout << "obs_pres:" << obstacle_present << " dist_ok:" << dist_ok << " allowed:" << passing_allowed << endl;

        if (obstacle_present && dist_ok && passing_allowed) {
            m_prob = 1;
	    if (m_verbose || m_debug)
	      cout << "Transitioning to Passing1..." << endl;
        }
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing1_LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
