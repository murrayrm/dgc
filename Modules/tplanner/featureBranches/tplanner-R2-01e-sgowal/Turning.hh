#ifndef TURNING_HH_
#define TURNING_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"

class Turning : public ControlState {

public:

	Turning(int stateId, ControlStateFactory::ControlStateType type);
	~Turning();
	virtual int determineCorridor(Corridor &corr,VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* map);

};
#endif /*LANEKEEPING_HH_*/
