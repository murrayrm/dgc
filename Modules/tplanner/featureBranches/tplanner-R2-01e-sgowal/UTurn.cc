#include "UTurn.hh"

/**
 * UTurn is defined by 5 different stages, this new version SHOULD use 5 different instances of the same class
 * to navigate through the 5 stages. UTurnToUTurn transition will take care of the rest.
 */
UTurn::UTurn(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId,type)
{
	m_stage = 1; 
	m_firstRun = true;
}

UTurn::~UTurn()
{
}

int UTurn::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
	cout << "In UTurn.cc::determine corridor" << endl;
	
	SegGoals currSegment = planHoriz.getSegGoal(0);

	// TODO: when I receive a uturn, I do not (at the moment) get any entry info. Thus, I either have to remember this info, or Nok has to send it. 
	cout << " ==============================" << endl;
	cout << "HARD CODING SEGMENT ENTRY INFO FOR NOW. THIS NEEDS TO CHANGE!!!!!" << endl;
	currSegment.entrySegmentID = 1;
	currSegment.entryLaneID = 1;
	currSegment.entryWaypointID = 1;
	cout << " ==============================" << endl;
	PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

	double velIn, velOut, acc;
	vector<double> velProfile;
	double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
	point2 IC_pos;
	double IC_velMin, IC_velMax;
	point2 FC_pos, tmpPt;
	point2arr leftBound, rightBound;
	point2arr leftBound_tmp, rightBound_tmp;
	point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
	point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
	double currVel = AliceStateHelper::getVelocityMag(vehState);
	point2 aliceInitPos = getInitialPosition();
	int index;
	int getBoundsErr, FC_posErr;
	double range;

	/* Step 1: Get bounds (from obstacle and back to 30 m)
	 * from the control state transition, we have our position when we transition into uturn. All other maneuvers relative to this?
	 */
	LaneLabel laneLabel_curr(currSegment.entrySegmentID, currSegment.entryLaneID);
	// TODO: for now just deal with two lanes (until sam has this wrapped up)
	int otherLane;
	if (currSegment.entryLaneID == 1) {
		otherLane =  currSegment.entryLaneID + 1;
	} else {
		otherLane =  currSegment.entryLaneID - 1;
	}
	LaneLabel laneLabel_other(currSegment.entrySegmentID, otherLane);
	
	/* What is getObstaclePoint ??? */
	if (m_firstRun) {
		m_obsPos = localMap->getObstaclePoint(currFrontPos, 0);
		m_rearBound = localMap->getObstaclePoint(currFrontPos, 30);
		localMap->getLaneCenterPoint(m_ptStage1, laneLabel_curr, aliceInitPos, -18.0);
		localMap->getLaneRightPoint(m_ptStage2, laneLabel_other, aliceInitPos, 7.0);
		localMap->getLaneCenterPoint(m_ptStage3, laneLabel_curr, aliceInitPos, 5.0);
		localMap->getLaneCenterPoint(m_ptStage4, laneLabel_other, aliceInitPos, 20.0);
		m_firstRun = false;
	}
  
	if (5 != m_stage) {
		localMap->getLaneRightPoint(tmpPt, laneLabel_other, m_rearBound, 0);
		leftBound.push_back(tmpPt);
		localMap->getLaneRightPoint(tmpPt, laneLabel_curr, m_rearBound, 0);
		rightBound.push_back(tmpPt);

		localMap->getLaneRightPoint(tmpPt, laneLabel_other, m_obsPos, 0);
		leftBound.push_back(tmpPt);
		localMap->getLaneRightPoint(tmpPt, laneLabel_curr, m_obsPos, 0);
		rightBound.push_back(tmpPt);
	}

	double heading, laneDir, orient;
	double compDist;
	double dotProd;
	double stoppedVel = 0.1;
	PointLabel nextPtLabel, ptLabel;

	switch(m_stage) {
	
	// stage 1: reverse until we are at pt in current lane, 25 m from obs
	case 1:
	
		// note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
		cout << "stage 1: reverse" << endl;
		corr.setOCPmode(1);

		// get all the relevant points for the u-turn
		// specify the ocpParams final conditions - lower bounds
		// final pos should be 25 m from the obstacle
		FC_pos = m_ptStage1;
		FC_velMin =  0;
		localMap->getHeading(laneDir, FC_pos);
		cout << "laneDir = " << laneDir << endl;
		heading = laneDir;
		cout << "heading = " << heading << endl;
		orient = laneDir - M_PI;
		cout << "orient = " << orient << endl;
		
		// check if we are done with this reverse and we are stopped before switching to stage 2
		dotProd = (-FC_pos.x+currRearPos.x)*cos(orient) + (-FC_pos.y+currRearPos.y)*sin(orient);
		cout << "in uturn: dot product  = " << dotProd << endl;
		// specify some vel profile
		compDist = 1.5;
		if (dotProd>-compDist) {
			velIn = 0;
			velOut = 0;
			acc = 0;
		} else {
			velIn = 1;
			velOut = 1;
			acc = 0;
		}
		FC_headingMin = fmod(heading,2*M_PI); 
		FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
		FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
		// specify the ocpParams final conditions - upper bounds
		FC_velMax =  velOut;
		FC_headingMax = fmod(heading, 2*M_PI);
		FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
		FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
		
		// Initial cond's
		IC_pos = currRearPos;

		if((dotProd>-compDist)&&(currVel<stoppedVel)) {
			// when we get within 1 m of the hyperplane defined by the exit pt, goal complete
			cout << "UTURN: SWITCHING TO STAGE 2!!!" << endl;
			m_stage = 2;
		}
		break;

	// stage 2: drive fwd and turn 90 degrees, at dist 8m from obstacle
	case 2:
	
		// note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
		cout << "stage 2: fwd" << endl;
		corr.setOCPmode(0);

		// specify the ocpParams final conditions - lower bounds
		// final pos should be  17m from the obstacle, and facing out of the lane
		FC_pos = m_ptStage2;
		FC_velMin =  0;
		// get direction of lane
		localMap->getLaneCenterPoint(tmpPt, laneLabel_other, FC_pos, 0.0);
		localMap->getHeading(laneDir, tmpPt); // lane dir is the orientation of the lane
		cout << "laneDir = " << laneDir << endl;
		heading = laneDir + M_PI/2; // heading is the heading for the final cond
		cout << "heading = " << heading << endl;
		orient = laneDir + M_PI/2; // orientation is the orientation of the unit vector for the complete test
		cout << "orient = " << orient << endl;
		// check if we are done with this reverse and we are stopped before switching to stage 2
		
		dotProd = (-FC_pos.x+currFrontPos.x)*cos(orient) + (-FC_pos.y+currFrontPos.y)*sin(orient);
		cout << "in uturn: dot product  = " << dotProd << endl;
		// specify some vel profile
		compDist = 1;
		if (dotProd>-compDist) {
			velIn = 0;
			velOut = 0;
			acc = 0;
		} else {
			velIn = 1;
			velOut = 1;
			acc = 0;
		}

		FC_headingMin = fmod(heading, 2*M_PI);
		FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
		FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
		// specify the ocpParams final conditions - upper bounds
		FC_velMax =  velOut;
		FC_headingMax = fmod(heading, 2*M_PI);
		FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
		FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

		// Initial cond's
		IC_pos = currFrontPos;

		if((dotProd>-compDist)&&(currVel<stoppedVel)) {
			// when we get within 1 m of the hyperplane defined by the exit pt, goal complete
			cout << "UTURN: SWITCHING TO STAGE 3!!!" << endl;
			m_stage = 3;
		}

		break;

	// stage 3: Reverse until 8 m from obstacle
	case 3:
		// note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
		cout << "stage 3: reverse" << endl;
		corr.setOCPmode(1);
		cout << "Not yet implemented" << endl;

		// get all the relevant points for the u-turn
		// specify the ocpParams final conditions - lower bounds
		// final pos should be 25 m from the obstacle
		FC_pos = m_ptStage3;
		FC_velMin =  0;
		localMap->getHeading(laneDir, FC_pos);
		cout << "laneDir = " << laneDir << endl;
		heading = laneDir - M_PI;
		cout << "heading = " << heading << endl;
		orient = laneDir;
		cout << "orient = " << orient << endl;

		dotProd = (-FC_pos.x+currRearPos.x)*cos(orient) + (-FC_pos.y+currRearPos.y)*sin(orient);
		cout << "in uturn: dot product  = " << dotProd << endl;
		// specify some vel profile
		compDist = 1.5;
		if (dotProd>-compDist) {
			velIn = 0;
			velOut = 0;
			acc = 0;
		} else {
			velIn = 1;
			velOut = 1;
			acc = 0;
		}
		FC_headingMin = fmod(heading,2*M_PI); 
		FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
		FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
		// specify the ocpParams final conditions - upper bounds
		FC_velMax =  velOut;
		FC_headingMax = fmod(heading, 2*M_PI);
		FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
		FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

		// Initial cond's
		IC_pos = currRearPos;

		if((dotProd>-compDist)&&(currVel<stoppedVel)) {
			// when we get within 1 m of the hyperplane defined by the exit pt, goal complete
			cout << "UTURN: SWITCHING TO STAGE 4!!!" << endl;
			m_stage = 4;
		}

		break;

	// stage 4: drive fwd to exit pt in other lane
	case 4:
		// note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
		cout << "stage 4: fwd" << endl;
		corr.setOCPmode(0);

		// specify the ocpParams final conditions - lower bounds
		// final pos should be 30m from the obstacle in the other lane
		// TODO: fix this so that we exit in the middle of the other lane, but for now just exit in the middle of the lanes
		FC_pos = m_ptStage4;
		FC_velMin =  0;
		// offset this pt a little away from the lane so that it returns something (a map bug)
		localMap->getHeading(laneDir, FC_pos); // lane dir is the orientation of the lane
		cout << "laneDir = " << laneDir << endl;
		heading = laneDir; // heading is the heading for the final cond
		cout << "heading = " << heading << endl;
		orient = laneDir; // orientation is the orientation of the unit vector for the complete test
		cout << "orient = " << orient << endl;
		// check if we are done with this reverse and we are stopped before switching to stage 2

		dotProd = (-FC_pos.x+currFrontPos.x)*cos(orient) + (-FC_pos.y+currFrontPos.y)*sin(orient);
		cout << "in uturn: dot product  = " << dotProd << endl;
		// specify some vel profile
		compDist = 1;
		if (dotProd>-compDist) {
			velIn = 0;
			velOut = 0;
			acc = 0;
		} else {
			velIn = 1;
			velOut = 1;
			acc = 0;
		}

		FC_headingMin = fmod(heading, 2*M_PI);
		FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
		FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
		// specify the ocpParams final conditions - upper bounds
		FC_velMax =  velOut;
		FC_headingMax = fmod(heading, 2*M_PI);
		FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
		FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

		// Initial cond's
		IC_pos = currFrontPos;

		if(dotProd>-compDist) {
			// when we get within 1 m of the hyperplane defined by the exit pt, goal complete
			cout << "UTURN: SWITCHING TO STAGE 5!!!" << endl;
			m_stage = 5;
		}

		break;
		
	// stage 4: drive fwd to exit pt in other lane
	case 5:
		// This should just be normal lane keeping from here on out
		cout << "UTurn stage 5: drive fwd to the exit waypt" << endl; 
		corr.setOCPmode(0);

		range = 50;
		getBoundsErr = localMap->getBounds(leftBound_tmp, rightBound_tmp, laneLabel_other, currFrontPos, range);
		if (getBoundsErr!=0){
			cerr << "Uturn stage 5: boundary read from map error" << endl;
			return (getBoundsErr);
		}

		// take some subset of these boundary points as the corridor
		// find the closest pt on the boundary
		TrafficUtils::insertProjPtInBoundary(leftBound_tmp, currFrontPos);
		index = TrafficUtils::getClosestPtIndex(leftBound_tmp, currFrontPos);
		for (int ii=index; ii<(int)leftBound_tmp.size(); ii++) {
			leftBound.push_back(leftBound_tmp[ii]);
		}
		
		TrafficUtils::insertProjPtInBoundary(rightBound_tmp, currFrontPos);
		index = TrafficUtils::getClosestPtIndex(rightBound_tmp, currFrontPos);
		for (int ii=index; ii<(int)rightBound_tmp.size(); ii++) {
			rightBound.push_back(rightBound_tmp[ii]);
		}
		// Specify the velocity profile
		velOut = 1;
		velIn = 1;
		acc = 0;

		// specify the ocpParams final conditions - lower bounds
		// want to define this position based on the corridor?
		FC_posErr = localMap->getWaypoint(FC_pos, exitPtLabel);
		if (FC_posErr!=0){
			cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
			return (FC_posErr);
		}
		FC_velMin =  0;
		FC_headingMin = -M_PI; // unbounded
		FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
		FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
		// specify the ocpParams final conditions - upper bounds
		FC_velMax =  velOut;
		FC_headingMax = M_PI; // unbounded
		FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
		FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

		IC_pos = currFrontPos;
		// the parameters gets set in the transition from uturn to lane keeping
	}

	corr.addPolyline(leftBound);
	corr.addPolyline(rightBound);

	// Set the velocity profile - need to think this through some
	velProfile.push_back(velIn); // entry pt
	velProfile.push_back(velOut); // exit pt
	corr.setVelProfile(velProfile);
	corr.setDesiredAcc(acc);

	// set the final conditions in the ocpspecs
	corr.setOCPfinalCondLB(EASTING_IDX_C, FC_pos.y);
	corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_pos.x);
	corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
	corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
	corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
	corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
	// specify the ocpParams final conditions - upper bounds
	corr.setOCPfinalCondUB(EASTING_IDX_C, FC_pos.y);
	corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_pos.x);
	corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
	corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
	corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
	corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

	corr.setOCPinitialCondLB(EASTING_IDX_C, IC_pos.y);
	corr.setOCPinitialCondLB(NORTHING_IDX_C, IC_pos.x);
	corr.setOCPinitialCondUB(EASTING_IDX_C, IC_pos.y);
	corr.setOCPinitialCondUB(NORTHING_IDX_C, IC_pos.x);

	return 0;
}


void UTurn::resetParameters() {
	cout << "reset parameters for next uturn" << endl;
	m_stage = 1;
	m_firstRun = true;
}
