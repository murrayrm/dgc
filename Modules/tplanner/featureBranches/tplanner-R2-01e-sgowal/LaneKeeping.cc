#include "LaneKeeping.hh"


LaneKeeping::LaneKeeping(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
{
}

LaneKeeping::~LaneKeeping()
{
}

int LaneKeeping::determineCorridor(Corridor &corr,VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
	cout << "In LaneKeeping::determineCorridor "<< endl;
	
	point2arr leftBound, rightBound;
	SegGoals currSegment = planHoriz.getSegGoal(0);
	double velIn, velOut, acc;
	double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
	point2 FC_finalPos;
	point2arr leftBound1, rightBound1;
	point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

	/* We can only be in a Road Region */
    cout << "LaneKeeping::determineCorridor inside ROAD_REGION" << endl;
	
    /* We want to plan for current segment only */
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);

    double range = 50;
    int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
    if (getBoundsErr!=0){
		cerr << "LaneKeeping.cc: boundary read from map error" << endl;
		return (getBoundsErr);
    }     
    
    /* We have to take some subset of these boundary points as the corridor
     * and find the closest pt on the boundary */
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
	
    for (int ii=index; ii<(int)leftBound1.size(); ii++) {
		leftBound.push_back(leftBound1[ii]);
	}
	
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++) {
        rightBound.push_back(rightBound1[ii]);
	}
    
    /* Specify the velocity profile */
    velIn = currSegment.maxSpeedLimit;
    velOut = currSegment.maxSpeedLimit;
    acc = 0;

    /* Specify the ocpParams final conditions - lower bounds */
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0) {
        cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
        return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    
    /* Specify the ocpParams final conditions - upper bounds */
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

    /* If we know that a stop line is coming, we will transition to a Stopping state
     * So there are no needs to specify a different corridor if we are approaching an
     * intersection */

    /* Assign lines to corridor variables */
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    /* Set the velocity profile - need to think this through some */
    vector<double> velProfile;
    velProfile.push_back(velIn); // entry pt
    velProfile.push_back(velOut); // exit pt
    corr.setVelProfile(velProfile);
    corr.setDesiredAcc(acc);

    /* set the final conditions in the ocpspecs */
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);

    /* specify the ocpParams final conditions - upper bounds */
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    return 0;
}
