#include "ApproachInterSafeToInterStop.hh"
#include <math.h>


ApproachInterSafeToInterStop::ApproachInterSafeToInterStop(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2), m_distToStop(1)
{
}

ApproachInterSafeToInterStop::~ApproachInterSafeToInterStop()
{
}

double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
  return 0.0;
} 


double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel ptLabel)
{
  cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() " << endl;

  double distance = 50;
  int stopLineErr = -1;
  point2 stopLinePos;

  // this is what I need to use
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos  = AliceStateHelper::getPositionRearAxle(vehState);
  cout << "curr pos = " << currFrontPos << endl;

  // use getStopline() call that gives next stopline in my lane based on currPos
  stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);
  cout << "ptLabel = " << ptLabel << endl;
  cout << "stopLinePos = " << stopLinePos << endl;

  double angle;
  localMap->getHeading(angle, ptLabel);
  cout << "heading = " << angle << endl;

  double dotProd = (-stopLinePos.x+currFrontPos.x)*cos(angle) + (-stopLinePos.y+currFrontPos.y)*sin(angle);
  cout << "in AIS2IS: dot product  = " << dotProd << endl;

  if (dotProd > -m_distToStop)
    m_trafTransProb = 1;	
  else 
    m_trafTransProb = 0;
  setUncertainty(m_trafTransProb);

  return m_trafTransProb;
} 
