#include "RoadToApproachInterSafety.hh"
#include <math.h>


RoadToApproachInterSafety::RoadToApproachInterSafety(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distSafeStop(30)
{
}

RoadToApproachInterSafety::~RoadToApproachInterSafety()
{

}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
	cout << "in RoadToApproachInterSafety::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
	return 0.0;
} 

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState,PointLabel ptLabel)
{
	cout << "in RoadToApproachInterSafety::meetTransitionConditions() " << endl;
	double prob = 0;
	double distance = 0;
	int stopLineErr = -1;
	point2 stopLinePoint;
	//	PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());

	point2 currPos =  AliceStateHelper::getPositionFrontBumper(vehState);

	stopLineErr = localMap->getStopline(stopLinePoint,ptLabel);
	distance = stopLinePoint.dist(currPos);
	if (distance <= m_distSafeStop) {
	  m_trafTransProb = 1;	
	  prob = m_trafTransProb;
	}
	setUncertainty(prob);
	return prob;
} 
