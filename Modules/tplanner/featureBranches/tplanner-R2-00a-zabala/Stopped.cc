#include "Stopped.hh"


Stopped::Stopped(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
{
}

Stopped::~Stopped()
{
}

int Stopped::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
  //SAM  Corridor corridor = Corridor();

  point2arr leftBound, rightBound;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;

  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2arr leftBound1, rightBound1;
  point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);

  if(TrafficStateFactory::ZONE_REGION == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to do this properly in the future
    point2 exitWaypt, currPos;
    PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
    if (wayptErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (wayptErr);
    }     
    // get our current position
    currPos = AliceStateHelper::getPositionFrontBumper(vehState);
    
    // create a corridor of LaneWidth from current position to entry point
    double corrHalfWidth = 5;
    double theta = atan2(exitWaypt.y-currPos.y, exitWaypt.x-currPos.x);
    point2 temppt;
    // Left Boundary
    // pt 1 on left boundary
    temppt.x = currPos.x + corrHalfWidth*cos(theta+M_PI/2);
    temppt.y = currPos.y + corrHalfWidth*sin(theta+M_PI/2);
    leftBound.push_back(temppt);
    // pt 2 on left boundary
    temppt.x = exitWaypt.x + corrHalfWidth*cos(theta+M_PI/2);
    temppt.y = exitWaypt.y + corrHalfWidth*sin(theta+M_PI/2);
    leftBound.push_back(temppt);
    // Right Boundary
    // pt 1 on right boundary
    temppt.x = currPos.x + corrHalfWidth*cos(theta-M_PI/2);
    temppt.y = currPos.y + corrHalfWidth*sin(theta-M_PI/2);
    rightBound.push_back(temppt);
    // pt 2 on right boundary
    temppt.x = exitWaypt.x + corrHalfWidth*cos(theta-M_PI/2);
    temppt.y = exitWaypt.y + corrHalfWidth*sin(theta-M_PI/2);
    rightBound.push_back(temppt);

    // Specify the velocity profile
		// TODO Specify some max velocity for here
    velIn = 0;
    velOut = 0;
    acc = 0;

    // specify the ocpParams final conditions - lower bounds
    FC_finalPos = exitWaypt;
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];


  } else if(TrafficStateFactory::ROAD_REGION == traffState->getType())
  {
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr); 
   }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }

    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  } else if (TrafficStateFactory::APPROACH_INTER_SAFETY == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr);
    }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  } else if (TrafficStateFactory::INTERSECTION_STOP == traffState->getType()){
    // want to plan over two segments
    SegGoals segment2 = planHoriz.getSegGoal(1);
    point2arr leftBound1, rightBound1, leftBound2, rightBound2;
    // Set of boundary points due to intersection lane
    PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
    PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut);
    if (interBoundErr!=0){
      cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
      return (interBoundErr);
		}
    // Set of boudary pts due to lane after intersection
    LaneLabel laneLabel(segment2.entrySegmentID, segment2.entryLaneID);      
    int rightBoundErr = localMap->getRightBound(rightBound2, laneLabel); 
    int leftBoundErr = localMap->getLeftBound(leftBound2, laneLabel);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return rightBoundErr;
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return leftBoundErr;
   }
    // add the boundaries together
    leftBound = leftBound1;
    rightBound = rightBound1;
    for (unsigned i=0; i<leftBound2.size(); i++){
      if (leftBound.back()!=leftBound2[i]){
	leftBound.push_back(leftBound2[i]);
      }
    }
    for (unsigned i=0; i<rightBound2.size(); i++){
      if (rightBound.back()!=rightBound2[i]){
	rightBound.push_back(rightBound2[i]);
      }
    }

    // TODO: take some subset of these boundary points as the corridor

    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;    

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  }

  cout << "leftBound.size()" << leftBound.size() << endl;
  cout << "rightBound.size()" << rightBound.size() << endl;
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector<double> velProfile;
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);


  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  //        cout << "printing FC's" << endl;
  //  for (int ii=0; ii<6 ; ii++) {
  //    cout << "FCLB[" << ii << "] = " << corr.getOCPparams().finalConditionLB[ii] << endl;
  //  }
  //  for (int ii=0; ii<6 ; ii++) {
  //    cout << "FCLB[" << ii << "] = " << corr.getOCPparams().finalConditionUB[ii] << endl;
  //   }

  return 0;
}
