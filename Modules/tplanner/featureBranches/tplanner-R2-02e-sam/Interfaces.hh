#ifndef TRAFFICPLANNERINTERNALINTERFACES_HH
#define TRAFFICPLANNERINTERNALINTERFACES_HH

#include "gcinterfaces/GcModuleInterfaces.hh"
#include "TrafficState.hh" 
#include "ControlState.hh" 
#include "PlanningHorizon.hh" 

enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };


struct TrafficManagerResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};


struct CorrGenResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct CorridorCreate : public ITransmissive
{
  enum DirectiveName{CORRIDOR_CREATE, PAUSE, END_OF_MISSION};

  /* id of the corresponding mplanner goal id */ //TODO we may need a list here based on the planning horizon 
  vector<unsigned int> m_segGoalIds;

  /* The id of the goal sent by mplanner */
  unsigned int ID; 

  DirectiveName name;

  /* Parameters */
  PointLabel exitPtLabel;
  TrafficState* traffState; 
  ControlState* controlState; 
  PlanningHorizon planHorizon; 

  CorridorCreate()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~CorridorCreate()
  {
    delete traffState; 
    delete controlState; 
  }

  unsigned int getDirectiveId() {
    return ID;
  }

  string toString() const {
    return "CorridorCreate";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & ID;
  }

};

struct CorridorCreateStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ INVALID_MAP_BDRY };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryCreate : public ITransmissive
{
  unsigned int ID;

  // Parameters
  PointLabel exitPtLabel;

  TrajectoryCreate()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryCreate()
  {
  }

  unsigned int getDirectiveId() {
    return ID;
  }

  string toString() const {
    return "TrajectoryCreate";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & ID;
  }

};

struct TrajectoryCreateStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryPause : public ITransmissive
{
  unsigned int ID;

  // Parameters
  PointLabel exitPtLabel;

  TrajectoryPause()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryPause()
  {
  }

  unsigned int getDirectiveId() {
    return ID;
  }

  string toString() const {
    return "TrajectoryPause";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & ID;
  }

};

struct TrajectoryPauseStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryEndMission : public ITransmissive
{
  unsigned int ID;

  // Parameters
  PointLabel exitPtLabel;

  TrajectoryEndMission()
    :ID(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryEndMission()
  {
  }

  unsigned int getDirectiveId() {
    return ID;
  }

  string toString() const {
    return "TrajectoryEndMission";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & ID;
  }

};

struct TrajectoryEndMissionStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned ID;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return ID; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &ID;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

#endif //TRAFFICPLANNERINTERNALINTERFACES_HH
