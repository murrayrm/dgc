#ifndef CONTROLSTATETRANSITION_HH_
#define CONTROLSTATETRANSITION_HH_

#include "ControlState.hh"
#include "TrafficState.hh"
#include "TrafficUtils.hh"
#include "PlanningHorizon.hh"
#include "map/Map.hh"
#include "interfaces/VehicleState.h"
#include "state/StateTransition.hh"
#include "gcinterfaces/SegGoals.hh"
#include "TrafficStateFactory.hh"
#include "frames/point2.hh"


class ControlStateTransition:public StateTransition {

  public:

    ControlStateTransition();
    ControlStateTransition(ControlState * state1, ControlState * state2);

    virtual ~ ControlStateTransition();
    static void setOutputParams(bool debug, bool verbose, bool log);

    virtual double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planningHorizon, Map * localMap, VehicleState vehState) = 0;

    ControlState *getControlStateTo();

  protected:
    int m_probability;
    static bool m_verbose;
    static bool m_debug;
    static bool m_log;



  private:

     ControlState * m_controlState1;
    ControlState *m_controlState2;


};

#endif                          /*CONTROLSTATETRANSITION_HH_ */
