#include "UTurn.hh"

point2 UTurn::m_obsPos;
point2 UTurn::m_rearBound;
bool UTurn::m_firstRun;
point2 UTurn::m_ptStage1;
point2 UTurn::m_ptStage2;
point2 UTurn::m_ptStage3;
point2 UTurn::m_ptStage4;
int UTurn::m_entrySegmentID;
int UTurn::m_entryLaneID;
int UTurn::m_entryWaypointID;
LaneLabel UTurn::m_currLaneLabel;
LaneLabel UTurn::m_otherLaneLabel;
double UTurn::m_dotProd;
double UTurn::m_compDist;
bool UTurn::m_obsUTurn;

UTurn::UTurn(int stateId, ControlStateFactory::ControlStateType type, int stage)
: ControlState(stateId, type)
{
    m_stage = stage;                // want to go into stage one first
    m_firstRun = true;
}

UTurn::~UTurn()
{

}

int UTurn::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{

  cout << "In UTurn.cc::determine corridor" << endl;
  
    
    SegGoals currSegment = planHoriz.getSegGoal(0);
    // TODO: when I receive a UTurn, I do not (at the moment) get any entry info. Thus, I either have to remember this info, or Nok has to send it. 

    double velIn, velOut;
    vector < double >velProfile;
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 IC_pos;
    point2 FC_pos, tmpPt;
    point2arr leftBound, rightBound;
    point2arr leftBound_tmp, rightBound_tmp;
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
    point2 aliceInitPos = getInitialPosition(); /* It does not change between UTurn state transitions */
    //int index;
    //int getBoundsErr, FC_posErr;
    //    double range;
    cout << "aliceInitPos = " << aliceInitPos << endl;
  
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

    // Need to be able to deal with uturns in two scenarios:
    // a) darpa specifies a uturn
    // b) uturn because we sense an obstacle
    //    bool obsUTurn = true;

    if (m_firstRun) {
      m_entrySegmentID = localMap->getSegmentID(aliceInitPos);
      m_entryLaneID = localMap->getLaneID(aliceInitPos);
      PointLabel tmpPtLabel;
      localMap->getLastPointID(tmpPtLabel,aliceInitPos);
      m_entryWaypointID = tmpPtLabel.point;
      m_currLaneLabel = LaneLabel(m_entrySegmentID, m_entryLaneID);
      m_otherLaneLabel= LaneLabel(currSegment.exitSegmentID, currSegment.exitLaneID);
      // check to see if there is an obstacle in front of us. If there is, then set to obsUturn
      double distToObs = TrafficUtils::getNearestObsDist(localMap, vehState, m_currLaneLabel);
      if ((distToObs>0) && (distToObs<30)) {
        m_obsUTurn = true;
        cout << "UTurn due to obstacle " << endl;
      } else {
        m_obsUTurn = false;
        cout << "UTurn specified by DARPA" << endl;
      }
      
      // SVEN: point2 obsPt = localMap->getObstaclePoint(aliceInitPos, 0);
      point2 refPt;
      if (m_obsUTurn) {
        TrafficUtils::getNearestObsPoint(refPt, localMap, vehState, m_currLaneLabel, 0);
        localMap->getLaneCenterPoint(m_ptStage1, m_currLaneLabel, refPt, -21.0);
        localMap->getLaneRightPoint(m_ptStage2, m_otherLaneLabel, refPt, 15.0);
        localMap->getLaneRightPoint(m_ptStage3, m_currLaneLabel, refPt, -10.0); // -14
        localMap->getLaneCenterPoint(m_ptStage4, m_otherLaneLabel, refPt, 27.0);
      } else {
        refPt.set(aliceInitPos);
        localMap->getLaneCenterPoint(m_ptStage1, m_currLaneLabel, refPt, -10.0);
        localMap->getLaneRightPoint(m_ptStage2, m_otherLaneLabel, refPt, 0);
        localMap->getLaneRightPoint(m_ptStage3, m_currLaneLabel, refPt, 1);
        localMap->getLaneCenterPoint(m_ptStage4, m_otherLaneLabel, refPt, 15);
      }
      cout << endl << "################### My first run !!! ###################" << endl;
      cout << "refPt = " << refPt << endl;
      cout << "m_ptStage1 = " << m_ptStage1 << endl;
      cout << "m_ptStage2 = " << m_ptStage2 << endl;
      cout << "m_ptStage3 = " << m_ptStage3 << endl;
      cout << "m_ptStage4 = " << m_ptStage4 << endl;
      m_firstRun = false;
    }

    // Generate the 30m corridor where we perform the UTurn.
    if (5 != m_stage) {
      cout << "UTurn.cc : Generating corridor" << endl;
        
      if (m_obsUTurn) {
        localMap->getLaneRightPoint(tmpPt, m_otherLaneLabel, aliceInitPos, 20);
        leftBound.push_back(tmpPt);
        localMap->getLaneRightPoint(tmpPt, m_currLaneLabel, aliceInitPos, -20);
        rightBound.push_back(tmpPt);

        localMap->getLaneRightPoint(tmpPt, m_otherLaneLabel, aliceInitPos, -10);
        leftBound.push_back(tmpPt);
        localMap->getLaneRightPoint(tmpPt, m_currLaneLabel, aliceInitPos, 10);
        rightBound.push_back(tmpPt);
      } else {
        point2 ptLB_ref, ptRB_ref;
        point2 ptLB1, ptLB2, ptRB1, ptRB2;

        localMap->getLaneRightPoint(ptLB1, m_otherLaneLabel, aliceInitPos, 20);
        leftBound.push_back(ptLB1);

        localMap->getLaneRightPoint(ptRB1, m_currLaneLabel, aliceInitPos, -20);
        rightBound.push_back(ptRB1);

        localMap->getLaneRightPoint(ptLB_ref, m_otherLaneLabel, aliceInitPos, 0);
        localMap->getLaneRightPoint(ptLB2, m_otherLaneLabel, aliceInitPos, -10);
        if (ptLB2.dist(ptLB_ref)<14) {
          // This is the case where the rndf does not stretch out far enough
          double angle = atan2(ptLB_ref.y-ptLB1.y, ptLB_ref.x-ptLB1.x);
          ptLB2.x = ptLB_ref.x + 15*cos(angle);
          ptLB2.y = ptLB_ref.y + 15*sin(angle);
        }
        leftBound.push_back(ptLB2);

        localMap->getLaneRightPoint(ptRB_ref, m_currLaneLabel, aliceInitPos, 0);
        localMap->getLaneRightPoint(ptRB2, m_currLaneLabel, aliceInitPos, 10);
        if (ptRB2.dist(ptRB_ref)<14) {
          // This is the case where the rndf does not stretch out far enough
          double angle = atan2(ptRB_ref.y-ptRB1.y, ptRB_ref.x-ptRB1.x);
          ptRB2.x = ptRB_ref.x + 15*cos(angle);
          ptRB2.y = ptRB_ref.y + 15*sin(angle);
        }
        rightBound.push_back(ptRB2);
      }

    }

    // Variables (parameters) for the stages of the UTurn maneuver.
    double laneDir, heading, hplaneAng;
    double compDist;
    double dotProd;
    PointLabel nextPtLabel, ptLabel;

    switch (m_stage) {

    // Stage 1: reverse until we are at pt in current lane, ~25 m from obs
    case 1:
      {
        // note: setting to reverse mode. mode = 1 --> reverse; mode = 0 --> fwd
        cout << "UTurnStage 1: Reverse" << endl;
        corr.setOCPmode(1);

        // Specify the ocpParams final conditions - lower bounds
        FC_pos = m_ptStage1;
        FC_velMin = 0;

        localMap->getHeading(laneDir, FC_pos); // lane dir is the orientation of the lane
        heading = laneDir ;      // heading is the heading for the final cond
        hplaneAng = laneDir + M_PI;       // hplaneAng is the orientation of the unit vector for the complete test
 
        cout << "laneDir1= " << laneDir << endl;
        cout << "heading1= " << heading << endl;
        cout << "hplaneAng1= " << hplaneAng << endl;
               
        dotProd = (-FC_pos.x + currRearPos.x) * cos(hplaneAng) + (-FC_pos.y + currRearPos.y) * sin(hplaneAng);
        cout << "UTurnStage1: dot product  = " << -FC_pos.x<<" "<<currRearPos.x<<" "<<hplaneAng<<" "<<-FC_pos.y<<" "<<currRearPos.y<<" "<<dotProd << endl;
                
        // Specify some vel profile
        compDist = 1.5;
       if (dotProd > -compDist) {
            velIn = 0;
            velOut = 0;
        } else {
            velIn = 1;
            velOut = 1;
        }

        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial conditions for next stage
        IC_pos = currRearPos;
        m_dotProd = dotProd;
        m_compDist = compDist;
        break;
      }
    // Stage 2: drive fwd and turn ~90 degrees, at dist ~15 m from obstacle
    case 2:
      {
        // note: setting to forward mode. mode = 1 --> reverse; mode = 0 --> fwd
        cout << "UTurnStage 2: Forward" << endl;
        corr.setOCPmode(0);

        // Specify the ocpParams final conditions - lower bounds
        // Get direction of lane
        localMap->getLaneCenterPoint(tmpPt, m_otherLaneLabel, m_ptStage2, 0);
        localMap->getHeading(laneDir, tmpPt);   // lane dir is the orientation of the lane
        heading = laneDir + M_PI / 2;         // heading is the heading for the final cond
        hplaneAng = laneDir + M_PI / 2;       // hplaneAng is the orientation of the unit vector for the complete test
 
        cout << "laneDir2= " << laneDir << endl;
        cout << "heading2= " << heading << endl;
        cout << "hplaneAng2= " << hplaneAng << endl;
        
        //move to point back a little to make sure we stay in the lane
        double easting = -0.2*sin(heading);
        double northing = -0.2*cos(heading);
        FC_pos.set(m_ptStage2.x+northing, m_ptStage2.y+easting);
        //FC_pos = m_ptStage2;

        // Check if we are done with this stage (forward) and we are stopped before switching to stage 3
        dotProd = (-FC_pos.x + currFrontPos.x) * cos(hplaneAng) + (-FC_pos.y + currFrontPos.y) * sin(hplaneAng);
        
        cout << "UTurnStage2: dot product  = " << FC_pos.x<<" "<<currRearPos.x<<" "<<hplaneAng<<" "<<FC_pos.y<<" "<<currRearPos.y<<" "<<dotProd << endl;

        
        // specify some vel profile
        compDist = 2;
        if (dotProd > -compDist) {
          velIn = 0;
          velOut = 0;
        } else {
          velIn = 1;
          velOut = 1;
        }

        FC_velMin = 0;
        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
        
        // Initial conditions for next stage
        IC_pos = currRearPos;
        m_dotProd = dotProd;
        m_compDist = compDist;


      }
      break;

       // Stage 3: Reverse until 8 m from obstacle
    case 3:
      {
        // note: setting to reverse mode. mode = 1 --> reverse; mode = 0 --> fwd
        if ((m_verbose) || (m_debug)) {
            cout << "UTurnStage3: Reverse" << endl;
        }
        corr.setOCPmode(1);

        // Specify the ocpParams final conditions - lower bounds

        // Get direction of lane
        localMap->getLaneCenterPoint(tmpPt, m_currLaneLabel, m_ptStage3, 0);
        localMap->getHeading(laneDir, tmpPt);   // lane dir is the orientation of the lane
        heading = laneDir - 3*M_PI/4;         // heading is the heading for the final cond -M_PI/2
        hplaneAng = laneDir + 3*M_PI/4;       // hplaneAng is the orientation of the unit vector for the complete test M_PI/2
        cout << "UT3laneDir = " << laneDir << endl;
        cout << "UT3heading = " << heading << endl;
        cout << "UT3hplaneAng = " << hplaneAng << endl;

        // Move back point so that we plan for the rear axle
        double easting = 0.2*sin(heading);
        double northing = 0.2*cos(heading);
        FC_pos.x = m_ptStage3.x+northing;
        FC_pos.y = m_ptStage3.y+easting;
        
        dotProd = (-FC_pos.x + currRearPos.x) * cos(hplaneAng) + (-FC_pos.y + currRearPos.y) * sin(hplaneAng);
        cout << "UTurnStage3: dot product  = " << dotProd << endl;
        
        // specify some vel profile
        compDist = 1.5;
        if (dotProd > -compDist) {
          velIn = 0;
          velOut = 0;
        } else {
          velIn = 1;
          velOut = 1;
        }
        
        FC_velMin = 0;
        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial cond's
        IC_pos = currRearPos;
        m_dotProd = dotProd;
        m_compDist = compDist;

      }
        break;

    // Stage 4: drive fwd to exit pt in other lane
    case 4:
      {
        // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
        if ((m_verbose) || (m_debug)) {
            cout << "stage 4: fwd" << endl;
        }
        corr.setOCPmode(0);

        // specify the ocpParams final conditions - lower bounds
        // final pos should be 30m from the obstacle in the other lane
        // TODO: Fix this so that we exit in the middle of the other lane, but for now just exit in the middle of the lanes
        FC_pos = m_ptStage4;

        FC_velMin = 0;
        // offset this pt a little away from the lane so that it returns something (a map bug) | What this comment?
        localMap->getHeading(laneDir, FC_pos);  // lane dir is the orientation of the lane
        
        if ((m_verbose) || (m_debug)) {
            cout << "laneDir = " << laneDir << endl;
        }
        heading = laneDir;      // heading is the heading for the final cond
        if ((m_verbose) || (m_debug)) {
            cout << "heading = " << heading << endl;
        }
        hplaneAng = laneDir;       // hplaneAng is the orientation of the unit vector for the complete test
        if ((m_verbose) || (m_debug)) {
            cout << "hplaneAng = " << hplaneAng << endl;
        }
        
        // check if we are done with this reverse and we are stopped before switching to stage 5
        dotProd = (-FC_pos.x + currFrontPos.x) * cos(hplaneAng) + (-FC_pos.y + currFrontPos.y) * sin(hplaneAng);
        if ((m_verbose) || (m_debug)) {
            cout << "in Uturn_s4: dot product  = " << dotProd << endl;
        }
        
        // specify some vel profile
        compDist = 1;
        if (dotProd > -compDist) {
            velIn = 0;
            velOut = 0;
        } else {
            velIn = 1;
            velOut = 1;
        }

        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial cond's
        IC_pos = currRearPos;
        m_dotProd = dotProd;
        m_compDist = compDist;


      }
        break;

    // stage 5: Normal lane keeping
    case 5:
      {

        // This should just be normal lane keeping from here on out
        if ((m_verbose) || (m_debug)) {
            cout << "UTurn stage 5: drive fwd to the exit waypt" << endl;
        }
        corr.setOCPmode(0);

        bool isReverse = false;
        CorridorUtils::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
        CorridorUtils::setFinalCondLK(corr, currFrontPos, localMap, currSegment, isReverse);
        // specify the ocpParams final conditions - lower bounds
        //        if (traffState->getType() == TrafficStateFactory::ROAD_REGION) {
        //          CorridorUtils::setFinalCondLK(corr, currFrontPos, localMap, currSegment, isReverse);
        //        } else if (traffState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY) {
        //          CorridorUtils::setFinalCondStop(corr, localMap, currSegment);
        //        }

        /*

        range = 50;
        getBoundsErr = localMap->getBounds(leftBound_tmp, rightBound_tmp, m_otherLaneLabel, currFrontPos, range);

        if (getBoundsErr != 0) {
            cerr << "Uturn stage 5: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        // take some subset of these boundary points as the corridor
        // find the closest pt on the boundary
        TrafficUtils::insertProjPtInBoundary(leftBound_tmp, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(leftBound_tmp, currFrontPos);
        for (int ii = index; ii < (int) leftBound_tmp.size(); ii++) {
            leftBound.push_back(leftBound_tmp[ii]);
        }
        TrafficUtils::insertProjPtInBoundary(rightBound_tmp, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(rightBound_tmp, currFrontPos);
        for (int ii = index; ii < (int) rightBound_tmp.size(); ii++) {
            rightBound.push_back(rightBound_tmp[ii]);
        }
        
        // Specify the velocity profile
        // velIn = currSegment.maxSpeedLimit;
        velOut = currSegment.maxSpeedLimit; 
        velOut = 1;
        velIn = 1;

        // specify the ocpParams final conditions - lower bounds
        // want to define this position based on the corridor?
        FC_posErr = localMap->getWaypoint(FC_pos, exitPtLabel);
        if (FC_posErr != 0) {
            cerr << "UTurn.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FC_posErr);
        }
        FC_velMin = 0;

        localMap->getHeading(heading,exitPtLabel);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
        
        IC_pos = currRearPos;
        */
      }
        break;

    default:
        break;
        
    }
    
    if (m_stage!=5) { 
      corr.addPolyline(leftBound);
      corr.addPolyline(rightBound);
      //    cout << "leftBound = " << leftBound << endl << "rightBound = " << rightBound << endl;
      // Set the velocity profile - need to think this through some
      //    point2 tmpEntryPt((leftBound[0].x+rightBound[0].x)/2,(leftBound[0].y+rightBound[0].y)/2);
      // point2 tmpExitPt((leftBound[1].x+rightBound[1].x)/2,(leftBound[1].y+rightBound[1].y)/2);
      // corr.setSpeedProfileDist(tmpEntryPt, velIn, velOut, tmpExitPt, false);
     
      //    cout << "Uturn stage final position = " << FC_pos << endl;
     
      // set the final conditions in the ocpspecs

      corr.setOCPfinalCondLB(EASTING_IDX_C, FC_pos.y);
      corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_pos.x);
      corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
      corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
      corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
      corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
      // specify the ocpParams final conditions - upper bounds
      corr.setOCPfinalCondUB(EASTING_IDX_C, FC_pos.y);
      corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_pos.x);
      corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
      corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
      corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
      corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  
      corr.setOCPinitialCondLB(EASTING_IDX_C, IC_pos.y);
      corr.setOCPinitialCondLB(NORTHING_IDX_C, IC_pos.x);
      corr.setOCPinitialCondUB(EASTING_IDX_C, IC_pos.y);
      corr.setOCPinitialCondUB(NORTHING_IDX_C, IC_pos.x);
    }
    return 0;
}

void UTurn::resetParameters()
{
    if ((m_verbose) || (m_debug)) {
        cout << "reset parameters for next uturn" << endl;
    }
    m_firstRun = true;
    m_obsUTurn = false;
}

bool UTurn::isComplete() {
  return (5 == m_stage);
}
