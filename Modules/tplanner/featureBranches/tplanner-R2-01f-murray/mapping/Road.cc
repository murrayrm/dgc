#include "Road.hh"


Road::Road()
{
	//TODO remove this 
}

Road::Road(int roadId)
{
//TODO remove this		
		
}

Road::Road(int roadId, std::vector<Line> roadBoundary)
: m_id(roadId)
, m_roadBoundary(roadBoundary)
{  
}

Road::Road(int roadId, std::vector<Lane> lanes, std::vector<Line> roadBoundary)
: m_id(roadId)
, m_lanes(lanes)
, m_roadBoundary(roadBoundary)
{ 
}


Road::Road(int roadId, std::vector<Lane> lanes, std::vector<Line> roadBoundary, std::vector<RoadLink> roadLinks)
: m_id(roadId)
, m_lanes(lanes)
, m_roadBoundary(roadBoundary)
, m_roadLinks(roadLinks)
{ 
}

Road::~Road()
{
}

bool Road::hasStopLine() 
{
bool hasStop = false; 
for (int i=0; m_interLinks.size(); i++) {
if (m_interLinks[i].hasStopLine()) {
hasStop = true;  
}	
}
return hasStop;
}

Line* Road::getStopLine(){  
//TODO is this necessary getStopLine? 
return new Line();
}

std::vector<Line> Road::getRoadBoundary(){  
return m_roadBoundary; 
}

std::vector<Lane> Road::getLanes(){  
return m_lanes;  
}

std::vector<RoadLink> Road::getRoadLinks(){  
return m_roadLinks;  
}

void Road::setLanes(std::vector<Lane> lanes)
{
m_lanes = lanes;   
}

void Road::setStopLine(Line stopLine)
{

//TODO is this necessary setStopline ? 
}  

int Road::getId()
{  
return m_id; 
}

void Road::assignLaneNumbers() {


}

