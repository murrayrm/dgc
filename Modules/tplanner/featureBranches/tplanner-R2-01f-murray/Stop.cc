#include "Stop.hh"


Stop::Stop(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
  , m_desDecc(-0.5)
{
}

Stop::~Stop()
{
}

int Stop::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
  point2arr leftBound, rightBound;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;

  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  double IC_velMin, IC_velMax;

  point2arr leftBound1, rightBound1;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);




  if(TrafficStateFactory::ROAD_REGION == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    double range = 50;
    int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
    if (getBoundsErr!=0){
      cerr << "LaneKeeping.cc: boundary read from map error" << endl;
      return (getBoundsErr);
    }  
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // in the road region want to continue stopping at the same acc as slowdown
    // TODO: think through the vel profile specification some more, but use current velocity.
    // TODO: fix this /2 - mismatch in vel profiles! when switching from slowdown to stop
    double initVel = getInitialVelocity()/2;
    if ((m_verbose) || (m_debug)){
      cout << "stop initial velocity = " << initVel << endl;
}
    double cStateLength = -pow(initVel,2)/(2*m_desDecc);
    if ((m_verbose) || (m_debug)){
      cout << "stop corridor length = " << cStateLength << endl;
    }
    double distIntoControlState = calcDistFromInitPos(vehState);

    velIn = max(initVel + ((0-initVel)/cStateLength)*distIntoControlState,0.0);
    velOut = 0;
    acc = m_desDecc;

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];







  } else if (TrafficStateFactory::APPROACH_INTER_SAFETY == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    double range = 50;
    int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
    if (getBoundsErr!=0){
      cerr << "LaneKeeping.cc: boundary read from map error" << endl;
      return (getBoundsErr);
    }  

    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
        leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
        rightBound.push_back(rightBound1[ii]);
      }

    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // here I want to adjust my acc based on my current vel and the stopline pos

    // TODO: think through the vel profile specification some more, but use current velocity.
    // TODO: fix this /2 - the problem is that the vel profile changes when we switch from slowdown to stop - and we do not want that!
    double initVel = getInitialVelocity()/2;
    if ((m_verbose) || (m_debug)){
      cout << "stop initial velocity = " << initVel << endl;
    }
    double cStateLength = -pow(initVel,2)/(2*m_desDecc);
    if ((m_verbose) || (m_debug)){
      cout << "stop corridor length = " << cStateLength << endl;
    }
    double distIntoControlState = calcDistFromInitPos(vehState);
    //velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState;

    velIn = max(initVel + ((0-initVel)/cStateLength)*distIntoControlState,0.0);
    velOut = 0;
    acc = m_desDecc;


    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

    //    IC_velMin = min(velIn, AliceStateHelper::getVelocityMag(vehState));
    //IC_velMax = IC_velMin;



  } else if (TrafficStateFactory::INTERSECTION_STOP == traffState->getType()){
    // want to plan over two segments
    SegGoals nextSegGoal = planHoriz.getSegGoal(1);
    point2arr leftBound1, rightBound1, leftBound2, rightBound2;
    // Set of boundary points due to intersection lane
    PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
    if ((m_verbose) || (m_debug)){
      cout << "intersection entry waypt id = " << ptLabelIn << endl;
    }
    PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    if ((m_verbose) || (m_debug)){
      cout << "intersection exit waypt id = " << ptLabelOut << endl;
    }
    double range = 50;
    int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
    if (interBoundErr!=0){
      cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
      return (interBoundErr);
    }

    leftBound = leftBound1;
    rightBound = rightBound1;

    // Specify the velocity profile - want to come to a stop as quickly as possible
    // correct behavior is to come to a stop as quickly as possible
    velIn = 0;
    velOut = 0;
    acc = 0;

    // specify the ocpParams final conditions - lower bounds
    // want to define this position based on the corridor?
    PointLabel exitPtLabel(nextSegGoal.exitSegmentID, nextSegGoal.exitLaneID, nextSegGoal.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    FC_velMin =  0;
    FC_headingMin = -M_PI; // unbounded
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = M_PI; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  }






  if ((m_verbose) || (m_debug)){
    cout << "velocity profile: in = " << velIn << " and out = " << velOut << endl;
    cout << "specified acceleration = " << acc <<  endl;
  }
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector<double> velProfile;
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  corr.setOCPinitialCondLB(VELOCITY_IDX_C, IC_velMin);
  corr.setOCPinitialCondUB(VELOCITY_IDX_C, IC_velMax);

  //        cout << "printing FC's" << endl;
  //  for (int ii=0; ii<6 ; ii++) {
  //    cout << "FCLB[" << ii << "] = " << corr.getOCPparams().finalConditionLB[ii] << endl;
  //  }
  //  for (int ii=0; ii<6 ; ii++) {
  //    cout << "FCLB[" << ii << "] = " << corr.getOCPparams().finalConditionUB[ii] << endl;
  //   }

  return 0;
}
