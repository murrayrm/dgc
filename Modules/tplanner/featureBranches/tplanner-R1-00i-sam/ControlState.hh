#ifndef CONTROLSTATE_HH_
#define CONTROLSTATE_HH_

#include "Corridor.hh"
#include "map/Map.hh"
#include "interfaces/VehicleState.h"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "state/State.hh" 
#include "ControlStateFactory.hh"


class ControlState : public State {

public: 

  ControlState(int stateId, ControlStateFactory::ControlStateType type);
  ControlState();
  virtual ~ControlState();
  virtual int determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* map) =0;
  //ControlStateFactory::ControlStateType getType();
 
  void setInitialPosition(VehicleState vehState);
  void setUpdatedPosition(VehicleState vehState);
  point2 getInitialPosition();
  point2 getUpdatedPosition();
  double calcDistFromInitPos(VehicleState vehState);  

private: 

  point2 m_AliceInitPos;
  point2 m_AliceUpdatePos;
  double m_currDistFromInitPos; 
  //ControlStateFactory::ControlStateType m_type; 
  
};
#endif /*CONTROLSTATE_HH_*/
