#include "SlowDownToStop.hh"
#include <math.h>
#include <list>


SlowDownToStop::SlowDownToStop(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_distToStopTrans(1.5)
, m_desiredDecel(-0.5)
{
//TODO FIX this desired Decel
}

SlowDownToStop::~SlowDownToStop()
{

}

double SlowDownToStop::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon horiz ,Map* map, VehicleState vehState)
{

	cout << "in SlowDownToStop::meetTransitionConditions" <<endl;
	double vel = 0; 
	double dstop = 0;
	int stopLineErr = -1;
	point2 stopLinePos;
	PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
	point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);
	//TODO fix composite state 
	//if (TrafficStateFactory::CLEARREGION == trafficState.getType()) 
	if (TrafficStateFactory::APPROACH_INTER_SAFETY == trafficState->getType()) {
	  vel = AliceStateHelper::getVelocityMag(vehState);

	  stopLineErr = map->getStopline(stopLinePos,label);
	  dstop = stopLinePos.dist(currPos);
	  
	  if (dstop <= m_distToStopTrans ) {
	    m_probability = 1;
	  } else { m_probability = 0; }
	}
	setUncertainty(m_probability);
	return m_probability;
}

