#include "UTurn.hh"

point2 UTurn::m_obsPos;
point2 UTurn::m_rearBound;
bool UTurn::m_firstRun;
point2 UTurn::m_ptStage1;
point2 UTurn::m_ptStage2;
point2 UTurn::m_ptStage3;
point2 UTurn::m_ptStage4;
int UTurn::m_entrySegmentID;
int UTurn::m_entryLaneID;
int UTurn::m_entryWaypointID;

UTurn::UTurn(int stateId, ControlStateFactory::ControlStateType type, int stage)
: ControlState(stateId, type)
{
    m_stage = stage;                // want to go into stage one first
    m_firstRun = true;
}

UTurn::~UTurn()
{

}

int UTurn::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{

    if ((m_verbose) || (m_debug)) {
        cout << "In UTurn.cc::determine corridor" << endl;
    }
    
    SegGoals currSegment = planHoriz.getSegGoal(0);
    // TODO: when I receive a UTurn, I do not (at the moment) get any entry info. Thus, I either have to remember this info, or Nok has to send it. 


    double velIn, velOut, acc;
    vector < double >velProfile;
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 IC_pos;
    point2 FC_pos, tmpPt;
    point2arr leftBound, rightBound;
    point2arr leftBound_tmp, rightBound_tmp;
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
    point2 aliceInitPos = getInitialPosition(); /* It does not change between UTurn state transitions */
    if ((m_verbose) || (m_debug)) {
        cout << "aliceInitPos = " << aliceInitPos << endl;
    }
    int index;
    int getBoundsErr, FC_posErr;
    double range;
    cout << "aliceInitPos = " << aliceInitPos << endl;
  
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

    // Step 1: Get bounds (from obstacle and back to 30 m)
    // From the control state transition, we have our position when we transition into UTurn.
    // All other maneuvers relative to this?
    LaneLabel laneLabel_curr(m_entrySegmentID, m_entryLaneID);
    LaneLabel laneLabel_other(currSegment.exitSegmentID, currSegment.exitLaneID);
    
    /* Variables to set on the first run */
    if (m_firstRun) {
      m_entrySegmentID = localMap->getSegmentID(aliceInitPos);
      m_entryLaneID = localMap->getLaneID(aliceInitPos);
      PointLabel tmpPtLabel;
      localMap->getLastPointID(tmpPtLabel,aliceInitPos);
      m_entryWaypointID = tmpPtLabel.point;

      point2 obsPt = localMap->getObstaclePoint(aliceInitPos, 0);
      
      cout << "################### My first run !!! ###################" << endl;
      localMap->getLaneCenterPoint(m_ptStage1, laneLabel_curr, obsPt, -28.0);
      localMap->getLaneRightPoint(m_ptStage2, laneLabel_other, obsPt, 17.0);
      localMap->getLaneCenterPoint(m_ptStage3, laneLabel_curr, obsPt, -8.0);
      localMap->getLaneCenterPoint(m_ptStage4, laneLabel_other, obsPt, 30.0);
      m_firstRun = false;
    }

    if (5 != m_stage) {
        cout << "UTurn.cc : Generating corridor" << endl;
        localMap->getLaneRightPoint(tmpPt, laneLabel_other, aliceInitPos, 20);
        leftBound.push_back(tmpPt);
        localMap->getLaneRightPoint(tmpPt, laneLabel_curr, aliceInitPos, -20);
        rightBound.push_back(tmpPt);

        localMap->getLaneRightPoint(tmpPt, laneLabel_other, aliceInitPos, -10);
        leftBound.push_back(tmpPt);
        localMap->getLaneRightPoint(tmpPt, laneLabel_curr, aliceInitPos, 10);
        rightBound.push_back(tmpPt);
    }

    double heading, laneDir, orient;
    double compDist;
    double dotProd;
    PointLabel nextPtLabel, ptLabel;

    switch (m_stage) {

    // Stage 1: reverse until we are at pt in current lane, 25 m from obs
    case 1:
    
        // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
        if ((m_verbose) || (m_debug)) {
            cout << "stage 1: reverse" << endl;
        }
        corr.setOCPmode(1);

        // Get all the relevant points for the u-turn
        // Specify the ocpParams final conditions - lower bounds
        // Final pos should be 25 m from the obstacle
        FC_pos = m_ptStage1;
        FC_velMin = 0;
        localMap->getHeading(laneDir, FC_pos);
        
        if ((m_verbose) || (m_debug)) {
            cout << "laneDir = " << laneDir << endl;
        }
        heading = laneDir;
        if ((m_verbose) || (m_debug)) {
            cout << "heading = " << heading << endl;
        }
        orient = laneDir - M_PI;
        if ((m_verbose) || (m_debug)) {
            cout << "orient = " << orient << endl;
        }
        
        // Check if we are done with this reverse and we are stopped before switching to stage 2
        dotProd = (-FC_pos.x + currRearPos.x) * cos(orient) + (-FC_pos.y + currRearPos.y) * sin(orient);
        if ((m_verbose) || (m_debug)) {
            cout << "in UTurn_s11: dot product  = " << -FC_pos.x<<" "<<currRearPos.x<<" "<<orient<<" "<<-FC_pos.y<<" "<<currRearPos.y<<" "<<dotProd << endl;
        }
        
        // Specify some vel profile
        compDist = 1.5;
        if (dotProd > -compDist) {
            velIn = 0;
            velOut = 0;
            acc = 0;
        } else {
            velIn = 1;
            velOut = 1;
            acc = 0;
        }
        
        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial cond's
        IC_pos = currRearPos;
        
        break;

    // Stage 2: drive fwd and turn 90 degrees, at dist 8m from obstacle
    case 2:
    
        // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
        if ((m_verbose) || (m_debug)) {
            cout << "stage 2: fwd" << endl;
        }
        
        corr.setOCPmode(0);

        // Specify the ocpParams final conditions - lower bounds
        // final pos should be  17m from the obstacle, and facing out of the lane
        /* tmpPt = m_ptStage2;
        TrafficUtils::insertProjPtInBoundary(leftBound, tmpPt);
        index = TrafficUtils::getClosestPtIndex(leftBound, tmpPt); */
        FC_pos = m_ptStage2;
        FC_velMin = 0;
        
        // Get direction of lane
        localMap->getLaneCenterPoint(tmpPt, laneLabel_other, FC_pos, 0.0);
        localMap->getHeading(laneDir, tmpPt);   // lane dir is the orientation of the lane
        
        if ((m_verbose) || (m_debug)) {
            cout << "laneDir = " << laneDir << endl;
        }
        heading = laneDir + M_PI / 2;   // heading is the heading for the final cond
        if ((m_verbose) || (m_debug)) {
            cout << "heading = " << heading << endl;
        }
        orient = laneDir + M_PI / 2;    // orientation is the orientation of the unit vector for the complete test
        if ((m_verbose) || (m_debug)) {
            cout << "orient = " << orient << endl;
        }
        
        // Check if we are done with this reverse and we are stopped before switching to stage 2
        dotProd = (-FC_pos.x + currFrontPos.x) * cos(orient) + (-FC_pos.y + currFrontPos.y) * sin(orient);

        if ((m_verbose) || (m_debug)) {
            cout << "in UTurn_s2: dot product  = " << dotProd << endl;
        }
        
        // specify some vel profile
        compDist = 1;
        if (dotProd > -compDist) {
            velIn = 0;
            velOut = 0;
            acc = 0;
        } else {
            velIn = 1;
            velOut = 1;
            acc = 0;
        }

        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial cond's
        IC_pos = currFrontPos;

        break;

    // Stage 3: Reverse until 8 m from obstacle
    case 3:
    
        // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
        if ((m_verbose) || (m_debug)) {
            cout << "stage 3: reverse" << endl;
        }
        corr.setOCPmode(1);
        if ((m_verbose) || (m_debug)) {
            cout << "Not yet implemented" << endl; // What does that mean?
        }
        
        // get all the relevant points for the u-turn
        // specify the ocpParams final conditions - lower bounds
        // final pos should be 25 m from the obstacle
        FC_pos = m_ptStage3;
        FC_velMin = 0;
        localMap->getHeading(laneDir, FC_pos);
        
        if ((m_verbose) || (m_debug)) {
            cout << "laneDir = " << laneDir << endl;
        }
        heading = laneDir - M_PI;
        if ((m_verbose) || (m_debug)) {
            cout << "heading = " << heading << endl;
        }
        orient = laneDir;
        if ((m_verbose) || (m_debug)) {
            cout << "orient = " << orient << endl;
        }

        dotProd = (-FC_pos.x + currRearPos.x) * cos(orient) + (-FC_pos.y + currRearPos.y) * sin(orient);
        if ((m_verbose) || (m_debug)) {
            cout << "in UTurn_s3: dot product  = " << dotProd << endl;
        }
        
        // specify some vel profile
        compDist = 1.5;
        if (dotProd > -compDist) {
            velIn = 0;
            velOut = 0;
            acc = 0;
        } else {
            velIn = 1;
            velOut = 1;
            acc = 0;
        }
        
        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial cond's
        IC_pos = currRearPos;

        break;

    // Stage 4: drive fwd to exit pt in other lane
    case 4:
    
        // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
        if ((m_verbose) || (m_debug)) {
            cout << "stage 4: fwd" << endl;
        }
        corr.setOCPmode(0);

        // specify the ocpParams final conditions - lower bounds
        // final pos should be 30m from the obstacle in the other lane
        // TODO: Fix this so that we exit in the middle of the other lane, but for now just exit in the middle of the lanes
        FC_pos = m_ptStage4;

        FC_velMin = 0;
        // offset this pt a little away from the lane so that it returns something (a map bug) | What this comment?
        localMap->getHeading(laneDir, FC_pos);  // lane dir is the orientation of the lane
        
        if ((m_verbose) || (m_debug)) {
            cout << "laneDir = " << laneDir << endl;
        }
        heading = laneDir;      // heading is the heading for the final cond
        if ((m_verbose) || (m_debug)) {
            cout << "heading = " << heading << endl;
        }
        orient = laneDir;       // orientation is the orientation of the unit vector for the complete test
        if ((m_verbose) || (m_debug)) {
            cout << "orient = " << orient << endl;
        }
        
        // check if we are done with this reverse and we are stopped before switching to stage 2
        dotProd = (-FC_pos.x + currFrontPos.x) * cos(orient) + (-FC_pos.y + currFrontPos.y) * sin(orient);
        if ((m_verbose) || (m_debug)) {
            cout << "in Uturn_s4: dot product  = " << dotProd << endl;
        }
        
        // specify some vel profile
        compDist = 1;
        if (dotProd > -compDist) {
            velIn = 0;
            velOut = 0;
            acc = 0;
        } else {
            velIn = 1;
            velOut = 1;
            acc = 0;
        }

        FC_headingMin = heading;
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        // Initial cond's
        IC_pos = currFrontPos;

        break;

    // stage 5: Normal lane keeping
    case 5:
        // This should just be normal lane keeping from here on out
        if ((m_verbose) || (m_debug)) {
            cout << "UTurn stage 5: drive fwd to the exit waypt" << endl;
        }
        corr.setOCPmode(0);

        range = 50;
        getBoundsErr = localMap->getBounds(leftBound_tmp, rightBound_tmp, laneLabel_other, currFrontPos, range);

        if (getBoundsErr != 0) {
            cerr << "Uturn stage 5: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        // take some subset of these boundary points as the corridor
        // find the closest pt on the boundary
        TrafficUtils::insertProjPtInBoundary(leftBound_tmp, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(leftBound_tmp, currFrontPos);
        for (int ii = index; ii < (int) leftBound_tmp.size(); ii++) {
            leftBound.push_back(leftBound_tmp[ii]);
        }
        TrafficUtils::insertProjPtInBoundary(rightBound_tmp, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(rightBound_tmp, currFrontPos);
        for (int ii = index; ii < (int) rightBound_tmp.size(); ii++) {
            rightBound.push_back(rightBound_tmp[ii]);
        }
        
        // Specify the velocity profile
        /* velIn = currSegment.maxSpeedLimit;
        velOut = currSegment.maxSpeedLimit; */
        velOut = 1;
        velIn = 1;
        acc = 0;

        // specify the ocpParams final conditions - lower bounds
        // want to define this position based on the corridor?
        FC_posErr = localMap->getWaypoint(FC_pos, exitPtLabel);
        if (FC_posErr != 0) {
            cerr << "UTurn.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FC_posErr);
        }
        FC_velMin = 0;

        double heading;
        localMap->getHeading(heading,exitPtLabel);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        IC_pos = currFrontPos;

        break;

    default:
        break;
        
    }

    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // Set the velocity profile - need to think this through some
    velProfile.push_back(velIn);        // entry pt
    velProfile.push_back(velOut);       // exit pt
    corr.setVelProfile(velProfile);
    corr.setDesiredAcc(acc);

    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_pos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_pos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_pos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_pos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    corr.setOCPinitialCondLB(EASTING_IDX_C, IC_pos.y);
    corr.setOCPinitialCondLB(NORTHING_IDX_C, IC_pos.x);
    corr.setOCPinitialCondUB(EASTING_IDX_C, IC_pos.y);
    corr.setOCPinitialCondUB(NORTHING_IDX_C, IC_pos.x);

    return 0;
}

void UTurn::resetParameters()
{
    if ((m_verbose) || (m_debug)) {
        cout << "reset parameters for next uturn" << endl;
    }
    m_firstRun = true;
}
