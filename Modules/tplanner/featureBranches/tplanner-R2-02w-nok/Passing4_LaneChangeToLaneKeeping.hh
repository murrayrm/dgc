#ifndef PASSING4_LANECHANGETOLANEKEEPING_HH_
#define PASSING4_LANECHANGETOLANEKEEPING_HH_

#include "ControlStateTransition.hh"
#include "LaneChange.hh"

class Passing4_LaneChangeToLaneKeeping:public ControlStateTransition {

public:
  Passing4_LaneChangeToLaneKeeping(ControlState *state1, ControlState *state2);
  Passing4_LaneChangeToLaneKeeping();
  ~Passing4_LaneChangeToLaneKeeping();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
