#include "GeometricConstraints.hh"


GeometricConstraints::GeometricConstraints()
{
  	//nothing to do
}
  

GeometricConstraints::~GeometricConstraints()
{
  //nothing to do
}

GeometricConstraints::GeometricConstraints(std::vector<LinearInequality> planes)
  : m_planes(planes) 
{
  //nothing to do
}

  
GeometricConstraints::GeometricConstraints(const GeometricConstraints& constraints) 
{
  //	m_planes = constraints.getLinearInequalities();
  //	m_geoUncert = constraints.getGeometricUncertainty();		
}

std::vector<LinearInequality> GeometricConstraints::getLinearInequalities() 
{
  return m_planes;
}
	
double GeometricConstraints::getGeometricUncertainty() 
{
  return m_geoUncert;
}

/** 
 *  Projects the geometry defined by planes 3D object to the plane and returns a nx3 matrix.
 *  
 */
std::vector<LinearInequality> GeometricConstraints::projectToTwoDimensions() 
{
  std::vector<LinearInequality> twoDimVectors; 
  //TODO construct convex hull in xy plane 	
  return twoDimVectors;
}
  

