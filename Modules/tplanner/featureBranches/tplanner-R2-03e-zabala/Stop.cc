#include "Stop.hh"

Stop::Stop(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
, m_desDecc(-0.5)
{

}

Stop::~Stop()
{

}

int Stop::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    /* This is not in LaneKeeping + not initialized by used! */
    /* double IC_velMin, IC_velMax; */

    point2arr leftBound, rightBound, leftBound1, rightBound1;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 FC_finalPos;
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);

    switch (traffState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        // Want to plan for current segment only
        // TODO: Want to use function that does not specify specific lane
        // LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, desiredLane, currFrontPos, range);
        if (getBoundsErr != 0) {
            cerr << "LaneKeeping.cc: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currRearPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currRearPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currRearPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currRearPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }

        // Specify the ocpParams final conditions - lower bounds
        // Want to define this position based on the corridor?
        PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

        // The only difference between a Road Region and Approach Intersection Safety is here
        int FCPos_Err = 0;
        if (traffState->getType() == TrafficStateFactory::ROAD_REGION) {
            FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
        } else if (traffState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY) {
            FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
        }

        if (FCPos_Err != 0) {
            cerr << "Stop.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FCPos_Err);
        }

        FC_velMin = 0;
        double heading;
        localMap->getHeading(heading,exitPtLabel);


        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // Specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
        
        /* IC_velMin = min(velIn, AliceStateHelper::getVelocityMag(vehState));
        IC_velMax = IC_velMin; */
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
        // Want to plan over two segments
        SegGoals nextSegGoal = planHoriz.getSegGoal(1);
        point2arr leftBound1, rightBound1, leftBound2, rightBound2;
        
        // Set of boundary points due to intersection lane
        PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
        if ((m_verbose) || (m_debug)) {
            cout << "intersection entry waypt id = " << ptLabelIn << endl;
        }
        
        PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        if ((m_verbose) || (m_debug)) {
            cout << "intersection exit waypt id = " << ptLabelOut << endl;
        }
        
        double range = 50;
        int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range, range);
        if (interBoundErr != 0) {
            cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
            return (interBoundErr);
        }

        leftBound.set(leftBound1);
        rightBound.set(rightBound1);

        // Specify the ocpParams final conditions - lower bounds
        // Want to define this position based on the corridor?
        // If we are still coming up to the stop line, I want the FC to 
        // be the stopline, otherwise the FC should be our current location
        double heading;
        localMap->getHeading(heading, ptLabelIn);
        point2 tmpPt;
        localMap->getNextStopline(tmpPt, ptLabelIn);
        double dotProd = (-tmpPt.x+currFrontPos.x)*cos(heading) + (-tmpPt.y+currFrontPos.y)*sin(heading);
        cout << "dot product = " << dotProd << endl;
        if (dotProd<0) { 
          // This is the case where we are still approaching the intersection
          FC_finalPos = tmpPt;
          heading = AliceStateHelper::getHeading(vehState);
        } else {
          // This is the case where we want to stop inside the intersection
          FC_finalPos = currFrontPos;
        }
        
        FC_velMin = 0;
        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // Specify the ocpParams final conditions - upper bounds
        FC_velMax = 0;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;
        
    default:
    
        cerr << "Stop.cc: Undefined Traffic state" << endl;

    }

    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    /* corr.setOCPinitialCondLB(VELOCITY_IDX_C, IC_velMin);
    corr.setOCPinitialCondUB(VELOCITY_IDX_C, IC_velMax); */

    return 0;
}
