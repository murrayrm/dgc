#ifndef PASSING1_LANEKEEPINGTOLANECHANGE_HH_
#define PASSING1_LANEKEEPINGTOLANECHANGE_HH_

#include "ControlStateTransition.hh"

class Passing1_LaneKeepingToLaneChange:public ControlStateTransition {

public:
  Passing1_LaneKeepingToLaneChange(ControlState *state1, ControlState *state2);
  Passing1_LaneKeepingToLaneChange();
  ~Passing1_LaneKeepingToLaneChange();
  meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon *phorizon, Map *map, VehicleState *vstate);

private:
  double m_prob;
}

#endif
