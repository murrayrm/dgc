#include "LaneKeepingToLaneChange.hh"
#include <math.h>
#include <list>


LaneKeepingToLaneChange::LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

LaneKeepingToLaneChange::LaneKeepingToLaneChange()
{

}

LaneKeepingToLaneChange::~LaneKeepingToLaneChange()
{

}

double LaneKeepingToLaneChange::meetTransitionConditions(ControlState * cstate, TrafficState * tstate, PlanningHorizon phorizon, Map * map, VehicleState vstate)
{

    switch (tstate->getType()) {
    
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        
        if ((m_verbose) || (m_debug)) {
            cout << "in LaneKeepingToLaneChange: APPROACH_INTER_SAFETY" << endl;
        }
        
	int currLane = 0;
        double currVel = 0;
        double delta = 0;
        double dstop = 0;
        int stopLineErr = -1;
        point2 stopLinePos, currFrontPos;
        PointLabel label = PointLabel(phorizon.getCurrentExitSegment(), phorizon.getCurrentExitLane(), phorizon.getCurrentExitWaypoint());
        currFrontPos = AliceStateHelper::getPositionFrontBumper(vstate);
        currVel = AliceStateHelper::getVelocityMag(vstate);
        delta = -pow(currVel, 2) / (2 * m_desiredDecel);
        // Use getStopline() call based on our current pos here
        stopLineErr = localMap->getNextStopline(stopLinePos, label);
        // Or should we use: stopLineErr = map->getStopline(stopLinePos,currFrontPos);
        dstop = stopLinePos.dist(currFrontPos);
        cout << "distance to stop = " << dstop << endl;
        if (dstop <= delta && phorizon.getCurrentExitLane() != currLane) {
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;

    case TrafficStateFactory::ZONE_REGION:
    
        m_probability = 0;
        
        break;

    case TrafficStateFactory::ROAD_REGION:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "in LaneKeepingToLaneChange: ROAD_REGION" << endl;
        }
        
	int currLane = 0;
        double currVel = AliceStateHelper::getVelocityMag(vstate);
        double delta = -pow(currVel, 2) / (2 * m_desiredDecel) + m_distToObs;
        if ((m_verbose) || (m_debug)) {
            cout << "LaneKeepingToLaneChange delta = " << delta << endl;
        }
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vstate);
        double distToObs = map->getObstacleDist(currFrontPos, 0);
        if (distToObs <= delta) {
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
   
        m_probability = 0;
        
        break;
    
    default:
    
        m_probability = 0;
        cerr << "LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }

    setUncertainty(m_probability);
    
    return m_probability;
}
