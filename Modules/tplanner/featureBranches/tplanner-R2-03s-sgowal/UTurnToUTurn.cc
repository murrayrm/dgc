#include "UTurnToUTurn.hh"
#include "UTurn.hh"

UTurnToUTurn::UTurnToUTurn(ControlState * state1, ControlState * state2, int stage)
: ControlStateTransition(state1, state2)
{
    m_stage = stage;
}

UTurnToUTurn::UTurnToUTurn()
{

}

UTurnToUTurn::~UTurnToUTurn()
{

}

double UTurnToUTurn::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * localMap, VehicleState vehState)
{

    cout << "in UTurnToUTurn::meetTransitionConditions" << endl;
    PointLabel ptLabel = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
    point2 exitWayPt;
    localMap->getWaypoint(exitWayPt, ptLabel);
    double angle;
    localMap->getHeading(angle, ptLabel);
        
    m_probability = 0;
    double stoppedVel = 0.2;
    double dotProd = UTurn::m_dotProd;
    double compDist = UTurn::m_compDist;
    double currVel = AliceStateHelper::getVelocityMag(vehState);
    
    switch (m_stage) {
    
    case 1:
      {
        dotProd = UTurn::m_dotProd;
        compDist = UTurn::m_compDist;
        currVel = AliceStateHelper::getVelocityMag(vehState);
        
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          cout << "UTURN1: SWITCHING TO STAGE 2!!!" << endl;
          m_probability = 1;
        }
      }
        break;

       
    case 2:
      {
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          cout << endl << "in UTURN2: SWITCHING TO STAGE 3!!!" << endl;
          m_probability = 1;
        }
      }
      break;
      
    case 3:
      {
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          cout << "UTURN3: SWITCHING TO STAGE 4!!!" << endl;
          m_probability = 1;
        }
        
      }
      break;

    case 4:
      {
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          cout << "UTURN4: SWITCHING TO STAGE 5!!!" << endl;
          m_probability = 1;
        }
      }
      break;
      
    default:
      break;
      
    }
    
    cout << endl << "UTurn (" << m_stage << ") = " << m_probability << endl <<endl;
    setUncertainty(m_probability);
    return m_probability;
}
