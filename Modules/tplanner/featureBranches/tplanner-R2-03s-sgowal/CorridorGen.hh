#ifndef CORRIDORGEN_HH_
#define CORRIDORGEN_HH_

#include "TrafficUtils.hh"
#include "frames/point2.hh"
#include <vector>
//#include "alice/AliceConstants.h"
#include "interfaces/VehicleState.h"
#include "map/Map.hh"
#include "Corridor.hh"
//#include "Conflict.hh"
#include "mapping/Segment.hh"
//#include "TrafficStateEstimator.hh"
//#include "ControlState.hh"
//#include "ControlStateFactory.hh"
#include "TrafficState.hh"
//#include "ControlStateTransition.hh"
//#include "state/StateGraph.hh"
//#include "travgraph/Graph.hh"
//#include "gcinterfaces/SegGoals.hh"
//#include "gcinterfaces/SegGoalsStatus.hh"
#include "PlanningHorizon.hh"


class CorridorGen {
public:
  CorridorGen() {}
  ~CorridorGen() {}

  static int makeCorridorLane(Corridor & corr, point2 currFrontPos, Map * localMap, bool isReverse);
  static int makeCorridorIntersection(Corridor & corr, point2 currFrontPos, Map * localMap, SegGoals currSegment);
  static int makeCorridorZone(Corridor & corr, point2 currRearPos, Map* localMap, SegGoals currSegment, SegGoals nextSegment);
  static int makeCorridorZonePause(Corridor & corr, VehicleState vehState);
  static int makeCorridorLaneChange(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, LaneLabel destLane, point2 startPos, int reverse);

  static int setFinalCondLK(Corridor &corr, point2 currFrontPos, Map* localMap, SegGoals segmentGoal, bool isReverse);
  static int setFinalCondStop(Corridor & corr, Map* localMap, SegGoals segmentGoal);
  static int setFinalCondStopped(Corridor & corr, VehicleState vehState);
  static int setFinalCondPause(Corridor & corr, VehicleState vehState, Map* localMap);
  static int setFinalCondLaneChange(Corridor &corr, point2 currFrontPos, Map* localMap, SegGoals segmentGoal, bool isReverse);


private:


  };
#endif /*CORRIDORGEN_HH_*/
