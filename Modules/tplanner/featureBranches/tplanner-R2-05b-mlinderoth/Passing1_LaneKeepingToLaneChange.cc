#include "Passing1_LaneKeepingToLaneChange.hh"
#include "Log.hh"

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange()
{

}

Passing1_LaneKeepingToLaneChange::~Passing1_LaneKeepingToLaneChange()
{

}

double Passing1_LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    Log::getStream(1) << "in Passing1_LaneKeepingToLaneChange::meetTransitionConditions" << endl;

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      LaneLabel currLane = AliceStateHelper::getDesiredLaneLabel();
      
      double delta_o = 30; // the obstacle distance in meters
      double distToObs = TrafficUtils::getNearestObsDist(map, vehState, currLane);
      bool obstacle_present = (distToObs > 0) && (distToObs < delta_o);

      if (obstacle_present) {

        SegGoals seg = planHorizon.getSegGoal(0);
        
        bool dist_ok = true;
        point2 stopLinePos;
        PointLabel label = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
        int stopLineErr = map->getNextStopline(stopLinePos, label);
        if (stopLineErr != -1) {
          Log::getStream(1) << "We have a stopline!" << endl;
          point2 obsPt;
          double dist_stopline;
          point2arr centerline;
          map->getLaneCenterLine(centerline, currLane);
          TrafficUtils::getNearestObsFarthermostPoint(obsPt, map, vehState, currLane, 0);
          Log::getStream(1) << "Obstacle point " << obsPt << " - Stopline point " << stopLinePos << endl;
          map->getDistAlongLine(dist_stopline, centerline, stopLinePos, obsPt);
          Log::getStream(1) << "Distance is " << dist_stopline << endl;
          if (dist_stopline > 0.0)
            dist_ok = dist_stopline > 20;
        }
        
        Log::getStream(1) << "--------------THINK THERE IS OBSTACLE PRESENT" << endl;
        
        MapElement obstacle;
        TrafficUtils::getNearestObsInLane(obstacle, map, vehState, currLane);
        bool is_static = TrafficUtils::isObsStatic(obstacle);
        
        // Check for blockage
        point2 exitPt;
        PointLabel exitWayptLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        map->getWayPoint(exitPt, exitWayptLabel);
        bool lane_blocked = TrafficUtils::isLaneBlocked(map, vehState, exitPt, currLane);

        string lt, rt;
        
        map->getLeftBoundType(lt, currLane);
        map->getRightBoundType(rt, currLane);
        
        bool passing_allowed = false;
        
        LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;
        
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;
        
        LaneLabel lane_left, lane_right;
        map->getNeighborLane(lane_left, currLane, -1);
        map->getNeighborLane(lane_right, currLane, 1);
        
        bool sameDirLeft = false;
        bool sameDirRight = false;
        vector<LaneLabel> sameDirLanes;
        map->getSameDirLanes(sameDirLanes, currLane);
        for (unsigned int i=0; i<sameDirLanes.size(); i++) {
          if (sameDirLanes[i] == lane_left)
            sameDirLeft = true;
          if (sameDirLanes[i] == lane_right)
            sameDirRight = true;
        }
        
        LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
        
        if (lane_left.lane>0 && (lt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
          desiredLane = lane_left;
          laneChange->setReverse(sameDirLeft?0:1);
          passing_allowed = true;
          Log::getStream(1) << "Can change lanes left to lane " << lane_left << endl;
          Log::getStream(1) << "Same direction left: " << sameDirLeft << endl;
        }
        
        if (lane_right.lane>0 && (rt.compare("broken_white") == 0 || seg.illegalPassingAllowed)) {
          desiredLane = lane_right;
          laneChange->setReverse(sameDirRight?0:1);
          passing_allowed = true;
          Log::getStream(1) << "Can change lanes right to lane " << lane_right << endl;
          Log::getStream(1) << "Same direction right: " << sameDirRight << endl;
        }
        
        Log::getStream(1) << "obs_pres:" << obstacle_present << " dist_ok:" << dist_ok << " allowed:" << passing_allowed << endl;
        
        CheckPass cpass;
        double p=cpass.getProbability(vehState,map);
        Log::getStream(1) << "Probability for passing..." << p << endl;
        
        if (dist_ok && is_static && lane_blocked && passing_allowed && p>=.70) {
          AliceStateHelper::setDesiredLaneLabel(desiredLane);
          m_prob = 1;
          Log::getStream(1) << "Transitioning to Passing1..." << endl;
        }
      }
    }
    break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing1_LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
