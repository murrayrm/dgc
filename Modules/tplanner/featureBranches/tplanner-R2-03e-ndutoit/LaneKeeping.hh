#ifndef LANEKEEPING_HH_
#define LANEKEEPING_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"
#include "CorridorGen.hh"

class LaneKeeping:public ControlState {

  public:

    LaneKeeping(int stateId, ControlStateFactory::ControlStateType type);
    ~LaneKeeping();
    int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
  void setReverse(bool reverse) { isReverse = reverse; }
  bool getReverse() { return isReverse; }

private:
  bool isReverse;
  ControlState* newCopy() {
    return  (ControlState*) new LaneKeeping(*this);
  }
};

#endif                          /*LANEKEEPING_HH_ */
