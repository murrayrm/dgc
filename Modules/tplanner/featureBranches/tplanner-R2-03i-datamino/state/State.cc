#include "State.hh"


State::State(int stateId, int type) 
  : m_stateId(stateId)
  , m_type(type)
  , m_uncertainty(0)
{
	
} 

State::State() 
{
//do nothing 	
} 

State::~State() 
{

} 

int State::getStateId() {
  return m_stateId; 
}

void State::setStateId(int stateId) {
  m_stateId=stateId; 
}

void State::setUncertainty(double uncertainty) {
  m_uncertainty = uncertainty; 
}

double State::getUncertainty(){
  return m_uncertainty;
}

int State::getType()
{
  return m_type;
}
