#ifndef STOP_HH_
#define STOP_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class Stop : public ControlState {


public: 

  Stop(int stateId, ControlStateFactory::ControlStateType type);
  ~Stop();
 virtual int determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* map);
  
private: 
  double m_desDecc;
};
#endif /*STOP_HH_*/
