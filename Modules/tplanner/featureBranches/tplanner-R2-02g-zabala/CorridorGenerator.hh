#ifndef CORRIDORGENERATOR_HH_
#define CORRIDORGENERATOR_HH_

//TODO: parse out these includes
#include "Corridor.hh"
#include "Conflict.hh"
#include "mapping/Segment.hh"
#include "ControlState.hh"
#include "ControlStateFactory.hh"
#include "TrafficState.hh"
#include "ControlStateTransition.hh"
#include "state/StateGraph.hh"
#include "interfaces/VehicleState.h"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "Interfaces.hh"

extern int DEBUG_LEVEL;


/*! Input interface that sends corridor create directives to the Corridor Control */
typedef GcInterface<CorridorCreate, CorridorCreateStatus, SNcorridorCreate, SNcorridorStatus, MODtrafficplanner> TrafficCorridorInterface ;

/*! Output interface that sends corridor create directives to the Trajectory Planner  */
typedef GcInterface<TrajectoryCreate, TrajectoryCreateStatus, SNtrajCreate, SNtrajCreateStatus, MODcorridorGenerator> CorrTrajPlannerInterface;

/*! Output interface that trajectory pause directives to the Trajectory Planner  */
typedef GcInterface<TrajectoryPause, TrajectoryPauseStatus, SNtrajPause, SNtrajPauseStatus, MODcorridorGenerator> CorrTrajPlannerPauseInterface;

/*! Output interface that trajectory pause directives to the Trajectory Planner  */
typedef GcInterface<TrajectoryEndMission, TrajectoryEndMissionStatus, SNtrajEndMission, SNtrajEndMissionStatus, MODcorridorGenerator> CorrTrajPlannerEndMissionInterface;


/*! The structure for merged directive that is "sent" from arbiter to control. 
 * Since the arbiter just passes through the directive MergedDirective has
 * the same structure as MContrDirective. */
//typedef TrafficPlannerDirective CorrGenMergedDirective;  


/*! The structure for status of merged directive. This status is "sent"
 *  from control back to arbiter. The structure for this is the same
 *  as MContrDirectiveStatus. */
//typedef TrafficPlannerDirectiveResponse CorrGenControlStatus;

class CorrGenControlStatus : public ControlStatus
{
public:
  /* Generally, status can be COMPLETED, FAILED, 
     or PENDING . You probably don't need the whole list or
     you may need other status such as when the control is still
     working on a given directive. 
     TODO: Redefine this. */
  enum CorrGenStatus{ COMPLETED, FAILED, READY_FOR_NEXT };
  
  unsigned ID; // The id of the merged directive that this control status corresponds to.
  CorrGenStatus status;
  CorrGenResponse::ReasonForFailure reason;
};

class CorrGenMergedDirective : public MergedDirective
{
public:
  unsigned ID;
  double number_arg;
};

/*! TODO = what is this exactly? The structure for response from corridor? */
struct CorrGenContrMsgResponse
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  CorrGenContrMsgResponse()
    :status(QUEUED)
  {
  }

  Status status;
  CorridorCreateStatus::ReasonForFailure reason;
};

/*! The structure for pairing up the directives sent to the
 *  corridor control (but in the language of the control) and the response
 *  to this directive. */
struct CorrGenContrMsgWrapper
{
  CorrGenContrMsgWrapper()
  {
    response.status = CorrGenContrMsgResponse::QUEUED;
  }
  CorridorCreate directive;  // corridorID is in here
  CorrGenContrMsgResponse response;
};


class CorridorGenerator : public GcModule {

public: 

  /*! Constructor */
  CorridorGenerator(int skynetKey, bool debug, bool verbose, bool log);

  /*! Destructor */
  virtual ~CorridorGenerator();

  /*! This is the function that continually runs the planner in a loop */
  void CorridorGeneratorLoop(void);

  
private :
  
  /*! Arbitration for the traffic planner control module. It computes the next
    merged directive based on the directives from mission control
    and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the traffic planner control module. It computes and sends
    directives to all its controlled modules based on the 
    merged directive and outputs the control status
    based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);
 
  /*! 
   * Given the corridor passed, determine the conflicts that may arise with respect to moving obstacles.  
   */
  Conflict evaluateDynamicConflicts(Corridor currCorridor, ControlState *controlState);
  
  /*! Choose best corridor */
  void evaluateCorridors();
  
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;
  
  /*!\param control status sent from control to arbiter */
  CorridorCreateStatus m_corrCreateStatus;
  
  /*!\param corridorID of control directive that corresponds to the completion of
   * m_mergedDirective */
  int m_mergedDirectiveCorridorID;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_prevMergedDirective */
  int m_prevMergedDirectiveCorridorID;

 /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<CorridorCreate> m_contrDirectiveQ;

  /*!\param the mutex to protect m_contrDirectiveQ */
  pthread_mutex_t m_contrDirectiveQMutex;

  /*!\param the goal corresponding to the end of mission */
  CorridorCreate m_endMissionGoal; 

  /*!\param control status sent from control to arbiter */
  CorrGenControlStatus m_controlStatus;
  /*!\param merged directive sent from arbiter to control */
  CorrGenMergedDirective m_mergedDirective; 

  /*!\param GcInterface variable */
  CorrTrajPlannerInterface::Southface* m_corrTrajPlannerSF;

  /*!\param GcInterface variable */
  CorrTrajPlannerPauseInterface::Southface* m_corrTrajPlannerPauseSF;

  /*!\param GcInterface variable */
  CorrTrajPlannerEndMissionInterface::Southface* m_corrTrajPlannerEndMissionSF;

  /*!\param GcInterface variable */
  TrafficCorridorInterface::Northface* m_traffCorrInterfaceNF;

  bool m_verbose;
  bool m_debug;
  bool m_log;

  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command
 
  std::vector<Conflict> m_conflicts;
  
};

#endif /*CORRIDORGENERATOR_HH_*/
