#ifndef INTERSECTION_LANEKEEPINGTOSTOP_HH_
#define INTERSECTION_LANEKEEPINGTOSTOP_HH_

#include "ControlStateTransition.hh"

class Intersection_LaneKeepingToStop: public ControlStateTransition {

  public:


    Intersection_LaneKeepingToStop(ControlState * state1, ControlState * state2);
    Intersection_LaneKeepingToStop();
    ~Intersection_LaneKeepingToStop();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:

    double m_probability;
    double m_desiredDecel;
    
};

#endif                          /*LANEKEEPINGTOSLOWDOWN_HH_ */
