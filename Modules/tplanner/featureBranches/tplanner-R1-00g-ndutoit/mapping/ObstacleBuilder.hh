#ifndef OBSTACLEBUILDER_HH_
#define OBSTACLEBUILDER_HH_


#include "Obstacle.hh"


class ObstacleBuilder { 

public:

ObstacleBuilder();
~ObstacleBuilder();
Obstacle* buildObstacle();

private: 

int getNextObstacleId();

int m_currObsId;	

};



#endif /*OBSTACLEBUILDER_HH_*/
