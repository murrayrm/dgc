#include "CorridorGen.hh"
#include <math.h>

int CorridorGen::makeCorridorLane(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, double range)
{
  bool m_verbose = false;
  bool m_debug = false;
  if ((m_verbose) || (m_debug)) {
    cout << "In LaneKeeping::determineCorridor " << endl;
  }

  //planning to next stop line?
  bool toStopLine = false;
  if (range == 0)
  {
    toStopLine = true;
    range = 50;
  bool m_verbose = false;
  bool m_debug = false;
  }
    
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  if ((m_verbose) || (m_debug)) {
    cout << "In CorridorGen::makeCorridorLane " << endl;
  }
        
  // We want to plan for current segment only
  // TODO: want to use function that does not specify specific lane
  LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
  int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
  if (getBoundsErr != 0) {
    cerr << "LaneKeeping.cc: boundary read from map error" << endl;
    return (getBoundsErr);
  }
        
  // Take some subset of these boundary points as the corridor
  // Find the closest pt on the boundary
  int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
  index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
  for (int ii = index; ii < (int) leftBound1.size(); ii++) {
    leftBound.push_back(leftBound1[ii]);
  }
        
  index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
  index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
  for (int ii = index; ii < (int) rightBound1.size(); ii++) {
    rightBound.push_back(rightBound1[ii]);
  }
        
  // Specify the velocity profile
  velIn = currSegment.maxSpeedLimit;
  velOut = currSegment.maxSpeedLimit;
  acc = 0;

  // specify the ocpParams final conditions - lower bounds
  // want to define this position based on the corridor?
  PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        
  int FCPos_Err = 0;

  //Changed this for road region so that we only plan as far as the map goes.
  FC_finalPos.set((rightBound.back().x+leftBound.back().x)/2,
      (rightBound.back().y+leftBound.back().y)/2);

  //If we are planning to stop line
  if (toStopLine)
    FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
  
  if (FCPos_Err != 0) {
    cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
    return (FCPos_Err);
  } 

  FC_velMin = 0;
  FC_headingMin = -M_PI;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = M_PI;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector < double >velProfile;
  velProfile.push_back(velIn);        // entry pt
  velProfile.push_back(velOut);       // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs - lower bounds
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  
  return 0;
}

int CorridorGen::makeCorridorTurn(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  bool m_verbose = false;
  bool m_debug = false;

  if ((m_verbose) || (m_debug)) {
    cout << "In LaneKeeping::determineCorridor " << endl;
  }
    
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  SegGoals segment1 = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // We want to plan over two segments
  SegGoals segment2 = planHoriz.getSegGoal(1);

  // Set of boundary points due to intersection lane
  //PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  //PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
  //int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
  //if (interBoundErr != 0) {
  //  cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
  //  return (interBoundErr);
  // }
        
  //get entry and exit lane labels
  LaneLabel laneLabel1(segment2.entrySegmentID, segment2.entryLaneID);
  LaneLabel laneLabel(segment1.exitSegmentID, segment1.exitLaneID);
  PointLabel ptLabel1(segment2.entrySegmentID, segment2.entryLaneID, segment2.entryWaypointID);
  point2 entryPos;
  int entryErr = localMap->getWaypoint(entryPos, ptLabel1);
  if (entryErr != 0) {
    cerr << "CorridorGen.cc: could not get entry waypoint" << endl;
    return entryErr;
  }

  int boundErr = localMap->getBounds(leftBound,rightBound,laneLabel,currFrontPos,1);
  int boundErr1 = localMap->getBounds(leftBound1,rightBound1,laneLabel1,entryPos,10);
  if (boundErr!=0){
    cerr << "CorridorGen.cc: exit lane read from map error" << endl;
    return boundErr;
  }     
  if (boundErr1!=0){
    cerr << "CorridorGen.cc: entry lane read from map error" << endl;
    return boundErr1;
  }

  point2 exitpt_left = leftBound[leftBound.size()-1];
  point2 exitpt_right = rightBound[rightBound.size()-1];
  point2 enterpt_left = leftBound1[0];
  point2 enterpt_right = rightBound1[0];
        
  double distleft = exitpt_left.dist(enterpt_left);
  double distright = exitpt_right.dist(enterpt_right);

  if (distleft<distright){
    //need to make sure intersection point is in a good place
    line2 lineA(rightBound[rightBound.size()-2],exitpt_right);
    line2 lineB(enterpt_right,rightBound[1]);
    point2 inter = lineA.intersect(lineB);
    if (inter.dist(exitpt_right) > distright || inter.dist(enterpt_right) > distright)
      inter = (exitpt_right+enterpt_right)/2;
    point2 mid = (exitpt_left+enterpt_left)/2;
    leftBound.push_back(mid);
    leftBound.connect(leftBound1);
    rightBound.push_back(inter);
    rightBound.connect_intersect(rightBound1);
  } else{
    line2 lineA(leftBound[leftBound.size()-2],exitpt_left);
    line2 lineB(enterpt_left,leftBound[1]);
    point2 inter = lineA.intersect(lineB);
    if (inter.dist(exitpt_left) > distleft || inter.dist(enterpt_left) > distleft)
      inter = (exitpt_left+enterpt_left)/2;
    point2 mid = (exitpt_right + enterpt_right)/2;
    leftBound.push_back(inter);
    leftBound.connect(leftBound1);
    rightBound.push_back(mid);
    rightBound.connect(rightBound1);
  }


  // Specify the velocity profile
  velIn = segment1.maxSpeedLimit;
  velOut = segment2.maxSpeedLimit;
  acc = 0;

  // Specify the ocpParams final conditions - lower bounds
  // Do we want to define this position based on the corridor?
  //  PointLabel exitPtLabel(nextSegGoal.exitSegmentID, nextSegGoal.exitLaneID, nextSegGoal.exitWaypointID);
  //int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
  //if (FCPos_Err != 0) {
  //    cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
  //    return (FCPos_Err);
  //}
  FC_finalPos.set((rightBound.back().x+leftBound.back().x)/2,
		  (rightBound.back().y+leftBound.back().y)/2);
  FC_velMin = 0;
  FC_headingMin = -M_PI;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = M_PI;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector < double >velProfile;
  velProfile.push_back(velIn);        // entry pt
  velProfile.push_back(velOut);       // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  return 0;
}

int CorridorGen::makeCorridorZone(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  bool m_verbose = false;
  bool m_debug = false;

  if ((m_verbose) || (m_debug)) {
    cout << "In LaneKeeping::determineCorridor " << endl;
  }
    
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // We want to plan for current segment only
  // TODO: want to do this properly in the future
        
  PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
  if ((m_verbose) || (m_debug)) {
    cout << "exit waypt label = " << exitWayptLabel << endl;
  }
        
  point2 exitWaypt;
  int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
  if (wayptErr != 0) {
    cerr << "LaneKeeping.cc: waypt read from map error" << endl;
    return (wayptErr);
  }

  if ((m_verbose) || (m_debug)) {
    cout << "exit waypt pos = " << exitWaypt << endl;
  }
        
  // Get our current position
  if ((m_verbose) || (m_debug)) {
    cout << "current pos = " << currFrontPos << endl;
  }
        
  // Create a corridor of LaneWidth from current position to entry point
  double corrHalfWidth = 3;
  double theta = atan2(exitWaypt.y - currFrontPos.y, exitWaypt.x - currFrontPos.x);
  point2 temppt;
        
  // Left Boundary
  // pt 1 on left boundary
  temppt.x = currFrontPos.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = currFrontPos.y + corrHalfWidth * sin(theta + M_PI / 2);
  leftBound.push_back(temppt);
  // pt 2 on left boundary
  temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
  leftBound.push_back(temppt);
  // Right Boundary
  // pt 1 on right boundary
  temppt.x = currFrontPos.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = currFrontPos.y + corrHalfWidth * sin(theta - M_PI / 2);
  rightBound.push_back(temppt);
  // pt 2 on right boundary
  temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
  rightBound.push_back(temppt);

  // TODO: Take some subset of these boundary points as the corridor

  // Specify the velocity profile
  velIn = 3;
  velOut = 3;
  acc = 0;

  // specify the ocpParams final conditions - lower bounds
  FC_finalPos = exitWaypt;
  FC_velMin = 0;
  FC_headingMin = -M_PI;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = M_PI;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector < double >velProfile;
  velProfile.push_back(velIn);        // entry pt
  velProfile.push_back(velOut);       // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  return 0;
}
