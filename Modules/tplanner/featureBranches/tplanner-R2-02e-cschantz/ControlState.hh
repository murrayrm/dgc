#ifndef CONTROLSTATE_HH_
#define CONTROLSTATE_HH_

#include "Corridor.hh"
#include "map/Map.hh"
#include "interfaces/VehicleState.h"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "state/State.hh"
#include "ControlStateFactory.hh"
#include <time.h>

class ControlState: public State {

  public:

    ControlState(int stateId, ControlStateFactory::ControlStateType type);
    ControlState();
    virtual ~ ControlState();
    virtual int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map) = 0;
	
    static void setOutputParams(bool debug, bool verbose, bool log);

    void setInitialPosition(VehicleState vehState);
    void setUpdatedPosition(VehicleState vehState);
    void setInitialVelocity(VehicleState vehState);
    void setInitialTime();
    point2 getInitialPosition();
    double getInitialVelocity();
    point2 getUpdatedPosition();
    double calcDistFromInitPos(VehicleState vehState);
    time_t getInitialTime();
    int getStateID();
    ControlStateFactory::ControlStateType getType();

  protected:
  
    static bool m_verbose;
    static bool m_debug;
    static bool m_log;

  private:

	point2 m_AliceInitPos;
    double m_AliceInitVel;
    point2 m_AliceUpdatePos;
    double m_currDistFromInitPos;
    time_t m_AliceInitTime;
    int m_stateID;
    ControlStateFactory::ControlStateType m_type;

};

#endif                          /*CONTROLSTATE_HH_ */
