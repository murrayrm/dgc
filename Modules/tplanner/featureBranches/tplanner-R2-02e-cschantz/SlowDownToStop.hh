#ifndef SLOWDOWNTOSTOP_HH_
#define SLOWDOWNTOSTOP_HH_

#include "ControlStateTransition.hh"


class SlowDownToStop: public ControlStateTransition {

  public:

    SlowDownToStop(ControlState * state1, ControlState * state2);
    ~SlowDownToStop();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon horiz, Map * map, VehicleState vehState);

  private:

    double m_distToStopTrans;
    const double m_desiredDecel;
    double m_distToObs;
    
};
#endif                          /*SLOWDOWNTOSTOP_HH_ */
