#include "CreepToStop.hh"
#include <math.h>
#include <list>

CreepToStop::CreepToStop(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

CreepToStop::CreepToStop()
{

}

CreepToStop::~CreepToStop()
{

}

double CreepToStop::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in CreepToStop::meetTransitionConditions" << endl;
    }

    switch (trafficState->getType()) {

    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel label;
        if (trafficState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY)
            label =  PointLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        else
            label = PointLabel(seg.entrySegmentID, seg.entryLaneID, seg.entryWaypointID);
        point2 stopLinePos;
        map->getNextStopline(stopLinePos, label);
        double distance = stopLinePos.dist(currFrontPos);

        if (m_verbose || m_debug) {
            cout << "Distance to stopline is " << distance << endl;
        }

        if (distance < 1)
            m_probability = 1;
        else
            m_probability = 0;
    }
        break;
        
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::ZONE_REGION:

        m_probability = 0;
        break;

    default:
    
        m_probability = 0;
        cerr << "CreepToStop.cc: Undefined Traffic state" << endl;
    
    }
    
    setUncertainty(m_probability);
    return m_probability;
}
