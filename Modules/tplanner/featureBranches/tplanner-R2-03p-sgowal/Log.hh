#ifndef LOG_HH_
#define LOG_HH_

#include <stdio.h>
#include <string>

#include "ControlStateFactory.hh"
#include "TrafficStateFactory.hh"
#include "ControlState.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "TrafficPlanner.hh"
#include "gcinterfaces/SegGoals.hh" 

using namespace std;

class Log {

  public:

    Log();
    ~Log();

    int init(string filename, bool logit);
    void logSegment(PlanningHorizon ph);
    void logTrafficState(TrafficState *state, ControlState *cstate, PlanningHorizon ph, Map *map);
    void logControlState(ControlState *state);
    void logString(string str);

  private:
  
    string file;
    FILE *fp;
    bool log;
    int control_id;
    int traffic_id;
    int segment_id;
    int open();
    void close();

};

#endif                          /*LOG_HH_ */

