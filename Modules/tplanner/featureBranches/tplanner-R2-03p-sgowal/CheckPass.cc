/*!CheckPass.cc
 * Author: Christian Looman
 * Last revision: May 09 2007
 * */

#include "CheckPass.hh"

using namespace std;

bool CheckPass::init=false;
vector<ListToA> CheckPass::VehicleList;
bool CheckPass::ClearToGo,CheckPass::LegalToGo,CheckPass::IntersectionCreated;
vector<PointLabel> CheckPass::WayPoint_WithStop,CheckPass::WayPoint_NoStop,CheckPass::WayPointEntries;
PointLabel CheckPass::StopLine_alice,CheckPass::StopLine_reverse;
double CheckPass::distance_stopline_alice,CheckPass::distance_reverse;
CMapElementTalker CheckPass::testMap;
MapElement CheckPass::alice;


void CheckPass::checkForObstacles(VehicleState vehState, Map* localMap, LaneLabel lane,int searchDirection, double& distance_obstacle,double& vel_obstacle, double& size_obstacle)
{
  // Calculate Alice position of Front Bumper
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);

  // Get Alice's lane information
  LaneLabel lane_alice;
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);
  localMap->getLane(lane_alice,position_alice);
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  vector<MapElement> obstacle;
  localMap->getObsInLane(obstacle,lane);
  distance_obstacle=10000;
  point2 position_obstacle;
  int id_obstacle=-1;

  double distance_temp;
  // Loop through all obstacles found in lane and find the one with the lowest distance
  for (unsigned int i=0;i<obstacle.size();i++)
    {
      if (obstacle[i].type!=ELEMENT_OBSTACLE && obstacle[i].type!=ELEMENT_VEHICLE) continue;

      // Transform from uncertain to certain position
      position_obstacle.set(obstacle[i].position);
      localMap->getDistAlongLine(distance_temp,centerline_alice,position_obstacle,position_alice);
      // Find lowest distance

      if (((searchDirection==AHEAD && distance_temp>0) || (searchDirection==BEHIND && distance_temp<0)) && fabs(distance_temp)<distance_obstacle)
	{
	  distance_obstacle=fabs(distance_temp);
	  id_obstacle=i;
	}
    }
  size_obstacle=6;
  vel_obstacle=0;

  if (id_obstacle!=-1)
    {
      if (obstacle[id_obstacle].length>obstacle[id_obstacle].width)
	size_obstacle=obstacle[id_obstacle].length;
      else
	size_obstacle=obstacle[id_obstacle].width;

      vel_obstacle=sqrt(pow(obstacle[id_obstacle].velocity.x,2)+pow(obstacle[id_obstacle].velocity.y,2));

      // The distance needs to be corrected to obtain the bumper2bumper distance
      distance_obstacle=distance_obstacle-size_obstacle/2;
    }
  else
    distance_obstacle=-1;
}


double CheckPass::getProbability(VehicleState vehState,Map* localMap)
{
  const int SAFETY_FACTOR=20;
  const double size_alice=VEHICLE_LENGTH;

  // COMPUTE ALICE'S VALUES
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);

  // Calculate Velocity over Ground
  double vel_alice=sqrt(vehState.utmNorthVel*vehState.utmNorthVel + vehState.utmEastVel*vehState.utmEastVel);
  // Get Alice's lane
  LaneLabel lane_alice;
  point2arr centerline_alice;
  localMap->getLane(lane_alice,position_alice);

  // Get general lane information whether to turn legally or illegaly
  vector<LaneLabel> SameDirLanes;
  bool LegalTurn;
  localMap->getSameDirLanes(SameDirLanes,lane_alice);
  if (SameDirLanes.size()>1)
    {
      LegalTurn=true;
      cout<<"Legal passing possible...true"<<endl;
    }
  else
    {
      cout<<"Legal passing possible...false"<<endl;
      LegalTurn=false;
    }

  // If illegal turn is necessary, check for oncoming traffic
  if (!LegalTurn)
    {

      // COMPUTE OBSTACLE1'S VALUES
      double distance_obstacle1;
      double vel_obstacle1;
      double size_obstacle1;
      checkForObstacles(vehState,localMap,lane_alice,AHEAD,distance_obstacle1,vel_obstacle1,size_obstacle1);
      // if no obstacle was found, return probability of .70
      if (distance_obstacle1==-1) return .70;

      // COMPUTE OBSTACLE2'S VALUES
      double distance_obstacle2;
      double vel_obstacle2;
      double size_obstacle2;
      LaneLabel lane_opposite;
      localMap->getNeighborLane(lane_opposite,lane_alice,-1);
      checkForObstacles(vehState,localMap,lane_opposite,AHEAD,distance_obstacle2,vel_obstacle2,size_obstacle2);
      // if no obstacle was found, return probability of .70
      if (distance_obstacle2==-1) return .70;

      double vel_difference=vel_alice-vel_obstacle1;
      // If obstacle is faster than Alice, return 0
      if (vel_difference<0) return 0;

      // the necessary time to pass a vehicle has two components:
      // 1) time to pass obstacle and be max 3*length of Alice ahead to make a safe lanechange back into own lane
      // 2) Uncertainties
      double corridor_alice=distance_obstacle1+size_obstacle1+3*size_alice;
      double t_1 = sqrt(2*corridor_alice/(0.7*VEHICLE_MAX_ACCEL));
      double t_2 = 0.5;
        
      double t_necessary=t_1+t_2;

      // The length of the obstacle's corridor is computed by computing the distance that the obstacle drives in the time
      // that Alice needs to pass it
      double corridor_obstacle2=vel_obstacle2*t_necessary;

      // Compute how much of the corridor on the opposite lane is left when Alice would pass the obstacle
      double corridor_left=distance_obstacle2-corridor_obstacle2-corridor_alice;
      // Output some results
      cout<<"Vehicle Speed..."<<vel_alice<<endl;
      cout<<"Position Alice..."<<position_alice<<endl;
      cout<<"Obstacle Distance own lane..."<<distance_obstacle1<<endl;
      cout<<"Obstacle velocity own lane..."<<vel_obstacle1<<endl;
      cout<<"Obstacle Distance opposite lane..."<<distance_obstacle2<<endl;
      cout<<"Obstacle velocity opposite lane..."<<vel_obstacle2<<endl;
      cout<<"necessary passing time..."<<t_necessary<<endl;
      cout<<"Corridor length Alice..."<<corridor_alice<<endl;
      cout<<"Corridor length obstacle2..."<<corridor_obstacle2<<endl;
      cout<<"Corridor left..."<<corridor_left<<endl;


      if (corridor_left<0)
	{
	  return 0;
	}
      else
	{
	  double ratio=(corridor_left/size_alice*SAFETY_FACTOR)/100;
	  cout<<"Ratio..."<<ratio<<endl;
	  return (1 < ratio)?1:ratio;
	}
    } 
  // Check for traffic on neighbor lanes check whether lanes are free
  else
    {
       for (unsigned int i=0;i<SameDirLanes.size();i++)
 	{
  	  if (SameDirLanes[i].lane!=lane_alice.lane)
  	    {
	      // COMPUTE OBSTACLE1'S VALUES
	      double distance_obstacle1;
	      double vel_obstacle1;
	      double size_obstacle1;
	      checkForObstacles(vehState,localMap,SameDirLanes[i],BEHIND,distance_obstacle1,vel_obstacle1,size_obstacle1);
	      // if no obstacle was found, return probability of .70
	      if (distance_obstacle1==-1) return .70;

	      if ((vel_obstacle1>0) && (distance_obstacle1/vel_obstacle1)<5)
		return 0;
	      else if ((vel_obstacle1>0) && (distance_obstacle1/vel_obstacle1)>10)
		return .70;
	      else
		return 1;

	      // Output some results
	      cout<<"Vehicle Speed..."<<vel_alice<<endl;
	      cout<<"Position Alice..."<<position_alice<<endl;
	      cout<<"Obstacle Distance neighbor lane..."<<distance_obstacle1<<endl;
	      cout<<"Obstacle velocity neighbor lane..."<<vel_obstacle1<<endl;
  	    }
 	}
    }
  return -1;
 }

bool CheckPass::checkIntersection(VehicleState vehState,Map* localMap, ControlState * cstate)
{
  // Update Alice state on testMap
  alice.set_alice(vehState);
  testMap.sendMapElement(&alice,sendChannel);

  cout<<"Intersection Control State...";
  ControlStateFactory::print(cstate->getType());

  if (!IntersectionCreated)
    createIntersection(vehState,localMap);
  
  if (IntersectionCreated)
    {
      // Check for current status of other obstacles. Only when Alice is still moving.
      checkExistenceObstacles(localMap,cstate);

      cout<<"Vehicles at intersection ..."<<VehicleList.size()<<endl;

      for (unsigned int m=0;m<VehicleList.size();m++)
	{
	  cout<<"--------------------------------"<<endl;
	  cout<<"ToA["<<m<<"] Id..."<<VehicleList[m].Id<<endl;
	  cout<<"ToA["<<m<<"] WayPoint..."<<VehicleList[m].WayPoint<<endl;
	  cout<<"ToA["<<m<<"] distance..."<<VehicleList[m].distance<<endl;
	  cout<<"ToA["<<m<<"] velocity..."<<VehicleList[m].velocity<<endl;
	  cout<<"ToA["<<m<<"] ETA..."<<VehicleList[m].eta<<endl;
	}
      
      // Alice needs to be stopped, then check for Precedence
      if (cstate->getType()==ControlStateFactory::STOPPED)
	{
	  LegalToGo=checkPrecedence();
	  cout<<"Alice stopped. Check Precedence..."<<LegalToGo<<endl;
	}
      // After it is legal to go, check for whether it's clear to go
      if (LegalToGo && !ClearToGo)
	  ClearToGo=checkClearance(localMap);
    }
  else
    {
      cout<<"Error: Couldn't build Intersection. But give ok for Legal&Clear to go."<<endl;
      cout<<"Next Stopline Alice..."<<StopLine_alice<<", distance "<<distance_stopline_alice<<endl;
      cout<<"Last Stopline Alice..."<<StopLine_reverse<<", distance "<<distance_reverse<<endl;
      LegalToGo=true;
      ClearToGo=true;
    }

  cout<<"Intersection legal to go..."<<LegalToGo<<endl;
  cout<<"Intersection clear to go..."<<ClearToGo<<endl;
 
  if (LegalToGo && ClearToGo)
    return true;
  else
    return false;
}

bool CheckPass::checkPrecedence()
{
  for (unsigned int i=0; i<VehicleList.size(); i++)
    {
      cout<<"Check Precedence for "<<VehicleList[i].Id<<endl;
      // If vehicle found with a very little ETA, then this vehicle arrived before Alice probably
      if (VehicleList[i].eta<ETA_EPS)
	{
	  return false;
	  cout<<"Has <ETA_EPS"<<endl;
	}
      else
	cout<<"this veh has to wait"<<endl;
    }

  // Otherwise return true
  return true;
}


void CheckPass::resetIntersection()
{
  cout<<"Reset Intersection"<<endl;
  LegalToGo=false;
  ClearToGo=false;
  IntersectionCreated=false;
  VehicleList.clear();

  testMap.initSendMapElement(skynetKey);

  init=true;
}

void CheckPass::checkExistenceObstacles(Map* localMap,ControlState * cstate)
{
  point2 p;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  point2 position_obstacle;
  double distance_temp;
  ListToA list_temp;
  int foundindex;
  vector<ListToA> tempVehicleList;
  
  MapElement obstacleMap;

  // Reset updated-flag
  for (unsigned int k=0; k<VehicleList.size(); k++)
    VehicleList[k].updated=false;

  // Look for obstacles at all stoplines
  for (unsigned int i=0; i<WayPoint_WithStop.size(); i++)
    {
      localMap->getWayPoint(p,WayPoint_WithStop[i]);
      localMap->getLane(lane,p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);
      
      cout<<"Looking at waypoint..."<<WayPoint_WithStop[i]<<". Number of obstacles found "<<obstacle.size()<<endl;

      // Loop through all obstacles found in lane
      for (unsigned int j=0;j<obstacle.size();j++)
	{
	  testMap.sendMapElement(&obstacle[j],sendChannel);

	  if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE)
	    {
	      cout<<"Obstacle["<<j<<"] is not a vehicle"<<endl;
	      continue;
	    }

	  // Transform from uncertain to certain position
	  position_obstacle.set(obstacle[j].position);
	  localMap->getDistAlongLine(distance_temp,centerline,p,position_obstacle);

	  if (distance_temp>0 && distance_temp!=INFINITY)
	    {
	      // Look whether vehicle already exists
	      foundindex=-1;
	      for (unsigned int k=0; k<VehicleList.size(); k++)
		{
		  if (VehicleList[k].Id==obstacle[j].id)
		    {
		      VehicleList[k].updated=true;
		      foundindex=k;
		      cout<<"Obstacle["<<j<<"] found in list"<<endl;
		    }
		}

	      // Don't add or update information when Alice stopped. We only wait until existing vehicles with ETA<epsilon are leaving intersection
	      // If vehicle is new, then add it to list
	      if (foundindex==-1 && cstate->getType()!=ControlStateFactory::STOPPED)
		{
		  if (distance_temp>0)
		    {
		      // Create list object
		      list_temp.Id=obstacle[j].id;
		      list_temp.WayPoint=WayPoint_WithStop[i];
		      list_temp.velocity=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));
		      list_temp.distance=distance_temp;
		      list_temp.updated=true;
		      time(&list_temp.time);
		      
		      // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<.5m/s
		      if (list_temp.velocity>=VELOCITY_OBSTACLE_THRESHOLD)
			list_temp.eta=list_temp.distance/list_temp.velocity;
		      else if (distance_temp>=DISTANCE_OBSTACLE_THRESHOLD)
			list_temp.eta=INFINITY;
		      else
			list_temp.eta=0;

		      
		      VehicleList.push_back(list_temp);
		      cout<<"Obstacle["<<j<<"] stored"<<endl;
		    }
		  else
		    cout<<"Obstacle["<<j<<"]... negative distance"<<endl;
		}
	      // Otherwise update vehicle's state
	      else if (cstate->getType()!=ControlStateFactory::STOPPED)
		{
		  time_t t,t2;
		  double delta_t,v,v2,a;
		  
		  time(&t);
		  v=VehicleList[foundindex].velocity;
		  
		  t2=VehicleList[foundindex].time;
		  
		  v2=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));;
		  delta_t=difftime(t2,t);
		  
		  // Update VehicleList
		  VehicleList[foundindex].velocity=v2;
		  VehicleList[foundindex].distance=distance_temp;
		  VehicleList[foundindex].updated=true;
		  time(&VehicleList[foundindex].time);
		  
		  int debug=-1;
		  // s=0.5 * a * t * t
		  if ((t2-t)!=0)
		    a=(v2-v)/(t2-t);
		  else
		    a=0;
		  
		  if (a!=0)
		    {
		      if ((distance_temp-DISTANCE_STOPLINE_OBSTACLE)>=0)
			{
			  VehicleList[foundindex].eta=sqrt(2*(distance_temp-DISTANCE_STOPLINE_OBSTACLE)/fabs(a));
			  debug=1;
			}
		      else if (distance_temp!=0)
			{
			  VehicleList[foundindex].eta=sqrt(2*distance_temp/fabs(a));
			  debug=2;
			}
		      else
			{
			  VehicleList[foundindex].eta=0;
			  debug=3;
			}
		    }
		  else if (v2>=VELOCITY_OBSTACLE_THRESHOLD)
		    if ((distance_temp-DISTANCE_STOPLINE_OBSTACLE)>=0)
		      {
			VehicleList[foundindex].eta=(distance_temp-DISTANCE_STOPLINE_OBSTACLE)/v2;
			debug=4;
		      }
		    else if (distance_temp!=0)
		      {
			VehicleList[foundindex].eta=distance_temp/v2;
			debug=5;
		      }
		    else
		      {
			VehicleList[foundindex].eta=0;
			debug=6;
		      }
		  else if (distance_temp>=DISTANCE_OBSTACLE_THRESHOLD)
		    {
		      VehicleList[foundindex].eta=INFINITY;
		      debug=7;
		    }
		  else
		    {
		      VehicleList[foundindex].eta=INFINITY;
		      debug=8;
		    }

		  if (isnan(VehicleList[foundindex].eta))
		    {
		      cout<<"ETA IS NAN"<<endl;
		      cout<<"t..."<<t<<endl;
		      cout<<"t2.."<<t2<<endl;
		      cout<<"v..."<<v<<endl;
		      cout<<"v2..."<<v2<<endl;
		      cout<<"a..."<<a<<endl;
		      cout<<"distance_temp..."<<distance_temp<<endl;
		      cout<<"debug code..."<<debug<<endl;
		    }
		  if (VehicleList[foundindex].eta==0)
		    {
		      cout<<"ETA==0 BECAUSE Debug code..."<<debug<<endl;
		      cout<<"t..."<<t<<endl;
		      cout<<"t2.."<<t2<<endl;
		      cout<<"v..."<<v<<endl;
		      cout<<"v2..."<<v2<<endl;
		      cout<<"a..."<<a<<endl;
		      cout<<"distance_temp..."<<distance_temp<<endl;
		    }
		  if (VehicleList[foundindex].eta>15)
		    {
		      cout<<"LARGE ETA BECAUSE Debug code..."<<debug<<endl;
		      cout<<"t..."<<t<<endl;
		      cout<<"t2.."<<t2<<endl;
		      cout<<"v..."<<v<<endl;
		      cout<<"v2..."<<v2<<endl;
		      cout<<"a..."<<a<<endl;
		      cout<<"distance_temp..."<<distance_temp<<endl;
		    }
		}
	    }
	}
    }
  // Clean up VehicleList
  for (unsigned int k=0; k<VehicleList.size(); k++)
    {
      if (VehicleList[k].updated==false)
	{
	  // Clear element out of map
	  obstacleMap.setId(VehicleList[k].Id);
	  obstacleMap.setTypeClear();
	  testMap.sendMapElement(&obstacleMap,sendChannel);

	  cout<<"Remove obstacle at WayPoint..."<<VehicleList[k].WayPoint<<endl;
	}
      else
	tempVehicleList.push_back(VehicleList[k]);
    }

  VehicleList=tempVehicleList;
}


bool CheckPass::checkClearance(Map* localMap)
{
  vector<PointLabel> WayPointExits;
  point2 p_entry,p_exit,position_obstacle;
  LaneLabel lane_entry,lane_exit;
  vector<MapElement> obstacles;
  double distance_entry,distance_exit;
  point2arr centerline;

  // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
  for (unsigned i=0; i<WayPointEntries.size(); i++)
    {
      localMap->getWaypoint(p_entry,WayPointEntries[i]);
      localMap->getLane(lane_entry,p_entry);

      // get all exits for this entry
      localMap->getWayPointExits(WayPointExits,WayPointEntries[i]);
      for (unsigned j=0;j<WayPointExits.size(); j++)
	{
	  localMap->getWaypoint(p_exit,WayPointExits[j]);
	  localMap->getLane(lane_exit,p_exit);
	  
	  // If Entry/Exit-lane is the same, check whether there are obstacles in between
	  if (lane_entry==lane_exit)
	    {
	      localMap->getLaneCenterLine(centerline,lane_entry);

	      localMap->getObsInLane(obstacles,lane_entry);
	      for (unsigned int k=0; k<obstacles.size(); k++)
		{
		  position_obstacle.set(obstacles[k].position);
		  localMap->getDistAlongLine(distance_entry,centerline,p_entry,position_obstacle);
		  localMap->getDistAlongLine(distance_exit,centerline,p_exit,position_obstacle);

		  // If obstacle is in between Entry and Exit, return false!
		  if ((distance_entry+DISTANCE_STOPLINE_OBSTACLE_EPS)<0 && (distance_exit-DISTANCE_STOPLINE_OBSTACLE_EPS)>0)
		    {
		      // send obstacle that blocks the lane; DO I NEED TO DELETE IT LATER AGAIN???
		      obstacles[k].setColor(MAP_COLOR_RED,100);
		      testMap.sendMapElement(&obstacles[k],sendChannel);
 		      cout<<"Intersection...blocked by obstacle"<<endl;
		      return false;
		    }
		}
	    }
	}
    }

  return true;
}

void CheckPass::populateWayPoints(Map* localMap, vector<PointLabel>& WayPointEntries, PointLabel InitialWayPointEntry)
{
  vector<PointLabel> WayPointExits;
  vector<PointLabel> WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++)
    {
      localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++)
	{
	  bool found=false;
	  for (unsigned int k=0; k<WayPointEntries.size(); k++)
	    {
	      if (WayPoint[j]==WayPointEntries[k])
		{
		  found=true;
		  break;
		}
	    }

	  // If not, add it
	  if (!found)
	    {
	      WayPointEntries.push_back(WayPoint[j]);
	      populateWayPoints(localMap,WayPointEntries,WayPoint[j]);
	    }
	}
    }
}


void CheckPass::findStoplines(Map* localMap, vector<PointLabel>& WayPoint, vector<PointLabel>& WayPoint_NoStop, vector<PointLabel>& WayPoint_WithStop)
{
  for (unsigned int i=0; i<WayPoint.size(); i++)
    {
      if (localMap->isStopLine(WayPoint[i]))
	WayPoint_WithStop.push_back(WayPoint[i]);
      else
	WayPoint_NoStop.push_back(WayPoint[i]);
    }
}

void CheckPass::createIntersection(VehicleState vehState,Map* localMap)
{
  vector<PointLabel> WayPoint;
  vector<PointLabel> StopLines;
  point2 p;
  double distance_temp;

  // Obtain Alice's position
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);
  LaneLabel lane_alice;
  localMap->getLane(lane_alice,position_alice);

  // get Alice's centerline
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  if (localMap->getLaneStopLines(StopLines,lane_alice)!=0)
    {
      distance_stopline_alice=INFINITY;
      distance_reverse=INFINITY;

      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      for (unsigned int i=0; i<StopLines.size(); i++)
	{
	  localMap->getWaypoint(p,StopLines[i]);
	  localMap->getDistAlongLine(distance_temp,centerline_alice,p,position_alice);

	  if (distance_temp>0 && distance_temp<distance_stopline_alice)
 	  {
	    distance_stopline_alice=distance_temp;
	    StopLine_alice=StopLines[i];

	    // If first Stopline was found that is ahead of Alice, set IntersectionCreated=true
	    IntersectionCreated=true;
	  }

	  if (distance_temp<0 && fabs(distance_temp)<distance_reverse)
	    {
	      distance_reverse=fabs(distance_temp);
	      StopLine_reverse=StopLines[i];
	    }
	}
      if (distance_stopline_alice<INFINITY)
	{
	  // get all entry points of the intersection
	  populateWayPoints(localMap,WayPointEntries,StopLine_alice);
	  findStoplines(localMap,WayPointEntries,WayPoint_NoStop,WayPoint_WithStop);
	}
    }
}















