#ifndef STOPOBSTOSTOPPEDOBS_HH_
#define STOPOBSTOSTOPPEDOBS_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class StopObsToStoppedObs:public ControlStateTransition {

  public:

    StopObsToStoppedObs(ControlState * state1, ControlState * state2);
    StopObsToStoppedObs();
    ~StopObsToStoppedObs();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:

    double m_boundStoppedVel;

};

#endif                          /*STOPOBSTOSTOPPEDOBS_HH_ */
