#ifndef INTERSECTIONHANDLING_HH
#define INTERSECTIONHANDLING_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
//run gsl-config --libs,--cflags tomorrow to see if its installed; then put 
//these variables in your makefile so that it will run
#include <cv.h>
#include <highgui.h>
#include <time.h>
//open cv stuff

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
//#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "state/AliceStateHelper.hh"
#include "ControlState.hh"
#include "ControlStateFactory.hh"
#include "TrafficUtils.hh"
#include "cmdline.h"
#include "Log.hh"
#include "sys/time.h"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

class ListToA
{
public:
  PointLabel WayPoint;
  MapElement element;
  double velocity;
  time_t time;
  double distance;
  double eta;
  bool precedence;
  bool closest;
  bool checkedQueuing;
  bool updated;
};

class ListBlockedObstacles
{
public:
  MapElement obstacle;
  bool updated;
};

class IntersectionHandling
{ 
public:
  enum IntersectionReturn { INTERSECTION_JAMMED = -1, INTERSECTION_WAIT = 0, INTERSECTION_GO = 1 };
  // Functions for dyn. obstacles at intersections
  static void loadConfiguration();
  static void resetIntersection(Map* localMap);
  static IntersectionReturn checkIntersection(VehicleState vehState,Map* localMap,SegGoals currSegment);
  static void createIntersection(VehicleState vehState,Map* localMap);
  static void populateWayPoints(Map* localMap, PointLabel InitialWayPointEntry);
  static void findStoplines(Map* localMap);
  static void checkExistenceObstacles(Map* localMap,VehicleState vehState);
  static bool checkPrecedence();
  static bool checkMerging(Map* localMap,SegGoals currSegment);
  static IntersectionReturn checkClearance(Map* localMap,SegGoals currSegment);
  static bool checkVisibility(Map* localMap, VehicleState Alice,MapElement obstacle);
  static bool getIntersectionSafetyFlag(); 
  static bool getIntersectionCabmodeFlag();
  static int getCountPrecedence();
  static bool getLegalToGo();
  static bool getPossibleToGo();
  static int getClearToGo();

  static bool init;

private:
  // Variables for checking the intersection
  static vector<ListToA> VehicleList;
  static vector<ListBlockedObstacles> BlockedObstacles;
  static bool LegalToGo;
  static bool LegalToGoNoReset;
  static bool PossibleToGo;
  static bool PossibleToGoNoReset;
  static IntersectionReturn ClearToGo;
  static IntersectionReturn ClearToGoNoReset;
  static bool IntersectionCreated;
  static time_t timestamp;
  static vector<PointLabel> WayPoint_WithStop;
  static vector<PointLabel> WayPoint_NoStop;
  static vector<PointLabel> WayPointEntries;
  static PointLabel StopLine_alice;
  static PointLabel StopLine_reverse;
  static double distance_stopline_alice;
  static double distance_reverse;
  static bool JammedTimerStarted;

  static double DISTANCE_STOPLINE_OBSTACLE;
  static double DISTANCE_STOPLINE_ALICE;
  static double VELOCITY_OBSTACLE_THRESHOLD;
  static double DISTANCE_OBSTACLE_THRESHOLD;
  static double ETA_EPS;
  static bool INTERSECTION_SAFETY_FLAG;
  static bool INTERSECTION_CABMODE_FLAG;
  static double TRACKING_VEHICLE_SEPERATION;

  static CMapElementTalker testMap;

  static const int sendChannel=-5;
  static MapElement alice;
};

#endif  // INTERSECTIONHANDLING_HH
