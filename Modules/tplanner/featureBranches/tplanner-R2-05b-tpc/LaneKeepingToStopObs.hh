#ifndef LANEKEEPINGTOSTOPOBS_HH_
#define LANEKEEPINGTOSTOPOBS_HH_

#include "ControlStateTransition.hh"

class LaneKeepingToStopObs:public ControlStateTransition {

  public:


    LaneKeepingToStopObs(ControlState * state1, ControlState * state2);
    LaneKeepingToStopObs();
    ~LaneKeepingToStopObs();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:

    double m_prob;
    double m_desiredDecel;
    double m_distToObs;
  double m_minDistToObs;
};

#endif                          /*LANEKEEPINGTOSTOPOBS_HH_ */
