#include "Console.hh"
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdarg.h>
#include "frames/point2.hh"
#include "map/Map.hh"
#include "IntersectionHandling.hh"

/* Properties of state machine window */
#define FSM_WIN_X      0
#define FSM_WIN_Y      1
#define FSM_WIN_HEIGHT 6
#define FSM_WIN_WIDTH  40

/* Properties of the corridor window */
#define CORR_WIN_X      FSM_WIN_X
#define CORR_WIN_Y      (FSM_WIN_Y + FSM_WIN_HEIGHT)
#define CORR_WIN_HEIGHT 9
#define CORR_WIN_WIDTH  FSM_WIN_WIDTH

/* Properties of the state window */
#define STATE_WIN_X      (FSM_WIN_X + FSM_WIN_WIDTH)
#define STATE_WIN_Y      FSM_WIN_Y
#define STATE_WIN_HEIGHT 10
#define STATE_WIN_WIDTH  40

/* Properties of the inter window */
#define INTER_WIN_X      (FSM_WIN_X + FSM_WIN_WIDTH)
#define INTER_WIN_Y      (STATE_WIN_Y + STATE_WIN_HEIGHT)
#define INTER_WIN_HEIGHT 8
#define INTER_WIN_WIDTH  40

/* Properties of the message window */
#define MSG_WIN_X      FSM_WIN_X
#define MSG_WIN_Y      (INTER_WIN_Y + INTER_WIN_HEIGHT)
#define MSG_WIN_HEIGHT 8
#define MSG_WIN_WIDTH  (STATE_WIN_X - FSM_WIN_X + STATE_WIN_WIDTH)
#define MAX_MSG        (MSG_WIN_HEIGHT - 2)
#define MSG_LENGTH     (MSG_WIN_WIDTH-3)

WINDOW *Console::fsm_win = NULL;
WINDOW *Console::corr_win = NULL;
WINDOW *Console::state_win = NULL;
WINDOW *Console::inter_win = NULL;
WINDOW *Console::msg_win = NULL;
char Console::messages[MAX_MSG][MSG_LENGTH];
bool Console::initialized = false;

void Console::init()
{
  initialized = true;

  initscr();
  noecho();
  cbreak();
  curs_set(0);
  refresh();
    
  attron(A_BOLD);
  mvprintw(0, 0, " TPlanner%71c", ' ');
  attroff(A_BOLD);

  /* Create a window for the state machines */
  fsm_win = newwin(FSM_WIN_HEIGHT, FSM_WIN_WIDTH, FSM_WIN_Y, FSM_WIN_X);
  wborder(fsm_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(fsm_win);
  mvwprintw(fsm_win, 0, 2, " State Machines ");
  mvwprintw(fsm_win, 2, 2, "Traffic State: ");
  mvwprintw(fsm_win, 3, 2, "Control State: ");
  wrefresh(fsm_win);

  /* Create a window for the Corridor */
  corr_win = newwin(CORR_WIN_HEIGHT, CORR_WIN_WIDTH, CORR_WIN_Y, CORR_WIN_X);
  wborder(corr_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(corr_win);
  mvwprintw(corr_win, 0, 2, " Corridor ");
  attron(A_UNDERLINE);
  mvwprintw(corr_win, 2, 11, "Initial");
  mvwprintw(corr_win, 2, 23, "Final");
  attroff(A_UNDERLINE);
  mvwprintw(corr_win, 3, 9, "X");
  mvwprintw(corr_win, 4, 9, "Y");
  mvwprintw(corr_win, 5, 2, "Velocity");
  mvwprintw(corr_win, 6, 3, "Heading");
  wrefresh(corr_win);

  /* Create a window for the state */
  state_win = newwin(STATE_WIN_HEIGHT, STATE_WIN_WIDTH, STATE_WIN_Y, STATE_WIN_X);
  wborder(state_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(state_win);
  mvwprintw(state_win, 0, 2, " Alice State ");
  mvwprintw(state_win, 2, 2, "Position: ");
  mvwprintw(state_win, 3, 2, "Velocity: ");
  mvwprintw(state_win, 5, 2, "Desired Lane: ");
  mvwprintw(state_win, 6, 2, "Queueing: ");
  mvwprintw(state_win, 7, 2, "Turning: ");
  wrefresh(state_win);

  /* Create a window for the intersection */
  inter_win = newwin(INTER_WIN_HEIGHT, INTER_WIN_WIDTH, INTER_WIN_Y, INTER_WIN_X);
  wborder(inter_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(inter_win);
  mvwprintw(inter_win, 0, 2, " Intersection ");
  mvwprintw(inter_win, 1, 2, "Safety flag: ");
  mvwprintw(inter_win, 2, 2, "Cab mode flag: ");
  mvwprintw(inter_win, 3, 2, "Veh with precedence: ");
  mvwprintw(inter_win, 4, 2, "Legal to go: ");
  mvwprintw(inter_win, 5, 2, "Poss to go: ");
  mvwprintw(inter_win, 6, 2, "Clear to go: ");
  wrefresh(inter_win);

  /* Create a window for the messages */
  msg_win = newwin(MSG_WIN_HEIGHT, MSG_WIN_WIDTH, MSG_WIN_Y, MSG_WIN_X);
  wborder(msg_win, '|', '|', '-', '-', '+', '+', '+', '+');
  wrefresh(msg_win);
  mvwprintw(msg_win, 0, 2, " Messages ");
  wrefresh(msg_win);
  for (int i=0; i<MAX_MSG; i++) {
    messages[i][0] = '\0';
  }
}

void Console::refresh()
{
  if (!initialized) return;

  touchwin(stdscr);
  touchwin(fsm_win);
  touchwin(corr_win);
  touchwin(msg_win);
  touchwin(state_win);
  touchwin(inter_win);
  wrefresh(stdscr);
  wrefresh(fsm_win);
  wrefresh(corr_win);
  wrefresh(state_win); 
  wrefresh(inter_win);
  wrefresh(msg_win);
}

void Console::destroy()
{
  if (!initialized) return;

  delwin(fsm_win);
  delwin(corr_win);
  delwin(state_win);
  delwin(inter_win);
  delwin(msg_win);
  endwin();
}

void Console::updateCorridor(Corridor &corr)
{
  if (!initialized) return;

  point2 initial, final, tmp;
  corr.getOCPinitialPos(initial, tmp);
  corr.getOCPfinalPos(final, tmp);

  double ivel = corr.getOCPinitialCondLB(VELOCITY_IDX_C);
  double fvel = corr.getOCPfinalCondLB(VELOCITY_IDX_C);

  double iheading = corr.getOCPinitialCondLB(HEADING_IDX_C);
  double fheading = corr.getOCPinitialCondLB(HEADING_IDX_C);

  mvwprintw(corr_win, 3, 11, "%7.2f", initial.x); /* Initial - X */
  mvwprintw(corr_win, 4, 11, "%7.2f", initial.y); /* Initial - Y */
  mvwprintw(corr_win, 5, 11, "%7.2f", ivel);      /* Initial - Velocity */
  mvwprintw(corr_win, 6, 11, "%7.2f", iheading);  /* Initial - Heading */
  mvwprintw(corr_win, 3, 23, "%7.2f", final.x);   /* Final - X */
  mvwprintw(corr_win, 4, 23, "%7.2f", final.y);   /* Final - Y */
  mvwprintw(corr_win, 5, 23, "%7.2f", fvel);      /* Final - Velocity */
  mvwprintw(corr_win, 6, 23, "%7.2f", fheading);  /* Final - Heading */
  wrefresh(corr_win);
}

void Console::updateState(VehicleState &vehState)
{
  if (!initialized) return;

  point2 pos = AliceStateHelper::getPositionRearAxle(vehState);
  double vel = AliceStateHelper::getVelocityMag(vehState);
  LaneLabel lane = AliceStateHelper::getDesiredLaneLabel();

  stringstream str;
  str << lane;

  mvwprintw(state_win, 2, 16, "[ %7.2f, %7.2f ]", pos.x, pos.y); /* Position */
  mvwprintw(state_win, 3, 16, "%.2f", vel);                      /* Velocity */
  mvwprintw(state_win, 5, 16, "%s", str.str().c_str());          /* Desired Lane */
  wrefresh(state_win);
}

void Console::updateInter()
{
    if (!initialized) return;

    mvwprintw(inter_win, 1, 24, "%i", IntersectionHandling::getIntersectionSafetyFlag() ); /* Safety flag set? */
    mvwprintw(inter_win, 2, 24, "%i", IntersectionHandling::getIntersectionCabmodeFlag() ); /* Cabmode flag set? */
    mvwprintw(inter_win, 3, 24, "%i", IntersectionHandling::getCountPrecedence() ); /* Vehicles with Precedence */
    mvwprintw(inter_win, 4, 24, "%i", IntersectionHandling::getLegalToGo() ); /* Is it legal to go? */
    mvwprintw(inter_win, 5, 24, "%i", IntersectionHandling::getPossibleToGo() ); /* Is it possible to go? */
    mvwprintw(inter_win, 6, 24, "%i", IntersectionHandling::getClearToGo() ); /* Is it clear to go? */
    wrefresh(inter_win);
}

void Console::updateQueueing(bool queueing)
{
  if (!initialized) return;

  mvwprintw(state_win, 6, 16, "%d", queueing);                  /* Queueing */
  wrefresh(fsm_win);
}

void Console::updateTurning(int direction)
{
  if (!initialized) return;

  if (direction == -1)
    mvwprintw(state_win, 7, 16, "<--");                  /* Turning */
  else if (direction == 1)
    mvwprintw(state_win, 7, 16, "-->");
  else
    mvwprintw(state_win, 7, 16, "   ");
  wrefresh(fsm_win);
}

void Console::updateTrafficState(TrafficState *state)
{
  if (!initialized) return;

  string str = TrafficStateFactory::printString(state->getType());
  mvwprintw(fsm_win, 2, 17, "%-21s", str.c_str());       /* Traffic State */
  wrefresh(fsm_win);
}

void Console::updateControlState(ControlState *state)
{
  if (!initialized) return;

  string str = ControlStateFactory::printString(state->getType());
  mvwprintw(fsm_win, 3, 17, "%-21s", str.c_str());      /* Control State */
  wrefresh(fsm_win);
}

void Console::addMessage(char *format, ...)
{
  static char msg[MSG_LENGTH];

  if (!initialized) return;

  va_list valist;
  va_start(valist, format);
  vsnprintf(msg, MSG_LENGTH, format, valist);
  va_end(valist);

  for (int i = 0; i < MAX_MSG; i++) {
    strncpy(messages[i], messages[i+1], MSG_LENGTH-1);
    messages[i][MSG_LENGTH-1] = '\0';
  }
  strncpy(messages[MAX_MSG-1], msg, MSG_LENGTH-1);
  messages[MAX_MSG-1][MSG_LENGTH-1] = '\0';
  for (int i=0; i<MAX_MSG; i++) {
    mvwprintw(msg_win, 1+i, 2, "%-76s", messages[i]);
  }
  wrefresh(msg_win);
}
