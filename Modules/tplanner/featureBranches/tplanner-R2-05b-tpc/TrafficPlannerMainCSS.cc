#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "TrafficManager.hh"
#include "CorridorGenerator.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include "skynet/skynet.hh"
#include "iostream"
#include <sys/time.h>
#include <sys/stat.h> 
#include "CmdArgs.hh"

using namespace std;             
     
int main(int argc, char **argv)              
{
  gengetopt_args_info cmdline;    
 

  /* Figure out where logged data go */  
  string logFileName;

  CmdArgs::sn_key = skynet_findkey(argc, argv);
 
  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);

  CmdArgs::console = !cmdline.disable_console_given;

  if (cmdline.verbose_given) {
    CmdArgs::verbose_level = cmdline.verbose_arg;
    if (CmdArgs::verbose_level > 0) CmdArgs::debug = true;
  }
    
  // Initialize the map with rndf if given
  if (cmdline.rndf_given){
    CmdArgs::use_RNDF = true;
    CmdArgs::RNDF_file = cmdline.rndf_arg;
    if (!CmdArgs::console)
      cout << "RNDF Filename in = "  << CmdArgs::RNDF_file << endl;
    // Load the RNDF in the constructor of TrafficManager 
  }     

  CmdArgs::logging = cmdline.log_given;
  CmdArgs::log_path = cmdline.log_path_arg;

  if(CmdArgs::logging) {
    string tmpRNDFname;
  
    ostringstream oss;
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    tmpRNDFname.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
    oss << CmdArgs::log_path << "tplanner-" << tmpRNDFname << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";

    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
  }

  if (!CmdArgs::console){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;
    cout << "debug = " << CmdArgs::debug << endl;
    cout << "verbose level = " << CmdArgs::verbose_level << endl;
  }

  CmdArgs::lane_cost = true;

  TrafficManager* traffManager = new TrafficManager(logFileName.c_str(), dgcFindConfigFile(cmdline.config_arg, "tplanner")); 
  sleep(10); 

  traffManager->Start();

  while (true) {
    sleep(1);
  }

  return 0;
}

