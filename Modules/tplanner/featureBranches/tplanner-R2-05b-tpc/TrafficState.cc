#include "TrafficState.hh"

TrafficState::TrafficState(int stateId, TrafficStateFactory::TrafficStateType type)
: State(stateId, type)
{
  m_stateID = stateId;
  m_type = type;
}

TrafficState::TrafficState()
  : State()
  , m_stateID(-1)
{ 

}

TrafficState::~TrafficState()
{ 

}

TrafficState& TrafficState::operator=(const TrafficState& ts) 
{
  m_stateID = ts.m_stateID;
  m_type = ts.m_type;
  return (*this); 
}


TrafficState::TrafficState(const TrafficState& ts) 
: State() 
{
  m_stateID = ts.m_stateID;
  m_type = ts.m_type;
}

TrafficStateFactory::TrafficStateType TrafficState::getType()
{
    return m_type;
}

int TrafficState::getStateID()
{
    return m_stateID;
}

string TrafficState::toString() 
{

  string traffStateType;
  switch(m_type) {
  case TrafficStateFactory::ROAD_REGION:
    traffStateType = "Type = ROAD_REGION  ";
    break;
  case TrafficStateFactory::ZONE_REGION:
    traffStateType = "Type = ZONE_REGION  ";
    break;
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    traffStateType = "Type = APPROACH_INTER_SAFETY ";
    break;
  case TrafficStateFactory::INTERSECTION_STOP:
    traffStateType = "Type = INTERSECTION_STOP ";
    break;
  default: 
    break;
  };
 
  return traffStateType;
}
