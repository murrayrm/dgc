#include "LaneKeepingToStop.hh"
#include <math.h>
#include <list>
#include "Log.hh"
#include "Console.hh"

/**
 * This class handles the transition from LaneKeepingToStop when approaching an intersection
 * In case of an obstacle, the standard LaneKeepingToStop is used!!!
 */

LaneKeepingToStop::LaneKeepingToStop(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
, m_minDelta(10)
{

}

LaneKeepingToStop::LaneKeepingToStop()
{

}

LaneKeepingToStop::~LaneKeepingToStop()
{

}

double LaneKeepingToStop::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon horiz, Map * localMap, VehicleState vehState)
{

    switch (trafficState->getType()) {
    
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        Log::getStream(1) << "in LaneKeepingToStop" << endl;

        point2 stopLinePos, currFrontPos;
        PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
        currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double delta = -pow(currVel, 2) / (2 * m_desiredDecel);

	      int stopLineErr = localMap->getNextStopline(stopLinePos, label);
        if (stopLineErr == -1) {
            m_probability = 0;
            break;
        }
	      double distToStopLine;
      	point2arr centerline;
      	LaneLabel laneLabel(label.segment, label.lane);
      	localMap->getLaneCenterLine(centerline, laneLabel);
      	localMap->getDistAlongLine(distToStopLine, centerline, stopLinePos, currFrontPos);

        Log::getStream(1) << "Distance to stopline = " << distToStopLine << "(<= "<<delta<<")"<<endl;
	//        Console::addMessage("LaneKeepingToStop: Distance to stopline = %.2f", distToStopLine);

        if ((distToStopLine <= delta && distToStopLine > 0.0)  || (distToStopLine <= m_minDelta && distToStopLine > 0.0)){
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;

    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
    case TrafficStateFactory::ROAD_REGION:
   
        m_probability = 0;
        break;
    
    default:
    
        m_probability = 0;
        cerr << "LaneKeepingToStop.cc: Undefined Traffic state" << endl;
    
    }

    setUncertainty(m_probability);
    
    return m_probability;
}
