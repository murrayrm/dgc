#ifndef ROADTOROADREGION_HH_
#define ROADTOROADREGION_HH_


#include "TrafficStateTransition.hh"
#include "TrafficUtils.hh"


class RoadToRoadRegion:public TrafficStateTransition {

  public:

    RoadToRoadRegion(TrafficState * state1, TrafficState * state2);
    ~RoadToRoadRegion();

    double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState);
    double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel ptLabel);

};

#endif                          /*ROADTOROADREGION_HH_ */
