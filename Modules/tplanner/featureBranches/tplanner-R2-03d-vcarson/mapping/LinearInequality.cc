#include "LinearInequality.hh" 



LinearInequality::LinearInequality(int dim, std::vector<double> vec)
  : m_dimension(dim)
  , m_vector(vec) 
{
  //nothing to do			
}
	
std::vector<double> LinearInequality::getVector() {
  return m_vector;
}
	
int LinearInequality::getDimension() {
  return m_dimension;
}

