#include "StoppedToCreep.hh"
#include <math.h>
#include <list>

StoppedToCreep::StoppedToCreep(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

StoppedToCreep::StoppedToCreep()
{

}

StoppedToCreep::~StoppedToCreep()
{

}

double StoppedToCreep::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToCreep::meetTransitionConditions" << endl;
    }

    switch (trafficState->getType()) {

    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    case TrafficStateFactory::INTERSECTION_STOP:
    {
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel label(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
	LaneLabel lane;
        point2 stopLinePos;
        map->getNextStopline(stopLinePos, label);

	map->getLane(lane,currFrontPos);
	point2arr centerline;
	map->getLaneCenterLine(centerline,lane);
        double distance;
	map->getDistAlongLine(distance,centerline,stopLinePos,currFrontPos);

        if (distance >= 1) {
          m_probability = 1;
          //         cout << "transitioning to creep" << endl;
        }
        else
            m_probability = 0;
    }
        break;
        
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::ZONE_REGION:

        m_probability = 0;
        break;

    default:
    
        m_probability = 0;
        cerr << "StoppedToCreep.cc: Undefined Traffic state" << endl;
    
    }
    
    setUncertainty(m_probability);
    return m_probability;
}
