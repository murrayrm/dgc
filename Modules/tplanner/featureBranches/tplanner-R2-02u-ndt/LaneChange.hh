#ifndef LANECHANGE_HH_
#define LANECHANGE_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"

class LaneChange:public ControlState {

  public:

    LaneChange(int stateId, ControlStateFactory::ControlStateType type);
    ~LaneChange();
    virtual int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
    void setLaneChangeID(int destLane);
    int getLaneChangeID();

  ControlState* newCopy() {
    return  (ControlState*) new LaneChange(*this);
  }

  private:
    int desiredLaneID;
};

#endif                          /*LANECHANGE_HH_ */
