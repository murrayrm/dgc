#include "Passing2_LaneChangeToLaneKeeping.hh"

Passing2_LaneChangeToLaneKeeping::Passing2_LaneChangeToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing2_LaneChangeToLaneKeeping::Passing2_LaneChangeToLaneKeeping()
{

}

Passing2_LaneChangeToLaneKeeping::~Passing2_LaneChangeToLaneKeeping()
{

}

double Passing2_LaneChangeToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in Passing2_LaneChangeToLaneKeeping::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
   
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        LaneChange *laneChange = dynamic_cast<LaneChange*>(controlState);
        if (laneChange == 0)
          cerr << "Error retrieving LaneChange destination" << endl;

        int laneID = laneChange->getLaneChangeID(); // This should be a preferredLaneID instead of exitLaneID
    
        if (map->getLaneID(AliceStateHelper::getPositionRearBumper(vehState)) == laneID) {
            m_prob = 1;
        }
    }
        break;

    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;
        
    default:
    
        m_prob = 0;
        cerr << "Passing2_LaneChangeToLaneKeeping.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
