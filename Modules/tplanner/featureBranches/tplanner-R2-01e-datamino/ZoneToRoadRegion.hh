#ifndef ZONETOROADREGION_HH_
#define ZONETOROADREGION_HH_


#include "TrafficStateTransition.hh"
#include "TrafficUtils.hh"


class ZoneToRoadRegion : public TrafficStateTransition
{

public: 

ZoneToRoadRegion(TrafficState* state1, TrafficState* state2);
~ZoneToRoadRegion();

  double meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState);

  double meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel pointLabel);

private: 

double m_distZoneExit; //in meters

};
#endif /*ZONETOROADREGION_HH_*/

