#include "ControlState.hh"
#include "TrafficUtils.hh"

bool ControlState::m_verbose = false;
bool ControlState::m_debug = false;
bool ControlState::m_log = false;


ControlState::ControlState(int stateId, ControlStateFactory::ControlStateType type)
  : State(stateId, type)
    , m_failed(false)
    , m_AliceInitPos(0, 0)
    , m_AliceUpdatePos(0, 0)
    , m_currDistFromInitPos(0)
    , m_stateID(stateId)
    , m_type(type)
    , m_isQueue(false)
    , m_isVehPresent(false)
{

}

ControlState::ControlState()
  : State()
    , m_failed(false)
    , m_AliceInitPos(0, 0)
    , m_AliceUpdatePos(0, 0)
    , m_currDistFromInitPos(0)
    , m_stateID(-1)
    , m_type(ControlStateFactory::NOSTATE)
    , m_isQueue(false)
{

}

ControlState::~ControlState(){
 
}

ControlState::ControlState(const ControlState &c) 
: State()
{
  m_AliceInitPos = point2(c.m_AliceInitPos);
  m_AliceInitVel = c.m_AliceInitVel;
  m_AliceUpdatePos = point2(c.m_AliceUpdatePos);
  m_currDistFromInitPos = c.m_currDistFromInitPos; 
  m_AliceInitTime = c.m_AliceInitTime; 
  m_stateID = c.m_stateID;
  m_type = c.m_type;
  m_isQueue = false;
}

ControlState& ControlState::operator=(const ControlState& c) 
{
  m_AliceInitPos = c.m_AliceInitPos;
  m_AliceInitVel = c.m_AliceInitVel;
  m_AliceUpdatePos = c.m_AliceUpdatePos;
  m_currDistFromInitPos = c.m_currDistFromInitPos; 
  m_AliceInitTime = c.m_AliceInitTime; 
  m_stateID = c.m_stateID;
  m_type = c.m_type;
  return(*this);
}

void ControlState::setOutputParams(bool debug, bool verbose, bool log)
{
    m_verbose = verbose;
    m_debug = debug;
    m_log = log;
}

void ControlState::setInitialPosition(VehicleState vehState)
{
    m_AliceInitPos = point2(vehState.localX, vehState.localY);
}

void ControlState::setUpdatedPosition(VehicleState vehState)
{
    m_AliceUpdatePos = point2(vehState.localX, vehState.localY);
}

void ControlState::setInitialTime()
{
    time(&m_AliceInitTime);
}

time_t ControlState::getInitialTime()
{
    return m_AliceInitTime;
}

point2 ControlState::getInitialPosition()
{
    return m_AliceInitPos;
}

point2 ControlState::getUpdatedPosition()
{
    return m_AliceUpdatePos;
}

double ControlState::calcDistFromInitPos(VehicleState vehState)
{
    setUpdatedPosition(vehState);
    m_currDistFromInitPos = TrafficUtils::calculateDistance(m_AliceUpdatePos, m_AliceInitPos);
    return m_currDistFromInitPos;
}

void ControlState::setInitialVelocity(VehicleState vehState)
{
    m_AliceInitVel = AliceStateHelper::getVelocityMag(vehState);

}

double ControlState::getInitialVelocity()
{
    return m_AliceInitVel;
}

int ControlState::getStateID()
{
    return m_stateID;
}

ControlStateFactory::ControlStateType ControlState::getType()
{
    return m_type;
}

bool ControlState::hasFailed()
{
    return m_failed;
}

void ControlState::resetFailure()
{
    m_failed = false;
}

string ControlState::toString() 
{

  string controlStateType;
  switch(m_type) {
  case ControlStateFactory::LANE_KEEPING:
    controlStateType = "Type = LANE_KEEPING  ";
    break;
  case ControlStateFactory::STOP:
    controlStateType = "Type = STOP ";
    break;
  case ControlStateFactory::STOPPED:
    controlStateType = "Type = STOPPED ";
    break;
  case ControlStateFactory::UTURN:
    controlStateType = "Type = UTURN ";
    break;
  case ControlStateFactory::LANE_CHANGE:
    controlStateType = "Type = LANE_CHANGE ";
    break;
  case ControlStateFactory::CREEP:
    controlStateType = "Type = CREEP ";
    break;
  case ControlStateFactory::PAUSE:
    controlStateType = "Type = PAUSE ";
    break;
  default: 
    break;
  };
 
  return controlStateType;
}

void ControlState::setIsQueueing(bool isQ)
{
  m_isQueue = isQ;
}

bool ControlState::getIsQueueing()
{
  return m_isQueue;
}


void ControlState::setIsVehPresentInOppositeLane(bool isP) {
  m_isVehPresent = isP;
}

bool ControlState::getIsVehPresentInOppositeLane() {
  return m_isVehPresent; 
}

bool ControlState::isComplete() {
  return false; 
}
