#include "Passing_LaneKeepingToLaneKeeping.hh"

Passing_LaneKeepingToLaneKeeping::Passing_LaneKeepingToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing_LaneKeepingToLaneKeeping::Passing_LaneKeepingToLaneKeeping()
{

}

Passing_LaneKeepingToLaneKeeping::~Passing_LaneKeepingToLaneKeeping()
{

}

double Passing_LaneKeepingToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in Passing_LaneKeepingToLaneKeeping::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        double delta_o = 10; // the obstacle distance in meters
	double obs_dist = map->getObstacleDist(AliceStateHelper::getPositionFrontBumper(vehState), 0);
	bool obstacle_present = (obs_dist > 0) && (obs_dist < delta_o);

	LaneLabel currLane;
	int laneErr = map->getLane(currLane, AliceStateHelper::getPositionFrontBumper(vehState));

        bool correct_lane = (planHorizon.getSegGoal(0).exitLaneID == currLane.lane);

        if (obstacle_present || correct_lane) {
            m_prob = 1;
        }
    }
        break;
        
    case TrafficStateFactory::INTERSECTION_STOP:
    case TrafficStateFactory::ZONE_REGION:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing_LaneKeepingToLaneKeeping.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
