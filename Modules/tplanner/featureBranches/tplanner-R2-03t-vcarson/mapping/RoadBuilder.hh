#ifndef ROADBUILDER_HH_
#define ROADBUILDER_HH_


#include "Road.hh"
#include "RoadLink.hh"


class RoadBuilder { 

public:

RoadBuilder();
~RoadBuilder();
Road* buildRoad();
RoadLink* buildRoadLink();

private:

	int getNextRoadId();
	int getNextRoadLinkId();
 
	int m_currRoadId;
	int m_currRoadLinkId; 

};



#endif /*ROADBUILDER_HH_*/
