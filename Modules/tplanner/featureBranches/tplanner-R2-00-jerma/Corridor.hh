#ifndef CORRIDOR_HH_
#define CORRIDOR_HH_

#include "dgcutils/RDDF.hh"
#include "mapping/GeometricConstraints.hh"
#include "Conflict.hh"
//#include "mapping/Line.hh"
#include "frames/point2.hh"
//#include <unistd.h>
#include <stdio.h>
//#include <iostream>
//#include <sstream>
//#include <string>
#include <vector>
#include <math.h>
#include "dgcutils/ggis.h"
#include "state/AliceStateHelper.hh"
#include "map/Map.hh"
//#include "ocpspecs/OCPtSpecs.hh"
//#include "interfaces/SegGoals.hh"


#define EPS 0.05
#define BIGNUMBER 1000000000.0f 
#define PI 3.1416

class Corridor {

public :  		

  Corridor();
  ~Corridor();

  GeometricConstraints* getGeometricConstraints();
  RDDF* getRddfCorridor(point2 gloToLocalDelta);
  int getControlStateId();
  void addPolyline(point2arr);
  void setVelProfile(vector<double> velProfile);
  void setDesiredAcc(double desAcc);
  void setAlicePrevPos(point2 alicePos);
  //  void updateDistFromConStateBegin(point2 alicePrevPos);
  vector<double> getVelProfile();
  double getDesiredAcc();
	void clear();
  //  void initializeOCPSpecs(VehicleState vehState);

private: 
  RDDF* convertToRddf(point2 gloToLocalDelta);  
  // These are internal functions for convertToRddf()
  void closept(double lnseg[4], double pt[2], double cpt[2]);
  void midpt(double pt1[2], double pt2[2], double mpt[2]);
  void radius(double mpt[2], double pt1[2], double pt2[2], double &radius);

  GeometricConstraints* m_geometricConstraints;
  
  Conflict* m_conflict;
  
  vector<point2arr> m_polylines; 
  int m_controlStateId;

  vector<double> m_velProfile; // [V_entry V_exit]
  double m_desiredAcc;
  double m_distFromConStateBegin;
  point2 m_alicePrevPos;
  //  OCPtSpecs m_ocpSpecs;
};

#endif /*CORRIDOR_HH_*/
