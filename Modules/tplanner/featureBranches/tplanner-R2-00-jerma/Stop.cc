#include "Stop.hh"


Stop::Stop(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
  , m_desDecc(-0.5)
{
}

Stop::~Stop()
{
}

int Stop::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
  point2arr leftBound, rightBound;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  point2arr leftBound1, rightBound1;
  point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);

  if(TrafficStateFactory::ROAD_REGION == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr); 
   }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // in the road region want to continue stopping at the same acc as slowdown
    // TODO: think through the vel profile specification some more, but use current velocity.
    velIn = 0;
    velOut = 0;
    acc = 0;

  } else if (TrafficStateFactory::APPROACH_INTER_SAFETY == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr);
    }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }

    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // here I want to adjust my acc based on my current vel and the stopline pos
    // TODO: think through the vel profile specification some more, but use current velocity.
    point2 stopLinePos;
		point2 alicePos = AliceStateHelper::getPositionRearAxle(vehState);
    int stopLineErr = localMap->getStopline(stopLinePos,alicePos);
    double corrLength = alicePos.dist(stopLinePos);
    velIn = 0;
    velOut = 0;
    acc = 0;

  } else if (TrafficStateFactory::INTERSECTION_STOP == traffState->getType()){
    // want to plan over two segments
    SegGoals segment2 = planHoriz.getSegGoal(1);
    point2arr leftBound1, rightBound1, leftBound2, rightBound2;
    // Set of boundary points due to intersection lane
    PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
		cout << "intersection entry waypt id = " << ptLabelIn << endl;
    PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
		cout << "intersection exit waypt id = " << ptLabelOut << endl;
    int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut);
    if (interBoundErr!=0){
      cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
      return (interBoundErr);
    }
    // Set of boudary pts due to lane after intersection
    LaneLabel laneLabel(segment2.entrySegmentID, segment2.entryLaneID);      
    int rightBoundErr = localMap->getRightBound(rightBound2, laneLabel); 
    int leftBoundErr = localMap->getLeftBound(leftBound2, laneLabel);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return rightBoundErr;
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return leftBoundErr;
   }
    // add the boundaries together
    leftBound = leftBound1;
    rightBound = rightBound1;
    for (unsigned i=0; i<leftBound2.size(); i++){
      if (leftBound.back()!=leftBound2[i]){
	leftBound.push_back(leftBound2[i]);
      }
    }
    for (unsigned i=0; i<rightBound2.size(); i++){
      if (rightBound.back()!=rightBound2[i]){
	rightBound.push_back(rightBound2[i]);
      }
    }

    // TODO: take some subset of these boundary points as the corridor

    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;

  }

  cout << "leftBound.size()" << leftBound.size() << endl;
  cout << "rightBound.size()" << rightBound.size() << endl;
	cout << "velocity profile: in = " << velIn << " and out = " << velOut << endl;
	cout << "specified acceleration = " << acc <<  endl;

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector<double> velProfile;
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  return 0;
}
