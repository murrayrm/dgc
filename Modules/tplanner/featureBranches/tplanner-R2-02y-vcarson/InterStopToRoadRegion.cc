#include "InterStopToRoadRegion.hh"
#include <math.h>


InterStopToRoadRegion::InterStopToRoadRegion(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distInterExit(1)
{

}

InterStopToRoadRegion::~InterStopToRoadRegion()
{

}

double InterStopToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    if ((m_verbose) || (m_debug)) {
        cout << "in InterStopToRoadRegion::meetTransitionConditions(), SHOULD NOT BE HERE " << endl;
    }
    
    return 0.0;
}

double InterStopToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel pointLabel)
{
    if ((m_verbose) || (m_debug)) {
        cout << "in InterStopToRoadRegion::meetTransitionConditions() " << endl;
    }
    
    // This is some distance to some waypoint
    point2 exitWaypt;
    double angle;
    point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);
    localMap->getWaypoint(exitWaypt, pointLabel);
    localMap->getHeading(angle, pointLabel);
    if ((m_verbose) || (m_debug)) {
        cout << "heading = " << angle << endl;
    }
    
    double dotProd = (-exitWaypt.x + currPos.x) * cos(angle) + (-exitWaypt.y + currPos.y) * sin(angle);
    if ((m_verbose) || (m_debug)) {
        cout << "in InterStopToRoadRegion: dot product  = " << dotProd << endl;
    }

    double AliceHeading = AliceStateHelper::getHeading(vehState);
    double absDist = exitWaypt.dist(currPos);
    double headingDiff = fmod(angle-AliceHeading,M_PI);

    if ((dotProd > -m_distInterExit) && (fabs(headingDiff)<M_PI/12) && (absDist<10))
        m_trafTransProb = 1;
    else
        m_trafTransProb = 0;
    setUncertainty(m_trafTransProb);
    
    return m_trafTransProb;
}
