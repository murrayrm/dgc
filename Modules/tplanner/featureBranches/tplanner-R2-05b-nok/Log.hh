#ifndef LOG_HH_
#define LOG_HH_

#include <ostream>
#include <stdio.h>
#include <string>

#include "ControlStateFactory.hh"
#include "TrafficStateFactory.hh"
#include "ControlState.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "gcinterfaces/SegGoals.hh" 

using namespace std;

class NullStream : public std::ostringstream {
  public:
    template<typename T> NullStream& operator<<(T t) {
      return *this;
    }
};

class Doublebuf: public std::streambuf {
  public:
    typedef std::char_traits<char> traits_type;
    typedef traits_type::int_type  int_type;

    Doublebuf(std::streambuf* sb1, std::streambuf* sb2):
      m_sb1(sb1),
      m_sb2(sb2)
    {}

    int_type overflow(int_type c) {
      if (m_sb1->sputc(c) == traits_type::eof() || m_sb2->sputc(c) == traits_type::eof())
        return traits_type::eof();
      return c;
    }

  private:
    std::streambuf* m_sb1;
    std::streambuf* m_sb2;
};

class DoubleStream: public ostream {
  public:
    DoubleStream(ostream &str1, ostream &str2) : ios(0), ostream(new Doublebuf(str1.rdbuf(), str2.rdbuf())) {}
    ~DoubleStream() { delete rdbuf(); };
};

class Log {

  public:

    Log();
    ~Log();

    int init(string filename, bool logit);
    void logSegment(PlanningHorizon ph);
    void logTrafficState(TrafficState *state, ControlState *cstate, PlanningHorizon ph, Map *map);
    void logControlState(ControlState *state);
    void logString(string str);

    static int getVerboseLevel();
    static void setVerboseLevel(int level);
    static ostream& getStream(int level);
    static void setGenericLogFile(const char *filename);

  private:
  
    string file;
    FILE *fp;
    bool log;
    int control_id;
    int traffic_id;
    int segment_id;
    int open();
    void close();

    static int verbose_level;
    static bool generic_logging;
    static NullStream nStream;
    static ofstream genericFileStream;
    static DoubleStream dualStream;

};

#endif                          /*LOG_HH_ */
