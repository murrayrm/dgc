#include "ControlStateTransition.hh"

ControlStateTransition::ControlStateTransition()
{
}

ControlStateTransition::ControlStateTransition(ControlState * state1, ControlState * state2)
:  StateTransition(state1, state2)
{
    m_controlState1 = state1;
    m_controlState2 = state2;
}


ControlStateTransition::~ControlStateTransition()
{
    m_controlState1 = 0;
    m_controlState2 = 0;

}

ControlState *ControlStateTransition::getControlStateTo()
{
    return m_controlState2;
}

ControlState *ControlStateTransition::getControlStateFrom()
{
    return m_controlState1;
}
