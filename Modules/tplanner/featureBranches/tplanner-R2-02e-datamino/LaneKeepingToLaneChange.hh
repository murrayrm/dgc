#ifndef LANEKEEPINGTOLANECHANGE_HH_
#define LANEKEEPINGTOLANECHANGE_HH_

#include "ControlStateTransition.hh"

class LaneKeepingToLaneChange:public ControlStateTransition {

public:
  LaneKeepingToLaneChange(ControlState *state1, ControlState *state2);
  LaneKeepingToLaneChange();
  ~LaneKeepingToLaneChange();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
