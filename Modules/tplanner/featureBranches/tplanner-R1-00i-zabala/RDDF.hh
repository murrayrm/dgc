/*!
 * \file RDDF.hh 
 * \brief class to read the rddf file and return the waypoints
 *
 * \author Thyago Consort 
 * \date 2003-04
 *
 * The RDDF class allows manipulation of RDDF files.
 *
 * This file has been simplifed to just include the type of RDDF
 * processing we need for backward compatibility in the Urban
 * Challenge.  See the full RDDF library if you need the old code.
 *
 */

#ifndef __RDDF_HH__
#define __RDDF_HH__

class RDDF 
{
protected:
  int numTargetPoints;

public:
  RDDF();			///< Generic constructor
  RDDF(char*pFileName);		///< Create an RDDF from a file
  ~RDDF();			///< Generic destructor

 };
#endif //__RDDF_HH__
