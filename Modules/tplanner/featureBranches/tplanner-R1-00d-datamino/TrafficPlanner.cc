/*!TrafficPlanner.cc
 * Author: Noel duToit
 * Jan 25 2007
 * */

#include "TrafficPlanner.hh"
#include "dgcutils/DGCutils.hh"

using namespace std;

#define MAX_DELTA_SIZE 100000
GisCoordLatLon latlon;
GisCoordUTM utm;
bool DEBUG=true;  // use the default while I figure out optget
bool VERBOSE=true; // use the default while I figure out optget
bool NOWAIT_FOR_STATE=false; // input Alice's location manually

CTrafficPlanner::CTrafficPlanner(int sn_key, bool bWaitForStateFill, char* RNDFFileName)
  : CSkynetContainer(MODtrafficplanner, sn_key),
    CStateClient(bWaitForStateFill),
    m_bReceivedAtLeastOneDelta(false)
{
  utm.zone = 11;
  utm.letter = 'S';
  
  DGCcreateMutex(&m_GloNavMapMutex);
  DGCcreateMutex(&m_LocalMapMutex);
  DGCcreateMutex(&m_LocalMapRecvMutex);
  //DGCcreateMutex(&m_ObstacleMutex);
  //DGCcreateMutex(&m_CostMapMutex);
  DGCcreateMutex(&m_DPlannerStatusMutex);
  DGCcreateMutex(&m_SegGoalsMutex);
  DGCcreateMutex(&m_deltaReceivedMutex);
  DGCcreateCondition(&m_LocalMapRecvCond);
  DGCcreateCondition(&m_deltaReceivedCond);
  
  m_GloNavMapRequestSocket = m_skynet.get_send_sock(SNlocalGloNavMapRequest);
  m_GloNavMapSocket = m_skynet.listen(SNlocalGloNavMap, MODgloNavMapLib);
  /* DON'T FORGET TO CHANGE THE NAME OF SKYNET SOCKET */
  m_fullMapRequestSocket = m_skynet.get_send_sock(SNfullmaprequest);
  requestGloNavMap();
  m_rndfRevNum = 0;

  //m_rndf->print();
  /*
    m_rndf = new RNDF();
    if (!m_rndf->loadFile(RNDFFileName))
    {
    cerr << "Error:  Unable to load RNDF file " << RNDFFileName << ", exiting program" << endl;
    exit(1);
    }
    m_rndf->assignLaneDirection();
    m_rndfRevNum = 0;
  */

  localMapSocket = m_skynet.listen(SNtrafficLocalMap, MODmapping);
  //mapTalker = CLocalMapTalker(MODtrafficplanner, sn_key, false);
  m_bReceivedAtLeastOneLocalMap = false;
  
  m_bReceivedAtLeastOneDelta = false;
  segGoalsSocket = m_skynet.listen(SNsegGoals, MODmissionplanner);
  if(segGoalsSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;
}

CTrafficPlanner::~CTrafficPlanner() 
{
  delete m_rndf;
  
  //for (unsigned i=0; i < m_obstacle.size(); i++)
    //delete m_obstacle[i];
  	
  delete m_dplannerStatus;

  DGCdeleteMutex(&m_GloNavMapMutex);
  DGCdeleteMutex(&m_LocalMapMutex);
  //DGCdeleteMutex(&m_ObstacleMutex);
  //DGCdeleteMutex(&m_CostMapMutex);
  DGCdeleteMutex(&m_DPlannerStatusMutex);
  DGCdeleteMutex(&m_SegGoalsMutex);
  DGCdeleteMutex(&m_deltaReceivedMutex);
  DGCdeleteCondition(&m_deltaReceivedCond);
}

/*
void CTrafficPlanner::getMapDeltasThread()
{
  int mapLayer;
  char* elevationMapDelta = new char[MAX_DELTA_SIZE];

  // constants in GlobalConstants.h
  m_elevationMap.initMap(CONFIG_FILE_DEFAULT_MAP);

  // The skynet socket for receiving map deltas (from fusionmapper)
  int mapDeltaSocket = m_skynet.listen(SNdeltaElevationMap, MODmapping);
  if(mapDeltaSocket < 0)
    cerr << "CTrafficPlanner::getMapDeltasThread(): skynet listen returned error" << endl;

  while(true)
    {
      int deltasize;
      RecvMapdelta(mapDeltaSocket, elevationMapDelta, &deltasize);
      m_elevationMap.applyDelta<double>(mapLayer, elevationMapDelta, deltasize);

      // set the condition to signal that the first delta was received
      if(!m_bReceivedAtLeastOneDelta)
	DGCSetConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);
    }
}
*/

void CTrafficPlanner::getLocalMapThread()
{
  bool RECVMAP = false;
  if (RECVMAP)
  {
    if (DEBUG)
    {
      cout << "Listening for map from Mapper" << endl;
    }
    //vector<Mapper::Segment> segments;
    //Mapper::Map receivedLocalMap(localMapSegments);
    Mapper::Map receivedLocalMap;
    //receivedLocalMap.print();
    //Mapper::Map map;
    int size;
    cout << "socket = " << localMapSocket << endl;
    
    while(true)
    {
      bool localMapReceived = RecvLocalMap(localMapSocket, &receivedLocalMap, &size);
      if (localMapReceived)
      {
	if (DEBUG)
	{
	  cout << "Received local map" << endl;
	}
        DGClockMutex(&m_LocalMapMutex);
        m_localMap = receivedLocalMap;
	if (DEBUG)
	{
	  cout << "*****************************************************************" << endl;
	  //m_localMap.print();
	  cout << "Trying to access the map" << endl;
	  Mapper::Segment LMcurrentSegment = m_localMap.getSegment(1);
	  cout << "Assigned segment" << endl;
	  Mapper::Lane LMCS_entryLane = LMcurrentSegment.getLane(2);
	  cout << "Assigned Lane" << endl;
	  vector<Mapper::Location> LMCS_entryLane_LB = LMCS_entryLane.getLB();
	  printf("LMCS_entryLane_LB[0] = (%3.6f, %3.6f)\n", LMCS_entryLane_LB[0].x, LMCS_entryLane_LB[0].y);
	  Mapper::LaneBoundary::Divider currentLaneLBType =  LMCS_entryLane.getLBType();
	  printf("currentLaneLBType = %d\n", currentLaneLBType);
	  vector<Mapper::StopLine> LMCS_entryLane_stopLines = LMCS_entryLane.getStopLines();
	  printf("LMCS_entryLane_stoplines[0] = (%3.6f, %3.6f)\n", LMCS_entryLane_stopLines[0].getXLoc(), LMCS_entryLane_stopLines[0].getYLoc());
	  cout << "=================================================================" << endl;
	}
        DGCunlockMutex(&m_LocalMapMutex);
        if (!m_bReceivedAtLeastOneLocalMap)
          DGCSetConditionTrue(m_bReceivedAtLeastOneLocalMap, m_LocalMapRecvCond, m_LocalMapRecvMutex);
      }
      //DGClockMutex(&m_LocalMapMutex);
      //m_localMap.print();
      //DGCunlockMutex(&m_LocalMapMutex);      
      usleep(500000);
    }
  }
  else
  {
    if (DEBUG)
    {
      cout << "Generating map" << endl;
    }
    while(true)
    {  
      DGClockMutex(&m_LocalMapMutex);
      //want to change later to access correct constructor in map to generate this map directly
      generateMapFromRNDF(m_rndf, m_localMap); //Use local function to generate map
      //m_localMap.print();
      DGCunlockMutex(&m_LocalMapMutex);
      usleep(500000);
    }
  }
}

void CTrafficPlanner::getDPlannerStatusThread()
{
  // The skynet socket for receiving dplanner status
  //  int dplannerStatusSocket = m_skynet.listen(SNdplannerStatus, MODdynamicplanner);
  //  DPlannerStatus* dplannerStatus = new DPlannerStatus();
  //  if(dplannerStatusSocket < 0)
  //    cerr << "TrafficPlanner::getDPlannerStatusThread(): skynet listen returned error" << endl;

  //  while(true)
  //  {
  //    bool dPlannerStatusReceived = RecvDPlannerStatus(dplannerStatusSocket, dplannerStatus);
  //    /* YOU NEED TO FIGURE OUT WHAT TO DO HERE */
  //    if (dPlannerStatusReceived)
  //    {
  //      DGClockMutex(&m_DPlannerStatusMutex);
  //      m_dplannerStatus = dplannerStatus;
  //      DGCunlockMutex(&m_DPlannerStatusMutex);      
  //    }
  //  }
}

void CTrafficPlanner::getSegGoalsThread()
{
  SegGoals* segGoals = new SegGoals();
  while(true)
    {
  	
      //cout << "segGoalsReceived before = " << endl;
      bool segGoalsReceived = RecvSegGoals(segGoalsSocket, segGoals);
      //cout << "segGoalsReceived after = " << segGoalsReceived << endl;
      if (segGoalsReceived)
	{
	  DGClockMutex(&m_SegGoalsMutex);
	  m_segGoals.push_back(*segGoals);
	  DGCunlockMutex(&m_SegGoalsMutex);
	}
    }
}

void CTrafficPlanner::requestGloNavMap()
{

  int deltasize;
  RNDF* receivedRndf = new RNDF();

  bool bRequestMap = true;

  cout << "Waiting for Global Navigation Map" << endl;

  while (!NewGloNavMap(m_GloNavMapSocket))
  {
    m_skynet.send_msg(m_GloNavMapRequestSocket,&bRequestMap, sizeof(bool) , 0);
    sleep(5);
  }

  bool GloNavMapReceived = RecvGloNavMap(m_GloNavMapSocket, receivedRndf, &deltasize);
  
  //GloNavMapReceived = true;
  //************************************************************
  
  if (GloNavMapReceived)
    {
      cout << "Received a new GloNav map" << endl;    
    
      DGClockMutex(&m_GloNavMapMutex);
      m_rndf = receivedRndf;
      DGCunlockMutex(&m_GloNavMapMutex);
    }
}
	
void CTrafficPlanner::TPlanningLoop(void)
{
  /***************************************************************************/
  /* CONFIRM THAT WE HAVE JOIN THE SEGGOALS GROUP */
  	
  SegGoalsStatus* segGoalsStatus = new SegGoalsStatus();
  segGoalsStatus->goalID = 0;
  segGoalsStatus->status = COMPLETED;

  int segGoalsStatusSocket = m_skynet.get_send_sock(SNtplannerStatus);
  int rddfSocket = m_skynet.get_send_sock(SNrddf);

  if(segGoalsStatusSocket < 0)
    cerr << "TPlanningLoop(): skynet get_send_sock returned error" << endl;

  // check map
  if (DEBUG)
  {
    cout << "Print map" << endl;
    DGClockMutex(&m_LocalMapMutex);
    m_localMap.print();
    DGCunlockMutex(&m_LocalMapMutex);
    cout << "Wait for 5 seconds" << endl;
    sleep(5);
  }

  // don't send goals until traffic planner starts listening
  //DGCWaitForConditionTrue(m_bReceivedAtLeastOneDelta, m_deltaReceivedCond, m_deltaReceivedMutex);

  //cout << "Waiting for local map ...";
  //DGCWaitForConditionTrue(m_bReceivedAtLeastOneLocalMap, m_LocalMapRecvCond, m_LocalMapRecvMutex);
  //cout << "done" << endl;

  /******************************************************************************/
  // OUTER LOOP - runs continuously
  bool outerLoopFlag = true;
  if ((VERBOSE) || (DEBUG))
  {
    cout << "Entering outer planning loop..." << endl;
  }
  while(outerLoopFlag)
  {
    SegGoals currentSegGoals, nextSegGoals, nextNextSegGoals;
    
    DGClockMutex(&m_SegGoalsMutex);
    unsigned numSegGoals = m_segGoals.size();
    DGCunlockMutex(&m_SegGoalsMutex);
    
    while (numSegGoals == 0)
    {
      segGoalsStatus->goalID = 0;
      segGoalsStatus->status = COMPLETED;
      // Request seggoals from mission planner
      bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
      if ((VERBOSE) || (DEBUG))
      {
        if (!statusSent)
        {
          cout << "Error sending segGoals status to mplanner" << endl;
          cerr << "TPlanningLoop(): Error sending segGoals status to mplanner" << endl;
        }
        else
          cout << "Requesting segGoals" <<  endl;
      }
      sleep(5);
      DGClockMutex(&m_SegGoalsMutex);
      numSegGoals = m_segGoals.size();
      DGCunlockMutex(&m_SegGoalsMutex);
    }

    //Read list of segment goals
    DGClockMutex(&m_SegGoalsMutex);
    list<SegGoals> mySegGoals = m_segGoals;
    m_segGoals.pop_front(); // discard the first goal
    DGCunlockMutex(&m_SegGoalsMutex);
    
    bool stopAtEndOfSegment;
    PlanningHorizon planningHorizon;
    numSegGoals = mySegGoals.size();
    if (numSegGoals > 0)
  	{
      if ((VERBOSE) || (DEBUG))
      {
  		  cout << "Using new segment goal: ";
      }
  		
      //Assign first segment goal as the current goal
  		currentSegGoals = mySegGoals.front();
      
      if ((VERBOSE) || (DEBUG))
      {	
  		  cout << "current goalID " << currentSegGoals.goalID << endl;
      }
  
  	  /*************************************************************************/
  	  /* Decide how far I need to plan */
      // safe m_segGoals in temp variable to extract next and next-next segment goals info
      if ((VERBOSE) || (DEBUG))
      {
        cout << "Determing how many segments to plan over..." << endl;
      }     
      stopAtEndOfSegment = currentSegGoals.stopAtExit;
      switch (currentSegGoals.segment_type)
      {
        case ROAD_SEGMENT:
          if (currentSegGoals.stopAtExit)
          {
            if ((VERBOSE) || (DEBUG))
            {
             cout << "Need to plan in current segment only" << endl;
            }  
            planningHorizon = CURRENT_SEGMENT;
            stopAtEndOfSegment = true;
          } 
          else
          {
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Need to plan to next segment" << endl;
            } 
            mySegGoals.pop_front();
            nextSegGoals = mySegGoals.front();
            if (nextSegGoals.segment_type == INTERSECTION)
            {
              if ((VERBOSE) || (DEBUG))
              {
                cout << "Need to plan to the next next segment (intersection and road segment beyond)" << endl;
              }
              mySegGoals.pop_front();
              nextNextSegGoals = mySegGoals.front();
              if (nextNextSegGoals.segment_type == END_OF_MISSION)
                planningHorizon = NEXT_SEGMENT;
              else
                planningHorizon = NEXT_NEXT_SEGMENT;
            } 
            else if (nextSegGoals.segment_type == PARKING_ZONE)
            {
              if ((VERBOSE) || (DEBUG))
              {
                cout << "Need to plan to the beginning of the parking zone only" << endl;
              }
              planningHorizon = CURRENT_SEGMENT;
              stopAtEndOfSegment = true;
            } 
            else if (nextSegGoals.segment_type == END_OF_MISSION)
            {
              if ((VERBOSE) || (DEBUG))
              {
                cout << "Plan to next segment" << endl;
              }
              planningHorizon = NEXT_SEGMENT;
            }
          }
          break;
        case INTERSECTION:
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Need to plan to the next segment (road segment)" << endl;
          }
          planningHorizon = NEXT_SEGMENT;
          mySegGoals.pop_front();
          nextSegGoals = mySegGoals.front();
          break;
        case PARKING_ZONE:
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Need to plan in the current segment only" << endl;
          }
          planningHorizon = CURRENT_SEGMENT;
          stopAtEndOfSegment = true;
          break;
        case UTURN:
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Need to plan in the current segment only" << endl;
          }
          planningHorizon = CURRENT_SEGMENT;
          break;
        case END_OF_MISSION:
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Need to plan in the current segment only" << endl;
          }
          planningHorizon = CURRENT_SEGMENT;
          stopAtEndOfSegment = true;
          break;	
        default:
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Something is likely wrong in segment type specification, or deciding how far I need to plan" << endl;
          }
          planningHorizon = CURRENT_SEGMENT; //bring vehicle to safe stop so that we can replan or something
          stopAtEndOfSegment = true;
      }         	
    }

    if (numSegGoals > 0) // DONT START PLANNING BEFORE I HAVE SOME GOALS
    {
      // Deal with general case first
      if (!((currentSegGoals.entrySegmentID==0) && (currentSegGoals.entryLaneID==0) && (currentSegGoals.entryWaypointID==0)))
      {

        /*************************************************************************/
        // INNER LOOP - planning loop
        if ((VERBOSE) || (DEBUG))
        {
          cout << "Entering inner planning loop..." << endl;
        }
        bool stayInLoopFlag = true;
        while(stayInLoopFlag)
        {
          double x_A,y_A, vx_A, vy_A;
          //************************************************************************
          // READ ALICE POSITION
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Reading Alice State" << endl;
          }
          UpdateState(); // from here I get m_state, which has fields m_state.Northing and m_state.Easting
          x_A = m_state.utmNorthing;  // position of the rear axle
          y_A = m_state.utmEasting;
          vx_A = m_state.utmEastVel;
          vy_A = m_state.utmNorthVel;
          if (NOWAIT_FOR_STATE)
          {
            x_A = 396422; 
            y_A = 3778142;
            vx_A = 0;
            vy_A = 0;
          } 				

          //************************************************************************
          // READ LOCAL MAP
          // SHOULD NOW RECEIVE MAP FROM MAPPER
          //generateMapFromRNDF(m_rndf, m_localMap);
          DGClockMutex(&m_LocalMapMutex);
          myLocalMap = m_localMap; // take map that was received from Mapping and assign to local copy
          DGCunlockMutex(&m_LocalMapMutex);
          //myLocalMap.print();
          //sleep(5);
          
          // ************************************************************************
          // GloNavMap utilization
          #warning "talk to Nok about locking rndf"
          //Segment* rndfSegment = m_rndf->getSegment(segmentID); // this is in the RNDF class
          double laneWidth = 12;					
          //m_rndf->print();

          // ***********************************************************************
          // READ LOCAL ELEVATION MAP
          // Convert elevation map to cost map
          //if ((VERBOSE) || (DEBUG))
          //{
          //  cout << "Waiting for elevation map" << endl;
          //}
          //if ((VERBOSE) || (DEBUG))
          //{
          //  cout << "Manipulate elevation map..." << endl;
          //}          
          //CMapPlus m_costMap;
          //DGClockMutex(&m_CostMapMutex);
          //elevation2Cost(m_elevationMap, m_costMap);
          //DGCunlockMutex(&m_CostMapMutex);
          
          //************************************************************************
          //* EXTRACT TRAFFIC RULES
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Extract traffic rules from segment type..." << endl;
          }          
          vector<TrafficRules> trafficRules;
          trafficRules = extractRules(currentSegGoals.segment_type);

          /************************************************************************/
          /* PARAMETER ESTIMATION */
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Estimate parameters necessary for constraint generation..." << endl;
          }
          
          // Estimate current lane
          // Don't use this for now, but will become very important in the future
          //int currentLane = 1;
          
          // This is a hack to account for limited sensing range
          double dLimit = 100;

          // Need to estimate distance to the end of the segment (the exit point).
          // This will depend either on the waypoint position, or in the case of a stopline, 
          // the location of the stop line.
          // When far away rely on GPS only and as we get closer, rely more on lane boundary data.
          // from GPS
          cout << "ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER = " << ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER << endl;
          cout << "DIST_SWITCH_FROM_GPS_TO_SENSORS = " << DIST_SWITCH_FROM_GPS_TO_SENSORS << endl;
          double dist2Exit_GPS, dist2Exit, dist2Exit_shifted;
          double CSExitX, CSExitY, CSExitWayptX, CSExitWayptY;
          double CSExitX_shifted, CSExitY_shifted;
          Waypoint* CSExitWaypt = m_rndf->getWaypoint(currentSegGoals.exitSegmentID, currentSegGoals.exitLaneID, currentSegGoals.exitWaypointID);
          CSExitWayptX = CSExitWaypt->getNorthing();
          CSExitWayptY = CSExitWaypt->getEasting();
          if (DEBUG)
          {
            printf("Exit point before stop info used = (%3.6f,%3.6f)\n",CSExitWayptX, CSExitWayptY);
          }
          // When we have other info (such as checkpoint, or stopline information, switch to that info
          if (currentSegGoals.segment_type == ROAD_SEGMENT)
          {
            // Get rough idea of how far away we are
            dist2Exit_GPS = sqrt(pow(CSExitWayptX-x_A,2) + pow(CSExitWayptY-y_A,2));

            if ((stopAtEndOfSegment) && (dist2Exit_GPS < DIST_SWITCH_FROM_GPS_TO_SENSORS+ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
            { 
    		  		// Get lane info from local map
    			    //vector<Mapper::Location> stopLineCoord;
    			    Mapper::Segment LMCS = myLocalMap.getSegment(currentSegGoals.entrySegmentID);
    			    cout << "stopline segment = " << currentSegGoals.entrySegmentID << endl;
    			    Mapper::Lane LMCS_entryLane = LMCS.getLane(currentSegGoals.entryLaneID);
    			    cout << "stopline lane =  " << currentSegGoals.entryLaneID << endl;
    			    vector<Mapper::StopLine> LMCS_entryLane_stopLines = LMCS_entryLane.getStopLines();
    			    cout << "LMCS_entryLane_stopLines.size() = " << LMCS_entryLane_stopLines.size() << endl;
    					cout << "Switching to stopline data now" << endl;
    					CSExitX = LMCS_entryLane_stopLines[0].getXLoc()-1;	// make the exit pt x = stopline x
    			    CSExitY = LMCS_entryLane_stopLines[0].getYLoc()-1;
    			    cout << "Switched!" << endl;

              if (DEBUG)
              {
                printf("Exit point after stop info used = (%3.6f,%3.6f)\n",CSExitX, CSExitY);
              }

              // FOR NOW USE WAYPOINT INFO
              //CSExitX = CSExitWayptX;
              //CSExitY = CSExitWayptY;			    
            }
            else	// we are far away, or dont have any other info 
            {
              CSExitX = CSExitWayptX;
              CSExitY = CSExitWayptY;
            } 
          }
          else if ((currentSegGoals.segment_type == INTERSECTION) || (currentSegGoals.segment_type == PARKING_ZONE))
          {	
            // NOT SURE WHAT SENSORY DATA SHOULD BE USED TO UPDATE THIS
            CSExitX = CSExitWayptX;
            CSExitY = CSExitWayptY;
          }
          dist2Exit = sqrt(pow(CSExitX-x_A,2) + pow(CSExitY-y_A,2));
          if (DEBUG)
          {
            printf("Exit point = (%3.6f,%3.6f)\n",CSExitX, CSExitY);
          }  
          /************************************************************************/
          /* DECISION MAKING */
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Decision making..." << endl;
          }

          /************************************************************************/	
          /* GENERATE CONSTRAINTS */
          // Read latest information from the map and use throughout the planning cycle
          //DGClockMutex(&m_LocalMapMutex);
          //Mapper::Map myLocalMap = m_localMap;
          //DGCunlockMutex(&m_LocalMapMutex);         
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Generate constraints..." << endl;
          }          
          ConstraintSet constraintSet;
          
          if (currentSegGoals.segment_type == ROAD_SEGMENT)
          {
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Segment type: Road segment" << endl;
            }
            // Extract info from map
            // Variable definitions
            vector<Mapper::Location> currentLaneLBCoord, currentLaneRBCoord;
            
            // Variable assignments
            Mapper::Segment LMcurrentSegment = myLocalMap.getSegment(currentSegGoals.entrySegmentID);
            Mapper::Lane LMCS_entryLane = LMcurrentSegment.getLane(currentSegGoals.entryLaneID);
            vector<Mapper::Location> LMCS_entryLane_LB = LMCS_entryLane.getLB();
            vector<Mapper::Location> LMCS_entryLane_RB = LMCS_entryLane.getRB();
            Mapper::LaneBoundary::Divider currentLaneLBType =  LMCS_entryLane.getLBType();
            Mapper::LaneBoundary::Divider currentLaneRBType =  LMCS_entryLane.getRBType();
            
            #warning "check if there are enough points on the boundary, and if not, do something"
            // Check that hte boundary has enough points. If not, need to place points, or something?!
            
            
            // Evaluate boundaries
						if (DEBUG)
            {
              cout << "Lane boundaries from mapping..." << endl;
              for (unsigned ii=0; ii<LMCS_entryLane_LB.size(); ii++)
              {
                printf("LB [%d] = [%3.3f,%3.3f]\n", ii, LMCS_entryLane_LB[ii].x, LMCS_entryLane_LB[ii].y);
              }
              for (unsigned ii=0; ii<LMCS_entryLane_RB.size(); ii++)
              {
                printf("RB [%d] = [%3.3f,%3.3f]\n", ii, LMCS_entryLane_RB[ii].x, LMCS_entryLane_RB[ii].y);
              }            
            }
            
				    // Need to only use information in the road segment up to the exit point
				    // find the point on the boundary that is closest to the exit waypoint and insert a point there
				    vector<double> x_RB, y_RB, x_LB, y_LB;
				    int ind_RBend_CP, ind_LBend_CP, ind_RBend_Ins, ind_LBend_Ins;
				    int ind_RBshift_CP, ind_LBshift_CP, ind_RBshift_Ins, ind_LBshift_Ins;
				    double x_temp, y_temp;
				    Mapper::Location tmploc;
            // Left boundary
				    for (unsigned ii=0;ii<LMCS_entryLane_LB.size();ii++)
						{
						  x_LB.push_back(LMCS_entryLane_LB[ii].x);
						 	y_LB.push_back(LMCS_entryLane_LB[ii].y);
						}
            ind_LBend_CP = closestExistingBoundaryPt(x_LB, y_LB, CSExitX, CSExitY);
            ind_LBend_Ins = closestPtOnBoundary(x_LB, y_LB, CSExitX, CSExitY, ind_LBend_CP, x_temp, y_temp);
				    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LBend_Ins);
			 			// right boundary
            for (unsigned ii=0; ii<LMCS_entryLane_RB.size(); ii++)
						{
				  		x_RB.push_back(LMCS_entryLane_RB[ii].x);
				  		y_RB.push_back(LMCS_entryLane_RB[ii].y);
						}
            ind_RBend_CP = closestExistingBoundaryPt(x_RB, y_RB, CSExitX, CSExitY);
            ind_RBend_Ins = closestPtOnBoundary(x_RB, y_RB, CSExitX, CSExitY, ind_RBend_CP, x_temp, y_temp);
			      insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RBend_Ins);		 
			 			
			      // calculate how far I really am from the exit point (adjusting for Alice's length
			      // Use the lane boundary orientation at the exit point
			      double theta_ave, theta_LB, theta_RB;
			      // left boundary
			      if (ind_LBend_Ins>0)
			      	theta_LB = atan2(y_LB[ind_LBend_Ins-1] - y_LB[ind_LBend_Ins],x_LB[ind_LBend_Ins-1] - x_LB[ind_LBend_Ins]);
			      else
			      {
			      	cerr << "Only one point on the left boundary - cannot calculate slope with which to offset the distance to exit" << endl;
			      	// In this case use the slope from the exit waypt to alice's current position
			      	theta_LB = atan2(y_A-y_LB[ind_LBend_Ins], x_A - x_LB[ind_LBend_Ins]);
			      }
			      // right boundary
			      if (ind_RBend_Ins>0)
			      	theta_RB = atan2(y_RB[ind_RBend_Ins-1] - y_RB[ind_RBend_Ins],x_RB[ind_RBend_Ins-1] - x_RB[ind_RBend_Ins]);
			      else
			      {
			      	cerr << "Only one point on the right boundary - cannot calculate slope with which to offset the distance to exit" << endl;
			      	// In this case use the slope from the exit waypt to alice's current position
			      	theta_RB = atan2(y_A-y_RB[ind_RBend_Ins], x_A - x_RB[ind_RBend_Ins]);
			      }
			      // take the average of these angles
			      theta_ave = (theta_LB+theta_RB)/2;
			      // Shift back the segment exit pt by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMBER
						CSExitX_shifted = CSExitX + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*cos(theta_ave);
						CSExitY_shifted = CSExitY + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*sin(theta_ave);
            dist2Exit_shifted = sqrt(pow(CSExitX_shifted-x_A,2) + pow(CSExitY_shifted-y_A,2));

			 			// Insert a point on the boundary at the off-set exit point
            // right boundary
            ind_RBshift_CP = closestExistingBoundaryPt(x_RB, y_RB, CSExitX_shifted, CSExitY_shifted);
            ind_RBshift_Ins = closestPtOnBoundary(x_RB, y_RB, CSExitX_shifted, CSExitY_shifted, ind_RBshift_CP, x_temp, y_temp);
			      insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RBshift_Ins);
            // left boundary
            ind_LBshift_CP = closestExistingBoundaryPt(x_LB, y_LB, CSExitX_shifted, CSExitY_shifted);
            ind_LBshift_Ins = closestPtOnBoundary(x_LB, y_LB, CSExitX_shifted, CSExitY_shifted, ind_LBshift_CP, x_temp, y_temp);
            insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LBshift_Ins);
			     	
			     	// find the index of the point on the boundary closest to the exit point
            ind_RBend_CP = closestExistingBoundaryPt(x_RB, y_RB, CSExitX, CSExitY);
            ind_LBend_CP = closestExistingBoundaryPt(x_LB, y_LB, CSExitX, CSExitY);
			      
			 			for (int ii = 0; ii<=ind_LBend_CP; ii++)
						{
					  	tmploc.x = x_LB[ii];
					  	tmploc.y = y_LB[ii];
					  	currentLaneLBCoord.push_back(tmploc);
						} 
			      for (int ii = 0; ii<=ind_RBend_CP; ii++)
						{
				  		tmploc.x = x_RB[ii];
				  		tmploc.y = y_RB[ii];
				  		currentLaneRBCoord.push_back(tmploc);
						}
			      
            if (DEBUG)
			      {
              printf("Shifted exit point = (%3.6f,%3.6f)\n",CSExitX_shifted, CSExitY_shifted);
              printf("Dist to shifted exit point = %3.6f\n",dist2Exit_shifted);
              
              cout << "Lane boundaries on road segment up to shifted exit point..." << endl;
              for (unsigned ii=0;ii<currentLaneLBCoord.size();ii++)
              {
                printf("LB [%d] = [%3.3f,%3.3f]\n", ii, currentLaneLBCoord[ii].x, currentLaneLBCoord[ii].y);
              }
              for (unsigned ii=0;ii<currentLaneRBCoord.size();ii++)
              {
                printf("RB [%d] = [%3.3f,%3.3f]\n", ii, currentLaneRBCoord[ii].x, currentLaneRBCoord[ii].y);
              }
            }     	      
			      constraintSet = laneFollowing(currentLaneLBCoord, currentLaneRBCoord, currentLaneLBType, currentLaneRBType, trafficRules, dLimit, dist2Exit, x_A, y_A);
			      
			 		}
			  	else if (currentSegGoals.segment_type == INTERSECTION)
			    {
            if ((VERBOSE)||(DEBUG))
            {
              cout << "Segment type: Intersection" << endl;
            }
            
            // Variable definition
			      vector<Mapper::Location> intLaneLBCoord, intLaneRBCoord;
            
            // Variable assignment
            Mapper::Segment LM_intEntrySegment = myLocalMap.getSegment(currentSegGoals.entrySegmentID);
			      Mapper::Segment LM_intExitSegment = myLocalMap.getSegment(currentSegGoals.exitSegmentID);
			      Mapper::Lane LM_intEntryLane = LM_intEntrySegment.getLane(currentSegGoals.entryLaneID);
			      Mapper::Lane LM_intExitLane = LM_intExitSegment.getLane(currentSegGoals.exitLaneID);
			      vector<Mapper::Location> LM_intEntryLane_LB = LM_intEntryLane.getLB();
			      vector<Mapper::Location> LM_intEntryLane_RB = LM_intEntryLane.getRB();
			      vector<Mapper::Location> LM_intExitLane_LB = LM_intExitLane.getLB();
			      vector<Mapper::Location> LM_intExitLane_RB = LM_intExitLane.getRB();
		
            #warning "check if there are enough points on the boundary, and if not, do something"
            // Check that hte boundary has enough points. If not, need to place points, or something?!
            
 
			      // INTERSECTION ENTRY LANE
			      
						// wait for dom to implement stoplines before dealing with this	      
			      //double intEntryWayptX = LMCS_entryLane_stopLines[currentStopLineID].getXLoc();	// make the exit pt x = stopline x
			      //double intEntryWayptY = LMCS_entryLane_stopLines[currentStopLineID].getYLoc();
			      
			      Waypoint* intEntryWaypt = m_rndf->getWaypoint(currentSegGoals.entrySegmentID, currentSegGoals.entryLaneID, currentSegGoals.entryWaypointID);
			      double intEntryWayptX = intEntryWaypt->getNorthing();
			      double intEntryWayptY = intEntryWaypt->getEasting();
			      
			      vector<double> x_RB, y_RB, x_LB, y_LB;
			      int ind_RB_CP, ind_LB_CP, ind_RB_Ins, ind_LB_Ins;
			      int ind_inserted;
			      double x_temp, y_temp;
			      Mapper::Location tmploc;
			      // left boudary
			      for (unsigned ii=0;ii<LM_intEntryLane_LB.size();ii++)
						{
					  	x_LB.push_back(LM_intEntryLane_LB[ii].x);
					  	y_LB.push_back(LM_intEntryLane_LB[ii].y);
						}
            ind_LB_CP = closestExistingBoundaryPt(x_LB, y_LB, intEntryWayptX, intEntryWayptY);
            cout << "ind_LB_CP = " << ind_LB_CP <<endl;
            ind_LB_Ins = closestPtOnBoundary(x_LB, y_LB, intEntryWayptX, intEntryWayptY, ind_LB_CP, x_temp, y_temp);     
            cout << "ind_LB_Ins = " << ind_LB_Ins << endl;       
				    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
				    
            // from this point, move back along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
				    distanceAlongBoundary(x_LB, y_LB, ind_LB_Ins, ind_inserted, -ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
            cout << "ind_inserted = " << ind_inserted << endl; 
				    // make this the first point on the LB
				    tmploc.x = x_LB[ind_inserted]; tmploc.y = y_LB[ind_inserted];
            intLaneLBCoord.push_back(tmploc);
                        
            // for now just take the previous point
//            ind_LB_CP = closestExistingBoundaryPt(x_LB, y_LB, intEntryWayptX, intEntryWayptY);
//				    tmploc.x = x_LB[ind_LB_CP-1]; tmploc.y = y_LB[ind_LB_CP-1];
//            intLaneLBCoord.push_back(tmploc);
           
				    // Make the point closest to the stopline the entry point the next point on the LB 
            ind_LB_CP = closestExistingBoundaryPt(x_LB, y_LB, intEntryWayptX, intEntryWayptY);
            tmploc.x = x_LB[ind_LB_CP]; tmploc.y = y_LB[ind_LB_CP];
				    intLaneLBCoord.push_back(tmploc);
				      
            // right boudary
            for (unsigned ii=0;ii<LM_intEntryLane_RB.size();ii++)
            {
              x_RB.push_back(LM_intEntryLane_RB[ii].x);
              y_RB.push_back(LM_intEntryLane_RB[ii].y);
            }
            ind_RB_CP = closestExistingBoundaryPt(x_RB, y_RB, intEntryWayptX, intEntryWayptY);
            ind_RB_Ins = closestPtOnBoundary(x_RB, y_RB, intEntryWayptX, intEntryWayptY, ind_RB_CP, x_temp, y_temp);            
            insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
            
            // from this point, move back along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
            distanceAlongBoundary(x_RB, y_RB, ind_RB_Ins, ind_inserted, -ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
            // make this the first point on the RB
            tmploc.x = x_RB[ind_inserted]; tmploc.y = y_RB[ind_inserted];
            intLaneRBCoord.push_back(tmploc);
            
            // for now just take the previous point
//            ind_RB_CP = closestExistingBoundaryPt(x_RB, y_RB, intEntryWayptX, intEntryWayptY);
//            tmploc.x = x_RB[ind_RB_CP-1]; tmploc.y = y_RB[ind_RB_CP-1];
//            intLaneRBCoord.push_back(tmploc);
           
            // Make the point closest to the stopline the entry point the next point on the LB 
            ind_RB_CP = closestExistingBoundaryPt(x_RB, y_RB, intEntryWayptX, intEntryWayptY);
            tmploc.x = x_RB[ind_RB_CP]; tmploc.y = y_RB[ind_RB_CP];
            intLaneRBCoord.push_back(tmploc);
		
				    // clear vectors
				    x_LB.clear(); y_LB.clear(); x_RB.clear(); y_RB.clear();
			
			      // INTERSECTION EXIT LANE
			      // find the point on the boundary that is closest to the entry waypoint and use this as 
				    // the starting point of the LB for the virtual lane inside the intersection
				    for (unsigned ii=0;ii<LM_intExitLane_LB.size();ii++)
						{
					  	x_LB.push_back(LM_intExitLane_LB[ii].x);
					  	y_LB.push_back(LM_intExitLane_LB[ii].y);
						}
            ind_LB_CP = closestExistingBoundaryPt(x_LB, y_LB, CSExitX, CSExitY);
            ind_LB_Ins = closestPtOnBoundary(x_LB, y_LB, CSExitX, CSExitY, ind_LB_CP, x_temp, y_temp);
				    //insert these points on the boudary
				    insertPointOnBoundary(x_LB, y_LB, x_temp, y_temp, ind_LB_Ins);
            //use this as the third point on the boundary
            tmploc.x = x_temp; tmploc.y = y_temp;
            intLaneLBCoord.push_back(tmploc);            
				    // from this point, move along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
            ind_LB_CP = closestExistingBoundaryPt(x_LB, y_LB, CSExitX, CSExitY);
				    distanceAlongBoundary(x_LB, y_LB, ind_LB_CP, ind_inserted, ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
				    // make this the last point on the LB
				    tmploc.x = x_LB[ind_inserted]; tmploc.y = y_LB[ind_inserted];
				    intLaneLBCoord.push_back(tmploc);
				    
            // Do the same for the RB
            for (unsigned ii=0; ii<LM_intExitLane_RB.size(); ii++)
            {
              x_RB.push_back(LM_intExitLane_RB[ii].x);
              y_RB.push_back(LM_intExitLane_RB[ii].y);
            }
            ind_RB_CP = closestExistingBoundaryPt(x_RB, y_RB, CSExitX, CSExitY);
            ind_RB_Ins = closestPtOnBoundary(x_RB, y_RB, CSExitX, CSExitY, ind_RB_CP, x_temp, y_temp);
            //insert these points on the boudary
            insertPointOnBoundary(x_RB, y_RB, x_temp, y_temp, ind_RB_Ins);
            //use this as the third point on the boundary
            tmploc.x = x_temp; tmploc.y = y_temp;
            intLaneRBCoord.push_back(tmploc);            
            // from this point, move along the boundary by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER and insert a point there
            ind_RB_CP = closestExistingBoundaryPt(x_RB, y_RB, CSExitX, CSExitY);
            distanceAlongBoundary(x_RB, y_RB, ind_RB_CP, ind_inserted, ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER);
            // make this the last point on the RB
            tmploc.x = x_RB[ind_inserted]; tmploc.y = y_RB[ind_inserted];
            intLaneRBCoord.push_back(tmploc);
					    
				    // Next I want to shift the exit point back by some distance to account for Alice's length
			     	// in the case of an intersection, I think we want to clear the intersection before switching to
			     	// roadsegment mode. 
						CSExitX_shifted = CSExitX;
						CSExitY_shifted = CSExitY;
            dist2Exit_shifted = sqrt(pow(CSExitX_shifted-x_A,2) + pow(CSExitY_shifted-y_A,2)); 
		        if (DEBUG)
            {
              printf("Shifted exit point = (%3.6f,%3.6f)\n",CSExitX_shifted, CSExitY_shifted);
              printf("Dist to shifted exit point = %3.6f\n",dist2Exit_shifted);
              cout << "Lane boundaries in the intersection..." << endl;
              for (unsigned ii=0;ii<intLaneLBCoord.size();ii++)
              {
                printf("LB [%d] = [%3.3f,%3.3f]\n", ii, intLaneLBCoord[ii].x, intLaneLBCoord[ii].y);
              } 
              for (unsigned ii=0;ii<intLaneRBCoord.size();ii++)
              {
                printf("RB [%d] = [%3.3f,%3.3f]\n", ii, intLaneRBCoord[ii].x, intLaneRBCoord[ii].y);
              }
            } 	
				    constraintSet = intersectionNav(intLaneLBCoord, intLaneRBCoord, trafficRules, dLimit, x_A, y_A);
          } // if segment type is intersection
				  
          if (DEBUG)
				  {
            cout << "Generated constraints: boundaries" << endl;
            cout << "rightBoundaryConstraint.size() = " << constraintSet.rightBoundaryConstraint.size() << endl;
            cout << "leftBoundaryConstraint.size() = " << constraintSet.leftBoundaryConstraint.size() << endl;
            // print the boundary points
            for (unsigned ii=0;ii<constraintSet.leftBoundaryConstraint.size();ii++)
            {  
              printf("LBconstraints [%d] = [%3.3f,%3.3f] and [%3.6f, %3.6f]\n", ii, constraintSet.leftBoundaryConstraint[ii].xrange1, constraintSet.leftBoundaryConstraint[ii].yrange1, constraintSet.leftBoundaryConstraint[ii].xrange2, constraintSet.leftBoundaryConstraint[ii].yrange2);
            } 
            for (unsigned ii=0;ii<constraintSet.rightBoundaryConstraint.size();ii++)
            {
              printf("RBconstraints [%d] = [%3.3f,%3.3f] and [%3.6f, %3.6f]\n", ii, constraintSet.rightBoundaryConstraint[ii].xrange1, constraintSet.rightBoundaryConstraint[ii].yrange1, constraintSet.rightBoundaryConstraint[ii].xrange2, constraintSet.rightBoundaryConstraint[ii].yrange2);
            }	
          }
          
				  /************************************************************************/    
				  /* MERGE CONSTRAINTS */
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Merging constraints..." << endl;
          }										
							
				  /************************************************************************/    
				  /* POST-PROCESS CONSTRAINTS 											*/
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Post-processing constraints..." << endl;
          }          
				  //RDDF* newRddf = new RDDF("rddf.dat",true);
				  vector<double> rddfx, rddfy, rddfr;	// a clean slate to send to convRDDF
				  		  
				  RDDF* newRddf = new RDDF();		    		
				    		
				  bool rddfError;
				  rddfError = convRDDF(constraintSet.rightBoundaryConstraint, constraintSet.leftBoundaryConstraint, currentSegGoals, x_A, y_A, laneWidth, rddfx, rddfy, rddfr);
		  		
          RDDFData rddfData;
		  		double previousNorthing, previousEasting;
		  		int rddfNum=1;
		      
		      // Specify a velocity profile for 
		  		//double rddfMaxSpeed = currentSegGoals.maxSpeedLimit;
		  		double rddfMaxSpeed = 5;
		  		double rddfSpeed, dist2Exit_RDDF, dist2Exit_shifted_RDDF;
		  		double posStartToBrake = pow(rddfMaxSpeed,2)/(2*ALICE_DESIRED_DECELERATION); // The position where we want to start slowing down
		
					//cout << "posStartToBrake = " << posStartToBrake << endl;
					if((DEBUG) || (stopAtEndOfSegment))
          {
						cout << "need to stop at the end of the segment" << endl;
          }
				  for (int ii = 0; ii < (int)rddfx.size(); ii++)
		    	{
		      	if (ii==0)
						{
					  	// convert to rddf format
					  	utm.n = rddfx[ii];
					  	utm.e = rddfy[ii];
					  	gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
		
							// Calculate speed using a ramp-down speed profile
							// This speed is dependent on our distance from the exit, whether
							// we want to stop, and Alice's dynamics
							dist2Exit_RDDF = sqrt(pow(CSExitX-rddfx[ii],2) + pow(CSExitY-rddfy[ii],2));
							//cout << "dist2Exit_RDDF = " << dist2Exit_RDDF << endl;
							dist2Exit_shifted_RDDF = sqrt(pow(CSExitX_shifted-rddfx[ii],2) + pow(CSExitY_shifted-rddfy[ii],2));
							//cout << "dist2Exit_shifted_RDDF = " << dist2Exit_shifted_RDDF << endl;
							
				  		if ((stopAtEndOfSegment) && (dist2Exit_shifted_RDDF<posStartToBrake) && (dist2Exit_RDDF>ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
				    		rddfSpeed = sqrt(2*ALICE_DESIRED_DECELERATION*dist2Exit_shifted_RDDF); //follow velocity profile
				    		
				    	else if ((stopAtEndOfSegment) && (dist2Exit_RDDF<ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
				    		rddfSpeed = 0; // we are techinically inside the intersection
				    		
				    	else
				    		rddfSpeed = rddfMaxSpeed; // we are away from the intersection
				    	  	
				    	//cout << "rddfSpeed = " << rddfSpeed << endl;
					
					  	rddfData.number = rddfNum;
					  	rddfData.Northing = rddfx[ii];
					  	rddfData.Easting = rddfy[ii];
					  	rddfData.maxSpeed = rddfSpeed;
					  	rddfData.radius = rddfr[ii];
					  	rddfData.latitude = latlon.latitude;
					  	rddfData.longitude = latlon.longitude;
				
							if (rddfx.size()==1)
              {
								cerr << "ERROR convRDDF: only one data point in RDDFVector." << endl;
              }
              
					  	newRddf->addDataPoint(rddfData);
					  	previousNorthing = utm.n;
					  	previousEasting = utm.e;
					  	rddfNum++;
						} 
				    else if (sqrt(pow(rddfx[ii]-previousNorthing,2)+pow(rddfy[ii]-previousEasting,2))>EPS)
						{
					  	// convert to rddf format
					  	utm.n = rddfx[ii];
					  	utm.e = rddfy[ii];
					  	gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
					  	
							// Calculate speed using a ramp-down speed profile
							// This speed is dependent on our distance from the exit, whether
							// we want to stop, and Alice's dynamics
							dist2Exit_RDDF = sqrt(pow(CSExitX-rddfx[ii],2) + pow(CSExitY-rddfy[ii],2));
							dist2Exit_shifted_RDDF = sqrt(pow(CSExitX_shifted-rddfx[ii],2) + pow(CSExitY_shifted-rddfy[ii],2));
				  		if ((stopAtEndOfSegment) && (dist2Exit_shifted_RDDF<posStartToBrake) && (dist2Exit_RDDF>ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
				    		rddfSpeed = sqrt(2*ALICE_DESIRED_DECELERATION*dist2Exit_shifted_RDDF); //follow velocity profile
				    	else if ((stopAtEndOfSegment) && (dist2Exit_RDDF<ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))  
				    		rddfSpeed = 0; // we are techinically inside the intersection
				    	else
				    		rddfSpeed = rddfMaxSpeed; // we are away from the intersection
				    	  	
				    	//cout << "rddfSpeed = " << rddfSpeed << endl;
					
					  	rddfData.number = rddfNum;
					  	rddfData.Northing = rddfx[ii];
					  	rddfData.Easting = rddfy[ii];
					  	rddfData.maxSpeed = rddfSpeed;
					  	rddfData.radius = rddfr[ii];
					  	rddfData.latitude = latlon.latitude;
					  	rddfData.longitude = latlon.longitude;
				
					 	 	// Shift the Northing and Easting to those of the rear axle
							//	  double angle = atan2(rddf[ii][1]-previousNorthing, rddf[ii][0]-previousEasting);
							//	  double distBetweenDataPoints = sqrt(pow(rddf[ii][1]-previousNorthing,2) + pow(rddf[ii][0]-previousEasting,2));
							//	  rddfData.Northing = previousNorthing + (distBetweenDataPoints - VEHICLE_WHEELBASE)*sin(angle);
							//	  rddfData.Easting = previousEasting + (distBetweenDataPoints - VEHICLE_WHEELBASE)*cos(angle);
				
							//	  double dist2Exit = sqrt(pow(CSExitWayptX-rddfData.Easting,2) + pow(CSExitWayptY-rddfData.Northing,2));
					
							//	  if (currentSegGoals.stopAtExit && dist2Exit < posBrake)
							//	    rddfMaxSpeed = sqrt(2*ALICE_MAX_DECELERATION*dist2Exit);
				
						  newRddf->addDataPoint(rddfData);
					  	previousNorthing = utm.n;
					  	previousEasting = utm.e;
					  	rddfNum++;
						}
					} 
			  
				  if (rddfError)
				  {
				  	cout << "Error with rddf generation" << endl;
				      // NEED TO SEND mplanner FAILURE MESSAGE
				  }
				
				  /************************************************************************/    
				  /* SEND DATA TO DPLANNER */
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Sending RDDF to dplanner..." << endl;
          }			
				  SendRDDF(rddfSocket, newRddf);
          if (DEBUG)
				  {
            // Print to screen
				    newRddf->print();
				    // Print to file
				    FILE * pFile;
				    pFile = fopen ("rddfFile.txt","w");
				    int numOfWayPts = newRddf->getNumTargetPoints(); 
				    RDDFVector rddfPoints = newRddf->getTargetPoints();
				    for (int ii=0; ii<numOfWayPts; ii++)
            {
              fprintf (pFile, "%i\t%f\t%3.9f\t%3.9f\t%3.9f\t%3.9f\t%f\t%f\t%f\t\n", 
                rddfPoints[ii].number, 
                rddfPoints[ii].Northing, rddfPoints[ii].Easting, 
                rddfPoints[ii].longitude, rddfPoints[ii].latitude, 
                rddfPoints[ii].maxSpeed, rddfPoints[ii].radius);
            }
            fprintf(pFile, "\n");
            fclose (pFile);
          } // if debug
			    	
				  /************************************************************************/    
				  /* DECIDE WHEN SEGMENT COMPLETED */
				  // FOR NOW COMPARE POSITION WITH EXIT_WAYPT AND VELOCITY SHOULD BE SMALL AS WELL
				  #warning "need to work on check for segment complete"
				  //if ((fabs(dist2ExitTest)<1) && ((sqrt(pow(vx_A,2)+pow(vy_A,2)))<1))
				  if (fabs(dist2Exit_shifted)<SEG_COMPLETE_DIST_TO_EXIT || 
				      pow(vx_A,2) + pow(vy_A,2) < STOP_SPEED_SQR && fabs(dist2Exit_shifted)<SEG_ALMOST_COMPLETE_DIST_TO_EXIT)
				    {
				      segGoalsStatus->status = COMPLETED;
				      stayInLoopFlag = false;
              if ((VERBOSE) || (DEBUG))
              {
                cout << "Completed!" << endl;
                cout << "Exiting inner planning loop..." << endl;
              }
				  	  // Sleep more if we have a stop sign.
				      if (currentSegGoals.stopAtExit)
					  	sleep(5);
				    } //else
								
				  usleep(100000);
          
        } //WHILE STAY IN LOOP
					
	      /************************************************************************/    
	      /* SEND STATUS TO MPLANNER */
        if ((VERBOSE) || (DEBUG))
        {
        cout << "Sending status to mplanner...";
        cout << "goal ID = " << segGoalsStatus->goalID << endl;
        }        
	      segGoalsStatus->goalID = currentSegGoals.goalID;
	      bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
	      if (!statusSent)
        {
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Error sending segment complete status to mplanner" << endl;
          }        
          cerr << " Error sending segment complete status to mplanner" << endl;
        }
	      else
        {
          if ((VERBOSE) || (DEBUG))
          {
            cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
          }
        }
	    } 
      else // THIS IS AT THE BEGINNING OR END OF A MISSION
      {
        if (!(currentSegGoals.segment_type==END_OF_MISSION))
		    {
		      double x_A,y_A, vx_A, vy_A;
          if ((VERBOSE) || (DEBUG))
          {
  		      cout << "Beginning of the mission..." << endl;
            cout << "Entering planning loop..." << endl;
          }
		      bool stayInLoopFlag = true;
          
		      while(stayInLoopFlag)
		      {
            /************************************************************************/
            // READ ALICE POSITION
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Read Alice State..." << endl;
            }
            UpdateState(); // from here I get m_state, which has fields m_state.Northing and m_state.Easting
            x_A = m_state.utmNorthing;  // position of the rear axle
            y_A = m_state.utmEasting;
            vx_A = m_state.utmEastVel;
            vy_A = m_state.utmNorthVel;
            if (NOWAIT_FOR_STATE) //revert to default, which is somewhere in Steele lot
            {
              x_A = 396422; 
              y_A = 3778142;
              vx_A = 0;
              vy_A = 0;
            } 
    
            //Find the exit pt, or rather the entry point into the next segment
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Find extry point into the first segment..." << endl;
            }            
            Waypoint* CSEntryWaypt = m_rndf->getWaypoint(currentSegGoals.exitSegmentID, currentSegGoals.exitLaneID, currentSegGoals.exitWaypointID);
            double CSEntryX = CSEntryWaypt->getNorthing();
            double CSEntryY = CSEntryWaypt->getEasting();
            double CSEntryX_shifted, CSEntryY_shifted;
            double dist2Segment1 = sqrt(pow(x_A-CSEntryX,2)+pow(y_A-CSEntryY,2));
            double theta_ave = atan2(y_A-CSEntryY, x_A-CSEntryX);
            // Shift back the segment exit pt by distance ALICE_DIST_REAR_AXLE_TO_FRONT_BUMBER
            CSEntryX_shifted = CSEntryX + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*cos(theta_ave);
            CSEntryY_shifted = CSEntryY + ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*sin(theta_ave);	 
            //printf("Shifted exit point = (%3.6f,%3.6f)\n",CSExitX_shifted, CSExitY_shifted);  
            double dist2Segment1_shifted = sqrt(pow(x_A-CSEntryX_shifted,2)+pow(y_A-CSEntryY_shifted,2));
            if (DEBUG)
            {
              printf("Entry point = (%3.6f, %3.6f)\n",CSEntryX, CSEntryY);
              //printf("ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*cos(theta_ave) = %3.6f \n",ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*cos(theta_ave));
              //printf("ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*sin(theta_ave) = %3.6f \n",ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER*sin(theta_ave));
              printf("Entry point shifted = (%3.6f, %3.6f)\n",CSEntryX_shifted, CSEntryY_shifted);
              cout << "dist2Segment1 = " << dist2Segment1 << endl;
              cout << "dist2Segment1_shifted = " << dist2Segment1_shifted << endl;
            }
                        
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Generate initial rddf..." << endl;
            }
            stopAtEndOfSegment = true;
            double rddfSpeed; 
            double rddfMaxSpeed = 8;
            double posStartToBrake = pow(rddfMaxSpeed,2)/(2*ALICE_DESIRED_DECELERATION); // The position where we want to start slowing down			
            
            if ((stopAtEndOfSegment) && (dist2Segment1_shifted<posStartToBrake) && (dist2Segment1>ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))
            {
              rddfSpeed = sqrt(2*ALICE_DESIRED_DECELERATION*dist2Segment1_shifted); //follow velocity profile
            }
            else if ((stopAtEndOfSegment) && (dist2Segment1<ALICE_DIST_REAR_AXLE_TO_FRONT_BUMPER))  
            {
              rddfSpeed = 0; // we are techinically inside the intersection
            }
            else
            {
              rddfSpeed = rddfMaxSpeed; // we are away from the intersection
            }
            
            // Generate an rddf consisting of just these two points
            RDDF* newRddf = new RDDF();
            RDDFData rddfData;
						
            // Point 1: at Alice's current location
            utm.n = x_A;
            utm.e = y_A;
            gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
            rddfData.number = 1;
            rddfData.Northing = x_A;
            rddfData.Easting = y_A;
            rddfData.maxSpeed = rddfSpeed;
            rddfData.radius = dist2Segment1+LANEWIDTH;
            rddfData.latitude = latlon.latitude;
            rddfData.longitude = latlon.longitude;
            newRddf->addDataPoint(rddfData);  		
            
            // Point 2: at entry point to Segment 1
            utm.n = CSEntryX;
            utm.e = CSEntryY;
            gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);\
            rddfData.number = 2;
            rddfData.Northing = CSEntryX;
            rddfData.Easting = CSEntryY;
            rddfData.maxSpeed = 0;
            rddfData.radius = LANEWIDTH;
            rddfData.latitude = latlon.latitude;
            rddfData.longitude = latlon.longitude;
            newRddf->addDataPoint(rddfData); 
            
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Send rddf to dplanner..." << endl;
            }
            SendRDDF(rddfSocket, newRddf);
            if (DEBUG)
            {
        			newRddf->print();
              // Print to file
              FILE * pFile;
              pFile = fopen ("rddfFile.txt","w");
              RDDFVector rddfPoints = newRddf->getTargetPoints();
              for (int ii=0; ii<2; ii++)
              {
                fprintf (pFile, "%i\t%f\t%3.9f\t%3.9f\t%3.9f\t%3.9f\t%f\t%f\t%f\t\n", 
                  rddfPoints[ii].number,
                  rddfPoints[ii].Northing, rddfPoints[ii].Easting, 
                  rddfPoints[ii].longitude, rddfPoints[ii].latitude, 
                  rddfPoints[ii].maxSpeed,
                  rddfPoints[ii].radius);
              }
              fprintf(pFile, "\n");
              fclose (pFile);
            }    
            // Check to see if we have completed this segment:
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Check if segment completed" << endl;
            }
            
            #warning "Need to work on segment complete check"
            if (fabs(dist2Segment1_shifted)<2)
            {
              segGoalsStatus->status = COMPLETED;
              stayInLoopFlag = false;
              /* SEND STATUS TO MPLANNER */
              if ((VERBOSE)||(DEBUG))
              {
                cout << "Segment completed!" << endl;
                cout << "Sending status to mplanner...";
              }
              segGoalsStatus->goalID = currentSegGoals.goalID;
              if (DEBUG)
              {
                cout << "goal ID = " << segGoalsStatus->goalID << endl;
              }
              bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
              if (!statusSent)
              {
                if ((VERBOSE) || (DEBUG))
                {
                  cout << "Error sending segment complete status to mplanner" << endl;
                }        
                cerr << " Error sending segment complete status to mplanner" << endl;
              }
              else
              {
                if ((VERBOSE) || (DEBUG))
                {
                  cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
                }
              }
              if ((VERBOSE)||(DEBUG))
              {
                cout << "Exiting inner planning loop..." << endl;
              }
            } //if segment is completed
            //sleep(1);
            usleep(100000);
          } // WHILE STAY IN INNER LOOP (for segment 0)
		    } 
        else // THIS IS THE END OF THE MISSION
		    {
          if ((VERBOSE) || (DEBUG))
          {
            cout << "End of the mission..." << endl;
            cout << "letting mplanner know...";
          }
          /* SEND STATUS TO MPLANNER */
          segGoalsStatus->goalID = currentSegGoals.goalID;
          if (DEBUG)
          {
            cout << "goal ID = " << segGoalsStatus->goalID << endl;
          }
          bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
          if (!statusSent)
          {
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Error sending segment complete status to mplanner" << endl;
            }        
            cerr << " Error sending segment complete status to mplanner" << endl;
          }
          else
          {
            if ((VERBOSE) || (DEBUG))
            {
              cout << "Successfully sent status for goal " << segGoalsStatus->goalID << endl;
            }
          }
          if (DEBUG)
          {
		        cout << "Exiting traffic planner" << endl;
          }
		      outerLoopFlag = false;
		    } // if end of mission	
      }  // END: if segment 0
    } // END: if (numOfSegGoals>0)		    	
    else 
    {
      if (DEBUG)
      {
        cout << "No segment goals yet ...  still waiting." << endl;
      }
      outerLoopFlag = true;
    }
    usleep(100000);
  }//WHILE STAY IN OUTER LOOP
}


