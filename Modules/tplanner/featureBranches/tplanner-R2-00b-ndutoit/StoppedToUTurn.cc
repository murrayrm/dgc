#include "StoppedToUTurn.hh"
#include <math.h>
#include <list>


StoppedToUTurn::StoppedToUTurn(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
  ,m_boundStoppedVel(0.2) 
{
}

StoppedToUTurn::StoppedToUTurn()
{

}

StoppedToUTurn::~StoppedToUTurn()
{

}

double StoppedToUTurn::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{
  cout << "in StoppedToUTurn::meetTransitionConditions" <<endl;
  Stopped* cState = static_cast<Stopped*>(controlState);  

  if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {

    //if (SegGoals::UTURN == planHorizon.getSegGoal(0).segment_type){
    //time_t currTime;
    //time(&currTime);
      // wait for three seconds
    //if (currTime-getInitTime()>3){

    //if (cState->getUTurnBool()){
    //  cout << "here now" << endl;
    //}

    if (cState->getUTurnBool()){
      m_probability = 1;
      cState->resetConditions();
    } else {
      m_probability = 0;
    }
    cout << "stopped to uturn m_probability = " << m_probability << endl;
  }
  setUncertainty(m_probability);
  return m_probability;
} 


