#include "Edge.hh"

Edge::Edge(Vertex* vertex1, Vertex* vertex2)
: m_vertex1(vertex1)
, m_vertex2(vertex2)
{  

}


Edge::~Edge()
{  
delete m_vertex1; 
delete m_vertex2; 
}

Vertex* Edge::getInitialVertex()
{  
return m_vertex1; 
}


Vertex* Edge::getFinalVertex()
{  
return m_vertex2; 
} 
 

