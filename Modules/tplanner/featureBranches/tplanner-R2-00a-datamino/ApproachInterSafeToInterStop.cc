#include "ApproachInterSafeToInterStop.hh"
#include <math.h>


ApproachInterSafeToInterStop::ApproachInterSafeToInterStop(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distToStop(1)
{
}

ApproachInterSafeToInterStop::~ApproachInterSafeToInterStop()
{

}

double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
  return 0.0;
} 


double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel ptLabel)
{
  cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() " << endl;
  double distance = 50;
  int stopLineErr = -1;
  point2 stopLinePos;

  //==================================================
  // this is what I need to use
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
  // use getStopline() call that gives next stopline in my lane based on currPos
  stopLineErr = localMap->getStopline(stopLinePos,currRearPos);
  //stopLineErr = localMap->getStopline(stopLinePos, ptLabel);
  //stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);

  distance = stopLinePos.dist(currFrontPos);
  
  cout << "distance to stopline = " << distance << endl;
  //===================================================

  if (distance <= m_distToStop) {
    m_trafTransProb = 1;	
  } else 
    m_trafTransProb = 0;
  setUncertainty(m_trafTransProb);
  return m_trafTransProb;
} 
