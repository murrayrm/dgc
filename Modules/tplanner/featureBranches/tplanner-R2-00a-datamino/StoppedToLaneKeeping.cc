#include "StoppedToLaneKeeping.hh"
#include <math.h>
#include <list>


StoppedToLaneKeeping::StoppedToLaneKeeping(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
{
}

StoppedToLaneKeeping::StoppedToLaneKeeping()
{

}

StoppedToLaneKeeping::~StoppedToLaneKeeping()
{

}

double StoppedToLaneKeeping::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{
  cout << "inside StoppedToLaneKeeping::meetTransitionConditions" <<endl;
  //TODO fix composite state 
  //if (TrafficStateFactory::CLEARREGION == trafficState.getType()) 
  if (TrafficStateFactory::ZONE_REGION == trafficState->getType()) {
    // Check if the intersection is clear
    // TODO: DO THIS PROPERLY
    bool clear = true;
    if (clear) {
      cout  << "in stoppedToLaneKeeping: ZONE_REGION = CLEAR" << endl;
      m_probability = 1;
    } else { m_probability = 0; }
  } else if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    // Check if the intersection is clear
    // TODO: DO THIS PROPERLY
    bool clear = true;
    if (clear) {
      cout  << "in stoppedToLaneKeeping: ROAD_REGION = CLEAR" << endl;
      m_probability = 1;
    } else { 
      m_probability = 0; 
    }
  } else if (TrafficStateFactory::INTERSECTION_STOP == trafficState->getType()) {
    // Check if the intersection is clear
    // TODO: DO THIS PROPERLY
    sleep(3);
    cout << "checking to see if intersection is clear" << endl;
    bool intersectionClear = true;
    // Check if it is our turn
    // TODO: DO THIS PROPERLY
    bool ourTurn = true;

    if ((ourTurn)&&(intersectionClear)) {
      cout  << "in stoppedToLaneKeeping: INTERSECTION_STOP = CLEAR and OURTURN" << endl;
      m_probability = 1;
    } else { 
      m_probability = 0; 
    }
  }
  cout << "probability = " << m_probability << endl;
  //	cout << "SETTING PROBABILITY TO ONE.  THIS NEEDS TO BE FIXED " << endl;
  //m_probability = 1;
  setUncertainty(m_probability);
  return m_probability;
} 


