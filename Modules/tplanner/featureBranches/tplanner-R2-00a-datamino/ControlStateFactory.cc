#include "ControlStateFactory.hh"
#include "ControlStateTransition.hh"

#include "LaneKeeping.hh"
#include "SlowDown.hh"
#include "Stop.hh"
#include "Stopped.hh"
#include "UTurn.hh"

#include "LaneKeepingToSlowDown.hh"
#include "SlowDownToStop.hh"
#include "StopToStopped.hh"
#include "StoppedToLaneKeeping.hh"
#include "StoppedToUTurn.hh"


ControlStateFactory::ControlStateFactory()
{
  //do nothing here 

}


ControlStateFactory::~ControlStateFactory()
{
  //do nothing here 

}

StateGraph ControlStateFactory::createControlStates()
{      
  
  vector<StateTransition*> trans;

  int stateId = 0; //TODO fix this nonsense

  //Control States 
  LaneKeeping* laneKeeping = new LaneKeeping(++stateId, LANE_KEEPING);
  SlowDown* slowDown = new SlowDown(++stateId, SLOW_DOWN);
  Stop* stop = new Stop(++stateId,STOP);
  Stopped* stopped = new Stopped(++stateId, STOPPED);
  UTurn* uTurn = new UTurn(++stateId, UTURN);

  //Control State Transitions
  LaneKeepingToSlowDown* laneKeepSlowTrans = new LaneKeepingToSlowDown (laneKeeping, slowDown);
  SlowDownToStop* slowStopTrans = new SlowDownToStop(slowDown, stop);
  StopToStopped* stopStoppedTrans = new StopToStopped(stop, stopped);
  StoppedToLaneKeeping* stoppedLaneKeepTrans = new StoppedToLaneKeeping(stopped, laneKeeping);
  StoppedToUTurn* stoppedUTurnTrans = new StoppedToUTurn(stopped, uTurn);
 
  trans.push_back(laneKeepSlowTrans);
  trans.push_back(slowStopTrans);
  trans.push_back(stopStoppedTrans);
  trans.push_back(stoppedLaneKeepTrans);
  trans.push_back(stoppedUTurnTrans);

  return StateGraph(trans);
}


int ControlStateFactory::getNextId()
{
  return 0;
}


void ControlStateFactory::print(int type)
{
 cout<<"Control State Type: ";
 switch (type){
 case ControlStateFactory::LANE_KEEPING:
   cout<<"LANE KEEPING"<<endl;
   break;
 case ControlStateFactory::SLOW_DOWN:
   cout<<"SLOW DOWN"<<endl;
   break;
 case ControlStateFactory::STOP:
   cout<<"STOP"<<endl;
   break; 
 case ControlStateFactory::STOPPED:
    cout<<"STOPPED"<<endl;
   break;
 default:
   break; 
 };
}
