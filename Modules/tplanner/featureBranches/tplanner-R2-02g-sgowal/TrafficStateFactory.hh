#ifndef TRAFFICSTATEFACTORY_HH_
#define TRAFFICSTATEFACTORY_HH_

#include <string>
#include "state/StateGraph.hh"
//#include "TrafficState.hh"

class TrafficState;

class TrafficStateFactory {


  public:

    TrafficStateFactory();
    TrafficStateFactory(bool debug, bool verbose, bool log);
    ~TrafficStateFactory();
    static typedef enum { ROAD_REGION, ZONE_REGION, APPROACH_INTER_SAFETY, INTERSECTION_STOP } TrafficStateType;
    static StateGraph createTrafficStates();
    static int getNextId();
    static void print(int type);
    static string printString(int type);
    static TrafficState *getInitialState();
    
  private:
  
    int m_stateId;
    bool m_debug, m_verbose, m_log;
    static TrafficState *m_initState;
};

#endif                          /*TRAFFICSTATEFACTORY_HH_ */
