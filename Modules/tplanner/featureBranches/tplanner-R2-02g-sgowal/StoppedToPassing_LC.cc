#include "StoppedToPassing_LC.hh"

StoppedToPassing_LC::StoppedToPassing_LC(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

StoppedToPassing_LC::StoppedToPassing_LC()
{

}

StoppedToPassing_LC::~StoppedToPassing_LC()
{

}

double StoppedToPassing_LC::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToPassing_LC::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        SegGoals seg = planHorizon.getSegGoal(0);

        string lt, rt;
        LaneLabel label(seg.entrySegmentID, seg.entryLaneID);
        map->getLeftBoundType(lt, label);
        map->getRightBoundType(rt, label);
        bool passing_allowed = seg.illegalPassingAllowed || lt.compare("broken_white")==0 || rt.compare("broken_white")==0;
        
        if (passing_allowed) {
            m_prob = 1;
        }
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "StoppedToPassing_LC.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
