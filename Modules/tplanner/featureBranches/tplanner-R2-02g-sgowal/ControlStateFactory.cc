#include "ControlStateFactory.hh"
#include "ControlStateTransition.hh"

#include "LaneKeeping.hh"
#include "Stop.hh"
#include "Stopped.hh"
#include "UTurn.hh"
#include "LaneChange.hh"
#include "Creep.hh"

#include "LaneKeepingToStop.hh"
#include "LaneKeepingToLaneChange.hh"
#include "StopToStopped.hh"
#include "StoppedToLaneKeeping.hh"
#include "StoppedToUTurn.hh"
#include "UTurnToLaneKeeping.hh"
#include "UTurnToUTurn.hh"
#include "LaneChangeToLaneKeeping.hh"
#include "Passing1_LaneKeepingToLaneChange.hh"
#include "Passing3_LaneKeepingToLaneChange.hh"
#include "StoppedToPassing_LC.hh"
#include "Intersection_LaneKeepingToStop.hh"
#include "Passing_LaneKeepingToLaneKeeping.hh"
#include "CreepToStop.hh"
#include "StoppedToCreep.hh"

ControlState* ControlStateFactory::m_initState;

ControlStateFactory::ControlStateFactory()
{

}

ControlStateFactory::ControlStateFactory(bool debug, bool verbose, bool log)
{
    m_debug = debug;
    m_verbose = verbose;
    m_log = log;
	
    // We need to set these variables in ControlState and ControlStateTransition too
    ControlState::setOutputParams(debug, verbose, log);
    ControlStateTransition::setOutputParams(debug, verbose, log);
}


ControlStateFactory::~ControlStateFactory()
{

}

StateGraph ControlStateFactory::createControlStates()
{

    vector < StateTransition * >trans;

    int stateId = 0;            // TODO fix this nonsense

    // Control States
    // Road region
    LaneKeeping *laneKeeping = new LaneKeeping(++stateId, LANE_KEEPING);
    m_initState = laneKeeping;
    LaneChange *laneChange = new LaneChange(++stateId, LANE_CHANGE);
    LaneChange *passing1_LC = new LaneChange(++stateId, LANE_CHANGE);
    LaneKeeping *passing2_LK = new LaneKeeping(++stateId, LANE_KEEPING);
    LaneChange *passing3_LC = new LaneChange(++stateId, LANE_CHANGE);
    Stop *stopObs = new Stop(++stateId, STOP);
    Stopped *stoppedObs = new Stopped(++stateId, STOPPED, Stopped::OBSTACLE);
    UTurn *uTurn1 = new UTurn(++stateId, UTURN, 1);
    UTurn *uTurn2 = new UTurn(++stateId, UTURN, 2);
    UTurn *uTurn3 = new UTurn(++stateId, UTURN, 3);
    UTurn *uTurn4 = new UTurn(++stateId, UTURN, 4);
    UTurn *uTurn5 = new UTurn(++stateId, UTURN, 5);

    // Approach intersection
    Stop *stop = new Stop(++stateId, STOP);
    Stopped *stopped = new Stopped(++stateId, STOPPED, Stopped::INTERSECTION);
    Creep *creep = new Creep(++stateId, CREEP);

    // Control State Transitions
    // Nominal driving
    LaneKeepingToStop *laneKeepStopObsTrans = new LaneKeepingToStop(laneKeeping, stopObs);
    LaneKeepingToLaneChange *laneKeepLaneChangeTrans = new LaneKeepingToLaneChange(laneKeeping, laneChange);
    LaneChangeToLaneKeeping *laneChangeLaneKeepTrans = new LaneChangeToLaneKeeping(laneChange, laneKeeping);

    // Obstacle handling
    UTurnToUTurn *uturnUTurnTrans1 = new UTurnToUTurn(uTurn1, uTurn2, 1);
    UTurnToUTurn *uturnUTurnTrans2 = new UTurnToUTurn(uTurn2, uTurn3, 2);
    UTurnToUTurn *uturnUTurnTrans3 = new UTurnToUTurn(uTurn3, uTurn4, 3);
    UTurnToUTurn *uturnUTurnTrans4 = new UTurnToUTurn(uTurn4, uTurn5, 4);
    UTurnToLaneKeeping *uTurnLaneKeepTrans = new UTurnToLaneKeeping(uTurn5, laneKeeping);

    Passing1_LaneKeepingToLaneChange *laneKeepPassing1Trans = new Passing1_LaneKeepingToLaneChange(laneKeeping, passing1_LC);
    LaneChangeToLaneKeeping *passing1Passing2Trans = new LaneChangeToLaneKeeping(passing1_LC, passing2_LK);
    Passing3_LaneKeepingToLaneChange *passing2Passing3Trans = new Passing3_LaneKeepingToLaneChange(passing2_LK, passing3_LC);
    LaneChangeToLaneKeeping *passing3LaneKeepTrans = new LaneChangeToLaneKeeping(passing3_LC, laneKeeping);
    // Obstacle handling while passing
    Passing_LaneKeepingToLaneKeeping *passingLaneKeepLaneKeepTrans = new Passing_LaneKeepingToLaneKeeping(passing2_LK, laneKeeping);

    StopToStopped *stopStoppedObsTrans = new StopToStopped(stopObs, stoppedObs);
    StoppedToUTurn *stoppedObsUTurnTrans = new StoppedToUTurn(stoppedObs, uTurn1);
    StoppedToPassing_LC *stoppedObsPassing1Trans = new StoppedToPassing_LC(stoppedObs, passing1_LC);

    // intersection handling
    Intersection_LaneKeepingToStop *inter_LaneKeepStopTrans = new Intersection_LaneKeepingToStop(laneKeeping, stop);
    StoppedToLaneKeeping *stoppedLaneKeepTrans = new StoppedToLaneKeeping(stopped, laneKeeping);
    StopToStopped *stopStoppedTrans = new StopToStopped(stop, stopped);
    StoppedToCreep *stoppedCreepTrans = new StoppedToCreep(stopped, creep);
    CreepToStop *creepStopTrans = new CreepToStop(creep, stop);

    //    trans.push_back(laneKeepSlowTrans);
    trans.push_back(laneKeepStopObsTrans);
    trans.push_back(inter_LaneKeepStopTrans);
    trans.push_back(laneKeepLaneChangeTrans);
    trans.push_back(laneChangeLaneKeepTrans);
    trans.push_back(stopStoppedObsTrans);
    trans.push_back(stoppedObsUTurnTrans);
    trans.push_back(stopStoppedTrans);
    trans.push_back(stoppedLaneKeepTrans);
    trans.push_back(stoppedObsPassing1Trans);
    trans.push_back(uturnUTurnTrans1);
    trans.push_back(uturnUTurnTrans2);
    trans.push_back(uturnUTurnTrans3);
    trans.push_back(uturnUTurnTrans4);
    trans.push_back(uTurnLaneKeepTrans);
    trans.push_back(laneKeepPassing1Trans);
    trans.push_back(passing1Passing2Trans);
    trans.push_back(passing2Passing3Trans);
    trans.push_back(passing3LaneKeepTrans);
    trans.push_back(passingLaneKeepLaneKeepTrans);
    trans.push_back(stoppedCreepTrans);
    trans.push_back(creepStopTrans);

    return StateGraph(trans);
}

ControlState* ControlStateFactory::getInitialState()
{
    return m_initState;
}

int ControlStateFactory::getNextId()
{
    return 0;
}


void ControlStateFactory::print(int type)
{
    cout << "Control State Type: ";
    switch (type) {
    case ControlStateFactory::LANE_KEEPING:
        cout << "LANE KEEPING" << endl;
        break;
    case ControlStateFactory::LANE_CHANGE:
        cout << "LANE_CHANGE" << endl;
        break;
    case ControlStateFactory::STOP:
        cout << "STOP" << endl;
        break;
    case ControlStateFactory::STOPPED:
        cout << "STOPPED" << endl;
        break;
    case ControlStateFactory::UTURN:
        cout << "UTURN" << endl;
        break;
    case ControlStateFactory::CREEP:
        cout << "CREEP" << endl;
        break;
    default:
        cout << "ControlStateFactory: Control state not defined in print function" << endl;
        break;
    };
}

string ControlStateFactory::printString(int type)
{
    string cstate_type;
    switch (type) {
    case ControlStateFactory::LANE_KEEPING:
        cstate_type = string("LANE KEEPING");
        break;
    case ControlStateFactory::LANE_CHANGE:
        cstate_type = string("LANE CHANGE");
        break;
    case ControlStateFactory::STOP:
        cstate_type = string("STOP");
        break;
    case ControlStateFactory::STOPPED:
        cstate_type = string("STOPPED");
        break;
    case ControlStateFactory::UTURN:
        cstate_type = string("UTURN");
        break;
    case ControlStateFactory::CREEP:
        cstate_type = string("CREEP");
        break;
    default:
        cstate_type = string("INVALID");
        cerr << "TrafficStateFactory::printString: state type not recognized" << endl;
        break;
    };
    return cstate_type;
}
