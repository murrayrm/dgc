#ifndef CONTROLSTATEFACTORY_HH_
#define CONTROLSTATEFACTORY_HH_


#include "state/StateGraph.hh"
//#include "ControlState.hh"

class ControlState;

class ControlStateFactory {


  public:

    ControlStateFactory();
    ControlStateFactory(bool debug, bool verbose, bool log);
    ~ControlStateFactory();
	
    static typedef enum {LANE_KEEPING, STOP, STOPPED, UTURN, LANE_CHANGE, CREEP} ControlStateType;
    static StateGraph createControlStates();
    static int getNextId();
    static void print(int type);
    static string printString(int type);
    static ControlState *getInitialState();

  private:

    bool m_debug;
    bool m_verbose;
    bool m_log;
    int m_stateId;
    static ControlState *m_initState;

};

#endif                          /*CONTROLSTATEFACTORY_HH_ */
