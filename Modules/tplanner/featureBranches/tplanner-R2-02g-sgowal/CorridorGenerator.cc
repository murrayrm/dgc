#include "CorridorGenerator.hh"

CorridorGenerator::CorridorGenerator(int skynetKey, bool debug, bool verbose, bool log)
  : GcModule("CorridorGenerator", &m_controlStatus, &m_mergedDirective, 100000, 10000)
  , m_snKey(skynetKey)
  , m_verbose(verbose)
  , m_debug(debug)
  , m_log(log)
  , m_latestID(0)
  , m_currentID(0)
{


  m_traffCorrInterfaceNF = TrafficCorridorInterface::generateNorthface(skynetKey, this);

  m_corrTrajPlannerSF = CorrTrajPlannerInterface::generateSouthface(skynetKey, this);

  m_corrTrajPlannerPauseSF = CorrTrajPlannerPauseInterface::generateSouthface(skynetKey, this);
    
  m_corrTrajPlannerEndMissionSF = CorrTrajPlannerEndMissionInterface::generateSouthface(skynetKey, this);

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = CorrGenControlStatus::READY_FOR_NEXT;

}

CorridorGenerator::~CorridorGenerator()
{
  TrafficCorridorInterface::releaseNorthface(m_traffCorrInterfaceNF);
  CorrTrajPlannerInterface::releaseSouthface(m_corrTrajPlannerSF);
  CorrTrajPlannerPauseInterface::releaseSouthface(m_corrTrajPlannerPauseSF);
  CorrTrajPlannerEndMissionInterface::releaseSouthface(m_corrTrajPlannerEndMissionSF);

}

void CorridorGenerator::CorridorGeneratorLoop(void)
{
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective );
      control(&m_controlStatus, &m_mergedDirective );
      usleep(100000);
    }
}

void CorridorGenerator::arbitrate(ControlStatus* cs, MergedDirective* md) {

  CorrGenControlStatus *controlStatus =
    dynamic_cast<CorrGenControlStatus *>(cs);
  CorrGenMergedDirective *mergedDirective =
    dynamic_cast<CorrGenMergedDirective *>(md);

  CorrGenControlStatus corrGenControlStatus; 


}

void CorridorGenerator::control(ControlStatus* cs, MergedDirective* md) {

  CorrGenControlStatus* controlStatus =
    dynamic_cast<CorrGenControlStatus *>(cs);
  CorrGenMergedDirective* mergedDirective =
    dynamic_cast<CorrGenMergedDirective *>(md);



  //m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);
    //RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
    //SendRDDF(rddfSocket,rddf);
    //corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state.localX, m_state.localY, localMap);      
    //CDeltaList* deltaList = NULL;
    //unsigned long long timestamp;
    //DGCgettime(timestamp);
    //deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
    //SendMapdelta(staticCostMapSocket, deltaList);
    //m_costMap->resetDelta<double>(m_costLayerID);
                      // send the parameters
    //corridor.convertOCPtoGlobal(m_gloToLocalDelta);
    //bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
    //if (!sendOCPparamsErr){
    // cerr << "TrafficPlanner: problem with send OCP parameters" << endl;
    // cout << "TrafficPlanner: problem with send OCP parameters" << endl;
    //}

}

Conflict CorridorGenerator::evaluateDynamicConflicts(Corridor m_currCorridor, ControlState *controlState) 
{
  Conflict conflict;
  return conflict;
}

void CorridorGenerator::evaluateCorridors() 
{
  //return m_currCorridor;
}

