#include "Pause.hh"

Pause::Pause(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
, m_desDecc(-0.5)
{

}

Pause::~Pause()
{

}

int Pause::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  cout<<"PAUSE.determineCorridor - traffState type "<<traffState->toString() << endl;;
  
  int error = 0;
  cout << "HERE NOW!!" << endl;

  SegGoals currSegment = planHoriz.getSegGoal(0);
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
  
  bool foundTraffState = true; 

  switch (traffState->getType()) {
    
  case TrafficStateFactory::ZONE_REGION:
    {
      error += CorridorGen::makeCorridorZonePause(corr, vehState);
      error += CorridorGen::setFinalCondPause(corr, vehState, localMap);    
      //    cout << "error = " << error << endl;
      // cout << "setting final conditions" << endl;
      //error += CorridorGen::setFinalCondStopped(corr, vehState);
      //cout << "error = " << error << endl;
    }
    break;
    
  case TrafficStateFactory::ROAD_REGION:
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      bool isReverse = false;
      error += CorridorGen::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
      error += CorridorGen::setFinalCondPause(corr, vehState, localMap);
    }
    break;
    
  case TrafficStateFactory::INTERSECTION_STOP:
    {
      SegGoals nextSegment = planHoriz.getSegGoal(1);
      error += CorridorGen::makeCorridorIntersection(corr, currFrontPos, localMap, currSegment);
      error += CorridorGen::setFinalCondPause(corr, vehState, localMap);
    }
    break;        
  default:    
    {
      cerr << "Pause.cc: Undefined Traffic state" << endl;
      foundTraffState = false;
      error += 1;
      break;
    }
  }

  return error;
}
