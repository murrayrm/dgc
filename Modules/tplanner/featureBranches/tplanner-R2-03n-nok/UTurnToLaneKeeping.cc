#include "UTurnToLaneKeeping.hh"
#include "UTurn.hh"

UTurnToLaneKeeping::UTurnToLaneKeeping(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{
    m_distToTrans = 1;
}

UTurnToLaneKeeping::UTurnToLaneKeeping()
{

}

UTurnToLaneKeeping::~UTurnToLaneKeeping()
{

}

double UTurnToLaneKeeping::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * localMap, VehicleState vehState)
{
    UTurn *cState = static_cast < UTurn * >(controlState);
    SegGoals currSegGoal = planHorizon.getSegGoal(0);

    cout << "in UTurnToLaneKeeping::meetTransitionConditions" << endl;
    point2 exitWayPt;
    PointLabel ptLabel = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);

    // We transition to LaneKeeping from any TrafficState
    localMap->getWaypoint(exitWayPt, ptLabel);
    double angle;
    localMap->getHeading(angle, ptLabel);
    cout << "heading = " << angle << endl;
    double dotProd = (-exitWayPt.x + currFrontPos.x) * cos(angle) + (-exitWayPt.y + currFrontPos.y) * sin(angle);
    cout << "in UTurnToLaneKeeping: dot product  = " << dotProd << endl;

    if ((dotProd > -m_distToTrans) || (!(SegGoals::UTURN == currSegGoal.segment_type))) {
        m_probability = 1;
        cState->resetParameters();
	AliceStateHelper::setDesiredLaneLabel(LaneLabel(ptLabel.segment,ptLabel.lane));
    } else {
        m_probability = 0;
    }

    setUncertainty(m_probability);
    return m_probability;
}
