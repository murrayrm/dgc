#ifndef PAUSE_HH_
#define PAUSE_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class Pause:public ControlState {

  public:

    Pause(int stateId, ControlStateFactory::ControlStateType type);
    ~Pause();
    int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);

  ControlState* newCopy() {
    return  (ControlState*) new Pause(*this);
  }
  
  private:
    double m_desDecc;
    
};

#endif                          /*PAUSE_HH_ */
