#ifndef GEOMETRICCONSTRAINTS_HH_
#define GEOMETRICCONSTRAINTS_HH_

#include "LinearInequality.hh"

class GeometricConstraints {

	public:
	

	GeometricConstraints();
	~GeometricConstraints();
	GeometricConstraints(std::vector<LinearInequality> planes);
	
	GeometricConstraints(const GeometricConstraints&);
	
	std::vector<LinearInequality> getLinearInequalities();
   	double getGeometricUncertainty();

  	/** 
   	*  Projects the geometry defined by planes 3D object to the plane and returns a nx3 matrix.
   	*  
   	*/
   	std::vector<LinearInequality> projectToTwoDimensions();
    
  private:
    /** 
   	*  This is an nx4 matrix that will be populated like: 
   	*  a_11 a_12 a_13 b_1
   	*  a_21 a_22 a_23 b_2 
   	*  .... 
   	*  the a_n3 coefficient corresponds to the height coordinate. 
   	*/
  	std::vector<LinearInequality> m_planes;
  	double m_geoUncert;
  
};
#endif /*GEOMETRICCONSTRAINTS_HH_*/
