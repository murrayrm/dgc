#ifndef PASSING3_LANEKEEPINGTOLANECHANGE_HH_
#define PASSING3_LANEKEEPINGTOLANECHANGE_HH_

#include "ControlStateTransition.hh"
#include "LaneChange.hh"
#include "LaneKeeping.hh"

class Passing3_LaneKeepingToLaneChange:public ControlStateTransition {

public:
  Passing3_LaneKeepingToLaneChange(ControlState *state1, ControlState *state2);
  Passing3_LaneKeepingToLaneChange();
  ~Passing3_LaneKeepingToLaneChange();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
