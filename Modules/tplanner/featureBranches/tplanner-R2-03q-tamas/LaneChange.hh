#ifndef LANECHANGE_HH_
#define LANECHANGE_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"
#include "CorridorGen.hh"

class LaneChange:public ControlState {

  public:

    LaneChange(int stateId, ControlStateFactory::ControlStateType type);
    ~LaneChange();
    int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
  void setReverse(int reverse);
  int getReverse();

private:
  int isReverse; //stores what lane is reverse
  //1: destination, 0: none, -1: initial
  ControlState* newCopy() {
    return  (ControlState*) new LaneChange(*this);
  }
};

#endif                          /*LANECHANGE_HH_ */
