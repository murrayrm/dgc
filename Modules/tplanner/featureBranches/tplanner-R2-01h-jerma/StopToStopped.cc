#include "StopToStopped.hh"
#include <math.h>
#include <list>


StopToStopped::StopToStopped(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
  ,m_boundStoppedVel(0.2) 
{
}

StopToStopped::StopToStopped()
{

}

StopToStopped::~StopToStopped()
{

}

double StopToStopped::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{
  if ((m_verbose) || (m_debug)){  
    cout << "in StopToStopped::meetTransitionConditions" <<endl;
  }
  double vel = 0; 

  //TODO fix composite state 
  //if (TrafficStateFactory::CLEARREGION == trafficState.getType()) 
  if (TrafficStateFactory::INTERSECTION_STOP == trafficState->getType()) {
    vel = AliceStateHelper::getVelocityMag(vehState);
    
    if (vel <= m_boundStoppedVel ) {
      if ((m_verbose) || (m_debug)){  
        cout << "STOPPED!!" << endl;
      }
      m_probability = 1;
    } else { m_probability = 0; }
  } else if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    vel = AliceStateHelper::getVelocityMag(vehState);
    
    if (vel <= m_boundStoppedVel ) {
      if ((m_verbose) || (m_debug)){  
        cout << "STOPPED!!" << endl;
      }
      m_probability = 1;
    } else { m_probability = 0; }
  }
  setUncertainty(m_probability);
  return m_probability;
} 


