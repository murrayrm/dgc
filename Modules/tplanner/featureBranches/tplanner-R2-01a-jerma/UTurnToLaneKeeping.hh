#ifndef UTURNTOLANEKEEPING_HH_
#define UTURNTOLANEKEEPING_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class UTurnToLaneKeeping : public ControlStateTransition {


public: 

  UTurnToLaneKeeping(ControlState* state1, ControlState* state2);

  UTurnToLaneKeeping();

  ~UTurnToLaneKeeping();

  double meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon, Map* map, VehicleState vehState);



private: 



};
#endif /*UTURNTOLANEKEEPING_HH_*/
