#include "TrafficManager.hh"

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log)
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_snKey(skynetKey)
    , m_uniqueMergedDirID(0)
    , m_prevMergedDirectiveID(0)
    , m_isEndMission(false)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_logData(log)
    , m_latestID(0)
    , m_currentID(0)
{

  m_controlGraph = ControlStateFactory::createControlStates();

  m_estimator = new TrafficStateEst(skynetKey, true, debug, verbose, log);

  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionTrafficInterface::generateNorthface(skynetKey, this);

  /* Initialize ControlStatus to wait until a command is received */
  //m_controlStatus.status = TrafficManagerControlStatus::READY_FOR_NEXT;

  m_traffCorrInterfaceSF = TraffCorridorInterface::generateSouthface(skynetKey, this);

  if (true == log) {
    m_logger = new Log();
  }

}

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log, FILE* logFile)
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_snKey(skynetKey)
    , m_uniqueMergedDirID(0)
    , m_prevMergedDirectiveID(0)
    , m_isEndMission(false)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_logData(log)
    , m_logFile(logFile)
    , m_latestID(0)
    , m_currentID(0)
{

  m_controlGraph = ControlStateFactory::createControlStates();

  m_estimator = new TrafficStateEst(skynetKey, true, debug, verbose, log);

  /*!\param GcInterface variable */
  m_missTraffInterfaceNF = MissionTrafficInterface::generateNorthface(skynetKey, this);

  /* Initialize ControlStatus to wait until a command is received */
  //m_controlStatus.status = TrafficManagerControlStatus::READY_FOR_NEXT;

  m_traffCorrInterfaceSF = TraffCorridorInterface::generateSouthface(skynetKey, this);

  if (true == log) {
    m_logger = new Log();
  }

}

TrafficManager::~TrafficManager()
{

  MissionTrafficInterface::releaseNorthface(m_missTraffInterfaceNF);
  TraffCorridorInterface::releaseSouthface(m_traffCorrInterfaceSF);
  //TODO close file before deleting
  delete m_logFile;
  delete m_estimator; 
  delete m_logger;
}

void TrafficManager::TrafficPlanningLoop(void)
{
  while(true)
    {
      arbitrate(&m_controlStatus, &m_mergedDirective );
      control(&m_controlStatus, &m_mergedDirective );
      usleep(100000);
    }
}

void TrafficManager::arbitrate(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus *controlStatus = dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective *mergedDirective = dynamic_cast<TrafficManagerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus; 

  bool meetsPlanHorizReqs = false;
  PointLabel exitWayptLabel; 

  /* If we are executing, then do not try to get another directive */
  //  if (controlStatus->status == TrafficManagerControlStatus::EXECUTING) {
  //if (m_logData)
  //  {
  //	fprintf( m_logFile, "  Control status: goalID = %d\t status = %d\n", controlStatus->ID, controlStatus->status );
  //  }
  // return;
  //} //end EXECUTING
  
  
  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == TrafficManagerControlStatus::COMPLETED ||
      controlStatus->status == TrafficManagerControlStatus::FAILED) {
    
    if (controlStatus->status == TrafficManagerControlStatus::COMPLETED) {  
      for (unsigned int i=0; i < controlStatus->getSegGoalsSize(); i++) {
	segGoalsStatus.status = SegGoalsStatus::COMPLETED;
	segGoalsStatus.goalID = controlStatus->segGoalsIDs.front();
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
	controlStatus->segGoalsIDs.pop_front();
      }
      /* Now we must pop the previously merged dir off the queue, we can assume it's the front of the queue */
      if (controlStatus->ID == m_prevMergedDirective.id) {
	//m_contrDirectiveQ.pop_front();
	/* Do something to continue */
      } else {
	/* We have a problem, we didn't get a status back for this id so we won't mind resending in control */
      }

    } else if (controlStatus->status == TrafficManagerControlStatus::FAILED)  {
      
      for (unsigned int i=0; i < controlStatus->getSegGoalsSize(); i++) {
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	segGoalsStatus.goalID = controlStatus->segGoalsIDs.front();
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
	controlStatus->segGoalsIDs.pop_front();
      }     
   
      /* Flush out the NF queue and send failure status */
      while (m_missTraffInterfaceNF->haveNewDirective()) {
	SegGoals newDirective;
	m_missTraffInterfaceNF->getNewDirective( &newDirective );
	segGoalsStatus.goalID = newDirective.goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	m_missTraffInterfaceNF->sendResponse( &segGoalsStatus );
      }
  
      /* Flush out the accumulating segGoals control queue and send failure status */
      while (m_accSegGoalsQ.size() > 0) {
	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	m_accSegGoalsQ.pop_front();
      }
    }
  }

  /* Wait for Route Planner to send a new directive */
  usleep(500000);

  /* Get all the new directives and put it in the queue */
  while (m_missTraffInterfaceNF->haveNewDirective()) {
    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective(&newDirective);

    if (SegGoals::PAUSE == newDirective.segment_type) {
      SegGoalsStatus segGoalsStatus;
      TrafficManagerMergedDirective mergedDir;
      mergedDirective->id = getNextUniqueMergedDirID();
      mergedDirective->segType = TrafficManagerMergedDirective::PAUSE;
      mergedDirective->segGoalsIDs.push_back(newDirective.goalID);
      mergedDirective->exitSegmentID = newDirective.exitSegmentID;
      mergedDirective->exitLaneID = newDirective.exitLaneID;
      mergedDirective->exitWaypointID = newDirective.exitWaypointID;


      /* First flush the queue where the seg goals are accumulating */
      while (m_accSegGoalsQ.size()> 0) {
	segGoalsStatus.goalID = m_accSegGoalsQ.front().goalID;
	segGoalsStatus.status = SegGoalsStatus::FAILED;
	m_missTraffInterfaceNF->sendResponse(&segGoalsStatus);
	m_accSegGoalsQ.pop_front();
      }

    } else if (SegGoals::PAUSE != newDirective.segment_type) {

      /* Push back onto m_accSegGoalsQ */
      if (newDirective.goalID > m_accSegGoalsQ.back().goalID) {
	m_accSegGoalsQ.push_back(newDirective);
      }else {
	/* The newDirective's ID is smaller or same as the m_accSegGoals last seg goal */
      }


      /* Determine traffic state */
      m_currTraffState = m_estimator->determineTrafficState(exitWayptLabel);
      
      /* Update the current planning horizon and populate m_planHoriz */
      meetsPlanHorizReqs = determinePlanningHorizon(m_accSegGoalsQ);
      
      /* Do not create a new merged directive until we have the correct planning horizon */
      if(meetsPlanHorizReqs) { /* if we had enough segGoals to be able to plan properly */
	//m_currMergedPlanHoriz(m_currPlanHorizon);  //TODO implement copy constructor for planning horizon 
	for (int i = 0; i < m_currMergedPlanHoriz.getSize(); i++) {
	  mergedDirective->id = getNextUniqueMergedDirID();
	  mergedDirective->segGoalsIDs.push_back(m_accSegGoalsQ.front().goalID);
	  /* want to flush the the queue where we are accumulating seggoals */
	  m_accSegGoalsQ.clear();
	}
      } else { /* If the plan horiz is not met  */

      }
    }
  }
  //TODO implement if the queue gets too large
}

void TrafficManager::control(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus* controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective* mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);

  PointLabel endMissionWayptLabel, exitWayptLabel;
  Map* localMap = m_estimator->getMapAtLastEst();

  CorridorCreate cc;
  TrafficManagerMergedDirective mergedDir; 

  //int mergedDirSize = m_contrDirectiveQ.size();
  
  /* First check to see if the previous directive has been completed */
  if (m_traffCorrInterfaceSF->isStatus(CorridorCreateStatus::COMPLETED, m_prevMergedDirective.id)) {
      if (m_logData) {

      }     
      controlStatus->status = TrafficManagerControlStatus::COMPLETED;
      controlStatus->ID = m_prevMergedDirective.id;
  } 
  
  /* If we do not have merged directives, then we want to wait */
  //if (0 == mergedDirSize ) { 
  //   return;
  // }
  

  if (TrafficManagerMergedDirective::PAUSE == mergedDirective->segType) {
    
    /* If there is only one merged Directive then get it and send a corridor pause directive */
    cc.name = CorridorCreate::PAUSE;
    cc.ID = mergedDirective->id; 
    m_prevMergedDirective.id = mergedDirective->id;
    
    /* Get into the STOP control action, once we get stauts back of successful PAUSE, then we reinitialize the control action*/
    //m_currContrState =  this is the STOP state TODO determine this stop state
    
    m_traffCorrInterfaceSF->sendDirective(&cc);

    m_logger->logString("TRAFFMGR: Sent PAUSE directive");
    m_logger->logTrafficState(m_currTraffState, m_currContrState, m_currMergedPlanHoriz, localMap);
    
  } else {
    
    /* Check to see if the last directive was met */
    if (m_prevMergedDirectiveID < mergedDirective->id) {
      if ((TrafficManagerMergedDirective::END_OF_MISSION != mergedDirective->segType) 
	  && (TrafficManagerMergedDirective::PAUSE != mergedDirective->segType))  {
	
	/* Update the current control action */
	determineControlAction();
	
	/* exitPtLabel corresponds to the exit way point of the segGoal determined in determinePlanningHorizon */
	exitWayptLabel = PointLabel(m_currMergedPlanHoriz.getCurrentExitSegment()
				    , m_currMergedPlanHoriz.getCurrentExitLane(), m_currMergedPlanHoriz.getCurrentExitWaypoint());
	
	/* Set the previous merged directive member */
	m_prevMergedDirectiveID = mergedDirective->id;
	
	/* Now we want to send merged directive */
	cc.name = CorridorCreate::CORRIDOR_CREATE;
	cc.ID =  m_prevMergedDirectiveID;
	cc.exitPtLabel = exitWayptLabel; 
	cc.traffState = m_currTraffState;
	cc.controlState = m_currContrState; 
	cc.planHorizon = m_currPlanHorizon;
	
	m_traffCorrInterfaceSF->sendDirective(&cc);

	m_logger->logString("TRAFFMGR: Sent directive:");
	m_logger->logTrafficState(m_currTraffState, m_currContrState, m_currMergedPlanHoriz,localMap);

      } else if (TrafficManagerMergedDirective::END_OF_MISSION == mergedDirective->segType) {
	m_isEndMission = true;
	cc.name = CorridorCreate::END_OF_MISSION;
	cc.ID = mergedDirective->id;
	
	m_logger->logString("TRAFFMGR: Sent END_OF_MISSION directive.");
	m_logger->logTrafficState(m_currTraffState, m_currContrState, m_currMergedPlanHoriz, localMap);

	/* TODO: Get the correct way point */
	/* Get the exit waypoint from the seg goal preceding end of mission */
	//endMissionWayptLabel = PointLabel(m_contrDirectiveQ.front().exitSegmentID, 
	//			  m_contrDirectiveQ.front().exitLaneID,  
	//				  m_contrDirectiveQ.front().exitWaypointID);
	//cc.exitPtLabel = endMissionWayptLabel; 
      } 
      
    }          
    if (m_logData)
      {

      }      
  }
}

    
ControlState* TrafficManager::determineControlAction() 
{
  ControlState* controlState;

  /* Get the vehicle state and the map at the time of the last traffic state estimation */
  VehicleState vehState = m_estimator->getVehStateAtLastEst();
  Map* localMap = m_estimator->getMapAtLastEst();

  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(m_currContrState); 

  unsigned int i = 0;
  ControlStateTransition* transition;

  for (i; i < transitions.size(); i++) 
{		
  transition = static_cast<ControlStateTransition*> (transitions[i]);
  transition->meetTransitionConditions(m_currContrState, m_currTraffState, m_currPlanHorizon, localMap, vehState);
}
  
  controlState = chooseMostProbableControlState(m_currContrState, transitions);
  
  if (m_currContrState->getType() == controlState->getType())
    {
      controlState = m_currContrState;
    } else {
      VehicleState currVehState = m_estimator->getVehStateAtLastEst();
      controlState->setInitialPosition(currVehState);
      controlState->setInitialVelocity(currVehState);
      controlState->setInitialTime();
    }
  
  return controlState; 
}


ControlState* TrafficManager::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
  ControlState* currControlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficManager::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
      currControlState = controlState; 
    }
  } else { 
    if ((m_verbose) || (m_debug)){  
      cout << "in TrafficManager::chooseMostProbable... received transitions of size " << transitions.size() << endl;
    }
  }
  
  return currControlState;
}


bool TrafficManager::determinePlanningHorizon(deque<SegGoals> currSegGoals) 
{
  TrafficStateFactory::TrafficStateType tStateType = 
    (TrafficStateFactory::TrafficStateType) m_currTraffState->getType();
  
  m_currPlanHorizon.clear();
  
  bool meetsPlanHorizonReqs = false; 

  if (currSegGoals.size() > 0 ) {
    /* We need to make sure the currSegGoals have the correct size or else we will seg fault */
    switch(tStateType) {
    case TrafficStateFactory::ZONE_REGION:// want to plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	currSegGoals.pop_front();
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
      }
      break;
    case TrafficStateFactory::ROAD_REGION:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
      }
      break;
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      // plan over one segment
      m_numSegs = 1;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
      }
      break;
    case TrafficStateFactory::INTERSECTION_STOP:
      // plan over two segments
      m_numSegs = 2;
      if (currSegGoals.size() >= m_numSegs ) {
	meetsPlanHorizonReqs = true;
	m_currPlanHorizon.addGoal(currSegGoals.front()); 
	m_currPlanHorizon.addGoal(currSegGoals.front()); 		
      }
      break;
    default:
      break;
    };
    
    if ((m_verbose) || (m_debug)){  
      cout<<"SIZE OF CURRPLANHORIZON GOALS " <<  m_currPlanHorizon.getSize()<< endl;
    }
  }   
  return meetsPlanHorizonReqs;
}

unsigned int TrafficManager::getNextUniqueMergedDirID() {
  return ++m_uniqueMergedDirID; 
}


