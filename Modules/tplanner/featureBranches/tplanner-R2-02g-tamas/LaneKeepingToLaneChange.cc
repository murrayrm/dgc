#include "LaneKeepingToLaneChange.hh"

LaneKeepingToLaneChange::LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

LaneKeepingToLaneChange::LaneKeepingToLaneChange()
{

}

LaneKeepingToLaneChange::~LaneKeepingToLaneChange()
{

}

double LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    point2 rearPos = AliceStateHelper::getPositionRearBumper(vehState);
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    {
        if (planHorizon.getSegGoal(0).exitLaneID != map->getLaneID(rearPos)) {
          m_prob = 1;
          LaneChange *laneChange = dynamic_cast<LaneChange*>(this->getControlStateTo());
	  if (laneChange == 0)
	    cerr << "LK->LC: Error retrieving LC object" << endl;
	  laneChange->setLaneChangeID(planHorizon.getSegGoal(0).exitLaneID);
        }
    }
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      point2 stopLine;
      int stopLineErr = map->getStopline(stopLine,rearPos);
      if (stopLineErr != 0) //no stop line found, this is ok
        break;  
      double stopLineDist = rearPos.dist(stopLine);
      //max. lane change distance
      if (stopLineDist < 20)
        m_prob = 0;
    }
        break;

    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:
    	
	    m_prob = 0;
	    break;

    default:
    
        m_prob = 0;
        cerr << "LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
