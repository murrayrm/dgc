#include "Stopped.hh"


Stopped::Stopped(int stateId, ControlStateFactory::ControlStateType type, StoppedType stage)
: ControlState(stateId, type)
{
    m_uturn = false;
    m_changelane_left = false;
    m_changelane_right = false;
    m_stage = stage;
    m_failed = false;
}

Stopped::~Stopped()
{
}

/* Same code than LaneKeeping except velIn = 0, velOut = 0, acc = 0 */
int Stopped::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    point2arr leftBound, rightBound, leftBound1, rightBound1;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    double velIn, velOut, acc;
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 FC_finalPos;
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);

    switch (traffState->getType()) {
    
    case TrafficStateFactory::ZONE_REGION:
    {
        // We want to plan for current segment only
        // TODO: want to do this properly in the future
        if ((m_verbose) || (m_debug)) {
            cout << "Stopped:: Zone region" << endl;
        }
        
        PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        if ((m_verbose) || (m_debug)) {
            cout << "exit waypt label = " << exitWayptLabel << endl;
        }
        
        point2 exitWaypt;
        int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
        if (wayptErr != 0) {
            cerr << "Stopped.cc: waypt read from map error" << endl;
            return (wayptErr);
        }

        if ((m_verbose) || (m_debug)) {
            cout << "exit waypt pos = " << exitWaypt << endl;
        }
        
        // Get our current position
        if ((m_verbose) || (m_debug)) {
            cout << "current front bumper pos = " << currFrontPos << endl;
            cout << "current rear bumper pos = " << currRearPos << endl;
        }
        
        // Create a corridor of LaneWidth from current position to entry point
        double corrHalfWidth = 3;
        double theta = atan2(exitWaypt.y - currFrontPos.y, exitWaypt.x - currFrontPos.x);
        point2 temppt;
        
        // Left Boundary
        // pt 1 on left boundary
        temppt.x = currRearPos.x + corrHalfWidth * cos(theta + M_PI / 2);
        temppt.y = currRearPos.y + corrHalfWidth * sin(theta + M_PI / 2);
        leftBound.push_back(temppt);
        // pt 2 on left boundary
        temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
        temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
        leftBound.push_back(temppt);
        // Right Boundary
        // pt 1 on right boundary
        temppt.x = currRearPos.x + corrHalfWidth * cos(theta - M_PI / 2);
        temppt.y = currRearPos.y + corrHalfWidth * sin(theta - M_PI / 2);
        rightBound.push_back(temppt);
        // pt 2 on right boundary
        temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
        temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
        rightBound.push_back(temppt);

        // TODO: Take some subset of these boundary points as the corridor

        // Specify the velocity profile
        // TODO: specify some max velocity for here
        velIn = 0;
        velOut = 0;
        acc = 0;

        // specify the ocpParams final conditions - same as initial conditions,
        // but note that the fc pos gets shifted back to account for planning 
        // for the rear axle. So, should use currFrontPos here!
        FC_finalPos = currFrontPos; 
        FC_velMin = velIn;

        double heading;
        localMap->getHeading(heading,FC_finalPos);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];   
    }
        break;

    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "Stopped::determineCorridor inside ROAD_REGION" << endl;
        }

        /* We want to report a failure if we are stuck behind an obstacle */
        // TODO: SVEN - how should we handle this case? How can I distinguish between obstacle and intersection behavior here?
        if (m_stage == OBSTACLE) m_failed = true;
        
        // We want to plan for current segment only
        // TODO: want to use function that does not specify specific lane
        LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
        if (getBoundsErr != 0) {
            cerr << "Stopped.cc: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currRearPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currRearPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currRearPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currRearPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }
        
        // Specify the velocity profile
        velIn = 0;
        velOut = 0;
        acc = 0;

        // specify the ocpParams final conditions - same as initial conditions,
        // but note that the fc pos gets shifted back to account for planning 
        // for the rear axle. So, should use currFrontPos here!
        FC_finalPos = currFrontPos; 
        
        FC_velMin = 0;

        double heading;
        localMap->getHeading(heading,FC_finalPos);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
        // We want to plan over two segments
        SegGoals nextSegGoal = planHoriz.getSegGoal(1);
        point2arr leftBound1, rightBound1, leftBound2, rightBound2;
        // Set of boundary points due to intersection lane
        PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
        PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

        double range = 50;
        int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
        if (interBoundErr != 0) {
            cerr << "Stopped.cc: Intersection boundary read from map error" << endl;
            return (interBoundErr);
        }
        
        // Add the boundaries together
        leftBound = leftBound1;
        rightBound = rightBound1;

        // TODO: take some subset of these boundary points as the corridor

        // Specify the velocity profile
        velIn = 0;
        velOut = 0;
        acc = 0;

        // specify the ocpParams final conditions - same as initial conditions,
        // but note that the fc pos gets shifted back to account for planning 
        // for the rear axle. So, should use currFrontPos here!
        FC_finalPos = currFrontPos; 

        double heading;
        localMap->getHeading(heading,FC_finalPos);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;
        
    default:
    
        cerr << "Stopped.cc: Undefined Traffic state" << endl;
        
    }

    if ((m_verbose) || (m_debug)) {
        cout << "velocity profile: in = " << velIn << " and out = " << velOut << endl;
        cout << "specified acceleration = " << acc << endl;
    }
    
    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // Set the velocity profile - need to think this through some
    vector < double >velProfile;
    velProfile.push_back(velIn);        // entry pt
    velProfile.push_back(velOut);       // exit pt
    corr.setVelProfile(velProfile);
    corr.setDesiredAcc(acc);


    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    return 0;
}



bool Stopped::getUTurnBool()
{
    return m_uturn;
}

bool Stopped::getChangeLaneLeftBool()
{
    return m_changelane_left;
}

bool Stopped::getChangeLaneRightBool()
{
    return m_changelane_right;
}

void Stopped::resetConditions()
{
    m_uturn = false;
    m_changelane_left = false;
    m_changelane_right = false;
}
