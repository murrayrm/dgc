#ifndef CMDLINEARGS_HH_
#define CMDLINEARGS_HH_

struct CmdLineArgs
{
public:
  int sn_key;
  int debugLevel;
  int verboseLevel;
  bool debug;
  bool verbose;
  bool waitForStateFill;
  bool no_console;
  bool log;
  bool send_costmap;
  bool use_local;
  bool rndf_given;
private:

};

#endif /*CMDLINEARGS_HH_*/
