#include <getopt.h>

#include <iostream>
#include "TrafficPlanner.hh"
#include "dgcutils/DGCutils.hh"
using namespace std;

int NOWAIT               = 0;       // by the default, wait for state to fill
char* RNDFFileName = "../../nok/missionPlanner/DARPA_RNDF.txt"; // name of RNDF file

/* The name of this program. */
const char * program_name;

int main(int argc, char **argv) 
{
  gengetopt_args_info cmdline;
  int sn_key;  

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  //program_name = argv[0];
  //printf("\n");

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  
//  if( pSkynetkey == NULL )
//  {
//    cerr << "SKYNET_KEY environment variable isn't set" << endl;
//  }
//  else
//    sn_key = atoi(pSkynetkey);
//  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  CTrafficPlanner* pTrafficPlanner = new CTrafficPlanner(sn_key, NOWAIT == 0, RNDFFileName);

  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getMapDeltasThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getLocalMapThread);
  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getDPlannerStatusThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getSegGoalsThread);
  
  // I have had issues where some of the threads are not up yet by the time the planning loop starts,
  // and I do not know why that is, but I have added a sleep here to make sure that all the threads 
  // are running. Want something more robust here?!
  #warning "make threads more independent during startup"
  sleep(1);
  
  pTrafficPlanner->TPlanningLoop();
  //pTrafficPlanner->getLocalMapThread();
  return 0;
}
