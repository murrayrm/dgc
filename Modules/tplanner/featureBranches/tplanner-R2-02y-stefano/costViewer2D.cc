#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <sys/types.h>
#include <string.h>
#include "interfaces/sn_types.h"
#include "skynet/sn_msg.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"

using namespace std;

const int  NUM_ROWS=250;
const int  NUM_COLS=250;
const double ROW_RES=0.40;
const double COL_RES=0.40;

//OpenGL variables
float lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;
bool CIRCLE=true;
bool SQUARE=false;

const int WINDOW_WIDTH=1000;
const int WINDOW_HEIGHT=800;

const double BORDER_WIDTH=60;
const double BORDER_HEIGHT=60;

const double XMIN=-(double)NUM_COLS/2-BORDER_WIDTH;
const double XMAX= (double)NUM_COLS/2+BORDER_WIDTH;
const double YMIN=-(double)NUM_ROWS/2-BORDER_HEIGHT;
const double YMAX= (double)NUM_ROWS/2+BORDER_HEIGHT;

//plotting features
const double OPACITY = 1;
//const double LINE_WIDTH = 1;
const bool OUTLINE = false;
const bool TOP_OUTLINE = false;

// Rainbow color scale
uint8_t rainbow[0x10000][3];

#define MAX_DELTA_SIZE 1000000

using namespace std;

//Spread global variables
int tmapDeltaSocket;
int statesocket;

CMapPlus* tmap = new CMapPlus();
VehicleState currState;
int tmapID_cost;
unsigned long long timestamp;

skynet m_skynet = skynet(MODtrafficplanner,atoi(getenv("SKYNET_KEY")), NULL);

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

void init(void)
{	

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      rainbow[i][0] = 0x00;
      rainbow[i][1] = (int) (d * 0xFF);
      rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      rainbow[i][0] = 0x00;
      rainbow[i][1] = 0xFF;
      rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      rainbow[i][0] = (int) (d * 0xFF);
      rainbow[i][1] = 0xFF;
      rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      rainbow[i][0] = 0xFF;
      rainbow[i][1] = (int) ((1 - d) * 0xFF);
      rainbow[i][2] = 0x00;
    }
    else
    {
      rainbow[i][0] = 0xFF;
      rainbow[i][1] = 0x00;
      rainbow[i][2] = 0x00;
    }
  }

}

void drawString (char *s)
{
  unsigned int i;
  for (i = 0; i < strlen (s); i++)
    glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
};


void draw_floor(void)
{
  static char label[100];
  
  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT); //clear the color buffer 

  //====================
  //draw bounding box
  //====================
  //vertical lines
  glLineWidth(0.5);
  glColor3f (0.0F, 1.0F, 0.0F);

  glBegin (GL_LINES);
  glVertex2f (-NUM_COLS/2, YMIN+BORDER_HEIGHT);
  glVertex2f (-NUM_COLS/2, YMAX-BORDER_HEIGHT);
  glEnd ();

  glBegin (GL_LINES);
  glVertex2f (NUM_COLS/2, YMIN+BORDER_HEIGHT);
  glVertex2f (NUM_COLS/2, YMAX-BORDER_HEIGHT);
  glEnd ();
  
  
  //horizontal lines
  glLineWidth(0.5);
  glColor3f (0.7F, 0.7F, 0.7F);
  glBegin (GL_LINES);
  glVertex2f (XMIN+BORDER_WIDTH, -NUM_ROWS/2);
  glVertex2f (XMAX-BORDER_WIDTH, -NUM_ROWS/2);
  glEnd ();

  glBegin (GL_LINES);
  glVertex2f (XMIN+BORDER_WIDTH, NUM_ROWS/2);
  glVertex2f (XMAX-BORDER_WIDTH, NUM_ROWS/2);
  glEnd ();


}

void draw_block(int i, int j, double cost)
{
  float x_bottomleft, x_bottomright, x_topleft, x_topright;
  float y_bottomleft, y_bottomright, y_topleft, y_topright;

  // Select color based on cost
  int k = (int) (0x10000 * (float)(cost)/ 101); // MAGIC
  if (k < 0x0000) k = 0x0000;
  if (k > 0xFFFF) k = 0xFFFF;
  glColor3ub(rainbow[k][0], rainbow[k][1], rainbow[k][2]);
		  
  x_bottomleft = i-NUM_COLS/2;
  y_bottomleft = j-NUM_ROWS/2;
  x_bottomright = i+1-NUM_COLS/2;
  y_bottomright = j-NUM_ROWS/2;
  x_topleft = i-NUM_COLS/2;
  y_topleft = j+1-NUM_ROWS/2;
  x_topright = i+1-NUM_COLS/2;
  y_topright = j+1-NUM_ROWS/2;
  
  glBegin(GL_QUADS);
  glVertex2f(x_bottomleft,y_bottomleft);
  glVertex2f(x_bottomleft,y_topleft);
  glVertex2f(x_bottomright,y_topright);
  glVertex2f(x_bottomright,y_bottomright);	
  glEnd();
  
}


void draw_everything(void)
{
  double cost_value=0;
  static char time_label[100];
  static char title_label[100];
  static char state_label[100];

  draw_floor();

  pthread_mutex_lock(&mutex1);
 
  for(int r=0; r<NUM_ROWS; r++)
    {
      for(int c=0; c<NUM_COLS; c++)
	{
	  
	  cost_value = (tmap->getDataWin<double>(tmapID_cost,r,c));

	  // See if there is any data in this cell before we draw it
	  if (cost_value == 0)
	    continue;     	 	  

	  draw_block(c, r, cost_value); //by abuse of notation, it's (column, row)
	  
	}
    }

  if(SQUARE)
    {
      //===================
      //draw Alice as a square
      //===================
      glColor3f(1.0f,0.0f,0.0f);
      glBegin(GL_LINE_STRIP);
      glVertex2f(-5,-5);
      glVertex2f(-5,5);
      glVertex2f(5,5);
      glVertex2f(5,-5);
      glVertex2f(-5,-5);
      glEnd();
    }

  if(CIRCLE)
    {
      //===================
      //draw Alice as a circle
      //===================
      double angle;
      glColor3f(0.0f,1.0f,1.0f);
      glBegin(GL_LINE_LOOP);
      for(int j=0; j<360; j++)
	{
	  angle = 2*M_PI/360*j;
	  glVertex2f(5*cos(angle), 5*sin(angle));
	}
      glEnd();
    }

  DGCgettime(timestamp);
  
  sprintf(title_label, "TPLANNER:COST MAP VIEWER"); 
  glColor3f(0.0F,1.0F,0.0F);
  glRasterPos2f ( -50, YMAX-BORDER_HEIGHT/2);
  drawString (title_label); 

  sprintf(time_label, "timestamp : %20.1f", (double)timestamp);
  glColor3f(0.0F,1.0F,0.0F);
  glRasterPos2f ( XMAX-165, YMAX-BORDER_HEIGHT/2);
  drawString (time_label); 
  
  sprintf(state_label, "state(N,E) :   (%3.4f, %3.4f)", currState.localX, currState.localY);
  glColor3f(0.0F,1.0F,0.0F);
  glRasterPos2f ( XMAX-165, YMAX-BORDER_HEIGHT/2-15);
  drawString (state_label);       

  pthread_mutex_unlock(&mutex1);
  
}

void display(void)
{
  glClear (GL_COLOR_BUFFER_BIT);
  draw_everything();
  glutSwapBuffers();
}

void resize(int w, int h)
{
  glLoadIdentity();
  glViewport (0, 0, w, h); //set the viewport to the current window specifications
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(XMIN,XMAX,YMIN,YMAX);
}

 

void ReadMessage(void)
{           	    
  char* pMapDelta = new char[MAX_DELTA_SIZE];
      
  int statereceived;
  int deltasize;

  pthread_mutex_lock(&mutex1);

  while(m_skynet.is_msg(statesocket))
    {
      statereceived = m_skynet.get_msg(statesocket, &currState, sizeof(currState), 0);
    }
  if(statereceived>0)
    {
      tmap->updateVehicleLoc(currState.localX, currState.localY);
      //tmap->updateVehicleLoc(0,0);

      deltasize = m_skynet.get_msg(tmapDeltaSocket, pMapDelta, MAX_DELTA_SIZE, 0);	 
      if(deltasize>0)
	{
	  tmap->applyDelta<double>(tmapID_cost, pMapDelta, deltasize);	  	  
	}
      else
	{
	  printf("delta not received!\n");
	}      
    }  
  pthread_mutex_unlock(&mutex1);
  delete pMapDelta;
} 


void idle (void) 
{

  //read latest message
  ReadMessage();

  //replot data
  glutPostRedisplay();


}




void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	}
      else
	{
	  lbuttondown = false;
	}
    }

  if(button == GLUT_RIGHT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  rbuttondown = true;
	}
      else
	{
	  rbuttondown = false;
	}
    }
  

}



void motion(int x, int y)
{
  if (lbuttondown)
    {
      float diffx=(x-lastx)/10; //check the difference between the current x and the last x position  
      float diffy=(y-lasty)/10;

      lastx = x;
      lasty = y;

      glTranslatef(diffx, -diffy, 0);

      glutPostRedisplay();     
    }

  if (rbuttondown)
    {     

      GLint _viewport[4];
      GLdouble _modelMatrix[16];
      GLdouble _projMatrix[16];
      GLdouble objx, objy, objz;

      glGetIntegerv(GL_VIEWPORT,_viewport);
      glGetDoublev(GL_PROJECTION_MATRIX, _projMatrix);
      glGetDoublev(GL_MODELVIEW_MATRIX, _modelMatrix);

      gluUnProject(WINDOW_WIDTH/2,WINDOW_HEIGHT/2,0,_modelMatrix,_projMatrix,_viewport,&objx, &objy, &objz);

      float dy=(y-lasty); //check the difference between the current y and the last y position  

      double s = exp((double)dy*0.01);

      glTranslatef(objx,objy,0);
      glScalef(s,s,s);
      glTranslatef(-objx,-objy,0);

      lasty = y;
      lastx = x;

      glutPostRedisplay();  
    }
  
}

void motionPassive(int x, int y)
{
  lastx = x;
  lasty = y;
}


int main(int argc, char **argv)
{

  //========================
  //initialize openGL stuff
  //======================== 
  init();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowPosition(40, 40);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow("COSTVIEWER");  
  glClearColor (0.0,0.0,0.0,0.0); //clear the screen to black
  
  /* set callback functions */
  glutDisplayFunc(display); 
  glutReshapeFunc(resize);
  
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutPassiveMotionFunc(motionPassive);

  glutIdleFunc(idle);

  //========================
  //initialize skynet stuff
  //========================
  tmapDeltaSocket = m_skynet.listen(SNtplannerStaticCostMap, MODtrafficplanner);  
  statesocket = m_skynet.listen(SNstate, MODtrafficplanner);  
  if(tmapDeltaSocket < 0)
    cerr << "CTrafficPlanner::getMapDeltasThread(): skynet listen returned error" << endl; 

  //========================
  // initialize Map
  //========================
  tmap->initMap(0.0,0.0,NUM_ROWS,NUM_COLS,ROW_RES,COL_RES,0);
  tmapID_cost = tmap->addLayer<double>(100, -1.0, true);

  glutMainLoop();
  
  return 0;
}
