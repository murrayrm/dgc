#include "SlowDownToStop.hh"
#include <math.h>
#include <list>


SlowDownToStop::SlowDownToStop(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_distToStopTrans(1.15)
, m_desiredDecel(-0.5)
  , m_distToObs(10)
{
//TODO FIX this desired Decel
}

SlowDownToStop::~SlowDownToStop()
{

}

double SlowDownToStop::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon horiz ,Map* localMap, VehicleState vehState)
{

	cout << "in SlowDownToStop::meetTransitionConditions" <<endl;
	double vel = 0; 
	double dstop = 0;
	int stopLineErr = -1;
	point2 stopLinePos;
	PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
	point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
	point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
	//TODO fix composite state 
	//if (TrafficStateFactory::CLEARREGION == trafficState.getType()) 
	if (TrafficStateFactory::APPROACH_INTER_SAFETY == trafficState->getType()) {
    cout << "in SlowDownToStop::ApproachInterSafety" << endl;
    double vel = 0; 
    double dstop = 0;
    int stopLineErr = -1;
    point2 stopLinePos;

    PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());

    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
	  vel = AliceStateHelper::getVelocityMag(vehState);

    // use getStopline() that gives next stopline in current lane
    stopLineErr = localMap->getStopline(stopLinePos,currRearPos);
    //stopLineErr = localMap->getStopline(stopLinePos,label);
	  dstop = stopLinePos.dist(currFrontPos);
	  cout << "distance to stop = " << dstop << endl;
	  if (dstop <= m_distToStopTrans ) {
	    m_probability = 1;
	  } else { 
      m_probability = 0; 
    }


  } else if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    // TODO: this will need to be updated - but for now implemented as road region with obstacles
     cout << "in slowdowntostop: ROAD_REGION" << endl;
     
     point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
     
     double distToObs = localMap->getObstacleDist(currFrontPos,0);
     if (distToObs <= m_distToObs + m_distToStopTrans) {
       m_probability = 1;
     } else { 
       m_probability = 0; 
     }   
	}

	setUncertainty(m_probability);
	return m_probability;
}

