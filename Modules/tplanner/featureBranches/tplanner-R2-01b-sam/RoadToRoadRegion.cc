#include "RoadToRoadRegion.hh"
#include <math.h>


RoadToRoadRegion::RoadToRoadRegion(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
{
}

RoadToRoadRegion::~RoadToRoadRegion()
{

}

double RoadToRoadRegion::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  cout << "in RoadToRoadRegion::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
  return 0.0;
} 


double RoadToRoadRegion::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel ptLabel)
{
  cout << "in RoadToRoadRegion::meetTransitionConditions() " << endl;

  // want to stay in road region if we are not approaching an intersection
  //if (!(localMap->isStop(ptLabel)) && !(localMap->isExit(ptLabel))){
  //  m_trafTransProb = 1;	
  //} else {
    m_trafTransProb = 0;
    //}
  setUncertainty(m_trafTransProb);
  return m_trafTransProb;
} 
