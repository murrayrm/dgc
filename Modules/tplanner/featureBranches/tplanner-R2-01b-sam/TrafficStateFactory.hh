#ifndef TRAFFICSTATEFACTORY_HH_
#define TRAFFICSTATEFACTORY_HH_


#include "state/StateGraph.hh"


class TrafficStateFactory {


public: 

  TrafficStateFactory();
 ~TrafficStateFactory();
  static typedef enum {ROAD_REGION, ZONE_REGION, APPROACH_INTER_SAFETY, INTERSECTION_STOP } TrafficStateType; 
  static StateGraph createTrafficStates();
  static int getNextId();
  //void print(TrafficStateFactory::TrafficStateType type);

  void print(int type);
private:
  int m_stateId;
  
};

#endif /*TRAFFICSTATEFACTORY_HH_*/
