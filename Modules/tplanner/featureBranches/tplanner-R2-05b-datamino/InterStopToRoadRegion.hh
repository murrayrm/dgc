#ifndef INTERSTOPTOROADREGION_HH_
#define INTERSTOPTOROADREGION_HH_


#include "TrafficStateTransition.hh"
#include "TrafficUtils.hh"

class InterStopToRoadRegion:public TrafficStateTransition {

  public:

    InterStopToRoadRegion(TrafficState * state1, TrafficState * state2);
    ~InterStopToRoadRegion();

    double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState);
    double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel pointLabel);

  private:

    double m_distInterExit;     //in meters

};

#endif                          /*INTERSTOPTOROADREGION_HH_ */
