#include "ControlStateFactory.hh"
#include "ControlStateTransition.hh"

#include "ControlState.hh"
#include "LaneKeeping.hh"
#include "Stop.hh"
#include "Stopped.hh"
#include "UTurn.hh"
#include "LaneChange.hh"
#include "Creep.hh"

#include "LaneKeepingToStop.hh"
#include "LaneKeepingToLaneChange.hh"
#include "StopToStopped.hh"
#include "StopObsToStoppedObs.hh"
#include "StoppedToLaneKeeping.hh"
#include "StoppedToUTurn.hh"
#include "UTurnToLaneKeeping.hh"
#include "UTurnToUTurn.hh"
#include "LaneChangeToLaneKeeping.hh"
#include "Passing1_LaneKeepingToLaneChange.hh"
#include "Passing3_LaneKeepingToLaneChange.hh"
#include "StoppedToPassing_LC.hh"
#include "LaneKeepingToStopObs.hh"
#include "Passing_LaneKeepingToLaneKeeping.hh"
#include "CreepToStop.hh"
#include "StoppedToCreep.hh"
#include "Pause.hh"
#include "StopObsToLaneKeeping.hh"

#include "Log.hh"

ControlState* ControlStateFactory::m_initState;
ControlState* ControlStateFactory::m_pauseState;

ControlStateFactory::ControlStateFactory()
{

}

ControlStateFactory::~ControlStateFactory()
{
  delete m_initState; 
  delete m_pauseState; 
}

StateGraph ControlStateFactory::createControlStates()
{
    vector < StateTransition * >trans;

    int stateId = 0;            // TODO fix this nonsense

    // Control States

    // Pause state 
    Pause *pause = new Pause(++stateId, PAUSE);
    m_pauseState = pause; 

    // Road region
    LaneKeeping *laneKeeping = new LaneKeeping(++stateId, LANE_KEEPING, LaneKeeping::NOMINAL);
    m_initState = laneKeeping;
    LaneChange *laneChange = new LaneChange(++stateId, LANE_CHANGE);
    LaneChange *passing1_LC = new LaneChange(++stateId, LANE_CHANGE);
    LaneKeeping *passing2_LK = new LaneKeeping(++stateId, LANE_KEEPING, LaneKeeping::PASSING);
    LaneChange *passing3_LC = new LaneChange(++stateId, LANE_CHANGE);
    Stop *stop = new Stop(++stateId, STOP);
    Stopped *stopped = new Stopped(++stateId, STOPPED, Stopped::INTERSECTION);
    Creep *creep = new Creep(++stateId, CREEP);

    Stop *stopObs = new Stop(++stateId, STOP);
    Stopped *stoppedObs = new Stopped(++stateId, STOPPED, Stopped::OBSTACLE);
    UTurn *uTurn1 = new UTurn(++stateId, UTURN, 1);
    UTurn *uTurn2 = new UTurn(++stateId, UTURN, 2);
    UTurn *uTurn3 = new UTurn(++stateId, UTURN, 3);
    UTurn *uTurn4 = new UTurn(++stateId, UTURN, 4);
    UTurn *uTurn5 = new UTurn(++stateId, UTURN, 5);

    // Control State Transitions
    // Nominal driving
    // LaneKeepingToStop *laneKeepStopTrans = new LaneKeepingToStop(laneKeeping, stop);
    //    LaneKeepingToLaneChange *laneKeepLaneChangeTrans = new LaneKeepingToLaneChange(laneKeeping, laneChange);
    LaneChangeToLaneKeeping *laneChangeLaneKeepTrans = new LaneChangeToLaneKeeping(laneChange, laneKeeping);
    // Nominal UTurn
    StoppedToUTurn *laneKeepUTurnTrans = new StoppedToUTurn(laneKeeping, uTurn1);

    // Obstacle handling
    UTurnToUTurn *uturnUTurnTrans1 = new UTurnToUTurn(uTurn1, uTurn2, 1);
    UTurnToUTurn *uturnUTurnTrans2 = new UTurnToUTurn(uTurn2, uTurn3, 2);
    UTurnToUTurn *uturnUTurnTrans3 = new UTurnToUTurn(uTurn3, uTurn4, 3);
    UTurnToUTurn *uturnUTurnTrans4 = new UTurnToUTurn(uTurn4, uTurn5, 4);
    UTurnToLaneKeeping *uTurnLaneKeepTrans = new UTurnToLaneKeeping(uTurn5, laneKeeping);

    Passing1_LaneKeepingToLaneChange *laneKeepPassing1Trans = new Passing1_LaneKeepingToLaneChange(laneKeeping, passing1_LC);
    LaneChangeToLaneKeeping *passing1Passing2Trans = new LaneChangeToLaneKeeping(passing1_LC, passing2_LK);
    Passing3_LaneKeepingToLaneChange *passing2Passing3Trans = new Passing3_LaneKeepingToLaneChange(passing2_LK, passing3_LC);
    LaneChangeToLaneKeeping *passing3LaneKeepTrans = new LaneChangeToLaneKeeping(passing3_LC, laneKeeping);
    // Obstacle handling while passing
    Passing_LaneKeepingToLaneKeeping *passingLaneKeepLaneKeepTrans = new Passing_LaneKeepingToLaneKeeping(passing2_LK, laneKeeping);

    LaneKeepingToStopObs *laneKeepStopObsTrans = new LaneKeepingToStopObs(laneKeeping, stopObs);

    // Handling of wrong positive
    StopObsToLaneKeeping *stopToLK = new StopObsToLaneKeeping(stopObs, laneKeeping);

    // intersection handling
    LaneKeepingToStop *laneKeepStopTrans = new LaneKeepingToStop(laneKeeping, stop);
    StoppedToLaneKeeping *stoppedLaneKeepTrans = new StoppedToLaneKeeping(stopped, laneKeeping);
    StopToStopped *stopStoppedTrans = new StopToStopped(stop, stopped);
    StoppedToCreep *stoppedCreepTrans = new StoppedToCreep(stopped, creep);
    CreepToStop *creepStopTrans = new CreepToStop(creep, stop);

    StopObsToStoppedObs *stopStoppedObsTrans = new StopObsToStoppedObs(stopObs, stoppedObs);
    StoppedToUTurn *stoppedObsUTurnTrans = new StoppedToUTurn(stoppedObs, uTurn1);
    StoppedToPassing_LC *stoppedObsPassing1Trans = new StoppedToPassing_LC(stoppedObs, passing1_LC);

    // populate transitions list
    trans.push_back(laneKeepStopTrans);
    //trans.push_back(laneKeepLaneChangeTrans);
    trans.push_back(laneChangeLaneKeepTrans);
    trans.push_back(stopStoppedTrans);
    trans.push_back(stoppedLaneKeepTrans);
    trans.push_back(stoppedObsPassing1Trans);
    trans.push_back(uturnUTurnTrans1);
    trans.push_back(uturnUTurnTrans2);
    trans.push_back(uturnUTurnTrans3);
    trans.push_back(uturnUTurnTrans4);
    trans.push_back(uTurnLaneKeepTrans);
    trans.push_back(laneKeepPassing1Trans);
    trans.push_back(passing1Passing2Trans);
    trans.push_back(passing2Passing3Trans);
    trans.push_back(passing3LaneKeepTrans);
    trans.push_back(passingLaneKeepLaneKeepTrans);
    trans.push_back(stoppedCreepTrans);
    trans.push_back(creepStopTrans);
    trans.push_back(stopStoppedObsTrans);
    trans.push_back(stoppedObsUTurnTrans);
    trans.push_back(laneKeepStopObsTrans);
    trans.push_back(laneKeepUTurnTrans);
    trans.push_back(stopToLK);

    return StateGraph(trans);
}

ControlState* ControlStateFactory::getInitialState()
{
    return m_initState;
}

ControlState* ControlStateFactory::getPauseState()
{
    return m_pauseState;
}

int ControlStateFactory::getNextId()
{
    return 0;
}


void ControlStateFactory::print(int type)
{
    Log::getStream(1) << "Control State Type: ";
    switch (type) {
    case ControlStateFactory::LANE_KEEPING:
        Log::getStream(1) << "LANE KEEPING" << endl;
        break;
    case ControlStateFactory::LANE_CHANGE:
        Log::getStream(1) << "LANE_CHANGE" << endl;
        break;
    case ControlStateFactory::STOP:
        Log::getStream(1) << "STOP" << endl;
        break;
    case ControlStateFactory::STOPPED:
        Log::getStream(1) << "STOPPED" << endl;
        break;
    case ControlStateFactory::PAUSE:
        Log::getStream(1) << "PAUSE" << endl;
        break;
    case ControlStateFactory::UTURN:
        Log::getStream(1) << "UTURN" << endl;
        break;
    case ControlStateFactory::CREEP:
        Log::getStream(1) << "CREEP" << endl;
        break;
    default:
        Log::getStream(1) << "INVALID" << endl;
        break;
    };
}

string ControlStateFactory::printString(int type)
{
    string cstate_type;
    switch (type) {
    case ControlStateFactory::LANE_KEEPING:
        cstate_type = string("LANE KEEPING");
        break;
    case ControlStateFactory::LANE_CHANGE:
        cstate_type = string("LANE CHANGE");
        break;
    case ControlStateFactory::STOP:
        cstate_type = string("STOP");
        break;
    case ControlStateFactory::STOPPED:
        cstate_type = string("STOPPED");
        break;
    case ControlStateFactory::UTURN:
        cstate_type = string("UTURN");
        break;
    case ControlStateFactory::CREEP:
        cstate_type = string("CREEP");
        break;
    case ControlStateFactory::PAUSE:
        cstate_type = string("PAUSE");
        break;
    default:
        cstate_type = string("INVALID");
        break;
    };
    return cstate_type;
}

