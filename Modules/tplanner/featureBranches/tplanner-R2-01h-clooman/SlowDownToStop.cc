#include "SlowDownToStop.hh"
#include <math.h>
#include <list>


SlowDownToStop::SlowDownToStop(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_distToStopTrans(1.3)
, m_desiredDecel(-0.5)
  , m_distToObs(10)
{
//TODO FIX this desired Decel
}

SlowDownToStop::~SlowDownToStop()
{

}

double SlowDownToStop::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon horiz ,Map* localMap, VehicleState vehState)
{

	cout << "in SlowDownToStop::meetTransitionConditions" <<endl;

	point2 stopLinePos;
	PointLabel ptLabel = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
	point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
	point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
	//TODO fix composite state 
	//if (TrafficStateFactory::CLEARREGION == trafficState.getType()) 




	if (TrafficStateFactory::APPROACH_INTER_SAFETY == trafficState->getType()) {
    cout << "in SlowDownToStop::ApproachInterSafety" << endl;

    // use getStopline() that gives next stopline in current lane
    localMap->getNextStopline(stopLinePos, ptLabel);
    double angle;
    localMap->getHeading(angle, ptLabel);
    cout << "heading = " << angle << endl;
    double dotProd = (-stopLinePos.x+currFrontPos.x)*cos(angle) + (-stopLinePos.y+currFrontPos.y)*sin(angle);
    cout << "in SD2Stop: dot product  = " << dotProd << endl;

    if (dotProd > -m_distToStopTrans) {
	    m_probability = 1;
	  } else { 
      m_probability = 0; 
    }





  } else if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    // TODO: this will need to be updated - but for now implemented as road region with obstacles
     cout << "in slowdowntostop: road region" << endl;
     
     double distToObs = localMap->getObstacleDist(currFrontPos,0);
     if (distToObs <= m_distToObs + m_distToStopTrans) {
       m_probability = 1;
     } else { 
       m_probability = 0; 
     }   







	}	else if (TrafficStateFactory::INTERSECTION_STOP == trafficState->getType()) {
    cout << "in SlowDownToStop:: intersection stop" << endl;

    // immediately switch to stop
    m_probability = 1;

  }



	setUncertainty(m_probability);
	return m_probability;
}

