#include "Stopped.hh"


Stopped::Stopped(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
{
}

Stopped::~Stopped()
{
}

int Stopped::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
  //SAM  Corridor corridor = Corridor();

  point2arr leftBound, rightBound;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  point2arr leftBound1, rightBound1;
  point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);

  if(TrafficStateFactory::ZONE_REGION == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to do this properly in the future
    point2 exitWaypt, currPos;
    PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
    if (wayptErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (wayptErr);
    }     
    // get our current position
    currPos = AliceStateHelper::getPositionFrontBumper(vehState);
    
    // create a corridor of LaneWidth from current position to entry point
    double corrHalfWidth = 5;
    double theta = atan2(exitWaypt.y-currPos.y, exitWaypt.x-currPos.x);
    point2 temppt;
    // Left Boundary
    // pt 1 on left boundary
    temppt.x = currPos.x + corrHalfWidth*cos(theta+M_PI/2);
    temppt.y = currPos.y + corrHalfWidth*sin(theta+M_PI/2);
    leftBound.push_back(temppt);
    // pt 2 on left boundary
    temppt.x = exitWaypt.x + corrHalfWidth*cos(theta+M_PI/2);
    temppt.y = exitWaypt.y + corrHalfWidth*sin(theta+M_PI/2);
    leftBound.push_back(temppt);
    // Right Boundary
    // pt 1 on right boundary
    temppt.x = currPos.x + corrHalfWidth*cos(theta-M_PI/2);
    temppt.y = currPos.y + corrHalfWidth*sin(theta-M_PI/2);
    rightBound.push_back(temppt);
    // pt 2 on right boundary
    temppt.x = exitWaypt.x + corrHalfWidth*cos(theta-M_PI/2);
    temppt.y = exitWaypt.y + corrHalfWidth*sin(theta-M_PI/2);
    rightBound.push_back(temppt);

    // Specify the velocity profile
		// TODO Specify some max velocity for here
    velIn = 0;
    velOut = 0;
    acc = 0;

  } else if(TrafficStateFactory::ROAD_REGION == traffState->getType())
  {
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr); 
   }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }

    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;

  } else if (TrafficStateFactory::APPROACH_INTER_SAFETY == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr);
    }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;

  } else if (TrafficStateFactory::INTERSECTION_STOP == traffState->getType()){
    // want to plan over two segments
    SegGoals segment2 = planHoriz.getSegGoal(1);
    point2arr leftBound1, rightBound1, leftBound2, rightBound2;
    // Set of boundary points due to intersection lane
    PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
    PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut);
    if (interBoundErr!=0){
      cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
      return (interBoundErr);
		}
    // Set of boudary pts due to lane after intersection
    LaneLabel laneLabel(segment2.entrySegmentID, segment2.entryLaneID);      
    int rightBoundErr = localMap->getRightBound(rightBound2, laneLabel); 
    int leftBoundErr = localMap->getLeftBound(leftBound2, laneLabel);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return rightBoundErr;
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return leftBoundErr;
   }
    // add the boundaries together
    leftBound = leftBound1;
    rightBound = rightBound1;
    for (unsigned i=0; i<leftBound2.size(); i++){
      if (leftBound.back()!=leftBound2[i]){
	leftBound.push_back(leftBound2[i]);
      }
    }
    for (unsigned i=0; i<rightBound2.size(); i++){
      if (rightBound.back()!=rightBound2[i]){
	rightBound.push_back(rightBound2[i]);
      }
    }

    // TODO: take some subset of these boundary points as the corridor

    // Specify the velocity profile
    velIn = 0;
    velOut = 0;
    acc = 0;    

  }

  cout << "leftBound.size()" << leftBound.size() << endl;
  cout << "rightBound.size()" << rightBound.size() << endl;
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector<double> velProfile;
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  return 0;
}
