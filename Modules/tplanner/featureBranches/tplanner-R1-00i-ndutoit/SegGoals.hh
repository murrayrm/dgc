/*!**
 * Nok Wongpiromsarn
 * December 8, 2006
 */


#ifndef SEGGOALS_HH_
#define SEGGOALS_HH_

#include <iostream>
using namespace std;

enum SegmentType{ ROAD_SEGMENT, PARKING_ZONE, INTERSECTION, PREZONE, UTURN, PAUSE,
    END_OF_MISSION, UNKNOWN};

struct SegGoals
{
  SegGoals()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  void print()
  {
    cout << "GOAL " << goalID << ":\t" << entrySegmentID << "." << entryLaneID << "."
         << entryWaypointID;
    cout << "\t -> \t";
    cout << exitSegmentID << "." << exitLaneID << "." << exitWaypointID;
    cout << "\t\t";
    switch(segment_type)
    {
    case ROAD_SEGMENT:
      cout << "ROAD_SEGMENT";
      break;
    case PARKING_ZONE:
      cout << "PARKING_ZONE";
      break;
    case INTERSECTION:
      cout << "INTERSECTION";
      break;
    case PREZONE:
      cout << "PREZONE";
      break;
    case UTURN:
      cout << "UTURN";
      break;
    case PAUSE:
      cout << "PAUSE";
      break;
    case END_OF_MISSION:
      cout << "END_OF_MISSION";
      break;
    default:
      cout << "UNKNOWN";
    }
    cout << "\tMin Speed: " << minSpeedLimit << " Max Speed: " << maxSpeedLimit;
    if (illegalPassingAllowed)
      cout << "\tIllegal passing allowed";
  }
  int goalID;
  int globalMapRevisionNumber;
  int entrySegmentID;
  int entryLaneID;
  int entryWaypointID;
  int exitSegmentID;
  int exitLaneID;
  int exitWaypointID;
  double minSpeedLimit;
  double maxSpeedLimit;
  bool illegalPassingAllowed;
  bool stopAtExit;
  bool isExitCheckpoint;
  int perf_level;
  SegmentType segment_type;

};

#endif //SEGGOALS_HH_
