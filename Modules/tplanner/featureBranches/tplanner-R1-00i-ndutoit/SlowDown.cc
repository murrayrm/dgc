#include "SlowDown.hh"


SlowDown::SlowDown(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId, type)
  , m_desDecc(-0.5)
{
}

SlowDown::~SlowDown()
{
}

int SlowDown::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{
  point2arr leftBound, rightBound;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  point2arr leftBound1, rightBound1;
  point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);

  if(TrafficStateFactory::ROAD_REGION == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr); 
   }
  
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // here I want to specify the acc, and calculate the distance function
    // TODO: think through the vel profile specification some more.
    double cStateLength = (pow(0,2) - pow(currSegment.maxSpeedLimit,2))/(2*m_desDecc);
    double distIntoControlState = calcDistFromInitPos(vehState);
    velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState;
    velOut = 0;
    acc = m_desDecc;

  } else if (TrafficStateFactory::APPROACH_INTER_SAFETY == traffState->getType()){
    // want to plan for current segment only
    // TODO: want to use function that does not specify specific lane
    LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
    int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
    int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
    if (rightBoundErr!=0){
      cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
      return (rightBoundErr);
    }     
    if (leftBoundErr!=0){
      cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
      return (leftBoundErr);
    }
    
    // take some subset of these boundary points as the corridor
    // find the closest pt on the boundary
    int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(leftBound1, currPos);
    for (int ii=index; ii<(int)leftBound1.size(); ii++)
      {
	leftBound.push_back(leftBound1[ii]);
      }
    index = TrafficUtils::insertProjPtInBoundary(rightBound1, currPos);
    index = TrafficUtils::getClosestPtIndex(rightBound1, currPos);
    for (int ii=index; ii<(int)rightBound1.size(); ii++)
      {
	rightBound.push_back(rightBound1[ii]);
      }
    
    // Specify the velocity profile
    // Need to specify the entry and exit vel
    // need one more parameter: can set distance and calc acc, or the other way around
    // here I want to specify the acc, and calculate the distance function
    // TODO: think through the vel profile specification some more.
    double cStateLength = (pow(0,2) - pow(currSegment.maxSpeedLimit,2))/(2*m_desDecc);
    double distIntoControlState = calcDistFromInitPos(vehState);
    velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState;
    velOut = 0;
    acc = m_desDecc;
  }
  
  cout << "leftBound.size()" << leftBound.size() << endl;
  cout << "rightBound.size()" << rightBound.size() << endl;
	cout << "velocity profile: in = " << velIn << " and out = " << velOut << endl;
	cout << "specified acceleration = " << acc <<  endl;
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector<double> velProfile;
  velProfile.push_back(velIn); // entry pt
  velProfile.push_back(velOut); // exit pt
  corr.setVelProfile(velProfile);
  corr.setDesiredAcc(acc);

  return 0;
}
