/*! TrafficPlanner.hh
 * Noel duToit
 * Jan 25 2007
 */

#ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

#include "interfaces/StateClient.hh"
//#include "cmap/CMapPlus.hh"
//#include "MapdeltaTalker.h"
//#include "MapConstants.h"
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include "dgcutils/RDDF.hh"
#include "alice/AliceConstants.h"
#include "interfaces/RDDFTalker.hh"

#include "rndf/RNDF.hh"
#include "rndf/GloNavMapTalker.hh"
#include "interfaces/SegGoalsTalker.hh"
#include "map/Map.hh"

//SAM start changes
// #include "map/LocalMapTalker.hh"
#include "skynet/sn_msg.hh"
#include "interfaces/RoadLineMsg.h"
//SAM end changes

#include <vector>
#include <math.h>
#include "TrafficUtils.hh"



/****************************************************************************/
/* 						Temporary defs										*/
/****************************************************************************/
//enum Divider { DOUBLE_YELLOW=0, SOLID_WHITE=1, BROKEN_WHITE=2, ROAD_EDGE=3 };
enum PlanningHorizon {CURRENT_SEGMENT, NEXT_SEGMENT, NEXT_NEXT_SEGMENT};



/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/*! TrafficPlanner class
 * This is the main class for the traffic planner where the information from 
 * mapping is assimilated and combined with the goals from mplanner and the CA
 * traffic rules. This function inherits from StateClient, SegGoalsTalker, 
 * GloNavMapTalker, MapdeltaTalker, RDDFTalker, and LocalMapTalker
 * \brief Main class for traffic planner function  
 */ 
class CTrafficPlanner : public CStateClient, public CSegGoalsTalker, public CGloNavMapTalker, public CRDDFTalker  //SAM//, public CLocalMapTalker
{ 
  /*!\param m_rndf is a pointer to the RNDF object received from gloNavMap. 
   * There is also a revision number associated with this map*/
  RNDF* m_rndf;
  int m_rndfRevNum;                // RNDF revision#
  pthread_mutex_t m_GloNavMapMutex;

  // basic constructor of local map
  vector<Mapper::Segment> localMapSegments;
  /*!\param m_localMap is the local map object that is populated in the GetLocalMapThread*/
  Mapper::Map m_localMap; //this is the map that I receive from Mapper
  /*!\param myLocalMap is the local map object that is populated in the GetLocalMapThread*/
  Mapper::Map myLocalMap; //this is my local copy of that map
  //Mapper::Map m_localMap;
  pthread_mutex_t m_LocalMapMutex;
  pthread_cond_t m_LocalMapRecvCond;
  pthread_mutex_t m_LocalMapRecvMutex;
  bool m_bReceivedAtLeastOneLocalMap;
  //CLocalMapTalker mapTalker;
  int localMapSocket;                // The skynet socket for receiving local map from mapping
  
  //vector<Obstacle*> m_obstacle;
  //pthread_mutex_t m_ObstacleMutex;

  /*!\param m_costMap is the cost grid map that is created in tplanner and is sent to dplanner*/
  //CMapPlus m_costMap;
  /*!\param m_elevationMap is the elevation grid map recvd from Mapper*/
  //CMapPlus m_elevationMap;
  //pthread_mutex_t m_CostMapMutex;

  int m_fullMapRequestSocket;
  int m_GloNavMapRequestSocket;
  int m_GloNavMapSocket;

  /*!\param m_dplannerStatus is the dplanner status variable - the feedback from dplanner to tplanner*/
  DPlannerStatus* m_dplannerStatus;
  pthread_mutex_t m_DPlannerStatusMutex;

  /*!\param m_segGoals is the Segment goals variable that is populated by the SegGoalsTalker*/
  list<SegGoals> m_segGoals;
  pthread_mutex_t m_SegGoalsMutex;
  int segGoalsSocket;                // The skynet socket for receiving segment goals from mplanner

  /*! Whether at least one delta was received */
  bool m_bReceivedAtLeastOneDelta;
  pthread_mutex_t m_deltaReceivedMutex;
  pthread_cond_t m_deltaReceivedCond;

public:
  /*! Contstructor */
  CTrafficPlanner(int skynetKey, bool bWaitForStateFill, char*);
  /*! Standard destructor */
  ~CTrafficPlanner();

  /*! this is the function that continually reads map deltas and updates the map */
  //  void getMapDeltasThread();
  /*! this is the function that continually reads the latest object map*/
  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/
  void getDPlannerStatusThread();

  /*! this is the function that continually receives segment goals from mplanner */
  void getSegGoalsThread();
  
  /*! the main traffic planning loop*/
  void TPlanningLoop(void);

 

private:
  /*! This function is used to request the full map from mapping */
  void requestFullElevationMap();

  void requestGloNavMap();

  };

#endif  // TRAFFICPLANNER_HH

