#include "TrafficManager.hh"

TrafficManager::TrafficManager(int skynetKey)
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
    , m_currentID(0)
    , m_latestID(0)
{
  m_controlGraph = ControlStateFactory::createControlStates();

  /*!\param GcInterface variable */
  m_missTraffInterface = new MissionTrafficInterface(skynetKey, this);
  assert(m_missTraffInterface != NULL);
  m_missTraffInterfaceNF = m_missTraffInterface->getNorthface();

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = TrafficManagerControlStatus::READY_FOR_NEXT;

}

TrafficManager::TrafficManager(int skynetKey, bool debug, bool verbose, bool log)
  : GcModule("TrafficManager", &m_controlStatus, &m_mergedDirective, 100000, 10000)
  , m_debug(debug)
  , m_verbose(verbose)
  , m_log(log)
  , m_currentID(0)
  , m_latestID(0)
{
  m_controlGraph = ControlStateFactory::createControlStates();

  /*!\param GcInterface variable */
  m_missTraffInterface = new MissionTrafficInterface(skynetKey, this);
  assert(m_missTraffInterface != NULL);
  m_missTraffInterfaceNF = m_missTraffInterface->getNorthface();

  /* Initialize ControlStatus to wait until a command is received */
  m_controlStatus.status = TrafficManagerControlStatus::READY_FOR_NEXT;

}

TrafficManager::~TrafficManager()
{
  delete m_missTraffInterface;
  delete m_missTraffInterfaceNF;
  delete m_traffCorrInterface;
  delete m_traffCorrInterfaceSF;

}

void TrafficManager::TrafficPlanningLoop(void)
{

}

void TrafficManager::arbitrate(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus *controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective *mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);

  SegGoalsStatus segGoalsStatus; 

  /* Check on the status of the last command acted on by control */
  if (controlStatus->status == TrafficManagerControlStatus::COMPLETED ||
      controlStatus->status == TrafficManagerControlStatus::FAILED) {
    
    SegGoalsStatus response;
    response.goalID = controlStatus->id;
    
    if (controlStatus->status == TrafficManagerControlStatus::COMPLETED) {
      response.status = SegGoalsStatus::COMPLETED;
      
    } else {
      response.status = SegGoalsStatus::FAILED;
      //response.reason = controlStatus->reason;
    }

    cerr << "TrafficManagerControl: sending response (" 
	 << response.id << ") - "
	 << (response.status == SegGoalsStatus::COMPLETED ? "completed" : "other")
	 << endl;
    m_missTraffInterfaceNF->sendResponse( &response );
  }

  // Compute the next mergedDirective
  if (m_missTraffInterfaceNF->haveNewDirective()) {
    SegGoals newDirective;
    m_missTraffInterfaceNF->getNewDirective( &newDirective );

    cerr << "TrafficManagerControl: have new directive (" 
	 << newDirective.goalID << ")" << endl;

    // TODO: Determine if you want to reject this directive. This
    // includes checking that the id of newDirective is unique.
    bool reject = false;

    // Update the merged directive if newDirective is not rejected.
    if (!reject) {
      mergedDirective->id = newDirective.goalID;
    } else {
      // TODO: Default extension
    }
  }
}

void TrafficManager::control(ControlStatus* cs, MergedDirective* md) {

  TrafficManagerControlStatus* controlStatus =
    dynamic_cast<TrafficManagerControlStatus *>(cs);
  TrafficManagerMergedDirective* mergedDirective =
    dynamic_cast<TrafficManagerMergedDirective *>(md);

  double number_arg = mergedDirective->number_arg;

  if (mergedDirective->id != m_currentID) {
    cerr << "Sending command " << number_arg << " to Corridor Control (" 
	 << mergedDirective->id << ")" << endl;
    m_currentID = mergedDirective->id;
    controlStatus->id = m_currentID;
    controlStatus->status = TrafficManagerControlStatus::COMPLETED;

  } else {
    /* If no new directive, we are still executing the old one */
    controlStatus->status = TrafficManagerControlStatus::READY_FOR_NEXT;
  }
}


ControlState* TrafficManager::determineControlState(ControlState  *currControlState, TrafficState *currTrafficState, PlanningHorizon currPlanHorizon, Map* localMap, VehicleState currVehState) 
{
  ControlState* controlState; 

	// m_currSegGoals = currSegGoals; 
	//  m_localMap = localMap; 
	//  m_currVehState = vehState;

  
  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(currControlState); 

  //  determinePlanningHorizon();	
  unsigned int i = 0;
  ControlStateTransition* transition;

  for (i; i < transitions.size(); i++) 
    {		
      transition = static_cast<ControlStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(currControlState, currTrafficState, currPlanHorizon, localMap, currVehState);
    }
  
  controlState = chooseMostProbableControlState(currControlState, transitions);
  
  if (currControlState->getType() == controlState->getType())
    {
      controlState = currControlState;
    } else {
      controlState->setInitialPosition(currVehState);
      controlState->setInitialVelocity(currVehState);
      controlState->setInitialTime();
    }
  
  return controlState; 
  //  m_currStateType = (ControlStateFactory::ControlStateType) m_currControlState.getType(); //FIX we may not need this 
}


Conflict TrafficManager::evaluateDynamicConflicts(Corridor m_currCorridor, ControlState *controlState) 
{
  Conflict conflict;
  return conflict;
}

void TrafficManager::evaluateCorridors() 
{
  //return m_currCorridor;
}

// ControlState TrafficManager::getCurrentControlState() 
// {
//   return m_currControlState;
// }

// ControlStateFactory::ControlStateType TrafficManager::getCurrentControlStateType() 
// {
//   return m_currStateType;
// }


ControlState* TrafficManager::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
	ControlState* currControlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficManager::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
				transProb = transitions[i]->getUncertainty();
				if(1 == transProb) {
					probOneTrans.push_back(transitions[i]);
				} else if ( m_probThresh < transProb) {
					probAboveThresh.push_back(transitions[i]);		
				} else if ( m_probThresh >= transProb) {
					probBelowThresh.push_back(transitions[i]);		
				}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
			currControlState = controlState; 
		}
  } else { 
    if ((m_verbose) || (m_debug)){  
      cout << "in TrafficManager::chooseMostProbable... received transitions of size " << transitions.size() << endl;
    }
  }
	

  return currControlState;
}

void TrafficManager::determinePlanningHorizon(PlanningHorizon & currPlanHorizon, TrafficState * currTrafficState, list<SegGoals> currSegGoals) 
{
  //cout << "in TrafficManager::determinePlanningHorizon:" << endl;
  //cout << "currSegGoals size = " << currSegGoals.size() <<endl;
  
  //cout << "curr type = " << currTrafficState->getType() << endl;
  
  TrafficStateFactory::TrafficStateType tStateType = (TrafficStateFactory::TrafficStateType) currTrafficState->getType();
  
  //cout << "tStateType =  "<< tStateType <<endl;
  
  currPlanHorizon.clear();
  
  switch(tStateType) {
  case TrafficStateFactory::ZONE_REGION:		// want to plan over two segments

    currPlanHorizon.addGoal(currSegGoals.front()); 
    currSegGoals.pop_front();
    currPlanHorizon.addGoal(currSegGoals.front()); 		
    break;
  case TrafficStateFactory::ROAD_REGION:
    // plan over one segment
    currPlanHorizon.addGoal(currSegGoals.front()); 
    break;
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    // plan over one segment
    currPlanHorizon.addGoal(currSegGoals.front()); 
    break;
  case TrafficStateFactory::INTERSECTION_STOP:
    // plan over two segments
    currPlanHorizon.addGoal(currSegGoals.front()); 
    currSegGoals.pop_front();
    currPlanHorizon.addGoal(currSegGoals.front()); 		
    break;
  default:
    break;
  };
  if ((m_verbose) || (m_debug)){  
    cout<<"SIZE OF CURRPLANHORIZON GOALS " <<  currPlanHorizon.getSize()<< endl;
  } 

  //m_currPlanHorizon = subSegGoals;
  
}

