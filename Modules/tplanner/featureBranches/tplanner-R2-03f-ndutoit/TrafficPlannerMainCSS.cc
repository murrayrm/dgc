#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "TrafficManager.hh"
#include "CorridorGenerator.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include "skynet/skynet.hh"
#include "iostream"
#include <sys/time.h>
#include <sys/stat.h> 

using namespace std;             
     
int main(int argc, char **argv)              
{

  gengetopt_args_info cmdline;    

 
  int debugLevel = 0, verboseLevel = 0, sn_key = 0;
  bool recvLocalMap = false; 
  bool recvSegmentGoals = false;
  bool enableLogging = false;
  string RNDFfilename; 
  /* Figure out where logged data go */  
  string logFileName;
  FILE* logFile;

  sn_key = skynet_findkey(argc, argv);
  debugLevel = cmdline.debug_arg;
  verboseLevel = cmdline.verbose_arg; 
 
  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
    
  if (!cmdline.nomap_given)
  {
    recvLocalMap = true;
  } 
      
  if (!cmdline.nogoals_given)
  {
    recvSegmentGoals = true;
  } 

  // Initialize the map with rndf if given
  if (cmdline.rndf_given){
    RNDFfilename = cmdline.rndf_arg;
    cout << "RNDF Filename in = "  << RNDFfilename << endl;
    //Load the RNDF in the constructor of TrafficManager 
  }     

  if(true == (enableLogging = cmdline.enable_logging_given))
  {
    string tmpMDFname;
  
    ostringstream oss;
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
    oss << timestr << "-tplanner-" << RNDFfilename << ".log";
    logFileName = oss.str();
    string suffix = "";

    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    logFile = fopen(logFileName.c_str(), "w");
    if (logFile == NULL)
    {
      cerr << "Cannot open log file: " << logFile << endl;
      exit(1);
    }
  }

  if (cmdline.disable_console_flag){
    cout << "No display" << endl;
    cout << "Constructing skynet with KEY = " << sn_key << endl;
    cout  << "debug level = " << debugLevel << endl;
    cout << "verbose level = " << verboseLevel << endl;
  }


  TrafficManager* traffManager = new TrafficManager(sn_key, debugLevel, verboseLevel, true, true, RNDFfilename, !cmdline.disable_console_flag, 
						    dgcFindConfigFile(cmdline.config_arg, "tplanner")); 
  sleep(10); 

  traffManager->Start();

  while (true) {
    sleep(1);
  }
    
  if(cmdline.enable_logging_given)
    {
      pclose(logFile);
    }
  

  return 0;
}

