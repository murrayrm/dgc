/*!CheckPass.cc
 * Author: Christian Looman
 * Last revision: May 09 2007
 * */

#include "CheckPass.hh"

using namespace std;

CheckPass::CheckPass()
{
  // Initialize the boolean variables
  ListCreated=false;
  ClearToGo=false;
  LegalToGo=false;
}

CheckPass::~CheckPass() 
{
}

void CheckPass::checkForObstacles(VehicleState vehState, Map* localMap, LaneLabel lane,int searchDirection, double& distance_obstacle,double& vel_obstacle, double& size_obstacle)
{
  // Calculate Alice position of Front Bumper
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);

  // Get Alice's lane information
  LaneLabel lane_alice;
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);
  localMap->getLane(lane_alice,position_alice);
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  vector<MapElement> obstacle;
  localMap->getObsInLane(obstacle,lane);
  distance_obstacle=10000;
  point2 position_obstacle;
  int id_obstacle=-1;

  double distance_temp;
  // Loop through all obstacles found in lane and find the one with the lowest distance
  for (unsigned int i=0;i<obstacle.size();i++)
    {
      if (obstacle[i].type!=ELEMENT_OBSTACLE && obstacle[i].type!=ELEMENT_VEHICLE) continue;

      // Transform from uncertain to certain position
      position_obstacle.set(obstacle[i].position);
      localMap->getDistAlongLine(distance_temp,centerline_alice,position_obstacle,position_alice);
      // Find lowest distance

      if (((searchDirection==AHEAD && distance_temp>0) || (searchDirection==BEHIND && distance_temp<0)) && fabs(distance_temp)<distance_obstacle)
	{
	  distance_obstacle=fabs(distance_temp);
	  id_obstacle=i;
	}
    }
  size_obstacle=6;
  vel_obstacle=0;

  if (id_obstacle!=-1)
    {
      if (obstacle[id_obstacle].length>obstacle[id_obstacle].width)
	size_obstacle=obstacle[id_obstacle].length;
      else
	size_obstacle=obstacle[id_obstacle].width;

      vel_obstacle=sqrt(pow(obstacle[id_obstacle].velocity.x,2)+pow(obstacle[id_obstacle].velocity.y,2));

      // The distance needs to be corrected to obtain the bumper2bumper distance
      distance_obstacle=distance_obstacle-size_obstacle/2;
    }
  else
    distance_obstacle=-1;
}


double CheckPass::getProbability(VehicleState vehState,Map* localMap)
{
  const int SAFETY_FACTOR=20;
  const double size_alice=VEHICLE_LENGTH;

  // COMPUTE ALICE'S VALUES
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);

  // Calculate Velocity over Ground
  double vel_alice=sqrt(vehState.utmNorthVel*vehState.utmNorthVel + vehState.utmEastVel*vehState.utmEastVel);
  // Get Alice's lane
  LaneLabel lane_alice;
  point2arr centerline_alice;
  localMap->getLane(lane_alice,position_alice);

  // Get general lane information whether to turn legally or illegaly
  vector<LaneLabel> SameDirLanes;
  bool LegalTurn;
  localMap->getSameDirLanes(SameDirLanes,lane_alice);
  if (SameDirLanes.size()>1)
    {
      LegalTurn=true;
      cout<<"Legal passing possible...true"<<endl;
    }
  else
    {
      cout<<"Legal passing possible...false"<<endl;
      LegalTurn=false;
    }

  // If illegal turn is necessary, check for oncoming traffic
  if (!LegalTurn)
    {

      // COMPUTE OBSTACLE1'S VALUES
      double distance_obstacle1;
      double vel_obstacle1;
      double size_obstacle1;
      checkForObstacles(vehState,localMap,lane_alice,AHEAD,distance_obstacle1,vel_obstacle1,size_obstacle1);
      // if no obstacle was found, return probability of .70
      if (distance_obstacle1==-1) return .70;

      // COMPUTE OBSTACLE2'S VALUES
      double distance_obstacle2;
      double vel_obstacle2;
      double size_obstacle2;
      LaneLabel lane_opposite;
      localMap->getNeighborLane(lane_opposite,lane_alice,-1);
      checkForObstacles(vehState,localMap,lane_opposite,AHEAD,distance_obstacle2,vel_obstacle2,size_obstacle2);
      // if no obstacle was found, return probability of .70
      if (distance_obstacle2==-1) return .70;

      double vel_difference=vel_alice-vel_obstacle1;
      // If obstacle is faster than Alice, return 0
      if (vel_difference<0) return 0;

      // the necessary time to pass a vehicle has two components:
      // 1) time to pass obstacle and be max 3*length of Alice ahead to make a safe lanechange back into own lane
      // 2) Uncertainties
      double corridor_alice=distance_obstacle1+size_obstacle1+3*size_alice;
      double t_1 = sqrt(2*corridor_alice/(0.7*VEHICLE_MAX_ACCEL));
      double t_2 = 0.5;
        
      double t_necessary=t_1+t_2;

      // The length of the obstacle's corridor is computed by computing the distance that the obstacle drives in the time
      // that Alice needs to pass it
      double corridor_obstacle2=vel_obstacle2*t_necessary;

      // Compute how much of the corridor on the opposite lane is left when Alice would pass the obstacle
      double corridor_left=distance_obstacle2-corridor_obstacle2-corridor_alice;
      // Output some results
      cout<<"Vehicle Speed..."<<vel_alice<<endl;
      cout<<"Position Alice..."<<position_alice<<endl;
      cout<<"Obstacle Distance own lane..."<<distance_obstacle1<<endl;
      cout<<"Obstacle velocity own lane..."<<vel_obstacle1<<endl;
      cout<<"Obstacle Distance opposite lane..."<<distance_obstacle2<<endl;
      cout<<"Obstacle velocity opposite lane..."<<vel_obstacle2<<endl;
      cout<<"necessary passing time..."<<t_necessary<<endl;
      cout<<"Corridor length Alice..."<<corridor_alice<<endl;
      cout<<"Corridor length obstacle2..."<<corridor_obstacle2<<endl;
      cout<<"Corridor left..."<<corridor_left<<endl;


      if (corridor_left<0)
	{
	  return 0;
	}
      else
	{
	  double ratio=(corridor_left/size_alice*SAFETY_FACTOR)/100;
	  cout<<"Ratio..."<<ratio<<endl;
	  return (1 < ratio)?1:ratio;
	}
    } 
  // Check for traffic on neighbor lanes check whether lanes are free
  else
    {
       for (unsigned int i=0;i<SameDirLanes.size();i++)
 	{
  	  if (SameDirLanes[i].lane!=lane_alice.lane)
  	    {
	      // COMPUTE OBSTACLE1'S VALUES
	      double distance_obstacle1;
	      double vel_obstacle1;
	      double size_obstacle1;
	      checkForObstacles(vehState,localMap,SameDirLanes[i],BEHIND,distance_obstacle1,vel_obstacle1,size_obstacle1);
	      // if no obstacle was found, return probability of .70
	      if (distance_obstacle1==-1) return .70;

	      if ((vel_obstacle1>0) && (distance_obstacle1/vel_obstacle1)<5)
		return 0;
	      else if ((vel_obstacle1>0) && (distance_obstacle1/vel_obstacle1)>10)
		return .70;
	      else
		return 1;

	      // Output some results
	      cout<<"Vehicle Speed..."<<vel_alice<<endl;
	      cout<<"Position Alice..."<<position_alice<<endl;
	      cout<<"Obstacle Distance neighbor lane..."<<distance_obstacle1<<endl;
	      cout<<"Obstacle velocity neighbor lane..."<<vel_obstacle1<<endl;
  	    }
 	}
    }
  return -1;
 }

bool CheckPass::checkIntersection(VehicleState vehState,Map* localMap)
{
  vector<PointLabel> WayPoint;
  vector<PointLabel> WayPointEntries;
  vector<PointLabel> WayPoint_WithStop;
  vector<PointLabel> WayPoint_NoStop;
  vector<PointLabel> StopLines;
  PointLabel StopLine_alice,StopLine_reverse;
  point2 p;
  double distance_stopline_alice,distance_temp,distance_reverse;
  const double T_CREATE_PREC_LIST=10.0;

  // Obtain Alice's position
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);
  LaneLabel lane_alice;
  localMap->getLane(lane_alice,position_alice);

  // get Alice's centerline
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  if (localMap->getLaneStopLines(StopLines,lane_alice)!=0)
    {
      distance_stopline_alice=INFINITY;
      distance_reverse=INFINITY;

      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      for (unsigned int i=0; i<StopLines.size(); i++)
	{
	  localMap->getWaypoint(p,StopLines[i]);
	  localMap->getDistAlongLine(distance_temp,centerline_alice,p,position_alice);

	  if (distance_temp>0 && distance_temp<distance_stopline_alice)
 	  {
	    distance_stopline_alice=distance_temp;
	    StopLine_alice=StopLines[i];
	  }

	  if (distance_temp<0 && fabs(distance_temp)<distance_reverse)
	    {
	      distance_reverse=fabs(distance_temp);
	      StopLine_reverse=StopLines[i];
	    }
	}

      if (distance_stopline_alice<INFINITY && !ListCreated)
	{
	  // get all entry points of the intersection
	  populateWayPoints(localMap,WayPointEntries,StopLine_alice);
	  findStoplines(localMap,WayPointEntries,WayPoint_NoStop,WayPoint_WithStop);
	}

      // If Alice is close enough to intersection, create list of obstacles at other stop lines
      if (distance_stopline_alice<T_CREATE_PREC_LIST && !ListCreated)
	{
	  ListCreated=true;
	  cout<<"Precedence List created."<<endl;
	  cout<<"Distance Alice-Stopline..."<<distance_stopline_alice<<"m"<<endl;

	  createListTimeOfArrival(localMap,WayPoint_WithStop,T_CREATE_PREC_LIST);
	  cout<<"Vehicles found with precedence..."<<PrecedenceList.size()<<endl;

	  for (unsigned int m=0;m<PrecedenceList.size();m++)
	    {
	      cout<<"ToA["<<m<<"] Id..."<<PrecedenceList[m].Id<<endl;
	      cout<<"ToA["<<m<<"] WayPoint..."<<PrecedenceList[m].WayPoint<<endl;
	      cout<<"ToA["<<m<<"] distance..."<<PrecedenceList[m].distance<<endl;
	      cout<<"ToA["<<m<<"] velocity..."<<PrecedenceList[m].velocity<<endl;
	    }
	}
      else if (ListCreated && PrecedenceList.size()>0)
	checkExistenceObstacles(localMap);
      else if (!ListCreated)
	{
	  cout<<"Next Stopline Alice..."<<StopLine_alice<<", distance "<<distance_stopline_alice<<"m"<<endl;
	  cout<<"Last Stopline Alice..."<<StopLine_reverse<<", distance "<<distance_reverse<<"m"<<endl;
	  cout<<"Don't know why this function was called. But give ok for Legal&Clear to go"<<endl;
	  LegalToGo=true;
	  ClearToGo=true;
	}
      
      // If list was created and the list of vehicles with precedence is empty (there were no vehicles or they all left the intersection), then it's legal to go
      if (ListCreated && PrecedenceList.size()==0)
	  LegalToGo=true;

      // After it is legal to go, check for whether it's clear to go
      if (LegalToGo && !ClearToGo)
	{
	  populateWayPoints(localMap,WayPointEntries,StopLine_alice);
	  ClearToGo=checkClearance(localMap,WayPointEntries,0.5);
	}

      cout<<"Intersection legal to go..."<<LegalToGo<<endl;
      cout<<"Intersection clear to go..."<<ClearToGo<<endl;

      if (LegalToGo && ClearToGo)
	{
	  return true;
	}
      else
	{
	  return false;
	}
    }
  else
    distance_stopline_alice=-1;
  return false;
}

void CheckPass::resetIntersection()
{
  ListCreated=false;
  LegalToGo=false;
  ClearToGo=false;
  PrecedenceList.clear();
}

void CheckPass::checkExistenceObstacles(Map* localMap)
{
  point2 p,position_obstacle;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  vector<ListToA> tempPrecedenceList;
  double distance_temp;

  // Look for obstacles at all stoplines
  for (unsigned int i=0; i<PrecedenceList.size(); i++)
    {
      localMap->getWayPoint(p,PrecedenceList[i].WayPoint);
      localMap->getLane(lane,p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);
      
      bool found=false;
      // Loop through all obstacles found in lane and check whether they match the one in precedence list
      for (unsigned int j=0;j<obstacle.size();j++)
	{
	  if (obstacle[j].id==PrecedenceList[i].Id)
	    {
	      position_obstacle.set(obstacle[j].position);
	      localMap->getDistAlongLine(distance_temp,centerline,p,position_obstacle);

	      // If distance is less than zero. This means that obstacle passed the waypoint
	      // Then vehicle is considered to "almost" left the intersection and will be deleted
	      // out of the list of precedence
	      if (distance_temp>0)
		found=true;
	    }
	}

      // If obstacle is still found then put in new list as well, otherwise drop it
      if (found)
	tempPrecedenceList.push_back(PrecedenceList[i]);
      else
	cout<<"Remove obstacle at WayPoint..."<<PrecedenceList[i].WayPoint<<endl;
    }

  PrecedenceList=tempPrecedenceList;
}

void CheckPass::createListTimeOfArrival(Map* localMap, vector<PointLabel>& WayPoint_WithStop,double distance)
{
  point2 p;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  point2 position_obstacle;
  int id_obstacle=-1;
  double distance_temp,distance_obstacle;
  ListToA list_temp;

  // Look for obstacles at all stoplines
  for (unsigned int i=0; i<WayPoint_WithStop.size(); i++)
    {
      localMap->getWayPoint(p,WayPoint_WithStop[i]);
      localMap->getLane(lane,p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);
      
      distance_obstacle=INFINITY;

      cout<<"Looking at waypoint..."<<WayPoint_WithStop[i]<<". Number of obstacles found "<<obstacle.size()<<endl;

      // Loop through all obstacles found in lane and find the one with the lowest distance
      for (unsigned int j=0;j<obstacle.size();j++)
	{
	  if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE) continue;

	  // Transform from uncertain to certain position
	  position_obstacle.set(obstacle[j].position);
	  localMap->getDistAlongLine(distance_temp,centerline,p,position_obstacle);

	  double v;
	  v=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));

	  // Find lowest distance
	  // THIS NEEDS SOME DEBUGGING WHETHER IT IS DETECTED CORRECTLY WHETHER OBSTACLE IS BEFORE OR BEHIND LINE
 	  if (distance_temp>0 && distance_temp<distance_obstacle)
 	    {
 	      distance_obstacle=distance_temp;
 	      id_obstacle=j;
 	    }
	}

      cout<<"Obstacle distance to waypoint..."<<distance_obstacle<<endl;
  
      // if obstacle is within a certain distance to the stopline
      if (distance_obstacle<distance)
	{
	  // Create list object
	  list_temp.Id=obstacle[id_obstacle].id;
	  list_temp.WayPoint=WayPoint_WithStop[i];
	  list_temp.velocity=sqrt(pow(obstacle[id_obstacle].velocity.x,2)+pow(obstacle[id_obstacle].velocity.y,2));
	  list_temp.distance=distance_obstacle;

	  // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<1m/s
	  if (list_temp.velocity>1)
	    list_temp.time=0+list_temp.distance/list_temp.velocity;
	  else
	    list_temp.time=0;

	  PrecedenceList.push_back(list_temp);
	}
    }
}

bool CheckPass::checkClearance(Map* localMap, vector<PointLabel>& WayPointEntries,double distance_uncertain)
{
  vector<PointLabel> WayPointExits;
  point2 p_entry,p_exit,position_obstacle;
  LaneLabel lane_entry,lane_exit;
  vector<MapElement> obstacles;
  double distance_entry,distance_exit;
  point2arr centerline;

  bool IntersectionClear=true;

  // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
  for (unsigned i=0; i<WayPointEntries.size(); i++)
    {
      localMap->getWaypoint(p_entry,WayPointEntries[i]);
      localMap->getLane(lane_entry,p_entry);

      // get all exits for this entry
      localMap->getWayPointExits(WayPointExits,WayPointEntries[i]);
      for (unsigned j=0;j<WayPointExits.size(); j++)
	{
	  localMap->getWaypoint(p_exit,WayPointExits[j]);
	  localMap->getLane(lane_exit,p_exit);
	  
	  // If Entry/Exit-lane is the same, check whether there are obstacles in between
	  if (lane_entry==lane_exit)
	    {
	      localMap->getLaneCenterLine(centerline,lane_entry);

	      localMap->getObsInLane(obstacles,lane_entry);
	      for (unsigned int k=0; k<obstacles.size(); k++)
		{
		  position_obstacle.set(obstacles[k].position);
		  localMap->getDistAlongLine(distance_entry,centerline,p_entry,position_obstacle);
		  localMap->getDistAlongLine(distance_exit,centerline,p_exit,position_obstacle);

		  // If obstacle is in between Entry and Exit, return false!
		  if (distance_entry<0 && distance_exit>0)
		    {
		      IntersectionClear=false;
 		      cout<<"Intersection...blocked by obstacle"<<endl;
		    }
		}
	    }
	}
    }

  return IntersectionClear;
}


void CheckPass::populateWayPoints(Map* localMap, vector<PointLabel>& WayPointEntries, PointLabel InitialWayPointEntry)
{
  vector<PointLabel> WayPointExits;
  vector<PointLabel> WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++)
    {
      localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++)
	{
	  bool found=false;
	  for (unsigned int k=0; k<WayPointEntries.size(); k++)
	    {
	      if (WayPoint[j]==WayPointEntries[k])
		{
		  found=true;
		  break;
		}
	    }

	  // If not, add it
	  if (!found)
	    {
	      WayPointEntries.push_back(WayPoint[j]);
	      populateWayPoints(localMap,WayPointEntries,WayPoint[j]);
	    }
	}
    }
}


void CheckPass::findStoplines(Map* localMap, vector<PointLabel>& WayPoint, vector<PointLabel>& WayPoint_NoStop, vector<PointLabel>& WayPoint_WithStop)
{
  for (unsigned int i=0; i<WayPoint.size(); i++)
    {
      if (localMap->isStopLine(WayPoint[i]))
	WayPoint_WithStop.push_back(WayPoint[i]);
      else
	WayPoint_NoStop.push_back(WayPoint[i]);
    }
}

















