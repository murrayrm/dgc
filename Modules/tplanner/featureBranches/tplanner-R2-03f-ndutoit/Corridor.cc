#include "Corridor.hh"
#include "state/AliceStateHelper.hh"
#include "ControlState.hh"

#define DISTANCE_TO_OBSTACLE 20

GisCoordLatLon latlon;
GisCoordUTM utm;

Corridor::Corridor()   
  : m_nPolytopes(1)
  , m_polyCorridorTalker(0,SNpolyCorridor,MODdynamicplanner) 
 
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_polyCorridor = new CPolytope[m_nPolytopes];
  m_use_local = true; // Default for now is to use global coordinates - want to change this!!
  m_polyCorridorLegal = false;
}

Corridor::Corridor(bool debug, bool verbose, bool log)  
  : m_nPolytopes(1)
  , m_polyCorridorTalker(0,SNpolyCorridor,MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = verbose;
  m_debug = debug;
  m_use_local = true; // Default for now is to use global coordinates - want to change this!!
  m_polyCorridorLegal = false;
  
}

Corridor::Corridor(int skynetKey, bool debug, bool verbose, bool log)  
  : m_nPolytopes(1)
  , m_polyCorridorTalker(skynetKey,SNpolyCorridor,MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = verbose;
  m_debug = debug;
  m_use_local = true; // Default for now is to use global coordinates - want to change this!!
  m_polyCorridorLegal = false;
}

Corridor::Corridor(CmdLineArgs cLArgs)
  : m_nPolytopes(1)
  , m_polyCorridorTalker(cLArgs.sn_key, SNpolyCorridor, MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = cLArgs.verbose;
  m_debug = cLArgs.debug;
  m_use_local = cLArgs.use_local;
  m_polyCorridorLegal = false;
}

Corridor::Corridor(const Corridor& corr)
  : m_polyCorridorTalker(0,SNocpParams,MODdynamicplanner)
{
  utm.zone = 11;
  utm.letter = 'S';

  m_verbose = corr.m_verbose;
  m_debug = corr.m_debug;
  m_log = corr.m_log;

  m_geometricConstraints = NULL; /* This is not used for now */
  m_conflict = NULL; /* This is not used for now */

  m_polylines = corr.m_polylines;
  m_speedProfile = corr.m_speedProfile;
  m_accProfile = corr.m_accProfile;
  m_velProfile = corr.m_velProfile;
  m_polyCorridorLegal = corr.m_polyCorridorLegal;

  m_controlStateId = corr.m_controlStateId;
  m_desiredAcc = corr.m_desiredAcc;
  m_distFromConStateBegin = corr.m_distFromConStateBegin;
  m_alicePrevPos = corr.m_alicePrevPos;
  m_ocpParams = corr.m_ocpParams;
  m_nPolytopes = corr.m_nPolytopes;

  m_polyCorridor = new CPolytope [m_nPolytopes];
  *m_polyCorridor = *corr.m_polyCorridor;
}

Corridor::~Corridor() 
{
  //TODO why this is getting called at construction
  //delete[] m_polyCorridor; 
}

  
GeometricConstraints* Corridor::getGeometricConstraints() 
{
	return new GeometricConstraints(); 
}

  
int Corridor::getControlStateId() {

	return m_controlStateId;
  
}

void Corridor::addPolyline(point2arr line)
{
  m_polylines.push_back(line);
	//cout << "m_polylines.size = " << m_polylines.size() << endl;
}

vector<point2arr> Corridor::getPolylines()
{
  return m_polylines;
}

vector<double> Corridor::returnSpeedProfile()
{
  return m_speedProfile;
}

void Corridor::setAlicePrevPos(point2 alicePos)
{
  m_alicePrevPos = alicePos;
}

void Corridor::clear() 
{
  m_polylines.clear();

}


void Corridor::setSpeedProfileDist(point2 initPos, double initSpeed, double finalSpeed, point2 finalPos, bool insertPt)
{
  double dist_stop = 0;
  double dist = 0;
  point2 tmpPt, temppt1, temppt2;

  if (m_polylines.size() < 2) return;
  
  point2arr leftBound;
  leftBound.set(m_polylines[0]);
  point2arr rightBound; 
  rightBound.set(m_polylines[1]);
  point2arr centerline;

  m_speedProfile.clear();
  m_accProfile.clear();

  // need to insert a point on the boundary at the distance along the corridor to plan vel profile from
  if (insertPt) {
    TrafficUtils::insertProjPtInBoundary(leftBound, initPos);
    TrafficUtils::insertProjPtInBoundary(rightBound, initPos);
    m_polylines.clear();
    m_polylines.push_back(leftBound);
    m_polylines.push_back(rightBound);
  }

  //get the center line of the corridor
  for (int ii=0; ii<(int)leftBound.size(); ii++){
    tmpPt = (leftBound[ii]+rightBound[ii])/2;
    centerline.push_back(tmpPt);
  }
  
  //  cout << "centerline = " << centerline << endl;

  // get index of the boundary pt closest to initPos
  unsigned int index = TrafficUtils::getClosestPtIndex(centerline, initPos);
  //cout << "index = " << index << endl;
  // set the velocity up to that point as v_in;
  for (unsigned int ii=0; ii<=index; ii++) {
    m_speedProfile.push_back(initSpeed);
    m_accProfile.push_back(0);
  }

  //cout << "Speed profile 1 = " << m_speedProfile << endl;
  // calculate the distance to the final pos, from the init pos along the centerline
  for (unsigned int ii=index+1; ii<centerline.size(); ii++) {
    // cout << "ii = " << ii << endl;
    dist_stop += centerline[ii-1].dist(centerline[ii]);
  }

  // cout << "dist to stop = " << dist_stop << endl;
  // calculate the desired acc based on this distance
  double acc_req = (finalSpeed*finalSpeed - initSpeed*initSpeed)/2/dist_stop;

  //cout << "acc req = " << acc_req << endl;

  //interpolate the rest of the way to the stopping location
  double tmp;
  for (unsigned int ii=index+1; ii<centerline.size(); ii++) {
    //cout << "ii = " << ii << endl;
    dist += centerline[ii-1].dist(centerline[ii]);
    //cout << "dist = " << dist << endl;
    tmp = (finalSpeed-initSpeed)/dist_stop*dist + initSpeed;
    // make sure that we do not go below initSpeed
    if (tmp>finalSpeed)
      m_speedProfile.push_back(tmp);
    else
      m_speedProfile.push_back(finalSpeed);

    m_accProfile.push_back(acc_req);
  }

  return;
}

void Corridor::setVelProfile(Map *localMap, VehicleState vehState, TrafficState *currTrafState, ControlState *currControlState, PlanningHorizon planHoriz)
{
  SegGoals currSegGoal = planHoriz.getSegGoal(0);
  PointLabel exitPointID(currSegGoal.exitSegmentID, currSegGoal.exitLaneID, currSegGoal.exitLaneID);
  PointLabel entryPointID(currSegGoal.entrySegmentID, currSegGoal.entryLaneID, currSegGoal.entryLaneID);
  point2 entryPt, exitPt;
  point2 currFrontPos  = AliceStateHelper::getPositionFrontBumper(vehState);
  double vel_in, vel_out, heading;
  bool obsPresent = false;
  point2 obsPos;
  bool insertPtForVelProfile = true;
  double dec_des = -0.5;

  // Check if there are any obstacles that we need to deal with
  // SVEN: double dist2Obs = localMap->getObstacleDist(currFrontPos, 0);
  LaneLabel desiredLane = AliceStateHelper::getDesiredLaneLabel();
  double dist2Obs = TrafficUtils::getNearestObsDist(localMap, vehState, desiredLane);
  if (dist2Obs!=-1) {
    // only deal with obstacles within 30m
    if (dist2Obs<30) {
      cout << "Obstacle less than 30m away!" << endl;
      obsPresent = true;
      // SVEN: obsPos = localMap->getObstaclePoint(currFrontPos,0);
      TrafficUtils::getNearestObsPoint(obsPos, localMap, vehState, desiredLane, 0);
      cout << "Obstacle at: " << obsPos << endl;
    }
  }

  switch (currControlState->getType()) 
    {
    case ControlStateFactory::LANE_KEEPING:
      {

        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0.5;
          vel_out = 0.5;          
        } else {
          localMap->getWaypoint(exitPt, exitPointID);
          entryPt = currFrontPos;
          vel_in = currSegGoal.maxSpeedLimit;
          vel_out = currSegGoal.maxSpeedLimit;
        }

        if (obsPresent){
          // calculate a vel profile based on desired decceleration
          // TODO: set global variable for some of these things, like the stopping 20m vefore the obstacle, stopping 1m in front of the line, desired deceleration, etc
          if (dist2Obs<DISTANCE_TO_OBSTACLE)
            vel_in = 0; //stop now!
          else
            vel_in = sqrt(-2*dec_des*(dist2Obs-DISTANCE_TO_OBSTACLE));
          
          if (vel_in>currSegGoal.maxSpeedLimit) { 
            vel_in = currSegGoal.maxSpeedLimit;
          }
          vel_out = 0;
          
          // plan to 20 m before the obstacle
          // SVEN: exitPt = localMap->getObstaclePoint(currFrontPos,DISTANCE_TO_OBSTACLE);
          TrafficUtils::getNearestObsPoint(exitPt, localMap, vehState, desiredLane, DISTANCE_TO_OBSTACLE);
          localMap->getHeading(heading, exitPt);          
          // need to update the final condition
          setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondLB(VELOCITY_IDX_C, 0);
          setOCPfinalCondUB(VELOCITY_IDX_C, 0);
          setOCPfinalCondLB(HEADING_IDX_C, heading);
          setOCPfinalCondUB(HEADING_IDX_C, heading);          
        }
      }
      break;

    case ControlStateFactory::LANE_CHANGE:
      {
        // Nominal driving
        localMap->getWaypoint(exitPt, exitPointID);
        entryPt = currFrontPos;
        vel_in = currSegGoal.maxSpeedLimit;
        vel_out = currSegGoal.maxSpeedLimit;
      }
      break;
      
    case ControlStateFactory::STOP:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0;
          vel_out = 0;
        }
        else {
          localMap->getNextStopline(exitPt, exitPointID);
          // want to use the pos and vel when we switch into this state to calculate
          // desired initial velocty to ensure that we follow the correct vel profile
          point2 initPos = currControlState->getInitialPosition();
          // TODO: replace with distance to stopline along lane function
          double distInit2Stop = exitPt.dist(initPos);
          //          cout << "distance Init to stop = " << distInit2Stop << endl;
          // TODO: need to replace this with a function that returns dist along lane
          double distFromEntry = initPos.dist(currFrontPos);
          // cout << "distance from entry into STOP = " << distFromEntry << endl;
          double v_init = currControlState->getInitialVelocity();
          //cout << "initial velocity = " << v_init << endl;
          vel_in = (0-v_init)/distInit2Stop*distFromEntry + v_init;
          //cout << "vel_in = " << vel_in << endl;
          if (vel_in<0){
            vel_in = 0;
            cout << "vel_in < 0, set to " << vel_in << endl;
          }
          
          vel_out = 0;
          //TODO: Need to make the entry point here the entry waypoint
          entryPt = currFrontPos;
        }

        if (obsPresent){
          // calculate a vel profile based on desired decceleration
          // TODO: set global variable for some of these things, like the stopping 20m vefore the obstacle, stopping 1m in front of the line, desired deceleration, etc
          //cout << "Obs Pos = " << obsPos << endl;
          //cout << "distToObs = " << dist2Obs << endl;
          if (dist2Obs<DISTANCE_TO_OBSTACLE && dist2Obs>0)
            vel_in = 0; //stop now!
          else
            vel_in = sqrt(-2*dec_des*(dist2Obs-DISTANCE_TO_OBSTACLE));

          if (vel_in>currSegGoal.maxSpeedLimit) { 
            vel_in = currSegGoal.maxSpeedLimit;
          }
          vel_out = 0;
          
          // plan to 20 m before the obstacle
          // SVEN: exitPt = localMap->getObstaclePoint(currFrontPos,DISTANCE_TO_OBSTACLE);
          TrafficUtils::getNearestObsPoint(exitPt, localMap, vehState, desiredLane, DISTANCE_TO_OBSTACLE);
          localMap->getHeading(heading, exitPt);          

          // need to update the final condition
          setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondLB(VELOCITY_IDX_C, 0);
          setOCPfinalCondUB(VELOCITY_IDX_C, 0);
          setOCPfinalCondLB(HEADING_IDX_C, heading);
          setOCPfinalCondUB(HEADING_IDX_C, heading);          
        }


      }
      break;
    case ControlStateFactory::PAUSE:
      {
	vel_in = AliceStateHelper::getVelocityMag(vehState);
	vel_out = 0;
	//TODO: Need to make the entry point here the entry waypoint
	entryPt = currFrontPos;
	point2 FC_posLB, FC_posUB;
	getOCPfinalPos(FC_posLB, FC_posUB);
	exitPt = FC_posLB;   
      }
      break;
    case ControlStateFactory::STOPPED:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0;
          vel_out = 0;
        }
        else {
          localMap->getWaypoint(exitPt, exitPointID);
          entryPt = currFrontPos;
          vel_in = 0;
          vel_out = 0;
        }

      }
      break;

    case ControlStateFactory::CREEP:
      {
        if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
          SegGoals nextGoal = planHoriz.getSegGoal(1);
          PointLabel nextExitPointID(nextGoal.exitSegmentID, nextGoal.exitLaneID, nextGoal.exitLaneID);
          localMap->getWaypoint(entryPt, entryPointID);
          localMap->getWaypoint(exitPt, nextExitPointID);
          vel_in = 0;
          vel_out = 0;
        }
        else {
          // Nominal driving
          localMap->getWaypoint(exitPt, exitPointID);
          entryPt = currFrontPos;
          vel_in = 1;
          vel_out = 1;
        }

        if (obsPresent){
          // calculate a vel profile based on desired decceleration
          // TODO: set global variable for some of these things, like the stopping 20m vefore the obstacle, stopping 1m in front of the line, desired deceleration, etc
          if (dist2Obs<DISTANCE_TO_OBSTACLE)
            vel_in = 0; //stop now!

          vel_out = 0;
          
          // plan to 20 m before the obstacle
          // SVEN: exitPt = localMap->getObstaclePoint(currFrontPos,DISTANCE_TO_OBSTACLE);
          TrafficUtils::getNearestObsPoint(exitPt, localMap, vehState, desiredLane, DISTANCE_TO_OBSTACLE);
          localMap->getHeading(heading, exitPt);          
          // need to update the final condition
          setOCPfinalCondLB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondLB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondUB(EASTING_IDX_C, exitPt.y);
          setOCPfinalCondUB(NORTHING_IDX_C, exitPt.x);
          setOCPfinalCondLB(VELOCITY_IDX_C, 0);
          setOCPfinalCondUB(VELOCITY_IDX_C, 0);
          setOCPfinalCondLB(HEADING_IDX_C, heading);
          setOCPfinalCondUB(HEADING_IDX_C, heading);          
        }


      }
      break;

    case ControlStateFactory::UTURN:
      {
        // This velocity profile is set from the control state and should not be
        // called here too.
      }
      break;


    default:
      
      cerr << "Corridor::setVelProfile: Undefined control state" << endl;        
    }

  if (currTrafState->getType()==TrafficStateFactory::INTERSECTION_STOP) {
    setSpeedProfileDist(entryPt, vel_in, vel_out, exitPt, false);
  } else {
    setSpeedProfileDist(entryPt, vel_in, vel_out, exitPt, true);
  }
  
  return;
}



 
//void Corridor::updateDistFromConStateBegin(point2 alicePrevPos)
//{
//  point2 aliceCurrPos = AliceStateHelper::getPositionFrontBumber();
//  m_distFromConStateBegin = m_distFromConStateBegin + 
//    sqrt(pow(aliceCurrPos.x - alicePrevPos.x,2) + pow(aliceCurrPos.y - alicePrevPos.y,2));
//}

void Corridor::initializeOCPparams(VehicleState vehState, double vmin, double vmax)
{
  m_ocpParams = OCPparams();
  
  // set min and max speed to curr segment min/max speed (from mdf)
  m_ocpParams.parameters[VMIN_IDX_P] = vmin;
  m_ocpParams.parameters[VMAX_IDX_P] = vmax;


  // populate the initial conditions - lower bound
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).y;
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).x;
  m_ocpParams.initialConditionLB[VELOCITY_IDX_C] = AliceStateHelper::getVelocityMag(vehState);
  m_ocpParams.initialConditionLB[HEADING_IDX_C] = vehState.utmYaw;
  m_ocpParams.initialConditionLB[ACCELERATION_IDX_C] = AliceStateHelper::getAccelerationMag(vehState);
  // TODO: get the current steering angle
  m_ocpParams.initialConditionLB[STEERING_IDX_C] = 0;

  // populate the initial conditions - upper bound
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).y;
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).x;
  m_ocpParams.initialConditionUB[VELOCITY_IDX_C] = AliceStateHelper::getVelocityMag(vehState);
  m_ocpParams.initialConditionUB[HEADING_IDX_C] = vehState.utmYaw;
  m_ocpParams.initialConditionUB[ACCELERATION_IDX_C] = AliceStateHelper::getAccelerationMag(vehState);
  // TODO: get the current steering angle
  m_ocpParams.initialConditionUB[STEERING_IDX_C] = 0;

  // initialize to fwd mode by default
  m_ocpParams.mode = (int)md_FWD;
}

void Corridor::getOCPinitialPos(point2 &IC_posLB, point2 &IC_posUB)
{
  IC_posLB.x = m_ocpParams.initialConditionLB[NORTHING_IDX_C];
  IC_posLB.y = m_ocpParams.initialConditionLB[EASTING_IDX_C];
  IC_posUB.x = m_ocpParams.initialConditionUB[NORTHING_IDX_C];
  IC_posUB.y = m_ocpParams.initialConditionUB[EASTING_IDX_C];
  return;
}

void Corridor::getOCPfinalPos(point2 &FC_posLB, point2 &FC_posUB)
{
  FC_posLB.x = m_ocpParams.finalConditionLB[NORTHING_IDX_C];
  FC_posLB.y = m_ocpParams.finalConditionLB[EASTING_IDX_C];
  FC_posUB.x = m_ocpParams.finalConditionUB[NORTHING_IDX_C];
  FC_posUB.y = m_ocpParams.finalConditionUB[EASTING_IDX_C];
  return;
}

void Corridor::convertOCPtoGlobal(point2 gloToLocalDelta)
{
  point2 tmpPt, gloTmpPt;
  tmpPt.x = m_ocpParams.initialConditionLB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.initialConditionLB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.initialConditionUB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.initialConditionUB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.finalConditionLB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.finalConditionLB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.finalConditionUB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.finalConditionUB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = gloTmpPt.y;

  if ((m_verbose) || (m_debug)){ 
    for (int ii=0; ii<6 ; ii++) {
      cout << "ICLB[" << ii << "] = " << m_ocpParams.initialConditionLB[ii] << endl;
      cout << "FCLB[" << ii << "] = " << m_ocpParams.finalConditionLB[ii] << " and FCUB[" << ii << "] = " << m_ocpParams.finalConditionUB[ii] << endl;
    }
  }
}

void Corridor::setOCPmode(int mode)
{
  m_ocpParams.mode = mode;
}

void Corridor::setOCPfinalCondLB(int index, double cond)
{
  m_ocpParams.finalConditionLB[index] = cond;
}

void Corridor::adjustFCPosForRearAxle()
{
  // TODO: will need to fix this heading if I decide to specify a non-symm heading range
  double length = -DIST_REAR_AXLE_TO_FRONT;
  double heading = m_ocpParams.finalConditionUB[HEADING_IDX_C];
  double easting = length*sin(heading);
  double northing = length*cos(heading);

  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = m_ocpParams.finalConditionUB[NORTHING_IDX_C] + northing ;
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = m_ocpParams.finalConditionUB[EASTING_IDX_C] + easting ;
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = m_ocpParams.finalConditionLB[NORTHING_IDX_C] + northing ;
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = m_ocpParams.finalConditionLB[EASTING_IDX_C] + easting ;
  return;

}

void Corridor::setOCPfinalCondUB(int index, double cond)
{
  m_ocpParams.finalConditionUB[index] = cond;
}


void Corridor::setOCPinitialCondLB(int index, double cond)
{
  m_ocpParams.initialConditionLB[index] = cond;
}


void Corridor::setOCPinitialCondUB(int index, double cond)
{
  m_ocpParams.initialConditionUB[index] = cond;
}

void Corridor::printICFC()
{
    for (int ii=0; ii<6 ; ii++) {
      cout << "ICLB[" << ii << "] = " << m_ocpParams.initialConditionLB[ii] << " and ICUB[" << ii << "] = " << m_ocpParams.initialConditionUB[ii] << endl;
      cout << "FCLB[" << ii << "] = " << m_ocpParams.finalConditionLB[ii] << " and FCUB[" << ii << "] = " << m_ocpParams.finalConditionUB[ii] << endl;
    }
    return;
}

void Corridor::printPolylineCorr()
{
  if (m_polylines.size()>0) {
    cout << "leftBound = " << m_polylines[0] << endl;
    cout << "rightBound = " << m_polylines[1] << endl;
  } else {
    cout << "No polyline corridor to print" << endl;
  }
  return;
}


void Corridor::printVelProfile()
{
  cout << "Velocity profile = " << m_speedProfile << endl;
}


OCPparams Corridor::getOCPparams()
{
  return m_ocpParams;
}


RDDF* Corridor::convertToRddf(point2 gloToLocalDelta)
{
  // For this I am assuming that the corridor boundaries are specified in ordered pairs
  //  cout << "in new convertToRddf()" << endl;
  RDDFData rddfData;
  RDDF* rddf = new RDDF();

  if (m_polylines.size() < 2) return rddf;

  point2arr leftBound(m_polylines[0]);
  point2arr rightBound(m_polylines[1]);
  point2 tmpPt, tmpPt_glo;
  double radius;

  if (leftBound.size()==1){
    cerr << "ERROR convRDDF: only one data point on boundaries" << endl;
    return rddf;
  }

  int rddfNum = 1;
  for (int ii=0; ii<(int)leftBound.size(); ii++) {
    tmpPt = (leftBound[ii]+rightBound[ii])/2;
    radius = tmpPt.dist(leftBound[ii]);
    
    tmpPt_glo = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
    rddfData.number = rddfNum;
    if (m_use_local){
      rddfData.Northing = tmpPt.x;
      rddfData.Easting = tmpPt.y;
    } else {
      rddfData.Northing = tmpPt_glo.x;
      rddfData.Easting = tmpPt_glo.y;
    }

    rddfData.maxSpeed = m_speedProfile[ii];
    rddfData.radius = radius;
    rddf->addDataPoint(rddfData);
    rddfNum++;
  }
  if ((m_verbose) || (m_debug)){
    cout<<"CORRIDOR.convertRddfCorridor: OK"<<endl;
  }

	return rddf;
}


RDDF* Corridor::getRddfCorridor(point2 gloToLocalDelta)
{

	return convertToRddf(gloToLocalDelta);

}
void Corridor::paintLaneCostCenterLine(CMapPlus* map, int costLayer, int tempLayer, 
				       point2arr& leftbound, point2arr &rightbound)
{
  //for now, we just assume a straight corridor and xval refers to the center line x-value

  /*  
      point2arr ptarr = el->geometry;
  
      int ptarrsize= ptarr.size();
      point2 pt = ptarr[0];

      double x = pt.x;
  */

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // assume a known lane-width of 3m on each side
  // THIS SHOULD BE MOVED
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  double laneWidth=10;

  int n_laneWidth;
  int n_laneHalf;
  int n_left, n_right;

  int num_map_rows;
  int num_map_cols;

  double cost;
  double colRes, rowRes;
  int rowWinCenter; 
  int col_Win_temp;
  int check_inside;

  colRes = map->getResCols();
  rowRes = map->getResRows();
     
  num_map_rows = map->getNumRows();
  num_map_cols = map->getNumCols();

  //n_laneWidth =(int)(ceil(laneWidth/rowRes));
  //n_laneHalf = (int)(floor(n_laneWidth/2));

  //printf("n_right: %d  n_left: %d\n", n_right, n_left);
  
  //r is row
  //c is column
  point2 lproj,rproj;
  point2 ldpt, rdpt;
  point2 pt;
 
  double lcost;
  double rcost;
  double x, y;
  int val;
  int lside, rside;
  int lindex, rindex;
  int baseval = 100;
  double dval = 20;
  cout << "TRYING TO PAINT THE CORRIDOR-------------------------------" <<endl;


  
  // cout << "leftbound post offset= "  << leftbound << endl << endl;
  // cout << "rightbound post offset= "  << rightbound << endl << endl;
  



  for(int c=0; c<num_map_cols; c++)
    {
      for(int r=0; r<num_map_rows; r++)
        {
          val = map->Win2UTM(r,c,&x,&y);
          //          if ((c<10)&&(r<10)){
          //  map->setDataWin<double>(tempLayer,r,c,54.3);
            //    cout << "c=" <<c << " r=" <<r <<endl;
            //   cout << "x=" <<x << " y=" <<y <<endl;

          //}


          
          pt.x =x;
          pt.y =y;

          leftbound.get_side(pt,lside,lindex);
          rightbound.get_side(pt,rside,rindex);

 
          if (rside!=lside){
            if (lindex==0 && rindex==0){
              //              cout << "plootting " << endl;
              //             if (lindex>0 &&rindex>0 &&lindex<(double)(leftbound.size()-1) 
              //             && rindex< (double) (rightbound.size()-1)){
              lproj = leftbound.project(pt);
              rproj = rightbound.project(pt);
              ldpt = lproj-pt;
              rdpt = rproj-pt;
              lcost = dval/(ldpt.norm()+dval/baseval);
              rcost = dval/(rdpt.norm()+dval/baseval);
              
              if (lcost>rcost)
                map->setDataWin<double>(tempLayer,r,c,lcost);
              else
                map->setDataWin<double>(tempLayer,r,c,rcost);
            }
            
          }
        }
    }
}


void Corridor::convertMapElementToCost(CMapPlus* map, int costLayer, int tempLayer, MapElement& el)
{
  double x,y,radius; 
  double left_bound, right_bound; 
  double top_bound, bottom_bound; 
  double row_res, col_res; 
  int num_cell_cols; 
  int num_cell_rows; 

  double cost; 
  double offset;
  int step_size;
  double COST_DELTA = 1;
  double base_value = 200;

  double old_value;

  //here we update costs on the tempLayer
  if(el.height>0)
    {       
      x = el.center.x; 
      y = el.center.y; 
      radius = el.length;       
      
      row_res = map->getResRows();
      col_res = map->getResCols();     
      
      step_size = (int)floor(radius/row_res);

      for(int k=0; k<step_size; k++)
        {
          offset = radius/step_size;

          radius = radius - k*offset;

          left_bound = x-radius; 
          right_bound = x+radius;
	  
          bottom_bound = y-radius;   
          top_bound = y+radius; 
	  
          num_cell_cols = (int)(ceil( (right_bound-left_bound)/col_res ));
          num_cell_rows = (int)(ceil( (top_bound-bottom_bound)/row_res ));
	  	  
          for(int j=0; j<num_cell_rows; j++)
            {
              for(int i=0; i<num_cell_cols; i++)
                {
                  old_value = map->getDataUTM<double>(tempLayer,left_bound+i*col_res,bottom_bound+j*row_res);
                  if(old_value>0)
                    {
                      cost = old_value + COST_DELTA;
		      
                      if(k==0)
                        {
                          cost = cost + base_value;
                        }
		      
                      map->setDataUTM<double>(tempLayer,left_bound+i*col_res,bottom_bound+j*row_res,cost);
                    }
                }
            }
	  
        }
    }

}

void Corridor::getCostMap(CMapPlus* map, int costLayer, int tempLayer, VehicleState &state, Map* lmap)
{
  //=============================
  //tempLayer vs costLayer
  // - templayer is a layer in the map that is continually filled with new objects from sam's mapper
  //   so more deltas get registered than there should be
  // - costlayer is the map that's actually sent and should have the correct number of deltas 
  //   (more efficient, basically)
  //=============================


  //=============================
  //UPDATE VEHICLE LOCATION FIRST
  //=============================
  map->updateVehicleLoc(state.localX, state.localY);
  
  //==========================================
  //THIS IS WHERE YOU FILL THE MAP WITH VALUES
  //==========================================
  //--eventually this will come from mapper


  /*
    for(int i=200; i<250; i++)
    {
    for(int j =200; j<250; j++)
    {
	  map->setDataWin_Delta<double>(costLayer,i,j,20);
    }
    }
  */  
  
  map->clearLayer(tempLayer);
  
  //NOEL: I took out this code to specify the boundaries determined earlier

  //  MapElement leftbound;
  // MapElement rightbound;
  // LaneLabel llabel;
  //piont2 statept(state.localX,state.localY);

  //llabel.segment = lmap->getSegmentID(statept);
  // llabel.lane = lmap->getLaneID(statept);
  
  // point2arr leftptarr;
  //leftptarr =rightbound.geometry;
  //point2arr rightptarr;
  //rightptarr = leftbound.geometry;;
  //************************************************************
  point2arr leftptarr, rightptarr;
  leftptarr.set(m_polylines[0]);
  rightptarr.set(m_polylines[1]);
  //  cout << "leftptarr = " << leftptarr << endl;
  // cout << "rightptarr = " << rightptarr << endl;

  //int val = lmap->getBounds(leftptarr,rightptarr,llabel,statept,60);
  //cout << "############Left label = " << llabel << endl;
  double yaw = state.localYaw;

  if ((int)leftptarr.size()<2){
    //  paintLaneCostCenterLine(map, costLayer, tempLayer,yaw);
    cerr << "Only 1 pt on the boundaries: could not paint the corridor." << endl;
    cerr << "Only 1 pt on the boundaries: could not paint the corridor." << endl;
  } else {  
    cout << "painting corridor" << endl;

    //--------------------------------------------------
    // paint the corridor cost into the map
    //--------------------------------------------------
    paintLaneCostCenterLine(map, costLayer, tempLayer,rightptarr, leftptarr);
  }


  //--------------------------------------------------
  // draw the obstacles into the map
  //--------------------------------------------------
  for(unsigned int i=0; i<lmap->data.size(); i++)
    { 
      convertMapElementToCost(map, costLayer, tempLayer, lmap->data[i]); 
    }  
 
  //now we look at each cell in the tempLayer and compare it with the costLayer; 
  //if the compared cells differ, then update the costLayer
  int num_map_cols;
  int num_map_rows;
 
  double val_tempLayer;
  double val_costLayer;

  num_map_rows = map->getNumRows();
  num_map_cols = map->getNumCols();
  
  for(int i=0; i<num_map_cols; i++)
    {
      for(int j=0; j<num_map_rows; j++)
        {
          val_tempLayer = map->getDataWin<double>(tempLayer,i,j);
          val_costLayer = map->getDataWin<double>(costLayer,i,j);	  
	  
          if(val_tempLayer!=val_costLayer)
            {
              map->setDataWin_Delta<double>(costLayer,i,j,val_tempLayer);
              //map->setDataWin<double>(costLayer,i,j,val_tempLayer);
            }
          
        }
    }
  
  
}


void Corridor::convertLaneToPolygon(vector<Polygon>& polygons, point2arr& leftbound, 
				   point2arr &rightbound, float bval, float cval)
{
  bval = sqrt(bval);
  cval = sqrt(cval);

  if (leftbound.size() <= 1 || rightbound.size() <= 1) {
    cerr << "ERROR: " <<  __FILE__ << ":" << __LINE__ << " leftbound.size() = " 
	 << leftbound.size() << " rightbound.size() = " << rightbound.size() << endl;
    return;
  }

  unsigned leftboundInd = 0;
  unsigned rightboundInd = 0;
  bool useLeft = true;

  while (leftboundInd < leftbound.size() - 1 || rightboundInd < rightbound.size() - 1) {
    if (leftboundInd >= leftbound.size() || rightboundInd >= rightbound.size() ){
      cerr << "ERROR: " <<  __FILE__ << ":" << __LINE__ << "leftboundInd = " 
	   << leftboundInd << " leftbound.size() = " << leftbound.size()
	   << " rightboundInd = " << rightboundInd << " rightbound.size() = " 
	   << rightbound.size() << endl;
      return;
    }
    // if (m_debug) {
    //  cout << "leftboundInd = " << leftboundInd << " rightboundInd = "
	//   << rightboundInd << endl;
    // }

    vector<float> cost1, cost2;
    point2arr_uncertain vertices1;
    point2arr_uncertain vertices2;

    if (leftboundInd == leftbound.size() - 1)
      useLeft = false;
    else if (rightboundInd == rightbound.size() - 1)
      useLeft = true;
    else {
      float leftDist = leftbound[leftboundInd].dist(leftbound[leftboundInd + 1]) +
	rightbound[rightboundInd].dist(leftbound[leftboundInd + 1]);
      float rightDist = leftbound[leftboundInd].dist(rightbound[rightboundInd + 1]) +
	rightbound[rightboundInd].dist(rightbound[rightboundInd + 1]);
      if (leftDist < rightDist)
	useLeft = true;
      else
	useLeft = false;
    }

    if (useLeft) {
      /*
      vertices.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost.push_back(cval);
      vertices.push_back(leftbound[leftboundInd + 1]);
      cost.push_back(bval);
      if (m_debug) {
	cout << (leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2
	     << ":" << cost[3] 
	     << "\t" << leftbound[leftboundInd + 1] << ":" << cost[4]
	     << endl << endl;
      }
      */
      // Trapezoid
      vertices1.push_back(leftbound[leftboundInd]);
      cost1.push_back(bval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back(leftbound[leftboundInd + 1]);
      cost1.push_back(bval);

      // Triangle
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      vertices2.push_back(rightbound[rightboundInd]);
      cost2.push_back(bval);
      vertices2.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      /*if (m_debug) {
	cout << "poly1: " << vertices1[0] << ":" << cost1[0] << "\t" 
	     << vertices1[1] << ":" << cost1[1] << "\t" 
	     << vertices1[2] << ":" << cost1[2] << "\t"
	     << leftbound[leftboundInd + 1] << ":" << cost1[3] << endl;
	cout << "poly2: " << vertices2[0] << ":" << cost2[0] << "\t"
	     << vertices2[1] << ":" << cost2[1] << "\t" 
	     << vertices2[2] << ":" << cost2[2] << endl << endl;
       }*/

      leftboundInd++;
    }
    else {
      /*
      vertices.push_back(rightbound[rightboundInd + 1]);
      cost.push_back(bval);
      vertices.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost.push_back(cval);
      if (m_debug) {
	cout << rightbound[rightboundInd + 1] << ":" << cost[3] << "\t"
	     << (leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2
	     << ":" << cost[4] << endl << endl;
      }
      */

      // Triangle
      vertices1.push_back(leftbound[leftboundInd]);
      cost1.push_back(bval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost1.push_back(cval);

      // Trapezoid
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      vertices2.push_back(rightbound[rightboundInd]);
      cost2.push_back(bval);
      vertices2.push_back(rightbound[rightboundInd + 1]);
      cost2.push_back(bval);
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost2.push_back(cval);
      /*if (m_debug) {
      	cout << "poly1: " << vertices1[0] << ":" << cost1[0] << "\t" 
      	     << vertices1[1] << ":" << cost1[1] << "\t" 
             << vertices1[2] << ":" << cost1[2] << "\t"
             << leftbound[leftboundInd + 1] << ":" << cost1[3] << endl;
        cout << "poly2: " << vertices2[0] << ":" << cost2[0] << "\t"
             << vertices2[1] << ":" << cost2[1] << "\t" 
             << vertices2[2] << ":" << cost2[2] << endl << endl;
             }*/
      rightboundInd++;
    }

    Polygon tmpPoly;
    tmpPoly.setVertices(vertices1, cost1);
    tmpPoly.setFillFunc(FILL_SQUARE);
    tmpPoly.setCombFunc(COMB_REPLACE);
    polygons.push_back(tmpPoly);

    tmpPoly.setVertices(vertices2, cost2);
    polygons.push_back(tmpPoly);
  }
}


void Corridor::convertMapElementToPolygon(vector<Polygon>& polygons, MapElement& el,
					  float obsCost)
{
  if (el.type != ELEMENT_OBSTACLE && el.type != ELEMENT_VEHICLE) {
    cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle nonobstacle "
	 << el.type << " of map element" << endl;
    return; 
  }
  if (el.geometryType != GEOMETRY_POLY) {
    cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle geometry type "
	 << el.geometryType << " of map element" << endl;
    return;
  }
  Polygon tmpPoly;
  point2arr_uncertain vertices(el.geometry);
  vector<float> cost(el.geometry.size(), obsCost);
  if (m_debug) {
    cout << "Obstacle at " << el.geometry << endl;
    cout << "  height = " << endl;
  }
  tmpPoly.setVertices(el.geometry, cost);
  tmpPoly.setCombFunc(COMB_REPLACE);
  polygons.push_back(tmpPoly);
}



void Corridor::getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
			       VehicleState &state, Map* lmap, point2 gloToLocalDelta)
{
  // Lane boundaries
  if (m_polylines.size() != 2) {
    cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "m_polylines.size() = " 
	 << m_polylines.size() << endl;
    return;
  }

  if (m_use_local) {
    bmparams.centerX = state.localX;
    bmparams.centerY = state.localY;
  }
  else {
    bmparams.centerX = state.utmNorthing;
    bmparams.centerY = state.utmEasting;
  }

  bmparams.polygons.clear();

  point2arr leftptarr, rightptarr;
  leftptarr.set(m_polylines[0]);
  rightptarr.set(m_polylines[1]);
  /*
  if (m_debug) {
    cout << "leftptarr = " << leftptarr << endl;
    cout << "rightptarr = " << rightptarr << endl;
  }
  */

  if (leftptarr.size() < 2 || rightptarr.size() < 2){
    cerr << "ERROR: " << __FILE__ << ":" << __LINE__ 
	 << "Only 1 pt on the boundaries: could not paint the corridor." << endl;
  } 
  else {
    if (m_debug) {
      cout << "painting lane" << endl;
    }

    //--------------------------------------------------
    // paint the corridor cost into the map
    //--------------------------------------------------
    if (m_use_local) {
      convertLaneToPolygon(bmparams.polygons, leftptarr, rightptarr, 
			   bmparams.baseVal, polygonParams.centerlaneVal);
    }
    else {
      point2arr leftptarrGlo, rightptarrGlo;
      for (unsigned i = 0; i < leftptarr.size(); i++) {
	leftptarrGlo.push_back(AliceStateHelper::convertToGlobal(gloToLocalDelta, leftptarr[i]));
      }
      for (unsigned i = 0; i < rightptarr.size(); i++) {
	rightptarrGlo.push_back(AliceStateHelper::convertToGlobal(gloToLocalDelta, rightptarr[i]));
      }
      convertLaneToPolygon(bmparams.polygons, leftptarrGlo, rightptarrGlo, 
			   bmparams.baseVal, polygonParams.centerlaneVal);
    }
  }
  
  if (m_debug)
    cout << "Drawing " << lmap->data.size() << " obstacles" << endl;
  //--------------------------------------------------
  // draw the obstacles into the map
  //--------------------------------------------------
  for(unsigned int i=0; i<lmap->data.size(); i++) { 
    if ((lmap->data[i]).type == ELEMENT_OBSTACLE ||
	(lmap->data[i]).type == ELEMENT_VEHICLE ) {
      if ((lmap->data[i]).frameType != FRAME_LOCAL && (lmap->data[i]).frameType != FRAME_UNDEF) {
	cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle frame "
	     << (lmap->data[i]).frameType << " of map element" << endl;
	continue;
      }
      if ((lmap->data[i]).frameType == FRAME_UNDEF) {
	cerr << "Warning: convertMapElementToPolygon: frameType = FRAME_UNDEF" << endl;
      }
      if (m_use_local) {
	convertMapElementToPolygon(bmparams.polygons, lmap->data[i], 
				   polygonParams.obsCost); 
      }
      else {
	MapElement data = lmap->data[i];
	for (unsigned j = 0; j < data.geometry.size(); j++) {
	  data.geometry[j] = AliceStateHelper::convertToGlobal(gloToLocalDelta, data.geometry[j]);
	}
	convertMapElementToPolygon(bmparams.polygons, data, 
				   polygonParams.obsCost); 
      }
    }
    else if (m_debug) {
      cout << "Get map element of type " << (lmap->data[i]).type << endl;
    }
  }

  if (m_debug)
    cout << "Finish getBitmapParams" << endl;
}

// useful for debugging
void printCPolytope(const CPolytope& pt)
{
  cout << (&pt) << endl;
}

/*!Function that generates the polyhedral corridor
 * If you think this is a mess:
 * "Welcome to Computational Geometry in pure C++"
 *
 * S Di Cairano Apr-07
 */


void Corridor::generatePolyCorridor()
{  
    bool isLegal = true;
	
  //You should always have right and left boundary
    if (m_polylines.size() > 1 ) 
	{ 
        int Ngates=m_polylines[0].size();
        CPolytope** rawPoly;
        rawPoly=new CPolytope*[Ngates-1];
        point2arr leftBound(m_polylines[0]);
        point2arr rightBound(m_polylines[1]);
        if ((leftBound.size() > 1) && (rightBound.size() > 1)) 
		{
            if (0 != m_polyCorridor) 
			{
	            delete[] m_polyCorridor;  //clearing the previous corridor
            }

	        m_nPolytopes = Ngates-1;
	        m_polyCorridor = new CPolytope [m_nPolytopes]; //creating the empty corridor
	        int NofDim = 2;
	        CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
	        int NofVtx = 4;
	        double*** vertices;
	        vertices = new double**[m_nPolytopes];
	        for(int k=0; k<Ngates-1;k++)
	        {
	            vertices[k] = new double*[NofDim];
	            for(int i=0;i<NofDim;i++)
	            {
	                vertices[k][i] = new double[NofVtx];
	            }
	        }
			int i = 0 ;
			while( i<m_nPolytopes) 
            {
                vertices[i][0][1]=leftBound[i].x;
                vertices[i][1][1]=leftBound[i].y;
                vertices[i][0][2]=leftBound[i+1].x;
                vertices[i][1][2]=leftBound[i+1].y;
                vertices[i][0][3]=rightBound[i+1].x;
                vertices[i][1][3]=rightBound[i+1].y;
                vertices[i][0][0]=rightBound[i].x;
                vertices[i][1][0]=rightBound[i].y;
                rawPoly[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
      
                ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(rawPoly[i]);
                m_polyCorridor[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
                algErr = algGeom->VertexAndFacetEnumeration(&m_polyCorridor[i]);
                
				if( algErr != alg_NoError)
				{
				    cout << "error when building raw polytopes" << endl;
				    isLegal = false;
                }
                int nCompVtx = rawPoly[i]->getNofVertices() ;
                if( nCompVtx != 4) 
	            {
                    cerr << "The gates: " << i << "," << i+1 << " are degenerate " << endl;
                    cout << i <<  "L: ("<<leftBound[i].x<< ","<< leftBound[i].y << "), " <<  "R: ("<<rightBound[i].x<< ","<< rightBound[i].y << "), " <<  endl;
                    cout << i+1 <<  "L: ("<<leftBound[i+1].x<< ","<< leftBound[i+1].y << "), " <<  "R: ("<<rightBound[i+1].x<< ","<< rightBound[i+1].y << "), " <<  endl;
					cout << rawPoly[i] << endl;
//					cout << " cutting at polytope "<< i << endl;
//					m_nPolytopes = i;
//					delete rawPoly[i];
//					if(i<1)
//					  isLegal = false;
                }
                i++;
	        }
/*
            i = 0 ; 
	        while( (i<m_nPolytopes) && (isLegal) )
	        {  
			    ALGErrorType algErrIntNext = alg_NoError ;
			    ALGErrorType algErrIntPrev = alg_NoError ;
	            double** verticesFinal;
	            int NofVtxFinal = rawPoly[i]->getNofPoints();
	            double** verticesIntPrev;
	            int NofVtxIntPrev = 0;
	            double** verticesIntNext;
	            int NofVtxIntNext = 0;
	    
	            if(i>0)//expand backwards
		        {
		            int Nrows=rawPoly[i]->getNofRows(); 
		            int Ncols=rawPoly[i]->getNofColumns();
		            double** Amat;
		            double* bvect;
		            bvect = new double[Nrows];
		            Amat= new double*[Nrows];
		            for(int j=0;j<Nrows;j++)
		                Amat[j] = new double[Ncols];
        
		            rawPoly[i]->getA(&Nrows,&Ncols,Amat);
		            rawPoly[i]->getb(&Nrows,bvect);
*/
/*  search for the row that defines the border to be exapnaded. 
  *  It is such that Ai*x1=bi, Ai*x2=bi, hence Ai*(x1+x2)-2bi=0, which is  argmax_i Ai(x1+x2)-2*bi		                  
 */
/*
                    double myVect[NofDim];
                    myVect[0] = leftBound[i].x+rightBound[i].x;
                    myVect[1] = leftBound[i].y+rightBound[i].y;
			        double expNorm = 0.5*sqrt(pow((leftBound[i].x-leftBound[i-1].x),2)+pow((leftBound[i].y-leftBound[i-1].y),2))+
			                                  0.5* sqrt(pow((rightBound[i].x-rightBound[i-1].x),2)+pow((rightBound[i].y-rightBound[i-1].y),2));
			        int expIdx=0;
			        double myMax=-1;
			        for(int j=0;j<Nrows;j++)
			        {
			             double temp = 0;
			             for(int h=0;h<Ncols;h++)
			                  temp+=Amat[j][h]*myVect[h];
				          temp-=2*bvect[j];
                          if(temp>=myMax)
			              {
			                  myMax=temp;
				              expIdx=j;      //index of the row to be expanded
                          }
		            }
			        double normARow=0;
			        for(int j=0;j<Ncols;j++) //compute the norm of the row to be expanded to normalize
			        {
                        normARow+=Amat[expIdx][j]*Amat[expIdx][j];				
                    }
			        bvect[expIdx]+=(EXPPERC*expNorm)*normARow;    // expansion
                    int NrowsPrev=rawPoly[i-1]->getNofRows(); 
                    int NcolsPrev=rawPoly[i-1]->getNofColumns();
		            double** AmatPrev;
		            double* bvectPrev;
		            bvectPrev = new double[NrowsPrev];
		            AmatPrev = new double*[NrowsPrev];
		            for(int j=0;j<NrowsPrev;j++)
		                AmatPrev[j] = new double[NcolsPrev];
		            rawPoly[i-1]->getA(&NrowsPrev,&NcolsPrev,AmatPrev);
		            rawPoly[i-1]->getb(&NrowsPrev,bvectPrev);
			        double** Aint;
                    double* bint;
		            bint = new double[Nrows+NrowsPrev];
	            	Aint = new double*[Nrows+NrowsPrev];
                    for(int j=0;j<Nrows;j++)
			        {
			            Aint[j] = Amat[j];   //I use the memory which has been already allocated
			            bint[j] = bvect[j];
                    }
                    for(int j=Nrows;j<(NrowsPrev+Nrows);j++)
			        {
			            Aint[j] = AmatPrev[j-Nrows];   //I use the memory which has been already allocated
			            bint[j] = bvectPrev[j-Nrows];
                    }
	            	int NrowsIntPrev =Nrows+NrowsPrev;
		            int NcolsIntPrev =Ncols;
			
		            NofVtxIntPrev = 4;
	                verticesIntPrev = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                    verticesIntPrev[j] = new double[NofVtxIntPrev];

                    algGeom->VertexEnumeration(&NrowsIntPrev,&NcolsIntPrev,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntPrev, verticesIntPrev);
			        for(int j=0;j<NofDim;j++)
                       delete[] verticesIntPrev[j];
	                delete[] verticesIntPrev;

	                verticesIntPrev = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                   verticesIntPrev[j] = new double[NofVtxIntPrev];
		
                    algErrIntPrev = algGeom->VertexEnumeration(&NrowsIntPrev,&NcolsIntPrev,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntPrev, verticesIntPrev);
	       
					if(algErrIntPrev != alg_NoError)
					    cout << " Error while enumerating vertices in the BWD intersection " << endl; 
           
					delete[] bint;       //this uses memory allocated by others. Hence I deallocate it using others
                    delete[] bvectPrev;
                    delete[] bvect;
                    delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
	            	for(int j=0;j<Nrows;j++)
			           delete[] Amat[j];
                    delete[] Amat;
                       for(int j=0;j<NrowsPrev;j++)
                           delete[] AmatPrev[j];
                       delete[] AmatPrev;
	            } //end if (not the first)
	 
	            if(i<m_nPolytopes-2)//expand forward
		        {
                    int Nrows=rawPoly[i]->getNofRows(); 
                    int Ncols=rawPoly[i]->getNofColumns();
		            double** Amat;
		            double* bvect;
		            bvect = new double[Nrows];
		            Amat= new double*[Nrows];
		            for(int j=0;j<Nrows;j++)
		                Amat[j] = new double[Ncols];
        
		            rawPoly[i]->getA(&Nrows,&Ncols,Amat);
		            rawPoly[i]->getb(&Nrows,bvect);
*/
/*  search for the row that defines the border to be exapnaded. 
  *  It is such that Ai*x1=bi, Ai*x2=bi, hence Ai*(x1+x2)-2bi=0, which is  argmax_i Ai(x1+x2)-2*bi		                  
 */
/*                    double myVect[NofDim];
                    myVect[0] = leftBound[i+1].x+rightBound[i+1].x;
                    myVect[1] = leftBound[i+1].y+rightBound[i+1].y;
		            int expIdx=0;
		            double myMax=-1;
			        double expNorm = 0.5*sqrt(pow((leftBound[i].x-leftBound[i+1].x),2)+pow((leftBound[i].y-leftBound[i+1].y),2))+
			                                0.5* sqrt(pow((rightBound[i].x-rightBound[i+1].x),2)+pow((rightBound[i].y-rightBound[i+1].y),2));
			        for(int j=0;j<Nrows;j++)
	                {            
			            double temp = 0;
			            for(int h=0;h<Ncols;h++)
			                temp+=Amat[j][h]*myVect[h];
	            		temp-=2*bvect[j]; 
                        if(temp>=myMax)
			            {
			                myMax=temp;
				            expIdx=j;      //index of the row to be expanded
                        }
		            }
		            double normARow=0;
	            	for(int j=0;j<Ncols;j++) //compute the norm of the row to be expanded to normalize
	                {
                        normARow+=Amat[expIdx][j]*Amat[expIdx][j];				

			        }

		            bvect[expIdx]+=(EXPPERC*expNorm)*normARow;    // expansion
                    int NrowsNext=rawPoly[i+1]->getNofRows(); 
                    int NcolsNext=rawPoly[i+1]->getNofColumns();
		            double** AmatNext;
		            double* bvectNext;
		            bvectNext = new double[NrowsNext];
		            AmatNext= new double*[NrowsNext];
		            for(int j=0;j<NrowsNext;j++)
		                AmatNext[j] = new double[NcolsNext];
		            rawPoly[i+1]->getA(&NrowsNext,&NcolsNext,AmatNext);
		            rawPoly[i+1]->getb(&NrowsNext,bvectNext);
            
			        double** Aint;
                    double* bint;
			        bint = new double[Nrows+NrowsNext];
		            Aint = new double*[Nrows+NrowsNext];
                    for(int j=0;j<Nrows;j++)
			        {
			            Aint[j] = Amat[j];   //I use the memory which has been already allocated
			            bint[j] = bvect[j];
                    }
                    for(int j=Nrows;j<(NrowsNext+Nrows);j++)
			        {
			             Aint[j] = AmatNext[j-Nrows];   //I reuse the memory which has been already allocated
			             bint[j] = bvectNext[j-Nrows];
                    }
			        int NrowsIntNext =Nrows+NrowsNext;
		            int NcolsIntNext=Ncols;
			
			        NofVtxIntNext= 4;
	                verticesIntNext = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                    verticesIntNext[j] = new double[NofVtxIntNext];
                    algGeom->VertexEnumeration(&NrowsIntNext,&NcolsIntNext,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntNext, verticesIntNext);
		            for(int j=0;j<NofDim;j++)
                        delete[] verticesIntNext[j];
	                delete[] verticesIntNext;

	                verticesIntNext = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                    verticesIntNext[j] = new double[NofVtxIntNext];
		
                    algErrIntNext = algGeom->VertexEnumeration(&NrowsIntNext,&NcolsIntNext,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntNext, verticesIntNext);
					if(algErrIntNext != alg_NoError)
					    cout << " Error while enumerating vertices in the FWD intersection " << endl; 
	       
                    delete[] bint;     //this uses memory allocated by others. Hence I deallocate it using others
                    delete[] bvectNext;
                    delete[] bvect;
                    delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
		            for(int j=0;j<Nrows;j++)
			            delete[] Amat[j];
                    delete[] Amat;
                    for(int j=0;j<NrowsNext;j++)
                        delete[] AmatNext[j];
                    delete[] AmatNext;

	            } //end if (not the last)

        //building the overlapping polytopes
		        if( (algErrIntNext == alg_NoError) && (algErrIntPrev == alg_NoError))
                {
                    if(i>0)
		                NofVtxFinal+=NofVtxIntPrev;
	                if(i<m_nPolytopes-1)
		                NofVtxFinal+=NofVtxIntNext;
                    verticesFinal = new double*[NofDim];
		            for(int h=0;h<NofDim;h++)
                        verticesFinal[h] = new double[NofVtxFinal];
	                for(int j=0;j<NofVtxFinal;j++)
		            {
                        verticesFinal[0][j]=0.0;
                        verticesFinal[1][j]=0.0;
	                }
	                for(int j=0;j<NofVtx;j++)
	                {
                        verticesFinal[0][j]=vertices[i][0][j];
                        verticesFinal[1][j]=vertices[i][1][j];
		            }
	                if(i>0)
                    {          
		                for(int j=0;j<NofVtxIntPrev;j++)
		                {
                            verticesFinal[0][j+NofVtx]=verticesIntPrev[0][j];
                            verticesFinal[1][j+NofVtx]=verticesIntPrev[1][j];
		                }
		            }
		            if(i<m_nPolytopes-1)
                    {          
		                for(int j=0;j<NofVtxIntNext;j++)
		                {
                            verticesFinal[0][j+NofVtx+NofVtxIntPrev]=verticesIntNext[0][j];
                            verticesFinal[1][j+NofVtx+NofVtxIntPrev]=verticesIntNext[1][j];
		                }
		            }
		            m_polyCorridor[i]  =  new CPolytope( &NofDim, &NofVtxFinal, (const double**)  verticesFinal ); //Ask Melvin: is this Correct?
		            ALGErrorType algErrFinal = algGeom->VertexAndFacetEnumeration(&m_polyCorridor[i]);
			    if ((algErrFinal != alg_NoError) && (algErrFinal != alg_ImproperAdjacency) )
			  {
					    
			    cout << "error when building overlapping polytopes" << endl;
			    cout << "alg_ErrFinal " << algErrFinal<<endl;
			    isLegal = false;
                    }
                    for(int j=0;j<NofDim;j++)
                    {  
                        delete[] verticesFinal[j];
                        if(i<m_nPolytopes-2)
			                delete[] verticesIntNext[j];
                        if(i>0)
			                delete[] verticesIntPrev[j];
                    }
			    }

			    i++;
            
			} //end while
*/
            for(int i=0; i<Ngates-1; i++)
	        {
	            for(int j=0; j<NofDim; j++)
	                delete[] vertices[i][j];
	            delete[] vertices[i];
            }
	        delete[] vertices;
            for(int i=0; i<m_nPolytopes; i++)
	        {
	            delete rawPoly[i];
            }
            delete[] rawPoly;
	        delete algGeom;
        } 
        else 
        {
		    isLegal = false;
            cout<<"CORR - FAILED - leftBound.size()< 2,rightBound.size()< 2"<<endl; 
        } 
    }
	else
	{ 
		isLegal = false;
        cout<<"CORR - FAILED - m_polylines.size()< 2"<<endl; 
    }
	if(isLegal)
        m_polyCorridorLegal = true;
    else
	{   
	    cout << "Corridor generation failed " << endl;
	    m_polyCorridorLegal = false;
    }
}

void Corridor::printPolyCorridor(void)
{
    if(!m_polyCorridorLegal)
	   cout << "the current Polytopic Corridor is NOT LEGAL " << endl;
	else
	{
        cout << "Number of Polytopes : " << m_nPolytopes << endl;
        for(int i=0;i<m_nPolytopes;i++)
	       cout << " Polytope " << i << endl << &m_polyCorridor[i] << endl;
    }
}

void Corridor::printMatlabPolyCorridor(void)
{
    if(!m_polyCorridorLegal)
       cout << "the current Polytopic Corridor is NOT LEGAL " << endl;
    else
    {
        for (unsigned int i=0; i<m_nPolytopes;i++) {
            double** vertices ;
            int NofDim = 2 ;
            vertices = new double*[NofDim];
            int NofVtx = m_polyCorridor[i].getNofVertices();
            for(int j = 0; j < NofDim ; j++)
                vertices[j] = new double[NofVtx] ;

            m_polyCorridor[i].getVertices(&NofDim, &NofVtx, vertices);

            for(int j = 0; j < NofDim ; j++) {
                printf("P%d_%c = [ ", i, (j==0)?'x':'y');
                for(int k=0; k < NofVtx; k++) {
                    printf("%f ", vertices[j][k]);
                }
                printf("];\n");
            }

            char color;
            if (i % 3 == 0) color = 'g';
            if (i % 2 == 0) color = 'k';
            else color = 'm';
            printf("plot(P%d_x, P%d_y, '%c--'); hold on;\n\n", i, i, color);
        }
    }
}

/*!Function that sends  the polyhedral corridor
 * to reduce the network load we extract the polytopes vertices and we send those.
 * the resulting message is a vector of vector of 2D points. The external vector represents
 * the list of polytopes, which are lists (the internal vectors) of vertices, which are 2-d arrays of double.
 *
 * S Di Cairano Apr-07
 */
void Corridor::sendPolyCorridor(void)
{
    sendCorr corrVertices;
    sendPoly polyVertices;
//    if(!m_polyCorridorLegal)
//	{
//	   cout << "the current Polytopic Corridor is NOT LEGAL " << endl;
//	   cout << "SendPolyCorridor ABORTED" << endl;
//	}
//	else
//	{
        for(int i=0;i<m_nPolytopes; i++)
	    {
            double** vertices;
		    int NofDim =  m_polyCorridor[i].getNofDimensions();
		    int NofVtx = m_polyCorridor[i].getNofVertices();
		    vertices = new double*[NofDim];
		    for(int j=0;j<NofDim;j++)
              vertices[j]=new double[NofVtx];
            m_polyCorridor[i].getVertices(&NofDim, &NofVtx, vertices);
		
		    for(int j=0;j<NofVtx;j++)
		    {
		        polyVertices.push_back( sendPoint(vertices[0][j],  vertices[1][j]));
            }
	        corrVertices.push_back(polyVertices);
       
            polyVertices.clear(); //clearing the current polytope
		    for(int j=0;j<NofDim;j++)
                delete[] vertices[j]; //clearing the current vertices
	        delete[] vertices;
	    }
        m_polyCorridorTalker.send(&corrVertices);    
//	}
}


void Corridor::printPolylines() {

  if (m_polylines.size()> 0 ) {
    for (int i=0; i<m_polylines.size(); i++) {
      for (int j=0;j<m_polylines[i].size(); j++) {
	cout<<"m_polyline["<<i<<"]["<<j<<"]="<< m_polylines[i].arr[j]<<endl;
      }
    }
  } 
}

void Corridor::printMatlabPolylines() {
    if (m_polylines.size()> 0 ) {
        for (unsigned int i=0; i<m_polylines.size(); i++) {
            printf("L%d_x = [ ", i);
            for (int j=0;j<m_polylines[i].size(); j++) {
                printf("%f ", m_polylines[i].arr[j].x);
            }
            printf(" ];\n");
            printf("L%d_y = [ ", i);
            for (int j=0;j<m_polylines[i].size(); j++) {
                printf("%f ", m_polylines[i].arr[j].y);
            }
            printf(" ];\n");

            char color;
            if (i % 2 == 0) color = 'r';
            else color = 'b';
            printf("plot(L%d_x, L%d_y, '%c*:'); hold on;\n\n", i, i, color);
        }
    } 
}
