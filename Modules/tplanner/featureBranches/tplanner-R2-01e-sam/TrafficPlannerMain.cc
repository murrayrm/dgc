#include <getopt.h>
#include "cmdline.h"
#include <iostream>
#include "TrafficPlanner.hh"
#include "dgcutils/DGCutils.hh"
#include "skynet/skynet.hh"
#include "iostream"
      
using namespace std;             
     
int main(int argc, char **argv)              
{
  gengetopt_args_info cmdline;

  if (cmdline_parser(argc, argv, &cmdline) != 0)
  {
    exit (1);
  }
   
  // Figure out what skynet key to use
  int sn_key = skynet_findkey(argc, argv);
  cout << "Constructing skynet with KEY = " << sn_key << endl;
  
  int debugLevel, verboseLevel;
  debugLevel = cmdline.debug_arg;
  cout  << "debug level = " << debugLevel << endl;

  verboseLevel = cmdline.verbose_arg; 
  cout << "verbose level = " << verboseLevel << endl;

  bool recvLocalMap; 
  if (cmdline.nomap_given)
  {
    recvLocalMap = false; 
    cout << "Not receiving map" << endl; 
  } 
  else 
  {  
    recvLocalMap = true;
  }    
      
  bool recvSegmentGoals;
  if (cmdline.nogoals_given)
  {
    recvSegmentGoals = false;
    cout << "not receiving goals" << endl;
  }
  else
  { 
    recvSegmentGoals = true;
  } 

  // Initialize Traffic Planner Class
  CTrafficPlanner* pTrafficPlanner = new CTrafficPlanner(sn_key, !cmdline.nowait_given,
	recvLocalMap, recvSegmentGoals, debugLevel, verboseLevel);

  // Initialize the map with rndf if given
  if (cmdline.rndf_given){
    string RNDFfilename = cmdline.rndf_arg;
    cout << "RNDF Filename in = "  << RNDFfilename << endl;
    if (!pTrafficPlanner->loadRNDF(RNDFfilename)){
      return 0; 
    }  
		 
  }     

  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getMapDeltasThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getLocalMapThread);
  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getDPlannerStatusThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getSegGoalsThread); 
  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getStaticCostMapRequestThread);
      
  
  // I have had issues where some of the threads are not up yet by the time the planning loop starts,
  // and I do not know why that is, but I have added a sleep here to make sure that all the threads 
  // are running. Want something more robust here?!
  //#warning "make threads more independent during startup"
  sleep(1);
  pTrafficPlanner->TPlanningLoop();
  return 0;
}

