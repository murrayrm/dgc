#include <getopt.h>

#include <iostream>
#include "TrafficPlanner.hh"
#include "dgcutils/DGCutils.hh"
using namespace std;

int NOWAIT               = 0;       // by the default, wait for state to fill
char* RNDFFileName = "../../nok/missionPlanner/DARPA_RNDF.txt"; // name of RNDF file

/* The name of this program. */
const char * program_name;

/* Prints usage information for this program to STREAM (typically stdout 
   or stderr), and exit the program with EXIT_CODE. Does not return. */
void print_usage (FILE* stream, int exit_code)
{
  fprintf( stream, "Usage:  %s [options]\n", program_name );
  fprintf( stream,
           "  --nowait          Do not wait for state to fill, plan from vehicle state .\n"
				 );      
  exit(exit_code);
}

int main(int argc, char **argv) 
{
  int ch;
  /* A string listing valid short options letters. */
  const char* const short_options = "h";
  /* An array describing valid long options. */
  static struct option long_options[] = 
  {
    // first: long option (--option) string
    // second: 0 = no_argument, 1 = required_argument, 2 = optional_argument
    // third: if pointer, set variable to value of fourth argument
    //        if NULL, getopt_long returns fourth argument
    {"nowait",     0, &NOWAIT,           1},
    {"help",       0, NULL,              'h'},
    {NULL,         0, NULL,              0}
  };

  /* Remember the name of the program, to incorporate in messages.
     The name is stored in argv[0]. */
  program_name = argv[0];
  printf("\n");

  // Loop through and process all of the command-line input options.
  while((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1)
  {
    switch(ch)
    {
      case 'h':
        /* User has requested usage information. Print it to standard
           output, and exit with exit code zero (normal 
           termination). */
        print_usage(stdout, 0);

      case '?': /* The user specified an invalid option. */
        /* Print usage information to standard error, and exit with exit
           code one (indicating abnormal termination). */
        print_usage(stderr, 1);

      case -1: /* Done with options. */
        break;

    }
  }

  int sn_key = 0;
  char* pSkynetkey = getenv("SKYNET_KEY");
  if( pSkynetkey == NULL )
  {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
  }
  else
    sn_key = atoi(pSkynetkey);
  cerr << "Constructing skynet with KEY = " << sn_key << endl;

  CTrafficPlanner* pTrafficPlanner = new CTrafficPlanner(sn_key, NOWAIT == 0, RNDFFileName);

  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getMapDeltasThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getLocalMapThread);
  //DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getDPlannerStatusThread);
  DGCstartMemberFunctionThread(pTrafficPlanner, &CTrafficPlanner::getSegGoalsThread);
  
  // I have had issues where some of the threads are not up yet by the time the planning loop starts,
  // and I do not know why that is, but I have added a sleep here to make sure that all the threads 
  // are running. Want something more robust here?!
  #warning "make threads more independent during startup"
  sleep(1);
  
  pTrafficPlanner->TPlanningLoop();
  //pTrafficPlanner->getLocalMapThread();
  return 0;
}
