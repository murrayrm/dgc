#include "UTurnToUTurn.hh"
#include "UTurn.hh"

UTurnToUTurn::UTurnToUTurn(ControlState * state1, ControlState * state2, int stage)
: ControlStateTransition(state1, state2)
{
    m_stage = stage;
}

UTurnToUTurn::UTurnToUTurn()
{

}

UTurnToUTurn::~UTurnToUTurn()
{

}

double UTurnToUTurn::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * localMap, VehicleState vehState)
{

    cout << "in UTurnToUTurn::meetTransitionConditions" << endl;
    PointLabel ptLabel = PointLabel(planHorizon.getCurrentExitSegment(), planHorizon.getCurrentExitLane(), planHorizon.getCurrentExitWaypoint());
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
    point2 exitWayPt;
    localMap->getWaypoint(exitWayPt, ptLabel);
    double angle;
    localMap->getHeading(angle, ptLabel);
    double hplaneAng;
    double laneDir;
    point2 FC_pos,tmpPt;
        
    m_probability = 0;
    
    switch (m_stage) {
    
    case 1:
      {
        FC_pos = UTurn::m_ptStage1;

        localMap->getHeading(laneDir, FC_pos);
        hplaneAng = laneDir - M_PI;
        double dotProd = (-FC_pos.x + currRearPos.x) * cos(hplaneAng) + (-FC_pos.y + currRearPos.y) * sin(hplaneAng);
        double compDist = 1.5;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
            // When we get within 1 m of the hyperplane defined by the exit pt, goal complete
            if ((m_verbose) || (m_debug)) {
                cout << "UTURN1: SWITCHING TO STAGE 2!!!" << endl;
            }
            m_probability = 1;
        }
      }
        break;

       
    case 2:
      {
        FC_pos = UTurn::m_ptStage2;
        
        // Get direction of lane
        localMap->getLaneCenterPoint(tmpPt, UTurn::m_otherLaneLabel, FC_pos, 0.0);
        localMap->getHeading(laneDir, tmpPt);   // lane dir is the orientation of the lane
        hplaneAng = laneDir + M_PI / 3;       // hplaneAng is the orientation of the unit vector for the complete test
 
        double dotProd = (-FC_pos.x + currFrontPos.x) * cos(hplaneAng) + (-FC_pos.y + currFrontPos.y) * sin(hplaneAng);
        double compDist = 1.5;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;

        // When we get within 1 m of the hyperplane defined by the exit pt, goal complete
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          if ((m_verbose) || (m_debug)) {
                cout << endl << "in UTURN2: SWITCHING TO STAGE 3!!!" << endl;
     
          }
          m_probability = 1;
        }
      }
      break;
      
    case 3:
      {
        FC_pos = UTurn::m_ptStage3;
        localMap->getHeading(laneDir, FC_pos);
        hplaneAng = laneDir - 1.5 * M_PI;
        double dotProd = (-FC_pos.x + currRearPos.x) * cos(hplaneAng) + (-FC_pos.y + currRearPos.y) * sin(hplaneAng);
        double compDist = 1.5;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && (currVel < stoppedVel)) {
          // When we get within 1 m of the hyperplane defined by the exit pt, goal complete
          if ((m_verbose) || (m_debug)) {
            cout << "UTURN3: SWITCHING TO STAGE 4!!!" << endl;
          }
          m_probability = 1;
        }
        
      }
      break;

    case 4:
      {
        FC_pos = UTurn::m_ptStage4;
        localMap->getHeading(laneDir, FC_pos);
        hplaneAng = laneDir;
        double dotProd = (-FC_pos.x + currFrontPos.x) * cos(hplaneAng) + (-FC_pos.y + currFrontPos.y) * sin(hplaneAng);
        double compDist = 1;
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double stoppedVel = 0.1;
        
        if ((dotProd > -compDist) && ((m_stage == 4) || (currVel < stoppedVel))) {
          // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
          if ((m_verbose) || (m_debug)) {
            cout << "UTURN4: SWITCHING TO STAGE 5!!!" << endl;
          }
          m_probability = 1;
        }
      }
      break;
      
    default:
      break;
      
    }
    
    cout << endl << "UTurn (" << m_stage << ") = " << m_probability << endl <<endl;
    setUncertainty(m_probability);
    return m_probability;
}
