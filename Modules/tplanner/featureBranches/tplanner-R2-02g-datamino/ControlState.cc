#include "ControlState.hh"
#include "TrafficUtils.hh"
bool ControlState::m_verbose = false;
bool ControlState::m_debug = false;
bool ControlState::m_log = false;


ControlState::ControlState(int stateId, ControlStateFactory::ControlStateType type)
: State(stateId, type)
, m_AliceInitPos(0, 0)
, m_AliceUpdatePos(0, 0)
, m_currDistFromInitPos(0)
{
    m_stateID = stateId;
    m_type = type;
    m_failed = false;
}

ControlState::ControlState()
{

}

ControlState::~ControlState()
{

}

void ControlState::setOutputParams(bool debug, bool verbose, bool log)
{
    m_verbose = verbose;
    m_debug = debug;
    m_log = log;
}

void ControlState::setInitialPosition(VehicleState vehState)
{
    m_AliceInitPos = point2(vehState.localX, vehState.localY);
}

void ControlState::setUpdatedPosition(VehicleState vehState)
{
    m_AliceUpdatePos = point2(vehState.localX, vehState.localY);
}

void ControlState::setInitialTime()
{
    time(&m_AliceInitTime);
}

time_t ControlState::getInitialTime()
{
    return m_AliceInitTime;
}

point2 ControlState::getInitialPosition()
{
    return m_AliceInitPos;
}

point2 ControlState::getUpdatedPosition()
{
    return m_AliceUpdatePos;
}

double ControlState::calcDistFromInitPos(VehicleState vehState)
{
    setUpdatedPosition(vehState);
    m_currDistFromInitPos = TrafficUtils::calculateDistance(m_AliceUpdatePos, m_AliceInitPos);
    return m_currDistFromInitPos;
}

void ControlState::setInitialVelocity(VehicleState vehState)
{
    m_AliceInitVel = AliceStateHelper::getVelocityMag(vehState);

}

double ControlState::getInitialVelocity()
{
    return m_AliceInitVel;
}

int ControlState::getStateID()
{
    return m_stateID;
}

ControlStateFactory::ControlStateType ControlState::getType()
{
    return m_type;
}

bool ControlState::hasFailed()
{
    return m_failed;
}

void ControlState::resetFailure()
{
    m_failed = false;
}
