#include "ControlStateTransition.hh"
bool ControlStateTransition::m_verbose = false;
bool ControlStateTransition::m_debug = false;
bool ControlStateTransition::m_log = false;



ControlStateTransition::ControlStateTransition() 
{
}

ControlStateTransition::ControlStateTransition(ControlState* state1, ControlState* state2)
: StateTransition(state1,state2)
{ 
	m_controlState1 = state1;
	m_controlState2 = state2; 
}


ControlStateTransition::~ControlStateTransition()
{
  m_controlState1 = 0;
  m_controlState2 = 0; 

}

ControlState *ControlStateTransition::getControlStateTo()
{
  return m_controlState2;
}

void ControlStateTransition::setOutputParams(bool debug, bool verbose, bool log)
{
  m_verbose = verbose;
  m_debug = debug;
  m_log = log;  
  //  if (m_debug)
  //  cout << "in ControlStateTransition.cc: debug switched on" << endl;

}


