#ifndef CONTROLSTATEFACTORY_HH_
#define CONTROLSTATEFACTORY_HH_


#include "state/StateGraph.hh"

class ControlStateFactory {


  public:

    ControlStateFactory();
    ControlStateFactory(bool debug, bool verbose, bool log);
    ~ControlStateFactory();
	
    static typedef enum { LANE_KEEPING, SLOW_DOWN, STOP, STOPPED, UTURN } ControlStateType;
    static StateGraph createControlStates();
    static int getNextId();
    static void print(int type);
    static string *printString(int type);

  private:

    bool m_debug;
    bool m_verbose;
    bool m_log;
    int m_stateId;

};

#endif                          /*CONTROLSTATEFACTORY_HH_ */
