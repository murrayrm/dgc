#ifndef ZONEREGION_HH_
#define ZONEREGION_HH_

#include "TrafficState.hh"

// ZoneRegion defines a region of road that does not include an intersection, or an approach to an intersection 

class ZoneRegion : public TrafficState {
  
public: 

  ZoneRegion(int stateId, TrafficStateFactory::TrafficStateType type);
  ~ZoneRegion();

};

#endif /*ZONEREGION_HH_*/
