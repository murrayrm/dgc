#include "StateGraph.hh"


StateGraph::StateGraph()
{
   
}

StateGraph::~StateGraph()
{

}

StateGraph::StateGraph(std::vector<StateTransition*> stateTransitions) 
{
  m_stateTransitions = stateTransitions; 
}

void StateGraph::addStateTransition(StateTransition* stateTransition) 
{
  m_stateTransitions.push_back(stateTransition);
}

std::vector<StateTransition*> StateGraph::getOutStateTransitions(State *state) 
{
  std::vector<StateTransition*> transitions;
  unsigned int i = 0;
  
  //cout << "in StateGraph::getOutStateTransitions() : transitions size = " << m_stateTransitions.size() << endl;
  for (i=0; i < m_stateTransitions.size(); i++) 
    {
      if (m_stateTransitions[i]->getInitialState()->getStateId() == state->getStateId()) {
	transitions.push_back(m_stateTransitions[i]);
      }
    }
  return transitions; 
}


//State getState(int type)
//{
  
//}
