#include "RoadBuilder.hh"


RoadBuilder::RoadBuilder()
: m_currRoadId(0)
, m_currRoadLinkId(0)
{   

}

RoadBuilder::~RoadBuilder() 
{   
}

Road* RoadBuilder::buildRoad()
{   
	//parse the RNDF and get all the data in this mode
	return new Road(getNextRoadId());
}


RoadLink* RoadBuilder::buildRoadLink()
{   
	//parse the RNDF and get all the data in this mode
	return new RoadLink(getNextRoadLinkId());

}

int RoadBuilder::getNextRoadId() 
{
	return ++m_currRoadId; 
}
	
int RoadBuilder::getNextRoadLinkId() 
{

	return ++m_currRoadLinkId; 
}



