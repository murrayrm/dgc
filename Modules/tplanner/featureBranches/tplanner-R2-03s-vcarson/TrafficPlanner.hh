/*! TrafficPlanner.hh
 * Noel duToit
 * Jan 25 2007
 */

#ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

#include "CmdLineArgs.hh"

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/RDDFTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
//#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/RDDF.hh"
#include <cotk/cotk.h>
#include <ncurses.h>

// Mapping objects (tplanner version for now)
#include "mapping/Segment.hh"

// tplanner state (graph level)
#include "state/StateGraph.hh"

// new tplanner classes
#include "TrafficPlannerControl.hh"
#include "Corridor.hh"
#include "TrafficState.hh"
#include "ControlState.hh"
#include "TrafficStateEstimator.hh"
#include "TrafficStateFactory.hh"
#include "ControlStateFactory.hh"


const int NUM_ROWS = 250;
const int NUM_COLS = 250;
const double ROW_RES = 0.40;
const double COL_RES = 0.40;


/*! TrafficPlanner class
 * This is the main class for the traffic planner where the information from 
 * mapping is assimilated and combined with the goals from mplanner and the CA
 * traffic rules. This function inherits from StateClient, SegGoalsTalker, 
 * MapdeltaTalker, RDDFTalker, and LocalMapTalker
 * \brief Main class for traffic planner function  
 */ 
class CTrafficPlanner : public CStateClient, public CRDDFTalker, public CMapElementTalker, public CMapdeltaTalker  //SAM//, public CLocalMapTalker
{ 
public:

  /*! Contstructor */
  CTrafficPlanner(CmdLineArgs cmdLineArgs);
  /*! Standard destructor */
  ~CTrafficPlanner();

  // Console
  int initConsole();
	int closeConsole();
	static int onUserQuit(cotk_t *console, CTrafficPlanner *self, const char *token);
  //  static int onUserPause(cotk_t *console, CTrafficPlanner *self, const char *token);
  //static int onUserReset(cotk_t *console, CTrafficPlanner *self, const char *token);


  /////////////////////////////////////////////////////////////////////
  // Threads
  /////////////////////////////////////////////////////////////////////
  /*! this is the function that continually reads the latest object map*/
  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/
  void getDPlannerStatusThread();

  /*! this is the function that continually receives segment goals from mplanner */
  void getSegGoalsThread();
  
  /*! listening for full cost map requests from dplanner */
  void getStaticCostMapRequestThread();

  /////////////////////////////////////////////////////////////////////
  // Main planning loop
  /////////////////////////////////////////////////////////////////////
  /*! the main traffic planning loop*/
  void TPlanningLoop(void);

  /////////////////////////////////////////////////////////////////////
  // initialize map from rndf file
  /////////////////////////////////////////////////////////////////////
  bool loadRNDF(string filename) 
  {
    bool loadedMap = m_localMap->loadRNDF(filename);
    //TEMPORARY
    localMap->loadRNDF(filename);
    cout << "DONE LOADING RNDF" << endl;
    cout <<"Prior data size = " << localMap->prior.data.size() << endl;
    cout <<"Prior data full size = " << localMap->prior.fulldata.size() << endl;
    cout << "=================================================" << endl;
    //MapElement el;
    //el.set_id(-1);
    //el.set_circle_obs();
    //    point2 tmppt(-.75,-35);
    //    point2 tmppt;
    // PointLabel ptlabel(1,1,5);
    //localMap->getWaypoint(tmppt, ptlabel);
    //el.set_geometry(tmppt,3.0);
    //localMap->data.push_back(el);
    //m_localMap->data.push_back(el);
    
    //    PointLabel ptlabel2(1,2,4);
    // localMap->getWaypoint(tmppt, ptlabel2);
    //el.set_geometry(tmppt,3.0);
    //localMap->data.push_back(el);
    //m_localMap->data.push_back(el);

    return loadedMap;
  }


  bool isComplete(PointLabel exitWayptLabel, VehicleState vehState);

  /*! this is the function that continually reads map deltas and updates the map */
  //void getMapDeltasThread();
  
  // current tplanner state variables
  static TrafficState * m_currTrafficState;
  static ControlState * m_currControlState;
  static PlanningHorizon m_currPlanHorizon;
  static Map * localMap;

private:
  void readConfigFile(char* file);

  /*!\param m_snKey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_recvLocalMap specifies whether tplanner listens for map updates
   * from mapper, or just uses the initialized map (from rndf)
   */
  bool m_recvLocalMap;

  /*!\param m_recvSegmentGoals specifies whether tplanner listens for segment
   * goals from mplanner, or just uses some default (this is for dev only)
   */ 
  bool m_recvSegmentGoals;

  /*!\param m_debugLevel specifies level of debugging
   */
  bool m_debug;

  /*!\param m_verbose specifies level of debugging
   */
  bool m_verbose;

  /*!\param m_log indicates whether logging was enabled
   */
  bool m_log;

  /*!\param m_use_local indicates whether we are using local or global coordinates for the corridor (temporary)
   */
  bool m_use_local; // For right now the default is to plan in global. Want to switch in the near future.

  /*!\param m_console indicates whether console was enabled
   */
  bool m_console;

  /*!\param m_sendCostMap indicates whether we want to send parameters for
   * painting cost map */
  bool m_sendCostMapParams;

  /*!\Parameters for painting cost map */
  BitmapParams m_bmparams;
  PolygonParams m_polygonParams;

  // Console
  cotk_t *console;

  // Local Map
  Map * m_localMap;

  pthread_mutex_t m_LocalMapMutex;

  // Cost map
  CMapPlus *m_costMap;
  int m_costLayerID;
  int m_tempLayerID;
  pthread_mutex_t m_CostMapMutex;
  int requestFullStaticCostMapSocket;

  // dplanner Status
  /*!\param m_dplannerStatus is the dplanner status variable - the feedback from dplanner to tplanner*/
  //DPlannerStatus* m_dplannerStatus;
  //pthread_mutex_t m_DPlannerStatusMutex;

  // Segment goals
  /*!\param m_segGoals is the Segment goals variable that is populated by the SegGoalsTalker*/
  list<SegGoals> m_segGoals;

  pthread_mutex_t m_SegGoalsMutex;
  int segGoalsSocket;                // The skynet socket for receiving segment goals from mplanner

  TrafficStateEstimator* m_trafStateEst;
  TrafficPlannerControl* m_tplannerControl;

  //Factories 
  TrafficStateFactory m_tfac; 
  ControlStateFactory m_cfac; 

  // corridor
  Corridor corridor;
  point2 m_gloToLocalDelta;
  // initialize skynet talker for OCPparams
  SkynetTalker <OCPparams> m_OCPparamsTalker;
  // vehicle state
  //VehicleState m_currVehState;
};

#endif  // TRAFFICPLANNER_HH

