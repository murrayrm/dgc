#include "Creep.hh"

Creep::Creep(int stateId, ControlStateFactory::ControlStateType type)
:ControlState(stateId, type)
{
}

Creep::~Creep()
{
}

int Creep::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    if ((m_verbose) || (m_debug)) {
        cout << "In Creep::determineCorridor " << endl;
    }
    
    int error = 0;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);


    switch (traffState->getType()) {
    
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      {
        bool isReverse = false;
        error += CorridorGen::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
        error += CorridorGen::setFinalCondStop(corr, localMap, currSegment);
      }
      break;

    case TrafficStateFactory::INTERSECTION_STOP:
      {
        SegGoals nextSegment = planHoriz.getSegGoal(1);
        bool isReverse = false;
        error += CorridorGen::makeCorridorIntersection(corr, currFrontPos, localMap, currSegment);        
        error += CorridorGen::setFinalCondStopped(corr, vehState);
      }
      break;
        
    default:
      {    
        cerr << "Creep.cc: Undefined Traffic state" << endl;
        error += 1;
      }
    }
    return error;
}


ControlState* Creep::newCopy() {
  return new Creep(*this);
}
