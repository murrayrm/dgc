#ifndef CORRIDOR_HH_
#define CORRIDOR_HH_

#include "dgcutils/RDDF.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/Polytope.hh"
#include "mapping/GeometricConstraints.hh"
#include "Conflict.hh"
//#include "mapping/Line.hh"
#include "frames/point2.hh"
//#include <unistd.h>
#include <stdio.h>
//#include <iostream>
//#include <sstream>
//#include <string>
#include <vector>
#include <math.h>
#include "dgcutils/ggis.h"
#include "state/AliceStateHelper.hh"
#include "map/Map.hh"
//#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "cmap/CMapPlus.hh"
#include <boost/serialization/vector.hpp>


#define EPS 0.05
#define BIGNUMBER 1000000000.0f 
#define PI 3.1416
#define EXPFACT 1
#define EXPPERC 0.25
/*      
struct sendPoint
{
    double x;
    double y;

    sendPoint(double v1, double v2)
    {
        x = v1;
		y = v2;
   
    }
    sendPoint()
    {
        x = 0;
	    y = 0;
    }
    
	sendPoint(const sendPoint& p)
    {
        x = p.x;
	    y = p.y;
    }
    template <class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
        ar & x;
	    ar & y;
	}
};

typedef vector <sendPoint > sendPoly;
typedef vector <sendPoly> sendCorr;

*/

class Corridor {

public :  		

  Corridor();
  Corridor(bool debug, bool verbose, bool log);
  ~Corridor();

  GeometricConstraints* getGeometricConstraints();
  RDDF* getRddfCorridor(point2 gloToLocalDelta);
  int getControlStateId();
  void addPolyline(point2arr);
  void setVelProfile(vector<double> velProfile);
  void setDesiredAcc(double desAcc);
  void setAlicePrevPos(point2 alicePos);
  //  void updateDistFromConStateBegin(point2 alicePrevPos);
  vector<double> getVelProfile();
  double getDesiredAcc();
	void clear();

  void initializeOCPparams(VehicleState vehState, double vmin, double vmax);
  OCPparams getOCPparams();
  void setOCPfinalCondLB(int index, double cond);
  void setOCPfinalCondUB(int index, double cond);
  void setOCPinitialCondLB(int index, double cond);
  void setOCPinitialCondUB(int index, double cond);
  void convertOCPtoGlobal(point2 gloToLocalDelta);
  void setOCPmode(int mode);

  void paintLaneCost(CMapPlus* map, int costLayer, int tempLayer);
  void convertMapElementToCost(CMapPlus* map, int costLayer, int tempLayer, MapElement* el);
  void getCostMap(CMapPlus* map, int costLayer, int tempLayer, double currNorthing, double currEasting, Map* lmap);
  void generatePolyCorridor();
  void printPolyCorridor(void);
  void sendPolyCorridor(void);
private: 
  bool m_verbose;
  bool m_debug;
  bool m_log;

  RDDF* convertToRddf(point2 gloToLocalDelta);  
  // These are internal functions for convertToRddf()
  void closept(double lnseg[4], double pt[2], double cpt[2]);
  void midpt(double pt1[2], double pt2[2], double mpt[2]);
  void radius(double mpt[2], double pt1[2], double pt2[2], double &radius);

  GeometricConstraints* m_geometricConstraints;
  
  Conflict* m_conflict;
  
  vector<point2arr> m_polylines; 
  int m_controlStateId;

  vector<double> m_velProfile; // [V_entry V_exit]
  double m_desiredAcc;
  double m_distFromConStateBegin;
  point2 m_alicePrevPos;
  OCPparams m_ocpParams;
  CPolytope* m_polyCorridor;
  int m_nPolytopes;
  
  SkynetTalker<sendCorr> m_polyCorridorTalker;

};
 

#endif /*CORRIDOR_HH_*/
