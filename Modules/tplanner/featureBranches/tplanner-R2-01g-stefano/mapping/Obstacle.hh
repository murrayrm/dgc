#ifndef OBSTACLE_HH_
#define OBSTACLE_HH_

#include "GeometricConstraints.hh"



class Obstacle {

public:
  	Obstacle(int obsId, GeometricConstraints& constraints, std::vector<double> pos, std::vector<double> vel, 
			double velMag,std::vector<double> posUncert, std::vector<double> velUncert,
 			double velMagUncert, bool exists, double existsUncert, bool traversable,
   			double traversUncert);
   			
   	//TODO remove this constructor int obsId
   	Obstacle(int obsId);
   			
	int getId();
   	std::vector<double> getPosition();
   	std::vector<double> getPositionUncertainty();

   	std::vector<double> getVelocityVector();
	std::vector<double> getVelocityVectorUncertainty();
   	double getVelocityMagnitude();
   	double getVelocityMagnitudeUncertainty();

	GeometricConstraints getGeometry(); 
   	GeometricConstraints  getGeometryProjectionToRoad();

   	bool exists();
   	bool isTraversable(); 	
   	double existenceUncertainty();
   	double traversabilityUncertainty();
   	double getDecay();
   	void setDecay(double decay);
  
private: 

	int m_id;

	GeometricConstraints m_geometry;

  	/** 
   	*  The position point describes the midpoint of the geometric object 
   	*/
   	std::vector<double> m_pos;
   	std::vector<double> m_vel;
   	double m_velMag; 

 
	/** Uncertainties */
   	std::vector<double> m_posUncert;  	
   	std::vector<double> m_velUncert;
 	double m_velMagUncert;
  
  
 	bool m_exists;
   	double m_existsUncert;
   	bool m_traversable;
   	double m_traversUncert;
   	double m_decay;
   
   };
   
#endif /*OBSTACLE_HH_*/
