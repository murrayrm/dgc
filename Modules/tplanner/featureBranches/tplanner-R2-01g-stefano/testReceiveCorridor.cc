#include <iostream>
#include "Corridor.hh"
#include <boost/serialization/vector.hpp>
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/Polytope.hh"

int main(int argc,char** argv)
{   
    CAlgebraicGeometry algGeom = CAlgebraicGeometry();
    int sn_key;
    CPolytope* pP;
    cout << " Insert Skynet key :" ;
	cin >> sn_key;
    double** vertices;
	int NofDim = 2;
    SkynetTalker<sendCorr> recCorTalker= SkynetTalker<sendCorr>(sn_key, SNpolyCorridor,MODdynamicplanner);
	sendCorr corridor;
	recCorTalker.receive(&corridor);
	cout << "corridor received" << endl;
	int corSize = corridor.size();
	//delete[] pP;
	pP = new CPolytope[corSize];
	cout << " Polytopes number: " << corSize<<endl;;
	for(int i=0; i<corSize; i++)
	{
        cout << "Polytope " << i << " :  Number of Vertices " <<  corridor[i].size() << endl;
        int polySize = corridor[i].size();
	   vertices = new double*[NofDim];
	   for(int j=0;j<NofDim;j++)
            vertices[j] = new double[polySize];
		for(int j=0;j<polySize;j++)
		{
		    vertices[0][j] = corridor[i][j].x ;
		    vertices[1][j] = corridor[i][j].y ;
		    cout << "Vtx " << j<< " : (" <<  corridor[i][j].x << "," <<  corridor[i][j].y << ")" << endl;
        }    
        
        pP[i] = new CPolytope(&NofDim, &polySize, (const double**) vertices);
        cout << "empty poly created " << endl;
		algGeom.FacetEnumeration(&pP[i]);
        algGeom.VertexEnumeration(&pP[i]); //vertex reduction
        cout << &pP[i] << endl;
      //  delete pP;

	    for(int j=0;j<NofDim;j++)
            delete[]  vertices[j];
        delete vertices;		   


	}
    



}
