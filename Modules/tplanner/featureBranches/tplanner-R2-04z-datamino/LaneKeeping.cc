#include "LaneKeeping.hh"
#include "Log.hh"

LaneKeeping::LaneKeeping(int stateId, ControlStateFactory::ControlStateType type, LaneKeepingType lkt)
:ControlState(stateId, type)
{
  isReverse = false;
  m_lanekeeping_type = lkt;
}

LaneKeeping::~LaneKeeping()
{
}

bool LaneKeeping::isPassing()
{
  return (m_lanekeeping_type == PASSING);
}

int LaneKeeping::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    if ((m_verbose) || (m_debug)) {
        Log::getStream(1) << "In LaneKeeping::determineCorridor " << endl;
    }
    
    int error = 0;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);


    switch (traffState->getType()) {
    
    case TrafficStateFactory::ZONE_REGION:
    {
        SegGoals nextSegment = planHoriz.getSegGoal(1);
	//        bool isReverse=false;
        error += CorridorUtils::makeCorridorZone(corr, currRearPos, localMap, currSegment, nextSegment);
        error += CorridorUtils::setFinalCondZone(corr, currFrontPos, localMap, currSegment);
    }
        break;

    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
      {

        // bool isReverse = false;
        error += CorridorUtils::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
 
        // specify the ocpParams final conditions - lower bounds
        if (traffState->getType() == TrafficStateFactory::ROAD_REGION) {
          error += CorridorUtils::setFinalCondLK(corr, currFrontPos, localMap, currSegment, isReverse);
        } else if (traffState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY) {
          error += CorridorUtils::setFinalCondStop(corr, localMap, currSegment);
        }
        
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
      SegGoals nextSegment = planHoriz.getSegGoal(1);
      bool isReverse = false;
      error += CorridorUtils::makeCorridorIntersection(corr, currFrontPos, localMap, currSegment);
      error += CorridorUtils::setFinalCondLK(corr, currFrontPos, localMap, nextSegment, isReverse);
    }
        break;
        
    default:
      {
        cerr << "LaneKeeping.cc: Undefined Traffic state" << endl;
        error += 1;
      } 
    }

    return error;

}
