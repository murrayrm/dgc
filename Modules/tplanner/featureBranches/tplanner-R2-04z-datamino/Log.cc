#include "Log.hh"

#include <math.h>

/** Initialize verbose level to maximum */
int Log::verbose_level = 9; 
NullStream Log::nStream;

Log::Log()
{
    traffic_id = -1;
    control_id = -1;
    segment_id = -1;
    log = false;
}

Log::~Log()
{

}

int Log::init(string filename, bool logit)
{
    log = logit;
    if (!log) return 0;

    file = filename;

    /* Empty file or create it */
    fp = fopen(file.c_str(), "w");

    if (fp == NULL) return -1;
    else close();

    return 0;
}

void Log::logSegment(PlanningHorizon ph)
{
    if (open()) return;
   
    SegGoals seg = ph.getSegGoal(0);

    int id = seg.entrySegmentID;

    if (segment_id == id)
        goto err;

    segment_id = id;

    fprintf(fp, "Current Segment:\n%s\n\n", seg.toString().c_str());
    
err:
    close();
}

void Log::logTrafficState(TrafficState *state, ControlState *cstate, PlanningHorizon ph, Map *map)
{
    double distance;
    time_t time;
    string info;

    if (open()) return;

    /* Get info */
    int id = state->getStateId();
    TrafficStateFactory::TrafficStateType type = state->getType();
    point2 pos = cstate->getInitialPosition();
    SegGoals seg = ph.getSegGoal(0);
    PointLabel exitWayptLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
    point2 exit;
    if (map->getWaypoint(exit, exitWayptLabel) != 0) goto err;

    if (traffic_id == id)
        goto err;

    traffic_id = id;
    
    /* Compute info */
    distance = sqrt((pos.x - exit.x)*(pos.x - exit.x) + (pos.y - exit.y)*(pos.y - exit.y));
    time = cstate->getInitialTime();
    info = TrafficStateFactory::printString(type);
    
    /* Print info */
    fprintf(fp, "Traffic state: %s\n", info.c_str());
    fprintf(fp, "  Distance to exit: %f\n", distance);
    fprintf(fp, "  Time entered: %ld\n\n", time);
    
err:
    close();
}

void Log::logControlState(ControlState *state)
{
    double vel;
    point2 pos;
    time_t time;
    string info;

    if (open()) return;
    
    int id = state->getStateId();
    ControlStateFactory::ControlStateType type = state->getType();

    if (control_id == id)
        goto err;

    control_id = id;
        
    /* Compute info */
    vel = state->getInitialVelocity();
    pos = state->getInitialPosition();
    time = state->getInitialTime();
    info =  ControlStateFactory::printString(type);
    
    fprintf(fp, "Control state: %s\n", info.c_str());
    fprintf(fp, "  Time entered: %ld\n", time);
    fprintf(fp, "  Position entered: [ %f , %f ]\n", pos.x, pos.y);
    fprintf(fp, "  Velocity entered: %f\n\n", vel);
    
err:
    close();
}

/**
 * Corridor gen progress (i.e., initial generation complete, static obs added, cost map generated, feasibility checked, etc.)
 * Goal complete info
 */
void Log::logString(string str)
{
    if (open()) return;    
    
    fprintf(fp, "%s\n\n", str.c_str());
    
    close();
}

int Log::open()
{
    if (!log) return -1;
    
    fp = fopen(file.c_str(), "a");
    return (fp == NULL);
}

void Log::close()
{
    if (!log) return;

    fclose(fp);
}

int Log::getVerboseLevel()
{
    return verbose_level;
}

void Log::setVerboseLevel(int level)
{
    verbose_level = level;
}

ostream& Log::getStream(int level)
{
    if (verbose_level >= level)
        return cout;
    else
        return nStream;
}
