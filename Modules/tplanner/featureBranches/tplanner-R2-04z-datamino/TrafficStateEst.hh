#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_


#include <vector>
#include <deque>
#include "map/Map.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"
#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"
#include "interfaces/VehicleState.h"
#include "skynettalker/StateClient.hh"
#include "map/MapElementTalker.hh"
#include "interfaces/LeadingVehicleInfo.hh" 
#include "skynettalker/SkynetTalker.hh"

typedef std::vector<double> FollowObsData; 

class TrafficStateEst : public CStateClient {


protected: 

  /*! The constructors are protected since we want a singleton */
  TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log, bool useRNDF, string RNDFfile);
 
  TrafficStateEst(const TrafficStateEst&);
  ~TrafficStateEst();

public:

  /*! Get the singleton instance */

  static TrafficStateEst* Instance(int skynetKey, bool waitForStateFill, bool debug, 
				   bool verbose, bool log, bool useRNDF, string RNDFfilename);

  /*! Free the singleton instance */
  static void Destroy();

  /*! You are not allowed to copy a singleton */
  TrafficStateEst& operator=(const TrafficStateEst&);

  /*! Estimate traffic state */
  TrafficState* determineTrafficState(PointLabel ptLabel);

  /*! Get traffic state at the last estimate */ 
  TrafficState* getCurrentTrafficState();

  /*! Get the current vehicle state */
  void updateVehState();

  /*! Get the vehicle state at the last estimate */
  VehicleState getVehState();

  /*! Get the updated vehicle state */
  VehicleState getUpdatedVehState();

  /*! Get map at last estimate */
  Map* getMap(); 

  void updateMap();

  /*! Get most recent map */
  Map* getUpdatedMap();

  /*! Gets a local map update thread */
  void getLocalMapUpdate();

  /*! Get vel and distance for the obstacle that we are following */
  FollowObsData getFollowObstacleData();

  /*! Update vel and distance for the obstacle that we are following 
   in a thread. */
  void getFollowObstacleUpdate();

  /*! Get positions of obstacles in the opposite lane in a thread. */
  void getOppositeLaneObstacleUpdate();

  /*! Send the nearest obstacle velocity and distance in the lane */
  void sendFollowObstacle();

  /*! Updates m_queueing whether we are in a queueing situation  */
  void updateQueueing();

  /*! Updates m_vehInOppositeLane whether there are vehicles in the opposite lane */
  void updateVehiclesInOppositeLane();

  /*! Returns whether we are in a queueing situation  */
  bool isQueueing();

  /*! Returns whether there are vehicles in the opposite lane  */
  bool isVehiclePresentInOppositeLane();

  /*! Calculates separation distance given any vehicle state */
  double getSeparationDistance(VehicleState vehState);

  vector<MapId> getVehicleIds() {
    return vehicle_ids;
  }

private : 

  /*! Get singleton instance */
  static TrafficStateEst* pinstance;

  /*! Get the map at the last estimate */
  void buildTrafficStateGraph();

  /*! Choose the most probable state given the most recent traffic state estimate */
  TrafficState* chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions);

  /*! Get the local to global map update. */
  void getLocalGlobalMapUpdate();

  bool loadRNDF(string filename); 

  /*! Is the obstacle in front of us moving. */
  bool isObstacleMoving();

  /*! Is there an obstacle moving in the opposite lane. */
  bool isObstacleMovingInOppositeLane();

  /*! Size of the follow obstacle data. */
  int sizeOfFollowObsData();

  /*! Size of the opposite lane vehicle data. */
  int sizeOfOppositeLaneVehData();

  bool m_verbose;
  bool m_debug;
  bool m_log;

  /*! The traffic state machine  */
  StateGraph m_trafficGraph;

  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The updated vehicle state  */
  VehicleState m_updatedVehState;

  /*! The vehicle state at the last traffic state estimate */
  VehicleState m_vehStateAtLastEst;

  /*! The current traffic state */
  TrafficState* m_currTraffState; 

  /*! The map element talker to receive map updates*/
  CMapElementTalker m_mapElemTalker;

  /*! The map getting updated */
  Map* m_localUpdateMap;

  /*! The map to read  */
  Map* m_localMap;

  /*! The updated map to return */
  Map* m_updatedMap;

  /*! The current map */
  pthread_mutex_t m_localMapUpMutex;

  /*! The current follow  obs data mutex */
  pthread_mutex_t m_followObsDataMutex;

  /*! The current follow  obs data mutex */
  pthread_mutex_t m_oppLaneVehDataMutex;

  /*! The current m_queueing mutex */
  pthread_mutex_t m_queueingMutex;

  /*!\Follow Vehicle talker  */
  SkynetTalker<leadVehInfo> m_followVehTalker;

  /*!\The current obstacle we are following vel and distance*/
  FollowObsData m_currFollowObs;

  /*!\A queue of the obstacles we are following vel and distance*/
  std::deque<point2> m_followObsData;

  /*!\A queue of the obstacles we are following vel and distance*/
  std::deque<point2> m_oppLaneVehData;

  /*!\A queue of the obstacles we are following vel and distance*/
  std::deque<point2> m_followObsDataSnap;

  /*!\A queue of the vehicle in opposite lane. */
  std::deque<point2> m_oppLaneVehDataSnap;

  /*!\The current obstacle we are following vel and distance updated*/
  FollowObsData m_currFollowObsUpdated;

  /*! The global to local delta */
  point2 m_gloToLocalDelta;

  /*! Variable to use RNDF or not */
  bool m_useRNDF;

  /*! Variable for RNDF filename */
  string m_RNDFFile; 

  /*! Variable to know if we are queueing */
  bool m_queueing; 

  /*! Variable to know if there is an obstacle we are following present as we are querying map */
  bool m_followObsPresent; 

  /*! Follow Id */
  MapId m_followObsId; 

  /*! Variable to know if we are waiting for vehicles in opposite lane */
  bool m_vehInOppLane; 

  /*! Variable to know if there are vehicles present in the opposite lane */
  bool m_oppLaneVehPresent; 

  /*! Follow Id */
  MapId m_oppLaneVehId; 

  vector<MapId> vehicle_ids;
  
};

#endif /*TRAFFICSTATEEST_HH_*/
