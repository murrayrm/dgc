#ifndef ALICESTATEHELPER_HH_
#define ALICESTATEHELPER_HH_


#include "frames/point2.hh"
#include <vector>
#include "alice/AliceConstants.h"
#include "interfaces/VehicleState.h"
#include "map/Map.hh"


class AliceStateHelper {

public:
  static point2 getPositionFrontAxle(VehicleState vehState);
  static point2 getPositionFrontBumper(VehicleState vehState);
  static point2 getPositionRearAxle(VehicleState vehState); 
  static point2 getPositionRearBumper(VehicleState vehState); 
  static double getVelocityMag(VehicleState vehState); 
  static double getAccelerationMag(VehicleState vehState);
	static point2 convertToGlobal(point2 gloToLocalDelta, point2 localPoint);
};
#endif /*ALICESTATEHELPER_HH_*/
