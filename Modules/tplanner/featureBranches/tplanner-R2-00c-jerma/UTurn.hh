#ifndef UTURN_HH_
#define UTURN_HH_

#include "ControlState.hh"
#include "TrafficUtils.hh"
#include <math.h>

class UTurn : public ControlState {

public: 

  UTurn(int stateId, ControlStateFactory::ControlStateType type);
  ~UTurn();
  virtual int determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* map);


private:
  bool stage1;

};
#endif /*UTURN_HH_*/
