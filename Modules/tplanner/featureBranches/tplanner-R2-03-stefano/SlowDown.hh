#ifndef SLOWDOWN_HH_
#define SLOWDOWN_HH_

#include "ControlState.hh"
#include "alice/AliceConstants.h"
#include "TrafficUtils.hh"
#include <math.h>

class SlowDown:public ControlState {

public:

  SlowDown(int stateId, ControlStateFactory::ControlStateType type);
  ~SlowDown();
  int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);
  
  ControlState* newCopy() { return (ControlState*) new SlowDown(*this); }
private:
  
  double m_desDecc;
};

#endif                          /*SLOWDOWN_HH_ */
