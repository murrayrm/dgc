#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_


#include "map/Map.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"
#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"
#include "interfaces/VehicleState.h"
#include "skynettalker/StateClient.hh"
#include "map/MapElementTalker.hh"

class TrafficStateEst : public CStateClient {

public: 

  TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log); 
  ~TrafficStateEst();

  /*! Estimate traffic state */
  TrafficState* determineTrafficState(PointLabel ptLabel);

  /*! Get traffic state at the last estimate */ 
  TrafficState* getCurrentTrafficState();

  /*! Get the current vehicle state */
  VehicleState getUpdatedVehState();

  /*! Get the vehicle state at the last estimate */
  VehicleState getVehStateAtLastEst();

  /*! Get the current map */
  Map* getUpdatedMap();

  /*! Get the map at the last estimate */
  Map* getMapAtLastEst();

private : 

  /*! Get the map at the last estimate */
  void buildTrafficStateGraph();

  /*! Choose the most probable state given the most recent traffic state estimate */
  TrafficState* chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions);

  /*! Gets a local map update */
  void getLocalMapUpdate();

  bool m_verbose;
  bool m_debug;
  bool m_log;

  /*! The traffic state machine  */
  StateGraph m_trafficGraph;

  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The vehicle state at the last traffic state estimate */
  VehicleState m_vehStateAtLastEst;

  /*! The current traffic state */
  TrafficState* m_currTraffState; 

  /*! The map element talker to receive map updates*/
  CMapElementTalker m_mapElemTalker;

  /*! The current map */
  Map* m_localMap;
  /*! The map at the last traffic state estimate */
  Map* m_localMapAtLastEst;

};

#endif /*TRAFFICSTATEEST_HH_*/
