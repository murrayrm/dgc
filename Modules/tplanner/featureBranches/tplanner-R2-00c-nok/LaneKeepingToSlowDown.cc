#include "LaneKeepingToSlowDown.hh"
#include <math.h>
#include <list>


LaneKeepingToSlowDown::LaneKeepingToSlowDown(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
  , m_distToObs(10)
{

}

LaneKeepingToSlowDown::LaneKeepingToSlowDown()
{

}

LaneKeepingToSlowDown::~LaneKeepingToSlowDown()
{

}

double LaneKeepingToSlowDown::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon horiz,Map* localMap, VehicleState vehState)
{

  double currVel = 0; 
  double delta = 0;
  double dstop = 0;
  int stopLineErr = -1;
  point2 stopLinePos, currFrontPos;
  PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
  currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
 
 //TODO FIX composite state
  //if (TrafficStateFactory::CLEARREGION == trafficState.getType()) {
	 
  if (TrafficStateFactory::APPROACH_INTER_SAFETY == trafficState->getType()) {
    double currVel = 0; 
    double delta = 0;
    double dstop = 0;
    int stopLineErr = -1;
    point2 stopLinePos, currFrontPos;
    PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
    currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    cout << "in lanekeepingtoslowdown: APPROACH_INTER_SAFETY" << endl;
    currVel = AliceStateHelper::getVelocityMag(vehState);
    delta = -pow(currVel,2)/(2*m_desiredDecel);
    // use getStopline() call based on our current pos here
    stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
    //stopLineErr = localMap->getStopline(stopLinePos, label);
    //stopLineErr = localMap->getNextStopline(stopLinePos, label);
    dstop = stopLinePos.dist(currFrontPos);
    cout << "distance to stop = " << dstop << endl;
    if (dstop <= delta) {
      m_probability = 1;
    } else { 
      m_probability = 0; 
    }

  } else if (TrafficStateFactory::ZONE_REGION == trafficState->getType()) {
    m_probability = 0;

  } else if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
     cout << "in lanekeepingtoslowdown: ROAD_REGION" << endl;
     double currVel = AliceStateHelper::getVelocityMag(vehState);
     double delta = -pow(currVel,2)/(2*m_desiredDecel)+m_distToObs;
     cout << "lane keep to slow down delta = " << delta << endl;
     point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
     double distToObs = localMap->getObstacleDist(currFrontPos,0);
     if (distToObs <= delta) {
       m_probability = 1;
     } else { 
       m_probability = 0; 
     }   


  } else if (TrafficStateFactory::INTERSECTION_STOP == trafficState->getType()) {
    m_probability = 0;
  }


  setUncertainty(m_probability);
  return m_probability;
} 


