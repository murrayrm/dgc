#ifndef INTERSECTIONLINK_HH_
#define INTERSECTIONLINK_HH_


#include "RoadLink.hh"

class IntersectionLink : public RoadLink {

public:

   	IntersectionLink(int linkId);
   	IntersectionLink(int linkId, Line stopLine);
	~IntersectionLink();
	void setStopLine(Line stopLine);
	bool hasStopLine();
  	  
private: 	
	Line m_stopLine;

   };
   
#endif /*INTERSECTIONLINK_HH_*/
