#ifndef INTERSECTIONSTOP_HH_
#define INTERSECTIONSTOP_HH_

#include "TrafficState.hh"

// IntersectionStop defines a region of road that does not include an intersection, or an approach to an intersection 

class IntersectionStop : public TrafficState {
  
public: 

  IntersectionStop(int stateId, TrafficStateFactory::TrafficStateType type);
  ~IntersectionStop();

};

#endif /*INTERSECTIONSTOP_HH_*/
