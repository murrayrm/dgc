#ifndef EDGE_HH_
#define EDGE_HH_

#include "Vertex.hh"

class Edge  {

public:

Edge(Vertex* vertex1, Vertex* vertex2);
Edge();
~Edge();

Vertex* getInitialVertex();
Vertex* getFinalVertex(); 
//TODO FIX
 
private:

Vertex* m_vertex1; 
Vertex* m_vertex2;
	
}; 
#endif /*EDGE_HH_*/
