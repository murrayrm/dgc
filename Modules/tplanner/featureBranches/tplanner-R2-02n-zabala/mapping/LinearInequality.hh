#ifndef LINEARINEQUALITY_HH_
#define LINEARINEQUALITY_HH_

#include <vector>

class LinearInequality {

public: 

	LinearInequality(int dim, std::vector<double> vec);
	std::vector<double> getVector();
	int getDimension();
	
private:	
	int m_dimension; 
	std::vector<double> m_vector;
};

#endif /*LINEARINEQUALITY_HH_*/
