#include "StateTransition.hh"

StateTransition::StateTransition(State* State1, State* State2)
: m_State1(State1)
, m_State2(State2)
, m_uncertainty(0)
{  

}

StateTransition::StateTransition()
{  
}

StateTransition::~StateTransition()
{  
}

State* StateTransition::getInitialState()
{  
  return m_State1; 
}


State* StateTransition::getFinalState()
{  
  return m_State2; 
} 
 

double StateTransition::getUncertainty()
{  
  return m_uncertainty;
} 
 
void StateTransition::setUncertainty(double uncert)
{
  m_uncertainty = uncert; 
	
} 

int StateTransition::getType() 
{
  return m_type; 	
}
