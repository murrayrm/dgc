#include "TrafficState.hh"
//#include "TrafficStateFactory.hh"

bool TrafficState::m_verbose = false;
bool TrafficState::m_debug = false;
bool TrafficState::m_log = false;


TrafficState::TrafficState(int stateId, TrafficStateFactory::TrafficStateType type)
: State(stateId, type)
{
	m_stateID = stateId;
	m_type = type;
}

TrafficState::TrafficState()
{ 

}


TrafficState::~TrafficState()
{ 

}

void TrafficState::setOutputParams(bool debug, bool verbose, bool log)
{
  m_verbose = verbose;
  m_debug = debug;
  m_log = log;
}

TrafficStateFactory::TrafficStateType TrafficState::getType()
{
    return m_type;
}

int TrafficState::getStateID()
{
    return m_stateID;
}

string TrafficState::toString() 
{

  string traffStateType;
  switch(m_type) {
  case TrafficStateFactory::ROAD_REGION:
    traffStateType = "Type = ROAD_REGION  ";
    break;
  case TrafficStateFactory::ZONE_REGION:
    traffStateType = "Type = ZONE_REGION  ";
    break;
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    traffStateType = "Type = APPROACH_INTER_SAFETY ";
    break;
  case TrafficStateFactory::INTERSECTION_STOP:
    traffStateType = "Type = INTERSECTION_STOP ";
    break;
  default: 
    break;
  };
 
  return traffStateType;
}
