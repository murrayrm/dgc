#ifndef ROADTOAPPROACHINTERSAFETY_HH_
#define ROADTOAPPROACHINTERSAFETY_HH_


#include "TrafficStateTransition.hh"
#include "TrafficUtils.hh"


class RoadToApproachInterSafety:public TrafficStateTransition {

  public:

    RoadToApproachInterSafety(TrafficState * state1, TrafficState * state2);
    ~RoadToApproachInterSafety();

    double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState);
    double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel ptLabel);
    
  private:

    double m_distSafeStop;      //in meters

};

#endif                          /*ROADTOAPPROACHINTERSAFETY_HH_ */
