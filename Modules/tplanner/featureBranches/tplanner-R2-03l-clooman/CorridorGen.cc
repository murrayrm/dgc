#include "CorridorGen.hh"
#include <cmath>
#include <fstream>

int CorridorGen::makeCorridorLane(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, double range, bool reverse, LaneLabel desiredLane)
{
  bool m_verbose = true;
  bool m_debug = true;

  if ((m_verbose) || (m_debug)) {
    cout << "CorridorGen::makeCorridorLane" << endl;
  }
  
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);

  int getBoundsErr = 0;
  if (reverse)
    getBoundsErr = localMap->getBoundsReverse(leftBound1, rightBound1, desiredLane, currFrontPos, range);
  else {
    point2arr_uncertain tmpL, tmpR;
    int getBoundsErr = localMap->prior.getLaneBoundsFull(tmpL, tmpR, desiredLane);
    
    leftBound1.set(tmpL);
    rightBound1.set(tmpR);
  }
        
  if (getBoundsErr < 0) {
    cerr << "CorridorGen.cc: boundary read from map error" << endl;
    return (getBoundsErr);
  }
        
  // Take some subset of these boundary points as the corridor
  // Find the closest pt on the boundary
  int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currRearPos);
  index = TrafficUtils::getClosestPtIndex(leftBound1, currRearPos);
  for (int ii = index; ii < (int) leftBound1.size(); ii++) {
    leftBound.push_back(leftBound1[ii]);
  }
  
  index = TrafficUtils::insertProjPtInBoundary(rightBound1, currRearPos);
  index = TrafficUtils::getClosestPtIndex(rightBound1, currRearPos);
  for (int ii = index; ii < (int) rightBound1.size(); ii++) {
    rightBound.push_back(rightBound1[ii]);
  }
  
  // Specify the velocity profile
  velIn = currSegment.maxSpeedLimit;
  velOut = currSegment.maxSpeedLimit;
  acc = 0;

  // specify the ocpParams final conditions - lower bounds
  // want to define this position based on the corridor?
  PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

  //make sure our exit point to plan to is in our current lane
  if (reverse) {
    point2 exitPt, exitPtOpp;
    localMap->getWaypoint(exitPt, exitPtLabel);
    localMap->getLaneCenterPoint(exitPtOpp, desiredLane, exitPt, 0);
    localMap->getClosestPointID(exitPtLabel, exitPtOpp);
  }
  
  // The only difference between a Road Region and Approach Intersection Safety is here
  int FCPos_Err = 0;
  double distanceToExitPt, distanceToCorrExitPt;
  point2 exitPt, corrExitPt;
  localMap->getWaypoint(exitPt, exitPtLabel);
  double heading;
  if (traffState->getType() == TrafficStateFactory::ROAD_REGION) {
    // Changed this for road region so that we only plan as far as the map goes.   
    corrExitPt.set((rightBound.back().x+leftBound.back().x)/2,(rightBound.back().y+leftBound.back().y)/2);
    point2arr centerline;
    LaneLabel laneLabel(exitPtLabel.segment, exitPtLabel.lane);
    localMap->getLaneCenterLine(centerline, laneLabel);
    localMap->getDistAlongLine(distanceToExitPt, centerline, exitPt, currFrontPos);
    localMap->getDistAlongLine(distanceToCorrExitPt, centerline, corrExitPt, currFrontPos);
    
    cout << "exit waypt label = " << exitPtLabel << endl;
    cout << "exitPt = " << exitPt << endl;
    cout << "CorrExitPt = " <<  corrExitPt << endl;
    cout << "distToExit along the lane = " << distanceToExitPt << endl;
    cout << "distToCorrExit along the lane = " << distanceToCorrExitPt << endl;

    // TODO: NOEL need to fix this - convoluted rndf causes this projection into the lane since the lane loops around on itself.
    if (1) {
      cout << "using exit pt for FC_pos"  << endl;
      FC_finalPos.set(exitPt);
      localMap->getHeading(heading, exitPtLabel);  
    } else {
      FC_finalPos.set(corrExitPt);
      cout << "using last pt in range for FC_pos"  << endl;
      localMap->getHeading(heading, FC_finalPos);
    }
    
  } else if (traffState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY) {
    FC_finalPos.set(exitPt);
    localMap->getHeading(heading, exitPtLabel);
  }
        
  if (FCPos_Err != 0) {
    cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
    return (FCPos_Err);
  }
  FC_velMin = 0;
  
  if (reverse) {
    heading += M_PI;
    if (heading > (2*M_PI))
      heading -= (2*M_PI);
  }

  FC_headingMin = heading;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = heading;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // set the final conditions in the ocpspecs - lower bounds
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);
  
  return 0;
}

int CorridorGen::makeCorridorTurn(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  bool m_verbose = true;
  bool m_debug = false;

  if ((m_verbose) || (m_debug)) {
    cout << "In CorridorGen::makeCorridorTurn " << endl;
  }
    
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  //plan over two segments
  SegGoals segment1 = planHoriz.getSegGoal(0);
  SegGoals segment2 = planHoriz.getSegGoal(1);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  //point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // Set of boundary points due to intersection lane
  //PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  //PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
  //int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
  //if (interBoundErr != 0) {
  //  cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
  //  return (interBoundErr);
  // }
        
  //get entry and exit lane labels
  LaneLabel laneLabel1(segment2.entrySegmentID, segment2.entryLaneID);
  LaneLabel laneLabel(segment1.exitSegmentID, segment1.exitLaneID);
  PointLabel ptLabel1(segment2.entrySegmentID, segment2.entryLaneID, segment2.entryWaypointID);
  PointLabel ptLabel(segment1.exitSegmentID, segment1.exitLaneID, segment1.exitWaypointID);
  point2 entryPos;
  int entryErr = localMap->getWaypoint(entryPos, ptLabel1);
  if (entryErr != 0) {
    cerr << "CorridorGen.cc: could not get entry waypoint" << endl;
    return entryErr;
  }

  point2 exitPos;
  entryErr = localMap->getWaypoint(exitPos, ptLabel);
  if (entryErr != 0) {
    cerr << "CorridorGen.cc: could not get exit waypoint" << endl;
    return entryErr;
  }

  int boundErr = localMap->getLeftBound(leftBound,laneLabel);
  boundErr = localMap->getRightBound(rightBound,laneLabel);
  boundErr = localMap->getLeftBound(leftBound1,laneLabel1);
  boundErr = localMap->getRightBound(rightBound1,laneLabel1);
  
  if (boundErr!=0){
    cerr << "CorridorGen.cc: exit lane read from map error" << endl;
    return boundErr;
  }     

  ofstream os("output.dat");
  os << leftBound << "---" << endl << rightBound << "---" << endl << leftBound1 << "---" << endl << rightBound1 << "---" << endl;

  point2 exitpt_left = leftBound.back();
  point2 exitpt_right = rightBound.back();
  point2 enterpt_left = leftBound1[0];
  point2 enterpt_right = rightBound1[0];
        
  double distleft = exitpt_left.dist(enterpt_left);
  double distright = exitpt_right.dist(enterpt_right);

  if (distleft<distright){
    //need to make sure intersection point is in a good place
    line2 lineA(rightBound[rightBound.size()-2],exitpt_right);
    line2 lineB(enterpt_right,rightBound1[1]);
    point2 inter = lineA.intersect(lineB);
    if (inter.dist(exitpt_right) > distright || inter.dist(enterpt_right) > distright)
      inter = (exitpt_right+enterpt_right)/2;
    point2 mid = (exitpt_left+enterpt_left)/2;
    leftBound.push_back(mid);
    leftBound.connect(leftBound1);
    rightBound.push_back(inter);
    rightBound.connect_intersect(rightBound1);
  } 
  else{
    line2 lineA(leftBound[leftBound.size()-2],exitpt_left);
    line2 lineB(enterpt_left,leftBound1[1]);
    point2 inter = lineA.intersect(lineB);
    if (inter.dist(exitpt_left) > distleft || inter.dist(enterpt_left) > distleft)
      inter = (exitpt_left+enterpt_left)/2;
    point2 mid = (exitpt_right + enterpt_right)/2;
    leftBound.push_back(inter);
    leftBound.connect(leftBound1);
    rightBound.push_back(mid);
    rightBound.connect(rightBound1);
  }


  // Specify the velocity profile
  velIn = segment1.maxSpeedLimit;
  velOut = segment2.maxSpeedLimit;
  acc = 0;

  // Specify the ocpParams final conditions - lower bounds
  // Do we want to define this position based on the corridor?
  //  PointLabel exitPtLabel(nextSegGoal.exitSegmentID, nextSegGoal.exitLaneID, nextSegGoal.exitWaypointID);
  //int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
  //if (FCPos_Err != 0) {
  //    cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
  //    return (FCPos_Err);
  //}
  FC_finalPos.set((rightBound.back().x+leftBound.back().x)/2,
		  (rightBound.back().y+leftBound.back().y)/2);
  FC_velMin = 0;
  FC_headingMin = -M_PI;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = M_PI;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector < double >velProfile;
  velProfile.push_back(velIn);        // entry pt
  velProfile.push_back(velOut);       // exit pt
  //  corr.setVelProfile(velProfile);
  // corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  os << endl;
  for (int i=0; i<leftBound.size(); i++)
  {
    //    os << leftBound[i] << endl << rightBound[i] << endl;
  }
  os << endl;
  os << exitPos << endl << entryPos << endl;
  os << FC_finalPos << endl;

  return 0;
}

int CorridorGen::makeCorridorZone(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  bool m_verbose = false;
  bool m_debug = false;

  if ((m_verbose) || (m_debug)) {
    cout << "In LaneKeeping::determineCorridor " << endl;
  }
    
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // We want to plan for current segment only
  // TODO: want to do this properly in the future
        
  PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
  if ((m_verbose) || (m_debug)) {
    cout << "exit waypt label = " << exitWayptLabel << endl;
  }
        
  point2 exitWaypt;
  int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
  if (wayptErr != 0) {
    cerr << "LaneKeeping.cc: waypt read from map error" << endl;
    return (wayptErr);
  }

  if ((m_verbose) || (m_debug)) {
    cout << "exit waypt pos = " << exitWaypt << endl;
  }
        
  // Get our current position
  if ((m_verbose) || (m_debug)) {
    cout << "current pos = " << currFrontPos << endl;
  }
        
  // Create a corridor of LaneWidth from current position to entry point
  double corrHalfWidth = 3;
  double theta = atan2(exitWaypt.y - currFrontPos.y, exitWaypt.x - currFrontPos.x);
  point2 temppt;
        
  // Left Boundary
  // pt 1 on left boundary
  temppt.x = currFrontPos.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = currFrontPos.y + corrHalfWidth * sin(theta + M_PI / 2);
  leftBound.push_back(temppt);
  // pt 2 on left boundary
  temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
  temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
  leftBound.push_back(temppt);
  // Right Boundary
  // pt 1 on right boundary
  temppt.x = currFrontPos.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = currFrontPos.y + corrHalfWidth * sin(theta - M_PI / 2);
  rightBound.push_back(temppt);
  // pt 2 on right boundary
  temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
  temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
  rightBound.push_back(temppt);

  // TODO: Take some subset of these boundary points as the corridor

  // Specify the velocity profile
  velIn = 3;
  velOut = 3;
  acc = 0;

  // specify the ocpParams final conditions - lower bounds
  FC_finalPos = exitWaypt;
  FC_velMin = 0;
  FC_headingMin = -M_PI;  // unbounded
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = M_PI;   // unbounded
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  vector < double >velProfile;
  velProfile.push_back(velIn);        // entry pt
  velProfile.push_back(velOut);       // exit pt
  //  corr.setVelProfile(velProfile);
  // corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  return 0;
}

int CorridorGen::makeCorridorLaneChange(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, LaneLabel destLane, point2 startPos, int reverse)
{
  bool m_verbose = true;
  bool m_debug = false;

  if ((m_verbose) || (m_debug)) {
    cout << "Making LaneChange corridor " << endl;
    cout << "Reverse value: " << reverse << endl;
  }
    
  point2arr leftBoundCur, rightBoundCur, leftBoundDest, rightBoundDest;
  SegGoals currSegment = planHoriz.getSegGoal(0);

  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);

  //get current lane label
  LaneLabel currLane;
  int laneErr = localMap->getLane(currLane, currRearPos);

  if (laneErr == -1) //map read error
    currLane = destLane;
  if (currLane == destLane)
    return makeCorridorLane(corr, vehState, traffState, planHoriz, localMap, 50,(reverse == 1), destLane);

  double laneChangeTime = 6; //seconds
  double laneChangeLength = currSegment.maxSpeedLimit * laneChangeTime;

  //is there an obstacle?
  // SVEN: double obs = localMap->getObstacleDist(startPos, 0);
  double obs = TrafficUtils::getNearestObsDist(localMap, vehState, currLane);
  if (obs > 0) {
    cout << "Adjusting lane change for obstacle in " << obs << " meters" << endl;
    if (laneChangeLength > (obs - 10))
      laneChangeLength = (obs-10);
    if (laneChangeLength < 8)
      laneChangeLength = 8;
  }

  cout << "laneChangeLength = " << laneChangeLength << endl;

  int boundErr = 0;
  int boundErr1 = 0;

  //current lane boundary
  if (reverse == -1)
    boundErr = localMap->getBoundsReverse(leftBoundCur,rightBoundCur,currLane,startPos,laneChangeLength+30);
  else
    boundErr = localMap->getBounds(leftBoundCur,rightBoundCur,currLane,startPos,laneChangeLength+30);

  //lane change boundary
  if (reverse == 1)
    boundErr1 = localMap->getBoundsReverse(leftBoundDest,rightBoundDest,destLane,startPos,laneChangeLength+30);
  else
    boundErr1 = localMap->getBounds(leftBoundDest,rightBoundDest,destLane,startPos,laneChangeLength+30);

  if (boundErr!=0){
    cerr << "CorridorGen.cc: current lane read from map error" << endl;
    return boundErr;
  }     
  if (boundErr1!=0){
    cerr << "CorridorGen.cc: destination lane read from map error" << endl;
    return boundErr1;
  }

  point2arr nearBound(rightBoundCur);
  point2arr midBound(leftBoundCur);
  point2arr farBound1(leftBoundDest);
  bool leftChange = true;

  //are we changing lanes to the right?
  if (leftBoundDest[0].dist(rightBoundCur[0]) < rightBoundDest[0].dist(leftBoundCur[0]))
  {
    nearBound = leftBoundCur;
    midBound = rightBoundCur;
    farBound1 = rightBoundDest;
    leftChange = false;
  }

  point2arr farBound = farBound1;

  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;

  //  for (int i=0;i<midBound.size();i++)
  //  farBound.push_back(farBound1.project(midBound[i]));
  TrafficUtils::alignBoundaries(midBound,farBound);

  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;

  TrafficUtils::alignBoundaries(midBound,farBound);
  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;

  //find start of lane change
  int indNear = TrafficUtils::insertProjPtInBoundary(nearBound, startPos);
  indNear = TrafficUtils::getClosestPtIndex(nearBound, startPos);
  int indMid = TrafficUtils::insertProjPtInBoundary(midBound, startPos);
  indMid = TrafficUtils::getClosestPtIndex(midBound, startPos);
  int indFar = TrafficUtils::insertProjPtInBoundary(farBound, midBound[indMid]);
  indFar = TrafficUtils::getClosestPtIndex(farBound, startPos);

  cout << "ind(Near,Mid,Far): " << indNear << " " << indMid << " " << indFar << endl;


  //add points at important distance, to give lane change maneuver some width
  double insertDist = 5;

  TrafficUtils::insertPtAtDistance(nearBound,indNear,laneChangeLength-0.1);
  TrafficUtils::insertPtAtDistance(midBound,indMid,laneChangeLength-0.1);
  TrafficUtils::insertPtAtDistance(farBound,indFar,laneChangeLength-0.1);

  TrafficUtils::insertPtAtDistance(nearBound,indNear,insertDist);
  TrafficUtils::insertPtAtDistance(midBound,indMid,insertDist);
  TrafficUtils::insertPtAtDistance(farBound,indFar,insertDist);

  TrafficUtils::insertPtAtDistance(nearBound,indNear,laneChangeLength+insertDist);
  TrafficUtils::insertPtAtDistance(midBound,indMid,laneChangeLength+insertDist);
  TrafficUtils::insertPtAtDistance(farBound,indFar,laneChangeLength+insertDist);

  //we hope that indNear == indMid

  //the cumulative distance from the starting position
  double lcDist = 0;//startPos.dist(nearBound[indNear]);

  srand(time(0));
  double foo = (double)(rand() & 1);

  //weighted average transition between the two lanes
  //iterate from lane change start to current location
  for (int i=indMid; i<midBound.size()-1; i++)
  {
    double alpha = lcDist / laneChangeLength;
    double alpha2 = (lcDist - insertDist) / laneChangeLength;
    lcDist += midBound[i].dist(midBound[i+1]);

    //    alpha = foo;
    //    alpha2 = foo;

    if (alpha2 < 0) alpha2 = 0;
    if (alpha > 1) alpha = 1;
    if (alpha2 > 1) alpha2 = 1;

    nearBound[i] = (alpha2*midBound[i] + (1-alpha2)*nearBound[i]);
    midBound[i] = (alpha*farBound[i] + (1-alpha)*midBound[i]);
  }

  nearBound.back() = midBound.back();
  midBound.back() = farBound.back();

  cout << "Near, Mid, Far size: " << nearBound.size() << "," << midBound.size() << "," << farBound.size() << endl;
  //  cout << "Near Back: " << nearBound.back() << ", Mid Back: " << midBound.back() << endl;

  // Specify the velocity profile
  velIn = currSegment.maxSpeedLimit;
  velOut = currSegment.maxSpeedLimit;
  acc = 0;

  FC_finalPos = 0.5*(nearBound.back() + midBound.back());

  double heading;
  localMap->getHeading(heading,FC_finalPos);

  if (reverse == 1) {
    heading += M_PI;
    if (heading > (2*M_PI))
      heading -= (2*M_PI);
  }

  // Specify the ocpParams final conditions - lower bounds
  // Do we want to define this position based on the corridor?
  FC_velMin = 0;
  FC_headingMin = heading;
  FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
  FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
  // specify the ocpParams final conditions - upper bounds
  FC_velMax = velOut;
  FC_headingMax = heading;
  FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
  FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

  // Assign lines to corridor variables
  if (leftChange) {
    corr.addPolyline(midBound);
    corr.addPolyline(nearBound);
  }
  else {
    corr.addPolyline(nearBound);
    corr.addPolyline(midBound);
  }

  // Set the velocity profile - need to think this through some
  vector < double >velProfile;
  velProfile.push_back(velIn);        // entry pt
  velProfile.push_back(velOut);       // exit pt
  //  corr.setVelProfile(velProfile);
  // corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  return 0;
}
