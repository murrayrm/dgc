#include "SlowDown.hh"


SlowDown::SlowDown(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
, m_desDecc(-0.5)
{

}

SlowDown::~SlowDown()
{

}

int SlowDown::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    point2arr leftBound, rightBound, leftBound1, rightBound1;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    double velIn, velOut, acc;
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 FC_finalPos;
    double IC_velMin, IC_velMax;
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

    switch (traffState->getType()) {
    
    /* We slow down in front of an obstacle, this should automatically be handled
     * by the automatic velocity re-profiler */ 
    case TrafficStateFactory::ROAD_REGION:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "Slowdown: Road Region" << endl;
        }
    
        // We want to plan for current segment only
        // TODO: want to use function that does not specify specific lane
        LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
        if (getBoundsErr != 0) {
            cerr << "LaneKeeping.cc: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        /* int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
        int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
        if (rightBoundErr!=0){
            cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
            return (rightBoundErr);
        }     
        if (leftBoundErr!=0){
            cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
            return (leftBoundErr); 
        } */

        // Insert a pt at the obstacle, and a distance of 10 m (m_distToObs from LaneKeepingToSlowDown) and with zero offset
        point2 tmppt = localMap->getObstaclePoint(currFrontPos, 10);
        int errInsLeftProj = TrafficUtils::insertProjPtInBoundary(leftBound1, tmppt);
        int errInsRightProj = TrafficUtils::insertProjPtInBoundary(rightBound1, tmppt);
        tmppt = localMap->getObstaclePoint(currFrontPos, 0);
        errInsLeftProj = TrafficUtils::insertProjPtInBoundary(leftBound1, tmppt);
        errInsRightProj = TrafficUtils::insertProjPtInBoundary(rightBound1, tmppt);

        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }

        // Specify the velocity profile
        // Need to specify the entry and exit vel
        // Need one more parameter: can set distance and calc acc, or the other way around
        // here I want to specify the acc, and calculate the distance function
        // TODO: think through the vel profile specification some more.
        double initVel = getInitialVelocity();
        if ((m_verbose) || (m_debug)) {
            cout << "SlowDown initial velocity = " << initVel << endl;
        }
        double cStateLength = -pow(initVel, 2) / (2 * m_desDecc);
        if ((m_verbose) || (m_debug)) {
            cout << "SlowDown corridor length = " << cStateLength << endl;
        }
        double distIntoControlState = calcDistFromInitPos(vehState);
        /* velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState; */
        // TODO: fix this /2
        if ((m_verbose) || (m_debug)) {
            cout << "SlowDown velIn = " << velIn << endl;
        }
        velIn = initVel + ((0 - initVel) / cStateLength) * distIntoControlState;
        velOut = 0;
        acc = m_desDecc;

        // specify the ocpParams final conditions - lower bounds
        // want to define this position based on the corridor?
        PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
        if (FCPos_Err != 0) {
            cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FCPos_Err);
        }
        FC_velMin = 0;
        FC_headingMin = -M_PI;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = M_PI;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        IC_velMin = min(velIn, AliceStateHelper::getVelocityMag(vehState));
        IC_velMax = IC_velMin;
    }
        break;

    /* We slow down in front of a stopline */
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "Slowdown: Approach Intersection Safety" << endl;
        }
        
        // We want to plan for current segment only
        // TODO: want to use function that does not specify specific lane
        LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
        if (getBoundsErr != 0) {
            cerr << "LaneKeeping.cc: boundary read from map error" << endl;
            return (getBoundsErr);
        }
        
        /* int leftBoundErr = localMap->getLeftBound(leftBound1, laneLabel1);
        int rightBoundErr = localMap->getRightBound(rightBound1, laneLabel1);
        if (rightBoundErr!=0){
            cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
            return (rightBoundErr);
        }     
        if (leftBoundErr!=0){
            cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
            return (leftBoundErr);
        } */

        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currFrontPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currFrontPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currFrontPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }

        // Specify the velocity profile
        // Need to specify the entry and exit vel
        // Need one more parameter: can set distance and calc acc, or the other way around
        // here I want to specify the acc, and calculate the distance function
        // TODO: think through the vel profile specification some more.
        double initVel = getInitialVelocity();
        if ((m_verbose) || (m_debug)) {
            cout << "SlowDown initial velocity = " << initVel << endl;
        }
        double cStateLength = -pow(initVel, 2) / (2 * m_desDecc);
        if ((m_verbose) || (m_debug)) {
            cout << "SlowDown corridor length = " << cStateLength << endl;
        }
        double distIntoControlState = calcDistFromInitPos(vehState);
        // velIn = currSegment.maxSpeedLimit + ((0-currSegment.maxSpeedLimit)/cStateLength)*distIntoControlState;
        velIn = (initVel + ((0 - initVel) / cStateLength) * distIntoControlState);
        velOut = 0;
        acc = m_desDecc;

        // Specify the ocpParams final conditions - lower bounds
        // Want to define this position based on the corridor?
        PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        int FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
        if (FCPos_Err != 0) {
            cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FCPos_Err);
        }
        FC_velMin = 0;
        FC_headingMin = -M_PI;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = M_PI;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        IC_velMin = min(velIn, AliceStateHelper::getVelocityMag(vehState));
        IC_velMax = IC_velMin;
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "Slowdown: Intersection Stop" << endl;
        }
    
        // Want to plan over two segments
        SegGoals segment2 = planHoriz.getSegGoal(1);
        point2arr leftBound1, rightBound1, leftBound2, rightBound2;
        // Set of boundary points due to intersection lane
        PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
        if ((m_verbose) || (m_debug)) {
            cout << "Intersection entry waypt id = " << ptLabelIn << endl;
        }
        PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        if ((m_verbose) || (m_debug)) {
            cout << "Intersection exit waypt id = " << ptLabelOut << endl;
        }
        double range = 50;
        int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
        if (interBoundErr != 0) {
            cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
            return (interBoundErr);
        }
        
        // Set of boudary pts due to lane after intersection
        /* LaneLabel laneLabel(segment2.entrySegmentID, segment2.entryLaneID);      
        int rightBoundErr = localMap->getRightBound(rightBound2, laneLabel); 
        int leftBoundErr = localMap->getLeftBound(leftBound2, laneLabel);
        if (rightBoundErr!=0){
            cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
            return rightBoundErr;
        }     
        if (leftBoundErr!=0){
            cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
            return leftBoundErr;
        } */
        
        // Add the boundaries together
        leftBound = leftBound1;
        rightBound = rightBound1;
        /* for (unsigned i=0; i<leftBound2.size(); i++){
            if (leftBound.back()!=leftBound2[i]){
                leftBound.push_back(leftBound2[i]);
            }
        }
        for (unsigned i=0; i<rightBound2.size(); i++){
            if (rightBound.back()!=rightBound2[i]){
                rightBound.push_back(rightBound2[i]);
            }
        } */

        // TODO: take some subset of these boundary points as the corridor

        // Specify the velocity profile
        velIn = 0;
        velOut = 0;
        acc = 0;

        // specify the ocpParams final conditions - lower bounds
        // want to define this position based on the corridor?
        PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
        if (FCPos_Err != 0) {
            cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FCPos_Err);
        }
        FC_velMin = 0;
        FC_headingMin = -M_PI;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = M_PI;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        IC_velMin = min(velIn, AliceStateHelper::getVelocityMag(vehState));
        IC_velMax = IC_velMin;
    }
        break;
        
    default:
    
        cerr << "SlowDown.cc: Undefined Traffic state" << endl;

    }
    
    if ((m_verbose) || (m_debug)) {
        cout << "leftBound.size()" << leftBound.size() << endl;
        cout << "rightBound.size()" << rightBound.size() << endl;
        cout << "velocity profile: in = " << velIn << " and out = " << velOut << endl;
        cout << "specified acceleration = " << acc << endl;
    }
    
    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // Set the velocity profile - need to think this through some
    //    vector < double >velProfile;
    //    velProfile.push_back(velIn);        // entry pt
    //    velProfile.push_back(velOut);       // exit pt
    //    corr.setVelProfile(velProfile);
    //    corr.setDesiredAcc(acc);

    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    corr.setOCPinitialCondLB(VELOCITY_IDX_C, IC_velMin);
    corr.setOCPinitialCondUB(VELOCITY_IDX_C, IC_velMax);

    return 0;
}
