#include "Point.hh"

Point::Point(double x, double y, double z) 
	: m_x(x)
	, m_y(y)
	, m_z(z)
{

}

Point::Point()
{  
}


Point::~Point(){  
}

double Point::getXCoordinate(){  
	return m_x;
}

double Point::getYCoordinate(){  
	return m_y;
}

double Point::getZCoordinate(){  
	return m_z;
}



