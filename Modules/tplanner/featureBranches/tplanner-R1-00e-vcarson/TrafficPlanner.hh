#ifndef TRAFFICPLANNER_HH_
#define TRAFFICPLANNER_HH_

#include "Corridor.hh"
#include "Conflict.hh"
#include "Segment.hh"
#include "state/ControlState.hh"
#include "state/ControlStateTransition.hh"
#include "state/TrafficState.hh"
#include "state/StateGraph.hh"


class TrafficPlanner {


public: 
 /** 
   *  Determine traffic state with respect to the segment that needs to be accomplished given the most recent sensed data. 
   */
   TrafficState determineTrafficState();

  /** 
   *  Given the traffic state passed, give the most likely control state to maintain. 
   */
   ControlState determineControlState(TrafficState trafficState);

  /** 
   *  Gets the set of possible transition control states given the control state in the argument. 
   */
   std::vector<ControlStateTransition> getTransitionControlStates(ControlState controlState);

  /** 
   *  Calculate the possible corridors based on the segment to be executed and the control state passed. 
   */
   std::vector<Corridor> calculateCorridors(ControlState controlState);

  /** 
   *  Given the corridor passed, determine the conflicts that may arise with respect to moving obstacles.  
   *  
   */
   Conflict evaluateDynamicConflicts(Corridor corridor, ControlState controlState);
  
  private : 
   	Corridor m_corridors;
	Segment m_segment;
	double m_entryPoint;
	double m_exitPoint;
    bool m_stopAtExit;
	bool m_ckpointAtExit;
	
	/** Current states */
	
	VehicleState m_currVehState;
	ControlState m_currControlState;
	TrafficState m_currTrafficState;
	Corridor m_currCorridor;
	
	/** State databases */ 
	StateGraph<ControlStates, ControlStatesTransition> m_controlGraph;
	StateGraph<TrafficStates,TrafficStatesTransition> m_trafficGraph;
	
	Vector m_conflicts;
    
 
#endif /*TRAFFICPLANNER_HH_*/
