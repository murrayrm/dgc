#ifndef PLANNINGHORIZON_HH_
#define PLANNINGHORIZON_HH_


#include "mapping/Road.hh" 
#include "SegGoals.hh" 
#include "mapping/Obstacle.hh" 
#include "mapping/Map.hh"
#include <list>
#include <vector>

class PlanningHorizon 
{

public: 

PlanningHorizon(SegGoals segGoals);
PlanningHorizon();
~PlanningHorizon();
std::vector<Road> getRoadObjects();
std::vector<Obstacle> getObstacles();
int getExitLaneId(SegGoals segGoal);
SegGoals getFirstSegGoal();
Line* getStopLine(int segmentId, int laneId);

private:

SegGoals m_segGoals; 
std::vector<Road> m_roads;
std::vector<Obstacle> m_obstacles;
std::vector<Line> m_lines;
Map* m_map;



};

#endif /*PLANNINGHORIZON_HH_*/
