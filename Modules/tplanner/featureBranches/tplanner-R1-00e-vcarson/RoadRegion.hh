#ifndef ROADREGION_HH_
#define ROADREGION_HH_

#include "TrafficState.hh"

// RoadRegion defines a region of road that does not include an intersection, or an approach to an intersection 

class RoadRegion : public TrafficState {

public: 

RoadRegion();
~RoadRegion();

};

#endif /*ROADREGION_HH_*/
