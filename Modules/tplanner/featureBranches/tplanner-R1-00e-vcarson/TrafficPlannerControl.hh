#ifndef TRAFFICPLANNERCONTROL_HH_
#define TRAFFICPLANNERCONTROL_HH_

#include "Corridor.hh"
#include "Conflict.hh"
#include "mapping/Segment.hh"
#include "TrafficStateEstimator.hh"
#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"


class TrafficPlannerControl {


public: 


TrafficPlannerControl(TrafficStateEstimator* estimator);
TrafficPlannerControl();

~TrafficPlannerControl();


void controlLoop();


/**   Given the traffic state passed, give the most likely control state to maintain. */
ControlState determineControlState(SegGoals segGoals);


/** 
*  Calculate the possible corridors based on the segment to be executed and the control state passed. 
*/
std::vector<Line> calculateCorridors(TrafficState traffState, ControlState controlState);


/** 
*  Given the corridor passed, determine the conflicts that may arise with respect to moving obstacles.  
*  
*/
Conflict evaluateDynamicConflicts(Corridor corridor, ControlState controlState);

void determinePlanningHorizon(SegGoals segGoals);

/** Choose best corridor */
Corridor evaluateCorridors();

ControlState getCurrentControlState();

ControlState chooseMostProbableControlState(std::vector<ControlStateTransition> transitions);

private : 

TrafficStateEstimator* m_traffStateEst;	
Map* m_localMap; 
Corridor m_corridor;
double m_entryPoint;
double m_exitPoint;
bool m_stopAtExit;
bool m_ckpointAtExit;

/** Current states */

VehicleState m_currVehState;
ControlState m_currControlState;
TrafficState m_currTrafficState;
Corridor m_currCorridor;
PlanningHorizon m_currPlanHorizon;

/** State databases */ 
StateGraph m_controlGraph;

std::vector<Conflict> m_conflicts;
TrafficStateEstimator* m_estimator;
};

#endif /*TRAFFICPLANNERCONTROL_HH_*/
