#include "TrafficStateTransition.hh"


TrafficStateTransition::TrafficStateTransition()
{
}

TrafficStateTransition::TrafficStateTransition(TrafficState* state1, TrafficState* state2)
{   
  m_trafState1 = state1;
  m_trafState2 = state2;

}

TrafficStateTransition::~TrafficStateTransition()
{   
  m_trafState1 = 0;;
  m_trafState2 = 0;
}

