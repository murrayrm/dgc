#ifndef ROADTOAPPROACHINTERSAFETY_HH_
#define ROADTOAPPROACHINTERSAFETY_HH_


#include "TrafficStateTransition.hh"
#include "TrafficUtils.hh"


class RoadToApproachInterSafety : public TrafficStateTransition
{

public: 

RoadToApproachInterSafety(TrafficState* state1, TrafficState* state2);
RoadToApproachInterSafety();
~RoadToApproachInterSafety();

double meetTransitionConditions(TrafficState currTrafficState, Map* localMap,VehicleState vehicleState);

private: 

double m_distSafeStop; //in meters
TrafficUtils m_traffUtils;

};
#endif /*ROADTOAPPROACHINTERSAFETY_HH_*/

