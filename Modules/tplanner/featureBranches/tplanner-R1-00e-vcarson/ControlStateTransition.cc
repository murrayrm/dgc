#include "ControlStateTransition.hh"



ControlStateTransition::ControlStateTransition() 
{
	m_traffUtils = new TrafficUtils();
}

ControlStateTransition::ControlStateTransition(ControlState* state1, ControlState* state2)
: m_controlState1(state1)
, m_controlState2(state2) 
{
	m_traffUtils = new TrafficUtils();

}

ControlStateTransition::~ControlStateTransition()
{
  m_controlState1 = 0;
  m_controlState2 = 0; 

}

ControlState ControlStateTransition::getControlStateTo()
{
	return *m_controlState2;
}


