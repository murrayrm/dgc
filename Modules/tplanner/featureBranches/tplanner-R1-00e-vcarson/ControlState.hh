#ifndef CONTROLSTATE_HH_
#define CONTROLSTATE_HH_

#include "Corridor.hh"
#include "mapping/Map.hh"
#include "state/VehicleState.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "state/State.hh" 

//TODO fix the Vertex here 

class ControlState : public State {


public: 

ControlState();
~ControlState();
Corridor* determineCorridor(VehicleState vehState, TrafficState traffState, PlanningHorizon planHoriz, Map* map);
int getType();


private: 
 int m_cStateType; //FIX typedef
};
#endif /*CONTROLSTATE_HH_*/
