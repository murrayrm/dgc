#include "TrafficStateEstimator.hh"

#include "ClearRegion.hh"
#include "RoadToApproachInterSafety.hh"
#include "RoadRegion.hh"
#include "ApproachInterSafety.hh"


TrafficStateEstimator::TrafficStateEstimator() 
{
}

TrafficStateEstimator::TrafficStateEstimator(Map* roadObsMap) 
:m_localMap(roadObsMap)
{
	TrafficStateEstimator();
}

TrafficStateEstimator::~TrafficStateEstimator() 
{
	delete m_localMap;
}

void TrafficStateEstimator::determineTrafficState() {

	//TOOD given sensing info determineTrafficState...
	//Given current traffic state, query graph get  possible transitions list  
	//After a query to the map object, vehicle state, determine possible transitions 
	//for all possible transitions calculate the probability of meeting the transition conditions 
	VehicleState vehState; 
	
	std::vector<TrafficStateTransition> transitions;
	//std::vector<TrafficStateTransition> transitions = m_trafficGraph.getOutStateTransitions(m_currTrafficState); 
	unsigned int i = 0;
	
	for (i; i < transitions.size(); i++) {
		transitions[i].meetTransitionConditions(m_currTrafficState, m_localMap, vehState);
	}

	m_currTrafficState = TrafficStateEstimator::chooseMostProbableTrafficState(transitions);
	
}


TrafficState TrafficStateEstimator::getCurrentTrafficState() {
	return m_currTrafficState; 
}

void TrafficStateEstimator::buildTrafficStateGraph() 
{	
	RoadToApproachInterSafety* raTransSafe = 
	new RoadToApproachInterSafety(new RoadRegion(), new ApproachInterSafety());

	StateGraph clearRegionSubgraph;
	clearRegionSubgraph.addStateTransition(*raTransSafe);

}

TrafficState TrafficStateEstimator::chooseMostProbableTrafficState(std::vector<TrafficStateTransition> transitions) {

//TODO finish method 		
std::vector<TrafficStateTransition> probOneTrans;
std::vector<TrafficStateTransition> probAboveThresh;
std::vector<TrafficStateTransition> probBelowThresh;

double transProb = 0;
unsigned int i = 0;
	
for (i=0; i < transitions.size(); i++) {
transProb = transitions[i].getUncertainty();
if(1 == transProb) {
probOneTrans.push_back(transitions[i]);
} else if ( m_probThresh < transProb) {
probAboveThresh.push_back(transitions[i]);		
} else if ( m_probThresh >= transProb) {
probBelowThresh.push_back(transitions[i]);		
}
}

 State* state = new State ();

 TrafficState*  ts = static_cast<TrafficState*> (state);
 return *(ts);
 //return *(TrafficState)(probOneTrans.front().getFinalState());


}
