#ifndef TRAFFICSTATEESTIMATOR_HH_
#define TRAFFICSTATEESTIMATOR_HH_


#include "mapping/Map.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"
#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"

class TrafficStateEstimator {

public: 

TrafficStateEstimator();

//TODO this constructor changes after we implement with css 
TrafficStateEstimator(Map* roadObsMap);

~TrafficStateEstimator();

/** 
*  Determine traffic state with respect to the segment that needs to be accomplished given the most recent sensed data. 
*/
void determineTrafficState();

TrafficState getCurrentTrafficState();

private : 

void buildTrafficStateGraph();
TrafficState chooseMostProbableTrafficState(std::vector<TrafficStateTransition> transitions);

Map* m_localMap;

TrafficState m_currTrafficState;
StateGraph m_trafficGraph;
double m_probThresh;

};

#endif /*TRAFFICSTATEESTIMATOR_HH_*/
