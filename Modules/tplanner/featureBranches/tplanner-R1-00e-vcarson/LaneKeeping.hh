#ifndef LANEKEEPING_HH_
#define LANEKEEPING_HH_

#include "ControlState.hh"


class LaneKeeping : public ControlState {


public: 

LaneKeeping();
~LaneKeeping();
Corridor* determineCorridor(VehicleState vehState, TrafficState traffState, PlanningHorizon planHoriz, Map* map);

private: 

};
#endif /*LANEKEEPING_HH_*/
