#ifndef TRAFFICSTATE_HH_
#define TRAFFICSTATE_HH_

#include "TrafficStateFactory.hh"
#include "state/State.hh"

class TrafficState : public State 
{

public: 

TrafficState();
TrafficState(int stateId);
~TrafficState();
int getTrafficStateId();
//TrafficStateFactory::TrafficStateType getType();
TrafficStateFactory::TrafficStateType getType();


private: 
int m_stateId;
TrafficStateFactory::TrafficStateType m_type;
//int m_type; 
};

#endif /*TRAFFICSTATE_HH_*/
