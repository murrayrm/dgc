#ifndef TRAFFICSTATEFACTORY_HH_
#define TRAFFICSTATEFACTORY_HH_


class TrafficStateFactory {


public: 

typedef enum { CLEARREGION, APPROACHINTERSAFETY } TrafficStateType; 

TrafficStateFactory();
~TrafficStateFactory();
void createTrafficStates();
int getNextTrafficStateId();

private: 
int m_trafficStateId;

};

#endif /*TRAFFICSTATEFACTORY_HH_*/
