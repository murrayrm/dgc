#include "Corridor.hh"
GisCoordLatLon latlon;
GisCoordUTM utm;

Corridor::Corridor() 
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
 
}


Corridor::~Corridor() 
{
  
}

  
GeometricConstraints* Corridor::getGeometricConstraints() 
{
return new GeometricConstraints(); 
}

  
int Corridor::getControlStateId() {

return m_controlStateId;
  
}


void Corridor::convertToRddf()
{
  RDDFData rddfData;
  // This code is inherited from the old tplanner - was created by Julia Braman

  int i, j, k, l1, l2 ;
  vector<double> rddfx, rddfy, rddfr;

  //TODO: Is this laneWidth really necessary?
  double laneWidth = 12;

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% FIND CLOSEST SEGMENTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  bool error = false ;
  vector<Point> line1 = m_polylines[0].getPoints();
  vector<Point> line2 = m_polylines[1].getPoints();
  int numline1seg = line1.size();
  int numline2seg = line2.size();
  double l1p[numline1seg][2], l2p[numline2seg][2] ;
  for( i = 0 ; i < numline1seg ; i++ ) {
    l1p[i][0] = line1[i].getXCoordinate();
    l1p[i][1] = line1[i].getYCoordinate();
  }
  int numline1pts = numline1seg;

  for( i = 0; i < numline2seg ; i++ ) {
    l2p[i][0] = line2[i].getXCoordinate();
    l2p[i][1] = line2[i].getYCoordinate();
  }
  int numline2pts = numline2seg;

  // Check that end points of two lines are not the same (full lane obstacle)
  if( (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1]) ){
    error = true ;
    cerr << "ERROR convRDDF: (l1p[numline1seg][0] == l2p[numline2seg][0]) && (l1p[numline1seg][1] == l2p[numline2seg][1])" << endl;
  }

  double mpt[2] = {0, 0} ;
  double r = 0 ;

  // Line 1
  double min1, min2, dist, xdiff, ydiff ;
  int min1num, min2num;
  bool skip = false ;
  double a[2], b[2], maga, magb, adotb, th, check[4], check1[4], check2[4] ;
  double cpt1[2], cpt2[2];
  double cpt[2], pnt[2] ;
  double rddfln1[numline1pts][3], rddfln2[numline2pts][3] ;
  for( i = 0 ; i < numline1pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0; j < numline2pts; j++ ) {
      xdiff = l1p[i][0] - l2p[j][0] ;
      ydiff = l1p[i][1] - l2p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
	if( dist < min2 ) {
	  min1 = min2 ;
	  min1num = min2num ;
	  min2 = dist ;
	  min2num = j ;
	} else {
	  min1 = dist ;
	  min1num = j ;
	}
      }
    }
    skip = false ;
    if( (i == 0) || (i == (numline1pts-1))) {
      if( i == 0 ) {
	a[0] = l1p[0][0] - l2p[0][0] ;
	a[1] = l1p[0][1] - l2p[0][1] ;
	b[0] = l2p[1][0] - l2p[0][0] ;
	b[1] = l2p[1][1] - l2p[0][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
	a[0] = l1p[numline1seg][0] - l2p[numline2seg][0] ;
	a[1] = l1p[numline1seg][1] - l2p[numline2seg][1] ;
	b[0] = l2p[numline2seg-1][0] - l2p[numline2seg][0] ;
	b[1] = l2p[numline2seg-1][1] - l2p[numline2seg][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
	skip = true ;
    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l1p[i][0] ;
      pnt[1] = l1p[i][1] ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num)) {
	//fprintf( stderr, "min1num = %i, minnum2 = %i\n" , min1num, min2num) ;
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check[k] = l2p[min1num][k] ;
	    else
	      check[k] = l2p[min1num+1][k-2];
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check[k] = l2p[min2num][k] ;
	    else
	      check[k] = l2p[min2num+1][k-2] ;
	  }
	}
	closept( check, pnt, cpt ) ;
	midpt( cpt, pnt, mpt ) ;
	radius( mpt, pnt, cpt, r ) ;
      } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l2p[min1num][k] ;
	      check2[k] = l2p[min2num-1][k] ;
	    } else {
	      check1[k] = l2p[min1num+1][k-2] ;
	      check2[k] = l2p[min2num][k-2] ;
	    }
	  }
	} else { 
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) {
	      check1[k] = l2p[min2num][k] ;
	      check2[k] = l2p[min1num-1][k] ;
	    } else {
	      check1[k] = l2p[min2num+1][k-2] ;
	      check2[k] = l2p[min1num][k-2] ;
	    }
	  }
	}
	closept(check1, pnt, cpt1) ;
	closept(check2, pnt, cpt2) ;
	double xdiff1 = cpt1[0] - pnt[0] ;
	double ydiff1 = cpt1[1] - pnt[1] ;
	double xdiff2 = cpt2[0] - pnt[0];
	double ydiff2 = cpt2[1] - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1[0] ;
	  cpt[1] = cpt1[1] ;
	} else {
	  cpt[0] = cpt2[0] ;
	  cpt[1] = cpt2[1] ;
	}
	midpt( cpt, pnt, mpt ) ;
	radius( mpt, pnt, cpt, r ) ;
      } else {
	if( min1num < (numline2pts-1) ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check1[k] = l2p[min1num][k] ;
	    else
	      check1[k] = l2p[min1num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check1[k] = l2p[min1num-1][k] ;
	    else 
	      check1[k] = l2p[min1num][k-2] ;
	  }
	}
	if( min2num < (numline2pts-1) ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l2p[min2num][k] ;
	    else
	      check2[k] = l2p[min2num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check2[k] = l2p[min2num-1][k] ;
	    else 
	      check2[k] = l2p[min2num][k-2] ;
	  }
	}
	closept(check1, pnt, cpt1) ;
	closept(check2, pnt, cpt2) ;
	double xdiff1 = cpt1[0] - pnt[0] ;
	double ydiff1 = cpt1[1] - pnt[1] ;
	double xdiff2 = cpt2[0] - pnt[0];
	double ydiff2 = cpt2[1] - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ){
	  cpt[0] = cpt1[0] ;
	  cpt[1] = cpt1[1] ;
	} else {
	  cpt[0] = cpt2[0] ;
	  cpt[1] = cpt2[1] ;
	}
	midpt( cpt, pnt, mpt ) ;
	radius( mpt, pnt, cpt, r ) ;
      }
    }
    if( r > laneWidth / 2 )
      r = laneWidth / 2 ;

    rddfln1[i][0] = mpt[0] ;
    rddfln1[i][1] = mpt[1] ;
    rddfln1[i][2] = r ;
    //printf("rddfln1[%d] (x,y) = (%3.6f,%3.6f)\n", i, rddfln1[i][0], rddfln1[i][1]);
  }

  // Line 2
  for( i = 0 ; i < numline2pts ; i++ ) {
    min1 = BIGNUMBER ;
    min2 = BIGNUMBER ;
    min1num = 0 ;
    min2num = 0 ;
    for( j = 0 ; j < numline1pts ; j++ ) {
      xdiff = l2p[i][0] - l1p[j][0] ;
      ydiff = l2p[i][1] - l1p[j][1] ;
      dist = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
      if( dist < min1 ) {
	if( dist < min2 ) {
	  min1 = min2 ;
	  min1num = min2num ;
	  min2 = dist ;
	  min2num = j ;
	} else {
	  min1 = dist ;
	  min1num = j ;
	}
      }
    }
    skip = false ;
    if( (i == 0) || (i == numline2seg) ) {
      if( i == 0 ) { 
	a[0] = l2p[0][0] - l1p[0][0] ;
	a[1] = l2p[0][1] - l1p[0][1] ;
	b[0] = l1p[1][0] - l1p[0][0] ;
	b[1] = l1p[1][1] - l1p[0][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      } else {
	a[0] = l2p[numline2pts-1][0] - l1p[numline1pts-1][0] ;
	a[1] = l2p[numline2pts-1][1] - l1p[numline1pts-1][1] ;
	b[0] = l1p[numline1pts-2][0] - l1p[numline1pts-1][0] ;
	b[1] = l1p[numline1pts-2][1] - l1p[numline1pts-1][1] ;
	maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
	magb = sqrt( b[0] * b[0] + b[1] * b[1] ) ;
	adotb = a[0] * b[0] + a[1] * b[1] ;
      }
      th = acos( adotb/(maga * magb) ) ;
      if( th > (PI / 2 + EPS) ) 
	skip = true ;
    }
    if( skip ) {
      mpt[0] = 0 ;
      mpt[1] = 0 ;
      r = 0 ;
    } else {
      pnt[0] = l2p[i][0] ;
      pnt[1] = l2p[i][1] ;
      //    fprintf( stderr, "min1 = %i, min2 = %i\n", min1num, min2num ) ;
      if( (min1num + 1 == min2num) || (min2num + 1 == min1num) ) {
	if( min1num < min2num ) {
	  for( k = 0 ; k < 4 ; k++ ){
	    if( k < 2 )
	      check[k] = l1p[min1num][k] ;
	    else
	      check[k] = l1p[min1num+1][k-2];
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 )
	      check[k] = l1p[min2num][k] ;
	    else
	      check[k] = l1p[min2num+1][k-2];
	  }
	}
	closept( check, pnt, cpt ) ;
	midpt( cpt, pnt, mpt ) ;
	radius( mpt, pnt, cpt, r ) ;
      } else if( (min1num + 2 == min2num) || (min2num + 2 == min1num)) {
	if( min1num < min2num) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l1p[min1num][k] ;
	      check2[k] = l1p[min2num-1][k] ;
	    } else {
	      check1[k] = l1p[min1num+1][k-2] ;
	      check2[k] = l1p[min2num][k-2] ;
	    }
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if ( k < 2 ) {
	      check1[k] = l1p[min2num][k] ;
	      check2[k] = l1p[min1num-1][k] ;
	    } else {
	      check1[k] = l1p[min2num+1][k-2] ;
	      check2[k] = l1p[min1num][k-2] ;
	    }
	  }
	}
	closept(check1, pnt, cpt1) ;
	closept(check2, pnt, cpt2) ;
	double xdiff1 = cpt1[0] - pnt[0] ;
	double ydiff1 = cpt1[1] - pnt[1] ;
	double xdiff2 = cpt2[0] - pnt[0];
	double ydiff2 = cpt2[1] - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1[0] ;
	  cpt[1] = cpt1[1] ;
	} else {
	  cpt[0] = cpt2[0] ;
	  cpt[1] = cpt2[1] ;
	}
	midpt( cpt, pnt, mpt ) ;
	radius( mpt, pnt, cpt, r ) ;
      } else {
	if( min1num < numline1seg ) {
	  for( k = 0 ; k < 4 ; k++ ){
	    if( k < 2 ) 
	      check1[k] = l1p[min1num][k] ;
	    else
	      check1[k] = l1p[min1num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check1[k] = l1p[min1num-1][k] ;
	    else
	      check1[k] = l1p[min1num][k-2] ;
	  }
	}
	if( min2num < numline1seg ) {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l1p[min2num][k] ;
	    else
	      check2[k] = l1p[min2num+1][k-2] ;
	  }
	} else {
	  for( k = 0 ; k < 4 ; k++ ) {
	    if( k < 2 ) 
	      check2[k] = l1p[min2num-1][k] ;
	    else
	      check2[k] = l1p[min2num][k-2] ;
	  }
	}
	closept(check1, pnt, cpt1) ;
	closept(check2, pnt, cpt2) ;
	double xdiff1 = cpt1[0] - pnt[0] ;
	double ydiff1 = cpt1[1] - pnt[1] ;
	double xdiff2 = cpt2[0] - pnt[0];
	double ydiff2 = cpt2[1] - pnt[1] ;
	double dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
	double dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
	if( dist1 < dist2 ) {
	  cpt[0] = cpt1[0] ;
	  cpt[1] = cpt1[1] ;
	} else {
	  cpt[0] = cpt2[0] ;
	  cpt[1] = cpt2[1] ;
	}
	//       fprintf( stderr, "3: cpt[%i] = %lf, %lf\n", i, cpt[0], cpt[1] ) ;
	midpt( cpt, pnt, mpt ) ;
	radius( mpt, pnt, cpt, r ) ;
      }
    }
    if( r > laneWidth / 2)
      r = laneWidth / 2 ;

    rddfln2[i][0] = mpt[0] ;
    rddfln2[i][1] = mpt[1] ;
    rddfln2[i][2] = r ;
    
    //printf("rddfln2[%d] (x,y) = (%3.6f,%3.6f)\n", i, rddfln2[i][0], rddfln2[i][1]);
  }

  //%%%%%%%%%%%%%%%%%%%%%%%%
  //% SORT RDDF POINTS
  //%%%%%%%%%%%%%%%%%%%%%%%%
  l1 = 0 ;
  l2 = 0 ;
  double xdiff1, ydiff1, dist1, xdiff2, ydiff2, dist2 ;
  while( (l1 < numline1pts) || (l2 < numline2pts) ) {
    if( (rddfln1[l1][0] == 0) && (rddfln1[l1][1] == 0) && (rddfln1[l1][2] == 0))
      l1++;
    if( (rddfln2[l2][0] == 0) && (rddfln2[l2][1] == 0) && (rddfln2[l2][2] == 0))
      l2++ ;
        
    if( l1 < numline1pts ) {
      xdiff1 = rddfln1[l1][0] - rddfln1[0][0] ;
      ydiff1 = rddfln1[l1][1] - rddfln1[0][1] ;
      dist1 = sqrt( xdiff1 * xdiff1 + ydiff1 * ydiff1 ) ;
    } 
    if( l2 < numline2pts ) {
      xdiff2 = rddfln2[l2][0] - rddfln2[0][0] ;
      ydiff2 = rddfln2[l2][1] - rddfln2[0][1] ;
      dist2 = sqrt( xdiff2 * xdiff2 + ydiff2 * ydiff2 ) ;
    } 
    if( (l1 < numline1pts) && (dist1 <= dist2) || (l2 >= numline2pts) ){
      rddfx.push_back(rddfln1[l1][0]);
      rddfy.push_back(rddfln1[l1][1]);
      rddfr.push_back(rddfln1[l1][2]);
      l1++ ;
    } 
    else if ( l2 < numline2pts ){
      rddfx.push_back(rddfln2[l2][0]);
      rddfy.push_back(rddfln2[l2][1]);
      rddfr.push_back(rddfln2[l2][2]);
      l2++ ;
    }
  } 
  
  double previousNorthing, previousEasting, previousRadius;
  int rddfNum=1;
  
  // Specify a velocity profile for 
  double rddfMaxSpeed = 5;
  double rddfSpeed = rddfMaxSpeed;
  
  for (int ii = 0; ii < (int)rddfx.size(); ii++){
    if (ii==0){
      // convert to rddf format
      utm.n = rddfx[ii];
      utm.e = rddfy[ii];
      gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
// TODO: Find some way to specify the velocity profile, but for now use max velocity only
      rddfData.number = rddfNum;
      rddfData.Northing = rddfx[ii];
      rddfData.Easting = rddfy[ii];
      rddfData.maxSpeed = rddfSpeed;
      rddfData.radius = rddfr[ii];
      rddfData.latitude = latlon.latitude;
      rddfData.longitude = latlon.longitude;
      
      if (rddfx.size()==1){
	cerr << "ERROR convRDDF: only one data point in RDDFVector." << endl;
      }
      
      m_rddf->addDataPoint(rddfData);
      previousNorthing = rddfx[ii];
      previousEasting = rddfy[ii];
      previousRadius = rddfr[ii];
      rddfNum++;
    } 
    else if ((sqrt(pow(rddfx[ii]-previousNorthing,2)+pow(rddfy[ii]-previousEasting,2))>EPS) ||
	     ((sqrt(pow(rddfx[ii]-previousNorthing,2)+pow(rddfy[ii]-previousEasting,2))<=EPS) && (fabs(previousRadius-rddfr[ii])>EPS))){ 
      // this gets rid of rddf's that are too close together
      utm.n = rddfx[ii];
      utm.e = rddfy[ii];
      gis_coord_utm_to_latlon(&utm, &latlon, GEODETIC_MODEL);
// TODO: Velocity profile
      rddfData.number = rddfNum;
      rddfData.Northing = rddfx[ii];
      rddfData.Easting = rddfy[ii];
      rddfData.maxSpeed = rddfSpeed;
      rddfData.radius = rddfr[ii];
      rddfData.latitude = latlon.latitude;
      rddfData.longitude = latlon.longitude;
      
      m_rddf->addDataPoint(rddfData);
      previousNorthing = utm.n;
      previousEasting = utm.e;
      rddfNum++;
    }
  } 
  
  m_convRddf = true; 
//TODO other conversions
} 
 
 
void Corridor::closept( double lnseg[4], double pt[2], double cpt[2] ) 
{ 
  double min[2], max[2], a[2], b1[2], b2[2] ;
  double maga, magb, adotb1, adotb2, minth, maxth, distmin, distln, distmax ;

  min[0] = lnseg[0] ;
  min[1] = lnseg[1] ;
  max[0] = lnseg[2] ;
  max[1] = lnseg[3] ;

  a[0] = pt[0] - min[0] ;
  a[1] = pt[1] - min[1] ;
  b1[0] = max[0] - min[0] ;
  b1[1] = max[1] - min[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  magb = sqrt( b1[0] * b1[0] + b1[1] * b1[1] ) ;
  adotb1 = a[0] * b1[0] + a[1] * b1[1] ;
  minth = acos(adotb1/(maga*magb)) ;
  distmin = maga ;
  distln = magb ;

  a[0] = pt[0] - max[0] ;
  a[1] = pt[1] - max[1] ;
  b2[0] = min[0] - max[0] ;
  b2[1] = min[1] - max[1] ;
  maga = sqrt( a[0] * a[0] + a[1] * a[1] ) ;
  adotb2 = a[0] * b2[0] + a[1] * b2[1] ;
  maxth = acos(adotb2/(maga*magb)) ;
  distmax = maga ;

  double mdist1, a1, mdist2, a2, check, apt1[2], apt2[2] ;
  if( (minth <= PI/2) && (maxth <= PI/2 ) && (minth > 0) && (maxth > 0)) {
    mdist1 = distmin * sin(minth) ;
    a1 = mdist1 / tan(minth) ;
    mdist2 = distmax * sin(maxth) ;
    a2 = mdist2 / tan(maxth) ;
    check = fabs( a1 + a2 - distln ) ;
    apt1[0] = ( a1 / distln ) * b1[0] + min[0] ;
    apt1[1] = ( a1 / distln ) * b1[1] + min[1] ;
    apt2[0] = ( a2 / distln ) * b2[0] + max[0] ;
    apt2[1] = ( a2 / distln ) * b2[1] + max[1] ;
    if( check < 0.1 ){ 
      cpt[0] = ( apt1[0] + apt2[0] ) / 2 ;
      cpt[1] = ( apt1[1] + apt2[1] ) / 2 ;
    } else {
      if( mdist1 < mdist2 ) {
	cpt[0] = apt1[0] ;
	cpt[1] = apt1[1] ;
      } else { 
	cpt[0] = apt2[0] ;
	cpt[1] = apt2[1] ;
      }
    }
  } else {
    if( distmin < distmax ) {
      cpt[0] = min[0] ;
      cpt[1] = min[1] ;
    } else {
      cpt[0] = max[0] ;
      cpt[1] = max[1] ;
    }
  }
}

void Corridor::midpt( double pt1[2], double pt2[2], double mpt[2] ) 
{ 
  mpt[0] = (pt1[0] + pt2[0]) / 2 ;
  mpt[1] = (pt1[1] + pt2[1]) / 2 ;
}

void Corridor::radius( double mpt[2], double pt1[2], double pt2[2], double &r ) 
{
  double xdiff, ydiff, dist1, dist2;

  xdiff = (mpt[0] - pt1[0]) ;
  ydiff = (mpt[1] - pt1[1]) ;
  dist1 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  xdiff = (mpt[0] - pt2[0]) ;
  ydiff = (mpt[1] - pt2[1]) ;
  dist2 = sqrt( xdiff * xdiff + ydiff * ydiff ) ;
  if( dist1 <= dist2 )
    r = dist1 ;
  else
    r = dist2 ;
}


RDDF* Corridor::getRddfCorridor()
{
if (!m_convRddf) {
	convertToRddf();
}
return m_rddf;
}
