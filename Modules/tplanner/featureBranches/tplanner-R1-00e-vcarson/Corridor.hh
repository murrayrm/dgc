#ifndef CORRIDOR_HH_
#define CORRIDOR_HH_

#include "dgcutils/RDDF.hh"
#include "mapping/GeometricConstraints.hh"
#include "Conflict.hh"
#include "mapping/Line.hh"
//#include <unistd.h>
#include <stdio.h>
//#include <iostream>
//#include <sstream>
//#include <string>
#include <vector>
#include <math.h>
#include "dgcutils/ggis.h"

#define EPS 0.05
#define BIGNUMBER 1000000000.0f 
#define PI 3.1416

class Corridor {

public :  		

Corridor();
~Corridor();

GeometricConstraints* getGeometricConstraints();
RDDF* getRddfCorridor();
int getControlStateId();

private: 

void convertToRddf();  
// These are internal functions for convertToRddf()
void closept(double lnseg[4], double pt[2], double cpt[2]);
void midpt(double pt1[2], double pt2[2], double mpt[2]);
void radius(double mpt[2], double pt1[2], double pt2[2], double &radius);

GeometricConstraints* m_geometricConstraints;

Conflict* m_conflict;

std::vector<Line> m_polylines; 
int m_controlStateId;
RDDF* m_rddf;
bool m_convRddf;

};

#endif /*CORRIDOR_HH_*/
