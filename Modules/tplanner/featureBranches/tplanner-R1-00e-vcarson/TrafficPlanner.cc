#include "TrafficPlanner.hh"


  TrafficState TrafficPlanner::determineTrafficState() {
  	return null;
  }

  ControlState TrafficPlanner::determineControlState(TrafficState trafficState) {
  	return null;
  }

  std::vector<ControlStateTransition> TrafficPlanner::getTransitionControlStates(ControlState controlState) {
  	return null;
  }

  std::vector<ControlState> TrafficPlanner::calculateCorridors(ControlState controlState) {
  	return null;
  }

  Conflict TrafficPlanner::evaluateDynamicConflicts(Corridor corridor, ControlState controlState) {
  	return null;
  }

  CostFunction TrafficPlanner::determineCorridorCost(Corridor corridor) {
  	return null;
  }
  
  Corridor TrafficPlanner::evaluateCorridors() {
  	return null;
  }

  ControlState TrafficPlanner::getCurrentControlState() {
  	return null;
  }

  TrafficState TrafficPlanner::getCurrentTrafficState() {
  	return null;
  }

  bool TrafficPlanner::compareTrafficStates(TrafficState trafficState, TrafficState trafficStateOther) {
  	return null;
  }