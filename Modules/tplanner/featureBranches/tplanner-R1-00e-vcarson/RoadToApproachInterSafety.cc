#include "RoadToApproachInterSafety.hh"
#include <math.h>


RoadToApproachInterSafety::RoadToApproachInterSafety(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distSafeStop(30)
{
}

RoadToApproachInterSafety::RoadToApproachInterSafety()
{

}

RoadToApproachInterSafety::~RoadToApproachInterSafety()
{

}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState currTrafficState, Map* localMap,VehicleState vehState)
{
	double prob = 0;
	double distance = 0;
	distance = m_traffUtils.calculateDistance(localMap->getStopLine(), vehState.getPosition()); 
	if (distance <= m_distSafeStop) {
		m_trafTransProb = 1;	
		prob = m_trafTransProb;
	}
	return prob;
} 
