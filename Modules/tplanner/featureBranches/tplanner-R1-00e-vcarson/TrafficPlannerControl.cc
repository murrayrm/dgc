#include "TrafficPlannerControl.hh"


TrafficPlannerControl::TrafficPlannerControl(TrafficStateEstimator* estimator)
: m_traffStateEst(estimator)
{
//TODO Fix 

}

TrafficPlannerControl::TrafficPlannerControl()
{

}

TrafficPlannerControl::~TrafficPlannerControl()
{

}

void TrafficPlannerControl::controlLoop() 
{

TrafficState trafficState; 
ControlState controlState; 
VehicleState vehState; 
SegGoals segGoals; 

//TODO get the traffic state 

controlState = determineControlState(segGoals);
Corridor* corridor = controlState.determineCorridor(m_currVehState, m_currTrafficState, m_currPlanHorizon, m_localMap);
//RDDF* rddf = corridor->getRddfCorridor();
//determineFeasibility(corr);

}

ControlState TrafficPlannerControl::determineControlState(SegGoals segGoals) 
{
	VehicleState vehState; 
	std::vector<ControlStateTransition> transitions;
	
	//TODO do we determine if we are in the current control state, given TrafficState first? 
	m_currTrafficState = m_estimator->getCurrentTrafficState();
	
	//std::vector<ControlStateTransition> edges = m_controlGraph.getOutStateTransitions(m_currControlState); 
	determinePlanningHorizon(segGoals);	
	unsigned int i = 0;
	
	for (i; i < transitions.size(); i++) 
	{		
		transitions[i].meetTransitionConditions(m_currControlState, m_currTrafficState, m_currPlanHorizon, m_localMap, vehState);
	}
	
	//Now consider traffic state 
	//Now consider the segment goals in determining the best control state to transition to 
	
	m_currControlState = chooseMostProbableControlState(transitions);

}


Conflict TrafficPlannerControl::evaluateDynamicConflicts(Corridor corridor, ControlState controlState) 
{
	Conflict conflict;
	return conflict;
}

Corridor TrafficPlannerControl::evaluateCorridors() 
{
	Corridor corridor;
	return corridor;
}

ControlState TrafficPlannerControl::getCurrentControlState() 
{
	return m_currControlState;
}



ControlState TrafficPlannerControl::chooseMostProbableControlState(std::vector<ControlStateTransition> transitions) 
{

	//TODO finish method 		
	vector<ControlStateTransition> probOneTrans;
	vector<ControlStateTransition> probAboveThresh;
	vector<ControlStateTransition> probBelowThresh;
	
	double transProb = 0;
	double m_probThresh = 0;
	unsigned int i = 0;

	for (i; i < transitions.size(); i++) 
	{
		transProb = transitions[i].getUncertainty();
		if(1 == transProb) {
		probOneTrans.push_back(transitions[i]);
		} else if ( m_probThresh < transProb) {
		probAboveThresh.push_back(transitions[i]);		
		} else if ( m_probThresh >= transProb) {
		probBelowThresh.push_back(transitions[i]);		
		}
	}

	return probOneTrans.front().getControlStateTo();
}


void TrafficPlannerControl::determinePlanningHorizon(SegGoals segGoals) 
{

//TODO determine what subset of segGoals we need to consider given the TrafficState 
list<SegGoals> subSegGoals; 

//TrafficStateFactory::TrafficStateType tStateType = m_currTrafficState.getType();  TODO FIX 

int tStateType = m_currTrafficState.getType();

switch(tStateType) {
case TrafficStateFactory::APPROACHINTERSAFETY:
subSegGoals.push_back(segGoals); 
break;
default:
break;
};

}

