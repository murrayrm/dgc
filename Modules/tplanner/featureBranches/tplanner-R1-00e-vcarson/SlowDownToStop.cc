#include "SlowDownToStop.hh"
#include <math.h>
#include <list>


SlowDownToStop::SlowDownToStop(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_distToStopTrans(0.5)
, m_desiredDecel(0)
{
//TODO FIX this desired Decel
}

SlowDownToStop::~SlowDownToStop()
{

}

double SlowDownToStop::meetTransitionConditions(ControlState controlState, TrafficState trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{
	double vel = 0; 
	double dstop = 0;

	//TODO fix composite state 
	//if (TrafficStateFactory::CLEARREGION == trafficState.getType()) 
	if (TrafficStateFactory::APPROACHINTERSAFETY == trafficState.getType()) {
		vel = vehState.getVelocityMag();
        dstop = m_traffUtils->calculateDistance(map->getStopLine(), vehState.getPosition()); 

		if (dstop <= m_distToStopTrans ) {
			m_probability = 1;
		} else { m_probability = 0; }
	}
	return m_probability;
}

