#ifndef MAP_HH_
#define MAP_HH_


#include <iostream>
#include <vector>
#include "Location.hh"
#include "MapElement.hh"
#include "RoadBuilder.hh"
#include "ObstacleBuilder.hh"

class Map {

public:

Map() ;
~Map();

//For best guess on Traffic State 
std::vector<MapElement> getVehicles(int segment, int lane);
std::vector<MapElement> getLines(int segment, int lane);
std::vector<MapElement> getObstacles(int segment, int lane);
std::vector<MapElement> getStopLine(int segment, int lane);
std::vector<MapElement> getRoadObjects(int segment, int lane);
Location getStopLine();
int getCurrentLane();
int getCurrentSegment();

void setStopLine(Location stopLine);


private: 

Location m_stopLine;
RoadBuilder* m_roadBuilder;
ObstacleBuilder* m_obstacleBuilder;
std::vector<MapElement> m_veh;
std::vector<MapElement> m_obs;
std::vector<MapElement> m_lines; 
std::vector<MapElement> m_stopLines;
std::vector<MapElement> m_roadObjects;

};


#endif /*MAP_HH_*/
