#ifndef LINE_HH_
#define LINE_HH_


#include <vector>
#include "Point.hh"

class Line {

public:

//TODO fix 
//typedef enum { NO_BOUNDARY,DASHED_WHITE, DOUBLE_YELLOW } Boundary_Type;

//Line(int lineId, std::vector<Point>, Boundary_Type bdryType);	
Line(int lineId);
Line();		
~Line();
std::vector<Point> getPoints();
//Boundary_Type getBoundaryType();
bool isPassable();
int getId();
Point getClosestPoint(Point point);
int getIndexOfClosestPoint(Point point);

private: 

int m_id;
std::vector<Point> m_points;

};

#endif /*LINE_HH_*/

