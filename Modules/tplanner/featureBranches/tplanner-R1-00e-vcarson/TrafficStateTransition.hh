#ifndef TRAFFICSTATETRANSITION_HH_
#define TRAFFICSTATETRANSITION_HH_


#include "TrafficState.hh"
#include "state/VehicleState.hh"
#include "mapping/Map.hh"
#include "state/StateTransition.hh" 


class TrafficStateTransition : public StateTransition {


public: 

  TrafficStateTransition();
  TrafficStateTransition(TrafficState* state1, TrafficState* state2);
  virtual ~TrafficStateTransition();
  virtual double meetTransitionConditions(TrafficState trafficState, Map* localMap,VehicleState vehicleState);


protected: 
  double m_trafTransProb; 

private: 

  TrafficState* m_trafState1;
  TrafficState* m_trafState2;

};

#endif /*TRAFFICSTATETRANSITION_HH_*/
