#ifndef LANEKEEPTOSLOWDOWN_HH_
#define LANEKEEPTOSLOWDOWN_HH_

#include "ControlStateTransition.hh"


class LaneKeepToSlowDown : public ControlStateTransition

{

public: 


LaneKeepToSlowDown(ControlState* state1, ControlState* state2);

LaneKeepToSlowDown();

~LaneKeepToSlowDown();

double meetTransitionConditions(ControlState controlState, TrafficState trafficState, PlanningHorizon planHorizon, Map* map,VehicleState vehState);

private: 

double m_probability;
double m_desiredDecel;

};
#endif /*LANEKEEPTOSLOWDOWN_HH_*/
