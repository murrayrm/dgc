#include "LaneKeepToSlowDown.hh"
#include <math.h>
#include <list>
#include "TrafficStateFactory.hh"

LaneKeepToSlowDown::LaneKeepToSlowDown(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_desiredDecel(1.5)
{

}

LaneKeepToSlowDown::LaneKeepToSlowDown()
{

}

LaneKeepToSlowDown::~LaneKeepToSlowDown()
{

}

double LaneKeepToSlowDown::meetTransitionConditions(ControlState controlState, TrafficState trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{

	double vel = 0; 
	double delta = 0;
	double dstop = 0;


	//TODO FIX composite state
	//if (TrafficStateFactory::CLEARREGION == trafficState.getType()) {
	 
	if (TrafficStateFactory::APPROACHINTERSAFETY == trafficState.getType()) {
		vel = vehState.getVelocityMag();
		delta = pow(vel,2)/ (2*m_desiredDecel);
		dstop = m_traffUtils->calculateDistance(map->getStopLine(), vehState.getPosition());	
		if (dstop <= delta) {
			m_probability = 1;
		} else { m_probability = 0; }
	}
	//}
	return m_probability;
} 


