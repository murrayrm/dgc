#include "StopToStopped.hh"
#include <math.h>
#include <list>


StopToStopped::StopToStopped(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
  ,m_boundStoppedVel(0) 
{
}

StopToStopped::StopToStopped()
{

}

StopToStopped::~StopToStopped()
{

}

double StopToStopped::meetTransitionConditions(ControlState controlState, TrafficState trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{
	return m_probability;
} 


