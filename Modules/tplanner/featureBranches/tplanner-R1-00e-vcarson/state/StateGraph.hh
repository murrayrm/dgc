#ifndef STATEGRAPH_HH_
#define STATEGRAPH_HH_


#include <vector>
#include "StateTransition.hh"
#include "../ControlState.hh" 

//TODO fix test 

class StateGraph  {

public:

StateGraph();
~StateGraph();
StateGraph(std::vector<StateTransition> stateTransitions);

void addStateTransition(StateTransition stateTransition);

template<class X>
std::vector<X> getOutStateTransitions(State& state);

private:

std::vector<StateTransition> m_stateTransitions; 

	
}; 
#endif /*STATEGRAPH_HH_*/
