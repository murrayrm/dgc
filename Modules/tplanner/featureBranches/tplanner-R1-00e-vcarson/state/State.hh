#ifndef STATE_HH_
#define STATE_HH_

class State  {

 public:
 
 	State(int stateId);
 	State();
 	~State();
 	int getStateId();
 	void setStateId(int stateId);
 	void setUncertainty(double uncertainty);
  	double getUncertainty();
  	
  	int getType(); //TODO fix int

protected: 
	int m_type; 
  	
private:
	
	int m_stateId; 
	double m_uncertainty;

};

#endif /*STATE_HH_*/
