#ifndef STATETRANSITION_HH_
#define STATETRANSITION_HH_

#include "State.hh"

class StateTransition {

public:

StateTransition(State* State1, State* State2);
StateTransition();
virtual ~StateTransition();

State* getInitialState();
State* getFinalState(); 

double getUncertainty();
void setUncertainty(double uncert);
int getType(); //TODO FIX


private:

State* m_State1; 
State* m_State2;
int m_type;	
double m_uncertainty; 
}; 
#endif /*STATETRANSITION_HH_*/
