#ifndef VEHICLESTATE_HH_
#define VEHICLESTATE_HH_

#include "State.hh"
#include "../mapping/Location.hh"
#include <vector>


class VehicleState : State {

public: 

VehicleState(Location pos, std::vector<double> vel);
VehicleState(); //TODO get rid of this constructor
~VehicleState();
Location getPosition();
std::vector<double> getVelocity();
double getVelocityMag();

private: 
	
Location m_pos;
std::vector<double> m_vel;
double m_velMag;
	
};
#endif /*VEHICLESTATE_HH_*/
