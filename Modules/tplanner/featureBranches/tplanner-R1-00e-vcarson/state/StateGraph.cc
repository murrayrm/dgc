#include "StateGraph.hh"


StateGraph::StateGraph()
{
	
	
}

StateGraph::~StateGraph()
{
	
}
StateGraph::StateGraph(std::vector<StateTransition> stateTransitions) 
{
m_stateTransitions = stateTransitions; 
}

void StateGraph::addStateTransition(StateTransition stateTransition) 
{
m_stateTransitions.push_back(stateTransition);
}

template<class X>
std::vector<X> StateGraph::getOutStateTransitions(State& state) 
{

std::vector<X> transitions;
unsigned int i = 0;

for (i=0; i < m_stateTransitions.size(); i++) 
{
	if (m_stateTransitions[i].getInitialState()->getType() == state.getType()) {
	transitions.push_back(m_stateTransitions[i]);
	}
}

return transitions; 
}
