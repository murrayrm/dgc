#ifndef MAP_HH_
#define MAP_HH_


#include <boost/config.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <boost/graph/adjacency_list.hpp>
#include <boost/tuple/tuple.hpp>


#include "RoadBuilder.hh"
#include "ObstacleBuilder.hh"


class Map {

public:

//TODO Map(RNDF) 
Map() ;
~Map();

//For best guess on Traffic State 
vector<MapElement> getVehicles(int segment, int lane);

vector<MapElement> getLines(int segment, int lane);
vector<MapElement> getObstacles(int segment, int lane);
vector<MapElement> getStopLine(int segment, int lane);

private: 

//void constructPrior(RNDF);

/** This is the entire road. Roads are nodes and 
/*  the road links are edges.
/*  This includes the prior. We have a parallel graphs
/*  inside the m_road graph.  
*/


};


#endif /*MAP_HH_*/

RoadBuilder* m_roadBuilder;
ObstacleBuilder* m_obstacleBuilder;
boost::graph::adjacency_list <> m_road;
boost::graph::adjacency_list <> m_prior;
vector<Obstacle>  m_obstacles;
