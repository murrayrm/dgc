#ifndef CORRIDOR_HH_
#define CORRIDOR_HH_

class ControlState;
//class TrafficState;

#include "dgcutils/RDDF.hh"
#include "mapping/GeometricConstraints.hh"
#include "Conflict.hh"
#include "frames/point2.hh"
#include <stdio.h>
#include <vector>
#include <math.h>
#include "dgcutils/ggis.h"
#include "state/AliceStateHelper.hh"
#include "map/Map.hh"
#include "interfaces/TpDpInterface.hh"
#include "cmap/CMapPlus.hh"
#include "bitmap/Polygon.hh"
#include "bitmap/BitmapParams.hh"
#include "TrafficState.hh"
//#include "ControlState.hh"
#include "PlanningHorizon.hh"
#include "ControlStateFactory.hh"
#include "TrafficUtils.hh"
#include <boost/serialization/vector.hpp>
#include "interfaces/sn_types.h"
#include "skynettalker/SkynetTalker.hh"
#include "AlgGeom/AlgebraicGeometry.hh"
#include "AlgGeom/Polytope.hh"
#include "CmdLineArgs.hh"

#define EPS 0.05
#define BIGNUMBER 1000000000.0f 

#undef PI
#define PI 3.1416
#define EXPFACT 1
#define EXPPERC 0.25

struct PolygonParams
{
  float centerlaneVal;
  float obsCost;
  
  PolygonParams()
  {
    centerlaneVal = 0;
    obsCost = 10000;
  }
};

using namespace bitmap;

class Corridor {

public :  		

  Corridor();
  // These constructors are old, should use the one with the cmdlineargs
  Corridor(bool debug, bool verbose, bool log);
  Corridor(int sn_key, bool debug, bool verbose, bool log, bool laneCost);
  // This is the constructor to use...
  Corridor(CmdLineArgs cLArgs);
  Corridor(const Corridor& corr);
  ~Corridor();

  GeometricConstraints* getGeometricConstraints();
  RDDF* getRddfCorridor(point2 gloToLocalDelta);

  int getControlStateId();
  void addPolyline(point2arr);
  vector<point2arr> getPolylines();
  void setAlicePrevPos(point2 alicePos);
  //  void updateDistFromConStateBegin(point2 alicePrevPos);
  void clear();

  //  void setVelProfile(Map *localMap, VehicleState vehState, TrafficState *currTrafState, ControlState *currControlState, PlanningHorizon planHoriz);
  // void setSpeedProfileDist(point2 initPos, double initSpeed, double finalSpeed, point2 finalPos, bool insertPt);
  // vector<double> returnSpeedProfile();
  int setVelocityLimits(double velMin, double velMax);
  int calculateSeparationDist(VehicleState vehState, TrafficState* currTraffState);
  double getSeparationDist();
  void initializeOCPparams(VehicleState vehState, double vmin, double vmax);
  OCPparams getOCPparams();
  void setOCPfinalCondLB(int index, double cond);
  void setOCPfinalCondUB(int index, double cond);
  void setOCPinitialCondLB(int index, double cond);
  void setOCPinitialCondUB(int index, double cond);
  void convertOCPtoGlobal(point2 gloToLocalDelta);
  void setOCPmode(int mode);
  void adjustFCPosForRearAxle();

  void getOCPinitialPos(point2 &IC_posLB, point2 &IC_posUB);
  void getOCPfinalPos(point2 &FC_posLB, point2 &FC_posUB);

  void paintLaneCostCenterLine(CMapPlus* map, int costLayer, int tempLayer, 
			       point2arr& leftbound, point2arr& rightbound);
  void convertMapElementToCost(CMapPlus* map, int costLayer, int tempLayer, 
			       MapElement& el);
  void getCostMap(CMapPlus* map, int costLayer, int tempLayer,  
		  VehicleState &state, Map* lmap);

  // convert the corridor (given left and right boundaries) to polygons
  void convertLaneToTrianglePolygons(vector<Polygon>& polygons, point2arr& leftbound, 
				    point2arr& rightbound, float bval, float cval);
  void convertLaneToTrapezoidPolygons(vector<Polygon>& polygons, point2arr& leftbound, 
				    point2arr& rightbound, float bval, float cval);
  // convert an obstacle with GEOMETRY_POLY geometry to polygons
  void convertObstacleToPolygon(vector<Polygon>& polygons, MapElement& el,
				  float obsCost, float bval);
  // convert a sensed line (or obstacle) with geometry GEOMETRY_LINE to polygons
  void convertLineToPolygon(vector<Polygon>& polygons, MapElement& el,
				  float obsCost, float bval);

  void convertMapElementToPolygon(vector<Polygon>& polygons, MapElement& el,
				  float obsCost, float bval);
  void getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
		       ControlState* controlState, TrafficState* trafficState,
		       VehicleState &state, Map* lmap, point2 gloToLocalDelta);

  void printPolylineCorr();
  void printICFC();
  // void printVelProfile();
  void generatePolyCorridor();
  void printPolyCorridor(void);
  void printMatlabPolyCorridor(void);
  void sendPolyCorridor(void);

  void printPolylines();
  void printMatlabPolylines();
  void printSeparationDist();
  void printSpeedLimits();
private: 
  bool m_verbose;
  bool m_debug;
  bool m_log;
  bool m_use_local;
  bool m_useLaneCost;

  RDDF* convertToRddf(point2 gloToLocalDelta);  

  GeometricConstraints* m_geometricConstraints;
  
  Conflict* m_conflict;
  
  vector<point2arr> m_polylines; 

  vector<double> m_speedProfile;
  vector<double> m_accProfile;

  int m_controlStateId;

  vector<double> m_velProfile; // [V_entry V_exit]
  double m_desiredAcc;
  double m_distFromConStateBegin;
  point2 m_alicePrevPos;
  OCPparams m_ocpParams;
  CPolytope* m_polyCorridor;
  int m_nPolytopes;
  SkynetTalker<sendCorr> m_polyCorridorTalker;
  bool m_polyCorridorLegal;
  double m_separationDist;
};

#endif /*CORRIDOR_HH_*/
