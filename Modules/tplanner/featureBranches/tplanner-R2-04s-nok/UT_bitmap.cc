#include <iostream>
#include <frames/point2.hh>
#include <bitmap/BitmapParams.hh>
#include <skynettalker/SkynetTalker.hh>
#include <ocpspecs/CostMapEstimator.hh>
#include <highgui.h>
#include "Corridor.hh"
#include "UTurn.hh"
#include "LaneKeeping.hh"
#include "RoadRegion.hh"
#include <boost/serialization/vector.hpp>

using namespace std;

int main(int argc,char** argv)
{
  int skynetKey = skynet_findkey(argc, argv);
  cout << "Skynet key: " << skynetKey << endl;
  /* Receiver */
  CostMap cmap;
  CostMapEstimator cmapEst(skynetKey, true, false, 2);
  //ControlState* controlState = new UTurn(0, ControlStateFactory::UTURN, 1);
  ControlState* controlState = new LaneKeeping(0, ControlStateFactory::LANE_KEEPING);
  TrafficState* trafficState = new RoadRegion(0, TrafficStateFactory::ROAD_REGION);

  /* Sender */
  SkynetTalker< BitmapParams > polyTalker(skynetKey, SNbitmapParams, 
					  MODtrafficplanner);

  Map lmap;
  BitmapParams bmparams;
  PolygonParams polygonParams;

  //Parameters
  bmparams.width = 800;
  bmparams.height = 800;
  bmparams.resX = 0.1;
  bmparams.resY = 0.1;
  bmparams.baseVal = 100;
  polygonParams.centerlaneVal = 1;
  polygonParams.obsCost = 1000;

  VehicleState state;
  state.localX = 70;
  state.localY = -20;
  Corridor corr = Corridor(true, false, false);

  /*
  point2arr leftBound = point2arr();
  point2 pt=point2(23.7701, -0.321133);
  leftBound.push_back(pt);
  pt=point2(42.4599, 6.23704);
  leftBound.push_back(pt);
  pt=point2(60.9154, 13.1588);
  leftBound.push_back(pt);
  pt=point2(66.7974, 10.0959);
  leftBound.push_back(pt);
  pt=point2(72.6794, 7.03303);
  leftBound.push_back(pt);
  pt=point2(79.5468, -11.4646);
  leftBound.push_back(pt);
  pt=point2(87.064, -31.2927);
  leftBound.push_back(pt);
  pt=point2(92.1976, -45.6156);
  leftBound.push_back(pt);
  pt=point2(99.1599, -64.4611);
  leftBound.push_back(pt);
  corr.addPolyline(leftBound);

  point2arr rightBound = point2arr();
  pt=point2(22.2397, 3.98717);
  rightBound.push_back(pt);
  pt=point2(40.9, 10.535);
  rightBound.push_back(pt);
  pt=point2(59.3099, 17.4397);
  rightBound.push_back(pt);
  pt=point2(71.9349, 22.1747);
  rightBound.push_back(pt);
  pt=point2(76.9119, 8.76878);
  rightBound.push_back(pt);
  pt=point2(83.8274, -9.85859);
  rightBound.push_back(pt);
  pt=point2(91.3539, -29.7109);
  rightBound.push_back(pt);
  pt=point2(96.494, -44.052);
  rightBound.push_back(pt);
  pt=point2(103.446, -62.8697);
  corr.addPolyline(rightBound);
  */

  point2arr leftBound = point2arr();
  point2 pt=point2(4.77344, -7.15118);
  leftBound.push_back(pt);
  pt=point2(23.7701, -0.321133);
  leftBound.push_back(pt);
  pt=point2(42.4599, 6.23704);
  leftBound.push_back(pt);
  pt=point2(60.9154, 13.1588);
  leftBound.push_back(pt);
  pt=point2(66.7974, 10.0959);
  leftBound.push_back(pt);
  pt=point2(72.6794, 7.03303);
  leftBound.push_back(pt);
  pt=point2(79.5468, -11.4646);
  leftBound.push_back(pt);
  pt=point2(87.064, -31.2927);
  leftBound.push_back(pt);
  pt=point2(92.1976, -45.6156);
  leftBound.push_back(pt);
  pt=point2(99.1599, -64.4611);
  leftBound.push_back(pt);
  corr.addPolyline(leftBound);

  point2arr rightBound = point2arr();
  pt=point2(3.22656, -2.84882);
  rightBound.push_back(pt);
  pt=point2(22.2397, 3.98717);
  rightBound.push_back(pt);
  pt=point2(40.9, 10.535);
  rightBound.push_back(pt);
  pt=point2(59.3099, 17.4397);
  rightBound.push_back(pt);
  pt=point2(71.9349, 22.1747);
  rightBound.push_back(pt);
  pt=point2(76.9119, 8.76878);
  rightBound.push_back(pt);
  pt=point2(83.8274, -9.85859);
  rightBound.push_back(pt);
  pt=point2(91.3539, -29.7109);
  rightBound.push_back(pt);
  pt=point2(96.494, -44.052);
  rightBound.push_back(pt);
  pt=point2(103.446, -62.8697);
  rightBound.push_back(pt);
  corr.addPolyline(rightBound);

  /*
  point2arr obs;
  pt=point2(4,5);
  obs.push_back(pt);
  pt=point2(4,7);
  obs.push_back(pt);
  pt=point2(7,7);
  obs.push_back(pt);
  pt=point2(7,3);
  obs.push_back(pt);
  MapElement mapElement;
  mapElement.setTypeObstacle();
  mapElement.frameType = FRAME_LOCAL;
  mapElement.setId(1);
  mapElement.setGeometry(obs);
  mapElement.height = 0.2;
  lmap.addEl(mapElement);
  */

  corr.getBitmapParams(bmparams, polygonParams, controlState, trafficState, state, &lmap, point2(0,0));
  polyTalker.send(&bmparams);
  usleep(100000);
  bool cmapRecv = cmapEst.updateMap(&cmap);
  if (!cmapRecv) {
    cerr << "ERROR: No cmap received" << endl;
  }

  cout << "Press any key to quit" << endl;
  cvWaitKey(0);
  delete controlState;

}
