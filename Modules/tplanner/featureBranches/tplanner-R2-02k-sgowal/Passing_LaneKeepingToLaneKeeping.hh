#ifndef PASSING_LANEKEEPINGTOLANEKEEPING_HH_
#define PASSING_LANEKEEPINGTOLANEKEEPING_HH_

#include "ControlStateTransition.hh"

class Passing_LaneKeepingToLaneKeeping: public ControlStateTransition {

public:
  Passing_LaneKeepingToLaneKeeping(ControlState *state1, ControlState *state2);
  Passing_LaneKeepingToLaneKeeping();
  ~Passing_LaneKeepingToLaneKeeping();
  double meetTransitionConditions(ControlState *cstate, TrafficState *tstate, PlanningHorizon phorizon, Map *map, VehicleState vstate);

private:
  double m_prob;
};

#endif
