#include "Stopped.hh"
#include "Log.hh"

Stopped::Stopped(int stateId, ControlStateFactory::ControlStateType type, StoppedType stage)
: ControlState(stateId, type)
{
    m_uturn = false;
    m_changelane_left = false;
    m_changelane_right = false;
    m_stage = stage;
    m_failed = false;
}

Stopped::~Stopped()
{
}

/* Same code than LaneKeeping except velIn = 0, velOut = 0, acc = 0 */
int Stopped::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  Log::getStream(1) << "In Stopped::determineCorridor " << endl;
    
  int error = 0;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);


  switch (traffState->getType()) {
    
  case TrafficStateFactory::ZONE_REGION:
    {
      SegGoals nextSegment = planHoriz.getSegGoal(1);
      error += CorridorUtils::makeCorridorZone(corr, currRearPos, localMap, currSegment, nextSegment);
      error += CorridorUtils::setFinalCondStopped(corr, vehState);
    }
    break;

  case TrafficStateFactory::ROAD_REGION:
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      /* We want to report a failure if we are stuck behind an obstacle */
      if (m_stage == OBSTACLE) {
        // Log::getStream(1)<<"STOPPED: getIsVehPresentInOppLane "<<getIsVehPresentInOppositeLane()<<endl;
        // if (getIsVehPresentInOppositeLane()) {
        // m_failed = false; 
        // } else {
          m_failed = true;
        // }
      }

      bool isReverse = false;
      error += CorridorUtils::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
      error += CorridorUtils::setFinalCondStopped(corr, vehState);
    }
    break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
      SegGoals nextSegment = planHoriz.getSegGoal(1);
      error += CorridorUtils::makeCorridorIntersection(corr, currFrontPos, localMap, currSegment);
      error += CorridorUtils::setFinalCondStopped(corr, vehState);
    }
        break;
        
    default:
      {    
        cerr << "Stopped.cc: Undefined Traffic state" << endl;
        error += 1; 
      }
  }
  return error;
}



bool Stopped::getUTurnBool()
{
    return m_uturn;
}

bool Stopped::getChangeLaneLeftBool()
{
    return m_changelane_left;
}

bool Stopped::getChangeLaneRightBool()
{
    return m_changelane_right;
}

void Stopped::resetConditions()
{
    m_uturn = false;
    m_changelane_left = false;
    m_changelane_right = false;
}
