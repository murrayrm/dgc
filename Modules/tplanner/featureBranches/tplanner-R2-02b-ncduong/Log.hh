#ifndef LOG_HH_
#define LOG_HH_

#include <stdio.h>
#include <string>

#include "ControlStateFactory.hh"
#include "TrafficStateFactory.hh"
#include "ControlState.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "TrafficPlanner.hh"
#include "gcinterfaces/SegGoals.hh" 

using namespace std;

class Log {

  public:

    static int init(string filename, bool logit);
    static void destroy();
    
    static void logSegment(SegGoals *seg);
    static void logTrafficState(TrafficState *state, ControlState *cstate, SegGoals *seg, Map *map);
    static void logControlState(ControlState *state);
    static void logString(string str);

  private:
  
    static string file;
    static FILE *fp;
    static bool log;
    static int control_id;
    static int traffic_id;
    static int open();
    static void close();

};

#endif                          /*LOG_HH_ */

