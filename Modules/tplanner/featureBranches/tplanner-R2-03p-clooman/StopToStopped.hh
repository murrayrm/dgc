#ifndef STOPTOSTOPPED_HH_
#define STOPTOSTOPPED_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class StopToStopped:public ControlStateTransition {

  public:

    StopToStopped(ControlState * state1, ControlState * state2);
    StopToStopped();
    ~StopToStopped();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:

    double m_boundStoppedVel;

};

#endif                          /*STOPTOSTOPPED_HH_ */
