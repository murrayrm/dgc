#include "TrafficStateEstimator.hh"


TrafficStateEstimator::TrafficStateEstimator() 
{
  m_trafficGraph = TrafficStateFactory::createTrafficStates();
	// m_currTrafficState = .setType(TrafficStateFactory::ZONE_REGION);
	//m_currStateType = TrafficStateFactory::ZONE_REGION;
  //  m_currTrafficState = m_trafficGraph.getState(m_currStateType); TODO FIX LATER

}

TrafficStateEstimator::~TrafficStateEstimator() 
{
}


TrafficState* TrafficStateEstimator::determineTrafficState(TrafficState *currTrafficState, Map* localMap, VehicleState vehState, PointLabel exitPtLabel) 
{
  //TODO given sensing info determineTrafficState...
  //Given current traffic state, query graph get  possible transitions list  
  //After a query to the map object, vehicle state, determine possible transitions 
  //for all possible transitions calculate the probability of meeting the transition conditions 
  //VehicleState vehState; 
  
  m_currVehState = vehState;
  
  cout << "TrafficStateEstimator::determineTrafficState getting transitions"<< endl;
	//cout << "======m_currTrafficState = " << currTrafficState->getType() << endl; 
	std::vector<StateTransition*> transitions = m_trafficGraph.getOutStateTransitions(currTrafficState); 
  cout << "done getting transitions"<< endl;
  TrafficStateTransition* transition;

  unsigned int i = 0;
  
  cout << "in TrafficStateEstimator::determineTrafficState transition size = " << transitions.size() << endl; 
  for (i; i < transitions.size(); i++) 
    {
      transition = static_cast<TrafficStateTransition*> (transitions[i]);
      cout << "Calling MeetTransitionCondition " << i << endl;
      transition->meetTransitionConditions(currTrafficState, localMap, m_currVehState, exitPtLabel);
    }
  
  cout << "choosing trafficstate"<< endl;
  return chooseMostProbableTrafficState(currTrafficState, transitions);
  //m_currStateType = (TrafficStateFactory::TrafficStateType)m_currTrafficState.getType(); //FIX we may not need this 
}


//TrafficState TrafficStateEstimator::getCurrentTrafficState() 
//{
// return m_currTrafficState; 
//}

//TrafficStateFactory::TrafficStateType TrafficStateEstimator::getCurrentTrafficStateType() 
//{
// return m_currStateType; 
//}

TrafficState * TrafficStateEstimator::chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions) 
{
  TrafficState *currTrafficState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  TrafficStateTransition* trans; 
  
  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  
  cout << "in TrafficStateEsimtator::chooseMostProbable... " << transitions.size() << endl;
  
  if (transitions.size() > 0) {
    for (i; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<TrafficStateTransition*> (probOneTrans.front());
      currTrafficState = trans->getTrafficStateTo(); 
    } else { 
			cout<<"There are no TrafficStateTransitions with probability 1"<<endl;
			currTrafficState = trafState; 
		}
  } else { cout << "in TrafficStateEstimator::chooseMostProbable... " << transitions.size() << endl; }
	  
	  return currTrafficState;

}

