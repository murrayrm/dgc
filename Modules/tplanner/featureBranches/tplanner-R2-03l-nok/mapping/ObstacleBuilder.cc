
#include "ObstacleBuilder.hh"



ObstacleBuilder::ObstacleBuilder()
: m_currObsId (0) 
{

}
 
ObstacleBuilder::~ObstacleBuilder() 
{

}

Obstacle*  ObstacleBuilder::buildObstacle() 
{

return new Obstacle(getNextObstacleId());
}

int ObstacleBuilder::getNextObstacleId() 
{
return ++m_currObsId;
}
