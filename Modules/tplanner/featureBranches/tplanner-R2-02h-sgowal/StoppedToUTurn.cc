#include "StoppedToUTurn.hh"
#include <math.h>
#include <list>


StoppedToUTurn::StoppedToUTurn(ControlState * state1, ControlState * state2)
:  ControlStateTransition(state1, state2)
    , m_boundStoppedVel(0.2)
{
}

StoppedToUTurn::StoppedToUTurn()
{

}

StoppedToUTurn::~StoppedToUTurn()
{

}

double StoppedToUTurn::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in StoppedToUTurn::meetTransitionConditions" << endl;
    }
    
    // cState was not used because the resetConditions was commented!
    /* Stopped *cState = static_cast < Stopped * >(controlState); */

    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        if (SegGoals::UTURN == planHorizon.getSegGoal(0).segment_type) {
            m_probability = 1;
            /* cState->resetConditions(); */
        } else {
            m_probability = 0;
        }
        
        if ((m_verbose) || (m_debug)) {
            cout << "stopped to uturn m_probability = " << m_probability << endl;
        }
    }
        break;
        
    default:
    
        m_probability = 0;
        cerr << "StoppedToUTurn.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_probability);
    
    return m_probability;
}
