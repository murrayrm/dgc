// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/RDDFTalker.hh"
#include "map/MapElementTalker.hh"
//#include "interfaces/CostMapMsg.h"
#include "gcinterfaces/SegGoals.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
//#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "skynettalker/SkynetTalker.hh"
// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
//#include "cmap/CMapPlus.hh"
#include "dgcutils/RDDF.hh"

// Mapping objects (tplanner version for now)
#include "mapping/Segment.hh"

// tplanner state (graph level)
#include "state/StateGraph.hh"

// new tplanner classes
#include "TrafficPlannerControl.hh"
#include "Corridor.hh"
#include "TrafficState.hh"
#include "ControlState.hh"
#include "TrafficStateEstimator.hh"
#include "TrafficStateFactory.hh"
#include "ControlStateFactory.hh"

// need to include the traffic and control states that you are testing here
#include "IntersectionStop.hh"
#include "LaneKeeping.hh"

using namespace std;             

void spoofSegGoals(list<SegGoals> &segGoals);

class dummyTPlanner : public CStateClient , public CRDDFTalker
{
public:
  dummyTPlanner(int skynetKey, string RNDFfilename)
    //dummyTPlanner(int skynetKey)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(true)
  , m_OCPparamsTalker(skynetKey, SNocpParams, MODtrafficplanner)
  {
    int rddfSocket = m_skynet.get_send_sock(SNrddf);
    // ===================================================
    // GET ALL THE INFO I NEED
    // vehicle state
    UpdateState();
    cout << "curr state = " << AliceStateHelper::getPositionFrontBumper(m_state) << endl;
    // Load local Map
    localMap = new Map();
    //string RNDFfilename = "singleroad.rndf";
    bool loadedMap = localMap->loadRNDF(RNDFfilename);
    // insert obstacle
    //    MapElement el;
    //el.set_id(-1);
    // el.set_circle_obs();
    //point2 tmppt(-.15,-14.3);
    //point2 tmppt;
    //PointLabel ptlabel(1,1,4);
    //int err = localMap->getWaypoint(tmppt, ptlabel);
    // el.set_geometry(tmppt,3.0);
    //localMap->data.push_back(el);

    // global to local conversion
    point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
    localMap->prior.delta = statedelta;
    point2 m_gloToLocalDelta;
    m_gloToLocalDelta = localMap->prior.delta;
    cout << "map delta = " << m_gloToLocalDelta << endl;

    // spoof the segGoals for now
    list<SegGoals> segGoals;
    spoofSegGoals(segGoals);
    segGoals.front().print(); cout << endl;
    
    // initialize traffic and control state estimators
    //TrafficStateEstimator* m_trafStateEst = new TrafficStateEstimator();
    TrafficPlannerControl* m_tplannerControl = new TrafficPlannerControl();

    // initialize traffic and control state factories 
    TrafficStateFactory m_tfac = TrafficStateFactory(); 
    ControlStateFactory m_cfac = ControlStateFactory(); 


    // ==================================================================
    // DEFINE CONTROL AND TRAFFIC STATES
    // current tplanner state variables
    TrafficState * m_currTrafficState = new IntersectionStop(0, TrafficStateFactory::INTERSECTION_STOP);
    m_tfac.print(m_currTrafficState->getType());
    ControlState * m_currControlState = new LaneKeeping(0, ControlStateFactory::LANE_KEEPING);
    PlanningHorizon m_currPlanHorizon; 
    m_tplannerControl->determinePlanningHorizon(m_currPlanHorizon, m_currTrafficState, segGoals);  
    m_cfac.print(m_currControlState->getType());
    
    // set the initial conditions
    m_currControlState->setInitialPosition(m_state);
    m_currControlState->setInitialVelocity(m_state);
    m_currControlState->setInitialTime();    

    // ===================================================================
    // generate corridor based on control and traffic state
    Corridor corridor;
    
    // initialize OCPparams
    corridor.initializeOCPparams(m_state, m_currPlanHorizon.getSegGoal(0).minSpeedLimit, m_currPlanHorizon.getSegGoal(0).maxSpeedLimit);
    
    cout << "calculate corridor polylines" << endl;
    m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);

    

    corridor.setVelProfile(localMap, m_state, m_currTrafficState, m_currControlState, m_currPlanHorizon);

    corridor.printICFC();

    corridor.printVelProfile();

    point2 tmpPt;
    vector<point2arr> polylines = corridor.getPolylines();
    point2arr leftBound = polylines[0];
    cout << "leftBound = " << leftBound << endl;
    point2arr rightBound = polylines[1];
    cout << "rightBound = " << rightBound << endl;
    tmpPt.set((rightBound[0].x+rightBound[1].x)/2,(rightBound[0].y+rightBound[1].y)/2);
    cout << "tmpPt = " << tmpPt << endl;
    point2 exitPt = (leftBound.back()+rightBound.back())/2;
    cout << "exitPt = " << exitPt << endl;
    corridor.setSpeedProfileDist(tmpPt, 10, 0, exitPt);
    vector<double> speed = corridor.returnSpeedProfile();


    
    cout << "speed at tmpPt = " << speed << endl;
    cout << "convert to rddf" << endl;
    RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
    rddf->print();

    


    // send the data
    cout << "sending rddf" << endl;
    SendRDDF(rddfSocket,rddf);

    cout << "converting ICs and FCs to global frame" << endl;
    corridor.convertOCPtoGlobal(m_gloToLocalDelta);
    
    cout << "sending OCPparams" << endl;
    bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
    if (!sendOCPparamsErr){
      cout << "TrafficPlanner: problem with send OCP parameters" << endl;
    }
    
  }

  ~dummyTPlanner()
  {
    delete localMap;
  }

  void spoofSegGoals(list<SegGoals> &segGoals)
  {
    SegGoals tmpSegGoal;
    
    tmpSegGoal.segment_type = SegGoals::ROAD_SEGMENT;
    tmpSegGoal.goalID = 1;
    tmpSegGoal.entrySegmentID = 1;
    tmpSegGoal.entryLaneID = 1;
    tmpSegGoal.entryWaypointID = 4;
    tmpSegGoal.exitSegmentID = 4;
    tmpSegGoal.exitLaneID = 2;
    tmpSegGoal.exitWaypointID = 1;
    tmpSegGoal.minSpeedLimit = 0;
    tmpSegGoal.maxSpeedLimit = 10;
    tmpSegGoal.illegalPassingAllowed = false;
    tmpSegGoal.stopAtExit = true;
    tmpSegGoal.isExitCheckpoint = false;
    tmpSegGoal.perf_level = 0;
    
    segGoals.push_back(tmpSegGoal);

    tmpSegGoal.goalID = 2;
    tmpSegGoal.entrySegmentID = 4;
    tmpSegGoal.entryLaneID = 2;
    tmpSegGoal.entryWaypointID = 1;
    tmpSegGoal.exitSegmentID = 4;
    tmpSegGoal.exitLaneID = 2;
    tmpSegGoal.exitWaypointID = 3;
    
    segGoals.push_back(tmpSegGoal);
  }

private:
  SkynetTalker <OCPparams> m_OCPparamsTalker;
  Map * localMap;

};

int main(int argc, char ** argv)
{
  // interfaces and communication
  int skynetKey = 130;

  if (argc == 3)
  {
    skynetKey = atoi(argv[1]);
    string rndf(argv[2]);

    cout << "skynet key = " << skynetKey << endl;

    //dummyTPlanner dummytplanner = dummyTPlanner(skynetKey);
    dummyTPlanner dummytplanner = dummyTPlanner(skynetKey,rndf);
  }
  else
  {
    cerr << "usage: testCState SKYNET_ID rndf";
    exit(1);
  }
  
  return 0;
}



