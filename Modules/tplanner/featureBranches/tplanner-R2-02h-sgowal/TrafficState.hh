#ifndef TRAFFICSTATE_HH_
#define TRAFFICSTATE_HH_

#include "TrafficStateFactory.hh"
#include "state/State.hh"

class TrafficState : public State 
{

public: 

  TrafficState();
  TrafficState(int stateId, TrafficStateFactory::TrafficStateType type);
  ~TrafficState();
  TrafficStateFactory::TrafficStateType getType();
  int getStateID();
  static void setOutputParams(bool debug, bool verbose, bool log);
  string toString();
	
protected:
  static bool m_verbose;
  static bool m_debug;
  static bool m_log;

private: 
  int m_stateID;
  TrafficStateFactory::TrafficStateType m_type;
  
};

#endif /*TRAFFICSTATE_HH_*/
