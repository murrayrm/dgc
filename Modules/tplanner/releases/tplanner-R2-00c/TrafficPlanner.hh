/*! TrafficPlanner.hh
 * Noel duToit
 * Jan 25 2007
 */

#ifndef TRAFFICPLANNER_HH
#define TRAFFICPLANNER_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/RDDFTalker.hh"
#include "skynettalker/SegGoalsTalker.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
//#include "interfaces/CostMapMsg.h"
#include "interfaces/SegGoals.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
//#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"

// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
#include "cmap/CMapPlus.hh"
#include "dgcutils/RDDF.hh"

// Mapping objects (tplanner version for now)
#include "mapping/Segment.hh"

// tplanner state (graph level)
#include "state/StateGraph.hh"

// new tplanner classes
#include "TrafficPlannerControl.hh"
#include "Corridor.hh"
#include "TrafficState.hh"
#include "ControlState.hh"
#include "TrafficStateEstimator.hh"
#include "TrafficStateFactory.hh"
#include "ControlStateFactory.hh"

/*! TrafficPlanner class
 * This is the main class for the traffic planner where the information from 
 * mapping is assimilated and combined with the goals from mplanner and the CA
 * traffic rules. This function inherits from StateClient, SegGoalsTalker, 
 * MapdeltaTalker, RDDFTalker, and LocalMapTalker
 * \brief Main class for traffic planner function  
 */ 
class CTrafficPlanner : public CStateClient, public CSegGoalsTalker, public CRDDFTalker, public CMapElementTalker  //SAM//, public CLocalMapTalker
{ 
public:

  /*! Contstructor */
  CTrafficPlanner(int skynetKey, bool bWaitForStateFill, bool recvLocalMap, 
      bool recvSegmentGoals, int debugLevel, int verboseLevel);
  /*! Standard destructor */
  ~CTrafficPlanner();

  /////////////////////////////////////////////////////////////////////
  // Threads
  /////////////////////////////////////////////////////////////////////
  /*! this is the function that continually reads the latest object map*/
  void getLocalMapThread();

  /*! this is the function that continually reads dplanner status*/
  void getDPlannerStatusThread();

  /*! this is the function that continually receives segment goals from mplanner */
  void getSegGoalsThread();
  
  /*! listening for full cost map requests from dplanner */
  void getStaticCostMapRequestThread();

  /////////////////////////////////////////////////////////////////////
  // Main planning loop
  /////////////////////////////////////////////////////////////////////
  /*! the main traffic planning loop*/
  void TPlanningLoop(void);

  /////////////////////////////////////////////////////////////////////
  // initialize map from rndf file
  /////////////////////////////////////////////////////////////////////
  bool loadRNDF(string filename) 
  {
    bool loadedMap = m_localMap->loadRNDF(filename);
    //TEMPORARY
    localMap->loadRNDF(filename);

    //MapElement el;
    //el.set_id(-1);
    //el.set_circle_obs();
    //point2 tmppt(-.75,-35);
    //point2 tmppt;
    //PointLabel ptlabel(1,1,4);
    //int err = localMap->getWaypoint(tmppt, ptlabel);
    //el.set_geometry(tmppt,3.0);
    //localMap->data.push_back(el);
    //m_localMap->data.push_back(el);
    

    return loadedMap;
  }


  bool isComplete(PointLabel exitWayptLabel, VehicleState vehState);

  /*! this is the function that continually reads map deltas and updates the map */
  //void getMapDeltasThread();

private:

  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;

  /*!\param m_recvLocalMap specifies whether tplanner listens for map updates
   * from mapper, or just uses the initialized map (from rndf)
   */
  bool m_recvLocalMap;

  /*!\param m_recvSegmentGoals specifies whether tplanner listens for segment
   * goals from mplanner, or just uses some default (this is for dev only)
   */ 
  bool m_recvSegmentGoals;

  /*!\param m_debugLevel specifies level of debugging
   */
  bool m_debug;

  /*!\param m_verbose specifies level of debugging
   */
  bool m_verbose;


  // Local Map
  Map * m_localMap;
  Map * localMap;

  pthread_mutex_t m_LocalMapMutex;

  // Cost map
  /*!\param m_costMap is the cost grid map that is created in tplanner and is sent to dplanner*/
  CMapPlus m_costMap;
  /*!\param m_costLayerID identifies the layer number associated with the cost. This member variable
  * is used in getFullStaticCostMapRequestThread() */
  int m_costLayerID, m_conditionLayerID; 
  int m_numRows, m_numCols;
  pthread_mutex_t m_CostMapMutex;
  int requestFullStaticCostMapSocket;

  //CostMapMsg costMsg;

  // another map to test the sending of cost maps
  CMapPlus m_testMap;
  int m_testLayerID;

  // dplanner Status
  /*!\param m_dplannerStatus is the dplanner status variable - the feedback from dplanner to tplanner*/
  //DPlannerStatus* m_dplannerStatus;
  //pthread_mutex_t m_DPlannerStatusMutex;

  // Segment goals
  /*!\param m_segGoals is the Segment goals variable that is populated by the SegGoalsTalker*/
  list<SegGoals> m_segGoals;

  pthread_mutex_t m_SegGoalsMutex;
  int segGoalsSocket;                // The skynet socket for receiving segment goals from mplanner

  TrafficStateEstimator* m_trafStateEst;
  TrafficPlannerControl* m_tplannerControl;

  // current tplanner state variables
  TrafficState * m_currTrafficState;
  ControlState * m_currControlState;
  PlanningHorizon m_currPlanHorizon;  


  //Factories 
  TrafficStateFactory m_tfac; 
  ControlStateFactory m_cfac; 

  // corridor
  Corridor corridor;
  point2 m_gloToLocalDelta;
  // initialize skynet talker for OCPparams
  SkynetTalker <OCPparams> m_OCPparamsTalker;
  // vehicle state
  //VehicleState m_currVehState;
};

#endif  // TRAFFICPLANNER_HH

