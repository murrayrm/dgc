#include "UTurn.hh"


UTurn::UTurn(int stateId, ControlStateFactory::ControlStateType type)
  : ControlState(stateId,type)
{
  stage1 = true;
}

UTurn::~UTurn()
{
}

int UTurn::determineCorridor(Corridor &corr, VehicleState vehState, TrafficState *traffState, PlanningHorizon planHoriz, Map* localMap) 
{

  
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;

  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;
  point2arr leftBound, rightBound;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  if(stage1) {
    // for first step: command a reverse
    // note: setting to reverse mode -- mode = 1 --> reverse, mode - 0 --> fwd
    //  corr.setOCPmode(1);
    cout << "stage 1: reverse" << endl;
    corr.setOCPmode(1);
    
    LaneLabel laneLabel(currSegment.entrySegmentID, currSegment.entryLaneID);
    double range = 30;
    int getBoundsErr = localMap->getBoundsReverse(leftBound, rightBound, laneLabel, currFrontPos, range);
    if (getBoundsErr!=0){
      cerr << "LaneKeeping.cc: boundary read from map error" << endl;
      return (getBoundsErr);
    }
    
    // set the velocity profile
    velIn = 3;
    velOut = 3;
    acc = 0;
    
    // specify the ocpParams final conditions - lower bounds
    cout << "obstacle pos = " << localMap->getObstaclePoint(currFrontPos, 0) << endl;
    // final pos should be 25 m from the obstacle
    FC_finalPos = localMap->getObstaclePoint(currFrontPos, 25);
    cout << "final pos = " << FC_finalPos << endl;
    FC_velMin =  0;
    double tmp_angle;
    // TODO: Want to get heading of arbitrary point in lane
    PointLabel nextPtLabel = localMap->getNextPointID(currFrontPos);
    PointLabel ptLabel(nextPtLabel.segment, nextPtLabel.lane, nextPtLabel.point-1);
    localMap->getHeading(tmp_angle, ptLabel);
    FC_headingMin = tmp_angle-M_PI/6;
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = tmp_angle+M_PI/6; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    

    cout << "leftBound.size()" << leftBound.size() << endl;
    cout << "rightBound.size()" << rightBound.size() << endl;
    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);
    
    // Set the velocity profile - need to think this through some
    vector<double> velProfile;
    velProfile.push_back(velIn); // entry pt
    velProfile.push_back(velOut); // exit pt
    corr.setVelProfile(velProfile);
    corr.setDesiredAcc(acc);
    
    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);


    stage1 = false;
    return 0;
  }
  else {
    cout << "stage 2: fwd" << endl;
    corr.setOCPmode(0);
    LaneLabel laneLabel(currSegment.entrySegmentID, currSegment.entryLaneID);
    double range = 50;
    int getBoundsErr = localMap->getBounds(leftBound, rightBound, laneLabel, currFrontPos, range);
    if (getBoundsErr!=0){
      cerr << "LaneKeeping.cc: boundary read from map error" << endl;
      return (getBoundsErr);
    }
    
    // set the velocity profile
    velIn = 3;
    velOut = 3;
    acc = 0;
    
    // specify the ocpParams final conditions - lower bounds
    
    PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
    if (FCPos_Err!=0){
      cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      return (FCPos_Err);
    }
    cout << "final pos = " << FC_finalPos << endl;
    FC_velMin =  0;
    double tmp_angle;
    // TODO: Want to get heading of arbitrary point in lane
    localMap->getHeading(tmp_angle, exitPtLabel);
    FC_headingMin = tmp_angle-M_PI/6;
    FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
    FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
    // specify the ocpParams final conditions - upper bounds
    FC_velMax =  velOut;
    FC_headingMax = tmp_angle+M_PI/6; // unbounded
    FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
    FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    

    cout << "leftBound.size()" << leftBound.size() << endl;
    cout << "rightBound.size()" << rightBound.size() << endl;
    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // Set the velocity profile - need to think this through some
    vector<double> velProfile;
    velProfile.push_back(velIn); // entry pt
    velProfile.push_back(velOut); // exit pt
    corr.setVelProfile(velProfile);
    corr.setDesiredAcc(acc);

    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    stage1 = true;
    return 0;
  }

  return 0;


}
