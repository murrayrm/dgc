#include "LaneChange.hh"
#include "CorridorGen.hh"

LaneChange::LaneChange(int stateId, ControlStateFactory::ControlStateType type)
:ControlState(stateId, type)
{
  isReverse = 0;
}

LaneChange::~LaneChange()
{
}

void LaneChange::setLaneChangeID(int lane)
{
  desiredLaneID = lane;
}

int LaneChange::getLaneChangeID()
{
  return desiredLaneID;
}

void LaneChange::setReverse(int reverse)
{
  isReverse = reverse;
}

int LaneChange::getReverse()
{
  return isReverse;
}

int LaneChange::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  if (desiredLaneID == 0) {
    desiredLaneID = planHoriz.getSegGoal(0).exitLaneID;
    cerr << "Error in LaneChange::determineCorridor: desiredLaneID == 0" << endl;
  }
    
  if ((m_verbose) || (m_debug)) {
    cout << "In LaneChange::determineCorridor " << endl;
  }
    
  point2arr leftBound, rightBound, leftBound1, rightBound1;
  SegGoals currSegment = planHoriz.getSegGoal(0);
  double velIn, velOut, acc;
  double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
  point2 FC_finalPos;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  switch (traffState->getType()) {
    
  case TrafficStateFactory::ZONE_REGION:
    {
      // We want to plan for current segment only
      // TODO: want to do this properly in the future
      if ((m_verbose) || (m_debug)) {
	cout << "LaneChange:: Zone region" << endl;
      }
        
      PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
      if ((m_verbose) || (m_debug)) {
	cout << "exit waypt label = " << exitWayptLabel << endl;
      }
        
      point2 exitWaypt;
      int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
      if (wayptErr != 0) {
	cerr << "LaneChange.cc: waypt read from map error" << endl;
	return (wayptErr);
      }

      if ((m_verbose) || (m_debug)) {
	cout << "exit waypt pos = " << exitWaypt << endl;
      }
        
      // Get our current position
      currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
      if ((m_verbose) || (m_debug)) {
	cout << "current pos = " << currFrontPos << endl;
      }
        
      // Create a corridor of LaneWidth from current position to entry point
      double corrHalfWidth = 3;
      double theta = atan2(exitWaypt.y - currFrontPos.y, exitWaypt.x - currFrontPos.x);
      point2 temppt;
        
      // Left Boundary
      // pt 1 on left boundary
      temppt.x = currFrontPos.x + corrHalfWidth * cos(theta + M_PI / 2);
      temppt.y = currFrontPos.y + corrHalfWidth * sin(theta + M_PI / 2);
      leftBound.push_back(temppt);
      // pt 2 on left boundary
      temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
      temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
      leftBound.push_back(temppt);
      // Right Boundary
      // pt 1 on right boundary
      temppt.x = currFrontPos.x + corrHalfWidth * cos(theta - M_PI / 2);
      temppt.y = currFrontPos.y + corrHalfWidth * sin(theta - M_PI / 2);
      rightBound.push_back(temppt);
      // pt 2 on right boundary
      temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
      temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
      rightBound.push_back(temppt);

      // TODO: Take some subset of these boundary points as the corridor

      // Specify the velocity profile
      velIn = 3;
      velOut = 3;
      acc = 0;

      // specify the ocpParams final conditions - lower bounds
      FC_finalPos = exitWaypt;
      FC_velMin = 0;

      double heading;
      localMap->getHeading(heading,exitWayptLabel);

      FC_headingMin = heading;  // unbounded
      FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
      FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
      // specify the ocpParams final conditions - upper bounds
      FC_velMax = velOut;
      FC_headingMax = heading;   // unbounded
      FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
      FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

      /* IC_velMin = max(velIn, AliceStateHelper::getVelocityMag(vehState));
	 IC_velMax = IC_velMin; */    
    }
    break;

  case TrafficStateFactory::ROAD_REGION:
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      LaneLabel destLane(currSegment.exitSegmentID,desiredLaneID);
      cout << "LaneChange corridor: destination lane: " << destLane << endl;
      point2 initPos = this->getInitialPosition();
      int cErr = CorridorGen::makeCorridorLaneChange(corr, vehState, traffState, planHoriz, localMap, destLane, initPos, isReverse);
      corr.setVelProfile(localMap, vehState, traffState, this, planHoriz);
      if (cErr != 0)
	{
	  cerr << "LaneChange corridor generation failed: " << cErr << endl;
	  return cErr;
	}
      return 0;
    }
    break;

  case TrafficStateFactory::INTERSECTION_STOP:
    {
      // We want to plan over two segments
      SegGoals nextSegGoal = planHoriz.getSegGoal(1);
      point2arr leftBound1, rightBound1, leftBound2, rightBound2;
      // Set of boundary points due to intersection lane
      PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
      PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

      double range = 50;
      int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
      if (interBoundErr != 0) {
	cerr << "LaneChange.cc: Intersection boundary read from map error" << endl;
	return (interBoundErr);
      }
        
      // Set of boudary pts due to lane after intersection
      /* LaneLabel laneLabel(segment2.entrySegmentID, segment2.entryLaneID);      
	 int rightBoundErr = localMap->getRightBound(rightBound2, laneLabel); 
	 int leftBoundErr = localMap->getLeftBound(leftBound2, laneLabel);
	 if (rightBoundErr!=0){
	 cerr << "LaneKeeping.cc: right boundary read from map error" << endl;
	 return rightBoundErr;
	 }     
	 if (leftBoundErr!=0){
	 cerr << "LaneKeeping.cc: left boundary read from map error" << endl;
	 return leftBoundErr;
	 } */
        
      // Add the boundaries together
      leftBound = leftBound1;
      rightBound = rightBound1;
      /* for (unsigned i=0; i<leftBound2.size(); i++){
	 if (leftBound.back()!=leftBound2[i]){
	 leftBound.push_back(leftBound2[i]);
	 }
	 }
	 for (unsigned i=0; i<rightBound2.size(); i++){
	 if (rightBound.back()!=rightBound2[i]){
	 rightBound.push_back(rightBound2[i]);
	 }
	 } */

      // TODO: take some subset of these boundary points as the corridor

      // Specify the velocity profile
      velIn = currSegment.maxSpeedLimit;
      velOut = currSegment.maxSpeedLimit;
      acc = 0;

      // Specify the ocpParams final conditions - lower bounds
      // Do we want to define this position based on the corridor?
      PointLabel exitPtLabel(nextSegGoal.exitSegmentID, nextSegGoal.exitLaneID, nextSegGoal.exitWaypointID);
      //int FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
      //if (FCPos_Err != 0) {
      //    cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
      //    return (FCPos_Err);
      //}
      FC_finalPos.set((rightBound.back().x+leftBound.back().x)/2,
		      (rightBound.back().y+leftBound.back().y)/2);
      FC_velMin = 0;

      double heading;
      localMap->getHeading(heading,exitPtLabel);

      FC_headingMin = heading;  // unbounded
      FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
      FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
      // specify the ocpParams final conditions - upper bounds
      FC_velMax = velOut;
      FC_headingMax = heading;   // unbounded
      FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
      FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
    break;
        
  default:
    
    cerr << "LaneChange.cc: Undefined Traffic state" << endl;
        
  }
    
  // Assign lines to corridor variables
  corr.addPolyline(leftBound);
  corr.addPolyline(rightBound);

  // Set the velocity profile - need to think this through some
  //  vector < double >velProfile;
  //  velProfile.push_back(velIn);        // entry pt
  //  velProfile.push_back(velOut);       // exit pt
  //  corr.setVelProfile(velProfile);
  //  corr.setDesiredAcc(acc);

  // set the final conditions in the ocpspecs
  corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
  corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
  corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
  corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
  // specify the ocpParams final conditions - upper bounds
  corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
  corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
  corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
  corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
  corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
  corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

  return 0;
}

