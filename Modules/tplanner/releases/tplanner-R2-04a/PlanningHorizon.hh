#ifndef PLANNINGHORIZON_HH_
#define PLANNINGHORIZON_HH_


#include "mapping/Road.hh" 
#include "gcinterfaces/SegGoals.hh" 
#include "mapping/Obstacle.hh" 
#include "map/Map.hh"
#include <list>
#include <vector>

class PlanningHorizon 
{
public: 

  PlanningHorizon(const PlanningHorizon &p);
  PlanningHorizon();
  ~PlanningHorizon();

  PlanningHorizon& operator=(const PlanningHorizon&);

  std::vector<Road> getRoadObjects();
  std::vector<Obstacle> getObstacles();
  int getExitLaneId(SegGoals segGoal);
  SegGoals getSegGoal(int i);
  Line* getStopLine(int segmentId, int laneId);
  void addGoal(SegGoals segGoal);
  int getSize();

  int getCurrentExitSegment();
  int getCurrentExitLane();
  int getCurrentExitWaypoint();
  void clear();
  void resetIllegalPassing();

private:

  list<SegGoals> m_segGoals; 
  std::vector<Road> m_roads;
  std::vector<Obstacle> m_obstacles;
  std::vector<Line> m_lines;
  //Map* m_map;

};

#endif /*PLANNINGHORIZON_HH_*/
