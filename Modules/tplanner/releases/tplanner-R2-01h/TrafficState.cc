#include "TrafficState.hh"
bool TrafficState::m_verbose = false;
bool TrafficState::m_debug = false;
bool TrafficState::m_log = false;


TrafficState::TrafficState(int stateId, TrafficStateFactory::TrafficStateType type)
  : State(stateId, type)
{
  // TODO: need to fix this up
  //m_verbose = true;
  //m_debug = true;
  //m_logging = true;
}

TrafficState::TrafficState()
{ 

}


TrafficState::~TrafficState()
{ 

}

void TrafficState::setOutputParams(bool debug, bool verbose, bool log)
{
  m_verbose = verbose;
  m_debug = debug;
  m_log = log;
  //  if (m_debug)
  //  cout << "in TrafficState.cc: debug switched on" << endl;
}
