#ifndef STATE_HH_
#define STATE_HH_

class State  {

 public:
 
  State(int stateId);
  State(int stateId, int type);
  State();
  virtual ~State();
  int getStateId();
  void setStateId(int stateId);
  void setUncertainty(double uncertainty);
  double getUncertainty();
  int getType();

private:
  
  int m_stateId; 
  int m_type;
  double m_uncertainty;
  
};

#endif /*STATE_HH_*/
