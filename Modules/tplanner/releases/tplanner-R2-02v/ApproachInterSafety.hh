#ifndef APPROACHINTERSAFETY_HH_
#define APPROACHINTERSAFETY_HH_

#include "TrafficState.hh"

/** ApproachInterSafety defines a region of road that does not include an intersection, or an approach to an intersection */

class ApproachInterSafety:public TrafficState {

  public:

    ApproachInterSafety(int stateId, TrafficStateFactory::TrafficStateType type);
    ~ApproachInterSafety();

  TrafficState* newCopy() {
    return (TrafficState*) new ApproachInterSafety(*this);
  }

  private:

};

#endif                          /*APPROACHINTERSAFETY_HH_ */
