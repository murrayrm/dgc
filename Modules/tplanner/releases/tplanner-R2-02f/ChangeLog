Mon Apr 30 13:20:01 2007	Noel duToit (ndutoit)

	* version R2-02f
	BUGS:  
	New files: Creep.cc Creep.hh CreepToStop.cc CreepToStop.hh
		DummyMissionPlanner.cc StoppedToCreep.cc StoppedToCreep.hh
		TrafficStateEst.cc TrafficStateEst.hh UT_TrafficManager.cc
		costViewer2D.cc testCorridor.cc
		tplanner_unittest_rightleft.mdf
	FILES: ControlState.cc(20979), ControlState.hh(20979),
		ControlStateFactory.cc(20979), Stopped.cc(20979),
		Stopped.hh(20979), TrafficPlanner.cc(20979)
	Added cleaner failure interface. The failure is now reported at the
	ControlState level.

	FILES: ControlState.hh(21008), Corridor.cc(21008),
		Corridor.hh(21008)
	updated the corridor function to specify a velocity profile. This
	is a shell function still (vel profile) and will be populated by
	Russell.

	FILES: ControlStateFactory.cc(20725),
		ControlStateFactory.hh(20725), StateGraph.cc(20725),
		StopToStopped.cc(20725), TrafficPlanner.cc(20725),
		TrafficPlannerMain.cc(20725),
		TrafficStateFactory.cc(20725),
		TrafficStateFactory.hh(20725)
	fixed state initialization. Fixed transitions to use stateId
	instead of state type. Still needs more debugging.

	FILES: ControlStateFactory.cc(20928), Corridor.cc(20928),
		TrafficPlanner.cc(20928), TrafficPlannerControl.cc(20928),
		TrafficStateFactory.hh(20928)
	Fixed possible segfault in TrafficPlanner

	FILES: ControlStateFactory.cc(20955), Log.cc(20955), Log.hh(20955),
		Makefile.yam(20955), RoadToApproachInterSafety.cc(20955),
		TrafficPlanner.cc(20955)
	Added Log and Creep

	FILES: ControlStateFactory.cc(20970),
		ControlStateFactory.hh(20970), Corridor.cc(20970),
		Corridor.hh(20970), Makefile.yam(20970)
	Merged Corridor and added Creep control state

	FILES: ControlStateFactory.cc(21385), CorridorGenerator.cc(21390),
		CorridorGenerator.hh(21389), Interfaces.hh(21387),
		PlanningHorizon.cc(21386), PlanningHorizon.hh(21392),
		StateGraph.hh(21393), TrafficManager.cc(21391),
		TrafficManager.hh(21388)
	finished coding TrafficManager

	FILES: Corridor.cc(20736), Corridor.hh(20736), Makefile.yam(20736),
		TrafficPlanner.cc(20736), TrafficPlannerMain.cc(20736)
	added cost map painting based on lane bounds.  Also added
	costViewer2D cost map visualization program. 

	FILES: Corridor.cc(20957), Corridor.hh(20957)
	added velocity profiling to corridor.cc

	FILES: Corridor.cc(21079), Corridor.hh(21079),
		TrafficPlanner.cc(21079)
	added shell for setVelProfile function

	FILES: Corridor.cc(21131), Corridor.hh(21131),
		CorridorGenerator.hh(21131), TrafficManager.hh(21131)
	fixed TrafficManager and CorridorGenerator dependency on travgraph.

	FILES: Corridor.cc(21156)
	Implemented setVelProfile function

	FILES: Corridor.cc(21160)
	Fixed bugs in initial implementation of Corridor.hh

	FILES: Corridor.cc(21241), Corridor.hh(21241), testCState.cc(21241)
	Implemented velocity profile specification. Rewrote convToRddf()
	assuming paired pts on the boundaries.

	FILES: Corridor.cc(21260), TrafficPlanner.cc(21260)
	implemented nominal velocity profile specification. Added velocity
	profile generation in trafficplanner.cc

	FILES: Corridor.cc(21285), UTurn.cc(21285), UTurnToUTurn.cc(21285)
	debugged uturn transitions

	FILES: Corridor.cc(21342), LaneChange.cc(21342),
		LaneKeeping.cc(21342), Stop.cc(21342), Stopped.cc(21342),
		TrafficUtils.cc(21342), TrafficUtils.hh(21342),
		UTurn.cc(21342)
	fixed heading upper and lower bound spec in fc.

	FILES: Corridor.cc(21482), Corridor.hh(21482),
		TrafficPlanner.cc(21482), testCState.cc(21482)
	sending rddf's in local coordinates now. Same for IC anc FC's.

	FILES: Corridor.cc(21527), TrafficPlanner.cc(21527)
	debugging the cost map painting

	FILES: Corridor.cc(21582), Corridor.hh(21582), Makefile.yam(21582),
		TrafficPlanner.cc(21582)
	Polytopic corridor generation merged into tplanner. The changes are
	in Corridor.cc, Corridor.hh, Makefile.yam. A test program,
	testCorridor.cc has also been added.

	FILES: Corridor.cc(21631)
	took out delete from destructor

	FILES: Corridor.cc(21721), Corridor.hh(21721), Stopped.cc(21721),
		TrafficPlanner.cc(21721), tplanner_unittest.rndf(21721)
	updated velocity profile spec for stopped state. Also fixed bug in
	stopped state FC spec.

	FILES: CorridorGen.cc(21013), CorridorGen.hh(21013)
	Added LaneChange to CorridorGen

	FILES: CorridorGen.cc(21259), LaneChange.cc(21259),
		LaneKeeping.cc(21259), testCState.cc(21259)
	Worked on lanechanging

	FILES: CorridorGen.cc(21487)
	Re-enabled basic lane change code

	FILES: CorridorGenerator.cc(21109), CorridorGenerator.hh(21110),
		Interfaces.hh(21110), PlanningHorizon.hh(21110),
		TrafficManager.cc(21109), TrafficManager.hh(21110),
		TrafficPlannerControl.cc(21109), TrafficState.cc(21109),
		TrafficState.hh(21110)
	merged CSS tplanner and new logic for obs avoidance

	FILES: Interfaces.hh(21150), Makefile.yam(21152),
		TrafficManager.cc(21153), TrafficManager.hh(21151),
		TrafficPlannerMainCSS.cc(21149)
	reverted to old stateestimator for support of non CSS

	FILES: Intersection_LaneKeepingToStop.cc(20972),
		TrafficPlanner.cc(20972), tplanner_unittest.rndf(20972)
	debugging the logic of tplanner

	FILES: LaneChange.cc(21238)
	Now uses CorridorGen::makeCorridorLaneChange

	FILES: LaneChange.cc(21486)
	Fixed finalPos bug

	FILES: LaneKeeping.cc(21112), LaneKeeping.hh(21112)
	Changed code to use CorridorGen

	FILES: Log.cc(20959), TrafficPlanner.cc(20959)
	Log (still buggy)

	FILES: Log.cc(21093), TrafficPlanner.cc(21093),
		TrafficPlannerControl.cc(21093), UTurn.cc(21093)
	Debugging UTurn and Log

	FILES: Log.cc(21099)
	Log.cc now compiles

	FILES: Log.cc(21119), Log.hh(21119), TrafficPlanner.cc(21119)
	Added Log (working)

	FILES: Log.cc(21145), Log.hh(21145), TrafficPlanner.cc(21145),
		UTurn.cc(21145), UTurnToUTurn.cc(21145)
	Log is now a standard class (as opposed to a static class) + UTurn
	corridor fixed

	FILES: Makefile.yam(20715)
	Fixed makefile

	FILES: Makefile.yam(20754), testCState.cc(20754)
	updated Makefile.yam to compile the testCState function. This will
	allow us to test the corridor generation separately.

	FILES: Makefile.yam(21437), TrafficManager.cc(21434),
		TrafficManager.hh(21435), TrafficPlannerMainCSS.cc(21433)
	cleared warnings

	FILES: Makefile.yam(21591)
	added test files

	FILES: TrafficManager.cc(20726)
	arbitrate method fixes

	FILES: TrafficManager.cc(21590), TrafficManager.hh(21589)
	took map out

	FILES: TrafficPlanner.cc(21014), TrafficPlannerMain.cc(21014)
	Fixed map bug (you should check the latest module of interfaces)

	FILES: TrafficPlanner.cc(21629)
	fixed corr instantiation

	FILES: TrafficPlannerControl.cc(21162)
	UTurn fix (due to a bug in TrafficPlannerControl)

	FILES: TrafficPlannerMain.cc(21630)
	took out polytope corridor

	FILES: TrafficUtils.cc(21237), TrafficUtils.hh(21237)
	Added insertPtAtDistance function

	FILES: TrafficUtils.cc(21240)
	Fixed insertPtAtDistance

	FILES: testCState.cc(20756)
	fixed small bug in testCState.cc to avoid segfault

	FILES: testCState.cc(21135)
	Now correctly reads skynet id from command line

	FILES: testCState.cc(21137)
	testCState now takes skynet id and rndf as input arguments

Wed Apr 25 16:40:03 2007	vcarson (vcarson)

	* version R2-02e
	BUGS:  
	FILES: ControlStateFactory.cc(20698), LaneChange.cc(20698)
	Used LaneKeeping in LaneChange for now. Remove useful Passing2 and
	4

	FILES: CorridorGenerator.cc(20704), CorridorGenerator.hh(20703),
		TrafficManager.cc(20702), TrafficManager.hh(20701)
	fixed with new gcmodule interface

	FILES: LaneChange.cc(20699), LaneChange.hh(20699),
		LaneChangeToLaneKeeping.cc(20699)
	Fixed compilation errors

	FILES: StoppedToPassing_LC.cc(20697)
	Added missing file

Wed Apr 25 15:31:33 2007	Noel duToit (ndutoit)

	* version R2-02d
	BUGS:  
	New files: CorridorGen.cc CorridorGen.hh
		Intersection_LaneKeepingToStop.cc
		Intersection_LaneKeepingToStop.hh LaneChange.cc
		LaneChange.hh LaneChangeToLaneKeeping.cc
		LaneChangeToLaneKeeping.hh LaneKeepingToLaneChange.cc
		LaneKeepingToLaneChange.hh LaneKeepingToStop.cc
		LaneKeepingToStop.hh Passing1_LaneKeepingToLaneChange.cc
		Passing1_LaneKeepingToLaneChange.hh
		Passing2_LaneChangeToLaneKeeping.cc
		Passing2_LaneChangeToLaneKeeping.hh
		Passing3_LaneKeepingToLaneChange.cc
		Passing3_LaneKeepingToLaneChange.hh
		Passing4_LaneChangeToLaneKeeping.cc
		Passing4_LaneChangeToLaneKeeping.hh
		Passing_LaneKeepingToLaneKeeping.cc
		Passing_LaneKeepingToLaneKeeping.hh StoppedToPassing_LC.cc
		StoppedToPassing_LC.hh
	FILES: ControlStateFactory.cc(20658),
		ControlStateFactory.hh(20658), Makefile.yam(20658)
	merging vanessa and noel's branches

	New files: CorridorGen.cc CorridorGen.hh
		Intersection_LaneKeepingToStop.cc
		Intersection_LaneKeepingToStop.hh LaneChange.cc
		LaneChange.hh LaneChangeToLaneKeeping.cc
		LaneChangeToLaneKeeping.hh LaneKeepingToLaneChange.cc
		LaneKeepingToLaneChange.hh LaneKeepingToStop.cc
		LaneKeepingToStop.hh Passing1_LaneKeepingToLaneChange.cc
		Passing1_LaneKeepingToLaneChange.hh
		Passing2_LaneChangeToLaneKeeping.cc
		Passing2_LaneChangeToLaneKeeping.hh
		Passing3_LaneKeepingToLaneChange.cc
		Passing3_LaneKeepingToLaneChange.hh
		Passing4_LaneChangeToLaneKeeping.cc
		Passing4_LaneChangeToLaneKeeping.hh
		Passing_LaneKeepingToLaneKeeping.cc
		Passing_LaneKeepingToLaneKeeping.hh StoppedToPassing_LC.cc
		StoppedToPassing_LC.hh
	FILES: ControlStateFactory.cc(20524), ControlStateFactory.hh(20524)
	updated control state machine. Defined transitions and states
	required for extension to static obstacle. Created shell for lane
	changing code.

	FILES: ControlStateFactory.cc(20527), Makefile.yam(20527)
	fixed a few bugs

	FILES: ControlStateFactory.cc(20528), Makefile.yam(20528)
	Added cc shell

	FILES: ControlStateFactory.cc(20563), Makefile.yam(20563)
	Added a Intersection_LaneKeepingToStop transition and implemented
	most of the transitions

	FILES: ControlStateFactory.cc(20577), Makefile.yam(20577)
	Added a fallback possibilty while passing:
	Passing_LaneKeepingToLaneKeeping

	FILES: Makefile.yam(20522)
	Shells of new Transitions done

	FILES: Makefile.yam(20525)
	CorridorGen class added - parameterizing corridor generation
	functions

Wed Apr 25 13:51:38 2007	vcarson (vcarson)

	* version R2-02c
	BUGS:  
	New files: Interfaces.hh TrafficPlannerMainCSS.cc
	FILES: CorridorGenerator.cc(20621), CorridorGenerator.hh(20620),
		Makefile.yam(20624), TrafficManager.cc(20619),
		TrafficManager.hh(20618), cmdline.c(20615),
		cmdline.h(20615), tplanner.ggo(20616)
	merged

	New files: Interfaces.hh TrafficPlannerMainCSS.cc
	FILES: CorridorGenerator.cc(20606), CorridorGenerator.hh(20605),
		Makefile.yam(20604), TrafficManager.cc(20603),
		TrafficManager.hh(20602), cmdline.c(20599),
		cmdline.h(20601), tplanner.ggo(20600)
	CSS port

Tue Apr 24 11:55:54 2007	Noel duToit (ndutoit)

	* version R2-02b
	BUGS:  
	FILES: AliceStateHelper.cc(20418), AliceStateHelper.hh(20418),
		ApproachInterSafeToInterStop.cc(20418),
		InterStopToRoadRegion.cc(20418), LaneKeeping.cc(20418),
		RoadToApproachInterSafety.cc(20418),
		TrafficPlanner.cc(20418), ZoneToRoadRegion.cc(20418)
	updated mission complete to use absolute distance and heading as
	well as hyperplane checking. Will have to be even more careful.
	Also had to change some things in traffic state transitions.
	Lastly, changed the final pos spec.

Mon Apr 23 18:39:53 2007	Noel duToit (ndutoit)

	* version R2-02a
	BUGS:  
	New files: Log.cc Log.hh UTurnToUTurn.cc UTurnToUTurn.hh
	FILES: ApproachInterSafeToInterStop.cc(20371),
		ApproachInterSafeToInterStop.hh(20371),
		ApproachInterSafety.cc(20371),
		ApproachInterSafety.hh(20371), ControlState.cc(20371),
		ControlState.hh(20371), ControlStateFactory.cc(20371),
		ControlStateFactory.hh(20371),
		ControlStateTransition.cc(20371),
		ControlStateTransition.hh(20371), Corridor.hh(20371),
		InterStopToRoadRegion.cc(20371),
		InterStopToRoadRegion.hh(20371),
		IntersectionStop.cc(20371), IntersectionStop.hh(20371),
		LaneKeepToStop.cc(20371), LaneKeepToStop.hh(20371),
		LaneKeeping.cc(20371), LaneKeeping.hh(20371),
		LaneKeepingToSlowDown.cc(20371),
		LaneKeepingToSlowDown.hh(20371), Makefile.yam(20371),
		RoadRegion.cc(20371), RoadRegion.hh(20371),
		RoadToApproachInterSafety.cc(20371),
		RoadToApproachInterSafety.hh(20371),
		RoadToRoadRegion.cc(20371), RoadToRoadRegion.hh(20371),
		SlowDown.cc(20371), SlowDown.hh(20371),
		SlowDownToStop.cc(20371), SlowDownToStop.hh(20371),
		Stop.cc(20371), Stop.hh(20371), StopToStopped.cc(20371),
		StopToStopped.hh(20371), Stopped.cc(20371),
		Stopped.hh(20371), StoppedToLaneKeeping.cc(20371),
		StoppedToLaneKeeping.hh(20371), StoppedToUTurn.cc(20371),
		StoppedToUTurn.hh(20371), TrafficPlanner.cc(20371),
		TrafficPlanner.hh(20371), TrafficState.cc(20371),
		TrafficState.hh(20371), TrafficStateFactory.cc(20371),
		TrafficStateFactory.hh(20371),
		TrafficStateTransition.cc(20371),
		TrafficStateTransition.hh(20371), UTurn.cc(20371),
		UTurn.hh(20371), UTurnToLaneKeeping.cc(20371),
		UTurnToLaneKeeping.hh(20371), ZoneRegion.cc(20371),
		ZoneRegion.hh(20371), ZoneToRoadRegion.cc(20371),
		ZoneToRoadRegion.hh(20371)
	Sven's cleaning up of code

	New files: Log.cc Log.hh UTurnToUTurn.cc UTurnToUTurn.hh
	FILES: ApproachInterSafeToInterStop.cc(20058),
		ApproachInterSafeToInterStop.hh(20058),
		ApproachInterSafety.cc(20058),
		ApproachInterSafety.hh(20058), ControlState.cc(20058),
		ControlState.hh(20058), ControlStateFactory.cc(20058),
		ControlStateFactory.hh(20058),
		ControlStateTransition.cc(20058),
		ControlStateTransition.hh(20058),
		InterStopToRoadRegion.cc(20058),
		InterStopToRoadRegion.hh(20058),
		IntersectionStop.cc(20058), IntersectionStop.hh(20058),
		LaneKeepToStop.cc(20058), LaneKeepToStop.hh(20058),
		LaneKeeping.cc(20058), LaneKeeping.hh(20058),
		LaneKeepingToSlowDown.cc(20058),
		LaneKeepingToSlowDown.hh(20058), RoadRegion.cc(20058),
		RoadRegion.hh(20058), RoadToApproachInterSafety.cc(20058),
		RoadToApproachInterSafety.hh(20058),
		RoadToRoadRegion.cc(20058), RoadToRoadRegion.hh(20058),
		SlowDown.cc(20058), SlowDown.hh(20058),
		SlowDownToStop.cc(20058), SlowDownToStop.hh(20058),
		Stop.cc(20058), Stop.hh(20058), StopToStopped.cc(20058),
		StopToStopped.hh(20058), Stopped.cc(20058),
		Stopped.hh(20058), StoppedToLaneKeeping.cc(20058),
		StoppedToLaneKeeping.hh(20058), StoppedToUTurn.cc(20058),
		StoppedToUTurn.hh(20058), TrafficStateFactory.cc(20058),
		TrafficStateFactory.hh(20058),
		TrafficStateTransition.cc(20058),
		TrafficStateTransition.hh(20058), UTurn.cc(20058),
		UTurn.hh(20058), UTurnToLaneKeeping.cc(20058),
		UTurnToLaneKeeping.hh(20058), ZoneRegion.cc(20058),
		ZoneRegion.hh(20058), ZoneToRoadRegion.cc(20058),
		ZoneToRoadRegion.hh(20058)
	Changed if statements for switch (faster and easier to understand)
	Removed identical code lines across if statements and combined them
	into the switch statements Most code is now cleaned up

	FILES: ApproachInterSafeToInterStop.cc(20105),
		ControlStateFactory.cc(20105),
		LaneKeepingToSlowDown.cc(20105), Makefile.yam(20105),
		SlowDown.cc(20105), SlowDownToStop.cc(20105),
		StopToStopped.cc(20105), StopToStopped.hh(20105),
		Stopped.cc(20105), Stopped.hh(20105),
		StoppedToLaneKeeping.cc(20105),
		StoppedToLaneKeeping.hh(20105), StoppedToUTurn.cc(20105),
		StoppedToUTurn.hh(20105), TrafficStateFactory.cc(20105),
		TrafficStateFactory.hh(20105),
		TrafficStateTransition.cc(20105),
		TrafficStateTransition.hh(20105), UTurn.cc(20105),
		UTurn.hh(20105), UTurnToLaneKeeping.cc(20105),
		UTurnToLaneKeeping.hh(20105), ZoneRegion.cc(20105),
		ZoneRegion.hh(20105), ZoneToRoadRegion.cc(20105),
		ZoneToRoadRegion.hh(20105)
	All traffic and control states as well as transitions should be
	clean UTurn is a little cleaner (actually defined by 5 instances of
	UTurn), but could be better if I can modify Vanessa's file

	FILES: ControlState.cc(20334), ControlState.hh(20334),
		Corridor.hh(20334), Makefile.yam(20334),
		TrafficPlanner.cc(20334), TrafficPlanner.hh(20334),
		TrafficState.cc(20334), TrafficState.hh(20334),
		TrafficStateFactory.hh(20334)
	Added a Log class (had to modify a little TrafficPlanner). We can
	now easily log the current states and segment

Mon Apr 23 18:07:55 2007	Noel duToit (ndutoit)

	* version R2-01j
	BUGS:  
	New files: CorridorGenerator.cc CorridorGenerator.hh
		TrafficManager.cc TrafficManager.hh
	FILES: Makefile.yam(20358)
	merging branches, releases h and i

	New files: CorridorGenerator.cc CorridorGenerator.hh
		TrafficManager.cc TrafficManager.hh
	FILES: Makefile.yam(20346)
	converted to CSS

Sat Apr 21 19:20:23 2007	murray (murray)

	* version R2-01i
	BUGS:  
	FILES: Makefile.yam(20214), costViewer.cc(20214), zpr.h(20214)
	updated for OS X to properly compile against OpenGL and GLUT

	FILES: stluke_ft2.rndf(20211),
		tplanner_unittest_straight.mdf(20211)
	removed extraneous executable property

Thu Apr 12 16:31:44 2007	Noel duToit (ndutoit)

	* version R2-01h
	BUGS:  
	FILES: ControlStateTransition.hh(19428), Makefile.yam(19428),
		PlanningHorizon.hh(19428), TrafficPlanner.hh(19428),
		testCState.cc(19428)
	Fixed the include statements and Makefile due to the changes in the
	interfaces and gcinterfaces modules

	FILES: TrafficPlanner.cc(19453)
	commented out the cost map generation and sending for now (but left
	it in the correct place).

	FILES: TrafficPlanner.cc(19468), TrafficPlanner.hh(19468),
		TrafficPlannerMain.cc(19468)
	fixed bug related to '--disable-console' flag.

Wed Apr 11 15:46:03 2007	Noel duToit (ndutoit)

	* version R2-01g
	BUGS: 
	FILES: TrafficPlanner.cc(19326)
	moved cost map generation to correct place in planning loop. Also
	implemented full map request functionality. Will require more
	testing, but currently waiting for mapviewer functionality.

Tue Apr 10  8:40:01 2007	Noel duToit (ndutoit)

	* version R2-01f
	BUGS: 
	FILES: ApproachInterSafeToInterStop.cc(19242),
		ControlState.cc(19242), ControlState.hh(19242),
		ControlStateFactory.cc(19242),
		ControlStateFactory.hh(19242),
		ControlStateTransition.cc(19242),
		ControlStateTransition.hh(19242), Corridor.cc(19242),
		Corridor.hh(19242), InterStopToRoadRegion.cc(19242),
		LaneKeeping.cc(19242), Makefile.yam(19242),
		RoadToApproachInterSafety.cc(19242),
		RoadToRoadRegion.cc(19242), SlowDown.cc(19242),
		Stop.cc(19242), StopToStopped.cc(19242), Stopped.cc(19242),
		StoppedToLaneKeeping.cc(19242), StoppedToUTurn.cc(19242),
		TrafficPlanner.cc(19242), TrafficPlanner.hh(19242),
		TrafficPlannerControl.cc(19242),
		TrafficPlannerControl.hh(19242),
		TrafficPlannerMain.cc(19242), TrafficState.cc(19242),
		TrafficState.hh(19242), TrafficStateEstimator.cc(19242),
		TrafficStateEstimator.hh(19242),
		TrafficStateFactory.cc(19242),
		TrafficStateFactory.hh(19242),
		TrafficStateTransition.cc(19242),
		TrafficStateTransition.hh(19242), UTurn.cc(19242),
		ZoneToRoadRegion.cc(19242), cmdline.c(19242),
		cmdline.h(19242), tplanner.ggo(19242)
	adding display and output parameters (such as debug, verbose,
	logging).

	FILES: ControlState.cc(19251), ControlStateTransition.cc(19251),
		Corridor.cc(19251), StateGraph.cc(19251),
		TrafficPlanner.cc(19251), TrafficPlanner.hh(19251),
		TrafficState.cc(19251), TrafficStateTransition.cc(19251)
	initial implementation of console and added some debugging and
	verbose functionality. Also added some logging flags, but need to
	implement that next.

Mon Mar 19 14:21:09 2007	Noel duToit (ndutoit)

	* version R2-01e
	BUGS: 
	FILES: TrafficPlanner.cc(18575), UTurn.cc(18575),
		testCState.cc(18575)
	post field test release. Changed vel profile at end of mission

Sun Mar 18  8:53:10 2007	Noel duToit (ndutoit)

	* version R2-01d
	BUGS: 
	FILES: Makefile.yam(18417), UTurnToLaneKeeping.cc(18417)
	made switching out of uturn more robust. It now switches out of
	that mode no matter what traffic state we are in

	FILES: Makefile.yam(18418)
	small bug in make file fixed

Sat Mar 17 20:25:52 2007	Noel duToit (ndutoit)

	* version R2-01c
	BUGS: 
	New files: stluke_ft2.rndf zpr.c zpr.h
	FILES: Corridor.cc(18363), TrafficPlanner.cc(18363),
		TrafficPlannerMain.cc(18363), costViewer.cc(18363)
	costViewer is now in color!! also, since noel said there were some
	issues with the getStaticCostMapThread, i've left that commented
	out.

	FILES: Makefile.yam(18358)
	made slight modification to the makefile to account for the new 3d
	visualization in costViewer

	FILES: RoadToApproachInterSafety.cc(18383),
		SlowDownToStop.cc(18383), Stop.cc(18383),
		Stopped.cc(18383), TrafficPlanner.cc(18383),
		UTurn.cc(18383)
	made tplanner more robust with intersection handling and tested
	system as a whole in simulation.

	FILES: SlowDownToStop.cc(18351), Stopped.cc(18351),
		StoppedToUTurn.cc(18351), UTurn.cc(18351), UTurn.hh(18351),
		UTurnToLaneKeeping.cc(18351), UTurnToLaneKeeping.hh(18351),
		testCState.cc(18351)
	still debugging uturn, but the first 4 stages work, the last one
	doesnt yet. Still working on it.

	FILES: UTurn.cc(18364), UTurnToLaneKeeping.cc(18364)
	uturn function that works

	FILES: costViewer.cc(18357)
	modified costViewer to do three dimensional viewing of the cost

Sat Mar 17 13:44:49 2007	Noel duToit (ndutoit)

	* version R2-01b
	BUGS: 
	New files: Makefile.testCState costViewer.cc
	FILES: ApproachInterSafeToInterStop.cc(18300), Corridor.cc(18300),
		InterStopToRoadRegion.cc(18300),
		LaneKeepingToSlowDown.cc(18300), Makefile.yam(18300),
		SlowDown.cc(18300), SlowDownToStop.cc(18300),
		Stop.cc(18300), Stopped.cc(18300), UTurn.cc(18300),
		testCState.cc(18300), tplanner_unittest.rndf(18300)
	bugs fixed for intersections. mplanner-tplanner interface for uturn
	tested. uturn needs more debugging

	FILES: Corridor.cc(18159), Corridor.hh(18159),
		LaneKeeping.cc(18159), Makefile.yam(18159),
		SlowDown.cc(18159), Stop.cc(18159), Stopped.cc(18159),
		StoppedToUTurn.cc(18159), TrafficPlanner.cc(18159),
		TrafficPlanner.hh(18159), TrafficPlannerMain.cc(18159),
		TrafficUtils.hh(18159), UTurn.cc(18159), UTurn.hh(18159),
		singleroad.rndf(18159), testobs.mdf(18159)
	merged nok and my branch. main thing was switching seggoals to
	skynet talker instead of seggoals talker.

Fri Mar 16 12:07:47 2007	Nok Wongpiromsarn (nok)

	* version R2-01a
	BUGS: 
	FILES: TrafficPlanner.cc(18007), TrafficPlanner.hh(18007),
		testCState.cc(18007)
	Traffic planner that uses skynettalker to communite with mission
	planner.

Fri Mar 16 23:02:19 2007	Noel duToit (ndutoit)

	* version R2-00d-ndutoit
	BUGS: 
	New files: Makefile.testCState costViewer.cc
	FILES: Corridor.cc(18076), Corridor.hh(18076),
		LaneKeeping.cc(18076), SlowDown.cc(18076), Stop.cc(18076),
		UTurn.cc(18076), UTurn.hh(18076), testCState.cc(18076)
	working on uturn. implementing state machine inside uturn

	FILES: Corridor.cc(18083), Corridor.hh(18083), Makefile.yam(18083),
		TrafficPlanner.cc(18083), TrafficPlanner.hh(18083)
	merged changes; tplanner now generates cost maps from objects sent
	from mapper and sends them to dplanner

	FILES: Corridor.cc(18153), Makefile.yam(18153), SlowDown.cc(18153),
		Stopped.cc(18153), StoppedToUTurn.cc(18153),
		TrafficPlanner.cc(18153), TrafficPlanner.hh(18153),
		TrafficPlannerMain.cc(18153), TrafficUtils.hh(18153),
		UTurn.cc(18153), UTurn.hh(18153), singleroad.rndf(18153),
		testCState.cc(18153), testobs.mdf(18153)
	implemented the first two stages on uturn. In the process of
	linking up with mplanner.

	FILES: Makefile.yam(18085), TrafficPlannerMain.cc(18085)
	adding costViewer which is a viewing application for the costmap
	thats being sent to dplanner.

	FILES: UTurn.cc(18081)
	fixes in uturn to make it compile. this is so that jeremy can
	insert the cost maps into tplanner

	FILES: testCState.cc(18078)
	minor fix in testCState

Fri Mar 16 11:15:48 2007	Noel duToit (ndutoit)

	* version R2-00d
	BUGS: 
	FILES: ApproachInterSafeToInterStop.cc(17984),
		InterStopToRoadRegion.cc(17984), LaneKeeping.cc(17984),
		Stop.cc(17984), Stopped.cc(17984),
		TrafficPlanner.cc(17984), TrafficPlannerControl.cc(17984),
		TrafficPlannerMain.cc(17984), ZoneToRoadRegion.cc(17984)
	added more robust traffic state switching based on hyper plane.
	Also adjusted vel profile in stop control state. Tested middle of
	segment checkpt and mission complete - seems ok for now.

	FILES: TrafficPlanner.cc(17946), TrafficPlannerMain.cc(17946)
	changed goal complete checking to not use distance, but rather
	checking if we have crossed some hyperplane, which is defined
	relative to the specified exit point

Thu Mar 15 21:15:56 2007	Sam Pfister (sam)

	* version R2-00c
	BUGS: 
	FILES: TrafficPlanner.cc(17877), TrafficPlanner.hh(17877),
		TrafficPlannerMain.cc(17877)
	turned on talker to listen to obstacle messages from map.  removed
	hard coded obstacle.

Thu Mar 15 14:18:40 2007	Noel duToit (ndutoit)

	* version R2-00b
	BUGS: 
	New files: UTurnToLaneKeeping.cc UTurnToLaneKeeping.hh
		singleroad.rndf testBoundaryInsert.cc testCState.cc
		testobs.mdf tplanner_unittest_left.mdf
	FILES: ControlState.cc(17512), ControlState.hh(17512),
		LaneKeepingToSlowDown.cc(17512),
		LaneKeepingToSlowDown.hh(17512), SlowDown.cc(17512),
		SlowDownToStop.cc(17512), SlowDownToStop.hh(17512),
		StopToStopped.cc(17512), StoppedToUTurn.cc(17512),
		TrafficPlannerControl.cc(17512),
		TrafficPlannerMain.cc(17512)
	fixed velocity profile specification for SlowDown. Also added more
	memory to the control states in the form of initial velocity (when
	you enter the control state).

	FILES: ControlState.cc(17589), ControlState.hh(17589),
		ControlStateFactory.cc(17589), Makefile.yam(17589),
		Stopped.cc(17589), Stopped.hh(17589),
		StoppedToLaneKeeping.cc(17589), StoppedToUTurn.cc(17589),
		StoppedToUTurn.hh(17589), TrafficPlanner.hh(17589),
		TrafficPlannerControl.cc(17589), TrafficUtils.cc(17589),
		UTurn.hh(17589)
	implemented control state machine to switch all the way into uturn.
	Need to update Stopped.cc, UTurn.cc. Added more memory into control
	state (initial time). Also, tried static casting with stopped
	control state - seems to work fine.

	FILES: Corridor.cc(17813), Corridor.hh(17813),
		LaneKeeping.cc(17813), Makefile.yam(17813),
		SlowDown.cc(17813), Stop.cc(17813), Stopped.cc(17813),
		TrafficPlanner.cc(17813), TrafficPlanner.hh(17813),
		TrafficPlannerMain.cc(17813), UTurn.cc(17813),
		UTurn.hh(17813), tplanner_unittest.rndf(17813)
	wrote function with which we can test in a presribed control state
	(testCState.cc). Made the corresponding changes in the make file.
	Updated Uturn to command a reverse and then a fwd traj. Also, got
	rid of OCPtSpecs dependencies.

	FILES: Makefile.yam(17524), SlowDown.cc(17524),
		TrafficPlanner.hh(17524), TrafficPlannerMain.cc(17524),
		TrafficUtils.cc(17524)
	implemented control state machine for dealing with obstacles. In
	the process of implementing the control states necessary now. Also
	fixed bug in TrafficUtils that was causing havoc with the insert
	function in point2.hh

Tue Mar 13 15:18:53 2007	Noel duToit (ndutoit)

	* version R2-00a
	BUGS: 
	Deleted files: state/AliceState.cc state/AliceState.hh
	FILES: Corridor.cc(17460), Corridor.hh(17460),
		LaneKeeping.cc(17460), Makefile.yam(17460),
		PlanningHorizon.cc(17460), SlowDown.cc(17460),
		Stop.cc(17460), Stopped.cc(17460),
		TrafficPlanner.cc(17460), TrafficPlanner.hh(17460),
		TrafficPlannerMain.cc(17460)
	implemented ocpParams population in tplanner, and interface with
	ocpspecs

Tue Mar 13  0:18:27 2007	Noel duToit (ndutoit)

	* version R2-00
	BUGS: 
	New files: RoadToRoadRegion.cc RoadToRoadRegion.hh
		StoppedToUTurn.cc StoppedToUTurn.hh UTurn.cc UTurn.hh
	Deleted files: RDDF.cc RDDF.hh SegGoals.hh
	FILES: AliceStateHelper.cc(17401), AliceStateHelper.hh(17401),
		ApproachInterSafeToInterStop.cc(17401),
		ControlStateTransition.hh(17401), Corridor.cc(17401),
		Corridor.hh(17401), InterStopToRoadRegion.cc(17401),
		LaneKeeping.cc(17401), LaneKeepingToSlowDown.cc(17401),
		PlanningHorizon.cc(17401), PlanningHorizon.hh(17401),
		RoadToApproachInterSafety.cc(17401),
		SlowDownToStop.cc(17401), StopToStopped.cc(17401),
		StoppedToLaneKeeping.cc(17401), TrafficPlanner.cc(17401),
		TrafficPlannerMain.cc(17401), ZoneToRoadRegion.cc(17401),
		tplanner_unittest.rndf(17401)
	fixed bug with seggoals. Updated state machine using
	getNextStopline() function to make more robust.

	FILES: ControlStateFactory.cc(16970),
		ControlStateFactory.hh(16970),
		RoadToApproachInterSafety.cc(16970),
		TrafficPlanner.cc(16970), TrafficPlanner.hh(16970),
		TrafficStateFactory.cc(16970)
	updated traffic and control state factories for changes due to
	u-turn. Also changed end of mission handling, but not clean enough
	yet. Added road-to-road transition (and implemented), and refined
	road-to-approach intersection.

	FILES: Makefile.yam(16899), TrafficStateFactory.cc(16899)
	added transitions and states for uturn

Fri Mar  9 12:23:06 2007	Sam Pfister (sam)

	* version R1-00i
	BUGS: 
	FILES: TrafficPlanner.hh(16851)
	Updated tplanner to include MapElementTalker.hh in the new
	location.  No change in functionality.

Fri Mar  9 10:34:49 2007	Noel duToit (ndutoit)

	* version R1-00h
	BUGS: 3060
	New files: ApproachInterSafeToInterStop.cc
		ApproachInterSafeToInterStop.hh ApproachInterSafety.cc
		ApproachInterSafety.hh ClearRegion.cc ClearRegion.hh
		CompositeTrafficState.cc CompositeTrafficState.hh
		Conflict.cc Conflict.hh ControlState.cc ControlState.hh
		ControlStateFactory.cc ControlStateFactory.hh
		ControlStateTransition.cc ControlStateTransition.hh
		Corridor.cc Corridor.hh InterStopToRoadRegion.cc
		InterStopToRoadRegion.hh IntersectionStop.cc
		IntersectionStop.hh LaneKeepToStop.cc LaneKeepToStop.hh
		LaneKeeping.cc LaneKeeping.hh LaneKeepingToSlowDown.cc
		LaneKeepingToSlowDown.hh PlanningHorizon.cc
		PlanningHorizon.hh RDDF.cc RDDF.hh RoadRegion.cc
		RoadRegion.hh RoadToApproachInterSafety.cc
		RoadToApproachInterSafety.hh SegGoals.hh SlowDown.cc
		SlowDown.hh SlowDownToStop.cc SlowDownToStop.hh Stop.cc
		Stop.hh StopToStopped.cc StopToStopped.hh Stopped.cc
		Stopped.hh StoppedToLaneKeeping.cc StoppedToLaneKeeping.hh
		TrafficPlannerControl.cc TrafficPlannerControl.hh
		TrafficPlannerTest.cc TrafficState.cc TrafficState.hh
		TrafficStateEstimator.cc TrafficStateEstimator.hh
		TrafficStateFactory.cc TrafficStateFactory.hh
		TrafficStateTransition.cc TrafficStateTransition.hh
		ZoneRegion.cc ZoneRegion.hh ZoneToRoadRegion.cc
		ZoneToRoadRegion.hh cmdline.c cmdline.h
		mapping/GeometricConstraints.cc
		mapping/GeometricConstraints.hh mapping/IntersectionLink.cc
		mapping/IntersectionLink.hh mapping/Lane.cc mapping/Lane.hh
		mapping/Line.cc mapping/Line.hh mapping/LinearInequality.cc
		mapping/LinearInequality.hh mapping/Location.cc
		mapping/Location.hh mapping/Obstacle.cc mapping/Obstacle.hh
		mapping/ObstacleBuilder.cc mapping/ObstacleBuilder.hh
		mapping/ObstacleFactory.hh mapping/Point.cc
		mapping/Point.hh mapping/RNDFParser.hh mapping/Road.cc
		mapping/Road.hh mapping/RoadBuilder.cc
		mapping/RoadBuilder.hh mapping/RoadFactory.hh
		mapping/RoadLink.cc mapping/RoadLink.hh mapping/Segment.cc
		mapping/Segment.hh mapping/test.cc rddf.rddf
		state/AliceState.cc state/AliceState.hh
		state/AliceStateHelper.cc state/AliceStateHelper.hh
		state/Edge.cc state/Edge.hh state/State.cc state/State.hh
		state/StateGraph.cc state/StateGraph.hh
		state/StateTransition.cc state/StateTransition.hh
		state/Vertex.cc state/Vertex.hh tplanner.ggo
		tplanner_unittest.rndf tplanner_unittest_right.mdf
		tplanner_unittest_straight.mdf
	FILES: AliceState.cc(16829), AliceState.hh(16829),
		AliceStateHelper.cc(16829), AliceStateHelper.hh(16829),
		Edge.cc(16829), Edge.hh(16829),
		GeometricConstraints.cc(16829),
		GeometricConstraints.hh(16829), IntersectionLink.cc(16829),
		IntersectionLink.hh(16829), Lane.cc(16829), Lane.hh(16829),
		Line.cc(16829), Line.hh(16829), LinearInequality.cc(16829),
		LinearInequality.hh(16829), Location.cc(16829),
		Location.hh(16829), Obstacle.cc(16829), Obstacle.hh(16829),
		ObstacleBuilder.cc(16829), ObstacleBuilder.hh(16829),
		ObstacleFactory.hh(16829), Point.cc(16829),
		Point.hh(16829), RNDFParser.hh(16829), Road.cc(16829),
		Road.hh(16829), RoadBuilder.cc(16829),
		RoadBuilder.hh(16829), RoadFactory.hh(16829),
		RoadLink.cc(16829), RoadLink.hh(16829), Segment.cc(16829),
		Segment.hh(16829), State.cc(16829), State.hh(16829),
		StateGraph.cc(16829), StateGraph.hh(16829),
		StateTransition.cc(16829), StateTransition.hh(16829),
		Vertex.cc(16829), Vertex.hh(16829), test.cc(16829)
	merging new tplanner into trunk

	FILES: ApproachInterSafeToInterStop.cc(16824),
		ApproachInterSafeToInterStop.hh(16824),
		ApproachInterSafety.cc(16824),
		ApproachInterSafety.hh(16824), ClearRegion.cc(16824),
		ClearRegion.hh(16824), CompositeTrafficState.cc(16824),
		CompositeTrafficState.hh(16824), Conflict.cc(16824),
		Conflict.hh(16824), ControlState.cc(16824),
		ControlState.hh(16824), ControlStateFactory.cc(16824),
		ControlStateFactory.hh(16824),
		ControlStateTransition.cc(16824),
		ControlStateTransition.hh(16824), Corridor.cc(16824),
		Corridor.hh(16824), InterStopToRoadRegion.cc(16824),
		InterStopToRoadRegion.hh(16824),
		IntersectionStop.cc(16824), IntersectionStop.hh(16824),
		LaneKeepToStop.cc(16824), LaneKeepToStop.hh(16824),
		LaneKeeping.cc(16824), LaneKeeping.hh(16824),
		LaneKeepingToSlowDown.cc(16824),
		LaneKeepingToSlowDown.hh(16824), Makefile.yam(16824),
		PlanningHorizon.cc(16824), PlanningHorizon.hh(16824),
		RDDF.cc(16824), RDDF.hh(16824), RoadRegion.cc(16824),
		RoadRegion.hh(16824), RoadToApproachInterSafety.cc(16824),
		RoadToApproachInterSafety.hh(16824), SegGoals.hh(16824),
		SlowDown.cc(16824), SlowDown.hh(16824),
		SlowDownToStop.cc(16824), SlowDownToStop.hh(16824),
		Stop.cc(16824), Stop.hh(16824), StopToStopped.cc(16824),
		StopToStopped.hh(16824), Stopped.cc(16824),
		Stopped.hh(16824), StoppedToLaneKeeping.cc(16824),
		StoppedToLaneKeeping.hh(16824), TrafficPlanner.cc(16824),
		TrafficPlanner.hh(16824), TrafficPlannerControl.cc(16824),
		TrafficPlannerControl.hh(16824),
		TrafficPlannerMain.cc(16824), TrafficPlannerTest.cc(16824),
		TrafficState.cc(16824), TrafficState.hh(16824),
		TrafficStateEstimator.cc(16824),
		TrafficStateEstimator.hh(16824),
		TrafficStateFactory.cc(16824),
		TrafficStateFactory.hh(16824),
		TrafficStateTransition.cc(16824),
		TrafficStateTransition.hh(16824), TrafficUtils.cc(16824),
		TrafficUtils.hh(16824), ZoneRegion.cc(16824),
		ZoneRegion.hh(16824), ZoneToRoadRegion.cc(16824),
		ZoneToRoadRegion.hh(16824), cmdline.c(16824),
		cmdline.h(16824), rddf.rddf(16824), tplanner.ggo(16824),
		tplanner_unittest.rndf(16824),
		tplanner_unittest_right.mdf(16824),
		tplanner_unittest_straight.mdf(16824)
	merging new tplanner into trunk - yam failed here last night

Wed Feb 21 13:13:50 2007	Nok Wongpiromsarn (nok)

	* version R1-00g
	BUGS: 
	FILES: TrafficPlanner.cc(15268)
	Fixed TrafficPlanner.cc so it compiles against revision R2-00b of
	the interfaces module

Tue Feb 20 17:24:39 2007	Nok Wongpiromsarn (nok)

	* version R1-00f
	BUGS: 
	FILES: Makefile.yam(15212), TrafficPlanner.cc(15212),
		TrafficPlanner.hh(15212)
	Fixed include statements so tplanner compiles against revision
	R2-00a of interfaces and skynet

Fri Feb  2  8:01:07 2007	Sam Pfister (sam)

	* version R1-00e
	BUGS: 
	FILES: Makefile.yam(14072)
	Updated Makefile.yam to fix broken link in bin directory.

	FILES: TrafficPlanner.cc(14071), TrafficPlanner.hh(14071)
	Fixed Mapper->Planner interface to receive stoplines and added
	method to select closest stopline.

Wed Jan 31 14:25:49 2007	Noel duToit (ndutoit)

	* version R1-00d
	BUGS: 
Wed Jan 31 11:59:58 2007	Noel duToit (ndutoit)

	* version R1-00c
	BUGS: 3109, 3108
	FILES: Makefile.yam(13708), TrafficPlanner.cc(13708),
		TrafficPlanner.hh(13708), TrafficUtils.cc(13708),
		TrafficUtils.hh(13708)
	Updated tplanner for changes due to change in StateClient
	interface. Removed tplanner dependency on cmap and cmapplus since
	the cost map is not yet implemented.

Mon Jan 29  8:10:51 2007	Nok Wongpiromsarn (nok)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(13460)
	Changed trafficUtils.{cc,hh} to TrafficUtils.{cc,hh} so they comply
	with the coding standards

Sun Jan 28 18:01:02 2007	Nok Wongpiromsarn (nok)

	* version R1-00a
	BUGS: 
	New files: TrafficPlanner.cc TrafficPlanner.hh
		TrafficPlannerMain.cc TrafficUtils.cc TrafficUtils.hh
	FILES: Makefile.yam(13380)
	Added basic files for tplanner modules and modified traffic planner
	so that mplanner, tplanner and gloNavMapLib can be started in any
	order

Sat Jan 27  3:56:30 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created tplanner module.





































