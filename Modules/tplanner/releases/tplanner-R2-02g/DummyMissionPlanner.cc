#include <iostream>
#include <deque>

#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"


using namespace std;             

typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MissTraffInterface ;

class CMissionPlanner : public GcModule
{
  
  ControlStatus m_controlStatus;
  MergedDirective m_mergedDirective;
  MissTraffInterface::Southface* mtInterfaceSF;

  
public:
  /*! Contstructor */
  CMissionPlanner(int skynetKey) 
    :GcModule( "MissionPlanner", &m_controlStatus, &m_mergedDirective, 100000, 100000 )
  
  {
    //this->setLogLevel(9);
    mtInterfaceSF = MissTraffInterface::generateSouthface(skynetKey, this);
  }

  void arbitrate(ControlStatus* cs, MergedDirective* md) {
    SegGoals tmpSegGoals;  
    
    deque<SegGoals> segGoals; 
    
    /* Goal 1 */
    tmpSegGoals.segment_type = SegGoals::PARKING_ZONE;
    tmpSegGoals.goalID = 1;
    tmpSegGoals.entrySegmentID = 0;
    tmpSegGoals.entryLaneID = 0;
    tmpSegGoals.entryWaypointID = 0;
    tmpSegGoals.exitSegmentID = 1;
    tmpSegGoals.exitLaneID = 1;
    tmpSegGoals.exitWaypointID = 2;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 5;
    tmpSegGoals.stopAtExit = false;

    segGoals.push_back(tmpSegGoals);

    /* Goal 2 */
    tmpSegGoals.segment_type = SegGoals::ROAD_SEGMENT;
    tmpSegGoals.goalID = 2;
    tmpSegGoals.entrySegmentID = 1;
    tmpSegGoals.entryLaneID = 1;
    tmpSegGoals.entryWaypointID = 2;
    tmpSegGoals.exitSegmentID = 1;
    tmpSegGoals.exitLaneID = 1;
    tmpSegGoals.exitWaypointID = 4;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 5;
    tmpSegGoals.stopAtExit = true;

    segGoals.push_back(tmpSegGoals);

    /* Goal 3 */
    tmpSegGoals.segment_type = SegGoals::INTERSECTION;
    tmpSegGoals.goalID = 3;
    tmpSegGoals.entrySegmentID = 1;
    tmpSegGoals.entryLaneID = 1;
    tmpSegGoals.entryWaypointID = 4;
    tmpSegGoals.exitSegmentID = 4;
    tmpSegGoals.exitLaneID = 2;
    tmpSegGoals.exitWaypointID = 1;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 5;
    tmpSegGoals.stopAtExit = false;

    segGoals.push_back(tmpSegGoals);

    /* Goal 4 */
    tmpSegGoals.segment_type = SegGoals::ROAD_SEGMENT;
    tmpSegGoals.goalID = 4;
    tmpSegGoals.entrySegmentID = 4;
    tmpSegGoals.entryLaneID = 2;
    tmpSegGoals.entryWaypointID = 1;
    tmpSegGoals.exitSegmentID = 4;
    tmpSegGoals.exitLaneID = 2;
    tmpSegGoals.exitWaypointID = 4;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 5;
    tmpSegGoals.stopAtExit = true ;

    segGoals.push_back(tmpSegGoals);

    /* Goal 5 */
    tmpSegGoals.segment_type = SegGoals::INTERSECTION;
    tmpSegGoals.goalID = 5;
    tmpSegGoals.entrySegmentID = 4;
    tmpSegGoals.entryLaneID = 2;
    tmpSegGoals.entryWaypointID = 4;
    tmpSegGoals.exitSegmentID = 3;
    tmpSegGoals.exitLaneID = 1;
    tmpSegGoals.exitWaypointID = 1;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 5;
    tmpSegGoals.stopAtExit = false;

    segGoals.push_back(tmpSegGoals);

    /* Goal 6 */
    tmpSegGoals.segment_type = SegGoals::ROAD_SEGMENT;
    tmpSegGoals.goalID = 6;
    tmpSegGoals.entrySegmentID = 3;
    tmpSegGoals.entryLaneID = 1;
    tmpSegGoals.entryWaypointID = 1;
    tmpSegGoals.exitSegmentID = 3;
    tmpSegGoals.exitLaneID = 1;
    tmpSegGoals.exitWaypointID = 2;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 5;
    tmpSegGoals.stopAtExit = true;

    segGoals.push_back(tmpSegGoals);


    /* Goal 7 */
    tmpSegGoals.segment_type = SegGoals::END_OF_MISSION;
    tmpSegGoals.goalID = 7;
    tmpSegGoals.entrySegmentID = 0;
    tmpSegGoals.entryLaneID = 0;
    tmpSegGoals.entryWaypointID = 0;
    tmpSegGoals.exitSegmentID = 0;
    tmpSegGoals.exitLaneID = 0;
    tmpSegGoals.exitWaypointID = 0;
    tmpSegGoals.minSpeedLimit = 0;
    tmpSegGoals.maxSpeedLimit = 0;
    tmpSegGoals.stopAtExit = true;

    segGoals.push_back(tmpSegGoals);

    SegGoalsStatus* segGoalsStatus;
  

    if (mtInterfaceSF->haveNewStatus()) {
      segGoalsStatus = mtInterfaceSF->getLatestStatus();
      if ((segGoals.front().goalID -1) != segGoalsStatus->goalID){
	cout<<"DUMMYMISSPLAN- SegGoalsStatus.goalID" << segGoalsStatus->goalID <<" does not match the next goal id ";
	cout<<segGoals.front().goalID -1<<endl;
	exit(1);
      } 
      if (SegGoalsStatus::COMPLETED != segGoalsStatus->status) {
	cout<<"DUMMYMISSPLAN- SegGoalsStatus.status, not COMPLETED" << segGoalsStatus->status<<endl;
	exit(1);
      }
      
      mtInterfaceSF->sendDirective(&segGoals.front());
      segGoals.pop_front();
      
    } 
  }
  
  void control(ControlStatus* cs, MergedDirective* md) {
  }
};

