#ifndef CREEP_HH_
#define CREEP_HH_

#include "ControlState.hh"
#include <math.h>
#include "TrafficUtils.hh"

class Creep:public ControlState {

  public:

    Creep(int stateId, ControlStateFactory::ControlStateType type);
    ~Creep();
    virtual int determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * map);

};

#endif                          /*LANEKEEPING_HH_ */
