#include <iostream>
#include <frames/point2.hh>
#include <bitmap/BitmapParams.hh>
#include <skynettalker/SkynetTalker.hh>
#include <ocpspecs/CostMapEstimator.hh>
#include <highgui.h>
#include "Corridor.hh"
#include <boost/serialization/vector.hpp>

using namespace std;

int main(int argc,char** argv)
{
  int skynetKey = skynet_findkey(argc, argv);
  cout << "Skynet key: " << skynetKey << endl;
  /* Receiver */
  CostMap cmap;
  CostMapEstimator cmapEst(skynetKey, true, false, 2);

  /* Sender */
  SkynetTalker< BitmapParams > polyTalker(skynetKey, SNbitmapParams, 
					  MODtrafficplanner);

  Map lmap;
  BitmapParams bmparams;
  PolygonParams polygonParams;

  //Parameters
  bmparams.centerX = 10;
  bmparams.centerY = 10;
  bmparams.width = 800;
  bmparams.height = 800;
  bmparams.resX = 0.1;
  bmparams.resY = 0.1;
  bmparams.baseVal = 100;
  polygonParams.centerlaneVal = 0;
  polygonParams.obsCost = 1000;

  VehicleState state;
  state.localX = 10;
  state.localY = 10;
  Corridor corr = Corridor();

  point2arr leftBound = point2arr();
  point2 pt=point2(0.0,0.0);
  leftBound.push_back(pt);
  pt=point2(3.0,0.0);
  leftBound.push_back(pt);
  pt=point2(6.0,2.0);
  leftBound.push_back(pt);
  pt=point2(9.0,2.0);
  leftBound.push_back(pt);
  pt=point2(14.0,6.0);
  leftBound.push_back(pt);
  pt=point2(9.0,10.0);
  leftBound.push_back(pt);
  pt=point2(5.0,10.0);
  leftBound.push_back(pt);
  corr.addPolyline(leftBound);

  point2arr rightBound = point2arr();
  pt=point2(0.0,2.0);
  rightBound.push_back(pt);
  pt=point2(3.0,2.0);
  rightBound.push_back(pt);
  pt=point2(6.0,4.0);
  rightBound.push_back(pt);
  pt=point2(9.0,4.0);
  rightBound.push_back(pt);
  pt=point2(12.0,6.0);
  rightBound.push_back(pt);
  pt=point2(9.0,8.0);
  rightBound.push_back(pt);
  pt=point2(5.0,8.0);
  rightBound.push_back(pt);
  corr.addPolyline(rightBound);

  point2arr obs;
  pt=point2(4,5);
  obs.push_back(pt);
  pt=point2(4,7);
  obs.push_back(pt);
  pt=point2(7,7);
  obs.push_back(pt);
  pt=point2(7,3);
  obs.push_back(pt);
  MapElement mapElement;
  mapElement.setTypeObstacle();
  mapElement.frameType = FRAME_LOCAL;
  mapElement.setId(1);
  mapElement.setGeometry(obs);
  mapElement.height = 0.2;
  lmap.addEl(mapElement);


  corr.getBitmapParams(bmparams, polygonParams, state, &lmap, point2(0,0));
  polyTalker.send(&bmparams);
  usleep(100000);
  bool cmapRecv = cmapEst.updateMap(&cmap);
  if (!cmapRecv) {
    cerr << "ERROR: No cmap received" << endl;
  }

  cout << "Press any key to quit" << endl;
  cvWaitKey(0);

}
