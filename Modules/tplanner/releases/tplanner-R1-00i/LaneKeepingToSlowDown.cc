#include "LaneKeepingToSlowDown.hh"
#include <math.h>
#include <list>


LaneKeepingToSlowDown::LaneKeepingToSlowDown(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
{

}

LaneKeepingToSlowDown::LaneKeepingToSlowDown()
{

}

LaneKeepingToSlowDown::~LaneKeepingToSlowDown()
{

}

double LaneKeepingToSlowDown::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon horiz,Map* map, VehicleState vehState)
{

  double vel = 0; 
  double delta = 0;
  double dstop = 0;
  int stopLineErr = -1;
  point2 stopLinePos, exitWayptPos, currPos;

  PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());

  cout << "in lanekeepingtoslowdown: exit pt label = " << label << endl; 
  currPos = AliceStateHelper::getPositionFrontBumper(vehState);
  //TODO FIX composite state
  //if (TrafficStateFactory::CLEARREGION == trafficState.getType()) {
	 
  if (TrafficStateFactory::APPROACH_INTER_SAFETY == trafficState->getType()) {
    cout << "here now" << endl;
    vel = AliceStateHelper::getVelocityMag(vehState);
    cout << "vel = " << vel << endl;
    delta = -pow(vel,2)/(2*m_desiredDecel);
    cout << "distance to transition at = " << delta << endl;
    // TODO: want to use stopline info here, not waypt info
    int wayptErr = map->getWaypoint(exitWayptPos, label);
    //stopLineErr = map->getStopline(stopLinePos,label);
    //dstop = stopLinePos.dist(currPos);
    dstop = exitWayptPos.dist(currPos);	
    cout << "dist to stop = " << dstop << endl;
    if (dstop <= delta) {
      m_probability = 1;
    } else { m_probability = 0; }

  } else if (TrafficStateFactory::ZONE_REGION == trafficState->getType()) {
    m_probability = 0;

  } else if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    m_probability = 0;
  } else if (TrafficStateFactory::INTERSECTION_STOP == trafficState->getType()) {
    m_probability = 0;
  }


  setUncertainty(m_probability);
  return m_probability;
} 


