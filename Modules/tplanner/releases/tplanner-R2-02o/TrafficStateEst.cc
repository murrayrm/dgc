#include "TrafficStateEst.hh"
#include "ZoneRegion.hh"
#include "interfaces/sn_types.h"

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile) 
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(waitForStateFill)
  , m_verbose(verbose)
  , m_debug(debug)
  , m_log(log)
  , m_useRNDF(useRNDF)
  , m_RNDFFile(RNDFFile)
{
  m_trafficGraph = TrafficStateFactory::createTrafficStates();  
  m_currTraffState = new ZoneRegion(0,TrafficStateFactory::ZONE_REGION);
  if (useRNDF){
    if (!RNDFFile.empty()){
      m_mapElemTalker.initRecvMapElement(skynetKey, MODtrafficplanner);
    } else {
      cout<<"TRFSTEST: RNDFfilename is empty"<<endl;
    }
  }

  m_localMap = new Map();
  m_localMapAtLastEst = new Map();
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_currTraffState; 
}


TrafficStateEst* TrafficStateEst::Instance(int skynetKey, bool waitForStateFill, bool debug, 
					   bool verbose, bool log, bool useRNDF, string RNDFfilename)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(skynetKey, waitForStateFill, debug, verbose, log, useRNDF, RNDFfilename);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

TrafficState* TrafficStateEst::determineTrafficState(PointLabel exitPtLabel) 
{
  //TODO given sensing info determineTrafficState...
  //Given current traffic state, query graph get  possible transitions list  
  //After a query to the map object, vehicle state, determine possible transitions 
  //for all possible transitions calculate the probability of meeting the transition conditions 
  //VehicleState vehState; 
  UpdateState();
  m_currVehState = m_state;
  m_vehStateAtLastEst = m_state;
  if ((m_verbose) || (m_debug)){  
    cout << "TrafficStateEst::determineTrafficState getting transitions"<< endl;
  }

  cout << "TrafficStateEst::determinTrafficState about to get localMap update"<< endl;

  /* First get an updated map, we do this once, since arbitrate and control are running in the same thread*/  
  getLocalMapUpdate();

  cout << "TrafficStateEst::determinTrafficState got localMap update"<< endl;

  /* Now we want to track the map we had during this estimation */
  m_localMapAtLastEst = m_localMap;

  std::vector<StateTransition*> transitions = m_trafficGraph.getOutStateTransitions(m_currTraffState);
 
  TrafficStateTransition* transition;

  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficStateEst::determineTrafficState transition size = " << transitions.size() << endl;
  } 
  for (i=0; i < transitions.size(); i++) 
    {
      transition = static_cast<TrafficStateTransition*> (transitions[i]);
      if ((m_verbose) || (m_debug)){  
        cout << "Calling MeetTransitionCondition " << i << endl;
      }
      transition->meetTransitionConditions(m_currTraffState, m_localMapAtLastEst, m_currVehState, exitPtLabel);
    }
  if ((m_verbose) || (m_debug)){  
    cout << "choosing trafficstate"<< endl;
  }

  m_currTraffState = chooseMostProbableTrafficState(m_currTraffState, transitions);
  return m_currTraffState;
}

TrafficState* TrafficStateEst::getCurrentTrafficState() 
{
  return m_currTraffState; 
}

VehicleState TrafficStateEst::getUpdatedVehState() 
{
  UpdateState();
  m_currVehState = m_state; 
  return m_currVehState;
}

VehicleState TrafficStateEst::getVehStateAtLastEst() 
{
  return m_vehStateAtLastEst;
}

TrafficState * TrafficStateEst::chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions) 
{
  TrafficState *currTrafficState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  TrafficStateTransition* trans; 
  
  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficStateEst::chooseMostProbable... " << transitions.size() << endl;
  }
  
  if (transitions.size() > 0) {
    for (i=0; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<TrafficStateTransition*> (probOneTrans.front());
      currTrafficState = trans->getTrafficStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        cout<<"There are no TrafficStateTransitions with probability 1"<<endl;
      }
      currTrafficState = trafState; 
    }
  } else {
    if ((m_verbose) || (m_debug)){   
      cout << "in TrafficStateEst::chooseMostProbable... " << currTrafficState->toString()<< endl;
    }
  }
  
  return currTrafficState;

}

Map* TrafficStateEst::getUpdatedMap() {

  getLocalMapUpdate();
  return m_localMap;

}

Map* TrafficStateEst::getMapAtLastEst() {
  return m_localMapAtLastEst;
}


void TrafficStateEst::getLocalMapUpdate()
{

  MapElement recvEl;
  int bytesRecv;

  bytesRecv = m_mapElemTalker.recvMapElementNoBlock(&recvEl,1);
 
  if (bytesRecv>0){
    m_localMap->addEl(recvEl);
  }else {
    cout << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
	 << bytesRecv << endl;
  }
}


bool TrafficStateEst::loadRNDF(string filename) {
  
  return m_localMap->loadRNDF(filename);

}
