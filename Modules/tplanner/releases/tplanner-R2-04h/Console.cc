/*
 *  Console.cc
 *  Console
 *
 *  Created by Sven Gowal on 24.05.07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#include "Console.hh"
#include "stdarg.h"
#include "stdio.h"

int Console::row = 0;
int Console::col = 0;

void Console::init()
{
    erase_screen();
}

void Console::destroy()
{
    printf("\n");
}

int Console::cprintf(int row, int col, char *format, ...)
{
    set_row(row);
    set_col(col);
    
    printf("\033[%d;%df", row, col);
    
    va_list valist;
    va_start(valist, format);
    int ret = vprintf(format, valist);
    va_end(valist);
    
    return ret;
}

int Console::cprintf(char *format, ...)
{
    printf("\033[%d;%df", row, col);

    va_list valist;
    va_start(valist, format);
    int ret = vprintf(format, valist);
    va_end(valist);
    
    return ret;
}

void Console::erase_screen()
{
    printf("\033[2J");
}

void Console::set_row(int r)
{
    row = r;
}

void Console::set_col(int c)
{
    col = c;
}
