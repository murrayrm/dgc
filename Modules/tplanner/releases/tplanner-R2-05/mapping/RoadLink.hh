#ifndef ROADLINK_HH_
#define ROADLINK_HH_


#include "Lane.hh"
#include <vector>

class RoadLink {

public:
	//TODO get rid of default constructor
	RoadLink();
   	RoadLink(int linkId);
	~RoadLink();
	std::vector<Lane> getMergingLanes();
  	  
private: 

	int m_id;
	std::vector<Lane>* m_roadLanes1; 
	std::vector<Lane>* m_roadLanes2; 
		
   
   };
   
#endif /*ROADLINK_HH_*/

