#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <sys/types.h>
#include <string.h>
#include "interfaces/sn_types.h"
#include "skynet/sn_msg.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"
#include "zpr.h"

#define GLERROR                                                    \
  {								   \
    GLenum code = glGetError();					   \
    while (code!=GL_NO_ERROR)					   \
      {								   \
	printf("%s\n",(char *) gluErrorString(code));		   \
	code = glGetError();					   \
      }								   \
  }

using namespace std;

const int  NUM_ROWS=250;
const int  NUM_COLS=250;
const double ROW_RES=0.40;
const double COL_RES=0.40;

const int WINDOW_WIDTH=1000;
const int WINDOW_HEIGHT=800;

//plotting features
const double OPACITY = 1;
//const double LINE_WIDTH = 1;
const bool OUTLINE = false;
const bool TOP_OUTLINE = false;

// Rainbow color scale
uint8_t rainbow[0x10000][3];


int KEYBOARD = 1;
int MOUSE = 2;

const int MODE = MOUSE;

#define MAX_DELTA_SIZE 1000000

using namespace std;

//OpenGL variables
float lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;

//Spread global variables
int flag = 0;
int mess_recvd = 0;
int tmapDeltaSocket;
int statesocket;


CMapPlus* tmap = new CMapPlus();
VehicleState currState;
int tmapID_cost;
unsigned long long timestamp;

skynet m_skynet = skynet(MODtrafficplanner,atoi(getenv("SKYNET_KEY")), NULL);

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

void init(void)
{	
  glEnable(GL_DEPTH_TEST);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);   

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
  {
    d = 4 * ((double) i / 0x10000);

    if (d >= 0 && d < 1.0)
    {
      rainbow[i][0] = 0x00;
      rainbow[i][1] = (int) (d * 0xFF);
      rainbow[i][2] = 0xFF;
    }
    else if (d < 2.0)
    {
      d -= 1.0;
      rainbow[i][0] = 0x00;
      rainbow[i][1] = 0xFF;
      rainbow[i][2] = (int) ((1 - d) * 0xFF);
    }
    else if (d < 3.0)
    {
      d -= 2.0;
      rainbow[i][0] = (int) (d * 0xFF);
      rainbow[i][1] = 0xFF;
      rainbow[i][2] = 0x00;
    }
    else if (d < 4.0)
    {
      d -= 3.0;
      rainbow[i][0] = 0xFF;
      rainbow[i][1] = (int) ((1 - d) * 0xFF);
      rainbow[i][2] = 0x00;
    }
    else
    {
      rainbow[i][0] = 0xFF;
      rainbow[i][1] = 0x00;
      rainbow[i][2] = 0x00;
    }
  }

}

void drawString (char *s)
{
  unsigned int i;
  for (i = 0; i < strlen (s); i++)
    glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
};


void draw_floor(void)
{
  //  static char label[100];
  glLineWidth (1);
  glColor3f (0.0F, 1.0F, 0.0F);

  glBegin(GL_LINES);

  glVertex3f(0,0,0);
  glVertex3f(0,0,-NUM_ROWS);

  glVertex3f(0,0,-NUM_ROWS);
  glVertex3f(NUM_COLS,0,-NUM_ROWS);

  glVertex3f(NUM_COLS,0,-NUM_ROWS);
  glVertex3f(NUM_COLS,0,0);

  glVertex3f(NUM_COLS,0,0);
  glVertex3f(0,0,0);

  glEnd();

    
  //====================
  //draw coordinate axes
  //====================
  glLineWidth (4);
  glColor3f (0.0F, 0.0F, 1.0F);
  glBegin(GL_LINES);
  glVertex3f(0,0,0);
  glVertex3f(1,0,0);
  glVertex3f(0,0,0);
  glVertex3f(0,0,-1);
  glVertex3f(0,0,0);
  glVertex3f(0,1,0);
  glEnd();


}

void draw_block(int i, int j, double cost)
{

  // Select color based on elevation relative to vehicle
  int k = (int) (0x10000 * (float)(cost)/ 101); // MAGIC
  if (k < 0x0000) k = 0x0000;
  if (k > 0xFFFF) k = 0xFFFF;
  glColor3ub(rainbow[k][0], rainbow[k][1], rainbow[k][2]);
  
  //===========
  //TOP SURFACE
  //===========
  //glColor4f(color_scale,color_scale,color_scale,1);
  glBegin(GL_QUADS);
  glVertex3f(i,cost,-j);
  glVertex3f(i,cost,-j-1);
  glVertex3f(i+1,cost,-j-1);
  glVertex3f(i+1,cost,-j);
  glEnd();         

  //===========
  //FRONT SURFACE
  //===========
  //glColor4f(color_scale,color_scale,color_scale,1);
  //glColor3f(1,1,1);
  glBegin(GL_QUADS);
  glVertex3f(i,0,-j);
  glVertex3f(i,cost,-j);
  glVertex3f(i+1,cost,-j);
  glVertex3f(i+1,0,-j);
  glEnd(); 


  //===========
  //LEFT SURFACE
  //===========
  //glColor4f(color_scale,color_scale,color_scale,1);
  //glColor3f(color_scale,color_scale,color_scale);
  glBegin(GL_QUADS);
  glVertex3f(i,0,-j);
  glVertex3f(i,cost,-j);
  glVertex3f(i,cost,-j-1);
  glVertex3f(i,0,-j-1);
  glEnd();  
  
  //===========
  //RIGHT SURFACE
  //===========
  //glColor4f(color_scale,color_scale,color_scale,1);
  //glColor3f(color_scale,color_scale,color_scale);
  glBegin(GL_QUADS);
  glVertex3f(i+1,0,-j);
  glVertex3f(i+1,cost,-j);
  glVertex3f(i+1,cost,-j-1);
  glVertex3f(i+1,0,-j-1);
  glEnd();    

  //===========
  //BACK SURFACE
  //===========
  //glColor4f(color_scale,color_scale,color_scale,1);
  //glColor3f(color_scale,color_scale,color_scale);
  glBegin(GL_QUADS);
  glVertex3f(i,0,-j-1);
  glVertex3f(i,cost,-j-1);
  glVertex3f(i+1,cost,-j-1);
  glVertex3f(i+1,0,-j-1);
  glEnd();        
}


void draw_everything(void)
{
  double cost_value=0;
  static char time_label[100];
  static char title_label[100];
  static char state_label[100];

  draw_floor();

  pthread_mutex_lock(&mutex1);
 
  for(int j=0; j<NUM_ROWS; j++)
    {
      for(int i=0; i<NUM_COLS; i++)
	{
	  
	  cost_value = (tmap->getDataWin<double>(tmapID_cost, i,j));

	  // See if there is any data in this cell before we draw it
	  if (cost_value == 0)
	    continue;     	 	  

	  draw_block(i, j, cost_value);
	  
	}
    }

  pthread_mutex_unlock(&mutex1);

  DGCgettime(timestamp);
  
  sprintf(title_label, "TPLANNER:COST MAP VIEWER"); 
  glColor3f(0.0F,1.0F,0.0F);
  glRasterPos2f ( 0, -40);
  drawString (title_label); 

  sprintf(time_label, "timestamp : %20.1f", (double)timestamp);
  glColor3f(0.0F,1.0F,0.0F);
  glRasterPos2f ( 0, -50);
  drawString (time_label); 
  
  sprintf(state_label, "state(N,E) :   (%3.4f, %3.4f)", currState.localX, currState.localY);
  glColor3f(0.0F,1.0F,0.0F);
  glRasterPos2f ( 0,-60);
  drawString (state_label); 
  
}

void display(void)
{
  
  GLERROR;
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glEnable(GL_DEPTH_TEST);
  draw_everything();
  glutSwapBuffers();
  GLERROR;

}

void resize(int w, int h)
{
  glViewport (0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
  glMatrixMode (GL_PROJECTION); //set the matrix to projection
  glLoadIdentity ();
  gluPerspective (60, (GLfloat)w / (GLfloat)h, 1.0, 1000.0); //set the perspective (angle of sight, width, height, , depth)
  glMatrixMode (GL_MODELVIEW); //set the matrix back to model  
}
 

void ReadMessage(void)
{           	    
  char* pMapDelta = new char[MAX_DELTA_SIZE];
      
  int statereceived;
  int deltasize;
  
  statereceived = m_skynet.get_msg(statesocket, &currState, sizeof(currState), 0);
  if(statereceived>0)
    {
      pthread_mutex_lock(&mutex1);
      tmap->updateVehicleLoc(currState.localX, currState.localY);
      deltasize = m_skynet.get_msg(tmapDeltaSocket, pMapDelta, MAX_DELTA_SIZE, 0);	 
      if(deltasize>0)
	{
	  tmap->applyDelta<double>(tmapID_cost, pMapDelta, deltasize);	  	  
	}
      else
	{
	  printf("delta not received!\n");
	}      
      pthread_mutex_unlock(&mutex1);
    }  
  delete pMapDelta;
} 

void arrowpad(int arrow, int x, int y)
{
  switch(arrow)
    {
    case GLUT_KEY_LEFT :
      glTranslatef(-0.1*10,0.0,0.0);
      break;
    case GLUT_KEY_RIGHT :
      glTranslatef(0.1*10,0.0,0.0);
      break;
    case GLUT_KEY_UP :
      glTranslatef(0.0,0.0,-0.1*10);
      break;
    case GLUT_KEY_DOWN :
      glTranslatef(0.0,0.0,0.1*10);
      break;
    }
  glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{


  switch(key)
    {
    case '1' :
      glTranslatef(0.0,0.5*10,0.0);
      break;
    case '3' :
      glTranslatef(0.0,-0.5*10,0.0);
      break;
    case '4' :
      glRotatef(0.5*10,0.0,1.0,0.0);
      break;
    case '6' :
      glRotatef(-0.5*10,0.0,1.0,0.0);
      break;
    case '8' :
      glRotatef(-0.5*10,1.0,0.0,0.0);
      break;
    case '2' :
      glRotatef(0.5*10,1.0,0.0,0.0);
      break;
    case '7' :
      glRotatef(0.5*10,0.0,0.0,1.0);
      break;
    case '9' :
      glRotatef(-.5*10,0.0,0.0,1.0);
      break;
    }

  glutPostRedisplay();
  
}


void idle (void) 
{

  //read latest message
  ReadMessage();

  //replot data
  glutPostRedisplay();


}

void pick(GLint name)
{
  fflush(stdout);
}

int main(int argc, char **argv)
{

  //========================
  //initialize skynet stuff
  //========================
  tmapDeltaSocket = m_skynet.listen(SNtplannerStaticCostMap, MODtrafficplanner);  
  statesocket = m_skynet.listen(SNstate, MODtrafficplanner);  
  if(tmapDeltaSocket < 0)
    cerr << "CTrafficPlanner::getMapDeltasThread(): skynet listen returned error" << endl; 

  //========================
  // initialize Map
  //========================
  tmap->initMap(0.0,0.0,NUM_ROWS,NUM_COLS,ROW_RES,COL_RES,0);
  tmapID_cost = tmap->addLayer<double>(0.0, -1.0, true);

  //========================
  //initialize openGL stuff
  //======================== 
  init();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowPosition(40, 40);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow("COSTVIEWER");
  
  glClearColor (0.0,0.0,0.0,0.0); //clear the screen to black
  
  /* set callback functions */
  glutDisplayFunc(display); 
  glutReshapeFunc(resize);
  
  if(MODE==KEYBOARD)
    {      
      gluLookAt(40,30,25, -17.5,-25,-50, 0,1,0);  
      glScalef(0.15,0.15,0.15);  
      glutKeyboardFunc(keyboard);
      glutSpecialFunc(arrowpad);
    }
  else if(MODE==MOUSE)
    {  
      gluLookAt(5,4,5.5, -2.25,-2.5,-5.0, 0,1,0);  
      glScalef(0.006,0.006,0.006);
      zprInit();
      zprSelectionFunc(draw_everything);     /* Selection mode draw function */
      zprPickFunc(pick);              /* Pick event client callback   */    
    }
  
  
  GLERROR;

  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_NORMALIZE);
  glEnable (GL_BLEND); 
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
  GLERROR;

  glutIdleFunc(idle);

  glutMainLoop();
  
  return 0;
}
