#include "ZoneToRoadRegion.hh"
#include <math.h>
#include "Log.hh"

ZoneToRoadRegion::ZoneToRoadRegion(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distZoneExit(1)
{

}

ZoneToRoadRegion::~ZoneToRoadRegion()
{

}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    Log::getStream(1) << "in ZoneToRoadRegion::meetTransitionConditions(), SHOULD NOT BE HERE " << endl;
    return 0.0;
}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel pointLabel)
{
    Log::getStream(1) << "in ZoneToRoadRegion::meetTransitionConditions() " << endl;

    LaneLabel lane(pointLabel.segment, pointLabel.lane);

    Log::getStream(1) << "ZoneToRoadRegion: Lane = " << lane << endl;
    Log::getStream(1) << "  Position = " << AliceStateHelper::getPositionRearAxle(vehState) << endl;

    double lane_heading; localMap->getHeading(lane_heading, pointLabel);
    double alice_heading = AliceStateHelper::getHeading(vehState);
    double angle_diff = TrafficUtils::getAngleInRange(lane_heading - alice_heading);

    /* We are on the road with the correct orientation */
    if (localMap->isPointInLane(AliceStateHelper::getPositionRearAxle(vehState), lane) && (fabs(angle_diff) < M_PI/12)) {
        m_trafTransProb = 1;
        AliceStateHelper::setDesiredLaneLabel(lane);
    } else
        m_trafTransProb = 0;
    setUncertainty(m_trafTransProb);
    
    return m_trafTransProb;
}
