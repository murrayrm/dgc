#include "TrafficUtils.hh"
#include <alice/AliceConstants.h>
#include <math.h>
#include "Log.hh"

#define EPS 0.2

double TrafficUtils::calculateDistance(Location loc1, Location loc2) 
{
	return sqrt(pow(loc1.getNorthing() - loc2.getNorthing(),2) +	
			  pow(loc1.getEasting() - loc2.getEasting(),2));

}


double TrafficUtils::calculateDistance(point2 pt1, point2 pt2) 
{
  return sqrt(pow(pt1.x -pt2.x, 2) + pow(pt1.y -pt2.y, 2)); 

}

int TrafficUtils::getClosestPtIndex(point2arr boundary, point2 point)
{
  // to specified point x,y
	int index;
  int lengthBound = boundary.size();
  double distance = 1000000.0;
  
  // closest point on Boundary
  for (int ii = 0; ii<lengthBound; ii++)
  {
    double distance_temp = point.dist(boundary[ii]);
    if (distance_temp<distance)
    {
      distance=distance_temp;
      index=ii;
    }
  }
  return index;
}

int TrafficUtils::insertPtAtDistance(point2arr& boundary, int index, double dist)
{
  double curDist = 0;
  if (dist == 0) {
    //return boundary.insert((unsigned int)index, boundary[index]);
    dist = 0.01;
  }
  if (index >= (int)boundary.size() || index < 0) {
    cerr << "insertPtAtDistance:Index out of range" << endl;
    return -1;
  }

  //  cerr << "in insertPtAtDistance: " << index << ", " << dist << endl;
  
  if (dist > 0) {
    //for (size_t i=index; i<boundary.size()-1; i++)
    for (unsigned int i=index; i<boundary.size()-1; i++)
    {
      //      i = (unsigned int)i;
      curDist += boundary[i].dist(boundary[i+1]);
      if (curDist > dist) //point lies between i, i+1
      {
        double d = boundary[i].dist(boundary[i+1]);
        double fac = (curDist - dist) / d;
        if (fac > 1) fac = 1;
        point2 pt = (fac*boundary[i] + (1-fac)*boundary[i+1]);
        return boundary.insert((i+1),pt);
      }
    }
    //point lies past end
    point2 delta = boundary.back() - boundary[boundary.size()-2];
    double dLeft = dist - curDist;
    delta.normalize();
    delta = delta*dLeft;
    point2 pt = boundary.back() + delta;
    boundary.push_back(pt);
    return boundary.size() - 1;
  }
  else if (dist < 0) {
    for (unsigned int i=index; i>0; i--)
    {
      curDist += boundary[i].dist(boundary[i-1]);
      if (curDist > dist) //point lies between i, i-1
      {
        double d = boundary[i].dist(boundary[i-1]);
        double fac = (curDist - dist) / d;
        if (fac > 1) fac = 1;
        point2 pt = (fac*boundary[i] + (1-fac)*boundary[i-1]);
        return boundary.insert((unsigned int)i,pt);
      }
    }
    point2 delta = boundary[0] - boundary[1];
    double dLeft = dist - curDist;
    delta.normalize();
    delta = delta*dLeft;
    point2 pt = boundary[0] + delta;
    return boundary.insert(0,pt);
  }
  return -1;
}

// TODO Should check for reverse direction
double TrafficUtils::getNearestObsInLane(MapElement & me, Map * map, VehicleState vehState, LaneLabel lane)
{
  vector<MapElement> obstacles;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  int obsErr = map->getObsInLane(obstacles, lane);
  if (obsErr < 1) //no obstacles
    return -1;

  int obs_index = -1;
  double min_dist = INFINITY;
  double dist;
  point2 obs_pt;
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  for (unsigned int i=0; i<obstacles.size(); i++) {
    for (unsigned int j=0; j<obstacles[i].geometry.size(); j++) {
        obs_pt.set(obstacles[i].geometry[j]);
        map->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
        if (dist > 0.0 && dist < min_dist) {
          min_dist = dist;
          obs_index = i;
        }
    }
  }

  if (min_dist == INFINITY)
    return -1;

  //Log::getStream(1) << "Found obstacle at distance " << min_dist << endl;
  me = obstacles[obs_index];

  return min_dist; 
}


double TrafficUtils::getNearestObsInOppositeLane(MapElement & me, Map * localMap, VehicleState vehState, LaneLabel lane)
{
  vector<MapElement> obstacles;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  LaneLabel oppositeLane;
  localMap->getNeighborLane(oppositeLane,lane,-1);

  int obsErr = localMap->getObsInLane(obstacles, oppositeLane);
  if (obsErr < 1) //no obstacles
    return -1;

  int obs_index = -1;
  double min_dist = INFINITY;
  double dist;
  point2 obs_pt;
  point2arr centerline;
  localMap->getLaneCenterLine(centerline, lane);

  for (unsigned int i=0; i<obstacles.size(); i++) {
    for (unsigned int j=0; j<obstacles[i].geometry.size(); j++) {
        obs_pt.set(obstacles[i].geometry[j]);
        localMap->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
        if (dist > 0.0 && dist < min_dist) {
          min_dist = dist;
          obs_index = i;
        }
    }
  }

  if (min_dist == INFINITY)
    return -1;

  //Log::getStream(1) << "Found obstacle at distance " << min_dist << endl;
  me = obstacles[obs_index];

  return min_dist; 
}

double TrafficUtils::getNearestObsDist(Map * map, VehicleState vehState, LaneLabel lane)
{
  MapElement obstacle;
  return getNearestObsInLane(obstacle, map, vehState, lane);
}

double TrafficUtils::getNearestObsPoint(point2 &pt, Map *map, VehicleState vehState, LaneLabel lane, double offset)
{
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  MapElement obstacle;
  double dist_obs = getNearestObsInLane(obstacle, map, vehState, lane);

  double dist;
  double min_dist = INFINITY;
  point2 obs_pt;
  for (unsigned int j=0; j<obstacle.geometry.size(); j++) {
    obs_pt.set(obstacle.geometry[j]);
    map->getDistAlongLine(dist, centerline, obs_pt);
    if (dist > 0.0 && dist < min_dist) {
      min_dist = dist;
    }
  }

  if (offset > min_dist) return -1;

  map->getPointAlongLine(pt, centerline, min_dist-offset);
  return dist_obs;
}

double TrafficUtils::getNearestObsFarthermostPoint(point2 &pt, Map * map, VehicleState vehState, LaneLabel lane, double offset)
{
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  MapElement obstacle;
  double dist_obs = getNearestObsInLane(obstacle, map, vehState, lane);

  double dist;
  double max_dist = -1;
  double min_dist = INFINITY;
  point2 obs_pt;
  for (unsigned int j=0; j<obstacle.geometry.size(); j++) {
    obs_pt.set(obstacle.geometry[j]);
    map->getDistAlongLine(dist, centerline, obs_pt);
    if (dist > 0.0 && dist > max_dist) {
      max_dist = dist;
    }
    if (dist > 0.0 && dist < min_dist) {
      min_dist = dist;
    }
  }

  map->getPointAlongLine(pt, centerline, max_dist+offset);
  return dist_obs + (max_dist-min_dist);
}

#define EXTRA_WIDTH 1.5

bool TrafficUtils::isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane)
{
  point2arr lb, rb;
  map->getLaneBounds(lb, rb, lane);

  double ldist = me.dist(lb, GEOMETRY_LINE);
  double rdist = me.dist(rb, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

bool TrafficUtils::isLaneBlocked(Map *map, VehicleState vehState, point2 finalPt, LaneLabel lane)
{
  /* Test all obstacles in front of the car and closer than finalPt */

  vector<MapElement> obstacles;

  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2arr lb, rb;
  map->getLaneBounds(lb, rb, lane);

  int obsErr = map->getObsInLane(obstacles, lane);
  if (obsErr < 1) //no obstacles
    return -1;

  double dist;
  double ldist, rdist;
  point2 obs_pt;
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  double dist2finalPt;
  map->getDistAlongLine(dist2finalPt, centerline, finalPt, currFrontPos);
  if (dist2finalPt>40)
    dist2finalPt = 40;

  Log::getStream(1)<<"Obstacle blockage check for obstacle within "<<dist2finalPt<<" meters."<<endl;
  Log::getStream(1) << "dist2finalPt = " << dist2finalPt << endl;
  for (unsigned int i=0; i<obstacles.size(); i++) {
    obs_pt.set(obstacles[i].position);
    map->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
    Log::getStream(1) << "dist to obstacle point = " << dist << endl;
    if (dist > 0.0 && dist < dist2finalPt) {
      /* Test for blockage */
      ldist = obstacles[i].dist(lb, GEOMETRY_LINE);
      rdist = obstacles[i].dist(rb, GEOMETRY_LINE);

      Log::getStream(1) << "Left distance to bound = " << ldist << endl;
      Log::getStream(1) << "Right distance to bound = " << rdist << endl;

      if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
        Log::getStream(1) << "Lane is blocked" << endl;
        return true;
      }
    }
  }

  Log::getStream(1)<<"Lane is not blocked"<<endl;
  return false;
}

MapElementType TrafficUtils::getObsType(MapElement & me)
{
    return me.type;
}

bool TrafficUtils::isObsStatic(MapElement & me)
{
    return (me.type == ELEMENT_OBSTACLE || me.type == ELEMENT_OBSTACLE_EDGE);
}

int TrafficUtils::alignBoundaries(point2arr& bound1, point2arr& bound2)
{
  //first, figure out offset using mean distance
  double md_m1=0, md_0=0, md_1=0;
  size_t s1 = bound1.size();
  size_t s2 = bound2.size();

  if ((s1 < 2) || (s2 < 2)) {
    Log::getStream(1) << "In alignBoundaries: degenerate boundaries given" << endl;
    return -1;
  }

  //aligned distance
  for (unsigned int i=0; i<min(s1,s2); i++)
    md_0 += bound1[i].dist(bound2[i]);
  md_0 /= min(s1,s2);

  //positive offset
  for (unsigned int i=0; i<min(s1,s2-1); i++)
    md_1 += bound1[i].dist(bound2[i+1]);
  md_1 /= min(s1,s2-1);

  //negative offset
  for (unsigned int i=0; i<min(s1-1,s2); i++)
    md_m1 += bound1[i+1].dist(bound2[i]);
  md_m1 /= min(s1-1,s2);

  //positive offset is smallest
  if ((md_1 < md_0) && (md_1 < md_m1))
    bound2.remove(0);
  //negative offset is smallest
  else if ((md_m1 < md_0) && (md_m1 < md_1))
    bound1.remove(0);

  //sizes might now be different, so remove as needed
  while (bound1.size() > bound2.size())
    bound1.pop_back();
  while (bound2.size() > bound1.size())
    bound2.pop_back();

  return 0;
}

int TrafficUtils::insertProjPtInBoundary(point2arr& boundary, point2 point)
{
  int index, insertIndex;
  point2arr dataToFit;
  int lengthBound = (int)boundary.size();
  // find the index of the closest pt on the boundary
  index = getClosestPtIndex(boundary, point);
  // Log::getStream(1) << "index of closest point is: " << index << endl;

  if (index > 0 && index+1 < lengthBound){
      dataToFit.push_back(boundary[index-1]);
      dataToFit.push_back(boundary[index]);
      dataToFit.push_back(boundary[index+1]);
  } else if (index == 0 && index+1 < lengthBound) {
      dataToFit.push_back(boundary[index]);
      dataToFit.push_back(boundary[index+1]);
  } else if (index > 0 && index+1 == lengthBound) {
      dataToFit.push_back(boundary[index-1]);
      dataToFit.push_back(boundary[index]);
  } else {// case where only one point is given
    Log::getStream(1) << "TrafficUtils::insertPtOnBoundary() only one pt on boundary - cannot fit a line to insert the closest point" << endl;
    return -1;
  }

  // then find best approximation to the point on the line
  // transform into coordinate frame at begin pt of line x-axis along line
  int ptsToFit = (int)dataToFit.size();
  double theta = atan2(dataToFit[ptsToFit-1].y - dataToFit[0].y, dataToFit[ptsToFit-1].x - dataToFit[0].x); 
  double R11 = cos(theta);
  double R12 = -sin(theta);
  double R21 = sin(theta);
  double R22 = cos(theta);
  point2 d_vec = boundary[index];
  point2 posXYPt_line, posBDPt_line, posBDPt_local;
  posXYPt_line.x = R11*(point.x-d_vec.x)+R21*(point.y-d_vec.y);
  posXYPt_line.y = 0;  // this does not matter - is not used
  // find projection of point onto x-axis, which is aligned with line
  posBDPt_line.x = posXYPt_line.x;
  //Log::getStream(1) << "projected pt x = " << posBDPt_line.x << endl;
  posBDPt_line.y = 0;
  // transform new point back to local frame
  posBDPt_local.x = d_vec.x + R11*posBDPt_line.x + R12*posBDPt_line.y;
  posBDPt_local.y = d_vec.y + R21*posBDPt_line.x + R22*posBDPt_line.y;

  //  Log::getStream(1) << "posDPt_line.x = " << posBDPt_line.x << endl;
  if (posBDPt_line.x > EPS) {
    insertIndex = index+1;
    //  Log::getStream(1) << "insert index = " << insertIndex << endl;
    boundary.insert((unsigned int)insertIndex, posBDPt_local);
  } else if (posBDPt_line.x<-EPS) {
    insertIndex = index;
    //  Log::getStream(1) << "insert index = " << insertIndex << endl;
    boundary.insert((unsigned int)insertIndex, posBDPt_local);
  }
  else {
    insertPtAtDistance(boundary,insertIndex,0.01);
  }
  return 0;
}

// Adds two angles and adjust the sum according to the PI/-PI convention
int TrafficUtils::addAngles(double &angle_out, double angle_in1, double angle_in2) {
  if ((angle_in1+angle_in2)>M_PI) 
    angle_out=angle_in1+angle_in2-2*M_PI;
  else if ((angle_in1+angle_in2)<=-M_PI)
    angle_out=angle_in1+angle_in2+2*M_PI;
  else
    angle_out=angle_in1+angle_in2;
  
  return 0;
}

double TrafficUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

double TrafficUtils::getObstacleVelocityMag(MapElement el)
{
  return sqrt(el.velocity.x*el.velocity.x + el.velocity.y*el.velocity.y);  
}

double TrafficUtils::getObstacleVelocityMagUncert(MapElement el)
{
  return 0;
}

MapElement* TrafficUtils::getMapElement(Map* locMap, MapId id) {

  for (unsigned int i=0; i< locMap->data.size(); i++) {
    if (id == locMap->data[i].id) {
      return &locMap->data[i];
    }
  }

  return NULL;
}

int TrafficUtils::getLaneSide(Map* localMap, VehicleState vehState, LaneLabel currLane, LaneLabel desiredLane) {

  bool isReverse = false;

  if (currLane == desiredLane) {
    return 0; 
  }

  double currlane_angle;
  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearbumper = AliceStateHelper::getPositionRearBumper(vehState);
  localMap->getHeading(currlane_angle, alice_rearbumper);
  double diff_angle = fabs(getAngleInRange(alice_angle-currlane_angle));
  isReverse = (diff_angle > M_PI/2);

  LaneLabel lane;
  localMap->getNeighborLane(lane,currLane,-1);

  if (lane == desiredLane) {
    if (isReverse)
      return 1;
    return -1;
  } 

  localMap->getNeighborLane(lane,currLane,1);

  if (lane == desiredLane) {
    if (isReverse)
      return -1;
    return 1;
  } 

  return 0;

}


