#ifndef TRAFFICMANAGER_HH_
#define TRAFFICMANAGER_HH_

#include <fstream>
#include "Corridor.hh"
#include "Conflict.hh"
#include "mapping/Segment.hh"
#include "TrafficStateEstimator.hh"
#include "ControlState.hh"
#include "ControlStateFactory.hh"
#include "TrafficState.hh"
#include "ControlStateTransition.hh"
#include "state/StateGraph.hh"
#include "interfaces/VehicleState.h"
#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "travgraph/Graph.hh"
#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "Interfaces.hh"


extern int DEBUG_LEVEL;

/*! Input interface from which SegGoals are received from the Route Planner */

typedef GcInterface<SegGoals, SegGoalsStatus, SNsegGoals, SNtplannerStatus, MODmissionplanner> MissionTrafficInterface;

/*! Output interface that sends corridor create directives to the Corridor Control */
typedef GcInterface<CorridorCreate, CorridorCreateStatus, SNcorridorCreate, SNcorridorStatus, MODtrafficplanner> TraffCorridorInterface;


/*! The structure for merged directive that is "sent" from arbiter to control. 
 * Since the arbiter just passes through the directive MergedDirective has
 * the same structure as MContrDirective. */
//typedef RoutePlannerDirective TrafficManagerMergedDirective;  


/*! The structure for status of merged directive. This status is "sent"
 *  from control back to arbiter. The structure for this is the same
 *  as MContrDirectiveStatus. */
//typedef RoutePlannerDirectiveResponse TrafficManagerControlStatus;

class TrafficManagerControlStatus : public ControlStatus
{
public:
  /* Generally, status can be COMPLETED, FAILED, 
     or PENDING . You probably don't need the whole list or
     you may need other status such as when the control is still
     working on a given directive. 
     TODO: Redefine this. */
  enum TrafficManagerStatus{ COMPLETED, FAILED, READY_FOR_NEXT };
  
  unsigned id; // The id of the merged directive that this control status corresponds to.
  TrafficManagerStatus status;
  TrafficManagerResponse::ReasonForFailure reason;
  RoutePlannerDirectiveResponse rpcs; 
};

class TrafficManagerMergedDirective : public MergedDirective
{
public:
  RoutePlannerDirective directive;
  unsigned id;
  //some idea of type 
  double number_arg;
};

/*! TODO = what is this exactly? The structure for response from corridor? */
struct TrafficManagerContrMsgResponse
{
  enum Status{ QUEUED, SENT, ACCEPTED, COMPLETED, FAILED };
  TrafficManagerContrMsgResponse()
    :status(QUEUED)
  {
  }

  Status status;
  CorridorCreateStatus::ReasonForFailure reason;
};

/*! The structure for pairing up the directives sent to the
 *  corridor control (but in the language of the control) and the response
 *  to this directive. */
struct TrafficManagerContrMsgWrapper
{
  TrafficManagerContrMsgWrapper()
  {
    response.status = TrafficManagerContrMsgResponse::QUEUED;
  }
  CorridorCreate directive;  // corridorID is in here
  TrafficManagerContrMsgResponse response;
};


class TrafficManager : public GcModule {

public: 
  
  /*! Constructor */
  TrafficManager(int skynetKey, bool debug, bool verbose, bool log, FILE* logFile);

  /*! Destructor */
  virtual ~TrafficManager();

  /*! This is the function that continually runs the planner in a loop */
  void TrafficPlanningLoop(void);

  //TODO: move all methods below to PRIVATE  
  /*! Given the traffic state passed, give the most likely control state to maintain. */
  ControlState* determineControlState(ControlState *currControlState, TrafficState *currTrafficState,PlanningHorizon currPlanHorizon, Map* localMap, VehicleState currVehState);
    
  /*! 
   * Determine the planning horizon, given the current traffic state. 
   */
  void determinePlanningHorizon(PlanningHorizon &currPlanHorizon, TrafficState *currTrafficState, list<SegGoals> currSegGoals) ;
  
    
  /*! Choose the most probable control action. */
  ControlState* chooseMostProbableControlState(ControlState* controlState,std::vector<StateTransition*> transitions);   
  
private :
  
  /*! Arbitration for the traffic planner control module. It computes the next
    merged directive based on the directives from mission control
    and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the traffic planner control module. It computes and sends
    directives to all its controlled modules based on the 
    merged directive and outputs the control status
    based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);
  
  /*!\param m_snkey is a skynet key that is set in MissionPlannerMain and
   * never changed. */
  int m_snKey;
  
  /*!\param control status sent from control to arbiter */
  CorridorCreateStatus m_corrCreateStatus;

  /*!\param merged directive sent from arbiter to control */
  TrafficManagerMergedDirective m_mergedDirective; 
  
  /*!\param corridorID of control directive that corresponds to the completion of
   * m_mergedDirective */
  int m_mergedDirectiveCorridorID;

  /*!\param goalID of control directive that corresponds to the completion of
   * m_prevMergedDirective */
  int m_prevMergedDirectiveCorridorID;

 /*!\param directives currently stored in control waiting to be added to contrGcPort */
  deque<SegGoals> m_contrDirectiveQ;

  /*!\param the mutex to protect m_contrDirectiveQ */
  pthread_mutex_t m_contrDirectiveQMutex;

  /*!\param control status sent from control to arbiter */
  TrafficManagerControlStatus m_controlStatus;


  /*!\param GcInterface variable */
  MissionTrafficInterface* m_missTraffInterface;
  MissionTrafficInterface::Northface* m_missTraffInterfaceNF;

  TraffCorridorInterface* m_traffCorrInterface;
  TraffCorridorInterface::Southface* m_traffCorrInterfaceSF;

  bool m_verbose;
  bool m_debug;
  
  /*!\param m_logData is true when data is to be logged */
  bool m_logData;

  /*!\param m_logFile is the name of the log file */
  FILE* m_logFile;

  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*!\Control State */
  StateGraph m_controlGraph;
  
  TrafficStateEstimator* m_estimator;
  
};

#endif /*TRAFFICMANAGER_HH_*/
