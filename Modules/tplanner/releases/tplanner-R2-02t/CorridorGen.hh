#ifndef CORRIDORGEN_HH_
#define CORRIDORGEN_HH_

#include "TrafficUtils.hh"
#include "frames/point2.hh"
#include <vector>
//#include "alice/AliceConstants.h"
#include "interfaces/VehicleState.h"
#include "map/Map.hh"
#include "Corridor.hh"
//#include "Conflict.hh"
#include "mapping/Segment.hh"
//#include "TrafficStateEstimator.hh"
//#include "ControlState.hh"
//#include "ControlStateFactory.hh"
#include "TrafficState.hh"
//#include "ControlStateTransition.hh"
//#include "state/StateGraph.hh"
//#include "travgraph/Graph.hh"
//#include "gcinterfaces/SegGoals.hh"
//#include "gcinterfaces/SegGoalsStatus.hh"
#include "PlanningHorizon.hh"


class CorridorGen {
public:
  static int makeCorridorLane(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, double range);
  static int makeCorridorTurn(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap);
  static int makeCorridorZone(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap);
  static int makeCorridorLaneChange(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap, LaneLabel destLane, point2 startPos);
  CorridorGen() {}
  ~CorridorGen() {}
  };
#endif /*CORRIDORGEN_HH_*/
