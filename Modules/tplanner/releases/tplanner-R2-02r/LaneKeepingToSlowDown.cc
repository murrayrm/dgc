#include "LaneKeepingToSlowDown.hh"
#include <math.h>
#include <list>


LaneKeepingToSlowDown::LaneKeepingToSlowDown(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
, m_desiredDecel(-0.5)
, m_distToObs(10)
{

}

LaneKeepingToSlowDown::LaneKeepingToSlowDown()
{

}

LaneKeepingToSlowDown::~LaneKeepingToSlowDown()
{

}

double LaneKeepingToSlowDown::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon horiz, Map * localMap, VehicleState vehState)
{

    switch (trafficState->getType()) {
    
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        
        if ((m_verbose) || (m_debug)) {
            cout << "in LaneKeepingToSlowDown: APPROACH_INTER_SAFETY" << endl;
        }
        
        double currVel = 0;
        double delta = 0;
        double dstop = 0;
        int stopLineErr = -1;
        point2 stopLinePos, currFrontPos;
        PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
        currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        currVel = AliceStateHelper::getVelocityMag(vehState);
        delta = -pow(currVel, 2) / (2 * m_desiredDecel);
        // Use getStopline() call based on our current pos here
        stopLineErr = localMap->getNextStopline(stopLinePos, label);
        // Or should we use: stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
        dstop = stopLinePos.dist(currFrontPos);
        cout << "distance to stop = " << dstop << endl;
        if (dstop <= delta) {
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;

    case TrafficStateFactory::ZONE_REGION:
    
        m_probability = 0;
        
        break;

    case TrafficStateFactory::ROAD_REGION:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "in LaneKeepingToSlowDown: ROAD_REGION" << endl;
        }
        
        double currVel = AliceStateHelper::getVelocityMag(vehState);
        double delta = -pow(currVel, 2) / (2 * m_desiredDecel) + m_distToObs;
        if ((m_verbose) || (m_debug)) {
            cout << "LaneKeepingToSlowDown delta = " << delta << endl;
        }
        point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
        double distToObs = localMap->getObstacleDist(currFrontPos, 0);
        if (distToObs <= delta) {
            m_probability = 1;
        } else {
            m_probability = 0;
        }
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
   
        m_probability = 0;
        
        break;
    
    default:
    
        m_probability = 0;
        cerr << "LaneKeepingToSlowDown.cc: Undefined Traffic state" << endl;
    
    }

    setUncertainty(m_probability);
    
    return m_probability;
}
