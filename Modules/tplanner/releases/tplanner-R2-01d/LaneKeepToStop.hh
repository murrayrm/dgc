#ifndef LANEKEEPTOSTOP_HH_
#define LANEKEEPTOSTOP_HH_

#include "ControlStateTransition.hh"


class LaneKeepToStop : public ControlStateTransition {


public: 



LaneKeepToStop(ControlState* state1, ControlState* state2);

LaneKeepToStop();

~LaneKeepToStop();

double meetTransitionConditions(ControlState controlState, TrafficState trafficState, PlanningHorizon planHorizon, Map* map,VehicleState vehState);

private: 

double m_probability; 

};
#endif /*LANEKEEPTOSTOP_HH_*/
