#ifndef STOPPEDTOUTURN_HH_
#define STOPPEDTOUTURN_HH_


#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class StoppedToUTurn : public ControlStateTransition {


public: 

  StoppedToUTurn(ControlState* state1, ControlState* state2);

  StoppedToUTurn();

  ~StoppedToUTurn();

  double meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon, Map* map, VehicleState vehState);



private: 

  double m_boundStoppedVel; 

};
#endif /*STOPPEDTOUTURN_HH_*/
