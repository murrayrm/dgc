#include "PlanningHorizon.hh"

PlanningHorizon::PlanningHorizon(SegGoals segGoals)
{

}

PlanningHorizon::PlanningHorizon() 
{
}

PlanningHorizon::~PlanningHorizon() 
{
  //delete m_map;
}


std::vector<Road> PlanningHorizon::getRoadObjects()
{
	
	//TODO fix transformtation Map Element to Road object 
	//int laneId = 0;
	//int segmentId = 0;
	// m_map->getRoadObjects(segmentId, laneId);
	return m_roads;

}

std::vector<Obstacle> PlanningHorizon::getObstacles() 
{
	//TODO fix Map Element to Obstacle 
	//	int laneId = 0;
	//	int segmentId = 0;
	//m_obstacles = m_map->getObstacles(segmentId, laneId);
	return m_obstacles; 
}


Line* PlanningHorizon::getStopLine(int segmentId, int laneId) 
{
	//TODO fix stop line tranformation 
	//m_map->getStopLine(segmentId, laneId); TODO FIX map object 
	return new Line(); 
	
}

int PlanningHorizon::getExitLaneId(SegGoals segGoal) 
{
	return segGoal.exitLaneID;

}


SegGoals PlanningHorizon::getSegGoal(int i) 
{
  list<SegGoals> tmpSegGoals;
  tmpSegGoals.assign(m_segGoals.begin(), m_segGoals.end()); 
  for(int j=0;j<i; j++){
    tmpSegGoals.pop_front();
  }
  m_segGoals.front().print();
  return tmpSegGoals.front();
}

void PlanningHorizon::addGoal(SegGoals segGoal)
{
  m_segGoals.push_back(segGoal);
}


int PlanningHorizon::getCurrentExitSegment()
{
  return m_segGoals.front().exitSegmentID;

}

int PlanningHorizon::getCurrentExitLane()
{
  return m_segGoals.front().exitLaneID;
}
  
int PlanningHorizon::getCurrentExitWaypoint()
{
  return m_segGoals.front().exitWaypointID;
}

int PlanningHorizon::getSize()
{
  return m_segGoals.size();
}

void PlanningHorizon::clear()
{
  return m_segGoals.clear();
}
