#ifndef POINT_HH_
#define POINT_HH_



class Point {

public: 
	Point(double x, double y, double z);
	Point();
	~Point();
	double getXCoordinate();
	double getYCoordinate();
	double getZCoordinate();
private: 
	double m_x;
	double m_y;
	double m_z;

};

#endif /*POINT_HH_*/
