#ifndef LINE_HH_
#define LINE_HH_


#include <vector>
#include "frames/point2.hh"

class Line {

public:

//TODO fix 
//typedef enum { NO_BOUNDARY,DASHED_WHITE, DOUBLE_YELLOW } Boundary_Type;

//Line(int lineId, std::vector<Point>, Boundary_Type bdryType);	
Line(int lineId);
Line();		
~Line();
point2arr getPoints();
//Boundary_Type getBoundaryType();
bool isPassable();
int getId();
point2 getClosestPoint(point2 point);
int getIndexOfClosestPoint(point2 point);

private: 

int m_id;
point2arr m_points;

};

#endif /*LINE_HH_*/

