#include "ZoneToRoadRegion.hh"
#include <math.h>


ZoneToRoadRegion::ZoneToRoadRegion(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distZoneExit(1)
{

}

ZoneToRoadRegion::~ZoneToRoadRegion()
{

}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    if ((m_verbose) || (m_debug)) {
        cout << "in ZoneToRoadRegion::meetTransitionConditions(), SHOULD NOT BE HERE " << endl;
    }
    return 0.0;
}

double ZoneToRoadRegion::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel pointLabel)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in ZoneToRoadRegion::meetTransitionConditions() " << endl;
    }
    
    // This is some distance to some waypoint
    point2 exitWaypt;
    point2 currPos = AliceStateHelper::getPositionFrontBumper(vehState);
    localMap->getWaypoint(exitWaypt, pointLabel);
    double angle;
    localMap->getHeading(angle, pointLabel);
    if ((m_verbose) || (m_debug)) {
        cout << "heading = " << angle << endl;
    }
    double dotProd = (-exitWaypt.x + currPos.x) * cos(angle) + (-exitWaypt.y + currPos.y) * sin(angle);
    if ((m_verbose) || (m_debug)) {
        cout << "in iscomplete(): dot product  = " << dotProd << endl;
    }

    if (dotProd > -m_distZoneExit)
        m_trafTransProb = 1;
    else
        m_trafTransProb = 0;
    setUncertainty(m_trafTransProb);
    
    return m_trafTransProb;
}
