#include "RoadToApproachInterSafety.hh"
#include <math.h>


RoadToApproachInterSafety::RoadToApproachInterSafety(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distSafeStop(22)
{

}

RoadToApproachInterSafety::~RoadToApproachInterSafety()
{

}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    if ((m_verbose) || (m_debug)) {
        cout << "in RoadToApproachInterSafety::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
    }
    
    return 0.0;
}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel ptLabel)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in RoadToApproachInterSafety::meetTransitionConditions() " << endl;
    }
    
    double distance = 0;
    int stopLineErr = -1;
    point2 stopLinePos;
    /* PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint()); */
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

    // Use getNextStopline() call which gives the next stopline in my lane
    stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);
    /* Or should we use: stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
                         stopLineErr = localMap->getStopline(stopLinePos, ptLabel); */
    distance = stopLinePos.dist(currFrontPos);
    if ((m_verbose) || (m_debug)) {
        cout << "distance to stopline = " << distance << endl;
    }
 
    double angle;
    localMap->getHeading(angle, stopLinePos);   
    double AliceHeading = AliceStateHelper::getHeading(vehState);
    double headingDiff = fmod(angle-AliceHeading,M_PI);

    if ((distance <= m_distSafeStop) && (fabs(headingDiff)<M_PI/12) && (distance<10))
        m_trafTransProb = 1;
    else
        m_trafTransProb = 0;

    setUncertainty(m_trafTransProb);
    
    return m_trafTransProb;
}
