#include "Passing3_LaneKeepingToLaneChange.hh"

Passing3_LaneKeepingToLaneChange::Passing3_LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing3_LaneKeepingToLaneChange::Passing3_LaneKeepingToLaneChange()
{

}

Passing3_LaneKeepingToLaneChange::~Passing3_LaneKeepingToLaneChange()
{

}

double Passing3_LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in Passing3_LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        // How do I define if an obstacle is passed
        bool obstacle_passed = true;
        if (planHorizon.getSegGoal(0).exitLaneID != map->getLaneID(AliceStateHelper::getPositionRearBumper(vehState)) && obstacle_passed) {
            m_prob = 1;
        }
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing3_LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
