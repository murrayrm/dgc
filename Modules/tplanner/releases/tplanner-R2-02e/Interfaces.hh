#ifndef TRAFFICPLANNERINTERNALINTERFACES_HH
#define TRAFFICPLANNERINTERNALINTERFACES_HH

#include "gcinterfaces/GcModuleInterfaces.hh"


enum VehicleCap{ MAX_CAP, MEDIUM_CAP, MIN_CAP };

  enum DirectiveName{ SEGMENT_GOALS, PAUSE, BACK_UP, END_OF_MISSION };
// Directive from Route Planner 
struct RoutePlannerDirective : public ITransmissive
{
  RoutePlannerDirective()
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int goalID;
  DirectiveName name;
  // Parameters
  // The waypoint id of the next checkpoint
  int segmentID;
  int laneID;
  int waypointID;
  VehicleCap vehicleCap;

  virtual unsigned getDirectiveId() { return goalID; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &goalID;
    ar &segmentID;
    ar &laneID;
    ar &waypointID;
  }
  string toString() const {
    return "";
  }


};

struct RoutePlannerDirectiveResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ NO_POSSIBLE_ACTION, NO_CORRIDOR};
  RoutePlannerDirectiveResponse()
  {
    // initialize the segment goals to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }
  int m_id;
  Status status;
  ReasonForFailure reason;

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &m_id;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }

};


struct TrafficManagerResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned id;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return id; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &id;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};


struct CorrGenResponse : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned id;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return id; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &id;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct CorridorCreate : public ITransmissive
{

  unsigned int m_Id;
  DirectiveName name;
  // Parameters

  CorridorCreate()
    :m_Id(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~CorridorCreate()
  {
  }

  unsigned int getDirectiveId() {
    return m_Id;
  }

  string toString() const {
    return "CorridorCreate";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & m_Id;
  }

};

struct CorridorCreateStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned id;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return id; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &id;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

struct TrajectoryCreate : public ITransmissive
{

  unsigned int m_Id;
  DirectiveName name;
  // Parameters

  TrajectoryCreate()
    :m_Id(0)
  {
    // initialize THIS to zeros to appease the memory profilers
    memset(this, 0, sizeof(*this));
  }

  ~TrajectoryCreate()
  {
  }

  unsigned int getDirectiveId() {
    return m_Id;
  }

  string toString() const {
    return "TrajectoryCreate";
  }

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)  
  {
    ar & m_Id;
  }

};

struct TrajectoryCreateStatus : public GcInterfaceDirectiveStatus
{
  enum Status{ REJECTED, FAILED, READY_FOR_NEXT, COMPLETED, EXECUTING };
  enum ReasonForFailure{ R1, R2, R3 };
  unsigned id;
  Status status;
  ReasonForFailure reason;
  virtual unsigned getDirectiveId() { return id; }
  virtual int getCustomStatus() { return status; }

  /*! Serialize function */
  template<class Archive>
  void serialize(Archive &ar, const unsigned int version) {
    ar & boost::serialization::base_object<GcInterfaceDirectiveStatus>(*this);
    ar &id;
    ar &status;
    ar &reason;
  }
  string toString() const {
    return "";
  }
};

#endif //TRAFFICPLANNERINTERNALINTERFACES_HH
