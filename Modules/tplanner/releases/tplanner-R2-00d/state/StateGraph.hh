#ifndef STATEGRAPH_HH_
#define STATEGRAPH_HH_


#include <vector>
#include "StateTransition.hh"

class StateGraph  {

public:

  StateGraph();
  ~StateGraph();
  StateGraph(std::vector<StateTransition*> stateTransitions);
  void addStateTransition(StateTransition* stateTransition);
  std::vector<StateTransition*> getOutStateTransitions(State  *state);
  //State getState(int type);

private:

std::vector<StateTransition*> m_stateTransitions; 

	
}; 
#endif /*STATEGRAPH_HH_*/
