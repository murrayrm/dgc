#include "LaneKeeping.hh"

LaneKeeping::LaneKeeping(int stateId, ControlStateFactory::ControlStateType type)
:ControlState(stateId, type)
{
}

LaneKeeping::~LaneKeeping()
{
}

int LaneKeeping::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
    if ((m_verbose) || (m_debug)) {
        cout << "In LaneKeeping::determineCorridor " << endl;
    }
    
    point2arr leftBound, rightBound, leftBound1, rightBound1;
    SegGoals currSegment = planHoriz.getSegGoal(0);
    double velIn, velOut, acc;
    double FC_accMin, FC_accMax, FC_velMin, FC_velMax, FC_headingMin, FC_headingMax, FC_steeringMin, FC_steeringMax;
    point2 FC_finalPos;

    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);

    switch (traffState->getType()) {
    
    case TrafficStateFactory::ZONE_REGION:
    {
        // We want to plan for current segment only
        // TODO: want to do this properly in the future
        if ((m_verbose) || (m_debug)) {
            cout << "LaneKeeping:: Zone region" << endl;
        }
        
        PointLabel exitWayptLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        if ((m_verbose) || (m_debug)) {
            cout << "exit waypt label = " << exitWayptLabel << endl;
        }
        
        point2 exitWaypt;
        int wayptErr = localMap->getWaypoint(exitWaypt, exitWayptLabel);
        if (wayptErr != 0) {
            cerr << "LaneKeeping.cc: waypt read from map error" << endl;
            return (wayptErr);
        }

        if ((m_verbose) || (m_debug)) {
            cout << "exit waypt pos = " << exitWaypt << endl;
        }
        
        // Create a corridor of LaneWidth from current position to entry point
        double corrHalfWidth = 3;
        double theta = atan2(exitWaypt.y - currFrontPos.y, exitWaypt.x - currFrontPos.x);
        point2 temppt;
        
        // Left Boundary
        // pt 1 on left boundary
        temppt.x = currRearPos.x + corrHalfWidth * cos(theta + M_PI / 2);
        temppt.y = currRearPos.y + corrHalfWidth * sin(theta + M_PI / 2);
        leftBound.push_back(temppt);
        // pt 2 on left boundary
        temppt.x = exitWaypt.x + corrHalfWidth * cos(theta + M_PI / 2);
        temppt.y = exitWaypt.y + corrHalfWidth * sin(theta + M_PI / 2);
        leftBound.push_back(temppt);
        // Right Boundary
        // pt 1 on right boundary
        temppt.x = currRearPos.x + corrHalfWidth * cos(theta - M_PI / 2);
        temppt.y = currRearPos.y + corrHalfWidth * sin(theta - M_PI / 2);
        rightBound.push_back(temppt);
        // pt 2 on right boundary
        temppt.x = exitWaypt.x + corrHalfWidth * cos(theta - M_PI / 2);
        temppt.y = exitWaypt.y + corrHalfWidth * sin(theta - M_PI / 2);
        rightBound.push_back(temppt);

        // Add the lane boundaries for the segment beyond the zone region
        //        double dist=30;
        //LaneLabel laneLabel1(nextSegment.entrySegmentID, nextSegment.entryLaneID);
        //localMap->getLaneLeftPoint(temppt, laneLabel1, currSegmentExitWaypt, dist);
        //leftBound.push_back(temppt);
        //localMap->getLaneRightPoint(temppt, laneLabel1, currSegmentExitWaypt, dist);
        //rightBound.push_back(temppt);

        // Specify the velocity profile
        velIn = 3;
        velOut = 3;
        acc = 0;

        // specify the ocpParams final conditions - lower bounds
        FC_finalPos = exitWaypt;
        FC_velMin = 0;
        double heading;
        localMap->getHeading(heading, exitWayptLabel);
        //TODO: want to specify some cone around this heading
        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];

        /* IC_velMin = max(velIn, AliceStateHelper::getVelocityMag(vehState));
        IC_velMax = IC_velMin; */    
    }
        break;

    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        if ((m_verbose) || (m_debug)) {
            cout << "LaneKeeping::determineCorridor inside ROAD_REGION" << endl;
        }
        
        // We want to plan for current segment only
        // TODO: want to use function that does not specify specific lane
        //LaneLabel laneLabel1(currSegment.entrySegmentID, currSegment.entryLaneID);
        LaneLabel laneLabel1(localMap->getSegmentID(currFrontPos),localMap->getLaneID(currFrontPos));
        if (laneLabel1.lane == -1)
          laneLabel1 = LaneLabel(currSegment.exitSegmentID, currSegment.exitLaneID);
        double range = 50;
        int getBoundsErr = localMap->getBounds(leftBound1, rightBound1, laneLabel1, currFrontPos, range);
        if (getBoundsErr != 0) {
          cerr << "LaneKeeping.cc: boundary read from map error" << endl;
          return (getBoundsErr);
        }
        
        // Take some subset of these boundary points as the corridor
        // Find the closest pt on the boundary
        int index = TrafficUtils::insertProjPtInBoundary(leftBound1, currRearPos);
        index = TrafficUtils::getClosestPtIndex(leftBound1, currRearPos);
        for (int ii = index; ii < (int) leftBound1.size(); ii++) {
            leftBound.push_back(leftBound1[ii]);
        }
        
        index = TrafficUtils::insertProjPtInBoundary(rightBound1, currRearPos);
        index = TrafficUtils::getClosestPtIndex(rightBound1, currRearPos);
        for (int ii = index; ii < (int) rightBound1.size(); ii++) {
            rightBound.push_back(rightBound1[ii]);
        }
        
        // Specify the velocity profile
        velIn = currSegment.maxSpeedLimit;
        velOut = currSegment.maxSpeedLimit;
        acc = 0;

        // specify the ocpParams final conditions - lower bounds
        // want to define this position based on the corridor?
        PointLabel exitPtLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
        
        // The only difference between a Road Region and Approach Intersection Safety is here
        int FCPos_Err = 0;
        if (traffState->getType() == TrafficStateFactory::ROAD_REGION) {
          // Changed this for road region so that we only plan as far as the map goes.   
          //FCPos_Err = localMap->getWaypoint(FC_finalPos, exitPtLabel);
          FC_finalPos.set((rightBound.back().x+leftBound.back().x)/2,
                        (rightBound.back().y+leftBound.back().y)/2);
          
        } else if (traffState->getType() == TrafficStateFactory::APPROACH_INTER_SAFETY) {
            FCPos_Err = localMap->getNextStopline(FC_finalPos, exitPtLabel);
        }
        
        if (FCPos_Err != 0) {
            cerr << "LaneKeeping.cc: map read error for exit waypt (FC_finalPos)" << endl;
            return (FCPos_Err);
        }
        FC_velMin = 0;
        double heading;
        localMap->getHeading(heading, exitPtLabel);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;

    case TrafficStateFactory::INTERSECTION_STOP:
    {
        // We want to plan over two segments
        SegGoals nextSegGoal = planHoriz.getSegGoal(1);
        point2arr leftBound1, rightBound1, leftBound2, rightBound2;
        // Set of boundary points due to intersection lane
        PointLabel ptLabelIn(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
        PointLabel ptLabelOut(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

        double range = 50;
        int interBoundErr = localMap->getTransitionBounds(leftBound1, rightBound1, ptLabelIn, ptLabelOut, currFrontPos, range);
        if (interBoundErr != 0) {
            cerr << "LaneKeeping.cc: Intersection boundary read from map error" << endl;
            return (interBoundErr);
        }
        
        // Add the boundaries together
        leftBound = leftBound1;
        rightBound = rightBound1;

        // Specify the velocity profile
        velIn = currSegment.maxSpeedLimit;
        velOut = currSegment.maxSpeedLimit;
        acc = 0;

        // Specify the ocpParams final conditions - lower bounds
        // Do we want to define this position based on the corridor?
        PointLabel exitPtLabel(nextSegGoal.exitSegmentID, nextSegGoal.exitLaneID, nextSegGoal.exitWaypointID);
        FC_finalPos.set((rightBound.back().x+leftBound.back().x)/2,
                        (rightBound.back().y+leftBound.back().y)/2);

        FC_velMin = 0;
        double heading;
        localMap->getHeading(heading,exitPtLabel);

        FC_headingMin = heading;  // unbounded
        FC_accMin = corr.getOCPparams().parameters[AMIN_IDX_P];
        FC_steeringMin = corr.getOCPparams().parameters[PHIMIN_IDX_P];
        // specify the ocpParams final conditions - upper bounds
        FC_velMax = velOut;
        FC_headingMax = heading;   // unbounded
        FC_accMax = corr.getOCPparams().parameters[AMAX_IDX_P];
        FC_steeringMax = corr.getOCPparams().parameters[PHIMAX_IDX_P];
    }
        break;
        
    default:
    
        cerr << "LaneKeeping.cc: Undefined Traffic state" << endl;
        
    }
    
    // Assign lines to corridor variables
    corr.addPolyline(leftBound);
    corr.addPolyline(rightBound);

    // Set the velocity profile - need to think this through some
    corr.setVelProfile(localMap, vehState, traffState, this, planHoriz);
    corr.setDesiredAcc(acc);

    // set the final conditions in the ocpspecs
    corr.setOCPfinalCondLB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondLB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
    corr.setOCPfinalCondLB(HEADING_IDX_C, FC_headingMin);
    corr.setOCPfinalCondLB(ACCELERATION_IDX_C, FC_accMin);
    corr.setOCPfinalCondLB(STEERING_IDX_C, FC_steeringMin);
    // specify the ocpParams final conditions - upper bounds
    corr.setOCPfinalCondUB(EASTING_IDX_C, FC_finalPos.y);
    corr.setOCPfinalCondUB(NORTHING_IDX_C, FC_finalPos.x);
    corr.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
    corr.setOCPfinalCondUB(HEADING_IDX_C, FC_headingMax);
    corr.setOCPfinalCondUB(ACCELERATION_IDX_C, FC_accMax);
    corr.setOCPfinalCondUB(STEERING_IDX_C, FC_steeringMax);

    return 0;

    //    test;
}
