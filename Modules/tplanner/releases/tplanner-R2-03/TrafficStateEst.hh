#ifndef TRAFFICSTATEEST_HH_
#define TRAFFICSTATEEST_HH_


#include "map/Map.hh"
#include "TrafficState.hh"
#include "state/StateGraph.hh"
#include "TrafficStateFactory.hh"
#include "TrafficStateTransition.hh"
#include "interfaces/VehicleState.h"
#include "skynettalker/StateClient.hh"
#include "map/MapElementTalker.hh"

class TrafficStateEst : public CStateClient {


protected: 

  /*! The constructors are protected since we want a singleton */
  TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log, bool useRNDF, string RNDFfile);
 
  TrafficStateEst(const TrafficStateEst&);
  ~TrafficStateEst();

public:

  /*! Get the singleton instance */

  static TrafficStateEst* Instance(int skynetKey, bool waitForStateFill, bool debug, 
				   bool verbose, bool log, bool useRNDF, string RNDFfilename);

  /*! Free the singleton instance */
  static void Destroy();

  /*! You are not allowed to copy a singleton */
  TrafficStateEst& operator=(const TrafficStateEst&);

  /*! Estimate traffic state */
  TrafficState* determineTrafficState(PointLabel ptLabel);

  /*! Get traffic state at the last estimate */ 
  TrafficState* getCurrentTrafficState();

  /*! Get the current vehicle state */
  VehicleState getUpdatedVehState();

  /*! Get the vehicle state at the last estimate */
  VehicleState getVehStateAtLastEst();

  /*! Get map at last estimate */
  Map* getMapAtLastEst();

  /*! DEPRECATED Get map at last estimate */
  Map* getUpdatedMap();

  /*! Gets a local map update */
  void getLocalMapUpdate();

private : 

  /*! Get singleton instance */
  static TrafficStateEst* pinstance;

  /*! Get the map at the last estimate */
  void buildTrafficStateGraph();

  /*! Choose the most probable state given the most recent traffic state estimate */
  TrafficState* chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions);

  /*! Get the local to global map update. */
  void getLocalGlobalMapUpdate();

  bool loadRNDF(string filename); 

  bool m_verbose;
  bool m_debug;
  bool m_log;

  /*! The traffic state machine  */
  StateGraph m_trafficGraph;

  /*! The current vehicle state  */
  VehicleState m_currVehState;

  /*! The vehicle state at the last traffic state estimate */
  VehicleState m_vehStateAtLastEst;

  /*! The current traffic state */
  TrafficState* m_currTraffState; 

  /*! The map element talker to receive map updates*/
  CMapElementTalker m_mapElemTalker;

  /*! The map getting updated */
  Map* m_localUpdateMap;

  /*! The map to read  */
  Map* m_localMap;

  /*! The current map */
  pthread_mutex_t m_localMapUpMutex;

  /*! The global to local delta */
  point2 m_gloToLocalDelta;

  /*! Variable to use RNDF or not */
  bool m_useRNDF;

  /*! Variable for RNDF filename */
  string m_RNDFFile; 

  
};

#endif /*TRAFFICSTATEEST_HH_*/
