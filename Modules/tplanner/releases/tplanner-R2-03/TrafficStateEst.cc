#include "TrafficStateEst.hh"
#include "ZoneRegion.hh"
#include "interfaces/sn_types.h"

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile) 
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(waitForStateFill)
  , m_verbose(verbose)
  , m_debug(debug)
  , m_log(log)
  , m_useRNDF(useRNDF)
  , m_RNDFFile(RNDFFile)
{
  m_trafficGraph = TrafficStateFactory::createTrafficStates();  
  m_currTraffState = TrafficStateFactory::getInitialState();  

  
  DGCcreateMutex(&m_localMapUpMutex);

  if (useRNDF){
    cout<<"I am using the RNDF"<<endl;
    if (!RNDFFile.empty()){
      cout<<"RNDF is not empty "<<endl;
      m_mapElemTalker.initRecvMapElement(skynetKey, 1);
      m_mapElemTalker.initSendMapElement(skynetKey);
    } else {
      cout<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    cout<<"TFEST: not using RNDF"<<endl;
  }

  m_localUpdateMap = new Map();
  m_localMap = new Map();

  loadRNDF(RNDFFile);

  UpdateState();
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;
  DGClockMutex(&m_localMapUpMutex);
  m_localUpdateMap->prior.delta = statedelta;
  DGCunlockMutex(&m_localMapUpMutex);
 
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_currTraffState; 
}


TrafficStateEst* TrafficStateEst::Instance(int skynetKey, bool waitForStateFill, bool debug, 
					   bool verbose, bool log, bool useRNDF, string RNDFfilename)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(skynetKey, waitForStateFill, debug, verbose, log, useRNDF, RNDFfilename);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

TrafficState* TrafficStateEst::determineTrafficState(PointLabel exitPtLabel) 
{
  //TODO given sensing info determineTrafficState...
  //Given current traffic state, query graph get  possible transitions list  
  //After a query to the map object, vehicle state, determine possible transitions 
  //for all possible transitions calculate the probability of meeting the transition conditions 
  //VehicleState vehState; 
  UpdateState();
  m_currVehState = m_state;
  m_vehStateAtLastEst = m_state;

  getLocalGlobalMapUpdate();

  std::vector<StateTransition*> transitions = m_trafficGraph.getOutStateTransitions(m_currTraffState);
 
  TrafficStateTransition* transition;

  unsigned int i = 0;

  for (i=0; i < transitions.size(); i++) 
    {
      transition = static_cast<TrafficStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(m_currTraffState, m_localMap, m_currVehState, exitPtLabel);
    }
  m_currTraffState = chooseMostProbableTrafficState(m_currTraffState, transitions);
  return m_currTraffState;
}

TrafficState* TrafficStateEst::getCurrentTrafficState() 
{
  return m_currTraffState; 
}

VehicleState TrafficStateEst::getUpdatedVehState() 
{
  UpdateState();
  m_currVehState = m_state; 
  return m_currVehState;
}

VehicleState TrafficStateEst::getVehStateAtLastEst() 
{
  return m_vehStateAtLastEst;
}

TrafficState * TrafficStateEst::chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions) 
{
  TrafficState *currTrafficState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  TrafficStateTransition* trans; 
  
  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;

  cout<<"Traffic Transitions size "<<transitions.size()<<endl;

  if (transitions.size() > 0) {
    for (i=0; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }

    cout<<"Traffic ProbOneTrans size "<<probOneTrans.size()<<endl;
    if (probOneTrans.size() > 0) {
      trans = static_cast<TrafficStateTransition*> (probOneTrans.front());
      currTrafficState = trans->getTrafficStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        //cout<<"There are no TrafficStateTransitions with probability 1"<<endl;
      }
      currTrafficState = trafState; 
    }
  } else {
    /* TO DO Fix */
  }
 
  return currTrafficState;
}

Map* TrafficStateEst::getMapAtLastEst() {
  return m_localMap;
}

Map* TrafficStateEst::getUpdatedMap() {
  return m_localMap;
}


void TrafficStateEst::getLocalMapUpdate()
{

  MapElement recvEl;
  int bytesRecv;
  cout<<"In local map update ..."<<endl; 
  while (true) {
    bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);
    
    if (bytesRecv>0){
      DGClockMutex(&m_localMapUpMutex);
      m_localUpdateMap->addEl(recvEl);
      DGCunlockMutex(&m_localMapUpMutex);
    }else {
      cout << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
	   << bytesRecv << endl;
      usleep(100);
    }
  }
}

void TrafficStateEst::getLocalGlobalMapUpdate() {

  DGClockMutex(&m_localMapUpMutex);
  m_localMap = m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  m_gloToLocalDelta = m_localMap->prior.delta;

}

bool TrafficStateEst::loadRNDF(string filename) {
  
  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);

}
