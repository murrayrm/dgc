#include "TrafficStateEst.hh"
#include "ZoneRegion.hh"
#include "interfaces/sn_types.h"

TrafficStateEst* TrafficStateEst::pinstance = 0;

TrafficStateEst::TrafficStateEst(int skynetKey, bool waitForStateFill, bool debug, bool verbose, bool log, bool useRNDF, string RNDFFile) 
  : CSkynetContainer(MODtrafficplanner, skynetKey)
    , CStateClient(waitForStateFill)
    , m_verbose(verbose)
    , m_debug(debug)
    , m_log(log)
    , m_followVehTalker(skynetKey, SNleadVehicleInfo, MODtrafficplanner)   
    , m_useRNDF(useRNDF)
    , m_RNDFFile(RNDFFile)
    , m_queueing(false)
    , m_followObsPresent(false)
    , m_followObsId(0)
    , m_vehInOppLane(false)
    , m_oppLaneVehPresent(false)
    , m_oppLaneVehId(0)
{
  m_trafficGraph = TrafficStateFactory::createTrafficStates();  
  m_currTraffState = TrafficStateFactory::getInitialState();  
 
  DGCcreateMutex(&m_localMapUpMutex);
  DGCcreateMutex(&m_followObsDataMutex);
  // DGCcreateMutex(&m_oppLaneVehDataMutex);
  DGCcreateMutex(&m_queueingMutex);

  if (useRNDF){
    cout<<"I am using the RNDF"<<endl;
    if (!RNDFFile.empty()){
      cout<<"RNDF is not empty "<<endl;
      m_mapElemTalker.initRecvMapElement(skynetKey, 1);
      m_mapElemTalker.initSendMapElement(skynetKey);
    } else {
      cout<<"TFEST: RNDFfilename is empty"<<endl;
    }
  } else {
    cout<<"TFEST: not using RNDF"<<endl;
  }

  m_localUpdateMap = new Map();
  m_localMap = new Map();

  loadRNDF(RNDFFile);

  UpdateState();
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  m_localMap->prior.delta = statedelta;
  DGClockMutex(&m_localMapUpMutex);
  m_localUpdateMap->prior.delta = statedelta;
  DGCunlockMutex(&m_localMapUpMutex);
 
}

TrafficStateEst::~TrafficStateEst() 
{
  delete m_currTraffState; 
}


TrafficStateEst* TrafficStateEst::Instance(int skynetKey, bool waitForStateFill, bool debug, 
					   bool verbose, bool log, bool useRNDF, string RNDFfilename)
{
    if (pinstance == 0)
        pinstance = new TrafficStateEst(skynetKey, waitForStateFill, debug, verbose, log, useRNDF, RNDFfilename);
    return pinstance;
}

void TrafficStateEst::Destroy()
{
    delete pinstance;
    pinstance = 0;
}

TrafficState* TrafficStateEst::determineTrafficState(PointLabel exitPtLabel) 
{
  //TODO given sensing info determineTrafficState...
  //Given current traffic state, query graph get  possible transitions list  
  //After a query to the map object, vehicle state, determine possible transitions 
  //for all possible transitions calculate the probability of meeting the transition conditions 
  //VehicleState vehState; 
  UpdateState();
  m_currVehState = m_state;
  m_vehStateAtLastEst = m_state;

  // getLocalGlobalMapUpdate();

  std::vector<StateTransition*> transitions = m_trafficGraph.getOutStateTransitions(m_currTraffState);
 
  TrafficStateTransition* transition;

  unsigned int i = 0;

  for (i=0; i < transitions.size(); i++) 
    {
      transition = static_cast<TrafficStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(m_currTraffState, m_localMap, m_currVehState, exitPtLabel);
    }
  m_currTraffState = chooseMostProbableTrafficState(m_currTraffState, transitions);
  return m_currTraffState;
}

TrafficState* TrafficStateEst::getCurrentTrafficState() 
{
  return m_currTraffState; 
}

void TrafficStateEst::updateVehState() 
{
  UpdateState();
  m_currVehState = m_state; 
}

VehicleState TrafficStateEst::getVehState() 
{
  return m_currVehState;
}


VehicleState TrafficStateEst::getUpdatedVehState() 
{
  UpdateState();
  m_updatedVehState = m_state; 
  return m_updatedVehState;
}

TrafficState * TrafficStateEst::chooseMostProbableTrafficState(TrafficState* trafState, std::vector<StateTransition*> transitions) 
{
  TrafficState *currTrafficState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  TrafficStateTransition* trans; 
  
  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;

  if (transitions.size() > 0) {
    for (i=0; i < transitions.size(); i++) 
      {
	transProb = transitions[i]->getUncertainty();
	if(1 == transProb) {
	  probOneTrans.push_back(transitions[i]);
	} else if ( m_probThresh < transProb) {
	  probAboveThresh.push_back(transitions[i]);		
	} else if ( m_probThresh >= transProb) {
	  probBelowThresh.push_back(transitions[i]);		
	}
      }

    //cout<<"Traffic ProbOneTrans size "<<probOneTrans.size()<<endl;
    if (probOneTrans.size() > 0) {
      trans = static_cast<TrafficStateTransition*> (probOneTrans.front());
      currTrafficState = trans->getTrafficStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        //cout<<"There are no TrafficStateTransitions with probability 1"<<endl;
      }
      currTrafficState = trafState; 
    }
  } else {
    /* TO DO Fix */
  }
 
  return currTrafficState;
}


void TrafficStateEst::getLocalMapUpdate()
{

  MapElement recvEl;
  int bytesRecv;
  cout<<"In local map update ..."<<endl; 
  while (true) {
    bytesRecv = m_mapElemTalker.recvMapElementBlock(&recvEl,1);
    
    if (bytesRecv>0){
      DGClockMutex(&m_localMapUpMutex);
      m_localUpdateMap->addEl(recvEl);
      DGCunlockMutex(&m_localMapUpMutex);
    }else {
      cout << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementNoBlock = " 
	   << bytesRecv << endl;
      usleep(100);
    }
  }
}

void TrafficStateEst::updateMap() {

  DGClockMutex(&m_localMapUpMutex);
  m_localMap = m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  m_gloToLocalDelta = m_localMap->prior.delta;

}

Map* TrafficStateEst::getUpdatedMap() {

  DGClockMutex(&m_localMapUpMutex);
  m_updatedMap = m_localUpdateMap;
  DGCunlockMutex(&m_localMapUpMutex);
  return m_updatedMap;

}

Map* TrafficStateEst::getMap() {
  return m_localMap;
}

bool TrafficStateEst::loadRNDF(string filename) {
  
  m_localUpdateMap->loadRNDF(filename);
  return m_localMap->loadRNDF(filename);

}

void TrafficStateEst::getFollowObstacleUpdate() {

  MapElement me; 
  double distObst = 0, velMagObst =0;
  point2 obsPos; 
  double AliceLength = DIST_REAR_AXLE_TO_FRONT; 
  double sepDist = 4*AliceLength;
  double distThresh = 2 ;

  while (true) {
    distObst = TrafficUtils::getNearestObsInLane(me,getUpdatedMap(), getUpdatedVehState(), 
						 AliceStateHelper::getDesiredLaneLabel());

    if (distObst != -1) { 
      m_followObsPresent = true; 
      m_followObsData.push_back(obsPos);
 
      velMagObst = TrafficUtils::getObstacleVelocityMag(me);
      
      TrafficUtils::getNearestObsPoint(obsPos,getUpdatedMap(), 
                                       getUpdatedVehState(), 
                                       AliceStateHelper::getDesiredLaneLabel(), 0);

      sepDist = getSeparationDistance(getUpdatedVehState());

      DGClockMutex(&m_followObsDataMutex);    

      if (m_followObsData.back().dist(obsPos) > distThresh) {
	m_followObsData.clear();
      }

      /* We want to keep the size of the vector to be relatively small, ie over seconds */
      while (m_followObsData.size() > 99) {
        m_followObsData.pop_front(); //get rid of old data 
      }

      m_followObsData.push_back(obsPos);
      m_followObsId = me.id; 
      DGCunlockMutex(&m_followObsDataMutex);

      /* Send obstacle if we are in APPROACH INTERSECTION SAFETY  */
      DGClockMutex(&m_queueingMutex);    
      if (!m_queueing) {
	velMagObst = 0;
	distObst = 50; 
	sepDist = 2;
      }

      DGCunlockMutex(&m_queueingMutex);

    } else {
      m_followObsPresent = false; 
      velMagObst = 0;
      distObst = 50; 
      sepDist = 2;
      DGClockMutex(&m_followObsDataMutex);    
      m_followObsData.clear();
      DGCunlockMutex(&m_followObsDataMutex);

    }

    m_currFollowObs.clear();
    m_currFollowObs.push_back(velMagObst);
    m_currFollowObs.push_back(distObst);
    m_currFollowObs.push_back(sepDist);  
    
    sendFollowObstacle();
    
    usleep(100000);
  }
}

void TrafficStateEst::getOppositeLaneObstacleUpdate() {

  MapElement me; 
  double distObst = 0, velMagObst =0;
  point2 obsPos; 
  double AliceLength = DIST_REAR_AXLE_TO_FRONT; 
  double sepDist = 4*AliceLength;
  double distThresh = 2 ;

  while (true) {
    
    distObst = TrafficUtils::getNearestObsInOppositeLane(me,getUpdatedMap(), getUpdatedVehState(), 
							 AliceStateHelper::getDesiredLaneLabel());

    if (distObst != -1) { 
      m_oppLaneVehPresent = true;
 
      obsPos.set(me.position);
      m_oppLaneVehData.push_back(obsPos);
 
      velMagObst = TrafficUtils::getObstacleVelocityMag(me);
      
      sepDist = getSeparationDistance(getUpdatedVehState());

      DGClockMutex(&m_oppLaneVehDataMutex);    

      if (m_oppLaneVehData.back().dist(obsPos) > distThresh) {
	m_oppLaneVehData.clear();
      }

      /* We want to keep the size of the vector to be relatively small, ie over seconds */
      while (m_oppLaneVehData.size() > 99) {
        m_oppLaneVehData.pop_front(); //get rid of old data 
      }

      m_oppLaneVehData.push_back(obsPos);
      m_oppLaneVehId = me.id; 
      DGCunlockMutex(&m_oppLaneVehDataMutex);

    } else {
      m_oppLaneVehPresent = false; 
      velMagObst = 0;
      distObst = 50; 
      sepDist = 4*AliceLength;
      DGClockMutex(&m_oppLaneVehDataMutex);    
      m_oppLaneVehData.clear();
      DGCunlockMutex(&m_oppLaneVehDataMutex);
    }
    usleep(100000);
  }
}

void TrafficStateEst::sendFollowObstacle() {

  leadVehInfo followVeh; 

  followVeh.velocity = m_currFollowObs[0];
  followVeh.distance = m_currFollowObs[1];
  followVeh.separationDistance = m_currFollowObs[2];
  m_followVehTalker.send(&followVeh);
}

void TrafficStateEst::updateQueueing() {

  if (isObstacleMoving() || (TrafficStateFactory::APPROACH_INTER_SAFETY == m_currTraffState->getType())) {
    m_queueing = true;
  } else {
    m_queueing = false; 
  }

  DGClockMutex(&m_localMapUpMutex);
  MapElement* mapElem = TrafficUtils::getMapElement(getMap(), m_followObsId);

  if (NULL != mapElem) {
    if (m_queueing) {
      mapElem->type = ELEMENT_VEHICLE;
    } else {
      mapElem->type = ELEMENT_OBSTACLE;
    }
  }
  DGCunlockMutex(&m_localMapUpMutex);
}

void TrafficStateEst::updateVehiclesInOppositeLane() {

  if (isObstacleMovingInOppositeLane()) {
    m_vehInOppLane = true;
  } else {
    m_vehInOppLane = false; 
  }

  DGClockMutex(&m_localMapUpMutex);
  MapElement* mapElem = TrafficUtils::getMapElement(getMap(), m_oppLaneVehId);

  if (NULL != mapElem) {
    if (m_vehInOppLane) {
      mapElem->type = ELEMENT_VEHICLE;
    } else {
      mapElem->type = ELEMENT_OBSTACLE;
    }
  }
  DGCunlockMutex(&m_localMapUpMutex);

}

bool TrafficStateEst::isQueueing() {
  return m_queueing; 
}

bool TrafficStateEst::isVehiclePresentInOppositeLane() {
  return m_vehInOppLane;
}

bool TrafficStateEst::isObstacleMovingInOppositeLane() {

  double distThreshold = 0.02;

  /* By default we consider the obstacle as moving
   * at least until we have enough data to say differently */
  if (m_oppLaneVehPresent) {
    DGClockMutex(&m_oppLaneVehDataMutex);
    m_oppLaneVehDataSnap = m_oppLaneVehData; 
    DGCunlockMutex(&m_oppLaneVehDataMutex);

    int N = sizeOfOppositeLaneVehData();
    if (N < 30) {
      m_oppLaneVehDataSnap.clear();
      return true;
    }

    point2 mean_diff(0, 0);
    for (int i=1; i<N; i++) {
      mean_diff = mean_diff + (m_oppLaneVehDataSnap[i] - m_oppLaneVehDataSnap[i-1]);
    }
    mean_diff = mean_diff/(double)N;
    double avg_diff = mean_diff.norm();

    cout << "Avg dist by car in opposite lane within 0.1 second is " << avg_diff << endl;

    m_oppLaneVehDataSnap.clear();  
    if (avg_diff < distThreshold)
      return false;
    return true;
    
  }

  return false;
}

bool TrafficStateEst::isObstacleMoving() {

  double distThreshold = 0.02;

  /* By default we consider the obstacle as moving
   * at least until we have enough data to say differently */
  if (m_followObsPresent) {
    DGClockMutex(&m_followObsDataMutex);
    m_followObsDataSnap = m_followObsData; 
    DGCunlockMutex(&m_followObsDataMutex);

    int N = sizeOfFollowObsData();
    if (N < 30) {
      m_followObsDataSnap.clear();
      return true;
    }

    point2 mean_diff(0, 0);
    for (int i=1; i<N; i++) {
      mean_diff = mean_diff + (m_followObsDataSnap[i] - m_followObsDataSnap[i-1]);
    }
    mean_diff = mean_diff/(double)N;
    double avg_diff = mean_diff.norm();

    cout << "Avg dist by front car within 0.1 second is " << avg_diff << endl;

    m_followObsDataSnap.clear();  
    if (avg_diff < distThreshold)
      return false;
    return true;
    
  }

  return false;
}

int TrafficStateEst::sizeOfFollowObsData() {
  return m_followObsDataSnap.size();
}

int TrafficStateEst::sizeOfOppositeLaneVehData() {
  return m_oppLaneVehDataSnap.size();
}

double TrafficStateEst::getSeparationDistance(VehicleState vehState) {

  double currVel = 0 ; 
  double AliceLength = VEHICLE_LENGTH;
  double separationDist = 4*AliceLength;
  double baseVel = 4.4704;  // 10 miles/hour converted to m/s 

  // Calculate the separation distance
  currVel = AliceStateHelper::getVelocityMag(vehState);

  switch (m_currTraffState->getType())
    {    
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    case TrafficStateFactory::INTERSECTION_STOP:
      {
        //m_separationDist=2;
        separationDist=1.2*AliceLength;
      }
      break;
    case TrafficStateFactory::ROAD_REGION:
      {
        separationDist = AliceLength*(1.2+currVel/baseVel);
      }
      break;
    default:
      {
        separationDist = 4*AliceLength;
      }
    }
  return separationDist; 

}


