Release R2-04u (Sun Jun 10 10:40:56 2007):
	Added debug information for the 1s-wait-problem at intersections. Debug information will 
	be sent to the log files

Release R2-04t (Sun Jun 10 10:14:00 2007):
	Added uncertainty that Intersection Handling works even when Alice comes to a stop within 
	+-1m of the stopline. Also added sleep(1) after Intersection is clear as it wasn't clear 
	whether Alice stopped or not

Release R2-04s (Sun Jun 10  8:50:30 2007):
	Fixed the cost map for zone region

Release R2-04r (Sat Jun  9 19:17:52 2007):
	Fixed all UTurn completions (and subsequent goal completions).
Release R2-04q (Sat Jun  9 14:43:30 2007):
	Fixed nominal UTurn so that it completes the goal when it gets to stage 5.  
	For all goal completions, fixed the heading difference between Alice and the exit 
waypoint to be between -PI and PI.  
Release R2-04p (Sat Jun  9 14:06:52 2007):	
	Fixed the cost map so s1planner doesn't cut corner.

Release R2-04o (Sat Jun  9 11:41:08 2007):
	Fixed core dump at intersection. Also vehicles won't be tracked anymore. When 
	Alice comes to control state STOPPED, a snapshot of the Intersection will be 
	created. This should increase the reliability as tracking of vehicles is not 
	stable yet.

Release R2-04n (Sat Jun  9 10:31:25 2007):
	Fixed bug in the separation distance causing the car to stop while UTurning or not lane change

Release R2-04m (Sat Jun  9  1:32:55 2007):
        Made a mess while releasing last time, this one should
        be good.

Release R2-04l (Sat Jun  9  0:40:27 2007):
	Added painting (generating polygons) for sensed lanes.
	It's disabled by default, can be turned on using the
	--lanecost commandline switch.

Release R2-04k (Fri Jun  8 22:19:40 2007):
	Fixed bug in RoadToApproachInterSafety
	Fixed front vehicle tracking
	Modified UTurn (which is actually working in simulation now with gcdrive option --steer-speed-threshold=2)

Release R2-04j (Fri Jun  8 13:41:19 2007):
	Made changes to parameters sent to gcfollower.   

Release R2-04i (Fri Jun  8 12:52:13 2007):
	Fixed bug due to false positive detection of obstacles

Release R2-04h (Fri Jun  8 12:28:50 2007):
	Turn signals implemented for lane changing. 
	Implemented opposite lane vehicle tracking, but we are not using it in this release. 
	Queueing update, now we are queueing as soon as we approach an intersection as well.
	We are not sending vehicle information to gcfollower if we are not queueing. 
	Fixed final condition for corridor in zone region.  
	Now waiting 1 second for mplanner seggoals if we have previously failed. 

Release R2-04g (Fri Jun  8 10:59:41 2007):
	Labels obstacles within the debug mapper (on --recv-subgroup=-5) so 
	that you can see the obstacle type, distance, velocity, estimated 
	time of arrival and precedence by selecting an obstacle.

	Also fixed a bug within CheckClearance for rare types of 
	intersections (T-intersections with one-way-street)

Release R2-04f (Thu Jun  7 10:57:49 2007):
	Updated/debugged queueing
	Updated uturns

Release R2-04e (Thu Jun  7 10:23:23 2007):
	Quick fix for mysterious obstacle found at intersection and blocks so that 
	Alice wouldnt move

Release R2-04d (Wed Jun  6 23:23:26 2007):
	Added queueing and following capability in tplanner. These 
functions have been tested with basic simulation scenarios, but will 
likely need a lot of tweaking. The functions seem to work well for 
static obstacles. We are now ignoring map's classification of obstacles 
and doing our own estimation of whether the relavent obstacle is moving 
or not.
	Updated final conditions and velocity specification to reflect 
the above changes.
	Updated the way separation distance is calculated.

Release R2-04c (Wed Jun  6 20:47:53 2007):
	Updated startup in ZoneRegion, Alice should now be able to start in any situation (e.g. turned 90 degree compared to the lane, ...)

Release R2-04b (Wed Jun  6 16:03:30 2007):
	Checks for obstacles behind WayPointExit, distance 15m. Enhanced Debug-Map, recv-subgroup=-5

Release R2-04a (Wed Jun  6  9:56:14 2007):
	Added checks, so that Alice will not pass an obstacle to close to the intersections

Release R2-04 (Wed Jun  6  8:23:47 2007):
	CheckPass handles partially blocked lanes

Release R2-03z (Tue Jun  5 18:43:50 2007):
	Fixed startup region.
	Fixed bug in LaneKeepingToStopObs.

Release R2-03y (Tue Jun  5 18:28:19 2007):
	Supported queueing by querying map for moving obstacles in lane (desired lane)  in front of Alice 
in a separate thread.  If the obstacle is moving, the obstacle information (velocity and relative distance) is sent to gcfollower.

Release R2-03x (Tue Jun  5 14:56:20 2007):
 	Send turn signals continuously when needed, and turn signals off when not needed.  (Note: Adrive no
	longer has to implement a turn signal timeout).

Release R2-03w (Tue Jun  5 14:12:36 2007):
Resetting illegal passing flags
	Added isObstacleBlockingLane function in TrafficUtils

Release R2-03w-vcarson (Tue Jun  5 14:36:28 2007):
	Send turn signals continuously when needed, and turn signals off when not needed.  (Note: Adrive no 
longer has to implement a turn signal timeout). 

Release R2-03v (Tue Jun  5 10:30:08 2007):
	Cleaned up comments left over from last night.
	Added comments to be able to debug following.
	Removed antiquated functions.
	Static obstacles: now calculates and uses the separation distances to stop Alice.
	Dynamic obstacle handling needs A LOT more testing, but the static obstacle handling is improved so I wanted to get this release out.

Release R2-03u (Tue Jun  5 10:08:53 2007):
	Add waypoint and obstacles to debug-intersection-map.
	Fix bug in Traffic State Transition at intersections
	fix bug that checkIntersection is called in RoadRegion

Release R2-03t (Tue Jun  5  1:14:01 2007):
	Updated the way final conditions are specified 
for obstacles.
	Updated the max velocity specification for 
nominal driving.
	Updated max velocity specification for obstacles 
(static and dynamic). *might* be enough for basic 
following and queueig behavior, but needs to be tested a 
lot more.
	Changed from CorridorGen to CorridorUtils due to 
a conflict with CSS implementation.
	Started taking out tplannerNoCSS implementation.


Release R2-03s (Mon Jun  4 20:22:53 2007):
	Added Debug-Map
	Fixed few bugs at intersection/precedence
	
Release R2-03r (Mon Jun  4 18:57:57 2007):
	Added functionality in Log to use verbose level. Instead of "cout << ...", everybody should use "Log::getStream(x) << ..." (x being the 
verbose level of the string you want to print). TrafficManager can set the default verbose level with Log::setVerboseLevel(x)

Release R2-03s-ndutoit (Tue Jun  5  0:54:26 2007):
	Updated the way final conditions are specified 
for obstacles.
	Updated the max velocity specification for 
nominal driving.
	Updated max velocity specification for obstacles 
(static and dynamic). *might* be enough for basic 
following and queueig behavior, but needs to be tested a 
lot more.
	Changed from CorridorGen to CorridorUtils due to 
a conflict with CSS implementation.
	Started taking out tplannerNoCSS implementation.

Release R2-03q (Mon Jun  4 13:19:47 2007):
	Fixed checkpoint handling so that we only stop when mplanner tells us to stop at the exit or when there is a stopline.
	Fixed uturn to be able to do nominal uturn as well as obstacle uturn. This includes the transitions.
	Fixed lane change final condition specification (known bug from before).

Release R2-03p (Mon Jun  4  9:59:01 2007):
	Included test map for intersection precedence and removed Control State Creep at IntersectionStop.

Release R2-03o (Sun Jun  3 14:54:14 2007):
	Switched over to corridorgen for corridor specification and final condition specification. This works for nominal driving cases. Need to be 
done still for lane change and obstacle handling.
	Implemented (Vanessa) turn signal commands.

Release R2-03n (Fri Jun  1 17:30:17 2007):
	Updated the transition from LK to Stop to be either velocity dependent, or when we get within 5m of the stopline.	

Release R2-03m (Fri Jun  1 16:54:29 2007):
	Fixed final condition specification to always be inside the corridor.

Release R2-03l (Fri Jun  1  4:23:52 2007):
	Grow obstacle to account for the size of Alice and use gaussian 
	function to gradually decrease the
	cost right outside teh boundary of obstacle so the map is smooth.

Release R2-03k (Fri Jun  1  0:41:27 2007):
	Fixed obstacle handling bug in Corridor.cc
	Fixed Uturn for obstacles (now gives reasonable intermediate points - just need to tweek with s1planner).
	Added transition to uturn for nominal driving (though this still needs work)

Release R2-03j (Thu May 31 19:56:05 2007):
	Fixed the costmap for UTURN state

Release R2-03i (Thu May 31 17:51:48 2007):
	ZR_RR transition updated to plan beyond the first waypoint.

Release R2-03h (Thu May 31 17:07:12 2007):
	Enhancing functionality for intersection handling & Debugging

Release R2-03g (Thu May 31 14:13:02 2007):
	Partial lane blocking is now handled

Release R2-03f (Wed May 30 14:49:37 2007):
	Added partial lane handling (although currently buggy due to a bug in frames)

Release R2-03e (Wed May 30 14:40:45 2007):
	Fixed a bug in the update of the final condition when an obstacle is detected.	

Release R2-03d (Wed May 30 12:18:20 2007):
	Fixed bugs in checkPass function
	Fixed dist check in LaneKeepingToStop
	Fixed LaneChange corridor
	Removed a few sleeps
	Fixed bugs that checks whether in the exit pt or end of corridor is closer and plans to that point.
	Fixed missiong waypoint bug.
	Changed pause handling.
	Fixed insertProfPtInBoundary to always insert a point (in trajUtils).

Release R2-03c (Tue May 29 13:35:31 2007):
	Removed debugging messages. Also fixed bugs in computing parameters
	for painting costmap. (Forgot to clear the memory -- it will work 
	but with poor performance esp if you run it for a long time.). 
	Only changes are in Corridor.cc.

Release R2-03b (Mon May 28 22:06:24 2007):
	* Read parameters for painting costmap from a config file
	* Fixed cost for obstacle (not a function of height anymore).

Release R2-03a (Sat May 26 12:21:02 2007):
	Fixed a few major bugs in the CSS implementation of tplanner. See the Changelog for details, but roughly, tplannerCSS now uses consistent 
vehicle state and map information. Furthermore, Stefano changed the way the overlap of polytopes are generated, and the tplanner-s1planner interface 
seems a lot more stable. I tested using system-tests-R1-00g (which is not the latest) because the latest version places a obstacle at the origin 
which means that tplanner does plan. This will hopefully be fixed soon.	

Release R2-03 (Wed May 23  1:05:13 2007):
	The is a collection of changes made today by the tplanner team:
* Updated tplanner to use desired lane instead of current lane. This 
makes the corridor generation a lot more robust.
* Fixed a few bugs in the corridor specification, tplanner now supports 
nominal driving (except uturn).
* Corrected map population in CSS implementation so that planner can see 
the obstacles sent by map.
* Updated lane change to use desired lane notions
* continued debugging on uturn, but is not completely done yet.
* Updated the corridorGen function (central function to define basic 
corridor specification). Currently used in LaneKeeping, but need to 
extend to other control states as well.

Release R2-02z (Tue May 22 14:59:03 2007):
	Third try to merge Tamas' changes into latest release.	

Release R2-02u-tamas (Fri May 18 14:58:38 2007):
	Changed all corridor generating functions to use the new map 
getLane() functions, which is much more stable; changed Passing and 
LaneChange transition conditions to use a proper map function to check 
when it changes to a lane going in reverse, and changed LaneChange and 
LaneKeeping corridors to be able to plan in such lanes.

Release R2-02t (Sat May 19 14:57:25 2007):
	tried to fix final pos specification for lane keeping, but had 
to hard code exit pt use (vs. corr exit pt) due to map behavior (which 
is expected) and the convoluted rndf.

Release R2-02s (Sat May 19 13:29:14 2007):
	Fixed a few bugs related to endpoint specification.

Release R2-02r (Thu May 10 11:36:37 2007):
	Fixed *some* bugs with CSS. Fixed CSS port.   
Release R2-02q (Sat May 19 10:12:24 2007):
	Cleaned up old velocity profile specification code. Added 
flag to explicitly NOT add a point on the boundary in 
intersections. Updated RRtoAIS to use more robust map functions 
when checking the distance to exit. THis needs to be done for all 
the other transitions as well. Made FC_pos specification more 
robust for LaneKeeping. We want to do the same for the other 
control states. Fixed buggy Makefile to allow compilation on both 
macosx and lab machines.

Release R2-02y (Tue May 22 14:30:30 2007):
	Added back CSS changes 

Release R2-02x (Tue May 22 13:45:36 2007):
	Reverted back to release u, which was the last stable release.

Release R2-02w (Mon May 21 15:17:32 2007):
	* Retried to merge Tamas' changes into release u. This does not add functionality 
	beyond release u, but is a replacement of release v since the merge did not go 
	through properly.	

Release R2-02u (Sun May 20  0:41:16 2007):
	* Handle the case where frame type of map is undef (which is why obs
	was not drawn in the cost map earlier). Added testCostMapRecv.
	* Properly handle --use-local flag in cost map painting.
	* Noel's changes in the field

Release R2-02t (Sat May 19 14:57:25 2007):
	tried to fix final pos specification for lane keeping, but had 
to hard code exit pt use (vs. corr exit pt) due to map behavior (which 
is expected) and the convoluted rndf.

Release R2-02s (Sat May 19 13:29:14 2007):
	Fixed a few bugs related to endpoint specification.

Release R2-02r (Thu May 10 11:36:37 2007):
	Fixed *some* bugs with CSS. Fixed CSS port.   
Release R2-02q (Sat May 19 10:12:24 2007):
	Cleaned up old velocity profile specification code. Added 
flag to explicitly NOT add a point on the boundary in 
intersections. Updated RRtoAIS to use more robust map functions 
when checking the distance to exit. THis needs to be done for all 
the other transitions as well. Made FC_pos specification more 
robust for LaneKeeping. We want to do the same for the other 
control states. Fixed buggy Makefile to allow compilation on both 
macosx and lab machines.

>>>>>>> .merge-right.r24091
Release R2-02p (Fri May 18 12:05:59 2007):
	Debugging polytope representation of corridor for s1planner. Removed memory leaks due to NGC code.	

Release R2-02o (Thu May 17 16:40:34 2007):
	Fixed the cost map painting so it works with the new version of map

Release R2-02n-ndutoit (Thu May 17 23:46:29 2007):
    Debugging against s1planner. Various changes by different people. Retreive them from the ChangeLog.

Release R2-02n (Mon May 14 13:57:55 2007):
	Updated to work with new map.  Noel fixed something in
	TrafficPlanner.cc for Kenny here as well.

Release R2-02m (Sat May 12 13:43:01 2007):
	Changed the command line argument handling: created a struct which is populated with 
the arguments. This struct is then passed to the CTrafficPlanner constructor. This allows for 
the addition of command line arguments (which will require an update in the arguments struct) 
but not an update to the constructor. This change was also trickled through to Corridor.cc, 
TrafficStateFactory.cc and ControlStateFactory.cc. This will allow easier extension to 
debugging and logging capabilities. Also, this allows for specific flags, such as use-local 
to be implemented at the different layers. Update tplanner.ggo to get rid of unused flags. 

This change may break a few things in CSS implementation (mainly Corridor.cc).

Release R2-02l (Fri May 11 17:09:09 2007):
	Updated the uturn corridor generation and ic/fc specification. This has not been tested in simulation.
	Updated the corridor specification to ensure that Alice is inside the corridor (initial condition).
	Initial testing of polytope corridor representation, but needs a lot more testing.

Release R2-02k (Fri May 11 16:27:19 2007):
	Latest stable release with lane changing and passing implemented more "correctly".
	tplanner need the bitmap module to compile.

Release R2-02j (Wed May  9 21:24:03 2007):
	tplanner that sends parameters for painting cost map when --send-costmap flag is given.

Release R2-02i (Wed May  9 16:50:23 2007):
	Latest stable release with most of the static obstacle handling 
implemented. Uturn for the nominal case needs some more 
debugging/ implementation (when DARPA specifies a uturn, as opposed to a 
uturn due to an obstacle). Lane changing and passing is now implemented. 
Transition from stopped due to an obstacle to passing needs to be 
tested some more. Also, in this release the corridor is sent as convex 
polytopes, in addition to rddf. This functionality needs more testing before 
it will be released as 'stable'. 

Release R2-02h (Fri May  4 10:11:14 2007):
	Merged CSS

Release R2-02g (Mon Apr 30 20:59:50 2007):
	Fixed cost map initialization in TrafficPlanner.cc.  Updated cost
	painting in Corridor.cc to draw cost from general lane bounds. 
	Updated map element talker initialization to work with new map
	version.

Release R2-02f (Mon Apr 30 13:21:06 2007):
	This is a pretty big release of the tplanner. The state machines have been updated. The 
velocity profile specification has been implemented. The corridors are now sent in local 
coordinates. The polytopes representation of the corridor has been implemented. The CSS structure 
has been implemented. ALL OF THIS NEEDS MORE DEBUGGING. This will follow in the next week. Then we 
should be able to deal with static obstacles fairly robustly. Check the ChangeLog for details on 
all the changes.

Release R2-02e (Wed Apr 25 16:40:17 2007):
	Fixed some problems with new release of gcmodule. 

Release R2-02d (Wed Apr 25 14:10:23 2007):
	Initial implementation of transition conditions for the control state machines. Mostly merging with VC's changes.

Release R2-02c (Wed Apr 25 13:22:54 2007):
	Continuing conversion to CSS.  Changed Makefile.yam also to 
compile tplanner as library and two different executables: the CSS 
version of tplanner, and the non CSS version of tplanner.  
 
Release R2-02b (Tue Apr 24 11:56:06 2007):
	Changed the mission complete check so that we can deal with roads that 
loop back on itself (added abs distance and heading conditions). Also had to 
change some transition conditions for the traffic states. 

Release R2-02a (Mon Apr 23 18:41:24 2007):
	Merged Sven's changes. Added internal logging capability. Stepped up 
release number since the previous release was for the CSS (structural change) but 
the release number did not step up.

Release R2-01j (Mon Apr 23 18:07:59 2007):
	Initial conversion into CSS

Release R2-01j-ndutoit (Mon Apr 23 18:30:24 2007):
	Merging in Sven's clean-up of the code. Pretty extensive changes to the states and 
state transition files.

Release R2-01i (Sat Apr 21 19:20:31 2007):
	Updated some of the costviewer files (+ Makefile.yam) to allow to
	compile under OS X.  Basically uses the OpenGL and GLUT frameworks
	instead of the unix library equivalents.  Shouldn't have any effect
	outside of OS X.

Release R2-01i-vcarson (Mon Apr 23 17:57:42 2007):
	Converted to CSS. Needs some debugging still.

Release R2-01h (Thu Apr 12 16:31:57 2007):
	Fixed a small bug with the console.	

Release R2-01g (Wed Apr 11 15:46:06 2007):
	Moved cost map generation to correct place in planning cycle. Also added 'requestFullMap' functionality. This will require some more debugging and testing, but Sam will be working on 
that part, and this is in preparation for him.	

Release R2-01f (Tue Apr 10  8:40:50 2007):
	Added basic console to tplanner. Also implemented some debugging and 
verbose functionality.

Release R2-01e (Mon Mar 19 14:21:14 2007):
	Post field test 2 release of tplanner. Only real change was the eend-of- 
	mission velocity profile specification. Currently the cost map 
	generation and sending thread is not started up. If you need this, 
	umcomment the line in TrafficPlannerMain.cc. 

Release R2-01d (Sun Mar 18  8:53:27 2007):
	Changed switching from uturn state so that we always switch from that state, no
	matter what traffic state we are in. It just checks what the current seggoal is,
	and if it is not uturn, and we have completed the uturn, then we transition.	

Release R2-01c (Sat Mar 17 20:26:48 2007):
	Made tplanner more robust in intersections. Tested with complicated sequence and 
	works for the most part - still some issue with switching out of the uturn state, 
	but I believe that is because we end to close to an intersection in the opposite dir.

Release R2-01b (Sat Mar 17 13:48:46 2007):
	Debugged intersection functions. added mplanner-tplanner comm during uturn. 
	uturn needs more debugging.	

Release R2-01a (Fri Mar 16 12:07:52 2007):
	Traffic planner that uses skynettalker to communite with mission planner.

Release R2-00d-ndutoit (Fri Mar 16 23:02:41 2007):
	Merging with new release of tplanner.

Release R2-00d (Fri Mar 16 11:16:01 2007):
	Added robust goal and traffic state distance checking 
(based on hyper plane now, not just dist). Also adjusted 
stop.cc vel profile. Checked middle of segment (non stop) end 
of mission/checkpt crossing - seems ok now. Tested with new 
rddfplanner (planning to the end pts) and that also seems ok.

Release R2-00c (Thu Mar 15 21:16:01 2007):
		turned on talker to listen to obstacle messages from map.  removed
	hard coded obstacle.

Release R2-00b (Thu Mar 15 14:19:18 2007):
	Created testCState.cc to check one control state (specified) 
at a time. Updated UTurn.cc - now commands a reverse traj, and then 
a fwd traj to test switching of the stack. Removed dependency on 
OCPtSpecs by using new interfaces file (TpDpInterface.hh). 

Release R2-00a (Tue Mar 13 15:19:07 2007):
	Added ocpParams to tplanner, and this struct is now populated in the planner.
	Implemented interface for ocpParams, and did initial testing of send function
	by running testOCPSpecs function (which receives the message).

Release R2-00 (Tue Mar 13  0:18:57 2007):
	Updated state machines to separate transitions and mission complete 
checks. Also, tracked down a bug related to the changed segGoals interface.	

Release R1-00i (Fri Mar  9 12:23:09 2007):
Updated tplanner to include MapElementTalker.hh in the new
	location.  No change in functionality.

Release R1-00h (Fri Mar  9 10:35:21 2007):
	This is the new tplanner!!! The traffic and control state 
machines have been implemented. It uses the new map. The interface 
between mapper and tplanner has NOT been implemented. The interfaces 
with mplanner and dplanner has not changed. I do not use the GloNavMap 
anymore, but just obtain that data from map (which is instantiated from 
an rndf locally and not updated). 
	This version of the tplanner is stable for the following case 
only: drive down a road segment, come to a stop at an intersection, 
continue STRAIGHT through the intersection, and then drive down the road 
again. The files necessary to run this case are included in the release: 
tplanner_unittest.rndf, tplanner_unittest_straight.mdf, rddf.rddf (for 
asim). There are still bugs, including checking for mission complete 
(interaction with traffic state transitions), end of mission handling, 
and some others. 
	This version SHOULD be able to make left and right turns at the 
intersection, but that depends on map capability for the short term. 
Thus it has not been tested, and there are no guarentees. 
	
Release R1-00g (Wed Feb 21 13:13:53 2007):
	Fixed TrafficPlanner.cc so it compiles against revision R2-00b of interfaces.

Release R1-00f (Tue Feb 20 17:25:22 2007):
	Fixed include statements so this version of tplanner compiles against revision R2-00a of interfaces and skynet

Release R1-00e (Fri Feb  2  8:01:13 2007):
  Implemented new mapper->planner interface.  Now stopping at closest detected line.  Fixed broken link in Makefile

Release R1-00d (Wed Jan 31 14:25:50 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00c (Wed Jan 31 12:00:07 2007):
	Fixed tplanner's state interfaces - this change was necessary since the StateClient interface was changed

Release R1-00b (Mon Jan 29  8:10:54 2007):
	Changed trafficUtils.{cc,hh} to TrafficUtils.{cc,hh} so they 
comply with the coding standards.

Release R1-00a (Sun Jan 28 18:01:08 2007):
	Added basic files for tplanner modules. Also modified tplanner to get rid of the startup problem.

Release R1-00 (Sat Jan 27  3:56:30 2007):
	Created.













































=======
=======



>>>>>>> .merge-right.r23109

>>>>>>> .merge-right.r23850






<<<<<<< .working
=======
>>>>>>> .merge-right.r23809



=======
=======
<<<<<<< .working

=======

=======
=======
=======

>>>>>>> .merge-right.r22963

>>>>>>> .merge-right.r23109

>>>>>>> .merge-right.r23850





>>>>>>> .merge-right.r24064
>>>>>>> .merge-right.r24091

>>>>>>> .merge-right.r24450









































































