#ifndef LOG_HH_
#define LOG_HH_

#include <ostream>
#include <stdio.h>
#include <string>

#include "ControlStateFactory.hh"
#include "TrafficStateFactory.hh"
#include "ControlState.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"
#include "TrafficPlanner.hh"
#include "gcinterfaces/SegGoals.hh" 

using namespace std;

class NullStream : public std::ostringstream
{
  public:
    // create a no-op for our nullOStringstream
    template<typename T>
    NullStream& operator<<( T t ) { return *this; }
};

class Log {

  public:

    Log();
    ~Log();

    int init(string filename, bool logit);
    void logSegment(PlanningHorizon ph);
    void logTrafficState(TrafficState *state, ControlState *cstate, PlanningHorizon ph, Map *map);
    void logControlState(ControlState *state);
    void logString(string str);

    static int getVerboseLevel();
    static void setVerboseLevel(int level);
    static ostream& getStream(int level);

  private:
  
    string file;
    FILE *fp;
    bool log;
    int control_id;
    int traffic_id;
    int segment_id;
    int open();
    void close();

    static int verbose_level;
    static NullStream nStream;

};

#endif                          /*LOG_HH_ */
