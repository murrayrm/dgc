#ifndef TRAFFICSTATETRANSITION_HH_
#define TRAFFICSTATETRANSITION_HH_

#include "TrafficState.hh"
#include "interfaces/VehicleState.h"
#include "map/Map.hh"
#include "state/StateTransition.hh"
#include "TrafficUtils.hh"

class TrafficStateTransition:public StateTransition {


  public:

    TrafficStateTransition(TrafficState * state1, TrafficState * state2);
    virtual ~ TrafficStateTransition();
    static void setOutputParams(bool debug, bool verbose, bool log);
    virtual double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState) = 0;
    virtual double meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel ptLabel) = 0;
    TrafficState *getTrafficStateTo();

  protected:
  
    double m_trafTransProb;
    static bool m_verbose;
    static bool m_debug;
    static bool m_log;

  private:
  
    TrafficState * m_trafState1;
    TrafficState *m_trafState2;

};

#endif                          /*TRAFFICSTATETRANSITION_HH_ */
