#ifndef CONTROLSTATEFACTORY_HH_
#define CONTROLSTATEFACTORY_HH_


#include "state/StateGraph.hh"
//#include "ControlState.hh"
#include "CmdLineArgs.hh"

class ControlState;

class ControlStateFactory {
  
  public:
  
  ControlStateFactory();
  ControlStateFactory(bool debug, bool verbose, bool log);
  ControlStateFactory(CmdLineArgs cLArgs);
  ~ControlStateFactory();
  
  static typedef enum {LANE_KEEPING, STOP, STOPPED, UTURN, LANE_CHANGE, CREEP, PAUSE, NOSTATE} ControlStateType;
  static StateGraph createControlStates();
  static int getNextId();
  static void print(int type);
  static string *printString(int type);
  static ControlState *getInitialState();
  static ControlState *getPauseState();


private:
  
  bool m_debug;
  bool m_verbose;
  bool m_log;
  int m_stateId;
  static ControlState *m_initState;
  static ControlState *m_pauseState; 
};

#endif                          /*CONTROLSTATEFACTORY_HH_ */
