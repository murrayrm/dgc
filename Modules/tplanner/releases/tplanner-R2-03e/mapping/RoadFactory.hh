#ifndef ROADBUILDER_HH_
#define ROADBUILDER_HH_



#include "Road.hh"
#include "RoadLink.hh"


class RoadBuilder { 

public:

RoadBuilder();
~RoadBuilder();
void buildRoad();
void buildRoadLink();

private: 


}



#endif /*ROADBUILDER_HH_*/