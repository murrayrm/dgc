// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/RDDFTalker.hh"
//#include "skynettalker/SegGoalsTalker.hh"
//#include "map/MapElementTalker.hh"
//#include "interfaces/CostMapMsg.h"
#include "interfaces/SegGoals.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
//#include "ocpspecs/OCPtSpecs.hh"
#include "interfaces/TpDpInterface.hh"
#include "skynettalker/SkynetTalker.hh"
// Alice std includes
#include "alice/AliceConstants.h"

// Other modules/def's
#include "map/Map.hh"
//#include "cmap/CMapPlus.hh"
#include "dgcutils/RDDF.hh"

// Mapping objects (tplanner version for now)
#include "mapping/Segment.hh"

// tplanner state (graph level)
#include "state/StateGraph.hh"

// new tplanner classes
#include "TrafficPlannerControl.hh"
#include "Corridor.hh"
#include "TrafficState.hh"
#include "ControlState.hh"
#include "TrafficStateEstimator.hh"
#include "TrafficStateFactory.hh"
#include "ControlStateFactory.hh"

#include "RoadRegion.hh"
#include "UTurn.hh"

using namespace std;             

void spoofSegGoals(list<SegGoals> &segGoals);

class dummyTPlanner : public CStateClient , public CRDDFTalker
{
public:
  dummyTPlanner(int skynetKey)
  : CSkynetContainer(MODtrafficplanner, skynetKey)
  , CStateClient(true)
  , m_OCPparamsTalker(skynetKey, SNocpParams, MODtrafficplanner)
  {
    int rddfSocket = m_skynet.get_send_sock(SNrddf);

    // ===================================================
    // GET ALL THE INFO I NEED
    // vehicle state
    UpdateState();
    cout << "curr state = " << AliceStateHelper::getPositionFrontBumper(m_state) << endl;
    // Load local Map
    localMap = new Map();
    string RNDFfilename = "singleroad.rndf";
    bool loadedMap = localMap->loadRNDF(RNDFfilename);
    // insert obstacle
    MapElement el;
    el.set_id(-1);
    el.set_circle_obs();
    point2 tmppt(-.15,-14.3);
    //point2 tmppt;
    //PointLabel ptlabel(1,1,4);
    //int err = localMap->getWaypoint(tmppt, ptlabel);
    el.set_geometry(tmppt,3.0);
    localMap->data.push_back(el);
    // global to local conversion
    point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
    localMap->prior.delta = statedelta;
    point2 m_gloToLocalDelta;
    m_gloToLocalDelta = localMap->prior.delta;
    cout << "map delta = " << m_gloToLocalDelta << endl;

    // spoof the segGoals for now
    list<SegGoals> segGoals;
    spoofSegGoals(segGoals);
    segGoals.front().print(); cout << endl;
    
    // initialize traffic and control state estimators
    //TrafficStateEstimator* m_trafStateEst = new TrafficStateEstimator();
    TrafficPlannerControl* m_tplannerControl = new TrafficPlannerControl();
    
    // initialize traffic and control state factories 
    TrafficStateFactory m_tfac = TrafficStateFactory(); 
    ControlStateFactory m_cfac = ControlStateFactory(); 


    // ==================================================================
    // DEFINE CONTROL AND TRAFFIC STATES
    // current tplanner state variables
    TrafficState * m_currTrafficState = new RoadRegion(0, TrafficStateFactory::ROAD_REGION);
    m_tfac.print(m_currTrafficState->getType());
    ControlState * m_currControlState = new UTurn(0, ControlStateFactory::UTURN);
    PlanningHorizon m_currPlanHorizon; 
    m_tplannerControl->determinePlanningHorizon(m_currPlanHorizon, m_currTrafficState, segGoals);  
    m_cfac.print(m_currControlState->getType());
    

    // ===================================================================
    // generate corridor based on control and traffic state
    Corridor corridor;
    
    // initialize OCPparams
    corridor.initializeOCPparams(m_state, m_currPlanHorizon.getSegGoal(0).minSpeedLimit, m_currPlanHorizon.getSegGoal(0).maxSpeedLimit);
    
    cout << "calculate corridor polylines" << endl;
    m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);

    cout << "convert to rddf" << endl;
    RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);
    //    rddf->print();

    // send the data
    cout << "sending rddf" << endl;
    SendRDDF(rddfSocket,rddf);

    cout << "converting ICs and FCs to global frame" << endl;
    corridor.convertOCPtoGlobal(m_gloToLocalDelta);
    
    cout << "sending OCPparams" << endl;
    bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
    if (!sendOCPparamsErr){
      cout << "TrafficPlanner: problem with send OCP parameters" << endl;
    }

    int pause;
    cin >> pause;
    UpdateState();
    Corridor corridor2;

    corridor2.initializeOCPparams(m_state, m_currPlanHorizon.getSegGoal(0).minSpeedLimit, m_currPlanHorizon.getSegGoal(0).maxSpeedLimit);

    cout << "calculate corridor polylines" << endl;
    m_currControlState->determineCorridor(corridor2, m_state, m_currTrafficState, m_currPlanHorizon, localMap);

    cout << "convert to rddf" << endl;
    RDDF* rddf2 = corridor2.getRddfCorridor(m_gloToLocalDelta);
    // rddf2->print();

    // send the data
    cout << "sending rddf" << endl;
    SendRDDF(rddfSocket,rddf2);

    cout << "converting ICs and FCs to global frame" << endl;
    corridor2.convertOCPtoGlobal(m_gloToLocalDelta);
    
    cout << "sending OCPparams" << endl;
    sendOCPparamsErr = m_OCPparamsTalker.send(&corridor2.getOCPparams());
    if (!sendOCPparamsErr){
      cout << "TrafficPlanner: problem with send OCP parameters" << endl;
    }

    cin >> pause;
    // send the data
    cout << "sending rddf" << endl;
    SendRDDF(rddfSocket,rddf2);

    cout << "sending OCPparams" << endl;
    sendOCPparamsErr = m_OCPparamsTalker.send(&corridor2.getOCPparams());
    if (!sendOCPparamsErr){
      cout << "TrafficPlanner: problem with send OCP parameters" << endl;
    }

  }

  ~dummyTPlanner()
  {
    delete localMap;
  }

  void spoofSegGoals(list<SegGoals> &segGoals)
  {
    SegGoals tmpSegGoal;
    
    tmpSegGoal.segment_type = SegGoals::ROAD_SEGMENT;
    tmpSegGoal.goalID = 1;
    tmpSegGoal.entrySegmentID = 0;
    tmpSegGoal.entryLaneID = 0;
    tmpSegGoal.entryWaypointID = 0;
    tmpSegGoal.exitSegmentID = 1;
    tmpSegGoal.exitLaneID = 1;
    tmpSegGoal.exitWaypointID = 6;
    tmpSegGoal.minSpeedLimit = 0;
    tmpSegGoal.maxSpeedLimit = 5;
    tmpSegGoal.illegalPassingAllowed = false;
    tmpSegGoal.stopAtExit = true;
    tmpSegGoal.isExitCheckpoint = false;
    tmpSegGoal.perf_level = 0;
    
    segGoals.push_back(tmpSegGoal);
    
  }

private:
  SkynetTalker <OCPparams> m_OCPparamsTalker;
  Map * localMap;

};

int main()
{
  // interfaces and communication
  int skynetKey = 130;
  dummyTPlanner dummytplanner = dummyTPlanner(skynetKey);
  
  return 0;
}



