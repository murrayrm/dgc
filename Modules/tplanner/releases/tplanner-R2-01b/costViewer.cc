#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <sys/types.h>
#include <string.h>
#include <pthread.h>
#include "interfaces/sn_types.h"
#include "skynet/sn_msg.hh"
#include "cmap/CMapPlus.hh"
#include "cmap/MapdeltaTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "skynettalker/StateClient.hh"

const int  NUM_ROWS=250;
const int  NUM_COLS=250;
const double ROW_RES=0.40;
const double COL_RES=0.40;

const int WINDOW_WIDTH=1000;
const int WINDOW_HEIGHT=800;

const double BORDER_WIDTH = 60;
const double BORDER_HEIGHT = 60;

const double XMIN=-(double)NUM_COLS/2-BORDER_WIDTH;
const double XMAX= (double)NUM_COLS/2+BORDER_WIDTH;
const double YMIN=-(double)NUM_ROWS/2-BORDER_HEIGHT;
const double YMAX= (double)NUM_ROWS/2+BORDER_HEIGHT;

#define MAX_DELTA_SIZE 1000000

using namespace std;

//OpenGL variables
float lastx, lasty;
bool lbuttondown = false;
bool rbuttondown = false;

//Spread global variables
int flag = 0;
int mess_recvd = 0;

// Rainbow color scale
uint8_t rainbow[0x10000][3];

CMapPlus* tmap = new CMapPlus();
VehicleState currState;
int tmapID_cost;
unsigned long long timestamp;

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

void init(void)
{
  
  // initialize Map
  tmap->initMap(0.0,0.0,NUM_ROWS,NUM_COLS,ROW_RES,COL_RES,0);
  tmapID_cost = tmap->addLayer<double>(0.0, -1.0, true);

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
    {
      d = 4 * ((double) i / 0x10000);
      
      if (d >= 0 && d < 1.0)
	{
	  rainbow[i][0] = 0x00;
	  rainbow[i][1] = (int) (d * 0xFF);
	  rainbow[i][2] = 0xFF;
	}
      else if (d < 2.0)
	{
	  d -= 1.0;
	  rainbow[i][0] = 0x00;
	  rainbow[i][1] = 0xFF;
	  rainbow[i][2] = (int) ((1 - d) * 0xFF);
	}
      else if (d < 3.0)
	{
	  d -= 2.0;
	  rainbow[i][0] = (int) (d * 0xFF);
	  rainbow[i][1] = 0xFF;
	  rainbow[i][2] = 0x00;
	}
      else if (d < 4.0)
	{
	  d -= 3.0;
	  rainbow[i][0] = 0xFF;
	  rainbow[i][1] = (int) ((1 - d) * 0xFF);
	  rainbow[i][2] = 0x00;
	}
      else
	{
	  rainbow[i][0] = 0xFF;
	  rainbow[i][1] = 0x00;
	  rainbow[i][2] = 0x00;
	}
    }
  
}

void drawString (char *s)
{
  unsigned int i;
  for (i = 0; i < strlen (s); i++)
    glutBitmapCharacter (GLUT_BITMAP_HELVETICA_12, s[i]);
};


void display(void)
{
  double cost_value=0;
  int k;
  float x_bottomleft, x_bottomright, x_topleft, x_topright;
  float y_bottomleft, y_bottomright, y_topleft, y_topright;
  static char time_label[100];
  static char title_label[100];
  static char state_label[100];

  glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
  glClear (GL_COLOR_BUFFER_BIT); //clear the color buffer 

  //====================
  //draw bounding box
  //====================
  //vertical lines
  glLineWidth(0.5);
  glColor3f (0.7F, 0.7F, 0.7F);

  glBegin (GL_LINES);
  glVertex2f (-NUM_COLS/2, YMIN+BORDER_HEIGHT);
  glVertex2f (-NUM_COLS/2, YMAX-BORDER_HEIGHT);
  glEnd ();

  glBegin (GL_LINES);
  glVertex2f (NUM_COLS/2, YMIN+BORDER_HEIGHT);
  glVertex2f (NUM_COLS/2, YMAX-BORDER_HEIGHT);
  glEnd ();
  
  
  //horizontal lines
  glLineWidth(0.5);
  glColor3f (0.7F, 0.7F, 0.7F);
  glBegin (GL_LINES);
  glVertex2f (XMIN+BORDER_WIDTH, -NUM_ROWS/2);
  glVertex2f (XMAX-BORDER_WIDTH, -NUM_ROWS/2);
  glEnd ();

  glBegin (GL_LINES);
  glVertex2f (XMIN+BORDER_WIDTH, NUM_ROWS/2);
  glVertex2f (XMAX-BORDER_WIDTH, NUM_ROWS/2);
  glEnd ();

  if(mess_recvd==1||flag==1)
    {
      
      
      //============START MUTEX LOCK=============
      pthread_mutex_lock(&mutex1);
      
      for(int j=0; j<NUM_ROWS; j++)
	{
	  for(int i=0; i<NUM_COLS; i++)
	    {
	      
	      cost_value = tmap->getDataWin<double>(tmapID_cost, i,j);
	      
	      // See if there is any data in this cell before we draw it
	      if (cost_value == 0)
		continue;     
	      
	      // Select color based on elevation relative to vehicle
	      //k = (int) (0x10000 * (float)(cost_value) / 2.0); // MAGIC
	      //if (k < 0x0000) k = 0x0000;
	      //if (k > 0xFFFF) k = 0xFFFF;
	      //glColor3ub(rainbow[k][0], rainbow[k][1], rainbow[k][2]);
	      glColor3f(cost_value/100,cost_value/100,cost_value/100);


	      //start filling in boxes from bottom left of grid

	      x_bottomleft = i-NUM_COLS/2;
	      y_bottomleft = j-NUM_ROWS/2;
	      x_bottomright = i+1-NUM_COLS/2;
	      y_bottomright = j-NUM_ROWS/2;
	      x_topleft = i-NUM_COLS/2;
	      y_topleft = j+1-NUM_ROWS/2;
	      x_topright = i+1-NUM_COLS/2;
	      y_topright = j+1-NUM_ROWS/2;
	      
	      glBegin(GL_QUADS);
	      glVertex2f(x_bottomleft,y_bottomleft);
	      glVertex2f(x_bottomleft,y_topleft);
	      glVertex2f(x_bottomright,y_topright);
	      glVertex2f(x_bottomright,y_bottomright);	
	      glEnd();

	      
	    }
	}

      DGCgettime(timestamp);
	  
      sprintf(time_label, "timestamp : %20.1f", (double)timestamp);
      glColor3f(0.0F,1.0F,0.0F);
      glRasterPos2f ( XMAX-165, YMAX-BORDER_HEIGHT/2);
      drawString (time_label); 

      sprintf(state_label, "state(N,E) :   (%3.4f, %3.4f)", currState.localX, currState.localY);
      glColor3f(0.0F,1.0F,0.0F);
      glRasterPos2f ( XMAX-165, YMAX-BORDER_HEIGHT/2-15);
      drawString (state_label); 
	  
      sprintf(title_label, "TPLANNER:COST MAP VIEWER"); 
      glColor3f(0.0F,1.0F,0.0F);
      glRasterPos2f ( -50, YMAX-BORDER_HEIGHT/2);
      drawString (title_label); 

      
      //============STOP MUTEX LOCK==============
      pthread_mutex_unlock(&mutex1);
    }
  glutSwapBuffers();

}


void resize(int w, int h)
{
  //if (height == 0) height = 1;
  flag = 1;
  glLoadIdentity();
  glViewport (0, 0, w, h); //set the viewport to the current window specifications
  glMatrixMode(GL_PROJECTION);
  gluOrtho2D(XMIN,XMAX,YMIN,YMAX);
}


void *Read_message( void* ptr)
{

  // initialize skynet
  char* skynetKey_char;
  int skynetKey;
  if( (skynetKey_char=getenv("SKYNET_KEY"))!=NULL )
    {
      skynetKey = atoi(skynetKey_char);
      printf("SKYNET_KEY = %d\n", skynetKey);
    }
  else
    {
      printf("Environment Variable SKYNET_KEY not set!!! \n Using SKYNET_KEY=1000 ... \n");
    }

  skynet m_skynet = skynet(MODtrafficplanner,skynetKey, NULL); 

  // The skynet socket for receiving map deltas (from fusionmapper)
  int tmapDeltaSocket = m_skynet.listen(SNtplannerStaticCostMap, MODtrafficplanner);

  // The skynet socket for receiving state information (from SNstate)
  int statesocket = m_skynet.listen(SNstate, MODtrafficplanner);

  if(tmapDeltaSocket < 0)
    cerr << "CTrafficPlanner::getMapDeltasThread(): skynet listen returned error" << endl;

  char* pMapDelta = new char[MAX_DELTA_SIZE];

  int statereceived;	        
  int deltasize;

  while(true)
    {      
      //============START MUTEX LOCK=============
      pthread_mutex_lock(&mutex1);             
	  
      statereceived = m_skynet.get_msg(statesocket, &currState, sizeof(currState), 0);
      if(statereceived>0)
	{
	  tmap->updateVehicleLoc(currState.localX, currState.localY);
	  printf("waiting for delta...\n");	
	  deltasize = m_skynet.get_msg(tmapDeltaSocket, pMapDelta, MAX_DELTA_SIZE, 0);
	  printf("delta received. size is  %d\n", deltasize);
	  if(deltasize>0)
	    {
	      tmap->applyDelta<double>(tmapID_cost, pMapDelta, deltasize);	  
	      mess_recvd = 1;
	    }
	  else
	    {
	      printf("delta not received!\n");
	    }
	  
	}
      
      //==============STOP  MUTEX LOCK===============
      pthread_mutex_unlock(&mutex1); 
      
      usleep(1000);
      
    } 

  delete pMapDelta;

}


void idle (void) 
{
  /* Check flag variable */ 

  //============START MUTEX LOCK============= 
  pthread_mutex_lock(&mutex1);

  if(mess_recvd==1||flag==1)
    {
	glutPostRedisplay();
    }
			  
  //============STOP  MUTEX LOCK=============
  pthread_mutex_unlock(&mutex1);

}





void mouse(int button, int state, int x, int y)
{

  if(button == GLUT_LEFT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  lbuttondown = true;
	}
      else
	{
	  lbuttondown = false;
	}
    }

  if(button == GLUT_RIGHT_BUTTON)
    {
      if(state == GLUT_DOWN)
	{
	  rbuttondown = true;
	}
      else
	{
	  rbuttondown = false;
	}
    }
  

}



void motion(int x, int y)
{
  if (lbuttondown)
    {
      float diffx=(x-lastx)/5; //check the difference between the current x and the last x position  
      float diffy=(y-lasty)/5;

      lastx = x;
      lasty = y;

      glTranslatef(diffx, -diffy, 0);

      glutPostRedisplay();     
    }

  if (rbuttondown)
    {     

      GLint _viewport[4];
      GLdouble _modelMatrix[16];
      GLdouble _projMatrix[16];
      GLdouble objx, objy, objz;

      glGetIntegerv(GL_VIEWPORT,_viewport);
      glGetDoublev(GL_PROJECTION_MATRIX, _projMatrix);
      glGetDoublev(GL_MODELVIEW_MATRIX, _modelMatrix);

      gluUnProject(WINDOW_WIDTH/2,WINDOW_HEIGHT/2,0,_modelMatrix,_projMatrix,_viewport,&objx, &objy, &objz);

      float dy=(y-lasty); //check the difference between the current y and the last y position  

      double s = exp((double)dy*0.01);

      glTranslatef(objx,objy,0);
      glScalef(s,s,s);
      glTranslatef(-objx,-objy,0);

      lasty = y;
      lastx = x;

      glutPostRedisplay();  
    }
  
}

void motionPassive(int x, int y)
{
  lastx = x;
  lasty = y;
}


int main(int argc, char **argv)
{
  init();
  
  int iret1;
  pthread_t thread1;
  
  iret1 = pthread_create( &thread1, NULL, Read_message, NULL);
  
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  glutInitWindowPosition(40, 40);
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow("COSTVIEWER");
  
  //glClearColor(1.0, 1.0, 1.0, 1.0); //white bkgrnd
  glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
  glColor3f(1.0, 1.0, 1.0);
  
  
  /* set callback functions */
  glutDisplayFunc(display); 
  
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutPassiveMotionFunc(motionPassive);
  
  glutReshapeFunc(resize);
  glutIdleFunc(idle);
  
  glutMainLoop();
  
  return 0;
}
