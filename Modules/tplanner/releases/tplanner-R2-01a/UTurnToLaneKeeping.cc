#include "UTurnToLaneKeeping.hh"

UTurnToLaneKeeping::UTurnToLaneKeeping(ControlState* state1, ControlState* state2) 
: ControlStateTransition(state1, state2)
{
}

UTurnToLaneKeeping::UTurnToLaneKeeping()
{

}

UTurnToLaneKeeping::~UTurnToLaneKeeping()
{

}

double UTurnToLaneKeeping::meetTransitionConditions(ControlState *controlState, TrafficState *trafficState, PlanningHorizon planHorizon,Map* map, VehicleState vehState)
{
  cout << "in UTurnToLaneKeeping::meetTransitionConditions" <<endl;

  if (TrafficStateFactory::ROAD_REGION == trafficState->getType()) {
    m_probability = 0;
  }
  setUncertainty(m_probability);
  return m_probability;
} 


