#include "TrafficPlannerControl.hh"


TrafficPlannerControl::TrafficPlannerControl()
{
  m_controlGraph = ControlStateFactory::createControlStates();
 
}

TrafficPlannerControl::TrafficPlannerControl(bool debug, bool verbose, bool log)
{
  m_controlGraph = ControlStateFactory::createControlStates();
  m_debug = debug;
  m_verbose = verbose;
  m_log = log;
}

TrafficPlannerControl::~TrafficPlannerControl()
{

}

ControlState* TrafficPlannerControl::determineControlState(ControlState  *currControlState, TrafficState *currTrafficState, PlanningHorizon currPlanHorizon, Map* localMap, VehicleState currVehState) 
{
  ControlState* controlState; 

	// m_currSegGoals = currSegGoals; 
	// m_localMap = localMap; 
	// m_currVehState = vehState;
  
  std::vector<StateTransition*> transitions = m_controlGraph.getOutStateTransitions(currControlState); 

  //  determinePlanningHorizon();	
  unsigned int i = 0;
  ControlStateTransition* transition;

  for (i = 0; i < transitions.size(); i++) 
    {		
      transition = static_cast<ControlStateTransition*> (transitions[i]);
      transition->meetTransitionConditions(currControlState, currTrafficState, currPlanHorizon, localMap, currVehState);
    }
  
  /* Save old initial conditions */
  VehicleState oldVehState;
  point2 pos = currControlState->getInitialPosition();
  oldVehState.localX = pos.x;
  oldVehState.localY = pos.y;

  controlState = chooseMostProbableControlState(currControlState, transitions);
  
  if (currControlState->getType() != controlState->getType()) {
      controlState->setInitialPosition(currVehState);
      controlState->setInitialVelocity(currVehState);
      controlState->setInitialTime();
  } else if (currControlState->getStateId() != controlState->getStateId()) {
      controlState->setInitialPosition(oldVehState);
      controlState->setInitialVelocity(oldVehState);
      controlState->setInitialTime();
  }
  
  return controlState; 
  //  m_currStateType = (ControlStateFactory::ControlStateType) m_currControlState.getType(); //FIX we may not need this 
}


Conflict TrafficPlannerControl::evaluateDynamicConflicts(Corridor m_currCorridor, ControlState *controlState) 
{
  Conflict conflict;
  return conflict;
}

void TrafficPlannerControl::evaluateCorridors() 
{
  //return m_currCorridor;
}

// ControlState TrafficPlannerControl::getCurrentControlState() 
// {
//   return m_currControlState;
// }

// ControlStateFactory::ControlStateType TrafficPlannerControl::getCurrentControlStateType() 
// {
//   return m_currStateType;
// }


ControlState* TrafficPlannerControl::chooseMostProbableControlState(ControlState* controlState, std::vector<StateTransition*> transitions) 
{
  ControlState* currControlState = controlState;
  vector<StateTransition*> probOneTrans;
  vector<StateTransition*> probAboveThresh;
  vector<StateTransition*> probBelowThresh;
  ControlStateTransition* trans; 

  double transProb = 0;
  double m_probThresh = 0;
  unsigned int i = 0;
  if ((m_verbose) || (m_debug)){  
    cout << "in TrafficPlannerControl::chooseMostProbable... " << transitions.size() << endl;
  }
  if (transitions.size() > 0) {
    for (i=0; i < transitions.size(); i++) 
      {
				transProb = transitions[i]->getUncertainty();
				if(1 == transProb) {
					probOneTrans.push_back(transitions[i]);
				} else if ( m_probThresh < transProb) {
					probAboveThresh.push_back(transitions[i]);		
				} else if ( m_probThresh >= transProb) {
					probBelowThresh.push_back(transitions[i]);		
				}
      }
    if (probOneTrans.size() > 0) {
      trans = static_cast<ControlStateTransition*> (probOneTrans.front());
      currControlState = trans->getControlStateTo(); 
    } else { 
      if ((m_verbose) || (m_debug)){  
        cout<<"There are no ControlStateTransitions with probability 1"<<endl;
      }
      currControlState = controlState; 
    }
  } else { 
    if ((m_verbose) || (m_debug)){  
      cout << "in TrafficPlannerControl::chooseMostProbable... received transitions of size " << transitions.size() << endl;
    }
  }

  return currControlState;
}


void TrafficPlannerControl::determinePlanningHorizon(PlanningHorizon & currPlanHorizon, TrafficState * currTrafficState, list<SegGoals> currSegGoals) 
{
  //cout << "in TrafficPlannerControl::determinePlanningHorizon:" << endl;
  //cout << "currSegGoals size = " << currSegGoals.size() <<endl;
  
  //cout << "curr type = " << currTrafficState->getType() << endl;
  
  TrafficStateFactory::TrafficStateType tStateType = (TrafficStateFactory::TrafficStateType) currTrafficState->getType();
  
  //cout << "tStateType =  "<< tStateType <<endl;
  
  currPlanHorizon.clear();
  
  switch(tStateType) {
  case TrafficStateFactory::ZONE_REGION:		// want to plan over two segments

    currPlanHorizon.addGoal(currSegGoals.front()); 
    currSegGoals.pop_front();
    currPlanHorizon.addGoal(currSegGoals.front()); 		
    break;
  case TrafficStateFactory::ROAD_REGION:
    // plan over one segment
    currPlanHorizon.addGoal(currSegGoals.front()); 
    break;
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    // plan over one segment
    currPlanHorizon.addGoal(currSegGoals.front()); 
    break;
  case TrafficStateFactory::INTERSECTION_STOP:
    // plan over two segments
    currPlanHorizon.addGoal(currSegGoals.front()); 
    currSegGoals.pop_front();
    currPlanHorizon.addGoal(currSegGoals.front()); 		
    break;
  default:
    break;
  };
  if ((m_verbose) || (m_debug)){  
    cout<<"SIZE OF CURRPLANHORIZON GOALS " <<  currPlanHorizon.getSize()<< endl;
  } 

  //m_currPlanHorizon = subSegGoals;
  
}

