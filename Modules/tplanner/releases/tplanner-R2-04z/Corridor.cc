#include <alice/AliceConstants.h>
#include "Corridor.hh"
#include "state/AliceStateHelper.hh"
#include "ControlState.hh"
#include "Log.hh"

#define DISTANCE_TO_OBSTACLE 10

GisCoordLatLon latlon;
GisCoordUTM utm;

Corridor::Corridor()   
  : m_nPolytopes(1)
  , m_polyCorridorTalker(0,SNpolyCorridor,MODdynamicplanner) 
 
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_polyCorridor = new CPolytope[m_nPolytopes];
  m_use_local = true; // Default for now is to use global coordinates - want to change this
  m_polyCorridorLegal = false;
}

Corridor::Corridor(bool debug, bool verbose, bool log)  
  : m_nPolytopes(1)
  , m_polyCorridorTalker(0,SNpolyCorridor,MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = verbose;
  m_debug = debug;
  m_use_local = true; // Default for now is to use global coordinates - want to change this!!
  m_polyCorridorLegal = false;
  
}

Corridor::Corridor(int skynetKey, bool debug, bool verbose, bool log, bool laneCost)
  : m_nPolytopes(1)
  , m_polyCorridorTalker(skynetKey,SNpolyCorridor,MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = verbose;
  m_debug = debug;
  m_use_local = true; // Default for now is to use global coordinates - want to change this!!
  m_polyCorridorLegal = false;
  m_useLaneCost = laneCost;
}

Corridor::Corridor(CmdLineArgs cLArgs)
  : m_nPolytopes(1)
  , m_polyCorridorTalker(cLArgs.sn_key, SNpolyCorridor, MODdynamicplanner)
{
  // This is necessary for RDDF conversion, and can hopefully be removed later
  utm.zone = 11;
  utm.letter = 'S';
  m_verbose = cLArgs.verbose;
  m_debug = cLArgs.debug;
  m_use_local = cLArgs.use_local;
  m_polyCorridorLegal = false;
}

Corridor::Corridor(const Corridor& corr)
  : m_polyCorridorTalker(0,SNocpParams,MODdynamicplanner)
{
  utm.zone = 11;
  utm.letter = 'S';

  m_verbose = corr.m_verbose;
  m_debug = corr.m_debug;
  m_log = corr.m_log;

  m_geometricConstraints = NULL; /* This is not used for now */
  m_conflict = NULL; /* This is not used for now */

  m_polylines = corr.m_polylines;
  m_speedProfile = corr.m_speedProfile;
  m_accProfile = corr.m_accProfile;
  m_velProfile = corr.m_velProfile;
  m_polyCorridorLegal = corr.m_polyCorridorLegal;

  m_controlStateId = corr.m_controlStateId;
  m_desiredAcc = corr.m_desiredAcc;
  m_distFromConStateBegin = corr.m_distFromConStateBegin;
  m_alicePrevPos = corr.m_alicePrevPos;
  m_ocpParams = corr.m_ocpParams;
  m_nPolytopes = corr.m_nPolytopes;

  m_polyCorridor = new CPolytope [m_nPolytopes];
  *m_polyCorridor = *corr.m_polyCorridor;
}

Corridor::~Corridor() 
{
  //TODO why this is getting called at construction
  //delete[] m_polyCorridor; 
}

  
GeometricConstraints* Corridor::getGeometricConstraints() 
{
	return new GeometricConstraints(); 
}

  
int Corridor::getControlStateId() {

	return m_controlStateId;
  
}

void Corridor::addPolyline(point2arr line)
{
  m_polylines.push_back(line);
	// Log::getStream(1) << "m_polylines.size = " << m_polylines.size() << endl;
}

vector<point2arr> Corridor::getPolylines()
{
  return m_polylines;
}

void Corridor::setAlicePrevPos(point2 alicePos)
{
  m_alicePrevPos = alicePos;
}

void Corridor::clear() 
{
  m_polylines.clear();
}


int Corridor::setVelocityLimits(double velMin, double velMax)
{
  m_ocpParams.parameters[VMIN_IDX_P] = velMin;
  m_ocpParams.parameters[VMAX_IDX_P] = velMax;
  return 0;
}

int Corridor::calculateSeparationDist(VehicleState vehState, TrafficState* currTraffState)
{
  int error = 0;
  double currVel = fabs(AliceStateHelper::getVelocityMag(vehState));
  double AliceLength = VEHICLE_LENGTH;
  double baseVel = 4.4704; // 10 miles/hour converted to m/s 
  switch (currTraffState->getType())
    {    
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    case TrafficStateFactory::INTERSECTION_STOP:
      {
        //m_separationDist=2;
        m_separationDist=1.5*AliceLength;
      }
      break;
    case TrafficStateFactory::ROAD_REGION:
      {
        if (currVel<baseVel/2) // less than 5mph
          m_separationDist = 1.5*AliceLength;
        else if (currVel<2.5*baseVel) // between 5 and 25 mph
          m_separationDist = AliceLength*(2+((currVel-baseVel/2)/baseVel));
        else 
          m_separationDist = 4*AliceLength; // more than 25 mph
      }
      break;
    default:
      {
        cerr<<"WARNING: Corridor:calculateSeparationDist() not defined for this traffic state. Setting this distance to maximum (4*AliceLength)" << endl;
        error +=1;
        m_separationDist = 4*AliceLength;
      }
    }
  return error;
}

double Corridor::getSeparationDist()
{
  return m_separationDist;
}


void Corridor::initializeOCPparams(VehicleState vehState, double vmin, double vmax)
{
  m_ocpParams = OCPparams();
  
  // set min and max speed to curr segment min/max speed (from mdf)
  m_ocpParams.parameters[VMIN_IDX_P] = vmin;
  m_ocpParams.parameters[VMAX_IDX_P] = vmax;

  // populate the initial conditions - lower bound
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).y;
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).x;
  m_ocpParams.initialConditionLB[VELOCITY_IDX_C] = AliceStateHelper::getVelocityMag(vehState);
  m_ocpParams.initialConditionLB[HEADING_IDX_C] = vehState.utmYaw;
  m_ocpParams.initialConditionLB[ACCELERATION_IDX_C] = AliceStateHelper::getAccelerationMag(vehState);
  // TODO: get the current steering angle
  m_ocpParams.initialConditionLB[STEERING_IDX_C] = 0;

  // populate the initial conditions - upper bound
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).y;
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = AliceStateHelper::getPositionRearAxle(vehState).x;
  m_ocpParams.initialConditionUB[VELOCITY_IDX_C] = AliceStateHelper::getVelocityMag(vehState);
  m_ocpParams.initialConditionUB[HEADING_IDX_C] = vehState.utmYaw;
  m_ocpParams.initialConditionUB[ACCELERATION_IDX_C] = AliceStateHelper::getAccelerationMag(vehState);
  // TODO: get the current steering angle
  m_ocpParams.initialConditionUB[STEERING_IDX_C] = 0;

  // initialize to fwd mode by default
  m_ocpParams.mode = (int)md_FWD;
}

void Corridor::getOCPinitialPos(point2 &IC_posLB, point2 &IC_posUB)
{
  IC_posLB.x = m_ocpParams.initialConditionLB[NORTHING_IDX_C];
  IC_posLB.y = m_ocpParams.initialConditionLB[EASTING_IDX_C];
  IC_posUB.x = m_ocpParams.initialConditionUB[NORTHING_IDX_C];
  IC_posUB.y = m_ocpParams.initialConditionUB[EASTING_IDX_C];
  return;
}

void Corridor::getOCPfinalPos(point2 &FC_posLB, point2 &FC_posUB)
{
  FC_posLB.x = m_ocpParams.finalConditionLB[NORTHING_IDX_C];
  FC_posLB.y = m_ocpParams.finalConditionLB[EASTING_IDX_C];
  FC_posUB.x = m_ocpParams.finalConditionUB[NORTHING_IDX_C];
  FC_posUB.y = m_ocpParams.finalConditionUB[EASTING_IDX_C];
  return;
}

void Corridor::convertOCPtoGlobal(point2 gloToLocalDelta)
{
  point2 tmpPt, gloTmpPt;
  tmpPt.x = m_ocpParams.initialConditionLB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.initialConditionLB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.initialConditionLB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.initialConditionLB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.initialConditionUB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.initialConditionUB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.initialConditionUB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.initialConditionUB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.finalConditionLB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.finalConditionLB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = gloTmpPt.y;

  tmpPt.x = m_ocpParams.finalConditionUB[NORTHING_IDX_C];
  tmpPt.y = m_ocpParams.finalConditionUB[EASTING_IDX_C]; 
  gloTmpPt = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = gloTmpPt.x;
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = gloTmpPt.y;

  if ((m_verbose) || (m_debug)) { 
    for (int ii=0; ii<6 ; ii++) {
      Log::getStream(1) << "ICLB[" << ii << "] = " << m_ocpParams.initialConditionLB[ii] << endl;
      Log::getStream(1) << "FCLB[" << ii << "] = " << m_ocpParams.finalConditionLB[ii] << " and FCUB[" << ii << "] = " << m_ocpParams.finalConditionUB[ii] << endl;
    }
  }
}

void Corridor::setOCPmode(int mode)
{
  m_ocpParams.mode = mode;
}

void Corridor::setOCPfinalCondLB(int index, double cond)
{
  m_ocpParams.finalConditionLB[index] = cond;
}

void Corridor::adjustFCPosForRearAxle()
{
  // TODO: will need to fix this heading if I decide to specify a non-symm heading range
  double length = -0.05;
  double heading = m_ocpParams.finalConditionUB[HEADING_IDX_C];
  double easting = length*sin(heading);
  double northing = length*cos(heading);

  m_ocpParams.finalConditionUB[NORTHING_IDX_C] = m_ocpParams.finalConditionUB[NORTHING_IDX_C] + northing ;
  m_ocpParams.finalConditionUB[EASTING_IDX_C] = m_ocpParams.finalConditionUB[EASTING_IDX_C] + easting ;
  m_ocpParams.finalConditionLB[NORTHING_IDX_C] = m_ocpParams.finalConditionLB[NORTHING_IDX_C] + northing ;
  m_ocpParams.finalConditionLB[EASTING_IDX_C] = m_ocpParams.finalConditionLB[EASTING_IDX_C] + easting ;
  return;

}

void Corridor::setOCPfinalCondUB(int index, double cond)
{
  m_ocpParams.finalConditionUB[index] = cond;
}

void Corridor::setOCPinitialCondLB(int index, double cond)
{
  m_ocpParams.initialConditionLB[index] = cond;
}


void Corridor::setOCPinitialCondUB(int index, double cond)
{
  m_ocpParams.initialConditionUB[index] = cond;
}

void Corridor::printICFC()
{
    for (int ii=0; ii<6 ; ii++) {
      Log::getStream(1) << "ICLB[" << ii << "] = " << m_ocpParams.initialConditionLB[ii] << " and ICUB[" << ii << "] = " << m_ocpParams.initialConditionUB[ii] << endl;
      Log::getStream(1) << "FCLB[" << ii << "] = " << m_ocpParams.finalConditionLB[ii] << " and FCUB[" << ii << "] = " << m_ocpParams.finalConditionUB[ii] << endl;
    }
    return;
}

void Corridor::printPolylineCorr()
{
  if (m_polylines.size()>0) {
    Log::getStream(1) << "CORRIDOR: leftBound = " << m_polylines[0] << endl;
    Log::getStream(1) << "CORRIDOR: rightBound = " << m_polylines[1] << endl;
  } else {
    Log::getStream(1) << "CORRIDOR: No polyline corridor to print" << endl;
  }
  return;
}


void Corridor::printSpeedLimits()
{
  Log::getStream(1) << "CORRIDOR: Vel Min  = " << m_ocpParams.parameters[VMIN_IDX_P] << " and Vel Max = " << m_ocpParams.parameters[VMAX_IDX_P] << endl;
}

void Corridor::printSeparationDist()
{
  Log::getStream(1) << "CORRIDOR: Separation distance = " << m_separationDist << endl;
}


OCPparams Corridor::getOCPparams()
{
  return m_ocpParams;
}


RDDF* Corridor::convertToRddf(point2 gloToLocalDelta)
{
  // For this I am assuming that the corridor boundaries are specified in ordered pairs
  //  Log::getStream(1) << "in new convertToRddf()" << endl;
  RDDFData rddfData;
  RDDF* rddf = new RDDF();

  if (m_polylines.size() < 2) return rddf;

  point2arr leftBound(m_polylines[0]);
  point2arr rightBound(m_polylines[1]);
  point2 tmpPt, tmpPt_glo;
  double radius;

  if (leftBound.size()==1){
    cerr << "ERROR convRDDF: only one data point on boundaries" << endl;
    return rddf;
  }

  int rddfNum = 1;
  for (int ii=0; ii<(int)leftBound.size(); ii++) {
    tmpPt = (leftBound[ii]+rightBound[ii])/2;
    radius = tmpPt.dist(leftBound[ii]);
    
    tmpPt_glo = AliceStateHelper::convertToGlobal(gloToLocalDelta, tmpPt);
    rddfData.number = rddfNum;
    if (m_use_local){
      rddfData.Northing = tmpPt.x;
      rddfData.Easting = tmpPt.y;
    } else {
      rddfData.Northing = tmpPt_glo.x;
      rddfData.Easting = tmpPt_glo.y;
    }

    rddfData.maxSpeed = 1;//m_speedProfile[ii];
    rddfData.radius = radius;
    rddf->addDataPoint(rddfData);
    rddfNum++;
  }
  if ((m_verbose) || (m_debug)){
    Log::getStream(1)<<"CORRIDOR: OK - convertToRddf"<<endl;
  }

	return rddf;
}


RDDF* Corridor::getRddfCorridor(point2 gloToLocalDelta)
{

	return convertToRddf(gloToLocalDelta);

}
void Corridor::paintLaneCostCenterLine(CMapPlus* map, int costLayer, int tempLayer, 
				       point2arr& leftbound, point2arr &rightbound)
{
  //for now, we just assume a straight corridor and xval refers to the center line x-value

  /*  
      point2arr ptarr = el->geometry;
  
      int ptarrsize= ptarr.size();
      point2 pt = ptarr[0];

      double x = pt.x;
  */

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // assume a known lane-width of 3m on each side
  // THIS SHOULD BE MOVED
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//   double laneWidth=10;

//   int n_laneWidth;
//   int n_laneHalf;
//   int n_left, n_right;

  int num_map_rows;
  int num_map_cols;

//   double cost;
  double colRes, rowRes;
//   int rowWinCenter; 
//   int col_Win_temp;
//   int check_inside;

  colRes = map->getResCols();
  rowRes = map->getResRows();
     
  num_map_rows = map->getNumRows();
  num_map_cols = map->getNumCols();

  //n_laneWidth =(int)(ceil(laneWidth/rowRes));
  //n_laneHalf = (int)(floor(n_laneWidth/2));

  //printf("n_right: %d  n_left: %d\n", n_right, n_left);
  
  //r is row
  //c is column
  point2 lproj,rproj;
  point2 ldpt, rdpt;
  point2 pt;
 
  double lcost;
  double rcost;
  double x, y;
  int val;
  int lside, rside;
  int lindex, rindex;
  int baseval = 100;
  double dval = 20;
  //Log::getStream(1) << "TRYING TO PAINT THE CORRIDOR-------------------------------" <<endl;

  // Log::getStream(1) << "leftbound post offset= "  << leftbound << endl << endl;
  // Log::getStream(1) << "rightbound post offset= "  << rightbound << endl << endl;

  for(int c=0; c<num_map_cols; c++)
    {
      for(int r=0; r<num_map_rows; r++)
        {
          val = map->Win2UTM(r,c,&x,&y);
          //          if ((c<10)&&(r<10)){
          //  map->setDataWin<double>(tempLayer,r,c,54.3);
            //    Log::getStream(1) << "c=" <<c << " r=" <<r <<endl;
            //   Log::getStream(1) << "x=" <<x << " y=" <<y <<endl;

          //}


          
          pt.x =x;
          pt.y =y;

          leftbound.get_side(pt,lside,lindex);
          rightbound.get_side(pt,rside,rindex);

 
          if (rside!=lside){
            if (lindex==0 && rindex==0){
              //              Log::getStream(1) << "plootting " << endl;
              //             if (lindex>0 &&rindex>0 &&lindex<(double)(leftbound.size()-1) 
              //             && rindex< (double) (rightbound.size()-1)){
              lproj = leftbound.project(pt);
              rproj = rightbound.project(pt);
              ldpt = lproj-pt;
              rdpt = rproj-pt;
              lcost = dval/(ldpt.norm()+dval/baseval);
              rcost = dval/(rdpt.norm()+dval/baseval);
              
              if (lcost>rcost)
                map->setDataWin<double>(tempLayer,r,c,lcost);
              else
                map->setDataWin<double>(tempLayer,r,c,rcost);
            }
            
          }
        }
    }
}


void Corridor::convertMapElementToCost(CMapPlus* map, int costLayer, int tempLayer, MapElement& el)
{
  double x,y,radius; 
  double left_bound, right_bound; 
  double top_bound, bottom_bound; 
  double row_res, col_res; 
  int num_cell_cols; 
  int num_cell_rows; 

  double cost; 
  double offset;
  int step_size;
  double COST_DELTA = 1;
  double base_value = 200;

  double old_value;

  //here we update costs on the tempLayer
  if(el.height>0)
    {       
      x = el.center.x; 
      y = el.center.y; 
      radius = el.length;       
      
      row_res = map->getResRows();
      col_res = map->getResCols();     
      
      step_size = (int)floor(radius/row_res);

      for(int k=0; k<step_size; k++)
        {
          offset = radius/step_size;

          radius = radius - k*offset;

          left_bound = x-radius; 
          right_bound = x+radius;
	  
          bottom_bound = y-radius;   
          top_bound = y+radius; 
	  
          num_cell_cols = (int)(ceil( (right_bound-left_bound)/col_res ));
          num_cell_rows = (int)(ceil( (top_bound-bottom_bound)/row_res ));
	  	  
          for(int j=0; j<num_cell_rows; j++)
            {
              for(int i=0; i<num_cell_cols; i++)
                {
                  old_value = map->getDataUTM<double>(tempLayer,left_bound+i*col_res,bottom_bound+j*row_res);
                  if(old_value>0)
                    {
                      cost = old_value + COST_DELTA;
		      
                      if(k==0)
                        {
                          cost = cost + base_value;
                        }
		      
                      map->setDataUTM<double>(tempLayer,left_bound+i*col_res,bottom_bound+j*row_res,cost);
                    }
                }
            }
	  
        }
    }

}

void Corridor::getCostMap(CMapPlus* map, int costLayer, int tempLayer, VehicleState &state, Map* lmap)
{
  //=============================
  //tempLayer vs costLayer
  // - templayer is a layer in the map that is continually filled with new objects from sam's mapper
  //   so more deltas get registered than there should be
  // - costlayer is the map that's actually sent and should have the correct number of deltas 
  //   (more efficient, basically)
  //=============================


  //=============================
  //UPDATE VEHICLE LOCATION FIRST
  //=============================
  map->updateVehicleLoc(state.localX, state.localY);
  
  //==========================================
  //THIS IS WHERE YOU FILL THE MAP WITH VALUES
  //==========================================
  //--eventually this will come from mapper


  /*
    for(int i=200; i<250; i++)
    {
    for(int j =200; j<250; j++)
    {
	  map->setDataWin_Delta<double>(costLayer,i,j,20);
    }
    }
  */  
  
  map->clearLayer(tempLayer);
  
  //NOEL: I took out this code to specify the boundaries determined earlier

  //  MapElement leftbound;
  // MapElement rightbound;
  // LaneLabel llabel;
  //piont2 statept(state.localX,state.localY);

  //llabel.segment = lmap->getSegmentID(statept);
  // llabel.lane = lmap->getLaneID(statept);
  
  // point2arr leftptarr;
  //leftptarr =rightbound.geometry;
  //point2arr rightptarr;
  //rightptarr = leftbound.geometry;;
  //************************************************************
  point2arr leftptarr, rightptarr;
  leftptarr.set(m_polylines[0]);
  rightptarr.set(m_polylines[1]);
  //  Log::getStream(1) << "leftptarr = " << leftptarr << endl;
  // Log::getStream(1) << "rightptarr = " << rightptarr << endl;

  //int val = lmap->getBounds(leftptarr,rightptarr,llabel,statept,60);
  //Log::getStream(1) << "############Left label = " << llabel << endl;
//   double yaw = state.localYaw;

  if ((int)leftptarr.size()<2){
    //  paintLaneCostCenterLine(map, costLayer, tempLayer,yaw);
    cerr << "CORRIDOR: FAILED - Only 1 pt on the boundaries: could not paint the corridor." << endl;
    cerr << "CORRIDOR: FAILED - Only 1 pt on the boundaries: could not paint the corridor." << endl;
  } else {  
    Log::getStream(1) << "CORRIDOR: OK painting corridor" << endl;

    //--------------------------------------------------
    // paint the corridor cost into the map
    //--------------------------------------------------
    paintLaneCostCenterLine(map, costLayer, tempLayer,rightptarr, leftptarr);
  }


  //--------------------------------------------------
  // draw the obstacles into the map
  //--------------------------------------------------
  for(unsigned int i=0; i<lmap->data.size(); i++)
    { 
      convertMapElementToCost(map, costLayer, tempLayer, lmap->data[i]); 
    }  
 
  //now we look at each cell in the tempLayer and compare it with the costLayer; 
  //if the compared cells differ, then update the costLayer
  int num_map_cols;
  int num_map_rows;
 
  double val_tempLayer;
  double val_costLayer;

  num_map_rows = map->getNumRows();
  num_map_cols = map->getNumCols();
  
  for(int i=0; i<num_map_cols; i++)
    {
      for(int j=0; j<num_map_rows; j++)
        {
          val_tempLayer = map->getDataWin<double>(tempLayer,i,j);
          val_costLayer = map->getDataWin<double>(costLayer,i,j);	  
	  
          if(val_tempLayer!=val_costLayer)
            {
              map->setDataWin_Delta<double>(costLayer,i,j,val_tempLayer);
              //map->setDataWin<double>(costLayer,i,j,val_tempLayer);
            }
          
        }
    }
  
  
}


void Corridor::convertLaneToPolygon(vector<Polygon>& polygons, point2arr& leftbound, 
				   point2arr &rightbound, float bval, float cval)
{
  bval = sqrt(bval);
  cval = sqrt(cval);

  if (leftbound.size() <= 1 || rightbound.size() <= 1) {
    cerr << "ERROR: " <<  __FILE__ << ":" << __LINE__ << " leftbound.size() = " 
	 << leftbound.size() << " rightbound.size() = " << rightbound.size() << endl;
    return;
  }

  unsigned leftboundInd = 0;
  unsigned rightboundInd = 0;
  unsigned lastLeftInd = 0;
  unsigned lastRightInd = 0;
  bool useLeft = true;

  while (leftboundInd < leftbound.size() - 1 || rightboundInd < rightbound.size() - 1) {
    if (leftboundInd >= leftbound.size() || rightboundInd >= rightbound.size() ){
      cerr << "ERROR: " <<  __FILE__ << ":" << __LINE__ << "leftboundInd = " 
	   << leftboundInd << " leftbound.size() = " << leftbound.size()
	   << " rightboundInd = " << rightboundInd << " rightbound.size() = " 
	   << rightbound.size() << endl;
      return;
    }
    // if (m_debug) {
    //  Log::getStream(1) << "leftboundInd = " << leftboundInd << " rightboundInd = "
	//   << rightboundInd << endl;
    // }

    vector<float> cost1, cost2;
    point2arr_uncertain vertices1;
    point2arr_uncertain vertices2;

    if (leftboundInd == leftbound.size() - 1)
      useLeft = false;
    else if (rightboundInd == rightbound.size() - 1)
      useLeft = true;
    else if (leftbound.size() == rightbound.size() && 
	     leftboundInd > rightboundInd) {
      useLeft = false;
    }
    else if (leftbound.size() == rightbound.size() && 
	     leftboundInd < rightboundInd) {
      useLeft = true;
    }
    else {
      /*
      float leftDist = leftbound[leftboundInd].dist(leftbound[leftboundInd + 1]) +
	rightbound[rightboundInd].dist(leftbound[leftboundInd + 1]);
      float rightDist = leftbound[leftboundInd].dist(rightbound[rightboundInd + 1]) +
	rightbound[rightboundInd].dist(rightbound[rightboundInd + 1]);
      */
      float leftDist = leftbound[lastLeftInd].dist(leftbound[leftboundInd + 1]);
      float rightDist = rightbound[lastRightInd].dist(rightbound[rightboundInd + 1]);
      if (leftDist < rightDist)
	useLeft = true;
      else
	useLeft = false;
    }

    if (useLeft) {
      lastRightInd = rightboundInd;
      /*
      vertices.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost.push_back(cval);
      vertices.push_back(leftbound[leftboundInd + 1]);
      cost.push_back(bval);
      if (m_debug) {
	Log::getStream(1) << (leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2
	     << ":" << cost[3] 
	     << "\t" << leftbound[leftboundInd + 1] << ":" << cost[4]
	     << endl << endl;
      }
      */
      // Trapezoid
      vertices1.push_back(leftbound[leftboundInd]);
      cost1.push_back(bval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back(leftbound[leftboundInd + 1]);
      cost1.push_back(bval);

      // Triangle
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      vertices2.push_back(rightbound[rightboundInd]);
      cost2.push_back(bval);
      vertices2.push_back((leftbound[leftboundInd + 1] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      /*if (m_debug) {
	Log::getStream(1) << "poly1: " << vertices1[0] << ":" << cost1[0] << "\t" 
	     << vertices1[1] << ":" << cost1[1] << "\t" 
	     << vertices1[2] << ":" << cost1[2] << "\t"
	     << leftbound[leftboundInd + 1] << ":" << cost1[3] << endl;
	Log::getStream(1) << "poly2: " << vertices2[0] << ":" << cost2[0] << "\t"
	     << vertices2[1] << ":" << cost2[1] << "\t" 
	     << vertices2[2] << ":" << cost2[2] << endl << endl;
       }*/

      leftboundInd++;
    }
    else {
      lastLeftInd = leftboundInd;
      /*
      vertices.push_back(rightbound[rightboundInd + 1]);
      cost.push_back(bval);
      vertices.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost.push_back(cval);
      if (m_debug) {
	Log::getStream(1) << rightbound[rightboundInd + 1] << ":" << cost[3] << "\t"
	     << (leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2
	     << ":" << cost[4] << endl << endl;
      }
      */

      // Triangle
      vertices1.push_back(leftbound[leftboundInd]);
      cost1.push_back(bval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost1.push_back(cval);
      vertices1.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost1.push_back(cval);

      // Trapezoid
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd])/2);
      cost2.push_back(cval);
      vertices2.push_back(rightbound[rightboundInd]);
      cost2.push_back(bval);
      vertices2.push_back(rightbound[rightboundInd + 1]);
      cost2.push_back(bval);
      vertices2.push_back((leftbound[leftboundInd] + rightbound[rightboundInd + 1])/2);
      cost2.push_back(cval);
      /*if (m_debug) {
      	Log::getStream(1) << "poly1: " << vertices1[0] << ":" << cost1[0] << "\t" 
      	     << vertices1[1] << ":" << cost1[1] << "\t" 
             << vertices1[2] << ":" << cost1[2] << "\t"
             << leftbound[leftboundInd + 1] << ":" << cost1[3] << endl;
        Log::getStream(1) << "poly2: " << vertices2[0] << ":" << cost2[0] << "\t"
             << vertices2[1] << ":" << cost2[1] << "\t" 
             << vertices2[2] << ":" << cost2[2] << endl << endl;
             }*/
      rightboundInd++;
    }

    Polygon tmpPoly;
    tmpPoly.setVertices(vertices1, cost1);
    tmpPoly.setFillFunc(FILL_SQUARE);
    tmpPoly.setCombFunc(COMB_REPLACE);
    polygons.push_back(tmpPoly);

    tmpPoly.setVertices(vertices2, cost2);
    polygons.push_back(tmpPoly);
  }
}

void Corridor::convertLineToPolygon(vector<Polygon>& polygons, MapElement& el,
                                    float cost, float bval)
{
  const double width = VEHICLE_WIDTH/2;
  if (el.geometryType != GEOMETRY_LINE) {
    cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle geometry type "
        << el.geometryType << " of map element" << endl;
    return;
  }

  // TODO: use uncertainty information! (do like this: increase the width based on uncertainty along
  // the direction of the bisector)

  Polygon p;
  const cost_t sideVal = 2; // go down to 2*sigma (cost*ext(-0.5*2*2)=cost*0.1353)
  // use a gaussian-like function
  p.setFillFunc(FILL_EXP2);
  p.setA(-0.5);
  p.setB(cost);
  p.setC(bval);
  p.setCombFunc(COMB_MAX);

  // for ease of notation
  const point2arr_uncertain& geom = el.geometry;
  for (unsigned int i = 0; i < geom.size()-1; i++)
  {
      // calculate bisectors for vertices v1 and v2. For a generic vertex k, the
      // bisector is the orthogonal to v(k+1) - v(k-1) through the vertex v(k)
      // except for the first and the last, which are treated specially
      point2 bisec[2];
      point2 distVec;

      if (i > 0) {
          distVec = geom[i + 1] - geom[i - 1];
      } else {
          distVec = geom[i + 1] - geom[i];
      }
      bisec[0] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[0].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      if (i + 2 < geom.size()) {
          distVec = geom[i + 2] - geom[i];
      } else {
          distVec = geom[i + 1] - geom[i];
      }
      bisec[1] = point2(-distVec.y, distVec.x); // orthog to distVec
      bisec[1].normalize(); // FIXME: what if the points are so close that the distance is ~zero?

      point2arr vert(4);
      vector<cost_t> val(4);
      // FIXME: there is the possibility that the top egde intersects the
      // bottom edge, for very sharp turns, creating something that is not a polygon
      // (a star? a butterfly? it's just that vertices are not in cw nor in ccw order)
      // The resulting cost map will be weird, but still acceptable, I think.

      // left side
      vert[0] = geom[i + 1] + bisec[1]*width;
      val[0] = -sideVal;
      vert[1] = geom[i]     + bisec[0]*width;
      val[1] = -sideVal;

      // right side
      vert[2] = geom[i]     - bisec[0]*width;
      val[2] = sideVal;
      vert[3] = geom[i + 1] - bisec[1]*width;
      val[3] = sideVal;

      p.setVertices(vert, val);
      polygons.push_back(p);
  }
  
}

void Corridor::convertObstacleToPolygon(vector<Polygon>& polygons, MapElement& el,
                                        float obsCost, float bval)
{
  double sigma = 1*VEHICLE_WIDTH/2;
  if (el.type != ELEMENT_OBSTACLE && el.type != ELEMENT_VEHICLE) {
    // this shouldn't happen, because it's checked in convertMapElementToPolygon()
    cerr << "CORRIDOR: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " cannot handle nonobstacle "
	 << el.type << " of map element" << endl;
    return; 
  }
  if (el.type == ELEMENT_VEHICLE) {
    Log::getStream(1) << "CORRIDOR: el.type = ELEMENT_VEHICLE. Will not be painted in the cost map" << endl;
    return;
  }
  if (el.geometryType != GEOMETRY_POLY) {
    cerr << "CORRIDOR- FAILED ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle geometry type "
	 << el.geometryType << " of map element" << endl;
    return;
  }
  point2arr_uncertain vertices1(el.geometry);
  point2arr_uncertain vertices2(el.geometry);
  if (vertices1.size() == 0) {
    cerr << "CORRIDOR: FAILED ERROR: " << __FILE__ << ":" << __LINE__ << " obstacle has no vertices." << endl;
    return;
  }

  Log::getStream(1) << "CORRIDOR: el.type = ELEMENT_OBSTACLE. Will be painted in the map" << endl;

  //if (m_debug) {
    // Log::getStream(1) << "Obstacle at " << el.geometry << endl;
    //Log::getStream(1) << "  height = " << endl;
  //}
  point2_uncertain obsCenter(0,0);
  for (unsigned i=0; i < vertices1.size(); i++)
    obsCenter = obsCenter + vertices1[i];
  obsCenter = obsCenter/vertices1.size();

  // Grow the obstacle
  for (unsigned i=0; i < vertices1.size(); i++) {
    double distFromCenter1 = obsCenter.dist(vertices1[i]) + 3*VEHICLE_WIDTH/4;
    double distFromCenter2 = obsCenter.dist(vertices1[i]) + 7*VEHICLE_WIDTH/4;
    double directionFromCenter = (vertices1[i] - obsCenter).heading();
    vertices1[i].set_point(obsCenter.x + distFromCenter1*cos(directionFromCenter),
			   obsCenter.y + distFromCenter1*sin(directionFromCenter));
    vertices2[i].set_point(obsCenter.x + distFromCenter2*cos(directionFromCenter),
			   obsCenter.y + distFromCenter2*sin(directionFromCenter));
  }
  vector<float> cost(vertices1.size(), obsCost);
  Polygon tmpPoly;
  tmpPoly.setVertices(vertices1, cost);
  tmpPoly.setCombFunc(COMB_MAX);
  polygons.push_back(tmpPoly);

  // Gaussian function for continuity
  double a = -1/(2*pow(sigma,2));
  tmpPoly.setA(a);
  tmpPoly.setB(obsCost);
  tmpPoly.setC(0);
  tmpPoly.setFillFunc(FILL_EXP2);
  for (unsigned i=0; i < vertices1.size(); i++) {
    point2arr_uncertain tmpVertices;
    cost.clear();

    cost.push_back(0);
    cost.push_back(0);
    double baseX = sqrt((1/a)*log(bval/obsCost));
    cost.push_back(baseX);
    cost.push_back(baseX);
    tmpVertices.push_back(vertices1[i]);
    if (i < vertices1.size() - 1) {
      tmpVertices.push_back(vertices1[i+1]);
      tmpVertices.push_back(vertices2[i+1]);
    }
    else {
      tmpVertices.push_back(vertices1[0]);
      tmpVertices.push_back(vertices2[0]);
    }
    tmpVertices.push_back(vertices2[i]);

    tmpPoly.setVertices(tmpVertices, cost);
    polygons.push_back(tmpPoly);
  }
}


void Corridor::convertMapElementToPolygon(vector<Polygon>& polygons, MapElement& el,
                                         float obsCost, float bval)
{
  switch (el.type)
  {
  case ELEMENT_OBSTACLE:
    convertObstacleToPolygon(polygons, el, obsCost, bval);
    break;

  case ELEMENT_LINE:
  case ELEMENT_LANELINE:
    if (m_useLaneCost) { // it's a new feature, enabled only if --lanecost is specified
      // use a smaller cost for lane lines
      // TODO: add a parameter to PolygonParameters for the cost of lane lines
      convertLineToPolygon(polygons, el, obsCost/4.0, bval);
    }
    break;

 default:
   break;
  }
}


void Corridor::getBitmapParams(BitmapParams& bmparams, PolygonParams &polygonParams,
			       ControlState* controlState, TrafficState* trafficState,
			       VehicleState &state, Map* lmap, vector<MapId> vehIds, point2 gloToLocalDelta)
{
  // Lane boundaries
  if (m_polylines.size() != 2) {
    cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "m_polylines.size() = " 
	 << m_polylines.size() << endl;
    return;
  }

  if (m_use_local) {
    bmparams.centerX = state.localX;
    bmparams.centerY = state.localY;
  }
  else {
    bmparams.centerX = state.utmNorthing;
    bmparams.centerY = state.utmEasting;
  }

  bmparams.polygons.clear();

  point2arr leftptarr, rightptarr;
  leftptarr.set(m_polylines[0]);
  rightptarr.set(m_polylines[1]);
  /*
  if (m_debug) {
    Log::getStream(1) << "leftptarr = " << leftptarr << endl;
    Log::getStream(1) << "rightptarr = " << rightptarr << endl;
  }
  */

  if (leftptarr.size() < 2 || rightptarr.size() < 2){
    cerr << "CORRIDOR: FAILED ERROR: " << __FILE__ << ":" << __LINE__ 
	 << "Only 1 pt on the boundaries: could not paint the corridor." << endl;
  } 
  else {
    if (m_debug) {
      Log::getStream(1) << "CORRIDOR: painting lane" << endl;
    }

    //--------------------------------------------------
    // paint the corridor cost into the map
    //--------------------------------------------------
    if (m_use_local) {
      if (ControlStateFactory::UTURN == controlState->getType() ||
	  TrafficStateFactory::ZONE_REGION == trafficState->getType() ) {
	convertLaneToPolygon(bmparams.polygons, leftptarr, rightptarr, 
			     polygonParams.centerlaneVal, polygonParams.centerlaneVal);
      }
      else {
	convertLaneToPolygon(bmparams.polygons, leftptarr, rightptarr, 
			     bmparams.baseVal, polygonParams.centerlaneVal);
      }
    }
    else {
      point2arr leftptarrGlo, rightptarrGlo;
      for (unsigned i = 0; i < leftptarr.size(); i++) {
	leftptarrGlo.push_back(AliceStateHelper::convertToGlobal(gloToLocalDelta, leftptarr[i]));
      }
      for (unsigned i = 0; i < rightptarr.size(); i++) {
	rightptarrGlo.push_back(AliceStateHelper::convertToGlobal(gloToLocalDelta, rightptarr[i]));
      }
      if (ControlStateFactory::UTURN == controlState->getType() ||
	  TrafficStateFactory::ZONE_REGION == trafficState->getType() ) {
	convertLaneToPolygon(bmparams.polygons, leftptarrGlo, rightptarrGlo, 
			     polygonParams.centerlaneVal, polygonParams.centerlaneVal);
      }
      else {
	convertLaneToPolygon(bmparams.polygons, leftptarrGlo, rightptarrGlo, 
			     bmparams.baseVal, polygonParams.centerlaneVal);
      }
    }
  }
  
    Log::getStream(1) << "CORRIDOR: Got " << lmap->data.size() << " map elements" << endl;
  //--------------------------------------------------
  // draw the obstacles into the map
  //--------------------------------------------------
  for(unsigned int i=0; i<lmap->data.size(); i++) {

    bool is_veh = false;
    for (unsigned int j=0; j<vehIds.size(); j++) {
      if (lmap->data[i].id == vehIds[j])
        is_veh = true;
    }
    if (is_veh) continue;

    if ((lmap->data[i]).type == ELEMENT_OBSTACLE ||
        (lmap->data[i]).type == ELEMENT_VEHICLE ||
        (lmap->data[i]).type == ELEMENT_LINE || 
        (lmap->data[i]).type == ELEMENT_LANELINE ) {

      if ((lmap->data[i]).frameType != FRAME_LOCAL && (lmap->data[i]).frameType != FRAME_UNDEF) {
	cerr << "ERROR: " << __FILE__ << ":" << __LINE__ << "cannot handle frame "
	     << (lmap->data[i]).frameType << " of map element" << endl;
	continue;
      }
      if ((lmap->data[i]).frameType == FRAME_UNDEF) {
	cerr << "Warning: convertMapElementToPolygon: frameType = FRAME_UNDEF" << endl;
      }
      if (m_use_local) {
	/*
	convertMapElementToPolygon(bmparams.polygons, lmap->data[i], 
				   polygonParams.obsCost, polygonParams.centerlaneVal); 
	*/
	convertMapElementToPolygon(bmparams.polygons, lmap->data[i], 
				   polygonParams.obsCost, bmparams.baseVal); 
      }
      else {
	MapElement data = lmap->data[i];
	for (unsigned j = 0; j < data.geometry.size(); j++) {
	  data.geometry[j] = AliceStateHelper::convertToGlobal(gloToLocalDelta, data.geometry[j]);
	}
	/*
 	convertMapElementToPolygon(bmparams.polygons, data, 
				   polygonParams.obsCost, polygonParams.centerlaneVal); 
	*/
 	convertMapElementToPolygon(bmparams.polygons, data, 
				   polygonParams.obsCost, bmparams.baseVal); 
      }
    }
    else if (m_debug) {
      Log::getStream(1) << "CORRIDOR: Get map element of type " << (lmap->data[i]).type << endl;
    }
  }

  if (m_debug)
    Log::getStream(1) << "CORRIDOR: Finish getBitmapParams" << endl;
}

// useful for debugging
void printCPolytope(const CPolytope& pt)
{
  Log::getStream(1) << (&pt) << endl;
}

/*!Function that generates the polyhedral corridor
 * If you think this is a mess:
 * "Welcome to Computational Geometry in pure C++"
 *
 * S Di Cairano Apr-07
 */


void Corridor::generatePolyCorridor()
{  
    bool isLegal = true;
	
  //You should always have right and left boundary
    if (m_polylines.size() > 1 ) 
	{ 
        int Ngates=m_polylines[0].size();
        CPolytope** rawPoly;
        rawPoly=new CPolytope*[Ngates-1];
        point2arr leftBound(m_polylines[0]);
        point2arr rightBound(m_polylines[1]);
        if ((leftBound.size() > 1) && (rightBound.size() > 1)) 
		{
            if (0 != m_polyCorridor) 
			{
	            delete[] m_polyCorridor;  //clearing the previous corridor
            }

	        m_nPolytopes = Ngates-1;
	        m_polyCorridor = new CPolytope [m_nPolytopes]; //creating the empty corridor
	        int NofDim = 2;
	        CAlgebraicGeometry* algGeom = new CAlgebraicGeometry();
	        int NofVtx = 4;
	        double*** vertices;
	        vertices = new double**[m_nPolytopes];
	        for(int k=0; k<Ngates-1;k++)
	        {
	            vertices[k] = new double*[NofDim];
	            for(int i=0;i<NofDim;i++)
	            {
	                vertices[k][i] = new double[NofVtx];
	            }
	        }
			int i = 0 ;
			while( i<m_nPolytopes) 
            {
                vertices[i][0][1]=leftBound[i].x;
                vertices[i][1][1]=leftBound[i].y;
                vertices[i][0][2]=leftBound[i+1].x;
                vertices[i][1][2]=leftBound[i+1].y;
                vertices[i][0][3]=rightBound[i+1].x;
                vertices[i][1][3]=rightBound[i+1].y;
                vertices[i][0][0]=rightBound[i].x;
                vertices[i][1][0]=rightBound[i].y;
                rawPoly[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
      
                ALGErrorType algErr = algGeom->VertexAndFacetEnumeration(rawPoly[i]);
                m_polyCorridor[i] = new CPolytope( &NofDim, &NofVtx, (const double** const)  vertices[i] ); //Ask Melvin: is this Correct?
                algErr = algGeom->VertexAndFacetEnumeration(&m_polyCorridor[i]);
                
				if( algErr != alg_NoError)
				{
				    Log::getStream(1) << "CORRIDOR: error when building raw polytopes" << endl;
				    isLegal = false;
                }
                int nCompVtx = rawPoly[i]->getNofVertices() ;
                if( nCompVtx != 4) 
	            {
                    cerr << "CORRIDOR: The gates: " << i << "," << i+1 << " are degenerate " << endl;
                    Log::getStream(1) << i <<  "L: ("<<leftBound[i].x<< ","<< leftBound[i].y << "), " <<  "R: ("<<rightBound[i].x<< ","<< rightBound[i].y << "), " <<  endl;
                    Log::getStream(1) << i+1 <<  "L: ("<<leftBound[i+1].x<< ","<< leftBound[i+1].y << "), " <<  "R: ("<<rightBound[i+1].x<< ","<< rightBound[i+1].y << "), " <<  endl;
					Log::getStream(1) << rawPoly[i] << endl;
//					Log::getStream(1) << " cutting at polytope "<< i << endl;
//					m_nPolytopes = i;
//					delete rawPoly[i];
//					if(i<1)
//					  isLegal = false;
                }
                i++;
	        }
/*
            i = 0 ; 
	        while( (i<m_nPolytopes) && (isLegal) )
	        {  
			    ALGErrorType algErrIntNext = alg_NoError ;
			    ALGErrorType algErrIntPrev = alg_NoError ;
	            double** verticesFinal;
	            int NofVtxFinal = rawPoly[i]->getNofPoints();
	            double** verticesIntPrev;
	            int NofVtxIntPrev = 0;
	            double** verticesIntNext;
	            int NofVtxIntNext = 0;
	    
	            if(i>0)//expand backwards
		        {
		            int Nrows=rawPoly[i]->getNofRows(); 
		            int Ncols=rawPoly[i]->getNofColumns();
		            double** Amat;
		            double* bvect;
		            bvect = new double[Nrows];
		            Amat= new double*[Nrows];
		            for(int j=0;j<Nrows;j++)
		                Amat[j] = new double[Ncols];
        
		            rawPoly[i]->getA(&Nrows,&Ncols,Amat);
		            rawPoly[i]->getb(&Nrows,bvect);
*/
/*  search for the row that defines the border to be exapnaded. 
  *  It is such that Ai*x1=bi, Ai*x2=bi, hence Ai*(x1+x2)-2bi=0, which is  argmax_i Ai(x1+x2)-2*bi		                  
 */
/*
                    double myVect[NofDim];
                    myVect[0] = leftBound[i].x+rightBound[i].x;
                    myVect[1] = leftBound[i].y+rightBound[i].y;
			        double expNorm = 0.5*sqrt(pow((leftBound[i].x-leftBound[i-1].x),2)+pow((leftBound[i].y-leftBound[i-1].y),2))+
			                                  0.5* sqrt(pow((rightBound[i].x-rightBound[i-1].x),2)+pow((rightBound[i].y-rightBound[i-1].y),2));
			        int expIdx=0;
			        double myMax=-1;
			        for(int j=0;j<Nrows;j++)
			        {
			             double temp = 0;
			             for(int h=0;h<Ncols;h++)
			                  temp+=Amat[j][h]*myVect[h];
				          temp-=2*bvect[j];
                          if(temp>=myMax)
			              {
			                  myMax=temp;
				              expIdx=j;      //index of the row to be expanded
                          }
		            }
			        double normARow=0;
			        for(int j=0;j<Ncols;j++) //compute the norm of the row to be expanded to normalize
			        {
                        normARow+=Amat[expIdx][j]*Amat[expIdx][j];				
                    }
			        bvect[expIdx]+=(EXPPERC*expNorm)*normARow;    // expansion
                    int NrowsPrev=rawPoly[i-1]->getNofRows(); 
                    int NcolsPrev=rawPoly[i-1]->getNofColumns();
		            double** AmatPrev;
		            double* bvectPrev;
		            bvectPrev = new double[NrowsPrev];
		            AmatPrev = new double*[NrowsPrev];
		            for(int j=0;j<NrowsPrev;j++)
		                AmatPrev[j] = new double[NcolsPrev];
		            rawPoly[i-1]->getA(&NrowsPrev,&NcolsPrev,AmatPrev);
		            rawPoly[i-1]->getb(&NrowsPrev,bvectPrev);
			        double** Aint;
                    double* bint;
		            bint = new double[Nrows+NrowsPrev];
	            	Aint = new double*[Nrows+NrowsPrev];
                    for(int j=0;j<Nrows;j++)
			        {
			            Aint[j] = Amat[j];   //I use the memory which has been already allocated
			            bint[j] = bvect[j];
                    }
                    for(int j=Nrows;j<(NrowsPrev+Nrows);j++)
			        {
			            Aint[j] = AmatPrev[j-Nrows];   //I use the memory which has been already allocated
			            bint[j] = bvectPrev[j-Nrows];
                    }
	            	int NrowsIntPrev =Nrows+NrowsPrev;
		            int NcolsIntPrev =Ncols;
			
		            NofVtxIntPrev = 4;
	                verticesIntPrev = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                    verticesIntPrev[j] = new double[NofVtxIntPrev];

                    algGeom->VertexEnumeration(&NrowsIntPrev,&NcolsIntPrev,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntPrev, verticesIntPrev);
			        for(int j=0;j<NofDim;j++)
                       delete[] verticesIntPrev[j];
	                delete[] verticesIntPrev;

	                verticesIntPrev = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                   verticesIntPrev[j] = new double[NofVtxIntPrev];
		
                    algErrIntPrev = algGeom->VertexEnumeration(&NrowsIntPrev,&NcolsIntPrev,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntPrev, verticesIntPrev);
	       
					if(algErrIntPrev != alg_NoError)
					    Log::getStream(1) << " Error while enumerating vertices in the BWD intersection " << endl; 
           
					delete[] bint;       //this uses memory allocated by others. Hence I deallocate it using others
                    delete[] bvectPrev;
                    delete[] bvect;
                    delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
	            	for(int j=0;j<Nrows;j++)
			           delete[] Amat[j];
                    delete[] Amat;
                       for(int j=0;j<NrowsPrev;j++)
                           delete[] AmatPrev[j];
                       delete[] AmatPrev;
	            } //end if (not the first)
	 
	            if(i<m_nPolytopes-2)//expand forward
		        {
                    int Nrows=rawPoly[i]->getNofRows(); 
                    int Ncols=rawPoly[i]->getNofColumns();
		            double** Amat;
		            double* bvect;
		            bvect = new double[Nrows];
		            Amat= new double*[Nrows];
		            for(int j=0;j<Nrows;j++)
		                Amat[j] = new double[Ncols];
        
		            rawPoly[i]->getA(&Nrows,&Ncols,Amat);
		            rawPoly[i]->getb(&Nrows,bvect);
*/
/*  search for the row that defines the border to be exapnaded. 
  *  It is such that Ai*x1=bi, Ai*x2=bi, hence Ai*(x1+x2)-2bi=0, which is  argmax_i Ai(x1+x2)-2*bi		                  
 */
/*                    double myVect[NofDim];
                    myVect[0] = leftBound[i+1].x+rightBound[i+1].x;
                    myVect[1] = leftBound[i+1].y+rightBound[i+1].y;
		            int expIdx=0;
		            double myMax=-1;
			        double expNorm = 0.5*sqrt(pow((leftBound[i].x-leftBound[i+1].x),2)+pow((leftBound[i].y-leftBound[i+1].y),2))+
			                                0.5* sqrt(pow((rightBound[i].x-rightBound[i+1].x),2)+pow((rightBound[i].y-rightBound[i+1].y),2));
			        for(int j=0;j<Nrows;j++)
	                {            
			            double temp = 0;
			            for(int h=0;h<Ncols;h++)
			                temp+=Amat[j][h]*myVect[h];
	            		temp-=2*bvect[j]; 
                        if(temp>=myMax)
			            {
			                myMax=temp;
				            expIdx=j;      //index of the row to be expanded
                        }
		            }
		            double normARow=0;
	            	for(int j=0;j<Ncols;j++) //compute the norm of the row to be expanded to normalize
	                {
                        normARow+=Amat[expIdx][j]*Amat[expIdx][j];				

			        }

		            bvect[expIdx]+=(EXPPERC*expNorm)*normARow;    // expansion
                    int NrowsNext=rawPoly[i+1]->getNofRows(); 
                    int NcolsNext=rawPoly[i+1]->getNofColumns();
		            double** AmatNext;
		            double* bvectNext;
		            bvectNext = new double[NrowsNext];
		            AmatNext= new double*[NrowsNext];
		            for(int j=0;j<NrowsNext;j++)
		                AmatNext[j] = new double[NcolsNext];
		            rawPoly[i+1]->getA(&NrowsNext,&NcolsNext,AmatNext);
		            rawPoly[i+1]->getb(&NrowsNext,bvectNext);
            
			        double** Aint;
                    double* bint;
			        bint = new double[Nrows+NrowsNext];
		            Aint = new double*[Nrows+NrowsNext];
                    for(int j=0;j<Nrows;j++)
			        {
			            Aint[j] = Amat[j];   //I use the memory which has been already allocated
			            bint[j] = bvect[j];
                    }
                    for(int j=Nrows;j<(NrowsNext+Nrows);j++)
			        {
			             Aint[j] = AmatNext[j-Nrows];   //I reuse the memory which has been already allocated
			             bint[j] = bvectNext[j-Nrows];
                    }
			        int NrowsIntNext =Nrows+NrowsNext;
		            int NcolsIntNext=Ncols;
			
			        NofVtxIntNext= 4;
	                verticesIntNext = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                    verticesIntNext[j] = new double[NofVtxIntNext];
                    algGeom->VertexEnumeration(&NrowsIntNext,&NcolsIntNext,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntNext, verticesIntNext);
		            for(int j=0;j<NofDim;j++)
                        delete[] verticesIntNext[j];
	                delete[] verticesIntNext;

	                verticesIntNext = new double*[NofDim];
	                for(int j=0;j<NofDim;j++)
	                    verticesIntNext[j] = new double[NofVtxIntNext];
		
                    algErrIntNext = algGeom->VertexEnumeration(&NrowsIntNext,&NcolsIntNext,(const double**)Aint, (const double*)bint, &NofDim, &NofVtxIntNext, verticesIntNext);
					if(algErrIntNext != alg_NoError)
					    Log::getStream(1) << " Error while enumerating vertices in the FWD intersection " << endl; 
	       
                    delete[] bint;     //this uses memory allocated by others. Hence I deallocate it using others
                    delete[] bvectNext;
                    delete[] bvect;
                    delete[] Aint;         //note the second level pointers point to memory shared with other pointers. I deallocate from those.
		            for(int j=0;j<Nrows;j++)
			            delete[] Amat[j];
                    delete[] Amat;
                    for(int j=0;j<NrowsNext;j++)
                        delete[] AmatNext[j];
                    delete[] AmatNext;

	            } //end if (not the last)

        //building the overlapping polytopes
		        if( (algErrIntNext == alg_NoError) && (algErrIntPrev == alg_NoError))
                {
                    if(i>0)
		                NofVtxFinal+=NofVtxIntPrev;
	                if(i<m_nPolytopes-1)
		                NofVtxFinal+=NofVtxIntNext;
                    verticesFinal = new double*[NofDim];
		            for(int h=0;h<NofDim;h++)
                        verticesFinal[h] = new double[NofVtxFinal];
	                for(int j=0;j<NofVtxFinal;j++)
		            {
                        verticesFinal[0][j]=0.0;
                        verticesFinal[1][j]=0.0;
	                }
	                for(int j=0;j<NofVtx;j++)
	                {
                        verticesFinal[0][j]=vertices[i][0][j];
                        verticesFinal[1][j]=vertices[i][1][j];
		            }
	                if(i>0)
                    {          
		                for(int j=0;j<NofVtxIntPrev;j++)
		                {
                            verticesFinal[0][j+NofVtx]=verticesIntPrev[0][j];
                            verticesFinal[1][j+NofVtx]=verticesIntPrev[1][j];
		                }
		            }
		            if(i<m_nPolytopes-1)
                    {          
		                for(int j=0;j<NofVtxIntNext;j++)
		                {
                            verticesFinal[0][j+NofVtx+NofVtxIntPrev]=verticesIntNext[0][j];
                            verticesFinal[1][j+NofVtx+NofVtxIntPrev]=verticesIntNext[1][j];
		                }
		            }
		            m_polyCorridor[i]  =  new CPolytope( &NofDim, &NofVtxFinal, (const double**)  verticesFinal ); //Ask Melvin: is this Correct?
		            ALGErrorType algErrFinal = algGeom->VertexAndFacetEnumeration(&m_polyCorridor[i]);
			    if ((algErrFinal != alg_NoError) && (algErrFinal != alg_ImproperAdjacency) )
			  {
					    
			    Log::getStream(1) << "error when building overlapping polytopes" << endl;
			    Log::getStream(1) << "alg_ErrFinal " << algErrFinal<<endl;
			    isLegal = false;
                    }
                    for(int j=0;j<NofDim;j++)
                    {  
                        delete[] verticesFinal[j];
                        if(i<m_nPolytopes-2)
			                delete[] verticesIntNext[j];
                        if(i>0)
			                delete[] verticesIntPrev[j];
                    }
			    }

			    i++;
            
			} //end while
*/
            for(int i=0; i<Ngates-1; i++)
	        {
	            for(int j=0; j<NofDim; j++)
	                delete[] vertices[i][j];
	            delete[] vertices[i];
            }
	        delete[] vertices;
            for(int i=0; i<m_nPolytopes; i++)
	        {
	            delete rawPoly[i];
            }
            delete[] rawPoly;
	        delete algGeom;
        } 
        else 
        {
		    isLegal = false;
            Log::getStream(1)<<"CORRIDOR: FAILED - leftBound.size()< 2,rightBound.size()< 2"<<endl; 
        } 
    }
	else
	{ 
		isLegal = false;
        Log::getStream(1)<<"CORRIDOR: FAILED - m_polylines.size()< 2"<<endl; 
    }
	if(isLegal)
        m_polyCorridorLegal = true;
    else
	{   
	    Log::getStream(1) << "CORRIDOR: FAILED - Corridor generation " << endl;
	    m_polyCorridorLegal = false;
    }
}

void Corridor::printPolyCorridor(void)
{
    if(!m_polyCorridorLegal)
	   Log::getStream(1) << "CORRIDOR: FAILED - the current Polytopic Corridor is NOT LEGAL " << endl;
	else
	{
        Log::getStream(1) << "CORRIDOR: Number of Polytopes : " << m_nPolytopes << endl;
        for(int i=0;i<m_nPolytopes;i++)
	       Log::getStream(1) << "Polytope " << i << endl << &m_polyCorridor[i] << endl;
    }
}

void Corridor::printMatlabPolyCorridor(void)
{
    if(!m_polyCorridorLegal)
       Log::getStream(1) << "CORRIDOR: FAILED - the current Polytopic Corridor is NOT LEGAL " << endl;
    else
    {
        for (int i=0; i<m_nPolytopes;i++) {
            double** vertices ;
            int NofDim = 2 ;
            vertices = new double*[NofDim];
            int NofVtx = m_polyCorridor[i].getNofVertices();
            for(int j = 0; j < NofDim ; j++)
                vertices[j] = new double[NofVtx] ;

            m_polyCorridor[i].getVertices(&NofDim, &NofVtx, vertices);

            for(int j = 0; j < NofDim ; j++) {
                printf("P%d_%c = [ ", i, (j==0)?'x':'y');
                for(int k=0; k < NofVtx; k++) {
                    printf("%f ", vertices[j][k]);
                }
                printf("];\n");
            }

            char color;
            if (i % 3 == 0) color = 'g';
            if (i % 2 == 0) color = 'k';
            else color = 'm';
            printf("plot(P%d_x, P%d_y, '%c--'); hold on;\n\n", i, i, color);
        }
    }
}

/*!Function that sends  the polyhedral corridor
 * to reduce the network load we extract the polytopes vertices and we send those.
 * the resulting message is a vector of vector of 2D points. The external vector represents
 * the list of polytopes, which are lists (the internal vectors) of vertices, which are 2-d arrays of double.
 *
 * S Di Cairano Apr-07
 */
void Corridor::sendPolyCorridor(void)
{
    sendCorr corrVertices;
    sendPoly polyVertices;
//    if(!m_polyCorridorLegal)
//	{
//	   Log::getStream(1) << "the current Polytopic Corridor is NOT LEGAL " << endl;
//	   Log::getStream(1) << "SendPolyCorridor ABORTED" << endl;
//	}
//	else
//	{
        for(int i=0;i<m_nPolytopes; i++)
	    {
            double** vertices;
		    int NofDim =  m_polyCorridor[i].getNofDimensions();
		    int NofVtx = m_polyCorridor[i].getNofVertices();
		    vertices = new double*[NofDim];
		    for(int j=0;j<NofDim;j++)
              vertices[j]=new double[NofVtx];
            m_polyCorridor[i].getVertices(&NofDim, &NofVtx, vertices);
		
		    for(int j=0;j<NofVtx;j++)
		    {
		        polyVertices.push_back( sendPoint(vertices[0][j],  vertices[1][j]));
            }
	        corrVertices.push_back(polyVertices);
       
            polyVertices.clear(); //clearing the current polytope
		    for(int j=0;j<NofDim;j++)
                delete[] vertices[j]; //clearing the current vertices
	        delete[] vertices;
	    }
        m_polyCorridorTalker.send(&corrVertices);    
//	}
}


void Corridor::printPolylines() {

  if (m_polylines.size()> 0 ) {
    for (unsigned int i=0; i<m_polylines.size(); i++) {
      for (unsigned int j=0;j<m_polylines[i].size(); j++) {
	      Log::getStream(1) <<"m_polyline["<<i<<"]["<<j<<"]="<< m_polylines[i].arr[j]<<endl;
      }
    }
  } 
}

void Corridor::printMatlabPolylines() {
    if (m_polylines.size()> 0 ) {
        for (unsigned int i=0; i<m_polylines.size(); i++) {
            printf("L%d_x = [ ", i);
            for (unsigned int j=0;j<m_polylines[i].size(); j++) {
                printf("%f ", m_polylines[i].arr[j].x);
            }
            printf(" ];\n");
            printf("L%d_y = [ ", i);
            for (unsigned int j=0;j<m_polylines[i].size(); j++) {
                printf("%f ", m_polylines[i].arr[j].y);
            }
            printf(" ];\n");

            char color;
            if (i % 2 == 0) color = 'r';
            else color = 'b';
            printf("plot(L%d_x, L%d_y, '%c*:'); hold on;\n\n", i, i, color);
        }
    } 
}
