#include "Pause.hh"
#include "Log.hh"

Pause::Pause(int stateId, ControlStateFactory::ControlStateType type)
: ControlState(stateId, type)
, m_desDecc(-0.5)
{

}

Pause::~Pause()
{

}

int Pause::determineCorridor(Corridor & corr, VehicleState vehState, TrafficState * traffState, PlanningHorizon planHoriz, Map * localMap)
{
  Log::getStream(1)<<"PAUSE.determineCorridor - traffState type "<<traffState->toString() << endl;;
  
  int error = 0;
  Log::getStream(1) << "HERE NOW!!" << endl;

  SegGoals currSegment = planHoriz.getSegGoal(0);
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearBumper(vehState);
  
  bool foundTraffState = true; 

  switch (traffState->getType()) {
    
  case TrafficStateFactory::ZONE_REGION:
    {
      error += CorridorUtils::makeCorridorZonePause(corr, vehState);
      error += CorridorUtils::setFinalCondPause(corr, vehState, localMap);    
      //    Log::getStream(1) << "error = " << error << endl;
      // Log::getStream(1) << "setting final conditions" << endl;
      //error += CorridorUtils::setFinalCondStopped(corr, vehState);
      //Log::getStream(1) << "error = " << error << endl;
    }
    break;
    
  case TrafficStateFactory::ROAD_REGION:
  case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
      bool isReverse = false;
      error += CorridorUtils::makeCorridorLane(corr, currFrontPos, localMap, isReverse);
      error += CorridorUtils::setFinalCondPause(corr, vehState, localMap);
    }
    break;
    
  case TrafficStateFactory::INTERSECTION_STOP:
    {
      SegGoals nextSegment = planHoriz.getSegGoal(1);
      error += CorridorUtils::makeCorridorIntersection(corr, currFrontPos, localMap, currSegment);
      error += CorridorUtils::setFinalCondPause(corr, vehState, localMap);
    }
    break;        
  default:    
    {
      cerr << "Pause.cc: Undefined Traffic state" << endl;
      foundTraffState = false;
      error += 1;
      break;
    }
  }

  return error;
}
