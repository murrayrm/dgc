#include "ApproachInterSafeToInterStop.hh"
#include <math.h>
#include "Log.hh"

ApproachInterSafeToInterStop::ApproachInterSafeToInterStop(TrafficState * state1, TrafficState * state2)
: TrafficStateTransition(state1, state2)
, m_distToStop(1)
{

}

ApproachInterSafeToInterStop::~ApproachInterSafeToInterStop()
{

}

double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState)
{
    Log::getStream(1) << "in ApproachInterSafeToInterStop::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
    return 0.0;
}


double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState * trafficState, Map * localMap, VehicleState vehState, PointLabel ptLabel)
{
    Log::getStream(1) << "in ApproachInterSafeToInterStop::meetTransitionConditions()" << endl;
	
    /* double distance = 50; */
    int stopLineErr = -1;
    point2 stopLinePos;
	
    // This is what I need to use
    point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
    point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
    // use getStopline() call that gives next stopline in my lane based on currPos
    Log::getStream(1) << "curr pos = " << currFrontPos << endl;
	
    stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);
	  //  Or should we use: stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
    Log::getStream(1) << "ptLabel = " << ptLabel << endl;
    /* Log::getStream(1) << "stopLinePos = " << stopLinePos << endl; */
	
    double angle;
    localMap->getHeading(angle, ptLabel);
    Log::getStream(1) << "heading = " << angle << endl;
	
    double dotProd = (-stopLinePos.x + currFrontPos.x) * cos(angle) + (-stopLinePos.y + currFrontPos.y) * sin(angle);
    Log::getStream(1) << "in ApproachInterSafeToInterStop: dot product  = " << dotProd << endl;

    double AliceHeading = AliceStateHelper::getHeading(vehState);
    double absDist = stopLinePos.dist(currFrontPos);
    double headingDiff = TrafficUtils::getAngleInRange(angle-AliceHeading);

    if ((dotProd > -m_distToStop) && (fabs(headingDiff)<M_PI/12) && (absDist<10))
        m_trafTransProb = 1;
    else
        m_trafTransProb = 0;
    setUncertainty(m_trafTransProb);
	
    return m_trafTransProb;
}
