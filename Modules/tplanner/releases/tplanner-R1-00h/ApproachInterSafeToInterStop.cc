#include "ApproachInterSafeToInterStop.hh"
#include <math.h>


ApproachInterSafeToInterStop::ApproachInterSafeToInterStop(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distToStop(1)
{
}

ApproachInterSafeToInterStop::~ApproachInterSafeToInterStop()
{

}

double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
  return 0.0;
} 


double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel ptLabel)
{
  cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() " << endl;
  double prob = 0;
  double distance = 0;
  int stopLineErr = -1;
  point2 stopLinePoint;
  point2 currPos =  AliceStateHelper::getPositionFrontBumper(vehState);
  stopLineErr = localMap->getStopline(stopLinePoint,ptLabel);
  distance = stopLinePoint.dist(currPos);
  if (distance <= m_distToStop) {
    m_trafTransProb = 1;	
    prob = m_trafTransProb;
  }
  setUncertainty(prob);
  return prob;
} 
