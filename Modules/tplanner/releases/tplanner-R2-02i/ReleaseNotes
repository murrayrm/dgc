              Release Notes for "tplanner" module

Release R2-02i (Wed May  9 16:50:23 2007):
	Latest stable release with most of the static obstacle handling 
implemented. Uturn for the nominal case needs some more 
debugging/ implementation (when DARPA specifies a uturn, as opposed to a 
uturn due to an obstacle). Lane changing and passing is now implemented. 
Transition from stopped due to an obstacle to passing needs to be 
tested some more. Also, in this release the corridor is sent as convex 
polytopes, in addition to rddf. This functionality needs more testing before 
it will be released as 'stable'. 

Release R2-02h (Fri May  4 10:11:14 2007):
	Merged CSS

Release R2-02g (Mon Apr 30 20:59:50 2007):
	Fixed cost map initialization in TrafficPlanner.cc.  Updated cost
	painting in Corridor.cc to draw cost from general lane bounds. 
	Updated map element talker initialization to work with new map
	version.

Release R2-02f (Mon Apr 30 13:21:06 2007):
	This is a pretty big release of the tplanner. The state machines have been updated. The 
velocity profile specification has been implemented. The corridors are now sent in local 
coordinates. The polytopes representation of the corridor has been implemented. The CSS structure 
has been implemented. ALL OF THIS NEEDS MORE DEBUGGING. This will follow in the next week. Then we 
should be able to deal with static obstacles fairly robustly. Check the ChangeLog for details on 
all the changes.

Release R2-02e (Wed Apr 25 16:40:17 2007):
	Fixed some problems with new release of gcmodule. 

Release R2-02d (Wed Apr 25 14:10:23 2007):
	Initial implementation of transition conditions for the control state machines. Mostly merging with VC's changes.

Release R2-02c (Wed Apr 25 13:22:54 2007):
	Continuing conversion to CSS.  Changed Makefile.yam also to 
compile tplanner as library and two different executables: the CSS 
version of tplanner, and the non CSS version of tplanner.  
 
Release R2-02b (Tue Apr 24 11:56:06 2007):
	Changed the mission complete check so that we can deal with roads that 
loop back on itself (added abs distance and heading conditions). Also had to 
change some transition conditions for the traffic states. 

Release R2-02a (Mon Apr 23 18:41:24 2007):
	Merged Sven's changes. Added internal logging capability. Stepped up 
release number since the previous release was for the CSS (structural change) but 
the release number did not step up.

Release R2-01j (Mon Apr 23 18:07:59 2007):
	Initial conversion into CSS

Release R2-01j-ndutoit (Mon Apr 23 18:30:24 2007):
	Merging in Sven's clean-up of the code. Pretty extensive changes to the states and 
state transition files.

Release R2-01i (Sat Apr 21 19:20:31 2007):
	Updated some of the costviewer files (+ Makefile.yam) to allow to
	compile under OS X.  Basically uses the OpenGL and GLUT frameworks
	instead of the unix library equivalents.  Shouldn't have any effect
	outside of OS X.

Release R2-01i-vcarson (Mon Apr 23 17:57:42 2007):
	Converted to CSS. Needs some debugging still.

Release R2-01h (Thu Apr 12 16:31:57 2007):
	Fixed a small bug with the console.	

Release R2-01g (Wed Apr 11 15:46:06 2007):
	Moved cost map generation to correct place in planning cycle. Also added 'requestFullMap' functionality. This will require some more debugging and testing, but Sam will be working on 
that part, and this is in preparation for him.	

Release R2-01f (Tue Apr 10  8:40:50 2007):
	Added basic console to tplanner. Also implemented some debugging and 
verbose functionality.

Release R2-01e (Mon Mar 19 14:21:14 2007):
	Post field test 2 release of tplanner. Only real change was the eend-of- 
	mission velocity profile specification. Currently the cost map 
	generation and sending thread is not started up. If you need this, 
	umcomment the line in TrafficPlannerMain.cc. 

Release R2-01d (Sun Mar 18  8:53:27 2007):
	Changed switching from uturn state so that we always switch from that state, no
	matter what traffic state we are in. It just checks what the current seggoal is,
	and if it is not uturn, and we have completed the uturn, then we transition.	

Release R2-01c (Sat Mar 17 20:26:48 2007):
	Made tplanner more robust in intersections. Tested with complicated sequence and 
	works for the most part - still some issue with switching out of the uturn state, 
	but I believe that is because we end to close to an intersection in the opposite dir.

Release R2-01b (Sat Mar 17 13:48:46 2007):
	Debugged intersection functions. added mplanner-tplanner comm during uturn. 
	uturn needs more debugging.	

Release R2-01a (Fri Mar 16 12:07:52 2007):
	Traffic planner that uses skynettalker to communite with mission planner.

Release R2-00d-ndutoit (Fri Mar 16 23:02:41 2007):
	Merging with new release of tplanner.

Release R2-00d (Fri Mar 16 11:16:01 2007):
	Added robust goal and traffic state distance checking 
(based on hyper plane now, not just dist). Also adjusted 
stop.cc vel profile. Checked middle of segment (non stop) end 
of mission/checkpt crossing - seems ok now. Tested with new 
rddfplanner (planning to the end pts) and that also seems ok.

Release R2-00c (Thu Mar 15 21:16:01 2007):
		turned on talker to listen to obstacle messages from map.  removed
	hard coded obstacle.

Release R2-00b (Thu Mar 15 14:19:18 2007):
	Created testCState.cc to check one control state (specified) 
at a time. Updated UTurn.cc - now commands a reverse traj, and then 
a fwd traj to test switching of the stack. Removed dependency on 
OCPtSpecs by using new interfaces file (TpDpInterface.hh). 

Release R2-00a (Tue Mar 13 15:19:07 2007):
	Added ocpParams to tplanner, and this struct is now populated in the planner.
	Implemented interface for ocpParams, and did initial testing of send function
	by running testOCPSpecs function (which receives the message).

Release R2-00 (Tue Mar 13  0:18:57 2007):
	Updated state machines to separate transitions and mission complete 
checks. Also, tracked down a bug related to the changed segGoals interface.	

Release R1-00i (Fri Mar  9 12:23:09 2007):
Updated tplanner to include MapElementTalker.hh in the new
	location.  No change in functionality.

Release R1-00h (Fri Mar  9 10:35:21 2007):
	This is the new tplanner!!! The traffic and control state 
machines have been implemented. It uses the new map. The interface 
between mapper and tplanner has NOT been implemented. The interfaces 
with mplanner and dplanner has not changed. I do not use the GloNavMap 
anymore, but just obtain that data from map (which is instantiated from 
an rndf locally and not updated). 
	This version of the tplanner is stable for the following case 
only: drive down a road segment, come to a stop at an intersection, 
continue STRAIGHT through the intersection, and then drive down the road 
again. The files necessary to run this case are included in the release: 
tplanner_unittest.rndf, tplanner_unittest_straight.mdf, rddf.rddf (for 
asim). There are still bugs, including checking for mission complete 
(interaction with traffic state transitions), end of mission handling, 
and some others. 
	This version SHOULD be able to make left and right turns at the 
intersection, but that depends on map capability for the short term. 
Thus it has not been tested, and there are no guarentees. 
	
Release R1-00g (Wed Feb 21 13:13:53 2007):
	Fixed TrafficPlanner.cc so it compiles against revision R2-00b of interfaces.

Release R1-00f (Tue Feb 20 17:25:22 2007):
	Fixed include statements so this version of tplanner compiles against revision R2-00a of interfaces and skynet

Release R1-00e (Fri Feb  2  8:01:13 2007):
  Implemented new mapper->planner interface.  Now stopping at closest detected line.  Fixed broken link in Makefile

Release R1-00d (Wed Jan 31 14:25:50 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00c (Wed Jan 31 12:00:07 2007):
	Fixed tplanner's state interfaces - this change was necessary since the StateClient interface was changed

Release R1-00b (Mon Jan 29  8:10:54 2007):
	Changed trafficUtils.{cc,hh} to TrafficUtils.{cc,hh} so they 
comply with the coding standards.

Release R1-00a (Sun Jan 28 18:01:08 2007):
	Added basic files for tplanner modules. Also modified tplanner to get rid of the startup problem.

Release R1-00 (Sat Jan 27  3:56:30 2007):
	Created.




































