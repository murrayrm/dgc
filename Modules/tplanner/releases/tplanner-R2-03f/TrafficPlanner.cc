/*!TrafficPlanner.cc
 * Author: Noel duToit
 * Last revision: Feb 24 2007
 * */

#include <highgui.h>
#include "TrafficPlanner.hh"
//#include "TrafficStateFactory.hh"
//#include "ControlStateFactory.hh"
#include "ZoneRegion.hh"
#include "Stopped.hh"
#include "Log.hh"
#include <boost/serialization/vector.hpp>

using namespace std;

//#define MAX_DELTA_SIZE 100000

/* We can make nice getters for the following */
TrafficState * CTrafficPlanner::m_currTrafficState;
ControlState * CTrafficPlanner::m_currControlState;
PlanningHorizon CTrafficPlanner::m_currPlanHorizon;
Map * CTrafficPlanner::localMap;

CTrafficPlanner::CTrafficPlanner(CmdLineArgs cLArgs)
  : CSkynetContainer(MODtrafficplanner, cLArgs.sn_key)
  , CStateClient(cLArgs.waitForStateFill)
  , m_OCPparamsTalker(cLArgs.sn_key, SNocpParams, MODtrafficplanner)
  , corridor(cLArgs)
{
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Assign member variables based on cmdline input
  m_snKey = cLArgs.sn_key;
  m_sendCostMapParams = cLArgs.send_costmap;
  m_debug = cLArgs.debug;
  m_verbose = cLArgs.verbose;
  m_use_local = cLArgs.use_local;
  m_log = cLArgs.log;
  m_console = !cLArgs.no_console;

  if (m_console) {
    // initialize the cotk console
    initConsole();
    cotk_update(console);  
    if ((m_verbose)||(m_debug)){    
      cout << "initialized console" << endl;
    }
  }

  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //Mutexes
  // Local Map
  DGCcreateMutex(&m_LocalMapMutex);
  // dplanner status
  //DGCcreateMutex(&m_DPlannerStatusMutex);
  // segment goals
  DGCcreateMutex(&m_SegGoalsMutex);
  //cost map
  DGCcreateMutex(&m_CostMapMutex);

 
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Conditions

  
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Skynet listen sockets
  // dplanner static cost map request
  requestFullStaticCostMapSocket = m_skynet.listen(SNtplannerStaticCostMapRequest, MODdynamicplanner);
    
  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // Initialization
  // Local Map
  initRecvMapElement(m_snKey,1);
  initSendMapElement(m_snKey);
  m_localMap = new Map();
  localMap = new Map();

  
  m_costMap = new CMapPlus();
  m_costMap->initMap(0.0,0.0,NUM_ROWS,NUM_COLS,ROW_RES,COL_RES,0);
  m_costLayerID = m_costMap->addLayer<double>(100.0,-1.0,true);
  m_tempLayerID = m_costMap->addLayer<double>(100.0,-1.0,false);

  //Initialize parameters for painting cost map
  readConfigFile(cLArgs.config);

  // Segment Goals
  if(segGoalsSocket < 0)
    cerr << "TrafficPlanner: skynet listen returned error" << endl;

  // Initialize the control and traffic states for startup
  //m_tfac = TrafficStateFactory(m_debug, m_verbose, m_log);
  m_tfac = TrafficStateFactory(cLArgs);

  //m_cfac = ControlStateFactory(m_debug, m_verbose, m_log);
  m_cfac = ControlStateFactory(cLArgs); // This constructor passes more info about output

  m_trafStateEst = new TrafficStateEstimator(m_debug, m_verbose, m_log);
  m_currTrafficState = m_tfac.getInitialState();
  m_tplannerControl =  new TrafficPlannerControl(m_debug, m_verbose, m_log);
  m_currControlState =  m_cfac.getInitialState();

  if (m_console){
    string* tempString;
    cotk_printf(console, "%planLoopCount%", A_NORMAL, 
                "%d", 0);
    //    cotk_update(console);
    cotk_printf(console, "%seggoal%", A_NORMAL, 
                "%d.%d.%d", 0, 0, 0);
    cotk_printf(console, "%num_seggoal%", A_NORMAL, 
                "%d", 0);
    cotk_printf(console, "%dist_seggoal%", A_NORMAL, 
                "%3.6f", 0.0);
    tempString = m_tfac.printString(m_currTrafficState->getType());
    cotk_printf(console, "%prev_tstate%", A_NORMAL, 
                "%s", tempString->c_str());
    cotk_printf(console, "%curr_tstate%", A_NORMAL, 
                "%s", tempString->c_str());
    //cotk_printf(console, "%prev_tstate%", A_NORMAL, 
    //            "%d", m_currTrafficState->getType());
    //cotk_printf(console, "%curr_tstate%", A_NORMAL, 
    //            "%d", m_currTrafficState->getType());
    cotk_printf(console, "%num_tstate%", A_NORMAL, 
                "%d", 0);
    tempString = m_cfac.printString(m_currControlState->getType());
    cotk_printf(console, "%prev_cstate%", A_NORMAL, 
                "%s", tempString->c_str());
    cotk_printf(console, "%curr_cstate%", A_NORMAL, 
                "%s", tempString->c_str());
    //    cotk_printf(console, "%prev_cstate%", A_NORMAL, 
    //            "%d", m_currControlState->getType());
    //cotk_printf(console, "%curr_cstate%", A_NORMAL, 
    //            "%d", m_currControlState->getType());
    cotk_printf(console, "%num_cstate%", A_NORMAL, 
                "%d", 0);
    cotk_printf(console, "%dist_cstate%", A_NORMAL, 
                "%3.6f", 0.0);
    cotk_printf(console, "%time_cstate%", A_NORMAL, 
                "%f", 0.0 );
    cotk_update(console);
    // HAVE TO REMEMBER TO DELETE THIS POINTER AFTER EVERY CALL!
    delete tempString;
  }
}

CTrafficPlanner::~CTrafficPlanner() 
{
  // delete pointers
  delete m_localMap;
  delete localMap;
  //delete m_dplannerStatus;
  delete m_costMap;
  // delete mutexes
  //DGCdeleteMutex(&m_LocalMapMutex);
  //DGCdeleteMutex(&m_ObstacleMutex);
  //DGCdeleteMutex(&m_DPlannerStatusMutex);
  DGCdeleteMutex(&m_SegGoalsMutex);
  DGCdeleteMutex(&m_CostMapMutex);
  //delete conditions

  delete m_trafStateEst;
  delete m_tplannerControl;

  if (m_console) {
    closeConsole();
  }

}

static char *consoleTemplate =
"tplanner                                                                   \n"
"Skynet Info:   %spread%                                                    \n"
"Planning cycle:  %planLoopCount%                                           \n"
"                                                                           \n"
"Segment Goal:  %seggoal%                                                   \n"
"Segment goal number: %num_seggoal%                                         \n"
"Distance to end of goal: %dist_seggoal%                                    \n"
"Traffic states: 0=RR, 1=ZR, 2=AIS, 3=IS                                    \n"
"Previous traffic state: %prev_tstate%                                      \n"
"Current traffic state: %curr_tstate%                                       \n"
"Traffic state number: %num_tstate%                                         \n"
"Control states: 0=LK, 1=SL, 2=Stop, 3=Stopped, 4=UTurn                     \n"
"Previous control state: %prev_cstate%                                      \n"
"Current control state: %curr_cstate%                                       \n"
"Control state number: %num_cstate%                                         \n"
"Distance into control state: %dist_cstate%                                 \n"
"Time into control state: %time_cstate%                                     \n";
//"                                                                           \n";
//"[%QUIT%|%PAUSE%|%RESET%]                                                   \n"
//"                                                                           \n"


// Initialize console display
int CTrafficPlanner::initConsole()
{
  // Initialize console
  console = cotk_alloc();
  assert(console);

  // Set the console template
  cotk_bind_template(console, ::consoleTemplate);

  // Bind buttons and toggles
  //  cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq",
  //                 (cotk_callback_t) onUserQuit, this);
  //cotk_bind_toggle(console, "%PAUSE%", " PAUSE ", "Pp",
  //                 (cotk_callback_t) onUserPause, this);
  //cotk_bind_button(console, "%RESET%", " RESET ", "Rr",
  //                 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(console,NULL);
  
  // Display some fixed values
  cotk_printf(console, "%spread%", A_NORMAL, 
							"%d", m_snKey);

  return 0;
}

// Finalize sparrow display
int CTrafficPlanner::closeConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}

// Handle button callbacks
//int CTrafficPlanner::onUserQuit(cotk_t *console, CTrafficPlanner *self, const char *token) 
//{
  //TODO: implement this bit
  //MSG("user quit");
  //self->quit = true;
//  return 0;
//}


// Handle button callbacks
//int CTrafficPlanner::onUserPause(cotk_t *console, CTrafficPlanner *self, const char *token)
//{
  //TODO: Implement this bit
  //  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
//  return 0;
//}

// Handle button callbacks
//int CTrafficPlanner::onUserReset(cotk_t *console, CTrafficPlanner *self, const char *token)
//{
  //TODO: implement this bit
  //self->resetMap();
  //MSG("pause %s", (self->pause ? "on" : "off"));
//  return 0;
//}

void CTrafficPlanner::getLocalMapThread()
{

  MapElement recvEl;
  int bytesRecv;
  while (true){
    bytesRecv = recvMapElementBlock(&recvEl,1);
    
    if (bytesRecv>0){
      DGClockMutex(&m_LocalMapMutex);
      m_localMap->addEl(recvEl);
      DGCunlockMutex(&m_LocalMapMutex);
     }else {
      cout << "Error in CTrafficPlanner::getLocalMapThread, received value from recvMapElementBlock = " << bytesRecv << endl;
      usleep(100);
    }
  }
}


void CTrafficPlanner::getDPlannerStatusThread()
{
  // TODO: Update this function
  // The skynet socket for receiving dplanner status
  //  int dplannerStatusSocket = m_skynet.listen(SNdplannerStatus, MODdynamicplanner);
  //  DPlannerStatus* dplannerStatus = new DPlannerStatus();
  //  if(dplannerStatusSocket < 0)
  //    cerr << "TrafficPlanner::getDPlannerStatusThread(): skynet listen returned error" << endl;

  //  while(true)
  //  {
  //    bool dPlannerStatusReceived = RecvDPlannerStatus(dplannerStatusSocket, dplannerStatus);
  //    /* YOU NEED TO FIGURE OUT WHAT TO DO HERE */
  //    if (dPlannerStatusReceived)
  //    {
  //      DGClockMutex(&m_DPlannerStatusMutex);
  //      m_dplannerStatus = dplannerStatus;
  //      DGCunlockMutex(&m_DPlannerStatusMutex);      
  //    }
  //  }
}

void CTrafficPlanner::getSegGoalsThread()
{
  SkynetTalker<SegGoals> segGoalsTalker(m_snKey, SNsegGoals, MODtrafficplanner);
  SegGoals* segGoals = new SegGoals();
  while(true){
    //bool segGoalsReceived = RecvSegGoals(segGoalsSocket, segGoals);
    bool segGoalsReceived = segGoalsTalker.receive(segGoals);
    if (segGoalsReceived){
      DGClockMutex(&m_SegGoalsMutex);
      m_segGoals.push_back(*segGoals);
      DGCunlockMutex(&m_SegGoalsMutex);
      if ((m_verbose)||(m_debug)){
        cout << "seggoal received " << endl;
      }            
    }
  }
}


void CTrafficPlanner::getStaticCostMapRequestThread()
{
  // skynet send socket
  int staticCostMapSocket = m_skynet.get_send_sock(SNtplannerStaticCostMap);
  //note Skynet listen socket is requestFullStaticCostMapSocket

  // not sure what this is for!
  //WaitForNewState();

  bool requestFullMap;
  while(true)
    {
      // listen for requests
      //TODO: figure out what this MAX_DELTA_SIZE is used for, and if it is even necessary.
      int MAX_DELTA_SIZE = 100000;
      int numreceived =  m_skynet.get_msg(requestFullStaticCostMapSocket, &requestFullMap, MAX_DELTA_SIZE, 0);
      
      if (numreceived>0){
        DGClockMutex(&m_CostMapMutex);       
        if (requestFullMap) {
          // send deltas using the SendMapdelta function
          CDeltaList* deltaList = NULL;
          unsigned long long timestamp;
          DGCgettime(timestamp);
          deltaList = m_costMap->serializeFullMapDelta<double>(m_costLayerID,timestamp);
          if (!SendMapdelta(staticCostMapSocket, deltaList)){
            cerr << "TrafficPlanner::getStaticCostMapRequestThread error: map not sent properly" << endl;
          }
          m_costMap->resetDelta<double>(m_costLayerID);
          DGCunlockMutex(&m_CostMapMutex);
          delete deltaList;
        }
      }
      DGCusleep(500000);

    }
}


void CTrafficPlanner::TPlanningLoop(void)
{
  Log log = Log();
  log.init("./log.txt", m_log);

  SkynetTalker<SegGoalsStatus> statusTalker(m_snKey, SNtplannerStatus, MODtrafficplanner);
  if ((m_verbose)||(m_debug)) {
    cout << "Traffic State BEGIN LOOP..." << endl;
  }

  SkynetTalker< BitmapParams > polyTalker(m_snKey, SNbitmapParams, MODtrafficplanner);
  

  // Get send skynet sockets
  int rddfSocket = m_skynet.get_send_sock(SNrddf);
  int staticCostMapSocket = m_skynet.get_send_sock(SNtplannerStaticCostMap);

  // Confirm that we have joined seggoals group
  SegGoalsStatus* segGoalsStatus = new SegGoalsStatus();
  segGoalsStatus->goalID = 0;
  segGoalsStatus->status = SegGoalsStatus::COMPLETED;
  // bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
  bool statusSent = statusTalker.send(segGoalsStatus);
  if (!statusSent)
    {
      cout << "Error sending segGoals status to mplanner" << endl;
      cerr << "TPlanningLoop(): Error sending segGoals status to mplanner" << endl;
    } else if ((m_verbose)||(m_debug)) {
      cout << "Successfully sent initial status" <<  endl;
    }
  
  //TODO: remove later when the entire map is received from maper
  //=====================================================
  UpdateState(); // this gives m_state
  point2 statedelta(m_state.utmNorthing-m_state.localX, m_state.utmEasting-m_state.localY);
  localMap->prior.delta = statedelta;
  DGClockMutex(&m_LocalMapMutex);
  m_localMap->prior.delta = statedelta;         
  DGCunlockMutex(&m_LocalMapMutex);
  //======================================================

  bool outerLoopFlag = true;
  if ((m_verbose) || (m_debug)){
    cout << "Entering outer planning loop..." << endl;
  }
  int innerLoopCounter = 0;
  while(outerLoopFlag)
    {
      DGClockMutex(&m_SegGoalsMutex);
      unsigned numSegGoals = m_segGoals.size();
      DGCunlockMutex(&m_SegGoalsMutex);
    
      //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
      // robust startup
      while (numSegGoals == 0)
        {
          if ((m_verbose) || (m_debug)){
            cout << "Waiting for segment goals" <<  endl;
          }
          sleep(1);
          //segGoalsStatus->goalID = 0;
          //segGoalsStatus->status = SegGoalsStatus::COMPLETED;
          // Request seggoals from mission planner
          //bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
          DGClockMutex(&m_SegGoalsMutex);
          numSegGoals = m_segGoals.size();
          DGCunlockMutex(&m_SegGoalsMutex);
          if ((m_verbose) || (m_debug)){
            cout << "numseggoals = " << numSegGoals << endl;
          }
        }
      //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    
      //Read list of segment goals
      DGClockMutex(&m_SegGoalsMutex);
      list<SegGoals> segGoals = m_segGoals;
      m_segGoals.pop_front(); // discard the first goal
      DGCunlockMutex(&m_SegGoalsMutex);
   
      if (numSegGoals > 0) // DONT START PLANNING BEFORE I HAVE SOME GOALS
        {
          //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
          // INNER LOOP - planning loop
          if ((m_verbose) || (m_debug)){
            cout << "Entering inner planning loop..." << endl;
          }

          while(true) // inner loop
            {  
              innerLoopCounter++;
              if (m_console) {
                cotk_printf(console, "%planLoopCount%", A_NORMAL, 
                            "%d", innerLoopCounter);
              }
              DGClockMutex(&m_LocalMapMutex);
              localMap->prior.delta = m_localMap->prior.delta;
              localMap->data = m_localMap->data;
              DGCunlockMutex(&m_LocalMapMutex);

              DGClockMutex(&m_LocalMapMutex);
              localMap = m_localMap;
              DGCunlockMutex(&m_LocalMapMutex);
              // TODO: use map updated in thread
              // set the global to local transformation
              m_gloToLocalDelta = localMap->prior.delta;

              if ((m_verbose) || (m_debug)){              
                cout << endl;
                cout << endl;
                cout << "updating state" << endl;
              }
              UpdateState(); // this gives m_state
              if ((m_verbose) || (m_debug)){
                cout << "current velocity = " << AliceStateHelper::getVelocityMag(m_state) << endl;
              }

              if (!((segGoals.front().segment_type == SegGoals::END_OF_MISSION))) 
                {
                  // if not and end of MISSION, update traffic and control states
                  // Get waypoint label for exit point from current segment
                  PointLabel exitWayptLabel(segGoals.front().exitSegmentID, segGoals.front().exitLaneID, segGoals.front().exitWaypointID);
                  if (m_console){
                    cotk_printf(console, "%seggoal%", A_NORMAL, 
                                "%d.%d.%d", segGoals.front().exitSegmentID, 
                                segGoals.front().exitLaneID, 
                                segGoals.front().exitWaypointID);
                    cotk_printf(console, "%num_seggoal%", A_NORMAL, 
                                "%d", segGoals.front().goalID);
                  }

                  if ((m_verbose) || (m_debug)){
                    cout<< "Exit waypoint label = " <<exitWayptLabel<<endl;
                    point2 exitWaypt;
                    localMap->getWaypoint(exitWaypt, exitWayptLabel);
                    cout<< "Exit waypoint = " <<   exitWaypt << endl;
                    cout << "Traffic State BEFORE determineTrafficState()..." << endl;
                    m_tfac.print(m_currTrafficState->getType());
                  }
                  if (m_console){
                    string* tempString;
                    tempString = m_tfac.printString(m_currTrafficState->getType());
                    cotk_printf(console, "%prev_tstate%", A_NORMAL, 
                                "%s", tempString->c_str());
                    delete tempString;
                  }

                  m_currTrafficState = m_trafStateEst->determineTrafficState(m_currTrafficState,localMap, m_state, exitWayptLabel);

                  if (m_console){
                    string* tempString;
                    tempString = m_tfac.printString(m_currTrafficState->getType());
                    cotk_printf(console, "%curr_tstate%", A_NORMAL, 
                                "%s", tempString->c_str());
                    delete tempString;
                  }
                  if ((m_verbose) || (m_debug)){
                    cout << "Traffic State AFTER determineTrafficState()" << endl;
                    //    m_tfac.print(m_trafStateEst->getCurrentTrafficStateType());
                    m_tfac.print(m_currTrafficState->getType());
                    cout << "Determine Planning Horizion()" << endl;
                  }

                  m_tplannerControl->determinePlanningHorizon(m_currPlanHorizon,m_currTrafficState, segGoals);
          

                  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                  // run the control loop, which should return the corridor object to be sent to dplanner          
                  // first see if we have completed a mission
                  if ((m_verbose) || (m_debug)){
                    cout<< "Checking if complete , exit waypoint = "<< exitWayptLabel<<endl;
                  }
                  if (isComplete(exitWayptLabel, m_state)){
                    if ((m_verbose) || (m_debug)){
                      cout << "GOAL " << segGoals.front().goalID << " COMPLETED!!!" << endl;
                    }
                    
                    // send status to mplanner
                    segGoalsStatus->status = SegGoalsStatus::COMPLETED;
                    segGoalsStatus->goalID = segGoals.front().goalID;
                    bool statusSent = statusTalker.send(segGoalsStatus);
                    if (statusSent) {
                      if ((m_verbose) || (m_debug)){
                        cout << "Successfully sent status to mplanner" << endl;
                      }
                    }
                    if (m_console) {
                      cotk_update(console);
                    }
                    //if segment is completed
                    break;
                  }
          
                  if ((m_verbose) || (m_debug)){
                    cout << endl << "Control State BEFORE determineControlState()..." << endl;
                    m_cfac.print(m_currControlState->getType());  
		    cout << endl;
                  }
                  if (m_console){
                    string* tempString;
                    tempString = m_cfac.printString(m_currControlState->getType());
                    cotk_printf(console, "%prev_cstate%", A_NORMAL, 
                                "%s", tempString->c_str());
                    delete tempString;
                  }

                  m_currControlState = m_tplannerControl->determineControlState(m_currControlState, m_currTrafficState, m_currPlanHorizon, localMap, m_state);

                  log.logSegment(m_currPlanHorizon);
                  log.logTrafficState(m_currTrafficState, m_currControlState, m_currPlanHorizon, localMap);
                  log.logControlState(m_currControlState);

                  if (m_console){
                    string* tempString;
                    tempString = m_cfac.printString(m_currControlState->getType());
                    cotk_printf(console, "%prev_cstate%", A_NORMAL, 
                                "%s", tempString->c_str());
                    delete tempString;
                  }
                  if ((m_verbose) || (m_debug)){          
                    cout << endl << "Control State AFTER determineControlState()..." << endl;
                    m_cfac.print(m_currControlState->getType());
		    cout << endl;
                    cout << "distance into control state = " << m_currControlState->calcDistFromInitPos(m_state) << endl;
                  }

                  // determine the corridor
                  // initialize the ocpSpecs - initial conditions set to our current pos here. Also, mode initialized as fwd here - need to explicitly set only when going into reverse
                  corridor.initializeOCPparams(m_state, m_currPlanHorizon.getSegGoal(0).minSpeedLimit, m_currPlanHorizon.getSegGoal(0).maxSpeedLimit);

                  if ((m_verbose) || (m_debug)){
                    cout << "determine the corridor" << endl;
                  }

                  // ocpParams finalconditions get set inside this function
                  // also, the mode gets set (if necessary) in this function
                  m_currControlState->resetFailure(); // we have reset the failure (This could be done in each individual state, but is less prone to errors)
                  m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);
                  cout << "Got corridor" << endl;
                  //                  cout << "print polyline corridor after initial corridor generation" << endl;
                  //corridor.printPolylineCorr();

                  // ==============================================================
                  // Exception handling

                  /* if(TrafficStateFactory::ROAD_REGION == m_currTrafficState->getType()){
                    if(ControlStateFactory::STOPPED == m_currControlState->getType()){ */

                  if (m_currControlState->hasFailed()) {
                      // we want to transition into uturn here
                      // step 1: clear m_segGoals
                      DGClockMutex(&m_SegGoalsMutex);
                      m_segGoals.clear();
                      DGCunlockMutex(&m_SegGoalsMutex);

                      // step 2: let mplanner know we have failed
                      if ((m_verbose) || (m_debug)){                      
                        cout << "GOAL " << segGoals.front().goalID << " FAILED!!!" << endl;
                      }
                      // send status to mplanner
                      segGoalsStatus->status = SegGoalsStatus::FAILED;
                      segGoalsStatus->goalID = segGoals.front().goalID;
                      //bool statusSent = SendSegGoalsStatus(segGoalsStatusSocket, segGoalsStatus);
                      bool statusSent = statusTalker.send(segGoalsStatus);
                      if (statusSent)
                        if ((m_verbose) || (m_debug)){
                          cout << "Successfully sent status to mplanner" << endl;
                        }

                      bool newGoal = false;
                      while (!(newGoal)) {
                        if ((m_verbose) || (m_debug)){
                          cout << "waiting for new goal ... " << endl;
                        }
                        sleep(1);
                        DGClockMutex(&m_SegGoalsMutex);
                        if (m_segGoals.size() > 0){
                          newGoal = true;
                        }
                        DGCunlockMutex(&m_SegGoalsMutex);
                      }
                      // we now have a new goal, so we should be able to uturn or pass now
                      if ((m_verbose) || (m_debug)){
                        cout << "received new goal ... conituing" << endl;
                      }
                      break;
                    }
                  /* } */                    

                  // ==============================================================

                  // Set the velocity profile
                  corridor.setVelProfile(localMap, m_state, m_currTrafficState, m_currControlState, m_currPlanHorizon);

                  cout << "print polyline corridor after velocity profile specification" << endl;
                  corridor.printPolylineCorr();
                  

                  if ((m_verbose) || (m_debug)){
                    cout << "About to getRddfCorridor() " << endl;
                  }

                  RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);

                  // Send rddf
                  cout << "about to send rddf" << endl;
                  SendRDDF(rddfSocket,rddf);

		  corridor.generatePolyCorridor();
		  cout << "about to send polytope corridor" << endl;
		  corridor.sendPolyCorridor();


                  //--------------------------------------------------
                  // SET THIS TO "if (1)" TO ENABLE COST PAINTING
                  //--------------------------------------------------
                  if (0){
                    cout << "begin cost map generation" << endl;
                    DGClockMutex(&m_CostMapMutex);       
                    corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state, localMap);      
                    cout << "done getting cost map" << endl;
                    CDeltaList* deltaList = NULL;
                    unsigned long long timestamp;
                    DGCgettime(timestamp);
                    deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
                    cout << "sending cost map" << endl;
                    SendMapdelta(staticCostMapSocket, deltaList);
                    cout << "sent cost map" << endl;
                    m_costMap->resetDelta<double>(m_costLayerID);
                    DGCunlockMutex(&m_CostMapMutex);
                  }

                  //--------------------------------------------------
                  // Nok: SEND COSTMAP PARAMS
                  //--------------------------------------------------
                  if (m_sendCostMapParams){
                    corridor.getBitmapParams(m_bmparams, m_polygonParams, m_state, localMap, m_gloToLocalDelta);      
                    polyTalker.send(&m_bmparams);
                  }


                  // send the parameters
                  if (!m_use_local){
                    corridor.convertOCPtoGlobal(m_gloToLocalDelta);
                  }
                  // corridor.printVelProfile();
                  corridor.adjustFCPosForRearAxle();
                  corridor.printICFC();
                  bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
                  if (!sendOCPparamsErr){
                    cerr << "TrafficPlanner: problem with send OCP parameters" << endl;

                  }

                } else { // now at the end of the mission

                  // do not update the traffic and control states, but use the previous control state to update the corridor
                  m_currControlState->determineCorridor(corridor, m_state, m_currTrafficState, m_currPlanHorizon, localMap);
                  if ((m_verbose) || (m_debug)){
                    cout << "End of mission, bring alice to a complete stop (for now)" << endl;
                  }
                  double FC_velMin =  0;
                  double FC_velMax =  0;
                  corridor.setOCPfinalCondLB(VELOCITY_IDX_C, FC_velMin);
                  corridor.setOCPfinalCondUB(VELOCITY_IDX_C, FC_velMax);
                  corridor.adjustFCPosForRearAxle();
                  corridor.printICFC();
                  // send the parameters
                  bool sendOCPparamsErr = m_OCPparamsTalker.send(&corridor.getOCPparams());
                  if (!sendOCPparamsErr){
                    cerr << "TrafficPlanner: problem with send OCP parameters" << endl;
                  }
                  
                  RDDF* rddf = corridor.getRddfCorridor(m_gloToLocalDelta);

                  corridor.generatePolyCorridor();
                  corridor.printPolyCorridor();
                  corridor.sendPolyCorridor();

                  
                  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                  // Send this corridor to dplanner
                  SendRDDF(rddfSocket,rddf);
                  if ((m_verbose) || (m_debug)){
                    cout << "GOAL " << segGoals.front().goalID << " COMPLETED!!!" << endl;
                  }
                  
                  //////////////////////////
                  // send the cost map to dplanner
                  // TODO: SAM, this is where the 2nd (and last) bit of code for cost map lives
                  if (0) {
                    DGClockMutex(&m_CostMapMutex);       
                    corridor.getCostMap(m_costMap, m_costLayerID, m_tempLayerID,  m_state, localMap);      
                    CDeltaList* deltaList = NULL;
                    unsigned long long timestamp;
                    DGCgettime(timestamp);
                    deltaList = m_costMap->serializeDelta<double>(m_costLayerID,timestamp);
                    SendMapdelta(staticCostMapSocket, deltaList);
                    m_costMap->resetDelta<double>(m_costLayerID);
                    DGCunlockMutex(&m_CostMapMutex);
                  }
                  //////////////////////////

                  //--------------------------------------------------
                  // Nok: SET THIS TO "if (1)" TO ENABLE SENDING COSTMAP PARAMS
                  //--------------------------------------------------
                  if (m_sendCostMapParams){
                    corridor.getBitmapParams(m_bmparams, m_polygonParams, m_state, localMap, m_gloToLocalDelta);      
                    polyTalker.send(&m_bmparams);
                  }

                  
                  // send status to mplanner
                  segGoalsStatus->status = SegGoalsStatus::COMPLETED;
                  segGoalsStatus->goalID = segGoals.front().goalID;
                  
                  bool statusSent = statusTalker.send(segGoalsStatus);
                  if (statusSent)
                    if ((m_verbose) || (m_debug)){
                      cout << "Signalled mplanner that MISSION COMPLETE!!" << endl;
                    }
                  
                  // TODO: what does tplanner do while waiting for a new mission?
                  outerLoopFlag = false; // exit the outer loop
                  break;
                }
              if (m_console) {
                cotk_update(console);
              }
              corridor.clear();
              usleep(100000); 
              sleep(1);
            } //WHILE STAY IN LOOP
      
        } // END: if (numOfSegGoals>0)
      else 
        {
          if ((m_verbose)||(m_debug)){
            cout << "No segment goals yet ...  still waiting." << endl;
            cout << "SHOULD NOT GET HERE!!" << endl;
          }
          outerLoopFlag = true;
        }
      usleep(100000);
      sleep(1);
    }//WHILE STAY IN OUTER LOOP
}




bool CTrafficPlanner::isComplete(PointLabel exitWayptLabel, VehicleState vehState)
{
  double completeDist = 1;
  bool completed = false;
  point2 exitWaypt, currPos;
  localMap->getWaypoint(exitWaypt, exitWayptLabel);
  currPos = AliceStateHelper::getPositionFrontBumper(vehState);
  double angle;
  localMap->getHeading(angle, exitWayptLabel);
  if ((m_verbose)||(m_debug)){
    cout << "heading = " << angle << endl;
  }
  //  double distance = exitWaypt.dist(currPos); 
  //cout << exitWaypt << currPos << distance << endl;
  double dotProd = (-exitWaypt.x+currPos.x)*cos(angle) + (-exitWaypt.y+currPos.y)*sin(angle);

  if ((m_verbose)||(m_debug)){
    cout << "in iscomplete(): dot product  = " << dotProd << endl;
  }

  double AliceHeading = AliceStateHelper::getHeading(vehState);
  double absDist = exitWaypt.dist(currPos);
  double headingDiff = fmod(angle-AliceHeading,M_PI);

  if((dotProd>-completeDist) && (fabs(headingDiff)<M_PI/12) && (absDist<10)) {
    // when we get within 1 m of the hyperplane defined by the exit pt, goal complete
    completed = true;
  }
  else {
    completed = false;
  }
  return completed;
}




void CTrafficPlanner::readConfigFile(char* file)
{
  ifstream infile(file);
  if (infile.fail()) {
    // check with the user on whether we should continue
    cerr << "costmap painter: couldn't open config file '" << file
	 << "'; continue (y/n)?";

    // Anything except y or Y will cause us to abort; otherwise return
    if (tolower(fgetc(stdin)) != 'y') 
      exit(-1);
    else
      return;
  }
  string param;
  while ( !infile.eof() ) {
    infile >> param;
    if ( "resX" == param )
      infile >> m_bmparams.resX;
    else if ( "resY" == param )
      infile >> m_bmparams.resY;
    else if ( "width" == param )
      infile >> m_bmparams.width;
    else if ( "height" == param )
      infile >> m_bmparams.height;
    else if ( "baseVal" == param )
      infile >> m_bmparams.baseVal;
    else if ( "outOfBounds" == param )
      infile >> m_bmparams.outOfBounds;
    else if ( "centerlaneVal" == param )
      infile >> m_polygonParams.centerlaneVal;
    else if ( "obsCost" == param )
      infile >> m_polygonParams.obsCost;
  }
}
