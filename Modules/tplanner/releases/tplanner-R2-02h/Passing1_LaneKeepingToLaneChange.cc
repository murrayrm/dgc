#include "Passing1_LaneKeepingToLaneChange.hh"

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange(ControlState * state1, ControlState * state2)
: ControlStateTransition(state1, state2)
{

}

Passing1_LaneKeepingToLaneChange::Passing1_LaneKeepingToLaneChange()
{

}

Passing1_LaneKeepingToLaneChange::~Passing1_LaneKeepingToLaneChange()
{

}

double Passing1_LaneKeepingToLaneChange::meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState)
{

    if ((m_verbose) || (m_debug)) {
        cout << "in Passing1_LaneKeepingToLaneChange::meetTransitionConditions" << endl;
    }

    m_prob = 0;
    
    switch (trafficState->getType()) {
    
    case TrafficStateFactory::ROAD_REGION:
    case TrafficStateFactory::APPROACH_INTER_SAFETY:
    {
        double delta_p = 30; // 30 meters passing manouver
        SegGoals seg = planHorizon.getSegGoal(0);
        PointLabel exitWayptLabel(seg.exitSegmentID, seg.exitLaneID, seg.exitWaypointID);
        point2 exit;
        if (map->getWaypoint(exit, exitWayptLabel) != 0) {
            m_prob = 0;
            break;
        }
        double distance = AliceStateHelper::getPositionFrontBumper(vehState).dist(exit);
        bool dist_ok = distance > delta_p;

        double delta_o = 10; // the obstacle distance in meters
        double dist2Obs = map->getObstacleDist(AliceStateHelper::getPositionFrontBumper(vehState), 0);
        bool obstacle_present = ((dist2Obs<delta_o)&&(dist2Obs>0));

        string lt, rt;
        LaneLabel label(seg.entrySegmentID, seg.entryLaneID);
        map->getLeftBoundType(lt, label);
        map->getRightBoundType(rt, label);
        bool passing_allowed = seg.illegalPassingAllowed || lt.compare("broken_white")==0 || rt.compare("broken_white")==0;

        if (obstacle_present && dist_ok && passing_allowed) {
            m_prob = 1;
        }
        m_prob=0;
    }
        break;
        
    case TrafficStateFactory::ZONE_REGION:
    case TrafficStateFactory::INTERSECTION_STOP:

        m_prob = 0;
        break;

    default:
    
        m_prob = 0;
        cerr << "Passing1_LaneKeepingToLaneChange.cc: Undefined Traffic state" << endl;
    
    }
    setUncertainty(m_prob);
    
    return m_prob;
}
