#ifndef UTURNTOUTURN_HH_
#define UTURNTOTURN_HH_

#include "ControlState.hh"
#include "ControlStateTransition.hh"
#include "TrafficState.hh"
#include "PlanningHorizon.hh"

class UTurnToUTurn: public ControlStateTransition {

  public:

    UTurnToUTurn(ControlState * state1, ControlState * state2, int stage);
    UTurnToUTurn();
    ~UTurnToUTurn();
    double meetTransitionConditions(ControlState * controlState, TrafficState * trafficState, PlanningHorizon planHorizon, Map * map, VehicleState vehState);

  private:
  
    int m_stage;

};

#endif                          /*UTURNTOLANEKEEPING_HH_ */
