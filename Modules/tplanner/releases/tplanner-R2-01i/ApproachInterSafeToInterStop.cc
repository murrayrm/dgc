#include "ApproachInterSafeToInterStop.hh"
#include <math.h>


ApproachInterSafeToInterStop::ApproachInterSafeToInterStop(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distToStop(1)
{
}

ApproachInterSafeToInterStop::~ApproachInterSafeToInterStop()
{

}

double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  if ((m_verbose) || (m_debug)){
    cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
  }
  return 0.0;
} 


double ApproachInterSafeToInterStop::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel ptLabel)
{
  if ((m_verbose) || (m_debug)){
    cout << "in ApproachInterSafeToInterStop::meetTransitionConditions() " << endl;
  }
  double distance = 50;
  int stopLineErr = -1;
  point2 stopLinePos;

  //==================================================
  // this is what I need to use
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2 currRearPos = AliceStateHelper::getPositionRearAxle(vehState);
  // use getStopline() call that gives next stopline in my lane based on currPos
  if ((m_verbose) || (m_debug)){
    cout << "curr pos = " << currFrontPos << endl;
  }
  //  stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
  //  stopLineErr = localMap->getStopline(stopLinePos, ptLabel);
  stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);
  if ((m_verbose) || (m_debug)){
    cout << "ptLabel = " << ptLabel << endl;
    cout << "stopLinePos = " << stopLinePos << endl;
  }
  double angle;
  localMap->getHeading(angle, ptLabel);
  if ((m_verbose) || (m_debug)){
    cout << "heading = " << angle << endl;
  }
  double dotProd = (-stopLinePos.x+currFrontPos.x)*cos(angle) + (-stopLinePos.y+currFrontPos.y)*sin(angle);
  if ((m_verbose) || (m_debug)){
    cout << "in AIS2IS: dot product  = " << dotProd << endl;
  }

  if (dotProd > -m_distToStop) {
    m_trafTransProb = 1;	
  } else 
    m_trafTransProb = 0;
  setUncertainty(m_trafTransProb);
  return m_trafTransProb;
} 
