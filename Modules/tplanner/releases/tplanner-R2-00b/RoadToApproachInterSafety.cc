#include "RoadToApproachInterSafety.hh"
#include <math.h>


RoadToApproachInterSafety::RoadToApproachInterSafety(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distSafeStop(30)
{
}

RoadToApproachInterSafety::~RoadToApproachInterSafety()
{

}

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
	cout << "in RoadToApproachInterSafety::meetTransitionConditions() SHOULD NOT BE HERE" << endl;
	return 0.0;
} 

double RoadToApproachInterSafety::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState,PointLabel ptLabel)
{
  cout << "in RoadToApproachInterSafety::meetTransitionConditions() " << endl;
  double distance = 0;
  int stopLineErr = -1;
  point2 stopLinePos;
  //	PointLabel label = PointLabel(horiz.getCurrentExitSegment(), horiz.getCurrentExitLane(), horiz.getCurrentExitWaypoint());
  
  point2 currFrontPos =  AliceStateHelper::getPositionFrontBumper(vehState);
  
  //if (localMap->isStop(ptLabel)){
  // use getStopline() call which gives the next stopline in my lane
  //stopLineErr = localMap->getStopline(stopLinePos,currFrontPos);
  //stopLineErr = localMap->getStopline(stopLinePos, ptLabel);
  stopLineErr = localMap->getNextStopline(stopLinePos, ptLabel);
  distance = stopLinePos.dist(currFrontPos);
  cout << "distance to stopline = " << distance << endl;
  if (distance <= m_distSafeStop) {
    m_trafTransProb = 1;	
  } else {
    m_trafTransProb = 0;
  }
  setUncertainty(m_trafTransProb);
  return m_trafTransProb;
} 
