#include "InterStopToRoadRegion.hh"
#include <math.h>


InterStopToRoadRegion::InterStopToRoadRegion(TrafficState* state1, TrafficState* state2) 
: TrafficStateTransition(state1, state2)
, m_distInterExit(1)
{
}

InterStopToRoadRegion::~InterStopToRoadRegion()
{

}

double InterStopToRoadRegion::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState)
{
  cout << "in InterStopToRoadRegion::meetTransitionConditions(), SHOULD NOT BE HERE " << endl;
  return 0.0;
} 


double InterStopToRoadRegion::meetTransitionConditions(TrafficState *trafficState, Map* localMap, VehicleState vehState, PointLabel pointLabel)
{
	cout << "in InterStopToRoadRegion::meetTransitionConditions() " << endl;
	//this is some distance to some waypoint
	double prob = 0;
	double distance = 0;
	point2 wayPtCoord; 
	point2 currPos =  AliceStateHelper::getPositionFrontBumper(vehState);

	localMap->getWaypoint(wayPtCoord,pointLabel);
	distance = wayPtCoord.dist(currPos);
	cout << "distance = " << distance << " m_distInterExit = " << m_distInterExit << endl;
	if (distance <= m_distInterExit) {
	  m_trafTransProb = 1;	
	  prob = m_trafTransProb;
	}
	//cout << "Prob = "<< prob <<endl;
	//	cout << "IN InterStopToRoadRegion.cc SETTING PROB TO 1 NO MATTER WHAT" << endl;
	//prob = 1;
	setUncertainty(prob);
	return prob;
} 

