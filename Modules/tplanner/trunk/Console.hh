#include "Corridor.hh"
#include "TrafficState.hh"
#include "ControlState.hh"
#include "interfaces/VehicleState.h"
#include <ncurses.h>

class Console {

  public:
    static void init();
    static void refresh();
    static void destroy();
 
    static void updateCorridor(Corridor &corr);
    static void updateState(VehicleState &vehState);
    static void updateQueueing(bool queueing);
    static void updateTurning(int direction);
    static void updateTrafficState(TrafficState *state);
    static void updateControlState(ControlState *state);
    static void addMessage(char *format, ...);

  private:
    static WINDOW *fsm_win;
    static WINDOW *corr_win;
    static WINDOW *state_win;
    static WINDOW *msg_win;
    static char messages[6][77];
    static bool initialized;
};
