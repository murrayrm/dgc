
/*
 * $Id: directpercept.cc
 * Author: Jeremy Ma (adapted from source code provided by Directed Perception)

 This driver communicates with the PTU via serial interface

*/

#ifndef DIRECTPERCEPT_DRIVER_H
#define DIRECTPERCEPT_DRIVER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include "ptu.h"

#define DIRECTPERCEPT_DEFAULT_PORT "/dev/ttyS0"

#define DP_MAXPAN                         180   //deg //this is due to the physical length of cable
#define DP_MINPAN                        -180   //deg //this is due to the physical length of cable
#define DP_MAXTILT                        45    //deg
#define DP_MINTILT                       -45    //deg

#define DP_MAXSPEED                        40   //deg/s

#define DP_PAN_THRESH                      0.1
#define DP_TILT_THRESH                     0.1

#define DP_PAN_ACCEL                       25 // deg/s/s
#define DP_TILT_ACCEL                      25 // deg/s/s

#define DP_PAN_ID                          0
#define DP_TILT_ID                         1
#define DP_ALL_ID                          2

#define DP_BASE_TO_TILTAXIS               0.09 // about 9cm

// degree/radian conversions
#ifndef USEFUL_CONVERSIONS
#define RTOD(r)   ((r) * 180 / M_PI)
#define DTOR(d)   ((d) * M_PI / 180)
#define NORMALIZE(z)   atan2(sin(z), cos(z))
#endif

// Error handling 
#ifndef USEFUL_MACROS
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#endif 

struct DPCommand
{
  float pan;
  float tilt;
  float panspeed;
  float tiltspeed;
};

class DirectPercept
{

private:

  portstream_fd m_ptu;
  float m_ptu_pan_res;
  float m_ptu_tilt_res;
  float m_currpan, m_currtilt;
  float m_panspeed, m_tiltspeed;
  
public:    
  // Constructor
  DirectPercept(int panSpeed, int tiltSpeed);

  // Constructor -- default
  DirectPercept();

  // Destructor
  ~DirectPercept();
  
  // higher-level methods for common use
  int GetPanTiltPos(float* pan, float* tilt);
  int GetPanTiltSpeed(float* panspeed, float* tiltspeed);
  int SetPanPos(float currpan, float desiredpan);
  int SetTiltPos(float currtilt, float desiredtilt);
  int SetPanSpeed(float currpanspeed, float desiredpanspeed);
  int SetTiltSpeed(float currtiltspeed, float desiredtiltspeed);
  int Home();
  int Halt();
  int Reset();

  // MessageHandler
  int ProcessMessage(DPCommand command); 
  int Setup(char* serialPort);
  int Shutdown();  
};

#endif
