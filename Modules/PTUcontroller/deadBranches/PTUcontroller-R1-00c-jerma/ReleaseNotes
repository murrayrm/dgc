              Release Notes for "PTUcontroller" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "PTUcontroller" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "PTUcontroller" module can be found in
the ChangeLog file.

Release R1-00c-jerma (Wed Jul 18 23:48:21 2007):
	Added simulation capabilities for the PTU. Simply use the 
--sim option and the PTU will simulate pan and tilt angles in response 
to  any given command. This makes development a lot easier, not having 
to sit inside alice everytime.

Release R1-00c (Thu Jun 14 15:13:32 2007):
	Let me preface these release note with the following statement: 
I checked with Sam and Nok and both confirmed that it was OK to make a
release now and the code freeze is (for the most part) lifted. If there 
is any problem with this release, they take full responsibility (Nok 
first, then Sam). 

	Modified the PTU driver and feeder such that you can now specify 
the speed of each motor individually for either of the two types of 
commands (i.e. line-of-site or raw). So, for example, if you desire to 
move the pan-tilt unit from one pose to another, yet desire to move 
faster in the panning and slower in the tilting, you can do that by 
simply specifying a faster/slower pan/tilt speed (respectively) in your 
command structure. If you specify a command of 0 speed (or a speed that's way 
too fast or even negative) it will be ignored and the previously set 
speed is kept. 

Release R1-00b (Thu Jun  7 10:34:40 2007):
	In this release, I've re-written the driver to handle CAN 
interface coomunications which has a much higher baudrate (500kbps) and 
thus won't be the bottle-neck for data-transfer (in the sense of frame 
transformations). I've still kept the original working release of the 
old driver/feeder in a subfolder "rs232". 

Also in this release, I've added functionality for the any user desiring 
to send a command to the PTU to specify that command in one of two ways:
1) a line-of-site pose in local coordinates
2) raw pan-tilt commands in the PTU ref frame

If a type (1) command is sent, the PTU will work out the appropriate 
inverse kinematics such that the line-of-site (the x-axis of the tool 
frame) is intersecting with that point. 

If a type (2) command is sent, the PTU will basically move to that 
pan-angle and tilt-angle in the PTU-ref-frame.

Release R1-00a (Sat May 26 16:07:37 2007):
	Adding appropriate files to this module, namely: 
1) a working driver that interfaces via RS-232 connection at 38400bps
2) a working low-level feeder that checks the PTU-state health, listens 
for incoming motion commands and executes upon receipt, and lastly 
broadcasts the currrent pan, angle states, and appropriate 
transformations.
3) a test script (send_test.cc) that can be used to interact with the 
feeder and see on the cotk display what commands are being executed to
the PTU

I've modeled the PTU as some kind of mix between a sensor/feeder and a 
controller. It's a sensor/feeder in the sense that it broadcasts it's 
measured pan-tilt state angles; yet it's a controller in the sense that 
it accepts commands of where to point to next. So in that regard, the 
sending of  ptu-state messages are done using Sensnet while the 
receiving of the commands is done through skynet. 

Release R1-00 (Mon May 21 12:50:28 2007):
	Created.



