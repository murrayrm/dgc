              Release Notes for "PTUcontroller" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "PTUcontroller" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "PTUcontroller" module can be found in
the ChangeLog file.

Release R1-00y-jerma (Sat Oct 20  2:09:49 2007):
Field commits; longer usleep time; 

Release R1-00y (Fri Oct 19  2:32:53 2007):
Increasing the acceleration to 20 deg/s/s (was 15 deg/s/s before). If this causes irradic 
beahvior during the tomorrow's testing (which I highly doubt) please let me know asap. 

Release R1-00x (Wed Oct 17 22:39:40 2007):
Appears that a usleep(0) may have solved the problem (fingers crossed!)

Release R1-00w (Tue Oct 16 23:40:38 2007):
Still trying to chase down these canRead() and canSend() error messages that are causing the PTU to hang in some 
situations; hopefully this cleanup of the driver code will do it. 

Release R1-00v (Sat Oct 13  0:23:17 2007):
Fixed problem with the canRead and canSend commands in the amtecpowercube driver. This was most 
likely what was causing the ptufeeder to just lock in one motor control mode. 

Release R1-00u (Thu Oct 11 19:22:09 2007):
While waiting at Ford dealership, the following changes were made:
1) Changed the swivel mode for panning in tilting; what used to be a 
   STEP command is now a RAMP command; much smoother motion
2) the PTU sends an interpolated state to help with latency issues. 
3) reduced the max/min pan angles (was potentially sending the PTU
   into a locked error state. 

Release R1-00t (Fri Oct  5 13:30:56 2007):
	Changed the internal error checking; now if the internal state 
of either pan or tilt module goes bad, a reset() is issued; if that 
fails, a shutdown() is issued on both modules and at that point 
processControl should take over and restart the process.

Release R1-00s (Sun Sep 30 14:17:04 2007):
	When running ptuladar internal to the feeder, the ladarRangeBLob 
now has fields to include what the state of the PTU was when that blob 
was taken.

Release R1-00r (Sat Sep 29 15:54:51 2007):
	Added functionality for the PTU-ladar to run internal to the 
PTUfeeder. Simply use the --use-ladar option and it will run the 
ladarfeeder internal. I also physically removed the ladar cable from 
the DM500 so it's no longer accessible by IP address on blade16. This 
was to reduce as much as possible all sources of latencies that could 
exist between PTU and ladar. Eventually I'll move the radars internal 
but for now this works for ladar only. 

Logging capabilities also work; simply use the --enable-ladar-log and 
that'll do it. Using the --enable-log only logs the PTU state data.

This release does not yet have functionality to update the ladar blob 
with a flag on whether the ptu was moving or not when the data was 
taken. That functionality will exist in the next release which should 
happen tonight.

Release R1-00q (Thu Sep 27  0:18:38 2007):
	Added capability for more than one CAN device to exist on the 
same machine. Now the canOpen function will only open a can handle for a 
device that matches the serial number stored in the CFG file. This is 
all to start moving towards having an all-in-one 
ptufeeder/ladarfeeder/radarfeeder to address those nasty time delays. 

Release R1-00p (Sat Sep 15  7:11:31 2007):
field commits; i removed unnecessary CAN handles. 
Originally I had four separate handles (one send 
and one receive for each rotation module -- 
pan/tilt). After reading through the documentation 
a bit more, there's no need to have a CAN handle for 
receiving; that can be done through the transmit 
handle. minor fix.

Release R1-00o (Wed Sep 12  1:43:28 2007):
Making some valgrind fixes. nothing major here.

Release R1-00n (Tue Sep 11 19:07:45 2007):
	Cleaned up the cotk display and added a usleep(0) to reduce the rate at which PTU states are sent; it was 140Hz before (yikes!) but 
it's now around 60Hz. 

Release R1-00m (Tue Sep  4  5:19:40 2007):
	Removing '?' from last line in SENSNET_MF_PTU.CFG.

Release R1-00l (Tue Sep  4  4:02:41 2007):
	Two changes: modified tool2ptu transform matrix to be a rotation matrix only; the position of the PTU in SENSNET_MF_PTU.CFG is now the center of the pan and tilt axes of the PTU. 
This removes hidden BASE_TILTAXIS variables and such, but should be entirely compatible with all existing modules. Also, used ladar-calib to align PTU config to ground plane.

Release R1-00k (Tue Aug 28 14:05:23 2007):
	Fixed a bug that allowed the PTU to potentially move to a desired pan/tilt 
position in 0 seconds (resulting action would be *THUNK*). This should fix that so it 
doesn't happen anymore.

Release R1-00j (Thu Aug 23 19:46:54 2007):
	Added higher resolution to simulation of the PTU.

Release R1-00i (Tue Aug 21 16:04:30 2007):
	Changed config file to reflect new position, now with 
(hopefully?) accurate pitch & yaw values. Also changed the way raw 
commands are accepted to match the way the state is sent, and set 
sensor-id to SENSNET_MF_PTU by default.

Release R1-00h (Wed Aug 15 22:40:33 2007):
	adding additional option to send_test...mainly for my own use in
	filtering out groundstrikes

Release R1-00g (Wed Aug 15  4:03:08 2007):
	Changed sensor position based on field tweaks so that it lined 
up with other sensors/ground

Release R1-00f (Thu Aug  9  0:16:03 2007):
	Changed config file to reflect new position on alice 
(removed a 1 foot tall mount for PTU, otherwise the same)

Release R1-00e (Tue Aug  7  0:05:13 2007):
	Added a small script to the send_test.cc file that will pitch 
the ladar on the PTU to three specified tilt angles at 10 second 
intervals. This is to help with assessing the best location/orientation 
for a sweeping ladar and also to help collect ladar data for road/curb 
detection at El Toro.

Release R1-00d (Wed Jul 18 23:48:21 2007):
	Added simulation capabilities for the PTU. Simply use the 
--sim option and the PTU will simulate pan and tilt angles in response 
to  any given command. This makes development a lot easier, not having 
to sit inside alice everytime.

Release R1-00c (Thu Jun 14 15:13:32 2007):
	Let me preface these release note with the following statement: 
I checked with Sam and Nok and both confirmed that it was OK to make a
release now and the code freeze is (for the most part) lifted. If there 
is any problem with this release, they take full responsibility (Nok 
first, then Sam). 

	Modified the PTU driver and feeder such that you can now specify 
the speed of each motor individually for either of the two types of 
commands (i.e. line-of-site or raw). So, for example, if you desire to 
move the pan-tilt unit from one pose to another, yet desire to move 
faster in the panning and slower in the tilting, you can do that by 
simply specifying a faster/slower pan/tilt speed (respectively) in your 
command structure. If you specify a command of 0 speed (or a speed that's way 
too fast or even negative) it will be ignored and the previously set 
speed is kept. 

Release R1-00b (Thu Jun  7 10:34:40 2007):
	In this release, I've re-written the driver to handle CAN 
interface coomunications which has a much higher baudrate (500kbps) and 
thus won't be the bottle-neck for data-transfer (in the sense of frame 
transformations). I've still kept the original working release of the 
old driver/feeder in a subfolder "rs232". 

Also in this release, I've added functionality for the any user desiring 
to send a command to the PTU to specify that command in one of two ways:
1) a line-of-site pose in local coordinates
2) raw pan-tilt commands in the PTU ref frame

If a type (1) command is sent, the PTU will work out the appropriate 
inverse kinematics such that the line-of-site (the x-axis of the tool 
frame) is intersecting with that point. 

If a type (2) command is sent, the PTU will basically move to that 
pan-angle and tilt-angle in the PTU-ref-frame.

Release R1-00a (Sat May 26 16:07:37 2007):
	Adding appropriate files to this module, namely: 
1) a working driver that interfaces via RS-232 connection at 38400bps
2) a working low-level feeder that checks the PTU-state health, listens 
for incoming motion commands and executes upon receipt, and lastly 
broadcasts the currrent pan, angle states, and appropriate 
transformations.
3) a test script (send_test.cc) that can be used to interact with the 
feeder and see on the cotk display what commands are being executed to
the PTU

I've modeled the PTU as some kind of mix between a sensor/feeder and a 
controller. It's a sensor/feeder in the sense that it broadcasts it's 
measured pan-tilt state angles; yet it's a controller in the sense that 
it accepts commands of where to point to next. So in that regard, the 
sending of  ptu-state messages are done using Sensnet while the 
receiving of the commands is done through skynet. 

Release R1-00 (Mon May 21 12:50:28 2007):
	Created.

























