
/*
 * $Id: amtecpowercube.cc,v 1.17.2.2 2006/09/25 15:54:58 gerkey Exp $
 * Author: Brian Gerkey
 * Modified: Jeremy Ma 2007/05/18 for DGC07
 */
/*
The amtecpowercube driver controls the Amtec PowerCube Wrist,
a powerful pan-tilt unit that can, for example, carry a SICK laser

This driver communicates with the PowerCube via RS232, and does NOT handle
the newer CAN-based units.  Please submit a patch to support the CAN
protocol.

The amtecpowercube driver supports both position and velocity control.  
For constant swiveling, the PowerCube works better under velocity control.

Note that this driver is relatively new and not thoroughly tested. 

- port (string)
  - Default: "/dev/ttyUSB0"
  - Serial port where the unit is attached.
- home (integer)
  - Default: 0
  - Whether to home (i.e., reset to the zero position) the unit before
    commanding it
- speed (angle)
  - Default: 15 deg/sec
  - Maximum pan/tilt speed 
- blocking (bool)
  - Default: true
  - Command blocking enabled or not; i.e. a command must be executed to 
    completion before another command can be executed
*/

#include "amtecpowercube.hh"

// ===============================
// Constructor for Pan Tilt Unit
// ===============================
AmtecPowerCube::AmtecPowerCube(const char *port, int speedLimit, bool blocking) 
{
  fd = -1;

  this->serial_port = port;

  if(speedLimit > MAXSPEED || speedLimit < 0)
    this->speed = 15; //15deg/s just seems to be a safe speed
  else
    this->speed = speedLimit;

  this->cmd_blocking = blocking;
  this->minpan = MINPAN;
  this->maxpan = MAXPAN;
  this->mintilt = MINTILT;
  this->maxtilt = MAXTILT;

  return;
}


// ===============================
// Default Constructor for Pan Tilt Unit
// ===============================
AmtecPowerCube::AmtecPowerCube() 
{
  fd = -1;

  this->serial_port = "/dev/ttyUSB0";
  this->speed = 15; //deg/s
  this->cmd_blocking = true;
  this->minpan = MINPAN;
  this->maxpan = MAXPAN;
  this->mintilt = MINTILT;
  this->maxtilt =MAXTILT;
  return;

}


// ===============================
// Destructor for Pan Tilt Unit
// ===============================
AmtecPowerCube::~AmtecPowerCube() 
{  
  return;
}


// ===============================
// Reset (Clear Error State)
// ===============================
int AmtecPowerCube::Reset()
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[1];

  cmd[0] = AMTEC_CMD_RESET;
  if(SendCommand(AMTEC_MODULE_PAN,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));
  
  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));
  
  if(SendCommand(AMTEC_MODULE_TILT,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  return(0);
}

// ===============================
// Begin homing procedure back to initial pose
// ===============================
int AmtecPowerCube::Home()
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[1];
  unsigned int state;
  int status;

  cmd[0] = AMTEC_CMD_HOME;

  MSG("sending home command");
  if(SendCommand(AMTEC_MODULE_PAN,cmd,1) < 0)
    return(ERROR("SendCommand() for pan failed"));

  MSG("reading home response");
  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  // poll the device state, wait for homing to finish
  for(;;)
  {
    usleep(AMTEC_SLEEP_TIME_USEC);

    status = CheckCubeState(AMTEC_MODULE_PAN, &state);
    if(status==0)
      break;
    else if(status==-1)
      return(-1);
    
  }

  if(SendCommand(AMTEC_MODULE_TILT,cmd,1) < 0)   
    return(ERROR("SendCommand() for tilt failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  // poll the device state, wait for homing to finish
  for(;;)
  {
    usleep(AMTEC_SLEEP_TIME_USEC);

    status = CheckCubeState(AMTEC_MODULE_TILT, &state);    
    if(status==0)
      break;
    else if(status==-1)
      return(-1);
  }

  if( GetPanTiltPos(&this->lastpan, &this->lasttilt) < 0 )
    WARN("Could not get pan-tilt position");

  if( GetPanTiltVel(&this->lastpanspeed, &this->lasttiltspeed) < 0 )
    WARN("Could not get pan-tilt velocity");
  

  return(0);
}


// ===============================
// Halt (Stop Immediately)
// ===============================
int AmtecPowerCube::Halt()
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[1];

  cmd[0] = AMTEC_CMD_HALT;
  if(SendCommand(AMTEC_MODULE_PAN,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  if(SendCommand(AMTEC_MODULE_TILT,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  return(0);
}


// ===============================
// Setup (Connect to serial port)
// ===============================
int AmtecPowerCube::Setup()
{
  struct termios term;

  // default to position control
  this->controlmode = POS_CONTROL;

  MSG("Amtec PowerCube connection initializing on %s ", serial_port);
  fflush(stdout);

  MSG("opening serial port ...");
  // open it.  non-blocking at first, in case there's no ptz unit.
  if((fd = open(serial_port, O_RDWR | O_SYNC , S_IRUSR | S_IWUSR )) < 0 )
    return(ERROR("open() failed: %s", strerror(errno)));
 
  if(tcflush(fd, TCIFLUSH ) < 0 )
    {
      close(fd);
      fd = -1;
      return(ERROR("tcflush() failed: %s", strerror(errno)));
    }

  MSG("getting port attributes ...");
  if(tcgetattr(fd, &term) < 0 )
    {
      close(fd);
      fd = -1;
      return(ERROR("tcgetattr() failed: %s", strerror(errno)));
    }
  

  MSG("setting term speed to 38400");
  cfmakeraw(&term);
  cfsetispeed(&term, B38400);
  cfsetospeed(&term, B38400);
  //cfsetispeed(&term, B9600);
  //cfsetospeed(&term, B9600);
  
  MSG("resetting port attributes ...");
  if(tcsetattr(fd, TCSAFLUSH, &term) < 0 )
  {
    close(fd);
    fd = -1;
    return(ERROR("tcsetattr() failed: %s", strerror(errno)));
  }

  fd_blocking = false;

  MSG("Commanding Reset() of PTU ...");
  if(Reset() < 0)
  {
    close(fd);
    fd = -1;
    return(ERROR("Reset() failed; bailing."));
  }

  MSG("homing the unit now");
  if(Home() < 0)
  {
    close(fd);
    fd = -1;
    return(ERROR("Home() failed; bailing."));
  }

  return(0);
}


// ===============================
// Shutdown (Close all connections)
// ===============================
int AmtecPowerCube::Shutdown()
{
  if(fd == -1)
    return(0);

  // stop the unit
  if(Halt())
    WARN("Halt() failed.");

  // maybe return it to home
  if(Home())
    WARN("Home() failed.");

  if(close(fd))
    WARN("close() failed:%s",strerror(errno));

  fd = -1;
  puts("Amtec PowerCube has been shutdown");
  return(0);
}


// NOTE: these conversion methods only work on little-endian machines
// (the Amtec protocol also uses little-endian).

// ===============================
// Convert butes to ints and floats and vise versa
// ===============================
float AmtecPowerCube::BytesToFloat(unsigned char *bytes)
{
  float f;
  memcpy((void*)&f, bytes, 4);
  return(f);
}
unsigned int AmtecPowerCube::BytesToUint32(unsigned char* bytes)
{
  unsigned int i;
  memcpy((void*)&i, bytes, 4);
  return(i);
}
void AmtecPowerCube::FloatToBytes(unsigned char *bytes, float f)
{
  memcpy(bytes, (void*)&f, 4);
}
void AmtecPowerCube::Uint16ToBytes(unsigned char *bytes, unsigned short s)
{
  memcpy(bytes, (void*)&s, 2);
}



// ===============================
// Send commands to the PTU
// ===============================
int AmtecPowerCube::SendCommand(int id, unsigned char* cmd, size_t len)
{
  size_t i;
  int ctr, add;
  unsigned char rcmd[AMTEC_MAX_CMDSIZE];
  unsigned char bcc;
  unsigned char umnr;
  unsigned char lmnr;

  add  = 0;
  lmnr = id & 7;
  lmnr = lmnr << 5;
  umnr = id >> 3;
  umnr = umnr | 4;
  for (i=0;i<len;i++) {
    if ( (cmd[i]==0x02) ||
	 (cmd[i]==0x03) ||
	 (cmd[i]==0x10) ) {
      add++;
    }
  }
  lmnr = lmnr + len;
  rcmd[0] = AMTEC_STX;
  rcmd[1] = umnr;
  rcmd[2] = lmnr;
  ctr = 3;
  for (i=0;i<len;i++) {
    switch(cmd[i]) {
    case 0x02:
      rcmd[ctr] = 0x10;
      rcmd[++ctr] = 0x82;
      break;
    case 0x03:
      rcmd[ctr] = 0x10;
      rcmd[++ctr] = 0x83;
      break;
    case 0x10:
      rcmd[ctr] = 0x10;
      rcmd[++ctr] = 0x90;
      break;
    default:
      rcmd[ctr] = cmd[i];
    }
    ctr++;
  }
  bcc = id;
  for (i=0;i<len;i++) {
    bcc += cmd[i];
  }
  bcc = bcc + (bcc>>8);
  switch(bcc) {
  case 0x02:
    rcmd[ctr++] = 0x10;
    rcmd[ctr++] = 0x82;
    break;
  case 0x03:
    rcmd[ctr++] = 0x10;
    rcmd[ctr++] = 0x83;
    break;
  case 0x10:
    rcmd[ctr++] = 0x10;
    rcmd[ctr++] = 0x90;
    break;
  default:
    rcmd[ctr++] = bcc;
  }
  rcmd[ctr++] = AMTEC_ETX;

  if(WriteData(rcmd, ctr) == ctr)
    return(0);
  else
    return(ERROR("short write"));

}


// ===============================
// Write Data to buffer
// ===============================
int AmtecPowerCube::WriteData(unsigned char *buf, size_t len)
{
  size_t written = 0;
  int tmp = 0;

  while(written < len)
  {
    if((tmp = write(fd, buf, len)) < 0)
      return(ERROR("write() failed: %s", strerror(errno)));

    written += tmp;
  }
  return(written);
}



// ===============================
// Await end of text
// ===============================
int AmtecPowerCube::AwaitETX(unsigned char* buf, size_t len)
{
  int pos, loop, numread, totalnumread;
  pos = 0; loop = 0;
  while(loop<10)
  {
    if((numread = read(fd,buf+pos,len-pos)) < 0)
      return(ERROR("read() failed:%s", strerror(errno)));
    else if(!numread)
    {
      if(!fd_blocking)
        usleep(10000);
      loop++;
    }
    else
    {
      if(buf[pos+numread-1]==AMTEC_ETX)
      {
	totalnumread = pos+numread-1;
	return(totalnumread);
      }
      pos += numread;
    }
  }
  return(ERROR("never found ETX"));
}



// ===============================
// Await answer
// ===============================
int AmtecPowerCube::AwaitAnswer(unsigned char* buf, size_t len)
{
  int numread;

  // if we're not blocking, give the unit some time to respond
  if(!fd_blocking)
    usleep(AMTEC_SLEEP_TIME_USEC);

  for(;;)
  {
    if((numread = read(fd, buf, 1)) < 0)
      return(ERROR("read() failed:%s", strerror(errno)));
    else if(!numread)
    {
      // hmm...we were expecting something, yet we read
      // zero bytes. some glitch.  drain input, and return
      // zero.  we'll get a message next time through.
      WARN("read 0 bytes");
      if(tcflush(fd, TCIFLUSH ) < 0 )
        return(ERROR("tcflush() failed:%s",strerror(errno)));
      
      return(0);
    }
    else
    {
      if(buf[0]==AMTEC_STX) 
        return(AwaitETX(buf,len));
      else
        continue;
    }
  }
}



// ===============================
// Convert Buffer
// ===============================
size_t AmtecPowerCube::ConvertBuffer(unsigned char* buf, size_t len)
{
  size_t i, j, actual_len;

  actual_len = len;

  for (i=0;i<len;i++) 
  {
    if(buf[i]==AMTEC_DLE) 
    {
      switch(buf[i+1]) 
      {
        case 0x82:
          buf[i] = 0x02;
          for(j=i+2;j<len;j++) 
            buf[j-1] = buf[j];
          actual_len--;
          break;
        case 0x83:
          buf[i] = 0x03;
          for(j=i+2;j<len;j++) 
            buf[j-1] = buf[j];
          actual_len--;
          break;
        case 0x90:
          buf[i] = 0x10;
          for(j=i+2;j<len;j++) 
            buf[j-1] = buf[j];
          actual_len--;
          break;
      }
    }
  }
  return(actual_len);
}


// ===============================
// Read Answer response from PTU
// ===============================
int AmtecPowerCube::ReadAnswer(unsigned char* buf, size_t len)
{
  int actual_len;

  if((actual_len = AwaitAnswer(buf, len)) <= 0)
    return(actual_len);
  else
    return((int)ConvertBuffer(buf, (size_t)actual_len));
}


// ===============================
// Get Float Parameter
// ===============================
int AmtecPowerCube::GetFloatParam(int id, int param, float* val)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  memset(buf, 0, sizeof(buf)/sizeof(buf[0]));
  unsigned char cmd[2];

  cmd[0] = AMTEC_CMD_GET_EXT;
  cmd[1] = param;

  if(SendCommand(id, cmd, 2) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));
  
  /*
  MSG("this is the return from GetPanTiltPos(): ");
  for(int i=0; i<8; i++)
    {
      MSG("buf[%d]: 0x%x", i,buf[i]);
    }
  */

  *val = BytesToFloat(buf+4); //need the +4 because the header is 4 bytes long
 
  return(0);
}


// ===============================
// Get unsigned int parameter
// ===============================
int AmtecPowerCube::GetUint32Param(int id, int param, unsigned int* val)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[2];

  cmd[0] = AMTEC_CMD_GET_EXT;
  cmd[1] = param;

  if(SendCommand(id, cmd, 2) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  *val = BytesToUint32(buf+4);
  return(0);
}


// ===============================
// Set Float Parameter
// ===============================
int AmtecPowerCube::SetFloatParam(int id, int param, float val)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[6];

  cmd[0] = AMTEC_CMD_SET_EXT;
  cmd[1] = param;
  FloatToBytes(cmd+2, val);

  if(SendCommand(id, cmd, 6) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(ERROR("ReadAnswer() failed"));

  return(0);
}


// ===============================
// Get PTU pose
// ===============================
int AmtecPowerCube::GetPanTiltPos(float* pan, float* tilt)
{
  float tmp;

  // get the pan
  if(GetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_ACT_POS, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  // reverse pan angle, to increase ccw, then normalize
  *pan = -RTOD(NORMALIZE(tmp));
  
  // get the tilt
  if(GetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_ACT_POS, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  
  *tilt = RTOD(tmp);

  return(0);
}


// ===============================
// Get PTU Velocity
// ===============================
int AmtecPowerCube::GetPanTiltVel(float* panspeed, float* tiltspeed)
{
  float tmp;

  // get the pan
  if(GetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_ACT_VEL, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  
  // reverse pan angle, to increase ccw, then normalize
  *panspeed = -RTOD(NORMALIZE(tmp));
  
  // get the tilt
  if(GetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_ACT_VEL, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  
  *tiltspeed = RTOD(tmp);

  return(0);
}


// ===============================
// Set PTU Limits
// ===============================
int AmtecPowerCube::SetLimits()
{
  // have to reverse the signs for the pan limits, since the Amtec unit
  // counts up clockwise, rather than ccw.
  if(this->minpan != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_MAX_POS, 
                     NORMALIZE(DTOR(-this->minpan))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  if(this->maxpan != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_MIN_POS, 
                     NORMALIZE(DTOR(-this->maxpan))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  if(this->mintilt != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_MIN_POS, 
                     NORMALIZE(DTOR(this->mintilt))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  if(this->maxtilt != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_MAX_POS, 
                     NORMALIZE(DTOR(this->maxtilt))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  return(0);
}


// ===============================
// Set Pan Position (specify in degrees)
// ===============================
int AmtecPowerCube::SetPanPos(float oldpan, float pan)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[8];
  float newpan;
  unsigned short time;

  newpan = DTOR(pan);
  time = (unsigned short)rint(((double)fabs(pan - oldpan) / 
                               (double)this->speed) * 1e3);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FSTEP_ACK;
  FloatToBytes(cmd+2,newpan);
  Uint16ToBytes(cmd+6,time);
  if(SendCommand(AMTEC_MODULE_PAN,cmd,8) < 0)
    return(-1);
  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(-1);
  
  return(0);
}


// ===============================
// Set Tilt Position (specify in degrees)
// ===============================
int AmtecPowerCube::SetTiltPos(float oldtilt, float tilt)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[8];
  float newtilt;
  unsigned short time;

  newtilt = DTOR(tilt);
  time = (unsigned short)rint(((double)fabs(tilt - oldtilt) / 
                               (double)this->speed) * 1e3);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FSTEP_ACK;
  FloatToBytes(cmd+2,newtilt);
  Uint16ToBytes(cmd+6,time);
  if(SendCommand(AMTEC_MODULE_TILT,cmd,8) < 0)
    return(-1);
  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(-1);

  return(0);
}


// ===============================
// Set Pan Velocity (specify in deg/s)
// ===============================
int AmtecPowerCube::SetPanVel(float panspeed)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[6];
  float newpanspeed;

  newpanspeed = DTOR(panspeed);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FVEL_ACK;
  FloatToBytes(cmd+2,newpanspeed);
  if(SendCommand(AMTEC_MODULE_PAN,cmd,6) < 0)
    return(-1);
  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(-1);
  return(0);
}


// ===============================
// Set Tilt Position (specify in deg/s)
// ===============================
int AmtecPowerCube::SetTiltVel(float tiltspeed)
{
  unsigned char buf[AMTEC_MAX_CMDSIZE];
  unsigned char cmd[6];
  float newtiltspeed;

  newtiltspeed = DTOR(tiltspeed);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FVEL_ACK;
  FloatToBytes(cmd+2,newtiltspeed);
  if(SendCommand(AMTEC_MODULE_TILT,cmd,6) < 0)
    return(-1);
  if(ReadAnswer(buf,sizeof(buf)) < 0)
    return(-1);
  return(0);
}



// ===============================
// Check the state of the power cube
// ===============================
int AmtecPowerCube::CheckCubeState(int id, unsigned int *state)
{
  // get the module state (for debugging and warning)
  if(GetUint32Param(id, AMTEC_PARAM_CUBESTATE, state) < 0)
    return(ERROR("GetUint32Param() failed; bailing"));

  if(*state & AMTEC_STATE_HOME_OK)
    return(0);

  if(*state & AMTEC_STATE_ERROR)      
    return(ERROR("the Amtec unit has encountered an error and will need\n"
	    "    to be reset; bailing.   Current module state: 0x%x", 
		 *state));    
  else if(*state & AMTEC_POWERFAULT)      
    return(ERROR("The Amtec unit has encountered an error. Powerfault encountered. \n"
	    "The module needs to be switched off to reset encountered error. Current module state: 0x%x", 
		 *state));    
  else if(*state & AMTEC_TOW_ERROR)      
    return(ERROR("The Amtec unit has encountered an error. Servo loop unable to follow target position within given limits. \n"
	    "Check if module was overloaded. Current module state: 0x%x", 
		 *state)); 
  else if(*state & AMTEC_STATE_CPU_OVERLOAD)      
    return(ERROR("The Amtec unit has encountered an error. Communication breakdown between CPU and current controller. \n"
	    "Power must be switched off. Please consult your service partner. Current module state: 0x%x", 
		 *state));
  else if(*state & AMTEC_STATE_BEYOND_HARD)      
    return(ERROR("The Amtec unit has encountered an error. State has reached a hard limit. Emergency stop has been executed automatically. \n"
	    "To remove module from this position, see 'PowerCube Operation System: Disorder'. Current module state: 0x%x", 
		 *state));
  else if(*state & AMTEC_STATE_POW_SETUP_ERR)      
    return(ERROR("The Amtec unit has encountered an error. Error in initializing the current controller with power setup. \n"
	    "Power must be switched off. Please consult your service partner. Current module state: 0x%x", 
		 *state));
  return(1);
}

// ===============================
// Process Message commands
// ===============================
int AmtecPowerCube::ProcessMessage(Command command)
{
  float newpan, newtilt;  
  float newpanspeed, newtiltspeed;
  unsigned int panstate, tiltstate;
  unsigned int panMotionState = 0;
  unsigned int tiltMotionState = 0;
  int status;
  
  if(this->controlmode == POS_CONTROL)
    {
      // watch the hard limits of panning
      if( command.pan > this->maxpan )
	{
	  MSG("\n requested pan angle exceeds hard limit! \n sending maxpan instead: %d", this->maxpan);
	  command.pan = this->maxpan;
	}
      if(command.pan < this->minpan )
	{
	  MSG("\n requested pan angle exceeds hard limit! \n sending minpan instead: %d", this->minpan);
	  command.pan = this->minpan;
	}

      // watch the hard limits of tilting
      if(command.tilt > this->maxtilt)
	{
	  MSG("\n requested tilt angle exceeds hard limit! \n sending maxtilt instead: %d", this->maxtilt);
	  command.tilt = this->maxtilt;
	}
      if(command.tilt < this->mintilt)
	{
	  MSG("\n requested tilt angle exceeds hard limit! \n sending mintilt instead: %d", this->mintilt);
	  command.tilt = this->mintilt;
	}

      newpan =  command.pan; //need the minus for right sign convention.
      newtilt = command.tilt;
      
      if(newpan != lastpan)
	{
	  // send new pan position
	  if(SetPanPos(lastpan,newpan))
	    return(ERROR("SetPan() failed(); bailing."));
	  
	  lastpan = newpan;
	}
      
      if(newtilt != lasttilt)
	{
	  // send new tilt position
	  if(SetTiltPos(lasttilt,newtilt))
	    return(ERROR("SetTilt() failed(); bailing."));
	  
	  lasttilt = newtilt;
	}

      // if cmd_blocking enabled, poll the device state, wait for cmd to finish
      if(this->cmd_blocking)
	{
	  for(;;)
	    {
	      usleep(AMTEC_SLEEP_TIME_USEC);
	      
	      status = CheckCubeState(AMTEC_MODULE_TILT, &panstate);
	      status = CheckCubeState(AMTEC_MODULE_PAN, &tiltstate);
	      panMotionState = panstate & 0x800;
	      tiltMotionState = tiltstate & 0x800;
	      if( (panMotionState == 0x800) || (tiltMotionState == 0x800) )
		continue;	  	  
	      else
		break;	  
	    }
	}


    }
  else if(this->controlmode == VEL_CONTROL)
    {
      // reverse pan angle, to increase ccw
      newpanspeed =  command.panspeed;
      newtiltspeed = command.tiltspeed;
      
      if(newpanspeed != lastpanspeed)
	{
	  // send new pan speed
	  if(SetPanVel(newpanspeed))
	    return(ERROR("SetPanVel() failed(); bailing."));
	  
	  lastpanspeed = newpanspeed;
	}
      
      if(newtiltspeed != lasttiltspeed)
	{
	  // send new tilt position
	  if(SetTiltVel(newtiltspeed))
	    return(ERROR("SetTiltVel() failed(); bailing."));
	  
	  lasttiltspeed = newtiltspeed;
	}
    }
  else
    return(ERROR("unkown control mode: %d; bailing",this->controlmode));

  return 0;
 
}

#if AMTEC_DRIVER_TEST

int main(int argc, char *argv[])
{

  float currpan, currtilt;
  float currpanspeed, currtiltspeed;
  const char *port;
  int home;
  int speed;
  int val;
  bool blocking;
  unsigned int state;
  Command command;

  if(argc<4)
    {
      WARN("\n usage: ./amtectest <DEVICE> <SPEEDLIMIT (deg)> <BLOCKING>\n     (e.g. ./amtectest /dev/ttyUSB0 1 15 1");
      WARN("\n using default values: \n     <DEVICE> = /dev/ttyUSB0   <SPEEDLIMIT> = 15   <BLOCKING> = 1");      
      port = "/dev/ttyUSB0";
      home = 1;
      speed = 15;
      blocking = true;
    }
  else
    {
      if(strncmp(argv[1],"/dev",4)==0)
	port = argv[1];
      else
	{
	  WARN("invalid DEVICE; using '/dev/ttyUSB0' ");
	  port = "/dev/ttyUSB0";
	}

      speed = atoi(argv[2]);

      if(strncmp(argv[3],"1",1)==0 || strncmp(argv[3],"0",1)==0)	
	{
	  val = atoi(argv[3]);
	  if(val==1)
	    blocking = true;
	  else
	    blocking = false;
	}
      else
	{
	  WARN("invalid <BLOCKING>; using '1' ");
	  blocking = true;
	}

    }

  AmtecPowerCube amtecptu(port, speed, blocking);

  printf("Setting up the PTU ... \n");

  if(amtecptu.Setup()<0)
    return(ERROR("Could not establish connection. Abort."));

  printf("Connection established!! \n");

  if(amtecptu.GetPanTiltPos(&currpan, &currtilt)<0)
    return(ERROR("Could not get pan-tilt position"));

  if(amtecptu.GetPanTiltVel(&currpanspeed, &currtiltspeed)<0)
    return(ERROR("Could not get pan-tilt velocity"));

  printf("--- STATUS ---\n");
  printf("currpan: %f  currtilt: %f\n", currpan, currtilt);
  printf("currpanspeed: %f  currtiltspeed: %f\n", currpanspeed, currtiltspeed);


  if(amtecptu.CheckCubeState(AMTEC_MODULE_PAN, &state) < 0)
    {
      WARN("Bad cube state! exiting now.");
      if(amtecptu.Shutdown() < 0)
	WARN("exiting un-cleanly...");
    }

  if(amtecptu.CheckCubeState(AMTEC_MODULE_TILT, &state) < 0)
    {
      WARN("Bad cube state! exiting now.");
      if(amtecptu.Shutdown() < 0)
	WARN("exiting un-cleanly...");
    }


  printf("starting sleep\n");
  sleep(3);
  printf("sleep over.\n");

  command.pan = -30;
  command.tilt = -30;
  amtecptu.ProcessMessage(command);

  printf("--- STATUS ---\n");
  for(int i=0; i<100; i++)
    {
      if(amtecptu.GetPanTiltPos(&currpan, &currtilt)<0)
	WARN("Could not get pan-tilt position");
      if(amtecptu.GetPanTiltVel(&currpanspeed, &currtiltspeed)<0)
	WARN("Could not get pan-tilt velocity");

      printf("currpan: %f  currtilt: %f\n", currpan, currtilt);
      printf("currpanspeed: %f  currtiltspeed: %f\n", currpanspeed, currtiltspeed);

    }

  printf("shutdown now!\n");
  if(amtecptu.Shutdown() < 0)
    WARN("exiting un-cleanly...");  
  
  delete amtecptu; 

  return(0);

}

#endif
