#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <interfaces/PTUCommand.h>

using namespace std;

int main(int argc, char* argv[])
{
 
  int ptuCommandSocket;
  int sendSuccess;
  float pan, tilt;
  PTUCommand ptuCommand;
 
  skynet m_skynet = skynet(217, atoi(getenv("SKYNET_KEY")), NULL); 
  ptuCommandSocket = m_skynet.get_send_sock(SNptuCommand);
  if(ptuCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }

  while(true)
    {
      cout << "Enter commanded pan and tilt angle (deg): <PAN> <TILT> " << endl;
      cin >> pan >> tilt ;
      ptuCommand.pan = pan;
      ptuCommand.tilt = tilt;
      ptuCommand.panspeed = 0;
      ptuCommand.tiltspeed = 0;
      sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
    }
      
  return(0);
}
