Mon Nov 12  2:59:40 2007	Jeremy Ma (jerma)

	* version R1-01b-field
	BUGS:  
	FILES: PTUfeeder.cc(46877), cmdline.c(46877), cmdline.ggo(46877),
		cmdline.h(46877)
	field fixes; pan and tilt faster; runs nominally at 60Hz now.

	FILES: PTUfeeder.cc(46967), amtecpowercube.cc(46967),
		amtecpowercube.hh(46967), cmdline.c(46967),
		cmdline.ggo(46967), cmdline.h(46967)
	added watchdog and more health checks.

	FILES: SENSNET_LF_PTU.CFG(46904)
	new config file 

	FILES: SENSNET_LF_PTU.CFG(47024)
	modified cfg file for more accurate placement of radar tracks.

Thu Oct 25  4:10:40 2007	Jeremy Ma (jerma)

	* version R1-01b
	BUGS:  
	FILES: PTUfeeder.cc(46549), SENSNET_LF_PTU.CFG(46549),
		directpercept.cc(46549), directpercept.hh(46549)
	field changes.

Mon Oct 22 20:40:53 2007	Jeremy Ma (jerma)

	* version R1-01a
	BUGS:  
	FILES: PTUfeeder.cc(46203), SENSNET_LF_PTU.CFG(46203),
		directpercept.cc(46203)
	added some normalizing functions, since before I was getting angles
	outside the range of 0 and 2PI. Minor changes here and there for
	the new PTU.

Sat Oct 20 22:44:54 2007	Jeremy Ma (jerma)

	* version R1-01
	BUGS:  
	New files: SENSNET_LF_PTU.CFG directpercept.cc directpercept.hh
		linuxser.c linuxser.h opcodes.h ptu.c ptu.h
	FILES: Makefile.yam(45946), PTUfeeder.cc(45946),
		amtecpowercube.cc(45946), amtecpowercube.hh(45946),
		send_test.cc(45946)
	changes to account for the new PTU

Sat Oct 20  2:09:46 2007	Jeremy Ma (jerma)

	* version R1-00z
	BUGS:  
	FILES: PTUfeeder.cc(45625)
	field commits.

Fri Oct 19  2:32:50 2007	Jeremy Ma (jerma)

	* version R1-00y
	BUGS:  
	FILES: amtecpowercube.hh(45511)
	increasing acceleration to 20 deg/s/s. 

Wed Oct 17 22:39:35 2007	Jeremy Ma (jerma)

	* version R1-00x
	BUGS:  
	FILES: PTUfeeder.cc(45232), amtecpowercube.cc(45232)
	field commits; seems like all it needed was a usleep(0) in the
	loop.

Tue Oct 16 23:40:33 2007	Jeremy Ma (jerma)

	* version R1-00w
	BUGS:  
	FILES: PTUfeeder.cc(45119), amtecpowercube.cc(45119),
		amtecpowercube.hh(45119)
	cleaned up driver code; hopefully this will track down the PTU
	problems seen in the field today

Sat Oct 13  0:23:13 2007	Jeremy Ma (jerma)

	* version R1-00v
	BUGS:  
	FILES: PTUfeeder.cc(44330), amtecpowercube.cc(44330)
	field commits; fixed how the canRead and canSend commands were
	being executed in the amtecpowercube driver.

Thu Oct 11 19:22:03 2007	Jeremy Ma (jerma)

	* version R1-00u
	BUGS:  
	FILES: PTUfeeder.cc(44189), amtecpowercube.cc(44189),
		amtecpowercube.hh(44189)
	changed the swivel motion from STEP to RAMP; the PTU now moves in a
	continuous motion, not so jerky anymore. Also added some better
	error handling and reduced the MAX pan angle to 140. 

Fri Oct  5 13:30:45 2007	Jeremy Ma (jerma)

	* version R1-00t
	BUGS:  
	FILES: PTUfeeder.cc(43016), amtecpowercube.cc(43016),
		amtecpowercube.hh(43016)
	fixed error handling; now when the tilt module gets a bad state
	flag, it issues a reset() command on that module to clear error
	states rather than just going into shutdown() mode. if the reset
	fails, then it goes into shutdown mode, at which point
	processControl should take care of restarting that module

Sun Sep 30 14:17:00 2007	Jeremy Ma (jerma)

	* version R1-00s
	BUGS:  
	FILES: PTULadarFeeder.cc(41852)
	added functionality for reporting in the ptuFlag of the ladarblob
	whether the PTU was moving or not at time of blob capture

	FILES: PTULadarFeeder.cc(41872)
	rather than using a flag, we'll just send the ptu state data
	altogether

Sat Sep 29 15:54:42 2007	Jeremy Ma (jerma)

	* version R1-00r
	BUGS:  
	New files: PTULadarFeeder.cc PTULadarFeeder.hh
		SENSNET_PTU_LADAR.CFG sick_driver.c sick_driver.h
	FILES: Makefile.yam(41124), amtecpowercube.hh(41124),
		cmdline.c(41124), cmdline.ggo(41124), cmdline.h(41124)
	added ladarfeeder functionality to run the PTUladar in a separate
	thread

	FILES: Makefile.yam(41561), PTUfeeder.cc(41561)
	added and tested functionality of having ladarfeeder run internal
	to PTUfeeder

	FILES: PTUfeeder.cc(41128)
	forgot to commit the correct version of this file earlier

Thu Sep 27  0:18:27 2007	Jeremy Ma (jerma)

	* version R1-00q
	BUGS:  
	FILES: PTUfeeder.cc(40590), SENSNET_MF_PTU.CFG(40590),
		amtecpowercube.cc(40590), amtecpowercube.hh(40590),
		cmdline.c(40590), cmdline.ggo(40590), cmdline.h(40590)
	adding capibility for running multiple CAN devices on the same
	machine. The CFG file now specifies a CAN serial number and will
	look to connect to that CAN device only. 

Sat Sep 15  7:11:27 2007	Jeremy Ma (jerma)

	* version R1-00p
	BUGS:  
	FILES: amtecpowercube.cc(38930), amtecpowercube.hh(38930)
	field commits; i removed unnecessary CAN handles. Originally I had
	four separate handles (one send and one receive for each rotation
	module -- pan/tilt). After reading through the documentation a bit
	more, there's no need to have a CAN handle for receiving; that can
	be done through the transmit handle. minor fix. 

Wed Sep 12  1:43:17 2007	Jeremy Ma (jerma)

	* version R1-00o
	BUGS:  
	FILES: PTUfeeder.cc(38484), amtecpowercube.cc(38484),
		amtecpowercube.hh(38484)
	made some valgrind fixes to code; nothing too major here.

Tue Sep 11 19:07:42 2007	Jeremy Ma (jerma)

	* version R1-00n
	BUGS:  
	FILES: PTUfeeder.cc(38390)
	cleaned up the cotk display and reduced the rate at which state
	updates are sent (was 140Hz before; now it's more like 60Hz);

Tue Sep  4  5:19:37 2007	Tamas Szalay (tamas)

	* version R1-00m
	BUGS:  
	FILES: SENSNET_MF_PTU.CFG(37508)
	Stray character in CFG file.

Tue Sep  4  4:02:36 2007	Tamas Szalay (tamas)

	* version R1-00l
	BUGS:  
	FILES: PTUfeeder.cc(37265)
	Changed PTU transform to remove fixed (hidden) base to tiltaxis and
	tiltaxis to tool offsets. Position of PTU in config file is now
	simply the position of the center of the tilt axis.

	FILES: SENSNET_MF_PTU.CFG(37479)
	Used ladar-calib to align PTU.

Tue Aug 28 14:05:11 2007	Jeremy Ma (jerma)

	* version R1-00k
	BUGS:  
	FILES: PTUfeeder.cc(35944), SENSNET_MF_PTU.CFG(35944),
		amtecpowercube.cc(35944), amtecpowercube.hh(35944),
		cmdline.c(35944), cmdline.ggo(35944), cmdline.h(35944)
	fixed a bug in the driver that allowed for the PTU to swivel to a
	commanded pan/tilt angle in 0 seconds!!! (resulting action would be
	a *THUNK*). It should no longer do this.

Thu Aug 23 19:46:51 2007	Jeremy Ma (jerma)

	* version R1-00j
	BUGS:  
	FILES: PTUfeeder.cc(35348)
	added higher resolution to ptu simulator.

Tue Aug 21 16:04:19 2007	Tamas Szalay (tamas)

	* version R1-00i
	BUGS:  
	FILES: PTUfeeder.cc(34801), SENSNET_MF_PTU.CFG(34801),
		amtecpowercube.hh(34801), cmdline.c(34801),
		cmdline.ggo(34801), cmdline.h(34801)
	Uses --sensor-id=SENSNET_MF_PTU by default, config file set to use
	extending mount, changed maximum/minimum tilt values to avoid
	damage, and now offsets raw commands to account for initial
	pan/tilt values.

	FILES: SENSNET_MF_PTU.CFG(34802)
	Tweaked config file some more

Wed Aug 15 22:40:26 2007	Laura Lindzey (lindzey)

	* version R1-00h
	BUGS:  
	FILES: Makefile.yam(33814), send_test.cc(33814)
	adding additional option to send_test...mainly for my own use in
	filtering out groundstrikes

Wed Aug 15  4:03:05 2007	Tamas Szalay (tamas)

	* version R1-00g
	BUGS:  
	FILES: SENSNET_MF_PTU.CFG(33701)
	Some more tweaks to the config file, this time to match real-world
	data.

Thu Aug  9  0:16:00 2007	Tamas Szalay (tamas)

	* version R1-00f
	BUGS:  
	FILES: SENSNET_MF_PTU.CFG(32714)
	Updated config file to reflect new position of PTU

Tue Aug  7  0:05:10 2007	Jeremy Ma (jerma)

	* version R1-00e
	BUGS:  
	FILES: send_test.cc(32465)
	adding option to send_test.cc that will move ladar to various tilt
	angles to help asses the best configuration for a sweeping ladar.

Wed Jul 18 23:48:15 2007	Jeremy Ma (jerma)

	* version R1-00d
	BUGS:  
	FILES: PTUfeeder.cc(28277), cmdline.c(28277), cmdline.ggo(28277),
		cmdline.h(28277)
	added simulation option to the PTUfeeder. 

	FILES: PTUfeeder.cc(29588)
	minor modifications for simulation purposes.

	FILES: cmdline.c(29589), cmdline.ggo(29589), cmdline.h(29589)
	dumb bug; int to string error.

Thu Jun 14 15:13:18 2007	Jeremy Ma (jerma)

	* version R1-00c
	BUGS:  
	FILES: PTUfeeder.cc(27883), SENSNET_MF_PTU.CFG(27883),
		amtecpowercube.cc(27883), amtecpowercube.hh(27883),
		cmdline.c(27883), cmdline.ggo(27883), cmdline.h(27883),
		send_test.cc(27883)
	added changes to allow speed control as well. 

	FILES: PTUfeeder.cc(27948), SENSNET_MF_PTU.CFG(27948),
		amtecpowercube.cc(27948), amtecpowercube.hh(27948),
		send_test.cc(27948)
	modified changes so that now you can adjust the speed at which you
	get to your desired pan-tilt angles. There was a slight bug before
	with a negative sign but I fixed that too.

	FILES: SENSNET_MF_PTU.CFG(27955), amtecpowercube.cc(27955),
		amtecpowercube.hh(27955)
	some clean up before the commit; also changed the initial pose of
	the PTU base in the configuration file. 

	FILES: amtecpowercube.cc(27884)
	modified some debug messages.

	FILES: send_test.cc(27861)
	adding a little demo option for the send_test script to demo at
	site visit.

Thu Jun  7 10:34:22 2007	Jeremy Ma (jerma)

	* version R1-00b
	BUGS:  
	New files: rs232/PTUfeeder.cc rs232/amtecpowercube.cc
		rs232/amtecpowercube.hh
	FILES: Makefile.yam(27072), PTUfeeder.cc(27072),
		SENSNET_MF_PTU.CFG(27072), amtecpowercube.cc(27072),
		amtecpowercube.hh(27072)
	changed the driver of the PTU to handle a CAN interface as opposed
	to the rs232 version I wrote last week. *much* faster now. 

	FILES: PTUfeeder.cc(26154), SENSNET_MF_PTU.CFG(26154),
		amtecpowercube.cc(26154), amtecpowercube.hh(26154),
		send_test.cc(26154)
	changed the type of commands that can be sent to the ptu. What used
	to be raw pan and tilt angles has now been changed so that you can
	specify a point in local coordinates and the line-of-site of the
	pan-tilt-unit will point to that location. 

	FILES: PTUfeeder.cc(26396), send_test.cc(26396)
	changed to account for two types of PTU commands: "line-of-site
	point" or "raw pan-tilt". The former works by simply specifying a
	local position point and the PTU will move such that the
	line-of-site points there. The latter works by simply sending the
	raw pan-tilt commands to the PTU. 

	FILES: PTUfeeder.cc(26674), amtecpowercube.cc(26674)
	small change to destructor.

Sat May 26 16:07:33 2007	Jeremy Ma (jerma)

	* version R1-00a
	BUGS:  
	New files: PTUfeeder.cc SENSNET_MF_PTU.CFG amtecpowercube.cc
		amtecpowercube.hh cmdline.c cmdline.ggo cmdline.h
		send_test.cc
	FILES: Makefile.yam(24272)
	fixed some warnings output at compilation.

	FILES: Makefile.yam(25091)
	this is a huge commit: added the driver code, the low-level feeder
	that listens/executes commands and broadcasts pan-tilt state, and
	corresponding header files. All have been tested to work on the
	PTU.  I also made a small test program called send_test.cc that can
	be ran to test sending commands to the PTU. Works like a charm. 

Mon May 21 12:50:28 2007	Jeremy Ma (jerma)

	* version R1-00
	Created PTUcontroller module.





























