
/*
 * $Id: amtecpowercube.cc
 * Author: Jeremy Ma (adapted from Brian Gerkey's driver for RS232)
 */
/*
The amtecpowercube driver controls the Amtec PowerCube Wrist,
a powerful pan-tilt unit that can, for example, carry a SICK laser

This driver communicates with the PowerCube via CAN interface,

The amtecpowercube driver supports both position and velocity control.  
For constant swiveling, the PowerCube works better under velocity control.

*/

#include "amtecpowercube.hh"

// ===============================
// Constructor for Pan Tilt Unit
// ===============================
AmtecPowerCube::AmtecPowerCube(int speedLimit, bool blocking) 
{
  memset(this, 0, sizeof(this));

  MSG("You're requested speed is way too fast (or invalid). Using 15mdeg/s instead");
  if(speedLimit > MAXSPEED || speedLimit < 0)
    this->speed = 15; // 15 mdeg/s just seems to be a safe speed; 
  else
    this->speed = speedLimit;

  this->cmd_blocking = blocking;
  this->minpan = MINPAN;
  this->maxpan = MAXPAN;
  this->mintilt = MINTILT;
  this->maxtilt = MAXTILT;

  // ----------------------------------------------
  // Initialize private variables to default values
  // ----------------------------------------------  
  baud = 2; //this is really 500kbps

  net=0;                  /* logical netnumber */
  mode=0;                 /* mode for canOpen */
  txqueuesize=10;         /* maximum number of messages to transmit */
  rxqueuesize=10;         /* maximum number of messages to receive */
  txtimeout=100;          /* timeout for transmit */
  rxtimeout=10000;        /* timeout for receiving data */  

  return;

}


// ===============================
// Default Constructor for Pan Tilt Unit
// ===============================
AmtecPowerCube::AmtecPowerCube() 
{
  memset(this, 0, sizeof(this));

  MSG("no speed specified. Using a safe speed of 15mdeg/s");
  this->speed = 15; //mdeg/s ... yes, mili-deg/s 
  this->cmd_blocking = true;
  this->minpan = MINPAN;
  this->maxpan = MAXPAN;
  this->mintilt = MINTILT;
  this->maxtilt =MAXTILT;

  // ----------------------------------------------
  // Initialize private variables to default values
  // ----------------------------------------------  
  baud = 2; //this is really 500kbps

  net=0;                  /* logical netnumber */
  mode=0;                 /* mode for canOpen */
  txqueuesize=10;         /* maximum number of messages to transmit */
  rxqueuesize=10;         /* maximum number of messages to receive */
  txtimeout=100;          /* timeout for transmit */
  rxtimeout=10000;        /* timeout for receiving data */  

  return;

}


// ===============================
// Destructor for Pan Tilt Unit
// ===============================
AmtecPowerCube::~AmtecPowerCube() 
{  
  return;
}


// ===============================
// Setup (Connect to CAN interface)
// ===============================
int AmtecPowerCube::Setup()
{

  // default to position control
  this->controlmode = POS_CONTROL;

  int retvalue;
  
  // ------------------------------------
  // Open CAN handle for sending messages to Pan-module
  // ------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &txhandle_pan);
  
  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() transmit handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else    
    printf("opened transmit handle OK !\n");
  
  // ------------------------
  // Set Baudrate for sending
  // ------------------------
  retvalue = canSetBaudrate(txhandle_pan,baud);
  if(retvalue!=0)
    {
      printf("canSetBaudrate() failed with error %d!\n", retvalue);
      canClose(txhandle_pan);
      return(-1);
    }
  else    
    printf("function canSetBaudrate() returned OK ! \n");
    

  // ------------------------------------
  // Open CAN handle for sending messages to Tilt-module
  // ------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &txhandle_tilt);
  
  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() transmit handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else    
    printf("opened transmit handle OK !\n");
  
  // ------------------------
  // Set Baudrate for sending
  // ------------------------
  retvalue = canSetBaudrate(txhandle_tilt,baud);
  if(retvalue!=0)
    {
      printf("canSetBaudrate() failed with error %d!\n", retvalue);
      canClose(txhandle_tilt);
      return(-1);
    }
  else    
    printf("function canSetBaudrate() returned OK ! \n");


  /// ----------------------------------
  /// Open CAN handle for reading from Pan module
  /// ----------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_pan);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() track handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened pan handle OK !\n");

  // -------------------------------
  // Add handle ID for Pan module 
  // -------------------------------
  int32_t pan_id;
  pan_id = 0x0a0 + AMTEC_MODULE_PAN;
  retvalue = canIdAdd(rxhandle_pan,pan_id);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_pan);
      return(-1);
    }
 
  /// ------------------------------------------
  /// Open CAN handle for reading from tilt module
  /// ------------------------------------------
  retvalue = canOpen(net,
		     mode,
		     txqueuesize,
		     rxqueuesize,
		     txtimeout,
		     rxtimeout,
		     &rxhandle_tilt);

  if(retvalue!=NTCAN_SUCCESS)
    {      
      printf("canOpen() target handle failed with error %d!\n", retvalue);
      return(-1);
    }
  else
    printf("opened tilt handle OK !\n");

  // --------------------------------
  // Add handle ID for tilt module
  // --------------------------------
  int32_t tilt_id;
  tilt_id = 0x0a0 + AMTEC_MODULE_TILT;
  retvalue = canIdAdd(rxhandle_tilt,tilt_id);
  if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canIdAdd() failed with error %d!\n", retvalue);
      printf(" could not add handle ID for general status message.\n");
      canClose(rxhandle_tilt);
      return(-1);
    }

  MSG("Commanding Reset() of PTU ...");
  if(Reset() < 0)
  {
    canClose(txhandle_pan);
    canClose(txhandle_tilt);
    canClose(rxhandle_pan);
    canClose(rxhandle_tilt);
    return(ERROR("Reset() failed; bailing."));
  }

  MSG("homing the unit now");
  if(Home() < 0)
  {
    canClose(txhandle_pan);
    canClose(txhandle_tilt);
    canClose(rxhandle_pan);
    canClose(rxhandle_tilt);
    return(ERROR("Home() failed; bailing."));
  }

  return(0);
}


// ===============================
// Reset (Clear Error State)
// ===============================
int AmtecPowerCube::Reset()
{
  unsigned char cmd[1];
  CMSG rmsg_pan, rmsg_tilt;

  cmd[0] = AMTEC_CMD_RESET;
  if(SendCommand(0x0e0 + AMTEC_MODULE_PAN,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));
  
  if(ReadAnswer(AMTEC_MODULE_PAN,&rmsg_pan) < 0)
    return(ERROR("ReadAnswer() failed"));
  
  if(SendCommand(0x0e0 + AMTEC_MODULE_TILT,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(AMTEC_MODULE_TILT,&rmsg_tilt) < 0)
    return(ERROR("ReadAnswer() failed"));

  return(0);
}

// ===============================
// Begin homing procedure back to initial pose
// ===============================
int AmtecPowerCube::Home()
{
  unsigned char cmd[1];
  unsigned int state;
  int status;
  CMSG rmsg_pan, rmsg_tilt;

  cmd[0] = AMTEC_CMD_HOME;

  MSG("sending home command");
  if(SendCommand(0x0e0 + AMTEC_MODULE_PAN,cmd,1) < 0)
    return(ERROR("SendCommand() for pan failed"));

  MSG("reading home response");
  if(ReadAnswer(AMTEC_MODULE_PAN,&rmsg_pan) < 0)
    return(ERROR("ReadAnswer() failed"));
  
  // poll the device state, wait for homing to finish
  for(;;)
  {
    usleep(AMTEC_SLEEP_TIME_USEC);

    status = CheckCubeState(AMTEC_MODULE_PAN, &state);
    if(status==0)
      break;
    else if(status==-1)
      return(-1);
    
  }

  if(SendCommand(0x0e0 + AMTEC_MODULE_TILT,cmd,1) < 0)   
    return(ERROR("SendCommand() for tilt failed"));

  if(ReadAnswer(AMTEC_MODULE_TILT,&rmsg_tilt) < 0)
    return(ERROR("ReadAnswer() failed"));

  // poll the device state, wait for homing to finish
  for(;;)
  {
    usleep(AMTEC_SLEEP_TIME_USEC);

    status = CheckCubeState(AMTEC_MODULE_TILT, &state);    
    if(status==0)
      break;
    else if(status==-1)
      return(-1);
  }

  if( GetPanTiltPos(&this->lastpan, &this->lasttilt) < 0 )
    WARN("Could not get pan-tilt position");

  if( GetPanTiltVel(&this->lastpanspeed, &this->lasttiltspeed) < 0 )
    WARN("Could not get pan-tilt velocity");
  

  return(0);
}


// ===============================
// Halt (Stop Immediately)
// ===============================
int AmtecPowerCube::Halt()
{
  unsigned char cmd[1];
  CMSG rmsg_pan, rmsg_tilt;

  cmd[0] = AMTEC_CMD_HALT;
  if(SendCommand(0x0e0 + AMTEC_MODULE_PAN,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(AMTEC_MODULE_PAN,&rmsg_pan) < 0)
    return(ERROR("ReadAnswer() failed"));

  if(SendCommand(0x0e0 + AMTEC_MODULE_TILT,cmd,1) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(AMTEC_MODULE_TILT,&rmsg_tilt) < 0)
    return(ERROR("ReadAnswer() failed"));

  return(0);
}


// ===============================
// Shutdown (Close all connections)
// ===============================
int AmtecPowerCube::Shutdown()
{
  // stop the unit
  if(Halt())
    WARN("Halt() failed.");

  // maybe return it to home
  if(Home())
    WARN("Home() failed.");

  canClose(txhandle_pan);
  canClose(txhandle_tilt);
  canClose(rxhandle_pan);
  canClose(rxhandle_tilt);

  puts("Amtec PowerCube has been shutdown");
  return(0);
}


// NOTE: these conversion methods only work on little-endian machines
// (the Amtec protocol also uses little-endian).

// ===============================
// Convert bytes to ints and floats and vise versa
// ===============================
float AmtecPowerCube::BytesToFloat(CMSG* rmsg)
{
  float f;
  unsigned char tmp_buf[4];
  tmp_buf[0] = rmsg->data[2];
  tmp_buf[1] = rmsg->data[3];
  tmp_buf[2] = rmsg->data[4];
  tmp_buf[3] = rmsg->data[5];

  memcpy((void*)&f, tmp_buf, 4);
  return(f);
}
unsigned int AmtecPowerCube::BytesToUint32(CMSG* rmsg)
{
  unsigned int i;
  unsigned char tmp_buf[4];
  tmp_buf[0] = rmsg->data[2];
  tmp_buf[1] = rmsg->data[3];
  tmp_buf[2] = rmsg->data[4];
  tmp_buf[3] = rmsg->data[5];

  memcpy((void*)&i, tmp_buf, 4);
  return(i);
}
void AmtecPowerCube::FloatToBytes(unsigned char* bytes, float f)
{
  memcpy(bytes, (void*)&f, 4);
}
void AmtecPowerCube::Uint16ToBytes(unsigned char *bytes, unsigned short s)
{
  memcpy(bytes, (void*)&s, 2);
}


// ===============================
// Send commands to the PTU
// ===============================
int AmtecPowerCube::SendCommand(int32_t canid, unsigned char* cmd, size_t len)
{
  int retvalue;
  int32_t frame_len;
  frame_len = 1;
  CMSG cmsg;

  cmsg.id = canid;
  cmsg.len = (uint8_t)len;
  for (uint i=0;i<len;i++) 
    {
      cmsg.data[i] = cmd[i];
    }

  if( ((int)(canid & 0x1F))==AMTEC_MODULE_TILT)
    retvalue = canSend(txhandle_tilt, &cmsg, &frame_len);
  else if( ((int)(canid & 0x1F))==AMTEC_MODULE_PAN)
    retvalue = canSend(txhandle_pan, &cmsg, &frame_len);

  if(retvalue != NTCAN_SUCCESS)
    {
      printf("ERROR: unable to send command!!\n");  
      return(-1);
    }
  
  return(0);

}


// ===============================
// Read Answer response from PTU
// ===============================
int AmtecPowerCube::ReadAnswer(int id, CMSG* rmsg)
{
  int retvalue;
  int32_t len = 8;

  if(id==AMTEC_MODULE_PAN)
    retvalue = canRead(rxhandle_pan,rmsg,&len,NULL);
  else if(id==AMTEC_MODULE_TILT)
    retvalue = canRead(rxhandle_tilt,rmsg,&len,NULL);
  
  if(retvalue==NTCAN_RX_TIMEOUT)
    {
      printf("canRead() returned timeout\n");
      return(-1);
    }
  else if(retvalue!=NTCAN_SUCCESS)
    {
      printf("canRead() failed with error %d!\n", retvalue);
      return(-1);
    }  
  return(0);
}


// ===============================
// Get Float Parameter
// ===============================
int AmtecPowerCube::GetFloatParam(int id, int param, float* val)
{
  unsigned char cmd[2];
  CMSG rmsg;

  cmd[0] = AMTEC_CMD_GET_EXT;
  cmd[1] = param;

  if(SendCommand(0x0c0 + id, cmd, 2) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(id,&rmsg) < 0)
    return(ERROR("ReadAnswer() failed"));

  *val = BytesToFloat(&rmsg);
 
  return(0);
}


// ===============================
// Get unsigned int parameter
// ===============================
int AmtecPowerCube::GetUint32Param(int id, int param, unsigned int* val)
{
  unsigned char cmd[2];
  CMSG rmsg;

  cmd[0] = AMTEC_CMD_GET_EXT;
  cmd[1] = param;

  if(SendCommand(0x0c0 + id, cmd, 2) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(id,&rmsg) < 0)
    return(ERROR("ReadAnswer() failed"));  

  *val = BytesToUint32(&rmsg);

  return(0);
}


// ===============================
// Set Float Parameter
// ===============================
int AmtecPowerCube::SetFloatParam(int id, int param, float val)
{
  unsigned char cmd[6];
  CMSG rmsg;

  cmd[0] = AMTEC_CMD_SET_EXT;
  cmd[1] = param;
  FloatToBytes(cmd+2, val);

  if(SendCommand(0x0e0 + id, cmd, 6) < 0)
    return(ERROR("SendCommand() failed"));

  if(ReadAnswer(id,&rmsg) < 0)
    return(ERROR("ReadAnswer() failed"));

  return(0);
}


// ===============================
// Get PTU pose
// ===============================
int AmtecPowerCube::GetPanTiltPos(float* pan, float* tilt)
{
  float tmp;
  // get the pan
  if(GetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_ACT_POS, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  // reverse pan angle, to increase ccw, then normalize
  *pan = -RTOD(NORMALIZE(tmp));
  
  // get the tilt
  if(GetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_ACT_POS, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  
  *tilt = RTOD(tmp);

  return(0);
}


// ===============================
// Get PTU Velocity
// ===============================
int AmtecPowerCube::GetPanTiltVel(float* panspeed, float* tiltspeed)
{
  float tmp;

  // get the pan
  if(GetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_ACT_VEL, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  
  // reverse pan angle, to increase ccw, then normalize
  *panspeed = -RTOD(NORMALIZE(tmp));
  
  // get the tilt
  if(GetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_ACT_VEL, &tmp) < 0)
    return(ERROR("GetFloatParam() failed"));
  
  *tiltspeed = RTOD(tmp);

  return(0);
}


// ===============================
// Set PTU Limits
// ===============================
int AmtecPowerCube::SetLimits()
{
  // have to reverse the signs for the pan limits, since the Amtec unit
  // counts up clockwise, rather than ccw.
  if(this->minpan != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_MAX_POS, 
                     NORMALIZE(DTOR(-this->minpan))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  if(this->maxpan != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_PAN, AMTEC_PARAM_MIN_POS, 
                     NORMALIZE(DTOR(-this->maxpan))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  if(this->mintilt != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_MIN_POS, 
                     NORMALIZE(DTOR(this->mintilt))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  if(this->maxtilt != INT_MAX)
  {
    if(SetFloatParam(AMTEC_MODULE_TILT, AMTEC_PARAM_MAX_POS, 
                     NORMALIZE(DTOR(this->maxtilt))) < 0)
      return(ERROR("SetFloatParam() failed"));
  }
  return(0);
}


// ===============================
// Set Pan Position (specify in degrees)
// ===============================
int AmtecPowerCube::SetPanPos(float oldpan, float pan)
{
  unsigned char cmd[8];
  float newpan;
  unsigned short time;
  CMSG rmsg;

  newpan = DTOR(pan);
  time = (unsigned short)rint(((double)fabs(pan - oldpan) / 
                               (double)this->speed) * 1e3);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FSTEP_ACK;
  FloatToBytes(cmd+2,newpan);
  Uint16ToBytes(cmd+6,time);
  if(SendCommand(0x0e0 + AMTEC_MODULE_PAN,cmd,8) < 0)
    return(-1);
  if(ReadAnswer(AMTEC_MODULE_PAN,&rmsg) < 0)
    return(-1);
  
  return(0);
}


// ===============================
// Set Tilt Position (specify in degrees)
// ===============================
int AmtecPowerCube::SetTiltPos(float oldtilt, float tilt)
{
  unsigned char cmd[8];
  float newtilt;
  unsigned short time;
  CMSG rmsg;

  newtilt = DTOR(tilt);
  time = (unsigned short)rint(((double)fabs(tilt - oldtilt) / 
                               (double)this->speed) * 1e3);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FSTEP_ACK;
  FloatToBytes(cmd+2,newtilt);
  Uint16ToBytes(cmd+6,time);
  if(SendCommand(0x0e0 + AMTEC_MODULE_TILT,cmd,8) < 0)
    return(-1);
  if(ReadAnswer(AMTEC_MODULE_TILT,&rmsg) < 0)
    return(-1);

  return(0);
}


// ===============================
// Set Pan Velocity (specify in deg/s)
// ===============================
int AmtecPowerCube::SetPanVel(float panspeed)
{
  unsigned char cmd[6];
  float newpanspeed;
  CMSG rmsg;

  newpanspeed = DTOR(panspeed);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FVEL_ACK;
  FloatToBytes(cmd+2,newpanspeed);
  if(SendCommand(0x0e0 + AMTEC_MODULE_PAN,cmd,6) < 0)
    return(-1);
  if(ReadAnswer(AMTEC_MODULE_PAN,&rmsg) < 0)
    return(-1);
  return(0);
}


// ===============================
// Set Tilt Position (specify in deg/s)
// ===============================
int AmtecPowerCube::SetTiltVel(float tiltspeed)
{
  unsigned char cmd[6];
  float newtiltspeed;
  CMSG rmsg;

  newtiltspeed = DTOR(tiltspeed);

  cmd[0] = AMTEC_CMD_SET_MOTION;
  cmd[1] = AMTEC_MOTION_FVEL_ACK;
  FloatToBytes(cmd+2,newtiltspeed);
  if(SendCommand(0x0e0 + AMTEC_MODULE_TILT,cmd,6) < 0)
    return(-1);
  if(ReadAnswer(AMTEC_MODULE_TILT,&rmsg) < 0)
    return(-1);
  return(0);
}



// ===============================
// Check the state of the power cube
// ===============================
int AmtecPowerCube::CheckCubeState(int id, unsigned int *state)
{
  // get the module state (for debugging and warning)
  if(GetUint32Param(id, AMTEC_PARAM_CUBESTATE, state) < 0)
    return(ERROR("GetUint32Param() failed; bailing"));

  if(*state & AMTEC_STATE_HOME_OK)
    return(0);

  if(*state & AMTEC_STATE_ERROR)      
    return(ERROR("the Amtec unit has encountered an error and will need\n"
	    "    to be reset; bailing.   Current module state: 0x%x", 
		 *state));    
  else if(*state & AMTEC_POWERFAULT)      
    return(ERROR("The Amtec unit has encountered an error. Powerfault encountered. \n"
	    "The module needs to be switched off to reset encountered error. Current module state: 0x%x", 
		 *state));    
  else if(*state & AMTEC_TOW_ERROR)      
    return(ERROR("The Amtec unit has encountered an error. Servo loop unable to follow target position within given limits. \n"
	    "Check if module was overloaded. Current module state: 0x%x", 
		 *state)); 
  else if(*state & AMTEC_STATE_CPU_OVERLOAD)      
    return(ERROR("The Amtec unit has encountered an error. Communication breakdown between CPU and current controller. \n"
	    "Power must be switched off. Please consult your service partner. Current module state: 0x%x", 
		 *state));
  else if(*state & AMTEC_STATE_BEYOND_HARD)      
    return(ERROR("The Amtec unit has encountered an error. State has reached a hard limit. Emergency stop has been executed automatically. \n"
	    "To remove module from this position, see 'PowerCube Operation System: Disorder'. Current module state: 0x%x", 
		 *state));
  else if(*state & AMTEC_STATE_POW_SETUP_ERR)      
    return(ERROR("The Amtec unit has encountered an error. Error in initializing the current controller with power setup. \n"
	    "Power must be switched off. Please consult your service partner. Current module state: 0x%x", 
		 *state));
  return(1);
}

// ===============================
// Process Message commands
// ===============================
int AmtecPowerCube::ProcessMessage(Command command)
{
  float newpan, newtilt;  
  float newpanspeed, newtiltspeed;
  unsigned int panstate, tiltstate;
  unsigned int panMotionState = 0;
  unsigned int tiltMotionState = 0;
  int status;
  
  if(this->controlmode == POS_CONTROL)
    {
      // watch the hard limits of panning
      if( command.pan > this->maxpan )
	{
	  MSG("\n requested pan angle exceeds hard limit! \n sending maxpan instead: %d", this->maxpan);
	  command.pan = this->maxpan;
	}
      if(command.pan < this->minpan )
	{
	  MSG("\n requested pan angle exceeds hard limit! \n sending minpan instead: %d", this->minpan);
	  command.pan = this->minpan;
	}

      // watch the hard limits of tilting
      if(command.tilt > this->maxtilt)
	{
	  MSG("\n requested tilt angle exceeds hard limit! \n sending maxtilt instead: %d", this->maxtilt);
	  command.tilt = this->maxtilt;
	}
      if(command.tilt < this->mintilt)
	{
	  MSG("\n requested tilt angle exceeds hard limit! \n sending mintilt instead: %d", this->mintilt);
	  command.tilt = this->mintilt;
	}

      newpan =  command.pan; //need the minus for right sign convention.
      newtilt = command.tilt;
      
      if(newpan != lastpan)
	{
	  // send new pan position
	  printf("Setting pan position to: %f\n", newpan);
	  if(SetPanPos(lastpan,newpan))
	    return(ERROR("SetPan() failed(); bailing."));
	  
	  lastpan = newpan;
	}
      
      if(newtilt != lasttilt)
	{
	  // send new tilt position
	  printf("Setting tilt position to: %f\n", newtilt);
	  if(SetTiltPos(lasttilt,newtilt))
	    return(ERROR("SetTilt() failed(); bailing."));
	  
	  lasttilt = newtilt;
	}

      // if cmd_blocking enabled, poll the device state, wait for cmd to finish
      if(this->cmd_blocking)
	{
	  for(;;)
	    {
	      usleep(AMTEC_SLEEP_TIME_USEC);
	      
	      status = CheckCubeState(AMTEC_MODULE_TILT, &panstate);
	      status = CheckCubeState(AMTEC_MODULE_PAN, &tiltstate);
	      panMotionState = panstate & 0x800;
	      tiltMotionState = tiltstate & 0x800;
	      if( (panMotionState == 0x800) || (tiltMotionState == 0x800) )
		continue;	  	  
	      else
		break;	  
	    }
	}


    }
  else if(this->controlmode == VEL_CONTROL)
    {
      // reverse pan angle, to increase ccw
      newpanspeed =  command.panspeed;
      newtiltspeed = command.tiltspeed;
      
      if(newpanspeed != lastpanspeed)
	{
	  // send new pan speed
	  if(SetPanVel(newpanspeed))
	    return(ERROR("SetPanVel() failed(); bailing."));
	  
	  lastpanspeed = newpanspeed;
	}
      
      if(newtiltspeed != lasttiltspeed)
	{
	  // send new tilt position
	  if(SetTiltVel(newtiltspeed))
	    return(ERROR("SetTiltVel() failed(); bailing."));
	  
	  lasttiltspeed = newtiltspeed;
	}
    }
  else
    return(ERROR("unkown control mode: %d; bailing",this->controlmode));

  return 0;
 
}

#if AMTEC_DRIVER_TEST

int main(int argc, char *argv[])
{

  float currpan, currtilt;
  float currpanspeed, currtiltspeed;
  int home;
  int speed;
  int val;
  bool blocking;
  unsigned int state;
  Command command;

  if(argc<3)
    {
      WARN("\n usage: ./amtectest <SPEEDLIMIT(deg/s)> <BLOCKING>\n     (e.g. ./amtectest 15 1");
      WARN("\n using default values: \n     <SPEEDLIMIT> = 15   <BLOCKING> = 1");      
      home = 1;
      speed = 15;
      blocking = true;
    }
  else
    {
      speed = atoi(argv[1]);

      if(strncmp(argv[2],"1",1)==0 || strncmp(argv[2],"0",1)==0)	
	{
	  val = atoi(argv[2]);
	  if(val==1)
	    blocking = true;
	  else
	    blocking = false;
	}
      else
	{
	  WARN("invalid <BLOCKING>; using '1' ");
	  blocking = true;
	}

    }

  AmtecPowerCube amtecptu(speed, blocking);

  printf("Setting up the PTU ... \n");

  if(amtecptu.Setup()<0)
    return(ERROR("Could not establish connection. Abort."));

  printf("Connection established!! \n");

  if(amtecptu.GetPanTiltPos(&currpan, &currtilt)<0)
    return(ERROR("Could not get pan-tilt position"));

  if(amtecptu.GetPanTiltVel(&currpanspeed, &currtiltspeed)<0)
    return(ERROR("Could not get pan-tilt velocity"));

  printf("--- STATUS ---\n");
  printf("currpan: %f  currtilt: %f\n", currpan, currtilt);
  printf("currpanspeed: %f  currtiltspeed: %f\n", currpanspeed, currtiltspeed);


  if(amtecptu.CheckCubeState(AMTEC_MODULE_PAN, &state) < 0)
    {
      WARN("Bad cube state! exiting now.");
      delete &amtecptu;
      return -1;
    }

  if(amtecptu.CheckCubeState(AMTEC_MODULE_TILT, &state) < 0)
    {
      WARN("Bad cube state! exiting now.");
      delete &amtecptu;
      return -1;
    }
  

  printf("starting sleep\n");
  sleep(3);
  printf("sleep over.\n");


  command.pan = -30;
  command.tilt = -30;
  amtecptu.ProcessMessage(command);

  printf("--- STATUS ---\n");
  for(int i=0; i<100; i++)
    {
      if(amtecptu.GetPanTiltPos(&currpan, &currtilt)<0)
	WARN("Could not get pan-tilt position");
      if(amtecptu.GetPanTiltVel(&currpanspeed, &currtiltspeed)<0)
	WARN("Could not get pan-tilt velocity");

      printf("currpan: %f  currtilt: %f\n", currpan, currtilt);
      printf("currpanspeed: %f  currtiltspeed: %f\n", currpanspeed, currtiltspeed);

    }

  command.pan = 0;
  command.tilt = 0;
  amtecptu.ProcessMessage(command);


  printf("going to shutdown amtecptu in 5 seconds ...\n");
  sleep(2);
  amtecptu.Shutdown();

  return(0);

}

#endif
