#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <interfaces/PTUCommand.h>

using namespace std;

int main(int argc, char* argv[])
{
 
  int ptuCommandSocket;
  int sendSuccess;
  int type;
  float pan, tilt;
  float localx, localy, localz;
  float panspeed, tiltspeed;
  PTUCommand ptuCommand;
 
  skynet m_skynet = skynet(217, atoi(getenv("SKYNET_KEY")), NULL); 
  ptuCommandSocket = m_skynet.get_send_sock(SNptuCommand);
  if(ptuCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }

  while(true)
    {

      cout << "What would you like to do? " << endl;
      cout << "0: Send Line-of-site command" << endl;
      cout << "1: Send Raw commands " << endl;
      cout << "2: Start Demo" << endl;
      cin >> type;

      if(type==0)
	{
	  cout << "Enter commanded pose for line of site: <localx> <localy> <localz> <panspeed> <tiltspeed>" << endl;
	  cin >> localx >> localy >> localz >> panspeed >> tiltspeed;
	  
	  ptuCommand.type = LINEOFSITE;
	  ptuCommand.localx = localx;
	  ptuCommand.localy = localy;
	  ptuCommand.localz = localz;
	  ptuCommand.panspeed = panspeed; //mdeg/s
	  ptuCommand.tiltspeed = tiltspeed; //mdeg/s
	  
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	}
      else if(type==1)
	{
	  cout << "Enter commanded pan and tilt angle (deg): <PAN> <TILT> <PANSPEED> <TILTSPEED>" << endl;
	  cin >> pan >> tilt >> panspeed >> tiltspeed;
	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan;
	  ptuCommand.tilt = tilt;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	}
      else if(type==2)
      	{
	  while(true)
	  {
	    ptuCommand.type = RAW;
	    ptuCommand.pan = 60;
	    ptuCommand.tilt = -30;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	    sleep(10);

	    ptuCommand.type = RAW;
	    ptuCommand.pan = 0;
	    ptuCommand.tilt = 0;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	    sleep(10);

	    ptuCommand.type = RAW;
	    ptuCommand.pan = -60;
	    ptuCommand.tilt = -30;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	
	    sleep(10);

	    ptuCommand.type = RAW;
	    ptuCommand.pan = 0;
	    ptuCommand.tilt = 0;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	   sleep(10);

	  }
	}
      else
	printf("Unrecognized type. Please try again\n");
    }
      
  return(0);
}
