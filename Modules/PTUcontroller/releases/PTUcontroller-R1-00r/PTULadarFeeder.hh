#ifndef __PTU_LADAR_FEEDER__
#define __PTU_LADAR_FEEDER__

/* 
 * Desc: PTU Ladar feeder module 
 * Date: 26 September 2007
 * Author: Jeremy Ma (adapted from Andrew Howard's Ladarfeeder)
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/LadarRangeBlob.h>
#include <cotk/cotk.h>
#include <interfaces/PTUStateBlob.h>
#include "sick_driver.h"
#include "cmdline.h"

// Useful message macro
#ifndef USEFUL_MACROS
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#endif 

/// @brief Ladar feeder class
class PTULadarFeeder
{
  public:   

  /// Default constructor
  PTULadarFeeder();

  /// Default constructor
  PTULadarFeeder(bool logging );

  /// Default destructor
  ~PTULadarFeeder();
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize feeder for live capture
  int initLive();
  
  /// Finalize feeder for live capture
  int finiLive();

  /// Capture a scan 
  int captureLive();

  /// Initialize sensnet
  int initSensnet(const char *configPath);

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data over sensnet
  int writeSensnet(PTUStateBlob ptublob, bool enableLadarLog);

  public:

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char *defaultConfigPath;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;
  
  // Port we are talking to (this can be either a USB or an IP w/ port)
  const char *ladarPort;

  // SICK driver
  sick_driver_t *sick;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  // Sensor-to-tool frame transform for PTU ladar
  float sens2tool[4][4];
  
  // Log file name
  char logName[1024];
  
  // Is logging enabled?
  bool enableLog;

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log handle
  sensnet_log_t *sensnet_log;
  
  // Blob buffer
  LadarRangeBlob *blob;

  // Console text display
  cotk_t *console;

  // Current scan id
  int scanId;

  // Current scan time (microseconds)
  uint64_t scanTime;

  // Current vehicle state data
  VehicleState state;

  // Point data (bearing, range, intensity)
  int numPoints;
  float points[361][3];
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Logging stats
  int logCount, logSize;

  // Export stats
  int exportId;

};

#endif
