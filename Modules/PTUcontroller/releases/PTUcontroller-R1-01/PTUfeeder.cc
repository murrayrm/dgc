
/* 
 * Desc: PTUfeeder module
 * Date: 21 May 2007
 * Author: Jeremy Ma
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/PTUStateBlob.h>
#include <interfaces/PTUCommand.h>
#include <cotk/cotk.h>

#include "amtecpowercube.hh"
#include "directpercept.hh"
#include "cmdline.h"
#include "PTULadarFeeder.hh"

/// @brief PTU feeder class
class PTUfeeder
{
  public:   

  /// Default constructor
  PTUfeeder();

  /// Default destructor
  ~PTUfeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize feeder for live capture
  int initSim(const char *configPath);

  /// Initialize feeder for live capture
  int initLive(const char *configPath);
  
  /// Finalize feeder for live capture
  int finiLive();

  // Capture a live scan of the PTU state
  int captureLive();

  // Capture a live scan of the PTU state
  int captureSim();

  /// Initialize sensnet
  int initSensnet(const char *configPath);

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data over sensnet
  int writeSensnet();

  /// Get the predicted vehicle state
  int getState(uint64_t timestamp);

  /// Get the process state
  int getProcessState();

  /// Process a scan
  int process();

  /// Start ladarfeeder thread
  /// -- this will start ladarfeeder in serial mode and only for the PTU ladar (very specific)
  void startLadarFeederThread();

  public:

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, PTUfeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, PTUfeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, PTUfeeder *self, const char *token);

  public:

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char *defaultConfigPath;

  // Serial number
  char *serialNumber;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;
  
  // What mode are we in?
  enum {modeLive, modeSim} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Port we are talking to (this can be either a USB or an IP w/ port)
  char *port;

  // Speed at which the rotations are executed (mdeg/s)
  int initPanSpeed;
  int initTiltSpeed;

  // enable or disable blocking wait on execution of commands
  bool blocking;

  // amtec driver
  AmtecPowerCube *amtecptu;  

  DirectPercept *directpercept;

  PTULadarFeeder *ladarfeeder;

  // Sensor-to-PTU transform
  float tool2ptu[4][4];

  // PTU-to-vehicle transform
  float ptu2veh[4][4];
  
  // Log file name
  char logName[1024];
  
  // Is logging enabled?
  bool enableLog;

  // Is ladar logging enabled?
  bool enableLadarLog;

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log handle
  sensnet_log_t *sensnet_log;
  
  // Blob buffer
  PTUStateBlob *blob;

  PTUStateBlob m_blob;

  // Console text display
  cotk_t *console;

  // Current scan id
  int scanId;

  // Current scan time (microseconds)
  uint64_t scanTime;

  // Last scan time (microseconds)
  uint64_t last_scanTime;


  // Current vehicle state data
  VehicleState state;

  // PTU state data
  float currpan, initpan, lastpan;
  float currtilt, inittilt, lasttilt;
  float currpanspeed;
  float currtiltspeed;

  // This is the net change in angles from the current position and the initial position
  // -- for the most part, the delta values will be approximately the same as currpan, currtilt
  //    but there is a slight offset in the initial homing configuration that pitches the 
  //    PTU slightly up a bit by 1 deg.
  float pandelta, tiltdelta;
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Capture stats
  int capCount;
  //uint64_t capTime;
  //double capRate, capPeriod;

  int healthCount;

  // Logging stats
  int logCount, logSize;

  // Struct for command to send to PTU
  AmtecCommand command;

  // Struct for command to send to the Directed Perception PTU
  DPCommand dp_command;
  
  // Skynet object
  skynet *m_skynet;

  // command Sockets
  int ptuCommandSocket;
  int ptuCommandReceived;
  PTUCommand ptuCommand; // this is the incoming command

  // Process rate info
  double currenttime;
  double lasttime;
  double cycleAvg;
  int cycleCount;
  struct timeval stv;

  // Mutex stuff
  pthread_mutex_t ptuStateMutex;

};

// Default constructor
PTUfeeder::PTUfeeder()
{
  //memset(this, 0, sizeof(*this));

  this->scanId = -1;

  gettimeofday(&stv, 0);
  this->lasttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  this->initpan = 0;
  this->inittilt = 0;
  this->initPanSpeed = 0;
  this->initTiltSpeed = 0;
  this->currpanspeed = 0;
  this->currtiltspeed = 0;
  this->currpan = 0;
  this->currtilt = 0;
  this->pandelta = 0;
  this->tiltdelta = 0;

  this->lastpan = 0;
  this->lasttilt = 0;
  
  this->capCount = 0;
  this->logCount  = 0;
  this->logSize = 0;
  this->cycleCount = 0;
  this->cycleAvg = 0;

  this->healthCount = 0;

  this->enableLog = false;
  this->enableLadarLog = false;

  DGCcreateMutex(&ptuStateMutex);

  return;
}


// Default destructor
PTUfeeder::~PTUfeeder()
{
  DGCdeleteMutex(&ptuStateMutex);
  return;
}


// Parse the command line
int PTUfeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
  
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("PTUcontroller");
 
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  /*
  if(this->options.enable_ladar_log_flag)
    this->enableLadarLog = true;
  */
  
  return 0;
}


// Parse the config file
int PTUfeeder::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  this->port = this->options.port_arg;

  if(sscanf(this->options.init_pan_speed_arg, "%d", &this->initPanSpeed) < 1)
    return ERROR("invalid initial pan speed");

  if(sscanf(this->options.init_tilt_speed_arg, "%d", &this->initTiltSpeed) < 1)
    return ERROR("invalid initial tilt speed");

  if(this->options.enable_blocking_flag)
    this->blocking = true;
  else
    this->blocking = false;

  // Parse transform
  pose3_t pose;
  float px, py, pz;
  float rx, ry, rz;

  if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
    return ERROR("syntax error in sensor pos argument");
  if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
    return ERROR("syntax error in sensor rot argument");
    
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->ptu2veh);


  return 0;
}

// Initialize feeder for live capture
int PTUfeeder::initSim(const char *configPath)
{
  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  // homing the device always brings it to the home position of (0,0)
  this->initpan = 0.0; 
  this->inittilt = 0.0;

  // initialize past values
  this->lastpan = this->initpan;
  this->lasttilt = this->inittilt;
  this->last_scanTime = DGCgettime();
    
  // Initialize sockets for receiving commands
  this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);
  this->ptuCommandSocket = this->m_skynet->listen(SNptuCommand, this->moduleId);
  if(this->ptuCommandSocket < 0)
    ERROR("PTUfeeder::initSim(): skynet listen returned error");

  this->mode = modeSim;

  return 0;
}


// Initialize feeder for live capture
int PTUfeeder::initLive(const char *configPath)
{
  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  MSG("connecting at this port %s", this->port);


  if(this->sensorId==SENSNET_LF_PTU)
    {
      // Initialize the Directed Perception PTU
      this->directpercept = new DirectPercept(this->initPanSpeed, this->initTiltSpeed);
      assert(this->directpercept);
      
      // There is a homing of the device done when Setup is called
      if(this->directpercept->Setup(this->port)<0)
	return(ERROR("Could not establish connection. Abort"));
      
      if(this->directpercept->GetPanTiltPos(&this->initpan, &this->inittilt) < 0 )
	WARN("Could not get pan-tilt position");    

      // Initialize sockets for receiving commands
      this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);
      this->ptuCommandSocket = this->m_skynet->listen(SNptuMiniCommand, this->moduleId);
      if(this->ptuCommandSocket < 0)
	return ERROR("PTUfeeder::initLive(): skynet listen returned error");
    }
  else
    {
      // Initialize the Amtec Powercube PTU
      this->amtecptu = new AmtecPowerCube(this->initPanSpeed, this->initTiltSpeed, this->blocking);
      assert(this->amtecptu);
      
      // There is a homing of the device done when Setup is called
      if(this->amtecptu->Setup(this->options.serial_number_arg)<0)
	return(ERROR("Could not establish connection. Abort"));
      
      if(this->amtecptu->GetPanTiltPos(&this->initpan, &this->inittilt) < 0 )
	WARN("Could not get pan-tilt position");    

      // Initialize sockets for receiving commands
      this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);
      this->ptuCommandSocket = this->m_skynet->listen(SNptuCommand, this->moduleId);
      if(this->ptuCommandSocket < 0)
	return ERROR("PTUfeeder::initLive(): skynet listen returned error");
    }

  this->mode = modeLive;

  return 0;

}


// Finalize feeder for live capture
int PTUfeeder::finiLive()
{
  if(this->sensorId==SENSNET_LF_PTU)
    {
      MSG("commanding shutdown for finiLive()");
      this->directpercept->Shutdown();

      MSG("deleting directpercept object");
      delete this->directpercept;
    }
  else
    {
      MSG("commanding shutdown for finiLive()");
      this->amtecptu->Shutdown();
      
      MSG("deleting amtecptu object");
      delete this->amtecptu;
    }
  return 0;
}
 
// Capture a scan 
int PTUfeeder::captureLive()
{  
  pose3_t pose;
  PTUStateBlob *blob;
  float vx,vy,vz;
  float px,py,pz;
  float L, X, zeta, beta;

  //the captureLive cycle will do the following:
  // 1) Check the cube state 
  // 2) Check to see if message arrived in mailbox; if so, execute
  // 3) Grab the most recent ptu-state and post it

  // Compute process rate first
  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);
  this->cycleAvg = (1/(currenttime-lasttime) + this->cycleAvg*cycleCount)/(cycleCount+1);
  this->cycleCount++;
  if(this->console)
    {
      cotk_printf(this->console, "%cycle%", A_NORMAL, "%03.2f", 1/(currenttime - lasttime));
      cotk_printf(this->console, "%cycleavg%", A_NORMAL, "%03.2f", this->cycleAvg);
    }
  this->lasttime = this->currenttime;

  // Only check state for amtecptu powercube
  if(this->sensorId==SENSNET_MF_PTU)
    {
      if(this->healthCount % 100 == 0)
	{
	  // Health check on PAN module
	  if(this->amtecptu->CheckCubeState(AMTEC_MODULE_PAN) < 0)
	    {
	      ERROR("Cube state bad! shutting down now!");
	      if(this->amtecptu->Shutdown() < 0)
		{
		  MSG("exiting un-cleanly...");  
		  ERROR("PAN module is bad!");
		  return -1;
		}
	      ERROR("PTU has been shutdown. ");
	      return -1;
	    }  
	  
	  // Health check on Tilt module
	  if(this->amtecptu->CheckCubeState(AMTEC_MODULE_TILT) < 0) 
	    {
	      ERROR("Cube state bad! shutting down now!");
	      if(this->amtecptu->Shutdown() < 0)
		{
		  MSG("exiting un-cleanly...");  
		  ERROR("TILT module is bad!");
		  return -1;
		}
	      ERROR("PTU has been shutdown. ");
	      return -1;
	    }	  
	  this->healthCount++;	  
	}
    }
  

  // Check if message is in the mailbox
  if(this->m_skynet->is_msg(this->ptuCommandSocket))
    {
      this->ptuCommandReceived = this->m_skynet->get_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

      if(this->ptuCommandReceived>0)
	{
	  // only process the message if the command type is defined
	  if(ptuCommand.type==LINEOFSITE)
	    {
	      blob = this->blob;
	      
	      blob->blobType = SENSNET_PTU_STATE_BLOB;
	      blob->version = PTU_STATE_BLOB_VERSION;
	      blob->sensorId = this->sensorId;
	      blob->scanId = this->scanId;
	      blob->timestamp = this->scanTime;
	      blob->state = this->state;
	      
	      // just need the transforms
	      
	      // Tool to PTU transform
	      memcpy(blob->tool2ptu, this->tool2ptu, sizeof(this->tool2ptu));
	      mat44f_inv(blob->ptu2tool, blob->tool2ptu);
	      
	      // PTU to vehicle transform
	      memcpy(blob->ptu2veh, this->ptu2veh, sizeof(this->ptu2veh));
	      mat44f_inv(blob->veh2ptu, blob->ptu2veh);
	      
	      // Vehicle to local transform
	      pose.pos = vec3_set(blob->state.localX,
				  blob->state.localY,
				  blob->state.localZ);
	      pose.rot = quat_from_rpy(blob->state.localRoll,
				       blob->state.localPitch,
				       blob->state.localYaw);  
	      pose3_to_mat44f(pose, blob->veh2loc);  
	      mat44f_inv(blob->loc2veh, blob->veh2loc);	
	      
	      PTUStateBlobLocalToVehicle(blob, 
					 ptuCommand.localx, ptuCommand.localy, ptuCommand.localz,
					 &vx, &vy, &vz);
	      
	      PTUStateBlobVehicleToPTU(blob,
				       vx,vy,vz,
				       &px, &py, &pz);

	      L = sqrt(pow(px,2)+pow(py,2));

	      if(sensorId==SENSNET_LF_PTU)		
		X = sqrt(pow(L,2)+pow(pz + DP_BASE_TO_TILTAXIS,2));
	      else
		X = sqrt(pow(L,2)+pow(pz + AMTEC_BASE_TO_TILTAXIS,2));

	      zeta = atan2(pz, L);
	      beta = 0;
	      
	      MSG("Commanded pose in PTU frame is: %f %f %f", px, py, pz);
	      
	      if(this->sensorId==SENSNET_LF_PTU)
		{
		  this->dp_command.pan = (atan2(py, px))*180/M_PI; //must be in degrees 
		  this->dp_command.tilt = -(zeta+beta)*180/M_PI + this->inittilt;
		  this->dp_command.panspeed = ptuCommand.panspeed;
		  this->dp_command.tiltspeed = ptuCommand.tiltspeed;

		  if(this->directpercept->ProcessMessage(this->dp_command)<0)
		    {
		      ERROR("Could not process command.");	 
		      return -1;
		    }
		}
	      else
		{
		  this->command.pan = (atan2(py, px))*180/M_PI; //must be in degrees 
		  this->command.tilt = -(zeta+beta)*180/M_PI + this->inittilt;
		  this->command.panspeed = ptuCommand.panspeed;
		  this->command.tiltspeed = ptuCommand.tiltspeed;

		  if(this->amtecptu->ProcessMessage(this->command)<0)
		    {
		      ERROR("Could not process command.");	 
		      return -1;
		    }
		}
	    }
	  else if(ptuCommand.type==RAW)
	    {
	      if(this->sensorId==SENSNET_LF_PTU)
		{
		  this->dp_command.pan = ptuCommand.pan + this->initpan; 
		  this->dp_command.tilt = ptuCommand.tilt + this->inittilt;
		  this->dp_command.panspeed = ptuCommand.panspeed;
		  this->dp_command.tiltspeed = ptuCommand.tiltspeed;

		  //now send the command to the unit
		  if(this->directpercept->ProcessMessage(this->dp_command)<0)
		    {
		      ERROR("Could not process command.");	 
		      return -1;
		    }
		}
	      else
		{
		  this->command.pan = ptuCommand.pan + this->initpan; 
		  this->command.tilt = ptuCommand.tilt + this->inittilt;
		  this->command.panspeed = ptuCommand.panspeed; //the nominal speed of panning
		  this->command.tiltspeed = ptuCommand.tiltspeed;  //the nominal speed of tilting
		  
		  //now send the command to the unit
		  if(this->amtecptu->ProcessMessage(this->command)<0)
		    {
		      ERROR("Could not process command.");	 
		      return -1;
		    }
		}
	    }	     
	}
    }
  
  
  // Now grab current pan and tilt angles (deg)
  if(this->sensorId==SENSNET_LF_PTU)
    {
      if(this->directpercept->GetPanTiltPos(&this->currpan, &this->currtilt)<0)
	{
	  ERROR("Could not get pan-tilt position");
	  ERROR("Should shut down now!");
	  return -1;
	}
      if(this->directpercept->GetPanTiltSpeed(&this->currpanspeed, &this->currtiltspeed)<0)
	{
	  ERROR("Could not get pan-tilt velocity");
	  ERROR("Should shut down now!");
	  return -1;
	}
    }
  else
    {
      if(this->amtecptu->GetPanTiltPos(&this->currpan, &this->currtilt)<0)
	{
	  ERROR("Could not get pan-tilt position");
	  ERROR("Should shut down now!");
	  return -1;
	}
      if(this->amtecptu->GetPanTiltVel(&this->currpanspeed, &this->currtiltspeed)<0)
	{
	  ERROR("Could not get pan-tilt velocity");
	  ERROR("Should shut down now!");
	  return -1;
	}
    }
  
  this->pandelta = this->currpan - this->initpan; //deg 
  this->tiltdelta = this->currtilt - this->inittilt; //deg

  // Update the transformations
  float theta, phi;
  theta = DTOR(this->pandelta); //make this radians
  phi = DTOR(this->tiltdelta);  //make this radians

  this->tool2ptu[0][0] = cos(theta)*cos(phi);
  this->tool2ptu[0][1] = -sin(theta);
  this->tool2ptu[0][2] = cos(theta)*sin(phi);
  this->tool2ptu[0][3] = 0; 

  this->tool2ptu[1][0] = sin(theta)*cos(phi);
  this->tool2ptu[1][1] = cos(theta);
  this->tool2ptu[1][2] = sin(theta)*sin(phi);
  this->tool2ptu[1][3] = 0; 

  this->tool2ptu[2][0] = -sin(phi);
  this->tool2ptu[2][1] = 0;
  this->tool2ptu[2][2] = cos(phi);
  this->tool2ptu[2][3] = 0; 

  this->tool2ptu[3][0] = 0;
  this->tool2ptu[3][1] = 0;
  this->tool2ptu[3][2] = 0;
  this->tool2ptu[3][3] = 1;
  

  this->scanId += 1;
  this->scanTime = DGCgettime();
  
  // Get the matching state data
  if (this->getState(this->scanTime) != 0)
    return MSG("unable to get state; ignoring scan");

  // Update the display
  if (this->console)
    {
      cotk_printf(this->console, "%capid%", A_NORMAL, "%5d",
		  this->scanId);

      if(this->options.use_ladar_flag)
	cotk_printf(this->console, "%ladarcapcount%", A_NORMAL, 
		    "%d", this->ladarfeeder->scanId);
      
      cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
		  fmod((double) this->state.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%spos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  this->state.localX, this->state.localY, this->state.localZ);
      cotk_printf(this->console, "%srot%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  this->state.localRoll*180/M_PI,
		  this->state.localPitch*180/M_PI,
		  this->state.localYaw*180/M_PI);
      //cotk_printf(this->console, "%paninit%", A_NORMAL, "%+03.2f",this->initpan);
      cotk_printf(this->console, "%panspeed%", A_NORMAL, "%+03.2f",(double)(this->currpanspeed));
      //cotk_printf(this->console, "%tiltinit%", A_NORMAL, "%+03.2f",(double)(this->inittilt));
      cotk_printf(this->console, "%tiltspeed%", A_NORMAL, "%+03.2f",(double)(this->currtiltspeed));
      cotk_printf(this->console, "%pandelta%", A_NORMAL, "%+03.2f",(double)(this->pandelta));
      cotk_printf(this->console, "%tiltdelta%", A_NORMAL, "%+03.2f",(double)(this->tiltdelta));
      
      cotk_printf(this->console, "%pancommand%", A_NORMAL, "%+03.2f",(double)(this->command.pan));
      cotk_printf(this->console, "%tiltcommand%", A_NORMAL, "%+03.2f",(double)(this->command.tilt));
      cotk_printf(this->console, "%panspeedcommand%", A_NORMAL, "%+03.2f",(double)(this->command.panspeed));
      cotk_printf(this->console, "%tiltspeedcommand%", A_NORMAL, "%+03.2f",(double)(this->command.tiltspeed));
     
    }
  
  return 0;
}



// Capture a scan 
int PTUfeeder::captureSim()
{  
  pose3_t pose;
  PTUStateBlob *blob;
  float vx,vy,vz;
  float px,py,pz;
  float L, X, zeta, beta;
  float dT;
  int sign;

  //the captureSim cycle will do the following:
  // 1) Check to see if message arrived in mailbox; if so, simulat an execution
  // 2) Simulate the most recent ptu-state and post it

  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);
  this->cycleAvg = (1/(currenttime-lasttime) + cycleAvg*cycleCount)/(cycleCount+1);
  this->cycleCount++;
  if(this->console)
    {
      cotk_printf(this->console, "%cycle%", A_NORMAL, "%03.2f", 1/(currenttime - lasttime));
      cotk_printf(this->console, "%cycleavg%", A_NORMAL, "%03.2f", this->cycleAvg);
    }
  this->lasttime = this->currenttime;

  // Simulate 75Hz
  usleep(13000);

  // Check if message is in the mailbox
  if(this->m_skynet->is_msg(this->ptuCommandSocket))
    {
      this->ptuCommandReceived = this->m_skynet->get_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

      if(this->ptuCommandReceived>0)
	{
	  // only process the message if the command type is defined
	  if(ptuCommand.type==LINEOFSITE)
	    {
	      blob = this->blob;
	      
	      blob->blobType = SENSNET_PTU_STATE_BLOB;
	      blob->version = PTU_STATE_BLOB_VERSION;
	      blob->sensorId = this->sensorId;
	      blob->scanId = this->scanId;
	      blob->timestamp = this->scanTime;
	      blob->state = this->state;
	      
	      // just need the transforms
	      
	      // Tool to PTU transform
	      memcpy(blob->tool2ptu, this->tool2ptu, sizeof(this->tool2ptu));
	      mat44f_inv(blob->ptu2tool, blob->tool2ptu);
	      
	      // PTU to vehicle transform
	      memcpy(blob->ptu2veh, this->ptu2veh, sizeof(this->ptu2veh));
	      mat44f_inv(blob->veh2ptu, blob->ptu2veh);
	      
	      // Vehicle to local transform
	      pose.pos = vec3_set(blob->state.localX,
				  blob->state.localY,
				  blob->state.localZ);
	      pose.rot = quat_from_rpy(blob->state.localRoll,
				       blob->state.localPitch,
				       blob->state.localYaw);  
	      pose3_to_mat44f(pose, blob->veh2loc);  
	      mat44f_inv(blob->loc2veh, blob->veh2loc);	
	      
	      PTUStateBlobLocalToVehicle(blob, 
					 ptuCommand.localx, ptuCommand.localy, ptuCommand.localz,
					 &vx, &vy, &vz);
	      
	      PTUStateBlobVehicleToPTU(blob,
				       vx,vy,vz,
				       &px, &py, &pz);

	      L = sqrt(pow(px,2)+pow(py,2));
	      X = sqrt(pow(L,2)+pow(pz+AMTEC_BASE_TO_TILTAXIS,2));
	      zeta = atan2(pz+AMTEC_BASE_TO_TILTAXIS, L);
	      beta = asin(AMTEC_TILTAXIS_TO_TOOLFRAME/X);	      
	      
	      //MSG("Commanded pose in PTU frame is: %f %f %f", px, py, pz);
	      
	      this->command.pan = (atan2(py, px))*180/M_PI; //must be in degrees 
	      this->command.tilt = -(zeta+beta)*180/M_PI + this->inittilt;
	      this->command.panspeed = ptuCommand.panspeed;
	      this->command.tiltspeed = ptuCommand.tiltspeed;

	    }
	  else if(ptuCommand.type==RAW)
	    {
	      this->command.pan = ptuCommand.pan; 
	      this->command.tilt = ptuCommand.tilt;
	      this->command.panspeed = ptuCommand.panspeed; //the nominal speed of panning
	      this->command.tiltspeed = ptuCommand.tiltspeed;  //the nominal speed of tilting

	    }	     
	}
    }


  this->scanId += 1;
  this->scanTime = DGCgettime();
  
  // Get the matching state data
  if (this->getState(this->scanTime) != 0)
    return MSG("unable to get state; ignoring scan");

  // Simulate !!
  if(this->command.tiltspeed > AMTEC_MAXSPEED || this->command.tiltspeed < 0)
    {
      WARN("The commanded tiltspeed is too fast. Using the nominal speed.");
      this->command.tiltspeed = (float)this->initTiltSpeed;
    }

  if(this->command.panspeed > AMTEC_MAXSPEED || this->command.panspeed < 0)
    {
      WARN("The requested panspeed is too fast. Using the nominal speed.");
      this->command.panspeed = (float)this->initPanSpeed;
    }

  if(this->command.pan > AMTEC_MAXPAN)
    this->command.pan = AMTEC_MAXPAN;

  if(this->command.pan < AMTEC_MINPAN)
    this->command.pan = AMTEC_MINPAN;

  if(this->command.tilt > AMTEC_MAXTILT)
    this->command.tilt = AMTEC_MAXTILT;
 
  if(this->command.tilt < AMTEC_MINTILT)
    this->command.tilt = AMTEC_MINTILT;

  dT = (float)(this->scanTime - this->last_scanTime)/1000000;
  
  if(fabs(this->command.pan - this->currpan) > 0.025)
    {
      if(this->command.pan < this->currpan)
	sign = -1;
      else
	sign = 1;
      
      this->currpan = sign*(this->command.panspeed)*dT + this->lastpan;
    }
  else
    this->command.panspeed = 0;

  if(fabs(this->command.tilt - this->currtilt) > 0.025)
    {
      if(this->command.tilt < this->currtilt)
	sign = -1;
      else
	sign = 1;

      this->currtilt = sign*(this->command.tiltspeed)*dT + this->lasttilt;
    }
  else
    this->command.tiltspeed = 0;
    
  this->currpanspeed = this->command.panspeed;
  this->currtiltspeed = this->command.tiltspeed;

  this->pandelta = this->currpan - this->initpan; //deg 
  this->tiltdelta = this->currtilt - this->inittilt; //deg

  // Update the transformations
  float theta, phi, b, a;
  theta = DTOR(this->pandelta); //make this radians
  phi = DTOR(this->tiltdelta);  //make this radians
  a = AMTEC_BASE_TO_TILTAXIS;
  b = AMTEC_TILTAXIS_TO_TOOLFRAME; 

  this->tool2ptu[0][0] = cos(theta)*cos(phi);
  this->tool2ptu[0][1] = -sin(theta);
  this->tool2ptu[0][2] = cos(theta)*sin(phi);
  this->tool2ptu[0][3] = -b*cos(theta)*sin(phi);

  this->tool2ptu[1][0] = sin(theta)*cos(phi);
  this->tool2ptu[1][1] = cos(theta);
  this->tool2ptu[1][2] = sin(theta)*sin(phi);
  this->tool2ptu[1][3] = -b*sin(theta)*sin(phi);

  this->tool2ptu[2][0] = -sin(phi);
  this->tool2ptu[2][1] = 0;
  this->tool2ptu[2][2] = cos(phi);
  this->tool2ptu[2][3] = -b*cos(phi)-a;

  this->tool2ptu[3][0] = 0;
  this->tool2ptu[3][1] = 0;
  this->tool2ptu[3][2] = 0;
  this->tool2ptu[3][3] = 1;
  
  // Update the display
  if (this->console)
    {
      cotk_printf(this->console, "%capid%", A_NORMAL, "%5d",
		  this->scanId);
      
      cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
		  fmod((double) this->state.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
		  this->state.localX, this->state.localY, this->state.localZ);
      cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
		  this->state.localRoll*180/M_PI,
		  this->state.localPitch*180/M_PI,
		  this->state.localYaw*180/M_PI);
      //cotk_printf(this->console, "%paninit%", A_NORMAL, "%+03.2f",(double)(this->initpan));
      cotk_printf(this->console, "%panspeed%", A_NORMAL, "%+03.2f",(double)(this->currpanspeed));
      //cotk_printf(this->console, "%tiltinit%", A_NORMAL, "%+03.2f",(double)(this->inittilt));
      cotk_printf(this->console, "%tiltspeed%", A_NORMAL, "%+03.2f",(double)(this->currtiltspeed));
      cotk_printf(this->console, "%pandelta%", A_NORMAL, "%+03.2f",(double)(this->pandelta));
      cotk_printf(this->console, "%tiltdelta%", A_NORMAL, "%+03.2f",(double)(this->tiltdelta));
      
      cotk_printf(this->console, "%pancommand%", A_NORMAL, "%+03.2f",(double)(this->command.pan));
      cotk_printf(this->console, "%tiltcommand%", A_NORMAL, "%+03.2f",(double)(this->command.tilt));
      cotk_printf(this->console, "%panspeedcommand%", A_NORMAL, "%+03.2f",(double)(this->command.panspeed));
      cotk_printf(this->console, "%tiltspeedcommand%", A_NORMAL, "%+03.2f",(double)(this->command.tiltspeed));
      
    }
  
  this->last_scanTime = this->scanTime;
  this->lastpan = this->currpan;
  this->lasttilt = this->currtilt;

  return 0;
}


// Initialize sensnet
int PTUfeeder::initSensnet(const char *configPath)
{    
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(PTUStateBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(PTUStateBlob),
                 512 - sizeof(PTUStateBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (PTUStateBlob*) valloc(sizeof(PTUStateBlob));

  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->sensorId) != 0)
    return ERROR("unable to connect to sensnet");
    
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  // Initialize logging
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);
        
    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);

    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             configPath, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
  }

  return 0;
}


// Finalize sensnet
int PTUfeeder::finiSensnet()
{  
  if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }

  sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;

  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish data
int PTUfeeder::writeSensnet()
{
  pose3_t pose;
  PTUStateBlob *blob;

  blob = this->blob;

  // Construct the blob header
  blob->blobType = SENSNET_PTU_STATE_BLOB;
  blob->version = PTU_STATE_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->scanId = this->scanId;
  blob->timestamp = this->scanTime;
  blob->state = this->state;

  // sensor-to-PTU transform
  memcpy(blob->tool2ptu, this->tool2ptu, sizeof(this->tool2ptu));
  mat44f_inv(blob->ptu2tool, blob->tool2ptu);
  
  // PTU to vehicle transform
  memcpy(blob->ptu2veh, this->ptu2veh, sizeof(this->ptu2veh));
  mat44f_inv(blob->veh2ptu, blob->ptu2veh);

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);  
  mat44f_inv(blob->loc2veh, blob->veh2loc);
  
  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Copy the angle data
  blob->currpan = this->pandelta; //this accounts for initial offset
  blob->currtilt = this->tiltdelta; //this accounts for initial offset
  blob->currpanspeed = this->currpanspeed;
  blob->currtiltspeed = this->currtiltspeed;

  DGClockMutex(&ptuStateMutex);
  memcpy(&this->m_blob, blob, sizeof(*blob));
  DGCunlockMutex(&ptuStateMutex);

  // Write blob
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, this->sensorId, SENSNET_PTU_STATE_BLOB,
                    this->scanId, sizeof(*blob), blob) != 0)
    return ERROR("unable to write blob");
  
  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, blob->timestamp,
                          this->sensorId, SENSNET_PTU_STATE_BLOB,
                          this->scanId, sizeof(*blob), blob) != 0)
      return ERROR("unable to write blob");
  }


  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag) && this->console)
  {    
    // Keep some stats on logging
    this->logCount += 1;
    this->logSize += sizeof(*blob);
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->logCount, this->logSize / 1024 / 1024);    
  }

  if(!this->options.sim_flag)
    {
      if(this->options.use_ladar_flag)
	{
	  if (this->ladarfeeder->sensnet_log && (this->enableLadarLog || this->options.always_log_flag) && this->console)
	    {    
	      // Keep some stats on logging
	      if(this->options.use_ladar_flag && this->options.enable_ladar_log_flag)
		cotk_printf(this->console, "%ladarlogsize%", A_NORMAL, "%df %dMb",
			    this->ladarfeeder->logCount, this->ladarfeeder->logSize / 1024 / 1024);
	    }
	}
    }
  
  return 0;
}


// Get the predicted vehicle state
int PTUfeeder::getState(uint64_t timestamp)
{
  int blobId;
  
  // Default to all zeros in state
  memset(&this->state, 0, sizeof(this->state));
  
  // Get the current state value
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state data");
  if (blobId < 0)
    return ERROR("state is invalid");
  
  return 0;
}


// Get the process state
int PTUfeeder::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = this->logSize / 1024;
  response.healthStatus = 2;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  this->enableLog = request.enableLog;
  this->enableLadarLog = request.enableLog;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


// Process a scan
int PTUfeeder::process()
{
  // Processing currently does nothing for a laser scan  
  return 0;
}

/// Start ladarfeeder thread
/// -- this will start ladarfeeder in serial mode and only for the PTU ladar (very specific)
void PTUfeeder::startLadarFeederThread()
{
  PTUStateBlob ptublob;
    
  // Start processing
  while (!this->quit)
  {
    // only want to start taking ladar scan data once the PTU has initialized
    if(this->scanId>0)
      {
	if (ladarfeeder->captureLive() != 0)
	  break;    
	
	// If paused, give up our time slice.
	if (this->pause)
	  {      
	    usleep(0);
	    continue;
	  }
	
	DGClockMutex(&ptuStateMutex);
	memcpy(&ptublob,&m_blob, sizeof(m_blob));
	DGCunlockMutex(&ptuStateMutex);
	
	// Publish data using ptublob to get the right transforms
	if (ladarfeeder->writeSensnet(ptublob, this->enableLadarLog) != 0)
	  break;
      }
    else
      usleep(10);
       
  }
  
  // Clean up
  ladarfeeder->finiSensnet(); 
  ladarfeeder->finiLive();

  delete ladarfeeder;
  printf("program exited cleanly\n");

  return;
}



// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"PTUfeeder $SKYNET$: %spread%                                          \n"
"Sensor: %sensor%                                                      \n"
"----------------------------------------------------------------------\n"
"PTU STATE                        |  LADAR  (%ladarmode%)              \n"
"Pandelta   (deg)  : %pandelta%   |  Scan:  %ladarcapcount%            \n"
"Tiltdelta  (deg)  : %tiltdelta%  |  Log :  %ladarlogname%             \n"
"Pan-rate   (deg/s): %panspeed%   |  Size:  %ladarlogsize%             \n"
"Tilt-rate  (deg/s): %tiltspeed%  |------------------------------------\n"
"---------------------------------|  RECEIVED COMMAND                  \n"
"STATS                            |  Pan     (deg): %pancommand%       \n"
"Capture (%mode%)                 |  Tilt    (deg): %tiltcommand%      \n"
"Port : %port%                    |  dPan  (deg/s): %panspeedcommand%  \n"
"Scan : %capid%                   |  dTilt (deg/s): %tiltspeedcommand% \n"
"Log  : %log%                     |------------------------------------\n"
"---------------------------------|  ALICE STATE                       \n" 
"PROCESS RATE                     |  Time    : %stime%                 \n"
"Rate: %cycle%      Hz            |  Pos     : %spos%                  \n" 
"Avg : (%cycleavg%) Hz            |  Rot     : %srot%                  \n"
"----------------------------------------------------------------------\n" 
"%stderr%                                                              \n" 
"%stderr%                                                              \n" 
"                                                                      \n" 
"[%PAUSE%|%QUIT%|%LOG%]                                                \n"; 

// Initialize console display
int PTUfeeder::initConsole()
{
  char filename[1024];
    
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);
    
  // Initialize the display
  snprintf(filename, sizeof(filename), "%s/%s.msg",
           this->options.log_path_arg, sensnet_id_to_name(this->sensorId));
  if (cotk_open(this->console, filename) != 0)
    return -1;
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%port%", A_NORMAL, "%s", this->port);
    
  cotk_printf(this->console, "%logname%", A_NORMAL, this->logName);

  if(this->options.sim_flag)
    cotk_printf(this->console, "%mode%", A_NORMAL, "sim " );
  else
    {
      cotk_printf(this->console, "%mode%", A_NORMAL, "live " );

      if(this->options.use_ladar_flag)
	{
	  cotk_printf(this->console, "%ladarmode%", A_NORMAL, "live ");
	  cotk_printf(this->console, "%ladarcapcount%", A_NORMAL, "%d", this->ladarfeeder->scanId);
	  cotk_printf(this->console, "%ladarlogname%", A_NORMAL, "%s", this->ladarfeeder->logName);
	}
    }

  return 0;
}


// Finalize console display
int PTUfeeder::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int PTUfeeder::onUserQuit(cotk_t *console, PTUfeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int PTUfeeder::onUserPause(cotk_t *console, PTUfeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events; occurs in sparrow thread
int PTUfeeder::onUserLog(cotk_t *console, PTUfeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  self->enableLadarLog = !self->enableLadarLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  int status;
  PTUfeeder *feeder;

  // Create feeder
  feeder = new PTUfeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  if(feeder->options.sim_flag)
    {
      // Initialize for live capture
      if (feeder->initSim(feeder->defaultConfigPath) != 0)
	return -1;      
    }
  else
    {
      // Initialize for live capture
      if (feeder->initLive(feeder->defaultConfigPath) != 0)
	return -1;

      if(feeder->options.use_ladar_flag)
	{
	  // Create feeder
	  feeder->ladarfeeder = new PTULadarFeeder(feeder->options.enable_ladar_log_flag);
	  assert(feeder->ladarfeeder);
	  
	  // Initialize for live capture
	  if (feeder->ladarfeeder->initLive() != 0)
	    {
	      MSG("could not initialize live capture for ladarfeeder!");
	      return -1;
	    }
	  
	  // Initialize sensnet
	  if (feeder->ladarfeeder->initSensnet(feeder->ladarfeeder->defaultConfigPath) != 0)
	    {
	      MSG("could not initialize sensnet for ladarfeeder!");
	      return -1;
	    }
	  
	  DGCstartMemberFunctionThread(feeder, &PTUfeeder::startLadarFeederThread);
	}

    }
  
  // Initialize sensnet
  if (feeder->initSensnet(feeder->defaultConfigPath) != 0)
    return -1;

  // Initialize console
  if (!feeder->options.disable_console_flag)
  {
    if (feeder->initConsole() != 0)
      return -1;
  }

  feeder->startTime = DGCgettime();
  
  // Start processing
  while (!feeder->quit)
  {       
    usleep(10);

    // If paused, give up our time slice.
    if (feeder->pause)
    {
      usleep(0);
      continue;
    }

    // Do heartbeat occasionally
    if (feeder->capCount % 15 == 0)
      feeder->getProcessState();

    if(feeder->mode==PTUfeeder::modeSim)
      {
	if(feeder->captureSim() != 0)
	  break;
      }
    else
      {
	if (feeder->captureLive() < 0)
	{
	  MSG("quitting main loop of PTUfeeder!");
 	  feeder->quit = true;
	  sleep(10);
	  break;
	}		
      }

    // Compute some diagnostics
    feeder->capCount += 1;

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);
    
    // Process one scan
    status = feeder->process();
    if (status != 0)
      break;

    // Publish data
    if (feeder->writeSensnet() != 0)
      break;

  }  

  // give other thread enough time to quit
  sleep(2);
  
  // Clean up  
  MSG("exiting sensnet");
  feeder->finiSensnet();
  
  if(feeder->mode==PTUfeeder::modeLive)
    {
      MSG("finiLive() being called.");
      feeder->finiLive();
    }

  MSG("finishing console.");
  feeder->finiConsole();

  cmdline_parser_free(&feeder->options);  
  delete feeder;
  
  MSG("program exited cleanly");
  
  return 0;
}
