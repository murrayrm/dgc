/*
 * $Id: directpercept.cc
 * Author: Jeremy Ma (adapted from source code provided by Directed Perception)
 
 This driver communicates with the PTU via serial interface

*/

#include "directpercept.hh"

// ===============================
// Constructor for Pan Tilt Unit
// ===============================
DirectPercept::DirectPercept(int panSpeed, int tiltSpeed)
{
  memset(this, 0, sizeof(this));

  if(panSpeed > DP_MAXSPEED || panSpeed <= 0)
    {
      MSG("You're requested panning-speed is way too fast (or invalid). Using 15deg/s instead");
      this->m_panspeed = 15; // 15 deg/s just seems to be a safe speed; 
    }
  else
    this->m_panspeed = panSpeed;

  if(tiltSpeed > DP_MAXSPEED || tiltSpeed <= 0)
    {
      MSG("You're requested tilting-speed is way too fast (or invalid). Using 15deg/s instead");
      this->m_tiltspeed = 15; // 15 deg/s just seems to be a safe speed; 
    }
  else
    this->m_tiltspeed = tiltSpeed;

  return;

}


// ===============================
// Default Constructor for Pan Tilt Unit
// ===============================
DirectPercept::DirectPercept() 
{
  memset(this, 0, sizeof(this));

  MSG("no speeds specified. Using safe speeds of 15deg/s");
  this->m_panspeed = 15; //deg/s
  this->m_tiltspeed = 15; //deg/s

  return;

}


// ===============================
// Destructor for Pan Tilt Unit
// ===============================
DirectPercept::~DirectPercept() 
{  
  return;
}


// ===============================
// Setup (Connect to CAN interface)
// ===============================
int DirectPercept::Setup(char* serialPort)
{
  char status; 
  short int pd, td;

  // Initialize serial connection
  MSG("opening DirectedPerception PTU on %s", serialPort);

  this->m_ptu = open_host_port(serialPort);

  if (this->m_ptu == PORT_NOT_OPENED)
    return ERROR("unable to find PTU on %s", serialPort);

  set_mode(DEFAULTS, RESTORE_FACTORY_SETTINGS);

  if (reset_PTU_parser(5000) != PTU_OK)
    return ERROR("unable to initialize PTU");
  
  if (set_mode(POSITION_LIMITS_MODE,OFF_MODE) != PTU_OK)
    return ERROR("unable to disable limits");  

  // Grab pan-tilt resolution
  this->m_ptu_pan_res = (float) get_current(PAN, RESOLUTION) / 3600 / 60 * M_PI / 180;
  this->m_ptu_tilt_res = (float) get_current(TILT, RESOLUTION) / 3600 / 60 * M_PI / 180;

  // Reset
  //if(this->Reset() < 0)
  //return ERROR("Reset failed.");

  // Home
  if(this->Home() < 0)
    return ERROR("Home failed.");


  // Set Speed values
 pd = (int) (DTOR(this->m_panspeed) / this->m_ptu_pan_res);

  // Set Acceleration value
  pd = (int) (DTOR(DP_PAN_ACCEL) / this->m_ptu_pan_res);
  td = (int) (DTOR(DP_TILT_ACCEL) / this->m_ptu_tilt_res);
  if ((status = set_desired(PAN, ACCELERATION, &pd, ABSOLUTE)) == TRUE)
    return ERROR("PAN_SET_ACCELERATION failed with status: 0x%x", status);

  if ((status = set_desired(TILT, ACCELERATION, &td, ABSOLUTE)) == TRUE)
    return ERROR("TILT_SET_ACCELERATION failed with status: 0x%x", status);

  return 0;
}


// ===============================
// Reset (Clear Error State)
// ===============================
int DirectPercept::Reset()
{
  char status;
  if ( (status = reset_ptu())  == TRUE)   
    return ERROR( "Could not Reset(); status: 0x%x", status);    

  return(0);
}

// ===============================
// Begin homing procedure back to initial pose
// ===============================
int DirectPercept::Home()
{
  char status;
  short int pd = 0;
  status = set_desired(PAN, POSITION, &pd, ABSOLUTE);
  if(status != PTU_OK)
    return ERROR("Could not home pan position.");

  status = set_desired(TILT, POSITION, &pd, ABSOLUTE);
  if(status != PTU_OK)
    return ERROR("Could not home tilt position.");

  return(0);
}


// ===============================
// Halt (Stop Immediately)
// ===============================
int DirectPercept::Halt()
{
  unsigned char status;
  if ((status = halt(ALL) ) == TRUE)
    return ERROR("Halt error; status: 0x%x", status);
    
  return(0);
}


// ===============================
// Shutdown (Close all connections)
// ===============================
int DirectPercept::Shutdown()
{
  // stop the unit
  if(Halt())
    WARN("Halt() failed.");

  // maybe return it to home
  if(Home())
    WARN("Home() failed.");

  close_host_port(this->m_ptu);

  return(0);
}


// ===============================
// Get PTU pose
// ===============================
int DirectPercept::GetPanTiltPos(float* pan, float* tilt)
{
  float tmp_pan, tmp_tilt;
  tmp_pan = get_current(PAN, POSITION) * this->m_ptu_pan_res;
  tmp_tilt = get_current(TILT, POSITION) * this->m_ptu_tilt_res;

  *pan = -RTOD(tmp_pan); //need to invert pan value;
  *tilt = RTOD(tmp_tilt);

  return(0);
}


// ===============================
// Get PTU Velocity
// ===============================
int DirectPercept::GetPanTiltSpeed(float* panspeed, float* tiltspeed)
{
  float tmp_panspeed, tmp_tiltspeed;
  tmp_panspeed = get_current(PAN, SPEED) * this->m_ptu_pan_res;
  tmp_tiltspeed = get_current(TILT, SPEED) * this->m_ptu_tilt_res;

  *panspeed =  RTOD(tmp_panspeed);
  *tiltspeed = RTOD(tmp_tiltspeed);

  return(0);
}


// ===============================
// Set Pan Position (specify in degrees) -- blocking!
// ===============================
int DirectPercept::SetPanPos(float currpan, float desiredpan)
{
  char status;
  short int pd;
  float pan;
  
  pan = -desiredpan; // need to invert pan value
  
  // check if already at pan position in degrees
  if(fabs(pan-currpan)<DP_PAN_THRESH)
    {
      MSG("pan already at desired position. Command not sent.");
      return(0);
    }
  
  if (pan < DP_MINPAN)
    pan = DP_MINPAN;
  if (pan > DP_MAXPAN)
    pan = DP_MAXPAN;
  
  pd = (int) (DTOR(pan) / this->m_ptu_pan_res);
  status = set_desired(PAN, POSITION, &pd, ABSOLUTE);
  if (status != PTU_OK)
    return ERROR("unable to set pan; status 0x%x", status);
  
  /*
  status = await_completion();
  if (status != PTU_OK)
    return ERROR("wait failed: status 0x%x", status);
  */

  return(0);
}


// ===============================
// Set Tilt Position (specify in degrees) -- blocking!
// ===============================
int DirectPercept::SetTiltPos(float currtilt, float desiredtilt)
{
  char status;
  short int td;
  float tilt;

  tilt = desiredtilt;

  // check if already at pan position in degrees
  if(fabs(tilt-currtilt)<DP_TILT_THRESH)
    {
      MSG("tilt already at desired position. Command not sent.");
      return(0);
    }

  if (tilt < DP_MINTILT)
    tilt = DP_MINTILT;
  if (TILT > DP_MAXTILT)
    tilt = DP_MAXTILT;
  
 td = (int) ( DTOR(tilt) / this->m_ptu_tilt_res);
 status = set_desired(TILT, POSITION, &td, ABSOLUTE);
 if (status != PTU_OK)
   return ERROR("unable to set tilt; status 0x%x", status);

/*
 status = await_completion();
 if (status != PTU_OK)
   return ERROR("wait failed: status 0x%x", status);
*/

  return(0);
}

// ===============================
// Set Pan Velocity (specify in deg/s)
// ===============================
int DirectPercept::SetPanSpeed(float currpanspeed, float desiredpanspeed)
{
  char status;
  short int pd;
  float panspeed;
  
  panspeed = desiredpanspeed;
  
  // check if already at pan position in degrees
  if(fabs(currpanspeed-panspeed)<DP_PAN_THRESH)
    {
      MSG("pan already at desired position. Command not sent.");
      return(0);
    }

  if (panspeed < 0) 
    panspeed = 15; 
  if (panspeed > DP_MAXSPEED)
    panspeed = DP_MAXSPEED;
  
 pd = (int) (DTOR(panspeed) / this->m_ptu_pan_res);
 status = set_desired(PAN, SPEED, &pd, ABSOLUTE);
 if (status != PTU_OK)
   return ERROR("unable to set pan; status 0x%x", status);

  return(0);
}


// ===============================
// Set Tilt Velocity (specify in deg/s) -- used only in VEL_MODE -- not yet tested
// ===============================
int DirectPercept::SetTiltSpeed(float currtiltspeed, float desiredtiltspeed)
{
  char status;
  short int td;
  float tiltspeed;
  
  tiltspeed = desiredtiltspeed;
  
  // check if already at pan position in degrees
  if(fabs(currtiltspeed-tiltspeed)<DP_TILT_THRESH)
    {
      MSG("tilt already at desired position. Command not sent.");
      return(0);
    }

  if (tiltspeed < 0)
    tiltspeed = 15;
  if (tiltspeed > DP_MAXSPEED)
    tiltspeed = DP_MAXSPEED;
  
  td = (int) (DTOR(tiltspeed) / this->m_ptu_tilt_res);
  status = set_desired(TILT, SPEED, &td, ABSOLUTE);
  if (status != PTU_OK)
    return ERROR("unable to set tilt; status 0x%x", status);

  return(0);
}


// ===============================
// Process Message commands
// ===============================
int DirectPercept::ProcessMessage(DPCommand command)
{
  float currpan, currtilt;
  float currpanspeed, currtiltspeed;
  
  // Grab current pan tilt values first
  if(GetPanTiltPos(&currpan, &currtilt) < 0)
    return ERROR("Get pan tilt pos failed!");
  if(GetPanTiltSpeed(&currpanspeed, &currtiltspeed) < 0)
    return ERROR("Get pan tilt pos failed!"); 

  // Set speed values
  if(SetPanSpeed(currpanspeed, command.panspeed) < 0)
    return ERROR("Set pan speed failed!");
  if(SetTiltSpeed(currtiltspeed, command.tiltspeed) < 0)
    return ERROR("Set tilt speed failed!");

  // Set new values
  if(SetPanPos(currpan, command.pan) < 0)
    return ERROR("Could not set pan!");
  if(SetTiltPos(currtilt, command.tilt) < 0)
    return ERROR("Could not set tilt!");  
  
  return 0;
  
}
