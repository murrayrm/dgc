
/*
 * $Id: amtecpowercube.cc
 * Author: Jeremy Ma (adapted from Brian Gerkey's driver for RS232)
 */
/*
The amtecpowercube driver controls the Amtec PowerCube Wrist,
a powerful pan-tilt unit that can, for example, carry a SICK laser

This driver communicates with the PowerCube via CAN interface,

The amtecpowercube driver supports both position and velocity control.  
For constant swiveling, the PowerCube works better under velocity control.

*/

#ifndef AMTEC_DRIVER_H
#define AMTEC_DRIVER_H


#include <stdio.h>
#include <ntcan/ntcan.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>

// possibly remove the following include files
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <termios.h>
#include <sys/time.h>
#include <netinet/in.h> 
#include <sys/ioctl.h>
#include <linux/serial.h>

#define AMTEC_DEFAULT_PORT "/dev/ttyUSB0"

#define AMTEC_SLEEP_TIME_USEC 20000

/* angular velocity used when in position control mode */
#define AMTEC_DEFAULT_SPEED_DEG_PER_SEC DTOR(40)

// start, end, and escape chars
#define AMTEC_STX                       0x02
#define AMTEC_ETX                       0x03
#define AMTEC_DLE                       0x10

// sizes
#define AMTEC_MAX_CMDSIZE               48

// module IDs -- module address 1 <= M <= 31
#define AMTEC_MODULE_TILT               13 // this is specific to the unit
#define AMTEC_MODULE_PAN                14 // this is specific to the unit

// command IDs
#define AMTEC_CMD_RESET                 0x00
#define AMTEC_CMD_HOME                  0x01
#define AMTEC_CMD_HALT                  0x02
#define AMTEC_CMD_SET_EXT               0x08
#define AMTEC_CMD_GET_EXT               0x0a
#define AMTEC_CMD_SET_MOTION            0x0b
#define AMTEC_CMD_SET_ISTEP             0x0d


// for setting/getting extended parameters, use parameter IDs (not motion IDs)
// parameter IDs
#define AMTEC_PARAM_ACT_POS             0x3c
#define AMTEC_PARAM_IPOL_POS            0x3e
#define AMTEC_PARAM_MIN_POS             0x45
#define AMTEC_PARAM_MAX_POS             0x46
#define AMTEC_PARAM_CUBESTATE           0x27
#define AMTEC_PARAM_MAXCURR             0x4c
#define AMTEC_PARAM_ACT_VEL             0x41
#define AMTEC_PARAM_IPOL_VEL            0x42
#define AMTEC_PARAM_SET_ACCEL           0x50
#define AMTEC_PARAM_SET_VEL             0x4f

// for setting motion commands, use motion IDs (not parameter IDs)
// motion IDs
#define AMTEC_MOTION_FRAMP               4
#define AMTEC_MOTION_FRAMP_ACK          14
#define AMTEC_MOTION_FSTEP_ACK          16
#define AMTEC_MOTION_FVEL_ACK           17

// module state bitmasks
#define AMTEC_STATE_ERROR               0x01
#define AMTEC_STATE_HOME_OK             0x02
#define AMTEC_STATE_HALTED              0x04
#define AMTEC_POWERFAULT                0x08
#define AMTEC_TOW_ERROR                 0x10
#define AMTEC_STATE_CPU_OVERLOAD        0x01000000
#define AMTEC_STATE_BEYOND_HARD         0x02000000
#define AMTEC_STATE_BEYOND_SOFT         0x04000000
#define AMTEC_STATE_POW_SETUP_ERR       0x08000000

#define MAXSPEED                        40   //(deg/sec) ; note that 80deg/s is freakishly fast; 150deg/s caused failure
#define MAXPAN                         140   //deg //this is due to the physical length of cable
#define MINPAN                        -140   //deg //this is due to the physical length of cable
#define MAXTILT                         40   //deg
#define MINTILT                        -85   //deg

#define PAN_ACCEL                       15   // deg/s/s ; play value for now
#define TILT_ACCEL                      15   // deg/s/s ; play value for now

#define BASE_TO_TILTAXIS                0.115 //m
#define TILTAXIS_TO_TOOLFRAME           0.071 //m

#define PAN_THRESH                      0.1
#define TILT_THRESH                     0.1

#define NUM_TRIES                       5

// degree/radian conversions
#define RTOD(r)   ((r) * 180 / M_PI)
#define DTOR(d)   ((d) * M_PI / 180)
#define NORMALIZE(z)   atan2(sin(z), cos(z))

// Error handling 
#ifndef USEFUL_MACROS
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#endif 

enum CONTROLMODE { POS_CONTROL, VEL_CONTROL};

struct Command
{
  float pan;
  float tilt;
  float panspeed;
  float tiltspeed;
};

class AmtecPowerCube
{

private:

  // CAN id for PAN-module handle ack
  int32_t pan_id_ack;

  // CAN id for PAN-module handle get
  int32_t pan_id_get;

  // CAN id for PAN-module handle put
  int32_t pan_id_put;

  // CAN id for TILT-module handle ack
  int32_t tilt_id_ack;

  // CAN id for TILT-module handle get
  int32_t tilt_id_get;

  // CAN id for TILT-module handle put
  int32_t tilt_id_put;

  // CAN Handle for receiving status messages for PAN-module
  int32_t rxhandle_pan;

  // CAN Handle for receiving status messages for TILT-module
  int32_t rxhandle_tilt;

  // CAN baud rate (2 is actually 500kbps)
  unsigned long baud;
  
  // CAN logical netnumber
  int net;

  // CAN mode for canOpen
  unsigned long mode; 

  // CAN maximum number of messages to transmit
  long txqueuesize;  

  // CAN maximum number of messages to receive
  long rxqueuesize;  

  // CAN timeout for transmit
  long txtimeout;   

  // CAN timeout for receiving data
  long rxtimeout;   

  // CAN device serial number
  char serialNumber[40];

  // bookkeeping
  bool cmd_blocking;
  int minpan, maxpan;
  int mintilt, maxtilt;
  int nominalPanSpeed; //pan speed
  int nominalTiltSpeed; //tilt speed

  CONTROLMODE controlmode;
  
  // low-level methods to interact with the device
  int SendCommand(int32_t canid, unsigned char* cmd, size_t len);
  int ReadAnswer(int32_t canid, unsigned char* rcv, size_t len, CMSG* rmsg);
  int GetFloatParam(int id, int param, float* val);
  int GetUint32Param(int id, int param, unsigned int* val);
  int SetFloatParam(int id, int param, float val);
  void SetPanSpeed(int panspeed); //sets the nominal speed of panning for POS_CONTROL mode
  void SetTiltSpeed(int tiltspeed); //sets the nominal speed of tilting for POS_CONTROL mode

  // data (de)marshalling helpers
  // NOTE: these currently assume little-endianness, which is NOT
  //       portable (but works fine on x86).
  float BytesToFloat(CMSG* rmsg);
  unsigned int BytesToUint32(CMSG* rmsg);
  void FloatToBytes(unsigned char *bytes, float f);
  void Uint16ToBytes(unsigned char *bytes, unsigned short s);

  float lastpanspeed, lasttiltspeed;
  
public:    
  // Constructor
  AmtecPowerCube(int panSpeedLimit, int tiltSpeedLimit, bool blocking);

  // Constructor -- default
  AmtecPowerCube();

  // Destructor
  ~AmtecPowerCube();
  
  // higher-level methods for common use
  int GetPanTiltPos(float* pan, float* tilt);
  int GetPanTiltVel(float* panspeed, float* tiltspeed);
  int SetPanPos(float currpan, float pan);
  int SetTiltPos(float currtilt, float tilt);
  int SetPanVel(float panspeed);
  int SetTiltVel(float tiltspeed);
  int Home();
  int Halt();
  int Reset(int id);
  int SetLimits();
  int CheckCubeState(int id);

  // MessageHandler
  int ProcessMessage(Command command); 
  int Setup(const char* p_serialNumber);

  int Shutdown();  
};

#endif
