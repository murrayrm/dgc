#include "PTULadarFeeder.hh"

// Default constructor
PTULadarFeeder::PTULadarFeeder()
{
  return;
}

PTULadarFeeder::PTULadarFeeder(bool logging)
{
  memset(this, 0, sizeof(*this));

  this->scanId = -1;
  
  // Enable logging flag or not
  if(logging)
    this->enableLog = true;
  else
    this->enableLog = false;

  //snprintf(this->logName, sizeof(this->logName), "%s", "n/a");

  return;
}


// Default destructor
PTULadarFeeder::~PTULadarFeeder()
{
  return;
}


// Parse the config file
int PTULadarFeeder::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  this->ladarPort = this->options.port_arg;
  
  // Parse transform
  float px, py, pz;
  float rx, ry, rz;

  if (sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz) < 3)
    return ERROR("syntax error in sensor pos argument");
  if (sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz) < 3)
    return ERROR("syntax error in sensor rot argument");
  
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2tool);
  
  return 0;
}


// Initialize feeder for live capture
int PTULadarFeeder::initLive()
{
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("PTUcontroller");
  
  // Fill out the spread name
  if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else    
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");    
  
  // Fill out the skynet key
  if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    return ERROR("unknown SKYNET KEY");
  
  // Fill out sensor id
  this->sensorId = SENSNET_PTU_LADAR;

  
  // Load configuration file
  if (this->parseConfigFile(this->defaultConfigPath) != 0)
    return -1;
  
  if (this->ladarPort == NULL)
    return ERROR("ladar port is not set");
  
  MSG("connecting %s", this->ladarPort);
  
  this->sick = sick_driver_alloc();
  assert(this->sick);
  
  // Try connecting at 500000
  // each ladar has the EPROM permanently set to transfer data at 500K
  // so there's no need to try 9600
  if(sick_driver_open_serial(this->sick, this->ladarPort, 500000, 100, 10) != 0)
    return ERROR("unable to connect to ladar on this port: %s", this->ladarPort);
  
  return 0;
}

// Finalize feeder for live capture
int PTULadarFeeder::finiLive()
{
  sick_driver_close(this->sick);
  sick_driver_free(this->sick);
  this->sick = NULL;

  return 0;
}

// Capture a scan 
int PTULadarFeeder::captureLive()
{

  int i, numRanges;
  float pt, ranges[181];
  int values[181];
  
  // Read data from sensor.  Note that this ignores errors on read
  // (e.g., CRC errors) to prevent the program from terminating.
  if (sick_driver_read(this->sick, &this->scanTime, 181, &numRanges, ranges, values) != 0)
    return 0;    
  
  this->scanId += 1;
  
  // Unpack the data.  Assumes standard format for SICK.
  // This also checks against a min/max scan angle to exclude points
  // that lie of the vehicle itself.
  this->numPoints = 0;
  for (i = 0; i < numRanges; i++)
    {
      pt = (i - numRanges/2) * M_PI/180;
      if (this->options.scan_min_given && pt < this->options.scan_min_arg * M_PI/180)
        continue;
      if (this->options.scan_max_given && pt > this->options.scan_max_arg * M_PI/180)
	continue;      
      this->points[this->numPoints][0] = pt;
      this->points[this->numPoints][1] = ranges[i];
      this->points[this->numPoints][2] = values[i];
      this->numPoints++;
    }
  
  return 0;
}

// Initialize sensnet
int PTULadarFeeder::initSensnet(const char *configPath)
{    
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(LadarRangeBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(LadarRangeBlob), 512 - sizeof(LadarRangeBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (LadarRangeBlob*) valloc(sizeof(LadarRangeBlob));

  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return ERROR("unable to connect to sensnet");

  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");

  // Initialize logging
  time_t t;
  char timestamp[64];
  char cmd[256];
  sensnet_log_header_t header;
  
  // Construct log name
  if(enableLog)
    {
      t = time(NULL);
      strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
      snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
	       this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));
      
      MSG("opening log %s", this->logName);
      
      // Initialize sensnet logging
      this->sensnet_log = sensnet_log_alloc();
      assert(this->sensnet_log);
      memset(&header, 0, sizeof(header));
      if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
	return ERROR("unable to open log: %s", this->logName);
      
      // Copy configuration files
      snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
	       configPath, sensnet_id_to_name(this->sensorId), this->logName);
      system(cmd);

    }

  return 0;
}


// Finalize sensnet
int PTULadarFeeder::finiSensnet()
{  
  if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }

  // Leave the state group
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;

  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish data
int PTULadarFeeder::writeSensnet(PTUStateBlob ptublob, bool enableLadarLog)
{
  int i;
  pose3_t pose;
  LadarRangeBlob *blob;

  blob = this->blob;

  // Construct the blob header
  blob->blobType = SENSNET_LADAR_BLOB;
  blob->version = LADAR_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->scanId = this->scanId;
  blob->timestamp = this->scanTime;
  blob->state = ptublob.state;
  
  // For the ptu ladar, we'll need to be careful about transforms
  float sens2ptu[4][4];
  
  // Sensor to vehicle transform
  mat44f_mul(sens2ptu,ptublob.tool2ptu, this->sens2tool);
  mat44f_mul(this->sens2veh, ptublob.ptu2veh, sens2ptu);
  
  memcpy(blob->sens2veh, this->sens2veh, sizeof(this->sens2veh));
  mat44f_inv(blob->veh2sens, blob->sens2veh);    

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
		      blob->state.localY,
		      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
			   blob->state.localPitch,
			   blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);  
  mat44f_inv(blob->loc2veh, blob->veh2loc);
  
  // Reset reserved values
  memset(blob->reserved, 0, sizeof(blob->reserved));
  // use reserved values to send raw PTU tilt params

  blob->ptuPan = ptublob.currpan;
  blob->ptuTilt = ptublob.currtilt;
  blob->ptuPanSpeed = ptublob.currpanspeed;
  blob->ptuTiltSpeed = ptublob.currtiltspeed;
  
  // Copy the scan data
  blob->numPoints = this->numPoints;
  for (i = 0; i < this->numPoints; i++)
  {
    assert(i < (int) (sizeof(blob->points) / sizeof(blob->points[0])));
    assert(i < (int) (sizeof(this->points) / sizeof(this->points[0])));
    blob->points[i][0] = this->points[i][0];
    blob->points[i][1] = this->points[i][1];
    blob->intensities[i] = (uint8_t) this->points[i][2];
  }
  
  // Write blob
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, this->sensorId, SENSNET_LADAR_BLOB,
                    this->scanId, sizeof(*blob), blob) != 0)
    return ERROR("unable to write blob");
  
  // Write to log
  if (this->sensnet_log && enableLadarLog)
    {
      if (sensnet_log_write(this->sensnet_log, blob->timestamp,
			    this->sensorId, SENSNET_LADAR_BLOB,
			    this->scanId, sizeof(*blob), blob) != 0)
	return ERROR("unable to write blob");
    }
  
  if (this->sensnet_log && enableLadarLog)
    {    
      // Keep some stats on logging
      this->logCount += 1;
      this->logSize += sizeof(*blob);
    }
  
  return 0;
}
