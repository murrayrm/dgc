
/*
 * $Id: amtecpowercube.cc,v 1.17.2.2 2006/09/25 15:54:58 gerkey Exp $
 * Author: Brian Gerkey
 * Modified: Jeremy Ma 2007/05/18 for DGC07
 */
/*
The amtecpowercube driver controls the Amtec PowerCube Wrist,
a powerful pan-tilt unit that can, for example, carry a SICK laser

This driver communicates with the PowerCube via RS232, and does NOT handle
the newer CAN-based units.  Please submit a patch to support the CAN
protocol.

The amtecpowercube driver supports both position and velocity control.  
For constant swiveling, the PowerCube works better under velocity control.

Note that this driver is relatively new and not thoroughly tested. 

- port (string)
  - Default: "/dev/ttyS0"
  - Serial port where the unit is attached.
- home (integer)
  - Default: 0
  - Whether to home (i.e., reset to the zero position) the unit before
    commanding it
- speed (angle)
  - Default: 40 deg/sec
  - Maximum pan/tilt speed 
- blocking (bool)
  - Default: true
  - Command blocking enabled or not; i.e. a command must be executed to 
    completion before another command can be executed
*/

#ifndef AMTEC_DRIVER_H
#define AMTEC_DRIVER_H

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/poll.h>
#include <termios.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <netinet/in.h>  /* for struct sockaddr_in, htons(3) */
#include <math.h>
#include <sys/ioctl.h>
#include <linux/serial.h>

  //#include <replace/replace.h>


#define AMTEC_DEFAULT_PORT "/dev/ttyUSB0"

#define AMTEC_SLEEP_TIME_USEC 20000

/* angular velocity used when in position control mode */
#define AMTEC_DEFAULT_SPEED_DEG_PER_SEC DTOR(40)

// start, end, and escape chars
#define AMTEC_STX       0x02
#define AMTEC_ETX       0x03
#define AMTEC_DLE       0x10

// sizes
#define AMTEC_MAX_CMDSIZE     48

// module IDs -- module address 1 <= M <= 31
#define AMTEC_MODULE_TILT       13 // this is specific to the unit
#define AMTEC_MODULE_PAN        14 // this is specific to the unit

// command IDs
#define AMTEC_CMD_RESET        0x00
#define AMTEC_CMD_HOME         0x01
#define AMTEC_CMD_HALT         0x02
#define AMTEC_CMD_SET_EXT      0x08
#define AMTEC_CMD_GET_EXT      0x0a
#define AMTEC_CMD_SET_MOTION   0x0b
#define AMTEC_CMD_SET_ISTEP    0x0d

// parameter IDs
#define AMTEC_PARAM_ACT_POS   0x3c
#define AMTEC_PARAM_MIN_POS   0x45
#define AMTEC_PARAM_MAX_POS   0x46
#define AMTEC_PARAM_CUBESTATE 0x27
#define AMTEC_PARAM_MAXCURR   0x4c
#define AMTEC_PARAM_ACT_VEL   0x41

// motion IDs
#define AMTEC_MOTION_FRAMP       4
#define AMTEC_MOTION_FRAMP_ACK  14
#define AMTEC_MOTION_FSTEP_ACK  16
#define AMTEC_MOTION_FVEL_ACK   17

// module state bitmasks
#define AMTEC_STATE_ERROR     0x01
#define AMTEC_STATE_HOME_OK   0x02
#define AMTEC_STATE_HALTED    0x04
#define AMTEC_POWERFAULT      0x08
#define AMTEC_TOW_ERROR       0x10
#define AMTEC_STATE_CPU_OVERLOAD    0x01000000
#define AMTEC_STATE_BEYOND_HARD     0x02000000
#define AMTEC_STATE_BEYOND_SOFT     0x04000000
#define AMTEC_STATE_POW_SETUP_ERR   0x08000000

#define MAXSPEED 100 //(deg/sec)
#define MAXPAN   170 //deg //this is due to the physical length of cable
#define MINPAN  -170 //deg //this is due to the physical length of cable
#define MAXTILT  90 //deg
#define MINTILT -90 //deg

#define BASE_TO_TILTAXIS       0.115 // m
#define TILTAXIS_TO_TOOLFRAME  0.071 //m

// degree/radian conversions
#define RTOD(r)   ((r) * 180 / M_PI)
#define DTOR(d)   ((d) * M_PI / 180)
#define NORMALIZE(z)   atan2(sin(z), cos(z))

// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

enum CONTROLMODE { POS_CONTROL, VEL_CONTROL};

struct Command
{
  float pan;
  float tilt;
  float panspeed;
  float tiltspeed;
};

class AmtecPowerCube
{
private:
  // bookkeeping
  bool fd_blocking;
  bool cmd_blocking;
  int minpan, maxpan;
  int mintilt, maxtilt;
  int speed; //maximum pan-tilt speed
  CONTROLMODE controlmode;
  
  // low-level methods to interact with the device
  int SendCommand(int id, unsigned char* cmd, size_t len);
  int WriteData(unsigned char *buf, size_t len);
  int AwaitAnswer(unsigned char* buf, size_t len);
  int AwaitETX(unsigned char* buf, size_t len);
  int ReadAnswer(unsigned char* buf, size_t len);
  size_t ConvertBuffer(unsigned char* buf, size_t len);
  int GetFloatParam(int id, int param, float* val);
  int GetUint32Param(int id, int param, unsigned int* val);
  int SetFloatParam(int id, int param, float val);
  
  // data (de)marshalling helpers
  // NOTE: these currently assume little-endianness, which is NOT
  //       portable (but works fine on x86).
  float BytesToFloat(unsigned char* bytes);
  unsigned int BytesToUint32(unsigned char* bytes);
  void FloatToBytes(unsigned char *bytes, float f);
  void Uint16ToBytes(unsigned char *bytes, unsigned short s);

  // helper for dealing with config requests.
  //void HandleConfig(void *client, unsigned char *buf, size_t len);
  float lastpan, lasttilt;  
  float lastpanspeed, lasttiltspeed;
  
public:
  int fd; // amtec device file descriptor 
  const char* serial_port;
    
  // Constructor
  AmtecPowerCube(const char *port, int speedLimit, bool blocking);

  // Constructor -- default
  AmtecPowerCube();

  // Destructor
  ~AmtecPowerCube();

  // higher-level methods for common use
  int GetPanTiltPos(float* pan, float* tilt);
  int GetPanTiltVel(float* panspeed, float* tiltspeed);
  int SetPanPos(float oldpan, float pan);
  int SetTiltPos(float oldtilt, float tilt);
  int SetPanVel(float panspeed);
  int SetTiltVel(float tiltspeed);
  int Home();
  int Halt();
  int Reset();
  int SetLimits();
  int CheckCubeState(int id, unsigned int *state);

  // MessageHandler
  int ProcessMessage(Command command);
  
  int Setup();
  int Shutdown();
};

#endif
