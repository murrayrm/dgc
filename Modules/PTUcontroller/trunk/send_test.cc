#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <interfaces/PTUCommand.h>


#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/StateClient.hh"

using namespace std;


//class CStateWrapper;

/// Wraps a state client into an object
class CStateWrapper : public CStateClient
{
public:
  CStateWrapper(int snkey) : CSkynetContainer(MODmapping,snkey),CStateClient(true) { };
  ~CStateWrapper() {};
  
  VehicleState getstate() {UpdateState();return m_state;}

};

int main(int argc, char* argv[])
{
 
  int ptuCommandSocket;
  int sendSuccess;
  int type, ptuType;
  float pan, tilt;
  float localx, localy, localz;
  float panspeed, tiltspeed;
  PTUCommand ptuCommand;

  cout << "Which PTU are you commanding? " << endl;
  cout << "0: AMTEC" << endl;
  cout << "1: DIRECT" << endl;
  cin >> ptuType;
 
  skynet m_skynet = skynet(217, atoi(getenv("SKYNET_KEY")), NULL); 
  if(ptuType==0)
    ptuCommandSocket = m_skynet.get_send_sock(SNptuCommand);
  else if(ptuType==1)
    ptuCommandSocket = m_skynet.get_send_sock(SNptuMiniCommand);
  else
    {
      printf("invalid PTU type. Aborting.\n");
      return(0);
    }

  if(ptuCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }

  while(true)
    {

      cout << "What would you like to do? " << endl;
      cout << "0: Send Line-of-site command" << endl;
      cout << "1: Send Raw commands " << endl;
      cout << "2: Start Demo" << endl;
      cout << "3: Demo 2" << endl;
      cout << "4: Demo 3 - stationary when moving, tilting when alice stationary" << endl;
      cin >> type;

      if(type==0)
	{
	  cout << "Enter commanded pose for line of site: <localx> <localy> <localz> <panspeed> <tiltspeed>" << endl;
	  cin >> localx >> localy >> localz >> panspeed >> tiltspeed;
	  
	  ptuCommand.type = LINEOFSITE;
	  ptuCommand.localx = localx;
	  ptuCommand.localy = localy;
	  ptuCommand.localz = localz;
	  ptuCommand.panspeed = panspeed; //mdeg/s
	  ptuCommand.tiltspeed = tiltspeed; //mdeg/s
	  
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	}
      else if(type==1)
	{
	  cout << "Enter commanded pan and tilt angle (deg): <PAN> <TILT> <PANSPEED> <TILTSPEED>" << endl;
	  cin >> pan >> tilt >> panspeed >> tiltspeed;
	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan;
	  ptuCommand.tilt = tilt;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	}
      else if(type==2)
      	{
	  while(true)
	  {
	    ptuCommand.type = RAW;
	    ptuCommand.pan = 60;
	    ptuCommand.tilt = -30;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	    sleep(10);

	    ptuCommand.type = RAW;
	    ptuCommand.pan = 0;
	    ptuCommand.tilt = 0;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	    sleep(10);

	    ptuCommand.type = RAW;
	    ptuCommand.pan = -60;
	    ptuCommand.tilt = -30;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	
	    sleep(10);

	    ptuCommand.type = RAW;
	    ptuCommand.pan = 0;
	    ptuCommand.tilt = 0;
	    ptuCommand.panspeed = 5;
	    ptuCommand.tiltspeed = 5;
	    sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	   sleep(10);

	  }
	 
	}
      else if(type==3)
	{
	  while(true)
	    {
	      ptuCommand.type = RAW;
	      ptuCommand.pan = 0;
	      ptuCommand.tilt = -5;
	      ptuCommand.panspeed = 5;
	      ptuCommand.tiltspeed = 5;
	      sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	      sleep(10);
	      
	      ptuCommand.type = RAW;
	      ptuCommand.pan = 0;
	      ptuCommand.tilt = -15;
	      ptuCommand.panspeed = 5;
	      ptuCommand.tiltspeed = 5;
	      sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	      sleep(10);
	      
	      ptuCommand.type = RAW;
	      ptuCommand.pan = 0;
	      ptuCommand.tilt = -25;
	      ptuCommand.panspeed = 5;
	      ptuCommand.tiltspeed = 5;
	      sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	      sleep(10);

	      ptuCommand.type = RAW;
	      ptuCommand.pan = 0;
	      ptuCommand.tilt = -15;
	      ptuCommand.panspeed = 5;
	      ptuCommand.tiltspeed = 5;
	      sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	      sleep(10);
	      	      
	    }
	}
     else if(type==4)
	{
	  int skynetKey = atoi(getenv("SKYNET_KEY"));
	  CStateWrapper * stateTalker;
	  stateTalker = new CStateWrapper(skynetKey);
	  VehicleState state;

	  //while alice is stopped
	     //one full sweep (out->in->out)
	  //if alice moving
	    //send PTU to tilt = -5

	  while(true)
	    {

	      state = stateTalker->getstate();
	      fprintf(stderr, "velocity = %f \n", state.vehSpeed);
	      //alice is stopped/going slow, so sweep down and back up
	      if(state.vehSpeed < 1.0) {

  	        ptuCommand.type = RAW;
	        ptuCommand.pan = 0;
	        ptuCommand.tilt = -25;
	        ptuCommand.panspeed = 5;
	        ptuCommand.tiltspeed = 5;
	        sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	        sleep(4);


	        ptuCommand.type = RAW;
	        ptuCommand.pan = 0;
	        ptuCommand.tilt = 0;
	        ptuCommand.panspeed = 5;
	        ptuCommand.tiltspeed = 5;
	        sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	        sleep(5);


	        ptuCommand.type = RAW;
	        ptuCommand.pan = 0;
	        ptuCommand.tilt = -5;
	        ptuCommand.panspeed = 5;
	        ptuCommand.tiltspeed = 5;
	        sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	      
	        sleep(1);


	      } else { //alice is moving. stay at -5
		sleep(2);

	      }
	    } // end while(true)
	}

      else
	printf("Unrecognized type. Please try again\n");
    }
  
  return(0);
}


