#ifndef INTERSECTIONHANDLING_HH
#define INTERSECTIONHANDLING_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "sys/time.h"
#include "map/Map.hh"
#include "gcinterfaces/SegGoals.hh"
#include <trajutils/maneuver.h>
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "temp-planner-interfaces/LogClass.hh"


class IntersectionHandling
{ 
public:
  enum IntersectionReturn
    { 
      RESET,
      WAIT_PRECEDENCE,
      WAIT_MERGING,
      WAIT_CLEAR,
      GO,
      JAMMED
    };

  // Functions for dyn. obstacles at intersections
  static void init();
  static void reset(PlanGraph* g, Map* m);
  static void updateConsole();
  static IntersectionReturn checkIntersection(VehicleState vehState, SegGoals currSegment);

private:

  struct WayPoints_t {
    PointLabel WayPoint;
    bool monitorWhileMerging;
  };

  static void populateWayPoints(PointLabel InitialWayPointEntry);
  static void detectIntersectingTrafficWhileMerging(SegGoals currSegment);
  static void findStoplines(PointLabel InitialWayPointEntry);
  static void maintainPrecedenceList(VehicleState vehState);
  static int checkFeasibility(bool result, PointLabel entryPointLabel, PointLabel exitPointLabel);
  static bool checkVisibility(VehicleState Alice,MapElement obstacle);
  static int getTransitionAlongGraph(point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, SegGoals currSegment, bool grow, int leftright);
  static int getTransition(point2arr& transition, PointLabel entryPointLabel, PointLabel exitPointLabel);
  static bool crossPoint(point2arr a, point2arr b, point2 &crosspoint);
  static bool inSection(point2 a, point2 b, point2 x);
  static void outputLog(IntersectionReturn previousReturnValue, IntersectionReturn ReturnValue);
  static IntersectionReturn checkPrecedence();
  static IntersectionReturn checkMerging(SegGoals currSegment);
  static IntersectionReturn checkClearance(SegGoals currSegment);

  // Graph, map and logging
  static Map* localMap;
  static PlanGraph* graph;
  static CLog* logIntersection;

  // Variables for checking the intersection
  static IntersectionReturn ReturnValue;
  static vector<PrecedenceList_t> PrecedenceList;
  static vector<MapElement> ClearanceList;
  static vector<MapElement> MergingList;
  static bool FirstCheck;
  static time_t timestamp;
  static PointLabel entryWaypoint;

  static vector<PointLabel> WayPointsWithStop;
  static vector<WayPoints_t> WayPointsNoStop;
  static vector<PointLabel> WayPointsEntries;
  static bool JammedTimerStarted;

  // logging and timing
  static bool timerPrecedenceStarted;
  static bool logPrecedenceCounter;
  static int logLostVisibilityCounter;
  static int logLostIdCounter;
  static int logNearbyObstacleCounter;
  static int logUnfeasibleTurnCounter;
  static unsigned long long IntersectionStartTime;
  static double IntersectionTimer;
  static bool clearset;
  static unsigned long long cleartime;

  // debug information sent to MapViewer
  static int mapcounter;
  static const int MAPSTART = 6000;
  static CMapElementTalker testMap;
  static const int sendChannel=-2;
  static const int debugChannel=-10;

  // CONSTANTS
  static double DISTANCE_STOPLINE_OBSTACLE;
  static double DISTANCE_STOPLINE_ALICE;
  static double VELOCITY_OBSTACLE_THRESHOLD;
  static double DISTANCE_OBSTACLE_THRESHOLD;
  static double ETA_EPS;
  static bool INTERSECTION_SAFETY_FLAG;
  static bool INTERSECTION_CABMODE_FLAG;
  static double TRACKING_VEHICLE_SEPERATION;
  static double FLICKER_FILTER_THRESHOLD;
  static double INTERSECTION_TIMEOUT;
  static double INTERSECTION_TIME_CLEARANCE;
  static double INTERSECTION_MERGING_WAIT_LONGER;
  static double INTERSECTION_GROW_CORRIDOR;
  static double MAXIMUM_VELOCITY;
  static double INTERSECTION_MERGING_LANE_OFFSET;
};

#endif  // INTERSECTIONHANDLING_HH
