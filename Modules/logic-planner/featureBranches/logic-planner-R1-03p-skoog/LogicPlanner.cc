#include <math.h>

#include <temp-planner-interfaces/Utils.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/ConfigFile.hh>

#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include "LogicPlanner.hh"

FSM_state_t LogicPlanner::current_state;
FSM_flag_t LogicPlanner::current_flag;
FSM_region_t LogicPlanner::current_region;
FSM_obstacle_t LogicPlanner::current_obstacle;
FSM_planner_t LogicPlanner::current_planner;

uint64_t LogicPlanner::start_current_lp_state_time;
double LogicPlanner::start_current_lp_state_xpos;
double LogicPlanner::start_current_lp_state_ypos;
CIntersectionHandling* LogicPlanner::IntersectionHandling;


#define MAX_PASSING_1_LENGTH      (30 + 2*VEHICLE_LENGTH)
#define STOP_VEL                  0.2

// Initialize class constants
double LogicPlanner::DESIRED_DECELERATION = 0.25;
double LogicPlanner::INTERSECTION_BUFFER = 2;
double LogicPlanner::MAX_INTERSECTION_Q_LENGTH = 30;
double LogicPlanner::DIST_NOCHECK_OBS_STOPINT = 0.5;
double LogicPlanner::TIME_CLEAR_INTERSECTION = 45.0;
int LogicPlanner::MAX_NUM_CHECK_INTERSECTION = 5;
double LogicPlanner::ALICE_SAFETY_FRONT = 1.0;
double LogicPlanner::ALICE_SAFETY_REAR = 0.5;
double LogicPlanner::ALICE_SAFETY_SIDE = 0.5;
bool LogicPlanner::BACKUP_WITH_RAIL_PLANNER = 0;
bool LogicPlanner::USE_RAIL_PLANNER_REV = 0;
bool LogicPlanner::ALLOW_BACKUP = 0;
int LogicPlanner::MAX_NUM_BACKUP = 3;
double LogicPlanner::MIN_DIST_BETWEEN_DIFF_BACKUP = 30.0;
int LogicPlanner::MAX_NUM_DRIVE_PASS_REV = 3;
int LogicPlanner::MAX_NUM_DRIVE_AGG = 2;
int LogicPlanner::MAX_NUM_DRIVE_BARE = 2;
double LogicPlanner::MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV = 30.0;
int LogicPlanner::MAX_NUM_S1PLANNER_BACKUP = 2;

// Timeout
double LogicPlanner::TIMEOUT_OBS_PASSING = 3.0;
double LogicPlanner::TIMEOUT_OBS_PASSING_EXTENDED = 17.0;
double LogicPlanner::TIMEOUT_OBS_INT = 20.0;
double LogicPlanner::TIMEOUT_STOPINT = 30.0;
double LogicPlanner::MIN_TIME_STOP_BEFORE_UTURN = 7.0;
double LogicPlanner::MIN_TIME_OBS_DISAPPEAR = 1.5;
double LogicPlanner::MIN_TIME_STOPOBS_SHORT = 2.0;
double LogicPlanner::MIN_TIME_STOPOBS_LONG = 3.0;
double LogicPlanner::MIN_TIME_PASSING = 8.0;
double LogicPlanner::MIN_DIST_PASSING = 20.0;
double LogicPlanner::MIN_TIME_LOST = 30.0;

// For failure s1planner
double LogicPlanner::MIN_TIME_STOP_BEFORE_S1PLANNER = 5.0;
double LogicPlanner::MAX_TIME_USE_S1PLANNER = 20.0;
double LogicPlanner::MAX_DIST_USE_S1PLANNER = 30.0;
double LogicPlanner::MIN_TIME_IN_DRIVE_S1PLANNER = 3.0;

// For DRIVE_AGG or DRIVE_BARE
double LogicPlanner::TIMEOUT_USE_SMALL_OBS = 50.0;
double LogicPlanner::MAX_DIST_USE_SMALL_OBS = 50.0;

// For failure handling: make sure that we keep moving
double LogicPlanner::FH_TIMEOUT_NONNOMINAL = 20.0;
double LogicPlanner::FH_MAX_DISTANCE_NONNOMINAL = 30.0;
double LogicPlanner::FH_MAX_STOP_TIME = 10.0;
double LogicPlanner::FH_MAX_STOP_TIME_BETWEEN_FH = 3.0;
double LogicPlanner::FH_MIN_TIME_WAIT_TO_PASS = 25.0;
double LogicPlanner::FH_DIST_CHECK_VEH_PASSING = 60.0;

// Whether we want to change obstacle size when we use rail planner
int LogicPlanner::NUM_CHANGE_RP_OBS_SIZE = 2;
double LogicPlanner::MIN_TIME_STOP_BEFORE_CHANGE_OBS = 3.0;

// Use S1PLANNER only
bool LogicPlanner::USE_S1PLANNER_ONLY = false;
bool LogicPlanner::USE_S1PLANNER_ON_ROAD = true;

// Go to OFFROAD mode when rail planner returns PP_NOPATH_LEN
bool LogicPlanner::USE_OFFROAD = 1;
double LogicPlanner::TIMEOUT_NOPATHLEN = 2.0;
double LogicPlanner::TIMEOUT_OFFROAD = 5.0;
double LogicPlanner::MIN_DIST_OFFROAD = 15.0;

// Go to OFFROAD when a one lane road is blocked
bool LogicPlanner::USE_OFFROAD_ONELANE = 1;

// Go to OFFROAD when we're in zone
bool LogicPlanner::USE_OFFROAD_ZONE = 1;

  // Specify which zone ID we want to use RAIL_PLANNER_NOPASS, RAIL_PLANNER_OFFROAD
int LogicPlanner::ZONE_ID_USE_DRIVE_NOPASS = 43;
int LogicPlanner::ZONE_ID_USE_OFFROAD = 42;

int LogicPlanner::init()
{
  readConfig();
  current_state = PAUSE;
  current_flag = NO_PASS;
  current_obstacle = OBSTACLE_SAFETY;
  current_region = ROAD_REGION;
  current_planner = RAIL_PLANNER;

  start_current_lp_state_time = Utils::getTime();
  start_current_lp_state_xpos = 0;
  start_current_lp_state_ypos = 0;

  IntersectionHandling = new CIntersectionHandling();

  return 0;
}

void LogicPlanner::destroy()
{
  return;
}

void LogicPlanner::readConfig()
{
  char *path;

  path=dgcFindConfigFile("logicplanner.conf","logic-planner");
  FILE* configFile = fopen(path,"r");
  if (configFile == NULL) {
    Console::addMessage("Warning: Couldn't read Logic Planner configuration.");
    Log::getStream(1)<<"Warning: Couldn't read Logic Planner configuration."<<endl;
  }
  else {
    ConfigFile config(path);
    config.readInto(DESIRED_DECELERATION, "DESIRED_DECELERATION");
    config.readInto(INTERSECTION_BUFFER, "INTERSECTION_BUFFER");
    config.readInto(MAX_INTERSECTION_Q_LENGTH, "MAX_INTERSECTION_Q_LENGTH");
    config.readInto(DIST_NOCHECK_OBS_STOPINT, "DIST_NOCHECK_OBS_STOPINT");
    config.readInto(TIME_CLEAR_INTERSECTION, "TIME_CLEAR_INTERSECTION");
    config.readInto(MAX_NUM_CHECK_INTERSECTION, "MAX_NUM_CHECK_INTERSECTION");
    config.readInto(ALICE_SAFETY_FRONT, "ALICE_SAFETY_FRONT");
    config.readInto(ALICE_SAFETY_REAR, "ALICE_SAFETY_REAR");
    config.readInto(ALICE_SAFETY_SIDE, "ALICE_SAFETY_SIDE");
    config.readInto(BACKUP_WITH_RAIL_PLANNER, "BACKUP_WITH_RAIL_PLANNER");
    config.readInto(USE_RAIL_PLANNER_REV, "USE_RAIL_PLANNER_REV");
    config.readInto(ALLOW_BACKUP, "ALLOW_BACKUP");
    config.readInto(MAX_NUM_BACKUP, "MAX_NUM_BACKUP");
    config.readInto(MIN_DIST_BETWEEN_DIFF_BACKUP, "MIN_DIST_BETWEEN_DIFF_BACKUP");
    config.readInto(MAX_NUM_DRIVE_PASS_REV, "MAX_NUM_DRIVE_PASS_REV");
    config.readInto(MAX_NUM_DRIVE_AGG, "MAX_NUM_DRIVE_AGG");
    config.readInto(MAX_NUM_DRIVE_BARE, "MAX_NUM_DRIVE_BARE");
    config.readInto(MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV, "MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV");
    config.readInto(MAX_NUM_S1PLANNER_BACKUP, "MAX_NUM_S1PLANNER_BACKUP");

    // Timeout
    config.readInto(TIMEOUT_OBS_PASSING, "TIMEOUT_OBS_PASSING");
    config.readInto(TIMEOUT_OBS_PASSING_EXTENDED, "TIMEOUT_OBS_PASSING_EXTENDED");
    config.readInto(TIMEOUT_OBS_INT, "TIMEOUT_OBS_INT");
    config.readInto(TIMEOUT_STOPINT, "TIMEOUT_STOPINT");
    config.readInto(MIN_TIME_STOP_BEFORE_UTURN, "MIN_TIME_STOP_BEFORE_UTURN");
    config.readInto(MIN_TIME_OBS_DISAPPEAR, "MIN_TIME_OBS_DISAPPEAR");
    config.readInto(MIN_TIME_STOPOBS_SHORT, "MIN_TIME_STOPOBS_SHORT");
    config.readInto(MIN_TIME_STOPOBS_LONG, "MIN_TIME_STOPOBS_LONG");
    config.readInto(MIN_TIME_PASSING, "MIN_TIME_PASSING");
    config.readInto(MIN_DIST_PASSING, "MIN_DIST_PASSING");
    config.readInto(MIN_TIME_LOST, "MIN_TIME_LOST");

    // For s1planner
    config.readInto(MIN_TIME_STOP_BEFORE_S1PLANNER, "MIN_TIME_STOP_BEFORE_S1PLANNER");
    config.readInto(MAX_TIME_USE_S1PLANNER, "MAX_TIME_USE_S1PLANNER");
    config.readInto(MAX_DIST_USE_S1PLANNER, "MAX_DIST_USE_S1PLANNER");
    config.readInto(MIN_TIME_IN_DRIVE_S1PLANNER, "MIN_TIME_IN_DRIVE_S1PLANNER");

    // For DRIVE_AGG or DRIVE_BARE
    config.readInto(TIMEOUT_USE_SMALL_OBS, "TIMEOUT_USE_SMALL_OBS");
    config.readInto(MAX_DIST_USE_SMALL_OBS, "MAX_DIST_USE_SMALL_OBS");

    // For failure handling: make sure that we keep moving
    config.readInto(FH_TIMEOUT_NONNOMINAL, "FH_TIMEOUT_NONNOMINAL");
    config.readInto(FH_MAX_DISTANCE_NONNOMINAL, "FH_MAX_DISTANCE_NONNOMINAL");
    config.readInto(FH_MAX_STOP_TIME, "FH_MAX_STOP_TIME");
    config.readInto(FH_MAX_STOP_TIME_BETWEEN_FH, "FH_MAX_STOP_TIME_BETWEEN_FH");
    config.readInto(FH_MIN_TIME_WAIT_TO_PASS, "FH_MIN_TIME_WAIT_TO_PASS");
    config.readInto(FH_DIST_CHECK_VEH_PASSING, "FH_DIST_CHECK_VEH_PASSING");

    // For obstacle
    config.readInto(NUM_CHANGE_RP_OBS_SIZE, "NUM_CHANGE_RP_OBS_SIZE");
    config.readInto(MIN_TIME_STOP_BEFORE_CHANGE_OBS, "MIN_TIME_STOP_BEFORE_CHANGE_OBS");
    
    // Use S1PLANNER only
    config.readInto(USE_S1PLANNER_ONLY, "USE_S1PLANNER_ONLY");
    config.readInto(USE_S1PLANNER_ON_ROAD, "USE_S1PLANNER_ON_ROAD");

    // Go to OFFROAD mode when rail planner returns PP_NOPATH_LEN
    config.readInto(USE_OFFROAD, "USE_OFFROAD");
    config.readInto(TIMEOUT_NOPATHLEN, "TIMEOUT_NOPATHLEN");
    config.readInto(TIMEOUT_OFFROAD, "TIMEOUT_OFFROAD");
    config.readInto(MIN_DIST_OFFROAD, "MIN_DIST_OFFROAD");

    // Go to OFFROAD when a one lane road is blocked
    config.readInto(USE_OFFROAD_ONELANE, "USE_OFFROAD_ONELANE");

    // Go to OFFROAD when we're in zone
    config.readInto(USE_OFFROAD_ZONE, "USE_OFFROAD_ZONE");

    // Specify which zone ID we want to use RAIL_PLANNER_NOPASS, RAIL_PLANNER_OFFROAD
    config.readInto(ZONE_ID_USE_DRIVE_NOPASS, "ZONE_ID_USE_DRIVE_NOPASS");
    config.readInto(ZONE_ID_USE_OFFROAD, "ZONE_ID_USE_OFFROAD");

    Log::getStream(1)<<"Info: Read Logic Planner configuration successfully."<<endl;
    fclose(configFile);

    // Log all the parameters
    Log::getStream(5) << "INTERSECTION_BUFFER: " << INTERSECTION_BUFFER << endl;
    Log::getStream(5) << "MAX_INTERSECTION_Q_LENGTH: " << MAX_INTERSECTION_Q_LENGTH << endl;
    Log::getStream(5) << "DIST_NOCHECK_OBS_STOPINT: " << DIST_NOCHECK_OBS_STOPINT << endl;
    Log::getStream(5) << "TIME_CLEAR_INTERSECTION: " << TIME_CLEAR_INTERSECTION << endl;
    Log::getStream(5) << "ALICE_SAFETY_FRONT: " << ALICE_SAFETY_FRONT << endl;
    Log::getStream(5) << "ALICE_SAFETY_REAR: " << ALICE_SAFETY_REAR << endl;
    Log::getStream(5) << "ALICE_SAFETY_SIDE: " << ALICE_SAFETY_SIDE << endl;
    Log::getStream(5) << "ALLOW_BACKUP: " << ALLOW_BACKUP << endl;
    Log::getStream(5) << "MAX_NUM_BACKUP: " << MAX_NUM_BACKUP << endl;
    Log::getStream(5) << "MIN_DIST_BETWEEN_DIFF_BACKUP: " << MIN_DIST_BETWEEN_DIFF_BACKUP << endl;
    Log::getStream(5) << "MAX_NUM_S1PLANNER_BACKUP: " << MAX_NUM_S1PLANNER_BACKUP << endl;
    Log::getStream(5) << "BACKUP_WITH_RAIL_PLANNER: " << BACKUP_WITH_RAIL_PLANNER << endl;
    Log::getStream(5) << "USE_RAIL_PLANNER_REV: " << USE_RAIL_PLANNER_REV << endl;
    Log::getStream(5) << "MAX_NUM_DRIVE_PASS_REV: " << MAX_NUM_DRIVE_PASS_REV << endl;
    Log::getStream(5) << "MAX_NUM_DRIVE_AGG: " << MAX_NUM_DRIVE_AGG << endl;
    Log::getStream(5) << "MAX_NUM_DRIVE_BARE: " << MAX_NUM_DRIVE_BARE << endl;
    Log::getStream(5) << "MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV: " << MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV << endl;
    Log::getStream(5) << "TIMEOUT_OBS_PASSING: " << TIMEOUT_OBS_PASSING << endl;
    Log::getStream(5) << "TIMEOUT_OBS_PASSING_EXTENDED: " << TIMEOUT_OBS_PASSING_EXTENDED << endl;
    Log::getStream(5) << "TIMEOUT_OBS_INT: " << TIMEOUT_OBS_INT << endl;
    Log::getStream(5) << "TIMEOUT_STOPINT: " << TIMEOUT_STOPINT << endl;
    Log::getStream(5) << "MIN_TIME_STOP_BEFORE_UTURN: " << MIN_TIME_STOP_BEFORE_UTURN << endl;
    Log::getStream(5) << "MIN_TIME_OBS_DISAPPEAR: " << MIN_TIME_OBS_DISAPPEAR << endl;
    Log::getStream(5) << "MIN_TIME_STOPOBS_SHORT: " << MIN_TIME_STOPOBS_SHORT << endl;
    Log::getStream(5) << "MIN_TIME_STOPOBS_LONG: " << MIN_TIME_STOPOBS_LONG << endl;
    Log::getStream(5) << "MIN_TIME_PASSING: " << MIN_TIME_PASSING << endl;
    Log::getStream(5) << "MIN_DIST_PASSING: " << MIN_DIST_PASSING << endl;
    Log::getStream(5) << "MIN_TIME_LOST: " << MIN_TIME_LOST << endl;
    Log::getStream(5) << "MIN_TIME_STOP_BEFORE_S1PLANNER: " << MIN_TIME_STOP_BEFORE_S1PLANNER << endl;
    Log::getStream(5) << "MAX_TIME_USE_S1PLANNER: " << MAX_TIME_USE_S1PLANNER << endl;
    Log::getStream(5) << "MAX_DIST_USE_S1PLANNER: " << MAX_DIST_USE_S1PLANNER << endl;
    Log::getStream(5) << "MIN_TIME_IN_DRIVE_S1PLANNER: " << MIN_TIME_IN_DRIVE_S1PLANNER << endl;
    Log::getStream(5) << "TIMEOUT_USE_SMALL_OBS: " << TIMEOUT_USE_SMALL_OBS << endl;
    Log::getStream(5) << "MAX_DIST_USE_SMALL_OBS: " << MAX_DIST_USE_SMALL_OBS << endl;
    Log::getStream(5) << "FH_TIMEOUT_NONNOMINAL: " << FH_TIMEOUT_NONNOMINAL << endl;
    Log::getStream(5) << "FH_MAX_DISTANCE_NONNOMINAL: " << FH_MAX_DISTANCE_NONNOMINAL << endl;
    Log::getStream(5) << "FH_MAX_STOP_TIME: " << FH_MAX_STOP_TIME << endl;
    Log::getStream(5) << "FH_MAX_STOP_TIME_BETWEEN_FH: " << FH_MAX_STOP_TIME_BETWEEN_FH << endl;
    Log::getStream(5) << "FH_MIN_TIME_WAIT_TO_PASS: " << FH_MIN_TIME_WAIT_TO_PASS << endl;
    Log::getStream(5) << "FH_DIST_CHECK_VEH_PASSING: " << FH_DIST_CHECK_VEH_PASSING << endl;
    Log::getStream(5) << "NUM_CHANGE_RP_OBS_SIZE: " << NUM_CHANGE_RP_OBS_SIZE << endl;
    Log::getStream(5) << "MIN_TIME_STOP_BEFORE_CHANGE_OBS: " << MIN_TIME_STOP_BEFORE_CHANGE_OBS << endl;
    Log::getStream(5) << "USE_S1PLANNER_ONLY: " << USE_S1PLANNER_ONLY << endl;
    Log::getStream(5) << "USE_S1PLANNER_ON_ROAD: " << USE_S1PLANNER_ON_ROAD << endl;
    Log::getStream(5) << "USE_OFFROAD: " << USE_OFFROAD << endl;
    Log::getStream(5) << "TIMEOUT_NOPATHLEN: " << TIMEOUT_NOPATHLEN << endl;
    Log::getStream(5) << "TIMEOUT_OFFROAD: " << TIMEOUT_OFFROAD << endl;
    Log::getStream(5) << "USE_OFFROAD_ONELANE: " << USE_OFFROAD_ONELANE << endl;
    Log::getStream(5) << "USE_OFFROAD_ZONE" << USE_OFFROAD_ZONE << endl;
  }
}

void LogicPlanner::updateIntersectionHandlingConsole()
{
  IntersectionHandling->updateConsole();
}

/**
 * @brief This function is the main function of the logic-planner library, it returns a number of possible state Alice can be in.
 *
 * This function takes as input prevErr in order to handle the previous error/errors
 * generated by the previous planner cycle (by adjusting the future state), the vehicle
 * state, the map and diverse parameters (not yet defined). It outputs a vector of state
 * problems (structure containing a possible state along with its probability).
 *
 * It can also fail if the map information is too bad, in this case, the logic-planner
 * will switch to the PAUSE state and stay there until we are able to plan again.
 *
 * @return The function currently returns an array of size 1 of possible state problems we might be in.
 */
 Err_t LogicPlanner::planLogic(vector<StateProblem_t> &problems, PlanGraph *graph, Err_t prevErr, VehicleState &vehState,
			       Map *map, Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
			       bool& replanInZone)
{
  static LP_state_t current_lp_state = LP_PAUSE;

  // intersection variables
  static PointLabel last_intersection_point = PointLabel(0,0,0);
  static int numCheckCurrentIntersection = 0;
  static uint64_t last_intersection_time = 0;
  static CIntersectionHandling::IntersectionReturn inter_ret = CIntersectionHandling::RESET;

  // backup variables
  // variables that keep track of where to transition from LP_BACKUP
  static LP_state_t backup_lp_state = LP_DRIVE_PASS_BACKUP;
  static double just_finished_backup_xpos = 0;
  static double just_finished_backup_ypos = 0;
  static int num_backup = 0;

  // variables that keep track of where to transition from LP_DRIVE_PASS_REV
  static double just_finished_drpsrv_xpos = 0;
  static double just_finished_drpsrv_ypos = 0;
  static int num_drpsrv = 0;
  static double just_finished_dagg_xpos = 0;
  static double just_finished_dagg_ypos = 0;
  static int num_dagg = 0;
  static double just_finished_dbare_xpos = 0;
  static double just_finished_dbare_ypos = 0;
  static int num_dbare = 0;

  // variable that keeps track of how long we've seen an obstacle
  static uint64_t last_obs_time = 0;

  // variables that keep track of where to transition from LP_PAUSE
  static LP_state_t paused_lp_state = LP_DRIVE_NOPASS;

  // variables that keep track of the number of times s1planner fails and 
  // we need to switch to back up
  static int num_s1planner_backup = 0;  

  // zone variable
  static int zone_id = 0;
  static bool switch_to_s1planner = false;

  // Variables that keep track of whether we're lost
  static int last_report_lost_segment_id = 0;
  static uint64_t last_report_lost_time = 0;
  static SegGoals prevSegGoals;

  // FH variables
  static uint64_t fh_last_action_time = 0;
  static double fh_last_xpos = 0;
  static double fh_last_ypos = 0;
  static LP_state_t fh_lp_state = LP_DRIVE_NOPASS;
  static bool fh_used = false;
  // Start intersection timer as soon as state changes to STOP_INT
  static bool startIntersectionTimer = false;
  //static bool firstTimeout = false;
  //static bool lockFirstTimeout = false;
  static unsigned long long IntersectionTimeStart;
  static unsigned long long IntersectionTimeEnd;
  static double correct_value = 0.0;

  // To keep track whether we need to replan before passing
  static bool stopobs_replan = false;

  // To keep track of OFFROAD mode
  static double last_nopathlen_time = 0.0;
  static double start_offroad_time = 0.0;
  static double start_offroad_xpos = 0;
  static double start_offroad_ypos = 0;
  static bool nopathlen = false;

  // A paramter that keeps track of whether we want to replan/backup/fail
  bool sendReplan = false;
  bool sendBackup = false;
  bool sendFail = false;

  Err_t ret_value = LP_OK;
  double current_velocity;
  double stopping_distance;
  double stopline_distance;
  double obstacle_distance;

  double stopped_duration = Utils::monitorAliceSpeed(vehState, params.m_estop);
  Console::updateStoppedDuration(stopped_duration);
  point2 alice = AliceStateHelper::getPositionFrontBumper(vehState);

  LaneLabel current_lane;
  SegGoals intersectionSegGoals; 

  Utils::updateStoppedMapElements(map);
  if (params.readConfig)
    readConfig();

  char timestr[64];
  time_t logTime = time(NULL);
  strftime(timestr, sizeof(timestr), "%H:%M:%S", localtime(&logTime));
  Log::getStream(1) << "LOGIC_PLANNER: " << timestr << endl;

  Log::getStream(3) << params.seg_goal << endl;

  // Reset variables
  
  double distFromPrevBackup = sqrt( pow(alice.x - just_finished_backup_xpos, 2) + 
				    pow(alice.y - just_finished_backup_ypos, 2));
  if ( distFromPrevBackup > 3*MIN_DIST_BETWEEN_DIFF_BACKUP ) {
    num_backup = 0;
    just_finished_backup_xpos = 0;
    just_finished_backup_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Resetting backup variables" << endl;
  }

  double distFromPrevDrPsRv = sqrt( pow(alice.x - just_finished_drpsrv_xpos, 2) + 
				    pow(alice.y - just_finished_drpsrv_ypos, 2));
  if ( distFromPrevDrPsRv > 3*MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
    num_drpsrv = 0;
    just_finished_drpsrv_xpos = 0;
    just_finished_drpsrv_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Resetting drpsrv variables" << endl;
  }

  double distFromPrevDriveAgg = sqrt( pow(alice.x - just_finished_dagg_xpos, 2) + 
				      pow(alice.y - just_finished_dagg_ypos, 2));
  if ( distFromPrevDriveAgg > 3*MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
    num_dagg = 0;
    just_finished_dagg_xpos = 0;
    just_finished_dagg_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Resetting dagg variables" << endl;
  }

  double distFromPrevDriveBare = sqrt( pow(alice.x - just_finished_dbare_xpos, 2) + 
				       pow(alice.y - just_finished_dbare_ypos, 2));
  if ( distFromPrevDriveBare > 3*MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
    num_dbare = 0;
    just_finished_dbare_xpos = 0;
    just_finished_dbare_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Resetting dbare variables" << endl;
  }

  double distOffRoad = sqrt( pow(alice.x - start_offroad_xpos, 2) + 
			     pow(alice.y - start_offroad_ypos, 2));
  if ( distOffRoad > 3*MIN_DIST_OFFROAD ) {
    start_offroad_xpos = 0;
    start_offroad_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Resetting offroad variables" << endl;
  }

  // Reset intersection if we drive in reverse
  if (params.transpos == -1) {
    Log::getStream(3) << "LOGIC_PLANNER: Shift to reverse. Resetting last_intersection_point" 
		      << endl;
    Console::addMessage("LP: Shift to reverse. Resetting last_intersection_point");
    last_intersection_point = PointLabel(0,0,0);
  }

  Log::getStream(3) << "LOGIC_PLANNER: begin: current region: " 
                    << regionToString(current_region) 
                    << " current lp state: " << lpstateToString(current_lp_state) 
		    << " stopped duration = " << stopped_duration << endl;



  /* NOTE: Do NOT switch the order of these. This is the order of how we handle directive
     1) PAUSE
     2) UTURN
     3) ANY -> ZONE, ZONE -> ROAD
     4) DRIVE -> OFFROAD, OFFROAD -> ROAD
     5) PAUSE/UTURN -> DRIVE_NOPASS
     6) ANY (that's not uturn or offroad) -> BACKUP
  */

  // reset state if command received by console
  if (Console::queryResetStateFlag()) {
    start_current_lp_state_time = Utils::getTime();
    num_drpsrv = 0;
    just_finished_drpsrv_xpos = 0;
    just_finished_drpsrv_ypos = 0;
    num_dagg = 0;
    just_finished_dagg_xpos = 0;
    just_finished_dagg_ypos = 0;
    num_dbare = 0;
    just_finished_dbare_xpos = 0;
    just_finished_dbare_ypos = 0;
    start_offroad_xpos = 0;
    start_offroad_ypos = 0;
    if (current_region == ZONE_REGION) {
      if (zone_id == ZONE_ID_USE_DRIVE_NOPASS)
	current_lp_state = LP_DRIVE_NOPASS;
      else if (zone_id == ZONE_ID_USE_OFFROAD)
	current_lp_state = LP_OFFROAD_SAFETY;
      else
	current_lp_state = LP_ZONE_SAFETY;
    }
    else
      current_lp_state = LP_DRIVE_NOPASS;
    startNewState(vehState, replanInZone);
    Log::getStream(3) << "LOGIC_PLANNER: Got reset from console. Switching to LP_DRIVE_NOPASS, " 
                      << regionToString(current_region) << endl;    
    Console::addMessage("LP: Got reset from console. Switching to LP_DRIVE_NOPASS");
    goto planLogic_end;
  }

  // Do not do anything when we're in estop pause
  if (params.m_estop != EstopRun) {
    start_current_lp_state_time = Utils::getTime();
    num_drpsrv = 0;
    just_finished_drpsrv_xpos = 0;
    just_finished_drpsrv_ypos = 0;
    num_dagg = 0;
    just_finished_dagg_xpos = 0;
    just_finished_dagg_ypos = 0;
    num_dbare = 0;
    just_finished_dbare_xpos = 0;
    just_finished_dbare_ypos = 0;
    start_offroad_xpos = 0;
    start_offroad_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Got ESTOP" << endl;

    if (current_lp_state != LP_PAUSE) {
      if (current_lp_state == LP_STOPINT ||
	  current_lp_state == LP_STOPINT_OFFROAD)
	current_lp_state = LP_STOPINT;
      else if (current_region != ZONE_REGION ||
	  zone_id == ZONE_ID_USE_DRIVE_NOPASS)
	current_lp_state = LP_DRIVE_NOPASS;
      else if (zone_id == ZONE_ID_USE_OFFROAD)
	current_lp_state = LP_OFFROAD_SAFETY;
      else
	current_lp_state = LP_ZONE_SAFETY;
    }
    goto planLogic_end;
  }

  // ANY -> PAUSE
  // Keep the flag identical. current_region stays the same.
  if (params.segment_type == SegGoals::RESET || 
      params.segment_type == SegGoals::EMERGENCY_STOP || 
      params.segment_type == SegGoals::END_OF_MISSION 
      /* || LogicUtils::isObstacleInSafetyBox(vehState, map, 
         ALICE_SAFETY_FRONT, ALICE_SAFETY_REAR, ALICE_SAFETY_SIDE) */) {
    if (current_lp_state != LP_PAUSE) {
      // mplanner tells us to reset, so we'll reset the planner
      paused_lp_state = current_lp_state;
      if (paused_lp_state == LP_STOPOBS_NOPASS ||
          paused_lp_state == LP_UTURN || paused_lp_state == LP_PAUSE)
        paused_lp_state = LP_DRIVE_NOPASS;
      else if (paused_lp_state == LP_STOPOBS_PASS)
        paused_lp_state = LP_DRIVE_PASS;
      else if (paused_lp_state == LP_STOPOBS_PASS_BACKUP ||
               paused_lp_state == LP_BACKUP)
        paused_lp_state = LP_DRIVE_PASS_BACKUP;
      startNewState(vehState, replanInZone);
    }
    if (params.segment_type == SegGoals::RESET) {
      Log::getStream(3)<<"LOGIC_PLANNER: Got RESET directive.";
      Console::addMessage("LP: Got RESET directive");
    }
    else if (params.segment_type == SegGoals::EMERGENCY_STOP) {
      Log::getStream(3)<<"LOGIC_PLANNER: Got EMERGENCY_STOP directive.";
      Console::addMessage("LP: Got EMERGENCY_STOP directive");
    }
    else {
      Log::getStream(3)<<"LOGIC_PLANNER: Got END_OF_MISSION directive.";
      Console::addMessage("LP: Got END_OF_MISSION directive");
    }
      
    current_lp_state = LP_PAUSE;
    Log::getStream(3) << " Switching to LP_PAUSE. paused_lp_state = " 
		      << lpstateToString(paused_lp_state) << endl;
    goto planLogic_end;
  }

  // DARPA_BALL mode
  // Treat evasion mode as we get ESTOP
  if (params.evasion) {
    startNewState(vehState, replanInZone);
    Log::getStream(3) << "LOGIC_PLANNER: params.evasion = true" << endl;
    Console::addMessage("LP: In DARPA BALL mode");
    stopped_duration = Utils::monitorAliceSpeed(vehState, EstopPause);

    if (current_lp_state != LP_PAUSE) {
      if (current_region != ZONE_REGION || zone_id == ZONE_ID_USE_DRIVE_NOPASS) {
	if (prevErr & PP_COLLISION_BARE)
	  current_lp_state = LP_STOPOBS_NOPASS;
	else
	  current_lp_state = LP_DRIVE_NOPASS;
      }
      else if (zone_id == ZONE_ID_USE_OFFROAD) {
	if (prevErr & PP_COLLISION_BARE)
	  current_lp_state = LP_STOPOBS_OFFROAD_SAFETY;
	else
	  current_lp_state = LP_OFFROAD_SAFETY;
      }
      else
	current_lp_state = LP_ZONE_SAFETY;
    }
    goto planLogic_end;
  }


  // ANY -> UTURN (make sure we are stationary)
  if (params.segment_type == SegGoals::UTURN && stopped_duration > 1.0) {
    if (current_lp_state != LP_UTURN)
      startNewState(vehState, replanInZone);

    current_lp_state = LP_UTURN;
    current_region = ROAD_REGION;
    Log::getStream(3)<<"LOGIC_PLANNER: Got UTURN directive."
                     << " Switching to LP_UTURN." << endl;
    Console::addMessage("LP: Got UTURN directive");
    goto planLogic_end;
  }

  // ANY -> ZONE
  if ((params.seg_goal.exitSegmentID > (int)(map->prior.segments.size()) &&
       params.seg_goal.exitSegmentID != (int)zone_id) ||
      (params.seg_goal.entrySegmentID > (int)(map->prior.segments.size()) &&
       params.seg_goal.entrySegmentID != zone_id)) {
    point2arr zonePerim; 
    map->getZonePerimeter(zonePerim, params.seg_goal.exitSegmentID);

    /* Rotate and translate to alice real position */
    point2 pt = AliceStateHelper::getPositionRearAxle(vehState);

    Log::getStream(3)<<"LOGIC_PLANNER: Got ZONE directive."
                     << " exitSegmentID = " << params.seg_goal.exitSegmentID
                     << " entrySegmentID = " << params.seg_goal.entrySegmentID
                     << " zoneid = " << zone_id << endl;

    /* Check if Alice rear axle is inside the zone */
    if (zonePerim.is_inside_poly(pt)) {
      if (current_region != ZONE_REGION)
	startNewState(vehState, replanInZone);

      if (params.seg_goal.exitSegmentID > (int)(map->prior.segments.size()))
        zone_id = params.seg_goal.exitSegmentID;
      else
        zone_id = params.seg_goal.entrySegmentID;

      if (USE_OFFROAD_ZONE &&
	  ((params.seg_goal.exitSegmentID > (int)(map->prior.segments.size()) && 
	    params.seg_goal.exitLaneID != 0 && params.seg_goal.isExitCheckpoint) ||
	   (params.seg_goal_queue->size() > 1 && 
	    (params.seg_goal_queue->at(1)).exitSegmentID > (int)(map->prior.segments.size()) &&
	    (params.seg_goal_queue->at(1)).exitLaneID != 0 && 
	    (params.seg_goal_queue->at(1)).isExitCheckpoint))) {
	current_lp_state = LP_DRIVE_NOPASS;
	current_region = ZONE_REGION;
	switch_to_s1planner = true;
      }
      else if (zone_id == ZONE_ID_USE_DRIVE_NOPASS) {
	current_lp_state = LP_DRIVE_NOPASS;
	current_region = ZONE_REGION;
      }
      else if (zone_id == ZONE_ID_USE_OFFROAD) {
	current_lp_state = LP_OFFROAD_SAFETY;
	current_region = ZONE_REGION;
      }
      else {
	current_lp_state = LP_PAUSE;
	current_region = ZONE_REGION;
	/*
	if ((params.seg_goal.exitLaneID == 0 && params.seg_goal.entryLaneID == 0 && params.seg_goal.isSparse) ||
	    (params.seg_goal_queue->size() > 1 && params.seg_goal_queue->at(1).exitLaneID == 0 &&
	     params.seg_goal_queue->at(1).entryLaneID == 0 && params.seg_goal_queue->at(1).isSparse)) {
	  paused_lp_state = LP_OFFROAD_SAFETY;
	}
	else
	*/
	paused_lp_state = LP_ZONE_SAFETY;
      }

      Log::getStream(3) << "LOGIC_PLANNER: Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << " paused_lp_state = " << lpstateToString(paused_lp_state)
			<< " zoneid = " << zone_id << endl;
      Console::addMessage("LP: switch to zone, id = %d", zone_id);
      goto planLogic_end;
    }
    else {
      Log::getStream(3) << "LOGIC_PLANNER: zonePerim.is_inside_poly(pt) = false. Not switching to ZONE_REGION" << endl;
    } 
  }

  // Switch to s1planner in zone when we're close enough to the parking spot
  if (switch_to_s1planner) {
    Log::getStream(3) << "LOGIC_PLANNER: Using OFFROAD in ZONE. Checking if we should switch to s1planner" << endl;
    if (current_region == ZONE_REGION && current_planner == RAIL_PLANNER) {
      if (params.m_path->pathLen < 2 || params.m_path->dists[params.m_path->pathLen - 1] < 30) {
	if (params.m_path->pathLen < 2)
	  Log::getStream(3) << "LOGIC_PLANNER: pathLen = " << params.m_path->pathLen << endl;
	else
	  Log::getStream(3) << "LOGIC_PLANNER: distToGoal = " << params.m_path->dists[params.m_path->pathLen - 1] << endl;
	current_lp_state = LP_PAUSE;
	current_region = ZONE_REGION;
	paused_lp_state = LP_ZONE_SAFETY;
	startNewState(vehState, replanInZone);
	switch_to_s1planner = false;
	goto planLogic_end;
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: distToGoal = " << params.m_path->dists[params.m_path->pathLen - 1]
			  << ". Too far from goal to switch to s1planner." << endl;
      }
    }
    else {
      Log::getStream(3) << "LOGIC_PLANNER: current_region = " << regionToString(current_region)
			<< " current_planner = " << plannerToString(current_planner)
			<< ". Setting switch_to_s1planner to false" << endl;
      switch_to_s1planner = false;
    }
  }

 
  // ZONE -> DRIVE_NOPASS (E.G. SWITCHING OUT OF ZONE because completed goal)
  if (current_region == ZONE_REGION && params.seg_goal.exitSegmentID != zone_id) {
    current_lp_state = LP_DRIVE_NOPASS;
    current_region = ROAD_REGION;
    zone_id = 0;
    switch_to_s1planner = false;
    startNewState(vehState, replanInZone);
    Log::getStream(3) << "LOGIC_PLANNER: Switching from ZONE_REGION to LP_DRIVE_NOPASS, ROAD_REGION" << endl;
    Console::addMessage("LP: Switching out of zone");
    goto planLogic_end;
  }


  // DRIVE -> OFFROAD if we're in sparse waypoint section
  if (current_state == DRIVE &&
      current_flag != OFFROAD && current_lp_state != LP_BACKUP &&
      //current_planner != S1PLANNER &&
      (params.seg_goal.exitSegmentID == CmdArgs::sparse || params.seg_goal.isSparse)) {
    Log::getStream(3)<<"LOGIC_PLANNER: In sparse waypoint area" << endl;
    Console::addMessage("LP: Switching to OFFROAD");
    startNewState(vehState, replanInZone);
    current_lp_state = LP_OFFROAD_SAFETY;
    //current_region = ROAD_REGION;
    sendReplan = true;
    nopathlen = false;
    goto planLogic_end;
  }


  // DRIVE -> OFFROAD if path planner cannot find a path, try offroad mode
  if (current_planner == RAIL_PLANNER && 
      current_flag != OFFROAD && current_lp_state != LP_BACKUP && 
      current_lp_state != LP_UTURN &&
      params.seg_goal.segment_type == SegGoals::ROAD_SEGMENT &&
      (prevErr & PP_NOPATH_LEN)) {
    if (USE_OFFROAD && (Utils::getTime() - last_nopathlen_time)/1000000.0 > TIMEOUT_NOPATHLEN) {
      Log::getStream(3)<<"LOGIC_PLANNER: path-planner returns PP_NOPATH_LEN" << endl;
      Console::addMessage("LP: Got PP_NOPATH_LEN. Switching to OFFROAD");
      startNewState(vehState, replanInZone);
      start_offroad_time = Utils::getTime();
      start_offroad_xpos = alice.x;
      start_offroad_ypos = alice.y;
      current_lp_state = LP_OFFROAD_SAFETY;
      //current_region = ROAD_REGION;
      sendReplan = true;
      nopathlen = true;
      goto planLogic_end;  
    }
  }
  else
    last_nopathlen_time = Utils::getTime();    


  // OFFROAD -> DRIVE_NOPASS
  if (current_flag == OFFROAD && 
      ((!nopathlen && params.seg_goal.exitSegmentID != CmdArgs::sparse &&
	!params.seg_goal.isSparse && zone_id != ZONE_ID_USE_OFFROAD) ||
       (nopathlen && (Utils::getTime() - start_offroad_time)/1000000.0 > TIMEOUT_OFFROAD &&
	distOffRoad > MIN_DIST_OFFROAD) ||
       (nopathlen && !USE_OFFROAD && !USE_OFFROAD_ONELANE))) {
    Log::getStream(3)<<"LOGIC_PLANNER: Exiting sparse waypoint area. nopathlen = "
		     << nopathlen << " time in offroad = "
		     << (Utils::getTime() - start_offroad_time)/1000000.0
		     << " dist in offroad = " << distOffRoad << endl;
    Console::addMessage("LP: Finished OFFROAD. Switching to DRIVE_NOPASS");
    startNewState(vehState, replanInZone);
    if (current_region != ZONE_REGION ||
	zone_id == ZONE_ID_USE_DRIVE_NOPASS)
      current_lp_state = LP_DRIVE_NOPASS;
    else if (zone_id == ZONE_ID_USE_OFFROAD)
      current_lp_state = LP_OFFROAD_SAFETY;
    else
      current_lp_state = LP_ZONE_SAFETY;
    //current_region = ROAD_REGION;
    sendReplan = true;
    nopathlen = false;
    goto planLogic_end;
  }

  // PAUSE/UTURN -> DRIVE_NOPASS (E.G. SWITCHING OUT OF pause/uturn because mplanner replanned
  // and send us a new goal)
  if (current_lp_state == LP_UTURN || current_lp_state == LP_PAUSE) {
    if (params.seg_goal.segment_type != SegGoals::RESET &&
	params.seg_goal.segment_type != SegGoals::EMERGENCY_STOP &&
	params.seg_goal.segment_type != SegGoals::UTURN &&
	params.seg_goal.segment_type != SegGoals::BACK_UP &&
	params.seg_goal.segment_type != SegGoals::END_OF_MISSION) {
      current_velocity = AliceStateHelper::getVelocityMag(vehState);
      if (current_lp_state == LP_PAUSE) {
	// If we're stopped, switch to the state specified by paused_lp_state
	if (current_velocity < STOP_VEL) {
	  if (paused_lp_state != LP_PAUSE && paused_lp_state != LP_UTURN &&
	      paused_lp_state != LP_STOPINT && paused_lp_state != LP_BACKUP)
	    current_lp_state = paused_lp_state;
	  else if (paused_lp_state == LP_BACKUP)
	    current_lp_state = LP_DRIVE_PASS_BACKUP;
	  else
	    current_lp_state = LP_DRIVE_NOPASS;
	  startNewState(vehState, replanInZone);
	  if (paused_lp_state == LP_ZONE_SAFETY)
	    replanInZone = false;
	}
      }
      else {
	current_lp_state = LP_DRIVE_NOPASS;
	startNewState(vehState, replanInZone);
      }
    }

    sendReplan = true;
    Log::getStream(3)<<"LOGIC_PLANNER: Got DRIVE directive."
                     << " Switching to " << lpstateToString(current_lp_state) << endl;
    Console::addMessage("LP: PAUSE/UTURN finished");
    goto planLogic_end;
  }


  // ANY -> BACKUP (E.G. switching to backup because mplanner wants to back up and replan)
  if (params.seg_goal.segment_type == SegGoals::BACK_UP) {
    // Can only backup when we're on a road and not in the offroad mode
    if (current_region == ROAD_REGION && current_flag != OFFROAD && ALLOW_BACKUP) {
      current_lp_state = LP_BACKUP;
      backup_lp_state = LP_DRIVE_PASS_BACKUP;
      startNewState(vehState, replanInZone);
      Log::getStream(3)<<"LOGIC_PLANNER: Got BACKUP directive."
		       << " Switching to " << lpstateToString(current_lp_state) << ", " 
		       << regionToString(current_region)
		       << ". backup_lp_state = " << backup_lp_state << endl;
    }
    else {
      startNewState(vehState, replanInZone);
      paused_lp_state = current_lp_state;
      current_lp_state = LP_PAUSE;
      current_lane = Utils::getCurrentLane(vehState, graph);
      currentSegmentId = current_lane.segment;
      currentLaneId = current_lane.lane;
      Log::getStream(3) << "LOGIC_PLANNER: Got BACKUP directive when current_region =  "
			<< regionToString(current_region) << " flag = "
			<< flagToString(current_flag) << ". Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			<< currentSegmentId << " currentLaneId = " << currentLaneId
			<< " paused_lp_state = " << lpstateToString(paused_lp_state)
			<< endl;
      Console::addMessage("LP: In %s, %s, cannot backup", regionToString(current_region).c_str(), 
			  flagToString(current_flag).c_str());
      // Notify MPlanner that we "failed"
      ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
      return ret_value;
    }
    goto planLogic_end;
  }

  IntersectionHandling->updateConsole();

  
  ///////////////////////////////////////////////////////////////////////////////////////////////
  // check INTERSECTION
  ///////////////////////////////////////////////////////////////////////////////////////////////
  // First, reset intersection

  if (current_state == DRIVE || current_state == STOP_OBS) {
    // Set useful variables
    current_velocity = AliceStateHelper::getVelocityMag(vehState);
    stopping_distance = pow(current_velocity,2)/(2.0*DESIRED_DECELERATION);
    stopline_distance = Utils::getDistToStopline(params.m_path, params.seg_goal_queue, intersectionSegGoals);
    Console::updateStopline(stopline_distance);
    if (params.m_path->hasStop)
      Utils::displayCircle(-30, point2(2.0, stopline_distance), 1.0, 3, MAP_COLOR_YELLOW, 103);
    else
      Utils::displayClear(-30, 3, 103);
    current_lane = Utils::getCurrentLane(vehState, graph);
    Log::getStream(5) << "LOGIC_PLANNER: stopline_distance = " << stopline_distance << endl;

    // DRIVE -> STOP_INT or STOP_OBS -> STOP_INT
    if ((current_state == DRIVE && stopline_distance > -2.0 && 
	 stopping_distance + INTERSECTION_BUFFER >= stopline_distance) ||
	(current_state == STOP_OBS && 
	 Utils::getNearestObsOnPath(params.m_path, graph, vehState) +
	 2 * DIST_NOCHECK_OBS_STOPINT > stopline_distance &&
	 (stopping_distance + INTERSECTION_BUFFER >= stopline_distance) &&
	 //Utils::getNearestObsDist(vehState, map, current_lane) > stopline_distance &&
	 stopline_distance > -2.0)) {
      PointLabel new_intersection_point = PointLabel(intersectionSegGoals.entrySegmentID, 
                                                     intersectionSegGoals.entryLaneID, intersectionSegGoals.entryWaypointID);
      if (intersectionSegGoals.segment_type != SegGoals::INTERSECTION)
        new_intersection_point = PointLabel(intersectionSegGoals.exitSegmentID, 
                                            intersectionSegGoals.exitLaneID, intersectionSegGoals.exitWaypointID);

      uint64_t new_intersection_time = Utils::getTime();

      /*
      if (!(new_intersection_point == last_intersection_point) || 
	  (new_intersection_time - last_intersection_time)/1000000.0 > TIME_CLEAR_INTERSECTION)
	numCheckCurrentIntersection = 0;
      else
	numCheckCurrentIntersection++;
      Log::getStream(5) << "LOGIC_PLANNER: numCheckCurrentIntersection = " << numCheckCurrentIntersection
			<< endl;
      */

      Log::getStream(3) << " New intersection point: " << new_intersection_point.segment
			<< "." << new_intersection_point.lane << "." << new_intersection_point.point
			<< " Last intersection point: " << last_intersection_point.segment
			<< "." << last_intersection_point.lane << "." << last_intersection_point.point
			<< " Time since last intersection: " 
			<< (new_intersection_time - last_intersection_time)/1000000.0 << " s" << endl;

      if (!(new_intersection_point == last_intersection_point) || 
	  (new_intersection_time - last_intersection_time)/1000000.0 > TIME_CLEAR_INTERSECTION) {
	Log::getStream(3) << "LOGIC_PLANNER: Resetting IntersectionHandling" << endl;
        IntersectionHandling->reset(graph,map);
	inter_ret = CIntersectionHandling::RESET;
	if (current_lp_state == LP_OFFROAD_SAFETY || current_lp_state == LP_STOPOBS_OFFROAD_SAFETY ||
	    current_lp_state == LP_OFFROAD_AGG || current_lp_state == LP_STOPOBS_OFFROAD_AGG || 
	    current_lp_state == LP_OFFROAD_BARE || current_lp_state == LP_STOPOBS_OFFROAD_BARE)
	  current_lp_state = LP_STOPINT_OFFROAD;
	else
	  current_lp_state = LP_STOPINT;
        //current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
        Log::getStream(3) << "LOGIC_PLANNER: See intersection. Switching to LP_STOPINT, ROAD_REGION" 
                          << endl;
        goto planLogic_end;
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: Same intersection as before" << endl;
      }
    }

    // If a ROW intersection is in front of Alice, change PlanGraphNode (as Stopline is not in the RNDF)
    bool found = false;
    for (unsigned int i= 0; i < params.seg_goal_queue->size(); i++) {      
      // only check the first three goal in queue
      if (i>3) break;
      SegGoals goal =  (*(params.seg_goal_queue))[i];
      PointLabel waypoint = PointLabel(goal.entrySegmentID, goal.entryLaneID, goal.entryWaypointID);
      
      // if an intersection with a planned left turn is ahead, check as well if there is no stopline
      vector<PointLabel> wpEntries, wpExits;
      map->getWayPointEntries(wpEntries, PointLabel(goal.exitSegmentID, goal.exitLaneID, goal.exitWaypointID));
      map->getWayPointEntries(wpExits, waypoint);
      
      if (wpEntries.size() == 1 && wpExits.size() == 1)
	continue;

      if (SegGoals::INTERSECTION == goal.segment_type && 
          SegGoals::INTERSECTION_LEFT == goal.intersection_type && !map->isStopLine(waypoint) && wpEntries.size()>1) {
        params.m_path->hasRow = true;
        PlanGraphNode* node = graph->getWaypoint(goal.entrySegmentID, goal.entryLaneID, goal.entryWaypointID);
        params.m_path->rowWaypoint = node->nextWaypoint;
        found = true;
        break;
      }      
    }
    if (!found)
      params.m_path->hasRow = false;
  }

  // ANY (that is not STOP_INT) -> START_CHUTE
  if (params.seg_goal.segment_type == SegGoals::START_CHUTE && current_lp_state != LP_STOPINT) {
    if (current_region != ZONE_REGION ||
	zone_id == ZONE_ID_USE_DRIVE_NOPASS)
      current_lp_state = LP_DRIVE_NOPASS;
    else if (zone_id == ZONE_ID_USE_OFFROAD)
      current_lp_state = LP_OFFROAD_SAFETY;
    else
      current_lp_state = LP_ZONE_SAFETY;
    startNewState(vehState, replanInZone);
    Log::getStream(3) << "LOGIC_PLANNER: Got START_CHUTE directive"
		      << ". Switching to " << lpstateToString(current_lp_state) << ", " 
		     << regionToString(current_region) << endl;
    Console::addMessage("LP: In START_CHUTE mode. GOOD LUCK!!!");
    goto planLogic_end;
  }



  Log::getStream(3) << "LOGIC_PLANNER: current region: " 
                    << regionToString(current_region) 
                    << " current lp state: " << lpstateToString(current_lp_state) 
		    << " stopped duration = " << stopped_duration << endl;


  ///////////////////////////////////////////////////////////////////////////////////////////////
  // Now check whether we're in the same path mplanner thinks we're using
  ///////////////////////////////////////////////////////////////////////////////////////////////
  //#if (0)
  if ((Utils::getTime() - last_report_lost_time)/1000000.0 > MIN_TIME_LOST &&
      current_lp_state != LP_STOPINT &&
      current_planner == RAIL_PLANNER && 
      // params.seg_goal.entrySegmentID != 0 &&
      params.m_path->pathLen > 2 && 
      ((params.segment_type != SegGoals::RESET && 
        params.segment_type != SegGoals::END_OF_MISSION &&
        params.segment_type != SegGoals::EMERGENCY_STOP &&
        params.segment_type != SegGoals::DRIVE_AROUND &&
        params.segment_type != SegGoals::BACK_UP) ||
       params.segment_type == SegGoals::UNKNOWN) ) {
    PlanGraphNode *node;
    RNDFGraphWaypoint *wp;
    for (int i = 1; i < params.m_path->pathLen && params.m_path->dists[i] < 50; i++) {
      node = params.m_path->nodes[i];
      if (node == NULL || params.m_path->dists[i] < 2)
	continue;
      wp = node->nextWaypoint;

      if (wp != NULL && 
	  wp->segmentId != params.seg_goal.entrySegmentID &&
	  wp->segmentId != params.seg_goal.exitSegmentID && 
	  params.seg_goal_queue->size() > 1 &&
	  wp->segmentId != params.seg_goal_queue->at(1).entrySegmentID && 
	  wp->segmentId != params.seg_goal_queue->at(1).exitSegmentID &&
	  wp->segmentId != last_report_lost_segment_id &&
	  wp->segmentId <= (int)(map->prior.segments.size()) &&
	  !wp->flags.isExit &&
	  !wp->flags.isEntry) {
	paused_lp_state = current_lp_state;
	current_lp_state = LP_PAUSE;
	currentSegmentId = wp->segmentId;
	currentLaneId = 0;
	ret_value |= LP_FAIL_MPLANNER_LOST;
	last_report_lost_time = Utils::getTime();
	last_report_lost_segment_id = currentSegmentId;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: we're lost. Return sendFail" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << ". Send LP_FAIL_MPLANNER_LOST with currentSegmentId = " 
			  << currentSegmentId << " currentLaneId = " << currentLaneId
			  << "(" << currentSegmentId << "." << wp->laneId << "."
			  << wp->waypointId << ")"
			  << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	Console::addMessage("We're lost. current segment = %d", currentSegmentId);
	return ret_value;	  
      }
      else if (wp != NULL) {
	Log::getStream(5) << "LOGIC_PLANNER: currentSegmentId = " 
			  << wp->segmentId << " currentLaneId = " << wp->laneId
			  << " currentWaypointId = " << wp->waypointId
			  << endl;
	break;
      }
    }
  }
  //#endif



  /* Taken care of above already
  ///////////////////////////////////////////////////////////////////////////////////////////////
  // When we're in PAUSE
  ///////////////////////////////////////////////////////////////////////////////////////////////
  if (current_lp_state == LP_PAUSE) {
    // Set useful variables
    current_velocity = AliceStateHelper::getVelocityMag(vehState);

    // If we're stopped, switch to the state specified by paused_lp_state
    if (current_velocity < STOP_VEL && params.segment_type != SegGoals::RESET && 
        params.segment_type != SegGoals::EMERGENCY_STOP) {
      current_lp_state = paused_lp_state;
      startNewState(vehState, replanInZone);
      sendReplan = true;
      Log::getStream(3) << "LOGIC_PLANNER: Finished PAUSE" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: Finished PAUSE");
    }
    goto planLogic_end;
  }
  */



  ///////////////////////////////////////////////////////////////////////////////////////////////
  // When we're in DRIVE_OFFROAD or STOPOBS_OFFROAD
  ///////////////////////////////////////////////////////////////////////////////////////////////
  if (current_flag == OFFROAD && current_lp_state != LP_STOPINT_OFFROAD) {
    switch (current_lp_state) {
    case LP_OFFROAD_SAFETY: {
      // OFFROAD_SAFETY -> STOPOBS_OFFROAD_SAFETY
      if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
	// The path-planner could not plan, so we bring the car to a stop
	current_lp_state = LP_STOPOBS_OFFROAD_SAFETY;
	//current_region = ROAD_REGION;
	last_obs_time = Utils::getTime();
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In OFFROAD_SAFETY: prevErr = PP_COLLISION_SAFETY");
      }
      break;
    }

    case LP_OFFROAD_AGG: {
      // OFFROAD_AGG -> STOPOBS_OFFROAD_AGG
      if ((prevErr & PP_COLLISION_AGGRESSIVE) || (prevErr & PP_NOPATH_LEN)) {
	// The path-planner could not plan, so we bring the car to a stop
	current_lp_state = LP_STOPOBS_OFFROAD_AGG;
	//current_region = ROAD_REGION;
	last_obs_time = Utils::getTime();
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In OFFROAD_AGG: prevErr = PP_COLLISION_AGGRESSIVE");
      }
      // OFFROAD_AGG -> OFFROAD_SAFETY
      /*
      else if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS ||
	       sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		     pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_SMALL_OBS) {
      */
      else if (((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS) &&
	       !(prevErr & PP_COLLISION_SAFETY)) {
	current_lp_state = LP_OFFROAD_SAFETY;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: OFFROAD_AGG timeout (time = "
			  << (Utils::getTime() - start_current_lp_state_time)/1000000.0
			  << " s, dist = " 
			  << sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
				   pow(alice.y - start_current_lp_state_ypos, 2))
			  << " m. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In OFFROAD_AGG: Timeout and !PP_COLLISION_SAFETY. Switching to OFFROAD_SAFETY");
      }
      break;
    }

    case LP_OFFROAD_BARE: {
      // OFFROAD_BARE -> STOPOBS_OFFROAD_BARE
      if ((prevErr & PP_COLLISION_BARE) || (prevErr & PP_NOPATH_LEN)) {
	// The path-planner could not plan, so we bring the car to a stop
	current_lp_state = LP_STOPOBS_OFFROAD_BARE;
	//current_region = ROAD_REGION;
	last_obs_time = Utils::getTime();
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In OFFROAD_BARE: prevErr = PP_COLLISION_BARE");
      }
      // OFFROAD_BARE -> OFFROAD_SAFETY
      /*
      else if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS ||
	       sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		     pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_SMALL_OBS) {
      */
      else if (((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS) &&
	       !(prevErr & PP_COLLISION_AGGRESSIVE)) {
	current_lp_state = LP_OFFROAD_AGG;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: OFFROAD_BARE timeout (time = "
			  << (Utils::getTime() - start_current_lp_state_time)/1000000.0
			  << " s, dist = " 
			  << sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
				   pow(alice.y - start_current_lp_state_ypos, 2))
			  << " m. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In OFFROAD_BARE: Timeout and !PP_COLLISION_AGGRESSIVE. Switching to OFFROAD_AGG");
      }
      break;
    }

    case LP_STOPOBS_OFFROAD_SAFETY: {
      if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN))
	last_obs_time = Utils::getTime();
      // STOPOBS_OFFROAD_SAFETY -> OFFROAD_SAFETY: no obstacles anymore
      else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
	// If path-planner is now able to plan -> no obstacles
	current_lp_state = LP_OFFROAD_SAFETY;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: no more obstacle in front. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In STOPOBS_OFFROAD_SAFETY: obstacle went away");
	break;
      }
      if (((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) &&
	  (Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_SHORT &&
	  stopped_duration > MIN_TIME_STOPOBS_SHORT) {
	Console::addMessage("LP: In STOPOBS_OFFROAD_SAFETY: prevErr = PP_COLLISION_SAFETY");

	// STOPOBS_OFFROAD_SAFETY -> STOPOBS_OFFROAD_AGG
	if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	  current_lp_state = LP_STOPOBS_AGG;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: path-planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	// STOPOBS_OFFROAD_SAFETY -> DRIVE_S1PLANNER_SAFETY
	else if (USE_S1PLANNER_ON_ROAD ) {
	  current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: path-planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	else {
	  if (backup_lp_state != current_lp_state) {
	    num_s1planner_backup = 0;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: Resetting num_s1planner_backup = 0" << endl;
	  }
	  else {
	    num_s1planner_backup++;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: Incrementing num_s1planner_backup = " 
			      << num_s1planner_backup << endl;
	  }
	
	  // STOPOBS_OFFROAD_SAFETY -> BACKUP
	  if (num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP && ALLOW_BACKUP) {
	    backup_lp_state = current_lp_state;
	    current_lp_state = LP_BACKUP;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: path-planner could not plan" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". backup_lp_state = " << backup_lp_state << endl;
	  }
	  // STOPOBS_OFFROAD_SAFETY -> UTURN
	  else {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_STOPOBS_OFFROAD_SAFETY;
	    startNewState(vehState, replanInZone);
	  
	    current_lane = Utils::getCurrentLane(vehState, graph);
	    currentSegmentId = current_lane.segment;
	    currentLaneId = current_lane.lane;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_SAFETY: path-planner could not plan " << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			      << currentSegmentId << " currentLaneId = " << currentLaneId
			      << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	  }
	}
      }
      break;
    }

    case LP_STOPOBS_OFFROAD_AGG: {
      if ((prevErr & PP_COLLISION_AGGRESSIVE) || (prevErr & PP_NOPATH_LEN))
	last_obs_time = Utils::getTime();
      // STOPOBS_OFFROAD_AGG -> OFFROAD_AGG: no obstacles anymore
      else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
	// If path-planner is now able to plan -> no obstacles
	current_lp_state = LP_OFFROAD_AGG;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: no more obstacle in front. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In STOPOBS_OFFROAD_AGG: obstacle went away");
	break;
      }
      if (((prevErr & PP_COLLISION_AGGRESSIVE) || (prevErr & PP_NOPATH_LEN)) &&
	  (Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_LONG &&
	  stopped_duration > MIN_TIME_STOPOBS_LONG) {
	Console::addMessage("LP: In STOPOBS_OFFROAD_AGG: prevErr = PP_COLLISION_AGGRESSIVE");

	// STOPOBS_OFFROAD_AGG -> STOPOBS_OFFROAD_BARE
	if (NUM_CHANGE_RP_OBS_SIZE > 1) {
	  current_lp_state = LP_STOPOBS_BARE;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: path-planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	// STOPOBS_OFFROAD_AGG -> DRIVE_S1PLANNER_SAFETY
	else if (USE_S1PLANNER_ON_ROAD ) {
	  current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: path-planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	else {
	  if (backup_lp_state != current_lp_state) {
	    num_s1planner_backup = 0;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: Resetting num_s1planner_backup = 0" << endl;
	  }
	  else {
	    num_s1planner_backup++;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: Incrementing num_s1planner_backup = " 
			      << num_s1planner_backup << endl;
	  }
	
	  // STOPOBS_OFFROAD_AGG -> BACKUP
	  if (num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP && ALLOW_BACKUP) {
	    backup_lp_state = current_lp_state;
	    current_lp_state = LP_BACKUP;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: path-planner could not plan" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". backup_lp_state = " << backup_lp_state << endl;
	  }
	  // STOPOBS_OFFROAD_AGG -> UTURN
	  else {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_STOPOBS_OFFROAD_SAFETY;
	    startNewState(vehState, replanInZone);
	  
	    current_lane = Utils::getCurrentLane(vehState, graph);
	    currentSegmentId = current_lane.segment;
	    currentLaneId = current_lane.lane;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_AGG: path-planner could not plan " << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			      << currentSegmentId << " currentLaneId = " << currentLaneId
			      << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	  }
	}
      }
      break;
    }

    case LP_STOPOBS_OFFROAD_BARE: {
      if ((prevErr & PP_COLLISION_BARE) || (prevErr & PP_NOPATH_LEN))
	last_obs_time = Utils::getTime();
      // STOPOBS_OFFROAD_BARE -> OFFROAD_BARE: no obstacles anymore
      else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
	// If path-planner is now able to plan -> no obstacles
	current_lp_state = LP_OFFROAD_BARE;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_BARE: no more obstacle in front. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In STOPOBS_OFFROAD_BARE: obstacle went away");
	break;
      }
      if (((prevErr & PP_COLLISION_BARE) || (prevErr & PP_NOPATH_LEN)) &&
	  (Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_LONG &&
	  stopped_duration > MIN_TIME_STOPOBS_LONG) {
	Console::addMessage("LP: In STOPOBS_OFFROAD_BARE: prevErr = PP_COLLISION_BARE");

	// STOPOBS_OFFROAD_BARE -> DRIVE_S1PLANNER_SAFETY
	if (USE_S1PLANNER_ON_ROAD ) {
	  current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_BARE: path-planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	else {
	  if (backup_lp_state != current_lp_state) {
	    num_s1planner_backup = 0;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_BARE: Resetting num_s1planner_backup = 0" << endl;
	  }
	  else {
	    num_s1planner_backup++;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_BARE: Incrementing num_s1planner_backup = " 
			      << num_s1planner_backup << endl;
	  }
	
	  // STOPOBS_OFFROAD_BARE -> BACKUP
	  if (num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP && ALLOW_BACKUP) {
	    backup_lp_state = current_lp_state;
	    current_lp_state = LP_BACKUP;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_BARE: path-planner could not plan" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". backup_lp_state = " << backup_lp_state << endl;
	  }
	  // STOPOBS_OFFROAD_BARE -> UTURN
	  else {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_STOPOBS_OFFROAD_SAFETY;
	    startNewState(vehState, replanInZone);
	  
	    current_lane = Utils::getCurrentLane(vehState, graph);
	    currentSegmentId = current_lane.segment;
	    currentLaneId = current_lane.lane;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	    Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_OFFROAD_BARE: path-planner could not plan " << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			      << currentSegmentId << " currentLaneId = " << currentLaneId
			      << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	  }
	}
      }
      break;
    }
    default: {
      Log::getStream(1) << "ERROR: LOGIC_PLANNER: Flag = OFFROAD but current_lp_state = "
			<< lpstateToString(current_lp_state) << " is not handled." << endl;
      Console::addMessage("ERROR: LOGIC_PLANNER: Flag = OFFROAD but current_lp_state is not handled.");
      current_lp_state = LP_OFFROAD_SAFETY;
      startNewState(vehState, replanInZone);
      break;
    }
    }
    
    sendReplan = true;
    goto planLogic_end;
  }



  ///////////////////////////////////////////////////////////////////////////////////////////////
  // When we're in ZONE_REGION
  ///////////////////////////////////////////////////////////////////////////////////////////////
  if (current_region == ZONE_REGION && (current_planner == S1PLANNER ||
					current_planner == DPLANNER ||
					current_planner == CIRCLE_PLANNER)) {
    if ((prevErr & S1PLANNER_FAILED) ||
	(prevErr & S1PLANNER_START_BLOCKED) ||
        (prevErr & S1PLANNER_END_BLOCKED) ) {
      if (prevErr & S1PLANNER_FAILED) {
        Log::getStream(3) << "LOGIC_PLANNER: S1PLANNER_FAILED" << endl;
        Console::addMessage("LP: S1PLANNER_FAILED");
      }
      if (prevErr & S1PLANNER_START_BLOCKED) {
        Log::getStream(3) << "LOGIC_PLANNER: S1PLANNER_START_BLOCKED" << endl;
        Console::addMessage("LP: S1PLANNER_START_BLOCKED");
      }
      if (prevErr & S1PLANNER_END_BLOCKED) {
        Log::getStream(3) << "LOGIC_PLANNER: S1PLANNER_END_BLOCKED" << endl;
        Console::addMessage("LP: S1PLANNER_END_BLOCKED");
      }
      if (prevErr & S1PLANNER_COST_TOO_HIGH) {
        Log::getStream(3) << "LOGIC_PLANNER: S1PLANNER_COST_TOO_HIGH" << endl;
        Console::addMessage("LP: S1PLANNER_COST_TOO_HIGH");
      }

      if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_IN_DRIVE_S1PLANNER) {
	switch (current_lp_state) {
	case LP_ZONE_SAFETY:
	  // LP_ZONE_SAFETY -> LP_ZONE_AGG
	  if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	    current_lp_state = LP_ZONE_AGG;
	    startNewState(vehState, replanInZone);
	  }
	  
	  // LP_ZONE_SAFETY -> FAIL: Wait for certain amount of time to make sure that we really can't do it.
	  else if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_ZONE_SAFETY;
	    startNewState(vehState, replanInZone);

	    currentSegmentId = zone_id;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  }
	  break;
	case LP_ZONE_AGG:
	  // LP_ZONE_AGG -> LP_ZONE_BARE
	  if (NUM_CHANGE_RP_OBS_SIZE > 1) {	
	    current_lp_state = LP_ZONE_BARE;
	    startNewState(vehState, replanInZone);
	  }
	  // LP_ZONE_AGG -> FAIL: Wait for certain amount of time to make sure that we really can't do it.
	  else if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_ZONE_AGG;
	    startNewState(vehState, replanInZone);
	    currentSegmentId = zone_id;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  }
	  break;
	case LP_ZONE_BARE:
	  // LP_ZONE_BARE -> FAIL: Wait for certain amount of time to make sure that we really can't do it.
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_ZONE_BARE;
	    startNewState(vehState, replanInZone);
	    currentSegmentId = zone_id;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  }
	  break;
	default:
	  Log::getStream(1) << "ERROR: LOGIC_PLANNER: region = ZONE but current_lp_state = "
			    << lpstateToString(current_lp_state) << " is not handled." << endl;
	  Console::addMessage("ERROR: LOGIC_PLANNER: region = ZONE but current_lp_state is not handled.");
	  current_lp_state = LP_ZONE_SAFETY;
	  startNewState(vehState, replanInZone);
	  break;
	}
      }
    }
    goto planLogic_end;
  }



  ///////////////////////////////////////////////////////////////////////////////////////////////
  // When we're in ROAD_REGION
  ///////////////////////////////////////////////////////////////////////////////////////////////
  switch (current_lp_state) {
  case LP_DRIVE_NOPASS: {
    // DRIVE_NOPASS -> STOPOBS_NOPASS
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
      // The path-planner could not plan, so we bring the car to a stop to enable illegal passing
      current_lp_state = LP_STOPOBS_NOPASS;
      //current_region = ROAD_REGION;
      last_obs_time = Utils::getTime();
      stopobs_replan = true;
      startNewState(vehState, replanInZone);
      if (prevErr & PP_COLLISION_SAFETY) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
	Console::addMessage("LP: In DRIVE_NOPASS: PP_COLLISION_SAFETY");
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In DRIVE_NOPASS: PP_NOPATH_LEN");
      }
      Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
    }
    break;
  }

  case LP_DRIVE_PASS: {
    // DRIVE_PASS -> STOPOBS_PASS
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
      // The path-planner could not plan, so we bring the car to a stop to enable illegal passing
      current_lp_state = LP_STOPOBS_PASS;
      //current_region = ROAD_REGION;
      last_obs_time = Utils::getTime();
      startNewState(vehState, replanInZone);
      if (prevErr & PP_COLLISION_SAFETY) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
	Console::addMessage("LP: In DRIVE_PASS: PP_COLLISION_SAFETY");
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In DRIVE_PASS: PP_NOPATH_LEN");
      }
      break;
    }

    double distPassing = sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
			       pow(alice.y - start_current_lp_state_ypos, 2));

    // DRIVE_PASS -> DRIVE_NOPASS: finish passing manouver
    current_lane = Utils::getCurrentLane(vehState, graph);
    obstacle_distance = Utils::getNearestObsDist(vehState, map, current_lane);
    if (obstacle_distance <= MAX_PASSING_1_LENGTH && obstacle_distance > 0.0)
      last_obs_time = Utils::getTime();

    Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
		      << " seconds" << ", " << distPassing << " m" << endl;

    if (!(Utils::isReverse(vehState, map, current_lane)) &&
	(graph->vehicleNode != NULL) && !graph->vehicleNode->flags.isOncoming &&
	(obstacle_distance > 10*MAX_PASSING_1_LENGTH || obstacle_distance < -2*MAX_PASSING_1_LENGTH) &&

	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_PASSING &&
	distPassing > MIN_DIST_PASSING) {
      // Resets the PASS flag to NO_PASS, we must have finished the passing manouver
      current_lp_state = LP_DRIVE_NOPASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);	
      sendReplan = true;
      Log::getStream(3) << "LOGIC_PLANNER: finished passing maneuver. Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
      Console::addMessage("LP: In DRIVE_PASS: Finished passing");
      break;
    }
    
    // DRIVE_PASS -> DRIVE_NOPASS: no more obstacle in front (the passing manouver is useless now)
    if (!(Utils::isReverse(vehState, map, current_lane)) &&
	(graph->vehicleNode != NULL) && !graph->vehicleNode->flags.isOncoming &&
	(obstacle_distance > 10*MAX_PASSING_1_LENGTH || obstacle_distance < 0.0) &&
	(Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_PASSING &&
	distPassing > MIN_DIST_PASSING) {
      current_lp_state = LP_DRIVE_NOPASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstacle in front. Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
      Console::addMessage("LP: In DRIVE_PASS: obstacle went away");
      break;
    }
    break;
  }

  case LP_DRIVE_PASS_REV: {
    
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN) ||
	num_drpsrv >= MAX_NUM_DRIVE_PASS_REV) {
      if (prevErr & PP_COLLISION_SAFETY) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
	Console::addMessage("LP: In DRIVE_PASS_REV: PP_COLLISION_SAFETY");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In DRIVE_PASS_REV: PP_NOPATH_LEN");
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: num_drpsrv = " << num_drpsrv << endl;
	Console::addMessage("LP: In DRIVE_PASS_REV: num_drpsrv = %d" , num_drpsrv);
      }

      /*
      double distFromPrevDrPsRv = sqrt( pow(alice.x - just_finished_drpsrv_xpos, 2) + 
					pow(alice.y - just_finished_drpsrv_ypos, 2));
      if ( distFromPrevDrPsRv > MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
	num_drpsrv = 0;
	Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_PASS_REV state, distFromPrevDrPsRv = " << distFromPrevDrPsRv
			  << ". Resetting num_drpsrv to 0." <<endl;
      }
      else {
	num_drpsrv++;
	Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_PASS_REV state, distFromPrevDrPsRv = " << distFromPrevDrPsRv
			  << ". Incrementing num_drpsrv to " << num_drpsrv <<endl;
      }
      just_finished_drpsrv_xpos = alice.x;
      just_finished_drpsrv_ypos = alice.y;
      */
      
      // DRIVE_PASS_REV -> STOPOBS_PASS_REV
      if (num_drpsrv < MAX_NUM_DRIVE_PASS_REV) {
	// The path-planner could not plan, so we bring the car to a stop to enable illegal passing
	current_lp_state = LP_STOPOBS_PASS_REV;
	//current_region = ROAD_REGION;
	last_obs_time = Utils::getTime();
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: num_drpsrv < MAX_NUM_DRIVE_PASS_REV. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	break;
      }

      else {
	Log::getStream(3) << "LOGIC_PLANNER: Transition to DRIVE_PASS_REV too many times" << endl;
	Console::addMessage("LOGIC_PLANNER: Transition to DRIVE_PASS_REV too many times");
	vector<LaneLabel> lanesAllDir;
	LaneLabel laneAlice;
	laneAlice = Utils::getCurrentLane(vehState, graph);
	map->getAllDirLanes(lanesAllDir, laneAlice);
	// DRIVE_PASS_REV -> BACKUP
	if (lanesAllDir.size() > 1 && ALLOW_BACKUP) {
	  current_lp_state = LP_BACKUP;
	  //current_region = ROAD_REGION;
	  backup_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: Transition to DRIVE_PASS_REV too many times" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". backup_lp_state = " << backup_lp_state << endl;
	  break;
	}

	// If there is one lane, backup is useless
	Log::getStream(3) << "LOGIC_PLANNER: number of lanes = " << lanesAllDir.size()
			  << " Don't want to back up. stopped_duration = " 
			  << stopped_duration;
	// DRIVE_PASS_REV -> STOPOBS_AGG
	if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_CHANGE_OBS) {
	    current_lp_state = LP_STOPOBS_AGG;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	}
	// DRIVE_PASS_REV -> OFFROAD_SAFETY
	else if (USE_OFFROAD_ONELANE && lanesAllDir.size() <= 1) {
	  startNewState(vehState, replanInZone);
	  start_offroad_time = Utils::getTime();
	  start_offroad_xpos = alice.x;
	  start_offroad_ypos = alice.y;
	  current_lp_state = LP_OFFROAD_SAFETY;
	  //current_region = ROAD_REGION;
	  sendReplan = true;
	  nopathlen = true;
	  Log::getStream(3) << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	  break;
	}
	else if (USE_S1PLANNER_ON_ROAD ) {
	  // DRIVE_PASS_REV -> S1PLANNER_SAFETY
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_S1PLANNER) {
	    current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	}
	// DRIVE_PASS_REV -> UTURN
	else if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	  current_lp_state = LP_PAUSE;
	  //current_region = ROAD_REGION;
	  paused_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	    
	  current_lane = Utils::getCurrentLane(vehState, graph);
	  currentSegmentId = current_lane.segment;
	  currentLaneId = current_lane.lane;
	  ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  Log::getStream(3) << "LOGIC_PLANNER: stopped_duration = " << stopped_duration << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			    << currentSegmentId << " currentLaneId = " << currentLaneId
			    << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	  break;
	}
      }
    }
      
    // DRIVE_PASS_REV -> DRIVE_NOPASS: finish passing manouver
    double distPassing = sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
			       pow(alice.y - start_current_lp_state_ypos, 2));
    current_lane = Utils::getCurrentLane(vehState, graph);
    obstacle_distance = Utils::getNearestObsDist(vehState, map, current_lane);
    if (obstacle_distance <= MAX_PASSING_1_LENGTH && obstacle_distance > 0.0)
      last_obs_time = Utils::getTime();
      

    Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
		      << " seconds" << ", " << distPassing << " m" << endl;

    if ((graph->vehicleNode != NULL) && !graph->vehicleNode->flags.isOncoming &&
	(obstacle_distance > 10*MAX_PASSING_1_LENGTH || obstacle_distance < -2*MAX_PASSING_1_LENGTH) &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_PASSING &&
	distPassing > MIN_DIST_PASSING) {
      // Resets the PASS flag to NO_PASS, we must have finished the passing manouver
      current_lp_state = LP_DRIVE_NOPASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);	
      sendReplan = true;
      Log::getStream(3) << "LOGIC_PLANNER: finished passing maneuver. Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
      Console::addMessage("LP: In DRIVE_PASS_REV: Finished passing");
      break;
    }
    
    // DRIVE_PASS_REV -> DRIVE_NOPASS: no more obstacle in front (the passing manouver is useless now)
    if (!(Utils::isReverse(vehState, map, current_lane)) &&
	(graph->vehicleNode != NULL) && !graph->vehicleNode->flags.isOncoming &&
	(obstacle_distance > 10*MAX_PASSING_1_LENGTH || obstacle_distance < 0.0) &&
	(Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_PASSING &&
	distPassing > MIN_DIST_PASSING) {
      current_lp_state = LP_DRIVE_NOPASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstacle in front. Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
      Console::addMessage("LP: In DRIVE_PASS_REV: obstacle went away");
      break;
    }
    break;
  }

  case LP_STOPOBS_NOPASS: {
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
      last_obs_time = Utils::getTime();
      if (prevErr & PP_COLLISION_SAFETY) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
	Console::addMessage("LP: In STOPOBS_NOPASS: PP_COLLISION_SAFETY");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In STOPOBS_NOPASS: PP_NOPATH_LEN");
      }
    }

    // STOP_OBS_NOPASS -> DRIVE_NOPASS: no obstacles anymore
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
      // If path-planner is now able to plan -> no obstacles
      current_lp_state = LP_DRIVE_NOPASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstacle in front. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In STOPOBS_NOPASS: obstacle went away");
      break;
    }

    // STOP_OBS -> STOP_OBS
    // If we are close to an intersection, we want to wait a little longer
    // before that normally, the obstacle might start moving and thus become a vehicle to follow
    current_lane = Utils::getCurrentLane(vehState, graph);
    stopline_distance = Utils::getDistToStopline(params.m_path, params.seg_goal_queue, intersectionSegGoals);

    if (stopline_distance > 0.0 && stopline_distance < MAX_INTERSECTION_Q_LENGTH && 
        stopped_duration < TIMEOUT_OBS_INT) {
      Log::getStream(3) << "LOGIC_PLANNER: Close to an intersection (" << stopline_distance
			<< "m. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      break;
    }

    // STOP_OBS -> DRIVE: passing
    if (stopped_duration > TIMEOUT_OBS_PASSING) {
      // check for multiple lanes
      vector<LaneLabel> lanes;
      LaneLabel laneAlice;
      laneAlice = Utils::getCurrentLane(vehState, graph);
      map->getSameDirLanes(lanes, laneAlice);

      Log::getStream(3)<<"LOGIC_PLANNER: Number lanes: "<<lanes.size()<<endl;
      if (stopobs_replan || (lanes.size()>1 && stopped_duration<TIMEOUT_OBS_PASSING_EXTENDED)) {
        sendReplan = true;
        stopobs_replan = false;
        Log::getStream(3) <<"LOGIC_PLANNER: stopped_duration = " << stopped_duration
                          << " Want to replan before passing" << endl;
	Console::addMessage("LP: Replan before allowing passing");
      }
      else {
        current_lp_state = LP_DRIVE_PASS;
        //current_region = ROAD_REGION;
        startNewState(vehState, replanInZone);
        Log::getStream(3) << "LOGIC_PLANNER: stopped_duration = " << stopped_duration << ". Switching to "
                          << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                          << endl;

        if (!params.seg_goal.illegalPassingAllowed) {
          paused_lp_state = LP_DRIVE_PASS;
          current_lane = Utils::getCurrentLane(vehState, graph);
          currentSegmentId = current_lane.segment;
          currentLaneId = current_lane.lane;
          Log::getStream(3) << "LOGIC_PLANNER: mplanner doesn't allow me to pass" << ". Switching to "
                            << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                            << ". Send LP_FAIL_MPLANNER_LANE_BLOCKED with currentSegmentId = " 
                            << currentSegmentId << " currentLaneId = " << currentLaneId
                            << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
          Console::addMessage("LP: In STOPOBS_NOPASS: TIMEOUT_OBS_PASSING");
          // Notify MPlanner that we "failed"
          ret_value |= LP_FAIL_MPLANNER_LANE_BLOCKED;
        }
        break;
      }
    }
    break;
  }

  case LP_STOPOBS_PASS: {

    // STOP_OBS_PASS -> STOP_OBS_PASS
    // If we are close to an intersection, we want to wait a little longer
    // before that normally, the obstacle might start moving and thus become a vehicle to follow
    current_lane = Utils::getCurrentLane(vehState, graph);
    stopline_distance = Utils::getDistToStopline(params.m_path, params.seg_goal_queue, intersectionSegGoals);
    
    if (stopline_distance > 0.0 && stopline_distance < MAX_INTERSECTION_Q_LENGTH && 
        stopped_duration < TIMEOUT_OBS_INT) {
      current_lp_state = LP_STOPOBS_PASS;
      //current_region = ROAD_REGION;
      Log::getStream(3) << "LOGIC_PLANNER: too close to stop line so should not pass" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      break;
    }

    vector<LaneLabel> lanesAllDir;
    LaneLabel laneAlice;
    laneAlice = Utils::getCurrentLane(vehState, graph);
    map->getAllDirLanes(lanesAllDir, laneAlice);

    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN))
      last_obs_time = Utils::getTime();
    // STOP_OBS_PASS -> DRIVE_PASS: no obstacles anymore
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
      // If path-planner is now able to plan -> no obstacles
      current_lp_state = LP_DRIVE_PASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstacle in front" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In STOPOBS_PASS: obstacle went away");
      break;
    }

    if (prevErr & PP_COLLISION_SAFETY) {
      Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
      Console::addMessage("LP: In STOPOBS_PASS: PP_COLLISION_SAFETY");
    }
    else if (prevErr & PP_NOPATH_LEN) {
      Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
      Console::addMessage("LP: In STOPOBS_PASS: PP_NOPATH_LEN");
    }
    Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
		      << " seconds" << endl;

    // STOPOBS_PASS -> DRIVE_PASS_REV
    if (((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) && USE_RAIL_PLANNER_REV &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_SHORT &&
	stopped_duration > MIN_TIME_STOPOBS_SHORT) {
      double distFromPrevDrPsRv = sqrt( pow(alice.x - just_finished_drpsrv_xpos, 2) + 
					pow(alice.y - just_finished_drpsrv_ypos, 2));
      if ( distFromPrevDrPsRv > MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
	num_drpsrv = 0;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_PASS state, distFromPrevDrPsRv = " << distFromPrevDrPsRv
			  << ". Resetting num_drpsrv to 0." <<endl;
      }
      else {
	num_drpsrv++;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_PASS state, distFromPrevDrPsRv = " << distFromPrevDrPsRv
			  << ". Incrementing num_drpsrv to " << num_drpsrv <<endl;
      }
      just_finished_drpsrv_xpos = alice.x;
      just_finished_drpsrv_ypos = alice.y;

      if (num_drpsrv >= MAX_NUM_DRIVE_PASS_REV) {
	// The path-planner could not plan, so we bring the car to a stop to enable illegal passing
	current_lp_state = LP_STOPOBS_PASS_REV;
	//current_region = ROAD_REGION;
	last_obs_time = Utils::getTime();
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: num_drpsrv >= MAX_NUM_DRIVE_PASS_REV. Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	Console::addMessage("LP: In STOPOBS_PASS: num_drpsrv >= MAX_NUM_DRIVE_PASS_REV");
	break;
      }
      else {
	current_lp_state = LP_DRIVE_PASS_REV;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: num_drpsrv < MAX_NUM_DRIVE_PASS_REV" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	break;
      }
    }

    // STOPOBS_PASS -> BACKUP when there are multiple lanes
    if (((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_SHORT &&
	stopped_duration > MIN_TIME_STOPOBS_SHORT &&
	lanesAllDir.size() > 1 && ALLOW_BACKUP) {
      current_lp_state = LP_BACKUP;
      //current_region = ROAD_REGION;
      backup_lp_state = LP_DRIVE_PASS_BACKUP;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan" << ". Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< ". backup_lp_state = " << backup_lp_state << endl;
      break;
    }

    // If there is one lane, backup is useless
    if (((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_SHORT &&
	stopped_duration > MIN_TIME_STOPOBS_SHORT) {
      Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. stopped_duration = " 
			<< stopped_duration;

      // STOPOBS_PASS -> STOPOBS_AGG
      if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	if (stopped_duration > MIN_TIME_STOP_BEFORE_CHANGE_OBS) {
	  current_lp_state = LP_STOPOBS_AGG;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	  break;
	}
      }
      // STOPOBS_PASS -> OFFROAD_SAFETY
      else if (USE_OFFROAD_ONELANE && lanesAllDir.size() <= 1) {
	startNewState(vehState, replanInZone);
	start_offroad_time = Utils::getTime();
	start_offroad_xpos = alice.x;
	start_offroad_ypos = alice.y;
	current_lp_state = LP_OFFROAD_SAFETY;
	//current_region = ROAD_REGION;
	sendReplan = true;
	nopathlen = true;
	Log::getStream(3) << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	break;
      }
      else if (USE_S1PLANNER_ON_ROAD ) {
	// STOPOBS_PASS -> S1PLANNER_SAFETY
	if (stopped_duration > MIN_TIME_STOP_BEFORE_S1PLANNER) {
	  current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	  break;
	}
      }
      // STOPOBS_PASS -> UTURN
      else if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	current_lp_state = LP_PAUSE;
	//current_region = ROAD_REGION;
	paused_lp_state = LP_DRIVE_PASS_BACKUP;
	startNewState(vehState, replanInZone);
	
	current_lane = Utils::getCurrentLane(vehState, graph);
	currentSegmentId = current_lane.segment;
	currentLaneId = current_lane.lane;
	ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	Log::getStream(3) << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			  << currentSegmentId << " currentLaneId = " << currentLaneId
			  << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	break;
      }
    }
    break;
  }

  case LP_STOPOBS_PASS_REV: {

    /*
    // STOP_OBS_PASS_REV -> STOP_OBS_PASS_REV
    // If we are close to an intersection, we want to wait a little longer
    // before that normally, the obstacle might start moving and thus become a vehicle to follow
    current_lane = Utils::getCurrentLane(vehState, graph);
    stopline_distance = Utils::getDistToStopline(params.m_path, params.seg_goal_queue, intersectionSegGoals);
    
    if (stopline_distance > 0.0 && stopline_distance < MAX_INTERSECTION_Q_LENGTH && 
        stopped_duration < TIMEOUT_OBS_INT) {
      current_lp_state = LP_STOPOBS_PASS_REV;
      //current_region = ROAD_REGION;
      Log::getStream(3) << "LOGIC_PLANNER: too close to stop line so should not pass" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      break;
    }
    */

    vector<LaneLabel> lanesAllDir;
    LaneLabel laneAlice;
    laneAlice = Utils::getCurrentLane(vehState, graph);
    map->getAllDirLanes(lanesAllDir, laneAlice);

    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN))
      last_obs_time = Utils::getTime();
    // STOP_OBS_PASS_REV -> DRIVE_PASS_REV: no obstacles anymore
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
      // If path-planner is now able to plan -> no obstacles
      double distFromPrevDrPsRv = sqrt( pow(alice.x - just_finished_drpsrv_xpos, 2) + 
					pow(alice.y - just_finished_drpsrv_ypos, 2));
      if ( distFromPrevDrPsRv > MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
	num_drpsrv = 0;
	Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_PASS_REV state, distFromPrevDrPsRv = " << distFromPrevDrPsRv
			  << ". Resetting num_drpsrv to 0." <<endl;
      }
      else {
	num_drpsrv++;
	Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_PASS_REV state, distFromPrevDrPsRv = " << distFromPrevDrPsRv
			  << ". Incrementing num_drpsrv to " << num_drpsrv <<endl;
      }
      just_finished_drpsrv_xpos = alice.x;
      just_finished_drpsrv_ypos = alice.y;

      current_lp_state = LP_DRIVE_PASS_REV;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstacle in front" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In STOPOBS_PASS_REV: obstacle went away");
      break;
    }


    if (prevErr & PP_COLLISION_SAFETY) {
      Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
      Console::addMessage("LP: In STOPOBS_PASS_REV: PP_COLLISION_SAFETY");
    }
    else if (prevErr & PP_NOPATH_LEN) {
      Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
      Console::addMessage("LP: In STOPOBS_PASS_REV: PP_NOPATH_LEN");
    }
    Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
		      << " seconds" << endl;

    // STOPOBS_PASS_REV -> BACKUP when there are multiple lanes
    if (((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_LONG &&
	stopped_duration > MIN_TIME_STOPOBS_LONG &&
	lanesAllDir.size() > 1 && ALLOW_BACKUP) {
      current_lp_state = LP_BACKUP;
      //current_region = ROAD_REGION;
      backup_lp_state = LP_DRIVE_PASS_BACKUP;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan" << ". Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< ". backup_lp_state = " << backup_lp_state << endl;
      break;
    }

    // If there is one lane, backup is useless
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
      if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_LONG &&
	  stopped_duration > MIN_TIME_STOPOBS_LONG) {
	// STOPOBS_PASS_REV -> STOPOBS_AGG
	if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_CHANGE_OBS) {
	    current_lp_state = LP_STOPOBS_AGG;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. stopped_duration = " 
			      << stopped_duration << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	}
	// STOPOBS_PASS_REV -> OFFROAD_SAFETY
	else if (USE_OFFROAD_ONELANE && lanesAllDir.size() <= 1) {
	  startNewState(vehState, replanInZone);
	  start_offroad_time = Utils::getTime();
	  start_offroad_xpos = alice.x;
	  start_offroad_ypos = alice.y;
	  current_lp_state = LP_OFFROAD_SAFETY;
	  //current_region = ROAD_REGION;
	  sendReplan = true;
	  nopathlen = true;
	  Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. stopped_duration = " 
			    << stopped_duration << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	  break;
	}
	else if (USE_S1PLANNER_ON_ROAD ) {
	  // STOPOBS_PASS_REV -> S1PLANNER_SAFETY
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_S1PLANNER) {
	    current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	}
	// STOPOBS_PASS_REV -> UTURN
	else if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	  current_lp_state = LP_PAUSE;
	  //current_region = ROAD_REGION;
	  paused_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	  
	  current_lane = Utils::getCurrentLane(vehState, graph);
	  currentSegmentId = current_lane.segment;
	  currentLaneId = current_lane.lane;
	  ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  Log::getStream(3) << "LOGIC_PLANNER: stopped_duration = " << stopped_duration << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			    << currentSegmentId << " currentLaneId = " << currentLaneId
			    << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	}
      }
    } 

    break;
  }

    
  case LP_DRIVE_PASS_BACKUP: {
    // DRIVE_PASS_BACKUP -> UTURN if we have stopped for long enough -- I don't think we need this.
    // If it turns out that we do, just uncomment the code below.
    // DRIVE_PASS_BACKUP -> STOPOBS_PASS_BACKUP if we haven't stopped for long enough.
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
      last_obs_time = Utils::getTime();

      if (prevErr & PP_COLLISION_SAFETY) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
	Console::addMessage("LP: In DRIVE_PASS_BACKUP: PP_COLLISION_SAFETY");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In DRIVE_PASS_BACKUP: PP_NOPATH_LEN");
      }

      current_lp_state = LP_STOPOBS_PASS_BACKUP;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
      /* 
      if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
        current_lp_state = LP_PAUSE;
        //current_region = ROAD_REGION;
        paused_lp_state = LP_DRIVE_PASS_BACKUP;
	startNewState(vehState, replanInZone);

        current_lane = Utils::getCurrentLane(vehState, graph);
        currentSegmentId = current_lane.segment;
        currentLaneId = current_lane.lane;
        ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
        Log::getStream(3) << "LOGIC_PLANNER: stopped_duration = " << stopped_duration << ". Switching to "
                          << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                          << " Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
                          << currentSegmentId << " currentLaneId = " << currentLaneId
                          << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
      }
      else {
        current_lp_state = LP_STOPOBS_PASS_BACKUP;
        //current_region = ROAD_REGION;
      }
      */
    } 
    // DRIVE_PASS_BACKUP -> DRIVE_PASS if obstacle disappears.
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
      current_lp_state = LP_DRIVE_PASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstacle in front" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In DRIVE_PASS_BKUP: obstacle went away");
    }
    break;
  }

  case LP_STOPOBS_PASS_BACKUP: {
    if ((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) {
      last_obs_time = Utils::getTime();
      if (prevErr & PP_COLLISION_SAFETY) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_SAFETY" << endl;
	Console::addMessage("LP: In STOPOBS_PASS_BACKUP: PP_COLLISION_SAFETY");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In STOPOBS_PASS_BACKUP: PP_NOPATH_LEN");
      }
      Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
			<< " seconds" << endl;

      if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_SHORT &&
	  stopped_duration > MIN_TIME_STOPOBS_SHORT) {
	// STOPOBS_PASS_BACKUP -> STOPOBS_AGG
	if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_CHANGE_OBS) {
	    current_lp_state = LP_STOPOBS_AGG;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. stopped_duration = " 
			      << stopped_duration << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    sendReplan = true;
	    break;
	  }
	}
	else if (USE_S1PLANNER_ON_ROAD ) {
	  // STOPOBS_PASS_BACKUP -> S1PLANNER
	  if (stopped_duration > MIN_TIME_STOP_BEFORE_S1PLANNER) {
	    current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	}
	// STOPOBS_PASS_BACKUP -> UTURN
	else if (stopped_duration > MIN_TIME_STOP_BEFORE_UTURN) {
	  current_lp_state = LP_PAUSE;
	  //current_region = ROAD_REGION;
	  paused_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	  
	  current_lane = Utils::getCurrentLane(vehState, graph);
	  currentSegmentId = current_lane.segment;
	  currentLaneId = current_lane.lane;
	  ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  Log::getStream(3) << "LOGIC_PLANNER: stopped_duration = " << stopped_duration << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			    << currentSegmentId << " currentLaneId = " << currentLaneId
			    << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	}
      }
    } 
    // STOPOBS_PASS_BACKUP -> DRIVE_PASS if obstacle disappears.
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR) {
      current_lp_state = LP_DRIVE_PASS;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: no more obstalce in front" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In STOPOBS_PASS_BKUP: obstacle went away");
    }
    break;
  }

  case LP_BACKUP: {
    if (prevErr & VP_BACKUP_FINISHED) {
      Log::getStream(3) << "LOGIC_PLANNER: Backup completed" << endl;
      Console::addMessage("LOGIC_PLANNER: Backup completed");

      double distFromPrevBackup = sqrt( pow(alice.x - just_finished_backup_xpos, 2) + 
                                        pow(alice.y - just_finished_backup_ypos, 2));
      if ( distFromPrevBackup > MIN_DIST_BETWEEN_DIFF_BACKUP ) {
        num_backup = 0;
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP state, distFromPrevBackup = " << distFromPrevBackup
                          << ". Resetting num_backup to 0." <<endl;
      }
      else {
        num_backup++;
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP state, distFromPrevBackup = " << distFromPrevBackup
                          << ". Incrementing num_backup to " << num_backup <<endl;
      }
      just_finished_backup_xpos = alice.x;
      just_finished_backup_ypos = alice.y;

      if (backup_lp_state == LP_DRIVE_PASS_BACKUP && num_backup >= MAX_NUM_BACKUP) {
	Console::addMessage("LP: In BACKUP: Backup too many times");
	Log::getStream(3) << "LOGIC_PLANNER: Backup too many times";

	// BACKUP -> STOPOBS_AGG
	if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	  current_lp_state = LP_STOPOBS_AGG;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	  sendReplan = true;
	  break;
	}
	// BACKUP -> S1PLANNER
        else if (USE_S1PLANNER_ON_ROAD ) {
          current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
          //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
          Log::getStream(3) << ". Switching to "
                            << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                            << endl;
          break;
        }
	// BACKUP -> UTURN
        else {
          current_lp_state = LP_PAUSE;
          //current_region = ROAD_REGION;
          paused_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	  
          current_lane = Utils::getCurrentLane(vehState, graph);
          currentSegmentId = current_lane.segment;
          currentLaneId = current_lane.lane;
          ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
          Log::getStream(3) << ". Switching to "
                            << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                            << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
                            << currentSegmentId << " currentLaneId = " << currentLaneId
                            << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
        }
        break;
      }

      // BACKUP -> DRIVE_PASS_BACKUP
      else {
        current_lp_state = backup_lp_state;
        //current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	sendReplan = true;
        Log::getStream(3) << "LOGIC_PLANNER: Backup finished" << ". Switching to "
                          << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                          << endl;
	Console::addMessage("LP: In BACKUP: Backup finished");
      }
      break;
    }
    // If BACKUP fails:
    else if ((prevErr & S1PLANNER_FAILED) ||
	     (prevErr & S1PLANNER_START_BLOCKED) ||
	     (prevErr & S1PLANNER_END_BLOCKED) ||
	     (prevErr & PP_COLLISION_BARE) ||
	     (prevErr & PP_NOPATH_LEN)) {
      last_obs_time = Utils::getTime();
      if (prevErr & S1PLANNER_FAILED) {
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP: S1PLANNER_FAILED" << endl;
        Console::addMessage("LP: In BACKUP: S1PLANNER_FAILED");
      }
      if (prevErr & S1PLANNER_START_BLOCKED) {
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP: S1PLANNER_START_BLOCKED" << endl;
        Console::addMessage("LP: In BACKUP: S1PLANNER_START_BLOCKED");
      }
      if (prevErr & S1PLANNER_END_BLOCKED) {
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP: S1PLANNER_END_BLOCKED" << endl;
        Console::addMessage("LP: In BACKUP: S1PLANNER_END_BLOCKED");
      }
      if (prevErr & S1PLANNER_COST_TOO_HIGH) {
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP: S1PLANNER_COST_TOO_HIGH" << endl;
        Console::addMessage("LP: In BACKUP: S1PLANNER_COST_TOO_HIGH");
      }

      double distFromPrevBackup = sqrt( pow(alice.x - just_finished_backup_xpos, 2) + 
                                        pow(alice.y - just_finished_backup_ypos, 2));
      if ( distFromPrevBackup > MIN_DIST_BETWEEN_DIFF_BACKUP ) {
        num_backup = 0;
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP state, distFromPrevBackup = " << distFromPrevBackup
                          << ". Resetting num_backup to 0." <<endl;
      }
      else {
        num_backup++;
        Log::getStream(3) << "LOGIC_PLANNER: In BACKUP state, distFromPrevBackup = " << distFromPrevBackup
                          << ". Incrementing num_backup to " << num_backup <<endl;
      }
      just_finished_backup_xpos = alice.x;
      just_finished_backup_ypos = alice.y;

      // BACKUP -> backup_lp_state (DRIVE_PASS_BACKUP or DRIVE_S1PLANNER or STOPOBS_AGG)
      if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_IN_DRIVE_S1PLANNER) {
	if ((USE_S1PLANNER_ON_ROAD || NUM_CHANGE_RP_OBS_SIZE > 0) && 
	    (num_backup < MAX_NUM_BACKUP || backup_lp_state != LP_DRIVE_PASS_BACKUP)) {
	  current_lp_state = backup_lp_state;
	  //current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
	  sendReplan = true;
	  Log::getStream(3) << "LOGIC_PLANNER: Backup failed" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	  break;
	}
	// BACKUP -> S1PLANNER/STOPOBS_AGG/UTURN
	else {
	  Console::addMessage("LP: In BACKUP: Backup too many times");
	  Log::getStream(3) << "LOGIC_PLANNER: Backup too many times";
	  // BACKUP -> STOPOBS_AGG
	  if (NUM_CHANGE_RP_OBS_SIZE > 0) {
	    current_lp_state = LP_STOPOBS_AGG;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    sendReplan = true;
	    Log::getStream(3) << "LOGIC_PLANNER: path-planner could not plan. stopped_duration = " 
			      << stopped_duration << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	  // BACKUP -> S1PLANNER
	  else if (USE_S1PLANNER_ON_ROAD ) {
	    current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	    //current_region = ROAD_REGION;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: Backup too many times" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << endl;
	    break;
	  }
	  // BACKUP -> UTURN
	  else {
	    current_lp_state = LP_PAUSE;
	    //current_region = ROAD_REGION;
	    paused_lp_state = LP_DRIVE_PASS_BACKUP;
	    startNewState(vehState, replanInZone);
	  
	    current_lane = Utils::getCurrentLane(vehState, graph);
	    currentSegmentId = current_lane.segment;
	    currentLaneId = current_lane.lane;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	    Log::getStream(3) << "LOGIC_PLANNER: Backup too many times" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			      << currentSegmentId << " currentLaneId = " << currentLaneId
			      << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	  }
	  break;
	}
      }
    }
    else if (prevErr & S1PLANNER_COST_TOO_HIGH) {
      Log::getStream(3) << "LOGIC_PLANNER: In BACKUP: S1PLANNER_COST_TOO_HIGH" << endl;
      Console::addMessage("LP: In BACKUP: S1PLANNER_COST_TOO_HIGH");
    }
    break;
  }

  case LP_UTURN: {
    // UTURN -> DRIVE
    if (prevErr & VP_UTURN_FINISHED) {
      current_lp_state = LP_DRIVE_NOPASS;
      current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      sendReplan = true;
      Log::getStream(3) << "LOGIC_PLANNER: Uturn finished" << ". Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In UTURN: Uturn finished");
    }
    break;
  }
#warning: Need to handle failure in uturn

  case LP_DRIVE_AGG: {
    // DRIVE_AGG -> STOPOBS_AGG
    if ((prevErr & PP_COLLISION_AGGRESSIVE) || (prevErr & PP_NOPATH_LEN)) {
      last_obs_time = Utils::getTime();

      if (prevErr & PP_COLLISION_AGGRESSIVE) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_AGGRESSIVE" << endl;
	Console::addMessage("LP: In DRIVE_AGG: PP_COLLISION_AGGRESSIVE");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In DRIVE_AGG: PP_NOPATH_LEN");
      }

      current_lp_state = LP_STOPOBS_AGG;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_AGG: path-planner could not plan" << ". Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
    }
    // DRIVE_AGG -> DRIVE_NOPASS
    /*
    else if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS ||
	     sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		   pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_SMALL_OBS) {
    */
    else if (((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS ||
	      sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		    pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_SMALL_OBS) &&
	     !(prevErr & PP_COLLISION_SAFETY) && !(prevErr & PP_NOPATH_LEN)) {
      current_lp_state = LP_DRIVE_NOPASS;
      startNewState(vehState, replanInZone);
      sendReplan = true;
      Log::getStream(3) << "LOGIC_PLANNER: DRIVE_AGG timeout (time = "
			<< (Utils::getTime() - start_current_lp_state_time)/1000000.0
			<< " s, dist = " 
			<< sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
				 pow(alice.y - start_current_lp_state_ypos, 2))
			<< " m. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In DRIVE_AGG: Timeout. Switching to DRIVE_NOPASS");
    }
    //current_region = ROAD_REGION;
    break;
  }

  case LP_DRIVE_BARE: {
    // DRIVE_BARE -> STOPOBS_BARE
    if ((prevErr & PP_COLLISION_BARE) || (prevErr & PP_NOPATH_LEN)) {
      last_obs_time = Utils::getTime();
      if (prevErr & PP_COLLISION_AGGRESSIVE) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_BARE" << endl;
	Console::addMessage("LP: In DRIVE_BARE: PP_COLLISION_BARE");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In DRIVE_BARE: PP_NOPATH_LEN");
      }

      current_lp_state = LP_STOPOBS_BARE;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_BARE: path-planner could not plan" << ". Switching to "
			<< lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			<< endl;
    }
    // DRIVE_BARE -> DRIVE_NOPASS
    /*
    else if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS ||
	     sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		   pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_SMALL_OBS) {
    */
    else if (((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_USE_SMALL_OBS ||
	      sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		    pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_SMALL_OBS) &&
	     !(prevErr & PP_COLLISION_AGGRESSIVE) && !(prevErr & PP_NOPATH_LEN)) {
      current_lp_state = LP_DRIVE_AGG;
      startNewState(vehState, replanInZone);
      sendReplan = true;
      Log::getStream(3) << "LOGIC_PLANNER: DRIVE_BARE timeout (time = "
			<< (Utils::getTime() - start_current_lp_state_time)/1000000.0
			<< " s, dist = " 
			<< sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
				 pow(alice.y - start_current_lp_state_ypos, 2))
			<< " m. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In DRIVE_BARE: Timeout. Switching to DRIVE_NOPASS");
    }
    //current_region = ROAD_REGION;
    break;
  }

  case LP_STOPOBS_AGG: {
    if ((prevErr & PP_COLLISION_AGGRESSIVE) || (prevErr & PP_NOPATH_LEN))
      last_obs_time = Utils::getTime();

    // STOP_OBS_AGG -> DRIVE_AGG: no obstacles anymore
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR &&
	     num_dagg < MAX_NUM_DRIVE_AGG) {
      // If path-planner is now able to plan -> no obstacles
      double distFromPrevDriveAgg = sqrt( pow(alice.x - just_finished_dagg_xpos, 2) + 
					  pow(alice.y - just_finished_dagg_ypos, 2));
      if ( distFromPrevDriveAgg > MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
	num_dagg = 0;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG state, distFromPrevDriveAgg = " << distFromPrevDriveAgg
			  << ". Resetting num_dagg to 0." <<endl;
      }
      else {
	num_dagg++;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG state, distFromPrevDriveAgg = " << distFromPrevDriveAgg
			  << ". Incrementing num_dagg to " << num_dagg <<endl;
      }
      just_finished_dagg_xpos = alice.x;
      just_finished_dagg_ypos = alice.y;

      current_lp_state = LP_DRIVE_AGG;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: no more obstacle in front. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In STOPOBS_AGG: obstacle went away");
      break;
    }

    if (((prevErr & PP_COLLISION_AGGRESSIVE) || (prevErr & PP_NOPATH_LEN) ||
	 num_dagg >= MAX_NUM_DRIVE_AGG) &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_LONG &&
	stopped_duration > MIN_TIME_STOPOBS_LONG) {

      if (prevErr & PP_COLLISION_AGGRESSIVE) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_AGG" << endl;
	Console::addMessage("LP: In STOPOBS_AGG: PP_COLLISION_AGG");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In STOPOBS_AGG: PP_NOPATH_LEN");
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: num_dagg = " << num_drpsrv << endl;
	Console::addMessage("LP: In STOPOBS_AGG: num_dagg = %d" , num_dagg);
      }

      Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
			<< " seconds" << endl;

      vector<LaneLabel> lanesAllDir;
      LaneLabel laneAlice;
      laneAlice = Utils::getCurrentLane(vehState, graph);
      map->getAllDirLanes(lanesAllDir, laneAlice);

      // STOPOBS_AGG -> STOPOBS_BARE
      if (NUM_CHANGE_RP_OBS_SIZE > 1) {
	current_lp_state = LP_STOPOBS_BARE;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	sendReplan = true;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: path-planner could not plan" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
      }
      // STOPOBS_AGG -> OFFROAD_SAFETY
      else if (USE_OFFROAD_ONELANE && lanesAllDir.size() <= 1) {
	startNewState(vehState, replanInZone);
	start_offroad_time = Utils::getTime();
	start_offroad_xpos = alice.x;
	start_offroad_ypos = alice.y;
	current_lp_state = LP_OFFROAD_SAFETY;
	//current_region = ROAD_REGION;
	sendReplan = true;
	nopathlen = true;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: path-planner could not plan" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	break;
      }
      // STOPOBS_AGG -> DRIVE_S1PLANNER_SAFETY
      else if (USE_S1PLANNER_ON_ROAD ) {
	current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: path-planner could not plan" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
      }
      else {
	if (backup_lp_state != current_lp_state) {
	  num_s1planner_backup = 0;
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: Resetting num_s1planner_backup = 0" << endl;
	}
	else {
	  num_s1planner_backup++;
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: Incrementing num_s1planner_backup = " 
			    << num_s1planner_backup << endl;
	}
	
	// STOPOBS_AGG -> BACKUP
	if (num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP && ALLOW_BACKUP) {
	  backup_lp_state = current_lp_state;
	  current_lp_state = LP_BACKUP;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". backup_lp_state = " << backup_lp_state << endl;
	}
	// STOPOBS_AGG -> UTURN
	else {
	  current_lp_state = LP_PAUSE;
	  paused_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	  
	  current_lane = Utils::getCurrentLane(vehState, graph);
	  currentSegmentId = current_lane.segment;
	  currentLaneId = current_lane.lane;
	  ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_AGG: path-planner could not plan " << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			    << currentSegmentId << " currentLaneId = " << currentLaneId
			    << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	}
      }
    }
    //current_region = ROAD_REGION;
    break;
  }

  case LP_STOPOBS_BARE: {
    if ((prevErr & PP_COLLISION_BARE) || (prevErr & PP_NOPATH_LEN))
      last_obs_time = Utils::getTime();

    // STOP_OBS_BARE -> DRIVE_BARE: no obstacles anymore
    else if ((Utils::getTime() - last_obs_time)/1000000.0 > MIN_TIME_OBS_DISAPPEAR &&
	     num_dbare < MAX_NUM_DRIVE_BARE) {
      // If path-planner is now able to plan -> no obstacles
      double distFromPrevDriveBare = sqrt( pow(alice.x - just_finished_dbare_xpos, 2) + 
					pow(alice.y - just_finished_dbare_ypos, 2));
      if ( distFromPrevDriveBare > MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV ) {
	num_dbare = 0;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE state, distFromPrevDriveBare = " << distFromPrevDriveBare
			  << ". Resetting num_dagg to 0." <<endl;
      }
      else {
	num_dbare++;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE state, distFromPrevDriveBare = " << distFromPrevDriveBare
			  << ". Incrementing num_dagg to " << num_dagg <<endl;
      }
      just_finished_dbare_xpos = alice.x;
      just_finished_dbare_ypos = alice.y;

      current_lp_state = LP_DRIVE_BARE;
      //current_region = ROAD_REGION;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE: no more obstacle in front. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In STOPOBS_BARE: obstacle went away");
      break;
    }

    if (((prevErr & PP_COLLISION_BARE) || (prevErr & PP_NOPATH_LEN) || num_dbare >= MAX_NUM_DRIVE_BARE) &&
	(Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_STOPOBS_LONG &&
	stopped_duration > MIN_TIME_STOPOBS_LONG) {
      if (prevErr & PP_COLLISION_AGGRESSIVE) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_COLLISION_BARE" << endl;
	Console::addMessage("LP: In STOPOBS_BARE: PP_COLLISION_BARE");
      }
      else if (prevErr & PP_NOPATH_LEN) {
	Log::getStream(3) << "LOGIC_PLANNER: prevErr & PP_NOPATH_LEN" << endl;
	Console::addMessage("LP: In STOPOBS_BARE: PP_NOPATH_LEN");
      }
      else {
	Log::getStream(3) << "LOGIC_PLANNER: num_dagg = " << num_drpsrv << endl;
	Console::addMessage("LP: In STOPOBS_BARE: num_dbare = %d" , num_dbare);
      }

      Log::getStream(3) << "LOGIC_PLANNER: In this state for " << (Utils::getTime() - start_current_lp_state_time)/1000000.0
			<< " seconds" << endl;

      vector<LaneLabel> lanesAllDir;
      LaneLabel laneAlice;
      laneAlice = Utils::getCurrentLane(vehState, graph);
      map->getAllDirLanes(lanesAllDir, laneAlice);

      // STOPOBS_BARE -> OFFROAD_SAFETY
      if (USE_OFFROAD_ONELANE && lanesAllDir.size() <= 1) {
	startNewState(vehState, replanInZone);
	start_offroad_time = Utils::getTime();
	start_offroad_xpos = alice.x;
	start_offroad_ypos = alice.y;
	current_lp_state = LP_OFFROAD_SAFETY;
	//current_region = ROAD_REGION;
	sendReplan = true;
	nopathlen = true;
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE: path-planner could not plan" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
	break;
      }
      // STOPOBS_BARE -> DRIVE_S1PLANNER_SAFETY
      if (USE_S1PLANNER_ON_ROAD ) {
	current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	//current_region = ROAD_REGION;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE: path-planner could not plan" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << endl;
      }
      else {
	if (backup_lp_state != current_lp_state) {
	  num_s1planner_backup = 0;
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE: Resetting num_s1planner_backup = 0" << endl;
	}
	else {
	  num_s1planner_backup++;
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE: Incrementing num_s1planner_backup = " 
			    << num_s1planner_backup << endl;
	}
	
	// STOPOBS_BARE -> BACKUP
	if (num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP && ALLOW_BACKUP) {
	  backup_lp_state = current_lp_state;
	  current_lp_state = LP_BACKUP;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: In STOPOBS_BARE: path-planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". backup_lp_state = " << backup_lp_state << endl;
	}
	// STOPOBS_BARE -> UTURN
	else {
	  current_lp_state = LP_PAUSE;
	  paused_lp_state = LP_DRIVE_PASS_BACKUP;
	  startNewState(vehState, replanInZone);
	  
	  current_lane = Utils::getCurrentLane(vehState, graph);
	  currentSegmentId = current_lane.segment;
	  currentLaneId = current_lane.lane;
	  ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	  Log::getStream(3) << "LOGIC_PLANNER:  In STOPOBS_BARE: path-planner could not plan " << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			    << currentSegmentId << " currentLaneId = " << currentLaneId
			    << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	}
      }
    }

    //current_region = ROAD_REGION;
    break;
  }

  case LP_DRIVE_S1PLANNER_SAFETY:
  case LP_DRIVE_S1PLANNER_AGG:
  case LP_DRIVE_S1PLANNER_BARE: {
    if ((prevErr & S1PLANNER_FAILED) ||
	(prevErr & S1PLANNER_START_BLOCKED) ||
        (prevErr & S1PLANNER_END_BLOCKED) ||
	(prevErr & S1PLANNER_COST_TOO_HIGH)) {
      last_obs_time = Utils::getTime();
      if (prevErr & S1PLANNER_FAILED) {
        Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_S1PLANNER: S1PLANNER_FAILED" << endl;
        Console::addMessage("LP: In DRIVE_S1PLANNER: S1PLANNER_FAILED");
      }
      if (prevErr & S1PLANNER_START_BLOCKED) {
        Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_S1PLANNER: S1PLANNER_START_BLOCKED" << endl;
        Console::addMessage("LP: In DRIVE_S1PLANNER: S1PLANNER_START_BLOCKED");
      }
      if (prevErr & S1PLANNER_END_BLOCKED) {
        Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_S1PLANNER: S1PLANNER_END_BLOCKED" << endl;
        Console::addMessage("LP: In DRIVE_S1PLANNER: S1PLANNER_END_BLOCKED");
      }
      if (prevErr & S1PLANNER_COST_TOO_HIGH) {
        Log::getStream(3) << "LOGIC_PLANNER: In DRIVE_S1PLANNER: S1PLANNER_COST_TOO_HIGH" << endl;
        Console::addMessage("LP: In DRIVE_S1PLANNER: S1PLANNER_COST_TOO_HIGH");
      }

      if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > MIN_TIME_IN_DRIVE_S1PLANNER) {
	// DRIVE_S1PLANNER_SAFETY -> DRIVE_S1PLANNER_AGG
	if (current_lp_state == LP_DRIVE_S1PLANNER_SAFETY &&
	    NUM_CHANGE_RP_OBS_SIZE > 0) {
	  current_lp_state = LP_DRIVE_S1PLANNER_AGG;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: s1planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	// DRIVE_S1PLANNER_AGG -> DRIVE_S1PLANNER_BARE
	else if (current_lp_state == LP_DRIVE_S1PLANNER_AGG &&
		 NUM_CHANGE_RP_OBS_SIZE > 1) {
	  current_lp_state = LP_DRIVE_S1PLANNER_BARE;
	  startNewState(vehState, replanInZone);
	  Log::getStream(3) << "LOGIC_PLANNER: s1planner could not plan" << ". Switching to "
			    << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			    << endl;
	}
	else {
	  if (backup_lp_state != current_lp_state) {
	    num_s1planner_backup = 0;
	    Log::getStream(3) << "LOGIC_PLANNER: Resetting num_s1planner_backup = 0" << endl;
	  }
	  else {
	    num_s1planner_backup++;
	    Log::getStream(3) << "LOGIC_PLANNER: Incrementing num_s1planner_backup = " << num_s1planner_backup << endl;
	  }
	  
	  // DRIVE_S1PLANNER_BARE -> BACKUP
	  if (num_s1planner_backup < MAX_NUM_S1PLANNER_BACKUP && ALLOW_BACKUP) {
	    backup_lp_state = current_lp_state;
	    current_lp_state = LP_BACKUP;
	    startNewState(vehState, replanInZone);
	    Log::getStream(3) << "LOGIC_PLANNER: s1planner could not plan" << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". backup_lp_state = " << backup_lp_state << endl;
	  }
	  // DRIVE_S1PLANNER_BARE -> UTURN
	  else {
	    current_lp_state = LP_PAUSE;
	    paused_lp_state = LP_DRIVE_PASS_BACKUP;
	    startNewState(vehState, replanInZone);
	    
	    current_lane = Utils::getCurrentLane(vehState, graph);
	    currentSegmentId = current_lane.segment;
	    currentLaneId = current_lane.lane;
	    ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	    Log::getStream(3) << "LOGIC_PLANNER: s1planner could not plan " << ". Switching to "
			      << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			      << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			      << currentSegmentId << " currentLaneId = " << currentLaneId
			      << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	  }
	}
      }
    }
    // DRIVE_S1PLANNER_* -> DRIVE_NOPASS
    else if ((Utils::getTime() - start_current_lp_state_time)/1000000.0 > MAX_TIME_USE_S1PLANNER ||
	     sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
		   pow(alice.y - start_current_lp_state_ypos, 2)) > MAX_DIST_USE_S1PLANNER) {
      current_lp_state = LP_DRIVE_NOPASS;
      startNewState(vehState, replanInZone);
      Log::getStream(3) << "LOGIC_PLANNER: S1PLANNER timeout (time = "
			<< (Utils::getTime() - start_current_lp_state_time)/1000000.0
			<< " s, dist = " 
			<< sqrt( pow(alice.x - start_current_lp_state_xpos, 2) + 
				 pow(alice.y - start_current_lp_state_ypos, 2))
			<< " m. Switching to "
                        << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                        << endl;
      Console::addMessage("LP: In DRIVE_S1PLANNER: Timeout. Switching to DRIVE_NOPASS");
    }
    //current_region = ROAD_REGION;
    break;
  }

  case LP_STOPINT:
  case LP_STOPINT_OFFROAD: {
    // Set useful variables
    current_velocity = AliceStateHelper::getVelocityMag(vehState);
    stopline_distance = Utils::getDistToStopline(params.m_path, params.seg_goal_queue, intersectionSegGoals);
    stopping_distance = pow(current_velocity,2)/(2.0*DESIRED_DECELERATION);
    Console::updateStopline(stopline_distance);

    if (params.m_path->hasStop)
      Utils::displayCircle(-30, point2(2.0, stopline_distance), 1.0, 3, MAP_COLOR_YELLOW, 103);
    else
      Utils::displayClear(-30, 3, 103);


    // STOP_INT -> DRIVE_NOPASS: We are stopped and have the priority
    // Being stopped in STOP_INT should imply that the stopline is less than a meter away
    // STOP_INT -> DRIVE

    Log::getStream(1)<<"stopline_distance ="<<stopline_distance<<" other value "<< stopping_distance + 5.0*INTERSECTION_BUFFER<<" current velocity = "<< current_velocity<<endl;

    if ((stopline_distance < -3.0 || (stopping_distance + 5.0*INTERSECTION_BUFFER) < stopline_distance) && !(prevErr & PP_NOPATH_LEN)) {
      if (current_lp_state == LP_STOPINT)
	current_lp_state = LP_DRIVE_NOPASS;
      else
	current_lp_state = LP_OFFROAD_SAFETY;
      startNewState(vehState, replanInZone);
      //sendReplan = true;
      Log::getStream(1)<<"LOGIC_PLANNER: switch back to DRIVE stopping_distance = "
                       <<stopping_distance<<" stopline_distance "<< stopline_distance <<endl;
      break;
    }

    if ((current_velocity < STOP_VEL) && (stopline_distance < 1.5) && 
        (stopline_distance>-1.0) && intersectionSegGoals.entryWaypointID == 0) {
      Console::addMessage("WARNING: Found stopline but no SegGoal. Switch back into DRIVE.");
      Log::getStream(1)<<"WARNING: Found stopline but no SegGoal. Switch back into DRIVE."<<endl;
      if (current_lp_state == LP_STOPINT)
	current_lp_state = LP_DRIVE_NOPASS;
      else
	current_lp_state = LP_OFFROAD_SAFETY;
      startNewState(vehState, replanInZone);
      //sendReplan = true;
      break;
    }

    if (((current_velocity < STOP_VEL) && (stopline_distance < 1.5) && (stopline_distance>-3.0)) ||
	((Utils::getTime() - start_current_lp_state_time)/1000000.0 > TIMEOUT_STOPINT &&
	 stopped_duration > TIMEOUT_STOPINT)) {
      Log::getStream(1) << "LOGIC_PLANNER: now start checking Intersection "
                        << stopline_distance<<" in lane "<<current_lane<<endl;
      Log::getStream(1)<< "Entry "<< intersectionSegGoals.entrySegmentID << "." 
		       << intersectionSegGoals.entryLaneID <<"."
		       <<intersectionSegGoals.entryWaypointID <<" -> Exit "
		       <<intersectionSegGoals.exitSegmentID<<"."
		       <<intersectionSegGoals.exitLaneID <<"." <<intersectionSegGoals.exitWaypointID << endl;
      inter_ret = IntersectionHandling->checkIntersection(vehState, intersectionSegGoals, params.mergingWaypoints, params.readConfig);
      if (inter_ret == CIntersectionHandling::GO && stopped_duration > 1.0) {
        if (intersectionSegGoals.segment_type == SegGoals::INTERSECTION) {
          last_intersection_point.segment = intersectionSegGoals.entrySegmentID;
          last_intersection_point.lane = intersectionSegGoals.entryLaneID;
          last_intersection_point.point = intersectionSegGoals.entryWaypointID;
        }
        else {
          last_intersection_point.segment = intersectionSegGoals.exitSegmentID;
          last_intersection_point.lane = intersectionSegGoals.exitLaneID;
          last_intersection_point.point = intersectionSegGoals.exitWaypointID;
        }
        last_intersection_time = Utils::getTime();
        current_lp_state = LP_DRIVE_NOPASS;
        startNewState(vehState, replanInZone);
        //sendReplan = true;
        Log::getStream(1)<<"LOGIC_PLANNER: Switching to "
                         << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                         << ". Setting last intersection point to " << last_intersection_point.segment
                         << "." << last_intersection_point.lane << "." << last_intersection_point.point
                         << endl;
        break;
      }
      // If the intersection is jammed, replan and switch to DRIVE_PASS
      else if (inter_ret == CIntersectionHandling::JAMMED) {
        if (intersectionSegGoals.segment_type == SegGoals::INTERSECTION) {
          last_intersection_point.segment = intersectionSegGoals.entrySegmentID;
          last_intersection_point.lane = intersectionSegGoals.entryLaneID;
          last_intersection_point.point = intersectionSegGoals.entryWaypointID;
        }
        else {
          last_intersection_point.segment = intersectionSegGoals.exitSegmentID;
          last_intersection_point.lane = intersectionSegGoals.exitLaneID;
          last_intersection_point.point = intersectionSegGoals.exitWaypointID;
        }
        last_intersection_time = Utils::getTime();
        sendReplan = true;
        current_lp_state = LP_DRIVE_PASS;
        //current_region = ROAD_REGION;
        startNewState(vehState, replanInZone);
        Log::getStream(3) << "LOGIC_PLANNER: Intersection is jammed" << ". Switching to "
                          << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                          << ". Setting last intersection point to " << last_intersection_point.segment
                          << "." << last_intersection_point.lane << "." << last_intersection_point.point
                          << endl;
        break;
      }

      /*
      // If the intersection is jammed, either switch to S1PLANNER or fail to mplanner.
      else if (inter_ret == IntersectionHandling::JAMMED) {
        last_intersection_point.segment = intersectionSegGoals.entrySegmentID;
        last_intersection_point.lane = intersectionSegGoals.entryLaneID;
        last_intersection_point.point = intersectionSegGoals.entryWaypointID;
        last_intersection_time = Utils::getTime();
        if (USE_S1PLANNER_ON_ROAD ) {
	  if (NUM_CHANGE_RP_OBS_SIZE < 1)
	    current_lp_state = LP_DRIVE_S1PLANNER_SAFETY;
	  else if (NUM_CHANGE_RP_OBS_SIZE < 2)
	    current_lp_state = LP_DRIVE_S1PLANNER_AGG;
	  else
	    current_lp_state = LP_DRIVE_S1PLANNER_BARE;
          current_region = ROAD_REGION;
	  startNewState(vehState, replanInZone);
          Log::getStream(3) << "LOGIC_PLANNER: Intersection is jammed" << ". Switching to "
                            << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                            << endl;
        }
        else {
          ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
          current_lp_state = LP_PAUSE;
          current_region = ROAD_REGION;
          paused_lp_state = LP_DRIVE_NOPASS;
	  startNewState(vehState, replanInZone);
          Log::getStream(3) << "LOGIC_PLANNER: Intersection is jammed " << ". Switching to "
                            << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
                            << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
                            << currentSegmentId << " currentLaneId = " << currentLaneId
                            << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
        }
        break;
      }
      */
    }

    // STOP_INT -> STOP_OBS: an obstacle popped in front of the car
    if (((prevErr & PP_COLLISION_SAFETY) || (prevErr & PP_NOPATH_LEN)) &&
	inter_ret != CIntersectionHandling::WAIT_PRECEDENCE &&
	inter_ret != CIntersectionHandling::WAIT_MERGING &&
	inter_ret != CIntersectionHandling::WAIT_CLEAR) {
      last_obs_time = Utils::getTime();
      stopobs_replan = true;
      // If path-planner is not able to plan because of a collision switch to STOP_OBS
      // if the obstacle is closer than the stopline
      current_lane = Utils::getCurrentLane(vehState, graph);
      obstacle_distance = Utils::getNearestObsOnPath(params.m_path, graph, vehState);
      stopline_distance = Utils::getDistToStopline(params.m_path, params.seg_goal_queue, intersectionSegGoals);
      if (obstacle_distance > 0.0 && stopline_distance > obstacle_distance + DIST_NOCHECK_OBS_STOPINT) {
	if (current_lp_state == LP_STOPINT)
	  current_lp_state = LP_STOPOBS_NOPASS;
	else
	  current_lp_state = LP_STOPOBS_OFFROAD_SAFETY;
	startNewState(vehState, replanInZone);
	Log::getStream(1)<<"LOGIC_PLANNER: an obstacle popped in front of the car. Switching to "
			 << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			 << endl;
	Console::addMessage("LP: In STOP_INT: an obstacle popped in front of the car");
	break;
      }
    }
    break;
  }

  default: {
    Log::getStream(1) << "ERROR: LOGIC_PLANNER: region = ROAD_REGION but current_lp_state = "
		      << lpstateToString(current_lp_state) << " is not handled." << endl;
    Console::addMessage("ERROR: LOGIC_PLANNER: region = ROAD_REGION but current_lp_state is not handled.");
    break;
  }
  }

 planLogic_end:


  ///////////////////////////////////////////////////////////////////////////////////////////////
  // Set all the states based on the logic planner internal state
  ///////////////////////////////////////////////////////////////////////////////////////////////

  // temporary: create one state problem with probability 1.0
  StateProblem_t problem;
  getStatesFromLPState(current_lp_state, current_state, current_flag, 
                       current_planner, current_obstacle, paused_lp_state);

  Log::getStream(5) << "LOGIC_PLANNER: current_lp_state = " << lpstateToString(current_lp_state)
                    << " (" << stateToString(current_state) << ", " << flagToString(current_flag)
                    << ", " << plannerToString(current_planner) << ", " << regionToString(current_region)
                    << ", " << obstacleToString(current_obstacle) << endl;

  /* Nok: I'm not sure if we should do this. Comment out for now.
  // Use S1PLANNER when mplanner tells us to do so
  if (params.segment_type == SegGoals::PARKING_ZONE) {
    if (USE_S1PLANNER_ON_ROAD ) {
      current_planner = S1PLANNER;
      Log::getStream(5) << "LOGIC_PLANNER: intersectionSegGoals.segment_type = ZONE. Switching to S1PLANNER." << endl;
    }
  }
  */

  /*
  // If we're in a zone, can't use RAIL_PLANNER
  if (current_region == ZONE_REGION) {
    current_planner = S1PLANNER;
    Log::getStream(5) << "LOGIC_PLANNER: in ZONE_REGION. Switching to S1PLANNER." << endl;
  }
  */

  // Change between S1PLANNER, DPLANNER, CIRCLE_PLANNER based on
  // cmdline option
  if (current_planner == S1PLANNER) {
    if (CmdArgs::use_s1planner)
      current_planner = S1PLANNER;
    else if (CmdArgs::use_dplanner)
      current_planner = DPLANNER;
    else if (CmdArgs::use_circle_planner)
      current_planner = CIRCLE_PLANNER;
  }

  //Utils::displayState(-2, 0);

  ///////////////////////////////////////////////////////////////////////////////////////////////
  // FAILURE HANDLER: See if we're stuck 
  ///////////////////////////////////////////////////////////////////////////////////////////////

  // get current time
  uint64_t currentTime;
  currentTime = Utils::getTime();

  // Start intersection timer as soon as state changes to STOP_INT
  // As soon as state is back into DRIVE, subtract intersection time from stopped_duration
  if (current_state == STOP_INT && !startIntersectionTimer) {
    DGCgettime(IntersectionTimeStart);
    startIntersectionTimer = true;
  }
  if (current_state == DRIVE && startIntersectionTimer) {
    DGCgettime(IntersectionTimeEnd);
    startIntersectionTimer = false;
    correct_value = stopped_duration;
  }

  // If stopped_duration is larger than the time Alice was stopped at the intersection, 
  // subtract this; otherwise the condition might be true too
  // but it will subtract 0
  if (!startIntersectionTimer && stopped_duration>=correct_value && correct_value!=0.0) {
    stopped_duration -= correct_value;
    Log::getStream(1)<<"FH: Correct stopped_duration by "<<correct_value<<endl;
  }
  else if (!startIntersectionTimer && stopped_duration<correct_value) {
    correct_value = 0.0;
    Log::getStream(1)<<"FH: Correct value reset back to 0.0"<<endl;
  }

  /*
  // NOK: I think this is not needed anymore because we detect PAUSE
  // hack, otherwise timeout at beginning
  if (stopped_duration > 3.0 && !firstTimeout) {
  Log::getStream(1) << "FH: Resetting stopped_duration to 0" << endl;
  stopped_duration = 0.0;
  lockFirstTimeout = true;
  }
  else if (stopped_duration < 5.0 && lockFirstTimeout) {
  Log::getStream(1) << "FH: setting firstTimeout = true" << endl;
  firstTimeout = true;
  }
  */

  // If we are close to an intersection, we want to wait a little longer
  // before that normally, the obstacle might start moving and thus become a vehicle to follow
  if ((current_state == STOP_OBS) && 
      stopline_distance > 0.0 && stopline_distance < MAX_INTERSECTION_Q_LENGTH)
    stopped_duration -= 2*TIMEOUT_OBS_INT;

  // If FailureHandler is deactivated through console, keep in current states and return. 
  // This will prevent the code further down to be executed
  params.planFromCurrPos = ((prevErr & PP_NOPATH_LEN) || (prevErr & PP_NOPATH_COST) ||
			    (prevErr & PP_NONODEFOUND) || sendReplan);

  if (!Console::queryToggleFailureHandling() || params.m_estop != EstopRun) {
    problem.state = current_state;
    problem.region = current_region;
    problem.obstacle = current_obstacle;
    problem.planner = current_planner;
    problem.probability = 1.0;
    problem.flag = current_flag;
    if (USE_S1PLANNER_ONLY)
      problem.planner = S1PLANNER;
    if (params.planFromCurrPos) {
      Log::getStream(5) << "LOGIC_PLANNER: planFromCurrPos" << endl;
      Console::addMessage("LP: planFromCurrPos");
    }
    else
      Log::getStream(5) << "LOGIC_PLANNER: NOT planFromCurrPos" << endl;
    problems.push_back(problem);
    return ret_value;
  }


  // Reset FH. Switch back into nominal mode (the mode before FH got triggered).
  double distFromLastAction = sqrt( pow(alice.x - fh_last_xpos, 2) + 
				    pow(alice.y - fh_last_ypos, 2));  

  if ( distFromLastAction > 3*FH_MAX_DISTANCE_NONNOMINAL ) {
    fh_last_xpos = 0;
    fh_last_ypos = 0;
    Log::getStream(3) << "LOGIC_PLANNER: Resetting fh variables" << endl;
  }

  if (fh_used && (Utils::getTime() - fh_last_action_time)/1000000.0 > FH_TIMEOUT_NONNOMINAL &&
      distFromLastAction > FH_MAX_DISTANCE_NONNOMINAL) {
    /* //We don't need to switch back into the nominal mode anymore. The main logic planner will
       //take care of this
    Log::getStream(1)<<"FH: Switch back into nominal current_lp_state = " << lpstateToString(current_lp_state)
		     << " fh_lp_state = " << lpstateToString(fh_lp_state) << " as nothing failed for "
		     << FH_TIMEOUT_NONNOMINAL << " seconds"<< endl;
    Console::addMessage("FH: Switch back into nominal");
    if (current_lp_state == LP_DRIVE_AGG || current_lp_state == LP_DRIVE_BARE ||
	current_lp_state != LP_DRIVE_S1PLANNER_AGG || current_lp_state == LP_DRIVE_S1PLANNER_BARE) {
      current_lp_state = fh_lp_state;
    }
    */
    Log::getStream(1) << "FH: reset FH as nothing failed for "
		      << FH_TIMEOUT_NONNOMINAL << " seconds, "
		      << distFromLastAction << " m" << endl;
    Console::addMessage("FH: reset FH");
    failureHandler(current_state, current_region, current_planner, 
		   current_obstacle, current_flag, inter_ret, 
                   sendReplan, sendBackup, sendFail, true);
    fh_used = false;
  }
  // If we have been stopped for a long time, trigger FH.
  else if (((stopped_duration > FH_MAX_STOP_TIME && current_lp_state != LP_STOPINT && current_lp_state != LP_UTURN) ||
	    (current_lp_state==LP_UTURN && stopped_duration > FH_MAX_STOP_TIME + 10)) && 
           (currentTime - fh_last_action_time)/1000000.0 > FH_MAX_STOP_TIME_BETWEEN_FH &&
           current_lp_state != LP_PAUSE) {
    // Determine if we're waiting for a vehicle before passing
    bool waitForVeh = false;
    for (int i = 1; i < params.m_path->pathLen && 
	   params.m_path->dists[i] < FH_DIST_CHECK_VEH_PASSING; i++) {
      if (params.m_path->nodes[i]->status.carCollision &&
          params.m_graph->isStatusFresh(params.m_path->nodes[i]->status.carTime)) {
	waitForVeh = true;
	Log::getStream(1)<<"FH: Car is in our path" << endl;
	break;
      }
    }

    // Determine if there are multiple lanes so we should wait longer
    vector<LaneLabel> lanesSameDir;
    LaneLabel laneAlice;
    laneAlice = Utils::getCurrentLane(vehState, graph);
    map->getSameDirLanes(lanesSameDir, laneAlice);

    // If we're not in PASS and there're multiple lanes, we want to wait longer so we don't have to
    // do illegal lane change.
    // If we're in PASS, we want to make sure that we're not waiting for a vehicle
    if ((current_flag != PASS && 
	 (lanesSameDir.size() <= 1 || stopped_duration > TIMEOUT_OBS_PASSING_EXTENDED + FH_MAX_STOP_TIME_BETWEEN_FH)) || 
	(current_flag == PASS && !waitForVeh) ||
	stopped_duration > FH_MIN_TIME_WAIT_TO_PASS) {
      bool just_finished_backup = false;
      if (current_lp_state == LP_DRIVE_PASS_BACKUP ||
	  current_lp_state == LP_STOPOBS_PASS_BACKUP)
	just_finished_backup = true;
      
      Console::addMessage("FH: Stopped for %f",stopped_duration);
      Log::getStream(1)<<"FH: Stopped for "<<stopped_duration<<endl;
      fh_lp_state = current_lp_state;
      fh_last_action_time = Utils::getTime();
      fh_last_xpos = alice.x;
      fh_last_ypos = alice.y;
      
      failureHandler(current_state, current_region, current_planner, 
		     current_obstacle, current_flag, inter_ret, 
		     sendReplan, sendBackup, sendFail, false);
    
      getLPStatesFromStates(current_lp_state, current_state,
			    current_flag, current_planner, 
			    current_obstacle, current_region,
			    just_finished_backup);

      if (fh_lp_state != current_lp_state)
	startNewState(vehState, replanInZone);
      
      fh_used = true;

      // handles the return values from the failure handler
      params.planFromCurrPos = sendReplan;
      if (sendReplan) {
	Log::getStream(3) << "FH: return sendReplan" << endl;
	Console::addMessage("FH: planFromCurrPos");
      }
      
      if (sendFail) {
	paused_lp_state = current_lp_state;
	current_lp_state = LP_PAUSE;
	startNewState(vehState, replanInZone);
	
	current_lane = Utils::getCurrentLane(vehState, graph);
	currentSegmentId = current_lane.segment;
	currentLaneId = current_lane.lane;
	ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
	Log::getStream(3) << "FH: return sendFail" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << ". Send LP_FAIL_MPLANNER_ROAD_BLOCKED with currentSegmentId = " 
			  << currentSegmentId << " currentLaneId = " << currentLaneId
			  << " paused_lp_state = " << lpstateToString(paused_lp_state) << endl;
	ret_value |= LP_FAIL_MPLANNER_ROAD_BLOCKED;
      }
      
      if (sendBackup && ALLOW_BACKUP) {
	current_state = BACKUP;
	backup_lp_state = fh_lp_state;
	current_lp_state = LP_BACKUP;
	startNewState(vehState, replanInZone);
	Log::getStream(3) << "FH: return sendBackup" << ". Switching to "
			  << lpstateToString(current_lp_state) << ", " << regionToString(current_region)
			  << ". backup_lp_state = " << backup_lp_state << endl;
      }
    }
    else {
      Log::getStream(1)<<"FH: Wait for car" << endl;
    }
  }
  else {
    Log::getStream(5) << "FH: not active. Stopped for "<< stopped_duration 
                      << " Time since last action = " 
                      << (currentTime - fh_last_action_time)/1000000.0 << endl;
  }

  // Change between S1PLANNER, DPLANNER, CIRCLE_PLANNER based on
  // cmdline option
  if (current_planner == S1PLANNER) {
    if (CmdArgs::use_s1planner)
      current_planner = S1PLANNER;
    else if (CmdArgs::use_dplanner)
      current_planner = DPLANNER;
    else if (CmdArgs::use_circle_planner)
      current_planner = CIRCLE_PLANNER;
  }

  problem.state = current_state;
  problem.region = current_region;
  problem.obstacle = current_obstacle;
  problem.planner = current_planner;
  problem.probability = 1.0;
  problem.flag = current_flag;
  if (USE_S1PLANNER_ONLY)
    problem.planner = S1PLANNER;
  problems.push_back(problem);

  // returning LP_OK means that there is at least one problem in the array
  return ret_value;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void LogicPlanner::getLPStatesFromStates(LP_state_t& lp_state, FSM_state_t state,
                                         FSM_flag_t flag, FSM_planner_t planner, 
                                         FSM_obstacle_t obstacle,
                                         FSM_region_t region, bool just_finished_backup)
{
  if (flag == OFFROAD) {
    if (state == STOP_INT)
      lp_state = LP_STOPINT_OFFROAD;
    else if (state == DRIVE) {
      if (obstacle == OBSTACLE_SAFETY)
	lp_state = LP_OFFROAD_SAFETY;
      else if (obstacle == OBSTACLE_AGGRESSIVE)
	lp_state = LP_OFFROAD_AGG;
      else
	lp_state = LP_OFFROAD_BARE;
    }
    else {
      if (obstacle == OBSTACLE_SAFETY)
	lp_state = LP_STOPOBS_OFFROAD_SAFETY;
      else if (obstacle == OBSTACLE_AGGRESSIVE)
	lp_state = LP_STOPOBS_OFFROAD_AGG;
      else
	lp_state = LP_STOPOBS_OFFROAD_BARE;
    }
    return;
  }

  if (region == ZONE_REGION && planner == S1PLANNER) {
    if (obstacle == OBSTACLE_SAFETY)
      lp_state = LP_ZONE_SAFETY;
    else if (obstacle == OBSTACLE_AGGRESSIVE)
      lp_state = LP_ZONE_AGG;
    else
      lp_state = LP_ZONE_BARE;
    return;
  }

  switch (state) {
  case DRIVE:
    if (planner == RAIL_PLANNER) {
      if (obstacle == OBSTACLE_SAFETY) {
	if (flag == PASS_REV)
	  lp_state = LP_DRIVE_PASS_REV;
	else if (just_finished_backup)
	  lp_state = LP_DRIVE_PASS_BACKUP;
	else if (flag == PASS)
	  lp_state = LP_DRIVE_PASS;
	else
	  lp_state = LP_DRIVE_NOPASS;
      }
      else if (obstacle == OBSTACLE_AGGRESSIVE)
	lp_state = LP_DRIVE_AGG;
      else
	lp_state = LP_DRIVE_BARE;
    }
    else {
      if (obstacle == OBSTACLE_SAFETY)
	lp_state = LP_DRIVE_S1PLANNER_SAFETY;
      else if (obstacle == OBSTACLE_AGGRESSIVE)
	lp_state = LP_DRIVE_S1PLANNER_AGG;
      else
	lp_state = LP_DRIVE_S1PLANNER_BARE;
    }
    return;
  case STOP_INT:
    lp_state = LP_STOPINT;
    return;
  case STOP_OBS:
    if (obstacle == OBSTACLE_AGGRESSIVE)
      lp_state = LP_STOPOBS_AGG;
    else if (obstacle == OBSTACLE_BARE)
      lp_state = LP_STOPOBS_BARE;
    else if (just_finished_backup)
      lp_state = LP_STOPOBS_PASS_BACKUP;
    else if (flag == PASS_REV)
      lp_state = LP_STOPOBS_PASS_REV;
    else if (flag == PASS)
      lp_state = LP_STOPOBS_PASS;
    else
      lp_state = LP_STOPOBS_NOPASS;
    return;
  case BACKUP: 
    lp_state = LP_BACKUP;
    return;
  case UTURN:
    lp_state = LP_UTURN;
    return;
  case PAUSE:
    lp_state = LP_PAUSE;
    return;
  }

  Log::getStream(0)<<"ERROR: LP: UNKNOWN REGION" << endl;  
  Console::addMessage("ERROR: LP: UNKNOWN REGION");
  return;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void LogicPlanner::getStatesFromLPState(LP_state_t lp_state, FSM_state_t &state,
                                        FSM_flag_t &flag, FSM_planner_t &planner, 
                                        FSM_obstacle_t &obstacle, LP_state_t paused_lp_state)
{
  switch (lp_state) {
  case  LP_DRIVE_NOPASS:
    state = DRIVE;
    flag = NO_PASS;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_STOPOBS_NOPASS:
    state = STOP_OBS;
    flag = NO_PASS;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_DRIVE_PASS:
    state = DRIVE;
    flag = PASS;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_STOPOBS_PASS:
    state = STOP_OBS;
    flag = PASS;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_DRIVE_PASS_REV:
    state = DRIVE;
    flag = PASS_REV;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_STOPOBS_PASS_REV:
    state = STOP_OBS;
    flag = PASS_REV;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_BACKUP:
    state = BACKUP;
    flag = NO_PASS;
    if (NUM_CHANGE_RP_OBS_SIZE < 1)
      obstacle = OBSTACLE_SAFETY;
    else if (NUM_CHANGE_RP_OBS_SIZE < 2)
      obstacle = OBSTACLE_AGGRESSIVE;
    else
      obstacle = OBSTACLE_BARE;
    planner = S1PLANNER;
    if (BACKUP_WITH_RAIL_PLANNER)
      planner = RAIL_PLANNER;
    break;
  case LP_DRIVE_PASS_BACKUP:
    state = DRIVE;
    flag = PASS_REV;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_STOPOBS_PASS_BACKUP:
    state = STOP_OBS;
    flag = PASS_REV;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_DRIVE_AGG:
    state = DRIVE;
    flag = PASS_REV;
    obstacle = OBSTACLE_AGGRESSIVE;
    planner = RAIL_PLANNER;
    break;
  case LP_STOPOBS_AGG:
    state = STOP_OBS;
    flag = PASS_REV;
    if (!USE_RAIL_PLANNER_REV)
      flag = PASS;
    obstacle = OBSTACLE_AGGRESSIVE;
    planner = RAIL_PLANNER;
    break;
  case LP_DRIVE_BARE:
    state = DRIVE;
    flag = PASS_REV;
    if (!USE_RAIL_PLANNER_REV)
      flag = PASS;
    obstacle = OBSTACLE_BARE;
    planner = RAIL_PLANNER;
    break;
  case LP_STOPOBS_BARE:
    state = STOP_OBS;
    flag = PASS_REV;
    obstacle = OBSTACLE_BARE;
    planner = RAIL_PLANNER;
    break;
  case LP_UTURN:
    state = UTURN;
    flag = NO_PASS;
    if (NUM_CHANGE_RP_OBS_SIZE < 1)
      obstacle = OBSTACLE_SAFETY;
    else if (NUM_CHANGE_RP_OBS_SIZE < 2)
      obstacle = OBSTACLE_AGGRESSIVE;
    else
      obstacle = OBSTACLE_BARE;
    planner = S1PLANNER;
    break;
  case LP_PAUSE:
    state = PAUSE;
    flag = NO_PASS;
    obstacle = OBSTACLE_SAFETY;
    if (paused_lp_state == LP_DRIVE_S1PLANNER_SAFETY ||
        paused_lp_state == LP_DRIVE_S1PLANNER_AGG ||
        paused_lp_state == LP_DRIVE_S1PLANNER_BARE ||
        paused_lp_state == LP_ZONE_SAFETY ||
        paused_lp_state == LP_ZONE_AGG ||
        paused_lp_state == LP_ZONE_BARE)
      planner = S1PLANNER;
    else
      planner = RAIL_PLANNER;
    break;
  case LP_STOPINT:
    state = STOP_INT;
    flag = NO_PASS;
    obstacle = OBSTACLE_SAFETY;
    planner = RAIL_PLANNER;
    break;
  case LP_DRIVE_S1PLANNER_SAFETY:
    state = DRIVE;
    flag = PASS;
    planner = S1PLANNER;
    obstacle = OBSTACLE_SAFETY;
    break;
  case LP_DRIVE_S1PLANNER_AGG:
    state = DRIVE;
    flag = PASS;
    planner = S1PLANNER;
    obstacle = OBSTACLE_AGGRESSIVE;
    break;
  case LP_DRIVE_S1PLANNER_BARE:
    state = DRIVE;
    flag = PASS;
    planner = S1PLANNER;
    obstacle = OBSTACLE_BARE;
    break;
  case LP_ZONE_SAFETY:
    state = DRIVE;
    flag = NO_PASS;
    planner = S1PLANNER;
    obstacle = OBSTACLE_SAFETY;
    break;
  case LP_ZONE_AGG:
    state = DRIVE;
    flag = NO_PASS;
    planner = S1PLANNER;
    obstacle = OBSTACLE_AGGRESSIVE;
    break;
  case LP_ZONE_BARE:
    state = DRIVE;
    flag = NO_PASS;
    planner = S1PLANNER;
    obstacle = OBSTACLE_BARE;
    break;
  case LP_OFFROAD_SAFETY:
    state = DRIVE;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_SAFETY;
    break;
  case LP_STOPOBS_OFFROAD_SAFETY:
    state = STOP_OBS;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_SAFETY;
    break;
  case LP_OFFROAD_AGG:
    state = DRIVE;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_AGGRESSIVE;
    break;
  case LP_STOPOBS_OFFROAD_AGG:
    state = STOP_OBS;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_AGGRESSIVE;
    break;
  case LP_OFFROAD_BARE:
    state = DRIVE;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_BARE;
    break;
  case LP_STOPOBS_OFFROAD_BARE:
    state = STOP_OBS;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_BARE;
    break;
  case LP_STOPINT_OFFROAD:
    state = STOP_INT;
    flag = OFFROAD;
    planner = RAIL_PLANNER;
    obstacle = OBSTACLE_SAFETY;
    break;
  }
}


string LogicPlanner::lpstateToString(LP_state_t state)
{
  switch (state) {
  case LP_DRIVE_NOPASS:
    return string("LP_DRIVE_NOPASS");
  case LP_STOPOBS_NOPASS:
    return string("LP_STOPOBS_NOPASS");
  case LP_DRIVE_PASS:
    return string("LP_DRIVE_PASS");
  case LP_STOPOBS_PASS:
    return string("LP_STOPOBS_PASS");
  case LP_DRIVE_PASS_REV:
    return string("LP_DRIVE_PASS_REV");
  case LP_STOPOBS_PASS_REV:
    return string("LP_STOPOBS_PASS_REV");
  case LP_BACKUP:
    return string("LP_BACKUP");
  case LP_DRIVE_PASS_BACKUP:
    return string("LP_DRIVE_PASS_BACKUP");
  case LP_STOPOBS_PASS_BACKUP:
    return string("LP_STOPOBS_PASS_BACKUP");
  case LP_DRIVE_AGG:
    return string("LP_DRIVE_AGG");
  case LP_STOPOBS_AGG:
    return string("LP_STOPOBS_AGG");
  case LP_DRIVE_BARE:
    return string("LP_DRIVE_BARE");
  case LP_STOPOBS_BARE:
    return string("LP_STOPOBS_BARE");
  case LP_UTURN:
    return string("LP_UTURN");
  case LP_PAUSE:
    return string("LP_PAUSE");
  case LP_STOPINT:
    return string("LP_STOPINT");
  case LP_DRIVE_S1PLANNER_SAFETY:
    return string("LP_DRIVE_S1PLANNER_SAFETY");
  case LP_DRIVE_S1PLANNER_AGG:
    return string("LP_DRIVE_S1PLANNER_AGG");
  case LP_DRIVE_S1PLANNER_BARE:
    return string("LP_DRIVE_S1PLANNER_BARE");
  case LP_ZONE_SAFETY:
    return string("LP_ZONE_SAFETY");
  case LP_ZONE_AGG:
    return string("LP_ZONE_AGG");
  case LP_ZONE_BARE:
    return string("LP_ZONE_BARE");
  case LP_OFFROAD_SAFETY:
    return string("LP_OFFROAD_SAFETY");
  case LP_STOPOBS_OFFROAD_SAFETY:
    return string("LP_STOPOBS_OFFROAD_SAFETY");
  case LP_OFFROAD_AGG:
    return string("LP_OFFROAD_AGG");
  case LP_STOPOBS_OFFROAD_AGG:
    return string("LP_STOPOBS_OFFROAD_AGG");
  case LP_OFFROAD_BARE:
    return string("LP_OFFROAD_BARE");
  case LP_STOPOBS_OFFROAD_BARE:
    return string("LP_STOPOBS_OFFROAD_BARE");
  case LP_STOPINT_OFFROAD:
    return string("LP_STOPINT_OFFROAD");
  }
  return string("");
}

string LogicPlanner::stateToString(FSM_state_t state)
{
  switch (state) {
  case DRIVE:
    return string("DRIVE");
  case STOP_INT:
    return string("STOP_INT");
  case STOP_OBS:
    return string("STOP_OBS");
  case PAUSE:
    return string("PAUSE");
  case UTURN:
    return string("UTURN");
  case BACKUP:
    return string("BACKUP");
  }
  return string("UNKNOWN");
}

string LogicPlanner::flagToString(FSM_flag_t flag)
{
  switch (flag) {
  case PASS:
    return string("PASS");
  case NO_PASS:
    return string("NO_PASS");
  case OFFROAD:
    return string("OFFROAD");
  case PASS_REV:
    return string("PASS_REV");
  }
  return string("UNKNOWN");
}

string LogicPlanner::regionToString(FSM_region_t region)
{
  switch (region) {
  case ROAD_REGION:
    return string("ROAD_REGION");
  case ZONE_REGION:
    return string("ZONE_REGION");
  case INTERSECTION:
    return string("INTERSECTION");
  }
  return string("UNKNOWN");
}

string LogicPlanner::plannerToString(FSM_planner_t planner)
{
  switch (planner) {
  case RAIL_PLANNER:
    return string("RAIL_PLANNER");
  case S1PLANNER:
    return string("S1PLANNER");
  case DPLANNER:
    return string("DPLANNER");
  case CIRCLE_PLANNER:
    return string("CIRCLE_PLANNER");
  }
  return string("UNKNOWN");
}

string LogicPlanner::obstacleToString(FSM_obstacle_t obstacle)
{
  switch (obstacle) {
  case OBSTACLE_SAFETY:
    return string("OBSTACLE_SAFETY");
  case OBSTACLE_AGGRESSIVE:
    return string("OBSTACLE_AGGRESSIVE");
  case DPLANNER:
    return string("OBSTACLE_BARE");
  }
  return string("UNKNOWN");
}


void LogicPlanner::resetState()
{
  current_state = DRIVE;
  current_region = ROAD_REGION;
  current_obstacle = OBSTACLE_SAFETY;
  current_planner = RAIL_PLANNER;
}

void LogicPlanner::startNewState(VehicleState &vehState, bool& replanInZone)
{
  point2 alice = AliceStateHelper::getPositionFrontBumper(vehState);
  start_current_lp_state_time = Utils::getTime();
  start_current_lp_state_xpos = alice.x;
  start_current_lp_state_ypos = alice.y;
  replanInZone = true;
}

void LogicPlanner::failureHandler(FSM_state_t &state, FSM_region_t &region, 
                                  FSM_planner_t &planner, FSM_obstacle_t &obstacle, FSM_flag_t &flag, 
                                  CIntersectionHandling::IntersectionReturn inter_ret, bool &sendReplan, 
                                  bool &sendBackup, bool &sendFail, bool resetFH)
{
  // Counters
  static int counterDriveRoad = 0;
  static int counterDriveZone = 0;
  static int counterDriveOffRoad = 0;
  static int counterStopint = 0;
  static int counterStopobs = 0;
  static int counterPause = 0;
  static int counterUturn = 0;
  static int counterBackup = 0;
  
  int currentCounter, index;

  if (resetFH) {
    counterDriveRoad = 0;
    counterDriveZone = 0;
    counterDriveOffRoad = 0;
    counterStopint = 0;
    counterStopobs = 0;
    counterPause = 0;
    counterUturn = 0;
    counterBackup = 0;
    return;
  }

  // some constants
  const int DRIVE_ROAD_STATE = 0;
  const int DRIVE_ZONE_STATE = 1;
  const int DRIVE_OFFROAD_STATE = 2;
  const int STOPINT_STATE = 3;
  const int STOPOBS_STATE = 4;
  const int PAUSE_STATE = 5;
  const int UTURN_STATE = 6;
  const int BACKUP_STATE = 7;
  const int NUM_OPTIONS = 8;

  // MATRIX - STRUCTURE
  // DRIVE_ROAD; DRIVE_ZONE; DRIVE_OFFROAD; STOP_INT; STOP_OBS; PAUSE; UTURN; BACKUP
  const int REPLAN1[]   = {1, 0, 0, 0, 1, 0, 0, 0};
  const int REPLAN2[]   = {5, 0, 0, 0, 5, 0, 0, 0};
  const int PLANNER[]   = {8, 0, 3, 3, 8, 0, 0, 0};
  const int OBSTACLE1[] = {6, 1, 1, 1, 6, 0, 1, 1};
  const int OBSTACLE2[] = {7, 2, 2, 2, 7, 0, 2, 2};
  const int BACK_UP[]   = {4, 0, 3, 0, 4, 0, 0, 0};
  const int FLAG1[]     = {2, 0, 0, 0, 2, 0, 0, 0};
  const int FLAG2[]     = {3, 0, 0, 0, 3, 0, 0, 0};

  switch(state) {
  case DRIVE:
    if (flag == OFFROAD) {
      currentCounter = ++counterDriveOffRoad;
      index = DRIVE_OFFROAD_STATE;
    }
    else if (region == ZONE_REGION && planner == S1PLANNER) {
      currentCounter = ++counterDriveZone;
      index = DRIVE_ZONE_STATE;
    }
    else {
      currentCounter = ++counterDriveRoad;
      index = DRIVE_ROAD_STATE;
    }
    
    Log::getStream(1)<<"FH: Failure in DRIVE. Counter "<<currentCounter<<endl;
    break;
  case STOP_INT:
    currentCounter = ++counterStopint;
    index = STOPINT_STATE;
    Log::getStream(1)<<"FH: Failure in STOP_INT. Counter "<<currentCounter<<endl;
    break;
  case STOP_OBS:
    currentCounter = ++counterStopobs;
    index = STOPOBS_STATE;
    Log::getStream(1)<<"FH: Failure in STOP_OBS. Counter "<<currentCounter<<endl;
    break;
  case PAUSE:
    currentCounter = ++counterPause;
    index = PAUSE_STATE;
    Log::getStream(1)<<"FH: Failure in PAUSE. Counter "<<currentCounter<<endl;
    break;
  case UTURN:
    currentCounter = ++counterUturn;
    index = UTURN_STATE;
    Log::getStream(1)<<"FH: Failure in UTURN. Counter "<<currentCounter<<endl;
    break;
  case BACKUP:
    currentCounter = ++counterBackup;
    index = BACKUP_STATE;
    Log::getStream(1)<<"FH: Failure in BACKUP. Counter "<<currentCounter<<endl;
    break;
  }


  if (currentCounter == REPLAN1[index] || currentCounter == REPLAN2[index]) {
    // set replan-flag
    if (sendReplan && currentCounter <= NUM_OPTIONS)
      failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		     sendReplan, sendBackup, sendFail, false);
    else {
      sendReplan = true;
      Console::setFailureIndex(currentCounter);
      Console::addMessage("FH: REPLAN ");
      Log::getStream(1)<<"FH: Send replan from current position"<<endl;
    }
  }
  else if (currentCounter == PLANNER[index]) {
    // change planner
    if (planner == S1PLANNER && currentCounter <= NUM_OPTIONS)
      failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		     sendReplan, sendBackup, sendFail, false);
    else {
      planner = S1PLANNER;
      Console::setFailureIndex(currentCounter);
      Console::addMessage("FH: PLANNER ");
      Log::getStream(1)<<"FH: Set planner to: S1Planner"<<endl;
    }
  }
  else if (currentCounter == OBSTACLE1[index] ||
	   currentCounter == OBSTACLE2[index]) {
    // change obstacle
    switch(obstacle) {
    case OBSTACLE_SAFETY:
      if (NUM_CHANGE_RP_OBS_SIZE < 1 && currentCounter <= NUM_OPTIONS)
	failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		       sendReplan, sendBackup, sendFail, false);
      else {
	obstacle = OBSTACLE_AGGRESSIVE;
	Log::getStream(1)<<"FH: Set obstacle to: OBSTACLE_AGGRESSIVE"<<endl;
	break;
      }
    case OBSTACLE_AGGRESSIVE:
      if (NUM_CHANGE_RP_OBS_SIZE < 2 && currentCounter == OBSTACLE1[index] && currentCounter <= NUM_OPTIONS)
	failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		       sendReplan, sendBackup, sendFail, false);
      else {
	obstacle = OBSTACLE_BARE;
	Log::getStream(1)<<"FH: Set obstacle to: OBSTACLE_BARE"<<endl;
	break;
      }
    case OBSTACLE_BARE:
      if (currentCounter <= NUM_OPTIONS)
	failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		       sendReplan, sendBackup, sendFail, false);
      break;
    }
    Console::addMessage("FH: OBSTACLE ");
    Console::setFailureIndex(currentCounter);
  }
  else if (currentCounter == BACK_UP[index] && current_region != ZONE_REGION) {
    if (!ALLOW_BACKUP && currentCounter <= NUM_OPTIONS)
      failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		     sendReplan, sendBackup, sendFail, false);
    // command backup command
    else {
      sendBackup = true;
      Console::setFailureIndex(currentCounter);
      Console::addMessage("FH: BACKUP");
      Log::getStream(1)<<"FH: Send backup"<<endl;
    }
  }
  else if (currentCounter == FLAG1[index]) {
    if (flag != NO_PASS && currentCounter <= NUM_OPTIONS)
      failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		     sendReplan, sendBackup, sendFail, false);
    // do not change anything here while Alice is within Zones
    else {
      flag = PASS;
      Console::setFailureIndex(currentCounter);
      Console::addMessage("FH: PASS ");
      Log::getStream(1)<<"FH: Set flag: PASS"<<endl;
    }
  }
  else if (currentCounter == FLAG2[index]) {
    if ((!USE_RAIL_PLANNER_REV || flag == PASS_REV)  && currentCounter <= NUM_OPTIONS)
      failureHandler(state, region, planner, obstacle, flag, inter_ret, 
		     sendReplan, sendBackup, sendFail, false);
    else {
      flag = PASS_REV;
      Console::setFailureIndex(currentCounter);
      Console::addMessage("FH: PASS_REV");
      Log::getStream(1)<<"FH: Set flag: PASS_REV"<< endl;
    }
  }
  else {
    // send fail bit to MPLANNER
    sendFail = true;
      
    counterDriveRoad = 0;
    counterDriveZone = 0;
    counterStopint = 0;
    counterStopobs = 0;
    counterPause = 0;
    counterUturn = 0;
    counterBackup = 0;
    Console::setFailureIndex(currentCounter);
    Console::addMessage("FH: MPLANNER FAIL ");
  }
}
