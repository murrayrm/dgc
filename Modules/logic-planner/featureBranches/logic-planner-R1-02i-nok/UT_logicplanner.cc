/**********************************************************
 **
 **  UT_PLANNER.CC
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>

#include "IntersectionHandling.hh"
#include "gcinterfaces/SegGoals.hh"
#include "graph-updater/GraphUpdater.hh"

using namespace std;

char **split_text(char *string,char *token, int *count)
{
     char **retval = NULL;
     char *ptr = NULL;
     *count = 0;
     ptr = strtok(string,token);

       while(ptr != NULL)
    {
         *count = *count + 1;
            retval = (char**)realloc(retval,(*count * sizeof(char*)));
            retval[*count - 1] = strdup(ptr);
          ptr = strtok(NULL,token);
     }
     return retval;
}


int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);

  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  CmdArgs::stepbystep_load = false;
  CmdArgs::load_graph_files = false;

  Graph_t* graph;
  GraphUpdater::init(&graph, map);

  CMapElementTalker testMap;
  testMap.initSendMapElement(CmdArgs::sn_key);

  MapElement el;
  for (unsigned int k=0; k<map->prior.data.size(); k++)
    {
      map->prior.getEl(el,k);
      testMap.sendMapElement(&el,-2);
    }


  IntersectionHandling::init();
  IntersectionHandling::reset(map);
  VehicleState vehState;
  SegGoals segGoal;

  int count1, count2;
  char **wp1 = split_text(args[2],".",&count1);
  char **wp2 = split_text(args[3],".",&count2);

  segGoal.entrySegmentID = atoi(wp1[0]);
  segGoal.entryLaneID = atoi(wp1[1]);
  segGoal.entryWaypointID = atoi(wp1[2]);

  segGoal.exitSegmentID = atoi(wp2[0]);
  segGoal.exitLaneID = atoi(wp2[1]);
  segGoal.exitWaypointID = atoi(wp2[2]);

  MapElement me;
  me.id=1234;
  me.setTypePoints();
  point2 p = point2(-5, -70);
  me.setPosition(p);
  me.setGeometry(p, 1.0);
  me.setColor(MAP_COLOR_RED,100);
  map->addEl(me);

  
  vector<PointLabel> WayPointsEntries;
  IntersectionHandling::populateWayPoints(graph, map, PointLabel(segGoal.entrySegmentID, segGoal.entryLaneID, segGoal.entryWaypointID),WayPointsEntries);
  //  IntersectionHandling::checkIntersection(vehState, graph, map, segGoal);
  point2arr leftBound, rightBound;

  SegGoals tempSegGoal;

  IntersectionHandling::getTransitionAlongGraph(graph, map, leftBound, PointLabel(segGoal.entrySegmentID, segGoal.entryLaneID, segGoal.entryWaypointID), PointLabel(segGoal.exitSegmentID, segGoal.exitLaneID, segGoal.exitWaypointID), tempSegGoal, false, -1);
  IntersectionHandling::getTransitionAlongGraph(graph, map, rightBound, PointLabel(segGoal.entrySegmentID, segGoal.entryLaneID, segGoal.entryWaypointID), PointLabel(segGoal.exitSegmentID, segGoal.exitLaneID, segGoal.exitWaypointID), tempSegGoal, false, 1);

  // Send left bound to map
  stringstream s;
  me.id = 1235;
  me.setTypeLine();
  me.setColor(MAP_COLOR_GREEN,100);
  me.setGeometry(leftBound);
  s.str("");
  s<<"Left bound";
  me.label.clear();
  me.label.push_back(s.str());
  testMap.sendMapElement(&me,-2);
      
  // Send right bound to map
  me.id = 1236;
  me.setGeometry(rightBound);
  s.str("");
  s<<"Right bound";
  me.label.clear();
  me.label.push_back(s.str());
  testMap.sendMapElement(&me,-2);
 
}

