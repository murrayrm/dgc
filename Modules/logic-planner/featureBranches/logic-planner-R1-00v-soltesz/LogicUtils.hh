#ifndef LOGICUTILS_HH_
#define LOGICUTILS_HH_

#include <frames/point2.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <gcinterfaces/SegGoals.hh>

class LogicUtils {

public: 
  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(point2 &pos, Map *map, LaneLabel &lane);
  static double getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getAngleInRange(double angle);
  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement **me, VehicleState &vehState, Map *map, LaneLabel &lane); 
  static double monitorAliceSpeed(VehicleState &vehState);
  static void updateStoppedMapElements(Map *map);
  static uint64_t getTime();

private :
};

#endif /*LOGICUTILS_HH_*/




