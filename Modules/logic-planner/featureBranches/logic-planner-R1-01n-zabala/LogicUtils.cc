#include "LogicUtils.hh"
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Console.hh>
#include <math.h>
#include <map/MapElementTalker.hh>
#include <frames/vec3.h>

bool LogicUtils::isObstacleInSafetyBox(VehicleState vehState, Map* map, double front, double rear, double sides)
{
  // Construct the bounding box around alice
  point2 point;
  point2arr aliceBox, obsGeo;
  double offsetFront = DIST_REAR_AXLE_TO_FRONT + front;
  double offsetRear = -(DIST_REAR_TO_REAR_AXLE + rear);
  double offsetSide = VEHICLE_WIDTH + sides;
  double heading = AliceStateHelper::getHeading(vehState);
  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  // Front left
  point.set(currPos.x + (offsetFront)*cos(heading)-(offsetSide)*sin(heading), currPos.y + (offsetFront)*sin(heading)+(offsetSide)*cos(heading));
  aliceBox.push_back(point);
  // Front right
  point.set(currPos.x + (offsetFront)*cos(heading)-(-offsetSide)*sin(heading), currPos.y + (offsetFront)*sin(heading)+(-offsetSide)*cos(heading));
  aliceBox.push_back(point);
  // Rear right
  point.set(currPos.x + (offsetRear)*cos(heading)-(-offsetSide)*sin(heading), currPos.y + (offsetRear)*sin(heading)+(-offsetSide)*cos(heading));
  aliceBox.push_back(point);
  // Rear left
  point.set(currPos.x + (offsetRear)*cos(heading)-(offsetSide)*sin(heading), currPos.y + (offsetRear)*sin(heading)+(offsetSide)*cos(heading));
  aliceBox.push_back(point);
  // Front left again to close the polygon - not sure why this is necessary
  point.set(currPos.x + (offsetFront)*cos(heading)-(offsetSide)*sin(heading), currPos.y + (offsetFront)*sin(heading)+(offsetSide)*cos(heading));
  aliceBox.push_back(point);

  //  Log::getStream(1)<<"LogicUtils::isObstacleInSafetyBox: Alice box = " << aliceBox << endl << "offsets: f, r, s = " << offsetFront << " " << offsetRear << " " << offsetSide << " " << endl;

  // get all the obstacles close to Alice
  double radius = sqrt(offsetFront*offsetFront + offsetSide*offsetSide) + 2;
  vector<MapElement> obstacles;
  map->getObsNearby(obstacles, currPos, radius);

  // Cycle through the mapelements and return true as soon as an overlapping obstacle is found
  for (unsigned i=0; i<obstacles.size(); i++) {
    obsGeo = ((point2arr)obstacles[i].geometry);
    if (obsGeo.is_intersect(aliceBox)) {
      //    if (obstacles[i].isOverlap(aliceBox)) {
      Console::addMessage("Obstacle in Alice' safety box. Pausing now.");
      return true;
    }
  }

  return false;

}

