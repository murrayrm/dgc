#ifndef INTERSECTIONHANDLING_HH
#define INTERSECTIONHANDLING_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "sys/time.h"
#include "map/Map.hh"
#include "gcinterfaces/SegGoals.hh"
#include <trajutils/maneuver.h>
#include "temp-planner-interfaces/PlannerInterfaces.h"


class IntersectionHandling
{ 
public:
  enum IntersectionReturn
    { 
      RESET,
      WAIT_PRECEDENCE,
      WAIT_MERGING,
      WAIT_CLEAR,
      GO,
      JAMMED
    };

  // Functions for dyn. obstacles at intersections
  static void init();
  static void reset(Graph_t* g, Map* m);
  static void updateConsole();
  static IntersectionReturn checkIntersection(VehicleState vehState, SegGoals currSegment);

private:

  struct WayPoints_t {
    PointLabel WayPoint;
    bool monitorWhileMerging;
  };

  // Graph and map
  static Map* localMap;
  static Graph_t* graph;

  // Variables for checking the intersection
  static vector<PrecedenceList_t> PrecedenceList;
  static vector<MapElement> ClearanceList;
  static vector<MapElement> MergingList;
  static IntersectionReturn ReturnValue;
  static bool FirstCheck;
  static time_t timestamp;
  static PointLabel entryWaypoint;

  static vector<PointLabel> WayPointsWithStop;
  static vector<WayPoints_t> WayPointsNoStop;
  static vector<PointLabel> WayPointsEntries;
  static bool JammedTimerStarted;

  static double DISTANCE_STOPLINE_OBSTACLE;
  static double DISTANCE_STOPLINE_ALICE;
  static double VELOCITY_OBSTACLE_THRESHOLD;
  static double DISTANCE_OBSTACLE_THRESHOLD;
  static double ETA_EPS;
  static bool INTERSECTION_SAFETY_FLAG;
  static bool INTERSECTION_CABMODE_FLAG;
  static double TRACKING_VEHICLE_SEPERATION;
  static int WAIT_CYCLES;
  static double INTERSECTION_TIMEOUT;
  static double INTERSECTION_TIME_CLEARANCE;
  static double INTERSECTION_GROW_CORRIDOR;
  static double MAXIMUM_VELOCITY;

  static CMapElementTalker testMap;
  static int mapcounter;
  static const int MAPSTART = 6000;

  static const int sendChannel=-2;
  static bool clearset;
  static unsigned long long cleartime;
  static unsigned long long IntersectionStartTime;
  static double IntersectionTimer;
  static void populateWayPoints(PointLabel InitialWayPointEntry);
  static void detectIntersectingTrafficWhileMerging(SegGoals currSegment);
  static void findStoplines(PointLabel InitialWayPointEntry);
  static void maintainPrecedenceList(VehicleState vehState);
  static int checkFeasibility(bool result, PointLabel entryPointLabel, PointLabel exitPointLabel);

  static IntersectionReturn checkPrecedence();
  static IntersectionReturn checkMerging(SegGoals currSegment);
  static IntersectionReturn checkClearance(SegGoals currSegment);

  static bool checkVisibility(VehicleState Alice,MapElement obstacle);
  static int getTransitionAlongGraph(point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, SegGoals currSegment, bool grow, int leftright);
  static int getTransition(point2arr& transition, PointLabel entryPointLabel, PointLabel exitPointLabel);
  static bool crossPoint(point2arr a, point2arr b, point2 &crosspoint);
  static bool inSection(point2 a, point2 b, point2 x);
};

#endif  // INTERSECTIONHANDLING_HH
