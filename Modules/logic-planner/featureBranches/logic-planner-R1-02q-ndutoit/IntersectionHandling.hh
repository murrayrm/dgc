#ifndef INTERSECTIONHANDLING_HH
#define INTERSECTIONHANDLING_HH

// stf package includes
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <pthread.h>
#include <vector>
#include <math.h>
#include <time.h>

// skynet, talker, and interfaces
#include "skynet/sn_msg.hh"
#include "skynettalker/StateClient.hh"
#include "skynettalker/SkynetTalker.hh"
#include "map/MapElementTalker.hh"
#include "dgcutils/DGCutils.hh"
#include "sys/time.h"
#include "map/Map.hh"
#include "gcinterfaces/SegGoals.hh"
#include <trajutils/maneuver.h>
#include "temp-planner-interfaces/PlannerInterfaces.h"


class IntersectionHandling
{ 
public:
  enum IntersectionReturn
    { 
      RESET,
      WAIT_LEGAL,
      WAIT_POSSIBLE,
      WAIT_CLEAR,
      GO,
      JAMMED
    };
  // Functions for dyn. obstacles at intersections
  static void init();
  static void reset(Map* localMap);
  static void updateConsole();
  static IntersectionReturn checkIntersection(VehicleState vehState, Graph_t* graph, Map* localMap,SegGoals currSegment);
  static void populateWayPoints(Graph_t* graph, Map* localMap, PointLabel InitialWayPointEntry, vector<PointLabel> &WP);
  static void findStoplines(Graph_t* graph, Map* localMap, SegGoals currSegment);
  static void maintainPrecedenceList(Graph_t* graph, Map* localMap,VehicleState vehState);
  static IntersectionReturn checkPrecedence();
  static IntersectionReturn checkMerging(Graph_t* graph, Map* localMap,SegGoals currSegment);
  static IntersectionReturn checkClearance(Graph_t* graph, Map* localMap,SegGoals currSegment);
  static bool checkVisibility(Map* localMap, VehicleState Alice,MapElement obstacle);
  static void toggleIntersectionSafety();
  static void toggleIntersectionCabmode();
  static int getTransition(Graph_t* graph, Map* localMap, point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, int leftright);
  static bool crossPoint(point2arr a, point2arr b, point2 &crosspoint);
  static bool inSection(point2 a, point2 b, point2 x);
  static int getClosestIndex(int &index, point2arr parr, point2 p);
  static int getTransitionAlongGraph(Graph_t* graph, Map* localMap, point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, SegGoals currSegment, bool grow, int leftright);

  static bool init_flag;

private:
  // Variables for checking the intersection
  static vector<PrecedenceList_t> PrecedenceList;
  static vector<MapElement> ClearanceList;
  static vector<MapElement> MergingList;
  static IntersectionReturn ReturnValue;
  static bool FirstCheck;
  static time_t timestamp;
  static PointLabel entryWaypoint;

  static vector<PointLabel> WayPointsWithStop;
  static vector<PointLabel> WayPointsNoStop;
  static vector<PointLabel> WayPointsEntries;
  static bool JammedTimerStarted;

  static double DISTANCE_STOPLINE_OBSTACLE;
  static double DISTANCE_STOPLINE_ALICE;
  static double VELOCITY_OBSTACLE_THRESHOLD;
  static double DISTANCE_OBSTACLE_THRESHOLD;
  static double ETA_EPS;
  static bool INTERSECTION_SAFETY_FLAG;
  static bool INTERSECTION_CABMODE_FLAG;
  static double TRACKING_VEHICLE_SEPERATION;
  static int WAIT_CYCLES;
  static double INTERSECTION_TIMEOUT;
  static double INTERSECTION_TIME_CLEARANCE;
  static double INTERSECTION_GROW_CORRIDOR;
  static double MAXIMUM_VELOCITY;

  static CMapElementTalker testMap;
  static int mapcounter;
  static const int MAPSTART = 6000;

  static const int sendChannel=-2;
  static bool clearset;
  static unsigned long long cleartime;
  static unsigned long long IntersectionStartTime;
  static double IntersectionTimer;
};

#endif  // INTERSECTIONHANDLING_HH
