#include "IntersectionHandling.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Log.hh"
#include <sstream>
#include <iomanip>
#include "temp-planner-interfaces/Console.hh"
#include "ConfigFile.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include "alice/AliceConstants.h"
#include <cotk/cotk.h>
#include <ncurses.h>
#include "LogicUtils.hh"
#include "dgcutils/cfgfile.h"
// #include "planner/cmdline.h"
using namespace std;

/* Initialize class constants */
double IntersectionHandling::DISTANCE_STOPLINE_ALICE=1;
double IntersectionHandling::VELOCITY_OBSTACLE_THRESHOLD=0.2;
double IntersectionHandling::DISTANCE_OBSTACLE_THRESHOLD=6;
double IntersectionHandling::ETA_EPS=0.5;
double IntersectionHandling::TRACKING_VEHICLE_SEPERATION=1.0;
bool IntersectionHandling::INTERSECTION_SAFETY_FLAG=1;
bool IntersectionHandling::INTERSECTION_CABMODE_FLAG=0;

bool IntersectionHandling::init_flag=false;
time_t IntersectionHandling::timestamp;
vector<ListToA> IntersectionHandling::VehicleList;
vector<ListBlockedObstacles> IntersectionHandling::BlockedObstacles;

IntersectionHandling::IntersectionReturn IntersectionHandling::ReturnValue;

vector<PointLabel> IntersectionHandling::WayPointsWithStop;
vector<PointLabel> IntersectionHandling::WayPointsNoStop;
vector<PointLabel> IntersectionHandling::WayPointsEntries;
CMapElementTalker IntersectionHandling::testMap;
bool IntersectionHandling::JammedTimerStarted;
bool IntersectionHandling::FirstCheck;


void IntersectionHandling::init()
{
   char *path;

   path=dgcFindConfigFile("IntersectionHandling.conf","logic-planner");
   if (fopen(path,"r") == NULL)
    {
      Console::addMessage("Warning: Couldn't read Intersection configuration.");
      Log::getStream(1)<<"Warning: Couldn't read Intersection configuration."<<endl;
      Log::getStream(1)<<fopen("IntersectionHandling.conf","r")<<endl;
    }
  else
    {
      ConfigFile config(path);
      config.readInto(DISTANCE_STOPLINE_ALICE,"DISTANCE_STOPLINE_ALICE");
      config.readInto(VELOCITY_OBSTACLE_THRESHOLD,"VELOCITY_OBSTACLE_THRESHOLD");
      config.readInto(DISTANCE_OBSTACLE_THRESHOLD,"DISTANCE_OBSTACLE_THRESHOLD");
      config.readInto(ETA_EPS,"ETA_EPS");
      config.readInto(TRACKING_VEHICLE_SEPERATION,"TRACKING_VEHICLE_SEPERATION");
      config.readInto(INTERSECTION_SAFETY_FLAG,"INTERSECTION_SAFETY_FLAG");
      config.readInto(INTERSECTION_CABMODE_FLAG,"INTERSECTION_CABMODE_FLAG");
      Log::getStream(1)<<"Info: Read Intersection configuration successfully."<<endl;
    }
}

void IntersectionHandling::reset(Map* localMap)
{
  if (!init_flag)
    {
      ReturnValue=RESET;

      // This resets the intersection, so the next time, a new intersection will be created
      WayPointsEntries.clear();
      WayPointsWithStop.clear();
      WayPointsNoStop.clear();

      VehicleList.clear();
      BlockedObstacles.clear();
      JammedTimerStarted=false;
      FirstCheck=false;
  
      
      // Initialize Debug-Map
      testMap.initSendMapElement(CmdArgs::sn_key);
      
      //Read RNDF from real map and pass it to Debug-Map
      // REDO: Read RNDF from file
      MapElement el;
      for (unsigned int k=0; k<localMap->prior.data.size(); k++)
        {
          localMap->prior.getEl(el,k);
          testMap.sendMapElement(&el,sendChannel);
        }
      init_flag=true;
    }
  updateConsole();
}

void IntersectionHandling::updateConsole()
{
  // Update Console

  int counter=0;
  for (unsigned int i=0; i<VehicleList.size(); i++)
      if (VehicleList[i].precedence)
        counter++;

  string str="";

  switch(ReturnValue)
    {
    case RESET:         str="RESET        "; break;
    case WAIT_LEGAL:    str="WAIT_LEGAL   "; break;
    case WAIT_POSSIBLE: str="WAIT_POSSIBLE"; break;
    case WAIT_CLEAR:    str="WAIT_CLEAR   "; break;
    case GO:            str="GO           "; break;
    case JAMMED:        str="JAMMED       "; break;
    }
  
  Console::updateInter(INTERSECTION_SAFETY_FLAG,INTERSECTION_CABMODE_FLAG,counter,str);
}


IntersectionHandling::IntersectionReturn IntersectionHandling::checkIntersection(VehicleState vehState,Map* localMap, SegGoals currSegment)
{
  // Send Alice to the map
  MapElement alice;
  alice.set_alice(vehState);
  testMap.sendMapElement(&alice,sendChannel);

  // Create intersection if it didn't happen yet
  if (WayPointsEntries.size()==0)
    createIntersection(vehState,localMap);
  
  // If intersection created successfully
  if (WayPointsEntries.size()>0)
    {
      // Check for current status of other obstacles.
      checkExistenceObstacles(localMap,vehState);

      if (ReturnValue==RESET || ReturnValue==WAIT_LEGAL)
        ReturnValue=checkPrecedence();

      if (ReturnValue==WAIT_POSSIBLE ||  ReturnValue==WAIT_CLEAR)
        ReturnValue=checkMerging(localMap,currSegment);
      
      // While waiting for possibility to merge, do not start timer yet!
      if (ReturnValue==WAIT_POSSIBLE)
        JammedTimerStarted=false;
      
      if (ReturnValue==WAIT_CLEAR)
        ReturnValue=checkClearance(localMap,currSegment);
    }
  else
    {
      // Output some debugging information
      Log::getStream(7)<<"Error: Couldn't build Intersection. But give ok for Legal&Clear to go."<<endl;
      Console::addMessage("Error: Couldn't build Intersection. But give ok for Legal&Clear to go.");

      ReturnValue=GO;
    }
 
  updateConsole();

  if (ReturnValue==GO || ReturnValue==JAMMED )
    init_flag=false;

  return ReturnValue;
}

void IntersectionHandling::createIntersection(VehicleState vehState,Map* localMap)
{
  vector<PointLabel> WayPoint;
  vector<PointLabel> StopLines;
  int c;
  point2 p;
  double distance_temp;

  // Obtain Alice's position
  point2 position_alice = AliceStateHelper::getPositionFrontBumper(vehState);
  LaneLabel lane_alice;
  localMap->getLane(lane_alice,position_alice);

  // get Alice's centerline
  point2arr centerline_alice;
  localMap->getLaneCenterLine(centerline_alice,lane_alice);

  if (localMap->getLaneStopLines(StopLines,lane_alice)!=0)
    {
      double distance_stopline_alice=INFINITY;

      // Find closest stopline in Alice's direction of travel and save PointLabel and distance
      for (unsigned int i=0; i<StopLines.size(); i++)
        {
          localMap->getWaypoint(p,StopLines[i]);
          localMap->getDistAlongLine(distance_temp,centerline_alice,p,position_alice);

          if ((distance_temp+DISTANCE_STOPLINE_ALICE)>0 && distance_temp<distance_stopline_alice)
            {
              distance_stopline_alice=distance_temp;
              c=i;
            }
          if (distance_temp>-2 && distance_temp>2)
            Log::getStream(1)<<"Warning: Alice might be stopped behind stopline. Found stopline at distance "<<distance_temp<<endl;
        }

      if (distance_stopline_alice<INFINITY)
        {
          // get all entry points of the intersection
          WayPointsEntries.clear();
          populateWayPoints(localMap,StopLines[c],WayPointsEntries,TRUE);
          findStoplines(localMap);
        }
    }
}

void IntersectionHandling::populateWayPoints(Map* localMap, PointLabel InitialWayPointEntry, vector<PointLabel> &WP, bool sendMap)
{
  vector<PointLabel> WayPointExits, WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++)
    {
      localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++)
        {
          bool found=false;
          for (unsigned int k=0; k<WP.size(); k++)
            {
              if (WayPoint[j]==WP[k])
                {
                  found=true;
                  break;
                }
            }

          // If not, add it to list and call function recursivly
          if (!found)
            {
              WP.push_back(WayPoint[j]);
              populateWayPoints(localMap,WayPoint[j],WP,TRUE);
            }
        }
    }

  if (sendMap)
    {
      // Send Waypoints to map
      MapElement me;
      point2 p;
      int status;
      stringstream s;
      for (unsigned k=0; k<WP.size(); k++)
        {
          me.setId(k+5000);
          me.setTypeWayPoints();
          localMap->getWayPoint(p,WP[k]);
          me.setPosition(p);
          s.str("");
          s<<"Waypoint "<<WP[k];
          me.label.clear();
          me.label.push_back(s.str());
          // little circle
          me.setGeometry(p,1.0);
          status=testMap.sendMapElement(&me,sendChannel);
        }
    }
}

void IntersectionHandling::findStoplines(Map* localMap)
{
  MapElement me;
  point2 p;
  stringstream s;
  
  for (unsigned int i=0; i<WayPointsEntries.size(); i++)
    {
      if (localMap->isStopLine(WayPointsEntries[i]))
        {
          // Store information in vector
          WayPointsWithStop.push_back(WayPointsEntries[i]);

          // Send stopline to map, color red
          me.setId(i+6000);
          me.setTypeStopLine();
          me.setColor(MAP_COLOR_RED);
          localMap->getWayPoint(p,WayPointsEntries[i]);
          me.setGeometry(p,1,1);
          me.setPosition(p);
          s.str("");
          s<<"Stopsign at Waypoint "<<WayPointsEntries[i];
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);
        }
      else
        {
          // Store information in vector
          WayPointsNoStop.push_back(WayPointsEntries[i]);	

          // Even so there is no stopline, send a stopline with color green to the map
          me.setId(i+6000);
          me.setTypeStopLine();
          me.setColor(MAP_COLOR_GREEN);
          localMap->getWayPoint(p,WayPointsEntries[i]);
          me.setPosition(p);
          s.str("");
          s<<"NO Stopsign at Waypoint "<<WayPointsEntries[i];
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);
        }
    }
}

void IntersectionHandling::checkExistenceObstacles(Map* localMap,VehicleState vehState)
{
  point2 p,p2,position_obstacle;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,distance;
  ListToA list_temp;
  int foundindex;
  vector<ListToA> tempVehicleList;
  int CounterObstacle=0;
  MapElement obstacleMap;
  stringstream s;
  
  // Reset updated-flag
  for (unsigned int k=0; k<VehicleList.size(); k++)
    VehicleList[k].updated=false;

  CounterObstacle=0;


  // Look for obstacles at those waypoints with stopsigns
  for (unsigned int i=0; i<WayPointsWithStop.size(); i++)
    {
      localMap->getWayPoint(p,WayPointsWithStop[i]);
      localMap->getLane(lane,p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);


      // Loop through all obstacles found in lane
      for (unsigned int j=0;j<obstacle.size();j++)
        {
          if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE)
            continue;

          distance_geometry=INFINITY;
          // Get distance from stopline to closest point of obstacle
          for (unsigned l=0; l<obstacle[j].geometry.size();l++)
            {
              position_obstacle.set(obstacle[j].geometry[l]);
              localMap->getDistAlongLine(distance_temp, centerline, p, position_obstacle);
              if (distance_temp > 0.0 && distance_temp < distance_geometry)
                distance_geometry = distance_temp;
            }

          // Look within 30m of stopline
          if (distance_geometry>0 && distance_geometry<30)
            {
              //              Console::addMessage("obst %i.%i.%i d=%f.2",WayPointsWithStop[i].segment,WayPointsWithStop[i].lane,WayPointsWithStop[i].point,distance_geometry);

              CounterObstacle++;

              // Look whether vehicle already exists
              foundindex=-1;

              // Store information that this vehicle still exist in list and won't be deleted later by "garbage collector"
              for (unsigned int k=0; k<VehicleList.size(); k++)
                {
                  if (VehicleList[k].element.id==obstacle[j].id)
                    {
                      VehicleList[k].updated=true;
                      foundindex=k;
                    }
                }

              // Don't add or update information when Alice stopped.
              // If Alice is stopped, we only wait until existing vehicles with ETA<epsilon are leaving intersection
              // If vehicle is new, then add it to list
              // DOES THIS STILL WORK??????
              if (foundindex==-1 && !FirstCheck)
                {
                  if (distance_temp>0)
                    {
                      // Create list object
                      list_temp.element=obstacle[j];
                      list_temp.WayPoint=WayPointsWithStop[i];
                      list_temp.velocity=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));
                      list_temp.distance=distance_geometry;
                      list_temp.precedence=false;
                      list_temp.checkedQueuing=false;
                      list_temp.updated=true;
		      
                      // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<.5m/s
                      if (list_temp.velocity>=VELOCITY_OBSTACLE_THRESHOLD)
                        list_temp.eta=list_temp.distance/list_temp.velocity;
                      else if (list_temp.distance>=DISTANCE_OBSTACLE_THRESHOLD)
                        list_temp.eta=INFINITY;
                      else
                        list_temp.eta=0;
		      
                      // store new vehicle
                      VehicleList.push_back(list_temp);

                      Log::getStream(7)<<"Obstacle "<<obstacle[j].id<<" stored"<<endl;
                      Log::getStream(7)<<"distance "<<list_temp.distance<<endl;
                      Log::getStream(7)<<"velocity "<<list_temp.velocity<<endl;
                      Log::getStream(7)<<"ETA "<<list_temp.eta<<endl;
                    }
                }
            }
        }
    }



  // This prevents that new vehicles will be added to list after Alice came to a stopline
  FirstCheck=true;

  int found;

  // Clean up VehicleList "Garbage collector"
  for (unsigned int k=0; k<VehicleList.size(); k++)
    {
      // If obstacle seems to disappear, do some checks first before deleting this vehicle
      if (VehicleList[k].updated==false)
        {
          //          Console::addMessage("Try deleting obstacle out of list");
          // If there is something blocking the view, keep the obstacle that seemed to disappear
          if (!checkVisibility(localMap,vehState,VehicleList[k].element))
            VehicleList[k].updated=true;

          // the obstacle might have changed the ID. So look for obstacles that are really close to the old ID. If so, assign this new object to the VehicleList
          // obtain position of old obstacle and get the lane that is is supposed to be 
          
          p.set(VehicleList[k].element.position);
          localMap->getLane(lane,p);
          localMap->getLaneCenterLine(centerline,lane);
          localMap->getObsInLane(obstacle,lane);

          distance=INFINITY;
          found=-1;
          for (unsigned int m=0; m<obstacle.size(); m++)
            {
              p2.set(obstacle[m].position);
              localMap->getDistAlongLine(distance_temp,centerline,p,p2);
              if (fabs(distance_temp)<distance)
                {
                  distance=fabs(distance_temp);
                  found=m;
                }
            }

          // if there is an obstacle than replace the old obstacle with this new one
          if (found!=-1 && distance<TRACKING_VEHICLE_SEPERATION)
            {
              Console::addMessage("Found obstacle nearby. Do not delete in list");
              VehicleList[k].element=obstacle[found];
              VehicleList[k].updated=true;
            }
        }
     
      // only delete object when it is not updated
      if (VehicleList[k].updated==false)
        {
          // Clear element out of map
          obstacleMap.setId(VehicleList[k].element.id);
          obstacleMap.setTypeClear();
          testMap.sendMapElement(&obstacleMap,sendChannel);

          Log::getStream(7)<<"Remove obstacle at WayPoint..."<<VehicleList[k].WayPoint<<"... ObstacleID..."<<VehicleList[k].element.id<<endl;
        }
      else
        tempVehicleList.push_back(VehicleList[k]);
    }
  VehicleList=tempVehicleList;

  // Output all vehicles that are at the intersection currently to the map
  for (unsigned int m=0;m<VehicleList.size();m++)
    {
      // Send obstacle to map
      if (VehicleList[m].precedence)
        VehicleList[m].element.setColor(MAP_COLOR_YELLOW,100);
      else
        VehicleList[m].element.setColor(MAP_COLOR_GREY,100);
	  
      s.str("");
      if (VehicleList[m].element.type==ELEMENT_OBSTACLE)
        s<<"stat.Obs., ";
      else if (VehicleList[m].element.type==ELEMENT_VEHICLE)
        s<<"dyn.Obs., ";
      else
        s<<"unk.Obs., ";
      
      s<<"d="<<setprecision(3)<<VehicleList[m].distance<<", ";
      s<<"v="<<setprecision(3)<<VehicleList[m].velocity<<", ";
      s<<"ETA="<<setprecision(3)<<VehicleList[m].eta;
      if (VehicleList[m].precedence)
        s<<"Prec=TRUE";
	  
      VehicleList[m].element.label.clear();
      VehicleList[m].element.label.push_back(s.str());
      testMap.sendMapElement(&VehicleList[m].element,sendChannel);
    }
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkPrecedence()
{
  MapElement me;
  //  stringstream s;

  // All Vehicles with a very small ETA have precedence
  for (unsigned int i=0; i<VehicleList.size(); i++)
    {
      // only the vehicle with the closest distance to each waypoint shall have precedence
      if (!VehicleList[i].checkedQueuing)
        {
          VehicleList[i].closest=true;
	  
          // get obstacle that is the closest to the stopline
          for (unsigned k=0; k<VehicleList.size(); k++)
            {
              // If vehicles are at same waypoint and one is further away than the other, uncheck flag
              if ((VehicleList[i].WayPoint==VehicleList[k].WayPoint) && (VehicleList[i].distance>VehicleList[k].distance))
                VehicleList[i].closest=false;
            }

          // If vehicle is not flagged as "closest" and is within 3m to the closest obstacle than also give this obstacle
          // precedence as it should be the same obstacle
          if (!VehicleList[i].closest)
            {
              for (unsigned z=0; z<VehicleList.size(); z++)
                {
                  if ((VehicleList[i].WayPoint==VehicleList[z].WayPoint) && (VehicleList[z].closest) && (VehicleList[i].distance-VehicleList[z].distance<3.0))
                    VehicleList[i].closest=true;
                }
            }
	    
          // Set flag so that this check is only done once per vehicle
          VehicleList[i].checkedQueuing=true;
        }

      // If ETA is so small that it should have precedence and is the first vehicle in queue, then this
      // vehicle is flagged with precedene
      if (VehicleList[i].eta<ETA_EPS && VehicleList[i].closest)
        VehicleList[i].precedence=true;
      else
        VehicleList[i].precedence=false;
    }
  
  int counter=0;
  for (unsigned int i=0; i<VehicleList.size(); i++)
    if (VehicleList[i].precedence)
      counter++;

  // If there are other vehicles with precedence, return false
  if (counter>0)
    return WAIT_LEGAL;
  else
    return WAIT_POSSIBLE;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkMerging(Map* localMap, SegGoals currSegment)
{
  point2 p,p_exit,position_obstacle;
  LaneLabel lane,lane_exit;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,velocity;

  for (unsigned int i=0; i<WayPointsNoStop.size(); i++)
    {
      // get lane of waypoint that we are looping through
      localMap->getWayPoint(p,WayPointsNoStop[i]);
      localMap->getLane(lane,p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);

      // get WayPoint that Alice would like to go to
      PointLabel WayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
      // get all WayPoints that can exit to the Waypoint that Alice wants to exit to
      localMap->getWayPointEntries(WayPointsEntries,WayPointExit);

      // dependant on the safety flag, check every waypoint or only those that can exit to the waypoint that Alice wants to exit to
      if (!INTERSECTION_SAFETY_FLAG)
        {
          bool found=false;
          for (unsigned int m=0; m<WayPointsEntries.size(); m++)
            if (WayPointsEntries[m]==WayPointsNoStop[i])
              found=true;
          // if not found, continue in loop; so this waypoint won't be checked for traffic
          if (!found) continue;
        }


      // Loop through all obstacles found in lane
      for (unsigned int j=0;j<obstacle.size();j++)
        {
          if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE)
            continue;

          distance_geometry=INFINITY;
          // Get distance from waypoint to closest point of obstacle
          for (unsigned l=0; l<obstacle[j].geometry.size();l++)
            {
              position_obstacle.set(obstacle[j].geometry[l]);
              localMap->getDistAlongLine(distance_temp, centerline, p, position_obstacle);
              if (distance_temp > 0.0 && distance_temp < distance_geometry)
                distance_geometry = distance_temp;
            }

          // DARPA requires to have a 10s gap
          velocity=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));
	  
          // check whether vehicle is approaching the intersection (condition >0) and whether it is far away enough
          if (distance_geometry>0 && distance_geometry<10*velocity)
            return WAIT_POSSIBLE;
        }
    }
  
  return WAIT_CLEAR;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkClearance(Map* localMap,SegGoals currSegment)
{
  point2arr centerline;
  vector<PointLabel> WayPointExits;
  point2 p_entry,p_exit,position_obstacle;
  vector<MapElement> obstacles;
  int counter=0;
  bool found=false;
  ListBlockedObstacles tempBlockObstacle;
  vector<ListBlockedObstacles> tempList;
  point2arr leftBound, rightBound;
  MapElement me;
  LaneLabel lane_exit;
  //  double distance_exit;
  stringstream s;

  // reset status of all obstacles blocking the intersection
  for (unsigned k=0; k<BlockedObstacles.size(); k++)
    BlockedObstacles[k].updated=false;

  // start timer to make sure that Alice doesn't brake the 10s rule
  if (!JammedTimerStarted)
    {
      time(&timestamp);
      JammedTimerStarted=true;
    }

  // get current WayPointEntry and WayPointEntry of Alice
  PointLabel currentWayPointEntry(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  PointLabel currentWayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

  int idcounter=3000;
  // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
  for (unsigned i=0; i<WayPointsEntries.size(); i++)
    {
      // if safety flag is not set, only check clearance in current entry/exit-corridor. Skip the rest
      if (!INTERSECTION_SAFETY_FLAG && !(WayPointsEntries[i]==currentWayPointEntry))
        continue;

      localMap->getWaypoint(p_entry,WayPointsEntries[i]);

      // get all exits for this entry
      localMap->getWayPointExits(WayPointExits,WayPointsEntries[i]);
      for (unsigned j=0;j<WayPointExits.size(); j++)
        {
          // if safety flag is not set, only check clearance in current entry/exit-corridor. Skip the rest
          if (!INTERSECTION_SAFETY_FLAG && !(WayPointExits[j]==currentWayPointExit))
            continue;

          // Create TransitionBound between this WayPointEntry and WayPointExit
          localMap->getShortTransitionBounds(leftBound, rightBound, WayPointsEntries[i], WayPointExits[j]);
          
          // Get all obstacles within these bounds
          localMap->getObsInBounds(obstacles, leftBound, rightBound);

          // Send left bound to map
          idcounter++;
          me.setId(idcounter);
          me.setTypeLine();
          me.setColor(MAP_COLOR_GREEN,100);
          me.setGeometry(leftBound);
          s.str("");
          s<<"Left bound "<<WayPointsEntries[i]<<"->"<<WayPointExits[j];
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);

          // Send right bound to map
          idcounter++;
          me.setId(idcounter);
          me.setGeometry(rightBound);
          s.str("");
          s<<"Right bound "<<WayPointsEntries[i]<<"->"<<WayPointExits[j];
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);
	  
          for (unsigned int k=0; k<obstacles.size(); k++)
            {
              // only matter when obstacle is blocking the corridor
              // actually this rule is only allowed after a 10s window!!!
              if (!INTERSECTION_SAFETY_FLAG && !LogicUtils::isObstacleBlockingBounds(obstacles[k],leftBound,rightBound))
                {
                  Console::addMessage("Intersection is partially blocked only. GO!");
                  continue;
                }

              counter++;
	      
              // send obstacle that blocks the lane	
              s.str("");
              if (obstacles[k].type==ELEMENT_OBSTACLE)
                s<<"stat.Obs., blocking intersection";
              else if (obstacles[k].type==ELEMENT_VEHICLE)
                s<<"dyn.Obs., blocking intersection";
              else
                s<<"unk.Obs., blocking intersection";

              obstacles[k].label.push_back(s.str());
              obstacles[k].setColor(MAP_COLOR_RED,100);
              testMap.sendMapElement(&obstacles[k],sendChannel);
              Log::getStream(7)<<"Intersection...blocked by obstacle "<<obstacles[k].id<<endl;

              found=false;
              // if obstacle is found in list, set updated=true
              for (unsigned l=0; l<BlockedObstacles.size(); l++)
                {
                  if (obstacles[k].id==BlockedObstacles[l].obstacle.id)
                    {
                      BlockedObstacles[l].updated=true;
                      found=true;
                    }
                }
              // otherwise add it to list
              if (!found)
                {
                  tempBlockObstacle.obstacle=obstacles[k];
                  tempBlockObstacle.updated=true;
                  BlockedObstacles.push_back(tempBlockObstacle);
                }
            }
        }
    }

  // Clear out obstacles that disappear from map; this doesnt affect Alice and is for debugging/mapping purposes only
  for (unsigned m=0;m<BlockedObstacles.size(); m++)
    {
      if (BlockedObstacles[m].updated)
        tempList.push_back(BlockedObstacles[m]);
      else
        {
          // clear obstacle out of map
          BlockedObstacles[m].obstacle.setTypeClear();
          testMap.sendMapElement(&BlockedObstacles[m].obstacle,sendChannel);
        }
    }
  BlockedObstacles=tempList;

  // check whether Alice needs to switch in cab mode
  time_t currentTime;
  time(&currentTime);
  double diff=difftime(currentTime,timestamp);

  if (counter>0 && diff<10.0)
    return WAIT_CLEAR;
  else if (counter>0 && diff>=10.0 && INTERSECTION_CABMODE_FLAG)
    {
      Console::addMessage("Warning: Switching into Cab mode");
      return JAMMED;
    }
  else if (counter>0 && diff>=10.0 && !INTERSECTION_CABMODE_FLAG)
    return WAIT_CLEAR;
  else
    return GO;
}

bool IntersectionHandling::checkVisibility(Map* localMap,VehicleState Alice,MapElement obstacle)
{
  double maxRight=-INFINITY;
  double maxLeft=INFINITY;
  point2 maxRightPoint2,maxLeftPoint2;

  // Obtain the max/min points of the obstacle's geometry
  for (unsigned int i=0; i<obstacle.geometry.size(); i++)
    {
      if (obstacle.geometry[i].x>maxRight)
        {
          maxRight=obstacle.geometry[i].x;
          maxRightPoint2.set(obstacle.geometry[i]);
        }
      if (obstacle.geometry[i].x<maxLeft)
        {
          maxLeft=obstacle.geometry[i].x;
          maxLeftPoint2.set(obstacle.geometry[i]);
        }
    }
  
  // Obtain front left and front right point of Alice
  point2 bumper=AliceStateHelper::getPositionFrontBumper(Alice);
  point2 frontLeft=point2(bumper.x+sin(Alice.localYaw)*VEHICLE_WIDTH/2,bumper.y-cos(Alice.localYaw)*VEHICLE_WIDTH/2);
  point2 frontRight=point2(bumper.x-sin(Alice.localYaw)*VEHICLE_WIDTH/2,bumper.y+cos(Alice.localYaw)*VEHICLE_WIDTH/2);

  point2arr leftBound,rightBound;
  leftBound.push_back(frontLeft);
  leftBound.push_back(maxLeftPoint2);
  rightBound.push_back(frontRight);
  rightBound.push_back(maxRightPoint2);

  int idcounter=10000;
  MapElement me;
  // Send left bound to map
  idcounter++;
  me.setId(idcounter);
  me.setTypeLine();
  me.setColor(MAP_COLOR_MAGENTA,100);
  me.setGeometry(leftBound);
  testMap.sendMapElement(&me,sendChannel);

  // Send right bound to map
  idcounter++;
  me.setId(idcounter);
  me.setGeometry(rightBound);
  testMap.sendMapElement(&me,sendChannel);

  // Get all obstacles within these bounds
  vector<MapElement> obst;
  localMap->getObsInBounds(obst, leftBound, rightBound);
  
  if (obst.size()>0)
    {
      double distObstacle=INFINITY;
      double dist=INFINITY;
      double distTemp;
      
      // calculate distance between the obstacle that we are looking at and Alice
      // this is independant from whether this obstacle does still exist or is gone already
      for (unsigned int k=0; k<obstacle.geometry.size(); k++)
        {
          distTemp=sqrt(pow(obstacle.geometry[k].x-Alice.localX,2)+pow(obstacle.geometry[k].y-Alice.localY,2));
          if (distTemp<distObstacle)
            distObstacle=distTemp;
        }
  
      for (unsigned int i=0; i<obst.size(); i++)
        {
          if (obst[i].id!=obstacle.id)
            {
              for (unsigned int l=0; l<obst[i].geometry.size(); l++)
                {
                  distTemp=sqrt(pow(obst[i].geometry[l].x-Alice.localX,2)+pow(obst[i].geometry[l].y-Alice.localY,2));
                  if (distTemp<dist)
                    dist=distTemp;
                }
            }
        }

      // If any other obstacle is closer, return false as it seems to be in betweenthe obstacle that we are looking at and Alice
      if (dist<distObstacle)
        {
          Console::addMessage("Warning: There might be an obstacle blocking the view to a vehicle with precedence");
          return false;
        }
      else
        return true;
    }
  else
    return true;
}





