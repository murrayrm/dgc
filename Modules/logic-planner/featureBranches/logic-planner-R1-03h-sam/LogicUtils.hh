#ifndef LOGICUTILS_HH_
#define LOGICUTILS_HH_

#include <frames/point2.hh>
#include <map/Map.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
//#include <gcinterfaces/SegGoals.hh>
//#include <temp-planner-interfaces/PlannerInterfaces.h>

class LogicUtils {

public: 
  static bool isObstacleInSafetyBox(VehicleState vehState, Map* map, double front, double rear, double sides);

private :
};

#endif /*LOGICUTILS_HH_*/


