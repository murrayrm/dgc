#ifndef LOGICPLANNER_HH_
#define LOGICPLANNER_HH_

#include <vector>

#include "map/Map.hh"
#include "frames/pose3.h"
#include "frames/point2.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "interfaces/VehicleState.h"
#include "temp-planner-interfaces/CmdArgs.hh"

class LogicPlanner {

public: 
  static int init();
  static void destroy();
  static Err_t planLogic(vector<StateProblem_t> &problems, Graph_t *graph, Err_t prevErr, VehicleState &vehState, Map *map, Logic_params_t params);
  static string stateToString(FSM_state_t state);
  static string flagToString(FSM_flag_t flag);
  static void resetState();

private:
  static FSM_state_t current_state;
  static FSM_flag_t current_flag;
};

#endif /*LOGICPLANNER_HH_*/




