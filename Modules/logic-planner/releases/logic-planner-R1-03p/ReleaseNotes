introduced in new versions of the 
"logic-planner" module. Thisinformation should be kept in mind when 
upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "logic-planner" module can be found in
the ChangeLog file.

Release R1-03p (Mon Nov 12  8:39:19 2007):
        * Changes in the field including using new getDistToObs function 
to fix the STOP_OBS-STOP_INT problem. Implemented the logic for 
using RAIL_PLANNER in zone.
        * check bug
        * fixed core dump
        * Adapt Intersection Timeouts to NQE A and handle false-positive 
intersections correctly
        * Fixed merging: correct calculation from obstacle to stop line 
and rejecting fake intersections
        * More output to log file about vehicles with precedence
        * Fixed bug in checkClearance
        * calls function to display the intersection state.
        * Adjusted conf file for NQE A. While merging, discard vehicles 
that don't have same heading as their lane
        * Area A tweaks
        * Set changes back to intersections with precedence, fitting the 
requirements for NQE run A and C
        * Handle the logic for using rail planner in zone (both nominal 
and offroad mode)
        * Reduced teh number of states from config file. Check with 
intersection handling before transitioning out of STOP_INT
        * Only check intersection once even when we reverse back to the 
previous intersection.
        * Fixed STOP_INT logic.
        * NQE run 3

Release R1-03o (Wed Oct 24 23:46:22 2007):
        * Wait longer when finding static obstacles in intersections
        * read from config files
        * Passes along the merging waypoints from IntersectionHandling 
to planner so that prediction will pick it up

Release R1-03n (Wed Oct 24 20:16:44 2007):
	Stable version as of 2007/10/24 8:30 pm	Deleted files: ConfigFile.cc ConfigFile.hh

	* Moved ConfigFile.* to temp-planner-interfaces
	* Handle 3 different types of collision. Allow rail planner to
	reverse in agg and bare mode. Can use rail planner in BACKUP mode.
	(Need to be specified in the config file.) Changed the condition
	for switching out of agg and bare mode. Allow an option for not
	switching to BACKUP after DRIVE_PASS_REV mode.
	* Use ConfigFile from temp-planner-interfaces
	* Changed the distance we check if we're getting lost.
	* We can start off the road now
	* Fixed bug where FH may try to back up even when the back up flag is off.
	* Changed the distance to check for obstacle when we're in DRIVE_PASS and 
DRIVE_PASS_REV to 10*MAX_PASSING_1_LENGTH
	* also handle PP_NOPATH_LEN
	* Limit the number of times we can transition to DRIVE_PASS_REV,
	DRIVE_AGG and DRIVE_BARE.
	* Transition from STOPOBS_PASS to STOPOBS_PASS_REVERSE if we've
	already transitioned to DRIVE_PASS_REVERSE many times.
	* Added timestamp to log
	* Solved bug in ROW intersections
	* Fixed bug in the config file where we still switch to S1PLANNER
	reverse even when ALLOW_BACKUP flag is false.



Release R1-03m (Mon Oct 22  8:15:01 2007):
        * Moved ConfigFile.* to temp-planner-interfaces
        * Handle 3 different types of collision. Allow rail planner to 
reverse in agg and bare mode. Can use rail planner in BACKUP mode. (Need 
to be specified in the config file.) Changed the condition for switching out of agg and bare mode. Allow an option for not  
switching to BACKUP after DRIVE_PASS_REV mode.
        * Use ConfigFile from temp-planner-interfaces
        * We can start off the road now.
        * Changed the distance we check if we're getting lost.
        * Fixed bug where FH may try to back up even when the back up 
flag is off.
        * Changed the distance to check for obstacle when we're in 
DRIVE_PASS and DRIVE_PASS_REV to 10*MAX_PASSING_1_LENGTH
        * also handle PP_NOPATH_LEN
        * Limit the number of times we can transition to DRIVE_PASS_REV, 
DRIVE_AGG and DRIVE_BARE.
        * Transition from STOPOBS_PASS to STOPOBS_PASS_REVERSE if we've 
already transitioned to DRIVE_PASS_REVERSE many times.
        * Added timestamp to log
        * Solved bug in ROW intersections
        * Fixed bug in the config file where we still switch to 
S1PLANNER reverse even when ALLOW_BACKUP flag is false.

Release R1-03l (Sat Oct 20 12:27:01 2007):
	* Integrated logic while losing track of vehicles while merging 
	* Waiting 6s at ROW intersections
	* Added more states for off-road: LP_OFFROAD_SAFETY, LP_OFFROAD_AGG, LP_OFFROAD_BARE. Currently only using LP_OFFROAD_SAFETY and PP_COLLISION is not handled in this state.
	* Added 7 more states: ** OFFROAD_SAFETY ** OFFROAD_AGG ** OFFROAD_BARE ** STOPOBS_OFFROAD_SAFETY ** STOPOBS_OFFROAD_AGG ** STOPOBS_OFFROAD_BARE ** STOPINT_OFFROAD  * Allow backup using rail 
planner. (Need to modify logicplanner.conf.)
	* Fixed bug where FH allows rail planner to reverse even when the config file doesn't allow that.
	* Fixed bug where we drove through obstacle when we're in DRIVE_PASS_REV
	* Added STOPOBS_PASS_REV state.
	* Changed the condition for determining the next waypoint in the path.
	* Reread config file when the user presses 'u' on the planner console
	* Added an option for not using s1planner on road
	* Fixed bug where we keep transitioning back to DRIVE_PASS_BACKUP when we don't use s1planner on road
	* Define STOP_VEL
�
Release R1-03k (Tue Oct 16 21:31:17 2007):
	* Adapted to new getDistToStopline function
	* Now new stop line structure handles row intersection 
	* checks the next 4 seg goals forrow intersections
	* Fixes from the field: Do not back up on 1 lane segments until
	s1planner obstacle_bare fails; reset the timer (that keeps track of
	how long we have been in the current state) when we are pause; when
	in start-chute, we are now stopping at intersections; switching to
	DRIVE from PAUSE if we were in backup; switch the order in the
	logic (try all options for rail planner before s1planner)

Release R1-03j (Mon Oct 15 23:39:31 2007):
	* Fixed bug in analysis part of intersections and adjusted for 
utils display fcns
	* counts number of switches between wait clear and wait merging	
	* special intersection handling for multiple lane roads
	* Added timeout for stopint and added transition from stopobs to 
stopint
	* implemented start chute handling
	* wait longer for failure handling in multi-lane roads.

Release R1-03i (Mon Oct 15  8:00:20 2007):
	* Bug fixes from previous el toro test.
	* Report when we are lost
	* updated for using graph freshness
	* tweaked some parameters.
	
Release R1-03h (Thu Oct 11  9:03:30 2007):
	* Fixed bugs where we sometimes transition from PAUSE to DRIVE_NOPASS
	instead of to paused_lp_state.
	* Fixed the logic planner config file

Release R1-03g (Thu Oct 11  2:08:49 2007):
	* Report to mplanner when planner is not following mplanner goals
	* Reduce the size of obstacle when backup or make a uturn
	* Replan from current position when we switch from STOP_INT to DRIVE
	* Remember the last intersection so we don't switch to STOP_INT again
	* Fixed backup_lp_state
	* Added two more states: STOPOBS_AGG and STOPOBS_BARE so instead of
	transitioning to DRIVE_AGG or DRIVE_BARE right away, we switch to
	STOPOBS_AGG or STOPOBS_BARE first to prevent us from getting closer
	to the obstacle.

Release R1-03f (Thu Oct 11  0:35:35 2007):
  Switched to new graph structure.

Release R1-03e (Mon Oct  8 18:12:02 2007):
	Also handle PP_NONODEFOUND

Release R1-03d (Mon Oct  8 13:45:00 2007):
	* in STOP_OBS: replan before go into backup
	* intersection handling: fixed bug so we do not give precedence when Alice is on a
	right-of-way lane at a ROW intersection

Release R1-03c (Sun Oct  7 19:03:35 2007):
	* Added two more states: DRIVE_AGGRESSIVE and DRIVE_BARE. 
	* Handle S1PLANNER_FAILED. Send replan if path-planner returns 
	  PP_NOPATH_LEN or PP_NOPATH_COST
	* Make sure that we don't switch out of s1planner too quickly. Even
	when s1planner fails to find a path, we'll wait in that state for 3
	seconds. (This is specified in logicplanenr.conf, so you can change
	this number if you want.)
	* Make sure that failure handling actually changes the state. For
	example, if the flag is already PASS, and currentCounter is also
	PASS, increment currentCounter.

Release R1-03b (Sat Oct  6 16:58:14 2007):
	* Release memory properly in getTransitionAlongGraph
	* If there are any obstacles nearby while merging, wait longer.
	* Changed back stopping_distance at intersections to 1.0m
	* Limit the distance we can drive with s1planner.
	* Fixed bug where we keep switching between backing up and using s1planner.
	* Added more output to log
	* Reset backup_lp_state to DRIVE_PASS_BACKUP when the user switches off FH.
	* reverted back to the 2m - limit while stopping at intersections as the real bug couldnt be tracked 
down
	* spelling mistake
	* Clean up the FSM. Removed the useless transition from DRIVE_PASS_BACKUP to UTURN. (Switch to 
STOPOBS_PASS_BACKUP first.)
	* Removed the cycle: BACKUP -> S1PLANNER_SAFETY -> S1PLANNER_AGG -> S1PLANNER_BARE -> BACKUP
Release R1-03a (Fri Oct  5  8:33:09 2007):
	Changes from the field test in El Toro, including the fix for the segfault while merging at intersections

Release R1-03 (Wed Oct  3 18:37:08 2007):
	In failure handler, can change obstacle size 2 times.

Release R1-02z (Wed Oct  3 16:43:26 2007):
	Outputs FailureHandler index to Console

Release R1-02y (Wed Oct  3 15:39:40 2007):
	Reduced the amount of time before triggering failure handling.
	Fixed bug where we don't switch out of backup when failure
	handling is enabled.

Release R1-02x (Wed Oct  3  3:33:24 2007):
	Parameters for logic planner are now specified in the config file
	logicplanner.conf so we don't have to recompile logic planner and
	relink planner every time we want to change a parameter.

Release R1-02w (Tue Oct  2 22:59:51 2007):
	* Restructured the logic planner so it has internal state instead of having to set 5 different
	types of states each time. With failure handling, it currently has 16 internal states (for different
	combinations of region, FSM state, flag, obstacle separation, planner).
	* Use s1planner error. I still see s1planner plans a path through an obstacle without returning
	an error.
	* Failure handling is now working. But the cmdline option to enable it somehow disappears.
	To enable failure handling (by this, I mean start using s1planner in ROAD_REGION and try
	playing around with obstacle separation), hit 'f' on the planner display.

Release R1-02v (Tue Oct  2 15:18:29 2007):
	removed unnecessary debug information

Release R1-02u (Tue Oct  2 15:12:00 2007):
	* fixed bug in merging procedure to fix core dump from night simulations

Release R1-02t (Tue Oct  2  9:25:24 2007):
	* adjusted to AliceStateHelper function (which uses site coordinates now)
	* merging: better evaluation of intersecting traffic
	* merging: takes into account unfeasible turns and checks more lanes if necessary

Release R1-02s (Mon Oct  1  9:08:16 2007):
	Do not stop using s1planner if we're in a zone.

Release R1-02r (Sun Sep 30 14:41:56 2007):
	Fixed the logic so instead of failing to mplanner after backing up
	more than 3 times, it switches to s1planner first.

Release R1-02q (Sun Sep 30  3:12:06 2007):
	* A simple version of fault handling. Only switch to
	S1PLANNER/CIRCLE_PLANNER/DPLANNER (depending on cmdline option) and
	change the distance from obstacle before making a u-turn. S1PLANNER
	still plans right through the obstacle but at least it should be
	able to go through the narrow one lane road at El Toro.

Release R1-02p (Sat Sep 29 23:22:52 2007):
	Make planLogic more robust to noise (obstacle flickering in and
	out, time_stop gets reset, etc). If we need to go back and forth
	more than 3 times, we'll transition to UTURN. Also, do not
	transition from STOP_OBS to DRIVE right when we see obstacle
	disappears. Make sure that it has disappeared for at least 1.5 sec
	before switching to DRIVE so we don't keep switching between
	STOP_OBS and DRIVE when obstacle is flickering in and out. All these
	only work when failure handling is OFF.


Release R1-02o (Sat Sep 29 17:57:01 2007):
	Incrementing failure count when they occur.  Querying the failure toggle from the 
console before calling failureHandler method. 

Release R1-02n (Sat Sep 29 13:19:48 2007):
	Fixed bug where we kept backing up after STOP_OBS when current_flag
	is PASS.

Release R1-02m (Sat Sep 29 10:56:46 2007):
	* planLogic also determines whether the lane is blocked or 
	the road is blocked.
	* Wait for 4 seconds before switching to UTURN and 1 sec before 
	switching to PASS to give sensing enough time to filter out 
	groundstrikes.
	* If we have not stopped long enough, we'll transition to STOP_OBS,
	PASS before transitioning to UTURN. Added a transition STOP_OBS ->
	UTURN.
	* Returns the current lane we're in (so mplanner knows
	what planner thinks we're in case we get out of sync).


Release R1-02l (Fri Sep 28 16:05:03 2007):
	fault handler does not start interacting while Alice is in Estop Pause/Disabled

Release R1-02k (Fri Sep 28 15:49:09 2007):
	Fixed a bug that was starting logic planner init state with s1planner.  
Release R1-02j (Fri Sep 28 12:53:09 2007):
	Fixed a segfault in zones that was caused by new implementation of getCurrentLane.  This function returns graph 
nodes at 1m from Alice and these do not always exist when in a zone.  Changed LogicPlanner to check if we are in a zone 
before calling this method.  
	Fixed issues with changing planners in zones.    

Release R1-02i (Fri Sep 28 10:31:08 2007):
	For reasons of development the different zone planners: within fault handling, no change of 
	planners while being in zones

Release R1-02h (Fri Sep 28  9:20:48 2007):
	Fault handling distinguishes between different regions (ROAD, ZONE, INTERSECTION) properly. This should fix the bug that it switches into 
	RAIL_PLANNER whie being in zones.

Release R1-02g (Thu Sep 27 10:17:05 2007):
	Using new Utils::getCurrentLane to improve performances (gained 200 ms while in DRIVE in victorville).

Release R1-02f (Wed Sep 26 21:52:47 2007):
	Fixed bug in intersection handling so that it does not check for precedence infinitly.

	Then, added first implementation of fault handling.

Release R1-02e (Wed Sep 26 10:42:15 2007):
	Propagating changes in temp-planner-interfaces

Release R1-02d (Wed Sep 26  8:51:47 2007):
	Uncommented a condition that was used for testing but deactivated some uncertainity at intersections

Release R1-02c (Tue Sep 25 11:21:12 2007):
	Other vehicle's velocity is restricted to 15m/s in case that sensing returns some unbelievable high values.

Release R1-02b (Sun Sep 23 15:18:22 2007):
	Now sets the zone planner to be used based on the cmd args 
given.

Release R1-02a (Fri Sep 21 17:53:53 2007):
	Increased the size of the corridor that is checked for static 
obstacles and increased the time that intersection will be checked for 
clearance.

Release R1-02 (Fri Sep 21 16:37:44 2007):
	Implemented the new fields in the state problem. No new functionality.

Release R1-01z (Tue Sep 18 19:01:05 2007):
	Outputs information about other vehicles into console while merging.
	Decrease the time waiting for clearance at intersection from 3 to 2 seconds

Release R1-01y (Mon Sep 17 20:57:32 2007):
	Added INTERSECTION_TIMEOUT (default 45 seconds). If Alice waits longer than this value at
	the intersection, it tries to go through; no matter whether other vehicles might have 
	precedence.

Release R1-01x (Thu Sep 13 17:57:03 2007):
	Do not check for precedence in Alice lane. Also, using 
isObstacle/isVehicle functions now

Release R1-01w (Tue Sep 11 21:42:03 2007):
	Alice waits at intersections for 2s to verify that intersection is clear and no moving obstacles are blocking the intersection.
	
Release R1-01v (Tue Sep 11 13:47:55 2007):
	Small bug in transition, only affected output in mapviewer

Release R1-01u (Tue Sep 11 13:11:57 2007):
	Now adds information to console about obstacles that affect Intersection handling

Release R1-01t (Mon Sep 10 12:33:31 2007):
 	Changed condition for entering into a zone by simply checking that Alice positions wrt the rear axle 
is inside the zone. 

Release R1-01s (Mon Sep 10  9:24:54 2007):
	Fixed bugs in IntersectionHandling.

Release R1-01r (Tue Sep  4  1:17:52 2007):
	Makes sure that entire Alice is in zone before switching to ZONE. 

Release R1-01q (Sun Sep  2 11:49:30 2007):
	Removed Alice safety box
	Added a transition from STOP_INT to DRIVE

Release R1-01p (Sun Sep  2 11:13:52 2007):
	Fixed bug so that IntersectionHandling won't use the SegGoals from the SegGoalsQueue if they are messed 
up

Release R1-01o (Fri Aug 31 17:11:45 2007):
	Added condition to not double check the same intersection

Release R1-01n (Thu Aug 30 18:13:07 2007):
	Reduced the safety box around Alice a little.

Release R1-01m (Thu Aug 30 17:23:21 2007):
	Updates the distance to the next stopline in the planner 
	console. This will help debugging if Alice does not stop at a 
	stopline or does not want to go through

Release R1-01l (Thu Aug 30 10:57:07 2007):
	Removed mapviewer output created by unittest.

Release R1-01k (Wed Aug 29 22:07:57 2007):
	Changed Unittest to test intersections in different RNDFs, 
	especially the algorithm to check for clearance

Release R1-01j (Tue Aug 28 17:14:35 2007):
	Added safety feature that brings Alice to a stop when there is an obstacle in a safety region around Alice.

Release R1-01i (Tue Aug 28 19:00:09 2007):
	Changed interface to getDistToStopline again.

Release R1-01h (Tue Aug 28 13:59:23 2007):
	Dont know why yam wants this release...

Release R1-01g (Tue Aug 28  9:25:17 2007):
	1.) IntersectionHandling works with Zones.
	2.) IntersectionHandling waits for a defined number of cycles until it deletes obstacles out of its internal 
	precedence list. The number of cycles are defined in WAIT_CYCLES in IntersectionHandling.conf
	This should improve dyn. obstacle handling when obstacles are flickering in the map

Release R1-01f (Mon Aug 27  9:00:51 2007):
	Moved the LogicUtils methods into a file in temp-planner-interfaces so they are accessible by the planning stack.  
Release R1-01e (Thu Aug 23 17:06:48 2007):
	Fixed bug so planner does not switch into STOP_INT right after passing the 
	stopline of an intersection and gets stuck.

Release R1-01d (Thu Aug 23 15:56:51 2007):
	Changed the way how to stop at stoplines at to go through. Now, it pulls the 
	location of stoplines from the graph. Some debugging is still necessary. At least, 
	others can continue working on zones now.

Release R1-01c (Wed Aug 22 18:51:46 2007):
	Removed output in mapviewer while checking for visibility.
	Fixed bug in getDistToStop so we also get negative distances to stoplines.
	Fixed bug when switching into STOP_INT at left turns of ROW intersections.

Release R1-01b (Wed Aug 22 17:55:16 2007):
	Stopping when first entrering a zone

Release R1-01a (Wed Aug 22 15:30:40 2007):
	Increased threshold for obstacles decected as being stopped at intersections.

Release R1-01 (Tue Aug 21 19:13:03 2007):
	Fixed SegFault at intersections. Also, IntersectionHandling will 
	be only called if distance to stopline <1m

Release R1-00z (Tue Aug 21  6:18:18 2007):
	Changed debugging output channel from 0 which is reserved for
	perceptors to -2 which is the standard debugging channel

Release R1-00y (Mon Aug 20 21:43:04 2007):
	Replaced getHeading/getWaypoint functions with graph functions. Increased the zone where vehicles will be detected having precedence.

	Added functionality to corridor check. First the whole intersection will be checked for obstacles. Obstacles that did not move for more than 10 seconds and do not
	block the corridor that Alice wants to got, are neglected. If this kind of obstacle is blocking Alice's corridor, the function returns CABMODE after 10 seconds (if CABMODE_FLAG is set). 
	Moving obstacles will always be tracked no matter where they are within the intersection.

Release R1-00x (Fri Aug 17 18:47:20 2007):
	Changed condition for which we determine that we are in a Zone by checking if the position of 
our rear axle is inside the polygon delimited by the zone perimeter.  

Release R1-00w (Thu Aug 16 20:07:32 2007):
	Enabled zones

Release R1-00v (Wed Aug 15 17:53:41 2007):
	Fixed intersection bug
	Forcing Alice to stop to perform a Uturn

Release R1-00u (Tue Aug 14 19:27:39 2007):
	Brighten up colors at intersections in mapviewer to increase readability

Release R1-00t (Tue Aug 14 10:55:01 2007):
	Fixed bug causing Alice to freeze at intersections

Release R1-00s (Tue Aug 14 12:10:15 2007):
	Updated makefile to reflect changes in temp-planner-interfaces	

Release R1-00r (Tue Aug 14  9:24:15 2007):
	The new console allows to reset the current FSM State and to overwrite manually the intersection handling. The keyboard commands that can be
	used for that are shown within the () in the Console

Release R1-00q (Thu Aug  9 12:50:41 2007):
	UTURN is now divided in BACKUP/UTURN. The car will first backup then check for a possible passing maneuver, if no passing maneuver can be 
made, it will UTURN.

Release R1-00p (Thu Aug  9 11:31:32 2007):
	Specific mapviewer output from intersection handling will be output now on the regular mapviewer (channel 0). Also removed a bug that 
	prevented others from compiling it correctly (hehe).

Release R1-00o (Wed Aug  8 17:26:12 2007):
	Correct bug in integration of left turns at ROW intersections.

Release R1-00n (Wed Aug  8 11:25:47 2007):
	Integrated handling of ROF intersections in LogicPlanner. Implementation in IntersectionHandling not done yet.

Release R1-00m (Fri Aug  3 20:22:05 2007):
	Minor fix (when path cannot be found because of high cost)

Release R1-00l (Fri Aug  3 18:37:03 2007):
	Added sleep(1) after intersection clearance and modified the IntersectionHandling.conf.
	Cars that are within 10m of their stoplines will be given precedence.

Release R1-00k (Fri Aug  3  8:40:17 2007):
	fixed bug in Map output

Release R1-00j (Fri Aug  3  0:40:08 2007):
	Fixed UTURN bug

Release R1-00i (Tue Jul 31 19:40:41 2007):
	Added logic to report errors to mplanner
        Knows when UTurn gets completed

Release R1-00h (Tue Jul 31 13:36:07 2007):
	Fixed bug in IntersectionHandling. The configuration file IntersectionHandling.conf will be found now.
	Changed function populateWaypoints so that the module Prediction also has access to it. 

Release R1-00g (Fri Jul 27  9:35:24 2007):
	Added logic to handle passing maneuver and UTurns

Release R1-00f (Mon Jul 23 19:25:01 2007):
	Added some logic to handle obstacles appearing before intersections

Release R1-00e (Thu Jul 19 18:40:16 2007):
	Handling intersection, pause and end of mission now

Release R1-00d (Wed Jul 18 19:03:58 2007):
	Fixed path-planner error handling

Release R1-00c (Wed Jul 18 16:34:03 2007):
	Added intersection output to the console

Release R1-00b (Wed Jul 18 12:30:14 2007):
	Added basic logic to handle everything we had at the sitevisit (except UTURN), this assume that PathPlanner returns a PP_COLLISION error if 
an obstacle is blocking the lane

Release R1-00a (Fri Jul 13 16:10:26 2007):
	Added basic functionality in order to start coding

Release R1-00 (Thu Jul 12 17:02:33 2007):
	Created.























































































































