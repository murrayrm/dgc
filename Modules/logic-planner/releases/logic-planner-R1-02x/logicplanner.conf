// Initialize class constants
DESIRED_DECELERATION = 0.25

// Parameters for defining intersection region
INTERSECTION_BUFFER = 2
MAX_INTERSECTION_Q_LENGTH = 30

// Safety box around Alice
ALICE_SAFETY_FRONT = 1.0
ALICE_SAFETY_REAR = 0.5
ALICE_SAFETY_SIDE = 0.5

// The number of times we can backup before switching into uturn
// This is only used when failure handling is not activated.
MAX_NUM_BACKUP = 3

// The number of times we can backup before start using s1planner in ROAD_REGION
// This is only used when failure handling is activated.
MAX_NUM_S1PLANNER_BACKUP = 2

// Parameter that defines how far different backup can be
MIN_DIST_BETWEEN_DIFF_BACKUP = 30.0

// How long we need to stop before starting passing maneuver
TIMEOUT_OBS_PASSING = 3.0

// How long we need to stop before starting passing maneuver near intersection
TIMEOUT_OBS_INT = 20.0

// How long we need to stop before starting uturn maneuver
MIN_TIME_STOP_BEFORE_UTURN = 7.0

// How long obstacle has to disappear before we think that it doesn't exist.
// This is to prevent logic planner from switching back and forth
// before DRIVE and STOP_OBS too quickly when obstacle is flickering in and out.
MIN_TIME_OBS_DISAPPEAR = 1.5

// How long we need to stop before starting using s1planner in ROAD_REGION
MIN_TIME_STOP_BEFORE_S1PLANNER = 5.0

// How long we will use s1planner in ROAD_REGION before switching back to rail planner
MAX_TIME_USE_S1PLANNER = 20.0

// How long we can be in non-nominal mode
FH_TIMEOUT_NONNOMINAL = 10.0

// Timeout before failure handling starts doing something
FH_MAX_STOP_TIME = 12.0
