
#include <sstream>
#include <iomanip>
#include <ncurses.h>
#include <cotk/cotk.h>

#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Log.hh"
#include "temp-planner-interfaces/Console.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include "alice/AliceConstants.h"
#include "dgcutils/cfgfile.h"

#include "ConfigFile.hh"
#include "IntersectionHandling.hh"
#include "temp-planner-interfaces/Utils.hh"

// #include "planner/cmdline.h"
using namespace std;

CIntersectionHandling::CIntersectionHandling()
{
  // Output statistic information
  time_t currentTime;
  time(&currentTime);
  logIntersection = new CLog(1);
  string filename = CmdArgs::log_filename;

  filename.replace(filename.find("planner"),7,"intersectionHandler");
  logIntersection->setGenericLogFile(filename.c_str());
  logIntersection->getStream(1)<<"% Starting up Intersection Handler at "<<ctime(&currentTime)<<endl;
 
  char *path;
  
  path=dgcFindConfigFile("IntersectionHandling.conf","logic-planner");
  if (fopen(path,"r") == NULL) {
    Console::addMessage("Warning: Couldn't read Intersection configuration.");
    Log::getStream(1)<<"Warning: Couldn't read Intersection configuration."<<endl;
    Log::getStream(1)<<fopen("IntersectionHandling.conf","r")<<endl;
    
    DISTANCE_STOPLINE_ALICE=1;
    VELOCITY_OBSTACLE_THRESHOLD=0.2;
    DISTANCE_OBSTACLE_THRESHOLD=6;
    ETA_EPS=1;
    TRACKING_VEHICLE_SEPERATION=1.0;
    FLICKER_FILTER_THRESHOLD=0.2;
    INTERSECTION_TIMEOUT=45.0;
    INTERSECTION_TIME_CLEARANCE = 3.0;
    INTERSECTION_MERGING_WAIT_LONGER = 6.0;
    INTERSECTION_GROW_CORRIDOR = 2.0;
    MAXIMUM_VELOCITY = 15.0;
    INTERSECTION_MERGING_LANE_OFFSET = 1.0;
  }
  else {
    ConfigFile config(path);
 
    config.readInto(DISTANCE_STOPLINE_ALICE,"DISTANCE_STOPLINE_ALICE");
    config.readInto(VELOCITY_OBSTACLE_THRESHOLD,"VELOCITY_OBSTACLE_THRESHOLD");
    config.readInto(DISTANCE_OBSTACLE_THRESHOLD,"DISTANCE_OBSTACLE_THRESHOLD");
    config.readInto(ETA_EPS,"ETA_EPS");
    config.readInto(TRACKING_VEHICLE_SEPERATION,"TRACKING_VEHICLE_SEPERATION");
    config.readInto(FLICKER_FILTER_THRESHOLD,"FLICKER_FILTER_THRESHOLD");
    config.readInto(INTERSECTION_TIMEOUT,"INTERSECTION_TIMEOUT");
    config.readInto(INTERSECTION_TIME_CLEARANCE,"INTERSECTION_TIME_CLEARANCE");
    config.readInto(INTERSECTION_MERGING_WAIT_LONGER,"INTERSECTION_MERGING_WAIT_LONGER");
    config.readInto(INTERSECTION_GROW_CORRIDOR,"INTERSECTION_GROW_CORRIDOR");
    config.readInto(MAXIMUM_VELOCITY,"MAXIMUM_VELOCITY");
    config.readInto(INTERSECTION_MERGING_LANE_OFFSET,"INTERSECTION_MERGING_LANE_OFFSET");
    Log::getStream(1)<<"Info: Read Intersection configuration successfully."<<endl;
  }

  logIntersection->getStream(1)<<"% INTERSECTION_TIMEOUT: "<<INTERSECTION_TIMEOUT<<endl;
  logIntersection->getStream(1)<<"% INTERSECTION_TIME_CLEARANCE: "<<INTERSECTION_TIME_CLEARANCE<<endl;
  logIntersection->getStream(1)<<"% INTERSECTION_MERGING_WAIT_LONGER: "<<INTERSECTION_MERGING_WAIT_LONGER<<endl;
}

CIntersectionHandling::~CIntersectionHandling()
{
}

void CIntersectionHandling::reset(PlanGraph* g, Map* m)
{
  // output log information
  time_t currentTime;
  time(&currentTime);
  logIntersection->getStream(1)<<endl<<"% Reset intersection at "<<ctime(&currentTime)<<endl;

  // assign map and graph
  graph = g;
  localMap = m;

  // This resets the intersection, so the next time, a new intersection will be created
  WayPointsEntries.clear();
  WayPointsWithStop.clear();
  WayPointsNoStop.clear();
  
  precedenceList.clear();
  mergingList.clear();
  clearanceList.clear();
  clearanceLeftBound.clear();
  clearanceRightBound.clear();

  JammedTimerStarted=false;

  // bool variables for first checking different in states
  firstCheckIntersection=false;
  firstCheckPrecedence=false;
  firstCheckClearance=false;

  ReturnValue = RESET;
  
  logPrecedenceCounter = false;
  logLostVisibilityCounter = 0;
  logLostIdCounter = 0;
  logNearbyObstacleCounter = 0;
  logUnfeasibleTurnCounter = 0;
  switchMergingClearCounter = 0;
  clearset = false;

  // clean up maviewer
  MapElement me;
  for (int i = MAPSTART; i<mapcounter; i++)
    Utils::displayClear(SENDCHANNEL, 2, i);

  mapcounter = MAPSTART;


  updateConsole();
}

void CIntersectionHandling::updateConsole()
{
  // Update Console

  int counter=0;
  for (unsigned int i=0; i<precedenceList.size(); i++)
      if (precedenceList[i].precedence)
        counter++;

  string str="";

  switch(ReturnValue)
    {
    case RESET:           str="RESET          "; break;
    case WAIT_PRECEDENCE: str="WAIT_PRECEDENCE"; break;
    case WAIT_MERGING:    str="WAIT_MERGING   "; break;
    case WAIT_CLEAR:      str="WAIT_CLEAR     "; break;
    case GO:              str="GO             "; break;
    case JAMMED:          str="JAMMED         "; break;
    }
  
  unsigned long long t;
  DGCgettime(t);

  Console::updateInter(counter,str,entryWaypoint,IntersectionTimer);
  Console::updateInterPage2(precedenceList, clearanceList, mergingList);
}

CIntersectionHandling::IntersectionReturn CIntersectionHandling::checkIntersection(VehicleState vehState, SegGoals currSegment)
{
  if (ReturnValue == GO)
    return ReturnValue;

  // save previous ReturnValue
  IntersectionReturn previousReturnValue;
  previousReturnValue = ReturnValue;

  // start Intersection Timer
  if (!firstCheckIntersection) {
    DGCgettime(IntersectionStartTime);
    firstCheckIntersection = true;
  }

  // if Alice waits longer than the timeout value, give GO command
  unsigned long long t;
  DGCgettime(t);
  IntersectionTimer = (t - IntersectionStartTime)/1000000.0;

  if (IntersectionTimer>INTERSECTION_TIMEOUT)
    ReturnValue = GO;

  entryWaypoint = PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);

  // Create intersection if it didn't happen yet
  if (WayPointsEntries.size()==0) {
    // output some log information first
    time_t t;
    time(&t);
    Log::getStream(1)<<"Check Intersection starting with "<<entryWaypoint<<". Time "<<ctime(&t)<<endl;
    logIntersection->getStream(1)<<"% Check Intersection starting with "<<entryWaypoint<<". Time "<<ctime(&t)<<endl;
  
    populateWayPoints(entryWaypoint);
    findStoplines(entryWaypoint);
    detectIntersectingTrafficWhileMerging(currSegment);
  }
  // If intersection created successfully
  if (WayPointsEntries.size()>0) {
    // if Alice is at ROW intersection and have no stopline, switch into MERGE right away
    if (ReturnValue == RESET) {
      if (localMap->isStopLine(entryWaypoint)) {
        logIntersection->getStream(1)<<"Stopline ;TRUE"<<endl;
        ReturnValue=WAIT_PRECEDENCE;
      }
      else {
        logIntersection->getStream(1)<<"Stopline ;FALSE"<<endl;
        ReturnValue=WAIT_MERGING;
      }
    }
    else if (ReturnValue == WAIT_PRECEDENCE) {
      // Check for current status of other obstacles.
      maintainPrecedenceList(vehState);
      ReturnValue=checkPrecedence();
      if (Console::queryResetIntersectionFlag())
        ReturnValue=WAIT_MERGING;
    }
    else if (ReturnValue==WAIT_MERGING || ReturnValue==WAIT_CLEAR) {
      ReturnValue = checkMerging(currSegment);

      if (previousReturnValue == WAIT_CLEAR && ReturnValue == WAIT_CLEAR)
        goto labelCheckClearance;

      if (ReturnValue!=WAIT_CLEAR && Console::queryResetIntersectionFlag())
        ReturnValue=WAIT_CLEAR;
    }
    else if (ReturnValue==WAIT_CLEAR) {
    labelCheckClearance:
      ReturnValue=checkClearance(currSegment);
      if (Console::queryResetIntersectionFlag()) {
        ReturnValue=GO;
      }
    }

    // While waiting for possibility to merge, do not start timer yet!
    if (ReturnValue==WAIT_MERGING)
      JammedTimerStarted=false;
  }
  else {
    // Output some debugging information
    Log::getStream(7)<<"Error: Couldn't build Intersection. Give GO. WayPoint "<<PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID)<<endl;
    
    stringstream s;
    s<<entryWaypoint;
    Console::addMessage("Error: Couldn't build Intersection. %s",s.str().c_str());
    
    ReturnValue=GO;
  }
    string str;
    switch(ReturnValue)
      {
      case RESET:           str="RESET"; break;
      case WAIT_PRECEDENCE: str="WAIT_PRECEDENCE"; break;
      case WAIT_MERGING:    str="WAIT_MERGING"; break;
      case WAIT_CLEAR:      str="WAIT_CLEAR"; break;
      case GO:              str="GO"; break;
      case JAMMED:          str="JAMMED"; break;
      }
    
  outputLog(previousReturnValue, ReturnValue);
  
  return ReturnValue;
}

void CIntersectionHandling::outputLog(IntersectionReturn previousReturnValue, IntersectionReturn ReturnValue)
{
  static bool precedenceTimerStarted = false;
  static bool clearanceTimerStarted = false;
  static unsigned long long precedenceTimer, mergingTimer, clearanceTimer;
  static double precedenceTime, mergingTime, clearanceTime;
  unsigned long long currentTime;

  DGCgettime(currentTime);
  
  if (previousReturnValue == RESET) {
    precedenceTimerStarted = false;
    clearanceTimerStarted = false;
    precedenceTime = 0;
    mergingTime = 0;
    clearanceTime = 0;
  }

  if (previousReturnValue == RESET && ReturnValue == WAIT_PRECEDENCE) {
    precedenceTimerStarted = true;
    DGCgettime(precedenceTimer);
  }
  else if ((previousReturnValue == WAIT_PRECEDENCE && ReturnValue == WAIT_MERGING) || (previousReturnValue == WAIT_CLEAR && ReturnValue == WAIT_MERGING)) {
    if (precedenceTimerStarted) {
      precedenceTime = (currentTime - precedenceTimer) / 1000000.0;
      precedenceTimerStarted = false;
    }
    if (clearanceTimerStarted) {
      clearanceTime += (currentTime - clearanceTimer) / 1000000.0;
      clearanceTimerStarted = false;
    }

    if (previousReturnValue == WAIT_CLEAR)
      switchMergingClearCounter++;

    DGCgettime(mergingTimer);
  }
  else if (previousReturnValue == WAIT_MERGING && ReturnValue == WAIT_CLEAR) {
    mergingTime += (currentTime - mergingTimer) / 1000000.0;
    clearanceTimerStarted = true;
    DGCgettime(clearanceTimer);
  }
  else if (previousReturnValue == WAIT_CLEAR && (ReturnValue == GO || ReturnValue == JAMMED)) {
      clearanceTime += (currentTime - clearanceTimer) / 1000000.0;
  }

  if (IntersectionTimer > INTERSECTION_TIMEOUT) {
    string str;
    switch(previousReturnValue)
      {
      case RESET:           str="RESET"; break;
      case WAIT_PRECEDENCE: str="WAIT_PRECEDENCE"; break;
      case WAIT_MERGING:    str="WAIT_MERGING"; break;
      case WAIT_CLEAR:      str="WAIT_CLEAR"; break;
      case GO:              str="GO"; break;
      case JAMMED:          str="JAMMED"; break;
      }
    
    logIntersection->getStream(1)<<"Timeout ; "<<str<<endl;
  }

  if (previousReturnValue == RESET && ReturnValue == GO)
    logIntersection->getStream(1)<<"Error ; buliding intersection"<<endl;

  if (ReturnValue == GO || ReturnValue == JAMMED) {
    logIntersection->getStream(1)<<"Lost Visibility ; "<<logLostVisibilityCounter<<endl;
    logIntersection->getStream(1)<<"Lost Id ; "<<logLostIdCounter<<endl;
    logIntersection->getStream(1)<<"Replaced by nearby obstacle ; "<<logNearbyObstacleCounter<<endl;
    logIntersection->getStream(1)<<"Switch Merging/Clearance ; "<<switchMergingClearCounter<<endl;
    logIntersection->getStream(1)<<"Precedence time: ;"<<precedenceTime<<endl;
    logIntersection->getStream(1)<<"Merging time: ;"<<mergingTime<<endl;
    logIntersection->getStream(1)<<"Clearance time: ;"<<clearanceTime<<endl;
    logIntersection->getStream(1)<<"Total time: ;"<<(currentTime - IntersectionStartTime)/1000000.0<<endl;
  }
}

void CIntersectionHandling::populateWayPoints(PointLabel InitialWayPointEntry)
{
  vector<PointLabel> WayPointExits, WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++) {
    localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

    // Look whether WayPointEntry already exists in list
    for (unsigned int j=0; j<WayPoint.size(); j++) {
      bool found=false;
      for (unsigned int k=0; k<WayPointsEntries.size(); k++) {
        if (WayPoint[j]==WayPointsEntries[k]) {
          found=true;
          break;
        }
      }
      
      // If not, add it to list and call function recursivly
      if (!found) {
        WayPointsEntries.push_back(WayPoint[j]);

        populateWayPoints(WayPoint[j]);
      }
    }
  }
}

void CIntersectionHandling::detectIntersectingTrafficWhileMerging(SegGoals currSegment)
{
  if (WayPointsNoStop.size() ==0)
    return;

  // create Alice's graph
  point2arr transitionAlice, transitionTemp;
  getTransition(transitionAlice, PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID), PointLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID));

  localMap->extendLine(transitionAlice, transitionTemp, 5.0);
 
  // go through all entries found at this intersection
  for(unsigned int i=0; i<WayPointsNoStop.size(); i++) {
    vector<PointLabel> WayPointExits;
    localMap->getWayPointExits(WayPointExits,WayPointsNoStop[i].WayPoint);

    // loop through all exits for this entry and see whether possible transitions intersect with Alice
    bool intersect = false;
    for (unsigned int k=0; k<WayPointExits.size(); k++) {
      point2arr transitionTraffic;
      getTransition(transitionTraffic, WayPointsNoStop[i].WayPoint, WayPointExits[k]);

      point2 crosspoint;

      if (transitionAlice.is_intersect(transitionTraffic)) {
        intersect = true;
        break;
      }
    }
    WayPointsNoStop[i].monitorWhileMerging = intersect;
  }

  // a special case is when Alice turns right. If it tries to follow an unfeasible turn,
  // it should also monitor traffic coming from the right to prevent running into its lane while turning right
  if (currSegment.intersection_type == SegGoals::INTERSECTION_RIGHT) {
    // 
    vector<LaneLabel> oppLanes;
    localMap->getOppDirLanes(oppLanes, LaneLabel(currSegment.exitSegmentID, currSegment.exitLaneID));

    // check whether opposite lane has no stop line
    bool foundindex = -1;
    if (oppLanes.size()>0) {
      for (unsigned int l=0; l<oppLanes.size(); l++) {
        for (unsigned int k=0; k<WayPointsNoStop.size(); k++) {
          if (WayPointsNoStop[k].WayPoint.segment == oppLanes[l].segment && WayPointsNoStop[k].WayPoint.lane == oppLanes[l].lane) {
            foundindex=k;
            break;
          }
        }
      }
    }

    // if opposite has no stopline, check whether graph is feasible
    if (foundindex != -1) {
      bool feasible = false;

      if (checkFeasibility(feasible, PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID), PointLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID)) == -1 || !feasible) {
          WayPointsNoStop[foundindex].monitorWhileMerging = true;
          logIntersection->getStream(1)<<"Add monitoring ; "<< WayPointsNoStop[foundindex].WayPoint<<endl;
          logUnfeasibleTurnCounter++;
      }
    }
  }

  return;
}


int CIntersectionHandling::checkFeasibility(bool result, PointLabel entryPointLabel, PointLabel exitPointLabel)
{
  point2arr transition, entryBound, exitBound;
  point2 p_entry, p_exit, projectEntry, projectExit;

  // get point2 from graph
  PlanGraphNode* entry = graph -> getWaypoint(entryPointLabel.segment, entryPointLabel.lane, entryPointLabel.point);
  PlanGraphNode* exit = graph -> getWaypoint(exitPointLabel.segment, exitPointLabel.lane, exitPointLabel.point);
  p_entry = point2(entry->pose.pos.x, entry->pose.pos.y);
  p_exit = point2(exit->pose.pos.x, exit->pose.pos.y);

  // create graph transition from entry to exit
  getTransition(transition, entryPointLabel, exitPointLabel);

  // obtain right projection point onto boundary from entry and exit
  localMap->getRightBound(entryBound,LaneLabel(entryPointLabel.segment, entryPointLabel.lane));
  localMap->getRightBound(exitBound,LaneLabel(exitPointLabel.segment, exitPointLabel.lane));

  localMap->getProjectToLine(projectEntry, entryBound, p_entry);
  localMap->getProjectToLine(projectExit, exitBound, p_exit);

  // create point2arr, extend it and get crosspoint
  point2arr tempEntryLine, tempExitLine, entryLine, exitLine;

  tempEntryLine.push_back(p_entry);
  tempEntryLine.push_back(projectEntry);
  localMap->extendLine(entryLine, tempEntryLine, 50);

  tempExitLine.push_back(p_exit);
  tempExitLine.push_back(projectExit);
  localMap->extendLine(exitLine, tempExitLine, 50);

  // if false, there is somethingw wrong
  point2 cpoint;
  if (!crossPoint(entryLine, exitLine, cpoint))
    return -1;
 
  // go along transition and check whether radius is always larger than ALICE_MIN_RADIUS
  for (unsigned int i=0; i<transition.size(); i++) {
    double distance = sqrt(pow(transition[i].x - cpoint.x, 2) + pow(transition[i].y - cpoint.y, 2));
    if (distance < VEHICLE_MIN_TURNING_RADIUS) {
      result = false;
      return 0;
    }
  }
         
  result = true;
  return 0;
}

void CIntersectionHandling::findStoplines(PointLabel InitialWayPointEntry)
{
  MapElement me;
  point2 p;
  stringstream s;
  
  for (unsigned int i=0; i<WayPointsEntries.size(); i++) {

    // Do not check Alice lane
    if (WayPointsEntries[i] == InitialWayPointEntry)
      continue;

    if (localMap->isStopLine(WayPointsEntries[i])) {
      // Store information in vector
      WayPointsWithStop.push_back(WayPointsEntries[i]);
      
      // Send stopline to map, color red
      PlanGraphNode* node = graph -> getWaypoint(WayPointsEntries[i].segment, WayPointsEntries[i].lane, WayPointsEntries[i].point);
      point2 p = point2(node->pose.pos.x, node->pose.pos.y);

      // send area that will be checked for precedence
      LaneLabel lane = LaneLabel(WayPointsEntries[i].segment, WayPointsEntries[i].lane);
        
      // get centerline for this waypoint
      point2arr centerline;
        
      point2 p1_l, p1_r;
      localMap->getLaneLeftPoint(p1_l, lane, p, 0);
      localMap->getLaneRightPoint(p1_r, lane, p, 0);
      
      point2 p2_l, p2_r;
      localMap->getLaneLeftPoint(p2_l, lane, p,-DISTANCE_OBSTACLE_THRESHOLD);
      localMap->getLaneRightPoint(p2_r, lane, p,-DISTANCE_OBSTACLE_THRESHOLD);
      
      point2arr parr;
      parr.push_back(p1_l);
      parr.push_back(p1_r);
      parr.push_back(p2_l);
      parr.push_back(p2_r);
      
      // paint box
      Utils::displayLine(SENDCHANNEL, parr, 2, MAP_COLOR_ORANGE, mapcounter++);
    }
    else {
      // Store information in vector
      WayPoints_t temp;
      temp.WayPoint = WayPointsEntries[i];
      temp.monitorWhileMerging = false;
      WayPointsNoStop.push_back(temp);	
    }
  }

  logIntersection->getStream(1)<<"Waypoints with stop lines: ;"<<(WayPointsEntries.size() - WayPointsNoStop.size())<<endl;
  logIntersection->getStream(1)<<"Waypoints without stop lines: ;"<<WayPointsNoStop.size()<<endl;
}

void CIntersectionHandling::maintainPrecedenceList(VehicleState vehState)
{
  point2 p_stopline,p2,position_obstacle;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,distance;
  PrecedenceList_t list_temp;
  int foundindex;
  vector<PrecedenceList_t> tempPrecedenceList;
  int CounterObstacle=0;
  MapElement obstacleMap;
  stringstream s;
  
  // Reset updated-flag
  for (unsigned int k=0; k<precedenceList.size(); k++)
    precedenceList[k].updated=false;

  CounterObstacle=0;

  // Look for obstacles at those waypoints with stopsigns
  for (unsigned int i=0; i<WayPointsWithStop.size(); i++) {
    PlanGraphNode* node = graph -> getWaypoint(WayPointsWithStop[i].segment, WayPointsWithStop[i].lane, WayPointsWithStop[i].point);
    point2 p_stopline = point2(node->pose.pos.x, node->pose.pos.y);
    lane = LaneLabel(WayPointsWithStop[i].segment, WayPointsWithStop[i].lane);
    localMap->getLaneCenterLine(centerline,lane);
    localMap->getObsInLane(obstacle,lane);


    // Loop through all obstacles found in lane
    for (unsigned int j=0;j<obstacle.size();j++) {
      if (!obstacle[j].isObstacle())
        continue;

      distance_geometry=INFINITY;
      // Get distance from stopline to closest point of obstacle
      for (unsigned l=0; l<obstacle[j].geometry.size();l++) {
        position_obstacle.set(obstacle[j].geometry[l]);
        localMap->getDistAlongLine(distance_temp, centerline, p_stopline, position_obstacle);
        if (distance_temp < distance_geometry)
          distance_geometry = distance_temp;
      }
      
          // Look within 30m of stopline
      if (distance_geometry>-1.0 && distance_geometry<30) {
        CounterObstacle++;
        
        // Look whether vehicle already exists
        foundindex=-1;
        
        // Store information that this vehicle still exist in list and won't be deleted later by "garbage collector"
        for (unsigned int k=0; k<precedenceList.size(); k++) {
          if (precedenceList[k].element.id==obstacle[j].id) {
            precedenceList[k].updated=true;
            precedenceList[k].element = obstacle[j];
            precedenceList[k].lane = lane;
            precedenceList[k].distance = distance_geometry;
            DGCgettime(precedenceList[k].lastUpdated);
            foundindex=k;
          }
        }
        
        if (foundindex==-1 && !firstCheckPrecedence) {
          // Create list object
          list_temp.element=obstacle[j];
          list_temp.WayPoint=WayPointsWithStop[i];
          list_temp.velocity=min(sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2)), MAXIMUM_VELOCITY);
          list_temp.distance=distance_geometry;
          list_temp.precedence=false;
          list_temp.checkedQueuing=false;
          list_temp.lane = lane;
          list_temp.updated=true;
          DGCgettime(list_temp.lastUpdated);

		      
          // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<.5m/s
          if (list_temp.velocity>=VELOCITY_OBSTACLE_THRESHOLD)
              list_temp.eta=list_temp.distance/list_temp.velocity;
          else if (list_temp.distance>=DISTANCE_OBSTACLE_THRESHOLD)
              list_temp.eta=INFINITY;
          else
              list_temp.eta=0;
          
          // store new vehicle
          precedenceList.push_back(list_temp);
          
          Log::getStream(7)<<"Obstacle "<<obstacle[j].id<<" stored"<<endl;
          Log::getStream(7)<<"distance "<<list_temp.distance<<endl;
          Log::getStream(7)<<"velocity "<<list_temp.velocity<<endl;
          Log::getStream(7)<<"ETA "<<list_temp.eta<<endl;
        }
      }
    }
  }
  
  // This prevents that new vehicles will be added to list after Alice came to a stopline
  firstCheckPrecedence=true;

  int found;

  // Clean up precedenceList "Garbage collector"
  for (unsigned int k=0; k<precedenceList.size(); k++) {
    // obstacle must disappear for a defined time (some planner cycles) until it gets removed. This will avoid that
    // flickering obstacles are deleted wrongly
    unsigned long long currentTime;
    DGCgettime(currentTime);

    // if the obstacle did not disappear for long enough, set updated flag back to true. This will (hopefully) avoid that flickering obstacles are mis-interpreted
    if (!precedenceList[k].updated) {
      if ((currentTime - precedenceList[k].lastUpdated)/1000000.0 < FLICKER_FILTER_THRESHOLD) {
        precedenceList[k].updated = true;
        logLostIdCounter++;
      }
    }

    // If obstacle seems to disappear, do some checks first before deleting this vehicle
    if (!precedenceList[k].updated) {
      // the obstacle might have changed the ID. So look for obstacles that are really close to the old ID. If so, assign this new object to the precedenceList
      // obtain position of old obstacle and get the lane that is is supposed to be 
      found=-1;
      distance = INFINITY;
      localMap->getObsNearby(obstacle,precedenceList[k].element.position, 5);
      for (unsigned int m=0; m<obstacle.size(); m++) {
        if (obstacle[m].id == precedenceList[k].element.id)
          continue;
        distance_temp = sqrt(pow(precedenceList[k].element.position.x - obstacle[m].position.x, 2) + pow(precedenceList[k].element.position.y - obstacle[m].position.y, 2));
        
        if (distance_temp<distance) {
          distance=fabs(distance_temp);
          found=m;
        }
      }
      
      // if there is an obstacle nearby, then replace the old obstacle with this new one
      if (found!=-1 && distance<TRACKING_VEHICLE_SEPERATION) {
        Console::addInterMessage("Found obstacle nearby. Do not delete in list");
        precedenceList[k].element=obstacle[found];
        DGCgettime(precedenceList[k].lastUpdated);
        precedenceList[k].updated=true;

        logNearbyObstacleCounter++;
      }

      // If there is something blocking the view, keep the obstacle that seemed to disappear
      if (precedenceList[k].precedence && !checkVisibility(vehState,precedenceList[k].element)) {
        DGCgettime(precedenceList[k].lastUpdated);
        precedenceList[k].updated=true;
        
        logLostVisibilityCounter++;
      }
    }  

    // only delete object when it is not updated
    if (!precedenceList[k].updated)
      Log::getStream(7)<<"Remove obstacle at WayPoint..."<<precedenceList[k].WayPoint<<"... ObstacleID..."<<precedenceList[k].element.id<<endl;
    else
      tempPrecedenceList.push_back(precedenceList[k]);
  }
  precedenceList=tempPrecedenceList;
}

CIntersectionHandling::IntersectionReturn CIntersectionHandling::checkPrecedence()
{
  int precedence_counter=0;
  MapElement me;

  // All Vehicles with a very small ETA have precedence
  for (unsigned int i=0; i<precedenceList.size(); i++) {
    // only the vehicle with the closest distance to each waypoint shall have precedence
    if (!precedenceList[i].checkedQueuing) {
      precedenceList[i].closest=true;
      
      // get obstacle that is the closest to the stopline
      for (unsigned k=0; k<precedenceList.size(); k++) {
        // If vehicles are at same waypoint and one is further away than the other, uncheck flag
        if ((precedenceList[i].WayPoint==precedenceList[k].WayPoint) && (precedenceList[i].distance>precedenceList[k].distance))
          precedenceList[i].closest=false;
      }
      
      // If vehicle is not flagged as "closest" and is within 3m to the closest obstacle than also give this obstacle
      // precedence as it should be the same obstacle
      if (!precedenceList[i].closest) {
        for (unsigned z=0; z<precedenceList.size(); z++) {
          if ((precedenceList[i].WayPoint==precedenceList[z].WayPoint) && (precedenceList[z].closest) && (precedenceList[i].distance-precedenceList[z].distance<3.0))
              precedenceList[i].closest=true;
        }
      }
	    
      // Set flag so that this check is only done once per vehicle
      precedenceList[i].checkedQueuing=true;
    }
    
    // If ETA is so small that it should have precedence and is the first vehicle in queue, then this
    // vehicle is flagged with precedene
    if (precedenceList[i].eta<ETA_EPS && precedenceList[i].closest) {
      precedence_counter++;
      precedenceList[i].precedence=true;
    }
    else
      precedenceList[i].precedence=false;
  }


  if (!logPrecedenceCounter) {
    logIntersection->getStream(1)<<"Higher precedence ; "<<precedence_counter<<endl;
    logPrecedenceCounter = true;
  }

  //if there are other vehicles with precedence, return false
  if (precedence_counter>0)
    return WAIT_PRECEDENCE;
  else
    return WAIT_MERGING;
}

CIntersectionHandling::IntersectionReturn CIntersectionHandling::checkMerging(SegGoals currSegment)
{
  point2 p_stopline,p_exit,position_obstacle;
  LaneLabel lane,lane_exit;
  point2arr centerline, leftBound, rightBound, leftBoundOffset, rightBoundOffset;

  vector<MapElement> obstacle,obstacle2;
  double distance_temp,distance_geometry,velocity;
  mergingList.clear();

  for (unsigned int i=0; i<WayPointsNoStop.size(); i++) {
    // if lane has no possible intersecting traffic, skip checking this lane
    if (!WayPointsNoStop[i].monitorWhileMerging)
      continue;

    // get lane of waypoint that we are looping through
    PlanGraphNode* node = graph -> getWaypoint(WayPointsNoStop[i].WayPoint.segment, WayPointsNoStop[i].WayPoint.lane, WayPointsNoStop[i].WayPoint.point);
    point2 p_stopline = point2(node->pose.pos.x, node->pose.pos.y);
    lane = LaneLabel(WayPointsNoStop[i].WayPoint.segment, WayPointsNoStop[i].WayPoint.lane);
    localMap->getLaneCenterLine(centerline,lane);
    //    localMap->getObsInLane(obstacle,lane);
    // get lane boundaries and grow them to the left and right. So we also catch obstacles from RADAR that are not perfectly within the lane
    localMap->getLaneLeftBound(leftBound, lane);
    localMap->getLaneRightBound(rightBound, lane);
    leftBoundOffset = leftBound.get_offset(-INTERSECTION_MERGING_LANE_OFFSET);
    rightBoundOffset = rightBound.get_offset(INTERSECTION_MERGING_LANE_OFFSET);

    localMap->getObsInBounds(obstacle, leftBoundOffset, rightBoundOffset);
    
    // Loop through all obstacles found in lane
    for (unsigned int j=0;j<obstacle.size();j++) {
      // do not limit search to isVehicle only! It might happen, that a vehicle broke down near the intersection. When Alice checks for merging, this vehicle might start
      // to move a few moments later. If there are any obstacles within a certain distance and a low velocity, increase the CHECK_CLEARANCE time so that Alice waits a little
      // bit longer
      //      if (!obstacle[j].isObstacle())
      //  continue;
      
      distance_geometry=INFINITY;
      // Get distance from waypoint to closest point of obstacle
      for (unsigned l=0; l<obstacle[j].geometry.size();l++) {
        position_obstacle.set(obstacle[j].geometry[l]);
        localMap->getDistAlongLine(distance_temp, centerline, p_stopline, position_obstacle);
        if (distance_temp > 0.0 && distance_temp < distance_geometry)
          distance_geometry = distance_temp;
      }

      // DARPA requires to have a 10s gap
      velocity=min(sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2)), MAXIMUM_VELOCITY);

      // if there are any obstacles nearby with a low velocity, this is suspicous. Wait longer therefore
      if (distance_geometry<10.0 && velocity<1.0 && INTERSECTION_TIME_CLEARANCE < INTERSECTION_MERGING_WAIT_LONGER)
        INTERSECTION_TIME_CLEARANCE = INTERSECTION_MERGING_WAIT_LONGER;


      logIntersection->getStream(1)<<"%MERGING: type="<<obstacle[j].type<<" ID "<<obstacle[j].id<<" distance="<<distance_geometry<<" velocity="<<velocity<<" Position="<<obstacle[j].position<<endl;
      
      // check whether vehicle is approaching the intersection (condition >0) and whether it is far away enough
      if (distance_geometry>0 && distance_geometry<10*velocity) {
        mergingList.push_back(obstacle[j]);
        return WAIT_MERGING;
      }
    }
  }
  
  return WAIT_CLEAR;
}

CIntersectionHandling::IntersectionReturn CIntersectionHandling::checkClearance(SegGoals currSegment)
{
  point2arr centerline;
  vector<PointLabel> WayPointExits;
  vector<MapElement> obstacles, obstaclesAlongGraph;
  int cabmode_counter=0;
  int moving_counter=0;
  point2arr leftBound, rightBound, leftBoundAlongGraph, rightBoundAlongGraph;
  MapElement me;
  LaneLabel lane_exit;
  stringstream s;

  clearanceList.clear();

  // start timer to make sure that Alice doesn't brake the 10s rule
  if (!JammedTimerStarted) {
    time(&timestamp);
    JammedTimerStarted=true;
  }

  if (!firstCheckClearance) {
    // get current WayPointEntry and WayPointEntry of Alice
    PointLabel currentWayPointEntry(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
    PointLabel currentWayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
    
    // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
    for (unsigned i=0; i<WayPointsEntries.size(); i++) {
      // get all exits for this entry
      localMap->getWayPointExits(WayPointExits,WayPointsEntries[i]);
      
      for (unsigned j=0;j<WayPointExits.size(); j++) {
        // Create TransitionBound between this WayPointEntry and WayPointExit
        if (getTransitionAlongGraph(leftBound, WayPointsEntries[i], WayPointExits[j], currSegment, false, -1) == -1)
          Log::getStream(1)<<"Error in getTransition. Entry "<<WayPointsEntries[i]<<". Exit "<<WayPointExits[j]<<". Left Bound"<<endl;
        if (getTransitionAlongGraph(rightBound, WayPointsEntries[i], WayPointExits[j], currSegment, false, 1) == 1)
          Log::getStream(1)<<"Error in getTransition. Entry "<<WayPointsEntries[i]<<". Exit "<<WayPointExits[j]<<". Right Bound"<<endl;

        clearanceLeftBound.push_back(leftBound);
        clearanceRightBound.push_back(rightBound);

        // Send left bound to map
        Utils::displayLine(SENDCHANNEL, leftBound, 2, MAP_COLOR_GREEN, mapcounter++);
        // Send right bound to map
        Utils::displayLine(SENDCHANNEL, rightBound, 2, MAP_COLOR_GREEN, mapcounter++);
      }

      // create transition along Alice path, including some safety space (by setting true)
      if (getTransitionAlongGraph(corridorAlongGraphLeft, currentWayPointEntry, currentWayPointExit, currSegment, true, -1) == -1)
        Log::getStream(1)<<"Error in getTransitionAlongGraph. Entry "<<currentWayPointEntry<<". Exit "<<currentWayPointExit<<". Left Bound"<<endl;
      if (getTransitionAlongGraph(corridorAlongGraphRight, currentWayPointEntry, currentWayPointExit, currSegment, true, 1) == -1)
        Log::getStream(1)<<"Error in getTransitionAlongGraph. Entry "<<currentWayPointEntry<<". Exit "<<currentWayPointExit<<". Right Bound"<<endl;
      
      // Send left bound to map
      Utils::displayLine(SENDCHANNEL, corridorAlongGraphLeft, 2, MAP_COLOR_ORANGE, mapcounter++);
      Utils::displayLine(SENDCHANNEL, corridorAlongGraphRight, 2, MAP_COLOR_ORANGE, mapcounter++);
    }        

    // set flag
    firstCheckClearance = true;

    // check for obstacles within bounds
    for (unsigned int i=0; i<clearanceRightBound.size(); i++) {
      // Get all obstacles within these bounds
      localMap->getObsInBounds(obstacles, clearanceLeftBound[i], clearanceRightBound[i]);
      
      for (unsigned int k=0; k<obstacles.size(); k++) {
        if (!obstacles[k].isObstacle())
          continue;
        // add obstacle to internal list; mainly for the console for right now
        bool f = false;
        for (unsigned int l=0; l<clearanceList.size(); l++)
          if (clearanceList[l].id == obstacles[k].id)
            f = true;
        if (!f)
          clearanceList.push_back(obstacles[k]);

        // if obstacles blocks, check if one of the following conditions is true
        // 1) static obstacle
        // 2) moving obstacle stopped longer than 10 seconds
        // 3) moving obstacle that is slower than 0.2 m/s
        // then check whether this obstacle blocks Alice's Corridor
        double v = sqrt(pow(obstacles[k].velocity.x,2) + pow(obstacles[k].velocity.y,2));
        if ((obstacles[k].isVehicle() && !obstacles[k].isPredicted() && obstacles[k].timeStopped>=10.0) || (obstacles[k].isVehicle() && !obstacles[k].isPredicted() && v<= VELOCITY_OBSTACLE_THRESHOLD) || (obstacles[k].isObstacle() && !obstacles[k].isVehicle())) {
          // query all obstacles within this boundary ...
          localMap->getObsInBounds(obstaclesAlongGraph, corridorAlongGraphLeft, corridorAlongGraphRight);
          // ... and check whether this obstacle is there
          for (unsigned int o=0; o<obstaclesAlongGraph.size(); o++)
            if (obstaclesAlongGraph[o].id == obstacles[k].id)
              cabmode_counter++;
        }
        else {
          Log::getStream(1)<<"Intersection Handling. This is a moving obstacle. Type "<< obstacles[k].type <<" Stopped = "<<obstacles[k].timeStopped<<" Pos "<<obstacles[k].position<<endl;
          moving_counter++;
        }
      }
    }
  }
  
  // check whether Alice needs to switch in cab mode
  time_t currentTime;
  time(&currentTime);
  double diff=difftime(currentTime,timestamp);

  if (moving_counter == 0 && cabmode_counter == 0) {
    if (!clearset) {
      DGCgettime(cleartime);
      clearset = true;
    }

    unsigned long long temptime;
    DGCgettime(temptime);

    if ((temptime - cleartime)/1000000.0 > INTERSECTION_TIME_CLEARANCE)
      return GO;
    else
      return WAIT_CLEAR;
  }
  else if (moving_counter==0 && cabmode_counter>0 && diff>=10.0)
    return JAMMED;

  clearset = false;
  return WAIT_CLEAR;
}

bool CIntersectionHandling::checkVisibility(VehicleState vehState, MapElement obstacle)
{
  double maxRight=-INFINITY;
  double maxLeft=INFINITY;
  point2 maxRightPoint2,maxLeftPoint2;

  // Obtain the max/min points of the obstacle's geometry
  for (unsigned int i=0; i<obstacle.geometry.size(); i++) {
    if (obstacle.geometry[i].x>maxRight) {
      maxRight=obstacle.geometry[i].x;
      maxRightPoint2.set(obstacle.geometry[i]);
    }
    if (obstacle.geometry[i].x<maxLeft) {
      maxLeft=obstacle.geometry[i].x;
      maxLeftPoint2.set(obstacle.geometry[i]);
    }
  }
  
  // Obtain front left and front right point of Alice
  point2 bumper=AliceStateHelper::getPositionFrontBumper(vehState);
  double heading = AliceStateHelper::getHeading(vehState);
  point2 frontLeft=point2(bumper.x+sin(heading)*VEHICLE_WIDTH/2,bumper.y-cos(vehState.localYaw)*VEHICLE_WIDTH/2);
  point2 frontRight=point2(bumper.x-sin(heading)*VEHICLE_WIDTH/2,bumper.y+cos(vehState.localYaw)*VEHICLE_WIDTH/2);
  
  point2arr leftBound,rightBound;
  leftBound.push_back(frontLeft);
  leftBound.push_back(maxLeftPoint2);
  rightBound.push_back(frontRight);
  rightBound.push_back(maxRightPoint2);
  
  // Get all obstacles within these bounds
  vector<MapElement> obst;
  localMap->getObsInBounds(obst, leftBound, rightBound);
  
  if (obst.size()>0) {
    double distObstacle2Alice=INFINITY;
    double distBlockingObstacle2Alice=INFINITY;
    double distTemp;
    
    // calculate distance between the obstacle that we are looking at and Alice
    // this is independant from whether this obstacle does still exist or is gone already
    point2 alice = AliceStateHelper::getPositionFrontBumper(vehState);
    for (unsigned int k=0; k<obstacle.geometry.size(); k++) {
      distTemp=sqrt(pow(obstacle.geometry[k].x-alice.x,2)+pow(obstacle.geometry[k].y-alice.y,2));
      if (distTemp<distObstacle2Alice)
        distObstacle2Alice=distTemp;
    }
    
    for (unsigned int i=0; i<obst.size(); i++) {
      if (obst[i].id!=obstacle.id) {
        for (unsigned int l=0; l<obst[i].geometry.size(); l++) {
          distTemp=sqrt(pow(obst[i].geometry[l].x-alice.x,2)+pow(obst[i].geometry[l].y-alice.y,2));
          if (distTemp<distBlockingObstacle2Alice)
            distBlockingObstacle2Alice=distTemp;
        }
      }
    }
    
    // If any other obstacle is closer, return false as it seems to be in between the obstacle that we are looking at and Alice
    // this includes, that the two obstacles must be apart more than 1.0 meters. This should prevent misinterpretation when fusing has problems and two obstacles representing
    // the same real obstacle are "block" each other
    if ( distBlockingObstacle2Alice<distObstacle2Alice && fabs(distBlockingObstacle2Alice - distObstacle2Alice)> 1.0 ) {
      Console::addInterMessage("Warning: Obstacle blocking view");
      return false;
    }
    else
      return true;
  }
  else
    return true;
}

bool CIntersectionHandling::crossPoint(point2arr a, point2arr b, point2 &crosspoint)
{
  for (unsigned int ai = 1; ai<a.size(); ai++) {
    for (unsigned int bi = 1; bi<b.size(); bi++) {
      double ax=a[ai-1].x;
      double ay=a[ai-1].y;
      double bx=a[ai].x;
      double by=a[ai].y;

      double cx=b[bi-1].x;
      double cy=b[bi-1].y;
      double dx=b[bi].x;
      double dy=b[bi].y;


      double v = ((cx-ax) * (by-ay) - (cy-ay) * (bx-ax)) / ((dy-cy) * (bx-ax) - (dx-cx) * (by-ay));

      crosspoint.x = cx + v * (dx-cx);
      crosspoint.y = cy + v * (dy-cy);


      bool result_a = inSection(a[ai-1],a[ai], crosspoint);
      bool result_b = inSection(b[bi-1],b[bi], crosspoint);
      
      if (result_a && result_b) return true;
    }
  }
  return false;
}

bool CIntersectionHandling::inSection(point2 a, point2 b, point2 x)
{
  double v = (x.x - a.x) / (b.x - a.x);
  if (v>=0 && v<=1) return true;
  return false;
}


int CIntersectionHandling::getTransitionAlongGraph(point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, SegGoals currSegment, bool grow, int leftright)
{
  Vehicle *vp;
  Maneuver *mp;  
  point2 projectEntry, projectExit, p_entry, p_exit;
  Pose2D pose_entry, pose_exit;
  VehicleConfiguration config;
  LaneLabel entryLane, exitLane;
  point2arr entryBound, exitBound;
  
  // get point2 of WaypointEntry and WaypointExit
  PlanGraphNode* entry = graph -> getWaypoint(WaypointEntry.segment, WaypointEntry.lane, WaypointEntry.point);
  PlanGraphNode* exit = graph -> getWaypoint(WaypointExit.segment, WaypointExit.lane, WaypointExit.point);

  // get the lanes of entry and exit
  p_entry = point2(entry->pose.pos.x, entry->pose.pos.y);
  p_exit = point2(exit->pose.pos.x, exit->pose.pos.y);
  entryLane = LaneLabel(WaypointEntry.segment, WaypointEntry.lane);
  exitLane = LaneLabel(WaypointExit.segment, WaypointExit.lane);

  // get the left and right points of entry/exit
  if (leftright == -1) {
    localMap->getLeftBound(entryBound,entryLane);
    localMap->getLeftBound(exitBound,exitLane);
  }
  else if (leftright == 1) {
    localMap->getRightBound(entryBound,entryLane);
    localMap->getRightBound(exitBound,exitLane);
  }
  localMap->getProjectToLine(projectEntry, entryBound, p_entry);
  localMap->getProjectToLine(projectExit, exitBound, p_exit);

  // for safety reasons, grow this corridor to the left and right
  point2arr extendEntryTemp, extendEntry, extendExitTemp, extendExit;

  extendEntryTemp.push_back(p_entry);
  extendEntryTemp.push_back(projectEntry);
  extendExitTemp.push_back(p_exit);
  extendExitTemp.push_back(projectExit);

  localMap->extendLine(extendEntry, extendEntryTemp, INTERSECTION_GROW_CORRIDOR / 2.0);
  localMap->extendLine(extendExit, extendExitTemp, INTERSECTION_GROW_CORRIDOR / 2.0);

  // If left turn, grow right side of corridor only
  // If right turn, grow left side of corridor only
  // If Straigt, don't grow corridor

  if (grow && ((currSegment.intersection_type == SegGoals::INTERSECTION_LEFT && leftright == 1) || (currSegment.intersection_type == SegGoals::INTERSECTION_RIGHT && leftright == -1))) {
    projectEntry = extendEntry.back();
    projectExit = extendExit.back();
  }

  pose_entry.x = projectEntry.x;
  pose_entry.y = projectEntry.y;


  //double r, p;
  //quat_to_rpy(entry->pose.rot, &r, &p, &pose_entry.theta);
  pose_entry.theta = entry->pose.rot;
  //quat_to_rpy(exit->pose.rot, &r, &p, &pose_exit.theta);
  pose_exit.theta = exit->pose.rot;
  pose_exit.x = projectExit.x;
  pose_exit.y = projectExit.y;

  // Vehicle properties
  vp = maneuver_create_vehicle( VEHICLE_WHEELBASE, M_PI/2 );
    // Create maneuver object
  mp = maneuver_pose2pose(vp, &pose_entry, &pose_exit);
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // create boundaries
  bound.clear();
  int numSteps = 20;
  for (int i = 1; i < numSteps + 1; i++) 
  {
    double s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    point2 p=point2(config.x, config.y);
    bound.push_back(p);
  }

  maneuver_free(mp);
  free(vp);

  return 0;
}

int CIntersectionHandling::getTransition(point2arr& transition, PointLabel entryPointLabel, PointLabel exitPointLabel)
{
  Pose2D pose_entry, pose_exit;

  PlanGraphNode* entry = graph -> getWaypoint(entryPointLabel.segment, entryPointLabel.lane, entryPointLabel.point);
  PlanGraphNode* exit = graph -> getWaypoint(exitPointLabel.segment, exitPointLabel.lane, exitPointLabel.point);

  //double r, p;
  //quat_to_rpy(entry->pose.rot, &r, &p, &pose_entry.theta);
  pose_entry.theta = entry->pose.rot;
  //quat_to_rpy(exit->pose.rot, &r, &p, &pose_exit.theta);
  pose_exit.theta = entry->pose.rot;

  // Vehicle properties
  Vehicle *vp;
  vp = maneuver_create_vehicle( VEHICLE_WHEELBASE, M_PI/2 );
 
  // Create maneuver object
  Maneuver *mp;  
  pose_entry.x = entry->pose.pos.x;
  pose_entry.y = entry->pose.pos.y;
  pose_exit.x = exit->pose.pos.x;
  pose_exit.y = exit->pose.pos.y;

  mp = maneuver_pose2pose(vp, &pose_entry, &pose_exit);
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // create boundaries
  transition.clear();
  int numSteps = 20;
  for (int i = 1; i < numSteps + 1; i++) 
  {
    double s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    VehicleConfiguration config;
    config = maneuver_evaluate_configuration(vp, mp, s);    
    point2 p=point2(config.x, config.y);
    transition.push_back(p);
  }

  maneuver_free(mp);
  free(vp);
  return 0;
}
