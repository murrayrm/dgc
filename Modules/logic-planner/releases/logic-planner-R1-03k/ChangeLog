Tue Oct 16 21:31:12 2007	Noel duToit (ndutoit)

	* version R1-03k
	BUGS:  
	FILES: LogicPlanner.cc(44967)
	Adapted to new getDistToStopline function

	FILES: LogicPlanner.cc(45015)
	latest commit

	FILES: LogicPlanner.cc(45016)
	Now new stop line structure handles row intersection 

	FILES: LogicPlanner.cc(45021)
	checks the next 4 seg goals forrow intersections

	FILES: LogicPlanner.cc(45024), logicplanner.conf(45024)
	Fixes from the field: Do not back up on 1 lane segments until
	s1planner obstacle_bare fails; reset the timer (that keeps track of
	how long we have been in the current state) when we are pause; when
	in start-chute, we are now stopping at intersections; switching to
	DRIVE from PAUSE if we were in backup; switch the order in the
	logic (try all options for rail planner before s1planner)

Mon Oct 15 23:39:23 2007	Noel duToit (ndutoit)

	* version R1-03j
	BUGS:  
	FILES: IntersectionHandling.cc(44774),
		IntersectionHandling.hh(44774), LogicPlanner.cc(44774),
		LogicPlanner.hh(44774)
	* Fixed bug in analyses part of intersection * Created Intersection
	Class to avoid all the static obstacles * uses Utils::displayXXXXXX
	functions for printing output to Mapviewer

	FILES: IntersectionHandling.cc(44789),
		IntersectionHandling.hh(44789)
	* countes the switches between WAIT_CLEAR and WAIT_MERGING and puts
	it into analyses file

	FILES: IntersectionHandling.cc(44834), LogicPlanner.cc(44834),
		LogicPlanner.hh(44834), logicplanner.conf(44834)
	In STOB_OBS and having multiple lanes, wait longer in case other
	lane is blocked by vehicles

	FILES: LogicPlanner.cc(44805), LogicPlanner.hh(44805)
	Added updateIntersectionHandlingConsole function

	FILES: LogicPlanner.cc(44810), LogicPlanner.hh(44810),
		logicplanner.conf(44810)
	Added timeout for STOPINT and added a transition from STOPOBS to
	STOPINT

	FILES: LogicPlanner.cc(44836)
	add stopped_duration to Console

	FILES: LogicPlanner.cc(44839)
	Handle START_CHUTE seg goal

	FILES: LogicPlanner.cc(44848)
	Wait longer in failure handling when there're multiple lanes so we
	don't have to do illegal passing

Mon Oct 15  7:59:53 2007	Noel duToit (ndutoit)

	* version R1-03i
	BUGS:  
	FILES: IntersectionHandling.cc(44287),
		IntersectionHandling.conf(44287),
		IntersectionHandling.hh(44287)
	Bug fixes and integration of changes from last El Toro field test

	FILES: IntersectionHandling.cc(44288),
		IntersectionHandling.conf(44288)
	bug fixes

	FILES: IntersectionHandling.cc(44432),
		IntersectionHandling.conf(44432),
		IntersectionHandling.hh(44432)
	Included analyses information into seperate log files

	FILES: IntersectionHandling.cc(44434)
	bug fixes in outputLog

	FILES: LogicPlanner.cc(44134)
	Fixed bug where planner sometimes doesn't report to mplanner that
	it's using different path.

	FILES: LogicPlanner.cc(44137)
	Commented out the part where we determine if we get lost.

	FILES: LogicPlanner.cc(44138), LogicPlanner.hh(44138),
		logicplanner.conf(44138)
	Do not keep reporting that we're lost if our current segment is the
	same as what we reported previously.

	FILES: LogicPlanner.cc(44191), LogicPlanner.hh(44191),
		logicplanner.conf(44191)
	In STOPOBS_PASS, do not switch in FH when dyn vehicles are around

	FILES: LogicPlanner.cc(44193)
	minor fix

	FILES: LogicPlanner.cc(44278, 44280)
	removed getting lost error reporting (temporary).

	FILES: LogicPlanner.cc(44380)
	put back the getting lost code.

	FILES: LogicPlanner.cc(44417), LogicPlanner.hh(44417),
		logicplanner.conf(44417)
	Removed the transition from STOP_INT to STOP_OBS. Added the
	transition from STOP_OBS to STOP_INT. (velocity planner also checks
	for obstacle when we're in STOP_INT.) Added an option in the config
	file to use rail planner for backing up.

	FILES: LogicPlanner.cc(44488), logicplanner.conf(44488)
	Transition from PASS to NOPASS only when we're already in the
	desired lane.

	FILES: LogicPlanner.cc(44499), LogicPlanner.hh(44499)
	set replanInZone to true when we change state so we clear the
	clothoid tree.

	FILES: LogicPlanner.cc(44502)
	Fixed minor bug in setting replanInZone

	FILES: LogicPlanner.cc(44601)
	Updated freshness handling

	FILES: logicplanner.conf(44220)
	Increased FH_STOP_TIME

Thu Oct 11  9:03:26 2007	Nok Wongpiromsarn (nok)

	* version R1-03h
	BUGS:  
	FILES: LogicPlanner.cc(44060), logicplanner.conf(44060)
	Fixed bugs where we sometimes transition from PAUSE to DRIVE_NOPASS
	instead of to paused_lp_state.

Thu Oct 11  2:08:46 2007	Nok Wongpiromsarn (nok)

	* version R1-03g
	BUGS:  
	FILES: LogicPlanner.cc(44034), LogicPlanner.hh(44034)
	Merged my previous branch with Andrew's changes

Thu Oct 11  0:35:29 2007	Andrew Howard (ahoward)

	* version R1-03f
	BUGS:  
	FILES: IntersectionHandling.cc(43862),
		IntersectionHandling.hh(43862), LogicPlanner.cc(43862),
		LogicPlanner.hh(43862)
	Switched to plan graph

Mon Oct  8 18:11:59 2007	Nok Wongpiromsarn (nok)

	* version R1-03e
	BUGS:  
	FILES: LogicPlanner.cc(43610)
	Also handle PP_NONODEFOUND

Mon Oct  8 13:44:56 2007	Christian Looman (clooman)

	* version R1-03d
	BUGS:  
	FILES: IntersectionHandling.cc(43545), LogicPlanner.cc(43545)
	* in STOP_OBS: replan before go into backup * intersection
	handling: fixed bug so we do not give precedence when Alice is on a
	right-of-way lane at a ROW intersection

Sun Oct  7 19:03:28 2007	Nok Wongpiromsarn (nok)

	* version R1-03c
	BUGS:  
	FILES: LogicPlanner.cc(43385), LogicPlanner.hh(43385),
		logicplanner.conf(43385)
	Added two more states: DRIVE_AGGRESSIVE and DRIVE_BARE. Handle
	S1PLANNER_FAILED. Send replan if path-planner returns PP_NOPATH_LEN
	or PP_NOPATH_COST

	FILES: LogicPlanner.cc(43423), LogicPlanner.hh(43423),
		logicplanner.conf(43423)
	Make sure that we don't switch out of s1planner too quickly. Even
	when s1planner fails to find a path, we'll wait in that state for 3
	seconds. (This is specified in logicplanenr.conf, so you can change
	this number if you want.)

	FILES: LogicPlanner.cc(43440), logicplanner.conf(43440)
	Make sure that failure handling actually changes the state. For
	example, if the flag is already PASS, and currentCounter is also
	PASS, increment currentCounter.

Sat Oct  6 16:58:05 2007	Christian Looman (clooman)

	* version R1-03b
	BUGS:  
	FILES: IntersectionHandling.cc(43001)
	release memory properly in getTransitionAlongGraph

	FILES: IntersectionHandling.cc(43011),
		IntersectionHandling.conf(43011),
		IntersectionHandling.hh(43011)
	If there are any obstacles with low velocity while merging, wait
	longer.

	FILES: IntersectionHandling.cc(43027),
		IntersectionHandling.conf(43027),
		IntersectionHandling.hh(43027)
	verified correct intersection handling

	FILES: LogicPlanner.cc(43139)
	Changed back stopping_distance at intersections to 1.0m

	FILES: LogicPlanner.cc(43153), LogicPlanner.hh(43153),
		logicplanner.conf(43153)
	Limit the distance we can drive with s1planner.

	FILES: LogicPlanner.cc(43166)
	Fixed bug where we keep switching between backing up and using
	s1planner.

	FILES: LogicPlanner.cc(43168)
	Added more output to log

	FILES: LogicPlanner.cc(43170)
	Reset backup_lp_state to DRIVE_PASS_BACKUP when the user switches
	off FH.

	FILES: LogicPlanner.cc(43204)
	reverted back to the 2m - limit while stopping at intersections as
	the real bug couldnt be tracked down

	FILES: LogicPlanner.cc(43205)
	spelling mistake

	FILES: LogicPlanner.cc(43253)
	Clean up the FSM. Removed the useless transition from
	DRIVE_PASS_BACKUP to UTURN. (Switch to STOPOBS_PASS_BACKUP first.)

	FILES: LogicPlanner.cc(43279)
	Removed the cycle: BACKUP -> S1PLANNER_SAFETY -> S1PLANNER_AGG ->
	S1PLANNER_BARE -> BACKUP

Fri Oct  5  8:33:05 2007	Christian Looman (clooman)

	* version R1-03a
	BUGS:  
	FILES: IntersectionHandling.cc(42954), LogicPlanner.cc(42954)
	Changes from El-Toro 2007-10-04, including the fix for the coredump
	while merging at intersections

Wed Oct  3 18:37:04 2007	Nok Wongpiromsarn (nok)

	* version R1-03
	BUGS:  
	FILES: LogicPlanner.cc(42673), logicplanner.conf(42673)
	In failure handler, can change obstacle size 2 times.

Wed Oct  3 16:43:23 2007	Christian Looman (clooman)

	* version R1-02z
	BUGS:  
	FILES: LogicPlanner.cc(42607)
	sets FailureHandler index in the console

Wed Oct  3 15:39:30 2007	Nok Wongpiromsarn (nok)

	* version R1-02y
	BUGS:  
	FILES: LogicPlanner.cc(42556), LogicPlanner.hh(42556),
		logicplanner.conf(42556)
	Reduced the amount of time before triggering failure handling.
	Fixed bug where we don't switch out of backup

Wed Oct  3  3:33:18 2007	Nok Wongpiromsarn (nok)

	* version R1-02x
	BUGS:  
	New files: logicplanner.conf
	FILES: LogicPlanner.cc(42462), LogicPlanner.hh(42462),
		Makefile.yam(42462)
	Parameters for logic planner are now specified in the config file
	logicplanner.conf so we don't have to recompile logic planner and
	relink planner every time we want to change a parameter.

Tue Oct  2 22:59:46 2007	Nok Wongpiromsarn (nok)

	* version R1-02w
	BUGS:  
	FILES: LogicPlanner.cc(42369), LogicPlanner.hh(42369),
		Makefile.yam(42369)
	Fixed couple bugs

Tue Oct  2 15:18:26 2007	Christian Looman (clooman)

	* version R1-02v
	BUGS:  
	FILES: LogicPlanner.cc(42271)
	removed debugging information

Tue Oct  2 15:11:56 2007	Christian Looman (clooman)

	* version R1-02u
	BUGS:  
	FILES: IntersectionHandling.cc(42263), LogicPlanner.cc(42263)
	Fixed bug in Merging Procedure to fix core dump from night
	simulations

Tue Oct  2  9:25:16 2007	Christian Looman (clooman)

	* version R1-02t
	BUGS:  
	FILES: IntersectionHandling.cc(42081),
		IntersectionHandling.hh(42081), LogicPlanner.cc(42081),
		UT_logicplanner.cc(42081)
	checks for feasibility while merging. Also, no more
	INTERSECTION_SAFEY-FLAG and CABMODE-flag anymore

	FILES: IntersectionHandling.cc(42083)
	bug fix

	FILES: IntersectionHandling.cc(42187), LogicPlanner.cc(42187)
	Change to site coordinates

	FILES: IntersectionHandling.conf(42082)
	missed one file at the last commit

Mon Oct  1  9:08:13 2007	Nok Wongpiromsarn (nok)

	* version R1-02s
	BUGS:  
	FILES: LogicPlanner.cc(42023)
	Do not stop using s1planner if we're in a zone.

Sun Sep 30 14:41:53 2007	Nok Wongpiromsarn (nok)

	* version R1-02r
	BUGS:  
	FILES: LogicPlanner.cc(41895)
	Fixed the logic so instead of failing to mplanner after backing up
	more than 3 times, it switches to s1planner first.

Sun Sep 30  3:12:02 2007	Nok Wongpiromsarn (nok)

	* version R1-02q
	BUGS:  
	FILES: LogicPlanner.cc(41688)
	A simple version of fault handling. Only switch to
	S1PLANNER/CIRCLE_PLANNER/DPLANNER (depending on cmdline option) and
	change the distance from obstacle before making a u-turn. S1PLANNER
	still plans right through the obstacle but at least it should be
	able to go through the narrow one lane road at El Toro.

Sat Sep 29 23:22:49 2007	Nok Wongpiromsarn (nok)

	* version R1-02p
	BUGS:  
	FILES: LogicPlanner.cc(41651)
	Make planLogic more robust to noise (obstacle flickering in and
	out, time_stop gets reset, etc). If we need to go back and forth
	more than 3 times, we'll transition to UTURN. Also, do not
	transition from STOP_OBS to DRIVE right when we see obstacle
	disappears. Make sure that it has disappeared for at least 1.5 sec
	before switching to DRIVE so we don't keep switching between
	STOP_OBS and DRIVE when obstacle is flickering in and out.

Sat Sep 29 17:56:58 2007	vcarson (vcarson)

	* version R1-02o
	BUGS:  
	FILES: LogicPlanner.cc(41602)
	Incrementing failures.	Querying the toggling of fault handling
	before calling faultHandler method.

Sat Sep 29 13:19:45 2007	Nok Wongpiromsarn (nok)

	* version R1-02n
	BUGS:  
	FILES: LogicPlanner.cc(41495)
	Fixed bug where we kept backing up after STOP_OBS when current_flag
	is PASS.

Sat Sep 29 11:06:57 2007	Nok Wongpiromsarn (nok)

	* version R1-02m
	BUGS:  
	FILES: LogicPlanner.cc(41306), LogicPlanner.hh(41306)
	planLogic also returns the current lane we're in (so mplanner knows
	what planner thinks we're in case we get out of sync). It also
	determines whether the lane is blocked or the road is blocked. Wait
	for 4 seconds before switching to UTURN and 1 sec before switching
	to PASS to give sensing enough time to filter out groundstrikes.

	FILES: LogicPlanner.cc(41414)
	If we have not stopped long enough, we'll transition to STOP_OBS,
	PASS before transitioning to UTURN. Added a transition STOP_OBS ->
	UTURN.

	FILES: LogicPlanner.cc(41432)
	Forgot to set current lane before sending failure to mplanner

	FILES: LogicPlanner.cc(41306), LogicPlanner.hh(41306)
	planLogic also returns the current lane we're in (so mplanner knows
	what planner thinks we're in case we get out of sync). It also
	determines whether the lane is blocked or the road is blocked. Wait
	for 4 seconds before switching to UTURN and 1 sec before switching
	to PASS to give sensing enough time to filter out groundstrikes.

	FILES: LogicPlanner.cc(41414)
	If we have not stopped long enough, we'll transition to STOP_OBS,
	PASS before transitioning to UTURN. Added a transition STOP_OBS ->
	UTURN.

	FILES: LogicPlanner.cc(41432)
	Forgot to set current lane before sending failure to mplanner

Fri Sep 28 16:08:58 2007	Christian Looman (clooman)

	* version R1-02l
	BUGS:  
	FILES: LogicPlanner.cc(41161), LogicPlanner.hh(41161)
	Commit after merge

	FILES: LogicPlanner.cc(41154), LogicPlanner.hh(41154)
	fault handler takes into account when Alice is estopped

Fri Sep 28 15:49:01 2007	vcarson (vcarson)

	* version R1-02k
	BUGS:  
	FILES: IntersectionHandling.cc(41127), LogicPlanner.cc(41127)
	Fixed bug that was starting logic planner with s1planner. 

Fri Sep 28 14:21:16 2007	vcarson (vcarson)

	* version R1-02j
	BUGS:  
	FILES: LogicPlanner.cc(41082)
	Merges

	FILES: LogicPlanner.cc(41082)
	Merges

	FILES: LogicPlanner.cc(41082)
	Merges

	FILES: LogicPlanner.cc(41050)
	Only calling getCurrentLane if we are not in a zone. 

	FILES: LogicPlanner.cc(41068)
	Fixed problem was causing a segfault in zones because the
	Utils::getCurrentLane had changed to return graph nodes 1 m from
	Alice.	In a zone, nodes at 1m from Alice do not always exist. 
	This was causing null pointers to be returned.	  Fixed bugs
	related to fault handling - added condition that if we are in a
	zone, we will not BACKUP, or change planners. 

Fri Sep 28 10:31:01 2007	Christian Looman (clooman)

	* version R1-02i
	BUGS:  
	FILES: IntersectionHandling.cc(41037),
		IntersectionHandling.hh(41037), LogicPlanner.cc(41037),
		UT_logicplanner.cc(41037)
	do not change planner in zones

Fri Sep 28  9:23:33 2007	Christian Looman (clooman)

	* version R1-02h
	BUGS:  
	FILES: LogicPlanner.cc(41019)
	Commit after merge

	FILES: LogicPlanner.cc(41012)
	Fualt handling distinguishes between different regions. That should
	fix the bug that fault handler switched into rail planner while
	being in zones

Thu Sep 27 10:22:28 2007	Sven Gowal (sgowal)

	* version R1-02g
	BUGS:  
	FILES: LogicPlanner.cc(40799)
	Merged

	FILES: LogicPlanner.cc(40792)
	Using new Utils::getCurrentLane to improve performances.

Wed Sep 26 21:52:41 2007	Christian Looman (clooman)

	* version R1-02f
	BUGS:  
	FILES: IntersectionHandling.cc(40641),
		IntersectionHandling.conf(40641), LogicPlanner.cc(40641),
		LogicPlanner.hh(40641)
	Fixed bug in intersection handling.  Integrated first version of
	fault handling

Wed Sep 26 10:43:43 2007	Sven Gowal (sgowal)

	* version R1-02e
	BUGS:  
Wed Sep 26  8:51:43 2007	Christian Looman (clooman)

	* version R1-02d
	BUGS:  
	FILES: IntersectionHandling.cc(40414)
	Uncommented out expression that was used for testing before

Tue Sep 25 11:21:06 2007	Christian Looman (clooman)

	* version R1-02c
	BUGS:  
	FILES: IntersectionHandling.cc(40266),
		IntersectionHandling.conf(40266),
		IntersectionHandling.hh(40266)
	Maximum velocity of each obstacles restricted to 15 m/s

Sun Sep 23 15:18:19 2007	Noel duToit (ndutoit)

	* version R1-02b
	BUGS:  
	FILES: LogicPlanner.cc(40061)
	sets the planner to be used for zone planning based on the cmd line
	args.

Fri Sep 21 18:06:40 2007	Christian Looman (clooman)

	* version R1-02a
	BUGS:  
	FILES: IntersectionHandling.cc(39938),
		IntersectionHandling.conf(39938),
		IntersectionHandling.hh(39938), UT_logicplanner.cc(39938)
	Commit after merge

	FILES: IntersectionHandling.cc(39792),
		IntersectionHandling.conf(39792),
		IntersectionHandling.hh(39792), UT_logicplanner.cc(39792)
	Grow corridor within intersection depending on right or left turns.

Fri Sep 21 16:37:40 2007	Noel duToit (ndutoit)

	* version R1-02
	BUGS:  
	FILES: LogicPlanner.cc(39858), LogicPlanner.hh(39858)
	Updated to populate appropriate fields in the state problem to
	enable fault handling.

Tue Sep 18 19:01:00 2007	Christian Looman (clooman)

	* version R1-01z
	BUGS:  
	FILES: IntersectionHandling.cc(39380),
		IntersectionHandling.hh(39380)
	Outputs information about merging into the console. Reduced
	checking for clearance from 3 to 2 seconds

Mon Sep 17 20:57:27 2007	Christian Looman (clooman)

	* version R1-01y
	BUGS:  
	FILES: IntersectionHandling.cc(39239),
		IntersectionHandling.conf(39239),
		IntersectionHandling.hh(39239)
	Added INTERSECTION_TIMEOUT. If Alice waits longer than this values
	at the intersection, it gives the GO command.

Thu Sep 13 17:57:00 2007	Christian Looman (clooman)

	* version R1-01x
	BUGS:  
	FILES: IntersectionHandling.cc(38663)
	Implemented the changes from the test in El Toro. Do not check for
	Precedence in own lane.

Tue Sep 11 21:41:59 2007	Christian Looman (clooman)

	* version R1-01w
	BUGS:  
	FILES: IntersectionHandling.cc(38448),
		IntersectionHandling.hh(38448)
	Alice checks intersection's clearance for 2 seconds now

Tue Sep 11 13:47:52 2007	Christian Looman (clooman)

	* version R1-01v
	BUGS:  
	FILES: IntersectionHandling.cc(38349)
	Little bug in transitions

Tue Sep 11 13:14:26 2007	Christian Looman (clooman)

	* version R1-01u
	BUGS:  
	FILES: IntersectionHandling.cc(38332),
		IntersectionHandling.hh(38332)
	commit after merge

	FILES: IntersectionHandling.cc(38320),
		IntersectionHandling.hh(38320)
	Add output to console about obstacles that affect Intersection
	Handling

Mon Sep 10 12:33:28 2007	vcarson (vcarson)

	* version R1-01t
	BUGS:  
	FILES: LogicPlanner.cc(38058)
	Different checking for switching to ZONE state.  Simply checking if
	rear axle is inside zone polygon.  

Mon Sep 10  9:28:09 2007	Christian Looman (clooman)

	* version R1-01s
	BUGS:  
	FILES: IntersectionHandling.cc(38025),
		IntersectionHandling.conf(38025),
		IntersectionHandling.hh(38025), LogicPlanner.cc(38025)
	Commit after merge

	FILES: IntersectionHandling.cc(38018),
		IntersectionHandling.conf(38018),
		IntersectionHandling.hh(38018), LogicPlanner.cc(38018)
	Fixed thousand bugs.

Tue Sep  4  1:17:49 2007	vcarson (vcarson)

	* version R1-01r
	BUGS:  
	FILES: LogicPlanner.cc(37403)
	Change to get into zone when Alice is completely in zone. 

Sun Sep  2 17:33:07 2007	Sven Gowal (sgowal)

	* version R1-01q
	BUGS:  
	FILES: LogicPlanner.cc(37107)
	Merged

	FILES: LogicPlanner.cc(37109)
	Avoid double checking the same intersection

	FILES: LogicPlanner.cc(37089)
	Removed safety box + added STOP_INT->DRIVE transition

Sun Sep  2 11:17:43 2007	Christian Looman (clooman)

	* version R1-01p
	BUGS:  
	FILES: IntersectionHandling.cc(37072), LogicPlanner.cc(37072),
		LogicUtils.cc(37072)
	Commit after merge

	FILES: IntersectionHandling.cc(37065), LogicPlanner.cc(37065),
		LogicUtils.cc(37065)
	Fixed bug that IntersectionHandling used SegGoals when SegGoalQueue
	was messed up

Fri Aug 31 17:14:15 2007	Sven Gowal (sgowal)

	* version R1-01o
	BUGS:  
	FILES: LogicPlanner.cc(36878)
	Merged

	FILES: LogicPlanner.cc(36871)
	Added condition to not double check the same intersection

Thu Aug 30 18:15:06 2007	Noel duToit (ndutoit)

	* version R1-01n
	BUGS:  
	FILES: LogicPlanner.cc(36698)
	merged with the latest release

	FILES: LogicPlanner.cc(36686)
	Reduced the safety box around alice a little.

Thu Aug 30 17:23:18 2007	Christian Looman (clooman)

	* version R1-01m
	BUGS:  
	FILES: LogicPlanner.cc(36622)
	Updates the distance to stopline in the console

Thu Aug 30 10:57:03 2007	Christian Looman (clooman)

	* version R1-01l
	BUGS:  
	FILES: IntersectionHandling.cc(36486)
	removed debug output from mapviewer again

	FILES: IntersectionHandling.cc(36487)
	removed more output

Wed Aug 29 22:11:56 2007	Christian Looman (clooman)

	* version R1-01k
	BUGS:  
	FILES: IntersectionHandling.cc(36467),
		IntersectionHandling.conf(36467), UT_logicplanner.cc(36467)
	Commit after merge

	FILES: IntersectionHandling.cc(36460),
		IntersectionHandling.conf(36460), UT_logicplanner.cc(36460)
	Changed UnitTest to test different RNDFs more detailed

Tue Aug 28 19:32:49 2007	Noel duToit (ndutoit)

	* version R1-01j
	BUGS:  
	New files: LogicUtils.cc LogicUtils.hh
	FILES: LogicPlanner.cc(36063), LogicPlanner.hh(36063),
		Makefile.yam(36063)
	merged with latest release

	New files: LogicUtils.cc LogicUtils.hh
	FILES: LogicPlanner.cc(36014), LogicPlanner.hh(36014),
		Makefile.yam(36014)
	Added back the logic utils files. Added function that pauses Alice
	when there is an obstacle very close (inside our safety box).

	FILES: LogicPlanner.cc(36016)
	minor change

Tue Aug 28 19:00:05 2007	Christian Looman (clooman)

	* version R1-01i
	BUGS:  
	FILES: LogicPlanner.cc(36049)
	Interaction with getDistToStopline

Tue Aug 28 13:59:20 2007	Christian Looman (clooman)

	* version R1-01h
	BUGS:  
	FILES: IntersectionHandling.cc(35942)
	changed getLeftBound/getRightBound to
	getLaneLeftBound/getLaneRightBound

Tue Aug 28 11:26:30 2007	Christian Looman (clooman)

	* version R1-01g
	BUGS:  
	FILES: IntersectionHandling.cc(35856), LogicPlanner.cc(35856)
	Intersections now work with zones as well

	FILES: IntersectionHandling.cc(35870),
		IntersectionHandling.conf(35870),
		IntersectionHandling.hh(35870), LogicPlanner.cc(35870)
	IntersectionHandling will wait for a defined amount of cycles until
	it deletes obstacles out of its internal list

	FILES: IntersectionHandling.cc(35856), LogicPlanner.cc(35856)
	Intersections now work with zones as well

Mon Aug 27  9:00:46 2007	vcarson (vcarson)

	* version R1-01f
	BUGS:  
	Deleted files: LogicUtils.cc LogicUtils.hh
	FILES: IntersectionHandling.cc(35426), LogicPlanner.cc(35426),
		Makefile.yam(35426)
	Removed LogicUtils and put functions in
	temp-planner-interfaces/Utils.hh.  These functions need to be used
	from planner as well as logic planner.	  

Thu Aug 23 17:06:39 2007	Christian Looman (clooman)

	* version R1-01e
	BUGS:  
	FILES: LogicPlanner.cc(35305), LogicUtils.cc(35305)
	Fixed bug that planner does not switch into STOP_INT right after
	passing the stopline

Thu Aug 23 15:56:42 2007	Christian Looman (clooman)

	* version R1-01d
	BUGS:  
	FILES: LogicPlanner.cc(35258), LogicUtils.cc(35258),
		LogicUtils.hh(35258)
	Changed the way how to stop at stoplines. Also works with zones
	reliable. Some debugging is still necessary but at least, people
	can continue working on zones

Wed Aug 22 18:56:31 2007	Christian Looman (clooman)

	* version R1-01c
	BUGS:  
	FILES: IntersectionHandling.cc(35027), LogicPlanner.cc(35027),
		LogicUtils.cc(35027)
	Commit after Merge

	FILES: IntersectionHandling.cc(35020), LogicPlanner.cc(35020),
		LogicUtils.cc(35020)
	Removed mapviewer output when checking visibility. Removed bug in
	getDistToStopline and when changing into STOP_INT at left turns
	(ROW intersections)

Wed Aug 22 17:55:12 2007	Sven Gowal (sgowal)

	* version R1-01b
	BUGS:  
	FILES: LogicPlanner.cc(34988)
	Pause when first going into a zone

Wed Aug 22 15:30:37 2007	Christian Looman (clooman)

	* version R1-01a
	BUGS:  
	FILES: IntersectionHandling.conf(34936)
	changed obstacle_velocity_threshold to 0.5 in
	IntersectionHandling.conf

Tue Aug 21 19:12:58 2007	Christian Looman (clooman)

	* version R1-01
	BUGS:  
	FILES: IntersectionHandling.cc(34764), LogicPlanner.cc(34764),
		UT_logicplanner.cc(34764)
	Fixed SegFault at intersection. Only calls IntersectionHandling
	when close to stopline

Tue Aug 21  6:18:14 2007	Sam Pfister (sam)

	* version R1-00z
	BUGS:  
	FILES: IntersectionHandling.cc(34546),
		IntersectionHandling.hh(34546)
	Changed debugging output channel from 0 which is reserved for
	perceptors to -2 which is the standard debugging channel

Mon Aug 20 23:44:45 2007	Christian Looman (clooman)

	* version R1-00y
	BUGS:  
	New files: UT_logicplanner.cc
	FILES: IntersectionHandling.cc(34497),
		IntersectionHandling.hh(34497), LogicPlanner.cc(34497),
		LogicPlanner.hh(34497), Makefile.yam(34497)
	Commit after merge

	New files: UT_logicplanner.cc
	FILES: IntersectionHandling.cc(33843, 34088),
		IntersectionHandling.hh(33843, 34088), Makefile.yam(33843,
		34088)
	backup

	FILES: IntersectionHandling.cc(34449),
		IntersectionHandling.hh(34449), LogicPlanner.cc(34449),
		LogicPlanner.hh(34449)
	Replaced getWaypoint and getHeading with graph functions.
	Implemented new checkClearance function (along graph) extended the
	region where obstacles are detected with precedence

Fri Aug 17 18:47:17 2007	vcarson (vcarson)

	* version R1-00x
	BUGS:  
	FILES: LogicPlanner.cc(34102)
	Changed getting into a zone until we are actually inside. 

Thu Aug 16 20:07:29 2007	Sven Gowal (sgowal)

	* version R1-00w
	BUGS:  
	FILES: LogicPlanner.cc(33985)
	Enabled zones

Wed Aug 15 17:53:37 2007	Sven Gowal (sgowal)

	* version R1-00v
	BUGS:  
	FILES: LogicPlanner.cc(33764)
	Fixed intersection bug + forcing Alice to stop to perform a Uturn

Tue Aug 14 19:27:36 2007	Christian Looman (clooman)

	* version R1-00u
	BUGS:  
	FILES: IntersectionHandling.cc(33557)
	Adjusted colors at intersections for better readability

Tue Aug 14 17:29:23 2007	Sven Gowal (sgowal)

	* version R1-00t
	BUGS:  
	FILES: LogicPlanner.cc(33418), LogicUtils.cc(33418)
	merged

	FILES: LogicPlanner.cc(33345), LogicUtils.cc(33345)
	Fixed at intersections

Tue Aug 14 12:10:05 2007	Noel duToit (ndutoit)

	* version R1-00s
	BUGS:  
	FILES: Makefile.yam(33361)
	Changed the makefile for the new cSpecs interface.

Tue Aug 14  9:50:28 2007	Christian Looman (clooman)

	* version R1-00r
	BUGS:  
	FILES: IntersectionHandling.cc(33284),
		IntersectionHandling.hh(33284), LogicPlanner.cc(33284),
		Makefile.yam(33284)
	Commit after merge

	FILES: IntersectionHandling.cc(33257),
		IntersectionHandling.hh(33257), LogicPlanner.cc(33257),
		Makefile.yam(33257)
	Integrated new states BACKUP and EMERGENCY STOP. Also updated some
	functions to work with the new console

Thu Aug  9 12:58:03 2007	Sven Gowal (sgowal)

	* version R1-00q
	BUGS:  
	FILES: LogicPlanner.cc(32808)
	merged

	FILES: LogicPlanner.cc(32809)
	merging

	FILES: LogicPlanner.cc(32801)
	Handling backup maneuvers

Thu Aug  9 11:31:25 2007	Christian Looman (clooman)

	* version R1-00p
	BUGS:  
	FILES: IntersectionHandling.cc(32758),
		IntersectionHandling.hh(32758), LogicUtils.hh(32758)
	Integrated intersection output to channel = 0

Wed Aug  8 17:26:05 2007	Christian Looman (clooman)

	* version R1-00o
	BUGS:  
	FILES: IntersectionHandling.cc(32641), LogicPlanner.cc(32641),
		LogicUtils.cc(32641), LogicUtils.hh(32641)
	Alice now stops at ROW intersections. left turn does not check for
	oncoming traffic yet

Wed Aug  8 11:25:34 2007	Christian Looman (clooman)

	* version R1-00n
	BUGS:  
	FILES: LogicPlanner.cc(32589), LogicUtils.cc(32589),
		LogicUtils.hh(32589)
	LogicPlanner handles left turns at ROW intersections. Not implement
	in IntersectionHandling yet

Fri Aug  3 20:27:27 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: LogicPlanner.cc(31860)
	merged

	FILES: LogicPlanner.cc(31848)
	minor fix

Fri Aug  3 18:36:59 2007	Christian Looman (clooman)

	* version R1-00l
	BUGS:  
	FILES: IntersectionHandling.conf(31821), LogicPlanner.cc(31821)
	Added sleep(1) after intersection clearance. Also modified
	IntersectionHandling.conf so that Alice gives precedence to cars
	further away from their intersection

Fri Aug  3  8:42:57 2007	Christian Looman (clooman)

	* version R1-00k
	BUGS:  
	FILES: IntersectionHandling.cc(31717)
	Merge

	FILES: IntersectionHandling.cc(31710)
	Minor bug

Fri Aug  3  0:40:03 2007	Sven Gowal (sgowal)

	* version R1-00j
	BUGS:  
	FILES: LogicPlanner.cc(31646)
	Fix UTURN bug

Tue Jul 31 20:04:22 2007	Sven Gowal (sgowal)

	* version R1-00i
	BUGS:  
	FILES: LogicPlanner.cc(31271), LogicUtils.cc(31271),
		LogicUtils.hh(31271)
	merged

	New files: IntersectionHandling.conf
	FILES: IntersectionHandling.cc(30871), Makefile.yam(30871)
	Adjusted a few lines so that the file IntersectionHandling.conf
	will be found again

	FILES: IntersectionHandling.cc(30913),
		IntersectionHandling.hh(30913), Makefile.yam(30913)
	Adjusted header so that prediction can access some of the functions

	FILES: LogicPlanner.cc(30853)
	Changed MAX_INTERSECTION_Q_LENGTH to reflect the fact that we stop
	at 2 VEHICLE_LENGTH behind an obstacle now

	FILES: LogicPlanner.cc(30859), LogicUtils.cc(30859),
		LogicUtils.hh(30859)
	Starting to handle stopped vehicle

	FILES: LogicPlanner.cc(30874), LogicUtils.cc(30874),
		LogicUtils.hh(30874)
	merged

Tue Jul 31 13:36:01 2007	Christian Looman (clooman)

	* version R1-00h
	BUGS:  
	New files: IntersectionHandling.conf
	FILES: IntersectionHandling.cc(31063),
		IntersectionHandling.hh(31063), Makefile.yam(31063)
	- Fixed bug that IntersectionHandling.conf wasn't found - adjusted
	populateWaypoints to work with Prediction as well

Fri Jul 27  9:35:14 2007	Sven Gowal (sgowal)

	* version R1-00g
	BUGS:  
	FILES: IntersectionHandling.cc(30391), LogicPlanner.cc(30391),
		LogicPlanner.hh(30391), LogicUtils.hh(30391)
	trickling through changes in temp-planner-interfaces.

	FILES: LogicPlanner.cc(30373), LogicPlanner.hh(30373),
		LogicUtils.cc(30373), LogicUtils.hh(30373)
	Added logic to report errors to mplanner

	FILES: LogicPlanner.cc(30509)
	completing UTurn now

Mon Jul 23 19:24:55 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: LogicPlanner.cc(30025), LogicUtils.cc(30025),
		LogicUtils.hh(30025)
	Added logic to reset the PASS flag

Thu Jul 19 18:40:07 2007	Sven Gowal (sgowal)

	* version R1-00e
	BUGS:  
	FILES: IntersectionHandling.cc(29600),
		IntersectionHandling.hh(29600)
	Fixed bug that new vehicles were given precedence even when Alice
	was stopped for quite a while already

	FILES: IntersectionHandling.cc(29609),
		IntersectionHandling.hh(29609)
	bug fixing

	FILES: IntersectionHandling.cc(29612)
	Adjusting functions to new plannig-stack

	FILES: IntersectionHandling.cc(29616)
	fixed bug that Intersection will get reset at the next intersection

	FILES: IntersectionHandling.cc(29625),
		IntersectionHandling.hh(29625)
	updated console information

	FILES: LogicPlanner.cc(29601), LogicUtils.cc(29601),
		LogicUtils.hh(29601)
	Added temporary solution to the stopline in the other lane problem

	FILES: LogicPlanner.cc(29605)
	Removed UTURN for testing today

	FILES: LogicPlanner.cc(29607)
	Removed the STOP_INT -> STOP_OBS transition for now

	FILES: LogicPlanner.cc(29610)
	Merging

	FILES: LogicPlanner.cc(29709)
	Merged

Wed Jul 18 19:03:55 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: LogicPlanner.cc(29519)
	Fixed collision checking to account for the new Err_t defines

Wed Jul 18 16:33:59 2007	Christian Looman (clooman)

	* version R1-00c
	BUGS:  
	FILES: IntersectionHandling.cc(29490),
		IntersectionHandling.hh(29490)
	Bug fixes

Wed Jul 18 12:30:06 2007	Sven Gowal (sgowal)

	* version R1-00b
	BUGS:  
	New files: ConfigFile.cc ConfigFile.hh IntersectionHandling.cc
		IntersectionHandling.hh LogicUtils.cc LogicUtils.hh
	FILES: LogicPlanner.cc(29397), LogicPlanner.hh(29397)
	Added basic logic

	FILES: LogicPlanner.cc(29401), LogicPlanner.hh(29401)
	Using AliceStateHelper now

	FILES: LogicPlanner.cc(29407)
	Switch to pause on end of mission

	FILES: LogicPlanner.cc(29415)
	Added intersection handling

	FILES: Makefile.yam(29408)
	added intersection handling to logic-planner

Fri Jul 13 16:10:23 2007	Sven Gowal (sgowal)

	* version R1-00a
	BUGS:  
	New files: LogicPlanner.cc LogicPlanner.hh
	FILES: Makefile.yam(28909)
	Created main files and functions for this library, but have not yet
	implemented the functionality.

Thu Jul 12 17:02:33 2007	Noel duToit (ndutoit)

	* version R1-00
	Created logic-planner module.












































































































































