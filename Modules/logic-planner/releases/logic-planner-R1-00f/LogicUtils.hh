#ifndef LOGICUTILS_HH_
#define LOGICUTILS_HH_

#include <frames/point2.hh>
#include <map/Map.hh>
#include <planner/AliceStateHelper.hh>
#include <gcinterfaces/SegGoals.hh>


class LogicUtils {

public: 

  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static LaneLabel getCurrentLane(VehicleState &vehState, Map *map);
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal);
  static double getAngleInRange(double angle);
  static double getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane);
  static double getNearestObsInLane(MapElement & me, VehicleState &vehState, Map *map, LaneLabel &lane); 

private :

};

#endif /*LOGICUTILS_HH_*/




