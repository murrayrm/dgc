#ifndef LOGICPLANNER_HH_
#define LOGICPLANNER_HH_

#include <vector>

#include "map/Map.hh"
#include "frames/pose3.h"
#include "frames/point2.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "interfaces/VehicleState.h"
#include "planner/CmdArgs.hh"

class LogicPlanner {

public: 

  static int init();
  static void destroy();
  static Err_t planLogic(vector<StateProblem_t> &problems, int prevErr, VehicleState &vehState, Map *map, Logic_params_t params);

private :

};

#endif /*LOGICPLANNER_HH_*/




