#ifndef LOGICPLANNER_HH_
#define LOGICPLANNER_HH_

#include <vector>

#include "map/Map.hh"
#include "frames/pose3.h"
#include "frames/point2.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
#include "interfaces/VehicleState.h"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "LogicUtils.hh"
#include "IntersectionHandling.hh"

class LogicPlanner {

public: 
  static int init();
  static void destroy();
  static Err_t planLogic(vector<StateProblem_t> &problems, Graph_t *graph, Err_t prevErr, VehicleState &vehState, Map *map, 
			 Logic_params_t &params, int& currentSegmentId, int& currentLaneId);
  static string stateToString(FSM_state_t state);
  static string flagToString(FSM_flag_t flag);
  static string plannerToString(FSM_planner_t planner);
  static string regionToString(FSM_region_t region);
  static string obstacleToString(FSM_obstacle_t obstacle);
  static void resetState();
  static void failureHandler(double stopped_duration, FSM_state_t &state, FSM_region_t &region, FSM_planner_t &planner, FSM_obstacle_t &obstacle, FSM_flag_t &flag, IntersectionHandling::IntersectionReturn inter_ret, bool &sendReplan, bool &sendBackup, bool &sendFail, int m_estop);

private:
  static FSM_state_t current_state;
  static FSM_flag_t current_flag;
  static FSM_region_t current_region;
  static FSM_planner_t current_planner;
  static FSM_obstacle_t current_obstacle;
};

#endif /*LOGICPLANNER_HH_*/




