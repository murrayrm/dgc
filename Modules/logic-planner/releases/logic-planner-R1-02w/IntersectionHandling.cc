
#include <sstream>
#include <iomanip>
#include <ncurses.h>
#include <cotk/cotk.h>

#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Log.hh"
#include "temp-planner-interfaces/Console.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include "alice/AliceConstants.h"
#include "dgcutils/cfgfile.h"

#include "ConfigFile.hh"
#include "IntersectionHandling.hh"

// #include "planner/cmdline.h"
using namespace std;

/* Initialize class constants */
double IntersectionHandling::DISTANCE_STOPLINE_ALICE=1;
double IntersectionHandling::VELOCITY_OBSTACLE_THRESHOLD=0.2;
double IntersectionHandling::DISTANCE_OBSTACLE_THRESHOLD=6;
double IntersectionHandling::ETA_EPS=1;
double IntersectionHandling::TRACKING_VEHICLE_SEPERATION=1.0;
int IntersectionHandling::WAIT_CYCLES=15;
double IntersectionHandling::INTERSECTION_TIMEOUT=45.0;
double IntersectionHandling::INTERSECTION_TIME_CLEARANCE = 3.0;
double IntersectionHandling::INTERSECTION_GROW_CORRIDOR = 2.0;
double IntersectionHandling::MAXIMUM_VELOCITY = 15.0;

time_t IntersectionHandling::timestamp;
vector<PrecedenceList_t> IntersectionHandling::PrecedenceList;
vector<MapElement> IntersectionHandling::ClearanceList;
vector<MapElement> IntersectionHandling::MergingList;
PointLabel IntersectionHandling::entryWaypoint;

IntersectionHandling::IntersectionReturn IntersectionHandling::ReturnValue;

vector<PointLabel> IntersectionHandling::WayPointsWithStop;
vector<IntersectionHandling::WayPoints_t> IntersectionHandling::WayPointsNoStop;
vector<PointLabel> IntersectionHandling::WayPointsEntries;
CMapElementTalker IntersectionHandling::testMap;
bool IntersectionHandling::JammedTimerStarted;
bool IntersectionHandling::FirstCheck;
int IntersectionHandling::mapcounter;
bool IntersectionHandling::clearset=false;
unsigned long long IntersectionHandling::cleartime;
unsigned long long IntersectionHandling::IntersectionStartTime;
double IntersectionHandling::IntersectionTimer;

Map* IntersectionHandling::localMap;
Graph_t* IntersectionHandling::graph;


void IntersectionHandling::init()
{
   char *path;

   path=dgcFindConfigFile("IntersectionHandling.conf","logic-planner");
   if (fopen(path,"r") == NULL)
    {
      Console::addMessage("Warning: Couldn't read Intersection configuration.");
      Log::getStream(1)<<"Warning: Couldn't read Intersection configuration."<<endl;
      Log::getStream(1)<<fopen("IntersectionHandling.conf","r")<<endl;
    }
  else
    {
      ConfigFile config(path);
      config.readInto(DISTANCE_STOPLINE_ALICE,"DISTANCE_STOPLINE_ALICE");
      config.readInto(VELOCITY_OBSTACLE_THRESHOLD,"VELOCITY_OBSTACLE_THRESHOLD");
      config.readInto(DISTANCE_OBSTACLE_THRESHOLD,"DISTANCE_OBSTACLE_THRESHOLD");
      config.readInto(ETA_EPS,"ETA_EPS");
      config.readInto(TRACKING_VEHICLE_SEPERATION,"TRACKING_VEHICLE_SEPERATION");
      config.readInto(WAIT_CYCLES,"WAIT_CYCLES");
      config.readInto(INTERSECTION_TIMEOUT,"INTERSECTION_TIMEOUT");
      config.readInto(INTERSECTION_TIME_CLEARANCE,"INTERSECTION_TIME_CLEARANCE");
      config.readInto(INTERSECTION_GROW_CORRIDOR,"INTERSECTION_GROW_CORRIDOR");
      config.readInto(MAXIMUM_VELOCITY,"MAXIMUM_VELOCITY");
      Log::getStream(1)<<"Info: Read Intersection configuration successfully."<<endl;
    }

   mapcounter = MAPSTART;
}

void IntersectionHandling::reset(Graph_t* g, Map* m)
{
  ReturnValue=RESET;

  // assign map and graph
  graph = g;
  localMap = m;

  // This resets the intersection, so the next time, a new intersection will be created
  WayPointsEntries.clear();
  WayPointsWithStop.clear();
  WayPointsNoStop.clear();
  
  PrecedenceList.clear();
  JammedTimerStarted=false;
  FirstCheck=false;
  clearset = false;
  
  // Initialize Debug-Map
  testMap.initSendMapElement(CmdArgs::sn_key);
  
  // clean up maviewer
  MapElement me;
  for (int i = MAPSTART; i<mapcounter; i++) {
    me.id = i;
    me.setTypeClear();
    testMap.sendMapElement(&me, sendChannel);
  }
  mapcounter = MAPSTART;

  IntersectionTimer = 0.0;

  updateConsole();
}

void IntersectionHandling::updateConsole()
{
  // Update Console

  int counter=0;
  for (unsigned int i=0; i<PrecedenceList.size(); i++)
      if (PrecedenceList[i].precedence)
        counter++;

  string str="";

  switch(ReturnValue)
    {
    case RESET:           str="RESET          "; break;
    case WAIT_PRECEDENCE: str="WAIT_PRECEDENCE"; break;
    case WAIT_MERGING:    str="WAIT_MERGING   "; break;
    case WAIT_CLEAR:      str="WAIT_CLEAR     "; break;
    case GO:              str="GO             "; break;
    case JAMMED:          str="JAMMED         "; break;
    }
  
  unsigned long long t;
  DGCgettime(t);

  Console::updateInter(counter,str,entryWaypoint,IntersectionTimer);
  Console::updateInterPage2(PrecedenceList, ClearanceList, MergingList);
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkIntersection(VehicleState vehState, SegGoals currSegment)
{
  // start Intersection Timer
  if (!FirstCheck)
    DGCgettime(IntersectionStartTime);

  // if Alice waits longer than the timeout value, give GO command
  unsigned long long t;
  DGCgettime(t);
  IntersectionTimer = (t - IntersectionStartTime)/1000000.0;
  if (IntersectionTimer > INTERSECTION_TIMEOUT)
    ReturnValue = GO;

  entryWaypoint = PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  // Create intersection if it didn't happen yet
  if (WayPointsEntries.size()==0) {
    // output some log information first
    time_t t;
    time(&t);
    Log::getStream(1)<<"Check Intersection starting with "<<entryWaypoint<<". Time "<<ctime(&t)<<endl;
    
    populateWayPoints(entryWaypoint);
    findStoplines(entryWaypoint);
    detectIntersectingTrafficWhileMerging(currSegment);
  }

  // If intersection created successfully
  if (WayPointsEntries.size()>0) {
    if (ReturnValue==RESET || ReturnValue==WAIT_PRECEDENCE) {
      // Check for current status of other obstacles.
      maintainPrecedenceList(vehState);
      ReturnValue=checkPrecedence();
      if (Console::queryResetIntersectionFlag())
        ReturnValue=WAIT_MERGING;
      Log::getStream(9)<<"Intersection Handling. Current State: "<<ReturnValue<<endl;
    }
    
    if (ReturnValue==WAIT_MERGING || ReturnValue==WAIT_CLEAR) {
      ReturnValue=checkMerging(currSegment);
      if (ReturnValue!=WAIT_CLEAR && Console::queryResetIntersectionFlag())
        ReturnValue=WAIT_CLEAR;
      Log::getStream(9)<<"Intersection Handling. Current State: "<<ReturnValue<<endl;
    }
      
    // While waiting for possibility to merge, do not start timer yet!
    if (ReturnValue==WAIT_MERGING)
      JammedTimerStarted=false;
    
    if (ReturnValue==WAIT_CLEAR) {
      ReturnValue=checkClearance(currSegment);
      if (Console::queryResetIntersectionFlag())
        ReturnValue=GO;
      Log::getStream(9)<<"Intersection Handling. Current State: "<<ReturnValue<<endl;
    }
  }
  else {
    // Output some debugging information
    Log::getStream(7)<<"Error: Couldn't build Intersection. Give GO. WayPoint "<<PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID)<<endl;
    
    stringstream s;
    s<<entryWaypoint;
    Console::addMessage("Error: Couldn't build Intersection. %s",s.str().c_str());
    
    ReturnValue=GO;
  }
  
  return ReturnValue;
}

void IntersectionHandling::populateWayPoints(PointLabel InitialWayPointEntry)
{
  vector<PointLabel> WayPointExits, WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++) {
    localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

    // Look whether WayPointEntry already exists in list
    for (unsigned int j=0; j<WayPoint.size(); j++) {
      bool found=false;
      for (unsigned int k=0; k<WayPointsEntries.size(); k++) {
        if (WayPoint[j]==WayPointsEntries[k]) {
          found=true;
          break;
        }
      }
      
      // If not, add it to list and call function recursivly
      if (!found) {
        WayPointsEntries.push_back(WayPoint[j]);

        populateWayPoints(WayPoint[j]);
      }
    }
  }
}

void IntersectionHandling::detectIntersectingTrafficWhileMerging(SegGoals currSegment)
{
  if (WayPointsNoStop.size() ==0)
    return;

  // create Alice's graph
  point2arr transitionAlice;
  getTransition(transitionAlice, PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID), PointLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID));
 
  // go through all entries found at this intersection
  for(unsigned int i=0; i<WayPointsNoStop.size(); i++) {
    vector<PointLabel> WayPointExits;
    localMap->getWayPointExits(WayPointExits,WayPointsNoStop[i].WayPoint);

    // loop through all exits for this entry and see whether possible transitions intersect with Alice
    bool intersect = false;
    for (unsigned int k=0; k<WayPointExits.size(); k++) {
      point2arr transitionTraffic;
      getTransition(transitionTraffic, WayPointsNoStop[i].WayPoint, WayPointExits[k]);

      point2 crosspoint;

      if (transitionAlice.is_intersect(transitionTraffic)) {
        intersect = true;
        break;
      }
    }
    WayPointsNoStop[i].monitorWhileMerging = intersect;
  }

  // a special case is when Alice turns right. If it tries to follow an unfeasible turn,
  // it should also monitor traffic coming from the right to prevent running into its lane while turning right
  if (currSegment.intersection_type == SegGoals::INTERSECTION_RIGHT) {
    // 
    vector<LaneLabel> oppLanes;
    localMap->getOppDirLanes(oppLanes, LaneLabel(currSegment.exitSegmentID, currSegment.exitLaneID));

    // check whether opposite lane has no stop line
    bool foundindex = -1;
    if (oppLanes.size()>0) {
      for (unsigned int l=0; oppLanes.size(); l++) {
        for (unsigned int k=0; WayPointsNoStop.size(); k++) {
          if (WayPointsNoStop[k].WayPoint.segment == oppLanes[l].segment && WayPointsNoStop[k].WayPoint.lane == oppLanes[l].lane) {
            foundindex=k;
            break;
          }
        }
      }
    }

    // if opposite has no stopline, check whether graph is feasible
    if (foundindex != -1) {
      bool feasible = false;

      if (checkFeasibility(feasible, PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID), PointLabel(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID)) == -1 || !feasible)
          WayPointsNoStop[foundindex].monitorWhileMerging = true;
    }
  }

  return;
}


int IntersectionHandling::checkFeasibility(bool result, PointLabel entryPointLabel, PointLabel exitPointLabel)
{
  point2arr transition, entryBound, exitBound;
  point2 p_entry, p_exit, projectEntry, projectExit;

  // get point2 from graph
  GraphNode* entry = graph -> getNodeFromRndfId(entryPointLabel.segment, entryPointLabel.lane, entryPointLabel.point);
  GraphNode* exit = graph -> getNodeFromRndfId(exitPointLabel.segment, exitPointLabel.lane, exitPointLabel.point);
  p_entry = point2(entry->pose.pos.x, entry->pose.pos.y);
  p_exit = point2(exit->pose.pos.x, exit->pose.pos.y);

  // create graph transition from entry to exit
  getTransition(transition, entryPointLabel, exitPointLabel);

  // obtain right projection point onto boundary from entry and exit
  localMap->getRightBound(entryBound,LaneLabel(entryPointLabel.segment, entryPointLabel.lane));
  localMap->getRightBound(exitBound,LaneLabel(exitPointLabel.segment, exitPointLabel.lane));

  localMap->getProjectToLine(projectEntry, entryBound, p_entry);
  localMap->getProjectToLine(projectExit, exitBound, p_exit);

  // create point2arr, extend it and get crosspoint
  point2arr tempEntryLine, tempExitLine, entryLine, exitLine;

  tempEntryLine.push_back(p_entry);
  tempEntryLine.push_back(projectEntry);
  localMap->extendLine(entryLine, tempEntryLine, 50);

  tempExitLine.push_back(p_exit);
  tempExitLine.push_back(projectExit);
  localMap->extendLine(exitLine, tempExitLine, 50);

  // if false, there is somethingw wrong
  point2 cpoint;
  if (!crossPoint(entryLine, exitLine, cpoint))
    return -1;
 
  // go along transition and check whether radius is always larger than ALICE_MIN_RADIUS
  for (unsigned int i=0; i<transition.size(); i++) {
    double distance = sqrt(pow(transition[i].x - cpoint.x, 2) + pow(transition[i].y - cpoint.y, 2));
    if (distance < VEHICLE_MIN_TURNING_RADIUS) {
      result = false;
      return 0;
    }
  }
         
  result = true;
  return 0;
}

void IntersectionHandling::findStoplines(PointLabel InitialWayPointEntry)
{
  MapElement me;
  point2 p;
  stringstream s;
  
  for (unsigned int i=0; i<WayPointsEntries.size(); i++) {

    // Do not check Alice lane
    if (WayPointsEntries[i] == InitialWayPointEntry)
      continue;

    if (localMap->isStopLine(WayPointsEntries[i])) {
      // Store information in vector
      WayPointsWithStop.push_back(WayPointsEntries[i]);
      
      // Send stopline to map, color red
      GraphNode* node = graph -> getNodeFromRndfId(WayPointsEntries[i].segment, WayPointsEntries[i].lane, WayPointsEntries[i].point);
      point2 p = point2(node->pose.pos.x, node->pose.pos.y);

      // send area that will be checked for precedence
      LaneLabel lane = LaneLabel(WayPointsEntries[i].segment, WayPointsEntries[i].lane);
        
      // get centerline for this waypoint
      point2arr centerline;
        
      point2 p1_l, p1_r;
      localMap->getLaneLeftPoint(p1_l, lane, p, 0);
      localMap->getLaneRightPoint(p1_r, lane, p, 0);
      
      point2 p2_l, p2_r;
      localMap->getLaneLeftPoint(p2_l, lane, p,-DISTANCE_OBSTACLE_THRESHOLD);
      localMap->getLaneRightPoint(p2_r, lane, p,-DISTANCE_OBSTACLE_THRESHOLD);
      
      point2arr parr;
      parr.push_back(p1_l);
      parr.push_back(p1_r);
      parr.push_back(p2_l);
      parr.push_back(p2_r);
      
      // paint box
      me.id = mapcounter++;
      me.setTypePoly();
      me.setGeometry(parr);
      me.setColor(MAP_COLOR_ORANGE,100);
      testMap.sendMapElement(&me,sendChannel);
    }
    else {
      // Store information in vector
      WayPoints_t temp;
      temp.WayPoint = WayPointsEntries[i];
      temp.monitorWhileMerging = false;
      WayPointsNoStop.push_back(temp);	
    }
  }
}

void IntersectionHandling::maintainPrecedenceList(VehicleState vehState)
{
  point2 p_stopline,p2,position_obstacle;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,distance;
  PrecedenceList_t list_temp;
  int foundindex;
  vector<PrecedenceList_t> tempPrecedenceList;
  int CounterObstacle=0;
  MapElement obstacleMap;
  stringstream s;
  
  // Reset updated-flag
  for (unsigned int k=0; k<PrecedenceList.size(); k++)
    PrecedenceList[k].updated=false;

  CounterObstacle=0;

  // Look for obstacles at those waypoints with stopsigns
  for (unsigned int i=0; i<WayPointsWithStop.size(); i++) {
    GraphNode* node = graph -> getNodeFromRndfId(WayPointsWithStop[i].segment, WayPointsWithStop[i].lane, WayPointsWithStop[i].point);
    point2 p_stopline = point2(node->pose.pos.x, node->pose.pos.y);
    lane = LaneLabel(WayPointsWithStop[i].segment, WayPointsWithStop[i].lane);
    localMap->getLaneCenterLine(centerline,lane);
    localMap->getObsInLane(obstacle,lane);


    // Loop through all obstacles found in lane
    for (unsigned int j=0;j<obstacle.size();j++) {
      if (!obstacle[j].isObstacle())
        continue;

      distance_geometry=INFINITY;
      // Get distance from stopline to closest point of obstacle
      for (unsigned l=0; l<obstacle[j].geometry.size();l++) {
        position_obstacle.set(obstacle[j].geometry[l]);
        localMap->getDistAlongLine(distance_temp, centerline, p_stopline, position_obstacle);
        if (distance_temp < distance_geometry)
          distance_geometry = distance_temp;
      }
      
          // Look within 30m of stopline
      if (distance_geometry>-1.0 && distance_geometry<30) {
        CounterObstacle++;
        
        // Look whether vehicle already exists
        foundindex=-1;
        
        // Store information that this vehicle still exist in list and won't be deleted later by "garbage collector"
        for (unsigned int k=0; k<PrecedenceList.size(); k++) {
          if (PrecedenceList[k].element.id==obstacle[j].id) {
            PrecedenceList[k].updated=true;
            PrecedenceList[k].element = obstacle[j];
            PrecedenceList[k].lane = lane;
            PrecedenceList[k].distance = distance_geometry;
            foundindex=k;
          }
        }
        
        if (foundindex==-1 && !FirstCheck) {
          // Create list object
          list_temp.element=obstacle[j];
          list_temp.WayPoint=WayPointsWithStop[i];
          list_temp.velocity=min(sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2)), MAXIMUM_VELOCITY);
          list_temp.distance=distance_geometry;
          list_temp.precedence=false;
          list_temp.checkedQueuing=false;
          list_temp.lane = lane;
          list_temp.updated=true;
          list_temp.updateCounter = 0;
		      
          // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<.5m/s
          if (list_temp.velocity>=VELOCITY_OBSTACLE_THRESHOLD)
            list_temp.eta=list_temp.distance/list_temp.velocity;
          else if (list_temp.distance>=DISTANCE_OBSTACLE_THRESHOLD)
            list_temp.eta=INFINITY;
          else
            list_temp.eta=0;
          
          // store new vehicle
          PrecedenceList.push_back(list_temp);
          
          Log::getStream(7)<<"Obstacle "<<obstacle[j].id<<" stored"<<endl;
          Log::getStream(7)<<"distance "<<list_temp.distance<<endl;
          Log::getStream(7)<<"velocity "<<list_temp.velocity<<endl;
          Log::getStream(7)<<"ETA "<<list_temp.eta<<endl;
        }
      }
    }
  }
  
  // This prevents that new vehicles will be added to list after Alice came to a stopline
  FirstCheck=true;

  int found;

  // Clean up PrecedenceList "Garbage collector"
  for (unsigned int k=0; k<PrecedenceList.size(); k++) {
    // obstacle must disappear for a defined time (some planner cycles) until it gets removed. This will avoid that
    // flickering obstacles are deleted wrongly
    if (!PrecedenceList[k].updated)
      PrecedenceList[k].updateCounter++;
    else
      PrecedenceList[k].updateCounter = 0;

    // If obstacle seems to disappear, do some checks first before deleting this vehicle
    if (!PrecedenceList[k].updated && PrecedenceList[k].updateCounter >= WAIT_CYCLES) {
      // the obstacle might have changed the ID. So look for obstacles that are really close to the old ID. If so, assign this new object to the PrecedenceList
      // obtain position of old obstacle and get the lane that is is supposed to be 
      found=-1;
      distance = INFINITY;
      localMap->getObsNearby(obstacle,PrecedenceList[k].element.position, 5);
      for (unsigned int m=0; m<obstacle.size(); m++) {
        if (obstacle[m].id == PrecedenceList[k].element.id)
          continue;
        distance_temp = sqrt(pow(PrecedenceList[k].element.position.x - obstacle[m].position.x, 2) + pow(PrecedenceList[k].element.position.y - obstacle[m].position.y, 2));
        
        if (distance_temp<distance) {
          distance=fabs(distance_temp);
          found=m;
        }
      }
      
      // if there is an obstacle nearby, then replace the old obstacle with this new one
      if (found!=-1 && distance<TRACKING_VEHICLE_SEPERATION) {
        Console::addInterMessage("Found obstacle nearby. Do not delete in list");
        PrecedenceList[k].element=obstacle[found];
        PrecedenceList[k].updated=true;
      }

      // If there is something blocking the view, keep the obstacle that seemed to disappear
      if (PrecedenceList[k].precedence && !checkVisibility(vehState,PrecedenceList[k].element))
        PrecedenceList[k].updated=true;
    }  

    // only delete object when it is not updated
    if (!PrecedenceList[k].updated && PrecedenceList[k].updateCounter >= WAIT_CYCLES)
      Log::getStream(7)<<"Remove obstacle at WayPoint..."<<PrecedenceList[k].WayPoint<<"... ObstacleID..."<<PrecedenceList[k].element.id<<endl;
    else
      tempPrecedenceList.push_back(PrecedenceList[k]);
  }
  PrecedenceList=tempPrecedenceList;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkPrecedence()
{
  int precedence_counter=0;
  MapElement me;

  // All Vehicles with a very small ETA have precedence
  for (unsigned int i=0; i<PrecedenceList.size(); i++) {
    // only the vehicle with the closest distance to each waypoint shall have precedence
    if (!PrecedenceList[i].checkedQueuing) {
      PrecedenceList[i].closest=true;
      
      // get obstacle that is the closest to the stopline
      for (unsigned k=0; k<PrecedenceList.size(); k++) {
        // If vehicles are at same waypoint and one is further away than the other, uncheck flag
        if ((PrecedenceList[i].WayPoint==PrecedenceList[k].WayPoint) && (PrecedenceList[i].distance>PrecedenceList[k].distance))
          PrecedenceList[i].closest=false;
      }
      
      // If vehicle is not flagged as "closest" and is within 3m to the closest obstacle than also give this obstacle
      // precedence as it should be the same obstacle
      if (!PrecedenceList[i].closest) {
        for (unsigned z=0; z<PrecedenceList.size(); z++) {
          if ((PrecedenceList[i].WayPoint==PrecedenceList[z].WayPoint) && (PrecedenceList[z].closest) && (PrecedenceList[i].distance-PrecedenceList[z].distance<3.0))
              PrecedenceList[i].closest=true;
        }
      }
	    
      // Set flag so that this check is only done once per vehicle
      PrecedenceList[i].checkedQueuing=true;
    }
    
    // If ETA is so small that it should have precedence and is the first vehicle in queue, then this
    // vehicle is flagged with precedene
    if (PrecedenceList[i].eta<ETA_EPS && PrecedenceList[i].closest) {
      precedence_counter++;
      PrecedenceList[i].precedence=true;
    }
    else
      PrecedenceList[i].precedence=false;
  }
  

  //if there are other vehicles with precedence, return false
  if (precedence_counter>0)
    return WAIT_PRECEDENCE;
  else
    return WAIT_MERGING;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkMerging(SegGoals currSegment)
{
  point2 p_stopline,p_exit,position_obstacle;
  LaneLabel lane,lane_exit;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,velocity;
  MergingList.clear();

  for (unsigned int i=0; i<WayPointsNoStop.size(); i++) {
    // if lane has no possible intersecting traffic, skip checking this lane
    if (!WayPointsNoStop[i].monitorWhileMerging)
      continue;

    // get lane of waypoint that we are looping through
    GraphNode* node = graph -> getNodeFromRndfId(WayPointsNoStop[i].WayPoint.segment, WayPointsNoStop[i].WayPoint.lane, WayPointsNoStop[i].WayPoint.point);
    point2 p_stopline = point2(node->pose.pos.x, node->pose.pos.y);
    lane = LaneLabel(WayPointsNoStop[i].WayPoint.segment, WayPointsNoStop[i].WayPoint.lane);
    localMap->getLaneCenterLine(centerline,lane);
    localMap->getObsInLane(obstacle,lane);
    
    // Loop through all obstacles found in lane
    for (unsigned int j=0;j<obstacle.size();j++) {
      if (!obstacle[j].isVehicle())
        continue;
      
      distance_geometry=INFINITY;
      // Get distance from waypoint to closest point of obstacle
      for (unsigned l=0; l<obstacle[j].geometry.size();l++) {
        position_obstacle.set(obstacle[j].geometry[l]);
        localMap->getDistAlongLine(distance_temp, centerline, p_stopline, position_obstacle);
        if (distance_temp > 0.0 && distance_temp < distance_geometry)
          distance_geometry = distance_temp;
      }

      // DARPA requires to have a 10s gap
      velocity=min(sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2)), MAXIMUM_VELOCITY);
      
      // check whether vehicle is approaching the intersection (condition >0) and whether it is far away enough
      if (distance_geometry>0 && distance_geometry<10*velocity) {
        MergingList.push_back(obstacle[j]);
        return WAIT_MERGING;
      }
    }
  }
  
  return WAIT_CLEAR;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkClearance(SegGoals currSegment)
{
  point2arr centerline;
  vector<PointLabel> WayPointExits;
  vector<MapElement> obstacles, obstaclesAlongGraph;
  int cabmode_counter=0;
  int moving_counter=0;
  point2arr leftBound, rightBound, leftBoundAlongGraph, rightBoundAlongGraph;
  MapElement me;
  LaneLabel lane_exit;
  stringstream s;

  ClearanceList.clear();

  // start timer to make sure that Alice doesn't brake the 10s rule
  if (!JammedTimerStarted) {
    time(&timestamp);
    JammedTimerStarted=true;
  }
  
  // get current WayPointEntry and WayPointEntry of Alice
  PointLabel currentWayPointEntry(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  PointLabel currentWayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

  // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
  for (unsigned i=0; i<WayPointsEntries.size(); i++) {
    // get all exits for this entry
    localMap->getWayPointExits(WayPointExits,WayPointsEntries[i]);

    for (unsigned j=0;j<WayPointExits.size(); j++) {
      // Create TransitionBound between this WayPointEntry and WayPointExit
      if (getTransitionAlongGraph(leftBound, WayPointsEntries[i], WayPointExits[j], currSegment, false, -1) == -1)
        Log::getStream(1)<<"Error in getTransition. Entry "<<WayPointsEntries[i]<<". Exit "<<WayPointExits[j]<<". Left Bound"<<endl;
      if (getTransitionAlongGraph(rightBound, WayPointsEntries[i], WayPointExits[j], currSegment, false, 1) == 1)
        Log::getStream(1)<<"Error in getTransition. Entry "<<WayPointsEntries[i]<<". Exit "<<WayPointExits[j]<<". Right Bound"<<endl;
      
      // Get all obstacles within these bounds
      localMap->getObsInBounds(obstacles, leftBound, rightBound);
      
      // Send left bound to map
      me.id = mapcounter++;
      me.setTypeLine();
      me.setColor(MAP_COLOR_GREEN,100);
      me.setGeometry(leftBound);
      testMap.sendMapElement(&me,sendChannel);
      
      // Send right bound to map
      me.id = mapcounter++;
      me.setGeometry(rightBound);
      testMap.sendMapElement(&me,sendChannel);
       
      for (unsigned int k=0; k<obstacles.size(); k++) {
        if (!obstacles[k].isObstacle())
          continue;

        // add obstacle to internal list; mainly for the console for right now
        bool f = false;
        for (unsigned int l=0; l<ClearanceList.size(); l++)
          if (ClearanceList[l].id == obstacles[k].id)
            f = true;
        if (!f)
          ClearanceList.push_back(obstacles[k]);

        // if obstacles blocks, check if one of the following conditions is true
        // 1) static obstacle
        // 2) moving obstacle stopped longer than 10 seconds
        // 3) moving obstacle that is slower than 0.2 m/s
        // then check whether this obstacle blocks Alice's Corridor
        double v = sqrt(pow(obstacles[k].velocity.x,2) + pow(obstacles[k].velocity.y,2));
        if ((obstacles[k].isVehicle() && !obstacles[k].isPredicted() && obstacles[k].timeStopped>=10.0) || (obstacles[k].isVehicle() && !obstacles[k].isPredicted() && v<= VELOCITY_OBSTACLE_THRESHOLD) || (obstacles[k].isObstacle() && !obstacles[k].isVehicle())) {

          Log::getStream(1)<<"Intersection Handling: Check along Graph because type = "<<obstacles[k].type<<" and stopped = "<<obstacles[k].timeStopped<<endl;
          if (getTransitionAlongGraph(leftBoundAlongGraph, currentWayPointEntry, currentWayPointExit, currSegment, true, -1) == -1)
            Log::getStream(1)<<"Error in getTransitionAlongGraph. Entry "<<currentWayPointEntry<<". Exit "<<currentWayPointExit<<". Left Bound"<<endl;
          if (getTransitionAlongGraph(rightBoundAlongGraph, currentWayPointEntry, currentWayPointExit, currSegment, true, 1) == -1)
            Log::getStream(1)<<"Error in getTransitionAlongGraph. Entry "<<currentWayPointEntry<<". Exit "<<currentWayPointExit<<". Right Bound"<<endl;
          
          // Send left bound to map
          me.id = mapcounter++;
          me.setTypeLine();
          me.setColor(MAP_COLOR_ORANGE,100);
          me.setGeometry(leftBoundAlongGraph);
          s.str("");
          s<<"Left bound "<<currentWayPointEntry<<"->"<<currentWayPointExit;
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);
      
          // Send right bound to map
          me.id = mapcounter++;
          me.setGeometry(rightBoundAlongGraph);
          s.str("");
          s<<"Right bound "<<currentWayPointEntry<<"->"<<currentWayPointExit;
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);

          // query all obstacles within this boundary ...
          localMap->getObsInBounds(obstaclesAlongGraph, leftBoundAlongGraph, rightBoundAlongGraph);
          // ... and check whether this obstacle is there
          for (unsigned int o=0; o<obstaclesAlongGraph.size(); o++)
            if (obstaclesAlongGraph[o].id == obstacles[k].id)
              cabmode_counter ++;
        }
        else {
          Log::getStream(1)<<"Intersection Handling. This is a moving obstacle. Type "<< obstacles[k].type <<" Stopped = "<<obstacles[k].timeStopped<<" Pos "<<obstacles[k].position<<endl;
          moving_counter++;
        }
      }
    }
  }
  
  // check whether Alice needs to switch in cab mode
  time_t currentTime;
  time(&currentTime);
  double diff=difftime(currentTime,timestamp);

  if (moving_counter == 0 && cabmode_counter == 0) {
    if (!clearset) {
      DGCgettime(cleartime);
      clearset = true;
    }

    unsigned long long temptime;
    DGCgettime(temptime);
    if ((temptime - cleartime)/1000000.0 > INTERSECTION_TIME_CLEARANCE)
      return GO;
    else
      return WAIT_CLEAR;
  }
  else if (moving_counter==0 && cabmode_counter>0 && diff>=10.0)
    return JAMMED;

  clearset = false;
  return WAIT_CLEAR;
}

bool IntersectionHandling::checkVisibility(VehicleState vehState, MapElement obstacle)
{
  double maxRight=-INFINITY;
  double maxLeft=INFINITY;
  point2 maxRightPoint2,maxLeftPoint2;

  // Obtain the max/min points of the obstacle's geometry
  for (unsigned int i=0; i<obstacle.geometry.size(); i++)
    {
      if (obstacle.geometry[i].x>maxRight)
        {
          maxRight=obstacle.geometry[i].x;
          maxRightPoint2.set(obstacle.geometry[i]);
        }
      if (obstacle.geometry[i].x<maxLeft)
        {
          maxLeft=obstacle.geometry[i].x;
          maxLeftPoint2.set(obstacle.geometry[i]);
        }
    }
  
  // Obtain front left and front right point of Alice
  point2 bumper=AliceStateHelper::getPositionFrontBumper(vehState);
  double heading = AliceStateHelper::getHeading(vehState);
  point2 frontLeft=point2(bumper.x+sin(heading)*VEHICLE_WIDTH/2,bumper.y-cos(vehState.localYaw)*VEHICLE_WIDTH/2);
  point2 frontRight=point2(bumper.x-sin(heading)*VEHICLE_WIDTH/2,bumper.y+cos(vehState.localYaw)*VEHICLE_WIDTH/2);

  point2arr leftBound,rightBound;
  leftBound.push_back(frontLeft);
  leftBound.push_back(maxLeftPoint2);
  rightBound.push_back(frontRight);
  rightBound.push_back(maxRightPoint2);

  MapElement me;

  // Get all obstacles within these bounds
  vector<MapElement> obst;
  localMap->getObsInBounds(obst, leftBound, rightBound);
  
  if (obst.size()>0)
    {
      double distObstacle=INFINITY;
      double dist=INFINITY;
      double distTemp;
      
      // calculate distance between the obstacle that we are looking at and Alice
      // this is independant from whether this obstacle does still exist or is gone already
      point2 alice = AliceStateHelper::getPositionFrontBumper(vehState);
      for (unsigned int k=0; k<obstacle.geometry.size(); k++)
        {
          distTemp=sqrt(pow(obstacle.geometry[k].x-alice.x,2)+pow(obstacle.geometry[k].y-alice.y,2));
          if (distTemp<distObstacle)
            distObstacle=distTemp;
        }
  
      for (unsigned int i=0; i<obst.size(); i++)
        {
          if (obst[i].id!=obstacle.id)
            {
              for (unsigned int l=0; l<obst[i].geometry.size(); l++)
                {
                  distTemp=sqrt(pow(obst[i].geometry[l].x-alice.x,2)+pow(obst[i].geometry[l].y-alice.y,2));
                  if (distTemp<dist)
                    dist=distTemp;
                }
            }
        }

      // If any other obstacle is closer, return false as it seems to be in betweenthe obstacle that we are looking at and Alice
      if (dist<distObstacle)
        {
          Console::addInterMessage("Warning: Obstacle blocking view");
          return false;
        }
      else
        return true;
    }
  else
    return true;
}

bool IntersectionHandling::crossPoint(point2arr a, point2arr b, point2 &crosspoint)
{
  for (unsigned int ai = 1; ai<a.size(); ai++) {
    for (unsigned int bi = 1; bi<b.size(); bi++) {
      double ax=a[ai-1].x;
      double ay=a[ai-1].y;
      double bx=a[ai].x;
      double by=a[ai].y;

      double cx=b[bi-1].x;
      double cy=b[bi-1].y;
      double dx=b[bi].x;
      double dy=b[bi].y;


      double v = ((cx-ax) * (by-ay) - (cy-ay) * (bx-ax)) / ((dy-cy) * (bx-ax) - (dx-cx) * (by-ay));

      crosspoint.x = cx + v * (dx-cx);
      crosspoint.y = cy + v * (dy-cy);


      bool result_a = inSection(a[ai-1],a[ai], crosspoint);
      bool result_b = inSection(b[bi-1],b[bi], crosspoint);
      
      if (result_a && result_b) return true;
    }
  }
  return false;
}

bool IntersectionHandling::inSection(point2 a, point2 b, point2 x)
{
  double v = (x.x - a.x) / (b.x - a.x);
  if (v>=0 && v<=1) return true;
  return false;
}


int IntersectionHandling::getTransitionAlongGraph(point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, SegGoals currSegment, bool grow, int leftright)
{
  Vehicle *vp;
  Maneuver *mp;  
  point2 projectEntry, projectExit, p_entry, p_exit;
  Pose2D pose_entry, pose_exit;
  VehicleConfiguration config;
  LaneLabel entryLane, exitLane;
  point2arr entryBound, exitBound;
  
  // get point2 of WaypointEntry and WaypointExit
  GraphNode* entry = graph -> getNodeFromRndfId(WaypointEntry.segment, WaypointEntry.lane, WaypointEntry.point);
  GraphNode* exit = graph -> getNodeFromRndfId(WaypointExit.segment, WaypointExit.lane, WaypointExit.point);

  // get the lanes of entry and exit
  p_entry = point2(entry->pose.pos.x, entry->pose.pos.y);
  p_exit = point2(exit->pose.pos.x, exit->pose.pos.y);
  entryLane = LaneLabel(WaypointEntry.segment, WaypointEntry.lane);
  exitLane = LaneLabel(WaypointExit.segment, WaypointExit.lane);

  // get the left and right points of entry/exit
  if (leftright == -1) {
    localMap->getLeftBound(entryBound,entryLane);
    localMap->getLeftBound(exitBound,exitLane);
  }
  else if (leftright == 1) {
    localMap->getRightBound(entryBound,entryLane);
    localMap->getRightBound(exitBound,exitLane);
  }
  localMap->getProjectToLine(projectEntry, entryBound, p_entry);
  localMap->getProjectToLine(projectExit, exitBound, p_exit);

  // for safety reasons, grow this corridor to the left and right
  point2arr extendEntryTemp, extendEntry, extendExitTemp, extendExit;

  extendEntryTemp.push_back(p_entry);
  extendEntryTemp.push_back(projectEntry);
  extendExitTemp.push_back(p_exit);
  extendExitTemp.push_back(projectExit);

  localMap->extendLine(extendEntry, extendEntryTemp, INTERSECTION_GROW_CORRIDOR / 2.0);
  localMap->extendLine(extendExit, extendExitTemp, INTERSECTION_GROW_CORRIDOR / 2.0);

  // If left turn, grow right side of corridor only
  // If right turn, grow left side of corridor only
  // If Straigt, don't grow corridor

  if (grow && ((currSegment.intersection_type == SegGoals::INTERSECTION_LEFT && leftright == 1) || (currSegment.intersection_type == SegGoals::INTERSECTION_RIGHT && leftright == -1))) {
    projectEntry = extendEntry.back();
    projectExit = extendExit.back();
  }

  pose_entry.x = projectEntry.x;
  pose_entry.y = projectEntry.y;

  double r, p;
  quat_to_rpy(entry->pose.rot, &r, &p, &pose_entry.theta);
  quat_to_rpy(exit->pose.rot, &r, &p, &pose_exit.theta);
  pose_exit.x = projectExit.x;
  pose_exit.y = projectExit.y;

  // Vehicle properties
  vp = maneuver_create_vehicle( VEHICLE_WHEELBASE, M_PI/2 );
    // Create maneuver object
  mp = maneuver_pose2pose(vp, &pose_entry, &pose_exit);
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // create boundaries
  bound.clear();
  int numSteps = 20;
  for (int i = 1; i < numSteps + 1; i++) 
  {
    double s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    point2 p=point2(config.x, config.y);
    bound.push_back(p);
  }
  return 0;
}

int IntersectionHandling::getTransition(point2arr& transition, PointLabel entryPointLabel, PointLabel exitPointLabel)
{
  Pose2D pose_entry, pose_exit;

  GraphNode* entry = graph -> getNodeFromRndfId(entryPointLabel.segment, entryPointLabel.lane, entryPointLabel.point);
  GraphNode* exit = graph -> getNodeFromRndfId(exitPointLabel.segment, exitPointLabel.lane, exitPointLabel.point);

  double r, p;
  quat_to_rpy(entry->pose.rot, &r, &p, &pose_entry.theta);
  quat_to_rpy(exit->pose.rot, &r, &p, &pose_exit.theta);


  // Vehicle properties
  Vehicle *vp;
  vp = maneuver_create_vehicle( VEHICLE_WHEELBASE, M_PI/2 );

  // Create maneuver object
  Maneuver *mp;  
  pose_entry.x = entry->pose.pos.x;
  pose_entry.y = entry->pose.pos.y;
  pose_exit.x = exit->pose.pos.x;
  pose_exit.y = exit->pose.pos.y;

  mp = maneuver_pose2pose(vp, &pose_entry, &pose_exit);
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // create boundaries
  transition.clear();
  int numSteps = 20;
  for (int i = 1; i < numSteps + 1; i++) 
  {
    double s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    VehicleConfiguration config;
    config = maneuver_evaluate_configuration(vp, mp, s);    
    point2 p=point2(config.x, config.y);
    transition.push_back(p);
  }
  return 0;
}
