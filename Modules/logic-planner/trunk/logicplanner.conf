// Initialize class constants
DESIRED_DECELERATION = 0.25

// Parameters for defining intersection region
INTERSECTION_BUFFER = 2
MAX_INTERSECTION_Q_LENGTH = 30
DIST_NOCHECK_OBS_STOPINT = 1
TIME_CLEAR_INTERSECTION = 45.0
MAX_NUM_CHECK_INTERSECTION = 1

// Safety box around Alice
ALICE_SAFETY_FRONT = 1.0
ALICE_SAFETY_REAR = 0.5
ALICE_SAFETY_SIDE = 0.5

// Whether we allow BACKUP after STOPOBS_PASS or STOPOBS_PASS_REV
ALLOW_BACKUP = 0

// The number of times we can backup before switching into uturn
// This is only used when failure handling is not activated.
MAX_NUM_BACKUP = 3

// Parameter that defines how far different backup can be
MIN_DIST_BETWEEN_DIFF_BACKUP = 30.0

// The number of times we can backup before start using s1planner in ROAD_REGION
// This is only used when failure handling is activated.
MAX_NUM_S1PLANNER_BACKUP = 1

// Do we want to use rail planner in BACKUP mode
BACKUP_WITH_RAIL_PLANNER = 0

// Whether we allow DRIVE_PASS_REV
USE_RAIL_PLANNER_REV = 1

// The number of times we can transition to DRIVE_PASS_REV before switching into backup
MAX_NUM_DRIVE_PASS_REV = 3
MAX_NUM_DRIVE_AGG = 2
MAX_NUM_DRIVE_BARE = 2

// Parameter that defines how far different backup can be
MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV = 15.0

// How long we need to stop before starting passing maneuver
TIMEOUT_OBS_PASSING = 3.0

// How long we need to stop before starting passing maneuver while having multiple lanes
TIMEOUT_OBS_PASSING_EXTENDED = 17.0

// How long we need to stop before starting passing maneuver near intersection
TIMEOUT_OBS_INT = 20.0

// The maximum time we can stop in the STOP_INT mode
TIMEOUT_STOPINT = 30.0

// How long we need to stop before starting uturn maneuver
MIN_TIME_STOP_BEFORE_UTURN = 7.0

// How long obstacle has to disappear before we think that it doesn't exist.
// This is to prevent logic planner from switching back and forth
// before DRIVE and STOP_OBS too quickly when obstacle is flickering in and out.
MIN_TIME_OBS_DISAPPEAR = 1.5

// How long we need to be in STOP_OBS state before transitioning to a worse state
MIN_TIME_STOPOBS_SHORT = 5.0
MIN_TIME_STOPOBS_LONG = 6.0

// How long we need to stay in DRIVE_PASS before switching to DRIVE_NOPASS
MIN_TIME_PASSING = 8.0
MIN_DIST_PASSING = 20.0

// How long we wait before reporting the second time that we're lost
MIN_TIME_LOST = 10.0

// How long we need to stop before starting using s1planner in ROAD_REGION
MIN_TIME_STOP_BEFORE_S1PLANNER = 5.0

// How long we will use s1planner in ROAD_REGION before switching back to rail planner
MAX_TIME_USE_S1PLANNER = 40.0
MAX_DIST_USE_S1PLANNER = 40.0
MIN_TIME_IN_DRIVE_S1PLANNER = 3.0

// How long we will use other type of obstacle before switching back  to obstacle safety
TIMEOUT_USE_SMALL_OBS = 40.0
MAX_DIST_USE_SMALL_OBS = 50.0

// How long we can be in non-nominal mode
FH_TIMEOUT_NONNOMINAL = 22.0
FH_MAX_DISTANCE_NONNOMINAL = 50.0

// Timeout before failure handling starts doing something
FH_MAX_STOP_TIME = 25.0

// Timeout between two FH actions
FH_MAX_STOP_TIME_BETWEEN_FH = 8.0

// Timeout when we're waiting for a vehicle before we can pass an obstacle
FH_MIN_TIME_WAIT_TO_PASS = 30.0
FH_DIST_CHECK_VEH_PASSING = 60.0

// Whether we want to change obstacle size when we use rail planner
NUM_CHANGE_RP_OBS_SIZE = 0
MIN_TIME_STOP_BEFORE_CHANGE_OBS = 3.0

// Use S1PLANNER
USE_S1PLANNER_ONLY = 0
USE_S1PLANNER_ON_ROAD = 0

// Go to OFFROAD mode when rail planner returns PP_NOPATH_LEN
USE_OFFROAD = 0
TIMEOUT_NOPATHLEN = 2.0
TIMEOUT_OFFROAD = 15.0
MIN_DIST_OFFROAD = 15.0

// Go to OFFROAD when a one lane road is blocked
USE_OFFROAD_ONELANE = 0

// Go to OFFROAD when we're in zone
USE_OFFROAD_ZONE = 0

// Specify which zone ID we want to use RAIL_PLANNER_NOPASS, RAIL_PLANNER_OFFROAD
ZONE_ID_USE_DRIVE_NOPASS = 43
ZONE_ID_USE_OFFROAD = 42