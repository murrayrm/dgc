#ifndef LOGICPLANNER_HH_
#define LOGICPLANNER_HH_

#include <vector>

#include "interfaces/VehicleState.h"
#include "map/Map.hh"
// REMOVE #include "frames/pose3.h"
// REMOVE #include "frames/point2.hh"
#include "temp-planner-interfaces/PlannerInterfaces.h"
// REMOVE #include "temp-planner-interfaces/CmdArgs.hh"
#include "LogicUtils.hh"
#include "IntersectionHandling.hh"

typedef enum {LP_DRIVE_NOPASS, 
	      LP_STOPOBS_NOPASS, 
	      LP_DRIVE_PASS, 
	      LP_STOPOBS_PASS,
	      LP_DRIVE_PASS_REV,
	      LP_STOPOBS_PASS_REV,
	      LP_BACKUP, 
	      LP_DRIVE_PASS_BACKUP, 
	      LP_STOPOBS_PASS_BACKUP, 
	      LP_DRIVE_AGG, 
	      LP_STOPOBS_AGG, 
	      LP_DRIVE_BARE, 
	      LP_STOPOBS_BARE,
	      LP_DRIVE_S1PLANNER_SAFETY, 
	      LP_DRIVE_S1PLANNER_AGG, 
	      LP_DRIVE_S1PLANNER_BARE, 
	      LP_UTURN, 
	      LP_PAUSE, 
	      LP_STOPINT,
              LP_ZONE_SAFETY, 
	      LP_ZONE_AGG, 
	      LP_ZONE_BARE,
              LP_OFFROAD_SAFETY,
	      LP_STOPOBS_OFFROAD_SAFETY,
              LP_OFFROAD_AGG,
	      LP_STOPOBS_OFFROAD_AGG,
              LP_OFFROAD_BARE,
              LP_STOPOBS_OFFROAD_BARE,
              LP_STOPINT_OFFROAD} LP_state_t;

class LogicPlanner {

public: 
  static int init();
  static void readConfig();
  static void destroy();
  static Err_t planLogic(vector<StateProblem_t*> &problems_p, PlanGraph *graph, Err_t prevErr, 
			 VehicleState &vehState, Map *map, 
			 Logic_params_t &params, int& currentSegmentId, int& currentLaneId,
			 bool& replanInZone);
  static string lpstateToString(LP_state_t state);
  static string stateToString(FSM_state_t state);
  static string flagToString(FSM_flag_t flag);
  static string plannerToString(FSM_planner_t planner);
  static string regionToString(FSM_region_t region);
  static string obstacleToString(FSM_obstacle_t obstacle);
  static void updateIntersectionHandlingConsole();
  static void resetState();
  static void failureHandler(FSM_state_t &state, FSM_region_t &region, 
			     FSM_planner_t &planner, FSM_obstacle_t &obstacle, 
			     FSM_flag_t &flag, CIntersectionHandling::IntersectionReturn inter_ret, 
			     bool &sendReplan, bool &sendBackup, bool &sendFail, bool reset);

private:
  static void getStatesFromLPState(LP_state_t lp_state, FSM_state_t &state,
			    FSM_flag_t &flag, FSM_planner_t &planner, 
			    FSM_obstacle_t &obstacle, LP_state_t paused_lp_state);
  static void getLPStatesFromStates(LP_state_t& lp_state, FSM_state_t state,
				    FSM_flag_t flag, FSM_planner_t planner, 
				    FSM_obstacle_t obstacle, FSM_region_t region, 
				    bool just_finished_backup);
  static void startNewState(VehicleState &vehState, bool& replanInZone);
			    
  static FSM_state_t current_state;
  static FSM_flag_t current_flag;
  static FSM_region_t current_region;
  static FSM_planner_t current_planner;
  static FSM_obstacle_t current_obstacle;

  // variable that keeps track of how long we've been in the current state
  static uint64_t start_current_lp_state_time;
  static double start_current_lp_state_xpos;
  static double start_current_lp_state_ypos;

  // Parameters
  static double DESIRED_DECELERATION;
  static double INTERSECTION_BUFFER;
  static double MAX_INTERSECTION_Q_LENGTH;
  static double DIST_NOCHECK_OBS_STOPINT;
  static double TIME_CLEAR_INTERSECTION;
  static int MAX_NUM_CHECK_INTERSECTION;
  static double ALICE_SAFETY_FRONT;
  static double ALICE_SAFETY_REAR;
  static double ALICE_SAFETY_SIDE;
  static bool ALLOW_BACKUP;
  static int MAX_NUM_BACKUP;
  static double MIN_DIST_BETWEEN_DIFF_BACKUP;
  static bool BACKUP_WITH_RAIL_PLANNER;
  static bool USE_RAIL_PLANNER_REV;
  static int MAX_NUM_DRIVE_PASS_REV;
  static int MAX_NUM_DRIVE_AGG;
  static int MAX_NUM_DRIVE_BARE;
  static double MIN_DIST_BETWEEN_DIFF_DRIVE_PASS_REV;
  static int MAX_NUM_S1PLANNER_BACKUP;

  // Timeout
  static double TIMEOUT_OBS_PASSING;
  static double TIMEOUT_OBS_PASSING_EXTENDED;
  static double TIMEOUT_OBS_INT;
  static double TIMEOUT_STOPINT;
  static double MIN_TIME_STOP_BEFORE_UTURN;
  static double MIN_TIME_OBS_DISAPPEAR;
  static double MIN_TIME_STOPOBS_SHORT;
  static double MIN_TIME_STOPOBS_LONG;
  static double MIN_TIME_PASSING;
  static double MIN_DIST_PASSING;
  static double MIN_TIME_LOST;

  // For s1planner
  static double MIN_TIME_STOP_BEFORE_S1PLANNER;
  static double MAX_TIME_USE_S1PLANNER;
  static double MAX_DIST_USE_S1PLANNER;
  static double MIN_TIME_IN_DRIVE_S1PLANNER;

  // For DRIVE_AGG or DRIVE_BARE
  static double TIMEOUT_USE_SMALL_OBS;
  static double MAX_DIST_USE_SMALL_OBS;

  // For failure handling: make sure that we keep moving
  static double FH_TIMEOUT_NONNOMINAL;
  static double FH_MAX_DISTANCE_NONNOMINAL;
  static double FH_MAX_STOP_TIME;
  static double FH_MAX_STOP_TIME_BETWEEN_FH;
  static double FH_MIN_TIME_WAIT_TO_PASS;
  static double FH_DIST_CHECK_VEH_PASSING;

  // Whether we want to change obstacle size when we use rail planner
  static int NUM_CHANGE_RP_OBS_SIZE;
  static double MIN_TIME_STOP_BEFORE_CHANGE_OBS;

  // Use S1PLANNER
  static bool USE_S1PLANNER_ONLY;
  static bool USE_S1PLANNER_ON_ROAD;

  // Go to OFFROAD mode when rail planner returns PP_NOPATH_LEN
  static bool USE_OFFROAD;
  static double TIMEOUT_NOPATHLEN;
  static double TIMEOUT_OFFROAD;
  static double MIN_DIST_OFFROAD;

  // Go to OFFROAD when a one lane road is blocked
  static bool USE_OFFROAD_ONELANE;

  // Go to OFFROAD when we're in zone
  static bool USE_OFFROAD_ZONE;

  // Specify which zone ID we want to use RAIL_PLANNER_NOPASS, RAIL_PLANNER_OFFROAD
  static int ZONE_ID_USE_DRIVE_NOPASS;
  static int ZONE_ID_USE_OFFROAD;

  // Intersection Handler
  static CIntersectionHandling* IntersectionHandling;
};

#endif /*LOGICPLANNER_HH_*/




