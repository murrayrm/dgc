#ifndef LOGICUTILS_HH_
#define LOGICUTILS_HH_

//#include "mapping/Location.hh"
#include "frames/point2.hh"
#include "map/Map.hh"
#include "planner/AliceStateHelper.hh"


class LogicUtils {

public: 

  static bool isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane);
  static bool isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound);
  static double getDistToStopline(VehicleState &vehState, Map *map);
private :

};

#endif /*LOGICUTILS_HH_*/




