introduced in new versions of the "logic-planner" module. Thisinformation should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "logic-planner" module can be found in
the ChangeLog file.

Release R1-01z-ndutoit2 (Fri Sep 21 16:37:44 2007):
	Implemented the new fields in the state problem. No new functionality.

Release R1-01z (Tue Sep 18 19:01:05 2007):
	Outputs information about other vehicles into console while merging.
	Decrease the time waiting for clearance at intersection from 3 to 2 seconds

Release R1-01y (Mon Sep 17 20:57:32 2007):
	Added INTERSECTION_TIMEOUT (default 45 seconds). If Alice waits longer than this value at
	the intersection, it tries to go through; no matter whether other vehicles might have 
	precedence.

Release R1-01x (Thu Sep 13 17:57:03 2007):
	Do not check for precedence in Alice lane. Also, using 
isObstacle/isVehicle functions now

Release R1-01w (Tue Sep 11 21:42:03 2007):
	Alice waits at intersections for 2s to verify that intersection is clear and no moving obstacles are blocking the intersection.
	
Release R1-01v (Tue Sep 11 13:47:55 2007):
	Small bug in transition, only affected output in mapviewer

Release R1-01u (Tue Sep 11 13:11:57 2007):
	Now adds information to console about obstacles that affect Intersection handling

Release R1-01t (Mon Sep 10 12:33:31 2007):
 	Changed condition for entering into a zone by simply checking that Alice positions wrt the rear axle 
is inside the zone. 

Release R1-01s (Mon Sep 10  9:24:54 2007):
	Fixed bugs in IntersectionHandling.

Release R1-01r (Tue Sep  4  1:17:52 2007):
	Makes sure that entire Alice is in zone before switching to ZONE. 

Release R1-01q (Sun Sep  2 11:49:30 2007):
	Removed Alice safety box
	Added a transition from STOP_INT to DRIVE

Release R1-01p (Sun Sep  2 11:13:52 2007):
	Fixed bug so that IntersectionHandling won't use the SegGoals from the SegGoalsQueue if they are messed 
up

Release R1-01o (Fri Aug 31 17:11:45 2007):
	Added condition to not double check the same intersection

Release R1-01n (Thu Aug 30 18:13:07 2007):
	Reduced the safety box around Alice a little.

Release R1-01m (Thu Aug 30 17:23:21 2007):
	Updates the distance to the next stopline in the planner 
	console. This will help debugging if Alice does not stop at a 
	stopline or does not want to go through

Release R1-01l (Thu Aug 30 10:57:07 2007):
	Removed mapviewer output created by unittest.

Release R1-01k (Wed Aug 29 22:07:57 2007):
	Changed Unittest to test intersections in different RNDFs, 
	especially the algorithm to check for clearance

Release R1-01j (Tue Aug 28 17:14:35 2007):
	Added safety feature that brings Alice to a stop when there is an obstacle in a safety region around Alice.

Release R1-01i (Tue Aug 28 19:00:09 2007):
	Changed interface to getDistToStopline again.

Release R1-01h (Tue Aug 28 13:59:23 2007):
	Dont know why yam wants this release...

Release R1-01g (Tue Aug 28  9:25:17 2007):
	1.) IntersectionHandling works with Zones.
	2.) IntersectionHandling waits for a defined number of cycles until it deletes obstacles out of its internal 
	precedence list. The number of cycles are defined in WAIT_CYCLES in IntersectionHandling.conf
	This should improve dyn. obstacle handling when obstacles are flickering in the map

Release R1-01f (Mon Aug 27  9:00:51 2007):
	Moved the LogicUtils methods into a file in temp-planner-interfaces so they are accessible by the planning stack.  
Release R1-01e (Thu Aug 23 17:06:48 2007):
	Fixed bug so planner does not switch into STOP_INT right after passing the 
	stopline of an intersection and gets stuck.

Release R1-01d (Thu Aug 23 15:56:51 2007):
	Changed the way how to stop at stoplines at to go through. Now, it pulls the 
	location of stoplines from the graph. Some debugging is still necessary. At least, 
	others can continue working on zones now.

Release R1-01c (Wed Aug 22 18:51:46 2007):
	Removed output in mapviewer while checking for visibility.
	Fixed bug in getDistToStop so we also get negative distances to stoplines.
	Fixed bug when switching into STOP_INT at left turns of ROW intersections.

Release R1-01b (Wed Aug 22 17:55:16 2007):
	Stopping when first entrering a zone

Release R1-01a (Wed Aug 22 15:30:40 2007):
	Increased threshold for obstacles decected as being stopped at intersections.

Release R1-01 (Tue Aug 21 19:13:03 2007):
	Fixed SegFault at intersections. Also, IntersectionHandling will 
	be only called if distance to stopline <1m

Release R1-00z (Tue Aug 21  6:18:18 2007):
	Changed debugging output channel from 0 which is reserved for
	perceptors to -2 which is the standard debugging channel

Release R1-00y (Mon Aug 20 21:43:04 2007):
	Replaced getHeading/getWaypoint functions with graph functions. Increased the zone where vehicles will be detected having precedence.

	Added functionality to corridor check. First the whole intersection will be checked for obstacles. Obstacles that did not move for more than 10 seconds and do not
	block the corridor that Alice wants to got, are neglected. If this kind of obstacle is blocking Alice's corridor, the function returns CABMODE after 10 seconds (if CABMODE_FLAG is set). 
	Moving obstacles will always be tracked no matter where they are within the intersection.

Release R1-00x (Fri Aug 17 18:47:20 2007):
	Changed condition for which we determine that we are in a Zone by checking if the position of 
our rear axle is inside the polygon delimited by the zone perimeter.  

Release R1-00w (Thu Aug 16 20:07:32 2007):
	Enabled zones

Release R1-00v (Wed Aug 15 17:53:41 2007):
	Fixed intersection bug
	Forcing Alice to stop to perform a Uturn

Release R1-00u (Tue Aug 14 19:27:39 2007):
	Brighten up colors at intersections in mapviewer to increase readability

Release R1-00t (Tue Aug 14 10:55:01 2007):
	Fixed bug causing Alice to freeze at intersections

Release R1-00s (Tue Aug 14 12:10:15 2007):
	Updated makefile to reflect changes in temp-planner-interfaces	

Release R1-00r (Tue Aug 14  9:24:15 2007):
	The new console allows to reset the current FSM State and to overwrite manually the intersection handling. The keyboard commands that can be
	used for that are shown within the () in the Console

Release R1-00q (Thu Aug  9 12:50:41 2007):
	UTURN is now divided in BACKUP/UTURN. The car will first backup then check for a possible passing maneuver, if no passing maneuver can be 
made, it will UTURN.

Release R1-00p (Thu Aug  9 11:31:32 2007):
	Specific mapviewer output from intersection handling will be output now on the regular mapviewer (channel 0). Also removed a bug that 
	prevented others from compiling it correctly (hehe).

Release R1-00o (Wed Aug  8 17:26:12 2007):
	Correct bug in integration of left turns at ROW intersections.

Release R1-00n (Wed Aug  8 11:25:47 2007):
	Integrated handling of ROF intersections in LogicPlanner. Implementation in IntersectionHandling not done yet.

Release R1-00m (Fri Aug  3 20:22:05 2007):
	Minor fix (when path cannot be found because of high cost)

Release R1-00l (Fri Aug  3 18:37:03 2007):
	Added sleep(1) after intersection clearance and modified the IntersectionHandling.conf.
	Cars that are within 10m of their stoplines will be given precedence.

Release R1-00k (Fri Aug  3  8:40:17 2007):
	fixed bug in Map output

Release R1-00j (Fri Aug  3  0:40:08 2007):
	Fixed UTURN bug

Release R1-00i (Tue Jul 31 19:40:41 2007):
	Added logic to report errors to mplanner
        Knows when UTurn gets completed

Release R1-00h (Tue Jul 31 13:36:07 2007):
	Fixed bug in IntersectionHandling. The configuration file IntersectionHandling.conf will be found now.
	Changed function populateWaypoints so that the module Prediction also has access to it. 

Release R1-00g (Fri Jul 27  9:35:24 2007):
	Added logic to handle passing maneuver and UTurns

Release R1-00f (Mon Jul 23 19:25:01 2007):
	Added some logic to handle obstacles appearing before intersections

Release R1-00e (Thu Jul 19 18:40:16 2007):
	Handling intersection, pause and end of mission now

Release R1-00d (Wed Jul 18 19:03:58 2007):
	Fixed path-planner error handling

Release R1-00c (Wed Jul 18 16:34:03 2007):
	Added intersection output to the console

Release R1-00b (Wed Jul 18 12:30:14 2007):
	Added basic logic to handle everything we had at the sitevisit (except UTURN), this assume that PathPlanner returns a PP_COLLISION error if 
an obstacle is blocking the lane

Release R1-00a (Fri Jul 13 16:10:26 2007):
	Added basic functionality in order to start coding

Release R1-00 (Thu Jul 12 17:02:33 2007):
	Created.





































































