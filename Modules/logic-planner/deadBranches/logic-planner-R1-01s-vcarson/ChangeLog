Mon Sep 10 12:33:28 2007	vcarson (vcarson)

	* version R1-01s-vcarson
	BUGS:  
	FILES: LogicPlanner.cc(38058)
	Different checking for switching to ZONE state.  Simply checking if
	rear axle is inside zone polygon.  

Mon Sep 10  9:28:09 2007	Christian Looman (clooman)

	* version R1-01s
	BUGS:  
	FILES: IntersectionHandling.cc(38025),
		IntersectionHandling.conf(38025),
		IntersectionHandling.hh(38025), LogicPlanner.cc(38025)
	Commit after merge

	FILES: IntersectionHandling.cc(38018),
		IntersectionHandling.conf(38018),
		IntersectionHandling.hh(38018), LogicPlanner.cc(38018)
	Fixed thousand bugs.

Tue Sep  4  1:17:49 2007	vcarson (vcarson)

	* version R1-01r
	BUGS:  
	FILES: LogicPlanner.cc(37403)
	Change to get into zone when Alice is completely in zone. 

Sun Sep  2 17:33:07 2007	Sven Gowal (sgowal)

	* version R1-01q
	BUGS:  
	FILES: LogicPlanner.cc(37107)
	Merged

	FILES: LogicPlanner.cc(37109)
	Avoid double checking the same intersection

	FILES: LogicPlanner.cc(37089)
	Removed safety box + added STOP_INT->DRIVE transition

Sun Sep  2 11:17:43 2007	Christian Looman (clooman)

	* version R1-01p
	BUGS:  
	FILES: IntersectionHandling.cc(37072), LogicPlanner.cc(37072),
		LogicUtils.cc(37072)
	Commit after merge

	FILES: IntersectionHandling.cc(37065), LogicPlanner.cc(37065),
		LogicUtils.cc(37065)
	Fixed bug that IntersectionHandling used SegGoals when SegGoalQueue
	was messed up

Fri Aug 31 17:14:15 2007	Sven Gowal (sgowal)

	* version R1-01o
	BUGS:  
	FILES: LogicPlanner.cc(36878)
	Merged

	FILES: LogicPlanner.cc(36871)
	Added condition to not double check the same intersection

Thu Aug 30 18:15:06 2007	Noel duToit (ndutoit)

	* version R1-01n
	BUGS:  
	FILES: LogicPlanner.cc(36698)
	merged with the latest release

	FILES: LogicPlanner.cc(36686)
	Reduced the safety box around alice a little.

Thu Aug 30 17:23:18 2007	Christian Looman (clooman)

	* version R1-01m
	BUGS:  
	FILES: LogicPlanner.cc(36622)
	Updates the distance to stopline in the console

Thu Aug 30 10:57:03 2007	Christian Looman (clooman)

	* version R1-01l
	BUGS:  
	FILES: IntersectionHandling.cc(36486)
	removed debug output from mapviewer again

	FILES: IntersectionHandling.cc(36487)
	removed more output

Wed Aug 29 22:11:56 2007	Christian Looman (clooman)

	* version R1-01k
	BUGS:  
	FILES: IntersectionHandling.cc(36467),
		IntersectionHandling.conf(36467), UT_logicplanner.cc(36467)
	Commit after merge

	FILES: IntersectionHandling.cc(36460),
		IntersectionHandling.conf(36460), UT_logicplanner.cc(36460)
	Changed UnitTest to test different RNDFs more detailed

Tue Aug 28 19:32:49 2007	Noel duToit (ndutoit)

	* version R1-01j
	BUGS:  
	New files: LogicUtils.cc LogicUtils.hh
	FILES: LogicPlanner.cc(36063), LogicPlanner.hh(36063),
		Makefile.yam(36063)
	merged with latest release

	New files: LogicUtils.cc LogicUtils.hh
	FILES: LogicPlanner.cc(36014), LogicPlanner.hh(36014),
		Makefile.yam(36014)
	Added back the logic utils files. Added function that pauses Alice
	when there is an obstacle very close (inside our safety box).

	FILES: LogicPlanner.cc(36016)
	minor change

Tue Aug 28 19:00:05 2007	Christian Looman (clooman)

	* version R1-01i
	BUGS:  
	FILES: LogicPlanner.cc(36049)
	Interaction with getDistToStopline

Tue Aug 28 13:59:20 2007	Christian Looman (clooman)

	* version R1-01h
	BUGS:  
	FILES: IntersectionHandling.cc(35942)
	changed getLeftBound/getRightBound to
	getLaneLeftBound/getLaneRightBound

Tue Aug 28 11:26:30 2007	Christian Looman (clooman)

	* version R1-01g
	BUGS:  
	FILES: IntersectionHandling.cc(35856), LogicPlanner.cc(35856)
	Intersections now work with zones as well

	FILES: IntersectionHandling.cc(35870),
		IntersectionHandling.conf(35870),
		IntersectionHandling.hh(35870), LogicPlanner.cc(35870)
	IntersectionHandling will wait for a defined amount of cycles until
	it deletes obstacles out of its internal list

	FILES: IntersectionHandling.cc(35856), LogicPlanner.cc(35856)
	Intersections now work with zones as well

Mon Aug 27  9:00:46 2007	vcarson (vcarson)

	* version R1-01f
	BUGS:  
	Deleted files: LogicUtils.cc LogicUtils.hh
	FILES: IntersectionHandling.cc(35426), LogicPlanner.cc(35426),
		Makefile.yam(35426)
	Removed LogicUtils and put functions in
	temp-planner-interfaces/Utils.hh.  These functions need to be used
	from planner as well as logic planner.	  

Thu Aug 23 17:06:39 2007	Christian Looman (clooman)

	* version R1-01e
	BUGS:  
	FILES: LogicPlanner.cc(35305), LogicUtils.cc(35305)
	Fixed bug that planner does not switch into STOP_INT right after
	passing the stopline

Thu Aug 23 15:56:42 2007	Christian Looman (clooman)

	* version R1-01d
	BUGS:  
	FILES: LogicPlanner.cc(35258), LogicUtils.cc(35258),
		LogicUtils.hh(35258)
	Changed the way how to stop at stoplines. Also works with zones
	reliable. Some debugging is still necessary but at least, people
	can continue working on zones

Wed Aug 22 18:56:31 2007	Christian Looman (clooman)

	* version R1-01c
	BUGS:  
	FILES: IntersectionHandling.cc(35027), LogicPlanner.cc(35027),
		LogicUtils.cc(35027)
	Commit after Merge

	FILES: IntersectionHandling.cc(35020), LogicPlanner.cc(35020),
		LogicUtils.cc(35020)
	Removed mapviewer output when checking visibility. Removed bug in
	getDistToStopline and when changing into STOP_INT at left turns
	(ROW intersections)

Wed Aug 22 17:55:12 2007	Sven Gowal (sgowal)

	* version R1-01b
	BUGS:  
	FILES: LogicPlanner.cc(34988)
	Pause when first going into a zone

Wed Aug 22 15:30:37 2007	Christian Looman (clooman)

	* version R1-01a
	BUGS:  
	FILES: IntersectionHandling.conf(34936)
	changed obstacle_velocity_threshold to 0.5 in
	IntersectionHandling.conf

Tue Aug 21 19:12:58 2007	Christian Looman (clooman)

	* version R1-01
	BUGS:  
	FILES: IntersectionHandling.cc(34764), LogicPlanner.cc(34764),
		UT_logicplanner.cc(34764)
	Fixed SegFault at intersection. Only calls IntersectionHandling
	when close to stopline

Tue Aug 21  6:18:14 2007	Sam Pfister (sam)

	* version R1-00z
	BUGS:  
	FILES: IntersectionHandling.cc(34546),
		IntersectionHandling.hh(34546)
	Changed debugging output channel from 0 which is reserved for
	perceptors to -2 which is the standard debugging channel

Mon Aug 20 23:44:45 2007	Christian Looman (clooman)

	* version R1-00y
	BUGS:  
	New files: UT_logicplanner.cc
	FILES: IntersectionHandling.cc(34497),
		IntersectionHandling.hh(34497), LogicPlanner.cc(34497),
		LogicPlanner.hh(34497), Makefile.yam(34497)
	Commit after merge

	New files: UT_logicplanner.cc
	FILES: IntersectionHandling.cc(33843, 34088),
		IntersectionHandling.hh(33843, 34088), Makefile.yam(33843,
		34088)
	backup

	FILES: IntersectionHandling.cc(34449),
		IntersectionHandling.hh(34449), LogicPlanner.cc(34449),
		LogicPlanner.hh(34449)
	Replaced getWaypoint and getHeading with graph functions.
	Implemented new checkClearance function (along graph) extended the
	region where obstacles are detected with precedence

Fri Aug 17 18:47:17 2007	vcarson (vcarson)

	* version R1-00x
	BUGS:  
	FILES: LogicPlanner.cc(34102)
	Changed getting into a zone until we are actually inside. 

Thu Aug 16 20:07:29 2007	Sven Gowal (sgowal)

	* version R1-00w
	BUGS:  
	FILES: LogicPlanner.cc(33985)
	Enabled zones

Wed Aug 15 17:53:37 2007	Sven Gowal (sgowal)

	* version R1-00v
	BUGS:  
	FILES: LogicPlanner.cc(33764)
	Fixed intersection bug + forcing Alice to stop to perform a Uturn

Tue Aug 14 19:27:36 2007	Christian Looman (clooman)

	* version R1-00u
	BUGS:  
	FILES: IntersectionHandling.cc(33557)
	Adjusted colors at intersections for better readability

Tue Aug 14 17:29:23 2007	Sven Gowal (sgowal)

	* version R1-00t
	BUGS:  
	FILES: LogicPlanner.cc(33418), LogicUtils.cc(33418)
	merged

	FILES: LogicPlanner.cc(33345), LogicUtils.cc(33345)
	Fixed at intersections

Tue Aug 14 12:10:05 2007	Noel duToit (ndutoit)

	* version R1-00s
	BUGS:  
	FILES: Makefile.yam(33361)
	Changed the makefile for the new cSpecs interface.

Tue Aug 14  9:50:28 2007	Christian Looman (clooman)

	* version R1-00r
	BUGS:  
	FILES: IntersectionHandling.cc(33284),
		IntersectionHandling.hh(33284), LogicPlanner.cc(33284),
		Makefile.yam(33284)
	Commit after merge

	FILES: IntersectionHandling.cc(33257),
		IntersectionHandling.hh(33257), LogicPlanner.cc(33257),
		Makefile.yam(33257)
	Integrated new states BACKUP and EMERGENCY STOP. Also updated some
	functions to work with the new console

Thu Aug  9 12:58:03 2007	Sven Gowal (sgowal)

	* version R1-00q
	BUGS:  
	FILES: LogicPlanner.cc(32808)
	merged

	FILES: LogicPlanner.cc(32809)
	merging

	FILES: LogicPlanner.cc(32801)
	Handling backup maneuvers

Thu Aug  9 11:31:25 2007	Christian Looman (clooman)

	* version R1-00p
	BUGS:  
	FILES: IntersectionHandling.cc(32758),
		IntersectionHandling.hh(32758), LogicUtils.hh(32758)
	Integrated intersection output to channel = 0

Wed Aug  8 17:26:05 2007	Christian Looman (clooman)

	* version R1-00o
	BUGS:  
	FILES: IntersectionHandling.cc(32641), LogicPlanner.cc(32641),
		LogicUtils.cc(32641), LogicUtils.hh(32641)
	Alice now stops at ROW intersections. left turn does not check for
	oncoming traffic yet

Wed Aug  8 11:25:34 2007	Christian Looman (clooman)

	* version R1-00n
	BUGS:  
	FILES: LogicPlanner.cc(32589), LogicUtils.cc(32589),
		LogicUtils.hh(32589)
	LogicPlanner handles left turns at ROW intersections. Not implement
	in IntersectionHandling yet

Fri Aug  3 20:27:27 2007	Sven Gowal (sgowal)

	* version R1-00m
	BUGS:  
	FILES: LogicPlanner.cc(31860)
	merged

	FILES: LogicPlanner.cc(31848)
	minor fix

Fri Aug  3 18:36:59 2007	Christian Looman (clooman)

	* version R1-00l
	BUGS:  
	FILES: IntersectionHandling.conf(31821), LogicPlanner.cc(31821)
	Added sleep(1) after intersection clearance. Also modified
	IntersectionHandling.conf so that Alice gives precedence to cars
	further away from their intersection

Fri Aug  3  8:42:57 2007	Christian Looman (clooman)

	* version R1-00k
	BUGS:  
	FILES: IntersectionHandling.cc(31717)
	Merge

	FILES: IntersectionHandling.cc(31710)
	Minor bug

Fri Aug  3  0:40:03 2007	Sven Gowal (sgowal)

	* version R1-00j
	BUGS:  
	FILES: LogicPlanner.cc(31646)
	Fix UTURN bug

Tue Jul 31 20:04:22 2007	Sven Gowal (sgowal)

	* version R1-00i
	BUGS:  
	FILES: LogicPlanner.cc(31271), LogicUtils.cc(31271),
		LogicUtils.hh(31271)
	merged

	New files: IntersectionHandling.conf
	FILES: IntersectionHandling.cc(30871), Makefile.yam(30871)
	Adjusted a few lines so that the file IntersectionHandling.conf
	will be found again

	FILES: IntersectionHandling.cc(30913),
		IntersectionHandling.hh(30913), Makefile.yam(30913)
	Adjusted header so that prediction can access some of the functions

	FILES: LogicPlanner.cc(30853)
	Changed MAX_INTERSECTION_Q_LENGTH to reflect the fact that we stop
	at 2 VEHICLE_LENGTH behind an obstacle now

	FILES: LogicPlanner.cc(30859), LogicUtils.cc(30859),
		LogicUtils.hh(30859)
	Starting to handle stopped vehicle

	FILES: LogicPlanner.cc(30874), LogicUtils.cc(30874),
		LogicUtils.hh(30874)
	merged

Tue Jul 31 13:36:01 2007	Christian Looman (clooman)

	* version R1-00h
	BUGS:  
	New files: IntersectionHandling.conf
	FILES: IntersectionHandling.cc(31063),
		IntersectionHandling.hh(31063), Makefile.yam(31063)
	- Fixed bug that IntersectionHandling.conf wasn't found - adjusted
	populateWaypoints to work with Prediction as well

Fri Jul 27  9:35:14 2007	Sven Gowal (sgowal)

	* version R1-00g
	BUGS:  
	FILES: IntersectionHandling.cc(30391), LogicPlanner.cc(30391),
		LogicPlanner.hh(30391), LogicUtils.hh(30391)
	trickling through changes in temp-planner-interfaces.

	FILES: LogicPlanner.cc(30373), LogicPlanner.hh(30373),
		LogicUtils.cc(30373), LogicUtils.hh(30373)
	Added logic to report errors to mplanner

	FILES: LogicPlanner.cc(30509)
	completing UTurn now

Mon Jul 23 19:24:55 2007	Sven Gowal (sgowal)

	* version R1-00f
	BUGS:  
	FILES: LogicPlanner.cc(30025), LogicUtils.cc(30025),
		LogicUtils.hh(30025)
	Added logic to reset the PASS flag

Thu Jul 19 18:40:07 2007	Sven Gowal (sgowal)

	* version R1-00e
	BUGS:  
	FILES: IntersectionHandling.cc(29600),
		IntersectionHandling.hh(29600)
	Fixed bug that new vehicles were given precedence even when Alice
	was stopped for quite a while already

	FILES: IntersectionHandling.cc(29609),
		IntersectionHandling.hh(29609)
	bug fixing

	FILES: IntersectionHandling.cc(29612)
	Adjusting functions to new plannig-stack

	FILES: IntersectionHandling.cc(29616)
	fixed bug that Intersection will get reset at the next intersection

	FILES: IntersectionHandling.cc(29625),
		IntersectionHandling.hh(29625)
	updated console information

	FILES: LogicPlanner.cc(29601), LogicUtils.cc(29601),
		LogicUtils.hh(29601)
	Added temporary solution to the stopline in the other lane problem

	FILES: LogicPlanner.cc(29605)
	Removed UTURN for testing today

	FILES: LogicPlanner.cc(29607)
	Removed the STOP_INT -> STOP_OBS transition for now

	FILES: LogicPlanner.cc(29610)
	Merging

	FILES: LogicPlanner.cc(29709)
	Merged

Wed Jul 18 19:03:55 2007	Sven Gowal (sgowal)

	* version R1-00d
	BUGS:  
	FILES: LogicPlanner.cc(29519)
	Fixed collision checking to account for the new Err_t defines

Wed Jul 18 16:33:59 2007	Christian Looman (clooman)

	* version R1-00c
	BUGS:  
	FILES: IntersectionHandling.cc(29490),
		IntersectionHandling.hh(29490)
	Bug fixes

Wed Jul 18 12:30:06 2007	Sven Gowal (sgowal)

	* version R1-00b
	BUGS:  
	New files: ConfigFile.cc ConfigFile.hh IntersectionHandling.cc
		IntersectionHandling.hh LogicUtils.cc LogicUtils.hh
	FILES: LogicPlanner.cc(29397), LogicPlanner.hh(29397)
	Added basic logic

	FILES: LogicPlanner.cc(29401), LogicPlanner.hh(29401)
	Using AliceStateHelper now

	FILES: LogicPlanner.cc(29407)
	Switch to pause on end of mission

	FILES: LogicPlanner.cc(29415)
	Added intersection handling

	FILES: Makefile.yam(29408)
	added intersection handling to logic-planner

Fri Jul 13 16:10:23 2007	Sven Gowal (sgowal)

	* version R1-00a
	BUGS:  
	New files: LogicPlanner.cc LogicPlanner.hh
	FILES: Makefile.yam(28909)
	Created main files and functions for this library, but have not yet
	implemented the functionality.

Thu Jul 12 17:02:33 2007	Noel duToit (ndutoit)

	* version R1-00
	Created logic-planner module.













































































