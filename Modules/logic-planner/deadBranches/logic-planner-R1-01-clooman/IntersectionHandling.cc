#include "IntersectionHandling.hh"
#include "temp-planner-interfaces/CmdArgs.hh"
#include "temp-planner-interfaces/Log.hh"
#include <sstream>
#include <iomanip>
#include "temp-planner-interfaces/Console.hh"
#include "ConfigFile.hh"
#include "temp-planner-interfaces/AliceStateHelper.hh"
#include "alice/AliceConstants.h"
#include <cotk/cotk.h>
#include <ncurses.h>
#include "LogicUtils.hh"
#include "dgcutils/cfgfile.h"
// #include "planner/cmdline.h"
using namespace std;

/* Initialize class constants */
double IntersectionHandling::DISTANCE_STOPLINE_ALICE=1;
double IntersectionHandling::VELOCITY_OBSTACLE_THRESHOLD=0.2;
double IntersectionHandling::DISTANCE_OBSTACLE_THRESHOLD=6;
double IntersectionHandling::ETA_EPS=0.5;
double IntersectionHandling::TRACKING_VEHICLE_SEPERATION=1.0;
bool IntersectionHandling::INTERSECTION_SAFETY_FLAG=1;
bool IntersectionHandling::INTERSECTION_CABMODE_FLAG=0;

bool IntersectionHandling::init_flag=false;
time_t IntersectionHandling::timestamp;
vector<ListToA> IntersectionHandling::VehicleList;
vector<ListBlockedObstacles> IntersectionHandling::BlockedObstacles;

IntersectionHandling::IntersectionReturn IntersectionHandling::ReturnValue;

vector<PointLabel> IntersectionHandling::WayPointsWithStop;
vector<PointLabel> IntersectionHandling::WayPointsNoStop;
vector<PointLabel> IntersectionHandling::WayPointsEntries;
CMapElementTalker IntersectionHandling::testMap;
bool IntersectionHandling::JammedTimerStarted;
bool IntersectionHandling::FirstCheck;
int IntersectionHandling::mapcounter;


void IntersectionHandling::init()
{
   char *path;

   path=dgcFindConfigFile("IntersectionHandling.conf","logic-planner");
   if (fopen(path,"r") == NULL)
    {
      Console::addMessage("Warning: Couldn't read Intersection configuration.");
      Log::getStream(1)<<"Warning: Couldn't read Intersection configuration."<<endl;
      Log::getStream(1)<<fopen("IntersectionHandling.conf","r")<<endl;
    }
  else
    {
      ConfigFile config(path);
      config.readInto(DISTANCE_STOPLINE_ALICE,"DISTANCE_STOPLINE_ALICE");
      config.readInto(VELOCITY_OBSTACLE_THRESHOLD,"VELOCITY_OBSTACLE_THRESHOLD");
      config.readInto(DISTANCE_OBSTACLE_THRESHOLD,"DISTANCE_OBSTACLE_THRESHOLD");
      config.readInto(ETA_EPS,"ETA_EPS");
      config.readInto(TRACKING_VEHICLE_SEPERATION,"TRACKING_VEHICLE_SEPERATION");
      config.readInto(INTERSECTION_SAFETY_FLAG,"INTERSECTION_SAFETY_FLAG");
      config.readInto(INTERSECTION_CABMODE_FLAG,"INTERSECTION_CABMODE_FLAG");
      Log::getStream(1)<<"Info: Read Intersection configuration successfully."<<endl;
    }

   mapcounter = MAPSTART;
}

void IntersectionHandling::reset(Map* localMap)
{
//   if (!init_flag)
//     {
      ReturnValue=RESET;

      // This resets the intersection, so the next time, a new intersection will be created
      WayPointsEntries.clear();
      WayPointsWithStop.clear();
      WayPointsNoStop.clear();

      VehicleList.clear();
      BlockedObstacles.clear();
      JammedTimerStarted=false;
      FirstCheck=false;
  
      // Initialize Debug-Map
      testMap.initSendMapElement(CmdArgs::sn_key);
      
      // clean up maviewer
      MapElement me;
      for (int i = MAPSTART; i<mapcounter; i++) {
        me.id = i;
        me.setTypeClear();
        testMap.sendMapElement(&me, sendChannel);
      }
      mapcounter = MAPSTART;

      // set init flag
//       init_flag=true;
//     }
  updateConsole();
}

void IntersectionHandling::updateConsole()
{
  // Update Console

  int counter=0;
  for (unsigned int i=0; i<VehicleList.size(); i++)
      if (VehicleList[i].precedence)
        counter++;

  string str="";

  switch(ReturnValue)
    {
    case RESET:         str="RESET        "; break;
    case WAIT_LEGAL:    str="WAIT_LEGAL   "; break;
    case WAIT_POSSIBLE: str="WAIT_POSSIBLE"; break;
    case WAIT_CLEAR:    str="WAIT_CLEAR   "; break;
    case GO:            str="GO           "; break;
    case JAMMED:        str="JAMMED       "; break;
    }
  
  Console::updateInter(INTERSECTION_SAFETY_FLAG,INTERSECTION_CABMODE_FLAG,counter,str);
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkIntersection(VehicleState vehState, Graph_t* graph, Map* localMap, SegGoals currSegment)
{
  // Create intersection if it didn't happen yet
  if (WayPointsEntries.size()==0)
    {
      // output some log information first
      time_t t;
      time(&t);
      Log::getStream(1)<<"Check Intersection starting with "<<PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID)<<". Time "<<ctime(&t)<<endl;
      stringstream s;
      s<<PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
      Console::addMessage("Check Intersection starting with %s", s.str().c_str());

      populateWayPoints(graph, localMap, PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID),WayPointsEntries,FALSE);
      findStoplines(graph, localMap, currSegment);
    }

  // If intersection created successfully
  if (WayPointsEntries.size()>0)
    {
      // Check for current status of other obstacles.
      checkExistenceObstacles(graph, localMap,vehState);

      // check whether it is a ROW intersection or whether Alice has a stopline
      PointLabel entryWaypoint(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);

      if (ReturnValue==RESET && !localMap->isStopLine(entryWaypoint))
        ReturnValue=WAIT_POSSIBLE;

      if (ReturnValue==RESET || ReturnValue==WAIT_LEGAL) {
          ReturnValue=checkPrecedence();
          if (Console::queryResetIntersectionFlag())
            ReturnValue=WAIT_POSSIBLE;
      }

      if (ReturnValue==WAIT_POSSIBLE || ReturnValue==WAIT_CLEAR) {
        ReturnValue=checkMerging(graph, localMap,currSegment);
        if (ReturnValue!=WAIT_CLEAR && Console::queryResetIntersectionFlag())
          ReturnValue=WAIT_CLEAR;
      }
      
      // While waiting for possibility to merge, do not start timer yet!
      if (ReturnValue==WAIT_POSSIBLE)
        JammedTimerStarted=false;
      
      if (ReturnValue==WAIT_CLEAR) {
        ReturnValue=checkClearance(graph, localMap,currSegment);
        if (Console::queryResetIntersectionFlag())
          ReturnValue=GO;
      }
    }
  else
    {
      // Output some debugging information
      Log::getStream(7)<<"Error: Couldn't build Intersection. Give GO. WayPoint "<<PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID)<<endl;

      stringstream s;
      s<<PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
      Console::addMessage("Error: Couldn't build Intersection. %s",s.str().c_str());

      ReturnValue=GO;
    }

//   if (ReturnValue==GO || ReturnValue==JAMMED )
//     init_flag=false;

  return ReturnValue;
}

void IntersectionHandling::populateWayPoints(Graph_t* graph, Map* localMap, PointLabel InitialWayPointEntry, vector<PointLabel> &WP, bool sendMap)
{
  vector<PointLabel> WayPointExits, WayPoint;

  // Obtain all WayPointExits for the initial WayPointEntry
  localMap->getWayPointExits(WayPointExits,InitialWayPointEntry);

  for (unsigned int i=0; i<WayPointExits.size(); i++)
    {
      localMap->getWayPointEntries(WayPoint,WayPointExits[i]);

      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++)
        {
          bool found=false;
          for (unsigned int k=0; k<WP.size(); k++)
            {
              if (WayPoint[j]==WP[k])
                {
                  found=true;
                  break;
                }
            }

          // If not, add it to list and call function recursivly
          if (!found)
            {
              WP.push_back(WayPoint[j]);
              populateWayPoints(graph, localMap,WayPoint[j],WP,sendMap);
            }
        }
    }

  if (sendMap)
    {
      // Send Waypoints to map
      MapElement me;
      int status;
      stringstream s;
      for (unsigned k=0; k<WP.size(); k++)
        {
          me.id = mapcounter++;
          me.setTypeWayPoints();

          GraphNode* node = graph -> getNodeFromRndfId(WP[k].segment, WP[k].lane, WP[k].point);

          me.setPosition(point2(node->pose.pos.x, node->pose.pos.y));
          s.str("");
          s<<"Waypoint "<<WP[k];
          me.label.clear();
          me.label.push_back(s.str());
          // little circle
          point2 p = point2(node->pose.pos.x, node->pose.pos.y);
          me.setGeometry(p,1.0);
          status=testMap.sendMapElement(&me,sendChannel);
        }
    }
}

void IntersectionHandling::findStoplines(Graph_t* graph, Map* localMap, SegGoals currSegment)
{
  MapElement me;
  point2 p;
  stringstream s;
  
  for (unsigned int i=0; i<WayPointsEntries.size(); i++) {
    if (localMap->isStopLine(WayPointsEntries[i])) {
      // Store information in vector
      WayPointsWithStop.push_back(WayPointsEntries[i]);
      
      // Send stopline to map, color red
      me.id = mapcounter++;
      me.setTypeStopLine();
      me.setColor(MAP_COLOR_RED);
      GraphNode* node = graph -> getNodeFromRndfId(WayPointsEntries[i].segment, WayPointsEntries[i].lane, WayPointsEntries[i].point);
      point2 p = point2(node->pose.pos.x, node->pose.pos.y);
      me.setGeometry(p,1,1);
      me.setPosition(p);
      s.str("");
      s<<"Stopsign at Waypoint "<<WayPointsEntries[i];
      me.label.clear();
      me.label.push_back(s.str());
      testMap.sendMapElement(&me,sendChannel);

      // Do not paint those boxes where Alice is. Does not look nice!
      if (!(WayPointsEntries[i] == PointLabel(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID))) {
        // send area that will be checked for precedence
        LaneLabel lane;
        
        // get centerline for this waypoint
        point2arr centerline;
        localMap->getLane(lane, p);
        
        point2 p1_l, p1_r;
        localMap->getLaneLeftPoint(p1_l, lane, p,0);
        localMap->getLaneRightPoint(p1_r, lane, p,0);
        
        point2 p2_l, p2_r;
        localMap->getLaneLeftPoint(p2_l, lane, p,-DISTANCE_OBSTACLE_THRESHOLD);
        localMap->getLaneRightPoint(p2_r, lane, p,-DISTANCE_OBSTACLE_THRESHOLD);
        
        point2arr parr;
        parr.push_back(p1_l);
        parr.push_back(p1_r);
        parr.push_back(p2_l);
        parr.push_back(p2_r);
      
        // paint box
        me.id = mapcounter++;
        me.setTypePoly();
        me.setGeometry(parr);
        me.setColor(MAP_COLOR_ORANGE,100);
        testMap.sendMapElement(&me,sendChannel);
      }
    }
    else {
      // Store information in vector
      WayPointsNoStop.push_back(WayPointsEntries[i]);	
    }
  }
}

void IntersectionHandling::checkExistenceObstacles(Graph_t* graph, Map* localMap,VehicleState vehState)
{
  point2 p,p2,position_obstacle;
  LaneLabel lane;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,distance;
  ListToA list_temp;
  int foundindex;
  vector<ListToA> tempVehicleList;
  int CounterObstacle=0;
  MapElement obstacleMap;
  stringstream s;
  
  // Reset updated-flag
  for (unsigned int k=0; k<VehicleList.size(); k++)
    VehicleList[k].updated=false;

  CounterObstacle=0;


  // Look for obstacles at those waypoints with stopsigns
  for (unsigned int i=0; i<WayPointsWithStop.size(); i++)
    {
      GraphNode* node = graph -> getNodeFromRndfId(WayPointsWithStop[i].segment, WayPointsWithStop[i].lane, WayPointsWithStop[i].point);
      point2 p = point2(node->pose.pos.x, node->pose.pos.y);
      localMap->getLane(lane, p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);


      // Loop through all obstacles found in lane
      for (unsigned int j=0;j<obstacle.size();j++)
        {
          if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE)
            continue;

          distance_geometry=INFINITY;
          // Get distance from stopline to closest point of obstacle
          for (unsigned l=0; l<obstacle[j].geometry.size();l++)
            {
              position_obstacle.set(obstacle[j].geometry[l]);
              localMap->getDistAlongLine(distance_temp, centerline, p, position_obstacle);
              if (distance_temp > -1.0 && distance_temp < distance_geometry)
                distance_geometry = distance_temp;
            }

          // Look within 30m of stopline
          if (distance_geometry>-1.0 && distance_geometry<30)
            {
              //              Console::addMessage("obst %i.%i.%i d=%f.2",WayPointsWithStop[i].segment,WayPointsWithStop[i].lane,WayPointsWithStop[i].point,distance_geometry);

              CounterObstacle++;

              // Look whether vehicle already exists
              foundindex=-1;

              // Store information that this vehicle still exist in list and won't be deleted later by "garbage collector"
              for (unsigned int k=0; k<VehicleList.size(); k++)
                {
                  if (VehicleList[k].element.id==obstacle[j].id)
                    {
                      VehicleList[k].updated=true;
                      foundindex=k;
                    }
                }

              // Don't add or update information when Alice stopped.
              // If Alice is stopped, we only wait until existing vehicles with ETA<epsilon are leaving intersection
              // If vehicle is new, then add it to list
              // DOES THIS STILL WORK??????
              if (foundindex==-1 && !FirstCheck)
                {
                  if (distance_temp>0)
                    {
                      // Create list object
                      list_temp.element=obstacle[j];
                      list_temp.WayPoint=WayPointsWithStop[i];
                      list_temp.velocity=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));
                      list_temp.distance=distance_geometry;
                      list_temp.precedence=false;
                      list_temp.checkedQueuing=false;
                      list_temp.updated=true;
		      
                      // Arrival assumption at stop line. Assume that dyn. obstacle has stopped when velocity<.5m/s
                      if (list_temp.velocity>=VELOCITY_OBSTACLE_THRESHOLD)
                        list_temp.eta=list_temp.distance/list_temp.velocity;
                      else if (list_temp.distance>=DISTANCE_OBSTACLE_THRESHOLD)
                        list_temp.eta=INFINITY;
                      else
                        list_temp.eta=0;
		      
                      // store new vehicle
                      VehicleList.push_back(list_temp);

                      Log::getStream(7)<<"Obstacle "<<obstacle[j].id<<" stored"<<endl;
                      Log::getStream(7)<<"distance "<<list_temp.distance<<endl;
                      Log::getStream(7)<<"velocity "<<list_temp.velocity<<endl;
                      Log::getStream(7)<<"ETA "<<list_temp.eta<<endl;
                    }
                }
            }
        }
    }

  // This prevents that new vehicles will be added to list after Alice came to a stopline
  FirstCheck=true;

  int found;

  // Clean up VehicleList "Garbage collector"
  for (unsigned int k=0; k<VehicleList.size(); k++)
    {
      // If obstacle seems to disappear, do some checks first before deleting this vehicle
      if (VehicleList[k].updated==false)
        {
          //          Console::addMessage("Try deleting obstacle out of list");
          // If there is something blocking the view, keep the obstacle that seemed to disappear
          if (!checkVisibility(localMap,vehState,VehicleList[k].element))
            VehicleList[k].updated=true;

          // the obstacle might have changed the ID. So look for obstacles that are really close to the old ID. If so, assign this new object to the VehicleList
          // obtain position of old obstacle and get the lane that is is supposed to be 
          
          p.set(VehicleList[k].element.position);
          localMap->getLane(lane,p);
          localMap->getLaneCenterLine(centerline,lane);
          localMap->getObsInLane(obstacle,lane);

          distance=INFINITY;
          found=-1;
          for (unsigned int m=0; m<obstacle.size(); m++)
            {
              p2.set(obstacle[m].position);
              localMap->getDistAlongLine(distance_temp,centerline,p,p2);
              if (fabs(distance_temp)<distance)
                {
                  distance=fabs(distance_temp);
                  found=m;
                }
            }

          // if there is an obstacle than replace the old obstacle with this new one
          if (found!=-1 && distance<TRACKING_VEHICLE_SEPERATION)
            {
              Console::addMessage("Found obstacle nearby. Do not delete in list");
              VehicleList[k].element=obstacle[found];
              VehicleList[k].updated=true;
            }
        }
     
      // only delete object when it is not updated
      if (VehicleList[k].updated==false)
        {
          // Clear element out of map
          obstacleMap.setId(VehicleList[k].element.id);
          obstacleMap.setTypeClear();
          testMap.sendMapElement(&obstacleMap,sendChannel);

          Log::getStream(7)<<"Remove obstacle at WayPoint..."<<VehicleList[k].WayPoint<<"... ObstacleID..."<<VehicleList[k].element.id<<endl;
        }
      else
        tempVehicleList.push_back(VehicleList[k]);
    }
  VehicleList=tempVehicleList;

  // Output all vehicles that are at the intersection currently to the map
  for (unsigned int m=0;m<VehicleList.size();m++)
    {
      // Send obstacle to map
      if (VehicleList[m].precedence)
        VehicleList[m].element.setColor(MAP_COLOR_YELLOW,100);
      else
        VehicleList[m].element.setColor(MAP_COLOR_GREY,100);

      testMap.sendMapElement(&VehicleList[m].element,sendChannel);
    }
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkPrecedence()
{
  int precedence_counter=0;
  MapElement me;

  // All Vehicles with a very small ETA have precedence
  for (unsigned int i=0; i<VehicleList.size(); i++) {
    // only the vehicle with the closest distance to each waypoint shall have precedence
    if (!VehicleList[i].checkedQueuing) {
      VehicleList[i].closest=true;
      
      // get obstacle that is the closest to the stopline
      for (unsigned k=0; k<VehicleList.size(); k++) {
        // If vehicles are at same waypoint and one is further away than the other, uncheck flag
        if ((VehicleList[i].WayPoint==VehicleList[k].WayPoint) && (VehicleList[i].distance>VehicleList[k].distance))
          VehicleList[i].closest=false;
      }
      
      // If vehicle is not flagged as "closest" and is within 3m to the closest obstacle than also give this obstacle
      // precedence as it should be the same obstacle
      if (!VehicleList[i].closest) {
        for (unsigned z=0; z<VehicleList.size(); z++) {
          if ((VehicleList[i].WayPoint==VehicleList[z].WayPoint) && (VehicleList[z].closest) && (VehicleList[i].distance-VehicleList[z].distance<3.0))
              VehicleList[i].closest=true;
        }
      }
	    
      // Set flag so that this check is only done once per vehicle
      VehicleList[i].checkedQueuing=true;
    }
    
    // If ETA is so small that it should have precedence and is the first vehicle in queue, then this
    // vehicle is flagged with precedene
    if (VehicleList[i].eta<ETA_EPS && VehicleList[i].closest) {
      precedence_counter++;
      VehicleList[i].precedence=true;
    }
    else
      VehicleList[i].precedence=false;
  }
  
  // If there are other vehicles with precedence, return false
  if (precedence_counter>0)
    return WAIT_LEGAL;
  else
    return WAIT_POSSIBLE;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkMerging(Graph_t* graph, Map* localMap, SegGoals currSegment)
{
  point2 p,p_exit,position_obstacle;
  LaneLabel lane,lane_exit;
  point2arr centerline;
  vector<MapElement> obstacle;
  double distance_temp,distance_geometry,velocity;

  for (unsigned int i=0; i<WayPointsNoStop.size(); i++)
    {
      // get lane of waypoint that we are looping through
      GraphNode* node = graph -> getNodeFromRndfId(WayPointsNoStop[i].segment, WayPointsNoStop[i].lane, WayPointsNoStop[i].point);
      point2 p = point2(node->pose.pos.x, node->pose.pos.y);
      localMap->getLane(lane, p);
      localMap->getLaneCenterLine(centerline,lane);
      localMap->getObsInLane(obstacle,lane);

      // get WayPoint that Alice would like to go to
      PointLabel WayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);
      // get all WayPoints that can exit to the Waypoint that Alice wants to exit to
      localMap->getWayPointEntries(WayPointsEntries,WayPointExit);

      // dependant on the safety flag, check every waypoint or only those that can exit to the waypoint that Alice wants to exit to
      if (!INTERSECTION_SAFETY_FLAG)
        {
          bool found=false;
          for (unsigned int m=0; m<WayPointsEntries.size(); m++)
            if (WayPointsEntries[m]==WayPointsNoStop[i])
              found=true;
          // if not found, continue in loop; so this waypoint won't be checked for traffic
          if (!found) continue;
        }


      // Loop through all obstacles found in lane
      for (unsigned int j=0;j<obstacle.size();j++)
        {
          if (obstacle[j].type!=ELEMENT_OBSTACLE && obstacle[j].type!=ELEMENT_VEHICLE)
            continue;

          distance_geometry=INFINITY;
          // Get distance from waypoint to closest point of obstacle
          for (unsigned l=0; l<obstacle[j].geometry.size();l++)
            {
              position_obstacle.set(obstacle[j].geometry[l]);
              localMap->getDistAlongLine(distance_temp, centerline, p, position_obstacle);
              if (distance_temp > 0.0 && distance_temp < distance_geometry)
                distance_geometry = distance_temp;
            }

          // DARPA requires to have a 10s gap
          velocity=sqrt(pow(obstacle[j].velocity.x,2)+pow(obstacle[j].velocity.y,2));
	  
          // check whether vehicle is approaching the intersection (condition >0) and whether it is far away enough
          if (distance_geometry>0 && distance_geometry<10*velocity)
            return WAIT_POSSIBLE;
        }
    }
  
  return WAIT_CLEAR;
}

IntersectionHandling::IntersectionReturn IntersectionHandling::checkClearance(Graph_t* graph, Map* localMap,SegGoals currSegment)
{
  point2arr centerline;
  vector<PointLabel> WayPointExits;
  vector<MapElement> obstacles, obstaclesAlongGraph;
  int cabmode_counter=0;
  int moving_counter=0;
  bool found=false;
  ListBlockedObstacles tempBlockObstacle;
  vector<ListBlockedObstacles> tempList;
  point2arr leftBound, rightBound, leftBoundAlongGraph, rightBoundAlongGraph;
  MapElement me;
  LaneLabel lane_exit;
  //  double distance_exit;
  stringstream s;

  // reset status of all obstacles blocking the intersection
  for (unsigned k=0; k<BlockedObstacles.size(); k++)
    BlockedObstacles[k].updated=false;

  // start timer to make sure that Alice doesn't brake the 10s rule
  if (!JammedTimerStarted) {
    time(&timestamp);
    JammedTimerStarted=true;
  }
  
  // get current WayPointEntry and WayPointEntry of Alice
  PointLabel currentWayPointEntry(currSegment.entrySegmentID, currSegment.entryLaneID, currSegment.entryWaypointID);
  PointLabel currentWayPointExit(currSegment.exitSegmentID, currSegment.exitLaneID, currSegment.exitWaypointID);

  // Check all WayPoint, get their Exit and check whether Entry and Exit are in the same lane
  for (unsigned i=0; i<WayPointsEntries.size(); i++) {
    // if safety flag is not set, only check clearance in current entry/exit-corridor. Skip the rest
    if (!INTERSECTION_SAFETY_FLAG && !(WayPointsEntries[i]==currentWayPointEntry))
      continue;
    
    // get all exits for this entry
    localMap->getWayPointExits(WayPointExits,WayPointsEntries[i]);
    for (unsigned j=0;j<WayPointExits.size(); j++) {
      // if safety flag is not set, only check clearance in current entry/exit-corridor. Skip the rest
      if (!INTERSECTION_SAFETY_FLAG && !(WayPointExits[j]==currentWayPointExit))
        continue;
      
      // Create TransitionBound between this WayPointEntry and WayPointExit
      if (getTransition(graph, localMap, leftBound, WayPointsEntries[i], WayPointExits[j], -1) == -1)
        Log::getStream(1)<<"Error in getTransition. Entry "<<WayPointsEntries[i]<<". Exit "<<WayPointExits[j]<<". Left Bound"<<endl;
      if (getTransition(graph, localMap, rightBound, WayPointsEntries[i], WayPointExits[j], 1) == -1)
        Log::getStream(1)<<"Error in getTransition. Entry "<<WayPointsEntries[i]<<". Exit "<<WayPointExits[j]<<". Right Bound"<<endl;
      
      // Get all obstacles within these bounds
      localMap->getObsInBounds(obstacles, leftBound, rightBound);
      
      // Send left bound to map
      me.id = mapcounter++;
      me.setTypeLine();
      me.setColor(MAP_COLOR_GREEN,100);
      me.setGeometry(leftBound);
      s.str("");
      s<<"Left bound "<<WayPointsEntries[i]<<"->"<<WayPointExits[j];
      me.label.clear();
      me.label.push_back(s.str());
      testMap.sendMapElement(&me,sendChannel);
      
      // Send right bound to map
      me.id = mapcounter++;
      me.setGeometry(rightBound);
      s.str("");
      s<<"Right bound "<<WayPointsEntries[i]<<"->"<<WayPointExits[j];
      me.label.clear();
      me.label.push_back(s.str());
      testMap.sendMapElement(&me,sendChannel);
      
      for (unsigned int k=0; k<obstacles.size(); k++) {
        bool sendToMap = false;

        if (obstacles[k].type!=ELEMENT_OBSTACLE && obstacles[k].type!=ELEMENT_VEHICLE)
          continue;

        // if obstacle is stopped longer than 10 seconds, check whether this obstacle blocks Alice's Corridor
        if ((obstacles[k].type == ELEMENT_VEHICLE && obstacles[k].timeStopped>=10.0) || obstacles[k].type==ELEMENT_OBSTACLE) {

          if (getTransitionAlongGraph(graph, localMap, leftBoundAlongGraph, currentWayPointEntry, currentWayPointExit, -1) == -1)
            Log::getStream(1)<<"Error in getTransitionAlongGraph. Entry "<<currentWayPointEntry<<". Exit "<<currentWayPointExit<<". Left Bound"<<endl;
          if (getTransitionAlongGraph(graph, localMap, rightBoundAlongGraph, currentWayPointEntry, currentWayPointExit, 1) == -1)
            Log::getStream(1)<<"Error in getTransitionAlongGraph. Entry "<<currentWayPointEntry<<". Exit "<<currentWayPointExit<<". Right Bound"<<endl;
          
          // Send left bound to map
          me.id = mapcounter++;
          me.setTypeLine();
          me.setColor(MAP_COLOR_ORANGE,100);
          me.setGeometry(leftBoundAlongGraph);
          s.str("");
          s<<"Left bound "<<currentWayPointEntry<<"->"<<currentWayPointExit;
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);
      
          // Send right bound to map
          me.id = mapcounter++;
          me.setGeometry(rightBoundAlongGraph);
          s.str("");
          s<<"Right bound "<<currentWayPointEntry<<"->"<<currentWayPointExit;
          me.label.clear();
          me.label.push_back(s.str());
          testMap.sendMapElement(&me,sendChannel);

          // query all obstacles within this boundary ...
          localMap->getObsInBounds(obstaclesAlongGraph, leftBoundAlongGraph, rightBoundAlongGraph);
          // ... and check whether this obstacle is there
          for (unsigned int o=0; o<obstaclesAlongGraph.size(); o++)
            if (obstaclesAlongGraph[o].id == obstacles[k].id) {
              cabmode_counter ++;
              sendToMap = true;
            }
        }
        else {
          moving_counter++;
          sendToMap = true;
        }
        
        
        if (sendToMap) {
                // send obstacle that blocks the lane	
          s.str("");
          if (obstacles[k].type==ELEMENT_OBSTACLE)
            s<<"stat.Obs., blocking intersection";
          else if (obstacles[k].type==ELEMENT_VEHICLE)
            s<<"dyn.Obs., blocking intersection";
          else
            s<<"unk.Obs., blocking intersection";
          
          obstacles[k].label.clear();
          obstacles[k].label.push_back(s.str());
          obstacles[k].setColor(MAP_COLOR_RED,100);
          testMap.sendMapElement(&obstacles[k],sendChannel);
          Log::getStream(7)<<"Intersection...blocked by obstacle "<<obstacles[k].id<<endl;
          
          found=false;
          // if obstacle is found in list, set updated=true
          for (unsigned l=0; l<BlockedObstacles.size(); l++) {
            if (obstacles[k].id==BlockedObstacles[l].obstacle.id) {
              BlockedObstacles[l].updated=true;
              found=true;
            }
          }
          // otherwise add it to list
          if (!found)  {
            tempBlockObstacle.obstacle=obstacles[k];
            tempBlockObstacle.updated=true;
            BlockedObstacles.push_back(tempBlockObstacle);
          }
        }
      }
    }
  }
  
  // Clear out obstacles that disappear from map; this doesnt affect Alice and is for debugging/mapping purposes only
  for (unsigned m=0;m<BlockedObstacles.size(); m++) {
    if (BlockedObstacles[m].updated)
      tempList.push_back(BlockedObstacles[m]);
    else {
      // clear obstacle out of map
      BlockedObstacles[m].obstacle.setTypeClear();
      testMap.sendMapElement(&BlockedObstacles[m].obstacle,sendChannel);
    }
  }
  BlockedObstacles=tempList;

  // check whether Alice needs to switch in cab mode
  time_t currentTime;
  time(&currentTime);
  double diff=difftime(currentTime,timestamp);

  if (moving_counter == 0 && cabmode_counter == 0)
    return GO;
  else if (moving_counter==0 && cabmode_counter>0 && diff>=10.0 && INTERSECTION_CABMODE_FLAG) {
    Console::addMessage("Warning: Switching into Cab mode");
    return JAMMED;
  }
  else
    return WAIT_CLEAR;
}

bool IntersectionHandling::checkVisibility(Map* localMap,VehicleState Alice,MapElement obstacle)
{
  double maxRight=-INFINITY;
  double maxLeft=INFINITY;
  point2 maxRightPoint2,maxLeftPoint2;

  // Obtain the max/min points of the obstacle's geometry
  for (unsigned int i=0; i<obstacle.geometry.size(); i++)
    {
      if (obstacle.geometry[i].x>maxRight)
        {
          maxRight=obstacle.geometry[i].x;
          maxRightPoint2.set(obstacle.geometry[i]);
        }
      if (obstacle.geometry[i].x<maxLeft)
        {
          maxLeft=obstacle.geometry[i].x;
          maxLeftPoint2.set(obstacle.geometry[i]);
        }
    }
  
  // Obtain front left and front right point of Alice
  point2 bumper=AliceStateHelper::getPositionFrontBumper(Alice);
  point2 frontLeft=point2(bumper.x+sin(Alice.localYaw)*VEHICLE_WIDTH/2,bumper.y-cos(Alice.localYaw)*VEHICLE_WIDTH/2);
  point2 frontRight=point2(bumper.x-sin(Alice.localYaw)*VEHICLE_WIDTH/2,bumper.y+cos(Alice.localYaw)*VEHICLE_WIDTH/2);

  point2arr leftBound,rightBound;
  leftBound.push_back(frontLeft);
  leftBound.push_back(maxLeftPoint2);
  rightBound.push_back(frontRight);
  rightBound.push_back(maxRightPoint2);

  MapElement me;
  // Send left bound to map
  me.id = mapcounter++;
  me.setTypeLine();
  me.setColor(MAP_COLOR_MAGENTA,100);
  me.setGeometry(leftBound);
  testMap.sendMapElement(&me,sendChannel);

  // Send right bound to map
  me.id = mapcounter++;
  me.setGeometry(rightBound);
  testMap.sendMapElement(&me,sendChannel);

  // Get all obstacles within these bounds
  vector<MapElement> obst;
  localMap->getObsInBounds(obst, leftBound, rightBound);
  
  if (obst.size()>0)
    {
      double distObstacle=INFINITY;
      double dist=INFINITY;
      double distTemp;
      
      // calculate distance between the obstacle that we are looking at and Alice
      // this is independant from whether this obstacle does still exist or is gone already
      for (unsigned int k=0; k<obstacle.geometry.size(); k++)
        {
          distTemp=sqrt(pow(obstacle.geometry[k].x-Alice.localX,2)+pow(obstacle.geometry[k].y-Alice.localY,2));
          if (distTemp<distObstacle)
            distObstacle=distTemp;
        }
  
      for (unsigned int i=0; i<obst.size(); i++)
        {
          if (obst[i].id!=obstacle.id)
            {
              for (unsigned int l=0; l<obst[i].geometry.size(); l++)
                {
                  distTemp=sqrt(pow(obst[i].geometry[l].x-Alice.localX,2)+pow(obst[i].geometry[l].y-Alice.localY,2));
                  if (distTemp<dist)
                    dist=distTemp;
                }
            }
        }

      // If any other obstacle is closer, return false as it seems to be in betweenthe obstacle that we are looking at and Alice
      if (dist<distObstacle)
        {
          Console::addMessage("Warning: There might be an obstacle blocking the view to a vehicle with precedence");
          return false;
        }
      else
        return true;
    }
  else
    return true;
}

void IntersectionHandling::toggleIntersectionSafety()
{
  INTERSECTION_SAFETY_FLAG = !INTERSECTION_SAFETY_FLAG;
}

void IntersectionHandling::toggleIntersectionCabmode()
{
  INTERSECTION_CABMODE_FLAG = !INTERSECTION_CABMODE_FLAG;
}

int IntersectionHandling::getTransition(Graph_t* graph, Map* localMap, point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, int leftright)
{
  bound.clear();

  point2 projectEntry,projectExit;
  LaneLabel entryLane, exitLane;
  point2arr entryBound, exitBound;
  point2arr extendEntryBound, extendExitBound;

  // get point2 of WaypointEntry and WaypointExit
  GraphNode* entry = graph -> getNodeFromRndfId(WaypointEntry.segment, WaypointEntry.lane, WaypointEntry.point);
  GraphNode* exit = graph -> getNodeFromRndfId(WaypointExit.segment, WaypointExit.lane, WaypointExit.point);

  // get the lanes of entry and exit
  localMap->getLane(entryLane, point2(entry->pose.pos.x, entry->pose.pos.y));
  localMap->getLane(exitLane, point2(exit->pose.pos.x, exit->pose.pos.y));

  if (leftright == -1) {
    // get the left and right points of entry/exit
    localMap->getLeftBound(entryBound,entryLane);
    localMap->getLeftBound(exitBound,exitLane);
  }
  else if (leftright == 1) {
    // get the left and right points of entry/exit
    localMap->getRightBound(entryBound,entryLane);
    localMap->getRightBound(exitBound,exitLane);
  }
  localMap->getProjectToLine(projectEntry, entryBound, point2(entry->pose.pos.x, entry->pose.pos.y));
  localMap->getProjectToLine(projectExit, exitBound, point2(exit->pose.pos.x, exit->pose.pos.y));

  // find connection point for left transition
  point2 cp;

  // get angles of lanes in case we change laneIDs but it is more or less straight
  double angleEntry, angleExit, angleDiff;

  double r, p;
  GraphNode* node = graph -> getNodeFromRndfId(WaypointEntry.segment, WaypointEntry.lane, WaypointEntry.point);
  quat_to_rpy(node->pose.rot, &r, &p, &angleEntry);

  node = graph -> getNodeFromRndfId(WaypointExit.segment, WaypointExit.lane, WaypointExit.point);
  quat_to_rpy(node->pose.rot, &r, &p, &angleExit);

  angleDiff = angleEntry - angleExit;

  // if entry and exit are not in the same lane, find correct transition
  if (!(entryLane == exitLane) && fabs(angleDiff)>0.5 && fabs(angleDiff)<=6.0) {
    bool problem = false;
    if (!crossPoint(entryBound, exitBound, cp)) {
      // extend entry lane
      localMap->extendLine(extendEntryBound, entryBound, 100.0);
      entryBound = extendEntryBound;
      
      if (!crossPoint(entryBound, exitBound, cp)) {

        // extend exit lane
        localMap->extendLine(extendExitBound, exitBound, -100.0);
        exitBound = extendExitBound;
        
        if (!crossPoint(entryBound, exitBound, cp))
          problem = true;
      }
    }
    if (problem) {
      Log::getStream(1)<<"Error in getTransition: could not connect lines"<<endl;
      return -1;
    }
  }
  // else if there are in the same lane, it is much easier
  else
    cp = projectEntry;

  int index_start, index_end;

  // find closest index of start/end point in entryLane
  if (getClosestIndex(index_start, entryBound, projectEntry) == -1) {
    Log::getStream(1)<<"Error in getTransition: did not find start index for entry"<<endl;
    return -1;
  }
  if (getClosestIndex(index_end, entryBound, cp) == -1) {
    Log::getStream(1)<<"Error in getTransition: did not find end index for entry"<<endl;
    return -1;
  }

  for (int i=index_start; i<index_end; i++)
    bound.push_back(entryBound[i]);

  if (index_start == index_end) {
    bound.push_back(projectEntry);
    bound.push_back(cp);
  }
                      
  // as the entry point might not be found at exit. This happens when to straight lanes are shifted a little bit
  if (cp == projectEntry)
    cp = projectExit;

  // find closest index of start/end point in exitLane
  if (getClosestIndex(index_start, exitBound, cp) == -1) {
    Log::getStream(1)<<"Error in getTransition: did not find start index for exit"<<endl;
    return -1;
  }
  if (getClosestIndex(index_end, exitBound, projectExit) == -1) {
    Log::getStream(1)<<"Error in getTransition: did not find end index for exit"<<endl;
    return -1;
  }

  for (int i=index_start; i<index_end; i++)
    bound.push_back(exitBound[i]);

  if (index_start == index_end) {
    bound.push_back(cp);
    bound.push_back(projectExit);
  }

  return 0;
}

int IntersectionHandling::getClosestIndex(int &index, point2arr parr, point2 p)
{
  for (unsigned int i=1; i<parr.size(); i++) {
    if (inSection(parr[i-1], parr[i], p)) {
      double distA = sqrt(pow((parr[i-1].x - p.x), 2) + pow((parr[i-1].y - p.y), 2));
      double distB = sqrt(pow((parr[i].x - p.x), 2) + pow((parr[i].y - p.y), 2));
    
      if (distA<distB) {
        index = i-1;
        return 0;
      }
      else {
        index = i;
        return 0;
      }
    }
  }
  return -1;
}

bool IntersectionHandling::crossPoint(point2arr a, point2arr b, point2 &crosspoint)
{
  for (unsigned int ai = 1; ai<a.size(); ai++) {
    for (unsigned int bi = 1; bi<b.size(); bi++) {
      double ax=a[ai-1].x;
      double ay=a[ai-1].y;
      double bx=a[ai].x;
      double by=a[ai].y;

      double cx=b[bi-1].x;
      double cy=b[bi-1].y;
      double dx=b[bi].x;
      double dy=b[bi].y;


      double v = ((cx-ax) * (by-ay) - (cy-ay) * (bx-ax)) / ((dy-cy) * (bx-ax) - (dx-cx) * (by-ay));

      crosspoint.x = cx + v * (dx-cx);
      crosspoint.y = cy + v * (dy-cy);


      bool result_a = inSection(a[ai-1],a[ai], crosspoint);
      bool result_b = inSection(b[bi-1],b[bi], crosspoint);
      
      if (result_a && result_b) return true;
    }
  }
  return false;
}

bool IntersectionHandling::inSection(point2 a, point2 b, point2 x)
{
  double v = (x.x - a.x) / (b.x - a.x);
  if (v>=0 && v<=1) return true;
  return false;
}


int IntersectionHandling::getTransitionAlongGraph(Graph_t* graph, Map* localMap, point2arr &bound, PointLabel WaypointEntry, PointLabel WaypointExit, int leftright)
{
  Vehicle *vp;
  Maneuver *mp;  
  point2 projectEntry, projectExit, p_entry, p_exit;
  Pose2D pose_entry, pose_exit;
  VehicleConfiguration config;
  LaneLabel entryLane, exitLane;
  point2arr entryBound, exitBound;
  
  // get point2 of WaypointEntry and WaypointExit
  GraphNode* entry = graph -> getNodeFromRndfId(WaypointEntry.segment, WaypointEntry.lane, WaypointEntry.point);
  GraphNode* exit = graph -> getNodeFromRndfId(WaypointExit.segment, WaypointExit.lane, WaypointExit.point);

  // get the lanes of entry and exit
  p_entry = point2(entry->pose.pos.x, entry->pose.pos.y);
  localMap->getLane(entryLane, p_entry);
  p_exit = point2(exit->pose.pos.x, exit->pose.pos.y);
  localMap->getLane(exitLane, p_exit);

  // get the left and right points of entry/exit
  if (leftright == -1) {
    localMap->getLeftBound(entryBound,entryLane);
    localMap->getLeftBound(exitBound,exitLane);
  }
  else if (leftright == 1) {
    localMap->getRightBound(entryBound,entryLane);
    localMap->getRightBound(exitBound,exitLane);
  }
  localMap->getProjectToLine(projectEntry, entryBound, p_entry);
  localMap->getProjectToLine(projectExit, exitBound, p_exit);

  pose_entry.x = projectEntry.x;
  pose_entry.y = projectEntry.y;

  double r, p;
  quat_to_rpy(entry->pose.rot, &r, &p, &pose_entry.theta);
  quat_to_rpy(exit->pose.rot, &r, &p, &pose_exit.theta);
  pose_exit.x = projectExit.x;
  pose_exit.y = projectExit.y;

  // Vehicle properties
  vp = maneuver_create_vehicle( VEHICLE_WHEELBASE, M_PI/2 );
    // Create maneuver object
  mp = maneuver_pose2pose(vp, &pose_entry, &pose_exit);
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // create boundaries
  bound.clear();
  int numSteps = 20;
  for (int i = 1; i < numSteps + 1; i++) 
  {
    double s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    point2 p=point2(config.x, config.y);
    bound.push_back(p);
  }
  return 0;
}
