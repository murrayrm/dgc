#include "LogicUtils.hh"
#include <alice/AliceConstants.h>
#include <math.h>

#define EPS 0.2


#define EXTRA_WIDTH 1.5

bool LogicUtils::isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane)
{
  point2arr lb, rb;
  map->getLaneBounds(lb, rb, lane);

  double ldist = me.dist(lb, GEOMETRY_LINE);
  double rdist = me.dist(rb, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

bool LogicUtils::isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound)
{
  double ldist = me.dist(leftBound, GEOMETRY_LINE);
  double rdist = me.dist(rightBound, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

double LogicUtils::getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal)
{
  // Set position
  point2 currFrontPos;
  currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);

  // Get current lane
  // Temporary: We want to use a desiredLaneLabel in the future
  LaneLabel current_lane(seg_goal.exitSegmentID, seg_goal.exitLaneID);
  // map->getLane(current_lane, currFrontPos);

  // Get all stoplines on that lane
  vector<PointLabel> stoplines;
  map->getLaneStopLines(stoplines, current_lane);

  // Get center line
  point2arr centerline;
  map->getLaneCenterLine(centerline, current_lane);

  // Find the closest stopline on the lane on front of use
  point2 stop_position;
  double dist, min_dist = -1;
  for (unsigned int i=0; i<stoplines.size(); i++) {
    map->getWaypoint(stop_position, stoplines[i]);
    map->getDistAlongLine(dist, centerline, stop_position, currFrontPos);
    if (dist >= 0.0 && (dist < min_dist || min_dist == -1)) {
      min_dist = dist;
    }
  }

  return min_dist;
}
