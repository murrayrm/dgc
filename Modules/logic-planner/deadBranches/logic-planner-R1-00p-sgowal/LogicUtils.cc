#include "LogicUtils.hh"
#include <alice/AliceConstants.h>
#include <temp-planner-interfaces/Log.hh>
#include "temp-planner-interfaces/CmdArgs.hh"
#include <math.h>
#include "map/MapElementTalker.hh"
#include "frames/vec3.h"

#define EXTRA_WIDTH 1.5

bool LogicUtils::isObstacleBlockingLane(MapElement & me, Map *map, LaneLabel lane)
{
  point2arr lb, rb;
  map->getLaneBounds(lb, rb, lane);

  double ldist = me.dist(lb, GEOMETRY_LINE);
  double rdist = me.dist(rb, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

bool LogicUtils::isObstacleBlockingBounds(MapElement & me, point2arr leftBound, point2arr rightBound)
{
  double ldist = me.dist(leftBound, GEOMETRY_LINE);
  double rdist = me.dist(rightBound, GEOMETRY_LINE);

  if (ldist < VEHICLE_WIDTH + EXTRA_WIDTH && rdist < VEHICLE_WIDTH + EXTRA_WIDTH) {
    return true;
  }

  return false;
}

LaneLabel LogicUtils::getCurrentLane(VehicleState &vehState, Map *map)
{
  point2 currPos = AliceStateHelper::getPositionRearAxle(vehState);
  LaneLabel lane;
  map->getLane(lane, currPos);
  return lane;
}

bool LogicUtils::isReverse(VehicleState &vehState, Map *map, LaneLabel &lane)
{
  double currlane_angle;
  point2 pt;

  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearaxle = AliceStateHelper::getPositionRearAxle(vehState);

  map->getHeading(currlane_angle, pt, lane, alice_rearaxle);
  double diff_angle = fabs(getAngleInRange(alice_angle-currlane_angle));
  
  return (diff_angle > M_PI/2);
}

double LogicUtils::getDistToStopline(VehicleState &vehState, Map *map, SegGoals &seg_goal)
{
  // Get current lane
  LaneLabel current_lane(seg_goal.exitSegmentID, seg_goal.exitLaneID);

  return getDistToStopline(vehState, map, current_lane);
}

double LogicUtils::getDistToStopline(point2 &pos, Map *map, LaneLabel &current_lane)
{
  // Get all stoplines on that lane
  vector<PointLabel> stoplines;
  map->getLaneStopLines(stoplines, current_lane);

  // Get center line
  point2arr centerline;
  map->getLaneCenterLine(centerline, current_lane);

  // Find the closest stopline on the lane on front of use
  point2 stop_position;
  double dist, min_dist = -1;
  for (unsigned int i=0; i<stoplines.size(); i++) {
    map->getWaypoint(stop_position, stoplines[i]);
    map->getDistAlongLine(dist, centerline, stop_position, pos);
    if (dist >= 0.0 && (dist < min_dist || min_dist == -1)) {
      min_dist = dist;
    }
  }

  return min_dist;
}

double LogicUtils::getDistToStopline(VehicleState &vehState, Map *map, LaneLabel &current_lane)
{
  // Set position
  point2 currFrontPos;
  currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  return getDistToStopline(currFrontPos, map, current_lane);
}

double LogicUtils::getDistToExit(VehicleState &vehState, Map *map, SegGoals &seg_goal)
{
  double dist;
  LaneLabel current_lane;
  map->getLane(current_lane, AliceStateHelper::getPositionFrontBumper(vehState));

  LaneLabel waypoint_lane(seg_goal.entrySegmentID, seg_goal.entryLaneID);

  if (current_lane == waypoint_lane) {
    point2arr centerline;
    map->getLaneCenterLine(centerline, waypoint_lane);
    point2 entry_position;
    PointLabel entryWaypoint = PointLabel(seg_goal.entrySegmentID, seg_goal.entryLaneID, seg_goal.entryWaypointID);
    map->getWaypoint(entry_position, entryWaypoint);

    map->getDistAlongLine(dist, centerline, entry_position, AliceStateHelper::getPositionFrontBumper(vehState));
  }

  return dist;
}

double LogicUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

double LogicUtils::getNearestObsInLane(MapElement ** me, VehicleState &vehState, Map *map, LaneLabel &lane)
{
  vector<MapElement> obstacles;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  bool is_reverse = isReverse(vehState, map, lane);

  int obsErr = map->getObsInLane(obstacles, lane);
  if (obsErr < 1) //no obstacles
    return -1;

  int obs_index = -1;
  double min_dist = INFINITY;
  double dist;
  point2 obs_pt;
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);

  for (unsigned int i=0; i<obstacles.size(); i++) {
    for (unsigned int j=0; j<obstacles[i].geometry.size(); j++) {
      obs_pt.set(obstacles[i].geometry[j]);
      map->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
      if (is_reverse) dist = -dist;
      if (dist > 0.0 && dist < min_dist) {
        min_dist = dist;
        obs_index = i;
      }
    }
  }

  if (min_dist == INFINITY)
    return -1;

  if (me != NULL) {
    *me = &obstacles[obs_index];
  }

  return min_dist; 
}

double LogicUtils::getNearestObsDist(VehicleState &vehState, Map * map, LaneLabel &lane)
{
  return getNearestObsInLane(NULL, vehState, map, lane);
}

double LogicUtils::monitorAliceSpeed(VehicleState &vehState)
{
  static bool first_time_stopped = true;
  static uint64_t stopped_since = 0;
  double current_velocity = AliceStateHelper::getVelocityMag(vehState);

  if (current_velocity < 0.2) {
    if (first_time_stopped) {
      stopped_since = getTime();
      first_time_stopped = false;
    }
    return (double)(getTime()-stopped_since)/1000000.0;
  } else {
    first_time_stopped = true;
    return 0.0;
  }
  return 0.0;
}

void LogicUtils::updateStoppedMapElements(Map *map)
{
  MapElement *mapEl;
  point2 point;
  LaneLabel obs_lane;
  double dist;

  for (int j = 0; j < (int)map->data.size(); j++) {
    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_VEHICLE) continue;
    if (mapEl->timeStopped < 10.0) continue;

    // Check if obstacle is close to an intersection
    point.set(mapEl->center);
    map->getLane(obs_lane, point);
    dist = getDistToStopline(point, map, obs_lane);
    if (dist < 30) {
      // We have at most 4 roads going into an intersection so each vehicle
      // at the stopline has to wait at most 30 seconds
      if (mapEl->timeStopped > dist*40.0/(1.5*VEHICLE_LENGTH)) // MAGIC
        mapEl->type = ELEMENT_OBSTACLE;
    } else {
      mapEl->type = ELEMENT_OBSTACLE;
    }
  }
}
 

uint64_t LogicUtils::getTime()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
}
