#include "LogicPlanner.hh"


#include <alice/AliceConstants.h>
#include <math.h>

int LogicPlanner::init()
{
  return 0;
}

void LogicPlanner::destroy()
{
  return;
}


Err_t LogicPlanner::planLogic(vector<StateProblem_t> &problems, int prevErr, VehicleState &vehState, Map *map, Logic_params_t params)
{
  // temporary: create one state problem
  StateProblem_t problem;
  problems.push_back(problem);

  // returning LP_OK means that there is at least one problem in the array
  return LP_OK;
}
