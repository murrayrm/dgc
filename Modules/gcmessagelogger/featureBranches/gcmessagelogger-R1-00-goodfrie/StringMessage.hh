#ifndef _GCMESSAGELOGGER_STRINGMESSAGE_HH_
#define _GCMESSAGELOGGER_STRINGMESSAGE_HH_

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/string.hpp>

#include <ostream>
#include <iostream>

class Message {
public:
	Message() { }
	virtual ~Message() { }
	template<class A>
	void serialize(A &ar, const unsigned int version) {}
};

class StringMessage : public Message {
public:
	template<class ArchM>
	void serialize(ArchM &ar, const unsigned int version) {
		ar & boost::serialization::base_object<Message>(*this);
		ar & str;
	}

	std::string str;
	
	StringMessage() :
		str("") { }
	StringMessage(std::string s) :
		str(s) { }
	virtual ~StringMessage() { }

	virtual std::string toString() {
		return str;
	}
};
#endif
