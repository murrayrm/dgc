#include <fstream>
#include <iostream>
#include <pthread.h>

#include "skynettalker/SkynetTalker.hh"
#include "interfaces/sn_types.h"
#include "./StringMessage.hh"

using namespace std;

int main(int argc, char* argv[]) {
	int sn_key;
	char* fileName;
	bool terminal;
	switch (argc) {
		case 4 :
			terminal = atoi(argv[3]);
		case 3 :
			sn_key = atoi(argv[1]);
			fileName = argv[2];
			break;
		default:
			sn_key = 1;
			fileName = "testing.txt";
			terminal = true;
	}
	
	SkynetTalker<StringMessage> talker(sn_key, SNstring, MODlogger);
	StringMessage* msg = new StringMessage();

	fstream writer;
	writer.open(fileName, fstream::out | fstream::app | fstream::ate);
	talker.hasNewMessage();

	while (true) {
		if(talker.receive(msg)) {
			if (terminal) cout << msg->toString() << endl;
			writer << msg->toString() << endl;
		}
	}
	delete msg;
	writer.close();
}
