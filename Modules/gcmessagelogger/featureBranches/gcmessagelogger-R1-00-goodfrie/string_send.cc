#include <iostream>

#include "skynettalker/SkynetTalker.hh"
#include "interfaces/sn_types.h"
#include "./StringMessage.hh"

using namespace std;

int main(int argc, char* argv[]) {
	int sn_key;
	int steps = 4000;

	if (argc == 2) {
		sn_key = atoi(argv[1]);
	}
	else if (argc == 3) {
		sn_key = atoi(argv[1]);
		steps = atoi(argv[2]);
	}
	else {
		cout << "Invalid parameters: " << endl;
		cout << "	This program only takes one integer." << endl;
		sn_key = 1;
	}

	SkynetTalker<StringMessage> talker(sn_key, SNstring, MODlogger);
	StringMessage* msg = new StringMessage();
	
	{
		stringstream ss;
		ss << "ss: string-send test - " << steps << " steps";
		cout << ss.str() << endl;
		msg->str = ss.str();
		talker.send(msg);
	}
	for (int i = 0 ; i < steps ; ++i) {
		stringstream ss;
		ss << "ss: message # " << i;
		cout << ss.str() << endl;
		msg->str = ss.str();
		talker.send(msg);
	}
	msg->str = "ss: end of string-send test";
	talker.send(msg);

	delete msg;
}
