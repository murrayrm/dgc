#ifndef _LOGGER_HH_
#define _LOGGER_HH_

#ifdef LOGGER_TERMINAL
	#include <iostream>
#endif
#include <fstream>
#include <pthread.h>

using namespace std;

#include "../skynettalker/SkynetTalker.hh"
#include "../interfaces/sn_types.h"
#include "../gcmessagelogger/StringMessage.hh"

class Logger {
private:
  pthread_mutex_t mutex;
  pthread_t thr;
	int kill;
public:
  char* fileName;
  sn_msg msgType;
  int sn_key;

  Logger();
  Logger(sn_msg msgType, int sn_key);
  Logger(char* file, int key);
  Logger(char* fileName, sn_msg type, int key);
  ~Logger();

  void stop();
};

pthread_t& initLogger(char* file, sn_msg msgType, int sn_key,
			 pthread_mutex_t& mutex);

struct pthread_args_logger {
	char* fileName;
	SkynetTalker<StringMessage> talker;
	pthread_mutex_t& mutex;
	int& kill;

	pthread_args_logger(char* file, 
						SkynetTalker<StringMessage>& sntalker,
						pthread_mutex_t& mut, int& k) :
		fileName(file), talker(sntalker), mutex(mut), kill(k) { }
};

void* pthread_log (void* init);

#endif
