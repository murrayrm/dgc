#include "Logger.hh"

// Added arbitrary number for logger key
Logger::Logger() :
	kill(1) { 
		thr = initLogger(fileName, msgType, sn_key, mutex);
		fileName = "log.txt";
		msgType(SNstring);
		sn_key = 1;
	}

Logger::Logger(sn_msg type, int key) :
	kill(1) { 
		fileName = "log.txt";
		msgType = SNstring;
		thr = initLogger(fileName,msgType, sn_key, mutex);
		sn_key = key;
	}

Logger::Logger(char* file, int key) :
	kill(1) { 
		fileName = file;
		msgType = SNstring;
		thr = initLogger(fileName, msgType, sn_key, mutex); 
		sn_key = key;
	}

Logger::Logger(char* file, sn_msg type, int key) :
	kill(1) {
		fileName = file;
		msgType = type; 
		thr = initLogger(fileName, msgType, sn_key, mutex); 
		sn_key = key;
	}

Logger::~Logger() { 
	stop();
}

// Need to find a better way of stopping
void Logger::stop() {
	pthread_mutex_lock(&mutex);
	kill = 1;
	pthread_mutex_unlock(&mutex);
}
	
pthread_t& initLogger(char* file, sn_msg msgType, int sn_key,
								pthread_mutex_t& mutex, int& kill) {
	// Where do these finally get deleted?
	SkynetTalker<StringMessage>& talker
			= *(new SkynetTalker<StringMessage>(sn_key, msgType, MODlogger));
	pthread_args_logger& init = 
			*(new pthread_args_logger(file, talker, mutex, kill));

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	pthread_mutex_init(&mutex, NULL);

	pthread_t* thr;
	pthread_create(thr, &attr, pthread_log, (void*)&init);
	return *thr;
}

// If a better way of stopping this thread is found, then it would be nice to
// stop opening and closing the file every time to log.
void* pthread_log(void* init) {
	pthread_args_logger& args = *(pthread_args_logger*)init;
	StringMessage* msg;

	fstream writer;
	writer.open(args.fileName, fstream::out | fstream::app | fstream::ate);
	
	for (pthread_mutex_lock(&(args.mutex)) ; args.kill == 0 ;
			pthread_mutex_lock(&(args.mutex))) {
		pthread_mutex_unlock(&(args.mutex));
		(args.talker).receive(msg, &talkerMutex);

		writer << msg->toString() << endl;
	}
	writer.close();
	pthread_exit(NULL);
}
