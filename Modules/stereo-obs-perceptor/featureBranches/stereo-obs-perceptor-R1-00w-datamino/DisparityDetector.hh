/* Emacs clue: -*-  indent-tabs-mode:nil; c-basic-offset:4; -*-*/
#ifndef __DISPARITYDETECTOR_HH__
#define __DISPARITYDETECTOR_HH__

/*!
 * \file DisparityDetector.hh
 * \brief Obstacle detector based on stereo disparity (header file)
 *
 * \author Daniele Tamino
 * \date 10 July 2007
 *
 * This file contains the declaration of the DisparityDetector class, which given
 * a sensnet StereoImageBlob blob, containing disparity information, outputs a set
 * of obstacles. This module can track the obstacles over time, but the reset()
 * method can be called to clear the prior imformation.
 *
 */

#include <deque>
#include <vector>

// boost::ublas headers
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>

#include <bitmap/Bitmap.hh>
#include <frames/ellipse.hh>
#include <interfaces/sn_types.h>
#include "Detector.hh"
#include "GlRenderer.hh"

struct gengetopt_args_info;

namespace stereo_obs
{
    using namespace std;
    using namespace bitmap;

    namespace ublas = boost::numeric::ublas;

    typedef ublas::c_vector<double, 2> vector2_t;
    typedef ublas::c_matrix<double, 2, 2> matrix2_t;
    typedef ublas::c_vector<double, 3> vector3_t;
    typedef ublas::c_matrix<double, 3, 3> matrix3_t;
    typedef ublas::c_vector<double, 4> vector4_t;
    typedef ublas::c_matrix<double, 4, 4> matrix4_t;

    // used for tracking (state is (x, y, z, vx, vy, vz))
    typedef ublas::c_vector<double, 6> vector6_t;
    typedef ublas::c_matrix<double, 6, 6> matrix6_t;


    static inline vector2_t make_vec2(double x, double y)
    {
        vector2_t v2(2);
        v2[0] = x;
        v2[1] = y;
        return v2;
    }

    static inline vector3_t make_vec3(double x, double y, double z)
    {
        vector3_t v3(3);
        v3[0] = x;
        v3[1] = y;
        v3[1] = z;
        return v3;
    }

    static const int MAX_OBSTACLE_ID = 499;
    static const int MAX_TRACK_ID = MAX_OBSTACLE_ID - 200; // allow 200 static obs max

    enum track_flags_enum_t
    {
        TRACK_DELETED = 1,   // deleted track, empty slot
        TRACK_CONFIRMED = 2, // track was actually sent to the mapper at least once
        TRACK_UPDATED = 4,   // track was updated or deleted in the last loop
        TRACK_DYNAMIC = 8,     // track is classified as static obstacle
        TRACK_LAST = TRACK_DYNAMIC
    };

    class TrackIdPool
    {
    private:
        deque<int> m_pool;
        // IDs to be cleared (send a ELEMENT_CLEAR)
        deque<int> m_clear;

    public:

        TrackIdPool();

        /*! 
         * Remove an available track ID from the ID pool and return it. If there are no more IDs,
         * return -1.
         */
        int acquireId();
        
        /*!
         * Return a track ID to the ID pool for a later reuse. The IDs will be reused only when no
         * new IDs are available anymore, and will be pulled out in FIFO order, unless the
         * wasConfirmed parameter is set to false, which means this ID was never sent to the mapper,
         * so we can (and should) reuse it as soon as possible.
         * If track ID was confirmed, put it in the clear queue too.
         */
        void releaseId(int id, bool wasConfirmed = true);

        /*!
         * Return and removes from the queue the next ID to be cleared
         * (i.e., to be sent as a ELEMENT_CLEAR). If there are no more
         * IDs, return -1.
         */
        int popClearID();

        /*!
         * Reset the pool to its initial state.
         */
        void reset();

        /*!
         * Debugging function, prints the state of the pool to cerr
         */
        void showState();

    };

    extern TrackIdPool trackIdPool;

    struct Measure
    {
        int flags; // some TRACK_* values OR-ed together
/*
        bool deleted; // deleted track, to be ignored
        bool updated; // track was updated or deleted in the last loop
*/
        // state is in local frame
        float state[6]; // x, y, z, dx, dy, dz
        float var[6][6]; // state error covariance matrix

        float likelihood; // remove me, just use the density of probability at the mean
        float maxLikelihood;

        // bounding box containing the last seen blob(s) in disparity map frame
        //CvRect rect;

        // best fitting ellipse (local frame), used to calculate approximate
        // distances between blobs and decide about associating and merging
        // TODO: replace me with a N-dimensional ellipsoid (N = number of states)
        ellipse el;

        // geometry of the detected region, in local frame, including only the side
        // facing us, as we don't really know anything about the other side
        point2arr geometry;
        // height of the obstacle
        float height;

        Measure()
            // only initialize required fields, not everything
            : flags(0)
        { }

        // utility methods to get and set the flags

        bool get(int fl)  const { return flags & fl; }
        bool deleted()    const { return get(TRACK_DELETED); }
        bool confirmed()  const { return get(TRACK_CONFIRMED); }
        bool updated()    const { return get(TRACK_UPDATED); }
        bool isDynamic()   const { return get(TRACK_DYNAMIC); }

        void set(int fl, bool val) { flags = val ? (flags | fl) : (flags & ~fl); }
        bool deleted(bool v)   { set(TRACK_DELETED, v); return v; }
        bool confirmed(bool v) { set(TRACK_CONFIRMED, v); return v; }
        bool updated(bool v)   { set(TRACK_UPDATED, v); return v; }
        bool isDynamic(bool v)   { set(TRACK_DYNAMIC, v); return v; }

    };

    // flags used to classify a track as dynamic or static
    enum track_dyn_flags_enum_t
    {
        TRACK_DYN_CONVERGED = 1, // position has converged (variance ~ kalman asymptotic variance)
        TRACK_DYN_SIZE = 2,      // size is ok to be classified as a vehicle
        TRACK_DYN_MOVING = 4,    // it as been seeing moving over a certain threshold
        TRACK_DYN_DISPLACED = 8, // it is far from the first position where convergence was reached
        TRACK_DYN_IS_DYNAMIC = 15 // all of them
    };

    // A Track is just like a Measure but has a unique ID
    class Track : public Measure
    {
    private:
        int m_id; // unique id

        // shifting bitmask, counting how many times this track was classified as static
        // (bit = 1) versus dynamic (bit = 0). If there are more 0s than 1s, it's dynamic,
        // else it's static. Each frame it is shifted by one to the left.
        uint16_t m_staticMask;

    public:
        // or combination of TRACK_DYN_* flags
        uint16_t m_dynFlags;
        // initial position, taken when first converged
        vector3_t m_initPos;


        Track() : m_id(-1), m_staticMask(~uint16_t(0)), m_dynFlags(0) { }

        // copy constructor, will set id to -1 instead of copying it
        Track(const Track& tr)
            : Measure(tr), m_id(-1),
              m_staticMask(tr.m_staticMask), m_dynFlags(tr.m_dynFlags)
        { }

        ~Track()
        {
            if (m_id != -1)
                trackIdPool.releaseId(m_id, confirmed());
        }

        // measure to track constructor
        Track(const Measure& m);

        // measure to track assignment
        Track& operator=(const Measure& m);

        // track copy, the will set the Id to -1 instead of copying it
        Track& operator=(const Track& tr);

        // returns track id
        int id() { return m_id; }

        // release current ID and acquire a new one (should be different,
        // but can be the same in extreme cases)
        void newId();

        // override Measure::deleted() to set release the ID
        bool deleted() { return Measure::deleted(); }
        void deleted(bool v)
        {
            if (v && m_id != -1) {
                trackIdPool.releaseId(m_id);
                m_id = -1;
            }
            Measure::deleted(v);
        }

        // add the current classification (true = static) to the shifting register
        void addStaticBit(bool stat)
        {
            m_staticMask = (m_staticMask << 1) | (stat ? 1 : 0);
        }

        bool updateIsDynamic()
        {
            int votes = 0;
            uint16_t val = m_staticMask;
            // count how many bits are 1
            for (int i = 0; i < 5; i++)
            {
                votes += val & 1;
                val >>= 1;
            }
            isDynamic(votes < 3);
            return isDynamic();
        }

    };

    struct Region
    {
        int id; // unique id (from 2 to 255 inclusive)

        float pos[3]; // average (col, row, disp)
        float cov[3][3]; // covariance matrix of the region, describing its size

        // sum of the point weights that contributed to this region
        // (each point has a weight in (0, 1), even though it's always 1 at the moment)
        float weight;

        // bounding box of the region (disparity map coordinates: col, disp)
        CvRect rect;

        float row; // deprecated (use pos[1])
        float rowStDev; // deprecated (use cov[1][1])

        bool staticObs; // true if we think it's a static obstacle

        // best fitting ellipse, used to calculate approximate distances
        // between blobs and decide about associating and merging.
        // Coordinates are (map column, disparity) not (map column, map row)!
        // TODO: replace me with a N-dimensional ellipsoid (N = number of states)
        ellipse el; // DEPRECATED

        // geometry of the detected region, (map col, disp) coords, including only the side
        // facing us, as we don't really know anything about the other side
        point2arr geometry;
        
        // the geometry is defined by a range of columns, and the disparity in between
        // those columns, that is, the part of the obstacle visible to us
        //int firstCol, lastCol;
        //vector<float> disparity; // disparity for each column between firstCol, lastCol
    };


    /**
     * This class implements a detector based on the disparity map, that detects
     * generic obstacles (blobs) without any classification.
     */
    class DisparityDetector : virtual public Detector
    {
        // mutex to access this object
        pthread_mutex_t m_mtx;

        modulename moduleId; // needed to set the ID in the MapElements

        typedef float dispmap_t;
        /// map with accumulated disparity (image frame)
        Bitmap<dispmap_t, 1> m_mapImg;
        /// 2 pixel taller and wider than m_mapImg. Needed for cvFloodFill.
        Bitmap<uint8_t, 1> m_maskImg;

        /// map in local frame, kept and updated over time
        CostMap m_localMap;
        /// 2 pixel taller and wider than m_localMap. Needed for cvFloodFill.
        Bitmap<uint8_t, 1> m_localMask;
        /// temp mask used to mask cells corresponding to dispmap regions
        Bitmap<uint8_t, 1> m_tempMask;
        /// confidence for each cell (note: maybe change me into a simple times seen count?)
        CostMap m_localConf;

        dispmap_t m_mapMax; // maximum value in the current map (image frame)
        
        /// used to calculate height (image row) mean value of points in each region
        Bitmap<float, 1> m_rowSum;
        /// used to calculate height (image row) st.dev. of points in each region
        Bitmap<float, 1> m_rowSumSq;

        // transformations from/to local frame/image frame
        matrix4_t m_img2loc;
        matrix4_t m_loc2img;

        // current tracks
        vector<Track> m_tracks;
        uint64_t m_lastTs; // timestamp of the last blob used to update the tracks
        // current observations, i.e. regions detected and not filtered
        vector<Region> m_regions;
        // current observations (regions) from the local map
        vector<Region> m_localRegs;
        // current observations, transformed into local frame (i.e. from Region to Track)
        vector<Measure> m_measures;
        // used to sort tracks based on their likelihood
        typedef vector<pair<float, int> > conf_idx_vector_t;
        conf_idx_vector_t m_toSort;
        // a set of available track ids, ready to use
        deque<int> m_trackIdPool;

        // configurable parameters

        /// maximum possible disparity value
        int m_maxDisp;
        /// minimum allowed disparity value
        int m_minDisp;
        /// vertical size of the map. This determines the map resolution
        /// (e.g. maxDisp=200, mapHeight=400, resolution is 200/400 = 0.5)
        unsigned int m_mapHeight;
        /// high threshold used to detect peaks in the disparity map
        double m_highThres;
        /// low threshold used to "fill" peaks found with the high threshold
        double m_lowThres;
        /// threshold for filtering small obstacles (number of points of the
        /// disparity image tht contributed to the observation).
        float m_smallObsThres;
        /// threshold for data association (gate size)
        float m_assocThres;
        /// constant measurement error variance for (column, row, disparity) resp.
        float m_errorVar[3];
        /// Disable tracking entirely
        bool m_disableTracking;
        /// Confidence threshold
        float m_confThres;
        /// Maximum number of tracks to keep at any given time
        int m_maxTracks;
        /// Height fuzzy threshold
        float m_heightThres;
        /// Height fuzzy threshold sigma (gaussian-like)
        float m_heightThresSigma;

        /// enable the local frame costmap, used to detect static obstacles
        bool m_locMapEnable;
        /// width of the local map, in meters
        float m_locMapWidth;
        /// width of the local map, in meters
        float m_locMapHeight;
        /// resolution of the local map (meters)
        float m_locMapRes;
        /// maximum number of local map regions (smaller ones are removed if necessary)
        int m_maxLocRegs;
        /// small obstacle threshold for the local map
        float m_smallObsLoc;
        /// center of the local map, as a fraction of min(m_locMapWidth, m_locMapHeight)
        float m_locMapCenter;

        /// Initial velocity covariance for the kalman filter
        matrix3_t m_velCov0;
        /// Model error covariance Q (x(t+1) = Ax(t) + v(t) where var(v) = Q)
        matrix6_t m_modelErr;
        /// don't do classification, send everything as static
        bool m_allStatic;
        /// send only obstacles classified as static
        bool m_onlyStatic;

        /*
         * DEBUGGING STUFF
         */

        friend class ObstOverlayRenderer;
        friend class ObstacleRenderer;
        friend class ObstacleRenderer3D;
        friend class DisparityDetectorRenderer;
        friend class TuneParamsWidget;
        friend class LocalMapRenderer;
        friend class SendDebugWidget;
        friend class GroundTruthEditorWidget;

        /// if > 0, run debugging code (and print some more messages)
        int m_debug;

    public:

        DisparityDetector(gengetopt_args_info& options);

        ~DisparityDetector() { }

        /* from Detector */
        
        virtual void detect(const SensnetBlob& blob, vector<MapElement>* elements);

        virtual vector<Fl_Widget*> getWidgets();

        /*! \brief Clear all the tracks, zero out this object' state */
        virtual void reset();

    private:

        /*!
         * \brief Given the current blob, builds the projection matrix
         * from local frame to image frame, and its inverse, storing them
         * into m_sens2loc and m_loc2sens, for later use.
         */
        void buildProjMatrix(StereoImageBlob* sib);

        /*! \brief Given the current stereo blob, containing disparity information,
         * build a map accumulating vertically points with the same disparity.
         */
        void buildMap(StereoImageBlob* sib);

        /*! \brief Given the current map (m_mapImg), find connected regions */
        void findRegions(StereoImageBlob* sib);



        /*! \brief Given the local map find connected regions */
        void findRegionsLocal(StereoImageBlob* sib);

        /*! \brief Update map in local frame using the current regions */
        void updateLocalMap(StereoImageBlob* sib, vector<MapElement>* elements);



        /*! \brief Update tracks with the current measurements (regions), and fill the elements vector */
        void updateTracks(StereoImageBlob* sib, vector<MapElement>* elements);

        /* \brief Predict the position of the track given the elapsed time from
         * the last update (or prediction), in seconds.
         */
        void predict(Track* tr, double elapsed);

        /*! \brief Returns the Mahalanobis distance of traco to the region, to
         * make decisions about association.
         */
        double assocDist(const Track& track, const Region& reg);

        /*! \brief Updates the track with the given detected region */
        void updateTrackWith(Track* track, const Measure& reg, double elapsed);

        /*! \brief Merges two tracks together */
        void mergeTracks(const Track& tr1, const Track& tr2, Track* result);

        
        /*!
         * Given a point in homogeneous coordinates in sensor frame, and the transformation
         * from image to sensor (in homog coords) and sensor to local frame, it calculates
         * the variance of the point in local frame, assuming a fixed variance in image frame
         * of diag(m_errorVar).
         */
        void calcPointCov(float cov[4][4], const float pi[3],
                          const float himg2sens[4][4], const float sens2loc[4][4]);
        
        // note: m cannot be const because point2arr.setGeometry() doesn't use const (and it should)
        void measureToMapElement(Measure& m, MapElement* mel, StereoImageBlob* sib);

        // note: track cannot be const because point2arr.setGeometry() doesn't use const (and it should)
        void trackToMapElement(Track& track, MapElement* mel, StereoImageBlob* sib);

        void regionToMeasure(const Region& reg, Measure* measure, StereoImageBlob* sib,
                             const float himg2sens[4][4], const float sens2loc[4][4]);



        /*! 
         * Remove an available track ID from the ID pool and return it. If there are no more IDs,
         * return -1.
         */
        int getNewTrackId();
        
        /*!
         * Return a track ID to the ID pool for a later reuse. The IDs will be reused only when no
         * new IDs are available anymore, and will be pulled out in FIFO order, unless the
         * wasConfirmed parameter is set to false, which means this ID was never sent to the mapper,
         * so we can (and should) reuse it as soon as possible.
         */
        void releaseTrackId(int id, bool wasConfirmed = true);


        void dbg_printAllTracks(const string& msg);
    };

}

#endif
