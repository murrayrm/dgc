/*!
 * \file DisparityDetectorDebug.cc
 * \brief OpenGL Debug Display for DisparityDetector and related stuff
 *
 * \author Daniele Tamino
 * \date 17 July 2007
 *
 */

// standard C++ headers
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>
#include <map>
// boost headers
#include <boost/scoped_array.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
// FLTK headers
#include <FL/Fl_Widget.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Counter.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Pack.H>
#include <Fl/Fl_File_Chooser.H>
// OpenCV headers
#include <cv.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <frames/mat44.h>
#include <dgcutils/AutoMutex.hh>
#include <alice/AliceConstants.h>
#include <interfaces/StereoImageBlob.h>
#include <map/MapElementTalker.hh>
// local headers
#include "DisparityDetector.hh"
#include "IUpdatable.hh"
#include "util.hh"
#include "glutil.hh"

namespace stereo_obs
{
    using namespace std;
    using namespace boost;


    void mat44f_vecmul(float out[4], float m[4][4], float v[4])
    {
        out[0] = v[0]*m[0][0] + v[1]*m[0][1] + v[2]*m[0][2] + v[3]*m[0][3];
        out[1] = v[0]*m[1][0] + v[1]*m[1][1] + v[2]*m[1][2] + v[3]*m[1][3];
        out[2] = v[0]*m[2][0] + v[1]*m[2][1] + v[2]*m[2][2] + v[3]*m[2][3];
        out[3] = v[0]*m[3][0] + v[1]*m[3][1] + v[2]*m[3][2] + v[3]*m[3][3];
    }

    /*! Shows the detected obstacles, in image coordinates.
     * Should be overlayed on a ImageRenderer.
     */
    class ObstOverlayRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        int w;
        int h;

    public:
        ObstOverlayRenderer(volatile DisparityDetector& me)
            : self(me), w(1), h(1)
        { }

        void update(const SensnetBlob& blob) throw();

        /// Shows the detected obstacles as overlay boxes.
        void render() throw(Error);
    };

    void ObstOverlayRenderer::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        this->w = sib->cols;
        this->h = sib->rows;
        /* Obstacle information updated in DisparityDetector::detect() */ 
    }
    
    /// Shows the detected obstacles as overlay boxes.
    void ObstOverlayRenderer::render() throw(Error)
    {
        //AutoMutex am(self.m_mtx);
        LockingPtr<DisparityDetector> This(&self);

        vector<Region>& regions = This->m_regions;

        if (regions.size() > 0)
        {
            pushViewport();

            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/this->w, -2.0/this->h, 0);

            glDisable(GL_TEXTURE_2D);
            glDisable(GL_CULL_FACE);


            glDisable(GL_BLEND);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            for (unsigned int i = 0; i < regions.size(); i++)
            {
                //if (!useTracks && regions[i].filteredOut == true)
                //    continue;

                float x1, x2, y1, y2;
                x1 = regions[i].rect.x;
                x2 = x1 + regions[i].rect.width;
                y1 = regions[i].row - regions[i].rowStDev * 1.5;
                y2 = regions[i].row + regions[i].rowStDev * 1.5;

                glColor4f(1.0, 0.0, 0.0, 1.0);
                glBegin(GL_POLYGON);

                glVertex3f(x1, y1, 0.0);
                glVertex3f(x2, y1, 0.0);
                glVertex3f(x2, y2, 0.0);
                glVertex3f(x1, y2, 0.0);

                glEnd();
/*
                MSG("Box" << i << ":"
                    << "(" << x1 << ", " << y1 << ") "
                    << "(" << x2 << ", " << y1 << ") "
                    << "(" << x2 << ", " << y2 << ") "
                    << "(" << x1 << ", " << y2 << ") "
                    );
*/
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            popViewport();
        }
    }


    // A renderer that just sets the matrix transformation so that one can use
    // coordinates in local frame. Doesn't actually do any rendering (it's meant
    // to be chained to another renderer).
    class SetLocalFrame2DRenderer : virtual public GlRenderer, public MutexLockable
    {
        float m_width, m_height;
        VehicleState m_state;

    public:
        SetLocalFrame2DRenderer(float width, float height)
            : m_width(width), m_height(height)
        { }

        void update(const SensnetBlob& blob) throw() {
            // get the state
            StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
            m_state = sib->state;
            dirty = true;
        }

        void render() throw()
        {
            GLfloat invertXY[4*4] = {
                0, 1, 0, 0,
                1, 0, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
            };
            // set up the local frame
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            // get the viewport aspect ratio (assumes square pixels)
            int vp[4];
            glGetIntegerv(GL_VIEWPORT, vp);
            float width = vp[2];
            float height = vp[3];

            glMultMatrixf(invertXY);
            if (width < height)
                glScalef(2.0/m_width * width/height, 2.0/m_height, 1);
            else
                glScalef(2.0/m_width, 2.0/m_height * height/width, 1);
            glTranslatef(-m_state.localX, -m_state.localY, 0.0);
        }

        VehicleState& getState() { return m_state; }
    };


    static void drawArrow(point2 pos, point2 vel)
    {
        point2 norm(vel.y, -vel.x);
        norm.normalize();

        // REMOVE ME
/*
        glPointSize(2);			
        glBegin(GL_POINTS);
        glVertex3f(pos.x, pos.y, pos.z);
        glVertex3f(pos.x+vel.x, pos.y+vel.y, pos.z);
        glEnd();
        glBegin(GL_LINES);
        glVertex3f(pos.x, pos.y, pos.z);
        glVertex3f(pos.x+vel.x, pos.y+vel.y, pos.z);
        glEnd();
*/
        // matrix is in column-order, so it appears traslated
        float M[4 * 4] = {
            vel.x,   vel.y,   0,    0,
            norm.x,  norm.y,  0,    0,
            0,       0,       1,    0,
            pos.x,   pos.y,   0,    1
        };
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glMultMatrixf(M);

        glPointSize(4);
        
        // draw the dot at the position
        glBegin(GL_POINTS);
        glVertex2f(0, 0);
        glEnd();

        // draw the arrow for the velocity
        glBegin(GL_LINE_STRIP);
        glVertex2f(0, 0);
        glVertex2f(1, 0);
        glVertex2f(0.9, 0.1);
        glVertex2f(0.9, -0.1);
        glVertex2f(1, 0);
        glEnd();
        
        glPopMatrix();
        glPointSize(1); // reset to default
    }

    // copy-pasted from mapviewer/MapWindow.cc, small changes
    void drawVehicle(VehicleState &state)
    {
        if (state.timestamp<=0)
            return;

        bool xAxisRight = true;
        double ymult = 1;

        double cx = xAxisRight ? state.localX : state.localY;
        double cy = xAxisRight ? state.localY : state.localX;
        double orient = xAxisRight ? state.localYaw : M_PI/2 - state.localYaw;

        double xvel = xAxisRight ? state.localXVel : state.localYVel;
        double yvel = xAxisRight ? state.localYVel : state.localXVel;

        double dfront = DIST_REAR_AXLE_TO_FRONT;
        double drear = DIST_REAR_TO_REAR_AXLE;
        double dside = VEHICLE_WIDTH/2;

        double tmpx[4];
        double tmpy[4];
	
        double co = cos(orient);
        double so = sin(orient);

        tmpx[0] = cx+(dfront*co -dside*so);
        tmpy[0] = cy+(dfront*so +dside*co);

        tmpx[1] = cx+(-drear*co -dside*so);
        tmpy[1] = cy+(-drear*so +dside*co);

        tmpx[2] = cx+(-drear*co +dside*so);
        tmpy[2] = cy+(-drear*so -dside*co);

        tmpx[3] = cx+(dfront*co +dside*so);
        tmpy[3] = cy+(dfront*so -dside*co);

        glLineWidth(1);
        glPointSize(4);			
        glColor3f(.5, .5, .5);
        glBegin(GL_POLYGON);
        for (int i =0 ; i<4; ++i){
            glVertex2f(tmpx[i],ymult*(tmpy[i]));
        }
        glEnd();
	
        glColor3f(1, 1, 1);
        glBegin(GL_LINE_LOOP);
        for (int i =0 ; i<4; ++i){
            glVertex2f(tmpx[i],ymult*(tmpy[i]));
        }
        glEnd();
	

        //  glColor3f(1, 1, 1);
        glBegin(GL_LINE_STRIP);
        glVertex2f(cx,ymult*(cy));
        glVertex2f(cx+xvel,ymult*(cy+yvel));
        glEnd();
	
        glBegin(GL_POINTS);
        glVertex2f(cx,ymult*cy);
        glEnd();
 
        // reset defaults
        glPointSize(1);
        glLineWidth(1);
    }



    /*! Shows the obstacles in local frame, as filled polygons.
     */
    class ObstacleRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        float m_loc2veh[4][4];

        SetLocalFrame2DRenderer m_locFrameRend;

    public:
        ObstacleRenderer(volatile DisparityDetector& me)
            : self(me),
              // FIXME: this is using fields about the localmap, even if it wasn't used
              m_locFrameRend(max(float(self.m_locMapWidth) + 10, self.m_locMapCenter*2),
                             max(float(self.m_locMapHeight) + 10, self.m_locMapCenter*2))
        {  }

        void update(const SensnetBlob& blob) throw();

        /// Shows the detected obstacles as overlay boxes.
        void render() throw(Error);
    };

    void ObstacleRenderer::update(const SensnetBlob& blob) throw()
    {
        m_locFrameRend.update(blob);

        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        memcpy(m_loc2veh, sib->loc2veh, sizeof(sib->loc2veh));
        // swap x and y (first two rows)
        for (int i = 0; i < 4; i++)
            swap(m_loc2veh[0][i], m_loc2veh[1][i]);
        // zero-out z coord, we just want 2D here
        m_loc2veh[2][0] = m_loc2veh[2][1] = m_loc2veh[2][2] = m_loc2veh[2][3] = 0;
        // and the w coord too, set it to always 1.0
        m_loc2veh[3][0] = m_loc2veh[3][1] = m_loc2veh[3][2] = 0; m_loc2veh[3][3] = 1;
    }

    /// Shows the detected obstacles as polygons in local frame
    void ObstacleRenderer::render() throw(Error)
    {
        m_locFrameRend.render();
        
        glDisable(GL_DEPTH_TEST);

        float minx = -100;
        float maxx = 100;
        float miny = -100;
        float maxy = 100;

        point2 pos(m_locFrameRend.getState().localX, m_locFrameRend.getState().localY);
        point2 center(floor(pos.x*0.1)*10, floor(pos.y*0.1)*10);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(center.x, center.y, 0.0);

        // draw x axis
        glColor4f(1.0, 1.0, 1.0, 1.0); // white
        glBegin(GL_LINES);
        glVertex3f((minx + maxx)/2, miny, 0.0);
        glVertex3f((minx + maxx)/2, maxy, 0.0);
        glEnd();
        
        // draw grid
        glColor4f(0.0, 1.0, 0.0, 1.0); // green
        for (float x = minx; x <= maxx; x += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(x, miny, 0.0);
            glVertex3f(x, maxy, 0.0);
            glEnd();
        }

        for (float y = miny; y <= maxy; y += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(minx, y, 0.0);
            glVertex3f(maxx, y, 0.0);
            glEnd();
        }
        glPopMatrix();

        LockingPtr<DisparityDetector> This(&self);
        vector<Track>& tracks = This->m_tracks;
        //vector<Region>& regions = This->m_regions;
        if (tracks.size() > 0)
        {
            float m[4][4];

            for (unsigned int i = 0; i < tracks.size(); i++)
            {
                if (tracks[i].deleted())
                    continue;
                point2arr& vert = tracks[i].geometry;
                //MSG("Poly" << i << ":");
                //glColor4f(1.0, 0.3, 0.3, 0.7); // bright red
                // color from blue (unlikely) to red (very likely)
                glColor3f(tracks[i].likelihood, 0.0, 1.0 - tracks[i].likelihood);

                glBegin(GL_LINE_STRIP);
                
                for (unsigned int j = 0; j < vert.size() - 2; j++)
                {
                    glVertex4f(vert[j].x, vert[j].y, 0/*tracks[i].state[2]*/, 1);
                    //cerr << "(" << vert[j][0] << ", " << vert[j][1] << ", "
                    //     << vert[j][2]  << ", " << vert[j][3]  << ")";
                }

                glEnd();
                //MSG(endl << "Poly" << i << " END");

                // draw arrow for velocity and position
                point2 pos(tracks[i].state[0], tracks[i].state[1], tracks[i].state[2]);
                point2 vel(tracks[i].state[3], tracks[i].state[4], tracks[i].state[5]);
                if (vel.norm2() > 1e-2)
                    drawArrow(pos, vel);

            }

        }

        drawVehicle(m_locFrameRend.getState());
        glEnable(GL_DEPTH_TEST);
    }


    class LocalMapRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        bool m_showRegs;
        VehicleState m_state;

        GLuint m_texId;

        SetLocalFrame2DRenderer m_locFrameRend;

    public:
        LocalMapRenderer(volatile DisparityDetector& me, bool showRegs = true)
            : self(me), m_showRegs(showRegs), m_texId(0),
              m_locFrameRend(max(float(self.m_locMapWidth) + 10, self.m_locMapCenter*2),
                             max(float(self.m_locMapHeight) + 10, self.m_locMapCenter*2))
        {
            dirty = true;
        }

        void update(const SensnetBlob& blob) throw();

        void render() throw();
    };

    void LocalMapRenderer::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        m_state = sib->state;
        m_locFrameRend.update(blob);
        dirty = true;
    }

    void LocalMapRenderer::render() throw()
    {
        LockingPtr<DisparityDetector> This(&self);

        ////////////////////////////////////////////
        // Create the texture for the local map
        ////////////////////////////////////////////
        float maxVal = This->m_highThres;

        float scaleX = 1.0;
        float scaleY = 1.0;

        if (m_texId == 0) {
            glGenTextures(1, &m_texId);
        }

#if 0
        if (dirty && This->m_tempMask.getWidth() > 0)
        {
            CvSize sz = This->m_tempMask.toGlTexture(m_texId);
            scaleX = float(This->m_tempMask.getWidth())/sz.width;
            scaleY = float(This->m_tempMask.getHeight())/sz.height;
        }
#else
        if (dirty && This->m_localMap.getWidth() > 0)
        {
            int w = This->m_localMap.getWidth();
            int h = This->m_localMap.getHeight();
            // create texture for the map, with luminance - alpha
            scoped_array<uint8_t> mapImg(new uint8_t[w*h*4]);

            // maybe can be optimized ...
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    cost_t val = This->m_localMap[r][c];
                    uint8_t mask = This->m_localMask[r+1][c+1][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (val > maxVal) {
                        val = maxVal;
                    }
                    pix = uint8_t(val * 255 / maxVal);
                    if (val == 0) {
                        alpha = 0;
                    } else {
                        cost_t conf = This->m_localConf[r][c];
                        alpha = uint8_t(conf * 255);
                    }
                    mapImg[(r * w + c)*4 + 0] = pix;
                    mapImg[(r * w + c)*4 + 1] = 255 - pix;
                    mapImg[(r * w + c)*4 + 2] = (mask > 0) ? 0 : 255;
                    mapImg[(r * w + c)*4 + 3] = alpha;
                }
            }

            glBindTexture(GL_TEXTURE_2D, m_texId);
        
            // no interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, This->m_mapImg.getWidth(),
            //                  This->m_mapImg.getHeight(), GL_LUMINANCE_ALPHA,
            //                  GL_UNSIGNED_BYTE, mapImg.get());
            int width, height;
            int bpp = 4;
            int bpr = w * bpp;
            buildPlainTexture(w, h, bpp, bpr, GL_RGBA, GL_UNSIGNED_BYTE,
                              mapImg.get(), &width, &height);
            scaleX = float(w)/width;
            scaleY = float(h)/height;
        }
#endif

        m_locFrameRend.render();
        // translate to put the costmap in the center
        point2 mapCenter = This->m_localMap.getCenter();
        point2 alicePos = point2(m_state.localX, m_state.localY);
        glTranslatef(alicePos.x - mapCenter.x, alicePos.y - mapCenter.y, 0.0);

        glEnable(GL_TEXTURE_2D);

        ////////////////////////////////////////////
        // Draw the local cost map
        ////////////////////////////////////////////

        //pushViewport();
        glColor3f(0.0, 0.0, 0.5); // dark blue
        glBindTexture(GL_TEXTURE_2D, m_texId);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

        //glTranslatef(1.0, 0.0, 0.0);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(scaleX, scaleY, 1.0);
        point2 ul = This->m_localMap.getUpperLeft();
        point2 lr = This->m_localMap.getLowerRight();

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (ul.x, ul.y, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (lr.x, ul.y, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (lr.x, lr.y, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (ul.x, lr.y, 0.0);
        glEnd ();

        //glPopMatrix();
        //popViewport();

        // no depth text, we want the next things to overlap and blend
        glDisable (GL_DEPTH_TEST);
        // enable blending
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        vector<Region>& regions = This->m_localRegs;
        if (m_showRegs && regions.size() > 0)
        {

            glDisable(GL_TEXTURE_2D);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_CULL_FACE);

            glColor4f(1.0, 1.0, 1.0, 1.0);

            for (unsigned int i = 0; i < regions.size(); i++)
            {
                point2arr& vert = regions[i].geometry;
                glBegin(GL_POLYGON);
                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    point2 pt = This->m_localMap.toLocalFrame(vert[j]);
                    glVertex3f(pt.x, pt.y, 0.1);
                }
                glEnd();
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        
        glColor3f(1.0, 1.0, 0.0); // yellow

        glBegin(GL_POINTS);
        glVertex2f(-20, -10);
        glEnd();

        glColor3f(0.0, 1.0, 1.0); // cyan

        glBegin(GL_POINTS);
        glVertex2f(20, 10);
        glEnd();

        drawVehicle(m_state);

        glDisable (GL_BLEND);
        glEnable (GL_DEPTH_TEST);

    }


    /*! Shows the disparity map and the detected obstacles in
     * (pixel column, disparity) coordinates.
     */
    class DisparityDetectorRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        bool m_showObst;
        bool m_showRegions;
        bool m_showEdges;

        float m_disp2cell;

        GLuint m_texId;

    public:
        /**
         * Constructor. Takes a reference to the DisparityDetector object to display
         * information of, and some parameters.
         * \param me The detector object.
         * \param showPolys Show the detected obstacles as polygons overimposed on the map
         * \param showRegions Show the detected regions with different colors instead of the
         * grayscale map.
         */
        DisparityDetectorRenderer(volatile DisparityDetector& me,
                                  bool showObst = true, bool showRegions = false,
                                  bool showEdges = false)
            : self(me), m_showObst(showObst), m_showRegions(showRegions),
              m_showEdges(showEdges), m_texId(0)
        { }

        void update(const SensnetBlob& blob) throw();

        void render() throw(Error);
    };

// FIXME: just fix me!!! :-P
#define DRAW_MAP 1
#define DRAW_MASK 0
#define DRAW_DEBUG_IMAGE 0 // is this used for anything anymore?
#define DRAW_EDGES 0 // actually, edge points
#define DRAW_OBSTACLES_DISPFRAME 0
    void DisparityDetectorRenderer::update(const SensnetBlob& /*blob*/) throw()
    {
        dirty = true;
    }

    void DisparityDetectorRenderer::render() throw(Error)
    {
        LockingPtr<DisparityDetector> This(&self);
        // conversion from disparity image to map cell index
        float disp2cell = float(This->m_mapHeight) / This->m_maxDisp;
        //AutoMutex am(m_mtx);

        // todo: check for errors!! (not a big deal actually, this is just for debugging)
        if (m_texId == 0) {
            glGenTextures(1, &m_texId);
        }
        
        float scaleX = 1.0;
        float scaleY = 1.0;

        ////////////////////////////////////////////
        // Create the texture for the disparity map
        ////////////////////////////////////////////

        if (This->m_mapMax == 0) {
            This->m_mapMax = 1; // avoid divide-by-zero
        }

        if (!m_showRegions && This->m_mapImg.getWidth() > 0) {
            int w = This->m_mapImg.getWidth();
            int h = This->m_mapImg.getHeight();
            float lowTh = This->m_lowThres;
            float highTh = This->m_highThres;
            Bitmap<uint8_t, 4> mapImg(w, h);
            typedef DisparityDetector::dispmap_t dispmap_t;
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    dispmap_t val = This->m_mapImg[r][c][0];
                    uint8_t maskval = This->m_maskImg[r+1][c+1][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (val == 0) {
                        pix = alpha = 0;
                        alpha = 0;
                    } else {
                        pix = uint8_t(val * 256 / This->m_mapMax);
                    }
/*
                    uint8_t rgb[4] = {0, 0, 0};
                    if (val < lowTh) rgb[1] = 255; // green
                    else if (val < highTh) rgb[0] = rgb[2] = 255; // magenta
                    else rgb[0] = 255; // red
*/
                    mapImg[r][c][0] = maskval ? 255 : pix;
                    mapImg[r][c][1] = maskval ? 0 : 255 - pix;
                    mapImg[r][c][2] = maskval ? pix : 0;
                    mapImg[r][c][3] = alpha;
                }
            }
            CvSize sz = mapImg.toGlTexture(m_texId);
            scaleX = float(This->m_maskImg.getWidth())/sz.width;
            scaleY = float(This->m_maskImg.getHeight())/sz.height;
#if 0
            // create texture for the map, with luminance - alpha
            // TODO: move this in the class declaration to avoid reallocation
            scoped_array<uint8_t> mapImg(new uint8_t[This->m_mapImg.getWidth()*This->m_mapImg.getHeight()*2]);

            typedef DisparityDetector::dispmap_t dispmap_t;
            // maybe can be optimized ...
            int w = This->m_mapImg.getWidth();
            int h = This->m_mapImg.getHeight();
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    dispmap_t val = This->m_mapImg[r][c][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (/*val < 64 && val >= 0*/ true) {
                        //pix = val * 256 / 64; // this will be optimized by the compiler
                        pix = uint8_t(val * 256 / This->m_mapMax);
                        if (val == 0) {
                            alpha = 0;
                        }
                    } else {
                        pix = 255;
                    }
                    mapImg[(r * w + c)*2] = pix;
                    mapImg[(r * w + c)*2 + 1] = alpha;
                }
            }

            glBindTexture(GL_TEXTURE_2D, m_texId);
        
            // no interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, This->m_mapImg.getWidth(),
            //                  This->m_mapImg.getHeight(), GL_LUMINANCE_ALPHA,
            //                  GL_UNSIGNED_BYTE, mapImg.get());
            int width, height;
            int bpp = 2;
            int bpr = w * bpp;
            buildPlainTexture(This->m_mapImg.getWidth(), This->m_mapImg.getHeight(), bpp, bpr,
                              GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, mapImg.get(),
                              &width, &height);
            scaleX = float(This->m_mapImg.getWidth())/width;
            scaleY = float(This->m_mapImg.getHeight())/height;
#endif
        }

        if (m_showRegions && This->m_maskImg.getWidth() > 0) {
            CvMat sub;
            IplImage imgHdr;
            IplImage* mask = NULL;
            cvGetSubRect(This->m_maskImg.getImage(), &sub,
                         cvRect(1, 1, This->m_maskImg.getWidth()-1, This->m_maskImg.getHeight()-1));
            mask = cvGetImage(&sub, &imgHdr);

            CvSize sz = BitmapBase::toGlTexture(mask, m_texId, GL_ALPHA);
            scaleX = float(This->m_maskImg.getWidth())/sz.width;
            scaleY = float(This->m_maskImg.getHeight())/sz.height;
        }

        glEnable(GL_TEXTURE_2D);

        ////////////////////////////////////////////
        // Draw the disparity map or the regions (whichever was selected)
        ////////////////////////////////////////////

        pushViewport();
        glColor3f(0.0, 0.0, 0.5); // dark blue
        glBindTexture(GL_TEXTURE_2D, m_texId);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

        //glTranslatef(1.0, 0.0, 0.0);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(scaleX, scaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (1.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

        // no depth text, we want the next images to overlap and blend
        glDisable (GL_DEPTH_TEST);
        // enable blending
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if DRAW_OBSTACLES_DISPFRAME
        vector<Region>& regions = This->m_regions;
        if (regions.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            //int vp[4];
            //glGetIntegerv(GL_VIEWPORT, vp);
            //glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            // image coordinates
            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/This->m_mapImg.getWidth(), -2.0/This->m_mapImg.getHeight(), 1.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_CULL_FACE);

            for (unsigned int i = 0; i < regions.size(); i++)
            {
                if (regions[i].staticObs)
                    glColor4f(1.0, 5.0, 0.0, 1.0);
                else
                    glColor4f(1.0, 1.0, 1.0, 1.0);


                point2arr& vert = regions[i].geometry;
                glBegin(GL_POLYGON);
                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    glVertex3f(vert[j].x, vert[j].y * disp2cell, 0.1);
                }
                glEnd();
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            //glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
        }
#endif

        glDisable (GL_BLEND);
        glEnable (GL_DEPTH_TEST);

    }

    /*! Shows the obstacles and the measures in local frame, as 3D objects
     */
    class ObstacleRenderer3D : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        float m_loc2veh[4][4]; // transformation from local frame to vehicle frame
        float m_veh2cam[4][4]; // transformation from vehicle frame to virtual camera frame
        float m_veh2loc[4][4]; // transformation from vehicle frame to local frame
        float m_sx, m_sy; // camera x ad y focus
        float m_near, m_far; // opengl near and far clipping planes
        point3 m_color;
        bool m_useMeasures;
        point3 m_alicePos; // local frame

    public:
        ObstacleRenderer3D(volatile DisparityDetector& me,
                           point3 color = makePoint3(1.0, 0.0, 0.0) /* RGB */,
                           bool measures = false,
                           float near = 10.0, float far = 200.0)
            : self(me), m_sx(1), m_sy(1),
              m_near(near), m_far(far),
              m_color(color), m_useMeasures(measures)
        {
            // set loc2veh to the identity matrix
            mat44f_zero(m_loc2veh);
            m_loc2veh[0][0] = m_loc2veh[1][1] = m_loc2veh[2][2] = m_loc2veh[3][3] = 1;
            // same for veh2cam
            mat44f_zero(m_veh2cam);
            m_veh2cam[0][0] = m_veh2cam[1][1] = m_veh2cam[2][2] = m_veh2cam[3][3] = 1;
            m_alicePos[0] = m_alicePos[1] = m_alicePos[2] = 0;
        }

        void update(const SensnetBlob& blob) throw();

        void render() throw();
    };

    void ObstacleRenderer3D::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        memcpy(m_loc2veh, sib->loc2veh, sizeof(sib->loc2veh));
        memcpy(m_veh2loc, sib->veh2loc, sizeof(sib->veh2loc));
        // at the moment, the virtual camera is the same as the real camera
        // in the future, the virtual camera could be moved using the mouse, so
        // this will change then
        memcpy(m_veh2cam, sib->leftCamera.veh2sens, sizeof(m_veh2cam));
        // normalize sx and sy for a [-1, 1] range
        m_sx = sib->leftCamera.sx * 2 / sib->cols;
        m_sy = sib->leftCamera.sy * 2 / sib->rows;
        m_alicePos[0] = sib->state.localX;
        m_alicePos[1] = sib->state.localY;
        m_alicePos[2] = sib->state.localZ;
    }

    void ObstacleRenderer3D::render() throw()
    {
        float loc2cam[4][4];
        mat44f_mul(loc2cam, m_veh2cam, m_loc2veh);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        
        glLoadIdentity();
        float minx = -100;
        float maxx = 100;
        float miny = -100;
        float maxy = 100;
        
        float from[4], to[4];
        from[0] = -15.0;
        from[1] =  0.0;
        from[2] = -15.0;
        from[3] =  1.0;
        to[0] = 15.0;
        to[1] =  0.0;
        to[2] =  0.0;
        to[3] =  1.0;
        
        float fromLoc[4], toLoc[4];
        mat44f_vecmul(fromLoc, m_veh2loc, from);
        mat44f_vecmul(toLoc, m_veh2loc, to);
        gluLookAt(fromLoc[0], fromLoc[1], fromLoc[2],
                  toLoc[0], toLoc[1], toLoc[2],
                  0, 0, -1);
        
/*
        float tmp[4][4]; 
        //mat44f_trans(tmp, loc2cam); // opengl wants column-major matrices
        mat44f_trans(tmp, m_loc2veh); // opengl wants column-major matrices
        // swap x and y (first two columns)
        for (int i = 0; i < 4; i++)
            swap(tmp[i][0], tmp[i][1]);
        glMultMatrixf((const GLfloat*)tmp);

        float proj[4][4] = {
            { m_sx, 0,     0,  0},
            { 0,    m_sy,  0,  0},
            { 0,    0,     (m_near + m_far)/(m_near - m_far),  (2*m_far*m_near)/(m_near - m_far) },
            { 0,    0,    -1,  0},
        };

*/

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        //mat44f_trans(proj, proj); // opengl wants column-major matrices
        //glLoadMatrixf((const GLfloat*)proj);
        //gluPerspective(42.0, 4.0/3.0, m_near, m_far);
        gluPerspective(80.0, 4.0/3.0, m_near, m_far);

        point3 gridc; // grid center
        gridc[0] = 10 * floor(m_alicePos[0] / 10.0 + 0.5); // round to the nearest multiple of 10
        gridc[1] = 10 * floor(m_alicePos[1] / 10.0 + 0.5); // round to the nearest multiple of 10
        gridc[2] = m_alicePos[2];

        // draw x axis
        glColor4f(1.0, 1.0, 1.0, 1.0); // white
        glBegin(GL_LINES);
        glVertex3f(gridc[0] + (minx + maxx)/2, gridc[1] + miny, gridc[2]);
        glVertex3f(gridc[0] + (minx + maxx)/2,  gridc[1] + maxy, gridc[2]);
        glEnd();
        
        // draw grid
        glColor4f(0.0, 1.0, 0.0, 1.0); // green
        for (float x = minx; x <= maxx; x += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(gridc[0] + x, gridc[1] + miny, gridc[2]);
            glVertex3f(gridc[0] + x, gridc[1] + maxy, gridc[2]);
            glEnd();
        }
        
        for (float y = miny; y <= maxy; y += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(gridc[0] + minx, gridc[1] + y, gridc[2]);
            glVertex3f(gridc[0] + maxx, gridc[1] + y, gridc[2]);
            glEnd();
        }

        // lock the detector object - don't do it before as it's not needed
        LockingPtr<DisparityDetector> This(&self);

        vector<Track> tracks;
        if (m_useMeasures)
        {
            // deep copy (heavyweight)
            tracks.resize(This->m_measures.size());
            copy(This->m_measures.begin(), This->m_measures.end(), tracks.begin());
        }
        else
            tracks = This->m_tracks; // this should be a copy-on-write (lightweight)

/*
        float mat[4][4];
        MSG("MODELVIEW MATRIX:");
        glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
        printGlMatrix(mat);

        MSG("PROJECTION MATRIX:");
        glGetFloatv(GL_PROJECTION_MATRIX, (float*) mat);
        printGlMatrix(mat);
*/
        //glColor3f(m_color[0], m_color[1], m_color[2]);

#if 0
        glEnable(GL_LIGHTING);
        glShadeModel(GL_FLAT);
        float global_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

        // Create light components
        float ambientLight[] = { 1.0, 1.0, 1.0, 1.0 };
        float diffuseLight[] = { 0.8, 0.8, 0.8, 1.0 };
        float specularLight[] = { 0.5, 0.5, 0.5, 1.0 };
        float positionVeh[] = { -5000, 5000, -5000.0, 1.0 };
        float position[4];
        mat44f_vecmul(position, m_veh2loc, positionVeh);
        
        // Assign created components to GL_LIGHT0
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
        glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
        glLightfv(GL_LIGHT0, GL_POSITION, position);
#endif

        for (int n = 0; n < 2; n++) {
        
            if (n == 0) {
                glColor3f(1.0, 1.0, 1.0);
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // wireframe mode
            } else {
                glPolygonMode(GL_FRONT, GL_FILL); // solid mode
            }
            
            for (unsigned int i = 0; i < tracks.size(); i++)
            {
                if (tracks[i].deleted())
                    continue;
                if (n == 1) {
                    glColor3f(tracks[i].likelihood, 0.0, 1.0 - tracks[i].likelihood);
                }
                float lowZ = tracks[i].state[2] - tracks[i].height/2;
                float highZ = tracks[i].state[2] + tracks[i].height/2;
                point2arr& points = tracks[i].geometry;
                assert(points.size() > 0);

                //glColor3f(float(rand())/RAND_MAX, float(rand())/RAND_MAX, float(rand())/RAND_MAX);
                glBegin(GL_QUAD_STRIP);
                //MSG("Obstacle3D: quad strip begin: lowZ=" << lowZ << ", highZ=" << highZ
                //    << ", height=" << tracks[i].height);
                for (unsigned int j = 0; j < points.size(); j++)
                {
                    glVertex3f(points[j].x, points[j].y, lowZ);
                    glVertex3f(points[j].x, points[j].y, highZ);
                    //cerr << '(' << points[j].x << ", " << points[j].y << ')';
                }
                // close the strip
                //cerr << '(' << points[0].x << ", " << points[0].y << ')' << endl;
                glVertex3f(points[0].x, points[0].y, lowZ);
                glVertex3f(points[0].x, points[0].y, highZ);
 
                //MSG("Obstacle3D: quad strip endn");
                glEnd();
            }
        } 
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // solid mode, both faces

        // set up vehicle frame
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        gluLookAt(from[0], from[1], from[2],
                  to[0], to[1], to[2],
                  0, 0, -1);

        // render alice
        glColor3f(1.0, 1.0, 0.0);
        glBegin(GL_QUAD_STRIP);
        glVertex3f(3, 3, 0.0);
        glVertex3f(3, 3, -3.0);

        glVertex3f(-3, 3, 0.0);
        glVertex3f(-3, 3, -3.0);

        glVertex3f(-3, -3, 0.0);
        glVertex3f(-3, -3, -3.0);

        glVertex3f(3, -3, 0.0);
        glVertex3f(3, -3, -3.0);

        glVertex3f(3, 3, 0.0);
        glVertex3f(3, 3, -3.0);

        glEnd();
        glPopMatrix();

        // draw translucent ground plane - do this at the end, as otherwise it will
        // block other object via the depth buffer
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // solid mode, both faces
        glEnable(GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glColor4f(0.6, 0.6, 0.6, 0.8);
        glBegin(GL_QUADS);

        glVertex3f(gridc[0] + minx, gridc[1] + miny, gridc[2]);
        glVertex3f(gridc[0] + maxx, gridc[1] + miny, gridc[2]);
        glVertex3f(gridc[0] + maxx, gridc[1] + maxy, gridc[2]);
        glVertex3f(gridc[0] + minx, gridc[1] + maxy, gridc[2]);
        
        glEnd();
        glDisable(GL_BLEND);

        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
    }

    /*!
     * This widget allows to tweak detector parameters on the fly without restarting
     * the main program.
     */
    class TuneParamsWidget : public Fl_Group
    {
        typedef Fl_Group base_t;
        typedef TuneParamsWidget self_t;

        // a widget to show and change the value of a variable
        template<typename T, typename Valuator>
        class InspectVarWidget : public Fl_Group
        {
            typedef Fl_Group base_t;
            typedef InspectVarWidget<T, Valuator> self_t;

            Valuator m_widget;
            volatile T& m_value;

        public:
            InspectVarWidget(volatile T& val, int x, int y, int w, int h, const char* label = NULL)
                : base_t(x, y, w, h, label),
                  m_widget(x, y, w, h),
                  m_value(val)
            {
                m_widget.callback(self_t::static_callback);
                end();
            }

            // assign the value in the slider to the actual variable
            void commit()
            {
                m_value = m_widget.value(); // convert from double to T
                m_widget.clear_changed();
                m_widget.redraw();
            }

            void range(const T& start, const T& end)
            {
                m_widget.range(start, end);
                // enforce range, in case it's not done already
                if (m_widget.value() < start)
                    m_widget.value(start);
                else if (m_widget.value() > end)
                    m_widget.value(end);
            }

        protected:
            
            virtual void draw()
            {
                if (!changed()) {
                    m_widget.value(m_value); // set value, convert from T to double
                    m_widget.color(FL_BACKGROUND2_COLOR);
                } else {
                    // display that the value was changed
                    m_widget.color(FL_YELLOW);
                }
                base_t::draw();
            }

            virtual int handle(int ev)
            {
                if (ev == FL_KEYDOWN && (Fl::event_key() == FL_Enter ||
                                         Fl::event_key() == FL_KP_Enter))
                {
                    do_callback();
                    return 1;
                }
                return base_t::handle(ev);
            }

        private:
            static void static_callback(Fl_Widget *w, void * /*data*/)
            {
                self_t* self = dynamic_cast<self_t*>(w->parent());
                assert(self != NULL);
                self->set_changed();
                self->m_widget.redraw(); // draw the yellow background
            }

        }; // end of class GenericValuator

        template <typename T> struct widget_t {
            //typedef InspectVarWidget<T, Fl_Counter> type;
            typedef InspectVarWidget<T, Fl_Value_Input> type;
        };

        template <typename T> struct callback_t {
            typedef void (self_t::*type)(typename widget_t<T>::type* w);
        };

        typedef widget_t<double>::type widDouble_t;
        typedef widget_t<float>::type widFloat_t;

        volatile DisparityDetector& m_dd;
        widDouble_t* m_widHigh;
        widDouble_t* m_widLow;
        widFloat_t* m_widSmall;
        widFloat_t* m_widAssoc;
        widFloat_t* m_widHeight;

    public:

        TuneParamsWidget(DisparityDetector& dd, int x, int y, int w, int h,
                         const char *label = NULL)
            : Fl_Group(x, y, w, h, label),
              m_dd(dd)
        {
            {
                {
                    Fl_Scroll* o = new Fl_Scroll(x, y, w, h);
                    int curY = y;
                    //Fl_Pack* o = new Fl_Pack(x, y + 10, w, h);
                    //o->type(Fl_Pack::VERTICAL);
                    //o->spacing(15);
                    m_widHigh = new widDouble_t(m_dd.m_highThres, x, curY, 100, 20, "High threshold");
                    m_widHigh->callback(self_t::static_hl_callback);
                    m_widHigh->align(FL_ALIGN_RIGHT);
                    curY += 40;
                        
                    m_widLow = new widDouble_t(m_dd.m_lowThres, x, curY, 100, 20, "Low threshold");
                    m_widLow->callback(self_t::static_hl_callback);
                    m_widLow->align(FL_ALIGN_RIGHT);
                    curY += 40;
                
                    m_widSmall = new widFloat_t(m_dd.m_smallObsThres, x, curY, 100, 20, "Small obs threshold");
                    m_widSmall->callback(self_t::static_callback<float>);
                    m_widSmall->range(0, numeric_limits<float>::max());
                    m_widSmall->align(FL_ALIGN_RIGHT);
                    curY += 40;

                    m_widAssoc = new widFloat_t(m_dd.m_assocThres, x, curY, 100, 20, "Assoc threshold");
                    m_widAssoc->callback(self_t::static_callback<float>);
                    m_widAssoc->range(0, numeric_limits<float>::max());
                    m_widAssoc->align(FL_ALIGN_RIGHT);
                    curY += 40;

                    m_widHeight = new widFloat_t(m_dd.m_heightThres, x, curY, 100, 20, "Height threshold");
                    m_widHeight->callback(self_t::static_callback<float>);
                    m_widHeight->range(0, numeric_limits<float>::max());
                    m_widHeight->align(FL_ALIGN_RIGHT);
                    curY += 40;
                    // add more ...
                        
                    o->end();
                }
            }
            end();
        }

    private:

        template <typename T>
        static void static_callback_helper(Fl_Widget *w, typename callback_t<T>::type cb)
        {
            self_t* self = dynamic_cast<self_t*>(w->parent()->parent());
            assert(self != NULL);
            typename widget_t<T>::type* wid = dynamic_cast<typename widget_t<T>::type*>(w);
            assert(wid != NULL);
            (self->*cb)(wid);
        }

        template <typename T>
        static void static_callback(Fl_Widget *w, void*)
        {
            static_callback_helper<T>(w, &self_t::myCallback<T>);
        }

        static void static_hl_callback(Fl_Widget *w, void*)
        {
            static_callback_helper<double>(w, &self_t::high_low_callback);
        }

        template <typename T>
        void myCallback(typename widget_t<T>::type* w)
        {
            // lock the DisparityDetector object before changing it!!!
            LockingPtr<DisparityDetector> dd(&m_dd);
            w->commit();
        }

        void high_low_callback(widDouble_t* w)
        {
            // lock the DisparityDetector object before changing it!!!
            LockingPtr<DisparityDetector> dd(&m_dd);

            // make sure the ranges are set correctly
            m_widHigh->range(dd->m_lowThres, 1000.0);
            m_widLow->range(0, dd->m_highThres);

            // now, copy the value to the DisparityDetector object
            w->commit();
        }

    };


    /*! This class is just to test OpenGL commands.
     */
    class TestRenderer : virtual public GlRenderer, public MutexLockable
    {
        float m_loc2veh[4][4];

    public:

        TestRenderer() {
            mat44f_zero(m_loc2veh);
            m_loc2veh[0][0] = m_loc2veh[1][1] = m_loc2veh[2][2] = m_loc2veh[3][3] = 1;
        }

        void update(const SensnetBlob& blob) throw()
        {
            StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
            memcpy(m_loc2veh, sib->loc2veh, sizeof(m_loc2veh));
            mat44f_trans(m_loc2veh, m_loc2veh);
        }

        void render() throw()
        {
            float mat[4][4];
            glMatrixMode(GL_MODELVIEW);

            //cerr << "TEST: Previous Mat:" << endl;
            //glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
            //printGlMatrix(mat);

            glPushMatrix();

            gluLookAt(0, 0, -1,  0, 0, 0,  1, 0, 0);
            //cerr << "TEST: LookAt matrix:" << endl;
            //glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
            //printGlMatrix(mat);
            glPushMatrix();

            glDisable(GL_DEPTH_TEST);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // wireframe mode

            float p1[4][4] = {
                { -0.6,  0.3, 0.0, 1.0 },
                { -0.5, -0.4, 0.0, 1.0 },
                {  0.6, -0.3, 0.0, 1.0 },
                {  0.5,  0.4, 0.0, 1.0 },
            };

            glColor3f(1.0, 0.0, 0.0);
            drawQuad(p1);

/*
            float p2[4][4] = {
                { -0.7,  0.4, 0.0, 1.0 },
                { -0.6, -0.5, 0.0, 1.0 },
                {  0.7, -0.4, 0.0, 1.0 },
                {  0.6,  0.5, 0.0, 1.0 },
            };
*/
            float p2[4][4] = {
                {41.135744, 80.112096, -114.062375, 1.000000},
                {40.300427, 79.765620, -114.015474, 1.000000},
                {40.759038, 78.545106, -114.011078, 1.000000},
                {41.594355, 78.891582, -114.057979, 1.000000},
            };

/*
            float p2v[4][4];
            float loc2veh[4][4];
            mat44f_trans(loc2veh, m_loc2veh); // use normal matrix order for our use!

            for (int i = 0; i < 4; i++)
                mat44f_vecmul(p2v[i], loc2veh, p2[i]);

            glLoadIdentity();
            glColor3f(0.0, 0.0, 1.0);
            drawQuad(p2v);
*/
            glLoadMatrixf((float*)m_loc2veh);

            cerr << "TEST: loc2veh matrix:" << endl;
            glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
            printGlMatrix(mat);
            cerr << "TEST: projection matrix:" << endl;
            glGetFloatv(GL_PROJECTION_MATRIX, (float*) mat);
            printGlMatrix(mat);

            glColor3f(0.0, 1.0, 0.0);
            drawQuad(p2);

            glPopMatrix();
        }

    private:
        void drawQuad(float p[4][4])
        {
            char text[64];

            glBegin(GL_QUADS);

            for (int i = 0; i < 4; i++) {
                //glVertex4f(p[i][0], p[i][1], p[i][2], p[i][3]);
                glVertex3f(p[i][0], p[i][1], p[i][2]);
            }

            glEnd();

            glColor3f(1.0, 1.0, 1.0);
            gl_font(FL_HELVETICA, 12);
            cerr << "TEST: quad ";
            for (int i = 0; i < 4; i++) {
                snprintf(text, sizeof(text), "(%.2f, %.2f)", p[i][0], p[i][1]);
                // use RasterPos instead of gl_draw(text, x, y) so we can use all the 4 coords
                // and have them transformed using the current opengl matrix
                glRasterPos4f(p[i][0], p[i][1], p[i][2], p[i][3]);
                gl_draw(text);
                cerr << "(" << p[i][0] << ", " << p[i][1] << ", "
                     << p[i][2]  << ", " << p[i][3]  << ")";
            }
            cerr << endl;
        }

    };

    class SendDebugWidget : public Fl_Group, public IUpdatable, public MutexLockable
    {
        volatile DisparityDetector& m_self;
        CMapElementTalker m_talker;
        int m_subgroup;

        struct TrackData
        {
            int id;
            bool deleted;
            point2arr traj;
            point2arr velTraj;
            // ...
            TrackData() : id(-1), deleted(false) { }
        };

        vector<TrackData> m_trData;

    public:

        SendDebugWidget(int skynetKey, int sendGroup, volatile DisparityDetector& self)
            : Fl_Group(0, 0, 100, 100, "Send debug"),
              m_self(self), m_subgroup(sendGroup)
        {
            end();
            m_talker.initSendMapElement(skynetKey);
            LockingPtr<DisparityDetector> This(&m_self);
            m_trData.resize(This->m_tracks.size());
        }

        void update(const SensnetBlob& /*blob*/) throw()
        {
            MapElement me, clearEl;
            me.setTypeDebug();
            me.geometryType = GEOMETRY_LINE;
            me.setColor(MAP_COLOR_PINK);
            clearEl.setTypeClear();
            {
                LockingPtr<DisparityDetector> self(&m_self);
                assert(m_trData.size() == self->m_tracks.size());

                for (int i = 0; i < int(m_trData.size()); i++)
                {
                    Track* tr = &self->m_tracks[i];
                    TrackData* td = &m_trData[i];
                    if (tr->deleted()) {
                        td->deleted = true;
                        td->traj.clear();
                        td->velTraj.clear();
                    } else {
                        td->deleted = false;
                        if (td->id != tr->id()) {
                            td->traj.clear();
                            td->velTraj.clear();
                            clearEl.setId(0, td->id);
                            m_talker.sendMapElement(&clearEl, m_subgroup);
                            td->id = tr->id();
                        }
                        point2 pos = point2(tr->state[0], tr->state[1], tr->state[2]);
                        point2 vel = point2(tr->state[3], tr->state[4], tr->state[5]);
                        if (true || td->traj.size() == 0 || (pos - td->traj.arr.back()).norm2() > 1)
                        {
                            td->traj.push_back(pos);
                            td->velTraj.push_back(vel);
                        }
                    }
                }
            } // release lock

            // send all trajs
            for (int i = 0; i < int(m_trData.size()); i++)
            {
                TrackData* td = &m_trData[i];
                if (td->deleted) {
                    clearEl.setId(0, td->id);
                    m_talker.sendMapElement(&clearEl, m_subgroup);
                } else {
                    me.setId(0, td->id);
                    me.geometry = td->traj;
                    m_talker.sendMapElement(&me, m_subgroup);
                }
            }
        }
    };

    Fl_Widget* createSendDebugWidget(int skynetKey, int subgroup, volatile DisparityDetector& det)
    {
        return new SendDebugWidget(skynetKey, subgroup, det);
    }



    // ugly, copied from DisparityDetector, but don't have time :/
    typedef ublas::c_vector<double, 4> vector4_t;
    typedef ublas::c_matrix<double, 4, 4> matrix4_t;
    void buildProjMatrix(StereoImageBlob* sib, matrix4_t* img2loc,
                         matrix4_t* loc2img)
    {
        // calculate transformation matrix from image to sensor in homogeneous
        // coordinates (col, row, disp, 1) -> (Xs, Ys, Zs, Ws)
        float sx = sib->leftCamera.sx;
        float sy = sib->leftCamera.sy;
        float cx = sib->leftCamera.cx;
        float cy = sib->leftCamera.cy;
        float b = sib->baseline;

        // projection matrix with disparity
        // alpha*(col, row, disp, 1)' = sens2img * (Xs, Ys, Zs, Ws)'
        const float sens2img_arr[4*4] = {
            sx,   0,     cx,    0,
            0,    sy,    cy,    0,
            0,    0,     0,     sx*b,
            0,    0,     1,     0
        };
        matrix4_t sens2img;
        copy(sens2img_arr, sens2img_arr+16, sens2img.data());

        // x = (c - cx) * baseline / d
        // y = (r - cy) * (sy/sx) * baseline / d
        // z = sx * baseline / d;
        const float img2sens_arr[4*4] = {
            b,    0,        0,       -cx*b,
            0,    b*sx/sy,  0,       -cy*b*sx/sy,
            0,    0,        0,          sx*b,
            0,    0,        1,          0
        };
        matrix4_t img2sens;
        copy(img2sens_arr, img2sens_arr+16, img2sens.data());

        // copy matrices from the blob into ublas matrices
        matrix4_t veh2loc, loc2veh;
        copy(&sib->veh2loc[0][0], &sib->veh2loc[3][4], veh2loc.data());
        copy(&sib->loc2veh[0][0], &sib->loc2veh[3][4], loc2veh.data());
        matrix4_t sens2veh, veh2sens;
        copy(&sib->leftCamera.sens2veh[0][0], &sib->leftCamera.sens2veh[3][4], sens2veh.data());
        copy(&sib->leftCamera.veh2sens[0][0], &sib->leftCamera.veh2sens[3][4], veh2sens.data());

        matrix4_t loc2sens(prod(veh2sens, loc2veh));
        if (loc2img)
            *loc2img = prod(sens2img, loc2sens);

        //bool singular;
        //m_img2loc = invert(m_loc2img, &singular);
        //assert(singular == FALSE);
        matrix4_t sens2loc(prod(veh2loc, sens2veh));
        if (img2loc)
            *img2loc = prod(sens2loc, img2sens);
    }

    class GroundTruthEditorWidget : public Fl_Group, public IUpdatable, public MutexLockable
    {
        typedef Fl_Group base_t;
        typedef GroundTruthEditorWidget self_t;

        volatile DisparityDetector& m_ddet;

        struct GTObstacle
        {
            float x, y, w, h; // rectangle in the left image
            float disp; // disparity
            float r, g, b; // color
            bool falseAlarm; // wrongly detected
            bool autoDet; // true = automatically detected

            GTObstacle() { memset((void*) this, 0, sizeof(*this)); }

            void setColor(float r, float g, float b)
            {
                this->r = r; this->g = g; this->b = b;
            }

            float dist(float px, float py) const
            {
                float dx, dy, d;
                if (px < x) {
                    dx = x - px;
                    if (py < y) {
                        dy = y - py;
                        d = sqrt(dx*dx + dy*dy);
                    } else if (py < y + h) {
                        d = dx;
                    } else {
                        dy = py - y - h;
                        d = sqrt(dx*dx + dy*dy);
                    }
                } else if (px < x + w) {
                    dx = min(px - x, x + w - px);
                    if (py < y) {
                        dy = y - py;
                        d = dy;
                    } else if (py < y + h) {
                        dy = min(py - y, y + h - py);
                        d = min(dx, dy);
                    } else {
                        dy = py - y - h;
                        d = dy;
                    }
                } else {
                    dx = px - x - w;
                    if (py < y) {
                        dy = y - py;
                        d = sqrt(dx*dx + dy*dy);
                    } else if (py < y + h) {
                        d = dx;
                    } else {
                        dy = py - y - h;
                        d = sqrt(dx*dx + dy*dy);
                    }
                }
                return d;
            }

            template <class OStr>
            friend OStr& operator <<(OStr& os, const GTObstacle& obs)
            {
                os << "(" << obs.x << "," << obs.y << ")-" << obs.w << "x" << obs.h
                   << " disp=" << obs.disp << " color=(" << obs.r << "," << obs.g << "," << obs.b
                   << ") " << (obs.falseAlarm ? "!" : "");
                return os;
            }
        };

        class RectRenderer : public GlRenderer, public MutexLockable
        {
        protected:
            int m_w;
            int m_h;
            bool m_right; // is this on the right image?
            const vector<GTObstacle>* m_obsVec;
            pthread_mutex_t* m_obsVecMtx;

        public:

            RectRenderer(const vector<GTObstacle>* vec, pthread_mutex_t* vecMtx,
                                bool right = false)
                : m_w(640), m_h(480), m_right(right), m_obsVec(vec), m_obsVecMtx(vecMtx)
            { }

            void update(const SensnetBlob& blob) throw()
            {
                StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
                m_w = sib->cols;
                m_h = sib->rows;
            }

            void render() throw(Error)
            {
                pushViewport();
                
                glTranslatef(-1.0, 1.0, 0.0);
                glScalef(2.0/m_w, -2.0/m_h, 0);
                
                glDisable(GL_TEXTURE_2D);
                glDisable(GL_CULL_FACE);
                
                glDisable(GL_BLEND);
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

                Lock lock(*m_obsVecMtx);

                for (unsigned int i = 0; i < m_obsVec->size(); i++)
                {
                    const GTObstacle& obs = (*m_obsVec)[i];
                    float x1 = obs.x - (m_right ? obs.disp : 0);
                    float x2 = obs.x + obs.w  - (m_right ? obs.disp : 0);
                    float y1 = obs.y;
                    float y2 = obs.y + obs.h;

                    glColor3f(obs.r, obs.g, obs.b);

                    glBegin(GL_POLYGON);
                    glVertex3f(x1, y1, 0.0);
                    glVertex3f(x2, y1, 0.0);
                    glVertex3f(x2, y2, 0.0);
                    glVertex3f(x1, y2, 0.0);
                    glEnd();

                    if (obs.falseAlarm) {
                        // draw a cross
                        glBegin(GL_LINES);
                        glVertex3f(x1, y1, 0.0);
                        glVertex3f(x2, y2, 0.0);
                        glVertex3f(x2, y1, 0.0);
                        glVertex3f(x1, y2, 0.0);
                        glEnd();
                    }
                }

                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                popViewport();
            }

        };

        class ImgOverlayRenderer : public RectRenderer
        {
            const ImageRenderer& m_left;
            const ImageRenderer& m_right;
            int m_cur;

        public:
            ImgOverlayRenderer(const vector<GTObstacle>* vec, pthread_mutex_t* vecMtx,
                               const ImageRenderer& left, const ImageRenderer& right)
                : RectRenderer(vec, vecMtx, true), m_left(left), m_right(right), m_cur(-1)
            { }

            void setCurrent(int idx)
            {
                if (idx < 0 || unsigned(idx) > m_obsVec->size()) {
                    m_cur = -1;
                } else {
                    m_cur = idx;
                }
            }
            
            void render() throw(Error)
            {
                if (m_cur < 0)
                    return;

                pushViewport();
                
                glTranslatef(-1.0, 1.0, 0.0);
                glScalef(2.0/m_w, -2.0/m_h, 0);
                
                glDisable(GL_CULL_FACE);
                glDisable(GL_DEPTH_TEST);
                glEnable(GL_TEXTURE_2D);
                
                glEnable(GL_BLEND);
                //glBlendFunc(GL_ONE, GL_ONE);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

                float scaleX, scaleY;
                GLuint txtId = m_left.getTextureInfo(&scaleX, &scaleY);

                glBindTexture (GL_TEXTURE_2D, txtId);
                glMatrixMode(GL_TEXTURE);
                glPushMatrix();
                glScalef(scaleX/m_w, scaleY/m_h, 1.0);
                //glScalef(scaleX, scaleY, 1.0);

                glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
                glColor4f(0.0, 0.0, 0.0, 0.5);

                Lock lock(*m_obsVecMtx);
                const GTObstacle& obs = (*m_obsVec)[m_cur];

                // use l(eft) for texture and r(ight) for vertex
                float x1r = obs.x - obs.disp;
                float x2r = obs.x + obs.w  - obs.disp;
                float x1l = obs.x;
                float x2l = obs.x + obs.w;
                float y1 = obs.y;
                float y2 = obs.y + obs.h;

                glBegin(GL_POLYGON);
                glTexCoord2f(x1l, y1);
                glVertex3f(x1r, y1, 0.0);
                glTexCoord2f(x2l, y1);
                glVertex3f(x2r, y1, 0.0);
                glTexCoord2f(x2l, y2);
                glVertex3f(x2r, y2, 0.0);
                glTexCoord2f(x1l, y2);
                glVertex3f(x1r, y2, 0.0);
                glEnd();

                glPopMatrix();

                glDisable(GL_BLEND);
                glDisable(GL_TEXTURE_2D);

            }
        };

        vector<GTObstacle> m_gtVec;
        pthread_mutex_t m_gtVecMtx;

        vector<GTObstacle> m_obsVec;
        pthread_mutex_t m_obsVecMtx;

        uint64_t m_timestamp;

        shared_ptr<SensnetImageRenderer> m_leftImgRend;
        shared_ptr<SensnetImageRenderer> m_rightImgRend;

        // render manually edited ground truth
        shared_ptr<GlRenderer> m_leftGtRend;
        shared_ptr<GlRenderer> m_rightGtRend;

        // render detected obstacles
        shared_ptr<GlRenderer> m_leftObsRend;
        shared_ptr<GlRenderer> m_rightObsRend;

        shared_ptr<ImgOverlayRenderer> m_rightImgOvlRend;

        shared_ptr<GlRenderList> m_leftRend;
        shared_ptr<GlRenderList> m_rightRend;

        RenderWidget* m_leftWid;
        RenderWidget* m_rightWid;

        // initial dragging position
        float m_dragX;
        float m_dragY;
        bool m_dragRight;
        bool m_drawRect;
        GTObstacle* m_dragObs;

        // selection
        int m_curSel;
        bool m_moveSel; // dragging to move selection?

        // keep track of all frames we've seen
        typedef map<uint64_t, vector<GTObstacle> > vecmap_t;
        vecmap_t m_obsMap;

        // current sensor to local tranformation
        matrix4_t m_img2loc;
        matrix4_t m_loc2img;

        // image info
        int m_w, m_h; // size in pixels

    public:

        GroundTruthEditorWidget(volatile DisparityDetector& dd,
                                int x, int y, int w, int h, const char* label) :
            Fl_Group(x, y, w, h, label),
            m_ddet(dd),
            m_obsVec(0),
            m_leftImgRend(new SensnetImageRenderer(SensnetImageRenderer::LEFT)),
            m_rightImgRend(new SensnetImageRenderer(SensnetImageRenderer::RIGHT)),
            m_leftGtRend(new RectRenderer(&m_gtVec, &m_gtVecMtx, false)),
            m_rightGtRend(new RectRenderer(&m_gtVec, &m_gtVecMtx, true)),
            m_leftObsRend(new RectRenderer(&m_obsVec, &m_obsVecMtx, false)),
            m_rightObsRend(new RectRenderer(&m_obsVec, &m_obsVecMtx, true)),
            m_rightImgOvlRend(new ImgOverlayRenderer(&m_gtVec, &m_gtVecMtx, *m_leftImgRend, *m_rightImgRend)),
            m_leftRend(new GlRenderList()),
            m_rightRend(new GlRenderList()),
            m_drawRect(false),
            m_dragObs(NULL),
            m_curSel(-1),
            m_moveSel(false),
            m_w(640), m_h(480)
        {
            
            Fl_Scroll* o = new Fl_Scroll(x, y, w, h, label);

            m_leftWid = new RenderWidget(m_leftRend, 0, 0, m_w, m_h, "left");
            m_rightWid = new RenderWidget(m_rightRend, m_w, 0, m_w, m_h, "right");

            o->end(); // no more fltk widgets
            end();

            pthread_mutex_init(&m_obsVecMtx, NULL);

            m_leftRend->addRenderer(m_leftImgRend);
            m_leftRend->addRenderer(m_leftGtRend);
            m_leftRend->addRenderer(m_leftObsRend);

            m_rightRend->addRenderer(m_rightImgRend);
            m_rightRend->addRenderer(m_rightGtRend);
            m_rightRend->addRenderer(m_rightObsRend);
            m_rightRend->addRenderer(m_rightImgOvlRend);
        }

        virtual void update(const SensnetBlob& blob) throw(Error)
        {
            StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
            
            bool doAssoc = false;

            // save gtVec to obsMap, save obsVec too if it's a new entry
            {
                select_rect(-1);
                // save current vector, if not empty
                if (m_gtVec.size() > 0 || m_obsVec.size() > 0) {
                    bool newEntry = (m_obsMap.find(m_timestamp) == m_obsMap.end());
                    vector<GTObstacle>& ov = m_obsMap[m_timestamp]; // create it if not there
                    ov.clear();
                    for (unsigned int i = 0; i < m_gtVec.size(); i++) {
                        ov.push_back(m_gtVec[i]);
                        ov.back().setColor(0.0, 1.0, 1.0);
                    }
                    /*
                    if (newEntry) {
                        for (unsigned int i = 0; i < m_obsVec.size(); i++) {
                            if (!m_obsVec[i].falseAlarm) {
                                ov.push_back(m_obsVec[i]);
                                ov.back().setColor(1.0, 0.0, 0.0);
                            }
                        }
                    }
                    */
                    // save false alarms
                    for (unsigned int i = 0; i < m_obsVec.size(); i++) {
                        if (m_obsVec[i].falseAlarm) {
                            ov.push_back(m_obsVec[i]);
                            ov.back().setColor(1.0, 0.0, 0.0);
                        }
                    }
                }

                m_w = sib->cols;
                m_h = sib->rows;

                // load the new vector, if present
                vecmap_t::iterator it = m_obsMap.find(sib->timestamp);
                if (it != m_obsMap.end())
                {
                    Lock lock(m_gtVecMtx);
                    m_gtVec = it->second;
                    buildProjMatrix(sib, &m_img2loc, &m_loc2img);
                } else {
                    matrix4_t i2lNew, l2iNew;
                    buildProjMatrix(sib, &i2lNew, &l2iNew);
                    matrix4_t old2new = prod(l2iNew, m_img2loc);
                    // transform the manually entered obstacles and use them
                    vector<GTObstacle> ov;
                    for (unsigned int i = 0; i < m_gtVec.size(); i++) {
                        if (!m_gtVec[i].autoDet) {
                            vector4_t v1, v2;
                            GTObstacle o = m_gtVec[i];
                            v1[0] = o.x + o.w*0.5;
                            v1[1] = o.y + o.h*0.5;
                            v1[2] = o.disp;
                            v1[3] = 1;
                            v2 = prod(old2new, v1);
                            v2 /= v2[3]; // normalize
                            o.x    += v2[0] - v1[0];
                            o.y    += v2[1] - v1[1];
                            o.disp += v2[2] - v1[2];
                            o.setColor(0.0, 1.0, 1.0);
                            // check if the rectancle is visible
                            if (o.x < m_w && o.x + o.w >= 0 && 
                                o.y < m_h && o.y + o.h >= 0)
                            {
                                ov.push_back(o);
                            }
                        }
                    }
                    doAssoc = true; // try to match previous obs with detected ones
                    m_gtVec = ov; // copy the new vector over the old one
                    // save the new transformation matrices
                    m_img2loc = i2lNew;
                    m_loc2img = l2iNew;
                }
                m_timestamp = sib->timestamp;
            }


            // populate obsVec with detection data
            {
                LockingPtr<DisparityDetector> This(&m_ddet);
                vector<Region>& regions = This->m_regions;
            
                if (regions.size() > 0)
                {
                    Lock lock(m_obsVecMtx);
                    m_obsVec.resize(regions.size());
                    
                    GTObstacle obs;
                    obs.setColor(1.0, 0.0, 0.0);
                    
                    for (unsigned int i = 0; i < regions.size(); i++)
                    {
                        //if (regions[i].filteredOut == true)
                        //    continue;
                        
                        obs.x = regions[i].rect.x;
                        obs.w = regions[i].rect.width;
                        obs.y = regions[i].row - regions[i].rowStDev * 1.5;
                        obs.h = regions[i].rowStDev * 3;
                        obs.disp = regions[i].pos[2];
                        obs.falseAlarm = false;
                        obs.autoDet = true;
                        
                        m_obsVec[i] = obs;
                    }
                }
            } // unlock m_ddet

            if (doAssoc)
                assoc_rects();

            m_leftWid->update(blob);
            m_rightWid->update(blob);
        }

        void redraw_lr()
        {
            m_leftWid->redraw();
            m_rightWid->redraw();
        }

        void assoc_rects()
        {
            const float AREA_THRES = 0.6;
            const float DISP_THRES = 2;
            MSG("begin");

            for (unsigned int j = 0; j < m_gtVec.size(); j++)
                m_gtVec[j].falseAlarm = false;

            for (unsigned int i = 0; i < m_obsVec.size(); i++) {
                GTObstacle& o1 = m_obsVec[i];
                o1.falseAlarm = true;;
                for (unsigned int j = 0; j < m_gtVec.size(); j++) {
                    GTObstacle& o2 = m_gtVec[j];
                    if (abs(o1.disp - o2.disp) < DISP_THRES)
                    {
                        MSG("Obs " << i << " and " << j << " have compatible disparities "
                            "(o1.disp=" << o1.disp << ", o2.disp=" << o2.disp << ")");
                        // compute intersection rect
                        float x1, x2, y1, y2;
                        x1 = max(o1.x, o2.x);
                        x2 = min(o1.x + o1.w, o2.x + o2.w);
                        y1 = max(o1.y, o2.y);
                        y2 = min(o1.y + o1.h, o2.y + o2.h);
                        float area = 0;
                        if (x2 > x1 && y2 > y1)
                            area = (x2 - x1) * (y2 - y1);
                        MSG("area(o1) = " << o1.w*o1.h << ", area(o2) = " << o2.w*o2.h
                            << ", area(inters(o1,o2)) = " << area);
                        if (area > o1.w*o1.h*AREA_THRES || area > o2.w*o2.h*AREA_THRES)
                        {
                            MSG("Obs " << i << " and " << j << " associated!");
                            // intersection more than 80% of at least one of them
                            // update o2 with detected obs o1
                            o2.disp = o1.disp;
                            float cx = (x1 + x2) / 2;
                            float cy = (y1 + y2) / 2;
                            o2.x = cx - o2.w/2;
                            o2.y = cy - o2.h/2;
                            o2.falseAlarm = true;
                            o1.falseAlarm = false;
                        }
                    }
                }
            }
            MSG("end");
        }

        bool select_rect(int sel)
        {
            bool update = false;
            if (m_curSel == sel && sel >= 0) {
                // special handling, toggle falseAlarm
                m_gtVec[sel].falseAlarm = !m_gtVec[sel].falseAlarm;
                return true;;
            }
            if (m_curSel >= 0) {
                m_gtVec[m_curSel].setColor(0.0, 1.0, 1.0);
                m_curSel = -1;
                update = true;
            }
            if (sel >= 0) {
                m_curSel = sel;
                m_gtVec[m_curSel].setColor(0.0, 1.0, 0.0);
                update = true;
            }
            return update;
        }

        // aborts the drawing of a new rect
        void cancel_rect()
        {
            if (m_dragObs == NULL) {
                return;
            }

            for (vector<GTObstacle>::iterator it = m_gtVec.begin();
                 it != m_gtVec.end();
                 ++it)
            {
                if (&*it == m_dragObs)
                {
                    Lock lock(m_gtVecMtx);
                    m_gtVec.erase(it);
                    break;
                }
            }
        }

        void delete_rect(int i)
        {
            if (i >= 0 && unsigned(i) < m_gtVec.size())
            {
                Lock lock(m_gtVecMtx);
                m_gtVec.erase(m_gtVec.begin() + i);
            }
        }

        virtual void draw()
        {
            // resize left and right to the window size, respecting pixel ratio
            int widWidth = w() / 2;
            int widHeight = widWidth * m_h / m_w;
            m_leftWid->resize(x(), y(), widWidth, widHeight);
            m_rightWid->resize(x() + widWidth, y(), widWidth, widHeight);
            base_t::draw();
        }

        virtual int handle(int e)
        {
            int ret = base_t::handle(e);

            switch (e)
            {
            case FL_PUSH: {
                if ( Fl::event_button() == 1 )
                {
                    float evX = Fl::event_x();
                    float evY = Fl::event_y();
                    float scaleX = 1;
                    float scaleY = 1;

                    bool inside = false;
                    bool isRight = false; // right side

                    if (evX >= m_leftWid->x() && evX < m_leftWid->x() + m_leftWid->w() &&
                        evY >= m_leftWid->y() && evY < m_leftWid->y() + m_leftWid->h())
                    {
                        inside = true;
                        isRight = false;
                        evX -= m_leftWid->x();
                        evY -= m_leftWid->y();
                        scaleX = float(m_w) / m_leftWid->w();
                        scaleY = float(m_h) / m_leftWid->h();
                    } else if (evX >= m_rightWid->x() && evX < m_rightWid->x() + m_rightWid->w() &&
                               evY >= m_rightWid->y() && evY < m_rightWid->y() + m_rightWid->h())
                    {
                        inside = true;
                        isRight = true;
                        evX -= m_rightWid->x();
                        evY -= m_rightWid->y();
                        scaleX = float(m_w) / m_rightWid->w();
                        scaleY = float(m_h) / m_rightWid->h();
                    }

                    // do anybody need the original window coords (opposed to image coords)?
                    evX *= scaleX;
                    evY *= scaleY;
                    
                    //MSG("event_x = " << Fl::event_x() << ", event_y = " << Fl::event_y()
                    //    << ", dragX = " << m_dragX << ", dragY = " << m_dragY
                    //    << " dragging = " << m_drawRect << ", right = " << m_dragRight);

                    if (!inside) {
                        MSG("clicked outside");
                        break;
                    }

                    ret = 1;
                        
                    m_dragRight = isRight;
                    m_dragX = evX;
                    m_dragY = evY;

                    // press CTRL to insert a new rectangle
                    if (Fl::event_state() & FL_CTRL)
                    {
                        // NOTE: unless a FL_DRAG event is received, nothing is actually done
                        m_drawRect = true;
                        break;
                    }

                    // handle selection
                    const float distThres = 20;
                    float dist = distThres; // max distance
                    int sel = -1;
                    for (unsigned int i = 0; i < m_gtVec.size(); i++)
                    {
                        const GTObstacle& o = m_gtVec[i];
                        float d = o.dist(evX + (isRight ? o.disp : 0), evY);
                        //MSG("rect (" << o.x << ", " << o.y << ")-" << o.w << "x" << o.h
                        //    << " dist from (" << evX << ", " << evY << ") = " << d);
                        if (d < dist) {
                            sel = i;
                            dist = d;
                        }
                    }

                    int sel2 = -1;
                    for (unsigned int i = 0; i < m_obsVec.size(); i++)
                    {
                        const GTObstacle& o = m_obsVec[i];
                        float d = o.dist(evX + (isRight ? o.disp : 0), evY);
                        //MSG("rect (" << o.x << ", " << o.y << ")-" << o.w << "x" << o.h
                        //    << " dist from (" << evX << ", " << evY << ") = " << d);
                        if (d < dist) {
                            sel2 = i;
                            dist = d;
                        }
                    }
                    if (sel2 >= 0) {
                        m_obsVec[sel2].falseAlarm = !m_obsVec[sel2].falseAlarm;
                        MSG("Toggling falseAlarm on " << sel2 << ": " << m_obsVec[sel2]);
                        redraw_lr();
                    } else {
                        if (sel == -1)
                            MSG("no selection, dist=" << dist);
                        else {
                            MSG("Selected: " << sel << " (dist=" << dist << "): "
                                << m_gtVec[sel]);
                        }
                        if (select_rect(sel) == true)
                            redraw_lr();
                    }
                }
                MSG(m_gtVec.size() << " ground truth obstacles");
                break;
            }

            case FL_RELEASE: {
                if (m_dragObs != NULL) {
                    // if empty, remove new rectangle
                    if (m_dragObs->w == 0 && m_dragObs->h == 0)
                    {
                        MSG("removing empty rect");
                        cancel_rect();
                    } else {
                        m_dragObs->setColor(0.0, 1.0, 1.0);
                    }
                    redraw_lr();
                }
                m_drawRect = false;
                m_dragObs = NULL;
                if (m_moveSel >= 0 && m_dragRight) {
                    m_rightImgOvlRend->setCurrent(-1);
                    redraw_lr();
                }
                m_moveSel = false;
                ret = 1;
                break;
            }

            case FL_DRAG: {

                float evX = Fl::event_x();
                float evY = Fl::event_y();
                float scaleX = 1, scaleY = 1;
                if (m_dragRight) {
                    evX -= m_rightWid->x();
                    evY -= m_rightWid->y();
                    scaleX = float(m_w) / m_leftWid->w();
                    scaleY = float(m_h) / m_leftWid->h();
                } else {
                    evX -= m_leftWid->x();
                    evY -= m_leftWid->y();
                    scaleX = float(m_w) / m_rightWid->w();
                    scaleY = float(m_h) / m_rightWid->h();
                }
                evX *= scaleX;
                evY *= scaleY;

                if (m_drawRect)
                {
                    Lock lock(m_gtVecMtx);

                    if (m_dragObs == NULL) {
                        m_gtVec.push_back(GTObstacle());
                        m_dragObs = &m_gtVec.back();
                    }

                    GTObstacle& obs = *m_dragObs;

                    // FIXME: pixels may not be 1-to-1 with image coords

                    obs.setColor(1.0, 0.0, 1.0);
                    obs.x = m_dragX;
                    obs.y = m_dragY;
                    obs.w = evX - m_dragX;
                    obs.h = evY - m_dragY;
                    obs.disp = 0;
                    if (obs.w < 0) {
                        obs.x += obs.w;
                        obs.w = -obs.w;
                    }
                    if (obs.h < 0) {
                        obs.y += obs.h;
                        obs.h = -obs.h;
                    }
                    ret = 1;

                    MSG("Dragging a rect: (" << obs.x << ", " << obs.y << ")-"
                        << obs.w << "x" << obs.h);

                    redraw_lr();
                    //Fl::awake();
                    break;
                }

                if (m_curSel >= 0) {
                    m_moveSel = true;
                    if (m_dragRight)
                        m_rightImgOvlRend->setCurrent(m_curSel);
                }

                if (m_moveSel) {
                    assert(m_curSel >= 0 && m_curSel < int(m_gtVec.size()));
                    GTObstacle& obs = m_gtVec[m_curSel];
                    if (m_dragRight) {
                        // change disparity
                        obs.disp +=  m_dragX - evX;
                    } else {
                        obs.x += evX - m_dragX;
                        obs.y += evY - m_dragY;
                    }
                    m_dragX = evX;
                    m_dragY = evY;
                    redraw_lr();
                }
                break;
            }

            case FL_FOCUS:
                ret = 1;
                break;

            case FL_KEYDOWN:
            case FL_SHORTCUT: // awful, seems there is no way to get the focus
            {
                switch (Fl::event_key())
                {
                case FL_Delete: // delete current selection, if any
                    if (m_drawRect) {
                        MSG("DEL pressed, cancelling new rect");
                        cancel_rect();
                        m_drawRect = false;
                        m_dragObs = NULL;
                        redraw_lr();
                    } else if (m_curSel >= 0) {
                        MSG("DEL pressed, deleting selection " << m_curSel);
                        int i = m_curSel;
                        select_rect(-1);
                        delete_rect(i);
                        redraw_lr();
                    }
                    ret = 1;
                    break;

                case FL_Escape: // clear selection, if any, or abort drawRect/moveSel
                    MSG("ESC pressed");
                    if (m_moveSel) {
                        m_moveSel = false;
                        redraw_lr();
                    } else if (m_drawRect) {
                        cancel_rect();
                        m_drawRect = false;
                        m_dragObs = NULL;
                        redraw_lr();
                    } else if (m_curSel >= 0) {
                        select_rect(-1);
                        redraw_lr();
                    }
                    ret = 1;
                    break;

                case 's': // save everything to a file
                {
                    ret = 1;
                    Fl_File_Chooser chooser(".",			// directory
                                            "*",			// filter
                                            Fl_File_Chooser::CREATE, 	// chooser type
                                            "Save To");			// title
                    chooser.show();

                    // Block until user picks something.
                    //     (The other way to do this is to use a callback())
                    //
                    while(chooser.shown())
                        Fl::wait();

                    // User hit cancel?
                    if ( chooser.value() == NULL ) {
                        MSG("(User hit 'Cancel')\n");
                        break;
                    }

                    MSG("DIRECTORY: " << chooser.directory());
                    MSG("    VALUE: " << chooser.value());

                    // NOTE WARNING: does not check if the file exists already!!!
                    ofstream of(chooser.value());
                    if (of.good()) {
                        MSG("Saving to " << chooser.value());
                        for  (vecmap_t::iterator it = m_obsMap.begin(); it != m_obsMap.end(); ++it)
                        {
                            uint64_t tstamp = it->first;
                            const vector<GTObstacle>& ov = it->second;
                            for (unsigned int i = 0; i < ov.size(); i++)
                            {
                                const GTObstacle& o = ov[i];
                                of << tstamp << ' ' <<  o.x << ' ' << o.y << ' '
                                   << o.w << ' ' << o.h << ' ' << o.disp << ' ' << o.falseAlarm
                                   << ' ' << o.autoDet
                                   << " color=(" << o.r << ',' << o.g << ',' << o.b << ")\n";
                            }
                        }
                    }

                    break;
                }

                case 'l': // load from file
                {
                    ret = 1;
                    Fl_File_Chooser chooser(".",			// directory
                                            "*",			// filter
                                            Fl_File_Chooser::SINGLE, 	// chooser type
                                            "Open");			// title
                    chooser.show();

                    // Block until user picks something.
                    //     (The other way to do this is to use a callback())
                    //
                    while(chooser.shown())
                        Fl::wait();

                    // User hit cancel?
                    if ( chooser.value() == NULL ) {
                        MSG("(User hit 'Cancel')\n");
                        break;
                    }

                    MSG("DIRECTORY: " << chooser.directory());
                    MSG("    VALUE: " << chooser.value());

                    MSG("Opening " << chooser.value());
                    ifstream ifs(chooser.value());
                    if (ifs.bad()) {
                        MSG("cannot open file");
                        break;
                    }
                    m_obsMap.clear();
                    while (ifs.good() && !ifs.eof())
                    {
                        string line;
                        getline(ifs, line);
                        istringstream is(line);
                        uint64_t tstamp;
                        GTObstacle o;
                        is >> tstamp >> o.x >> o.y >> o.w >> o.h >> o.disp
                           >> o.falseAlarm >> o.autoDet;
                        if (is.good()) {
                            o.setColor(0.0, 1.0, 1.0);
                            m_obsMap[tstamp].push_back(o);
                        } else {
                            MSG("invalid line: " << line);
                        }
                    }
                    redraw_lr();
                    break;
                }

                case 'a':
                    ret = 1;
                    assoc_rects();
                    redraw_lr();
                    break;

                default:
                    break;
                }
                break;
            }

            default:
                // ret=0;//???
                break;
            }

            return ret;
        }
         
   };

    vector<Fl_Widget*> DisparityDetector::getWidgets()
    {
        shared_ptr<GlRenderer> irLeft(new SensnetImageRenderer(SensnetImageRenderer::LEFT));
        shared_ptr<GlRenderer> overlayRend(new ObstOverlayRenderer(*this));
        //shared_ptr<GlRenderer> overlayRend2(new ObstOverlayRenderer(*this, true));

        shared_ptr<GlRenderList> rendList(new GlRenderList());
        rendList->addRenderer(irLeft);
        rendList->addRenderer(overlayRend);

        // add a second one that uses tracked regions instead of just measurements
        //shared_ptr<GlRenderList> rendList2(new GlRenderList());
        //rendList2->addRenderer(irLeft);
        //rendList2->addRenderer(overlayRend2);


        vector<Fl_Widget*> widgets;
        widgets.push_back(new RenderWidget(rendList, 0, 0, 640, 480, "Left w/ obst"));
        //widgets.push_back(new RenderWidget(rendList2, 0, 0, 640, 480, "Left w/ tracks"));

        shared_ptr<GlRenderer> detRend(new DisparityDetectorRenderer(*this));
        widgets.push_back(new RenderWidget(detRend, 0, 0, 640, 480, "Detector"));

        shared_ptr<ObstacleRenderer> obstRend(new ObstacleRenderer(*this));
        widgets.push_back(new RenderWidget(obstRend, 0, 0, 640, 480, "Obstacles"));

        shared_ptr<ObstacleRenderer3D> obstRend3D(new ObstacleRenderer3D(*this));
        widgets.push_back(new RenderWidget(obstRend3D, 0, 0, 640, 480, "Obstacles3D"));

        shared_ptr<LocalMapRenderer> locMapRend(new LocalMapRenderer(*this));
        widgets.push_back(new RenderWidget(locMapRend, 0, 0, 640, 480, "Local Map"));

        //shared_ptr<TestRenderer> testRend(new TestRenderer());
        //widgets.push_back(new RenderWidget(testRend, 0, 0, 640, 480, "*TEST*"));

        //shared_ptr<GlRenderer> maxRowRend(new DispDetMapRenderer(m_minRow, m_mtx));
        //widgets.push_back(new RenderWidget(maxRowRend, 0, 0, 640, 480, "MinRow"));

        widgets.push_back(new TuneParamsWidget(*this, 0, 0, 640, 480, "Tuning"));

        //widgets.push_back(new GroundTruthEditorWidget(*this, 0, 0, 640, 480, "Ground Truth Edit"));

        return widgets;
    }

}
