#ifndef __STEREO_OBS_UTIL_HH__
#define __STEREO_OBS_UTIL_HH__

#include <sstream>
#include <exception>
#include <sys/time.h>
#include <cotk/cotk.h>
#include <ncurses.h>

// convert anything to a string
// use it to build string on one line, like "i = " + toStr(i)
template<typename T> std::string toStr(const T& x) {
    std::ostringstream os;
    os << x;
    return os.str();
}
// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

#define FILE_LINE std::string(__FILE__ ":") + toStr(__LINE__)
#define ERRSTR(str) FILE_LINE  + ": " + str

// use like in: throw ERROR("message")
#define ERROR(str) stereo_obs::Error(std::string(__FILE__ ":") + toStr(__LINE__) + ": " + str)


namespace stereo_obs {

    /** Generic exception that is thrown when something goes wrong.
     */
    class Error : public std::exception {
        std::string msg;
    public:
        Error(std::string m) : msg(m) { }
        ~Error() throw() { }
        virtual std::string getMsg() const throw() { return msg; }
        virtual const char* what() const throw() { return msg.c_str(); }
    };

    inline uint64_t gettime()
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    }

    // wait for a keypress or for the timeout to pass, whichever comes first.
    // timeout is in microseconds.
    // Returns 0 if timeout expires, or != 0 if a key is pressed.
    inline int uwait(uint64_t tmout)
    {
        uint64_t endTime = gettime() + tmout;
        if (tmout > 100000L) // one tenth of a second
        {
            long tenths = tmout / 100000L;
            long msecs = tenths * 100;
            // wait for a keypress
            timeout(msecs); // it's a ncurses function, if it wasn't clear ... ;-)
            int c = getch();
            timeout(0);
            if (c != ERR) {
                ungetch(c);
                return 1;
            }
        }
        usleep(endTime - gettime());
        return 0;
    }

    // wrapper for using a member function as a cotk callback
    // (yes, brutally casting it could work too, I know, but this is guaranteed
    // to work, because it's standard compliant. for example, using the -ffast-this
    // compiler option would break direct casting).

    template <typename T>
    struct cotk_member_cb
    {
        typedef int (T::*type)(cotk_t *cotk, const char *token);
    };

    template <typename T>
    struct CotkMemberCallbackData {
        typedef typename cotk_member_cb<T>::type callback_t;
        callback_t cb;
        T* self;
        CotkMemberCallbackData(T* _self, callback_t _cb)
            : cb(_cb), self(_self)
        { }
    };
    
    template <typename T>
    inline int cotkMemberCallback(cotk_t *cotk, void *d, const char *token)
    {
        CotkMemberCallbackData<T>* data = reinterpret_cast<CotkMemberCallbackData<T>*>(d);
        return (data->self->*(data->cb))(cotk, token);
    }


    template<class OStr, class T>
    void printMat(OStr& out, T mat[4][4], std::string name = "mat")
    {
	// matlab-style (so you can cut'n'paste it in matlab if you want)
	std::string indent = std::string(name.length() + 4, ' ');
	out << name << " = [\n";
	for (int i = 0; i < 4; i++)
	{
	    out << indent << ' ';
	    for (int j = 0; j < 4; j++)
		out << mat[i][j] << (j < 3 ? ", " : "");
	    out << ";\n";
	}
	out << indent << "];" << std::endl;
    }

    template<class T>
    void printMat(T mat[4][4], std::string name = "mat")
    {
	printMat(std::cout, mat, name);
    }

    // homogeneous coordinates (x, y, z, w)
    template<int N> class pointN
    {
    public:
        float v[N];
        float& operator[](int i) { return v[i]; }
        float operator[](int i) const { return v[i]; }
    };

    typedef pointN<3> point3;
    typedef pointN<4> point4;

    static inline point3 makePoint3(float x, float y, float z)
    {
        point3 ret;
        ret[0] = x;
        ret[1] = y;
        ret[2] = z;
        return ret;
    }

    static inline point4 makePoint4(float x, float y, float z, float w)
    {
        point4 ret;
        ret[0] = x;
        ret[1] = y;
        ret[2] = z;
        ret[3] = w;
        return ret;
    }

    // locking class
    class Lock
    {
        pthread_mutex_t& mtx;
        bool locked;
    public:
        Lock(pthread_mutex_t& m) : mtx(m), locked(true) {
            pthread_mutex_lock(&m);
        }
        ~Lock() {
            unlock();
        }
        void unlock() {
            if (locked)
                pthread_mutex_unlock(&mtx);
        }
    };
}

#endif
