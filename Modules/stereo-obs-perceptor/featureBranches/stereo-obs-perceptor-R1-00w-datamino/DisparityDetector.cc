/* Emacs clue: -*-  indent-tabs-mode:nil; c-basic-offset:4; -*-*/
/*!
 * \file DisparityDetector.cc
 * \brief Obstacle detector based on stereo disparity
 *
 * \author Daniele Tamino
 * \date 10 July 2007
 *
 * This file contains the implementation of the DisparityDetector class, which given
 * a sensnet StereoImageBlob blob, containing disparity information, outputs a set
 * of obstacles. This module can track the obstacles over time, but the reset()
 * method can be called to clear the prior imformation.
 *
 */

// standard C++ headers
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <limits>
#include <cmath>
#include <algorithm>
// boost ublas headers
#include <boost/numeric/ublas/symmetric.hpp>
#include <boost/numeric/ublas/banded.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/matrix_expression.hpp>
#include "matrix_invert.hh" // ublas doesn't have a function to invert a matrix!!! :-O :-O
// OpenCV headers
#include <cv.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <frames/mat44.h>
#include <dgcutils/AutoMutex.hh>
#include <interfaces/StereoImageBlob.h>
// local headers
#include "perceptor_cmdline.h"
#include "DisparityDetector.hh"
#include "IUpdatable.hh"
#include "util.hh"

#ifdef AGGRESSIVE_DEBUG
#define AGG_ASSERT(x) assert(x)
#else
#define AGG_ASSERT(x)
#endif


#if 1 //def NDEBUG
#define DBG(x)
#else
#define DBG(x) x
#endif

// as incredible as it seems, ublas::matrix doesn't have a equality operator
namespace boost { namespace numeric { namespace ublas { 

    template <class ME1, class ME2>
    bool operator==(const matrix_expression<ME1>& m1, const matrix_expression<ME2>& m2)
    {
        assert(m1().size1() == m2().size1());
        assert(m1().size2() == m2().size2());
        for (size_t i = 0; i < m1().size1(); i++)
            for (size_t j = 0; j < m1().size1(); j++)
                if (m1()(i, j) != m2()(i, j))
                    return false;
        return true;
    }

    template <class ME1, class ME2>
    bool operator!=(const matrix_expression<ME1>& m1, const matrix_expression<ME2>& m2)
    {
        return !(m1 == m2);
    }


} } }

namespace stereo_obs
{
    using namespace std;


    // DEPRECATED use vector3_t (and use outer_prod ;-)
    typedef ublas::c_matrix<double, 3, 1> column_vec3_t;
    // DEPRECATED use make_vec3
    static inline column_vec3_t make_column_vec3(float x, float y, float z)
    {
        column_vec3_t ret;
        ret(0,0) = x;
        ret(1,0) = y;
        ret(2,0) = z;
        return ret;
    }

    // make a ublas matrix out of a c-vector (row-major)
    template <typename MAT, typename T>
    static MAT make_matrix(const T* vec)
    {
        MAT m;
        for (int i = 0; i < int(m.size1()); i++)
            for (int j = 0; j < int(m.size2()); j++)
                m(i, j) = *vec++;
        return m;
    }

    template <typename MAT>
    void assert_matrix_nan_inf(const MAT& m)
    {
        for (int i = 0; i < int(m.size1()); i++)
            for (int j = 0; j < int(m.size2()); j++)
                assert(!isnan(m(i, j)) && !isinf(m(i, j)));
    }

    template <typename Ostream, typename MAT>
    void print_matrix(Ostream& os, const MAT& m)
    {
        os << '[' << endl;
        for (int i = 0; i < int(m.size1()); i++)
        {
            for (int j = 0; j < int(m.size2()); j++)
                os << ' ' << m(i, j);
            os << ";" << endl;
        }
        os << ']' << endl;
    }

    template <typename T>
    T square(T x) { return x*x; }
    
    template <typename T>
    T cube(T x) { return x*x*x; }

    void calcPointCov(float cov[4][4], const float pi[3],
                      const float himg2sens[4][4], const float sens2loc[4][4],
                      const float errorVar[4]);
    

    /*!
     * This function is used, when the debugging flag is on, to force OpenCV to abort and
     * generate a core dump in case of error.
     */
    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
        abort();
    }
    
    DisparityDetector::DisparityDetector(gengetopt_args_info& options)
        : m_mapMax(0), /*m_nextId(0),*/ m_lastTs(0),
          // all the detector-related commandline options are here
          m_maxDisp(options.max_disp_arg),
          m_minDisp(options.min_disp_arg),
          m_mapHeight(options.map_height_arg),
          m_highThres(options.high_thres_arg),
          m_lowThres(options.low_thres_arg),
          m_smallObsThres(options.small_obs_thres_arg),
          m_assocThres(options.assoc_thres_arg),
          m_disableTracking(options.disable_tracking_flag),
          m_confThres(options.conf_thres_arg),
          m_maxTracks(options.max_tracks_arg),
          m_heightThres(options.height_thres_arg),
          m_heightThresSigma(options.hthres_sigma_arg),
          m_locMapEnable(!options.disable_localmap_flag),
          m_locMapWidth(options.locmap_width_arg),
          m_locMapHeight(options.locmap_height_arg),
          m_locMapRes(options.locmap_res_arg),
          m_maxLocRegs(options.max_static_obs_arg),
          m_smallObsLoc(options.locmap_smallobs_arg),
          m_locMapCenter(options.locmap_center_arg),
          m_allStatic(options.all_static_flag), 
          m_onlyStatic(options.only_static_flag), 
          m_debug(options.debug_arg)
    {
        pthread_mutex_init(&m_mtx, NULL);
        if (m_debug) {
            cvRedirectError(abortOnCvError);
        }

        if (m_maxLocRegs > MAX_OBSTACLE_ID - MAX_TRACK_ID)
            throw ERROR("Too many static obstacles allowed, limit is "
                        + toStr(MAX_OBSTACLE_ID - MAX_TRACK_ID));

        if (m_maxTracks > MAX_TRACK_ID + 1)
            throw ERROR("Too many tracks allowed, limit is "
                        + toStr(MAX_TRACK_ID + 1));

        this->moduleId = modulenamefromString(options.module_id_arg);
        if (this->moduleId <= 0)
            throw ERROR("invalid module id: " + string(options.module_id_arg));
 
        istringstream is(options.error_var_arg);
        is >> m_errorVar[0] >> m_errorVar[1] >> m_errorVar[2];
        if (!is) {
            throw ERROR("The error variance should be 3 numbers, like 'Sx Sy Sdisp', got "
                        + toStr(options.error_var_arg) + " instead");
        }

        // Kalman filter, initial velocity covariance --kalman-P0-vel
        ublas::matrix<double> mat(3, 3);
        istringstream is2(string("[3,3](") + options.kalman_P0_vel_arg + string(")"));
        is2 >> mat;
        if (!is2) {
            throw ERROR("Initial velocity covariance is not in the correct format!");
        }
        m_velCov0 = mat;
        // do some checks to see if it's positive definite or not (actually, test some
        // necessary buf not sufficient conditions, don't want to spend my life here)
        if (det3(m_velCov0) < 0)
            throw ERROR("Initial velocity covariance has det(P) < 0!");
        if (ublas::symmetric_adaptor<matrix3_t, ublas::lower>(m_velCov0) != m_velCov0)
            throw ERROR("Initial velocity covariance is not symmetric!");

        // Kalman filter, model error covariance Q, --kalman-Q
        istringstream is3(string("[6,6](") + options.kalman_Q_arg + string(")"));
        is3 >> mat;
        if (!is3) {
            throw ERROR("Model error covariance is not in the correct format!");
        }
        m_modelErr = mat;
        // check at least that it's symmetric
        if (ublas::symmetric_adaptor<matrix6_t, ublas::lower>(m_modelErr) != m_modelErr)
            throw ERROR("Model error covariance is not symmetric!");

        MSG("Initial velocity covariance: " << m_velCov0);
        MSG("Model error covariance: " << m_modelErr);

        reset(); // initialize the m_tracks array and the local map
    }

    void DisparityDetector::reset()
    {
        // clear local cost map
        if (m_locMapEnable) {
            m_localMap.init(int(m_locMapWidth/m_locMapRes), int(m_locMapHeight/m_locMapRes),
                            m_locMapRes, m_locMapRes, point2(0, 0), 0);
            m_localConf.init(int(m_locMapWidth/m_locMapRes), int(m_locMapHeight/m_locMapRes),
                             m_locMapRes, m_locMapRes, point2(0, 0), 0);
            m_localMask.init(m_localMap.getWidth() + 2, m_localMap.getHeight() + 2);
            m_localMask.clear();

            m_tempMask.init(m_localMap.getWidth(), m_localMap.getHeight());
            m_tempMask.clear();
        }

        
        // clear tracks
        m_tracks.clear();
        m_tracks.resize(m_maxTracks);
        for (int i = 0; i < m_maxTracks; i++)
            m_tracks[i].deleted(true);
        m_toSort.reserve(m_maxTracks);
        trackIdPool.reset();
    }

    void DisparityDetector::detect(const SensnetBlob& blob, vector<MapElement>* elements)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        
        if (sib->version != 4) {
            if (sib->version < 4) {
                ERRMSG("StereoImageBlob version " << sib->version << " not supported!!!");
                return;
            } else {
                MSG("StereoImageBlob version " << sib->version << " newer than 4, continuing");
            }
        }

        buildProjMatrix(sib);
        
        // initialize the map
        if (m_mapImg.getWidth() != sib->cols) {
            // allocate memory - this should only happen the first time detect() is called
            m_mapImg.init(sib->cols, m_mapHeight);
            m_maskImg.init(sib->cols + 2, m_mapHeight + 2);
            m_rowSum.init(sib->cols, m_mapHeight);
            m_rowSumSq.init(sib->cols, m_mapHeight);
        }
        m_mapImg.clear();
        m_maskImg.clear();
        m_rowSum.clear();
        m_rowSumSq.clear();

        // use the disparity image in 'sib' to fill m_mapImg
        buildMap(sib);

        // given the new map in m_mapImg, find connected regions using low and high
        // thresholds, and put them in m_regions. This filters small regions too.
        findRegions(sib);

        elements->clear();

        // given the regions found in the preceding step, put them in the local map,
        // and if there's enough prior data, declare them as static obstacles.
        // Then, find regions in the local map and add MapElements to 'elements'.
        if (m_locMapEnable)
            updateLocalMap(sib, elements);

        // given the detected regions in m_regions as the current observations, update
        // the filtered tracks in m_tracks, and fill the elements vector
        updateTracks(sib, elements);

    }


    /*!
     * Given the current stereo blob, containing disparity information,
     * build a map accumulating vertically points with the same disparity.
     * The map is written to m_mapImg.
     * \param sib The new stereo blob
     */
    void DisparityDetector::buildMap(StereoImageBlob* sib)
    {
        // conversion from disparity image to map cell index
        float disp2cell = float(m_mapHeight) / (m_maxDisp * sib->dispScale);
        float blob2disp = 1.0 / sib->dispScale;
        // minimum interesting disparity - cut things at infinity
        int16_t minDisp = int16_t(m_minDisp * sib->dispScale + 0.9);
        m_mapMax = 0;

        float sens2veh[4][4];
        memcpy(sens2veh, sib->leftCamera.sens2veh, sizeof(sens2veh));
        //float groundZ = sib->state.localZ;
        //MSG("pixel weights:");

        // precompute coefficients of (col, row, disp) to get local frame Z coord
        float baseline = sib->baseline;
        float cx = sib->leftCamera.cx;
        float cy = sib->leftCamera.cy;
        float sx = sib->leftCamera.sx;
        float sy = sib->leftCamera.sy;

        float img2locz[4] = {
            sens2veh[2][0]*baseline, // column
            sens2veh[2][1]*baseline*sx/sy, // row
            sens2veh[2][3], // disparity
            // constant
            baseline*(-sens2veh[2][0]*cx - sens2veh[2][1]*cy*sx/sy + sens2veh[2][2]*sx)
        };

        for (int r = 0; r < sib->rows; r++)
        {
            for (int c = 0; c < sib->cols; c++)
            {
                int16_t d = *(int16_t*) StereoImageBlobGetDisp(sib, c, r);
                if (d >= minDisp)
                {
                    unsigned int cell = unsigned(d * disp2cell + 0.5);
                    
                    if (cell >= m_mapHeight)
                        continue;

                    float w = 1.0;
                    
                    // get the height in local frame
                    float disp = d * blob2disp;
                    float z = (img2locz[0]*c + img2locz[1]*r + img2locz[3])/disp + img2locz[2];

                    float h = -z;//fabs(z - groundZ);
                    if (h > m_heightThres)
                    { 
                        // use a simple, rough, fast, linear weight
                        float hDiff = h - m_heightThres;
                        if (hDiff >= m_heightThresSigma) {
                            w = 0;
                        } else {
                            float hr = hDiff / m_heightThresSigma;
                            w = 1.0 - hr;
                        }
                    }
                    //cerr << w << ' ';

                    if (w > 0)
                    {
                        dispmap_t val = m_mapImg[cell][c][0] + w;
                        m_mapImg[cell][c][0] = val;
                        if (val > m_mapMax)
                            m_mapMax = val;

                        m_rowSum[cell][c][0]   += r * w;
                        m_rowSumSq[cell][c][0] += r*r * w;
                    }
                }
            }
            //cerr << '\n';
        }
        //cerr << endl;
        //MSG("end heights");
        //MSG("mapMax = " << m_mapMax);
        
    }

#define USE_CONVEX_HULL 0
#define USE_LINE_MATCH_ASSOC 0
#define USE_KALMAN_FILTER 1

    /*!
     * Given the current map in m_mapImg, find the connected regions using
     * two thresholds (m_lowThres and m_hiThres). The high threshold is used to
     * find peaks in the map, then for each peak the region is expanded to all
     * the connected cells with a value bigger than the low threshold.
     * The result of this is stored in m_regions.
     * \param sib The stereo blob the map was built from
     */
    void DisparityDetector::findRegions(StereoImageBlob* /*sib*/)
    {
        float cell2disp = float(m_maxDisp) / float(m_mapHeight);

        vector<Region> &regions = m_regions;
        regions.clear();

        Bitmap<float, 1>* mapImg = &m_mapImg;
        // we need the underlying IplImage to call OpenCV functions
        IplImage* map = mapImg->getImage();
        Bitmap<uint8_t>* maskImg = &m_maskImg;

        // valid region labels start at 2 (0 means not assigned, 1 is used internally)
        int label = 2;
        int height = mapImg->getHeight();
        int width = mapImg->getWidth();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                dispmap_t val = (*mapImg)[r][c][0];

                // find peaks using the high threshold
                if (val > m_highThres)
                {
                    dispmap_t loDiff = val - m_lowThres;
                    dispmap_t hiDiff = m_mapMax - val + 1;
                    if (hiDiff <= 0) {
                        hiDiff = numeric_limits<dispmap_t>::max();
                        //MSG("Found map value " << val << " bigger than mapMax = " << m_mapMax);
                    }
                    CvConnectedComp comp;

                    // if this cell belongs to a previously detected region, skip it
                    if (m_maskImg[r+1][c+1][0] != 0)
                        continue;

                    // find the connected component using the low threshold
                    cvFloodFill(map, cvPoint(c, r), cvRealScalar(1),
                                cvRealScalar(loDiff), cvRealScalar(hiDiff),
                                &comp, CV_FLOODFILL_FIXED_RANGE | CV_FLOODFILL_MASK_ONLY | 8,
                                maskImg->getImage());

                    if (comp.rect.width > 0 && comp.rect.height > 0)
                    {
                        Region reg;
                        reg.id = label++;
                        reg.rect = comp.rect;
                        reg.weight = 0;
                        reg.row = 0;
                        reg.rowStDev = 0;
                        reg.staticObs = false;
                        
                        column_vec3_t pos;
                        matrix3_t cov;
                        int numPts = 0;
#if !USE_CONVEX_HULL
                        // these values should be set already, but just in case ...
                        reg.el.ssxx = reg.el.ssyy = reg.el.ssxy = 0;
                        reg.el.numPoints = 0;
                        reg.el.center = point2(0, 0);
                        //reg.disparity.reserve(reg.rect.width);
                        reg.geometry.arr.reserve(reg.rect.width);
                        for (int c = reg.rect.x; c < reg.rect.x + reg.rect.width; c++)
                        {
                            int closestRow = -1;
                            int r;
                            for (r = reg.rect.y + reg.rect.height - 1; r >= reg.rect.y; r--)
                            {
                                if (m_maskImg[r+1][c+1][0] == 1) {

                                    // find the first nonzero cell starting from the bottom (closer to alice)
                                    if (closestRow == -1)
                                        closestRow = r;
                                    m_maskImg[r+1][c+1][0] = reg.id;

                                    float w = (*mapImg)[r][c][0];
                                    float disp = cell2disp * r;
                                    // compute average position, row and size of the blob
                                    // note: do not multiply r * cell2disp here yet, bad numerical conditioning
                                    column_vec3_t curPos = make_column_vec3(c * w, m_rowSum[r][c][0], r * w);
                                    pos += curPos;
                                    column_vec3_t curPosSq = make_column_vec3(c*w, sqrt(m_rowSumSq[r][c][0]), r*w);
                                    cov += prod(curPosSq, trans(curPosSq));
                                    reg.weight += w;

                                    //// deprecated  ////
                                    reg.row += m_rowSum[r][c][0];
                                    reg.rowStDev += m_rowSumSq[r][c][0];
                                    // data for the ellipse
                                    reg.el.ssxx += square(c);
                                    reg.el.ssyy += square(disp); // disparity
                                    reg.el.ssxy += disp * c;
                                    reg.el.center.x += c;
                                    reg.el.center.y += disp;
                                    reg.el.numPoints++;
                                    //// end deprecated ////
                                    //numPts++;
                                }
                            }
                            if (closestRow >= 0) {
                                reg.geometry.push_back(point2(c, closestRow * cell2disp));
                                //if (reg.disparity.empty())
                                //    reg.firstCol = c;
                                //reg.disparity.push_back(closestRow * cell2disp);
                            }
                        }
                        //reg.lastCol = reg.firstCol + reg.disparity.size() - 1;

                        // filter out small regions
                        if (reg.weight < m_smallObsThres)
                            continue;

                        // filter the data with a 4nd order FIR (only y)
                        const float w[5] = {1, 1, 2, 1, 1};
                        const float invsum = 1.0 / (w[0] + w[1] + w[2] + w[3] + w[4]);
                        point2arr& geom = reg.geometry;
                        float prev = geom[0].y;
                        float prev2 = prev;
                        for (int k = 1; k < int(geom.size()) - 2; k++)
                        { 
                            float old = geom[k].y;
                            geom[k].y = (prev2*w[0] + prev*w[1] + geom[k].y*w[2]
                                         + geom[k+1].y*w[3] + geom[k+2].y*w[4]) * invsum;
                            prev2 = prev;
                            prev = old;
                        }


#else

                        // store all the points in a vector AND in an opencv matrix ...
                        point2arr points;
                        CvMat ptMat = cvMat(reg.rect.width * reg.rect.height, 1, CV_32FC2);
                        cvCreateData(&ptMat);
                        float* mat = (float*) ptMat.data.ptr;
                        for (int r = reg.rect.y; r < reg.rect.y + reg.rect.height; r++)
                        {
                            for (int c = reg.rect.x; c < reg.rect.x + reg.rect.width; c++)
                            {
                                if (m_maskImg[r+1][c+1][0] == 1)
                                {
                                    m_maskImg[r+1][c+1][0] = reg.id;
                                    reg.row += m_rowSum[r][c][0];

                                    
                                    float w = (*mapImg)[r][c][0];
                                    column_vec3_t curPos(c, m_rowSum[r][c][0] / w, r);
                                    pos += curPos;
                                    cov += prod(curPos, trans(curPos));

                                    reg.rowStDev += m_rowSumSq[r][c][0];
                                    reg.weight += w;
                                    points.push_back(point2(c, r * cell2disp));
                                    *mat++ = c;
                                    *mat++ = r * cell2disp;
                                    //numPts++;
                                }
                            }
                        }
                        assert(points.size() > 0);
                        ptMat.rows = points.size();

                        // filter out small regions
                        if (reg.weight < m_smallObsThres) {
                            //MSG("Filtering out region " << reg.id << ", map sum was "
                            //    << reg.weight << ") < " << m_smallObsThres);
                            cvReleaseData(&ptMat); // TODO: try to avoid allocating memory at all
                            continue;
                        }
#endif
                        
                        // finish computing average pos and covariance
                        if (reg.weight != 0) {
                            pos /= reg.weight;
                            cov /= reg.weight;
                            cov -= prod(pos, trans(pos)); // get the covariance, not the correlation
                            
                            for (int i = 0; i < 3; i++)
                                reg.pos[i] = pos(i,0); // / reg.weight;//numPts;
                            for (int i = 0; i < 3; i++)
                                for (int j = 0; j < 3; j++)
                                    reg.cov[i][j] = cov(i, j); // / reg.weight;//numPts;

                            // fix values, convert from map row to disparity
                            reg.pos[2] *= cell2disp;
                            for (int i = 0; i < 3; i++)
                                reg.cov[i][2] *= cell2disp;
                            for (int j = 0; j < 3; j++)
                                reg.cov[2][j] *= cell2disp;

                            //// deprecated ////
                            reg.row /= reg.weight;
                            reg.rowStDev = sqrt(reg.rowStDev / reg.weight - reg.row*reg.row);
                            //// end deprecated ////
                            //assert(fabs(reg.cov[1][1] - reg.rowStDev*reg.rowStDev) < 1e-2);
                            //assert(fabs(reg.pos[1] - reg.row) < 1e-2);
                        } else {
                            reg.row = reg.rowStDev = 0.0; //// deprecated
                            memset(reg.cov, 0, sizeof(reg.cov));
                            memset(reg.pos, 0, sizeof(reg.pos));
                        }

#if !USE_CONVEX_HULL
                        // fit the ellipse to the points
                        reg.el.finish();
                        reg.el.center = reg.el.center / reg.el.numPoints;
                        // close the polygon with a straight line behind all the points
                        //if (minRow > 0) minRow--;
                        //reg.geometry.push_back(point2(maxCol, minRow * cell2disp));
                        //reg.geometry.push_back(point2(minCol, minRow * cell2disp));

#else
                        // ... use the point2arr to fit an ellipse to the points ...
                        reg.el.add_points(points);
                        reg.el.finish();
                        
                        // ... and the opencv matrix to calculate the convex hull ...
                        // .. but first allocate ANOTHER opencv matrix for the result ...
                        CvMat hull = cvMat(points.size(), 1, CV_32FC2);
                        cvCreateData(&hull);
                        // ... do the hard work ...
                        cvConvexHull2(&ptMat, &hull, CV_CLOCKWISE, 1 /* points, not indexes */);
                        // ... and copy the result back to the Region struct ...
                        mat = (float*) hull.data.ptr;
                        reg.geometry.resize(hull.rows * hull.cols);
                        for (unsigned int i = 0; i < reg.geometry.size(); i++)
                        {
                            float r = *mat++;
                            float c = *mat++;
                            reg.geometry[i] = point2(r, c);
                        }
                        // ... then finally release all the memory allocated for opencv.
                        cvReleaseData(&ptMat);
                        cvReleaseData(&hull);
#warning "FIXME: Need to think of a way to avoid all these memory (de)allocation and copying of data."

#endif
                        regions.push_back(reg);
                    } else { // connected component was zero-sized
                        ERRMSG("BUGBUG: cvFloodFill() did nothing, was the seed point masked???");
                        ERRMSG("BUGBUG: seed point: (c=" << c << ", r=" << r << ")" );
                        AGG_ASSERT(false);
                    }
                } // end of if (bigger than high threshold)
            } // end of for (all columns)
        } // end of for (all rows)
    }

    void DisparityDetector::findRegionsLocal(StereoImageBlob* /*sib*/)
    {
        vector<Region> &regions = m_localRegs;
        regions.clear();

        Bitmap<float, 1>* mapImg = &m_localMap;
        // we need the underlying IplImage to call OpenCV functions
        IplImage* map = mapImg->getImage();

        // valid region labels start at 2 (0 means not assigned, 1 is used internally)
        int label = 2;
        int height = mapImg->getHeight();
        int width = mapImg->getWidth();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                cost_t val = (*mapImg)[r][c][0];

                // find peaks using the high threshold
                if (val > m_highThres && m_localConf[r][c] > m_confThres)
                {
                    cost_t loDiff = val - m_lowThres;
                    cost_t hiDiff = 1e6;//numeric_limits<cost_t>::max();
                    CvConnectedComp comp;

                    // if this cell belongs to a previously detected region, skip it
                    if (m_localMask[r+1][c+1][0] != 0)
                        continue;

                    // find the connected component using the low threshold
                    cvFloodFill(map, cvPoint(c, r), cvRealScalar(1),
                                cvRealScalar(loDiff), cvRealScalar(hiDiff),
                                &comp, CV_FLOODFILL_FIXED_RANGE | CV_FLOODFILL_MASK_ONLY | 4,
                                m_localMask.getImage());
                    
                    if (comp.rect.width > 0 && comp.rect.height > 0)
                    {
                        for (int c = comp.rect.x; c < comp.rect.x + comp.rect.width; c++)
                        {
                            for (int r = comp.rect.y + comp.rect.height - 1; r >= comp.rect.y; r--)
                            {
                                if (m_localMask[r+1][c+1][0] == 1) {
                                    if (m_localConf[r][c] < m_confThres) {
                                        m_localMask[r+1][c+1][0] = 0; // invalid point
                                        continue;
                                    }
                                }
                            }
                        }
                    } else { // connected component was zero-sized
                        ERRMSG("BUGBUG: cvFloodFill() did nothing, was the seed point masked???");
                        ERRMSG("BUGBUG: seed point: (c=" << c << ", r=" << r << ")" );
                    }

                } // end of if (bigger than high threshold)
            } // end of for (all columns)
        } // end of for (all rows)

        //IplImage* mask = m_localMask.getImage();
        //cvDilate(mask, mask); // default structuring element
        //cvErode(mask, mask); // default structuring element

        CvMemStorage* storage = cvCreateMemStorage(0);
        Bitmap<uint8_t,1> tmp(m_localMask); // create a copy, cvFindCoutour will destroy the source
        CvContour* contour = 0;

        // now, find connected components on the mask image
        cvFindContours(tmp.getImage(), storage, (CvSeq**) &contour, sizeof(CvContour),
                       CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);


        multimap<float, int> sorted;
        int nRemoved = 0;

        int regId = 2; // maybe start from 1 (zero is no region) ... this is for backward compat
        int numComp = 0;
        for( ; contour != 0; contour = (CvContour*) contour->h_next )
        {
            CvSeqReader reader;
            cvStartReadSeq( (CvSeq*) contour, &reader);
            int length = contour->total;

            Region reg;
            reg.id = regId++;
            reg.rect = contour->rect;
            // the mask is 2 pixel taller and 2 pixel wider, decrease x and y by 1
            if(reg.rect.x == 0) // shouldn't happen, but ...
                reg.rect.width--;
            else
                reg.rect.x--;
            if(reg.rect.y == 0) // shouldn't happen, but ...
                reg.rect.height--;
            else
                reg.rect.y--;
            reg.weight = 0;
            reg.row = 0;
            reg.rowStDev = 0;
            
            // Sum the cells in the map to get the region weight
            // NOTE: this is not always correct (the bounding box may
            // overlap with another region, but it's the best that I can
            // get in 5 min of programming ;-)
            for (int c = reg.rect.x; c < reg.rect.x + reg.rect.width; c++)
            {
                for (int r = reg.rect.y + reg.rect.height - 1; r >= reg.rect.y; r--)
                {
                    if (m_localMask[r+1][c+1][0] == 1) {
                        m_localMask[r+1][c+1][0] = reg.id;
                        reg.weight += m_localMap[r][c];
                    }
                }
            }
            if (reg.weight < m_smallObsLoc)
                continue;

            point2arr* geom = 0;
            if (int(regions.size()) == m_maxLocRegs)
            {
                nRemoved++;
                // need to remove some small region
                if (sorted.size() == 0)
                {
                    // add all the current regions
                    for (int i = 0; i < int(regions.size()); i++)
                        sorted.insert(make_pair(regions[i].weight, i));
                }
                // pick the smallest one
                int smallest = sorted.begin()->second;
                float smallWeight = sorted.begin()->first;
                if (smallWeight >= reg.weight)
                    continue; // just don't add it

                // replace the smallest with the new one
                regions[smallest] = reg;
                geom = &regions[smallest].geometry;
                // replace it in the multimap too
                sorted.erase(sorted.begin());
                sorted.insert(make_pair(reg.weight, smallest));
            } else {
                // add the new region
                regions.push_back(reg);
                geom = &regions.back().geometry;
            }

            geom->clear();

            for (int i = 0; i < length; i++)
            {
                CvPoint pt;
                CV_READ_SEQ_ELEM(pt, reader);
                geom->push_back(point2(pt.x, pt.y));
            }
        }

        if (nRemoved > 0) {
            MSG("Had to remove " << nRemoved << " small local map regions");
        }

        cvReleaseMemStorage(&storage);
    }

    /*! Convert from image frame to local frame
     * \param s Source vector [col, row, disp]
     * \param d Destination vector [x, y, z]
     */
    static inline void convertImageToLocal(StereoImageBlob* sib, float* s, float* d)
    {
        // do all the trasformations required
        StereoImageBlobImageToSensor(sib, s[0], s[1], s[2], &d[0], &d[1], &d[2]);
        StereoImageBlobSensorToVehicle(sib, d[0], d[1], d[2], &s[0], &s[1], &s[2]);
        StereoImageBlobVehicleToLocal(sib, s[0], s[1], s[2], &d[0], &d[1], &d[2]);
    }

    static inline void convertLocalToImage(StereoImageBlob* sib, float* s, float* d)
    {
        // do all the trasformations required
        StereoImageBlobLocalToVehicle(sib, s[0], s[1], s[2], &d[0], &d[1], &d[2]);
        StereoImageBlobVehicleToSensor(sib, d[0], d[1], d[2], &s[0], &s[1], &s[2]);
        StereoImageBlobSensorToImage(sib, s[0], s[1], s[2], &d[0], &d[1], &d[2]);
    }

    static inline point2 imageToLocal(StereoImageBlob* sib, const point2 p, float row) // p.x = col, p.y = disp
    {
        float out[3], tmp[3];
        tmp[0] = p.x;
        tmp[1] = row;
        tmp[2] = p.y;
        assert(tmp[2] > 0); // disparity 0 means infinity!!!
        convertImageToLocal(sib, tmp, out);
        return point2(out[0], out[1], out[2]);
    }

    static inline point2 localToImage(StereoImageBlob* sib, const point2 p)
    {
        float out[3], tmp[3];
        tmp[0] = p.x;
        tmp[1] = p.y;
        tmp[2] = p.z;
        convertLocalToImage(sib, tmp, out);
        return point2(out[0], out[2]); // output column, disparity (dispmap frame)
    }

    /*!
     * Given the current regions in m_regions, add them to the local map and, if enough
     * evidence is found, declare them as static obstacles.
     * Then, run findRegions() over the whole map (masked by the already found regions)
     * and generate MapElements.
     */
    void DisparityDetector::updateLocalMap(StereoImageBlob* sib, vector<MapElement>* elements)
    {
        float cell2disp = float(m_maxDisp) / float(m_mapHeight);
        float disp2cell = float(m_mapHeight) / float(m_maxDisp);

        point2 center = point2(sib->state.localX, sib->state.localY);
/*
        float dist = min(m_locMapWidth, m_locMapHeight) * m_locMapCenter;
        center = center + dist*point2(cos(sib->state.localYaw), sin(sib->state.localYaw));
*/
        center = center + m_locMapCenter*point2(cos(sib->state.localYaw),
                                                sin(sib->state.localYaw));

        m_localMap.shiftTo(center);
        m_localConf.shiftTo(center);

        m_tempMask.clear();

        const int FRAC = 10; // fractional bits in CvPoint
        const float FRAC_MUL = float(1 << FRAC);
        // for each region, draw a polygon of 1's in the mask so we know
        // that cell matches a detected obstacle
        for (int i = 0; i < int(m_regions.size()); i++)
        {
            Region& reg = m_regions[i];
            // polygon in disparity map frame (image col, disparity)
            point2 locPts[4] = {
                point2(reg.rect.x, reg.rect.y * cell2disp),
                point2(reg.rect.x + reg.rect.width - 1, reg.rect.y * cell2disp),
                point2(reg.rect.x + reg.rect.width - 1, (reg.rect.y + reg.rect.height - 1) * cell2disp),
                point2(reg.rect.x, (reg.rect.y + reg.rect.height - 1) * cell2disp),
            };
            CvPoint pts[4];
            // convert to local frame, then to local map frame (locmap col, locmap row)
            for (int j = 0; j < 4; j++)
            {
                point2 pt = imageToLocal(sib, locPts[j], reg.row);
                float row, col;
                m_localMap.toMapFrame(pt.x, pt.y, &row, &col);
                pts[j].x = int(col * FRAC_MUL + 0.5);
                pts[j].y = int(row * FRAC_MUL + 0.5);
            }
            cvFillConvexPoly(m_tempMask.getImage(), pts, sizeof(pts)/sizeof(pts[0]),
                             cvRealScalar(255), 4, FRAC);
        }

        // For each cell, if the temp mask is 1 (something was detected there)
        // then find the corresponding cell in the disparity map, and update
        // the local map accordingly, increasing the confidence.
        // Instead, if nothing was found in the cell, decrease its confidence.
        for (int r = 0; r < m_localMap.getHeight(); r++)
        {
            for (int c = 0; c < m_localMap.getWidth(); c++)
            {
                if (m_tempMask[r][c][0] != 0)
                {
                    // find the coordinates of this cell in image frame
                    point2 ptLoc = m_localMap.toLocalFrame(point2(c, r));
                    ptLoc.z = sib->state.localZ;
                    point2 pt = localToImage(sib, ptLoc);
                    int dmapCol = int(pt.x + 0.5);
                    int dmapRow = int(pt.y * disp2cell + 0.5);

                    if (dmapCol < 0 || dmapCol >= m_mapImg.getWidth() ||
                        dmapRow < 0 || dmapRow >= m_mapImg.getHeight())
                    {
                        // this actually happen frequently, seems normal ...
                        //AGG_ASSERT(false);
                        //ERRMSG("Error, pixel is set in local mask but is outside of image frame map!!!");
                        continue;
                    }

                    int regId = m_maskImg[dmapRow+1][dmapCol+1][0];
                    if (regId >= 2) { // valid labels start at 2
                        float mapVal = m_mapImg[dmapRow][dmapCol][0];
                        float val = m_localMap[r][c];
                        float conf = m_localConf[r][c];
                        float newVal = (val + mapVal) * 0.5;
                        m_localMap[r][c] = newVal;
                        //m_localConf[r][c] = conf + 1; // just a count at the moment!!!
                        m_localConf[r][c] = 1.0 - (1.0 - conf)*0.5;
                        if (m_localMask[r+1][c+1][0] >= 1) // cant do this unless I use another temp map to do the polygon drawing...
                        //if (m_localMap[r][c] >= m_highThres)
                            m_regions[regId - 2].staticObs = true;

                        m_localMask[r+1][c+1][0] = 1;
                        //maxVal = max(maxVal, newVal);
                        //totalVal += newVal;
                        
                    } else {
                        // not detected as obstacle, clear it
                        m_localMask[r+1][c+1][0] = 0;
                    }
                }

                // decay confidence level for non-updated cells
                if (m_localMask[r+1][c+1][0] != 1) {
                    const float decayRatio = 0.95; // MAGIC NUMBER
                    // no new data here, reduce confidence
                    //m_localMap[r][c] = m_localMap[r][c] * decayRatio;
                    m_localConf[r][c] = m_localConf[r][c] * decayRatio;
                }
            }
        }

        m_localMask.clear();

        int prevNumRegs = m_localRegs.size();
        
        // find regions in local map
        findRegionsLocal(sib);

        // output regions as map elements
        int i;
        for (i = 0; i < int(m_localRegs.size()); i++)
        {
            Region& reg = m_localRegs[i];
            MapElement mel;
            mel.setId(moduleId, MAX_TRACK_ID + i + 1);
            mel.frameType = FRAME_LOCAL;
            mel.setTypeStereoObstacle();
            mel.label.resize(1);
            mel.label[0] = "stereo-obs static";
            mel.setColor(MAP_COLOR_ORANGE);
        
            mel.geometry.resize(reg.geometry.size());
            for (int i = 0; i < int(reg.geometry.size()); i++) {
                mel.geometry[i] = m_localMap.toLocalFrame(reg.geometry[i]);
                mel.geometry[i].z = sib->state.localZ; // FIXME (but better than leaving it to zero!)
            }

            if (reg.geometry.size() > 10) {
                // try to reduce the geometry complexity
                double tol = m_locMapRes;
                //mel.setGeometry(measure.geometry.simplify(tol)); // this doesn't work ... yet
                mel.geometry = point2arr(mel.geometry).simplify(tol);
                mel.updateFromGeometry();
                //MSG("simplify() on static obs reduced from " << measure.geometry.size() << " to " << mel.geometry.size());
            }
            mel.position = mel.center;

            //mel.label[1] = "obj" + toStr(j);
            mel.state = sib->state;
            mel.height = 1; // FIXME!!!!
            mel.heightVar = 1;// FIXME!!!!
            mel.elevation = sib->state.localZ; // FIXME
            mel.elevationVar = 1;//FIXME
            
            mel.conf = m_confThres;// FIXME!!!!
            elements->push_back(mel);
        }

        MapElement clearEl;
        clearEl.clear();
        clearEl.setTypeClear();
        for (; i < prevNumRegs; i++)
        {
            // clear old elements, if any
            clearEl.setId(moduleId, MAX_TRACK_ID + i + 1);
            elements->push_back(clearEl);
        }

    }

    /*!
     * Converts a Region object into a Measure object.
     * It converts from image to local frame and copied the relevant information.
     * Only used internally.
     */
    void DisparityDetector::regionToMeasure(const Region& reg, Measure* measure, StereoImageBlob* sib,
                                            const float himg2sens[4][4], const float sens2loc[4][4])
    {
        float cell2disp = float(m_maxDisp) / float(m_mapHeight);
        //measure->flags = TRACK_DYNAMIC; // default to dynamic
        measure->flags = 0; // default to static
        measure->updated(true);
        memset(measure->state, 0, sizeof(measure->state));

        // convert from image to local frame
        float tmp[3]; // temp storage (col, row, disp)
        tmp[0] = reg.el.center.x;
        tmp[1] = reg.row;
        tmp[2] = reg.el.center.y;
        assert(tmp[2] > 0); // disparity 0 means infinity!!!
        convertImageToLocal(sib, tmp, measure->state);

        // convert the convex hull and the ellipse to local frame too
        measure->el = ellipse();
        measure->geometry.resize(reg.geometry.size() + 2);
        unsigned int j;
        for (j = 0; j < reg.geometry.size(); j++)
        {
            measure->geometry[j] = imageToLocal(sib, reg.geometry[j], reg.row);
        }
        // close the polygon
        point2 p1back = point2(reg.rect.x + reg.rect.width, reg.rect.y * cell2disp);
        point2 p2back = point2(reg.rect.x, reg.rect.y * cell2disp);
        measure->geometry[j++] = imageToLocal(sib, p1back, reg.row);
        measure->geometry[j++] = imageToLocal(sib, p2back, reg.row);

        // transform the height from pixels into meters -- maybe there's a better way ...
        float up[3] = { reg.el.center.x, reg.row - reg.rowStDev * 1.5, reg.el.center.y };
        float down[3] = { reg.el.center.x, reg.row + reg.rowStDev * 1.5, reg.el.center.y };
        float ul[3], dl[3]; // up local, down local
        convertImageToLocal(sib, up, ul);
        convertImageToLocal(sib, down, dl);
        measure->height = fabs(ul[2] - dl[2]);

        float pi[3];
        pi[0] = reg.el.center.x;
        pi[1] = reg.row;
        pi[2] = reg.el.center.y;
        float cov[4][4];
        calcPointCov(cov, pi, himg2sens, sens2loc);

        fill(&measure->var[0][0], &measure->var[5][6], 0);
        // we only have the covariance for the position (we only have the position!)
        for (int j = 0; j < 3; j++)
            for (int k = 0; k < 3; k++)
                measure->var[j][k] = cov[j][k];
        for (int j = 0; j < 3; j++)
            for (int k = 0; k < 3; k++)
                measure->var[j+3][k+3] = m_velCov0(j, k);

        measure->likelihood = 0.4; // FIXME: magic
        measure->maxLikelihood = measure->likelihood;
    }


    // some fast 3x3 matrix inversion and determinant routines

    // return the determinant of the 3x3 matrix m
    template <class T>
    inline T fast_det_3x3(const T m[3][3])
    {
        // generated with matlab 6.5
        T det = (m[1][1]*m[2][2]-m[1][2]*m[2][1])*m[0][0]+m[2][0]*m[0][1]*m[1][2]-m[2][0]*m[0][2]*m[1][1]-m[1][0]*m[0][1]*m[2][2]+m[1][0]*m[0][2]*m[2][1];
        return det;
    }

    // m is a 3x3 matrix, det is det(m) precalculated. Use this if you know
    // the determinant already.
    template <class T>
    inline void fast_inv_3x3(const T m[3][3], T inv[3][3], T det)
    {
        assert(det >= 1e-15); // not singular
        T inv_det = 1 / det;
        // generated with matlab 6.5
        inv[0][0] = (m[1][1]*m[2][2]-m[1][2]*m[2][1])*inv_det;
        inv[0][1] = -(m[0][1]*m[2][2]-m[0][2]*m[2][1])*inv_det;
        inv[0][2] = (m[0][1]*m[1][2]-m[0][2]*m[1][1])*inv_det;
        inv[1][0] = -(m[1][0]*m[2][2]-m[1][2]*m[2][0])*inv_det;
        inv[1][1] = (m[0][0]*m[2][2]-m[0][2]*m[2][0])*inv_det;
        inv[1][2] = -(m[0][0]*m[1][2]-m[0][2]*m[1][0])*inv_det;
        inv[2][0] = (m[1][0]*m[2][1]-m[1][1]*m[2][0])*inv_det;
        inv[2][1] = -(m[0][0]*m[2][1]-m[0][1]*m[2][0])*inv_det;
        inv[2][2] = (m[0][0]*m[1][1]-m[0][1]*m[1][0])*inv_det;
    }

    // as above, but here the determinant is calculated within the function
    template <class T>
    inline void fast_inv_3x3(const T m[3][3], T inv[3][3])
    {
        fast_inv_3x3(m, inv, fast_det_3x3(m));
    }

    void DisparityDetector::trackToMapElement(Track& track, MapElement* mel,
                                              StereoImageBlob* sib)
    {
        assert(track.id() != -1);
        mel->setId(moduleId, track.id());
        measureToMapElement(track, mel, sib);
    }

    void DisparityDetector::measureToMapElement(Measure& measure, MapElement* mel,
                                                StereoImageBlob* sib)
    {
        mel->frameType = FRAME_LOCAL;
        if (measure.isDynamic()) {
            mel->setTypeVehicle();
            mel->setColor(MAP_COLOR_CYAN);
        } else {
            mel->setTypeStereoObstacle();
            mel->setColor(MAP_COLOR_LIGHT_BLUE);
        }
        //mel->setTypeObstacleEdge();
        mel->label.resize(1);
        mel->label[0] = "stereo-obs-perceptor";

        
        //vert.resize(measure.geometry.size());
        
        // set the position and its uncertainty
        ellipse ell;
        ell.ssxx = measure.var[0][0];
        ell.ssyy = measure.var[1][1];
        ell.ssxy = measure.var[0][1];
        ell.numPoints = 1; // so finish() will do its job
        ell.finish();

        if (measure.geometry.size() <= 10) {
            mel->setGeometry(measure.geometry);
        } else {
            // try to reduce the geometry complexity
            double tol = min(ell.a, ell.b);
            //mel->setGeometry(measure.geometry.simplify(tol)); // this doesn't work ... yet
            mel->geometry = measure.geometry.simplify(tol);
            mel->updateFromGeometry();
            //MSG("simplify() reduced from " << measure.geometry.size() << " to " << mel->geometry.size());
        }
        
        mel->position.x = measure.state[0];
        mel->position.y = measure.state[1];
        mel->position.z = measure.state[2];
        mel->position.max_var = ell.a;
        mel->position.min_var = ell.b;
        mel->position.axis = ell.theta;
        
        // uncertainty is always the same for each point. Is having uncertainty
        // for each point really useful (in some other perceptor)?
        for (unsigned int k = 0; k < mel->geometry.size(); k++)
        {
            mel->geometry[k].max_var = mel->position.max_var;
            mel->geometry[k].min_var = mel->position.min_var;
            mel->geometry[k].axis = mel->position.axis;
        }

        // set the center to be the same as the position
        mel->center = mel->position;

        // set the velocity with its uncertainty
        ell.ssxx = measure.var[3][3];
        ell.ssyy = measure.var[4][4];
        ell.ssxy = measure.var[3][4];
        ell.numPoints = 1; // so finish() will do its job
        ell.finish();

        mel->velocity.x = measure.state[3];
        mel->velocity.y = measure.state[4];
        mel->velocity.z = measure.state[5];
        mel->velocity.max_var = ell.a;
        mel->velocity.min_var = ell.b;
        mel->velocity.axis = ell.theta;

        //mel->label[1] = "obj" + toStr(j);
        mel->state = sib->state;
        mel->height = measure.height;
        mel->heightVar = measure.var[2][2]; // ?? don't know!
        mel->elevation = sib->state.localZ - measure.state[2] + mel->height/2;
        mel->elevationVar = measure.var[2][2];
        
        mel->conf = measure.likelihood;
        
    }


    static inline float dotConv(float x1, float y1, float x2, float y2, float icov[2][2])
    {
        return x1*(icov[0][0]*x2 + icov[0][1]*y2) + y1*(icov[1][0]*x2 + icov[1][1]*y2);
    }

    static inline float crossConv(float x1, float y1, float x2, float y2, float icov[2][2])
    {
        return dotConv(x1, y1, y2, -x2, icov);
    }

    static inline float dotConv(const point2& p1, const point2& p2, float icov[2][2])
    {
        return dotConv(p1.x, p1.y, p2.x, p2.y, icov);
    }

    static inline float crossConv(const point2& p1, const point2& p2, float icov[2][2])
    {
        return crossConv(p1.x, p1.y, p2.x, p2.y, icov);
    }

    static inline float norm2Cov(float x, float y, float icov[2][2])
    {
        return x*(icov[0][0]*x + icov[0][1]*y) + y*(icov[1][0]*x + icov[1][1]*y);
    }

    static inline float norm2Cov(const point2& p, float icov[2][2])
    {
        return norm2Cov(p.x, p.y, icov);
    }

    // returns the distance from pt to the line passing through linePt parallel to vec, using the metric icov
    static inline float distFrom(const point2& pt, const point2& linePt, const point2& vec, float icov[2][2])
    {
        return crossConv(vec, (pt - linePt), icov); // NOTE: can be negative
    }

    // returns the length of the projection of pt on line passing through linePt parallel to vec, using the metric icov
    static inline float projLen(const point2& pt, const point2& linePt, const point2& vec, float icov[2][2])
    {
        return dotConv(vec, (pt - linePt), icov); // NOTE: can be negative
    }

    /*
     * For each point in the first line (scan, blob, whatever has points in it), find the
     * closest point in the second line. Record the average distance vector, the RMS^2
     * (average distance^2) and the actual indexes (within line2) of the points.
     * The distance is defined by the metric Q, i.e. dist_xy^2 = x*Q*y'
     * Returns the number of points matched.
     */
    int matchLines(const point2arr& line1, const point2arr& line2,
                    const matrix2_t& Q, double thres,
                    // outputs
                    double* outRms2, vector<int>* out, vector2_t* outVec)
    {
        // Easiest (slowest?) ipc (iterative point matching) algorithm ever:
        // compare all of them and for each point in line1 choose the
        // closest point in line2 (using the metric Q).

        vector2_t avgDistVec = make_vec2(0, 0);
        double rms2 = 0;
        int nmatches = 0;
        if (out != NULL)
            out->resize(line1.size());

        for (int i = 0; i < int(line1.size()); i++)
        {
            int minJ = -1;
            double minDist2 = numeric_limits<double>::max();
            vector2_t minVec;
            for (int j = 0; j < int(line2.size()); j++)
            {
                vector2_t v1 = make_vec2(line1[i].x, line1[i].y);
                vector2_t v2 = make_vec2(line2[j].x, line2[j].y);
                vector2_t distVec(v2 - v1);
                double dist2 = inner_prod(distVec, vector2_t(prod(Q, distVec)));
                assert(dist2 >= 0);
                if (dist2 < thres*thres && minDist2 > dist2)
                {
                    minJ = j;
                    minDist2 = dist2;
                    minVec = distVec;
                }
            }

            if (minJ >= 0) {
                // match found
                nmatches++;
                avgDistVec += minVec;
                rms2 += minDist2;
                if (out != NULL)
                    (*out)[i] = minJ;
            }
        }
        rms2 /= nmatches;
        avgDistVec /= nmatches;
        if (outVec != NULL)
            *outVec = avgDistVec;
        if (outRms2 != NULL)
            *outRms2 = rms2;
        return nmatches;
    }

    void DisparityDetector::buildProjMatrix(StereoImageBlob* sib)
    {
        // calculate transformation matrix from image to sensor in homogeneous
        // coordinates (col, row, disp, 1) -> (Xs, Ys, Zs, Ws)
        float sx = sib->leftCamera.sx;
        float sy = sib->leftCamera.sy;
        float cx = sib->leftCamera.cx;
        float cy = sib->leftCamera.cy;
        float b = sib->baseline;

        // projection matrix with disparity
        // alpha*(col, row, disp, 1)' = sens2img * (Xs, Ys, Zs, Ws)'
        const float sens2img_arr[4*4] = {
            sx,   0,     cx,    0,
            0,    sy,    cy,    0,
            0,    0,     0,     sx*b,
            0,    0,     1,     0
        };
        matrix4_t sens2img;
        copy(sens2img_arr, sens2img_arr+16, sens2img.data());

        // x = (c - cx) * baseline / d
        // y = (r - cy) * (sy/sx) * baseline / d
        // z = sx * baseline / d;
        const float img2sens_arr[4*4] = {
            b,    0,        0,       -cx*b,
            0,    b*sx/sy,  0,       -cy*b*sx/sy,
            0,    0,        0,          sx*b,
            0,    0,        1,          0
        };
        matrix4_t img2sens;
        copy(img2sens_arr, img2sens_arr+16, img2sens.data());

        // copy matrices from the blob into ublas matrices
        matrix4_t veh2loc, loc2veh;
        copy(&sib->veh2loc[0][0], &sib->veh2loc[3][4], veh2loc.data());
        copy(&sib->loc2veh[0][0], &sib->loc2veh[3][4], loc2veh.data());
        matrix4_t sens2veh, veh2sens;
        copy(&sib->leftCamera.sens2veh[0][0], &sib->leftCamera.sens2veh[3][4], sens2veh.data());
        copy(&sib->leftCamera.veh2sens[0][0], &sib->leftCamera.veh2sens[3][4], veh2sens.data());

        matrix4_t loc2sens(prod(veh2sens, loc2veh));
        m_loc2img = prod(sens2img, loc2sens);

        //bool singular;
        //m_img2loc = invert(m_loc2img, &singular);
        //assert(singular == FALSE);
        matrix4_t sens2loc(prod(veh2loc, sens2veh));
        m_img2loc = prod(sens2loc, img2sens);
    }

    void concatRegionGeoms(const point2arr& geom1, const point2arr& geom2, point2arr* geomOut)
    {
        // just concatenate the two geometries, but making sure that for every
        // column value (integer) there is a point. Probably just changing the
        // representation of the geometry would be better ...
        point2arr& out = *geomOut;

        int size1 = geom1.size();
        int size2 = geom2.size();

        int start1 = int(geom1[0].x);
        int end1 = int(geom1[size1 - 1].x);
        int start2 = int(geom2[0].x);
        int end2 = int(geom2[size2 - 1].x);
        assert(end1 <= start2);

        out.resize(end2 - start1 + 1);

        for (int i = 0; i < size1; i++)
            out[i] = geom1[i];

        // interpolate in the middle
        int k;
        for (k = end1; k < start2; k++)
        {
            float p = float(k - end1) / (start2 - end1);
            out[k - start1] = point2(k, geom1.arr.back().y*p + geom2.arr.back().y*(1-p));
        }

        assert(k == start2);

        for (; k <= end2; k++)
            out[k - start1] = geom2[k - start2];

#ifndef NDEBUG
        for (volatile int i = 0; i < int(out.size()); i++)
            assert(int(out[i].x) == i + start1);
#endif
    }


    void mergeRegionGeoms(const Region& reg1, const Region& reg2, Region* regOut)
    {
        assert(regOut != NULL);
        point2arr& out = regOut->geometry;
        out.clear();
        out.arr.reserve(reg1.geometry.size() + reg2.geometry.size());

        const point2arr& geom1 = reg1.geometry;
        const point2arr& geom2 = reg2.geometry;
        int size1 = geom1.size();
        int size2 = geom2.size();

        int start1 = int(geom1[0].x);
        int end1 = int(geom1[size1 - 1].x);
        int start2 = int(geom2[0].x);
        int end2 = int(geom2[size2 - 1].x);
        assert(end1 - start1 == size1 - 1);
        assert(end2 - start2 == size2 - 1);

        // check if they do overlap at all
        if (end1 < start2) {
            // just output geom1 then geom2
            concatRegionGeoms(geom1, geom2, &out);
        } else if(end2 < start1) {
            // just output geom2 then geom1
            concatRegionGeoms(geom2, geom1, &out);
        } else {

            // here we're sure the two geometries overlap
            int k1 = start1, k2 = start2;

            // copy initial non matched points
            for (; k1 < k2; k1++)
                out.push_back(geom1[k1 - start1]);
            for (; k2 < k1; k2++)
                out.push_back(geom2[k2 - start2]);
            assert(k1 == k2);

            // points that actually match are handled here
            int end = min(end1, end2);
            for(; k1 <= end; k1++) {
                // keep the closest point
                if (geom1[k1 - start1].y > geom2[k1 - start2].y)
                    out.push_back(geom1[k1 - start1]);
                else
                    out.push_back(geom2[k1 - start2]);
            }
            k2 = k1;

            // copy final non matched points
            for (; k1 <= end1; k1++)
                out.push_back(reg1.geometry[k1 - start1]);
            for (; k2 <= end2; k2++)
                out.push_back(reg2.geometry[k2 - start2]);
        }

        // set other fields in the Region to allow further merging
        regOut->rect.x = min(reg1.rect.x, reg2.rect.x);
        regOut->rect.width = max(reg1.rect.x + reg1.rect.width, reg2.rect.x + reg2.rect.width) - regOut->rect.x;
        
        // these are not strictly necessary for merging, but are easy to set
        regOut->rect.y = min(reg1.rect.y, reg2.rect.y);
        regOut->rect.height = max(reg1.rect.y + reg1.rect.height, reg2.rect.y + reg2.rect.height) - regOut->rect.y;

        // merge ellipses (used for size check)
        regOut->el.ssxx = reg1.el.ssxx + reg2.el.ssxx;
        regOut->el.ssyy = reg1.el.ssyy + reg2.el.ssyy;
        regOut->el.ssyy = reg1.el.ssxy + reg2.el.ssxy;
        regOut->el.numPoints = reg1.el.numPoints + reg2.el.numPoints;
        regOut->el.center = point2(regOut->el.ssxx, regOut->el.ssyy)/regOut->el.numPoints;
        regOut->el.finish();

        regOut->id = reg1.id;
        regOut->weight = reg1.weight + reg2.weight;
#ifndef NDEBUG
        for (volatile int i = 0; i < int(out.size()); i++)
        {
            // for some reason, this is happening ... so I put this assert here in the hope
            // to catch this bug when it happens and be able to figure it out
            assert(float(out[i].y) > 0); // disparity 0 means infinity!
            assert(int(out[i].x) == i + min(start1, start2));
        }
#endif

    }

    void DisparityDetector::predict(Track* tr, double elapsed)
    {
        using ublas::zero_matrix;
        using ublas::banded_matrix;
        using ublas::diagonal_matrix;
        using ublas::slice;

        // apply our simple model to predict the position
        // In this simple case, the model can be separated into three
        // indipentend models, one for x, one for y and one for z,
        // all equal to each other.
        double A_arr[2*2] = { // state = [ x, vel_x ] (same for y, z)
            1, elapsed,
            0, 1
        };
        matrix2_t A2;
        copy(A_arr, A_arr+4, A2.data());
        
        matrix6_t P;
        copy(&tr->var[0][0], &tr->var[5][6], P.data());

        vector6_t state, newState;
        copy(tr->state, tr->state+6, state.data());

#ifndef NDEBUG
        float detP3 = det3(subrange(P, 0,3, 0,3));
        if (detP3 < 0) {
            ERRMSG("det(P3) = " << detP3 << " < 0!!!");
            ERRMSG("P = " << P);
            assert(false);
        }
        assert_matrix_nan_inf(P);
#endif

        // KF update equations
        // x(t+1) = A*x
        slice xs(0, 3, 2);
        slice ys(1, 3, 2);
        slice zs(2, 3, 2);
        // update state, using 3 indipendent models for x, y, z (faster)
        noalias(project(newState, xs)) = prod(A2, project(state, xs));
        noalias(project(newState, ys)) = prod(A2, project(state, ys));
        noalias(project(newState, zs)) = prod(A2, project(state, zs));
        // use the full size 6x6 matrix equations for the covariance
        // P(t+1) = A*P*A' + Q (A = diag(A2, A2, A2))
/*
        banded_matrix<double> A(6, 6, 1, 1);
        for (int i = 0; i < 6; i++)  // why is it so difficult to just clear a matrix???
            for (int j = i-1; j < i+1; j++)
                if (j >= 0 && j < 6)
                    A(i, j) = 0;
*/
        matrix6_t A(zero_matrix<double>(6, 6));
        project(A, xs, xs) = A2;
        project(A, ys, ys) = A2;
        project(A, zs, zs) = A2;
        
        matrix6_t Q = m_modelErr*elapsed;

        P = prod(A, matrix6_t(prod(P, trans(A)))) + Q;

#ifndef NDEBUG
        detP3 = det3(subrange(P, 0,3, 0,3));
        if (detP3 < 0) {
            ERRMSG("det(P3) = " << detP3 << " < 0!!!");
            ERRMSG("P = " << P);
            ERRMSG("A = " << A);
            ERRMSG("Q = " << Q);
            assert(false);
        }
        assert_matrix_nan_inf(P);
#endif

        // copy back to the track
        copy(P.data(), P.data()+6*6, &tr->var[0][0]);
        copy(newState.data(), newState.data()+6, tr->state);

        assert_matrix_nan_inf(make_matrix<matrix6_t>((float*)tr->var));

        // update (translate) the geometry
        point2 vec(newState[0] - state[0],
                   newState[1] - state[1],
                   newState[2] - state[2]);
        for (int i = 0; i < int(tr->geometry.size()); i++)
            tr->geometry[i] = tr->geometry[i] + vec;

        vector3_t posDiff = subrange(newState - state, 0, 3);

        //if (inner_prod(posDiff, posDiff) > square(0.2)) // MAGIC NUMBER (20cm)
        //    tr->updated(true);
        if (tr->confirmed())
            tr->updated(true);

        // update ellipse, do we use it anymore?
        tr->el.center.x += vec.x;
        tr->el.center.y += vec.y;
    }

    void DisparityDetector::updateTrackWith(Track* tr, const Measure& meas, double elapsed)
    {
        //using namespace ublas; // don't want to do this because of ublas::vector and std::vector
        using ublas::c_matrix;
        using ublas::identity_matrix;
        using ublas::zero_matrix;
        using ublas::diagonal_matrix;
        using ublas::symmetric_adaptor;
        using ublas::range;

        vector3_t m;
        copy(meas.state, meas.state + 3, m.data());

        matrix6_t P;
        copy(&tr->var[0][0], &tr->var[5][6], P.data());

        vector6_t state, newState;
        copy(tr->state, tr->state+6, state.data());

        // do it with the full matrix, then think of a way to speed it up
        // (premature optimizations ...)

        // x(t|t) = x(t|t-1) + K(y - C*x(t|t-1))
        // K = P(t|t-1)*C'*(C*P(T|T-1)*C' + R)^-1)
        // P(t|t) = (I-K*C)*P(t|t-1)
        // C = [I3, 0], R = diag(m_errorVar)

        range Cr(0, 3); // faster than using an actual matrix
#ifndef NDEBUG
        float detP3 = det3(project(P, Cr, Cr));
        if (detP3 < 0) {
            ERRMSG("det(C*P*C') < 0 (=" << detP3 << "!!!");
            assert(false);
        }
        assert_matrix_nan_inf(P);
#endif

        // measure error, taken directly from the measure
        matrix3_t R;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                R(i, j) = meas.var[i][j];

#if 1
        // S = C*P*C' + R
        matrix3_t S(project(P, Cr, Cr) + R);
        // K = P*C'*(S^-1)
        bool singular;
        c_matrix<double, 6, 3> K(prod(project(P, range(0,6), Cr), invert(S, &singular)));
        if (singular) {
            ERRMSG("SINGULAR MATRIX S = " << S);
            ERRMSG("det(S) = " << det3(S));
            ERRMSG("P = " << P);
            ERRMSG("R = " << R);
            assert(!singular);
        }
        noalias(newState) = state + prod(K, (m - project(state, Cr)));
        // compute I6 - K*C. Since C = [I3, 0] then (I6 - K*C) = ([I3, 0; 0, I3] - [K1, 0; K2, 0])
        // = [I3 - K1, 0; -K2, I3]
        matrix6_t IlessKC;
        subrange(IlessKC, 0,3, 0,3) = identity_matrix<double>(3, 3) - subrange(K, 0,3, 0,3);
        subrange(IlessKC, 0,3, 3,6) = zero_matrix<double>(3, 3);
        subrange(IlessKC, 3,6, 0,3) = - subrange(K, 3,6, 0,3);
        subrange(IlessKC, 3,6, 3,6) = identity_matrix<double>(3, 3);
#else
        // slow, safe (I hope) version for debugging
        c_matrix<double, 3, 6> C;
        subrange(C, 0,3, 0,3) = identity_matrix<double>(3, 3);
        subrange(C, 0,3, 3,6) = zero_matrix<double>(3, 3);

        // S = C*P*C' + R
        matrix3_t S(prod(C, c_matrix<double, 6, 3>(prod(P, trans(C)))) + R);
        // K = P*C'*(S^-1)
        bool singular;
        c_matrix<double, 6, 3> K(prod(P, c_matrix<double, 6, 3>(prod(trans(C), invert(S, &singular)))));
        if (singular) {
            ERRMSG("SINGULAR MATRIX S = " << S);
            ERRMSG("det(S) = " << det3(S));
            ERRMSG("P = " << P);
            ERRMSG("R = " << R);
            assert(!singular);
        }
        noalias(newState) = state + prod(K, (m - project(state, Cr)));
        matrix6_t IlessKC(identity_matrix<double>(6, 6) - prod(K, C));
#endif

/*
        // FASTER, LESS NUMERICALLY-STABLE VERSION
        // P(t+1) = (I6 - K*C)*P, C = [I3, 0] so P(t+1) = ([I3, 0; 0, I3] - [K1, 0; K2, 0])*P
        // ==> P(t+1) = [I3 - K1, 0; -K2, I3]*P
        matrix6_t tmp2(prod(IlessKC, P));
*/
        // SLOWER, BUT NUMERICALLY-STABLE VERSION
        // P(t+1) = (I6 - K*C)*P*(I6 - K*C)' + K*R*K',
        matrix6_t tmp2(prod(IlessKC, matrix6_t(prod(P, trans(IlessKC))))
                       + prod(K, c_matrix<double, 3, 6>(prod(R, trans(K)))));

        // use a symmetric adaptor to ensure it's really symmetric
        //noalias(P) = symmetric_adaptor<matrix6_t, ublas::lower>(tmp2);
        noalias(P) = 0.5*(tmp2 + trans(tmp2));

#ifndef NDEBUG
        detP3 = det3(project(P, Cr, Cr));
        if (detP3 < 0) {
            ERRMSG("det(C*P*C') < 0 (=" << detP3 << ") !!!");
            assert(false);
        }
        assert_matrix_nan_inf(P);
#endif

        // copy back the data
        copy(P.data(), P.data() + 6*6, &tr->var[0][0]);
        copy(newState.data(), newState.data() + 6, tr->state);

        // copy the geometry
        tr->geometry = meas.geometry;

        // from the geometry, build the ellipse used for sizd checks
        tr->el = ellipse(tr->geometry);
        tr->el.finish();

        // converged?
        if (!(tr->m_dynFlags & TRACK_DYN_CONVERGED))
        {
            // set initial position when it first converges
            tr->m_initPos = subrange(newState, 0, 3);
            //if (tr->likelihood > m_confThres) {
            if (tr->likelihood > 0.95) { // MAGIC
                tr->m_dynFlags |= TRACK_DYN_CONVERGED;
                //MSG("Track " << tr->id() << " converged, pos = " << tr->m_initPos << "!");
            }
        }

        if (m_allStatic) {
            tr->isDynamic(false);
        } else {

            vector3_t vel = subrange(newState, 3, 6);
            if (inner_prod(vel, vel) > square(5)) { // MAGIC (5 meters/sec)
                tr->m_dynFlags |= TRACK_DYN_MOVING;
                //MSG("Track " << tr->id() << " is moving at " << norm_2(vel) << "m/s !");
            }

            // check displacement from initial position
            if (tr->m_dynFlags & TRACK_DYN_CONVERGED)
            {
                vector3_t displ = subrange(newState, 0, 3) - tr->m_initPos;
                if (inner_prod(displ, displ) > square(15)) { // MAGIC (15 meters)
                    tr->m_dynFlags |= TRACK_DYN_DISPLACED;
                    //MSG("Track " << tr->id() << " is displaced (" << norm_2(displ) << " meters, init pos=" << tr->m_initPos << ")!");
                }
            }

            // now, do the voting
            int votes = 0;

            // these flags are kept over time, never cleared
            if (tr->m_dynFlags & TRACK_DYN_MOVING)
                votes++;
            if (tr->m_dynFlags & TRACK_DYN_DISPLACED)
                votes++;

            float maxSize = max(tr->el.a, tr->el.b) * 3; // use 3sigma size
            if (maxSize > 6 || maxSize < 0.7) { // MAGIC
                votes--;
                if (maxSize > 10) // MAGIC
                    votes--; // huge, remove another vote
                tr->state[3] = tr->state[4] = tr->state[5] = 0.0; // velocity must be zero!
                //MSG("Track " << tr->id() << " is too big, max size = " << maxSize);
            } else {
                //MSG("Track " << tr->id() << " is NOT big! max size = " << maxSize);
            }

            if (!meas.isDynamic()) {
                votes--;
                tr->state[3] = tr->state[4] = tr->state[5] = 0.0; // avoid static obstacle floating around
                //MSG("Track " << tr->id() << " associated with static measure, setting vel to zero");
            }

            if (votes > 0)
                tr->isDynamic(true);
        }
    }

    void DisparityDetector::dbg_printAllTracks(const string& msg)
    {
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (!m_tracks[i].deleted()) {
                cerr << "Track " << m_tracks[i].id() << " (index " << i << ") " << msg << endl;
                cerr << "P(" << m_tracks[i].id() << ") = ";
                print_matrix(cerr, make_matrix<matrix6_t>((float*)m_tracks[i].var));
                cerr << "state(" << m_tracks[i].id() << ") = ";
                for (int j = 0; j < 6; j++) {
                    cerr << m_tracks[i].state[j];
                    if (j < 5) cerr << ", ";
                }
                cerr << endl;
            }
        }
    }

    // value of (2*pi)^(N/2), where N = 6 (for the multivariate gaussian pdf)
    static const double _2piPow3 = cube(2 * M_PI);

    /*!
     * Given the current regions in m_regions, bring them in local frame, associate
     * them with previously detected objects and track them over time, and create
     * new tracks for unassociated regions. This also merges tracks that seem to
     * converge to one another (probably they are the same obstacle).
     * \param sib The stereo blob the map was built from
     * \param elements The output MapElements
     */
    void DisparityDetector::updateTracks(StereoImageBlob* sib, vector<MapElement>* elements)
    {
        float cell2disp = float(m_maxDisp) / float(m_mapHeight);

        MapElement clearEl;
        clearEl.clear();
        clearEl.setTypeClear();

        // calculate transformation matrix from image to sensor in homogeneous
        // coordinates (col, row, disp, 1) -> (Xs, Ys, Zs, Ws)
        float sx = sib->leftCamera.sx;
        float sy = sib->leftCamera.sy;
        float cx = sib->leftCamera.cx;
        float cy = sib->leftCamera.cy;
        float b = sib->baseline;
        // (Xs, Ys, Zs, Ws)' = himg2sens * (col, row, disp, 1)'
        float sxOverSy = sx/sy;
        const float himg2sens[4][4] = {
            {b, 0,          0, -b*cx},
            {0, b*sxOverSy, 0, -b*cy*sxOverSy},
            {0, 0,          0,  b*sx},
            {0, 0,          1,  0}
        };

        float sens2loc[4][4];
        mat44f_mul(sens2loc, sib->veh2loc, sib->leftCamera.sens2veh);

        //elements->reserve(m_tracks.size());
        //elements->clear();
        elements->reserve(m_tracks.size() + elements->size());

        if (m_disableTracking)
        {
            // FIXME: this code (when tracking is disabled) is not well tested and
            // known to have some bugs ...

            unsigned int i;
            // dummy version that just copies each region into one MapElement
            m_tracks.resize(m_regions.size());
            for (i = 0; i < m_regions.size(); i++)
            {
                regionToMeasure(m_regions[i], &m_tracks[i], sib, himg2sens, sens2loc);
                m_tracks[i].newId();
            }

            // clear messages first, if any
            int clearId = trackIdPool.popClearID();
            while(clearId >= 0)
            {
                clearEl.setId(moduleId, clearId);
                elements->push_back(clearEl);
                clearId = trackIdPool.popClearID();
            }

            for (i = 0; i < m_regions.size(); i++) {
                elements->push_back(MapElement());
                trackToMapElement(m_tracks[i], &elements->back(), sib);
            }
            
            return;
        }

        double elapsed = double(sib->timestamp - m_lastTs) * 1e-6;
        m_lastTs = sib->timestamp;

        DBG(dbg_printAllTracks("before prediction"));
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (!m_tracks[i].deleted()) {
                m_tracks[i].updated(false);
#if USE_KALMAN_FILTER
                // predictions step
                predict(&m_tracks[i], elapsed);
                // purge tracks with a huge covariance
                double trace = m_tracks[i].var[0][0] + m_tracks[i].var[1][1] + m_tracks[i].var[2][2];
                if (trace > 36) // MAGIC
                {
                    //MSG("Deleted track " << m_tracks[i].id() << ", trace(Var) = " << trace);
                    m_tracks[i].deleted(true);
                }
#endif
            }
        }
        DBG(dbg_printAllTracks("after prediction"));
        
        // convert the regions from image frame to local frame in the "m_measures" vector
        //m_measures.resize(m_regions.size());
        m_measures.clear();
        m_measures.reserve(m_regions.size());
        vector<int> meas2reg;
        meas2reg.reserve(m_regions.size());
        for (unsigned int i = 0; i < m_regions.size(); i++)
        {
            //if (!m_regions[i].staticObs) { // comment me to have static obstacles tracked with kalman too
                m_measures.push_back(Measure());
                regionToMeasure(m_regions[i], &m_measures.back(),
                                sib, himg2sens, sens2loc);
                if (m_regions[i].staticObs) {
                    m_measures.back().isDynamic(false);
                } else {
                    m_measures.back().isDynamic(true);
                }
                meas2reg.push_back(i);
            //}
        }

        // FIXME: preallocate this memory
        vector<vector<float> > assoc(m_tracks.size());
        for (unsigned int i = 0; i < assoc.size(); i++) {
            assoc[i].resize(m_measures.size());
            fill(assoc[i].begin(), assoc[i].end(), -1.0);
        }

        double thres = m_assocThres;
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted())
                continue;
            for (unsigned int j = 0; j < m_measures.size(); j++)
            {
                //if (m_regions[j].filteredOut == true)
                //    continue;

                // TODO: filter the data with a kalman filter or something
                // TODO: take into account the shape (size at least) of the track/measure!
                // the variance of the measure conditioned by the prior is
                // S = Var[z|X] = (C*P*C' + R) where
                //   P = variance of the state (m_tracks[i].state)
                //   C = output transformation matrix, i.e. z = C*x (z=measure, x=state)
                //   R = measure error variance
#if USE_LINE_MATCH_ASSOC
                matrix2_t Q;
                for (int k = 0; k < 2; k++)
                    for (int l = 0; l < 2; l++)
                        Q(k,l) = m_tracks[i].var[k][l] + m_measures[j].var[k][l];

                vector2_t distVec; // this is not used either
                double dist;
                int num;
                num = matchLines(m_tracks[i].geometry, m_measures[j].geometry, invert(Q),
                                 m_assocThres, &dist, NULL, &distVec);
                double matchRatio = double(num)/min(m_tracks[i].geometry.size(),
                                                    m_measures[j].geometry.size());
                if (/*num < 5 ||*/ matchRatio < 0.5) // MAGIC NUMBERS
                    continue;
#else
                float cov[3][3];
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        cov[k][l] = m_tracks[i].var[k][l] + m_measures[j].var[k][l];
                // the distance is (z - x)*(S^-1)*(z - x)', so we need to invert the matrix (expensive ...)
                float inv[3][3];
                fast_inv_3x3(cov, inv);

                float distVec[3];
                for (int k = 0; k < 3; k++)
                    distVec[k] = fabs(m_measures[j].state[k] - m_tracks[i].state[k]);
                float dist = 0;
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        dist += distVec[k]*inv[k][l]*distVec[l];
                // cov is semipos definite, so is inv ==> dist must be >=0
#endif

                if (dist < 0)
                    ERRMSG("*** updateTracks(): dist < 0!!! numeric error???");

                // note: dist is the mahalanobis distance *squared*
                if (dist < thres*thres) {
                    assoc[i][j] = dist;
                    //MSG("updateTracks(): dist^2=" << dist << " < " << thres*thres
                    //    << ", associating track " << i << " with measure " << j);
                } else {
                    //MSG("updateTracks(): dist=" << dist << " >= " << thres
                    //    << ", NOT associating track " << i << " with measure " << j);
                }
            }
        }

        // keep track of the free slots available in m_tracks
        vector<int> freeSlots(0);
        freeSlots.reserve(m_tracks.size());

#if USE_CONVEX_HULL
        // FIXME: store this as a member so we can avoid allocating memory in the loop
        CvMat points = cvMat(0, 0, CV_32FC2);
#else
        point2arr tmpGeom1, tmpGeom2;
#endif

        DBG(dbg_printAllTracks("before measure update"));
        // update or delete tracks using the new measures
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted()) {
                freeSlots.push_back(i);
                continue;
            }

            bool staticMeas = false; // was at least one measure classified as static?
            bool allStatic = true; // were all the measures static?
            //int nAssoc = 0;
            int nPoints = 0; // number of points to use to calculate the new convex hull
            int best = -1;
            float bestDist = numeric_limits<float>::max();
            float sum[6] = { 0, 0, 0, 0, 0, 0 };
            float weightSum = 0;
            for (unsigned int j = 0; j < m_measures.size(); j++)
            {
                // TODO: use the variance of the filtered data (once we have that)
                // to calculate the new mean
                if (assoc[i][j] >= 0)
                {
                    // mark this measure as associated
                    m_measures[j].confirmed(true);
                    // use only non-static measures
                    if (m_measures[j].isDynamic()) {
                        allStatic = false;
                        float weight = exp(-0.5 * assoc[i][j]);
                        weightSum += weight;
                        for (int k = 0; k < 6; k++)
                            sum[k] += m_measures[j].state[k] * weight;
                        nPoints += m_measures[j].geometry.size();
                        if (bestDist > assoc[i][j] || best == -1)
                        {
                            bestDist = assoc[i][j];
                            best = j;
                        }
                    } else {
                        // don't use static measures, and if all of them are static,
                        // delete the track
                        staticMeas = true;
                        assoc[i][j] = -1;
                    }
                }
            }

            if (nPoints > 0)
            {
                // if the obstacle was picked up by the local map (measure marked as static),
                // completely remove this track ...
                if (allStatic) {
                    m_tracks[i].deleted(true);
                    continue;
                }

#if !USE_KALMAN_FILTER
                // calculate the new updated center
                // FIXME: JUST REPLACING THE STATE WITH THE NEW ONE (use a kalman filter instead!)
                for (int k = 0; k < 6; k++)
                    m_tracks[i].state[k] = sum[k] / weightSum;

                // as the measures are all close to each other, they'll all have the same variance
                // as this depends only on the position.
                // FIXME: the variance should be higher when multiple measures are associated with
                // one track!!! (i.e. stop doing hacks :P) 
                memcpy(m_tracks[i].var, m_measures[best].var, sizeof(m_tracks[i].var));
#else
                Measure merged;
                merged.isDynamic(!staticMeas);
                for (int k = 0; k < 6; k++)
                    merged.state[k] = sum[k] / weightSum;
                // what is the correct thing to do with the measure covariance?
                // just use the "best" one? sum all of them, take the average, ...?
                // HACK: just using the best association variance here
                memcpy(merged.var, m_measures[best].var, sizeof(merged.var));
#endif

#if !USE_CONVEX_HULL
                int lastJ = -1;
                Region tmpReg1;
                Region tmpReg2;
                Region* tmp1 = &tmpReg1;
                Region* tmp2 = &tmpReg2;
                float avgRow = 0; // this is the image row (y image coord)
                int count = 0;
                for (unsigned int j = 0; j < m_measures.size(); j++)
                {
                    if (assoc[i][j] >= 0)
                    {
                        int reg = meas2reg[j];
                        avgRow += m_regions[j].row;
                        count++;

                        if (tmp1->geometry.size() == 0) {
                            *tmp1 = m_regions[reg];
                        } else {
                            mergeRegionGeoms(*tmp1, m_regions[reg], tmp2);
                        }
                        swap(tmp1, tmp2); // latest result is always in tmp2

                        lastJ = j;
                    }
                }

                assert(count > 0);
#if USE_KALMAN_FILTER
                if (count == 1) {
                    // only one measure
                    merged.geometry = m_measures[lastJ].geometry;
                } else {
                    avgRow /= count;
                    // close the polygon
                    point2 p1(tmp2->rect.x+tmp2->rect.width, tmp2->rect.y * cell2disp);
                    point2 p2(tmp2->rect.x, tmp2->rect.y * cell2disp);
                    tmp2->geometry.push_back(p1);
                    tmp2->geometry.push_back(p2);

                    point2arr& mergedGeom = merged.geometry;
                    mergedGeom.resize(tmp2->geometry.size());
                    for (unsigned int j = 0; j < tmp2->geometry.size(); j++)
                    {
                        mergedGeom[j] = imageToLocal(sib, tmp2->geometry[j], avgRow);
                    }
                }
                // kalman filter, update with measures step
                updateTrackWith(&m_tracks[i], merged, elapsed);

#else
                if (count == 1) {
                    // nothing was merged!
                    m_tracks[i].geometry = m_measures[lastJ].geometry;
                } else {
                    avgRow /= count;
                    // close the polygon
                    point2 p1(tmp2->rect.x+tmp2->rect.width, tmp2->rect.y * cell2disp);
                    point2 p2(tmp2->rect.x, tmp2->rect.y * cell2disp);
                    tmp2->geometry.push_back(p1);
                    tmp2->geometry.push_back(p2);

                    point2arr& mergedGeom = m_tracks[i].geometry;
                    mergedGeom.resize(tmp2->geometry.size());
                    for (unsigned int j = 0; j < tmp2->geometry.size(); j++)
                    {
                        mergedGeom[j] = imageToLocal(sib, tmp2->geometry[j], avgRow);
                    }
                }

#endif

#else
                // copy all the points of the convex hulls of the regions in a CvMat structure

                // first allocate the memory if needed ...
                if (points.rows * points.cols < nPoints)
                {
                    if (points.data.ptr != NULL)
                        cvReleaseData(&points);
                    points = cvMat(nPoints, 1, CV_32FC2);
                    cvCreateData(&points);
                }
                
                // ... then copy all the points ...
                float* mat = (float*) points.data.ptr;
                for (unsigned int j = 0; j < m_measures.size(); j++)
                {
                    if (assoc[i][j] >= 0) {
                        point2arr& mHull = m_measures[j].geometry;
                        for (unsigned int k = 0; k < mHull.size(); k++)
                        {
                            *mat++ = mHull[k].x;
                            *mat++ = mHull[k].y;
                        }
                    }
                }

                int allocated = points.rows * points.cols; // save allocated size
                points.rows = nPoints;
                points.cols = 1;
                
				
                // ... allocate another CvMat structure for storing the result ...
                CvMat hull = cvMat(points.rows, points.cols, CV_32FC2);
                cvCreateData(&hull);
                // ... do the hard work ...
                cvConvexHull2(&points, &hull, CV_CLOCKWISE, 1 /* points, not indexes */);
                // ... and copy the result back to the Track struct ...
                mat = (float*) hull.data.ptr;
                unsigned int size = hull.rows * hull.cols;
                m_tracks[i].geometry.resize(size);
                for (unsigned int k = 0; k < size; k++)
                {
                    float x = *mat++;
                    float y = *mat++;
                    m_tracks[i].geometry[k] = point2(x, y, m_tracks[i].state[2]);
                }
                cvReleaseData(&hull);
                // restore allocated size value
                points.rows = allocated;
                points.cols = 1;
#endif

#if 0 && USE_KALMAN_FILTER
                // use the covariance to calculate the likelihood
                matrix6_t P;
                copy(&m_tracks[i].var[0][0], &m_tracks[i].var[5][6], P.data());
                m_tracks[i].likelihood = exp(-fabs(det(P) / 10)); // MAGIC
#else
                // simple exponential filter for likelihood, uncertainty halves
                // every frame the track was associated with some measure
                m_tracks[i].likelihood = 1.0 - (1.0 - m_tracks[i].likelihood)*0.5;
#endif
                if (m_tracks[i].maxLikelihood < m_tracks[i].likelihood)
                    m_tracks[i].maxLikelihood = m_tracks[i].likelihood;
                m_tracks[i].updated(true);
            }
            else
            {
#if 0 && USE_KALMAN_FILTER
                // use the covariance to calculate the likelihood
                matrix6_t P;
                copy(&m_tracks[i].var[0][0], &m_tracks[i].var[5][6], P.data());
                m_tracks[i].likelihood = exp(-fabs(det(P) / 10)); // MAGIC
#else
                // reduce likelihood for unassociated tracks, and remove them if it goes
                // below a threshold
                //m_tracks[i].likelihood *= 0.5; // MAGIC
                m_tracks[i].likelihood *= 0.75; // MAGIC
                m_tracks[i].updated(true); // the kalman filter may have updated it ... FIXME: remove me, this wasn't in the code used for the race
#endif

                if (m_tracks[i].likelihood <= 0.1) { // MAGIC
                    //MSG("Deleted track " << m_tracks[i].id() << ", likelihood = "
                    //    << m_tracks[i].likelihood << " <= 0.1");
                    m_tracks[i].deleted(true);
                    m_tracks[i].updated(true); // to send a clear msg
                    freeSlots.push_back(i);
                } else {
                    //MSG("Track " << m_tracks[i].id << " not associated, new likelihood = "
                    //    << m_tracks[i].likelihood);
                }
            }

        }
        DBG(dbg_printAllTracks("after measure update"));


        // iterator to keep track of the next track to remove, when needed
        conf_idx_vector_t::iterator rmIt;

        // create new tracks for non associated measures
        for (unsigned int j = 0; j < m_measures.size(); j++)
        {
            if (!m_measures[j].confirmed())
            {
                int i = -1;
                // reuse a deleted track, if possible, else add a new one
                if (freeSlots.size() > 0) {
                    i = freeSlots.back();
                    assert(m_tracks[i].deleted());
                    m_tracks[i] = m_measures[j]; // copying may be expensive!
                    m_tracks[i].isDynamic(false); // never at the beginning
                    freeSlots.pop_back();
                } else {
                    // remove the track with lowest confidence, if lower than the measure
                    if (m_toSort.size() == 0) {
                        // sort the tracks "on-demand", only when needed
                        m_toSort.resize(m_tracks.size()); // memory is preallocated with reserve()
                        for (unsigned int i = 0; i < m_tracks.size(); i++) {
                            m_toSort[i].first = m_tracks[i].likelihood;
                            m_toSort[i].second = i;
                        }
                        std::sort(m_toSort.begin(), m_toSort.end());
                        rmIt = m_toSort.begin();
                    }
                    // check if it's worth to remove an old track to make space for the new one
                    if (rmIt != m_toSort.end() && rmIt->first <= m_measures[j].likelihood)
                    {
                        i = rmIt->second;
                        ++rmIt;
                        //MSG("Replacing track " << i << " (id=" << m_tracks[i].id() << ", conf="
                        //    << m_tracks[i].likelihood << ") with measure " << j << ", conf="
                        //    << m_measures[j].likelihood);
                        m_tracks[i] = m_measures[j];
                    }
                }
            }
        }
        DBG(dbg_printAllTracks("after new tracks creation"));

        m_toSort.clear();

        MSG("Current number of tracks: " << m_tracks.size() << " - "
            << freeSlots.size() << " (deleted)");

        // merge tracks that seem to converge to the same state
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted())
                continue;
            for (unsigned int j = i + 1; j < m_tracks.size(); j++)
            {
                if (m_tracks[j].deleted())
                    continue;
#if USE_KALMAN_FILTER
                // same as original matching code, no scan matching, just use the state
                // but here, use the full state including velocity
                // TODO: if the track is classified as static, use line matching instead
                matrix6_t P;
                for (int k = 0; k < 6; k++)
                    for (int l = 0; l < 6; l++)
                        P(k,l) = m_tracks[i].var[k][l] + m_tracks[j].var[k][l];
                vector6_t state1, state2;
                copy(m_tracks[i].state, m_tracks[i].state + 6, state1.data());
                copy(m_tracks[j].state, m_tracks[j].state + 6, state2.data());
                vector6_t diff = state2 - state1;
                double dist = inner_prod(diff, vector6_t(prod(invert(P), diff)));

#elif USE_LINE_MATCH_ASSOC
                matrix2_t Q;
                for (int k = 0; k < 2; k++)
                    for (int l = 0; l < 2; l++)
                        Q(k,l) = m_tracks[i].var[k][l] + m_tracks[j].var[k][l];

                vector2_t distVec; // this is not used either
                double dist;
                int num;
                num = matchLines(m_tracks[i].geometry, m_tracks[j].geometry, invert(Q),
                                 m_assocThres, &dist, NULL, &distVec);
                double matchRatio = double(num)/min(m_tracks[i].geometry.size(),
                                                    m_tracks[j].geometry.size());
                if (num < 5 || matchRatio < 0.5) // MAGIC NUMBERS
                    continue;
#else

                float cov[3][3];
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        cov[k][l] = m_tracks[i].var[k][l] + m_tracks[j].var[k][l];
                // the distance is (z - x)*(S^-1)*(z - x)', so we need to invert the matrix (expensive ...)
                float inv[3][3];
                fast_inv_3x3(cov, inv); // yeah, "fast" meaning "not that slow" ...

                float distVec[3];
                for (int k = 0; k < 3; k++)
                    distVec[k] = fabs(m_tracks[j].state[k] - m_tracks[i].state[k]);
                float dist = 0;
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        dist += distVec[k]*inv[k][l]*distVec[l];
                // cov is semipos definite, so is inv ==> dist must be >=0
#endif
                if (dist < 0)
                    ERRMSG("*** updateTracks(): dist < 0!!! numeric error???");
                // note: dist is the mahalanobis distance *squared*
                if (dist < thres*thres) {
                    // tracks are very close, let's merge them
                    // Actually, just keep the one with the highest confidence!
                    int remIdx  = (m_tracks[i].likelihood < m_tracks[j].likelihood) ? i : j;
                    int keepIdx = (m_tracks[i].likelihood >= m_tracks[j].likelihood) ? i : j;
                    // remove track m_tracks[remIdx]
                    //MSG("Deleted track " << m_tracks[remIdx].id() << ", merged with "
                    //    << m_tracks[keepIdx].id() << ", dist^2 == " << dist
                    //    << " < " << thres*thres);
                    m_tracks[remIdx].deleted(true);
                    m_tracks[remIdx].updated(true);
                }
            }
        }
        DBG(dbg_printAllTracks("after track merging"));

        // send clear messages first (we may have a new track with the same id)
        int clearId = trackIdPool.popClearID();
        while(clearId >= 0)
        {
            clearEl.setId(moduleId, clearId);
            elements->push_back(clearEl);
            clearId = trackIdPool.popClearID();
        }

        // send new and updated tracks
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            bool sendPrediction = m_tracks[i].isDynamic() && m_tracks[i].confirmed()
                && (m_tracks[i].likelihood > m_confThres*square(0.75)); // magic

            // translation: track is not deleted, has been updated, has a high confidence
            // or had been sent previously (so we want to keep updating it with predictions)
            if (!m_tracks[i].deleted()
                && m_tracks[i].updated()
                && (m_tracks[i].likelihood > m_confThres || sendPrediction))
            {
                if (!m_onlyStatic || !m_tracks[i].isDynamic())
                {
                    m_tracks[i].confirmed(true);
                    // send it to the mapper
                    elements->push_back(MapElement());
                    trackToMapElement(m_tracks[i], &elements->back(), sib);
                } else if (m_tracks[i].confirmed()) {
                    // requested to send only static and this is dynamic, but was
                    // previously classified as static, and confirmed: send clear
                    clearEl.setId(moduleId, m_tracks[i].id());
                    elements->push_back(clearEl);
                }
            }
        }
    }

    void calcPointCov(float cov[4][4], const float pi[3],
                      const float himg2sens[4][4], const float sens2loc[4][4],
                      const float errorVar[4])
    {
        // Given the variance of col, row, disp (m_errorVar[1, 2, 3]), calculate
        // the variance in local frame using the linearized transformation.
        // jacobian of nonlinear transformation in the current point:
        // (xs, ys, zs, 1) = (Xs/Ws, Ys/Ws, Zs/Ws, 1) (note: Ws == disparity)
        point4 ps;
        typedef const float (*mat44_t)[4];
        mat44_t& K = himg2sens;
        // transform from image to sensor frame (do NOT use StereoImageBlobImageToSensor
        // because it doesn't necessarily use the same transformation as himg2sens, may differ
        // of a scale factor)
        ps[0] = K[0][0]*pi[0] + K[0][1]*pi[1] + K[0][2]*pi[2] + K[0][3];
        ps[1] = K[1][0]*pi[0] + K[1][1]*pi[1] + K[1][2]*pi[2] + K[1][3];
        ps[2] = K[2][0]*pi[0] + K[2][1]*pi[1] + K[2][2]*pi[2] + K[2][3];
        ps[3] = K[3][0]*pi[0] + K[3][1]*pi[1] + K[3][2]*pi[2] + K[3][3];
        float invW = 1.0/ps[3];
        // jacobian of g(x, y, z, w) = (x/w, y/w, z/w, 1)
        float J[4][4] = {
            {invW,   0,    0, ps[0]*invW*invW},
            {0,   invW,    0, ps[1]*invW*invW},
            {0,      0, invW, ps[2]*invW*invW},
            {0,      0,    0,          0}
        };
        float img2sens[4][4];
        mat44f_mul(img2sens, J, himg2sens);
        float img2loc[4][4];
        mat44f_mul(img2loc, sens2loc, img2sens);

        // covariance is cov = img2loc * diag(errorVar) * img2loc'
        // diag(...) * img2loc', optimized
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                cov[i][j] = errorVar[i] * img2loc[j][i];
        mat44f_mul(cov, img2loc, cov);
        assert_matrix_nan_inf(make_matrix<matrix4_t>((float*)cov));
    }

    void DisparityDetector::calcPointCov(float cov[4][4], const float pi[3],
                                         const float himg2sens[4][4], const float sens2loc[4][4])
    {
        const float errVar[4] = { m_errorVar[0], m_errorVar[1], m_errorVar[2], 0 };
        stereo_obs::calcPointCov(cov, pi, himg2sens, sens2loc, errVar);
    }


    // TrackID Management, all done within the Track and TrackIdPool classes, completely
    // transparent to the rest of the code, except that a Track cannot be copied.


    // global track ID pool
    TrackIdPool trackIdPool = TrackIdPool();

    TrackIdPool::TrackIdPool()
    {
        reset();
    }

    void TrackIdPool::reset()
    {
        deque<int>::iterator it;
        int id = 0;
        m_pool.resize(MAX_TRACK_ID + 1);
        m_clear.clear();
        for (it = m_pool.begin(); it != m_pool.end(); ++it)
            *it = id++;
        assert(id == MAX_TRACK_ID + 1);
    }

    /*! 
     * Remove an available track ID from the ID pool and return it. If there are no more IDs,
     * return -1.
     */
    int TrackIdPool::acquireId()
    {
        if (m_pool.empty())
            return -1;
        int id = m_pool.front();
        m_pool.pop_front();
        return id;
    }
    
    /*!
     * Return a track ID to the ID pool for a later reuse. The IDs will be reused only when no
     * new IDs are available anymore, and will be pulled out in FIFO order, unless the
     * wasConfirmed parameter is set to false, which means this ID was never sent to the mapper,
     * so we can (and should) reuse it as soon as possible.
     */
    void TrackIdPool::releaseId(int id, bool wasConfirmed)
    {
        if (m_pool.size() >= MAX_TRACK_ID + 1)
        {
            MSG("BUGBUGBUG: releasing more than " << MAX_TRACK_ID + 1 << " track ids!");
            assert(false);
            return;
        }

        if (wasConfirmed) {
            m_pool.push_back(id);
            m_clear.push_back(id);
        } else {
            m_pool.push_front(id);
        }
        assert(m_pool.size() <= MAX_TRACK_ID + 1);
        assert(m_clear.size() <= MAX_TRACK_ID + 1);
    }
    
    /*!
     * Return and removes from the queue the next ID to be cleared
     * (i.e., to be sent as a ELEMENT_CLEAR). If there are no more
     * IDs, return -1.
     */
    int TrackIdPool::popClearID()
    {
        if (m_clear.empty())
            return -1;
        int id = m_clear.front();
        m_clear.pop_front();
        return id;
    }

    /*!
     * Debugging function, prints the state of the pool to cerr
     */
    void TrackIdPool::showState()
    {
        cerr << "TrackIdPool state: m_pool.size() = " << m_pool.size() << endl;
        cerr << "                   m_clear.size() = " << m_clear.size() << endl;
        cerr << " Content of the queue:" << endl;
        for (deque<int>::iterator it = m_pool.begin(); it != m_pool.end(); ++it)
        {
            if (*it > MAX_TRACK_ID || *it < 0)
                cerr << "\e[7m" << *it << "\e[m "; // invert fg and bg
            else
                cerr << *it << ' ';
        }
        cerr << endl;
        cerr << " Content of the clear queue:" << endl;
        for (deque<int>::iterator it = m_clear.begin(); it != m_clear.end(); ++it)
            cerr << *it << ' ';
        cerr << endl;
        // check for duplicates
        std::set<int> check;
        bool ok = true;
        for (deque<int>::iterator it = m_pool.begin(); it != m_pool.end(); ++it)
        {
            pair<set<int>::iterator, bool> ret = check.insert(*it);
            if (ret.second == false) {
                cerr << "\e[7mDuplicate value " << *it << "\e[m" << endl;
                ok = false;
            }
        }
        if (ok)
            cerr << "No duplicate values found" << endl;
        cerr << "End TrackIdPool state" << endl;
    }


    Track::Track(const Measure& m)
    {
        *(Measure*)this = m;
        m_id = trackIdPool.acquireId();
        m_dynFlags = 0;
        m_initPos = make_vec3(0, 0, 0);
    }

    Track& Track::operator=(const Measure& m)
    {
        if (m_id != -1)
            trackIdPool.releaseId(m_id, confirmed());
        *(Measure*)this = m; // this will overwrite flags, confirmed() will be lost
        m_id = trackIdPool.acquireId();
        m_dynFlags = 0;
        m_initPos = make_vec3(0, 0, 0);
        return *this;
    }

    void Track::newId()
    {
        if (m_id != -1)
            trackIdPool.releaseId(m_id, confirmed());
        m_id = trackIdPool.acquireId();
        m_dynFlags = 0;
        m_initPos = make_vec3(0, 0, 0);
    }
    
    Track& Track::operator=(const Track& tr)
    {
        *(Measure*)this = (Measure&) tr;
        m_id = -1;
        m_dynFlags = tr.m_dynFlags;
        m_initPos = tr.m_initPos;
        return *this;
    }

}
