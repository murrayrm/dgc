#ifndef __IMAGECV_HH__
#define __IMAGECV_HH__

#include <cassert>
#include <stdint.h>
#include <cv.h>

class Fl_Image; // don't include FLTK if not needed

namespace blobstereo
{

    /**
     * C++ Wrapper around OpenCV image structure.
     */
    class ImageCvBase
    {
    protected:
        IplImage* img;
        int bytesPerPix;

        // copy operators not implemented, let's prevent anyone
        // doing any damage until they are
        ImageCvBase(const ImageCvBase& img);
        ImageCvBase& operator=(const ImageCvBase& img);

    public:
        // REMOVE ME: deprecated, use opencv formats
	typedef enum { FORMAT_RGB, FORMAT_GRAY8, FORMAT_GRAY16 } format_t;

        // REMOVE ME: deprecated, only for compatibility
	ImageCvBase(int w, int h, format_t fmt)
            : img(NULL), bytesPerPix(0)
	{
            init(w, h, fmt);
	}

	ImageCvBase(int w=0, int h=0, int depth=IPL_DEPTH_8U, int channels=1)
            : img(NULL), bytesPerPix(0)
        {
            init(w, h, depth, channels);
        }

        ImageCvBase(IplImage* im)
            : img(im)
        {
            bytesPerPix = depthToBpc(im->depth) * im->nChannels;
        }

        ~ImageCvBase() {
            cvReleaseImage(&img);
        }

        // REMOVE ME: deprecated, only for compatibility
        void init(int width, int height, format_t fmt)
        { 
            int depth = IPL_DEPTH_8U;
            int channels = 1;
            switch(fmt) {
            case FORMAT_RGB:
                channels = 3;
                depth = IPL_DEPTH_8U;
                break;
            case FORMAT_GRAY8:
                channels = 1;
                depth = IPL_DEPTH_8U;
                break;
            case FORMAT_GRAY16:
                channels = 1;
                depth = IPL_DEPTH_16U;
                break;
            };
            init(width, height, depth, channels);
        }

        void init(int width, int height, int depth, int channels)
        {
            if (img != NULL) {
                cvReleaseImage(&img);
            }
            //if (width * height > 0) {
            img = cvCreateImage(cvSize(width, height), depth, channels);
            //}
            bytesPerPix = depthToBpc(depth) * channels;
        }

        void init(IplImage* img)
        {
            if (this->img != NULL) {
                cvReleaseImage(&this->img);
            }
            this->img = img;
            bytesPerPix = depthToBpc(img->depth) * img->nChannels;            
        }

        /// @return The width of the image
        int getWidth() const { return img->width; }
        /// @return The height of the image
        int getHeight() const { return img->height; }
        /// @return The distance in bytes from a pixel in a row and
        /// the same pixel (same column) on the next row.
        /// Bigger or equal to getWidth()*getBytesPerPixel().
        int getBytesPerRow() const { return img->widthStep; }

        /// @return The number of bytes per pixel.
        int getBytesPerPixel() const {
            return bytesPerPix;
        }

        /// Direct access to the data.
        uint8_t* getRawData() const { return (uint8_t*) img->imageData; }

        /// Replaces the internal data pointer with the one specified,
        /// does NOT copy the data.
        void setRawData(uint8_t* data, int bytesPerRow = 0)
        {
            if (bytesPerRow == 0) {
                bytesPerRow = getWidth()*getBytesPerPixel();
            }
            cvSetData(img, data, bytesPerRow);
        }

        /// Get the underlying IplImage, to be used with opencv functions
        IplImage* getImage() { return img; }
        const IplImage* getImage() const { return img; }

        /// @return The pixel format in use.
        /// @deprecated use getChannels() and getDepth()
        //format_t getFormat() const { return m_format; }

        uint8_t* getRow(int row)
        {
            return (uint8_t*) img->imageData + row * img->widthStep;
        }

        uint8_t* operator[](int row) {
            return getRow(row);
        }
	
        /// returns the size of the texture (which must be a power of two)
        /// The actual data is not resized, and is in the upper left corner
        CvSize toGlTexture(int txtId, int glFormat = -1, bool mipmap = false,
                         bool setParams = true) const
        {
            return toGlTexture(img, txtId, glFormat, mipmap, setParams);
        }

        static CvSize toGlTexture(const IplImage* img, int txtId, int glFormat = -1, bool mipmap = false,
                                  bool setParams = true);

        static Fl_Image* toFlImage(const IplImage* img);

        Fl_Image* toFlImage()
        {
            return toFlImage(img);
        }

        /// Convert OpenCV depth into the number of bytes per channel.
        static int depthToBpc(int depth)
        {
            switch(depth) {
            case IPL_DEPTH_8U: // unsigned 8-bit integers
            case IPL_DEPTH_8S: // signed 8-bit integers
                return  1;
            case IPL_DEPTH_16U: // unsigned 16-bit integers
            case IPL_DEPTH_16S: // signed 16-bit integers
                return 2;
            case IPL_DEPTH_32S: // signed 32-bit integers
            case IPL_DEPTH_32F: // single precision floating-point numbers
                return 4;
            case IPL_DEPTH_64F: // double precision floating-point numbers
                return 8;
            default:
                return 1; // ???
            }
        }

    };

    /* Some useful helper for generic programming (templates), mapping C++ types
       to OpenCV depth constants. */

    // The generic definition
    template <class type> struct IplType {
        static int depth() { return 0; }; // unknown
    };
    // specializations for useful types
    template<> struct IplType<int8_t> {
        static int depth() { return IPL_DEPTH_8S; }
    };
    template<> struct IplType<uint8_t> {
        static int depth() { return IPL_DEPTH_8U; }
    };
    template<> struct IplType<int16_t> {
        static int depth() { return IPL_DEPTH_16S; }
    };
    template<> struct IplType<uint16_t> {
        static int depth() { return IPL_DEPTH_16U; }
    };
    template<> struct IplType<int32_t> {
        static int depth() { return IPL_DEPTH_32S; }
    };
    template<> struct IplType<uint32_t> {
        static int depth() { return IPL_DEPTH_32S; } /* there is no  IPL_DEPTH_32U */
    };
    template<> struct IplType<float> {
        static int depth() { return IPL_DEPTH_32F; }
    };
    template<> struct IplType<double> {
        static int depth() { return IPL_DEPTH_64F; }
    };


    /**
     * Type safe template version of ImageCv.
     */
    template <class type, int channels = 1>
    class ImageCv : public ImageCvBase
    {
    public:
        ImageCv(int width = 0, int height = 0)
            : ImageCvBase(width, height, IplType<type>::depth(), channels)
        { }

        ImageCv(IplImage* im)
            : ImageCvBase(im)
        {
            assert(img->depth == IplType<type>::depth());
            assert(img->nChannels == channels);
        }

        void init(int width, int height)
        {
            ImageCvBase::init(width, height, IplType<type>::depth(), channels);
        }

        void init(IplImage* img)
        {
            assert(img->depth == IplType<type>::depth());
            assert(img->nChannels == channels);
            ImageCvBase::init(img);
        }

        type* getData() const { return (type*) getRawData(); }

        void setData(type* data, int bytesPerRow = 0) {
            setRawData((uint8_t*) data, bytesPerRow);
        }

        type* getRow(int row)
        {
            return (type*)ImageCvBase::getRow(row);
        }

        type* getPixPtr(int row, int col) {
            return ImageCv::getRow(row) + channels * col;
        }

        /* Operator[] requires a little work to be really useful */

        // hopefully, an optimizing compiler (gcc -O3) will traslate
        // the call to img[row][col][channel] to a single call to getPixPtr().

        class GetPixWrapper {
            ImageCv& self;
            int row;
        public:
            GetPixWrapper(ImageCv& img, int r)
                : self(img), row(r)
            { }
            type* operator[](int col) {
                return self.getPixPtr(row, col);
            }
            const type* operator[](int col) const {
                return self.getPixPtr(row, col);
            }
        };

        GetPixWrapper operator[](int row) {
            return GetPixWrapper(*this, row);
        }
/* This won't work, have to find a better way
        const GetPixWrapper operator[](int row) const {
            return GetPixWrapper(this, row);
        }
*/
        
    };

}

#endif
