#ifndef __BLOBSTEREO_GLDISPLAY_HH__
#define __BLOBSTEREO_GLDISPLAY_HH__

#include <vector>
#include <cassert>
#include <boost/shared_ptr.hpp>
//#include <boost/ptr_container/ptr_vector.hpp>

#undef border // NCURSES is defining a border macro which is breaking fltk ...
// FLTK
#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/gl.h>

#include <bitmap/Bitmap.hh>
#include "SensnetBlob.hh"
//#include "ImageCv.hh"
#include "Image.hh"
#include "util.hh"
#include "IUpdatable.hh"
#include "IRenderable.hh"

namespace blobstereo {

    using namespace bitmap;

    class NotStereoBlobError : public Error {
        const StereoImageBlob& blob;
    public:
        NotStereoBlobError(std::string src, const StereoImageBlob& sib)
            : Error(src + ": Blob type " + toStr(sib.blobType)
                    + " is not SENSNET_STEREO_IMAGE_BLOB"),
              blob(sib)
        { }
        const StereoImageBlob& getBlob() const { return blob; };
    };

    using boost::shared_ptr;
    //using boost::ptr_vector;

    /**
     * Base abstract class for some kind of object display with opengl given
     * the information in a SensnetBlob.
     */
    class GlRenderer : virtual public IUpdatable, virtual public IRenderable
    {
    protected:
        bool dirty;
        
    public:

        GlRenderer() : dirty(false) { }

        virtual ~GlRenderer() { }

        // from IRenderable:
        //virtual void render() throw(Error) = 0;

        // from IUpdatable:
        //virtual void update(const SensnetBlob& blob) throw(Error)  = 0;

        /**
         * @return true if update was called, and the state changed, but it hasn't been
         * rendered yet, with a call to render().
         */
        bool isDirty() throw() { return dirty; };

    };

    /**
     * A renderer that just holds a list of other renderers, and forwards the
     * update() and render() calls to all of them, in the order they were registered.
     */
    class GlRenderList : virtual public GlRenderer, public MutexLockable
    {
        //ptr_vector<GlRenderer> renderers; // maybe ... not now
        typedef std::vector<shared_ptr<GlRenderer> > renderer_vector;
        renderer_vector renderers;

    public:

        /** Add a renderer to the list. Each time the update() or render()
         * method is called, this in turn calls the update() or render() method of
         * each registered renderer.
         */
        void addRenderer(shared_ptr<GlRenderer> rend)
        {
            assert(rend.get() != this); // don't try this, infinite recursion!!!
            renderers.push_back(rend);
        }	

        /**
         * Removes all the renderers, and sets the dirty flag to false.
         */
        void clear()
        {
            renderers.clear();
            dirty = false;
        }

        /// Calls update for each registered GlRenderer
        virtual void update(const SensnetBlob& blob) throw(Error)
        {
            renderer_vector::iterator it;
            for (it = renderers.begin(); it != renderers.end(); ++it)
            {
                (*it)->update(blob); // note: may throw
                if ((*it)->isDirty()) //  check if the object state changed
                    dirty = true;
            }
        }

        /// Calls render for each registered GlRenderer
        virtual void render() throw(Error)
        {
            renderer_vector::iterator it;
            for (it = renderers.begin(); it != renderers.end(); ++it)
            {
                (*it)->render(); // note: may throw
            }
            dirty = false;
        }

    };

    class RenderWidget : public Fl_Gl_Window, virtual public IUpdatable, public MutexLockable
    {
        shared_ptr<volatile GlRenderer> rend;

    public:
        RenderWidget(shared_ptr<volatile GlRenderer> r, int x, int y, int w, int h,
                     const char *label = NULL)
            : Fl_Gl_Window(x, y, w, h, label),
              rend(r)
        { }

        virtual void update(const SensnetBlob& blob) throw(Error);

        // overcome Fl_Window glitches
        virtual void show();

    protected:
        virtual void draw();

    };

    //////////////////////////
    // some real stuff here!
    /////////////////////////

    /**
     * Initialize FLTK and create a thread to run the FLTK main loop in it (Fl::run()).
     * Creates a nice UI showing all the specified widgets in a convenient (and not better
     * specified) way.
     * @param argc The unchanged value passed to main()
     * @param argv The unchanged value passed to main()
     * @param widgets The RenderWidgets used to show something, usually using OpenGL.
     * @param rendererMtx The mutex used to call any of the RenderWidgets methods.
     * NOT USED ACTUALLY ... FIXME, remove me or use me!
     */
    int startFltkThread(int argc, char** argv,
                        const vector<Fl_Widget*>& widgets,
                        pthread_mutex_t* rendererMtx);


    void stopFltkThread();

    /**
     * Initialize GLUT and start a thread to run the glut main loop in.
     * @param argc The unchanged value passed to main()
     * @param argv The unchanged value passed to main()
     * @param mainRenderer The main renderer, used to render all the objects on the scene
     * (usually a GlRenderList).
     * @param rendererMtx The mutex used to call any of the mainRenderer methods.
     * @param rendererCond The condition used to signal that the display should be redrawn.
     * NOT USED ACTUALLY ... FIXME, remove me or use me!
     */
/*
    int startGlutThread(int argc, char** argv,
                         GlRenderer* mainRenderer,
                         pthread_mutex_t* rendererMtx,
                         pthread_cond_t* rendererCond);
*/
    
    /** Stops the thread started by startGlutThread(), and wait for its
     * termination with pthread_join().
     */
/*
    void stopGlutThread();
*/

    /**
     * Show either the left or the right image.
     */
    class ImageRenderer : virtual public IRenderable, public MutexLockable
    {
    public:
        typedef enum { LEFT, RIGHT, DISPARITY } which_image_t;

    private:
        const BitmapBase *img;
        GLuint txtId; // texture
        bool changed; // need to rebuild texture

    public:

        ImageRenderer(const BitmapBase* myImg = NULL)
            : img(myImg), txtId(0)
        { }

        ImageRenderer(const BitmapBase& myImg)
            : img(&myImg), txtId(0)
        { }

        void setImage(const BitmapBase& myImg)
        {
            img = &myImg;
            changed = true;
        }

        void update() { changed = true; }

        /** Shows the image on the display.
         */
        void render() throw(Error);
    };

    /**
     * Show either the left or the right image.
     */
    class SensnetImageRenderer  : virtual public ImageRenderer, virtual public GlRenderer
    {
    public:
        typedef enum { LEFT, RIGHT, DISPARITY } which_image_t;

    private:
        typedef ImageRenderer base_t;

        BitmapBase img;
        which_image_t which;

    public:

        SensnetImageRenderer(which_image_t wh = LEFT)
            : ImageRenderer(img), which(wh)
        { }

        /**
         * If the blob is not a StereoBlob, or some error occurs, throws
         * a GlRenderer::Error exception.
         */
        void update(const SensnetBlob& blob) throw(Error);

        void render() throw(Error);
    };

    /**
     * Show the left and right images side by side.
     */
    class ImagePairRenderer : virtual public GlRenderer, public MutexLockable
    {
        Image img[2]; // img[0] = left, img[1] = right
        GLuint txtId[2];
        GLuint imgList[2];

    public:

        ImagePairRenderer();

        /**
         * If the blob is not a StereoBlob, or some error occurs, throws
         * a GlRenderer::Error exception.
         */
        void update(const SensnetBlob& blob) throw(Error);

        /** Shows the two images on the screen side by side (2D).
         */
        void render() throw(Error);

    };

} // end namespace

#endif
