// Inverse and Determinant procedures for boost::numeric::ublas.
// Inverse taken from
// http://www.crystalclearsoftware.com/cgi-bin/boost_wiki/wiki.pl?LU_Matrix_Inversion
// Excerpt from the website:
// "The following code inverts the matrix input using LU-decomposition with
//  backsubstitution of unit vectors. Reference: Numerical Recipies in C, 2nd ed.,
//  by Press, Teukolsky, Vetterling & Flannery."
// "Hope someone finds this useful. Regards, Fredrik Orderud."
// Determinant taken from ublas mailing list, http://lists.boost.org/MailArchives/ublas/2005/12/0916.php
//  by Thomas Lemaire
// Both procedures use lu_factorize as a primitive, and both have been optimized adding a specialized
// version for 2x2 and 3x3 matrices.
#ifndef INVERT_MATRIX_HPP
#define INVERT_MATRIX_HPP

#include <limits>
#include <algorithm>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace boost { namespace numeric { namespace ublas {

    template <class MAT>
    typename MAT::value_type det2(const MAT& m)
    {
        return m(0, 0)*m(1, 1) - m(0, 1)*m(1, 0);
    }

    template <class MAT>
    typename MAT::value_type det3(const MAT& m)
    {
        return (m(1, 1)*m(2, 2)-m(1, 2)*m(2, 1))*m(0, 0)+m(2, 0)*m(0, 1)*m(1, 2)-m(2, 0)*m(0, 2)*m(1, 1)-m(1, 0)*m(0, 1)*m(2, 2)+m(1, 0)*m(0, 2)*m(2, 1);
    }

    /* Invert a 2x2 matrix, optimized. If the determinant is known already, it calculation
     * can be avoided by passing it as the third argument.
     */
    template<class MAT>
    bool invert2 (const MAT& input, MAT& inverse, typename MAT::value_type det = 0)
    {
        typedef typename MAT::value_type T;
        assert(input.size1() == 2);
        assert(input.size2() == 2);
        if (det == 0)
            det = det2(input);
        if (fabs(det) < std::numeric_limits<T>::epsilon()*2)
            return false;
        T inv_det = T(1.0)/det;
        inverse(0, 0) =  input(1, 1)*inv_det;
        inverse(0, 1) = -input(0, 1)*inv_det;
        inverse(1, 0) = -input(1, 0)*inv_det;
        inverse(1, 1) =  input(0, 0)*inv_det;
        return true;
    }

    /* Invert a 3x3 matrix, optimized. If the determinant is known already, it calculation
     * can be avoided by passing it as the third argument.
     */
    template<class MAT>
    bool invert3 (const MAT& m, MAT& inv, typename MAT::value_type det = 0)
    {
        typedef typename MAT::value_type T;
        assert(m.size1() == 3);
        assert(m.size2() == 3);
        if (det == 0)
            det = det3(m);
        if (fabs(det) < std::numeric_limits<T>::epsilon()*2)
            return false;
        T inv_det = T(1.0)/det;
        inv(0, 0) =  (m(1, 1)*m(2, 2) - m(1, 2)*m(2, 1))*inv_det;
        inv(0, 1) = -(m(0, 1)*m(2, 2) - m(0, 2)*m(2, 1))*inv_det;
        inv(0, 2) =  (m(0, 1)*m(1, 2) - m(0, 2)*m(1, 1))*inv_det;
        inv(1, 0) = -(m(1, 0)*m(2, 2) - m(1, 2)*m(2, 0))*inv_det;
        inv(1, 1) =  (m(0, 0)*m(2, 2) - m(0, 2)*m(2, 0))*inv_det;
        inv(1, 2) = -(m(0, 0)*m(1, 2) - m(0, 2)*m(1, 0))*inv_det;
        inv(2, 0) =  (m(1, 0)*m(2, 1) - m(1, 1)*m(2, 0))*inv_det;
        inv(2, 1) = -(m(0, 0)*m(2, 1) - m(0, 1)*m(2, 0))*inv_det;
        inv(2, 2) =  (m(0, 0)*m(1, 1) - m(0, 1)*m(1, 0))*inv_det;

        return true;
    }

    /* Matrix inversion routine.
       Uses lu_factorize and lu_substitute in uBLAS to invert a matrix */
    template<class MAT>
    bool invert (const MAT& input, MAT& inverse)
    {
        typedef permutation_matrix<std::size_t> pmatrix;
        typedef typename MAT::value_type T;

        assert(input.size1() == input.size2());

        if (input.size1() == 2)
            return invert2(input, inverse);

        if (input.size1() == 3)
            return invert3(input, inverse);

        // create a working copy of the input
        MAT A(input);
        // create a permutation matrix for the LU-factorization
        pmatrix pm(A.size1());

        // perform LU-factorization
        int res = lu_factorize(A,pm);
        if( res != 0 ) return false;

        // create identity matrix of "inverse"
        inverse.assign(identity_matrix<T>(A.size1()));

        // backsubstitute to get the inverse
        lu_substitute(A, pm, inverse);

        return true;
    }

    /* Same as invert(input, inverse), but instead returns the result as the return value,
     * easier to use in complex expressions. If singular, it returns the nunll matrix,
     * ans sets the singular flat to true (if not null);
     */
    template<class MAT>
    MAT invert (const MAT& input, bool* singular = NULL)
    {
        typedef typename MAT::value_type T;
        MAT inv (input.size1(), input.size2());
        bool sing = !invert(input, inv);
        if (singular != NULL)
            *singular = sing;
        if (sing)
            inv.assign(zero_matrix<T>(input.size1(), input.size2()));
        return inv;
    }


    /** General matrix determinant. 
     *  It uses lu_factorize in uBLAS.
     *  (from the ublas mailing list, by Thomas Lemaire, modified).
     */
    template<class MAT>
    double det (const MAT& input)
    {
        typedef permutation_matrix<std::size_t> pmatrix;
        typedef typename MAT::value_type T;

        assert(input.size1() == input.size2());

        if (input.size1() == 2)
            return det2(input);

        if (input.size1() == 3)
            return det3(input);

        // create a working copy of the input
        MAT A(input);
        // create a permutation matrix for the LU-factorization
        pmatrix pivots(A.size1());

        // perform LU-factorization
        int res = lu_factorize(A, pivots);
        if( res != 0 ) return 0;

        double det = 1.0;
        for (std::size_t i=0; i < pivots.size(); ++i)
        {
            if (pivots(i) != i)
              det *= -1.0;
            det *= A(i,i);
        }
        return det;
    }

} } } //  close namespaces

#endif //INVERT_MATRIX_HPP
