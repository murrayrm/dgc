#ifndef __BLOBSTEREO_DETECTOR_HH__
#define __BLOBSTEREO_DETECTOR_HH__

#include <vector>
#include <cv.h>
#include "SensnetBlob.hh"
//#include "Obstacle.hh"
#include "ILockable.hh"

// avoid including FLTK stuff if not needed
class Fl_Widget;

namespace blobstereo {

    using std::vector;

    struct Track
    {
        int id; // unique id
        bool deleted; // deleted track, to be ignored
        bool updated; // track was updated or deleted in the last loop

        // state is in local frame
        float state[6]; // x, y, z, dx, dy, dz
        float var[6][6]; // state error covariance matrix

        float likelihood; // remove me, just use the density of probability at the mean
        float maxLikelihood;

        // bounding box containing the last seen blob(s) in disparity map frame
        CvRect rect;

        // best fitting ellipse (local frame), used to calculate approximate
        // distances between blobs and decide about associating and merging
        // TODO: replace me with a N-dimensional ellipsoid (N = number of states)
        ellipse el;

        // convex hull of the last region(s), in local frame, clockwise order
        point2arr convexHull;
        // height of the obstacle
        float height;
    };

    class Detector : virtual public MutexLockable
    {

    public:
        virtual ~Detector() { }
        
        /*!
         * Execute the detection algorithm given the current Stereo blob.
         * \return An array of Obstacle objects, representing the detected
         * obstacles (possibly filtered and tracked).
         */
        //virtual vector<Obstacle> detect(const SensnetBlob& blob) = 0;
        //virtual vector<Track> detect(const SensnetBlob& blob, vector<int>* deleted) = 0;
        virtual vector<Track> detect(const SensnetBlob& blob) = 0;

        /*!
         * If this detector has a state that is kept from one call to detect()
         * to the other, this method should reset the state.
         * The default implementation does nothing, as if there was no state.
         */
        virtual void reset() { }

        /*!
         * Returns a vector of FLTK widgets to be used for the debugging display.
         * These can be anything, useful to show what's going on with the
         * detector and even to change parameters on the fly.
         * When the display is off, this function won't be called at all.
         * The default implementation returns an empty vector.
         * \return The vector of widgets, or an empty vector if none
         */
        virtual vector<Fl_Widget*> getWidgets() { return vector<Fl_Widget*>(); }


        // Implement locking and unlocking using volatile methods
        // See http://www.ddj.com/dept/cpp/184403766

        /*! Thread safe (volatile) version of detect() */
        vector<Track> detect(const SensnetBlob& blob) volatile
        //vector<Track> detect(const SensnetBlob& blob, vector<int>* deleted) volatile
        {
            LockingPtr<Detector> self(this);
            return self->detect(blob/*, deleted*/);
        }

        /*! Thread safe (volatile) version of reset() */
        void reset() volatile
        {
            LockingPtr<Detector> self(this);
            self->reset();
        }

        /*! Thread safe (volatile) version of getWidgets() */
        vector<Fl_Widget*> getWidgets() volatile
        {
            LockingPtr<Detector> self(this);
            return self->getWidgets();
        }

    };

}

#endif
