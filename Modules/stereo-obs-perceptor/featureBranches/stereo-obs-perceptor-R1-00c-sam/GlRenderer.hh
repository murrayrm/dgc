#ifndef __BLOBSTEREO_GLDISPLAY_HH__
#define __BLOBSTEREO_GLDISPLAY_HH__

#include <vector>
#include <cassert>
#include <boost/shared_ptr.hpp>
//#include <boost/ptr_container/ptr_vector.hpp>

#include <GL/gl.h>

#include "SensnetBlob.hh"
#include "Image.hh"

namespace blobstereo {

    using boost::shared_ptr;
    //using boost::ptr_vector;

    /**
     * Interface (abstract class) for some kind of object display with opengl.
     */
    class GlRenderer
    {
    protected:
        bool dirty;
        
    public:

	/** Thrown when something goes wrong in some method.
	 */
	class Error : public std::exception {
            std::string msg;
	public:
	    Error(std::string m) : msg(m) { }
	    ~Error() throw() { }
	    virtual std::string getMsg() const throw() { return msg; }
	    virtual const char* what() const throw() { return msg.c_str(); }
	};
        GlRenderer() : dirty(false) { }

        virtual ~GlRenderer() {}

        /** 
         * Renders the object using the current OpenGL context.
         * The camera is always at the position (0, 0, 0), looking in the x direction,
         * like in the vehicle frame.
         */
        virtual void render() throw(Error) = 0;

        /**
         * Update the imformation to be rendered with the new blob.
         * This method must NOT call any OpenGL function.
         */
        virtual void update(const SensnetBlob& blob) throw(Error)  = 0;

        /**
         * @return true if update was called, and the state changed, but it hasn't been
         * rendered yet, with a call to render().
         */
        bool isDirty() throw() { return dirty; };
    };

    /**
     * A renderer that just holds a list of other renderers, and forwards the
     * update() and render() calls to all of them, in the order they were registered.
     */
    class GlRenderList : virtual public GlRenderer
    {
        //ptr_vector<GlRenderer> renderers; // maybe ... not now
        typedef std::vector<shared_ptr<GlRenderer> > renderer_vector;
        renderer_vector renderers;

    public:

        /** Add a renderer to the list. Each time the update() or render()
         * method is called, this in turn calls the update() or render() method of
         * each registered renderer.
         */
        void addRenderer(shared_ptr<GlRenderer> rend)
        {
            assert(rend.get() != this); // don't try this, infinite recursion!!!
            renderers.push_back(rend);
        }	

        /**
         * Removes all the renderers, and sets the dirty flag to false.
         */
        void clear()
        {
            renderers.clear();
            dirty = false;
        }

        /// Calls update for each registered GlRenderer
        void update(const SensnetBlob& blob) throw(Error)
        {
            renderer_vector::iterator it;
            for (it = renderers.begin(); it != renderers.end(); ++it)
            {
                (*it)->update(blob); // note: may throw
                if ((*it)->isDirty()) //  check if the object state changed
                    dirty = true;
            }
        }

        /// Calls render for each registered GlRenderer
        void render() throw(Error)
        {
            renderer_vector::iterator it;
            for (it = renderers.begin(); it != renderers.end(); ++it)
            {
                (*it)->render(); // note: may throw
            }
            dirty = false;
        }

    };

    //////////////////////////
    // some real stuff here!
    /////////////////////////

    /**
     * Initialize GLUT and start a thread to run the glut main loop in.
     * @param argc The unchanged value passed to main()
     * @param argv The unchanged value passed to main()
     * @param mainRenderer The main renderer, used to render all the objects on the scene
     * (usually a GlRenderList).
     * @param rendererMtx The mutex used to call any of the mainRenderer methods.
     * @param rendererCond The condition used to signal that the display should be redrawn.
     * NOT USED ACTUALLY ... FIXME, remove me or use me!
     */
    int startGlutThread(int argc, char** argv,
                         GlRenderer* mainRenderer,
                         pthread_mutex_t* rendererMtx,
                         pthread_cond_t* rendererCond);
    
    /** Stops the thread started by startGlutThread(), and wait for its
     * termination with pthread_join().
     */
    void stopGlutThread();

    /**
     * Show the left and right images.
     */
    class ImagePairRenderer : virtual public GlRenderer
    {
        Image img[2]; // img[0] = left, img[1] = right
        GLuint txtId[2];
        GLuint imgList[2];

    public:

        ImagePairRenderer();

        /**
         * If the blob is not a StereoBlob, or some error occurs, throws
         * a GlRenderer::Error exception.
         */
        void update(const SensnetBlob& blob) throw(Error);

        /** Shows the two images on the screen side by side (2D).
         */
        void render() throw(Error);

    };

} // end namespace

#endif
