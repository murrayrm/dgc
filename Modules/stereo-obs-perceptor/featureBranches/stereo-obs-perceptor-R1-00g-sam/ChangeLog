Fri Jun 15 16:45:41 2007	datamino (datamino)

	* version R1-00g
	BUGS:  
	New files: ILockable.hh IRenderable.hh IUpdatable.hh
	FILES: BlobStereo.cc(25747), blobstereo_cmdline.c(25747),
		blobstereo_cmdline.ggo(25747), blobstereo_cmdline.h(25747)
	Improved the description of --sensor-id and --module-id options,
	added the module id to the console display.

	FILES: BlobStereo.cc(26129), Detector.hh(26129),
		DisparityDetector.cc(26129), DisparityDetector.hh(26129),
		GlRenderer.cc(26129), GlRenderer.hh(26129),
		ImageCv.cc(26129), ImageCv.hh(26129), Obstacle.hh(26129),
		blobstereo_cmdline.c(26129), blobstereo_cmdline.ggo(26129),
		blobstereo_cmdline.h(26129), util.hh(26129)
	- OpenGL display now off by default (can be turned on on the fly
	from the console display). - Created two interfaces, IUpdatable and
	IRenderable and made GlRenderer derive from these.   This was
	needed to be able to have OpenGL widgets that don't need to be
	updated with a	 SensnetBlob. - New display, MinRow, showing the
	minimum row for each map cell (trying to   detect overhanging trees
	and such). - Calculated uncertainty information for each obstacle
	detected. Uncertainty in image frame   (col, row, disp) is assumed
	to be constant and set to some arbitrary value. This is   then
	transformed to local frame, whic yields bigger uncertainty for
	distant obstacles. NOTE: not sure about how point2_uncertain stores
	uncertainty, i.e. the exact meaning of min_var, max_var and axis,
	and how to get those from a covariance matrix. NOTE2: This code
	relies on a modified version of mat44.h, which is not released yet.

	FILES: BlobStereo.cc(27521)
	Implemented process control (ProcessRequest and ProcessResponse).

	FILES: BlobStereo.cc(27847), Detector.hh(27847),
		DisparityDetector.cc(27847), DisparityDetector.hh(27847),
		GlRenderer.cc(27847), GlRenderer.hh(27847), util.hh(27847)
	- Implemented removal of small obstacles (threshold is hardcoded,
	need to make it, and all the parameters of the detector, a command
	line or config file option). - Fixed bug in the detector display,
	it was drawn before the detector was updated, resulting in showing
	the previous frame instead of the current one. - Trying to use
	volatile objects and methods to implement critical sections. Seems
	to work, but since not everybody knows how it works, it makes the
	code less readable. Maybe I'll revert back.

	FILES: BlobStereo.cc(27855), DisparityDetector.cc(27855),
		DisparityDetector.hh(27855)
	Separated display of obstacles in local frame from map display

	FILES: BlobStereo.cc(27988), DisparityDetector.cc(27988),
		DisparityDetector.hh(27988), Obstacle.hh(27988)
	- First implementation of a (simple) data association and tracking
	filter. Not working, Needs a lot of tuning and debugging. - Changed
	the way the variance is passed to the mapper. A lot of tuning and
	debugging needed here, too. - Some random cleanups (old code
	removal)

	FILES: BlobStereo.cc(28004)
	Added printing of covariance matrices (commented out for the
	release).

	FILES: util.hh(25581)
	Fixed error message formatting

	FILES: util.hh(28003)
	Added a function to print a 4x4 matrix

Fri May 25 12:07:09 2007	datamino (datamino)

	* version R1-00f
	BUGS:  
	FILES: BlobStereo.cc(23781), DisparityDetector.cc(23781),
		DisparityDetector.hh(23781), GlRenderer.cc(23781)
	Merge with previous branch

	FILES: BlobStereo.cc(24210), DisparityDetector.cc(24210),
		DisparityDetector.hh(24210), GlRenderer.cc(24210),
		GlRenderer.hh(24210), Makefile.yam(24210),
		Obstacle.hh(24210)
	Moving from glut to fltk for a more flexible UI. Working but not
	fully functional.

	FILES: BlobStereo.cc(24351), Detector.hh(24351),
		DisparityDetector.cc(24351), DisparityDetector.hh(24351),
		GlRenderer.cc(24351), GlRenderer.hh(24351),
		blobstereo_cmdline.c(24351), blobstereo_cmdline.ggo(24351),
		blobstereo_cmdline.h(24351)
	Split opengl display in multiple rendering objects, and draw them
	in separated tabs using fltk. In progress ...

	FILES: BlobStereo.cc(24391), DisparityDetector.cc(24391),
		DisparityDetector.hh(24391)
	Finished adapting the display to the new tabbed window using fltk,
	needs some cleanup though. Now it's modular, functional, but I
	don't like it :-P need to think of a better UI design ... good
	thing that it's modular, should be easy now.

	FILES: BlobStereo.cc(24729), GlRenderer.cc(24729),
		GlRenderer.hh(24729), SensnetBlob.hh(24729)
	display code cleanup

	FILES: GlRenderer.cc(24212), GlRenderer.hh(24212)
	Mostly working fltk/opengl display with tabs.

	FILES: GlRenderer.cc(24727)
	Better and more flexible OpenGL display using FLTK. Now you can
	select which views you want to see at the same time, and have them
	tiled side by side.

	FILES: GlRenderer.cc(24931)
	Fixed a deadlock, and possible race conditions, Fl::lock() must be
	locked before calling Fl::run(), and unlocked afterwards (otherwise
	Fl::wait will unlock an unlocked mutex).

	FILES: Makefile.yam(24211)
	Fixed usage of fltk-config in Makefile.yam.

Sun Apr 29  0:18:54 2007	datamino (datamino)

	* version R1-00d
	BUGS:  
	FILES: BlobStereo.cc(21384), DisparityDetector.cc(21384),
		blobstereo_cmdline.c(21384), blobstereo_cmdline.ggo(21384),
		blobstereo_cmdline.h(21384)
	- Added detected obstacles to the console display.
	- Removed output log commandline options and cotk button (no log
	  writing was implemented anyway).

	FILES: DisparityDetector.cc(21158), DisparityDetector.hh(21158)
	Cleaning up the code. Changed some variable and function names,
	removed some old code.

	FILES: DisparityDetector.cc(21432)
	small tweak to the gl display for the midrange camera.

Thu Apr 26 19:28:54 2007	datamino (datamino)

	* version R1-00c
	BUGS:  
	FILES: BlobStereo.cc(20975), DisparityDetector.cc(20975),
		DisparityDetector.hh(20975), Makefile.yam(20975),
		Obstacle.hh(20975)
	Finally sending obstacles to the mapper! Showing the number of
	obstacles sent in the cotk display. Fixed a bug in
	DisparityDetector, forgot to convert from cell coords back to
	disparity.

	FILES: Makefile.yam(20980)
	Computers in the lab don't support SSE3 instructions, changed
	optimizing flag from -msse3 to -msse2.

Thu Apr 26  2:08:48 2007	datamino (datamino)

	* version R1-00b
	BUGS:  
	New files: DisparityDetector.cc DisparityDetector.hh Image.hh
		ImageCv.cc ImageCv.hh glutil.hh
	Deleted files: GlDisplay.hh
	FILES: BlobStereo.cc(20149), Detector.hh(20149),
		GlRenderer.cc(20149), GlRenderer.hh(20149),
		Makefile.yam(20149), Obstacle.hh(20149)
	Implementing a simple detector, translating some matlab code to
	C++. Added two images to the debug display, showing disparity and a
	disparity accumulator with coords (cols, disparity).

	FILES: BlobStereo.cc(20504), GlRenderer.hh(20504),
		Makefile.yam(20504)
	Starting to use OpenCv to detect blobs, still incomplete (still not
	outputting any object). Added a lot of stuff in the debug display
	(disparity, detected blobs).

	FILES: Makefile.yam(20794)
	Changed the blob detection algorithm, now using two thresholds as I
	was doing in the matlab code. Added a lot of debugging code and
	debug visualizations. At the moment, all the visualizations are
	enabled and disabled with #if 0|1 ... #endif directly in the code.
	Still not tracking obstacles, and not sending anything to the
	mapper.

Wed Apr 18 20:53:09 2007	datamino (datamino)

	* version R1-00a
	BUGS:  
	New files: BlobStereo.cc Detector.hh GlDisplay.hh GlRenderer.cc
		GlRenderer.hh Obstacle.hh SensnetBlob.hh SensnetLog.hh
		blobstereo_cmdline.c blobstereo_cmdline.ggo
		blobstereo_cmdline.h util.hh
	FILES: Makefile.yam(19936)
	First version of the stereo obstacle detector ("blobstereo"). This
	is just a shell, no detection is performed.

Wed Apr 18 18:35:29 2007	datamino (datamino)

	* version R1-00
	Created stereo-obs-perceptor module.






