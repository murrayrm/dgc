#ifndef __BLOBLADAR_OBSTACLE_HH__
#define __BLOBLADAR_OBSTACLE_HH__

#include <vector>

#include <interfaces/VehicleState.h>
#include <frames/point2_uncertain.hh>

namespace blobstereo {

    using std::vector;

    class Obstacle
    {
        vector<point2_uncertain> vertices; // meters
        
        point2_uncertain speed; // m/s

        VehicleState state;

    public:

        Obstacle()
            : speed(0, 0)
        {
            memset(&state, 0, sizeof(state));
        }

        /* Getters and Setters */

        vector<point2_uncertain>& getVertices()
        {
            return vertices;
        }

        const vector<point2_uncertain>& getVertices() const
        {
            return vertices;
        }

        point2_uncertain& getSpeed()
        {
            return speed;
        }

        const point2_uncertain& getSpeed() const
        {
            return speed;
        }

        VehicleState& getState()
        {
            return state;
        }

        const VehicleState& getState() const
        {
            return state;
        }

        // ... anything else?
    };

} // end namespace

#endif
