#ifndef __BLOBSTEREO_DETECTOR_HH__
#define __BLOBSTEREO_DETECTOR_HH__

#include <vector>
#include "SensnetBlob.hh"
#include "Obstacle.hh"

namespace blobstereo {

    using std::vector;

    class Detector
    {
    public:
        virtual ~Detector();
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob);

        // does this class need any state?
        // just in case, this will resets everything ;-)
        virtual void reset() { }

    };

}

#endif
