#include <iostream>
#include <string>
#include <memory>
#include <cerrno>

// a little boost utility for vectors of owned pointers
//#include <boost/ptr_container/ptr_vector.hpp> // maybe later
#include <boost/shared_ptr.hpp>

// unix headers
#include <signal.h>
#include <sys/stat.h>

// DGC headers
#include <cotk/cotk.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/sn_types.h>
#include <interfaces/StereoImageBlob.h>
#include <map/MapElementTalker.hh>

// local headers
#include "util.hh"
#include "blobstereo_cmdline.h"
#include "Detector.hh"
//#include "Tracker.hh"
#include "GlRenderer.hh"
#include "SensnetLog.hh"

namespace blobstereo {

    const sensnet_id_t sensorIds[] = {
        /// Left-front bumper stereo 
        SENSNET_LF_SHORT_STEREO,

        /// Right-front short range stereo
        SENSNET_RF_SHORT_STEREO,

        /// Middle-front medium range stereo
        SENSNET_MF_MEDIUM_STEREO,

        /// Middle-front long range stereo
        SENSNET_MF_LONG_STEREO
    };

    const int numSensorIds = sizeof(sensorIds)/sizeof(sensorIds[0]);

    static const char* consoleTemplate =
    "Stereo Blob Detector\n"
    "Number of blobs received: %numrecv%\n"
    "Number of blobs sent: %numsent%\n"
    "\n"
    "Current stereo blob:\n"
    "    tstamp: %curtsRel%                  %curts%\n"
    "    id: %blobid%\n"
    "    sensor id: %sensorid%\n"
    "    type: %blobtype%\n"
    "    length: %bloblen% bytes\n"
    "\n"
    "Output log: %outlog%\n"
  //"      Size: %outsize%\n" // TBD
    "\n"
    "%QUIT%|%LOG%\n"
    "%stderr%\n"
    "%stderr%\n"
    "%stderr%\n"
    "%stderr%\n"
    "%stderr%\n";

    class BlobStereo
    {
    public:

        /** Thrown when something goes wrong in BlobStereo.
         */
        class Error : public std::exception {
            std::string msg;
        public:
            Error(std::string m) : msg(m) { }
            ~Error() throw() { }
            virtual std::string getMsg() const throw() { return msg; }
            virtual const char* what() const throw() { return msg.c_str(); }
        };

        BlobStereo(int argc, char** argv) throw(Error)
            : opengl(true), startTStamp(0), numBlobs(0)
        {
            this->argc = argc;
            this->argv = argv;
            if (cmdline_parser (argc, argv, &options) != 0)
            {
                exit(1);
            }
        
            // cmdline parsing ...
            // Fill out the spread name
            if (this->options.spread_daemon_given)
                this->spreadDaemon = this->options.spread_daemon_arg;
            else if (getenv("SPREAD_DAEMON"))
                this->spreadDaemon = getenv("SPREAD_DAEMON");
            else
                throw ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
            // Fill out the skynet key
            if (this->options.skynet_key_given)
                this->skynetKey = this->options.skynet_key_arg;
            else if (getenv("SKYNET_KEY"))
                this->skynetKey = atoi(getenv("SKYNET_KEY"));
            else
                this->skynetKey = 0;
  
            // Fill out module id
            this->moduleId = modulenamefromString(this->options.module_id_arg);
            if (this->moduleId <= 0)
                throw ERROR("invalid module id: " + string(this->options.module_id_arg));

            opengl = options.opengl_flag;
            outputLog = options.write_log_flag;

            initSensnet();
        }

        ~BlobStereo()
        {
            if (useConsole()) {
                finiConsole();
            }

            if (outputLog) {
                stopOutputLog();
            }

            if (opengl) {
                finiOpenGL();
            }

            finiSensnet();
        }


        // ---------------------------
        // SENSNET
        // ---------------------------

        // Initialize sensnet
        void initSensnet() throw(Error)
        {
            int i;
    
            // Create sensnet itnerface
            this->sensnet = sensnet_alloc();
            assert(this->sensnet);
            if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
                throw ERROR("sensnet_connect() failed");

#if 0
            // If replaying log files, now is the time to open them
            if (this->mode == modeReplay)
                if (sensnet_open_replay(this->sensnet, this->options.inputs_num, this->options.inputs) != 0)
                    throw ERROR("unable to open log");
#endif

            detectors.resize(numSensorIds);
            // join stereo groups
            for (i = 0; i < numSensorIds; i++)
            {
                detectors[i] = NULL; // TODO: create some detector here
                // Join the stereo data group
                if (sensnet_join(this->sensnet, sensorIds[i],
                                 SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
                    throw ERROR("unable to join " + toStr(sensorIds[i]));
            }

            mapTalker.initSendMapElement(skynetKey);
        }

        void finiSensnet()
        {
            if (this->sensnet)
            {
                int i;
                for (i = 0; i < numSensorIds; i++)
                {
                    sensnet_leave(this->sensnet, sensorIds[i], SENSNET_STEREO_IMAGE_BLOB);
                }
                sensnet_free(this->sensnet);
                this->sensnet = NULL;
            }
        }

        // ---------------------------
        // USER INTERFACE (cotk)
        // ---------------------------

        // Is cotk TUI (textual user interface ;-) enabled?
        bool useConsole() { return !options.disable_console_flag; }

        void initConsole()
        {
            if (console == NULL) {
                console = cotk_alloc();
            }
            cotk_bind_template(console, consoleTemplate);
        
            // bind buttons
            cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq", cotkMemberCallback,
                             new CotkMemberCallbackData(this, &BlobStereo::onQuit));
            cotk_bind_toggle(console, "%LOG%", " LOG ", "Ll", cotkMemberCallback,
                             new CotkMemberCallbackData(this, &BlobStereo::toggleLog));
            cotk_toggle_set(console, "%LOG%", options.write_log_flag);

            /* At the moment, starting and stopping the opengl display doesn't work (thanks to glut ...)
            cotk_bind_toggle(console, "%OPENGL%", " OPENGL ", "Ll", cotkMemberCallback,
                             new CotkMemberCallbackData(this, &BlobStereo::toggleOpenGL));
            cotk_toggle_set(console, "%OPENGL%", options.opengl_flag);
            */

            cotk_open(console, NULL); // no message logging at the moment
        }

        void finiConsole()
        {
            if (console != NULL) {
                cotk_close(console);
                cotk_free(console);
            }
        }

        void updateDisplay()
        {
            if (useConsole())
                cotk_update(console);
        }

        void displayStatus()
        {
            if (useConsole()) {
                // display the status on cotk console
                cotk_printf(console, "%numrecv%", A_NORMAL, "%-10d", numBlobs);
                cotk_printf(console, "%curtsRel%", A_NORMAL, "%-8g sec (relative)",
                            (blob.getTimestamp() - startTStamp)/1e6);
                cotk_printf(console, "%curts%", A_NORMAL, "%-12.0f usec (since the epoch)",
                            static_cast<double>(blob.getTimestamp()));
                cotk_printf(console, "%blobid%", A_NORMAL, "%d", blob.getId());
                cotk_printf(console, "%sensorid%", A_NORMAL, "%d", blob.getSensorId());
                cotk_printf(console, "%blobtype%", A_NORMAL, "%d", blob.getType());
                cotk_printf(console, "%bloblen%", A_NORMAL, "%ld", blob.getDataLen());

                cotk_update(console);
            } else {
                // display the status on normal console (cout)
            }
        } 

        int onQuit(cotk_t* /*cotk*/, const char* /*token*/)
        {
            quit ++;
            return 0;
        }

        // --------------------------------
        // SENSNET (SKYNET?) OUTPUT LOGGING
        // --------------------------------
        // FIXME: this code is copy-pasted from sensnetreplay, but I'm not sure if
        // it cam be used here too

        int startOutputLog()
        {
            bool ok = true;
	
            // check if log_dir exists, and if not, create it
            struct stat st;
            int ret = stat(options.log_path_arg, &st);
            if (ret == 0) {
                // log_path exists, check if it's a directory
                if (!S_ISDIR(st.st_mode))
                {
                    ERRMSG('\'' << options.log_path_arg << "' is not a directory, cannot create logs!!!");
                    ok = false;
                }
            } else if (errno == ENOENT) {
                // log_path doesn't exist, create it
                if(mkdir(options.log_path_arg, 0755) != 0)
                {
                    ERRMSG("cannot create dir. '" << options.log_path_arg
                           << "': " << strerror(errno) << "!!!");
                    ok = false;
                }
            } else {
                ERRMSG("cannot stat '" << options.log_path_arg
                       << "': " << strerror(errno) << "!!!");
                ok = false;
            }

            if (ok)
            {
                // open a new log file each time (is this the best way? you can
                // merge them afterwards if you want)
                ostringstream oss;
                char timestr[64];
                time_t t = time(NULL);
                strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t));
                oss << options.log_path_arg << "/" << timestr << "-" << options.log_name_arg;
                string name = oss.str();
                string suffix = "";
	    
                // if it exists already, append .1, .2, .3 ... 
                for (int i = 1; stat((name + suffix).c_str(), &st) == 0; i++) {
                    ostringstream tmp;
                    tmp << '.' << i;
                    suffix = tmp.str();
                }
                name += suffix;
	    
                if (!options.disable_console_flag) {
                    cotk_printf(console, "%outlog%", A_NORMAL, "%-70s", name.c_str());
                } else {
                    MSG("Start recording on '" << name << '\'');
                }
                sensnet_log_header_t header;
                header.timestamp = gettime();
                writer.openWrite(name, header);
                outputLog = true;
                return 0;
            } else {
                cotk_toggle_set(console, "%LOG%", false);
                return -1;
            }
        }

        int stopOutputLog()
        {
            // disable logging
            writer.close();
            outputLog = false;
            MSG("Stopped recording");
            return 0;
        }

        int toggleLog(cotk_t* /*cotk*/, const char* /*token*/)
        {
            if (cotk_toggle_get(console, "%LOG%"))
            {  
                return startOutputLog();
            } else {
                return stopOutputLog();
            }
        }

        // ---------------------------
        // OPENGL DISPLAY
        // ---------------------------

        int initOpenGL(int argc, char** argv)
        {
            renderList.addRenderer(shared_ptr<GlRenderer>(new ImagePairRenderer));
            // add more here!
        
            return startGlutThread(argc, argv, &renderList, &renderMtx, NULL/*not used*/);
        }

        void finiOpenGL()
        {
            stopGlutThread();
            renderList.clear();
        }

        int toggleOpenGL(cotk_t* /*cotk*/, const char* /*token*/)
        {
            if (cotk_toggle_get(console, "%OPENGL%"))
            {  
                return initOpenGL(argc, argv);
            } else {
                finiOpenGL();
                return 0;
            }
        }

        // ---------------------------
        // DETECTION AND TRACKING
        // ---------------------------

        void processBlob(const SensnetBlob& blob /* pseudo-code */)
        {
            numBlobs++;
            if (startTStamp == 0) {
                startTStamp = blob.getTimestamp();
            }
            if (opengl) {
                pthread_mutex_lock(&renderMtx);
                renderList.update(blob);
                pthread_mutex_unlock(&renderMtx);
            }

            /*
            // PSEUDO-CODE, TODO: implement me!!!
            vector<Obstacle> objs = detector.detect(blob);
            tracker.update(objs);
            */
        }

        /*! Update the state in time (tracking), without processing any new
         * actual measure.
         */
        void updateState()
        {
            /*
            // PSEUDO-CODE, TODO: implement me!!!
            tracker.update();
            */
        }
    
        // ---------------------------
        // MAIN LOOP
        // ---------------------------

        static void sigintHandler(int sig);

        int run()
        {
            int lastBlobID = -1;
            // catch CTRL-C and exit cleanly, if possible
            signal(SIGINT, BlobStereo::sigintHandler);
            // and SIGTERM, i.e. when someone types "kill <pid>" or the like
            signal(SIGTERM, BlobStereo::sigintHandler);

            if (useConsole()) {
                initConsole();
            }

            if (opengl) {
                initOpenGL(argc, argv);
            }

            if (outputLog) {
                startOutputLog();
            }

            // main loop
            while (!quit) {

                displayStatus(); // this handles keypresses too
                if (quit)
                    break;

                bool noNewBlobs = true;
                for (int i = 0; i < numSensorIds; i++) {
                    blob.setSensorId(sensorIds[i]);
                    blob.setType(SENSNET_STEREO_IMAGE_BLOB);
                    bool newBlob = blob.read(sensnet);
                    if (newBlob) {
                        // There was a new (unread) blob
                        processBlob(blob);
                        noNewBlobs = false;
                    }
                }

                if (noNewBlobs)
                {
                    // no new blobs, update state (prediction) ...
                    updateState();
                    displayStatus();
                    // wait for a blob at most 0.05 sec, still quite responsive to keyboard
                    //uwait(60000);
                    sensnet_wait(sensnet, 100);
                }
            
#warning "TODO: send state (output) to the mapper"

            } // end of main loop

            return 0;
        }

    private:

        int argc;
        char** argv;

        static volatile sig_atomic_t quit;

        gengetopt_args_info options;
    
        // Spread settings
        char* spreadDaemon;
        int skynetKey;
        modulename moduleId;
 
        // Sensnet module
        sensnet_t *sensnet;

        bool opengl; // true = enable opengl display
        bool outputLog; // true = log output blobs

        cotk_t *console;
        //char* consoleTempl;

        SensnetBlob blob;
        uint64_t startTStamp; // first timestamp received
        int numBlobs; // total number of blobs read

        vector<Detector*> detectors;

        /* PSEUDO-CODE, TODO: implement this!
        auto_ptr<Tracker> tracker;
        */

        SensnetLog writer;

        //pthread_t glutThreadId;
        pthread_mutex_t renderMtx;
        GlRenderList renderList;

        CMapElementTalker mapTalker;
    };

    volatile sig_atomic_t BlobStereo::quit = 0;

    void BlobStereo::sigintHandler(int /*sig*/)
    {
        // if the user presses CTRL-C three or more times, and the program still
        // doesn't terminate, abort
        if (quit > 2) {
            abort();
        }
        quit++;
    }

} // end namespace


// main program, just calls BlobStereo::run()
int main(int argc, char** argv)
{
    blobstereo::BlobStereo app(argc, argv);
    try {
        return app.run();
    } catch(std::exception &e) {
        cerr << "Caught std::exception " << typeid(e).name() << endl;
    }
    // other exceptions, not derived from std::exception, will just cause the program to abort

    return -1;
}
