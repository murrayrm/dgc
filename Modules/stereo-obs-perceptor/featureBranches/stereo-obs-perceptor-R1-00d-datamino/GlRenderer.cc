// std C++ headers
#include <iostream>
#include <string>
#include <csetjmp>
// opengl stuff
#include <GL/glu.h>
#include <GL/glut.h>
// DGC headers
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
// local headers
#include "GlRenderer.hh"
#include "glutil.hh" // opengl utilities
#include "util.hh" // general purpose utilities

namespace blobstereo
{

    ImagePairRenderer::ImagePairRenderer()
    {
        txtId[0] = txtId[1] = 0;
        imgList[0] = imgList[1] = 0;
    }

    /**
     * If the blob is not a StereoBlob, or some error occurs, throws
     * a GlRenderer::Error exception.
     */
    void ImagePairRenderer::update(const SensnetBlob& blob) throw(Error)
    {
        // copy the images from the blob to m_left and m_right
        StereoImageBlob* sib = reinterpret_cast<StereoImageBlob*>(blob.getData());

        if (sib->blobType != SENSNET_STEREO_IMAGE_BLOB) {
            throw Error(ERRSTR("Blob type is not SENSNET_STEREO_IMAGE_BLOB"));
        }
        
        Image::format_t fmt = Image::FORMAT_GRAY8;
        if (sib->channels == 3) {
            fmt = Image::FORMAT_RGB;
        }

        img[0].init(sib->cols, sib->rows, fmt);
        img[1].init(sib->cols, sib->rows, fmt);

        memcpy(img[0].getData(), sib->imageData + sib->leftOffset, sib->leftSize);
        memcpy(img[1].getData(), sib->imageData + sib->rightOffset, sib->rightSize);
 
        dirty = true;
    }

    /** Shows the two images on the screen side by side (2D).
     */
    void ImagePairRenderer::render() throw(Error)
    {
        //MSG("DEBUG: entering ImagePairRendere::render()");

        // todo: check for errors
        if (txtId[0] <= 0) {
            glGenTextures(2, txtId);
        }

        if (imgList[0] <= 0) {
            imgList[0] = glGenLists(2);
            imgList[1] = imgList[0] + 1;
        }

        imageToTexture(img[0], txtId[0]); // left
        imageToTexture(img[1], txtId[1]); // right

        for (int i = 0; i < 2; i++) { // two times
            glEnable(GL_TEXTURE_2D);
            glDisable(GL_BLEND);
            pushViewport();
            //setImageCoord();

            glBindTexture (GL_TEXTURE_2D, txtId[i]);
            if (i == 0) { // left image
                glTranslatef(-1.0, 0, 0);
            }
            //glScalef(0.5, 0.5, 1);

            glBegin (GL_QUADS);
            glTexCoord2f (0.0, 0.0);
            glVertex3f (0.0, 1.0, 0.0);
            glTexCoord2f (1.0, 0.0);
            glVertex3f (1.0, 1.0, 0.0);
            glTexCoord2f (1.0, 1.0);
            glVertex3f (1.0, 0.0, 0.0);
            glTexCoord2f (0.0, 1.0);
            glVertex3f (0.0, 0.0, 0.0);
            glEnd ();

            popViewport();
            glEnable(GL_BLEND);
            glDisable(GL_TEXTURE_2D);
        }

        dirty = false;
    }
    
    // using a lot of global variables is not considered good programming,
    // but it seems to be the only way with glut
    static GlRenderer* mainRenderer = NULL;
    static pthread_mutex_t* rendererMtx = NULL;
    static pthread_cond_t* rendererCond = NULL;
    static bool quit = false;
    static pthread_t glutThreadID = -1;
    static bool posted = false;

    // longjmp to exit from glutMainLoop, there seem to be no other easy way :-(
#warning "TODO: get rid of glut!!!"
    static jmp_buf quitJmp;

    static void displayFunc()
    {
        glDisable(GL_BLEND);
        glClearColor (0.0, 0.0, 0.0, 0.0); // clear the screen to transparent black
        glEnable(GL_BLEND);
        // clear the color buffer and the depth buffer
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        // camera position: at x,y,z, looking at x,y,z, up x,y,z
        gluLookAt (0.0, 0.0, 0.0,   1.0, 0.0, 0.0,   0.0, 1.0, 0.0);
        
        // usually a good idea
        glEnable (GL_DEPTH_TEST); //enable the depth testing
  
        pthread_mutex_lock(rendererMtx);
        mainRenderer->render();
        pthread_mutex_unlock(rendererMtx);

        glutSwapBuffers();
        posted = false;
    }


    static void idleFunc()
    {
        //MSG("IdleFunc() called");

        if (quit)
            longjmp(quitJmp, 1);

        bool redraw = false;
        pthread_mutex_lock(rendererMtx);
        redraw = mainRenderer->isDirty();
        pthread_mutex_unlock(rendererMtx);

        if (redraw) {
            if (!posted) {
                glutPostRedisplay();
                posted = true;
            }
        } else {
            usleep(0); // yield, give other threads/processes a chance to run
        }
    }

    void* glutThread(void* /* param */)
    {
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(640,480);
	glutCreateWindow("BlobStereo - Debug OpenGL Display");

        // set callbacks
        glutDisplayFunc(displayFunc);
        glutIdleFunc(idleFunc);

        glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
        glColor3f(1.0, 1.0, 1.0);
        if (setjmp(quitJmp) == 0) {
            glutMainLoop();
        } else {
            // nothing, we just wanted to get out of glutMainLoop!
        }
        return NULL;
    }

    int startGlutThread(int argc, char** argv,
                         GlRenderer* mainRenderer,
                         pthread_mutex_t* rendererMtx,
                         pthread_cond_t* rendererCond)
    {
        blobstereo::mainRenderer = mainRenderer; // make it available to glut callbacks
        blobstereo::rendererMtx = rendererMtx; // same as above
        blobstereo::rendererCond = rendererCond; // same as above // FIXME: NOT USED, use or remove
        static bool glutInited = false;
        if (!glutInited) {
            glutInit(&argc, argv);
            glutInited = true;
        }

        return pthread_create(&glutThreadID, NULL, glutThread, NULL);
        //return glutThreadId;
    }

    void stopGlutThread()
    {
        if (glutThreadID == pthread_t(-1))
            return;

        quit = true;
        pthread_join(glutThreadID, NULL);
        quit = false; // just in case we want to restart the thread
    }

}
