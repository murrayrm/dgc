#ifndef __IUPDATABLE_HH__
#define __IUPDATABLE_HH__

#include "SensnetBlob.hh"
#include "ILockable.hh"

namespace stereo_obs
{

    class Error;

    class IUpdatable : virtual public ILockable
    {
    public:
        virtual ~IUpdatable() { }

        /**
         * Update the information in this object with the new sensnet blob.
         */
        virtual void update(const SensnetBlob& blob) throw(Error) = 0;

        void update(const SensnetBlob& blob) volatile throw(Error)
        {
            LockingPtr<IUpdatable> self(this);
            self->update(blob);
        }
    };

}

#endif
