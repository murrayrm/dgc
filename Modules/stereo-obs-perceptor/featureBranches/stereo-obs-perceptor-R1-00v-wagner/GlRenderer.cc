// std C++ headers
#include <iostream>
#include <string>
#include <csetjmp>
// FLTK stuff
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Box.H>
// opengl stuff
#include <GL/glu.h>
#include <GL/glut.h>
// DGC headers
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <dgcutils/AutoMutex.hh>
// local headers
#include "GlRenderer.hh"
#include "glutil.hh" // opengl utilities
#include "util.hh" // general purpose utilities

namespace stereo_obs
{

    static pthread_mutex_t* rendererMtx = NULL;

    //-----------------------------------
    // GlRenderer
    //-----------------------------------

/*
    void GlRenderer::draw()
    {
        Fl_Gl_Window::draw();
        if (!valid()) {
            //glDisable(GL_BLEND);
            //glClearColor (0.0, 0.0, 0.0, 0.0); // clear the screen to transparent black
            glEnable(GL_BLEND);
            // clear the color buffer and the depth buffer
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glLoadIdentity();
            
            // camera position: at x,y,z, looking at x,y,z, up x,y,z
            gluLookAt (0.0, 0.0, 0.0,   1.0, 0.0, 0.0,   0.0, 1.0, 0.0);
            
            // usually a good idea
            glEnable (GL_DEPTH_TEST);
        }

        Fl::lock();
        pthread_mutex_lock(rendererMtx);
        render();
        pthread_mutex_unlock(rendererMtx);
        Fl::unlock();
    }

*/

    void RenderWidget::draw()
    {
/*
        cout << "Rendering " << label() << ": x()=" << x() << ", y()=" << y()
             << ", w()=" << w() << ", h()=" << h() << endl;
*/
        if (1 || !valid()) {
            // Make sure we are using the correct context
            make_current();
            glLoadIdentity();
            glViewport(0, 0, w(), h());

/*
            // Get the viewport dimensions
            GLint vp[4];
            glGetIntegerv(GL_VIEWPORT, vp);
            cout << label() << ": viewport is " << vp[0] << ", " << vp[1]
                 << ", " << vp[2] << ", " << vp[3] << endl;
            //glTranslatef(-1.0, -1.0, 0);
            //glScalef(2.0/vp[2], -2.0/vp[3], 1.0);
*/

            glDisable (GL_DEPTH_TEST);
            glDisable(GL_BLEND);
            glClearColor (0.0, 0.0, 0.0, 0.0); // clear the screen to transparent black
            glEnable(GL_BLEND);
            // clear the color buffer and the depth buffer
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            // camera position: at x,y,z, looking at x,y,z, up x,y,z
            //gluLookAt (0.0, 0.0, 0.0,   0.0, 0.0, 1.0,   0.0, 1.0, 0.0);
            
            // usually a good idea
            glEnable (GL_DEPTH_TEST);
            //glDisable (GL_DEPTH_TEST);
        }

        try {
/*
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glColor3f(1.0, 0.0, 1.0);
            glBegin(GL_POLYGON);
            glVertex3f(1.0,  1.0,  -1.0);
            glVertex3f(1.0,  -1.0, -1.0);
            glVertex3f(-1.0, -1.0, -1.0);
            glVertex3f(-1.0, 1.0,  -1.0);
            glEnd();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

            glColor3f(0.0, 1.0, 0.0);
            for (int i = 0; i <= 10; i++) {
                glBegin(GL_LINES);
                glVertex3f(-1.0 + 0.2*i, -1.0, -0.9);
                glVertex3f(-1.0 + 0.2*i, 1.0, -0.9);
                glEnd();
            }
            for (int i = 0; i <= 10; i++) {
                glBegin(GL_LINES);
                glVertex3f(-1.0, -1.0 + 0.2*i, -0.9);
                glVertex3f(1.0,  -1.0 + 0.2*i, -0.9);
                glEnd();
            }

            glColor4f(0.0, 0.0, 0.0, 0.0);
*/
            AutoMutex am(*rendererMtx);
            rend->render();
        } catch(Error& err) {
            cerr << label() << ": Error while rendering: " << err.what() << endl;
        }
    }

    void RenderWidget::update(const SensnetBlob& blob) throw(Error)
    {
        if (true) { // critical section
            AutoMutex am(*rendererMtx);
            rend->update(blob);
        }
        Fl::lock();
        redraw();
        Fl::awake();
        Fl::unlock();
    }
    
    void RenderWidget::show()
    {
        // Fl_Window (under x) has the bad behavior of setting the label
        // to FL_LABEL_NONE every time show() is called
        Fl_Labeltype savedType = labeltype();
        Fl_Gl_Window::show();
        labeltype(savedType);
    }

    //-----------------------------------
    // ImageRenderer
    //-----------------------------------

    void ImageRenderer::render() throw(Error)
    {
        if (txtId <= 0) {
            glGenTextures(1, &txtId);
        }

        CvSize sz = img->toGlTexture(txtId);
        float scaleX = float(img->getWidth())/sz.width;
        float scaleY = float(img->getHeight())/sz.height;        

        glEnable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
        pushViewport();
        
        glBindTexture (GL_TEXTURE_2D, txtId);
        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(scaleX, scaleY, 1.0);

        // no lightning or coloring
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
/*
        if (which == DISPARITY) {
            glColor3f(0.0, 0.0, 1.0);
        }
*/

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0,  1.0,  0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (1.0,   1.0,  0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (1.0,  -1.0,  0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0,  0.0);

        glEnd ();

        glPopMatrix(); // texture matrix
        popViewport();
        glEnable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);

        changed = false;
    }

    //-----------------------------------
    // SensnetImageRenderer
    //-----------------------------------

    void SensnetImageRenderer::update(const SensnetBlob& blob) throw(Error)
    {
        // copy the images from the blob to m_left and m_right
        StereoImageBlob* sib = reinterpret_cast<StereoImageBlob*>(blob.getData());

        if (sib->blobType != SENSNET_STEREO_IMAGE_BLOB) {
            throw NotStereoBlobError(ERRSTR("SensnetImageRenderer::update()"), *sib);
        }

        if (sib->cols != img.getWidth() || sib->rows != img.getHeight()) {
            if (which != DISPARITY) {
                img.init(sib->cols, sib->rows, IPL_DEPTH_8U, sib->channels);
            } else {
                img.init(sib->cols, sib->rows, IPL_DEPTH_8U, 2); // value and alpha channels
            }
        }

        switch (which)
        {
        default:
        case LEFT:
            memcpy(img.getRawData(), sib->imageData + sib->leftOffset, sib->leftSize);
            break;

        case RIGHT:
            memcpy(img.getRawData(), sib->imageData + sib->rightOffset, sib->rightSize);
            break;

        case DISPARITY:
            // copy disparity image and add alpha channel
            int bpr = img.getBytesPerRow();
            int16_t* start = (int16_t*)(sib->imageData + sib->dispOffset);
            int16_t* end = (int16_t*)(sib->imageData + sib->dispOffset + sib->dispSize);
            uint8_t* row = img.getRawData();
            for (int16_t* p = start; p != end; p += sib->cols) {
                uint8_t* rp = row;
                for (int16_t* q = p; q != p + sib->cols; q++) {
                    *rp++ = (*q >> 5) & 255;      // luminance
                    *rp++ = (*q == -1) ? 0 : 255; // alpha (opaqueness)
                }
                row += bpr;
            }
            break;
        };

        base_t::update();
        dirty = true;
    }

    void SensnetImageRenderer::render() throw(Error)
    {
        if (which == DISPARITY) {
            glColor3f(0.0, 0.0, 1.0);
        }
        base_t::render();
        dirty = false;
    }

    //-------------------------------------------
    // FLTK stuff
    //-------------------------------------------

    void* fltkThread(void* /* param */)
    {
	Fl::lock();
        void* ret = (void*) Fl::run();
	Fl::unlock();
	return ret;
    }

#define USE_TABS 0
#define USE_CHECKBOXES 1

#if USE_CHECKBOXES

    class MyTile : public Fl_Group
    {
        typedef Fl_Group base_t;

    public:
        MyTile(int x, int y, int w, int h, const char* label = NULL)
            : Fl_Group(x, y, w, h, label)
        { }

        void draw()
        {
            vector<unsigned int> shown;
            int i;
            for (i = 0; i < children(); i++)
            {
                if (child(i)->visible())
                    shown.push_back(i);
            }
            if (shown.size() > 0) {
                // find how many cells to subdivide the group
                unsigned int cols;
                for(cols = 0; cols*cols < shown.size(); cols++);
                unsigned int rows = (shown.size() + cols - 1) / cols;
                // we'll use a rows * cols cell array, align widgets to these cells
                i = 0;
                for (unsigned int r = 0; r < rows && i < (int)shown.size(); r++)
                {
                    for (unsigned int c = 0; c < cols && i < (int)shown.size(); c++, i++)
                    {
                        int x1 = x() + c * w() / cols;
                        int x2 = x() + (c + 1) * w() / cols;
                        int y1 = y() + r * h() / rows;
                        int y2 = y() + (r + 1) * h() / rows;
                        child(shown[i])->box(FL_DOWN_BOX);
                        child(shown[i])->resize(x1, y1, x2 - x1, y2 - y1);
                    }
                }
            }
            base_t::draw();
        }
    };

    MyTile* myTile = NULL;

    void checkboxCallback(Fl_Widget* w, void* userData)
    {
        Fl_Check_Button* cb = dynamic_cast<Fl_Check_Button*>(w);
        assert(cb != NULL);
        Fl_Widget* rw = (Fl_Widget*) userData;
        if (cb->value())
            rw->show();
        else
            rw->hide();
        myTile->redraw();
    }

#endif

    static Fl_Window* mainWindow = NULL;
    static pthread_t fltkThreadID = -1;

    int startFltkThread(int argc, char** argv,
                        const vector<Fl_Widget*>& widgets,
                        pthread_mutex_t* rendererMtx)
    { 
        stereo_obs::rendererMtx = rendererMtx; // same as above

        {
#if USE_TABS
            mainWindow = new Fl_Double_Window(0, 0, 640, 480, "Stereo Obstacle Perceptor");
            {
                Fl_Tabs* o = new Fl_Tabs(0, 0, 640, 480);
                for (unsigned int i = 0; i < widgets.size(); i++) {
                    const int spaceX = 5;
                    const int spaceY = 25;
                    int x = widgets[i]->x();
                    int y = widgets[i]->y();
                    if (x < spaceX || y < spaceY) {
                        int w = widgets[i]->w();
                        int h = widgets[i]->h();
                        if (x < spaceX) {
                            x = spaceX;
                            w += (spaceX - x);
                        }
                        if (y < spaceY) {
                            y = spaceY;
                            h += (spaceY - y);
                        }
                        widgets[i]->resize(x, y, w, h);
                    }
                    widgets[i]->labeltype(FL_NORMAL_LABEL);
                    o->resizable(widgets[i]);
                    o->add(widgets[i]);
                    cout << "Adding tab: " << widgets[i]->label() << endl;
                }
                o->end();
                //o->resizable(o);
            }
            mainWindow->end();
            mainWindow->resizable(mainWindow);
#elif USE_CHECKBOXES
            const int sidebarSize = 100;
            mainWindow = new Fl_Double_Window(0, 0, sidebarSize + 640, 480,
                                              "Stereo Obstacle Perceptor");
            {
                Fl_Tile* o = new Fl_Tile(0, 0, sidebarSize + 640, 480);
                {
                    Fl_Scroll* o = new Fl_Scroll(0, 0, sidebarSize, 480);
                    {
                        Fl_Pack* o = new Fl_Pack(0, 0, sidebarSize, 480);
                        o->type(Fl_Pack::VERTICAL);
                        o->spacing(5);
                        for (unsigned int i = 0; i < widgets.size(); i++)
                        {
                            Fl_Check_Button* o =
                                new Fl_Check_Button(0, 20*i, sidebarSize, 18, widgets[i]->label());
                            o->labeltype(FL_NORMAL_LABEL);
                            o->callback(checkboxCallback, (void*)  widgets[i]);
                            o->down_box(FL_DOWN_BOX);
                            o->value(i == 0 ? 1 : 0); // set to 1 only the first one
                        }
                        o->end();
                    }
                    o->end();
                }
                {
                    myTile = new MyTile(sidebarSize, 0, 640, 480);
                    for (unsigned int i = 0; i < widgets.size(); i++)
                    {
                        if (i > 0) // hide all except the first one
                            widgets[i]->hide();
                        widgets[i]->box(FL_DOWN_BOX);
                        myTile->add(widgets[i]);
                    }
                    myTile->box(FL_DOWN_BOX);
                    myTile->end();
                }
                o->end();
                mainWindow->resizable(o);
            }
            mainWindow->end();
#endif
        }

        mainWindow->show(argc,argv);

        return pthread_create(&fltkThreadID, NULL, fltkThread, NULL);
    }

    void stopFltkThread()
    {
        if (fltkThreadID == pthread_t(-1))
            return;
        assert(mainWindow != NULL);

        Fl::lock();
        Fl::delete_widget(mainWindow);
        Fl::awake();
        Fl::unlock();
        pthread_join(fltkThreadID, NULL);
        mainWindow = NULL;
    }


}
