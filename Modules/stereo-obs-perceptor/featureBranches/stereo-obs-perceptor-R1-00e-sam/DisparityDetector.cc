// C++ headers
#include <iostream>
#include <vector>
#include <limits>
// boost headers
#include <boost/scoped_array.hpp>
// OpenCV headers
#include <cv.h>
#include <highgui.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <interfaces/StereoImageBlob.h>
// local headers
#include "DisparityDetector.hh"
#include "util.hh"
#include "glutil.hh"

namespace blobstereo
{
    using namespace std;
    using namespace boost;

    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
        abort();
    }

    DisparityDetector::DisparityDetector(bool dbg, int maxDisp, int mapHeight,
                      double hiThres, double loThres)
        : m_maxDisp(maxDisp), m_mapHeight(mapHeight),
          m_mapMax(0), m_highThres(hiThres), m_lowThres(loThres),
          m_debug(dbg), m_dispTex(0), m_mapTex(0), m_debugTex(0)
    {
        if (m_debug) {
            cvRedirectError(abortOnCvError);
        }
        memset(&m_maskMat, 0, sizeof(m_maskMat));
    }

    DisparityDetector::~DisparityDetector()
    {
        if (m_maskMat.data.ptr != NULL)
            cvReleaseData(&m_maskMat);
    }

    /** Given a CvSeq sequence of points, in image coordinates, compute the
     * convex hull, convert coords into vehicle frame, and put them in the
     * 'vertices' array.
     */
    void DisparityDetector::pointsToObstacle(StereoImageBlob* sib, const CvArr* points,
                                             vector<point2_uncertain>* vertices)
    {
        // calculate the convex hull of the component
        CvMat stub;
        CvMat* contour = cvGetMat(points, &stub);
        int num = contour->rows * contour->cols;
        scoped_array<int32_t> hull(new int32_t[num*2]);
        CvMat hullMat = cvMat(1, num, CV_32SC2, hull.get());

        // debug
/*
        MSG("pointsToObstacle: contour is:");
        for (int i = 0; i < num*2; i+=2)
        {
            cerr << "(" << ((int32_t*)contour->data.ptr)[i] << ", "
                 << ((int32_t*)contour->data.ptr)[i+1] << ")";
        }
        cerr << endl;
*/

        cvConvexHull2(contour, &hullMat, CV_CLOCKWISE, 1 /* points, not inxedes */);

        int nPoints = hullMat.cols;

        if (m_debug) {
            vector<int32_t> hullX;
            vector<int32_t> hullY;
            for (int i = 0; i < nPoints; i++) {
                // (column, disparity) coordinates
                int32_t c = hull[2*i];
                int32_t d = hull[2*i + 1];
                hullX.push_back(c);
                hullY.push_back(d);
            }
            m_dbgHullX.push_back(hullX);
            m_dbgHullY.push_back(hullY);
        }

        assert(vertices != NULL);
        // use a reference for ease of notation (vert[i] instead of (*vert)[i])
        vector<point2_uncertain>& vert = *vertices;
        vert.resize(nPoints);
#warning "FIXME: ******************************************"
#warning "FIXME: - calculate height of the obstacle correctly, handle floating objects"
#warning "FIXME:   (tree foilage, the sky (yes sometines that appears as an obstacle!!!) )"
#warning "FIXME: - should I calculate the convex hull in vehicle/local frame?"
#warning "FIXME:   (seems to be fine in disparity space)"
#warning "FIXME: ******************************************"
        float cell2disp = float(m_maxDisp) / m_mapHeight;
        for (int i = 0; i < nPoints; i++) {
            // (column, disparity) coordinates
            float c = hull[2*i];
            float d = hull[2*i + 1] * cell2disp;
            float xs, ys, zs;
            StereoImageBlobImageToSensor(sib, c, 240 /*fixme*/, d, &xs, &ys, &zs);
            float x, y, z;
            StereoImageBlobSensorToVehicle(sib, xs, ys, zs, &x, &y, &z);
            // get world coordinates
            vert[i].x = x;
            vert[i].y = y;
            // no uncertainty information yet
            vert[i].max_var = 0;
            vert[i].min_var = 0;
            vert[i].axis = 0;
        }
    }

    /** Given a CvArr containing a mask, find all the edge points and put them
     * in the points matrix, which is a 1D row vector containing 2D points
     * (type CV_32SC2, two channels of int32_t);
     * Note: if maskArr is of size (rows, cols), it searches in the rectangle
     * (1, 1, rows - 2, cols - 2) for a blob, and the mask boundary should be
     * all set to zero.
     */
    void DisparityDetector::scanRegion(CvArr* maskArr, int xOff, int yOff, RegionData* reg)
    {
        vector<int32_t> xv;
        vector<int32_t> yv;
        CvMat maskStub;
        CvMat* mask = cvGetMat(maskArr, &maskStub);
        // must be of type int8_t or uint8_t
        assert(CV_MAT_TYPE(mask->type) == CV_8UC1 || CV_MAT_TYPE(mask->type) == CV_8SC1);

#warning "TODO: calculate other statistics about this blob (center, bounding box, height...)"

        // TODO: check this code for bugs. Seems to be working fine, but maybe it's not perfect

        uint8_t* start = (uint8_t*) mask->data.ptr;
        for (int r = 1; r < mask->rows - 1; r++) {
            uint8_t* row = start + mask->step * r + 1;
            uint8_t* q = row;
            uint8_t* qend = row + mask->cols - 2;
            for (; q != qend; q++) {
                int added = 0;
                if (*q != *(q - 1)) {
                    // vertical edge
                    if (*(q - 1) == 1) {
                        // add *(q - 1)
                        xv.push_back(q - row - 1 + xOff);
                        yv.push_back(r + yOff);
                    } else if (*q == 1) {
                        // add *q
                        xv.push_back(q - row + xOff);
                        yv.push_back(r + yOff);
                        added = 1;
                    }
                }
                if (*q != *(q - mask->step)) {
                    // horizontal edge
                    if (*(q - mask->step) == 1) {
                        // add *(q - mask->step)
                        xv.push_back(q - row + xOff);
                        yv.push_back(r - 1 + yOff);
                    } else if (!added && *q == 1) {
                        // add *q
                        xv.push_back(q - row + xOff);
                        yv.push_back(r + yOff);
                    }
                }
            } // end for (each column)
        } // end for (each row)

        // debugging stuff
        if (reg->debugXv)
            *reg->debugXv = xv;
        if (reg->debugYv)
            *reg->debugYv = yv;

        // create a CvMat out of the vector
	if (xv.size() > 0) {
	    reg->edges = cvMat(xv.size(), 1, CV_32SC2);
            cvCreateData(&reg->edges);
	    int32_t* mat = (int32_t*) reg->edges.data.ptr;
	    for (unsigned int i = 0; i < xv.size(); i++) {
                *mat++ = xv[i];
                *mat++ = yv[i];
	    }
	} else { // OpenCv doesn't like matricies with 0-sized dimensions
	    reg->edges = cvMat(1, 1, CV_32SC2);
            cvCreateData(&reg->edges);
	    int32_t* mat = (int32_t*) reg->edges.data.ptr;
	    mat[0] = xOff + mask->cols/2;
	    mat[1] = yOff + mask->rows/2;
	}
    }

    vector<Obstacle> DisparityDetector::detect(const SensnetBlob& blob)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        
        if (sib->version != 4) {
            if (sib->version < 4) {
                ERRMSG("StereoImageBlob version " << sib->version << " not supported!!!");
                return vector<Obstacle>(); // empty vector
            } else {
                MSG("StereoImageBlob version " << sib->version << " newer than 4, continuing");
            }
        }

        if (m_debug) {
            m_dbgEdgesX.clear();
            m_dbgEdgesY.clear();
            m_dbgHullX.clear();
            m_dbgHullY.clear();
        }

        // initialize the map
        if (m_mapImg.getWidth() != sib->cols) {
            m_mapImg.init(sib->cols, m_mapHeight);
            m_maskImg.init(sib->cols + 2, m_mapHeight + 2);
            // create maskImg sharing the same underlying data as m_maskImg
            cvGetSubRect(m_maskImg.getImage(), &m_maskMat, cvRect(1, 1, sib->cols, m_mapHeight));
            cvIncRefData(&m_maskMat);
           if (m_debug) {
                m_debugImg.init(sib->cols, m_mapHeight);
            }
        } else {
            cvZero(m_mapImg.getImage());
            cvZero(m_maskImg.getImage());
            if (m_debug) {
                cvZero(m_debugImg.getImage());
            }
        }

        double disp2cell = m_mapHeight / (m_maxDisp * sib->dispScale);
        const int16_t dispThres = 3; // minimum interesting disparity

        m_mapMax = 0;
        for (int c = 0; c < sib->cols; c++) {
            for (int r = 0; r < sib->rows; r++) {
                int16_t d = *(int16_t*) StereoImageBlobGetDisp(sib, c, r);
                if (d >= dispThres) {
                    unsigned int cell = unsigned(d * disp2cell);
                    
                    if (cell > m_mapHeight) {
                        cell = m_mapHeight;
                    }

                    int32_t val = m_mapImg[cell][c][0] + 1;
                    if (val == numeric_limits<dispmap_t>::max()) {
                        val = numeric_limits<dispmap_t>::max();
                    }
                    m_mapImg[cell][c][0] = dispmap_t(val);
                    if (val > m_mapMax) {
                        m_mapMax = val;
                    }
                }
            }
        }

        MSG("mapMax = " << int(m_mapMax));

        vector<Obstacle> obst;

        /* Use two thresholds to detect blobs. A blob is detected if all its cells
         * are above the low threshold and at least one is above the high threshold.
         * Method: first, threshold the image with the low threshold and create a mask
         * (done in the code above).
         * Then, scan for cells above the high threshold, and use cvFloodFill on the
         * mask to find the blob.
         */

        // work directly with IplImage from here on
        //CvMat* mask = &m_maskMat;
        IplImage* map = m_mapImg.getImage();

        //IplConvKernel* ball = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_ELLIPSE, NULL);
        //IplConvKernel* hLine = cvCreateStructuringElementEx(7, 1, 3, 0, CV_SHAPE_CROSS, NULL);

        //cvThreshold(map, mask, m_lowThres, numeric_limits<uint8_t>::max(),
        //            CV_THRESH_BINARY);

        //cvErode(mask, mask, hLine, 1);
        //cvDilate(mask, mask, ball, 1);

        //cvReleaseStructuringElement(&hLine);
        //cvReleaseStructuringElement(&ball);

        int height = m_mapImg.getHeight();
        int width = m_mapImg.getWidth();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                int val = m_mapImg[r][c][0];
                if (val > m_highThres) {
                    int loDiff = int(val - m_lowThres);
                    int hiDiff = 255 - val;
                    CvConnectedComp comp;

                    if (m_maskImg[r+1][c+1][0] != 0)
                        continue;

                    cvFloodFill(map, cvPoint(c, r), cvRealScalar(255),
                                cvRealScalar(loDiff), cvRealScalar(hiDiff),
                                &comp, CV_FLOODFILL_FIXED_RANGE | CV_FLOODFILL_MASK_ONLY | 4,
                                m_maskImg.getImage());
                    
                    Obstacle ob;
                    ob.getState() = sib->state;
                    CvMat subRect;
                    //CvMat* points = NULL;

                    if (comp.rect.width > 0 && comp.rect.height > 0) {
                        // Rect must be 1 pix wider in each direction than the blob,
                        // but m_maskImg is 1pix wider than mask already, so need only
                        // to set the width and height
                        comp.rect.width += 2;
                        comp.rect.height += 2;
                        
                        cvGetSubRect(m_maskImg.getImage(), &subRect, comp.rect);
                        RegionData data;
                        vector<int32_t> dbgXv, dbgYv;
                        if (m_debug) {
                            data.debugXv = &dbgXv;
                            data.debugYv = &dbgYv;
                        }
                        scanRegion(&subRect, comp.rect.x - 1, comp.rect.y - 1, &data);
                        if (m_debug) {
                            m_dbgEdgesX.push_back(dbgXv);
                            m_dbgEdgesY.push_back(dbgYv);
                        }
                        // substitute all 1's with 255's, so getMaskEdgePoints won't
                        // consider them the next time (and so they will be rendered
                        // correctly).
                        uint8_t* start = (uint8_t*) subRect.data.ptr;
                        for (int r = 1; r < subRect.rows - 1; r++) {
                            for (int c = 1; c < subRect.cols - 1; c++) {
                                uint8_t* p = start + r * subRect.step + c;
                                if (*p == 1)
                                    *p = 255;
                            }
                        }
                        pointsToObstacle(sib, &data.edges, &ob.getVertices());
                        obst.push_back(ob);
                    } else {
                        ERRMSG("BUGBUG: cvFloodFill() did nothing, was the seed point masked???");
                        ERRMSG("BUGBUG: seed point: (c=" << c << ", r=" << r << ")" );
                    }
                }
            }
        }
        

#if 0 // I don't remove this code yet, because it may be useful as a template
            // this code works for a polyline of signed 32 bit points (2 channels)
            assert(CV_IS_SEQ_POLYLINE( contour ));
            assert(CV_MAT_TYPE(contour->flags) == CV_32SC2); 
            CvSeqReader reader;
            cvStartReadSeq(contour, &reader, 0);
            for (int i = 0; i < contour->total; i++) {
                CvPoint pt;
                CV_READ_SEQ_ELEM( pt, reader );
                // do something with pt
            }
#endif

        MSG("detect(): returning " << obst.size() << " obstacles");
        
        if (m_debug)
            result = obst;
        // convert from vehicle frame to local frame
        for (unsigned int i = 0; i < obst.size(); i++)
        {
            vector<point2_uncertain>& v = obst[i].getVertices();
            for (unsigned int j = 0; j < v.size(); j++)
            {
                float xv = v[j].x;
                float yv = v[j].y;
                float zv = 0;
                float xl, yl, zl;
                StereoImageBlobVehicleToLocal(sib, xv, yv, zv, &xl, &yl, &zl);
                v[j].x = xl;
                v[j].y = yl;
                
            }
        }
        return obst;
    }

    //////////////////////////////////////////
    // OpenGL Debug Display stuff (GlRenderer)
    //////////////////////////////////////////


    void DisparityDetector::update(const SensnetBlob& blob) throw(Error)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();

        if (m_dispImg.getWidth() != sib->cols || m_dispImg.getHeight() != sib->rows) {
            m_dispImg.init(sib->cols, sib->rows);
        }

        // copy disparity image and add alpha channel
        int bpr = m_dispImg.getBytesPerRow();
        int16_t* start = (int16_t*)(sib->imageData + sib->dispOffset);
        int16_t* end = (int16_t*)(sib->imageData + sib->dispOffset + sib->dispSize);
        uint8_t* row = m_dispImg.getRawData();
        for (int16_t* p = start; p != end; p += sib->cols) {
            uint8_t* rp = row;
            for (int16_t* q = p; q != p + sib->cols; q++) {
                *rp++ = (*q >> 5) & 255;      // luminance
                *rp++ = (*q == -1) ? 0 : 255; // alpha (opaqueness)
            }
            row += bpr;
        }

    }

    void DisparityDetector::render() throw(Error)
    {
        // todo: check for errors!! (not a big deal actually, this is just for debugging)
        if (m_dispTex == 0 || m_mapTex == 0) {
            GLuint txt[4];
            glGenTextures(4, txt);
            m_dispTex = txt[0];
            m_mapTex = txt[1];
            m_maskTex = txt[2];
            m_debugTex = txt[3];
        }
        
        float dispScaleX = 1.0;
        float dispScaleY = 1.0;
        float mapScaleX = 1.0;
        float mapScaleY = 1.0;
        float maskScaleX = 1.0;
        float maskScaleY = 1.0;
        float debugScaleX = 1.0;
        float debugScaleY = 1.0;

        ////////////////////////////////////////////
        // Create the texture for the disparity image
        ////////////////////////////////////////////

        if (m_dispImg.getWidth() > 0) {
            CvSize sz = m_dispImg.toGlTexture(m_dispTex);
            dispScaleX = float(m_dispImg.getWidth())/sz.width;
            dispScaleY = float(m_dispImg.getHeight())/sz.height;
        }

        ////////////////////////////////////////////
        // Create the texture for the disparity map
        ////////////////////////////////////////////

        if (m_mapMax == 0) {
            m_mapMax = 1; // avoid divide-by-zero
        }

        if (m_mapImg.getWidth() > 0) {
            // create texture for the map, with luminance - alpha
            // TODO: move this in the class declaration to avoid reallocation
            scoped_array<uint8_t> mapImg(new uint8_t[m_mapImg.getWidth()*m_mapImg.getHeight()*2]);

            // maybe can be optimized ...
            int w = m_mapImg.getWidth();
            int h = m_mapImg.getHeight();
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    int32_t val = m_mapImg[r][c][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (/*val < 64 && val >= 0*/ true) {
                        //pix = val * 256 / 64; // this will be optimized by the compiler
                        pix = val * 256 / m_mapMax;
                        if (val == 0) {
                            alpha = 0;
                        }
                    } else {
                        pix = 255;
                    }
                    mapImg[(r * w + c)*2] = pix;
                    mapImg[(r * w + c)*2 + 1] = alpha;
                }
            }

            glBindTexture(GL_TEXTURE_2D, m_mapTex);
        
            // no interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, m_mapImg.getWidth(),
            //                  m_mapImg.getHeight(), GL_LUMINANCE_ALPHA,
            //                  GL_UNSIGNED_BYTE, mapImg.get());
            int width, height;
            int bpp = 2;
            int bpr = w * bpp;
            buildPlainTexture(m_mapImg.getWidth(), m_mapImg.getHeight(), bpp, bpr,
                              GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, mapImg.get(),
                              &width, &height);
            mapScaleX = float(m_mapImg.getWidth())/width;
            mapScaleY = float(m_mapImg.getHeight())/height;
        }

        CvMat sub;
        IplImage imgHdr;
        IplImage* mask = NULL;
        if (m_maskImg.getWidth() > 0) {
            cvGetSubRect(m_maskImg.getImage(), &sub,
                         cvRect(1, 1, m_maskImg.getWidth()-1, m_maskImg.getHeight()-1));
            mask = cvGetImage(&sub, &imgHdr);
        }
                
        if (m_maskImg.getWidth() > 0) {
            if (m_debug) {
                // check if m_maskImg is in fact binary (only 0 or 255 values)
                int nZero = 0;
                int nMax = 0;
                int nOther = 0;
                int otherVal = 0;
                for(int r = 1; r < m_maskImg.getHeight()-1; r++) {
                    for(int c = 1; c < m_maskImg.getWidth()-1; c++) {
                        int val = m_maskImg[r][c][0];
                        if (val == 0) {
                            nZero ++;
                        } else if (val == 255) {
                            nMax ++;
                        } else {
                            nOther++;
                            if (otherVal == 0) 
                                otherVal = m_maskImg[r][c][0];
                            //m_maskImg[r][c][0] = 255;
                        }
                        //if (val == 255)
                        //    cerr << 'O';
                        //else 
                        //    cerr << '.';
                    }
                    //cerr << '\n';
                }
                MSG("Mask: nZero = " << nZero << ", nMax = " << nMax);
                if (nOther > 0)
                    MSG("Found " << nOther << " values that are not 0 or 255 (val=" << otherVal << ")");
            }

            CvSize sz = ImageCvBase::toGlTexture(mask, m_maskTex, GL_ALPHA);
            maskScaleX = float(m_maskImg.getWidth())/sz.width;
            maskScaleY = float(m_maskImg.getHeight())/sz.height;
        }

        if (m_debug && m_debugImg.getWidth() > 0) {
            CvSize sz = m_debugImg.toGlTexture(m_debugTex);
            debugScaleX = float(m_maskImg.getWidth())/sz.width;
            debugScaleY = float(m_maskImg.getHeight())/sz.height;
        }

        ////////////////////////////////////////////
        // Draw the disparity image
        ////////////////////////////////////////////

        glEnable(GL_TEXTURE_2D);
        // no lightning or coloring
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);


        // draw the disparity image and the map
        pushViewport();

        glColor3f(0.0, 0.5, 0.25); // dark green
        glBindTexture(GL_TEXTURE_2D, m_dispTex);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(dispScaleX, dispScaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 0.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (0.0, 0.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (0.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

#if 0
        ////////////////////////////////////////////
        // Draw the disparity map
        ////////////////////////////////////////////

        pushViewport();
        glColor3f(0.0, 0.0, 0.5); // dark blue
        glBindTexture(GL_TEXTURE_2D, m_mapTex);

        glTranslatef(1.0, 0.0, 0.0);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(mapScaleX, mapScaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 0.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (0.0, 0.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (0.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

        
#endif
        // no depth text, we want the next images to overlap and blend
        glDisable (GL_DEPTH_TEST);
        // enable blending
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if 1
        ////////////////////////////////////////////
        // Draw the mask
        ////////////////////////////////////////////

        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);

        pushViewport();
        glTranslatef(1.0, 0.0, 0.0);

        glColor3f(1.0, 0.0, 0.0); // red
        glBindTexture(GL_TEXTURE_2D, m_maskTex);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(maskScaleX, maskScaleY, 1.0);


        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 0.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (0.0, 0.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (0.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix(); // texture
        popViewport();

#elif 0
        if (m_debug) {
            ////////////////////////////////////////////
            // Draw the debug image
            ////////////////////////////////////////////

            pushViewport();
            glTranslatef(1.0, 0.0, 0.0);

            glColor4f(0.0, 1.0, 0.0, 1.0); // green
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, m_debugTex);

            glMatrixMode(GL_TEXTURE);
            glPushMatrix();
            glScalef(debugScaleX, debugScaleY, 1.0);

            glBegin (GL_QUADS);
            glTexCoord2f (0.0, 0.0);
            glVertex3f (-1.0, 0.0, 0.0);

            glTexCoord2f (1.0, 0.0);
            glVertex3f (0.0, 0.0, 0.0);

            glTexCoord2f (1.0, 1.0);
            glVertex3f (0.0, -1.0, 0.0);

            glTexCoord2f (0.0, 1.0);
            glVertex3f (-1.0, -1.0, 0.0);
            glEnd ();

            glPopMatrix(); // texture
            popViewport();
        }
#endif

#if 1
        if (m_dbgEdgesX.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            int vp[4];
            glGetIntegerv(GL_VIEWPORT, vp);
            glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            // image coordinates
            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/mask->width, -2.0/mask->height, 1.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_CULL_FACE);

#if 0
            glColor4f(1.0, 0.0, 1.0, 1.0); // magenta

            for (unsigned int i = 0; i < m_dbgEdgesX.size(); i++)
            {
                vector<int32_t> xv = m_dbgEdgesX[i];
                vector<int32_t> yv = m_dbgEdgesY[i];

                glBegin(GL_POINTS);
                for (unsigned int j = 0; j < xv.size(); j++)
                {
                    glVertex3f(xv[j], yv[j], 1.0);
                }
                glEnd();
            }
#endif

#if 1
            glColor3f(1.0, 1.0, 1.0);
            for (unsigned int i = 0; i < m_dbgHullX.size(); i++)
            {
                vector<int32_t> xv = m_dbgHullX[i];
                vector<int32_t> yv = m_dbgHullY[i];

                glBegin(GL_POLYGON);
                for (unsigned int j = 0; j < xv.size(); j++)
                {
                    glVertex3f(xv[j], yv[j], 1.0);
                }
                glEnd();
            }
#endif
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
        }
#endif

#if 1
        if (result.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            int vp[4];
            glGetIntegerv(GL_VIEWPORT, vp);
            glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            glDisable(GL_CULL_FACE);


            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            // draw x axis
            glColor4f(1.0, 1.0, 1.0, 1.0); // white
            glBegin(GL_LINES);
            glVertex3f(0.0, -1.0, 0.0);
            glVertex3f(0.0, 1.0, 0.0);
            glEnd();

            glColor4f(1.0, 0.3, 0.3, 0.5); // bright red

            glTranslatef(0.0, -1.0, 0.0);
            glScalef(0.04, 0.04, 0.06);
            
            for (unsigned int i = 0; i < result.size(); i++)
            {
                vector<point2_uncertain>& vert = result[i].getVertices();
                //MSG("Poly" << i << ":");
                glBegin(GL_POLYGON);
                
                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    // invert x and y
                    glVertex3f(vert[j].y, vert[j].x, 0.0);
                    //cerr << "(" << vert[j].x << "," << vert[j].y << ") ";
                }

                glEnd(); 
                //cerr << endl;
                //MSG("Poly" << i << " END");
           }

            //glEnable(GL_CULL_FACE);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
            popViewport();
        }
#endif

        glDisable (GL_BLEND);
        glEnable (GL_DEPTH_TEST);

    }

}
