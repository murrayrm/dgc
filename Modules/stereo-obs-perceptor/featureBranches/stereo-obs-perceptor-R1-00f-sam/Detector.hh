#ifndef __BLOBSTEREO_DETECTOR_HH__
#define __BLOBSTEREO_DETECTOR_HH__

#include <vector>
#include "SensnetBlob.hh"
#include "Obstacle.hh"

namespace blobstereo {

    // avoid including FLTK stuff if not needed
    class RenderWidget;

    using std::vector;

    class Detector
    {
    public:
        virtual ~Detector() { }
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob) = 0;

        // does this class need any state?
        // just in case, this will resets everything ;-)
        virtual void reset() { }

        /// Returns a list of widgets used to display the status of the detector
        /// mainly for debugging purposes.
        virtual vector<RenderWidget*> getWidgets() { return vector<RenderWidget*>(); }

    };

}

#endif
