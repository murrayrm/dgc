// C++ headers
#include <iostream>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>
// boost headers
#include <boost/scoped_array.hpp>
// FLTK headers
#include <FL/Fl_Widget.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
// OpenCV headers
#include <cv.h>
#include <highgui.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <frames/mat44.h>
#include <dgcutils/AutoMutex.hh>
#include <interfaces/StereoImageBlob.h>
// local headers
#include "DisparityDetector.hh"
#include "IUpdatable.hh"
#include "util.hh"
#include "glutil.hh"

namespace blobstereo
{
    using namespace std;
    using namespace boost;

    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
        abort();
    }

    DisparityDetector::DisparityDetector(bool dbg, int maxDisp, int mapHeight,
                                         double hiThres, double loThres, double hThres, double hTSlope,
                                         smallObsMethod_t smoMethod, float smallObsThres, float assocThres)
        : m_maxDisp(maxDisp), m_mapHeight(mapHeight),
          m_assocThres(assocThres),
          m_mapMax(0), m_highThres(hiThres), m_lowThres(loThres),
          m_hThres(hThres), m_hThresSlope(hTSlope),
          m_smallObsMethod(smoMethod), m_smallObsThres(smallObsThres),
          m_debug(dbg), m_dispTex(0), m_mapTex(0), m_debugTex(0)
    {
        pthread_mutex_init(&m_mtx, NULL);
        if (m_debug) {
            cvRedirectError(abortOnCvError);
        }
        memset(&m_maskMat, 0, sizeof(m_maskMat));

        // arbitrary values ...
        m_errorVar[0] = 2;
        m_errorVar[1] = 4;
        m_errorVar[2] = 2;
    }

    DisparityDetector::~DisparityDetector()
    {
        if (m_maskMat.data.ptr != NULL)
            cvReleaseData(&m_maskMat);
    }

    /** Given a CvSeq sequence of points, in image coordinates, compute the
     * convex hull, convert coords into vehicle frame, and put them in the
     * 'vertices' array.
     */
    void DisparityDetector::pointsToObstacle(StereoImageBlob* sib, RegionData& data,
                                             vector<point4>* vertices)
    {
        // calculate the convex hull of the component
        CvMat* contour = &data.edges;
        int num = contour->rows * contour->cols;
        scoped_array<int32_t> hull(new int32_t[num*2]);
        CvMat hullMat = cvMat(1, num, CV_32SC2, hull.get());

        // debug
/*
        MSG("pointsToObstacle: contour is:");
        for (int i = 0; i < num*2; i+=2)
        {
            cerr << "(" << ((int32_t*)contour->data.ptr)[i] << ", "
                 << ((int32_t*)contour->data.ptr)[i+1] << ")";
        }
        cerr << endl;
*/

        cvConvexHull2(contour, &hullMat, CV_CLOCKWISE, 1 /* points, not indexes */);

        int nPoints = hullMat.cols;

        if (m_debug) {
            vector<int32_t> hullX;
            vector<int32_t> hullY;
            for (int i = 0; i < nPoints; i++) {
                // (column, disparity) coordinates
                int32_t c = hull[2*i];
                int32_t d = hull[2*i + 1];
                hullX.push_back(c);
                hullY.push_back(d);
            }
            m_dbgHullX.push_back(hullX);
            m_dbgHullY.push_back(hullY);
        }

        assert(vertices != NULL);
        // use a reference for ease of notation (vert[i] instead of (*vertices)[i])
        vector<point4>& vert = *vertices;
        vert.resize(nPoints);
#warning "FIXME: ******************************************"
#warning "FIXME: - calculate height of the obstacle correctly, handle floating objects"
#warning "FIXME:   (tree foilage, the sky (yes sometimes that appears as an obstacle!!!) )"
#warning "FIXME: ******************************************"
        float cell2disp = float(m_maxDisp) / m_mapHeight;
        for (int i = 0; i < nPoints; i++) {
            // (column, disparity) coordinates
            float c = hull[2*i];
            float r = data.meanRow;
            float d = hull[2*i + 1] * cell2disp;
            // convert from image (col, row, disp) to sensor frame
            StereoImageBlobImageToSensor(sib, c, r, d, &vert[i][0], &vert[i][1], &vert[i][2]);
            vert[i][3] = 1;
        }
    }

    /** Given a CvArr containing a mask, find all the edge points and put them
     * in the points matrix, which is a 1D row vector containing 2D points
     * (type CV_32SC2, two channels of int32_t);
     * Note: if maskArr is of size (rows, cols), it searches in the rectangle
     * (1, 1, rows - 2, cols - 2) for a blob, and the mask boundary should be
     * all set to zero.
     */
    void DisparityDetector::scanRegion(CvArr* maskArr, int xOff, int yOff, RegionData* reg)
    {
        vector<int32_t> xv;
        vector<int32_t> yv;
        CvMat maskStub;
        CvMat* mask = cvGetMat(maskArr, &maskStub);
        // must be of type int8_t or uint8_t
        assert(CV_MAT_TYPE(mask->type) == CV_8UC1 || CV_MAT_TYPE(mask->type) == CV_8SC1);
        reg->meanRow = 0.0;
        reg->varRow = 0.0;
        float weightSum = 0.0;
#warning "TODO: calculate other statistics about this blob (center, bounding box, height...)"

        // TODO: check this code for bugs. Seems to be working fine, but maybe it's not perfect

        uint8_t* start = (uint8_t*) mask->data.ptr;
        for (int r = 1; r < mask->rows - 1; r++) {
            uint8_t* row = start + mask->step * r + 1;
            uint8_t* q = row;
            uint8_t* qend = row + mask->cols - 2;
            for (; q != qend; q++) {
                int added = 0;
                if (*q != *(q - 1)) {
                    // vertical edge
                    if (*(q - 1) == 1) {
                        // add *(q - 1)
                        xv.push_back(q - row - 1 + xOff);
                        yv.push_back(r + yOff);
                    } else if (*q == 1) {
                        // add *q
                        xv.push_back(q - row + xOff);
                        yv.push_back(r + yOff);
                        added = 1;
                    }
                }
                if (*q != *(q - mask->step)) {
                    // horizontal edge
                    if (*(q - mask->step) == 1) {
                        // add *(q - mask->step)
                        xv.push_back(q - row + xOff);
                        yv.push_back(r - 1 + yOff);
                    } else if (!added && *q == 1) {
                        // add *q
                        xv.push_back(q - row + xOff);
                        yv.push_back(r + yOff);
                    }
                }
                // calculate mean and variance of row
                if (*q == 1)
                {
                    int mr = r + yOff;
                    int mc = q - row + xOff;
                    reg->meanRow += m_rowSum[mr][mc][0];
                    reg->varRow += m_rowSumSq[mr][mc][0];
                    weightSum += m_mapImg[mr][mc][0];
                }
            } // end for (each column)
        } // end for (each row)

        // statistics
        reg->sum = weightSum;
        if (weightSum != 0)
        {
            reg->meanRow /= weightSum;
            reg->varRow = reg->varRow / weightSum - reg->meanRow * reg->meanRow;
        }
        else
        {
            reg->meanRow = reg->varRow = 0.0;
        }
        // TODO: calc reg->meanHeight and varHeight using meanRow and varRow

        // debugging stuff
        if (reg->debugXv)
            *reg->debugXv = xv;
        if (reg->debugYv)
            *reg->debugYv = yv;

        // create a CvMat out of the vector
	if (xv.size() > 0) {
	    reg->edges = cvMat(xv.size(), 1, CV_32SC2);
            cvCreateData(&reg->edges);
	    int32_t* mat = (int32_t*) reg->edges.data.ptr;
	    for (unsigned int i = 0; i < xv.size(); i++) {
                *mat++ = xv[i];
                *mat++ = yv[i];
	    }
	} else { // OpenCv doesn't like matricies with 0-sized dimensions
	    reg->edges = cvMat(1, 1, CV_32SC2);
            cvCreateData(&reg->edges);
	    int32_t* mat = (int32_t*) reg->edges.data.ptr;
	    mat[0] = xOff + mask->cols/2;
	    mat[1] = yOff + mask->rows/2;
	}
    }

    /**
     * Filters (removes) all the points in the specified region (where the mask is == 1) that
     * have a minimum row bigger than the threshold.
     * This is mainly to cut trees.
     */
    void DisparityDetector::filterRegion(CvArr* maskArr, int xOff, int yOff,
                                         RegionData* data)
    {
        CvMat maskStub;
        CvMat* mask = cvGetMat(maskArr, &maskStub);

        // find min row and max row
        int minRow = m_mapHeight;
        int maxRow = 0;
        uint8_t* start = (uint8_t*) mask->data.ptr;
        for (int r = 1; r < mask->rows - 1; r++) {
            for (int c = 1; c < mask->cols-1; c++) {
                minRow = min(minRow, int(m_minRow[r + yOff][c + xOff][0]));
                maxRow = max(maxRow, int(m_maxRow[r + yOff][c + xOff][0]));
            }
        }
        data->minRow = minRow;
        data->maxRow = maxRow;
        // filter out everything that has a too high minimum row
        float rowThres = minRow + (maxRow - minRow) * 0.8;
        start = (uint8_t*) mask->data.ptr;
        for (int r = 1; r < mask->rows - 1; r++) {
            uint8_t* row = start + mask->step * r + 1;
            for (int c = 1; c < mask->cols-1; c++) {
                if (m_minRow[r + yOff][c + xOff][0] >= rowThres) {
                    row[c] = 0; // too high, remove pixel
                }
            }
        }
        //IplConvKernel* ball = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_ELLIPSE, NULL);
        //cvErode(mask, mask, ball, 1);
        //cvDilate(mask, mask, ball, 1);
        //cvReleaseStructuringElement(&ball);
    }

    void DisparityDetector::calcPointCov(float cov[4][4], const point4 ps,
                                         const float himg2sens[4][4], const float sens2loc[4][4])
    {
        // Given the variance of col, row, disp (m_errorVar[1, 2, 3]), calculate
        // the variance in local frame using the linearized transformation.
        // jacobian of nonlinear transformation in the current point:
        // (xs, ys, zs, 1) = (Xs/Ws, Ys/Ws, Zs/Ws, 1) (note: Ws == disparity)
        float invW = 1.0/ps[3];
        float J[4][4] = {
            {invW,   0,    0, ps[0]*invW},
            {0,   invW,    0, ps[1]*invW},
            {0,      0, invW, ps[2]*invW},
            {0,      0,    0,          0}
        };
        float img2sens[4][4];
        mat44f_mul(img2sens, J, himg2sens);
        float img2loc[4][4];
        mat44f_mul(img2loc, sens2loc, img2sens);
        // covariance is cov = img2loc * diag(m_errorVar) * img2loc'
        // diag(...) * img2loc', optimized
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                cov[i][j] = m_errorVar[i] * img2loc[j][i];
        mat44f_mul(cov, img2loc, cov);
    }

    vector<Obstacle> DisparityDetector::detect(const SensnetBlob& blob)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        
        if (sib->version != 4) {
            if (sib->version < 4) {
                ERRMSG("StereoImageBlob version " << sib->version << " not supported!!!");
                return vector<Obstacle>(); // empty vector
            } else {
                MSG("StereoImageBlob version " << sib->version << " newer than 4, continuing");
            }
        }

        //AutoMutex am(m_mtx);

        if (m_debug) {
            m_dbgEdgesX.clear();
            m_dbgEdgesY.clear();
            m_dbgHullX.clear();
            m_dbgHullY.clear();
        }

        // initialize the map
        if (m_mapImg.getWidth() != sib->cols) {
            m_mapImg.init(sib->cols, m_mapHeight);
            m_rowSum.init(sib->cols, m_mapHeight);
            m_rowSumSq.init(sib->cols, m_mapHeight);
            //m_maxRow.init(sib->cols, m_mapHeight);
            //m_minRow.init(sib->cols, m_mapHeight);

            m_maskImg.init(sib->cols + 2, m_mapHeight + 2);
            // create maskImg sharing the same underlying data as m_maskImg
            cvGetSubRect(m_maskImg.getImage(), &m_maskMat, cvRect(1, 1, sib->cols, m_mapHeight));
            cvIncRefData(&m_maskMat);
           if (m_debug) {
                m_debugImg.init(sib->cols, m_mapHeight);
            }
        } else {
            cvZero(m_mapImg.getImage());
            cvZero(m_maskImg.getImage());
            cvZero(m_rowSum.getImage());
            cvZero(m_rowSumSq.getImage());
            if (m_debug) {
                cvZero(m_debugImg.getImage());
            }
        }
        //cvSet(m_maxRow.getImage(), cvRealScalar(0));
        //cvSet(m_minRow.getImage(), cvRealScalar(m_mapHeight));

        double disp2cell = m_mapHeight / (m_maxDisp * sib->dispScale);
        //StereoImageCamera& leftCam = sib->leftCamera; // for ease of writing
        //double row2height = (leftCam.sx * sib->dispScale * sib->baseline / leftCam.sy);

        //********************************************************
        // FILL IN THE MAP
        //*******************************************************

        const int16_t dispThres = 3; // minimum interesting disparity - cut things at infinity

        m_mapMax = 0;
        for (int c = 0; c < sib->cols; c++)
        {
            for (int r = 0; r < sib->rows; r++)
            {
                int16_t d = *(int16_t*) StereoImageBlobGetDisp(sib, c, r);
                if (d >= dispThres)
                {
                    unsigned int cell = unsigned(d * disp2cell);
                    
                    if (cell >= m_mapHeight)
                    {
                        cell = m_mapHeight-1;
                    }

                    float val = m_mapImg[cell][c][0] + 1;
/*
                    if (val >= numeric_limits<dispmap_t>::max()) {
                        val = numeric_limits<dispmap_t>::max();
                    }
*/
                    m_mapImg[cell][c][0] = dispmap_t(val);
                    m_rowSum[cell][c][0]   += r;
                    m_rowSumSq[cell][c][0] += r*r;
                    //m_maxRow[cell][c][0] = max(m_maxRow[cell][c][0], float(r));
                    //m_minRow[cell][c][0] = min(m_minRow[cell][c][0], float(r));

                    if (val > m_mapMax)
                    {
                        m_mapMax = val;
                    }
                }
            }
        }

        MSG("mapMax = " << m_mapMax);

/*
        // finish calculating mean and variance for each cell
        for (int c = 0; c < sib->cols; c++)
        {
            for (int cell = 0; cell < m_mapHeight; cell++)
            {
                float weightSum = m_mapImg[cell][c][0];
                float mean = m_meanRow[cell][c][0] / weightSum;
                float var = m_varRow[cell][c][0] / weightSum - mean*mean;
                m_meanRow[cell][c][0] = mean;
                m_varRow[cell][c][0] = var;
            }
        }
*/

        //********************************************************
        // FIND BLOBS (connected regions) IN THE MAP
        //*******************************************************

        vector<Obstacle> obst;
        m_regions.clear();

        /* Use two thresholds to detect blobs. A blob is detected if all its cells
         * are above the low threshold and at least one is above the high threshold.
         * Method: first, threshold the image with the low threshold and create a mask
         * (done in the code above).
         * Then, scan for cells above the high threshold, and use cvFloodFill on the
         * mask to find the blob.
         */

        // work directly with IplImage from here on
        //CvMat* mask = &m_maskMat;
        IplImage* map = m_mapImg.getImage();

        //IplConvKernel* ball = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_ELLIPSE, NULL);
        //IplConvKernel* hLine = cvCreateStructuringElementEx(7, 1, 3, 0, CV_SHAPE_CROSS, NULL);

        //cvThreshold(map, mask, m_lowThres, numeric_limits<uint8_t>::max(),
        //            CV_THRESH_BINARY);

        //cvErode(mask, mask, hLine, 1);
        //cvDilate(mask, mask, ball, 1);

        //cvReleaseStructuringElement(&hLine);
        //cvReleaseStructuringElement(&ball);

        int height = m_mapImg.getHeight();
        int width = m_mapImg.getWidth();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                dispmap_t val = m_mapImg[r][c][0];
                if (val > m_highThres)
                {
                    dispmap_t loDiff = val - m_lowThres;
                    dispmap_t hiDiff = m_mapMax - val + 1;
                    if (hiDiff <= 0) {
                        hiDiff = 0;
                        MSG("Found map value " << val << " bigger than mapMax = " << m_mapMax);
                    }
                    CvConnectedComp comp;

                    if (m_maskImg[r+1][c+1][0] != 0)
                        continue;

                    cvFloodFill(map, cvPoint(c, r), cvRealScalar(1),
                                cvRealScalar(loDiff), cvRealScalar(hiDiff),
                                &comp, CV_FLOODFILL_FIXED_RANGE | CV_FLOODFILL_MASK_ONLY | 4,
                                m_maskImg.getImage());
                    
                    Obstacle ob;
                    ob.setState(sib->state);
                    //CvMat* points = NULL;

                    if (comp.rect.width > 0 && comp.rect.height > 0) {
                        RegionData data;
                        CvMat subRect;
                        data.rect = comp.rect;
                        vector<int32_t> dbgXv, dbgYv;
                        if (m_debug) {
                            data.debugXv = &dbgXv;
                            data.debugYv = &dbgYv;
                        }

                        // Rect must be 1 pix wider in each direction than the blob,
                        // but m_maskImg is 1pix wider than mask already, so need only
                        // to set the width and height
                        comp.rect.width += 2;
                        comp.rect.height += 2;
                        
                        cvGetSubRect(m_maskImg.getImage(), &subRect, comp.rect);
                        //filterRegion(&subRect, comp.rect.x, comp.rect.y, &data);
                        scanRegion(&subRect, comp.rect.x, comp.rect.y, &data);
                        if (m_debug) {
                            m_dbgEdgesX.push_back(dbgXv);
                            m_dbgEdgesY.push_back(dbgYv);
                        }
                        // substitute all 1's with 255's, so getMaskEdgePoints won't
                        // consider them the next time (and so they will be rendered
                        // correctly).
                        uint8_t* start = (uint8_t*) subRect.data.ptr;
                        for (int r = 1; r < subRect.rows - 1; r++) {
                            for (int c = 1; c < subRect.cols - 1; c++) {
                                uint8_t* p = start + r * subRect.step + c;
                                if (*p == 1)
                                    *p = 255;
                            }
                        }
                        pointsToObstacle(sib, data, &ob.getVertices());
                        // make sure that other internal variables are setup correctly
                        ob.setNumPoints(ob.getVertices().size());
                        obst.push_back(ob);
                        m_regions.push_back(data);
                    } else {
                        ERRMSG("BUGBUG: cvFloodFill() did nothing, was the seed point masked???");
                        ERRMSG("BUGBUG: seed point: (c=" << c << ", r=" << r << ")" );
                    }
                }
            }
        }

        // calculate transformation matrix from image to sensor in homogeneous
        // coordinates (col, row, disp, 1) -> (Xs, Ys, Zs, Ws)
        float sx = sib->leftCamera.sx;
        float sy = sib->leftCamera.sy;
        float sxOverSy = sx/sy;
        float cx = sib->leftCamera.cx;
        float cy = sib->leftCamera.cy;
        float b = sib->baseline;
        // (Xs, Ys, Zs, Ws)' = himg2sens * (col, row, disp, 1)'
        const float himg2sens[4][4] = {
            {b, 0,          0, -b*cx},
            {0, b*sxOverSy, 0, -b*cy*sxOverSy},
            {0, 0,          0,  b*sx},
            {0, 0,          1,  0}
        };

        if (m_debug) {
            m_result = obst;
            for (unsigned int i = 0; i < m_result.size(); i++)
            {
                vector<point4>& v = m_result[i].getVertices();
                for (unsigned int j = 0; j < v.size(); j++)
                {
                    // convert to vehicle frame for visualization
                    point4 tmp;
                    StereoImageBlobSensorToVehicle(sib, v[j][0], v[j][1], v[j][2],
                                                   &tmp[0], &tmp[1], &tmp[2]);
                    tmp[3] = 0;
                    v[j] = tmp;
                }
            }
        }

        float sens2loc[4][4];
        mat44f_mul(sens2loc, sib->veh2loc, sib->leftCamera.sens2veh);
        float cov[4][4];

        // convert from vehicle frame to local frame, and filter small obstacles
        for (unsigned int i = 0; i < obst.size(); i++)
        {
            //Obstacle o = obst[i];
            //vector<point4>& v = o.getVertices();
            //vector<point3>& var = o.getVariance();
            vector<point4>& v = obst[i].getVertices();
            vector<point3>& var = obst[i].getVariance();
            m_regions[i].filteredOut = false;

            // filter small obstacles
            switch (m_smallObsMethod)
            {
            case SMALLOBS_IMAGE_BBOX: {
                float stdev = sqrt(m_regions[i].varRow);
                //float top = m_regions[i].minRow;
                //float bottom = m_regions[i].maxRow;
                float left = m_regions[i].rect.x;
                float right = left + m_regions[i].rect.width;
                if (fabs(/*(bottom - top)*/1.5*stdev * (right - left)) < m_smallObsThres) {
                    MSG("Filtering out obstacle " << i << ", image area was fabs("
                        << /*(bottom - top)*/1.5*stdev * (right - left) << ") < " << m_smallObsThres);
                    MSG("\tstdev=" << stdev << ", left=" << left << ", right=" << right);
                    m_regions[i].filteredOut = true;
                }
                break;
            }

            case SMALLOBS_DISPSPC_SUM: {
                if (m_regions[i].sum < m_smallObsThres) {
                    MSG("Filtering out obstacle " << i << ", map sum was "
                        << m_regions[i].sum << ") < " << m_smallObsThres);
                    m_regions[i].filteredOut = true;
                }
                break;
            }

            default:
                ERRMSG("The selected small obstacle removal method is not implemented! (" << m_smallObsMethod << ")");
                break;
            }

            if (m_regions[i].filteredOut)
                continue;

            //point4 center = o.getCenter();
            point4 center = obst[i].getCenter();

            for (unsigned int j = 0; j < v.size(); j++)
            {
                // calculate covariance. Do this before transforming the point to local frame
                // because we need the coodrinates in sensor frame
                calcPointCov(cov, v[j], himg2sens, sens2loc);
                // take only the diagonal
                var[j][0] = cov[0][0];
                var[j][1] = cov[1][1];
                var[j][2] = cov[2][2];

                // transform from sensor frame to local frame
/*
   ==> Enable this code (which should be faster) after testing with the "good old one"
                point4 tmp;
                for (int r = 0; r < 4; r++)
                {
                    tmp[r] = sens2loc[r][0] * v[j][0] + sens2loc[r][1] * v[j][1]
                           + sens2loc[r][2] * v[j][2] + sens2loc[r][3] * v[j][3];
                }
                float inv3 = 1.0/tmp[3]; // actually, tmp[3] should be == 1 ...
                v[j][0] = tmp[0]*inv3;
                v[j][1] = tmp[1]*inv3;
                v[j][2] = tmp[2]*inv3;
                v[j][3] = 1; //tmp[3]*inv3;
*/
                float xv, yv, zv;
                StereoImageBlobSensorToVehicle(sib, v[j][0], v[j][1], v[j][2], &xv, &yv, &zv);
                StereoImageBlobVehicleToLocal(sib, xv, yv, zv, &v[j][0], &v[j][1], &v[j][2]);
                v[j][3] = 1;
            }
            // calculate the variance of the center point
            calcPointCov(obst[i].centerCov, center, himg2sens, sens2loc);
            //calcPointCov(o.centerCov, center, himg2sens, sens2loc);
            //finalResult.push_back(o);
        }

        // final resulting vector after filtering and frame conversion
        vector<Obstacle> finalResult;
        finalResult.reserve(obst.size());
        for (unsigned int i = 0; i < obst.size(); i++) {
            if (!m_regions[i].filteredOut) {
                obst[i].setId(m_regions[i].id);
                finalResult.push_back(obst[i]);
            }
        }

        //********************************************************
        // ASSOCIATE BLOBS WITH THE PREVIOUS SCAN
        //*******************************************************

#warning "Implement/fix data association and tracking"
        trackObstacles(sib->timestamp);

        MSG("detect(): returning " << finalResult.size() << " obstacles, "
            << obst.size() - finalResult.size() << " filtered out");
        
        return finalResult;
       
    }

    void DisparityDetector::trackObstacles(uint64_t tstamp)
    {
        // calculate center points for all the regions
        for (unsigned int j = 0; j < m_regions.size(); j++)
        {
            if (m_regions[j].filteredOut == true)
                continue;
            m_regions[j].xc = m_regions[j].rect.x + m_regions[j].rect.width*0.5;
            m_regions[j].yc = m_regions[j].rect.y + m_regions[j].rect.height*0.5;
        }


        vector<vector<float> > assoc(m_tracks.size());
        for (unsigned int i = 0; i < assoc.size(); i++) {
            assoc[i].resize(m_regions.size());
            fill(assoc[i].begin(), assoc[i].end(), -1);
        }

        double timediff = double(tstamp - m_lastTs) * 1e-6;
        double thres = m_assocThres * timediff;
        m_lastTs = tstamp;
        
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted)
                continue;
            for (unsigned int j = 0; j < m_regions.size(); j++)
            {
                if (m_regions[j].filteredOut == true)
                    continue;
                // TODO: filter the data, and use the covariance
                // in the filtered data to associate the measures
                float dx = m_tracks[i].st.col - m_regions[j].xc;
                float dd = m_tracks[i].st.disp - m_regions[j].yc;
                float dist = dx*dx/m_errorVar[0] + dd*dd/m_errorVar[1];
                if (dist < thres) {
                    assoc[i][j] = dist;
                    MSG("trackObstacles(): dist=" << dist << " < " << thres
                        << ", associating track " << i << " with measure " << j);
                }
            }
        }

        // save non associated tracks
        vector<int> nonAssoc(0);
        nonAssoc.reserve(m_tracks.size());

        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted) {
                nonAssoc.push_back(i);
                continue;
            }

            //int nAssoc = 0;
            int bestAssoc = -1;
            float bestAssocDist = numeric_limits<float>::max();
            float xSum = 0;
            float ySum = 0;
            float weightSum = 0;
            for (unsigned int j = 0; j < m_regions.size(); j++)
            {
                if (m_regions[j].filteredOut == true)
                    continue;
                // TODO: use the variance of the filtered data (once we have that)
                // to calculate the new mean
                if (assoc[i][j] >= 0) {
                    float weight = exp(-0.5 * assoc[i][j]);
                    weightSum += weight;
                    xSum += m_regions[j].xc * weight;
                    ySum += m_regions[j].yc * weight;
                    if (bestAssocDist > assoc[i][j] || bestAssoc == -1)
                    {
                        bestAssocDist = assoc[i][j];
                        bestAssoc = j;
                    }
                    // The value doesn't really matter, as long as it's != -1.
                    // The same i-th region may be actually associated with more than
                    // one track.
                    m_regions[j].id = 1;//m_tracks[i].id;
                }
            }
#warning "get rid of magic numbers here"
            if (bestAssoc >= 0) {
                float xc = m_tracks[i].st.col;
                float disp = m_tracks[i].st.disp;
                // use data from the best association for shape information
                float stdev = sqrt(m_regions[bestAssoc].varRow);
                m_tracks[i].top = m_regions[bestAssoc].meanRow - stdev*1.5;
                m_tracks[i].bottom = m_regions[bestAssoc].meanRow + stdev*1.5;
                m_tracks[i].left = m_regions[bestAssoc].rect.x ;
                m_tracks[i].right = m_regions[bestAssoc].rect.x
                    + m_regions[bestAssoc].rect.width;

                // simple exponential decay filter for position, with decay = 0.5
                // use the mean of the measures as "the measure"
                float xMean = xSum / weightSum;
                float yMean = ySum / weightSum;
                m_tracks[i].st.col = (xc + xMean)*0.5;
                m_tracks[i].st.disp = (disp + yMean)*0.5;

                // fix statistics
                float xdiff = m_tracks[i].st.col - xc;
                float ydiff = m_tracks[i].st.disp - disp;
                m_tracks[i].st.col += xdiff;
                m_tracks[i].st.disp += ydiff;

                // TODO: fix edges (or directly the convex hull, better) and meanHeight
                
                // same simple exponential filter, uncertainty halfes every frame
                // with some associations
                m_tracks[i].likelihood = 1.0 - (1.0 - m_tracks[i].likelihood)*0.5;
            } else {
                // reduce likelihood for unassociated tracks, and remove them if it goes
                // below a threshold
                m_tracks[i].likelihood *= 0.8;
                if (m_tracks[i].likelihood < 0.1) { // MAGIC
                    m_tracks[i].deleted = true;
                    nonAssoc.push_back(i);
                }
            }
        }

        // create new tracks for non associated regions
        for (unsigned int j = 0; j < m_regions.size(); j++)
        {
            if (m_regions[j].filteredOut == true)
                continue;
            if (m_regions[j].id == -1)
            {
                int i;
                Track* t = NULL;
                // reuse a deleted track, if possible, else add a new one
                if (nonAssoc.size() > 0) {
                    i = nonAssoc.back();
                    t = &m_tracks[i];
                    nonAssoc.pop_back();
                } else {
                    m_tracks.push_back(Track());
                    t = &m_tracks.back();
                    t->id = m_tracks.size()-1;
                }
                t->deleted = false;
                memset(t->state, 0, sizeof(t->state));
                t->st.col = m_regions[j].xc;
                t->st.row = m_regions[j].yc;
                float disp = m_regions[j].rect.y + m_regions[j].rect.height/2;
                t->st.disp = disp;
                memset(t->var, 0, sizeof(t->var)); // not used yet
                t->likelihood = 0.4;
                float stdev = sqrt(m_regions[j].varRow);
                t->top = m_regions[j].meanRow - stdev*1.5;
                t->bottom = m_regions[j].meanRow + stdev*1.5;
                t->left = m_regions[j].rect.x;
                t->right = t->left + m_regions[j].rect.width;
            }
        }
    }

    //////////////////////////////////////////
    // OpenGL Debug Display stuff (GlRenderer)
    //////////////////////////////////////////

#if 1

    class DispDetImageRenderer : public ImageRenderer, virtual public GlRenderer
    {
        typedef ImageRenderer base_t;
        pthread_mutex_t& m_mtx;

    public:
        DispDetImageRenderer(const ImageCvBase& myImg, pthread_mutex_t& myMtx)
            : base_t(myImg), m_mtx(myMtx)
        { }

        virtual void update(const SensnetBlob& /*blob*/) throw()
        {
            MSG("DispDetImageRenderer::update() called");
            dirty = true;
            base_t::update();
        }

        void render() throw(Error)
        {
            AutoMutex am(m_mtx);
            base_t::render();
        }
        
    };

    /*
     * Same as the DispDetImageRenderer (which is just an ImageRenderer with a mutex),
     * but works with 1 or 2 channel images only, and scales the values as to use the
     * full dynamic range.
     */
    class DispDetMapRenderer : public ImageRenderer, virtual public GlRenderer
    {
        typedef ImageRenderer base_t;
        ImageCvBase m_img;
        const ImageCvBase& m_origImg;
        pthread_mutex_t& m_mtx;

    public:
        DispDetMapRenderer(const ImageCvBase& myImg, pthread_mutex_t& myMtx)
            : base_t(m_img), m_origImg(myImg), m_mtx(myMtx)
        { }

        virtual void update(const SensnetBlob& /*blob*/) throw()
        {
            MSG("DispDetMapRenderer::update() called");
            dirty = true;
            base_t::update();
        }

        void render() throw(Error)
        {
            MSG("DispDetMapRenderer::render() called");
            if (dirty) {
                IplImage* img;
                if (true) { // critical section
                    AutoMutex am(m_mtx);
                    img = cvCloneImage(m_origImg.getImage());
                }
                double minVal, maxVal;
                cvSetImageCOI(img, 0); // channel of interest
                cvMinMaxLoc(img, &minVal, &maxVal);
                MSG("DispDetMapRenderer::render(): minVal=" << minVal << ", maxVal=" << maxVal);
                cvSubS(img, cvRealScalar(minVal), img);
                if (maxVal != minVal)
                    cvScale(img, img, 1/(maxVal - minVal));
                m_img.init(img);
            }
            base_t::render();
        }
    };

#else
    class DispDetImageWidget : public Fl_Button, virtual public IUpdatable
    {

        typedef Fl_Button base_t;

        const ImageCvBase& img;
        pthread_mutex_t& mtx;
        Fl_Image* flImg;
        bool dirty;

        //Fl_Box* box;

    public:
        DispDetImageWidget(const ImageCvBase& myImg, pthread_mutex_t& myMtx,
                           int x, int y, int w, int h, const char* label)
            : base_t(x, y, w, h, label), img(myImg), mtx(myMtx), flImg(NULL),
              dirty(true)
        {
            /*
            begin();
            box = new Fl_Box(x, y, w, h, label);
            box->box(FL_NO_BOX);
            box->labeltype(_FL_ICON_LABEL);
            end();
            */
            box(FL_NO_BOX);
        }

        ~DispDetImageWidget()
        {
            if (flImg) {
                delete flImg;
                flImg = NULL;
            }
        }

        virtual void update(const SensnetBlob& /*blob*/) throw()
        {
            MSG("DispDetImageWidget::update() called");
            dirty = true;
            redraw();
        }

        void draw()
        {
            if (flImg)
                delete flImg;
            if (true) {
                AutoMutex am(mtx);
                flImg = ImageCvBase::toFlImage(img.getImage());
            }
            //box->image(flImg);
            image(flImg);

            base_t::draw();
            dirty = false;
        }

    };
#endif

    vector<Fl_Widget*> DisparityDetector::getWidgets()
    {
        shared_ptr<GlRenderer> irLeft(new SensnetImageRenderer(SensnetImageRenderer::LEFT));
        shared_ptr<ObstOverlayRenderer> overlayRend(new ObstOverlayRenderer(*this, false));
        shared_ptr<ObstOverlayRenderer> overlayRend2(new ObstOverlayRenderer(*this, true));

        shared_ptr<GlRenderList> rendList(new GlRenderList());
        rendList->addRenderer(irLeft);
        rendList->addRenderer(overlayRend);

        // add a second one that uses tracked regions instead of just measurements
        shared_ptr<GlRenderList> rendList2(new GlRenderList());
        rendList2->addRenderer(irLeft);
        rendList2->addRenderer(overlayRend2);

        vector<Fl_Widget*> widgets;
        widgets.push_back(new RenderWidget(rendList, 0, 0, 640, 480, "Left w/ obst"));
        widgets.push_back(new RenderWidget(rendList2, 0, 0, 640, 480, "Left w/ tracks"));
        shared_ptr<ObstacleRenderer> obstRend(new ObstacleRenderer(*this));
        widgets.push_back(new RenderWidget(obstRend, 0, 0, 640, 480, "Obstacles"));
        //shared_ptr<GlRenderer> maxRowRend(new DispDetMapRenderer(m_minRow, m_mtx));
        //widgets.push_back(new RenderWidget(maxRowRend, 0, 0, 640, 480, "MinRow"));
        return widgets;
    }

    void DisparityDetector::update(const SensnetBlob& blob) throw(Error)
    {
        (void) blob;
        dirty = true;
//#warning "REMOVE ME and m_dispMap"
    }

    void DisparityDetector::ObstOverlayRenderer::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        this->w = sib->cols;
        this->h = sib->rows;
        /* Obstacle information updated in DisparityDetector::detect() */ 
    }
    
    /// Shows the detected obstacles as overlay boxes.
    void DisparityDetector::ObstOverlayRenderer::render() throw(Error)
    {
        //AutoMutex am(self.m_mtx);
        LockingPtr<DisparityDetector> This(&self);

        vector<RegionData>& regions = This->m_regions;
        vector<Track>& tracks = This->m_tracks;

        if ((!useTracks && regions.size() > 0) ||
            (useTracks && tracks.size() > 0))
        {
            pushViewport();

            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/this->w, -2.0/this->h, 0);

            glDisable(GL_TEXTURE_2D);
            glDisable(GL_CULL_FACE);


            glDisable(GL_BLEND);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            unsigned int size = useTracks ? tracks.size() : regions.size();

            for (unsigned int i = 0; i < size; i++)
            {
                if (!useTracks && regions[i].filteredOut == true)
                    continue;
                if (useTracks && tracks[i].deleted)
                    continue;

                float x1, x2, y1, y2;
                if (useTracks) {
                    x1 = tracks[i].left;
                    x2 = tracks[i].right;
                    y1 = tracks[i].top;
                    y2 = tracks[i].bottom;
                } else {
                    float stdev = sqrt(regions[i].varRow);
                    x1 = regions[i].rect.x;
                    x2 = x1 + regions[i].rect.width;
                    y1 = regions[i].meanRow - stdev*1.5;
                    y2 = regions[i].meanRow + stdev*1.5;
                    //y1 = regions[i].minRow;
                    //y2 = regions[i].maxRow;
                }

                if (useTracks && tracks[i].likelihood < 0.5)
                    //glColor4f(0.0, 1.0, 1.0, 1.0);
                    continue;
                else
                    glColor4f(1.0, 0.0, 0.0, 1.0);
                glBegin(GL_POLYGON);

                glVertex3f(x1, y1, 0.0);
                glVertex3f(x2, y1, 0.0);
                glVertex3f(x2, y2, 0.0);
                glVertex3f(x1, y2, 0.0);

                glEnd();
/*
                MSG("Box" << i << ":"
                    << "(" << x1 << ", " << y1 << ") "
                    << "(" << x2 << ", " << y1 << ") "
                    << "(" << x2 << ", " << y2 << ") "
                    << "(" << x1 << ", " << y2 << ") "
                    );
*/
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            popViewport();
        }
    }

    void DisparityDetector::ObstacleRenderer::update(const SensnetBlob& /*blob*/) throw()
    {
        /* Obstacle information updated in DisparityDetector::detect() */ 
    }
    
    /// Shows the detected obstacles as overlay boxes.
    void DisparityDetector::ObstacleRenderer::render() throw(Error)
    {
        LockingPtr<DisparityDetector> This(&self);
        vector<Obstacle>& result = This->m_result;
        vector<RegionData>& regions = This->m_regions;
        if (result.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            //int vp[4];
            //glGetIntegerv(GL_VIEWPORT, vp);
            //glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            glDisable(GL_CULL_FACE);


            //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            float minx = -50;
            float maxx = 50;
            float miny = 0;
            float maxy = 100;

            //glTranslatef(0.0, -1.0, 0.0);
            //glScalef(0.02, 0.02, 0.03);
            glScalef(2.0/(maxx - minx), 2.0/(maxy - miny), 1.0);
            glTranslatef(-(minx + maxx)/2, -(miny + maxy)/2, 0.0);
            
            // draw x axis
            glColor4f(1.0, 1.0, 1.0, 1.0); // white
            glBegin(GL_LINES);
            glVertex3f((minx + maxx)/2, miny, 0.0);
            glVertex3f((minx + maxx)/2, maxy, 0.0);
            glEnd();

            // draw grid
            glColor4f(0.0, 1.0, 0.0, 1.0); // green
            for (float x = minx; x <= maxx; x += 10)
            {
                glBegin(GL_LINES);
                glVertex3f(x, miny, 0.0);
                glVertex3f(x, maxy, 0.0);
                glEnd();
            }

            for (float y = miny; y <= maxy; y += 10)
            {
                glBegin(GL_LINES);
                glVertex3f(minx, y, 0.0);
                glVertex3f(maxx, y, 0.0);
                glEnd();
            }

            for (unsigned int i = 0; i < result.size(); i++)
            {
                if (!regions[i].filteredOut)
                    glColor4f(1.0, 0.3, 0.3, 0.7); // bright red
                else
                    //glColor4f(0.0, 1.0, 1.0, 0.7); // cyan
                    continue;

                vector<point4>& vert = result[i].getVertices();
                //MSG("Poly" << i << ":");
                glBegin(GL_POLYGON);
                
                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    // invert x and y
                    glVertex3f(vert[j][1], vert[j][0], -1.0);
                    //cerr << "(" << vert[j][0] << "," << vert[j][1] << ") ";
                }

                glEnd(); 
                //cerr << endl;
                //MSG("Poly" << i << " END");
            }

            //glEnable(GL_CULL_FACE);
            //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            //glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
            popViewport();
        }
    }

#define DRAW_MAP 1
#define DRAW_MASK 0
#define DRAW_DEBUG_IMAGE 0 // is this used for anything anymore?
#define DRAW_EDGES 0 // actually, edge points
#define DRAW_OBSTACLES_DISPFRAME 1

    void DisparityDetector::render() throw(Error)
    {
        //AutoMutex am(m_mtx);

        // todo: check for errors!! (not a big deal actually, this is just for debugging)
        if (m_dispTex == 0 || m_mapTex == 0) {
            GLuint txt[4];
            glGenTextures(4, txt);
            m_dispTex = txt[0];
            m_mapTex = txt[1];
            m_maskTex = txt[2];
            m_debugTex = txt[3];
        }
        
        float mapScaleX = 1.0;
        float mapScaleY = 1.0;
        float maskScaleX = 1.0;
        float maskScaleY = 1.0;
        float debugScaleX = 1.0;
        float debugScaleY = 1.0;

        ////////////////////////////////////////////
        // Create the texture for the disparity map
        ////////////////////////////////////////////

        if (m_mapMax == 0) {
            m_mapMax = 1; // avoid divide-by-zero
        }

        if (m_mapImg.getWidth() > 0) {
            // create texture for the map, with luminance - alpha
            // TODO: move this in the class declaration to avoid reallocation
            scoped_array<uint8_t> mapImg(new uint8_t[m_mapImg.getWidth()*m_mapImg.getHeight()*2]);

            // maybe can be optimized ...
            int w = m_mapImg.getWidth();
            int h = m_mapImg.getHeight();
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    dispmap_t val = m_mapImg[r][c][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (/*val < 64 && val >= 0*/ true) {
                        //pix = val * 256 / 64; // this will be optimized by the compiler
                        pix = uint8_t(val * 256 / m_mapMax);
                        if (val == 0) {
                            alpha = 0;
                        }
                    } else {
                        pix = 255;
                    }
                    mapImg[(r * w + c)*2] = pix;
                    mapImg[(r * w + c)*2 + 1] = alpha;
                }
            }

            glBindTexture(GL_TEXTURE_2D, m_mapTex);
        
            // no interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, m_mapImg.getWidth(),
            //                  m_mapImg.getHeight(), GL_LUMINANCE_ALPHA,
            //                  GL_UNSIGNED_BYTE, mapImg.get());
            int width, height;
            int bpp = 2;
            int bpr = w * bpp;
            buildPlainTexture(m_mapImg.getWidth(), m_mapImg.getHeight(), bpp, bpr,
                              GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, mapImg.get(),
                              &width, &height);
            mapScaleX = float(m_mapImg.getWidth())/width;
            mapScaleY = float(m_mapImg.getHeight())/height;
        }

        CvMat sub;
        IplImage imgHdr;
        IplImage* mask = NULL;
        if (m_maskImg.getWidth() > 0) {
            cvGetSubRect(m_maskImg.getImage(), &sub,
                         cvRect(1, 1, m_maskImg.getWidth()-1, m_maskImg.getHeight()-1));
            mask = cvGetImage(&sub, &imgHdr);
        }
                
        if (m_maskImg.getWidth() > 0) {
            if (m_debug) {
                // check if m_maskImg is in fact binary (only 0 or 255 values)
                int nZero = 0;
                int nMax = 0;
                int nOther = 0;
                int otherVal = 0;
                for(int r = 1; r < m_maskImg.getHeight()-1; r++) {
                    for(int c = 1; c < m_maskImg.getWidth()-1; c++) {
                        int val = m_maskImg[r][c][0];
                        if (val == 0) {
                            nZero ++;
                        } else if (val == 255) {
                            nMax ++;
                        } else {
                            nOther++;
                            if (otherVal == 0) 
                                otherVal = m_maskImg[r][c][0];
                            //m_maskImg[r][c][0] = 255;
                        }
                        //if (val == 255)
                        //    cerr << 'O';
                        //else 
                        //    cerr << '.';
                    }
                    //cerr << '\n';
                }
                MSG("Mask: nZero = " << nZero << ", nMax = " << nMax);
                if (nOther > 0)
                    MSG("Found " << nOther << " values that are not 0 or 255 (val=" << otherVal << ")");
            }

            CvSize sz = ImageCvBase::toGlTexture(mask, m_maskTex, GL_ALPHA);
            maskScaleX = float(m_maskImg.getWidth())/sz.width;
            maskScaleY = float(m_maskImg.getHeight())/sz.height;
        }

        if (m_debug && m_debugImg.getWidth() > 0) {
            CvSize sz = m_debugImg.toGlTexture(m_debugTex);
            debugScaleX = float(m_maskImg.getWidth())/sz.width;
            debugScaleY = float(m_maskImg.getHeight())/sz.height;
        }

        glEnable(GL_TEXTURE_2D);

#if DRAW_MAP
        ////////////////////////////////////////////
        // Draw the disparity map
        ////////////////////////////////////////////

        pushViewport();
        glColor3f(0.0, 0.0, 0.5); // dark blue
        glBindTexture(GL_TEXTURE_2D, m_mapTex);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

        //glTranslatef(1.0, 0.0, 0.0);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(mapScaleX, mapScaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (1.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

        
#endif
        // no depth text, we want the next images to overlap and blend
        glDisable (GL_DEPTH_TEST);
        // enable blending
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if DRAW_MASK
        ////////////////////////////////////////////
        // Draw the mask
        ////////////////////////////////////////////

        pushViewport();
        //glTranslatef(1.0, 0.0, 0.0);

        glColor3f(1.0, 0.0, 0.0); // red
        glBindTexture(GL_TEXTURE_2D, m_maskTex);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(maskScaleX, maskScaleY, 1.0);


        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (1.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix(); // texture
        popViewport();
#endif

#if DRAW_DEBUG_IMAGE
        if (m_debug) {
            ////////////////////////////////////////////
            // Draw the debug image
            ////////////////////////////////////////////

            pushViewport();
            //glTranslatef(1.0, 0.0, 0.0);

            glColor4f(0.0, 1.0, 0.0, 1.0); // green
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, m_debugTex);

            glMatrixMode(GL_TEXTURE);
            glPushMatrix();
            glScalef(debugScaleX, debugScaleY, 1.0);

            glBegin (GL_QUADS);
            glTexCoord2f (0.0, 0.0);
            glVertex3f (-1.0, 1.0, 0.0);

            glTexCoord2f (1.0, 0.0);
            glVertex3f (1.0, 1.0, 0.0);

            glTexCoord2f (1.0, 1.0);
            glVertex3f (1.0, -1.0, 0.0);

            glTexCoord2f (0.0, 1.0);
            glVertex3f (-1.0, -1.0, 0.0);
            glEnd ();

            glPopMatrix(); // texture
            popViewport();
        }
#endif

#if DRAW_OBSTACLES_DISPFRAME
        if (m_dbgEdgesX.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            //int vp[4];
            //glGetIntegerv(GL_VIEWPORT, vp);
            //glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            // image coordinates
            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/mask->width, -2.0/mask->height, 1.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_CULL_FACE);

#if DRAW_EDGES
            glColor4f(1.0, 0.0, 1.0, 1.0); // magenta

            for (unsigned int i = 0; i < m_dbgEdgesX.size(); i++)
            {
                vector<int32_t> xv = m_dbgEdgesX[i];
                vector<int32_t> yv = m_dbgEdgesY[i];

                glBegin(GL_POINTS);
                for (unsigned int j = 0; j < xv.size(); j++)
                {
                    glVertex3f(xv[j], yv[j], 1.0);
                }
                glEnd();
            }
#endif

#if 1
            for (unsigned int i = 0; i < m_dbgHullX.size(); i++)
            {
                vector<int32_t> xv = m_dbgHullX[i];
                vector<int32_t> yv = m_dbgHullY[i];
                
                if (m_regions[i].filteredOut) 
                    glColor3f(0.0, 0.0, 1.0);
                else
                    glColor3f(1.0, 0.0, 0.0);

                glBegin(GL_POLYGON);
                for (unsigned int j = 0; j < xv.size(); j++)
                {
                    glVertex3f(xv[j], yv[j], 1.0);
                }
                glEnd();
            }
#endif
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            //glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
        }
#endif

        glDisable (GL_BLEND);
        glEnable (GL_DEPTH_TEST);

    }
    
}
