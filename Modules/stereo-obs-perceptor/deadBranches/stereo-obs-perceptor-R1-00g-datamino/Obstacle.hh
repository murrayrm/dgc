#ifndef __BLOBLADAR_OBSTACLE_HH__
#define __BLOBLADAR_OBSTACLE_HH__

#include <vector>

#include <interfaces/VehicleState.h>
#include <frames/point2_uncertain.hh>
#include "util.hh"

namespace blobstereo {

    using std::vector;

    struct Obstacle
    {
        int id;
        vector<point4> vertices; // meters
        vector<point3> variance;

        // center point, and mean height
        bool hasCenter;
        point4 center;
        
        point2_uncertain speed; // m/s

        float likelihood; // 0 = this obstacle is not there, 0<x<1 = maybe it's there, 1 = it's definitely there

        VehicleState state;

        float centerCov[4][4]; // and covariance


        Obstacle()
            : hasCenter(false), speed(0, 0), likelihood(1)
        {
            memset(&state, 0, sizeof(state));
            fill(center.v, center.v + 4, -1.0);
            fill(*centerCov, *centerCov + 4*4, -1.0);
        }

        /* Getters and Setters */

        int getId() const { return id; }
        void setId(int id) { this->id = id; }

        vector<point4>& getVertices()
        {
            return vertices;
        }

        const vector<point4>& getVertices() const
        {
            return vertices;
        }

        vector<point3>& getVariance()
        {
            return variance;
        }

        const vector<point3>& getVariance() const
        {
            return variance;
        }

        int getNumPoints() const
        {
            return vertices.size();
        }

        void setNumPoints(int n)
        {
            vertices.resize(n);
            variance.resize(n);
        }

        point4& getCenter()
        {
            if (!hasCenter) {
                calculateCenter();
            }
            return center;
        }

        const point4& getCenter() const
        {
            assert(hasCenter);
            return center;
        }

        void calculateCenter()
        {
            point4 sum = { {0, 0, 0, 0} };
            int num = getNumPoints();
            for (int i = 0; i < num; i++)
            {
                sum[0] += vertices[i][0];
                sum[1] += vertices[i][1];
                sum[2] += vertices[i][2];
                assert(fabs(vertices[i][3] - 1.0) < 1e-6);
            }
            center[0] = sum[0]/num;
            center[1] = sum[1]/num;
            center[2] = sum[2]/num;
            center[3] = 1;
            hasCenter = true;
        }

        void setCenter(point4 center)
        {
            hasCenter = true;
            this->center = center;
        }

        point2_uncertain& getSpeed()
        {
            return speed;
        }

        const point2_uncertain& getSpeed() const
        {
            return speed;
        }

        float getLikelihood() const
        {
            return likelihood;
        }

        void setLikelihood(float lh)
        {
            likelihood = lh;
        }

        VehicleState& getState()
        {
            return state;
        }

        const VehicleState& getState() const
        {
            return state;
        }

        void setState(const VehicleState& state)
        {
            this->state = state;
        }

        // ... anything else?

    };

} // end namespace

#endif
