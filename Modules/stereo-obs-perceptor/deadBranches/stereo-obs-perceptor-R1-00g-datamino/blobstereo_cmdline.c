/*
  File autogenerated by gengetopt version 2.16
  generated with the following command:
  gengetopt -i blobstereo_cmdline.ggo --conf-parser --unamed-opts= -F blobstereo_cmdline 

  The developers of gengetopt consider the fixed text that goes in all
  gengetopt output files to be in the public domain:
  we make no copyright claims on it.
*/

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "getopt.h"

#include "blobstereo_cmdline.h"

static
void clear_given (struct gengetopt_args_info *args_info);
static
void clear_args (struct gengetopt_args_info *args_info);

static int
cmdline_parser_internal (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required, const char *additional_error);

struct line_list
{
  char * string_arg;
  struct line_list * next;
};

static struct line_list *cmd_line_list = 0;
static struct line_list *cmd_line_list_tmp = 0;


static char *
gengetopt_strdup (const char *s);

static
void clear_given (struct gengetopt_args_info *args_info)
{
  args_info->help_given = 0 ;
  args_info->version_given = 0 ;
  args_info->spread_daemon_given = 0 ;
  args_info->skynet_key_given = 0 ;
  args_info->module_id_given = 0 ;
  args_info->sensor_id_given = 0 ;
  args_info->opengl_given = 0 ;
  args_info->disable_console_given = 0 ;
  args_info->debug_given = 0 ;
  args_info->max_disp_given = 0 ;
  args_info->map_height_given = 0 ;
  args_info->high_thres_given = 0 ;
  args_info->low_thres_given = 0 ;
  args_info->small_obs_thres_given = 0 ;
  args_info->error_var_given = 0 ;
}

static
void clear_args (struct gengetopt_args_info *args_info)
{
  args_info->spread_daemon_arg = gengetopt_strdup ("4083@localhost");
  args_info->spread_daemon_orig = NULL;
  args_info->skynet_key_orig = NULL;
  args_info->module_id_arg = gengetopt_strdup ("MODstereoObsPerceptorLong");
  args_info->module_id_orig = NULL;
  args_info->sensor_id_arg = gengetopt_strdup ("SENSNET_MF_LONG_STEREO");
  args_info->sensor_id_orig = NULL;
  args_info->opengl_flag = 0;
  args_info->disable_console_flag = 0;
  args_info->debug_arg = 1;
  args_info->debug_orig = NULL;
  args_info->max_disp_arg = 65;
  args_info->max_disp_orig = NULL;
  args_info->map_height_arg = 300;
  args_info->map_height_orig = NULL;
  args_info->high_thres_arg = 8;
  args_info->high_thres_orig = NULL;
  args_info->low_thres_arg = 3;
  args_info->low_thres_orig = NULL;
  args_info->small_obs_thres_arg = 40;
  args_info->small_obs_thres_orig = NULL;
  args_info->error_var_arg = gengetopt_strdup ("2 4 2");
  args_info->error_var_orig = NULL;
  
}

void
cmdline_parser_print_version (void)
{
  printf ("%s %s\n", CMDLINE_PARSER_PACKAGE, CMDLINE_PARSER_VERSION);
}

void
cmdline_parser_print_help (void)
{
  cmdline_parser_print_version ();
  printf("\n%s\n", "Reads stereo blobs and detects obstacles using the disparity information");
  printf("\nUsage: Stereo Obstacle Perceptor [OPTIONS]... []...\n\n");
  printf("%s\n","  -h, --help                   Print help and exit");
  printf("%s\n","  -V, --version                Print version and exit");
  printf("%s\n","\nBasic options:");
  printf("%s\n","  -d, --spread-daemon=STRING   Spread daemon  (default=`4083@localhost')");
  printf("%s\n","  -S, --skynet-key=INT         Skynet key");
  printf("%s\n","  -m, --module-id=STRING       Module id, should be one of \n                                 MODstereoObsPerceptorXXX, where XXX is Long, \n                                 Medium or PTU  \n                                 (default=`MODstereoObsPerceptorLong')");
  printf("%s\n","  -s, --sensor-id=STRING       Sensor id, usually SENSNET_MF_LONG_STEREO or \n                                 SENSNET_MF_MEDIUM_STEREO (PTU? we'll see when \n                                 we have it)  \n                                 (default=`SENSNET_MF_LONG_STEREO')");
  printf("%s\n","  -g, --opengl                 Show OpenGL debug display  (default=off)");
  printf("%s\n","  -b, --disable-console        Disable console display (batch mode)  \n                                 (default=off)");
  printf("%s\n","\nAdvanced tuning options:");
  printf("%s\n","      --debug=INT              Turn on some debugging code  (default=`1')");
  printf("%s\n","      --max-disp=INT           Maximum expected disparity (bigger values will \n                                 be ignored)  (default=`65')");
  printf("%s\n","      --map-height=INT         Number of rows used in the 'map' (accumulation \n                                 buffer)  (default=`300')");
  printf("%s\n","      --high-thres=FLOAT       High threshold. Used to detect peaks in the map  \n                                 (default=`8')");
  printf("%s\n","      --low-thres=FLOAT        Low threshold. Used to flood fill peaks found \n                                 with the high threshold  (default=`3')");
  printf("%s\n","      --small-obs-thres=FLOAT  Minimum number of votes (disparity image pixels) \n                                 for a blob to be detected  (default=`40')");
  printf("%s\n","      --error-var=STRING       Variance of the error for the three coordinates \n                                 (col, row, disparity). It should be a string \n                                 with 3 values (covariance assumed to be \n                                 diagonal)  (default=`2 4 2')");
  
}

void
cmdline_parser_init (struct gengetopt_args_info *args_info)
{
  clear_given (args_info);
  clear_args (args_info);

  args_info->inputs = NULL;
  args_info->inputs_num = 0;
}

static void
cmdline_parser_release (struct gengetopt_args_info *args_info)
{
  
  unsigned int i;
  if (args_info->spread_daemon_arg)
    {
      free (args_info->spread_daemon_arg); /* free previous argument */
      args_info->spread_daemon_arg = 0;
    }
  if (args_info->spread_daemon_orig)
    {
      free (args_info->spread_daemon_orig); /* free previous argument */
      args_info->spread_daemon_orig = 0;
    }
  if (args_info->skynet_key_orig)
    {
      free (args_info->skynet_key_orig); /* free previous argument */
      args_info->skynet_key_orig = 0;
    }
  if (args_info->module_id_arg)
    {
      free (args_info->module_id_arg); /* free previous argument */
      args_info->module_id_arg = 0;
    }
  if (args_info->module_id_orig)
    {
      free (args_info->module_id_orig); /* free previous argument */
      args_info->module_id_orig = 0;
    }
  if (args_info->sensor_id_arg)
    {
      free (args_info->sensor_id_arg); /* free previous argument */
      args_info->sensor_id_arg = 0;
    }
  if (args_info->sensor_id_orig)
    {
      free (args_info->sensor_id_orig); /* free previous argument */
      args_info->sensor_id_orig = 0;
    }
  if (args_info->debug_orig)
    {
      free (args_info->debug_orig); /* free previous argument */
      args_info->debug_orig = 0;
    }
  if (args_info->max_disp_orig)
    {
      free (args_info->max_disp_orig); /* free previous argument */
      args_info->max_disp_orig = 0;
    }
  if (args_info->map_height_orig)
    {
      free (args_info->map_height_orig); /* free previous argument */
      args_info->map_height_orig = 0;
    }
  if (args_info->high_thres_orig)
    {
      free (args_info->high_thres_orig); /* free previous argument */
      args_info->high_thres_orig = 0;
    }
  if (args_info->low_thres_orig)
    {
      free (args_info->low_thres_orig); /* free previous argument */
      args_info->low_thres_orig = 0;
    }
  if (args_info->small_obs_thres_orig)
    {
      free (args_info->small_obs_thres_orig); /* free previous argument */
      args_info->small_obs_thres_orig = 0;
    }
  if (args_info->error_var_arg)
    {
      free (args_info->error_var_arg); /* free previous argument */
      args_info->error_var_arg = 0;
    }
  if (args_info->error_var_orig)
    {
      free (args_info->error_var_orig); /* free previous argument */
      args_info->error_var_orig = 0;
    }
  
  for (i = 0; i < args_info->inputs_num; ++i)
    free (args_info->inputs [i]);
  
  if (args_info->inputs_num)
    free (args_info->inputs);
  
  clear_given (args_info);
}

int
cmdline_parser_file_save(const char *filename, struct gengetopt_args_info *args_info)
{
  FILE *outfile;
  int i = 0;

  outfile = fopen(filename, "w");

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot open file for writing: %s\n", CMDLINE_PARSER_PACKAGE, filename);
      return EXIT_FAILURE;
    }

  if (args_info->help_given) {
    fprintf(outfile, "%s\n", "help");
  }
  if (args_info->version_given) {
    fprintf(outfile, "%s\n", "version");
  }
  if (args_info->spread_daemon_given) {
    if (args_info->spread_daemon_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "spread-daemon", args_info->spread_daemon_orig);
    } else {
      fprintf(outfile, "%s\n", "spread-daemon");
    }
  }
  if (args_info->skynet_key_given) {
    if (args_info->skynet_key_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "skynet-key", args_info->skynet_key_orig);
    } else {
      fprintf(outfile, "%s\n", "skynet-key");
    }
  }
  if (args_info->module_id_given) {
    if (args_info->module_id_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "module-id", args_info->module_id_orig);
    } else {
      fprintf(outfile, "%s\n", "module-id");
    }
  }
  if (args_info->sensor_id_given) {
    if (args_info->sensor_id_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "sensor-id", args_info->sensor_id_orig);
    } else {
      fprintf(outfile, "%s\n", "sensor-id");
    }
  }
  if (args_info->opengl_given) {
    fprintf(outfile, "%s\n", "opengl");
  }
  if (args_info->disable_console_given) {
    fprintf(outfile, "%s\n", "disable-console");
  }
  if (args_info->debug_given) {
    if (args_info->debug_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "debug", args_info->debug_orig);
    } else {
      fprintf(outfile, "%s\n", "debug");
    }
  }
  if (args_info->max_disp_given) {
    if (args_info->max_disp_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "max-disp", args_info->max_disp_orig);
    } else {
      fprintf(outfile, "%s\n", "max-disp");
    }
  }
  if (args_info->map_height_given) {
    if (args_info->map_height_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "map-height", args_info->map_height_orig);
    } else {
      fprintf(outfile, "%s\n", "map-height");
    }
  }
  if (args_info->high_thres_given) {
    if (args_info->high_thres_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "high-thres", args_info->high_thres_orig);
    } else {
      fprintf(outfile, "%s\n", "high-thres");
    }
  }
  if (args_info->low_thres_given) {
    if (args_info->low_thres_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "low-thres", args_info->low_thres_orig);
    } else {
      fprintf(outfile, "%s\n", "low-thres");
    }
  }
  if (args_info->small_obs_thres_given) {
    if (args_info->small_obs_thres_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "small-obs-thres", args_info->small_obs_thres_orig);
    } else {
      fprintf(outfile, "%s\n", "small-obs-thres");
    }
  }
  if (args_info->error_var_given) {
    if (args_info->error_var_orig) {
      fprintf(outfile, "%s=\"%s\"\n", "error-var", args_info->error_var_orig);
    } else {
      fprintf(outfile, "%s\n", "error-var");
    }
  }
  
  fclose (outfile);

  i = EXIT_SUCCESS;
  return i;
}

void
cmdline_parser_free (struct gengetopt_args_info *args_info)
{
  cmdline_parser_release (args_info);
  if (cmd_line_list)
    {
      /* free the list of a previous call */
      while (cmd_line_list) {
        cmd_line_list_tmp = cmd_line_list;
        cmd_line_list = cmd_line_list->next;
        free (cmd_line_list_tmp->string_arg);
        free (cmd_line_list_tmp);
      }
    }
}


/* gengetopt_strdup() */
/* strdup.c replacement of strdup, which is not standard */
char *
gengetopt_strdup (const char *s)
{
  char *result = NULL;
  if (!s)
    return result;

  result = (char*)malloc(strlen(s) + 1);
  if (result == (char*)0)
    return (char*)0;
  strcpy(result, s);
  return result;
}

int
cmdline_parser (int argc, char * const *argv, struct gengetopt_args_info *args_info)
{
  return cmdline_parser2 (argc, argv, args_info, 0, 1, 1);
}

int
cmdline_parser2 (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required)
{
  int result;

  result = cmdline_parser_internal (argc, argv, args_info, override, initialize, check_required, NULL);

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser_required (struct gengetopt_args_info *args_info, const char *prog_name)
{
  return EXIT_SUCCESS;
}

int
cmdline_parser_internal (int argc, char * const *argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required, const char *additional_error)
{
  int c;	/* Character of the parsed option.  */

  int error = 0;
  struct gengetopt_args_info local_args_info;

  if (initialize)
    cmdline_parser_init (args_info);

  cmdline_parser_init (&local_args_info);

  optarg = 0;
  optind = 0;
  opterr = 1;
  optopt = '?';

  while (1)
    {
      int option_index = 0;
      char *stop_char;

      static struct option long_options[] = {
        { "help",	0, NULL, 'h' },
        { "version",	0, NULL, 'V' },
        { "spread-daemon",	1, NULL, 'd' },
        { "skynet-key",	1, NULL, 'S' },
        { "module-id",	1, NULL, 'm' },
        { "sensor-id",	1, NULL, 's' },
        { "opengl",	0, NULL, 'g' },
        { "disable-console",	0, NULL, 'b' },
        { "debug",	1, NULL, 0 },
        { "max-disp",	1, NULL, 0 },
        { "map-height",	1, NULL, 0 },
        { "high-thres",	1, NULL, 0 },
        { "low-thres",	1, NULL, 0 },
        { "small-obs-thres",	1, NULL, 0 },
        { "error-var",	1, NULL, 0 },
        { NULL,	0, NULL, 0 }
      };

      stop_char = 0;
      c = getopt_long (argc, argv, "hVd:S:m:s:gb", long_options, &option_index);

      if (c == -1) break;	/* Exit from `while (1)' loop.  */

      switch (c)
        {
        case 'h':	/* Print help and exit.  */
          cmdline_parser_print_help ();
          cmdline_parser_free (&local_args_info);
          exit (EXIT_SUCCESS);

        case 'V':	/* Print version and exit.  */
          cmdline_parser_print_version ();
          cmdline_parser_free (&local_args_info);
          exit (EXIT_SUCCESS);

        case 'd':	/* Spread daemon.  */
          if (local_args_info.spread_daemon_given)
            {
              fprintf (stderr, "%s: `--spread-daemon' (`-d') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->spread_daemon_given && ! override)
            continue;
          local_args_info.spread_daemon_given = 1;
          args_info->spread_daemon_given = 1;
          if (args_info->spread_daemon_arg)
            free (args_info->spread_daemon_arg); /* free previous string */
          args_info->spread_daemon_arg = gengetopt_strdup (optarg);
          if (args_info->spread_daemon_orig)
            free (args_info->spread_daemon_orig); /* free previous string */
          args_info->spread_daemon_orig = gengetopt_strdup (optarg);
          break;

        case 'S':	/* Skynet key.  */
          if (local_args_info.skynet_key_given)
            {
              fprintf (stderr, "%s: `--skynet-key' (`-S') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->skynet_key_given && ! override)
            continue;
          local_args_info.skynet_key_given = 1;
          args_info->skynet_key_given = 1;
          args_info->skynet_key_arg = strtol (optarg, &stop_char, 0);
          if (!(stop_char && *stop_char == '\0')) {
            fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
            goto failure;
          }
          if (args_info->skynet_key_orig)
            free (args_info->skynet_key_orig); /* free previous string */
          args_info->skynet_key_orig = gengetopt_strdup (optarg);
          break;

        case 'm':	/* Module id, should be one of MODstereoObsPerceptorXXX, where XXX is Long, Medium or PTU.  */
          if (local_args_info.module_id_given)
            {
              fprintf (stderr, "%s: `--module-id' (`-m') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->module_id_given && ! override)
            continue;
          local_args_info.module_id_given = 1;
          args_info->module_id_given = 1;
          if (args_info->module_id_arg)
            free (args_info->module_id_arg); /* free previous string */
          args_info->module_id_arg = gengetopt_strdup (optarg);
          if (args_info->module_id_orig)
            free (args_info->module_id_orig); /* free previous string */
          args_info->module_id_orig = gengetopt_strdup (optarg);
          break;

        case 's':	/* Sensor id, usually SENSNET_MF_LONG_STEREO or SENSNET_MF_MEDIUM_STEREO (PTU? we'll see when we have it).  */
          if (local_args_info.sensor_id_given)
            {
              fprintf (stderr, "%s: `--sensor-id' (`-s') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->sensor_id_given && ! override)
            continue;
          local_args_info.sensor_id_given = 1;
          args_info->sensor_id_given = 1;
          if (args_info->sensor_id_arg)
            free (args_info->sensor_id_arg); /* free previous string */
          args_info->sensor_id_arg = gengetopt_strdup (optarg);
          if (args_info->sensor_id_orig)
            free (args_info->sensor_id_orig); /* free previous string */
          args_info->sensor_id_orig = gengetopt_strdup (optarg);
          break;

        case 'g':	/* Show OpenGL debug display.  */
          if (local_args_info.opengl_given)
            {
              fprintf (stderr, "%s: `--opengl' (`-g') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->opengl_given && ! override)
            continue;
          local_args_info.opengl_given = 1;
          args_info->opengl_given = 1;
          args_info->opengl_flag = !(args_info->opengl_flag);
          break;

        case 'b':	/* Disable console display (batch mode).  */
          if (local_args_info.disable_console_given)
            {
              fprintf (stderr, "%s: `--disable-console' (`-b') option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
              goto failure;
            }
          if (args_info->disable_console_given && ! override)
            continue;
          local_args_info.disable_console_given = 1;
          args_info->disable_console_given = 1;
          args_info->disable_console_flag = !(args_info->disable_console_flag);
          break;


        case 0:	/* Long option with no short option */
          /* Turn on some debugging code.  */
          if (strcmp (long_options[option_index].name, "debug") == 0)
          {
            if (local_args_info.debug_given)
              {
                fprintf (stderr, "%s: `--debug' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->debug_given && ! override)
              continue;
            local_args_info.debug_given = 1;
            args_info->debug_given = 1;
            args_info->debug_arg = strtol (optarg, &stop_char, 0);
            if (!(stop_char && *stop_char == '\0')) {
              fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
              goto failure;
            }
            if (args_info->debug_orig)
              free (args_info->debug_orig); /* free previous string */
            args_info->debug_orig = gengetopt_strdup (optarg);
          }
          /* Maximum expected disparity (bigger values will be ignored).  */
          else if (strcmp (long_options[option_index].name, "max-disp") == 0)
          {
            if (local_args_info.max_disp_given)
              {
                fprintf (stderr, "%s: `--max-disp' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->max_disp_given && ! override)
              continue;
            local_args_info.max_disp_given = 1;
            args_info->max_disp_given = 1;
            args_info->max_disp_arg = strtol (optarg, &stop_char, 0);
            if (!(stop_char && *stop_char == '\0')) {
              fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
              goto failure;
            }
            if (args_info->max_disp_orig)
              free (args_info->max_disp_orig); /* free previous string */
            args_info->max_disp_orig = gengetopt_strdup (optarg);
          }
          /* Number of rows used in the 'map' (accumulation buffer).  */
          else if (strcmp (long_options[option_index].name, "map-height") == 0)
          {
            if (local_args_info.map_height_given)
              {
                fprintf (stderr, "%s: `--map-height' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->map_height_given && ! override)
              continue;
            local_args_info.map_height_given = 1;
            args_info->map_height_given = 1;
            args_info->map_height_arg = strtol (optarg, &stop_char, 0);
            if (!(stop_char && *stop_char == '\0')) {
              fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
              goto failure;
            }
            if (args_info->map_height_orig)
              free (args_info->map_height_orig); /* free previous string */
            args_info->map_height_orig = gengetopt_strdup (optarg);
          }
          /* High threshold. Used to detect peaks in the map.  */
          else if (strcmp (long_options[option_index].name, "high-thres") == 0)
          {
            if (local_args_info.high_thres_given)
              {
                fprintf (stderr, "%s: `--high-thres' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->high_thres_given && ! override)
              continue;
            local_args_info.high_thres_given = 1;
            args_info->high_thres_given = 1;
            args_info->high_thres_arg = (float)strtod (optarg, &stop_char);
            if (!(stop_char && *stop_char == '\0')) {
              fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
              goto failure;
            }
            if (args_info->high_thres_orig)
              free (args_info->high_thres_orig); /* free previous string */
            args_info->high_thres_orig = gengetopt_strdup (optarg);
          }
          /* Low threshold. Used to flood fill peaks found with the high threshold.  */
          else if (strcmp (long_options[option_index].name, "low-thres") == 0)
          {
            if (local_args_info.low_thres_given)
              {
                fprintf (stderr, "%s: `--low-thres' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->low_thres_given && ! override)
              continue;
            local_args_info.low_thres_given = 1;
            args_info->low_thres_given = 1;
            args_info->low_thres_arg = (float)strtod (optarg, &stop_char);
            if (!(stop_char && *stop_char == '\0')) {
              fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
              goto failure;
            }
            if (args_info->low_thres_orig)
              free (args_info->low_thres_orig); /* free previous string */
            args_info->low_thres_orig = gengetopt_strdup (optarg);
          }
          /* Minimum number of votes (disparity image pixels) for a blob to be detected.  */
          else if (strcmp (long_options[option_index].name, "small-obs-thres") == 0)
          {
            if (local_args_info.small_obs_thres_given)
              {
                fprintf (stderr, "%s: `--small-obs-thres' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->small_obs_thres_given && ! override)
              continue;
            local_args_info.small_obs_thres_given = 1;
            args_info->small_obs_thres_given = 1;
            args_info->small_obs_thres_arg = (float)strtod (optarg, &stop_char);
            if (!(stop_char && *stop_char == '\0')) {
              fprintf(stderr, "%s: invalid numeric value: %s\n", argv[0], optarg);
              goto failure;
            }
            if (args_info->small_obs_thres_orig)
              free (args_info->small_obs_thres_orig); /* free previous string */
            args_info->small_obs_thres_orig = gengetopt_strdup (optarg);
          }
          /* Variance of the error for the three coordinates (col, row, disparity). It should be a string with 3 values (covariance assumed to be diagonal).  */
          else if (strcmp (long_options[option_index].name, "error-var") == 0)
          {
            if (local_args_info.error_var_given)
              {
                fprintf (stderr, "%s: `--error-var' option given more than once%s\n", argv[0], (additional_error ? additional_error : ""));
                goto failure;
              }
            if (args_info->error_var_given && ! override)
              continue;
            local_args_info.error_var_given = 1;
            args_info->error_var_given = 1;
            if (args_info->error_var_arg)
              free (args_info->error_var_arg); /* free previous string */
            args_info->error_var_arg = gengetopt_strdup (optarg);
            if (args_info->error_var_orig)
              free (args_info->error_var_orig); /* free previous string */
            args_info->error_var_orig = gengetopt_strdup (optarg);
          }
          
          break;
        case '?':	/* Invalid option.  */
          /* `getopt_long' already printed an error message.  */
          goto failure;

        default:	/* bug: option not considered.  */
          fprintf (stderr, "%s: option unknown: %c%s\n", CMDLINE_PARSER_PACKAGE, c, (additional_error ? additional_error : ""));
          abort ();
        } /* switch */
    } /* while */




  cmdline_parser_release (&local_args_info);

  if ( error )
    return (EXIT_FAILURE);

  if (optind < argc)
    {
      int i = 0 ;

      args_info->inputs_num = argc - optind ;
      args_info->inputs =
        (char **)(malloc ((args_info->inputs_num)*sizeof(char *))) ;
      while (optind < argc)
        args_info->inputs[ i++ ] = gengetopt_strdup (argv[optind++]) ;
    }

  return 0;

failure:
  
  cmdline_parser_release (&local_args_info);
  return (EXIT_FAILURE);
}

#ifndef CONFIG_FILE_LINE_SIZE
#define CONFIG_FILE_LINE_SIZE 2048
#endif
#define ADDITIONAL_ERROR " in configuration file "

#define CONFIG_FILE_LINE_BUFFER_SIZE (CONFIG_FILE_LINE_SIZE+3)
/* 3 is for "--" and "=" */

char my_argv[CONFIG_FILE_LINE_BUFFER_SIZE+1];

int
cmdline_parser_configfile (char * const filename, struct gengetopt_args_info *args_info, int override, int initialize, int check_required)
{
  FILE* file;
  char linebuf[CONFIG_FILE_LINE_SIZE];
  int line_num = 0;
  int i, result, equal;
  char *fopt, *farg;
  char *str_index;
  size_t len, next_token;
  char delimiter;
  int my_argc = 0;
  char **my_argv_arg;
  char *additional_error;

  /* store the program name */
  cmd_line_list_tmp = (struct line_list *) malloc (sizeof (struct line_list));
  cmd_line_list_tmp->next = cmd_line_list;
  cmd_line_list = cmd_line_list_tmp;
  cmd_line_list->string_arg = gengetopt_strdup (CMDLINE_PARSER_PACKAGE);

  if ((file = fopen(filename, "r")) == NULL)
    {
      fprintf (stderr, "%s: Error opening configuration file '%s'\n",
               CMDLINE_PARSER_PACKAGE, filename);
      result = EXIT_FAILURE;
      goto conf_failure;
    }

  while ((fgets(linebuf, CONFIG_FILE_LINE_SIZE, file)) != NULL)
    {
      ++line_num;
      my_argv[0] = '\0';
      len = strlen(linebuf);
      if (len > (CONFIG_FILE_LINE_BUFFER_SIZE-1))
        {
          fprintf (stderr, "%s:%s:%d: Line too long in configuration file\n",
                   CMDLINE_PARSER_PACKAGE, filename, line_num);
          result = EXIT_FAILURE;
          goto conf_failure;
        }

      /* find first non-whitespace character in the line */
      next_token = strspn ( linebuf, " \t\r\n");
      str_index  = linebuf + next_token;

      if ( str_index[0] == '\0' || str_index[0] == '#')
        continue; /* empty line or comment line is skipped */

      fopt = str_index;

      /* truncate fopt at the end of the first non-valid character */
      next_token = strcspn (fopt, " \t\r\n=");

      if (fopt[next_token] == '\0') /* the line is over */
        {
          farg  = NULL;
          equal = 0;
          goto noarg;
        }

      /* remember if equal sign is present */
      equal = (fopt[next_token] == '=');
      fopt[next_token++] = '\0';

      /* advance pointers to the next token after the end of fopt */
      next_token += strspn (fopt + next_token, " \t\r\n");
      /* check for the presence of equal sign, and if so, skip it */
      if ( !equal )
        if ((equal = (fopt[next_token] == '=')))
          {
            next_token++;
            next_token += strspn (fopt + next_token, " \t\r\n");
          }
      str_index  += next_token;

      /* find argument */
      farg = str_index;
      if ( farg[0] == '\"' || farg[0] == '\'' )
        { /* quoted argument */
          str_index = strchr (++farg, str_index[0] ); /* skip opening quote */
          if (! str_index)
            {
              fprintf
                (stderr,
                 "%s:%s:%d: unterminated string in configuration file\n",
                 CMDLINE_PARSER_PACKAGE, filename, line_num);
              result = EXIT_FAILURE;
              goto conf_failure;
            }
        }
      else
        { /* read up the remaining part up to a delimiter */
          next_token = strcspn (farg, " \t\r\n#\'\"");
          str_index += next_token;
        }

      /* truncate farg at the delimiter and store it for further check */
      delimiter = *str_index, *str_index++ = '\0';

      /* everything but comment is illegal at the end of line */
      if (delimiter != '\0' && delimiter != '#')
        {
          str_index += strspn(str_index, " \t\r\n");
          if (*str_index != '\0' && *str_index != '#')
            {
              fprintf
                (stderr,
                 "%s:%s:%d: malformed string in configuration file\n",
                 CMDLINE_PARSER_PACKAGE, filename, line_num);
              result = EXIT_FAILURE;
              goto conf_failure;
            }
        }

    noarg:
      ++my_argc;
      len = strlen(fopt);

      strcat (my_argv, len > 1 ? "--" : "-");
      strcat (my_argv, fopt);
      if (len > 1 && ((farg &&*farg) || equal))
          strcat (my_argv, "=");
      if (farg && *farg)
          strcat (my_argv, farg);

      cmd_line_list_tmp = (struct line_list *) malloc (sizeof (struct line_list));
      cmd_line_list_tmp->next = cmd_line_list;
      cmd_line_list = cmd_line_list_tmp;
      cmd_line_list->string_arg = gengetopt_strdup(my_argv);
    } /* while */

  ++my_argc; /* for program name */
  my_argv_arg = (char **) malloc((my_argc+1) * sizeof(char *));
  cmd_line_list_tmp = cmd_line_list;
  for (i = my_argc - 1; i >= 0; --i) {
    my_argv_arg[i] = cmd_line_list_tmp->string_arg;
    cmd_line_list_tmp = cmd_line_list_tmp->next;
  }
  my_argv_arg[my_argc] = 0;

  additional_error = (char *)malloc(strlen(filename) + strlen(ADDITIONAL_ERROR) + 1);
  strcpy (additional_error, ADDITIONAL_ERROR);
  strcat (additional_error, filename);
  result =
    cmdline_parser_internal (my_argc, my_argv_arg, args_info, override, initialize, check_required, additional_error);

  free (additional_error);
  free (my_argv_arg);

conf_failure:
  if (file)
    fclose(file);
  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}
