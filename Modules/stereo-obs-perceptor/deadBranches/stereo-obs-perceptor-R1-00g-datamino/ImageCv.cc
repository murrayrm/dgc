#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>

#include <Fl/Fl_Image.H>

#include "ImageCv.hh"
#include "util.hh"
#include "glutil.hh"

namespace blobstereo
{
    CvSize ImageCvBase::toGlTexture(const IplImage* img, int txtId, int glFormat,
                                    bool mipmap, bool setParams)
    {
        glBindTexture(GL_TEXTURE_2D, txtId);
            
        if (setParams) {
            //glPixelStoref(GL_UNPACK_ALIGNMENT, 1);
                
            // linear interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);        
        }

        static struct {
            int cv;
            int openGL;
        } cvTypes[] = {
            { IPL_DEPTH_8S, GL_BYTE },
            { IPL_DEPTH_8U, GL_UNSIGNED_BYTE },
            { IPL_DEPTH_16S, GL_SHORT },
            { IPL_DEPTH_16U, GL_UNSIGNED_SHORT },
            { IPL_DEPTH_32S, GL_INT },
            { IPL_DEPTH_32F, GL_FLOAT },
            { IPL_DEPTH_64F, GL_DOUBLE } // not sure if this works
        };
        int numTypes = sizeof(cvTypes)/sizeof(cvTypes[0]);

        int type = -1;
        for (int i = 0; i < numTypes; i++) {
            if (cvTypes[i].cv == img->depth) {
                type = i;
                break;
            }
        }
        if (type == -1) {
            ERRMSG("Image has an unknown depth (format) " << img->depth << "!!! ");
            return cvSize(1, 1);
        }

        int format = glFormat;
        if (format < 0) {
            switch(img->nChannels) {
            case 1:
                format = GL_LUMINANCE; break;
            case 2:
                format = GL_LUMINANCE_ALPHA; break;
            case 3:
                format = GL_RGB; break;
            case 4:
                format = GL_RGBA; break;
            default:
                ERRMSG("Too many channels (" << img->nChannels << ") (or too few?)!!!");
                format = GL_LUMINANCE;
            }
        }

        if (mipmap) {
            gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img->width, img->height, format,
                              cvTypes[type].openGL, img->imageData);
            return cvSize(img->width, img->height);
        } else {
            CvSize sz;
            int bpp = depthToBpc(img->depth) * img->nChannels;
            buildPlainTexture(img->width, img->height, bpp, img->widthStep, format,
                              cvTypes[type].openGL, (uint8_t*) img->imageData,
                              &sz.width, &sz.height);
            return sz;
        }

    }

    Fl_Image* ImageCvBase::toFlImage(const IplImage* img)
    {
        if (img->nChannels > 4)
            return NULL; // no way to convert this

        if (img->width == 0 || img->height == 0) {
            // create a dummy image, just to avoid returning null
            return new Fl_Image(1, 1, 1);
        }
        IplImage* newImg = cvCreateImageHeader(cvSize(img->width, img->height),
                                               IPL_DEPTH_8U, img->nChannels);
        uint8_t* data = new uint8_t[img->width * img->height * img->nChannels];
        cvSetData(newImg, data, img->width);

        // convert in 8 bit unsigned, just in case it wasn't in that format already
        cvConvert(img, newImg);

        Fl_RGB_Image* flImg = new Fl_RGB_Image((const uchar*) newImg->imageData, newImg->width, newImg->height, newImg->nChannels);
        flImg->alloc_array = 1; // please free it in the destructor!
        cvReleaseImageHeader(&newImg);
        return flImg;
    }

}
