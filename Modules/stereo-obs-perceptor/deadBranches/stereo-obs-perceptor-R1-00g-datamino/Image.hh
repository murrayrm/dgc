#ifndef __IMAGE_HH__
#define __IMAGE_HH__

#include <stdint.h>

namespace blobstereo
{

    /**
     * Very basic Image class, to hold some raw image data
     * in RGB or greyscale format.
     */
    class Image
    {
    public:
	typedef enum { FORMAT_RGB, FORMAT_GRAY8, FORMAT_GRAY16 } format_t;

	Image(int w=0, int h=0, format_t fmt=FORMAT_GRAY8)
	    : m_width(w), m_height(h), m_format(fmt), m_data(NULL)
	{
	    if (w*h > 0) {
		allocate();
	    }
	}

        ~Image() {
            if (m_data)
                delete[] m_data;
        }

        void init(int width, int height, format_t format = FORMAT_GRAY8)
        {
            m_width = width;
            m_height =  height;
            m_format = format;
	    if (m_width * m_height > 0) {
		allocate();
	    }
        }

        int getWidth() const { return m_width; }
        int getHeight() const { return m_height; }
        format_t getFormat() const { return m_format; }
        int8_t* getData() const { return m_data; }

        int getBytesPerPixel() const {
            switch (m_format) {
            case FORMAT_RGB: return 3;
            case FORMAT_GRAY8: return 1;
            case FORMAT_GRAY16: return 2;
            }
            return 1; // ??
        }

        int8_t* getRow(int row) const
        {
            return m_data + m_width * row * getBytesPerPixel();
        }

	// ...

    private:
	int m_width, m_height; // in pixels
	format_t m_format;
	int8_t* m_data; // raw data, an array of 4 byte pixels for RGB or 1 byte for greyscale.

	// allocates new memory for the data, using the current format,
	// width and height
	void allocate()
        {
            if (m_data) {
                delete[] m_data;
                m_data = NULL;
            }
            m_data = new int8_t[m_width * m_height * getBytesPerPixel()];
        }
    };
}

#endif
