#ifndef __DISPARITYDETECTOR_HH__
#define __DISPARITYDETECTOR_HH__

#include "Detector.hh"
#include "GlRenderer.hh"
#include "ImageCv.hh"

namespace blobstereo
{

    struct Track
    {
        int id; // unique id
        bool deleted; // deleted track, to be ignored
        // state is in image frame (but using 1/disparity)
        union {
            float state[6]; // col, row, disp, dcol, drow, d(disp)
            // for notational conveniencex
            struct {
                float col, row, disp, dcol, drow, ddisp;
            } st;
        };
        float var[6][6]; // error covariance matrix
        float likelihood; // remove me, just use the density of probability at the mean

        // Information about the last seen blob(s)
        // bounding box (image frame (col, row), top < bottom, left < right)
        float top, left, bottom, right;
        // shape in image frame
        //vector<point4> vertices; //not used yet
    };

    struct RegionData
    {
        int id;
        float xc, yc; // center point (mean), x = column, y = disparity (yeah it's confusing)
        CvMat cov; // 2 x 2 covariance matrix
        CvRect rect; // bounding box
        CvMat edges; // edge points
        // statistics about height of the blob
        float meanHeight;
        float varHeight;
        float sum; // sum of map values part of the region
        float likelihood; // probability of existence (actually, just a likelyhood)

        // used only for debugging purposes, when m_debug == true
        vector<int32_t>* debugXv;
        vector<int32_t>* debugYv;
        // same mean/varHeight, but in image coords
        float meanRow;
        float varRow;
            
        int minRow;
        int maxRow;

        bool filteredOut;

        RegionData(float _xc = 0, float _yc = 0, 
                   vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
            : id(-1), xc(_xc), yc(_yc), meanHeight(-1), varHeight(-1), sum(0),
              likelihood(0.4), debugXv(dbgXv), debugYv(dbgYv),
              meanRow(-1), varRow(-1), minRow(-1), maxRow(-1),
              filteredOut(false)
        {
            memset(&cov, 0, sizeof(cov));
            memset(&rect, 0, sizeof(rect));
            memset(&edges, 0, sizeof(edges));
        }

        // are the following two constructors needed at all??
#if 0

        RegionData(float _xc, float _yc, CvMat _cov, CvRect _rect,
                   vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
            : id(-1), xc(_xc), yc(_yc), cov(_cov), rect(_rect),
              meanHeight(-1), varHeight(-1), sum(0),
              likelihood(0.1), debugXv(dbgXv), debugYv(dbgYv),
              meanRow(-1), varRow(-1), minRow(-1), maxRow(-1),
              filteredOut(false)
        {
            memset(&edges, 0, sizeof(edges));
        }

        RegionData(float _xc, float _yc, CvMat _cov, CvRect _rect, CvMat _edges,
                   vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
            : id(-1), xc(_xc), yc(_yc), cov(_cov), rect(_rect), edges(_edges),
              meanHeight(-1), varHeight(-1), sum(0),
              likelihood(0.1), debugXv(dbgXv), debugYv(dbgYv),
              meanRow(-1), varRow(-1), minRow(-1), maxRow(-1),
              filteredOut(false)
        { }
#endif

        RegionData(const RegionData& d)
        {
            cov.data.ptr = NULL;
            edges.data.ptr = NULL;
            *this = d; // call operator=()
        }

        RegionData& operator=(const RegionData& d)
        {
            if (this == &d)
                return *this;
            if (cov.data.ptr != NULL)
                cvReleaseData(&cov);
            if (edges.data.ptr != NULL)
                cvReleaseData(&edges);

            memcpy(this, &d, sizeof(RegionData));
            // avoid memcpy'ing pointers, and we don't need them anyway
            debugXv = debugYv = NULL;

            // need to copy the data of matrices, if any, can't just memcpy them
            if (d.cov.data.ptr != NULL)
            {
                // semantically not correct, it's not making a copy or behaving as it did, but it works
                cvIncRefData(&cov);
            }

            if (d.edges.data.ptr != NULL)
            {
                // semantically not correct, it's not making a copy or behaving as it did, but it works
                cvIncRefData(&edges);
            }
            return *this;
        }

        ~RegionData()
        {
            if (cov.data.ptr != NULL)
            {
                cvReleaseData(&cov);
            }
            if (edges.data.ptr != NULL)
            {
                cvReleaseData(&edges);
            }
        }
    };


    /**
     * This class implements a detector based on the disparity map, that detects
     * generic obstacles (blobs) without any classification.
     */
    class DisparityDetector : virtual public Detector, virtual public GlRenderer
    {
        // mutex to access this object 
        pthread_mutex_t m_mtx;

        typedef int16_t disp_t;
        unsigned int m_maxDisp;
        unsigned int m_mapHeight; // REMOVE ME

        //typedef uint8_t dispmap_t;
        typedef float dispmap_t;
        /// map with accumulated disparity
        ImageCv<dispmap_t, 1> m_mapImg;
        /// Other information about the map, one for each channel:
        /// - maximum height
        /// - minimum height (TODO: if >>0 ==> floating ==> discard)
        /// - height mean
        /// - height variance
        /// TODO: USE ME!!!
        CvMat m_mapStat;
        /// 2 pixel taller and wider than m_mapImg. Needed for cvFloodFill.
        ImageCv<uint8_t, 1> m_maskImg; 
        /// Shares part of the data with maskImg, so no need to copy.
        CvMat m_maskMat;

        ImageCv<float, 1> m_rowSum;
        ImageCv<float, 1> m_rowSumSq;
        ImageCv<float, 1> m_maxRow;
        ImageCv<float, 1> m_minRow;

        vector<RegionData> m_regions;
        //vector<RegionData> m_tracks; // tracked regions (obstacles)
        vector<Track> m_tracks;
        uint64_t m_lastTs; // timestamp of the last frame
        float m_assocThres; // distance in pixels/seconds (it's not a velocity!)
                            // for associating obstacles

        dispmap_t m_mapMax; // maximum value in the current map
        double m_highThres;
        double m_lowThres;
        // constant measurement error variance for column, row, disparity
        float m_errorVar[3];

        double m_hThres; // fuzzy height threshold   REMOVE ME
        double m_hThresSlope; // slope of the curve when h == m_heightThres   REMOVE ME

        // weight tables
        static const float m_weightTabRes = 0.1;
        vector<float> m_hWeight;
        vector<float> m_dhWeight;

        enum smallObsMethod_t {
            SMALLOBS_IMAGE_BBOX,
            SMALLOBS_DISPSPC_WIDTH,
            SMALLOBS_DISPSPC_SUM,
            SMALLOBS_LOCAL_WIDTH,
            SMALLOBS_LOCAL_AREA,
        };
        smallObsMethod_t m_smallObsMethod;
        float m_smallObsThres;

        /*
         * DEBUGGING STUFF
         */

        /** Shows the detected obstacles, in image coordinates.
         * Should be overlayed on a ImageRenderer.
         */
        class ObstOverlayRenderer : virtual public GlRenderer, public MutexLockable
        {
            volatile DisparityDetector& self;
            int w;
            int h;
            bool useTracks;

        public:
            ObstOverlayRenderer(volatile DisparityDetector& me, bool tracks = false)
                : self(me), w(1), h(1), useTracks(tracks)
            { }

            void update(const SensnetBlob& blob) throw();

            /// Shows the detected obstacles as overlay boxes.
            void render() throw(Error);
        };

        /** Shows the obstacles in local frame, as filled polygons.
         */
        class ObstacleRenderer : virtual public GlRenderer, public MutexLockable
        {
            volatile DisparityDetector& self;

        public:
            ObstacleRenderer(volatile DisparityDetector& me)
                : self(me)
            { }

            void update(const SensnetBlob& blob) throw();

            /// Shows the detected obstacles as overlay boxes.
            void render() throw(Error);
        };

        /// if true, run debugging code
        bool m_debug;

        ImageCv<uint8_t, 2> m_dispImg; /// Disparity, 2 channels, value and alpha
        ImageCv<uint8_t, 4> m_debugImg; /// General purpose debugging display (RGBA)

        // opengl stuff
        GLuint m_dispTex;
        GLuint m_mapTex;
        GLuint m_maskTex;
        GLuint m_debugTex;

        vector<Obstacle> m_result; // for debug display
        vector<vector<int32_t> > m_dbgEdgesX;
        vector<vector<int32_t> > m_dbgEdgesY;
        vector<vector<int32_t> > m_dbgHullX;
        vector<vector<int32_t> > m_dbgHullY;

    public:
        DisparityDetector(bool dbg = true,
                          int maxDisp = 65, int mapHeight = 300,
                          double hiThres = 8, double loThres = 3,
                          double heightThres = 4, double hThresSlope = 3,
                          smallObsMethod_t smoMethod = SMALLOBS_DISPSPC_SUM,
                          float smallObsThres = 40 /*250*/, float assocThres = 100);

        ~DisparityDetector();

        /* from Detector */
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob);

        /* from GlRenderer */

        virtual void render() throw(Error);

        /**
         * Copy the disparity image from the blob to 'disp'.
         */
        virtual void update(const SensnetBlob& blob) throw(Error);

        virtual vector<Fl_Widget*> getWidgets();

    private:

        void pointsToObstacle(StereoImageBlob* sib, RegionData& data,
                              vector<point4>* vertices);

        // gather separate statistics for the high part and low part
        // so we can detect and handle trees properly.
        // (trees appears as very wide objects, but we care only about
        // the trunk).
        
        /** Given a CvArr containing a mask, find all the useful information about
         * the new blob contained into it (points with value == 1), and put it
         * into the 'data' structure.
         * find all the edge points and put them
         * in the points matrix, which is a 1D row vector containing 2D points
         * (type CV_32SC2, two channels of int32_t);
         * Note: if maskArr is of size (rows, cols), it searches in the rectangle
         * (1, 1, rows - 2, cols - 2) for a blob, and the mask boundary should be
         * all set to zero.
         */
        void scanRegion(CvArr* maskArr, int xOff, int yOff, RegionData* data);

        /**
         * Filters (removes) all the points in the specified region (where the mask is == 1) that
         * have a minimum row bigger than the threshold.
         * This is mainly to cut trees.
         */
        void filterRegion(CvArr* maskArr, int xOff, int yOff, RegionData* data);

        /**
         * Associate obstacles detected in the current scan with obstacles
         * seen in the last scan.
         */
        void trackObstacles(uint64_t tstamp);

        /**
         * Given a point in homogeneous coordinates in sensor frame, and the transformation
         * from image to sensor (in homog coords) and sensor to local frame, it calculates
         * the variance of the point in local frame, assuming a fixed variance in image frame
         * of diag(m_errorVar).
         */
        void calcPointCov(float cov[4][4], const point4 ps,
                          const float himg2sens[4][4], const float sens2loc[4][4]);
    };

}

#endif
