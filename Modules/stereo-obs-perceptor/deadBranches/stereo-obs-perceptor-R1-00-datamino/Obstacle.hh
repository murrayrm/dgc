#ifndef __BLOBLADAR_OBSTACLE_HH__
#define __BLOBLADAR_OBSTACLE_HH__

#include <vector>

#include <frames/point2_uncertain.hh>

namespace blobstereo {

    using std::vector;

    class Obstacle
    {
        vector<point2_uncertain> vertices; // meters
        
        point2_uncertain speed; // m/s

    public:

        vector<point2_uncertain>& getVertices()
        {
            return vertices;
        }

        const vector<point2_uncertain>& getVertices() const
        {
            return vertices;
        }

        // ... anything else?
    };

} // end namespace

#endif
