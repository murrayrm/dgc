#ifndef __SENSNET_BLOB_HH__
#define __SENSNET_BLOB_HH__

#include <iostream>
#include <exception>
#include <cassert>
#include <sensnet/sensnet.h>

#include "util.hh"

namespace blobstereo {

/** All sensnet blobs have the same generic fields at the beginnings,
 * with blob type, version, sensor id, blob id and timestamp. All of these
 * are reported by the sensnet_read() and sensnet_peek() functions, except
 * for the timestamp. It's possible to use this structure to get it.
 */
struct CommonBlob
{
  int blobType;
  int32_t version;
  int32_t sensorId;
  int32_t blobId;
  uint64_t timestamp;
} __attribute__((packed));


/** This class encapsulates a sensnet blob, and has methods
 * to read values from and write them to a sensnet "object".
 * It's not the most elegant solution (would be better to have a Sensnet
 * class that has read(SensnetBlob*) and write(const SensnetBlob&) methods)
 * but it's simple and it works well, to ease manipulating blobs in a
 * more OOP fashion.
 */
class SensnetBlob
{
private:

    uint64_t m_timestamp;
    int m_sensorId; 
    int m_type;
    int m_id;
    int m_len;
    
    int m_size; // allocated size for "data" (should be >= m_len)
    int8_t *m_data;

public:

    /** Thrown when something goes wrong in some method of the SensnetBlob class.
     */
    class Error : public std::exception {
        std::string msg;
    public:
        Error(std::string m) : msg(m) { }
        ~Error() throw() { }
        virtual std::string getMsg() const throw() { return msg; }
        virtual const char* what() const throw() { return msg.c_str(); }
    };

    SensnetBlob(uint64_t tstamp = 0, int sensId = -1, int _type = -1, int _id = -1,
                int _len = 0)
        :  m_timestamp(tstamp), m_sensorId(sensId), m_type(_type),
           m_id(_id), m_len(_len), m_size(0), m_data(NULL)
    { }

    ~SensnetBlob()
    {
        if (m_data != NULL) 
            delete[] m_data;
    }

    /** Reads the latest blob, and allocates a big enough buffer for it.
     * \param sensnet The sensnet "object".
     * \return true if a new blob was available, false if the last available
     * was the same we had before.
     */
    bool read(sensnet_t* sensnet) throw (Error)
    {
#warning "Check this, not sure about the behavior of sensnet_peek"
        int lastId = m_id;
        int newLen = 0;
        //MSG("DEBUG: sensorId = " << m_sensorId << ", type = " << m_type);
        if (sensnet_peek(sensnet, m_sensorId, m_type, &m_id, &newLen) != 0)
            //throw Error("read(): sensnet_peek() failed");
            return false; // no blobs available
        
        if (m_id == -1 || newLen <= 0) {
            m_id = lastId; // no new blob
            return false;
        }

        if (lastId == m_id) {
            // same blob we read before, nothing new
            //assert(lastLen == m_len);
//             if (lastLen != m_len) {
//                 ERRMSG("lastLen (" << lastLen << ") != m_len (" << m_len << ")");
//                 ERRMSG("m_id = " << m_id << ",  lastId = " << lastId);
//             }
            return false;
        }

        m_len = newLen;

        if (m_len > m_size)
            resizeData(m_len);

        if (sensnet_read(sensnet, m_sensorId, m_type, &m_id, m_size, m_data) != 0)
            throw Error("read(): sensnet_read() failed");

        // get the timestamp
        m_timestamp = reinterpret_cast<CommonBlob*>(m_data)->timestamp;

        return true;
    }

    void write(sensnet_t* sensnet, int method = SENSNET_METHOD_CHUNK) const throw (Error)
    {
        if (sensnet_write(sensnet, method, m_sensorId, m_type, m_id, m_len, m_data) != 0)
            throw Error("write(): sensnet_write() failed! ");
    }

    /// @brief Returns true if a new blob of type this->type and from sensor this->sensor_id
    /// is available.
    bool hasNew(sensnet_t* sensnet)
    {
        int newId, newLen; // discarded
        return hasNew(sensnet, &newId, &newLen);
    }

    /// @brief Returns true if a new blob of type this->type and from sensor this->sensor_id
    /// is available. Sets the values of newId and newLen to the ones of the new blob.
    bool hasNew(sensnet_t* sensnet, int* newId, int* newLen) {
        if (sensnet_peek(sensnet, m_sensorId, m_type, newId, newLen) != 0)
            throw Error("hasNew(): sensnet_peek() failed");
        
        if (m_id == *newId) {
            // same blob we read before, nothing new
            assert(*newLen == m_len); // just a little check
            return false;
        }
        return true;
    }

    /** Clears the current blob. It won't actually free the memory, though.
     */
    void clear()
    {
        m_id = -1;
        m_len = 0;
    }

    /** Clears the current blob and frees the memory buffer.
     */
    void free()
    {
        clear();
        if (m_data != NULL) {
            delete[] m_data;
            m_data = NULL;
        }
        m_size = 0;
    }

    // Warning: when calling one of the setXXX function, to *read* from a different
    // sensor or type, be sure, to reset the ID to -1, so the next call will always
    // read something instead of thinking the old blob is the same one, if by chance
    // they happen to have the same ID.

    /// @brief Get the current blob's timestamp
    uint64_t getTimestamp() const { return m_timestamp; }

    /// @brief Get the current sensor ID.
    int getSensorId() const { return m_sensorId; }
    /// @brief Change the current sensor ID. Warning: if you plan use read()
    /// be sure to reset the ID with resetID().
    void setSensorId(int sensId) { m_sensorId = sensId; }

    /// @brief Get the current blob type.
    int getType() const { return m_type; }
    /// @brief Change the current blob type. Warning: if you plan use read()
    /// be sure to reset the ID with resetID().
    void setType(int type) { m_type = type; }

    /// @brief Get the current blob ID, unique for each blob, given one sensor_id (and type?).
    int getId() const { return m_id; }
    /// @brief Set the current blob ID.
    void setId(int id) { m_id = id; }
    /// @brief Reset the current blob ID to an invalid ID (-1).
    void resetId() { setId(-1); }

    /// @brief Returns the current data length, in bytes.
    int getDataLen() const { return m_len; }
    /// @brief Get the current data pointer. Note: this pointer may change (will be
    /// deleted and reallocated) everytime a new blob is read, of when setDataLength()
    /// is called.
    int8_t* getData() const { return m_data; }
    /// @brief Set the length of the data buffer. If necessary, the buffer will be
    /// reallocated to fit the new size, but the data will NOT be copied.
    /// Use it to resize the buffer prior to filling it up.
    void setDataLength(int newLen)
    {
        if (m_len != newLen) {
            if (newLen > m_size)
                resizeData(newLen);
            m_len = newLen;
        }
    }

    /** Set the buffer to a user allocated buffer. The SensnetBlob will take
     * ownership of the buffer, and will eventually free it.
     * @param data The data buffer. Must be at least 'size' bytes long.
     * @param size The size, in bytes, of the buffer.
     * @param len If >= 0, the length of the blob. Must be <= size.
     */
    void setData(int8_t* data, int size, int len = -1)
    {
        m_data = data;
        m_size = size;
        if (len >= 0) {
            if (len <= m_size) {
                m_len = len;
            } else {
                // throw? abort? just a warning at the moment
                std::cerr << "Warning: SensnetBlob::setData(): len=" << len
                     << " bigger than size" << size << "!!!" << std::endl;
                m_len = m_size;
            }
        } else {
            m_len = m_size;
        }
    }

protected:

    void resizeData(int newSize)
    {
        if (m_data != NULL)
            delete[] m_data;
        m_data = new int8_t[newSize];
        m_size = newSize;
    }

    
};

} // end namespace

#endif
