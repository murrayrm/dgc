#ifndef __BLOBLADAR_OBSTACLE_HH__
#define __BLOBLADAR_OBSTACLE_HH__

#include <vector>

#include <frames/point2_uncertain.hh>

namespace blobstereo {

    using std::vector;

    class Obstacle
    {
        vector<point2_uncertain> vertices; // meters
        
        point2_uncertain speed; // m/s

    public:

        /* Getters and Setters */

        vector<point2_uncertain>& getVertices()
        {
            return vertices;
        }

        const vector<point2_uncertain>& getVertices() const
        {
            return vertices;
        }

        point2_uncertain& getSpeed()
        {
            return speed;
        }

        const point2_uncertain& getSpeed() const
        {
            return speed;
        }

        // ... anything else?
    };

} // end namespace

#endif
