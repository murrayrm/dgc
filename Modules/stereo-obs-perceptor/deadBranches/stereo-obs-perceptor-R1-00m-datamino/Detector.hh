#ifndef __BLOBSTEREO_DETECTOR_HH__
#define __BLOBSTEREO_DETECTOR_HH__

#include <vector>
#include <cv.h>
#include <map/MapElement.hh>
#include "SensnetBlob.hh"
//#include "Obstacle.hh"
#include "ILockable.hh"


// avoid including FLTK stuff if not needed
class Fl_Widget;

namespace blobstereo {

    using std::vector;

    class Detector : virtual public MutexLockable
    {

    public:
        virtual ~Detector() { }
        
        /*!
         * Execute the detection algorithm given the current Stereo blob.
         * \return An array of Obstacle objects, representing the detected
         * obstacles (possibly filtered and tracked).
         */
        //virtual vector<Obstacle> detect(const SensnetBlob& blob) = 0;
        //virtual vector<Track> detect(const SensnetBlob& blob, vector<int>* deleted) = 0;
        virtual void detect(const SensnetBlob& blob, vector<MapElement>* elements) = 0;

        /*!
         * If this detector has a state that is kept from one call to detect()
         * to the other, this method should reset the state.
         * The default implementation does nothing, as if there was no state.
         */
        virtual void reset() { }

        /*!
         * Returns a vector of FLTK widgets to be used for the debugging display.
         * These can be anything, useful to show what's going on with the
         * detector and even to change parameters on the fly.
         * When the display is off, this function won't be called at all.
         * The default implementation returns an empty vector.
         * \return The vector of widgets, or an empty vector if none
         */
        virtual vector<Fl_Widget*> getWidgets() { return vector<Fl_Widget*>(); }


        // Implement locking and unlocking using volatile methods
        // See http://www.ddj.com/dept/cpp/184403766

        /*! Thread safe (volatile) version of detect() */
        void detect(const SensnetBlob& blob, vector<MapElement>* elements) volatile
        //vector<Track> detect(const SensnetBlob& blob, vector<int>* deleted) volatile
        {
            LockingPtr<Detector> self(this);
            return self->detect(blob, elements);
        }

        /*! Thread safe (volatile) version of reset() */
        void reset() volatile
        {
            LockingPtr<Detector> self(this);
            self->reset();
        }

        /*! Thread safe (volatile) version of getWidgets() */
        vector<Fl_Widget*> getWidgets() volatile
        {
            LockingPtr<Detector> self(this);
            return self->getWidgets();
        }

    };

}

#endif
