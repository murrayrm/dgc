/*!
 * \file DisparityDetectorDebug.cc
 * \brief OpenGL Debug Display for DisparityDetector and related stuff
 *
 * \author Daniele Tamino
 * \date 17 July 2007
 *
 */

// standard C++ headers
#include <iostream>
#include <cstdlib>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>
// boost headers
#include <boost/scoped_array.hpp>
// FLTK headers
#include <FL/Fl_Widget.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
// OpenCV headers
#include <cv.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <frames/mat44.h>
#include <dgcutils/AutoMutex.hh>
#include <interfaces/StereoImageBlob.h>
// local headers
#include "DisparityDetector.hh"
#include "IUpdatable.hh"
#include "util.hh"
#include "glutil.hh"

namespace blobstereo
{
    using namespace std;
    using namespace boost;


    void mat44f_vecmul(float out[4], float m[4][4], float v[4])
    {
        out[0] = v[0]*m[0][0] + v[1]*m[0][1] + v[2]*m[0][2] + v[3]*m[0][3];
        out[1] = v[0]*m[1][0] + v[1]*m[1][1] + v[2]*m[1][2] + v[3]*m[1][3];
        out[2] = v[0]*m[2][0] + v[1]*m[2][1] + v[2]*m[2][2] + v[3]*m[2][3];
        out[3] = v[0]*m[3][0] + v[1]*m[3][1] + v[2]*m[3][2] + v[3]*m[3][3];
    }

    /*! Shows the detected obstacles, in image coordinates.
     * Should be overlayed on a ImageRenderer.
     */
    class ObstOverlayRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        int w;
        int h;

    public:
        ObstOverlayRenderer(volatile DisparityDetector& me)
            : self(me), w(1), h(1)
        { }

        void update(const SensnetBlob& blob) throw();

        /// Shows the detected obstacles as overlay boxes.
        void render() throw(Error);
    };

    void ObstOverlayRenderer::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        this->w = sib->cols;
        this->h = sib->rows;
        /* Obstacle information updated in DisparityDetector::detect() */ 
    }
    
    /// Shows the detected obstacles as overlay boxes.
    void ObstOverlayRenderer::render() throw(Error)
    {
        //AutoMutex am(self.m_mtx);
        LockingPtr<DisparityDetector> This(&self);

        vector<Region>& regions = This->m_regions;

        if (regions.size() > 0)
        {
            pushViewport();

            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/this->w, -2.0/this->h, 0);

            glDisable(GL_TEXTURE_2D);
            glDisable(GL_CULL_FACE);


            glDisable(GL_BLEND);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            for (unsigned int i = 0; i < regions.size(); i++)
            {
                //if (!useTracks && regions[i].filteredOut == true)
                //    continue;

                float x1, x2, y1, y2;
                x1 = regions[i].rect.x;
                x2 = x1 + regions[i].rect.width;
                y1 = regions[i].row - regions[i].rowStDev * 1.5;
                y2 = regions[i].row + regions[i].rowStDev * 1.5;

                glColor4f(1.0, 0.0, 0.0, 1.0);
                glBegin(GL_POLYGON);

                glVertex3f(x1, y1, 0.0);
                glVertex3f(x2, y1, 0.0);
                glVertex3f(x2, y2, 0.0);
                glVertex3f(x1, y2, 0.0);

                glEnd();
/*
                MSG("Box" << i << ":"
                    << "(" << x1 << ", " << y1 << ") "
                    << "(" << x2 << ", " << y1 << ") "
                    << "(" << x2 << ", " << y2 << ") "
                    << "(" << x1 << ", " << y2 << ") "
                    );
*/
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            popViewport();
        }
    }



    /*! Shows the obstacles in local frame, as filled polygons.
     */
    class ObstacleRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        float m_loc2veh[4][4];

    public:
        ObstacleRenderer(volatile DisparityDetector& me)
            : self(me)
        { }

        void update(const SensnetBlob& blob) throw();

        /// Shows the detected obstacles as overlay boxes.
        void render() throw(Error);
    };

    void ObstacleRenderer::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        memcpy(m_loc2veh, sib->loc2veh, sizeof(sib->loc2veh));
        // swap x and y (first two rows)
        for (int i = 0; i < 4; i++)
            swap(m_loc2veh[0][i], m_loc2veh[1][i]);
        // zero-out z coord, we just want 2D here
        m_loc2veh[2][0] = m_loc2veh[2][1] = m_loc2veh[2][2] = m_loc2veh[2][3] = 0;
        // and the w coord too, set it to always 1.0
        m_loc2veh[3][0] = m_loc2veh[3][1] = m_loc2veh[3][2] = 0; m_loc2veh[3][3] = 1;
    }

    /// Shows the detected obstacles as overlay boxes.
    void ObstacleRenderer::render() throw(Error)
    {
        pushViewport();
        
        // set the viewport to the lower right quarter
        //int vp[4];
        //glGetIntegerv(GL_VIEWPORT, vp);
        //glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);
        
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_CULL_FACE);


        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        float minx = -50;
        float maxx = 50;
        float miny = 0;
        float maxy = 100;

        glScalef(2.0/(maxx - minx), 2.0/(maxy - miny), 1);
        glTranslatef(-(minx + maxx)/2, -(miny + maxy)/2, 0.0);
/*
        cerr << "MODELVIEW_MATRIX (scale and trasl) =" << endl;
        glGetFloatv(GL_MODELVIEW_MATRIX, (float*) m);
        printGlMatrix(m);
*/
        // draw x axis
        glColor4f(1.0, 1.0, 1.0, 1.0); // white
        glBegin(GL_LINES);
        glVertex3f((minx + maxx)/2, miny, 0.0);
        glVertex3f((minx + maxx)/2, maxy, 0.0);
        glEnd();
        
        // draw grid
        glColor4f(0.0, 1.0, 0.0, 1.0); // green
        for (float x = minx; x <= maxx; x += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(x, miny, 0.0);
            glVertex3f(x, maxy, 0.0);
            glEnd();
        }

        for (float y = miny; y <= maxy; y += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(minx, y, 0.0);
            glVertex3f(maxx, y, 0.0);
            glEnd();
        }

        LockingPtr<DisparityDetector> This(&self);
        vector<Track>& tracks = This->m_tracks;
        //vector<Region>& regions = This->m_regions;
        if (tracks.size() > 0)
        {
            float m[4][4];

            // set local frame transformation
/*
            cerr << "m_loc2veh = " << endl;
            printGlMatrix(m_loc2veh, true);
*/
            mat44f_trans(m, m_loc2veh); // opengl wants column-major matrices
            // All the functions that multiply the current matrix with something
            // (glScale, glTranslate, glMultMatrix...) PRE-MULTIPLY the matrix, so
            // the transformation will happen BEFORE the current one!!! keep this in mind!!!
            // In this case, the m_loc2veh transform will happen before the scale and translate. 
            glMultMatrixf((const float*)m);
            
/*
            cerr << "MODELVIEW_MATRIX (loc2veh scaled and trasl) =" << endl;
            glGetFloatv(GL_MODELVIEW_MATRIX, (float*) m);
            printGlMatrix(m);

            cerr << "PROJECTION_MATRIX =" << endl;
            glGetFloatv(GL_PROJECTION_MATRIX, (float*) m);
            printGlMatrix(m);
*/

            for (unsigned int i = 0; i < tracks.size(); i++)
            {
                if (tracks[i].deleted())
                    continue;
                point2arr& vert = tracks[i].convexHull;
                //MSG("Poly" << i << ":");
                glBegin(GL_POLYGON);
                
                //if (!regions[i].filteredOut)
                    glColor4f(1.0, 0.3, 0.3, 0.7); // bright red
                //else
                    //glColor4f(0.0, 1.0, 1.0, 0.7); // cyan
                    //continue;

                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    glVertex4f(vert[j].x, vert[j].y, tracks[i].state[2], 1);
                    //cerr << "(" << vert[j][0] << ", " << vert[j][1] << ", "
                    //     << vert[j][2]  << ", " << vert[j][3]  << ")";
                }

                glEnd(); 
                //MSG(endl << "Poly" << i << " END");

            }

        }
        //glEnable(GL_CULL_FACE);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        //glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
        popViewport();
    }

    /*! Shows the disparity map and the detected obstacles in
     * (pixel column, disparity) coordinates.
     */
    class DisparityDetectorRenderer : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        bool m_showObst;
        bool m_showRegions;
        bool m_showEdges;

        float m_disp2cell;

        GLuint m_texId;

    public:
        /**
         * Constructor. Takes a reference to the DisparityDetector object to display
         * information of, and some parameters.
         * \param me The detector object.
         * \param showPolys Show the detected obstacles as polygons overimposed on the map
         * \param showRegions Show the detected regions with different colors instead of the
         * grayscale map.
         */
        DisparityDetectorRenderer(volatile DisparityDetector& me,
                                  bool showObst = true, bool showRegions = false,
                                  bool showEdges = false)
            : self(me), m_showObst(showObst), m_showRegions(showRegions),
              m_showEdges(showEdges), m_texId(0)
        { }

        void update(const SensnetBlob& blob) throw();

        /// Shows the detected obstacles as overlay boxes.
        void render() throw(Error);
    };

// FIXME: just fix me!!! :-P
#define DRAW_MAP 1
#define DRAW_MASK 0
#define DRAW_DEBUG_IMAGE 0 // is this used for anything anymore?
#define DRAW_EDGES 0 // actually, edge points
#define DRAW_OBSTACLES_DISPFRAME 1
    void DisparityDetectorRenderer::update(const SensnetBlob& /*blob*/) throw()
    {
        // conversion from disparity image to map cell index
        dirty = true;
    }

    void DisparityDetectorRenderer::render() throw(Error)
    {
        LockingPtr<DisparityDetector> This(&self);
        float disp2cell = float(This->m_mapHeight) / This->m_maxDisp;
        //AutoMutex am(m_mtx);

        // todo: check for errors!! (not a big deal actually, this is just for debugging)
        if (m_texId == 0) {
            glGenTextures(1, &m_texId);
        }
        
        float scaleX = 1.0;
        float scaleY = 1.0;

        ////////////////////////////////////////////
        // Create the texture for the disparity map
        ////////////////////////////////////////////

        if (This->m_mapMax == 0) {
            This->m_mapMax = 1; // avoid divide-by-zero
        }

        if (!m_showRegions && This->m_mapImg.getWidth() > 0) {
            // create texture for the map, with luminance - alpha
            // TODO: move this in the class declaration to avoid reallocation
            scoped_array<uint8_t> mapImg(new uint8_t[This->m_mapImg.getWidth()*This->m_mapImg.getHeight()*2]);

            typedef DisparityDetector::dispmap_t dispmap_t;
            // maybe can be optimized ...
            int w = This->m_mapImg.getWidth();
            int h = This->m_mapImg.getHeight();
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    dispmap_t val = This->m_mapImg[r][c][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (/*val < 64 && val >= 0*/ true) {
                        //pix = val * 256 / 64; // this will be optimized by the compiler
                        pix = uint8_t(val * 256 / This->m_mapMax);
                        if (val == 0) {
                            alpha = 0;
                        }
                    } else {
                        pix = 255;
                    }
                    mapImg[(r * w + c)*2] = pix;
                    mapImg[(r * w + c)*2 + 1] = alpha;
                }
            }

            glBindTexture(GL_TEXTURE_2D, m_texId);
        
            // no interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, This->m_mapImg.getWidth(),
            //                  This->m_mapImg.getHeight(), GL_LUMINANCE_ALPHA,
            //                  GL_UNSIGNED_BYTE, mapImg.get());
            int width, height;
            int bpp = 2;
            int bpr = w * bpp;
            buildPlainTexture(This->m_mapImg.getWidth(), This->m_mapImg.getHeight(), bpp, bpr,
                              GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, mapImg.get(),
                              &width, &height);
            scaleX = float(This->m_mapImg.getWidth())/width;
            scaleY = float(This->m_mapImg.getHeight())/height;
        }

        if (m_showRegions && This->m_maskImg.getWidth() > 0) {
            CvMat sub;
            IplImage imgHdr;
            IplImage* mask = NULL;
            cvGetSubRect(This->m_maskImg.getImage(), &sub,
                         cvRect(1, 1, This->m_maskImg.getWidth()-1, This->m_maskImg.getHeight()-1));
            mask = cvGetImage(&sub, &imgHdr);

            CvSize sz = BitmapBase::toGlTexture(mask, m_texId, GL_ALPHA);
            scaleX = float(This->m_maskImg.getWidth())/sz.width;
            scaleY = float(This->m_maskImg.getHeight())/sz.height;
        }

        glEnable(GL_TEXTURE_2D);

        ////////////////////////////////////////////
        // Draw the disparity map or the regions (whichever was selected)
        ////////////////////////////////////////////

        pushViewport();
        glColor3f(0.0, 0.0, 0.5); // dark blue
        glBindTexture(GL_TEXTURE_2D, m_texId);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

        //glTranslatef(1.0, 0.0, 0.0);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(scaleX, scaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (1.0, 1.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (1.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

        // no depth text, we want the next images to overlap and blend
        glDisable (GL_DEPTH_TEST);
        // enable blending
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if DRAW_OBSTACLES_DISPFRAME
        vector<Region>& regions = This->m_regions;
        if (regions.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            //int vp[4];
            //glGetIntegerv(GL_VIEWPORT, vp);
            //glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            // image coordinates
            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/This->m_mapImg.getWidth(), -2.0/This->m_mapImg.getHeight(), 1.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_CULL_FACE);

            glColor4f(1.0, 1.0, 1.0, 1.0);

            for (unsigned int i = 0; i < regions.size(); i++)
            {
                point2arr& vert = regions[i].convexHull;
                glBegin(GL_POLYGON);
                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    glVertex3f(vert[j].x, vert[j].y * disp2cell, 0.1);
                }
                glEnd();
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            //glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
        }
#endif

        glDisable (GL_BLEND);
        glEnable (GL_DEPTH_TEST);

    }

    /*! Shows the obstacles and the measures in local frame, as 3D objects
     */
    class ObstacleRenderer3D : virtual public GlRenderer, public MutexLockable
    {
        volatile DisparityDetector& self;
        float m_loc2veh[4][4]; // transformation from local frame to vehicle frame
        float m_veh2cam[4][4]; // transformation from vehicle frame to virtual camera frame
        float m_veh2loc[4][4]; // transformation from vehicle frame to local frame
        float m_sx, m_sy; // camera x ad y focus
        float m_near, m_far; // opengl near and far clipping planes
        point3 m_color;
        bool m_useMeasures;
        point3 m_alicePos; // local frame

    public:
        ObstacleRenderer3D(volatile DisparityDetector& me,
                           point3 color = makePoint3(1.0, 0.0, 0.0) /* RGB */,
                           bool measures = false,
                           float near = 10.0, float far = 200.0)
            : self(me), m_sx(1), m_sy(1),
              m_near(near), m_far(far),
              m_color(color), m_useMeasures(measures)
        {
            // set loc2veh to the identity matrix
            mat44f_zero(m_loc2veh);
            m_loc2veh[0][0] = m_loc2veh[1][1] = m_loc2veh[2][2] = m_loc2veh[3][3] = 1;
            // same for veh2cam
            mat44f_zero(m_veh2cam);
            m_veh2cam[0][0] = m_veh2cam[1][1] = m_veh2cam[2][2] = m_veh2cam[3][3] = 1;
            m_alicePos[0] = m_alicePos[1] = m_alicePos[2] = 0;
        }

        void update(const SensnetBlob& blob) throw();

        void render() throw();
    };

    void ObstacleRenderer3D::update(const SensnetBlob& blob) throw()
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        memcpy(m_loc2veh, sib->loc2veh, sizeof(sib->loc2veh));
        memcpy(m_veh2loc, sib->veh2loc, sizeof(sib->veh2loc));
        // at the moment, the virtual camera is the same as the real camera
        // in the future, the virtual camera could be moved using the mouse, so
        // this will change then
        memcpy(m_veh2cam, sib->leftCamera.veh2sens, sizeof(m_veh2cam));
        // normalize sx and sy for a [-1, 1] range
        m_sx = sib->leftCamera.sx * 2 / sib->cols;
        m_sy = sib->leftCamera.sy * 2 / sib->rows;
        m_alicePos[0] = sib->state.localX;
        m_alicePos[1] = sib->state.localY;
        m_alicePos[2] = sib->state.localZ;
    }

    void ObstacleRenderer3D::render() throw()
    {
        float loc2cam[4][4];
        mat44f_mul(loc2cam, m_veh2cam, m_loc2veh);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        
        glLoadIdentity();
        float minx = -100;
        float maxx = 100;
        float miny = -100;
        float maxy = 100;
        
        float from[4], to[4];
        from[0] = -15.0;
        from[1] =  0.0;
        from[2] = -15.0;
        from[3] =  1.0;
        to[0] = 15.0;
        to[1] =  0.0;
        to[2] =  0.0;
        to[3] =  1.0;
        
        float fromLoc[4], toLoc[4];
        mat44f_vecmul(fromLoc, m_veh2loc, from);
        mat44f_vecmul(toLoc, m_veh2loc, to);
        gluLookAt(fromLoc[0], fromLoc[1], fromLoc[2],
                  toLoc[0], toLoc[1], toLoc[2],
                  0, 0, -1);
        
/*
        float tmp[4][4]; 
        //mat44f_trans(tmp, loc2cam); // opengl wants column-major matrices
        mat44f_trans(tmp, m_loc2veh); // opengl wants column-major matrices
        // swap x and y (first two columns)
        for (int i = 0; i < 4; i++)
            swap(tmp[i][0], tmp[i][1]);
        glMultMatrixf((const GLfloat*)tmp);

        float proj[4][4] = {
            { m_sx, 0,     0,  0},
            { 0,    m_sy,  0,  0},
            { 0,    0,     (m_near + m_far)/(m_near - m_far),  (2*m_far*m_near)/(m_near - m_far) },
            { 0,    0,    -1,  0},
        };

*/

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        //mat44f_trans(proj, proj); // opengl wants column-major matrices
        //glLoadMatrixf((const GLfloat*)proj);
        //gluPerspective(42.0, 4.0/3.0, m_near, m_far);
        gluPerspective(80.0, 4.0/3.0, m_near, m_far);

        point3 gridc; // grid center
        gridc[0] = 10 * floor(m_alicePos[0] / 10.0 + 0.5); // round to the nearest multiple of 10
        gridc[1] = 10 * floor(m_alicePos[1] / 10.0 + 0.5); // round to the nearest multiple of 10
        gridc[2] = m_alicePos[2];

        // draw x axis
        glColor4f(1.0, 1.0, 1.0, 1.0); // white
        glBegin(GL_LINES);
        glVertex3f(gridc[0] + (minx + maxx)/2, gridc[1] + miny, gridc[2]);
        glVertex3f(gridc[0] + (minx + maxx)/2,  gridc[1] + maxy, gridc[2]);
        glEnd();
        
        // draw grid
        glColor4f(0.0, 1.0, 0.0, 1.0); // green
        for (float x = minx; x <= maxx; x += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(gridc[0] + x, gridc[1] + miny, gridc[2]);
            glVertex3f(gridc[0] + x, gridc[1] + maxy, gridc[2]);
            glEnd();
        }
        
        for (float y = miny; y <= maxy; y += 10)
        {
            glBegin(GL_LINES);
            glVertex3f(gridc[0] + minx, gridc[1] + y, gridc[2]);
            glVertex3f(gridc[0] + maxx, gridc[1] + y, gridc[2]);
            glEnd();
        }

        // lock the detector object - don't do it before as it's not needed
        LockingPtr<DisparityDetector> This(&self);

        vector<Track>& tracks = m_useMeasures ? This->m_measures : This->m_tracks;

/*
        float mat[4][4];
        MSG("MODELVIEW MATRIX:");
        glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
        printGlMatrix(mat);

        MSG("PROJECTION MATRIX:");
        glGetFloatv(GL_PROJECTION_MATRIX, (float*) mat);
        printGlMatrix(mat);
*/
        //glColor3f(m_color[0], m_color[1], m_color[2]);

#if 0
        glEnable(GL_LIGHTING);
        glShadeModel(GL_FLAT);
        float global_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

        // Create light components
        float ambientLight[] = { 1.0, 1.0, 1.0, 1.0 };
        float diffuseLight[] = { 0.8, 0.8, 0.8, 1.0 };
        float specularLight[] = { 0.5, 0.5, 0.5, 1.0 };
        float positionVeh[] = { -5000, 5000, -5000.0, 1.0 };
        float position[4];
        mat44f_vecmul(position, m_veh2loc, positionVeh);
        
        // Assign created components to GL_LIGHT0
        glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
        glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
        glLightfv(GL_LIGHT0, GL_POSITION, position);
#endif

        for (int n = 0; n < 2; n++) {
        
            if (n == 0) {
                glColor3f(1.0, 1.0, 1.0);
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // wireframe mode
            } else {
                glColor3f(1.0, 0.0, 0.0);
                glPolygonMode(GL_FRONT, GL_FILL); // solid mode
            }
            
            for (unsigned int i = 0; i < tracks.size(); i++)
            {
                if (tracks[i].deleted())
                    continue;
                float lowZ = tracks[i].state[2] - tracks[i].height/2;
                float highZ = tracks[i].state[2] + tracks[i].height/2;
                point2arr& points = tracks[i].convexHull;
                assert(points.size() > 0);

                //glColor3f(float(rand())/RAND_MAX, float(rand())/RAND_MAX, float(rand())/RAND_MAX);
                glBegin(GL_QUAD_STRIP);
                //MSG("Obstacle3D: quad strip begin: lowZ=" << lowZ << ", highZ=" << highZ
                //    << ", height=" << tracks[i].height);
                for (unsigned int j = 0; j < points.size(); j++)
                {
                    glVertex3f(points[j].x, points[j].y, lowZ);
                    glVertex3f(points[j].x, points[j].y, highZ);
                    //cerr << '(' << points[j].x << ", " << points[j].y << ')';
                }
                // close the strip
                //cerr << '(' << points[0].x << ", " << points[0].y << ')' << endl;
                glVertex3f(points[0].x, points[0].y, lowZ);
                glVertex3f(points[0].x, points[0].y, highZ);
 
                //MSG("Obstacle3D: quad strip endn");
                glEnd();
            }
        } 
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // solid mode, both faces

        // set up vehicle frame
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        gluLookAt(from[0], from[1], from[2],
                  to[0], to[1], to[2],
                  0, 0, -1);

        // render alice
        glColor3f(1.0, 1.0, 0.0);
        glBegin(GL_QUAD_STRIP);
        glVertex3f(3, 3, 0.0);
        glVertex3f(3, 3, -3.0);

        glVertex3f(-3, 3, 0.0);
        glVertex3f(-3, 3, -3.0);

        glVertex3f(-3, -3, 0.0);
        glVertex3f(-3, -3, -3.0);

        glVertex3f(3, -3, 0.0);
        glVertex3f(3, -3, -3.0);

        glVertex3f(3, 3, 0.0);
        glVertex3f(3, 3, -3.0);

        glEnd();
        glPopMatrix();

        // draw translucent ground plane - do this at the end, as otherwise it will
        // block other object via the depth buffer
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // solid mode, both faces
        glEnable(GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glColor4f(0.6, 0.6, 0.6, 0.8);
        glBegin(GL_QUADS);

        glVertex3f(gridc[0] + minx, gridc[1] + miny, gridc[2]);
        glVertex3f(gridc[0] + maxx, gridc[1] + miny, gridc[2]);
        glVertex3f(gridc[0] + maxx, gridc[1] + maxy, gridc[2]);
        glVertex3f(gridc[0] + minx, gridc[1] + maxy, gridc[2]);
        
        glEnd();
        glDisable(GL_BLEND);

        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
    }

    /*! This class is just to test OpenGL commands.
     */
    class TestRenderer : virtual public GlRenderer, public MutexLockable
    {
        float m_loc2veh[4][4];

    public:

        TestRenderer() {
            mat44f_zero(m_loc2veh);
            m_loc2veh[0][0] = m_loc2veh[1][1] = m_loc2veh[2][2] = m_loc2veh[3][3] = 1;
        }

        void update(const SensnetBlob& blob) throw()
        {
            StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
            memcpy(m_loc2veh, sib->loc2veh, sizeof(m_loc2veh));
            mat44f_trans(m_loc2veh, m_loc2veh);
        }

        void render() throw()
        {
            float mat[4][4];
            glMatrixMode(GL_MODELVIEW);

            //cerr << "TEST: Previous Mat:" << endl;
            //glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
            //printGlMatrix(mat);

            glPushMatrix();

            gluLookAt(0, 0, -1,  0, 0, 0,  1, 0, 0);
            //cerr << "TEST: LookAt matrix:" << endl;
            //glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
            //printGlMatrix(mat);
            glPushMatrix();

            glDisable(GL_DEPTH_TEST);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // wireframe mode

            float p1[4][4] = {
                { -0.6,  0.3, 0.0, 1.0 },
                { -0.5, -0.4, 0.0, 1.0 },
                {  0.6, -0.3, 0.0, 1.0 },
                {  0.5,  0.4, 0.0, 1.0 },
            };

            glColor3f(1.0, 0.0, 0.0);
            drawQuad(p1);

/*
            float p2[4][4] = {
                { -0.7,  0.4, 0.0, 1.0 },
                { -0.6, -0.5, 0.0, 1.0 },
                {  0.7, -0.4, 0.0, 1.0 },
                {  0.6,  0.5, 0.0, 1.0 },
            };
*/
            float p2[4][4] = {
                {41.135744, 80.112096, -114.062375, 1.000000},
                {40.300427, 79.765620, -114.015474, 1.000000},
                {40.759038, 78.545106, -114.011078, 1.000000},
                {41.594355, 78.891582, -114.057979, 1.000000},
            };

/*
            float p2v[4][4];
            float loc2veh[4][4];
            mat44f_trans(loc2veh, m_loc2veh); // use normal matrix order for our use!

            for (int i = 0; i < 4; i++)
                mat44f_vecmul(p2v[i], loc2veh, p2[i]);

            glLoadIdentity();
            glColor3f(0.0, 0.0, 1.0);
            drawQuad(p2v);
*/
            glLoadMatrixf((float*)m_loc2veh);

            cerr << "TEST: loc2veh matrix:" << endl;
            glGetFloatv(GL_MODELVIEW_MATRIX, (float*) mat);
            printGlMatrix(mat);
            cerr << "TEST: projection matrix:" << endl;
            glGetFloatv(GL_PROJECTION_MATRIX, (float*) mat);
            printGlMatrix(mat);

            glColor3f(0.0, 1.0, 0.0);
            drawQuad(p2);

            glPopMatrix();
        }

    private:
        void drawQuad(float p[4][4])
        {
            char text[64];

            glBegin(GL_QUADS);

            for (int i = 0; i < 4; i++) {
                //glVertex4f(p[i][0], p[i][1], p[i][2], p[i][3]);
                glVertex3f(p[i][0], p[i][1], p[i][2]);
            }

            glEnd();

            glColor3f(1.0, 1.0, 1.0);
            gl_font(FL_HELVETICA, 12);
            cerr << "TEST: quad ";
            for (int i = 0; i < 4; i++) {
                snprintf(text, sizeof(text), "(%.2f, %.2f)", p[i][0], p[i][1]);
                // use RasterPos instead of gl_draw(text, x, y) so we can use all the 4 coords
                // and have them transformed using the current opengl matrix
                glRasterPos4f(p[i][0], p[i][1], p[i][2], p[i][3]);
                gl_draw(text);
                cerr << "(" << p[i][0] << ", " << p[i][1] << ", "
                     << p[i][2]  << ", " << p[i][3]  << ")";
            }
            cerr << endl;
        }

    };

    vector<Fl_Widget*> DisparityDetector::getWidgets()
    {
        shared_ptr<GlRenderer> irLeft(new SensnetImageRenderer(SensnetImageRenderer::LEFT));
        shared_ptr<GlRenderer> overlayRend(new ObstOverlayRenderer(*this));
        //shared_ptr<GlRenderer> overlayRend2(new ObstOverlayRenderer(*this, true));

        shared_ptr<GlRenderList> rendList(new GlRenderList());
        rendList->addRenderer(irLeft);
        rendList->addRenderer(overlayRend);

        // add a second one that uses tracked regions instead of just measurements
        //shared_ptr<GlRenderList> rendList2(new GlRenderList());
        //rendList2->addRenderer(irLeft);
        //rendList2->addRenderer(overlayRend2);


        vector<Fl_Widget*> widgets;
        widgets.push_back(new RenderWidget(rendList, 0, 0, 640, 480, "Left w/ obst"));
        //widgets.push_back(new RenderWidget(rendList2, 0, 0, 640, 480, "Left w/ tracks"));

        shared_ptr<GlRenderer> detRend(new DisparityDetectorRenderer(*this));
        widgets.push_back(new RenderWidget(detRend, 0, 0, 640, 480, "Detector"));

        shared_ptr<ObstacleRenderer> obstRend(new ObstacleRenderer(*this));
        widgets.push_back(new RenderWidget(obstRend, 0, 0, 640, 480, "Obstacles"));

        shared_ptr<ObstacleRenderer3D> obstRend3D(new ObstacleRenderer3D(*this));
        widgets.push_back(new RenderWidget(obstRend3D, 0, 0, 640, 480, "Obstacles3D"));

        //shared_ptr<TestRenderer> testRend(new TestRenderer());
        //widgets.push_back(new RenderWidget(testRend, 0, 0, 640, 480, "*TEST*"));

        //shared_ptr<GlRenderer> maxRowRend(new DispDetMapRenderer(m_minRow, m_mtx));
        //widgets.push_back(new RenderWidget(maxRowRend, 0, 0, 640, 480, "MinRow"));
        return widgets;
    }

}
