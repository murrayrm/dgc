#ifndef __BLOBSTEREO_DETECTOR_HH__
#define __BLOBSTEREO_DETECTOR_HH__

#include <vector>
#include "SensnetBlob.hh"
#include "Obstacle.hh"
#include "ILockable.hh"

// avoid including FLTK stuff if not needed
class Fl_Widget;

namespace blobstereo {

    using std::vector;

    class Detector : virtual public MutexLockable
    {

    public:
        virtual ~Detector() { }
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob) = 0;
        // does this class need any state?
        // just in case, this will resets everything ;-)
        virtual void reset() { }

        /// Returns a list of widgets used to display the status of the detector
        /// mainly for debugging purposes.
        virtual vector<Fl_Widget*> getWidgets() { return vector<Fl_Widget*>(); }


        // Implement locking and unlocking using volatile methods
        // See http://www.ddj.com/dept/cpp/184403766

        vector<Obstacle> detect(const SensnetBlob& blob) volatile
        {
            LockingPtr<Detector> self(this);
            return self->detect(blob);
        }

        void reset() volatile
        {
            LockingPtr<Detector> self(this);
            self->reset();
        }

        vector<Fl_Widget*> getWidgets() volatile
        {
            LockingPtr<Detector> self(this);
            return self->getWidgets();
        }

    };

}

#endif
