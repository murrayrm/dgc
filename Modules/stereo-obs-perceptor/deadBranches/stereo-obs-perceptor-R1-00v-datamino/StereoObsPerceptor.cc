#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <memory>
#include <cerrno>

// a little boost utility for vectors of owned pointers
//#include <boost/ptr_container/ptr_vector.hpp> // maybe later
#include <boost/shared_ptr.hpp>

// unix headers
#include <signal.h>
#include <sys/stat.h>
#include <sys/times.h>

// DGC headers
#include <cotk/cotk.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/sn_types.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/ProcessState.h>
#include <map/MapElementTalker.hh>
#include <dgcutils/AutoMutex.hh>
#include <dgcutils/cfgfile.h>
#include <frames/ellipse.hh>

// local headers
#include "perceptor_cmdline.h"
#include "Detector.hh"
#include "DisparityDetector.hh"
//#include "Tracker.hh"
#include "SensnetLog.hh"
#include "GlRenderer.hh"
#include "IUpdatable.hh"
#include "util.hh"

// hack to avoid cotk stderr redirection to deadlock us (bug #3392)
extern "C" int cotk_update_stderr(cotk_t *self);
        
namespace stereo_obs {

    using namespace std;
    using boost::shared_ptr;
    using boost::dynamic_pointer_cast;

    static const char* consoleTemplate =
    "Stereo Blob Detector %spread%\n"
    "                     skynet key: %skynetkey%\n"
    "\n"
    "Number of blobs received: %numrecv%\n"
    "Number of total obstacles sent: %totsent%\n"
    "\n"
    "Current input stereo blob:\n"
    "    tstamp: %curtsRel%                  %curts%\n"
    "    id: %blobid%\n"
    "    sensor id: %sensorid%\n"
    "    type: %blobtype%\n"
    "    length: %bloblen% bytes\n"
    "\n"
    "Number of obstacles sent to mapper: %numsent%\n"
    "Number of obstacles cleared: %numclear%\n"
    "Detected obstacles:        max id = %maxid%\n"
    "    Num: %obstNum%            %PV%|%NX%\n"
    "    ID: %obstId%\n"
    "    (Coordinates are in Local frame)\n"
    "    Center: %obstCenter%\n"
    "    Bounding box: %obstBox%\n"
//    "    Points:\n"
//    "    %obstGeom%\n"
    "\n" // additional line if obstacle has many points
    "Timings:\n"
    "    Current:       %curTime%\n"
    "    Avg (last 10): %avgTime%\n"
    "    Max:           %maxTime%\n"
    "\n"
    "%QUIT%|%RESET%|%DISPLAY%\n"
    "%stderr%\n"
    "%stderr%\n"
    "%stderr%\n"
    "%stderr%\n"
    "%stderr%\n";

    // factory function to create a debugging object
    Fl_Widget* createSendDebugWidget(int skynetKey, int subgroup, volatile DisparityDetector& det);

    class StereoObsPerceptor
    {
        static const uint64_t HEARTBEAT_TIME = 2000000L;

        int argc;
        char** argv;

        static volatile sig_atomic_t quit;

        gengetopt_args_info options;
    
        // Spread settings
        char* spreadDaemon;
        int skynetKey;
        modulename moduleId;
        int sensorId;
 
        // Sensnet module
        sensnet_t *sensnet;

        bool opengl; // true = enable opengl display

        float m_confThres; // confidence threshold

        cotk_t *console;
        pthread_mutex_t cotkMtx; // stderr updating thread, bug #3392
        //char* consoleTempl;
        //vector<Track> m_tracks;
        vector<MapElement> m_elements;
        unsigned int obstIdx; // index of the obstacle showing

        SensnetBlob blob;
        uint64_t startTStamp; // first timestamp received
        int numBlobs; // total number of blobs read

        shared_ptr<volatile Detector> detector;

        // number of obstacles sent the last time
        int m_totalObstSent; // obstcles sent from the beginning
        int m_obstSent; // obstacle sent the last time
        int m_clearSent; // number of ELEMENT_CLEAR sent
        int m_maxId; // maximum obstacle ID

/*
        clock_t m_userTicks; // detection time, this process only
        clock_t m_sysTicks; // detection time, this process only
        uint64_t m_time; // real detection time, usec
*/
        // all timings are in usecs
        static const int NUM_PAST_TIMES = 10;
        uint64_t m_time; // real time elapsed in the latest detect() call
        uint64_t m_lastTimes[NUM_PAST_TIMES]; // time elapsed in the last 10 calls
        uint64_t m_maxTime; // maximum elapsed time ever
        

        // list of objects (widgets usually) to update when a new blob is received
        // Only when the debugging display is active
        pthread_mutex_t renderMtx;
        vector<IUpdatable*> renderList;

        CMapElementTalker mapTalker;

        // last time we sent an heartbeat (ProcessResponse message)
        uint64_t beatTime;

    public:

        /**
         * Constructor. Takes command line arguments as input, parses them
         * and loads the correct config file. Then, initializes sensnet and
         * skynet.
         */
        StereoObsPerceptor(int argc, char** argv) throw(Error)
            : sensnet(NULL), opengl(true),
              console(NULL), obstIdx(0), startTStamp(0), numBlobs(0),
              m_time(0), m_maxTime(0),
              beatTime(-5000000) /* 5 secs in the past */
        {
            // initialize muxetes and other data fields
            pthread_mutex_init(&renderMtx, NULL);
            AutoMutex::initMutex(&cotkMtx);
            this->argc = argc;
            this->argv = argv;

            // zero means empty slot
            fill(m_lastTimes, m_lastTimes + NUM_PAST_TIMES, 0);

            // parse commandline options
            if (cmdline_parser (argc, argv, &options) != 0)
            {
                exit(1);
            }

            char fnameBuf[256];
            char cfgpathBuf[256];
            char* cfgFileName = NULL;
            char* cfgFilePath = NULL; // full name and path

            // check for shortcut options
            bool shortcut = false;
            if (options.long_flag) {
                this->moduleId = MODstereoObsPerceptorLong;
                this->sensorId = SENSNET_MF_LONG_STEREO;
                cfgFileName = "SENSNET_MF_LONG_STEREO.cfg";
                shortcut = true;
            } else if (options.medium_flag) {
                this->moduleId = MODstereoObsPerceptorMedium;
                this->sensorId = SENSNET_MF_MEDIUM_STEREO;
                cfgFileName = "SENSNET_MF_MEDIUM_STEREO.cfg";
                shortcut = true;
            } else if (options.rear_flag) {
                this->moduleId = MODstereoObsPerceptorRear;
                this->sensorId = SENSNET_REAR_SHORT_STEREO;
                cfgFileName = "SENSNET_REAR_SHORT_STEREO.cfg";
                shortcut = true;
            } else {
                snprintf(fnameBuf, sizeof(fnameBuf), "%s.cfg", options.sensor_id_arg);
                cfgFileName = fnameBuf;
            }

            struct stat st;
            if (options.config_file_given) {
                if (stat(options.config_file_arg, &st) == 0)
                    cfgFilePath = options.config_file_arg;
                else
                    cfgFileName = options.config_file_arg;
            }


            // Fill out the default config path
            if (cfgFilePath == NULL)
            {
                if (this->options.config_path_given)
                {
                    snprintf(cfgpathBuf, sizeof(cfgpathBuf),"%s/%s",
                             this->options.config_path_arg, cfgFileName);
                    cfgFilePath = cfgpathBuf;
                }
                else
                {
                    cfgFilePath = dgcFindConfigFile(cfgFileName, "stereo-obs-perceptor");
                }
            }

            //MSG("DBG: cfgFileName=" << cfgFileName << ", cfgFilePath=" << cfgFilePath);
                
            // read config file, if any, depending on the sensor-id
            if (stat(cfgFilePath, &st) == 0) {
                MSG("Reading config file: " << cfgFilePath);
                cmdline_parser_configfile(cfgFilePath, &options, 0, 0, 0);
            }
            else {
                // if using a shortcut, better to quit if we cannot find the config file
                // otherwise, maybe it's just an experimental new camera ... use default.
                if (shortcut)
                    throw ERROR("Config file " + toStr(cfgFilePath) + " not found");
                else
                    ERRMSG("Config file " << cfgFilePath << " not found, using default");
            }

            // cmdline parsing ...
            // Fill out the spread name
            if (this->options.spread_daemon_given)
                this->spreadDaemon = this->options.spread_daemon_arg;
            else if (getenv("SPREAD_DAEMON"))
                this->spreadDaemon = getenv("SPREAD_DAEMON");
            else
                throw ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
            // Fill out the skynet key
            if (this->options.skynet_key_given)
                this->skynetKey = this->options.skynet_key_arg;
            else if (getenv("SKYNET_KEY"))
                this->skynetKey = atoi(getenv("SKYNET_KEY"));
            else
                this->skynetKey = 0;
  
            if (!shortcut) {
                // Fill out module id
                this->moduleId = modulenamefromString(this->options.module_id_arg);
                if (this->moduleId <= 0)
                    throw ERROR("invalid module id: " + string(this->options.module_id_arg));
                
                // Fill out sensor id
                this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
                if (this->sensorId <= SENSNET_NULL_SENSOR)
                    throw ERROR("invalid sensor id: " + string(this->options.sensor_id_arg));
            }

            opengl = options.opengl_flag;
            m_confThres = options.conf_thres_arg;
            initSensnet();
        }

        ~StereoObsPerceptor()
        {
            if (useConsole()) {
                finiConsole();
            }
            // not needed, and actually, it's better without ;-)
            //if (opengl) {
            //    finiOpenGL();
            //}

            finiSensnet();
        }


        // ---------------------------
        // SENSNET
        // ---------------------------

        // Initialize sensnet
        void initSensnet() throw(Error)
        {
            // Create sensnet interface
            this->sensnet = sensnet_alloc();
            assert(this->sensnet);
            if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
                throw ERROR("sensnet_connect() failed");

#if 0
            // If replaying log files, now is the time to open them
            if (this->mode == modeReplay)
                if (sensnet_open_replay(this->sensnet, this->options.inputs_num, this->options.inputs) != 0)
                    throw ERROR("unable to open log");
#endif

            detector = shared_ptr<Detector>(new DisparityDetector(options));

            // join stereo groups
            if (sensnet_join(this->sensnet, this->sensorId,
                             SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
                throw ERROR("unable to join " + toStr(sensorId));

            // Subscribe to process state messages
            if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
                throw ERROR("unable to join process group");

            // initialize talker
            mapTalker.initSendMapElement(skynetKey);
        }

        void finiSensnet()
        {
            if (sensnet)
            {
                sensnet_leave(sensnet, sensorId, SENSNET_STEREO_IMAGE_BLOB);
                sensnet_disconnect(sensnet);
                sensnet_free(sensnet);
                sensnet = NULL;
            }
        }

        // Given a ProcessRequest request, take the corresponding action
        void handleProcessControl() throw()
        {
            int blobId;
            ProcessRequest request;
            ProcessResponse response;

            // Send heart-beat message every HEARTBEAT_TIME usecs
            if (gettime() - this->beatTime > HEARTBEAT_TIME)
            {
                memset(&response, 0, sizeof(response));  
                response.moduleId = this->moduleId;
                response.timestamp = gettime();
                response.logSize = 0;
                response.healthStatus = 2;
                sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                              this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
                this->beatTime = 0;
            }
            
            // Read process request
            if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                             &blobId, sizeof(request), &request) != 0)
                return;
            if (blobId < 0)
                return;
            
            if (request.quit)
                quit ++;
        }

        // ---------------------------
        // USER INTERFACE (cotk)
        // ---------------------------

        typedef CotkMemberCallbackData<StereoObsPerceptor> callback_data_t;

        // Is cotk TUI (textual user interface ;-) enabled?
        bool useConsole() { return !options.disable_console_flag; }

        void initConsole()
        {
            if (console == NULL) {
                console = cotk_alloc();
            }
            cotk_bind_template(console, consoleTemplate);
        
            cotk_callback_t callback_func = cotkMemberCallback<StereoObsPerceptor>;
            // bind buttons
            cotk_bind_button(console, "%QUIT%", " QUIT ", "Qq", callback_func,
                             new callback_data_t(this, &StereoObsPerceptor::onQuit));

            cotk_bind_button(console, "%RESET%", " RESET ", "Rr", callback_func,
                             new callback_data_t(this, &StereoObsPerceptor::onReset));

            cotk_bind_button(console, "%NX%", " >> ", "Nn", callback_func,
                             new callback_data_t(this, &StereoObsPerceptor::onNextObst));
            cotk_bind_button(console, "%PV%", " << ", "Pp", callback_func,
                             new callback_data_t(this, &StereoObsPerceptor::onPrevObst));

            cotk_bind_toggle(console, "%DISPLAY%", " DISPLAY ", "Ll", callback_func,
                             new callback_data_t(this, &StereoObsPerceptor::toggleOpenGL));
            cotk_toggle_set(console, "%DISPLAY%", options.opengl_flag);

            cotk_open(console, NULL); // no message logging at the moment

            // bug #3392, start stderr updating thread
            DGCstartMemberFunctionThread(this, &stereo_obs::StereoObsPerceptor::updateStderrThread);
        }

        void finiConsole()
        {
            if (console != NULL) {
                cotk_close(console);
                cotk_free(console);
            }
        }

        void updateDisplay()
        {
            if (useConsole())
            {
                AutoMutex am(cotkMtx);
                cotk_update(console);
            }
        }

        void displayObstacle(int id)
        {
            double xc = 0, yc = 0;
            double xMin = numeric_limits<float>::max();
            double xMax = numeric_limits<float>::min();
            double yMin = numeric_limits<float>::max();
            double yMax = numeric_limits<float>::min();

            bool deleted = false;
            
            MapElement* el = NULL;
            if (id >= 0 && id < (int) m_elements.size())
            {
                el = &m_elements[id];
                const point2arr_uncertain& vert = el->geometry;
                for (unsigned int i = 0; i < vert.size(); i++)
                {
                    xc += vert[i].x;
                    yc += vert[i].y;
                    xMin = min(xMin, double(vert[i].x));
                    xMax = max(xMax, double(vert[i].x));
                    yMin = min(yMin, double(vert[i].y));
                    yMax = max(yMax, double(vert[i].y));
                }
                xc /= vert.size();
                yc /= vert.size();
            } else {
/*
                if (id >= 0 && id < (int) m_tracks.size())
                    deleted = true;
                xMin = 0;
                yMin = 0;
                xMax = 0;
                yMax = 0;
*/
            }

            if (useConsole())
            {
                AutoMutex am(cotkMtx);
                int conWidth, conHeight;

                // show obstacle in cotk display
                getmaxyx(stdscr, conHeight, conWidth);
                // clear the fields
                cotk_printf(console, "%obstCenter%", A_NORMAL, "%*s", conWidth - 20, "");
                cotk_printf(console, "%obstBox%", A_NORMAL, "%*s", conWidth - 20, "");
                //cotk_printf(console, "%obstGeom%", A_NORMAL, "%*s", conWidth*2 - 4, "");

                cotk_printf(console, "%obstNum%", A_NORMAL, "%-10d", id);

                if (el != NULL && el->geometry.size() > 0) {
                    ostringstream os;
                    os << el->id;
                    cotk_printf(console, "%obstId%", A_NORMAL, "%s", os.str().c_str());

                    cotk_printf(console, "%obstCenter%", A_NORMAL, "(%f, %f)", xc, yc);
                    cotk_printf(console, "%obstBox%", A_NORMAL,
                                "(%5f, %5f)-(%5f, %5f) [(xMin, yMin)-(xMax, yMax)]",
                                xMin, yMin, xMax, yMax);
/*
                    const point2arr_uncertain& vert = el->geometry;
                    os.str("");
                    for (unsigned int i = 0; i < vert.size(); i++)
                    {
                        os.precision(4);
                        os << "(" << vert[i].x << "," << vert[i].y << ")";
                    }
                    cotk_printf(console, "%obstGeom%", A_NORMAL, "%s", os.str().c_str());
*/
                }
                else
                {
                    const char* msg;
                    if (deleted)
                        msg = "Track deleted"; /* NOT USED ANYMORE */
                    else
                        msg =  "No obstacle with this ID (or empty obstacle)";
                    cotk_printf(console, "%obstCenter%", A_NORMAL, msg);
                    cotk_printf(console, "%obstBox%", A_NORMAL, msg);
                    //cotk_printf(console, "%obstGeom%", A_NORMAL, msg);
                }
            }
            else
            {
                // TODO
            }
            
        }

        void displayStatus(bool newBlob)
        {
            // calculate average time
            double avgTime = 0; // msecs
            int count = 0;
            for (int i = 0; i < NUM_PAST_TIMES; i++) {
                if (m_lastTimes[i] > 0) {
                    avgTime += double(m_lastTimes[i]) * 1e-3;
                    count++;
                }
            }

            if (count > 0) avgTime /= count;

            if (useConsole()) {
                displayObstacle(obstIdx);
                {   
                    AutoMutex am(cotkMtx);
                    // display the status on cotk console
                    cotk_printf(console, "%spread%", A_NORMAL, "%s:%s",
                                spreadDaemon, modulename_asString(this->moduleId));
                    cotk_printf(console, "%skynetkey%", A_NORMAL, "%d", skynetKey);
                    
                    cotk_printf(console, "%numrecv%", A_NORMAL, "%-10d", numBlobs);
                    cotk_printf(console, "%numsent%", A_NORMAL, "%-10d", m_obstSent);
                    cotk_printf(console, "%numclear%", A_NORMAL, "%-10d", m_clearSent);
                    cotk_printf(console, "%totsent%", A_NORMAL, "%-10d", m_totalObstSent);
                    cotk_printf(console, "%maxid%", A_NORMAL, "%-10d", m_maxId);
                    cotk_printf(console, "%curtsRel%", A_NORMAL, "%-8g sec (relative)",
                                (blob.getTimestamp() - startTStamp)/1e6);
                    cotk_printf(console, "%curts%", A_NORMAL, "%-12.0f usec (since the epoch)",
                                static_cast<double>(blob.getTimestamp()));
                    cotk_printf(console, "%blobid%", A_NORMAL, "%d", blob.getId());
                    cotk_printf(console, "%sensorid%", A_NORMAL, "%d", blob.getSensorId());
                    cotk_printf(console, "%blobtype%", A_NORMAL, "%d", blob.getType());
                    cotk_printf(console, "%bloblen%", A_NORMAL, "%ld", blob.getDataLen());
                    

                    cotk_printf(console, "%curTime%", A_NORMAL, "%g ms (%g Hz MAX)",
                                double(m_time) / 1000.0, 1e6/double(m_time));
                    cotk_printf(console, "%avgTime%", A_NORMAL, "%g ms (%g Hz MAX)",
                                avgTime, 1000.0/avgTime);
                    cotk_printf(console, "%maxTime%", A_NORMAL, "%g ms (%g Hz MAX)",
                                double(m_maxTime) / 1000.0, 1e6/double(m_maxTime));
/*
                    double ticksPerSec = (double) sysconf(_SC_CLK_TCK);
                    cotk_printf(console, "%userTime%", A_NORMAL, "%g ms (%g Hz MAX)",
                                m_userTicks * 1000 / ticksPerSec, ticksPerSec / double(m_userTicks));
                    cotk_printf(console, "%sysTime%", A_NORMAL, "%g ms", m_sysTicks * 1000 / ticksPerSec);
*/
                    
                    cotk_update(console);
                }
            } else if (newBlob) {
                // display the status on normal console (cout)
                cerr << "timestamp: " << blob.getTimestamp() << endl;
                cerr << "  Obstacles sent: " << m_obstSent << endl;
                cerr << "  Clear msg sent: " << m_clearSent << endl;
                cerr << "  time: " << double(m_time) / 1000.0 << " ms ("
                     << 1e6/double(m_time) << "Hz MAX)"
                     << "  avg: " << avgTime << " ms (" << 1000.0/avgTime << "Hz MAX)"
                     << "  max: " << double(m_maxTime) / 1000.0 << " ms ("
                     << 1e6/double(m_maxTime) << "Hz MAX)" << endl;
            }
        } 

        int onNextObst(cotk_t* /*cotk*/, const char* /*token*/)
        {
            obstIdx++;
            displayObstacle(obstIdx);
            return 0;
        }

        int onPrevObst(cotk_t* /*cotk*/, const char* /*token*/)
        {
            if (obstIdx > 0)
            {
                obstIdx--;
            }
            displayObstacle(obstIdx);
            return 0;
        }

        int onQuit(cotk_t* /*cotk*/, const char* /*token*/)
        {
            quit ++;
            return 0;
        }

        int onReset(cotk_t* /*cotk*/, const char* /*token*/)
        {
            detector->reset();
            fill(m_lastTimes, m_lastTimes + NUM_PAST_TIMES, 0);
            m_maxTime = 0;
            return 0;
        }

        // ---------------------------
        // OPENGL DISPLAY
        // ---------------------------

        int initOpenGL()
        {
            // widgets are deleted by fltk, no need to delete them here
            renderList.clear();
            vector<Fl_Widget*> widgets;
            RenderWidget* rwid;

            shared_ptr<volatile GlRenderer> rend = dynamic_pointer_cast<volatile GlRenderer>(detector);
            if (rend) {
                rwid = new RenderWidget(rend, 0, 0, 640, 480, "Detector");
                renderList.push_back(rwid);
                widgets.push_back(rwid);
            }

            shared_ptr<GlRenderer> irLeft(new SensnetImageRenderer(SensnetImageRenderer::LEFT));
            rwid = new RenderWidget(irLeft, 0, 0, 640, 480, "Left");
            renderList.push_back(rwid);
            widgets.push_back(rwid);

            shared_ptr<GlRenderer> irRight(new SensnetImageRenderer(SensnetImageRenderer::RIGHT));
            rwid = new RenderWidget(irRight, 0, 0, 640, 480, "Right");
            renderList.push_back(rwid);
            widgets.push_back(rwid);

            shared_ptr<GlRenderer> irDisp(new SensnetImageRenderer(SensnetImageRenderer::DISPARITY));
            rwid = new RenderWidget(irDisp, 0, 0, 640, 480, "Disparity");
            renderList.push_back(rwid);
            widgets.push_back(rwid);

            // add more here!

            //const vector<Fl_Widget*>& detWidgets = detector->getWidgets();
            vector<Fl_Widget*> detWidgets = detector->getWidgets();
            // debug mapelement sender istantiated here, is kinda hacky but it's just debugging
            // commented for the release
            //detWidgets.push_back(createSendDebugWidget(skynetKey, -27, dynamic_cast<volatile DisparityDetector&>(*detector.get())));

            for (unsigned int i = 0; i < detWidgets.size(); i++)
            {
                widgets.push_back(detWidgets[i]);
                // if it implements IUpdatable, add it to renderList
                IUpdatable* upd = dynamic_cast<IUpdatable*>(detWidgets[i]);
                if (upd != NULL) {
                    renderList.push_back(upd);
                }
            }

            opengl = true;
            //return startFltkThread(argc, argv, widgets, &renderMtx);
            static char* fake_argv[2] = { "stereo-obs-perceptor", NULL };
            return startFltkThread(1, fake_argv, widgets, &renderMtx);
        }

        void finiOpenGL()
        {
            stopFltkThread();
            renderList.clear();
            opengl = false;
        }

        int toggleOpenGL(cotk_t* /*cotk*/, const char* /*token*/)
        {
            if (!opengl && cotk_toggle_get(console, "%DISPLAY%"))
            {  
                int ret = initOpenGL(/*argc, argv*/);
                opengl = (ret == 0);
                return ret;
            } else if (opengl && !cotk_toggle_get(console, "%DISPLAY%")){
                finiOpenGL();
                opengl = false;
                return 0;
            }
            return 0;
        }

        // ---------------------------
        // DETECTION AND TRACKING
        // ---------------------------

        void processBlob(const SensnetBlob& blob)
        {
            numBlobs++;
            if (startTStamp == 0)
            {
                startTStamp = blob.getTimestamp();
            }

            m_obstSent = 0;
            m_clearSent = 0;
            if (detector)
            {
                //struct tms tmStart, tmEnd;
                //times(&tmStart);
                uint64_t start = gettime();

                // run the actual detection algorithm
                detector->detect(blob, &m_elements);

                m_time = gettime() - start;
                //times(&tmEnd);
                //m_userTicks = tmEnd.tms_utime - tmStart.tms_utime;
                //m_sysTicks = tmEnd.tms_stime - tmStart.tms_stime;
                
                //curObst = objs;
                //m_tracks = tracks;

                // keep the last 10 times
                memmove(m_lastTimes + 1, m_lastTimes, NUM_PAST_TIMES - 1);
                m_lastTimes[0] = m_time;

                m_maxTime = max(m_time, m_maxTime);

                // send the obstacles to the mapper (or planner or whoever)
                m_obstSent = 0;
                m_clearSent = 0;
                unsigned int maxPoints = 0;
                unsigned int minPoints = numeric_limits<int>::max();
                //MSG("*** BEGIN OF MAP ELEMENTS ***");
                for (unsigned int i = 0; i < m_elements.size(); i++)
                {
                    //cerr << m_elements[i] << endl;
                    mapTalker.sendMapElement(&m_elements[i], options.send_subgroup_arg);
                    if (m_elements[i].type == ELEMENT_CLEAR/*m_elements[i].isClear()*/)
                        m_clearSent++;
                    else {
                        m_obstSent++;
                        maxPoints = max(m_elements[i].geometry.size(), maxPoints);
                        minPoints = min(m_elements[i].geometry.size(), minPoints);
                    }
                }
                //MSG("*** END OF MAP ELEMENTS ***");
                //MSG("Maximum number of points: " << maxPoints << ", minimum: " << minPoints);
                m_totalObstSent += m_obstSent;

                if (options.send_alice_flag)
                {
                    // send alice, so the mapviewer will show her
                    StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
                    MapElement aliceEl;
                    aliceEl.set_alice(sib->state);
                    //mel.print_state();
                    mapTalker.sendMapElement(&aliceEl, options.send_subgroup_arg);
                }
            }

            if (opengl)
            {
                try {
                    for (unsigned int i = 0; i < renderList.size(); i++)
                        renderList[i]->update(blob);
                } catch (const NotStereoBlobError& e) {
                    cerr << "Got exception: " << e.what() << endl;
                    cerr << "Details about the bad blob:" << endl;
                    cerr << "blob.blobType = " << e.getBlob().blobType << endl;
                    cerr << "blob.version = " << e.getBlob().version << endl;
                    cerr << "blob.sensorId = " << e.getBlob().sensorId << endl;
                    cerr << "blob.frameId = " << e.getBlob().frameId << endl;
                    cerr << "blob.timestamp = " << e.getBlob().timestamp << endl;
                }
            }

        }

        /*! Update the state in time (tracking), without processing any new
         * actual measure.
         */
        void updateState()
        {
            /*
            // PSEUDO-CODE, TODO: implement me!!!
            tracker.update(timestamp);
            */
        }
    

        void updateStderrThread()
        {
            while (!quit) {
                usleep(50000);
                AutoMutex am(cotkMtx);
                cotk_update_stderr(console);
            }
        }

        // ---------------------------
        // MAIN LOOP
        // ---------------------------

        static void sigintHandler(int sig);

        int run()
        {
            //int lastBlobID = -1; // not used
            // catch CTRL-C and exit cleanly, if possible
            signal(SIGINT, StereoObsPerceptor::sigintHandler);
            // and SIGTERM, i.e. when someone types "kill <pid>" or the like
            signal(SIGTERM, StereoObsPerceptor::sigintHandler);

            if (useConsole()) {
                initConsole();
            }

            if (opengl) {
                initOpenGL(/*argc, argv*/);
            }

            // main loop
            while (!quit) {

                if (quit)
                    break;

                handleProcessControl();

                blob.setSensorId(sensorId);
                blob.setType(SENSNET_STEREO_IMAGE_BLOB);
                bool newBlob = blob.read(sensnet);
                if (newBlob) {
                    displayStatus(true); // this handles keypresses too
                    // There was a new (unread) blob
                    processBlob(blob);
                } else {
                    // no new blobs, update state (prediction) ...
                    updateState();
                    displayStatus(false);
                    // wait for a blob at most 0.05 sec, still quite responsive to keyboard
                    //uwait(60000);
                    sensnet_wait(sensnet, 100);
                }
            

            } // end of main loop

            return 0;
        }

    };

    volatile sig_atomic_t StereoObsPerceptor::quit = 0;

    void StereoObsPerceptor::sigintHandler(int /*sig*/)
    {
        // if the user presses CTRL-C three or more times, and the program still
        // doesn't terminate, abort
        if (quit > 2) {
            abort();
        }
        quit++;
    }

} // end namespace



// main program, just calls StereoObsPerceptor::run()
int main(int argc, char** argv)
{
    //try {
        stereo_obs::StereoObsPerceptor app(argc, argv);
        return app.run();
/*
    } catch(stereo_obs::Error& e) {
        cerr << "Exception caught: " << e.what() << endl;
    } catch(std::exception& e) {
        cerr << "Caught std::exception " << typeid(e).name()
             << ": " << e.what() << endl;
    }
    // other exceptions, not derived from std::exception, will just cause the program to abort
    return -1;
*/
}
