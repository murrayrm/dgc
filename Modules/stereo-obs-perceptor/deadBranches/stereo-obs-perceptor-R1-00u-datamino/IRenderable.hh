#ifndef __RENDERABLE_HH__
#define __RENDERABLE_HH__

#include "ILockable.hh"

namespace stereo_obs
{

    class Error;

    /**
     * Interface (abstract class) for some kind of object able to display
     * someting with opengl.
     */
    class IRenderable : virtual public ILockable
    {
    public:

        virtual ~IRenderable() { }

        /** 
         * Renders the object using the current OpenGL context. An OpenGL context
         * will always be set up when calling this method, and it shouldn't be changed.
         * The camera is always at the position (0, 0, 0), looking in the x direction,
         * like in the vehicle frame.
         */
        virtual void render() throw(Error) = 0;

        void render() volatile throw(Error)
        {
            LockingPtr<IRenderable> self(this);
            self->render();
        }
    };

}

#endif
