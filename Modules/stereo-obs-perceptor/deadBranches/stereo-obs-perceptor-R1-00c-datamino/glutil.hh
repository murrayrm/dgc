#ifndef __GLUTIL_HH__
#define __GLUTIL_HH__

#include <GL/gl.h>
#include <GL/glu.h>
#include "Image.hh"

namespace blobstereo
{

#warning "TODO: move some of these function in a .cc file"

    inline void pushViewport()
    {
        // cut'n'pasted from sensviewer, little changes
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
    }

    inline void setImageCoord()
    {
        // cut'n'pasted from sensviewer, little changes
        int vp[4];
      
        // Get the viewport dimensions
        glGetIntegerv(GL_VIEWPORT, vp);
        
        // Shift and rescale such that (0, 0) is the top-left corner
        // and (cols, rows) is the bottom-right corner.
        glTranslatef(0.0, 1.0, 0);
        glScalef(2.0/vp[2], -2.0/vp[3], 1.0);
    }

    inline void popViewport()
    {
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
    }

    inline void imageToTexture(const Image& img, GLuint textureId, bool setParams = true)
    {
        glBindTexture(GL_TEXTURE_2D, textureId);
        
        if (setParams) {
            //glPixelStoref(GL_UNPACK_ALIGNMENT, 1);
            
            // linear interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            // no lightning or coloring
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
        }

        switch(img.getFormat()) {
        case Image::FORMAT_GRAY8: 
            gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img.getWidth(), img.getHeight(), GL_LUMINANCE,
                              GL_UNSIGNED_BYTE, img.getData());           
            break;
        case Image::FORMAT_GRAY16:
            gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img.getWidth(), img.getHeight(), GL_LUMINANCE,
                              GL_SHORT, img.getData());
            break;
        case Image::FORMAT_RGB:
            gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img.getWidth(), img.getHeight(), GL_RGB,
                              GL_UNSIGNED_BYTE, img.getData());
            break;
        }
    }

    /**
     * Creates a texture without changing the actual values of the original
     * pixels (no mipmapping, rescaling, etc).
     * If the size is not a power of two, a bigger texture is created, filled with zeros,
     * and the original data is copied in the upper left corner.
     * @param inWidth Width of the original image.
     * @param inWidth Height of the original image.
     * @param bpp Bytes per pixel of the image.
     * @param bpr Bytes per row of the image (>= bpp * width)
     * @param format The texture format, as required by glTexImage2D.
     * @param type The input data type, as required by glTexImage2D.
     * @param dataIn The raw image data.
     * @param[out] outWidth The resulting width of the texture.
     * @param[out] outHeight The resulting height of the texture.
     * @return The new size of the texture.
     */
    inline void buildPlainTexture(int inWidth, int inHeight, int bpp, int bpr,
                                  int format, int type, uint8_t* dataIn,
                                  int* outWidth, int* outHeight)
    {
        // let's do it manually, without mipmapping, our own way
        // find minimum power of 2 bigger than width and height
        int pw, ph;
        for (pw = 1; pw < 32; pw++) {
            if ((1L << pw) >= inWidth)
                break;
        }
        for (ph = 1; ph < 32; ph++) {
            if ((1L << ph) >= inHeight)
                break;
        }
        int width = 1 << pw;
        int height = 1 << ph;
        uint8_t* data = new uint8_t[width * height * bpp];
        // do not scale, copy image in the upper left part of the new one
        memset(data, 0, width * height * bpp);
        uint8_t* rowOut = data;
        uint8_t* rowIn = dataIn;
        for (int r = 0; r < inHeight; r++) {
            memcpy(rowOut, rowIn, inWidth * bpp);
            rowOut += width * bpp;
            //rowIn  += inWidth * bpp; // assumption: no extra bytes at the end of the row
            rowIn += bpr;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, format,
                     type, data);
        delete[] data;
        if (outWidth)
            *outWidth = width;
        if (outHeight)
            *outHeight = height;
    } 

}

#endif
