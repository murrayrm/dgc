Tue Sep 18 18:53:17 2007	datamino (datamino)

	* version R1-00r
	BUGS:  
	FILES: SENSNET_MF_LONG_STEREO.cfg(39371),
		SENSNET_MF_MEDIUM_STEREO.cfg(39371)
	Lowered height threshold back to 3m (was 6, needed with the old bad
	calibration). This way we shouldn't see overhanging tree branches
	as obstacles. Lowered low threshold on the medium range to get
	bigger cluster instead of a lot of small obstacles. Helps with cars
	for example.

Thu Sep 13 22:11:13 2007	datamino (datamino)

	* version R1-00q
	BUGS:  
	FILES: BlobStereo.cc(38689), DisparityDetector.cc(38689),
		DisparityDetector.hh(38689),
		DisparityDetectorDebug.cc(38689), Makefile.yam(38689)
	- Changing the way track IDs are assigned, should be more robust
	and bugproof (?), should not use the same id twice or invalid ids
	anymore. - Added an option in the Makefile to disable assertions,
	just compile with "ymk all ASSERT=0"

	FILES: SENSNET_MF_LONG_STEREO.cfg(38692),
		SENSNET_MF_MEDIUM_STEREO.cfg(38692)
	Raised height threshold from 3m to 6m, was filtering too much.

Thu Aug 30 18:34:15 2007	datamino (datamino)

	* version R1-00p
	BUGS:  
	FILES: DisparityDetector.cc(36621), DisparityDetector.hh(36621)
	Implemented track ids between [0, 499], reusing them when no new
	ones are available. Also, removed the old DisparityDetector
	constructor.

	FILES: DisparityDetector.cc(36703)
	Setting the z coordinate in the MapElements (geometry, position,
	center). The z coordinate in the position is the center of the
	obstacle, the bottom and top can be easily calculated using the
	height. The center is set to be equal to the position.

Tue Aug 28 13:36:16 2007	datamino (datamino)

	* version R1-00o
	BUGS:  
	FILES: BlobStereo.cc(35823), DisparityDetector.cc(35823),
		DisparityDetector.hh(35823), blobstereo_cmdline.c(35823),
		blobstereo_cmdline.ggo(35823), blobstereo_cmdline.h(35823)
	Added cmdline options for the fuzzy height threshold
	(--height-thres and --hthres-sigma). Also added cmdline option to
	change the subgroup obstacles are sent to.

	FILES: DisparityDetector.cc(35720)
	Prototype of a height filter to remove floating obstacles (tree
	leaves). Does not remove ground strikes, but maybe it can be
	patched to do that too.

	FILES: DisparityDetector.cc(35908)
	Fixed bug in row calculation when using height threshold

	FILES: DisparityDetectorDebug.cc(35702)
	Color-code 3D obstacles based on likelihood (0.0=blue, 1.0=red).

	FILES: Makefile.yam(35911)
	Added -lbitmap_gl to compile with the latest bitmap release

	FILES: blobstereo_cmdline.c(35919), blobstereo_cmdline.h(35919)
	committing for release

	FILES: blobstereo_cmdline.ggo(35913)
	Changed default values for height thresholding

Fri Aug 24  3:45:22 2007	datamino (datamino)

	* version R1-00n
	BUGS:  
	New files: SENSNET_MF_LONG_STEREO.cfg SENSNET_MF_MEDIUM_STEREO.cfg
	FILES: BlobStereo.cc(35379), Detector.hh(35379),
		DisparityDetector.cc(35379), DisparityDetector.hh(35379),
		Makefile.yam(35379), blobstereo_cmdline.c(35379),
		blobstereo_cmdline.ggo(35379), blobstereo_cmdline.h(35379)
	Refactored the way obstacles are passed from DisparityDetector to
	the main program (now detect() directly fills a	vector<MapElement>).
	Added an option to control the minimum disparity (cut objects too
	far away). Added option to send the alice MapElement (previously was
	done by default, but this may be confusing, it's off by default).
	Renamed main executable from blobstereo to stereo-obs-perceptor.
	To be backward compatible, a link blobstereo -> stereo-obs-perceptor
	is created in the i486-gentoo-linux directory.

	FILES: BlobStereo.cc(35380), blobstereo_cmdline.c(35380),
		blobstereo_cmdline.ggo(35380), blobstereo_cmdline.h(35380)
	Added configuration files support. Added shortcut options --long
	and --medium to select medium or long range. Properly catch
	exceptions thrown in the BlboStereo constructor (no more core dumps
	for a bad cmdline option).

	FILES: BlobStereo.cc(35382), DisparityDetector.cc(35382),
		DisparityDetector.hh(35382),
		DisparityDetectorDebug.cc(35382),
		blobstereo_cmdline.c(35382), blobstereo_cmdline.ggo(35382),
		blobstereo_cmdline.h(35382)
	Implemented a maximum number of tracks in DisparityDetector. The
	m_tracks array is preallocated and never grown (or shrunk) at any
	later point. When a new track needs to be created and the array is
	full, the track with the lowest confidence is replaced. Removed
	some debugging messages for the release.

	FILES: Makefile.yam(35381)
	Linking config files into etc/stereo-obs-perceptor.

Wed Aug 22 18:08:05 2007	datamino (datamino)

	* version R1-00m
	BUGS:  3472, 3441
	FILES: DisparityDetector.cc(34989)
	Reverted Sam's changes (sorry :-P) and implemented a better fix (I
	hope): merging tracks which are too close to each other. Now the
	number of tracks is well below what it used to be, but this fix may
	slow down the perceptor (or make it faster, since we have less
	tracks...). Still, compiling with optimizations should be fast
	enough though.

Wed Aug 22  6:25:43 2007	Sam Pfister (sam)

	* version R1-00l
	BUGS:  
	FILES: DisparityDetector.cc(34874)
	temporary fix for a bug which would cause number of tracks to grow
	without bound due to multiple associations of overlapping objects.

Mon Aug 20 21:33:06 2007	datamino (datamino)

	* version R1-00k
	BUGS: 3392
	Deleted files: Image.hh
	FILES: BlobStereo.cc(30488), GlRenderer.cc(30488),
		GlRenderer.hh(30488)
	Removed obsolete Image and ImagePairRenderer classes.

	FILES: BlobStereo.cc(30924)
	Hack to fix bug #3392: Added a new thread, scheduled at 20Hz, to
	update stderr dislay in cotk (using a private cotk function,
	cotk_update_stderr, this is why it's a hack).

	FILES: BlobStereo.cc(34261), Detector.hh(34261),
		DisparityDetector.cc(34261)
	Fixed: hitting 'n' or 'p' (next/prev obstacle )caused a deadlock.
	Fixed: Now showing the correct number of obstacles and clear
	messages sent to the mapper, no more, no less (not including
	Alice's pos, which is always sent).

	FILES: BlobStereo.cc(34432), Detector.hh(34432),
		DisparityDetector.cc(34432), DisparityDetector.hh(34432)
	Trying to reduce the number of messages sent to the mapper. Not
	sending messages for tracks which weren't updated with new measures
	anymore. Fixed a bug that was sending clear messages for tracks
	which were never sent to the mapper at all, because their
	confidence level was too low.

	FILES: DisparityDetectorDebug.cc(30923)
	Reimplemented region geometry display in the "Detector" view.

	FILES: glutil.hh(30491)
	Finished removing Image class, deleted Image.hh.

Tue Jul 24 20:52:54 2007	datamino (datamino)

	* version R1-00j
	BUGS: 3392
	New files: DisparityDetectorDebug.cc
	Deleted files: ImageCv.cc ImageCv.hh
	FILES: BlobStereo.cc(29901), Detector.hh(29901),
		DisparityDetector.cc(29901), DisparityDetector.hh(29901),
		Makefile.yam(29901), Obstacle.hh(29901), README(29901),
		blobstereo_cmdline.c(29901), blobstereo_cmdline.ggo(29901),
		blobstereo_cmdline.h(29901), glutil.hh(29901),
		util.hh(29901)
	Merging changes with the latest release

	FILES: BlobStereo.cc(30068), DisparityDetector.cc(30068),
		Obstacle.hh(30068)
	Fixed bug in height computation, now values seem more reasonable
	than before. Also setting height and elevation in the MapElement
	sent. Minor: don't pass arguments to fltk, it doesn't need them ;-)

	FILES: BlobStereo.cc(30198), Detector.hh(30198),
		DisparityDetector.cc(30198), DisparityDetector.hh(30198),
		blobstereo_cmdline.c(30198), blobstereo_cmdline.ggo(30198),
		blobstereo_cmdline.h(30198)
	- Fixed a bug in the computation of the convex hull while
	associating measurements with tracks. Now association seems to work
	well enough. - Detector::detect() now returns a vector of Track
	(class Obstacle is not used anymore), and takes a pointer to a
	vector<int> deleted, to be filled with the deleted track ids. -
	Thanks to the above, CLEAR MapElements are correctly sent to mapper
	when needed. - Cotk Display: now showing obstacle index and id (no
	more equal). - Changed some default parameters (tuning).

	FILES: BlobStereo.cc(30201), DisparityDetector.cc(30201),
		DisparityDetector.hh(30201), blobstereo_cmdline.c(30201),
		blobstereo_cmdline.ggo(30201), blobstereo_cmdline.h(30201)
	Added a commandline option to disable tracking. Some cleanup.

	FILES: BlobStereo.cc(30204), DisparityDetector.cc(30204),
		blobstereo_cmdline.c(30204), blobstereo_cmdline.ggo(30204),
		blobstereo_cmdline.h(30204)
	Added commandline option to set the minimum confidence value for an
	obstacle to be sent to the mapper. Since the planner is not using
	the confidence AFAIK, the default is a fairly high value, 0.8.
	Removed some debugging messages to temporary fix bug #3392.

	FILES: GlRenderer.hh(30203), Makefile.yam(30203)
	Removed ImageCv class, replaced by Bitmap (bitmap was originally
	derived from ImageCv, and moved to its own module).

	New files: DisparityDetector.old.cc DisparityDetector.old.hh
		DisparityDetectorDebug.cc
	FILES: BlobStereo.cc(28570), README(28570)
	Minor changes, just to sync my laptop with the lab computers

	FILES: BlobStereo.cc(28815), Detector.hh(28815),
		Makefile.yam(28815), blobstereo_cmdline.c(28815),
		blobstereo_cmdline.h(28815)
	work in progress 2 ...

	FILES: BlobStereo.cc(28965), DisparityDetector.cc(28965),
		DisparityDetector.hh(28965)
	Calculating disparity for sensed obstacles. It seems much more
	reasonable than the older version, but still I'm not sure if it's
	correct (need to do some checks), and it's based on some
	assumptions that may or may not be true.

	FILES: BlobStereo.cc(29301)
	Small changes in BlobStereo.cc (position and center of MapElement).
	Added the skeleton of ObstacleRenderer3D, which is supposed to
	render obstacles and measures as 3D objects using OpenGL, in the
	future.

	FILES: BlobStereo.cc(29894), DisparityDetector.cc(29894),
		DisparityDetector.hh(29894), blobstereo_cmdline.c(29894),
		blobstereo_cmdline.ggo(29894), blobstereo_cmdline.h(29894)
	Added commandline options for most of the detection algorithm
	parameters (merged changes from the latest release).

	FILES: Detector.hh(29299)
	Added (and corrected) some doxygen comments.

	FILES: DisparityDetector.cc(28816), DisparityDetector.hh(28816)
	Trying to make the code easied to understand and maintain,
	refactored DisparityDetector. Still work in progress ...

	FILES: DisparityDetector.cc(28904), DisparityDetector.hh(28904),
		Obstacle.hh(28904)
	work in progress: the new code correctly detects obstacles and
	sends them to the mapper, without tracking

	FILES: DisparityDetector.cc(29015)
	copied association code from the old version to the new one (not
	working)

	FILES: DisparityDetector.cc(29262), DisparityDetector.hh(29262),
		Makefile.yam(29262)
	Ported data association from the old code. Implemented association
	using covariance instead of just euclidean distance. It needs
	debugging and tuning, but does something vaguely reasonable ...
	Moved OpenGL and debugging code from DisparityDetector.cc to
	DisparityDetectorDebug.cc.

	FILES: DisparityDetector.cc(29263)
	Cleanup

	FILES: DisparityDetector.cc(29300), DisparityDetector.hh(29300)
	Moved the measures vector from local to member variable (this way
	the memory will be reused and not reallocated every time). Also
	changes some parameters.

	FILES: DisparityDetector.cc(29797), DisparityDetector.hh(29797),
		glutil.hh(29797), util.hh(29797)
	Changed the ObstacleRenderer to show obstacles in vehicle frame
	instead of local frame. Added a 3D obstacle renderer, that clearly
	shows how the obstacle height is completely wrong ... need to
	investigate.

	FILES: Obstacle.hh(28604), util.hh(28604)
	Working with the data association code, added a class Track to hold
	a track (obstacle) over time. Seems to work better now, still very
	basic/no filtering, etc, needs a lot of work.

	FILES: README(29022)
	Added bitmap dependency

	FILES: blobstereo_cmdline.ggo(28611)
	Fixed name (was obviously copy-pasted from sensnetreplay)

Fri Jul 20  3:59:15 2007	datamino (datamino)

	* version R1-00i
	BUGS:  
	FILES: BlobStereo.cc(29836), DisparityDetector.cc(29836),
		DisparityDetector.hh(29836), blobstereo_cmdline.c(29836),
		blobstereo_cmdline.ggo(29836), blobstereo_cmdline.h(29836)
	Added commandline options for most of the detector parameters, for
	easier tuning.

Thu Jul 19 17:54:43 2007	datamino (datamino)

	* version R1-00h
	BUGS:  
	FILES: BlobStereo.cc(29691)
	Commented unneeded #include that makes compilation fail

Fri Jun 15 16:45:41 2007	datamino (datamino)

	* version R1-00g
	BUGS:  
	New files: ILockable.hh IRenderable.hh IUpdatable.hh
	FILES: BlobStereo.cc(25747), blobstereo_cmdline.c(25747),
		blobstereo_cmdline.ggo(25747), blobstereo_cmdline.h(25747)
	Improved the description of --sensor-id and --module-id options,
	added the module id to the console display.

	FILES: BlobStereo.cc(26129), Detector.hh(26129),
		DisparityDetector.cc(26129), DisparityDetector.hh(26129),
		GlRenderer.cc(26129), GlRenderer.hh(26129),
		ImageCv.cc(26129), ImageCv.hh(26129), Obstacle.hh(26129),
		blobstereo_cmdline.c(26129), blobstereo_cmdline.ggo(26129),
		blobstereo_cmdline.h(26129), util.hh(26129)
	- OpenGL display now off by default (can be turned on on the fly
	from the console display). - Created two interfaces, IUpdatable and
	IRenderable and made GlRenderer derive from these.   This was
	needed to be able to have OpenGL widgets that don't need to be
	updated with a	 SensnetBlob. - New display, MinRow, showing the
	minimum row for each map cell (trying to   detect overhanging trees
	and such). - Calculated uncertainty information for each obstacle
	detected. Uncertainty in image frame   (col, row, disp) is assumed
	to be constant and set to some arbitrary value. This is   then
	transformed to local frame, whic yields bigger uncertainty for
	distant obstacles. NOTE: not sure about how point2_uncertain stores
	uncertainty, i.e. the exact meaning of min_var, max_var and axis,
	and how to get those from a covariance matrix. NOTE2: This code
	relies on a modified version of mat44.h, which is not released yet.

	FILES: BlobStereo.cc(27521)
	Implemented process control (ProcessRequest and ProcessResponse).

	FILES: BlobStereo.cc(27847), Detector.hh(27847),
		DisparityDetector.cc(27847), DisparityDetector.hh(27847),
		GlRenderer.cc(27847), GlRenderer.hh(27847), util.hh(27847)
	- Implemented removal of small obstacles (threshold is hardcoded,
	need to make it, and all the parameters of the detector, a command
	line or config file option). - Fixed bug in the detector display,
	it was drawn before the detector was updated, resulting in showing
	the previous frame instead of the current one. - Trying to use
	volatile objects and methods to implement critical sections. Seems
	to work, but since not everybody knows how it works, it makes the
	code less readable. Maybe I'll revert back.

	FILES: BlobStereo.cc(27855), DisparityDetector.cc(27855),
		DisparityDetector.hh(27855)
	Separated display of obstacles in local frame from map display

	FILES: BlobStereo.cc(27988), DisparityDetector.cc(27988),
		DisparityDetector.hh(27988), Obstacle.hh(27988)
	- First implementation of a (simple) data association and tracking
	filter. Not working, Needs a lot of tuning and debugging. - Changed
	the way the variance is passed to the mapper. A lot of tuning and
	debugging needed here, too. - Some random cleanups (old code
	removal)

	FILES: BlobStereo.cc(28004)
	Added printing of covariance matrices (commented out for the
	release).

	FILES: util.hh(25581)
	Fixed error message formatting

	FILES: util.hh(28003)
	Added a function to print a 4x4 matrix

Fri May 25 12:07:09 2007	datamino (datamino)

	* version R1-00f
	BUGS:  
	FILES: BlobStereo.cc(23781), DisparityDetector.cc(23781),
		DisparityDetector.hh(23781), GlRenderer.cc(23781)
	Merge with previous branch

	FILES: BlobStereo.cc(24210), DisparityDetector.cc(24210),
		DisparityDetector.hh(24210), GlRenderer.cc(24210),
		GlRenderer.hh(24210), Makefile.yam(24210),
		Obstacle.hh(24210)
	Moving from glut to fltk for a more flexible UI. Working but not
	fully functional.

	FILES: BlobStereo.cc(24351), Detector.hh(24351),
		DisparityDetector.cc(24351), DisparityDetector.hh(24351),
		GlRenderer.cc(24351), GlRenderer.hh(24351),
		blobstereo_cmdline.c(24351), blobstereo_cmdline.ggo(24351),
		blobstereo_cmdline.h(24351)
	Split opengl display in multiple rendering objects, and draw them
	in separated tabs using fltk. In progress ...

	FILES: BlobStereo.cc(24391), DisparityDetector.cc(24391),
		DisparityDetector.hh(24391)
	Finished adapting the display to the new tabbed window using fltk,
	needs some cleanup though. Now it's modular, functional, but I
	don't like it :-P need to think of a better UI design ... good
	thing that it's modular, should be easy now.

	FILES: BlobStereo.cc(24729), GlRenderer.cc(24729),
		GlRenderer.hh(24729), SensnetBlob.hh(24729)
	display code cleanup

	FILES: GlRenderer.cc(24212), GlRenderer.hh(24212)
	Mostly working fltk/opengl display with tabs.

	FILES: GlRenderer.cc(24727)
	Better and more flexible OpenGL display using FLTK. Now you can
	select which views you want to see at the same time, and have them
	tiled side by side.

	FILES: GlRenderer.cc(24931)
	Fixed a deadlock, and possible race conditions, Fl::lock() must be
	locked before calling Fl::run(), and unlocked afterwards (otherwise
	Fl::wait will unlock an unlocked mutex).

	FILES: Makefile.yam(24211)
	Fixed usage of fltk-config in Makefile.yam.

Sun Apr 29  0:18:54 2007	datamino (datamino)

	* version R1-00d
	BUGS:  
	FILES: BlobStereo.cc(21384), DisparityDetector.cc(21384),
		blobstereo_cmdline.c(21384), blobstereo_cmdline.ggo(21384),
		blobstereo_cmdline.h(21384)
	- Added detected obstacles to the console display.
	- Removed output log commandline options and cotk button (no log
	  writing was implemented anyway).

	FILES: DisparityDetector.cc(21158), DisparityDetector.hh(21158)
	Cleaning up the code. Changed some variable and function names,
	removed some old code.

	FILES: DisparityDetector.cc(21432)
	small tweak to the gl display for the midrange camera.

Thu Apr 26 19:28:54 2007	datamino (datamino)

	* version R1-00c
	BUGS:  
	FILES: BlobStereo.cc(20975), DisparityDetector.cc(20975),
		DisparityDetector.hh(20975), Makefile.yam(20975),
		Obstacle.hh(20975)
	Finally sending obstacles to the mapper! Showing the number of
	obstacles sent in the cotk display. Fixed a bug in
	DisparityDetector, forgot to convert from cell coords back to
	disparity.

	FILES: Makefile.yam(20980)
	Computers in the lab don't support SSE3 instructions, changed
	optimizing flag from -msse3 to -msse2.

Thu Apr 26  2:08:48 2007	datamino (datamino)

	* version R1-00b
	BUGS:  
	New files: DisparityDetector.cc DisparityDetector.hh Image.hh
		ImageCv.cc ImageCv.hh glutil.hh
	Deleted files: GlDisplay.hh
	FILES: BlobStereo.cc(20149), Detector.hh(20149),
		GlRenderer.cc(20149), GlRenderer.hh(20149),
		Makefile.yam(20149), Obstacle.hh(20149)
	Implementing a simple detector, translating some matlab code to
	C++. Added two images to the debug display, showing disparity and a
	disparity accumulator with coords (cols, disparity).

	FILES: BlobStereo.cc(20504), GlRenderer.hh(20504),
		Makefile.yam(20504)
	Starting to use OpenCv to detect blobs, still incomplete (still not
	outputting any object). Added a lot of stuff in the debug display
	(disparity, detected blobs).

	FILES: Makefile.yam(20794)
	Changed the blob detection algorithm, now using two thresholds as I
	was doing in the matlab code. Added a lot of debugging code and
	debug visualizations. At the moment, all the visualizations are
	enabled and disabled with #if 0|1 ... #endif directly in the code.
	Still not tracking obstacles, and not sending anything to the
	mapper.

Wed Apr 18 20:53:09 2007	datamino (datamino)

	* version R1-00a
	BUGS:  
	New files: BlobStereo.cc Detector.hh GlDisplay.hh GlRenderer.cc
		GlRenderer.hh Obstacle.hh SensnetBlob.hh SensnetLog.hh
		blobstereo_cmdline.c blobstereo_cmdline.ggo
		blobstereo_cmdline.h util.hh
	FILES: Makefile.yam(19936)
	First version of the stereo obstacle detector ("blobstereo"). This
	is just a shell, no detection is performed.

Wed Apr 18 18:35:29 2007	datamino (datamino)

	* version R1-00
	Created stereo-obs-perceptor module.




















