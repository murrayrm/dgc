/* Emacs clue: -*-  indent-tabs-mode:nil; c-basic-offset:4; -*-*/
/*!
 * \file DisparityDetector.cc
 * \brief Obstacle detector based on stereo disparity
 *
 * \author Daniele Tamino
 * \date 10 July 2007
 *
 * This file contains the implementation of the DisparityDetector class, which given
 * a sensnet StereoImageBlob blob, containing disparity information, outputs a set
 * of obstacles. This module can track the obstacles over time, but the reset()
 * method can be called to clear the prior imformation.
 *
 */

// standard C++ headers
#include <iostream>
#include <vector>
#include <limits>
#include <cmath>
#include <algorithm>
// OpenCV headers
#include <cv.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <frames/mat44.h>
#include <dgcutils/AutoMutex.hh>
#include <interfaces/StereoImageBlob.h>
// local headers
#include "blobstereo_cmdline.h"
#include "DisparityDetector.hh"
#include "IUpdatable.hh"
#include "util.hh"

namespace blobstereo
{
    using namespace std;
    using namespace boost;
    
    void calcPointCov(float cov[4][4], const float pi[3],
                      const float himg2sens[4][4], const float sens2loc[4][4],
                      const float errorVar[3]);
    
    /*!
     * This function is used, when the debugging flag is on, to force OpenCV to abort and
     * generate a core dump in case of error.
     */
    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
        abort();
    }
    
    DisparityDetector::DisparityDetector(int dbg , int maxDisp, int mapHeight,
                                         double hiThres, double loThres,
                                         float smallObsThres, float assocThres)
        : m_mapMax(0), m_nextId(0), m_lastTs(0),
          m_maxDisp(maxDisp), m_mapHeight(mapHeight),
          m_highThres(hiThres), m_lowThres(loThres),
          m_smallObsThres(smallObsThres), m_assocThres(assocThres),
          m_disableTracking(false), m_debug(dbg)
    {
        pthread_mutex_init(&m_mtx, NULL);
        if (m_debug) {
            cvRedirectError(abortOnCvError);
        }

        // arbitrary values ...
        m_errorVar[0] = 64;
        m_errorVar[1] = 25;
        m_errorVar[2] = 1;
    }

    DisparityDetector::DisparityDetector(gengetopt_args_info& options)
        : m_mapMax(0), m_nextId(0), m_lastTs(0),
          m_maxDisp(options.max_disp_arg),
          m_minDisp(options.min_disp_arg),
          m_mapHeight(options.map_height_arg),
          m_highThres(options.high_thres_arg),
          m_lowThres(options.low_thres_arg),
          m_smallObsThres(options.small_obs_thres_arg),
          m_assocThres(options.assoc_thres_arg),
          m_disableTracking(options.disable_tracking_flag),
          m_confThres(options.conf_thres_arg),
          m_maxTracks(options.max_tracks_arg),
          m_heightThres(options.height_thres_arg),
          m_heightThresSigma(options.hthres_sigma_arg),
          m_debug(options.debug_arg)
    {
        pthread_mutex_init(&m_mtx, NULL);
        if (m_debug) {
            cvRedirectError(abortOnCvError);
        }

        this->moduleId = modulenamefromString(options.module_id_arg);
        if (this->moduleId <= 0)
            throw ERROR("invalid module id: " + string(options.module_id_arg));
 
        istringstream is(options.error_var_arg);
        is >> m_errorVar[0] >> m_errorVar[1] >> m_errorVar[2];
        if (!is) {
            throw ERROR("The error variance should be 3 numbers, like 'Sx Sy Sdisp', got "
                        + toStr(options.error_var_arg) + " instead");
/*
            m_errorVar[0] = 2;
            m_errorVar[1] = 4;
            m_errorVar[2] = 2;
*/
        }

        reset(); // initialize the m_tracks array
    }

    void DisparityDetector::reset()
    {
        m_tracks.clear();
        m_tracks.resize(m_maxTracks);
        for (int i = 0; i < m_maxTracks; i++)
            m_tracks[i].flags = TRACK_DELETED;
        m_toSort.reserve(m_maxTracks);
    }

    void DisparityDetector::detect(const SensnetBlob& blob, vector<MapElement>* elements)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        
        if (sib->version != 4) {
            if (sib->version < 4) {
                ERRMSG("StereoImageBlob version " << sib->version << " not supported!!!");
                return;
            } else {
                MSG("StereoImageBlob version " << sib->version << " newer than 4, continuing");
            }
        }
        
        // initialize the map
        if (m_mapImg.getWidth() != sib->cols) {
            // allocate memory - this should only happen the first time detect() is called
            m_mapImg.init(sib->cols, m_mapHeight);
            m_maskImg.init(sib->cols + 2, m_mapHeight + 2);
            m_rowSum.init(sib->cols, m_mapHeight);
            m_rowSumSq.init(sib->cols, m_mapHeight);
        } else {
            cvZero(m_mapImg.getImage());
            cvZero(m_maskImg.getImage());
            cvZero(m_rowSum.getImage());
            cvZero(m_rowSumSq.getImage());
        }

        // use the disparity image in 'sib' to fill m_mapImg
        buildMap(sib);

        // given the new map in m_mapImg, find connected regions using low and high
        // thresholds, and put them in m_regions. This filters small regions too.
        findRegions(sib);

        // given the detected regions in m_regions as the current observations, update
        // the filtered tracks in m_tracks, and fill the elements vector
        updateTracks(sib, elements);

/*
        // given the current tracks in m_tracks, return a vector<Obstacle> as required,
        // applying all the coordiates transformations to get positions in local frame
        //obstaclesFromTracks(sib);
        return m_result;

        // finally, return the current tracks to the caller
        return m_tracks;
*/

    }


    /*!
     * Given the current stereo blob, containing disparity information,
     * build a map accumulating vertically points with the same disparity.
     * The map is written to m_mapImg.
     * \param sib The new stereo blob
     */
    void DisparityDetector::buildMap(StereoImageBlob* sib)
    {
        // conversion from disparity image to map cell index
        float disp2cell = float(m_mapHeight) / (m_maxDisp * sib->dispScale);
        float blob2disp = 1.0 / sib->dispScale;
        // minimum interesting disparity - cut things at infinity
        int16_t minDisp = int16_t(m_minDisp * sib->dispScale + 0.9);
        m_mapMax = 0;

        float sens2veh[4][4];
        memcpy(sens2veh, sib->leftCamera.sens2veh, sizeof(sens2veh));
        //float groundZ = sib->state.localZ;
        //MSG("pixel weights:");
        for (int r = 0; r < sib->rows; r++)
        {
            for (int c = 0; c < sib->cols; c++)
            {
                int16_t d = *(int16_t*) StereoImageBlobGetDisp(sib, c, r);
                if (d >= minDisp)
                {
                    unsigned int cell = unsigned(d * disp2cell + 0.5);
                    
                    if (cell >= m_mapHeight)
                        continue;

                    float w = 1.0;
                    if (true/*m_heightFilter*/)
                    {
                        // transform the height from pixels into meters -- maybe there's a better way ...
                        float p[3] = { c, r, d * blob2disp };
                        float q[3];
                        StereoImageBlobImageToSensor(sib, p[0], p[1], p[2], &q[0], &q[1], &q[2]);
                        //StereoImageBlobSensorToVehicle(sib, q[0], q[1], q[2], &p[0], &p[1], &p[2]);
                        float z = sens2veh[2][0]*q[0] + sens2veh[2][1]*q[1] + sens2veh[2][2]*q[2] + sens2veh[2][3];
                        float h = -z;//fabs(z - groundZ);
                        if (h > m_heightThres) { 
                            float hr = (h - m_heightThres) / m_heightThresSigma;
                            w = exp(-0.5 * hr * hr);
                        }
                        //cerr << w << ' ';
                    }

                    dispmap_t val = m_mapImg[cell][c][0] + w;
                    m_mapImg[cell][c][0] = val;
                    if (val > m_mapMax)
                    {
                        m_mapMax = val;
                    }
                    m_rowSum[cell][c][0]   += r * w;
                    m_rowSumSq[cell][c][0] += r*r * w;
                }
            }
            //cerr << '\n';
        }
        //cerr << endl;
        //MSG("end heights");

        //MSG("mapMax = " << m_mapMax);
        
    }

    /*!
     * Given the current map in m_mapImg, find the connected regions using
     * two thresholds (m_lowThres and m_hiThres). The high threshold is used to
     * find peaks in the map, then for each peak the region is expanded to all
     * the connected cells with a value bigger than the low threshold.
     * The result of this is stored in m_regions.
     * \param sib The stereo blob the map was built from
     */
    void DisparityDetector::findRegions(StereoImageBlob* /*sib*/)
    {
        float cell2disp = float(m_maxDisp) / float(m_mapHeight);

        m_regions.clear();

        // we need the underlying IplImage to call OpenCV functions
        IplImage* map = m_mapImg.getImage();

        // valid region labels start at 2 (0 means not assigned, 1 is used internally)
        int label = 2;
        int height = m_mapImg.getHeight();
        int width = m_mapImg.getWidth();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                dispmap_t val = m_mapImg[r][c][0];
                // find peaks using the high threshold
                if (val > m_highThres)
                {
                    dispmap_t loDiff = val - m_lowThres;
                    dispmap_t hiDiff = m_mapMax - val + 1;
                    if (hiDiff <= 0) {
                        hiDiff = 0;
                        MSG("Found map value " << val << " bigger than mapMax = " << m_mapMax);
                    }
                    CvConnectedComp comp;

                    // if this cell belongs to a previously detected region, skip it
                    if (m_maskImg[r+1][c+1][0] != 0)
                        continue;

                    // find the connected component using the low threshold
                    cvFloodFill(map, cvPoint(c, r), cvRealScalar(1),
                                cvRealScalar(loDiff), cvRealScalar(hiDiff),
                                &comp, CV_FLOODFILL_FIXED_RANGE | CV_FLOODFILL_MASK_ONLY | 4,
                                m_maskImg.getImage());
                    
                    if (comp.rect.width > 0 && comp.rect.height > 0)
                    {
                        Region reg;
                        reg.id = label++;
                        reg.rect = comp.rect;
                        reg.weight = 0;
                        reg.row = 0;
                        reg.rowStDev = 0;

                        // store all the points in a vector AND in an opencv matrix ...
                        point2arr points;
                        CvMat ptMat = cvMat(reg.rect.width * reg.rect.height, 1, CV_32FC2);
                        cvCreateData(&ptMat);
                        float* mat = (float*) ptMat.data.ptr;
                        for (int r = reg.rect.y; r < reg.rect.y + reg.rect.height; r++)
                        {
                            for (int c = reg.rect.x; c < reg.rect.x + reg.rect.width; c++)
                            {
                                if (m_maskImg[r+1][c+1][0] == 1)
                                {
                                    m_maskImg[r+1][c+1][0] = reg.id;
                                    reg.row += m_rowSum[r][c][0];
                                    reg.rowStDev += m_rowSumSq[r][c][0];
                                    reg.weight += m_mapImg[r][c][0];
                                    points.push_back(point2(c, r * cell2disp));
                                    *mat++ = c;
                                    *mat++ = r * cell2disp;
                                }
                            }
                        }
                        assert(points.size() > 0);
                        ptMat.rows = points.size();

                        // filter out small regions
                        if (reg.weight < m_smallObsThres) {
                            //MSG("Filtering out region " << reg.id << ", map sum was "
                            //    << reg.weight << ") < " << m_smallObsThres);
                            cvReleaseData(&ptMat); // TODO: try to avoid allocating memory at all
                            continue;
                        }

                        // mean image row and standard deviation
                        if (reg.weight != 0) {
                            reg.row /= reg.weight;
                            reg.rowStDev = sqrt(reg.rowStDev / reg.weight - reg.row*reg.row);
                        } else {
                            reg.row = reg.rowStDev = 0.0;
                        }

                        // ... use the point2arr to fit an ellipse to the points ...
                        reg.el.add_points(points);
                        reg.el.finish();
                        
                        // ... and the opencv matrix to calculate the convex hull ...
                        // .. but first allocate ANOTHER opencv matrix for the result ...
                        CvMat hull = cvMat(points.size(), 1, CV_32FC2);
                        cvCreateData(&hull);
                        // ... do the hard work ...
                        cvConvexHull2(&ptMat, &hull, CV_CLOCKWISE, 1 /* points, not indexes */);
                        // ... and copy the result back to the Region struct ...
                        mat = (float*) hull.data.ptr;
                        reg.convexHull.resize(hull.rows * hull.cols);
                        for (unsigned int i = 0; i < reg.convexHull.size(); i++)
                        {
                            float r = *mat++;
                            float c = *mat++;
                            reg.convexHull[i] = point2(r, c);
                        }
                        // ... then finally release all the memory allocated for opencv.
                        cvReleaseData(&ptMat);
                        cvReleaseData(&hull);
#warning "FIXME: Need to think of a way to avoid all these memory (de)allocation and copying of data."

                        m_regions.push_back(reg);
                    } else { // connected component was zero-sized
                        ERRMSG("BUGBUG: cvFloodFill() did nothing, was the seed point masked???");
                        ERRMSG("BUGBUG: seed point: (c=" << c << ", r=" << r << ")" );
                    }
                } // end of if (bigger than high threshold)
            } // end of for (all columns)
        } // end of for (all rows)
    }

    /*! Convert from image frame to local frame
     * \param s Source vector [col, row, disp]
     * \param d Destination vector [x, y, z]
     */
    void convertImageToLocal(StereoImageBlob* sib, float* s, float* d)
    {
        // do all the trasformations required
        StereoImageBlobImageToSensor(sib, s[0], s[1], s[2], &d[0], &d[1], &d[2]);
        StereoImageBlobSensorToVehicle(sib, d[0], d[1], d[2], &s[0], &s[1], &s[2]);
        StereoImageBlobVehicleToLocal(sib, s[0], s[1], s[2], &d[0], &d[1], &d[2]);
    }

    /*!
     * Converts a Region object into a Track object, and while creating a new track
     * from a new measure that couldn't be associated with anything.
     * It converts from image to local frame and copied the relevant information.
     * Only used internally.
     */
    void regionToTrack(const Region& reg, Track* track, StereoImageBlob* sib,
                       const float himg2sens[4][4], const float sens2loc[4][4],
                       const float errorVar[3])
    {
        track->id = reg.id;
        track->flags = 0;
        track->updated(true);
        memset(track->state, 0, sizeof(track->state));

        // convert from image to local frame
        float tmp[3]; // temp storage (col, row, disp)
        tmp[0] = reg.el.center.x;
        tmp[1] = reg.row;
        tmp[2] = reg.el.center.y;
        convertImageToLocal(sib, tmp, track->state);

        // convert the convex hull and the ellipse to local frame too
        track->el = ellipse();
        track->convexHull.resize(reg.convexHull.size());
        for (unsigned int j = 0; j < track->convexHull.size(); j++)
        {
            float out[3];
            tmp[0] = reg.convexHull[j].x;
            tmp[1] = reg.row;
            tmp[2] = reg.convexHull[j].y;
            convertImageToLocal(sib, tmp, out);
            track->convexHull[j].x = out[0];
            track->convexHull[j].y = out[1];
        }
        // transform the height from pixels into meters -- maybe there's a better way ...
        float up[3] = { reg.el.center.x, reg.row - reg.rowStDev * 1.5, reg.el.center.y };
        float down[3] = { reg.el.center.x, reg.row + reg.rowStDev * 1.5, reg.el.center.y };
        float ul[3], dl[3]; // up local, down local
        convertImageToLocal(sib, up, ul);
        convertImageToLocal(sib, down, dl);
        track->height = fabs(ul[2] - dl[2]);

        float pi[3];
        pi[0] = reg.el.center.x;
        pi[1] = reg.row;
        pi[2] = reg.el.center.y;
        float cov[4][4];
        calcPointCov(cov, pi, himg2sens, sens2loc, errorVar);

        memset(track->var, 0, sizeof(track->var));
        // we only have the covariance for the position (we only have the position!)
        for (int j = 0; j < 3; j++)
            for (int k = 0; k < 3; k++)
                track->var[j][k] = cov[j][k];

        track->likelihood = 0.4; // FIXME: magic
        track->maxLikelihood = track->likelihood;
        track->rect = reg.rect;
    }


    // some fast 3x3 matrix inversion and determinant routines

    // return the determinant of the 3x3 matrix m
    template <class T>
    inline T fast_det_3x3(const T m[3][3])
    {
        // generated with matlab 6.5
        T det = (m[1][1]*m[2][2]-m[1][2]*m[2][1])*m[0][0]+m[2][0]*m[0][1]*m[1][2]-m[2][0]*m[0][2]*m[1][1]-m[1][0]*m[0][1]*m[2][2]+m[1][0]*m[0][2]*m[2][1];
        return det;
    }

    // m is a 3x3 matrix, det is det(m) precalculated. Use this if you know
    // the determinant already.
    template <class T>
    inline void fast_inv_3x3(const T m[3][3], T inv[3][3], T det)
    {
        assert(det >= 1e-15); // not singular
        T inv_det = 1 / det;
        // generated with matlab 6.5
        inv[0][0] = (m[1][1]*m[2][2]-m[1][2]*m[2][1])*inv_det;
        inv[0][1] = -(m[0][1]*m[2][2]-m[0][2]*m[2][1])*inv_det;
        inv[0][2] = (m[0][1]*m[1][2]-m[0][2]*m[1][1])*inv_det;
        inv[1][0] = -(m[1][0]*m[2][2]-m[1][2]*m[2][0])*inv_det;
        inv[1][1] = (m[0][0]*m[2][2]-m[0][2]*m[2][0])*inv_det;
        inv[1][2] = -(m[0][0]*m[1][2]-m[0][2]*m[1][0])*inv_det;
        inv[2][0] = (m[1][0]*m[2][1]-m[1][1]*m[2][0])*inv_det;
        inv[2][1] = -(m[0][0]*m[2][1]-m[0][1]*m[2][0])*inv_det;
        inv[2][2] = (m[0][0]*m[1][1]-m[0][1]*m[1][0])*inv_det;
    }

    // as above, but here the determinant is calculated within the function
    template <class T>
    inline void fast_inv_3x3(const T m[3][3], T inv[3][3])
    {
        fast_inv_3x3(m, inv, fast_det_3x3(m));
    }

#define TRACK_OBSTACLES 1

        void DisparityDetector::trackToMapElement(Track& track, MapElement* mel, StereoImageBlob* sib)
        {
            mel->frameType = FRAME_LOCAL;
            mel->type = ELEMENT_OBSTACLE;
            mel->geometryType = GEOMETRY_POLY;
            mel->label.resize(1);
            mel->label[0] = "stereo-obs-perceptor";

            mel->setId(moduleId, track.id);
            //vert.resize(track.convexHull.size());
            
            mel->setGeometry(track.convexHull);
            
            // set the position and its uncertainty
            ellipse ell;
            ell.ssxx = track.var[0][0];
            ell.ssyy = track.var[1][1];
            ell.ssxy = track.var[0][1];
            ell.numPoints = 1; // so finish() will do its job
            ell.finish();
            mel->position.x = track.state[0];
            mel->position.y = track.state[1];
            mel->position.max_var = ell.a;
            mel->position.min_var = ell.b;
            mel->position.axis = ell.theta;
            // set the center uncertainty to be the same as the position
            mel->center.max_var = mel->position.max_var;
            mel->center.min_var = mel->position.min_var;
            mel->center.axis = mel->position.axis;
            
            // uncertainty is always the same for each point. Is having uncertainty
            // for each point really useful (in some other perceptor)?
            for (unsigned int k = 0; k < mel->geometry.size(); k++)
            {
                mel->geometry[k].max_var = mel->position.max_var;
                mel->geometry[k].min_var = mel->position.min_var;
                mel->geometry[k].axis = mel->position.axis;
            }
            //mel->label[1] = "obj" + toStr(j);
            mel->state = sib->state;
            mel->height = track.height;
            mel->heightVar = track.var[2][2]; // ?? don't know!
            mel->elevation = sib->state.localZ - track.state[2] + mel->height/2;
            mel->elevationVar = track.var[2][2];
            
            mel->conf = track.likelihood;

        }


    /*!
     * Given the current regions in m_regions, bring them in local frame, associate
     * them with previously detected objects and track them over time, and create
     * new tracks for unassociated regions. This also merges tracks that seem to
     * converge to one another (probably they are the same obstacle).
     * \param sib The stereo blob the map was built from
     */
    void DisparityDetector::updateTracks(StereoImageBlob* sib, vector<MapElement>* elements)
    {
        MapElement clearEl;
        clearEl.clear();
        clearEl.type = ELEMENT_CLEAR;

        // calculate transformation matrix from image to sensor in homogeneous
        // coordinates (col, row, disp, 1) -> (Xs, Ys, Zs, Ws)
        float sx = sib->leftCamera.sx;
        float sy = sib->leftCamera.sy;
        float cx = sib->leftCamera.cx;
        float cy = sib->leftCamera.cy;
        float b = sib->baseline;
        // (Xs, Ys, Zs, Ws)' = himg2sens * (col, row, disp, 1)'
/*
          float sxOverSy = sx/sy;
          const float himg2sens[4][4] = {
          {b, 0,          0, -b*cx},
          {0, b*sxOverSy, 0, -b*cy*sxOverSy},
          {0, 0,          0,  b*sx},
          {0, 0,          1,  0}
          };
*/
        const float himg2sens[4][4] = {
            {1/sx, 0,     0,    -cx/sx},
            {0,    1/sy,  0,    -cy/sy},
            {0,    0,     0,     1},
            {0,    0,     b/sx,  0}
        };

        float sens2loc[4][4];
        mat44f_mul(sens2loc, sib->veh2loc, sib->leftCamera.sens2veh);

        elements->reserve(m_tracks.size());
        elements->clear();

        if (m_disableTracking)
        {
            unsigned int i;
            // dummy version that just copies each region into one MapElement
            for (i = 0; i < m_tracks.size(); i++)
            {
                if (m_tracks[i].deleted()) {
                    i++;
                    break;
                }
            }
            m_tracks.resize(max(m_regions.size(), i));
            for (i = 0; i < m_regions.size(); i++)
            {
                regionToTrack(m_regions[i], &m_tracks[i], sib, himg2sens, sens2loc, m_errorVar);
            }

            for (i = 0; i < m_regions.size(); i++) {
                elements->push_back(MapElement());
                trackToMapElement(m_tracks[i], &elements->back(), sib);
            }
            
            for (i = m_regions.size(); i < m_tracks.size(); i++) {
                if (m_tracks[i].updated()) {
                    clearEl.setId(moduleId, m_tracks[i].id);
                    elements->push_back(clearEl);
                    m_tracks[i].updated(false);
                }
            }
            
            return;
        }

        // convert the regions from image frame to local frame in the "m_measures" vector
        m_measures.resize(m_regions.size());
        for (unsigned int i = 0; i < m_regions.size(); i++)
        {
            regionToTrack(m_regions[i], &m_measures[i], sib, himg2sens, sens2loc, m_errorVar);
        }

        // FIXME: preallocate this memory
        vector<vector<float> > assoc(m_tracks.size());
        for (unsigned int i = 0; i < assoc.size(); i++) {
            assoc[i].resize(m_measures.size());
            fill(assoc[i].begin(), assoc[i].end(), -1.0);
        }

        //double timediff = double(sib->timestamp - m_lastTs) * 1e-6;
        // MAGIC HERE!! (ok for 10Hz, timediff=0.1 ==> thres = m_assocThres)
        //double thres = m_assocThres * 10 * timediff;
        double thres = m_assocThres;
        m_lastTs = sib->timestamp;
        
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            m_tracks[i].updated(false);
            if (m_tracks[i].deleted())
                continue;
            for (unsigned int j = 0; j < m_measures.size(); j++)
            {
                //if (m_regions[j].filteredOut == true)
                //    continue;

                // TODO: filter the data with a kalman filter or something
                // TODO: take into account the shape (size at least) of the track/measure!
                // the variance of the measure conditioned by the prior is
                // S = Var[z|X] = (C*P*C' + R) where
                //   P = variance of the state (m_tracks[i].state)
                //   C = output transformation matrix, i.e. z = C*x (z=measure, x=state)
                //   R = measure error variance
                float cov[3][3];
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        cov[k][l] = m_tracks[i].var[k][l] + m_measures[j].var[k][l];
                // the distance is (z - x)*(S^-1)*(z - x)', so we need to invert the matrix (expensive ...)
                float inv[3][3];
                fast_inv_3x3(cov, inv); // yeah, "fast" meaning "not that slow" ...

                float distVec[3];
                for (int k = 0; k < 3; k++)
                    distVec[k] = fabs(m_measures[j].state[k] - m_tracks[i].state[k]);
                float dist = 0;
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        dist += distVec[k]*inv[k][l]*distVec[l];
                // cov is semipos definite, so is inv ==> dist must be >=0
                if (dist < 0)
                    ERRMSG("*** updateTracks(): dist < 0!!! numeric error???");
                // note: dist is the mahalanobis distance *squared*
                if (dist < thres*thres) {
                    assoc[i][j] = dist;
                    //MSG("updateTracks(): dist^2=" << dist << " < " << thres*thres
                    //    << ", associating track " << i << " with measure " << j);
                } else {
                    //MSG("updateTracks(): dist=" << dist << " >= " << thres
                    //    << ", NOT associating track " << i << " with measure " << j);
                }
            }
        }

        // keep track of the free slots available in m_tracks
        vector<int> freeSlots(0);
        freeSlots.reserve(m_tracks.size());

        // FIXME: store this as a member so we can avoid allocating memory in the loop
        CvMat points = cvMat(0, 0, CV_32FC2);

        // update or delete tracks using the new measures
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted()) {
                freeSlots.push_back(i);
                continue;
            }

            //int nAssoc = 0;
            int nPoints = 0; // number of points to use to calculate the new convex hull
            int best = -1;
            float bestDist = numeric_limits<float>::max();
            point3 sum;
            sum[0] = sum[1] = sum[2] = 0;
            float weightSum = 0;
            for (unsigned int j = 0; j < m_measures.size(); j++)
            {
                // TODO: use the variance of the filtered data (once we have that)
                // to calculate the new mean
                // SAM - added check to make sure measures aren't reused
                if (assoc[i][j] >= 0 /* && m_measures[j].id != -1 */) {
						

                    float weight = exp(-0.5 * assoc[i][j]);
                    weightSum += weight;
                    sum[0] += m_measures[j].state[0] * weight;
                    sum[1] += m_measures[j].state[1] * weight;
                    sum[2] += m_measures[j].state[2] * weight;
                    nPoints += m_measures[j].convexHull.size();
                    if (bestDist > assoc[i][j] || best == -1)
                    {
                        bestDist = assoc[i][j];
                        best = j;
                    }
                    // mark this measure as associated
                    //m_measures[j].id = -1;
                }
            }

            if (nPoints > 0)
            {
                // calculate the new updated center
                // FIXME: JUST REPLACING THE STATE WITH THE NEW ONE (use a kalman filter instead!)
                for (int k = 0; k < 3; k++)
                    m_tracks[i].state[k] = sum[k] / weightSum;

                // as the measures are all close to each other, they'll all have the same variance
                // as this depends only on the position.
                // FIXME: the variance should be higher when multiple measures are associated with
                // one track!!! (i.e. stop doing hacks :P) 
                memcpy(m_tracks[i].var, m_measures[best].var, sizeof(m_tracks[i].var));

                // copy all the points of the convex hulls of the regions in a CvMat structure
                // first allocate the memory if needed ...
                if (points.rows * points.cols < nPoints)
                {
                    if (points.data.ptr != NULL)
                        cvReleaseData(&points);
                    points = cvMat(nPoints, 1, CV_32FC2);
                    cvCreateData(&points);
                }
                
                // ... then copy all the points ...
                float* mat = (float*) points.data.ptr;


                for (unsigned int j = 0; j < m_measures.size(); j++)
                {
                    //SAM added check to make sure measures arent reused
                    if (assoc[i][j] >= 0 /* && m_measures[j].id != -1 */) {
                        point2arr& mHull = m_measures[j].convexHull;
                        for (unsigned int k = 0; k < mHull.size(); k++)
                        {
                            *mat++ = mHull[k].x;
                            *mat++ = mHull[k].y;
                        }

                        // mark this measure as associated
                        //SAM  this line was moved from line 617
                        m_measures[j].id = -1;
                    }
                }

                int allocated = points.rows * points.cols; // save allocated size
                points.rows = nPoints;
                points.cols = 1;
                
				
                // ... allocate another CvMat structure for storing the result ...
                CvMat hull = cvMat(points.rows, points.cols, CV_32FC2);
                cvCreateData(&hull);
                // ... do the hard work ...
                cvConvexHull2(&points, &hull, CV_CLOCKWISE, 1 /* points, not indexes */);
                // ... and copy the result back to the Track struct ...
                mat = (float*) hull.data.ptr;
                unsigned int size = hull.rows * hull.cols;
                m_tracks[i].convexHull.resize(size);
                for (unsigned int k = 0; k < size; k++)
                {
                    float x = *mat++;
                    float y = *mat++;
                    m_tracks[i].convexHull[k] = point2(x, y);
                }
                cvReleaseData(&hull);
                // restore allocated size value
                points.rows = allocated;
                points.cols = 1;

                // simple exponential filter for likelihood, uncertainty halves
                // every frame the track was associated with some measure
                m_tracks[i].likelihood = 1.0 - (1.0 - m_tracks[i].likelihood)*0.5;
                if (m_tracks[i].maxLikelihood < m_tracks[i].likelihood)
                    m_tracks[i].maxLikelihood = m_tracks[i].likelihood;
                m_tracks[i].updated(true);

            }
            else
            {
                // reduce likelihood for unassociated tracks, and remove them if it goes
                // below a threshold
                m_tracks[i].likelihood *= 0.5; // MAGIC
                if (m_tracks[i].likelihood <= 0.1) { // MAGIC
                    m_tracks[i].deleted(true);
                    m_tracks[i].updated(true); // to send a clear msg
                    freeSlots.push_back(i);
                    //MSG("Deleted track " << m_tracks[i].id << ", likelyhood = "
                    //    << m_tracks[i].likelihood << " <= 0.1");

                    if (m_tracks[i].confirmed())
                    {
                        // send a clear to the mapper
                        clearEl.setId(moduleId, m_tracks[i].id);
                        elements->push_back(clearEl);
                    }
                } else {
                    //MSG("Track " << m_tracks[i].id << " not associated, new likelihood = "
                    //    << m_tracks[i].likelihood);
                }
            }

        }

        // iterator to keep track of the next track to remove, when needed
        conf_idx_vector_t::iterator rmIt;

        // create new tracks for non associated measures
        for (unsigned int j = 0; j < m_measures.size(); j++)
        {
            if (m_measures[j].id != -1)
            {
                int i = -1;
                // reuse a deleted track, if possible, else add a new one
                if (freeSlots.size() > 0) {
                    i = freeSlots.back();
                    assert(m_tracks[i].deleted());
                    m_tracks[i] = m_measures[j]; // copying may be expensive!
                    freeSlots.pop_back();
                } else {
                    //m_tracks.push_back(m_measures[j]); // copying may be expensive!
                    //i = m_tracks.size() - 1;
                    // remove the track with lowest confidence, if lower than the measure
                    if (m_toSort.size() == 0) {
                        // sort the tracks "on-demand", only when needed
                        m_toSort.resize(m_tracks.size()); // memory is preallocated with reserve()
                        for (unsigned int i = 0; i < m_tracks.size(); i++) {
                            m_toSort[i].first = m_tracks[i].likelihood;
                            m_toSort[i].second = i;
                        }
                        std::sort(m_toSort.begin(), m_toSort.end());
                        rmIt = m_toSort.begin();
                    }
                    // check if it's worth to remove an old track to make space for the new one
                    if (rmIt != m_toSort.end() && rmIt->first <= m_measures[j].likelihood)
                    {
                        i = rmIt->second;
                        ++rmIt;
                        //MSG("Replacing track " << i << " (id=" << m_tracks[i].id << ", conf="
                        //    << m_tracks[i].likelihood << ") with measure " << j << ", conf="
                        //    << m_measures[j].likelihood);
                        if (m_tracks[i].confirmed())
                        {
                            // send a clear to the mapper
                            clearEl.setId(moduleId, m_tracks[i].id);
                            elements->push_back(clearEl);
                        }
                        m_tracks[i] = m_measures[j];
                    }
                }
                if (i > 0)
                    m_tracks[i].id = m_nextId++;
            }
        }

        m_toSort.clear();

        MSG("Current number of tracks: " << m_tracks.size() << " - "
            << freeSlots.size() << " (deleted)");

        // TODO: merge tracks that seem to converge to the same state
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (m_tracks[i].deleted())
                continue;
            for (unsigned int j = i + 1; j < m_tracks.size(); j++)
            {
                if (m_tracks[j].deleted())
                    continue;
                float cov[3][3];
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        cov[k][l] = m_tracks[i].var[k][l] + m_tracks[j].var[k][l];
                // the distance is (z - x)*(S^-1)*(z - x)', so we need to invert the matrix (expensive ...)
                float inv[3][3];
                fast_inv_3x3(cov, inv); // yeah, "fast" meaning "not that slow" ...

                float distVec[3];
                for (int k = 0; k < 3; k++)
                    distVec[k] = fabs(m_tracks[j].state[k] - m_tracks[i].state[k]);
                float dist = 0;
                for (int k = 0; k < 3; k++)
                    for (int l = 0; l < 3; l++)
                        dist += distVec[k]*inv[k][l]*distVec[l];
                // cov is semipos definite, so is inv ==> dist must be >=0
                if (dist < 0)
                    ERRMSG("*** updateTracks(): dist < 0!!! numeric error???");
                // note: dist is the mahalanobis distance *squared*
                if (dist < thres*thres) {
                    // tracks are very close, let's merge them
                    // TODO: do actually merge them!!! Now only keeping the one with
                    // the highest confidence!
                    int remIdx  = (m_tracks[i].likelihood < m_tracks[j].likelihood) ? i : j;
                    int keepIdx = (m_tracks[i].likelihood > m_tracks[j].likelihood) ? i : j;
                    // remove track m_tracks[remIdx]
                    m_tracks[remIdx].deleted(true);
                    m_tracks[remIdx].updated(true); // to send a clear msg
                    //MSG("Deleted track " << m_tracks[remIdx].id << ", merged with "
                    //    << m_tracks[keepIdx].id << ", dist^2 == " << dist
                    //    << " < " << thres*thres);

                    if (m_tracks[remIdx].confirmed())
                    {
                        // send a clear to the mapper
                        clearEl.setId(moduleId, m_tracks[remIdx].id);
                        elements->push_back(clearEl);
                    }
                }
            }
        }

        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            if (!m_tracks[i].deleted()
                && m_tracks[i].updated()
                && m_tracks[i].likelihood > m_confThres)
            {
                m_tracks[i].confirmed(true);
                // send it to the mapper
                elements->push_back(MapElement());
                trackToMapElement(m_tracks[i], &elements->back(), sib);
            }
        }
    }

    /*! Given the updated tracks, create a corresponding vector
     * containing Obstacle objects to be returned to the main program,
     * and store them in m_result.
     */
/*
    void DisparityDetector::obstaclesFromTracks(StereoImageBlob* sib)
    {
        // FIXME: think about having only one structure instead of Obstacle and Track,
        // so it's not necessary to copy data from one to the other. They basically
        // contain the same information.
        m_result.resize(m_tracks.size()); // reserve enough memory
        int n = 0;
        for (unsigned int i = 0; i < m_tracks.size(); i++)
        {
            // remove me???
            if (m_tracks[i].likelihood < 0.5)
                continue;
            // unique id
            m_result[n].id = m_tracks[i].id;
            // copy vertices of convex hull
            unsigned int nVert = m_tracks[i].convexHull.size();
            vector<point4>& resVert = m_result[n].vertices;
            vector<point3>& resVar = m_result[n].variance;
            resVert.resize(nVert);
            resVar.resize(nVert);
            for (unsigned int j = 0; j < nVert; j++)
            {
                resVert[j][0] = m_tracks[i].convexHull[j].x;
                resVert[j][1] = m_tracks[i].convexHull[j].y;
                resVert[j][2] = m_tracks[i].state[2];
                resVert[j][3] = 1;
                resVar[j][0] = m_tracks[i].var[0][0];
                resVar[j][1] = m_tracks[i].var[1][1];
                resVar[j][2] = m_tracks[i].var[2][2];
            }
            // set center
            memcpy(m_result[n].center.v, m_tracks[i].state, sizeof(float)*3);
            m_result[n].center[3] = 1;
            m_result[n].speed.x = m_tracks[i].state[3];
            m_result[n].speed.y = m_tracks[i].state[4];
            // FIXME: uncertainty set to zero!!!
            m_result[n].speed.max_var = 0;
            m_result[n].speed.min_var = 0;
            m_result[n].speed.axis = 0;

            // FIXME: just use the probability density at the mean
            m_result[n].likelihood = m_tracks[i].likelihood;
            // vehicle state
            m_result[n].state = sib->state;
            // state covariance matrix, just position though
            memset(m_result[n].centerCov, 0, sizeof(m_result[n].centerCov));
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    m_result[n].centerCov[j][k] = m_tracks[i].var[j][k];
            m_result[n].centerCov[3][3] = 1;
            n++;

            // height
            m_result[n].height = m_tracks[i].height;
            // elevation - remember that positive z is down!
            m_result[n].elevation = sib->state.localZ - 
                m_tracks[i].state[2] + m_tracks[i].height/2;
        }

        m_result.resize(n);

    }
*/

    void calcPointCov(float cov[4][4], const float pi[3],
                      const float himg2sens[4][4], const float sens2loc[4][4],
                      const float errorVar[3])
    {
        // Given the variance of col, row, disp (m_errorVar[1, 2, 3]), calculate
        // the variance in local frame using the linearized transformation.
        // jacobian of nonlinear transformation in the current point:
        // (xs, ys, zs, 1) = (Xs/Ws, Ys/Ws, Zs/Ws, 1) (note: Ws == disparity)
        point4 ps;
        typedef const float (*mat44_t)[4];
        mat44_t& K = himg2sens;
        // transform from image to sensor frame (do NOT use StereoImageBlobImageToSensor
        // because it doesn't necessarily use the same transformation as himg2sens, may differ
        // of a scale factor)
        ps[0] = K[0][0]*pi[0] + K[0][1]*pi[1] + K[0][2]*pi[2] + K[0][3];
        ps[1] = K[1][0]*pi[0] + K[1][1]*pi[1] + K[1][2]*pi[2] + K[1][3];
        ps[2] = K[2][0]*pi[0] + K[2][1]*pi[1] + K[2][2]*pi[2] + K[2][3];
        ps[3] = K[3][0]*pi[0] + K[3][1]*pi[1] + K[3][2]*pi[2] + K[3][3];
        float invW = 1.0/ps[3];
        // jacobian of g(x, y, z, w) = (x/w, y/w, z/w, 1)
        float J[4][4] = {
            {invW,   0,    0, ps[0]*invW*invW},
            {0,   invW,    0, ps[1]*invW*invW},
            {0,      0, invW, ps[2]*invW*invW},
            {0,      0,    0,          0}
        };
        float img2sens[4][4];
        mat44f_mul(img2sens, J, himg2sens);
        float img2loc[4][4];
        mat44f_mul(img2loc, sens2loc, img2sens);

        // covariance is cov = img2loc * diag(errorVar) * img2loc'
        // diag(...) * img2loc', optimized
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                cov[i][j] = errorVar[i] * img2loc[j][i];
        mat44f_mul(cov, img2loc, cov);
    }

    void DisparityDetector::calcPointCov(float cov[4][4], const float pi[3],
                                         const float himg2sens[4][4], const float sens2loc[4][4])
    {
        blobstereo::calcPointCov(cov, pi, himg2sens, sens2loc, m_errorVar);
    }


}
