/* Emacs clue: -*-  indent-tabs-mode:nil; c-basic-offset:4; -*-*/
#ifndef __DISPARITYDETECTOR_HH__
#define __DISPARITYDETECTOR_HH__

/*!
 * \file DisparityDetector.hh
 * \brief Obstacle detector based on stereo disparity (header file)
 *
 * \author Daniele Tamino
 * \date 10 July 2007
 *
 * This file contains the declaration of the DisparityDetector class, which given
 * a sensnet StereoImageBlob blob, containing disparity information, outputs a set
 * of obstacles. This module can track the obstacles over time, but the reset()
 * method can be called to clear the prior imformation.
 *
 */


#include <bitmap/Bitmap.hh>
#include <frames/ellipse.hh>
#include <interfaces/sn_types.h>
#include "Detector.hh"
#include "GlRenderer.hh"

#include "Obstacle.hh" // remove me

struct gengetopt_args_info;

namespace blobstereo
{
    using namespace bitmap;

    enum track_flags_enum_t
    {
        TRACK_DELETED = 1,   // deleted track, empty slot
        TRACK_CONFIRMED = 2, // track was actually sent to the mapper at least once
        TRACK_UPDATED = 4    // track was updated or deleted in the last loop
    };

    struct Track
    {
        int id; // unique id
        int flags; // some TRACK_* values OR-ed together
/*
        bool deleted; // deleted track, to be ignored
        bool updated; // track was updated or deleted in the last loop
*/
        // state is in local frame
        float state[6]; // x, y, z, dx, dy, dz
        float var[6][6]; // state error covariance matrix

        float likelihood; // remove me, just use the density of probability at the mean
        float maxLikelihood;

        // bounding box containing the last seen blob(s) in disparity map frame
        CvRect rect;

        // best fitting ellipse (local frame), used to calculate approximate
        // distances between blobs and decide about associating and merging
        // TODO: replace me with a N-dimensional ellipsoid (N = number of states)
        ellipse el;

        // convex hull of the last region(s), in local frame, clockwise order
        point2arr convexHull;
        // height of the obstacle
        float height;


        // utility methods to get and set the flags

        bool get(int fl) { return flags & fl; }
        bool deleted()   { return get(TRACK_DELETED); }
        bool confirmed() { return get(TRACK_CONFIRMED); }
        bool updated()   { return get(TRACK_UPDATED); }

        void set(int fl, bool val) { flags = val ? (flags | fl) : (flags & ~fl); }
        bool deleted(bool v)   { set(TRACK_DELETED, v); return v; }
        bool confirmed(bool v) { set(TRACK_CONFIRMED, v); return v; }
        bool updated(bool v)   { set(TRACK_UPDATED, v); return v; }

    };

    struct Region
    {
        int id; // unique id (from 2 to 255 inclusive)

        // sum of the point weights that contributed to this region
        // (each point has a weight in (0, 1), even though it's always 1 at the moment)
        float weight;

        // bounding box of the region (disparity map coordinates)
        CvRect rect;

        float row;
        float rowStDev;

        // best fitting ellipse, used to calculate approximate distances
        // between blobs and decide about associating and merging.
        // Coordinates are (map column, disparity) not (map column, map row)!
        // TODO: replace me with a N-dimensional ellipsoid (N = number of states)
        ellipse el;

        // convex hull of the region, (map col, disp) coordinates, clockwise order
        point2arr convexHull;
    };


    /**
     * This class implements a detector based on the disparity map, that detects
     * generic obstacles (blobs) without any classification.
     */
    class DisparityDetector : virtual public Detector
    {
        // mutex to access this object
        pthread_mutex_t m_mtx;

        modulename moduleId; // needed to set the ID in the MapElements

        typedef float dispmap_t;
        /// map with accumulated disparity
        Bitmap<dispmap_t, 1> m_mapImg;
        /// 2 pixel taller and wider than m_mapImg. Needed for cvFloodFill.
        Bitmap<uint8_t, 1> m_maskImg;

        dispmap_t m_mapMax; // maximum value in the current map
        
        /// used to calculate height (image row) mean value of points in each region
        Bitmap<float, 1> m_rowSum;
        /// used to calculate height (image row) st.dev. of points in each region
        Bitmap<float, 1> m_rowSumSq;

        // current tracks
        int m_nextId; // next track id to use
        vector<Track> m_tracks;
        uint64_t m_lastTs; // timestamp of the last blob used to update the tracks
        // current observations, i.e. regions detected and not filtered
        vector<Region> m_regions;
        // current observations, transformed into local frame (i.e. from Region to Track)
        vector<Track> m_measures;
        // uset to sort tracks based on their likelihood
        typedef vector<pair<float, int> > conf_idx_vector_t;
        conf_idx_vector_t m_toSort;

        // configurable parameters

        /// maximum possible disparity value
        int m_maxDisp;
        /// minimum allowed disparity value
        int m_minDisp;
        /// vertical size of the map. This determines the map resolution
        /// (e.g. maxDisp=200, mapHeight=400, resolution is 200/400 = 0.5)
        unsigned int m_mapHeight;
        /// high threshold used to detect peaks in the disparity map
        double m_highThres;
        /// low threshold used to "fill" peaks found with the high threshold
        double m_lowThres;
        /// threshold for filtering small obstacles (number of points of the
        /// disparity image tht contributed to the observation).
        float m_smallObsThres;
        /// threshold for data association (gate size)
        float m_assocThres;
        /// constant measurement error variance for (column, row, disparity) resp.
        float m_errorVar[3];
        /// Disable tracking entirely
        bool m_disableTracking;
        /// Confidence threshold
        float m_confThres;
        /// Maximum number of tracks to keep at any given time
        int m_maxTracks;
        /// Height fuzzy threshold
        float m_heightThres;
        /// Height fuzzy threshold sigma (gaussian-like)
        float m_heightThresSigma;

        /*
         * DEBUGGING STUFF
         */

        friend class ObstOverlayRenderer;
        friend class ObstacleRenderer;
        friend class ObstacleRenderer3D;
        friend class DisparityDetectorRenderer;

        /// if > 0, run debugging code (and print some more messages)
        int m_debug;

    public:

        DisparityDetector(int dbg = 1,
                          int maxDisp = 65, int mapHeight = 300,
                          double hiThres = 8, double loThres = 3,
                          float smallObsThres = 40, float assocThres = 2);

        DisparityDetector(gengetopt_args_info& options);

        ~DisparityDetector() { }

        /* from Detector */
        
        virtual void detect(const SensnetBlob& blob, vector<MapElement>* elements);

        virtual vector<Fl_Widget*> getWidgets();

        /*! \brief Clear all the tracks, zero out this object' state */
        virtual void reset();

    private:

        /*! \brief Given the current stereo blob, containing disparity information,
         * build a map accumulating vertically points with the same disparity.
         */
        void buildMap(StereoImageBlob* sib);

        /*! \brief Given the current map, find connected regions */
        void findRegions(StereoImageBlob* sib);

        /*! \brief Update tracks with the current measurements (regions), and fill the elements vector */
        void updateTracks(StereoImageBlob* sib, vector<MapElement>* elements);

        /*! \brief Given the updated tracks, fill m_result with Obstacles */
/*
        void obstaclesFromTracks(StereoImageBlob* sib);
*/
        
        /*!
         * Given a point in homogeneous coordinates in sensor frame, and the transformation
         * from image to sensor (in homog coords) and sensor to local frame, it calculates
         * the variance of the point in local frame, assuming a fixed variance in image frame
         * of diag(m_errorVar).
         */
        void calcPointCov(float cov[4][4], const float pi[3],
                          const float himg2sens[4][4], const float sens2loc[4][4]);
        
        void trackToMapElement(Track& track, MapElement* mel, StereoImageBlob* sib);
    };

}

#endif
