// C++ headers
#include <iostream>
#include <vector>
#include <limits>
// boost headers
#include <boost/scoped_array.hpp>
// OpenCV headers
#include <cv.h>
#include <highgui.h>
// frames.h defines these too
#undef MIN 
#undef MAX
// DGC headers
#include <interfaces/StereoImageBlob.h>
// local headers
#include "DisparityDetector.hh"
#include "util.hh"
#include "glutil.hh"

namespace blobstereo
{
    using namespace std;
    using namespace boost;

    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name,
                       line, userdata );
        abort();
    }

    DisparityDetector::DisparityDetector(bool dbg, int _maxDisp, int _mapSize,
                      double hiThres, double loThres)
        : debug(dbg), maxDisp(_maxDisp), mapSize(_mapSize),
          mapMax(0), highThres(hiThres), lowThres(loThres),
          dispTex(0), mapTex(0), debugTex(0)
    {
        cvRedirectError(abortOnCvError);
        memset(&maskMat, 0, sizeof(maskMat));
    }

    DisparityDetector::~DisparityDetector()
    {
#ifndef DISPDET_SINGLE_THRES
        if (maskMat.data.ptr != NULL)
            cvReleaseData(&maskMat);
#endif
    }

    /** Given a CvSeq sequence of points, in image coordinates, compute the
     * convex hull, convert coords into vehicle frame, and put them in the
     * 'vertices' array.
     */
    void DisparityDetector::pointsToObstacle(StereoImageBlob* sib, const CvArr* points,
                                             vector<point2_uncertain>* vertices)
    {
        // calculate the convex hull of the component
        CvMat stub;
        CvMat* contour = cvGetMat(points, &stub);
        int num = contour->rows * contour->cols;
        scoped_array<int32_t> hull(new int32_t[num*2]);
        CvMat hullMat = cvMat(1, num, CV_32SC2, hull.get());

        // debug
/*
        MSG("pointsToObstacle: contour is:");
        for (int i = 0; i < num*2; i+=2)
        {
            cerr << "(" << ((int32_t*)contour->data.ptr)[i] << ", "
                 << ((int32_t*)contour->data.ptr)[i+1] << ")";
        }
        cerr << endl;
*/

        cvConvexHull2(contour, &hullMat, CV_CLOCKWISE, 1 /* points, not inxedes */);

        int nPoints = hullMat.cols;

        if (debug) {
            vector<int32_t> hullX;
            vector<int32_t> hullY;
            for (int i = 0; i < nPoints; i++) {
                // (column, disparity) coordinates
                int32_t c = hull[2*i];
                int32_t d = hull[2*i + 1];
                hullX.push_back(c);
                hullY.push_back(d);
            }
            dbgHullX.push_back(hullX);
            dbgHullY.push_back(hullY);
        }

        assert(vertices != NULL);
        // use a reference for ease of notation (vert[i] instead of (*vert)[i])
        vector<point2_uncertain>& vert = *vertices;
        vert.resize(nPoints);
#warning "FIXME: ******************************************"
#warning "FIXME: - calculate height of the obstacle correctly, handle floating objects"
#warning "FIXME:   (tree foilage, the sky (yes sometines that appears as an obstacle!!!) )"
#warning "FIXME: - should I calculate the convex hull in vehicle/local frame?"
#warning "FIXME:   (seems to be fine in disparity space)"
#warning "FIXME: ******************************************"
        float cell2disp = float(maxDisp) / mapSize;
        for (int i = 0; i < nPoints; i++) {
            // (column, disparity) coordinates
            float c = hull[2*i];
            float d = hull[2*i + 1] * cell2disp;
            float xs, ys, zs;
            StereoImageBlobImageToSensor(sib, c, 240 /*fixme*/, d, &xs, &ys, &zs);
            float x, y, z;
            StereoImageBlobSensorToVehicle(sib, xs, ys, zs, &x, &y, &z);
            // get world coordinates
            vert[i].x = x;
            vert[i].y = y;
            // no uncertainty information yet
            vert[i].max_var = 0;
            vert[i].min_var = 0;
            vert[i].axis = 0;
        }
    }

    /** Given a CvArr containing a mask, find all the edge points and put them
     * in the points matrix, which is a 1D row vector containing 2D points
     * (type CV_32SC2, two channels of int32_t);
     * Note: if maskArr is of size (rows, cols), it searches in the rectangle
     * (1, 1, rows - 2, cols - 2) for a blob, and the mask boundary should be
     * all set to zero.
     */
    void getMaskEdgePoints(CvArr* maskArr, CvMat** points, int xOff, int yOff,
                           vector<int32_t>* debugXv=NULL, vector<int32_t>* debugYv=NULL)
    {
        vector<int32_t> xv;
        vector<int32_t> yv;
        CvMat maskStub;
        CvMat* mask = cvGetMat(maskArr, &maskStub);
        // must be of type int8_t or uint8_t
        assert(CV_MAT_TYPE(mask->type) == CV_8UC1 || CV_MAT_TYPE(mask->type) == CV_8SC1);
        uint8_t* start = (uint8_t*) mask->data.ptr;
        for (int r = 1; r < mask->rows - 1; r++) {
            uint8_t* row = start + mask->step * r + 1;
            uint8_t* q = row;
            uint8_t* qend = row + mask->cols - 2;
            for (; q != qend; q++) {
                int added = 0;
                if (*q != *(q - 1)) {
                    // vertical edge
                    if (*(q - 1) == 1) {
                        // add *(q - 1)
                        xv.push_back(q - row - 1 + xOff);
                        yv.push_back(r + yOff);
                    } else if (*q == 1) {
                        // add *q
                        xv.push_back(q - row + xOff);
                        yv.push_back(r + yOff);
                        added = 1;
                    }
                }
                if (*q != *(q - mask->step)) {
                    // horizontal edge
                    if (*(q - mask->step) == 1) {
                        // add *(q - mask->step)
                        xv.push_back(q - row + xOff);
                        yv.push_back(r - 1 + yOff);
                    } else if (!added && *q == 1) {
                        // add *q
                        xv.push_back(q - row + xOff);
                        yv.push_back(r + yOff);
                    }
                }
            } // end for (each column)
        } // end for (each row)

        // debugging stuff
        if (debugXv)
            *debugXv = xv;
        if (debugYv)
            *debugYv = yv;

        // create a CvMat out of the vector
	if (xv.size() > 0) {
	    *points = cvCreateMat(xv.size(), 1, CV_32SC2);
	    int32_t* mat = (int32_t*) (*points)->data.ptr;
	    for (unsigned int i = 0; i < xv.size(); i++) {
                *mat++ = xv[i];
                *mat++ = yv[i];
	    }
	} else { // OpenCv doesn't like matricies with 0-sized dimensions
	    *points = cvCreateMat(1, 1, CV_32SC2);
	    int32_t* mat = (int32_t*) (*points)->data.ptr;
	    mat[0] = xOff + mask->cols/2;
	    mat[1] = yOff + mask->rows/2;
	}
    }

    vector<Obstacle> DisparityDetector::detect(const SensnetBlob& blob)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();
        
        if (sib->version != 4) {
            if (sib->version < 4) {
                ERRMSG("StereoImageBlob version " << sib->version << " not supported!!!");
                return vector<Obstacle>(); // empty vector
            } else {
                MSG("StereoImageBlob version " << sib->version << " newer than 4, continuing");
            }
        }

        if (debug) {
            dbgEdgesX.clear();
            dbgEdgesY.clear();
            dbgHullX.clear();
            dbgHullY.clear();
        }

        // initialize the map
        if (dispMap.getWidth() != sib->cols) {
            dispMap.init(sib->cols, mapSize);
#ifndef DISPDET_SINGLE_THREAD
            maskFill.init(sib->cols + 2, mapSize + 2);
            // create maskImg sharing the same underlying data as maskFill
            cvGetSubRect(maskFill.getImage(), &maskMat, cvRect(1, 1, sib->cols, mapSize));
            cvIncRefData(&maskMat);
#else
            maskImg.init(sib->cols, mapSize);
#endif
           if (debug) {
                debugImg.init(sib->cols, mapSize);
            }
        } else {
            cvZero(dispMap.getImage());
#ifndef DISPDET_SINGLE_THRES
            cvZero(maskFill.getImage());
#else
            cvZero(maskImg.getImage());
#endif
            if (debug) {
                cvZero(debugImg.getImage());
            }
        }

        double disp2cell = mapSize / (maxDisp * sib->dispScale);
        const int16_t dispThres = 3; // minimum interesting disparity

        mapMax = 0;
        for (int c = 0; c < sib->cols; c++) {
            for (int r = 0; r < sib->rows; r++) {
                int16_t d = *(int16_t*) StereoImageBlobGetDisp(sib, c, r);
                if (d >= dispThres) {
                    unsigned int cell = unsigned(d * disp2cell);
                    
                    if (cell > mapSize) {
                        cell = mapSize;
                    }

                    int32_t val = dispMap[cell][c][0] + 1;
                    if (val == numeric_limits<dispmap_t>::max()) {
                        val = numeric_limits<dispmap_t>::max();
                    }
                    dispMap[cell][c][0] = dispmap_t(val);
                    if (val > mapMax) {
                        mapMax = val;
                    }
                }
            }
        }

        MSG("mapMax = " << int(mapMax));

        vector<Obstacle> obst;

#ifndef DISPDET_SINGLE_THRES
        /* Use two thresholds to detect blobs. A blob is detected if all its cells
         * are above the low threshold and at least one is above the high threshold.
         * Method: first, threshold the image with the low threshold and create a mask
         * (done in the code above).
         * Then, scan for cells above the high threshold, and use cvFloodFill on the
         * mask to find the blob.
         */

        // work directly with IplImage from here on
        //CvMat* mask = &maskMat;
        IplImage* map = dispMap.getImage();

        //IplConvKernel* ball = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_ELLIPSE, NULL);
        //IplConvKernel* hLine = cvCreateStructuringElementEx(7, 1, 3, 0, CV_SHAPE_CROSS, NULL);

        //cvThreshold(map, mask, lowThres, numeric_limits<uint8_t>::max(),
        //            CV_THRESH_BINARY);

        //cvErode(mask, mask, hLine, 1);
        //cvDilate(mask, mask, ball, 1);

        //cvReleaseStructuringElement(&hLine);
        //cvReleaseStructuringElement(&ball);

        int height = dispMap.getHeight();
        int width = dispMap.getWidth();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                int val = dispMap[r][c][0];
                if (val > highThres) {
                    int loDiff = int(val - lowThres);
                    int hiDiff = 255 - val;
                    CvConnectedComp comp;

                    if (maskFill[r+1][c+1][0] != 0)
                        continue;

                    cvFloodFill(map, cvPoint(c, r), cvRealScalar(255),
                                cvRealScalar(loDiff), cvRealScalar(hiDiff),
                                &comp, CV_FLOODFILL_FIXED_RANGE | CV_FLOODFILL_MASK_ONLY | 4,
                                maskFill.getImage());
                    
                    Obstacle ob;
                    ob.getState() = sib->state;
                    CvMat subRect;
                    CvMat* points = NULL;

                    if (comp.rect.width > 0 && comp.rect.height > 0) {
                        // Rect must be 1 pix wider in each direction than the blob,
                        // but maskFill is 1pix wider than mask already, so need only
                        // to set the width and height
                        comp.rect.width += 2;
                        comp.rect.height += 2;
                        
                        cvGetSubRect(maskFill.getImage(), &subRect, comp.rect);
                        vector<int32_t> dbgXvObj, dbgYvObj;
                        vector<int32_t>* dbgXv = debug ? &dbgXvObj : NULL;
                        vector<int32_t>* dbgYv = debug ? &dbgYvObj : NULL;
                        getMaskEdgePoints(&subRect, &points,
                                          comp.rect.x - 1, comp.rect.y - 1,
                                          dbgXv, dbgYv);
                        if (debug) {
                            dbgEdgesX.push_back(*dbgXv);
                            dbgEdgesY.push_back(*dbgYv);
                        }
                        // substitute all 1's with 255's, so getMaskEdgePoints won't
                        // consider them the next time (and so they will be rendered
                        // correctly).
                        uint8_t* start = (uint8_t*) subRect.data.ptr;
                        for (int r = 1; r < subRect.rows - 1; r++) {
                            for (int c = 1; c < subRect.cols - 1; c++) {
                                uint8_t* p = start + r * subRect.step + c;
                                if (*p == 1)
                                    *p = 255;
                            }
                        }
                        pointsToObstacle(sib, points, &ob.getVertices());
                        obst.push_back(ob);
                        
                        cvReleaseMat(&points);
                    } else {
                        ERRMSG("BUGBUG: cvFloodFill() did nothing, was the seed point masked???");
                        ERRMSG("BUGBUG: seed point: (c=" << c << ", r=" << r << ")" );
                    }
                }
            }
        }
        

#else
        /* Use cvFindContour. This approach does not allow to use a high and a low threshold
         * to detect blobs, resulting in a lot of false positives (or false negatives,
         * depending on how you set the single threshold, the low one).
         * It has the advantage of being simple and taking advantage of code in OpenCV.
         */

        // work directly with IplImage from here on
        IplImage* mask = maskImg.getImage();
        IplImage* map = dispMap.getImage();

        IplConvKernel* ball = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_ELLIPSE, NULL);
        IplConvKernel* hLine = cvCreateStructuringElementEx(7, 1, 3, 0, CV_SHAPE_CROSS, NULL);

        cvThreshold(map, mask, lowThres, numeric_limits<uint8_t>::max(),
                    CV_THRESH_BINARY);

        cvErode(mask, mask, hLine, 1);
        cvDilate(mask, mask, ball, 1);

        cvReleaseStructuringElement(&hLine);
        cvReleaseStructuringElement(&ball);

        CvMemStorage* storage = cvCreateMemStorage(0);
        CvSeq* contour = NULL;
        IplImage* img = cvCloneImage(mask);
        //int count = cvFindContours(img, storage, &contour, sizeof(CvContour),
        //               CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        //int count = cvFindContours(img, storage, &contour, sizeof(CvContour),
        //               CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_L1);
        int count = cvFindContours(img, storage, &contour, sizeof(CvContour),
                       CV_RETR_LIST, CV_LINK_RUNS);
        MSG("Found " << count << " blobs");

        for( ; contour != 0; contour = contour->h_next )
        {
            if (debug) {
                CvScalar color = cvScalar(0, 255, 0, 128);
                /* replace CV_FILLED with 1 to see the outlines */
                cvDrawContours(debugImg.getImage(), contour, color, color, 1, CV_FILLED, 8);
            }
            int num = contour->total;
            scoped_array<int32_t> hull(new int32_t[num*2]);
            CvMat hullMat = cvMat(1, num, CV_32SC2, hull.get());
            cvConvexHull2(contour, &hullMat, CV_CLOCKWISE, 1 /* points, not indexes */);

            int nPoints = hullMat.cols;

            Obstacle ob;
            pointsToObstacle(sib, contour, &ob.getVertices());            
            obst.push_back(ob);
#if 0
            // this code works for a polyline of signed 32 bit points (2 channels)
            assert(CV_IS_SEQ_POLYLINE( contour ));
            assert(CV_MAT_TYPE(contour->flags) == CV_32SC2); 
            CvSeqReader reader;
            cvStartReadSeq(contour, &reader, 0);
            for (int i = 0; i < contour->total; i++) {
                CvPoint pt;
                CV_READ_SEQ_ELEM( pt, reader );
                // do something with pt
            }
#endif
        }        
        cvReleaseImage(&img);
        cvReleaseMemStorage(&storage);
#endif

        MSG("detect(): returning " << obst.size() << " obstacles");
        
        if (debug)
            result = obst;
        // convert from vehicle frame to local frame
        for (unsigned int i = 0; i < obst.size(); i++)
        {
            vector<point2_uncertain>& v = obst[i].getVertices();
            for (unsigned int j = 0; j < v.size(); j++)
            {
                float xv = v[j].x;
                float yv = v[j].y;
                float zv = 0;
                float xl, yl, zl;
                StereoImageBlobVehicleToLocal(sib, xv, yv, zv, &xl, &yl, &zl);
                v[j].x = xl;
                v[j].y = yl;
                
            }
        }
        return obst;
    }

    //////////////////////////////////////////
    // OpenGL Debug Display stuff (GlRenderer)
    //////////////////////////////////////////


    void DisparityDetector::update(const SensnetBlob& blob) throw(Error)
    {
        StereoImageBlob* sib = (StereoImageBlob*) blob.getData();

        if (disp.getWidth() != sib->cols || disp.getHeight() != sib->rows) {
            disp.init(sib->cols, sib->rows);
        }

        // copy disparity image and add alpha channel
        int bpr = disp.getBytesPerRow();
        int16_t* start = (int16_t*)(sib->imageData + sib->dispOffset);
        int16_t* end = (int16_t*)(sib->imageData + sib->dispOffset + sib->dispSize);
        uint8_t* row = disp.getRawData();
        for (int16_t* p = start; p != end; p += sib->cols) {
            uint8_t* rp = row;
            for (int16_t* q = p; q != p + sib->cols; q++) {
                *rp++ = (*q >> 5) & 255;      // luminance
                *rp++ = (*q == -1) ? 0 : 255; // alpha (opaqueness)
            }
            row += bpr;
        }

    }

    void DisparityDetector::render() throw(Error)
    {
        // todo: check for errors!! (not a big deal actually, this is just for debugging)
        if (dispTex == 0 || mapTex == 0) {
            GLuint txt[4];
            glGenTextures(4, txt);
            dispTex = txt[0];
            mapTex = txt[1];
            maskTex = txt[2];
            debugTex = txt[3];
        }
        
        float dispScaleX = 1.0;
        float dispScaleY = 1.0;
        float mapScaleX = 1.0;
        float mapScaleY = 1.0;
        float maskScaleX = 1.0;
        float maskScaleY = 1.0;
        float debugScaleX = 1.0;
        float debugScaleY = 1.0;

        ////////////////////////////////////////////
        // Create the texture for the disparity image
        ////////////////////////////////////////////

        if (disp.getWidth() > 0) {
            CvSize sz = disp.toGlTexture(dispTex);
            dispScaleX = float(disp.getWidth())/sz.width;
            dispScaleY = float(disp.getHeight())/sz.height;
        }

        ////////////////////////////////////////////
        // Create the texture for the disparity map
        ////////////////////////////////////////////

        if (mapMax == 0) {
            mapMax = 1; // avoid divide-by-zero
        }

        if (dispMap.getWidth() > 0) {
            // create texture for the map, with luminance - alpha
            // TODO: move this in the class declaration to avoid reallocation
            scoped_array<uint8_t> mapImg(new uint8_t[dispMap.getWidth()*dispMap.getHeight()*2]);

            // maybe can be optimized ...
            int w = dispMap.getWidth();
            int h = dispMap.getHeight();
            for (int r = 0; r < h; r++) {
                for (int c = 0; c < w; c++) {
                    int32_t val = dispMap[r][c][0];
                    uint8_t pix;
                    uint8_t alpha = 255;
                    if (/*val < 64 && val >= 0*/ true) {
                        //pix = val * 256 / 64; // this will be optimized by the compiler
                        pix = val * 256 / mapMax;
                        if (val == 0) {
                            alpha = 0;
                        }
                    } else {
                        pix = 255;
                    }
                    mapImg[(r * w + c)*2] = pix;
                    mapImg[(r * w + c)*2 + 1] = alpha;
                }
            }

            glBindTexture(GL_TEXTURE_2D, mapTex);
        
            // no interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
            //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, dispMap.getWidth(),
            //                  dispMap.getHeight(), GL_LUMINANCE_ALPHA,
            //                  GL_UNSIGNED_BYTE, mapImg.get());
            int width, height;
            int bpp = 2;
            int bpr = w * bpp;
            buildPlainTexture(dispMap.getWidth(), dispMap.getHeight(), bpp, bpr,
                              GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, mapImg.get(),
                              &width, &height);
            mapScaleX = float(dispMap.getWidth())/width;
            mapScaleY = float(dispMap.getHeight())/height;
        }

#ifndef DISPDET_SINGLE_THRES
        CvMat sub;
        IplImage imgHdr;
        IplImage* mask = NULL;
        if (maskFill.getWidth() > 0) {
            cvGetSubRect(maskFill.getImage(), &sub,
                         cvRect(1, 1, maskFill.getWidth()-1, maskFill.getHeight()-1));
            mask = cvGetImage(&sub, &imgHdr);
        }
                
        // trick to get it to work quickly ;-)
        ImageCv<uint8_t, 1>& maskImg = maskFill;
#else
        IplImage* mask = maskImg.getImage();
#endif

        if (maskImg.getWidth() > 0) {
            if (debug) {
                // check if maskImg is in fact binary (only 0 or 255 values)
                int nZero = 0;
                int nMax = 0;
                int nOther = 0;
                int otherVal = 0;
                for(int r = 1; r < maskImg.getHeight()-1; r++) {
                    for(int c = 1; c < maskImg.getWidth()-1; c++) {
                        int val = maskImg[r][c][0];
                        if (val == 0) {
                            nZero ++;
                        } else if (val == 255) {
                            nMax ++;
                        } else {
                            nOther++;
                            if (otherVal == 0) 
                                otherVal = maskImg[r][c][0];
                            //maskImg[r][c][0] = 255;
                        }
                        //if (val == 255)
                        //    cerr << 'O';
                        //else 
                        //    cerr << '.';
                    }
                    //cerr << '\n';
                }
                MSG("Mask: nZero = " << nZero << ", nMax = " << nMax);
                if (nOther > 0)
                    MSG("Found " << nOther << " values that are not 0 or 255 (val=" << otherVal << ")");
            }

            CvSize sz = ImageCvBase::toGlTexture(mask, maskTex, GL_ALPHA);
            maskScaleX = float(maskImg.getWidth())/sz.width;
            maskScaleY = float(maskImg.getHeight())/sz.height;
        }

        if (debug && debugImg.getWidth() > 0) {
            CvSize sz = debugImg.toGlTexture(debugTex);
            debugScaleX = float(maskImg.getWidth())/sz.width;
            debugScaleY = float(maskImg.getHeight())/sz.height;
        }

        ////////////////////////////////////////////
        // Draw the disparity image
        ////////////////////////////////////////////

        glEnable(GL_TEXTURE_2D);
        // no lightning or coloring
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);


        // draw the disparity image and the map
        pushViewport();

        glColor3f(0.0, 0.5, 0.25); // dark green
        glBindTexture(GL_TEXTURE_2D, dispTex);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(dispScaleX, dispScaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 0.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (0.0, 0.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (0.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

#if 1
        ////////////////////////////////////////////
        // Draw the disparity map
        ////////////////////////////////////////////

        pushViewport();
        glColor3f(0.0, 0.0, 0.5); // dark blue
        glBindTexture(GL_TEXTURE_2D, mapTex);

        glTranslatef(1.0, 0.0, 0.0);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(mapScaleX, mapScaleY, 1.0);

        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 0.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (0.0, 0.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (0.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix();
        popViewport();

        
#endif
        // no depth text, we want the next images to overlap and blend
        glDisable (GL_DEPTH_TEST);
        // enable blending
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if 0
        ////////////////////////////////////////////
        // Draw the mask
        ////////////////////////////////////////////

        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);

        pushViewport();
        glTranslatef(1.0, 0.0, 0.0);

        glColor3f(1.0, 0.0, 0.0); // red
        glBindTexture(GL_TEXTURE_2D, maskTex);

        glMatrixMode(GL_TEXTURE);
        glPushMatrix();
        glScalef(maskScaleX, maskScaleY, 1.0);


        glBegin (GL_QUADS);
        glTexCoord2f (0.0, 0.0);
        glVertex3f (-1.0, 0.0, 0.0);

        glTexCoord2f (1.0, 0.0);
        glVertex3f (0.0, 0.0, 0.0);

        glTexCoord2f (1.0, 1.0);
        glVertex3f (0.0, -1.0, 0.0);

        glTexCoord2f (0.0, 1.0);
        glVertex3f (-1.0, -1.0, 0.0);
        glEnd ();

        glPopMatrix(); // texture
        popViewport();

#elif 0
        if (debug) {
            ////////////////////////////////////////////
            // Draw the debug image
            ////////////////////////////////////////////

            pushViewport();
            glTranslatef(1.0, 0.0, 0.0);

            glColor4f(0.0, 1.0, 0.0, 1.0); // green
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, debugTex);

            glMatrixMode(GL_TEXTURE);
            glPushMatrix();
            glScalef(debugScaleX, debugScaleY, 1.0);

            glBegin (GL_QUADS);
            glTexCoord2f (0.0, 0.0);
            glVertex3f (-1.0, 0.0, 0.0);

            glTexCoord2f (1.0, 0.0);
            glVertex3f (0.0, 0.0, 0.0);

            glTexCoord2f (1.0, 1.0);
            glVertex3f (0.0, -1.0, 0.0);

            glTexCoord2f (0.0, 1.0);
            glVertex3f (-1.0, -1.0, 0.0);
            glEnd ();

            glPopMatrix(); // texture
            popViewport();
        }
#endif

#if 1
        if (dbgEdgesX.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            int vp[4];
            glGetIntegerv(GL_VIEWPORT, vp);
            glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            // image coordinates
            glTranslatef(-1.0, 1.0, 0.0);
            glScalef(2.0/mask->width, -2.0/mask->height, 1.0);
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glDisable(GL_CULL_FACE);

#if 0
            glColor4f(1.0, 1.0, 1.0, 1.0); // white

            for (unsigned int i = 0; i < dbgEdgesX.size(); i++)
            {
                vector<int32_t> xv = dbgEdgesX[i];
                vector<int32_t> yv = dbgEdgesY[i];

                glBegin(GL_POINTS);
                //MSG("Points" << i << ":");
                for (unsigned int j = 0; j < xv.size(); j++)
                {
                    //cerr << "(" << xv[j] << "," << yv[j] << ") ";
                    glVertex3f(xv[j], yv[j], 1.0);
                }
                //cerr << endl;
                //MSG("Poly" << i << " END");
                glEnd();
            }
#endif

            glColor3f(1.0, 1.0, 1.0);
            for (unsigned int i = 0; i < dbgHullX.size(); i++)
            {
                vector<int32_t> xv = dbgHullX[i];
                vector<int32_t> yv = dbgHullY[i];

                glBegin(GL_POLYGON);
                //MSG("Poly" << i << ":");
                for (unsigned int j = 0; j < xv.size(); j++)
                {
                    //cerr << "(" << xv[j] << "," << yv[j] << ") ";
                    glVertex3f(xv[j], yv[j], 1.0);
                }
                //cerr << endl;
                //MSG("Poly" << i << " END");
                glEnd();
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
        }
#endif

#if 1
        if (result.size() > 0)
        {
            pushViewport();

            // set the viewport to the lower right quarter
            int vp[4];
            glGetIntegerv(GL_VIEWPORT, vp);
            glViewport(vp[0] + vp[2]/2, vp[1], vp[2]/2, vp[3]/2);

            glDisable(GL_TEXTURE_2D);
            glDisable(GL_CULL_FACE);


            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

            // draw x axis
            glColor4f(1.0, 1.0, 1.0, 1.0); // white
            glBegin(GL_LINES);
            glVertex3f(0.0, -1.0, 0.0);
            glVertex3f(0.0, 1.0, 0.0);
            glEnd();

            glColor4f(1.0, 0.3, 0.3, 0.5); // bright red

            glTranslatef(0.0, -1.3, 0.0);
            glScalef(0.06, 0.02, 0.06); // FIXME - skewed
            
            for (unsigned int i = 0; i < result.size(); i++)
            {
                vector<point2_uncertain>& vert = result[i].getVertices();
                //MSG("Poly" << i << ":");
                glBegin(GL_POLYGON);
                
                for (unsigned int j = 0; j < vert.size(); j++)
                {
                    // invert x and y
                    glVertex3f(vert[j].y, vert[j].x, 0.0);
                    //cerr << "(" << vert[j].x << "," << vert[j].y << ") ";
                }

                glEnd(); 
                //cerr << endl;
                //MSG("Poly" << i << " END");
           }

            //glEnable(GL_CULL_FACE);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glViewport(vp[0], vp[1], vp[2], vp[3]); // reset
            popViewport();
        }
#endif

        glEnable (GL_DEPTH_TEST);

    }

}
