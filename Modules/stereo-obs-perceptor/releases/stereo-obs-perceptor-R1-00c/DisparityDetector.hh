#ifndef __DISPARITYDETECTOR_HH__
#define __DISPARITYDETECTOR_HH__

#include "Detector.hh"
#include "GlRenderer.hh"
#include "ImageCv.hh"

namespace blobstereo
{
    /**
     * This class implements a detector based on the disparity map, that detects
     * generic obstacles (blobs) without any classification.
     */
    class DisparityDetector : virtual public Detector, virtual public GlRenderer
    {
        bool debug;

        typedef int16_t disp_t;
        ImageCv<uint8_t, 2> disp; // 2 channels, value and alpha
        unsigned int maxDisp;
        unsigned int mapSize;

        // map with accumulated disparity
        typedef uint8_t dispmap_t;
        ImageCv<dispmap_t, 1> dispMap;
#ifndef DISPDET_SINGLE_THRES
        CvMat maskMat;
        /// 2 pixel taller and wider than dispMap. Needed for cvFloodFill, shares part
        /// of the data with maskMat, so no need to copy.
        ImageCv<uint8_t, 1> maskFill; 
#else
        ImageCv<uint8_t, 1> maskImg;
#endif
/*
        ImageCv<uint16_t, 1> mapAvgRow; // average row value for some cell
        ImageCv<uint16_t, 1> mapVarRow; // variance of row value for some cell
*/
        dispmap_t mapMax; // maximum map value
        double highThres;
        double lowThres;

        ImageCv<uint8_t, 4> debugImg;

        // opengl stuff
        GLuint dispTex;
        GLuint mapTex;
        GLuint maskTex;
        GLuint debugTex;

        vector<Obstacle> result; // for debug display
        vector<vector<int32_t> > dbgEdgesX;
        vector<vector<int32_t> > dbgEdgesY;
        vector<vector<int32_t> > dbgHullX;
        vector<vector<int32_t> > dbgHullY;

    public:
        DisparityDetector(bool dbg = true,
                          int _maxDisp = 100, int _mapSize = 200,
                          double hiThres = 20, double loThres = 7);

        ~DisparityDetector();

        /* from Detector */
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob);

        /* from GlRenderer */

        virtual void render() throw(Error);

        /**
         * Copy the disparity image from the blob to 'disp'.
         */
        virtual void update(const SensnetBlob& blob) throw(Error);

    private:

        void pointsToObstacle(StereoImageBlob* sib, const CvArr* points,
                         vector<point2_uncertain>* vertices);

    };

}

#endif
