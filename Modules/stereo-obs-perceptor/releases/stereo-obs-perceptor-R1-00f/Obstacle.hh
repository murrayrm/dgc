#ifndef __BLOBLADAR_OBSTACLE_HH__
#define __BLOBLADAR_OBSTACLE_HH__

#include <vector>

#include <interfaces/VehicleState.h>
#include <frames/point2_uncertain.hh>

namespace blobstereo {

    using std::vector;

    class Obstacle
    {
        vector<point2_uncertain> vertices; // meters
        
        point2_uncertain speed; // m/s

        float likelyhood; // 0 = this obstacle is not there, 0<x<1 = maybe it's there, 1 = it's definitely there

        VehicleState state;

    public:

        Obstacle()
            : speed(0, 0), likelyhood(1)
        {
            memset(&state, 0, sizeof(state));
        }

        /* Getters and Setters */

        vector<point2_uncertain>& getVertices()
        {
            return vertices;
        }

        const vector<point2_uncertain>& getVertices() const
        {
            return vertices;
        }

        point2_uncertain& getSpeed()
        {
            return speed;
        }

        const point2_uncertain& getSpeed() const
        {
            return speed;
        }

        float getLikelyhood() const
        {
            return likelyhood;
        }

        void setLikelyhood(float lh)
        {
            likelyhood = lh;
        }

        VehicleState& getState()
        {
            return state;
        }

        const VehicleState& getState() const
        {
            return state;
        }

        // ... anything else?
    };

} // end namespace

#endif
