#ifndef __DISPARITYDETECTOR_HH__
#define __DISPARITYDETECTOR_HH__

#include "Detector.hh"
#include "GlRenderer.hh"
#include "ImageCv.hh"

namespace blobstereo
{


    /**
     * This class implements a detector based on the disparity map, that detects
     * generic obstacles (blobs) without any classification.
     */
    class DisparityDetector : virtual public Detector, virtual public GlRenderer
    {
        struct RegionData
        {
            float xc, yc; // center point (mean)
            CvMat cov; // 2 x 2 covariance matrix
            CvRect rect; // bounding box
            CvMat edges; // edge points
            // statistics about height of the blob
            float meanHeight;
            float varHeight;

            // used only for debugging porposes, when m_debug == true
            vector<int32_t>* debugXv;
            vector<int32_t>* debugYv;
            // same mean/varHeight, but in image coords
            float meanRow;
            float varRow;
            
            int minRow;
            int maxRow;
            
            RegionData(float _xc = 0, float _yc = 0, 
                       vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
                : xc(_xc), yc(_yc), debugXv(dbgXv), debugYv(dbgYv)
            {
                memset(&cov, 0, sizeof(cov));
                memset(&rect, 0, sizeof(rect));
                memset(&edges, 0, sizeof(edges));
            }

            // are the following two constructors needed at all??

            RegionData(float _xc, float _yc, CvMat _cov, CvRect _rect,
                       vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
                : xc(_xc), yc(_yc), cov(_cov), rect(_rect),
                  debugXv(dbgXv), debugYv(dbgYv)
            {
                memset(&edges, 0, sizeof(edges));
            }

            RegionData(float _xc, float _yc, CvMat _cov, CvRect _rect, CvMat _edges,
                       vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
                : xc(_xc), yc(_yc), cov(_cov), rect(_rect), edges(_edges),
                  debugXv(dbgXv), debugYv(dbgYv)
            { }

            RegionData(const RegionData& d)
            {
                cov.data.ptr = NULL;
                edges.data.ptr = NULL;
                *this = d; // call operator=()
            }

            RegionData& operator=(const RegionData& d)
            {
                xc = d.xc;
                yc = d.yc;
                rect = d.rect;
                meanHeight = d.meanHeight;
                varHeight = d.varHeight;
                debugXv = debugYv = NULL; // no need to copy these
                meanRow = d.meanRow;
                varRow = d.varRow;

                if (cov.data.ptr != NULL)
                {
                    CvMat *m = cvCloneMat(&d.cov);
                    cov = *m;
                    cvIncRefData(&m);
                    cvReleaseMat(&m);
                }

                if (edges.data.ptr != NULL)
                {
                    CvMat* m = cvCloneMat(&d.edges);
                    edges = *m;
                    cvIncRefData(&m);
                    cvReleaseMat(&m);
                }
                return *this;
            }

            ~RegionData()
            {
                if (cov.data.ptr != NULL)
                {
                    cvReleaseData(&cov);
                }
                if (edges.data.ptr != NULL)
                {
                    cvReleaseData(&edges);
                }
            }
        };

        typedef int16_t disp_t;
        unsigned int m_maxDisp;
        unsigned int m_mapHeight; // REMOVE ME

        //typedef uint8_t dispmap_t;
        typedef float dispmap_t;
        /// map with accumulated disparity
        ImageCv<dispmap_t, 1> m_mapImg;
        /// Other information about the map, one for each channel:
        /// - maximum height
        /// - minimum height (TODO: if >>0 ==> floating ==> discard)
        /// - height mean
        /// - height variance
        /// TODO: USE ME!!!
        CvMat m_mapStat;
        /// 2 pixel taller and wider than m_mapImg. Needed for cvFloodFill.
        ImageCv<uint8_t, 1> m_maskImg; 
        /// Shares part of the data with maskImg, so no need to copy.
        CvMat m_maskMat;

        ImageCv<float, 1> m_rowSum;
        ImageCv<float, 1> m_rowSumSq;
        ImageCv<float, 1> m_maxRow;
        ImageCv<float, 1> m_minRow;

        dispmap_t m_mapMax; // maximum value in the current map
        double m_highThres;
        double m_lowThres;
        double m_hThres; // fuzzy height threshold
        double m_hThresSlope; // slope of the curve when h == m_heightThres

        // weight tables
        static const float m_weightTabRes = 0.1;
        vector<float> m_hWeight;
        vector<float> m_dhWeight;


        /*
         * DEBUGGING STUFF
         */

        /** Shows the detected obstacles, in image coordinates.
         * Should be overlayed on a ImageRenderer.
         */
        class ObstOverlayRenderer : virtual public GlRenderer
        {
            DisparityDetector& self;
            int w;
            int h;

        public:
            ObstOverlayRenderer(DisparityDetector& me)
                : self(me), w(1), h(1)
            { }

            void update(const SensnetBlob& blob) throw();

            /// Shows the detected obstacles as overlay boxes.
            void render() throw(Error);
        };

        /// if true, run debugging code
        bool m_debug;

        ImageCv<uint8_t, 2> m_dispImg; /// Disparity, 2 channels, value and alpha
        ImageCv<uint8_t, 4> m_debugImg; /// General purpose debugging display (RGBA)

        // opengl stuff
        GLuint m_dispTex;
        GLuint m_mapTex;
        GLuint m_maskTex;
        GLuint m_debugTex;

        vector<Obstacle> m_result; // for debug display
        vector<RegionData> m_regions;
        vector<vector<int32_t> > m_dbgEdgesX;
        vector<vector<int32_t> > m_dbgEdgesY;
        vector<vector<int32_t> > m_dbgHullX;
        vector<vector<int32_t> > m_dbgHullY;

    public:
        DisparityDetector(bool dbg = true,
                          int maxDisp = 100, int mapHeight = 200,
                          double hiThres = 10, double loThres = 3,
                          double heightThres = 4, double hThresSlope = 3);

        ~DisparityDetector();

        /* from Detector */
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob);

        /* from GlRenderer */

        virtual void render() throw(Error);

        /**
         * Copy the disparity image from the blob to 'disp'.
         */
        virtual void update(const SensnetBlob& blob) throw(Error);

        virtual vector<RenderWidget*> getWidgets();

    private:

        void pointsToObstacle(StereoImageBlob* sib, const CvArr* points,
                              vector<point2_uncertain>* vertices);

        // gather separate statistics for the high part and low part
        // so we can detect and handle trees properly.
        // (trees appears as very wide objects, but we care only about
        // the trunk).
        
        /** Given a CvArr containing a mask, find all the useful information about
         * the new blob contained into it (points with value == 1), and put it
         * into the 'data' structure.
         * find all the edge points and put them
         * in the points matrix, which is a 1D row vector containing 2D points
         * (type CV_32SC2, two channels of int32_t);
         * Note: if maskArr is of size (rows, cols), it searches in the rectangle
         * (1, 1, rows - 2, cols - 2) for a blob, and the mask boundary should be
         * all set to zero.
         */
        void scanRegion(CvArr* maskArr, int xOff, int yOff, RegionData* data);

        /**
         * Filters (removes) all the points in the specified region (where the mask is == 1) that
         * have a minimum row bigger than the threshold.
         * This is mainly to cut trees.
         */
        void filterRegion(CvArr* maskArr, int xOff, int yOff, RegionData* data);
    };

}

#endif
