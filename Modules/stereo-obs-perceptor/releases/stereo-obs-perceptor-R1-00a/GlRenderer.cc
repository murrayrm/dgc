// std C++ headers
#include <iostream>
#include <string>
#include <csetjmp>
// opengl stuff
#include <GL/glu.h>
#include <GL/glut.h>
// DGC headers
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
// local headers
#include "GlRenderer.hh"
#include "util.hh"

namespace blobstereo
{

    static void pushViewport()
    {
        // cut'n'pasted from sensviewer, little changes
        int vp[4];
      
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();

        // Get the viewport dimensions
        glGetIntegerv(GL_VIEWPORT, vp);
        
        // Shift and rescale such that (0, 0) is the top-left corner
        // and (cols, rows) is the bottom-right corner.
        glTranslatef(0.0, 1.0, 0);
        glScalef(2.0/vp[2], -2.0/vp[3], 1.0);
    }

    static void popViewport()
    {
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
    }


    ImagePairRenderer::ImagePairRenderer()
    {
        txtId[0] = txtId[1] = 0;
        imgList[0] = imgList[1] = 0;
    }

    /**
     * If the blob is not a StereoBlob, or some error occurs, throws
     * a GlRenderer::Error exception.
     */
    void ImagePairRenderer::update(const SensnetBlob& blob) throw(Error)
    {
        // copy the images from the blob to m_left and m_right
        StereoImageBlob* sib = reinterpret_cast<StereoImageBlob*>(blob.getData());

        if (sib->blobType != SENSNET_STEREO_IMAGE_BLOB) {
            throw Error(ERRSTR("Blob type is not SENSNET_STEREO_IMAGE_BLOB"));
        }
        
        Image::format_t fmt = Image::FORMAT_GREY;
        if (sib->channels == 3) {
            fmt = Image::FORMAT_RGB;
        }

        img[0].init(sib->cols, sib->rows, fmt);
        img[1].init(sib->cols, sib->rows, fmt);

#if 1
        memcpy(img[0].getData(), sib->imageData + sib->leftOffset, sib->leftSize);
        memcpy(img[1].getData(), sib->imageData + sib->rightOffset, sib->rightSize);
#endif

        dirty = true;
    }


#if 0 // remove me
        if (fmt == Image::FORMAT_GREY) {
            // just memcopy, une byte per pixel
            memcpy(left.getData(), sib->imageData + sib->leftOffset, sib->leftSize);
            memcpy(right.getData(), sib->imageData + sib->rightOffset, sib->rightSize);
        }
        else
        {
            // convert from RGB24 ro RGBA32
            int8_t* start = sib->imageData + sib->leftOffset;
            int8_t* end = start + sib->leftSize;
            int8_t* data = left.getData();
            for (int8_t* p = start; p != end; p+=3, data += 4)
            {
                data[0] = p[0];
                data[1] = p[1];
                data[2] = p[2];
                data[3] = 0;
            }
            // again for the right image
            start = sib->imageData + sib->rightOffset;
            end = start + sib->rightSize;
            data = right.getData();
            for (int8_t* p = start; p != end; p+=3, data += 4)
            {
                data[0] = p[0];
                data[1] = p[1];
                data[2] = p[2];
                data[3] = 0;
            }
        }
#endif

    /** Shows the two images on the screen side by side (2D).
     */
    void ImagePairRenderer::render() throw(Error)
    {
        //MSG("DEBUG: entering ImagePairRendere::render()");

        // todo: check for errors
        if (txtId[0] <= 0) {
            glGenTextures(2, txtId);
        }

        if (imgList[0] <= 0) {
            imgList[0] = glGenLists(2);
            imgList[1] = imgList[0] + 1;
        }

        for (int i = 0; i < 2; i++) { // two times
            glBindTexture(GL_TEXTURE_2D, txtId[i]);

            //glPixelStoref(GL_UNPACK_ALIGNMENT, 1);

            // linear interpolation
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

            // no lightning or coloring
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);

            if (img[i].getBytesPerPixel() == 1) {
                gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img[i].getWidth(), img[i].getHeight(), GL_LUMINANCE,
                                  GL_UNSIGNED_BYTE, img[i].getData());
//                 glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, sib->cols, sib->rows,
//                               0, GL_LUMINANCE, GL_UNSIGNED_BYTE, img[i].getData());
            } else {
                gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img[i].getWidth(), img[i].getHeight(), GL_RGB,
                                  GL_UNSIGNED_BYTE, img[i].getData());
//                 glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, sib->cols, sib->rows,
//                               0, GL_RGB, GL_UNSIGNED_BYTE, img[i].getData());
            }
        }

        for (int i = 0; i < 2; i++) { // two times
            glEnable(GL_TEXTURE_2D);
            pushViewport();
            glBindTexture (GL_TEXTURE_2D, txtId[i]);
            if (i == 0) { // left image
                glTranslatef(-img[i].getWidth()/2, 0, 0);
            }
            glScalef(0.5, 0.5, 1);

            glBegin (GL_QUADS);
            glTexCoord2f (0.0, 0.0);
            glVertex3f (0.0, 0.0, 0.0);
            glTexCoord2f (1.0, 0.0);
            glVertex3f (img[i].getWidth(), 0.0, 0.0);
            glTexCoord2f (1.0, 1.0);
            glVertex3f (img[i].getWidth(), img[i].getHeight(), 0.0);
            glTexCoord2f (0.0, 1.0);
            glVertex3f (0.0, img[i].getHeight(), 0.0);
            glEnd ();

            popViewport();
            glDisable(GL_TEXTURE_2D);
        }

        dirty = false;
    }
    
    // using a lot of global variables is not considered good programming,
    // but it seems to be the only way with glut
    static GlRenderer* mainRenderer = NULL;
    static pthread_mutex_t* rendererMtx = NULL;
    static pthread_cond_t* rendererCond = NULL;
    static bool quit = false;
    static pthread_t glutThreadID = -1;
    static bool posted = false;

    // longjmp to exit from glutMainLoop, there seem to be no other easy way :-(
#warning "TODO: get rid of glut!!!"
    static jmp_buf quitJmp;

    static void displayFunc()
    {
        glClearColor (0.0, 0.0, 0.0, 1.0); // clear the screen to black
        // clear the color buffer and the depth buffer
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        // camera position: at x,y,z, looking at x,y,z, up x,y,z
        gluLookAt (0.0, 0.0, 0.0,   1.0, 0.0, 0.0,   0.0, 1.0, 0.0);
        
        // usually a good idea
        glEnable (GL_DEPTH_TEST); //enable the depth testing
  
        pthread_mutex_lock(rendererMtx);
        mainRenderer->render();
        pthread_mutex_unlock(rendererMtx);

        glutSwapBuffers();
        posted = false;
    }


    static void idleFunc()
    {
        //MSG("IdleFunc() called");

        if (quit)
            longjmp(quitJmp, 1);

        bool redraw = false;
        pthread_mutex_lock(rendererMtx);
        redraw = mainRenderer->isDirty();
        pthread_mutex_unlock(rendererMtx);

        if (redraw) {
            if (!posted) {
                glutPostRedisplay();
                posted = true;
            }
        } else {
            usleep(0); // yield, give other threads/processes a chance to run
        }
    }

    void* glutThread(void* /* param */)
    {
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(640,480);
	glutCreateWindow("BlobStereo - Debug OpenGL Display");

        // set callbacks
        glutDisplayFunc(displayFunc);
        glutIdleFunc(idleFunc);

        glClearColor(0.0, 0.0, 0.0, 0.0); //black bkgrnd
        glColor3f(1.0, 1.0, 1.0);
        if (setjmp(quitJmp) == 0) {
            glutMainLoop();
        } else {
            // nothing, we just wanted to get out of glutMainLoop!
        }
        return NULL;
    }

    int startGlutThread(int argc, char** argv,
                         GlRenderer* mainRenderer,
                         pthread_mutex_t* rendererMtx,
                         pthread_cond_t* rendererCond)
    {
        blobstereo::mainRenderer = mainRenderer; // make it available to glut callbacks
        blobstereo::rendererMtx = rendererMtx; // same as above
        blobstereo::rendererCond = rendererCond; // same as above // FIXME: NOT USED, use or remove
        static bool glutInited = false;
        if (!glutInited) {
            glutInit(&argc, argv);
            glutInited = true;
        }

        return pthread_create(&glutThreadID, NULL, glutThread, NULL);
        //return glutThreadId;
    }

    void stopGlutThread()
    {
        if (glutThreadID == pthread_t(-1))
            return;

        quit = true;
        pthread_join(glutThreadID, NULL);
        quit = false; // just in case we want to restart the thread
    }

}
