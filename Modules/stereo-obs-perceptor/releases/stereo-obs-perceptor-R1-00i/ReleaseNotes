              Release Notes for "stereo-obs-perceptor" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "stereo-obs-perceptor" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "stereo-obs-perceptor" module can be found in
the ChangeLog file.

Release R1-00i (Fri Jul 20  3:59:24 2007):
	Added commandline options for most of the detector parameters, for
	easier tuning.

Release R1-00h (Thu Jul 19 17:54:47 2007):
        Fixed compilation error.
        Also, this release's link module is linked against the latest
        version of map and interfaces, so it shouldn't make mapper and
        other modules segfault.

Release R1-00g (Fri Jun 15 16:46:00 2007):
        - Better FLTK-OpenGL display.
        - Implemented small obstacle removal.
        - Not working (yet): first implementation of a basic
        association and filtering algorithm, needs debugging
        and tuning. It's disabled (commented out) in the release.
        - Added uncertainty to the generated obstacles. It looks
        like it's buggy, but nobody is using it for now, though.

Release R1-00f (Fri May 25 12:07:51 2007):
	Changed the OpenGL Display, moved from GLUT to a much nicer FLTK GUI.
	The detection algorithm is more or less the same, I've been trying with
	some new code, but it's commented out because it doesn't work very well.

Release R1-00d (Sun Apr 29  0:19:06 2007):
	Now showing detected obstacles in the console display.
	Some code cleanup and refactoring (more needs to be done).
	Did some quick tests with alice and the medium range cameras:
	* The perceptor can see obstacles as close as approx 5 meters
	  (~9.6m from the rear axle).
	* There seems to be an overestimation error of about 50-60cm,
	  and this could be a bug in the code (need to investigate).
	* The feeder and the perceptor are running at just 2Hz, with the
	  perceptor using approx 15% of cpu time, and 85% the feeder.
	  This will improve when the new core2 cpus arrive, but needs
	  some work on the software side too.

Release R1-00c (Thu Apr 26 19:29:05 2007):
	Finally sending obstacles to the mapper! Showing the number of
	obstacles sent in the cotk display.
	Fixed a bug in the detector which generated wrong range values.
	The opengl display uses a lot of cpu time (80% in the lab), need
	to be fixed. The actual detection algorithm is a lot faster (use
	"-g no" to disable opengl).

Release R1-00b (Thu Apr 26  2:08:59 2007):
	This version uses a disparity-based algorithm to detect blobs, and plots
	them on the debug display window as 2D polygons.
	No message is sent to the mapper yet, and no tracking is done.

Release R1-00a (Wed Apr 18 20:53:12 2007):
	First version of the stereo obstacle detector ("blobstereo"). This
	is just a shell, no detection is performed.
	Compiles correctly, and creates a simple opengl view showing the left
	and right images.	

Release R1-00 (Wed Apr 18 18:35:29 2007):
	Created.








