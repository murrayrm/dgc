#ifndef __GLUTIL_HH__
#define __GLUTIL_HH__

#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>

namespace stereo_obs
{

#warning "TODO: move some of these function in a .cc file"

    inline void pushViewport()
    {
        // cut'n'pasted from sensviewer, little changes
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
    }

    inline void setImageCoord()
    {
        // cut'n'pasted from sensviewer, little changes
        int vp[4];
      
        // Get the viewport dimensions
        glGetIntegerv(GL_VIEWPORT, vp);
        
        // Shift and rescale such that (0, 0) is the top-left corner
        // and (cols, rows) is the bottom-right corner.
        glTranslatef(0.0, 1.0, 0);
        glScalef(2.0/vp[2], -2.0/vp[3], 1.0);
    }

    inline void popViewport()
    {
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
    }

    /**
     * Creates a texture without changing the actual values of the original
     * pixels (no mipmapping, rescaling, etc).
     * If the size is not a power of two, a bigger texture is created, filled with zeros,
     * and the original data is copied in the upper left corner.
     * @param inWidth Width of the original image.
     * @param inWidth Height of the original image.
     * @param bpp Bytes per pixel of the image.
     * @param bpr Bytes per row of the image (>= bpp * width)
     * @param format The texture format, as required by glTexImage2D.
     * @param type The input data type, as required by glTexImage2D.
     * @param dataIn The raw image data.
     * @param[out] outWidth The resulting width of the texture.
     * @param[out] outHeight The resulting height of the texture.
     * @return The new size of the texture.
     */
    inline void buildPlainTexture(int inWidth, int inHeight, int bpp, int bpr,
                                  int format, int type, uint8_t* dataIn,
                                  int* outWidth, int* outHeight)
    {
        // let's do it manually, without mipmapping, our own way
        // find minimum power of 2 bigger than width and height
        int pw, ph;
        for (pw = 1; pw < 32; pw++) {
            if ((1L << pw) >= inWidth)
                break;
        }
        for (ph = 1; ph < 32; ph++) {
            if ((1L << ph) >= inHeight)
                break;
        }
        int width = 1 << pw;
        int height = 1 << ph;
        uint8_t* data = new uint8_t[width * height * bpp];
        // do not scale, copy image in the upper left part of the new one
        memset(data, 0, width * height * bpp);
        uint8_t* rowOut = data;
        uint8_t* rowIn = dataIn;
        for (int r = 0; r < inHeight; r++) {
            memcpy(rowOut, rowIn, inWidth * bpp);
            rowOut += width * bpp;
            //rowIn  += inWidth * bpp; // assumption: no extra bytes at the end of the row
            rowIn += bpr;
        }
        glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, format,
                     type, data);
        delete[] data;
        if (outWidth)
            *outWidth = width;
        if (outHeight)
            *outHeight = height;
    } 

    static inline void printGlMatrix(const float m[4][4], bool rowMaj = false)
    {
        using std::cerr;
        using std::endl;
        if (rowMaj)
        {
            for (int i = 0; i < 4; i++) {
                cerr << "\t[";
                for (int j = 0; j < 4; j++) {
                    cerr << m[i][j] << ",\t";
                } 
                cerr << "]\n";
            }
        } else {
            for (int i = 0; i < 4; i++) {
                cerr << "\t[";
                for (int j = 0; j < 4; j++) {
                    cerr << m[j][i] << ",\t";
                } 
                cerr << "]\n";
            }
         }
        cerr << endl;
    }
    
}

#endif
