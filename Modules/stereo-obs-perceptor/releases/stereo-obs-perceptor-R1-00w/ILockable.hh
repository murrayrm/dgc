#ifndef __STEREO_OBS_ILOCKABLE__
#define __STEREO_OBS_ILOCKABLE__

#include <pthread.h>

namespace stereo_obs {

    class ILockable
    {
      public:
        virtual ~ILockable() { } 
        virtual void lock() = 0;
        virtual void unlock() = 0;
    };

    class MutexLockable : virtual public ILockable
    {
    protected:
        pthread_mutex_t m_mtx;
    public:
        MutexLockable() {
            pthread_mutex_init(&m_mtx, NULL);
        }
        virtual void lock() { pthread_mutex_lock(&m_mtx); };
        virtual void unlock() { pthread_mutex_unlock(&m_mtx); };
    };

    // Use volatile objects and methods to avoid race conditions
    // See http://www.ddj.com/dept/cpp/184403766
    template <typename T> // T should be a ILockable, or have lock() and unlock() methods
    class LockingPtr {
    public:
        // Constructors/destructors
        LockingPtr(volatile T* myObj)
            : obj(const_cast<T*>(myObj))
        {
            obj->lock();
        }

        ~LockingPtr()
        {
            obj->unlock();
        }

        T* get() { return obj; }
        // Pointer behavior
        T& operator*() { return *obj; }
        T* operator->() { return obj; }
    private:
        T* obj;
        // copy forbidden
        LockingPtr(const LockingPtr&);
        LockingPtr& operator=(const LockingPtr&);
    };

}

#endif
