# Command-line options for blobstereo (use gengetopt to generate C the source code)
# gengetopt -i blobstereo_cmdline.ggo -F blobstereo_cmdline --unamed-opts

# note: string wrapping using \ at the end of the line doesn't seem to work well
 
package "Stereo Obstacle Perceptor"
purpose "Reads stereo blobs and detects obstacles using the disparity information"
version "0.1"

section "Basic options"

option "spread-daemon" d
  "Spread daemon" string default="4083@localhost" no

option "skynet-key" S
  "Skynet key" int no

option "module-id" m
  "Module id, should be one of MODstereoObsPerceptorXXX, where XXX is Long, Medium or PTU" string default="MODstereoObsPerceptorLong" no

option "sensor-id" s
  "Sensor id, usually SENSNET_MF_LONG_STEREO or SENSNET_MF_MEDIUM_STEREO (PTU? we'll see when we have it)" string default="SENSNET_MF_LONG_STEREO" no

option "opengl" g
  "Show OpenGL debug display" flag off

option "disable-console" b
  "Disable console display (batch mode)" flag off


section "Advanced tuning options"


option "debug" -
  "Turn on some debugging code" int default="1" no

option "max-disp" -
  "Maximum expected disparity (bigger values will be ignored)" int default="80" no

option "map-height" -
  "Number of rows used in the 'map' (accumulation buffer)" int default="300" no

option "high-thres" -
  "High threshold. Used to detect peaks in the map" float default="8" no

option "low-thres" -
  "Low threshold. Used to flood fill peaks found with the high threshold" float default="3" no

option "small-obs-thres" -
  "Minimum number of votes (disparity image pixels) for a blob to be detected" float default="40" no

option "error-var" -
  "Variance of the error for the three coordinates (col, row, disparity). It should be a string with 3 values (covariance assumed to be diagonal)" string default="64 25 1" no

option "assoc-thres" -
  "Threshold for associating one track to one measure" float default="2" no

option "disable-tracking" -
  "Disable tracking entirely" flag off

option "conf-thres" -
  "Minimum confidence level for a track to be actually sent to the mapper" float default="0.8" no
