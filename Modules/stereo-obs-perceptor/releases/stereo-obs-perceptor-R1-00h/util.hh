#ifndef __BLOBSTEREO_UTIL_HH__
#define __BLOBSTEREO_UTIL_HH__

#include <sstream>
#include <exception>
#include <sys/time.h>
#include <cotk/cotk.h>
#include <ncurses.h>

// convert anything to a string
// use it to build string on one line, like "i = " + toStr(i)
template<typename T> std::string toStr(const T& x) {
    std::ostringstream os;
    os << x;
    return os.str();
}
// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

#define FILE_LINE std::string(__FILE__ ":") + toStr(__LINE__)
#define ERRSTR(str) FILE_LINE  + ": " + str

// use like in: throw ERROR("message")
#define ERROR(str) blobstereo::Error(std::string(__FILE__ ":") + toStr(__LINE__) + ": " + str)


namespace blobstereo {

    /** Generic exception that is thrown when something goes wrong.
     */
    class Error : public std::exception {
        std::string msg;
    public:
        Error(std::string m) : msg(m) { }
        ~Error() throw() { }
        virtual std::string getMsg() const throw() { return msg; }
        virtual const char* what() const throw() { return msg.c_str(); }
    };

    inline uint64_t gettime()
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    }

    // wait for a keypress or for the timeout to pass, whichever comes first.
    // timeout is in microseconds.
    // Returns 0 if timeout expires, or != 0 if a key is pressed.
    inline int uwait(uint64_t tmout)
    {
        uint64_t endTime = gettime() + tmout;
        if (tmout > 100000L) // one tenth of a second
        {
            long tenths = tmout / 100000L;
            long msecs = tenths * 100;
            // wait for a keypress
            timeout(msecs); // it's a ncurses function, if it wasn't clear ... ;-)
            int c = getch();
            timeout(0);
            if (c != ERR) {
                ungetch(c);
                return 1;
            }
        }
        usleep(endTime - gettime());
        return 0;
    }

    class BlobStereo;
    typedef int (BlobStereo::*cotk_member_callback_t)(cotk_t *cotk, const char *token);

    struct CotkMemberCallbackData {
        cotk_member_callback_t cb;
        BlobStereo* self;
        CotkMemberCallbackData(BlobStereo* _self, cotk_member_callback_t _cb)
            : cb(_cb), self(_self)
        { }
    };

    inline int cotkMemberCallback(cotk_t *cotk, void *d, const char *token)
    {
        CotkMemberCallbackData* data = reinterpret_cast<CotkMemberCallbackData*>(d);
        return (data->self->*(data->cb))(cotk, token);
    }


    template<class OStr, class T>
    void printMat(OStr& out, T mat[4][4], std::string name = "mat")
    {
	// matlab-style (so you can cut'n'paste it in matlab if you want)
	std::string indent = std::string(name.length() + 4, ' ');
	out << name << " = [\n";
	for (int i = 0; i < 4; i++)
	{
	    out << indent << ' ';
	    for (int j = 0; j < 4; j++)
		out << mat[i][j] << (j < 3 ? ", " : "");
	    out << ";\n";
	}
	out << indent << "];" << std::endl;
    }

    template<class T>
    void printMat(T mat[4][4], std::string name = "mat")
    {
	printMat(std::cout, mat, name);
    }

}

#endif
