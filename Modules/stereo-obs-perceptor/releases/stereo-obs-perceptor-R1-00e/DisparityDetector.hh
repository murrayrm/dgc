#ifndef __DISPARITYDETECTOR_HH__
#define __DISPARITYDETECTOR_HH__

#include "Detector.hh"
#include "GlRenderer.hh"
#include "ImageCv.hh"

namespace blobstereo
{
    /**
     * This class implements a detector based on the disparity map, that detects
     * generic obstacles (blobs) without any classification.
     */
    class DisparityDetector : virtual public Detector, virtual public GlRenderer
    {
        typedef int16_t disp_t;
        unsigned int m_maxDisp;
        unsigned int m_mapHeight; // REMOVE ME

        // map with accumulated disparity
        typedef uint8_t dispmap_t;
        ImageCv<dispmap_t, 1> m_mapImg;
        /// 2 pixel taller and wider than m_mapImg. Needed for cvFloodFill.
        ImageCv<uint8_t, 1> m_maskImg; 
        /// Shares part of the data with maskImg, so no need to copy.
        CvMat m_maskMat;
/*
        ImageCv<uint16_t, 1> mapAvgRow; // average row value for some cell
        ImageCv<uint16_t, 1> mapVarRow; // variance of row value for some cell
*/
        dispmap_t m_mapMax; // maximum value in the current map
        double m_highThres;
        double m_lowThres;

        /*
         * DEBUGGING STUFF
         */

        /// if true, run debugging code
        bool m_debug;

        ImageCv<uint8_t, 2> m_dispImg; /// Disparity, 2 channels, value and alpha
        ImageCv<uint8_t, 4> m_debugImg; /// General purpose debugging display (RGBA)

        // opengl stuff
        GLuint m_dispTex;
        GLuint m_mapTex;
        GLuint m_maskTex;
        GLuint m_debugTex;

        vector<Obstacle> result; // for debug display
        vector<vector<int32_t> > m_dbgEdgesX;
        vector<vector<int32_t> > m_dbgEdgesY;
        vector<vector<int32_t> > m_dbgHullX;
        vector<vector<int32_t> > m_dbgHullY;

    public:
        DisparityDetector(bool dbg = true,
                          int maxDisp = 100, int mapHeight = 200,
                          double hiThres = 20, double loThres = 7);

        ~DisparityDetector();

        /* from Detector */
        
        virtual vector<Obstacle> detect(const SensnetBlob& blob);

        /* from GlRenderer */

        virtual void render() throw(Error);

        /**
         * Copy the disparity image from the blob to 'disp'.
         */
        virtual void update(const SensnetBlob& blob) throw(Error);

    private:

        void pointsToObstacle(StereoImageBlob* sib, const CvArr* points,
                              vector<point2_uncertain>* vertices);

        struct RegionData
        {
            float xc, yc; // center point (mean)
            CvMat cov; // 2 x 2 covariance matrix
            CvRect rect; // bounding box
            CvMat edges; // edge points

            // used only for debugging porposes, when m_debug == true
            vector<int32_t>* debugXv;
            vector<int32_t>* debugYv;
            
            RegionData(float _xc = 0, float _yc = 0, 
                       vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
                : xc(_xc), yc(_yc), debugXv(dbgXv), debugYv(dbgYv)
            {
                memset(&cov, 0, sizeof(cov));
                memset(&rect, 0, sizeof(rect));
                memset(&edges, 0, sizeof(edges));
            }

            // are the following constructors needed at all??

            RegionData(float _xc, float _yc, CvMat _cov, CvRect _rect,
                       vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
                : xc(_xc), yc(_yc), cov(_cov), rect(_rect),
                  debugXv(dbgXv), debugYv(dbgYv)
            {
                memset(&edges, 0, sizeof(edges));
            }

            RegionData(float _xc, float _yc, CvMat _cov, CvRect _rect, CvMat _edges,
                       vector<int32_t>* dbgXv=NULL, vector<int32_t>* dbgYv=NULL)
                : xc(_xc), yc(_yc), cov(_cov), rect(_rect), edges(_edges),
                  debugXv(dbgXv), debugYv(dbgYv)
            { }

            ~RegionData()
            {
                if (cov.data.ptr != NULL)
                {
                    cvReleaseData(&cov);
                }
                if (edges.data.ptr != NULL)
                {
                    cvReleaseData(&edges);
                }
            }
        };

        // gather separate statistics for the high part and low part
        // so we can detect and handle trees properly.
        // (trees appears as very wide objects, but we care only about
        // the trunk).
        
        /** Given a CvArr containing a mask, find all the useful information about
         * the new blob contained into it (points with value == 1), and put it
         * into the 'data' structure.
         * find all the edge points and put them
         * in the points matrix, which is a 1D row vector containing 2D points
         * (type CV_32SC2, two channels of int32_t);
         * Note: if maskArr is of size (rows, cols), it searches in the rectangle
         * (1, 1, rows - 2, cols - 2) for a blob, and the mask boundary should be
         * all set to zero.
         */
        static void scanRegion(CvArr* maskArr, int xOff, int yOff, RegionData* data);
    };

}

#endif
