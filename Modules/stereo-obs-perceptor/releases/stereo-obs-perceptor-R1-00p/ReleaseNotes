              Release Notes for "stereo-obs-perceptor" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "stereo-obs-perceptor" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "stereo-obs-perceptor" module can be found in
the ChangeLog file.

Release R1-00p (Thu Aug 30 18:34:20 2007):
	Implemented track ids between [0, 499], reusing them when no new
	ones are available.
	Setting the z coordinate in the MapElements (geometry, position,
	center). The z coordinate in the position is at the center of the
	obstacle, the bottom and top can be easily calculated using the
	height. The center is set to be equal to the position.

Release R1-00o (Tue Aug 28 13:27:15 2007):
        Implemented a fuzzy threshold on the height (in vehicle frame) of
        disparity points, to remove floating obstacles (tree leaves). 
        Does not remove ground strikes, but that can (and will) be added later
        if needed.
        Default is to cut at 3m, with a weight going down like a gaussian with
        sigma 0.5 (options --height-thres and --hthres-sigma).
	Added -lbitmap_gl to compile with the latest bitmap release.

Release R1-00n (Fri Aug 24  3:45:35 2007):
        This release should put an end on the monotonically increasing
        number of obstacles over time sent to the mapper. The tracks array
        is now preallocated at the beginning to a maximum size (--max-tracks
        cmdline option, default=50) and the size never changes.
        Of course, a lot of other bad things may still happen ... ;-)

        Added an option (--min-disp, default=4) to control the minimum 
        allowed disparity (cut objects too far away). This should stop those
        giant big amorphous obstacles to appear at all.

        Added option to send the alice MapElement (previously was done by
        default, but this may be confusing, it's off by default).

	Renamed main executable from blobstereo to stereo-obs-perceptor (finally!).
	To be backward compatible, a link blobstereo -> stereo-obs-perceptor
	is created in the i486-gentoo-linux directory, so process-control
        *should* be happy enough.

	Added configuration files support, dependent on the sensor-id. The
        two .cfg files provided for medium and long range contain the
        correct options for each camera pair, without long command lines.
        Added shortcut options --long and --medium to select medium or long
        range.
        Properly catch exceptions thrown in the BlboStereo constructor
        (no more core dumps for a bad cmdline option).

Release R1-00m (Wed Aug 22 18:04:12 2007):
	Reverted Sam's changes (sorry :-P) and implemented a better fix (I
	hope): merging tracks which are too close to each other. Now the
	number of tracks is well below what it used to be, but this fix may
	slow down the perceptor (or make it faster, since we have less
	tracks...). Still, compiling with optimizations should be fast
	enough though.

Release R1-00l (Wed Aug 22  6:25:46 2007):
	temporary fix for a bug which would cause number of tracks to grow
	without bound due to multiple associations of overlapping objects.

Release R1-00k (Mon Aug 20 21:33:19 2007):
	Trying to reduce the number of messages sent to the mapper. Not
	sending messages for tracks which weren't updated with new measures
	anymore. Fixed a bug that was sending clear messages for tracks
	which were never sent to the mapper at all, because their
	confidence level was too low.
        Fixed some bugs (bug #3392, cotk deadlock), removed old code
        (Image class), introduced new unkonwn bugs to be found ...

Release R1-00j (Tue Jul 24 20:53:13 2007):
        Added a basic but working association and tracking algorithm. It correctly
        associates new measures with the previous ones, can associate multiple
        measures with one track, and multiple tracks with one measure (is this good?).
        IMPORTANT: This release depends on the bitmap module (the bitmap module was
        actually a branch of some code originated here).
        NOTE: As tracking seems to work well (at least, better than without), it's enabled
        *by default*, to disable use --disable-tracking.
        NOTE2: As, AFAIK, nobody is using confidence values at the moment, there's a
        confidence threshold below which an obstacle won't be sent to the mapper at all.
        The default is 0.8, to change it, use --conf-thres=XX.
        There is a bug related to cotk, stderr redirect and printing a lot of debugging
        messages, see bug #3392. The current workaround is not to print to many
        debugging messages (they've been commented out for the release).
        

Release R1-00i (Fri Jul 20  3:59:24 2007):
	Added commandline options for most of the detector parameters, for
	easier tuning.

Release R1-00h (Thu Jul 19 17:54:47 2007):
        Fixed compilation error.
        Also, this release's link module is linked against the latest
        version of map and interfaces, so it shouldn't make mapper and
        other modules segfault.

Release R1-00g (Fri Jun 15 16:46:00 2007):
        - Better FLTK-OpenGL display.
        - Implemented small obstacle removal.
        - Not working (yet): first implementation of a basic
        association and filtering algorithm, needs debugging
        and tuning. It's disabled (commented out) in the release.
        - Added uncertainty to the generated obstacles. It looks
        like it's buggy, but nobody is using it for now, though.

Release R1-00f (Fri May 25 12:07:51 2007):
	Changed the OpenGL Display, moved from GLUT to a much nicer FLTK GUI.
	The detection algorithm is more or less the same, I've been trying with
	some new code, but it's commented out because it doesn't work very well.

Release R1-00d (Sun Apr 29  0:19:06 2007):
	Now showing detected obstacles in the console display.
	Some code cleanup and refactoring (more needs to be done).
	Did some quick tests with alice and the medium range cameras:
	* The perceptor can see obstacles as close as approx 5 meters
	  (~9.6m from the rear axle).
	* There seems to be an overestimation error of about 50-60cm,
	  and this could be a bug in the code (need to investigate).
	* The feeder and the perceptor are running at just 2Hz, with the
	  perceptor using approx 15% of cpu time, and 85% the feeder.
	  This will improve when the new core2 cpus arrive, but needs
	  some work on the software side too.

Release R1-00c (Thu Apr 26 19:29:05 2007):
	Finally sending obstacles to the mapper! Showing the number of
	obstacles sent in the cotk display.
	Fixed a bug in the detector which generated wrong range values.
	The opengl display uses a lot of cpu time (80% in the lab), need
	to be fixed. The actual detection algorithm is a lot faster (use
	"-g no" to disable opengl).

Release R1-00b (Thu Apr 26  2:08:59 2007):
	This version uses a disparity-based algorithm to detect blobs, and plots
	them on the debug display window as 2D polygons.
	No message is sent to the mapper yet, and no tracking is done.

Release R1-00a (Wed Apr 18 20:53:12 2007):
	First version of the stereo obstacle detector ("blobstereo"). This
	is just a shell, no detection is performed.
	Compiles correctly, and creates a simple opengl view showing the left
	and right images.	

Release R1-00 (Wed Apr 18 18:35:29 2007):
	Created.
















