Thu May 17 22:42:38 2007	datamino (datamino)

	* version R1-00f
	BUGS:  
	New files: UT_polygon.cc test.m
	FILES: Bitmap.cc(23231), Bitmap.hh(23231)
	Changed some 'float' to 'cost_t' (which, at the moment, is
	typedef'd as a float). Added a clear() in the CostMap constructors
	(except the copy constructor!). Trying to fix some random segfaults
	that happens when showing multiple opencv windows with
	CostMap::showWindow(), but nope. It could be a bug in opencv (not
	sure).

	FILES: Bitmap.cc(23316), Bitmap.hh(23316), Polygon.cc(23316),
		Polygon.hh(23316), testpolygon.cc(23316)
	Moved saveToMatlabScript() functions into CostMap and Polygon
	classes (now they are methods). Added const version of operator[],
	getPixPtr() and getRow() to Bitmap and CostMap.

	FILES: Makefile.yam(23234), testpolygon.cc(23234)
	Added a new unit test UT_polygon, to test the Polygon class.
	Because of segfaults with opencv (still to figure out why) it
	generates matlab .m files as output, and a test.m matlab script is
	provided to look at the results.

	FILES: Makefile.yam(23646), Polygon.cc(23646)
	Added option to compile with optimization to the Makefile.yam. Use
	like in 'ymk all OPTIMIZE=1'.

	FILES: Polygon.cc(23233)
	Changed the modifed bresenham algorithm so that only pixels inside
	the polygon are drawn (as opposed to pixels close to the edge by
	less than 0.5). Pixel corresponding to the edges are always drawn,
	though. This was done, when using COMB_ADD and drawing polygons
	which share an edge, to avoid having the edge be drawn twice and
	summed up (this still happens with vertices).

	FILES: Polygon.cc(23235)
	cleanup

	FILES: Polygon.cc(23240)
	Removed debugging message

	FILES: Polygon.cc(23291)
	Reverting back some changes, better to have overlapping polygons
	when two share one edge, than to have disconnected "polygons"
	(which are not polygons anymore). Handling the last scanline as a
	special case to avold some other defects.

	FILES: Polygon.cc(23608), Polygon.hh(23608)
	Fixed the interpolation of the value within the polygon, now it
	should be shoother and more accurate (i.e. the gradient of the
	value should be constant within the polygon, except for numeric
	approximations).

	FILES: Polygon.cc(23610), Polygon.hh(23610), testpolygon.cc(23610)
	Code cleanup. Small API change, (almost) backward compatible:
	FILL_NONE is deprecated in favor of FILL_LINEAR. Just use
	FILL_LINEAR with A = B = 1 and C = 0, or call setLinear() to do the
	same. If you call setFillFunc(FILL_NONE) you'll get the above and a
	warning on cout.

	FILES: Polygon.cc(23638)
	- Fixed another bug in the way the polygons are filled.   Now using
	a linear cost (FILL_LINEAR) should actually give a constant  
	gradient value.   As a conseguence, drawing the corridor should not
	give that "diagonal line"   that could be seen with the previous
	version. - Added another small test in UT_polygon that draws a
	corridor-like map.

	FILES: Polygon.hh(23232)
	Changed some 'float' into 'cost_t' (which, at the moment, is a
	float). Added Polygon::setVertNum() to set the number of vertices.
	This could be done resizing the vecgors returned by getXy(),
	getYv(), getCostVect(), but this way is simpler.

	FILES: Polygon.hh(23250), testpolygon.cc(23250)
	Added functions to save a Polygon or a CostMap to a matlab .m
	script. Now saving the output of testpolygon to 'testpoly.m' (it's
	huge, beware!).

Fri May 11 23:21:14 2007	datamino (datamino)

	* version R1-00e
	BUGS:  
	New files: UT_costmap.cc
	FILES: Bitmap.cc(22796), Bitmap.hh(22796), Makefile.yam(22796),
		testpolygon.cc(22796)
	- Fixed some segfaults happening when the CostMap was created with
	the default   constructor (and no init() function called). - Added
	a unit test for the CostMap class (UT_costmap). - Added methods to
	get and set the resolution of the costmap. - Added a method to show
	the costmap in a window, using OpenCV's high gui.   Clicking on any
	point will print the coordinates and the value on cout. - Added
	CostMap::getWidthLoc() and getHeightLoc() to get width and height
	in   meters (local frame). - Added setPixelLoc(value, x, y) and
	getPixel(x, y) in addition to versions	 that take a point2.

	FILES: Bitmap.hh(22797)
	Moved #warnings to the .cc file.

	FILES: Bitmap.hh(22805)
	Added a doxygen comment for the CostMap class (otherwise doxygen
	won't generate documentation at all for that class by default).

	FILES: Makefile.yam(22804)
	Trying to make doxygen work, don't really know how to do this (maybe
	it is working, but where are the generated files?).

Wed May  9  1:02:29 2007	Nok Wongpiromsarn (nok)

	* version R1-00d
	BUGS:  
	New files: BitmapParams.hh
	FILES: Makefile.yam(22561)
	Added a structure for sending parameters to paint cost map

Tue May  8 19:32:30 2007	datamino (datamino)

	* version R1-00c
	BUGS:  
	FILES: Bitmap.cc(22533), testpolygon.cc(22533)
	Fixed bug in new init() function (segfault).

	FILES: Bitmap.cc(22535), Bitmap.hh(22535)
	Added a Bitmap::clear function to clear the bitmap with a specific
	value, or zero if called without a value. Now CostMap::init() (all
	variants) clear the map to zero (but Bitmap::init() doesn't).

	FILES: Bitmap.cc(22536), Bitmap.hh(22536)
	Fixed compilation errors (always try to compile before
	commiting!!!)

	FILES: testpolygon.cc(22537)
	Added some messages (minor change).

Tue May  8 18:00:59 2007	datamino (datamino)

	* version R1-00b
	BUGS:  
	FILES: Bitmap.cc(22524), Bitmap.hh(22524)
	Implemented a constructor and a init() function in CostMap that
	takes the coordinates of the center point in local frame and the
	resolution. Implemented CostMap::setPixelLoc() and
	CostMap::getPixelLoc() to get and set points using local frame
	coordinates.

	FILES: Polygon.cc(22525), Polygon.hh(22525), testpolygon.cc(22525)
	Implemented drawing using the specified filling and combining
	functions. Cleaned up the code a little.

Tue May  8 13:03:27 2007	datamino (datamino)

	* version R1-00a
	BUGS:  
	New files: Bitmap.cc Bitmap.hh Polygon.cc Polygon.hh testpolygon.cc
	FILES: Makefile.yam(22489)
	Moved files from interfaces to this new module.

Tue May  8 12:39:46 2007	datamino (datamino)

	* version R1-00
	Created bitmap module.


	ChangeLog entries from interfaces:
	
Tue May  8  1:49:36 2007	datamino (datamino)

	* version R4-01i
	BUGS:  
	New files: testpolygon.cc
	Deleted files: testcostmap.cc
	FILES: Bitmap.hh(22377), Polygon.cc(22377)
	Renamed set/getP0 and set/getP1 to set/getUpperLeft and
	set/getLowerRight (nobody was using this code anyway, so not a
	problem). Fixed some bugs in the drawing code.

	FILES: Bitmap.hh(22387)
	Implemented copy constructor and assignment operator for
	BitmapBase, Bitmap and CostMap.

	FILES: Bitmap.hh(22388)
	Added CostMap::init(width, height) (c++ is strange)

	FILES: Bitmap.hh(22389)
	foo

	FILES: Bitmap.hh(22427)
	In CostMap::init(width, height), set the upper left and lower right
	corners to some reasonable default.

	FILES: Bitmap.hh(22455), Polygon.cc(22455)
	Fixed bug in polygon drawing when the polygon is partly out of the
	top or bottom boundary.

	FILES: Bitmap.hh(22456)
	Inverted width and height in setPixel/getPixel

	FILES: Makefile.yam(22369), Polygon.cc(22369)
	Fixed makefile to correctly recompile testcostmap when needed.
	Added Bresenham's algorithm to polygon drawing functions.

	FILES: Makefile.yam(22411)
	Now the polygon::draw() is fixed, so use that instead of
	drawMapCoords(). Changed the makefile to link with -lframes.

	FILES: Makefile.yam(22460)
	Renamed testcostmap into testpolygon. Changed Makefile.yam
	accordingly.

	FILES: Polygon.cc(22412)
	Fixed draw() method. Removed old draw_polygon function. Removed
	some debugging messages.

	FILES: Polygon.cc(22428)
	Fixed bug where some polygons where drawn incorrectly, changed all
	computations in the bresenham's algorithm to use only integer math,
	as they were meant to be.

	FILES: Polygon.cc(22457)
	Cleaned up drawing code that seems to work well in all cases.

	FILES: Polygon.cc(22459)
	More cleanup and restyling of the drawing code.

	FILES: Polygon.hh(22408)
	Fixed: forgot to serialize the cost vector

Fri May  4 17:09:05 2007	datamino (datamino)

	* version R4-01h
	BUGS:  
	New files: Bitmap.cc Bitmap.hh Polygon.cc Polygon.hh testcostmap.cc
	FILES: Makefile.yam(22208)
	Added the Polygon class, to be sent from the tplanner to the
	dplanner to describe how to draw the cost map, and the Bitmap class
	(ans yes, another CostMap class ...) to actually hold the map,
	where polygons can be drawn. Only linearly filled polygons can be
	drawn at the moment, and they are drawn one on top of the other
	(replacing values).






