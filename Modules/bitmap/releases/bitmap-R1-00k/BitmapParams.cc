#include <algorithm>

#include "BitmapParams.hh"

void BitmapParams::getMinMaxCost(cost_t* pMinCost, cost_t* pMaxCost) const 
{
    cost_t minCost =  numeric_limits<cost_t>::max();
    cost_t maxCost = -numeric_limits<cost_t>::max();

    for (unsigned int i = 0; i < polygons.size(); i++)
    {
        cost_t minVal =  numeric_limits<cost_t>::max();
        cost_t maxVal = -numeric_limits<cost_t>::max();
        const Polygon& p = polygons[i];
        const vector<cost_t>& val = p.getCostVec();

        for (unsigned int j = 0; j < p.getVertNum(); j++)
        {
            minVal = min(minVal, val[j]);
            maxVal = max(minVal, val[j]);
        }
        bool useDefault = false;

        cost_t minCostPoly;
        cost_t maxCostPoly;

        switch (p.getFillFunc()) {
        case FILL_NONE:
        case FILL_LINEAR:
        case FILL_EXP:
        case FILL_ATAN:
            // simple cases, these functions are monotonic
            useDefault = true;
            break;

        case FILL_SIN:
        case FILL_COS:
            {
                // we just take the worst case, max = 1, min = -1
                cost_t c1 = p.getC() + 1 * p.getB();
                cost_t c2 = p.getC() - 1 * p.getB();
                minCostPoly = min(c1, c2);
                maxCostPoly = max(c1, c2);
                break;
            }

        case FILL_EXP2:
        case FILL_SQUARE:
            // these are monotonic when both value are positive or negative
            // otherwise, they have a max or min in zero
            {
                if(minVal * maxVal >= 0) {
                    useDefault = true;
                } else {
                    cost_t c = p.getCost(0);
                    minCostPoly = min(c, min(p.getCost(minVal), p.getCost(maxVal)));
                    maxCostPoly = max(c, max(p.getCost(minVal), p.getCost(maxVal)));
                }
                break;
            }
 

        case FILL_TAN:
            {
                if(minVal * maxVal <= 0) {
                    // bad, theoretically, max = inf and min = -inf!!!
                    cerr << __FILE__ << ":" << __LINE__
                         << ": WARNING: bad polygon, cost goes to +inf or -inf" << endl;
                    minCostPoly = 1e13;
                    maxCostPoly = 1e-13;
                } else {
                    useDefault = true;
                }
                break;
            }

        default:
            useDefault=true;
            break;
        }

        if (useDefault) {
            minCostPoly = min(p.getCost(minVal), p.getCost(maxVal));
            maxCostPoly = max(p.getCost(minVal), p.getCost(maxVal));
        }

        minCost = min(minCostPoly, minCost);
        maxCost = max(maxCostPoly, maxCost);
    }
    if (pMinCost != NULL)
        *pMinCost = min(baseVal, minCost);
    if (pMaxCost != NULL)
        *pMaxCost = max(baseVal, maxCost);
}

