              Release Notes for "bitmap" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "bitmap" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "bitmap" module can be found in
the ChangeLog file.

Release R1-00e (Fri May 11 23:21:20 2007):
	- Fixed some segfaults happening when the CostMap was created with
	the default   constructor (and no init() function called). - Added
	a unit test for the CostMap class (UT_costmap). - Added methods to
	get and set the resolution of the costmap. - Added a method to show
	the costmap in a window, using OpenCV's high gui.   Clicking on any
	point will print the coordinates and the value on cout. - Added
	CostMap::getWidthLoc() and getHeightLoc() to get width and height
	in   meters (local frame). - Added setPixelLoc(value, x, y) and
	getPixel(x, y) in addition to versions	 that take a point2.

Release R1-00d (Wed May  9  1:02:33 2007):
	Added a structure for sending parameters to paint cost map

Release R1-00c (Tue May  8 19:32:36 2007):
	Fixed a segfault in the new CostMap::init() method.
	Now CostMap::init() (all of them) clear the map to zero.

Release R1-00b (Tue May  8 18:01:07 2007):
	- Implemented drawing using the specified filling and combining
	functions. Cleaned up the code a little.
	- Implemented a constructor and a init() function in CostMap that
	takes the coordinates of the center point in local frame and the
	resolution.
	- Implemented CostMap::setPixelLoc() and
	CostMap::getPixelLoc() to get and set points using local frame
	coordinates.

Release R1-00a (Tue May  8 13:03:30 2007):
	Moved files from interfaces to this new module.


Release R1-00 (Tue May  8 12:39:46 2007):
	Created.


Other Release notes from the interfaces module:

	R4-01i (Tue May  8  1:49:43 2007):
		Changed the drawing code, now uses Bresenham's algorithm to enumerate
		pixels on the edges of polygons, and fixed a bunch of bugs on the
		Bitmap (CostMap) and Polygon classes (see ChangeLog).

	R4-01h (Fri May  4 17:09:09 2007):
		Added the Polygon class, to be sent from the tplanner to the
		dplanner to describe how to draw the cost map, and the Bitmap class
		(ans yes, another CostMap class ...) to actually hold the map,
		where polygons can be drawn. Only linearly filled polygons can be
		drawn at the moment, and they are drawn one on top of the other
		(replacing values).





