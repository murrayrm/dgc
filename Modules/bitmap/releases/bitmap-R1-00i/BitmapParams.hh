#ifndef BITMAPPARAMS_HH9087653456789623
#define BITMAPPARAMS_HH9087653456789623

#include <frames/point2_uncertain.hh>
#include "Polygon.hh"

using namespace bitmap;

struct BitmapParams
{
  float centerX;
  float centerY;
  float resX;
  float resY;
  int width;
  int height;
  float baseVal;
  float outOfBounds;
  vector<Polygon> polygons;

  BitmapParams()
  {
    centerX = 0;
    centerY = 0;
    resX = 0.1;
    resY = 0.1;
    width = 800;
    height = 800;
    baseVal = 100;
    outOfBounds = 200;
  }

  /**
   * Returns the minimum costs among all the vertices in all the polygons, and
   * the base value (baseVal).
   * This is a fast way to approximately calculate the minimum cost in the map, instead
   * of drawing the map and then searching in all the cells, but it doesn't always
   * return the true minimum, when using non-linear costs (e.g. FILL_SQUARE, value
   * is -10 in vertex V1 and 10 in V2, both evaulate to 10^2=100, so this function
   * returns 100, even though while interpolating from -10 to 10 the minimum value
   * is 0^2 = 0). Useful to rescale the map for display purposes.
   */
  cost_t getMinCost() const;

  /**
   * Returns the maximum costs among all the vertices in all the polygons, and
   * the base value (baseVal).
   * This is a fast way to approximately calculate the maximum cost in the map, instead
   * of drawing the map and then searching in all the cells, but it doesn't always
   * return the true maximum, when using non-linear costs (e.g. FILL_EXP2, A=-0.5 gaussian
   * like, value is -10 in vertex V1 and 10 in V2, both evaulate to exp(-0.5*10^2)=exp(-50)
   * which is close to zero, even though while interpolating from -10 to 10 the maximum
   * cost is exp(-0.5*0^2) = exp(0) = 1). Useful to rescale the map for display purposes.
   */
  cost_t getMaxCost() const;

  template <class Archive>
  void serialize(Archive &ar,const unsigned int version)
  {
    ar & centerX;
    ar & centerY;
    ar & resX;
    ar & resY;
    ar & width;
    ar & height;
    ar & baseVal;
    ar & polygons;
  }
};

#endif
