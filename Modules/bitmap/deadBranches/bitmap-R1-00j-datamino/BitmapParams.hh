#ifndef BITMAPPARAMS_HH9087653456789623
#define BITMAPPARAMS_HH9087653456789623

#include <frames/point2_uncertain.hh>
#include "Polygon.hh"

using namespace bitmap;

struct BitmapParams
{
  float centerX;
  float centerY;
  float resX;
  float resY;
  int width;
  int height;
  float baseVal;
  float outOfBounds;
  vector<Polygon> polygons;

  BitmapParams()
  {
    centerX = 0;
    centerY = 0;
    resX = 0.1;
    resY = 0.1;
    width = 800;
    height = 800;
    baseVal = 100;
    outOfBounds = 200;
  }

  /**
   * Returns the minimum costs among all the vertices in all the polygons, and
   * the base value (baseVal).
   * This is a fast way to calculate the minimum cost in the map, instead
   * of drawing the map and then searching in all the cells. It's not 100% accurate, but
   * should at least provide a worst case range (i.e. all the values in the map should be
   * in the returned range).
   */
  cost_t getMinCost() const
  {
      cost_t min;
      getMinMaxCost(&min, NULL);
      return min;
  }

  /**
   * Returns the maximum costs among all the vertices in all the polygons, and
   * the base value (baseVal).
   * This is a fast way to calculate the maximum cost in the map, instead
   * of drawing the map and then searching in all the cells. It's not 100% accurate, but
   * should at least provide a worst case range (i.e. all the values in the map should be
   * in the returned range).
   */
  cost_t getMaxCost() const
  {
      cost_t max;
      getMinMaxCost(NULL, &max);
      return max;
  }

  /**
   * Returns the minimun and maximum costs among all the vertices in all the polygons,
   * and the base value (baseVal).
   * This is a fast way to calculate the maximum and minimun cost in the map, instead
   * of drawing the map and then searching in all the cells. It's not 100% accurate, but
   * should at least provide a worst case range (i.e. all the values in the map should be
   * in the returned range).
   * @param minCost Output parameter to return the minimun cost. Can be NULL.
   * @param maxCost Output parameter to return the maximum cost. Can be NULL.
   */
  void getMinMaxCost(cost_t* minCost, cost_t* maxCost) const;

  template <class Archive>
  void serialize(Archive &ar,const unsigned int version)
  {
    ar & centerX;
    ar & centerY;
    ar & resX;
    ar & resY;
    ar & width;
    ar & height;
    ar & baseVal;
    ar & polygons;
  }
};

#endif
