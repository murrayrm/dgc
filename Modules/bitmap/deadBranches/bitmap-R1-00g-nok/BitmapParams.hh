#ifndef BITMAPPARAMS_HH9087653456789623
#define BITMAPPARAMS_HH9087653456789623

#include <frames/point2_uncertain.hh>
#include "Polygon.hh"

using namespace bitmap;

struct BitmapParams
{
  float centerX;
  float centerY;
  float resX;
  float resY;
  int width;
  int height;
  float baseVal;
  float outOfBounds;
  vector<Polygon> polygons;

  BitmapParams()
  {
    centerX = 0;
    centerY = 0;
    resX = 0.1;
    resY = 0.1;
    width = 800;
    height = 800;
    baseVal = 100;
    outOfBounds = 200;
  }

  template <class Archive>
  void serialize(Archive &ar,const unsigned int version)
  {
    ar & centerX;
    ar & centerY;
    ar & resX;
    ar & resY;
    ar & width;
    ar & height;
    ar & baseVal;
    ar & polygons;
  }
};

#endif
