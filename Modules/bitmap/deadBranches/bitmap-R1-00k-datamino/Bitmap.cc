#include <iostream>
#include <fstream>
#include <sstream>
#include <highgui.h> // opencv simple gui

#include "Bitmap.hh"

// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

//#warning "*** TODO:"
//#warning "*** - getRows()? getWidthLoc()? API changes???"

namespace bitmap
{


    void BitmapBase::releaseImage()
    {
        if (img != NULL) {
            if (imgData == NULL) {
                // data was allocated by cvCreateImage not by us, release it
                cvReleaseImage(&img);
            } else {
                cvReleaseImageHeader(&img);
                }
            img = NULL;
        }
    }
   
    void BitmapBase::releaseImageData()
    {
        if (imgData != NULL) {
            delete[] imgData;
            imgData = NULL;
            dataSize = 0;
        }
    }

    void BitmapBase::allocateData()
    {
        unsigned long newSize = getWidth() * getHeight() * bytesPerPix;
        if (newSize > dataSize) {
            delete[] imgData;
            imgData = new uint8_t[newSize];
            dataSize = newSize;
        }
        cvSetData(img, imgData, getWidth() * bytesPerPix);
    }

    void BitmapBase::init(int width, int height, int depth, int channels)
    {
        releaseImage();
        img = cvCreateImageHeader(cvSize(width, height), depth, channels);
        bytesPerPix = depthToBpc(depth) * channels;
        allocateData();
    }

    void BitmapBase::init(IplImage* img)
    {
        releaseImage();
        releaseImageData();
        this->img = img;
        bytesPerPix = depthToBpc(img->depth) * img->nChannels;
    }

    BitmapBase& BitmapBase::operator=(const BitmapBase& bmp)
    {
        if (img == NULL) {
            img = cvCreateImageHeader(cvSize(bmp.getWidth(), bmp.getHeight()),
                                      bmp.img->depth, bmp.img->nChannels);
        } else {
            *img = *bmp.img;
        }
        bytesPerPix = bmp.bytesPerPix;
        allocateData();
        for (int r = 0; r < img->height; r++) {
            memcpy(getRow(r), bmp.getRow(r), getBytesPerRow());
        }
        return *this;
    }

    CostMap::~CostMap()
    {
        if (m_dbgImg != NULL /* !m_dbgWinName.empty() */)
        {
            cvDestroyWindow(m_dbgWinName.c_str());
            cvReleaseImage(&m_dbgImg);
        }
    }

    void CostMap::init(int width, int height, cost_t baseVal)
    {
        base_t::init(width, height);
        setUpperLeft(0, 0);
        setLowerRight(width, height);
        clear(baseVal);
    }
    
    void CostMap::init(int width, int height, const point2& upLeft, const point2& lowRight, cost_t baseVal)
    {
        base_t::init(width, height);
        setUpperLeft(upLeft);
        setLowerRight(lowRight);
        clear(baseVal);
    }
    
    void CostMap::init(IplImage* img, const point2& upLeft, const point2& lowRight, cost_t baseVal)
    {
        base_t::init(img);
        setUpperLeft(upLeft);
        setLowerRight(lowRight);
        clear(baseVal);
    }


    void CostMap::init(int width, int height, float resX, float resY, const point2& center, cost_t baseVal)
    {
        point2 upLeft;
        point2 lowRight;
        if (width & 1 == 0) { // width is even
            upLeft.x = center.x - (width/2 - 1) * resX;
            lowRight.x = center.x + (width/2 - 1) * resX;
        } else {
            upLeft.x = center.x - width/2 * resX;
            lowRight.x = center.x + width/2 * resX;
        }
        if (height & 1 == 0) { // height is even
            upLeft.y = center.y - (height/2 - 1) * resY;
            lowRight.y = center.y + (height/2 - 1) * resY;
        } else {
            upLeft.y = center.y - height/2 * resY;
            lowRight.y = center.y + height/2 * resY;
        }
        //MSG("Upper Left: (" << upLeft.x << ", " << upLeft.y << ")");
        //MSG("Lower Right: (" << lowRight.x << ", " << lowRight.y << ")");
        init(width, height, upLeft, lowRight, baseVal);
    }

    /// @return the cell resolution in the x direction, that is, the width of a cell in meters.
    float CostMap::getResX() const
    {
        int w = getWidth();
        if (w == 0) return 0;
        return (m_p1.x - m_p0.x) / w;
    }
    /// @return the cell resolution in the y direction, that is, the height of a cell in meters.
    float CostMap::getResY() const
    {
        int h = getHeight();
        if (h == 0) return 0;
        return (m_p1.y - m_p0.y) / h;
    }

    /// Sets the resolution in the x direction (width of cell in meters).
    /// This will scale the map around the center column
    void CostMap::setResX(float rx)
    {
        float widthLoc = m_p1.x - m_p0.x;
        m_p1.x = (m_p1.x - widthLoc/2) + getWidth() * rx / 2;
        m_p0.x = (m_p0.x + widthLoc/2) - getWidth() * rx / 2;
    }
    
    /// Sets the resolution in the y direction (width of cell in meters).
    /// This will scale the map around the center row.
    void CostMap::setResY(float ry)
    {
        float heightLoc = m_p1.y - m_p0.y;
        m_p1.y = (m_p1.y - heightLoc/2) + getHeight() * ry / 2;
        m_p0.y = (m_p0.y + heightLoc/2) - getHeight() * ry / 2;
    }

    void CostMap::mouseCallback(int event, int x, int y, int flags, void* param)
    {
        CostMap* self = (CostMap*) param;
        if (event == CV_EVENT_LBUTTONDOWN)
        {
            float xLoc, yLoc;
            self->toLocalFrame(y, x, &xLoc, &yLoc);
            cout << "(r, c) = (" << y << ", " << x << "), ";
            cout << "x = " << xLoc << ", y = " << yLoc << ", cost = " << self->getPixel(y, x) << endl;
        }
    }

    void CostMap::showWindow(string title)
    {
        if (m_dbgImg != NULL /*!m_dbgWinName.empty()*/ && title != m_dbgWinName)
        {
            cvDestroyWindow(m_dbgWinName.c_str());
            cvReleaseImage(&m_dbgImg);
            m_dbgWinName = title;
        }

        IplImage* img = cvCloneImage(getImage());
        double minVal, maxVal;
        cvMinMaxLoc(img, &minVal, &maxVal);
        cvSubS(img, cvRealScalar(minVal), img);
        if (maxVal != minVal)
            cvScale(img, img, 1/(maxVal - minVal));
        
        cvNamedWindow( title.c_str(), CV_WINDOW_AUTOSIZE );
        cvShowImage( title.c_str(), img);
        cvSetMouseCallback( title.c_str(), mouseCallback, this);
        cvWaitKey(10);
        m_dbgImg = img;
    }

    void CostMap::saveToMatlabScript(ostream& out, string name) const
    {
        out << name << ".map = [" << endl;
        for (int r = 0; r < getHeight(); r++) {
            for (int c = 0; c < getWidth(); c++) {
                out << (*this)[r][c] << " ";
            }
            out << ";" << endl;
        }
        out << "];" << endl;
        out << name << ".upLeft = [" << m_p0.x << ", " << m_p0.y << "];" << endl;
        out << name << ".lowRight = [" << m_p1.x << ", " << m_p1.y << "];" << endl;
    }

    void CostMap::saveToMatlabScript(const char* filename, string name) const
    {
        ofstream of(filename);
        saveToMatlabScript(of, name);
    }

}
