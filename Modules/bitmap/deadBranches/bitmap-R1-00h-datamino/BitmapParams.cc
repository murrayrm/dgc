#include <algorithm>

#include "BitmapParams.hh"

cost_t BitmapParams::getMinCost() const
{
    cost_t minCost = baseVal;
    for (unsigned int i = 0; i < polygons.size(); i++)
    {
        const Polygon& p = polygons[i];
        for (unsigned int j = 0; j < p.getVertNum(); j++)
        {
            minCost = min(minCost, p.getCostAt(j));
        }
    }
    return minCost;
}

cost_t BitmapParams::getMaxCost() const
{
    cost_t maxCost = baseVal;
    for (unsigned int i = 0; i < polygons.size(); i++)
    {
        const Polygon& p = polygons[i];
        for (unsigned int j = 0; j < p.getVertNum(); j++)
        {
            maxCost = max(maxCost, p.getCostAt(j));
        }
    }
    return maxCost;
}
