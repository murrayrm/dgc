#include <iostream>
#include <fstream>
#include <sstream>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Bitmap.hh"

// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

//#warning "*** TODO:"
//#warning "*** - getRows()? getWidthLoc()? API changes???"

namespace bitmap
{

    /**
     * Creates a texture without changing the actual values of the original
     * pixels (no mipmapping, rescaling, etc).
     * If the size is not a power of two, a bigger texture is created, filled with zeros,
     * and the original data is copied in the upper left corner.
     * @param inWidth Width of the original image.
     * @param inWidth Height of the original image.
     * @param bpp Bytes per pixel of the image.
     * @param bpr Bytes per row of the image (>= bpp * width)
     * @param format The texture format, as required by glTexImage2D.
     * @param type The input data type, as required by glTexImage2D.
     * @param dataIn The raw image data.
     * @param[out] outWidth The resulting width of the texture.
     * @param[out] outHeight The resulting height of the texture.
     * @return The new size of the texture.
     */
    void buildPlainTexture(int inWidth, int inHeight, int bpp, int bpr,
                           int format, int type, uint8_t* dataIn,
                           int* outWidth, int* outHeight)
    {
        // let's do it manually, without mipmapping, our own way
        // find minimum power of 2 bigger than width and height
        int pw, ph;
        for (pw = 1; pw < 32; pw++) {
            if ((1L << pw) >= inWidth)
                break;
        }
        for (ph = 1; ph < 32; ph++) {
            if ((1L << ph) >= inHeight)
                break;
        }
        int width = 1 << pw;
        int height = 1 << ph;
        if (width*bpp != bpr || height != inHeight) {
            // use width+1 and height+1 because otherwise the nvidia opengl
            // goes beyond the buffer ... nvidia bug or I didn't understand
            // the specs?
            uint8_t* data = new uint8_t[(width+1) * (height+1) * bpp];
            // do not scale, copy image in the upper left part of the new one
            //memset(data, 0, width * height * bpp);
            uint8_t* rowOut = data;
            uint8_t* rowIn = dataIn;
            for (int r = 0; r < inHeight; r++) {
                memcpy(rowOut, rowIn, inWidth * bpp);
                rowOut += width * bpp;
                rowIn += bpr;
            }
             glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, format,
                         type, data);
            delete[] data;
        } else {
            glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, format,
                         type, dataIn);
        }
        if (outWidth)
            *outWidth = width;
        if (outHeight)
            *outHeight = height;
    } 

    CvSize BitmapBase::toGlTexture(const IplImage* img, int txtId, int glFormat,
                                    bool mipmap, bool setParams)
    {
        glBindTexture(GL_TEXTURE_2D, txtId);
            
        if (setParams) {
            //glPixelStoref(GL_UNPACK_ALIGNMENT, 1);
                
            // do not interpolate when magnifying
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                
            // don't repeat the texture
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);        
        }

        static struct {
            int cv;
            int openGL;
        } cvTypes[] = {
            { IPL_DEPTH_8S, GL_BYTE },
            { IPL_DEPTH_8U, GL_UNSIGNED_BYTE },
            { IPL_DEPTH_16S, GL_SHORT },
            { IPL_DEPTH_16U, GL_UNSIGNED_SHORT },
            { IPL_DEPTH_32S, GL_INT },
            { IPL_DEPTH_32F, GL_FLOAT },
            { IPL_DEPTH_64F, GL_DOUBLE } // not sure if this works
        };
        int numTypes = sizeof(cvTypes)/sizeof(cvTypes[0]);

        int type = -1;
        for (int i = 0; i < numTypes; i++) {
            if (cvTypes[i].cv == img->depth) {
                type = i;
                break;
            }
        }
        if (type == -1) {
            ERRMSG("Bitmap has an unknown depth (format) " << img->depth << "!!! ");
            return cvSize(1, 1);
        }

        int format = glFormat;
        if (format < 0) {
            switch(img->nChannels) {
            case 1:
                format = GL_LUMINANCE; break;
            case 2:
                format = GL_LUMINANCE_ALPHA; break;
            case 3:
                format = GL_RGB; break;
            case 4:
                format = GL_RGBA; break;
            default:
                ERRMSG("Too many channels (" << img->nChannels << ") (or too few?)!!!");
                format = GL_LUMINANCE;
            }
        }

        if (mipmap) {
            gluBuild2DMipmaps(GL_TEXTURE_2D, 4, img->width, img->height, format,
                              cvTypes[type].openGL, img->imageData);
            return cvSize(img->width, img->height);
        } else {
            CvSize sz;
            int bpp = depthToBpc(img->depth) * img->nChannels;
            buildPlainTexture(img->width, img->height, bpp, img->widthStep, format,
                              cvTypes[type].openGL, (uint8_t*) img->imageData,
                              &sz.width, &sz.height);
            return sz;
        }

    }

}
