#ifndef __BITMAP_HH__
#define __BITMAP_HH__

#include <cassert>
#include <stdint.h>
#include <cv.h>

#include <frames/point2.hh>
#include <frames/point2_uncertain.hh>

namespace bitmap
{

    /**
     * A class holding a generic bitmap (an image, a cost map, a matrix), where polygons can be drawn on
     * At the moment, it's a C++ wrapper around OpenCV image structure, not because I use OpenCv here,
     * but it has a lot of functions that could be useful in the future.
     * This class can hold different formats, and multiple channels, even though we don't use them
     * (cut'n'pasted from stereo-obs-perceptor, which uses them).
     */
    class BitmapBase
    {
    protected:
        IplImage* img;
        int bytesPerPix;

    public:

	BitmapBase(int w=0, int h=0, int depth=IPL_DEPTH_8U, int channels=1)
            : img(NULL), bytesPerPix(0)
        {
            init(w, h, depth, channels);
        }

        BitmapBase(IplImage* im)
            : img(im)
        {
            bytesPerPix = depthToBpc(im->depth) * im->nChannels;
        }

        /** @brief Copy constructor */
        BitmapBase(const BitmapBase& img)
        {
            *this = img;
        }

        /** @brief Standard assignment operator */
        BitmapBase& operator=(const BitmapBase& bmp)
        {
            img = cvCloneImage(bmp.img);
            bytesPerPix = bmp.bytesPerPix;
            return *this;
        }

        ~BitmapBase() {
            cvReleaseImage(&img);
        }

        void init(int width, int height, int depth, int channels)
        {
            if (img != NULL) {
                cvReleaseImage(&img);
                img = NULL;
            }
            if (width > 0 && height > 0) {
                img = cvCreateImage(cvSize(width, height), depth, channels);
            }
            bytesPerPix = depthToBpc(depth) * channels;
        }

        void init(IplImage* img)
        {
            if (this->img != NULL) {
                cvReleaseImage(&this->img);
            }
            this->img = img;
            bytesPerPix = depthToBpc(img->depth) * img->nChannels;            
        }

        /// @return The width of the bitmap
        int getWidth() const { return img->width; }

        /// @return The height of the bitmap
        int getHeight() const { return img->height; }

        /// @return The distance in bytes from a pixel in a row and
        /// the same pixel (same column) on the next row.
        /// Bigger or equal to getWidth()*getBytesPerPixel().
        int getBytesPerRow() const { return img->widthStep; }

        /// @return The number of bytes per pixel.
        int getBytesPerPixel() const {
            return bytesPerPix;
        }

        /// Direct access to the data.
        uint8_t* getRawData() const { return (uint8_t*) img->imageData; }

        /// Replaces the internal data pointer with the one specified,
        /// does NOT copy the data.
        /// doubt: not sure about the behavior of cvReleaseImage(), when the
        /// data was set with cvSetData(): will it free the user data or not?
        void setRawData(uint8_t* data, int bytesPerRow = 0)
        {
            if (bytesPerRow == 0) {
                bytesPerRow = getWidth()*getBytesPerPixel();
            }
            cvSetData(img, data, bytesPerRow);
        }

        /// Get the underlying IplImage, to be used with opencv functions
        IplImage* getImage() { return img; }

        /// Get the pointer to the first pixel of the specified row.
        uint8_t* getRow(int row)
        {
            return (uint8_t*) img->imageData + row * img->widthStep;
        }

        /// Same as getRow(), use as in myBitmap[row][col].
        uint8_t* operator[](int row) {
            return getRow(row);
        }
	
        /// Same as the static function toGlTexture(IplImage*, int, int, bool, bool), but
        /// uses 'this' as the bitmap to convert.
        CvSize toGlTexture(int txtId, int glFormat = -1, bool mipmap = false,
                         bool setParams = true)
        {
            return toGlTexture(img, txtId, glFormat, mipmap, setParams);
        }

        /// Creates (or replaces) an OpenGL texture with the data in 'img'.
        /// Returns the size of the texture (which must be a power of two).
        /// The actual data is not resized, it's just copied in the upper left
        /// part of the texture, and the rest is filled with color (0, 0, 0, 0) 
        /// (transparent black).
        /// Use the returned new size to transform texture coordinates accordingly.
        /// @param img The image to be converted.
        /// @param txtId OpenGL texture Id.
        /// @param glFormat The format of the data used for the texture. If set to -1, it's selected
        /// automatically. It should be ont of GL_LUMINANCE, GL_RB, etc.
        static CvSize toGlTexture(IplImage* img, int txtId, int glFormat = -1, bool mipmap = false,
                                  bool setParams = true);

        /// Convert OpenCV depth into the number of bytes per channel.
        static int depthToBpc(int depth)
        {
            switch(depth) {
            case IPL_DEPTH_8U: // unsigned 8-bit integers
            case IPL_DEPTH_8S: // signed 8-bit integers
                return  1;
            case IPL_DEPTH_16U: // unsigned 16-bit integers
            case IPL_DEPTH_16S: // signed 16-bit integers
                return 2;
            case IPL_DEPTH_32S: // signed 32-bit integers
            case IPL_DEPTH_32F: // single precision floating-point numbers
                return 4;
            case IPL_DEPTH_64F: // double precision floating-point numbers
                return 8;
            default:
                return 1; // ???
            }
        }

    };

    /* Some useful helper for generic programming (templates), mapping C++ types
       to OpenCV depth constants. */

    // The generic definition
    template <class type> struct IplType {
        static int depth() { return 0; }; // unknown
    };
    // specializations for useful types
    template<> struct IplType<int8_t> {
        static int depth() { return IPL_DEPTH_8S; }
    };
    template<> struct IplType<uint8_t> {
        static int depth() { return IPL_DEPTH_8U; }
    };
    template<> struct IplType<int16_t> {
        static int depth() { return IPL_DEPTH_16S; }
    };
    template<> struct IplType<uint16_t> {
        static int depth() { return IPL_DEPTH_16U; }
    };
    template<> struct IplType<int32_t> {
        static int depth() { return IPL_DEPTH_32S; }
    };
    template<> struct IplType<uint32_t> {
        static int depth() { return IPL_DEPTH_32S; } /* there is no  IPL_DEPTH_32U */
    };
    template<> struct IplType<float> {
        static int depth() { return IPL_DEPTH_32F; }
    };
    template<> struct IplType<double> {
        static int depth() { return IPL_DEPTH_64F; }
    };


    /**
     * Type safe template version of BitmapBase.
     */
    template <class type, int channels = 1>
    class Bitmap : public BitmapBase
    {
        type m_outOfBounds; // value returned when getPixel is called with out-of-bounds coordintes

    public:
        Bitmap(int width = 0, int height = 0, type outOfBounds = -1)
            : BitmapBase(width, height, IplType<type>::depth(), channels),
              m_outOfBounds(outOfBounds)
        { }

        Bitmap(IplImage* im, type outOfBounds = -1)
            : BitmapBase(im),
              m_outOfBounds(outOfBounds)
        {
            assert(img->depth == IplType<type>::depth());
            assert(img->nChannels == channels);
        }

        Bitmap(const Bitmap& bmp)
            : BitmapBase(bmp), m_outOfBounds(bmp.m_outOfBounds)
        { }

        Bitmap& operator= (const Bitmap& bmp)
        {
            *(BitmapBase*)this = (BitmapBase) bmp;
            m_outOfBounds = bmp.m_outOfBounds;
            return *this;
        }

        void init(int width, int height)
        {
            BitmapBase::init(width, height, IplType<type>::depth(), channels);
        }

        void init(IplImage* img)
        {
            assert(img->depth == IplType<type>::depth());
            assert(img->nChannels == channels);
            BitmapBase::init(img);
        }


        type* getData() const { return (type*) getRawData(); }

        void setData(type* data, int bytesPerRow = 0) {
            setRawData((uint8_t*) data, bytesPerRow);
        }

        type* getRow(int row)
        {
            return (type*)BitmapBase::getRow(row);
        }

        type* getPixPtr(int row, int col) {
            return Bitmap::getRow(row) + channels * col;
        }

        type getOutOfBounds() const
        {
            return m_outOfBounds;
        }

        void setOutOfBounds(type oob)
        {
            m_outOfBounds = oob;
        }

        /* Operator[] requires a little work to be really useful */

        // hopefully, an optimizing compiler (gcc -O3) will traslate
        // the call to img[row][col][channel] to a single call to getPixPtr().

        class GetPixWrapper {
            Bitmap& self;
            int row;
        public:
            GetPixWrapper(Bitmap& img, int r)
                : self(img), row(r)
            { }
            type* operator[](int col) {
                return self.getPixPtr(row, col);
            }
            const type* operator[](int col) const {
                return self.getPixPtr(row, col);
            }
        };

        /// Use the operator[] to access to any value within the bitmap, specifying
        /// row, column and channel (if needed), like in:
        ///  value = img[row][col][chn];
        ///  img[row][col][chn] = value;
        /// For a 1-channel bitmap, use img[r][c][0] or *img[r][c], like in;
        ///  value = img[row][col][0];  OR   value = *img[row][col];
        ///  img[row][col][0] = value;  OR   *img[row][col] = value;

        GetPixWrapper operator[](int row) {
            return GetPixWrapper(*this, row);
        }

        /// This is the same as doing img[row][col][chn], but it checks if the coordinates
        /// are out of the boundaries, and in this case, it returns the outOfBounds value.
        type getPixel(int row, int col, int chn = 0)
        {
            if (row < 0 || col < 0 || row >= img->height || col >= img->width)
            {
                return m_outOfBounds;
            }
            return getPixPtr(row, col)[chn];
        }

        /// This is the same as doing img[row][col][chn] = value, but if the row and col are
        /// outside of the boundaries, it does nothing.
        void setPixel(type value, int row, int col, int chn = 0)
        {
            if (row >= 0 && col >= 0 && row < img->height && col < img->width)
            {
                getPixPtr(row, col)[chn] = value;
            }
        }
    };

    typedef float cost_t;

    class CostMap : public Bitmap<cost_t, 1>
    {
        // coordinates in local frame of the point at row=0 col=0
        point2 m_p0;
        // coordinates in local frame of the point at row=width col=height
        point2 m_p1;
    public:
        typedef Bitmap<cost_t, 1> base_t;

        CostMap(int width = 0, int height = 0, cost_t outOfBounds = -1)
            : base_t(width, height, outOfBounds)
        {
            m_p0.x = m_p0.y = 0;
            m_p1.x = width;
            m_p1.y = height;
        }

        CostMap(int width, int height, const point2& upLeft, const point2& lowRight,
                cost_t outOfBounds = -1)
            : base_t(width, height, outOfBounds),
              m_p0(upLeft), m_p1(lowRight)
        {
            m_p0.x = m_p0.y = 0;
            m_p1.x = width;
            m_p1.y = height;
        }

        CostMap(IplImage* im, cost_t outOfBounds = -1)
            : base_t(im, outOfBounds)
        {
            m_p0.x = m_p0.y = 0;
            m_p1.x = getWidth();
            m_p1.y = getHeight();
        }

        CostMap(const CostMap& cmap)
            : base_t(cmap), m_p0(cmap.m_p0), m_p1(cmap.m_p1)
        { }

        CostMap& operator= (const CostMap& cmap)
        {
            *(base_t*)this = (base_t) cmap;
            m_p0 = cmap.m_p0;
            m_p1 = cmap.m_p1;
            return *this;
        }


        /// This is a one-channel bitmap, so use a simpler operator[].
        /// Use it like in "map[row][col] = 1;" "v = map[row][col]";
        cost_t* operator[](int row)
        {
            return getRow(row);
        }

        void init(int width, int height)
        {
            base_t::init(width, height);
            setUpperLeft(0, 0);
            setLowerRight(width, height);
        }

        void init(int width, int height, const point2& upLeft, const point2& lowRight)
        {
            Bitmap<cost_t, 1>::init(width, height);
            setUpperLeft(upLeft);
            setLowerRight(lowRight);
        }

        void init(IplImage* img, const point2& upLeft, const point2& lowRight)
        {
            Bitmap<cost_t, 1>::init(img);
            setUpperLeft(upLeft);
            setLowerRight(lowRight);
        }

        /// Get the coordinates in local frame of the point at row 0 column 0.
        const point2& getUpperLeft() const { return m_p0; }
        /// Set the coordinates in local frame of the point at row 0 column 0.
        void setUpperLeft(const point2& p0) { m_p0 = p0; }
        void setUpperLeft(float x, float y) { m_p0.x = x; m_p0.y = y; }

        /// Get the coordinates in local frame of the point at
        /// row=getWidth() and column=getHeight() (which is out of the bitmap).
        const point2& getLowerRight() const { return m_p1; }
        /// Get the coordinates in local frame of the point at row=width, col=height.
        void setLowerRight(const point2& p1) { m_p1 = p1; }
        void setLowerRight(float x, float y) { m_p1.x = x; m_p1.y = y; }

        /// Convert coordinates from local frame to row and column.
        void toMapFrame(float x, float y, float* row, float* col)
        {
            *row = (y - m_p0.y) * getHeight() / (m_p1.y - m_p0.y);
            *col = (x - m_p0.x) * getWidth() / (m_p1.x - m_p0.x);
        }
        /// Convert coordinates from local frame to row (y) and column (y).
        point2 toMapFrame(const point2& p)
        {
            point2 ret;
            ret.y = (p.y - m_p0.y) * getHeight() / (m_p1.y - m_p0.y);
            ret.x = (p.x - m_p0.x) * getWidth() / (m_p1.x - m_p0.x);
            return ret;
        }

        /// Convert coordinates from row and column to local frame.
        void toLocalFrame(float row, float col, float* x, float* y)
        {
            *x = m_p0.x + col * (m_p1.x - m_p0.x) / getWidth();
            *y = m_p0.y + row * (m_p1.y - m_p0.y) / getHeight();
        }
        /// Convert coordinates from row (y) and column (y) to local frame.
        point2 toLocalFrame(const point2& p)
        {
            point2 ret;
            ret.x = m_p0.x + p.x * (m_p1.x - m_p0.x) / getWidth();
            ret.y = m_p0.y + p.y * (m_p1.y - m_p0.y) / getHeight();
            return ret;
        }
    };


}

#endif
