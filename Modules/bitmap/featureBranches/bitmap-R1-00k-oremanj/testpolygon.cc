#include <iostream>
#include <fstream>
#include <sstream>
#include <highgui.h> // opencv gui
#include <cstdlib>

#include "Bitmap.hh"
#include "Polygon.hh"

using namespace std;
using namespace bitmap;

void doDrawPoly(const Polygon& p, CostMap* map, ostream* os)
{
    const vector<cost_t>& xv = p.getXv();
    const vector<cost_t>& yv = p.getYv();
    const vector<cost_t>& cost = p.getCostVec();
    for (unsigned int i = 0; i < xv.size(); i++)
    {
        cout << "(x=" << xv[i] << ", y=" << yv[i] << ", cost=" << cost[i] << ")" <<endl;
    }
    p.draw(map);
    for (unsigned int i = 0; i < xv.size(); i++)
        map->setPixelLoc(1.0, point2(xv[i], yv[i]));

    // output the polygon to the matlab script
    static int i = 1;
    if (os != NULL) {
        ostringstream ostr;
        ostr << "poly{" << i++ << "}";
        p.saveToMatlabScript(*os, ostr.str());
    }
}

int main(void)
{
    const int numPoly = 3;
    bool saveToMatlab = true;

    float minX = 50;
    float minY = 50;
    float maxX = 1650;
    float maxY = 1250;
    point2 center((maxX - minX)/2 + minX, (maxY - minY)/2 + minY);

    CostMap map;
    map.init(800, 600, 2, 2, center);

    map.setPixelLoc(1.0, point2(824, 624));
    map.setPixelLoc(1.0, point2(minX, minY));
    map.setPixelLoc(1.0, point2(maxX-2, maxY-2));

    int seed = time(0);
    //int seed = 1178255800; // splitted polygon bug
    //int seed = 1178276601; // bottom edge handling bug
    //int seed = 1178306497; // bug with new code and dangling lines
    //int seed = 1178526376; // bug in bresenham algorithm (first impl)
    //int seed = 1178603976;
    cout << "random seed: " << seed << endl;
    srand(seed);

    ostream* of = NULL;
    if (saveToMatlab) {
        of = new ofstream("testpoly.m"); // generated matlab scripts with the results
        *of << "minX = " << minX << ";" << endl;
        *of << "minY = " << minY << ";" << endl;
        *of << "maxX = " << maxX << ";" << endl;
        *of << "maxY = " << maxY << ";" << endl;
    }

    Polygon p;
    vector<cost_t>& xv = p.getXv();
    vector<cost_t>& yv = p.getYv();
    vector<cost_t>& cost = p.getCostVec();
    // generate a triangle, because it's always convex
    // TODO: try with more complex convex polygons!
    xv.resize(3);
    yv.resize(3);
    cost.resize(3);
    for (int k = 0; k < numPoly; k++)
    {
        cout << "Generating random polygon " << k << ": " << endl;
        for (unsigned int i = 0; i < xv.size(); i++)
        {
            xv[i] = (rand()/double(RAND_MAX)) * (maxX - minX) + minX;
            yv[i] = (rand()/double(RAND_MAX)) * (maxY - minY) + minY;
            cost[i] = rand()/double(RAND_MAX) * 0.4;
        }
        doDrawPoly(p, &map, of);
    }

    // draw a polygon with a very acute angle
    cout << "Building very acute triangle" << endl;
    xv[0] = minX;
    yv[0] = minY + (maxY - minY) / 4.0;
    xv[1] = maxX-1;
    yv[1] = minY + (maxY - minY) / 3.0;
    xv[2] = maxX - 100;
    yv[2] = minY + (maxY - minY) / 3.0;
    fill(cost.begin(), cost.end(), 0.4);
    doDrawPoly(p, &map, of);


    p.setCombFunc(COMB_MAX);
    // gaussian-like
    p.setFillFunc(FILL_EXP2);
    p.setA(-1);
    p.setB(0.8);
    p.setC(0.2);

    cout << "Drawing a rectangle" << endl;
    xv.resize(4);
    yv.resize(4);
    cost.resize(4);
    xv[0] = 100;
    yv[0] = 100;
    cost[0] = 0;
    xv[1] = 300;
    yv[1] = 100;
    cost[1] = 0;
    xv[2] = 300;
    yv[2] = 200;
    cost[2] = 5;
    xv[3] = 100;
    yv[3] = 200;
    cost[3] = 5;
    doDrawPoly(p, &map, of);

    p.setLinear();
    cout << "Drawing another overlapping rectangle" << endl;
    for (unsigned int i = 0; i < xv.size(); i++)
    {
        xv[i] += 50;
        yv[i] += 50;
    }
    fill(cost.begin(), cost.end(), 0.2);
    doDrawPoly(p, &map, of);


    cout << "Drawing a triagle" << endl;
    p.setCombFunc(COMB_ADD);
    p.setLinear();
    xv.resize(3);
    yv.resize(3);
    cost.resize(3);
    xv[0] = 400;
    yv[0] = 100;
    cost[0] = 0.3;
    xv[1] = 400;
    yv[1] = 200;
    cost[1] = 0.3;
    xv[2] = 600;
    yv[2] = 200;
    cost[2] = 0.3;
    doDrawPoly(p, &map, of);

    cout << "Drawing a triagle with an edge in common with the previous" << endl;
    xv[0] = 400;
    yv[0] = 100;
    cost[0] = 0.6;
    xv[1] = 600;
    yv[1] = 200;
    cost[1] = 0.6;
    xv[2] = 600;
    yv[2] = 100;
    cost[2] = 0.6;
    doDrawPoly(p, &map, of);

    
#if 0

    // buggy polygon
    xv[0] = 238.553;
    yv[0] = 529.621;
    xv[2] = 107.38;
    yv[2] = 475.76;
    xv[1] = 423.964;
    yv[1] = 390.272;
    for (unsigned int i = 0; i < xv.size(); i++)
    {
        cost[i] = 0.4;
        cout << "(x=" << xv[i] << ", y=" << yv[i] << ", cost=" << cost[i] << ")" <<endl;
    }
    p.draw(&map);
    for (unsigned int i = 0; i < xv.size(); i++)
        map.setPixelLoc(1.0, point2(xv[i], yv[i]));

    // draw a peculiar polygon, with more than 3 vertexes, the bottom edges completely
    // out of the grid, but some other edge is visible
    xv.resize(5);
    yv.resize(5);
    cost.resize(5);
    cout << "Building peculiar polygon" << endl;
    xv[0] = minX + (maxX - minX) / 5.0;
    yv[0] = maxY + 100;
    xv[1] = minX + (maxX - minX) / 4.0;
    yv[1] = maxY + 50;
    xv[2] = minX + (maxX - minX) / 4.0;
    yv[2] = maxY - 50;
    xv[3] = minX + (maxX - minX) * 3.0 / 4.0;
    yv[3] = maxY - 50;
    xv[4] = minX + (maxX - minX) * 3.0 / 4.0;
    yv[4] = maxY + 50;
    for (unsigned int i = 0; i < xv.size(); i++)
    {
        cost[i] = rand()/double(RAND_MAX) * 0.5;
        cout << "(x=" << xv[i] << ", y=" << yv[i] << ", cost=" << cost[i] << ")" <<endl;
    }
    p.draw(&map);
    for (unsigned int i = 0; i < xv.size(); i++)
        map.setPixelLoc(1.0, point2(xv[i], yv[i]));
#endif

    if (saveToMatlab) {
        map.saveToMatlabScript(*of, "map");
    }
    map.showWindow("Map");

    while (true)
        cvWaitKey(0);
    
    return 0;
}
