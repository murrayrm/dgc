#include <iostream>
#include <cmath>
#include <cassert>

#include <highgui.h>

#include "Bitmap.hh"

using namespace std;

namespace bitmap
{

    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
        abort();
    }

    void testBasicMethods(CostMap* map)
    {
        volatile int foo; // use volatile to force access to memory
                          // (avoid compiler to optimize away the code)
        // make sure these methods works, no matter the returned value
        foo = map->getBytesPerRow();
        foo = map->getBytesPerPixel();
        volatile uint8_t* data = map->getRawData();
        (void) data; // stop compiler warnings
        (void) foo;
        // getResX/Y, setResX/Y
        float resx = 0.0123;
        float resy = 0.4567;
        if (map->getWidth() != 0) {
            map->setResX(resx);
            assert(fabs(map->getResX() - resx) < 0.0001);
        }
        if (map->getHeight() != 0) {
            map->setResY(resy);
            assert(fabs(map->getResY() - resy) < 0.0001);
        }
    }

    void testClear(CostMap* map)
    {
        float value = 123.4;
        map->clear(value);
        for (int r = 0; r < map->getHeight(); r++) {
            for (int c = 0; c < map->getWidth(); c++) {
                assert(map->getPixel(r, c) == value);
            }
        }
    }

    void testSetGetPixel(CostMap* map)
    {
        map->clear();
        for (int r = 0; r < map->getHeight(); r++) {
            for (int c = 0; c < map->getWidth(); c++) {
                cost_t val = cost_t(rand()) / RAND_MAX;
                map->setPixel(val, r, c);
                assert(map->getPixel(r, c) == val);
            }
        }
    }

    void testSetGetPixelLoc(CostMap* map)
    {
        map->clear();
        point2 upLeft = map->getUpperLeft();
        point2 lowRight = map->getLowerRight();
        float xstep[3] = {
            map->getWidthLoc() / map->getWidth(),
            map->getWidthLoc() / 1000.0, // smaller than 1
            map->getWidthLoc() / 100.0 // bigger than 1
        };
        float ystep[3] = {
            map->getHeightLoc() / map->getHeight(),
            map->getHeightLoc() / 1000.0, // smaller than 1
            map->getHeightLoc() / 100.0 // bigger than 1
        };

        for(unsigned int i = 0; i < sizeof(xstep)/sizeof(xstep[0]); i++) {
            for (float x = upLeft.x; x < lowRight.x - 0.5; x += xstep[i]) {
                for (float y = upLeft.y; y < lowRight.y - 0.5; y += ystep[i]) {
                    cost_t val = cost_t(rand()) / RAND_MAX;
                    map->setPixelLoc(val, x, y);
                    assert(map->getPixelLoc(x, y) == val);
                }
            }
        }
    }

    void testOutOfBounds(CostMap* map)
    {
        int rows = map->getHeight();
        int cols = map->getWidth();

        // check that getPixel always returns the outOfBounds value
        cost_t outOfBounds = map->getOutOfBounds();
        assert(map->getPixel(rows, cols) == outOfBounds);
        assert(map->getPixel(-1, -1) == outOfBounds);
        assert(map->getPixel(rows, 0) == outOfBounds);
        assert(map->getPixel(0, cols) == outOfBounds);
        // same for getPixelLoc
        float hl = map->getHeightLoc();
        float wl = map->getWidthLoc();
        float ox = map->getUpperLeft().x;
        float oy = map->getUpperLeft().y;
        assert(map->getPixelLoc(ox + wl, oy + hl) == outOfBounds);
        assert(map->getPixelLoc(ox - 1, oy - 1) == outOfBounds);
        assert(map->getPixelLoc(ox + wl, oy) == outOfBounds);
        assert(map->getPixelLoc(ox, oy + hl) == outOfBounds);

        // check that setPixel does nothing (i.e. does not segfault or change values)
        map->clear(); // zero all
        // setPixel
        map->setPixel(10.0, rows, cols);
        map->setPixel(10.0, -1, -1);
        map->setPixel(10.0, rows, 0);
        map->setPixel(10.0, 0, cols);
        // setPixelLoc
        map->setPixelLoc(10.0, wl, hl);
        map->setPixelLoc(10.0, ox - 1, oy - 1);
        map->setPixelLoc(10.0, ox + wl, oy);
        map->setPixelLoc(10.0, ox, oy + hl);

        // no value should have changed at all
        for (int r = 0; r < map->getHeight(); r++) {
            for (int c = 0; c < map->getWidth(); c++) {
                assert(map->getPixel(r, c) == 0.0);
            }
        }

    }

    void testDefaultConstructor(CostMap* /*ignored*/)
    {
        CostMap myMap;
        assert(myMap.getWidth() == 0);
        assert(myMap.getHeight() == 0);
        testBasicMethods(&myMap);

        // these tests are a little different than the ones done just calling
        // testOutOfBounds, because w=0 and h=0

#if 0
        testOutOfBounds(&myMap);
#else
        // check that getPixel always returns the outOfBounds value, and setPixel
        // does nothing (i.e. does not segfault)
        cost_t outOfBounds = myMap.getOutOfBounds();
        assert(myMap.getPixel(0, 0) == outOfBounds);
        assert(myMap.getPixel(1, 1) == outOfBounds);
        assert(myMap.getPixel(-1, -1) == outOfBounds);
        assert(myMap.getPixel(1, 0) == outOfBounds);
        assert(myMap.getPixel(0, 1) == outOfBounds);
        // same for getPixelLoc
        assert(myMap.getPixelLoc(0, 0) == outOfBounds);
        assert(myMap.getPixelLoc(1, 1) == outOfBounds);
        assert(myMap.getPixelLoc(-1, -1) == outOfBounds);
        assert(myMap.getPixelLoc(1, 0) == outOfBounds);
        assert(myMap.getPixelLoc(0, 1) == outOfBounds);
        myMap.clear(); // should do nothing
        // setPixel
        myMap.setPixel(10.0, 0, 0);
        myMap.setPixel(10.0, 1, 1);
        myMap.setPixel(10.0, -1, -1);
        myMap.setPixel(10.0, 1, 0);
        myMap.setPixel(10.0, 0, 1);
        // setPixelLoc
        myMap.setPixelLoc(10.0, 0, 0);
        myMap.setPixelLoc(10.0, 1, 1);
        myMap.setPixelLoc(10.0, -1, -1);
        myMap.setPixelLoc(10.0, 1, 0);
        myMap.setPixelLoc(10.0, 0, 1);
#endif
    }

    void assertEqual(const CostMap& map1, const CostMap& map2)
    {
        assert(map1.getWidth() == map2.getWidth());
        assert(map1.getHeight() == map2.getHeight());
        assert(map1.getBytesPerPixel() == map2.getBytesPerPixel());

        // all values should be the same
        for (int r = 0; r < map1.getHeight(); r++) {
            for (int c = 0; c < map1.getWidth(); c++) {
                assert(map1.getPixel(r, c) == (map2.getPixel(r, c)));
            }
        }
    }

    void testCopyConstructorAndAssignment(CostMap* map)
    {
        for (int r = 0; r < map->getHeight(); r++) {
            for (int c = 0; c < map->getWidth(); c++) {
                (*map)[r][c] = rand() * 15.0 / RAND_MAX + 5;
            }
        }
        CostMap map2(*map);
        assertEqual(*map, map2);
        CostMap map3;
        // assigment to an empty map
        map3 = *map;
        assertEqual(*map, map3);
        // assignment to a non-empty map
        map3.init(10, 10, point2(-5, -5), point2(5, 5));
        map3 = *map;
        assertEqual(*map, map3);
    }

    void testShowWindow(CostMap* map)
    {
        for (int r = 0; r < map->getHeight(); r++) {
            for (int c = 0; c < map->getWidth(); c++) {
                (*map)[r][c] = rand() * 15.0 / RAND_MAX + 5;
            }
        }
        map->showWindow("test");
        cvWaitKey(0);
    }

    typedef void (*test_f)(CostMap* map);

    test_f testTable[] = {
        testDefaultConstructor,
        testBasicMethods,
        testOutOfBounds,
        testClear,
        testSetGetPixel,
        testSetGetPixelLoc,
        testCopyConstructorAndAssignment,
        testShowWindow,
        NULL
    };
    
}

using namespace bitmap;

int main()
{
    CostMap map(640, 480);

    // call abort() and generate a core dump when opencv detects an error
    cvRedirectError(abortOnCvError);

    for (int i = 0; testTable[i] != NULL; i++)
    {
        testTable[i](&map);
    }
    return 0;
}

