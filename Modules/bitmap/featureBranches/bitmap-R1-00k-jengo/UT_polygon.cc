#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cassert>
#include <algorithm>

#include <highgui.h>

#include "Bitmap.hh"
#include "Polygon.hh"

using namespace std;

namespace bitmap
{

    int CV_CDECL abortOnCvError(int status, const char* func_name, const char* err_msg,
                                const char* file_name, int line, void* userdata)
    {
        cvStdErrReport(status, func_name, err_msg, file_name, line, userdata );
        abort();
    }

    void testBasicMethods(Polygon* poly)
    {
        /// TBD
    }


    // If v1 and v2 are not null, the indices of two vertices
    // of an edge which is visible from the vertex are stored there.
    bool insidePolygon(const Polygon& poly, float x, float y,
		       int* v1 = NULL, int* v2 = NULL)
    {
        const vector<float>& xv = poly.getXv();
        const vector<float>& yv = poly.getYv();
        bool inside = true;

	// check for cw or ccw order. Assumes at least 3 vertices ;-) !!!
	int last = poly.getVertNum()-1;
	float cross = (xv[1] - xv[0])*(yv[last] - yv[0]) - (yv[1] - yv[0])*(xv[last] - xv[0]);
	float ccw = (cross < 0);
	float premul = ccw ? 1 : -1;

        for (unsigned int i = 0; inside && i < xv.size(); i++) {
            int next = (i + 1 < xv.size()) ? (i + 1) : 0;
            float dxP = x - xv[i];
            float dyP = y - yv[i];
            float dxEdge = xv[next] - xv[i];
            float dyEdge = yv[next] - yv[i];
            cross = dxEdge*dyP - dyEdge*dxP;
            if (premul * cross > 0) {
                inside = false;
		if (v1 != NULL)
		    *v1 = i;
		if (v2 != NULL)
		    *v2 = next;
            }
        }

        return inside;
    }

    // NOTE: assumes counter clockwise order
    void dummyDrawPolygon(Polygon* poly, CostMap* map)
    {
        vector<float> xv = poly->getXv();
        vector<float> yv = poly->getYv();
        vector<cost_t> cost = poly->getCostVec();
        float xMax = 0, yMax = 0;
        float xMin = map->getWidth(), yMin = map->getHeight();

        // convert polygon to map coordinates
        for (unsigned int i = 0; i < xv.size(); i++) {
            map->toMapFrame(xv[i], yv[i], &yv[i], &xv[i]);
            // round to the nearest integer: this is what Polygon::draw does at the moment
            xv[i] = floor(xv[i] + 0.5);
            yv[i] = floor(yv[i] + 0.5);

            if (xv[i] < xMin)
                xMin = xv[i];
            if (xv[i] > xMax)
                xMax = xv[i];
            if (yv[i] < yMin)
                yMin = yv[i];
            if (yv[i] > yMax)
                yMax = yv[i];
        }

        for (int r = 0; r < map->getHeight(); r++) {
            for (int c = 0; c < map->getWidth(); c++) {
                // check to see if point is inside the polygon
                if (r >= yMin && r <= yMax && c >= xMin && c <= xMax)
                {
                    bool edge = false;
                    if (insidePolygon(*poly, c, r)) {
                        (*map)[r][c] = cost[0]; // FIXME
                    } else if (edge) {
                        (*map)[r][c] = cost[0]/2; // FIXME
                        cerr << "Painting edge" << endl;
                    }
                }
            }
        }

    }

    void testPolygonDrawing(Polygon* poly)
    {
        CostMap map1(100, 100);
        CostMap map2(100, 100);
        CostMap diff(100, 100);
        
        dummyDrawPolygon(poly, &map1);
        poly->draw(&map2);

        cvSub(map1.getImage(), map2.getImage(), diff.getImage());

        map1.saveToMatlabScript("reference.m", "ref");
        map2.saveToMatlabScript("polydraw.m", "poly");
        diff.saveToMatlabScript("difference.m", "dif");
/*
        map1.showWindow("Reference polygon (dummy)");
        cvWaitKey(0);
        map2.showWindow("poly->draw()");
        cvWaitKey(0);
        diff.showWindow("difference");

        while (true)
            cvWaitKey(0);
*/
    }

    void generatePolygonsWithCommonEdge(Polygon* p1, Polygon* p2)
    {
	p1->setVertNum(3);
        vector<float>& xv = p1->getXv();
        vector<float>& yv = p1->getYv();
        vector<cost_t>& cost = p1->getCostVec();

        for (unsigned int i = 0; i < 3; i++)
        {
            xv[i] = (rand()/double(RAND_MAX)) * 100;
            yv[i] = (rand()/double(RAND_MAX)) * 100;
            cost[i] = rand()/double(RAND_MAX);
        }
        //fill(poly.getCostVec().begin(), poly.getCostVec().end(), 0.5);

        // create a polygon with one edge in common
        // start finding a point outside of the polygon
        float x, y;
        int edge = -1;
        int edgeNext = -1;
        do
        {
            x = (rand()/double(RAND_MAX)) * 100;
            y = (rand()/double(RAND_MAX)) * 100;
        } while(insidePolygon(*p1, x, y, &edge, &edgeNext));

        p2->setVertNum(3);
        vector<float>& xv2 = p2->getXv();
        vector<float>& yv2 = p2->getYv();
	vector<cost_t>& cost2 = p2->getCostVec();
        xv2[0] = x;
        yv2[0] = y;
	cost2[0] = rand()/double(RAND_MAX);
        xv2[1] = xv[edge];
        yv2[1] = yv[edge];
	cost2[1] = cost[edge];
        xv2[2] = xv[edgeNext];
        yv2[2] = yv[edgeNext];
	cost2[2] = cost[edgeNext];
    }

    void testEdgeInCommon()
    {
        CostMap map(100, 100);

	Polygon p1, p2;
	generatePolygonsWithCommonEdge(&p1, &p2);

        fill(p1.getCostVec().begin(), p1.getCostVec().end(), 0.5);
        fill(p2.getCostVec().begin(), p2.getCostVec().end(), 0.5);

        p1.setCombFunc(COMB_REPLACE);
        p1.draw(&map);
        p2.setCombFunc(COMB_ADD);
        p2.draw(&map);
        ofstream of("commonedge.m");
        map.saveToMatlabScript(of, "edge");
        p1.saveToMatlabScript(of, "poly1");
        p2.saveToMatlabScript(of, "poly2");

    }

    void testEdgeInCommon2()
    {
	// this time use COMB_MAX and use a gradient instead of a
	// flat value, and see if the cost matches at the edges
        CostMap map(100, 100);

	Polygon p1, p2;
	generatePolygonsWithCommonEdge(&p1, &p2);

        p1.setCombFunc(COMB_REPLACE);
        p1.draw(&map);
        p2.setCombFunc(COMB_MAX);
        p2.draw(&map);
        ofstream of("commonedge2.m");
        map.saveToMatlabScript(of, "edge2");
        p1.saveToMatlabScript(of, "p1grad");
        p2.saveToMatlabScript(of, "p2grad");
    }

    void testCorridorSimple()
    {
	// Same as testEdgeInCommon3, but uses some specific polygons instead
	// of randomly generated (similar to a corridor).
        CostMap map(100, 100);

	Polygon p[4];

	//   _____    ___ high cost
	//  |\    |   
	//  |1\ 3 |
	//  |--\--|   <-- zero cost
	//  | 2 \4|
	//  |____\|   ___ high cost

	float y1 = 10;
	float y2 = 90;
	float x1 = 10;
	float x2 = 60;
	vector<point2> v;
	vector<cost_t> cost;

	// polygon 1 (see ascii figure above)
	v.resize(3);
	cost.resize(3);
	v[0] = point2(x1, y1);
	cost[0] = 10;
	v[1] = point2(x1 + (x2 - x1)/2, y1);
	cost[1] = 0;
	v[2] = point2(x1 + (x2 - x1)/2, y1 + (y2 - y1)/2);
	cost[2] = 0;
	p[0].setVertices(v, cost);
	
	// polygon 2 (see ascii figure above)
	v.resize(4);
	cost.resize(4);
	v[0] = point2(x1 + (x2 - x1)/2, y1);
	cost[0] = 0;
	v[1] = point2(x2, y1);
	cost[1] = 10;
	v[2] = point2(x2, y2);
	cost[2] = 10;
	v[3] = point2(x1 + (x2 - x1)/2, y1 + (y2 - y1)/2);
	cost[3] = 0;
	p[1].setVertices(v, cost);

	// polygon 3 (see ascii figure above)
	v.resize(4);
	cost.resize(4);
	v[0] = point2(x1 + (x2 - x1)/2, y2);
	cost[0] = 0;
	v[1] = point2(x1, y2);
	cost[1] = 10;
	v[2] = point2(x1, y1);
	cost[2] = 10;
	v[3] = point2(x1 + (x2 - x1)/2, y1 + (y2 - y1)/2);
	cost[3] = 0;
	p[2].setVertices(v, cost);

	// polygon 4 (see ascii figure above)
	v.resize(3);
	cost.resize(3);
	v[0] = point2(x2, y2);
	cost[0] = 10;
	v[1] = point2(x1 + (x2 - x1)/2, y2);
	cost[1] = 0;
	v[2] = point2(x1 + (x2 - x1)/2, y1 + (y2 - y1)/2);
	cost[2] = 0;
	p[3].setVertices(v, cost);

	ofstream of("testcorr.m");
	for (int i = 0; i < 4; i++)
	{
	    p[i].setCombFunc(COMB_REPLACE);
	    p[i].draw(&map);
	    ostringstream os;
	    os << "pcorr{" << i+1 << "}";
	    p[i].saveToMatlabScript(of, os.str());
	}
	map.saveToMatlabScript(of, "corridor");
    }

    void testCorridor()
    {
        // code generated automatically from the output of tplannerNoCSS
        // from a matlab script
        // This is a real corridor that was sent by the tplanner.
        CostMap map;
        map.init(800, 800, point2(-40.0024, -44.2656), point2(39.9976, 35.7344));
        vector<Polygon> poly(8);
        vector<point2> vertices;
        vector<cost_t> values;
        vertices.resize(3);
        values.resize(3);
        vertices[0] = point2(3.03423, -3.166);
        values[0] = 10;
        vertices[1] = point2(0.0348262, -3.10625);
        values[1] = 0;
        vertices[2] = point2(-0.0245388, -5.82033);
        values[2] = 0;
        poly[0].setVertices(vertices, values);
        vertices.resize(4);
        values.resize(4);
        vertices[0] = point2(0.0348262, -3.10625);
        values[0] = 0;
        vertices[1] = point2(-2.96458, -3.04649);
        values[1] = 10;
        vertices[2] = point2(-3.08331, -8.47466);
        values[2] = 10;
        vertices[3] = point2(-0.0245388, -5.82033);
        values[3] = 0;
        poly[1].setVertices(vertices, values);
        vertices.resize(4);
        values.resize(4);
        vertices[0] = point2(3.03423, -3.166);
        values[0] = 10;
        vertices[1] = point2(-0.0245388, -5.82033);
        values[1] = 0;
        vertices[2] = point2(-0.0840318, -8.54026);
        values[2] = 0;
        vertices[3] = point2(2.91525, -8.60586);
        values[3] = 10;
        poly[2].setVertices(vertices, values);
        vertices.resize(3);
        values.resize(3);
        vertices[0] = point2(-0.0245388, -5.82033);
        values[0] = 0;
        vertices[1] = point2(-3.08331, -8.47466);
        values[1] = 10;
        vertices[2] = point2(-0.0840318, -8.54026);
        values[2] = 0;
        poly[3].setVertices(vertices, values);
        vertices.resize(4);
        values.resize(4);
        vertices[0] = point2(2.91525, -8.60586);
        values[0] = 10;
        vertices[1] = point2(-0.0840318, -8.54026);
        values[1] = 0;
        vertices[2] = point2(-0.396939, -22.8459);
        values[2] = 0;
        vertices[3] = point2(2.28943, -37.2172);
        values[3] = 10;
        poly[4].setVertices(vertices, values);
        vertices.resize(3);
        values.resize(3);
        vertices[0] = point2(-0.0840318, -8.54026);
        values[0] = 0;
        vertices[1] = point2(-3.08331, -8.47466);
        values[1] = 10;
        vertices[2] = point2(-0.396939, -22.8459);
        values[2] = 0;
        poly[5].setVertices(vertices, values);
        vertices.resize(3);
        values.resize(3);
        vertices[0] = point2(2.28943, -37.2172);
        values[0] = 10;
        vertices[1] = point2(-0.396939, -22.8459);
        values[1] = 0;
        vertices[2] = point2(-0.709974, -37.1574);
        values[2] = 0;
        poly[6].setVertices(vertices, values);
        vertices.resize(4);
        values.resize(4);
        vertices[0] = point2(-0.396939, -22.8459);
        values[0] = 0;
        vertices[1] = point2(-3.08331, -8.47466);
        values[1] = 10;
        vertices[2] = point2(-3.70938, -37.0977);
        values[2] = 10;
        vertices[3] = point2(-0.709974, -37.1574);
        values[3] = 0;
        poly[7].setVertices(vertices, values);

	ofstream of("testrealcorr.m");
	for (unsigned int i = 0; i < poly.size(); i++)
	{
	    poly[i].setCombFunc(COMB_REPLACE);
	    poly[i].draw(&map);
	    ostringstream os;
	    os << "prealcorr{" << i+1 << "}";
	    poly[i].saveToMatlabScript(of, os.str());
	}
	map.saveToMatlabScript(of, "realcorr");
    }

    void testGaussianFill()
    {
        // this is a real test, trying to paint a cost for the sensed lane lines
        CostMap map;
        map.init(800, 800, point2(41.4287, -55.6805), point2(121.429, 24.3195));
        vector<Polygon> poly(1);
        vector<point2> vertices(4);
        vector<cost_t> values(4);
/*
        vertices[0] = point2(88.1334, -21.1202);
        values[0] = -2;
        vertices[1] = point2(82.0267, -4.66793);
        values[1] = -2;
        vertices[2] = point2(80.0265, -5.41038);
        values[2] = 2;
        vertices[3] = point2(86.1333, -21.8631);
        values[3] = 2;
*/
        vertices[0] = point2(82.5083, -23.2078);
        values[0] = -2;
        vertices[1] = point2(76.4017, -6.7558);
        values[1] = -2;
        vertices[2] = point2(74.4015, -7.49825);
        values[2] = 2;
        vertices[3] = point2(80.5082, -23.9506);
        values[3] = 2;

        poly[0].setVertices(vertices, values);
        poly[0].setFillFunc(FILL_EXP2);
        poly[0].setA(-0.5);
        poly[0].setB(10);
        poly[0].setC(0);

	ofstream of("testgaussfill.m");
	for (unsigned int i = 0; i < poly.size(); i++)
	{
	    poly[i].setCombFunc(COMB_REPLACE);
	    poly[i].draw(&map);
	    ostringstream os;
	    os << "pgauss{" << i+1 << "}";
	    poly[i].saveToMatlabScript(of, os.str());
	}
	map.saveToMatlabScript(of, "mapgauss");
    }

    void runAll()
    {
        Polygon poly;
        testBasicMethods(&poly);
        vector<float>& xv = poly.getXv();
        vector<float>& yv = poly.getYv();
        vector<cost_t>& cost = poly.getCostVec();
        xv.resize(3);
        yv.resize(3);
        cost.resize(3);
        xv[0] = 10;
        yv[0] = 10;
        cost[0] = 1;
        xv[1] = 10;
        yv[1] = 95;
        cost[1] = 1;
        xv[2] = 22;
        yv[2] = 10;
        cost[2] = 1;
        testPolygonDrawing(&poly);
        testEdgeInCommon();
        testEdgeInCommon2();
        testCorridorSimple();
        testCorridor();
        testGaussianFill();
    }
    
}

using namespace bitmap;

int main()
{
    CostMap map(640, 480);

    //int seed = time(0);
    //int seed = 1179276242;
    int seed = 1179357088;
    cout << "random seed: " << seed << endl;
    srand(seed);

    // call abort() and generate a core dump when opencv detects an error
    cvRedirectError(abortOnCvError);

    runAll();

    return 0;
}

