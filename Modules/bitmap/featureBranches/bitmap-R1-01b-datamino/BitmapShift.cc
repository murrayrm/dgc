#include <iostream>
#include <cstdlib>
#include <cassert>

#include "Bitmap.hh"

// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

namespace bitmap
{

    void BitmapBase::shift(int cols, int rows, bool clear, const CvScalar& value)
    {
        if (rows == 0 && cols == 0) return;

        if (abs(cols) >= getWidth() || abs(rows) >= getHeight())
        {
            if (clear)
                cvSet(img, value);
            return;
        }

        uint8_t* data = getRawData();
        int bytesPerRow = getBytesPerRow(); // may be bigger than bytesPerPix*width

        uint8_t* dataEnd = data + getHeight()*bytesPerRow;
        uint8_t* src = data;
        uint8_t* dst = data;
        if (cols > 0)
            src += cols * bytesPerPix;
        else
            dst += (-cols) * bytesPerPix;
            
        if (rows > 0)
            src += rows * bytesPerRow;
        else
            dst += (-rows) * bytesPerRow;

        if (src != dst)
            memmove(dst, src, min(dataEnd - src, dataEnd - dst));

        if (clear)
        {
            int width = getWidth();
            int height = getHeight();
            // find out the regions to clear
            if (rows > 0) {
                // clear bottom
                cvRectangle(img, cvPoint(0, height - rows), cvPoint(width, height),
                            value, CV_FILLED);
                if (cols > 0) {
                    // clear bottom-right
                    cvRectangle(img, cvPoint(width-cols-1, 0), cvPoint(width, height-rows),
                                value, CV_FILLED);
                } else if (cols < 0) {
                    // clear bottom-left
                    cvRectangle(img, cvPoint(0, 0), cvPoint(-cols+1, height-rows),
                                value, CV_FILLED);
                }
            } else if (rows < 0) {
                // clear top
                cvRectangle(img, cvPoint(0,  0), cvPoint(width, -rows+1),
                            value, CV_FILLED);
                if (cols > 0) {
                    // clear top-right
                    cvRectangle(img, cvPoint(width-cols-1, -rows), cvPoint(width, height),
                                value, CV_FILLED);
                } else if (cols < 0) {
                    // clear top-left
                    cvRectangle(img, cvPoint(0, -rows), cvPoint(-cols+1, height),
                                value, CV_FILLED);
                }
            }
        }
    }

    void CostMap::shift(float x, float y, cost_t val)
    {
        point2 ul = getUpperLeft();
        point2 lr = getLowerRight();
        int cols = int(x * getWidth() / (lr.x - ul.x));
        int rows = int(y * getHeight() / (lr.y - ul.y));

        // use this to update upper-left and lower right, to avoid numeric errors!
        point2 actualDiff = point2(cols * (lr.x - ul.x) / getWidth(),
                                   rows * (lr.y - ul.y) / getHeight());

        base_t::shift(cols, rows, true, cvRealScalar(val));

        if (cols != 0) {
            ul.x += actualDiff.x;
            lr.x += actualDiff.x;
        }
        if (rows != 0) {
            ul.y += actualDiff.y;
            lr.y += actualDiff.y;
        }

        setUpperLeft(ul);
        setLowerRight(lr);
    }

}
