#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <cmath>

#include "Polygon.hh"

// Useful message macro
#define MSG(arg) (std::cerr << __FILE__ << ':' << __LINE__ << ": " << arg << std::endl)
// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)

namespace bitmap
{
    using namespace std;

    void Polygon::setVertices(const vector<point2>& v, const vector<float>& cost)
    {
        assert(cost.size() == v.size());
        m_xv.resize(v.size());
        m_yv.resize(v.size());
        m_costVec.resize(cost.size());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            m_xv[i] = v[i].x;
            m_yv[i] = v[i].y;
            m_costVec[i] = cost[i];
        }
    }

    // the code is exactly the same as for the method using point2
    void Polygon::setVertices(const vector<point2_uncertain>& v, const vector<float>& cost)
    {
        assert(cost.size() == v.size());
        m_xv.resize(v.size());
        m_yv.resize(v.size());
        m_costVec.resize(cost.size());
        for (unsigned int i = 0; i < v.size(); i++)
        {
            m_xv[i] = v[i].x;
            m_yv[i] = v[i].y;
            m_costVec[i] = cost[i];
        }
    }


    void combineReplace(cost_t* dest, cost_t val)
    {
        *dest = val;
    }

    void combineMax(cost_t* dest, cost_t val)
    {
        *dest = max(*dest, val);
    }

    void combineMin(cost_t* dest, cost_t val)
    {
        *dest = min(*dest, val);
    }

    void combineAdd(cost_t* dest, cost_t val)
    {
        *dest += val;
    }

    void combineSub(cost_t* dest, cost_t val)
    {
        *dest -= val;
    }
    
    void combineMul(cost_t* dest, cost_t val)
    {
        *dest *= val;
    }

    void combineDiv(cost_t* dest, cost_t val)
    {
        *dest /= val; // if val == 0, you'll get +inf, if this is fine, you decide!
    }

    void combineAvg(cost_t* dest, cost_t val)
    {
        *dest = (*dest + val)/2;
    }

    /// pointer to function that combines two values, and stores the result in 'dest'.
    typedef void (*combine_f)(cost_t* dest, cost_t val);

    combine_f combineTable[] =
    {
        combineReplace,
        combineMax,
        combineMin,
        combineAdd,
        combineSub,
        combineMul,
        combineDiv,
        combineAvg,
        NULL /* just in case, used as a terminator */
    };


    cost_t fillNone(cost_t x, cost_t a)
    {
	(void) a; // avoid compiler warning
	return x;
    }

    cost_t fillLinear(cost_t x, cost_t a)
    {
        return a * x;
    }

    cost_t fillSquare(cost_t x, cost_t a)
    {
        return a * x*x;
    }

    cost_t fillExp(cost_t x, cost_t a)
    {
        return exp(a * x);
    }

    cost_t fillExp2(cost_t x, cost_t a)
    {
        return exp(a * x*x);
    }

    cost_t fillCos(cost_t x, cost_t a)
    {
        return cos(a * x);
    }

    cost_t fillSin(cost_t x, cost_t a)
    {
        return sin(a * x);
    }

    cost_t fillTan(cost_t x, cost_t a)
    {
        return tan(a * x);
    }

    cost_t fillAtan(cost_t x, cost_t a)
    {
        return atan(a * x);
    }

    /// pointer to function that calculates some mathematical function.
    typedef cost_t (*fill_f)(cost_t val, cost_t a);

    fill_f fillTable[] =
    {
        fillNone, //NULL, /* FILL_NONE */
        fillLinear,
        fillSquare,
        fillExp,
        fillExp2,
        fillCos,
        fillSin,
        fillTan,
        fillAtan,
        NULL
    };

    /* Modified Bresenham's line drawing algorithm */
    struct PolyEdge
    {
    private:
        int x;
        float c;
        float incC;
        int rowEnd;

        bool bottom;
        
        int deltay;
        int deltax;
        int error;
        int incX;
        int stepX;
	float stepC;
        
    public:

        /// @param x1 x coordinate of first point (column)
        /// @param x2 x coordinate of second point (column)
        /// @param y1 y coordinate of first point (row)
        /// @param y2 y coordinate of secons point (row). Must be y2 < y1
        /// @param c1 Cost of the first vertex
        /// @param c2 Cost of the second vertex
        /// @param left Set to true if this is a left edge
        void set(float x1, float x2, float y1, float y2, float c1, float c2, bool left)
        {
            //MSG("PolyEdge::set(" << x1 << ", " << x2 << ", " << y1 << ", " << y2 << ", "
            //    << c1 << ", " << c2 << ", " << left << ")");
            
            c = c1;
	    //assert(x2 >= x1);
            if (y1 - y2 > 0.001)
            {
                incC = (c2 - c1) / (y1 - y2);
            }
            else
            {
                incC = 0;
            }
            rowEnd = int(y2 + 0.5);
            if (rowEnd < 0)
            {
                rowEnd = 0;
            }

            bottom = (left && (x2 < x1)) || (!left && (x2 > x1));

            deltay = int(y1+0.5) - int(y2+0.5);
            deltax = abs(int(x2+0.5) - int(x1+0.5));
            error = -deltay / 2; // use this to draw the pixel closest to the line
	                         // using -deltay would draw the pixels ABOVE the line
	                         // using 0 would draw the pixels BELOW the line

	    if (fabs(x2 - x1) > 0.001) {
		stepC = (c2 - c1) / fabs(x2 - x1);
	    } else {
		stepC = 0;
	    }
            if (deltay != 0) {
                incX = deltax / deltay;
                deltax = deltax % deltay;
                if (x2 < x1) {
                    incX = -incX;
                    stepX = -1;
                } else {
                    stepX = 1;
                }
            } else {
                incX = 0;
                stepX = 0;
            }

            x = int(x1 + 0.5);
            c = c1;
            if (bottom)
            { 	 
                x += incX;
            }
            //MSG("PolyEdge::set: x=" << x << ", incX=" << incX << ", c=" << c
            //    << ", incC=" << incC << ", rowEnd=" << rowEnd);
        }

        void next()
        {
            c += incC;

            if (!bottom) {
                x += incX;
	    }

            error = error + deltax;
            if (error > 0)
            {
                x += stepX;
                error = error - deltay;
            }

            if (bottom) {
                x += incX;
	    }
        }

	int getX()
	{
	    return x;
	}

	float getC()
	{
	    return c;
	}

	float getErr()
	{
	    return -bottom*incX + stepX * (double(error)/deltay + 0.5);
	}

        bool done(int row)
        {
            return row <= rowEnd;
        }

    };

    
    /** Draws a scanline (horizontal straight line). The length of the scanline once
     * drawn will be (x2 - x1 + 1) cells, including the starting point and the
     * ending one. If x1 > x2, the endpoints will be exchanged.
     * @param map CostMap where to draw the scanline
     * @param x1 starting point on the row. It's NOT required that x1 < x2.
     * @param x2 ending point on the row.
     * @param val1 Desired value of cell at x1.
     * @param val2 Desired value of cell at x2.
     * @param row The row where all the drawing should happen.
     */
    void Polygon::drawScanline(CostMap* map, int x1, int x2, float val1, float val2,
			       float err1, float err2, int row) const
    {
        //MSG("DBG: drawScanline: row=" << row << ", x1=" << x1 << ", x2=" << x2);
        cost_t curVal;
        cost_t incVal;
  
        // we want x2 > x1
        if (x2 < x1) {
	    // this shouldn't happen with the current version of drawPolygon though
            swap(x2, x1);
            swap(val1, val2);
            swap(err1, err2);
        }
        if (x1 > (map->getWidth() - 1) || x2 < 0) // completely out of bounds
            return;
        //if (x2 == x1)
        //    return;

	// use error to calculate subpixel coordinates
	float x1f = x1 + err1;
	float x2f = x2 + err2;
	if (x2f - x1f > 1e-6) {
	    incVal = (val2 - val1) / (x2f - x1f);
	    curVal = val1 - err1 * incVal;
	} else {
	    incVal = 0;
	    curVal = val1;
	}

        if (x1 < 0) {
            curVal += incVal*(-x1);
            x1 = 0;
        }
        if (x2 > (map->getWidth() - 1))
            x2 = map->getWidth() - 1;
        //if(x2 == x1)
        //    return;

        cost_t* adr = map->getRow(row) + x1;

	// check for special cases and run optimized (faster) versions
	if (m_fill == FILL_LINEAR || m_fill == FILL_NONE) {

	    if (m_fa != 1 || m_fb != 1 || m_fc != 0) {
		curVal = m_fb * m_fa * curVal + m_fc;
		incVal = m_fb * m_fa * incVal;
	    }

	    if (m_comb == COMB_REPLACE) {
		// optimized version for the simplest case
		for(int x = x1; x <= x2; x++) {
		    *adr = curVal;
		    adr++;
		    curVal += incVal;
		}
	    } else {
		// optimized version for the simpler case
		combine_f comb = combineTable[m_comb];		
		for(int x = x1; x <= x2; x++) {
		    comb(adr, curVal);
		    adr++;
		    curVal += incVal;
		}
	    }
	} else {
	    // completely general version
	    fill_f fill = fillTable[m_fill];
	    combine_f comb = combineTable[m_comb];
	    // load some variables into cpu registers
	    register float a = m_fa, b = m_fb, c = m_fc;
	    
	    for(int x = x1; x <= x2; x++) {
		comb(adr, b * fill(curVal, a) + c);
		adr++;
		curVal += incVal;
	    }
	}
    }



    static inline int nextVert(int vert, int nVert)
    {
        return (vert + 1 >= nVert) ? 0 : (vert + 1);
    }

    static inline int prevVert(int vert, int nVert)
    {
        return (vert - 1 < 0) ? (nVert - 1) : (vert - 1);
    }

    void Polygon::drawPolygon(CostMap* map,
                      const vector<float>& vertX,
                      const vector<float>& vertY,
                      const vector<cost_t>& vertVal) const
    {
        int nVert = int(vertX.size());
        assert(int(vertY.size()) == nVert);
        assert(int(vertVal.size()) == nVert);

        //MSG("drawPolygon BEGIN");

        float miny=numeric_limits<float>::max();
        float maxy=numeric_limits<float>::min();

        int bottom = 0;
        int row;

        // search for the vertex with the highes y value
        for (int i = 0; i < nVert; i++) {
            if (vertY[i] > maxy) {
                bottom = i;
                maxy = vertY[i];
            }
            if (vertY[i] < miny) {
                miny = vertY[i];
            }
        }
        if(vertY[bottom] < 0) return; // completely over the top boundary
        if(miny < 0) miny = 0; //bugfix (chance of infinite loop otherwise)

        int left = prevVert(bottom, nVert);
        int right = nextVert(bottom, nVert);
        bool cw = false;
        float dxL = vertX[left]  - vertX[bottom];
        float dyL = vertY[left]  - vertY[bottom];
        float dxR = vertX[right] - vertX[bottom];
        float dyR = vertY[right] - vertY[bottom];
        if (dxL*dyR - dyL*dxR < 0)
        {
            //MSG("clockwise order, swapping left and right");
            swap(left, right);
            cw = true; // clockwise vertex order
        }

        PolyEdge leftEdge, rightEdge;
        leftEdge.set(vertX[bottom],   vertX[left],
                     vertY[bottom],   vertY[left],
                     vertVal[bottom], vertVal[left],
                     true
            );
        rightEdge.set(vertX[bottom],   vertX[right],
                      vertY[bottom],   vertY[right],
                      vertVal[bottom], vertVal[right],
                      false
            );

        int minRow = int(miny + 0.5);
        for (row = int(vertY[bottom] + 0.5); row > minRow; row--)
        {
            //MSG("row " << row << ":");
            if (leftEdge.done(row))
            {
                //MSG("  next left edge");
                int last = left;
                if (cw)
                    left = nextVert(left, nVert); // want them in ccw
                else
                    left = prevVert(left, nVert);

                leftEdge.set(vertX[last],   vertX[left],
                             vertY[last],   vertY[left],
                             vertVal[last], vertVal[left],
                             true
                    );
            }
            if (rightEdge.done(row))
            {
                //MSG("  next right edge");
                int last = right;
                if (cw)
                    right = prevVert(right, nVert); // want them in ccw
                else
                    right = nextVert(right, nVert);

                rightEdge.set(vertX[last],   vertX[right],
                              vertY[last],   vertY[right],
                              vertVal[last], vertVal[right],
                              false
                    );
            }

            if (row < map->getHeight())
            {
                drawScanline(map, leftEdge.getX(), rightEdge.getX(),
                             leftEdge.getC(), rightEdge.getC(),
                             leftEdge.getErr(), rightEdge.getErr(),
			     row);
            }
            leftEdge.next();
            rightEdge.next();
            //MSG("  leftEdge.x = " << leftEdge.x);
            //MSG("  rightEdge.x = " << rightEdge.x);
        }

        // special case for the last row ... didn't find any better way to deal with it
        drawScanline(map, int(vertX[left]+0.5), int(vertX[right]+0.5),
		     vertVal[left], vertVal[right], 0.0, 0.0, row);
        

        //MSG("drawPolygon END");
    }

    /**
     * Draw the polygon on the specified cost map, interpreting the vertices
     * as they are in local frame.
     * The polygon will be correctly clipped if it's partly or completely
     * outside of the map.
     */
    void Polygon::draw(CostMap* bmp) const
    {
        unsigned int nVec = m_xv.size();
        vector<float> rowVec(nVec);
        vector<float> colVec(nVec);
        //MSG("DBG: draw(): translating coordinates (up left = "
        //    << bmp->getUpperLeft() << ", low right = " << bmp->getLowerRight() << ")");
        for (unsigned int i = 0; i < nVec; i++)
        {
            bmp->toMapFrame(m_xv[i], m_yv[i], &rowVec[i], &colVec[i]);
            //MSG("   (" << m_xv[i] << ", " << m_yv[i] << ") --> (" << colVec[i] << ", " << rowVec[i] << ")");
        }
        //MSG("DBG: draw() end");
        drawPolygon(bmp, colVec, rowVec, m_costVec);
    }

    /**
     * Draw the polygon on the specified cost map, interpreting the vertices
     * as they are in map coordinates already (y = row, x = column).
     * The polygon will be correctly clipped if it's partly or completely
     * outside of the map.
     */
    void Polygon::drawMapCoords(CostMap* bmp) const
    {
        drawPolygon(bmp, m_xv, m_yv, m_costVec);

    }


    void Polygon::saveToMatlabScript(ostream& out, string name) const
    {
        out << name << " = [ " << endl;
        for (unsigned int i = 0; i < getVertNum(); i++) {
            out << m_xv[i] << ", " << m_yv[i] << ", " << m_costVec[i] << ";" << endl;
        }
        out << "];" << endl;
    }

    void Polygon::saveToMatlabScript(const char* filename, string name) const
    {
        ofstream of(filename);
        saveToMatlabScript(of, name);
    }

};
