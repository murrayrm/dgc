              Release Notes for "bitmap" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "bitmap" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "bitmap" module can be found in
the ChangeLog file.

Release R1-01b (Wed Oct 10 20:51:12 2007):
	Added routines to shift a Bitmap or a CostMap, used to shift the
        map when alice moves (yes, like cmap and emap, now bitmap can do
        that too :-).

	In buildPlainTexture(), allocating a bigger buffer, even though
	it's not needed *in theory*, so maybe the nvidia OpenGL won't
	segfault anymore (not sure if it's a nvidia bug though).

	Added CostMap::getCenter().
	Removed annoying warning about MIN and MAX redefined.


Release R1-01a (Tue Aug 28 13:27:30 2007):
	Moved OpenGL-dependent functions to a separate library (-lbitmap_gl).

Release R1-00k (Fri Jun  8 23:53:06 2007):
        Improved getMinCost and getMaxCost methods in BitmapParams, added
	getMinMaxCost to get both at once. It's needed for the mapviewer
        to display correctly the cost of sensed lane lines.
        Fixed some rare segfaults in drawPolygons (polygon with
        zero vertices, or completely outside of the map under
        certain conditions).
        

Release R1-00j (Wed Jun  6 19:45:17 2007):
	Optimization: in BitmapBase::toGlTexture(), if the input image has
	power-of-two size, don't copy it to a temp buffer, use it directly.
	Also, when using the temp buffer, don't memset to zero the buffer
	(pixel in the texture outside of the size of the image will be
	uninitialized).
        No API changes.

Release R1-00i (Tue Jun  5 22:00:34 2007):
	- Changed the way the memory for the bitmap is allocated, now is
	done in a more efficient way, i.e. if you call init() with a width
	and height that are less than or equal to the previous ones (if
	any), the same buffer is used.
        NOTE: this version is tested with all of our software and it works,
        but if you have problems (segfaults), let me know and/or revert to
        the previous release!
        - Added a parameter to the init() functions in CostMap to set the
        initial base value of the map (before it was cleared to zero).
        This way, you can avoid clearing the map to zero then again to some
        other value, which is slow.
        - Added a const qualifier to a bunch of methods, so they can be used
        with const objects.
	- Added some functions to get the final cost at each vertex in the
	Polygon class, and to get the minimum and maximum cost in
	BitmapParams (they're not perfect, read the comments in the code!).

Release R1-00h (Mon May 28 21:45:59 2007):
	Added more parameter for out of bounds

Release R1-00g (Wed May 23  2:25:03 2007):
	- Fixed a segfault when a polygon is completely below the bottom
	of the costmap.
	- Fixed drawing routines that sometimes were drawing horizontal lines
	with a constant cost instead of the correct gradient.
	- Added a test case to UT_polygon taken from a real run of
	tplannerNoCSS	that showed some problems.

Release R1-00f (Thu May 17 22:42:51 2007):
	- Added some functions to Bitmap, CostMap and Polygon, among which
	a method to save the map or the polygon to a matlab .m file.
	- Small change in the API (almost backward compatible, shouldn't
	break existing code): FILL_NONE is deprecated in favor of FILL_LINEAR.
	Just use FILL_LINEAR with A = B = 1 and C = 0, or call setLinear()
	to do the same. If you call setFillFunc(FILL_NONE) you'll get the
	above and a warning on cout.
	- Fixed drawing code, now the polygons should be filled correctly,
 	e.g. a polygon with a linear cost should actually yield a constant
	gradient in the map (no more strange artifacts, hopefully).
	

Release R1-00e (Fri May 11 23:21:20 2007):
	- Fixed some segfaults happening when the CostMap was created with
	the default   constructor (and no init() function called). - Added
	a unit test for the CostMap class (UT_costmap). - Added methods to
	get and set the resolution of the costmap. - Added a method to show
	the costmap in a window, using OpenCV's high gui.   Clicking on any
	point will print the coordinates and the value on cout. - Added
	CostMap::getWidthLoc() and getHeightLoc() to get width and height
	in   meters (local frame). - Added setPixelLoc(value, x, y) and
	getPixel(x, y) in addition to versions	 that take a point2.

Release R1-00d (Wed May  9  1:02:33 2007):
	Added a structure for sending parameters to paint cost map

Release R1-00c (Tue May  8 19:32:36 2007):
	Fixed a segfault in the new CostMap::init() method.
	Now CostMap::init() (all of them) clear the map to zero.

Release R1-00b (Tue May  8 18:01:07 2007):
	- Implemented drawing using the specified filling and combining
	functions. Cleaned up the code a little.
	- Implemented a constructor and a init() function in CostMap that
	takes the coordinates of the center point in local frame and the
	resolution.
	- Implemented CostMap::setPixelLoc() and
	CostMap::getPixelLoc() to get and set points using local frame
	coordinates.

Release R1-00a (Tue May  8 13:03:30 2007):
	Moved files from interfaces to this new module.


Release R1-00 (Tue May  8 12:39:46 2007):
	Created.


Other Release notes from the interfaces module:

	R4-01i (Tue May  8  1:49:43 2007):
		Changed the drawing code, now uses Bresenham's algorithm to enumerate
		pixels on the edges of polygons, and fixed a bunch of bugs on the
		Bitmap (CostMap) and Polygon classes (see ChangeLog).

	R4-01h (Fri May  4 17:09:09 2007):
		Added the Polygon class, to be sent from the tplanner to the
		dplanner to describe how to draw the cost map, and the Bitmap class
		(ans yes, another CostMap class ...) to actually hold the map,
		where polygons can be drawn. Only linearly filled polygons can be
		drawn at the moment, and they are drawn one on top of the other
		(replacing values).













