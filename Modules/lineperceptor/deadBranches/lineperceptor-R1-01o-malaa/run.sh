#this file runs the lineperceptor for the four clips


#clips to run
path=~/images/clips/
clips="cordova1 cordova2 washington1 washington2"

#run
for clip in $clips; do
	echo "Running for $clip..."
	#screen -d -m 
	/home/users/malaa/work/src/lineperceptor/lineperceptor mf-medium --close --no-stoplines --save-debug --lanes-conf=Lanes-Medium.conf.try "$path$clip"
done


#now sleep and loop till no screens are there
#cont=1
#while [[ $cont == 1 ]]; do
#	sc=`screen -ls | grep -i "No sockets found" | wc -l`
#	if [[ $sc == 0 ]]; then
#		sleep 1s
#		echo -n "."
#	else
#		cont=0
#		echo
#	fi
#done;





