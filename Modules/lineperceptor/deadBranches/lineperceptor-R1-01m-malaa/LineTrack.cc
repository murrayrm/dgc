/**
 * \file LineTrack.cc
 * \author Mohamed Aly
 * \date 31 July 2007
 * \brief This file contains the implementation for the track repredentation
 *
 */




#include "LineTrack.hh"
#include "StopLinePerceptor.hh"

#include <map/MapElement.hh>
#include <interfaces/RoadLineBlob.h>
#include <interfaces/SensnetTypes.h>

#include <cv.h>



int Track::idState = 0;

float SplineTrack::MERGE_THETA_THRESHOLD = .523;

float SplineTrack::MERGE_R_THRESHOLD = 15;

float SplineTrack::MERGE_MEAN_THETA_THRESHOLD = .523;

float SplineTrack::MERGE_MEAN_R_THRESHOLD = 15;

float SplineTrack::MERGE_CENTROID_THRESHOLD = 50;


float LineTrack::MERGE_THETA_THRESHOLD = .523;

float LineTrack::MERGE_R_THRESHOLD = 15;

bool Track::USE_GROUND_PLANE = true;

bool Track::USE_RIGHT_CAMERA = false;
    

/**
 * \brief gets a unique map id for this spline track
 * 
 * \param mapIdPrefix the prefix to use for the map id
 */
int Track::getMapId(MapId& mapIdPrefix)
{
    //get a new id from the static member
    this->id = MapId(mapIdPrefix);
    this->id.push_back(++Track::idState);

    return Track::idState;
}


/**
 * \brief gets a unique map id for this spline track
 * 
 * \param mapTalker the map element talker to use 
 */
int Track::deleteFromMap(CMapElementTalker& mapTalker)
{
    //delete this map id
   //fill in the map element
    MapElement me;
    me.type = ELEMENT_CLEAR;
    me.setId(this->id);

    //send
    mapTalker.sendMapElement(&me, 0);

    //clear
    me.clear();

    return 0;
}


/**
 * \brief compares two lines and checks if to merge them
 * \param line the input line
 * \return true if they should be merged, false otherwise
 */
bool LineTrack::checkMerge(const Track &line)
{
    //convert to splinetrack object
    const LineTrack& lt = static_cast<const LineTrack&> (line);

    //we want to check if these two splines should be merged
    return mcvCheckMergeLines(this->line, lt.line,
			      LineTrack::MERGE_THETA_THRESHOLD,
			      LineTrack::MERGE_R_THRESHOLD);

}

/**
 * \brief compares two splins and checks if to merge them
 * \param sp the input spline
 * \return true if they should be merged, false otherwise
 */
bool SplineTrack::checkMerge(const Track &sp)
{
    //convert to splinetrack object
    const SplineTrack& st = static_cast<const SplineTrack&> (sp);

    //we want to check if these two splines should be merged
    return mcvCheckMergeSplines(this->spline, st.spline,
				SplineTrack::MERGE_THETA_THRESHOLD,
				SplineTrack::MERGE_R_THRESHOLD,
				SplineTrack::MERGE_MEAN_THETA_THRESHOLD,
				SplineTrack::MERGE_MEAN_R_THRESHOLD,
				SplineTrack::MERGE_CENTROID_THRESHOLD);
}


/**
 * \brief merges two tracks together
 * \param t the input track
 * 
 */
int Track::merge(const Track &t)
{
    //increment
    //this->numSeenFrames++;
    

    //take latest frame last seen
    this->frameLastSeen = this->frameLastSeen > t.frameLastSeen ? 
	this->frameLastSeen : t.frameLastSeen;

    //take earliest frame Id
    this->frameId = this->frameId<t.frameId ? this->frameId : t.frameId;

    //set the color
    //update color
//     if (this->color != LINE_COLOR_YELLOW)
    this->color = t.color;

    return 0;
}

/**
 * \brief merges two spline tracks together
 * \param sp the input spline
 * 
 */
int LineTrack::merge(const Track &t)
{
    //convert to splinetrack object
    const LineTrack& lt = static_cast<const LineTrack&> (t);

//     //check which has the highest score
//     if (this->score < st.score)
    //put the new spline in place of old one
    if (this->frameLastSeen < lt.frameLastSeen)
	this->line = lt.line;


    //put score
    this->score += lt.score;

    //increment
    //this->numSeenFrames++;
    

    //do basic merging
    Track::merge(t);

    return 0;
}

/**
 * \brief merges two spline tracks together
 * \param sp the input spline
 * 
 */
int SplineTrack::merge(const Track &t)
{
    //convert to splinetrack object
    const SplineTrack& st = static_cast<const SplineTrack&> (t);

//     //check which has the highest score
//     if (this->score < st.score)
    //put the new spline in place of old one
    if (this->frameLastSeen < st.frameLastSeen)
//     if ((this->score < st.score) ||
// 	(st.frameLastSeen - this->frameLastUpdated) >2)
    {
	this->spline = st.spline;
	this->frameLastUpdated = st.frameLastSeen;
    }


    //put score
    //this->score += st.score;
    this->score = this->score>st.score ? this->score : st.score;
    
    //increment
    //this->numSeenFrames++;

    //do basic merging
    Track::merge(t);
    

//     //take latest frame last seen
//     this->frameLastSeen = this->frameLastSeen > st.frameLastSeen ? this->frameLastSeen : st.frameLastSeen;

//     //take earliest frame Id
//     this->frameId = this->frameId<st.frameId ? this->frameId : st.frameId;

    return 0;
}



/**
 * \brief sends this track to the map
 *
 * \param mapTalker the map element talker to use 
 * \param stereoBlob the stereo blob to use for conversion to local frame
 */
int SplineTrack::sendToMap(CMapElementTalker& mapTalker, 
			   sensnet_t *sensnet,
			   StereoImageBlob* stereoBlob)
    //bool rightCamera)
{

    //get the spline points
    float px, py, pz;

    //convert spline to points
    CvMat *splinePoints = mcvEvalBezierSpline(this->spline, .05);

    //points to send to map
    point2arr points;

    //get vehicle position
    point2 veh(stereoBlob->state.localX, 
	       stereoBlob->state.localY);

    //loop on points and convert to local frame
    for(int i=0; i<splinePoints->height; i++)
    {	

	float x = CV_MAT_ELEM(*splinePoints, float, i, 0);
	float y = CV_MAT_ELEM(*splinePoints, float, i, 1);

	//check bounds
	if (x<0 || x>stereoBlob->cols-1 || 
	    y<0 || y>stereoBlob->rows-1)
	    continue;
	//check if outside circle in medium image
	float dx = x - 320;
	float dy = y - 235;
	if ((dx*dx + dy*dy > 280*280) && stereoBlob->cols==640)
	    continue;

	//get point in local frame
	int valid;
	valid = Track::ImageToLocal(stereoBlob, x, y, &px, &py, &pz);
	
	if (!valid)
	    continue;
	point2 pt(px, py);

// 	//get disparity
// 	float pd = (float) (*StereoImageBlobGetDisp(stereoBlob, (int)cvGetReal2D(splinePoints, i, 0), (int) cvGetReal2D(splinePoints, i, 1)));
// 	cerr << "i=" << i << endl;
// 	cerr << "disparity=" << pd << endl;
// 	cerr << "valid=" << valid << endl;
// 	cerr << "image point=(" << cvGetReal2D(splinePoints, i, 0) << ","
// 	     << cvGetReal2D(splinePoints, i, 1) << ")" << endl;
// 	cerr << "local point=(" << pt.x << "," << pt.y << ")" << endl;
// 	cerr << "Distance = " << pt.dist(veh) << endl << endl;

	//check distance and add to map
	double dist = pt.dist(veh);
	if (dist <= 50 && dist>=4)
	    //add point to map points
	    points.push_back(point2(px, py));
    }

//     for (unsigned int i=0; i<points.size(); i++) 
// 	cout << i << ":" << points[i].dist(veh) << endl;

    //fill in the map element
    MapElement me;
    me.setGeometry(points);
    me.height = 0;
    if (this->color == LINE_COLOR_WHITE)
	me.plotColor = MAP_COLOR_GREEN;
    else if (this->color == LINE_COLOR_YELLOW)
	me.plotColor = MAP_COLOR_YELLOW;
    me.geometryType = GEOMETRY_LINE;
    me.type = ELEMENT_LANELINE;
    me.setId(this->id);
    me.state = stereoBlob->state;

    //send
    mapTalker.sendMapElement(&me, 0);

    //send to sensviewer
    RoadLineBlob roadLineBlob;
    int blobSize;

    //header
    roadLineBlob.blobType = SENSNET_ROAD_LINE_BLOB;
    roadLineBlob.version = ROAD_LINE_BLOB_VERSION;
    roadLineBlob.sensnetId = this->id.front();
    roadLineBlob.frameId = stereoBlob->frameId;
    roadLineBlob.timestamp = stereoBlob->timestamp;
    roadLineBlob.state = stereoBlob->state;
    roadLineBlob.numLines = 0;

    //now loop on the points, and send every two points as line
    for (int i=0; i<points.size()-1; i++)
    {
	//type
	roadLineBlob.lines[i].type = this->color == LINE_COLOR_YELLOW ?
	    ROAD_LINE_BLOB_TYPE_YELLOW : ROAD_LINE_BLOB_TYPE_WHITE;
	
	//coordinates
	roadLineBlob.lines[i].ax = points[i].x;
	roadLineBlob.lines[i].ay = points[i].y;
	roadLineBlob.lines[i].az = stereoBlob->state.localZ;

	roadLineBlob.lines[i].bx = points[i+1].x;
	roadLineBlob.lines[i].by = points[i+1].y;
	roadLineBlob.lines[i].bz = stereoBlob->state.localZ;
	
	//increment
	roadLineBlob.numLines++;
    }

    //send
    blobSize = sizeof(roadLineBlob) - sizeof(roadLineBlob.lines) + 
	roadLineBlob.numLines*sizeof(roadLineBlob.lines[0]);

    sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
		  roadLineBlob.sensnetId, 
		  roadLineBlob.blobType, 
		  roadLineBlob.frameId, blobSize, 
		  &roadLineBlob);


    //clear
    points.clear();
    cvReleaseMat(&splinePoints);
    me.clear();

//     cerr << "Sent element: " << this->id << endl;
    return 0;
}


/**
 * \brief sends this track to the map
 *
 * \param mapTalker the map element talker to use 
 * \param stereoBlob the stereo blob to use for conversion to local frame
 */
int LineTrack::sendToMap(CMapElementTalker& mapTalker, 
			 sensnet_t *sensnet,
			 StereoImageBlob* stereoBlob)
    //			 bool rightCamera)
{

    //get the spline points
    float px, py, pz;

    //points to send to map
    point2arr points;

    //get line direction
    CvPoint2D32f dir = mcvSubtractVector(this->line.endPoint, 
					 this->line.startPoint);

    //flag for valid disparity
    int valid = 0;

    float t = 0.;
    CvPoint2D32f pt;

    //get start point
    while (!valid && t<=1)
    {
	//get point
	pt = mcvAddVector(line.startPoint,
			  mcvMultiplyVector(dir, t));
	//get point
	valid = Track::ImageToLocal(stereoBlob, pt.x, pt.y, &px, &py, &pz);

	//increment t
	t += .05;
    }
    points.push_back(point2(px, py));

    //get end point
    t = 1.;
    valid = 0;
    while (!valid && t>=0)
    {
	//get point
	pt = mcvAddVector(line.startPoint, 
			  mcvMultiplyVector(dir, t));
	//get point
	valid = Track::ImageToLocal(stereoBlob, pt.x, pt.y, &px, &py, &pz);
	//decrement t
	t -= .05;
    }
    points.push_back(point2(px, py));
    

    //fill in the map element
    MapElement me;
    me.setGeometry(points);
    me.height = 0;
    me.plotColor = MAP_COLOR_RED;
    me.geometryType = GEOMETRY_LINE;
    me.type = ELEMENT_STOPLINE;
    me.setId(this->id);
    me.state = stereoBlob->state;

    //send
    mapTalker.sendMapElement(&me, 0);


    //send to sensviewer
    RoadLineBlob roadLineBlob;
    int blobSize;

    //header
    roadLineBlob.blobType = SENSNET_ROAD_LINE_BLOB;
    roadLineBlob.version = ROAD_LINE_BLOB_VERSION;
    roadLineBlob.sensnetId = this->id.front();
    roadLineBlob.frameId = stereoBlob->frameId;
    roadLineBlob.timestamp = stereoBlob->timestamp;
    roadLineBlob.state = stereoBlob->state;
    roadLineBlob.numLines = 0;

    //now loop on the points, and send every two points as line
    roadLineBlob.lines[0].type = this->color == LINE_COLOR_YELLOW ?
	ROAD_LINE_BLOB_TYPE_YELLOW : ROAD_LINE_BLOB_TYPE_WHITE;
    
    //coordinates
    roadLineBlob.lines[0].ax = points[0].x;
    roadLineBlob.lines[0].ay = points[0].y;
    roadLineBlob.lines[0].az = stereoBlob->state.localZ;
    
    roadLineBlob.lines[0].bx = points[1].x;
    roadLineBlob.lines[0].by = points[1].y;
    roadLineBlob.lines[0].bz = stereoBlob->state.localZ;
    
    //increment
    roadLineBlob.numLines = 1;

    //send
    blobSize = sizeof(roadLineBlob) - sizeof(roadLineBlob.lines) + 
	roadLineBlob.numLines*sizeof(roadLineBlob.lines[0]);

    sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
		  roadLineBlob.sensnetId, 
		  roadLineBlob.blobType, 
		  roadLineBlob.frameId, blobSize, 
		  &roadLineBlob);
    
    //clear
    points.clear();
    me.clear();

    return 0;
}




/**
 * \brief This functions creates a vector of LineTrack objects with the passed in lines
 * \param lines the input lines
 * \param scores the line scores
 * \param frameId the current frame id
 *
 * \return a vector of LineTrack objects
 */
vector<Track*> 
LineTrack::CreateLineTracks(const vector<Line>& lines, 
			    const vector<float>& scores,
			    long frameId)
{
    vector<Track*> tracks;
    tracks.reserve(lines.size());

    //loop on the input vector and create objects
    for (unsigned int i=0; i<lines.size(); ++i)
    {
	LineTrack* tr = new LineTrack(lines[i], frameId, scores[i]);
	tracks.push_back(tr);
    }
    
    //return
    return tracks;
}

/**
 * \brief This functions creates a vector of SplineTrack objects with the passed in splines
 *
 * \param splines the input splines
 * \param scores the splien scores
 * \param frameId the current frame id
 *
 * \return a vector of SplineTrack objects
 */
vector<Track*> 
SplineTrack::CreateSplineTracks(const vector<Spline>& splines, 
				const vector<float>& scores,
				long frameId)
{
    vector<Track*> tracks;
    tracks.reserve(splines.size());

    //loop on the input vector and create objects
    
    for (unsigned int i=0; i<splines.size(); ++i)
    {
	SplineTrack* tr = new SplineTrack(splines[i], frameId, scores[i]);
	tracks.push_back(tr);
    }

    //return
    return tracks;
}

/**
 * \brief This functions destroys a vector of Tracks 
 * \param tracks the tracks to destroy
 * 
 */
int Track::DestroyTracks(vector<Track*>& tracks)
{
        
    //loop on the input vector and free memory
    for (unsigned int i=0; i<tracks.size(); ++i)
	if (tracks[i]->remove)
	    delete tracks[i];

    //clear vector
    tracks.clear();

    //return
    return 0;
}


bool Track::ImageToLocal(StereoImageBlob* stereoBlob,
			 float c, float r, 
			 float* x, float* y, float* z)
{

    int valid;
    if (Track::USE_RIGHT_CAMERA)
	if (Track::USE_GROUND_PLANE)
	    valid = StereoImageBlobImageToLocalPlaneRight(stereoBlob,
							  c, r,
							  x, y, z);
	else
	    valid = StereoImageBlobImageToLocalRight(stereoBlob,
						     c, r,
						     x, y, z);
    else
	if (Track::USE_GROUND_PLANE)
	    valid = StereoImageBlobImageToLocalPlane(stereoBlob,
							  c, r,
							  x, y, z);
	else
	    valid = StereoImageBlobImageToLocal(stereoBlob,
						     c, r,
						     x, y, z);
    
    //return
    return valid;
}
