#ifndef LINETRACKER_HH_
#define LINETRACKER_HH_

/**
 * \file LineTracker.hh
 * \author Mohamed Aly
 * \date 31 July 2007
 * \brief This file contains the header definitions for the linetracker class
 *
 */

#include <interfaces/StereoImageBlob.h>

#include "StopLinePerceptor.hh"
#include "LineTrack.hh"

#include <vector>

using namespace std;

/**
 * \class Tracker
 * \brief This class implements the interface that the tracker should have
 *
 *
 *
 */

class Tracker 
{


public:
    ///constructor
    Tracker(bool rightCamera = false) 
    {
	tracks.reserve(5);
	this->rightCamera = rightCamera;
    }

    ///destructor
    virtual ~Tracker() 
    {
	for (unsigned int i=0; i<this->tracks.size(); i++)
	    delete this->tracks[i];
	tracks.clear();
    }


    ///the tracks it has
    vector<Track*> tracks;

    ///whether to use right or left camera
    bool rightCamera;

    ///the map id prefix to use
    MapId mapIdPrefix;

    /**
     * \brief updates the tracks here with the tracks passed in
     * \param measurements the input tracks
     * \param frameId the current frame Id for the measurements
     */
    virtual int update(vector<Track*>& measurements, long frameId);


    /**
     * \brief sends the update tracks to the map
     *
     * \param mapTalker the MapElementTalker to use
     * \param stereoBlob the stereo blob to use for conversion to local frame
     */
    virtual int sendToMap(CMapElementTalker& mapTalker, 
			  sensnet_t *sensnet, 
			  StereoImageBlob* stereoBlob);


    ///cleans the list of tracks by deleting those marked for removal
    virtual int clean();

    ///number of frames the track is allowed to be absent before deleting it
    int NUM_ABSENT_FRAMES;

    ///number of frames before considering the track good
    int NUM_SEEN_FRAMES;


};


/**
 * \class SplineTracker
 * \brief This class implements a Spline tracker
 *
 *
 *
 */

class SplineTracker : public Tracker
{


public:

    ///constructor
    SplineTracker()
    {}

    ///desctructor
    ~SplineTracker()
    {

    }



};

/**
 * \class LineTracker
 * \brief This class implements a Line tracker
 *
 *
 *
 */

class LineTracker : public Tracker
{


public:

    ///constructor
    LineTracker()
    {}

    ///desctructor
    ~LineTracker()
    {

    }


};


#endif
