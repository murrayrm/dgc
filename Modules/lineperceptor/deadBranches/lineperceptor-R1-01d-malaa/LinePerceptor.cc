
/* 
 * Desc: Line perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard, Mohamed Aly
 * CVS: $Id$
*/

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineMsg.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>



// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"
#include "InversePerspectiveMapping.hh"
#include "StopLinePerceptor.hh"

#include "LinePerceptor.hh"

#define NUM_PRE_FRAMES  3
#define NUM_POST_FRAMES 3
#define STOPLINE_TRACKING_MODEL 1
//0: end points in local frame
//1: r-theta in local frame


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
LinePerceptor::LinePerceptor()
{
    memset(this, 0, sizeof(*this));
    return;
}


// Default destructor
LinePerceptor::~LinePerceptor()
{
  return;
}


// Parse the command line
int LinePerceptor::parseCmdLine(int argc, char **argv)
{
  // Default configuration path
  char defaultConfigPath[256];
  char filename[256];

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

    // Fill out the default config path
  if (this->options.conf_path_given)
      snprintf(defaultConfigPath, sizeof(defaultConfigPath),
             "%s", this->options.conf_path_arg);
  else if (getenv("DGC_CONFIG_PATH"))
    snprintf(defaultConfigPath, sizeof(defaultConfigPath),
             "%s/lineperceptor", getenv("DGC_CONFIG_PATH"));
  else
      return ERROR("unknown configuration path: please set DGC_CONFIG_PATH or set command line --conf-path");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
  {
      //check if replay or blob mode
      if (strstr(this->options.inputs[0], ".blob") != 0)
	  this->mode = modeBlob;
      else
	  this->mode = modeReplay;
  }
  else
    this->mode = modeLive;

  //show option
  if (this->options.show_given)
      this->show = this->options.show_flag;
  else
      this->show = false;
 
  //step option
  if (this->options.step_given)
      this->step = this->options.step_flag;
  else
      this->step = false;

  //export option
  if (this->options.save_given)
      this->save = this->options.save_flag;
  else
      this->save = false;

  //export option
  if (this->options.save_debug_given)
      this->save_debug = this->options.save_debug_flag;
  else
      this->save_debug = false;

  //debug option
  if (this->options.debug_given)
      this->debug = this->options.debug_flag;
  else
      this->debug = false;
  DEBUG_LINES = this->debug;

  //goto option
  if (this->options.goto_given)
      this->gotoFrame = this->options.goto_arg;
  else
      this->gotoFrame = -1;

  //read the stop line perceptor conf
  if (this->options.stoplines_conf_given)
      snprintf(filename, sizeof(filename), "%s/%s",
	       defaultConfigPath, this->options.stoplines_conf_arg); //"StopLinePerceptor.conf");
  else
  {
      switch (this->sensorId)
      {
      case SENSNET_LF_SHORT_STEREO:
      case SENSNET_RF_SHORT_STEREO:
	  snprintf(filename, sizeof(filename), "%s/%s",
		   defaultConfigPath, "Stoplines-Short.conf"); //"StopLinePerceptor.conf");
	  break;
      case SENSNET_MF_MEDIUM_STEREO:
	  snprintf(filename, sizeof(filename), "%s/%s",
		   defaultConfigPath, "Stoplines-Medium.conf"); //"StopLinePerceptor.conf");
	  break;
      case SENSNET_MF_LONG_STEREO:
	  snprintf(filename, sizeof(filename), "%s/%s",
		   defaultConfigPath, "Stoplines-Long.conf"); //"StopLinePerceptor.conf");
	  break;
      default:
	  break;
      }
  }
  mcvInitStopLinePerceptorConf(filename, &this->stopLineConf);

  //read the lane perceptor conf
  if (this->options.lanes_conf_given)
      snprintf(filename, sizeof(filename), "%s/%s",
	       defaultConfigPath, this->options.lanes_conf_arg); //"LanePerceptor.conf");
  else
  {
      switch (this->sensorId)
      {
      case SENSNET_LF_SHORT_STEREO:
      case SENSNET_RF_SHORT_STEREO:
	  snprintf(filename, sizeof(filename), "%s/%s",
		   defaultConfigPath, "Lanes-Short.conf"); //"StopLinePerceptor.conf");
	  break;
      case SENSNET_MF_MEDIUM_STEREO:
	  snprintf(filename, sizeof(filename), "%s/%s",
		   defaultConfigPath, "Lanes-Medium.conf"); //"StopLinePerceptor.conf");
	  break;
      case SENSNET_MF_LONG_STEREO:
	  snprintf(filename, sizeof(filename), "%s/%s",
		   defaultConfigPath, "Lanes-Long.conf"); //"StopLinePerceptor.conf");
	  break;
      default:
	  break;
      }
  }      
  mcvInitStopLinePerceptorConf(filename, &this->laneConf);
  
  //read camera info
  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "CameraInfo.conf");
  mcvInitCameraInfo(filename, &this->cameraInfo);    

  //tracking option
  if (this->options.track_stoplines_given)
      this->trackStopLines = this->options.track_stoplines_flag;
  else
      this->trackStopLines = false;
  this->numPreFrames = 0;
  this->numPostFrames = 0;

  if (this->options.track_lanes_given)
      this->trackLanes = this->options.track_lanes_flag;
  else
      this->trackLanes = false;

  //tracking model
  this->stopLineTrackingModel = STOPLINE_TRACKING_MODEL;

  //detections
  if (this->options.no_stoplines_given)
      this->noStoplines = this->options.no_stoplines_flag;
  else
      this->noStoplines = false;
  if (this->options.no_lanes_given)
      this->noLanes = this->options.no_lanes_flag;
  else
      this->noLanes = false;


  //initialize the spline tracker
  this->laneSplineTracker.mapIdPrefix = MapId(this->moduleId, this->sensorId, 0);
  //set the camera to use
  this->laneSplineTracker.rightCamera = this->options.right_camera_flag;
  //tracking parameters
  this->laneSplineTracker.NUM_ABSENT_FRAMES = this->laneConf.splineTrackingNumAbsentFrames;
  this->laneSplineTracker.NUM_SEEN_FRAMES = this->laneConf.splineTrackingNumSeenFrames;
  
  //initialize spline track constants
  SplineTrack::MERGE_THETA_THRESHOLD = this->laneConf.mergeSplineThetaThreshold;  
  SplineTrack::MERGE_R_THRESHOLD = this->laneConf.mergeSplineRThreshold;
  SplineTrack::MERGE_MEAN_THETA_THRESHOLD = this->laneConf.mergeSplineMeanThetaThreshold;  
  SplineTrack::MERGE_MEAN_R_THRESHOLD = this->laneConf.mergeSplineMeanRThreshold;
  SplineTrack::MERGE_CENTROID_THRESHOLD = this->laneConf.mergeSplineCentroidThreshold;  

  //initialize the stopline tracker
  this->stoplineTracker.mapIdPrefix = MapId(this->moduleId, this->sensorId, 1);
  //set the camera to use
  this->stoplineTracker.rightCamera = this->options.right_camera_flag;
  //tracking parameters
  this->stoplineTracker.NUM_ABSENT_FRAMES = this->stopLineConf.lineTrackingNumAbsentFrames;
  this->stoplineTracker.NUM_SEEN_FRAMES = this->stopLineConf.lineTrackingNumSeenFrames;
  
  //initialize spline track constants
  LineTrack::MERGE_THETA_THRESHOLD = this->stopLineConf.mergeLineThetaThreshold;  
  LineTrack::MERGE_R_THRESHOLD = this->stopLineConf.mergeLineRThreshold;
  
  
  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LinePerceptor $Revision$                                                   \n"
"                                                                           \n"
"Mode: %mode%                                                               \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"Stop lines: %stoplineStats%                                                \n"
"Lane lines: %laneStats%                                                    \n"
"Overall: %overallStats%                                                    \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"%stdout%                                                                   \n"
"%stdout%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%SHOW%|%FAKE%|%SKIP%|%DEBUG%|%EXPORT%|%STEP%]              \n";

// Initialize console display
int LinePerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%SHOW%", " SHOW ", "Ss",
		   (cotk_callback_t) onUserShow, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);

  //button to skip through the log files
  cotk_bind_button(this->console, "%SKIP%", " SKIP ", "Kk",
                   (cotk_callback_t) onUserSkip, this);

  //button to enable/disable debug
  cotk_bind_button(this->console, "%DEBUG%", " DEBUG ", "Dd",
                   (cotk_callback_t) onUserDebug, this);

  //button to enable/disable stepping
  cotk_bind_button(this->console, "%STEP%", " STEP ", "Tt",
                   (cotk_callback_t) onUserStep, this);

  //button to export current frame
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);

    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%mode%", A_NORMAL, "%s", 
	      this->mode==modeReplay ? "Replay" : this->mode==modeBlob ? "Blob" : "Normal");


  return 0;
}


// Finalize sparrow display
int LinePerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int LinePerceptor::initSensnet()
{
  // Start sensnet, even if we are in replay mode, since we may wish
  // to send line messages.
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  if (this->mode == modeLive)
  {
    // Join stereo group for live stereo data
    if (sensnet_join(this->sensnet, this->sensorId,
                     SENSNET_STEREO_IMAGE_BLOB, sizeof(StereoImageBlob)) != 0)
      return -1;
  }
  else if (this->mode == modeReplay)
  {
    // Open replay of exisiting log
    this->replay = sensnet_replay_alloc();
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs)!=0)
      return -1;
  }


  //init sending to mapp
  mapElementTalker.initSendMapElement(this->skynetKey);
  
  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
      return ERROR("unable to join process group");

  return 0;
}


// Clean up sensnet
int LinePerceptor::finiSensnet()
{
    if (this->sensnet)
    {
	//leave stereofeeder channel
	sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB);
	//leave process state channel
	sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
	sensnet_free(this->sensnet);
	this->sensnet = NULL;
    }

    if (this->replay)
    {
	sensnet_replay_close(this->replay);
	sensnet_replay_free(this->replay);
	this->replay = NULL;
    }
    
    return 0;
}


// Get the process state
int LinePerceptor::getProcessState()
{
    int blobId;
    ProcessRequest request;
    ProcessResponse response;

    // Send heart-beat message
    memset(&response, 0, sizeof(response));  
    response.moduleId = this->moduleId;
    response.timestamp = DGCgettime();
    //response.logSize = this->logSize;
    sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
		  this->moduleId, SNprocessResponse, 0, sizeof(response), &response);

    // Read process request
    if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
		     &blobId, sizeof(request), &request) != 0)
	return 0;
    if (blobId < 0)
	return 0;
    
    // If we have request data, override the console values
    this->quit = request.quit;
    //this->enableLog = request.enableLog;  
   
    return 0;
}

// Read the current image
int LinePerceptor::readImage()
{
    //int blobId;

  // Live mode
  if (this->mode == modeLive)
  {
//     // Take a peek at the latest data
//     if (sensnet_peek(this->sensnet, this->sensorId,
//                      SENSNET_STEREO_IMAGE_BLOB, &blobId, NULL) != 0)
//       return -1;
  
//     // Do we have new data?
//     if (blobId < 0 || blobId == this->blobId)
//       return -1;

    //use blocking wait for new data to arrive
    if( sensnet_wait(this->sensnet, 500) != 0)
	return -1;      
    
    // Read the new blob
    if (sensnet_read(this->sensnet, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                     &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;
  }

  // Replay mode
  else if (this->mode == modeReplay)
  {
    // Advance log one step
    if (sensnet_replay_next(this->replay, 0) != 0)
      return -1;
    
    // Read the new blob
    if (sensnet_replay_read(this->replay, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                            &this->blobId, sizeof(this->stereoBlob), &this->stereoBlob) != 0)
      return -1;

    MSG("read blob %d", this->blobId);
  }

  //modeBlob
  else if (this->mode == modeBlob)
  {
      //check if already read it
      if(this->stereoBlob.frameId!=0)
	  return -1;
      //load the blob from the blob file
      FILE *file;
      if ((file=fopen(this->options.inputs[0], "r")) == 0)
	  return ERROR("error opening blob file");
      if (fread(&this->stereoBlob, sizeof(this->stereoBlob), 1, file)!= 1)
	  return ERROR("error reading blob file");
      fclose(file);
      MSG("read blob: %d", this->stereoBlob.frameId);
  }

  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
}


// Write the current stop lines
int LinePerceptor::writeLines()
{
  if (!this->sensnet)
    return 0;

  // This is a bit sneaky: in replay mode, transmit our stereo blob so
  // sensviewer can display both the stereo and the lines
  if (this->mode == modeReplay)
    sensnet_write(this->sensnet, SENSNET_METHOD_SHMEM,
                  this->stereoBlob.sensorId, this->stereoBlob.blobType, this->blobId,
                  sizeof(this->stereoBlob), &this->stereoBlob);

  //send stop lines
  if (this->lineMsg.num_lines > 0)
  {
        
      // Prepare the stopline roadline message
      this->lineMsg.msg_type = SNroadLine;
      this->lineMsg.frameid = this->stereoBlob.frameId;
      this->lineMsg.timestamp = this->stereoBlob.timestamp;
      this->lineMsg.state = this->stereoBlob.state;
 
      // Send line data
      sensnet_write(this->sensnet, SENSNET_METHOD_SKYNET, SENSNET_SKYNET_SENSOR,
		    this->lineMsg.msg_type, 0, sizeof(this->lineMsg), &this->lineMsg);
  
      for (int i=0; i<this->lineMsg.num_lines; i++)
      {
	  //fill point array
	  point2arr points;
	  points.push_back(point2(this->lineMsg.lines[i].a[0], this->lineMsg.lines[i].a[1]));
	  points.push_back(point2(this->lineMsg.lines[i].b[0], this->lineMsg.lines[i].b[1]));
	  //cout << points;
	  //fill map element
	  this->mapElement.clear();
	  this->mapElement.setId(-30-i);
	  this->mapElement.setGeometry(points);
	  this->mapElement.height=0;
	  this->mapElement.plotColor = MAP_COLOR_RED;
	  this->mapElement.type = ELEMENT_STOPLINE;
	  this->mapElement.geometryType = GEOMETRY_LINE;
	  //set_stopline(0, points);
	  //send to map
	  this->mapElementTalker.sendMapElement(&this->mapElement, 0);
	  //clear
	  points.clear();
	  this->mapElement.clear();
      }      
      memset(&this->lineMsg, 0, sizeof(this->lineMsg));
  }

  //send lanes
  if (this->laneMsg.num_lines > 0)
  {
        
      // Prepare the stopline roadline message
      this->laneMsg.msg_type = SNroadLine;
      this->laneMsg.frameid = this->stereoBlob.frameId;
      this->laneMsg.timestamp = this->stereoBlob.timestamp;
      this->laneMsg.state = this->stereoBlob.state;
 
      // Send line data
      sensnet_write(this->sensnet, SENSNET_METHOD_SKYNET, SENSNET_SKYNET_SENSOR,
		    this->laneMsg.msg_type, 0, sizeof(this->laneMsg), &this->laneMsg);
  
      //fill point array
      point2arr points;
      for (int i=0; i<this->laneMsg.num_lines; i++)
      {
	  points.push_back(point2(this->laneMsg.lines[i].a[0], this->laneMsg.lines[i].a[1]));
	  points.push_back(point2(this->laneMsg.lines[i].b[0], this->laneMsg.lines[i].b[1]));
      
	  //cout << points;
	  //fill map element
	  this->mapElement.clear();
	  this->mapElement.setId(-40-i);
	  this->mapElement.setGeometry(points);
	  this->mapElement.height=0;
	  this->mapElement.plotColor = MAP_COLOR_GREEN;
	  this->mapElement.type = ELEMENT_LANELINE;
	  this->mapElement.geometryType = GEOMETRY_LINE;
	  //set_stopline(0, points);
	  //send to map
	  this->mapElementTalker.sendMapElement(&this->mapElement, 0);
	  //clear
	  points.clear();
	  this->mapElement.clear();
      }	  
      memset(&this->laneMsg, 0, sizeof(this->laneMsg));
  }

  
  return 0;
}    


// Handle button callbacks
int LinePerceptor::onUserQuit(cotk_t *console, LinePerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LinePerceptor::onUserPause(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



// Handle fake stop button
int LinePerceptor::onUserFake(cotk_t *console, LinePerceptor *self, const char *token)
{
  float px, py, pz;
    
  MSG("creating fake stop line data");

  self->lineMsg.num_lines = 0;
      
  // MAGIC
  px = VEHICLE_LENGTH + 3;
  py = -2;
  pz = VEHICLE_TIRE_RADIUS;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);

  self->lineMsg.lines[0].a[0] = px;
  self->lineMsg.lines[0].a[1] = py;
  self->lineMsg.lines[0].a[2] = pz;

  px = VEHICLE_LENGTH + 3;
  py = +2;
  pz = 0.5;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);
      
  self->lineMsg.lines[0].b[0] = px;
  self->lineMsg.lines[0].b[1] = py;
  self->lineMsg.lines[0].b[2] = pz;
  
  self->lineMsg.num_lines = 1;
  
  self->totalFakes += 1;

	self->writeLines();
	self->lineMsg.num_lines = 0;

	return 0;
}


int LinePerceptor::onUserShow(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->show = !self->show;
  MSG("show %s", (self->show ? "on" : "off"));
  return 0;
}

int LinePerceptor::onUserSkip(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->skip = !self->skip;
  MSG("skip %s", (self->skip ? "on" : "off"));
  return 0;
}

int LinePerceptor::onUserDebug(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->debug = !self->debug;
  DEBUG_LINES = self->debug;
  MSG("debug %s", (self->debug ? "on" : "off"));
  return 0;
}

int LinePerceptor::onUserStep(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->step = !self->step;
  MSG("step %s", (self->step ? "on" : "off"));
  return 0;
}

int LinePerceptor::onUserExport(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->exportImage();
  return 0;
}



// export current image in stereo blob
int LinePerceptor::exportImage()
{
    CvMat im3 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
		      this->stereoBlob.channels==1 ? CV_8UC1 : CV_8UC3, 
		      this->stereoBlob.imageData + this->stereoBlob.leftOffset);
    
    char str[255];
    sprintf(str, "im-%s-%08d.png", sensnet_id_to_name(this->sensorId),
	    this->stereoBlob.frameId);
    cvSaveImage(str, &im3);
    MSG("Written file: %s", str);

    //write the whole stereoblob structure
    sprintf(str, "blob-%s-%08d.blob", sensnet_id_to_name(this->sensorId),
	    this->stereoBlob.frameId);
    FILE *f;
    if( (f = fopen(str, "w")) == 0)
	return -1;
    if( fwrite(&this->stereoBlob, sizeof(this->stereoBlob), 1, f) != 1)
	return -1;
    fclose(f);
    MSG("Wrote blob: %s", str);

    return 0;
}


//initialize the kalman filter structure
void LinePerceptor::initStopLineTrack()
{

    switch (this->stopLineTrackingModel)
    {
    //end points in local frame
    case 0:
	{
	    //The model used is to track the stop line in local frame
	    //the state will be: (x1,y1,z1,x2,y2,z2) 
	    //and the model is: 
	    // x[k+1]= x[k] + w[k] i.e. A=F=I, B=0
	    // y[k] = x[k] + v[k] i.e. C=I
	    //as the coordinates of the stop line in local frame should
	    //be constant
	    this->kalman = cvCreateKalman(6, 6, 0);
	    
	    //A matrix
	    cvSetIdentity(this->kalman->transition_matrix, cvRealScalar(1));
	    
	    //C matrix
	    cvSetIdentity(this->kalman->measurement_matrix, cvRealScalar(1));
	    
	    //process noise: Rv
	    cvSetIdentity(this->kalman->process_noise_cov, cvRealScalar(1e-1));//e-3 .9e-1 1e-1 for hough
	    
	    //measurement noise: Rw
	    cvSetIdentity(this->kalman->measurement_noise_cov, cvRealScalar(1e-5));//e-6  1e-3 for hough
	    
	    //P0: initial covariance
	    cvSetIdentity(this->kalman->error_cov_pre, cvRealScalar(1));//1
	    
	    //x0: initial state is the current detected line
	    assert(this->lineMsg.lines);
	    float x00[] = {this->lineMsg.lines[0].a[0], this->lineMsg.lines[0].a[1],
			   this->lineMsg.lines[0].a[2], this->lineMsg.lines[0].b[0],
			   this->lineMsg.lines[0].b[1], this->lineMsg.lines[0].b[2]};
	    memcpy(this->kalman->state_pre->data.fl, x00, sizeof(x00));
	}
	
	break;
	
	//r-theta in local frame
    case 1:
	{
	    //The model used is to track the stop line in local frame
	    //the state will be: (r, theta) 
	    //and the model is: 
	    // x[k+1]= x[k] + w[k] i.e. A=F=I, B=0
	    // y[k] = x[k] + v[k] i.e. C=I
	    //as the coordinates of the stop line in local frame should
	    //be constant
	    this->kalman = cvCreateKalman(2, 2, 0);
	    
	    //A matrix
	    cvSetIdentity(this->kalman->transition_matrix, cvRealScalar(1));
	    
	    //C matrix
	    cvSetIdentity(this->kalman->measurement_matrix, cvRealScalar(1));
	    
	    //process noise: Rv
	    cvSetIdentity(this->kalman->process_noise_cov, cvRealScalar(1e-1));//e-1 e-3 .9e-1 1e-1 for hough
	    
	    //measurement noise: Rw
	    cvSetIdentity(this->kalman->measurement_noise_cov, cvRealScalar(1e-3));//e-5 e-6  1e-3 for hough
	    
	    //P0: initial covariance
	    cvSetIdentity(this->kalman->error_cov_pre, cvRealScalar(1));//1
	    
	    //x0: initial state is the current detected line
	    assert(this->lineMsg.lines);
	    float r, theta;
	    Line line = {{this->lineMsg.lines[0].a[0], this->lineMsg.lines[0].a[1]},
			 {this->lineMsg.lines[0].b[0], this->lineMsg.lines[0].b[1]}};
	    mcvLineXY2RTheta(line, r, theta);
	    float x01[] = {r, theta};
	    memcpy(this->kalman->state_pre->data.fl, x01, sizeof(x01));
	}
	
	break;
    }
    
    //msg
    MSG("Track created");
   
}

//destroy the track
void LinePerceptor::destroyStopLineTrack()
{
    if (this->stopLineTrackCreated)
    {
	//release the current track
	cvReleaseKalman(&this->kalman);
	//reset
	this->stopLineTrackCreated = false;
	//msg
	MSG("Track destroyed");
    }
}

//track the detected stop line
void LinePerceptor::updateStopLineTrack()
{

    CvMat im2, *im;

    //check whether to track or not
    if(this->trackStopLines)
    {
	//check if there's currently a measurement and track not created, 
	//then create the track
	if (this->lineMsg.num_lines>0 && !this->stopLineTrackCreated)
	{
	    //increment the number of frames with measurements
	    this->numPreFrames ++;
	    
	    //check if threshold to initiate the track
	    if (this->numPreFrames>NUM_PRE_FRAMES)
	    {
		//init track
		this->initStopLineTrack();
		//track created
		this->stopLineTrackCreated = true;
		//reset
		this->numPreFrames = 0;
		//init post frames
		this->numPostFrames = 0;
	    }
	}
	
	//check if there's a measurement, then correct
	if(this->stopLineTrackCreated)
	{
	    
	    // 	//predict    
	    // 	cvKalmanPredict(this->kalman);
	    
	    if (this->lineMsg.num_lines>0)
	    {
		CvMat y;
		switch (this->stopLineTrackingModel)
		{
		//end points in local frame
		case 0:
		    {
		    //get the measurement
		    assert(this->lineMsg.lines);
		    float yp0[] = {this->lineMsg.lines[0].a[0], this->lineMsg.lines[0].a[1],
				  this->lineMsg.lines[0].a[2], this->lineMsg.lines[0].b[0],
				  this->lineMsg.lines[0].b[1], this->lineMsg.lines[0].b[2]};
		    y = cvMat(6, 1, CV_32FC1, yp0);
		    }
		    break;

		//r-theta in local frame
		case 1:
		    {
		    float r, theta;
		    Line line = {{this->lineMsg.lines[0].a[0], this->lineMsg.lines[0].a[1]},
				 {this->lineMsg.lines[0].b[0], this->lineMsg.lines[0].b[1]}};
		    mcvLineXY2RTheta(line, r, theta);
		    float yp1[] = {r, theta};
		    y = cvMat(2, 1, CV_32FC1, yp1);
		    }
		    break;
		}
		//correct
		cvKalmanCorrect(this->kalman, &y);
		
	    }
	    else
	    {
		//no measurement while trackig, so update the numPostFrames
		this->numPostFrames++;
		
		//check if to destroy the track
		if (this->numPostFrames>NUM_POST_FRAMES)
		{
		    //destroy
		    destroyStopLineTrack();
		    //exit function
		    //return;
		    //reset
		    this->numPreFrames = 0;
		    this->numPostFrames = 0;
		}
	    }
	}
	
	if(this->show)
	{      
	  //get image to export it
	  //uint8_t *pix = StereoImageBlobGetLeft(&this->stereoBlob, 0, 0);
	    im2 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
			CV_8UC1, this->stereoBlob.imageData + this->stereoBlob.leftOffset);
	    im = cvCloneMat(&im2);
	}
	
	//if we still have the track and it wasn't destroyed, then predict
	if(this->stopLineTrackCreated)
	{
	    
	    //predict    
	    cvKalmanPredict(this->kalman);
	    
	    
	    // 	  //store the error
	    // 	  float e[6];
	    // 	  e[0] = this->lineMsg.lines[0].a[0] - this->kalman->state_post->data.fl[0];
	    // 	  e[1] = this->lineMsg.lines[0].a[1] - this->kalman->state_post->data.fl[1];
	    // 	  e[2] = this->lineMsg.lines[0].a[2] - this->kalman->state_post->data.fl[2];
	    // 	  e[3] = this->lineMsg.lines[0].b[0] - this->kalman->state_post->data.fl[3];
	    // 	  e[4] = this->lineMsg.lines[0].b[1] - this->kalman->state_post->data.fl[4];
	    // 	  e[5] = this->lineMsg.lines[0].b[2] - this->kalman->state_post->data.fl[5];	
	    
	    switch (this->stopLineTrackingModel)
	    {
		//end points in local frame
	    case 0:
		
		//		sqrt(e[0]*e[0]+e[1]*e[1]+e[2]*e[2]),
		//sqrt(e[3]*e[3]+e[4]*e[4]+e[5]*e[5]));
		
		//put result back into lineMsg
		this->lineMsg.lines[0].a[0] = this->kalman->state_post->data.fl[0];
		this->lineMsg.lines[0].a[1] = this->kalman->state_post->data.fl[1];
		this->lineMsg.lines[0].a[2] = this->kalman->state_post->data.fl[2];
		this->lineMsg.lines[0].b[0] = this->kalman->state_post->data.fl[3];
		this->lineMsg.lines[0].b[1] = this->kalman->state_post->data.fl[4];
		this->lineMsg.lines[0].b[2] = this->kalman->state_post->data.fl[5];	
		
		MSG("Line LF:(%.2f,%.2f,%.2f)->(%.2f,%.2f,%.2f)", 
		    this->lineMsg.lines[0].a[0], this->lineMsg.lines[0].a[1],
		    this->lineMsg.lines[0].a[2], this->lineMsg.lines[0].b[0],
		    this->lineMsg.lines[0].b[1], this->lineMsg.lines[0].b[2]);

		break;
		//r-theta in local frame
	    case 1:
		
		//intersect the line with the rectangle of the old end points of the line
		Line line;
		Line rect = {{ min(lineMsg.lines[0].a[0], lineMsg.lines[0].b[0]),
			       min(lineMsg.lines[0].a[1], lineMsg.lines[0].b[1]) },
			     { max(lineMsg.lines[0].a[0], lineMsg.lines[0].b[0]),
			       max(lineMsg.lines[0].a[1], lineMsg.lines[0].b[1]) }};
		mcvIntersectLineRThetaWithRect(this->kalman->state_post->data.fl[0], 
					       this->kalman->state_post->data.fl[1],
					       rect, line);

		//put results back into lineMsg
		this->lineMsg.lines[0].a[0] = line.startPoint.x;
		this->lineMsg.lines[0].a[1] = line.startPoint.y;
		//this->lineMsg.lines[0].a[2] = this->stereoBlob.state.localZ + VEHICLE_TIRE_RADIUS;
		this->lineMsg.lines[0].b[0] = line.endPoint.x;
		this->lineMsg.lines[0].b[1] = line.endPoint.y;
		//this->lineMsg.lines[0].b[2] = this->stereoBlob.state.localZ + VEHICLE_TIRE_RADIUS;

		MSG("Line LF:(r=%.4f,theta=%.4f)", 
		    this->kalman->state_post->data.fl[0],
		    this->kalman->state_post->data.fl[1]);

		break;
	    }

	    
	    if(this->show)
	    {
		//convert line to image coordinates
		float px, py, pz;
		float pi, pj, pd;
		Line line;
		//start point
		//StereoImageBlobSensorToVehicle(&this->stereoBlob, 0, 0, 0, &px, &py, &pz);
		//StereoImageBlobVehicleToSensor(&this->stereoBlob, px, py, pz, &px, &py, &pz);
		px = this->lineMsg.lines[0].a[0];
		py = this->lineMsg.lines[0].a[1];
		pz = this->lineMsg.lines[0].a[2];
		StereoImageBlobLocalToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);
		StereoImageBlobVehicleToSensor(&this->stereoBlob, px, py, pz, &px, &py, &pz);
		StereoImageBlobSensorToImage(&this->stereoBlob, px, py, pz, &pi, &pj, &pd);
		line.startPoint.x = pi;
		line.startPoint.y = pj;
		pd *= this->stereoBlob.dispScale;
		//end point
		px = this->lineMsg.lines[0].b[0];
		py = this->lineMsg.lines[0].b[1];
		pz = this->lineMsg.lines[0].b[2];
		StereoImageBlobLocalToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);
		StereoImageBlobVehicleToSensor(&this->stereoBlob, px, py, pz, &px, &py, &pz);
		StereoImageBlobSensorToImage(&this->stereoBlob, px, py, pz, &pi, &pj, &pd);
		line.endPoint.x = pi;
		line.endPoint.y = pj;
		//display line
		// 	    cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
		// 	    assert(im);
		// 	    for (int j = 0; j < this->stereoBlob.rows; j++)
		// 	    {
		// 		for (int i = 0; i < this->stereoBlob.cols; i++)
		// 		{
		// 		    CV_MAT_ELEM(*im, unsigned char, j, i ) = pix[0];
		// 		    //cvmSet(im, j, i,  pix[0]);
		// 		    pix += this->stereoBlob.channels;
		// 		}
		// 	    }
		
		//	    CvMat im2 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1, 
		//		     this->stereoBlob.colorData);
		//CvMat *im = cvCloneMat(&im2);
		
		mcvDrawLine(im, line, CV_RGB(255,0,0), 3);      
	    }
	}
	
	if(this->show)
	{
	    //display image
	    cvNamedWindow("Tracking");
	    cvShowImage("Tracking", im);
	    cvWaitKey(1);
	    //save
	    if(this->save_debug)
	    {
		char str[255];
		sprintf(str, "im-track-%08d.png", this->stereoBlob.frameId);
		cvSaveImage(str, im);
	    }
	    
	    //release
	    cvReleaseMat(&im);
	}
    }
    
}


// Find stop lines
int LinePerceptor::findLines() 
{
    int i, j;
    uint8_t *pix;
    CvMat *im2m;
    //CameraInfo cameraInfo;
    FLOAT_POINT2D focalLength, opticalCenter;
    vector<Line> stopLines, lanes;
    vector<float> lineScores, laneScores, splineScores;
    vector<Spline> splines;
 
    // Reset message object to zero
    memset(&this->lineMsg, 0, sizeof(this->lineMsg)); 

    //get pointer to image data
    pix = this->options.right_camera_flag ? 
	StereoImageBlobGetRight(&this->stereoBlob, 0, 0) :
	StereoImageBlobGetLeft(&this->stereoBlob, 0, 0);
    // Create float matrix for holding image
    im2m = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, FLOAT_MAT_TYPE);

    CvMat rawim, *im;
    //check number of channels of input image
    //
    //grayscale image
    if (this->stereoBlob.channels==1)
    {
	//get image
	rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1, pix);
	//convert to float
	cvConvertScale(&rawim, im2m, 1./255);
    }
    //RGB image
    else
    {
	//get raw image
	rawim = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3, pix);
	//convert to single channel
	im = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
	cvCvtColor(&rawim, im, CV_BGR2GRAY); 
	//get yellow channel
	CvMat *imr = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
// 	CvMat* img = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
// 	CvMat* imb = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC1);
	cvSplit(&rawim, imr, NULL, NULL, NULL);
// 	cvSplit(&rawim, NULL, img, NULL, NULL);
// 	cvScale(imr, imr, 1, 0);
// 	cvScale(img, img, 0, 0);
// 	cvAdd(imr, img, imr);
// 	cvScale(im, im, 0);
// 	cvScale(imr, imr, 1);
// 	cvAdd(imr, im, im);
	cvCopy(imr, im);
	cvReleaseMat(&imr);
// 	cvReleaseMat(&img);
// 	cvReleaseMat(&imb);

	//convert to float
	cvConvertScale(im, im2m, 1./255);
	//destroy
	cvReleaseMat(&im);
// 	if (this->show)
// 	    SHOW_IMAGE(im2m, "Processed image", 1);
    }
	
// 	// Copy data into float matrix.
// 	// TODO this only works properly for mono images.
// 	for (j = 0; j < this->stereoBlob.rows; j++)
// 	{
// 	    for (i = 0; i < this->stereoBlob.cols; i++)
// 	    {
// 		cvmSet(im2m, j, i, (float) pix[0] / 255);
// 		pix += this->stereoBlob.channels;
// 	    }
// 	}

    // Construct camera parameters
    if (this->options.right_camera_flag)
    {
	focalLength = cvPoint2D32f(this->stereoBlob.rightCamera.sx, this->stereoBlob.rightCamera.sy);
	opticalCenter = cvPoint2D32f(this->stereoBlob.rightCamera.cx, this->stereoBlob.rightCamera.cy);
    }
    else
    {
	focalLength = cvPoint2D32f(this->stereoBlob.leftCamera.sx, this->stereoBlob.leftCamera.sy);
	opticalCenter = cvPoint2D32f(this->stereoBlob.leftCamera.cx, this->stereoBlob.leftCamera.cy);
    }
    this->cameraInfo.focalLength = focalLength;
    this->cameraInfo.opticalCenter = opticalCenter;
    this->cameraInfo.imageWidth = this->stereoBlob.cols;
    this->cameraInfo.imageHeight = this->stereoBlob.rows;
    
    // Assume that camera height is in mm.
    // TODO: check that the yaw convention is correct.
    pose3_t pose;
    double rx, ry, rz;

    if (this->options.right_camera_flag)
	    pose = pose3_from_mat44f(this->stereoBlob.rightCamera.sens2veh);
    else
	pose = pose3_from_mat44f(this->stereoBlob.leftCamera.sens2veh);
    //   pose = pose3_inv(pose);
    vec3_t y = vec3_set(0, 1, 0);
    vec3_t z = vec3_set(0, 0, 1);
    
    vec3_t ty = vec3_rotate(pose.rot, y);
    double dot = vec3_dot(z, ty);
    this->cameraInfo.pitch = acos(dot);
    
    quat_to_rpy(pose.rot, &rx, &ry, &rz);
    this->cameraInfo.cameraHeight = -1000 * pose.pos.z + 1000 * VEHICLE_TIRE_RADIUS;
    //   this->cameraInfo.pitch = rx; //malaa: -ry
    //this->cameraInfo.yaw = ry; //malaa: rz
    
    MSG("Pitch=%f & height=%f", this->cameraInfo.pitch*180/3.14, this->cameraInfo.cameraHeight);
    
    MSG("Processing frame#%d", this->stereoBlob.frameId);

    if(!this->skip)
    {
	//used for statistics
	static long numStoplineFrames=0, numLaneFrames=0;
	static double totalStoplineTime=0.0, totalLaneTime=0.0;
	double stoplineTime, laneTime;

	//get stop lines
	if (!this->noStoplines)
	{
	    // Search for lines
	    int64 startTime, endTime;    
	    startTime = cvGetTickCount();
	    mcvGetStopLines(im2m, &stopLines, &lineScores,  &cameraInfo, &this->stopLineConf);
	    endTime = cvGetTickCount();
	    
	    //stats
	    numStoplineFrames++;
	    stoplineTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
	    totalStoplineTime += stoplineTime;

	    //display
	    if (this->console)
		cotk_printf(this->console, "%stoplineStats%", A_NORMAL, 
			    "%.2fms\tAvg:%.2fms\t%.2fHz", stoplineTime, 
			    totalStoplineTime/numStoplineFrames, 
			    numStoplineFrames/totalStoplineTime*1000);
	    else
		MSG("Stop line takes %.2fms\tavg %.2fms\t%.2fHz", stoplineTime,
		    totalStoplineTime/numStoplineFrames, 
		    numStoplineFrames/totalStoplineTime*1000);
	    //display coordinates
	    if (stopLines.size()>0)
		MSG("Stopline: %.2f (%.2f,%.2f)->(%.2f->%.2f)", lineScores[0], 
		    stopLines[0].startPoint.x, stopLines[0].startPoint.y,
		    stopLines[0].endPoint.x, stopLines[0].endPoint.y);
	    //check if to track 
	    if (this->trackStopLines)
	    {

		//get measurement tracks
		vector<Track*> stoplineTracks = 
		    LineTrack::CreateLineTracks(stopLines, lineScores,
						this->stereoBlob.frameId);
		//update
		this->stoplineTracker.update (stoplineTracks, this->stereoBlob.frameId);
		//send to map
		this->stoplineTracker.sendToMap(this->mapElementTalker,
						&this->stereoBlob);
		//clean up
		this->stoplineTracker.clean();
		Track::DestroyTracks(stoplineTracks);
						    
	    }
	}
	
	//get lanes
	if (!this->noLanes)
	{
	    static LineState laneState;
	    // Search for lines
	    int64 startTime, endTime;    
	    startTime = cvGetTickCount();
	    mcvGetLanes(im2m, &lanes, &laneScores, &splines, &splineScores, 
			&cameraInfo, &this->laneConf, &laneState);
	    endTime = cvGetTickCount();

	    //stats
	    numLaneFrames++;
	    laneTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
	    totalLaneTime += laneTime;

	    //display
	    if (this->console)
		cotk_printf(this->console, "%laneStats%", A_NORMAL, 
			    "%.2fms\tAvg:%.2fms\t%.2fHz", laneTime, 
			    totalLaneTime/numLaneFrames, 
			    numLaneFrames/totalLaneTime*1000);
	    else
		MSG("Lane line takes %.2fms\tavg %.2fms\t%.2fHz", laneTime,
		    totalLaneTime/numLaneFrames, numLaneFrames/totalLaneTime*1000);

	    //display coordinates
	    if (this->laneConf.ransacLine || !this->laneConf.ransacSpline)
		for(int i=0; i<(int)lanes.size(); i++)	    
		    MSG("Lane: %.2f (%.2f,%.2f)->(%.2f->%.2f)", laneScores[i], 
			lanes[i].startPoint.x, lanes[i].startPoint.y,
			lanes[i].endPoint.x, lanes[i].endPoint.y);	
	    if (this->laneConf.ransacSpline)
		for(int i=0; i<(int)splines.size(); i++)	    
		    MSG("Lane: %.2f ", splineScores[i]);

	    //check if to track 
	    if (this->trackLanes)
	    {

		//get measurement tracks
		vector<Track*> laneSplineTracks = 
		    SplineTrack::CreateSplineTracks(splines, splineScores,
						    this->stereoBlob.frameId);
		//update
		this->laneSplineTracker.update (laneSplineTracks, this->stereoBlob.frameId);
		//send to map
		this->laneSplineTracker.sendToMap(this->mapElementTalker,
						  &this->stereoBlob);
		//clean up
		this->laneSplineTracker.clean();
		Track::DestroyTracks(laneSplineTracks);
						    
	    }
	}
    }

    //show line or not
    if(this->show || this->save)
    {


	
	//CvMat im2 = cvMat(this->stereoBlob.rows, this->stereoBlob.cols, 
	//			CV_8UC1, this->stereoBlob.imageData + this->stereoBlob.leftOffset);
	CvMat *imDisplay;
	imDisplay = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, CV_8UC3);
	if(this->stereoBlob.channels == 1)
	    cvCvtColor(&rawim, imDisplay, CV_GRAY2RGB);
	else
	    //imDisplay = cvCloneMat(&rawim);
	    //CvMat expects them in BGR format (Blue first) while
	    //they are in RGB format in stereoblob (Red first) and so we reverse them 
	    //here
	    cvCvtColor(&rawim, imDisplay, CV_BGR2RGB);

// 	if (this->show)
// 	    SHOW_IMAGE(imDisplay, "Raw image", 1);
	
	
	if(this->show || this->save_debug)
	{
	    if(stopLines.size()>0)
		//display stop line
		mcvDrawLine(imDisplay, stopLines[0], CV_RGB(255,0,0), 3);
	    if (this->laneConf.ransacLine || !this->laneConf.ransacSpline)
		for(int i=0; i<(int)lanes.size(); i++)
		    mcvDrawLine(imDisplay, lanes[i], CV_RGB(0,125,0), 3);	  
	    if (this->laneConf.ransacSpline)
		for(int i=0; i<(int)splines.size(); i++)
		    mcvDrawSpline(imDisplay, splines[i], CV_RGB(0,255,0), 3);

	    //tracked stoplines
	    //tracked lanes
	    if (this->trackStopLines)
	    {
		for (int i=0; i<(int)this->stoplineTracker.tracks.size(); i++)
		{
		    LineTrack* lt = (LineTrack*)stoplineTracker.tracks[i];
		    if (!lt->valid)
			mcvDrawLine(imDisplay, lt->line, CV_RGB(255,255,0), 3);
		}
		for (int i=0; i<(int)this->stoplineTracker.tracks.size(); i++)
		{
		    LineTrack* lt = (LineTrack*)stoplineTracker.tracks[i];
		    if (lt->valid)
			mcvDrawLine(imDisplay, lt->line, CV_RGB(255,0,0), 3);
		}
	    }
	    //tracked lanes
	    if (this->trackLanes)
	    {
		for (int i=0; i<(int)this->laneSplineTracker.tracks.size(); i++)
		{
		    SplineTrack* st = (SplineTrack*)laneSplineTracker.tracks[i];
		    if (!st->valid)
			mcvDrawSpline(imDisplay, st->spline, CV_RGB(0,255,255), 3);
		}
		for (int i=0; i<(int)this->laneSplineTracker.tracks.size(); i++)
		{
		    SplineTrack* st = (SplineTrack*)laneSplineTracker.tracks[i];
		    if (st->valid)
			mcvDrawSpline(imDisplay, st->spline, CV_RGB(0,0,255), 3);
		}

	    }
	}
	
	
	//save image with detected lines
	if(this->save_debug)
	{
	    char str[255];
	    sprintf(str, "im-detect-%08d.png", this->stereoBlob.frameId);
	    cvSaveImage(str, imDisplay);
	}
	//show detected lines
	if (this->show)
	{
	    char str[256];
	    sprintf(str, "%s", this->options.module_id_arg);
	    cvNamedWindow(str, 0);
	    cvShowImage(str,imDisplay); //im2m
	    
	    int key=cvWaitKey( this->step ? 0 : 10);
	    if (key=='e' || key=='E')
		this->exportImage();
	    else if (key == 'q' || key == 'Q')
		this->onUserQuit(console, this, "");
	    else if (key == 'p' || key == 'P')
		this->onUserPause(console, this, "");
	    else if (key == 't' || key=='T')
		this->onUserStep(console, this, "");
	    //       else if (key == 'k' || key == 'K') //skip	 
	    // 	  return 0;
	}
	//not showing, so export the image
	if (this->save)
	{
	    this->exportImage();
	}
	
	//clear
	cvReleaseMat(&imDisplay);
    }
    else
    {
	//delete window
	cvDestroyWindow("window");
	cvWaitKey(10);
    }
    
    //get line score
    if((int) lineScores.size()>0)
    {
	lineScore = lineScores[0];
    }
    else
	lineScore = -1;
    
    //   Line line;
    //   float pi, pj, pd;
    //   float px, py, pz;
    
//     //put detected stop lines in message    
//     this->lineMsg.num_lines = 0;
//     for (i = 0; i<(int)stopLines.size(); i++)
//     {
// 	if (i >= (int) (sizeof(this->lineMsg.lines) / sizeof(this->lineMsg.lines[0])))
// 	    break;
	
// 	// TODO: use camera model rather than stereo (which may not be defined)
	
// 	//get line in local frame using stereo
// 	ImageLineToLocalLine(stopLines[i], this->lineMsg, i);
// 	this->lineMsg.num_lines++;    
//     }
    
    //put detected lanes in message    
    this->laneMsg.num_lines = 0;
    if (this->laneConf.ransacLine || !this->laneConf.ransacSpline)
	for (i = 0; i<(int)lanes.size(); i++)
	{
	    if (i >= (int) (sizeof(this->laneMsg.lines) / sizeof(this->laneMsg.lines[0])))
		break;
	    
	    // TODO: use camera model rather than stereo (which may not be defined)
	  
	    //get line in local frame using stereo
	    ImageLineToLocalLine(lanes[i], this->laneMsg, i);
	    this->laneMsg.num_lines++;
	}
//     if (this->laneConf.ransacSpline && !this->trackLanes)
// 	for (i = 0; i<(int)splines.size(); i++)
// 	    sendImageSpline(splines[i], i);
    
    // Record some stats
    this->totalStops += stopLines.size();
    
    cvReleaseMat(&im2m);
    stopLines.clear(); 
    lanes.clear();
    lineScores.clear();
    laneScores.clear();
    splineScores.clear();
    splines.clear();
    
    return 0;
}


//converts from a line in image coordinates to a line in local coordinates
int LinePerceptor::ImageLineToLocalLine(const Line &imageLine, RoadLineMsg &msg, int index)
{
    float pi, pj, pd;
    float px, py, pz;

    //start point
    pi = imageLine.startPoint.x;
    pj = imageLine.startPoint.y;
    pd = (float) (*StereoImageBlobGetDisp(&this->stereoBlob, (int) pi, (int) pj));
    pd /= this->stereoBlob.dispScale;
    // Compute point in the sensor frame
    if (this->options.right_camera_flag)
	StereoImageBlobImageToSensorRight(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);
    else
	StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);
    // Convert to vehicle frame
    if (this->options.right_camera_flag)
	StereoImageBlobSensorToVehicleRight(&this->stereoBlob, px, py, pz, &px, &py, &pz);
    else
	StereoImageBlobSensorToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);
    // Convert to local frame
    StereoImageBlobVehicleToLocal(&this->stereoBlob, px, py, pz, &px, &py, &pz);      
    msg.lines[index].a[0] = px;
    msg.lines[index].a[1] = py;
    msg.lines[index].a[2] = pz;

    //end point
    pi = imageLine.endPoint.x;
    pj = imageLine.endPoint.y;
    pd = (float) (*StereoImageBlobGetDisp(&this->stereoBlob, (int) pi, (int) pj));
    pd /= this->stereoBlob.dispScale;
    // Compute point in the sensor frame
    if (this->options.right_camera_flag)
	StereoImageBlobImageToSensorRight(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);
    else
	StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);
    // Convert to vehicle frame
    if (this->options.right_camera_flag)
	StereoImageBlobSensorToVehicleRight(&this->stereoBlob, px, py, pz, &px, &py, &pz);
    else
	StereoImageBlobSensorToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);
    // Convert to local frame
    StereoImageBlobVehicleToLocal(&this->stereoBlob, px, py, pz, &px, &py, &pz);      
    msg.lines[index].b[0] = px;
    msg.lines[index].b[1] = py;
    msg.lines[index].b[2] = pz;

    return 0;
}

//converts from a spline in image coordinates to a line in local coordinates
int LinePerceptor::sendImageSpline(Spline &imageSpline, int index)
{
    float px, py, pz;

    //convert spline to points
    CvMat *splinePoints = mcvEvalBezierSpline(imageSpline, .05);

    //points to send to map
    point2arr points;

    //loop on points and convert to local frame
    for(int i=0; i<splinePoints->height; i++)
    {	
	//get point in local frame
	if (this->options.right_camera_flag)
	    StereoImageBlobImageToLocalRight(&this->stereoBlob,
					     (float)cvGetReal2D(splinePoints, i, 0), 
					     (float)cvGetReal2D(splinePoints, i, 1), 
					     &px, &py, &pz);
	else
	    StereoImageBlobImageToLocal(&this->stereoBlob,
					(float)cvGetReal2D(splinePoints, i, 0), 
					(float)cvGetReal2D(splinePoints, i, 1), 
					&px, &py, &pz);
	//add point to map points
	points.push_back(point2(px, py));
    }

    //send points
    //fill map element
    this->mapElement.clear();
    this->mapElement.setId(-30-index);
    this->mapElement.setGeometry(points);
    this->mapElement.height=0;
    this->mapElement.plotColor = MAP_COLOR_RED;
    this->mapElement.type = ELEMENT_LANELINE;
    this->mapElement.geometryType = GEOMETRY_LINE;
    this->mapElement.state = this->stereoBlob.state;
    //send to map
    this->mapElementTalker.sendMapElement(&this->mapElement, 0);
    //clear
    points.clear();
    this->mapElement.clear();
    cvReleaseMat(&splinePoints);

    return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LinePerceptor *percept;


  percept = new LinePerceptor();
  assert(percept);

  memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  int frameId = 0;  
  long numFrames = 0;
  double lineTime = 0, totalLineTime = 0;

  while (!percept->quit)
  {
    //delay for a while
    if (percept->replay)
    {    
	//usleep(500);
    }


    // Update the console
    if (percept->console)
      cotk_update(percept->console);
    
    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Check for new data
    if (percept->readImage() == 0)
    {

	//check if goto frame given
	if (percept->gotoFrame!=-1 && 
	    percept->stereoBlob.frameId < percept->gotoFrame)
	    continue;	    

	// Detect lines
	int64 startTime, endTime;    
	startTime = cvGetTickCount();
	percept->findLines();
	endTime = cvGetTickCount();

	//stats
	numFrames++;
	lineTime = 0.001*(endTime-startTime)/cvGetTickFrequency();
	totalLineTime += lineTime;
	//display
	if (percept->console)
	    cotk_printf(percept->console, "%overallStats%", A_NORMAL, 
			"%.2fms\tAvg:%.2fms\t%.2fHz", lineTime, 
			totalLineTime/numFrames, 
			numFrames/totalLineTime*1000);
	else
	    MSG("Overall takes %.2fms\tavg %.2fms\t%.2fHz", lineTime, 
		totalLineTime/numFrames, 
		numFrames/totalLineTime*1000);

	//track lines
	//percept->updateStopLineTrack();


	//send Alice to map
	percept->mapElement.type = ELEMENT_ALICE;
	percept->mapElement.state = percept->stereoBlob.state;
	percept->mapElementTalker.sendMapElement(&percept->mapElement, 0);
	percept->mapElement.clear();


	//if (percept->lineMsg.num_lines >1)
	//	percept->lineMsg.num_lines = 1;
	
	percept->writeLines(); 
	memset(&percept->lineMsg, 0, sizeof(percept->lineMsg)); 
    }
    
    // Write message
  

//     // Update the display
//     if (percept->console)
//     {
// 	cotk_printf(percept->console, "%stops%", A_NORMAL, "%d (%d fakes)",
//                   percept->totalStops, percept->totalFakes);
//     }

    // Do heartbeat occasionally
    //if (percept->stereoBlob.frameId % 2 == 0)
    percept->getProcessState();

  } //end while

  percept->finiConsole();
  percept->finiSensnet();
  percept->destroyStopLineTrack();

  MSG("exited cleanly");
  
  return 0;
}



