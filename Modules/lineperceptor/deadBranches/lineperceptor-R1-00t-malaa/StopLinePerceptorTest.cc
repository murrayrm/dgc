
#include <iostream>
#include <vector>
#include <math.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <sstream>

using namespace std;

#include "cv.h"
#include "highgui.h"

#include "mcv.hh"
#include "InversePerspectiveMapping.hh"
#include "StopLinePerceptor.hh"

//#include "octave/oct.h"
#include "ranker.h"

int main(int argc, char **argv)
{ 
    char cwd[256];
    getcwd(cwd, 256);
    cout << "Current directory:" << cwd << "\n";
    /* 
   int sz=20;
   Matrix m = Matrix(sz,sz);
   for (int r=0;r<sz;r++) 
   {
     for (int c=0;c<sz;c++) 
     {
       m(r,c)=r*c;
     }
   }
   cout << "Hello world! " << endl;
   cout << m;
  
   exit(1); 
    */
    /*
    //images
    IplImage *im1, *im2, *im3;
    CvMat im1m, im2m, im3m;
    
    //read an image
    im1 = cvLoadImage("/home/malaa/College/dgc/projects/malaa/stopLinePerceptor/stopa05.jpg", CV_LOAD_IMAGE_GRAYSCALE);
    //cvGetMat(im1, &im1m);
    //cout << "Size:" << im1->width << "x" << im1->height << "\n";
    //cout << "Number of bytes=" << im1->imageSize << "?=" << 3*im1->width*im1->height << "\n";
    //SHOW_IMAGE(&im1m, "w1");
    
    //convert to float
    im2 = cvCreateImage(cvSize(im1->width,im1->height), FLOAT_IMAGE_TYPE, 1);
    cvGetMat(im2, &im2m);
    mcvMatInt2Float(&im1m, &im2m);
        //cvCvtColor(im1, im2, CV_RGB2GRAY);
        //cvConvertImage(im1, im2);
    cout << "Size:" << im2->width << "x" << im2->height << "\n";
    //cvNamedWindow("w1", 1); cvShowImage("w1", im2); cvWaitKey(0);
    
    //create mat for ipm
    im3 = cvCreateImage(cvSize(160,120), FLOAT_IMAGE_TYPE, 1);
    cvGetMat(im3, &im3m);
    //points
    CameraInfo cameraInfo;
    FLOAT_POINT2D focalLength = cvPoint2D32f(700, 700);
    FLOAT_POINT2D opticalCenter = cvPoint2D32f(320, 240);
    IPMInfo ipmInfo;
    cameraInfo.focalLength = focalLength;
    cameraInfo.opticalCenter = opticalCenter;
    cameraInfo.yaw = 0*CV_PI/180;
    cameraInfo.pitch = 20*CV_PI/180;
    cameraInfo.height = 700;
    mcvGetIPM(&im2m, &im3m, &ipmInfo, focalLength, opticalCenter, 
        710, 20*CV_PI/180, 0);    
    SHOW_IMAGE(&im3m, "w3");       
    
     
     
    //test the filter function
    CvMat *im4m;
    im4m = cvCloneMat(&im3m);
    mcvFilterLines(&im3m, im4m, 2, 2, 10,2,FILTER_LINE_HORIZONTAL);
    SHOW_IMAGE(im4m, "w4");      
    //SHOW_MAT(im4m);
    
    //get rid of below zero
    mcvThresholdLower(im4m, im4m, 0);
    
    //threshold with the quantile
    FLOAT qtile = mcvGetQuantile(im4m, .98);
    cout << "Quantile:" << qtile << "\n";
    mcvThresholdLower(im4m, im4m, qtile);
    
    //test the grouping function
    CvMat *im5m = cvCloneMat(&im3m);
    vector <Line> lines;
    vector <FLOAT> lineScores;
    mcvGetHVLines(im4m, &lines, &lineScores, HV_LINES_HORIZONTAL, 
        12*25.4*ipmInfo.yScale, false, false, 1);
    for (int i=0; i<lines.size(); i++)
    {
        cout << "i=" << i << "\n";
        SHOW_POINT(lines[i].startPoint, "start");
        SHOW_POINT(lines[i].endPoint, "end");
        cvLine(im5m, cvPoint((int)lines[i].startPoint.x,(int)lines[i].startPoint.y),
            cvPoint((int)lines[i].endPoint.x,(int)lines[i].endPoint.y), CV_RGB(255,0,0));
    }        
    SHOW_IMAGE(im5m, "w5");
    cout << "hi\n";
    
    //convert the line into World coordinates
    for (int i=0; i<lines.size(); i++)
    {
         mcvPointImIPM2World(&(lines[i].startPoint), &ipmInfo);
         mcvPointImIPM2World(&(lines[i].endPoint), &ipmInfo);
    }
    
    SHOW_POINT(lines[0].startPoint, "start");
    SHOW_POINT(lines[0].endPoint, "end");
    */
    
    //test getStopLines
    string str;
    //cout << "Enter image to test:";
    //cin >> str; //"/home/malaa/College/dgc/projects/malaa/stopLinePerceptor/stopa05.jpg" str.c_str()
    IplImage *im1, *im2;;
    if (argv[1])
    {
        im1 = cvLoadImage(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
	cout << "Image:" << argv[1] << "\n";
    }
    else
        im1 = cvLoadImage("stopa05.jpg", CV_LOAD_IMAGE_GRAYSCALE);
    //resize it to 256x192
    //im2 = cvCreateImage(cvSize(256, 192), IPL_DEPTH_8U, 1);
    //cvResize(im1, im2);
    im2 = im1;
    //SHOW_IMAGE(im2, "im2");
    //make mat
    CvMat *im1m = cvCreateMat(im2->height, im2->width, INT_MAT_TYPE);
    cvGetMat(im2, im1m);
    //convert to float
    CvMat *im2m = cvCreateMat(im2->height,im2->width, FLOAT_MAT_TYPE);
    //cvGetMat(im2m, im1m);
    mcvMatInt2Float(im1m, im2m);    
    //required data
    //get config path
    char configPath[256];
    char filename[256];
    if (getenv("DGC_CONFIG_PATH"))
	snprintf(configPath, sizeof(configPath),
		 "%s/lineperceptor", getenv("DGC_CONFIG_PATH"));
    else
    {
	cerr << "unknown configuration path: please set DGC_CONFIG_PATH";    
	return -1;
    }
    
    //get camera info
    CameraInfo cameraInfo;
    snprintf(filename, sizeof(filename), "%s/%s",
	     configPath, "CameraInfo.conf");
    mcvInitCameraInfo(filename, &cameraInfo);
    //scale it according to the input iamge
    mcvScaleCameraInfo(&cameraInfo, cvSize(im2->width, im2->height));    
    //get stop lines conf
    StopLinePerceptorConf stopLineConf, laneConf;
    snprintf(filename, sizeof(filename), "%s/%s",
	     //configPath, "StopLinePerceptor.conf");
	     configPath, "LanePerceptor.conf");
    mcvInitStopLinePerceptorConf(filename, &laneConf);    
    snprintf(filename, sizeof(filename), "%s/%s",
	     //configPath, "StopLinePerceptor.conf");
	     configPath, "StopLinePerceptor.conf");
    mcvInitStopLinePerceptorConf(filename, &stopLineConf);    
    //cout << "Binarize: " << stopLineConf.binarize << "\n";
    //get the stop lines
    vector<Line> stopLines, lanes;
    vector<float> stoplineScores, laneScores, splineScores;
    vector<Spline> splines;
    int64 startTime, endTime;    
    startTime = cvGetTickCount();
    cout << "Testing mcvGetStopLines\n";
    mcvGetStopLines(im2m, &stopLines, &stoplineScores, &cameraInfo, &stopLineConf);
    cout << "Testing mcvGetLanes\n";
    mcvGetLanes(im2m, &lanes, &laneScores, &splines, &splineScores, &cameraInfo, &laneConf);
    endTime = cvGetTickCount();
    //for (int i=0; i<(int)stopLines.size(); i++)
    for (int i=0; i<(int)stopLines.size(); i++)
    {
        string str;
        stringstream ss, ss2;
        ss << i+1;
        str = "Line #" + ss.str();
	ss2 << stoplineScores[i];
	str += "(" + ss2.str() + "):";
        SHOW_LINE(stopLines[i], (char*) str.c_str());
    }
    if (laneConf.ransacLine || !laneConf.ransacSpline)
	for (int i=0; i<(int)lanes.size(); i++)
	{
	    string str;
	    stringstream ss, ss2;
	    ss << i+1;
	    str = "Line #" + ss.str();
	    ss2 << laneScores[i];
	    str += "(" + ss2.str() + "):";
	    SHOW_LINE(lanes[i], (char*) str.c_str());
	}
    if (laneConf.ransacSpline)
	for (int i=0; i<(int)splines.size(); i++)
	{
	    string str;
	    stringstream ss, ss2;
	    ss << i+1;
	    str = "Line #" + ss.str();
	    ss2 << splineScores[i];
	    str += "(" + ss2.str() + "):";
	    cout << str << "\n";
	    //	    SHOW_LINE(lanes[i], (char*) str.c_str());
	}
	
    cout << "Takes:" << 0.001*(endTime-startTime)/cvGetTickFrequency() << " ms\n";
    //show detected line
    for (int i=0; i<(int)stopLines.size(); i++)
    {
	mcvDrawLine(im2m, stopLines[i], CV_RGB(255,0,0), 3);
    }    
    if (laneConf.ransacLine || !laneConf.ransacSpline)
	for (int i=0; i<(int)lanes.size(); i++)
	    mcvDrawLine(im2m, lanes[i], CV_RGB(255,0,0), 3);
    if (laneConf.ransacSpline)
	for (int i=0; i<(int)splines.size(); i++)
	    mcvDrawSpline(im2m, splines[i], CV_RGB(255,0,0), 3);
    SHOW_IMAGE(im2m, "Detected lines", 0);
	
    cvReleaseImage(&im1);
    //cvReleaseImage(&im2);
    //cvReleaseMat(&im1m);
    cvReleaseMat(&im2m);
    //cin.get();
    /*for (int i=0; i<lines.size(); i++)
    {
        cout << "i=" << i << "\n";
        SHOW_POINT(stopLines[i].startPoint, "start");
        SHOW_POINT(stopLines[i].endPoint, "end");
        //cvLine(im5m, cvPoint((int)lines[i].startPoint.x,(int)lines[i].startPoint.y),
        //    cvPoint((int)lines[i].endPoint.x,(int)lines[i].endPoint.y), CV_RGB(255,0,0));
    } */      
        
    
    //call
    //cvGetIPM(im1, im2, focalLength, opticalCenter, 
    //  700, 20, 0);
    //CvMat *
    
    //trasnformation functions
    /*FLOAT_MAT_ELEM_TYPE inp[] = {400, 100, 350, 100};
    CvMat in = cvMat(2, 2, FLOAT_MAT_TYPE, inp);
    CvMat *out = cvCreateMat(2, 2, FLOAT_MAT_TYPE);
    CvMat *in2 = cvCreateMat(2, 2, FLOAT_MAT_TYPE);
    SHOW_MAT(&in, "in:");
    mcvTransformImage2Ground(&in, out,
        focalLength, opticalCenter, 700, 20*CV_PI/180, 0);
    SHOW_MAT(out, "out:");
    mcvTransformGround2Image(out, in2,
        focalLength, opticalCenter, 700, 20*CV_PI/180, 0);
    SHOW_MAT(in2, "in2:");
    
    //get vanishing point
    FLOAT_POINT2D vp = mcvGetVanishingPoint(focalLength, 
        opticalCenter, 700, 20*CV_PI, 0);
    cout << "VP=(" << vp.x << "," << vp.y <<")\n"; 
    
    cvReleaseMat(&out);
    cvReleaseMat(&in2);/*/
    
    /*
    cvReleaseImage(&im1);
    cvReleaseImage(&im2);
    cvReleaseImage(&im3);
    */
    
    exit(1);
}


