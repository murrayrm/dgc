
/* 
 * Desc: Line perceptor run-time 
 * Date: 26 November 2006
 * Author: Andrew Howard, Mohamed Aly
 * CVS: $Id$
*/

#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <skynet/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineMsg.h>

// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"
#include "InversePerspectiveMapping.hh"
#include "StopLinePerceptor.hh"


// Line perceptor module
class LinePerceptor
{
  public:

  // Constructor
  LinePerceptor();

  // Destructor
  ~LinePerceptor();

  public:
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);

  public:
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Read the current image
  int readImage();

  // Write the current stop lines
  int writeLines();

  public:

  // Find stop lines
  int findStopLines();

  public:

  // Initialize sparrow display
  int initConsole();

  // Finalize sparrow display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserFake(cotk_t *console, LinePerceptor *self, const char *token);

  /* TODO  
  // Sparrow callbacks; occurs in sparrow thread
  static int onUserShow(long);
  */

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  // Source sensor id
  sensnet_id_t sensorId;

  // Sensnet module
  sensnet_t *sensnet;

  // Console display
  cotk_t *console;

  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //should we show the detected line?
  bool show;

  // Current blob id
  int blobId;
  
  // Current (incoming) stereo blob
  StereoImageBlob stereoBlob;

  // Current (outgoing) line message
  RoadLineMsg lineMsg;

  // Diagnostics on stop lines
  int totalStops, totalFakes;
    
  //stop line conf strucuture
  StopLinePerceptorConf stopLineConf;
  //camera info strucure
  CameraInfo cameraInfo;
  //line score
  float lineScore;

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
LinePerceptor::LinePerceptor()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
LinePerceptor::~LinePerceptor()
{
  return;
}


// Parse the command line
int LinePerceptor::parseCmdLine(int argc, char **argv)
{
  // Default configuration path
  char defaultConfigPath[256];
  char filename[256];

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

    // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(defaultConfigPath, sizeof(defaultConfigPath),
             "%s/lineperceptor", getenv("DGC_CONFIG_PATH"));
  else
    return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
    
  // Fill out (source) sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  //read the stop line perceptor conf
  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "StopLinePerceptor.conf");
  mcvInitStopLinePerceptorConf(filename, &this->stopLineConf);
  
  //read camera info
  snprintf(filename, sizeof(filename), "%s/%s",
           defaultConfigPath, "CameraInfo.conf");
  mcvInitCameraInfo(filename, &this->cameraInfo);    
  
  return 0;
}



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LinePerceptor $Revision$                                                   \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"Stops : %stops%                                                            \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%]                                                    \n";

// Initialize console display
int LinePerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
    
  // Initialize the display
  cotk_open(this->console);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, "%s", sensnet_id_to_name(this->sensorId));

  return 0;
}


// Finalize sparrow display
int LinePerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize sensnet
int LinePerceptor::initSensnet()
{
  // Start sensnet
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;
  
  // Join stereo group
  if (sensnet_join(this->sensnet, this->sensorId,
                   SENSNET_STEREO_BLOB, sizeof(StereoImageBlob), 5) != 0)
    return -1;

  return 0;
}


// Clean up sensnet
int LinePerceptor::finiSensnet()
{
  sensnet_leave(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB);
  sensnet_free(this->sensnet);
  
  return 0;
}


// Read the current image
int LinePerceptor::readImage()
{
  int blobId, blobLen;

  // Take a peek at the latest data
  if (sensnet_peek(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB, &blobId, &blobLen) != 0)
    return -1;

  // Do we have new data?
  if (blobId < 0 || blobId == this->blobId)
    return -1;
  this->blobId = blobId;
  
  // Read the new blob
  if (sensnet_read(this->sensnet, this->sensorId,
                   SENSNET_STEREO_BLOB, blobId, blobLen, &this->stereoBlob) != 0)
    return -1;

  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%sensor%", A_NORMAL, "%s %d %8.3f",
                sensnet_id_to_name(this->sensorId),
                this->stereoBlob.frameId,
                fmod((double) this->stereoBlob.timestamp * 1e-6, 10000));
  }

  return 0;
}


// Write the current stop lines
int LinePerceptor::writeLines()
{
  if (this->lineMsg.num_lines == 0)
    return 0;
        
  // Prepare the roadline message
  memset(&this->lineMsg, 0, sizeof(this->lineMsg));
  this->lineMsg.msg_type = SNroadLine;
  this->lineMsg.frameid = this->stereoBlob.frameId;
  this->lineMsg.timestamp = this->stereoBlob.timestamp;
  this->lineMsg.state = this->stereoBlob.state;

  // Send line data
  sensnet_write(this->sensnet, SENSNET_SKYNET_SENSOR,
                this->lineMsg.msg_type, 0, sizeof(this->lineMsg), &this->lineMsg);
  
  return 0;
}    


// Handle button callbacks
int LinePerceptor::onUserQuit(cotk_t *console, LinePerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LinePerceptor::onUserPause(cotk_t *console, LinePerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



// Handle fake stop button
int LinePerceptor::onUserFake(cotk_t *console, LinePerceptor *self, const char *token)
{
  float px, py, pz;
    
  MSG("creating fake stop line data");

  self->lineMsg.num_lines = 0;
      
  // MAGIC
  px = VEHICLE_LENGTH + 3;
  py = -2;
  pz = VEHICLE_TIRE_RADIUS;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);

  self->lineMsg.lines[0].a[0] = px;
  self->lineMsg.lines[0].a[1] = py;
  self->lineMsg.lines[0].a[2] = pz;

  px = VEHICLE_LENGTH + 3;
  py = +2;
  pz = 0.5;

  // Convert to local frame      
  StereoImageBlobVehicleToLocal(&self->stereoBlob, px, py, pz, &px, &py, &pz);
      
  self->lineMsg.lines[0].b[0] = px;
  self->lineMsg.lines[0].b[1] = py;
  self->lineMsg.lines[0].b[2] = pz;
  
  self->lineMsg.num_lines = 1;
  
  self->totalFakes += 1;

  return 0;
}


/* TODO
// Handle show button
int LinePerceptor::onUserShow(long arg)
{
  self->show = !self->show;
  return 0;
}
*/


// Find stop lines
int LinePerceptor::findStopLines()
{
  int i, j;
  uint8_t *pix;
  CvMat *im2m;
  //CameraInfo cameraInfo;
  FLOAT_POINT2D focalLength, opticalCenter;
  vector<Line> stopLines;
  vector<float> lineScores;

  // Create float matrix for holding image
  im2m = cvCreateMat(this->stereoBlob.rows, this->stereoBlob.cols, FLOAT_MAT_TYPE);
  
  // Copy data into float matrix.
  // TODO this only works properly for mono images.
  pix = this->stereoBlob.colorData;  
  for (j = 0; j < this->stereoBlob.rows; j++)
  {
    for (i = 0; i < this->stereoBlob.cols; i++)
    {
      cvmSet(im2m, j, i, (float) pix[0] / 255);
      pix += this->stereoBlob.colorChannels;
    }
  }

  // Construct camera parameters
  focalLength = cvPoint2D32f(this->stereoBlob.sx, this->stereoBlob.sy);
  opticalCenter = cvPoint2D32f(this->stereoBlob.cx, this->stereoBlob.cy);
  this->cameraInfo.focalLength = focalLength;
  this->cameraInfo.opticalCenter = opticalCenter;
  this->cameraInfo.imageWidth = this->stereoBlob.cols;
  this->cameraInfo.imageHeight = this->stereoBlob.rows;

  // Assume that camera height is in mm.
  // TODO: check that the yaw convention is correct.
  pose3_t pose;
  double rx, ry, rz;
  pose = pose3_from_mat44f(this->stereoBlob.sens2veh);
  quat_to_rpy(pose.rot, &rx, &ry, &rz);
  this->cameraInfo.cameraHeight = -1000 * pose.pos.z + 1000 * VEHICLE_TIRE_RADIUS;
  this->cameraInfo.pitch = rx; //malaa: -ry
  this->cameraInfo.yaw = ry; //malaa: rz
  
  // Search for lines
  mcvGetStopLines(im2m, &stopLines, &lineScores,  &cameraInfo, &this->stopLineConf);


  //show line or not
  if(this->show)
  {
    if(stopLines.size()>0)
      //display stop line
      mcvDrawLine(im2m, stopLines[0], CV_RGB(255,0,0), 3);
      
    //SHOW_IMAGE(image, "Detected lines");
    cvNamedWindow("window");
    cvShowImage("window",im2m);
    cvWaitKey(10);
  }
  else
  {
    //delete window
    cvDestroyWindow("window");
    cvWaitKey(10);
  }

  //get line score
  if((int) lineScores.size()>0)
  {
    lineScore = lineScores[0];
  }
  else
    lineScore = -1;
    
  Line line;
  int pi, pj, pd;
  float px, py, pz;
    
  for (i = 0; i < (int) stopLines.size(); i++)
  {
    if (i >= (int) (sizeof(this->lineMsg.lines) / sizeof(this->lineMsg.lines[0])))
      break;
    
    line = stopLines[i];

    // TODO: use camera model rather than stereo (which may not be defined)
    
    // Get point in image
    pi = (int) line.startPoint.x;
    pj = (int) line.startPoint.y;
    pd = StereoImageBlobGetDisp(&this->stereoBlob, pi, pj);

    // Compute point in the sensor frame
    StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);

    // Convert to vehicle frame
    StereoImageBlobSensorToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);

    // Convert to local frame
    StereoImageBlobVehicleToLocal(&this->stereoBlob, px, py, pz, &px, &py, &pz);
      
    this->lineMsg.lines[i].a[0] = px;
    this->lineMsg.lines[i].a[1] = py;
    this->lineMsg.lines[i].a[2] = pz;

    // Get point in image
    pi = (int) line.endPoint.x;
    pj = (int) line.endPoint.y;
    pd = StereoImageBlobGetDisp(&this->stereoBlob, pi, pj);

    // Compute point in the sensor frame
    StereoImageBlobImageToSensor(&this->stereoBlob, pi, pj, pd, &px, &py, &pz);

    // Convert to vehicle frame
    StereoImageBlobSensorToVehicle(&this->stereoBlob, px, py, pz, &px, &py, &pz);

    // Convert to local frame
    StereoImageBlobVehicleToLocal(&this->stereoBlob, px, py, pz, &px, &py, &pz);
      
    this->lineMsg.lines[i].b[0] = px;
    this->lineMsg.lines[i].b[1] = py;
    this->lineMsg.lines[i].b[2] = pz;
    this->lineMsg.num_lines++;
  }

  // Record some stats
  this->totalStops += stopLines.size();

  cvReleaseMat(&im2m);
  
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LinePerceptor *percept;

    
  percept = new LinePerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;
  
  while (!percept->quit)
  {
    // Update the console
    if (percept->console)
      cotk_update(percept->console);
    
    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Check for new data
    if (percept->readImage() == 0)
    {
      // Detect lines
      percept->findStopLines();
    }

    // Write message
    percept->writeLines();

    // Update the display
    if (percept->console)
    {
      cotk_printf(percept->console, "%stops%", A_NORMAL, "%d (%d fakes)",
                  percept->totalStops, percept->totalFakes);
    }
  }

  percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}



