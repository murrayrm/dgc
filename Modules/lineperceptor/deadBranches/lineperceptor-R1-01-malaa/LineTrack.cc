/**
 * \file LineTrack.cc
 * \author Mohamed Aly
 * \date 31 July 2007
 * \brief This file contains the implementation for the track repredentation
 *
 */




#include "LineTrack.hh"
#include "StopLinePerceptor.hh"

#include <map/MapElement.hh>

#include <cv.h>



int Track::idState = 0;


/**
 * \brief gets a unique map id for this spline track
 * 
 * \param mapIdPrefix the prefix to use for the map id
 */
int Track::getMapId(MapId& mapIdPrefix)
{
    //get a new id from the static member
    this->id = MapId(mapIdPrefix);
    this->id.push_back(++Track::idState);

    return Track::idState;
}


/**
 * \brief gets a unique map id for this spline track
 * 
 * \param mapTalker the map element talker to use 
 */
int Track::deleteFromMap(CMapElementTalker& mapTalker)
{
    //delete this map id
   //fill in the map element
    MapElement me;
    me.type = ELEMENT_CLEAR;
    me.setId(this->id);

    //send
    mapTalker.sendMapElement(&me, 0);

    //clear
    me.clear();

    return 0;
}


/**
 * \brief compares two splins and checks if to merge them
 * \param sp the input spline
 * \return true if they should be merged, false otherwise
 */
bool SplineTrack::checkMerge(const Track &sp)
{
    //convert to splinetrack object
    const SplineTrack& st = static_cast<const SplineTrack&> (sp);

    //we want to check if these two splines should be merged
    return mcvCheckMergeSplines(this->spline, st.spline);
}


/**
 * \brief merges two spline tracks together
 * \param sp the input spline
 * 
 */
int SplineTrack::merge(const Track &t)
{
    //convert to splinetrack object
    const SplineTrack& st = static_cast<const SplineTrack&> (t);

//     //check which has the highest score
//     if (this->score < st.score)
	//put the new spline in place of old one
	this->spline = st.spline;


    //put score
    this->score += st.score;

    //increment
    //this->numSeenFrames++;

    //take latest frame last seen
    this->frameLastSeen = this->frameId>st.frameId ? this->frameId : st.frameId;

    //take earliest frame Id
    this->frameId = this->frameId<st.frameId ? this->frameId : st.frameId;

    return 0;
}



/**
 * \brief sends this track to the map
 *
 * \param mapTalker the map element talker to use 
 * \param stereoBlob the stereo blob to use for conversion to local frame
 */
int SplineTrack::sendToMap(CMapElementTalker& mapTalker, StereoImageBlob* stereoBlob)
{

    //get the spline points
    float px, py, pz;

    //convert spline to points
    CvMat *splinePoints = mcvEvalBezierSpline(this->spline, .05);

    //points to send to map
    point2arr points;

    //loop on points and convert to local frame
    for(int i=0; i<splinePoints->height; i++)
    {	
	//get point in local frame
	StereoImageBlobImageToLocal(stereoBlob,
				    (float)cvGetReal2D(splinePoints, i, 0), 
				    (float)cvGetReal2D(splinePoints, i, 1), 
				    &px, &py, &pz);
	//add point to map points
	points.push_back(point2(px, py));
    }

    //fill in the map element
    MapElement me;
    me.setGeometry(points);
    me.height = 0;
    me.plotColor = MAP_COLOR_GREEN;
    me.geometryType = GEOMETRY_LINE;
    me.type = ELEMENT_LANELINE;
    me.setId(this->id);
    me.state = stereoBlob->state;

    //send
    mapTalker.sendMapElement(&me, 0);

    //clear
    points.clear();
    cvReleaseMat(&splinePoints);
    me.clear();

//     cerr << "Sent element: " << this->id << endl;
    return 0;
}




/**
 * \brief This functions creates a vector of SplineTrack objects with the passed in splines
 *
 * \param splines the input splines
 * \param scores the splien scores
 * \param frameId the current frame id
 *
 * \return a vector of SplineTrack objects
 */
vector<Track*> 
SplineTrack::CreateSplineTracks(const vector<Spline>& splines, 
				const vector<float>& scores,
				long frameId)
{
    vector<Track*> tracks;
    tracks.reserve(splines.size());

    //loop on the input vector and create objects
    
    for (unsigned int i=0; i<splines.size(); ++i)
    {
	SplineTrack* tr = new SplineTrack(splines[i], frameId, scores[i]);
	tracks.push_back(tr);
    }

    //return
    return tracks;
}

/**
 * \brief This functions destroys a vector of SplineTracks created by
 *  CreateSplineTracks
 * \param tracks the tracks to destroy
 * 
 */
int SplineTrack::DestroySplineTracks(vector<Track*>& tracks)
{
        
    //loop on the input vector and free memory
    for (unsigned int i=0; i<tracks.size(); ++i)
	if (tracks[i]->remove)
	    delete tracks[i];

    //clear vector
    tracks.clear();

    //return
    return 0;
}
