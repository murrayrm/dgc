#ifndef STOPLINEPERCEPTOR_HH_
#define STOPLINEPERCEPTOR_HH_

#include "mcv.hh"
#include "InversePerspectiveMapping.hh"

//#define DEBUG_GET_STOP_LINES



///line structure with start and end points
typedef struct Line
{
    FLOAT_POINT2D startPoint;
    FLOAT_POINT2D endPoint;
}Line;


#define GROUPING_TYPE_HV_LINES 0
#define GROUPING_TYPE_HOUGH_LINES 1

//Structure to hold line perceptor settings
typedef struct StopLinePerceptorConf
{
    ///width of IPM image to use
    FLOAT ipmWidth;
    ///height of IPM image
    FLOAT ipmHeight;
    ///width of line we are detecting
    FLOAT lineWidth;
    ///height of line we are detecting
    FLOAT lineHeight;
    ///kernel size to use for filtering 
    unsigned char kernelWidth;
    unsigned char kernelHeight;
    ///lower quantile to use for thresholding the filtered image
    FLOAT lowerQuantile;
    ///whether to return local maxima or just the maximum
    bool localMaxima;
    ///the type of grouping to use: 0 for HV lines and 1 for Hough Transform
    unsigned char groupingType;
    ///whether to binarize the thresholded image or use the 
    ///raw filtered image
    bool binarize;
    //unsigned char topClip;
    ///threshold for line scores to declare as line
    FLOAT detectionThreshold;
    ///whtehter to smooth the line scores detected or not
    bool smoothScores;
    ///rMin, rMax and rStep for Hough Transform (pixels)
    float rMin, rMax, rStep;
    ///thetaMin, thetaMax, thetaStep for Hough Transform (radians)
    float thetaMin, thetaMax, thetaStep;
    ///portion of image height to add to y-coordinate of vanishing
    ///point when computing the IPM image
    float ipmVpPortion;
    ///get end points or not
    bool getEndPoints;
    ///group nearby lines
    bool group;
    ///threshold for grouping nearby lines
    float groupThreshold;
    ///use RANSAC or not
    bool ransac;
    ///RANSAC parameters
    int ransacNumSamples;
    int ransacNumIterations;
    int ransacNumGoodFit;
    float ransacThreshold;
    float ransacScoreThreshold;
    bool ransacBinarize;


}StopLinePerceptorConf;


//function definitions


/**
 * This function gets a 1-D gaussian filter with specified
 * std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGetGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma);
    

/**
 * This function gets a 1-D second derivative gaussian filter 
 * with specified std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGet2DerivativeGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma);
    

/**
 * This function filters the input image looking for horizontal
 * or vertical lines with specific width or height.
 *
 * \param inImage the input image
 * \param outImage the output image in IPM
 * \param wx width of kernel window in x direction = 2*wx+1 
 * (default 2)
 * \param wy width of kernel window in y direction = 2*wy+1 
 * (default 2)
 * \param sigmax std deviation of kernel in x (default 1)
 * \param sigmay std deviation of kernel in y (default 1)
 * \param lineType type of the line
 *      FILTER_LINE_HORIZONTAL (default)
 *      FILTER_LINE_VERTICAL
 */ 
 
#define FILTER_LINE_HORIZONTAL 0
#define FILTER_LINE_VERTICAL 1
void mcvFilterLines(const CvMat *inImage, CvMat *outImage,
    unsigned char wx=2, unsigned char wy=2, FLOAT sigmax=1,
    FLOAT sigmay=1, unsigned char lineType=FILTER_LINE_HORIZONTAL);    
    
    
/** This function groups the input filtered image into 
 * horizontal or vertical lines.
 * 
 * \param inImage input image
 * \param lines returned detected lines (vector of points)
 * \param lineScores scores of the detected lines (vector of floats)
 * \param lineType type of lines to detect
 *      HV_LINES_HORIZONTAL (default) or HV_LINES_VERTICAL
 * \param linePixelWidth width (or height) of lines to detect
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 */ 
#define HV_LINES_HORIZONTAL 0
#define HV_LINES_VERTICAL   1
void mcvGetHVLines(const CvMat *inImage, vector<Line> *lines,
    vector<FLOAT> *lineScores, unsigned char lineType=HV_LINES_HORIZONTAL, 
    FLOAT linePixelWidth=1., bool binarize=false, bool localMaxima=false, 
    FLOAT detectionThreshold=1., bool smoothScores=true);
    

/** This function binarizes the input image i.e. nonzero elements
 * become 1 and others are 0.
 * 
 * \param inImage input & output image
 */ 
void mcvBinarizeImage(CvMat *inImage);

/** This function gets the maximum value in a vector (row or column) 
 * and its location
 * 
 * \param inVector the input vector
 * \param max the output max value
 * \param maxLoc the location (index) of the first max
 * \param ignore don't the first and last ignore elements
 * 
 */ 
void mcvGetVectorMax(const CvMat *inVector, double *max, int *maxLoc, int ignore=0);

/** This function gets the qtile-th quantile of the input matrix
 * 
 * \param mat input matrix
 * \param qtile required input quantile probability
 * \return the returned value
 * 
 */
FLOAT mcvGetQuantile(const CvMat *mat, FLOAT qtile);
        
/** This function thresholds the image below a certain value to the threshold
 * so: outMat(i,j) = inMat(i,j) if inMat(i,j)>=threshold
 *                 = threshold otherwise
 * 
 * \param inMat input matrix
 * \param outMat output matrix
 * \param threshold threshold value
 * 
 */
void mcvThresholdLower(const CvMat *inMat, CvMat *outMat, FLOAT threshold);


/** This function detects stop lines in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param stopLines a vector of returned stop lines in input image coordinates 
 * \param linescores a vector of line scores returned
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetStopLines(const CvMat *inImage, vector<Line> *stopLines, 
		     vector<float> *lineScores, const CameraInfo *cameraInfo, 
		     StopLinePerceptorConf *stopLineConf);

/** This function converts an array of lines to a matrix (already allocated)
 * 
 * \param lines input vector of lines
 * \param size number of lines to convert
 * \return the converted matrix, it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * 
 * 
 */
void mcvLines2Mat(const vector<Line> *lines, CvMat *mat);

/** This function converts matrix into n array of lines
 * 
 * \param mat input matrix , it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * \param  lines the rerurned vector of lines
 * 
 * 
 */
void mcvMat2Lines(const CvMat *mat, vector<Line> *lines);

/** This function intersects the input line with the given bounding box
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineWithBB(const Line *inLine, const CvSize bbox,
    Line *outLine);
    
/** This function checks if the given point is inside the bounding box
 * specified
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D point, CvSize bbox);

/** This function converts an INT mat into a FLOAT mat (already allocated)
 * 
 * \param inMat input INT matrix
 * \param outMat output FLOAT matrix
 * 
 */
void mcvMatInt2Float(const CvMat *inMat, CvMat *outMat);

    

/** This function draws a line onto the passed image
 * 
 * \param image the input iamge
 * \param line input line
 * \param line color
 * \param width line width
 * 
 */
void mcvDrawLine(CvMat *image, Line line, CvScalar color=CV_RGB(0,0,0), int width=1);


/** This initializes the stoplineperceptorinfo structure
 * 
 * \param fileName the input file name
 * \param stopLineConf the structure to fill
 *
 * 
 */
 void mcvInitStopLinePerceptorConf(char * const fileName, 
    StopLinePerceptorConf *stopLineConf);

void SHOW_LINE(const Line line, char str[]="Line:");

/** This fits a parabola to the entered data to get
 * the location of local maximum with sub-pixel accuracy
 * 
 * \param val1 first value
 * \param val2 second value
 * \param val3 third value
 *
 * \return the computed location of the local maximum
 */
double mcvGetLocalMaxSubPixel(double val1, double val2, double val3);


/** This function detects lines in images using Hough transform
 * 
 * \param inImage input image
 * \param lines vector of lines to hold the results
 * \param lineScores scores of the detected lines (vector of floats)
 * \param rMin minimum r use for finding the lines (default 0)
 * \param rMax maximum r to find (default max(size(im)))
 * \param rStep step to use for binning (default is 2)
 * \param thetaMin minimum angle theta to look for (default 0) all in radians
 * \param thetaMax maximum angle theta to look for (default 2*pi)
 * \param thetaStep step to use for binning theta (default 5)
 * \param  binarize if to binarize the input image or use the raw values so that
 *	non-zero values are not treated as equal
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 * \param smoothScores whether to smooth scores detected or not
 * \param group whether to group nearby detections (1) or not (0 default)
 * \param groupThreshold the minimum distance used for grouping (default 10)
 */ 

void mcvGetHoughTransformLines(const CvMat *inImage, vector <Line> *lines,
			       vector <FLOAT> *lineScores,
			       FLOAT rMin, FLOAT rMax, FLOAT rStep, 
			       FLOAT thetaMin, FLOAT thetaMax,
			       FLOAT thetaStep, bool binarize, bool localMaxima, 
			       FLOAT detectionThreshold, bool smoothScores,
			       bool group, FLOAT groupTthreshold);


/** This function intersects the input line (given in r and theta) with 
 *  the given bounding box where the line is represented by: 
 *  x cos(theta) + y sin(theta) = r
 * 
 * \param r the r value for the input line
 * \param theta the theta value for the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineRThetaWithBB(FLOAT r, FLOAT theta, const CvSize bbox,
				  Line *outLine);

/** This function gets the local maxima in a matrix and their positions 
 *  and its location
 * 
 * \param inMat input matrix
 * \param localMaxima the output vector of local maxima
 * \param localMaximaLoc the vector of locations of the local maxima, 
 *       where each location is cvPoint(x=col, y=row) zero-based
 * \param threshold threshold to return local maxima above
 * 
 */ 
void mcvGetMatLocalMax(const CvMat *inMat, vector<double> &localMaxima, 
		       vector<CvPoint> &localMaximaLoc, double threshold=0.0);

/** This function gets the local maxima in a vector and their positions 
 * 
 * \param inVec input vector
 * \param localMaxima the output vector of local maxima
 * \param localMaximaLoc the vector of locations of the local maxima, 
 * 
 */ 
void mcvGetVectorLocalMax(const CvMat *inVec, vector<double> &localMaxima, 
			  vector<int> &localMaximaLoc);


/** This functions implements Bresenham's algorithm for getting pixels of the
 * line given its two endpoints

 * 
 * \param line the input line
 * \param x a vector of x locations for the line pixels (0-based)
 * \param y a vector of y locations for the line pixels (0-based)
 *
 */
void mcvGetLinePixels(const Line &line, vector<int> &x, vector<int> &y);

/** This functions implements Bresenham's algorithm for getting pixels of the
 * line given its two endpoints

 * 
 * \param im the input image
 * \param inLine the input line
 * \param outLine the output line
 *
 */
void mcvGetLineExtent(const CvMat *im, const Line &inLine, Line &outLine);

/** This functions converts a line defined by its two end-points into its
    r and theta (origin is at top-left corner with x right and y down and theta
    measured positive clockwise<with y pointing down> -pi<theta<pi)

 * 
 * \param line input line
 * \param r the returned r (normal distance to the line from the origin)
 * \param outLine the output line
 *
 */
void mcvLineXY2RTheta(const Line &line, float &r, float &theta);


/** This function checks if the given point is inside the rectangle specified
 * 
 * \param inLine the input line
 * \param rect the specified rectangle
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D &point, const Line &rect);

/** This function intersects the input line (given in r and theta) with 
 *  the given rectangle where the line is represented by: 
 *  x cos(theta) + y sin(theta) = r
 * 
 * \param r the r value for the input line
 * \param theta the theta value for the input line
 * \param rect the input rectangle (given two opposite points in the rectangle, 
 *   upperleft->startPoint and bottomright->endPoint where x->right and y->down)
 * \param outLine the output line
 * 
 */
void mcvIntersectLineRThetaWithRect(FLOAT r, FLOAT theta, const Line &rect,
				    Line &outLine);


/** This function detects lanes in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param lanes a vector of returned stop lines in input image coordinates 
 * \param linescores a vector of line scores returned
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetLanes(const CvMat *inImage, vector<Line> *lanes, 
		 vector<FLOAT> *lineScores, const CameraInfo *cameraInfo, 
		 StopLinePerceptorConf *stopLineConf);

/** This function gets the indices of the non-zero values in a matrix

 * \param inMat the input matrix
 * \param outMat the output matrix, with 2xN containing the x and y in 
 *    each column
 * \param floatMat whether to return floating points or integers for 
 *    the outMat
 */
CvMat* mcvGetNonZeroPoints(const CvMat *inMat, bool floatMat);

/** This functions implements RANSAC algorithm for line fitting
    given an image

 * 
 * \param image input image
 * \param numSamples number of samples to take every iteration
 * \param numIterations number of iterations to run
 * \param threshold threshold to use to assess a point as a good fit to a line
 * \param numGoodFit number of points close enough to say there's a good fit 
 * \param lineXY the fitted line 
 * \param lineRTheta the fitted line [r; theta]
 *
 */
void mcvFitRansacLine(const CvMat *image, int numSamples, int numIterations,
		      float threshold, float scoreThreshold, int numGoodFit, Line *lineXY, 
		      float *lineRTheta, float *lineScore);


/** This functions fits a line using the orthogonal distance to the line
    by minimizing the sum of squares of this distance.

 * 
 * \param points the input points to fit the line to which is
 *    2xN matrix with x values on first row and y values on second
 * \param lineRTheta the return line [r, theta] where the line is
 *    x*cos(theta)+y*sin(theta)=r
 * \param lineAbc the return line in [a, b, c] where the line is
 *    a*x+b*y+c=0
 *
 */
void mcvFitRobustLine(const CvMat *points, float *lineRTheta, 
		      float *lineAbc);

/** This function groups nearby lines
 *
 * \param inLines vector of lines
 * \param outLines vector of grouped lines
 * \param groupThreshold the threshold used for grouping
 * \param bbox the bounding box to intersect with
 */
void mcvGroupLines(vector<Line> &lines, vector<float> &lineScores, 
		   float groupThreshold, CvSize bbox);


/** This function performs a RANSAC validation step on the detected lines
 *
 * \param image the input image
 * \param inLines vector of lines
 * \param outLines vector of grouped lines
 * \param groupThreshold the threshold used for grouping
 * \param bbox the bounding box to intersect with
 */
void mcvGetRansacLines(const CvMat *im, vector<Line> &lines, 
		       vector<float> &lineScores, StopLinePerceptorConf *lineConf,
		       unsigned char lineType);


/** This function sets the matrix to a value except for the mask window passed in
 *
 * \param inMat input matrix
 * \param mask the rectangle defining the mask: (xleft, ytop, width, height)
 * \param val the value to put
 */
void  mcvSetMat(CvMat *inMat, CvRect mask, double val);


#endif /*STOPLINEPERCEPTOR_*/
