#ifndef LINE_PERCEPTOR_HH
#define LINE_PERCEPTOR_HH


/**
 * \file LinePerceptor.hh
 * \author Mohamed Aly and Andrew Howard
 * \date Thu 26 Jul, 2007
 *
 */


#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include <alice/AliceConstants.h>
#include <frames/pose3.h>

// Sensnet/Skynet support
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>
#include <interfaces/RoadLineMsg.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>



// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

// Stop-line detection
#include <vector>
using namespace std;
#include <cv.h>
#include <highgui.h>
#include "mcv.hh"
#include "InversePerspectiveMapping.hh"
#include "StopLinePerceptor.hh"

#include "LineTracker.hh"

// Line perceptor module
class LinePerceptor
{
public:
    
    // Constructor
    LinePerceptor();
    
    // Destructor
    ~LinePerceptor();
    
public:
    
    // Parse the command line
    int parseCmdLine(int argc, char **argv);
    
public:
    
    // Initialize sensnet
    int initSensnet();
    
    // Clean up sensnet
    int finiSensnet();
    
    // Read the current image
    int readImage();
    
    // Write the current stop lines
    int writeLines();
    
    //convert lines from iamge frame to local frame and put into the roadline message
    int ImageLineToLocalLine(const Line &imageLine, RoadLineMsg &msg, int index);
    //sends a spline in the image to the map
    int sendImageSpline(Spline &imageSpline, int index);

  public:

  // Find stop lines
  int findLines();


  //export current image
  int exportImage();


  public:

  // Initialize sparrow display
  int initConsole();

  // Finalize sparrow display
  int finiConsole();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserFake(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserShow(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserSkip(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserDebug(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserStep(cotk_t *console, LinePerceptor *self, const char *token);

  // Console button callback
  static int onUserExport(cotk_t *console, LinePerceptor *self, const char *token);

  public:
  
  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
   
  // Operating mode
  enum {modeLive, modeReplay, modeBlob} mode;
  
  // Source sensor id
  sensnet_id_t sensorId;

  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet log replay module
  sensnet_replay_t *replay;

  // Console display
  cotk_t *console;

  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //should we show the detected line?
  bool show;

  //skip thourgh log?
  bool skip;

  //goto option given?
  int gotoFrame;

  //should we step through shown images by pressing a key or fall through
  bool step;

  //should we export every frame to an image
  bool save;

  //should we export debug images (detected and tracked lines)
  bool save_debug;

    //should we show debug images
    bool debug;

  // Current blob id
  int blobId;
  
  // Current (incoming) stereo blob
  StereoImageBlob stereoBlob;

    // Current (outgoing) line message
    RoadLineMsg lineMsg;
    RoadLineMsg laneMsg;

  // Diagnostics on stop lines
  int totalStops, totalFakes;
    
  //stop line conf strucuture
  StopLinePerceptorConf stopLineConf, laneConf;
  //camera info strucure
  CameraInfo cameraInfo;
  //line score
  float lineScore;


    //tracking lines
    //
    //track or not
    bool trackStopLines;
    bool trackLanes;
    //if there's a line currently being tracked
    bool stopLineTrackCreated;
    //number of frames with measurements before starting the track
    int numPreFrames;
    //number of frames without measurements before destroying the track
    int numPostFrames;
    //kalman structure
    CvKalman* kalman;
    //initialize track
    void initStopLineTrack();
    //destroy track
    void destroyStopLineTrack();
    //track stop lines
    void updateStopLineTrack();
    //tracking model
    int stopLineTrackingModel;

    //detections
    bool noStoplines;
    bool noLanes;

    //map element talker to send to map
    CMapElementTalker mapElementTalker; 
    //map element to send the stop line
    MapElement mapElement;

    //this holds the 
//     vector<SplineTrack> laneSplineTracks;

    //splien lane tracker
    SplineTracker laneSplineTracker;
    //stop line tracker
    LineTracker stoplineTracker;
    


};



#endif /*LINE_PERCEPTOR_HH*/
