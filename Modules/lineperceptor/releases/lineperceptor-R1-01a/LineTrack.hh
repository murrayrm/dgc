#ifndef LINETRACK_HH_
#define LINETRACK_HH_

/**
 * \file LineTrack.hh
 * \author Mohamed Aly
 * \date 31 July 2007
 * \brief This file contains the header definitions for the LineTrack class
 *
 */


#include "StopLinePerceptor.hh"
#include <map/MapId.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <interfaces/StereoImageBlob.h>


#include <vector>

/**
 * \class Track
 * \brief This class represents a tracked line through time.
 *
 *
 *
 */

class Track
{


public:


    ///constructor
    Track(long frameId = -1, float score = -10) 
    {
	this->numAbsentFrames = 0;
	this->numSeenFrames = 0;
	this->frameId = frameId;
	this->frameLastSeen = frameId;
	this->score = score;
	this->id = MapId(-1980);
	this->remove = false;
	this->valid = false;
    }


    ///destructor
    virtual ~Track() 
    {}

    ///track id
    MapId id;

    ///number of consecutive frames it's absent in 
    int numAbsentFrames;

    ///number of consecutive frames it's seen in
    int numSeenFrames;

    ///frame this track was created
    long frameId;

    ///score for the track
    float score;

    ///remove
    bool remove;

    ///frame this track was last seen
    long frameLastSeen;

    ///is it a valid track i.e. has been seen long enough or hasn't been missed long enough
    bool valid;

    /**
     * \brief compares two tracks and checks if to merge them
     * \param sp the input track
     *
     * \return true if they should be merged, false otherwise
     */
    virtual bool checkMerge(const Track&) {return false;};

    /**
     * \brief merges two tracks together
     * \param sp the input track
     * 
     */
    virtual int merge(const Track&) { return 0;};

    /**
     * \brief gets a unique map id for this spline track
     * 
     * \param mapIdPrefix the prefix to use for the map id
     */
    virtual int getMapId(MapId& mapIdPrefix);

    /// \brief send track to map
    virtual int sendToMap(CMapElementTalker&, StereoImageBlob* stereoBlob) 
    {return 0;};

    /**
     * \brief gets a unique map id for this spline track
     * 
     * \param mapTalker the map element talker to use 
     */
    virtual int deleteFromMap(CMapElementTalker& mapTalker);


    ///static member for id creation
    static int idState;
};


/**
 * \class SplineTrack
 * \brief This class represents a tracked spline through time.
 *
 *
 *
 */

class SplineTrack : public Track
{

public:

    ///default constructor
    SplineTrack(long frameId = -1, float score = -10.) :
	Track(frameId, score)
    {}

    ///default constructor
    SplineTrack(const Spline& spline, long frameId = -1, float score = -10.) : 
	Track(frameId, score)
    {
	this->spline = spline;
    }

    ///destructor
    ~SplineTrack()
    {}


    ///the spline this track represents
    Spline spline;


    /**
     * \brief compares two splins and checks if to merge them
     * \param sp the input spline
     * \return true if they should be merged, false otherwise
     */
    bool checkMerge(const Track &sp);

    /**
     * \brief merges two spline tracks together
     * \param sp the input spline
     * 
     */
    int merge(const Track &sp);


    /**
     * \brief sends this track to the map
     *
     * \param mapTalker the map element talker to use 
     * \param stereoBlob the stereo blob to use for conversion to local frame
     */
    virtual int sendToMap(CMapElementTalker& mapTalker, StereoImageBlob* stereoBlob);



    /**
     * \brief This functions creates a vector of SplineTrack objects with the passed in splines
     * \param splines the input splines
     * \param scores the splien scores
     * \param frameId the current frame id
     *
     * \return a vector of SplineTrack objects
     */
    static vector<Track*>  CreateSplineTracks(const vector<Spline>& splines, 
					     const vector<float>& scores,
					     long frameId);

    /**
     * \brief This functions destroys a vector of SplineTracks created by
     *  CreateSplineTracks
     * \param tracks the tracks to destroy
     * 
     */
    static int DestroySplineTracks(vector<Track*>& tracks);
    
};

#endif
