#ifndef MCV_HH_
#define MCV_HH_

#include "cv.h"

#include <stdio.h>
#include <vector>

//#include "gnuplot_i.h"
 
//constant definitions  
#define FLOAT_MAT_TYPE CV_32FC1
#define FLOAT_MAT_ELEM_TYPE float

#define INT_MAT_TYPE CV_8UC1
#define INT_MAT_ELEM_TYPE unsigned char

#define FLOAT_IMAGE_TYPE IPL_DEPTH_32F
#define FLOAT_IMAGE_ELEM_TYPE float

#define INT_IMAGE_TYPE IPL_DEPTH_8U
#define INT_IMAGE_ELEM_TYPE unsigned char

#define FLOAT_POINT2D CvPoint2D32f
#define FLOAT_POINT2D_F cvPoint2D632f

#define FLOAT float
#define INT int
#define SHORT_INT unsigned char

// Error macros
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)



//some helper functions for debugging
void SHOW_MAT(const CvMat *pmat, char str[]="Matrix");

void SHOT_MAT_TYPE(const CvMat *pmat);

void SHOW_IMAGE(const CvMat *pmat, char str[]="Window", int wait=0);
void SHOW_IMAGE(const IplImage *pmat, char str[]="Window");

void SHOW_POINT(const FLOAT_POINT2D pt, char str[]="Point:");


/**
 * This function scales the input image to have values 0->1
 * 
 * \param inImage the input image
 * \param outImage hte output iamge
 */
void mcvScaleMat(const CvMat *inImage, CvMat *outMat);

/**
 * This function plots a 1-D matrix
 * 
 * \param handle the handle to the gnuplot object to add to, or NULL to create
 * a new one
 * \param mat the input matrix
 * \param title the plot title
 * \return the passed handle or the created one
 *
 */
//gnuplot_ctrl* mcvPlotMat1D(gnuplot_ctrl* handle, const CvMat * mat, char *title="Title");

/**
 * This function plots a 2-D matrix
 * 
 * \param handle the handle to the gnuplot object to add to, or NULL to create
 * a new one
 * \param matx the x-axis matrix
 * \param maty the y-axis matrix
 * \param title the plot title
 *
 * \return the passed handle or the created one
 *
 */
//gnuplot_ctrl* mcvPlotMat2D(gnuplot_ctrl* handle, const CvMat * matx, 
			   const CvMat* maty, char *title="Title");


/**
 * This function creates a double matrix from an input vector
 * 
 * \param vec the input vector
 * \param mat the output matrix (column vector)
 *
 */
template <class T>
CvMat* mcvVector2Mat(const vector<T> &vec);



#endif /*MCV_HH_*/
