

#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;

#include "cv.h"
#include "highgui.h"

#include "mcv.hh"
#include "InversePerspectiveMapping.hh"
#include "StopLinePerceptor.hh"

#include "StopLinePerceptorOpt.h"

#include "ranker.h"

//#include "gnuplot_i.h"

/**
 * This function filters the input image looking for horizontal
 * or vertical lines with specific width or height.
 *
 * \param inImage the input image
 * \param outImage the output image in IPM
 * \param wx width of kernel window in x direction = 2*wx+1 
 * (default 2)
 * \param wy width of kernel window in y direction = 2*wy+1 
 * (default 2)
 * \param sigmax std deviation of kernel in x (default 1)
 * \param sigmay std deviation of kernel in y (default 1)
 * \param lineType type of the line
 *      FILTER_LINE_HORIZONTAL (default)
 *      FILTER_LINE_VERTICAL
 */ 
 void mcvFilterLines(const CvMat *inImage, CvMat *outImage,
    unsigned char wx, unsigned char wy, FLOAT sigmax,
    FLOAT sigmay, unsigned char lineType)
{
    //define the two kernels
    //this is for 7-pixels wide
//     FLOAT_MAT_ELEM_TYPE derivp[] = {-2.328306e-10, -6.984919e-09, -1.008157e-07, -9.313226e-07, -6.178394e-06, -3.129616e-05, -1.255888e-04, -4.085824e-04, -1.092623e-03, -2.416329e-03, -4.408169e-03, -6.530620e-03, -7.510213e-03, -5.777087e-03, -5.777087e-04, 6.932504e-03, 1.372058e-02, 1.646470e-02, 1.372058e-02, 6.932504e-03, -5.777087e-04, -5.777087e-03, -7.510213e-03, -6.530620e-03, -4.408169e-03, -2.416329e-03, -1.092623e-03, -4.085824e-04, -1.255888e-04, -3.129616e-05, -6.178394e-06, -9.313226e-07, -1.008157e-07, -6.984919e-09, -2.328306e-10};
//     int derivLen = 35;
//     FLOAT_MAT_ELEM_TYPE smoothp[] = {2.384186e-07, 5.245209e-06, 5.507469e-05, 3.671646e-04, 1.744032e-03, 6.278515e-03, 1.778913e-02, 4.066086e-02, 7.623911e-02, 1.185942e-01, 1.541724e-01, 1.681881e-01, 1.541724e-01, 1.185942e-01, 7.623911e-02, 4.066086e-02, 1.778913e-02, 6.278515e-03, 1.744032e-03, 3.671646e-04, 5.507469e-05, 5.245209e-06, 2.384186e-07};
//     int smoothLen = 23;

    //this is for 5-pixels wide
    FLOAT_MAT_ELEM_TYPE derivp[] = {-2.384186e-07, -4.768372e-06, -4.482269e-05, -2.622604e-04, -1.064777e-03, -3.157616e-03, -6.976128e-03, -1.136112e-02, -1.270652e-02, -6.776810e-03, 6.776810e-03, 2.156258e-02, 2.803135e-02, 2.156258e-02, 6.776810e-03, -6.776810e-03, -1.270652e-02, -1.136112e-02, -6.976128e-03, -3.157616e-03, -1.064777e-03, -2.622604e-04, -4.482269e-05, -4.768372e-06, -2.384186e-07};
    int derivLen = 25;
    FLOAT_MAT_ELEM_TYPE smoothp[] = {2.384186e-07, 5.245209e-06, 5.507469e-05, 3.671646e-04, 1.744032e-03, 6.278515e-03, 1.778913e-02, 4.066086e-02, 7.623911e-02, 1.185942e-01, 1.541724e-01, 1.681881e-01, 1.541724e-01, 1.185942e-01, 7.623911e-02, 4.066086e-02, 1.778913e-02, 6.278515e-03, 1.744032e-03, 3.671646e-04, 5.507469e-05, 5.245209e-06, 2.384186e-07};
    int smoothLen = 23;

    CvMat fx;
    CvMat fy;
    //create the convoultion kernel
    switch (lineType)
    {
        case FILTER_LINE_HORIZONTAL:
	    //horizontal is smoothing and vertical is derivative
	    fx = cvMat(1, smoothLen, FLOAT_MAT_TYPE, smoothp);
	    fy = cvMat(derivLen, 1, FLOAT_MAT_TYPE, derivp);

            break;  
        
        case FILTER_LINE_VERTICAL:
	    //horizontal is derivative and vertical is smoothing
	    fy = cvMat(1, smoothLen, FLOAT_MAT_TYPE, smoothp);
	    fx = cvMat(derivLen, 1, FLOAT_MAT_TYPE, derivp);

            break;
    }

    #ifdef DEBUG_GET_STOP_LINES
    //SHOW_MAT(kernel, "Kernel:");
    #endif
    
    //do the filtering
    cvFilter2D(inImage, outImage, &fx);
    cvFilter2D(outImage, outImage, &fy);


//     CvMat *deriv = cvCreateMat
//     //define x
//     CvMat *x = cvCreateMat(2*wx+1, 1, FLOAT_MAT_TYPE);
//     //define y
//     CvMat *y = cvCreateMat(2*wy+1, 1, FLOAT_MAT_TYPE);
        
//     //create the convoultion kernel
//     switch (lineType)
//     {
//         case FILTER_LINE_HORIZONTAL:
//             //guassian in x direction
// 	    mcvGetGaussianKernel(x, wx, sigmax);
//             //derivative of guassian in y direction
//             mcvGet2DerivativeGaussianKernel(y, wy, sigmay);            
//             break;  
        
//         case FILTER_LINE_VERTICAL:
//             //guassian in y direction
//             mcvGetGaussianKernel(y, wy, sigmay);
//             //derivative of guassian in x direction
//             mcvGet2DerivativeGaussianKernel(x, wx, sigmax);            
//             break;
//     }
    
//     //combine the 2D kernel
//     CvMat *kernel = cvCreateMat(2*wy+1, 2*wx+1, FLOAT_MAT_TYPE);
//     cvGEMM(y, x, 1, 0, 1, kernel, CV_GEMM_B_T);
    
//     //subtract the mean
//     CvScalar mean = cvAvg(kernel);
//     cvSubS(kernel, mean, kernel); 
    
//     #ifdef DEBUG_GET_STOP_LINES
//     //SHOW_MAT(kernel, "Kernel:");
//     #endif
    
//     //do the filtering
//     cvFilter2D(inImage, outImage, kernel);
    
//     //clean
//     cvReleaseMat(&x);
//     cvReleaseMat(&y);
//     cvReleaseMat(&kernel);            
}

/**
 * This function gets a 1-D gaussian filter with specified
 * std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGetGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma)
{
    //get variance
    sigma *= sigma;
    
    //get the kernel
    for (double i=-w; i<=w; i++)
        CV_MAT_ELEM(*kernel, FLOAT_MAT_ELEM_TYPE, int(i+w), 0) = 
           (FLOAT_MAT_ELEM_TYPE) exp(-(.5/sigma)*(i*i));    
}

/**
 * This function gets a 1-D second derivative gaussian filter 
 * with specified std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGet2DerivativeGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma)
{
    //get variance
    sigma *= sigma;
    
    //get the kernel
    for (double i=-w; i<=w; i++)
        CV_MAT_ELEM(*kernel, FLOAT_MAT_ELEM_TYPE, int(i+w), 0) = 
           (FLOAT_MAT_ELEM_TYPE)  
           (exp(-.5*i*i)/sigma - (i*i)*exp(-(.5/sigma)*i*i)/(sigma*sigma));    
}


/** This function groups the input filtered image into 
 * horizontal or vertical lines.
 * 
 * \param inImage input image
 * \param lines returned detected lines (vector of points)
 * \param lineScores scores of the detected lines (vector of floats)
 * \param lineType type of lines to detect
 *      HV_LINES_HORIZONTAL (default) or HV_LINES_VERTICAL
 * \param linePixelWidth width (or height) of lines to detect
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 * \param smoothScores whether to smooth scores detected or not
 */ 
void mcvGetHVLines(const CvMat *inImage, vector <Line> *lines,
    vector <FLOAT> *lineScores, unsigned char lineType, 
    FLOAT linePixelWidth, bool binarize, bool localMaxima, 
    FLOAT detectionThreshold, bool smoothScores)
{
    CvMat * image = cvCloneMat(inImage);
    //binarize input image if to binarize
    if (binarize)
    {
        mcvBinarizeImage(image);   
    }

    //get sum of lines through horizontal or vertical
    //sumLines is a column vector
    CvMat sumLines, *sumLinesp;
    int maxLineLoc;
    switch (lineType)
    {
    case HV_LINES_HORIZONTAL:
        sumLinesp = cvCreateMat(image->height, 1, FLOAT_MAT_TYPE);
        cvReduce(image, sumLinesp, 1, CV_REDUCE_SUM); //_AVG
        cvReshape(sumLinesp, &sumLines, 0, 0);
        //max location for a detected line
        maxLineLoc = image->height-1;
        break;
    case HV_LINES_VERTICAL:
        sumLinesp = cvCreateMat(1, image->width, FLOAT_MAT_TYPE);
        cvReduce(image, sumLinesp, 0, CV_REDUCE_SUM); //_AVG
        cvReshape(sumLinesp, &sumLines, 0, image->width);
        //max location for a detected line
        maxLineLoc = image->width-1;
        break;
    }
    //SHOW_MAT(&sumLines, "sumLines:");
    
    //smooth it
    int smoothWidth = 10;//int(linePixelWidth+.5)+1;
    CvMat *smooth = cvCreateMat(smoothWidth, 1, FLOAT_MAT_TYPE);
    cvSet(smooth, cvRealScalar(1./smooth->rows));  //was 1 1./smooth->rows
    if (smoothScores)
    {
	cvFilter2D(&sumLines, &sumLines, smooth);
    }
    //SHOW_MAT(smooth, "sumLines:");

    
    //get the max and its location
    vector <int> sumLinesMaxLoc;
    vector <double> sumLinesMax;
    int maxLoc; double max;
    //TODO: put the ignore in conf
    #define MAX_IGNORE 0 //(int(smoothWidth/2.)+1)
    #define LOCAL_MAX_IGNORE (int(MAX_IGNORE/4))
    mcvGetVectorMax(&sumLines, &max, &maxLoc, MAX_IGNORE);
    
    //put the local maxima stuff here
    if (localMaxima)
    {
	//loop to get local maxima
	for(int i=1+LOCAL_MAX_IGNORE; i<sumLines.rows-1-LOCAL_MAX_IGNORE; i++)
	{
	    //get that value
	    FLOAT val = CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i, 0);
	    //check if local maximum
	    if( (val > CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i-1, 0))
		&& (val > CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i+1, 0))
		//		&& (i != maxLoc) 
		&& (val >= detectionThreshold) )
	    {

		//iterators for the two vectors
		vector<double>::iterator j;
		vector<int>::iterator k;
		//loop till we find the place to put it in descendingly
		for(j=sumLinesMax.begin(), k=sumLinesMaxLoc.begin(); 
		    j != sumLinesMax.end()  && val<= *j; j++,k++);
		//add its index
		sumLinesMax.insert(j, val);
		sumLinesMaxLoc.insert(k, i);
	    }	
	}
    }
    
    //check if didnt find local maxima
    if(sumLinesMax.size()==0 && max>detectionThreshold)
    {
	//put maximum
	sumLinesMaxLoc.push_back(maxLoc);
	sumLinesMax.push_back(max);
    }

//     //sort it descendingly
//     sort(sumLinesMax.begin(), sumLinesMax.end(), greater<double>());
//     //sort the indices
//     for (int i=0; i<(int)sumLinesMax.size(); i++)
// 	for (int j=i; j<(int)sumLinesMax.size(); j++)
// 	    if(sumLinesMax[i] == CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, 
// 					     sumLinesMaxLoc[j], 0))
// 	    {
// 		int k = sumLinesMaxLoc[j];
// 		sumLinesMaxLoc[j] = sumLinesMaxLoc[i];
// 		sumLinesMaxLoc[i] = k;
// 	    }
//     //sort(sumLinesMaxLoc.begin(), sumLinesMaxLoc.end(), greater<int>());

    //plot the line scores and the local maxima
#ifdef DEBUG_GET_STOP_LINES
//     gnuplot_ctrl *h =  mcvPlotMat1D(NULL, &sumLines, "Line Scores");
//     CvMat *y = mcvVector2Mat(sumLinesMax);
//     CvMat *x =  mcvVector2Mat(sumLinesMaxLoc);
//     mcvPlotMat2D(h, x, y);
//     //gnuplot_plot_xy(h, (double*)&sumLinesMaxLoc,(double*)&sumLinesMax, sumLinesMax.size(),""); 
//     cin.get();
//     gnuplot_close(h);
//     cvReleaseMat(&x);
//     cvReleaseMat(&y);
#endif
    
    
    //process the found maxima
    for (int i=0; i<(int)sumLinesMax.size(); i++)
    {
	//get subpixel accuracy
	double maxLocAcc = 
	    mcvGetLocalMaxSubPixel(
	        CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, MAX(sumLinesMaxLoc[i]-1,0), 0),
		CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, sumLinesMaxLoc[i], 0),
		CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, MIN(sumLinesMaxLoc[i]+1,maxLineLoc), 0) );
	maxLocAcc += sumLinesMaxLoc[i];
	maxLocAcc = MIN(MAX(0, maxLocAcc), maxLineLoc);

            
	//TODO: get line extent
	
	//put the extracted line
	Line line;
	switch (lineType)
        {
	case HV_LINES_HORIZONTAL:
	    line.startPoint.x = 0.5;             
	    line.startPoint.y = (FLOAT)maxLocAcc + .5;//sumLinesMaxLoc[i]+.5;
	    line.endPoint.x = inImage->width-.5; 
	    line.endPoint.y = line.startPoint.y;  
	    break;
	case HV_LINES_VERTICAL:
	    line.startPoint.x = (FLOAT)maxLocAcc + .5;//sumLinesMaxLoc[i]+.5;   
	    line.startPoint.y = .5;
	    line.endPoint.x = line.startPoint.x;     
	    line.endPoint.y = inImage->height-.5;  
	    break;
	}
	(*lines).push_back(line);
	if (lineScores)
	    (*lineScores).push_back(sumLinesMax[i]);
    }//for
    
    //clean
    cvReleaseMat(&sumLinesp);
    cvReleaseMat(&smooth);
    sumLinesMax.clear();
    sumLinesMaxLoc.clear();
    cvReleaseMat(&image);
}


/** This function detects lines in images using Hough transform
 * 
 * \param inImage input image
 * \param lines vector of lines to hold the results
 * \param lineScores scores of the detected lines (vector of floats)
 * \param rMin minimum r use for finding the lines (default 0)
 * \param rMax maximum r to find (default max(size(im)))
 * \param rStep step to use for binning (default is 2)
 * \param thetaMin minimum angle theta to look for (default 0) all in radians
 * \param thetaMax maximum angle theta to look for (default 2*pi)
 * \param thetaStep step to use for binning theta (default 5)
 * \param  binarize if to binarize the input image or use the raw values so that
 *	non-zero values are not treated as equal
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 * \param smoothScores whether to smooth scores detected or not
 * \param group whether to group nearby detections (1) or not (0 default)
 * \param groupThreshold the minimum distance used for grouping (default 10)
 */ 

void mcvGetHoughTransformLines(const CvMat *inImage, vector <Line> *lines,
			       vector <FLOAT> *lineScores,
			       FLOAT rMin, FLOAT rMax, FLOAT rStep, 
			       FLOAT thetaMin, FLOAT thetaMax,
			       FLOAT thetaStep, bool binarize, bool localMaxima, 
			       FLOAT detectionThreshold, bool smoothScores,
			       bool group, FLOAT groupTthreshold)
{

    CvMat * image = cvCloneMat(inImage); assert(image!=0);
    //binarize input image if to binarize
    if (binarize)
    {
        mcvBinarizeImage(image);   
    }

    //define the accumulator array: rows correspond to r and columns to theta
    int rBins = int((rMax-rMin)/rStep);
    int thetaBins = int((thetaMax-thetaMin)/thetaStep);
    CvMat *houghSpace = cvCreateMat(rBins, thetaBins, FLOAT_MAT_TYPE);
    assert(houghSpace!=0);
    //init to zero
    cvSet(houghSpace, cvRealScalar(0.0));

    //init values of r and theta
    FLOAT *rs = new FLOAT[rBins];
    FLOAT *thetas = new FLOAT[thetaBins];
    FLOAT r, theta;
    int ri, thetai;
    for (r=rMin+rStep/2,  ri=0 ; ri<rBins; ri++,r+=rStep) 
	rs[ri] = r;
    for (theta=thetaMin, thetai=0 ; thetai<thetaBins; thetai++,theta+=thetaStep) 
	thetas[thetai] = theta;


    //calculate r values for all theta and all points
    CvMat *rPoints = cvCreateMat(image->width*image->height, thetaBins, FLOAT_MAT_TYPE);
    cvSet(rPoints, cvRealScalar(-1));
    //loop on x
    double x=0.5, y=0.5;
    int i, j, k;
    for (i=0, x=0; i<image->width; x++,i++)
	//loop on y
	for (j=0, y=0; j<image->height; y++, j++)
	    //loop on theta
	    for (k=0; k<thetaBins; k++)
	    {
		//compute the r value and then subtract rMin and div by rStep 
		//to get the r bin index to which it belongs (0->rBins-1)
		if (CV_MAT_ELEM(*image, FLOAT_MAT_ELEM_TYPE, j, i) !=0)
		{
		    theta = thetas[k];
		    double rval = x * cos(theta) + y * sin(theta);
		    CV_MAT_ELEM(*rPoints, FLOAT_MAT_ELEM_TYPE, 
				i*image->height + j, k) = 
			FLOAT_MAT_ELEM_TYPE(int( ( rval - rMin) / rStep));
		}
		
	    }

//      SHOW_MAT(rPoints, "rPoints");
//      cin.get();

    //now we should accumulate the values into the approprate bins in the houghSpace
    for (ri=0; ri<rBins; ri++)
	for (thetai=0; thetai<thetaBins; thetai++)
	    for (i=0; i<image->width; i++)
		for (j=0; j<image->height; j++)
		{
		    //check if this cell belongs to that bin or not
		    if (CV_MAT_ELEM(*rPoints, FLOAT_MAT_ELEM_TYPE, 
				    i*image->height + j , thetai)==ri)
		    {
			//add the value of that pixel to that bin
			CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, ri, thetai) += 
			    CV_MAT_ELEM(*image, FLOAT_MAT_ELEM_TYPE, j, i);
		    }
		}


    //smooth hough transform

    //group detected maxima

    //get local maxima
    vector <double> maxLineScores;
    vector <CvPoint> maxLineLocs;
    if (localMaxima)
    {
	//get local maxima in the hough space
	mcvGetMatLocalMax(houghSpace, maxLineScores, maxLineLocs);

    }

    //get the maximum value
    double maxLineScore;
    CvPoint maxLineLoc;
    cvMinMaxLoc(houghSpace, 0, &maxLineScore, 0, &maxLineLoc);
    if (maxLineScores.size()==0 && maxLineScore>=detectionThreshold)
    {
	maxLineScores.push_back(maxLineScore);
	maxLineLocs.push_back(maxLineLoc);
    }
    

    //debug
    #ifdef DEBUG_GET_STOP_LINES
        //show the hough space
        SHOW_IMAGE(houghSpace, "Hough Space", 10);
	//SHOW_MAT(houghSpace, "Hough Space:");
    #endif 

    //process detected maxima and return detected line(s)
    for(int i=0; i<int(maxLineScores.size()); i++)
    {
	//check if above threshold
	if (maxLineScores[i]>=detectionThreshold)
	{
	    //get sub-pixel accuracy
	    
	    //get the two end points from the r-theta
	    Line line;
	    assert(maxLineLocs[i].x>=0 && maxLineLocs[i].x<thetaBins);
	    assert(maxLineLocs[i].y>=0 && maxLineLocs[i].y<rBins);
	    mcvIntersectLineRThetaWithBB(rs[maxLineLocs[i].y], thetas[maxLineLocs[i].x],
					 cvSize(image->cols, image->rows), &line);

	    
	    //get line extent
	    
	    //put the extracted line
	    lines->push_back(line);
	    if (lineScores)
		(*lineScores).push_back(maxLineScores[i]);
	    
	}
	//not above threshold
	else
	{
	    //exit out of the for loop, as the scores are sorted descendingly
	    break;
	}
    }

    //clean up
    cvReleaseMat(&image);
    cvReleaseMat(&houghSpace);
    cvReleaseMat(&rPoints);
    delete [] rs;
    delete [] thetas;
    maxLineScores.clear();
    maxLineLocs.clear();
}



/** This function binarizes the input image i.e. nonzero elements
 * becomen 1 and others are 0.
 * 
 * \param inImage input & output image
 */ 
void mcvBinarizeImage(CvMat *inImage)
{

    if (CV_MAT_TYPE(inImage->type)==FLOAT_MAT_TYPE)
    {
        for (int i=0; i<inImage->height; i++)
            for (int j=0; j<inImage->width; j++)
                if (CV_MAT_ELEM(*inImage, FLOAT_MAT_ELEM_TYPE, i, j) != 0.f)
                    CV_MAT_ELEM(*inImage, FLOAT_MAT_ELEM_TYPE, i, j)=1;
    }
    else if (CV_MAT_TYPE(inImage->type)==INT_MAT_TYPE)
    {
        for (int i=0; i<inImage->height; i++)
            for (int j=0; j<inImage->width; j++)
                if (CV_MAT_ELEM(*inImage, INT_MAT_ELEM_TYPE, i, j) != 0)
                    CV_MAT_ELEM(*inImage, INT_MAT_ELEM_TYPE, i, j)=1;
    }
    else
    {
        cerr << "Unsupported type in mcvBinarizeImage\n";
        exit(1);   
    }                          

}


/** This function gets the maximum value in a vector (row or column) 
 * and its location
 * 
 * \param inVector the input vector
 * \param max the output max value
 * \param maxLoc the location (index) of the first max
 * 
 */ 
#define MCV_VECTOR_MAX(type)  \
    /*row vector*/ \
    if (inVector->height==1) \
    { \
        /*initial value*/ \
        tmax = (double) CV_MAT_ELEM(*inVector, type, 0, inVector->width-1); \
        tmaxLoc = inVector->width-1; \
        /*loop*/ \
        for (int i=inVector->width-1-ignore; i>=0+ignore; i--) \
        { \
            if (tmax<CV_MAT_ELEM(*inVector, type, 0, i)) \
            { \
                tmax = CV_MAT_ELEM(*inVector, type, 0, i); \
                tmaxLoc = i; \
            } \
        } \
    } \
    /*column vector */ \
    else \
    { \
        /*initial value*/ \
        tmax = (double) CV_MAT_ELEM(*inVector, type, inVector->height-1, 0); \
        tmaxLoc = inVector->height-1; \
        /*loop*/ \
        for (int i=inVector->height-1-ignore; i>=0+ignore; i--) \
        { \
            if (tmax<CV_MAT_ELEM(*inVector, type, i, 0)) \
            { \
                tmax = (double) CV_MAT_ELEM(*inVector, type, i, 0); \
                tmaxLoc = i; \
            } \
        } \
    } \

void mcvGetVectorMax(const CvMat *inVector, double *max, int *maxLoc, int ignore)
{
    double tmax;
    int tmaxLoc;
            
    if (CV_MAT_TYPE(inVector->type)==FLOAT_MAT_TYPE)
    {
        MCV_VECTOR_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inVector->type)==INT_MAT_TYPE)
    {
        MCV_VECTOR_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorMax\n";
        exit(1); 
    }
    
    
    //return
    if (max)
        *max = tmax;
    if (maxLoc)
        *maxLoc = tmaxLoc;
}



/** This function gets the local maxima in a matrix and their positions 
 *  and its location
 * 
 * \param inMat input matrix
 * \param localMaxima the output vector of local maxima
 * \param localMaximaLoc the vector of locations of the local maxima, 
 *       where each location is cvPoint(x=col, y=row) zero-based
 * 
 */ 
#define MCV_MAT_LOCAL_MAX(type)  \
    /*loop on the matrix and get points that are larger than their*/ \
    /*neighboring 8 pixels*/ \
    for(int i=1; i<inMat->rows-1; i++) \
	for (int j=1; j<inMat->cols-1; j++) \
	{ \
	    /*get the current value*/ \
	    val = CV_MAT_ELEM(*inMat, type, i, j); \
	    /*check if it's larger than all its neighbors*/ \
	    if( val >= CV_MAT_ELEM(*inMat, type, i-1, j-1) && \
		val >= CV_MAT_ELEM(*inMat, type, i-1, j) && \
		val >= CV_MAT_ELEM(*inMat, type, i-1, j+1) && \
		val >= CV_MAT_ELEM(*inMat, type, i, j-1) && \
		val >= CV_MAT_ELEM(*inMat, type, i, j+1) && \
		val >= CV_MAT_ELEM(*inMat, type, i+1, j-1) && \
		val >= CV_MAT_ELEM(*inMat, type, i+1, j) && \
		val >= CV_MAT_ELEM(*inMat, type, i+1, j+1) ) \
	    { \
		/*found a local maxima, put it in the return vector*/ \
		/*in decending order*/ \
		/*iterators for the two vectors*/ \
		vector<double>::iterator k; \
		vector<CvPoint>::iterator l; \
		/*loop till we find the place to put it in descendingly*/ \
		for(k=localMaxima.begin(), l=localMaximaLoc.begin(); \
		    k != localMaxima.end()  && val<= *k; k++,l++); \
		/*add its index*/ \
		localMaxima.insert(k, val); \
		localMaximaLoc.insert(l, cvPoint(j, i)); \
	    } \
	} 

void mcvGetMatLocalMax(const CvMat *inMat, vector<double> &localMaxima, 
		     vector<CvPoint> &localMaximaLoc)
{

    double val;

    //check type
    if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_MAT_LOCAL_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE)
    {
        MCV_MAT_LOCAL_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetMatLocalMax\n";
        exit(1); 
    }
    
    //return
}


/** This function gets the local maxima in a vector and their positions 
 * 
 * \param inVec input vector
 * \param localMaxima the output vector of local maxima
 * \param localMaximaLoc the vector of locations of the local maxima, 
 * 
 */ 
#define MCV_VECTOR_LOCAL_MAX(type)  \
    /*loop on the vector and get points that are larger than their*/ \
    /*neighboring points*/ \
    if(inVec->height == 1) \
    { \
	for(int i=1; i<inVec->width-1; i++) \
	{ \
	    /*get the current value*/ \
	    val = CV_MAT_ELEM(*inVec, type, 0, i); \
	    /*check if it's larger than all its neighbors*/ \
	    if( val > CV_MAT_ELEM(*inVec, type, 0, i-1) && \
		val > CV_MAT_ELEM(*inVec, type, 0, i+1) ) \
	    { \
		/*found a local maxima, put it in the return vector*/ \
		/*in decending order*/ \
		/*iterators for the two vectors*/ \
		vector<double>::iterator k; \
		vector<int>::iterator l; \
		/*loop till we find the place to put it in descendingly*/ \
		for(k=localMaxima.begin(), l=localMaximaLoc.begin(); \
		    k != localMaxima.end()  && val<= *k; k++,l++); \
		/*add its index*/ \
		localMaxima.insert(k, val); \
		localMaximaLoc.insert(l, i); \
	    } \
        } \
    } \
    else \
    { \
	for(int i=1; i<inVec->height-1; i++) \
	{ \
	    /*get the current value*/ \
	    val = CV_MAT_ELEM(*inVec, type, i, 0); \
	    /*check if it's larger than all its neighbors*/ \
	    if( val > CV_MAT_ELEM(*inVec, type, i-1, 0) && \
		val > CV_MAT_ELEM(*inVec, type, i+1, 0) ) \
	    { \
		/*found a local maxima, put it in the return vector*/ \
		/*in decending order*/ \
		/*iterators for the two vectors*/ \
		vector<double>::iterator k; \
		vector<int>::iterator l; \
		/*loop till we find the place to put it in descendingly*/ \
		for(k=localMaxima.begin(), l=localMaximaLoc.begin(); \
		    k != localMaxima.end()  && val<= *k; k++,l++); \
		/*add its index*/ \
		localMaxima.insert(k, val); \
		localMaximaLoc.insert(l, i); \
	    } \
        } \
    } 

void mcvGetVectorLocalMax(const CvMat *inVec, vector<double> &localMaxima, 
		     vector<int> &localMaximaLoc)
{

    double val;

    //check type
    if (CV_MAT_TYPE(inVec->type)==FLOAT_MAT_TYPE)
    {
        MCV_VECTOR_LOCAL_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inVec->type)==INT_MAT_TYPE)
    {
        MCV_VECTOR_LOCAL_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorLocalMax\n";
        exit(1); 
    }
    
    //return
}


/** This function gets the qtile-th quantile of the input matrix
 * 
 * \param mat input matrix
 * \param qtile required input quantile probability
 * \return the returned value
 * 
 */
FLOAT mcvGetQuantile(const CvMat *mat, FLOAT qtile)
{
    //make it a row vector
    CvMat rowMat;
    cvReshape(mat, &rowMat, 0, 1);
    
    //get the quantile
    FLOAT qval;
    qval = quantile((FLOAT*) rowMat.data.ptr, rowMat.width, qtile);
    
    return qval;
}


/** This function thresholds the image below a certain value to the threshold
 * so: outMat(i,j) = inMat(i,j) if inMat(i,j)>=threshold
 *                 = threshold otherwise
 * 
 * \param inMat input matrix
 * \param outMat output matrix
 * \param threshold threshold value
 * 
 */
void mcvThresholdLower(const CvMat *inMat, CvMat *outMat, FLOAT threshold)
{

#define MCV_THRESHOLD_LOWER(type) \
     for (int i=0; i<inMat->height; i++) \
        for (int j=0; j<inMat->width; j++) \
            if ( CV_MAT_ELEM(*inMat, type, i, j)<threshold) \
                CV_MAT_ELEM(*outMat, type, i, j)=(type) 0; /*check it, was: threshold*/\
                
    //check if to copy into outMat or not
    if (inMat != outMat)
        cvCopy(inMat, outMat);
        
    //check type
    if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_THRESHOLD_LOWER(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE)
    {
        MCV_THRESHOLD_LOWER(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorMax\n";
        exit(1); 
    }                            
}

/** This function detects stop lines in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param stopLines a vector of returned stop lines in input image coordinates 
 * \param linescores a vector of line scores returned
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetStopLines(const CvMat *inImage, vector<Line> *stopLines, 
		     vector<FLOAT> *lineScores, const CameraInfo *cameraInfo, 
		      StopLinePerceptorConf *stopLineConf)

{
    //input size
    CvSize inSize = cvSize(inImage->width, inImage->height);
    
    //TODO: smooth image
    CvMat *image = cvCloneMat(inImage);
    //cvSmooth(image, image, CV_GAUSSIAN, 5, 5, 1, 1);

    IPMInfo ipmInfo;

//     //get the IPM size such that we have height of the stop line
//     //is 3 pixels
//     double ipmWidth, ipmHeight;
//     mcvGetIPMExtent(cameraInfo, &ipmInfo);
//     ipmHeight = 3*(ipmInfo.yLimits[1]-ipmInfo.yLimits[0]) / (stopLineConf->lineHeight/3.);
//     ipmWidth = ipmHeight * 4/3;    
//     //put into the conf
//     stopLineConf->ipmWidth = int(ipmWidth);
//     stopLineConf->ipmHeight = int(ipmHeight);

//     #ifdef DEBUG_GET_STOP_LINES
//     cout << "IPM width:" << stopLineConf->ipmWidth << " IPM height:" 
// 	 << stopLineConf->ipmHeight << "\n";
//     #endif
    
    
    //Get IPM
    CvSize ipmSize = cvSize((int)stopLineConf->ipmWidth, 
        (int)stopLineConf->ipmHeight);    
    CvMat * ipm;
    ipm = cvCreateMat(ipmSize.height, ipmSize.width, inImage->type);
    //mcvGetIPM(inImage, ipm, &ipmInfo, cameraInfo);    
    ipmInfo.vpPortion = stopLineConf->ipmVpPortion;
    mcvGetIPM(image, ipm, &ipmInfo, cameraInfo);
    
    //smooth the IPM
    //cvSmooth(ipm, ipm, CV_GAUSSIAN, 5, 5, 1, 1);
        
    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImage = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImage);
    #endif

           
    //compute stop line width: 2000 mm
    FLOAT stopLinePixelWidth = stopLineConf->lineWidth * 
        ipmInfo.xScale;
    //stop line pixel height: 12 inches = 12*25.4 mm
    FLOAT stopLinePixelHeight = stopLineConf->lineHeight  * 
        ipmInfo.yScale;
    //kernel dimensions
    //unsigned char wx = 2;
    //unsigned char wy = 2;
    FLOAT sigmax = stopLinePixelWidth;
    FLOAT sigmay = stopLinePixelHeight;
    
    #ifdef DEBUG_GET_STOP_LINES
    //cout << "Line width:" << stopLinePixelWidth << "Line height:" 
    //	 << stopLinePixelHeight << "\n";
    #endif

    //filter the IPM image
    mcvFilterLines(ipm, ipm, stopLineConf->kernelWidth, 
        stopLineConf->kernelHeight, sigmax, sigmay, 
        FILTER_LINE_HORIZONTAL);     
    

    //zero out negative values    
    mcvThresholdLower(ipm, ipm, 0);
    
    //compute quantile: .985
    FLOAT qtileThreshold = mcvGetQuantile(ipm, stopLineConf->lowerQuantile);
    mcvThresholdLower(ipm, ipm, qtileThreshold);

    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImageThresholded = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImageThresholded);    
    #endif


    
    //group stop lines
    switch(stopLineConf->groupingType)
    {
    //use HV grouping
    case GROUPING_TYPE_HV_LINES:
	//vector <Line> ipmStopLines;
	//vector <FLOAT> lineScores;
	mcvGetHVLines(ipm, stopLines, lineScores, HV_LINES_HORIZONTAL, 
		      stopLinePixelHeight, stopLineConf->binarize, 
		      stopLineConf->localMaxima, stopLineConf->detectionThreshold, 
		      stopLineConf->smoothScores);

	break;

    //use Hough Transform grouping
    case GROUPING_TYPE_HOUGH_LINES:
	//FLOAT rMin = 0.05*ipm->height, rMax = 0.4*ipm->height, rStep = 1;
	//FLOAT thetaMin = 88*CV_PI/180, thetaMax = 92*CV_PI/180, thetaStep = 1*CV_PI/180;
	bool group = false; FLOAT groupThreshold = 1;
	mcvGetHoughTransformLines(ipm, stopLines, lineScores, 
				  stopLineConf->rMin, stopLineConf->rMax, 
				  stopLineConf->rStep, stopLineConf->thetaMin, 
				  stopLineConf->thetaMax, stopLineConf->thetaStep, 
				  stopLineConf->binarize, 
				  stopLineConf->localMaxima,  
				  stopLineConf->detectionThreshold, 
				  stopLineConf->smoothScores,
				  group, groupThreshold);	    

	break;	
    }

    if(stopLineConf->getEndPoints)
    {
	//get line extent in IPM image
	for(int i=0; i<(int)stopLines->size(); i++)
	    mcvGetLineExtent(ipm, (*stopLines)[i], (*stopLines)[i]);
    }
        
    #ifdef DEBUG_GET_STOP_LINES
        vector <Line> dbIpmStopLines = *stopLines;
        //print out lineScores
        cout << "LineScores:";
        //for (int i=0; i<(int)lineScores->size(); i++)
	for (int i=0; i<1 && lineScores->size()>0; i++)
            cout << (*lineScores)[i] << " ";
        cout << "\n";
    #endif

    //check if returned anything
    if (stopLines->size()!=0)
    {                
        //convert the line into world frame
        for (unsigned int i=0; i<stopLines->size(); i++)
        {
            Line *line;
            line = &(*stopLines)[i];
            
            mcvPointImIPM2World(&(line->startPoint), &ipmInfo);
            mcvPointImIPM2World(&(line->endPoint), &ipmInfo);
        }
        
        //convert them from world frame into camera frame
	//
	//put a dummy line at the beginning till we check that cvDiv bug
	Line dummy = {{1.,1.},{2.,2.}};
	stopLines->insert(stopLines->begin(), dummy);
	//convert to mat and get in image coordinates
        CvMat *mat = cvCreateMat(2, 2*stopLines->size(), FLOAT_MAT_TYPE);
        mcvLines2Mat(stopLines, mat);
        stopLines->clear();
        mcvTransformGround2Image(mat, mat, cameraInfo);
	//get back to vector
        mcvMat2Lines(mat, stopLines);
	//remove the dummy line at the beginning
	stopLines->erase(stopLines->begin());
        //clear
        cvReleaseMat(&mat);
                
        //clip the lines found and get their extent
        for (unsigned int i=0; i<stopLines->size(); i++)
	{
	    //clip
            mcvIntersectLineWithBB(&(*stopLines)[i], inSize, &(*stopLines)[i]);
	    
	    //get the line extent
	    //mcvGetLineExtent(inImage, (*stopLines)[i], (*stopLines)[i]);
	}


    }        
    
    //debugging
    #ifdef DEBUG_GET_STOP_LINES
        //show the IPM image
        SHOW_IMAGE(dbIpmImage, "IPM image", 10);
        //thresholded ipm
        SHOW_IMAGE(dbIpmImageThresholded, "Thresholded IPM image", 10);    
        //draw lines in IPM image
        //for (int i=0; i<(int)dbIpmStopLines.size(); i++)
	for (int i=0; i<1 && dbIpmStopLines.size()>0; i++)
        {   
            mcvDrawLine(dbIpmImage, dbIpmStopLines[i], CV_RGB(0,0,0), 3);        
        }    
        SHOW_IMAGE(dbIpmImage, "IPM with lines", 10);
        //draw lines on original image
        //CvMat *image = cvCreateMat(inImage->height, inImage->width, CV_32FC3);
        //cvCvtColor(inImage, image, CV_GRAY2RGB);
        //CvMat *image = cvCloneMat(inImage);
        //for (int i=0; i<(int)stopLines->size(); i++)
	for (int i=0; i<1 && stopLines->size()>0; i++)
        {
            //SHOW_POINT((*stopLines)[i].startPoint, "start");
            //SHOW_POINT((*stopLines)[i].endPoint, "end");
            mcvDrawLine(image, (*stopLines)[i], CV_RGB(255,0,0), 3);
        }    
        SHOW_IMAGE(image, "Detected lines", 10);
        //cvReleaseMat(&image);
        cvReleaseMat(&dbIpmImage);
        cvReleaseMat(&dbIpmImageThresholded);
        dbIpmStopLines.clear();    
    #endif //DEBUG_GET_STOP_LINES 

    //clear
    cvReleaseMat(&ipm);
    cvReleaseMat(&image);
    //ipmStopLines.clear();    
}



/** This function converts an array of lines to a matrix (already allocated)
 * 
 * \param lines input vector of lines
 * \param size number of lines to convert
 * \return the converted matrix, it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * 
 * 
 */
void mcvLines2Mat(const vector<Line> *lines, CvMat *mat)
{
    //allocate the matrix
    //*mat = cvCreateMat(2, size*2, FLOAT_MAT_TYPE);
    
    //loop and put values
    int j;
    for (int i=0; i<(int)lines->size(); i++)
    {
        j = 2*i;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j) =
            (*lines)[i].startPoint.x;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j) = 
            (*lines)[i].startPoint.y;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j+1) =
             (*lines)[i].endPoint.x;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j+1) = 
            (*lines)[i].endPoint.y;
    }       
}


/** This function converts matrix into n array of lines
 * 
 * \param mat input matrix , it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * \param  lines the rerurned vector of lines
 * 
 * 
 */
void mcvMat2Lines(const CvMat *mat, vector<Line> *lines)
{

    Line line;
    //loop and put values
    for (int i=0; i<int(mat->width/2); i++)
    {
        int j = 2*i;
        //get the line
        line.startPoint.x = 
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j);
        line.startPoint.y =
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j); 
        line.endPoint.x =    
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j+1);
        line.endPoint.y =
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j+1);
        //push it
        lines->push_back(line);            
    }    
}



/** This function intersects the input line with the given bounding box
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineWithBB(const Line *inLine, const CvSize bbox,
    Line *outLine)
{
    //put output
    outLine->startPoint.x = inLine->startPoint.x;
    outLine->startPoint.y = inLine->startPoint.y;
    outLine->endPoint.x = inLine->endPoint.x;
    outLine->endPoint.y = inLine->endPoint.y;

    //check which points are inside
    bool startInside, endInside;
    startInside = mcvIsPointInside(inLine->startPoint, bbox);
    endInside = mcvIsPointInside(inLine->endPoint, bbox);
    
    //now check        
    if (!(startInside && endInside))
    {
        //difference
        FLOAT deltax, deltay;
        deltax = inLine->endPoint.x - inLine->startPoint.x;
        deltay = inLine->endPoint.y - inLine->startPoint.y;
        //hold parameters
        FLOAT t[4]={2,2,2,2};
        FLOAT xup, xdown, yleft, yright;
        
        //intersect with top and bottom borders: y=0 and y=bbox.height-1
        if (deltay==0) //horizontal line
        {
            xup = xdown = bbox.width+2;
        }
        else
        {
            t[0] = -inLine->startPoint.y/deltay;
            xup = inLine->startPoint.x + t[0]*deltax;
            t[1] = (bbox.height-inLine->startPoint.y)/deltay;
            xdown = inLine->startPoint.x + t[1]*deltax;
        }
        
        //intersect with left and right borders: x=0 and x=bbox.widht-1
        if (deltax==0) //horizontal line
        {
            yleft = yright = bbox.height+2;
        }
        else
        {
            t[2] = -inLine->startPoint.x/deltax;
            yleft = inLine->startPoint.y + t[2]*deltay;
            t[3] = (bbox.width-inLine->startPoint.x)/deltax;
            yright = inLine->startPoint.y + t[3]*deltay;
        }
        
        //points of intersection
        FLOAT_POINT2D pts[4] = {{xup, 0},{xdown,bbox.height},
               {0, yleft},{bbox.width, yright}};
                       
        //now decide which stays and which goes
        int i;
        if (!startInside)
        {
            bool cont=true;
            for (i=0; i<4 && cont; i++)
            {
                if (t[i]>=0 && t[i]<=1 && mcvIsPointInside(pts[i],bbox) &&
		    !(pts[i].x == outLine->endPoint.x && 
		      pts[i].y == outLine->endPoint.y) )
                {
                    outLine->startPoint.x = pts[i].x;
                    outLine->startPoint.y = pts[i].y;
                    t[i] = 2;
                    cont = false; 
                }
            }
        }
        if (!endInside)
        {
            bool cont=true;
            for (i=0; i<4 && cont; i++)
            {
                if (t[i]>=0 && t[i]<=1 && mcvIsPointInside(pts[i],bbox) &&
		    !(pts[i].x == outLine->startPoint.x && 
		      pts[i].y == outLine->startPoint.y) )
                {
                    outLine->endPoint.x = pts[i].x;
                    outLine->endPoint.y = pts[i].y;
                    t[i] = 2;
                    cont = false;
                }
            }           
        }   
    }
}


/** This function intersects the input line (given in r and theta) with 
 *  the given bounding box where the line is represented by: 
 *  x cos(theta) + y sin(theta) = r
 * 
 * \param r the r value for the input line
 * \param theta the theta value for the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineRThetaWithBB(FLOAT r, FLOAT theta, const CvSize bbox,
    Line *outLine)
{
    //hold parameters
    double xup, xdown, yleft, yright;
        
    //intersect with top and bottom borders: y=0 and y=bbox.height-1
    if (cos(theta)==0) //horizontal line
    {
	xup = xdown = bbox.width+2;
    }
    else
    {
	xup = r / cos(theta);
	xdown = (r-bbox.height*sin(theta))/cos(theta);
    }
        
    //intersect with left and right borders: x=0 and x=bbox.widht-1
    if (sin(theta)==0) //horizontal line
    {
	yleft = yright = bbox.height+2;
    }
    else
    {
	yleft = r/sin(theta);
	yright = (r-bbox.width*cos(theta))/sin(theta);
    }
        
    //points of intersection
    FLOAT_POINT2D pts[4] = {{xup, 0},{xdown,bbox.height},
			    {0, yleft},{bbox.width, yright}};

    //get the starting point
    int i;
    for (i=0; i<4; i++)
    {
	//if point inside, then put it
	if(mcvIsPointInside(pts[i], bbox))
	{
	    outLine->startPoint.x = pts[i].x;
	    outLine->startPoint.y = pts[i].y;
	    //get out of for loop
	    break;
	}
    }
    //get the ending point
    for (i++; i<4; i++)
    {
	//if point inside, then put it
	if(mcvIsPointInside(pts[i], bbox))
	{
	    outLine->endPoint.x = pts[i].x;
	    outLine->endPoint.y = pts[i].y;
	    //get out of for loop
	    break;
	}
    }                       
}


/** This function checks if the given point is inside the bounding box
 * specified
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D point, CvSize bbox)
{
    return (point.x>=0 && point.x<=bbox.width 
        && point.y>=0 && point.y<=bbox.height) ? true : false;    
} 


/** This function converts an INT mat into a FLOAT mat (already allocated)
 * 
 * \param inMat input INT matrix
 * \param outMat output FLOAT matrix
 * 
 */
void mcvMatInt2Float(const CvMat *inMat, CvMat *outMat)
{
    for (int i=0; i<inMat->height; i++)
        for (int j=0; j<inMat->width; j++)
            CV_MAT_ELEM(*outMat, FLOAT_MAT_ELEM_TYPE, i, j) = 
                (FLOAT_MAT_ELEM_TYPE) 
                CV_MAT_ELEM(*inMat, INT_MAT_ELEM_TYPE, i, j)/255;
}


/** This function draws a line onto the passed image
 * 
 * \param image the input iamge
 * \param line input line
 * \param line color
 * \param width line width
 * 
 */
void mcvDrawLine(CvMat *image, Line line, CvScalar color, int width)
{
    cvLine(image, cvPoint((int)line.startPoint.x,(int)line.startPoint.y),
            cvPoint((int)line.endPoint.x,(int)line.endPoint.y), 
            color, width);
}

/** This initializes the stoplineperceptorinfo structure
 * 
 * \param fileName the input file name
 * \param stopLineConf the structure to fill
 *
 * 
 */
 void mcvInitStopLinePerceptorConf(char * const fileName, 
    StopLinePerceptorConf *stopLineConf)
{
  //parsed camera data
    StopLinePerceptorParserInfo stopLineParserInfo;
    //read the data
    assert(stopLinePerceptorParser_configfile(fileName, &stopLineParserInfo, 0, 1, 1)==0);
    //init the strucure
    stopLineConf->ipmWidth = stopLineParserInfo.ipmWidth_arg;
    stopLineConf->ipmHeight = stopLineParserInfo.ipmHeight_arg;
    stopLineConf->lineWidth = stopLineParserInfo.lineWidth_arg;
    stopLineConf->lineHeight = stopLineParserInfo.lineHeight_arg;
    stopLineConf->kernelWidth = stopLineParserInfo.kernelWidth_arg;
    stopLineConf->kernelHeight = stopLineParserInfo.kernelHeight_arg;
    stopLineConf->lowerQuantile = 
        stopLineParserInfo.lowerQuantile_arg;
    stopLineConf->localMaxima = 
        stopLineParserInfo.localMaxima_arg;
    stopLineConf->groupingType = stopLineParserInfo.groupingType_arg;
    stopLineConf->binarize = stopLineParserInfo.binarize_arg;
    stopLineConf->detectionThreshold = 
        stopLineParserInfo.detectionThreshold_arg;
    stopLineConf->smoothScores = 
        stopLineParserInfo.smoothScores_arg;
    stopLineConf->rMin = stopLineParserInfo.rMin_arg;
    stopLineConf->rMax = stopLineParserInfo.rMax_arg;
    stopLineConf->rStep = stopLineParserInfo.rStep_arg;
    stopLineConf->thetaMin = stopLineParserInfo.thetaMin_arg * CV_PI/180;
    stopLineConf->thetaMax = stopLineParserInfo.thetaMax_arg * CV_PI/180;
    stopLineConf->thetaStep = stopLineParserInfo.thetaStep_arg * CV_PI/180;
    stopLineConf->ipmVpPortion = stopLineParserInfo.ipmVpPortion_arg;
    stopLineConf->getEndPoints = stopLineParserInfo.getEndPoints_arg;
}
        
void SHOW_LINE(const Line line, char str[]) 
{
    cout << str;
    cout << "(" << line.startPoint.x << "," << line.startPoint.y << ")";
    cout << "->";
    cout << "(" << line.endPoint.x << "," << line.endPoint.y << ")";
    cout << "\n";
} 


/** This fits a parabola to the entered data to get
 * the location of local maximum with sub-pixel accuracy
 * 
 * \param val1 first value
 * \param val2 second value
 * \param val3 third value
 *
 * \return the computed location of the local maximum
 */
double mcvGetLocalMaxSubPixel(double val1, double val2, double val3)
{
    //build an array to hold the x-values
    double Xp[] = {1, -1, 1, 0, 0, 1, 1, 1, 1};
    CvMat X = cvMat(3, 3, CV_64FC1, Xp);

    //array to hold the y values
    double yp[] = {val1, val2, val3};
    CvMat y = cvMat(3, 1, CV_64FC1, yp);

    //solve to get the coefficients
    double Ap[3];
    CvMat A = cvMat(3, 1, CV_64FC1, Ap);
    cvSolve(&X, &y, &A, CV_SVD);

    //get the local max
    double max;
    max = -0.5 * Ap[1] / Ap[0];

    //return
    return max;        
}

/** This functions implements Bresenham's algorithm for getting pixels of the
 * line given its two endpoints

 * 
 * \param line the input line
 * \param x a vector of x locations for the line pixels (0-based)
 * \param y a vector of y locations for the line pixels (0-based)
 *
 */
void mcvGetLinePixels(const Line &line, vector<int> &x, vector<int> &y)
{
    //get two end points
    CvPoint start;
    start.x  = int(line.startPoint.x); start.y = int(line.startPoint.y);
    CvPoint end;
    end.x = int(line.endPoint.x); end.y = int(line.endPoint.y);

    //get deltas
    int deltay = end.y - start.y;
    int deltax = end.x - start.x;

    //check if slope is steep, then reflect the line along y=x i.e. swap x and y
    bool steep = false;
    if (abs(deltay) > abs(deltax))
    {
	steep = true;
	//swap x and y 
	int t;
	t = start.x;
	start.x = start.y;
	start.y = t;
	t = end.x;
	end.x = end.y;
	end.y = t;
    }


    //check to make sure we are going right
    if(start.x>end.x)
    {
	//swap the two points
	CvPoint t = start;
	start = end;
	end = t;
    }


    //get deltas again
    deltay = end.y - start.y;
    deltax = end.x - start.x;

    //error
    int error = 0;

    //delta error
    int deltaerror = abs(deltay);

    //ystep
    int ystep = -1;
    if (deltay>=0)
    {
	ystep = 1;
    }

    //loop
    int i, j;
    j = start.y;
    for (i=start.x; i<=end.x; i++)
    {
	//put the new point
	if(steep)
	{
	    x.push_back(j);
	    y.push_back(i);
	}
	else
	{
	    x.push_back(i);
	    y.push_back(j);
	}

	//adjust error
	error += deltaerror;
	//check
	if(2*error>=deltax)
	{
	    j = j + ystep;
	    error -= deltax;
	}
    }
}

/** This functions implements Bresenham's algorithm for getting pixels of the
 * line given its two endpoints

 * 
 * \param im the input image
 * \param inLine the input line
 * \param outLine the output line
 *
 */
void mcvGetLineExtent(const CvMat *im, const Line &inLine, Line &outLine)
{

    //first clip the input line to the image coordinates
    Line line = inLine;
    mcvIntersectLineWithBB(&inLine, cvSize(im->width-1, im->height-1), &line);
    
    //then get the pixel values of the line in the image
    vector<int> x, y;
    mcvGetLinePixels(line, x, y);

    //check which way to shift the line to get multiple readings
    bool changey = false;
    if (fabs(line.startPoint.x-line.endPoint.x) > 
	fabs(line.startPoint.y-line.endPoint.y))
    {
	//change the y-coordiantes
	changey = true;
    }
    char changes[] = {0, -1, 1};//, -2, 2};
    int numChanges = 3;
    
    //loop on the changes and get possible extents
    vector<int> startLocs;
    vector<int> endLocs;
    int endLoc;
    int startLoc;
    CvMat *pix = cvCreateMat(1, im->width, FLOAT_MAT_TYPE);
    CvMat *rstep = cvCreateMat(pix->height, pix->width, FLOAT_MAT_TYPE);
    CvMat *fstep = cvCreateMat(pix->height, pix->width, FLOAT_MAT_TYPE);
    for (int c=0; c<numChanges; c++)
    {

	//get the pixels
	for(int i=0; i<(int)x.size(); i++)
	{
	    CV_MAT_ELEM(*pix, FLOAT_MAT_ELEM_TYPE, 0, i) = 
		CV_MAT_ELEM(*im, FLOAT_MAT_ELEM_TYPE, 
			    //    y[i], x[i]);
			    changey ? min(max(y[i]+changes[c],0),im->height-1) : y[i], 
			    changey ? x[i] : min(max(x[i]+changes[c],0),im->width-1));
	}
	//remove the mean
	CvScalar mean = cvAvg(pix);
	cvSubS(pix, mean, pix); 

	//now convolve with rising step to get start point
	FLOAT_MAT_ELEM_TYPE stepp[] = {-0.3000, -0.2, -0.1, 0, 0, 0.1, 0.2, 0.3, 0.4};
	// {-0.6, -0.4, -0.2, 0.2, 0.4, 0.6};
	int stepsize = 9;
	//{-0.2, -0.4, -0.2, 0, 0, 0.2, 0.4, 0.2}; //{-.75, -.5, .5, .75};
	CvMat step = cvMat(1, stepsize, FLOAT_MAT_TYPE, stepp);
	//	  SHOW_MAT(&step,"step");
	//smooth
	//	  FLOAT_MAT_ELEM_TYPE smoothp[] = {.25, .5, .25};
	//CvMat smooth = cvMat(1, 3, FLOAT_MAT_TYPE, smoothp);
	//cvFilter2D(&step, &step, &smooth);
	//SHOW_MAT(&step,"smoothed step");
	//convolve	
	cvFilter2D(pix, rstep, &step);
	//get local max
	//     vector<double> localMax;
	//     vector<int> localMaxLoc;
	//     mcvGetVectorLocalMax(rstep, localMax, localMaxLoc);
	//     int startLoc = localMaxLoc[0];
	double max;
	mcvGetVectorMax(rstep, &max, &startLoc, 0);
	//check if zero
	if(max==0)
	    startLoc = startLocs[c-1];
	
	//convolve with falling step to get end point
	//cvFlip(&step, NULL, 1);
	//convolve
	//cvFilter2D(pix, fstep, &step);
	//get local max
	//     localMax.clear();
	//     localMaxLoc.clear();
	//     mcvGetVectorLocalMax(fstep, localMax, localMaxLoc);
	//     int endLoc = localMaxLoc[0];
	//take the negative
	cvConvertScale(rstep, fstep, -1);
	mcvGetVectorMax(fstep, &max, &endLoc, 0);
	//check if zero
	if(max==0)
	    endLoc = endLocs[c-1];
	if(endLoc<=startLoc)
	    endLoc = im->width-1;

	//put into vectors
	startLocs.push_back(startLoc);
	endLocs.push_back(endLoc);
    }

    //get median
    startLoc = quantile(startLocs, 0);
    endLoc = quantile(endLocs, 1);
    //    for (int i=0; i<(int)startLocs.size(); i++) cout << startLocs[i] << "  ";
    //cout << "\n";
    //for (int i=0; i<(int)endLocs.size(); i++) cout << endLocs[i] << "  ";

    //get the end-point
    outLine.startPoint.x = x[startLoc]; outLine.startPoint.y = y[startLoc];
    outLine.endPoint.x = x[endLoc]; outLine.endPoint.y = y[endLoc];
    
    //clear
    cvReleaseMat(&pix);
    cvReleaseMat(&rstep);
    cvReleaseMat(&fstep);
//     localMax.clear();
//     localMaxLoc.clear();
		      startLocs.clear();
		      endLocs.clear();
}


void dummy()
{
}

