#ifndef LINETRACK_HH_
#define LINETRACK_HH_

/**
 * \file LineTrack.hh
 * \author Mohamed Aly
 * \date 31 July 2007
 * \brief This file contains the header definitions for the LineTrack class
 *
 */


#include "StopLinePerceptor.hh"
#include <map/MapId.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <interfaces/StereoImageBlob.h>


#include <vector>

/**
 * \class Track
 * \brief This class represents a tracked line through time.
 *
 *
 *
 */

class Track
{


public:


    ///constructor
    Track(long frameId = -1, float score = -10) 
    {
	this->numAbsentFrames = 0;
	this->numSeenFrames = 0;
	this->frameId = frameId;
	this->frameLastSeen = frameId;
	this->frameLastUpdated = frameId;
	this->score = score;
	this->id = MapId(-1980);
	this->remove = false;
	this->valid = false;
    }


    ///destructor
    virtual ~Track() 
    {}

    ///track id
    MapId id;

    ///number of consecutive frames it's absent in 
    int numAbsentFrames;

    ///number of consecutive frames it's seen in
    int numSeenFrames;

    ///frame this track was created
    long frameId;

    ///score for the track
    float score;

    ///remove
    bool remove;

    ///frame this track was last seen
    long frameLastSeen;

    ///frame this track was last updated with merging track
    long frameLastUpdated;

    ///is it a valid track i.e. has been seen long enough or hasn't been missed long enough
    bool valid;

    /**
     * \brief compares two tracks and checks if to merge them
     * \param sp the input track
     *
     * \return true if they should be merged, false otherwise
     */
    virtual bool checkMerge(const Track&) {return false;};

    /**
     * \brief merges two tracks together
     * \param sp the input track
     * 
     */
    virtual int merge(const Track&);

    /**
     * \brief gets a unique map id for this spline track
     * 
     * \param mapIdPrefix the prefix to use for the map id
     */
    virtual int getMapId(MapId& mapIdPrefix);

    /// \brief send track to map
    virtual int sendToMap(CMapElementTalker&, StereoImageBlob* stereoBlob, bool rightCamera = false) 
    {return 0;};

    /**
     * \brief gets a unique map id for this spline track
     * 
     * \param mapTalker the map element talker to use 
     */
    virtual int deleteFromMap(CMapElementTalker& mapTalker);


    ///static member for id creation
    static int idState;

    /**
     * \brief This functions destroys a vector of Tracks 
     * \param tracks the tracks to destroy
     * 
     */
    static int DestroyTracks(vector<Track*>& tracks);

};


/**
 * \class SplineTrack
 * \brief This class represents a tracked spline through time.
 *
 *
 *
 */

class SplineTrack : public Track
{

public:

    ///default constructor
    SplineTrack(long frameId = -1, float score = -10.) :
	Track(frameId, score)
    {}

    ///default constructor
    SplineTrack(const Spline& spline, long frameId = -1, float score = -10.) : 
	Track(frameId, score)
    {
	this->spline = spline;
    }

    ///destructor
    ~SplineTrack()
    {}


    ///the spline this track represents
    Spline spline;


    ///thetaThreshold Angle threshold for merging splines (radians)
    static float MERGE_THETA_THRESHOLD;
    ///R threshold (distance from origin) for merging splines
    static float MERGE_R_THRESHOLD;
    ///Mean theta threshold Angle threshold for merging splines (radians)
    static float MERGE_MEAN_THETA_THRESHOLD;
    ///Mean r threshold (distance from origin) for merging splines
    static float MERGE_MEAN_R_THRESHOLD;
    ///Distance threshold between spline cetroids for merging
    static float MERGE_CENTROID_THRESHOLD;
    

    /**
     * \brief compares two splins and checks if to merge them
     * \param sp the input spline
     * \return true if they should be merged, false otherwise
     */
    bool checkMerge(const Track &sp);

    /**
     * \brief merges two spline tracks together
     * \param sp the input spline
     * 
     */
    int merge(const Track &sp);


    /**
     * \brief sends this track to the map
     *
     * \param mapTalker the map element talker to use 
     * \param stereoBlob the stereo blob to use for conversion to local frame
     * \param rightCamera use the right camera instead of the left camera
     */
    virtual int sendToMap(CMapElementTalker& mapTalker, StereoImageBlob* stereoBlob, bool rightCamera = false);



    /**
     * \brief This functions creates a vector of SplineTrack objects with the passed in splines
     * \param splines the input splines
     * \param scores the splien scores
     * \param frameId the current frame id
     *
     * \return a vector of SplineTrack objects
     */
    static vector<Track*>  CreateSplineTracks(const vector<Spline>& splines, 
					     const vector<float>& scores,
					     long frameId);

    
};



/**
 * \class LineTrack
 * \brief This class represents a tracked straight line through time.
 *
 */

class LineTrack : public Track
{

public:

    ///default constructor
    LineTrack(long frameId = -1, float score = -10.) :
	Track(frameId, score)
    {}

    ///default constructor
    LineTrack(const Line& line, long frameId = -1, float score = -10.) : 
	Track(frameId, score)
    {
	this->line = line;
    }

    ///destructor
    ~LineTrack()
    {}


    ///the line this track represents
    Line line;


    ///thetaThreshold Angle threshold for merging lines (radians)
    static float MERGE_THETA_THRESHOLD;
    ///R threshold (distance from origin) for merging lines
    static float MERGE_R_THRESHOLD;
    

    /**
     * \brief compares two lines and checks if to merge them
     * \param line the input line
     * \return true if they should be merged, false otherwise
     */
    bool checkMerge(const Track &line);

    /**
     * \brief merges two line tracks together
     * \param sp the input line
     * 
     */
    int merge(const Track &sp);


    /**
     * \brief sends this track to the map
     *
     * \param mapTalker the map element talker to use 
     * \param stereoBlob the stereo blob to use for conversion to local frame
     * \param rightCamera use the right camera instead of the left camera
     */
    virtual int sendToMap(CMapElementTalker& mapTalker, StereoImageBlob* stereoBlob, bool rightCamera = false);



    /**
     * \brief This functions creates a vector of LineTrack objects with the passed in lines
     * \param lines the input lines
     * \param scores the line scores
     * \param frameId the current frame id
     *
     * \return a vector of LineTrack objects
     */
    static vector<Track*>  CreateLineTracks(const vector<Line>& lines, 
					     const vector<float>& scores,
					     long frameId);

    
};

#endif
