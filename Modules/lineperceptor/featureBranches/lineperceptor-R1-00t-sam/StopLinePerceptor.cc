

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <math.h>

using namespace std;

#include "cv.h"
#include "highgui.h"

#include "mcv.hh"
#include "InversePerspectiveMapping.hh"
#include "StopLinePerceptor.hh"

#include "StopLinePerceptorOpt.h"

#include "ranker.h"

//#include "gnuplot_i.h"

/**
 * This function filters the input image looking for horizontal
 * or vertical lines with specific width or height.
 *
 * \param inImage the input image
 * \param outImage the output image in IPM
 * \param wx width of kernel window in x direction = 2*wx+1 
 * (default 2)
 * \param wy width of kernel window in y direction = 2*wy+1 
 * (default 2)
 * \param sigmax std deviation of kernel in x (default 1)
 * \param sigmay std deviation of kernel in y (default 1)
 * \param lineType type of the line
 *      FILTER_LINE_HORIZONTAL (default)
 *      FILTER_LINE_VERTICAL
 */ 
 void mcvFilterLines(const CvMat *inImage, CvMat *outImage,
    unsigned char wx, unsigned char wy, FLOAT sigmax,
    FLOAT sigmay, unsigned char lineType)
{
    //define the two kernels
    //this is for 7-pixels wide
//     FLOAT_MAT_ELEM_TYPE derivp[] = {-2.328306e-10, -6.984919e-09, -1.008157e-07, -9.313226e-07, -6.178394e-06, -3.129616e-05, -1.255888e-04, -4.085824e-04, -1.092623e-03, -2.416329e-03, -4.408169e-03, -6.530620e-03, -7.510213e-03, -5.777087e-03, -5.777087e-04, 6.932504e-03, 1.372058e-02, 1.646470e-02, 1.372058e-02, 6.932504e-03, -5.777087e-04, -5.777087e-03, -7.510213e-03, -6.530620e-03, -4.408169e-03, -2.416329e-03, -1.092623e-03, -4.085824e-04, -1.255888e-04, -3.129616e-05, -6.178394e-06, -9.313226e-07, -1.008157e-07, -6.984919e-09, -2.328306e-10};
//     int derivLen = 35;
//     FLOAT_MAT_ELEM_TYPE smoothp[] = {2.384186e-07, 5.245209e-06, 5.507469e-05, 3.671646e-04, 1.744032e-03, 6.278515e-03, 1.778913e-02, 4.066086e-02, 7.623911e-02, 1.185942e-01, 1.541724e-01, 1.681881e-01, 1.541724e-01, 1.185942e-01, 7.623911e-02, 4.066086e-02, 1.778913e-02, 6.278515e-03, 1.744032e-03, 3.671646e-04, 5.507469e-05, 5.245209e-06, 2.384186e-07};
//     int smoothLen = 23;


    CvMat fx;
    CvMat fy;
    //create the convoultion kernel
    switch (lineType)
    {
        case FILTER_LINE_HORIZONTAL:
	    {
		//this is for 5-pixels wide
		FLOAT_MAT_ELEM_TYPE derivp[] = {-2.384186e-07, -4.768372e-06, -4.482269e-05, -2.622604e-04, -1.064777e-03, -3.157616e-03, -6.976128e-03, -1.136112e-02, -1.270652e-02, -6.776810e-03, 6.776810e-03, 2.156258e-02, 2.803135e-02, 2.156258e-02, 6.776810e-03, -6.776810e-03, -1.270652e-02, -1.136112e-02, -6.976128e-03, -3.157616e-03, -1.064777e-03, -2.622604e-04, -4.482269e-05, -4.768372e-06, -2.384186e-07};
		int derivLen = 25;
		FLOAT_MAT_ELEM_TYPE smoothp[] = {2.384186e-07, 5.245209e-06, 5.507469e-05, 3.671646e-04, 1.744032e-03, 6.278515e-03, 1.778913e-02, 4.066086e-02, 7.623911e-02, 1.185942e-01, 1.541724e-01, 1.681881e-01, 1.541724e-01, 1.185942e-01, 7.623911e-02, 4.066086e-02, 1.778913e-02, 6.278515e-03, 1.744032e-03, 3.671646e-04, 5.507469e-05, 5.245209e-06, 2.384186e-07};
		int smoothLen = 23;
		//horizontal is smoothing and vertical is derivative
		fx = cvMat(1, smoothLen, FLOAT_MAT_TYPE, smoothp);
		fy = cvMat(derivLen, 1, FLOAT_MAT_TYPE, derivp);
	    }
	    
            break;  
        
        case FILTER_LINE_VERTICAL:
	    {
		//this is for 5-pixels wide
		FLOAT_MAT_ELEM_TYPE derivp[] = {1.000000e-11, 8.800000e-10, 3.531000e-08, 8.536000e-07, 1.383415e-05, 1.581862e-04, 1.306992e-03, 7.852691e-03, 3.402475e-02, 1.038205e-01, 2.137474e-01, 2.781496e-01, 2.137474e-01, 1.038205e-01, 3.402475e-02, 7.852691e-03, 1.306992e-03, 1.581862e-04, 1.383415e-05, 8.536000e-07, 3.531000e-08, 8.800000e-10, 1.000000e-11};
		int derivLen = 23;
		FLOAT_MAT_ELEM_TYPE smoothp[] = {-1.000000e-03, -2.200000e-02, -1.480000e-01, -1.940000e-01, 7.300000e-01, -1.940000e-01, -1.480000e-01, -2.200000e-02, -1.000000e-03};
		int smoothLen = 9;
		//horizontal is derivative and vertical is smoothing
		fy = cvMat(1, smoothLen, FLOAT_MAT_TYPE, smoothp);
		fx = cvMat(derivLen, 1, FLOAT_MAT_TYPE, derivp);
	    }
            break;
    }

    #ifdef DEBUG_GET_STOP_LINES
    //SHOW_MAT(kernel, "Kernel:");
    #endif
    
    //do the filtering
    cvFilter2D(inImage, outImage, &fx);
    cvFilter2D(outImage, outImage, &fy);


//     CvMat *deriv = cvCreateMat
//     //define x
//     CvMat *x = cvCreateMat(2*wx+1, 1, FLOAT_MAT_TYPE);
//     //define y
//     CvMat *y = cvCreateMat(2*wy+1, 1, FLOAT_MAT_TYPE);
        
//     //create the convoultion kernel
//     switch (lineType)
//     {
//         case FILTER_LINE_HORIZONTAL:
//             //guassian in x direction
// 	    mcvGetGaussianKernel(x, wx, sigmax);
//             //derivative of guassian in y direction
//             mcvGet2DerivativeGaussianKernel(y, wy, sigmay);            
//             break;  
        
//         case FILTER_LINE_VERTICAL:
//             //guassian in y direction
//             mcvGetGaussianKernel(y, wy, sigmay);
//             //derivative of guassian in x direction
//             mcvGet2DerivativeGaussianKernel(x, wx, sigmax);            
//             break;
//     }
    
//     //combine the 2D kernel
//     CvMat *kernel = cvCreateMat(2*wy+1, 2*wx+1, FLOAT_MAT_TYPE);
//     cvGEMM(y, x, 1, 0, 1, kernel, CV_GEMM_B_T);
    
//     //subtract the mean
//     CvScalar mean = cvAvg(kernel);
//     cvSubS(kernel, mean, kernel); 
    
//     #ifdef DEBUG_GET_STOP_LINES
//     //SHOW_MAT(kernel, "Kernel:");
//     #endif
    
//     //do the filtering
//     cvFilter2D(inImage, outImage, kernel);
    
//     //clean
//     cvReleaseMat(&x);
//     cvReleaseMat(&y);
//     cvReleaseMat(&kernel);            
}

/**
 * This function gets a 1-D gaussian filter with specified
 * std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGetGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma)
{
    //get variance
    sigma *= sigma;
    
    //get the kernel
    for (double i=-w; i<=w; i++)
        CV_MAT_ELEM(*kernel, FLOAT_MAT_ELEM_TYPE, int(i+w), 0) = 
           (FLOAT_MAT_ELEM_TYPE) exp(-(.5/sigma)*(i*i));    
}

/**
 * This function gets a 1-D second derivative gaussian filter 
 * with specified std deviation and range
 * 
 * \param kernel input mat to hold the kernel (2*w+1x1) 
 *      column vector (already allocated)
 * \param w width of kernel is 2*w+1
 * \param sigma std deviation
 */
void mcvGet2DerivativeGaussianKernel(CvMat *kernel, 
    unsigned char w, FLOAT sigma)
{
    //get variance
    sigma *= sigma;
    
    //get the kernel
    for (double i=-w; i<=w; i++)
        CV_MAT_ELEM(*kernel, FLOAT_MAT_ELEM_TYPE, int(i+w), 0) = 
           (FLOAT_MAT_ELEM_TYPE)  
           (exp(-.5*i*i)/sigma - (i*i)*exp(-(.5/sigma)*i*i)/(sigma*sigma));    
}


/** This function groups the input filtered image into 
 * horizontal or vertical lines.
 * 
 * \param inImage input image
 * \param lines returned detected lines (vector of points)
 * \param lineScores scores of the detected lines (vector of floats)
 * \param lineType type of lines to detect
 *      HV_LINES_HORIZONTAL (default) or HV_LINES_VERTICAL
 * \param linePixelWidth width (or height) of lines to detect
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 * \param smoothScores whether to smooth scores detected or not
 */ 
void mcvGetHVLines(const CvMat *inImage, vector <Line> *lines,
    vector <FLOAT> *lineScores, unsigned char lineType, 
    FLOAT linePixelWidth, bool binarize, bool localMaxima, 
    FLOAT detectionThreshold, bool smoothScores)
{
    CvMat * image = cvCloneMat(inImage);
    //binarize input image if to binarize
    if (binarize)
    {
        mcvBinarizeImage(image);   
    }

    //get sum of lines through horizontal or vertical
    //sumLines is a column vector
    CvMat sumLines, *sumLinesp;
    int maxLineLoc;
    switch (lineType)
    {
    case HV_LINES_HORIZONTAL:
        sumLinesp = cvCreateMat(image->height, 1, FLOAT_MAT_TYPE);
        cvReduce(image, sumLinesp, 1, CV_REDUCE_SUM); //_AVG
        cvReshape(sumLinesp, &sumLines, 0, 0);
        //max location for a detected line
        maxLineLoc = image->height-1;
        break;
    case HV_LINES_VERTICAL:
        sumLinesp = cvCreateMat(1, image->width, FLOAT_MAT_TYPE);
        cvReduce(image, sumLinesp, 0, CV_REDUCE_SUM); //_AVG
        cvReshape(sumLinesp, &sumLines, 0, image->width);
        //max location for a detected line
        maxLineLoc = image->width-1;
        break;
    }
    //SHOW_MAT(&sumLines, "sumLines:");
    
    //smooth it
    int smoothWidth = 10;//int(linePixelWidth+.5)+1;
    CvMat *smooth = cvCreateMat(smoothWidth, 1, FLOAT_MAT_TYPE);
    cvSet(smooth, cvRealScalar(1./smooth->rows));  //was 1 1./smooth->rows
    if (smoothScores)
    {
	cvFilter2D(&sumLines, &sumLines, smooth);
    }
    //SHOW_MAT(smooth, "sumLines:");

    
    //get the max and its location
    vector <int> sumLinesMaxLoc;
    vector <double> sumLinesMax;
    int maxLoc; double max;
    //TODO: put the ignore in conf
    #define MAX_IGNORE 0 //(int(smoothWidth/2.)+1)
    #define LOCAL_MAX_IGNORE (int(MAX_IGNORE/4))
    mcvGetVectorMax(&sumLines, &max, &maxLoc, MAX_IGNORE);
    
    //put the local maxima stuff here
    if (localMaxima)
    {
	//loop to get local maxima
	for(int i=1+LOCAL_MAX_IGNORE; i<sumLines.rows-1-LOCAL_MAX_IGNORE; i++)
	{
	    //get that value
	    FLOAT val = CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i, 0);
	    //check if local maximum
	    if( (val > CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i-1, 0))
		&& (val > CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, i+1, 0))
		//		&& (i != maxLoc) 
		&& (val >= detectionThreshold) )
	    {

		//iterators for the two vectors
		vector<double>::iterator j;
		vector<int>::iterator k;
		//loop till we find the place to put it in descendingly
		for(j=sumLinesMax.begin(), k=sumLinesMaxLoc.begin(); 
		    j != sumLinesMax.end()  && val<= *j; j++,k++);
		//add its index
		sumLinesMax.insert(j, val);
		sumLinesMaxLoc.insert(k, i);
	    }	
	}
    }
    
    //check if didnt find local maxima
    if(sumLinesMax.size()==0 && max>detectionThreshold)
    {
	//put maximum
	sumLinesMaxLoc.push_back(maxLoc);
	sumLinesMax.push_back(max);
    }

//     //sort it descendingly
//     sort(sumLinesMax.begin(), sumLinesMax.end(), greater<double>());
//     //sort the indices
//     for (int i=0; i<(int)sumLinesMax.size(); i++)
// 	for (int j=i; j<(int)sumLinesMax.size(); j++)
// 	    if(sumLinesMax[i] == CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, 
// 					     sumLinesMaxLoc[j], 0))
// 	    {
// 		int k = sumLinesMaxLoc[j];
// 		sumLinesMaxLoc[j] = sumLinesMaxLoc[i];
// 		sumLinesMaxLoc[i] = k;
// 	    }
//     //sort(sumLinesMaxLoc.begin(), sumLinesMaxLoc.end(), greater<int>());

    //plot the line scores and the local maxima
#ifdef DEBUG_GET_STOP_LINES
//     gnuplot_ctrl *h =  mcvPlotMat1D(NULL, &sumLines, "Line Scores");
//     CvMat *y = mcvVector2Mat(sumLinesMax);
//     CvMat *x =  mcvVector2Mat(sumLinesMaxLoc);
//     mcvPlotMat2D(h, x, y);
//     //gnuplot_plot_xy(h, (double*)&sumLinesMaxLoc,(double*)&sumLinesMax, sumLinesMax.size(),""); 
//     cin.get();
//     gnuplot_close(h);
//     cvReleaseMat(&x);
//     cvReleaseMat(&y);
#endif
    
    
    //process the found maxima
    for (int i=0; i<(int)sumLinesMax.size(); i++)
    {
	//get subpixel accuracy
	double maxLocAcc = 
	    mcvGetLocalMaxSubPixel(
	        CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, MAX(sumLinesMaxLoc[i]-1,0), 0),
		CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, sumLinesMaxLoc[i], 0),
		CV_MAT_ELEM(sumLines, FLOAT_MAT_ELEM_TYPE, MIN(sumLinesMaxLoc[i]+1,maxLineLoc), 0) );
	maxLocAcc += sumLinesMaxLoc[i];
	maxLocAcc = MIN(MAX(0, maxLocAcc), maxLineLoc);

            
	//TODO: get line extent
	
	//put the extracted line
	Line line;
	switch (lineType)
        {
	case HV_LINES_HORIZONTAL:
	    line.startPoint.x = 0.5;             
	    line.startPoint.y = (FLOAT)maxLocAcc + .5;//sumLinesMaxLoc[i]+.5;
	    line.endPoint.x = inImage->width-.5; 
	    line.endPoint.y = line.startPoint.y;  
	    break;
	case HV_LINES_VERTICAL:
	    line.startPoint.x = (FLOAT)maxLocAcc + .5;//sumLinesMaxLoc[i]+.5;   
	    line.startPoint.y = .5;
	    line.endPoint.x = line.startPoint.x;     
	    line.endPoint.y = inImage->height-.5;  
	    break;
	}
	(*lines).push_back(line);
	if (lineScores)
	    (*lineScores).push_back(sumLinesMax[i]);
    }//for
    
    //clean
    cvReleaseMat(&sumLinesp);
    cvReleaseMat(&smooth);
    sumLinesMax.clear();
    sumLinesMaxLoc.clear();
    cvReleaseMat(&image);
}


/** This function detects lines in images using Hough transform
 * 
 * \param inImage input image
 * \param lines vector of lines to hold the results
 * \param lineScores scores of the detected lines (vector of floats)
 * \param rMin minimum r use for finding the lines (default 0)
 * \param rMax maximum r to find (default max(size(im)))
 * \param rStep step to use for binning (default is 2)
 * \param thetaMin minimum angle theta to look for (default 0) all in radians
 * \param thetaMax maximum angle theta to look for (default 2*pi)
 * \param thetaStep step to use for binning theta (default 5)
 * \param  binarize if to binarize the input image or use the raw values so that
 *	non-zero values are not treated as equal
 * \param localMaxima whether to detect local maxima or just get
 *      the maximum
 * \param detectionThreshold threshold for detection
 * \param smoothScores whether to smooth scores detected or not
 * \param group whether to group nearby detections (1) or not (0 default)
 * \param groupThreshold the minimum distance used for grouping (default 10)
 */ 

void mcvGetHoughTransformLines(const CvMat *inImage, vector <Line> *lines,
			       vector <FLOAT> *lineScores, 
			       StopLinePerceptorConf* lineConf)
// 			       FLOAT rMin, FLOAT rMax, FLOAT rStep, 
// 			       FLOAT thetaMin, FLOAT thetaMax,
// 			       FLOAT thetaStep, bool binarize, bool localMaxima, 
// 			       FLOAT detectionThreshold, bool smoothScores,
// 			       bool group, FLOAT groupTthreshold)
{
    CvMat *image;

    //binarize input image if to binarize
    if (!lineConf->binarize)
    {
	image = cvCloneMat(inImage); assert(image!=0);
//         mcvBinarizeImage(image);   
    }
    //binarize input image
    else
    {
	image = cvCreateMat(inImage->rows, inImage->cols, INT_MAT_TYPE);
	cvThreshold(inImage, image, 0, 1, CV_THRESH_BINARY); //0.05
	//get max of image
	//double maxval, minval;
	//cvMinMaxLoc(inImage, &minval, &maxval);
	//cout << "Max = " << maxval << "& Min=" << minval << "\n";
	//CvScalar mean = cvAvg(inImage);
	//cout << "Mean=" << mean.val[0] << "\n";
    }

#ifdef DEBUG_GET_STOP_LINES
	SHOW_IMAGE(image, "Hough thresholded image", 10);
#endif

    //define the accumulator array: rows correspond to r and columns to theta
    int rBins = int((lineConf->rMax-lineConf->rMin)/lineConf->rStep);
    int thetaBins = int((lineConf->thetaMax-lineConf->thetaMin)/lineConf->thetaStep);
    CvMat *houghSpace = cvCreateMat(rBins, thetaBins, CV_MAT_TYPE(image->type)); //FLOAT_MAT_TYPE);
    assert(houghSpace!=0);
    //init to zero
    cvSet(houghSpace, cvRealScalar(0));

    //init values of r and theta
    FLOAT *rs = new FLOAT[rBins];
    FLOAT *thetas = new FLOAT[thetaBins];
    FLOAT r, theta;
    int ri, thetai;
    for (r=lineConf->rMin+lineConf->rStep/2,  ri=0 ; ri<rBins; ri++,r+=lineConf->rStep) 
	rs[ri] = r;
    for (theta=lineConf->thetaMin, thetai=0 ; thetai<thetaBins; thetai++,
	     theta+=lineConf->thetaStep) 
	thetas[thetai] = theta;

    //get non-zero points in the image
    int nzCount = cvCountNonZero(image);
    CvMat *nzPoints = cvCreateMat(nzCount, 2, CV_32SC1);
    int idx = 0;
    for (int i=0; i<image->width; i++)
	for (int j=0; j<image->height; j++)
	    if ( cvGetReal2D(image, j, i) )
	    {
		CV_MAT_ELEM(*nzPoints, int, idx, 0) = i;
		CV_MAT_ELEM(*nzPoints, int, idx, 1) = j;
		idx++;
	    }



    //calculate r values for all theta and all points
    //CvMat *rPoints = cvCreateMat(image->width*image->height, thetaBins, CV_32SC1);//FLOAT_MAT_TYPE)
    //CvMat *rPoints = cvCreateMat(nzCount, thetaBins, CV_32SC1);//FLOAT_MAT_TYPE);
    //cvSet(rPoints, cvRealScalar(-1));
    //loop on x
    //float x=0.5, y=0.5;
    int i, k; //j
    for (i=0; i<nzCount; i++)
	for (k=0; k<thetaBins; k++)
	{
	    //compute the r value for that point and that theta
	    theta = thetas[k];
	    float rval = CV_MAT_ELEM(*nzPoints, int, i, 0) * cos(theta) + 
		CV_MAT_ELEM(*nzPoints, int, i, 1) * sin(theta); //x y
	    int r = (int)( ( rval - lineConf->rMin) / lineConf->rStep);
	    //	    CV_MAT_ELEM(*rPoints, int, i, k) = 
	    //(int)( ( rval - lineConf->rMin) / lineConf->rStep);	    

	    //accumulate in the hough space if a valid value
	    if (r>=0 && r<rBins)
		if(lineConf->binarize)
		    CV_MAT_ELEM(*houghSpace, INT_MAT_ELEM_TYPE, r, k)++;
	    //CV_MAT_ELEM(*image, INT_MAT_ELEM_TYPE, j, i);
		else
		    CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, r, k)+=
			CV_MAT_ELEM(*image, FLOAT_MAT_ELEM_TYPE, 
				    CV_MAT_ELEM(*nzPoints, int, i, 1), 
				    CV_MAT_ELEM(*nzPoints, int, i, 0));
	}

    //clear
    cvReleaseMat(&nzPoints);

//     bool inside;
//     for (i=0; i<image->width; i++) //x=0; x++
// 	//loop on y
// 	for (j=0; j<image->height; j++) //y=0 y++
// 	    //loop on theta
// 	    for (k=0; k<thetaBins; k++)
// 	    {
// 		//compute the r value and then subtract rMin and div by rStep 
// 		//to get the r bin index to which it belongs (0->rBins-1)
// 		if (lineConf->binarize && CV_MAT_ELEM(*image, INT_MAT_ELEM_TYPE, j, i) !=0)
// 		    inside = true;
// 		else if (!lineConf->binarize && CV_MAT_ELEM(*image, FLOAT_MAT_ELEM_TYPE, 
// 							    j, i) !=0)
// 		    inside = true;
// 		else
// 		    inside = false;
// 		if (inside)
// 		{
// 		    theta = thetas[k];
// 		    float rval = i * cos(theta) + j * sin(theta); //x y
// 		    CV_MAT_ELEM(*rPoints, int, 
// 				i*image->height + j, k) = 
// 			(int)( ( rval - lineConf->rMin) / lineConf->rStep);
// 		}
		
// 	    }

//      SHOW_MAT(rPoints, "rPoints");
//      cin.get();

    //now we should accumulate the values into the approprate bins in the houghSpace
//     for (ri=0; ri<rBins; ri++)
// 	for (thetai=0; thetai<thetaBins; thetai++)
// 	    for (i=0; i<image->width; i++)
// 		for (j=0; j<image->height; j++)
// 		{
// 		    //check if this cell belongs to that bin or not
// 		    if (CV_MAT_ELEM(*rPoints, int, 
// 				    i*image->height + j , thetai)==ri)
// 		    {
// 			if(lineConf->binarize)
// 			    CV_MAT_ELEM(*houghSpace, INT_MAT_ELEM_TYPE, ri, thetai)++;
// 			//CV_MAT_ELEM(*image, INT_MAT_ELEM_TYPE, j, i);
// 			else
// 			    CV_MAT_ELEM(*houghSpace, FLOAT_MAT_ELEM_TYPE, ri, thetai)+=
// 				CV_MAT_ELEM(*image, FLOAT_MAT_ELEM_TYPE, j, i);
// 		    }
// 		}
    

    //smooth hough transform


    //get local maxima
    vector <double> maxLineScores;
    vector <CvPoint> maxLineLocs;
    if (lineConf->localMaxima)
    {
	//get local maxima in the hough space
	mcvGetMatLocalMax(houghSpace, maxLineScores, maxLineLocs, lineConf->detectionThreshold);
    }

    //get the maximum value
    double maxLineScore;
    CvPoint maxLineLoc;
    cvMinMaxLoc(houghSpace, 0, &maxLineScore, 0, &maxLineLoc);
    if (maxLineScores.size()==0 && maxLineScore>=lineConf->detectionThreshold)
    {
	maxLineScores.push_back(maxLineScore);
	maxLineLocs.push_back(maxLineLoc);
    }


#ifdef DEBUG_GET_STOP_LINES
    cout << "Local maxima = " << maxLineScores.size() << "\n";

    {
	CvMat *im, *im2 = cvCloneMat(image);
	if (lineConf->binarize)
	    cvConvertScale(im2, im2, 255, 0);

	if (lineConf->binarize)
	    im = cvCreateMat(image->rows, image->cols, CV_8UC3);//cvCloneMat(image);
	else
	    im = cvCreateMat(image->rows, image->cols, CV_32FC3);
	cvCvtColor(im2, im, CV_GRAY2RGB);
	for (int i=0; i<(int)maxLineScores.size(); i++)
	{   
	    Line line;
	    assert(maxLineLocs[i].x>=0 && maxLineLocs[i].x<thetaBins);
	    assert(maxLineLocs[i].y>=0 && maxLineLocs[i].y<rBins);
	    mcvIntersectLineRThetaWithBB(rs[maxLineLocs[i].y], thetas[maxLineLocs[i].x],
					 cvSize(image->cols, image->rows), &line);
	    if (lineConf->binarize)
		mcvDrawLine(im, line, CV_RGB(255,0,0), 1);        
	    else
		mcvDrawLine(im, line, CV_RGB(1,0,0), 1);        
	}    
	SHOW_IMAGE(im, "Hough before grouping", 10);
	cvReleaseMat(&im);
	cvReleaseMat(&im2);

	//debug
	cout << "Maxima detected:\n";
	for(int ii=0; ii<(int)maxLineScores.size(); ii++)
	    cout << " " << maxLineScores[ii];
	cout << "\n";
    }
#endif
    
    //group detected maxima
    if (lineConf->group && maxLineScores.size()>1)
    {
	//flag for stopping
	bool stop = false;
	while (!stop)
	{
	    //minimum distance so far
	    float minDist = lineConf->groupThreshold+5, dist;
	    vector<CvPoint>::iterator iloc, jloc, 
		minIloc=maxLineLocs.begin(), minJloc=minIloc+1;
	    vector<double>::iterator iscore, jscore, minIscore, minJscore;
	    //compute pairwise distance between detected maxima
	    for (iloc=maxLineLocs.begin(), iscore=maxLineScores.begin(); 
		 iloc!=maxLineLocs.end(); iloc++, iscore++)
		for (jscore=iscore+1, jloc=iloc+1; 
		     jscore!=maxLineScores.end(); jloc++, jscore++)
		{
		    //get distance
		    dist = fabs(rs[iloc->y]-rs[jloc->y]) + 
			0.1 * fabs(thetas[iloc->x]-thetas[jloc->x]);
		    //check if minimum
		    if (dist<minDist)
		    {
			minDist = dist;
			minIloc = iloc; minIscore = iscore;
			minJloc = jloc; minJscore = jscore;
		    }
		}
	    //check if minimum distance is less than groupThreshold
	    if (minDist >= lineConf->groupThreshold)
		stop = true;
	    else
	    {
// 		//debug
//  		cout << "Before grouping:\n";
//  		for(int ii=0; ii<(int)maxLineScores.size(); ii++)
//  		    cout << " " << maxLineScores[ii];
//  		cout << "\n";

 		//combine the two minimum ones with weighted average of 
 		//their scores
 		double x =  (minIloc->x * *minIscore + minJloc->x * *minJscore) / 
 		    (*minIscore + *minJscore);
 		double y =  (minIloc->y * *minIscore + minJloc->y * *minJscore) / 
 		    (*minIscore + *minJscore);
		//put into the first
		minIloc->x = (int)x;// ((minJloc->x + minJloc->x)/2.0); // (int) x;
		minIloc->y = (int)y;// ((minJloc->y + minIloc->y)/2.0); // (int) y;
		*minIscore = (*minJscore + *minIscore)/2;///2;
		//delete second one
		maxLineLocs.erase(minJloc);
		maxLineScores.erase(minJscore);

// 		//debug
//  		cout << "Before sorting:\n";
//  		for(int ii=0; ii<(int)maxLineScores.size(); ii++)
//  		    cout << " " << maxLineScores[ii];
//  		cout << "\n";

		//check if to put somewhere else depending on the changed score
		for (iscore=maxLineScores.begin(), iloc=maxLineLocs.begin(); 
		     iscore!=maxLineScores.end() && *minIscore <= *iscore;
		     iscore++, iloc++);
		//swap the original location if different
		if (iscore!=minIscore )
		{
		    //insert in new position
		    maxLineScores.insert(iscore, *minIscore);
		    maxLineLocs.insert(iloc, *minIloc);
		    //delte old
		    maxLineScores.erase(minIscore);
		    maxLineLocs.erase(minIloc);
// 		    //if at end, then back up one position
// 		    if(iscore == maxLineScores.end())
// 		    {
// 			iscore--;
// 			iloc--;
// 		    }
// 		    CvPoint tloc;
// 		    double tscore;
// 		    //swap
// 		    tloc = *minIloc;	 
// 		    tscore = *minIscore;

// 		    *minIloc = *iloc;
// 		    *minIscore = *iscore;

// 		    *iloc = tloc;
// 		    *iscore = tscore;
		}		

//  		cout << "after sorting:\n";
//  		for(int ii=0; ii<(int)maxLineScores.size(); ii++)
//  		    cout << " " << maxLineScores[ii];
//  		cout << "\n";

	    }		    
	}
    }

#ifdef DEBUG_GET_STOP_LINES
    {
	CvMat *im, *im2 = cvCloneMat(image);
	if (lineConf->binarize)
	    cvConvertScale(im2, im2, 255, 0);
	if (lineConf->binarize)
	    im = cvCreateMat(image->rows, image->cols, CV_8UC3);//cvCloneMat(image);
	else
	    im = cvCreateMat(image->rows, image->cols, CV_32FC3);
	cvCvtColor(im2, im, CV_GRAY2RGB);
	for (int i=0; i<(int)maxLineScores.size(); i++)
	{   
	    Line line;
	    assert(maxLineLocs[i].x>=0 && maxLineLocs[i].x<thetaBins);
	    assert(maxLineLocs[i].y>=0 && maxLineLocs[i].y<rBins);
	    mcvIntersectLineRThetaWithBB(rs[maxLineLocs[i].y], thetas[maxLineLocs[i].x],
					 cvSize(image->cols, image->rows), &line);
	    if (lineConf->binarize)
		mcvDrawLine(im, line, CV_RGB(255,0,0), 1);        
	    else
		mcvDrawLine(im, line, CV_RGB(1,0,0), 1);        
	}    
	SHOW_IMAGE(im, "Hough after grouping", 10);
	cvReleaseMat(&im);
	cvReleaseMat(&im2);
    }
#endif

    //debug
#ifdef DEBUG_GET_STOP_LINES
    //put local maxima in image
    for (int i=0; i<(int)maxLineLocs.size(); i++)
	if(lineConf->binarize)
	    CV_MAT_ELEM(*houghSpace, unsigned char, maxLineLocs[i].y, maxLineLocs[i].x) = 255;
	else
	    CV_MAT_ELEM(*houghSpace, float, maxLineLocs[i].y, maxLineLocs[i].x) = 1.f;
    //show the hough space
    SHOW_IMAGE(houghSpace, "Hough Space", 10);
    //SHOW_MAT(houghSpace, "Hough Space:");
#endif 

    //process detected maxima and return detected line(s)
    for(int i=0; i<int(maxLineScores.size()); i++)
    {
	//check if above threshold
	if (maxLineScores[i]>=lineConf->detectionThreshold)
	{
	    //get sub-pixel accuracy
	    
	    //get the two end points from the r-theta
	    Line line;
	    assert(maxLineLocs[i].x>=0 && maxLineLocs[i].x<thetaBins);
	    assert(maxLineLocs[i].y>=0 && maxLineLocs[i].y<rBins);
	    mcvIntersectLineRThetaWithBB(rs[maxLineLocs[i].y], thetas[maxLineLocs[i].x],
					 cvSize(image->cols, image->rows), &line);

	    
	    //get line extent
	    
	    //put the extracted line
	    lines->push_back(line);
	    if (lineScores)
		(*lineScores).push_back(maxLineScores[i]);
	    
	}
	//not above threshold
	else
	{
	    //exit out of the for loop, as the scores are sorted descendingly
	    break;
	}
    }

    //clean up
    cvReleaseMat(&image);
    cvReleaseMat(&houghSpace);
    //cvReleaseMat(&rPoints);
    delete [] rs;
    delete [] thetas;
    maxLineScores.clear();
    maxLineLocs.clear();
}



/** This function binarizes the input image i.e. nonzero elements
 * becomen 1 and others are 0.
 * 
 * \param inImage input & output image
 */ 
void mcvBinarizeImage(CvMat *inImage)
{

    if (CV_MAT_TYPE(inImage->type)==FLOAT_MAT_TYPE)
    {
        for (int i=0; i<inImage->height; i++)
            for (int j=0; j<inImage->width; j++)
                if (CV_MAT_ELEM(*inImage, FLOAT_MAT_ELEM_TYPE, i, j) != 0.f)
                    CV_MAT_ELEM(*inImage, FLOAT_MAT_ELEM_TYPE, i, j)=1;
    }
    else if (CV_MAT_TYPE(inImage->type)==INT_MAT_TYPE)
    {
        for (int i=0; i<inImage->height; i++)
            for (int j=0; j<inImage->width; j++)
                if (CV_MAT_ELEM(*inImage, INT_MAT_ELEM_TYPE, i, j) != 0)
                    CV_MAT_ELEM(*inImage, INT_MAT_ELEM_TYPE, i, j)=1;
    }
    else
    {
        cerr << "Unsupported type in mcvBinarizeImage\n";
        exit(1);   
    }                          

}


/** This function gets the maximum value in a vector (row or column) 
 * and its location
 * 
 * \param inVector the input vector
 * \param max the output max value
 * \param maxLoc the location (index) of the first max
 * 
 */ 
#define MCV_VECTOR_MAX(type)  \
    /*row vector*/ \
    if (inVector->height==1) \
    { \
        /*initial value*/ \
        tmax = (double) CV_MAT_ELEM(*inVector, type, 0, inVector->width-1); \
        tmaxLoc = inVector->width-1; \
        /*loop*/ \
        for (int i=inVector->width-1-ignore; i>=0+ignore; i--) \
        { \
            if (tmax<CV_MAT_ELEM(*inVector, type, 0, i)) \
            { \
                tmax = CV_MAT_ELEM(*inVector, type, 0, i); \
                tmaxLoc = i; \
            } \
        } \
    } \
    /*column vector */ \
    else \
    { \
        /*initial value*/ \
        tmax = (double) CV_MAT_ELEM(*inVector, type, inVector->height-1, 0); \
        tmaxLoc = inVector->height-1; \
        /*loop*/ \
        for (int i=inVector->height-1-ignore; i>=0+ignore; i--) \
        { \
            if (tmax<CV_MAT_ELEM(*inVector, type, i, 0)) \
            { \
                tmax = (double) CV_MAT_ELEM(*inVector, type, i, 0); \
                tmaxLoc = i; \
            } \
        } \
    } \

void mcvGetVectorMax(const CvMat *inVector, double *max, int *maxLoc, int ignore)
{
    double tmax;
    int tmaxLoc;
            
    if (CV_MAT_TYPE(inVector->type)==FLOAT_MAT_TYPE)
    {
        MCV_VECTOR_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inVector->type)==INT_MAT_TYPE)
    {
        MCV_VECTOR_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorMax\n";
        exit(1); 
    }
    
    
    //return
    if (max)
        *max = tmax;
    if (maxLoc)
        *maxLoc = tmaxLoc;
}



/** This function gets the local maxima in a matrix and their positions 
 *  and its location
 * 
 * \param inMat input matrix
 * \param localMaxima the output vector of local maxima
 * \param localMaximaLoc the vector of locations of the local maxima, 
 *       where each location is cvPoint(x=col, y=row) zero-based
 * 
 */ 
#define MCV_MAT_LOCAL_MAX(type)  \
    /*loop on the matrix and get points that are larger than their*/ \
    /*neighboring 8 pixels*/ \
    for(int i=1; i<inMat->rows-1; i++) \
	for (int j=1; j<inMat->cols-1; j++) \
	{ \
	    /*get the current value*/ \
	    val = CV_MAT_ELEM(*inMat, type, i, j); \
	    /*check if it's larger than all its neighbors*/ \
	    if( val > CV_MAT_ELEM(*inMat, type, i-1, j-1) && \
		val > CV_MAT_ELEM(*inMat, type, i-1, j) && \
		val > CV_MAT_ELEM(*inMat, type, i-1, j+1) && \
		val > CV_MAT_ELEM(*inMat, type, i, j-1) && \
		val > CV_MAT_ELEM(*inMat, type, i, j+1) && \
		val > CV_MAT_ELEM(*inMat, type, i+1, j-1) && \
		val > CV_MAT_ELEM(*inMat, type, i+1, j) && \
		val > CV_MAT_ELEM(*inMat, type, i+1, j+1) && \
                val >= threshold) \
	    { \
		/*found a local maxima, put it in the return vector*/ \
		/*in decending order*/ \
		/*iterators for the two vectors*/ \
		vector<double>::iterator k; \
		vector<CvPoint>::iterator l; \
		/*loop till we find the place to put it in descendingly*/ \
		for(k=localMaxima.begin(), l=localMaximaLoc.begin(); \
		    k != localMaxima.end()  && val<= *k; k++,l++); \
		/*add its index*/ \
		localMaxima.insert(k, val); \
		localMaximaLoc.insert(l, cvPoint(j, i)); \
	    } \
	} 

void mcvGetMatLocalMax(const CvMat *inMat, vector<double> &localMaxima, 
		     vector<CvPoint> &localMaximaLoc, double threshold)
{

    double val;

    //check type
    if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_MAT_LOCAL_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE)
    {
        MCV_MAT_LOCAL_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetMatLocalMax\n";
        exit(1); 
    }
    
    //return
}


/** This function gets the local maxima in a vector and their positions 
 * 
 * \param inVec input vector
 * \param localMaxima the output vector of local maxima
 * \param localMaximaLoc the vector of locations of the local maxima, 
 * 
 */ 
#define MCV_VECTOR_LOCAL_MAX(type)  \
    /*loop on the vector and get points that are larger than their*/ \
    /*neighboring points*/ \
    if(inVec->height == 1) \
    { \
	for(int i=1; i<inVec->width-1; i++) \
	{ \
	    /*get the current value*/ \
	    val = CV_MAT_ELEM(*inVec, type, 0, i); \
	    /*check if it's larger than all its neighbors*/ \
	    if( val > CV_MAT_ELEM(*inVec, type, 0, i-1) && \
		val > CV_MAT_ELEM(*inVec, type, 0, i+1) ) \
	    { \
		/*found a local maxima, put it in the return vector*/ \
		/*in decending order*/ \
		/*iterators for the two vectors*/ \
		vector<double>::iterator k; \
		vector<int>::iterator l; \
		/*loop till we find the place to put it in descendingly*/ \
		for(k=localMaxima.begin(), l=localMaximaLoc.begin(); \
		    k != localMaxima.end()  && val<= *k; k++,l++); \
		/*add its index*/ \
		localMaxima.insert(k, val); \
		localMaximaLoc.insert(l, i); \
	    } \
        } \
    } \
    else \
    { \
	for(int i=1; i<inVec->height-1; i++) \
	{ \
	    /*get the current value*/ \
	    val = CV_MAT_ELEM(*inVec, type, i, 0); \
	    /*check if it's larger than all its neighbors*/ \
	    if( val > CV_MAT_ELEM(*inVec, type, i-1, 0) && \
		val > CV_MAT_ELEM(*inVec, type, i+1, 0) ) \
	    { \
		/*found a local maxima, put it in the return vector*/ \
		/*in decending order*/ \
		/*iterators for the two vectors*/ \
		vector<double>::iterator k; \
		vector<int>::iterator l; \
		/*loop till we find the place to put it in descendingly*/ \
		for(k=localMaxima.begin(), l=localMaximaLoc.begin(); \
		    k != localMaxima.end()  && val<= *k; k++,l++); \
		/*add its index*/ \
		localMaxima.insert(k, val); \
		localMaximaLoc.insert(l, i); \
	    } \
        } \
    } 

void mcvGetVectorLocalMax(const CvMat *inVec, vector<double> &localMaxima, 
		     vector<int> &localMaximaLoc)
{

    double val;

    //check type
    if (CV_MAT_TYPE(inVec->type)==FLOAT_MAT_TYPE)
    {
        MCV_VECTOR_LOCAL_MAX(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inVec->type)==INT_MAT_TYPE)
    {
        MCV_VECTOR_LOCAL_MAX(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorLocalMax\n";
        exit(1); 
    }
    
    //return
}


/** This function gets the qtile-th quantile of the input matrix
 * 
 * \param mat input matrix
 * \param qtile required input quantile probability
 * \return the returned value
 * 
 */
FLOAT mcvGetQuantile(const CvMat *mat, FLOAT qtile)
{
    //make it a row vector
    CvMat rowMat;
    cvReshape(mat, &rowMat, 0, 1);
    
    //get the quantile
    FLOAT qval;
    qval = quantile((FLOAT*) rowMat.data.ptr, rowMat.width, qtile);
    
    return qval;
}


/** This function thresholds the image below a certain value to the threshold
 * so: outMat(i,j) = inMat(i,j) if inMat(i,j)>=threshold
 *                 = threshold otherwise
 * 
 * \param inMat input matrix
 * \param outMat output matrix
 * \param threshold threshold value
 * 
 */
void mcvThresholdLower(const CvMat *inMat, CvMat *outMat, FLOAT threshold)
{

#define MCV_THRESHOLD_LOWER(type) \
     for (int i=0; i<inMat->height; i++) \
        for (int j=0; j<inMat->width; j++) \
            if ( CV_MAT_ELEM(*inMat, type, i, j)<threshold) \
                CV_MAT_ELEM(*outMat, type, i, j)=(type) 0; /*check it, was: threshold*/\
                
    //check if to copy into outMat or not
    if (inMat != outMat)
        cvCopy(inMat, outMat);
        
    //check type
    if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_THRESHOLD_LOWER(FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE)
    {
        MCV_THRESHOLD_LOWER(INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetVectorMax\n";
        exit(1); 
    }                            
}

/** This function detects stop lines in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param stopLines a vector of returned stop lines in input image coordinates 
 * \param linescores a vector of line scores returned
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetStopLines(const CvMat *inImage, vector<Line> *stopLines, 
		     vector<FLOAT> *lineScores, const CameraInfo *cameraInfo, 
		      StopLinePerceptorConf *stopLineConf)

{
    //input size
    CvSize inSize = cvSize(inImage->width, inImage->height);
    
    //TODO: smooth image
    CvMat *image = cvCloneMat(inImage);
    //cvSmooth(image, image, CV_GAUSSIAN, 5, 5, 1, 1);

    IPMInfo ipmInfo;

//     //get the IPM size such that we have height of the stop line
//     //is 3 pixels
//     double ipmWidth, ipmHeight;
//     mcvGetIPMExtent(cameraInfo, &ipmInfo);
//     ipmHeight = 3*(ipmInfo.yLimits[1]-ipmInfo.yLimits[0]) / (stopLineConf->lineHeight/3.);
//     ipmWidth = ipmHeight * 4/3;    
//     //put into the conf
//     stopLineConf->ipmWidth = int(ipmWidth);
//     stopLineConf->ipmHeight = int(ipmHeight);

//     #ifdef DEBUG_GET_STOP_LINES
//     cout << "IPM width:" << stopLineConf->ipmWidth << " IPM height:" 
// 	 << stopLineConf->ipmHeight << "\n";
//     #endif
    
    
    //Get IPM
    CvSize ipmSize = cvSize((int)stopLineConf->ipmWidth, 
        (int)stopLineConf->ipmHeight);    
    CvMat * ipm;
    ipm = cvCreateMat(ipmSize.height, ipmSize.width, inImage->type);
    //mcvGetIPM(inImage, ipm, &ipmInfo, cameraInfo);    
    ipmInfo.vpPortion = stopLineConf->ipmVpPortion;
    list<CvPoint> outPixels;
    list<CvPoint>::iterator outPixelsi;
    mcvGetIPM(image, ipm, &ipmInfo, cameraInfo, &outPixels);
    
    //smooth the IPM
    //cvSmooth(ipm, ipm, CV_GAUSSIAN, 5, 5, 1, 1);
        
    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImage = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImage);
    #endif

           
    //compute stop line width: 2000 mm
    FLOAT stopLinePixelWidth = stopLineConf->lineWidth * 
        ipmInfo.xScale;
    //stop line pixel height: 12 inches = 12*25.4 mm
    FLOAT stopLinePixelHeight = stopLineConf->lineHeight  * 
        ipmInfo.yScale;
    //kernel dimensions
    //unsigned char wx = 2;
    //unsigned char wy = 2;
    FLOAT sigmax = stopLinePixelWidth;
    FLOAT sigmay = stopLinePixelHeight;
    
    #ifdef DEBUG_GET_STOP_LINES
    //cout << "Line width:" << stopLinePixelWidth << "Line height:" 
    //	 << stopLinePixelHeight << "\n";
    #endif

    //filter the IPM image
    mcvFilterLines(ipm, ipm, stopLineConf->kernelWidth, 
        stopLineConf->kernelHeight, sigmax, sigmay, 
        FILTER_LINE_HORIZONTAL);     
    
    //zero out points outside the image in IPM view
    for(outPixelsi=outPixels.begin(); outPixelsi!=outPixels.end(); outPixelsi++)
	CV_MAT_ELEM(*ipm, float, (*outPixelsi).y, (*outPixelsi).x) = 0;
    outPixels.clear();
    
    //zero out negative values    
    mcvThresholdLower(ipm, ipm, 0);
    
    //compute quantile: .985
    FLOAT qtileThreshold = mcvGetQuantile(ipm, stopLineConf->lowerQuantile);
    mcvThresholdLower(ipm, ipm, qtileThreshold);

    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImageThresholded = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImageThresholded);    
    #endif


    
    //group stop lines
    switch(stopLineConf->groupingType)
    {
    //use HV grouping
    case GROUPING_TYPE_HV_LINES:
	//vector <Line> ipmStopLines;
	//vector <FLOAT> lineScores;
	mcvGetHVLines(ipm, stopLines, lineScores, HV_LINES_HORIZONTAL, 
		      stopLinePixelHeight, stopLineConf->binarize, 
		      stopLineConf->localMaxima, stopLineConf->detectionThreshold, 
		      stopLineConf->smoothScores);

	break;

    //use Hough Transform grouping
    case GROUPING_TYPE_HOUGH_LINES:
	//FLOAT rMin = 0.05*ipm->height, rMax = 0.4*ipm->height, rStep = 1;
	//FLOAT thetaMin = 88*CV_PI/180, thetaMax = 92*CV_PI/180, thetaStep = 1*CV_PI/180;
	//bool group = false; FLOAT groupThreshold = 1;
	mcvGetHoughTransformLines(ipm, stopLines, lineScores, stopLineConf);
// 				  stopLineConf->rMin, stopLineConf->rMax, 
// 				  stopLineConf->rStep, stopLineConf->thetaMin, 
// 				  stopLineConf->thetaMax, stopLineConf->thetaStep, 
// 				  stopLineConf->binarize, 
// 				  stopLineConf->localMaxima,  
// 				  stopLineConf->detectionThreshold, 
// 				  stopLineConf->smoothScores,
// 				  stopLineConf->group, stopLineConf->groupThreshold);	    

	break;	
    }

    //get RANSAC lines
    if (stopLineConf->ransac)
    {
	mcvGetRansacLines(ipm, *stopLines, *lineScores, stopLineConf, HV_LINES_HORIZONTAL);
    }

    if(stopLineConf->getEndPoints)
    {
	//get line extent in IPM image
	for(int i=0; i<(int)stopLines->size(); i++)
	    mcvGetLineExtent(ipm, (*stopLines)[i], (*stopLines)[i]);
    }
        
    #ifdef DEBUG_GET_STOP_LINES
        vector <Line> dbIpmStopLines = *stopLines;
        //print out lineScores
        cout << "LineScores:";
        //for (int i=0; i<(int)lineScores->size(); i++)
	for (int i=0; i<1 && lineScores->size()>0; i++)
            cout << (*lineScores)[i] << " ";
        cout << "\n";
    #endif

    //check if returned anything
    if (stopLines->size()!=0)
    {                
        //convert the line into world frame
        for (unsigned int i=0; i<stopLines->size(); i++)
        {
            Line *line;
            line = &(*stopLines)[i];
            
            mcvPointImIPM2World(&(line->startPoint), &ipmInfo);
            mcvPointImIPM2World(&(line->endPoint), &ipmInfo);
        }
        
        //convert them from world frame into camera frame
	//
	//put a dummy line at the beginning till we check that cvDiv bug
	Line dummy = {{1.,1.},{2.,2.}};
	stopLines->insert(stopLines->begin(), dummy);
	//convert to mat and get in image coordinates
        CvMat *mat = cvCreateMat(2, 2*stopLines->size(), FLOAT_MAT_TYPE);
        mcvLines2Mat(stopLines, mat);
        stopLines->clear();
        mcvTransformGround2Image(mat, mat, cameraInfo);
	//get back to vector
        mcvMat2Lines(mat, stopLines);
	//remove the dummy line at the beginning
	stopLines->erase(stopLines->begin());
        //clear
        cvReleaseMat(&mat);
                
        //clip the lines found and get their extent
        for (unsigned int i=0; i<stopLines->size(); i++)
	{
	    //clip
            mcvIntersectLineWithBB(&(*stopLines)[i], inSize, &(*stopLines)[i]);
	    
	    //get the line extent
	    //mcvGetLineExtent(inImage, (*stopLines)[i], (*stopLines)[i]);
	}


    }        
    
    //debugging
    #ifdef DEBUG_GET_STOP_LINES
        //show the IPM image
        SHOW_IMAGE(dbIpmImage, "IPM image", 10);
        //thresholded ipm
        SHOW_IMAGE(dbIpmImageThresholded, "Thresholded IPM image", 10);    
        //draw lines in IPM image
        //for (int i=0; i<(int)dbIpmStopLines.size(); i++)
	for (int i=0; i<1 && dbIpmStopLines.size()>0; i++)
        {   
            mcvDrawLine(dbIpmImage, dbIpmStopLines[i], CV_RGB(0,0,0), 3);        
        }    
        SHOW_IMAGE(dbIpmImage, "IPM with lines", 10);
        //draw lines on original image
        //CvMat *image = cvCreateMat(inImage->height, inImage->width, CV_32FC3);
        //cvCvtColor(inImage, image, CV_GRAY2RGB);
        //CvMat *image = cvCloneMat(inImage);
        //for (int i=0; i<(int)stopLines->size(); i++)
	for (int i=0; i<1 && stopLines->size()>0; i++)
        {
            //SHOW_POINT((*stopLines)[i].startPoint, "start");
            //SHOW_POINT((*stopLines)[i].endPoint, "end");
            mcvDrawLine(image, (*stopLines)[i], CV_RGB(255,0,0), 3);
        }    
        SHOW_IMAGE(image, "Detected lines", 10);
        //cvReleaseMat(&image);
        cvReleaseMat(&dbIpmImage);
        cvReleaseMat(&dbIpmImageThresholded);
        dbIpmStopLines.clear();    
    #endif //DEBUG_GET_STOP_LINES 

    //clear
    cvReleaseMat(&ipm);
    cvReleaseMat(&image);
    //ipmStopLines.clear();    
}



/** This function detects lanes in the input image using IPM 
 * transformation and the input camera parameters. The returned lines 
 * are in a vector of Line objects, having start and end point in 
 * input image frame.
 * 
 * \param image the input image
 * \param lanes a vector of returned stop lines in input image coordinates 
 * \param linescores a vector of line scores returned
 * \param cameraInfo the camera parameters
 * \param stopLineConf parameters for stop line detection
 *
 * 
 */
void mcvGetLanes(const CvMat *inImage, vector<Line> *lanes, 
		 vector<FLOAT> *lineScores, vector<Spline> *splines,
		 vector<float> *splineScores, CameraInfo *cameraInfo, 
		 StopLinePerceptorConf *stopLineConf)

{
    //input size
    CvSize inSize = cvSize(inImage->width, inImage->height);
    
    //TODO: smooth image
    CvMat *image = cvCloneMat(inImage);
    //cvSmooth(image, image, CV_GAUSSIAN, 5, 5, 1, 1);

    IPMInfo ipmInfo;

//     //get the IPM size such that we have height of the stop line
//     //is 3 pixels
//     double ipmWidth, ipmHeight;
//     mcvGetIPMExtent(cameraInfo, &ipmInfo);
//     ipmHeight = 3*(ipmInfo.yLimits[1]-ipmInfo.yLimits[0]) / (stopLineConf->lineHeight/3.);
//     ipmWidth = ipmHeight * 4/3;    
//     //put into the conf
//     stopLineConf->ipmWidth = int(ipmWidth);
//     stopLineConf->ipmHeight = int(ipmHeight);

//     #ifdef DEBUG_GET_STOP_LINES
//     cout << "IPM width:" << stopLineConf->ipmWidth << " IPM height:" 
// 	 << stopLineConf->ipmHeight << "\n";
//     #endif
    
    
    //Get IPM
    CvSize ipmSize = cvSize((int)stopLineConf->ipmWidth, 
        (int)stopLineConf->ipmHeight);    
    CvMat * ipm;
    ipm = cvCreateMat(ipmSize.height, ipmSize.width, inImage->type);
    //mcvGetIPM(inImage, ipm, &ipmInfo, cameraInfo);    
    ipmInfo.vpPortion = stopLineConf->ipmVpPortion;
    list<CvPoint> outPixels;
    list<CvPoint>::iterator outPixelsi;
    mcvGetIPM(image, ipm, &ipmInfo, cameraInfo, &outPixels);
    
    //smooth the IPM
    //cvSmooth(ipm, ipm, CV_GAUSSIAN, 5, 5, 1, 1);
        
    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImage = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImage);
    #endif

           
    //compute stop line width: 2000 mm
    FLOAT stopLinePixelWidth = stopLineConf->lineWidth * 
        ipmInfo.xScale;
    //stop line pixel height: 12 inches = 12*25.4 mm
    FLOAT stopLinePixelHeight = stopLineConf->lineHeight  * 
        ipmInfo.yScale;
    //kernel dimensions
    //unsigned char wx = 2;
    //unsigned char wy = 2;
    FLOAT sigmax = stopLinePixelWidth;
    FLOAT sigmay = stopLinePixelHeight;
    
    #ifdef DEBUG_GET_STOP_LINES
    //cout << "Line width:" << stopLinePixelWidth << "Line height:" 
    //	 << stopLinePixelHeight << "\n";
    #endif

    //filter the IPM image
    mcvFilterLines(ipm, ipm, stopLineConf->kernelWidth, 
        stopLineConf->kernelHeight, sigmax, sigmay, 
        FILTER_LINE_VERTICAL);     
    
    //zero out points outside the image in IPM view
    for(outPixelsi=outPixels.begin(); outPixelsi!=outPixels.end(); outPixelsi++)
	CV_MAT_ELEM(*ipm, float, (*outPixelsi).y, (*outPixelsi).x) = 0;
    outPixels.clear();

    //zero out negative values    
    mcvThresholdLower(ipm, ipm, 0);
    
    //compute quantile: .985
    FLOAT qtileThreshold = mcvGetQuantile(ipm, stopLineConf->lowerQuantile);
    mcvThresholdLower(ipm, ipm, qtileThreshold);

    //debugging 
    #ifdef DEBUG_GET_STOP_LINES
        CvMat *dbIpmImageThresholded = cvCreateMat(ipm->height, ipm->width, ipm->type);
        cvCopy(ipm, dbIpmImageThresholded);    
    #endif


    
    //group stop lines
    switch(stopLineConf->groupingType)
    {
    //use HV grouping
    case GROUPING_TYPE_HV_LINES:
	//vector <Line> ipmStopLines;
	//vector <FLOAT> lineScores;
	mcvGetHVLines(ipm, lanes, lineScores, HV_LINES_VERTICAL, 
		      stopLinePixelHeight, stopLineConf->binarize, 
		      stopLineConf->localMaxima, stopLineConf->detectionThreshold, 
		      stopLineConf->smoothScores);

	break;

    //use Hough Transform grouping
    case GROUPING_TYPE_HOUGH_LINES:
	//FLOAT rMin = 0.05*ipm->height, rMax = 0.4*ipm->height, rStep = 1;
	//FLOAT thetaMin = 88*CV_PI/180, thetaMax = 92*CV_PI/180, thetaStep = 1*CV_PI/180;
	//	bool group = true; FLOAT groupThreshold = 15;
	mcvGetHoughTransformLines(ipm, lanes, lineScores, stopLineConf); 
// 				  stopLineConf->rMin, stopLineConf->rMax, 
// 				  stopLineConf->rStep, stopLineConf->thetaMin, 
// 				  stopLineConf->thetaMax, stopLineConf->thetaStep, 
// 				  stopLineConf->binarize, 
// 				  stopLineConf->localMaxima,  
// 				  stopLineConf->detectionThreshold, 
// 				  stopLineConf->smoothScores,
// 				  stopLineConf->group, stopLineConf->groupThreshold);	    

	break;	
    }

    //get RANSAC lines
    if (stopLineConf->ransac)
    {
	//check if to return splines or lines
	if (stopLineConf->ransacSpline)
	    mcvGetRansacSplines(ipm, *lanes, *lineScores, stopLineConf,
				HV_LINES_VERTICAL, *splines, *splineScores);
	if (stopLineConf->ransacLine)
	    mcvGetRansacLines(ipm, *lanes, *lineScores, stopLineConf, HV_LINES_VERTICAL);
    }

    //get line extent
    if(stopLineConf->getEndPoints)
    {
	//get line extent in IPM image
	for(int i=0; i<(int)lanes->size(); i++)
	    mcvGetLineExtent(ipm, (*lanes)[i], (*lanes)[i]);
    }
        
#ifdef DEBUG_GET_STOP_LINES
    vector <Line> dbIpmStopLines = *lanes;
    vector<Spline> dbIpmSplines = *splines;
    //print out lineScores
    cout << "LineScores:";
    //for (int i=0; i<(int)lineScores->size(); i++)
    for (int i=0; i<(int)lineScores->size(); i++)
	cout << (*lineScores)[i] << " ";
    cout << "\n";
#endif
    
    //if return straight lines
    if (stopLineConf->ransacLine || !stopLineConf->ransacSpline)
    {
	mcvLinesImIPM2Im(*lanes, ipmInfo, *cameraInfo, inSize);
// 	//check if returned anything
// 	if (lanes->size()!=0)
// 	{                
// 	    //convert the line into world frame
// 	    for (unsigned int i=0; i<lanes->size(); i++)
// 	    {
// 		Line *line;
// 		line = &(*lanes)[i];
		
// 		mcvPointImIPM2World(&(line->startPoint), &ipmInfo);
// 		mcvPointImIPM2World(&(line->endPoint), &ipmInfo);
// 	    }
	    
// 	    //convert them from world frame into camera frame
// 	    //
// 	    //put a dummy line at the beginning till we check that cvDiv bug
// 	    Line dummy = {{1.,1.},{2.,2.}};
// 	    lanes->insert(lanes->begin(), dummy);
// 	    //convert to mat and get in image coordinates
// 	    CvMat *mat = cvCreateMat(2, 2*lanes->size(), FLOAT_MAT_TYPE);
// 	    mcvLines2Mat(lanes, mat);
// 	    lanes->clear();
// 	    mcvTransformGround2Image(mat, mat, cameraInfo);
// 	    //get back to vector
// 	    mcvMat2Lines(mat, lanes);
// 	    //remove the dummy line at the beginning
// 	    lanes->erase(lanes->begin());
// 	    //clear
// 	    cvReleaseMat(&mat);
	    
// 	    //clip the lines found and get their extent
// 	    for (unsigned int i=0; i<lanes->size(); i++)
// 	    {
// 		//clip
// 		mcvIntersectLineWithBB(&(*lanes)[i], inSize, &(*lanes)[i]);
		
// 		//get the line extent
// 		//mcvGetLineExtent(inImage, (*stopLines)[i], (*stopLines)[i]);
// 	    }
// 	}        
    }
    //return spline
    if (stopLineConf->ransacSpline)
    {
	mcvSplinesImIPM2Im(*splines, ipmInfo, *cameraInfo, inSize);
    }
    
    //debugging
#ifdef DEBUG_GET_STOP_LINES
    //show the IPM image
    SHOW_IMAGE(dbIpmImage, "IPM image", 10);
    //thresholded ipm
    SHOW_IMAGE(dbIpmImageThresholded, "Thresholded IPM image", 10);    
    //draw lines in IPM image
    //for (int i=0; i<(int)dbIpmStopLines.size(); i++)
    if (stopLineConf->ransacLine || !stopLineConf->ransacSpline)
	for (int i=0; i<(int)dbIpmStopLines.size(); i++)
	    mcvDrawLine(dbIpmImage, dbIpmStopLines[i], CV_RGB(0,0,0), 3);        
    if (stopLineConf->ransacSpline)
	for (int i=0; i<(int)dbIpmSplines.size(); i++)
	    mcvDrawSpline(dbIpmImage, dbIpmSplines[i], CV_RGB(0,0,0), 1);
	
    SHOW_IMAGE(dbIpmImage, "IPM with lines", 10);
    //draw lines on original image
    //CvMat *image = cvCreateMat(inImage->height, inImage->width, CV_32FC3);
    //cvCvtColor(inImage, image, CV_GRAY2RGB);
    //CvMat *image = cvCloneMat(inImage);
    //for (int i=0; i<(int)stopLines->size(); i++)
    if (stopLineConf->ransacLine || !stopLineConf->ransacSpline)
	for (int i=0; i<(int)lanes->size(); i++)
	    mcvDrawLine(image, (*lanes)[i], CV_RGB(255,0,0), 3);
    if (stopLineConf->ransacSpline)
	for (int i=0; i<(int)splines->size(); i++)
	    mcvDrawSpline(image, (*splines)[i], CV_RGB(255,0,0), 1);

    
    SHOW_IMAGE(image, "Detected lines", 10);
    //cvReleaseMat(&image);
    cvReleaseMat(&dbIpmImage);
    cvReleaseMat(&dbIpmImageThresholded);
    dbIpmStopLines.clear();    
    dbIpmSplines.clear();
#endif //DEBUG_GET_STOP_LINES 

    //clear
    cvReleaseMat(&ipm);
    cvReleaseMat(&image);
    //ipmStopLines.clear();    
}


/** This function converts an array of lines to a matrix (already allocated)
 * 
 * \param lines input vector of lines
 * \param size number of lines to convert
 * \return the converted matrix, it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * 
 * 
 */
void mcvLines2Mat(const vector<Line> *lines, CvMat *mat)
{
    //allocate the matrix
    //*mat = cvCreateMat(2, size*2, FLOAT_MAT_TYPE);
    
    //loop and put values
    int j;
    for (int i=0; i<(int)lines->size(); i++)
    {
        j = 2*i;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j) =
            (*lines)[i].startPoint.x;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j) = 
            (*lines)[i].startPoint.y;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j+1) =
             (*lines)[i].endPoint.x;
        CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j+1) = 
            (*lines)[i].endPoint.y;
    }       
}


/** This function converts matrix into n array of lines
 * 
 * \param mat input matrix , it has 2x2*size where size is the
 *  number of lines, first row is x values (start.x, end.x) and second
 *  row is y-values
 * \param  lines the rerurned vector of lines
 * 
 * 
 */
void mcvMat2Lines(const CvMat *mat, vector<Line> *lines)
{

    Line line;
    //loop and put values
    for (int i=0; i<int(mat->width/2); i++)
    {
        int j = 2*i;
        //get the line
        line.startPoint.x = 
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j);
        line.startPoint.y =
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j); 
        line.endPoint.x =    
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 0, j+1);
        line.endPoint.y =
            CV_MAT_ELEM(*mat, FLOAT_MAT_ELEM_TYPE, 1, j+1);
        //push it
        lines->push_back(line);            
    }    
}



/** This function intersects the input line with the given bounding box
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineWithBB(const Line *inLine, const CvSize bbox,
    Line *outLine)
{
    //put output
    outLine->startPoint.x = inLine->startPoint.x;
    outLine->startPoint.y = inLine->startPoint.y;
    outLine->endPoint.x = inLine->endPoint.x;
    outLine->endPoint.y = inLine->endPoint.y;

    //check which points are inside
    bool startInside, endInside;
    startInside = mcvIsPointInside(inLine->startPoint, bbox);
    endInside = mcvIsPointInside(inLine->endPoint, bbox);
    
    //now check        
    if (!(startInside && endInside))
    {
        //difference
        FLOAT deltax, deltay;
        deltax = inLine->endPoint.x - inLine->startPoint.x;
        deltay = inLine->endPoint.y - inLine->startPoint.y;
        //hold parameters
        FLOAT t[4]={2,2,2,2};
        FLOAT xup, xdown, yleft, yright;
        
        //intersect with top and bottom borders: y=0 and y=bbox.height-1
        if (deltay==0) //horizontal line
        {
            xup = xdown = bbox.width+2;
        }
        else
        {
            t[0] = -inLine->startPoint.y/deltay;
            xup = inLine->startPoint.x + t[0]*deltax;
            t[1] = (bbox.height-inLine->startPoint.y)/deltay;
            xdown = inLine->startPoint.x + t[1]*deltax;
        }
        
        //intersect with left and right borders: x=0 and x=bbox.widht-1
        if (deltax==0) //horizontal line
        {
            yleft = yright = bbox.height+2;
        }
        else
        {
            t[2] = -inLine->startPoint.x/deltax;
            yleft = inLine->startPoint.y + t[2]*deltay;
            t[3] = (bbox.width-inLine->startPoint.x)/deltax;
            yright = inLine->startPoint.y + t[3]*deltay;
        }
        
        //points of intersection
        FLOAT_POINT2D pts[4] = {{xup, 0},{xdown,bbox.height},
               {0, yleft},{bbox.width, yright}};
                       
        //now decide which stays and which goes
        int i;
        if (!startInside)
        {
            bool cont=true;
            for (i=0; i<4 && cont; i++)
            {
                if (t[i]>=0 && t[i]<=1 && mcvIsPointInside(pts[i],bbox) &&
		    !(pts[i].x == outLine->endPoint.x && 
		      pts[i].y == outLine->endPoint.y) )
                {
                    outLine->startPoint.x = pts[i].x;
                    outLine->startPoint.y = pts[i].y;
                    t[i] = 2;
                    cont = false; 
                }
            }
        }
        if (!endInside)
        {
            bool cont=true;
            for (i=0; i<4 && cont; i++)
            {
                if (t[i]>=0 && t[i]<=1 && mcvIsPointInside(pts[i],bbox) &&
		    !(pts[i].x == outLine->startPoint.x && 
		      pts[i].y == outLine->startPoint.y) )
                {
                    outLine->endPoint.x = pts[i].x;
                    outLine->endPoint.y = pts[i].y;
                    t[i] = 2;
                    cont = false;
                }
            }           
        }   
    }
}


/** This function intersects the input line (given in r and theta) with 
 *  the given bounding box where the line is represented by: 
 *  x cos(theta) + y sin(theta) = r
 * 
 * \param r the r value for the input line
 * \param theta the theta value for the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
void mcvIntersectLineRThetaWithBB(FLOAT r, FLOAT theta, const CvSize bbox,
    Line *outLine)
{
    //hold parameters
    double xup, xdown, yleft, yright;
        
    //intersect with top and bottom borders: y=0 and y=bbox.height-1
    if (cos(theta)==0) //horizontal line
    {
	xup = xdown = bbox.width+2;
    }
    else
    {
	xup = r / cos(theta);
	xdown = (r-bbox.height*sin(theta))/cos(theta);
    }
        
    //intersect with left and right borders: x=0 and x=bbox.widht-1
    if (sin(theta)==0) //horizontal line
    {
	yleft = yright = bbox.height+2;
    }
    else
    {
	yleft = r/sin(theta);
	yright = (r-bbox.width*cos(theta))/sin(theta);
    }
        
    //points of intersection
    FLOAT_POINT2D pts[4] = {{xup, 0},{xdown,bbox.height},
			    {0, yleft},{bbox.width, yright}};

    //get the starting point
    int i;
    for (i=0; i<4; i++)
    {
	//if point inside, then put it
	if(mcvIsPointInside(pts[i], bbox))
	{
	    outLine->startPoint.x = pts[i].x;
	    outLine->startPoint.y = pts[i].y;
	    //get out of for loop
	    break;
	}
    }
    //get the ending point
    for (i++; i<4; i++)
    {
	//if point inside, then put it
	if(mcvIsPointInside(pts[i], bbox))
	{
	    outLine->endPoint.x = pts[i].x;
	    outLine->endPoint.y = pts[i].y;
	    //get out of for loop
	    break;
	}
    }                       
}


/** This function checks if the given point is inside the bounding box
 * specified
 * 
 * \param inLine the input line
 * \param bbox the bounding box
 * \param outLine the output line
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D point, CvSize bbox)
{
    return (point.x>=0 && point.x<=bbox.width 
        && point.y>=0 && point.y<=bbox.height) ? true : false;    
} 


/** This function intersects the input line (given in r and theta) with 
 *  the given rectangle where the line is represented by: 
 *  x cos(theta) + y sin(theta) = r
 * 
 * \param r the r value for the input line
 * \param theta the theta value for the input line
 * \param rect the input rectangle (given two opposite points in the rectangle, 
 *   upperleft->startPoint and bottomright->endPoint where x->right and y->down)
 * \param outLine the output line
 * 
 */
void mcvIntersectLineRThetaWithRect(FLOAT r, FLOAT theta, const Line &rect,
    Line &outLine)
{
    //hold parameters
    double xup, xdown, yleft, yright;
        
    //intersect with top and bottom borders: y=rect->startPoint.y and y=rect->endPoint.y
    if (cos(theta)==0) //horizontal line
    {
	xup = xdown = rect.endPoint.x+2;
    }
    else
    {
	xup = (r-rect.startPoint.y*sin(theta)) / cos(theta);
	xdown = (r-rect.endPoint.y*sin(theta)) / cos(theta);
    }
        
    //intersect with left and right borders: x=rect->startPoint.x and x=rect->endPoint.x
    if (sin(theta)==0) //horizontal line
    {
	yleft = yright = rect.endPoint.y+2;
    }
    else
    {
	yleft = (r-rect.startPoint.x*cos(theta)) / sin(theta);
	yright = (r-rect.endPoint.x*cos(theta)) / sin(theta);
    }
        
    //points of intersection
    FLOAT_POINT2D pts[4] = {{xup, rect.startPoint.y},{xdown,rect.endPoint.y},
			    {rect.startPoint.x, yleft},{rect.endPoint.x, yright}};

    //get the starting point
    int i;
    for (i=0; i<4; i++)
    {
	//if point inside, then put it
	if(mcvIsPointInside(pts[i], rect))
	{
	    outLine.startPoint.x = pts[i].x;
	    outLine.startPoint.y = pts[i].y;
	    //get out of for loop
	    break;
	}
    }
    //get the ending point
    for (i++; i<4; i++)
    {
	//if point inside, then put it
	if(mcvIsPointInside(pts[i], rect))
	{
	    outLine.endPoint.x = pts[i].x;
	    outLine.endPoint.y = pts[i].y;
	    //get out of for loop
	    break;
	}
    }                       
}


/** This function checks if the given point is inside the rectangle specified
 * 
 * \param inLine the input line
 * \param rect the specified rectangle
 * 
 */
bool mcvIsPointInside(FLOAT_POINT2D &point, const Line &rect)
{
    return (point.x>=rect.startPoint.x && point.x<=rect.endPoint.x 
        && point.y>=rect.startPoint.y && point.y<=rect.endPoint.y) ? true : false;    
} 


/** This function converts an INT mat into a FLOAT mat (already allocated)
 * 
 * \param inMat input INT matrix
 * \param outMat output FLOAT matrix
 * 
 */
void mcvMatInt2Float(const CvMat *inMat, CvMat *outMat)
{
    for (int i=0; i<inMat->height; i++)
        for (int j=0; j<inMat->width; j++)
            CV_MAT_ELEM(*outMat, FLOAT_MAT_ELEM_TYPE, i, j) = 
                (FLOAT_MAT_ELEM_TYPE) 
                CV_MAT_ELEM(*inMat, INT_MAT_ELEM_TYPE, i, j)/255;
}


/** This function draws a line onto the passed image
 * 
 * \param image the input iamge
 * \param line input line
 * \param line color
 * \param width line width
 * 
 */
void mcvDrawLine(CvMat *image, Line line, CvScalar color, int width)
{
    cvLine(image, cvPoint((int)line.startPoint.x,(int)line.startPoint.y),
            cvPoint((int)line.endPoint.x,(int)line.endPoint.y), 
            color, width);
}

/** This initializes the stoplineperceptorinfo structure
 * 
 * \param fileName the input file name
 * \param stopLineConf the structure to fill
 *
 * 
 */
 void mcvInitStopLinePerceptorConf(char * const fileName, 
    StopLinePerceptorConf *stopLineConf)
{
  //parsed camera data
    StopLinePerceptorParserInfo stopLineParserInfo;
    //read the data
    assert(stopLinePerceptorParser_configfile(fileName, &stopLineParserInfo, 0, 1, 1)==0);
    //init the strucure
    stopLineConf->ipmWidth = stopLineParserInfo.ipmWidth_arg;
    stopLineConf->ipmHeight = stopLineParserInfo.ipmHeight_arg;
    stopLineConf->lineWidth = stopLineParserInfo.lineWidth_arg;
    stopLineConf->lineHeight = stopLineParserInfo.lineHeight_arg;
    stopLineConf->kernelWidth = stopLineParserInfo.kernelWidth_arg;
    stopLineConf->kernelHeight = stopLineParserInfo.kernelHeight_arg;
    stopLineConf->lowerQuantile = 
        stopLineParserInfo.lowerQuantile_arg;
    stopLineConf->localMaxima = 
        stopLineParserInfo.localMaxima_arg;
    stopLineConf->groupingType = stopLineParserInfo.groupingType_arg;
    stopLineConf->binarize = stopLineParserInfo.binarize_arg;
    stopLineConf->detectionThreshold = 
        stopLineParserInfo.detectionThreshold_arg;
    stopLineConf->smoothScores = 
        stopLineParserInfo.smoothScores_arg;
    stopLineConf->rMin = stopLineParserInfo.rMin_arg;
    stopLineConf->rMax = stopLineParserInfo.rMax_arg;
    stopLineConf->rStep = stopLineParserInfo.rStep_arg;
    stopLineConf->thetaMin = stopLineParserInfo.thetaMin_arg * CV_PI/180;
    stopLineConf->thetaMax = stopLineParserInfo.thetaMax_arg * CV_PI/180;
    stopLineConf->thetaStep = stopLineParserInfo.thetaStep_arg * CV_PI/180;
    stopLineConf->ipmVpPortion = stopLineParserInfo.ipmVpPortion_arg;
    stopLineConf->getEndPoints = stopLineParserInfo.getEndPoints_arg;
    stopLineConf->group = stopLineParserInfo.group_arg;
    stopLineConf->groupThreshold = stopLineParserInfo.groupThreshold_arg;
    stopLineConf->ransac = stopLineParserInfo.ransac_arg;
    stopLineConf->ransacNumSamples = stopLineParserInfo.ransacNumSamples_arg;
    stopLineConf->ransacNumIterations = stopLineParserInfo.ransacNumIterations_arg;
    stopLineConf->ransacNumGoodFit = stopLineParserInfo.ransacNumGoodFit_arg;
    stopLineConf->ransacThreshold = stopLineParserInfo.ransacThreshold_arg;
    stopLineConf->ransacScoreThreshold = stopLineParserInfo.ransacScoreThreshold_arg;
    stopLineConf->ransacBinarize = stopLineParserInfo.ransacBinarize_arg;
    stopLineConf->ransacSplineDegree = stopLineParserInfo.ransacSplineDegree_arg;
    stopLineConf->ransacSpline = stopLineParserInfo.ransacSpline_arg;
    stopLineConf->ransacLine = stopLineParserInfo.ransacLine_arg;
}
        
void SHOW_LINE(const Line line, char str[]) 
{
    cout << str;
    cout << "(" << line.startPoint.x << "," << line.startPoint.y << ")";
    cout << "->";
    cout << "(" << line.endPoint.x << "," << line.endPoint.y << ")";
    cout << "\n";
} 

void SHOW_SPLINE(const Spline spline, char str[]) 
{
    cout << str;
    cout << "(" << spline.degree << ")";
    for (int i=0; i<spline.degree+1; i++)
    {
	cout << " (" << spline.points[i].x << "," << spline.points[i].y << ")";	
    }
    cout << "\n";
} 


/** This fits a parabola to the entered data to get
 * the location of local maximum with sub-pixel accuracy
 * 
 * \param val1 first value
 * \param val2 second value
 * \param val3 third value
 *
 * \return the computed location of the local maximum
 */
double mcvGetLocalMaxSubPixel(double val1, double val2, double val3)
{
    //build an array to hold the x-values
    double Xp[] = {1, -1, 1, 0, 0, 1, 1, 1, 1};
    CvMat X = cvMat(3, 3, CV_64FC1, Xp);

    //array to hold the y values
    double yp[] = {val1, val2, val3};
    CvMat y = cvMat(3, 1, CV_64FC1, yp);

    //solve to get the coefficients
    double Ap[3];
    CvMat A = cvMat(3, 1, CV_64FC1, Ap);
    cvSolve(&X, &y, &A, CV_SVD);

    //get the local max
    double max;
    max = -0.5 * Ap[1] / Ap[0];

    //return
    return max;        
}

/** This functions implements Bresenham's algorithm for getting pixels of the
 * line given its two endpoints

 * 
 * \param line the input line
 * \param x a vector of x locations for the line pixels (0-based)
 * \param y a vector of y locations for the line pixels (0-based)
 *
 */
void mcvGetLinePixels(const Line &line, vector<int> &x, vector<int> &y)
{
    //get two end points
    CvPoint start;
    start.x  = int(line.startPoint.x); start.y = int(line.startPoint.y);
    CvPoint end;
    end.x = int(line.endPoint.x); end.y = int(line.endPoint.y);

    //get deltas
    int deltay = end.y - start.y;
    int deltax = end.x - start.x;

    //check if slope is steep, then reflect the line along y=x i.e. swap x and y
    bool steep = false;
    if (abs(deltay) > abs(deltax))
    {
	steep = true;
	//swap x and y 
	int t;
	t = start.x;
	start.x = start.y;
	start.y = t;
	t = end.x;
	end.x = end.y;
	end.y = t;
    }


    //check to make sure we are going right
    if(start.x>end.x)
    {
	//swap the two points
	CvPoint t = start;
	start = end;
	end = t;
    }


    //get deltas again
    deltay = end.y - start.y;
    deltax = end.x - start.x;

    //error
    int error = 0;

    //delta error
    int deltaerror = abs(deltay);

    //ystep
    int ystep = -1;
    if (deltay>=0)
    {
	ystep = 1;
    }

    //loop
    int i, j;
    j = start.y;
    for (i=start.x; i<=end.x; i++)
    {
	//put the new point
	if(steep)
	{
	    x.push_back(j);
	    y.push_back(i);
	}
	else
	{
	    x.push_back(i);
	    y.push_back(j);
	}

	//adjust error
	error += deltaerror;
	//check
	if(2*error>=deltax)
	{
	    j = j + ystep;
	    error -= deltax;
	}
    }
}

/** This functions implements Bresenham's algorithm for getting pixels of the
 * line given its two endpoints

 * 
 * \param im the input image
 * \param inLine the input line
 * \param outLine the output line
 *
 */
void mcvGetLineExtent(const CvMat *im, const Line &inLine, Line &outLine)
{

    //first clip the input line to the image coordinates
    Line line = inLine;
    mcvIntersectLineWithBB(&inLine, cvSize(im->width-1, im->height-1), &line);
    
    //then get the pixel values of the line in the image
    vector<int> x, y;
    mcvGetLinePixels(line, x, y);

    //check which way to shift the line to get multiple readings
    bool changey = false;
    if (fabs(line.startPoint.x-line.endPoint.x) > 
	fabs(line.startPoint.y-line.endPoint.y))
    {
	//change the y-coordiantes
	changey = true;
    }
    char changes[] = {0, -1, 1};//, -2, 2};
    int numChanges = 3;
    
    //loop on the changes and get possible extents
    vector<int> startLocs;
    vector<int> endLocs;
    int endLoc;
    int startLoc;
    CvMat *pix = cvCreateMat(1, im->width, FLOAT_MAT_TYPE);
    CvMat *rstep = cvCreateMat(pix->height, pix->width, FLOAT_MAT_TYPE);
    CvMat *fstep = cvCreateMat(pix->height, pix->width, FLOAT_MAT_TYPE);
    for (int c=0; c<numChanges; c++)
    {

	//get the pixels
	for(int i=0; i<(int)x.size(); i++)
	{
	    CV_MAT_ELEM(*pix, FLOAT_MAT_ELEM_TYPE, 0, i) = 
		CV_MAT_ELEM(*im, FLOAT_MAT_ELEM_TYPE, 
			    //    y[i], x[i]);
			    changey ? min(max(y[i]+changes[c],0),im->height-1) : y[i], 
			    changey ? x[i] : min(max(x[i]+changes[c],0),im->width-1));
	}
	//remove the mean
	CvScalar mean = cvAvg(pix);
	cvSubS(pix, mean, pix); 

	//now convolve with rising step to get start point
	FLOAT_MAT_ELEM_TYPE stepp[] = {-0.3000, -0.2, -0.1, 0, 0, 0.1, 0.2, 0.3, 0.4};
	// {-0.6, -0.4, -0.2, 0.2, 0.4, 0.6};
	int stepsize = 9;
	//{-0.2, -0.4, -0.2, 0, 0, 0.2, 0.4, 0.2}; //{-.75, -.5, .5, .75};
	CvMat step = cvMat(1, stepsize, FLOAT_MAT_TYPE, stepp);
	//	  SHOW_MAT(&step,"step");
	//smooth
	//	  FLOAT_MAT_ELEM_TYPE smoothp[] = {.25, .5, .25};
	//CvMat smooth = cvMat(1, 3, FLOAT_MAT_TYPE, smoothp);
	//cvFilter2D(&step, &step, &smooth);
	//SHOW_MAT(&step,"smoothed step");
	//convolve	
	cvFilter2D(pix, rstep, &step);
	//get local max
	//     vector<double> localMax;
	//     vector<int> localMaxLoc;
	//     mcvGetVectorLocalMax(rstep, localMax, localMaxLoc);
	//     int startLoc = localMaxLoc[0];
	double max;
	mcvGetVectorMax(rstep, &max, &startLoc, 0);
	//check if zero
	if(max==0)
	    startLoc = startLocs[c-1];
	
	//convolve with falling step to get end point
	//cvFlip(&step, NULL, 1);
	//convolve
	//cvFilter2D(pix, fstep, &step);
	//get local max
	//     localMax.clear();
	//     localMaxLoc.clear();
	//     mcvGetVectorLocalMax(fstep, localMax, localMaxLoc);
	//     int endLoc = localMaxLoc[0];
	//take the negative
	cvConvertScale(rstep, fstep, -1);
	mcvGetVectorMax(fstep, &max, &endLoc, 0);
	//check if zero
	if(max==0)
	    endLoc = endLocs[c-1];
	if(endLoc<=startLoc)
	    endLoc = im->width-1;

	//put into vectors
	startLocs.push_back(startLoc);
	endLocs.push_back(endLoc);
    }

    //get median
    startLoc = quantile(startLocs, 0);
    endLoc = quantile(endLocs, 1);
    //    for (int i=0; i<(int)startLocs.size(); i++) cout << startLocs[i] << "  ";
    //cout << "\n";
    //for (int i=0; i<(int)endLocs.size(); i++) cout << endLocs[i] << "  ";

    //get the end-point
    outLine.startPoint.x = x[startLoc]; outLine.startPoint.y = y[startLoc];
    outLine.endPoint.x = x[endLoc]; outLine.endPoint.y = y[endLoc];
    
    //clear
    cvReleaseMat(&pix);
    cvReleaseMat(&rstep);
    cvReleaseMat(&fstep);
//     localMax.clear();
//     localMaxLoc.clear();
		      startLocs.clear();
		      endLocs.clear();
}



/** This functions converts a line defined by its two end-points into its
    r and theta (origin is at top-left corner with x right and y down and theta
    measured positive clockwise<with y pointing down> -pi<theta<pi)

 * 
 * \param line input line
 * \param r the returned r (normal distance to the line from the origin)
 * \param outLine the output line
 *
 */
void mcvLineXY2RTheta(const Line &line, float &r, float &theta)
{

    //check if vertical line x1==x2
    if(line.startPoint.x == line.endPoint.x)
    {
	//r is the x
	r = fabs(line.startPoint.x);
	//theta is 0 or pi
	theta = line.startPoint.x>=0 ? 0. : CV_PI;
    }
    //check if horizontal i.e. y1==y2
    else if(line.startPoint.y == line.endPoint.y)
    {
	//r is the y
	r = fabs(line.startPoint.y);
	//theta is pi/2 or -pi/2
	theta = (float) line.startPoint.y>=0 ? CV_PI/2 : -CV_PI/2;
    }
    //general line
    else
    {
	//tan(theta) = (x2-x1)/(y1-y2)
	theta =  atan2(line.endPoint.x-line.startPoint.x, 
		       line.startPoint.y-line.endPoint.y);
	//r = x*cos(theta)+y*sin(theta)
	float r1 = line.startPoint.x * cos(theta) + line.startPoint.y * sin(theta);
	r = line.endPoint.x * cos(theta) + line.endPoint.y * sin(theta);
	//adjust to add pi if necessary
	if(r1<0 || r<0)
	{
	    //add pi
	    theta += CV_PI;
	    if(theta>CV_PI)
		theta -= 2*CV_PI;
	    //take abs
	    r = fabs(r);	
	}
    }
}

/** This functions fits a line using the orthogonal distance to the line
    by minimizing the sum of squares of this distance.

 * 
 * \param points the input points to fit the line to which is
 *    2xN matrix with x values on first row and y values on second
 * \param lineRTheta the return line [r, theta] where the line is
 *    x*cos(theta)+y*sin(theta)=r
 * \param lineAbc the return line in [a, b, c] where the line is
 *    a*x+b*y+c=0
 *
 */
void mcvFitRobustLine(const CvMat *points, float *lineRTheta, 
		      float *lineAbc)
{
    //clone the points
    CvMat *cpoints = cvCloneMat(points);
    //get mean of the points and subtract from the original points
    float meanX=0, meanY=0;
    CvScalar mean;
    CvMat row1, row2;
    //get first row, compute avg and store
    cvGetRow(cpoints, &row1, 0);
    mean = cvAvg(&row1);
    meanX = (float) mean.val[0];
    cvSubS(&row1, mean, &row1);
    //same for second row
    cvGetRow(cpoints, &row2, 1);
    mean = cvAvg(&row2);
    meanY = (float) mean.val[0];
    cvSubS(&row2, mean, &row2);

    //compute the SVD for the centered points array
    CvMat *W = cvCreateMat(2, 1, CV_32FC1);
    CvMat *V = cvCreateMat(2, 2, CV_32FC1);
    //    CvMat *V = cvCreateMat(2, 2, CV_32fC1);
    CvMat *cpointst = cvCreateMat(cpoints->cols, cpoints->rows, CV_32FC1);
    cvTranspose(cpoints, cpointst);
    cvSVD(cpointst, W, 0, V, CV_SVD_V_T);
    cvTranspose(V, V);
    cvReleaseMat(&cpointst);

    //get the [a,b] which is the second column corresponding to
    //smaller singular value
    float a, b, c;
    a = CV_MAT_ELEM(*V, float, 0, 1);
    b = CV_MAT_ELEM(*V, float, 1, 1);

    //c = -meanX*a-meanY*b
    c = -(meanX * a + meanY * b);

    //compute r and theta
    //theta = atan(b/a)
    //r = meanX cos(theta) + meanY sin(theta)
    float r, theta;
    theta = atan2(b, a);
    r = meanX * cos(theta) + meanY * sin(theta);
    //correct
    if (r<0)
    {
	//correct r
	r = -r;
	//correct theta
	theta += CV_PI;
	if (theta>CV_PI)
	    theta -= 2*CV_PI;
    }

    //return
    if (lineRTheta)
    {
	lineRTheta[0] = r;
	lineRTheta[1] = theta;
    }
    if (lineAbc)
    {
	lineAbc[0] = a;
	lineAbc[1] = b;
	lineAbc[2] = c;
    }

    //clear
    cvReleaseMat(&cpoints);
    cvReleaseMat(&W);
    cvReleaseMat(&V);
}



/** This functions implements RANSAC algorithm for line fitting
    given an image

 * 
 * \param image input image
 * \param numSamples number of samples to take every iteration
 * \param numIterations number of iterations to run
 * \param threshold threshold to use to assess a point as a good fit to a line
 * \param numGoodFit number of points close enough to say there's a good fit 
 * \param lineXY the fitted line 
 * \param lineRTheta the fitted line [r; theta]
 * \param lineScore the score of the line detected
 *
 */
void mcvFitRansacLine(const CvMat *image, int numSamples, int numIterations,
		      float threshold, float scoreThreshold, int numGoodFit, 
		      Line *lineXY, float *lineRTheta, float *lineScore)
{

    //get the points with non-zero pixels
    CvMat *points;
    points = mcvGetNonZeroPoints(image,true);
    //subtract half
    cvAddS(points, cvRealScalar(0.5), points);

    //random number generator
    CvRNG rng = cvRNG(0xffffffff);
    //matrix to hold random sample
    CvMat *randInd = cvCreateMat(numSamples, 1, CV_32SC1);
    CvMat *samplePoints = cvCreateMat(2, numSamples, CV_32FC1);
    //flag for points currently included in the set
    CvMat *pointIn = cvCreateMat(1, points->cols, CV_8SC1);
    //returned lines
    float curLineRTheta[2], curLineAbc[3];
    float bestLineRTheta[2]={-1.f,0.f}, bestLineAbc[3];
    float bestScore=0, bestDist=1e5;
    //outer loop
    for (int i=0; i<numIterations; i++)
    {
	//set flag to zero
	//cvSet(pointIn, cvRealScalar(0));
	cvSetZero(pointIn);
	//get random sample from the points
	cvRandArr(&rng, randInd, CV_RAND_UNI, cvRealScalar(0), cvRealScalar(points->cols));
	for (int j=0; j<numSamples; j++)
	{
	    //flag it as included
	    CV_MAT_ELEM(*pointIn, char, 0, CV_MAT_ELEM(*randInd, int, j, 0)) = 1;
	    //put point
	    CV_MAT_ELEM(*samplePoints, float, 0, j) = 
		CV_MAT_ELEM(*points, float, 0, CV_MAT_ELEM(*randInd, int, j, 0));
	    CV_MAT_ELEM(*samplePoints, float, 1, j) = 
		CV_MAT_ELEM(*points, float, 1, CV_MAT_ELEM(*randInd, int, j, 0));
	}

	//fit the line
	mcvFitRobustLine(samplePoints, curLineRTheta, curLineAbc);

	//loop on other points and compute distance to the line
	float score=0;
	for (int j=0; j<points->cols; j++)
	    //if not already inside
	    if (!CV_MAT_ELEM(*pointIn, char, 0, j))
	    {
		//compute distance to line
		float dist = fabs(CV_MAT_ELEM(*points, float, 0, j) * curLineAbc[0] +
		    CV_MAT_ELEM(*points, float, 1, j) * curLineAbc[1] + curLineAbc[2]);
		//check distance
		if (dist<=threshold)
		{
		    //add this point
		    CV_MAT_ELEM(*pointIn, char, 0, j) = 1;
		    //update score
		    score += cvGetReal2D(image, (int)(CV_MAT_ELEM(*points, float, 1, j)-.5),
					  (int)(CV_MAT_ELEM(*points, float, 0, j)-.5));
		}
	    }

	//check the number of close points and whether to consider this a good fit
	int numClose = cvCountNonZero(pointIn);
	//cout << "numClose=" << numClose << "\n";
	if (numClose >= numGoodFit)
	{
	    //get the points included to fit this line
	    CvMat *fitPoints = cvCreateMat(2, numClose, CV_32FC1);
	    int k=0;
	    //loop on points and copy points included
	    for (int j=0; j<points->cols; j++)
		if(CV_MAT_ELEM(*pointIn, char, 0, j))
		{
		    CV_MAT_ELEM(*fitPoints, float, 0, k) = 
			CV_MAT_ELEM(*points, float, 0, j);
		    CV_MAT_ELEM(*fitPoints, float, 1, k) = 
			CV_MAT_ELEM(*points, float, 1, j);
		    k++;

		}

	    //fit the line
	    mcvFitRobustLine(fitPoints, curLineRTheta, curLineAbc);


	    //compute distances to new line
	    float dist = 0.;
	    for (int j=0; j<fitPoints->cols; j++)
		//compute distance to line
		dist += fabs(CV_MAT_ELEM(*fitPoints, float, 0, j) * curLineAbc[0] +
			     CV_MAT_ELEM(*fitPoints, float, 1, j) * curLineAbc[1] + 
			     curLineAbc[2]) *
		    cvGetReal2D(image, (int)(CV_MAT_ELEM(*fitPoints, float, 1, j)-.5),
				(int)(CV_MAT_ELEM(*fitPoints, float, 0, j)-.5));
	    //dist /= score;

	    //clear fitPoints
	    cvReleaseMat(&fitPoints);

	    //check if to keep the line as best
	    if (dist<bestDist && score>=scoreThreshold)//(numClose > bestScore)
	    {
		//update max
		bestScore = score; //numClose;
		bestDist = dist;
		//copy
		bestLineRTheta[0] = curLineRTheta[0];
		bestLineRTheta[1] = curLineRTheta[1];
		bestLineAbc[0] = curLineAbc[0];
		bestLineAbc[1] = curLineAbc[1];
		bestLineAbc[2] = curLineAbc[2];
	    }
	    
	}

	
    }

    //return
    if (lineRTheta)
    {
	lineRTheta[0] = bestLineRTheta[0];
	lineRTheta[1] = bestLineRTheta[1];
    }
    if (lineXY)
    {
	mcvIntersectLineRThetaWithBB(lineRTheta[0], lineRTheta[1],
				     cvSize(image->cols, image->rows), lineXY);
    }
    if (lineScore)
	*lineScore = bestScore;
    

    //clear
    cvReleaseMat(&points);
    cvReleaseMat(&samplePoints);
    cvReleaseMat(&randInd);
    cvReleaseMat(&pointIn);
}




/** This function gets the indices of the non-zero values in a matrix

 * \param inMat the input matrix
 * \param outMat the output matrix, with 2xN containing the x and y in 
 *    each column and the pixels value [xs; ys; pixel values]
 * \param floatMat whether to return floating points or integers for 
 *    the outMat
 */
CvMat* mcvGetNonZeroPoints(const CvMat *inMat, bool floatMat)
{


#define MCV_GET_NZ_POINTS(inMatType, outMatType) \
     /*loop and allocate the points*/ \
     for (int i=0; i<inMat->rows; i++) \
 	for (int j=0; j<inMat->cols; j++) \
 	    if (CV_MAT_ELEM(*inMat, inMatType, i, j)) \
 	    { \
 		CV_MAT_ELEM(*outMat, outMatType, 0, k) = j; \
 		CV_MAT_ELEM(*outMat, outMatType, 1, k) = i; \
                CV_MAT_ELEM(*outMat, outMatType, 2, k) = \
                  (outMatType) CV_MAT_ELEM(*inMat, inMatType, i, j); \
                k++; \
 	    } \

    int k=0;

    //get number of non-zero points
    int numnz = cvCountNonZero(inMat);

    //allocate the point array and get the points
    CvMat* outMat;
    if (floatMat)
	outMat = cvCreateMat(3, numnz, CV_32FC1);
    else 
	outMat = cvCreateMat(3, numnz, CV_32SC1);

    //check type
    if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE && 
	CV_MAT_TYPE(outMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_GET_NZ_POINTS(FLOAT_MAT_ELEM_TYPE, FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==FLOAT_MAT_TYPE && 
	CV_MAT_TYPE(outMat->type)==INT_MAT_TYPE)
    {
        MCV_GET_NZ_POINTS(FLOAT_MAT_ELEM_TYPE, INT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE && 
	CV_MAT_TYPE(outMat->type)==FLOAT_MAT_TYPE)
    {
        MCV_GET_NZ_POINTS(INT_MAT_ELEM_TYPE, FLOAT_MAT_ELEM_TYPE)
    }
    else if (CV_MAT_TYPE(inMat->type)==INT_MAT_TYPE && 
	CV_MAT_TYPE(outMat->type)==INT_MAT_TYPE)
    {
        MCV_GET_NZ_POINTS(INT_MAT_ELEM_TYPE, INT_MAT_ELEM_TYPE)
    }
    else
    {
        cerr << "Unsupported type in mcvGetMatLocalMax\n";
        exit(1); 
    }
    
    //return
    return outMat;
}


/** This function groups nearby lines
 *
 * \param lines vector of lines
 * \param lineScores scores of input lines
 * \param groupThreshold the threshold used for grouping
 * \param bbox the bounding box to intersect with
 */
void mcvGroupLines(vector<Line> &lines, vector<float> &lineScores, 
		     float groupThreshold, CvSize bbox)
{

    //convert the lines into r-theta parameters
    int numInLines = lines.size();
    vector<float> rs(numInLines);
    vector<float> thetas(numInLines);
    for (int i=0; i<numInLines; i++)
	mcvLineXY2RTheta(lines[i], rs[i], thetas[i]);


    //flag for stopping
    bool stop = false;
    while (!stop)
    {
	//minimum distance so far
	float minDist = groupThreshold+5, dist;
	vector<float>::iterator ir, jr, itheta, jtheta, minIr, minJr, minItheta, minJtheta,
	    iscore, jscore, minIscore, minJscore;
	//compute pairwise distance between detected maxima
	for (ir=rs.begin(), itheta=thetas.begin(), iscore=lineScores.begin();
	     ir!=rs.end(); ir++, itheta++, iscore++)
	    for (jr=ir+1, jtheta=itheta+1, jscore=iscore+1;
		 jr!=rs.end(); jr++, jtheta++, jscore++)
	    {
		//get distance
		dist = 1 * fabs(*ir - *jr) + 1 * fabs(*itheta - *jtheta);
		//check if minimum
		if (dist<minDist)
		{
		    minDist = dist;
		    minIr = ir; minItheta = itheta;
		    minJr = jr; minJtheta = jtheta;
		    minIscore = iscore; minJscore = jscore;
		}
	    }
	//check if minimum distance is less than groupThreshold
	if (minDist >= groupThreshold)
	    stop = true;
	else
	{
	    //put into the first
	    *minIr = (*minIr + *minJr)/2;
	    *minItheta = (*minItheta + *minJtheta)/2;
	    *minIscore = (*minIscore + *minJscore)/2;
	    //delete second one
	    rs.erase(minJr);
	    thetas.erase(minJtheta);
	    lineScores.erase(minJscore);	    
	}		
    }//while

    //put back the lines
    lines.clear();
    //lines.resize(rs.size());
    vector<float> newScores=lineScores;
    lineScores.clear();
    for (int i=0; i<(int)rs.size(); i++)
    {
	//get the line
	Line line;
	mcvIntersectLineRThetaWithBB(rs[i], thetas[i], bbox, &line);
	//put in place descendingly
	vector<float>::iterator iscore;
	vector<Line>::iterator iline;
	for (iscore=lineScores.begin(), iline=lines.begin(); 
	     iscore!=lineScores.end() && newScores[i]<=*iscore; iscore++, iline++);
	lineScores.insert(iscore, newScores[i]);
	lines.insert(iline, line);	
    }
    //clear
    newScores.clear();
}

/** This function performs a RANSAC validation step on the detected lines
 *
 * \param image the input image
 * \param inLines vector of lines
 * \param outLines vector of grouped lines
 * \param groupThreshold the threshold used for grouping
 * \param bbox the bounding box to intersect with
 * \param lineType the line type to work on (horizontal or vertical)
 */
void mcvGetRansacLines(const CvMat *im, vector<Line> &lines, 
		       vector<float> &lineScores, StopLinePerceptorConf *lineConf,
		       unsigned char lineType)
{
    //check if to binarize image
    CvMat *image = cvCloneMat(im);
    if (lineConf->ransacBinarize)
	mcvBinarizeImage(image);

    int width = image->width;
    int height = image->height;
    //try grouping the lines into regions
    float groupThreshold = 15;
    mcvGroupLines(lines, lineScores, lineConf->groupThreshold, cvSize(width, height));

    int window = 15;
    vector<Line> newLines;
    vector<float> newScores;
    for (int i=0; i<(int)lines.size(); i++)
    {
	Line line = lines[i];	    

	CvRect mask;
	switch (lineType)
	{
        case HV_LINES_HORIZONTAL:
	    {
		//get extent
		int ystart = (int)fmax(fmin(line.startPoint.y, line.endPoint.y)-window, 0);
		int yend = (int)fmin(fmax(line.startPoint.y, line.endPoint.y)+window, height-1);
		//get the mask
		mask = cvRect(0, ystart, width, yend-ystart+1);
	    }
	    break;

	case HV_LINES_VERTICAL:	   
	    {
		//get extent of window to search in
		int xstart = (int)fmax(fmin(line.startPoint.x, line.endPoint.x)-window, 0);
		int xend = (int)fmin(fmax(line.startPoint.x, line.endPoint.x)+window, width-1);
		//get the mask
		mask = cvRect(xstart, 0, xend-xstart+1, height);
	    }
	    break;
	}
	//get the subimage to work on
	CvMat *subimage = cvCloneMat(image);
	//clear all but the mask
	mcvSetMat(subimage, mask, 0);
	    
	//get the RANSAC line in this part
	//int numSamples = 5, numIterations = 10, numGoodFit = 15;
	//float threshold = 0.5;
	float lineRTheta[2]={-1,0};
	float lineScore;
	mcvFitRansacLine(subimage, lineConf->ransacNumSamples, 
			 lineConf->ransacNumIterations, 
			 lineConf->ransacThreshold, 
			 lineConf->ransacScoreThreshold, 
			 lineConf->ransacNumGoodFit, &line, lineRTheta, &lineScore);
	    
	//store the line if found
	if (lineRTheta[0]>=0)
	{
	    newLines.push_back(line);
	    newScores.push_back(lineScore);
	}

	//debug
#ifdef DEBUG_GET_STOP_LINES
	if (lineRTheta[0]>0)
	{
	    //get string
	    char str[256];
	    switch (lineType)
	    {
	    case HV_LINES_HORIZONTAL:
		sprintf(str, "Subimage Line H #%d", i);
		break;
	    case HV_LINES_VERTICAL:
		sprintf(str, "Subimage Line V #%d", i);
		break;
	    }
	    //convert image to rgb
	    mcvScaleMat(subimage, subimage);
	    CvMat *subimageClr = cvCreateMat(subimage->rows, subimage->cols, CV_32FC3);
	    cvCvtColor(subimage, subimageClr, CV_GRAY2RGB);
	    //draw line
	    mcvDrawLine(subimageClr, line, CV_RGB(1,0,0), 1);        
	    SHOW_IMAGE(subimageClr, str, 10);
	    //clear
	    cvReleaseMat(&subimageClr);
	}
#endif
	//clear
	cvReleaseMat(&subimage);
    }//for

    //group lines
    lines.clear();
    lineScores.clear();
    mcvGroupLines(newLines, newScores, lineConf->groupThreshold, cvSize(width, height));
    lines = newLines;
    lineScores = newScores;

//     //put lines back in descending order of scores
//     lines.clear();
//     lineScores.clear();
//     vector<Line>::iterator li;
//     vector<float>::iterator si;
//     for (int i=0; i<(int)newLines.size(); i++)
//     {
// 	//get its position
// 	for (li=lines.begin(), si=lineScores.begin();
// 	     si!=lineScores.end() && newScores[i]<=*si;
// 	     si++, li++);
// 	lines.insert(li, newLines[i]);
// 	lineScores.insert(si, newScores[i]);
//     }

    //clean
    newLines.clear();
    newScores.clear();
    cvReleaseMat(&image);
}

/** This function sets the matrix to a value except for the mask window passed in
 *
 * \param inMat input matrix
 * \param mask the rectangle defining the mask: (xleft, ytop, width, height)
 * \param val the value to put
 */
void  mcvSetMat(CvMat *inMat, CvRect mask, double val)
{
    //get x-end points of region to work on, and work on the whole image height
    int xstart = mask.x, xend = mask.x + mask.width-1; //(int)fmax(fmin(line.startPoint.x, line.endPoint.x)-xwindow, 0);
    int ystart = mask.y, yend = mask.y + mask.height-1; //xend = (int)fmin(fmax(line.startPoint.x, line.endPoint.x), width-1);

    //set other two windows to zero
    CvMat maskMat;
    CvRect rect;
    //part to the left of required region
    rect = cvRect(0, 0, xstart-1, inMat->height);
    if (rect.x<inMat->width && rect.y<inMat->height && 
	rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
    {
	cvGetSubRect(inMat, &maskMat, rect);
	cvSet(&maskMat, cvRealScalar(val));
    }
    //part to the right of required region
    rect = cvRect(xend+1, 0, inMat->width-xend-1, inMat->height);
    if (rect.x<inMat->width && rect.y<inMat->height && 
	rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
    {
	cvGetSubRect(inMat, &maskMat, rect);
	cvSet(&maskMat, cvRealScalar(val));
    }

    //part to the top
    rect = cvRect(xstart, 0, mask.width, ystart-1);
    if (rect.x<inMat->width && rect.y<inMat->height && 
	rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
    {
	cvGetSubRect(inMat, &maskMat, rect);
	cvSet(&maskMat, cvRealScalar(val));
    }

    //part to the bottom
    rect = cvRect(xstart, yend+1, mask.width, inMat->height-yend-1);
    if (rect.x<inMat->width && rect.y<inMat->height && 
	rect.x>=0 && rect.y>=0 && rect.width>0 && rect.height>0)
    {
	cvGetSubRect(inMat, &maskMat, rect);
	cvSet(&maskMat, cvRealScalar(val));
    }

}


/** This function fits a Bezier spline to the passed input points
 *
 * \param inPOints Nx2 matrix of points [x,y]
 * \param outPOints Nx2 matrix of points [x,y]
 * \param dim the dimension to sort on (0: x, 1:y)
 * \param dir direction of sorting (0: ascending, 1:descending)
 */
void mcvSortPoints(const CvMat *inPoints, CvMat *outPoints, 
		   int dim, int dir)
{
    //make a copy of the input
    CvMat *pts = cvCloneMat(inPoints);

    //clear the output
    //cvSetZero(outPoints);
    
    //make the list of sorted indices 
    list<int> sorted;
    list<int>::iterator sortedi;
    int i, j;

    //loop on elements and adjust its index
    for (i=0; i<pts->height; i++)
    {
	//if ascending
	if (dir==0)
	    for (sortedi = sorted.begin(); 
		 sortedi != sorted.end() && 
		     (CV_MAT_ELEM(*pts, float, i, dim) >= 
		     CV_MAT_ELEM(*outPoints, float, *sortedi, dim));
		 sortedi++);
	//descending
	else
	    for (sortedi = sorted.begin(); 
		 sortedi != sorted.end() && 
		     (CV_MAT_ELEM(*pts, float, i, dim) <= 
		     CV_MAT_ELEM(*outPoints, float, *sortedi, dim));
		 sortedi++);
	
	//found the position, so put it into sorted
	sorted.insert(sortedi, i);
    }

    //sorted the array, so put back
    for (i=0, sortedi=sorted.begin(); 
	 sortedi != sorted.end(); 
	 sortedi++, i++)
	for(j=0; j<outPoints->width; j++)
	    CV_MAT_ELEM(*outPoints, float, i, j) = 
		CV_MAT_ELEM(*pts, float, *sortedi, j);

    //clear
    cvReleaseMat(&pts);
    sorted.clear();
}

/** This function fits a Bezier spline to the passed input points
 *
 * \param points the input points
 * \param degree the required spline degree
 * \return spline the returned spline
 */
Spline mcvFitBezierSpline(CvMat *points, int degree)
{

    //set the degree
    Spline spline;
    spline.degree = degree;

    //get number of points
    int n = points->height;
    float step = 1./(n-1);

    //M matrices: M2 for quadratic (degree 2) and M3 for cubic
    float M2[] = {1, -2, 1, 
		  -2, 2, 0, 
		  1, 0, 0};
    float M3[] = {-1, 3, -3, 1,
		  3, -6, 3, 0,
		  -3, 3, 0, 0,
		  1, 0, 0, 0};

    //M matrix for Bezier
    CvMat M;

    //Basis matrix
    CvMat *B;

    //u value for points to create the basis matrix
    float u = 0.f;

    //switch on the degree
    switch(degree)
    {
    //Quadratic spline
    case 2:
	//M matrix
	M = cvMat(3, 3, CV_32FC1, M2);

	//create the basis matrix
	B = cvCreateMat(n, 3, CV_32FC1);
	for (int i=0; i<B->height; i++, u+=step)
	{
	    CV_MAT_ELEM(*B, float, i, 2) = 1;  //1
	    CV_MAT_ELEM(*B, float, i, 1) = u;  //u
	    CV_MAT_ELEM(*B, float, i, 0) = u*u;  //u^2
	}

     	break;

    //Cubic spline
    case 3:

	//M matrix
	M = cvMat(4, 4, CV_32FC1, M3);

	//create the basis matrix
	B = cvCreateMat(n, 4, CV_32FC1);
	for (int i=0; i<B->height; i++, u+=step)
	{
	    CV_MAT_ELEM(*B, float, i, 3) = 1;  //1
	    CV_MAT_ELEM(*B, float, i, 2) = u;  //u
	    CV_MAT_ELEM(*B, float, i, 1) = u*u;  //u^2
	    CV_MAT_ELEM(*B, float, i, 0) = u*u*u;  //u^2
	}
	break;
    }

    //multiply B by M
    cvMatMul(B, &M, B);

    //sort the points
    mcvSortPoints(points, points, 1, 0);

    //return the required control points by LS
    CvMat *sp = cvCreateMat(degree+1, 2, CV_32FC1);
    cvSolve(B, points, sp, CV_SVD);

    //put back into spline
    memcpy((float *)spline.points, sp->data.fl, sizeof(float)*(spline.degree+1)*2);
//     if(spline.points[0].x<0)
// 	SHOW_MAT(points, "INput Points");

    //clear
    cvReleaseMat(&B);
    cvReleaseMat(&sp);

    //return
    return spline;
}



/** This function evaluates Bezier spline with given resolution
 *
 * \param spline input spline
 * \param h the input resolution
 * \return computed points in an array Nx2 [x,y]
 */
CvMat* mcvEvalBezierSpline(Spline &spline, float h)
{
    //compute number of points to return
    int n = (int)(1./h)+1;

    //allocate the points
    CvMat *points = cvCreateMat(n, 2, CV_32FC1);

    //M matrices
    CvMat M;
    float M2[] = {1, -2, 1, 
		  -2, 2, 0, 
		  1, 0, 0};
    float M3[] = {-1, 3, -3, 1,
		  3, -6, 3, 0,
		  -3, 3, 0, 0,
		  1, 0, 0, 0};

    //spline points
    CvMat *sp = cvCreateMat(spline.degree+1, 2, CV_32FC1);
    memcpy(sp->data.fl, (float *)spline.points, sizeof(float)*(spline.degree+1)*2);

    //abcd
    CvMat *abcd;

    float P[2], dP[2], ddP[2], dddP[2];
    float h2 = h*h, h3 = h2*h;

    //switch the degree
    switch(spline.degree)
    {
	//Quadratic
    case 2:
	//get M matrix
	M = cvMat(3, 3, CV_32FC1, M2);

	//get abcd where a=row 0, b=row 1, ...
	abcd = cvCreateMat(3, 2, CV_32FC1);
	cvMatMul(&M, sp, abcd);
	
	//P = c
	P[0] = CV_MAT_ELEM(*abcd, float, 2, 0);
	P[1] = CV_MAT_ELEM(*abcd, float, 2, 1);
	
	//dP = b*h+a*h^2
	dP[0] = CV_MAT_ELEM(*abcd, float, 1, 0)*h + 
	    CV_MAT_ELEM(*abcd, float, 0, 0)*h2;
	dP[1] = CV_MAT_ELEM(*abcd, float, 1, 1)*h + 
	    CV_MAT_ELEM(*abcd, float, 0, 1)*h2;

	//ddP = 2*a*h^2
	ddP[0] = 2 * CV_MAT_ELEM(*abcd, float, 0, 0)*h2;
	ddP[1] = 2 * CV_MAT_ELEM(*abcd, float, 0, 1)*h2;

	//loop and put points
	for (int i=0; i<n; i++)
	{
	    //put point
	    CV_MAT_ELEM(*points, float, i, 0) = P[0];
	    CV_MAT_ELEM(*points, float, i, 1) = P[1];

	    //update 
	    P[0] += dP[0]; P[1] += dP[1];
	    dP[0] += ddP[0]; dP[1] += ddP[1];
	}
	
	break;

	/*Cubic*/
    case 3:
	//get M matrix
	M = cvMat(4, 4, CV_32FC1, M3);

	//get abcd where a=row 0, b=row 1, ...
	abcd = cvCreateMat(4, 2, CV_32FC1);
	cvMatMul(&M, sp, abcd);
	
	//P = d
	P[0] = CV_MAT_ELEM(*abcd, float, 3, 0);
	P[1] = CV_MAT_ELEM(*abcd, float, 3, 1);
	
	//dP = c*h + b*h^2+a*h^3
	dP[0] = CV_MAT_ELEM(*abcd, float, 2, 0)*h + 
	    CV_MAT_ELEM(*abcd, float, 1, 0)*h2 +
	    CV_MAT_ELEM(*abcd, float, 0, 0)*h3;
	dP[1] = CV_MAT_ELEM(*abcd, float, 2, 1)*h + 
	    CV_MAT_ELEM(*abcd, float, 1, 1)*h2 +
	    CV_MAT_ELEM(*abcd, float, 0, 1)*h3;

	//dddP = 6 * a * h3
	dddP[0] = 6 * CV_MAT_ELEM(*abcd, float, 0, 0) * h3;
	dddP[1] = 6 * CV_MAT_ELEM(*abcd, float, 0, 1) * h3;

	//ddP = 2*b*h2 + 6*a*h3
	ddP[0] = 2 * CV_MAT_ELEM(*abcd, float, 1, 0) * h2 + dddP[0];
	ddP[1] = 2 * CV_MAT_ELEM(*abcd, float, 1, 1) * h2 + dddP[1];

	//loop and put points
	for (int i=0; i<n; i++)
	{
	    //put point
	    CV_MAT_ELEM(*points, float, i, 0) = P[0];
	    CV_MAT_ELEM(*points, float, i, 1) = P[1];

	    //update 
	    P[0] += dP[0]; P[1] += dP[1];
	    dP[0] += ddP[0]; dP[1] += ddP[1];
	    ddP[0] += dddP[0]; ddP[1] += dddP[1];
	}
	break;
    }

    //clear
    cvReleaseMat(&abcd);
    cvReleaseMat(&sp);

    //return
    return points;
}


/** This function returns pixel coordinates for the Bezier
 * spline with the given resolution.
 *
 * \param spline input spline
 * \param h the input resolution
 * \param box the bounding box
 * \return computed points in an array Nx2 [x,y]
 */
CvMat* mcvGetBezierSplinePixels(Spline &spline, float h, CvSize box)
{
    //get the points belonging to the spline
    CvMat * points = mcvEvalBezierSpline(spline, h);


    //pixelize the spline
    //CvMat *inpoints = cvCreateMat(points->height, 1, CV_8SC1);
    //cvSet(, CvScalar value, const CvArr* mask=NULL);
    list<int> inpoints;
    list<int>::iterator inpointsi;
    int lastin = -1, numin = 0;
    for (int i=0; i<points->height; i++)
    {
	//round
	CV_MAT_ELEM(*points, float, i, 0) = cvRound(CV_MAT_ELEM(*points, float, i, 0));
	CV_MAT_ELEM(*points, float, i, 1) = cvRound(CV_MAT_ELEM(*points, float, i, 1));

	//check boundaries
	if(CV_MAT_ELEM(*points, float, i, 0) >= 0 && 
	   CV_MAT_ELEM(*points, float, i, 0) < box.width && 
	   CV_MAT_ELEM(*points, float, i, 1) >= 0 &&
	   CV_MAT_ELEM(*points, float, i, 1) < box.height)	    
	{
	    //it's inside, so check if the same as last one
	    if(lastin<0 || 
	       (lastin>=0 && 
		!(CV_MAT_ELEM(*points, float, lastin, 1)==
		  CV_MAT_ELEM(*points, float, i, 1) &&
		  CV_MAT_ELEM(*points, float, lastin, 0)==
		  CV_MAT_ELEM(*points, float, i, 0) )) )
	    {
		//put inside
		//CV_MAT_ELEM(*inpoints, char, i, 0) = 1;
		inpoints.push_back(i);
		lastin = i;
		numin++;
	    }	       
	}
    }

    //put the results in another matrix
    CvMat *rpoints = cvCreateMat(numin, 2, CV_32SC1);
    int ri;
    for (ri=0, inpointsi=inpoints.begin(); 
	 inpointsi!=inpoints.end(); ri++, inpointsi++)
    {
	CV_MAT_ELEM(*rpoints, int, ri, 0) = 
	    (int)CV_MAT_ELEM(*points, float, *inpointsi, 0);
	CV_MAT_ELEM(*rpoints, int, ri, 1) = 
	    (int)CV_MAT_ELEM(*points, float, *inpointsi, 1);
    }
//     int ri=0;
//     for (int i=0; i<points->height; i++)
// 	//if inside
// 	if(CV_MAT_ELEM(*inpoints, char, i, 0)==1)
// 	{
// 	    //put point
// 	    CV_MAT_ELEM(*rpoints, int, ri, 0) = (int)CV_MAT_ELEM(*points, float, i, 0);
// 	    CV_MAT_ELEM(*rpoints, int, ri, 1) = (int)CV_MAT_ELEM(*points, float, i, 1);
// 	    //inc
// 	    ri++;
// 	}

    //release
    //    cvReleaseMat(&inpoints);
    cvReleaseMat(&points);
    inpoints.clear();

    //return
    return rpoints;
}


/** This function performs a RANSAC validation step on the detected lines to
 * get splines
 *
 * \param image the input image
 * \param lines vector of input lines to refine
 * \param lineSCores the line scores input
 * \param groupThreshold the threshold used for grouping
 * \param bbox the bounding box to intersect with
 * \param lineType the line type to work on (horizontal or vertical)
 */
void mcvGetRansacSplines(const CvMat *im, vector<Line> &lines, 
			 vector<float> &lineScores, StopLinePerceptorConf *lineConf,
			 unsigned char lineType, vector<Spline> &splines, 
			 vector<float> &splineScores)
{
	
    //check if to binarize image
    CvMat *image = cvCloneMat(im);
    if (lineConf->ransacBinarize)
	mcvBinarizeImage(image);

    int width = image->width;
    int height = image->height;
    //try grouping the lines into regions
    float groupThreshold = 15;
    mcvGroupLines(lines, lineScores, lineConf->groupThreshold, cvSize(width, height));

    int window = 15;
    vector<Spline> newSplines;
    vector<float> newSplineScores;
    for (int i=0; i<(int)lines.size(); i++)
    {
	Line line = lines[i];	    

	CvRect mask;
	switch (lineType)
	{
        case HV_LINES_HORIZONTAL:
	    {
		//get extent
		int ystart = (int)fmax(fmin(line.startPoint.y, line.endPoint.y)-window, 0);
		int yend = (int)fmin(fmax(line.startPoint.y, line.endPoint.y)+window, height-1);
		//get the mask
		mask = cvRect(0, ystart, width, yend-ystart+1);
	    }
	    break;

	case HV_LINES_VERTICAL:	   
	    {
		//get extent of window to search in
		int xstart = (int)fmax(fmin(line.startPoint.x, line.endPoint.x)-window, 0);
		int xend = (int)fmin(fmax(line.startPoint.x, line.endPoint.x)+window, width-1);
		//get the mask
		mask = cvRect(xstart, 0, xend-xstart+1, height);
	    }
	    break;
	}
	//get the subimage to work on
	CvMat *subimage = cvCloneMat(image);
	//clear all but the mask
	mcvSetMat(subimage, mask, 0);
	    
	//get the RANSAC spline in this part
	//int numSamples = 5, numIterations = 10, numGoodFit = 15;
	//float threshold = 0.5;
	Spline spline;
	float splineScore;
	//resolution to use in pixelizing the spline
	float h = .1; //1. / max(image->width, image->height);
	mcvFitRansacSpline(subimage, lineConf->ransacNumSamples, 
			   lineConf->ransacNumIterations, 
			   lineConf->ransacThreshold, 
			   lineConf->ransacScoreThreshold, 
			   lineConf->ransacNumGoodFit, 
			   lineConf->ransacSplineDegree, h,
			   &spline, &splineScore);

	//store the line if found
	if (spline.degree > 0)
	{
	    newSplines.push_back(spline);
	    newSplineScores.push_back(splineScore);
	}

	//debug
#ifdef DEBUG_GET_STOP_LINES
	if (spline.degree > 0)
	{
	    //get string
	    char str[256];
	    switch (lineType)
	    {
	    case HV_LINES_HORIZONTAL:
		sprintf(str, "Subimage Spline H #%d", i);
		break;
	    case HV_LINES_VERTICAL:
		sprintf(str, "Subimage Spline V #%d", i);
		break;
	    }
	    //convert image to rgb
	    mcvScaleMat(subimage, subimage);
	    CvMat *subimageClr = cvCreateMat(subimage->rows, subimage->cols, CV_32FC3);
	    cvCvtColor(subimage, subimageClr, CV_GRAY2RGB);
	    //draw spline
	    mcvDrawSpline(subimageClr, spline, CV_RGB(1,0,0), 1);        
	    SHOW_IMAGE(subimageClr, str, 10);
	    //clear
	    cvReleaseMat(&subimageClr);
	}
#endif
	//clear
	cvReleaseMat(&subimage);
    }//for

    //put splines back in descending order of scores
    splines.clear();
    splineScores.clear();
    vector<Spline>::iterator li;
    vector<float>::iterator si;
    for (int i=0; i<(int)newSplines.size(); i++)
    {
	//get its position
	for (li=splines.begin(), si=splineScores.begin();
	     si!=splineScores.end() && newSplineScores[i]<=*si;
	     si++, li++);
	splines.insert(li, newSplines[i]);
	splineScores.insert(si, newSplineScores[i]);
    }

    //clean
    newSplines.clear();
    newSplineScores.clear();
    cvReleaseMat(&image);
}



/** This functions implements RANSAC algorithm for spline fitting
    given an image

 * 
 * \param image input image
 * \param numSamples number of samples to take every iteration
 * \param numIterations number of iterations to run
 * \param threshold threshold to use to assess a point as a good fit to a line
 * \param numGoodFit number of points close enough to say there's a good fit 
 * \param splineDegree the spline degree to fit
 * \param h the resolution to use for splines
 * \param spline the fitted line 
 * \param splineScore the score of the line detected
 *
 */
void mcvFitRansacSpline(const CvMat *image, int numSamples, int numIterations,
			float threshold, float scoreThreshold, int numGoodFit, 
			int splineDegree, float h, Spline *spline, float *splineScore)
{

    //get the points with non-zero pixels
    CvMat *points;
    points = mcvGetNonZeroPoints(image, true);
    //subtract half
    cvAddS(points, cvRealScalar(0.5), points);

    //normalize pixels values to get weights of each non-zero point
    //get third row of points containing the pixel values
    CvMat w;
    cvGetRow(points, &w, 2);
    //normalize it
    CvMat *weights = cvCloneMat(&w);
    cvNormalize(weights, weights, 1, 0, CV_L1);
    //get cumulative    sum
    mcvCumSum(weights, weights);

    //random number generator
    CvRNG rng = cvRNG(0xffffffff);
    //matrix to hold random sample
    CvMat *randInd = cvCreateMat(numSamples, 1, CV_32SC1);
    CvMat *samplePoints = cvCreateMat(numSamples, 2, CV_32FC1);
    //flag for points currently included in the set
    CvMat *pointIn = cvCreateMat(1, points->cols, CV_8SC1);
    //returned splines
    Spline curSpline, bestSpline;
    bestSpline.degree = 0;//initialize
    float bestScore=0, bestDist=1e5;


    //outer loop
    for (int i=0; i<numIterations; i++)
    {
	//set flag to zero
	cvSetZero(pointIn);
	//get random sample from the points
	//cvRandArr(&rng, randInd, CV_RAND_UNI, cvRealScalar(0), cvRealScalar(points->cols));
	mcvSampleWeighted(weights, numSamples, randInd, &rng);
	//SHOW_MAT(randInd, "randInd");
	for (int j=0; j<numSamples; j++)
	{
	    //flag it as included
	    CV_MAT_ELEM(*pointIn, char, 0, CV_MAT_ELEM(*randInd, int, j, 0)) = 1;
	    //put point
	    CV_MAT_ELEM(*samplePoints, float, j, 0) = 
		CV_MAT_ELEM(*points, float, 0, CV_MAT_ELEM(*randInd, int, j, 0));
	    CV_MAT_ELEM(*samplePoints, float, j, 1) = 
		CV_MAT_ELEM(*points, float, 1, CV_MAT_ELEM(*randInd, int, j, 0));
	}

	//fit the spline
	curSpline = mcvFitBezierSpline(samplePoints, splineDegree);

	//get the pixels that belong to the spline
	CvMat *pixels = mcvGetBezierSplinePixels(curSpline, h, 
						 cvSize(image->width, image->height));

	//compute its score by summing up pixel values belonging to it
	float score = 0.f;
	int jitter[] = {0, 1, -1}, jitterLength = 3;
	for (int j=0; j<jitterLength; j++)
	    for (int i=0; i<pixels->height; i++)
	    {
		//jitter the x
		score += cvGetReal2D(image, 
				     CV_MAT_ELEM(*pixels, int, i, 1), 
				     MIN(MAX(CV_MAT_ELEM(*pixels, int, i, 0)+
					     jitter[j], 0), image->width-1));
		//jitter the y
		score += cvGetReal2D(image, 
				     MIN(MAX(CV_MAT_ELEM(*pixels, int, i, 1)+
					     jitter[j], 0), image->height-1),
				     CV_MAT_ELEM(*pixels, int, i, 0));
		//CV_MAT_ELEM(*points, float, 2, CV_MAT_ELEM);
	    }
	//score += .5*pixels->height;

	//implement jitter

	//clear pixels
	cvReleaseMat(&pixels);

 	//check if better than best score so far
	if (score>bestScore && score >= scoreThreshold)
	{
	    //put it
	    bestScore = score;
	    bestSpline = curSpline;
	}

	//show image for debugging
#ifdef DEBUG_GET_STOP_LINES

	//get string
	char str[256];
	//sprintf(str, "Spline Fit: score=%f, best=%f", score, bestScore);
	sprintf(str, "Spline Fit");	

	//convert image to rgb
	CvMat *imageClr = cvCreateMat(image->rows, image->cols, 
					 CV_32FC3);
	cvCvtColor(image, imageClr, CV_GRAY2RGB);
	//draw spline
	if(curSpline.degree>0)
	    mcvDrawSpline(imageClr, curSpline, CV_RGB(1,0,0), 1);
	if(bestSpline.degree>0)
	    mcvDrawSpline(imageClr, bestSpline, CV_RGB(0,0,1), 1);
	SHOW_IMAGE(imageClr, str, 10);
	//clear
	cvReleaseMat(&imageClr);
   
#endif


    } //for

    //return
    if (spline)
	*spline = bestSpline;
    if (splineScore)
	*splineScore = bestScore;
    

    //clear
    cvReleaseMat(&points);
    cvReleaseMat(&samplePoints);
    cvReleaseMat(&randInd);
    cvReleaseMat(&pointIn);
    cvReleaseMat(&weights);
}

/** This function draws a spline onto the passed image
 * 
 * \param image the input iamge
 * \param spline input spline
 * \param spline color
 * 
 */
void mcvDrawSpline(CvMat *image, Spline spline, CvScalar color, int width)
{
    //get spline pixels
    CvMat *pixels = mcvGetBezierSplinePixels(spline, .05, 
					     cvSize(image->width, image->height));
    
    //draw pixels in image with that color
    for (int i=0; i<pixels->height-1; i++)
// 	cvSet2D(image, 
// 		(int)cvGetReal2D(pixels, i, 1),
// 		(int)cvGetReal2D(pixels, i, 0),
// 		color);
	cvLine(image, 
	       cvPoint((int)cvGetReal2D(pixels, i, 0),
		       (int)cvGetReal2D(pixels, i, 1)),
	       cvPoint((int)cvGetReal2D(pixels, i+1, 0),
		       (int)cvGetReal2D(pixels, i+1, 1)),
	       color, width);

    //put the control points with circles
    for (int i=0; i<spline.degree+1; i++)
	cvCircle(image, cvPointFrom32f(spline.points[i]), 3, color, -1);

    //release
    cvReleaseMat(&pixels);
}


/** This function converts lines from IPM image coordinates back to image coordinates
 * 
 * \param lines the input lines
 * \param ipmInfo the IPM info
 * \param cameraInfo the camera info
 * \param imSize the output image size (for clipping)
 * 
 */
void mcvLinesImIPM2Im(vector<Line> &lines, IPMInfo &ipmInfo, CameraInfo &cameraInfo, 
		      CvSize imSize)
{
    //check if returned anything
    if (lines.size()!=0)
    {                
	//convert the line into world frame
	for (unsigned int i=0; i<lines.size(); i++)
	{
	    Line *line;
	    line = & (lines[i]);
		
	    mcvPointImIPM2World(&(line->startPoint), &ipmInfo);
	    mcvPointImIPM2World(&(line->endPoint), &ipmInfo);
	}
	    
	//convert them from world frame into camera frame
	//
	//put a dummy line at the beginning till we check that cvDiv bug
	Line dummy = {{1.,1.},{2.,2.}};
	lines.insert(lines.begin(), dummy);
	//convert to mat and get in image coordinates
	CvMat *mat = cvCreateMat(2, 2*lines.size(), FLOAT_MAT_TYPE);
	mcvLines2Mat(&lines, mat);
	lines.clear();
	mcvTransformGround2Image(mat, mat, &cameraInfo);
	//get back to vector
	mcvMat2Lines(mat, &lines);
	//remove the dummy line at the beginning
	lines.erase(lines.begin());
	//clear
	cvReleaseMat(&mat);
	    
	//clip the lines found and get their extent
	for (unsigned int i=0; i<lines.size(); i++)
	{
	    //clip
	    mcvIntersectLineWithBB(&(lines[i]), imSize, &(lines[i]));
	    
	    //get the line extent
	    //mcvGetLineExtent(inImage, (*stopLines)[i], (*stopLines)[i]);
	}
    }        
}


/** This function converts splines from IPM image coordinates back to image coordinates
 * 
 * \param splines the input splines
 * \param ipmInfo the IPM info
 * \param cameraInfo the camera info
 * \param imSize the output image size (for clipping)
 * 
 */
void mcvSplinesImIPM2Im(vector<Spline> &splines, IPMInfo &ipmInfo, CameraInfo &cameraInfo, 
			CvSize imSize)
{
    //loop on splines and convert
    for (int i=0; i<(int)splines.size(); i++)
    {                
	//get points for this spline in IPM image
	CvMat *points = mcvEvalBezierSpline(splines[i], .1);

	//transform these points to image coordinates
	CvMat *points2 = cvCreateMat(2, points->height, CV_32FC1);
	cvTranspose(points, points2);
	//mcvPointImIPM2World(CvMat *mat, const IPMInfo *ipmInfo);
	//mcvTransformGround2Image(points2, points2, &cameraInfo);
	mcvTransformImIPM2Im(points2, points2, &ipmInfo, &cameraInfo);
	cvTranspose(points2, points);
	cvReleaseMat(&points2);	

	//refit the points into a spline in output image
	splines[i] = mcvFitBezierSpline(points, splines[i].degree);
    }        
}


/** This function samples uniformly with weights
 * 
 * \param cumSum cumulative sum for normalized weights for the differnet samples (last is 1)
 * \param numSamples the number of samples
 * \param randInd a 1XnumSamples of int containing the indices
 * \param rng a pointer to a random number generator
 * 
 */
void mcvSampleWeighted(const CvMat *cumSum, int numSamples, CvMat *randInd, CvRNG *rng)
{
//     //get cumulative sum of the weights
//     //OPTIMIZE:should pass it later instead of recomputing it
//     CvMat *cumSum = cvCloneMat(weights);
//     for (int i=1; i<weights->cols; i++)
// 	CV_MAT_ELEM(*cumSum, float, 0, i) += CV_MAT_ELEM(*cumSum, float, 0, i-1);

    //loop
    int i=0;
    while(i<numSamples)
    {
	//get random number
	double r = cvRandReal(rng);

	//get the index from cumSum
	int j;
	for (j=0; j<cumSum->cols && r>CV_MAT_ELEM(*cumSum, float, 0, j); j++);

	//make sure this index wasnt chosen before
	bool put = true;
	for (int k=0; k<i; k++)
	    if (CV_MAT_ELEM(*randInd, int, k, 0) == j)
		//put it
		put = false;


	if (put)
	{
	    //put it in array
	    CV_MAT_ELEM(*randInd, int, i, 0) = j;
	    //inc
	    i++;
	}
    }
}

/** This function computes the cumulative sum for a vector
 * 
 * \param inMat input matrix
 * \param outMat output matrix
 * 
 */
void mcvCumSum(const CvMat *inMat, CvMat *outMat)
{

#define MCV_CUM_SUM(type) 				\
    /*row vector*/ 					\
    if(inMat->rows == 1) 				\
	for (int i=1; i<outMat->cols; i++) 		\
	    CV_MAT_ELEM(*outMat, type, 0, i) += 	\
		CV_MAT_ELEM(*outMat, type, 0, i-1); 	\
    /*column vector*/					\
    else						\
	for (int i=1; i<outMat->rows; i++) 		\
	    CV_MAT_ELEM(*outMat, type, i, 0) += 	\
		CV_MAT_ELEM(*outMat, type, i-1, 0);
                
    //copy to output if not equal
    if(inMat != outMat)
	cvCopy(inMat, outMat);

    //check type
    if (CV_MAT_TYPE(inMat->type)==CV_32FC1)
    {
        MCV_CUM_SUM(float)
    }
    else if (CV_MAT_TYPE(inMat->type)==CV_32SC1)
    {
        MCV_CUM_SUM(int)
    }
    else
    {
        cerr << "Unsupported type in mcvCumSum\n";
        exit(1); 
    }                            
}


void dummy()
{
}

