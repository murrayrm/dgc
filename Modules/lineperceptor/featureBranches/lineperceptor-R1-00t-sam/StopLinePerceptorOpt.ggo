#Parameters for stop line perceptor
#command line: gengetopt -i stopLinePerceptorOpt.ggo -F stopLinePerceptorOpt \
#	  --func-name=stopLinePerceptorParser --arg-struct-name=StopLinePerceptorParserInfo \
#	  --conf-parser

package "stopLinePerceptor"
version "0.1"

option "ipmWidth"  	- 
	"width of IPM image to use" int required
option "ipmHeight"  	- 
	"height of IPM image to use" int required
option "lineWidth" 	- 
	"width of line to detect in mm (in the world)" double required
option "lineHeight" - 
	"height of line to detect in mm (in the world)" double required
option "kernelWidth" - 
	"widht of kernel to use for filtering" int required
option "kernelHeight" 	- 
	"Height of kernel to use for filtering" int required
option "lowerQuantile" 			- 
	"lower quantile to use for thresholding the filtered image" 
	double required
option "localMaxima" 			- 
	"whether to return local maxima or just the maximum" 
	int required
option "groupingType" 	- 
	"type of grouping to use (default 0: HV lines)" int required
option "binarize" 	- 
	"whether to binarize the thresholded image or use the raw values" 
	double required
option "detectionThreshold" 	- 
	"threshold for line scores to declare as line" double required
option "smoothScores" 	- 
	"whether to smooth scores of lines detected or not" int required

option "rMin" 	- 
	"rMin for Hough transform (in pixels)" double required
option "rMax" 	- 
	"rMax for Hough transform (in pixels)" double required
option "rStep" 	- 
	"rStep for Hough transform (in pixels)" double required
option "thetaMin" 	- 
	"thetaMin for Hough transform (in degrees)" double required
option "thetaMax" 	- 
	"thetaMax for Hough transform (in degrees)" double required
option "thetaStep" 	- 
	"thetaStep for Hough transform (in degrees)" double required

option "ipmVpPortion" 	- 
	"Portion of IPM image height to add to y-coordinate of VP" double required

option "getEndPoints" -
	"Get the endpoints of the line" int required

option "group" 	- 
	"group nearby lines or not (default 1: group)" int required

option "groupThreshold" 	- 
	"Threshold for grouping nearby lines (default 10)" double required

#RANSAC options
option "ransac" -
	"use RANSAC (1) or not (0)" int required

option "ransacNumSamples" 	- 
	"Number of samples to use for RANSAC" int required

option "ransacNumIterations" 	- 
	"Number of iterations to use for RANSAC" int required

option "ransacNumGoodFit" 	- 
	"Number of close points to consider a good line fit" int required

option "ransacThreshold" 	- 
	"Threshold to consider a point close" double required
option "ransacScoreThreshold" -
	"Threshold for detected line scores" double required
option "ransacBinarize" -
	"Whether to binarize image for RANSAC or not" int required
option "ransacSplineDegree" -
	"Degree of spline to use" int required
option "ransacSpline" -
	"Whether to use splines" int required
option "ransacLine" -
	"Whether to use lines" int required