/**
 * \file LineTracker.cc
 * \author Mohamed Aly
 * \date 31 July 2007
 * \brief This file contains the implementation for the LineTracker classes
 *
 */

#include "LineTracker.hh"


int Tracker::NUM_ABSENT_FRAMES = 3;

int Tracker::NUM_SEEN_FRAMES = 5;


int Tracker::clean()
{

    vector<Track*>::iterator track;

    //send all *valid* tracks and delete tracks marked for removal
    for (track = this->tracks.end()-1; track >= this->tracks.begin(); track--)
	//check if to remove
	if ((*track)->remove)
	{
	    delete *track;
	    this->tracks.erase(track);
	}


    return 0;
}



int SplineTracker::update(vector<Track*>& measurements, long frameId)
{
    unsigned int i, j;


    //loop and see if to merge, and if yes then merge
    for (i=0; i<measurements.size(); ++i)
	for (j=0; j<this->tracks.size(); j++)
	{
	    //if to merge
	    if (this->tracks[j]->checkMerge(*measurements[i]))
	    {
		//merge with existing one
		this->tracks[j]->merge(*measurements[i]);
		//mark it for deletion
		measurements[i]->remove = true;
		//update existing one
		this->tracks[j]->frameLastSeen = measurements[i]->frameId;
		//goto next measurement
		break;
	    }
	       
	}

    //do the same with existing tracks
    //loop and see if to merge, and if yes then merge
    for (i=0; i<this->tracks.size(); ++i)
	for (j=i+1; j<this->tracks.size(); ++j)
	    //if to merge
	    if (!this->tracks[i]->remove &&
		!this->tracks[j]->remove &&
		this->tracks[i]->checkMerge(*this->tracks[j]))
	    {
		//merge with existing one
		this->tracks[i]->merge(*this->tracks[j]);
		//mark it for deletion
		this->tracks[j]->remove = true;
		//goto next measurement
		break;
	    }


    //now check existing tracks and see whether to remove any of them
    vector<Track>::iterator track;
    for (i=0; i<this->tracks.size(); i++)
    {
	//update the numAbsentFrames
	this->tracks[i]->numAbsentFrames = frameId - this->tracks[i]->frameLastSeen;

	//mark for deletion
	if (this->tracks[i]->numAbsentFrames > Tracker::NUM_ABSENT_FRAMES)
	    this->tracks[i]->remove = true;

	//update numSeenFrames
	if (this->tracks[i]->numAbsentFrames != 0)
	    this->tracks[i]->numSeenFrames = 0;
	else
	    this->tracks[i]->numSeenFrames++;

	//check if to consider a valid frame or not
	if (!this->tracks[i]->valid && !this->tracks[i]->remove &&
	    this->tracks[i]->numSeenFrames > Tracker::NUM_SEEN_FRAMES)
	{
	    //get a new map id for this track
	    this->tracks[i]->getMapId(this->mapIdPrefix);
	    //mark as valid for sending to map
	    this->tracks[i]->valid = true;
	}

    }

    //add new measurements that were not merged
    for (i=0; i<measurements.size(); ++i)
	if (!measurements[i]->remove)
	    this->tracks.push_back(measurements[i]);

//     //remove those marked for removal (not merged)
//     for (i=0; i<measurements.size(); ++i)
// 	if (measurements[i]->remove)
// 	    delete measurements[i];

    return 0;

}


int SplineTracker::sendToMap(CMapElementTalker& mapTalker, StereoImageBlob* stereoBlob)
{
    vector<Track*>::iterator track;
    //send all *valid* tracks and delete tracks marked for removal
    for (track = this->tracks.end()-1; track >= this->tracks.begin(); track--)
    {
	//check if to remove
	if ((*track)->remove)
	{
	    (*track)->deleteFromMap(mapTalker);
	    //this->tracks.erase(track);
	}

	//check if to send
	else if ((*track)->valid)
	    (*track)->sendToMap(mapTalker, stereoBlob);
    }

    return 0;
}
