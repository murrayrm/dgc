              Release Notes for "lineperceptor" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "lineperceptor" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "lineperceptor" module can be found in
the ChangeLog file.


Release R1-01q (Mon Nov 12  2:17:17 2007):
	Set timeout for deleting stop lines to 3000 seconds

Release R1-01p (Tue Oct 23  9:46:58 2007):
	Added more options to color processing.

Release R1-01o (Wed Oct  3 12:24:58 2007):
	Fixed a small bug in RoadLineBlob

Release R1-01n (Wed Oct  3 12:10:08 2007):
	Added interface for RoadLineBlob.


Release R1-01m (Tue Sep 25 16:41:57 2007):
	Added checks for colors of lanes, and sending lines to map with
	either yellow (yellow lane) or green (white lane). Still need to be
	tested thouroughly. Also modified the RANSAC line fitting to get
	only the line extended by the inliers, not the infinite line.
	Should help with lines extended in the intersection.


Release R1-01l (Thu Sep 20 15:45:20 2007):
	Fixed a bug with disparity checking that send bad lines to map

Release R1-01g (Mon Aug 27 12:45:48 2007):
	Updated tracking to eliminate dragging lines


Release R1-01f (Thu Aug 23 10:27:35 2007):
	Fixed a bug that was using deprecated function to send map elements

	Adjust spline fit to take ratio of distance instead of
	y-coordinate, and this should be more robust for more curved
	splines


Release R1-01e (Mon Aug 20 17:20:00 2007):
	Adjusted config files for short range to work in St Luke. Adjusted
	merging splines to take into account frameLastUpdated parameter.
	Added horizontal strips to work on instead of the whole image.
	Added a limit on the extension of splines (bounding box) in image
	plane.

	Improved the spline fitting to take the step as a ratio of
	y-coordinate instead of uniform

	Adjusted functions to be more modular by making GetLines() and
	PostprocessLines() functions

Release R1-01d (Thu Aug 16 23:36:51 2007):
	Adjusted hough transform to get points above certain threshold when
	not computing local maxima. Also, added --conf-path option to
	LinePerceptor to read the conf files from instead of the
	environment variable DGC_CONFIG_PATH if this option is given

	Added process control interface. Need to test

	Working on the red channel instead of converting to RGB. This looks
	better for the yellow lines.

	Fixed sending map elements from medium range stereo, truncate
	points that are too close (4m) or too far(100m)


Release R1-01c (Fri Aug 10  7:53:45 2007):
	Added region of interest to IPM calculations to get rid of black
	areas in medium and long stereo images

	Added clearing of points outside IPM after filtering, which very
	much cleans up the fitlered image, and working fine for Medium and
	Short range now

	Added bilinear interpolation for IPM calculations

	Fixed a bug and memory leak in mcvFitRansacSpline() that causes seg
	fault when not fitting properly

	Added another overall stat to LinePerceptor. Adjusted tracking for
	medium range (still more to be done)

	Stoplines and lanes working fine on both short and medium. No
	broken lines yet though.

	Added option to use either left or right camera from the stereo
	pair

	Added stopline tracking and adjusted config files handling in
	LinePerceptor, it now picks the right conf file based on the sensor
	id

	Try to adjust medium range lanes in map, they seem wierd on the
	left side, possibly something has to do with disparity

	Added all options to conf files


Release R1-01b (Fri Aug  3 14:25:48 2007):
	Adjusted merging criteria to account for mean r and theta for
	splines.


Release R1-01a (Thu Aug  2 16:28:48 2007):
	Added initial version of spline tracking and association and map
	interaction. Working acceptably with Bumblebees. Should try to
	tweak for Medium stereo.
	Adjusted display of color images (stereo blob has them in BGR
	format). Added a config file option instead of the hard coded ones.
	Added a default box to getRansacSplines when no lines are there.
	Detection working on mid-range camera with 160x120 IPM by
	increasing the vpPortion that we clip, but works better with
	320x240. Need to do the bilinear stuff for IPM.



Release R1-01 (Fri Jul 27 16:17:58 2007):
	Fixed a bug that used the display to crash. Added option to skip to
	specific frame #
	Added statistics display in cotk interface
	Added header file for LinePerceptor
	Added check for extending splines to compute the mean direction of
	the set of points and check the deviation from that

Release R1-00z (Wed Jul 25 13:35:49 2007):
	Added initializing state of ransac spline from previous results. 
	Adjusted localize points to work on outPoints.


Release R1-00y (Thu Jul 19 18:49:00 2007):
	Several changes: debug option, export option, step option to
	lineperceptor. Cleaned up line types. Grouping of bounding boxes
	before spline ransac. Implemented better localization and spline
	extension filter (using gaussian to detect the line peak instead of
	detecting the two edges).
	Adjusted extension near intersection with stoplines. Added
	horizontal filtering to lane detection.
	

Release R1-00x (Mon Jul 16 16:12:30 2007):
	Fixed memory leak. Added support for exporting whole stereo blobs.
	Added ability to skip in logs in replay mode.

Release R1-00w (Mon Jul 16 14:44:51 2007):
	Fixed memory leak. Added support for exporting whole stereo blobs


Release R1-00w (Mon Jul 16 15:20:52 2007):
		Fixed memory leak. Added support for exporting whole stereo blobs. Added ability to skip in logs in replay mode.

Release R1-00w (Mon Jul 16 14:44:51 2007):
	Fixed memory leak. Added support for exporting whole stereo blobs


Release R1-00v (Thu Jul 12 14:55:46 2007):
	Adjusted issues in field

Release R1-00u (Tue Jul 10 13:46:48 2007):
	Added support to read medium and long range stereo color images.

Release R1-00r (Fri Apr 27 10:28:24 2007):
		Line perceptor can now send stop lines and lanes to map

Release R1-00q (Thu Apr 26 14:47:18 2007):
	Added missing conf file.

Release R1-00p (Thu Apr 26 11:28:00 2007):
	Implemented Lane detection. Added RANSAC step after coarse line
	detection.

Release R1-00o (Sun Apr 15 14:56:31 2007):
  Modified the replay mode to support the current sensnet_replay API.
  The program arguments have also changed: if a log file is given
  on the command line, the program will automatically run in replay
  mode (--replay is deprecated).

Release R1-00n (Fri Mar 16 11:47:28 2007):
	Added tracking capability (preliminary) and several other
	enhancements

Release R1-00m (Mon Mar 12 23:56:26 2007):
  Fixed so that it compiles with the new StereoImageBlob, but it's not
  really tested.

Release R1-00l (Sun Mar 11 11:02:19 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00k (Wed Feb 21 21:48:58 2007):
	Updated to work with new skynet release

Release R1-00j (Thu Feb 15 10:21:40 2007):
	Adjusted make file.

Release R1-00i (Thu Feb  8 11:09:42 2007):
	Adjust the camera parameters (till we recalibrate) and enhanced debugging view.

Release R1-00h (Tue Feb  6 17:25:22 2007):
	Added log playback functionality.

Release R1-00g (Sun Feb  4 19:20:51 2007):
	updated small changes from field test.	updated to work with new
	cotk interface.

Release R1-00f (Sat Feb  3 23:14:50 2007):
	Updated interface to add view button for debugging.  Updated
	StopLinePerceptor.conf from Mohamed's field changes.

Release R1-00e (Sat Feb  3 16:41:06 2007):
	updated writeLine to clear the message struct after sending. 
	Messages are only sent when a new image frame is received.

Release R1-00d (Sat Feb  3 13:48:35 2007):
	Fixed bug in writeLines which cleared line message before sending. 
	Updated fake line function to actively send a single line.  We can
	now send faked data from LinePerceptor to Mapper.  

Release R1-00c (Sat Feb  3  7:58:48 2007):
	Builds and runs, but not tested yet.

Release R1-00b (Sat Feb  3  7:41:29 2007):
	Added some enhancements. Builds but doesn't run.

Release R1-00a (Wed Jan 31 23:41:27 2007):
	Initial version that builds and runs.

Release R1-00 (Tue Jan 30 12:41:56 2007):
	Created.






































