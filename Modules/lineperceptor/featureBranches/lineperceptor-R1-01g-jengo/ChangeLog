Mon Aug 27 12:45:31 2007	Mohamed Aly (malaa)

	* version R1-01g
	BUGS:  
	FILES: Lanes-Medium.conf.try(35650), LinePerceptor.cc(35653),
		LineTrack.cc(35650), LineTrack.hh(35650),
		LineTracker.cc(35650), StopLinePerceptor.cc(35650, 35653),
		todo(35650)
	Updated tracking to eliminate dragging lines

Thu Aug 23 10:27:29 2007	Mohamed Aly (malaa)

	* version R1-01f
	BUGS:  
	New files: Lanes-Medium.conf.oldcalib Lanes-Medium.conf.try
	FILES: Lanes-Medium.conf(35120), LinePerceptor.cc(35120),
		Makefile.yam(35121), StopLinePerceptor.cc(35120)
	Fixed a bug that was using deprecated function to send map elements

	FILES: StopLinePerceptor.cc(34366)
	Adjust spline fit to take ratio of distance instead of
	y-coordinate, and this should be more robust for more curved
	splines

Mon Aug 20 17:19:40 2007	Mohamed Aly (malaa)

	* version R1-01e
	BUGS:  
	FILES: InversePerspectiveMapping.cc(34240),
		Lanes-Medium.conf(34240), Lanes-Short.conf(34240),
		LineTrack.cc(34240), LineTrack.hh(34240),
		StopLinePerceptor.cc(34240), StopLinePerceptor.hh(34240),
		StopLinePerceptorOpt.c(34240),
		StopLinePerceptorOpt.ggo(34240),
		StopLinePerceptorOpt.h(34240),
		Stoplines-Medium.conf(34240), Stoplines-Short.conf(34240),
		todo(34240)
	Adjusted config files for short range to work in St Luke. Adjusted
	merging splines to take into account frameLastUpdated parameter.
	Added horizontal strips to work on instead of the whole image.
	Added a limit on the extension of splines (bounding box) in image
	plane.

	FILES: Lanes-Medium.conf(34289), LinePerceptor.cc(34289),
		StopLinePerceptor.cc(34289)
	Improved the spline fitting to take the step as a ratio of
	y-coordinate instead of uniform

	FILES: Lanes-Medium.conf(34293), StopLinePerceptor.cc(34293)
	Commit for test tomorrow

	FILES: StopLinePerceptor.cc(34171), StopLinePerceptor.hh(34171)
	Adjusted functions to be more modular by making GetLines() and
	PostprocessLines() functions

Thu Aug 16 23:36:35 2007	Mohamed Aly (malaa)

	* version R1-01d
	BUGS:  
	FILES: Lanes-Medium.conf(33453), LinePerceptor.cc(33453),
		StopLinePerceptor.cc(33453), StopLinePerceptor.hh(33453),
		Stoplines-Medium.conf(33453), cmdline.c(33453),
		cmdline.ggo(33453), cmdline.h(33453), todo(33453)
	Adjusted hough transform to get points above certain threshold when
	not computing local maxima. Also, added --conf-path option to
	LinePerceptor to read the conf files from instead of the
	environment variable DGC_CONFIG_PATH if this option is given

	FILES: Lanes-Medium.conf(34009), LinePerceptor.cc(34009),
		LineTrack.cc(34009), StopLinePerceptor.cc(34009),
		todo(34009)
	Commit for field test

	FILES: LinePerceptor.cc(33017), LinePerceptor.hh(33017),
		Makefile.yam(33017), todo(33017)
	Added process control interface. Need to test

	FILES: LinePerceptor.cc(34011), StopLinePerceptor.cc(34011)
	Working on the red channel instead of converting to RGB. This looks
	better for the yellow lines.

	FILES: LineTrack.cc(32930)
	Fixed sending map elements from medium range stereo, truncate
	points that are too close (4m) or too far(100m)

Fri Aug 10  7:53:14 2007	Mohamed Aly (malaa)

	* version R1-01c
	BUGS:  
	New files: Lanes-Short.conf Stoplines-Medium.conf
		Stoplines-Short.conf
	FILES: InversePerspectiveMapping.cc(32628),
		InversePerspectiveMapping.hh(32628),
		LanePerceptor.conf(32628), Lanes-Medium.conf(32628),
		LinePerceptor.cc(32628), LineTracker.cc(32628),
		Makefile.yam(32628), StopLinePerceptor.cc(32628),
		StopLinePerceptor.conf(32628), StopLinePerceptor.hh(32628),
		StopLinePerceptorOpt.c(32628),
		StopLinePerceptorOpt.ggo(32628),
		StopLinePerceptorOpt.h(32628), lineperceptor(32628),
		todo(32628)
	Added region of interest to IPM calculations to get rid of black
	areas in medium and long stereo images

	FILES: InversePerspectiveMapping.cc(32631),
		Lanes-Medium.conf(32631)
	Added clearing of points outside IPM after filtering, which very
	much cleans up the fitlered image, and working fine for Medium and
	Short range now

	FILES: InversePerspectiveMapping.cc(32884),
		InversePerspectiveMapping.hh(32884),
		Lanes-Medium.conf(32884), StopLinePerceptor.cc(32884),
		StopLinePerceptor.hh(32884), StopLinePerceptorOpt.c(32884),
		StopLinePerceptorOpt.ggo(32884),
		StopLinePerceptorOpt.h(32884), todo(32884)
	Added bilinear interpolation for IPM calculations

	FILES: LanePerceptor.conf(32557), Lanes-Medium.conf(32557),
		LinePerceptor.cc(32557), LineTrack.cc(32557),
		LineTrack.hh(32557), StopLinePerceptor.cc(32557),
		StopLinePerceptor.conf(32557), StopLinePerceptor.hh(32557),
		StopLinePerceptorOpt.c(32557),
		StopLinePerceptorOpt.ggo(32557),
		StopLinePerceptorOpt.h(32557), todo(32557)
	Fixed a bug and memory leak in mcvFitRansacSpline() that causes seg
	fault when not fitting properly

	FILES: LanePerceptor.conf(32559), Lanes-Medium.conf(32559),
		LinePerceptor.cc(32559), LineTrack.cc(32559),
		LineTrack.hh(32559), StopLinePerceptor.cc(32559),
		StopLinePerceptor.conf(32559), StopLinePerceptor.hh(32559),
		StopLinePerceptorOpt.c(32559),
		StopLinePerceptorOpt.ggo(32559),
		StopLinePerceptorOpt.h(32559)
	Added another overall stat to LinePerceptor. Adjusted tracking for
	medium range (still more to be done)

	FILES: LanePerceptor.conf(32757), Lanes-Medium.conf(32757),
		LinePerceptor.cc(32757), LinePerceptor.hh(32757),
		LineTrack.cc(32757), LineTrack.hh(32757),
		LineTracker.cc(32757), LineTracker.hh(32757),
		StopLinePerceptor.cc(32757), StopLinePerceptor.conf(32757),
		StopLinePerceptor.hh(32757), StopLinePerceptorOpt.c(32757),
		StopLinePerceptorOpt.ggo(32757),
		StopLinePerceptorOpt.h(32757), todo(32757)
	Stoplines and lanes working fine on both short and medium. No
	broken lines yet though.

	FILES: LinePerceptor.cc(32552), LineTrack.cc(32552),
		LineTrack.hh(32552), LineTracker.cc(32552),
		LineTracker.hh(32552), cmdline.c(32552),
		cmdline.ggo(32552), cmdline.h(32552), lineperceptor(32552),
		todo(32552)
	Added option to use either left or right camera from the stereo
	pair

	FILES: LinePerceptor.cc(32767), Makefile.yam(32767),
		cmdline.c(32767), cmdline.ggo(32767), cmdline.h(32767),
		lineperceptor(32767)
	Added stopline tracking and adjusted config files handling in
	LinePerceptor, it now picks the right conf file based on the sensor
	id

	FILES: LineTrack.cc(32920), StopLinePerceptor.cc(32920),
		todo(32920)
	Try to adjust medium range lanes in map, they seem wierd on the
	left side, possibly something has to do with disparity

	FILES: StopLinePerceptorOpt.ggo(32558)
	Added all options to conf files

Fri Aug  3 14:25:39 2007	Mohamed Aly (malaa)

	* version R1-01b
	BUGS:  
	FILES: LineTrack.cc(31766), LineTracker.cc(31766),
		StopLinePerceptor.cc(31766), StopLinePerceptor.hh(31766),
		lineperceptor(31766), todo(31766)
	Adjusted merging criteria to account for mean r and theta for
	splines.

Thu Aug  2 16:28:11 2007	Mohamed Aly (malaa)

	* version R1-01a
	BUGS:  
	New files: Lanes-Medium.conf LineTrack.cc LineTrack.hh
		LineTracker.cc LineTracker.hh lineperceptor
	FILES: InversePerspectiveMapping.hh(31523),
		LinePerceptor.cc(31523), LinePerceptor.hh(31523),
		Makefile.yam(31523), StopLinePerceptor.cc(31523),
		StopLinePerceptor.hh(31523), cmdline.c(31523),
		cmdline.ggo(31523), cmdline.h(31523), mcv.cc(31523),
		mcv.hh(31523)
	Added initial version of spline tracking and association and map
	interaction. Working acceptably with Bumblebees. Should try to
	tweak for Medium stereo.

	FILES: LinePerceptor.cc(30918), Makefile.yam(30918),
		StopLinePerceptor.cc(30918), cmdline.c(30918),
		cmdline.ggo(30918), cmdline.h(30918), todo(30918)
	Adjusted display of color images (stereo blob has them in BGR
	format). Added a config file option instead of the hard coded ones.
	Added a default box to getRansacSplines when no lines are there.

	FILES: LinePerceptor.cc(30922)
	Detection working on mid-range camera with 160x120 IPM by
	increasing the vpPortion that we clip, but works better with
	320x240. Need to do the bilinear stuff for IPM.

	FILES: LinePerceptor.cc(31558)
	Initial tracking

Fri Jul 27 16:17:46 2007	Mohamed Aly (malaa)

	* version R1-01
	BUGS:  
	New files: LinePerceptor.hh r3
	FILES: LinePerceptor.cc(30427), StopLinePerceptor.cc(30427),
		StopLinePerceptor.hh(30427), cmdline.c(30427),
		cmdline.ggo(30427), cmdline.h(30427), todo(30427)
	Fixed a bug that used the display to crash. Added option to skip to
	specific frame #

	FILES: LinePerceptor.cc(30473), StopLinePerceptor.cc(30473),
		todo(30473)
	Added statistics display in cotk interface

	FILES: LinePerceptor.cc(30498), todo(30498)
	Added header file for LinePerceptor

	FILES: StopLinePerceptor.cc(30497), StopLinePerceptor.hh(30497)
	Added check for extending splines to compute the mean direction of
	the set of points and check the deviation from that

	FILES: StopLinePerceptor.cc(30637), todo(30637)
	subtract mean from image before filtering

Wed Jul 25 13:35:36 2007	Mohamed Aly (malaa)

	* version R1-00z
	BUGS:  
	New files: r1 r2
	Deleted files: r
	FILES: LanePerceptor.conf(29876), LinePerceptor.cc(29876),
		StopLinePerceptor.cc(29876), StopLinePerceptor.hh(29876),
		todo(29876)
	Created a function for computing spline scores. Added extension and
	localization for ipm image.

	FILES: LanePerceptor.conf(30199), LinePerceptor.cc(30199),
		StopLinePerceptor.cc(30199), StopLinePerceptor.hh(30199),
		todo(30199)
	Added initializing state of ransac spline from previous results.
	Adjusted localize points to work on outPoints.

Thu Jul 19 18:48:35 2007	Mohamed Aly (malaa)

	* version R1-00y
	BUGS:  
	New files: r
	FILES: LanePerceptor.conf(29564), LinePerceptor.cc(29564),
		Makefile.yam(29564), StopLinePerceptor.cc(29564),
		StopLinePerceptor.hh(29564), cmdline.c(29564),
		cmdline.ggo(29564), cmdline.h(29564), mcv.cc(29564),
		mcv.hh(29564), mk(29564), todo(29564)
	Several changes: debug option, export option, step option to
	lineperceptor. Cleaned up line types. Grouping of bounding boxes
	before spline ransac. Implemented better localization and spline
	extension filter (using gaussian to detect the line peak instead of
	detecting the two edges).

	FILES: LanePerceptor.conf(29741), StopLinePerceptor.cc(29741),
		StopLinePerceptor.hh(29741), cmdline.c(29741),
		cmdline.ggo(29741), cmdline.h(29741), todo(29741)
	Adjusted extension near intersection with stoplines. Added
	horizontal filtering to lane detection.

Mon Jul 16 16:12:18 2007	Mohamed Aly (malaa)

	* version R1-00x
	BUGS:  
	FILES: InversePerspectiveMapping.cc(29176),
		LinePerceptor.cc(29176), todo(29176)
	Committing changes on lineperceptor-R1-00v-malaa branch merged into
	trunk for release.

	FILES: LinePerceptor.cc(29194)
	Fixed memory leak. Added support for exporting whole stereo blobs.
	Added ability to skip in logs in replay mode.

Mon Jul 16 14:44:44 2007	Mohamed Aly (malaa)

	* version R1-00w
	BUGS:  
	FILES: InversePerspectiveMapping.cc(29170),
		LinePerceptor.cc(29170), todo(29170)
	Fixed memory leak. Added support for exporting whole stereo blobs


Mon Jul 16 15:20:42 2007	Mohamed Aly (malaa)

	* version R1-00w
	BUGS:  
	FILES: InversePerspectiveMapping.cc(29180), LinePerceptor.cc(29179)
	Fixed memory leak. Added support for exporting whole stereo blobs.
	Added ability to skip in logs in replay mode.

Mon Jul 16 14:44:44 2007	Mohamed Aly (malaa)

	* version R1-00w
	BUGS:  
	FILES: InversePerspectiveMapping.cc(29170),
		LinePerceptor.cc(29170), todo(29170)
	Fixed memory leak. Added support for exporting whole stereo blobs


Thu Jul 12 14:55:35 2007	Mohamed Aly (malaa)

	* version R1-00v
	BUGS:  
	New files: mk
	FILES: LanePerceptor.conf(28876, 28878), LinePerceptor.cc(28876),
		StopLinePerceptor.cc(28876, 28878),
		StopLinePerceptor.conf(28876),
		StopLinePerceptorTest.cc(28876), todo(28876)
	Adjusted issues in field

Tue Jul 10 13:46:36 2007	Mohamed Aly (malaa)

	* version R1-00u
	BUGS:  
	FILES: LanePerceptor.conf(28695), LinePerceptor.cc(28695),
		StopLinePerceptor.cc(28695), StopLinePerceptor.hh(28695),
		todo(28695)
	Can now read color images as input, and converts them to grayscale

Fri Apr 27 10:28:15 2007	Mohamed Aly (malaa)

	* version R1-00r
	BUGS:  
	FILES: LanePerceptor.conf(21075), LinePerceptor.cc(21075),
		StopLinePerceptor.cc(21075)
	Line perceptor can now send stop lines and lanes to map

Thu Apr 26 14:47:16 2007	Mohamed Aly (malaa)

	* version R1-00q
	BUGS:  
	New files: LanePerceptor.conf
Thu Apr 26 11:27:05 2007	Mohamed Aly (malaa)

	* version R1-00p
	BUGS:  
	New files: todo
	FILES: CameraInfo.conf(20924), InversePerspectiveMapping.cc(20924),
		LinePerceptor.cc(20924), Makefile.yam(20924),
		StopLinePerceptor.cc(20924), StopLinePerceptor.conf(20924),
		StopLinePerceptor.hh(20924), StopLinePerceptorOpt.c(20924),
		StopLinePerceptorOpt.ggo(20924),
		StopLinePerceptorOpt.h(20924),
		StopLinePerceptorTest.cc(20924), cmdline.c(20924),
		cmdline.ggo(20924), cmdline.h(20924), mcv.cc(20924),
		test.sh(20924)
	Implemented Lane detection. Added RANSAC step after coarse line
	detection.

Sun Apr 15 14:56:23 2007	Andrew Howard (ahoward)

	* version R1-00o
	BUGS:  
	FILES: LinePerceptor.cc(19513), Makefile.yam(19513)
	Fixed for sensnet replay changes

	FILES: LinePerceptor.cc(19641), Makefile.yam(19641),
		cmdline.c(19641), cmdline.ggo(19641), cmdline.h(19641)
	Tweaked for new sensnet_replay API

Fri Mar 16 11:47:07 2007	Mohamed Aly (malaa)

	* version R1-00n
	BUGS: 
	FILES: CameraInfo.conf(17998), LinePerceptor.cc(17998),
		Makefile.yam(17998), StopLinePerceptor.cc(17998),
		StopLinePerceptor.conf(17998), StopLinePerceptor.hh(17998),
		StopLinePerceptorOpt.c(17998),
		StopLinePerceptorOpt.ggo(17998),
		StopLinePerceptorOpt.h(17998), cmdline.c(17998),
		cmdline.ggo(17998), cmdline.h(17998), ranker.h(17998),
		test.sh(17998)
	Added tracking capability (preliminary) and several other
	enhancements

Mon Mar 12 23:56:10 2007	 (ahoward)

	* version R1-00m
	BUGS: 
	FILES: LinePerceptor.cc(17402)
	Fixed for new stereo blob, but not really tested

Sun Mar 11 11:02:09 2007	Mohamed Aly (malaa)

	* version R1-00l
	BUGS: 
	New files: test.sh
	FILES: LinePerceptor.cc(17085), Makefile.yam(17085),
		StopLinePerceptor.cc(17085), StopLinePerceptor.hh(17085),
		cmdline.c(17085), cmdline.ggo(17085), cmdline.h(17085)
	Added functions to get the two endpoints of the line segment from
	the original image

Wed Feb 21 21:48:55 2007	Sam Pfister (sam)

	* version R1-00k
	BUGS: 
	FILES: LinePerceptor.cc(15366)
	Updated to work with new skynet release

Thu Feb 15 10:21:32 2007	Mohamed Aly (malaa)

	* version R1-00j
	BUGS: 
	FILES: LinePerceptor.cc(15062), Makefile.yam(15062),
		cmdline.c(15062), cmdline.ggo(15062), cmdline.h(15062)
	adjusted make file

Thu Feb  8 11:09:22 2007	Mohamed Aly (malaa)

	* version R1-00i
	BUGS: 
	New files: StopLinePerceptorTest.cc
	FILES: CameraInfo.conf(14779), InversePerspectiveMapping.cc(14779),
		InversePerspectiveMapping.hh(14779),
		LinePerceptor.cc(14779), Makefile.yam(14779),
		StopLinePerceptor.cc(14779), StopLinePerceptor.conf(14779),
		StopLinePerceptor.hh(14779), StopLinePerceptorOpt.c(14779),
		StopLinePerceptorOpt.ggo(14779),
		StopLinePerceptorOpt.h(14779), cmdline.c(14779),
		cmdline.ggo(14779), cmdline.h(14779)
	Adjust camera parameters and enhanced debugging and playback

Tue Feb  6 17:25:10 2007	Mohamed Aly (malaa)

	* version R1-00h
	BUGS: 
	New files: logReader.c
	FILES: LinePerceptor.cc(14697), Makefile.yam(14697),
		StopLinePerceptor.cc(14697), StopLinePerceptor.hh(14697),
		cmdline.c(14697), cmdline.ggo(14697), cmdline.h(14697)
	Added replay option to lineperceptor. Still need to test it

	FILES: LinePerceptor.cc(14708), StopLinePerceptor.conf(14708)
	Added playback functionality

Sun Feb  4 19:20:47 2007	Sam Pfister (sam)

	* version R1-00g
	BUGS: 
	FILES: LinePerceptor.cc(14566), StopLinePerceptor.conf(14566)
	updated small changes from field test.	updated to work with new
	cotk interface.

Sat Feb  3 23:14:46 2007	Sam Pfister (sam)

	* version R1-00f
	BUGS: 
	FILES: LinePerceptor.cc(14470), StopLinePerceptor.conf(14470)
	Updated interface to add view button for debugging.  Updated
	StopLinePerceptor.conf from Mohamed's field changes.

Sat Feb  3 16:41:03 2007	Sam Pfister (sam)

	* version R1-00e
	BUGS: 
	FILES: LinePerceptor.cc(14396)
	updated writeLine to clear the message struct after sending. 
	Messages are only sent when a new image frame is received.

Sat Feb  3 13:48:32 2007	Sam Pfister (sam)

	* version R1-00d
	BUGS: 
	FILES: LinePerceptor.cc(14382)
	fixed bug in writeLines which cleared line message before sending. 
	Updated fake line function to actively send a single line.

Sat Feb  3  7:58:43 2007	Mohamed Aly (malaa)

	* version R1-00c
	BUGS: 
	FILES: mcv.cc(14336), mcv.hh(14336, 14337)
	build and runs

Sat Feb  3  7:41:11 2007	Mohamed Aly (malaa)

	* version R1-00b
	BUGS: 
	FILES: CameraInfoOpt.c(14328), CameraInfoOpt.ggo(14328),
		CameraInfoOpt.h(14328), LinePerceptor.cc(14328),
		StopLinePerceptor.cc(14328), StopLinePerceptor.conf(14328),
		StopLinePerceptor.hh(14328), StopLinePerceptorOpt.c(14328),
		StopLinePerceptorOpt.ggo(14328),
		StopLinePerceptorOpt.h(14328), mcv.cc(14328), mcv.hh(14328)
	Added enhanced pre-filtering and Hough transform

Wed Jan 31 23:41:24 2007	Andrew Howard (ahoward)

	* version R1-00a
	BUGS: 
	New files: CameraInfo.conf CameraInfoOpt.c CameraInfoOpt.ggo
		CameraInfoOpt.h InversePerspectiveMapping.cc
		InversePerspectiveMapping.hh LinePerceptor.cc
		StopLinePerceptor.cc StopLinePerceptor.conf
		StopLinePerceptor.hh StopLinePerceptorOpt.c
		StopLinePerceptorOpt.ggo StopLinePerceptorOpt.h cmdline.c
		cmdline.ggo cmdline.h mcv.cc mcv.hh ranker.h
	FILES: Makefile.yam(13837)
	Working towards buildable

	FILES: Makefile.yam(13851)
	Buildable version

	FILES: Makefile.yam(13852)
	Runnable version

Tue Jan 30 12:41:56 2007	Andrew Howard (ahoward)

	* version R1-00
	Created lineperceptor module.
































