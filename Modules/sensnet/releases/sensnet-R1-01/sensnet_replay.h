
/* 
 * Desc: Module for replaying replayple log files.
 * Date: 30 March 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SENSNET_REPLAY_H
#define SENSNET_REPLAY_H

#ifdef __cplusplus
extern "C"
{
#endif

/** @file

@brief Read data from replayple log files using log timestamps.

**/


#include <stdbool.h>
#include <stdint.h>

  
/// @brief Replay-read context (opaque).
typedef struct sensnet_replay sensnet_replay_t;

/// @brief Create context and allocate resources.
sensnet_replay_t *sensnet_replay_alloc();

/// @brief Destroy context and free all resources.
int sensnet_replay_free(sensnet_replay_t *self);

/// @brief Open log files for replay.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_replay_open(sensnet_replay_t *self, int num_filenames, char **filenames);

// @brief Clean up after log replay.
///  
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_replay_close(sensnet_replay_t *self);

/// @brief Seek to a particular time in the logs.
///
/// @param[in] self Context.
/// @param[in] timestamp Seek timestamp (microseconds since epoch).
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_replay_seek(sensnet_replay_t *self, uint64_t timestamp);

/// @brief Read the next entry from the log (replay mode).
///
/// The step time specifies how far to advance the log, in
/// milliseconds.  To get the next message, irrespective of the
/// timestamp, set timeout to 0.
///
/// @param[in] self Context
/// @param[in] steptime Step time in milliseconds.
/// @returns Returns 0 on success.
int sensnet_replay_next(sensnet_replay_t *self, int steptime);

/// @brief Peek at the current data.
///
/// @param[in] self Context
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[out] sensor_id Sensor ID.
/// @param[out] blob_type Blob type.
/// @param[out] blob_id   Blob id (-1 if there is no data).
/// @param[out] blob_size Blob size.   
/// @returns Returns 0 on success.
int sensnet_replay_peek(sensnet_replay_t *self, uint64_t *timestamp,
                       int *sensor_id, int *blob_type, int *blob_id, int *blob_size);
  
/// @brief Read the current data.
///
/// @param[in] self Context
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[out] sensor_id Sensor ID.
/// @param[out] blob_type Blob type.
/// @param[out] blob_id Blob id.
/// @param[in]  blob_size Maximum blob size. 
/// @param[out] blob_data Blob data.
/// @returns Returns 0 on success.
int sensnet_replay_read(sensnet_replay_t *self, uint64_t *timestamp,
                        int *sensor_id, int *blob_type, int *blob_id,
                        int blob_size, void *blob_data);

  
#ifdef __cplusplus
}
#endif

#endif
