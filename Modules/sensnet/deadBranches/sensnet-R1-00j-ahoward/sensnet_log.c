
/* Desc: Low-level logging interface for sensnet messages
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include "sensnet_log.h"


// Current log version
#define SENSNET_LOG_VERSION 0x0011


// Logging context.  
struct _sensnet_log_t
{
  // Log directory
  char logname[1024];

  // Index file
  FILE *index_file;

  // Data file
  FILE *data_file;

  // Current file count and offset within file
  int fileno, offset;
};


// Index entry.  This is written directly to the index file, so all
// types must be size-defined.
typedef struct
{
  uint64_t timestamp;
  uint32_t sensor_id;
  uint32_t blob_type;
  uint32_t blob_id;
  uint32_t blob_len;
  uint32_t fileno, offset;

  uint32_t reserved[8];
  
} __attribute__((packed))  sensnet_log_index_t;


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Allocate object
sensnet_log_t *sensnet_log_alloc()
{
  sensnet_log_t *self;

  self = calloc(1, sizeof(sensnet_log_t));

  return self;
}


// Free object
void sensnet_log_free(sensnet_log_t *self)
{
  free(self);

  return;
}


// Open file for writing
int sensnet_log_open_write(sensnet_log_t *self, const char *logname, sensnet_log_header_t *header)
{
  char filename[1024];

  // Copy the name
  strncpy(self->logname, logname, sizeof(self->logname) - 1);
  
  // Create directory
  if (mkdir(self->logname, 0x1ED) != 0) 
    return ERROR("unable to create: %s %s", self->logname, strerror(errno));

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.bin", self->logname);  
  self->index_file = fopen(filename, "w");
  if (!self->index_file)
    return ERROR("unable to open index file: %s %s", filename, strerror(errno));

  // Overwrite version
  header->version = SENSNET_LOG_VERSION;

  // Write the header
  if (fwrite(header, sizeof(*header), 1, self->index_file) < 0)
    return ERROR("unable to write header");
      
  return 0;
}


// Open file for reading
int sensnet_log_open_read(sensnet_log_t *self, const char *logname, sensnet_log_header_t *header)
{
  char filename[1024];

  // Copy the name
  strncpy(self->logname, logname, sizeof(self->logname) - 1);

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.bin", self->logname);  
  self->index_file = fopen(filename, "r");
  if (!self->index_file)
    return ERROR("unable to open index file: %s %s", filename, strerror(errno));

  // Read the header
  if (fread(header, sizeof(*header), 1, self->index_file) < 0)
    return ERROR("unable to read header");
  
  // Check the version
  if (header->version != SENSNET_LOG_VERSION)
    return ERROR("version mismatch: log = %X code = %X", header->version, SENSNET_LOG_VERSION);
  

  return 0;
}


// Close the log
int sensnet_log_close(sensnet_log_t *self)
{
  if (self->data_file)
    fclose(self->data_file);
  if (self->index_file)
    fclose(self->index_file);
  
  return 0;
}

// check for eof
int sensnet_log_has_next(const sensnet_log_t* self)
{
  return !feof(self->index_file);
}


// Write blob to the log
int sensnet_log_write(sensnet_log_t *self, uint64_t timestamp,
                      int sensor_id, int blob_type, int blob_id,
                      int blob_len, const void *blob_data)
{
  char filename[1024];
  sensnet_log_index_t index;

  // See if there is room in the current data file.
  // If not, open a new one.
  if (self->offset + blob_len > 0x40000000)
  {
    self->fileno += 1;
    self->offset = 0;
    fclose(self->data_file);
    self->data_file = NULL;
  }

  // Create data file as needed
  if (!self->data_file)
  {
    snprintf(filename, sizeof(filename), "%s/data-%02d.bin", self->logname, self->fileno);  
    self->data_file = fopen(filename, "w");
    if (!self->data_file)
      return ERROR("unable to open data file: %s %s", filename, strerror(errno));
  }

  // Write blob to data file
  // TODO: use non-buffered IO
  if (fwrite(blob_data, blob_len, 1, self->data_file) < 1)
    return ERROR("unable to write data: %s", strerror(errno));

  // Construct entry for index file
  index.timestamp = timestamp;
  index.sensor_id = sensor_id;
  index.blob_type = blob_type;
  index.blob_id = blob_id;
  index.blob_len = blob_len;  
  index.fileno = self->fileno;
  index.offset = self->offset;

  // Write index 
  if (fwrite(&index, sizeof(index), 1, self->index_file) < 1)
    return ERROR("unable to write index: %s", strerror(errno));

  self->offset += blob_len;

  // Flush the data file
  fflush(self->index_file);
  fflush(self->data_file);
  fsync(fileno(self->data_file));

  return 0;
}


// Peek at the next entry in the log, but do not read the data.
int sensnet_log_peek(sensnet_log_t *self, uint64_t *timestamp,
                     int *sensor_id, int *blob_type, int *blob_id, int *blob_len)
{
  sensnet_log_index_t index;
    
  // Check for eof
  if (feof(self->index_file))
    return ERROR("end-of-file");

  // Read the index
  if (fread(&index, sizeof(index), 1, self->index_file) < 1)
    return ERROR("unable to read index: %s", strerror(errno));

  // Rewind the file
  fseek(self->index_file, -sizeof(index), SEEK_CUR);

  if (timestamp)
    *timestamp = index.timestamp;
  if (sensor_id)
    *sensor_id = index.sensor_id;
  if (blob_type)
    *blob_type = index.blob_type;
  if (blob_id)
    *blob_id = index.blob_id;
  if (blob_len)
    *blob_len = index.blob_len;  

  return 0;
}


// Read a blob from the log
int sensnet_log_read(sensnet_log_t *self, uint64_t *timestamp,
                     int *sensor_id, int *blob_type, int *blob_id,
                     int *blob_len, void *blob_data)
{
  char filename[1024];
  sensnet_log_index_t index;
    
  // Check for eof
  if (feof(self->index_file))
    return ERROR("end-of-file");

  // Read the index
  if (fread(&index, sizeof(index), 1, self->index_file) < 1)
    return ERROR("unable to read index: %s", strerror(errno));

  // Make sure the lengths match
  if (blob_len && index.blob_len > *blob_len)
    return ERROR("mismatched lengths: file has %d, call has %d", index.blob_len, *blob_len);
  
  // See if we are still using the current data file
  if (index.fileno != self->fileno)
  {
    fclose(self->data_file);
    self->data_file = NULL;
  }

  self->fileno = index.fileno;  
  self->offset = index.offset;
    
  // Open data file
  if (!self->data_file)
  {
    snprintf(filename, sizeof(filename), "%s/data-%02d.bin", self->logname, self->fileno);  
    self->data_file = fopen(filename, "r");
    if (!self->data_file)
      return ERROR("unable to open data file: %s %s", filename, strerror(errno));
  }

  // See to right place in data file
  if (fseek(self->data_file, self->offset, SEEK_SET) != 0)
    return ERROR("unable to seek to byte %d: %s", self->offset, strerror(errno));

  // Read the data blob
  if (blob_data)
  {
    if (fread(blob_data, index.blob_len, 1, self->data_file) < 1)
      return ERROR("unable to read data: %s", strerror(errno));
  }
  else
  {
    if (fseek(self->data_file, index.blob_len, SEEK_CUR) < 0)
      return ERROR("unable to seek data: %s", strerror(errno));
  }

  if (blob_len)
    *blob_len = index.blob_len;  
  if (timestamp)
    *timestamp = index.timestamp;
  if (sensor_id)
    *sensor_id = index.sensor_id;
  if (blob_type)
    *blob_type = index.blob_type;
  if (blob_id)
    *blob_id = index.blob_id;

  return 0;
}

