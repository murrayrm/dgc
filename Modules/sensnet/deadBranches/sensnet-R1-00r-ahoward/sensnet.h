
/* 
 * Desc: DGC sensnet library for accessing sensor data.
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SENSNET_H
#define SENSNET_H

#ifdef __cplusplus
extern "C"
{
#endif

/** @file

@brief SensNet: distributes high-bandwidth sensor data from feeders to
perceptors.

The SensNet library is used to transport high-bandwidth sensor data
from feeders to perceptors.  Conceptually, it is very similar to
Spread/Skynet, but with three key differences:

-# When the sender and reciever are on the same machine, messages are
used to notify perceptors that data is available, but do not carry the
data itself.  Instead, the notification messages carry an index into a
shared memory cache where the actual data is stored.  This approach
minimizes total bandwidth over the network and allows perceptors to
easily assemble unsynchronized data from multiple sensors (such as
cameras and lasers).

-# When the sender and receiver are on different machines, data is
transmitted over the network using Spread.  Large "blobs" are broken
into chunks of managable size, transmitted serially, and re-assembled
at the receiving side.

- # Sensor data is contained in binary large objects (blobs).  On the
receiving side, only the most recent blob is retained, so SensNet is
not suitable for applications requiring guaranteed message delivery.


@par Usage

SensNet is closely modelled on Spread/Skynet, so the basic steps are
very similar:

-# Create a SensNet context using ::sensnet_alloc.
-# Connect to a Spread daemon using ::sensnet_connect.
-# Join a particular sensor group or groups using ::sensnet_join.
-# (Feeders) Write sensor data (blobs) using ::sensnet_write.
-# (Perceptors) Wait new for sensor data using ::sensnet_wait.
-# (Perceptors) Read sensor data (blobs) using ::sensnet_read.
-# Tidy up with ::sensnet_disconnect and ::sensnet_free.

SensNet will also read/write Skynet messages; see ::sensnet_join and
::sensnet_write for details.

@par Limitations

This library is probably thread-safe, but has not been tested.

@sa

- @ref sensnet_log.h

**/


#include <stdbool.h>

  
/// Maximum value for skynet key
#define SENSNET_SKYNET_KEY_MAX 0x7FFF

/// Maximum value for module id
#define SENSNET_MODULE_ID_MAX 0xFF

/// Send data as a skynet message
#define SENSNET_METHOD_SKYNET 0x0001

/// Send data in chunks over network
#define SENSNET_METHOD_CHUNK 0x0002

/// Send data over shared memory
#define SENSNET_METHOD_SHMEM 0x0004

  
/// @brief Sensnet library context (opaque).
typedef struct sensnet sensnet_t;

/// @brief Create context and allocate resources.
sensnet_t *sensnet_alloc();

/// @brief Destroy context and free all resources.
int sensnet_free(sensnet_t *self);

/// @brief Initialize connection.
///
/// @param[in] self Context
/// @param[in] spread_daemon Name of spread daemon.
/// @param[in] skynet_key Unique key to distinguish multiple
///            concurrent users on the same network.
/// @param[in] module_id Unique ID for the calling module.
/// @returns Returns 0 on success.
int sensnet_connect(sensnet_t *self,
                    const char *spread_daemon, int skynet_key, int module_id);

/// @brief Terminate connection.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_disconnect(sensnet_t *self);

/// @brief Join a message group.
///
/// Call this function to join a message group; may be called multiple
/// times to join multiple groups.
///
/// To join a Skynet group, set sensor_id to SENSET_SKYNET_SENSOR and
/// blob_type to the Skynet message type.
///
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.  
/// @param[in] blob_type Blob type.
/// @param[in] max_size Maximum blob size (bytes).  
/// @returns Returns 0 on success.
int sensnet_join(sensnet_t *self, int sensor_id, int blob_type, int max_size);

/// @brief Leave a message group.
///  
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @returns Returns 0 on success.
int sensnet_leave(sensnet_t *self, int sensor_id, int blob_type);

/// @brief Write new a blob.
///
/// The method used to send blobs is specified by the method parameter
/// (a bit field).  For example, to send a Skynet message, use
/// SENSNET_METHOD_SKYNET. 
///  
/// @param[in] self Context
/// @param[in] method Bit-field specifying transport methods.
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @param[in] blob_id Unique but arbitrary blob id.
/// @param[in] blob_size Blob size (bytes).
/// @param[in] blob_data Blob data.
/// @returns Returns 0 on success.
int sensnet_write(sensnet_t *self, int method, int sensor_id, int blob_type,
                  int blob_id, int blob_size, const void *blob_data);

/// @brief Peek at latest blob from the cache (non-blocking).
///
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type  Blob type.
/// @param[out] blob_id   Blob id.
/// @param[out] blob_size Blob size.  
/// @returns Returns 0 on success.
int sensnet_peek(sensnet_t *self, int sensor_id, int blob_type, int *blob_id, int *blob_size);

/// @brief Read the latest blob from the cache (non-blocking).
///
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @param[out] blob_id Blob id.
/// @param[in]  Maximum blob size (bytes).
/// @param[out] blob_data Blob data buffer.
/// @returns Returns 0 on success.
int sensnet_read(sensnet_t *self,
                 int sensor_id, int blob_type, int *blob_id, int max_size, void *blob_data);
  
/// @brief Wait for new data to arrive (blocking).
///
/// Blocks until new sensor data is available for a least one of the
/// currently joined groups.
///  
/// @param[in] self Context
/// @param[in] timeout Timeout in milliseconds; a negative value means infinite timeout.
/// @returns Returns 0 on success or ETIMEDOUT on timeout.
int sensnet_wait(sensnet_t *self, int timeout);

/// @brief Get the spread server name
const char *sensnet_spread_daemon(sensnet_t *self);
  
/// @brief Get the spread key value
int sensnet_skynet_key(sensnet_t *self);

/// @brief Get the spread private name
// REMOVE const char *sensnet_private_name(sensnet_t *self);

  
#ifdef __cplusplus
}
#endif

#endif
