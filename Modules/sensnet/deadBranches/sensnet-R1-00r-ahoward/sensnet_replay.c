
/* 
 * Desc: Module for reading from replayple logs.
 * Date: 30 March 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

#include "sensnet_log.h"
#include "sensnet_replay.h"


// Replay context
struct sensnet_replay
{  
  // List of logs 
  int num_logs;
  sensnet_log_t *logs[32];

  // Current log timestamp
  uint64_t current_timestamp;

  // Current log
  sensnet_log_t *current_log;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Create context and allocate resources.
sensnet_replay_t *sensnet_replay_alloc()
{
  sensnet_replay_t *self;

  self = (sensnet_replay_t*) calloc(1, sizeof(sensnet_replay_t));
  
  return self;
}


// Destroy context and free all resources.
int sensnet_replay_free(sensnet_replay_t *self)
{
  free(self);
    
  return 0;
}


// Open log files for replay
int sensnet_replay_open(sensnet_replay_t *self, int num_filenames, char **filenames)
{
  int i;
  sensnet_log_t *log;
  sensnet_log_header_t header;

  if (num_filenames >= sizeof(self->logs) / sizeof(self->logs[0]))
    return ERROR("too many log files");
  
  // Open replayple log files
  for (i = 0; i < num_filenames; i++)
  {
    log = self->logs[self->num_logs++] = sensnet_log_alloc();
    if (sensnet_log_open_read(log, filenames[i], &header) != 0)
      return -1;
  }
  
  return 0;
}


// Clean up log replay
int sensnet_replay_close(sensnet_replay_t *self)
{
  int i;
    
  for (i = 0; i < self->num_logs; i++)
  {
    sensnet_log_close(self->logs[i]);
    sensnet_log_free(self->logs[i]);
    self->logs[i] = NULL;
  }
  self->num_logs = 0;
  
  return 0;
}


// Seek to a particular time in the logs.
int sensnet_replay_seek(sensnet_replay_t *self, uint64_t timestamp)
{
  int i;
    
  for (i = 0; i < self->num_logs; i++)
    sensnet_log_seek(self->logs[i], timestamp);

  return 0;
}


// Read the next entry from the log
int sensnet_replay_next(sensnet_replay_t *self, int steptime)
{
  int i;
  sensnet_log_t *log;
  uint64_t timestamp;
  int sensor_id, blob_type, blob_id, blob_len;
  sensnet_log_t *min_log;
  uint64_t min_timestamp;
  int min_sensor_id, min_blob_type, min_blob_id;
  
  if (self->num_logs <= 0)
    return 0;

  while (true)
  {
    // Scan through the logs to find the lowest timestamp
    min_timestamp = 0;
    min_sensor_id = -1;
    min_log = NULL;
    min_blob_id = -1;
    min_blob_type = -1;
    for (i = 0; i < self->num_logs; i++)
    {
      log = self->logs[i];
      if (sensnet_log_peek(log, &timestamp, &sensor_id, &blob_type, &blob_id, &blob_len) != 0)
        return -1;
      if (min_log == NULL || timestamp < min_timestamp)
      {
        min_timestamp = timestamp;
        min_sensor_id = sensor_id;
        min_blob_type = blob_type;
        min_blob_id = blob_id;
        min_log = log;
      }
    }
    if (min_log == NULL)
      return 0;

    //MSG("peek %d %d %d", min_sensor_id, min_blob_type, min_blob_id);

    // TODO?
    if (steptime <= 0)
      break;
    if (min_timestamp - self->current_timestamp > (uint64_t) steptime * 1000)
      break;

    // Read and discard the entry we are not interested in
    if (sensnet_log_read(min_log, NULL, NULL, NULL, NULL, 0, NULL) != 0)
      break;
  }

  // Record the current log
  self->current_log = min_log;
  self->current_timestamp = min_timestamp;

  return 0;
}


// Peek at the current blob
int sensnet_replay_peek(sensnet_replay_t *self, uint64_t *timestamp,
                       int *sensor_id, int *blob_type, int *blob_id, int *blob_size)
{
  if (!self->current_log)
    return -1;
  return sensnet_log_peek(self->current_log, timestamp, sensor_id,
                          blob_type, blob_id, blob_size);
}


// Read the current blob
int sensnet_replay_read(sensnet_replay_t *self, uint64_t *timestamp,
                       int *sensor_id, int *blob_type, int *blob_id, int blob_size,
                       void *blob_data)
{
  if (!self->current_log)
    return -1;
  return sensnet_log_read(self->current_log, timestamp, sensor_id,
                          blob_type, blob_id, blob_size, blob_data);  
}



