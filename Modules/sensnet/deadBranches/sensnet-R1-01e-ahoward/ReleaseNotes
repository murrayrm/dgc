              Release Notes for "sensnet" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "sensnet" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "sensnet" module can be found in
the ChangeLog file.

Release R1-01e-ahoward (Mon Jun 25 22:38:43 2007):
  Switched from SP_poll()/usleep() to poll() to provide low-latency response.

Release R1-01e (Mon Jun  4 23:04:43 2007):
  Added sensnet_peek_ex() function to get blob timestamps.

Release R1-01d (Fri May 18 23:52:18 2007):
  Minor internal change that makes a big improvement to total bandwidth
  usage on small blobs.

Release R1-01c (Thu Apr 26  0:11:51 2007):
  Modified handling of skynet group names to support the upcoming skynet
  release (which supports channels).

Release R1-01b (Sun Apr 15 22:40:29 2007):
  Fixed a minor bug with timeout on wait.

Release R1-01a (Sun Apr 15 14:49:00 2007):
  Tweaks to the sensnet_replay API.

Release R1-01 (Sat Apr 14 12:25:08 2007):
  Added support for shmem transport.  A number of the APIs have also 
  changed, including the log replay functionality, which has been moved
  from sensnet.h to sensnet_replay.h.

Release R1-00r (Wed Mar 21 11:36:16 2007):
  Fix for problem with large skynet keys (unable to open multiple sensnet
	  modules).  This is the second fix after the first got lost.

Release R1-00q (Tue Mar 20 16:15:59 2007):
  Re-release after bogus release from main trunk.

Release R1-00p (Tue Mar 20 16:06:11 2007):
  Fix for problem with large skynet keys (unable to open multiple sensnet
  modules).

Release R1-00o (Mon Mar 19 21:10:15 2007):
  Added support for high-speed logging with fast DMA writes; minor change to
  the sensnet logging API to enable this new functionality.

Release R1-00m (Sat Feb 24 21:30:41 2007):
	Implemented sensnet_log_seek().

Release R1-00l (Fri Feb 23 17:42:59 2007):
	Re-added some changes lost in the last release

Release R1-00k (Fri Feb 23 17:33:10 2007):
	Moved sensnet-replay to its own module.
	Fixed sensnet_log_has_next().

Release R1-00j (Fri Feb 23  0:14:22 2007):
  Lost some changes in the merge; put them back in.

Release R1-00i (Wed Feb 21 23:44:28 2007):
  Tweaked replay API with addition of a "step" function.  Modules using "wait"
  should use "step" instead when running in replay mode.

Release R1-00h (Mon Feb 19 21:58:20 2007):
	Added sensnet-replay, a tool to load multiple logs and replay
	them synchronized. It reads all the blobs from the specified
	logs and it sends them through sensnet.

Release R1-00g (Wed Feb  7  9:08:41 2007):
	This is mostly a test of Yam remote saving.

Release R1-00f (Mon Feb  5  8:55:07 2007):
	Sensnet comms API now has built-in support for recording
	and replaying log files (see new "record" and "replay" 
	initialization methods).  In replay mode, the "wait" function 
	will step through the log.

Release R1-00c (Mon Jan 22 15:28:03 2007):
	Fixed broken link in makefile

Release R1-00b (Mon Jan 22 15:02:42 2007):
	Added test code.

Release R1-00a (Sun Jan 21 23:33:01 2007):
	Added the raw library code.

Release R1-00 (Sun Jan 21 23:06:21 2007):
	Created.























