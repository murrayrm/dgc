
/* Desc: Simple test for SensNetLog library
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "sensnet_log.h"


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Test blob data
typedef struct
{
  int data[1024];
  
} __attribute__((packed)) test_blob_t;


// Get system time
uint64_t gettime()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (uint64_t) tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Write a dummy log.  The log is filled with random data.
int writeLog(const char *filename, int seed)
{
  sensnet_log_t *log;
  sensnet_log_header_t header;
  test_blob_t *blob;
  int i, j;
  bool direct;
  uint64_t time;
  
  MSG("writing %s", filename);
  
  srand(seed);
    
  log = sensnet_log_alloc();
  assert(log);

  header.timestamp = gettime();

  // Use direct mode?
#ifdef USE_COMPRESSION
  direct = 0;
#else
  direct = 1;
#endif
  
  if (sensnet_log_open_write(log, filename, &header, direct) != 0)
    return -1;
   
  if (direct)
    blob = valloc(sizeof(*blob));
  else
    blob = malloc(sizeof(*blob));

  // Record start time
  time = gettime();
  
  for (i = 0; i < 5000; i++)
  {
    // Generate random data
#ifdef USE_COMPRESSION
    // make the data compressible
    int val = rand();
    for (j = 0; j < sizeof(blob->data) / sizeof(blob->data[0]); j++)
    {
      blob->data[j] = val;
    }
#else
    for (j = 0; j < sizeof(blob->data) / sizeof(blob->data[0]); j++)
      blob->data[j] = rand();
#endif

    // Write data
#ifdef USE_COMPRESSION
    if (sensnet_log_write_ex(log, gettime(), 0, 0, i, sizeof(*blob),
                             SENSNET_FLAG_COMP, blob) != 0)
      return -1;
#else
    if (sensnet_log_write(log, gettime(), 0, 0, i, sizeof(*blob), blob) != 0)
      return -1;
#endif

    //MSG("wrote blob %d %d bytes", i, sizeof(*blob));
  }

  MSG("wrote %d bytes in %.3f sec %.3f Mb/sec",
      i * sizeof(*blob), (double) (gettime() - time) * 1e-6,
      i * sizeof(*blob) / ((double) (gettime() - time) * 1e-6) / 1e6);
  
  free(blob);
  
  sensnet_log_close(log);
  sensnet_log_free(log);
  
  return 0;
}


// Read a dummy log.  The log is filled with random data.
int readLog(const char *filename, int seed)
{
  sensnet_log_t *log;
  sensnet_log_header_t header;
  int blob_id;
  test_blob_t blob;
  int i, j;
  uint64_t time;
  
  MSG("reading %s", filename);
  
  srand(seed);
    
  log = sensnet_log_alloc();
  assert(log);

  header.timestamp = gettime();

  if (sensnet_log_open_read(log, filename, &header) != 0)
    return -1;

  // Record start time
  time = gettime();

  for (i = 0; i < 5000; i++)
  {
    // Read data
    if (sensnet_log_read(log, NULL, NULL, NULL, &blob_id, sizeof(blob), &blob) != 0)
      return -1;

    //MSG("read blob %d %d bytes", blob_id, blob_len);

    // Check random data
#ifdef USE_COMPRESSION
    {
      int val = rand();
      for (j = 0; j < sizeof(blob.data) / sizeof(blob.data[0]); j++)
      {
        if (blob.data[j] != val)
          return ERROR("test failed; read/write files differs");
      }
    }
#else
    for (j = 0; j < sizeof(blob.data) / sizeof(blob.data[0]); j++)
    {
      if (blob.data[j] != rand())
        return ERROR("test failed; read/write files differs");
    }
#endif
  }

  MSG("read  %d bytes in %.3f sec %.3f Mb/sec",
      i * sizeof(blob), (double) (gettime() - time) * 1e-6,
      i * sizeof(blob) / ((double) (gettime() - time) * 1e-6) / 1e6);

  sensnet_log_close(log);
  sensnet_log_free(log);
  
  return 0;
}



// Simple self-test.
int main(int argc, char **argv)
{
  char *filename;
 
  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  // Write out a random log
  if (writeLog(filename, 4567) != 0)
    return -1;

  // Read the log and make sure it is what we wrote
  if (readLog(filename, 4567) != 0)
    return -1;

  MSG("all tests passed");
  
  return 0;
}

