
/* Desc: Simple test for SensNet logging of ladar data
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memcpy, memset
#include <stdbool.h>

#include <sensnet/sensnet_log.h>
#include <frames/mat44.h>

// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)

uint8_t buffer[1 << 22]; // 4Mb, should be enough for everything

// read and write compressed
int main(int argc, char **argv)
{
  char *fnameIn;
  char *fnameOut;
  sensnet_log_t *logIn;
  sensnet_log_t *logOut;
  sensnet_log_header_t header;

  if (argc < 3)
  {
    printf("usage: %s <UNCOMPRESSED-LOG> <COMPRESSED-LOG>\n", argv[0]);
    return -1;
  }
  fnameIn = argv[1];
  fnameOut = argv[2];

  // Create logs
  logIn = sensnet_log_alloc();
  assert(logIn);
  logOut = sensnet_log_alloc();
  assert(logOut);

  // Open log
  MSG("opening input log: %s", fnameIn);
  if (sensnet_log_open_read(logIn, fnameIn, &header) != 0)
    return -1;

  MSG("opening output log: %s", fnameOut);
  if (sensnet_log_open_write(logOut, fnameOut, &header, false) != 0)
    return -1;

 while (sensnet_log_has_next(logIn))
  {
    uint64_t timestamp;
    int blob_type, blob_id, sensor_id, blob_len;

    if (sensnet_log_peek(logIn, &timestamp, &sensor_id, &blob_type, &blob_id, &blob_len)) {
        MSG("sensnet_log_peek() failed");
        break;
    }

#if 0
    if (blob_len > bufsize) {
        // resize buffer
        free(buffer);
        buffer = malloc(blob_len);
        if (!buffer)
            return ERROR("OUT OF MEMORY!!!!!!!!");
        bufsize = blob_len;
    }
#endif

    // Read blob
    if (sensnet_log_read(logIn, &timestamp, &sensor_id, &blob_type, &blob_id, blob_len, buffer) != 0)
      return -1;

    
    // write compressed
    if (sensnet_log_write_ex(logOut, timestamp, sensor_id, blob_type, blob_id,
                          blob_len, SENSNET_FLAG_COMP, buffer) != 0)
        return ERROR("Cannot write compressed blob ID %d of type %d, len %d", blob_id, blob_type, blob_len);

    fputc('.', stdout); fflush(stdout);
  }

  // Clean up
  sensnet_log_close(logIn);
  sensnet_log_free(logIn);
  sensnet_log_close(logOut);
  sensnet_log_free(logOut);
  
  return 0;
}

