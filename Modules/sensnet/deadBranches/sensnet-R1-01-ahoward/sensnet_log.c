
/* Desc: Low-level logging interface for sensnet messages
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

// GNU extensions
#ifndef O_DIRECT
#define O_DIRECT         040000 /* direct disk access hint */
#endif

#include "sensnet_log.h"


// Current log version
#define SENSNET_LOG_VERSION 0x0011


// Logging context.  
struct _sensnet_log_t
{
  // Log directory
  char logname[1024];

  // Use direct access mode?
  bool direct;
  
  // Index file
  FILE *index_file;

  // Data file descriptor
  int data_fd;

  // Current file count and offset within file
  int fileno, offset;
};


// Index entry.  This is written directly to the index file, so all
// types must be size-defined.
typedef struct
{
  uint64_t timestamp;
  uint32_t sensor_id;
  uint32_t blob_type;
  uint32_t blob_id;
  uint32_t blob_size;
  uint32_t fileno, offset;

  uint32_t reserved[8];
  
} __attribute__((packed))  sensnet_log_index_t;


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Allocate object
sensnet_log_t *sensnet_log_alloc()
{
  sensnet_log_t *self;

  self = calloc(1, sizeof(sensnet_log_t));

  return self;
}


// Free object
void sensnet_log_free(sensnet_log_t *self)
{
  free(self);

  return;
}


// Open file for writing
int sensnet_log_open_write(sensnet_log_t *self, const char *logname,
                           sensnet_log_header_t *header, bool direct)
{
  char filename[1024];

  // Copy the name
  strncpy(self->logname, logname, sizeof(self->logname) - 1);
  
  // Create directory
  if (mkdir(self->logname, 0x1ED) != 0) 
    return ERROR("unable to create: %s %s", self->logname, strerror(errno));

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.bin", self->logname);  
  self->index_file = fopen(filename, "w");
  if (!self->index_file)
    return ERROR("unable to open index file: %s %s", filename, strerror(errno));

  // Overwrite version
  header->version = SENSNET_LOG_VERSION;

  // Write the header
  if (fwrite(header, sizeof(*header), 1, self->index_file) < 0)
    return ERROR("unable to write header");

  // Use DMA write?
  self->direct = direct;
      
  return 0;
}


// Open file for reading
int sensnet_log_open_read(sensnet_log_t *self, const char *logname, sensnet_log_header_t *header)
{
  char filename[1024];

  // Copy the name
  strncpy(self->logname, logname, sizeof(self->logname) - 1);

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.bin", self->logname);  
  self->index_file = fopen(filename, "r");
  if (!self->index_file)
    return ERROR("unable to open index file: %s %s", filename, strerror(errno));

  // Read the header
  if (fread(header, sizeof(*header), 1, self->index_file) < 0)
    return ERROR("unable to read header");
  
  // Check the version
  if (header->version != SENSNET_LOG_VERSION)
    return ERROR("version mismatch: log = %X code = %X", header->version, SENSNET_LOG_VERSION);
  

  return 0;
}


// Close the log
int sensnet_log_close(sensnet_log_t *self)
{
  if (self->data_fd)
    close(self->data_fd);
  if (self->index_file)
    fclose(self->index_file);
  
  return 0;
}

// check for eof
int sensnet_log_has_next(const sensnet_log_t* self)
{
  // feof() won't return EOF if we didn't try to read beyond the end ...
  // so read a character and put it back
  int c = fgetc(self->index_file);
  ungetc(c, self->index_file);
  return c != EOF;
}


// Write blob to the log
int sensnet_log_write(sensnet_log_t *self, uint64_t timestamp,
                      int sensor_id, int blob_type, int blob_id,
                      int blob_size, const void *blob_data)
{
  char filename[1024];
  sensnet_log_index_t index;

  // See if there is room in the current data file.
  // If not, open a new one.
  if (self->offset + blob_size > 0x40000000)
  {
    self->fileno += 1;
    self->offset = 0;
    close(self->data_fd);
    self->data_fd = 0;
  }

  // Create data file as needed
  if (self->data_fd <= 0)
  {
    snprintf(filename, sizeof(filename), "%s/data-%02d.bin", self->logname, self->fileno);
    if (self->direct)
      self->data_fd = open(filename, O_CREAT | O_WRONLY | O_APPEND | O_DIRECT, 00666 );
    else
      self->data_fd = open(filename, O_CREAT | O_WRONLY | O_APPEND , 00666 );    
    if (self->data_fd < 0)
      return ERROR("unable to open data file: %s %s", filename, strerror(errno));
  }

  // Write blob to data file
  if (write(self->data_fd, blob_data, blob_size) < 0)
    return ERROR("unable to write data: %s", strerror(errno));

  // Construct entry for index file
  index.timestamp = timestamp;
  index.sensor_id = sensor_id;
  index.blob_type = blob_type;
  index.blob_id = blob_id;
  index.blob_size = blob_size;  
  index.fileno = self->fileno;
  index.offset = self->offset;

  // Write index 
  if (fwrite(&index, sizeof(index), 1, self->index_file) < 1)
    return ERROR("unable to write index: %s", strerror(errno));
  fflush(self->index_file);
  
  self->offset += blob_size;

  // If not in direct mode, call sync to flush the disc buffers
  if (!self->direct)
  {
    // REMOVE fflush(self->data_file);
    fsync(self->data_fd);
  }

  return 0;
}


// Peek at the next entry in the log, but do not read the data.
int sensnet_log_peek(sensnet_log_t *self, uint64_t *timestamp,
                     int *sensor_id, int *blob_type, int *blob_id, int *blob_size)
{
  sensnet_log_index_t index;
    
  // Check for eof
  if (feof(self->index_file))
    return ERROR("end-of-file");

  // Read the index
  if (fread(&index, sizeof(index), 1, self->index_file) < 1)
    return ERROR("unable to read index: %s", strerror(errno));

  // Rewind the file
  fseek(self->index_file, -sizeof(index), SEEK_CUR);

  if (timestamp)
    *timestamp = index.timestamp;
  if (sensor_id)
    *sensor_id = index.sensor_id;
  if (blob_type)
    *blob_type = index.blob_type;
  if (blob_id)
    *blob_id = index.blob_id;
  if (blob_size)
    *blob_size = index.blob_size;  

  return 0;
}


// Read a blob from the log
int sensnet_log_read(sensnet_log_t *self, uint64_t *timestamp,
                     int *sensor_id, int *blob_type, int *blob_id,
                     int blob_size, void *blob_data)
{
  char filename[1024];
  sensnet_log_index_t index;
    
  // Check for eof
  if (feof(self->index_file))
    return ERROR("end-of-file");

  // Read the index
  if (fread(&index, sizeof(index), 1, self->index_file) < 1)
    return ERROR("unable to read index: %s", strerror(errno));

  if (blob_size > 0 && blob_data != NULL)
  {
    // Make sure the lengths match
    if (index.blob_size > blob_size)
      return ERROR("mismatched lengths: file has %d, call has %d", index.blob_size, blob_size);
  
    // See if we are still using the current data file
    if (index.fileno != self->fileno)
    {
      close(self->data_fd);
      self->data_fd = 0;
    }

    self->fileno = index.fileno;  
    self->offset = index.offset;
    
    // Open data file
    if (self->data_fd <= 0)
    {
      snprintf(filename, sizeof(filename), "%s/data-%02d.bin", self->logname, self->fileno);

      // Don't open direct here.  Read cache can help performance
      self->data_fd = open(filename, O_RDONLY);
      if (self->data_fd < 0)
        return ERROR("unable to open data file: %s %s", filename, strerror(errno));
    }

    // See to right place in data file
    if (lseek(self->data_fd, self->offset, SEEK_SET) < 0)
      return ERROR("unable to seek to byte %d: %s", self->offset, strerror(errno));

    // Read the data blob
    assert(blob_data);
    if (read(self->data_fd, blob_data, index.blob_size) < 0)
      return ERROR("unable to read data: %s", strerror(errno));
  }

  if (timestamp)
    *timestamp = index.timestamp;
  if (sensor_id)
    *sensor_id = index.sensor_id;
  if (blob_type)
    *blob_type = index.blob_type;
  if (blob_id)
    *blob_id = index.blob_id;

  return 0;
}

#define SMALL_RANGE 10

// search the index to find the blob with time stamp == 'timestamp'.
// start_pos, end_pos: current search range, in number of records (NOT in bytes!!!),
// start_ts, end_ts: timestamp of first and last records.
// timestamp: the timestamp to seek to.
// NB: Ranges always include the start_* and exclude the end_* elements.
static int seek_binary_search(sensnet_log_t* self,
                              uint32_t start_pos, uint32_t end_pos,
                              uint64_t start_ts, uint64_t end_ts,
                              uint64_t timestamp)
{
  uint64_t curr_ts;
  uint32_t curr_pos;
  uint32_t offset;

  assert(start_pos < end_pos);
  assert(start_ts < end_ts);
  assert(start_ts <= timestamp && timestamp < end_ts);

  // optimization: if range is small (less than SMALL_RANGE records, search linearly)
  if (end_pos - start_pos <= SMALL_RANGE)
  {
    sensnet_log_index_t index;
    uint64_t curr_ts = start_ts;
    offset = start_pos * sizeof(sensnet_log_index_t) + sizeof(sensnet_log_header_t);
    if (fseek(self->index_file, offset, SEEK_SET) < 0)
      return ERROR("unable to seek index: %s", strerror(errno));

    while (curr_ts < timestamp)
    {
      // Read the next index entry
      if (fread(&index, sizeof(index), 1, self->index_file) < 1)
        return ERROR("unable to read index: %s", strerror(errno));
      curr_ts = index.timestamp;
    }
    
    // Rewind the file
    fseek(self->index_file, -sizeof(index), SEEK_CUR);
    return 0;
  }
  
  curr_pos = start_pos + (end_pos - start_pos) / 2;
  offset = curr_pos * sizeof(sensnet_log_index_t) + sizeof(sensnet_log_header_t);

  if (fseek(self->index_file, offset, SEEK_SET) < 0)
    return ERROR("unable to seek index: %s", strerror(errno));

  sensnet_log_peek(self, &curr_ts, NULL, NULL, NULL, NULL);

  assert(start_ts <= curr_ts && curr_ts < end_ts);

  if (curr_ts == timestamp) // lucky exact match?
    return 0;

  // search recursively
  if (timestamp < curr_ts)
  {
    return seek_binary_search(self, start_pos, curr_pos, start_ts, curr_ts,
                              timestamp);
  }
  else
  { // curr_ts > timestamp, cannot be equal
    return seek_binary_search(self, curr_pos+1, end_pos, curr_ts, end_ts,
                              timestamp);
  }
}


int sensnet_log_seek(sensnet_log_t *self, uint64_t timestamp)
{
  uint64_t start_ts;
  uint64_t end_ts;
  uint32_t num_rec;

  if (sensnet_log_has_next(self))
  {
    // Read the index
    uint64_t ts;
    if (sensnet_log_peek(self, &ts, NULL, NULL, NULL, NULL) != 0)
      return -1;
    
    if (ts == timestamp)
      return 0; // we're already there!
  }

  // get start and end timestamps, and number of records
  if (fseek(self->index_file, sizeof(sensnet_log_header_t), SEEK_SET) < 0)
      return ERROR("unable to seek index: %s", strerror(errno));
  if (sensnet_log_peek(self, &start_ts, NULL, NULL, NULL, NULL) != 0)
    return -1;

  if (fseek(self->index_file, 0L, SEEK_END) < 0)
      return ERROR("unable to seek index: %s", strerror(errno));
  num_rec = ftell(self->index_file) / sizeof(sensnet_log_index_t); // number of records

  if (fseek(self->index_file, -sizeof(sensnet_log_index_t), SEEK_END) < 0)
      return ERROR("unable to seek index: %s", strerror(errno));
  if (sensnet_log_peek(self, &end_ts, NULL, NULL, NULL, NULL) != 0)
    return -1;

  if (timestamp <= start_ts)
  {
    // seek to the beginning
    fseek(self->index_file, sizeof(sensnet_log_header_t), SEEK_SET);
    return 0;
  }
  else if (timestamp >= end_ts)
  {
    // seek to the end
    fseek(self->index_file, -sizeof(sensnet_log_index_t), SEEK_END);
    return 0;
  }

  // perform the search
  if (seek_binary_search(self, 0, num_rec, start_ts, end_ts, timestamp) != 0)
    return -1;

  return 0;
}
