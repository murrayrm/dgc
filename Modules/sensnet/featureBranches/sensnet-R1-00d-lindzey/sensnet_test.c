
/* 
 * Desc: Test program for DGC sensnet library
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "sensnet.h"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Test data
#define DUMMY_SENSNET_ID 1
#define DUMMY_BLOB_TYPE 1


// Test writing
int test_write(int argc, const char *argv[])
{
  sensnet_t *sensnet;
  int i;
  int blob_size, *blob;

  MSG("sensnet write test");
  
  blob_size = 500000;
  blob = malloc(blob_size);

  sensnet = sensnet_alloc();
  if (sensnet_connect(sensnet, "4803", 1234, 0) < 0)
    return -1;
  
  for (i = 0; i < 1000; i++)
  {
    blob[0] = i * 10;
    MSG("sending %d %d", i, blob[0]);
    if (sensnet_write(sensnet, DUMMY_SENSNET_ID,
		      DUMMY_BLOB_TYPE, i, blob_size, blob) != 0)
      break;
    usleep(100000);
  }
  
  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  
  free(blob);

  MSG("exiting cleanly");
  
  return 0;
}


// Test wait and cache read
int test_read(int argc, const char *argv[])
{
  sensnet_t *sensnet;
  int i, id, len;
  int sensnet_id;
  int blob_type;
  int blob_size, *blob;

  MSG("senset read test");
    
  blob_size = 700000;
  blob = malloc(blob_size);

  sensnet = sensnet_alloc();
  if (sensnet_connect(sensnet, "4803", 1234, 1) < 0)
    return -1;

  sensnet_id = DUMMY_SENSNET_ID;
  blob_type = DUMMY_BLOB_TYPE;
  
  if (sensnet_join(sensnet, sensnet_id, blob_type, blob_size, 5) < 0)
    return -1;
    
  MSG("waiting");
      
  for (i = 0; i < 2000; i++)
  {
    if (sensnet_wait(sensnet) != 0)
      break;
    if (sensnet_peek(sensnet, sensnet_id, blob_type, &id, &len) != 0)
      break;
    assert(len <= blob_size);
    if (sensnet_read(sensnet, sensnet_id, blob_type, id, len, blob) != 0)
      break;
    MSG("recieved %d %d", id, blob[0]);
  }

  if (sensnet_leave(sensnet, sensnet_id, blob_type) < 0)
    return -1;

  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  
  free(blob);

  MSG("exiting cleanly");
    
  return 0;
}


int main(int argc, const char *argv[])
{

  assert(argc >= 1);
  if (argc < 2)
    return ERROR("usage: %s [r|w]", argv[0]);

  if (argv[1][0] == 'w')
    test_write(argc, argv);
  else if (argv[1][0] == 'r')
    test_read(argc, argv);
  
  return 0;
}


