
/* 
 * Desc: DGC sensnet library for accessing sensor data.
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/poll.h>

#include <sp.h>

#include "sensnet.h"
#include "sensnet_log.h"


// Reserved sensor id for Skynet messages.
// This must match the enumerated sensor id value.
#define SENSNET_SKYNET_SENSOR 1

// Possible packet types for sending over Spread.  Skynet packets are
// simply the raw message; chunk packets contain the message data
// broken into manageable chunks; shmem packets are notifications that
// the message has been copied to a shared memory buffer.
#define SENSNET_TYPE_SKYNET 0x0000
#define SENSNET_TYPE_CHUNK  0x7000
#define SENSNET_TYPE_SHMEM  0x7800

// Offset for sensnet shared memory keys
#define SENSNET_SHMEM_KEY 0x10000000
#define SENSNET_SHMEM_MAX_SEGMENTS 16

// Maximum number of slots in cache
#define SENSNET_MAX_SLOTS 32


// Data packet for Spread comms.
typedef struct
{
  // Data source
  int sensor_id;

  // Data type
  int blob_type, blob_id, blob_size;

  // IPC shared memory info
  int shmem_key, shmem_size, shmem_count;
  
  // Chunks of blob data (SENSNET_TYPE_CHUNK)
  int chunk_offset, chunk_size;
  unsigned char chunk_data[0x10000];

} packet_t;


// Format for data in a shared mem buffer
typedef struct
{
  int shmem_count, blob_id, blob_size;
  unsigned char blob_data;

} shmem_buffer_t;


// Cache slot for reading
typedef struct
{
  // Sensor ID and blob type (constant) 
  int sensor_id, blob_type;

  // Maximum blob length (constant)
  int max_size;
    
  // Reference count
  int num_joins;

  // Time we last recieved data
  uint64_t recv_timestamp;
  
  // Current blob data
  int blob_id, blob_size;
  unsigned char *blob_data;
  
} slot_t;



// Sensnet library context.
struct sensnet
{  
  // Server name
  char *spread_daemon;
  
  // Spread user key
  int skynet_key;

  // Private name
  char private_name[MAX_PRIVATE_NAME];
  
  // Workspace for composing group strings
  char group[MAX_GROUP_NAME];

  // Spread mail box
  int mbox;  

  // Offset for shmem keys
  int shmem_key;
  
  // Rolling counter for shared mem keys
  int shmem_count;  
  
  // Read cache
  int num_slots;
  slot_t slots[SENSNET_MAX_SLOTS];

  // Thread control
  bool quit;
  pthread_t thread;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  pthread_mutex_t cond_mutex;
};


// Library thread: read message from Spread and populate the fifos
int sensnet_main(sensnet_t *self);


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Useful timestamp (matches DGC timestamp convention)
uint64_t gettime()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Create context and allocate resources.
sensnet_t *sensnet_alloc()
{
  sensnet_t *self;

  self = (sensnet_t*) calloc(1, sizeof(sensnet_t));
  
  return self;
}


// Destroy context and free all resources.
int sensnet_free(sensnet_t *self)
{
  int i;
  slot_t *slot;
  
  for (i = 0; i < self->num_slots; i++)
  {
    slot = self->slots + i;
    free(slot->blob_data);
  }  
  if (self->spread_daemon)
    free(self->spread_daemon);
  free(self);
    
  return 0;
}


// Initialize connection.
int sensnet_connect(sensnet_t *self,
                    const char *spread_daemon, int skynet_key, int module_id)
{
  int status;
	char private_group[MAX_GROUP_NAME];

  assert(spread_daemon);
  self->spread_daemon = strdup(spread_daemon);

  assert(skynet_key >= 0 && skynet_key <= SENSNET_SKYNET_KEY_MAX);
  assert(module_id >= 0 && module_id <= SENSNET_MODULE_ID_MAX);
  self->skynet_key = skynet_key;

  // Construct a private name for spread
  snprintf(self->private_name, sizeof(self->private_name), "m:%X:%X", skynet_key, module_id);

  // Construct a key offset for shared mem segments
  self->shmem_key = SENSNET_SHMEM_KEY;
  self->shmem_key |= skynet_key << 16;
  self->shmem_key |= module_id << 8;  

  MSG("connecting %s %d %s %X", spread_daemon, skynet_key, self->private_name, self->shmem_key);
    
  // Create connection
  status = SP_connect(spread_daemon, self->private_name, 0, 0, &self->mbox, private_group);
  if (status != ACCEPT_SESSION)
    return ERROR("spread connect failed: %d", status);
  
  // Start the reading thread
  if (pthread_mutex_init(&self->mutex, NULL) != 0)
    return ERROR("unable to create mutex: %s", strerror(errno));
  if (pthread_cond_init(&self->cond, NULL) != 0)
    return ERROR("unable to create condition: %s", strerror(errno));
  if (pthread_mutex_init(&self->cond_mutex, NULL) != 0)
    return ERROR("unable to create mutex: %s", strerror(errno));
  if (pthread_create(&self->thread, NULL, (void*(*)(void*)) sensnet_main, self) != 0)
    return ERROR("unable to create thread: %s", strerror(errno));

  return 0;
}


// Terminate connection.
int sensnet_disconnect(sensnet_t *self)
{
  int i;
  
  self->quit = true;

  // Finalize threads
  if (pthread_join(self->thread, NULL) != 0)
    return ERROR("unable to join thread: %s", strerror(errno));  
  if (pthread_mutex_destroy(&self->mutex) != 0)
    return ERROR("unable to destroy mutex: %s", strerror(errno));
  if (pthread_cond_destroy(&self->cond) != 0)
    return ERROR("unable to destroy condition: %s", strerror(errno));
  if (pthread_mutex_destroy(&self->cond_mutex) != 0) 
    return ERROR("unable to destroy mutex: %s", strerror(errno));

  // Clean up spread
  SP_disconnect(self->mbox);
  
  // Tidy up shmem segments
  for (i = 0; i < SENSNET_SHMEM_MAX_SEGMENTS && i < self->shmem_count; i++)
  {
    int shmem_key, shmem_id;
    shmem_key = self->shmem_key + i;
    shmem_id = shmget(shmem_key, 0, S_IRWXU);
    if (shmem_id < 0)
      ERROR("failed to get shared mem %X [%s]", shmem_key, strerror(errno));      
    else if (shmctl(shmem_id, IPC_RMID, 0) < 0)
      ERROR("failed to remove shared mem %X [%s]", shmem_key, strerror(errno));
  }

  return 0;
}


// Get the spread server name
const char *sensnet_spread_daemon(sensnet_t *self)
{
  return self->spread_daemon;
}


// Get the spread key value
int sensnet_skynet_key(sensnet_t *self)
{
  return self->skynet_key;
}


// Get the spread module name
const char *sensnet_private_name(sensnet_t *self)
{
  return self->private_name;
}


// Create a group string for the given message type.
static
char *make_group(sensnet_t *self, int sensor_id, int blob_type)
{
  if (sensor_id == SENSNET_SKYNET_SENSOR)
  {
    // TODO use the sensor_id for skynet messages.
    // Construct group name for skynet messages.
    // The format is sn:KKKKK:TTTTT:CCCCC
    snprintf(self->group, sizeof(self->group),
             "sn:%05d:%05d:%05d", self->skynet_key, blob_type, 0);
  }
  else
  {
    // Construct group name for sensnet messages.
    // The format is se:KKKKK:TTTTT:CCCCC
    snprintf(self->group, sizeof(self->group),
             "se:%05d:%05d:%05d", self->skynet_key, blob_type, sensor_id);    
  }
  return self->group;
}


// Find a read slot 
static
slot_t *find_slot(sensnet_t *self, int sensor_id, int blob_type)
{
  int i;
  slot_t *slot;
  for (i = 0; i < self->num_slots; i++)
  {
    slot = self->slots + i;
    if (slot->sensor_id == sensor_id && slot->blob_type == blob_type)
      return slot;
  }  
  return NULL;
}


// Create a read slot
static
slot_t *add_slot(sensnet_t *self, int sensor_id, int blob_type, int max_size)
{
  slot_t *slot;
  assert(self->num_slots < sizeof(self->slots)/sizeof(self->slots[0]));
  slot = self->slots + self->num_slots++;
  memset(slot, 0, sizeof(*slot));  
  slot->sensor_id = sensor_id;
  slot->blob_type = blob_type;
  slot->blob_id = -1;
  slot->max_size = max_size;
  slot->blob_data = malloc(max_size);
  return slot;
}


// Remove a read slot
static
void del_slot(sensnet_t *self, int sensor_id, int blob_type)
{
  slot_t *slot;
  slot = find_slot(self, sensor_id, blob_type);
  assert(slot);
  free(slot->blob_data);
  self->num_slots--;
  memmove(slot, slot + 1, (self->num_slots + slot - self->slots) * sizeof(*slot));
  return;
}


// Encode and send a raw skynet message
static
int encode_skynet_packet(sensnet_t *self, int sensor_id, int blob_type,
                         int blob_id, int blob_size, const void *blob_data)
{
  int status;
  const char *group;

  // Compute the group string
  group = make_group(self, sensor_id, blob_type);

  // Send message over spread, formatted as a skynet message
  status = SP_multicast(self->mbox, FIFO_MESS | SELF_DISCARD,
                        group, SENSNET_TYPE_SKYNET + (int16) blob_type, blob_size, blob_data);
  if (status < 0)
    return ERROR("spread send failed: %d", status);

  return 0;
}


// Decode a packet containing a skynet message
static
int decode_skynet_packet(sensnet_t *self, int sensor_id, int blob_type,
                         int mess_len, const packet_t *packet)
{
  slot_t *slot;
  
  // Get the slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  assert(slot);
  
  // Copy the data
  slot->recv_timestamp = gettime();
  slot->blob_id += 1;
  slot->blob_size = mess_len;
  assert(mess_len <= slot->max_size);
  memcpy(slot->blob_data, (void*) packet, mess_len);

  return 0;
}


// Encode and send chunk packets containing an entire blob
static
int encode_chunk_packets(sensnet_t *self, int sensor_id, int blob_type,
                         int blob_id, int blob_size, const void *blob_data)
{
  int status;
  const char *group;
  packet_t packet;
  int i, num_chunks, chunk_size, chunk_offset, packet_size;

  // Compute the group string
  group = make_group(self, sensor_id, blob_type);

  // Compose packet
  packet.sensor_id = sensor_id;
  packet.blob_type = blob_type;
  packet.blob_id = blob_id;
  packet.blob_size = blob_size;

  // See how many chunks we need
  chunk_size = sizeof(packet.chunk_data);
  num_chunks = blob_size / chunk_size;
  if (chunk_size * num_chunks < blob_size)
    num_chunks += 1;

  // Break blob into chunks and send them individually.  We use FIFO
  // mode to ensure they arrive in order.
  chunk_offset = 0;  
  for (i = 0; i < num_chunks; i++)
  {
    if (chunk_offset + chunk_size > blob_size)
      chunk_size = blob_size - chunk_offset;
    packet.chunk_offset = chunk_offset;
    packet.chunk_size = chunk_size;
    memcpy(packet.chunk_data, (char*) blob_data + chunk_offset, chunk_size);
    chunk_offset += chunk_size;

    packet_size = sizeof(packet) - sizeof(packet.chunk_data) + chunk_size;

    // Send off each chunk as a spread message
    status = SP_multicast(self->mbox, FIFO_MESS | SELF_DISCARD,
                          group, SENSNET_TYPE_CHUNK, packet_size, (char*) &packet);
    if (status < 0)
      return ERROR("spread send failed: %d", status);
  }

  return 0;
}


// Decode a packet containing a blob chunk
static
int decode_chunk_packet(sensnet_t *self, const packet_t *packet)
{
  slot_t *slot;

  // Get the slot for this blob type
  slot = find_slot(self, packet->sensor_id, packet->blob_type);
  assert(slot);
      
  // If this is the first chunk...
  if (packet->chunk_offset == 0)
  {
    slot->recv_timestamp = gettime();
    slot->blob_id = packet->blob_id;
    slot->blob_size = packet->blob_size;
  }

  // Copy the data
  assert(slot->max_size >= packet->blob_size);
  assert(slot->max_size >= packet->chunk_offset + packet->chunk_size);
  memcpy(slot->blob_data + packet->chunk_offset, packet->chunk_data, packet->chunk_size);

  // Return EAGAIN to if we are not done with this blob yet.
  if (packet->chunk_offset + packet->chunk_size < packet->blob_size)
    return EAGAIN;

  return 0;
}


// Encode and send a packet containing a shared mem blob
static
int encode_shmem_packet(sensnet_t *self, int sensor_id, int blob_type,
                        int blob_id, int blob_size, const void *blob_data)
{
  int status;
  int shmem_key, shmem_id, shmem_size;
  shmem_buffer_t *buffer;
  const char *group;
  packet_t packet;

  // Compute buffer size that is large enough for blob plus meta-data
  shmem_size = sizeof(packet) + blob_size;

  // Construct a key for this shared mem segment using the sensor id
  // and a rolling counter.
  shmem_key = self->shmem_key + (self->shmem_count++ % SENSNET_SHMEM_MAX_SEGMENTS);

  //MSG("creating shmem key %X", shmem_key);

  // Create shared memory segment.  If the segment already exists,
  // assume it is old and remove it.
  shmem_id = shmget(shmem_key, shmem_size, IPC_CREAT | S_IRWXU);
  if (shmem_id < 0 && errno == EINVAL)
  {
    MSG("WARNING found an existing shmem segment; removing it now"); // REMOVE
    shmem_id = shmget(shmem_key, 0, S_IRWXU);
    if (shmem_id < 0)
      return ERROR("failed to allocate shared mem [%s]", strerror(errno));      
    if (shmctl(shmem_id, IPC_RMID, 0) < 0)
      return ERROR("failed to remove shared mem [%s]", strerror(errno));
    shmem_id = shmget(shmem_key, shmem_size, IPC_CREAT | S_IRWXU);
  }
  if (shmem_id < 0)
    return ERROR("failed to allocate shared mem [%s]", strerror(errno));      
  
  // Attach pointer to segment and copy the data
  buffer = shmat(shmem_id, NULL, 0);
  assert(buffer);
  buffer->shmem_count = self->shmem_count;
  buffer->blob_id = blob_id;
  buffer->blob_size = blob_size;
  memcpy(&buffer->blob_data, blob_data, blob_size);
  shmdt(buffer);

  // Compute the group string
  group = make_group(self, sensor_id, blob_type);
  
  // Compose packet
  memset(&packet, 0, sizeof(packet));
  packet.sensor_id = sensor_id;
  packet.blob_type = blob_type;
  packet.blob_id = blob_id;
  packet.blob_size = blob_size;
  packet.shmem_key = shmem_key;
  packet.shmem_size = shmem_size;
  packet.shmem_count = self->shmem_count;

  // Send packet using spread; the data remains in the shared mem
  // to be copied by the client
  status = SP_multicast(self->mbox, FIFO_MESS | SELF_DISCARD,
                        group, SENSNET_TYPE_SHMEM, sizeof(packet), (char*) &packet);
  if (status < 0)
    return ERROR("spread send failed: %d", status);

  return 0;
}


// Decode a packet containing a shared mem blob
static
int decode_shmem_packet(sensnet_t *self, const packet_t *packet)
{
  int shmem_id;
  shmem_buffer_t *buffer;
  slot_t *slot;

  // Get the slot for this blob type
  slot = find_slot(self, packet->sensor_id, packet->blob_type);
  assert(slot);

  // Initalize shared memory for reading
  shmem_id = shmget(packet->shmem_key, packet->shmem_size, S_IRWXU);
  if (shmem_id < 0)
    return ERROR("failed to get shared mem [%s]", strerror(errno));
  
  // Attach memory and copy data
  buffer = shmat(shmem_id, NULL, SHM_RDONLY);
  if (!buffer)
    return ERROR("failed to attach shared mem [%s]", strerror(errno));

  //MSG("reading %X %d %d %d %d", packet->shmem_key,
  //    packet->shmem_count, buffer->shmem_count, packet->blob_id, buffer->blob_id);

  // See if the blob in shared memory matches the on described in the
  // packet.  This provides an imperfect syncronization method, in that it
  // still possible to read a corrupted blob if the sender is writing
  // fast enough and the number of segments in use is small.
  if (buffer->shmem_count != packet->shmem_count)
  {
    ERROR("missing blob in shared mem; expecting %d, got %d",
          packet->shmem_count, buffer->shmem_count);
    shmdt(buffer);
    return EAGAIN;
  }
  
  // Check that everything looks sane
  assert(buffer->blob_id == packet->blob_id);
  assert(buffer->blob_size == packet->blob_size);
  
  // Copy data from shared mem to slot
  slot->recv_timestamp = gettime();
  slot->blob_id = buffer->blob_id;
  slot->blob_size = buffer->blob_size;
  assert(slot->max_size >= buffer->blob_size);
  memcpy(slot->blob_data, &buffer->blob_data, buffer->blob_size);
  shmdt(buffer);
  
  return 0;
}


// Join a message group.
int sensnet_join(sensnet_t *self, int sensor_id, int blob_type, int max_size)
{
  int status;
  slot_t *slot;
  
  pthread_mutex_lock(&self->mutex);

  // Find read slot for this message type
  slot = find_slot(self, sensor_id, blob_type);

  // If the read slot does not exist, create it now
  if (!slot)
  {
    slot = add_slot(self, sensor_id, blob_type, max_size);
    assert(slot);                    
  }

  // If the spread group is not joined, do it now
  if (slot->num_joins == 0)
  {
    MSG("joining %s %d bytes", make_group(self, sensor_id, blob_type), max_size);
    status = SP_join(self->mbox, make_group(self, sensor_id, blob_type));
    if (status != 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return ERROR("spread join failed: %d", status);
    }
  }

  // Keep track of how many times we have joined
  slot->num_joins++;

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Leave a particular message group.
int sensnet_leave(sensnet_t *self, int sensor_id, int blob_type)
{
  int status;
  slot_t *slot;

  pthread_mutex_lock(&self->mutex);
    
  // Find slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown sensor/blob type");
  }

  // Keep track of number of joins
  slot->num_joins--;

  // If we are no longer joined, leave the spread group
  if (slot->num_joins == 0)
  {
    MSG("leaving %s", make_group(self, sensor_id, blob_type));
    status = SP_leave(self->mbox, make_group(self, sensor_id, blob_type));
    if (status != 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return ERROR("spread leave failed: %d", status);
    }
  }

  // If the slot is no longer used, remove it from the list
  if (slot->num_joins == 0)
    del_slot(self, sensor_id, blob_type);

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Write new sensor data.
int sensnet_write(sensnet_t *self, int method, int sensor_id, int blob_type,
                  int blob_id, int blob_size, const void *blob_data)
{
  int status;

  pthread_mutex_lock(&self->mutex);

  if (sensor_id == SENSNET_SKYNET_SENSOR || (method & SENSNET_METHOD_SKYNET))
  {
    // Encode and send raw skynet message
    status = encode_skynet_packet(self, sensor_id, blob_type, blob_id, blob_size, blob_data);
    if (status < 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return -1;
    }
  }
  if (method & SENSNET_METHOD_CHUNK)
  {
    // Encode and send blob as a set of chunk packets
    status = encode_chunk_packets(self, sensor_id, blob_type, blob_id, blob_size, blob_data);
    if (status < 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return -1;
    }
  }
  if (method & SENSNET_METHOD_SHMEM)
  {
    // Encode and send blob over shared mem
    status = encode_shmem_packet(self, sensor_id, blob_type, blob_id, blob_size, blob_data);
    if (status < 0)
    {
      pthread_mutex_unlock(&self->mutex);
      return -1;
    }    
  }

  pthread_mutex_unlock(&self->mutex);
    
  return 0;
}


// Read data from the cache.
int sensnet_read(sensnet_t *self,
                 int sensor_id, int blob_type, int *blob_id, int max_size, void *blob_data)
{
  slot_t *slot;

  pthread_mutex_lock(&self->mutex);
    
  // Get the slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown sensor id or blob type");
  }
      
  // Check that we have enough room to read the entire blob
  if (max_size < slot->blob_size)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("blob size mismatch (%d bytes < %d bytes)", max_size, slot->blob_size);
  }
     
  // Copy data into user buffer
  if (blob_id)
    *blob_id = slot->blob_id;
  memcpy(blob_data, slot->blob_data, slot->blob_size);
  pthread_mutex_unlock(&self->mutex);

  return 0;
}


// Peek at data in the cache.
int sensnet_peek(sensnet_t *self, int sensor_id, int blob_type, int *blob_id, int *blob_size)
{
  slot_t *slot;

  pthread_mutex_lock(&self->mutex);
    
  // Get the slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown sensor id or blob type");
  }

  // Copy data into user buffer
  if (blob_id)
    *blob_id = slot->blob_id;
  if (blob_size)
    *blob_size = slot->blob_size;
  pthread_mutex_unlock(&self->mutex);
  
  return 0;
}


// Peek at the current blob in the cache (non-blocking).
int sensnet_peek_ex(sensnet_t *self, int sensor_id, int blob_type,
                    int *blob_id, int *blob_size, uint64_t *timestamp)
{
  slot_t *slot;

  pthread_mutex_lock(&self->mutex);
    
  // Get the slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
  {
    pthread_mutex_unlock(&self->mutex);
    return ERROR("unknown sensor id or blob type");
  }

  // Copy data into user buffer
  if (blob_id)
    *blob_id = slot->blob_id;
  if (blob_size)
    *blob_size = slot->blob_size;
  if (timestamp)
    *timestamp = slot->recv_timestamp;
  pthread_mutex_unlock(&self->mutex);

  return 0;
}


// Wait for new sensor data (blocking).
int sensnet_wait(sensnet_t *self, int timeout)
{
  uint64_t tabs;
  struct timeval tv;
  struct timespec ts;
  int status;
    
  pthread_mutex_lock(&self->cond_mutex);

  if (timeout >= 0)
  {
    // Compute absolute time for timeout
    gettimeofday(&tv, NULL);
    tabs = (uint64_t) tv.tv_sec * 1000000000 + (uint64_t) tv.tv_usec * 1000;
    tabs += timeout * 1000000;
    ts.tv_sec = (long) (tabs / 1000000000);
    ts.tv_nsec = (long) (tabs % 1000000000); 

    // Wait for the condition to be signaled, with timeout
    status = pthread_cond_timedwait(&self->cond, &self->cond_mutex, &ts);
  }
  else
  {
    // Wait for the condition to be signaled, with no timeout
    status = pthread_cond_wait(&self->cond, &self->cond_mutex);
  }
    
  pthread_mutex_unlock(&self->cond_mutex);
  
  return status;
}


// Library thread: read message from Spread and populate the fifos
int sensnet_main(sensnet_t *self)
{
  int num_groups, mess_count, mess_len, endian, status;
  int16_t msg_type;
  int sensor_id, blob_type;
  service service_type;
  char sender[MAX_GROUP_NAME];
  char group[MAX_GROUP_NAME];
  int16_t dummy;
  packet_t packet;
  struct pollfd fd;
    
  MSG("starting SensNet thread");

  // Initialize poll data
  fd.fd = self->mbox;
  fd.events = POLLIN;

  while (!self->quit)
  {
    // Block until a new message arrives.  The timeout is provided
    // simply to allow the loop to quit on termination.
    mess_count = poll(&fd, 1, 100);

    // Test for termination
    if (self->quit)
      break;
    
    // Test for errors    
    if (mess_count < 0)
    {
      if (errno == EINTR)
        continue;
      ERROR("spread poll failed: %s", strerror(errno));
      break;
    }

    // No messages, so loop back
    if (mess_count == 0)
      continue;
    
    // Read a packet packet
    service_type = dummy = endian = 0;  
    mess_len = SP_receive(self->mbox, &service_type, sender, 1, &num_groups, &group,
                          &msg_type, &endian, sizeof(packet), (char*) &packet);
    if (mess_len < 0)
    {
      ERROR("spread recv failed: %d", mess_len);
      break;
    }

    // See if this is a skynet message; if it is parse the message
    // info from the group name.  TODO use the sensor id component,
    // which is currently ignored.
    assert(num_groups == 1);
    if (group[1] == 'n')
    {
      msg_type = SENSNET_TYPE_SKYNET;
      //sensor_id = atoi(group + 15);
      sensor_id = SENSNET_SKYNET_SENSOR;
      blob_type = atoi(group + 9);
      //MSG("group %s (%d %d %d)", group, msg_type, sensor_id, blob_type);
    }
    
    pthread_mutex_lock(&self->mutex);

    if (msg_type == SENSNET_TYPE_SHMEM)
    {
      // Handle shared mem packets
      status = decode_shmem_packet(self, &packet);
    }
    else if (msg_type == SENSNET_TYPE_CHUNK)
    {
      // Handle spread chunk packets
      status = decode_chunk_packet(self, &packet);
    }
    else if (msg_type == SENSNET_TYPE_SKYNET)
    {
      // Handle skynet messages
      status = decode_skynet_packet(self, sensor_id, blob_type, mess_len, &packet);
    }       
    
    pthread_mutex_unlock(&self->mutex);

    // Signal the waiting thread that we have new data
    if (status == 0)
    {
      pthread_mutex_lock(&self->cond_mutex);
      pthread_cond_signal(&self->cond);  
      pthread_mutex_unlock(&self->cond_mutex);
    }
  }

  MSG("exiting SensNet thread");
  
  return 0;
}



