
/* 
 * Desc: Module for reading from replayple logs.
 * Date: 30 March 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>

#include "sensnet_log.h"
#include "sensnet_replay.h"


// Maximum number of slots in cache
#define SENSNET_MAX_SLOTS 32


// Cache slot for a sensor/type
typedef struct
{
  // Sensor ID and blob type (constant) 
  int sensor_id, blob_type;

  // Maximum blob length (constant)
  int max_size;

  // Current blob data
  int blob_id, blob_size;
  unsigned char *blob_data;
  
} slot_t;


// Replay context
struct sensnet_replay
{  
  // List of logs 
  int num_logs;
  sensnet_log_t *logs[32];

  // Current log
  sensnet_log_t *current_log;

  // Current log timestamp
  uint64_t current_timestamp;

  // Current meta data
  int current_sensor_id;
  int current_blob_type;

  // Read cache
  int num_slots;
  slot_t slots[SENSNET_MAX_SLOTS];
};



// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Find a read slot 
static
slot_t *find_slot(sensnet_replay_t *self, int sensor_id, int blob_type)
{
  int i;
  slot_t *slot;
  for (i = 0; i < self->num_slots; i++)
  {
    slot = self->slots + i;
    if (slot->sensor_id == sensor_id && slot->blob_type == blob_type)
      return slot;
  }  
  return NULL;
}


// Create a read slot
static
slot_t *add_slot(sensnet_replay_t *self, int sensor_id, int blob_type, int max_size)
{
  slot_t *slot;
  assert(self->num_slots < sizeof(self->slots)/sizeof(self->slots[0]));
  slot = self->slots + self->num_slots++;
  memset(slot, 0, sizeof(*slot));  
  slot->sensor_id = sensor_id;
  slot->blob_type = blob_type;
  slot->blob_id = -1;
  slot->max_size = max_size;
  slot->blob_data = malloc(max_size);
  return slot;
}


// Remove a read slot
static
void del_slot(sensnet_replay_t *self, int sensor_id, int blob_type)
{
  slot_t *slot;
  slot = find_slot(self, sensor_id, blob_type);
  assert(slot);
  free(slot->blob_data);
  self->num_slots--;
  memmove(slot, slot + 1, (self->num_slots + slot - self->slots) * sizeof(*slot));
  return;
}


// Create context and allocate resources.
sensnet_replay_t *sensnet_replay_alloc()
{
  sensnet_replay_t *self;

  self = (sensnet_replay_t*) calloc(1, sizeof(sensnet_replay_t));
  
  return self;
}


// Destroy context and free all resources.
int sensnet_replay_free(sensnet_replay_t *self)
{
  free(self);
    
  return 0;
}


// Open log files for replay
int sensnet_replay_open(sensnet_replay_t *self, int num_filenames, char **filenames)
{
  int i;
  sensnet_log_t *log;
  sensnet_log_header_t header;

  if (num_filenames >= sizeof(self->logs) / sizeof(self->logs[0]))
    return ERROR("too many log files");
  
  // Open all of the log files
  for (i = 0; i < num_filenames; i++)
  {
    log = self->logs[self->num_logs++] = sensnet_log_alloc();
    if (sensnet_log_open_read(log, filenames[i], &header) != 0)
      return -1;
  }
  
  return 0;
}


// Clean up log replay
int sensnet_replay_close(sensnet_replay_t *self)
{
  int i;

  // Free slots
  while (self->num_slots > 0)
    del_slot(self, self->slots[0].sensor_id, self->slots[0].blob_type);

  // Free logs
  for (i = 0; i < self->num_logs; i++)
  {
    sensnet_log_close(self->logs[i]);
    sensnet_log_free(self->logs[i]);
    self->logs[i] = NULL;
  }
  self->num_logs = 0;

  return 0;
}


// Seek to a particular time in the logs.
int sensnet_replay_seek(sensnet_replay_t *self, uint64_t timestamp)
{
  int i;
    
  for (i = 0; i < self->num_logs; i++)
    sensnet_log_seek(self->logs[i], timestamp);

  return sensnet_replay_next(self, 0);
}


// Read the next entry from the log
int sensnet_replay_next(sensnet_replay_t *self, uint64_t steptime)
{
  int i;
  sensnet_log_t *log;
  uint64_t timestamp;
  int sensor_id, blob_type, blob_id, blob_size;
  uint64_t min_timestamp;
  sensnet_log_t *min_log;
  slot_t *slot;
  
  if (self->num_logs <= 0)
    return 0;

  while (true)
  {
    // Scan through the logs to find the lowest timestamp
    min_timestamp = 0;
    min_log = NULL;
    for (i = 0; i < self->num_logs; i++)
    {
      log = self->logs[i];
      if (sensnet_log_peek(log, &timestamp, &sensor_id, &blob_type, &blob_id, &blob_size) != 0)
        return -1;
      if (min_log == NULL || timestamp < min_timestamp)
      {
        min_timestamp = timestamp;
        min_log = log;
      }
    }
    if (min_log == NULL)
      return 0;

    // Seek forward by some interval
    if (steptime <= 0)
      break;
    if (min_timestamp - self->current_timestamp >= steptime)
      break;

    // Read and discard entries we are not interested in
    if (sensnet_log_read(min_log, NULL, NULL, NULL, NULL, 0, NULL) != 0)
      break;
  }
  
  // Get blob info from the selected log  
  if (sensnet_log_peek(min_log, NULL, &sensor_id, &blob_type, &blob_id, &blob_size) != 0)
    return -1;

  // Record the current log data
  self->current_log = min_log;
  self->current_timestamp = min_timestamp;
  self->current_sensor_id = sensor_id;
  self->current_blob_type = blob_type;

  // Get matching slot
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
    slot = add_slot(self, sensor_id, blob_type, blob_size);
  assert(slot);

  // Read data from log directly into the slot
  slot->blob_id = blob_id;
  slot->blob_size = blob_size;  
  if (sensnet_log_read(min_log, NULL, NULL, NULL, NULL, slot->max_size, slot->blob_data) != 0)
    return -1;

  return 0;
}


// Query the meta-data for the current log entry.
int sensnet_replay_query(sensnet_replay_t *self,
                         uint64_t *timestamp, int *sensor_id, int *blob_type)
{
  if (!self->current_log)
    return -1;

  if (timestamp)
    *timestamp = self->current_timestamp;
  if (sensor_id)
    *sensor_id = self->current_sensor_id;
  if (blob_type)
    *blob_type = self->current_blob_type;

  return 0;
}


// Peek at the current blob
int sensnet_replay_peek(sensnet_replay_t *self, int sensor_id, int blob_type,
                        int *blob_id, int *blob_size)
{
  slot_t *slot;

  // Get the slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
  {
    // If there is no slot, return an invalid blob.  We dont want to
    // return an error, since the caller has no way of knowing what
    // kind of data is stored in the log.
    if (blob_id)
      *blob_id = -1;
    if (blob_size)
      *blob_size = 0;
    return 0;
  }

  // Copy data into user buffer
  if (blob_id)
    *blob_id = slot->blob_id;
  if (blob_size)
    *blob_size = slot->blob_size;

  return 0;
}


// Read the current blob
int sensnet_replay_read(sensnet_replay_t *self,  int sensor_id, int blob_type,
                        int *blob_id, int max_size, void *blob_data)
{
  slot_t *slot;

  // Get the slot for this message type
  slot = find_slot(self, sensor_id, blob_type);
  if (!slot)
    return ERROR("unknown sensor id or blob type");

  // Check that we have enough room to read the entire blob
  if (max_size < slot->blob_size)
    return ERROR("blob size mismatch (%d bytes < %d bytes)", max_size, slot->blob_size);

  // Copy data into user buffer
  if (blob_id)
    *blob_id = slot->blob_id;

  // Copy data into user buffer
  memcpy(blob_data, slot->blob_data, slot->blob_size);

  return 0;
}



