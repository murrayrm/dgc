// STL headers
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cctype>
// unix headers
#include <dirent.h>
// local headers
#include "SensnetMultiLog.hh"

using namespace std; // don't do this into a header!


/* HELPER FUNCTIONS */
// within anonymous namespace to avoid exporting them to the outside


namespace {

  // change to lowercase all the characters of the string
  inline string toLower(string s) {
    for (unsigned int i = 0; i < s.length(); i++) {
      s[i] = tolower(s[i]);
    }
    return s;
  }

  // removes whitespace from the beginning and the end of the string
  inline string trim(string s) {
    int i, j;
    for (i = 0; i < (int) s.length(); i++) {
      if (!isspace(s[i]))
	break;
    }
    for (j = s.length()-1; j >= 0; j--) {
      if (!isspace(s[j]))
	break;
    }
    return s.substr(i, j - i + 1);
  }

} // anonymous namespace


/* IMPLEMENTATION OF SensnetMultiLog */

SensnetMultiLog::~SensnetMultiLog()
{
  clear();
}

int SensnetMultiLog::addLogDir(string logdir) throw(SensnetLog::Error)
{
  SensnetLog* pLog = new SensnetLog(logdir);
  m_logs.push_back(pLog);
  m_blobs.push_back(SensnetBlob());

  uint64_t tstamp;
  int idx = m_logs.size()-1;
  // get timestamp of first scan and put timestamp and index in the queue
  pLog->peek(&tstamp, NULL, NULL, NULL, NULL); // assume we have at least one scan
  m_pqscans.push(pq_data(tstamp, idx));

  if(m_pqscans.top().logIdx == idx) { // if this blob is the first one ...
    m_timestamp = tstamp - 1; // ... set the timestamp just a little earlier
  }

  // FIXME: is returning the index useful anywhere???
  return m_logs.size()-1; // return index 
}

void SensnetMultiLog::clear() throw()
{
  // free the data in m_logs
  for (log_iterator it = m_logs.begin(); it != m_logs.end(); ++it) {
    if (*it != 0) {
      delete *it;
    }
  }
  m_logs.resize(0);
  m_blobs.resize(0);
  while(!m_pqscans.empty()) // empty queue
    m_pqscans.pop();
  m_timestamp = 0;
}


void SensnetMultiLog::read(SensnetBlob* blob) throw(SensnetMultiLog::Error, SensnetLog::Error)
{
  if (!hasNext()) {
    throw Error("SensnetMultiLog::read(): No next scan available!");
  }

  // remove from the queue
  pq_data pqd = m_pqscans.top();
  int idx = pqd.logIdx;
  m_pqscans.pop();
  SensnetLog* pLog = m_logs[idx];
  // if blob == 0 (NULL), just discard data
  if (blob != 0) {
    pLog->read(blob);
  }
  m_timestamp = blob->timestamp;

  // read the timestamp of the next (future) blob, if any, and put it in the pqueue
  if (pLog->hasNext()) {
    uint64_t tstamp;
    pLog->peek(&tstamp, NULL, NULL, NULL, NULL);
    m_pqscans.push(pq_data(tstamp, idx));
  }
}

bool SensnetMultiLog::hasNext() throw()
{
  return !m_pqscans.empty();
}

void SensnetMultiLog::skipUntil(uint64_t timestamp) throw(SensnetLog::Error)
{
#warning "TODO: use sensnet_log_seek() (SensnetLog::seek()) instead!"
  while (hasNext() && m_timestamp < timestamp) {
    skip();
  }
}
