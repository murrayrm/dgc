
/* 
 * Desc: Module for reading from multiple log files.
 * Date: 30 March 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SENSNET_MULTI_H
#define SENSNET_MULTI_H

#ifdef __cplusplus
extern "C"
{
#endif

/** @file

@brief Read data from multiple log files using log timestamps.

**/


#include <stdbool.h>
#include <stdint.h>

  
/// @brief Multi-read context (opaque).
typedef struct sensnet_multi sensnet_multi_t;

/// @brief Create context and allocate resources.
sensnet_multi_t *sensnet_multi_alloc();

/// @brief Destroy context and free all resources.
int sensnet_multi_free(sensnet_multi_t *self);

/// @brief Open log files for replay.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_multi_open(sensnet_multi_t *self, int num_filenames, char **filenames);

// @brief Clean up after log replay.
///  
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_multi_close(sensnet_multi_t *self);

/// @brief Seek to a particular time in the logs.
///
/// @param[in] self Context.
/// @param[in] timestamp Seek timestamp (microseconds since epoch).
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_multi_seek(sensnet_multi_t *self, uint64_t timestamp);

/// @brief Read the next entry from the log (replay mode).
///
/// The step time specifies how far to advance the log, in
/// milliseconds.  To get the next message, irrespective of the
/// timestamp, set timeout to 0.
///
/// @param[in] self Context
/// @param[in] steptime Step time in milliseconds.
/// @returns Returns 0 on success.
int sensnet_multi_next(sensnet_multi_t *self, int steptime);

/// @brief Peek at the current data.
///
/// @param[in] self Context
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[out] sensor_id Sensor ID.
/// @param[out] blob_type Blob type.
/// @param[out] blob_id Latest blob id (-1 if there is no data).
/// @param[out] blob_size Current blob size.   
/// @returns Returns 0 on success.
int sensnet_multi_peek(sensnet_multi_t *self, uint64_t *timestamp,
                       int *sensor_id, int *blob_type, int *blob_id, int *blob_size);
  
/// @brief Read the current data.
///
/// @param[in] self Context
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[out] sensor_id Sensor ID.
/// @param[out] blob_type Blob type.
/// @param[out] blob_id Blob id.
/// @param[in]  blob_size Maximum blob size. 
/// @param[out] blob_data Blob data.
/// @returns Returns 0 on success.
int sensnet_multi_read(sensnet_multi_t *self, uint64_t *timestamp,
                       int *sensor_id, int *blob_type, int *blob_id, int blob_size,
                       void *blob_data);

  
#ifdef __cplusplus
}
#endif

#endif
