
/* Desc: Low-level logging interface for sensnet messages
 * Author: Andrew Howard
 * Date: 2 Feb 2005
 * CVS: $Id$
 */

#ifndef SENSNET_LOG_H
#define SENSNET_LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>

  
/** @file

@brief SensNetLog: Record and replay Sensnet blob data.

A sensnet log is a directory containing an index file, a data file and
(optionally) miscellaneous calibration files.  The interface described
here can be used to read or write opaque blobs to these logs.

Sensnet logs can also be accessed through through the sensnet_record and
sensnet_replay functions, in which case the data read from the log is
indistinguishable from live data.

@sa

- @ref sensnet.h

*/

  
/// @brief Header data for sensnet log.
typedef struct
{
  /// @internal Log file version number
  uint32_t version;
  
  /// Unix time for file generation (seconds since the Epoch)
  uint32_t timestamp;

  // Reserved
  uint32_t reserved[30];
  
} __attribute__((packed)) sensnet_log_header_t;

  
/// @brief Sensnet log context (opaque).
typedef struct _sensnet_log_t sensnet_log_t;

  
/// @brief Allocate object
///
/// @return Returns a new log handle.
sensnet_log_t *sensnet_log_alloc();

/// @brief Free object
///
/// @param[in] self Log handle.
void sensnet_log_free(sensnet_log_t *self);

/// @brief Open log for writing
///
/// @param[in] self Log handle.
/// @param[in] logname Path and directory name for the new log.
/// @returns Returns 0 on success, non-zero on error.    
int sensnet_log_open_write(sensnet_log_t *self, const char *logname, sensnet_log_header_t *header);

/// @brief Open log for reading
///
/// @param[in] self Log handle.
/// @param[in] logname Path and directory name for an existing log.
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_log_open_read(sensnet_log_t *self, const char *logname, sensnet_log_header_t *header);

/// @brief Close the log
///  
/// @param[in] self Log handle.
int sensnet_log_close(sensnet_log_t *self);

/// @brief Seek to a particular time in the log.
///
/// @param[in] self Log handle.
/// @param[in] timestamp Blob timestamp (microseconds since epoch).
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_log_seek(sensnet_log_t *self, uint64_t timestamp);
  
/// @brief Write a blob to the log
///
/// @param[in] self Log handle.
/// @param[in] timestamp Blob timestamp (microseconds since epoch).
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @param[in] blob_id   Unique blob identifier.
/// @param[in] blob_len  Blob length.
/// @param[in] blob_data Blob data.
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_log_write(sensnet_log_t *self, uint64_t timestamp,
                      int sensor_id, int blob_type, int blob_id,
                      int blob_len, const void *blob_data);

/// @brief Peek at the next entry in the log, but do not read the data.
///
/// @param[in] self Log handle.
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[out] sensor_id Sensor ID.  
/// @param[out] blob_type Blob type.
/// @param[out] blob_id   Unique blob identifier.
/// @param[out] blob_len  Blob length.
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_log_peek(sensnet_log_t *self, uint64_t *timestamp,
                     int *sensor_id, int *blob_type, int *blob_id, int *blob_len);

// check for eof
int sensnet_log_has_next(const sensnet_log_t* self);

/// @brief Read a blob from the log
///
/// @param[in] self Log handle.
/// @param[out] timestamp Blob timestamp (microseconds since epoch).
/// @param[out] sensor_id Sensor ID.  
/// @param[out] blob_type Blob type.
/// @param[out] blob_id   Unique blob identifier.
/// @param[in,out] blob_len On input: maximum blob length. On output: actual blob length.
/// @param[out] blob_data Blob data.
/// @returns Returns 0 on success, non-zero on error.  
int sensnet_log_read(sensnet_log_t *self, uint64_t *timestamp,
                     int *sensor_id, int *blob_type, int *blob_id,
                     int *blob_len, void *blob_data);

#ifdef __cplusplus
}
#endif

#endif
