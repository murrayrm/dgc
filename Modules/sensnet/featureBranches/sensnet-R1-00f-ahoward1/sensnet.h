
/* 
 * Desc: DGC sensnet library for accessing sensor data.
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SENSNET_H
#define SENSNET_H

#ifdef __cplusplus
extern "C"
{
#endif

/** @file

@brief SensNet: distributes high-bandwidth sensor data from
feeders to perceptors.

The SensNet library is used to transport high-bandwidth sensor data
(e.g., images) from feeders to perceptors.  Conceptually, it is very
similar to Spread/Skynet, but with one key exception: messages are
used to notify perceptors that data is available, but do not carry the
data itself.  Instead, the notification messages carry an index into a
local shared memory cache where the actual data is stored.  This
approach minimizes total bandwidth over the network and allows
perceptors to easily assemble unsynchronized data from multiple
sensors (such as cameras and lasers).

The SensNet library can also read/write Skynet messages.

@par Usage

SensNet is closely modelled on Spread/Skynet, so the basic steps are
very similar:

-# Create a SensNet context using ::sensnet_alloc.
-# Connect to a Spread daemon using ::sensnet_connect.
-# Join a particular sensor group or groups (e.g., front-stereo) using ::sensnet_join.
-# (Feeders) Write sensor data (blobs) using ::sensnet_write.
-# (Perceptors) Wait new for sensor data using ::sensnet_wait.
-# (Perceptors) Read sensor data (blobs) using ::sensnet_peek and ::sensnet_read.
-# Tidy up with ::sensnet_disconnect and ::sensnet_free.

That sensor data is contained in binary large objects (blobs).  These 
are maintained in a shared memory cache and are retrieved using 
a unique (positive) blob id.  The cache has a fixed size, so older blobs will
eventually be lost.

SensNet will also read/write Skynet messages; see ::sensnet_join and ::sensnet_write
for details.

@par Limitations

This library is not thread-safe.

@todo Implement distributed shared-memory transfers.

@sa

- sensnet_log.h

**/

  
/// @brief Sensnet library context (opaque).
typedef struct sensnet sensnet_t;

/// @brief Create context and allocate resources.
sensnet_t *sensnet_alloc();

/// @brief Destroy context and free all resources.
int sensnet_free(sensnet_t *self);

/// @brief Initialize connection.
///
/// @param[in] self Context
/// @param[in] spread_daemon Name of spread daemon.
/// @param[in] skynet_key Unique key to distinguish multiple concurrent users
/// on the same network.
/// @param[in] module_id Unique ID for the calling module.
/// @returns Returns 0 on success.
int sensnet_connect(sensnet_t *self, const char *spread_daemon,
                    int skynet_key, int module_id);

/// @brief Terminate connection.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_disconnect(sensnet_t *self);

/// @brief Open log file for recording.
///
/// Data transmitted with ::sensnet_write will be mirrored a log file.
///
/// @param[in] self Context
/// @param[in] filename Name of log directory.  
/// @returns Returns 0 on success.
int sensnet_open_record(sensnet_t *self, const char *filename);

/// @brief Close log file for recording.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_close_record(sensnet_t *self);

/// @brief Enable/disable log file recording.
///
/// @param[in] self Context
/// @param[in] enable Set to true to enable, false to disable.  
/// @returns Returns 0 on success.
int sensnet_enable_record(sensnet_t *self, int enable);  

/// @brief Open log files for replay.
///
/// Always call this function before calling ::sensnet_join, otherwise
/// odd stuff will happen.
///
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_open_replay(sensnet_t *self, int num_filenames, char **filenames);

// @brief Clean up after log replay.
///  
/// @param[in] self Context
/// @returns Returns 0 on success.
int sensnet_close_replay(sensnet_t *self);

/// @brief Join a particular sensor group.
///
/// Call this function to subscribe to sensor message groups; may be
/// called multiple times to subscribe to multiple groups.
///
/// To subscribe to Skynet messages, set sensor_id to
/// SENSET_SKYNET_SENSOR and blob_type to the Skynet message type.
///
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.  
/// @param[in] blob_type Blob type.
/// @param[in] blob_len Blob length (bytes).
/// @param[in] max_slots Number of slots to allocate in our cache.
/// @returns Returns 0 on success.
int sensnet_join(sensnet_t *self,
                 int sensor_id, int blob_type, int blob_len, int max_slots);

/// @brief Leave a particular sensor group.
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @returns Returns 0 on success.
int sensnet_leave(sensnet_t *self, int sensor_id, int blob_type);

/// @brief Write new sensor data.
///
/// To write Skynet messages, set sensor_id to SENSET_SKYNET_SENSOR
/// and blob_type to the Skynet message type.
///  
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @param[in] blob_id Blob id.
/// @param[in] blob_len Blob length.
/// @param[in] blob_data Blob data.
/// @returns Returns 0 on success.
int sensnet_write(sensnet_t *self, int sensor_id,
                  int blob_type, int blob_id, int blob_len, const void *blob_data);

/// @brief Wait for new data to arrive (blocking).
///
/// Blocks until new sensor data is available for a least one of the
/// currently joined groups.  When running in log replay mode, this
/// simply reads the next entry from the log(s).  
///
/// @param[in] self Context
/// @param[in] timeout Timeout in milliseconds; a negative value means infinite timeout.
/// @returns Returns 0 on success or timeout.
int sensnet_wait(sensnet_t *self, int timeout);
  
/// @brief Peek at the latest data from a particular sensor.
///
/// Returns the blob id and blob length of the newest data in the
/// cache.  The blob data can be retrieved using ::dgc_client_read.
///
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @param[out] blob_id Latest blob id (-1 if there is no data).
/// @param[out] blob_len Latest blob length (-1 if there is no data).
/// @returns Returns 0 on success.
int sensnet_peek(sensnet_t *self, int sensor_id, int blob_type,
                 int *blob_id, int *blob_len);
  
/// @brief Read data from the cache.
///
/// @param[in] self Context
/// @param[in] sensor_id Sensor ID.
/// @param[in] blob_type Blob type.
/// @param[in] blob_id Blob id (set to -1 to get the newest blob).
/// @param[in] blob_len Blob length.
/// @param[out] blob_data Blob data.
/// @returns Returns 0 on success.
int sensnet_read(sensnet_t *self, int sensor_id, int blob_type,
                 int blob_id, int blob_len, void *blob_data);

/// @brief Get the spread server name
const char *sensnet_spread_daemon(sensnet_t *self);
  
/// @brief Get the spread key value
int sensnet_skynet_key(sensnet_t *self);

/// @brief Get the spread private name
const char *sensnet_private_name(sensnet_t *self);

  
#ifdef __cplusplus
}
#endif

#endif
