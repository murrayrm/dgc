
/* 
 * Desc: Test program for DGC sensnet library
 * Date: 08 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "sensnet.h"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Test data
#define DUMMY_SKYNET_KEY 0xAA
#define DUMMY_SENSNET_ID 0x11
#define DUMMY_BLOB_TYPE 0xFF


// Get time in microseconds
uint64_t get_time()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Test writing
int test_write(int argc, const char *argv[])
{
  const char *spread_daemon;
  int sensor_id, blob_type;
  sensnet_t *sensnet;
  int blob_rate;
  int blob_size;
  char *blob;
  int i;
  int total_size;
  uint64_t total_time, send_time;

  MSG("sensnet write test");

  srand(get_time());
  
  if (argc >= 3)
    spread_daemon = argv[2];
  else
    spread_daemon = "4803";

  if (argc >= 4)
    blob_size = atoi(argv[3]);
  else
    blob_size = 6 * 640 * 480;

  if (argc >= 5)
    blob_rate = atoi(argv[4]);
  else  
    blob_rate = 15;

  sensor_id = DUMMY_SENSNET_ID;
  blob_type = DUMMY_BLOB_TYPE;
  blob = malloc(blob_size);

  // Connect to sensnet
  sensnet = sensnet_alloc();
  if (sensnet_connect(sensnet, spread_daemon, DUMMY_SKYNET_KEY, rand() % 0xFF) < 0)
    return -1;
  
  total_time = get_time();
  total_size = 0;
  
  for (i = 0; i < 1000; i++)
  {
    *((uint64_t*) blob) = get_time();
    MSG("sending %d %lld", i, *((uint64_t*) blob));

    send_time = get_time();
    if (sensnet_write(sensnet, SENSNET_METHOD_CHUNK, DUMMY_SENSNET_ID, DUMMY_BLOB_TYPE,
                      i, blob_size, blob) != 0)
      break;
    total_size += blob_size;
    send_time = get_time() - send_time;

    printf("latency %lld\n", send_time);
    
    // Throttle to desired rate
    while ((int) (get_time() - total_time) / (i + 1) < (int) (1e6 / blob_rate))
      usleep(0);
  }

  total_time = get_time() - total_time;

  MSG("%d blobs, %.1f Mb in %.1f sec = %.1f blobs/sec, %.1f Mb/sec",
      i, total_size / 1e6, total_time / 1e6,
      (double) i * 1e6 / total_time, (double) total_size / total_time);

  // Clean up
  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);  
  free(blob);

  MSG("exiting cleanly");
  
  return 0;
}


// Test wait and cache read
int test_read(int argc, const char *argv[])
{
  const char *spread_daemon;
  int sensor_id, blob_type, blob_id;
  sensnet_t *sensnet;
  int blob_size;
  char *blob;
  int i;
  uint64_t total_time;

  srand(get_time());
    
  MSG("senset read test");
  
  if (argc >= 3)
    spread_daemon = argv[2];
  else
    spread_daemon = "4803";

  if (argc >= 4)
    blob_size = atoi(argv[3]);
  else
    blob_size = 6 * 640 * 480;

  sensor_id = DUMMY_SENSNET_ID;
  blob_type = DUMMY_BLOB_TYPE;  
  blob = malloc(blob_size);

  // Connect to sensnet
  sensnet = sensnet_alloc();
  if (sensnet_connect(sensnet, spread_daemon, DUMMY_SKYNET_KEY, rand() % 0xFF) < 0)
    return -1;

  // Join an existing group
  if (sensnet_join(sensnet, sensor_id, blob_type, blob_size) < 0)
    return -1;
    
  MSG("waiting");

  for (i = 0; i < 2000; i++)
  {
    if (sensnet_wait(sensnet, -1) != 0)
      break;
    if (sensnet_read(sensnet, sensor_id, blob_type, &blob_id, blob_size, blob) < 0)
      break;
    
    MSG("recieved %d %lld", blob_id, *((uint64_t*) blob));

    if (i == 0)
      total_time = get_time();

    printf("latency %lld period %lld\n",
           get_time() - *((uint64_t*) blob),
           (get_time() - total_time) / (i + 1));
    fflush(stdout);
  }

  if (sensnet_leave(sensnet, sensor_id, blob_type) < 0)
    return -1;

  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  
  free(blob);

  MSG("exiting cleanly");
    
  return 0;
}


int main(int argc, const char *argv[])
{

  assert(argc >= 1);
  if (argc < 2)
    return ERROR("usage: %s [r|w]", argv[0]);

  if (argv[1][0] == 'w')
    test_write(argc, argv);
  else if (argv[1][0] == 'r')
    test_read(argc, argv);
  
  return 0;
}


