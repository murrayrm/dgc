
/* Desc: Simple test for replay-log reader library
 * Author: Andrew Howard
 * Date: 30 March 2007
 * CVS: $Id$
 */


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "sensnet_replay.h"


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)



// Read a log.  
int read_log(int num_filenames, char **filenames)
{
  sensnet_replay_t *replay;
  uint64_t timestamp;
  int sensor_id, blob_type, blob_id, max_size;
  char *blob;

  max_size = 4000000;
  blob = malloc(max_size);

  // Open for replay
  replay = sensnet_replay_alloc();
  assert(replay);
  if (sensnet_replay_open(replay, num_filenames, filenames) != 0)
    return -1;

  while (true)
  {
    // Advance the log
    if (sensnet_replay_next(replay, 0) != 0)
      break;

    // Read meta-data
    if (sensnet_replay_query(replay, &timestamp, &sensor_id, &blob_type) != 0)
      break;
    
    // Read data
    if (sensnet_replay_read(replay, sensor_id, blob_type, &blob_id, max_size, blob) != 0)
      break;

    MSG("read blob %.3f %d %d", (double) timestamp * 1e-6, sensor_id, blob_id);
  }

  sensnet_replay_close(replay);
  sensnet_replay_free(replay);
  free(blob);
  
  return 0;
}



// Simple self-test.
int main(int argc, char **argv)
{
  if (argc < 2)
  {
    printf("usage: %s <FILENAME> <FILENAME> ...\n", argv[0]);
    return -1;
  }

  read_log(argc - 1, argv + 1);
  
  return 0;
}

