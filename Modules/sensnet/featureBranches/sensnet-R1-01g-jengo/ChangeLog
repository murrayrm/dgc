Fri Jul  6 17:38:49 2007	datamino (datamino)

	* version R1-01g
	BUGS:  
	FILES: sensnet.c(28594)
	Fix to correctly handle EINTR in sensnet_main. This allows, on some
	systems, programs that use sensnet to be debugged with gdb and
	still receive messages.

Mon Jun 25 22:38:30 2007	 (ahoward)

	* version R1-01f
	BUGS:  
	FILES: sensnet.c(28286)
	Switched from SP_poll to poll

Mon Jun  4 23:04:24 2007	 (ahoward)

	* version R1-01e
	BUGS:  
	FILES: sensnet.c(26448), sensnet.h(26448)
	Added new peek function for getting recv timestamps on blobs

Fri May 18 23:52:14 2007	Andrew Howard (ahoward)

	* version R1-01d
	BUGS:  
	FILES: sensnet.c(23907)
	Modified final chunk size to improve small-blob bandwidth

Thu Apr 26  0:11:25 2007	 (ahoward)

	* version R1-01c
	BUGS:  
	FILES: sensnet.c(20771)
	Modified for new skynet group naming convention

	FILES: sensnet_replay.c(20107)
	Fixed inconsistency in query function

Sun Apr 15 22:40:24 2007	Andrew Howard (ahoward)

	* version R1-01b
	BUGS:  
	FILES: sensnet.c(19688), sensnet_log_test.c(19688)
	Fixed timeout calculation

Sun Apr 15 14:48:50 2007	Andrew Howard (ahoward)

	* version R1-01a
	BUGS:  
	FILES: sensnet.c(19605), sensnet.h(19605), sensnet_log.c(19605),
		sensnet_replay.c(19605), sensnet_replay.h(19605),
		sensnet_replay_test.c(19605)
	Normalized replay API with live API

Sat Apr 14 12:30:35 2007	Andrew Howard (ahoward)

	* version R1-01
	BUGS:  
	New files: sensnet_multi.h sensnet_replay.c sensnet_replay.h
		sensnet_replay_test.c
	FILES: Makefile.yam(19028)
	Separated multi-log reader from sensnet

	FILES: Makefile.yam(19066)
	Tweaks

	FILES: Makefile.yam(19549), sensnet.c(19549), sensnet.h(19549),
		sensnet_log.c(19549), sensnet_log.h(19549)
	Changed sensnet_multi to more obviously named sensent_replay

	FILES: sensnet.c(18957), sensnet_test.c(18957)
	Performance testing

	FILES: sensnet.c(18958)
	Adding shared mem implementation; not complete

	FILES: sensnet.c(18967), sensnet.h(18967), sensnet_test.c(18967)
	Experimenting with shmem transport

	FILES: sensnet.c(18975), sensnet_test.c(18975)
	foo

	FILES: sensnet.c(18979), sensnet.h(18979), sensnet_test.c(18979)
	Fully functional shared memory transport

	FILES: sensnet.c(19023), sensnet.h(19023)
	Re-added peek method

	FILES: sensnet.c(19025), sensnet.h(19025), sensnet_test.c(19025)
	Added method bit-field for writing

	FILES: sensnet.c(19512), sensnet.h(19512)
	Fixed major bug in skynet message handling; memcpy from incorrect
	address

	FILES: sensnet.c(19548)
	Field test

	FILES: sensnet.c(19553)
	Added initializer for skynet blob id

	FILES: sensnet_log.c(19514), sensnet_log.h(19514)
	Normalized API for sensnet, log and multi

	FILES: sensnet_test.c(18956)
	Testing

Wed Mar 21 11:36:14 2007	 (abhoward)

	* version R1-00r
	BUGS: 
	FILES: sensnet.c(18769)
	Fixed some uno warnings

	FILES: sensnet.c(18786)
	Changed private name format (again)

Tue Mar 20 16:15:55 2007	 (abhoward)

	* version R1-00q
	BUGS: 
Mon Mar 19 21:09:35 2007	 (ahoward)

	* version R1-00o
	BUGS: 
	FILES: sensnet.c(18223), sensnet.h(18223), sensnet_log.c(18223),
		sensnet_log.h(18223), sensnet_log_test.c(18223)
	Added high-speed logging with DMA writes

	FILES: sensnet.c(18405)
	Added skynet key to module id so multiple instances of a module can
	run on the same network

	FILES: sensnet_log.c(18164)
	Minor formatting tweaks

	FILES: sensnet_log_test.c(18611)
	Tweaks

Mon Mar 12 22:46:53 2007	 (ahoward)

	* version R1-00n
	BUGS: 
	FILES: sensnet.h(17280), sensnet_log.h(17280),
		sensnet_log_test.c(17280)
	Documentation and examples

	FILES: sensnet_log.c(17279)
	Fixed bug on > 2Gb files

Sat Feb 24 21:30:38 2007	datamino (datamino)

	* version R1-00m
	BUGS: 
	FILES: sensnet_log.c(15628)
	Implemented sensnet_log_seek().

	FILES: sensnet_log.c(15629)
	minor cleanup

Fri Feb 23 17:42:56 2007	datamino (datamino)

	* version R1-00l
	BUGS: 
	FILES: sensnet_log.c(15594)
	Fixed sensnet_log_has_next()

Fri Feb 23 17:33:07 2007	datamino (datamino)

	* version R1-00k
	BUGS: 
	Deleted files: SensnetLog.hh SensnetMultiLog.cc SensnetMultiLog.hh
		replay_cmdline.c replay_cmdline.ggo replay_cmdline.h
		sensnet_replay.cc
	FILES: Makefile.yam(15550)
	Moved sensnet-replay to its own module "sensnetreplay".

Fri Feb 23  0:14:05 2007	 (ahoward)

	* version R1-00j
	BUGS: 
	FILES: sensnet_log.c(15503)
	Inserted log-read bug-fig that got lost in the merge

Wed Feb 21 23:44:12 2007	 (ahoward)

	* version R1-00i
	BUGS: 
	FILES: sensnet.c(15384), sensnet.h(15384)
	Merged log replay changes from feature branch

Mon Feb 19 21:58:11 2007	datamino (datamino)

	* version R1-00h
	BUGS: 3113
	New files: SensnetLog.hh SensnetMultiLog.cc SensnetMultiLog.hh
		replay_cmdline.c replay_cmdline.ggo replay_cmdline.h
		sensnet_replay.cc
	FILES: Makefile.yam(15071)
	First revision of sensnet-replay, a tool that can load multiple log
	files and replay them correctly synchronized (i.e. sort the blobs
	using timestamps, and wait the correct amount of time between
	them). It still lacks some command-line options, but it's usable.
	KNOWN BUG: when the log ends, it just aborts instead of exiting
	cleanly.

	FILES: Makefile.yam(15111)
	Added commandline parser generated with gengetopt.

	FILES: Makefile.yam(15122)
	Added console display using cotk.

	FILES: sensnet_log.c(15070), sensnet_log.h(15070)
	Added a function, sensnet_log_has_next(), to check whether the log
	has more blobs to be read or not.

Wed Feb  7  9:08:29 2007	 (ahoward)

	* version R1-00g
	BUGS: 
	FILES: sensnet.c(14754)
	Added additional length check on blob reading

Mon Feb  5  8:54:56 2007	Andrew Howard (ahoward)

	* version R1-00f
	BUGS: 
	FILES: Makefile.yam(14598), sensnet.c(14598), sensnet.h(14598),
		sensnet_log.c(14598), sensnet_log.h(14598),
		sensnet_log_test.c(14598), sensnet_test.c(14598)
	Added build-in log recording and replay


Sun Feb  4 17:12:01 2007	Andrew Howard (ahoward)

	* version R1-00e
	BUGS: 
	New files: sensnet_log.c sensnet_log.h sensnet_log_test.c
	FILES: Makefile.yam(14501)
	Moved sensnet logging code the sensnet

	FILES: sensnet.c(14507), sensnet.h(14506)
	Tweaks

Mon Jan 22 15:28:00 2007	Andrew Howard (ahoward)

	* version R1-00c
	BUGS: 
	FILES: Makefile.yam(12988)
	Fixed broken link

Mon Jan 22 15:02:36 2007	Andrew Howard (ahoward)

	* version R1-00b
	BUGS: 
	New files: sensnet_test.c
	FILES: Makefile.yam(12962), sensnet.c(12962)
	Added test code, fixed library generation

Sun Jan 21 23:32:58 2007	Andrew Howard (ahoward)

	* version R1-00a
	BUGS: 
	New files: sensnet.c sensnet.h
	FILES: Makefile.yam(12950)
	Added sensnet code

	FILES: Makefile.yam(12951, 12952, 12953)
	foo

Sun Jan 21 23:06:21 2007	Andrew Howard (ahoward)

	* version R1-00
	Created sensnet module.

























