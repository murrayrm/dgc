/* 
 * traj2.c - secondary trajectory tracking routines
 *
 * Lookup table behavior from the 1-d tables.
 * On-line data changing.
 * Write a trajectory to a file
 */

#include <stdio.h>
#include "traj.h"

/*
 * This routine return the data in a specified row of the 
 * trajectory data.
 */

int traj_table(TRAJ_DATA *trjp, double *vector, int index){
  double time;

  return traj_row(trjp, index, &time, vector);
}

int traj_table2(TRAJ_DATA *trjp, double *vector, double *other, int index){
  return traj_row(trjp, index, other, vector);
}

int traj_row(TRAJ_DATA *t, int row, double *time, double *vector){
  int j;

  /* Error conditions */
  if ((row<0) || (row >= t->nrows)) return -1;

  for (j=0; j<t->ncols; j++) vector[j]=*(t->data[row]+j);
  *time = t->time[row];
  return 0;
}

/*
 * Change a specified row of the trajectory.
 *
 * This does not do any of the needed verification, so invariants can be
 * violated by using this command.
 */
int traj_datachange(TRAJ_DATA *trjp, double time, double *data, int row){
  return traj_setrow(trjp, row, time, data);
}

int traj_setrow(TRAJ_DATA *trjp, int row, double time, double *data){
  int i;
  double *dataptr; 

  if ((row<0) || (row >= trjp->nrows)) return (-1);

  dataptr = trjp->data[row];
 
  trjp->time[row] = time; 
  for(i=0; i<trjp->ncols; i++) *dataptr++ = *data++;
  
  return 1; 
}

/*
 * Write a trajectory to a data file
 */

int traj_write(TRAJ_DATA *l, char *file){
  FILE *fp;
  int i,j;

  if ((fp = fopen(file, "w")) == NULL){
    return -1;
  }

  fprintf(fp, "%i  %i  ",l->nrows,l->ncols);

  /* Pad with zeros, so matlab can load it directly */
  for(i=2; i<=l->ncols; i++) fprintf(fp, "0.0  ");
  fprintf(fp,"\n");

  for (i=0; i< l->nrows; i++){
    fprintf(fp, "%f  ",*(l->time + i));

    for (j=0; j<l->ncols; j++){
      fprintf(fp, "%f  ",*(*(l->data + i) +j));
    }
    fprintf(fp,"\n");
  }

  fclose(fp);
  return 0;
}
