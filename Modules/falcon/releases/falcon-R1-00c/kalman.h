#ifndef __falcon_kalman__
#define __falcon_kalman__

#include "matrix.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* Continuous time Kalman filter structure */
typedef struct kalman_filter {
  int nstates;				/* Number of states (for system) */
  int ninputs;				/* Number of inputs (for system) */
  int noutputs;				/* Number of outputs (measurements) */
  int ndisturb;				/* Number of process disturbances */

  MATRIX *A, *B, *C, *D, *F;		/* State space description */
  MATRIX *xhat, *P;			/* Estimatation variables */
  MATRIX *Rv, *Rw, *L;			/* Covariances and gain matrices */

  /* Tempororary variables used internal to code */
  MATRIX *dP, *dxhat, *yerr;		/* updates for Kalman filter */
  MATRIX *Ct_Rw_inv, *Ct_Rw_inv_C;	/* pre-multiplied matrices */

  int namelen;				/* Name length (including NULL) */
  char name[20];			/* Name of the filter */
  int initialized;			/* Set if properly initalized */
} KALMAN_FILTER;

KALMAN_FILTER *kf_init(int nstates, int ninputs, int noutputs, int ndisturbs);
KALMAN_FILTER *kf_create();
int kf_setup(KALMAN_FILTER *f, MATRIX *list);
KALMAN_FILTER *kf_load(char *);
int kf_resize(KALMAN_FILTER *, int nstates, int ninputs, int noutputs,
              int ndisturbs);
int kf_free(KALMAN_FILTER *);

int kf_set_initial(KALMAN_FILTER *f, double *xinit, double **Pinit);
int kf_set_dynamics(KALMAN_FILTER *f, double **A, double **B, double **C);

double *kf_compute_continuous(KALMAN_FILTER *f, double *inp, double *meas,
			      double dT, double *der);

#ifdef __cplusplus
}
#endif
#endif
