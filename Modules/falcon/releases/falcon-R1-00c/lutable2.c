#include <stdio.h>
#include "lutable.h"

#ifdef MSDOS
#include <alloc.h>
#else
#ifdef MACOSX
#include <stdlib.h>
#else
#include <malloc.h>
#endif
#endif

#define LU_DEBUG

void lu_dispinfo(LU_DATA *l){
  int i;

  if (l==NULL){
    printf("The lookup table is undefined!\n");
    return;
  }

  printf(" %i elements, %i dimensions, %i outputs (%i are interpolated)\n",
	 l->elements,l->ndim,l->nout,l->ninterp);
  printf("The dimensional info is:\n");
  for(i=0; i<l->ndim; i++){
    printf("Dim %i:  low=%g, inc=%g, hi=%g, n=%i.\n",i,l->info[i].low,
	   l->info[i].inc,l->info[i].high,l->info[i].nsteps);
  }
}

/*
 * Set the dimension range for an unitialized lookup table.
 * This must be done for each dimension before lu_resize is called.
 *
 * This is just a glofied accessor routine.
 */
int lu_setrange(LU_DATA *l, int index, double low, double inc, double high,
		int steps){
  
  if (l == (LU_DATA *)0) return -1;

  l->info[index].low    = low;
  l->info[index].inc    = inc;
  l->info[index].high   = high;
  l->info[index].nsteps = steps;

  return 0;
}

/*
 * Set the number of outputs and interpolated outputs.
 * This must be done before lu_resize is called.
 *
 * This is just a glofied accessor routine.
 */
int lu_setoutput(LU_DATA *l, int output, int interp){

  if (l == (LU_DATA *)0) return -1;

  l->nout    = output;

  if (interp > output){
    l->ninterp = output;
    return 1;
  } else {
    l->ninterp = interp;
    return 0;
  }
  
  return -2;
}


int lu_setcoord(LU_DATA *l){
  int i,j;
  double *val;
  int    *cnt;
  
  if (l == (LU_DATA *)0) return -1;

  val = (double *)malloc(l->ndim * sizeof(double));
  cnt = (int *)malloc(l->ndim * sizeof(int));
  for (i=0; i< l->ndim; i++){
    val[i] = l->info[i].low;
    cnt[i] = 0;
  }

  for (i=0; i< l->elements; i++){
    /* Set the coordinate value */
    for (j=0; j<l->ndim; j++){
      *(*(l->coords+i) + j) = val[j];
    }

    /* Zero the data field */
    for (j=0; j<l->nout; j++){
      *(*(l->data+i) + j) = 0;
    }

    /* Increment the value */
    for (j=l->ndim-1; j>=0; j--){
      if (++cnt[j] == l->info[j].nsteps){
	cnt[j] = 0;
	val[j] = l->info[j].low;
      }else{
	val[j] += l->info[j].inc;
	break;
      }
    }

  }
  free(val);
  free(cnt);

  return 0;
}


int lu_write(LU_DATA *l, char *file){
  FILE *fp;
  int i,j;

  if ((fp = fopen(file, "w")) == NULL){
    return -1;
  }

  fprintf(fp, "%i %i %i\n",l->ndim,l->nout,l->ninterp);
  for(i=0; i<l->ndim; i++){
    fprintf(fp,"%f %f %f %i\n",l->info[i].low,l->info[i].inc,
	    l->info[i].high,l->info[i].nsteps);
  }

  for (i=0; i< l->elements; i++){
    for (j=0; j<l->ndim; j++){
      fprintf(fp, "%f  ",*(*(l->coords + i) +j));
    }
    for (j=0; j<l->nout; j++){
      fprintf(fp, "%f  ",*(*(l->data + i) +j));
    }
    fprintf(fp,"\n");
  }

  fclose(fp);
  return 0;
}


/* lu_setel
 * lu_setrow
 *
 * Set the data access in the lookup table
 */

int lu_setel(LU_DATA *l, int row, int col, double value){
  if (row >= l->elements) return -1;  /* Out of range */
  if (col >= l->nout) return -2;  /* Out of range */

  *(*(l->data + row ) + col) = value;
  return 0;
}  

double lu_getel(LU_DATA *l, int row, int col){
  if (row >= l->elements) return 0;  /* Out of range */
  if (col >= l->nout)     return 0;  /* Out of range */

  return *(*(l->data + row ) + col);
}  

double *lu_getrow(LU_DATA *l, int row){
  if (row >= l->elements) return (double *) 0;  /* Out of range */

  return *(l->data + row );
}  



