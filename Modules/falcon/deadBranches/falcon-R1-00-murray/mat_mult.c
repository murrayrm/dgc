/*
 * mat_mult.c - general purpose matrix routines
 *
 * version 0.50b, August 1995
 *
 * Copyright 1995, Michael Kantner, kantner@hot.caltech.edu
 *
 * Do not distribute without the consent of the author
 * No warranties are implied.  Use at your own risk.
 */

#include "matrix.h"

int mat_mult(MATRIX *dst, MATRIX *a, MATRIX *b) { /* dst = a.b */
  MATRIX *a1, *b1;
  int freea1=0;
  int freeb1=0;

  if ((dst == (MATRIX *)0) || (a == (MATRIX *)0) || (b == (MATRIX *)0)) return -1;
  if (a->ncols != b->nrows) return -1;

  if (dst == a){
    a1 = mat_create();
    mat_copy(a1,a);
    freea1 = 1;
  }
  else
    a1 = a;

  if (dst == b){
    b1 = mat_create();
    mat_copy(b1,b);
    freeb1 = 1;
  }
  else
    b1 = b;

  mat_resize(dst,a1->nrows,b1->ncols);

  mat_mult_f(dst,a1,b1);

  if (freea1) mat_free(a1);
  if (freeb1) mat_free(b1);
  
  return 0;
}

void mat_mult_f(MATRIX *dst, MATRIX *a, MATRIX *b) { /* dst = a.b */

  register int i,j,k;

  mat_reset(dst);

  for (i = 0; i < dst->nrows; i++)
    for (j = 0; j < dst->ncols; j++)
      for (k = 0; k < a->ncols; k++)
	*(dst->real + i + j*dst->nrows) += 
	  *(a->real + i + k*a->nrows ) * *(b->real + k + j*b->nrows);

}

