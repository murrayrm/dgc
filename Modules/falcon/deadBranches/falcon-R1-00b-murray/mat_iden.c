/*
 * mat_iden.c - general purpose matrix routines
 *
 * version 0.50b, August 1995
 *
 * Copyright 1995, Michael Kantner, kantner@hot.caltech.edu
 *
 * Do not distribute without the consent of the author
 * No warranties are implied.  Use at your own risk.
 */

#include "matrix.h"

int mat_identity(MATRIX *a){

  int i, size;

  if ( a == (MATRIX *)0) return -1;
  if (mat_rows(a) < mat_columns(a))
    size = mat_rows(a);
  else
    size = mat_columns(a);

  mat_reset(a);
  for(i=0; i<size; i++)
    mat_element_set(a,i,i,1.0);

  return 0;
}

