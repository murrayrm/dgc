#ifndef _have_lpv
#define _have_lpv

#ifdef __cplusplus
extern "C"
{
#endif

#include "state.h"

typedef struct lpv{
  int ndeltas;         /* The number of delta operators */
                       /* Some deltas may be of zero size. */
                       /* This is number of rows in DSIZE */
  int sumdeltas;       /* The total size of the delta blocks */
  int n;               /* The number of states  */
  int m;               /* The number of exogenous inputs  */
  int p;               /* The number of exogenous outputs */
                       /* NOTE: m+sumdeltas = columns of SYS->B */

  STATE_SPACE *SYS;    /* The state space system */
  MATRIX      *DSIZE;  /* The size of each delta block */
  MATRIX      *DOFFSET;/* The commands do the following: */
  MATRIX      *DSCALE; /* delta = DSCALE * (deltas + DOFFSET) */

  MATRIX      *deltas; /* The actual deltas (an input vector)  */
  MATRIX      *u;      /* The control inputs */  /*Not malloced*/
  MATRIX      *y;      /* The control outputs */ /*Not malloced*/

  int namelen;    /* name of state space system */
  char name[20];  /* name length (including NULL) */
} LPV;

LPV *lpv_load(char *fname);
int lpv_setup(LPV *l, MATRIX *list);
LPV *lpv_create();
int lpv_resize(LPV *l);
int lpv_set_name(LPV *l, char *name);
char *lpv_get_name(LPV *l);
void lpv_free(LPV *l);

double *lpv_compute(LPV *l, double *input, double *delta);
void lpv_equation(LPV *l);
int lpv_set_delta(LPV *l, double *delta);
int lpv_set_input(LPV *l, double *input);
double *lpv_output(LPV *l);
void lpv_reset(LPV *l);

#ifdef __cplusplus
}
#endif

#endif
