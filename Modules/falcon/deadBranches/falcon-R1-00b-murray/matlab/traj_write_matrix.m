function traj_write_mat(filename, time, data)

% compute the header information
[nrows, ncols] = size(data);
header = zeros(1, ncols+1);
header(1) = nrows;
header(2) = ncols;

% now generate the table
table = [header; [time data]];

% now save the table to a file
eval(['save ', filename, ' table -ascii']);

