#ifndef CARFACTORY_H_
#define CARFACTORY_H_

#include <utility>
#include "Object.hh"
#include "Car.hh"
#include "CarProperties.hh"

namespace TrafSim
{
	
enum CarFactoryFailMode { CF_FAIL_WAIT, CF_FAIL_SKIP, CF_FAIL_OVERLAP };

/**
 * Use one of these to create a random stream of cars, in lieu of creating them
 * manually, one by one. The cars can have varied properties, and the stream
 * can be infinite, or consist of a given number of cars.
 */
class CarFactory: public Object
{
public:
	/**
	 * No complex initialization is performed in the constructor.
	 */
	CarFactory();
	virtual ~CarFactory();
	
	/// \return "CarFactory"
	virtual std::string getClassID() const { return "CarFactory"; }
	/**
	 * Does nothing at the moment.
	 */
	virtual void draw();
	/**
	 * Creates cars as specified by the parameters.
	 * \param ticks The number of seconds elapsed since the last call to simulate().
	 */
	virtual bool simulate(float ticks);
	/**
	 * Set the number of cars to create. This number is decremented with each car created,
	 * and when it reaches zero, the CarFactory stops creating cars. If the number is -1,
	 * the stream of cars is infinite.
	 * \param nCars The number of cars to create, or -1.
	 */
	void setRemainingCars(int nCars);
	/**
	 * \return The number of cars that are still to be created.
	 */
	int  getRemainingCars();
	/**
	 * \param rate The number of cars to create each second.
	 */
	void  setCreationRate(double rate);
	/**
	 * \return The number of cars created each second.
	 */
	double getCreationRate();
	/**
	 * \param delta A radius defining an interval around the creation rate, from which
	 *    a random rate is selected for each new car. Adds an extra element of randomness to 
	 *    the stream.
	 */
	void   setCRRandomDelta(double delta);
	/**
	 * \return The random rate interval radius.
	 */
	double  getCRRandomDelta();
	/**
	 * \param low The lower bound for each random car parameter.
	 * \param high The upper bound for each random car parameter.
	 */
	void  setCarProperties(CarProperties const& low, CarProperties const& high);
	/**
	 * \param props The set of parameters to create each car with (all cars are
	 *   the same if this function is used instead of the low, high version).
	 */
	void  setCarProperties(CarProperties const& props);
	/**
	 * \return The lower and upper bounds for the car properties, in a pair.
	 */
	std::pair<CarProperties,CarProperties> getCarProperties();
	/**
	 * \param seed In case recreating previous results is important, set a specific random
	 *   seed to be used for all randomized functions.
	 */
	void setRandomSeed(unsigned int seed);
	/**
	 * \return In case recreating previous results is important, get the specific random
	 *   seed to be used for all randomized functions.
	 */
	unsigned int  getRandomSeed();
	/**
	 * \param mode A fail mode is used when a car is currently occupying the space
	 *   the factory is trying to place a new car into. If the mode is WAIT, the factory
	 *   will create the new car at its earliest opportunity. If the mode is SKIP, it will
	 *   give up on that car and wait another interval for the next one. If the mode is OVERLAP,
	 *   it will just place the car, ignoring the obstruction. 
	 */
	void setFailMode(CarFactoryFailMode mode);
	/**
	 * \return The method the CarFactory is using to handle obstructions.
	 */
	CarFactoryFailMode getFailMode();
	/**
	 * \param path The path to initially assign to all created cars
	 */
	void setPath(std::vector<CrossManeuver> const& path);
	/**
	 * \param path The path to initially assign to all created cars (SWIG enum bug workaround)
	 */
	void setPath(std::vector<unsigned int> const& path);
	/**
	 * \return The path to initially assign to all created cars
	 */
	std::vector<CrossManeuver> const& getPath();
	/**
	 * Sets the point at which new cars are spawned.
	 * \param road The road to put the cars onto
	 * \param laneIndex The integer ID of the lane to place the cars in
	 * \param t The t-coordinate to initialize the cars with
	 */
	void setPosition(Road* road, unsigned int laneIndex, float t);
	/**
	 * \return The road the cars are created on
	 */
	Road* getRoad();
	/**
	 * \return The index of the lane the cars are created on
	 */
	unsigned int getLaneIndex();
	/**
	 * \return The t-coordinate the cars are initialized with.
	 */
	float getT();
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a CarFactory
	/// \return A CarFactory*, pointing to the same object as source.	
	static CarFactory* downcast(Object* source);
	
protected:

	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);

private:

	Road* spawnRoad;              // the road the cars are created on
	unsigned int spawnLaneIndex;  // the index of the lane the cars are created on
	float spawnT;				  // the t-coordinate the cars are initialized with

	std::vector<CrossManeuver> carPath;  // the path to initialize the cars with
	int remainingCars;	// the number of cars left to create
	double rate;		// the number of cars created per second
	double delta;		// the random delta applied to the interval
	
	CarProperties lowProps;   // lower bound on randomized properties
	CarProperties highProps;  // upper bound on randomized properties
	unsigned int seed;		  // random seed to reproduce previous results
	
	CarFactoryFailMode failMode;  // method for dealing with obstructions
	
	float timeLeft;  // number of seconds to wait to create the next car
};

}

#endif /*CARFACTORY_H_*/
