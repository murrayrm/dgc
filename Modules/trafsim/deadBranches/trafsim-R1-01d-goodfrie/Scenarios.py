from libtrafsim_Py import *

from SplineEditor import SplineEditor

class Scenarios:
    def __init__(self, env):
        self.environment = env
    
    def UT_tplanner_RR_SO_LegalPass(self):
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalPass(self):
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalUturn(self):
	b = Obstacle(398992.68318241555-self.environment.xTranslate,3781259.8189164037-self.environment.yTranslate,2,15)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_Partial(self):
	b = Obstacle(399000.68318241555-self.environment.xTranslate,3781267.8189164037-self.environment.yTranslate,3)
	self.environment.addObject(b)

    def simple_straightline(self):
        print "A"
        
    def simple_turns(self):
        print "B"
        b1 = Obstacle(403961.84-self.environment.xTranslate,3778457.2-self.environment.yTranslate,-.35)
        self.environment.addObject(b1)
        b2 = Obstacle(403949.379988-self.environment.xTranslate,3778473.61999-self.environment.yTranslate,-.35)
        self.environment.addObject(b2)
        b3 = Obstacle(403964.779985-self.environment.xTranslate,3778483.97999-self.environment.yTranslate,-.35)
        self.environment.addObject(b3)

    def simple_staticobstacle(self):
        print "C"
        b1 = Obstacle(403927.42-self.environment.xTranslate,3778490.8599777-self.environment.yTranslate,1)
        self.environment.addObject(b1)
        b2 = Obstacle(403927.420038-self.environment.xTranslate,3778488.0699768-self.environment.yTranslate,1)
        self.environment.addObject(b2)

    def simple_roadblock(self):
        print "D"
        b1 = Obstacle(403875.9274-self.environment.xTranslate,3778567.5125-self.environment.yTranslate,20,3,-.35)
        self.environment.addObject(b1)
        b2 = Obstacle(403983.9975-self.environment.xTranslate,3778527.3875-self.environment.yTranslate,20,3,-.35)
        self.environment.addObject(b2)

    def simple_circleblock(self):
        print "E"
        print "past Alice"
        b1 = Obstacle(403981.019985-self.environment.xTranslate,3778500.21999-self.environment.yTranslate,2)
        self.environment.addObject(b1)
        print "past first object"
        b2 = Obstacle(403979.89999-self.environment.xTranslate,3778504.97999-self.environment.yTranslate,2)
        self.environment.addObject(b2)
        b3 = Obstacle(403980.179985-self.environment.xTranslate,3778510.2999-self.environment.yTranslate,2)
        self.environment.addObject(b3)
        b4 = Obstacle(403983.539986-self.environment.xTranslate,3778521.21999-self.environment.yTranslate,2)
        self.environment.addObject(b4)
        b5 = Obstacle(403985.779987-self.environment.xTranslate,3778523.21999-self.environment.yTranslate,2)
        self.environment.addObject(b5)
        b6 = Obstacle(403989.419987-self.environment.xTranslate,3778525.13999-self.environment.yTranslate,2)
        self.environment.addObject(b6)
        print "finished placing blocks"

#Add obstacle somewhere between 1.1.5 and 1.1.6 that blocks both lanes
    def stluke_singleroad_roadblock(self):
        barricade1 = Obstacle(398929.40206307068-self.environment.xTranslate,
3781259.1864437452-self.environment.yTranslate, 25, 4, 3.14159/2)
        self.environment.addObject(barricade1)

#Add obstacle a little behind 1.1.2 that blocks the lane so Alice can
#go around it (without changing lanes)
    def stluke_singleroad_partlaneblock(self):
        partBlock1 = Obstacle(399015.33643501892-self.environment.xTranslate, 3781266.3486046186-self.environment.yTranslate, 1.5)
        self.environment.addObject(partBlock1)

#Add obstacle a little behind 1.1.2 that blocks the lane so Alice can go around
#it (change lanes).
    def stluke_singleroad_laneblock(self):
        block1 = Obstacle(399017.00831001892-self.environment.xTranslate, 3781263.4837402692-self.environment.yTranslate, 1.5)
        self.environment.addObject(block1)

#Add obstacle midway somewhere between 4.1.6 and 4.1.7 that partially blocks
#the lane so Alice can go around it (shouldn't change lanes).
    def stluke_small_partlaneblock(self):
        partBlock1 = Obstacle(399009.48487251892-self.environment.xTranslate, 3781326.8688721051-self.environment.yTranslate, 1)
        partBlock2 = Obstacle(399019.51612251892-self.environment.xTranslate, 3781322.929686009-self.environment.yTranslate, 1)
        self.environment.addObject(partBlock1)
        self.environment.addObject(partBlock2)

#Add an obstacle somewhere between 2.2.8 and 2.2.9 (closer to 2.2.9) that
#block the whole road
    def santaanita_sitevisit_roadblock(self):
        barricade1 = Obstacle(403886.66986496828-self.environment.xTranslate, 3778504.2238156628-self.environment.yTranslate, 18, 2, 3.5/3)
        self.environment.addObject(barricade1)

#Add an obstacle at the same place that only partially blocks the lane so Alice
#can go past it (while staying in the lane)
    def santaanita_sitevisit_passblock(self):
        block1 = Obstacle(403885.37245591066-self.environment.xTranslate, 3778501.4632603955-self.environment.yTranslate, 4, 4, 3.5/3)
        self.environment.addObject(block1)

#Add an obstacle at the same place that blocks the drive lane so Alice can go
#around it (changing lanes)
    def santaanita_sitevisit_block(self):
        block1 = Obstacle(403887.53480179689-self.environment.xTranslate, 3778507.4444649052-self.environment.yTranslate, 4, 4, 3.5/3)
        self.environment.addObject(block1)

#Add an obstacle somewhere between 2.2.10 and 2.2.11 that only partially blocks
#the lane (no lane changing should be required).
    def santaanita_sitevisit_partlaneblock(self):
        partBlock1 = Obstacle(403863.75407059572-self.environment.xTranslate, 3778516.4020390343-self.environment.yTranslate, 1.5)
        partBlock2 = Obstacle(403866.09334214113-self.environment.xTranslate, 3778524.9194295239-self.environment.yTranslate, 1.5)
        self.environment.addObject(partBlock1)
        self.environment.addObject(partBlock2)

    def santaanita_sitevisit_sparseroadblock(self):
        self.environment.addObject(Obstacle(-58.646942138671875, -17.636882781982422, 2))
        self.environment.addObject(Obstacle(-57.014129638671875, -14.333579063415527, 2))
        self.environment.addObject(Obstacle(-55.381317138671875, -10.149394989013672, 2))
        self.environment.addObject(Obstacle(-53.748504638671875, -6.0753211975097656, 2))
        self.environment.addObject(Obstacle(-51.771942138671875, -2.2214672565460205, 2))
        self.environment.addObject(Obstacle(-49.967254638671875, 1.9627169370651245, 2))

    def santaanita_sitevisit_turnroadblock(self):
        self.environment.addObject(Obstacle(-77.0,1.0, 30, 3, 0.78500000000000003))

