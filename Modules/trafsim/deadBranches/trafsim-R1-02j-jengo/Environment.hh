#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_
//
#include "Vector.hh"
#include "Object.hh"
#include "Road.hh"
#include "skynet/SkynetContainer.hh"
#include "interfaces/sn_types.h"
#include "map/MapElement.hh"
#include "map/MapElementTalker.hh"
#include "skynettalker/StateClient.hh"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>
#include <string>
#include <list>
#include <utility>

namespace TrafSim
{

  /// Base id for sending map elements
  const int BASEID = -5;

  class Road;
  
  typedef std::map<std::string, Object*> ObjectMap;
  typedef std::map<std::string, ObjectMap> EnvironmentMap;
  typedef std::map<std::string, unsigned int> NameMap;
  typedef std::map<std::string, std::vector<std::vector<point2> > > BoundaryMap;

  /**
   * A directory of simulation objects. An Environment's primary purpose is to maintain
   * references to the "root" objects in the simulation (such as roads, intersections, and
   * other primary features), to ensure that each root object has a unique name, and
   * to be able to save and restore its entire state, including that of each of its components.
   * 
   * If required an Environment will delete references to the objects it holds when they expire;
   * however, this is NOT automatic. If objects are to be deleted, make sure to make a call
   * to Environment::clearObjects().
   * 
   * NOTE: This class is not thread-safe. Only use it from a single thread. Furthermore, calls to draw()
   * should take place in the same thread that the current Viewport was created in.
   */
  class Environment : public CMapElementTalker, public CStateClient
  {
  public:


    // skynet key
    int skynetKey;
    // amount to translate when going from trafsim frame to global (based on rndf)
    double xTranslate, yTranslate;
    // amount to translate when going from local to global (based on alice)
    double xInit, yInit;
    // whether or not to print debugging messages
    bool debug;
    // a hack to allow user to pause the simulation
    bool paused;
    // whether or not the traffic is paused
    bool trafficPaused;
    //whether or not a test scenario is being created.
    bool makingScenario;
    // indicates whether we are speeding up traffic
    bool speedUpTraffic;
    // indicates whether we are slowing down traffic
    bool slowDownTraffic;
    // true if traffic needs to go back to normal speed
    bool resumeTraffic;
    
    
    //integer to hold the group number that we need to use for
    //debugging visualization. Default -2.
    int subgroup;

    /* ******* CONSTRUCTORS AND DESTRUCTORS ******* */

    /// No complex processing is done in the constructor; 
    /// only some private members are initialized.
    /// Default constructor
    Environment(bool initGlut = false);

    /// Skynet key is pulled from the environment and stored
    Environment(int skynet_key, bool initGlut = false);

    /// destructor just runs clearObjects()
    ~Environment();
	
    /* ******* DIRECTORY FUNCTIONS ******* */
	
    /// Adds a reference to the given object, trying to give it the proposedName.
    /// if proposedName is already in use, it will generate a new unique name.
    /// \param object The object to be inserted into the directory
    /// \param proposedName The desired name for the object. Defaults to the object's class ID.
    /// \return The actual name given to the object
    std::string const& addObject(Object* object, std::string const& proposedName = "", bool addBoundary = true);

    /// Stores the intersection boundaries
    void storeIntersectionBoundaries();
	
    /// Removes an object from the directory. If canDelete is true, also frees the object's memory.
    /// Also note that there are NO safeguards against dangling references. If an object is removed
    /// while another object refers to it, by name or otherwise, the results are unspecified.
    /// \param objName The name of the object to be removed.
    /// \param canDelete If true, will call operator delete on the object (be sure it's actually OK to do this!)
    /// \return True on success, False on failure. The Environment's state will be unchanged if the call fails.
    bool removeObject(std::string const& objName, bool canDelete = true);
	
    /// Changes an object's name.
    /// \param oldName The name of the object to be renamed
    /// \param newName The new name to assign to the object
    /// \return The object's new name on success, "" on failure. The Environment's state will be unchanged if the call fails.
    std::string renameObject(std::string const& oldName, std::string const& newName);
	
    /// Looks up an object by name. O(log n) in the number of objects.
    /// \param objName The name of the object to be looked up
    /// \return A pointer to the object, or NULL on failure.
    Object* getObject(std::string const& objName);
    std::string getObjectName(float x, float y, float distance = 20.0f);
	
    /// Returns a name->object table of all the objects with the given classID.
    /// \param classID The name of the class of objects to return
    /// \returns a std::map from object name to object pointer
    ObjectMap& getObjectsByClassID(std::string const& classID);
	
    /// Resets the environment to its original state. All objects are removed, and deleted if
    /// canDelete is set to true. The name directory is also cleared out.
    /// \param canDelete If true, will call operator delete on each object (be sure it's actually OK to do this!)

    //Clears objects based on classID, all objects of 'classID' are removed and deleted if canDelete is set to true. The name directory is also cleared out.        // \param canDelete If true, will call operator delete on each object (be sure it's actually OK to do this!)
    void clearObjectsByClassID(std::string const & classID,
			       bool canDelete = true);
        
    /// Pauses all the cars in the environment (toggle switch)
    void pauseAllCars();

    void clearObjects(bool canDelete = true);

    /// Sets all the stop lines
    void setStopLines(vector<Vector> arr);
	
    /* ******* REFERENCE FRAME FUNCTIONS ******* */
	
    /// Returns the bottom left corner of the rectangle that determines the boundaries of the environment.
    /// This information is used when interfacing with other software.
    /// \return A vector specifying the location of the corner in world coordinates
    Vector const& getBottomLeftBound() const;

    /// Returns the top right corner of the rectangle that determines the boundaries of the environment.
    /// This information is used when interfacing with other software.
    /// \return A vector specifying the location of the corner in world coordinates
    Vector const& getTopRightBound() const;
	
    /// Sets the rectangle that determines the boundaries of the environment.
    /// This information is used when interfacing with other software.
    /// \param bottomLeft The bottom left corner, in world coordinates
    /// \param topRight The top right corner, in world coordinates
    void setBounds(Vector const& bottomLeft, Vector const& topRight);
	
    /* ******* SERIALIZATION FUNCTIONS ******* */
	
    /// Writes the environment and its contents to an output stream.
    /// \param os The stream to write to
    /// \param env The environment to write
    /// \return The ouput stream
    friend std::ostream& operator<<(std::ostream& os, Environment const& env);
	
    /// Loads an environment and its contents from an input stream.
    /// \param is The stream to read from
    /// \param env The environment to read into
    /// \return The input stream
    friend std::istream& operator>>(std::istream& is, Environment & env);
	
    /* ******* SIMULATION FUNCTIONS ******* */
	
    /// Renders each of the environment's components. Call this from a 
    /// thread in which you opened a Viewport.
    void draw();
	
    /// Tells each of the environment's components to update their state by another
    /// time step, by calling their own simulate() functions. Any component that returns
    /// "false" from simulate() gets automatically removed from the environment and deleted.
    /// \param ticks The number of (virtual) seconds elapsed since the last call to simulate()
    void simulate(float ticks);
    
    /*! Takes a point in trafsim coords and converts it to local
     * \param p the point to convert
     * \return a point in local frame
     */
    point2 toLocal(point2 p);

    /*! Takes a point in trafsim coords and converts it to global */
    point2 toGlobal(point2 p);

    /*! Updates alice's state using asim, if she is on the map */
    void updateAliceState();
    /*! \brief Exports environment data via skynet */
    void exportEnv();
    /*! Exports vehicle data via skynet */
    void exportCars();
    /*! Exports static obstacle data via skynet */
    void exportObstacles();
    /*! Exports road line data via skynet */
    void exportRoadLines();

    /*! test class for doxygen */
    void testClass() { }

    void setTranslation(double xTrans, double yTrans)
    { xTranslate = xTrans; yTranslate = yTrans; }


    /*! alice's current location in trafsim coords */
    Vector aliceLocationVect;
    /*! Whether or not we want to center on alice */
    bool centerOnAlice;

    /*! Open a new file for logging data 
     \param name The name for the log (usually the car or whatever) */
    void openNewLog(string name);
    /*! Writes a timestamp and coordinate to an open log 
      \param p The coord to write
      \param name The name of the log */
    void writeToLog(point2 p, string name);
    /*! Close all the open files */
    void closeLogs();

  private:
    std::string _generateName(std::string const& proposedName);  // constructs a unique name

    // environment boundaries
    Vector bottomLeftBound;  
    Vector topRightBound;
	
    EnvironmentMap typeDirectory;   // lookup table for class IDs
    ObjectMap nameDirectory;		// lookup table for names
    NameMap nameList;				// helps keep track of which names are in use
    BoundaryMap roadMarkings;  //< keeps track of road markings in the environment
    VehicleState aliceState; //used if we have alice on the map
    
    double currentTime; // keep track of time spent running for log files
    map<string, ofstream*> openLogs; // the streams of open logs
    string logFileNameBase;

    point2 aliceLocation;

    vector<Vector> stopLines;
    
  };

}

#endif /*ENVIRONMENT_H_*/
