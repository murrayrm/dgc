mv ./libtrafsim.so ../_libtrafsim.so

rm ../*.pyc

MALLOC_CHECK_=1 python ../libtrafsim.py

epydoc -o ../epydoc/html ../libtrafsim.py

doxygen ../Doxyfile > ../../../doc/
