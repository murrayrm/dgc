#include "Vector.hh"
#include <cmath>
#include <cassert>
#include <GL/gl.h>

namespace TrafSim
{
	
/* ******** CONSTRUCTORS ******** */

Vector::Vector(): x(0.0f), y(0.0f) {}

Vector::Vector(float _x, float _y): x(_x), y(_y) {}

/* ******** BINARY ARITHMETIC OPERATIONS ******** */

Vector Vector::operator+(Vector const& rhs) const
{
	return Vector(x + rhs.x, y + rhs.y);
}

Vector Vector::operator-(Vector const& rhs) const
{
	return Vector(x - rhs.x, y - rhs.y);
}

Vector Vector::operator*(float rhs) const
{
	return Vector(x * rhs, y * rhs);
}

Vector Vector::operator/(float rhs) const
{
	assert(rhs != 0);
	
	return Vector(x / rhs, y / rhs);
}

float Vector::dot(Vector const& v1, Vector const& v2)
{
	return v1.x * v2.x + v1.y * v2.y;
}

float Vector::dist2(Vector const& v1, Vector const& v2)
{
	float dx = v1.x - v2.x;
	float dy = v1.y - v2.y;
	
	return dx * dx + dy * dy;
}

/* ******** UNARY ARITHMETIC OPERATIONS ******** */

Vector Vector::operator-() const
{
	return Vector(-x, -y);
}

Vector& Vector::operator+=(Vector const& rhs)
{
	x += rhs.x;
	y += rhs.y;
	
	return *this;
}

Vector& Vector::operator-=(Vector const& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	
	return *this;
}

Vector& Vector::operator*=(float rhs)
{
	x *= rhs;
	y *= rhs;
	
	return *this;
}
	
Vector& Vector::operator/=(float rhs)
{
	assert(rhs != 0);
	
	x /= rhs;
	y /= rhs;
	
	return *this;
}

float Vector::length() const
{
	return sqrt(x*x + y*y);
}

Vector Vector::normalized() const
{
	float length = this->length();
	
	assert(length != 0);
	
	return Vector(x / length, y / length);
}

Vector Vector::perpendicular() const
{
	return Vector(-y, x);
}

void Vector::normalize()
{
	float length = this->length();
	
	assert(length != 0);
	
	x /= length;
	y /= length;
}
		
/* ******** UTILITY OPERATIONS ******** */

std::ostream& operator<< (std::ostream& os, Vector const& v)
{
	return os << "Vector(" << v.x << ", " << v.y << ")";
}

std::istream& operator>> (std::istream& is, Vector& v)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	try
	{
		matchString(is, "Vector");
		matchString(is, "(");
		is >> v.x;
		matchString(is, ",");
		is >> v.y;
		matchString(is, ")");
	}
	catch(ParseError& e)
	{
		e.info = "Failed to parse Vector:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		 std::cerr << "caught2!\n";
		throw ParseError("Failed to parse Vector:\n" + std::string(e.what()) );
	}
	
	return is;
}

void Vector::draw(Vector const& origin, float arrowSize) const
{
	glPushMatrix();
	glTranslatef(origin.x, origin.y, 0.0f);
	
	// draw an arrow
	Vector perp = this->perpendicular().normalized() * arrowSize;
	Vector tan  = -this->normalized() * arrowSize * 2.0f;
	
	Vector arrowSide1 = *this + (perp + tan) * 0.5f; 
	Vector arrowSide2 = *this + (-perp + tan) * 0.5f;
	
	glBegin(GL_LINE_STRIP);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x,    y,    0.0f);
		glVertex3f(arrowSide1.x,
				   arrowSide1.y, 0.0f);
		glVertex3f(arrowSide2.x,
				   arrowSide2.y, 0.0f);
		glVertex3f(x,	 y,    0.0f);
	glEnd();
	
	glPopMatrix();
}

bool Vector::intersection(Vector const& seg1pt1, Vector const& seg1pt2,
				  		  Vector const& seg2pt1, Vector const& seg2pt2,
				  		  Vector& output)
{
	float x1 = seg1pt1.x;
    float y1 = seg1pt1.y;
    float x2 = seg1pt2.x;
    float y2 = seg1pt2.y;
    float x3 = seg2pt1.x;
    float y3 = seg2pt1.y;
    float x4 = seg2pt2.x;
    float y4 = seg2pt2.y;
    
    float y4y3 = y4 - y3;
    float x2x1 = x2 - x1;
    float x4x3 = x4 - x3;
    float y2y1 = y2 - y1;
    float x1x3 = x1 - x3;
    float y1y3 = y1 - y3;

    float denom = y4y3 * x2x1 - x4x3 * y2y1;
    
    if (denom == 0)
    	return false;
    	
    float ua = ( x4x3 * y1y3 - y4y3 * x1x3 ) / denom;
    float ub = ( x2x1 * y1y3 - y2y1 * x1x3 ) / denom;
    
    if (ua < 0.0 || ua > 1.0 || ub < 0.0 || ua > 1.0)
    	return false;
    	
    output.x = x1 + ua * (x2 - x1);
    output.y = y1 + ua * (y2 - y1);
    
    return true;	
}
	
					 	
}
