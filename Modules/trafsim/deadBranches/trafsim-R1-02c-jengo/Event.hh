#ifndef EVENT__HH__
#define EVENT__HH__

#include "Object.hh"
#include "Color.hh"

#include <math.h>
#include <time.h>

namespace TrafSim {

  using namespace std;

  class Event {
    public:
	string function;
	float x;
	float y;
	float distance;
	float delay;

	time_t startTime;
	bool waiting;
	bool active;

	Color color;

	Event();

	Event(string Function, float X, float Y, float Distance = 1.0,
		float Delay = 0.0);
	
	~Event() { }

	virtual string getClassID() const { return "Event"; }

	virtual void draw();

	virtual bool invertColor();

	void setPositions(float X, float Y) { x = X; y = Y; }

	void setPositions(point2 pos) {x = pos.x; y = pos.y; }

	virtual bool simulate (float ticks) { return false; }
	bool simulate(float X, float Y);

	point2 getCenter() { return point2(x, y); }

	float getLength() { return distance; }
	float getWidth() { return distance; }

	float distanceFrom(float X, float Y) {
	  return sqrt(pow(X-x, 2) + pow(Y-y, 2));
	}

	void activate() {
	    active = true;
	}
  };
}

#endif //EVENT__HH__
