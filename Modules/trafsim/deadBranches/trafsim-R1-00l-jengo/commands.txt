This file contains various notes, which may be useful/irrelavant and/or recent/outdated 

ADD A CAR TO THE ROAD

spline = app.editor.selectedSpline
road = Road()
app.environment.addObject(road, 'MyRoad')
road.initialize(app.environment,spline,1,1,5.0)

app.timingFactor = 0.01

c = Car()
c.switchLane(road,0,0.0)
app.environment.addObject(c,'car1')
app.environment.removeObject("car1")

app.environment.exportEnv()



ADD A STOPPED CAR

spline = app.editor.selectedSpline
road = Road()
app.environment.addObject(road, 'MyRoad')
road.initialize(app.environment,spline,1,1,5.0)

stopped_car = Car(1)

app.environment.addObject(stopped_car,'car')
stopped_car.switchLane(road,0,0.0)
app.environment.removeObject("car")


ADD AN EXTERNAL CAR (ALICE)

lg = LaneGrid()
lg.partition(app.environment,5.0,Vector(-100,-100), Vector(100, 100))  
app.environment.addObject(lg)
ec = ExternalCar(app.environment.skynetKey)
ec.initialize(app.environment)
app.environment.addObject(ec,'Alice')
app.environment.exportEnv()




ADD AN INTERSECTION

splines = [Spline.downcast(s) for s in app.environment.getObjectsByClassID('Spline').values()]
roads = [Road() for s in splines]
for road in roads: app.environment.addObject(road)
for road, spline in zip(roads, splines):
    road.initialize(app.environment, spline, 2, 2, 5.0)

yi = YIntersection() 
app.environment.addObject(yi, 'MyIntersection')
for road in roads: yi.hookStart(road)
yi.computeGeometry(app.environment)

xi = XIntersection() 
app.environment.addObject(xi, 'MyIntersection')
for road in roads: xi.hookStart(road)
xi.computeGeometry(app.environment)





 make a spline
app.runCarTest()
app.runAliceTest()


app.environment.exportEnv()




//HACKS:

LaneGrid size needs to scale to RNDF, hacked to be large.

Automatically runs TestA, AliceTest because stray debug statement in
Environment.cc. Always use cout!


TO DO:
Allow user to change default setting for timing factor (ie how fast to simulate)
check the frames/orientation/shifting crap

try to export car factory cars



OBJECT STUFF

  /* **** ACCESSORS INHERITED FROM OBJECT ****/

    /// \return the x,y position of the center of the object, in meters
    Vector getCenter();
    /// \return the velocity of the object, in m/s
    float getVelocity();
    /// \return the orientation of the object, in radians from -pi to pi
    float getOrientation();
    
    /// \return the length of the object, in meters
    float getLength();
    /// \return the width of the object, in meters
    float getWidth();
    
    /// Accessor for generic boundary of an object
    /// \return a set of 2D points that form the object's boundary
    std::vector<point2> getBoundary();
    
    /// Returns true if this object "contains" a given object
    /// For example, a road might contain a car or a lane
    bool containsObject(Object* obj);

//CarInterface: public Object
//CarFactory: public Object
//TestObject: public Object
Intersection: public Object
//Lane: public Object
//LaneGrid: public Object
//Road: public Object
//Spline: public Object

implement these methods
test each with a viewer

note: could try changing getBoundary to take in a spacing parameter
that would could vary the resolution of data

IMPORTANT NOTE FOR INTERSECTIONS: external cars need to use the right-of-way commands or else bad things will happen (see Intersection.hh)

need to check if lanes pulled from an RNDF are actually created each with splines, or one spline per segment. then running "getBoundary" on each lane would not work and the method used in Environment::exportRoadLines() would still have to be used

note: cars can be given a path for them to follow. most excellent.

here's the idea:
start up trafsim, with alice reading from asim
pause the simulation (maybe manipulate the ticks? this could be bad because i'm sure there's someplace that's dividing by zero)
add cars in specific lanes
give cars a path (cross maneuvers)
unpause the simulation

maybe a good idea to store things like road boundaries whenever they're
added to the environment

FIXING THE SEGFAULT MESS
need to check exactly what is added to env
is it getting removed by more than one thing?
are there memory leaks?
note: when a lane is deleted, it also deletes all objects in the lane!

-------

>>> spline = app.editor.selectedSpline
>>> road = Road()
>>> app.environment.addObject(road, 'MyRoad')
'MyRoad'
>>> road.initialize(app.environment,spline,1,1,5.0)
>>> 
>>> app.timingFactor = 1
>>> 
>>> c = Car()
>>> app.environment.addObject(c,'car1')
'car1'
>>> app.environment.removeObject("car1")
Environment::removeObject -- car1
object erased from name dir
object erased from type dir
Car::removeSelf() called
car deleted

True
>>> 

#this seems to work

-------

>>> c.switchLane(road,0,0.0)
Car::switchLane
new multilane created
car added to currentLane
end Car::switchLane

>>> Car has reached a dead end.
Car::removeSelf() called
car removed from currentLane
Lane destructor
end Lane destructor

currentLane deleted
car deleted


>>> c = Car()
../../bin/Drun: line 220: 22710 Segmentation fault      (core dumped) python -i GenericApp.py

---------

>>> c = Car()
>>> app.environment.addObject(c,'car1')
'car1'
>>> c.switchLane(road,0,0.0)
Car::switchLane
new multilane created
car added to currentLane
end Car::switchLane

>>> Car has reached a dead end.
Environment::removeObject -- car1
object erased from name dir
object erased from type dir
Car destructor
car removed from currentLane
Lane destructor
end Lane destructor

currentLane deleted
end car destructor

object car1 deleted

>>> c = Car()
>>> app.environment.addObject(c,'car1')
'car1___1'
>>> c.switchLane(road,0,0.0)
Car::switchLane
new multilane created
car added to currentLane
end Car::switchLane

>>> Car has reached a dead end.
Environment::removeObject -- car1___1
object erased from name dir
object erased from type dir
Car destructor
car removed from currentLane
Lane destructor
end Lane destructor

currentLane deleted
end car destructor

\ufffd\ufffd\ufffd\ufffd\ufffdy\ufffdDar1___1`\ufffd\ufffd\ufffdm\ufffd\ufffd:l\ufffd\ufffdP\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\u638a\ufffd\ufffd\ufffd0\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd.


# note: this still happens even if you run 
# c2 = Car()
# c2.switchLane(road,0,0.0)

-----

