#ifndef CAR_H_
#define CAR_H_

#include "Object.hh"
#include "Vector.hh"
#include "Lane.hh"
#include "Intersection.hh"
#include "CarProperties.hh"
#include "Road.hh"

namespace TrafSim {

/**
 * This abstract class is for internal use.
 */
class CarInterface: public Object
{
public:
	/// \return "Car"
	virtual std::string getClassID() const { return "Car"; }
	
	// Get the 1D velocity of the car moving along its lane
	virtual float getLaneVelocity() = 0;
	// Both simulated and external cars must know what lane they're on
	virtual Lane* getCurrentLane() = 0;
	
	// see the CarProperties class about these
	void setProperties(CarProperties const& props) { this->props = props; }
	CarProperties getProperties() { return this->props; }

protected:
	CarProperties props;
};
	
/**
 * The simulated car class contains high-level navigation functionality,
 * and ties together curve constraint, IDM, and intersection handling functions.
 */
class Car: public CarInterface
{
public:
	/**
	 * The constructor doesn't perform any complex initialization.
	 */
	Car();
	virtual ~Car();
	/**
	 * Cars are drawn as solid, colored rectangles.
	 */
	virtual void draw();
	/**
	 * Runs the car's AI and kinematics code for one time step.
	 * \param ticks Amount of time since the last call to simulate(), in seconds.
	 * \return Always true.
	 */
	virtual bool simulate(float ticks);
	/**
	 * Immediately transfers the car over to the given location. 
	 * \param newRoad The road to put the car onto
	 * \param newLaneIndex The integer ID of the lane to place the car in
	 * \param startT The t-coordinate to initialize the car with
	 */ 
	virtual void switchLane(Road* newRoad, unsigned int newLaneIndex, float startT = 0.0f);   	
	/**
	 * Changes the car's path. A path is a list of CrossManeuvers. By default, the path
	 * is empty, which means that the cars just move straight. If you change the path to 
	 * [CROSS_RIGHT, CROSS_LEFT], for example, the car will alternate taking left and right
	 * turns. If a car enters an intersection in which the required maneuver is not available, 
	 * it will just make an arbitrary maneuver. If a car reaches a complete dead end, it is deleted.
	 * \param path The list of CrossManeuvers to follow.
	 */
	void setPath(std::vector<CrossManeuver> const& path);
	/**
	 * SWIG has a problem with Vectors of enums. This is a workaround, and behaves identically to the
	 * other setPath() method.
	 * \param path The list of CrossManeuvers to follow.
	 */
	void setPath(std::vector<unsigned int> const& path);
	/**
	 * Returns the car's current linear velocity.
	 * \return The velocity of the car, in meters per second.
	 */
	virtual float getLaneVelocity();
	/**
	 * \return The lane the car is currently occupying.
	 */
	virtual Lane* getCurrentLane();
		
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a Car
	/// \return A Car*, pointing to the same object as source.	
	static Car* downcast(Object* source);
	
protected:

	// constructs an internal 3-lane combination for intersection handling
	Lane* createMultiLane(); 
	// transfers the car to a new road when it leaves an intersection
	void switchRoad();
	// applies the curve constraint to the car
	void updateTargetVelocity(float t);
	// applies the intersection gap acceptance mechanism				   
	bool acceptGap(Lane* opposingLane);
	
	/// Writes the object to the output stream, in standard format.
	/// NOT IMPLEMENTED
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	
	/// Reads the object from the input stream, in standard format.
	/// NOT IMPLEMENTED
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);
	
private:

	Vector position;   // world position vector
	float orientation; // 2d heading angle
	
	float velocity;		// linear velocity
	float targetVelocity;	// optimum velocity at current location
	float nextCurveEvalPoint; // next point to reevaluate the targetvelocity

	std::vector<CrossManeuver> path; 
	int pathIndex;			// current point along the car's path
	CrossManeuver nextManeuver;  // the next maneuver in the car's path
	
	Road* currentRoad;		
	Lane* currentLane;
	unsigned int laneIndex;
	
	unsigned int state;
	Car* followingCar;		// when the car is enqueued at an intersection, this
							// contains the next car in line
	std::vector<Lane*> opposingLanes; // opposing traffic lanes for intersection gap acceptance
	
	bool dead;		// true when the car is approaching a dead end and is soon to be deleted
};

}

#endif /*CAR_H_*/
