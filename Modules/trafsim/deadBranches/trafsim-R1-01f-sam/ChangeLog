Tue May 22 21:19:35 2007	Sam Pfister (sam)

	* version R1-01f-sam
	BUGS:  
	FILES: Environment.cc(24479), Environment.hh(24479)
	Updated trafsim to send map elements to the proper channel for
	visualization -2, as well as sending simulated data to mapper on
	channel 0.  For right now, the lane lines are not sent to mapper
	because it is not a realistic simulation to have full lanes sent
	in.  The full lane lines are sent out on the visualization channel.
	 Obstacles and vehicles are sent to both mapper and to
	visualization.	The element type of the vehicles is changed to
	ELEMENT_VEHICLE instead of ELEMENT_OBSTACLE

	FILES: Environment.cc(24506)
	Sam's small changes in Environment.cc

	FILES: Environment.cc(24514), Environment.hh(24514),
		libtrafsim_Py.py(24514), libtrafsim_swigwrap_Py.cc(24514)
	Updated send map element functionality to simulate sensor range. 
	Obstacles will only be placed in the map if they are within 50
	meters of Alice.  On the visualization channel, obstacles not in
	view will be visualized in white, while obstacles in view will be
	visualized in yellow.  Lane lines also are truncated to a distance
	of 10m away from alice, and are sent on both the visualization
	channel (-2) and mappers channel (0).

	FILES: doxy-trafsim.tag(24515)
	committing some doxy-trafsim.tag file that changed during ymk docs

Tue May 22  3:35:37 2007	Scott Goodfriend (goodfrie)

	* version R1-01f
	BUGS:  
	New files: ScenarioDictionary.py
	FILES: GenericApp.py(24352), Scenarios.py(24352),
		SplineEditor.py(24352)
	Fixed trafsim (Alice shows up in simulator again) and playing with
	scenario dictionary.

	FILES: GenericApp.py(24353), Scenarios.py(24353)
	Scenario creation is automated by trafsim, including function
	creation and system-test script creation.  Added a few more testing
	scenarios.  Chance of brokenness: High.

Mon May 21 17:45:03 2007	Scott Goodfriend (goodfrie)

	* version R1-01e
	BUGS:  
	New files: Scenarios.py
	FILES: GenericApp.py(24296)
	Separated the scenarios from GenericApp into Scenarios.py, and
	created a new scenario (santaanita_sitevisit_sparseroadblock)

	FILES: GenericApp.py(24298), SplineEditor.py(24298)
	Added automated scenario function creation (makeScenario(s) and
	commitScenario)

	FILES: GenericApp.py(24299)
	Fixed the scenario that was autocreated.

Sun May 20 18:10:49 2007	Jessica Gonzalez (jengo)

	* version R1-01d
	BUGS:  
	New files: fastCompile.sh
	FILES: Environment.cc(24189), ExternalCar.cc(24189),
		GenericApp.py(24189), Road.cc(24189)
	removed some annoying debug lines

	FILES: ExternalCar.cc(24188), ExternalCar.hh(24188),
		GenericApp.py(24188), Makefile.yam(24188),
		libtrafsim_Py.py(24188), libtrafsim_swigwrap_Py.cc(24188)
	fixed bug where alice was visualized in wrong location

	FILES: doxy-trafsim.tag(24190)
	documentation

Fri May 18  2:13:59 2007	Luke Durant (luke)

	* version R1-01c
	BUGS:  
	FILES: Environment.cc(23700)
	Fixed bug with obstacles not having correct type or height in
	mapper.

	FILES: SplineEditor.py(23708)
	Modified to make it easier to build testing scenarios.

Thu May 17  7:35:35 2007	Luke Durant (luke)

	* version R1-01b
	BUGS:  
	FILES: Environment.cc(23428), GenericApp.py(23428)
	Updated saving/loading methods. Works cleanly with mapper now.

Wed May 16 14:48:01 2007	Scott Goodfriend (goodfrie)

	* version R1-01a
	BUGS:  
	FILES: GenericApp.py(23334)
	Set up some scenarios in trafsim for this week's testing.

	FILES: GenericApp.py(23336)
	Edited the comments to the scenario functions for this week's
	testing.

Mon May 14 23:11:20 2007	Luke Durant (luke)

	* version R1-01
	BUGS:  
	FILES: Environment.cc(23114)
	Updating with new mapper interface, and variable subgroup number.

	FILES: Environment.hh(23115)
	Updating to support variable subgroup number

	FILES: GenericApp.py(23116)
	Updated to support variable subgroup and added more unit testing.

	FILES: GenericApp.py(23117), libtrafsim_Py.py(23117),
		libtrafsim_swigwrap_Py.cc(23117)
	Commiting changes to make a new release of trafsim.

Thu May 10  5:34:07 2007	Luke Durant (luke)

	* version R1-00z
	BUGS:  
	FILES: GenericApp.py(22664), Intersection.hh(22664),
		Spline.cc(22664), Spline.hh(22664), SplineEditor.py(22664),
		XIntersection.hh(22664), YIntersection.hh(22664),
		libtrafsim_Py.py(22664), libtrafsim_swigwrap_Py.cc(22664)
	Commiting to release trafsim with intersection support.

	FILES: GenericApp.py(22665)
	Updating GenericApp to contain the shell for basic tests.

Mon May  7 13:36:42 2007	Jessica Gonzalez (jengo)

	* version R1-00y
	BUGS:  
	FILES: Environment.cc(22397), Environment.hh(22397),
		ExternalCar.cc(22397), GenericApp.py(22397),
		libtrafsim_Py.py(22397), libtrafsim_swigwrap_Py.cc(22397)
	trafsim now sends out info in local coords

Thu Apr 26 20:37:12 2007	Luke Durant (luke)

	* version R1-00x
	BUGS:  
	FILES: ExternalCar.hh(21015), libtrafsim_Py.py(21015),
		libtrafsim_swigwrap_Py.cc(21015)
	Fixed bug in externalcar constructor.

Thu Apr 26 19:55:46 2007	Luke Durant (luke)

	* version R1-00w
	BUGS:  
	FILES: Environment.cc(20996)
	Added xTranslate and yTranslate to the output and input files.

	FILES: ExternalCar.hh(20997)
	Slight cleanup.

	FILES: GenericApp.py(20993)
	Added more information to help() as well as added several more
	helper functions, and did some general cleanup.

	FILES: Lane.cc(20995)
	Removed cout statements in Lane destructor.

	FILES: Road.cc(20994)
	Removed cout statements in Road destructor.

Wed Apr 25 23:54:17 2007	Jessica Gonzalez (jengo)

	* version R1-00v
	BUGS:  
	FILES: GenericApp.py(20765)
	added some unit tests for navigation

Wed Apr 25 22:36:14 2007	Jessica Gonzalez (jengo)

	* version R1-00u
	BUGS:  
	FILES: Environment.cc(20739), doxy-trafsim.tag(20739)
	fixed frames issue with interface to mapper

Tue Apr 24 21:07:20 2007	Jessica Gonzalez (jengo)

	* version R1-00t
	BUGS:  
	New files: Line.cc Line.hh
	FILES: Environment.cc(20538), GenericApp.py(20538),
		Makefile.yam(20538), Spline.cc(20538),
		SplineEditor.py(20538), libtrafsim.i(20538),
		libtrafsim_Py.py(20538), libtrafsim_swigwrap_Py.cc(20538)
	Added generic line object

	FILES: GenericApp.py(20539)
	fixed typo

Tue Apr 24 13:06:04 2007	Jessica Gonzalez (jengo)

	* version R1-00s
	BUGS:  
	FILES: Environment.cc(20332), Environment.hh(20332),
		GenericApp.py(20332)
	Fixed static obstacle exporting.

	FILES: libtrafsim_Py.py(20476), libtrafsim_swigwrap_Py.cc(20476)
	rebuild to work with new linked modules

Thu Apr 19 18:15:43 2007	Luke Durant (luke)

	* version R1-00r
	BUGS:  
	FILES: RNDFSimIF.cc(20018), RNDFSimIF.hh(20018)
	Added methods to prevent offset happening in the sparse
	approximation of the waypoints.

	FILES: RNDFSimIF.cc(20019)
	Took out a cout statement.

	FILES: libtrafsim_Py.py(20020), libtrafsim_swigwrap_Py.cc(20020)
	Adding files in order to do a yam save.

Tue Apr 17 14:37:12 2007	Luke Durant (luke)

	* version R1-00q
	BUGS:  
	New files: darpa-sample.txt
	FILES: GenericApp.py(19836), RNDFSimIF.cc(19836),
		RNDFSimIF.hh(19836), libtrafsim_Py.py(19836),
		libtrafsim_swigwrap_Py.cc(19836)
	Updating files for new RNDF interface structure.

Sun Apr 15 20:04:55 2007	Jessica Gonzalez (jengo)

	* version R1-00p
	BUGS:  
	New files: Obstacle.cc Obstacle.hh trafsim
	FILES: Car.cc(19669), Car.hh(19669), ExternalCar.cc(19669),
		Makefile.yam(19669), SplineEditor.py(19669),
		libtrafsim.i(19669), libtrafsim_Py.py(19669),
		libtrafsim_swigwrap_Py.cc(19669)
	Added new Obstacle class, for generating static obstacles of
	arbitrary size and position.

	FILES: GenericApp.py(19693), SplineEditor.py(19693)
	Added python code to easily add obstacles to env

Tue Apr 10 21:09:14 2007	Jessica Gonzalez (jengo)

	* version R1-00o
	BUGS: 
	FILES: Car.cc(19291), Car.hh(19291), CarFactory.cc(19291),
		CarFactory.hh(19291), Environment.cc(19291),
		Environment.hh(19291), ExternalCar.cc(19291),
		ExternalCar.hh(19291), GenericApp.py(19291),
		Intersection.cc(19291), Intersection.hh(19291),
		Lane.cc(19291), Lane.hh(19291), LaneGrid.cc(19291),
		LaneGrid.hh(19291), Object.hh(19291), Road.cc(19291),
		Road.hh(19291), Spline.cc(19291), Spline.hh(19291),
		SplineEditor.py(19291), XIntersection.cc(19291),
		XIntersection.hh(19291), YIntersection.cc(19291),
		YIntersection.hh(19291), commands.txt(19291),
		libtrafsim_Py.py(19291), libtrafsim_swigwrap_Py.cc(19291)
	Fixed some lane line exporting issues so things aren't so messy.
	Added a few little tweaks, like the capability to pause the
	simulation

Mon Apr  9  0:21:21 2007	Jessica Gonzalez (jengo)

	* version R1-00n
	BUGS: 
	FILES: Environment.cc(19222), Environment.hh(19222),
		GenericApp.py(19222), Lane.cc(19222), Lane.hh(19222),
		XIntersection.cc(19222), YIntersection.cc(19222),
		commands.txt(19222), libtrafsim_Py.py(19222),
		libtrafsim_swigwrap_Py.cc(19222)
	Exports intersection information

Sun Apr  8 16:43:53 2007	Jessica Gonzalez (jengo)

	* version R1-00m
	BUGS: 
	FILES: Car.cc(19212), Car.hh(19212), GenericApp.py(19212),
		Lane.cc(19212), Makefile.yam(19212), Object.hh(19212),
		Road.cc(19212), Spline.hh(19212), commands.txt(19212)
	Merging luke's branch with mine

	FILES: Car.cc(19213), Car.hh(19213), Environment.cc(19213),
		Environment.hh(19213), ExternalCar.cc(19213),
		GenericApp.py(19213), SplineEditor.py(19213),
		libtrafsim_Py.py(19213), libtrafsim_swigwrap_Py.cc(19213)
	Added capability to move alice and add static obstacles with a
	mouse click. Also added a very brief help function.

Fri Apr  6  2:24:14 2007	Luke Durant (luke)

	* version R1-00l
	BUGS: 
	FILES: GenericApp.py(19166), RNDFSimIF.cc(19166),
		RNDFSimIF.hh(19166), Spline.cc(19166), Spline.hh(19166),
		SplineEditor.py(19166)
	Updated these files to support multi-lane roads.

	FILES: GenericApp.py(19167), RNDFSimIF.cc(19167)
	Removed debug comments.

	FILES: libtrafsim_Py.py(19168), libtrafsim_swigwrap_Py.cc(19168)
	Committing these because YAM was complaining.

Sun Apr  1 20:26:43 2007	Jessica Gonzalez (jengo)

	* version R1-00k
	BUGS: 
	New files: doc/doxy-trafsim.tag generateDocs.sh
	Deleted files: DARPA_RNDF_Default.txt ROSE_BOWL_RNDF.txt
		St_Lukes_RNDF.txt tplanner_unittest.rddf~
	FILES: Car.cc(19075), Car.hh(19075), CarFactory.cc(19075),
		CarFactory.hh(19075), Doxyfile(19075),
		Environment.cc(19075), Environment.hh(19075),
		ExternalCar.cc(19075), GenericApp.py(19075),
		Intersection.cc(19075), Intersection.hh(19075),
		Lane.cc(19075), Lane.hh(19075), LaneGrid.cc(19075),
		LaneGrid.hh(19075), Object.cc(19075), Object.hh(19075),
		Road.cc(19075), Road.hh(19075), Spline.cc(19075),
		Spline.hh(19075), XIntersection.cc(19075),
		XIntersection.hh(19075), YIntersection.cc(19075),
		YIntersection.hh(19075), commands.txt(19075),
		libtrafsim_Py.py(19075), libtrafsim_swigwrap_Py.cc(19075)
	trying to fix merge problems



Sun Apr  1 20:01:36 2007	Jessica Gonzalez (jengo)

	* version R1-00i-jengo
	BUGS: 
	New files: doc/doxy-trafsim.tag generateDocs.sh
	Deleted files: DARPA_RNDF_Default.txt ROSE_BOWL_RNDF.txt
		St_Lukes_RNDF.txt tplanner_unittest.rddf~
	FILES: Car.cc(19040), Car.hh(19040), Environment.cc(19040),
		Environment.hh(19040), ExternalCar.cc(19040),
		Lane.cc(19040), Lane.hh(19040), Object.cc(19040),
		Object.hh(19040), Spline.cc(19040), Spline.hh(19040),
		commands.txt(19040), libtrafsim_Py.py(19040),
		libtrafsim_swigwrap_Py.cc(19040)
	Started making a consistent Object interface with new methods like
	getBoundary, get position, etc. About halfway through. Also brought
	trafsim up to date with releases of other modules (I think).

	FILES: Car.cc(19067), Car.hh(19067), CarFactory.cc(19067),
		CarFactory.hh(19067), Environment.cc(19067),
		Intersection.cc(19067), Intersection.hh(19067),
		Lane.cc(19067), Lane.hh(19067), LaneGrid.cc(19067),
		LaneGrid.hh(19067), Object.hh(19067), Road.cc(19067),
		Road.hh(19067), Spline.cc(19067), Spline.hh(19067),
		XIntersection.cc(19067), XIntersection.hh(19067),
		YIntersection.cc(19067), YIntersection.hh(19067),
		commands.txt(19067), libtrafsim_Py.py(19067),
		libtrafsim_swigwrap_Py.cc(19067)
	finished updating all classes to work with new Object structure

	FILES: Doxyfile(18970), Environment.cc(18970),
		Environment.hh(18970), Makefile.yam(18970),
		commands.txt(18970), libtrafsim_Py.py(18970),
		libtrafsim_swigwrap_Py.cc(18970)
	now generates doxygen docs correctly 

	FILES: Environment.cc(19068), Lane.hh(19068),
		XIntersection.hh(19068), YIntersection.hh(19068)
	a few bug fixes

	FILES: GenericApp.py(16469)
	added some error handling if a skynet key is not set

Fri Mar  9 15:40:08 2007	Luke Durant (luke)
	Updated such that the output to mapper of roadlines for X is northing, and Y is easting. Internal trafsim representation is still northing-Y, 
easting-X.

	* version R1-00j
	BUGS: 
	FILES: Environment.cc(16882), Environment.hh(16882),
		Makefile.yam(16882)
	Modified to support latest mapper structure.


Sat Mar  3 16:33:43 2007	Luke Durant (luke)

	* version R1-00i
	BUGS: 
	New files: SPARSE_ROAD_MDF.txt SPARSE_ROAD_RNDF.txt
		TRAFSIM_TEST_MDF.txt tplanUnit.tsm tplanner_unittest.mdf
		tplanner_unittest.rddf tplanner_unittest.rddf~
		tplanner_unittest.rndf
	FILES: Environment.cc(16144), Environment.hh(16144),
		ExternalCar.cc(16144), ExternalCar.hh(16144),
		GenericApp.py(16144), RNDFSimIF.cc(16144),
		RNDFSimIF.hh(16144), TRAFSIM_TEST_RNDF.txt(16144),
		commands.txt(16144), libtrafsim_Py.py(16144),
		libtrafsim_swigwrap_Py.cc(16144)
	Working version of trafsim, before March2 Integration test.

	FILES: Environment.cc(16177), Environment.hh(16177),
		ExternalCar.cc(16177), GenericApp.py(16177),
		commands.txt(16177), libtrafsim_Py.py(16177),
		libtrafsim_swigwrap_Py.cc(16177)
	Updated after interface with mapper was closed. Hacks logged in
	commands.txt.

	FILES: Environment.cc(16403), ExternalCar.cc(16403),
		GenericApp.py(16403)
	Got tplanner unit test rndf working

	FILES: Makefile.yam(16414)
	Added frames library to python librar

Sun Feb 25 17:22:13 2007	Jessica Gonzalez (jengo)

	* version R1-00h
	BUGS: 
	FILES: Car.cc(15627), Car.hh(15627), Environment.cc(15627),
		Environment.hh(15627), ExternalCar.cc(15627),
		ExternalCar.hh(15627), Object.hh(15627),
		RNDFSimIF.hh(15627), Road.hh(15627), commands.txt(15627),
		libtrafsim.i(15627), libtrafsim_Py.py(15627),
		libtrafsim_swigwrap_Py.cc(15627)
	fixed header files to work with interfaces/skynet/talkers/etc
	rearrangement

	FILES: Car.hh(15112), Environment.cc(15112), Environment.hh(15112),
		LaneGrid.hh(15112), Road.hh(15112), commands.txt(15112),
		libtrafsim_Py.py(15112), libtrafsim_swigwrap_Py.cc(15112)
	removed redundant header files

	FILES: Environment.cc(15662), Makefile.yam(15662)
	exporting cars works with new mapper

	FILES: Environment.cc(15704), ExternalCar.hh(15704),
		GenericApp.py(15704), Object.hh(15704), Road.cc(15704),
		Road.hh(15704), Vector.cc(15704), Vector.hh(15704),
		commands.txt(15704), libtrafsim_Py.py(15704),
		libtrafsim_swigwrap_Py.cc(15704)
	trafsim now exports road boundaries. still need to do some geometry
	to get the intermediate lane lines

	FILES: Environment.cc(15726), Environment.hh(15726),
		Road.cc(15726), Road.hh(15726), commands.txt(15726),
		libtrafsim_Py.py(15726), libtrafsim_swigwrap_Py.cc(15726)
	added ability to export intermediate lane lines and tested with
	skynettalker/testRecvMapElement

Sun Feb 18 22:28:42 2007	Luke Durant (luke)

	* version R1-00g
	BUGS: 
	New files: DARPA_RNDF_Default.txt RNDFSimIF.cc RNDFSimIF.hh
		ROSE_BOWL_RNDF.txt ST_LUKE_RNDF_MOD.txt St_Lukes_RNDF.txt
		TRAFSIM_TEST_RNDF.txt
	FILES: Environment.cc(15101), Environment.hh(15101),
		GenericApp.py(15101), Makefile.yam(15101),
		SplineEditor.py(15101), libtrafsim.i(15101),
		libtrafsim_Py.py(15101), libtrafsim_swigwrap_Py.cc(15101)
	Added RNDF wrapper class, with ability to (minimally) parse RNDFs
	into trafsim, as well as modifications to the python interfaces for
	ease of use. Added RNDF data files so that they won't have to be
	tracked down for testing.

Sun Feb 18 21:38:18 2007	Jessica Gonzalez (jengo)

	* version R1-00f
	BUGS: 
	New files: commands.txt
	FILES: Car.cc(15085), Car.hh(15085), Environment.cc(15085),
		Environment.hh(15085), GenericApp.py(15085),
		Lane.cc(15085), libtrafsim_Py.py(15085),
		libtrafsim_swigwrap_Py.cc(15085)
	Added ability to export Car objects to the mapper, though not all
	attributes are sent at this time. Added ability to create a
	'stopped car'.

	FILES: Car.cc(15086), Car.hh(15086), CarProperties.cc(15086),
		CarProperties.hh(15086), Environment.cc(15086),
		GenericApp.py(15086), Object.hh(15086),
		libtrafsim_Py.py(15086), libtrafsim_swigwrap_Py.cc(15086)
	trafsim now exports more info (length, width, height, position,
	velocity, orientation) about cars in the simulator

Fri Feb  9 18:31:31 2007	Jessica Gonzalez (jengo)

	* version R1-00e
	BUGS: 
	FILES: Car.hh(14873), Environment.cc(14873), Environment.hh(14873),
		ExternalCar.cc(14873), ExternalCar.hh(14873),
		Makefile.yam(14873), libtrafsim_Py.py(14873),
		libtrafsim_swigwrap_Py.cc(14873)
	Added ability to recieve state data in an ExternalCar object

	FILES: ExternalCar.cc(14874)
	fixed the velocity input on ExternalCar::simulate()

Thu Feb  8 20:47:15 2007	Jessica Gonzalez (jengo)

	* version R1-00d
	BUGS: 
	New files: doctest/Color.i doctest/Environment.i doctest/Object.i
		doctest/Spline.i doctest/Vector.i doctest/Viewport.i
		libtrafsim_Py.py libtrafsim_swigwrap_Py.cc
	Deleted files: libtrafsim.py libtrafsim_wrap.cc
	FILES: CarDriver.py(14835), DemExporter.py(14835),
		GenericApp.py(14835), SplineEditor.py(14835)
	Changed the name of the libtrafsim Python module to libtrafsim_Py
	in line with YaM's conventions

	FILES: ExternalCar.cc(14652), ExternalCar.hh(14652),
		Makefile.yam(14652), libtrafsim.i(14652),
		postbuild.sh(14652)
	trying to incorporate skynet into trafsim

	FILES: Makefile.yam(14836)
	Cleaned up building of Python SWIG wrappers. Also now export Python
	modules so trafsim can be run from anywhere.

Sat Feb  3 23:23:54 2007	Jessica Gonzalez (jengo)

	* version R1-00c
	BUGS: 
	New files: Car.cc Car.hh CarDriver.py CarFactory.cc CarFactory.hh
		CarProperties.cc CarProperties.hh Color.cc Color.hh
		CurveConstraint.cc CurveConstraint.hh Debug/makefile
		Debug/objects.mk Debug/postbuild.sh Debug/sources.mk
		Debug/subdir.mk DemExporter.py Doxyfile Environment.cc
		Environment.hh ExternalCar.cc ExternalCar.hh GenericApp.py
		IDM.cc IDM.hh Intersection.cc Intersection.hh Lane.cc
		Lane.hh LaneGrid.cc LaneGrid.hh LinearSegmentDesc.hh
		Object.cc Object.hh Road.cc Road.hh Serialization.cc
		Serialization.hh Spline.cc Spline.hh SplineEditor.py
		SuperDynamic.py Vector.cc Vector.hh Viewport.cc Viewport.hh
		XIntersection.cc XIntersection.hh YIntersection.cc
		YIntersection.hh libtrafsim.i libtrafsim.py
		libtrafsim_wrap.cc
	Deleted files: src/Car.cc src/Car.hh src/CarDriver.py
		src/CarFactory.cc src/CarFactory.hh src/CarProperties.cc
		src/CarProperties.hh src/Color.cc src/Color.hh
		src/CurveConstraint.cc src/CurveConstraint.hh
		src/Debug/makefile src/Debug/objects.mk
		src/Debug/postbuild.sh src/Debug/sources.mk
		src/Debug/subdir.mk src/DemExporter.py src/Doxyfile
		src/Environment.cc src/Environment.hh src/ExternalCar.cc
		src/ExternalCar.hh src/GenericApp.py src/IDM.cc src/IDM.hh
		src/Intersection.cc src/Intersection.hh src/Lane.cc
		src/Lane.hh src/LaneGrid.cc src/LaneGrid.hh
		src/LinearSegmentDesc.hh src/Object.cc src/Object.hh
		src/README src/Road.cc src/Road.hh src/Serialization.cc
		src/Serialization.hh src/Spline.cc src/Spline.hh
		src/SplineEditor.py src/SuperDynamic.py src/Vector.cc
		src/Vector.hh src/Viewport.cc src/Viewport.hh
		src/XIntersection.cc src/XIntersection.hh
		src/YIntersection.cc src/YIntersection.hh
		src/doctest/Color.i src/doctest/Environment.i
		src/doctest/Object.i src/doctest/Spline.i
		src/doctest/Vector.i src/doctest/Viewport.i
		src/libtrafsim.i src/libtrafsim.py src/libtrafsim_wrap.cc
	FILES: Makefile.yam(14453)
	changed makefile to work with src directory move

	FILES: README(14438)
	moved files up out of redundant 'src' directory

Thu Feb  1 20:17:53 2007	datamino (datamino)

	* version R1-00b
	BUGS: 
	New files: src/Car.cc src/Car.hh src/CarFactory.cc
		src/CarFactory.hh src/CarProperties.cc src/CarProperties.hh
		src/Color.cc src/Color.hh src/CurveConstraint.cc
		src/CurveConstraint.hh src/Environment.cc
		src/Environment.hh src/ExternalCar.cc src/ExternalCar.hh
		src/IDM.cc src/IDM.hh src/Intersection.cc
		src/Intersection.hh src/Lane.cc src/Lane.hh src/LaneGrid.cc
		src/LaneGrid.hh src/LinearSegmentDesc.hh src/Object.cc
		src/Object.hh src/Road.cc src/Road.hh src/Serialization.cc
		src/Serialization.hh src/Spline.cc src/Spline.hh
		src/Vector.cc src/Vector.hh src/Viewport.cc src/Viewport.hh
		src/XIntersection.cc src/XIntersection.hh
		src/YIntersection.cc src/YIntersection.hh
		src/libtrafsim_wrap.cc
	Deleted files: src/Car.cpp src/Car.h src/CarFactory.cpp
		src/CarFactory.h src/CarProperties.cpp src/CarProperties.h
		src/Color.cpp src/Color.h src/CurveConstraint.cpp
		src/CurveConstraint.h src/Environment.cpp src/Environment.h
		src/ExternalCar.cpp src/ExternalCar.h src/IDM.cpp src/IDM.h
		src/Intersection.cpp src/Intersection.h src/Lane.cpp
		src/Lane.h src/LaneGrid.cpp src/LaneGrid.h
		src/LinearSegmentDesc.h src/Object.cpp src/Object.h
		src/Road.cpp src/Road.h src/Serialization.cpp
		src/Serialization.h src/Spline.cpp src/Spline.h
		src/Vector.cpp src/Vector.h src/Viewport.cpp src/Viewport.h
		src/XIntersection.cpp src/XIntersection.h
		src/YIntersection.cpp src/YIntersection.h
		src/libtrafsim_wrap.cxx
	FILES: Makefile.yam(13673)
	Starting to fill Makefile.yam with all the source files and get it
	to work. Not finished.

	FILES: Makefile.yam(13980)
	First working version of the yam Makefile. This compiles the
	libtrafsim.so correctly, but you have to copy it manually in the
	directory of the python script to use it.

	FILES: Makefile.yam(13990)
	Added flags '-Wall -g3' (debugging, almost all warnings).

	FILES: libtrafsim.i(13674), makefile(13674), subdir.mk(13674)
	Renamed all the files from .cpp and .h into .cc and .hh as required
	by our coding standards. Fixed #includes and makefiles to reflect
	the changes.

	FILES: makefile(13942), objects.mk(13942), subdir.mk(13942)
	Serialization.hh: Added warning that says you need gcc-4.1.1 or a
	compiler with tr1/type_traits STL extension.
	makefile, subdir.mk, objects.mk: Added variable GXX = g++, replaces
	occurrences of g++ with $(GXX). Now you can type "make all
	GXX=g++-4.1.1" and select which compiler to use.

	FILES: objects.mk(13833), subdir.mk(13833)
	Removed dependency on libstlport, there seem to be no need for it
	instead of the default one (GNU libstdc++), and not all computers
	have it installed. This also avoid to have compatibility (linking)
	problems with other programs that don't use it.

	FILES: subdir.mk(13979)
	Removed CXX_DEPS (minor change).

Sun Jan 21 16:25:27 2007	Jessica Gonzalez (jengo)

	* version R1-00a
	BUGS: 
	New files: src/Car.cpp src/Car.h src/CarDriver.py
		src/CarFactory.cpp src/CarFactory.h src/CarProperties.cpp
		src/CarProperties.h src/Color.cpp src/Color.h
		src/CurveConstraint.cpp src/CurveConstraint.h
		src/Debug/makefile src/Debug/objects.mk
		src/Debug/postbuild.sh src/Debug/sources.mk
		src/Debug/subdir.mk src/DemExporter.py src/Doxyfile
		src/Environment.cpp src/Environment.h src/ExternalCar.cpp
		src/ExternalCar.h src/GenericApp.py src/IDM.cpp src/IDM.h
		src/Intersection.cpp src/Intersection.h src/Lane.cpp
		src/Lane.h src/LaneGrid.cpp src/LaneGrid.h
		src/LinearSegmentDesc.h src/Object.cpp src/Object.h
		src/README src/Road.cpp src/Road.h src/Serialization.cpp
		src/Serialization.h src/Spline.cpp src/Spline.h
		src/SplineEditor.py src/SuperDynamic.py src/Vector.cpp
		src/Vector.h src/Viewport.cpp src/Viewport.h
		src/XIntersection.cpp src/XIntersection.h
		src/YIntersection.cpp src/YIntersection.h
		src/doctest/Color.i src/doctest/Environment.i
		src/doctest/Object.i src/doctest/Spline.i
		src/doctest/Vector.i src/doctest/Viewport.i
		src/libtrafsim.i src/libtrafsim.py src/libtrafsim_wrap.cxx
	FILES: Car.cpp(12899), Car.h(12899), CarDriver.py(12899),
		CarFactory.cpp(12899), CarFactory.h(12899),
		CarProperties.cpp(12899), CarProperties.h(12899),
		Color.cpp(12899), Color.h(12899), Color.i(12899),
		CurveConstraint.cpp(12899), CurveConstraint.h(12899),
		DemExporter.py(12899), Doxyfile(12899),
		Environment.cpp(12899), Environment.h(12899),
		Environment.i(12899), ExternalCar.cpp(12899),
		ExternalCar.h(12899), GenericApp.py(12899), IDM.cpp(12899),
		IDM.h(12899), Intersection.cpp(12899),
		Intersection.h(12899), Lane.cpp(12899), Lane.h(12899),
		LaneGrid.cpp(12899), LaneGrid.h(12899),
		LinearSegmentDesc.h(12899), Object.cpp(12899),
		Object.h(12899), Object.i(12899), README(12899),
		Road.cpp(12899), Road.h(12899), Serialization.cpp(12899),
		Serialization.h(12899), Spline.cpp(12899), Spline.h(12899),
		Spline.i(12899), SplineEditor.py(12899),
		SuperDynamic.py(12899), Vector.cpp(12899), Vector.h(12899),
		Vector.i(12899), Viewport.cpp(12899), Viewport.h(12899),
		Viewport.i(12899), XIntersection.cpp(12899),
		XIntersection.h(12899), YIntersection.cpp(12899),
		YIntersection.h(12899), libtrafsim.i(12899),
		libtrafsim.py(12899), libtrafsim_wrap.cxx(12899),
		makefile(12899), objects.mk(12899), postbuild.sh(12899),
		sources.mk(12899), subdir.mk(12899)
	Initial source import

Sun Jan 21 15:45:32 2007	Jessica Gonzalez (jengo)

	* version R1-00
	Created trafsim module.

































