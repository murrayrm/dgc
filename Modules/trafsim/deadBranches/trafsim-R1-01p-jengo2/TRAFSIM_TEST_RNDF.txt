RNDF_name	TRAFSIM_TEST_RNDF	
num_segments	4	
num_zones	0	
format_version	1.0	
creation_date	11/27/2006


segment	1	
num_lanes	2	
segment_name	Right_Seg	
lane	1.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white	
checkpoint	1.1.3	1
stop    1.1.4 
exit	1.1.4	1.2.1
exit	1.1.4	3.2.1
exit	1.1.4	2.1.1
1.1.1   34.167477       -118.091000
1.1.2   34.167477       -118.092000
1.1.3   34.167477       -118.093000
1.1.4   34.167477       -118.094400
end_lane		
lane	1.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white
checkpoint	1.2.1	2
checkpoint	1.2.4	3
1.2.1   34.167381       -118.094400
1.2.2   34.167381       -118.093000
1.2.3   34.167381       -118.092000
1.2.4   34.167381       -118.091000
end_lane		
end_segment		

segment	2	
num_lanes	2	
segment_name	Left_Seg	
lane	2.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white	
checkpoint	2.1.2	4
2.1.1   34.167477       -118.094600
2.1.2   34.167477       -118.096000
2.1.3   34.167477       -118.097000
2.1.4   34.167477       -118.098000
end_lane		
lane	2.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white
checkpoint	2.2.3	5
stop    2.2.4
exit	2.2.4	4.1.1
exit	2.2.4	1.2.1
exit	2.2.4	3.2.1
2.2.1   34.167381       -118.098000
2.2.2   34.167381       -118.097000
2.2.3   34.167381       -118.096000
2.2.4   34.167381       -118.094600
end_lane		
end_segment



segment	3	
num_lanes	2	
segment_name	Top_Seg	
lane	3.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white	
checkpoint	3.1.2	6
exit		3.1.4	2.1.1
exit		3.1.4	4.1.1
exit		3.1.4	1.2.1
3.1.1   34.170929       -118.094548
3.1.2   34.169929       -118.094548
3.1.3   34.168929       -118.094538
3.1.4   34.167529       -118.094538
end_lane		
lane	3.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white
checkpoint	3.2.3	7
3.2.1   34.167529       -118.094452
3.2.2   34.168929       -118.094452
3.2.3   34.169929       -118.094452
3.2.4   34.170929       -118.094452
end_lane		
end_segment		


segment	4	
num_lanes	2	
segment_name	Bottom_Seg	
lane	4.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white	
checkpoint	4.1.2	8
4.1.1   34.167329       -118.094548
4.1.2   34.165929       -118.094548
4.1.3   34.164929       -118.094548
4.1.4   34.163929       -118.094548
end_lane		
lane	4.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white
checkpoint	4.1.3	9
exit		4.2.4	3.2.1
exit		4.2.4	1.2.1
exit		4.2.4	2.1.1
4.2.1   34.163929       -118.094452
4.2.2   34.164929       -118.094452
4.2.3   34.165929       -118.094452
4.2.4   34.167329       -118.094452
end_lane		
end_segment		



end_file
