#ifndef __SPLINECAR__HH__
#define __SPLINECAR__HH__

#include "Object.hh"
#include "Spline.hh"

namespace TrafSim {


  /** SplineCars do not follow lanes or obey traffic rules. Instead,
   * they simply follow their spline over and over again.
   * In this way, cars with arbitrary motion can be created and
   * used in places like zones and parking lots, where the traffic
   * rules are arbitrary 
   */
  class SplineCar: public Object 
  {

  public:

    /// Constructor
    SplineCar();

    ~SplineCar();

    /// Initializes the SplineCar with a given spline
    void initialize(Spline s);

    /// Gives the name of the class (SplineCar)
    virtual std::string getClassID() const { return "SplineCar"; }

    /// Renders the object. It will be called when the client
    /// application calls Environment::draw().
    virtual void draw();

    /// Updates the object's state. 
    /// \param ticks The number of seconds elapsed since the last call to simulate()
    /// \return False if the object should be removed from the Environment and deleted.
    virtual bool simulate(float ticks);


    /* ****** ACCESSORS INHERITED FROM OBJECT ***** */

    /// \return the x,y position of the center of the SplineCar, in meters
    virtual point2 getCenter() { return point2(position.x,position.y); }
    /// \return the velocity of the SplineCar, in m/s
    virtual float getVelocity() { return velocity; }
    /// \return the orientation of the SplineCar, in radians from -pi to pi
    virtual float getOrientation() { return orientation; }

    /// \return the length (in y-dir) of the SplineCar, in meters
    virtual float getLength() { return size.y; }
    /// \return the width (in x-dir) of the SplineCar, in meters
    virtual float getWidth() { return size.x; }

    /// Accessor for generic boundary of an SplineCar
    /// \return a set of 2D points that form the SplineCar's boundary
    virtual std::vector<vector<point2> > getBoundary();
      
    /// Cars can't contain objects
    /// \return false
    virtual bool containsObject(Object* obj) { return false; }

    
    /// Inverts the color of the car
    virtual bool invertColor();

    /// NOT IMPLEMENTED
    /// \param os The output stream to write to
    virtual void serialize(std::ostream& os) const;

    /// NOT IMPLEMENTED
    /// \param is The input stream to read from
    virtual void deserialize(std::istream& is);


    /* *** HELPER FUNCTIONS *****/		

    /// A helper function, for Python's benefit. Python doesn't understand downcasting, so
    /// this function does it explicitly so that objects extracted from an Environment can be used.
    /// \param source The object pointer to be cast down to a SplineCar
    /// \return A SplineCar*, pointing to the same object as source.	
    static SplineCar* downcast(Object* source);
    
  private:

    Color color;
    Vector size;

    Spline spline;
    float splineLength;

    Vector position; 
    Vector orientationVector;
    float orientation;    
    float velocity;		// linear velocity

    float splineT; // our current position along the spline
        
    bool hasSpline;

    double tickMultiplier;

  };

}

#endif
