//#include "interfaces/StateClient.hh"
#include "rndf/RNDF.hh"

#include "Object.hh"
#include "Vector.hh"


//Define flags for use internally.
//APPEND_DIST may need to be modified. It appends a segment with
//another point colinear with previous two points at a distance
//APPEND_DIST away.
#define APPEND_START 0x1
#define APPEND_END   0x2
#define APPEND_DIST  5


namespace TrafSim
{


//Make sure SWIG doesn't explode on the new classes!


//Functions to move over:
//

//Moved functions to test:
//getPointsFromLane(int lane);
//setLaneInfo();
//appendStartEnd(..); //Should be fine, nothing actually changed.
//setCenterLine()
class IntersectionInfo;

//Define structures in order to organize the segment and intersection data.
class SegInfo {
    public:
    Segment* seg; //Pointer to segment in rndf itself.
    vector< Lane* > rawLanes; //Lanes in the rndf itself.
    Vector laneInfo; //(forward lanes, reverse lanes).
    vector< Vector > centerLine; //Points in the center line to be used in trafsim.
    vector< IntersectionInfo* > intersections; //Pointer to the intersections that this segment is in.
    
    //Set the number of lanes in the forward and reverse directions.
    void setLaneInfo();
                        
    //Returns a vector of Vectors for the points in a requested lane.
    vector< Vector> getPointsFromLane(int lane);
    
    //Calculate the center line of this segment.
    void setCenterLine();
};


class IntersectionInfo {
    public:
    Vector intersectLoc; //Location in (x, y) of the center of the intersection.
    vector< SegInfo* > relevantSegs; //vector of pointers to the segments that are in this intersection.
};


class RNDFSimIF
{
    public:
    RNDF* rndf;       
    
    RNDFSimIF();
    ~RNDFSimIF();
    
    //////////////////Functions to return data to Trafsim///////////////
    //Parse in the RNDF file.
    bool loadFile(char* filename);
    
    //Return the number of segments in the loaded RNDF file. For use
    //in building the RNDF in the python environment.
    int getNumSegs();

    //Return the number of lanes in 'segment'.
    int getNumLanes(int segment);

    //Return the lane infos. For use in trafsim. //TEST FIX FOR NEW STRUCTURE.
    std::vector<Vector> getLaneInfos();
    
    //Get the amount that the map was translated in the x direction.
    double getxTranslate();

    //Get the amount that the map was translated in the y direction.
    double getyTranslate();

    //Return center line of (seg). //FIX FOR NEW STRUCTURE.
    std::vector< Vector > getCenterLineOfSeg(int seg);
    

    ////////////////Functions for initialization and helping//////////////
    //Build up the segment info vector.
    void buildSegInfos();

    //Northing and Easting values are disgustingly too large for
    //the simulator to easily handle. Center these values so that it
    //can be used.
    void centerAllWaypoints();
    
    //Append the start of/end of/or both a segment. I'm leaving
    //this in RNDFSimIF because the segment itself shouldn't
    //mess with this, as ONLY trafsim needs this modification.
    //Flags: APPEND_START APPEND_END
    void appendStartEnd(std::vector<Vector> *points,
                        int flags=APPEND_START | APPEND_END);
    

    int numSegs() { return allSegs.size(); }

    private:
    //Safety function to ensure that there is an RNDF loaded.
    bool warn();
    
    //Boolean that tells whether or not an RNDF is loaded.
    bool loaded;
    
    //Store the xTranslate and yTranslate values, as obtained by
    //centerAllWaypoints() so that conversions can be made back to
    //Northing and Easting for closed loop simulation.
    double xTranslate, yTranslate; 

    //Store the center lines for ease of collection.
    std::vector< std::vector< Vector > > centerLines;
    
    //Store all the segments.
    vector< SegInfo > allSegs;
    
    //Store all the intersection information.
    vector< IntersectionInfo > intersections;
};


}


