#include "Environment.hh"
#include <sstream>
#include <cassert>

namespace TrafSim
{
	
/* ******* CLASS ENVIRONMENT ******* */	
	
/* ******* CONSTRUCTORS AND DESTRUCTORS ******* */	


Environment::Environment() : CSkynetContainer(MODmapping,atoi(std::getenv("SKYNET_KEY"))),
  bottomLeftBound(0.0f, 0.0f),
  topRightBound(100.0f, 100.0f) { 
  skynetKey = atoi(std::getenv("SKYNET_KEY"));
  std::cout << "Constructing skynet through default constructor with KEY = " <<skynetKey << std::endl;
}

Environment::Environment(int skynet_key) : 
  CSkynetContainer(MODmapping,skynet_key),
  bottomLeftBound(0.0f, 0.0f),
  topRightBound(100.0f, 100.0f)
{ 
  // store the skynet key so we can use it in ExternalCar, etc
  skynetKey = skynet_key;
  std::cout << "Constructing skynet with KEY = " << skynetKey << std::endl;
}


/* ******* DIRECTORY FUNCTIONS ******* */

std::string const& Environment::addObject(Object* object, std::string const& proposedName)
{
	// generate a unique name for the object
	if (proposedName == "")
		object->name = _generateName(object->getClassID());
	else
		object->name = _generateName(proposedName);
		
	// store the object in both directories
	typeDirectory[object->getClassID()][object->name] = object;
	nameDirectory[object->name] = object;

	return object->name;
}

bool Environment::removeObject(std::string const& objName, bool canDelete)
{
	
  Object* object = nameDirectory[objName];
  
	if (!object)
	{
		nameDirectory.erase(objName);
		return false;
	}
		
	// erase the object from both directories
	nameDirectory.erase(objName); std::cout<<"object erased from name dir"<<endl;
	typeDirectory[object->getClassID()].erase(objName); std::cout<<"object erased from type dir"<<endl;	

	if (canDelete)
	  delete object;
	std::cout<<"object deleted"<<endl;
	return true;
}

std::string Environment::renameObject(std::string const& oldName, std::string const& newName)
{
	Object* object = nameDirectory[oldName];
	
	if (!object)
	{
		nameDirectory.erase(oldName);
		return "";
	}

	removeObject(oldName, false);
	addObject(object, newName);
	
	return object->getName();
}

Object* Environment::getObject(std::string const& objName)
{
	Object* result = nameDirectory[objName];
	
	if (!result)
		nameDirectory.erase(objName);
		
	return result;
}

ObjectMap const& Environment::getObjectsByClassID(std::string const& classID)
{
	return typeDirectory[classID];
}


void Environment::clearObjectsByClassID(std::string const& classID,
                                        bool canDelete)
{
        ObjectMap *objMap = &typeDirectory[classID];
        for (ObjectMap::iterator i = objMap->begin();
                i != objMap->end();
                ++i)
        {
                nameDirectory.erase(i->second->getName());
                if(canDelete)
                        delete i->second;
        }
        typeDirectory.erase(classID);
}

void Environment::clearObjects(bool canDelete)
{
	if (canDelete)
	{
		for (ObjectMap::iterator i = nameDirectory.begin();
			 i != nameDirectory.end();
			 ++i)
		{
			delete i->second;
		}
	}
	
	nameDirectory.clear();
	typeDirectory.clear();
	nameList.clear();
}

/* ******* REFERENCE FRAME FUNCTIONS ******* */

Vector const& Environment::getBottomLeftBound() const
{
	return bottomLeftBound;
}
	
Vector const& Environment::getTopRightBound() const
{
	return topRightBound;
}

void Environment::setBounds(Vector const& bottomLeft, Vector const& topRight)
{
	bottomLeftBound = bottomLeft;
	topRightBound = topRight;
	
	assert(bottomLeftBound.x < topRightBound.x &&
		   bottomLeftBound.y < topRightBound.y);
}

/* ******* SIMULATION FUNCTIONS ******* */
	
void Environment::draw()
{
	for (ObjectMap::iterator i = nameDirectory.begin();
		 i != nameDirectory.end();
		 ++i)
	{
		i->second->draw();
	}
}

void Environment::simulate(float ticks)
{

	// if an object's simulate() function returns False,
	// that means it's asking to be deleted.
	for (ObjectMap::iterator i = nameDirectory.begin();
		 i != nameDirectory.end();)
	{
		Object* object = (i++)->second;

		if (!object->simulate(ticks))
			removeObject(object->name);
		  
	}
	

	// send appropriate environment objects to the mapper
	exportEnv();
}

void Environment::exportEnv()
{
  updateAliceState();
  exportRoadLines();
  exportCars();

}

void Environment::exportCars()
{

  // export all of the car objects
  ObjectMap cars = getObjectsByClassID("Car");

  MapElement obj;
  string name = "";
  double angle, length, width, posX, posY;
  int bytesSent = 0;
  for (ObjectMap::iterator i = cars.begin(); i != cars.end();)
    {
      obj.clear();

      Object* object = (i++)->second;

      name = object->getName();
      // don't want to export Alice
      if (name != "Alice") {
	
	// convert string to an integer
	vector<int> ident;
	ident.push_back(int(name.c_str()));
	
	//std::cout<<endl<<id<<endl;
	
	// get car properties
	length = object->getLength();
	width = object->getWidth();
	//	std::cout<<"l,w: "<<length<<" , "<<width<<endl;
	angle = object->getOrientation();
	
	// get car position and velocity
	posX = object->getCenter().x;
	posY = object->getCenter().y;
	//	std::cout<<"position: "<<posX<<" , "<<posY<<endl;
	double velocity = object->getVelocity();
	double theta = object->getOrientation();
	obj.velocity.x = velocity * cos(theta);
	obj.velocity.y = velocity * sin(theta);
	//	std::cout<<"velocity, orientation "<<velocity<<" , "<<velocity<<" , ";
	//	std::cout<<theta<<endl;
	
	// initialize the object
	obj.set_block_obs(ident, point2(posX+xTranslate,
	                                 posY+yTranslate), angle, length, width);
	obj.set_alice(aliceState);
	
	// send the object
	initSendMapElement(skynetKey);
	bytesSent = 0;
	bytesSent = sendMapElement(&obj);
	
//	std::cout<< "bytes sent = " << bytesSent << endl;
//	std::cout<<"object id = " <<obj.id.front() <<endl;
      }
     
    }

}

void Environment::exportRoadLines()
{
  
  // can't export road lines if we don't have Alice on the road
  if (!getObject("Alice")) {
    //std::cout<<"ERROR: No Alice vehicle. Cannot export road lines."<<endl;
  }

  else {

    // the lane we're sending`
    MapElement obj;
    string name = "";
    int bytesSent;
    vector< vector<point2> > boundaries;
    vector<point2> currentBoundary;
    int currentLaneNumber;

    // figure out what lane Alice is in
    Object* alice = getObject("Alice");
    ObjectMap lanes = getObjectsByClassID("Lane");
    Lane* aliceLane;
    bool laneFound = false;
    for (ObjectMap::iterator i = lanes.begin(); i != lanes.end();)
      {
	Lane* lane = static_cast<Lane*>((i++)->second);
	if (lane->containsObject(alice)) {
	  aliceLane = lane;
	  laneFound = true;
	  break;
	}
      }
    
    // if alice isn't near a lane, there are no boundaries to give
    if (!laneFound)
      return;	

    // search the roads in the environment to find which one has this lane
    ObjectMap roads = getObjectsByClassID("Road");
    
    for (ObjectMap::iterator i = roads.begin(); i != roads.end();)
      {
	Road* road = static_cast<Road*>((i++)->second);

	//	std::cout<<"Road "<<road->getName()<<endl;

	if (road->containsObject(aliceLane)) {

	  //	  std::cout<<"Road has alice lane"<<endl;

	  // get the road boundaries
	  boundaries = road->getLaneBoundaries(.01);
	  currentLaneNumber = 0;
	  for (vector< vector<point2> >::iterator laneIter = boundaries.begin();
	       laneIter != boundaries.end(); 
	       laneIter++) {

	    for(vector<point2>::iterator pointIter = (*laneIter).begin();
	        pointIter != (*laneIter).end();
		pointIter++) {
	
	        (*pointIter).x += xTranslate;
		(*pointIter).y += yTranslate;
	    }
	    
	    obj.clear();
	    bytesSent = 0;
	    currentLaneNumber++;

	    currentBoundary = *laneIter;

	    name = road->getName();
	    // convert string to an integer
	    obj.set_id(int(name.c_str())+currentLaneNumber);


	    // initialize the object
	    obj.set_laneline();
	    obj.set_geometry(currentBoundary);
	    // WARNING: GLOBAL FRAME
	    //	    obj.set_frame_global();
	    obj.set_alice(aliceState);

	    // send the object
	    initSendMapElement(skynetKey);
	    bytesSent = 0;
	    bytesSent = sendMapElement(&obj);
	  
	    //std::cout<< "bytes sent = " << bytesSent << endl;
	    //std::cout<<"object id = " <<obj.id.front() <<endl;

	  }
	}
      }
  }
}


/* ******* PRIVATE FUNCTIONS ******* */

std::string Environment::_generateName(std::string const& proposedName)
{
	if (nameList.find(proposedName) == nameList.end())
	{
		nameList[proposedName] = 1;
		return proposedName;
	}
	
	// if "name" is in use, return "name___n", where "n" is an integer
	std::ostringstream ostr;
	ostr << proposedName << "___" << nameList[proposedName]++;
	
	return ostr.str();
}

/* ******* SERIALIZATION FUNCTIONS ******* */

std::ostream& operator<<(std::ostream& os, Environment const& env)
{
	return os << "Environment( " << std::endl << 
		"bottomLeftBound --> " << env.bottomLeftBound << std::endl <<
		"topRightBound --> " << env.topRightBound << std::endl <<
		"nameDirectory --> " << env.nameDirectory << std::endl <<
		"nameList --> " << env.nameList << std::endl <<
		" )";	
}

std::istream& operator>>(std::istream& is, Environment & env)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	try
	{
		matchString(is, "Environment");
		matchString(is, "(");
		DESERIALIZE_RN(is, "bottomLeftBound", env.bottomLeftBound);
		DESERIALIZE_RN(is, "topRightBound", env.topRightBound);
		DESERIALIZE_RN(is, "nameDirectory", env.nameDirectory);
		DESERIALIZE_RN(is, "nameList", env.nameList);
		matchString(is, ")");
	}
	catch(ParseError& e)
	{
		e.info = "Failed to parse Environment:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		throw ParseError("Failed to parse Environment:\n " + std::string(e.what()) );
	}
	
	env.typeDirectory.clear();
	
	for (ObjectMap::iterator i = env.nameDirectory.begin();
	 i != env.nameDirectory.end();
	 ++i)
	{
		Object* object = i->second;	
		object->name = i->first;    // reload the object's name
		
		// store the object in the classID index as well
		env.typeDirectory[object->getClassID()][object->name] = object;
		
		// replace string references with actual pointers
		object->_resolveDependencies(env);
	}
	
	return is;	
}

/* ******* CLASS TESTOBJECT ******* *

TestObject::TestObject(): reference(0) 
{
}

std::string TestObject::getClassID() const 
{ 
	return "TestObject"; 
}
	
void TestObject::draw() 
{ 
	std::cout << "Called " << this->getName() << ".draw()\n";
}
	
bool TestObject::simulate(float ticks)
{
	std::cout << "Called " << this->getName() << ".simulate()\n";
	return ticks < 1.0f;
}
	
void TestObject::serialize(std::ostream& os) const
{
	if (!reference)
		os << "reference --> None";
	else
		os << "reference --> " << reference->getName();
}
	
void TestObject::deserialize(std::istream& is)
{
	std::string reference;
	DESERIALIZE(is, reference);
	
	// add the string reference, to be resolved later (provided that
	// this TestObject is being loaded as part of an Environment, not
	// on its own).
	this->_addReference(reference, &(this->reference));
}
	
TestObject* TestObject::downcast(Object* source)
{
	return dynamic_cast<TestObject*>(source);
}

end TestObject stuff */
		
}
