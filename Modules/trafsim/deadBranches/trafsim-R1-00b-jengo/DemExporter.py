import Image, ImageDraw
from libtrafsim import *

def ExportRoadData(environment, corner, dimensions, scale, filename, increment = 0.01):
    corner = Vector(corner[0], corner[1])
    im = Image.new('L', (dimensions[0], dimensions[1]), 0)
    draw = ImageDraw.Draw(im)
    
    for obj in environment.getObjectsByClassID('Road').values():
        road = Road.downcast(obj)
        polyPart1 = []
        polyPart2 = []
        
        t = 0.0
        while t < 1.0 + increment:
            point1 = ( road.getSpline().evalCoordinate(t) - corner ) + \
                      road.getSpline().evalNormal(t) * road.getLaneWidth() * road.numForwardLanes() 
            point2 = ( road.getSpline().evalCoordinate(t) - corner ) + \
                      road.getSpline().evalNormal(t) * -road.getLaneWidth() * road.numReverseLanes() 
           
            pixel1 = ( int( point1.x / scale ),
                          dimensions[1] - int( point1.y / scale ) )
            pixel2 = ( int( point2.x / scale ),
                          dimensions[1] - int( point2.y / scale ) )
            
            polyPart1.append(pixel1)
            polyPart2.append(pixel2)

            t += increment
        
        polyPart2.reverse()
        draw.polygon( polyPart1 + polyPart2, fill = 255 )

    for interType in (XIntersection, YIntersection):            
        for obj in environment.getObjectsByClassID(interType().getClassID()).values():
            inter = interType.downcast(obj)
            
            splineArray = SplineArray()
            inter.getBoundaries(splineArray)
            poly = []
            
            for spline in splineArray:
                t = 0
                while t < 1.0:
                    point = spline.evalCoordinate(t) - corner
                    pixel = ( int( point.x / scale ), 
                              dimensions[1] - int( point.y / scale ) )
                    
                    poly.append(pixel)
                    if t > 1.0: t = 1.0
                    else: t += increment
        
            draw.polygon( poly, fill = 255 ) 
                
    del draw
    im.save(filename)
    del im

    return True

def MergeImageIntoDem(dem, road_image_file, indent = 0.4):
    data = Image.open(road_image_file)

    if data.size != ( dem.numSamples(), dem.numLines() ):
        raise TypeError( 'Dem size does not match image size!' )

    new_dem = dem.copy()

    for i in xrange(data.size[0]):
        for j in xrange(data.size[1]):
            if data.getpixel( (i, j) ) == 255:
                height = new_dem.heightAtPixel( i, j )
                new_dem.heightAtPixel( i, j, height - indent )

    return new_dem
