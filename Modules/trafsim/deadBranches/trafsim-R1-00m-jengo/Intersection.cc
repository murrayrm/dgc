#include "Intersection.hh"

namespace TrafSim
{

Intersection::Intersection()
{
}

Intersection::~Intersection()
{
}

Intersection* Intersection::downcast(Object* source)
{
	return dynamic_cast<Intersection*>(source);
}

std::vector<point2> DeadEnd::getBoundary()
{
  vector<point2> v2;
  v2.push_back(point2(0,0));
  return v2; 
}

}
