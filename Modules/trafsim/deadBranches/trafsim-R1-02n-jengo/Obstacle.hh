#ifndef OBSTACLE__HH__
#define OBSTACLE__HH__

#include "Object.hh"
#include "Color.hh"

namespace TrafSim {

  enum ObstacleType { BLOCK, CIRCLE, VEGETATION };

   /**
    * The Obstacle class is used to create static obstacles of arbitrary
    * size and geometry. 
    */
  class Obstacle: public Object
  {
  public:
    
    /* ******** CONSTRUCTORS AND DESTRUCTORS ******/

    /**
     * By default, creates a block obstacle with size 2 meters by 
     * 2 meters and orientation of 0 radians
     */
    Obstacle();
    /**
     * Creates a block obstacle with the given position,
     * length, width, and orientation
     * \param x The x (northing) coord
     * \param y The y (easting) coord
     * \param l The length of the obstacle
     * \param w Thy width of the obstacle
     * \param o The orientation of the obstacle
     * \param v Whether or not to create vegetation obs
     */
    Obstacle(float x, float y, float l, float w, float o, bool v = false);
    /**
     * Creates a circle obstacle with the given radius and position
     * \param x The x (northing) coord
     * \param y The y (easting) coord
     * \param radius The radius of the obstacle
     */
    Obstacle(float x, float y, float radius);
    /**
     * Destructor.
     */
    virtual ~Obstacle();
   
    /* ***** FUNCTIONS ********/
    /**
     * Gives the class ID of the object
     * \return "Obstacle"
     */
    virtual std::string getClassID() const { return "Obstacle"; }

    /**
     * Draw the boundary of the obstacle
     */
    virtual void draw();

    virtual bool invertColor();

    /** 
     * Sets the position of a obstacle.
     * \param x The x (northing) coordinate
     * \param y The y (easting) coordinate
     */
    void setPosition(float x, float y);
    /** 
     * Sets the position of a obstacle.
     * \param pos The new position of the obstacle
     */
    void setPosition(point2 pos);
    /** 
     * Sets the orientation of the obstacle
     * \param newOrientation The new orientation of the obstacle, 
     * from -pi to pi
     */
    void setOrientation(float newOrientation);

    /** 
     * Sets the geometry of the obstacle to a block
     */
    void setBlockObs();
    /**
     * Sets the geometry of the obstacle to a circle
     */
    void setCircleObs();
    /**
     * Sets the obstacle to vegetation
     */
    void setVegetation();

    /**
     * Since obstacles are static by default, this function doesn't
     * do very much.
     * \param ticks Amount of time since the last call to simulate(), 
     * in seconds.
     * \return Always true.
     */
    virtual bool simulate(float ticks);
    	

    /* ********* ACCESSORS INHERITED FROM OBJECT *******/
    
    /// \return the location of the center of the obstacle
    point2 getCenter() { return position; }
    /** 
     * Static obstacles have no velocity
     * \return 0
     */
    float getVelocity() { return 0; }
    /**
     * \return The obstacle's orientation
     */
    float getOrientation() { return orientation; }
    /**
     * \return The length of the obstacle, in meters
     */
    float getLength() { return length; }
    /**
     * \return The width of the obstacle, in meters
     */
    float getWidth() { return width; }
    /**
     * \return A set of points describing the car's boundary
     */
    vector<vector<point2> > getBoundary();
    /** Obstacles can't contain objects
     * \return false
     */
    bool containsObject(Object* obj) { return false; }

    /* *** HELPER FUNCTIONS *****/		

    /// A helper function, for Python's benefit. Python doesn't understand downcasting, so
    /// this function does it explicitly so that objects extracted from an Environment can be used.
    /// \param source The object pointer to be cast down to a Car
    /// \return A Car*, pointing to the same object as source.	
    static Obstacle* downcast(Object* source);
    
  protected:

        
    /// Writes the object to the output stream, in standard format.
    /// NOT IMPLEMENTED
    /// \param os The stream to write to.
    virtual void serialize(std::ostream& os) const;
    
    /// Reads the object from the input stream, in standard format.
    /// NOT IMPLEMENTED
    /// \param is The stream to read from.
    virtual void deserialize(std::istream& is);
    
    float orientation; // 2d heading angle
    point2 position;   // world position vector
    point2 posDiff;   // if we've moved...
    float length;
    float width;
    ObstacleType type;
    Color color;
    vector<point2> boundary;
  };
  
}

#endif /*OBSTACLE__HH__*/
