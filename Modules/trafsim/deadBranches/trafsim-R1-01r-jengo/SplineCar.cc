#include "SplineCar.hh"
#include <GL/gl.h>
#include <cmath>

namespace TrafSim {

  SplineCar::SplineCar()
  {
    hasSpline = false;
    color = Color(1.0f, 0.0f, 0.0f, 1.0f);
    size = Vector(2,5);
    velocity = 0;
    orientationVector = Vector(0,0);
    orientation = 0;
    position = Vector(0,0);
    splineT = 0;

    paused = false;
    speedingUp = false;
    slowingDown = false;
    reversed = false;
    tickMultiplier = 1;
  }

  SplineCar::~SplineCar()
  {
  }

  void SplineCar::initialize(Spline s)
  {

    hasSpline = true;

    // copy the spline
    spline = Spline(s,0);

    splineLength = spline.getLength();
    velocity = 2.0;
    position = spline.evalCoordinate(splineT);
    orientationVector = spline.evalNormal(splineT);
    orientation = atan2(orientationVector.y,orientationVector.x);
  }

  void SplineCar::draw()
  {
    if (hasSpline) {

      //      cout<<"position: "<<position.x<<" "<<position.y<<endl;
      //      cout<<"orientation: "<<orientation<<endl;
      //      cout<<"spline name: "<<spline.getName()<<endl;

      // draw a colored rectangle
	
      glPushMatrix();
      glTranslatef(position.x, position.y, 0.0f);
      glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
      glSetColor(color);
      
      glBegin(GL_POLYGON);
      glVertex3f(-size.x * 0.5f, -size.y * 0.5f, 0.0f);
      glVertex3f(-size.x * 0.5f,  size.y * 0.5f, 0.0f);
      glVertex3f( size.x * 0.5f,  size.y * 0.5f, 0.0f);
      glVertex3f( size.x * 0.5f, -size.y * 0.5f,  0.0f);
      glEnd();
    
      glPopMatrix();

    }
    else {
      //      cout<<"hasSpline false"<<endl;
    }

  }

  bool SplineCar::simulate(float ticks)
  {

    if (hasSpline) {

      if (!paused) {
	
        if (speedingUp) {
	  tickMultiplier *= 2;
	  speedingUp = false;
	}
	else if (slowingDown) {
	  tickMultiplier *= .5;
	  slowingDown = false;
	}
      
	ticks *= tickMultiplier;
	
	//	cout<<"splineT: "<<splineT<<endl;
	//	cout<<"ticks: "<<ticks<<endl;
	//	cout<<"splineLength: "<<splineLength<<endl;

	if (reversed) {

	  // if we've made it to the end of the spline, start
	  // over at the beginning
	  if (splineT == 0) {
	    splineT = 1;
	  }
	  
	  // determine next location along the spline
	  else {
	    splineT -= velocity * ticks / splineLength;
	    if (splineT < 0)
	      splineT = 1;
	  }	  
	}
	
	else { // not reversed

	  if (splineT == 1) {
	    splineT = 0;
	  }

	  else {
	    splineT += velocity * ticks / splineLength;
	    if (splineT > 1)
	      splineT = 1;
	  }
	}
      
	
	// convert spline param to a world coordinate
	position = spline.evalCoordinate(splineT);
	orientationVector = spline.evalNormal(splineT);
	orientation = atan2(orientationVector.y,orientationVector.x);
	
      }
    }
    return true;

  }

  std::vector<vector<point2> > SplineCar::getBoundary()
  {
    // return the four corners of the car
   
    std::vector<point2> boundary;
    double ly = size.y;
    double lx = size.x;
    double x = position.x;
    double y = position.y;
    double t = getOrientation();

    // put in the points at origin
    boundary.push_back(point2(.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,-.5*ly));
    boundary.push_back(point2(.5*lx,-.5*ly));

    double oldY, oldX;
    // rotate points and translate
    for (std::vector<point2>::iterator iter = boundary.begin();
	 iter != boundary.end(); iter++) {
      oldX = iter->x;
      oldY = iter->y;
      iter->x = oldX*cos(t) - oldY*sin(t) +x;
      iter->y = oldX*sin(t) + oldY*cos(t) +y;
    }
  
    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    return boundaryWrapper;
  }

  bool SplineCar::invertColor() {
    color.r = 1.0f - color.r;
    color.g = 1.0f - color.g;
    return true;
  }

  /// NOT IMPLEMENTED
  void SplineCar::serialize(std::ostream& os) const { }

  /// NOT IMPLEMENTED
  void SplineCar::deserialize(std::istream& is) { }

  SplineCar* SplineCar::downcast(Object* source)
  {
    return dynamic_cast<SplineCar*>(source);
  }


}
