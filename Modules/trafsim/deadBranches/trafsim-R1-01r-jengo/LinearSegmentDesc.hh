#ifndef LINEARSEGMENTDESC_H_
#define LINEARSEGMENTDESC_H_

#include <string>
#include <iostream>
#include "Serialization.hh"

namespace TrafSim 
{

/**
 * Given a type of linear object, such as a spline or a lane, this template
 * represents a section of that linear object, used for various purposes.
 */
template<class SegmentType>
struct LinearSegmentDesc
{
	/// Default constructor does nothing.
	LinearSegmentDesc() {}
	
	/// Trivial constructor to initialize all the struct members.
	LinearSegmentDesc(SegmentType* _segment, float _t_start, float _t_end):
		segment(_segment), t_start(_t_start), t_end(_t_end) { }
	
	/// The linear object being described	
	SegmentType* segment;
	
	/// A string for deserialization purposes
	std::string segmentName;
	
	/// The lower bound of the represented segment
	float t_start;
	
	/// The upper bound of the represented segment
	float t_end;
	
	/// Serialization
	friend std::ostream& operator<<(std::ostream& os, LinearSegmentDesc<SegmentType> const& desc)
	{
		return os << "SegmentDesc( " << desc.t_start << " , " << desc.t_end << " , " << desc.segment->getName()
				  << " ) ";
	}
	
	// Deserialization
	friend std::istream& operator>>(std::istream& is, LinearSegmentDesc<SegmentType>& desc)
	{
		is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
		desc.segment = (SegmentType*)0;
		
		try
		{
			matchString(is, "SegmentDesc");
			matchString(is, "(");
			is >> desc.t_start;
			matchString(is, ",");
			is >> desc.t_end;
			matchString(is, ",");
			is >> desc.segmentName;
			matchString(is, ")");
		}
		catch(ParseError& e)
		{
			e.info = "Failed to parse LinearSegmentDesc:\n" + e.info;
			throw;
		}
		catch(std::exception& e)
		{
			throw ParseError("Failed to parse LinearSegmentDesc:\n" + std::string(e.what()) );
		}
		
		return is;
	}
	
};

}

#endif /*LINEARSEGMENTDESC_H_*/
