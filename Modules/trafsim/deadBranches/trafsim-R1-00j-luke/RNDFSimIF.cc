#include "RNDFSimIF.hh"
#include <cmath>

namespace TrafSim
{

RNDFSimIF::RNDFSimIF() {
    rndf = new RNDF();
    
    //Initialize translations to zero.
    xTranslate = 0;
    yTranslate = 0;

}

RNDFSimIF::~RNDFSimIF() {
    delete rndf;
}

bool RNDFSimIF::loadFile(char* filename) {
   return rndf->loadFile(filename);
}

double RNDFSimIF::getxTranslate() {
    return xTranslate;
}

double RNDFSimIF::getyTranslate() {
    return yTranslate;
}

int RNDFSimIF::getNumSegs() {
    return rndf->getNumOfSegments();
}

int RNDFSimIF::getNumLanes(int segment) {
    vector<Segment*> segs = rndf->getAllSegments();
    vector<Segment*>::iterator segIter;
    int counter = 1;

    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        if (counter == segment)
	    return (*segIter)->getNumOfLanes();
        else
	    counter++;
    }

    //If it gets here, something screwed up.
    return 0;
}

std::vector<Vector> RNDFSimIF::getLaneInfos() {
    return laneInfo;
}

std::vector< Vector > RNDFSimIF::getCenterLineOfSeg(int seg) {
    appendStartEnd(&centerLines.at(seg), APPEND_START|APPEND_END);
    return  centerLines.at(seg);
}
  

//Return the points for all of the segments for use in trafsim.
void RNDFSimIF::setCenterLines() {
    std::vector<Segment*> segs = rndf->getAllSegments();
    std::vector<Segment*>::iterator segIter;

    //Keep track of where we are.
    int counter = 1;

    //Iterate through all the segments, setting up the points.
    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        //If there are no 'reverse' lanes. Currently will not
	//account for necessary displacement. May not be necessary
	//to displace. Check on this.
	//CURRENT THEORY: OK if there is only one lane. If there are multiple
	//lanes, need to average points accordingly. Odd number of lanes, take
	//middle lane.
	
	if ( (laneInfo[counter-1]).y == 0) {
	    centerLines.push_back(getPointsFromLane(counter,
	                                       (int)(laneInfo[counter-1]).x));
        }
        //If there are lanes in the opposite direction, average the points
	//between the middle lanes.
	else {
            vector<Vector> lane1 = getPointsFromLane(counter,
	                                     (int)(laneInfo[counter-1]).x);
	    vector<Vector> lane2 = getPointsFromLane(counter,
	                                     (int)(laneInfo[counter-1]).x + 1);
	    
	    //Iterate through the vectors and average points.
            vector<Vector>::iterator iter1, iter2;
	   // iter1 = lane1.begin();
	    iter2 = lane2.end();
	    iter2--;


	    for(iter1=lane1.begin(); iter1 != lane1.end(); iter1++) {
	        (*iter1)+=(*iter2);
		(*iter1) /= 2;
		                         
                if(iter2 != lane2.begin())
		    iter2--;
	    }
	    centerLines.push_back(lane1);
	}
        counter++;
    }    
}

void RNDFSimIF::appendStartEnd(std::vector<Vector> *points, int flags)
{
    if (flags && APPEND_START)
    {
    	double X2 = points->at(0).x;
        double X1 = points->at(1).x;
        double Y2 = points->at(0).y;
        double Y1 = points->at(1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->begin();
	points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
    
    if (flags && APPEND_END)
    {
        int last = points->size() - 1;

        double X2 = points->at(last).x;
        double X1 = points->at(last - 1).x;
        double Y2 = points->at(last).y;
        double Y1 = points->at(last - 1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->end();
        points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
}


//This will determine how many 'forward'and 'reverse' lanes there are.
//To do this, it will take the first lane as forward. Then it will
//compare locations (make sure other lane's initial point is within
//some radius (define to be 30?) of the first lane's start. It will
//also check to see if the dot product between the vectors of the first
//two points in each lane is approximately zero.
void RNDFSimIF::setLaneInfos() {
    for(int segIter = 1; segIter <= getNumSegs(); segIter++) {
        //Initialize the radius. This value needs to be confirmed.
	int radius = getNumLanes(segIter) * 20;
	
	//Initialize 1 forward lane.
        Vector laneData = Vector(1,0);

        std::vector<Vector> lane1 = getPointsFromLane(segIter, 1);

        Vector lane1Init = lane1.at(0);
        Vector lane1Second = lane1.at(1);
        Vector lane1Dir = lane1Second - lane1Init;
        
        //Iterate through the lanes. Start at two because the lanes
        //because the lanes are 1 indexed, and 1 is 'lane1'.
        for(int laneIter = 2; laneIter <= getNumLanes(segIter); laneIter++) {
	    std::vector<Vector> curLane = getPointsFromLane(segIter, laneIter);

	    Vector curLaneInit = curLane.at(0);
            Vector curLaneSecond = curLane.at(1);
	    Vector curLaneDir = curLaneSecond - curLaneInit;

	   /* if((lane1Dir.dot(lane1Dir.normalized(),
	                     curLaneDir.normalized()) > 0.85 ) &&
	       (lane1Init.dist2(lane1Init, curLaneInit) < radius))
	        laneData.x += 1;
	    else
	        laneData.y += 1;*/

	    if(lane1Init.dist2(curLaneInit,lane1Init) <
	       lane1Init.dist2(curLane.back(),lane1Init))
	       (laneData.x)++;
	   else
	       (laneData.y)++;
        }
	laneInfo.push_back(laneData);
    }
}



std::vector<Vector> RNDFSimIF::getPointsFromLane(int seg, int lane) {
    std::vector<Vector> points;
    std::vector<Vector>::iterator pointsIter;

    Segment* seg1 = rndf->getSegment(seg);
    Lane* lane1 = seg1->getLane(lane);
    std::vector<Waypoint*> waypoints = lane1->getAllWaypoints();
 
    std::vector<Waypoint*>::iterator iter;
    
    for(iter = waypoints.begin(); iter != waypoints.end(); iter++)
	points.push_back(Vector((*iter)->getEasting(), (*iter)->getNorthing()));

    return points;
}


void RNDFSimIF::centerAllWaypoints() {
    vector<Segment*> segs = rndf->getAllSegments();
    vector<Segment*>::iterator segIter;

    double largestNorthing = 0, smallestNorthing = 0;
    double largestEasting = 0, smallestEasting = 0;
    bool hack = true;

    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        vector<Lane*> lanes = (*segIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;

	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
            vector<Waypoint*>::iterator iter;
            
	    for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
	        double tempHeadingN = (*iter)->getNorthing();
	        double tempHeadingE = (*iter)->getEasting();
	        
		if(hack) {
		     largestNorthing = tempHeadingN;
		     largestEasting = tempHeadingE;
		     smallestNorthing = tempHeadingN;
		     smallestEasting = tempHeadingE;
                     hack = false;
                }
	        
		if(tempHeadingN >= largestNorthing)
	            largestNorthing = tempHeadingN;
	
	        else if(tempHeadingN < smallestNorthing)
	            smallestNorthing = tempHeadingN;

	        if(tempHeadingE >= largestEasting)
	            largestEasting = tempHeadingE;

	        else if (tempHeadingE < smallestEasting)
	            smallestEasting = tempHeadingE;
            }
	}
    }

    yTranslate = 0.5*(largestNorthing + smallestNorthing);
    xTranslate = 0.5*(largestEasting + smallestEasting);

    
    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        vector<Lane*> lanes = (*segIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;
        
	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
   
            vector<Waypoint*>::iterator iter;
            for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
                (*iter)->setNorthingEasting(((*iter)->getNorthing()-yTranslate),((*iter)->getEasting()-xTranslate));
            }
        }
    }

    setLaneInfos();
    setCenterLines();
}
	

}//namespace Trafsim
