//#include "interfaces/StateClient.hh"
#include "rndf/RNDF.hh"

#include "Object.hh"
#include "Vector.hh"


//Define flags for use internally.
//APPEND_DIST may need to be modified. It appends a segment with
//another point colinear with previous two points at a distance
//APPEND_DIST away.
#define APPEND_START 0x1
#define APPEND_END   0x2
#define APPEND_DIST  5


namespace TrafSim
{

//I don't think I need these. Commenting out to see if compile still holds.
/*
typedef enum
{
    LEFT_A,
    RIGHT_A
} Dir;

typedef struct
{
    Dir dir;
    double dist;
} DirDist;
*/

class RNDFSimIF
{
    public:
    RNDF* rndf;       
    
    RNDFSimIF();
    ~RNDFSimIF();
        
    //Parse in the RNDF file.
    bool loadFile(char* filename);
    
    //Return the points for all of the segments.
    void setCenterLines();

    //Return center line of (seg).
    std::vector< Vector > getCenterLineOfSeg(int seg);
    
    //Return the points from (segment, lane).
    std::vector< Vector > getPointsFromLane(int seg, int lane);    
    
    //Northing and Easting values are disgustingly too large for
    //the simulator to easily handle. Center these values so that it
    //can be used.
    void centerAllWaypoints();
    
    //Return the number of segments in the loaded RNDF file. For use
    //in building the RNDF in the python environment.
    int getNumSegs();

    //Return the number of lanes in 'segment'.
    int getNumLanes(int segment);

    //Get number of forwards and reverse lanes.
    void setLaneInfos();

    //Return the lane infos. For use in trafsim.
    std::vector<Vector> getLaneInfos();

    //Get the amount that the map was translated.
    double getxTranslate();
    double getyTranslate();

    //Store the xTranslate and yTranslate values, as obtained by
    //centerAllWaypoints() so that conversions can be made back to
    //Northing and Easting for closed loop simulation.
    private:
    //Appends a start and/or end point to the vector to make
    //the splines build right.
    //Flags: APPEND_START APPEND_END
    void appendStartEnd(std::vector<Vector> *points,
                        int flags=APPEND_START | APPEND_END);

    double xTranslate, yTranslate; 

    //Stores the number of lanes in forward/reverse. Indexed
    //by segment number.
    std::vector<Vector> laneInfo;

    //Store the center lines for ease of collection.
    std::vector< std::vector< Vector > > centerLines;
};
}
