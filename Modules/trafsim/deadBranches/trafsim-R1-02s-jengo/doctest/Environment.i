
%define DOC_ENV(method, string)
%feature("docstring", string) TrafSim::Environment::method;
%enddef

%define DOC_TESTOBJ(method, string)
%feature("docstring", string) TrafSim::TestObject::method;
%enddef

%feature("docstring",
"""
See the doxygen documentation for details about Environments.

>>> env = Environment()
>>> env.addObject( TestObject() )
'TestObject'
>>> env.addObject( TestObject() )
'TestObject___1'
>>> env.draw()
... # 'Called TestObject.draw()'       
... # 'Called TestObject___1.draw()'
>>> env.simulate(2.0)   # this should kill all the TestObjects
... # 'Called TestObject.simulate()'
... # 'Called TestObject___1.simulate()'
>>> print env					# and indeed they're all gone now
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(0) {
 }
nameList --> collection(1) {
TestObject : 2 ,
 }
 )
>>> env.clearObjects()

""") TrafSim::Environment;

DOC_ENV( Environment, 
"""
Nothing special happens in the constructor, so you can 
create new environments without fear.


>>> print Environment()
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(0) {
 }
nameList --> collection(0) {
 }
 )

""")

DOC_ENV( addObject,
"""
Adds a reference to the given object, trying to give it the proposedName.
if proposedName is already in use, it will generate a new unique name.

Also, when a class constructed in Python is passed into addObject,
Python gives up ownership of the object so that it doesn't get prematurely
garbage collected.

>>> env = Environment()
>>> env.addObject( TestObject() )
'TestObject'
>>> env.addObject( TestObject(), 'MyObject' )
'MyObject'
>>> env.addObject( TestObject(), 'MyObject' )
'MyObject___1'
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(3) {
MyObject : TestObject( reference --> None ) ,
MyObject___1 : TestObject( reference --> None ) ,
TestObject : TestObject( reference --> None ) ,
 }
nameList --> collection(2) {
MyObject : 2 ,
TestObject : 1 ,
 }
 )
>>> env.clearObjects()

""")

DOC_ENV( removeObject,
"""
Removes an object from the directory. If canDelete is true, also frees the object's memory.
Also note that there are NO safeguards against dangling references. If an object is removed
while another object refers to it, by name or otherwise, the results are unspecified.

>>> env = Environment()
>>> env.addObject( TestObject() )
'TestObject'
>>> env.addObject( TestObject() )
'TestObject___1'
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(2) {
TestObject : TestObject( reference --> None ) ,
TestObject___1 : TestObject( reference --> None ) ,
 }
nameList --> collection(1) {
TestObject : 2 ,
 }
 )
>>> env.removeObject('TestObject___2')
False
>>> env.removeObject('TestObject')
True
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(1) {
TestObject___1 : TestObject( reference --> None ) ,
 }
nameList --> collection(1) {
TestObject : 2 ,
 }
 )
>>> env.clearObjects()

""")
	
DOC_ENV( renameObject,
"""
Changes an object's name.

>>> env = Environment()
>>> env.addObject(TestObject())
'TestObject'
>>> obj = env.getObject('TestObject')
>>> env.renameObject('TestObject__1', 'MyObject')
''
>>> env.addObject(TestObject(), 'MyObject')
'MyObject'
>>> env.renameObject('TestObject', 'MyObject') 
'MyObject___1'
>>> obj == env.getObject('MyObject___1')
True
>>> env.getObject('TestObject')
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(2) {
MyObject : TestObject( reference --> None ) ,
MyObject___1 : TestObject( reference --> None ) ,
 }
nameList --> collection(2) {
MyObject : 2 ,
TestObject : 1 ,
 }
 )
>>> env.clearObjects()

""")

DOC_ENV( getObject,
"""
Looks up an object by name. O(log n) in the number of objects.

>>> env = Environment()
>>> env.addObject(TestObject())
'TestObject'
>>> env.getObject('NotAnObject')
>>> print env.getObject('TestObject')
TestObject( reference --> None )
>>> env.clearObjects()

""")

DOC_ENV( getObjectsByClassID,
"""
Returns a name->object dict of all the objects with the given classID.

>>> env = Environment()
>>> env.addObject(TestObject())
'TestObject'
>>> env.addObject(TestObject())
'TestObject___1'
>>> [name for name in env.getObjectsByClassID('TestObject').keys()]
['TestObject', 'TestObject___1']
>>> env.clearObjects()

""")

DOC_ENV( clearObjects,
"""
Resets the environment to its original state. All objects are removed, and deleted if
canDelete is set to true. The name directory is also cleared out.

>>> env = Environment()
>>> env.addObject(TestObject())
'TestObject'
>>> env.addObject(TestObject())
'TestObject___1'
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(2) {
TestObject : TestObject( reference --> None ) ,
TestObject___1 : TestObject( reference --> None ) ,
 }
nameList --> collection(1) {
TestObject : 2 ,
 }
 )
>>> env.clearObjects()
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(0) {
 }
nameList --> collection(0) {
 }
 )

""")

DOC_ENV( draw,
"""
Renders each of the environment's components. Call this from a 
thread in which you opened a Viewport.

>>> env = Environment()
>>> env.addObject( TestObject() )
'TestObject'
>>> env.addObject( TestObject() )
'TestObject___1'
>>> env.draw()
... # 'Called TestObject.draw()'       
... # 'Called TestObject___1.draw()'
>>> env.clearObjects()

""")

DOC_ENV( simulate,
"""
Tells each of the environment's components to update their state by another
time step, by calling their own simulate() functions. Any component that returns
'false' from simulate() gets automatically removed from the environment and deleted.

>>> env = Environment()
>>> env.addObject( TestObject() )
'TestObject'
>>> env.addObject( TestObject() )
'TestObject___1'
>>> env.simulate(0.0)   # TestObjects continue to live so long as ticks < 1.0
... # 'Called TestObject.draw()'       
... # 'Called TestObject___1.draw()'
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(2) {
TestObject : TestObject( reference --> None ) ,
TestObject___1 : TestObject( reference --> None ) ,
 }
nameList --> collection(1) {
TestObject : 2 ,
 }
 )
>>> env.simulate(2.0) # Now they die
... # 'Called TestObject.draw()'       
... # 'Called TestObject___1.draw()'
>>> print env
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(0) {
 }
nameList --> collection(1) {
TestObject : 2 ,
 }
 )

""")


DOC_ENV( parse,
"""
An environment can save and restore its entire state in textual form. Here's how.

>>> env = Environment()
>>> env.addObject( TestObject(), 'Object1' )
'Object1'
>>> env.addObject( TestObject(), 'Object2' )
'Object2'
>>> env.addObject( TestObject(), 'Object3' )
'Object3'
>>> obj1 = TestObject.downcast( env.getObject( 'Object1' ) )   # turn the received Object into a TestObject
>>> obj2 = TestObject.downcast( env.getObject( 'Object2' ) )
>>> obj3 = TestObject.downcast( env.getObject( 'Object3' ) )
>>> obj1.reference = obj3
>>> obj2.reference = obj3
>>> obj3.reference = obj1 
>>> print env	
... # even though the references are pure pointers, the
... # environment knows which objects they are referring to
... # (so long as they are toplevel objects directly named
... # by the environment)
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(3) {
Object1 : TestObject( reference --> Object3 ) ,
Object2 : TestObject( reference --> Object3 ) ,
Object3 : TestObject( reference --> Object1 ) ,
 }
nameList --> collection(3) {
Object1 : 1 ,
Object2 : 1 ,
Object3 : 1 ,
 }
 )
>>> s1 = str(env)
>>> env2 = Environment.parse(s1)   # construct a new environment from a string
>>> print env2
Environment( 
bottomLeftBound --> Vector(0, 0)
topRightBound --> Vector(100, 100)
nameDirectory --> collection(3) {
Object1 : TestObject( reference --> Object3 ) ,
Object2 : TestObject( reference --> Object3 ) ,
Object3 : TestObject( reference --> Object1 ) ,
 }
nameList --> collection(3) {
Object1 : 1 ,
Object2 : 1 ,
Object3 : 1 ,
 }
 )
>>> env2obj1 = TestObject.downcast( env2.getObject('Object1') )
>>> env2obj2 = TestObject.downcast( env2.getObject('Object2') )
>>> env2obj3 = TestObject.downcast( env2.getObject('Object3') )
>>> env2obj1.reference == env2obj3
True
>>> env2obj2.reference == env2obj3
True
>>> env2obj3.reference == env2obj1
True
>>> env2obj1 == obj1
False
>>> env == env2
False

So everything is restored in the new environment exactly as it was in the original.

""")

%feature("docstring",
"""
A trivial Object subclass, used in the Environment class's doctests. Simply writes to 
stdout to report when its draw() and simulate() functions are called. Has a single 
Object* member to test the Environment's reference resolving functionality.

See the doxygen documentation for more details.
""") TrafSim::TestObject;
