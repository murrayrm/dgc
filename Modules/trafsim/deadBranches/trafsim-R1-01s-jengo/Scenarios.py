from libtrafsim_Py import *

from SplineEditor import SplineEditor

class Scenarios:
    def __init__(self, env, editor, sel):
        self.environment = env
	self.editor = editor
	self.sel = sel
    
    def UT_tplanner_RR_SO_LegalPass(self):
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalPass(self):
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalUturn(self):
	b = Obstacle(398992.68318241555-self.environment.xTranslate,3781259.8189164037-self.environment.yTranslate,2,15)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_Partial(self):
	b = Obstacle(399000.68318241555-self.environment.xTranslate,3781267.8189164037-self.environment.yTranslate,3)
	self.environment.addObject(b)

    def simple_straightline(self):
        print "A"
        
    def simple_turns(self):
        print "B"
        b1 = Obstacle(403961.84-self.environment.xTranslate,3778457.2-self.environment.yTranslate,-.35)
        self.environment.addObject(b1)
        b2 = Obstacle(403949.379988-self.environment.xTranslate,3778473.61999-self.environment.yTranslate,-.35)
        self.environment.addObject(b2)
        b3 = Obstacle(403964.779985-self.environment.xTranslate,3778483.97999-self.environment.yTranslate,-.35)
        self.environment.addObject(b3)

    def simple_staticobstacle(self):
        print "C"
        b1 = Obstacle(403927.42-self.environment.xTranslate,3778490.8599777-self.environment.yTranslate,1)
        self.environment.addObject(b1)
        b2 = Obstacle(403927.420038-self.environment.xTranslate,3778488.0699768-self.environment.yTranslate,1)
        self.environment.addObject(b2)

    def simple_roadblock(self):
        print "D"
        b1 = Obstacle(403875.9274-self.environment.xTranslate,3778567.5125-self.environment.yTranslate,20,3,-.35)
        self.environment.addObject(b1)
        b2 = Obstacle(403983.9975-self.environment.xTranslate,3778527.3875-self.environment.yTranslate,20,3,-.35)
        self.environment.addObject(b2)

    def simple_circleblock(self):
        print "E"
        print "past Alice"
        b1 = Obstacle(403981.019985-self.environment.xTranslate,3778500.21999-self.environment.yTranslate,2)
        self.environment.addObject(b1)
        print "past first object"
        b2 = Obstacle(403979.89999-self.environment.xTranslate,3778504.97999-self.environment.yTranslate,2)
        self.environment.addObject(b2)
        b3 = Obstacle(403980.179985-self.environment.xTranslate,3778510.2999-self.environment.yTranslate,2)
        self.environment.addObject(b3)
        b4 = Obstacle(403983.539986-self.environment.xTranslate,3778521.21999-self.environment.yTranslate,2)
        self.environment.addObject(b4)
        b5 = Obstacle(403985.779987-self.environment.xTranslate,3778523.21999-self.environment.yTranslate,2)
        self.environment.addObject(b5)
        b6 = Obstacle(403989.419987-self.environment.xTranslate,3778525.13999-self.environment.yTranslate,2)
        self.environment.addObject(b6)
        print "finished placing blocks"

#Add obstacle somewhere between 1.1.5 and 1.1.6 that blocks both lanes
    def stluke_singleroad_roadblock(self):
        barricade1 = Obstacle(398929.40206307068-self.environment.xTranslate,
3781259.1864437452-self.environment.yTranslate, 25, 4, 3.14159/2)
        self.environment.addObject(barricade1)

#Add obstacle a little behind 1.1.2 that blocks the lane so Alice can
#go around it (without changing lanes)
    def stluke_singleroad_partlaneblock(self):
        partBlock1 = Obstacle(399015.33643501892-self.environment.xTranslate, 3781266.3486046186-self.environment.yTranslate, 1.5)
        self.environment.addObject(partBlock1)

#Add obstacle a little behind 1.1.2 that blocks the lane so Alice can go around
#it (change lanes).
    def stluke_singleroad_laneblock(self):
        block1 = Obstacle(399017.00831001892-self.environment.xTranslate, 3781263.4837402692-self.environment.yTranslate, 1.5)
        self.environment.addObject(block1)

#Add obstacle midway somewhere between 4.1.6 and 4.1.7 that partially blocks
#the lane so Alice can go around it (shouldn't change lanes).
    def stluke_small_partlaneblock(self):
        partBlock1 = Obstacle(399009.48487251892-self.environment.xTranslate, 3781326.8688721051-self.environment.yTranslate, 1)
        partBlock2 = Obstacle(399019.51612251892-self.environment.xTranslate, 3781322.929686009-self.environment.yTranslate, 1)
        self.environment.addObject(partBlock1)
        self.environment.addObject(partBlock2)

#Add an obstacle somewhere between 2.2.8 and 2.2.9 (closer to 2.2.9) that
#block the whole road
    def santaanita_sitevisit_roadblock(self):
        barricade1 = Obstacle(403886.66986496828-self.environment.xTranslate, 3778504.2238156628-self.environment.yTranslate, 18, 2, 3.5/3)
        self.environment.addObject(barricade1)

#Add an obstacle at the same place that only partially blocks the lane so Alice
#can go past it (while staying in the lane)
    def santaanita_sitevisit_passblock(self):
        block1 = Obstacle(403885.37245591066-self.environment.xTranslate, 3778501.4632603955-self.environment.yTranslate, 4, 4, 3.5/3)
        self.environment.addObject(block1)

#Add an obstacle at the same place that blocks the drive lane so Alice can go
#around it (changing lanes)
    def santaanita_sitevisit_block(self):
        block1 = Obstacle(403887.53480179689-self.environment.xTranslate, 3778507.4444649052-self.environment.yTranslate, 4, 4, 3.5/3)
        self.environment.addObject(block1)

#Add an obstacle somewhere between 2.2.10 and 2.2.11 that only partially blocks
#the lane (no lane changing should be required).
    def santaanita_sitevisit_partlaneblock(self):
        partBlock1 = Obstacle(403863.75407059572-self.environment.xTranslate, 3778516.4020390343-self.environment.yTranslate, 1.5)
        partBlock2 = Obstacle(403866.09334214113-self.environment.xTranslate, 3778524.9194295239-self.environment.yTranslate, 1.5)
        self.environment.addObject(partBlock1)
        self.environment.addObject(partBlock2)

    def santaanita_sitevisit_sparseroadblock(self):
        self.environment.addObject(Obstacle(-58.646942138671875, -17.636882781982422, 2))
        self.environment.addObject(Obstacle(-57.014129638671875, -14.333579063415527, 2))
        self.environment.addObject(Obstacle(-55.381317138671875, -10.149394989013672, 2))
        self.environment.addObject(Obstacle(-53.748504638671875, -6.0753211975097656, 2))
        self.environment.addObject(Obstacle(-51.771942138671875, -2.2214672565460205, 2))
        self.environment.addObject(Obstacle(-49.967254638671875, 1.9627169370651245, 2))

    def santaanita_sitevisit_turnroadblock(self):
        self.environment.addObject(Obstacle(-77.0,1.0, 30, 3, 0.78500000000000003))


    def santaanita_sitevisit_simpleintersection(self):
	i = self.editor.numCars
	if i == 0:
	    self.editor.clickedCars = [Car(1)]
	else:
	    self.editor.clickedCars[i-1:i] = [self.self.environment.clickedCars[i-1],Car(1)]
	self.editor.clickedCars[i].setPosition(-50,-10)
	self.editor.clickedCars[i].setOrientation(0)
	self.environment.addObject(self.editor.clickedCars[i],'Car')
	self.editor.numCars += 1



    def santaanita_sitevisit_bigIntersectionBlock(self):
        self.environment.addObject(Obstacle(22.0, -38.5, 10))

    def santaanita_sitevisit_fakeIntersectionOrdering(self):
        self.environment.addObject(Obstacle(33.375, -39.5, 2))
        self.environment.addObject(Obstacle(22.625, -28.0, 2))

    def santaanita_sitevisit_directedDriving(self):
        self.environment.addObject(Obstacle(-18.958530426025391, -16.476669311523438, 0.25))
        self.environment.addObject(Obstacle(-20.528842926025391, -16.744937896728516, 0.25))
        self.environment.addObject(Obstacle(-22.099155426025391, -17.013206481933594, 0.25))
        self.environment.addObject(Obstacle(-23.669467926025391, -17.281473159790039, 0.25))
        self.environment.addObject(Obstacle(-25.763217926025391, -17.549741744995117, 0.25))
        self.environment.addObject(Obstacle(-27.856967926025391, -17.683876037597656, 0.25))
        self.environment.addObject(Obstacle(-29.950717926025391, -17.415607452392578, 0.25))
        self.environment.addObject(Obstacle(-32.253841400146484, -16.744937896728516, 0.25))
        self.environment.addObject(Obstacle(-33.928840637207031, -16.208400726318359, 0.25))
        self.environment.addObject(Obstacle(-35.394466400146484, -15.53773021697998, 0.25))
        self.environment.addObject(Obstacle(-36.860092163085938, -15.269461631774902, 0.25))
        self.environment.addObject(Obstacle(-38.011653900146484, -14.867059707641602, 0.25))
        self.environment.addObject(Obstacle(-39.267906188964844, -14.330523490905762, 0.25))
        self.environment.addObject(Obstacle(-40.524154663085938, -14.062254905700684, 0.25))
        self.environment.addObject(Obstacle(-41.780403137207031, -13.525718688964844, 0.25))
        self.environment.addObject(Obstacle(-43.455406188964844, -12.989181518554688, 0.25))
        self.environment.addObject(Obstacle(-45.130405426025391, -12.18437671661377, 0.25))
        self.environment.addObject(Obstacle(-47.119468688964844, -11.64784049987793, 0.25))
        self.environment.addObject(Obstacle(-48.480403900146484, -10.977169990539551, 0.25))
        self.environment.addObject(Obstacle(-50.050716400146484, -10.440632820129395, 0.25))
        self.environment.addObject(Obstacle(-51.411655426025391, -8.8310232162475586, 0.25))
        self.environment.addObject(Obstacle(-50.783531188964844, -9.6358280181884766, 0.25))
        self.environment.addObject(Obstacle(-52.667903900146484, -7.2214140892028809, 0.25))
        self.environment.addObject(Obstacle(-52.144466400146484, -8.0262184143066406, 0.25))
        self.environment.addObject(Obstacle(-53.400718688964844, -6.2824749946594238, 0.25))
        self.environment.addObject(Obstacle(-54.238216400146484, -4.941133975982666, 0.25))
        self.environment.addObject(Obstacle(-54.866340637207031, -4.136329174041748, 0.25))
        self.environment.addObject(Obstacle(-55.389778137207031, -3.197390079498291, 0.25))
        self.environment.addObject(Obstacle(-59.054428100585938, -14.330514907836914, 0.25))
        self.environment.addObject(Obstacle(-59.68255615234375, -13.793978691101074, 0.25))
        self.environment.addObject(Obstacle(-60.938804626464844, -12.855039596557617, 0.25))
        self.environment.addObject(Obstacle(-61.985679626464844, -12.050234794616699, 0.25))
        self.environment.addObject(Obstacle(-62.718490600585938, -11.37956428527832, 0.25))
        self.environment.addObject(Obstacle(-63.555992126464844, -10.574759483337402, 0.25))
        self.environment.addObject(Obstacle(-64.39349365234375, -9.7699546813964844, 0.25))
        self.environment.addObject(Obstacle(-65.545051574707031, -8.8310155868530273, 0.25))
        self.environment.addObject(Obstacle(-66.382553100585938, -7.6238083839416504, 0.25))
        self.environment.addObject(Obstacle(-67.53411865234375, -6.6848692893981934, 0.25))
        self.environment.addObject(Obstacle(-68.58099365234375, -5.7459306716918945, 0.25))
        self.environment.addObject(Obstacle(-69.62786865234375, -4.6728572845458984, 0.25))
        self.environment.addObject(Obstacle(-70.465370178222656, -4.0021867752075195, 0.25))
        self.environment.addObject(Obstacle(-71.72161865234375, -3.1973819732666016, 0.25))
        self.environment.addObject(Obstacle(-72.663803100585938, -2.7949795722961426, 0.25))
        self.environment.addObject(Obstacle(-74.024742126464844, -1.7219064235687256, 0.25))
        self.environment.addObject(Obstacle(-75.280990600585938, -0.64883339405059814, 0.25))
        self.environment.addObject(Obstacle(-76.327865600585938, 0.55837380886077881, 0.25))
        self.environment.addObject(Obstacle(-77.060676574707031, 0.96077626943588257, 0.25))
        self.environment.addObject(Obstacle(-77.793495178222656, 1.4973127841949463, 0.25))
        self.environment.addObject(Obstacle(-78.735679626464844, 2.0338492393493652, 0.25))
        self.environment.addObject(Obstacle(-80.09661865234375, 2.5703859329223633, 0.25))
        self.environment.addObject(Obstacle(-81.352867126464844, 2.9727883338928223, 0.25))
        self.environment.addObject(Obstacle(-82.609115600585938, 3.2410564422607422, 0.25))
        self.environment.addObject(Obstacle(-83.76068115234375, 3.6434588432312012, 0.25))
        self.environment.addObject(Obstacle(-85.226303100585938, 4.1799955368041992, 0.25))
        self.environment.addObject(Obstacle(-86.37786865234375, 4.5823979377746582, 0.25))
        self.environment.addObject(Obstacle(-75.595115661621094, 9.9477739334106445, 0.25))
        self.environment.addObject(Obstacle(-76.851364135742188, 10.618444442749023, 0.25))
        self.environment.addObject(Obstacle(-78.316986083984375, 11.423249244689941, 0.25))
        self.environment.addObject(Obstacle(-79.468551635742188, 11.959785461425781, 0.25))
        self.environment.addObject(Obstacle(-80.620109558105469, 12.496322631835938, 0.25))
        self.environment.addObject(Obstacle(-81.562301635742188, 13.971797943115234, 0.25))
        self.environment.addObject(Obstacle(-81.876365661621094, 15.58140754699707, 0.25))
        self.environment.addObject(Obstacle(-82.085739135742188, 16.922748565673828, 0.25))
        self.environment.addObject(Obstacle(-82.085739135742188, 18.129955291748047, 0.25))
        self.environment.addObject(Obstacle(-81.666984558105469, 19.605430603027344, 0.25))
        self.environment.addObject(Obstacle(-81.143547058105469, 20.544370651245117, 0.25))
        self.environment.addObject(Obstacle(-80.410736083984375, 21.349174499511719, 0.25))
        self.environment.addObject(Obstacle(-79.363861083984375, 22.019845962524414, 0.25))
        self.environment.addObject(Obstacle(-78.002922058105469, 22.288114547729492, 0.25))
        self.environment.addObject(Obstacle(-76.641990661621094, 22.55638313293457, 0.25))
        self.environment.addObject(Obstacle(-75.281051635742188, 22.824649810791016, 0.25))
        self.environment.addObject(Obstacle(-74.234176635742188, 22.824649810791016, 0.25))
        self.environment.addObject(Obstacle(-73.082611083984375, 23.092918395996094, 0.25))

    def santaanita_sitevisit_laneSeparation(self):
        self.environment.addObject(Obstacle(-44.300083160400391,-13.22499942779541, 40, 0.29999999999999999, 2.8033333333333337))

    def santaanita_sitevisit_centercircles(self):
        self.environment.addObject(Obstacle(-27.0, -19.6875, 0.25))
        self.environment.addObject(Obstacle(-28.75, -18.9375, 0.25))
        self.environment.addObject(Obstacle(-30.75, -18.4375, 0.25))
        self.environment.addObject(Obstacle(-32.0, -17.6875, 0.25))
        self.environment.addObject(Obstacle(-33.75, -16.9375, 0.25))
        self.environment.addObject(Obstacle(-36.0, -16.6875, 0.25))
        self.environment.addObject(Obstacle(-35.75, -15.4375, 0.25))
        self.environment.addObject(Obstacle(-35.25, -14.6875, 0.25))
        self.environment.addObject(Obstacle(-34.5, -13.6875, 0.25))
        self.environment.addObject(Obstacle(-33.5, -12.6875, 0.25))
        self.environment.addObject(Obstacle(-32.5, -12.1875, 0.25))
        self.environment.addObject(Obstacle(-27.75, -19.4375, 0.25))
        self.environment.addObject(Obstacle(-28.25, -20.1875, 0.25))
        self.environment.addObject(Obstacle(-29.0, -21.1875, 0.25))
        self.environment.addObject(Obstacle(-29.75, -22.6875, 0.25))
        self.environment.addObject(Obstacle(-30.5, -23.9375, 0.25))

    def greenscreen(self):
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.827844798565)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 1, 0.229775682092)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.0193713158369)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.978499233723)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.0181335899979)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.794734656811)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 1, 0.275294393301)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.799305677414)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.638250589371)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.501796364784)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.408837676048)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.345994114876)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.973053276539)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.0329178273678)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.707144379616)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 1, 0.302105516195)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.0515817813575)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.949868559837)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.684029877186)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.310414105654)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 1, 0.36421918869)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.616724193096)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.740340411663)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.450782775879)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.26901525259)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.140688836575)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.984619498253)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.930970549583)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.0267314445227)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.97621268034)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.0458426065743)

    def santaanita_sitevisit_slowIntersection(self):
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.354839146137)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.6245033741)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.907652497292)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.149581745267)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.546269595623)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.853952229023)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.975526750088)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.901707231998)
        c = Car(0.40000000000000002)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.808767557144)

    def stluke_singleroad_following(self):
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.343581914902)
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.172386705875)

    def santaanita_sitevisit_intersection(self):
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.921903073788)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.697003126144)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.280073046684)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.821314275265)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.0215758681297)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.330874502659)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.624558985233)

    def santaanita_sitevisit_simpleintersection(self):
        c = Car(0.80000000000000004)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.115637615323)

    def santaanita_sitevisit_sparseroadblock2(self):
        self.environment.addObject(Obstacle(-52.640861511230469, -18.097503662109375, 1))
        self.environment.addObject(Obstacle(-50.587287902832031, -16.408658981323242, 1))
        self.environment.addObject(Obstacle(-48.780147552490234, -14.873347282409668, 1))
        self.environment.addObject(Obstacle(-46.890861511230469, -13.082150459289551, 1))
        self.environment.addObject(Obstacle(-45.248004913330078, -11.444483757019043, 1))
        self.environment.addObject(Obstacle(-43.687290191650391, -9.8068170547485352, 1))
        self.environment.addObject(Obstacle(-42.126575469970703, -7.7597346305847168, 1))
        self.environment.addObject(Obstacle(-39.908718109130859, -5.7638287544250488, 1))

    def santaanita_sitevisit_sparseroadblock3(self):
        self.environment.addObject(Obstacle(-52.349998474121094, -19.005054473876953, 1.0))
        self.environment.addObject(Obstacle(-51.107143402099609, -16.52424430847168, 1.0))
        self.environment.addObject(Obstacle(-49.864284515380859, -13.689035415649414, 1.0))
        self.environment.addObject(Obstacle(-48.621429443359375, -10.499423980712891, 1.0))
        self.environment.addObject(Obstacle(-47.378570556640625, -7.6642146110534668, 1.0))
        self.environment.addObject(Obstacle(-46.135715484619141, -4.8290047645568848, 1.0))
        self.environment.addObject(Obstacle(-44.582141876220703, -1.6393935680389404, 1.0))

    def santaanita_sitevisit_bigintersectionblock2(self):
        self.environment.addObject(Obstacle(21.875, -38.6875, 10))

    def santaanitaCarOppositeLane(self):
        c = Car(1)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.817374765873)

    def santaanita_sitevisit_fakeintersectionordering(self):
        self.environment.addObject(Obstacle(22.625, -26.8125, 2.25))
        self.environment.addObject(Obstacle(10.75, -37.1875, 2.25))
        self.environment.addObject(Obstacle(36.0, -40.9375, 2.25))

    def santaanita_sitevisit_laneseparation(self):
        self.environment.addObject(Obstacle(-40.75,-14.4375, 40, 0.29999999999999999, 2.8033333333333301))

    def santaanita_sitevisit_waittopassobstacle(self):
        self.environment.addObject(Obstacle(-33.125,-13.9375, 4, 4, 1.1666666666666667))
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.883692920208)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.809564590454)

    def santaanita_sitevisit_longwaittopassobstacle(self):
        self.environment.addObject(Obstacle(-42.0,-10.6875, 4, 4, 1.1666666666666667))
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.817833483219)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.699961006641)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.564542889595)
        c = Car(3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.423085957766)

    def stluke_small_caroppositelane(self):
        c = Car(2)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.115051493049)
        c = Car(2)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___7')), 1, 0.218697935343)

    def stluke_small_carfollowing(self):
        c = Car(.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.334011316299)
        c = Car(.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___7')), 0, 0.1405826509)
        c = Car(.3)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___7')), 0, 0.588312923908)

    def stluke_small_waittopassobstacle(self):
        self.environment.addObject(Obstacle(43.5,-25.4375, 4, 4, 0))
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.0455506257713)
        c = Car(1.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___7')), 1, 0.07058160007)
        c = Car(1.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.785424828529)

    def stluke_small_circlingcars(self):
        c = Car(0.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.496568292379)
        c = Car(0.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___7')), 1, 0.31316486001)
        c = Car(0.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.920499324799)

    def santaanita_sitevisit_intersection_scripted(self):
        self.editor.events.append(Event("santaanita_sitevisit_intersection", 14.25, -66.9375, 2.0, 0.0))

    def santaanita_sitevisit_extended_intersection(self):
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.978281140327)
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.539788544178)
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.84430205822)
        c = Car(0.75)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.296279370785)

    def santaanita_sitevisit_extended_obspassing(self):
        c = Car(0.5)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.0735853016376)
        c = Car(1.0)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.955436825752)

    def santaanita_sitevisit_extended_obspassing2(self):
        c = Car(0.69999999999999996)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.875294804573)
        c = Car(0.69999999999999996)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.810615718365)
        c = Car(0.69999999999999996)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.764032959938)

    def santaanita_sitevisit_extended(self):
        self.environment.addObject(Obstacle(11.625,-75.8125, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-21.375,-18.0625, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-26.625,-16.3125, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-38.125,-12.3125, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-68.625, 0.6875, 0.25))
        self.environment.addObject(Obstacle(-69.875, 0.9375, 0.25))
        self.environment.addObject(Obstacle(-71.375, 0.9375, 0.25))
        self.environment.addObject(Obstacle(-72.875, 0.9375, 0.25))
        self.environment.addObject(Obstacle(-74.375, 1.1875, 0.25))
        self.environment.addObject(Obstacle(-75.625, 1.4375, 0.25))
        self.environment.addObject(Obstacle(-77.125, 3.1875, 0.25))
        self.environment.addObject(Obstacle(-77.875, 4.4375, 0.25))
        self.environment.addObject(Obstacle(-79.375, 7.1875, 0.25))
        self.environment.addObject(Obstacle(-79.125, 6.1875, 0.25))
        self.environment.addObject(Obstacle(-80.125, 8.9375, 0.25))
        self.environment.addObject(Obstacle(-80.875, 10.4375, 0.25))
        self.environment.addObject(Obstacle(-81.875, 13.1875, 0.25))
        self.environment.addObject(Obstacle(-81.625, 14.9375, 0.25))
        self.environment.addObject(Obstacle(-80.125, 14.9375, 0.25))
        self.environment.addObject(Obstacle(-78.625, 15.1875, 0.25))
        self.environment.addObject(Obstacle(-77.625, 15.1875, 0.25))
        self.environment.addObject(Obstacle(-73.875,46.1875, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-69.375,56.4375, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-58.625, 91.4375, 0.25))
        self.environment.addObject(Obstacle(-57.625, 91.6875, 0.25))
        self.environment.addObject(Obstacle(-54.875, 92.4375, 0.25))
        self.environment.addObject(Obstacle(-52.375, 93.4375, 0.25))
        self.environment.addObject(Obstacle(-50.125, 94.6875, 0.25))
        self.environment.addObject(Obstacle(-47.125, 96.1875, 0.25))
        self.environment.addObject(Obstacle(-44.625, 97.4375, 0.25))
        self.environment.addObject(Obstacle(-42.125, 98.4375, 0.25))
        self.environment.addObject(Obstacle(-39.625, 99.4375, 0.25))
        self.environment.addObject(Obstacle(-37.625, 100.1875, 0.25))
        self.environment.addObject(Obstacle(-34.625, 100.9375, 0.25))
        self.environment.addObject(Obstacle(-31.875, 101.6875, 0.25))
        self.environment.addObject(Obstacle(-28.125, 102.9375, 0.25))
        self.environment.addObject(Obstacle(-26.125, 104.1875, 0.25))
        self.environment.addObject(Obstacle(-7.125,87.6875, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(14.875,86.4375, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(33.125,71.4375, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(48.375, 48.9375, 0.25))
        self.environment.addObject(Obstacle(48.625, 48.1875, 0.25))
        self.environment.addObject(Obstacle(48.875, 46.8125, 0.25))
        self.environment.addObject(Obstacle(48.5, 45.6875, 0.25))
        self.environment.addObject(Obstacle(47.875, 44.4375, 0.25))
        self.environment.addObject(Obstacle(46.875, 43.4375, 0.25))
        self.environment.addObject(Obstacle(45.5, 42.3125, 0.25))
        self.environment.addObject(Obstacle(43.875, 41.4375, 0.25))
        self.environment.addObject(Obstacle(54.125, 55.1875, 0.25))
        self.environment.addObject(Obstacle(46.125, 62.6875, 0.25))
        self.editor.events.append(Event("santaanita_sitevisit_extended_intersection", 31.625, -2.4375, 2.0, 0.0))
        self.editor.events.append(Event("santaanita_sitevisit_extended_obspassing", 3.75, -89.4375, 8.0, 0.0))
        self.editor.events.append(Event("santaanita_sitevisit_extended_obspassing2", -3.75, -24.6875, 2.0, 0.0))

    def santaanita_sitevisit_extended2_beginning(self):
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.731732189655)
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.120290525258)
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.409093350172)
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.943865716457)
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.865472733974)

    def santaanita_sitevisit_extended2_following(self):
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.684387087822)
        c = Car(0.59999999999999998)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 1, 0.76328843832)

    def santaanita_sitevisit_extended2_intersection(self):
        c = Car(1)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.938808619976)
        c = Car(1)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.859373629093)
        c = Car(1)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___2')), 0, 0.840334296227)
        c = Car(1)
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.430287092924)

    def santaanita_sitevisit_extended2(self):
        self.environment.addObject(Obstacle(43.0,18.1875, 30, 3, 2.7366666666666668))
        self.environment.addObject(Obstacle(-11.75,89.3125, 4, 4, 1.1666666666666667))
        self.environment.addObject(Obstacle(-65.375, -2.8125, 0.29999999999999999))
        self.environment.addObject(Obstacle(-20.125, -21.5625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-21.375, -20.8125, 0.29999999999999999))
        self.environment.addObject(Obstacle(-22.875, -20.0625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-24.375, -19.3125, 0.29999999999999999))
        self.environment.addObject(Obstacle(-26.125, -18.8125, 0.29999999999999999))
        self.environment.addObject(Obstacle(-27.875, -18.5625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-29.125, -18.5625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-33.375, -11.0625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-34.625, -11.0625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-35.875, -10.5625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-37.875, -9.5625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-45.125, -7.3125, 0.29999999999999999))
        self.environment.addObject(Obstacle(-52.625, -9.0625, 0.29999999999999999))
        self.environment.addObject(Obstacle(-72.125, 4.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(-73.375, 6.1875, 0.29999999999999999))
        self.environment.addObject(Obstacle(-74.375, 7.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(-75.375, 9.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(-76.375, 12.6875, 0.29999999999999999))
        self.environment.addObject(Obstacle(-76.375, 15.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(-76.375, 27.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(-75.375, 28.6875, 0.29999999999999999))
        self.environment.addObject(Obstacle(-75.125, 30.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(-49.375, 90.1875, 0.29999999999999999))
        self.environment.addObject(Obstacle(16.375, 75.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(18.625, 75.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(20.875, 75.1875, 0.29999999999999999))
        self.environment.addObject(Obstacle(22.875, 74.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(23.5, 78.6875, 0.29999999999999999))
        self.environment.addObject(Obstacle(25.125, 77.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(26.875, 77.1875, 0.29999999999999999))
        self.environment.addObject(Obstacle(23.625, 73.1875, 0.29999999999999999))
        self.environment.addObject(Obstacle(24.875, 71.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(44.625, 63.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(46.625, 62.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(48.125, 60.4375, 0.29999999999999999))
        self.environment.addObject(Obstacle(49.25, 59.6875, 0.29999999999999999))
        self.environment.addObject(Obstacle(50.125, 57.6875, 0.29999999999999999))
        self.environment.addObject(Obstacle(49.875, 55.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(49.875, 53.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(49.625, 52.3125, 0.29999999999999999))
        self.environment.addObject(Obstacle(49.125, 51.1875, 0.29999999999999999))
        self.environment.addObject(Obstacle(48.375, 49.9375, 0.29999999999999999))
        self.environment.addObject(Obstacle(42.875, 64.4375, 0.29999999999999999))
        self.editor.events.append(Event("santaanita_sitevisit_extended2_beginning", 12.125, -74.6875, 2.0, 0.0))
        self.editor.events.append(Event("santaanita_sitevisit_extended2_following", -70.375, 55.4375, 1.0, 0.0))
        self.editor.events.append(Event("santaanita_sitevisit_extended2_intersection", -24.125, -24.5625, 1.0, 0.0))

    def santaanita_sitevisit_onecirclingcar(self):
        c = Car(1.0)
        c.setPath(CarPath([1]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road___1')), 0, 0.917627096176)

    def stluke_small_TESTcarcontrol(self):
        c = Car(1.0)
        c.setPath(CarPath([2, 1, 2, 1, 2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.301761746407)

    def stluke_singleroad_carlauncher(self):
        self.editor.sel["launchcar"] = self.editor.select(68.625, 3.8125)
        c = Car(1.0)
        c.setPath(CarPath([2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.0237809475511)

    def stluke_singleroad_TESTcarlauncher_launcher(self):
        c = Car(1.0000000000000001)
        c.setPath(CarPath([2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.0472353510559)
        self.editor.sel["ammo"] = self.editor.select(53.625, 3.0625)
        (self.sel["ammo"]).maxVel = 30.0
        self.editor.events.append(Event("stluke_singleroad_TESTcarlauncher_launcher", 52.375, 3.3125, -0.001, 1.5))

    def stluke_singleroad_TESTcarlauncher(self):
        c = Car(1.0000000000000001)
        c.setPath(CarPath([2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.0555929690599)
        self.editor.events.append(Event("stluke_singleroad_TESTcarlauncher_launcher", 52.875, 3.5625, -1.0, 4.0))

    def stluke_singleroad_TESTcarlauncher2_launcher2(self):
        self.editor.sel["ammo2"] = self.editor.select(-64.75, -4.4375)
        c = Car(1.0)
        c.setPath(CarPath([2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.0479342862964)
        (self.sel["ammo2"]).maxVel = 60.0
        self.editor.events.append(Event("stluke_singleroad_TESTcarlauncher2_launcher2", -5.75, -3.9375, -0.20000000000000001, 6.0))

    def stluke_singleroad_TESTcarlauncher2(self):
        c = Car(1.0)
        c.setPath(CarPath([2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 0, 0.0976786985993)
        c = Car(1.0)
        c.setPath(CarPath([2]))
        self.environment.addObject(c)
        c.switchLane(Road.downcast(self.environment.getObject('Road')), 1, 0.0292475204915)
        self.editor.events.append(Event("stluke_singleroad_TESTcarlauncher_launcher", 60.0, 3.0625, -0.20000000000000001, 2.0))
        self.editor.events.append(Event("stluke_singleroad_TESTcarlauncher2_launcher2", -61.0, -4.6875, -0.20000000000000001, 2.0))

    def NULL(self):
        i = 0
