# _libtrafsim NEEDS to be a shared library to be loaded by python
BUILD_STATIC_LIBS       := true
BUILDING_SHARED_LIBS = true

#------------------------------------------------------------------------------
#	DO NOT CHANGE OR MOVE THE LINES BELOW
#
# Include a file that provides much common functionality
# be sure to include this *after* setting the MODULE_* variables
# These lines should be the first thing in the makefile
#------------------------------------------------------------------------------
ifndef YAM_ROOT
  include ../../etc/SiteDefs/mkHome/shared/overall.mk
else
  include $(YAM_ROOT)/etc/SiteDefs/mkHome/shared/overall.mk
endif

#------------------------------------------------------------------------------
#	START OF MODULE SPECIFIC CUSTOMIZATION (below)
#------------------------------------------------------------------------------
#
# Uncomment and define the variables as appropriate
#
#------------------------------------------------------------------------------
# specify any module specific variable definitions here
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# specify what to build. Librares should be listed in the PROJ_LIBS
# variable, while binaries go into the PROJ_BINS variable.
#
# If building a library, the name should NOT contain the suffix such 
# as ".a" or ".so".
#
# If PROJ_LIBS and PROJ_BINS are left blank, no source files will be 
# compiled, but the module may still contain scripts or public header 
# files. 
#------------------------------------------------------------------------------
PROJ_LIBS 			:= libtrafsim
# PROJ_BINS 			:= 
# PROJ_INTERNAL_BINS 		:= 

# FLAVORS-<tgt> 		:= 
# FLAVOR_EXT-<tgt>-<flavor> 	:= 

PROJ_PYTHON_MODULES         := _libtrafsim_Py

#PROJ_PYTHON_MODULE_LIBS		:= _libtrafsim

#------------------------------------------------------------------------------
# specify public .h, .a, .so, etc. files by filling in *_LINKS variables
# symbolic links to these files get created in top-level directories
#------------------------------------------------------------------------------
# BIN_LINKS			:=
#LIB_MODULE_LINKS 		:= _libtrafsim.so

PYTHON_PKGS			:= trafsim
PYTHON_LINKS_trafsim		:= CarDriver.py  DemExporter.py  \
				   GenericApp.py  SplineEditor.py  \
                                   SuperDynamic.py libtrafsim_Py.py

INC_MODULE_LINKS 		:= Car.hh CarFactory.hh CarProperties.hh Color.hh CurveConstraint.hh Environment.hh Event.hh ExternalCar.hh IDM.hh Intersection.hh Lane.hh LaneGrid.hh Line.hh LinearSegmentDesc.hh Object.hh Obstacle.hh RNDFSimIF.hh Road.hh Serialization.hh Spline.hh Vector.hh Viewport.hh XIntersection.hh YIntersection.hh

# ETC_MODULE_LINKS 	 	:=


#------------------------------------------------------------------------------
# specify source code to compile (must end in .c, or .cc)
# for source files to be compiled for each PROJ_LIBS and PROJ_BINS
# value listed above
#------------------------------------------------------------------------------
# CC_SRC-<tgt> 			:=
CPLUSPLUS_SRC-libtrafsim	:= \
	Car.cc \
	CarFactory.cc \
	CarProperties.cc \
	Color.cc \
	CurveConstraint.cc \
	Environment.cc \
	ExternalCar.cc \
	Event.cc \
	IDM.cc \
	Intersection.cc \
	Lane.cc \
	LaneGrid.cc \
	Line.cc \
	Object.cc \
	Obstacle.cc \
	RNDFSimIF.cc \
	Road.cc \
	Serialization.cc \
	Spline.cc \
	Vector.cc \
	Viewport.cc \
	XIntersection.cc \
	YIntersection.cc 

CPLUSPLUS_SRC-_libtrafsim_Py	:= \
	libtrafsim_swigwrap_Py.cc

# F77_SRC-<tgt> 		:= 

#------------------------------------------------------------------------------
# compilation flags unique to the individual targets
#------------------------------------------------------------------------------
ifeq ($(YAM_TARGET),i386-darwin)
CFLAGS-_libtrafsim_Py 		:= -I/sw/include/python2.5 -fPIC
else
CFLAGS-_libtrafsim_Py 		:= -I/usr/include/python2.4 -fPIC
endif
# F77FLAGS-<tgt> 		:=

#------------------------------------------------------------------------------
# when building binary executables and shared libraries, list any extra
# libraries that must be linked in and any -L options needed to find them.
# set the LINKER-<tgt> flag to "$(CPLUPLUS)" to use the C++ linker
#------------------------------------------------------------------------------
#LIBS-_libtrafsim                := -lrndf

LIBS-_libtrafsim_Py		:= -ltrafsim -lGL -lGLU -lSDL -lpthread \
	-lmap -lskynettalker -lskynet -lspread -fPIC -lncurses -linterfaces \
	-lrndf -ldgcutils -lframes 

# Under darwin we can't use -shared; use equivalent args, found on the web
ifeq ($(YAM_TARGET),i386-darwin)
LD_SHARED			:= $(CPLUSPLUS) -bundle -flat_namespace \
	-undefined suppress -L/sw/lib -L/usr/X11R6/lib
else
LD_SHARED			:= $(CPLUSPLUS) -shared
endif

# MODULE_LINKER   		:=

#------------------------------------------------------------------------------
# augment flags used when compiling C and C++ source code
#------------------------------------------------------------------------------
#MODULE_COMPILE_FLAGS 		:=  -W -Wall -g3 # this issues a lot of warnings!
# MODULE_COMPILE_FLAGS 		:= -Wall -g3
MODULE_COMPILE_FLAGS		:= -Os -Wall	# optimized for size and 
						# speed of compilation
ifneq ($(YAM_TARGET),i386-darwin)
MODULE_LINK_FLAGS="-s"
endif

#------------------------------------------------------------------------------
# additional compiler flags to use for dependency information generations
# if left undefined, then all the CFLAGS-<tgt> values are used to set this
# variable
#------------------------------------------------------------------------------
#MODULE_DEPENDS_FLAGS 		:= -MMD # this doesn't seem to work
#CPLUSPLUS_DEPEND_FLAG 		:= -MM # this works!

#------------------------------------------------------------------------------
# specify information for building Doxygen documentation
# set DOXYGEN_DOCS to "true" to turn on documentation generation
# set DOXYGEN_TAGFILES to other module names for link generation
#------------------------------------------------------------------------------
#DOXYGEN_DOCS 			:= true
# DOXYGEN_TAGFILES 		:= 

#------------------------------------------------------------------------------
# Add any additional rules specific to the module
#------------------------------------------------------------------------------
# regtest-module::
#	@$(YAM_ROOT)/bin/Drun -fep - oeltest -d test


SWIG_WRAPPERS                   := libtrafsim
SWIG_IFILE_PYTHON-libtrafsim          := libtrafsim.i

 


#------------------------------------------------------------------------------
# Add module specific clean rule if necessary
#------------------------------------------------------------------------------
clean-module::
	$(RM) *.pyc 

#------------------------------------------------------------------------------
#	END OF MODULE SPECIFIC CUSTOMIZATION (below)
#------------------------------------------------------------------------------
#	DO NOT CHANGE OR MOVE THE LINE BELOW
#
# include the "stdrules.mk" file that provides much common functionality.
#------------------------------------------------------------------------------
include $(YAM_ROOT)/etc/SiteDefs/makefile-yam-tail.mk


# NOTE: i've noticed that in $(YAM_ROOT)/etc/SiteDefs/makefile-yam-tail.mk there are
# some rules to deal with swig nd with python, but I could't find any
# documentation for them

#%_wrap.cc %.py: %.i
#	-@echo 'SWIG wrapper generation'
#	swig -python -c++ -o $@ $^

###########################################################################
# Makefile.yam for "trafsim" module
#
# Makefile.yam is the top-level Makefile for the module, and is the one a
# developer should invoke directly to rebuild a single module.
# Invoke it while in the module's checked out src directory.
#
# Makefile.yam - specify source files, public header files and libraries,
#
# This Makefile is used by YAM scripts to build and link a module.
# It should have targets for:
#     yam-mklinks links depends libs bins clean
# even if some are no-ops.
#
# See http://dartslab.jpl.nasa.gov/cgi/dshell-fom.cgi?file=661
#	for more information and Makefile.yam examples
#
# When invoked from the YAM scripts, this Makefile is passed values for
# YAM_NATIVE, YAM_ROOT, YAM_SITE, and YAM_TARGET variables.
#
# Use etc/SiteDefs/Makefile.yam-common to take advantage of some common
# functionality.
#
#       PROJ            - what to build, either the name of a binary executable
#                         or a library (in which case $(PROJ) should start with
#                         "lib" and NOT contain a suffix such as .a or .so).
#                         if left blank, then no source files are compiled.
#			  Links for the libraries automatically get exported 
#			  into the top-level lib/YAM_TARGET, while those for 
#			  binaries get exported to bin/YAM_TARGET.
#                         but the module can contain scripts and header files
#       FLAVORS-<tgt>   - list of "flavors" of the module to build
#			  for the specified binary/library.
#                         If FLAVORS is set to "FOO BAR", then each source
#                         file gets compiled twice into the FOO & BAR
#                         sub-directories of YAM_TARGET. Libraries and binaries
#                         files have "-FOO" or "-BAR" appended to
#                         them. The suffix to use can be set by explicitly setting
#			  the FLAVOR_EXT-<tgt>-<flavor> to the value of the 
#			  desired suffix. No suffix is used if the "-NONE-"
#			  suffix is specified. 
#                         Makefile.yam, and append to CC_SRC (etc.) as
#                         appropriate for that flavor.
#       CC_SRC-<tgt>          - list of .c files to compile for the target
#       CPLUSPLUS_SRC-<tgt>   - list of .cc files to compile for the target
#       MODULE_COMPILE_FLAGS - augments standard C pre-processor flags (-I, -D)
#       MODULE_DEPENDS_FLAGS - flags to use for dependency information 
#       LIBS-<tgt>      - list of libraries to link in for shared libraries
#                         and binary executables for the target
#       LINKER-<tgt>    - set to "CPLUSPLUS" to use the C++ linker
#       CFLAGS-<tgt>    - compilation flags specific to the target
#	DOXYGEN_DOCS    - if "true" then Doxygen docs are generated
#       DOXYGEN_TAGFILES    - names of other modules to create links for in the
#			      Doxygen documentation
#
# The following variables are used by the yam-mklinks and yam-rmlinks rules 
# to export and deleted links for the module to the higher level directories.
#
# BIN_LINKS             - will set up links under ../../bin/
# BIN_MODULE_LINKS      - will set up links under ../../bin/<module>/
#
# Additional available variables can be obtained by replacing "BIN" with
# either of "INC", "ETC", "LIB", "BIN", or "DOC"
#
# Additional variables for target specific links are
#
# BIN_TARGET_LINKS      - will set up links under ../../bin/$(YAM_TARGET)/
# BIN_sparc-sunos5_LINKS - will set up linkvs under ../../bin/sparc-sunos5/
#
# Additional available variables can be obtained by replacing "BIN" with "LIB"
#
#
# You may also augment the default rules by specifiying them in this file.
# Since the "::" versions of the rules are used the effect is to append to
# rather than to replace the default rule.
#
# By default, a module is assumed to be support all the known targets.
# An optional .supported.mk file can be created in the top level module 
# directory to restrict the list of supported targets for the module.
# If you need to disable certain targets for this module then create
# a .supported.mk file in the module's directory and add lines so that 
# the the following variables are set appropriately:
#
#       MODULE_SUPPORTED_TARGETS
#       MODULE_UNSUPPORTED_TARGETS
#       MODULE_SUPPORTED_OS
#       MODULE_UNSUPPORTED_OS 
#
# Also an optional .directives.mk file can be created in the top level 
# module directory to pass on module specific directives to the site-config 
# files.
#
#
