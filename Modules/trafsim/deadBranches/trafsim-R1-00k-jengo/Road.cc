#include "Road.hh"
#include <sstream>
#include <cassert>
#include <GL/gl.h>

namespace TrafSim
{

Road::Road()
{
	deadEnd = new DeadEnd();
	initialized = false;
}

Road::~Road()
{
  cout<<"Road destructor: deleting deadEnd"<<endl;
  delete deadEnd;
  cout<<"end Road destructor"<<endl<<endl;
}

Road::Road(Environment& env, Spline& source, unsigned int numForwardLanes, 
	 unsigned int numReverseLanes, float laneWidth)
{
	deadEnd = new DeadEnd();
	initialize(env, source, numForwardLanes, numReverseLanes, laneWidth);
}

Road* Road::downcast(Object* source)
{
	return dynamic_cast<Road*>(source);
}

void Road::initialize(Environment& env, Spline& _source, unsigned int numForwardLanes, 
					  unsigned int numReverseLanes, float _laneWidth)
{
	source = _source;
	nBackLanes = numReverseLanes;
	nFwdLanes = numForwardLanes;
	laneWidth = _laneWidth;
	
	source.cacheData();
	
	for (unsigned int i = 0; i < nBackLanes; ++i)
	{
		Spline newSpline(source, -(i + 0.5f) * laneWidth, 0.05f, true);
		Lane* newLane = new Lane(newSpline);
		
		std::ostringstream nameStream;
		nameStream << this->getName() << "Lane" << "R" << i;
		
		env.addObject(newLane, nameStream.str());
		newLane->setParent(this->getName());
		newLane->spline->cacheData();
		lanes.push_back(newLane);
	}
	
	for (unsigned int i = 0; i < nFwdLanes; ++i)
	{
		Spline newSpline(source, (i + 0.5f) * laneWidth, 0.05f, false);
		Lane* newLane = new Lane(newSpline);
		
		std::ostringstream nameStream;
		nameStream << this->getName() << "Lane" << "F" << i;
		
		env.addObject(newLane, nameStream.str());
		newLane->setParent(this->getName());
		newLane->spline->cacheData();
		lanes.push_back(newLane);
	}
	
	start = deadEnd;
	end = deadEnd;	
	speedLimit = 13.5;
	initialized = true;
}

float Road::getSpeedLimit() const
{
	return speedLimit;
}

void Road::setSpeedLimit(float limit)
{
	assert(limit > 0.0f);
	speedLimit = limit;	
}

void _drawOffset(Spline const& s, Color const& c, float offset)
{
	const Vector* coords = s.getCoordinateData();
	const Vector* norms =  s.getNormalData();

	glSetColor(c);
	glBegin(GL_LINE_STRIP);	
	for (unsigned int i = 0; i < s.getResolution(); ++i)
	{
		Vector const& coord = coords[i];
		Vector const& norm  = norms[i];
		
		glVertex3f(coord.x + norm.x * offset,
				   coord.y + norm.y * offset,
				   0.0f);
	}
	glEnd();
}

void Road::draw()
{
	if (!initialized)
		return;

	glPushMatrix();
	
	for (int side = -1; side <= 1; side += 2)
	{
		int nLanes = (side == -1) ? nBackLanes : nFwdLanes;
		
		// boundaries
		_drawOffset(source, Color(0.0f, 0.0f, 1.0f, 1.0f),
					side * laneWidth * nLanes);
		
		// double yellow line
		_drawOffset(source, Color(1.0f, 1.0f, 0.0f, 1.0f),
					side * laneWidth * 0.1f);
					
		glLineStipple(2, 0xFFF0);
		// white dotted lines
		for (int i = 0; i < nLanes - 1; ++i)
			_drawOffset(source, Color(1.0f, 1.0f, 1.0f, 1.0f),
						side * laneWidth * (i + 1));
	
		glLineStipple(2, 0xFFFF);
	}
	
	glPopMatrix();
}

Lane* Road::getLane(unsigned int index)
{
	assert (index < nFwdLanes + nBackLanes);
	return lanes[index];
}

Lane* Road::getForwardLane(unsigned int index)
{
	assert (index < nFwdLanes);
	return lanes[nBackLanes + index];
}

Lane* Road::getReverseLane(unsigned int index)
{
	assert (index < nBackLanes);
	return lanes[index];
}

bool Road::isReverse(unsigned int lane) const
{
	return lane < nBackLanes;
}

unsigned int Road::numLanes() const { return nFwdLanes + nBackLanes; }
unsigned int Road::numForwardLanes() const { return nFwdLanes; }
unsigned int Road::numReverseLanes() const { return nBackLanes; }
	
Spline& Road::getSpline() { return source; }
Intersection* Road::getStart() { return start; }
Intersection* Road::getEnd() { return end; }
void Road::setStart(Intersection* newStart) { start = newStart; } 
void Road::setEnd(Intersection* newEnd) { end = newEnd; }

float Road::getLaneWidth() const { return laneWidth; }
	
Vector Road::getBoundary(float t, int side, bool reversed)
{
	assert(initialized);
	
	Vector norm = source.evalNormal(t);
	Vector coord = source.evalCoordinate(t);

	if (reversed)
		side *= -1;
		
	if (side == 1)
		return coord + norm * laneWidth * nFwdLanes;
	else if (side == -1)
		return coord - norm * laneWidth * nBackLanes;
	else
		assert(false);	
		
	return Vector();
}	

Vector Road::getLaneBoundary(float t, unsigned int laneNumber)
{
  unsigned int totalLanes = nFwdLanes + nBackLanes;
 
  // if the lane number given is smaller than the total number of 
  // lanes, return the left road boundary
  if (laneNumber < 0) {
    return getBoundary(t,-1);
  }

  // or if it is larger than the total number of lanes,
  // return the right road boundary
  else if (laneNumber > totalLanes) {
    return getBoundary(t,1);
  }

  else {
	
    Vector norm = source.evalNormal(t);
    Vector coord = source.evalCoordinate(t);
		
    // figure out whether the requested lane is forward
    // or backward

    if (laneNumber <= nBackLanes) {
      // need to change this since we're measuring from the center line
      laneNumber = nBackLanes - laneNumber;
      return coord - norm * laneWidth * laneNumber;
    }
    else {
      laneNumber = laneNumber - nBackLanes;
      return coord + norm * laneWidth * laneNumber;
    }

  }

}

std::vector<point2> Road::getRoadBoundary(int side, double spacing, bool reversed)
{

  vector<point2> boundary;
  double currentLocation = 0;
  Vector currentPoint;

  // go along the side of the road 
  while (currentLocation <= 1) {
    
    currentPoint = getBoundary(currentLocation, side);
    boundary.push_back(currentPoint.convertToPoint2());

    currentLocation += spacing;

  }

  return boundary;

}
 

std::vector< std::vector<point2> > Road::getLaneBoundaries(double spacing)
{

  int totalLanes = nFwdLanes + nBackLanes;

  vector< vector<point2> > boundaries;
  vector<point2> currentBoundary;
  double currentLocation;
  Vector currentPoint;

  // get left road boundary
  boundaries.push_back(getRoadBoundary(-1,spacing));

  // get intermediate lanes
  for (int i = 1; i<totalLanes; i++) {

    currentBoundary.clear();
    currentLocation = 0;
    currentPoint.x = 0;
    currentPoint.y = 0;

    // go along a lane 
    while (currentLocation <= 1) {
    
      currentPoint = getLaneBoundary(currentLocation, i);
      currentBoundary.push_back(currentPoint.convertToPoint2());
      
      currentLocation += spacing;
      
    }
    boundaries.push_back(currentBoundary);
  }

  // get right road boundary
  boundaries.push_back(getRoadBoundary(1,spacing));

  return boundaries;

}
 
float Road::getOrientation()
{
  // return spline's orientation
  return source.getOrientation();
}

float Road::getLength()
{
  // return spline's length
  return source.getLength();
}

float Road::getWidth()
{
  int numLanes = nFwdLanes + nBackLanes;
  return numLanes*laneWidth;
}

std::vector<point2> Road::getBoundary()
{
  // the spacing is a default of .5 meters
  // get each side of the road, and append them
  // right side
  std::vector<point2> boundary = getRoadBoundary(1, .5);
  // left side
  std::vector<point2> leftBoundary = getRoadBoundary(-1,.5,true);
  boundary.insert(boundary.end(),
		  leftBoundary.begin(),
		  leftBoundary.end());

  return boundary;
}

bool Road::containsObject(Object* obj)
{

  // only checks for lanes

  for (vector<Lane*>::iterator i = lanes.begin(); i != lanes.end(); i++)
    {
      if (*obj == *i) return true;
    }

  return false;

}


void Road::serialize(std::ostream& os) const
{
	std::vector<std::string> nameArray;
	for (std::vector<Lane*>::const_iterator i = lanes.begin();
		 i != lanes.end(); ++i)
		nameArray.push_back((*i)->getName());
		
	os << "source --> " << (Object*)&source << std::endl;
	os << "lanes --> " << nameArray << std::endl;
	os << "laneWidth --> " << laneWidth << std::endl;
	os << "nFwdLanes --> " << nFwdLanes << std::endl;
	os << "nBackLanes --> " << nBackLanes << std::endl;
	os << "speedLimit --> " << speedLimit << std::endl;
	os << "start --> " << (start != deadEnd? start->getName(): "None") << std::endl;
	os << "end --> " << (end != deadEnd? end->getName(): "None") << std::endl;
}

void Road::deserialize(std::istream& is)
{
	std::vector<std::string> nameArray;
	std::string startName, endName;
	
	Object* objSource;
	DESERIALIZE_RN(is, "source", objSource);
	source = *(Spline*)objSource;
	// BUG: there's a small memory leak here due to us being unable
	// to safely delete objSource, but whatever. It's not very important.
	
	DESERIALIZE_RN(is, "lanes", nameArray);
	DESERIALIZE(is, laneWidth);
	DESERIALIZE(is, nFwdLanes);
	DESERIALIZE(is, nBackLanes);
	DESERIALIZE(is, speedLimit);
	DESERIALIZE_RN(is, "start", startName);
	DESERIALIZE_RN(is, "end", endName);
	
	if (startName == "None")
		start = deadEnd;
	else
		this->_addReference(startName, (Object**)&start);
	
	if (endName == "None")
		end = deadEnd;
	else
		this->_addReference(endName, (Object**)&end);
	
	lanes.resize(nameArray.size());
	for (unsigned int i = 0; i < nameArray.size(); ++i)
	{
		lanes[i] = ((Lane*)0);
		this->_addReference(nameArray[i], (Object**)&lanes[i]);
	}
	
	initialized = true;
}


}
