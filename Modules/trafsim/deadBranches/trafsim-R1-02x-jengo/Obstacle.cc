#include "Obstacle.hh"
#include <cmath>
#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <time.h>

namespace TrafSim {

  Obstacle::Obstacle() 
  {
    position = point2(0,0);
    length = 2;
    width = 2;
    orientation = 0;
    setBlockObs();
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;

    isHappy = false;

    velocity = point2(0,0);

    srand((unsigned)time(0));
  }
  
  Obstacle::Obstacle(float x, float y, float l, float w, float o, bool v)
    : orientation(o), length(l), width(w)
  {
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    position = point2(x,y);

    while (orientation > M_PI)
      orientation -= 2*M_PI;
    while (orientation < -1*M_PI)
      orientation += 2*M_PI;

    isHappy = false;

    setBlockObs();

    velocity = point2(0,0);

    if (v)
      setVegetation();
    
  }

  Obstacle::Obstacle(float x, float y, float radius) 
    : orientation(0), length(radius), width(radius)
  {
    type = CIRCLE;
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    position = point2(x,y);
    setCircleObs();

    velocity = point2(0,0);

    isHappy = false;
  }
    

  Obstacle::~Obstacle() 
  {

  }

  void Obstacle::setPosition(float x, float y)
  {
    position.x = x;
    position.y = y;
  }

  void Obstacle::setPosition(point2 pos)
  {
    position = pos;
  }

  void Obstacle::setOrientation(float newOrientation)
  {
    // if the desired orientation isn't between -pi and pi,
    // fix it
    while (newOrientation > M_PI)
      newOrientation -= 2*M_PI;
    while (newOrientation < -1*M_PI)
      newOrientation += 2*M_PI;
  
    orientation = newOrientation;
    /*
    // need to recalculate boundaries
    if (type == BLOCK)
      setBlockObs();
    else if (type == CIRCLE) 
      setCircleObs();
    else { // vegetation
      setBlockObs();
      setVegetation();
    }
    */
  }

  void Obstacle::setBlockObs()
  {
    type = BLOCK;
    boundary.clear();

    double x = position.x;
    double y = position.y;
    double ly = width;
    double lx = length;
    double t = orientation;
      
    // put in the points without rotation
    boundary.push_back(point2(.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,-.5*ly));
    boundary.push_back(point2(.5*lx,-.5*ly));
      
    // rotate points
    double ca = cos(t);
    double sa = sin(t);
    for (std::vector<point2>::iterator iter = boundary.begin();
	 iter != boundary.end(); iter++)
      *iter = point2(iter->x*ca - iter->y*sa + x,
		     iter->x*sa + iter->y*ca + y);

    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;    

  }

  void Obstacle::setCircleObs()
  {
    type = CIRCLE;
    boundary.clear();

    double x = position.x;
    double y = position.y;
    double a = cos(M_PI/4);
    double r = length;
    boundary.push_back(point2(r+x   ,0+y));
    boundary.push_back(point2(r*a+x ,r*a+y));
    boundary.push_back(point2(0+x   ,r+y));
    boundary.push_back(point2(-r*a+x,r*a+y));
    boundary.push_back(point2(-r+x  ,0+y));
    boundary.push_back(point2(-r*a+x,-r*a+y));
    boundary.push_back(point2(0+x   ,-r+y));
    boundary.push_back(point2(r*a+x ,-r*a+y));

    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;    

  }

  void Obstacle::setVegetation()
  {
    type = VEGETATION;

    // take the original boundary and add noise to it
    // to make things more interesting

    vector<point2> newBoundary;
    
    vector<point2>::iterator nextpt;
    for (vector<point2>::iterator currpt = boundary.begin();
	 currpt != boundary.end(); currpt++) {
      nextpt = currpt+1;
      newBoundary.push_back(*currpt);
      if (nextpt != boundary.end()) 
	newBoundary.push_back(point2( (currpt->x+nextpt->x)/2 ,
				      (currpt->y+nextpt->y)/2));      
    }    


    boundary = newBoundary;

    double minNoise = 0.1;
    double rangeNoise = 1;
    double noise = .5;

    for (vector<point2>::iterator it = boundary.begin();
	 it != boundary.end(); it++) {
      noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);
      it->x = it->x + noise;
      noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);
      it->y = it->y + noise;
    }

    color.r = 0.0;
    color.g = 1.0;
    color.b = 0.0;
    color.a = 1.0;

    // set the path for swaying
    // go slowly towards final pos, hold there for some time,
    // then fairly quickly come back
    double ca = cos(orientation);
    double sa = sin(orientation);
    double currDist = 0;

    minNoise = -.25;
    rangeNoise = .5;
    noise = 0;

      noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);

    // go forward
    while (currDist < SWAYING_DIST + noise) {

      currDist += SWAYING_SPEED*TIMESTEP;
      path.push_back(point2(currDist*ca,currDist*sa));

    }

    // hang there for 1 sec
    for (int i = 0; i < 10; i++) {
      path.push_back(point2(currDist*ca,currDist*sa));
    }

    noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);

    // come back
    while (currDist > noise) {

      currDist -= SWAYING_SPEED*TIMESTEP;
      path.push_back(point2(currDist*ca,currDist*sa));

    }

    // hang there for .5 sec
    for (int i = 0; i < 5; i++) {
      path.push_back(point2(currDist*ca,currDist*sa));
    }

    // set the iterator to the beginning of the path
    currVegPos = path.begin();

  }

  void Obstacle::draw() 
  {
    // draw a light blue object
    
    glPushMatrix();
    //    glTranslatef(position.x, position.y, 0.0f);
    //    glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
    glSetColor(color);

    glBegin(GL_POLYGON);

    for (vector<point2>::iterator it = boundary.begin();
	 it != boundary.end(); it++) {	
      glVertex3f(it->x, it->y, 0.0f);      
    }

    glEnd();
    glPopMatrix();

    if (isHappy) {

      double x = position.x;
      double y = position.y;

      string txt = "";
      int len = 0;

      if (type==VEGETATION)
	txt = ">:D";
      /*
      else if (type==BLOCK)
	txt = "D:";
      else if (type==CIRCLE)
	txt = ":)";
      */
      else txt = "";

      len = (int)txt.size();

      glPushMatrix();

      glTranslatef(x,y-width/3, 0.0f);
      glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
      glScalef(length*0.003f,width*0.005f,1.0f);
      glSetColor(Color(0.0f,0.0f,0.0f,1.0f));
      
      for (int i =0 ; i < len ; ++i){
	glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, txt[i]);
      }

      glPopMatrix();
      /*
      glSetColor(Color(0.0f,0.0f,0.0f,0.0f));

      glRasterPos2f(x,y);

      double smallLength = width;
      if (length < width)
	smallLength = length;

      if (smallLength < 3) {
	for (int i =0 ; i < len ; ++i){
	  glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, txt[i]);
	}
      }
      else if (smallLength >3 && smallLength <
      */
    }

  }

  bool Obstacle::invertColor() {
    color.r = 1.0f - color.r;
    color.b = 1.0f - color.b;
    return true;
  }

  bool Obstacle::simulate(float ticks) 
  {

    // if vegetation, move it to next pt in the path
    if (type == VEGETATION) {

      point2 posDiff = *currVegPos;
      currVegPos++;
      if (currVegPos == path.end())
	currVegPos = path.begin();

      posDiff = posDiff - *currVegPos;
      posDiff = posDiff * -1;

      position = position + posDiff;

      for (vector<point2>::iterator it = boundary.begin();
	   it != boundary.end(); it++) {	
	*it = *it + posDiff;
      }

      velocity = posDiff/ticks;
      velocity = velocity/6;

    }

    return true;
  }

  float Obstacle::getVelocity()
  {

    if (type == VEGETATION)
      return sqrt(velocity.x*velocity.x + velocity.y*velocity.y);
    else
      return 0;

  }

  vector<vector<point2> > Obstacle::getBoundary()
  {
    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    return boundaryWrapper;
  }

  Obstacle* Obstacle::downcast(Object* source)
  {
    return dynamic_cast<Obstacle*>(source);
  }

  void Obstacle::serialize(std::ostream& os) const
  {
    // NOT IMPLEMENTED
  }

  void Obstacle::deserialize(std::istream& is)
  {
    // NOT IMPLEMENTED
  }

}
