RNDF_name	TRAFSIM_TEST_RNDF	
num_segments	1	
num_zones	0	
format_version	1.0	
creation_date	11/27/2006


segment	1	
num_lanes	2	
segment_name	Fast_Lane	
lane	1.1	
num_waypoints	8	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
1.1.1   34.167381       -118.091000
1.1.2   34.167381       -118.092000
1.1.3   34.167381       -118.093000
1.1.4   34.167381       -118.094000
1.1.5   34.167381       -118.095000
1.1.6   34.167381       -118.096000
1.1.7   34.167381       -118.097000
1.1.8   34.167381       -118.098000
end_lane		
lane	1.2	
num_waypoints	8	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white
checkpoint	1.2.5	1
1.2.1   34.167381       -118.098000
1.2.2   34.167381       -118.097000
1.2.3   34.167381       -118.096000
1.2.4   34.167381       -118.095000
1.2.5   34.167381       -118.094000
1.2.6   34.167381       -118.093000
1.2.7   34.167381       -118.092000
1.2.8   34.167381       -118.091000
end_lane		
end_segment		
		
end_file		


