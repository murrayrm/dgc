#ifndef SPLINE_H_
#define SPLINE_H_

#include "Vector.hh"
#include "Color.hh"
#include "Object.hh"
#include "LinearSegmentDesc.hh"
#include <vector>
#include <utility>

namespace TrafSim
{
	
class Spline;

/**
 * A helper struct for use with the Spline constructor that stitches together
 * multiple splines into one big one. Describes a subsection of a spline.
 */
typedef TrafSim::LinearSegmentDesc<Spline> JoinDesc;
typedef std::pair<JoinDesc, JoinDesc> SplineInterferenceDesc;

/**
 * An elaborate and fairly efficient implementation of Catmull-Rom splines.
 * Supports everything you might want to do with a spline, and some things you
 * probably wouldn't want to do.
 */
class Spline: public Object
{
public:

	/* ********** CONSTRUCTORS AND DESTRUCTORS *********** */

	/// The default constructor starts the spline in an uninitialized state. Use one of
	/// the initialize functions to get it to start working.
	Spline();
	
	/// This constructor creates a spline from a list of control points.
	/// \param points The array of control points to use. Can be modified later, but 
	///      the spline will not work correctly until there are >= 4 control points.
	///		 This array is copied into the spline class by value.
	/// \param resolution The number of data points to be stored in a call to cacheData
	Spline(std::vector<Vector> const& points, 
		   unsigned int resolution = 100);
		   
	/// This constructor creates a spline running "parallel" to another spline, displaced by
	/// a floating point parameter.
	/// \param original The old spline to base the new spline's data on. If original has not 
	///       cached its data, this call will do it. Otherwise, the spline remains unchanged.
	/// \param displacement Can be positive or negative or zero.
	/// \param relativeResolution 1.0 over the number of control points in the new spline.
	///		  Must be >= 1.0 / original.cacheResolution.
	/// \param reversed If true, the new spline will be constructed backwards, so that its t=1
	///		  coordinate will correspond to the original's t=0 coordinate, and vice versa.
	Spline(Spline& original, float displacement,
		   float relativeResolution = 0.05f, bool reversed = false);
		   	
	/// This constructor creates a new spline by stitching together a number of other splines.
	/// The new spline will NOT have any control points defined; it will exist only as cached data.
	/// \param parts See the description of the JoinDesc data structure
	Spline(std::vector<JoinDesc> const& parts);
	
	/// The destructor just deletes allocated member data
	virtual ~Spline();
	
	/* ********** INITIALIZATION FUNCTIONS *********** */
	
	/// This initializer creates a spline from a list of control points.
	/// \param points The array of control points to use. Can be modified later, but 
	///      the spline will not work correctly until there are >= 4 control points.
	///		 This array is copied into the spline class by value.
	/// \param resolution The number of data points to be stored in a call to cacheData
	void initialize(std::vector<Vector> const& points, 
					unsigned int resolution = 100);
					
	/// This initializer creates a spline running "parallel" to another spline, displaced by
	/// a floating point parameter.
	/// \param original The old spline to base the new spline's data on. If original has not 
	///       cached its data, this call will do it. Otherwise, the spline remains unchanged.
	/// \param displacement Can be positive or negative or zero.
	/// \param relativeResolution 1.0 over the number of control points in the new spline.
	///		  Must be >= 1.0 / original.cacheResolution.
	/// \param reversed If true, the new spline will be constructed backwards, so that its t=1
	///		  coordinate will correspond to the original's t=0 coordinate, and vice versa.
	void initialize(Spline& original, float displacement,
					float relativeResolution = 0.05f, bool reversed = false);
					
	/// This initializer creates a new spline by stitching together a number of other splines.
	/// The new spline will NOT have any control points defined; it will exist only as cached data.
	/// \param parts See the description of the JoinDesc data structure
	std::vector<unsigned int> 
		initialize(std::vector<JoinDesc> const& parts);
	
	/// A fairly expensive operation that calculates coorinate, normal, length, and curvature
	/// data at evenly spaced intervals along the spline. It must be called before length-related
	/// functions will work, and it will speed up all the other operations by at least an order
	/// of magnitude. If a spline's data is not likely to change often, it is a good idea to call
	/// this function, as it will make processing MUCH more efficient.
	/// \param regenerate Ordinarily, this function will do nothing if the spline is already cached.
	///			However, it is sometimes necessary to force re-caching; set regenerate to true to do this.
	void cacheData(bool regenerate = false);
	
	/// \return Whether or not cacheData has been called on this spline
	bool isCached() const;
	
	/* ********** DATA QUERYING FUNCTIONS *********** */
	
	/// Returns the (x,y) coordinates of the point of the spline corresponding to point t.
	/// \param t 0 < t < 1. The results for an out-of-range t are undefined.
	/// \return A coordinate vector.
	Vector evalCoordinate(float t) const;
	
	/// Returns the (normalized) normal vector to the spline corresponding to point t.
	/// All normals face in the same direction relative to the positive direction of the spline.
	/// \param t 0 < t < 1. The results for an out-of-range t are undefined.
	/// \return A normal vector
	Vector evalNormal(float t) const;
	
	/// Returns the curvature (1.0 / radius of curvature) of the spline at point t.
	/// \param t 0 < t < 1. The results for an out-of-range t are undefined.
	/// \return Curvature, in m^-1
	float  evalCurvature(float t) const;
	
	/// Returns the physical path length of the spline between points t1 and t2.
	/// If spline.isCached() is false, this function will always return -2.0f.
	/// \param t1 The starting point to measure from
	/// \param t2 The ending point to measure to
	/// \return The length of the section of spline, in world coordinates (meters)
	float evalLength(float t1, float t2) const;
	
	/// Returns the t-coordinate of the point on the spline that corresponds to moving
	/// forward from a previous point by a certain distance, in world coordinates (meters).
	/// If spline.isCached() is false, this function will always return -2.0f.
	/// \param t0 The starting point to measure from
	/// \param dist The distance, in world coordinates, to measure
	/// \return The t-coordinate of the new point
	float evalDisplacement(float t0, float dist) const;
	
	/// Returns the t-coordinate of the point on the spline that's physically closest to the
	/// given point. WARNING: this function is expensive, and requires spline.isCached().
	/// \param point The point to minimize distance to
	/// \param startIndex The index of the first cached data point to consider
	/// \param endIndex The index of the last cached data point to consider
	/// \return The t-coordinate of the closest point
	float closestToPoint(Vector const& point, unsigned int startIndex = 0, unsigned int endIndex = -1) const;
	
	/// Returns a list of coordinates in which the given spline draws closer to otherSpline than
	/// the parameter 'threshold'. This is an expensive operation, and requires both splines to be cached.
	/// \param otherSpline The spline to scan for "interference"
	/// \param result The output of the function
	/// \param threshold The maximum distance between spline coordinates to be included in the result
	/// \param subResolution The numer of data points to consider between the cached points
	/// \return The t-coordinate of the closest point
	void getInterference(Spline& otherSpline, std::vector<SplineInterferenceDesc>& result, 
						 float threshold, float subResolution = 10.0f);
	
	/// \return The number of data points used in the approximate spline data
	unsigned int getResolution() const;
	
	/// \return A pointer to the raw vertex data
	const Vector* getCoordinateData() const;
	
	/// \return A pointer to the raw normal data
	const Vector* getNormalData() const;
	
	/// \return A pointer to the raw length data
	const float*  getLengthData() const;
	
	/// \return A pointer to the raw curvature data
	const float*  getCurvatureData() const;
	
	/// \param t A t coordinate to query
	/// \return The location of the given t coordinate in the cache
	std::pair<unsigned int, float> _mapTCoord(float t) const;
	
	/* ********** GENERIC OBJECT FUNCTIONS *********** */
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a Spline
	/// \return A Spline*, pointing to the same object as source.	
	static Spline* downcast(Object* source);
	
	virtual std::string getClassID() const { return "Spline"; }
	
	/// Renders the entire spline to the screen. Call this from a thread in which a Viewport is open.
	virtual void draw();
	
	/// Renders a portion of the spline to the screen. Call this from a thread in which a Viewport is open.
	/// \param t0 The t coordinate to render from
	/// \param t1 The t coordinate to render to
	void draw(float t0, float t1);
	
	/* ********** PUBLIC DATA MEMBERS *********** */
	
	/// The control points of the spline. Public, since all operations on this list are supported.
	std::vector<Vector> points;
	
	/// The color used to draw the spline. Change it at will.
	Color color;
	
private:

	// writes the string representation of the spline
	virtual void serialize(std::ostream& os) const;

	// reads the string representation of a spline
	virtual void deserialize(std::istream& is);

	// raw evaluator
	Vector _catmullRomEval(float t) const;
	
	// helper function for the raw evaluator
	float  _basis(unsigned int i, float t) const;
	
	// helper function for cache lookup functions
	float _mapIndex(unsigned int index) const;

	// interpolates between two cached data points
	template <class T> T _interpolate(T* field, float t) const
	{
		std::pair<unsigned int, float> coordInfo = _mapTCoord(t);
        if (coordInfo.first == cacheResolution)
        	return field[cacheResolution];
        
        return field[coordInfo.first + 1] * coordInfo.second + 
        	   field[coordInfo.first] * (1.0f - coordInfo.second);
	}

	unsigned int cacheResolution; // number of cached data points - 1
	
	Vector* coordinates;  // vertex data
	Vector* normals;	  // vertex normal data
	float*  lengths;	  // physical distance between consecutive vertices
	float*  curvature;	  // 1.0f / radius of curvature at each data point
	
	friend float curveConstraint(Spline const& spline, float t, float vel, 
								 float linearAccel, float angularAccel);
};

}

#endif /*SPLINE_H_*/
