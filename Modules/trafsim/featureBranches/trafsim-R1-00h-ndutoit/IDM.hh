#ifndef IDM_H_
#define IDM_H_

#include "CarProperties.hh"

namespace TrafSim
{

/**
 * Uses the Intelligent Driver Model to find an appropriate acceleration for a car.
 * \param myVel The velocity of the car under consideration
 * \param otherVel The velocity of the car in front of our car
 * \param gap The distance in meters between our car and the car in front. If there IS no car in front, set this to -1.
 * \param targetVel The "desired" velocity for our car (can be calculated from curveConstraint)
 * \param myProps The properties of the car under consideration
 * \param otherProps The properties of the car in front
 * \return An appropriate acceleration for our car
 */
float evalIDMAccel(float myVel, float otherVel, float gap, float targetVel, 
				   CarProperties const& myProps, CarProperties const& otherProps);

};

#endif /*IDM_H_*/
