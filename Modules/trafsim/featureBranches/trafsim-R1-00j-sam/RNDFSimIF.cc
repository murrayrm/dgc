#include "RNDFSimIF.hh"
#include <cmath>

namespace TrafSim
{

RNDFSimIF::RNDFSimIF() {
    rndf = new RNDF();
    
    xTranslate = 0;
    yTranslate = 0;
}

RNDFSimIF::~RNDFSimIF() {
    delete rndf;
}

//SEG FAULTS!!!
//void RNDFSimIF::resetRNDF() {
//    delete rndf;
//    rndf = new RNDF();
//
 //   xTranslate = 0;
 //   yTranslate = 0;
//}

bool RNDFSimIF::loadFile(char* filename) {
   return rndf->loadFile(filename);
}

double RNDFSimIF::getxTranslate() {
    return xTranslate;
}

double RNDFSimIF::getyTranslate() {
    return yTranslate;
}

void RNDFSimIF::mergeLanesinSegments() {
    //The member function may be a bad idea after all...
}

int RNDFSimIF::getNumSegs() {
    return rndf->getNumOfSegments();
}

void RNDFSimIF::appendStartEnd(std::vector<Vector> *points, int flags)
{
    if (flags && APPEND_START)
    {
    	double X2 = points->at(0).x;
        double X1 = points->at(1).x;
        double Y2 = points->at(0).y;
        double Y1 = points->at(1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->begin();
	points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
    
    if (flags && APPEND_END)
    {
        int last = points->size() - 1;

        double X2 = points->at(last).x;
        double X1 = points->at(last - 1).x;
        double Y2 = points->at(last).y;
        double Y1 = points->at(last - 1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->end();
        points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
}

std::vector<Vector> RNDFSimIF::getPointsForSpline(int seg, int lane) {
    std::vector<Vector> points;
    std::vector<Vector>::iterator pointsIter;

    Segment* seg1 = rndf->getSegment(seg);
    Lane* lane1 = seg1->getLane(lane);
    Lane* lane2 = seg1->getLane(lane+1);
    std::vector<Waypoint*> waypoints = lane1->getAllWaypoints();
    std::vector<Waypoint*> wpoints2  = lane2->getAllWaypoints();
 
    std::vector<Waypoint*>::iterator iter;
    std::vector<Waypoint*>::iterator iter2;
    
    iter2 = wpoints2.end();
    iter2--;
    for(iter = waypoints.begin(); iter != waypoints.end(); iter++) {
	points.push_back(
	    Vector((((*iter)->getEasting()+(*iter2)->getEasting())/2), 
	           (((*iter)->getNorthing()+(*iter2)->getNorthing())/2)));
       
       if(iter2 != wpoints2.begin())
            iter2--;
    }
       
    appendStartEnd(&points);

    return points;
}


void RNDFSimIF::centerAllWaypoints() {
    vector<Segment*> segs = rndf->getAllSegments();
    vector<Segment*>::iterator segIter;

    double largestNorthing = 0, smallestNorthing = 0;
    double largestEasting = 0, smallestEasting = 0;
    bool hack = true;

    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        vector<Lane*> lanes = (*segIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;

	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
            vector<Waypoint*>::iterator iter;
            
	    for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
	        double tempHeadingN = (*iter)->getNorthing();
	        double tempHeadingE = (*iter)->getEasting();
	        
		if(hack) {
		     largestNorthing = tempHeadingN;
		     largestEasting = tempHeadingE;
		     smallestNorthing = tempHeadingN;
		     smallestEasting = tempHeadingE;
                     hack = false;
                }
	        
		if(tempHeadingN >= largestNorthing)
	            largestNorthing = tempHeadingN;
	
	        else if(tempHeadingN < smallestNorthing)
	            smallestNorthing = tempHeadingN;

	        if(tempHeadingE >= largestEasting)
	            largestEasting = tempHeadingE;

	        else if (tempHeadingE < smallestEasting)
	            smallestEasting = tempHeadingE;
            }
	}
    }

    yTranslate = 0.5*(largestNorthing + smallestNorthing);
    xTranslate = 0.5*(largestEasting + smallestEasting);

    
    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        vector<Lane*> lanes = (*segIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;
        
	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
   
            vector<Waypoint*>::iterator iter;
            for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
                (*iter)->setNorthingEasting(((*iter)->getNorthing()-yTranslate)                                          ,((*iter)->getEasting()-xTranslate));
            }
        }
    }
}
	

std::vector< std::vector<Vector> > RNDFSimIF::getPointsForAllSplines()
{
    std::vector< std::vector<Vector> > allPoints;
    std::vector<Segment*> segs = rndf->getAllSegments();
    std::vector<Segment*>::iterator segIter;

    for(segIter = segs.begin(); segIter != segs.end(); segIter++)
    {
        std::vector<Lane*> lanes = (*segIter)->getAllLanes();
        std::vector<Lane*>::iterator laneIter;

        DirDist* laneOrder = new DirDist[(*segIter)->getNumOfLanes()];

        Waypoint* lane1start = (*segIter)->getLane(1)->getWaypoint(1);
        Waypoint* lane1second = (*segIter)->getLane(1)->getWaypoint(2);
        Vector lane1(lane1start->getEasting(), lane1start->getNorthing());
        Vector lane1dir(lane1second->getEasting(), lane1second->getNorthing());
        lane1dir -= lane1;
        
        for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++)
        {
            int laneNum = (*laneIter)->getLaneID();
            Waypoint* laneCompStart = (*laneIter)->getWaypoint(1);
            Waypoint* laneCompSecond = (*laneIter)->getWaypoint(2);
            Vector laneComp(laneCompStart->getEasting(), laneCompStart->getNorthing());
            Vector laneCompDir(laneCompSecond->getEasting(), laneCompSecond->getNorthing());
            laneCompDir -= laneComp;
            
            if ((lane1dir.x * laneCompDir.y - laneCompDir.x * lane1dir.y) > 0)
                laneOrder[laneNum].dir = LEFT_A;
            else
                laneOrder[laneNum].dir = RIGHT_A;
            laneOrder[laneNum].dist = (lane1 - laneComp).length();
        }
        std::vector< Vector > cuts;

        for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++)
        {
            std::vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
            std::vector<Waypoint*>::iterator wayIter;

            for(wayIter = wpoints.begin(); wayIter != wpoints.end(); wayIter++)
            {
                if (((*wayIter)->isExit()) || ((*wayIter)->isEntry()))
                {
 Vector cut((*wayIter)->getEasting(), (*wayIter)->getNorthing());
                    int laneNum = (*laneIter)->getLaneID();
                    Vector *left;
		    if (wayIter == wpoints.begin())
                        left = new Vector((*wayIter)->getEasting(), (*wayIter)->getNorthing());
		    else
                    {
                        wayIter--;
                        left = new Vector((*wayIter)->getEasting(), (*wayIter)->getNorthing());
			wayIter++;
                    }
		    Vector *right;
		    if ((wayIter++) == wpoints.end())
		    {
                        wayIter--;
			right = new Vector((*wayIter)->getEasting(), (*wayIter)->getNorthing());
		    }
		    else
                    {
                        right = new Vector((*wayIter)->getEasting(), (*wayIter)->getNorthing());
			wayIter--;
		    }
                    Vector dirVec = *right - *left;
		    delete right;
		    delete left;
                    Vector perp(- dirVec.y, dirVec.x);
                    perp.normalize();
                    Dir dir;
                    if ((dirVec.x * perp.y - perp.x * dirVec.y) > 0)
                        dir = LEFT_A;
                    else
                        dir = RIGHT_A;
                    if (dir != laneOrder[laneNum].dir)
                        perp *= -1;
                    cut += (perp * laneOrder[laneNum].dist);
                    std::vector< Vector >::iterator cutIter;
                    int hack = 1;
                    for(cutIter = cuts.begin(); cutIter != cuts.end(); cutIter++)
                    {
                        if (((*cutIter) - cut).length() < SAME_DIST)
                        {
                            hack = 0;
                            break;
                        }
                    }
                    if (hack)
                        cuts.push_back(cut);
                }
            }
        }
        delete[] laneOrder;
	       
        std::vector<Vector> points;
        std::vector<Vector>::iterator pointsIter;

        std::vector<Waypoint*> wayPoints = (*segIter)->getLane(1)->getAllWaypoints();
        std::vector<Waypoint*>::iterator wayIter;

        for(wayIter = wayPoints.begin(); wayIter != wayPoints.end(); wayIter++) 
        {
            points.push_back(Vector((*wayIter)->getEasting(), (*wayIter)->getNorthing()));
        }

        appendStartEnd(&points);
        if(!cuts.empty())
        {
            std::vector<Vector>::iterator cutIter;
            std::vector<Vector> cutNext;
            std::vector<Vector> cutPrev;
            cutNext = points;
            for(cutIter = cuts.begin(); cutIter != cuts.end(); cutIter++)
            {
                Vector leftVect, rightVect;
                double squareSum = -1;
                pointsIter = cutNext.begin();
                pointsIter++;
                for (; pointsIter != cutNext.end(); pointsIter++)
                {
                    pointsIter--;
                    Vector tempLeft((*pointsIter).x, (*pointsIter).y);
                    pointsIter++;
                    Vector tempRight((*pointsIter).x, (*pointsIter).y);

                    double newDist = tempLeft.dist2(tempLeft, (*pointsIter)) * tempLeft.dist2(tempLeft, (*pointsIter)) +
                                     tempRight.dist2(tempRight, (*pointsIter)) * tempRight.dist2(tempRight, (*pointsIter));

                    //Minimize sum of squares
                    if(squareSum == -1)
                    {
                        squareSum = newDist;
                        leftVect = tempLeft;
                        rightVect = tempRight;
                    }

                    if(newDist <= squareSum)
                    {
                        squareSum = newDist;
                        leftVect = tempLeft;
                        rightVect = tempRight;
                    }
                }

                std::vector<Vector> newSegment;
                pointsIter = cutNext.begin();
                bool newSegged = false;

                for(pointsIter; pointsIter != cutNext.end(); pointsIter++)
                {
                    if(((*pointsIter).y == leftVect.y) &&
                       ((*pointsIter).x == leftVect.x) &&
                       (pointsIter != points.begin())  &&
                       (pointsIter != points.end()))
                    {       
                        newSegged = true;
                    }
                    if(newSegged && ((pointsIter != cutNext.begin()) && ((pointsIter != cutNext.end()))))
                    {
                            newSegment.push_back(*pointsIter);
                    }
                }
                cutPrev = cutNext;
                cutNext = newSegment;
                   
                pointsIter = cutPrev.begin();
                bool cut = false;
                while(!cut)
                {
                    if(((*pointsIter).x == (*(newSegment.begin())).x) &&
                       ((*pointsIter).y == (*(newSegment.begin())).y))
                    {
                        cutNext.erase(pointsIter, newSegment.end());
                        cut = true;
                    }
                    pointsIter++;
                }
                if(!newSegged)
                    allPoints.push_back(cutNext);
                allPoints.push_back(cutPrev);
            }
        }
    }
    return allPoints;                                
}













}//namespace Trafsim
