              Release Notes for "trafsim" module

Release R1-02f (Mon Aug 27 18:22:02 2007):
	Added scenario for new santa anita rndf

Release R1-02e (Sun Aug 26 13:35:33 2007):
	Now whenever a trafsim car is stopped, it shows the time stopped next tothe car in the trafsim gui. For more details about how to add/delete/stop cars, see http://gc.caltech.edu/wiki07/index.php/Trafsim#Using_trafsim

Release R1-02d (Fri Aug 24 18:32:56 2007):
	Added scenarios for advanced traffic situations

Release R1-02c (Fri Aug 24 16:53:08 2007):
	Increased min distance for sending objects to sensor-sim

Release R1-02b (Wed Aug 22 10:01:33 2007):
	More improvements to car intersection behavior

Release R1-02a (Tue Aug 21 16:43:54 2007):
	Some improvements on car behavior at intersections. Now cars will wait for alice when it's her turn. It still isn't perfect, so if a car sits there forever and alice is waiting for it, just delete it. Of course, this kind of behavior is great if you want to test if alice will go after 10 sec, or if she'll go when it's not her turn.

	Also (finally) fixed the weird yaw "spin" behavior for alice that happens sometimes when she stops.

Release R1-02 (Tue Aug 21  9:39:28 2007):
	Can now delete objects easily. After adding an obstacle, car, whatever, hold the 'delete' key and then click on it to delete it. This deletes it from trafsim and also sends a clear message on to mapper.

Release R1-01z (Tue Aug 14 17:27:22 2007):
	Added simple zone scenario

Release R1-01y (Mon Aug  6 14:28:39 2007):
	Added the timeStopped variable to cars that trafsim exports

Release R1-01x (Fri Aug  3 18:19:59 2007):
	Modified the way trafsim exports objects to be compatible with sensor-sim

Release R1-01w (Fri Aug  3 16:29:13 2007):
	Fixed bug in santaanita mock field demo follow scenario where trafsim would hang. Also removed car logging for now, because it may have been slowing things down.

Release R1-01v (Wed Aug  1 17:21:42 2007):
	Added scenarios for santa anita mock field demo1 system tests.

Release R1-01u (Tue Jul 31 15:59:37 2007):
	Added scenario for stluke area test

Release R1-01t (Mon Jul 30 17:54:43 2007):
	SplineCars can now be added to scenarios by running app.buildSplineCar() during scenario making session. (Note: holding 'k' and clicking will not save the splinecar in a scenario)
	
	Changed app.CommitScenario(...) to app.commitScenarioSysTests(...) to prevent ambiguity

	NOTE for system-tests: You can now load zones from the command line; simly use "./bin/Drun python -i GenericApp.py --loadZones RNDFNAME"

Release R1-01s (Fri Jul 27 16:45:11 2007):
	Fixed bug where app.clearCars() did not remove SplineCars

Release R1-01r (Thu Jul 26 12:31:24 2007):
	Removed doxygen file from version control

Release R1-01q (Thu Jul 26 10:24:10 2007):
	Created a new kind of car: SplineCar. To create a SplineCar, first create a spline. Then either run app.buildSplineCar() or hold 'k' and click anywhere on the screen. this will create a car that follows the path of the selected spline.
	To pause the car, hold '0' and click on the car
	To reverse the car, hold '9' and click on the car
	To speed up teh car, hold ']' and click on the car
	To slow down the car, hold '[' and click on the car

	SplineCars are ideal for unstructred parts of the RNDF, like zones and parking lots. Of course they can also be used on normal roads, for example to simulate a car on the wrong side of the road, heading straight towards us.


Release R1-01q (Thu Jul 26 10:05:44 2007):
	Created a new kind of car: SplineCar. To create a SplineCar, first create a spline. Then either run app.buildSplineCar() or hold 'k' and click anywhere on the screen. this will create a car that follows the path of the selected spline.
	To pause the car, hold '0' and click on the car
	To reverse the car, hold '9' and click on the car
	To speed up teh car, hold ']' and click on the car
	To slow down the car, hold '[' and click on the car

	SplineCars are ideal for unstructred parts of the RNDF, like zones and parking lots. Of course they can also be used on normal roads, for example to simulate a car on the wrong side of the road, heading straight towards us.

Release R1-01p (Wed Jul 25 20:58:45 2007):
		Greatly simplified Car intersection procedures. (No pointers or weird structs anymore... just integers). 

	Added ability to load zones and parking lots. To load a zone, first load the .rndf or .ts file, then run app.buildZones([rndf filename]). For example:

	app.load("etc/routes-darpa/darpa_sample.ts")
	app.buildZones("etc/routes-darpa/darpa_sample.rndf")

	In this way hopefully we shouldn't have to rebuild .ts files in order to load the zones additionally. (Note: I just realized I forgot to serialize Line class so saving zones won't work yet... gotta power through this YaM release though)

Release R1-01o-jengo (Wed Jul 25 15:33:51 2007):
	Greatly simplified Car intersection procedures. (No pointers or weird structs anymore... just integers). 

	Added ability to load zones and parking lots. To load a zone, first load the .rndf or .ts file, then run app.buildZones([rndf filename]). For example:

	app.load("etc/routes-darpa/darpa_sample.ts")
	app.buildZones("etc/routes-darpa/darpa_sample.rndf")

	In this way hopefully we shouldn't have to rebuild .ts files in order to load the zones additionally. (Note: I just realized I forgot to serialize Line class so saving zones won't work yet... gotta power through this YaM release though)

Release R1-01o (Wed Jul 18 20:35:27 2007):
	Added initial functionality for logging paths of trafsim cars. Every time a car is added, a new file is created in the system-tests/logs/ folder, with the format trafsim-YYYY-MM-DD-HH-MM-CarXXXX.log where XXXX may be replaced by the car number. When trafsim closes, it generates an additional file with the format trafsim-YYYY-MM-DD-HH-MM.log which contains the number of cars (ie log files) that have been generated.

	I suspect this method of logging will change... please let me know how I can modify it to make it more easy to use. It's more for system-tests than for trafsim.

Release R1-01n (Tue Jul 17 14:34:53 2007):
	Added some functionality for stopping and starting cars in trafsim.

	To pause/unpause all cars, hold 'p' and click anywhere on the screen
	To pause/unpause a specific car, hold 'x' and click on the car. This can be done for as many cars as you like, and can also be used after using 'p' to pause.
	To speed up all the cars, hold '=' and click anywhere on the screen.
	To slow down all the cars, hold '-' and click anywhere on the screen.

	(ignore the comment below about additional 'alice' cars, it's not working yet)

Release R1-01m (Mon Jun 11  2:22:06 2007):
	* Car paths (which direction cars take at intersections) can now 
be set in app.addCar(vel, path) or setPath(carPath(path)) where path is 
an array of enumerated types (CROSS_RIGHT, CROSS_STRAIGHT, CROSS_LEFT)
	* Objects and events can be selected by clicking next to the 
object while holding the 's' key.  Selection inverts the color of the 
object and they can accessed from app.editor.selected or app.sel[name] 
(dictionary), where name is a string specified in app.addSelect(name).
	* Selection can be used in scenario making.  
app.addToScenario(line) will allow for lines of python code to be added 
to the scenario being made.
	* Events are now drawn in trafsim as a green circle.  Won't 
appear in MapViewer.
	* Look at the wiki "Running and Making Scenarios" for help in 
scenario making

Release R1-01l (Thu May 31  6:36:39 2007):
	* trafsim now follows Alice's location.  Through Events, trafsim 
can start certain scenario functions if Alice gets within a 
certain distance of a location, a "checkpoint".  Hopefully, this will 
make simulations with dynamic cars easier, since we can just start the 
cars once Alice passes a certain point.
	* Added 2 extended Santa Anita simulation, each with their own 
internal events.
	* Upgraded a Santa Anita intersection scenario so that the cars 
appear once Alice passes a trafsim "checkpoint"
	* Also, I added a space in ScenarioDictionary so that scenario 
names don't get eaten during creation.

Release R1-01k (Sun May 27  2:17:11 2007):
	* Updated Makefile.yam to allow compilation on OS X.  There are
	still some problems with getting the display to initialize properly
	that need to be chased down; looking online for hints, but wanted to
	get the Makefile changes out to avoid a merge in case it takes a
	while to chase these down.

Release R1-01j (Sat May 26  3:37:42 2007):
	* Changed the 4 Santa Anita special case scenarios to have obs 
files
	* Added 4 St. Luke dynamic obstacle scenarios.
	* Stopped cars from moving during making scenarios
	* --sleep - trafsim sleeps for the specified number of seconds
	* --wait - trafsim waits for Alice to move 0.5 m in any 
direction before starting the simulator

Release R1-01i (Thu May 24 16:30:00 2007):
	Trafsim to mapper interface appears to be fixed. The cars 
show up correctly in mapviewer now. Need to verify some data 
members (obj.velocity.x, obj.velocity.y), but visually, the cars move 
correctly in mapviewer. The problem appears to have been with the useage 
of obj.setGeometry(pos, length, width, orientation). By getting the 
ptarr boundary instead, and just setting the geometry with that, it 
works.

There is still the bug with the internal clearing of roads when using buildRoads, but this shouldn't effect the 
usage. 
	Added variable that will give more control over the last created 
car. After adding a car, you can use 'app.editor.lastCar', with whatever 
methods car has. This can be used to change the velocity (i.e. 
app.editor.lastCar.maxVel=0 to stop the car), or set the path of the car 
(see wiki). app.clearCars() removes all cars from screen. 


Release R1-01h (Thu May 24  5:53:20 2007):
	This release of trafsim has cars. Unfortunately, the cars are 
not exported to mapper correctly, so until that interface is sorted out, 
they're not much use.

	There are several test scripts that were made to be used, but 
the scripts for santa anita + cars crash for unknown reason.

	To add cars manually, you can type app.addCar(speed), if you 
want speed to be something other than 1. Then, select the trafsim 
window, hold c, and click on the road where you want the car added. 
Currently, the cars will follow whatever path they want, but are 
generally well behaived. Set paths are possible, but the UI for these 
haven't been established yet.
	
	Several major bugs were fixed, including road boundary issues 
that were previously inhibiting car functionality.

	New major bug discovered, and currently unresolved. Internally, 
data from previously built roads isn't being cleared correctly. This 
shouldn't effect the testing scenarios, it just makes the .ts files much 
more difficult to create.



Release R1-01g (Tue May 22 21:19:44 2007):
	Updated send map element functionality to simulate sensor range. 
	Obstacles will only be placed in the map if they are within 50
	meters of Alice.  On the visualization channel, obstacles not in
	view will be visualized in white, while obstacles in view will be
	visualized in yellow.  Lane lines also are truncated to a distance
	of 10m away from alice, and are sent on both the visualization
	channel (-2) and mappers channel (0). The element type of the 
	vehicles is changed to ELEMENT_VEHICLE instead of ELEMENT_OBSTACLE



Release R1-01f (Tue May 22  3:35:42 2007):
	Fixed bug where Alice was never actually built in trafsim or mapper.  Also 
further automated the scenario creation process by making trafsim also add 
automatically to the function dictionary (Warning: might be buggy) and by making it 
create a standard system-test script in the system-tests directory.  If there are bugs, 
please go back to R1-01d, not the immediately previous one.

Release R1-01e (Mon May 21 17:45:07 2007):
	Made making scenarios easier by automatically adding them to a 
separate Scenarios.py folder.  Would still need to add the function to 
the function dictionary manually, though.

Release R1-01d (Sun May 20 18:11:10 2007):
	Fixed bug where alice was placed in wrong location. Added script (fastCompile.sh) that cuts 5+ min off compile time (thanks daniele!!)

Release R1-01c (Fri May 18  2:14:04 2007):
	Fixed bug that caused mapper to not think the obstacles had 
height -1, and wasn't set as type obstacle. Also modified trafsim so 
that it is easier to build testing scenarios.

Release R1-01b (Thu May 17  7:35:39 2007):
	Fixed several bugs that caused loaded trafsim (.ts) files to 
misalign/not show up on mapper. This version is set to work with testing 
static obstacles.

Release R1-01a (Wed May 16 14:48:04 2007):
	Added 8 scenario functions for St Luke and Santa Anita for this 
week's testing.

Release R1-01 (Mon May 14 23:09:18 2007):
	More commnad line arguments added to trafsim to handle setting 
the subgroup that trafsim talks to mapper over. This releases 
incorporates internal changes to Environment.cc that Sam made to talk to 
mapper.

Release R1-00z (Thu May 10  5:34:22 2007):
	Trafsim is now capable of easily building (not automatically!) 
intersections. In addition, there are command line arguments to be able 
to start trafsim with a pre-saved file, an RNDF, and possibly a function 
to run at the start. Improved serialization for saving the .ts files.
	There appear to be bugs in the coordinate frames/interfaces 
with asim/mapper.

Release R1-00y (Mon May  7 13:36:56 2007):
	Modified so that trafsim sends out everything in local coords. This matches up better with mapper. This is now a bug in trafsim where alice is visualized in the wrong location... this is not crucial however because you can just look at mapviewer for a correct visualization. 

Release R1-00x (Thu Apr 26 20:37:19 2007):
	Accidentally left a bug in ExternalCar.hh's constructor, fixed 
in this version.

Release R1-00w (Thu Apr 26 19:55:55 2007):
	Improved loading and saving functionality. Can now cleanly save 
and load any trafsim environment; RNDF does not have to be directly 
loaded if a trafsim file is loaded because the translations are stored 
in new save. Added app.exit() helper function to trafsim for a clean 
exit from python. Added functionality to app.clear() such that it 
completely clears all data from memory, including RNDF data. This can be 
used so trafsim does not have to be reset to load/reload new 
rndf's/other trafsim files. Added more information to app.help().

Release R1-00v (Wed Apr 25 23:54:20 2007):
	Added unit tests for navigation - static obstacles

Release R1-00u (Wed Apr 25 22:36:20 2007):
	Fix for frames discrepancy when exporting road lines

Release R1-00t (Tue Apr 24 21:07:34 2007):
	Added a generic line object for creating stop lines, parking spaces, etc.

Release R1-00s (Tue Apr 24 13:06:16 2007):
	Fixed a few bugs with exporting obstacles

Release R1-00r (Thu Apr 19 18:15:51 2007):
	Added methods to the segment class in the RNDF interface such 
that, based on several approximations, the roads will be built such that 
the lanes correctly line up with the lanes in the RNDF file. Need to 
verify the lane width.

Release R1-00q (Tue Apr 17 14:37:21 2007):
	Modified the internal structure of the RNDF interface in order 
to have a structure capable of handling intersections and multiple lanes 
more cleanly. User interface/implementation is exactly the same.

Release R1-00p (Sun Apr 15 20:05:11 2007):
	Can now add obstacles (circle or block) of arbitrary size to the environment

Release R1-00o (Tue Apr 10 21:09:57 2007):
	Cleaned up the lane boundaries so they don't look so messy on export. trafsim now calculates road markings only once, not every time they're exported. A few more small additions, see trafsim help for info.

Release R1-00n (Mon Apr  9  0:21:40 2007):
	Fixed a bug regarding exporting lane lines. trafsim now exports intersection information. added more help messages and command to toggle debugging messages.

Release R1-00m (Sun Apr  8 16:44:15 2007):
	Added some new functionality which makes it easy to add arbitrarily sized static obstacles to the environment. Also added capability to move Alice to a new, arbitrary location so we can mimic what would happen at the Site Visit if DARPA paused Alice and moved her.

Release R1-00l (Fri Apr  6  2:24:28 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>


Release R1-00k (Sun Apr  1 20:27:24 2007):
	Mostly cleanup, no new functionality yet. Made the classes inherited from Object more consistent. Also added a script to generate doxygen docs until a YaM solution comes up.

Release R1-00j (Fri Mar  9 15:40:13 2007):
	Updated trafsim to work with latest version of mapper. Currently exports northing as x and easting as y, but the internal trafsim 
representation of northing/easting in trafsim is still switched.

Release R1-00i (Sat Mar  3 16:34:01 2007):
	More functionality when loading RNDFs. This version should work 
for testing. Release for Implementation Test #2.
Release R1-00h (Sun Feb 25 17:22:47 2007):
	Fixed trafsim header files to work with new skynet/interfaces/etc rearrangement. Added ability to export the road lines for the road Alice is currently on. Tested this with the new mapper (skynettalker/testRecvMapElement).

Release R1-00g (Sun Feb 18 22:28:55 2007):
	Added RNDF wrapper class with (minimal) ability to parse RNDF 
files into trafsim. Minor useability changes for updated functionality.

Release R1-00f (Sun Feb 18 21:38:35 2007):
	Added MapElement interface to trafsim. trafsim can now export car objects to the mapper via skynet. Also added ability to create a 'stopped car' to trafsim.

Release R1-00e (Fri Feb  9 18:31:47 2007):
	Added ability to receive asim messages to ExternalCar objects. 
	
	To test out this capability, run the following (with XXXXXX replaced
	with some number):
	export SKYNET_KEY=XXXXXX
	bin/Drun asim
	
	New terminal:
	export SKYNET_KEY=XXXXXX
	bin/Drun python -i trafsim/GenericApp.py
	
	In the python prompt:
	lg = LaneGrid()
	lg.partition(app.environment,5.0,Vector(-100,-100), Vector(100, 100))  
	app.environment.addObject(lg)
	ec = ExternalCar(app.environment.skynetKey)
	ec.initialize(app.environment)
	app.editor = CarDriver(ec)
	ec.simulate()
	
	Note that ec.simulate() does not yet run automatically.
	Also make sure to unpause asim.	

Release R1-00d (Thu Feb  8 20:47:28 2007):
	Fixed a bunch of python makefile stuff (Abhi).

Release R1-00c (Sat Feb  3 23:23:59 2007):
	Moved files out of redundant trafsim/src directory into trafsim/ and
	fixed Makefile.yam to reflect these changes.

Release R1-00b (Thu Feb  1 20:18:02 2007):
	Renamed files from .cpp and .h to .cc and .hh, as required by our coding
	standards. Written Makefile.yam, it creates the python bindings library
	in <target>/_libtrafsim.so (e.g. i486-gentoo-linux/_libtrafsim.so)
	and creates a link in $YAM_ROOT/lib/PYTHON/_libtrafsim.so (not sure if
	this is the best way, but it was the simples one).

Release R1-00 (Sun Jan 21 15:45:32 2007):
	Created.




























































