RNDF_name	SPARSE_ROAD_RNDF	
num_segments	1	
num_zones	0	
format_version	1.0	
creation_date	11/27/2006


segment	1	
num_lanes	2	
segment_name	Seg1
lane	1.1	
num_waypoints	2	
lane_width	12	
left_boundary	double_Yellow	
right_boundary	solid_white
checkpoint	1.1.1    1
checkpoint	1.1.2    2
1.1.1   34.167477       -118.091000
1.1.2   34.167477       -118.094000
end_lane		
lane	1.2	
num_waypoints	2	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white
checkpoint	1.2.1	3
checkpoint	1.2.2   4
1.2.1   34.167381       -118.094000
1.2.2   34.167381       -118.091000
end_lane		
end_segment		

end_file		


