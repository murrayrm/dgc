from libtrafsim_Py import *

import time
import math

class Event:
    def __init__(self, functionName, x, y, distance = 1.0, delay = 0.0):
        self.functionName = functionName
        self.x = x
        self.y = y
        self.distance = distance
        self.delay = delay
        
        self.waiting = False
        self.active = False

    def simulate(self, x, y):
        if self.waiting:
	    if time.clock() - self.waitStart > self.delay:
	        self.active = True
	        return True
        else:
	    distance = math.sqrt(math.pow(self.x-x,2) + math.pow(self.y-y,2))
	    if distance < self.distance:
	        self.waiting = True
	        if self.delay == 0.0:
		    self.active = True
		    return True
	        self.waitStart = time.clock()
        return False
