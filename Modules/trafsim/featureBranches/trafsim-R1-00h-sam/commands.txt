Common trafsim stuff

ADD A CAR TO THE ROAD

spline = app.editor.selectedSpline
road = Road()
app.environment.addObject(road, 'MyRoad')
road.initialize(app.environment,spline,1,1,5.0)

app.timingFactor = 0.01

c = Car()
c.switchLane(road,0,0.0)
app.environment.addObject(c,'car1')
app.environment.removeObject("car1")

app.environment.exportEnv()



ADD A STOPPED CAR

spline = app.editor.selectedSpline
road = Road()
app.environment.addObject(road, 'MyRoad')
road.initialize(app.environment,spline,1,1,5.0)

stopped_car = Car(1)

app.environment.addObject(stopped_car,'car')
stopped_car.switchLane(road,0,0.0)
app.environment.removeObject("car")


ADD AN EXTERNAL CAR (ALICE)

lg = LaneGrid()
lg.partition(app.environment,5.0,Vector(-100,-100), Vector(100, 100))  
app.environment.addObject(lg)
ec = ExternalCar(app.environment.skynetKey)
ec.initialize(app.environment)
app.environment.addObject(ec,'Alice')
app.environment.exportEnv()




ADD AN INTERSECTION

splines = [Spline.downcast(s) for s in app.environment.getObjectsByClassID('Spline').values()]
roads = [Road() for s in splines]
for road in roads: app.environment.addObject(road)
for road, spline in zip(roads, splines):
    road.initialize(app.environment, spline, 2, 2, 5.0)

yi = YIntersection() 
app.environment.addObject(yi, 'MyIntersection')
for road in roads: yi.hookStart(road)
yi.computeGeometry(app.environment)

xi = XIntersection() 
app.environment.addObject(xi, 'MyIntersection')
for road in roads: xi.hookStart(road)
xi.computeGeometry(app.environment)





 make a spline
app.runCarTest()
app.runAliceTest()
app.environment.exportEnv()
