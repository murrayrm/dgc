#include "RNDFSimIF.hh"
#include <cmath>

namespace TrafSim
{

RNDFSimIF::RNDFSimIF() {
    rndf = new RNDF();
    
    xTranslate = 0;
    yTranslate = 0;
}

RNDFSimIF::~RNDFSimIF() {
    delete rndf;
}

//SEG FAULTS!!!
//void RNDFSimIF::resetRNDF() {
//    delete rndf;
//    rndf = new RNDF();
//
 //   xTranslate = 0;
 //   yTranslate = 0;
//}

bool RNDFSimIF::loadFile(char* filename) {
   return rndf->loadFile(filename);
}

double RNDFSimIF::getxTranslate() {
    return xTranslate;
}

double RNDFSimIF::getyTranslate() {
    return yTranslate;
}

void RNDFSimIF::mergeLanesinSegments() {
    //The member function may be a bad idea after all...
}

int RNDFSimIF::getNumSegs() {
    return rndf->getNumOfSegments();
}

void RNDFSimIF::appendStartEnd(std::vector<Vector> *points, int flags)
{
    if (flags && APPEND_START)
    {
    	double X2 = points->at(0).x;
        double X1 = points->at(1).x;
        double Y2 = points->at(0).y;
        double Y1 = points->at(1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->begin();
	points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
    
    if (flags && APPEND_END)
    {
        int last = points->size() - 1;

        double X2 = points->at(last).x;
        double X1 = points->at(last - 1).x;
        double Y2 = points->at(last).y;
        double Y1 = points->at(last - 1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->end();
        points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
}

std::vector<Vector> RNDFSimIF::getPointsForSpline(int seg, int lane) {
    std::vector<Vector> points;
    std::vector<Vector>::iterator pointsIter;

    Segment* seg1 = rndf->getSegment(seg);
    Lane* lane1 = seg1->getLane(lane);
    std::vector<Waypoint*> waypoints = lane1->getAllWaypoints();
 
    std::vector<Waypoint*>::iterator iter;
    for(iter = waypoints.begin(); iter != waypoints.end(); iter++) {
	points.push_back(Vector((*iter)->getEasting(), (*iter)->getNorthing()));
    }
       
    appendStartEnd(&points);

    return points;
}


void RNDFSimIF::centerAllWaypoints() {
    vector<Segment*> segs = rndf->getAllSegments();
    vector<Segment*>::iterator segIter;

    double largestNorthing = 0, smallestNorthing = 0;
    double largestEasting = 0, smallestEasting = 0;

    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        vector<Lane*> lanes = (*segIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;

	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
            vector<Waypoint*>::iterator iter;
            
	    for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
	        double tempHeadingN = (*iter)->getNorthing();
	        double tempHeadingE = (*iter)->getEasting();
	
	        if(tempHeadingN > largestNorthing)
	            largestNorthing = tempHeadingN;
	
	        else   //if(tempHeadingN <= smallestNorthing)
	            smallestNorthing = tempHeadingN;

	        if(tempHeadingE > largestEasting)
	            largestEasting = tempHeadingE;

	        else   //if (tempHeadingE <= smallestEasting)
	            smallestEasting = tempHeadingE;
            }
	}
    }

    yTranslate = 0.5*(largestNorthing + smallestNorthing);
    xTranslate = 0.5*(largestEasting + smallestEasting);

    
    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        vector<Lane*> lanes = (*segIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;
        
	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
   
            vector<Waypoint*>::iterator iter;
            for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
                (*iter)->setNorthingEasting(((*iter)->getNorthing()-yTranslate),
                                            ((*iter)->getEasting()-xTranslate));
            }
        }
    }
}
	
        


}//namespace Trafsim
