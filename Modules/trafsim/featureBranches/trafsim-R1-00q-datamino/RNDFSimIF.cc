#include "RNDFSimIF.hh"
#include <cmath>

namespace TrafSim
{

//Default constructor.
RNDFSimIF::RNDFSimIF() {
    rndf = new RNDF();
    
    //Initialize translations to zero.
    xTranslate = 0;
    yTranslate = 0;

    //Tell it that we don't have a file loaded initially.
    loaded=false;

}

//Default destructor.
RNDFSimIF::~RNDFSimIF() {
    delete rndf;
}

//Loads RNDF file.
bool RNDFSimIF::loadFile(char* filename) {
    loaded = true;
    return rndf->loadFile(filename);
}

//Returns number of segments.
int RNDFSimIF::getNumSegs() {
    if(warn())
        return 0;
		
    return rndf->getNumOfSegments();
}

//Returns the number of lanes in a segment.
int RNDFSimIF::getNumLanes(int segment) {
    if(warn())
        return 0;
		
    vector<Segment*> segs = rndf->getAllSegments();
    vector<Segment*>::iterator segIter;
    int counter = 1;

    for(segIter = segs.begin(); segIter != segs.end(); segIter++) {
        if (counter == segment)
            return (*segIter)->getNumOfLanes();
        else
	    counter++;
    }

    //If it gets here, something screwed up.
    return 0;
}

//Returns the number of lanes in each direction, for all of the lanes.
std::vector<Vector> RNDFSimIF::getLaneInfos() {
    if(warn())
        return vector<Vector>();
		
    vector< SegInfo >::iterator segIter;
    vector< Vector > allLaneInfos;
	
    for(segIter = allSegs.begin(); segIter != allSegs.end(); segIter++) {
        Vector temp = segIter->laneInfo;
        allLaneInfos.push_back(temp);
    }
    return allLaneInfos;
}

//Returns the x translation.
double RNDFSimIF::getxTranslate() {
    return xTranslate;
}

//Returns the y translation.
double RNDFSimIF::getyTranslate() {
    return yTranslate;
}

//Return the center line of seg.
std::vector< Vector > RNDFSimIF::getCenterLineOfSeg(int seg) {
    if(warn())
        return vector<Vector>();
		
    appendStartEnd(&((allSegs.at(seg)).centerLine), APPEND_START|APPEND_END);
    return (allSegs.at(seg)).centerLine;
}



//Initializes the vector of SegInfos, and sets the data for the SegInfos.
void RNDFSimIF::buildSegInfos() {
    if(warn())
        return;
		
    vector< Segment* > segs = rndf->getAllSegments();
    vector< Segment* >::iterator segIter = segs.begin();

    for(; segIter != segs.end(); segIter++) {
        SegInfo newSeg;
		
        newSeg.seg = (*segIter);
        newSeg.rawLanes = (*segIter)->getAllLanes();
        newSeg.setLaneInfo();
        newSeg.setCenterLine();
        
	allSegs.push_back(newSeg);	
    }
}

//Center all the waypoints in the rndf. Store the translation factors
//in order to be able to translate back into the original rndf frame.
void RNDFSimIF::centerAllWaypoints() {
    if(warn())
        return;
	
    std::vector<Segment*>::iterator segmentIter;
    std::vector<Segment*> segs = rndf->getAllSegments();
    
    double largestNorthing = 0, smallestNorthing = 0;
    double largestEasting = 0, smallestEasting = 0;
    bool hack = true;
    
    for(segmentIter = segs.begin(); segmentIter != segs.end(); segmentIter++) {
        vector<Lane*> lanes = (*segmentIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;
	
	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
            vector<Waypoint*>::iterator iter;
	    
	    for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
	        double tempHeadingN = (*iter)->getNorthing();
	        double tempHeadingE = (*iter)->getEasting();
		
		if(hack) {
		     largestNorthing = tempHeadingN;
		     largestEasting = tempHeadingE;
		     smallestNorthing = tempHeadingN;
		     smallestEasting = tempHeadingE;
                     hack = false;
		}
	        
		if(tempHeadingN >= largestNorthing)
	            largestNorthing = tempHeadingN;
	
	        else if(tempHeadingN < smallestNorthing)
	            smallestNorthing = tempHeadingN;

	        if(tempHeadingE >= largestEasting)
	            largestEasting = tempHeadingE;

	        else if (tempHeadingE < smallestEasting)
	            smallestEasting = tempHeadingE;
            }
		}
    }
	
    yTranslate = 0.5*(largestNorthing + smallestNorthing);
    xTranslate = 0.5*(largestEasting + smallestEasting);

    
    for(segmentIter = segs.begin(); segmentIter != segs.end(); segmentIter++) {
        vector<Lane*> lanes = (*segmentIter)->getAllLanes();
	vector<Lane*>::iterator laneIter;
	
	for(laneIter = lanes.begin(); laneIter != lanes.end(); laneIter++) {
	    vector<Waypoint*> wpoints = (*laneIter)->getAllWaypoints();
            vector<Waypoint*>::iterator iter;
            for(iter = wpoints.begin(); iter != wpoints.end(); iter++) {
                (*iter)->setNorthingEasting(((*iter)->getNorthing()-yTranslate),
		((*iter)->getEasting()-xTranslate));
            }
        }
    }
}


//Append the start of/end of/or both with another point at some distance
//away. This is to ensure trafsim doesn't explode when you try to build
//a spline out of 2-3 points.
void RNDFSimIF::appendStartEnd(std::vector<Vector> *points, int flags)
{
    if(warn())
        return;
		
    if (flags && APPEND_START)
    {
    	double X2 = points->at(0).x;
        double X1 = points->at(1).x;
        double Y2 = points->at(0).y;
        double Y1 = points->at(1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->begin();
	points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
    
    if (flags && APPEND_END)
    {
        int last = points->size() - 1;

        double X2 = points->at(last).x;
        double X1 = points->at(last - 1).x;
        double Y2 = points->at(last).y;
        double Y1 = points->at(last - 1).y;

        double dX = X2 - X1;
        double dY = Y2 - Y1;

        double X3 = (dX * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + X2;
        double Y3 = (dY * APPEND_DIST) / (sqrt(dX * dX + dY * dY)) + Y2;

        std::vector<Vector>::iterator pointsIter = points->end();
        points->insert(pointsIter, *(new Vector(X3, Y3)));
    }
}

//Safety function to ensure that an RNDF file has been loaded.
bool RNDFSimIF::warn() {
    if(loaded)
        return false;
    else {
        cout<<"Please load an RNDF file before proceeding.\n"<<endl;
	return true;
    }
}
//////////////////////////////////////////////////////////////////////
//////////////////////////SegInfo Member Functions////////////////////
//////////////////////////////////////////////////////////////////////


//This will determine how many 'forward'and 'reverse' lanes there are.
//To do this, it will take the first lane as forward. Then it will
//compare locations (make sure other lane's initial point is within
//some radius (define to be 30?) of the first lane's start. It will
//also check to see if the dot product between the vectors of the first
//two points in each lane is approximately zero.
void SegInfo::setLaneInfo() {
	//Initialize the radius. This value needs to be confirmed.
	//int radius = seg->getNumOfLanes() * 20;
	//CHANGED THE WAY IT DETERMINES LANES, SHOULDN'T USE RADIUS NOW.
	
	//Initialize 1 forward lane.
    laneInfo = Vector(1,0);
        
    std::vector<Vector> lane1 = getPointsFromLane(1);
        
    Vector lane1Init = lane1.at(0);
    Vector lane1Second = lane1.at(1);
    Vector lane1Dir = lane1Second - lane1Init;
        
    //Iterate through the lanes. Start at two because the lanes
	//because the lanes are 1 indexed, and 1 is 'lane1'.
    for(int laneIter = 2; laneIter <= seg->getNumOfLanes(); laneIter++) {
        std::vector<Vector> curLane = getPointsFromLane(laneIter);
        
	Vector curLaneInit = curLane.at(0);
	Vector curLaneSecond = curLane.at(1);
	Vector curLaneDir = curLaneSecond - curLaneInit;
	   
        if(lane1Init.dist2(curLaneInit,lane1Init) <
            lane1Init.dist2(curLane.back(),lane1Init))
            (laneInfo.x)++;
        else
            (laneInfo.y)++;
    }
}

//Return a vector<Vector> consisting of the points from int lane.
vector< Vector > SegInfo::getPointsFromLane(int lane) {
    std::vector<Vector> points;
    std::vector<Vector>::iterator pointsIter;

    std::vector<Waypoint*> waypoints = (seg->getLane(lane))->getAllWaypoints();
    std::vector<Waypoint*>::iterator iter;
    
    for(iter = waypoints.begin(); iter != waypoints.end(); iter++)
	points.push_back(Vector((*iter)->getEasting(), (*iter)->getNorthing()));
    return points;
}


//Return the points for the center of the segment for use in trafsim.
//Currently shifted because the points are silly.
void SegInfo::setCenterLine() {
    //If there are no 'reverse' lanes. Currently will not
    //account for necessary displacement. May not be necessary
    //to displace. Check on this.
    //CURRENT THEORY: OK if there is only one lane. If there are multiple
    //lanes, need to average points accordingly. Odd number of lanes, take
    //middle lane.
	
    if (laneInfo.y == 0) {
        centerLine = getPointsFromLane((int)(laneInfo.x));
    }
    //If there are lanes in the opposite direction, average the points
    //between the middle lanes.
    else {
        vector<Vector> lane1 = getPointsFromLane((int)(laneInfo.x));
        vector<Vector> lane2 = getPointsFromLane((int)(laneInfo.x) + 1);
	    
        //Iterate through the vectors and average points.
        vector<Vector>::iterator iter1, iter2;
        iter1 = lane1.begin();
        iter2 = lane2.end();
        iter2--;

        for(; iter1 != lane1.end(); iter1++) {
            (*iter1) += (*iter2);
            (*iter1) /= 2;
	                         
            if(iter2 != lane2.begin())
                iter2--;
        }
        centerLine = lane1;
    }
}

}//namespace Trafsim
