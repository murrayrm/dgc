#include "Obstacle.hh"
#include <cmath>
#include <GL/gl.h>
#include <iostream>

namespace TrafSim {

  Obstacle::Obstacle() 
  {
    position = point2(0,0);
    length = 2;
    width = 2;
    orientation = 0;
    circleObs = false;
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    boundary = getBoundary().front();
  }
  
  Obstacle::Obstacle(float x, float y, float l, float w, float o)
    : orientation(o), position(x,y), length(l), width(w)
  {
    circleObs = false;
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    boundary = getBoundary().front();
  }

  Obstacle::Obstacle(float x, float y, float radius) 
    : orientation(0), position(x,y), length(radius), width(radius)
  {
    circleObs = true;
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    boundary = getBoundary().front();
  }
    

  Obstacle::~Obstacle() 
  {

  }

  void Obstacle::setPosition(float x, float y)
  {
    position.x = x;
    position.y = y;
    boundary = getBoundary().front();
  }

  void Obstacle::setPosition(point2 pos)
  {
    position = pos;
    boundary = getBoundary().front();
  }

  void Obstacle::setOrientation(float newOrientation)
  {
    // if the desired orientation isn't between -pi and pi,
    // fix it
    while (newOrientation > M_PI)
      newOrientation -= 2*M_PI;
    while (newOrientation < -1*M_PI)
      newOrientation += 2*M_PI;
  
    orientation = newOrientation;
    boundary = getBoundary().front();
  }

  void Obstacle::setBlockObs()
  {
    circleObs = false;
    boundary = getBoundary().front();
  }

  void Obstacle::setCircleObs()
  {
    circleObs = true;
    boundary = getBoundary().front();
  }

  void Obstacle::draw() 
  {
    // draw a light blue object
    
    glPushMatrix();
    //    glTranslatef(position.x, position.y, 0.0f);
    //    glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
    glSetColor(color);

    glBegin(GL_POLYGON);

    for (vector<point2>::iterator it = boundary.begin();
	 it != boundary.end(); it++) {	
      glVertex3f(it->x, it->y, 0.0f);      
    }

    glEnd();
    glPopMatrix();
  }

  bool Obstacle::simulate(float ticks) 
  {
    return true;
  }

  vector<vector<point2> > Obstacle::getBoundary()
  {
    std::vector<point2> boundary;

    // circles
    if (circleObs) {
   
      double a = cos(M_PI/4);
      double r = length;
      double x = position.x;
      double y = position.y;
      boundary.push_back(point2(r+x,0+y));
      boundary.push_back(point2(r*a+x,r*a+y));
      boundary.push_back(point2(0+x,r+y));
      boundary.push_back(point2(-r*a+x,r*a+y));
      boundary.push_back(point2(-r+x,0+y));
      boundary.push_back(point2(-r*a+x,-r*a+y));
      boundary.push_back(point2(0+x,-r+y));
      boundary.push_back(point2(r*a+x,-r*a+y));
    }

    // blocks
    else {

      // return the four corners of the obstcle
      double ly = width;
      double lx = length;
      double x = position.x;
      double y = position.y;
      double t = orientation;

      
      // put in the points without rotation
      boundary.push_back(point2(.5*lx,.5*ly));
      boundary.push_back(point2(-.5*lx,.5*ly));
      boundary.push_back(point2(-.5*lx,-.5*ly));
      boundary.push_back(point2(.5*lx,-.5*ly));
      
      double oldY, oldX;
      // rotate points and translate
      for (std::vector<point2>::iterator iter = boundary.begin();
	   iter != boundary.end(); iter++) {
	oldX = iter->x;
	oldY = iter->y;
	iter->x = oldX*cos(t) - oldY*sin(t) + x;
	iter->y = oldX*sin(t) + oldY*cos(t) + y;

      }
    }

    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    return boundaryWrapper;
  }

  Obstacle* Obstacle::downcast(Object* source)
  {
    return dynamic_cast<Obstacle*>(source);
  }

  void Obstacle::serialize(std::ostream& os) const
  {
    // NOT IMPLEMENTED
  }

  void Obstacle::deserialize(std::istream& is)
  {
    // NOT IMPLEMENTED
  }

}
