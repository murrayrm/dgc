#ifndef CURVECONSTRAINT_H_
#define CURVECONSTRAINT_H_

#include "Spline.hh"

namespace TrafSim
{

/**
 * This function determines the optimal velocity for a car to have at a given point
 * on a given spline, given that it should not exceed the given linear and angular accelerations
 * at any point in the future. Aside from the acceleration constraints, a higher velocity is considered
 * more optimal; this function does not take speed limits into account.
 * \param spline The path the car in consideration is to follow
 * \param t The current position of the car along the path
 * \param vel The current velocity of the car
 * \param linearAccel The maximum linear acceleration of the car
 * \param angularAccel The maximum angular acceleration of the car
 * \return The highest velocity that does not force the car to exceed the given accelerations
 */
float curveConstraint(Spline const& spline, float t, float vel, 
					  float linearAccel, float angularAccel);
	
}

#endif /*CURVECONSTRAINT_H_*/
