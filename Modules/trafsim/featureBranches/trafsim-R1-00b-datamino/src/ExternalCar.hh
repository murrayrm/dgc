#ifndef EXTERNALCAR_H_
#define EXTERNALCAR_H_

#include "Car.hh"
#include "LaneGrid.hh"
#include <string>

namespace TrafSim
{
	
/**
 * This class allows you to plug in an externally-controlled vehicle (such as Alice) into the simulation,
 * and have the other cars be aware of it. Before you can use external cars, you must completely finish 
 * creating your environment's geometry, and then add a LaneGrid object to the environment. External cars
 * require a LaneGrid to efficiently determine their location in the world. Other than that, the car
 * behaves exactly like a normal simulated car, except that you can manually set its position, veloctity,
 * and acceleration vectors. Extermal cars have CarProperties, but the only relevant properties are size
 * and color.
 */
class ExternalCar: public CarInterface
{
public:
	/**
	 * No complex initialization is performed in the constructor.
	 */
	ExternalCar();
	virtual ~ExternalCar();
	/**
	 * Call this function before you do anything else.
	 * \param env The environment to set the external car up in
	 * \param gridName Optionally supply the name of the LaneGrid to use.
	 */
	void initialize(Environment& env, std::string gridName = "");
	/**
	 * Draws the car as a colored rectangle.
	 */
	virtual void draw();
	/**
	 * Applies basic kinematics to the car, and updates its lane-location.
	 * \param ticks The number of seconds elapsed since the last call to simulate()
	 * \return Always True
	 */
	virtual bool simulate(float ticks);
	/**
	 * \return The linear velocity, in lanes per second, that the car is assumed to be moving
	 * 		along its current lane. This value is computed during a call to simulate(), so fetching
	 *		it here is cheap.
	 */
	virtual float getLaneVelocity();
	/**
	 * \return The lane the car is currently closest to. This value is computed during a call to simulate(), 
	 * 		so fetching it here is cheap.
	 */
	virtual Lane* getCurrentLane();
	/**
	 * \return The closest point on the lane the car is currently closest to. If the car is more than 
	 * 		two LaneGrid partitions away from ANY lane, returns 2.0. This value is computed during a call 
	 * 		to simulate(), so fetching it here is cheap.
	 */
	float getLaneTCoord();
	
	void setWorldPosition(Vector position); 	///< setter method also updates currentLane and laneTCoord
	Vector getWorldPosition() const;			///< trivial getter method
	void setVelocity(Vector velVector);			///< trivial setter method
	Vector getVelocity() const;					///< trivial getter method
	void setAcceleration(Vector accelVector);	///< trivial setter method
	Vector getAcceleration() const;				///< trivial getter method
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to an ExternalCar
	/// \return An ExternalCar*, pointing to the same object as source.	
	static ExternalCar* downcast(Object* source);

protected:

	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);
	
private:

	// recomputes the values of lane and laneT
	void updateLanePos();
	
	Vector position;
	Vector velocity;
	Vector accel;
	
	Lane* lane;			// the lane the ExternalCar is currently CLOSEST to
	float laneT;		// the point on the lane the ExternalCar is closest to
	
	LaneGrid* laneGrid;	// the grid used for finding lane and laneT
};

}

#endif /*EXTERNALCAR_H_*/
