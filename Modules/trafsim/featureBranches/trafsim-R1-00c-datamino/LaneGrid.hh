#ifndef LANEGRID_H_
#define LANEGRID_H_

#include "Object.hh"
#include "Environment.hh"
#include "Vector.hh"
#include "Lane.hh"
#include <vector>

namespace TrafSim
{

/**
 * A LaneGrid partitions the lanes in an environment into a grid, to make finding the lane
 * closest to a given point in the world more efficient. One of these must be stored in
 * the environment before an ExternalCar may be used.
 */
class LaneGrid: public Object
{
public:
	/**
	 * No complex initialization is performed in the constructor.
	 */
	LaneGrid();
	virtual ~LaneGrid();
	
	/**
	 * Partition the environment into a grid of tileSize by tileSize (in meters) squares.
	 * \param env The environment to partition
	 * \param tileSize The width and height, in meters of a tile
	 * \param lowerLeft The lower-left corner of the rectangular section of the environment to partition.
	 * \param topRight The upper-right corner of the rectangular section of the environment to partition.
	 */
	void partition(Environment& env, float tileSize, 
				   Vector lowerLeft, Vector topRight);
	/**
	 * Once partition() has been called, use this function to find the lane closest to the given point.
	 * \param worldPos The point in question, in world coordinates
	 * \param laneOut To be filled with a pointer to the closest lane
	 * \return The t-coordinate of the closest point on laneOut to worldPos
	 */
	float getLanePosition(Vector worldPos, Lane** laneOut);
	
	/// \return "LaneGrid"
	virtual std::string getClassID() const { return "LaneGrid"; }

protected:
	
	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);

private:
	void mapLane(Lane* lane);

	std::vector<LaneSegment>* grid;
	unsigned int nTilesX, nTilesY;  
	Vector lowerLeft, topRight;
};

}

#endif /*LANEGRID_H_*/
