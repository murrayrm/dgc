#include "ExternalCar.hh"
#include <cmath>
#include <cassert>
#include <GL/gl.h>
//
namespace TrafSim
{

  ExternalCar::ExternalCar(int skynetKey, bool debug): CSkynetContainer(SNastate, skynetKey), 
						       orientation(0),xTrans(0),yTrans(0),lane(0), laneT(-1.0f),laneGrid(0)
  {

    // make the car yellow
    setColor(1.0f, 1.0f, 0.0f, 1.0f);
    this->debug = debug;

 
    approachingIntersection = false;
    crossingIntersection = false;
    firstRun = true;
  
    laneIndex = -1;

  }

  ExternalCar::~ExternalCar()
  {
  }

  void ExternalCar::initialize(Environment& env, std::string gridName)
  {
    if (gridName != "")
      laneGrid = (LaneGrid*)env.getObject(gridName);
    else
      {
	// just get an arbitrary lane grid from the environment (and fail if there isn't one)
	ObjectMap laneGrids = env.getObjectsByClassID("LaneGrid");
	assert(!laneGrids.empty());
		
	laneGrid = (LaneGrid*)laneGrids.begin()->second;
      }		
	
    updateLanePos();

    UpdateState();
    cout<<"Alice added at ("<< m_state.utmNorthing<<","<<m_state.utmEasting<<")"<<endl;
  }

  void ExternalCar::draw()
  {
    // draw a colored rectangle
    glPushMatrix();
    //    glTranslatef(0.0f,0.0f,0.0f);
    glTranslatef(position.x, position.y, 0.0f);
    if (fabs(velocity.x) >.5 || fabs(velocity.y) > .5) 
      glRotatef(-atan2(velocity.x, velocity.y) / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
    else
      glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
  
    //    glRotatef(0.0f,0.0f,0.0f,1.0f);
    glSetColor(props.color);
  
    glBegin(GL_POLYGON);
    //    vector<point2> boundary = getBoundary().front();
    //    for (vector<point2>::iterator it = boundary.begin();
    //	 it != boundary.end(); it++) {
    //      glVertex3f(it->x, it->y, 0.0f);
    //    }
    glVertex3f(-props.size.x * 0.5f, -props.size.y * 0.5f, 0.0f);
    glVertex3f(-props.size.x * 0.5f,  props.size.y * 0.5f, 0.0f);
    glVertex3f( props.size.x * 0.5f,  props.size.y * 0.5f, 0.0f);
    glVertex3f( props.size.x * 0.5f, -props.size.y * 0.5f,  0.0f);
    glEnd();
    glPopMatrix();       
  }

  bool ExternalCar::invertColor() {
    props.color.r = 1.0f - props.color.r;
    props.color.g = 1.0f - props.color.g;
    props.color.b = 1.0f - props.color.b;
    return true;
  }

  bool ExternalCar::simulate()
  {

	
    // grab state from asim
    // these variables are defined in skynettalker/StateClient.hh
    UpdateState();

    //Coordinates Flipped????
    velocity.y = m_state.utmNorthVel;
    velocity.x = m_state.utmEastVel;
    position.y = m_state.utmNorthing;
    position.x = m_state.utmEasting;
    orientation = m_state.localYaw;

    // shift the coordinates if necessary
    position.y -= xTrans;
    position.x -= yTrans;

    if (debug) {
      //    std::cout<<"pos = ( "<<position.x<<" , "<<position.y<<" )"<<endl;
    }
    updateLanePos();	
    return true;
  }

  bool ExternalCar::simulate(float ticks)
  {
    /*
    // apply kinematics and recalculate lane position
  
    velocity += accel * ticks;
    position += velocity * ticks;

    updateLanePos();	
    return true;
    */

    simulate();

    /* note: leaving this commented out until I can fix it. -Jessica

    if (!firstRun)
    checkIntersection();
    firstRun = false;

    */

    return true;
  }

  void ExternalCar::checkIntersection()
  {
    
    // Since ExternalCars are not controlled by trafsim,
    // this function does not worry about when it's Alice's turn
    // to go. Instead, it only queues her at the intersection
    // when she is within a certain distance of it, and takes
    // her out of the queue when she leaves the intersection.

    //   if (debug) cout<<"ExternalCar::checkIntersection"<<endl;

    if (approachingIntersection) {

      // find the distance to the stop line
      float remaining = lane->spline->evalLength(laneT, 0.3333333f);
      if (remaining < 0.0f) 
	remaining = 0.0f;

      if (remaining < props.size.y * 0.5f)
	{
	  // queue ourselves at the intersection
	  inter->enterQueue();

	  if (debug) cout<<"*** Alice queued at intersection."<<endl;
	 
	  approachingIntersection = false;
	  crossingIntersection = true;
	}
	
    }

    else if (crossingIntersection) {
      if (debug) cout<<"*** Alice crossing intersection... laneT: "<<laneT<<endl;

      // check to see if we've made it across
      if (laneT > 0.0f && laneT < .8)
	{
	  // remove ourselves from the queue
	  inter->exitIntersection();
	  crossingIntersection = false;
	}
    }

    // if not already at intersection, check to see if we're approaching one
    else {

      //      if (debug) cout<<"*** Alice not near intersection... laneT: "<<laneT<<endl;
      
      // NOTE: the currentRoad and laneIndex should have been 
      // updated by the Environment
      // wooo trafsim is awesome

      if (laneIndex != -1) {

	//	if (debug) cout<<"currentRoad is not empty"<<endl;

	// Get the intersection we're heading towards
	inter = currentRoad->isReverse(laneIndex)?
	  currentRoad->getEnd() :
	  currentRoad->getStart(); 

      
	// check for approaching stop signs
	if (laneT >= 0.85f && laneT <= 0.99f)
	  {
	    if (debug) cout<<"*** Alice approaching intersection"<<endl;
	    approachingIntersection = true;
	    
	  }
      }
      else {
	if (debug) cout<<"currentroad is empty!"<<endl;
      }
    }

    //    if (debug) cout<<"end ExternalCar::checkIntersection"<<endl;
    
  }


  float ExternalCar::getLaneVelocity()
  {
    // velocity "along" a particular lane is the actual velocity dot-producted with the 
    // tangent of the lane at the current location.
    Vector tan = lane->spline->evalNormal(laneT).perpendicular();
    return Vector::dot(velocity, tan);
  }

  void ExternalCar::updateLanePos()
  {
    if (lane)
      lane->removeObject(this);
    laneT = laneGrid->getLanePosition(position, &lane);
	
    if (laneT < 0.0f)
      {
	laneT = 2.0f;  // 2.0 if we're not on any lane at the moment
	lane = 0;
      }
    else
      lane->addObject(this, laneT);
  }

  void ExternalCar::setWorldPosition(Vector position)
  {
    this->position = position;
    updateLanePos();
  }

  Vector ExternalCar::getWorldPosition() const
  {
    return this->position;
  }

  void ExternalCar::setVelocity(Vector velVector)
  {
    this->velocity = velVector;
  }

  Vector ExternalCar::getVelocity() const
  {
    return this->velocity;
  }

  void ExternalCar::setAcceleration(Vector accelVector)
  {
    this->accel = accelVector;
  }

  Vector ExternalCar::getAcceleration() const
  {
    return this->accel;
  }

  float ExternalCar::getLaneTCoord()
  {
    if (laneT < 0.0f)
      updateLanePos();
		
    return this->laneT;
  }

  Lane* ExternalCar::getCurrentLane()
  {
    if (laneT < 0.0f)
      updateLanePos();
		
    return this->lane;
  }


  void ExternalCar::setCurrentRoad(map<string, Object*> &roads)
  {

    //  if (debug) cout<<"ExternalCar::setCurrentRoad"<<endl;
  
    Road* road;
    laneIndex = -1;
  
    for (map<string, Object*>::iterator it = roads.begin();
	 it != roads.end(); it++) {
    
    
      //    if (debug) {      
      //      if (!it->second) cout<<"roads is empty!!"<<endl;
      //      else cout<<"roads is not empty. size: "<<roads.size()<<endl;
      //    }
    
      // search through the roads and see if any of them contain our current lane
      road = static_cast<Road*>(it->second);
    
      //    cout<<"checking lane index..."<<endl;
      laneIndex = road->getLaneIndex(lane);
      //    cout<<"index is: "<<laneIndex<<endl;
      // we found the road hooray
      if (laneIndex != -1) { 
	//      if (debug) cout<<"found the current road!"<<endl;
	currentRoad = road;
	break;
      }
    }

    //  if (debug) cout<<"end ExternalCar::setCurrentRoad"<<endl;
  
  }

  float ExternalCar::getVelocity()
  {
    // take norm
    return sqrt(velocity.x*velocity.x + velocity.y*velocity.y);
  }

  std::vector<vector<point2> > ExternalCar::getBoundary()
  {
    // return the four corners of the car
 
    std::vector<point2> boundary;
    double ly = props.size.y;
    double lx = props.size.x;
    double x = position.x;
    double y = position.y;
    double t = getOrientation();

    // put in the points at origin
    boundary.push_back(point2(.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,-.5*ly));
    boundary.push_back(point2(.5*lx,-.5*ly));
  
    double oldY, oldX;
    // rotate points and translate
    for (std::vector<point2>::iterator iter = boundary.begin();
	 iter != boundary.end(); iter++) {
      oldX = iter->x;
      oldY = iter->y;
      iter->x = oldX*cos(t) - oldY*sin(t) +x;
      iter->y = oldX*sin(t) + oldY*cos(t) +y;
    }  
  
    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    return boundaryWrapper;

  }

  ExternalCar* ExternalCar::downcast(Object* source)
  {
    return dynamic_cast<ExternalCar*>(source);	
  }

  void ExternalCar::serialize(std::ostream& os) const
  {
    // NOT IMPLEMENTED
  }

  void ExternalCar::deserialize(std::istream& is)
  {	
    // NOT IMPLEMENTED
  }


}
