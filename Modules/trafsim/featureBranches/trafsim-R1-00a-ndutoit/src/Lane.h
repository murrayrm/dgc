#ifndef LANE_H_
#define LANE_H_

#include "Object.h"
#include "LinearSegmentDesc.h"
#include "Spline.h"

#include <map>
#include <vector>
#include <utility>
#include <set>

namespace TrafSim
{

class Lane;

typedef TrafSim::LinearSegmentDesc<Lane> LaneSegment;

/// Implementation-internal data structure
struct LaneInterferenceDesc
{
	LaneInterferenceDesc() {}
	LaneInterferenceDesc(LaneSegment const& _mySeg, 
						 LaneSegment const& _otherSeg,
						 float _lengthRatio): 
		mySeg(_mySeg), otherSeg(_otherSeg), lengthRatio(_lengthRatio) {}

	LaneSegment mySeg;
	LaneSegment otherSeg;
	float lengthRatio;
	
	friend std::ostream& operator<<(std::ostream& os, LaneInterferenceDesc const& desc);
	friend std::istream& operator>>(std::istream& is, LaneInterferenceDesc& desc);
};

/**
 * A Lane is an extension of a Spline, with two major additions. First, a Lane contains a sorted
 * list of arbitrary Objects, which can be efficiently added to and removed from all the time. It is easy
 * to get the next object before or after any given point on the Lane. Second, a Lane can contain
 * information about other lanes that occupy the same physical location. This feature can be used to efficiently find
 * objects that are physically "in front of" another object, even though their location is only represented
 * by points on independent lanes.
 */
class Lane: public Object
{
public:
	/**
	 * No complex initialization happens in the default constructor.
	 */
	Lane();
	/**
	 * This constructor initializes the lane with a given geometry.
	 * \param spline The spline to wrap the Lane around
	 */
	Lane(Spline const& spline);
	/**
	 * WARNING: The destructor deletes all the objects contained in the Lane!
	 */
	virtual ~Lane();
	/**
	 * Call this function if you used the default constructor.
	 * \param spline The spline to wrap the Lane around
	 */
	virtual void initialize(Spline const& spline);
	
	virtual std::string getClassID() const { return "Lane"; }
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a TestObject
	/// \return A TestObject*, pointing to the same object as source.	
	static Lane* downcast(Object* source);
	/**
	 * Calls simulate() on all the objects contained in the lane.
	 * \param ticks The number of seconds elapsed since the last call to simulate()
	 * \return Always true
	 */
	virtual bool simulate(float ticks);
	/**
	 * Calls draw() on all the objects contained in the lane.
	 */
	virtual void draw();
	/**
	 * Inserts an object inthe the lane at the given t-coordinate.
	 * \param object The object to insert
	 * \param t The t coordinate to place it at
	 * \return True on success, False on failure
	 */
	virtual bool addObject(Object* object, float t = 0.0f);
	/**
	 * Removes an object from the lane.
	 * \param object The object to remove
	 * \return True on success, False on failure
	 */
	virtual bool removeObject(Object* object);
	/**
	 * Changes the object's t-coordinate. Slightly more efficient than calling 
	 * removeObject(obj) followed by addObject(obj, newT).
	 * \param object The object to relocate
	 * \param newT The new t coordinate to give the object
	 * \return True on success, False on failure
	 */ 
	virtual bool  setT(Object* object, float newT);
	/**
	 * Fetches the t-coordinate of the given object. O(log n).
	 * \param object The object to locate
	 * \return The t-coordinate of the object
	 */
	virtual float getT(Object* object);
	/**
	 * A method that just Does The Right Thing to find the t-coordinate on this lane that 
	 * corresponds to the given t coordinate on otherLane. Will work if otherLane is somehow
	 * part of this lane, or addInterference has been called with it. Otherwise returns -1.0.
	 * \param otherLane The lane to map the coordinate from
	 * \param t The t-coordinate to map
	 * \return The corresponding t-coordinate on this lane, or -1.0.
	 */
	virtual float mapT(Lane* otherLane, float t);
	/**
	 * Internal method, for mapping velocities. Computes dot product of lane tangents.
	 * \param otherLane The lane to map the tangent from
	 * \param t The t-coordinate on the other lane.
	 * \param mappedT The t-coordinate on this lane.
	 * \return The dot-product of the tangents of the lanes' splines at the given coordinates
	 */
	virtual float mapTangent(Lane* otherLane, float t, float mappedT);
	/**
	 * If you suspect that another lane may overlap with this lane in some places, you can call
	 * this method on it, and the lanes will become aware of each other's presence.
	 * \param otherLane The overlapping lane
	 * \param threshold The minimum distance between lanes to be considered "overlapping"
	 * \param subResolution The number of points to evaluate on each lane per cached data point
	 * \return True if there is an overlap, False otherwise
	 */
	virtual bool addInterference(Lane* otherLane, float threshold, unsigned int subResolution = 10);
	/**
	 * Get the first point of overlap with the given lane, after the given t coordinate (on this lane).
	 * \param otherLane The overlapping lane
	 * \param t The minimum t-coordinate of the overlap (in this lane's coordinate system)
	 * \return A description of the overlapping area
	 */
	virtual LaneInterferenceDesc* getInterferenceAfter(Lane* otherLane, float t);
	/**
	 * \param otherLane A potentially overlapping lane, on which addInterference has already been called
	 * \return True if the lanes overlap, False otherwise
	 */
	virtual bool interferesWith(Lane* otherLane);
	/**
	 * \param otherLane Another lane
	 * \return True if this lane physically contains the other lane as a sub-component (only possible for MultiLanes).
	 */
	virtual bool contains(Lane* otherLane);
	/**
	 * Gets the first object before the given t coordinate.
	 * \param t The point of reference
	 * \param classID If this is anything but "object", find only objects with a specific classID
	 * \param interference If true, consider interference in determining the closest object.
	 * \return A t-coordinate, Object* pair
	 */
	virtual std::pair<float, Object*>
		getObjectBefore(float t, std::string const& classID = "object", bool interference = true);
	/**
	 * Gets the first object after the given t coordinate.
	 * \param t The point of reference
	 * \param classID If this is anything but "object", find only objects with a specific classID
	 * \param interference If true, consider interference in determining the closest object.
	 * \return A t-coordinate, Object* pair
	 */
	virtual std::pair<float, Object*> 
		getObjectAfter(float t, std::string const& classID = "object", bool interference = true);
	
	Spline* spline; ///< the spline describing the Lane's geometry

	friend class MultiLane;
	
protected:

	// used in the implementation of mapT, to account for MultiLane behavior
	virtual float auxMapT(Lane* otherLane, float t);
	// used in the implementation of mapT, to account for interference
	virtual float mapInterferenceT(LaneInterferenceDesc const& idesc, float t);
	
	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);

	std::map<Object*, float> objToT;
	std::multimap<float, Object*> tToObj;

	std::multimap<float, LaneInterferenceDesc> interferences;
	std::set<Lane*> interferingLanes;
};

/**
 * Since a Spline can be constructed from a number of smaller Splines, you can do the same with a Lane.
 * In this case, a new copy is made of the spline data, but a MultiLane refers to its component Lanes
 * when dealing with its list of objects. Thus, inserting an object into a MultiLane will actually insert
 * said object into a different Lane object. Deleting a MultiLane has no effect on the Lanes it is made of.
 * 
 * I won't independently document the MultiLane's methods, as its interface is absolutely identical to that of a
 * Lane, aside from its initialization function, which is analogous to the constructor for stitching multiple Splines
 * into a larger Spline.
 */
class MultiLane: public Lane
{
public:
	MultiLane();
	MultiLane(std::vector<LaneSegment> const& subLanes);
	virtual ~MultiLane();
	virtual std::string getClassID() const { return "MultiLane"; }

	void initialize(std::vector<LaneSegment> const& subLanes);
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a TestObject
	/// \return A TestObject*, pointing to the same object as source.	
	static MultiLane* downcast(Object* source);
	
	virtual bool simulate(float ticks);
	virtual void draw();

	virtual bool addObject(Object* object, float t = 0.0f);
	virtual bool removeObject(Object* object); 
	virtual bool  setT(Object* object, float newT);
	virtual float getT(Object* object);

	virtual float mapT(Lane* otherLane, float t);
	virtual float mapT(MultiLane* otherLane, float t);
	
	virtual bool addInterference(Lane* otherLane, float threshold, unsigned int subResolution = 10);
	virtual LaneInterferenceDesc* getInterferenceAfter(Lane* otherLane, float t);
	virtual bool interferesWith(Lane* otherLane);
	virtual bool contains(Lane* otherLane);

	virtual std::pair<float, Object*> 
		getObjectBefore(float t, std::string const& classID = "object", bool interference = true);
	virtual std::pair<float, Object*>
		getObjectAfter(float t, std::string const& classID = "object", bool interference = true);
		
protected:

	virtual float auxMapT(Lane* otherLane, float t);
	std::pair<unsigned int, float> mapToSubLane(float t);
	float mapFromSubLane(unsigned int index, float t);
	
	virtual void serialize(std::ostream& os) const;
	virtual void deserialize(std::istream& is);

	float totalSize;
	std::vector<LaneSegment> subLanes;
	std::vector<unsigned int> chunks;
};


}

#endif /*LANE_H_*/
