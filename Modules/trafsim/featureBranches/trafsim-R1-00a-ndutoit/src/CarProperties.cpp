#include "CarProperties.h"
#include "Serialization.h"

namespace TrafSim
{

CarProperties::CarProperties():
	size(2.0f, 5.0f),
	color(1.0f, 0.0f, 0.0f, 1.0f),
	comfyAccel(2.0f),
	maxAccel(6.0f),
	maxAngularAccel(5.0f),
	safetyTime(1.5f),
	minDistance(2.0f),
	carWaitFactor(0.5f),
	gapAcceptFactor(0.5f),
	accelDelta(2.0f),
	curveEvalInterval(5.0f)
{	
}

std::ostream& operator<<(std::ostream& os, CarProperties const& props)
{
	os << "CarProperties( " << std::endl;
	
	os << "size --> " << props.size << std::endl;
	os << "color --> " << props.color << std::endl;
	os << "comfyAccel --> " << props.comfyAccel << std::endl;
	os << "maxAccel --> " << props.maxAccel << std::endl;
	os << "maxAngularAccel --> " << props.maxAngularAccel << std::endl;
	os << "safetyTime --> " << props.safetyTime << std::endl;
	os << "minDistance --> " << props.minDistance << std::endl;
	os << "carWaitFactor --> " << props.carWaitFactor << std::endl;
	os << "gapAcceptFactor --> " << props.gapAcceptFactor << std::endl;
	os << "accelDelta --> " << props.accelDelta << std::endl;
	os << "curveEvalInterval --> " << props.curveEvalInterval << std::endl;
	
	return os << ") ";
}

std::istream& operator>>(std::istream& is, CarProperties& props)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	try
	{
		matchString(is, "CarProperties");
		matchString(is, "(");
		
		DESERIALIZE_RN(is, "size", props.size);
		DESERIALIZE_RN(is, "color", props.color);
		DESERIALIZE_RN(is, "comfyAccel", props.comfyAccel);
		DESERIALIZE_RN(is, "maxAccel", props.maxAccel);
		DESERIALIZE_RN(is, "maxAngularAccel", props.maxAngularAccel);
		DESERIALIZE_RN(is, "safetyTime", props.safetyTime);
		DESERIALIZE_RN(is, "minDistance", props.minDistance);
		DESERIALIZE_RN(is, "carWaitFactor", props.carWaitFactor);
		DESERIALIZE_RN(is, "gapAcceptFactor", props.gapAcceptFactor);
		DESERIALIZE_RN(is, "accelDelta", props.accelDelta);
		DESERIALIZE_RN(is, "curveEvalInterval", props.curveEvalInterval);
		
		matchString(is, ")");
	}
	catch(ParseError& e)
	{
		e.info = "Failed to parse CarProperties:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		throw ParseError("Failed to parse CarProperties:\n" + std::string(e.what()) );
	}
	
	return is;
}

}
