
%define DOC_COLOR(method, string)
%feature("docstring", string) TrafSim::Color::method;
%enddef

DOC_COLOR( Color,
"""
You can construct a color one of two ways:

>>> print Color()
Color(1, 1, 1, 1)
>>> print Color(0.1, 0.2, 0.3, 0.4)
Color(0.1, 0.2, 0.3, 0.4)

The default Color is opaque white.
""")

DOC_COLOR( operator+,
"""
Additive blending:

>>> print Color(0.1, 0.1, 0.1, 0.1) + Color(0.1, 0.2, 0.3, 0.4)
Color(0.2, 0.3, 0.4, 0.5)

""")

DOC_COLOR( operator-,
"""
Subtractive blending:

>>> print Color(0.1, 0.1, 0.1, 0.1) - Color(0.1, 0.2, 0.3, 0.4)
Color(0, -0.1, -0.2, -0.3)

""")

DOC_COLOR( operator*,
"""
Brightness scaling, or channel-wise modulation:

>>> print Color(0.2, 0.2, 0.2, 0) * Color(0.1, 0.2, 0.3, 0.4)
Color(0.02, 0.04, 0.06, 0)
>>> print Color(0.1, 0.2, 0.3, 0.4) * 5.0
Color(0.5, 1, 1.5, 2)

""")

DOC_COLOR( operator/,
"""
Brightness scaling, or channel-wise modulation:

>>> print Color(0.2, 0.2, 0.2, 0) / Color(0.1, 0.2, 0.3, 0.4)
Color(2, 1, 0.666667, 0)
>>> print Color(0.1, 0.2, 0.3, 0.4) / 5.0
Color(0.02, 0.04, 0.06, 0.08)

If rhs or any component of the denominator is zero, the results are undefined. 
If debugging is enabled, a failed assertion will terminate the program.

""")

DOC_COLOR( operator+=,
"""
Destructive additive blending:

>>> c = Color(0.1, 0.1, 0.1, 0.1)
>>> c += Color(0.1, 0.2, 0.3, 0.4)
>>> print c
Color(0.2, 0.3, 0.4, 0.5)

""")

DOC_COLOR( operator-=,
"""
Destructive subtractive blending:

>>> c = Color(0.1, 0.1, 0.1, 0.1)
>>> c -= Color(0.1, 0.2, 0.3, 0.4)
>>> print c
Color(0, -0.1, -0.2, -0.3)

""")

DOC_COLOR( operator*=,
"""
Destructive brightness scaling, or channel-wise modulation:

>>> c = Color(0.2, 0.2, 0.2, 0)
>>> c *= Color(0.1, 0.2, 0.3, 0.4)
>>> print c
Color(0.02, 0.04, 0.06, 0)
>>> c = Color(0.1, 0.2, 0.3, 0.4)
>>> c *= 5.0
>>> print c
Color(0.5, 1, 1.5, 2)

""")

DOC_COLOR( operator/=,
"""
Destructive brightness scaling, or channel-wise modulation:

>>> c = Color(0.2, 0.2, 0.2, 0)
>>> c /= Color(0.1, 0.2, 0.3, 0.4)
>>> print c
Color(2, 1, 0.666667, 0)
>>> c = Color(0.1, 0.2, 0.3, 0.4)
>>> c /= 5.0
>>> print c
Color(0.02, 0.04, 0.06, 0.08)

If rhs or any component of the denominator is zero, the results are undefined. 
If debugging is enabled, a failed assertion will terminate the program.

""")

DOC_COLOR( __str__,
"""
A color is represented by the format Color(r, g, b, a).
This format is readable by Color.parse()

>>> s = str(Color(0.1, 0.2, 0.3, 0.4))
>>> s
'Color(0.1, 0.2, 0.3, 0.4)'
>>> print Color.parse(s)
Color(0.1, 0.2, 0.3, 0.4)

""")

DOC_COLOR( parse,
"""
A color is represented by the format Color(r, g, b, a).
This format is readable by Color.parse()

>>> s = str(Color(0.1, 0.2, 0.3, 0.4))
>>> s
'Color(0.1, 0.2, 0.3, 0.4)'
>>> print Color.parse(s)
Color(0.1, 0.2, 0.3, 0.4)

""")

%feature("docstring", 
"""
Sets the OpenGL state to use this Color as the rendering color.
Note that internally, OpenGL color channels are clamped to [0, 1].
It is often convenient to allow for unclamped color values in
program logic, but be aware that any such extraneous information
is ignored by OpenGL.

>>> glSetColor( Color(0.5, 0.5, 0.5, 0.5) )

(The next object to be rendered should appear a translucent gray).
"""
) TrafSim::glSetColor;
