
%define DOC_VECTOR(method, string)
%feature("docstring", string) TrafSim::Vector::method;
%enddef

DOC_VECTOR( Vector, 
"""
You can construct a vector one of two ways:

>>> print Vector()
Vector(0, 0)
>>> print Vector(1, 1)
Vector(1, 1)

""")

DOC_VECTOR( operator+,
"""
Arithmetic addition:

>>> print Vector(1, 2) + Vector(3, 4)
Vector(4, 6)

""")

DOC_VECTOR( operator-,
"""
Arithmetic subtraction or negation:

>>> print Vector(1, 2) - Vector(3, 4)
Vector(-2, -2)
>>> print -Vector(1, 2)
Vector(-1, -2)

""")

DOC_VECTOR( operator*,
"""
Scalar multiplication:

>>> print Vector(1, 2) * 4.0
Vector(4, 8)

""")

DOC_VECTOR( operator/,
"""
Scalar division:

>>> print Vector(1, 2) / 4.0
Vector(0.25, 0.5)

If rhs is zero, the results are undefined. 
If debugging is enabled, a failed assertion will terminate the program.

""")

DOC_VECTOR( operator+=,
"""
Destructive vector addition:

>>> v = Vector(1, 2)
>>> v += Vector(3, 4)
>>> print v
Vector(4, 6)

""")

DOC_VECTOR( operator-=,
"""
Destructive vector subtraction:

>>> v = Vector(1, 2)
>>> v -= Vector(3, 4)
>>> print v
Vector(-2, -2)

""")

DOC_VECTOR( operator*=,
"""
Destructive scalar multiplication:

>>> v = Vector(1, 2)
>>> v *= 10.0
>>> print v
Vector(10, 20)

""")

DOC_VECTOR( operator/=,
"""
Destructive scalar division:

>>> v = Vector(1, 2)
>>> v /= 10.0
>>> print v
Vector(0.1, 0.2)

If rhs is zero, the results are undefined. 
If debugging is enabled, a failed assertion will terminate the program.

""")

DOC_VECTOR( dist2,
"""
Square of the distance between two vectors.
More efficient than (v1 - v2).length().

>>> Vector.dist2( Vector(0, 3), Vector(4, 0) )
25.0

""")

DOC_VECTOR( dot,
"""
Dot product.

>>> Vector.dot( Vector(1.0, 1.0), Vector(1.0, 0.0) )
1.0

""")

DOC_VECTOR( length,
"""
sqrt(v.x^2 + v.y^2)

>>> Vector(3.0, 4.0).length()
5.0

""")

DOC_VECTOR( normalized,
"""
Returns a new vector, scaled down to length 1.

>>> print Vector(1.0, 1.0).normalized()
Vector(0.707107, 0.707107)

If the vector's original length is zero, the results are undefined. 
If debugging is enabled, a failed assertion will terminate the program.

""")

DOC_VECTOR( perpendicular,
"""
Returns one of the vector's perpendiculars.

>>> print Vector(1.0, 2.0).perpendicular()
Vector(-2, 1)

""")

DOC_VECTOR( normalize,
"""
The destructive equivalent of Vector.normalized().

>>> v = Vector(1.0, 1.0)
>>> v.normalize()
>>> print v
Vector(0.707107, 0.707107)

If the vector's original length is zero, the results are undefined. 
If debugging is enabled, a failed assertion will terminate the program.

""")

DOC_VECTOR( __str__,
"""
A vector is represented by the format Vector(x, y).
This format is readable by Vector.parse()

>>> s = str(Vector(10.0, 20.0))
>>> s
'Vector(10, 20)'
>>> print Vector.parse(s)
Vector(10, 20)

""")

DOC_VECTOR( parse,
"""
A vector is represented by the format Vector(x, y).
This format is readable by Vector.parse()

>>> s = str(Vector(10.0, 20.0))
>>> s
'Vector(10, 20)'
>>> print Vector.parse(s)
Vector(10, 20)

""")

%feature("autodoc", "0") TrafSim::Vector::intersection;
DOC_VECTOR( intersection,
"""
Computes the point of intersection of the line segments defined
by the given vectors. If no such point exists, returns false.
Otherwise, stores the result in the Vector named 'output' and returns true.

>>> v = Vector()
>>> Vector.intersection( Vector(0.0, 0.0), Vector(1.0, 1.0),
...			 Vector(1.0, 0.0), Vector(0.0, 1.0),
...			 v ) 
True
>>> print v
Vector(0.5, 0.5)
>>> Vector.intersection( Vector(0.0, 0.0), Vector(1.0, 1.0),
...			 Vector(1.0, 0.0), Vector(1.1, 1.0),
...			 v )
False
>>> print v
Vector(0.5, 0.5)

""")



