import sys

class SuperDynamic:
    class_list = []

    @classmethod
    def _qualified_name(self, cls):
        try:
            return 'main.' + str(cls).split("'")[1]
        except IndexError:
            try:
                return 'main.' + str(cls).split('__main__.')[1]
            except IndexError:
                return 'main.' + str(cls)

    def __init__(self):
        self.class_list.append( ( self, self._qualified_name(self.__class__) ) )
  
    @classmethod
    def reload(self):
        main = sys.modules['__main__']
                    
        for obj, class_name in self.class_list:
            obj.__class__ = eval(class_name)
