#include "Viewport.hh"
#include "Color.hh"
#include "Vector.hh"

#include <string>
#include <cstdio>

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>

namespace TrafSim
{

/* ******** CONSTRUCTORS AND DESTRUCTORS ******** */

Viewport::Viewport(): 
	screen(0), screenWidth(0), screenHeight(0), 
	defaultPhysicalSize(100.0f, 100.0f), defaultPhysicalPos(0.0f, 0.0f),
	physicalSize(defaultPhysicalSize), physicalPos(defaultPhysicalPos),
	panZoomMode(NONE), panZoomEnabled(true), altDown(false), closed(true)
{}

Viewport::~Viewport()
{
	if (!closed)
		close();
}

/* ******** INITIALIZATION AND TERMINATION ******** */

bool Viewport::open(unsigned int width, unsigned int height, std::string const& caption)
{	
	if( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		std::cerr << "Unable to init SDL: " << SDL_GetError();
		return false;
	}
	
	closed = false;
	
	// initialize parameters to their default values
	resize(width, height);
	setPhysicalSize(defaultPhysicalSize);
	setPhysicalPosition(defaultPhysicalPos);
	
	SDL_WM_SetCaption(caption.c_str(), caption.c_str());
	
	// set up the rendering environment for the first frame
	glClear(GL_COLOR_BUFFER_BIT);
	repaint();
	
	glEnable(GL_LINE_STIPPLE);
	
	return true;
}

bool Viewport::close()
{
	SDL_Quit();
	closed = true;
	
	return true;
}

/* ******** DATA MEMBER ACCESS FUNCTIONS ******** */

unsigned int Viewport::getScreenWidth() const
{
	return screenWidth;
}

unsigned int Viewport::getScreenHeight() const
{
	return screenHeight;
}

Vector const& Viewport::getPhysicalSize() const
{
	return physicalSize;
}

Vector const& Viewport::getPhysicalPosition() const
{
	return physicalPos;
}

bool Viewport::isPanZoomEnabled() const
{
	return panZoomEnabled;
}

/* ******** PROPERTY SETTING FUNCTIONS ******** */

void Viewport::enablePanZoom()
{
	panZoomEnabled = true;
}

void Viewport::disablePanZoom()
{
	panZoomEnabled = false;
}

bool Viewport::resize(unsigned int width, unsigned int height)
{
	if (closed)
		return false;
	
	// make sure there's no aspect ratio distortion caused by
	// resizing one dimension more than the other
	if (screenWidth > 0 && screenHeight > 0)
		setPhysicalSize( Vector( physicalSize.x * (float)width  / (float)screenWidth,
								 physicalSize.y * (float)height / (float)screenHeight ));
		
	screenWidth = width;
	screenHeight = height;

	// reset the screen dimensions
	screen = SDL_SetVideoMode(screenWidth, screenHeight, 32, 
								SDL_HWSURFACE | SDL_ANYFORMAT |
								SDL_RESIZABLE | SDL_DOUBLEBUF | SDL_OPENGL);
	glViewport(0, 0, screenWidth, screenHeight);
	
	return true;
}

bool Viewport::setPhysicalSize(Vector const& size)
{
	if (closed)
		return false;
	
	physicalSize = size;
	
	// setup the OpenGL viewport
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, physicalSize.x, 0, physicalSize.y);
	glMatrixMode(GL_MODELVIEW);	
	
	return true;
}

bool Viewport::setPhysicalPosition(Vector const& position)
{
	if (closed)
		return false;
		
	physicalPos = position;

	return true;
}

bool Viewport::setBackgroundColor(Color const& c)
{
	if (closed)
		return false;
		
	// set the color used in the glClear(GL_COLOR_BUFFER_BIT) call
	glClearColor(c.r, c.g, c.b, c.a);
	
	return true;	
}

/* ******** MAIN WINDOWING API ******** */

bool Viewport::pollEvents(ViewportEvent& viewportEvent)
{
	if (closed)
		return false;
		
	SDL_Event event;
	
	// by default, no event is passed to the client app
	viewportEvent.type = EMPTY;
	
	if ( SDL_PollEvent(&event) ) 
	{
        switch (event.type) 
        {
        	case SDL_MOUSEMOTION:
        		_handleMouseMotion( (float)event.motion.xrel, (float)event.motion.yrel );  // pan/zoom function
        		
        		viewportEvent.type = MOUSEMOTION;
        		viewportEvent.pos = _translateCoords(event.motion.x, event.motion.y);
        	
        		break;        		
            
            case SDL_MOUSEBUTTONDOWN:
            	_handleMouseButtonDown( event.button.button, event.button.x, event.button.y ); // pan/zoom function
            	
            	viewportEvent.type = MOUSEBUTTONDOWN;
        		viewportEvent.button = (unsigned int)event.button.button;
        		viewportEvent.pos = _translateCoords(event.button.x, event.button.y);
        		
            	break;
            
            case SDL_MOUSEBUTTONUP:
            	_handleMouseButtonUp( event.button.button, event.button.x, event.button.y );  // pan/zoom function
            
            	viewportEvent.type = MOUSEBUTTONUP;
        		viewportEvent.button = (unsigned int)event.button.button;
        		viewportEvent.pos = _translateCoords(event.button.x, event.button.y);
        		
            	break;
            	
            case SDL_KEYDOWN:
            	_handleKeyDown( event.key.keysym.sym );
            	
              	viewportEvent.type = KEYDOWN;
        		viewportEvent.button = (unsigned int)event.key.keysym.sym;
       		
            	break;
            	
            case SDL_KEYUP:
            	_handleKeyUp( event.key.keysym.sym );
            	
            	viewportEvent.type = KEYUP;
        		viewportEvent.button = (unsigned int)event.key.keysym.sym;
        		
            	break;
        	
        	// the following two events are signalled when something happens that may
        	// require the client area to be repainted. so, pass that on to the client app
        	case SDL_VIDEORESIZE:
        		resize(event.resize.w, event.resize.h);
        		viewportEvent.type = REPAINT;
        		break;
        		
        	case SDL_ACTIVEEVENT:
        		if (event.active.state == SDL_APPINPUTFOCUS ||
        			event.active.state == SDL_APPACTIVE)
        			if (event.active.gain)
        				viewportEvent.type = REPAINT;
        		break;
            
            case SDL_QUIT:        	
            	close();
            	viewportEvent.type = QUIT;    
        }
        
        return true;
    }
	
	return false;
}

bool Viewport::repaint()
{
	if (closed)
		return false;
	
	SDL_GL_SwapBuffers(); 			// copy the work buffer to the screen
	glClear(GL_COLOR_BUFFER_BIT);	// clear out the work buffer
	
	// reset the rendering environment
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	// move the "camera" to the correct location
	glTranslatef(physicalSize.x * 0.5f - physicalPos.x,
				 physicalSize.y * 0.5f - physicalPos.y,
				 0.0f);

	return true;
}

/* ******** PRIVATE FUNCTIONS ******** */

void Viewport::_handleMouseMotion(float xrel, float yrel)
{
	const float zoomFactor = 3.0f;
	
	if (panZoomMode == PAN)
	{
		Vector newPan(physicalPos.x - xrel * physicalSize.x / (float)screenWidth,
					  physicalPos.y + yrel * physicalSize.y / (float)screenHeight);
		setPhysicalPosition(newPan);
	}
	else if (panZoomMode == ZOOM)
	{
		// the use of 'yrel' for both dimensions is intentional. we only want
		// the mouse's vertical position to control zoom.
		Vector newZoom(physicalSize.x + yrel * zoomFactor,
					   physicalSize.y + yrel * zoomFactor);

		// weird things happen if you let the zoom value drop too far
		if (newZoom.x < 1.0f)
			newZoom.x = 1.0f;
		if (newZoom.y < 1.0f)
			newZoom.y = 1.0f;
		
		setPhysicalSize(newZoom);
	}
}

void Viewport::_handleMouseButtonDown(unsigned int button, unsigned int x, unsigned int y)
{
	if (panZoomEnabled && altDown)
	{
		if (button == SDL_BUTTON_LEFT)
			panZoomMode = PAN;
		else if (button == SDL_BUTTON_RIGHT)
			panZoomMode = ZOOM;
	}
}

void Viewport::_handleMouseButtonUp(unsigned int button, unsigned int x, unsigned int y)
{
	panZoomMode = NONE;
}

void Viewport::_handleKeyDown(SDLKey key)
{
	if (key == SDLK_z)
		altDown = true;	
}

void Viewport::_handleKeyUp(SDLKey key)
{
	if (key == SDLK_z)
		altDown = false;
}
	
Vector Viewport::_translateCoords(int x, int y)
{
	return Vector( (x - (float)screenWidth / 2)  * physicalSize.x / (float)screenWidth + physicalPos.x,
				   ((float)screenHeight / 2 - y) * physicalSize.y / (float)screenHeight + physicalPos.y );
}

}
