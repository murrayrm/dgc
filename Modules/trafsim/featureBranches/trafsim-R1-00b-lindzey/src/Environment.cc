#include "Environment.hh"
#include <sstream>
#include <cassert>

namespace TrafSim
{
	
/* ******* CLASS ENVIRONMENT ******* */	
	
/* ******* CONSTRUCTORS AND DESTRUCTORS ******* */	
	
Environment::Environment(): bottomLeftBound(0.0f, 0.0f),
							topRightBound(100.0f, 100.0f)
{ }

/* ******* DIRECTORY FUNCTIONS ******* */

std::string const& Environment::addObject(Object* object, std::string const& proposedName)
{
	// generate a unique name for the object
	if (proposedName == "")
		object->name = _generateName(object->getClassID());
	else
		object->name = _generateName(proposedName);
		
	// store the object in both directories
	typeDirectory[object->getClassID()][object->name] = object;
	nameDirectory[object->name] = object;

	return object->name;
}

bool Environment::removeObject(std::string const& objName, bool canDelete)
{
	Object*	object = nameDirectory[objName];
	
	if (!object)
	{
		nameDirectory.erase(objName);
		return false;
	}
		
	// erase the object from both directories
	nameDirectory.erase(objName);
	typeDirectory[object->getClassID()].erase(objName);
	
	if (canDelete)
		delete object;
	
	return true;
}

std::string Environment::renameObject(std::string const& oldName, std::string const& newName)
{
	Object* object = nameDirectory[oldName];
	
	if (!object)
	{
		nameDirectory.erase(oldName);
		return "";
	}

	removeObject(oldName, false);
	addObject(object, newName);
	
	return object->getName();
}

Object* Environment::getObject(std::string const& objName)
{
	Object* result = nameDirectory[objName];
	
	if (!result)
		nameDirectory.erase(objName);
		
	return result;
}

ObjectMap const& Environment::getObjectsByClassID(std::string const& classID)
{
	return typeDirectory[classID];
}

void Environment::clearObjects(bool canDelete)
{
	if (canDelete)
	{
		for (ObjectMap::iterator i = nameDirectory.begin();
			 i != nameDirectory.end();
			 ++i)
		{
			delete i->second;
		}
	}
	
	nameDirectory.clear();
	typeDirectory.clear();
	nameList.clear();
}

/* ******* REFERENCE FRAME FUNCTIONS ******* */

Vector const& Environment::getBottomLeftBound() const
{
	return bottomLeftBound;
}
	
Vector const& Environment::getTopRightBound() const
{
	return topRightBound;
}

void Environment::setBounds(Vector const& bottomLeft, Vector const& topRight)
{
	bottomLeftBound = bottomLeft;
	topRightBound = topRight;
	
	assert(bottomLeftBound.x < topRightBound.x &&
		   bottomLeftBound.y < topRightBound.y);
}

/* ******* SIMULATION FUNCTIONS ******* */
	
void Environment::draw()
{
	for (ObjectMap::iterator i = nameDirectory.begin();
		 i != nameDirectory.end();
		 ++i)
	{
		i->second->draw();
	}
}

void Environment::simulate(float ticks)
{
	// if an object's simulate() function returns False,
	// that means it's asking to be deleted.
	for (ObjectMap::iterator i = nameDirectory.begin();
		 i != nameDirectory.end();)
	{
		Object* object = (i++)->second;
		
		if (!object->simulate(ticks))
			removeObject(object->name);
	}
}

/* ******* PRIVATE FUNCTIONS ******* */

std::string Environment::_generateName(std::string const& proposedName)
{
	if (nameList.find(proposedName) == nameList.end())
	{
		nameList[proposedName] = 1;
		return proposedName;
	}
	
	// if "name" is in use, return "name___n", where "n" is an integer
	std::ostringstream ostr;
	ostr << proposedName << "___" << nameList[proposedName]++;
	
	return ostr.str();
}

/* ******* SERIALIZATION FUNCTIONS ******* */

std::ostream& operator<<(std::ostream& os, Environment const& env)
{
	return os << "Environment( " << std::endl << 
		"bottomLeftBound --> " << env.bottomLeftBound << std::endl <<
		"topRightBound --> " << env.topRightBound << std::endl <<
		"nameDirectory --> " << env.nameDirectory << std::endl <<
		"nameList --> " << env.nameList << std::endl <<
		" )";	
}

std::istream& operator>>(std::istream& is, Environment & env)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	try
	{
		matchString(is, "Environment");
		matchString(is, "(");
		DESERIALIZE_RN(is, "bottomLeftBound", env.bottomLeftBound);
		DESERIALIZE_RN(is, "topRightBound", env.topRightBound);
		DESERIALIZE_RN(is, "nameDirectory", env.nameDirectory);
		DESERIALIZE_RN(is, "nameList", env.nameList);
		matchString(is, ")");
	}
	catch(ParseError& e)
	{
		e.info = "Failed to parse Environment:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		throw ParseError("Failed to parse Environment:\n " + std::string(e.what()) );
	}
	
	env.typeDirectory.clear();
	
	for (ObjectMap::iterator i = env.nameDirectory.begin();
	 i != env.nameDirectory.end();
	 ++i)
	{
		Object* object = i->second;	
		object->name = i->first;    // reload the object's name
		
		// store the object in the classID index as well
		env.typeDirectory[object->getClassID()][object->name] = object;
		
		// replace string references with actual pointers
		object->_resolveDependencies(env);
	}
	
	return is;	
}

/* ******* CLASS TESTOBJECT ******* */

TestObject::TestObject(): reference(0) 
{
}

std::string TestObject::getClassID() const 
{ 
	return "TestObject"; 
}
	
void TestObject::draw() 
{ 
	std::cout << "Called " << this->getName() << ".draw()\n";
}
	
bool TestObject::simulate(float ticks)
{
	std::cout << "Called " << this->getName() << ".simulate()\n";
	return ticks < 1.0f;
}
	
void TestObject::serialize(std::ostream& os) const
{
	if (!reference)
		os << "reference --> None";
	else
		os << "reference --> " << reference->getName();
}
	
void TestObject::deserialize(std::istream& is)
{
	std::string reference;
	DESERIALIZE(is, reference);
	
	// add the string reference, to be resolved later (provided that
	// this TestObject is being loaded as part of an Environment, not
	// on its own).
	this->_addReference(reference, &(this->reference));
}
	
TestObject* TestObject::downcast(Object* source)
{
	return dynamic_cast<TestObject*>(source);
}

		
}
