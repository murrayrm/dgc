#ifndef COLOR_H_
#define COLOR_H_

#include "Serialization.hh"
#include <sstream>

namespace TrafSim
{

/**
 * A 4D quasi-vector (the mathematical kind) utility class, for TrafSim. 
 * This represents a color, in RGBA format. 
 */
class Color
{
public:


	/* ******** CONSTRUCTORS ******** */
	
	/// The default constructor creates a solid white color.
	Color();
	
	/// A constructor for defining colors inline.
	Color(float r, float g, float b, float a);
	
	/* ******** BINARY ARITHMETIC OPERATIONS ******** */
	
	/// Additive blending operator 
	/// \param rhs The right hand side of the operation
	/// \return A new color
	Color operator+(Color const& rhs) const;

	/// Subtractive blending operator 
	/// \param rhs The right hand side of the operation
	/// \return A new color
	Color operator-(Color const& rhs) const;
	
	/// Modulates a color by another color 
	/// \param rhs The right hand side of the operation
	/// \return A new color
	Color operator*(Color const& rhs) const;
	
	/// Modulates a color by another color 
	/// \param rhs The right hand side of the operation. Must have all channels non-zero.
	/// \return A new color
	Color operator/(Color const& rhs) const;
	
	/// Modulates a color by a scalar value (changes the brightness) 
	/// \param rhs The right hand side of the operation
	/// \return A new color
	Color operator*(float rhs) const;
	
	/// Modulates a color by a scalar value (changes the brightness)
	/// \param rhs The right hand side of the operation. Must be non-zero.
	/// \return A new color
	Color operator/(float rhs) const;
	
	/* ******** UNARY ARITHMETIC OPERATIONS ******** */
	
	/// Unary additive blending 
	/// \param rhs The right hand side of the operation
	/// \return *this
	Color& operator+=(Color const& rhs);

	/// Unary subtractive blending 
	/// \param rhs The right hand side of the operation
	/// \return *this
	Color& operator-=(Color const& rhs);
	
	/// Unary color modulation 
	/// \param rhs The right hand side of the operation
	/// \return *this
	Color& operator*=(Color const& rhs);
	
	/// Unary color modulation 
	/// \param rhs The right hand side of the operation. Must have all channels non-zero.
	/// \return *this
	Color& operator/=(Color const& rhs);
	
	/// Unary scalar modulation 
	/// \param rhs The right hand side of the operation
	/// \return *this
	Color& operator*=(float rhs);
	
	/// Unary scalar modulation 
	/// \param rhs The right hand side of the operation. Must be non-zero.
	/// \return *this	
	Color& operator/=(float rhs);
	
	/* ******** UTILITY OPERATIONS ******** */
	
	/// Prints the vector's string representation to an output stream
	/// \param os The output stream to print to
	/// \param c The color object to print
	/// \return The same output stream 
	friend std::ostream& operator<<(std::ostream& os, Color const& c);
	
	/// Reads a vector's string representation from an input stream
	/// \param is The input stream to read from
	/// \param c The color object to parse
	/// \return The same input stream
	friend std::istream& operator>>(std::istream& is, Color& c);
	
	/* ******** PUBLIC DATA ******** */
	
	float r;  ///< Red channel, from 0.0 to 1.0
	float g;  ///< Green channel, from 0.0 to 1.0
	float b;  ///< Blue channel, from 0.0 to 1.0
	float a;  ///< Alpha (transparency) channel, from 0.0 to 1.0
	
};

/* ******** EXTERNAL UTILITY OPERATIONS ******** */

/// Sets OpenGL's current rendering color
/// \param value The color to render with
void glSetColor(Color const& value);

}

#endif /*COLOR_H_*/
