#include "Spline.hh"

#include <GL/gl.h>
#include <cassert>
#include <cstring>
#include <cmath>

namespace TrafSim
{

  /* ********** CONSTRUCTORS AND DESTRUCTORS *********** */

  Spline::Spline(): cacheResolution(0), coordinates(0), 
		    normals(0), lengths(0), curvature(0) 
  {
    numForwardLanes = 1;
    numReverseLanes = 1;
  }

  Spline::Spline(std::vector<Vector> const& initPoints, unsigned int resolution): 
    points(initPoints), cacheResolution(resolution), coordinates(0), 
    normals(0), lengths(0), curvature(0) 
  {
    numForwardLanes = 1;
    numReverseLanes = 1;
  }

  Spline::Spline(Spline& original, float displacement, 
		 float relativeResolution, bool reversed):
    coordinates(0), normals(0), lengths(0), curvature(0)
  {
    initialize(original, displacement, relativeResolution, reversed);
    numForwardLanes = 1;
    numReverseLanes = 1;
  }

  Spline::Spline(std::vector<JoinDesc> const& parts):
    cacheResolution(0), coordinates(0), normals(0), 
    lengths(0), curvature(0)
  {
    initialize(parts);
    numForwardLanes = 1;
    numReverseLanes = 1;
  }

  Spline::~Spline()
  {
    if (coordinates) delete[] coordinates;
    if (normals)	 delete[] normals;
    if (lengths) 	 delete[] lengths;
    if (curvature)	 delete[] curvature;
  }

  /* ********** INITIALIZATION FUNCTIONS *********** */

  void Spline::initialize(std::vector<Vector> const& points, unsigned int resolution)
  {
    this->points = points;
    this->cacheResolution = resolution;
  }

  // helper for the initializer below. 
  Vector _displace(Vector const& p0, Vector const& p1,
		   float shorten, float displacement)
  {
    Vector norm(p1.y - p0.y, p0.x - p1.x);
    norm.normalize();
	
    return ( p0 + (p1 - p0) * shorten ) + norm * displacement;
  }

  void Spline::initialize(Spline& original, float displacement,
			  float relativeResolution, bool reversed)
  {
    points.clear();
    unsigned int stride = (unsigned int)( (original.cacheResolution - 1) *
					  relativeResolution );

    assert(original.points.size() >= 4 || original.isCached() );
    assert(stride >= 1);

    // cached data is required for this operation
    original.cacheData();
	
    Vector point0, point1, point_1, point_2;
    if (original.points.size() >= 4)          
      {
	// in this case "displace" from the actual starting and ending control points
		
	point0  = original.points[ 0 ];
	point1  = original.points[ 1 ];
	point_1 = original.points[ original.points.size() - 1 ];
	point_2 = original.points[ original.points.size() - 2 ];
      }
    else
      {
	// in this case, guess where the original starting and ending control points were
		
	point0  = original.coordinates[0] - original.normals[0].perpendicular() * original.lengths[1];
	point1  = original.coordinates[0];
	point_1 = original.coordinates[ original.cacheResolution ];
	point_2 = original.coordinates[ original.cacheResolution ] +
	  original.normals[ original.cacheResolution ].perpendicular() *
	  original.lengths[ original.cacheResolution ];
      }
	
    if (reversed)
      {
	points.push_back( _displace(point_2, point_1, relativeResolution, displacement) );
		
	for (int i = original.cacheResolution; i > 0; i -= stride)
	  points.push_back( original.coordinates[i] + original.normals[i] * displacement );
	points.push_back( original.coordinates[0] + original.normals[0] * displacement );
		
	points.push_back( _displace(point1, point0, relativeResolution, -displacement) );
      }
    else
      {
	points.push_back( _displace(point1, point0, relativeResolution, -displacement) );	
		
	for (unsigned int i = 0; i < original.cacheResolution; i += stride)
	  points.push_back( original.coordinates[i] + original.normals[i] * displacement );
	points.push_back( original.coordinates[ original.cacheResolution ] +
			  original.normals[ original.cacheResolution ] * displacement );
		
	points.push_back( _displace(point_2, point_1, relativeResolution, displacement) );
      }
	
    cacheResolution = original.cacheResolution;
  }

  std::vector<unsigned int> Spline::initialize(std::vector<JoinDesc> const& parts)
  {
    assert(!parts.empty());
    points.clear();
	
    std::vector<unsigned int> result;
    std::vector<JoinDesc>::const_iterator i;
    unsigned int j = 0;
    unsigned int* startIndex = new unsigned int[parts.size()];
    unsigned int* endIndex = new unsigned int[parts.size()];
	
    for (i = parts.begin(), j = 0; i != parts.end(); ++i, ++j)
      {
	JoinDesc const& jd = *i;
		
	assert(jd.segment->points.size() >= 4 || jd.segment->isCached());
		
	startIndex[j] = jd.segment->_mapTCoord(jd.t_start).first + 1;
	endIndex[j]   = jd.segment->_mapTCoord(jd.t_end).first;
		
	assert(endIndex[j] > startIndex[j]);
      }
	
    // include the starting and ending points of the full spline
    startIndex[0]--;
    endIndex[parts.size()-1]++;
	
    cacheResolution = -1;  // n_data_points = cacheResolution + 1, so we start at -1
    for (j = 0; j < parts.size(); ++j)
      {
	cacheResolution += endIndex[j] - startIndex[j];
	result.push_back(endIndex[j] - startIndex[j]);
      }
	
    this->coordinates = new Vector[cacheResolution + 1];
    this->normals = new Vector[cacheResolution + 1];
    this->lengths = new float[cacheResolution + 1];
    this->curvature = new float[cacheResolution + 1];
	
    Vector* coord_ptr = this->coordinates;
    Vector* normal_ptr = this->normals;
    float*	length_ptr = this->lengths;
    float*  curvature_ptr = this->curvature;
	
    // physically copy over all the relevant data from the parent splines
    for (i = parts.begin(), j = 0; i != parts.end(); ++i, ++j)
      {
	Spline* s = i->segment;
	unsigned int dataSize = endIndex[j] - startIndex[j];
		
	s->cacheData();
		
	Vector* s_coordinates = s->coordinates;
	s_coordinates += startIndex[j];
	memmove(coord_ptr, s_coordinates, sizeof(Vector) * dataSize);
	coord_ptr += dataSize;
		
	Vector* s_normals = s->normals;
	s_normals += startIndex[j];
	memmove(normal_ptr, s_normals, sizeof(Vector) * dataSize);
	normal_ptr += dataSize;		
				
	float* s_lengths = s->lengths;
	s_lengths += startIndex[j];
	memmove(length_ptr, s_lengths, sizeof(float) * dataSize);
		
	// TODO: BUG! what really needs to happen here is that normals, lengths, and curvature
	// should be re-evaluated at each of the "seams" in the combined spline. In practice,
	// the only thing that I need to re-evaluate is the length. Do the Right Thing later.
	if (i != parts.begin())
	  length_ptr[0] = ((coord_ptr - dataSize)[0] -
			   (coord_ptr - dataSize - 1)[0]).length();
		
	length_ptr += dataSize;		
				
	float* s_curvature = s->curvature;
	s_curvature += startIndex[j];
	memmove(curvature_ptr, s_curvature, sizeof(float) * dataSize);
	curvature_ptr += dataSize;			
      }	
	
    delete[] startIndex;
    delete[] endIndex;
	
    return result;
  }
	
  void Spline::cacheData(bool regenerate)
  {
    if (points.size() < 4 || (isCached() && !regenerate))
      return;
	
    assert(cacheResolution > points.size());
    this->coordinates = new Vector[cacheResolution + 1];
    this->normals = new Vector[cacheResolution + 1];
    this->lengths = new float[cacheResolution + 1];
    this->curvature = new float[cacheResolution + 1];
	
    float t = 0.0f;
    unsigned int i = 0;
	
    while (i <= cacheResolution)
      {
	coordinates[i] = _catmullRomEval(t);
		
	if (i > 0)
	  {
	    // calculate normals and lengths for all but the first point
			
	    normals[i] = Vector( coordinates[i  ].y - coordinates[i-1].y,
				 coordinates[i-1].x - coordinates[i  ].x );
	    normals[i].normalize();
			
	    lengths[i] = Vector( coordinates[i].x - coordinates[i-1].x,
				 coordinates[i].y - coordinates[i-1].y).length();
	  }
		
	if (i > 1)
	  {
	    // calculate curvature for all but the first point
			
	    Vector const& pt0 = coordinates[i - 2];
	    Vector const& pt1 = coordinates[i - 1];
	    Vector const& pt2 = coordinates[i];
			
	    float l2 = lengths[i];
	    float l1 = lengths[i-1];
			
	    if (l1 == 0 || l2 == 0)
	      {
		curvature[i-1] = 0.0f;
		continue;
	      }
			
	    // direction vectors
	    Vector d1 = (pt1 - pt0) / l1;
	    Vector d2 = (pt2 - pt1) / l2;
			
	    // curvature vector
	    Vector curve = (d2 - d1) / (l1 + l2) * 2.0f;
	    if (Vector::dot(curve, normals[i-1]) > 0)
	      curvature[i-1] = curve.length();
	    else
	      curvature[i-1] = -curve.length();
	  }
		
	t += 1.0f / cacheResolution;
	i ++;
      }
	
    normals[0] = normals[1];
    lengths[0] = 0.0f;
    curvature[0] = 0.0f;
    curvature[cacheResolution] = 0.0f;
  }

  bool Spline::isCached() const
  {
    return (coordinates != 0);
  }

  /* ********** DATA QUERYING FUNCTIONS *********** */

  Vector Spline::_catmullRomEval(float t) const
  {
    // figure out which four control points flank the given t coordinate
    unsigned int n_indices = points.size() - 3;
    float f_indices = (float)n_indices;
    double f_index;
	
    t = modf(t * f_indices, &f_index);
    unsigned int index = (unsigned int)f_index;
		
    if (index == n_indices)
      return points[points.size() - 2];
	
    // feed the starting control index and the t offset to the evaluator
    Vector pt;
    for (int i = -2; i < 2; i++)
      pt += points[index + i + 2] * _basis(i, t);
		
    return pt;
  }

  // catmull rom spline evaluator
  float Spline::_basis(unsigned int i, float t) const
  {
    switch(i)
      {
      case -2: return ((-t + 2.0f) * t - 1.0f) * t * 0.5f;
      case -1: return (((3.0f * t - 5.0f) * t) * t + 2.0f) * 0.5f;
      case  0: return ((-3.0f * t + 4.0f) * t + 1.0f) * t * 0.5f;
      case  1: return ((t - 1.0f) * t * t) * 0.5f;
      }
	
    assert(false);  // should never get here
    return 0.0f;
  }
	
  std::pair<unsigned int, float> Spline::_mapTCoord(float t) const
  {
    // figure out (index, offset) from raw t coord

    if (t >= 1.0f)
      return std::make_pair(cacheResolution, 0.0f);
    else if (t <= 0.0f)
      return std::make_pair(0, 0.0f);
		
    double f_index;
    float rest = modf( t * cacheResolution, &f_index);
	
    return std::make_pair( (unsigned int)f_index, rest );
  } 

  float Spline::_mapIndex(unsigned int index) const
  {
    // figure out raw t coord from index
	
    return index / (float)cacheResolution;
  }	

  unsigned int Spline::getResolution() const
  {
    return cacheResolution;
  }

  const Vector* Spline::getCoordinateData() const
  {
    assert(isCached());
    return coordinates;
  }

  const Vector* Spline::getNormalData() const
  {
    assert(isCached());
    return normals;
  }
	
  const float*  Spline::getLengthData() const
  {
    assert(isCached());
    return lengths;
  }

  const float*  Spline::getCurvatureData() const
  {
    assert(isCached());
    return curvature;
  }

  Vector Spline::evalCoordinate(float t) const
  {
    if (isCached())
      return _interpolate(coordinates, t);
    else
      return _catmullRomEval(t);
  }

  Vector Spline::evalNormal(float t) const
  {
    if (isCached())
      return _interpolate(normals, t);
    else
      {
	// calculate normals manually
		
	float dist = 1.0f / (float)cacheResolution;
		
	Vector norm = (_catmullRomEval(t - dist) - _catmullRomEval(t)).perpendicular();
	if (norm.x != 0.0f || norm.y != 0.0f)
	  norm.normalize();

	return norm;
      }
  }

  float Spline::evalCurvature(float t) const
  {
    if (isCached())
      return _interpolate(curvature, t);
    else
      {
	// calculate curvature manually
		
	float dist = 1.0f / (float)cacheResolution;
		
	Vector pt0 = _catmullRomEval(t - dist);
	Vector pt1 = _catmullRomEval(t);
	Vector pt2 = _catmullRomEval(t + dist);
		
	Vector d1 = pt1 - pt0;
	Vector d2 = pt2 - pt1;
		
	float l1 = d1.length();
	float l2 = d2.length();
		
	if (l1 == 0 || l2 == 0)
	  return 0.0f;
			
	d1 /= l1;
	d2 /= l2;
		
	Vector curve = (d2 - d1) / (l1 + l2) * 2.0f;
	if (Vector::dot(curve, evalNormal(t)) > 0)
	  return curve.length();
	else
	  return -curve.length();
      }
  }

  float Spline::evalLength(float t0, float t1) const
  {
    float sign = 1.0f;

    // reversed t0, t1 yield negative length
    if (t1 < t0)
      {
	std::swap(t0, t1);
	sign = -1.0f;
      }
	
    // no worky if not cached
    if (!isCached())
      return -2.0f;
		
    std::pair<unsigned int, float> coord0 = _mapTCoord(t0),
      coord1 = _mapTCoord(t1);
							   
    float sum = 0.0f;
    for (unsigned int i = coord0.first + 1; i <= coord1.first; i++)
      sum += lengths[i];
	
    if (coord1.first != cacheResolution)
      sum += coord1.second * lengths[coord1.first + 1];
    if (coord0.first != cacheResolution)
      sum -= coord0.second * lengths[coord0.first + 1];
		
    return sum * sign;
  }

  float Spline::evalDisplacement(float t0, float dist) const
  {
    if (dist <= 0.0f)
      return t0;
		
    // no worky if not cached
    if (!isCached())
      return -2.0f;

    // return -1 if past the end of the spline
    std::pair<unsigned int, float> coord = _mapTCoord(t0);
    if (coord.first == cacheResolution)
      return -1.0f;
	
    float sum = -coord.second * lengths[coord.first + 1];
    unsigned int i_new = coord.first;
	
    while (sum < dist)
      {
	i_new ++;
	if (i_new > cacheResolution)
	  return -1;
	sum += lengths[i_new];
      }
	
    float remainder = sum - dist;
	
    float r_new = 0.0f;
    if (lengths[i_new] != 0.0f)
      r_new = 1.0f - (remainder / lengths[i_new]);
	
    i_new --;
	
    return (i_new + r_new) / (float)cacheResolution;
  }		

  bool perpSegDrop(Vector p0, Vector p1, Vector x, Vector& result)
  {
    Vector norm = (p0 - p1).perpendicular();
    if (Vector::dot(norm, p0 - x) < 0)
      norm = -norm;
	
    Vector farX = x + norm * 1000.0f;
	
    return Vector::intersection(p0, p1, x, farX, result);
  }
		
  float Spline::closestToPoint(Vector const& point, unsigned int startIndex, unsigned int endIndex) const
  {
    Vector closest(-100000.0f, -100000.0f);
    unsigned int closestIndex = 0;
    assert(endIndex > startIndex);
    if (endIndex > cacheResolution + 1)
      endIndex = cacheResolution + 1;
		
    // no worky if not cached
    if (!isCached())
      return -2.0f;

    // find the closest cached data point
    for (unsigned int i = startIndex; i < endIndex; ++i)
      {
	Vector const& splinePoint = coordinates[i];
	if (Vector::dist2(point, splinePoint) < Vector::dist2(point, closest))
	  {
	    closest = splinePoint;
	    closestIndex = i;
	  }
      }
	
    Vector result;
    unsigned int resultIndex; 
	
    if (closestIndex != 0 && 
	perpSegDrop( closest, coordinates[closestIndex - 1], point, result ))
      {
	resultIndex = closestIndex - 1;
      }
    else if (closestIndex != cacheResolution &&
	     perpSegDrop( closest, coordinates[closestIndex + 1], point, result ))
      {
	resultIndex = closestIndex;
      }
    else
      return _mapIndex(closestIndex);
	
    Vector p0 = coordinates[resultIndex];
    Vector p1 = coordinates[resultIndex + 1];
	
    return _mapIndex(resultIndex) + 
      (result - p0).length() / lengths[resultIndex + 1] / (float)cacheResolution;
  }

  void Spline::getInterference(Spline& otherSpline, 
			       std::vector<SplineInterferenceDesc>& result,
			       float threshold, float subResolution)
  {
    this->cacheData();
    otherSpline.cacheData();
	
    bool inInterval = false;
    float tempMyT, tempOtherT, myT, otherT;
	
    float offsetInterval = 1.0f / cacheResolution;
    for (unsigned int i = 0; i <= cacheResolution; ++i)
      for (float offset = 0.0f; offset < offsetInterval; offset += offsetInterval * (1.0f / subResolution))
	{
	  myT = _mapIndex(i) + offset;
	  Vector myPoint = evalCoordinate(myT);
			
	  otherT = otherSpline.closestToPoint( myPoint );
	  Vector otherPoint = otherSpline.evalCoordinate( otherT );
			
	  if ( (myPoint - otherPoint).length() < threshold )
	    {
	      if (!inInterval)
		{
		  inInterval = true;
		  tempMyT = myT;
		  tempOtherT = otherT;
		}
	    }
	  else
	    {
	      if (inInterval)
		{
		  result.push_back( std::make_pair(
						   JoinDesc( this, tempMyT, myT ),
						   JoinDesc( &otherSpline, tempOtherT, otherT) ) );
		}
				
	      inInterval = false;
	    }
	}
	
    if (inInterval)
      result.push_back( std::make_pair(
				       JoinDesc( this, tempMyT, 1.0f ),
				       JoinDesc( &otherSpline, tempOtherT, otherT) ) );
  }


  point2 Spline::getCenter()
  {
    Vector center = evalCoordinate(.5);
    return point2(center.x, center.y);
  }

  float Spline::getOrientation()
  {
    // grab the first and last points and calculate the orientation
    Vector start = evalCoordinate(0);
    Vector end = evalCoordinate(1);
    return atan2(end.y-start.y,end.x-start.x);
  }

  float Spline::getLength()
  {
    // make sure spline is cached
    if (!isCached())
      cacheData();
    // calculate the length of the spline
    return evalLength(0,1);
  }

  std::vector<std::vector<point2> > Spline::getBoundary()
  {
    // basically return the spline, but need to convert to point2 first
  
    std::vector<point2> boundary;
    for (std::vector<Vector>::iterator iter = points.begin();
	 iter != points.end(); iter++ ) {
      boundary.push_back(point2(iter->x,iter->y));
    }

    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    return boundaryWrapper;
  }
	
  /* ********** GENERIC OBJECT FUNCTIONS *********** */

  void Spline::draw()
  {
    draw(0.0f, 1.0f);
	
    glPushMatrix();
    glPointSize(5.0f);
	
    // draw some control points
    glColor4f(0.0f, 0.0f, 1.0f, 1.0f); 
    glBegin(GL_POINTS);
    
    for (unsigned int i = 0; i < points.size(); i++)
      {
    	glVertex3f( points[i].x, points[i].y, 0.0f);
      }
    
    glEnd();
    glPopMatrix();
  }

  void Spline::draw(float t0, float t1)
  {
    unsigned int i0 = _mapTCoord(t0).first;
    unsigned int i1 = _mapTCoord(t1).first;

    glPushMatrix();
    
    // draw the actual spline
    glSetColor(this->color);
    glBegin(GL_LINE_STRIP);	
    if (isCached())
      {
	for (unsigned int i = i0; i <= i1; ++i)
	  glVertex3f( coordinates[i].x, coordinates[i].y, 0.0f);
      }
    else if (points.size() >= 4)
      {
    	for (unsigned int i = i0; i <= i1; ++i)
	  {
	    Vector point = _catmullRomEval( _mapIndex(i) );
	    glVertex3f( point.x, point.y, 0.0f);
	  }
      }
    glEnd();
    
    
    
    // draw normals, just for fun
    if (isCached())
      for (unsigned int i = i0; i <= i1; ++i)
	if (curvature[i] != 0.0f)
	  {
	    glColor4f(0.03f / curvature[i], 0.0f, 30.0f * curvature[i], 1.0f);
	    (normals[i] / curvature[i] * 0.1f).draw(coordinates[i], 0.5f);
	  }
	
	
    glPopMatrix();
	
  }

  void Spline::serialize(std::ostream& os) const
  {
    os << "cacheResolution --> " << cacheResolution << std::endl;
    os << "points --> " << points << std::endl;
	
    if (isCached())
      {
	os << "cached --> " << 1 << std::endl;
	os << "coordinates --> " << std::make_pair(coordinates, (int)cacheResolution + 1) << std::endl;
	os << "normals --> " << std::make_pair(normals, (int)cacheResolution + 1) << std::endl;
	os << "lengths --> " << std::make_pair(lengths, (int)cacheResolution + 1) << std::endl;
	os << "curvature --> " << std::make_pair(curvature, (int)cacheResolution + 1) << std::endl;
      }
    else
      os << "cached --> " << 0 << std::endl;
  }

  void Spline::deserialize(std::istream& is)
  {
    DESERIALIZE(is, cacheResolution);
    DESERIALIZE(is, points);
	
    if (coordinates) delete[] coordinates;
    if (normals)	 delete[] normals;
    if (lengths) 	 delete[] lengths;
    if (curvature)	 delete[] curvature;
	
    int cached;
    DESERIALIZE(is, cached);
	
    if (cached)
      {	
	coordinates = new Vector[cacheResolution + 1];
	normals = new Vector[cacheResolution + 1];
	lengths = new float[cacheResolution + 1];
	curvature = new float[cacheResolution + 1];
			
	DESERIALIZE_RN(is, "coordinates", &coordinates);
	DESERIALIZE_RN(is, "normals", &normals);
	DESERIALIZE_RN(is, "lengths", &lengths);
	DESERIALIZE_RN(is, "curvature", &curvature);
      }
    else
      {
	coordinates = 0;
	normals = 0;
	lengths = 0;
	curvature = 0;
      }
  }

  Spline* Spline::downcast(Object* source)
  {
    return dynamic_cast<Spline*>(source);
  }

}
