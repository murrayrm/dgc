from libtrafsim_Py import *

from SplineEditor import SplineEditor

class Scenarios:
    def __init__(self, env, editor):
        self.environment = env
	self.editor = editor
    
    def UT_tplanner_RR_SO_LegalPass(self):
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalPass(self):
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalUturn(self):
	b = Obstacle(398992.68318241555-self.environment.xTranslate,3781259.8189164037-self.environment.yTranslate,2,15)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_Partial(self):
	b = Obstacle(399000.68318241555-self.environment.xTranslate,3781267.8189164037-self.environment.yTranslate,3)
	self.environment.addObject(b)

    def simple_straightline(self):
        print "A"
        
    def simple_turns(self):
        print "B"
        b1 = Obstacle(403961.84-self.environment.xTranslate,3778457.2-self.environment.yTranslate,-.35)
        self.environment.addObject(b1)
        b2 = Obstacle(403949.379988-self.environment.xTranslate,3778473.61999-self.environment.yTranslate,-.35)
        self.environment.addObject(b2)
        b3 = Obstacle(403964.779985-self.environment.xTranslate,3778483.97999-self.environment.yTranslate,-.35)
        self.environment.addObject(b3)

    def simple_staticobstacle(self):
        print "C"
        b1 = Obstacle(403927.42-self.environment.xTranslate,3778490.8599777-self.environment.yTranslate,1)
        self.environment.addObject(b1)
        b2 = Obstacle(403927.420038-self.environment.xTranslate,3778488.0699768-self.environment.yTranslate,1)
        self.environment.addObject(b2)

    def simple_roadblock(self):
        print "D"
        b1 = Obstacle(403875.9274-self.environment.xTranslate,3778567.5125-self.environment.yTranslate,20,3,-.35)
        self.environment.addObject(b1)
        b2 = Obstacle(403983.9975-self.environment.xTranslate,3778527.3875-self.environment.yTranslate,20,3,-.35)
        self.environment.addObject(b2)

    def simple_circleblock(self):
        print "E"
        print "past Alice"
        b1 = Obstacle(403981.019985-self.environment.xTranslate,3778500.21999-self.environment.yTranslate,2)
        self.environment.addObject(b1)
        print "past first object"
        b2 = Obstacle(403979.89999-self.environment.xTranslate,3778504.97999-self.environment.yTranslate,2)
        self.environment.addObject(b2)
        b3 = Obstacle(403980.179985-self.environment.xTranslate,3778510.2999-self.environment.yTranslate,2)
        self.environment.addObject(b3)
        b4 = Obstacle(403983.539986-self.environment.xTranslate,3778521.21999-self.environment.yTranslate,2)
        self.environment.addObject(b4)
        b5 = Obstacle(403985.779987-self.environment.xTranslate,3778523.21999-self.environment.yTranslate,2)
        self.environment.addObject(b5)
        b6 = Obstacle(403989.419987-self.environment.xTranslate,3778525.13999-self.environment.yTranslate,2)
        self.environment.addObject(b6)
        print "finished placing blocks"

#Add obstacle somewhere between 1.1.5 and 1.1.6 that blocks both lanes
    def stluke_singleroad_roadblock(self):
        barricade1 = Obstacle(398929.40206307068-self.environment.xTranslate,
3781259.1864437452-self.environment.yTranslate, 25, 4, 3.14159/2)
        self.environment.addObject(barricade1)

#Add obstacle a little behind 1.1.2 that blocks the lane so Alice can
#go around it (without changing lanes)
    def stluke_singleroad_partlaneblock(self):
        partBlock1 = Obstacle(399015.33643501892-self.environment.xTranslate, 3781266.3486046186-self.environment.yTranslate, 1.5)
        self.environment.addObject(partBlock1)

#Add obstacle a little behind 1.1.2 that blocks the lane so Alice can go around
#it (change lanes).
    def stluke_singleroad_laneblock(self):
        block1 = Obstacle(399017.00831001892-self.environment.xTranslate, 3781263.4837402692-self.environment.yTranslate, 1.5)
        self.environment.addObject(block1)

#Add obstacle midway somewhere between 4.1.6 and 4.1.7 that partially blocks
#the lane so Alice can go around it (shouldn't change lanes).
    def stluke_small_partlaneblock(self):
        partBlock1 = Obstacle(399009.48487251892-self.environment.xTranslate, 3781326.8688721051-self.environment.yTranslate, 1)
        partBlock2 = Obstacle(399019.51612251892-self.environment.xTranslate, 3781322.929686009-self.environment.yTranslate, 1)
        self.environment.addObject(partBlock1)
        self.environment.addObject(partBlock2)

#Add an obstacle somewhere between 2.2.8 and 2.2.9 (closer to 2.2.9) that
#block the whole road
    def santaanita_sitevisit_roadblock(self):
        barricade1 = Obstacle(403886.66986496828-self.environment.xTranslate, 3778504.2238156628-self.environment.yTranslate, 18, 2, 3.5/3)
        self.environment.addObject(barricade1)

#Add an obstacle at the same place that only partially blocks the lane so Alice
#can go past it (while staying in the lane)
    def santaanita_sitevisit_passblock(self):
        block1 = Obstacle(403885.37245591066-self.environment.xTranslate, 3778501.4632603955-self.environment.yTranslate, 4, 4, 3.5/3)
        self.environment.addObject(block1)

#Add an obstacle at the same place that blocks the drive lane so Alice can go
#around it (changing lanes)
    def santaanita_sitevisit_block(self):
        block1 = Obstacle(403887.53480179689-self.environment.xTranslate, 3778507.4444649052-self.environment.yTranslate, 4, 4, 3.5/3)
        self.environment.addObject(block1)

#Add an obstacle somewhere between 2.2.10 and 2.2.11 that only partially blocks
#the lane (no lane changing should be required).
    def santaanita_sitevisit_partlaneblock(self):
        partBlock1 = Obstacle(403863.75407059572-self.environment.xTranslate, 3778516.4020390343-self.environment.yTranslate, 1.5)
        partBlock2 = Obstacle(403866.09334214113-self.environment.xTranslate, 3778524.9194295239-self.environment.yTranslate, 1.5)
        self.environment.addObject(partBlock1)
        self.environment.addObject(partBlock2)

    def santaanita_sitevisit_sparseroadblock(self):
        self.environment.addObject(Obstacle(-58.646942138671875, -17.636882781982422, 2))
        self.environment.addObject(Obstacle(-57.014129638671875, -14.333579063415527, 2))
        self.environment.addObject(Obstacle(-55.381317138671875, -10.149394989013672, 2))
        self.environment.addObject(Obstacle(-53.748504638671875, -6.0753211975097656, 2))
        self.environment.addObject(Obstacle(-51.771942138671875, -2.2214672565460205, 2))
        self.environment.addObject(Obstacle(-49.967254638671875, 1.9627169370651245, 2))

    def santaanita_sitevisit_turnroadblock(self):
        self.environment.addObject(Obstacle(-77.0,1.0, 30, 3, 0.78500000000000003))


    def santaanita_sitevisit_simpleintersection(self):
	i = self.editor.numCars
	if i == 0:
	    self.editor.clickedCars = [Car(1)]
	else:
	    self.editor.clickedCars[i-1:i] = [self.self.environment.clickedCars[i-1],Car(1)]
	self.editor.clickedCars[i].setPosition(-50,-10)
	self.editor.clickedCars[i].setOrientation(0)
	self.environment.addObject(self.editor.clickedCars[i],'Car')
	self.editor.numCars += 1



    def santaanita_sitevisit_bigIntersectionBlock(self):
        self.environment.addObject(Obstacle(22.0, -38.5, 10))

    def santaanita_sitevisit_fakeIntersectionOrdering(self):
        self.environment.addObject(Obstacle(33.375, -39.5, 2))
        self.environment.addObject(Obstacle(22.625, -28.0, 2))

    def santaanita_sitevisit_directedDriving(self):
        self.environment.addObject(Obstacle(-18.958530426025391, -16.476669311523438, 0.25))
        self.environment.addObject(Obstacle(-20.528842926025391, -16.744937896728516, 0.25))
        self.environment.addObject(Obstacle(-22.099155426025391, -17.013206481933594, 0.25))
        self.environment.addObject(Obstacle(-23.669467926025391, -17.281473159790039, 0.25))
        self.environment.addObject(Obstacle(-25.763217926025391, -17.549741744995117, 0.25))
        self.environment.addObject(Obstacle(-27.856967926025391, -17.683876037597656, 0.25))
        self.environment.addObject(Obstacle(-29.950717926025391, -17.415607452392578, 0.25))
        self.environment.addObject(Obstacle(-32.253841400146484, -16.744937896728516, 0.25))
        self.environment.addObject(Obstacle(-33.928840637207031, -16.208400726318359, 0.25))
        self.environment.addObject(Obstacle(-35.394466400146484, -15.53773021697998, 0.25))
        self.environment.addObject(Obstacle(-36.860092163085938, -15.269461631774902, 0.25))
        self.environment.addObject(Obstacle(-38.011653900146484, -14.867059707641602, 0.25))
        self.environment.addObject(Obstacle(-39.267906188964844, -14.330523490905762, 0.25))
        self.environment.addObject(Obstacle(-40.524154663085938, -14.062254905700684, 0.25))
        self.environment.addObject(Obstacle(-41.780403137207031, -13.525718688964844, 0.25))
        self.environment.addObject(Obstacle(-43.455406188964844, -12.989181518554688, 0.25))
        self.environment.addObject(Obstacle(-45.130405426025391, -12.18437671661377, 0.25))
        self.environment.addObject(Obstacle(-47.119468688964844, -11.64784049987793, 0.25))
        self.environment.addObject(Obstacle(-48.480403900146484, -10.977169990539551, 0.25))
        self.environment.addObject(Obstacle(-50.050716400146484, -10.440632820129395, 0.25))
        self.environment.addObject(Obstacle(-51.411655426025391, -8.8310232162475586, 0.25))
        self.environment.addObject(Obstacle(-50.783531188964844, -9.6358280181884766, 0.25))
        self.environment.addObject(Obstacle(-52.667903900146484, -7.2214140892028809, 0.25))
        self.environment.addObject(Obstacle(-52.144466400146484, -8.0262184143066406, 0.25))
        self.environment.addObject(Obstacle(-53.400718688964844, -6.2824749946594238, 0.25))
        self.environment.addObject(Obstacle(-54.238216400146484, -4.941133975982666, 0.25))
        self.environment.addObject(Obstacle(-54.866340637207031, -4.136329174041748, 0.25))
        self.environment.addObject(Obstacle(-55.389778137207031, -3.197390079498291, 0.25))
        self.environment.addObject(Obstacle(-59.054428100585938, -14.330514907836914, 0.25))
        self.environment.addObject(Obstacle(-59.68255615234375, -13.793978691101074, 0.25))
        self.environment.addObject(Obstacle(-60.938804626464844, -12.855039596557617, 0.25))
        self.environment.addObject(Obstacle(-61.985679626464844, -12.050234794616699, 0.25))
        self.environment.addObject(Obstacle(-62.718490600585938, -11.37956428527832, 0.25))
        self.environment.addObject(Obstacle(-63.555992126464844, -10.574759483337402, 0.25))
        self.environment.addObject(Obstacle(-64.39349365234375, -9.7699546813964844, 0.25))
        self.environment.addObject(Obstacle(-65.545051574707031, -8.8310155868530273, 0.25))
        self.environment.addObject(Obstacle(-66.382553100585938, -7.6238083839416504, 0.25))
        self.environment.addObject(Obstacle(-67.53411865234375, -6.6848692893981934, 0.25))
        self.environment.addObject(Obstacle(-68.58099365234375, -5.7459306716918945, 0.25))
        self.environment.addObject(Obstacle(-69.62786865234375, -4.6728572845458984, 0.25))
        self.environment.addObject(Obstacle(-70.465370178222656, -4.0021867752075195, 0.25))
        self.environment.addObject(Obstacle(-71.72161865234375, -3.1973819732666016, 0.25))
        self.environment.addObject(Obstacle(-72.663803100585938, -2.7949795722961426, 0.25))
        self.environment.addObject(Obstacle(-74.024742126464844, -1.7219064235687256, 0.25))
        self.environment.addObject(Obstacle(-75.280990600585938, -0.64883339405059814, 0.25))
        self.environment.addObject(Obstacle(-76.327865600585938, 0.55837380886077881, 0.25))
        self.environment.addObject(Obstacle(-77.060676574707031, 0.96077626943588257, 0.25))
        self.environment.addObject(Obstacle(-77.793495178222656, 1.4973127841949463, 0.25))
        self.environment.addObject(Obstacle(-78.735679626464844, 2.0338492393493652, 0.25))
        self.environment.addObject(Obstacle(-80.09661865234375, 2.5703859329223633, 0.25))
        self.environment.addObject(Obstacle(-81.352867126464844, 2.9727883338928223, 0.25))
        self.environment.addObject(Obstacle(-82.609115600585938, 3.2410564422607422, 0.25))
        self.environment.addObject(Obstacle(-83.76068115234375, 3.6434588432312012, 0.25))
        self.environment.addObject(Obstacle(-85.226303100585938, 4.1799955368041992, 0.25))
        self.environment.addObject(Obstacle(-86.37786865234375, 4.5823979377746582, 0.25))
        self.environment.addObject(Obstacle(-75.595115661621094, 9.9477739334106445, 0.25))
        self.environment.addObject(Obstacle(-76.851364135742188, 10.618444442749023, 0.25))
        self.environment.addObject(Obstacle(-78.316986083984375, 11.423249244689941, 0.25))
        self.environment.addObject(Obstacle(-79.468551635742188, 11.959785461425781, 0.25))
        self.environment.addObject(Obstacle(-80.620109558105469, 12.496322631835938, 0.25))
        self.environment.addObject(Obstacle(-81.562301635742188, 13.971797943115234, 0.25))
        self.environment.addObject(Obstacle(-81.876365661621094, 15.58140754699707, 0.25))
        self.environment.addObject(Obstacle(-82.085739135742188, 16.922748565673828, 0.25))
        self.environment.addObject(Obstacle(-82.085739135742188, 18.129955291748047, 0.25))
        self.environment.addObject(Obstacle(-81.666984558105469, 19.605430603027344, 0.25))
        self.environment.addObject(Obstacle(-81.143547058105469, 20.544370651245117, 0.25))
        self.environment.addObject(Obstacle(-80.410736083984375, 21.349174499511719, 0.25))
        self.environment.addObject(Obstacle(-79.363861083984375, 22.019845962524414, 0.25))
        self.environment.addObject(Obstacle(-78.002922058105469, 22.288114547729492, 0.25))
        self.environment.addObject(Obstacle(-76.641990661621094, 22.55638313293457, 0.25))
        self.environment.addObject(Obstacle(-75.281051635742188, 22.824649810791016, 0.25))
        self.environment.addObject(Obstacle(-74.234176635742188, 22.824649810791016, 0.25))
        self.environment.addObject(Obstacle(-73.082611083984375, 23.092918395996094, 0.25))

    def santaanita_sitevisit_laneSeparation(self):
        self.environment.addObject(Obstacle(-44.300083160400391,-13.22499942779541, 40, 0.29999999999999999, 2.8033333333333337))
