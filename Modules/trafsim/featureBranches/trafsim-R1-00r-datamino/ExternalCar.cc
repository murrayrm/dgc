#include "ExternalCar.hh"
#include <cmath>
#include <cassert>
#include <GL/gl.h>

namespace TrafSim
{

  ExternalCar::ExternalCar(int skynetKey): CSkynetContainer(SNastate, skynetKey), 
					   orientation(0),xTrans(0),yTrans(0),lane(0), laneT(-1.0f),laneGrid(0)
{

  // make the car yellow
  setColor(1.0f, 1.0f, 0.0f, 1.0f);

}

ExternalCar::~ExternalCar()
{
}

void ExternalCar::initialize(Environment& env, std::string gridName)
{
	if (gridName != "")
		laneGrid = (LaneGrid*)env.getObject(gridName);
	else
	{
		// just get an arbitrary lane grid from the environment (and fail if there isn't one)
		ObjectMap laneGrids = env.getObjectsByClassID("LaneGrid");
		assert(!laneGrids.empty());
		
		laneGrid = (LaneGrid*)laneGrids.begin()->second;
	}		
	
	updateLanePos();
}

void ExternalCar::draw()
{
	// draw a colored rectangle
	
    glPushMatrix();
    glTranslatef(position.x, position.y, 0.0f);
    glRotatef(-atan2(velocity.x, velocity.y) / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
    glSetColor(props.color);
    
    glBegin(GL_POLYGON);
    glVertex3f(-props.size.x * 0.5f, -props.size.y * 0.5f, 0.0f);
    glVertex3f(-props.size.x * 0.5f,  props.size.y * 0.5f, 0.0f);
    glVertex3f( props.size.x * 0.5f,  props.size.y * 0.5f, 0.0f);
    glVertex3f( props.size.x * 0.5f, -props.size.y * 0.5f,  0.0f);
    glEnd();
    glPopMatrix();       
}

bool ExternalCar::simulate()
{

	
  // grab state from asim
  // these variables are defined in skynettalker/StateClient.hh
  UpdateState();

  //Coordinates Flipped????
  velocity.x = m_state.utmNorthVel;
  velocity.y = m_state.utmEastVel;
  position.x = m_state.utmNorthing;
  position.y = m_state.utmEasting;
  orientation = m_state.utmYaw;

  // shift the coordinates if necessary
  position.x -= xTrans;
  position.y -= yTrans;

   // std::cout<<"vel = ( "<<velocity.x<<" , "<<velocity.y<<" )"<<endl;
  //    std::cout<<"pos = ( "<<position.x<<" , "<<position.y<<" )"<<endl;

  updateLanePos();	
  return true;
}

bool ExternalCar::simulate(float ticks)
{
  /*
  // apply kinematics and recalculate lane position
  
  velocity += accel * ticks;
  position += velocity * ticks;

  updateLanePos();	
  return true;
  */

  simulate();

  return true;
}

float ExternalCar::getLaneVelocity()
{
	// velocity "along" a particular lane is the actual velocity dot-producted with the 
	// tangent of the lane at the current location.
	Vector tan = lane->spline->evalNormal(laneT).perpendicular();
	return Vector::dot(velocity, tan);
}

void ExternalCar::updateLanePos()
{
	if (lane)
		lane->removeObject(this);
	laneT = laneGrid->getLanePosition(position, &lane);
	
	if (laneT < 0.0f)
	{
		laneT = 2.0f;  // 2.0 if we're not on any lane at the moment
		lane = 0;
	}
	else
		lane->addObject(this, laneT);
}

void ExternalCar::setWorldPosition(Vector position)
{
	this->position = position;
	updateLanePos();
}

Vector ExternalCar::getWorldPosition() const
{
	return this->position;
}

void ExternalCar::setVelocity(Vector velVector)
{
	this->velocity = velVector;
}

Vector ExternalCar::getVelocity() const
{
	return this->velocity;
}

void ExternalCar::setAcceleration(Vector accelVector)
{
	this->accel = accelVector;
}

Vector ExternalCar::getAcceleration() const
{
	return this->accel;
}

float ExternalCar::getLaneTCoord()
{
	if (laneT < 0.0f)
		updateLanePos();
		
	return this->laneT;
}

Lane* ExternalCar::getCurrentLane()
{
	if (laneT < 0.0f)
		updateLanePos();
		
	return this->lane;
}

float ExternalCar::getVelocity()
{
  // take norm
  return sqrt(velocity.x*velocity.x + velocity.y*velocity.y);
}

std::vector<vector<point2> > ExternalCar::getBoundary()
{
  // return the four corners of the car
 
  std::vector<point2> boundary;
  double ly = props.size.y;
  double lx = props.size.x;
  double x = position.x;
  double y = position.y;
  double t = getOrientation();

  // put in the points at origin
  boundary.push_back(point2(.5*lx,.5*ly));
  boundary.push_back(point2(-.5*lx,.5*ly));
  boundary.push_back(point2(-.5*lx,-.5*ly));
  boundary.push_back(point2(.5*lx,-.5*ly));
  
  double oldY, oldX;
  // rotate points and translate
  for (std::vector<point2>::iterator iter = boundary.begin();
       iter != boundary.end(); iter++) {
    oldX = iter->x;
    oldY = iter->y;
    iter->x = oldX*cos(t) - oldY*sin(t) +x;
    iter->y = oldX*sin(t) + oldY*cos(t) +y;
  }  
  
  vector<vector<point2> > boundaryWrapper;
  boundaryWrapper.push_back(boundary);
  return boundaryWrapper;

}

ExternalCar* ExternalCar::downcast(Object* source)
{
	return dynamic_cast<ExternalCar*>(source);	
}

void ExternalCar::serialize(std::ostream& os) const
{
	// NOT IMPLEMENTED
}

void ExternalCar::deserialize(std::istream& is)
{	
	// NOT IMPLEMENTED
}


}
