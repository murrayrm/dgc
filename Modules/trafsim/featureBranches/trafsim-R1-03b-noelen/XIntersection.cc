#include "XIntersection.hh"
#include <cassert>
#include <cmath>
#include <algorithm>
#include <GL/gl.h>

namespace TrafSim
{
	
  const unsigned int NUM_ROADS = 4;

  XIntersection::XIntersection()
  {
    params.narrowing = 0.1f;
    params.sharpness = 0.5f;
    params.leftTightening = 1.3f;
	
    for (int i = 0; i < 4; ++i)
      {
	roads[i] = 0;
	maneuverData[i] = 0;
	laneData[i] = 0;
	stops[i] = true;
      }
	
    fourWayStop = true;
    lastCar = 0;
    computed = false;

    currentQueue.numCars = 0;
    currentQueue.carsExited = 0;

  }

  XIntersection::~XIntersection()
  {
    for (int i = 0; i < 4; ++i)
      {
	if (maneuverData[i])
	  delete maneuverData[i];
	if (laneData[i])
	  delete laneData[i];
      }	
	
    for (std::map<Lane*, Lane*>::iterator i = multiLanes.begin();
	 i != multiLanes.end(); ++i)
      delete i->second;
  }

  void XIntersection::draw()
  {
    if (!computed)
      return;
		
    for (int i = 0; i < 4; ++i)
      {
	boundaries[i].draw();
	if (stops[i])
	  {
	    Vector v0 = boundaries[i].evalCoordinate(0.0f);
	    Vector v1 = boundaries[(i + 3)%4].evalCoordinate(1.0f);
			
	    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
	    glBegin(GL_LINE_STRIP);
	    glVertex3f(v0.x, v0.y, 0.0f);
	    glVertex3f(v1.x, v1.y, 0.0f);
	    glEnd();
	  }	
      }
  }
	
  int XIntersection::hook(Road& road, bool reversed)
  {
    for (int i = 0; i < 4; ++i)
      if (!roads[i])
	{
	  roads[i] = &road;
	  unsigned int nLanes = reversed? road.numForwardLanes(): road.numReverseLanes();
			
	  maneuverData[i] = new std::vector<ManeuverDestMap>(nLanes);
	  for (unsigned int j = 0; j < nLanes; ++j)
	    for (int k = 0; k < 3; ++k)
	      (*maneuverData[i])[j].data[k] = std::make_pair( (Road*)0, 0 );
	
	  laneData[i] = new std::vector<ManeuverLaneMap>(nLanes);
	  return i;
	}
	
    return -1;
  }
	
  bool XIntersection::hookStart(Road& road)
  {
    if (computed)
      return false;
	
    int i = hook(road, true);
    if (i == -1)
      return false;
	
    reversed[i] = true;
    road.setStart(this);
	
    return true;
  }

  bool XIntersection::hookEnd(Road& road)
  {
    if (computed)
      return false;
	
    int i = hook(road, false);
    if (i == -1)
      return false;
	
    reversed[i] = false;
    road.setEnd(this);
	
    return true;
  }	

  bool XIntersection::blockManeuver(Road const& startRoad, 
				    unsigned int startLane, CrossManeuver maneuver)
  {
    if (computed)
      return false;
		
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &startRoad)
	{
	  (*maneuverData[i])[startLane].data[maneuver] = std::make_pair( (Road*)0, -1 );
	  return true;
	}
		
    return false;
  }

  bool XIntersection::unblockManeuver(Road const& startRoad, 
				      unsigned int startLane, CrossManeuver maneuver)
  {
    if (computed)
      return false;
		
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &startRoad)
	{
	  (*maneuverData[i])[startLane].data[maneuver] = std::make_pair( (Road*)0, 0 );
	  return true;
	}
		
    return false;	
  }	

  bool XIntersection::setStop(Road const& road, bool stopSign)
  {
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &road)
	{
	  if (stopSign)
	    {
	      if (!stops[i]) fourWayStop = true;
	    }
	  else
	    {
	      if (stops[i] && !fourWayStop) return false;
	      fourWayStop = false;
	    }
			
	  stops[i] = stopSign;
	  stops[ (i + 2) % 4 ] = stopSign;
	}
	
    return true;
  }

  bool XIntersection::isStop(Road const& road)
  {
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &road)
	return stops[i];
	
    assert(false);
    return false;
  }

  RightOfWay XIntersection::rightOfWay(Car* thisCar, Road const& road, Car** followCar)
  {	
    *followCar = lastCar;
    lastCar = thisCar;
		
    if (fourWayStop)
      return ROW_GO;
    else
      return ROW_GAP;		
  }

  void XIntersection::enterQueue()
  {
    currentQueue.numCars++;
    cout<<"Added 1 car to queue. Total in queue: "<<currentQueue.numCars<<endl;
  }

  void XIntersection::exitIntersection()
  {
    currentQueue.numCars--;
    currentQueue.carsExited++;
    cout<<"Removing 1 car from queue. Total removed: "<<currentQueue.carsExited<<endl;
  }

  void XIntersection::exitIntersection(Car* thisCar)
  {
    if (lastCar == thisCar)
      lastCar = 0;
  }

  void XIntersection::getOpposingTraffic(Road const& startRoad, unsigned int startLane, 
					 CrossManeuver maneuver, std::vector<Lane*>& result)
  {	
    Lane* myLane = getLane(startRoad, startLane, maneuver);
    unsigned int roadIndex;
	
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &startRoad)
	roadIndex = i;
    assert(roadIndex < 4);
	
    for (unsigned int offset = 1; offset < 4; offset += 2)
      {
	int i = (roadIndex + offset) % 4;
		
	for (unsigned int j = 0; j < laneData[i]->size(); ++j)
	  for (unsigned int k = 0; k < 3; ++k)
	    {
	      Lane* testLane = (*laneData[i])[j].data[k];
	      if (testLane && testLane->interferesWith(myLane))
		result.push_back(multiLanes[testLane]);
	    }
      }
  }

  void XIntersection::calculateCurve(Vector const& pt1, Vector const& tan1, 
				     Vector const& pt2, Vector const& tan2,
				     float sharpness, float narrowing, Spline& result)
  {
    Vector intersectionPt;
    Vector::intersection(pt1, pt1 + tan1 * 100000.0f,
			 pt2, pt2 + tan2 * 100000.0f,
			 intersectionPt);
	
    float dist1 = (intersectionPt - pt1).length();
    float dist2 = (intersectionPt - pt2).length();
	
    std::vector<Vector> points;
	
    points.push_back( pt1 - tan1 * dist1 );
    points.push_back( pt1 );
	
    if (dist1 >= dist2 * 0.1f)
      points.push_back( pt1 + tan1 * dist1 * sharpness );
	
    if (fabs( Vector::dot(tan1, tan2) ) <= 0.9f)
      points.push_back( intersectionPt - (tan1 + tan2) * (dist1 + dist2) * narrowing );
	
    if (dist2 >= dist1 * 0.1f)
      points.push_back( pt2 + tan2 * dist2 * sharpness );
	
    points.push_back( pt2 );
    points.push_back( pt2 - tan2 * dist2 );
	
    result.initialize(points);
  }

  template<typename V, typename T>
  void sortByKey(T& values, unsigned int keys[], const unsigned int n)
  {
    V sortedValues[n];
    for (unsigned int i = 0; i < n; ++i)
      sortedValues[i] = values[ keys[i] ];
    for (unsigned int i = 0; i < n; ++i)
      values[i] = sortedValues[i];
  }

  template <typename T>
  unsigned int indexOf(T value, T array[], const unsigned int n)
  {
    for (unsigned int i = 0; i < n; ++i)
      if (array[i] == value)
	return i;
    return (unsigned int)-1;
  }

  void XIntersection::sortRoads()
  {
    float angles[NUM_ROADS];
    float sortedAngles[NUM_ROADS];
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      {
	Vector v = !reversed[i] ? roads[i]->getSpline().getCoordinateData()[0] 
	  -roads[i]->getSpline().getCoordinateData()[1]
	  : roads[i]->getSpline().getCoordinateData()[roads[i]->getSpline().getResolution()]
	  -roads[i]->getSpline().getCoordinateData()[roads[i]->getSpline().getResolution() - 1];
	sortedAngles[i] = angles[i] = atan2(v.y, v.x);
      } 
    std::sort(sortedAngles, sortedAngles + NUM_ROADS);
	
    unsigned int keys[NUM_ROADS];
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      keys[i] = indexOf(sortedAngles[i], angles, NUM_ROADS);
	
    sortByKey<Road*>(roads, keys, NUM_ROADS);
    sortByKey<std::vector<ManeuverDestMap>*>(maneuverData, keys, NUM_ROADS);
    sortByKey<std::vector<ManeuverLaneMap>*>(laneData, keys, NUM_ROADS);
    sortByKey<bool>(reversed, keys, NUM_ROADS);
    sortByKey<bool>(stops, keys, NUM_ROADS);
  }

  void XIntersection::computeBoundaries()
  {
    center = Vector(0.0f, 0.0f);
    float numTerms = 0.0f;
	
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      {
	Spline& target = boundaries[i];
		
	Road* road_1 = roads[i];
	float end_1 = reversed[i] ? 1.0f : 0.0f;
	bool rev_1 = reversed[i];
		
	Vector tan_1 = road_1->getSpline().evalNormal(end_1).perpendicular();
	if (!rev_1)
	  tan_1 *= -1.0f;
		
	Road* road_2 = roads[(i + 1) % NUM_ROADS];
	float end_2 = reversed[(i + 1) % NUM_ROADS] ? 1.0f : 0.0f;
	bool rev_2 = reversed[(i + 1) % NUM_ROADS];
		
	Vector tan_2 = road_2->getSpline().evalNormal(end_2).perpendicular();
	if (!rev_2)
	  tan_2 *= -1.0f;
		
	Vector pt_1 = road_1->getBoundary( end_1, -1, rev_1 );
	Vector pt_2 = road_2->getBoundary( end_2,  1, rev_2 );
		
	if (Vector::dot(tan_1, tan_2) > -0.9f)
	  {
	    calculateCurve( pt_1, tan_1, pt_2, tan_2, 
			    params.sharpness, params.narrowing,
			    target );
			
	    Vector intersectionPt;
	    Vector::intersection(pt_1, pt_1 + tan_1 * 10000.0f,
				 pt_2, pt_2 + tan_2 * 10000.0f,
				 intersectionPt);
	    center += intersectionPt;
	    numTerms += 1.0f;
	  }
	else
	  {
	    std::vector<Vector> points;
	    points.push_back(pt_1 - tan_1);
	    points.push_back(pt_1);
	    points.push_back(pt_2);
	    points.push_back(pt_2 + tan_2);
	    target.initialize(points);
	  }
      }
	
    center /= numTerms;
  }

  void XIntersection::computeLanes(Environment& env)
  {
    for (int n_1 = 0; n_1 < 4; ++n_1)
      {
	unsigned int nLanes_1 = reversed[n_1] ?
	  roads[n_1]->numForwardLanes() :
	  roads[n_1]->numReverseLanes();
	Lane* (Road::*getLane_1)(unsigned int) = reversed[n_1] ?
	  &Road::getForwardLane :
	  &Road::getReverseLane;
		
	for (int offset = 1; offset < 4; ++offset)
	  {
	    int n_2 = (n_1 + offset) % 4;
			
	    unsigned int nLanes_2 = !reversed[n_2] ?
	      roads[n_2]->numForwardLanes() :
	      roads[n_2]->numReverseLanes();
	    Lane* (Road::*getLane_2)(unsigned int) = !reversed[n_2] ?
	      &Road::getForwardLane :
	      &Road::getReverseLane;
			
	    for (unsigned int lane_1 = 0; lane_1 < nLanes_1; ++lane_1)
	      {
		unsigned int maneuver, lane_2;
		switch (offset)
		  {
		  case 1: 
		    maneuver = CROSS_RIGHT;
		    lane_2 = nLanes_2 - 1; // rightmost lane
		    break;
		  case 2:
		    maneuver = CROSS_STRAIGHT;
		    lane_2 = lane_1; // same lane
		    break;
		  case 3:
		    maneuver = CROSS_LEFT;
		    lane_2 = 0;	// leftmost lane
		  }
	
		if (lane_2 >= nLanes_2 || lane_2 < 0)
		  continue;
					
		if ( (*maneuverData[n_1])[lane_1].data[maneuver].second == (unsigned int)-1 )
		  continue;
				
		Vector tan_1 = (roads[n_1]->*getLane_1)(lane_1)->spline->evalNormal(1.0f).perpendicular();
		Vector tan_2 = -(roads[n_2]->*getLane_2)(lane_2)->spline->evalNormal(0.0f).perpendicular();
				
		Vector pt_1  = (roads[n_1]->*getLane_1)(lane_1)->spline->evalCoordinate(1.0f);
		Vector pt_2  = (roads[n_2]->*getLane_2)(lane_2)->spline->evalCoordinate(0.0f);
				
		Spline spline;
		if (maneuver == CROSS_STRAIGHT)
		  {
		    float dist_1 = (center - pt_1).length();
		    float dist_2 = (center - pt_2).length();
		    Vector curveCenter = center + tan_2.perpendicular() * roads[n_2]->getLaneWidth() * 0.5f;
					
		    std::vector<Vector> points;		
		    points.push_back( pt_1 - tan_1 * dist_1 );
		    points.push_back( pt_1 );
		    points.push_back( curveCenter );
		    points.push_back( pt_2 );
		    points.push_back( pt_2 - tan_2 * dist_2 );
		    spline.initialize(points);
		  }
		else if (maneuver == CROSS_LEFT)
		  {
		    calculateCurve( pt_1, tan_1, pt_2, tan_2,
				    params.sharpness / params.leftTightening,
				    params.narrowing / params.leftTightening,
				    spline );
		  }
		else // maneuver == CROSS_RIGHT
		  {
		    calculateCurve( pt_1, tan_1, pt_2, tan_2,
				    params.sharpness, params.narrowing, spline );	
					
		  }
				
		Lane* newLane = new Lane(spline);
		// don't add intersection lane boundaries for export
		env.addObject(newLane,"",false);
		newLane->inIntersection = true;
		newLane->setParent(this->getName());
		newLane->spline->cacheData();
				
		std::vector<LaneSegment> lsa;
		lsa.push_back( LaneSegment(  (roads[n_1]->*getLane_1)(lane_1), 0.0f, 1.0f ) );
		lsa.push_back( LaneSegment(  newLane, 0.0f, 1.0f ) );
		lsa.push_back( LaneSegment(  (roads[n_2]->*getLane_2)(lane_2), 0.0f, 1.0f ) );
		multiLanes[newLane] = new MultiLane(lsa);
				
		if (!reversed[n_2])
		  lane_2 += roads[n_2]->numReverseLanes();
					
		(*maneuverData[n_1])[lane_1].data[maneuver] = std::make_pair(roads[n_2], lane_2);
		(*laneData[n_1])[lane_1].data[maneuver] = newLane;
						
	      } // end for starting lane
	  } // end for target road
      } // end for starting road
  }

  void XIntersection::computeInterference()
  {
    std::vector<Lane*> lanes;
    for (unsigned int i = 0; i < 4; ++i)
      for (unsigned int j = 0; j < laneData[i]->size(); ++j)
	for (unsigned int k = 0; k < 3; ++k)
	  if ( (*maneuverData[i])[j].data[k].first != 0 )
	    lanes.push_back( (*laneData[i])[j].data[k] );
					
    for (unsigned int i = 0; i < lanes.size(); ++i)
      for (unsigned int j = 0; j < lanes.size(); ++j)
	lanes[i]->addInterference(lanes[j], 3.0f);
  }

  bool XIntersection::computeGeometry(Environment& env)
  {
    if (computed)
      return false;
		
    for (int i = 0; i < 4; ++i)
      if (!roads[i])
	return false;

    multiLanes.clear();
				
    sortRoads();
    computeBoundaries();
    computeLanes(env);	
    computeInterference();
		
    computed = true;
    return true;
  }

  RoadLane XIntersection::getDestination(Road const& startRoad, 
					 unsigned int startLane, CrossManeuver maneuver) const
  {
    if (!startRoad.isReverse(startLane))
      startLane -= startRoad.numReverseLanes();
		
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &startRoad)
	return (*maneuverData[i])[startLane].data[maneuver];
			
    assert(false);
    return std::make_pair( (Road*)0, 0 );
  }

  Lane* XIntersection::getLane(Road const& startRoad, 
			       unsigned int startLane, CrossManeuver maneuver)
  {
    if (!startRoad.isReverse(startLane))
      startLane -= startRoad.numReverseLanes();
	
    for (int i = 0; i < 4; ++i)
      if (roads[i] == &startRoad)
	return (*laneData[i])[startLane].data[maneuver];

    assert(false);	
    return 0;
  }

  void XIntersection::getBoundaries(std::vector<Spline*>& boundsArray)
  {
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      boundsArray.push_back(&boundaries[i]);
  }

  std::vector<std::vector<point2> > XIntersection::getBoundary()
  {
    // the geometry should already have been computed
    assert(computed);

    // take the four splines forming the boundary, and grab points 
    // from each
    vector<point2> boundary = boundaries[0].getBoundary().front();

    // store the other boundaries
    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    for (unsigned int i = 1; i < NUM_ROADS; i++) {
      vector<point2> boundary2 = boundaries[i].getBoundary().front();
      boundaryWrapper.push_back(boundary2);
    }

    return boundaryWrapper;
  
  }

  XIntersection* XIntersection::downcast(Object* source)
  {
    return dynamic_cast<XIntersection*>(source);
  }

  void XIntersection::serialize(std::ostream& os) const
  {
    std::vector<std::vector<std::pair<std::string, unsigned int> > > strManeuverData[NUM_ROADS];
    std::vector<std::vector<std::string> > strLaneData[NUM_ROADS];
	
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      {
	os << "data " << i << ": " << std::endl;
	os << "boundaries --> " << (Object*)&boundaries[i] << std::endl;
	os << "roads --> " << roads[i]->getName() << std::endl;
	os << "reversed --> " << reversed[i] << std::endl;
	os << "stops --> " << stops[i] << std::endl;
	
	for (unsigned int j = 0; j < laneData[i]->size(); ++j)
	  {
	    strManeuverData[i].push_back( std::vector<std::pair<std::string, unsigned int> >() );
	    strLaneData[i].push_back( std::vector<std::string>() );
			
	    for (unsigned int k = 0; k < 3; ++k)
	      {
		RoadLane roadLane = (*maneuverData[i])[j].data[k];
		Lane* lane = (*laneData[i])[j].data[k];
				
		strManeuverData[i].back().push_back(
						    std::make_pair( roadLane.first? roadLane.first->getName(): "None",
								    roadLane.second ) );			
		strLaneData[i].back().push_back( lane? lane->getName(): "None" );
	      }
	  }
		
	os << "maneuverData --> " << strManeuverData[i] << std::endl;
	os << "laneData --> " << strLaneData[i] << std::endl;
      }
	
    os << "center --> " << center << std::endl;
    os << "fourWayStop --> " << fourWayStop << std::endl;
	
    std::map<std::string, Lane*> namedMap;
    for (std::map<Lane*, Lane*>::const_iterator i = multiLanes.begin();
	 i != multiLanes.end(); ++i)
      namedMap[i->first->getName()] = i->second;
	
    os << "multiLanes --> " << namedMap << std::endl;
  }

  void XIntersection::deserialize(std::istream& is)
  {
    std::string roadNames[NUM_ROADS];
    std::vector<std::vector<std::pair<std::string, unsigned int> > > strManeuverData[NUM_ROADS];
    std::vector<std::vector<std::string> > strLaneData[NUM_ROADS];
	
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      {
	std::ostringstream oss;
	oss << "data " << i << ":";
	matchString(is, oss.str());
		
	Object* splineObj;
	DESERIALIZE_RN(is, "boundaries", splineObj);
	boundaries[i] = *(Spline*)splineObj;
	// BUG: there's a small memory leak here due to us being unable
	// to safely delete splineObj, but whatever. It's not very important.
		
	DESERIALIZE_RN(is, "roads", roadNames[i]);
	DESERIALIZE_RN(is, "reversed", reversed[i]);
	DESERIALIZE_RN(is, "stops", stops[i]);
	DESERIALIZE_RN(is, "maneuverData", strManeuverData[i]);
	DESERIALIZE_RN(is, "laneData", strLaneData[i]);
      }
	
    for (unsigned int i = 0; i < NUM_ROADS; ++i)
      {
	this->_addReference(roadNames[i], (Object**)&roads[i]);
		
	if (maneuverData[i]) delete maneuverData[i];
	if (laneData[i]) delete laneData[i];
	maneuverData[i] = new std::vector<ManeuverDestMap>(strManeuverData[i].size());
	laneData[i] = new std::vector<ManeuverLaneMap>(strLaneData[i].size());
		
	for (unsigned int j = 0; j < laneData[i]->size(); ++j)
	  for (unsigned int k = 0; k < 3; ++k)
	    {
	      std::pair<std::string, unsigned int> strM = strManeuverData[i][j][k];
	      std::string strL = strLaneData[i][j][k];
				
	      (*maneuverData[i])[j].data[k] = std::make_pair((Road*)0, strM.second);
	      (*laneData[i])[j].data[k] = (Lane*)0;
				
	      if (strM.first != "None")
		this->_addReference(strM.first, (Object**)&((*maneuverData[i])[j].data[k].first));
	      if (strL != "None")
		this->_addReference(strL, (Object**)&((*laneData[i])[j].data[k]));
	    }
      }
	
    DESERIALIZE(is, center);
    DESERIALIZE(is, fourWayStop);
    DESERIALIZE_RN(is, "multiLanes", namedMap);
	
    lastCar = 0;
    computed = true;
  }

  void XIntersection::_postResolutionCallback(Environment& env)
  {
    for (std::map<Lane*, Lane*>::iterator i = multiLanes.begin();
	 i != multiLanes.end(); ++i)
      delete i->second;
    multiLanes.clear();
	
    for (std::map<std::string, Object*>::iterator i = namedMap.begin();
	 i != namedMap.end(); ++i)
      multiLanes[ (Lane*)env.getObject(i->first) ] = (Lane*)i->second;
  }

}


