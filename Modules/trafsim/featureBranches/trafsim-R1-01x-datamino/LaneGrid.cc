#include "LaneGrid.hh"
#include <map>
#include <GL/gl.h>

namespace TrafSim
{

LaneGrid::LaneGrid(): grid(0), nTilesX(0), nTilesY(0)
{
}

LaneGrid::~LaneGrid()
{
	if (grid)
		delete[] grid;
}

void LaneGrid::partition(Environment& env, float tileSize,
						 Vector lowerLeft, Vector topRight)
{
	nTilesX = (unsigned int) ((topRight.x - lowerLeft.x) / tileSize);
	nTilesY = (unsigned int) ((topRight.y - lowerLeft.y) / tileSize);
	this->lowerLeft = lowerLeft;
	this->topRight = topRight;
	
	if (grid) delete[] grid;
	grid = new std::vector<LaneSegment>[nTilesX * nTilesY];
	
	ObjectMap laneMap = env.getObjectsByClassID("Lane");
	for (ObjectMap::iterator i = laneMap.begin(); 
		 i != laneMap.end(); ++i)
	{
		mapLane((Lane*)i->second);	
	}
}

void LaneGrid::mapLane(Lane* lane)
{
	lane->spline->cacheData();
	const Vector* coordData = lane->spline->getCoordinateData();
	unsigned int nDataPoints = lane->spline->getResolution() + 1;

	float width  = topRight.x - lowerLeft.x;
	float height = topRight.y - lowerLeft.y; 
	
	for (unsigned int i = 0; i < nDataPoints; ++i)
	{
		int x = (int)((coordData[i].x - lowerLeft.x) / width * nTilesX);
		int y = (int)((coordData[i].y - lowerLeft.y) / height * nTilesY);
		
		if (x < 0 || x >= (int)nTilesX || y < 0 || y >= (int)nTilesY)
			continue;
		
		std::vector<LaneSegment>& tile = grid[y * nTilesX + x];
		std::vector<LaneSegment>::iterator j;
		for (j = tile.begin(); j != tile.end(); ++j)
			if (j->segment == lane) break;		
	
		if (j == tile.end())
			tile.push_back(LaneSegment(lane, (float)i, (float)(i + 1)));
		else
		{
			if (i < j->t_start) j->t_start = (float)i;
			if (i > j->t_end) j->t_end = (float)i;
		}
	}		
}

float LaneGrid::getLanePosition(Vector worldPos, Lane** laneOut)
{
	int _x = (int)((worldPos.x - lowerLeft.x) / (topRight.x - lowerLeft.x) * nTilesX);
	int _y = (int)((worldPos.y - lowerLeft.y) / (topRight.y - lowerLeft.y) * nTilesY);
		
	std::map<Lane*, Vector> laneMap;
	
	for (int offsetX = -1; offsetX <= 1; ++offsetX)
		for (int offsetY = -1; offsetY <= 1; ++offsetY)
		{
			int x = _x + offsetX, y = _y + offsetY;	
			if (x < 0 || x >= (int)nTilesX || y < 0 || y >= (int)nTilesY)
				continue;
				
			std::vector<LaneSegment>& tile = grid[y * nTilesX + x];
			for (std::vector<LaneSegment>::iterator j = tile.begin();
			 	 j != tile.end(); ++j)
			{				
				std::map<Lane*, Vector>::iterator i = laneMap.find(j->segment);
				if (i == laneMap.end())
					laneMap[j->segment] = Vector(j->t_start, j->t_end);
				else
				{
					if (j->t_start < i->second.x) i->second.x = j->t_start;
					if (j->t_end   > i->second.y) i->second.y = j->t_end;
				}
			}
		}
	
	Vector closest = Vector(-100000.0f, -100000.0f);
	float closestT = -1.0f;

	for (std::map<Lane*, Vector>::iterator i = laneMap.begin();
		 i != laneMap.end(); ++i)
	{
		float tryClosestT = i->first->spline->closestToPoint(
				worldPos, (unsigned int)(i->second.x), (unsigned int)(i->second.y));
		Vector tryClosest = i->first->spline->evalCoordinate(tryClosestT);
		
		if (Vector::dist2(tryClosest, worldPos) < Vector::dist2(closest, worldPos))
		{
			closest = tryClosest;
			closestT = tryClosestT;
			*laneOut = i->first;
		}
	}
	
	return closestT;
}

point2 LaneGrid::getCenter()
{
  return point2(lowerLeft.x + getWidth()/2,
		lowerLeft.y + getLength()/2);
}
 
float LaneGrid::getLength()
{
  return topRight.y - lowerLeft.y;
}
 
float LaneGrid::getWidth()
{
  return topRight.x - lowerLeft.x;
}

std::vector<std::vector<point2> > LaneGrid::getBoundary()
{
  std::vector<point2> boundary;
  boundary.push_back(point2(lowerLeft.x,lowerLeft.y));
  boundary.push_back(point2(topRight.x,lowerLeft.y));
  boundary.push_back(point2(topRight.x,topRight.y));
  boundary.push_back(point2(lowerLeft.x,topRight.y));

  vector<vector<point2> > boundaryWrapper;
  boundaryWrapper.push_back(boundary);
  return boundaryWrapper;
}

void LaneGrid::serialize(std::ostream& os) const
{
//	os << "grid --> " << std::make_pair(grid, (int)(nTilesX * nTilesY)) << std::endl;
//	os << "nTilesX --> " << nTilesX << std::endl;
//	os << "nTilesY --> " << nTilesY << std::endl;
//	os << "lowerLeft --> " << lowerLeft << std::endl;
//	os << "topRight --> " << topRight << std::endl;
}

void LaneGrid::deserialize(std::istream& is)
{
/*	if (grid)
		delete[] grid;
	
	DESERIALIZE_RN(is, "grid", &grid);
	DESERIALIZE(is, nTilesX);
	DESERIALIZE(is, nTilesY);
	DESERIALIZE(is, lowerLeft);
	DESERIALIZE(is, topRight);
	
	for (unsigned int j = 0; j < nTilesX * nTilesY; ++j)		
		for (std::vector<LaneSegment>::iterator i = grid[j].begin();
			 i != grid[j].end(); ++i)
		{
			this->_addReference(i->segmentName, (Object**)&(i->segment));
		}*/
}

void LaneGrid::draw()
{
  	
    glPushMatrix();
    glTranslatef(getCenter().x, getCenter().y, 0.0f);
    glRotatef(0.0f, 0.0f, 0.0f, 1.0f);
    glSetColor(Color(1.0f,0.0f,0.0f,1.0f));
    
    glBegin(GL_LINE_LOOP);
    glVertex3f(lowerLeft.x,lowerLeft.y,0.0f);
    glVertex3f(topRight.x,lowerLeft.y,0.0f);
    glVertex3f(topRight.x,topRight.y,0.0f);
    glVertex3f(lowerLeft.x,topRight.y,0.0f);    
    glEnd();
    
    glPopMatrix();
}

}
