#ifndef SERIALIZATION_H_
#define SERIALIZATION_H_

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <utility>
#include <iterator>
#include <tr1/type_traits>

namespace TrafSim
{
	
/**
 * Read in a class member in standard format. See any Object subclass for examples.
 */
#define DESERIALIZE(stream, field) ({ \
	matchString(stream, #field); \
	matchString(stream, "-->"); \
	stream >> field; 0; })
	
/**
 * Read in a class member in standard format. See any Object subclass for examples.
 * This version renames the field to the string "strrep".
 */
#define DESERIALIZE_RN(stream, strrep, field) ({ \
	matchString(stream, (strrep)); \
	matchString(stream, "-->"); \
	stream >> field; 0; })
	
/**
 * A trivial exception class for use with the serialization mechanism.
 */
class ParseError
{
public:
	
	/// Creates a new ParseError exception
	/// \param info Information about the cause of the error
	ParseError(std::string info);
	
	std::string info;  ///< Information about the cause of the error
};

/**
 * Ignoring leading whitespace, attempts to read a string from the stream
 * and compare it to the given string. If they do not match, a ParseError
 * exception is raised.
 * \param is The input stream to read from
 * \param str The string to compare to
 */
void matchString(std::istream& is, std::string const& str);

/**
 * Ignoring leading whitespace, reads in a string delimited by the character 
 * delim. Said character is put back into the stream.
 * \param is The input stream to read from
 * \param delim The character that terminates the desired string
 */
std::string readDelim(std::istream& is, char delim);

// the following short snippet is copied straight from boost::enable_if.
// I'm using this in my generic extraction operator to avoid the template
// parameter matching std::basic_string and causing all kinds of craziness

/* ***** BEGIN BOOST::ENABLE_IF ***** */
template <bool B, class T = void>
struct enable_if_c {
  typedef T type;
};

template <class T>
struct enable_if_c<false, T> {};

template <class Cond, class T = void>
struct enable_if : public enable_if_c<Cond::value, T> {};
/* ***** END BOOST::ENABLE_IF ***** */

// the following struct is a wonderful example of deep, black, template
// metaprogramming hackery. Depending on whether the typename T has
// a subtype called "iterator", but is NOT a std::basic_string, the 
// static parameter "value" is bound to true or false. In effect, 
// when used with enable_if, this can constrain an overloaded template
// function to match only STL containers, excluding std::basic_string;
// this is what I needed for operator>> to work.

template <typename T> struct non_string_iterable
{ 
	template <typename U>
	static long  test_iterator(U*, typename U::iterator* iter = 0);
	static short test_iterator( ... );
	
	static const bool value = ( sizeof(test_iterator((T*)0)) == sizeof(long) );
};

template <typename T> struct non_string_iterable< std::basic_string<T> >
{
	static const bool value = false;
};

// writes an std::pair to the given ostream
template< typename T1, typename T2 >
std::ostream& operator<<(std::ostream& os, std::pair<T1, T2> value)
{
	return os << value.first << " : " << value.second;
}

// to write arrays to an output stream, pass in a pair with their
// pointer representation combined with their length as an int
template< typename T1 >
std::ostream& operator<<(std::ostream& os, std::pair<T1*, int> array_desc)
{
	os << "collection(" << array_desc.second << ") {\n";
	
	for (int i = 0; i < array_desc.second; i++)
		os << array_desc.first[i] << " ,\n";
	
	return os << " }";
}

// writes any STL container to an output stream
template< typename Container >
typename enable_if< non_string_iterable<Container>, std::ostream >::type &
operator<<(std::ostream& os, Container const& container)
{	
	os << "collection(" << container.size() << ") {\n";
	
	for(typename Container::const_iterator i = container.begin();
		i != container.end(); ++i)
		os << (*i) << " ,\n";
		
	return os << " }";
}

// reads a std::pair from an input stream, discarding constness. This is
// an evil hack, but required to get maps to work with my operator>>
template< typename T1, typename T2 >
std::istream& operator>>(std::istream& is, std::pair<T1, T2>& result)
{
	try
	{
			
		typedef typename std::tr1::remove_const<T1>::type T1_nonconst;
		typedef typename std::tr1::remove_const<T2>::type T2_nonconst;
		 
		T1_nonconst first;
		is >> first;
		
		matchString(is, ":");
		
		T2_nonconst second;
		is >> second;
		
		const_cast<T1_nonconst&>(result.first) = first;
		const_cast<T2_nonconst&>(result.second) = second;
	
		return is;
	}
	catch (ParseError& e)
	{
		e.info = "Faild to parse ordered pair:\n" + e.info;
		throw;
	}
	catch (std::exception& e)
	{
		throw ParseError("Failed to parse ordered pair:\n" + std::string(e.what()) );
	}
}

// read in an array from the input stream. pass in an uninitialized pointer.
template< typename T >
std::istream& operator>>(std::istream& is, T** result)
{
	try
	{
		matchString(is, "collection");
		matchString(is, "(");
		
		unsigned int size;
		is >> size;
		
		matchString(is, ")");
		matchString(is, "{");
		
		(*result) = new T[size];
		
		for (unsigned int i = 0; i < size; i++)
		{
			is >> (*result)[i];
			matchString(is, ",");
		}
		
		matchString(is, "}");
		
		return is;
	}
	catch (ParseError& e)
	{
		e.info = "Faild to parse collection:\n" + e.info;
		throw;
	}
	catch (std::exception& e)
	{
		throw ParseError("Failed to parse collection:\n" + std::string(e.what()) );
	}
}

// reads any STL container from an input stream. I'm amazed I finally got this
// template monstrosity to work, and not try to match every last type in existance =P
template< typename Container >
typename enable_if< non_string_iterable<Container>, std::istream >::type &
operator>>(std::istream& is, Container& container)
{
	try
	{
		matchString(is, "collection");
		matchString(is, "(");
		
		unsigned int size;
		is >> size;
	
		matchString(is, ")");
		matchString(is, "{");
		
		typename std::insert_iterator<Container> iiter(container, container.begin());
		
		for (unsigned int i = 0; i < size; i++)
		{
			typename Container::value_type value;
			is >> value;
			
			*iiter++ = value;
			matchString(is, ",");
		}
					 
		matchString(is, "}");

		return is;
	}
	catch (ParseError& e)
	{
		e.info = "Faild to parse collection:\n" + e.info;
		throw;
	}
	catch (std::exception& e)
	{
		throw ParseError("Failed to parse collection:\n" + std::string(e.what()) );
	}
}

}

#endif /*SERIALIZATION_H_*/
