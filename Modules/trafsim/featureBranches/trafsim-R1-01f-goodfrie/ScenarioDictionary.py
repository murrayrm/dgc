from libtrafsim_Py import *

class ScenarioDictionary:
    def __init__(self, scenarios):
        self.scenarios = scenarios
        self.function = {
            "simple-1" : self.scenarios.simple_straightline,
            "simple-2" : self.scenarios.simple_turns,
            "simple-3" : self.scenarios.simple_staticobstacle,
            "simple-4" : self.scenarios.simple_roadblock,
            "simple-5" : self.scenarios.simple_circleblock,
            "stluke_singleroad_roadblock" : self.scenarios.stluke_singleroad_roadblock,
            "stluke_singleroad_laneblock" : self.scenarios.stluke_singleroad_laneblock,
            "stluke_singleroad_partlaneblock" : self.scenarios.stluke_singleroad_partlaneblock,
            "stluke_small_partlaneblock" : self.scenarios.stluke_small_partlaneblock,
            "santaanita_sitevisit_roadblock" : self.scenarios.santaanita_sitevisit_roadblock,
            "santaanita_sitevisit_passblock" : self.scenarios.santaanita_sitevisit_passblock,
            "santaanita_sitevisit_block" : self.scenarios.santaanita_sitevisit_block,
            "santaanita_sitevisit_partlaneblock" : self.scenarios.santaanita_sitevisit_partlaneblock,
            "santaanita_sitevisit_sparseroadblock" : self.scenarios.santaanita_sitevisit_sparseroadblock,
            "santaanita_sitevisit_turnroadblock" : self.scenarios.santaanita_sitevisit_turnroadblock,
            "santaanita_sitevisit_simpleintersection" : self.scenarios.santaanita_sitevisit_simpleintersection,
            "santaanita_sitevisit_bigIntersectionBlock" : self.scenarios.santaanita_sitevisit_bigIntersectionBlock,
            "santaanita_sitevisit_fakeIntersectionOrdering" : self.scenarios.santaanita_sitevisit_fakeIntersectionOrdering,
            "santaanita_sitevisit_directedDriving" : self.scenarios.santaanita_sitevisit_directedDriving,
            "santaanita_sitevisit_laneSeparation" : self.scenarios.santaanita_sitevisit_laneSeparation}