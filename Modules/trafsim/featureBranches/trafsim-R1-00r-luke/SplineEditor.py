from libtrafsim_Py import *
from SuperDynamic import SuperDynamic

class SplineEditor(SuperDynamic):
    def __init__(self, environment):
        SuperDynamic.__init__(self)
        
        self.environment = environment
        self.selectedSpline = None
        self.selectedPoint = None
        self.state = ['none']
	
	# stuff for creating new stopped cars
	self.clickedCars = []
	self.numCars = 0
	self.carOrientation = 0
	
	#stuff for creating new static obstacles
	self.obstacles = []
	self.numObs = 0
	self.length = 2
	self.width = 2
	self.radius = 2
	self.orientation = 0

        self.enabled = True
        
    def _switchSpline(self, newSpline):
        if self.selectedSpline:
            self.selectedSpline.color = Color(1, 1, 1, 1)
        
        newSpline.color = Color(1, 0, 0, 1)
        self.selectedSpline = newSpline
        
    def _selectSplineAt(self, pos, threshold = 30.0):
        closestPoint = Vector(-1000000, -1000000)
        closestSpline = None
        closestIndex = None
        
        for spline in self.environment.getObjectsByClassID('Spline').itervalues():
            spline = Spline.downcast(spline)
            
            for point, index in zip( spline.points, xrange(spline.points.size()) ):
                if Vector.dist2(pos, point) < Vector.dist2(pos, closestPoint):
                    closestPoint = point
                    closestSpline = spline
                    closestIndex = index
    
        if Vector.dist2(pos, closestPoint) < threshold:
            return closestSpline, closestIndex
        else:
            return None, None
        
    def onMouseMotion(self, event):
        if not self.enabled: return
        if self.state[-1] == 'drag':
            self.selectedSpline.points[ self.selectedPoint ] = event.pos
            
            if self.selectedSpline.isCached():
                self.selectedSpline.cacheData(True)

    def buildFromPoints(self, points, laneInfoVec):
        newSpline = Spline(points)
       # newSpline.initialize(points)
        self.environment.addObject(newSpline)
        newSpline.initialize(points)
	newSpline.numForwardLanes = int(laneInfoVec.x);
	newSpline.numReverseLanes = int(laneInfoVec.y);

	self._switchSpline(newSpline)

    def addCar(self,orientation=0):
	self.carOrientation = orientation
	print "Hold 'c' and click on the screen to add a stopped car at that location"

    def addCircleObs(self,radius):
	self.radius = radius
	print "Hold 'o' and click on the screen to add a circular obstacle at that location"

    def addBlockObs(self,length,width,orientation=0):
        self.length = length
	self.width = width
	self.orientation = orientation
	print "Hold 'b' and click on the screen to add a block obstacle at that location"
	

    def onMouseClick(self, event):
        if not self.enabled or event.button != LEFT: return
        if self.state[-1] == 'create':
            newSpline = Spline( VectorArray( [event.pos] ) )
            self.environment.addObject(newSpline)
            
            self._switchSpline(newSpline)
            self.selectedPoint = 0
            self.state.append('drag')
        
        elif self.state[-1] == 'append':
            if not self.selectedSpline:
                self.state.remove('append')
                return
        
            self.selectedSpline.points.push_back( event.pos )
            self.selectedPoint = self.selectedSpline.points.size() - 1
            self.state.append('drag')
            
            if self.selectedSpline.isCached():
                self.selectedSpline.cacheData(True)
                
        elif self.state[-1] == 'delete':
            newSpline, self.selectedPoint = self._selectSplineAt(event.pos)
            if newSpline != None:
                self._switchSpline(newSpline)
            
            if 'create' in self.state:
                if self.selectedSpline:    
                    self.environment.removeObject(self.selectedSpline.getName())
                    self.selectedSpline = None
                    self.selectedPoint = None
                    
            elif 'append' in self.state:
                if self.selectedPoint:            
                    del self.selectedSpline.points[ self.selectedPoint ]
                    if self.selectedPoint >= self.selectedSpline.points.size():
                        self.selectedPoint = self.selectedSpline.points.size() - 1
	elif self.state[-1] == 'pause':
	    if self.environment.paused == True:
	        self.environment.paused = False
	        print "Unpause"
	    elif self.environment.paused == False:
	        self.environment.paused = True
	        print "Pause"


	elif self.state[-1] == 'alice':	
	    newN = event.pos.x + self.environment.xTranslate
	    newE = event.pos.y + self.environment.yTranslate
	    print "To move Alice, restart asim with the following command:"
	    print "./asim -N " + `newN` + " -E " + `newE`
	    print "(Press enter to continue)"

	elif self.state[-1] == 'newCar':
	    i = self.numCars
	    if i == 0:
		self.clickedCars = [Car(1)]
	    else:
		self.clickedCars[i-1:i] = [self.clickedCars[i-1],Car(1)]
	    self.clickedCars[i].setPosition(event.pos.x,event.pos.y)
	    self.clickedCars[i].setOrientation(self.carOrientation)
	    self.environment.addObject(self.clickedCars[i],'Car')
	    self.numCars += 1
	    northing = event.pos.x + self.environment.xTranslate
	    easting = event.pos.y + self.environment.yTranslate
	    print "New car added at ( " + `northing` + "," + `easting` + " )"

	elif self.state[-1] == 'newBlockObs':
#	    self.getProps(0)
	    l = self.length
	    w = self.width
	    o = self.orientation
	    x = event.pos.x
	    y = event.pos.y
	    i = self.numObs
	    if i == 0:
		self.obstacles = [Obstacle(x,y,l,w,o)]
	    else:
		self.obstacles[i-1:i] = [self.obstacles[i-1],Obstacle(x,y,l,w,o)]
	    self.environment.addObject(self.obstacles[i],'Obstacle')
	    self.numObs += 1
	    northing = event.pos.x + self.environment.xTranslate
	    easting = event.pos.y + self.environment.yTranslate
	    print "New static obstacle added at ( " + `northing` + "," + `easting` + " )"

	elif self.state[-1] == 'newCircleObs':
#	    self.getProps(1)
	    r = self.radius
	    x = event.pos.x
	    y = event.pos.y
	    i = self.numObs
	    if i == 0:
		self.obstacles = [Obstacle(x,y,r)]
	    else:
		self.obstacles[i-1:i] = [self.obstacles[i-1],Obstacle(x,y,r)]
	    self.environment.addObject(self.obstacles[i],'Obstacle')
	    self.numObs += 1
	    northing = event.pos.x + self.environment.xTranslate
	    easting = event.pos.y + self.environment.yTranslate
	    print "New static obstacle added at ( " + `northing` + "," + `easting` + " )"

        elif self.state[-1] == 'none':
            newSpline, self.selectedPoint = self._selectSplineAt(event.pos)
            
            if newSpline != None:
                self._switchSpline(newSpline)
                self.state.append('drag')
                
            else:
                print event.pos
                
    def onMouseUnclick(self, event):
        if not self.enabled or event.button != LEFT: return
        try: 
            self.state.remove('drag')
        except: pass    
        
    def onKeyDown(self, event):
        if event.button in ( SDLK_LCTRL, SDLK_RCTRL ):
            self.state.append('create')
        elif event.button in ( SDLK_LSHIFT, SDLK_RSHIFT ):
            self.state.append('append')
        elif event.button in ( SDLK_d, ):
            self.state.append('delete')
        elif event.button in ( SDLK_a, ):
	    self.state.append('alice')
	elif event.button in ( SDLK_c, ):
	    self.state.append('newCar')
	elif event.button in ( SDLK_p, ):
	    self.state.append('pause')
	elif event.button in ( SDLK_o, ):
	    self.state.append('newCircleObs')
	elif event.button in ( SDLK_b, ):
	    self.state.append('newBlockObs')
        elif event.button in ( SDLK_z, ):
            self.enabled = False
     
    def onKeyUp(self, event):
        try:
            if event.button in ( SDLK_LCTRL, SDLK_RCTRL ):
                self.state.remove('create')
            elif event.button in ( SDLK_LSHIFT, SDLK_RSHIFT ):
                self.state.remove('append')
            elif event.button in ( SDLK_d, ):
                self.state.remove('delete')
 	    elif event.button in ( SDLK_a, ):
	    	self.state.remove('alice')
	    elif event.button in ( SDLK_c, ):
	    	self.state.remove('newCar')
	    elif event.button in ( SDLK_p, ):
	        self.state.remove('pause')
	    elif event.button in ( SDLK_o, ):
	        self.state.remove('newCircleObs')
	    elif event.button in ( SDLK_b, ):
	        self.state.remove('newBlockObs')
            elif event.button in ( SDLK_z, ):
                self.enabled = True
        except: pass

