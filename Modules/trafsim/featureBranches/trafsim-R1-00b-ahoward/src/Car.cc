#include "Car.hh"
#include "IDM.hh"
#include <GL/gl.h>
#include <cmath>
#include <cassert>

namespace TrafSim
{

enum { STATE_NORMAL, STATE_APPROACH_STOP, STATE_WAIT_CAR, STATE_WAIT_GAP, STATE_ENTER_INT, STATE_CROSSING_INT };

Car::Car(): 
	orientation(0.0f), velocity(0.0f), targetVelocity(0.0f),
	nextCurveEvalPoint(0.0f), pathIndex(0), nextManeuver(CROSS_STRAIGHT),
	currentRoad(0), currentLane(0), state(STATE_NORMAL), followingCar(0), dead(false)
{
}

Car::~Car()
{
}

Car* Car::downcast(Object* source)
{
	return dynamic_cast<Car*>(source);
}

void Car::setPath(std::vector<unsigned int> const& path)
{
	// this function just converts from a std::vector<unsigned int> 
	// to a std::vector<CrossManeuver>
	
	typedef std::vector<CrossManeuver> DestType;
	typedef std::vector<unsigned int> SrcType;
	
	DestType temp;
	for (SrcType::const_iterator i = path.begin(); i != path.end(); ++i)
		temp.push_back( (CrossManeuver)(*i) );
	
	setPath(temp);
}

void Car::setPath(std::vector<CrossManeuver> const& path)
{
	this->path = path;
	pathIndex = -1;
	
	// transfer over to the car's new location
	if (currentRoad)
	{
		Lane* newLane = createMultiLane();
		
		float newT = 0.0f;
		if (currentLane)
		{
			newT = newLane->mapT(currentLane, currentLane->getT(this));
			
			currentLane->removeObject(this);
			delete currentLane;
			
			newLane->addObject(this, newT);
		}
		else
			newLane->addObject(this, 0.0f);
		
		currentLane = newLane;
		updateTargetVelocity(newT);
	}
}

Lane* Car::createMultiLane()
{
	std::vector<LaneSegment> lsa;
	
	// a car's path loops around when it ends
	if (++pathIndex >= (int)path.size())
		pathIndex = 0;
	
	// if the path is empty, just go straight by default
	nextManeuver = CROSS_STRAIGHT;
	if (!path.empty())
		nextManeuver = path[pathIndex];

	Lane* lanes[3];
	// grab the intersection we're heading toward
	Intersection* inter = currentRoad->isReverse(laneIndex)?
		currentRoad->getEnd() :
		currentRoad->getStart();

	// these are the first two parts of the multilane to be constructed
	lanes[0] = currentRoad->getLane(laneIndex);
	lanes[1] = inter->getLane(*currentRoad, laneIndex, nextManeuver);

	// if the given maneuver is not available, just use the sequentially next one
	CrossManeuver origManeuver = nextManeuver;
	while (!lanes[1])
	{
		nextManeuver = (CrossManeuver)((nextManeuver + 1) % 3);
		lanes[1] = inter->getLane(*currentRoad, laneIndex, nextManeuver);
		
		// if the car is approaching a dead end...
		if (nextManeuver == origManeuver)
		{
			dead = true;
			lsa.push_back( LaneSegment(lanes[0], 0.0f, 1.0f) );
			return new MultiLane(lsa);
		}	
	}
	
	// the last part of the multilane to be constructed
	std::pair<Road*, unsigned int> dest = 
		inter->getDestination(*currentRoad, laneIndex, nextManeuver);
	lanes[2] = dest.first->getLane(dest.second);
	
	// now create and return the new multilane
	for (int i = 0; i < 3; ++i)
		lsa.push_back( LaneSegment(lanes[i], 0.0f, 1.0f) );

	return new MultiLane(lsa);
}

void Car::switchRoad()
{
	// grab the intersection we're heading toward
	Intersection* inter = currentRoad->isReverse(laneIndex)?
		currentRoad->getEnd() :
		currentRoad->getStart();
	
	// transfer over to the new road
	std::pair<Road*, unsigned int> dest = 
		inter->getDestination(*currentRoad, laneIndex, nextManeuver);
	
	currentRoad = dest.first;
	laneIndex = dest.second;
	nextCurveEvalPoint = 0.0f;
}

void Car::updateTargetVelocity(float t)
{
	// call the curveConstraint function at regular intervals
	if (t >= nextCurveEvalPoint)
	{
		nextCurveEvalPoint = currentLane->spline
			->evalDisplacement(t, props.curveEvalInterval);
		
		targetVelocity = curveConstraint(
			*currentLane->spline, t, velocity,
			props.comfyAccel, props.maxAngularAccel);
			
		if (targetVelocity > currentRoad->getSpeedLimit())
			targetVelocity = currentRoad->getSpeedLimit();
	}
}

bool Car::simulate(float ticks)
{
	if (!currentLane) return true;
	
	// update the position
	float t = currentLane->getT(this);
	t = currentLane->spline->evalDisplacement(t, velocity * ticks);
	
	// delete cars that hit a dead end
	if (dead && t >= 0.95f)
	{
		currentLane->removeObject(this);
		delete currentLane;
		delete this;
		return false;
	}		
	
	// grab the intersection we're heading toward
	Intersection* inter = currentRoad->isReverse(laneIndex)?
					currentRoad->getEnd() :
					currentRoad->getStart();
	
	// apply the curve constraint
	currentLane->setT(this, t);
	position = currentLane->spline->evalCoordinate(t);
	Vector norm = currentLane->spline->evalNormal(t);
	orientation = atan2(norm.y, norm.x);
	
	updateTargetVelocity(t);
	
	// get the next car, and apply the IDM
	std::pair<float, Object*> nextCarInfo = 
		currentLane->getObjectAfter(t, "Car");
	CarInterface* nextCar = (CarInterface*)nextCarInfo.second;

	float IDMAccel = 0.0f;
	if (nextCar)
	{	
		float carsOwnT = nextCar->getCurrentLane()->getT(nextCar);	
		float otherCarVel = nextCar->getLaneVelocity() * 
			currentLane->mapTangent(nextCar->getCurrentLane(),
				carsOwnT, nextCarInfo.first);		
		float gap = currentLane->spline->evalLength(t, nextCarInfo.first);
	
		if (otherCarVel < 0.0f)
			otherCarVel = 0.0f;
		
		IDMAccel = evalIDMAccel(velocity, otherCarVel, gap, targetVelocity,
							   props, nextCar->getProperties());					   
	}	
	else
		// there is no car in front of this one
		IDMAccel = evalIDMAccel(velocity, 0.0f, -1.0f, targetVelocity,
								 props, props);
								 
	// don't allow the IDM to accelerate us past the comfort level
	if (IDMAccel > props.comfyAccel)
		IDMAccel = props.comfyAccel;
	
	float remaining, targetVel2, followT, completedPart;
	RightOfWay row;
	switch (state)
	{
		case STATE_NORMAL:
		
			// check for approaching stop signs
			if (!dead && t >= 0.3f && t < 0.33f)
			{
				if (inter->isStop(*currentRoad))
					state = STATE_APPROACH_STOP;	
			}
				
			// if we're past an intersection, transfer to a new lane
			if (!dead && t >= 0.7f)
			{
				switchRoad();
				Lane* newLane = createMultiLane();
				currentLane->removeObject(this);
				t = newLane->mapT(currentLane, t);				 
				newLane->addObject(this, t);
				
				delete currentLane;
				currentLane = newLane;
			}
			
			// apply the acceleration
			velocity += IDMAccel * ticks;
			if (velocity < 0.0f)
				velocity = 0.0f;
				
			break;
			
		case STATE_APPROACH_STOP:
			// find the distance to the stop line
			remaining = currentLane->spline->evalLength(t, 0.3333333f);
			if (remaining < 0.0f) 
				remaining = 0.0f;
			
			// get the highest velocity that can let us stop before the stopline
			targetVel2 = sqrt(2.0f * props.maxAccel * remaining);
			
			// choose the highest velocity that fits both IDM and stop line constraints
			velocity += IDMAccel * ticks;
			if (velocity < 0.0f) velocity = 0.0f;
			if (targetVel2 < velocity) velocity = targetVel2;
				
			// if we're next in line at the stop sign, see what to do next
			if (velocity < 0.1f && remaining < props.size.y * 0.5f)
			{
				row = inter->rightOfWay(this, *currentRoad, &followingCar);
				if (row == ROW_GAP)
					inter->getOpposingTraffic(*currentRoad, laneIndex, nextManeuver, opposingLanes);
				
				if (followingCar)
					state = STATE_WAIT_CAR;
				else
					state = (row == ROW_GO) ? STATE_CROSSING_INT : STATE_WAIT_GAP;
			}
			
			break;
		
		case STATE_WAIT_CAR:  // waiting for a car to finish crossing at the intersection
		
			// find how much of the maneuver the crossing car has completed so far
			velocity = 0.0f;
			followT = followingCar->currentLane->getT(followingCar);
			completedPart = (followT - 0.3333333f) * 3.0f;
			
			// if it's gone far enough, enter the intersection
			if (completedPart > props.carWaitFactor)
			{
				followingCar = 0;
				
				if (opposingLanes.empty())
					state = STATE_CROSSING_INT;
				else
					state = STATE_WAIT_GAP;
			}
			
			break;
			
		case STATE_WAIT_GAP:  // waiting for a gap in opposing traffic
		
			// apply the gap acceptance function to all the potential streams of traffic
			velocity = 0.0f;
			for (std::vector<Lane*>::iterator i = opposingLanes.begin();
				 i != opposingLanes.end(); ++i)
			{
				if (!acceptGap(*i))
					return true;
			}
			
			// if they all pass, enter the intersection
			opposingLanes.clear();
			state = STATE_CROSSING_INT;
		
			break;
			
		case STATE_CROSSING_INT:
		
			// if we've left the intersection, notify and switch back to normal
			if (t > 0.666f)
			{
				inter->exitIntersection(this);
				state = STATE_NORMAL;
			}
	
			// apply the IDM acceleration
			velocity += IDMAccel * ticks;
			if (velocity < 0.0f)
				velocity = 0.0f;
		
			break;
	}
	
	return true;	
}

void Car::switchLane(Road* newRoad, unsigned int newLaneIndex, float startT)
{
	// remove ourselves fromt the current lane, and delete our own multilane.
	if (currentLane)
	{
		currentLane->removeObject(this);
		delete currentLane;
		currentLane = 0;
	}
	
	// reset all parameters, and start over at the given position
	state = STATE_NORMAL;
	pathIndex = -1;
	currentRoad = newRoad;
	laneIndex = newLaneIndex;
	
	currentLane = createMultiLane();
	startT = currentLane->mapT(newRoad->getLane(newLaneIndex), startT);
	currentLane->addObject(this, startT);
	
	velocity = 0.0f;
	nextCurveEvalPoint = 0.0f;
	updateTargetVelocity(startT);
}

bool Car::acceptGap(Lane* opposingLane)
{
	// get the first place the current lane intersects with the opposing traffic stream
	LaneInterferenceDesc* idesc = opposingLane->getInterferenceAfter(currentLane, -1.0f);
	
	// estimate how long it will take for us to reach that point
	float myInterfT = idesc->otherSeg.t_start;
	float otherInterfT = idesc->mySeg.t_start;
	float myMappedT = currentLane->mapT(idesc->otherSeg.segment, myInterfT);
	float otherMappedT = opposingLane->mapT(idesc->mySeg.segment, otherInterfT);
	
	float dist = currentLane->spline->evalLength(0.33333f, myMappedT);
	float timeEst = sqrt( dist / props.comfyAccel * 1.0f );
	
	// now, find the car that will be behind us at that point in time
	float predictedT = 1000.0f;
	std::pair<float, Object*> carInfo;	
	CarInterface* carBefore;
	float testT = otherMappedT;
	
	while (predictedT > otherMappedT)
	{
		carInfo = opposingLane->getObjectBefore(testT, "Car");
		carBefore = (CarInterface*)carInfo.second;
		
		if (!carBefore )
			return true;
			
		testT = carInfo.first - 0.001f;
		predictedT = opposingLane->spline->evalDisplacement(carInfo.first, carBefore->getLaneVelocity() * timeEst);
	}
	
	// and apply the IDM to the car behind us
	float dotp = opposingLane->mapTangent(currentLane, myMappedT, otherMappedT);
	if (dotp < 0.0f) 
		dotp = 0.0f;
		
	float gap = opposingLane->spline->evalLength(predictedT, otherMappedT);
	float forcedAccel = evalIDMAccel(carBefore->getLaneVelocity(), props.comfyAccel * timeEst * dotp, gap, 
									 carBefore->getLaneVelocity(), carBefore->getProperties(), props);

	return -forcedAccel < props.maxAccel * props.gapAcceptFactor;
}
		
float Car::getLaneVelocity()
{
	return velocity;
}

Lane* Car::getCurrentLane()
{
	return currentLane;
}
		
void Car::serialize(std::ostream& os) const
{
	// NOT IMPLEMENTED
}

void Car::deserialize(std::istream& is)
{
	// NOT IMPLEMENTED
}

void Car::draw()
{
	// draw a colored rectangle
	
    glPushMatrix();
    glTranslatef(position.x, position.y, 0.0f);
    glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
    glSetColor(props.color);
    
    glBegin(GL_POLYGON);
    glVertex3f(-props.size.x * 0.5f, -props.size.y * 0.5f, 0.0f);
    glVertex3f(-props.size.x * 0.5f,  props.size.y * 0.5f, 0.0f);
    glVertex3f( props.size.x * 0.5f,  props.size.y * 0.5f, 0.0f);
    glVertex3f( props.size.x * 0.5f, -props.size.y * 0.5f,  0.0f);
    glEnd();
    
    glPopMatrix();
}

}
