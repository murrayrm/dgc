#ifndef ROAD_H_
#define ROAD_H_

#include "Object.hh"
#include "Lane.hh"
#include "Spline.hh"
#include "Vector.hh"
#include "Intersection.hh"
#include "Environment.hh"

#include <vector>

namespace TrafSim
{
	
class Intersection;
class DeadEnd;

/**
 * A road is essentially a bundle of Lanes. Roads can be connected into a sort of graph
 * via intersections. Roads are probably the lowest-level objects (aside from Splines)
 * that need to be created when constructing an environment.
 */
class Road: public Object
{
public:
	/**
	 * No complex initialization happens in the default constructor.
	 */
	Road();
	/**
	 * This constructor is equivalent to calling initialize() with its arguments.
	 * \param env The environment to create the Road in
	 * \param source The spline describing the road's geometry
	 * \param numForwardLanes The number of lanes going in the direction of the source spline
	 * \param numReverseLanes The number of lanes going in the direction opposite the source spline
	 * \param laneWidth The width of a lane, in meters
	 */
	Road(Environment& env, Spline& source, unsigned int numForwardLanes, 
		 unsigned int numReverseLanes, float laneWidth = 5.0f);
	virtual ~Road();
	
	virtual std::string getClassID() const { return "Road"; }
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a Spline
	/// \return A Road*, pointing to the same object as source.	
	static Road* downcast(Object* source);
	/**
	 * Draws a simple representation of a road. Does not draw the road's lanes;
	 * those are considered generally independent.
	 */
	virtual void draw();
	/** 
	 * Call this function before anything else, if you used the default constructor.
	 * \param env The environment to create the Road in
	 * \param source The spline describing the road's geometry
	 * \param numForwardLanes The number of lanes going in the direction of the source spline
	 * \param numReverseLanes The number of lanes going in the direction opposite the source spline
	 * \param laneWidth The width of a lane, in meters
	 */
	void initialize(Environment& env, Spline& source, unsigned int numForwardLanes, 
					unsigned int numReverseLanes, float laneWidth = 5.0f);
	/**
	 * \param index An integer index into the road's list of lanes
	 * \return A Lane* corresponding to the road's lane at the given index.
	 */
	Lane* getLane(unsigned int index);
	/** 
	 * \param index An integer index into the road's list of forward-facing lanes.
	 * \return A Lane* corresponding to the road's lane at the given index.
	 */
	Lane* getForwardLane(unsigned int index);
	/** 
	 * \param index An integer index into the road's list of reverse-facing lanes.
	 * \return A Lane* corresponding to the road's lane at the given index.
	 */
	Lane* getReverseLane(unsigned int index);
	/**
	 * \return The total number of lanes in the road
	 */
	unsigned int numLanes() const;
	/**
	 * \return The number of forward-facing lanes in the road
	 */
	unsigned int numForwardLanes() const;
	/**
	 * \return The number of reverse-facing lanes in the road
	 */
	unsigned int numReverseLanes() const;
	/**
	 * \param lane An integer index into the road's list of lanes
	 * \return True if the lane is reversed, False if it is normal
	 */
	bool isReverse(unsigned int lane) const;
  /**
   * Checks if the road has a certain lane
   * \return true if road has the given lane, false otherwise
   */
  bool containsObject(Object* lane);

	/**
	 * \return The source spline for this road
	 */
	Spline& getSpline();
	/**
	 * \return The Intersection connected to the start of this road
	 */
	Intersection* getStart();
	/**
	 * \return The Intersection connected to the end of this road
	 */
	Intersection* getEnd();
	/**
	 * Internal. Do not use.
	 */
	void setStart(Intersection* newStart);
	/**
	 * Internal. Do not use.
	 */
	void setEnd(Intersection* newEnd);
	/**
	 * \return The width in meters of the road's lanes
	 */
	float getLaneWidth() const;
	/**
	 * \return The maximum speed cars are allowed to attain on this road (defaults to 30 mph)
	 */
	float getSpeedLimit() const;
	/**
	 * \param limit The maximum speed cars are allowed to attain on this road (defaults to 30 mph)
	 */
	void setSpeedLimit(float limit);
	/**
	 * \param t A t-coordinate from 0 to 1
	 * \param side -1 for left, 1 for right
	 * \param reversed Flips the meaning of "side" if true (internal use) 
	 * \return The world coordinates of the side of the road at the given t coordinate
	 */
	Vector getBoundary(float t, int side, bool reversed = false);

  /*! Gets the boundary of a given lane inside the road 
   * \param t a t-coordinate from 0 to 1
   * \param laneNumber the number of the lane, ranging from 1 to number of lanes
   * \param reversed Flips the meaning of "side" if true (internal use)
   */
  Vector getLaneBoundary(float t, int laneNumber);

  /*! returns the desired road boundary
   * \param side -1 for left, 1 for right
   * \param spacing how far apart to space the points. this should be a number
   * in between 0 and 1
   */
  std::vector<point2> getRoadBoundary(int side, double spacing);
  /*! returns a set of lane boundaries, given from left to right 
   * \param spacing how far apart to space the points. this should be a number
   * between 0 and 1
   */
  std::vector< std::vector<point2> > getLaneBoundaries(double spacing);
	
private:
	Spline source;
	std::vector<Lane*> lanes;
	float laneWidth;
	
	unsigned int nFwdLanes;
	unsigned int nBackLanes;
	
	Intersection* start;
	Intersection* end;
	
	DeadEnd* deadEnd;
	bool initialized;
	
	float speedLimit;
	
protected:
	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);
};

}

#endif /*ROAD_H_*/
