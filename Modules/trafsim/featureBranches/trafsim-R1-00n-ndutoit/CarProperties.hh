#ifndef CARPROPERTIES_H_
#define CARPROPERTIES_H_

#include <iostream>
#include "Vector.hh"
#include "Color.hh"

namespace TrafSim
{

/**
 * This POD class groups together all the properties that distinguish different cars,
 * in terms of their driving behavior. A default CarProperties() object makes a red
 * car with "reasonable" values for all parameters.
 */
class CarProperties
{
public:
	CarProperties();
	
	Vector size;        ///< width and length, in meters
  float height;           ///< height, in meters
	Color color;		///< just for visualization
	
	float comfyAccel; 		///< the highest "comfortable" acceleration a driver can withstand
	float maxAccel;			///< the absolute highest braking deceleration a driver can withstand
	float maxAngularAccel;	///< the highest angular acceleration a driver will voluntarily subject itself to
	
	float safetyTime;		///< time in seconds allotted to come to a complete stop in an emergency
	float minDistance;		///< the minimum bumper-to-bumper distance allowed to the car in front
	
	float carWaitFactor;	///< the percentage of an intersection-crossing maneuver that must be completed by
							///< the car currently in the intersection, before the next car will enter the intersection.
							
	float gapAcceptFactor;	///< the percentage of maxAngularAccel that the driver is willing to subject others to,
							///< while crossing a major stream of traffic
	
	float accelDelta;		///< the acceleration exponent parameter (see the IDM documentation)
	
	float curveEvalInterval;	///< the length in meters of the intervals at which a car reevaluates its targetVelocity
	
	/// Serialization
	friend std::ostream& operator<<(std::ostream& os, CarProperties const& desc);
	
	/// Deserialization
	friend std::istream& operator>>(std::istream& is, CarProperties& desc);
};

}

#endif /*CARPROPERTIES_H_*/
