from libtrafsim_Py import *
from SuperDynamic import SuperDynamic
from Event import Event

class SplineEditor(SuperDynamic):
    def __init__(self, environment):
        SuperDynamic.__init__(self)
        
        self.environment = environment
        self.selectedSpline = None
        self.selectedPoint = None
        self.selectedIntersection = None
	self.state = ['none']
	
	# stuff for creating new cars
	self.clickedCars = []
	self.numCars = 0
	self.carMaxVelocity = 1.0
	
	#stuff for creating new static obstacles
	self.obstacles = []
	self.numObs = 0
	self.length = 2
	self.width = 2
	self.radius = 2
	self.orientation = 0

        self.enabled = True
	self.obsFile = ""
        self.string=""

        self.makingEvents = []
	self.eventStrings = []
        self.events = []
	self.eventScenario = ""
        self.eventDistance = 1.0
        self.eventDelay = 0.0
    

    def _switchSpline(self, newSpline):
        if self.selectedSpline:
            self.selectedSpline.color = Color(.4, .4, .4, .4)
        
        newSpline.color = Color(1, 0, 0, 1)
        self.selectedSpline = newSpline
        
    def _selectSplineAt(self, pos, threshold = 30.0):
        closestPoint = Vector(-1000000, -1000000)
        closestSpline = None
        closestIndex = None
        
        for spline in self.environment.getObjectsByClassID('Spline').itervalues():
            spline = Spline.downcast(spline)
            
            for point, index in zip( spline.points, xrange(spline.points.size()) ):
                if Vector.dist2(pos, point) < Vector.dist2(pos, closestPoint):
                    closestPoint = point
                    closestSpline = spline
                    closestIndex = index
    
        if Vector.dist2(pos, closestPoint) < threshold:
            return closestSpline, closestIndex
        else:
            return None, None
        
    def onMouseMotion(self, event):
        if not self.enabled: return
        if self.state[-1] == 'drag':
            self.selectedSpline.points[ self.selectedPoint ] = event.pos
            
            if self.selectedSpline.isCached():
                self.selectedSpline.cacheData(True)

    def buildFromPoints(self, points, laneInfoVec):
        newSpline = Spline(points)
        self.environment.addObject(newSpline)
        newSpline.initialize(points)
	newSpline.numForwardLanes = int(laneInfoVec.x);
	newSpline.numReverseLanes = int(laneInfoVec.y);
	self._switchSpline(newSpline)

    def addCar(self,maxVelocity=1.0):
	self.carMaxVelocity = maxVelocity
	print "Hold 'c' and click on the screen to add a stopped car at that location"

    def addCircleObs(self,radius):
	self.radius = radius
	print "Hold 'o' and click on the screen to add a circular obstacle at that location"

    def addBlockObs(self,length,width,orientation=0):
        self.length = length
	self.width = width
	self.orientation = orientation
	print "Hold 'b' and click on the screen to add a block obstacle at that location"

    def addEvent(self, scenario, distance = 1.0, delay = 0.0):
        self.eventScenario = scenario
        self.eventDistance = distance
        self.eventDelay = delay
        print "Hold 'e' and click on the screen to add an event listener at that location"
	

    def onMouseClick(self, event):
        if not self.enabled or event.button != LEFT: return
        if self.state[-1] == 'create':
            newSpline = Spline( VectorArray( [event.pos] ) )
            self.environment.addObject(newSpline)
            
            self._switchSpline(newSpline)
            self.selectedPoint = 0
            self.state.append('drag')
        
        elif self.state[-1] == 'append':
            if not self.selectedSpline:
                self.state.remove('append')
                return
        
            self.selectedSpline.points.push_back( event.pos )
            self.selectedPoint = self.selectedSpline.points.size() - 1
            self.state.append('drag')
            
            if self.selectedSpline.isCached():
                self.selectedSpline.cacheData(True)
                
        elif self.state[-1] == 'delete':
            newSpline, self.selectedPoint = self._selectSplineAt(event.pos)
            if newSpline != None:
                self._switchSpline(newSpline)
            
            if 'create' in self.state:
                if self.selectedSpline:    
                    self.environment.removeObject(self.selectedSpline.getName())
                    self.selectedSpline = None
                    self.selectedPoint = None
                    
            elif 'append' in self.state:
                if self.selectedPoint:            
                    del self.selectedSpline.points[ self.selectedPoint ]
                    if self.selectedPoint >= self.selectedSpline.points.size():
                        self.selectedPoint = self.selectedSpline.points.size() - 1
	elif self.state[-1] == 'pause':
	    if self.environment.paused == True:
	        self.environment.paused = False
	        print "Unpause"
	    elif self.environment.paused == False:
	        self.environment.paused = True
	        print "Pause"


	elif self.state[-1] == 'alice':	
	    newN = event.pos.x + self.environment.xTranslate
	    newE = event.pos.y + self.environment.yTranslate
	    print "To move Alice, restart asim with the following command:"
	    print "./asim -N " + `newN` + " -E " + `newE`
	    print "(Press enter to continue)"

	elif self.state[-1] == 'newCar':
	    lane = self.lg.getLane(event.pos) 
	    tcoord = self.lg.getPosition(event.pos)
	    
	    roads = [Road.downcast(r) for r in self.environment.getObjectsByClassID('Road').values()]
            FoundLane = False
#	    print "Lane: " + lane.getName()
	    for road in roads:
#	        print "Road: " + road.getName()
	        for x in range(road.numLanes()):
#		    print "Finding...: " + (road.getLane(x)).getName()
		    if road.getLane(x) == lane:
                        FoundLane = True
		        RoadName = road.getName()
		        self.X = x
		        self.Tcoord = tcoord
#		        print RoadName
		        self.carPos = "Road.downcast(self.environment.getObject('" + road.getName() + "')), " + str(x) + ", " + str(tcoord)
	    if not FoundLane:
		print "Lane not found"
		return
#	    print self.X
#	    print RoadName
            c = Car(self.carMaxVelocity)
            self.environment.addObject(c)
            c.switchLane(Road.downcast(self.environment.getObject(RoadName)), self.X, self.Tcoord)
	    carInit = "c = Car(" + `self.carMaxVelocity` + ")"
            carAdd = "self.environment.addObject(c)"
            carMove = "c.switchLane(" + self.carPos + ")"
	    self.lastCar = c
	    if len(self.makingEvents) > 0:
		self.eventStrings[len(self.makingEvents)-1] += "        " + carInit + "\n        " + carAdd + "\n        " + carMove + "\n"
	    elif self.environment.makingScenario:
                self.string += "        " + carInit + "\n        " + carAdd + "\n        " + carMove + "\n"
	    else:
		print "New car added at " + RoadName + " (" + str(x) + ", " + str(tcoord) + ")"

	elif self.state[-1] == 'stoppedCar':

	    lane = self.lg.getLane(event.pos) 
	    tcoord = self.lg.getPosition(event.pos)
	    
	    roads = [Road.downcast(r) for r in self.environment.getObjectsByClassID('Road').values()]
            FoundLane = False
	    for road in roads:
	        for x in range(road.numLanes()):
		    if road.getLane(x) == lane:
                        FoundLane = True
		        RoadName = road.getName()
		        X = x
		        Tcoord = tcoord
		        carPos = "Road.downcast(self.environment.getObject('" + road.getName() + "')), " + str(x) + ", " + str(tcoord)
	    if not FoundLane:
		print "Lane not found"
		return
       
            c = Car(0.0)
            self.environment.addObject(c)
            c.switchLane(Road.downcast(self.environment.getObject(RoadName)), X, Tcoord)
	
	
	elif self.state[-1] == 'newBlockObs':
#	    self.getProps(0)
	    l = self.length
	    w = self.width
	    o = self.orientation
	    x = event.pos.x
	    y = event.pos.y
	    i = self.numObs
	    if i == 0:
		self.obstacles = [Obstacle(x,y,l,w,o)]
	    else:
		self.obstacles[i-1:i] = [self.obstacles[i-1],Obstacle(x,y,l,w,o)]
	    self.environment.addObject(self.obstacles[i],'Obstacle')
	    self.numObs += 1
	    northing = event.pos.x + self.environment.xTranslate
	    easting = event.pos.y + self.environment.yTranslate
	    if len(self.makingEvents) > 0:
		self.eventStrings[len(self.makingEvents)-1] += "        self.environment.addObject(Obstacle(" + `event.pos.x` + "," + `event.pos.y` + ", " + `l` + ", " + `w` + ", " + `o` + "))\n"
	    elif self.environment.makingScenario:
		self.string += "        self.environment.addObject(Obstacle(" + `event.pos.x` + "," + `event.pos.y` + ", " + `l` + ", " + `w` + ", " + `o` + "))\n"
                self.obsFile += `easting` + " " + `northing` + " " + `l` + " " + `w` + " " + `o` + "\n"

	    else :
		print "New static obstacle added at ( " + `northing` + "," + `easting` + " )"

	elif self.state[-1] == 'newCircleObs':
#	    self.getProps(1)
	    r = self.radius
	    x = event.pos.x
	    y = event.pos.y
	    i = self.numObs
	    if i == 0:
		self.obstacles = [Obstacle(x,y,r)]
	    else:
		self.obstacles[i-1:i] = [self.obstacles[i-1],Obstacle(x,y,r)]
	    self.environment.addObject(self.obstacles[i],'Obstacle')
	    self.numObs += 1
	    northing = event.pos.x + self.environment.xTranslate
	    easting = event.pos.y + self.environment.yTranslate
	    if len(self.makingEvents) > 0:
		self.eventStrings[len(self.makingEvents)-1] += "        self.environment.addObject(Obstacle(" + `x` + ", " + `y` +", " + `r` + "))\n"
	    elif self.environment.makingScenario:
		self.string += "        self.environment.addObject(Obstacle(" + `x` + ", " + `y` +", " + `r` + "))\n"
		self.obsFile += `easting` + " " + `northing` + " " + `r` + "\n"
	    else:
		print "New static obstacle added at ( " + `northing` + "," + `easting` + " )"


        elif self.state[-1] == 'newEvent':
	    distance = self.eventDistance
	    delay = self.eventDelay
	    name = self.eventScenario
	    if len(name) == 0:
	        print "No scenario specified for this event"
	        return
	    x = event.pos.x
	    y = event.pos.y
	    self.events.append(Event(name, x, y, distance, delay))
	    if len(self.makingEvents) > 0:
		self.eventStrings[len(self.makingEvents)-1] += "        self.editor.events.append(Event(" + name+ ", " + `x` + ", " + `y` + ", " + `distance` + ", " + `delay` + "))\n"
	    elif self.environment.makingScenario:
	        self.string += "        self.editor.events.append(Event(\"" + name+ "\", " + `x` + ", " + `y` + ", " + `distance` + ", " + `delay` + "))\n"
	    northing = event.pos.x + self.environment.xTranslate
	    easting = event.pos.y + self.environment.yTranslate
	    print "New event listener added at ( " + `northing` + ", " + `easting`+ " )"


        elif self.state[-1] == 'none':
            newSpline, self.selectedPoint = self._selectSplineAt(event.pos)
            
            if newSpline != None:
                self._switchSpline(newSpline)
                self.state.append('drag')
                
            else:
                print event.pos
        

        elif self.state[-1] == 'appendIntersection':
	    if self.selectedIntersection == None:
	        print "You must type use the app.[buildintersectiontype] command before attempting to add a road to an intersection."

	    else:
            
	        if self.selectedPoint == None or self.selectedPoint == None:
	            print "Please select a spline and a point, then try again."

                else:
                    if self.selectedIntersection.numRoads() < self.selectedIntersection.totalRoads:
			road = Road.downcast(self.selectedSpline.roadBuilt)
                        if (self.selectedSpline.points.size()-1) == self.selectedPoint:
        	            print self.selectedIntersection.hookStart(road)
                            self.selectedIntersection.addSpline(self.selectedSpline, True)

                        elif self.selectedPoint == 0:
		            print self.selectedIntersection.hookEnd(road)
			    self.selectedIntersection.addSpline(self.selectedSpline, False)

		        else:
		            print "Error: You must select an endpoint of a spline"
                        if self.selectedIntersection.numRoads() == self.selectedIntersection.totalRoads():
		            self.selectedIntersection.computeGeometry(self.environment) 
                    else:
	                print "Error: This intersection already has the correct number of splines attached."
        
        elif self.state[-1] == 'getRoadData':
	    lane = self.lg.getLane(event.pos) 
	    tcoord = self.lg.getPosition(event.pos)
	    
	    roads = [Road.downcast(r) for r in self.environment.getObjectsByClassID('Road').values()]

	    for road in roads:
	        for x in range(road.numLanes()):
		    if road.getLane(x) == lane:
		        print "Road.downcast(app.environment.getObject('" + road.getName() + "')), " + str(x) + ", " + str(tcoord)

    def onMouseUnclick(self, event):
        if not self.enabled or event.button != LEFT: return
        try: 
            self.state.remove('drag')
        except: pass    
        
    def onKeyDown(self, event):
        if event.button in ( SDLK_LCTRL, SDLK_RCTRL ):
            self.state.append('create')
        elif event.button in ( SDLK_LSHIFT, SDLK_RSHIFT ):
            self.state.append('append')
        elif event.button in ( SDLK_d, ):
            self.state.append('delete')
        elif event.button in ( SDLK_a, ):
	    self.state.append('alice')
	elif event.button in ( SDLK_c, ):
	    self.state.append('newCar')
	elif event.button in ( SDLK_p, ):
	    self.state.append('pause')
	elif event.button in ( SDLK_o, ):
	    self.state.append('newCircleObs')
	elif event.button in ( SDLK_b, ):
	    self.state.append('newBlockObs')
        elif event.button in ( SDLK_e, ):
	    self.state.append('newEvent')
        elif event.button in ( SDLK_i, ):
	    self.state.append('appendIntersection')
        elif event.button in ( SDLK_r, ):
	    self.state.append('getRoadData')
        elif event.button in ( SDLK_z, ):
            self.enabled = False
     
    def onKeyUp(self, event):
        try:
            if event.button in ( SDLK_LCTRL, SDLK_RCTRL ):
                self.state.remove('create')
            elif event.button in ( SDLK_LSHIFT, SDLK_RSHIFT ):
                self.state.remove('append')
            elif event.button in ( SDLK_d, ):
                self.state.remove('delete')
 	    elif event.button in ( SDLK_a, ):
	    	self.state.remove('alice')
	    elif event.button in ( SDLK_c, ):
	    	self.state.remove('newCar')
	    elif event.button in ( SDLK_p, ):
	        self.state.remove('pause')
	    elif event.button in ( SDLK_o, ):
	        self.state.remove('newCircleObs')
	    elif event.button in ( SDLK_b, ):
	        self.state.remove('newBlockObs')
	    elif event.button in ( SDLK_e, ):
	        self.state.remove('newEvent')
            elif event.button in ( SDLK_i, ):
                self.state.remove('appendIntersection')
            elif event.button in ( SDLK_r, ):
	        self.state.remove('getRoadData')
            elif event.button in ( SDLK_z, ):
                self.enabled = True
        except: pass

