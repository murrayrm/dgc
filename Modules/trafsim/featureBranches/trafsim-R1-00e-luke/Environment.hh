#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_

#include "Vector.hh"
#include "Object.hh"

#include <iostream>
#include <map>
#include <string>
#include <list>
#include <utility>

namespace TrafSim
{
	
typedef std::map<std::string, Object*> ObjectMap;
typedef std::map<std::string, ObjectMap> EnvironmentMap;
typedef std::map<std::string, unsigned int> NameMap;

/**
 * A directory of simulation objects. An Environment's primary purpose is to maintain
 * references to the "root" objects in the simulation (such as roads, intersections, and
 * other primary features), to ensure that each root object has a unique name, and
 * to be able to save and restore its entire state, including that of each of its components.
 * 
 * If required an Environment will delete references to the objects it holds when they expire;
 * however, this is NOT automatic. If objects are to be deleted, make sure to make a call
 * to Environment::clearObjects().
 * 
 * NOTE: This class is not thread-safe. Only use it from a single thread. Furthermore, calls to draw()
 * should take place in the same thread that the current Viewport was created in.
 */
class Environment
{
public:


  // skynet key
  int skynetKey;

	/* ******* CONSTRUCTORS AND DESTRUCTORS ******* */

	/// No complex processing is done in the constructor; 
	/// only some private members are initialized.
        /// Skynet key is pulled from the environment and stored
	Environment();
	
	/* ******* DIRECTORY FUNCTIONS ******* */
	
	/// Adds a reference to the given object, trying to give it the proposedName.
	/// if proposedName is already in use, it will generate a new unique name.
	/// \param object The object to be inserted into the directory
	/// \param proposedName The desired name for the object. Defaults to the object's class ID.
	/// \return The actual name given to the object
	std::string const& addObject(Object* object, std::string const& proposedName = "");
	
	/// Removes an object from the directory. If canDelete is true, also frees the object's memory.
	/// Also note that there are NO safeguards against dangling references. If an object is removed
	/// while another object refers to it, by name or otherwise, the results are unspecified.
	/// \param objName The name of the object to be removed.
	/// \param canDelete If true, will call operator delete on the object (be sure it's actually OK to do this!)
	/// \return True on success, False on failure. The Environment's state will be unchanged if the call fails.
	bool removeObject(std::string const& objName, bool canDelete = true);
	
	/// Changes an object's name.
	/// \param oldName The name of the object to be renamed
	/// \param newName The new name to assign to the object
	/// \return The object's new name on success, "" on failure. The Environment's state will be unchanged if the call fails.
	std::string renameObject(std::string const& oldName, std::string const& newName);
	
	/// Looks up an object by name. O(log n) in the number of objects.
	/// \param objName The name of the object to be looked up
	/// \return A pointer to the object, or NULL on failure.
	Object* getObject(std::string const& objName);
	
	/// Returns a name->object table of all the objects with the given classID.
	/// \param classID The name of the class of objects to return
	/// \returns a std::map from object name to object pointer
	ObjectMap const& getObjectsByClassID(std::string const& classID);
	
	/// Resets the environment to its original state. All objects are removed, and deleted if
	/// canDelete is set to true. The name directory is also cleared out.
	/// \param canDelete If true, will call operator delete on each object (be sure it's actually OK to do this!)
	void clearObjects(bool canDelete = true);
	
	/* ******* REFERENCE FRAME FUNCTIONS ******* */
	
	/// Returns the bottom left corner of the rectangle that determines the boundaries of the environment.
	/// This information is used when interfacing with other software.
	/// \return A vector specifying the location of the corner in world coordinates
	Vector const& getBottomLeftBound() const;

	/// Returns the top right corner of the rectangle that determines the boundaries of the environment.
	/// This information is used when interfacing with other software.
	/// \return A vector specifying the location of the corner in world coordinates
	Vector const& getTopRightBound() const;
	
	/// Sets the rectangle that determines the boundaries of the environment.
	/// This information is used when interfacing with other software.
	/// \param bottomLeft The bottom left corner, in world coordinates
	/// \param topRight The top right corner, in world coordinates
	void setBounds(Vector const& bottomLeft, Vector const& topRight);
	
	/* ******* SERIALIZATION FUNCTIONS ******* */
	
	/// Writes the environment and its contents to an output stream.
	/// \param os The stream to write to
	/// \param env The environment to write
	/// \return The ouput stream
	friend std::ostream& operator<<(std::ostream& os, Environment const& env);
	
	/// Loads an environment and its contents from an input stream.
	/// \param is The stream to read from
	/// \param env The environment to read into
	/// \return The input stream
	friend std::istream& operator>>(std::istream& is, Environment & env);
	
	/* ******* SIMULATION FUNCTIONS ******* */
	
	/// Renders each of the environment's components. Call this from a 
	/// thread in which you opened a Viewport.
	void draw();
	
	/// Tells each of the environment's components to update their state by another
	/// time step, by calling their own simulate() functions. Any component that returns
	/// "false" from simulate() gets automatically removed from the environment and deleted.
	/// \param ticks The number of (virtual) seconds elapsed since the last call to simulate()
	void simulate(float ticks);
	
private:
	std::string _generateName(std::string const& proposedName);  // constructs a unique name

	// environment boundaries
	Vector bottomLeftBound;  
	Vector topRightBound;
	
	EnvironmentMap typeDirectory;   // lookup table for class IDs
	ObjectMap nameDirectory;		// lookup table for names
	NameMap nameList;				// helps keep track of which names are in use
  
};

/**
 * A trivial Object subclass, used in the Environment class's doctests. Simply writes to 
 * stdout to report when its draw() and simulate() functions are called. Has a single 
 * Object* member to test the Environment's reference resolving functionality.
 */
class TestObject: public Object
{
public:

	/// The constructor just sets the reference to NULL.
	TestObject();
	
	/// \return "TestObject"
	virtual std::string getClassID() const;
	
	/// Prints a message to stdout, with the object's name.
	virtual void draw();
	
	/// Prints a message to stdout, with the object's name.
	/// \return True if ticks < 1.0, False otherwise
	virtual bool simulate(float ticks);   
		
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a TestObject
	/// \return A TestObject*, pointing to the same object as source.	
	static TestObject* downcast(Object* source);

	Object* reference;  ///< to be used to refer to another toplevel object in the Environment
protected:

	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);
};

}

#endif /*ENVIRONMENT_H_*/
