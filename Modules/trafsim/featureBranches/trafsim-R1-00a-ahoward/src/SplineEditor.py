from libtrafsim import *
from SuperDynamic import SuperDynamic

class SplineEditor(SuperDynamic):
    def __init__(self, environment):
        SuperDynamic.__init__(self)
        
        self.environment = environment
        self.selectedSpline = None
        self.selectedPoint = None
        self.state = ['none']
        
        self.enabled = True
        
    def _switchSpline(self, newSpline):
        if self.selectedSpline:
            self.selectedSpline.color = Color(1, 1, 1, 1)
        
        newSpline.color = Color(1, 0, 0, 1)
        self.selectedSpline = newSpline
        
    def _selectSplineAt(self, pos, threshold = 30.0):
        closestPoint = Vector(-1000000, -1000000)
        closestSpline = None
        closestIndex = None
        
        for spline in self.environment.getObjectsByClassID('Spline').itervalues():
            spline = Spline.downcast(spline)
            
            for point, index in zip( spline.points, xrange(spline.points.size()) ):
                if Vector.dist2(pos, point) < Vector.dist2(pos, closestPoint):
                    closestPoint = point
                    closestSpline = spline
                    closestIndex = index
    
        if Vector.dist2(pos, closestPoint) < threshold:
            return closestSpline, closestIndex
        else:
            return None, None
        
    def onMouseMotion(self, event):
        if not self.enabled: return
        if self.state[-1] == 'drag':
            self.selectedSpline.points[ self.selectedPoint ] = event.pos
            
            if self.selectedSpline.isCached():
                self.selectedSpline.cacheData(True)

    def onMouseClick(self, event):
        if not self.enabled or event.button != LEFT: return
        if self.state[-1] == 'create':
            newSpline = Spline( VectorArray( [event.pos] ) )
            self.environment.addObject(newSpline)
            
            self._switchSpline(newSpline)
            self.selectedPoint = 0
            self.state.append('drag')
        
        elif self.state[-1] == 'append':
            if not self.selectedSpline:
                self.state.remove('append')
                return
        
            self.selectedSpline.points.push_back( event.pos )
            self.selectedPoint = self.selectedSpline.points.size() - 1
            self.state.append('drag')
            
            if self.selectedSpline.isCached():
                self.selectedSpline.cacheData(True)
                
        elif self.state[-1] == 'delete':
            newSpline, self.selectedPoint = self._selectSplineAt(event.pos)
            if newSpline != None:
                self._switchSpline(newSpline)
            
            if 'create' in self.state:
                if self.selectedSpline:    
                    self.environment.removeObject(self.selectedSpline.getName())
                    self.selectedSpline = None
                    self.selectedPoint = None
                    
            elif 'append' in self.state:
                if self.selectedPoint:            
                    del self.selectedSpline.points[ self.selectedPoint ]
                    if self.selectedPoint >= self.selectedSpline.points.size():
                        self.selectedPoint = self.selectedSpline.points.size() - 1
            
        elif self.state[-1] == 'none':
            newSpline, self.selectedPoint = self._selectSplineAt(event.pos)
            
            if newSpline != None:
                self._switchSpline(newSpline)
                self.state.append('drag')
                
            else:
                print event.pos
                
    def onMouseUnclick(self, event):
        if not self.enabled or event.button != LEFT: return
        try: 
            self.state.remove('drag')
        except: pass    
        
    def onKeyDown(self, event):
        if event.button in ( SDLK_LCTRL, SDLK_RCTRL ):
            self.state.append('create')
        elif event.button in ( SDLK_LSHIFT, SDLK_RSHIFT ):
            self.state.append('append')
        elif event.button in ( SDLK_d, ):
            self.state.append('delete')
        elif event.button in ( SDLK_z, ):
            self.enabled = False
     
    def onKeyUp(self, event):
        try:
            if event.button in ( SDLK_LCTRL, SDLK_RCTRL ):
                self.state.remove('create')
            elif event.button in ( SDLK_LSHIFT, SDLK_RSHIFT ):
                self.state.remove('append')
            elif event.button in ( SDLK_d, ):
                self.state.remove('delete')
            elif event.button in ( SDLK_z, ):
                self.enabled = True
        except: pass