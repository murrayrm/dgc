#include "Environment.hh"
#include <sstream>
#include <cassert>

namespace TrafSim
{
	
  /* ******* CLASS ENVIRONMENT ******* */	
	
  /* ******* CONSTRUCTORS AND DESTRUCTORS ******* */	


  Environment::Environment() : CSkynetContainer(MODmapping,atoi(std::getenv("SKYNET_KEY"))),
			       bottomLeftBound(0.0f, 0.0f),
			       topRightBound(100.0f, 100.0f) { 
    skynetKey = atoi(std::getenv("SKYNET_KEY"));
    std::cout << "Constructing skynet through default constructor with KEY = " <<skynetKey << std::endl;
    debug = false;
    paused = false;
    xTranslate = 0;
    yTranslate = 0;
    xInit = 0;
    yInit = 0;
    subgroup = 1;

}

  Environment::Environment(int skynet_key) : 
    CSkynetContainer(MODmapping,skynet_key),
    bottomLeftBound(0.0f, 0.0f),
    topRightBound(100.0f, 100.0f)
  { 
    // store the skynet key so we can use it in ExternalCar, etc
    skynetKey = skynet_key;
    std::cout << "Constructing skynet with KEY = " << skynetKey << std::endl;
    debug = false;
    paused = false;
    xTranslate = 0;
    yTranslate = 0;
    xInit = 0;
    yInit = 0;
    subgroup = 1;
  }

  Environment::~Environment()
  {
    if (debug) cout<<"Environment destructor"<<endl;
    clearObjects();
    if (debug) cout<<"end Environment destructor"<<endl<<endl;
  }



  /* ******* DIRECTORY FUNCTIONS ******* */

  std::string const& Environment::addObject(Object* object, std::string const& proposedName, bool addBoundary)
  {
    // generate a unique name for the object
    if (proposedName == "")
      object->name = _generateName(object->getClassID());
    else
      object->name = _generateName(proposedName);
		
    if (debug) cout<<"Storing "<<object->name<<endl;
    // store the object in both directories
    typeDirectory[object->getClassID()][object->name] = object;
    nameDirectory[object->name] = object;

    if (object->getClassID() == "Spline") {
      // change the color
      Spline* s = static_cast<Spline*>(object);
      s->color.r = .4;
      s->color.g = .4;
      s->color.b = .4;
    }

    // get the object's boundary and store in the road markings directory
    if (addBoundary) {
      if (object->getClassID() == "Lane" || object->getClassID() == "Line") {
	if (debug) std::cout<<"adding "<<object->name<< " to road markings dir"<<endl;
	std::vector<vector<point2> > boundary = object->getBoundary();
	
	if (debug) std::cout<<"got boundary.. translating points.."<<endl;

	// translate the points
	updateAliceState();
	for(vector<vector<point2> >::iterator it1 = boundary.begin();
	    it1 != boundary.end(); it1++) {
	  if (debug) cout<<"next spline"<<endl;
	  for (vector<point2>::iterator it2 = it1->begin();
	       it2 != it1->end(); it2++) {
	    
	    *it2 = toLocal(*it2);

	    if (debug) cout<<it2->x<<" "<<it2->y<<";"<<endl;
	  }
	}
	roadMarkings[object->name] = boundary;
      }
    }
    if (debug) std::cout<<"done"<<endl;

    return object->name;
  }

  void Environment::storeIntersectionBoundaries()
  {
    ObjectMap xInter = getObjectsByClassID("XIntersection");
    ObjectMap yInter = getObjectsByClassID("YIntersection");

    for (ObjectMap::iterator it = xInter.begin(); it != xInter.end(); it++) {

      if (debug) std::cout<<"adding "<<it->first<< " to road markings dir"<<endl;
      std::vector<std::vector<point2> > boundary = it->second->getBoundary();
    
      if (debug) std::cout<<"got boundary.. translating points.."<<endl;

      // translate the points
      updateAliceState();
      for(vector<vector<point2> >::iterator it1 = boundary.begin();
	  it1 != boundary.end(); it1++) {
	for (vector<point2>::iterator it2 = it1->begin();
	     it2 != it1->end(); it2++) {

	  *it2 = toLocal(*it2);

	  if (debug) cout<<it2->x<<" "<<it2->y<<";"<<endl;
	}
      }
      roadMarkings[it->first] = boundary;

    }
    for (ObjectMap::iterator it = yInter.begin(); it != yInter.end(); it++) {

      if (debug) std::cout<<"adding "<<it->first<< " to road markings dir"<<endl;
      std::vector<std::vector<point2> > boundary = it->second->getBoundary();
    
      if (debug) std::cout<<"got boundary.. translating points.."<<endl;

      // translate the points
      updateAliceState();
      for(std::vector<std::vector<point2> >::iterator it1 = boundary.begin();
	  it1 != boundary.end(); it1++) {
	for (vector<point2>::iterator it2 = it1->begin();
	     it2 != it1->end(); it2++) {

	  *it2 = toLocal(*it2);

	  if (debug) cout<<it2->x<<" "<<it2->y<<";"<<endl;
	}
      }
      roadMarkings[it->first] = boundary;
    }
	
  }

  bool Environment::removeObject(std::string const& objName, bool canDelete)
  {
    if (debug) cout<<"Environment::removeObject -- "<<objName<<endl;
    Object* object = nameDirectory[objName];
  
    if (!object)
      {
	nameDirectory.erase(objName);
	return false;
      }
		
    // erase the object from both directories
    nameDirectory.erase(objName); if (debug) std::cout<<"object erased from name dir"<<endl;
    // debug note: i think when it tries to run object->getClassID(), there
    // isn't really an object there. that's where there's a segfault
    typeDirectory[object->getClassID()].erase(objName); if (debug) std::cout<<"object erased from type dir"<<endl;	

    if (canDelete) {
      delete object;
      if (debug) std::cout<<"object "<<objName<<" deleted"<<endl;	  
    }
    //	debug = true;
    if (debug) cout<<"about to return..."<<endl;
    return true;
  }

  std::string Environment::renameObject(std::string const& oldName, std::string const& newName)
  {
    Object* object = nameDirectory[oldName];
	
    if (!object)
      {
	nameDirectory.erase(oldName);
	return "";
      }

    removeObject(oldName, false);
    addObject(object, newName);
	
    return object->getName();
  }

  Object* Environment::getObject(std::string const& objName)
  {
    Object* result = nameDirectory[objName];
	
    if (!result)
      nameDirectory.erase(objName);
		
    return result;
  }

  ObjectMap const& Environment::getObjectsByClassID(std::string const& classID)
  {
    return typeDirectory[classID];
  }


  void Environment::clearObjectsByClassID(std::string const& classID,
					  bool canDelete)
  {
    ObjectMap *objMap = &typeDirectory[classID];
    for (ObjectMap::iterator i = objMap->begin();
	 i != objMap->end();
	 ++i)
      {
	nameDirectory.erase(i->second->getName());
	if(canDelete)
	  delete i->second;
      }
    typeDirectory.erase(classID);
  }

  void Environment::clearObjects(bool canDelete)
  {
    if (debug) cout<<"Environment::clearObjects()"<<endl;
    if (canDelete)
      {
	for (ObjectMap::iterator i = nameDirectory.begin();
	     i != nameDirectory.end();
	     ++i) {
	
	  if (i->second)
	    delete i->second;
	}
	
      }
	
    // the following only deletes pointers
    nameDirectory.clear();
    typeDirectory.clear();
    nameList.clear();
  
    if (debug) cout<<"end Environment::clearObjects"<<endl;
  }

  /* ******* REFERENCE FRAME FUNCTIONS ******* */

  Vector const& Environment::getBottomLeftBound() const
  {
    return bottomLeftBound;
  }
	
  Vector const& Environment::getTopRightBound() const
  {
    return topRightBound;
  }

  void Environment::setBounds(Vector const& bottomLeft, Vector const& topRight)
  {
    bottomLeftBound = bottomLeft;
    topRightBound = topRight;
	
    assert(bottomLeftBound.x < topRightBound.x &&
	   bottomLeftBound.y < topRightBound.y);
  }

  /* ******* SIMULATION FUNCTIONS ******* */
	
  void Environment::draw()
  {
    for (ObjectMap::iterator i = nameDirectory.begin();
	 i != nameDirectory.end();
	 ++i)
      {
	i->second->draw();
      }
  }

  void Environment::simulate(float ticks)
  {
    
    // if an object's simulate() function returns False,
    // that means it's asking to be deleted.
    for (ObjectMap::iterator i = nameDirectory.begin();
	 i != nameDirectory.end();)
      {
	Object* object = (i++)->second;

	if (!object->simulate(ticks)) {
	  if (debug) cout<<"simulate returned false"<<endl;
	  removeObject(object->name);
	}

		  
      }
    
    // send appropriate environment objects to the mapper
    // don't automatically send if we're in debug mode
    if (!debug) exportEnv();
  }

  void Environment::exportEnv()
  {
    updateAliceState();

    if (debug) {
      cout<<"Exporting environment information: "<<endl;
      cout<<"global->local x: "<<xInit<<endl;
      cout<<"global->local y: "<<yInit<<endl;
    }
    exportRoadLines();
    exportCars();
    exportObstacles();
  }

  void Environment::updateAliceState()
  {
    UpdateState();
    aliceState = m_state;
    xInit = m_state.utmNorthing - m_state.localX;
    yInit = m_state.utmEasting - m_state.localY;
  }

  point2 Environment::toLocal(point2 p)
  {
    double x = p.x;
    double y = p.y;

    if (debug) {
      cout<<"x (trafsim): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    // convert from trafsim frame to global
    x += xTranslate;
    y += yTranslate;
	
    if (debug) {
      cout<<"x (global): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }
    
    // switch x and y
    double temp = x;
    x = y;
    y = temp;

    if (debug) {
      cout<<"x (global, switched): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    // convert from global to local (alice)
    x -= xInit;
    y -= yInit;

    if (debug) {
      cout<<"x (alice local): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    return point2(x,y);
  }

  void Environment::exportCars()
  {

    // export all of the car objects
    ObjectMap cars = getObjectsByClassID("Car");

    MapElement obj;
    int name;
    string strName;
    double angle, length, width;
    point2 pos;
    int bytesSent;
    for (ObjectMap::iterator i = cars.begin(); i != cars.end();)
      {
	// reset everything
	obj.clear();
	bytesSent = 0;
	name = 0;
	strName = "";
	angle = 0;
	length = 0;
	width = 0;
	pos.x = 0;
	pos.y = 0;
	
	// get the car
	Object* object = (i++)->second;

	// get the name
	strName = object->getName();
	name = int(strName.c_str());

	// need to export alice in a special way
	if (strName == "Alice") {

	  obj.setTypeAlice();
    obj.setState(aliceState);
	  initSendMapElement(skynetKey);
	  bytesSent = 0;
	  bytesSent = sendMapElement(&obj, subgroup);
	
	  if (debug) {
	    std::cout<<"Sending Alice:"<<endl;
	    std::cout<<"  bytes sent = " << bytesSent << endl;
	    std::cout<<"  alice pos = ( "<<aliceState.utmNorthing<<" , "<<aliceState.utmEasting<<" )"<<endl<<endl;
	  }

	}

	// all other vehicles
	else {

	
	  // get car properties
	  length = object->getLength();
	  width = object->getWidth();
	  angle = object->getOrientation();
	
	  // get car position
	  pos = object->getCenter();

	  // translate to local coords
	  pos = toLocal(pos);

	  // set the velocity
	  double velocity = object->getVelocity();
	  double theta = object->getOrientation();
	  obj.velocity.x = velocity * cos(theta);
	  obj.velocity.y = velocity * sin(theta);

	  // initialize the object
	  // set teh name
	  obj.setId(name);
    obj.setGeometry(pos,length,width,angle);
	  //obj.set_block_obs(obj.id.arr, pos, angle, length, width);
	  obj.setState(aliceState);
	  obj.setTypeObstacle();
	  obj.height = 1;

	  // send the object
	  initSendMapElement(skynetKey);
	  bytesSent = 0;
	  bytesSent = sendMapElement(&obj, subgroup);
	
	  if (debug) {
	    std::cout<<"Sending Car object:"<<endl;
	    std::cout<<"  bytes sent = " << bytesSent << endl;
	    std::cout<<"  object id = " <<obj.id.front() <<endl;
	    std::cout<<"  Car position: ( "<<pos.x<<","<<pos.y<<" )"<<endl;
	  }
	}
     
      }

  }

void Environment::exportObstacles()
  {

    // grab all the obstacles
    ObjectMap obstacles = getObjectsByClassID("Obstacle");

    MapElement obj;
    int name;
    string strName;
    int bytesSent;
    for (ObjectMap::iterator i = obstacles.begin(); i != obstacles.end();)
      {
	// reset everything
	obj.clear();
	bytesSent = 0;
	name = 0;
	strName = "";
	
	// get the obstacle
	Object* object = (i++)->second;

	// get the name
	strName = object->getName();
	name = int(strName.c_str());

	// set teh name
	obj.setId(name);
	
	// get the boundary
	vector<point2> boundary = object->getBoundary().front();

	// translate to local coords
	for (vector<point2>::iterator it = boundary.begin();
	     it != boundary.end(); it++) {
	  *it = toLocal(*it);
	}
	
	// initialize the object
        obj.setGeometry(boundary);
	//obj.set_poly_obs(obj.id.arr,boundary);
	obj.setState(aliceState);
        obj.setTypeObstacle();
        obj.height = 1;
	
	// send the object
	initSendMapElement(skynetKey);
	bytesSent = 0;
	bytesSent = sendMapElement(&obj, subgroup);
	
	if (debug) {
	  std::cout<<"Sending static obstacle:"<<endl;
	  std::cout<<"  bytes sent = " << bytesSent << endl;
	  std::cout<<"  object id = " <<obj.id.front() <<endl;
	  std::cout<<"  Obstacle position: ( "<<boundary.front().x<<","<<boundary.front().y<<" )"<<endl;
	}
     
      }

  }

  void Environment::exportRoadLines()
  {
    // send all of the road lines
    MapElement obj;
    int name;
    int bytesSent;
    int laneNumber;

    for (std::map<std::string, std::vector<std::vector<point2> > >::iterator it = roadMarkings.begin();
	 it != roadMarkings.end(); it++) {

      laneNumber = 1;
      
      vector <vector< point2 > > pts = (*it).second;

      
      for (std::vector<std::vector<point2> >::iterator it2 = pts.begin();
           it2 != pts.end(); it2++) {

	obj.clear();
	bytesSent = 0;
	name = 0;

	name = int(it->first.c_str()) + laneNumber;
	// convert string to an integer
	obj.setId(name);

	laneNumber++;

	// initialize the object
	obj.setTypeLaneLine();
	obj.setGeometry(*it2);
	obj.setState(aliceState);

	// send the object
	initSendMapElement(skynetKey);
	bytesSent = 0;
	bytesSent = sendMapElement(&obj, subgroup);
	
	if (debug) {
	  std::cout<<"Sending road line object:"<<endl;
	  std::cout<<"  bytes sent = " << bytesSent << endl;
	  std::cout<<"  object id = " <<name <<endl;
	  std::cout<<"  first point = ( "<<it2->front().x<< " , "<<it2->front().y<<" ) "<<endl;
	}
      }
    }

  }


  /* ******* PRIVATE FUNCTIONS ******* */

  std::string Environment::_generateName(std::string const& proposedName)
  {
    if (nameList.find(proposedName) == nameList.end())
      {
	nameList[proposedName] = 1;
	return proposedName;
      }
	
    // if "name" is in use, return "name___n", where "n" is an integer
    std::ostringstream ostr;
    ostr << proposedName << "___" << nameList[proposedName]++;
	
    return ostr.str();
  }

  /* ******* SERIALIZATION FUNCTIONS ******* */

  std::ostream& operator<<(std::ostream& os, Environment const& env)
  {
    os.precision(16);
    return os << "Environment( " << std::endl << 
      "bottomLeftBound --> " << env.bottomLeftBound << std::endl <<
      "topRightBound --> " << env.topRightBound << std::endl <<
      "xTranslation --> " << env.xTranslate << std::endl <<
      "yTranslation --> " << env.yTranslate << std::endl <<
      "xInit --> " << env.xInit << std::endl <<
      "yInit --> " << env.yInit << std::endl <<
      "nameDirectory --> " << env.nameDirectory << std::endl <<
      "nameList --> " << env.nameList << std::endl <<
      " )";	
  }

  std::istream& operator>>(std::istream& is, Environment & env)
  {
    is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
    try
      {
	matchString(is, "Environment");
	matchString(is, "(");
	DESERIALIZE_RN(is, "bottomLeftBound", env.bottomLeftBound);
	DESERIALIZE_RN(is, "topRightBound", env.topRightBound);
	DESERIALIZE_RN(is, "xTranslation", env.xTranslate);
	DESERIALIZE_RN(is, "yTranslation", env.yTranslate);
	DESERIALIZE_RN(is, "xInit", env.xInit);
	DESERIALIZE_RN(is, "yInit", env.yInit);
	DESERIALIZE_RN(is, "nameDirectory", env.nameDirectory);
	DESERIALIZE_RN(is, "nameList", env.nameList);
	matchString(is, ")");
      }
    catch(ParseError& e)
      {
	e.info = "Failed to parse Environment:\n" + e.info;
	throw;
      }
    catch(std::exception& e)
      {
	throw ParseError("Failed to parse Environment:\n " + std::string(e.what()) );
      }
	
    env.typeDirectory.clear();
	
    for (ObjectMap::iterator i = env.nameDirectory.begin();
	 i != env.nameDirectory.end();
	 ++i)
      {
	Object* object = i->second;	
	object->name = i->first;    // reload the object's name
		
	// store the object in the classID index as well
	env.typeDirectory[object->getClassID()][object->name] = object;
		
	// replace string references with actual pointers
	object->_resolveDependencies(env);
      }
	
    return is;	
  }
		
}
