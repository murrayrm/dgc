import time
from libtrafsim_Py import *

from threading import Thread                        
from SuperDynamic import SuperDynamic
from SplineEditor import SplineEditor
from CarDriver import CarDriver
from Queue import Queue

class GenericApp(SuperDynamic, Thread):
    def __init__(self):
        Thread.__init__(self)
        SuperDynamic.__init__(self)
	
	# get the skynet key from the environment
	# TODO: add some error checking, in case SKYNET_KEY is not set
	import os
	skynetKey = 0
	skynetKey = int(os.environ['SKYNET_KEY'])
        
        self.viewport = Viewport()
        self.environment = Environment(skynetKey)
        self.editor = SplineEditor(self.environment)
        
        self.callQueue = Queue()
        self.timingFactor = 10.0

    def runTest(self):
	spline = self.editor.selectedSpline
	road = Road()
	self.environment.addObject(road, 'MyRoad')
	road.initialize(self.environment,spline,1,2,5.0)
	c = Car(1)
	self.environment.addObject(c, 'Car')	
    	c.switchLane(road,1,0.6)
	c2 = Car(1)
	self.environment.addObject(c2, 'Car')
	c2.switchLane(road,1,0.1)
	c3 = Car()
	self.timingFactor = .1;
	self.environment.addObject(c3, 'Car')
	c3.switchLane(road,0,0.0)

    def save(self, filename):
        f = open(filename, 'w')
        f.write(str(self.environment))
        f.close()
        
    def load(self, filename):
        f = open(filename)
        self.environment = Environment.parse(f.read())
        self.editor.environment = self.environment
        f.close()
        
    def run(self):
        self.viewport.open(800, 800, 'trafsim')
        
        lastTicks = time.time()
        
        try:
            while 1:
                event = ViewportEvent()
                while self.viewport.pollEvents(event):
                    if event.type == QUIT:
                        return
                    else:
                        { EMPTY            : lambda event: None, 
                          REPAINT          : lambda event: None, 
                          MOUSEMOTION      : self.editor.onMouseMotion,
                          MOUSEBUTTONUP    : self.editor.onMouseUnclick,
                          MOUSEBUTTONDOWN  : self.editor.onMouseClick,
                          KEYUP            : self.editor.onKeyUp,
                          KEYDOWN          : self.editor.onKeyDown,
                         }[event.type](event) 
                 
                # call the functions passed into the thread
                while not self.callQueue.empty():
                    fn, args, kwds = self.callQueue.get()
                    fn(*args, **kwds)
                
                # update time
                newTicks = time.time()
                ticks = newTicks - lastTicks
                lastTicks = newTicks
             
                try: self.editor.perFrame(ticks * self.timingFactor)
                except AttributeError: pass
        
                self.environment.simulate(ticks * self.timingFactor)
                self.environment.draw()
            
                self.viewport.repaint()
        finally:
            # unload the graphics in advance, just in case
            del self.viewport
        
    def call(self, fn, *args, **kwds):
        self.callQueue.put( (fn, args, kwds) )

if __name__ == '__main__':
    import GenericApp
    
    app = GenericApp.GenericApp()
    app.start()

            
    