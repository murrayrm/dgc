#include "Obstacle.hh"
#include <cmath>
#include <GL/gl.h>
#include <iostream>
#include <time.h>

namespace TrafSim {

  Obstacle::Obstacle() 
  {
    position = point2(0,0);
    length = 2;
    width = 2;
    orientation = 0;
    setBlockObs();
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;

    srand((unsigned)time(0));
  }
  
  Obstacle::Obstacle(float x, float y, float l, float w, float o, bool v)
    : orientation(o), length(l), width(w)
  {
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    setPosition(x,y);
    setBlockObs();
    if (v)
      setVegetation();
  }

  Obstacle::Obstacle(float x, float y, float radius) 
    : orientation(0), length(radius), width(radius)
  {
    type = CIRCLE;
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    setPosition(x,y);
    setCircleObs();
  }
    

  Obstacle::~Obstacle() 
  {

  }

  void Obstacle::setPosition(float x, float y)
  {
    point2 oldPos = position;
    position.x = x;
    position.y = y;
    posDiff = position - oldPos;

  }

  void Obstacle::setPosition(point2 pos)
  {
    posDiff = position - pos;
    position = pos;
  }

  void Obstacle::setOrientation(float newOrientation)
  {
    // if the desired orientation isn't between -pi and pi,
    // fix it
    while (newOrientation > M_PI)
      newOrientation -= 2*M_PI;
    while (newOrientation < -1*M_PI)
      newOrientation += 2*M_PI;
  
    orientation = newOrientation;

    // need to recalculate boundaries
    if (type == BLOCK)
      setBlockObs();
    else if (type == CIRCLE) 
      setCircleObs();
    else { // vegetation
      setBlockObs();
      setVegetation();
    }
  }

  void Obstacle::setBlockObs()
  {
    type = BLOCK;
    boundary.clear();

    double ly = width;
    double lx = length;
    double t = orientation;
      
    // put in the points without rotation
    boundary.push_back(point2(.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,.5*ly));
    boundary.push_back(point2(-.5*lx,-.5*ly));
    boundary.push_back(point2(.5*lx,-.5*ly));
      
    // rotate points
    double ca = cos(t);
    double sa = sin(t);
    for (std::vector<point2>::iterator iter = boundary.begin();
	 iter != boundary.end(); iter++)
      *iter = point2(iter->x*ca - iter->y*sa,
		     iter->x*sa + iter->y*ca);

    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;    

  }

  void Obstacle::setCircleObs()
  {
    type = CIRCLE;
    boundary.clear();

    double a = cos(M_PI/4);
    double r = length;
    boundary.push_back(point2(r,0));
    boundary.push_back(point2(r*a,r*a));
    boundary.push_back(point2(0,r));
    boundary.push_back(point2(-r*a,r*a));
    boundary.push_back(point2(-r,0));
    boundary.push_back(point2(-r*a,-r*a));
    boundary.push_back(point2(0,-r));
    boundary.push_back(point2(r*a,-r*a));

    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;    

  }

  void Obstacle::setVegetation()
  {
    type = VEGETATION;

    // take the original boundary and add noise to it

    vector<point2> newBoundary;
    
    vector<point2>::iterator nextpt;
    for (vector<point2>::iterator currpt = boundary.begin();
	 currpt != boundary.end(); currpt++) {
      nextpt = currpt+1;
      newBoundary.push_back(*currpt);
      if (nextpt != boundary.end()) 
	newBoundary.push_back(point2( (currpt->x+nextpt->x)/2 ,
				      (currpt->y+nextpt->y)/2));      
    }    


    boundary = newBoundary;

    double minNoise = 0.1;
    double rangeNoise = 1;
    double noise = .5;

    for (vector<point2>::iterator it = boundary.begin();
	 it != boundary.end(); it++) {
      noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);
      it->x = it->x + noise;
      noise = minNoise + rangeNoise*rand()/(RAND_MAX + 1.0);
      it->y = it->y + noise;
    }

    color.r = 0.0;
    color.g = 1.0;
    color.b = 0.0;
    color.a = 1.0;

  }

  void Obstacle::draw() 
  {
    // draw a light blue object
    
    glPushMatrix();
    //    glTranslatef(position.x, position.y, 0.0f);
    //    glRotatef(orientation / 3.14159f * 180.0f, 0.0f, 0.0f, 1.0f);
    glSetColor(color);

    glBegin(GL_POLYGON);

    for (vector<point2>::iterator it = boundary.begin();
	 it != boundary.end(); it++) {	
      glVertex3f(it->x, it->y, 0.0f);      
    }

    glEnd();
    glPopMatrix();
  }

  bool Obstacle::invertColor() {
    color.r = 1.0f - color.r;
    color.b = 1.0f - color.b;
    return true;
  }

  bool Obstacle::simulate(float ticks) 
  {
    // translate!
    if (posDiff.x != 0 || posDiff.y != 0) {
      for (vector<point2>::iterator it = boundary.begin();
	   it != boundary.end(); it++) {	
	*it = *it + posDiff;
      }
    }
    posDiff = point2(0,0);
    return true;
  }

  vector<vector<point2> > Obstacle::getBoundary()
  {
    vector<vector<point2> > boundaryWrapper;
    boundaryWrapper.push_back(boundary);
    return boundaryWrapper;
  }

  Obstacle* Obstacle::downcast(Object* source)
  {
    return dynamic_cast<Obstacle*>(source);
  }

  void Obstacle::serialize(std::ostream& os) const
  {
    // NOT IMPLEMENTED
  }

  void Obstacle::deserialize(std::istream& is)
  {
    // NOT IMPLEMENTED
  }

}
