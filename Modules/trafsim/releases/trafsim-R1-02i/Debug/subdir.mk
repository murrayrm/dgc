################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Car.cc \
../CarFactory.cc \
../CarProperties.cc \
../Color.cc \
../CurveConstraint.cc \
../Environment.cc \
../ExternalCar.cc \
../IDM.cc \
../Intersection.cc \
../Lane.cc \
../LaneGrid.cc \
../Object.cc \
../Road.cc \
../Serialization.cc \
../Spline.cc \
../Vector.cc \
../Viewport.cc \
../XIntersection.cc \
../YIntersection.cc \
../libtrafsim_wrap.cc

#CXX_SRCS += 

OBJS += \
./Car.o \
./CarFactory.o \
./CarProperties.o \
./Color.o \
./CurveConstraint.o \
./Environment.o \
./ExternalCar.o \
./IDM.o \
./Intersection.o \
./Lane.o \
./LaneGrid.o \
./Object.o \
./Road.o \
./Serialization.o \
./Spline.o \
./Vector.o \
./Viewport.o \
./XIntersection.o \
./YIntersection.o \
./libtrafsim_wrap.o 

CPP_DEPS += \
./Car.d \
./CarFactory.d \
./CarProperties.d \
./Color.d \
./CurveConstraint.d \
./Environment.d \
./ExternalCar.d \
./IDM.d \
./Intersection.d \
./Lane.d \
./LaneGrid.d \
./Object.d \
./Road.d \
./Serialization.d \
./Spline.d \
./Vector.d \
./Viewport.d \
./XIntersection.d \
./YIntersection.d \
./libtrafsim_wrap.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	$(GXX) -D_REENTRANT -I/usr/include/python2.4 -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
