#include "Environment.hh"
#include <sstream>
#include <cassert>
#include <math.h>
#include <time.h>
#include <GL/gl.h>
#include <GL/glut.h>

namespace TrafSim
{
	
  /* ******* CLASS ENVIRONMENT ******* */	
	
  /* ******* CONSTRUCTORS AND DESTRUCTORS ******* */	


  Environment::Environment(bool initGlut) : CSkynetContainer(MODmapping,atoi(std::getenv("SKYNET_KEY"))),
			       bottomLeftBound(0.0f, 0.0f),
			       topRightBound(100.0f, 100.0f) { 
    skynetKey = atoi(std::getenv("SKYNET_KEY"));
    std::cout << "Constructing skynet through default constructor with KEY = " <<skynetKey << std::endl;
    debug = false;
    paused = false;
    trafficPaused = false;
    makingScenario = false;
    speedUpTraffic = false;
    slowDownTraffic = false;
    resumeTraffic = false;
    xTranslate = 0;
    yTranslate = 0;
    xInit = 0;
    yInit = 0;
    subgroup = 0;
    currentTime = 0;
    initSendMapElement(skynetKey);

    aliceLocation = point2(0,0);

    // get the time/date for log filename
    char tempFilename[30];
    time_t t = time (NULL);
    tm *local;
    local = localtime(&t);
    
    sprintf(tempFilename, "logs/trafsim-%04d-%02d-%02d-%02d-%02d", 
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min);

    logFileNameBase = tempFilename;

    // initialize glut -- can only do this once
    if (initGlut) {
      int argcint = 0; int* argc = &argcint; // hack
      char argvchar = 'h'; char* argv[1] = {&argvchar}; // hack
      glutInit(argc, argv);
    }
  }

  Environment::Environment(int skynet_key, bool initGlut) : 
    CSkynetContainer(MODmapping,skynet_key),
    bottomLeftBound(0.0f, 0.0f),
    topRightBound(100.0f, 100.0f)
  { 
    // store the skynet key so we can use it in ExternalCar, etc
    skynetKey = skynet_key;
    std::cout << "Constructing skynet with KEY = " << skynetKey << std::endl;
    debug = false;
    paused = false;
    trafficPaused = false;
    makingScenario = false;
    speedUpTraffic = false;
    slowDownTraffic = false;
    resumeTraffic = false;
    xTranslate = 0;
    yTranslate = 0;
    xInit = 0;
    yInit = 0;
    subgroup = 0;
    currentTime = 0;
    initSendMapElement(skynetKey);

    aliceLocation = point2(0,0);

    // get the time/date for log filename
    char tempFilename[30];
    time_t t = time(NULL);
    tm *local;
    local = localtime(&t);

    sprintf(tempFilename, "logs/trafsim-%04d-%02d-%02d-%02d-%02d", 
	    local->tm_year+1900, local->tm_mon+1, local->tm_mday,
	    local->tm_hour, local->tm_min);

    logFileNameBase = tempFilename;

    // initialize glut -- can only do this once
    if (initGlut) {
      int argcint = 0; int* argc = &argcint; // hack
      char argvchar = 'h'; char* argv[1] = {&argvchar}; // hack
      glutInit(argc, argv);
    }
  }

  Environment::~Environment()
  {
    closeLogs();
    
    if (debug) cout<<"Environment destructor"<<endl;
    clearObjects();
    if (debug) cout<<"end Environment destructor"<<endl<<endl;
  }



  /* ******* DIRECTORY FUNCTIONS ******* */

  std::string const& Environment::addObject(Object* object, std::string const& proposedName, bool addBoundary)
  {
    // generate a unique name for the object
    if (proposedName == "")
      object->name = _generateName(object->getClassID());
    else
      object->name = _generateName(proposedName);
		
    if (debug) cout<<"Storing "<<object->name<<endl;
    // store the object in both directories
    typeDirectory[object->getClassID()][object->name] = object;
    nameDirectory[object->name] = object;

    if (object->getClassID() == "Spline") {
      // change the color
      Spline* s = static_cast<Spline*>(object);
      s->color.r = .4;
      s->color.g = .4;
      s->color.b = .4;
    }

    // get the object's boundary and store in the road markings directory
    if (addBoundary) {
      if (object->getClassID() == "Lane" || object->getClassID() == "Line") {
	if (debug) std::cout<<"adding "<<object->name<< " to road markings dir"<<endl;
	std::vector<vector<point2> > boundary = object->getBoundary();
	
	if (debug) std::cout<<"got boundary.. translating points.."<<endl;

	// translate the points
	updateAliceState();
	for(vector<vector<point2> >::iterator it1 = boundary.begin();
	    it1 != boundary.end(); it1++) {
	  if (debug) cout<<"next spline"<<endl;
	  for (vector<point2>::iterator it2 = it1->begin();
	       it2 != it1->end(); it2++) {
	    
	    *it2 = toLocal(*it2);

	    if (debug) cout<<it2->x<<" "<<it2->y<<";"<<endl;
	  }
	}
	roadMarkings[object->name] = boundary;
      }
    }

    /*
    // open a new log if it's a car
    if (object->getClassID() == "Car" || object->getClassID() == "SplineCar") {
      openNewLog(object->getName());
    }

    // write a log file saying how many logs there were for this run
    ofstream output;
    string temp = logFileNameBase + ".log";
    output.open(temp.c_str());
    output<<openLogs.size()<<endl;
    output.close();
    */

    if (debug) std::cout<<"done"<<endl;

    return object->name;
  }

  void Environment::storeIntersectionBoundaries()
  {
    ObjectMap xInter = getObjectsByClassID("XIntersection");
    ObjectMap yInter = getObjectsByClassID("YIntersection");

    for (ObjectMap::iterator it = xInter.begin(); it != xInter.end(); it++) {

      if (debug) std::cout<<"adding "<<it->first<< " to road markings dir"<<endl;
      std::vector<std::vector<point2> > boundary = it->second->getBoundary();
    
      if (debug) std::cout<<"got boundary.. translating points.."<<endl;

      // translate the points
      updateAliceState();
      for(vector<vector<point2> >::iterator it1 = boundary.begin();
	  it1 != boundary.end(); it1++) {
	for (vector<point2>::iterator it2 = it1->begin();
	     it2 != it1->end(); it2++) {

	  *it2 = toLocal(*it2);

	  if (debug) cout<<it2->x<<" "<<it2->y<<";"<<endl;
	}
      }
      roadMarkings[it->first] = boundary;

    }
    for (ObjectMap::iterator it = yInter.begin(); it != yInter.end(); it++) {

      if (debug) std::cout<<"adding "<<it->first<< " to road markings dir"<<endl;
      std::vector<std::vector<point2> > boundary = it->second->getBoundary();
    
      if (debug) std::cout<<"got boundary.. translating points.."<<endl;

      // translate the points
      updateAliceState();
      for(std::vector<std::vector<point2> >::iterator it1 = boundary.begin();
	  it1 != boundary.end(); it1++) {
	for (vector<point2>::iterator it2 = it1->begin();
	     it2 != it1->end(); it2++) {

	  *it2 = toLocal(*it2);

	  if (debug) cout<<it2->x<<" "<<it2->y<<";"<<endl;
	}
      }
      roadMarkings[it->first] = boundary;
    }
	
  }

  bool Environment::removeObject(std::string const& objName, bool canDelete)
  {
    if (debug) cout<<"Environment::removeObject -- "<<objName<<endl;
    Object* object = nameDirectory[objName];
  
    // send the "clear" message to the map
    MapElement clearObj;
    clearObj.setTypeClear();
    clearObj.setId(BASEID,int(object->getName().c_str()));
    sendMapElement(&clearObj,subgroup);
    // send to mapviewer debug channel
    sendMapElement(&clearObj, -2);

    if (!object)
      {
	nameDirectory.erase(objName);
	return false;
      }
		
    // erase the object from both directories
    nameDirectory.erase(objName); if (debug) std::cout<<"object erased from name dir"<<endl;
    // debug note: i think when it tries to run object->getClassID(), there
    // isn't really an object there. that's where there's a segfault
    typeDirectory[object->getClassID()].erase(objName); if (debug) std::cout<<"object erased from type dir"<<endl;	

    if (canDelete) {
      delete object;
      if (debug) std::cout<<"object "<<objName<<" deleted"<<endl;	  
    }
    //	debug = true;
    if (debug) cout<<"about to return..."<<endl;
    return true;
  }

  std::string Environment::renameObject(std::string const& oldName, std::string const& newName)
  {
    Object* object = nameDirectory[oldName];
	
    if (!object)
      {
	nameDirectory.erase(oldName);
	return "";
      }

    removeObject(oldName, false);
    addObject(object, newName);
	
    return object->getName();
  }

  Object* Environment::getObject(std::string const& objName)
  {
    Object* result = nameDirectory[objName];
	
    if (!result)
      nameDirectory.erase(objName);
		
    return result;
  }

  std::string Environment::getObjectName(float x, float y, float distance) {
    float bestDistance = distance;
    Object* bestObject = NULL;
    std::string bestName = "";
    for (ObjectMap::iterator i = nameDirectory.begin()
	   ; i != nameDirectory.end() ; ) {
      std::string name = i->first;
      Object* object = (i++)->second;
      if (object->getClassID() == "Road" 
	  || object->getClassID() == "Lane"
	  || object->getClassID() == "Spline"
	  || object->getClassID() == "XIntersection"
	  || object->getClassID() == "YIntersection"
	  || object->getClassID() == "LaneGrid")
	continue;
      point2 objLoc = object->getCenter();
      float objX = objLoc.x;
      float objY = objLoc.y;
      float distance = sqrt(pow(objX-x, 2) + pow(objY-y, 2));
      if (distance < bestDistance) {
	bestObject = object;
	bestDistance = distance;
	bestName = name;
      }
    }
    return bestName;
  }
	

  ObjectMap& Environment::getObjectsByClassID(std::string const& classID)
  {
    return typeDirectory[classID];
  }


  void Environment::clearObjectsByClassID(std::string const& classID,
					  bool canDelete)
  {
    if (classID == "Road")
      roadMarkings.clear();

    ObjectMap *objMap = &typeDirectory[classID];
    for (ObjectMap::iterator i = objMap->begin();
	 i != objMap->end();
	 ++i)
      {
	nameDirectory.erase(nameDirectory.find(i->second->getName()));
	if(canDelete)
	  delete i->second;
      }
    typeDirectory.erase(classID);
  }

  void Environment::clearObjects(bool canDelete)
  {
    if (debug) cout<<"Environment::clearObjects()"<<endl;
    if (canDelete)
      {
	for (ObjectMap::iterator i = nameDirectory.begin();
	     i != nameDirectory.end();
	     ++i) {
	
	  if (i->second)
	    delete i->second;
	}
	
      }
	
    // the following only deletes pointers
    nameDirectory.clear();
    typeDirectory.clear();
    nameList.clear();
    roadMarkings.clear();
  
    if (debug) cout<<"end Environment::clearObjects"<<endl;
  }

  void Environment::pauseAllCars()
  {
    ObjectMap cars = getObjectsByClassID("Car");
    ObjectMap splineCars = getObjectsByClassID("SplineCar");
    cars.insert(splineCars.begin(),splineCars.end());

    for (ObjectMap::iterator it = cars.begin(); it != cars.end(); it++) {

      // toggle the pause state of the car
      it->second->paused = !trafficPaused;
    }

    if (trafficPaused == true) {
      trafficPaused = false;
      cout<<"Traffic unpaused"<<endl;
    }
    else {
      trafficPaused = true;
      cout<<"Traffic paused"<<endl;
    }
    
  }

  /* ******* REFERENCE FRAME FUNCTIONS ******* */

  Vector const& Environment::getBottomLeftBound() const
  {
    return bottomLeftBound;
  }
	
  Vector const& Environment::getTopRightBound() const
  {
    return topRightBound;
  }

  void Environment::setBounds(Vector const& bottomLeft, Vector const& topRight)
  {
    bottomLeftBound = bottomLeft;
    topRightBound = topRight;
	
    assert(bottomLeftBound.x < topRightBound.x &&
	   bottomLeftBound.y < topRightBound.y);
  }

  /* ******* SIMULATION FUNCTIONS ******* */
	
  void Environment::draw()
  {
    for (ObjectMap::iterator i = nameDirectory.begin();
	 i != nameDirectory.end();
	 ++i)
      {
	i->second->draw();
      }
  }

  void Environment::simulate(float ticks)
  {

    currentTime += ticks;

    // if an object's simulate() function returns False,
    // that means it's asking to be deleted.
    for (ObjectMap::iterator i = nameDirectory.begin();
	 i != nameDirectory.end();)
      {
	Object* object = (i++)->second;
	
	if(!object->simulate(ticks)) {
	  if (debug) cout<<"simulate returned false"<<endl;
	  removeObject(object->name);
	}
    
      }
    
    ObjectMap externalCars = getObjectsByClassID("ExternalCar");

    Object* alice = externalCars.begin()->second;

    if (alice->getName() == "Alice") {
      
      //      cout<<"Setting Alice's current road info"<<endl;
      
      // tell alice (externalcar) to set her current road
      alice->setCurrentRoad(getObjectsByClassID("Road"));
    }

    // send appropriate environment objects to the mapper
    // don't automatically send if we're in debug mode
    if (!debug) exportEnv();
  }

  void Environment::exportEnv()
  {
    updateAliceState();

    if (debug) {
      cout<<"Exporting environment information: "<<endl;
      cout<<"global->local x: "<<xInit<<endl;
      cout<<"global->local y: "<<yInit<<endl;
    }
    exportRoadLines();
    exportCars();
    exportObstacles();

    Object* alice = getObject("Alice");
    if (alice) {
      aliceLocation = alice->getCenter();
    }
  }

  void Environment::updateAliceState()
  {
    UpdateState();
    aliceState = m_state;
    xInit = m_state.utmNorthing - m_state.localX;
    yInit = m_state.utmEasting - m_state.localY;
  }

  point2 Environment::toLocal(point2 p)
  {
    double x = p.x;
    double y = p.y;

    if (debug) {
      cout<<"x (trafsim): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    // convert from trafsim frame to global
    x += xTranslate;
    y += yTranslate;
	
    if (debug) {
      cout<<"x (global): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }
    
    // switch x and y
    double temp = x;
    x = y;
    y = temp;

    if (debug) {
      cout<<"x (global, switched): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    // convert from global to local (alice)
    x -= xInit;
    y -= yInit;

    if (debug) {
      cout<<"x (alice local): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    return point2(x,y);
  }

  point2 Environment::toGlobal(point2 p)
  {
    double x = p.x;
    double y = p.y;

    if (debug) {
      cout<<"x (trafsim): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    // convert from trafsim frame to global
    x += xTranslate;
    y += yTranslate;
	
    if (debug) {
      cout<<"x (global): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }
    
    // switch x and y
    double temp = x;
    x = y;
    y = temp;

    if (debug) {
      cout<<"x (global, switched): "<<x<<endl;
      cout<<"y: "<<y<<endl;
    }

    return point2(x,y);
  }

  void Environment::exportCars()
  {

    // export all of the car objects
    ObjectMap cars = getObjectsByClassID("Car");
    ObjectMap splineCars = getObjectsByClassID("SplineCar");
    cars.insert(splineCars.begin(),splineCars.end());

    MapElement obj;
    int name;
    string strName;
    int bytesSent;
    for (ObjectMap::iterator i = cars.begin(); i != cars.end();)
      {
	// reset everything
	obj.clear();
	bytesSent = 0;
	name = 0;
	strName = "";
	
	// get the obstacle
	Object* object = (i++)->second;

	// get the name
	strName = object->getName();
	name = int(strName.c_str());

	if(strName != "Alice") {

	  // set teh name
	  obj.setId(BASEID,name);
	
	  // get the boundary
	  vector<point2> boundary = object->getBoundary().front();

	  // translate to local coords
	  for (vector<point2>::iterator it = boundary.begin();
	       it != boundary.end(); it++) {
	    *it = toLocal(*it);
	  }

	  // store the position to a log
	  //	  writeToLog(toGlobal(object->getCenter()), object->getName());
	
	  // initialize the object
	  obj.setGeometry(boundary);

	  double velocity = object->getVelocity();
	  double theta = object->getOrientation();
	  obj.velocity.x = velocity * cos(theta);
	  obj.velocity.y = velocity * sin(theta);
	  obj.setState(aliceState);
	  obj.setTypeVehicle();
	  obj.height = 1;
	  obj.timeStopped = object->timeStopped;

	  // HACK: if the car is stopped at intersection and alice is already there, then pause
	  if (object->getCenter().dist(aliceLocation) < 25)
	    object->pauseForAlice = true;
	  else object->pauseForAlice = false;
	  if (debug) cout<<"time stopped: "<<object->timeStopped<<endl;
	  if (debug) cout<<"dist to alice: "<<object->getCenter().dist(aliceLocation)<<endl;

	  // send the object
	  bytesSent = 0;

	  // send to sensor-sim
	  //	  if (obj.center.dist(aliceLocation)<200)
	    bytesSent = sendMapElement(&obj,subgroup);
	  // send to mapviewer debug channel
	  obj.setColor(MAP_COLOR_GREY);
	  obj.geometryType =GEOMETRY_POINTS;
	  bytesSent = sendMapElement(&obj, -2);
	
	  if (debug) {
	    std::cout<<"Sending static obstacle:"<<endl;
	    std::cout<<"  bytes sent = " << bytesSent << endl;
	    std::cout<<"  object id = " <<obj.id.front() <<endl;
	    std::cout<<"  Obstacle position: ( "<<boundary.front().x<<","<<boundary.front().y<<" )"<<endl;
	  }
     
	}
      }
  }

  void Environment::exportObstacles()
  {

    // grab all the obstacles
    ObjectMap obstacles = getObjectsByClassID("Obstacle");

    MapElement obj;
    int name;
    string strName;
    int bytesSent;
    for (ObjectMap::iterator i = obstacles.begin(); i != obstacles.end();)
      {
	// reset everything
	obj.clear();
	bytesSent = 0;
	name = 0;
	strName = "";
	
	// get the obstacle
	Object* object = (i++)->second;

	// get the name
	strName = object->getName();
	name = int(strName.c_str());

	// set teh name
	obj.setId(BASEID,name);
	
	// get the boundary
	vector<point2> boundary = object->getBoundary().front();

	// translate to local coords
	for (vector<point2>::iterator it = boundary.begin();
	     it != boundary.end(); it++) {
	  *it = toLocal(*it);
	}
	
	// initialize the object
	obj.setGeometry(boundary);
	//obj.set_poly_obs(obj.id.arr,boundary);
	obj.setState(aliceState);
	obj.setTypeObstacle();
	obj.height = 1;
	
	// send the object
	bytesSent = 0;

	// send to sensor-sim
	//	if (obj.center.dist(aliceLocation)<200)
	  bytesSent = sendMapElement(&obj, subgroup);

	// send to mapviewer debug channel
	obj.setColor(MAP_COLOR_GREY); 
	obj.geometryType = GEOMETRY_POINTS;
	bytesSent = sendMapElement(&obj, -2);
	
	if (debug) {
	  std::cout<<"Sending static obstacle:"<<endl;
	  std::cout<<"  bytes sent = " << bytesSent << endl;
	  std::cout<<"  object id = " <<obj.id.front() <<endl;
	  std::cout<<"  Obstacle position: ( "<<boundary.front().x<<","<<boundary.front().y<<" )"<<endl;
	}
     
      }

  }

  void Environment::exportRoadLines()
  {
    // send all of the road lines
    MapElement obj;
    int name;
    int bytesSent;
    int laneNumber;

    //    point2arr tmpptarr;
    //    point2 tmppt;
    //    point2 statept(aliceState.localX,aliceState.localY);
    for (std::map<std::string, std::vector<std::vector<point2> > >::iterator it = roadMarkings.begin();
         it != roadMarkings.end(); it++) {
      
      laneNumber = 1;
      
      vector <vector< point2 > > pts = (*it).second;
      
      
      for (std::vector<std::vector<point2> >::iterator it2 = pts.begin();
           it2 != pts.end(); it2++) {
        
	obj.clear();
        bytesSent = 0;
        name = 0;
        
        name = int(it->first.c_str()) + laneNumber;
        laneNumber++;       


	obj.setGeometry(*it2);
	obj.setId(BASEID,name);
                    
	obj.setTypeLaneLine();
	obj.setState(aliceState);
	obj.setColor(MAP_COLOR_GREY);          
         
	// send to sensor-sim
	// don't send unless the line is w/in 50m
	//	if (obj.center.dist(aliceLocation)<200)
	  bytesSent = sendMapElement(&obj, subgroup);
	// send to mapviewer debug channel
	//	obj.setColor(MAP_COLOR_GREY); 
	//	bytesSent = sendMapElement(&obj, -2);

	
	//dont send roadline info to mapper yet
	// need to truncate this first to make it more like sensed data
	///bytesSent = sendMapElement(&obj, 0);


	if (debug) {
	  std::cout<<"Sending road line object:"<<endl;
	  std::cout<<"  bytes sent = " << bytesSent << endl;
	  std::cout<<"  object id = " <<name <<endl;
	  std::cout<<"  first point = ( "<<it2->front().x<< " , "<<it2->front().y<<" ) "<<endl;
	}
      }
    }

  }

  void Environment::openNewLog(string name)
  {
    string fileName = logFileNameBase + "-";
    fileName += name;
    fileName += ".log";

    openLogs.insert( make_pair(name, new ofstream(fileName.c_str())) );
   
  }

  void Environment::writeToLog(point2 p, string name)
  {

    map<string,ofstream*>::iterator it = openLogs.find(name);
    
    // check to see if the name even exists
    if (it != openLogs.end()) {

      *(it->second)<<fixed;
      *(it->second)<<setprecision(3)<<currentTime<<" "<<setprecision(1)<<p.x<<" "<<p.y<<endl;
    }

    else 
      cout<<"Error: log "<<logFileNameBase<<name<<" not found"<<endl;

  }

  void Environment::closeLogs()
  {
   
    // close all the output streams
    for(map<string,ofstream*>::iterator it = openLogs.begin();
	it != openLogs.end(); it++) {

      it->second->close();

    }

  }

  /* ******* PRIVATE FUNCTIONS ******* */

  std::string Environment::_generateName(std::string const& proposedName)
  {
    if (nameList.find(proposedName) == nameList.end())
      {
	nameList[proposedName] = 1;
	return proposedName;
      }
	
    // if "name" is in use, return "name___n", where "n" is an integer
    std::ostringstream ostr;
    ostr << proposedName << "___" << nameList[proposedName]++;
	
    return ostr.str();
  }

  /* ******* SERIALIZATION FUNCTIONS ******* */

  std::ostream& operator<<(std::ostream& os, Environment const& env)
  {
    os.precision(16);
    return os << "Environment( " << std::endl << 
      "bottomLeftBound --> " << env.bottomLeftBound << std::endl <<
      "topRightBound --> " << env.topRightBound << std::endl <<
      "xTranslation --> " << env.xTranslate << std::endl <<
      "yTranslation --> " << env.yTranslate << std::endl <<
      "xInit --> " << env.xInit << std::endl <<
      "yInit --> " << env.yInit << std::endl <<
      "nameDirectory --> " << env.nameDirectory << std::endl <<
      "nameList --> " << env.nameList << std::endl <<
      " )";	
  }

  std::istream& operator>>(std::istream& is, Environment & env)
  {
    is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
    try
      {
	matchString(is, "Environment");
	matchString(is, "(");
	DESERIALIZE_RN(is, "bottomLeftBound", env.bottomLeftBound);
	DESERIALIZE_RN(is, "topRightBound", env.topRightBound);
	DESERIALIZE_RN(is, "xTranslation", env.xTranslate);
	DESERIALIZE_RN(is, "yTranslation", env.yTranslate);
	DESERIALIZE_RN(is, "xInit", env.xInit);
	DESERIALIZE_RN(is, "yInit", env.yInit);
	DESERIALIZE_RN(is, "nameDirectory", env.nameDirectory);
	DESERIALIZE_RN(is, "nameList", env.nameList);
	matchString(is, ")");
      }
    catch(ParseError& e)
      {
	e.info = "Failed to parse Environment:\n" + e.info;
	throw;
      }
    catch(std::exception& e)
      {
	throw ParseError("Failed to parse Environment:\n " + std::string(e.what()) );
      }

    // clear the old directories
    env.typeDirectory.clear();
    env.roadMarkings.clear();

    for (ObjectMap::iterator i = env.nameDirectory.begin();
         i != env.nameDirectory.end();
         ++i)
      {
        Object* object = i->second;
        object->name = i->first;    // reload the object's name

        // store the object in the classID index as well
        env.typeDirectory[object->getClassID()][object->name] = object;

        // store the object's boundary
        if (object->getClassID() == "Lane"  || object->getClassID() == "Line") {

          if (object->getClassID() == "Lane") {
            Lane* lane = static_cast<Lane*>(object);
            if (lane->inIntersection == false || object->getClassID() == "Line") {

              std::vector<vector<point2> > boundary = object->getBoundary();

              // translate the points
              env.updateAliceState();
              for(vector<vector<point2> >::iterator it1 = boundary.begin();
                  it1 != boundary.end(); it1++) {

                for (vector<point2>::iterator it2 = it1->begin();
		     it2 != it1->end(); it2++) {

                  *it2 = env.toLocal(*it2);

                }
              }
              env.roadMarkings[object->name] = boundary;
            }
          }
        }

        // replace string references with actual pointers
        object->_resolveDependencies(env);
      }
	
    return is;	
  }
		
}
