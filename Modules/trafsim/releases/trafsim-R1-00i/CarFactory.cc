#include "CarFactory.hh"
#include <cstdlib>
#include <cmath>

#define RAND_RANGE(A,B) ( (A) + ((B)-(A))*((double)rand() / RAND_MAX) ) 

namespace TrafSim
{

CarFactory::CarFactory(): 
	spawnRoad(0), spawnLaneIndex(0), spawnT(0.0f),
	remainingCars(0), rate(0.0f), 
	delta(0.0f), failMode(CF_FAIL_WAIT),
	timeLeft(0.0f)
{
	seed = (unsigned int)time(0);
	srand(seed);
}

CarFactory::~CarFactory()
{
}

CarFactory* CarFactory::downcast(Object* source)
{
	return dynamic_cast<CarFactory*>(source);
}

void CarFactory::draw()
{
	// TODO: add optional visualization to view car factories, maybe in a static method
}

// check whether there's enough room to create a new car
bool isObstructed(Lane* lane, float t, float threshold)
{
	std::pair<float, Object*> beforePair = lane->getObjectBefore(t, "Car");
	std::pair<float, Object*> afterPair  = lane->getObjectAfter(t, "Car");
	
	Vector factoryPos = lane->spline->evalCoordinate(t);
	
	if (beforePair.second)
	{
		Car* car = (Car*)beforePair.second;
		Vector pos = lane->spline->evalCoordinate(beforePair.first);
		
		if ( sqrt( Vector::dist2(pos, factoryPos) ) < car->getProperties().size.y * 0.5f + threshold )
			return true;  		
	}
	if (afterPair.second)
	{
		Car* car = (Car*)afterPair.second;
		Vector pos = lane->spline->evalCoordinate(afterPair.first);
		
		if ( sqrt( Vector::dist2(pos, factoryPos) ) < car->getProperties().size.y * 0.5f + threshold )
			return true;  		
	}
	return false;
}
		
bool CarFactory::simulate(float ticks)
{
	if (!spawnRoad || rate <= 0.0f || remainingCars == 0)
		return true;
		
	timeLeft -= ticks;	
 	if (timeLeft < 0.0f)
 	{
 		double randomizedRate = rate + RAND_RANGE(-delta, delta);
 		float  newInterval = 1.0f / (float)randomizedRate;
 	
 		if (isObstructed(spawnRoad->getLane(spawnLaneIndex), 
 						 spawnT, highProps.size.y * 0.5f))
 		{
 			if (failMode == CF_FAIL_WAIT)
 				return true;
 			if (failMode == CF_FAIL_SKIP)
 			{
 				timeLeft = newInterval;
 				return true;
 			}
 		}
 			
 		if (remainingCars > 0)
 			remainingCars--;
 		
 		timeLeft = newInterval;
 		
 		CarProperties props;
 		props.size  = RAND_RANGE(lowProps.size, highProps.size);
 		props.color = RAND_RANGE(lowProps.color, highProps.color);
		props.comfyAccel = RAND_RANGE(lowProps.comfyAccel, highProps.comfyAccel);
 		props.maxAccel = RAND_RANGE(lowProps.maxAccel, highProps.maxAccel);
 		props.maxAngularAccel = RAND_RANGE(lowProps.maxAngularAccel, highProps.maxAngularAccel);
 		props.safetyTime = RAND_RANGE(lowProps.safetyTime, highProps.safetyTime);
 		props.minDistance = RAND_RANGE(lowProps.minDistance, highProps.minDistance);
 		props.carWaitFactor = RAND_RANGE(lowProps.carWaitFactor, highProps.carWaitFactor);
 		props.gapAcceptFactor = RAND_RANGE(lowProps.gapAcceptFactor, highProps.gapAcceptFactor);
		props.accelDelta = RAND_RANGE(lowProps.accelDelta, highProps.accelDelta);
		props.curveEvalInterval = RAND_RANGE(lowProps.curveEvalInterval, highProps.curveEvalInterval);
	
		Car* newCar = new Car();
 		newCar->setProperties(props);
 		newCar->setPath(carPath);
 		
 		newCar->switchLane(spawnRoad, spawnLaneIndex, spawnT);		
 	}
 	
	return true;	
}

void CarFactory::setRemainingCars(int nCars)
{
	if (nCars >= -1)
		remainingCars = nCars;
}

void CarFactory::setCreationRate(double rate)
{
	if (rate > delta)
		this->rate = rate;
}
	
void CarFactory::setCRRandomDelta(double delta)
{
	if (rate > delta)
		this->delta = delta;
}

void CarFactory::setCarProperties(CarProperties const& low, CarProperties const& high)
{
	// TODO: sanity checks for low/high
	lowProps = low;
	highProps = high;
}
	
void CarFactory::setCarProperties(CarProperties const& props)
{
	lowProps = props;
	highProps = props;
}

void CarFactory::setRandomSeed(unsigned int seed)
{
	this->seed = seed;
	srand(seed);
}

void CarFactory::setFailMode(CarFactoryFailMode mode)
{
	failMode = mode;
}

void CarFactory::setPath(std::vector<CrossManeuver> const& path)
{
	carPath = path;	
}

void CarFactory::setPath(std::vector<unsigned int> const& path)
{
	// TODO: this code is duplicated from Car.cpp. DRY principle violation.
	typedef std::vector<CrossManeuver> DestType;
	typedef std::vector<unsigned int> SrcType;
	
	DestType temp;
	for (SrcType::const_iterator i = path.begin(); i != path.end(); ++i)
		temp.push_back( (CrossManeuver)(*i) );
	
	setPath(temp);
}

void CarFactory::setPosition(Road* road, unsigned int laneIndex, float t)
{
	spawnRoad = road;
	spawnLaneIndex = laneIndex;
	spawnT = t;
}

int CarFactory::getRemainingCars() { return remainingCars; }
double CarFactory::getCreationRate() { return rate; }
double CarFactory::getCRRandomDelta() { return delta; }
std::pair<CarProperties,CarProperties> 
	CarFactory::getCarProperties() { return std::make_pair(lowProps, highProps); }
unsigned int CarFactory::getRandomSeed() { return seed; }
CarFactoryFailMode CarFactory::getFailMode() { return failMode; }
std::vector<CrossManeuver> const&
	CarFactory::getPath() { return carPath; }
Road* CarFactory::getRoad() { return spawnRoad; }
unsigned int CarFactory::getLaneIndex() { return spawnLaneIndex; }
float CarFactory::getT() { return spawnT; }

void CarFactory::serialize(std::ostream& os) const
{
	os << "spawnRoad --> " << spawnRoad->getName() << std::endl;
	os << "spawnLaneIndex --> " << spawnLaneIndex << std::endl;
	os << "spawnT --> " << spawnT << std::endl;
	
	os << "carPath --> " << carPath << std::endl;
	os << "remainingCars --> " << remainingCars << std::endl;
	os << "rate --> " << rate << std::endl;
	os << "delta --> " << delta << std::endl;
	
	os << "lowProps --> " << lowProps << std::endl;
	os << "highProps --> " << highProps << std::endl;
	os << "seed --> " << seed << std::endl;
	
	os << "failMode --> " << (int)failMode << std::endl;	
}

void CarFactory::deserialize(std::istream& is)
{
	std::string spawnRoadName;
	std::vector<unsigned int> path;
	int failModeInt;
	
	DESERIALIZE_RN(is, "spawnRoad", spawnRoadName);
	DESERIALIZE(is, spawnLaneIndex);
	DESERIALIZE(is, spawnT);
	DESERIALIZE_RN(is, "carPath", path);
	DESERIALIZE(is, remainingCars);
	DESERIALIZE(is, rate);
	DESERIALIZE(is, delta);
	DESERIALIZE(is, lowProps);
	DESERIALIZE(is, highProps);
	DESERIALIZE(is, seed);
	DESERIALIZE_RN(is, "failMode", failModeInt);
	
	this->setPath(path);
	_addReference(spawnRoadName, (Object**)&spawnRoad);
	failMode = (CarFactoryFailMode)failModeInt;
	timeLeft = 0.0f;
}

}
