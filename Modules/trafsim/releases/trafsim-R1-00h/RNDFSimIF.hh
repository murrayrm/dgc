#include "skynettalker/StateClient.hh"
#include "rndf/RNDF.hh"

#include "Object.hh"
#include "Vector.hh"


namespace TrafSim
{

typedef enum
{
    LEFT_A,
    RIGHT_A
} Dir;

typedef struct
{
    Dir dir;
    double dist;
} DirDist;

class RNDFSimIF
{
    public:
    RNDF* rndf;       
    
    RNDFSimIF();
    ~RNDFSimIF();
    
    //Resets the rndf data.
    //void resetRNDF();
    
    //Parse in the RNDF file.
    bool loadFile(char* filename);
    
    //Combine all the lanes in a segment so that multiple roads
    //aren't created when there should only be one.
    //Then again. Maybe not. Extremely complicated?
    //USE ONLY LANE 1 FOR NOW!
    void mergeLanesinSegments();

    //Return the points in the form needed for Spline() to create
    //the spline in simulation.
    std::vector<Vector> getPointsForSpline(int segNumber, int laneNumber);

    //Returns a vector of sets of points for each spline.
    //Output is similar to as though getPointsForSpline were called
    //on all splines, then aggregated into a vector.
    //WARNING: Currently only handles 1-laned roads.
    //#define SAME_DIST 2
    //std::vector< std::vector<Vector> > getPointsForAllSplines();

    //Northing and Easting values are disgustingly too large for
    //the simulator to easily handle. Center these values so that it
    //can be used.
    void centerAllWaypoints();
    
    //Return the number of segments in the loaded RNDF file. For use
    //in building the RNDF in the python environment.
    int getNumSegs();

    //Get the amount that the map was translated.
    double getxTranslate();
    double getyTranslate();

    //Store the xTranslate and yTranslate values, as obtained by
    //centerAllWaypoints() so that conversions can be made back to
    //Northing and Easting for closed loop simulation.
    private:
    //Appends a start and/or end point to the vector to make
    //the splines build right.
    //Flags: APPEND_START APPEND_END
    #define APPEND_START 0x1
    #define APPEND_END   0x2
    #define APPEND_DIST  5
    void appendStartEnd(std::vector<Vector> *points,
                        int flags=APPEND_START | APPEND_END);

    double xTranslate, yTranslate; 
};
}
