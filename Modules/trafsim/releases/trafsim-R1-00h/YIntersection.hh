#ifndef YINTERSECTION_H_
#define YINTERSECTION_H_

#include "Intersection.hh"

#include <vector>
#include <map>

namespace TrafSim
{

/**
 * A three-way intersection. The interface is identical to that of any intersection, so I will not 
 * document the methods independently here. Note that this code is almost identical to the code for XIntersection,
 * save for the number of roads and a few minor logic changes. This code should be refactored as soon as possible.
 */
class YIntersection: public Intersection
{
public:
	YIntersection();
	virtual ~YIntersection();
	
	virtual std::string getClassID() const { return "YIntersection"; }
	
	virtual void draw();
	
	virtual bool hookStart(Road& road);
	virtual bool hookEnd(Road& road);
	
	virtual bool blockManeuver(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver);
	virtual bool unblockManeuver(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver);
	
	virtual bool computeGeometry(Environment& env);
	
	virtual RoadLane getDestination(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver) const;
	virtual Lane* getLane(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver);
	
	virtual bool setStop(Road const& road, bool stopSign);
	virtual bool isStop(Road const& road);
	
	virtual RightOfWay rightOfWay(Car* thisCar, Road const& road, Car** followCar);
	virtual void exitIntersection(Car* thisCar);
	virtual void getOpposingTraffic(Road const& startRoad, unsigned int startLane, CrossManeuver maneuver,
									std::vector<Lane*>& result);
								
	virtual void getBoundaries(std::vector<Spline*>& boundsArray);
	
	/// A helper function, for Python's benefit. Python doesn't understand downcasting, so
	/// this function does it explicitly so that objects extracted from an Environment can be used.
	/// \param source The object pointer to be cast down to a YIntersection
	/// \return A YIntersection*, pointing to the same object as source.	
	static YIntersection* downcast(Object* source);
	
protected:

	/// Writes the object to the output stream, in standard format.
	/// \param os The stream to write to.
	virtual void serialize(std::ostream& os) const;
	
	/// Reads the object from the input stream, in standard format.
	/// \param is The stream to read from.
	virtual void deserialize(std::istream& is);
	
	int   hook(Road& road, bool reversed);
	void  sortRoads();
	void  computeBoundaries();
	void  computeLanes(Environment& env);
	void  computeInterference();
	void  calculateCurve(Vector const& pt1, Vector const& tan1, 
						 Vector const& pt2, Vector const& tan2,
						 float sharpness, float narrowing,
						 Spline& result);
	
	Spline boundaries[3];
	Road*  roads[3];
	std::vector<ManeuverDestMap>* maneuverData[3];
	std::vector<ManeuverLaneMap>* laneData[3];
	bool reversed[3];
	bool stops[3];
	Vector center;
	Car* lastCar;
	
	std::map<Lane*, Lane*> multiLanes; 
	std::map<std::string, Object*> namedMap;
	
	bool computed;
	
private:

	virtual void _postResolutionCallback(Environment& env);
};

}

#endif /*YINTERSECTION_H_*/
