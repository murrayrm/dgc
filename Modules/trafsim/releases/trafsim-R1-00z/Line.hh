#ifndef LINE__HH__
#define LINE__HH__

#include "Object.hh"
#include "Spline.hh"
#include "Color.hh"

namespace TrafSim {

   /**
    * The Line class is used to create arbitrary road lines, such as 
    * stop lines, parking spaces, and zone borders
    */
  class Line: public Object
  {
  public:
    
    /* ******** CONSTRUCTORS AND DESTRUCTORS ******/

    /**
     * By default, creates a line with ~0 length
     */
    Line();
    /**
     * Creates a line with the given endpoints
     * \param p1 The first point
     * \param p2 The second point
     */
    Line(point2 p1, point2 p2);
    /**
     * Creates a line with the given points
     * \param points The points
     */
    Line(vector<point2> points);
    /**
     * Creates a line from the given spline
     * \param s the spline
     */
    Line(Spline s);
    /**
     * Destructor.
     */
    virtual ~Line();
   
    /* ***** FUNCTIONS ********/
    /**
     * Gives the class ID of the object
     * \return "Obstacle"
     */
    virtual std::string getClassID() const { return "Line"; }

    /**
     * Draw the boundary of the obstacle
     */
    virtual void draw();


    /**
     * Changes the color of the line
     * \param r Red, range from 0 to 1
     * \param g Green, range from 0 to 1
     * \param b Blue, range from 0 to 1
     * \param a Alpha, range from 0 to 1
     */
    void setColor(float r, float g, float b, float a);

    /**
     * Since obstacles are static by default, this function doesn't
     * do very much.
     * \param ticks Amount of time since the last call to simulate(), 
     * in seconds.
     * \return Always true.
     */
    virtual bool simulate(float ticks);
    	

    /* ********* ACCESSORS INHERITED FROM OBJECT *******/
    
    /// \return the location of the center of the spline
    point2 getCenter() { return spline.getCenter(); }
    /** 
     * Lines have no velocity
     * \return 0
     */
    float getVelocity() { return 0; }
    /**
     * \return The line's orientation
     */
    float getOrientation() { return spline.getOrientation(); }
    /**
     * \return The length of the line, in meters
     */
    float getLength() { return spline.getLength(); }
    /**
     * \return The width of the line, in meters
     */
    float getWidth() { return width; }
    /**
     * \return A set of points describing the car's boundary
     */
    vector<vector<point2> > getBoundary();
    /** Lines can't contain objects
     * \return false
     */
    bool containsObject(Object* obj) { return false; }

    /* *** HELPER FUNCTIONS *****/		

    /// A helper function, for Python's benefit. Python doesn't understand downcasting, so
    /// this function does it explicitly so that objects extracted from an Environment can be used.
    /// \param source The object pointer to be cast down to a Car
    /// \return A Car*, pointing to the same object as source.	
    static Line* downcast(Object* source);
    
  protected:

        
    /// Writes the object to the output stream, in standard format.
    /// NOT IMPLEMENTED
    /// \param os The stream to write to.
    virtual void serialize(std::ostream& os) const;
    
    /// Reads the object from the input stream, in standard format.
    /// NOT IMPLEMENTED
    /// \param is The stream to read from.
    virtual void deserialize(std::istream& is);
    
    float width;
    Color color;
    Spline spline;
  };
  
}

#endif /*LINE__HH__*/
