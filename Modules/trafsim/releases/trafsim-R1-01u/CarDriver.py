from libtrafsim_Py import *
from SuperDynamic import SuperDynamic
import math

class CarDriver(SuperDynamic):
    def __init__(self, car):
        SuperDynamic.__init__(self)
        self.car = car
        self.heading = Vector(0.0, 1.0)
        self.accel = Vector()
        
        self.keymap = { 
           SDLK_UP    : Vector(0.0, 20.0), 
           SDLK_DOWN  : Vector(0.0, -20.0),
           SDLK_LEFT  : Vector(5.0, 0.0),
           SDLK_RIGHT : Vector(-5.0, 0.0)
        }
        
    def onMouseMotion(self, event): pass
    def onMouseUnclick(self, event): pass
    def onMouseClick(self, event): pass
    
    def perFrame(self, ticks):
        carVel = self.car.getVelocity()
        if carVel.length() != 0.0:
            self.heading = carVel.normalized()
        
        perp = self.heading.perpendicular() 
        accel = Vector(self.accel.x * carVel.length(), self.accel.y)
        
        self.car.setAcceleration(
             Vector( - self.heading.y * accel.x + perp.y * accel.y,
                       self.heading.x * accel.x - perp.x * accel.y ))
        
        if self.car.getLaneTCoord() > 1.0:
            self.car.simulate(ticks)
            self.car.draw()
        
    def onKeyDown(self, event):
        try: self.accel += self.keymap[event.button]
        except KeyError: pass
         
    def onKeyUp(self, event):
        try: self.accel -= self.keymap[event.button]
        except KeyError: pass
