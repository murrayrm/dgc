#ifndef XINTERSECTION_H_
#define XINTERSECTION_H_

#include "Intersection.hh"

#include <vector>
#include <map>

namespace TrafSim
{

  /**
   * A four-way intersection. The interface is identical to that of any intersection, 
   * so I will not  document the methods independently here. Note that this code
   * is almost identical to the code for YIntersection, save for the number of
   * roads and a few minor logic changes. This code should be refactored as soon 
   * as possible.
   */
  class XIntersection: public Intersection
  {
    
  public:

    XIntersection();
    virtual ~XIntersection();
	
    virtual std::string getClassID() const { return "XIntersection"; }
	
    virtual void draw();
	
    virtual bool hookStart(Road& road);
    virtual bool hookEnd(Road& road);
	
    virtual bool blockManeuver(Road const& startRoad, 
			       unsigned int startLane, 
			       CrossManeuver maneuver);
    virtual bool unblockManeuver(Road const& startRoad, 
				 unsigned int startLane, 
				 CrossManeuver maneuver);
	
    virtual bool computeGeometry(Environment& env);
	
    virtual RoadLane getDestination(Road const& startRoad, 
				    unsigned int startLane, 
				    CrossManeuver maneuver) const;
    virtual Lane* getLane(Road const& startRoad, 
			  unsigned int startLane, 
			  CrossManeuver maneuver);
	
    virtual bool setStop(Road const& road, bool stopSign);
    virtual bool isStop(Road const& road);
	
    virtual RightOfWay rightOfWay(Car* thisCar, Road const& road, Car** followCar);
    virtual void exitIntersection(Car* thisCar);
    virtual void getOpposingTraffic(Road const& startRoad, 
				    unsigned int startLane, 
				    CrossManeuver maneuver,
				    std::vector<Lane*>& result);
							
    virtual void getBoundaries(std::vector<Spline*>& boundsArray);

    /* **** ACCESSORS INHERITED FROM OBJECT ****/

    /// \return the x,y position of the center of the object, in meters
    point2 getCenter() { return point2(center.x,center.y); }
    /// Intersections have no velocity
    /// \return 0
    float getVelocity() { return 0; }

    /// NOT IMPLEMENTED. USE getBoundary() INSTEAD
    float getOrientation() { return 0; }
    /// NOT IMPLEMENTED. USE getBoundary() INSTEAD
    float getLength() { return 0; }
    /// NOT IMPLEMENTED. USE getBoundary() INSTEAD
    float getWidth() { return 0; }
    
    /// Get the boundary of the intersection
    /// \return a set of 2D points that form the object's boundary
    std::vector<std::vector<point2> > getBoundary();
    
    /// I believe intersections contain lanes, and those lanes contain cars.
    /// so for right now, intersections do not contain anything
    /// \return false
    bool containsObject(Object* obj) { return false; }

	
    /// A helper function, for Python's benefit. Python doesn't understand downcasting, 
    /// so this function does it explicitly so that objects extracted from an 
    /// Environment can be used.
    /// \param source The object pointer to be cast down to a XIntersection
    /// \return A XIntersection*, pointing to the same object as source.	
    static XIntersection* downcast(Object* source);
	
  protected:

    /// Writes the object to the output stream, in standard format.
    /// \param os The stream to write to.
    virtual void serialize(std::ostream& os) const;
	
    /// Reads the object from the input stream, in standard format.
    /// \param is The stream to read from.
    virtual void deserialize(std::istream& is);
	
    int   hook(Road& road, bool reverse);
    void  sortRoads();
    void  computeBoundaries();
    void  computeLanes(Environment& env);
    void  computeInterference();
    void  calculateCurve(Vector const& pt1, Vector const& tan1, 
			 Vector const& pt2, Vector const& tan2,
			 float sharpness, float narrowing,
			 Spline& result);
	
    Spline boundaries[4];
    Road*  roads[4];
    std::vector<ManeuverDestMap>* maneuverData[4];
    std::vector<ManeuverLaneMap>* laneData[4];
    bool reversed[4];
    bool stops[4];
    Vector center;
    bool fourWayStop;
    Car* lastCar;
	
    std::map<Lane*, Lane*> multiLanes; 
    std::map<std::string, Object*> namedMap;
	
    bool computed;	
	
  private:

    virtual void _postResolutionCallback(Environment& env);
  };

}

#endif /*XINTERSECTION_H_*/
