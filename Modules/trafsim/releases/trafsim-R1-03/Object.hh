#ifndef OBJECT_H_
#define OBJECT_H_
//
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "frames/point2_uncertain.hh"

namespace TrafSim
{

  class Object;
  class Environment;
  class Vector;
  
  typedef std::multimap<std::string, Object**> DependencyMap;
  typedef std::map<std::string, Object*(*)()> DispatchMap;
  
  /**
   * This is the abstract base class for every simulation object that can be stored
   * in an Environment. It provides a number of services, primarily serialization 
   * functionality. It also defines the simulation interface, which consists of the two
   * functions draw() and simulate().
   * 
   * To enable serialization capability for a derived class, add an entry to the
   * _registerObjects() function defined in Object.cpp. If this is done, the Environment will 
   * know when to dynamically load your object from  * an input stream, so long as you 
   * provide the serialization and deserialization functions. 
   * 
   * See the class TestObject in Environment.cpp for an illustration of this mechanism.
   */
  class Object
  {
  public:

    /* ****** CONSTRUCTORS AND DESTRUCTORS ****** */

    /// The constructor only initializes the default name, "object".
    Object();

    /// The destructor does nothing.
    virtual ~Object();

    virtual void removeSelf() { }

    /* ****** NAME AND ID FUNCTIONS ****** */

    /// Returns the name of a particular instance. Note that the name is NOT
    /// meant to be set by the user or the client application. A name can be
    /// "proposed" when the object is added to an Environment, but the 
    /// Environment itself has the final say in what the object's name will be.
    /// \return The object's name
    std::string const& getName() const;

    /// Override this, and set it to return the name of your class. For example,
    /// in the class MyClass, this function should just return "MyClass";
    /// \return The name of the class
    virtual std::string getClassID() const = 0;

    /// Gets the name of the object's parent (the empty string by default;
    /// not all objects have to have parents.
    /// \return The name of the object's parent
    std::string const& getParent() const;

    /// Sets the name of the object's parent. Should be a name within 
    /// the same environment as the child object.
    /// \param parentName The name of the object's parent
    void setParent(std::string const& parentName);

    /// For Python's benefit, a pointer comparison operator.
    /// For some reason even pointers that point to the same memory location
    /// aren't equal if you believe SWIG's semantics...
    /// \param rhs Another Object*
    /// \return True if they are the same object, false if not.
    bool operator==(Object* rhs) const;

    /* ****** ACCESSORS ***** */

    /// \return the x,y position of the center of the object, in meters
    virtual point2 getCenter() = 0;
    /// \return the velocity of the object, in m/s
    virtual float getVelocity() = 0;
    /// \return the orientation of the object, in radians from -pi to pi
    virtual float getOrientation() = 0;

    /// \return the length (in y-dir) of the object, in meters
    virtual float getLength() = 0;
    /// \return the width (in x-dir) of the object, in meters
    virtual float getWidth() = 0;

    /// Accessor for generic boundary of an object
    /// \return a set of 2D points that form the object's boundary
    virtual std::vector<vector<point2> > getBoundary() = 0;
      
    /// Returns true if this object "contains" a given object
    /// For example, a road might contain a lane, and a lane might contain a car
    /// This check is performed based on the Object's name
    /// (ie using Object::getName())
    virtual bool containsObject(Object* obj) = 0;
	
    /* ****** SIMULATION FUNCTIONS ****** */
	
    /// Renders the object. The default implementation does nothing, but you can
    /// override it with your own drawing code. It will be called when the client
    /// application calls Environment::draw().
    virtual void draw();
    virtual bool invertColor() { return false; }

    bool paused;
    bool speedingUp;
    bool slowingDown;
    bool reversed;
    bool pauseForAlice;
    bool isHappy;

    /// Updates the object's state. The default implementation does nothing, but you can
    /// override it with your own code. The function should always return true, unless
    /// your object has reached the end of its lifetime and wants to die. In that case, 
    /// simulate() should return false, and the environment will remove and delete it.
    /// Do NOT call Environment::removeObject() from within a simulate() function! 
    /// \param ticks The number of seconds elapsed since the last call to simulate()
    /// \return False if the object should be removed from the Environment and deleted.
    virtual bool simulate(float ticks);

    /// For ExternalCar use
    virtual void setCurrentRoad(map<string, Object*> &roads) { } 

    /// For Car use
    virtual void setStopLines(vector<Vector> arr) { }
	
    /* ****** SERIALIZATION FUNCTIONS ****** */
	
    /// Writes the object to an output stream.
    /// \param os The stream to write to
    /// \param obj The object to write
    /// \return The ouput stream
    friend std::ostream& operator<<(std::ostream& os, Object* obj);
	
    /// Given an uninitialized Object pointer, this function figures out which subclass
    /// of Object should be created based on the contents of the input stream, and deserializes it.
    /// \param is The stream to read from
    /// \param obj An uninitialized Object pointer to fill with data
    /// \return The input stream
    friend std::istream& operator>>(std::istream& is, Object*& obj);
	
    friend std::ostream& operator<<(std::ostream& os, Environment const& env);
    friend std::istream& operator>>(std::istream& is, Environment & env);
    friend class Environment;
		
  private:
    // called by Environment::operator>> to fill in loose pointers
    void _resolveDependencies(Environment& env);	
	
    // called by Environment::operator>> AFTER all dependency resolution is done.
    // does nothing by default, but can be overridden for fancy functionality.
    virtual void _postResolutionCallback(Environment& env);
	
    // Read the main description of Object to see what these are for.
    template<class T> static Object* _classFactory() { return new T(); } 
    static void _registerObjects();
	
    static DispatchMap dispatchMap;
    static bool _registered;
	
    // for use with _resolveDependencies()
    DependencyMap dependencyMap;
		
  protected:
    /// Override this function with your own serialization code.
    /// \param os The output stream to write to
    virtual void serialize(std::ostream& os) const = 0;

    /// Override this function with your own serialization code.
    /// \param is The input stream to read from
    virtual void deserialize(std::istream& is) = 0;

    /// When deserializing a custom object, said object may contain
    /// references to other toplevel objects that may be loaded independently
    /// by the environment. There is no way to resolve these references
    /// during the deserialization process, so instead just call this function
    /// and they will be sorted out later. See TestObject in Environment.cpp 
    /// for an illustration.
    /// \param name The string name of the reference
    /// \param object A pointer to the pointer that needs to be filled in later
    void _addReference(std::string const& name, Object** object);

    std::string name; ///< A unique identifier for each derived class instance

    std::string parent; ///< Some objects can be children of others

    double timeStopped;
  };

}

#endif /*OBJECT_H_*/
