#include "Line.hh"
#include <cmath>
#include <GL/gl.h>
#include <iostream>
#include <cassert>

namespace TrafSim {

  Line::Line() 
  {
    spline = Spline();
    width = .1;
    color = Color();
  }
  
  Line::Line(point2 p1, point2 p2)
  {
    vector<Vector> vPoints;
    Vector p3,p4;
    // generate two more points from the one given
    // since Splines work better with 4+ points
    if ((p2.x - p1.x) == 0) {
      p3 = Vector(p2.x,p2.y+.1);
      p4 = Vector(p2.x,p1.y-.1);
    }
    else if ((p2.y-p1.y)==0) {
      p3 = Vector(p2.x+.1,p2.y);
      p4 = Vector(p1.x-.1,p2.y);
    }
    else {
      double slope = (p2.y - p1.y)/(p2.x-p1.x);
      if (slope > 0) {
	p3 = Vector(p2.x+.1,p2.y+.1);
	p4 = Vector(p1.x-.1,p2.y-.1);
      }
      else {
	p3 = Vector(p2.x-.1,p2.y-.1);
	p4 = Vector(p1.x+.1,p2.y+.1);
      }
    }

    vPoints.push_back(p4);
    vPoints.push_back(Vector(p1.x,p1.y));
    vPoints.push_back(Vector(p2.x,p2.y));
    vPoints.push_back(p3);

    spline = Spline(vPoints);
    width = .1;
    color = Color();

  }

  Line::Line(vector<point2> points)
  {
    assert(points.size() > 1);

    vector<Vector> vPoints;
    if (points.size() < 4) {
      Vector p1,p2,p3,p4;
      p1 = Vector(points.front().x,points.front().y);
      p2 = Vector(points[1].x, points[1].y);
      if ((p2.x - p1.x) == 0) {
	p3 = Vector(p2.x,p2.y+.1);
	p4 = Vector(p2.x,p1.y-.1);
      }
      else if ((p2.y-p1.y)==0) {
	p3 = Vector(p2.x+.1,p2.y);
	p4 = Vector(p1.x-.1,p2.y);
      }
      else {
	double slope = (p2.y - p1.y)/(p2.x-p1.x);
	if (slope > 0) {
	  p3 = Vector(p2.x+.1,p2.y+.1);
	  p4 = Vector(p1.x-.1,p2.y-.1);
	}
	else {
	  p3 = Vector(p2.x-.1,p2.y-.1);
	  p4 = Vector(p1.x+.1,p2.y+.1);
	}
      }
      vPoints.push_back(p4);
      vPoints.push_back(Vector(p1.x,p1.y));
      vPoints.push_back(Vector(p2.x,p2.y));
      vPoints.push_back(p3);
    }
    
    else {
      for (vector<point2>::iterator it = points.begin();
	   it != points.end(); it++) {
	vPoints.push_back(Vector(it->x,it->y));
      }
    }

    spline = Spline(vPoints);
    color = Color();
    width = .1;

  }

  Line::Line(vector<Vector> points)
  {
    spline = Spline(points);
    color = Color();
    width = .1;
  }

  Line::Line(Spline s)
  {
    spline = s;
    color = Color();
    width = .1;
  }

  Line::~Line() 
  {

  }

  void Line::setColor(float r, float g, float b, float a)
  {
    color.r = r;
    color.g = g;
    color.b = b;
    color.a = a;
  }

  void Line::draw() 
  {
    // draw a light blue object
    
    glPushMatrix();
    glSetColor(color);

    glBegin(GL_LINE_STRIP);

    vector<point2> boundary = spline.getBoundary().front();
    for (vector<point2>::iterator it = boundary.begin();
	 it != boundary.end(); it++) {	
      glVertex3f(it->x, it->y, 0.0f);      
    }

    glEnd();
    glPopMatrix();
  }

  bool Line::simulate(float ticks) 
  {
    return true;
  }

  vector<vector<point2> > Line::getBoundary()
  {
    return spline.getBoundary();
  }

  Line* Line::downcast(Object* source)
  {
    return dynamic_cast<Line*>(source);
  }

  void Line::serialize(std::ostream& os) const
  {
    // NOT IMPLEMENTED
  }

  void Line::deserialize(std::istream& is)
  {
    // NOT IMPLEMENTED
  }

}
