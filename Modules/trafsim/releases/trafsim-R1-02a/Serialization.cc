#include "Serialization.hh"
#include <sstream>

namespace TrafSim
{

ParseError::ParseError(std::string _info): info(_info) {}

// strip whitespace from the stream
void discardWhitespace(std::istream& is)
{
	char c = ' ';
	while (c == ' ' || c == '\n' || c == '\t') 
	{
		if (is.eof())
			return;
			
		is.get(c);
	}
	
	is.putback(c);
}

std::string trim(std::string const& str, char const* delims)
{
	std::string::size_type const first = str.find_first_not_of(delims);
	return ( first == std::string::npos ) ? 
			std::string() : 
			str.substr(first, str.find_last_not_of(delims) - first + 1);
}

void matchString(std::istream& is, std::string const& str)
{
	unsigned int i = 0;
	char c = ' ';
	bool eof = false;
	
	try
	{		
		discardWhitespace(is);
		do is.get(c); while (str[i++] == c && !is.eof());
	}
	catch (std::exception& e) { eof = true; } // means EOF, with eofbit or failbit set

	if (i == str.length() && eof)
		is.clear();
	else if (i == str.length() + 1)
		is.putback(c);
	else
	{
		std::cerr << i << " " << c << std::endl;
		throw ParseError("expected '" + str + "'");	
	}
}

std::string readDelim(std::istream& is, char delim)
{
	std::ostringstream oss;
	is.get(*oss.rdbuf(), delim);
	is.ignore(1);
	
	return trim(oss.str(), " \t\n");
}

}
