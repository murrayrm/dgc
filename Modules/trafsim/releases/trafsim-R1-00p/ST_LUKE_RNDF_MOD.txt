RNDF_name	St_Lukes_RNDF_Full_Map	
num_segments	4		
num_zones	0	
format_version	1.0	
creation_date	11/27/2006

segment	1	
num_lanes	2	
segment_name	Waypoint_Road	
lane	1.1	
num_waypoints	10	
lane_width	12	
left_boundary	double_yellow	
checkpoint	1.2.4	1	
stop	1.1.10	
exit	1.1.10	3.1.3
exit	1.1.10	3.2.3
1.1.1	34.167553	-118.096183
1.1.2	34.167550	-118.096241
1.1.3	34.167549	-118.096324
1.1.4	34.167549	-118.096413
1.1.5	34.167545	-118.096507
1.1.6	34.167544	-118.096599
1.1.7	34.167542	-118.096693
1.1.8	34.167539	-118.096763
1.1.9	34.167537	-118.096821
1.1.10	34.167537	-118.096870
end_lane		
lane	1.2	
num_waypoints	10	
lane_width	12	
left_boundary	double_yellow
checkpoint	1.2.3	2
checkpoint	1.2.4	3
checkpoint	1.2.5	4
checkpoint	1.2.6	5
checkpoint	1.2.7	6
checkpoint	1.2.8	7
checkpoint	1.2.9	8
stop	1.2.10	
exit	1.2.10	4.1.11
exit	1.2.10	4.2.3
1.2.1	34.167503	-118.096871
1.2.2	34.167505	-118.096824
1.2.3	34.167509	-118.096762
1.2.4	34.167509	-118.096693
1.2.5	34.167509	-118.096597
1.2.6	34.167513	-118.096501
1.2.7	34.167516	-118.096411
1.2.8	34.167518	-118.096324
1.2.9	34.167520	-118.096238
1.2.10	34.167523	-118.096183
end_lane		
end_segment		

segment	2	
num_lanes	2	
segment_name	Murray_Road	
lane	2.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow
exit	2.1.2	3.1.1
exit	2.1.4	2.2.1
2.1.1	34.167735	-118.096182
2.1.2	34.167723	-118.096882
2.1.3	34.167724	-118.097020
2.1.4	34.167724	-118.097081
end_lane		
lane	2.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
checkpoint	2.2.3	9
stop	2.2.4	
exit	2.2.2	3.1.1
exit	2.2.4	4.1.9
exit	2.2.4	4.2.5
2.2.1	34.167688	-118.097086
2.2.2	34.167688	-118.097023
2.2.3	34.167694	-118.096882
2.2.4	34.167702	-118.096180
end_lane		
end_segment	

segment	3	
num_lanes	2	
segment_name	North_Street	
lane	3.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
stop	3.1.4	
exit	3.1.2	1.2.1
3.1.1	34.167657	-118.096984
3.1.2	34.167569	-118.096983
3.1.3	34.167481	-118.096983
3.1.4	34.167453	-118.096983
end_lane		
lane	3.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow
checkpoint	3.2.3	10	
exit	3.2.2	1.2.1
exit	3.2.4	2.1.3
exit	3.2.4	2.2.3
3.2.1	34.167453	-118.096928
3.2.2	34.167480	-118.096925
3.2.3	34.167568	-118.096929
3.2.4	34.167658	-118.096927
end_lane		
end_segment	

segment	4	
num_lanes	2	
segment_name	St_Luke_Lane	
lane	4.1	
num_waypoints	12	
lane_width	12
left_boundary	double_yellow	
checkpoint	4.1.9	11	
stop	4.1.10	
stop	4.1.12	
exit	4.1.8	2.1.1
exit	4.1.10	1.1.1
4.1.1	34.168000	-118.095329
4.1.2	34.167996	-118.095642
4.1.3	34.167996	-118.095742
4.1.4	34.167979	-118.096027
4.1.5	34.167924	-118.096100
4.1.6	34.167896	-118.096115
4.1.7	34.167818	-118.096135
4.1.8	34.167766	-118.096135
4.1.9	34.167672	-118.096134
4.1.10	34.167579	-118.096131
4.1.11	34.167498	-118.096126
4.1.12	34.167479	-118.096126
end_lane		
lane	4.2	
num_waypoints	12	
lane_width	12
left_boundary	double_yellow	
stop	4.2.2
exit	4.2.2	1.1.1
exit	4.2.4	2.1.1
4.2.1	34.167480	-118.096069
4.2.2	34.167497	-118.096069
4.2.3	34.167581	-118.096072
4.2.4	34.167671	-118.096073
4.2.5	34.167765	-118.096077
4.2.6	34.167822	-118.096078
4.2.7	34.167892	-118.096064
4.2.8	34.167920	-118.096041
4.2.9	34.167962	-118.095956
4.2.10	34.167976	-118.095753
4.2.11	34.167977	-118.095634
4.2.12	34.167981	-118.095346
end_lane		
end_segment

end_file