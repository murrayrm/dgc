<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>Car.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Car_8cc</filename>
    <includes id="Car_8hh" name="Car.hh" local="yes" imported="no">Car.hh</includes>
    <includes id="IDM_8hh" name="IDM.hh" local="yes" imported="no">IDM.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="enumvalue">
      <name>STATE_NORMAL</name>
      <anchor>118938ef36054ba328d21fcb760d46f7cd33e3630ac1d2db26fa9cc95750f604</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_APPROACH_STOP</name>
      <anchor>118938ef36054ba328d21fcb760d46f76ed23e6f008a193ef49564376913ebc4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_WAIT_CAR</name>
      <anchor>118938ef36054ba328d21fcb760d46f77ad4b9e50828d30c8ac4a3996f1ac2d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_WAIT_GAP</name>
      <anchor>118938ef36054ba328d21fcb760d46f7f75a11f8147f747ca7818b6890721118</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_ENTER_INT</name>
      <anchor>118938ef36054ba328d21fcb760d46f7e9a36b7c7e18ae08dafe2ad57e1ad620</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_CROSSING_INT</name>
      <anchor>118938ef36054ba328d21fcb760d46f7f19af5e9a559dc0847b91d1d99c21f1b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Car.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Car_8d</filename>
  </compound>
  <compound kind="file">
    <name>Car.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Car_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <includes id="Lane_8hh" name="Lane.hh" local="yes" imported="no">Lane.hh</includes>
    <includes id="Intersection_8hh" name="Intersection.hh" local="yes" imported="no">Intersection.hh</includes>
    <includes id="CarProperties_8hh" name="CarProperties.hh" local="yes" imported="no">CarProperties.hh</includes>
    <includes id="Road_8hh" name="Road.hh" local="yes" imported="no">Road.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::CarInterface</class>
    <class kind="class">TrafSim::Car</class>
  </compound>
  <compound kind="file">
    <name>CarDriver.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CarDriver_8py</filename>
    <namespace>CarDriver</namespace>
    <namespace>libtrafsim_Py::*</namespace>
    <class kind="class">CarDriver::CarDriver</class>
  </compound>
  <compound kind="file">
    <name>CarFactory.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CarFactory_8cc</filename>
    <includes id="CarFactory_8hh" name="CarFactory.hh" local="yes" imported="no">CarFactory.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="define">
      <type>#define</type>
      <name>RAND_RANGE</name>
      <anchorfile>CarFactory_8cc.html</anchorfile>
      <anchor>31de710ae918bf85873917e5e1762ba6</anchor>
      <arglist>(A, B)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isObstructed</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>bf389411fc611f79a746d3f1c35645b2</anchor>
      <arglist>(Lane *lane, float t, float threshold)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CarFactory.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>CarFactory_8d</filename>
  </compound>
  <compound kind="file">
    <name>CarFactory.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CarFactory_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Car_8hh" name="Car.hh" local="yes" imported="no">Car.hh</includes>
    <includes id="CarProperties_8hh" name="CarProperties.hh" local="yes" imported="no">CarProperties.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::CarFactory</class>
    <member kind="enumeration">
      <name>CarFactoryFailMode</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CF_FAIL_WAIT</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86d17799a62a2c24faac32fb138698570e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CF_FAIL_SKIP</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86c9708ecd80e1bea8e59d72c5e993a3cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CF_FAIL_OVERLAP</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86b4258922313ed5909ea0b64663a8809b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CarProperties.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CarProperties_8cc</filename>
    <includes id="CarProperties_8hh" name="CarProperties.hh" local="yes" imported="no">CarProperties.hh</includes>
    <includes id="Serialization_8hh" name="Serialization.hh" local="yes" imported="no">Serialization.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>f2253a992e82639cfca1c1f7d89e770d</anchor>
      <arglist>(std::ostream &amp;os, CarProperties const &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>1a017d6feec4e48d89f709c5b38ca698</anchor>
      <arglist>(std::istream &amp;is, CarProperties &amp;props)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CarProperties.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>CarProperties_8d</filename>
  </compound>
  <compound kind="file">
    <name>CarProperties.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CarProperties_8hh</filename>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::CarProperties</class>
  </compound>
  <compound kind="file">
    <name>Color.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Color_8cc</filename>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>void</type>
      <name>glColor4f</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>610e9609d1e15a65939f7fb5fed864b6</anchor>
      <arglist>(float, float, float, float)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>glSetColor</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>03ed727b566d08df4bdb29dafc9542db</anchor>
      <arglist>(Color const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>edc3bbb875348c83843d897ae9d56626</anchor>
      <arglist>(std::ostream &amp;os, Color const &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>2373e5ed11c04237e4500b7931c57ff0</anchor>
      <arglist>(std::istream &amp;is, Color &amp;c)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Color.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Color_8d</filename>
  </compound>
  <compound kind="file">
    <name>Color.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Color_8hh</filename>
    <includes id="Serialization_8hh" name="Serialization.hh" local="yes" imported="no">Serialization.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Color</class>
    <member kind="function">
      <type>void</type>
      <name>glSetColor</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>03ed727b566d08df4bdb29dafc9542db</anchor>
      <arglist>(Color const &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CurveConstraint.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CurveConstraint_8cc</filename>
    <includes id="CurveConstraint_8hh" name="CurveConstraint.hh" local="yes" imported="no">CurveConstraint.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="define">
      <type>#define</type>
      <name>min</name>
      <anchorfile>CurveConstraint_8cc.html</anchorfile>
      <anchor>c6afabdc09a49a433ee19d8a9486056d</anchor>
      <arglist>(a, b)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>curveConstraint</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ceedfff07db33764431ef7195fa49637</anchor>
      <arglist>(Spline const &amp;spline, float t, float vel, float linearAccel, float angularAccel)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>CurveConstraint.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>CurveConstraint_8d</filename>
  </compound>
  <compound kind="file">
    <name>CurveConstraint.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>CurveConstraint_8hh</filename>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>float</type>
      <name>curveConstraint</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ceedfff07db33764431ef7195fa49637</anchor>
      <arglist>(Spline const &amp;spline, float t, float vel, float linearAccel, float angularAccel)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DemExporter.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>DemExporter_8py</filename>
    <namespace>DemExporter</namespace>
    <member kind="function">
      <type>def</type>
      <name>ExportRoadData</name>
      <anchorfile>namespaceDemExporter.html</anchorfile>
      <anchor>2e519c932fb253d43eddaa0c7d1ab270</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>MergeImageIntoDem</name>
      <anchorfile>namespaceDemExporter.html</anchorfile>
      <anchor>cfae90bf2658cd4c077ba6ece3ef2ccb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Environment.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Environment_8cc</filename>
    <includes id="Environment_8hh" name="Environment.hh" local="yes" imported="no">Environment.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>273fb26838444932c42b2f0ad2a897c7</anchor>
      <arglist>(std::ostream &amp;os, Environment const &amp;env)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fb8c1c6373fdea81b61986b9b7e5b151</anchor>
      <arglist>(std::istream &amp;is, Environment &amp;env)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Environment.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Environment_8d</filename>
  </compound>
  <compound kind="file">
    <name>Environment.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Environment_8hh</filename>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Road_8hh" name="Road.hh" local="yes" imported="no">Road.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Environment</class>
    <member kind="typedef">
      <type>std::map&lt; std::string, Object * &gt;</type>
      <name>ObjectMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c5a24828073ec4b549ef717e3c88cff0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, ObjectMap &gt;</type>
      <name>EnvironmentMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b73d3ba98865b191467b92a3abe8a048</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, unsigned int &gt;</type>
      <name>NameMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b9e825e599a18a2caaaf15cb82ad996c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, std::vector&lt; std::vector&lt; point2 &gt; &gt; &gt;</type>
      <name>BoundaryMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c5e073fd109afbcf5abc521192bd6436</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Event.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Event_8cc</filename>
    <includes id="Event_8hh" name="Event.hh" local="yes" imported="no">Event.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>Event.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Event_8d</filename>
  </compound>
  <compound kind="file">
    <name>Event.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Event_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <namespace>TrafSim</namespace>
    <namespace>std</namespace>
    <class kind="class">TrafSim::Event</class>
  </compound>
  <compound kind="file">
    <name>ExternalCar.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>ExternalCar_8cc</filename>
    <includes id="ExternalCar_8hh" name="ExternalCar.hh" local="yes" imported="no">ExternalCar.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>ExternalCar.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>ExternalCar_8d</filename>
  </compound>
  <compound kind="file">
    <name>ExternalCar.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>ExternalCar_8hh</filename>
    <includes id="Car_8hh" name="Car.hh" local="yes" imported="no">Car.hh</includes>
    <includes id="LaneGrid_8hh" name="LaneGrid.hh" local="yes" imported="no">LaneGrid.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::ExternalCar</class>
  </compound>
  <compound kind="file">
    <name>GenericApp.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>GenericApp_8py</filename>
    <namespace>GenericApp</namespace>
    <class kind="class">GenericApp::GenericApp</class>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>app</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>d35d7d078472c5ad0823e1ba4ed8dcec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadRNDF</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>38e8ff6fb5146221df2da4f00baa8b83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFunction</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>6100ce97c4fda75d73a213c3cbfc0180</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFile</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>a4ee411dd43ff27c542a8cc30a46867f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>subgroup</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>e43a0dbec84f1fb92e26b10708a76933</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>wait</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>373fe64c37d9b53ef0c6aa871d311b9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>float</type>
      <name>sleeptime</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>9167921ce55fb9583078c1cd98dd928a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>rndfname</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>5e432fab28f242274d06342c15d8f804</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadRNDF</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>38e8ff6fb5146221df2da4f00baa8b83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>functionname</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>4de2d9c14c10f7240f8b24e29fd48659</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFunction</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>6100ce97c4fda75d73a213c3cbfc0180</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>filename</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>27ffdba40ae3e033165d177535760dc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFile</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>a4ee411dd43ff27c542a8cc30a46867f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>subgroup</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>f1ea8202b139ebb8d2b0f2243547ec37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>wait</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>373fe64c37d9b53ef0c6aa871d311b9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>sleeptime</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>80f5161a44c927807fd46135156064c6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>IDM.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>IDM_8cc</filename>
    <includes id="IDM_8hh" name="IDM.hh" local="yes" imported="no">IDM.hh</includes>
    <includes id="CarProperties_8hh" name="CarProperties.hh" local="yes" imported="no">CarProperties.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>float</type>
      <name>evalIDMAccel</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>35b3f51ad45f372da9102edf891d0b82</anchor>
      <arglist>(float myVel, float otherVel, float gap, float targetVel, CarProperties const &amp;myProps, CarProperties const &amp;otherProps)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>IDM.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>IDM_8d</filename>
  </compound>
  <compound kind="file">
    <name>IDM.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>IDM_8hh</filename>
    <includes id="CarProperties_8hh" name="CarProperties.hh" local="yes" imported="no">CarProperties.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>float</type>
      <name>evalIDMAccel</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>35b3f51ad45f372da9102edf891d0b82</anchor>
      <arglist>(float myVel, float otherVel, float gap, float targetVel, CarProperties const &amp;myProps, CarProperties const &amp;otherProps)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Intersection.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Intersection_8cc</filename>
    <includes id="Intersection_8hh" name="Intersection.hh" local="yes" imported="no">Intersection.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>Intersection.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Intersection_8d</filename>
  </compound>
  <compound kind="file">
    <name>Intersection.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Intersection_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Road_8hh" name="Road.hh" local="yes" imported="no">Road.hh</includes>
    <includes id="Environment_8hh" name="Environment.hh" local="yes" imported="no">Environment.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="struct">TrafSim::ManeuverDestMap</class>
    <class kind="struct">TrafSim::ManeuverLaneMap</class>
    <class kind="struct">TrafSim::Queue</class>
    <class kind="struct">TrafSim::IntersectionGeometryParams</class>
    <class kind="class">TrafSim::Intersection</class>
    <class kind="class">TrafSim::DeadEnd</class>
    <member kind="typedef">
      <type>std::pair&lt; Road *, unsigned int &gt;</type>
      <name>RoadLane</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>58408b9657f0c7e789a3be517dfc9306</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>RightOfWay</name>
      <anchor>efba763deb151aa5e18072cfcff1064f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ROW_GO</name>
      <anchor>efba763deb151aa5e18072cfcff1064febbac998fc9462476d3e87a6d3c81c56</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ROW_GAP</name>
      <anchor>efba763deb151aa5e18072cfcff1064f6037de699e8a9bd6a98f5b5eafc3d0a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>CrossManeuver</name>
      <anchor>01761d7c70c6c6683036c3436743973d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CROSS_LEFT</name>
      <anchor>01761d7c70c6c6683036c3436743973db4a9eceb412ba0167d55a12dd75dc798</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CROSS_RIGHT</name>
      <anchor>01761d7c70c6c6683036c3436743973d1f8cc67545fe0fbaf98ccf199cf8fd91</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CROSS_STRAIGHT</name>
      <anchor>01761d7c70c6c6683036c3436743973d6e93787da572979dadd8d538ed226798</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Lane.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Lane_8cc</filename>
    <includes id="Lane_8hh" name="Lane.hh" local="yes" imported="no">Lane.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="typedef">
      <type>std::map&lt; Object *, float &gt;::iterator</type>
      <name>OtfIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>2fba76fc7fe8d581578a7c71c18b9cee</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::multimap&lt; float, Object * &gt;::iterator</type>
      <name>FtoIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b643ee3f3a5624c82adf16e3623ea63e</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::multimap&lt; float, LaneInterferenceDesc &gt;::iterator</type>
      <name>InterfIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>2abb93d1f6c1eb34d074ff61fb1739e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; LaneSegment &gt;::iterator</type>
      <name>SubLaneIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b2e9fdd2bc60ff2c7077eae4945026aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; LaneSegment &gt;::const_iterator</type>
      <name>ConstSubLaneIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b9a76a5333a8ee63ff2e7f7c6417c61b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>cc58a736b80d7d6ec75594245f32ae40</anchor>
      <arglist>(std::ostream &amp;os, LaneInterferenceDesc const &amp;desc)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>7ce1691595eacca2cab5a9b0232aa4e2</anchor>
      <arglist>(std::istream &amp;is, LaneInterferenceDesc &amp;desc)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Lane.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Lane_8d</filename>
  </compound>
  <compound kind="file">
    <name>Lane.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Lane_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="LinearSegmentDesc_8hh" name="LinearSegmentDesc.hh" local="yes" imported="no">LinearSegmentDesc.hh</includes>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="struct">TrafSim::LaneInterferenceDesc</class>
    <class kind="class">TrafSim::Lane</class>
    <class kind="class">TrafSim::MultiLane</class>
    <member kind="typedef">
      <type>TrafSim::LinearSegmentDesc&lt; Lane &gt;</type>
      <name>LaneSegment</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>63c0d2f5bd275e219a9475f6d6e38896</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>LaneGrid.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>LaneGrid_8cc</filename>
    <includes id="LaneGrid_8hh" name="LaneGrid.hh" local="yes" imported="no">LaneGrid.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>LaneGrid.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>LaneGrid_8d</filename>
  </compound>
  <compound kind="file">
    <name>LaneGrid.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>LaneGrid_8hh</filename>
    <includes id="Environment_8hh" name="Environment.hh" local="yes" imported="no">Environment.hh</includes>
    <includes id="Lane_8hh" name="Lane.hh" local="yes" imported="no">Lane.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::LaneGrid</class>
  </compound>
  <compound kind="file">
    <name>libtrafsim_Py.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>libtrafsim__Py_8py</filename>
    <namespace>libtrafsim_Py</namespace>
    <class kind="class">libtrafsim_Py::_object</class>
    <class kind="class">libtrafsim_Py::PySwigIterator</class>
    <class kind="class">libtrafsim_Py::Vector</class>
    <class kind="class">libtrafsim_Py::Color</class>
    <class kind="class">libtrafsim_Py::ViewportEvent</class>
    <class kind="class">libtrafsim_Py::Viewport</class>
    <class kind="class">libtrafsim_Py::Object</class>
    <class kind="class">libtrafsim_Py::Environment</class>
    <class kind="class">libtrafsim_Py::Spline</class>
    <class kind="class">libtrafsim_Py::LaneInterferenceDesc</class>
    <class kind="class">libtrafsim_Py::Lane</class>
    <class kind="class">libtrafsim_Py::MultiLane</class>
    <class kind="class">libtrafsim_Py::ManeuverDestMap</class>
    <class kind="class">libtrafsim_Py::ManeuverLaneMap</class>
    <class kind="class">libtrafsim_Py::Queue</class>
    <class kind="class">libtrafsim_Py::IntersectionGeometryParams</class>
    <class kind="class">libtrafsim_Py::Intersection</class>
    <class kind="class">libtrafsim_Py::DeadEnd</class>
    <class kind="class">libtrafsim_Py::CarProperties</class>
    <class kind="class">libtrafsim_Py::CarInterface</class>
    <class kind="class">libtrafsim_Py::Car</class>
    <class kind="class">libtrafsim_Py::ExternalCar</class>
    <class kind="class">libtrafsim_Py::CarFactory</class>
    <class kind="class">libtrafsim_Py::Road</class>
    <class kind="class">libtrafsim_Py::XIntersection</class>
    <class kind="class">libtrafsim_Py::YIntersection</class>
    <class kind="class">libtrafsim_Py::LaneGrid</class>
    <class kind="class">libtrafsim_Py::SegInfo</class>
    <class kind="class">libtrafsim_Py::IntersectionInfo</class>
    <class kind="class">libtrafsim_Py::RNDFSimIF</class>
    <class kind="class">libtrafsim_Py::Obstacle</class>
    <class kind="class">libtrafsim_Py::Line</class>
    <class kind="class">libtrafsim_Py::Event</class>
    <class kind="class">libtrafsim_Py::SplineCar</class>
    <class kind="class">libtrafsim_Py::ObjectDictPair</class>
    <class kind="class">libtrafsim_Py::ObjectDict</class>
    <class kind="class">libtrafsim_Py::VectorArray</class>
    <class kind="class">libtrafsim_Py::LaneObject</class>
    <class kind="class">libtrafsim_Py::JoinDesc</class>
    <class kind="class">libtrafsim_Py::LaneSegment</class>
    <class kind="class">libtrafsim_Py::JoinDescArray</class>
    <class kind="class">libtrafsim_Py::LaneSegmentArray</class>
    <class kind="class">libtrafsim_Py::CarPath</class>
    <class kind="class">libtrafsim_Py::SplineArray</class>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_setattr_nondynamic</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>18e47dc885313d2d125ba6d77379ee1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_setattr</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5dc7a6d6d2796d55132576f83ff91e99</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_getattr</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6cb8a096e6550a02095d18aec765ed30</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_repr</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5e3c487f02f112d6d060c47c6cb40f0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_dot</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b14eb114d50a034b7fe979934d7b73f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_dist2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>502a01e73eb64bf151a82331e8fe3240</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_intersection</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0dad17a4ac53b06075a028e39a1b5adb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b9abc01ba02ea9fd6ae1e7ed1c1fdff5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Color_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d29592fbc81002f9a25c04b68e047c12</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>glSetColor</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0ba61e1d76bc0548e68bf6dcd33db491</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Object_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5db313beaeb9b782e6b98b30bc9cc19e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Environment_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9cd800550ae00dabe69503bd8022ef07</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Spline_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e201d64d6ed247c8a72eb828a95aaacd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Spline_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b97c38e23482b8a7cc17a3247bf06a06</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Lane_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d424c2937fa239638ed4452425714b61</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>MultiLane_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2c70a90d81eb4300af36ba8aba117e94</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Intersection_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2996cf3bdb7d168dad6215e1be0f5275</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Car_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c8521cc13f66b3f6674475311f59dc2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>ExternalCar_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5006166bbf980f9100d6d56c8bad8df1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>CarFactory_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>350d8a66fa06f1e15735feb3bf2baf53</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Road_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aba84ac1364b8fbe10776c00f310dba5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>XIntersection_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>24948acfa98d91e98f1f3121a2d25269</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>YIntersection_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21ea9e86c9a77559794d30ef52d07805</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Obstacle_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f34f85e98de8581ee740bc8f587b51ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Line_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fd3bbbfc83be85d34d3a2d77099ac393</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>SplineCar_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce8a82edf77a3adfeaeb9aa085cab3c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>__disown_METHOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4cc07fee528cd5d582d7f30e0bff184e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>new_instancemethod</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b13e709bf249c0219193408c1fe30211</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>_swig_property</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3c96424d186d6df137dac1944c04532b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>_object</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b51728f599d6e22c14f7a7ff6aa7b76e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>_newclass</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>36038e2f51ae3ca23d82f0f69b029fa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>_newclass</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>36038e2f51ae3ca23d82f0f69b029fa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>PySwigIterator_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>de22a49f5a3d9c13a014e3ac6e6a0fcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UNKNOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d49e44a45f749b7a6c73f1445f9fc8ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_FIRST</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b44c83f5cbd29c00759b25efd4db6690</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BACKSPACE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>87705c544f18dea5ffa31665f5359f85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_TAB</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e455d61e17e5d109fdf51acd8b4eef3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_CLEAR</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>865b13f96f3ef6ef2ab7934dbb2870bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RETURN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>abf1e58b31f8442eb316e81732856079</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PAUSE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>882fc9097935c3f1af6b3f57b0372e01</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_ESCAPE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2e83967d1fa3ba2cef48f94a794d7575</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SPACE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>013554a3fa3d78df32ae971dc7497ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_EXCLAIM</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>751a781af9d01baac401ae13d19859ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_QUOTEDBL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1d419692b6cff35cf43e363723fa8ffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_HASH</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e49144544e77b999ae749b2dacb2888f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_DOLLAR</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fc9ffe4c0d304220d24ae348be526f99</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_AMPERSAND</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>47e57078618e6eb1af892b337b3e32d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_QUOTE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>db735923e61dc3c1d9b9c61c2a3595f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LEFTPAREN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f028dd671bb225ede28e3ebca241ffab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RIGHTPAREN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>05150ffe56918171f8d949790c669b66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_ASTERISK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9f8945442e4028abcd8fe588ff33bab7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PLUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>403442b91005b7d277759256ac746e16</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_COMMA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b6961d3f85807b2dc03c7004cb153678</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_MINUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>bd4320e09cf875b75dd00c52c6595c07</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PERIOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>66f650ad5f72f2ae77ef53d3545c7fc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SLASH</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f08b099767edcb9d2bf83e098103aad0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_0</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6900ac8843a64b7b5e3daabadf57ac8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1507e540c2f02791f67f3538df8a4e5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7b639572ebac8a084489091e90c3068a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a51d74e20c774bbb0e62c5dbac8e2e64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>169d1b8d02241bc581b8b159aded46da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f1fd0a44cbdd3f9827b6a82d9c66d324</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>65c0788dc287a0b165101feca6776cdd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0532b1f3e76a3c3c1452222924dfdb62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>09430d8ed34c6fae0717444d44eb1d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3976e24f1e04d5713b6e8183d8694019</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_COLON</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>17b3873dabdf191def8d5410fd11bb14</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SEMICOLON</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>301d515db169c3f1008bd639c2a6bacb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LESS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce70a1d9d962d7faa632a12833c04326</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_EQUALS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0fbb1a3236b77768b4007d8318ccdf96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_GREATER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>54ad7dd712fe2474e913e0cb92ab77b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_QUESTION</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1ce36638f5fa9e773ee0163d0725da34</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_AT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>412494d45f3666b701a9752d9139e1e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LEFTBRACKET</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>784d48c7b605fa162a702788c030f8c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BACKSLASH</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5e3a4b584e166fdf208b6f32056aebb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RIGHTBRACKET</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f063b0594cc99db935b6f96329d55c2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_CARET</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b9afc735ad8c80587fcbdc466cde0483</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UNDERSCORE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a3e07c70af96e17bdf9401ce48917c04</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BACKQUOTE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed20448baf24ed70b6abe4d160b5607e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_a</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21053cb1e4095e32ebdbb37df17ebab8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_b</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fd88a9b56e845a241938f5fe8e068416</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_c</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0eb65f8918ae0da8570bb676fd2da557</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_d</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7767432a4710f78231d3961a01ba07d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_e</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>50d8d374594d7566333d3a00e519295b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_f</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>78aed06bd3e5741332ee3ff5857cccb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_g</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>96c2b8cd00883a2ff0af12994661a7c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_h</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fbef2bbacfa6d321bb9d9d4f46b758ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_i</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e54eb72a12a7ae54a9676fde9e356143</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_j</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>131bcc876d4d9c542439a0fe22c58df3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_k</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>027edc0fe462ab92b33884a85efc5bb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_l</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aff46b405018a17a3356352e46455241</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_m</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>50e815015375317df9faf609d0d1e03b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_n</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3a8cbece670673af9f6ec7b7f7991b89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_o</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7aea0b27e6cad442846900833e6fa420</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_p</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a4a1fcb8b1328f915bfc7336c1cd03c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_q</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9f3f82b99c86a121d0c1e34db9c3033f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_r</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3f79a3a3dd74b59236fd983096196569</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_s</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c63f4c188da4519c93aa9907d998eeeb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_t</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b57d41bce14a952f124907cf822fb232</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_u</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ec18fbf4a277ca04874cbbea28597efa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_v</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce6694404b71c475852c5790097d2624</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_w</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ac7544fab60dbe7262f37425ce4c1ec8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_x</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>375646443d16a2651bce69a81120156f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_y</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7ddd37a90a042ae9160a5a3718265bae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_z</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>412c87f8ae07ec272e19235211ce683a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_DELETE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>50b40dc1d2f66ada192c3c2ce80e1c4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_0</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8d93fef62bd16e38a571bc147b3ee16b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f5a89f49301b1045fea940a9398ad1ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>782ba12eb14de9cc324cedff5210f13c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0a08945b0e14e26d5ab24882db51d320</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>60a124c1a46e02df7d209f98fdd0fbd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce889181c898bde7c3d85faa5090fe7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>78529c4aa07c3334c26683f5edd43725</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4589cbab3fd79b60761c694855ea7d25</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>534d82d337e557a95ec6b4a3466fb1d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>13c2031f40fc08f88e6f6db4329de354</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_10</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>26bf6a3c853a0cd7ca5d3564f61288a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_11</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2cdf6edff280e68cbc165f424e03312a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_12</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b1d6c535dc37621011ea65b3a699754</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_13</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a1eb1e16f31fac115984c6ab55afc47f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_14</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1f71015c1a8f4536de5b4f448cf39150</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_15</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>435f110a2a376a4b4c27f3b45950123b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_16</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4718d2dd5fabbb9f0d24b3fc33e91459</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_17</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6b01aac3cb3231f36ec55ca441e970e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_18</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1e5ddb3c1e591bd6893d9e7e2deeeb87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_19</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>52f7061a623a74d0f1b5b56afc591882</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_20</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f13f89f1170dacf53d26b4362e2203d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_21</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>df380bccb5f050611fd31f3f22a1d0c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_22</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>dd2438920c04e7468de6af7dea4ba59f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_23</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c28755f28fc71bd98c73ca1574bd3f0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_24</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>021d7127fef995db0e8866792e3e7dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_25</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b437a2f5d3acd31cf09e2dc88443d7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_26</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c3e0d14975feb63c9bb4311726b2beb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_27</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>49716d8c36fc3f09149000e91224e420</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_28</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d03406dc787a69315c3dd70eda7512a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_29</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ec5d34b19a4105edbaf490ea2186c6dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_30</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>01c27e5c8d8480aa49a9177b379f083d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_31</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3f956c246b5e9cba6c5c50a7fd9cc157</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_32</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>52ccabcb87c0049e7867acb449a52abb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_33</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>05fb0291f85bef9b0ad18ccdeb710f0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_34</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>31f109f6cf176084b0e0aa588446f791</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_35</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5d2c29068c57183d394609f27309a7c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_36</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>331ce2fdd0d988f5413f0d2c62155cfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_37</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21ae0b571fdb095e13effb5f4bf122e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_38</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4585bab4b271894b6d7c6911c0a7d35a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_39</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f4fc2bdc960aa195729fadfab271260d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_40</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2383c7f013d04854eb89219291179ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_41</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21774a7a25f70d9658981cfc830904ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_42</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>17b3966a747122c6a07f028e6f1fc5a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_43</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2adc863246ab8a1fa54e66f54673c441</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_44</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d7075b33f9aa0008ff24b1b13eae6080</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_45</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c6d739c39fe331c03c6ad8d3f506afb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_46</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>757b627839782aabe4e842c407194780</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_47</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1a1cf67e0edb6f3bfc93fc0a70f22492</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_48</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2d81108259ea77b963cf330311e064f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_49</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a0a5dfa13b98ba34dc9d2272049767f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_50</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7bb6a01dce65b7dfdbaf6e0d39781d61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_51</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1064b80e4a0be1b4508425052d28b393</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_52</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>089b8bf3634f8dc0b4d8fb99451828f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_53</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>182abd7ccba51235b3db675ad81ccb9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_54</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>831d0ab9be419a29ebc76991f470df37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_55</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>df3a3268ecf6de5c6662e6531d9c3896</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_56</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1920638e8fb8df2ea9f07219a70f902d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_57</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>cd2254d66aabf795f37859ee2097440f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_58</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b9742e6c81ade5088bfae077428d0d92</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_59</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>424ca73ba83e0a270dcdd48b48912b1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_60</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9ab8f830f98ca5ec638a08244f1c89fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_61</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a5b426dd707b1537a1ae8416448357d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_62</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6e84b594bb0cb98987836d8d83a5df96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_63</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>55d8a0e8cd218516f82d60becb4232fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_64</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>05281ef10e1580220940b035600a4882</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_65</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>809751403d9a69f25a0f96c9b649421e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_66</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed177e281d627ec8c2e7195b27f51168</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_67</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>eae612f447cb16a1da15ccfc0cc1ee0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_68</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4b16cd660a551a8931fd4511d58cc283</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_69</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3b87ed8724541be3a471606b306472f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_70</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>33ca28bace843f9dadc30e9313cabf0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_71</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>bf83e1d106e3267488a4abff702bfe12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_72</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>dc042bb4eb9fa8705336b33953026809</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_73</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0ed297a43e354fdd4266a26db4a123a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_74</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>476b931d9569f6f9c853ba03e333982e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_75</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>97634201d24ddb0659dfcf22a8e4dc4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_76</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e523b94af0849a834eec6f2557cb8ce0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_77</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>585e5ddaacaeafe1367844954edb7667</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_78</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1813acfd1c04a665961a15da406a833b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_79</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1254f86d99ee13523202f1bad64edf11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_80</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>99c8ca5ec6066158eb19799de75d0d05</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_81</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5efbf007466505185ab16f7a5b2eac09</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_82</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>372be3f2a79f8f244a1a7cc1977db601</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_83</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e278fcaa75b773586b42d84e90888d4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_84</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b27cf58e0101ef40bbf8b836a3a8befd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_85</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b943cea4bf56388403d595f550fb4d76</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_86</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4e50bc6e47304efff508e50c43176988</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_87</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>386f1c6fc77e5b7fc4e96cdd51f8b26b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_88</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9a1c8a436c2bbc96c95bec293c6fa483</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_89</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>311b1966cabd151a5a16cad698c80242</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_90</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d9d8cd53ce186c0b45b065f022aa810b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_91</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>809a169cd5db8ade5f8619fbe0c488e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_92</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>65e9d7895c7d6a39cee526848e37b951</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_93</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0b333797ebddc876a1ab617c226fc064</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_94</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b8df56eb9e5ca9d380732805e9ea34d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_95</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9662ddf36bf35cabe5d2024a38505c51</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP0</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2d159acbc549f90d83d572b8f17de83a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>41cefd3bdcfd81c94f9ab66e3686e797</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a2d087d9bcb9680be60a138ce103b888</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f1524c1a78ab30f7e8f98ee42b1759fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>57b6c36bc759b35fc3de969fa5adc7da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a35db62a08c0edfff5fec320685f3e13</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aab798aac4848fcf906556527c69a26c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3b5ef27f362a272b32624cd39371e42a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5e187824cc48de15b9f770c096e2e310</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>486b45b1f5cd9fe4c11a86252477bc4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_PERIOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed5f640ace8b1dca3819b6648a3fa203</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_DIVIDE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>262d0a36746bbd67233563aed6d37654</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_MULTIPLY</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ad56832525aa58967fb9c1da0330a477</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_MINUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>57739ad84d2f944a00f2e183a755b985</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_PLUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1e9b26e787f6b771613693ca0544c046</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_ENTER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>121de346765d0b697cabad60db5010a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_EQUALS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4e7a868c55eccbc1cf08263253f96122</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3d27f3345e2fcaf3efe25d1f03df4ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_DOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>84d86f6808996db976f4f195ee580a0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>db9143da48b9c7ff8f36bae0c0033185</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LEFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>da538923e21064ebfaff1ef61617c753</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_INSERT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>dad8eab3462e47b6347d009d25af35c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_HOME</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fcc9b29d12a0d7104b3b95fda03944a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_END</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>cb44b39843dba5893068d6b68b6c2c17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PAGEUP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0629098a34b400b9401b325fddf718ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PAGEDOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ef9b1a436227d75e37754e13bf561fce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d82a68b6dfece7b340a2f23cf66ac5c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6c46d544a2cec286e20e375fdd5b5e7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9056d762ea604fc00f682ec9872f75ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6b5a22c6972a7576a8c04d523a85f6ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>86c4b81302c7b9ecd95d00df5fb29f65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>77f66c53cd09ded43df0af970b1a2531</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0577ce300a04e3c265b5a9c0db20939e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9892a5df2163a3da97ec3b300997420c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6d20661db2305c3281994532c27be64f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F10</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a9da135f0a2d6e56c0e0259363ceedfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F11</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8165c7012f527fd1765e4b5fff930e27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F12</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2de3f9379156a98d62d86ec3bbcf1d70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F13</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>deb9a6b87657c8fa21551d232325d1fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F14</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>df5e178a3c7dafbfe244a910d6101a98</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F15</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>931bc268a0c0a49db5453558d87c63e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_NUMLOCK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7b2ca96c8e8b8c0ef79bf0a7a7bdd809</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_CAPSLOCK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>27044b706e07d29e78bdf5e0a85cc70b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SCROLLOCK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2183eec2472b632df8e62343595af8af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e52a3e5f49c2ef4e39ca84cbd7d4d815</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>16c1c4b08223a4af012d3535eeb308c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0a096724caea81d9b66c3c1d47453168</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>349a5aa51ae459a125f6b2f3b810ee8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>874daf4a765d416d8fcc2a50fedb1a12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fd9434abf8ec0f68519ed144823fbc3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b005ebf1f00612e86751bad1489f0abb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b4ed29c9dc052cf091e67f5eba7fca78</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LSUPER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6ea9a5d9d544cd87e03a8a991e2a9bd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RSUPER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>73fd2c2c3b21318157c8af8a0f11ee57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_MODE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>38647b20723059f60e5906ba3388d49e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_COMPOSE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7065dbdc83e345db98735330ba3d90f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_HELP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9201c2cd2a74b6bd342eb1cd036e9435</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PRINT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d52f10e8216b08cc81e05795287dc734</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SYSREQ</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f0be4c9c4acb6bf229903b89caba611a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BREAK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8b67d7be286e3b295d5af47238027b96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_MENU</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9d9fbc19a5bff566e8a82c837d9e34a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_POWER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0a68789357a1fd352865d707d01418b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_EURO</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>511916a719cef9fa2f92d7b5d5a408ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UNDO</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b1fb1d5b8edcf82de9da04554b86c330</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LAST</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed1136fae2188ae8ee417d41c80b5c02</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_NONE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7bc6e0cc831d35983e74950b00e3ccfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6a152ed7851f1555fbb5401253eef58a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>520f6e7f01b96ceb66123dfb92f228c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1a732ff9d2a52d8ce8545ff38fa27056</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c1e38d5ff4b134021f041fb3265f7c30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8b14e6dcbc2920bd7dd555ddce7e077d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f3933ebec822161e5bd0c4b0526b163f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>54ecb480005aa8b24259e39e6616cc9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>56a5aef70b45d0799cd9ea4537e2fab8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_NUM</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>69a9fa48afcbee2697db10f0d024de2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_CAPS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8ca6f439b7718c4097265e5b894c6959</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_MODE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2b0997db8885d01743105643d9351829</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RESERVED</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9263d358b5a91ce7659b77d629610b0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Vector_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>10d4b22828d2aee2f249081d505a3371</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Color_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>47ebab2a1099e68dcd6f9787755b3541</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>EMPTY</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7e8ca0bad8209a8060beb9dbcd39d07a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MOUSEMOTION</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>41cbdb02e36d347499736292662445e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MOUSEBUTTONDOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>620afe38ba070507a6ee674e6b81991e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MOUSEBUTTONUP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aa47b0e1da7db633365e0e68d3724ff6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KEYDOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c2a6e3dc4bc26bc9c4779ce8b7c087b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KEYUP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e3f50bfdaf49ecab277b2cac7da5ea49</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>REPAINT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>54c1896b4db8389ef21f51e72dda363d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>QUIT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>457dce6ba5f3eee71e5801da1f8357a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LEFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6565d3237efa1e47f00c39d55d342f00</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MIDDLE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ef7719ae4ac49c246dcd0e1215d69c39</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>RIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2b0e8c025b3edd3ab325c9f0655e45ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ViewportEvent_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b9c289a7bd2c49c6dc0364ab6a94e8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Viewport_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5c165d149f28de0427866e8b709b4d55</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Object_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>33344f3bf97a46b02997a0ddf40dbe58</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Environment_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>98e865ecc6f9da6af5ac58708cc6e851</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Spline_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>01efb9b02eb9f85f2a06bba8f695067b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneInterferenceDesc_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1ebfeb0911e9b9dc621bdac2a90ff2de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Lane_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>401d4e379099895987db552cef942d67</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MultiLane_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ab912def6f4a93ddd735488897f0a49c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ROW_GO</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8f2a7a26179490e1cc971161edf95143</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ROW_GAP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>cd5b7fa5750f122545117fc54d39a5b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CROSS_LEFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>553a62ba8065540d6cb601ff71caf349</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CROSS_RIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>42e8c2a78cdfdbeadc09fb884ff45eb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CROSS_STRAIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9d17401e31294a439d62d6091004e7b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ManeuverDestMap_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c09d9b2076aca6819a8ad67f8d1904af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ManeuverLaneMap_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e490c067d0dacfefbd720c834a8bec87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Queue_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d936d75d37203d6279f20a5d2c2ba297</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>IntersectionGeometryParams_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4083eaf1e3f28c2841ad700151ba0a9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Intersection_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>738e660d2179ee819dbeb809c174d132</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>DeadEnd_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>03ecaa6df39f791dca8f6b49d483bf47</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarProperties_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>bc4769afe3e23bccaee923b1c374c44d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarInterface_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f956904e2a8fc9b17c6cb33ba516756a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Car_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1602629ae42353b306f2acc7c213cb53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ExternalCar_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9269788aadf386bdbe4f74145c4b285d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CF_FAIL_WAIT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f86092e62d8f24bbaeb289e4723a0cbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CF_FAIL_SKIP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9a7aec302ae0919616df4ff7523695cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CF_FAIL_OVERLAP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2a272ed2ba1a2e4e53f1d40b9d031808</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarFactory_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5ff1c1237e8898abb96700abd6374104</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Road_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4218d6a056896f6c3e14860e1e1aab8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>XIntersection_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d7a90dd432b622d98fa45be01c93fb1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>YIntersection_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fb5661999609ce6669d78c54f2e92116</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneGrid_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f0e12c2dd9be46f075bf80f0bd1549f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>APPEND_START</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6564d07117e9da8fd65f656ba6d8755a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>APPEND_END</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1f021849b933b3df1b5684b2d11687c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>APPEND_DIST</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d4fc29143c53a392d865f658d08c9929</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SegInfo_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>acb9b476b4d20dc6cd109c36a6038119</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>IntersectionInfo_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>61ec4d0c421b97080f49620cf24c3d6e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>RNDFSimIF_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>415ff325340fa6057d08f8dcf09f4154</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Obstacle_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4d895baaa55a2876a1bd4a7e672c496b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Line_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5f8bc25ec9842a245321537be71cd50e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Event_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>10ac9c5631ef6462d754a43252ed2f98</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SplineCar_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d6ea8b3108046118effcd68d31af7442</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ObjectDictPair_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4e3720fcaacd764345dc29336e676029</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ObjectDict_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ded12c530c3e1678607bbd3fc36204a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>VectorArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7b93ffa673e5f19f88ac8cc8958e94a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneObject_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f41b067e15ffe3014ebab27f7cf5d2d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>JoinDesc_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c2b1ea6cbc29dfd8d134dd70360523bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneSegment_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ae7cf5716fda59aea987962f3ee9de93</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>JoinDescArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2bacc4952cd391fbf42b4578de2e42f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneSegmentArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f068daecfed87dcd3b42d0f781772c97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarPath_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ebd5a70b7568bc5e313edb0f6c150df3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SplineArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fe7a70d56aab6b9c80ffad53f2e6ef99</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>__naked_CLASS_METHOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>daac502a4b34306d98af1e4285504b23</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Line.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Line_8cc</filename>
    <includes id="Line_8hh" name="Line.hh" local="yes" imported="no">Line.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>Line.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Line_8d</filename>
  </compound>
  <compound kind="file">
    <name>Line.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Line_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Line</class>
  </compound>
  <compound kind="file">
    <name>LinearSegmentDesc.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>LinearSegmentDesc_8hh</filename>
    <includes id="Serialization_8hh" name="Serialization.hh" local="yes" imported="no">Serialization.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="struct">TrafSim::LinearSegmentDesc</class>
  </compound>
  <compound kind="file">
    <name>Object.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Object_8cc</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Serialization_8hh" name="Serialization.hh" local="yes" imported="no">Serialization.hh</includes>
    <includes id="Environment_8hh" name="Environment.hh" local="yes" imported="no">Environment.hh</includes>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <includes id="Lane_8hh" name="Lane.hh" local="yes" imported="no">Lane.hh</includes>
    <includes id="Car_8hh" name="Car.hh" local="yes" imported="no">Car.hh</includes>
    <includes id="Road_8hh" name="Road.hh" local="yes" imported="no">Road.hh</includes>
    <includes id="Intersection_8hh" name="Intersection.hh" local="yes" imported="no">Intersection.hh</includes>
    <includes id="XIntersection_8hh" name="XIntersection.hh" local="yes" imported="no">XIntersection.hh</includes>
    <includes id="YIntersection_8hh" name="YIntersection.hh" local="yes" imported="no">YIntersection.hh</includes>
    <includes id="CarFactory_8hh" name="CarFactory.hh" local="yes" imported="no">CarFactory.hh</includes>
    <includes id="LaneGrid_8hh" name="LaneGrid.hh" local="yes" imported="no">LaneGrid.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>6945070048ef9d72e476a2783101e059</anchor>
      <arglist>(std::ostream &amp;os, Object *obj)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>4f6b0d0671267891ee6726f19b79ce65</anchor>
      <arglist>(std::istream &amp;is, Object *&amp;obj)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Object.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Object_8d</filename>
  </compound>
  <compound kind="file">
    <name>Object.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Object_8hh</filename>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Object</class>
    <member kind="typedef">
      <type>std::multimap&lt; std::string, Object ** &gt;</type>
      <name>DependencyMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>38338ffa3c131debde6b5142eba3c3fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, Object *(*)()&gt;</type>
      <name>DispatchMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>3c047e5ea20ab22bf1d85dca0fd6c8b7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Obstacle.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Obstacle_8cc</filename>
    <includes id="Obstacle_8hh" name="Obstacle.hh" local="yes" imported="no">Obstacle.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>Obstacle.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Obstacle_8d</filename>
  </compound>
  <compound kind="file">
    <name>Obstacle.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Obstacle_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Obstacle</class>
  </compound>
  <compound kind="file">
    <name>RNDFSimIF.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>RNDFSimIF_8cc</filename>
    <includes id="RNDFSimIF_8hh" name="RNDFSimIF.hh" local="yes" imported="no">RNDFSimIF.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>RNDFSimIF.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>RNDFSimIF_8d</filename>
  </compound>
  <compound kind="file">
    <name>RNDFSimIF.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>RNDFSimIF_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::SegInfo</class>
    <class kind="class">TrafSim::IntersectionInfo</class>
    <class kind="class">TrafSim::RNDFSimIF</class>
    <member kind="define">
      <type>#define</type>
      <name>APPEND_START</name>
      <anchorfile>RNDFSimIF_8hh.html</anchorfile>
      <anchor>6793dc272a408ba8517784ef7a5fa9b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>APPEND_END</name>
      <anchorfile>RNDFSimIF_8hh.html</anchorfile>
      <anchor>cae950ebd6b21859c95967ac438301dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>APPEND_DIST</name>
      <anchorfile>RNDFSimIF_8hh.html</anchorfile>
      <anchor>bb3299a60478c65265f4de944544f327</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Road.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Road_8cc</filename>
    <includes id="Road_8hh" name="Road.hh" local="yes" imported="no">Road.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>void</type>
      <name>_drawOffset</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>594680fe336397087787a70358664c4d</anchor>
      <arglist>(Spline const &amp;s, Color const &amp;c, float offset)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Road.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Road_8d</filename>
  </compound>
  <compound kind="file">
    <name>Road.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Road_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Lane_8hh" name="Lane.hh" local="yes" imported="no">Lane.hh</includes>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <includes id="Environment_8hh" name="Environment.hh" local="yes" imported="no">Environment.hh</includes>
    <includes id="Intersection_8hh" name="Intersection.hh" local="yes" imported="no">Intersection.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Road</class>
  </compound>
  <compound kind="file">
    <name>ScenarioDictionary.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>ScenarioDictionary_8py</filename>
    <namespace>ScenarioDictionary</namespace>
    <class kind="class">ScenarioDictionary::ScenarioDictionary</class>
  </compound>
  <compound kind="file">
    <name>Scenarios.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Scenarios_8py</filename>
    <namespace>Scenarios</namespace>
    <class kind="class">Scenarios::Scenarios</class>
  </compound>
  <compound kind="file">
    <name>Serialization.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Serialization_8cc</filename>
    <includes id="Serialization_8hh" name="Serialization.hh" local="yes" imported="no">Serialization.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>void</type>
      <name>discardWhitespace</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>14f75f080b7412f2293d00bab3520286</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>trim</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c2c73088893cd8a29affe8f567c073b4</anchor>
      <arglist>(std::string const &amp;str, char const *delims)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>matchString</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>54df508bdb3d63fd1e3079bca901b784</anchor>
      <arglist>(std::istream &amp;is, std::string const &amp;str)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>readDelim</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>3c4d671e9d0e4b4c1eb9bb3653e48760</anchor>
      <arglist>(std::istream &amp;is, char delim)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Serialization.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Serialization_8d</filename>
  </compound>
  <compound kind="file">
    <name>Serialization.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Serialization_8hh</filename>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::ParseError</class>
    <class kind="struct">TrafSim::enable_if_c</class>
    <class kind="struct">TrafSim::enable_if_c&lt; false, T &gt;</class>
    <class kind="struct">TrafSim::enable_if</class>
    <class kind="struct">TrafSim::non_string_iterable</class>
    <class kind="struct">TrafSim::non_string_iterable&lt; std::basic_string&lt; T &gt; &gt;</class>
    <member kind="define">
      <type>#define</type>
      <name>DESERIALIZE</name>
      <anchorfile>Serialization_8hh.html</anchorfile>
      <anchor>535057ae58aea6ca6f6f2facbf1a7d0a</anchor>
      <arglist>(stream, field)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>DESERIALIZE_RN</name>
      <anchorfile>Serialization_8hh.html</anchorfile>
      <anchor>91c5b6da9cc6ac4d0095feb9f48d8031</anchor>
      <arglist>(stream, strrep, field)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>matchString</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>54df508bdb3d63fd1e3079bca901b784</anchor>
      <arglist>(std::istream &amp;is, std::string const &amp;str)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>readDelim</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>3c4d671e9d0e4b4c1eb9bb3653e48760</anchor>
      <arglist>(std::istream &amp;is, char delim)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>33590be66761c46491cfc63aec8e73a7</anchor>
      <arglist>(std::ostream &amp;os, std::pair&lt; T1, T2 &gt; value)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c44b2ffb5e4b2104b57fca50c3cba3fa</anchor>
      <arglist>(std::ostream &amp;os, std::pair&lt; T1 *, int &gt; array_desc)</arglist>
    </member>
    <member kind="function">
      <type>enable_if&lt; non_string_iterable&lt; Container &gt;, std::ostream &gt;::type &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>76f266af8d6d9532f568bdf4269afe3b</anchor>
      <arglist>(std::ostream &amp;os, Container const &amp;container)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>049474cf761f78b5fea8bebbe8fe26aa</anchor>
      <arglist>(std::istream &amp;is, std::pair&lt; T1, T2 &gt; &amp;result)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>d3f10b9dfc7d21b917d3174057d83405</anchor>
      <arglist>(std::istream &amp;is, T **result)</arglist>
    </member>
    <member kind="function">
      <type>enable_if&lt; non_string_iterable&lt; Container &gt;, std::istream &gt;::type &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>32f0f07df1061f5d3e5c4fc80cbbc221</anchor>
      <arglist>(std::istream &amp;is, Container &amp;container)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Spline.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Spline_8cc</filename>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>Vector</type>
      <name>_displace</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>93dd84adbd08f7f3bf0c60e52c4d03bc</anchor>
      <arglist>(Vector const &amp;p0, Vector const &amp;p1, float shorten, float displacement)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>perpSegDrop</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fd976ce7a6376cfeb8354bf194096db0</anchor>
      <arglist>(Vector p0, Vector p1, Vector x, Vector &amp;result)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Spline.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Spline_8d</filename>
  </compound>
  <compound kind="file">
    <name>Spline.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Spline_8hh</filename>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="LinearSegmentDesc_8hh" name="LinearSegmentDesc.hh" local="yes" imported="no">LinearSegmentDesc.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Spline</class>
    <member kind="typedef">
      <type>TrafSim::LinearSegmentDesc&lt; Spline &gt;</type>
      <name>JoinDesc</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>6d65d2eecf499fc771cf3d9d43bf4dd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::pair&lt; JoinDesc, JoinDesc &gt;</type>
      <name>SplineInterferenceDesc</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b4e4a153a60813a6e6dc59ec38d3c287</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>SplineCar.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>SplineCar_8cc</filename>
    <includes id="SplineCar_8hh" name="SplineCar.hh" local="yes" imported="no">SplineCar.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>SplineCar.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>SplineCar_8d</filename>
  </compound>
  <compound kind="file">
    <name>SplineCar.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>SplineCar_8hh</filename>
    <includes id="Object_8hh" name="Object.hh" local="yes" imported="no">Object.hh</includes>
    <includes id="Spline_8hh" name="Spline.hh" local="yes" imported="no">Spline.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::SplineCar</class>
  </compound>
  <compound kind="file">
    <name>SplineEditor.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>SplineEditor_8py</filename>
    <namespace>SplineEditor</namespace>
    <class kind="class">SplineEditor::SplineEditor</class>
  </compound>
  <compound kind="file">
    <name>SuperDynamic.py</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>SuperDynamic_8py</filename>
    <namespace>SuperDynamic</namespace>
    <class kind="class">SuperDynamic::SuperDynamic</class>
  </compound>
  <compound kind="file">
    <name>Vector.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Vector_8cc</filename>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>66b5ff80d25ef27b9aac402fbf7990c0</anchor>
      <arglist>(std::ostream &amp;os, Vector const &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>aa45115f1cbf3fab34c87f73d104ff71</anchor>
      <arglist>(std::istream &amp;is, Vector &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Vector.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Vector_8d</filename>
  </compound>
  <compound kind="file">
    <name>Vector.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Vector_8hh</filename>
    <includes id="Serialization_8hh" name="Serialization.hh" local="yes" imported="no">Serialization.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::Vector</class>
  </compound>
  <compound kind="file">
    <name>Viewport.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Viewport_8cc</filename>
    <includes id="Viewport_8hh" name="Viewport.hh" local="yes" imported="no">Viewport.hh</includes>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <namespace>TrafSim</namespace>
  </compound>
  <compound kind="file">
    <name>Viewport.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>Viewport_8d</filename>
  </compound>
  <compound kind="file">
    <name>Viewport.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>Viewport_8hh</filename>
    <includes id="Color_8hh" name="Color.hh" local="yes" imported="no">Color.hh</includes>
    <includes id="Vector_8hh" name="Vector.hh" local="yes" imported="no">Vector.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="struct">TrafSim::ViewportEvent</class>
    <class kind="class">TrafSim::Viewport</class>
    <member kind="enumeration">
      <name>ViewportEventType</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>EMPTY</name>
      <anchor>e56e3e21dac6f23a82119589ffde635dfa19472d230c281b2be86947918bee44</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MOUSEMOTION</name>
      <anchor>e56e3e21dac6f23a82119589ffde635dc588df8d990959af9a884d6e270d7b2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MOUSEBUTTONDOWN</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d4aea8e33c2da8e975c2170fa7b51ba6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MOUSEBUTTONUP</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d3c54336c07c447ce6939d6c1f8da621f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>KEYDOWN</name>
      <anchor>e56e3e21dac6f23a82119589ffde635da4dc9ecb8c724335ba669c2addd3e956</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>KEYUP</name>
      <anchor>e56e3e21dac6f23a82119589ffde635dbe90e6943e0813571a861a9226616b57</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>REPAINT</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d802ab113d387be5b07a81b657b60f829</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>QUIT</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d68b82d03d6a7d5fce989ee448149ec54</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ViewportEventButton</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LEFT</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e96b105c29ad61c3d5f648a435a9c8811</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MIDDLE</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e696ce63bea8cd757736987b44af81c61</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>RIGHT</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e798d1c59d410950cc290311508dbace5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>XIntersection.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>XIntersection_8cc</filename>
    <includes id="XIntersection_8hh" name="XIntersection.hh" local="yes" imported="no">XIntersection.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>void</type>
      <name>sortByKey</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fadb650b4ec09edb17b70dd4b9bce48b</anchor>
      <arglist>(T &amp;values, unsigned int keys[], const unsigned int n)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>indexOf</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>af242294b69c9d63a5687ed6561608df</anchor>
      <arglist>(T value, T array[], const unsigned int n)</arglist>
    </member>
    <member kind="variable">
      <type>const unsigned int</type>
      <name>NUM_ROADS</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ae84f6e9f885dd9ec2f1363c9d75a794</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>XIntersection.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>XIntersection_8d</filename>
  </compound>
  <compound kind="file">
    <name>XIntersection.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>XIntersection_8hh</filename>
    <includes id="Intersection_8hh" name="Intersection.hh" local="yes" imported="no">Intersection.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::XIntersection</class>
  </compound>
  <compound kind="file">
    <name>YIntersection.cc</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>YIntersection_8cc</filename>
    <includes id="YIntersection_8hh" name="YIntersection.hh" local="yes" imported="no">YIntersection.hh</includes>
    <namespace>TrafSim</namespace>
    <member kind="function">
      <type>void</type>
      <name>sortByKey</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fadb650b4ec09edb17b70dd4b9bce48b</anchor>
      <arglist>(T &amp;values, unsigned int keys[], const unsigned int n)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>indexOf</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>af242294b69c9d63a5687ed6561608df</anchor>
      <arglist>(T value, T array[], const unsigned int n)</arglist>
    </member>
    <member kind="variable">
      <type>const unsigned int</type>
      <name>NUM_ROADS</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ae84f6e9f885dd9ec2f1363c9d75a794</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>YIntersection.d</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/i486-gentoo-linux/</path>
    <filename>YIntersection_8d</filename>
  </compound>
  <compound kind="file">
    <name>YIntersection.hh</name>
    <path>/home/users/jengo/sandbox/navigation-jengo07/src/trafsim/</path>
    <filename>YIntersection_8hh</filename>
    <includes id="Intersection_8hh" name="Intersection.hh" local="yes" imported="no">Intersection.hh</includes>
    <namespace>TrafSim</namespace>
    <class kind="class">TrafSim::YIntersection</class>
  </compound>
  <compound kind="namespace">
    <name>CarDriver</name>
    <filename>namespaceCarDriver.html</filename>
    <class kind="class">CarDriver::CarDriver</class>
  </compound>
  <compound kind="class">
    <name>CarDriver::CarDriver</name>
    <filename>classCarDriver_1_1CarDriver.html</filename>
    <base>SuperDynamic::SuperDynamic</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>82024d14a21c7b54a0248948b8f2f691</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onMouseMotion</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>82510069bfdefc0ce6fd51cabe94e966</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onMouseUnclick</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>91c9b7253346c7eb2d526552bfe53cd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onMouseClick</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>c84334c580ea23e9e953e74fd3de541e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>perFrame</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>9b1f8c87cd0f8892e145219d2f1db3e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onKeyDown</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>7c1fec48ddd3b2514cf5a45e3a34066c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onKeyUp</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>9f068b55b2f7d73485a2f95f359181f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>car</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>dd5993dc3c16d12704129484b5a3fd64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>heading</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>fe23e145dc1399aceb494aa2fa8b16c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>accel</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>4daeef7b655925487933eff3a78c1208</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>keymap</name>
      <anchorfile>classCarDriver_1_1CarDriver.html</anchorfile>
      <anchor>0f186c050f0c1c78a4be1fd3e4549203</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>DemExporter</name>
    <filename>namespaceDemExporter.html</filename>
    <member kind="function">
      <type>def</type>
      <name>ExportRoadData</name>
      <anchorfile>namespaceDemExporter.html</anchorfile>
      <anchor>2e519c932fb253d43eddaa0c7d1ab270</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>MergeImageIntoDem</name>
      <anchorfile>namespaceDemExporter.html</anchorfile>
      <anchor>cfae90bf2658cd4c077ba6ece3ef2ccb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>GenericApp</name>
    <filename>namespaceGenericApp.html</filename>
    <class kind="class">GenericApp::GenericApp</class>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>app</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>d35d7d078472c5ad0823e1ba4ed8dcec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadRNDF</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>38e8ff6fb5146221df2da4f00baa8b83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFunction</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>6100ce97c4fda75d73a213c3cbfc0180</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFile</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>a4ee411dd43ff27c542a8cc30a46867f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>subgroup</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>e43a0dbec84f1fb92e26b10708a76933</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>wait</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>373fe64c37d9b53ef0c6aa871d311b9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>float</type>
      <name>sleeptime</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>9167921ce55fb9583078c1cd98dd928a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>rndfname</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>5e432fab28f242274d06342c15d8f804</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadRNDF</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>38e8ff6fb5146221df2da4f00baa8b83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>functionname</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>4de2d9c14c10f7240f8b24e29fd48659</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFunction</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>6100ce97c4fda75d73a213c3cbfc0180</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>filename</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>27ffdba40ae3e033165d177535760dc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>loadFile</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>a4ee411dd43ff27c542a8cc30a46867f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>subgroup</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>f1ea8202b139ebb8d2b0f2243547ec37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>wait</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>373fe64c37d9b53ef0c6aa871d311b9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>sleeptime</name>
      <anchorfile>namespaceGenericApp.html</anchorfile>
      <anchor>80f5161a44c927807fd46135156064c6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>GenericApp::GenericApp</name>
    <filename>classGenericApp_1_1GenericApp.html</filename>
    <base>SuperDynamic::SuperDynamic</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>e2181e17a11fdf995a8c174158c92e0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rotate</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>f31593532ac5fc01439d7ff9cfb72f92</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addRoad</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>76d0563710cb83fffa771cb18cabc1b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildExternalCar</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>03535b869732173da784923ffb5b63db</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildAlice</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>e298b372646fe28b53912ace9a62650d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildSplineCar</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>8e74e4c29a85d6951d697659a74eed45</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>save</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>c671eb7193acfbd34589e6e96ef2db36</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>load</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>ad3f26373c90ab70ee54f9444d1bb693</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>loadRNDF</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>01e35be1fe11170f46f29f64a608af3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildRoads</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>e870e3457a180d020a8fcbc1487b5e08</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildZones</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>b9995000e1fead0246e33863999a3b8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addXIntersection</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>81d712ea664ed3625f3f86c164aca815</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addYIntersection</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>fa77f62eca4e1c98f53e8a78d8090a6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>51678af338834a1fbce71b89db933cda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clearCars</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>d86d3e4b2e7b715a71f1d8e964686b41</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>export</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>29a001bf225be34e572e6eb15ce0a519</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addCar</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>3b4e7e851afb6aebc57ebd0b976ed8bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addBlock</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>f9f3656a7eb4bba796adc93211881710</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addCircle</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>0af2f55a6372f0ac7f7f0d02d801959f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addEvent</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>fa66fd72b88990fa0036a2436fdb5b40</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addLine</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>b2b17e1eb0a9b81c1fb3ac660f11a5fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addSelect</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>262798b56ffb68c0b0a396056f57ecee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addToScenario</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>85a6746ac78ec04c0c5b4ea1f8982146</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>select</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>1967af65132d470746db843be0a449d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>debug</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>5ff5f5bc9987f51f3a7255f239764cc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>makeEvent</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>7fa7f4a7e91143e37455ed46e949f7a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>commitEvent</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>c2113474bbc29e16a9eec30db253c66c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>makeScenario</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>8e316054d5395eab8f41fefcaa1fb17e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>cancelScenario</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>626708ee0068302ee90cf3b49291089d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>commitScenario</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>da15c498d8b1d8d637ef8097cb39e403</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>CommitScenario</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>d7116c4edd73d28983709d71301f5b62</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulateEvents</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>cf7ec689140c2c1495fe1b8dbadb0e36</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>drawEvents</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>eb645f8aa61081b04c10d48ee65359ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exit</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>fea14f56cacf77e567ec5a6b86432d23</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>help</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>3d582d0e6ab63a84ed01098c170ebd4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>run</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>96c5099d8d65b4d455258fe4633a4a1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>call</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>90cda93817acd0ea479707b0ad8821be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>timestep</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>b4889b18fd7008f94ed1bde60a4dceaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>viewport</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>4f5269622205419a7f6ebe9af5cc953e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>environment</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>869f21bb5477d2b0a64229e4f4ade5d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>sel</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>0be33c6723a90227db6c555e06ee3591</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>editor</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>873d8cce51ee6361cd2caa6cdedf87a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>rndfIF</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>025d04b521ff7a247fba491139941f68</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>scenarios</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>5d325dbdc16716e6e80ee5a1a7234af7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>scenarioname</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>cadc48ceab4d6b19ad0a49c4e2c4a12a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dictionary</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>8d737e916324fc59139d61ae2b2aa19a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>ROADS</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>b1cf58a122f0daa84743392c18846d36</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>callQueue</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>84801d7221dff6c9cb355b7dfad2bd76</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>timingFactor</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>02bdeb83e603dca65e7bd284737921e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>aliceBuilt</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>9f09643be451d11cfff6246c340a9e86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>events</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>613c1f4c70e9e25b88ff79adc24174ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>quitMessage</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>521bf32b2d3f4a993618e6a0ed833e63</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>Alice</name>
      <anchorfile>classGenericApp_1_1GenericApp.html</anchorfile>
      <anchor>90c209a34209ccb96cd17d116e5883df</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>libtrafsim_Py</name>
    <filename>namespacelibtrafsim__Py.html</filename>
    <class kind="class">libtrafsim_Py::_object</class>
    <class kind="class">libtrafsim_Py::PySwigIterator</class>
    <class kind="class">libtrafsim_Py::Vector</class>
    <class kind="class">libtrafsim_Py::Color</class>
    <class kind="class">libtrafsim_Py::ViewportEvent</class>
    <class kind="class">libtrafsim_Py::Viewport</class>
    <class kind="class">libtrafsim_Py::Object</class>
    <class kind="class">libtrafsim_Py::Environment</class>
    <class kind="class">libtrafsim_Py::Spline</class>
    <class kind="class">libtrafsim_Py::LaneInterferenceDesc</class>
    <class kind="class">libtrafsim_Py::Lane</class>
    <class kind="class">libtrafsim_Py::MultiLane</class>
    <class kind="class">libtrafsim_Py::ManeuverDestMap</class>
    <class kind="class">libtrafsim_Py::ManeuverLaneMap</class>
    <class kind="class">libtrafsim_Py::Queue</class>
    <class kind="class">libtrafsim_Py::IntersectionGeometryParams</class>
    <class kind="class">libtrafsim_Py::Intersection</class>
    <class kind="class">libtrafsim_Py::DeadEnd</class>
    <class kind="class">libtrafsim_Py::CarProperties</class>
    <class kind="class">libtrafsim_Py::CarInterface</class>
    <class kind="class">libtrafsim_Py::Car</class>
    <class kind="class">libtrafsim_Py::ExternalCar</class>
    <class kind="class">libtrafsim_Py::CarFactory</class>
    <class kind="class">libtrafsim_Py::Road</class>
    <class kind="class">libtrafsim_Py::XIntersection</class>
    <class kind="class">libtrafsim_Py::YIntersection</class>
    <class kind="class">libtrafsim_Py::LaneGrid</class>
    <class kind="class">libtrafsim_Py::SegInfo</class>
    <class kind="class">libtrafsim_Py::IntersectionInfo</class>
    <class kind="class">libtrafsim_Py::RNDFSimIF</class>
    <class kind="class">libtrafsim_Py::Obstacle</class>
    <class kind="class">libtrafsim_Py::Line</class>
    <class kind="class">libtrafsim_Py::Event</class>
    <class kind="class">libtrafsim_Py::SplineCar</class>
    <class kind="class">libtrafsim_Py::ObjectDictPair</class>
    <class kind="class">libtrafsim_Py::ObjectDict</class>
    <class kind="class">libtrafsim_Py::VectorArray</class>
    <class kind="class">libtrafsim_Py::LaneObject</class>
    <class kind="class">libtrafsim_Py::JoinDesc</class>
    <class kind="class">libtrafsim_Py::LaneSegment</class>
    <class kind="class">libtrafsim_Py::JoinDescArray</class>
    <class kind="class">libtrafsim_Py::LaneSegmentArray</class>
    <class kind="class">libtrafsim_Py::CarPath</class>
    <class kind="class">libtrafsim_Py::SplineArray</class>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_setattr_nondynamic</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>18e47dc885313d2d125ba6d77379ee1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_setattr</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5dc7a6d6d2796d55132576f83ff91e99</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_getattr</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6cb8a096e6550a02095d18aec765ed30</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_swig_repr</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5e3c487f02f112d6d060c47c6cb40f0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_dot</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b14eb114d50a034b7fe979934d7b73f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_dist2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>502a01e73eb64bf151a82331e8fe3240</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_intersection</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0dad17a4ac53b06075a028e39a1b5adb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Vector_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b9abc01ba02ea9fd6ae1e7ed1c1fdff5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Color_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d29592fbc81002f9a25c04b68e047c12</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>glSetColor</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0ba61e1d76bc0548e68bf6dcd33db491</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Object_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5db313beaeb9b782e6b98b30bc9cc19e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Environment_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9cd800550ae00dabe69503bd8022ef07</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Spline_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e201d64d6ed247c8a72eb828a95aaacd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Spline_parse</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b97c38e23482b8a7cc17a3247bf06a06</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Lane_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d424c2937fa239638ed4452425714b61</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>MultiLane_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2c70a90d81eb4300af36ba8aba117e94</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Intersection_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2996cf3bdb7d168dad6215e1be0f5275</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Car_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c8521cc13f66b3f6674475311f59dc2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>ExternalCar_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5006166bbf980f9100d6d56c8bad8df1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>CarFactory_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>350d8a66fa06f1e15735feb3bf2baf53</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Road_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aba84ac1364b8fbe10776c00f310dba5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>XIntersection_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>24948acfa98d91e98f1f3121a2d25269</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>YIntersection_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21ea9e86c9a77559794d30ef52d07805</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Obstacle_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f34f85e98de8581ee740bc8f587b51ae</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>Line_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fd3bbbfc83be85d34d3a2d77099ac393</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>SplineCar_downcast</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce8a82edf77a3adfeaeb9aa085cab3c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>__disown_METHOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4cc07fee528cd5d582d7f30e0bff184e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>new_instancemethod</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b13e709bf249c0219193408c1fe30211</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>_swig_property</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3c96424d186d6df137dac1944c04532b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>_object</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b51728f599d6e22c14f7a7ff6aa7b76e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>_newclass</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>36038e2f51ae3ca23d82f0f69b029fa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>_newclass</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>36038e2f51ae3ca23d82f0f69b029fa3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>PySwigIterator_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>de22a49f5a3d9c13a014e3ac6e6a0fcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UNKNOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d49e44a45f749b7a6c73f1445f9fc8ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_FIRST</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b44c83f5cbd29c00759b25efd4db6690</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BACKSPACE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>87705c544f18dea5ffa31665f5359f85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_TAB</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e455d61e17e5d109fdf51acd8b4eef3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_CLEAR</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>865b13f96f3ef6ef2ab7934dbb2870bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RETURN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>abf1e58b31f8442eb316e81732856079</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PAUSE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>882fc9097935c3f1af6b3f57b0372e01</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_ESCAPE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2e83967d1fa3ba2cef48f94a794d7575</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SPACE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>013554a3fa3d78df32ae971dc7497ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_EXCLAIM</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>751a781af9d01baac401ae13d19859ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_QUOTEDBL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1d419692b6cff35cf43e363723fa8ffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_HASH</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e49144544e77b999ae749b2dacb2888f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_DOLLAR</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fc9ffe4c0d304220d24ae348be526f99</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_AMPERSAND</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>47e57078618e6eb1af892b337b3e32d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_QUOTE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>db735923e61dc3c1d9b9c61c2a3595f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LEFTPAREN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f028dd671bb225ede28e3ebca241ffab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RIGHTPAREN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>05150ffe56918171f8d949790c669b66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_ASTERISK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9f8945442e4028abcd8fe588ff33bab7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PLUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>403442b91005b7d277759256ac746e16</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_COMMA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b6961d3f85807b2dc03c7004cb153678</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_MINUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>bd4320e09cf875b75dd00c52c6595c07</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PERIOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>66f650ad5f72f2ae77ef53d3545c7fc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SLASH</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f08b099767edcb9d2bf83e098103aad0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_0</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6900ac8843a64b7b5e3daabadf57ac8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1507e540c2f02791f67f3538df8a4e5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7b639572ebac8a084489091e90c3068a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a51d74e20c774bbb0e62c5dbac8e2e64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>169d1b8d02241bc581b8b159aded46da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f1fd0a44cbdd3f9827b6a82d9c66d324</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>65c0788dc287a0b165101feca6776cdd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0532b1f3e76a3c3c1452222924dfdb62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>09430d8ed34c6fae0717444d44eb1d17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3976e24f1e04d5713b6e8183d8694019</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_COLON</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>17b3873dabdf191def8d5410fd11bb14</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SEMICOLON</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>301d515db169c3f1008bd639c2a6bacb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LESS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce70a1d9d962d7faa632a12833c04326</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_EQUALS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0fbb1a3236b77768b4007d8318ccdf96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_GREATER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>54ad7dd712fe2474e913e0cb92ab77b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_QUESTION</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1ce36638f5fa9e773ee0163d0725da34</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_AT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>412494d45f3666b701a9752d9139e1e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LEFTBRACKET</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>784d48c7b605fa162a702788c030f8c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BACKSLASH</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5e3a4b584e166fdf208b6f32056aebb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RIGHTBRACKET</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f063b0594cc99db935b6f96329d55c2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_CARET</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b9afc735ad8c80587fcbdc466cde0483</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UNDERSCORE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a3e07c70af96e17bdf9401ce48917c04</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BACKQUOTE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed20448baf24ed70b6abe4d160b5607e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_a</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21053cb1e4095e32ebdbb37df17ebab8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_b</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fd88a9b56e845a241938f5fe8e068416</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_c</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0eb65f8918ae0da8570bb676fd2da557</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_d</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7767432a4710f78231d3961a01ba07d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_e</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>50d8d374594d7566333d3a00e519295b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_f</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>78aed06bd3e5741332ee3ff5857cccb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_g</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>96c2b8cd00883a2ff0af12994661a7c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_h</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fbef2bbacfa6d321bb9d9d4f46b758ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_i</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e54eb72a12a7ae54a9676fde9e356143</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_j</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>131bcc876d4d9c542439a0fe22c58df3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_k</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>027edc0fe462ab92b33884a85efc5bb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_l</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aff46b405018a17a3356352e46455241</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_m</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>50e815015375317df9faf609d0d1e03b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_n</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3a8cbece670673af9f6ec7b7f7991b89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_o</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7aea0b27e6cad442846900833e6fa420</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_p</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a4a1fcb8b1328f915bfc7336c1cd03c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_q</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9f3f82b99c86a121d0c1e34db9c3033f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_r</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3f79a3a3dd74b59236fd983096196569</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_s</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c63f4c188da4519c93aa9907d998eeeb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_t</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b57d41bce14a952f124907cf822fb232</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_u</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ec18fbf4a277ca04874cbbea28597efa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_v</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce6694404b71c475852c5790097d2624</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_w</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ac7544fab60dbe7262f37425ce4c1ec8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_x</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>375646443d16a2651bce69a81120156f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_y</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7ddd37a90a042ae9160a5a3718265bae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_z</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>412c87f8ae07ec272e19235211ce683a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_DELETE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>50b40dc1d2f66ada192c3c2ce80e1c4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_0</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8d93fef62bd16e38a571bc147b3ee16b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f5a89f49301b1045fea940a9398ad1ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>782ba12eb14de9cc324cedff5210f13c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0a08945b0e14e26d5ab24882db51d320</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>60a124c1a46e02df7d209f98fdd0fbd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ce889181c898bde7c3d85faa5090fe7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>78529c4aa07c3334c26683f5edd43725</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4589cbab3fd79b60761c694855ea7d25</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>534d82d337e557a95ec6b4a3466fb1d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>13c2031f40fc08f88e6f6db4329de354</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_10</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>26bf6a3c853a0cd7ca5d3564f61288a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_11</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2cdf6edff280e68cbc165f424e03312a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_12</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b1d6c535dc37621011ea65b3a699754</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_13</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a1eb1e16f31fac115984c6ab55afc47f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_14</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1f71015c1a8f4536de5b4f448cf39150</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_15</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>435f110a2a376a4b4c27f3b45950123b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_16</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4718d2dd5fabbb9f0d24b3fc33e91459</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_17</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6b01aac3cb3231f36ec55ca441e970e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_18</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1e5ddb3c1e591bd6893d9e7e2deeeb87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_19</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>52f7061a623a74d0f1b5b56afc591882</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_20</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f13f89f1170dacf53d26b4362e2203d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_21</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>df380bccb5f050611fd31f3f22a1d0c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_22</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>dd2438920c04e7468de6af7dea4ba59f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_23</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c28755f28fc71bd98c73ca1574bd3f0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_24</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>021d7127fef995db0e8866792e3e7dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_25</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b437a2f5d3acd31cf09e2dc88443d7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_26</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c3e0d14975feb63c9bb4311726b2beb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_27</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>49716d8c36fc3f09149000e91224e420</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_28</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d03406dc787a69315c3dd70eda7512a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_29</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ec5d34b19a4105edbaf490ea2186c6dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_30</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>01c27e5c8d8480aa49a9177b379f083d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_31</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3f956c246b5e9cba6c5c50a7fd9cc157</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_32</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>52ccabcb87c0049e7867acb449a52abb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_33</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>05fb0291f85bef9b0ad18ccdeb710f0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_34</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>31f109f6cf176084b0e0aa588446f791</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_35</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5d2c29068c57183d394609f27309a7c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_36</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>331ce2fdd0d988f5413f0d2c62155cfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_37</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21ae0b571fdb095e13effb5f4bf122e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_38</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4585bab4b271894b6d7c6911c0a7d35a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_39</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f4fc2bdc960aa195729fadfab271260d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_40</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2383c7f013d04854eb89219291179ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_41</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>21774a7a25f70d9658981cfc830904ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_42</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>17b3966a747122c6a07f028e6f1fc5a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_43</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2adc863246ab8a1fa54e66f54673c441</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_44</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d7075b33f9aa0008ff24b1b13eae6080</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_45</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c6d739c39fe331c03c6ad8d3f506afb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_46</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>757b627839782aabe4e842c407194780</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_47</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1a1cf67e0edb6f3bfc93fc0a70f22492</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_48</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2d81108259ea77b963cf330311e064f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_49</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a0a5dfa13b98ba34dc9d2272049767f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_50</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7bb6a01dce65b7dfdbaf6e0d39781d61</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_51</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1064b80e4a0be1b4508425052d28b393</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_52</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>089b8bf3634f8dc0b4d8fb99451828f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_53</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>182abd7ccba51235b3db675ad81ccb9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_54</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>831d0ab9be419a29ebc76991f470df37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_55</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>df3a3268ecf6de5c6662e6531d9c3896</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_56</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1920638e8fb8df2ea9f07219a70f902d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_57</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>cd2254d66aabf795f37859ee2097440f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_58</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b9742e6c81ade5088bfae077428d0d92</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_59</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>424ca73ba83e0a270dcdd48b48912b1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_60</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9ab8f830f98ca5ec638a08244f1c89fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_61</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a5b426dd707b1537a1ae8416448357d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_62</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6e84b594bb0cb98987836d8d83a5df96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_63</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>55d8a0e8cd218516f82d60becb4232fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_64</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>05281ef10e1580220940b035600a4882</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_65</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>809751403d9a69f25a0f96c9b649421e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_66</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed177e281d627ec8c2e7195b27f51168</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_67</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>eae612f447cb16a1da15ccfc0cc1ee0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_68</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4b16cd660a551a8931fd4511d58cc283</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_69</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3b87ed8724541be3a471606b306472f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_70</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>33ca28bace843f9dadc30e9313cabf0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_71</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>bf83e1d106e3267488a4abff702bfe12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_72</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>dc042bb4eb9fa8705336b33953026809</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_73</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0ed297a43e354fdd4266a26db4a123a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_74</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>476b931d9569f6f9c853ba03e333982e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_75</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>97634201d24ddb0659dfcf22a8e4dc4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_76</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e523b94af0849a834eec6f2557cb8ce0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_77</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>585e5ddaacaeafe1367844954edb7667</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_78</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1813acfd1c04a665961a15da406a833b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_79</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1254f86d99ee13523202f1bad64edf11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_80</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>99c8ca5ec6066158eb19799de75d0d05</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_81</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5efbf007466505185ab16f7a5b2eac09</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_82</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>372be3f2a79f8f244a1a7cc1977db601</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_83</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e278fcaa75b773586b42d84e90888d4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_84</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b27cf58e0101ef40bbf8b836a3a8befd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_85</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b943cea4bf56388403d595f550fb4d76</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_86</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4e50bc6e47304efff508e50c43176988</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_87</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>386f1c6fc77e5b7fc4e96cdd51f8b26b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_88</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9a1c8a436c2bbc96c95bec293c6fa483</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_89</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>311b1966cabd151a5a16cad698c80242</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_90</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d9d8cd53ce186c0b45b065f022aa810b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_91</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>809a169cd5db8ade5f8619fbe0c488e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_92</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>65e9d7895c7d6a39cee526848e37b951</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_93</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0b333797ebddc876a1ab617c226fc064</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_94</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b8df56eb9e5ca9d380732805e9ea34d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_WORLD_95</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9662ddf36bf35cabe5d2024a38505c51</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP0</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2d159acbc549f90d83d572b8f17de83a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>41cefd3bdcfd81c94f9ab66e3686e797</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a2d087d9bcb9680be60a138ce103b888</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f1524c1a78ab30f7e8f98ee42b1759fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>57b6c36bc759b35fc3de969fa5adc7da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a35db62a08c0edfff5fec320685f3e13</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aab798aac4848fcf906556527c69a26c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3b5ef27f362a272b32624cd39371e42a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5e187824cc48de15b9f770c096e2e310</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_KP9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>486b45b1f5cd9fe4c11a86252477bc4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_PERIOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed5f640ace8b1dca3819b6648a3fa203</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_DIVIDE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>262d0a36746bbd67233563aed6d37654</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_MULTIPLY</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ad56832525aa58967fb9c1da0330a477</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_MINUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>57739ad84d2f944a00f2e183a755b985</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_PLUS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1e9b26e787f6b771613693ca0544c046</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_ENTER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>121de346765d0b697cabad60db5010a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_KP_EQUALS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4e7a868c55eccbc1cf08263253f96122</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>3d27f3345e2fcaf3efe25d1f03df4ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_DOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>84d86f6808996db976f4f195ee580a0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>db9143da48b9c7ff8f36bae0c0033185</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LEFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>da538923e21064ebfaff1ef61617c753</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_INSERT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>dad8eab3462e47b6347d009d25af35c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_HOME</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fcc9b29d12a0d7104b3b95fda03944a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_END</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>cb44b39843dba5893068d6b68b6c2c17</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PAGEUP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0629098a34b400b9401b325fddf718ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PAGEDOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ef9b1a436227d75e37754e13bf561fce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F1</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d82a68b6dfece7b340a2f23cf66ac5c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F2</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6c46d544a2cec286e20e375fdd5b5e7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F3</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9056d762ea604fc00f682ec9872f75ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F4</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6b5a22c6972a7576a8c04d523a85f6ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F5</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>86c4b81302c7b9ecd95d00df5fb29f65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F6</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>77f66c53cd09ded43df0af970b1a2531</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F7</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0577ce300a04e3c265b5a9c0db20939e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F8</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9892a5df2163a3da97ec3b300997420c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F9</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6d20661db2305c3281994532c27be64f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F10</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>a9da135f0a2d6e56c0e0259363ceedfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F11</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8165c7012f527fd1765e4b5fff930e27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F12</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2de3f9379156a98d62d86ec3bbcf1d70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F13</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>deb9a6b87657c8fa21551d232325d1fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F14</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>df5e178a3c7dafbfe244a910d6101a98</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>int</type>
      <name>SDLK_F15</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>931bc268a0c0a49db5453558d87c63e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_NUMLOCK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7b2ca96c8e8b8c0ef79bf0a7a7bdd809</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_CAPSLOCK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>27044b706e07d29e78bdf5e0a85cc70b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SCROLLOCK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2183eec2472b632df8e62343595af8af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e52a3e5f49c2ef4e39ca84cbd7d4d815</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>16c1c4b08223a4af012d3535eeb308c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0a096724caea81d9b66c3c1d47453168</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>349a5aa51ae459a125f6b2f3b810ee8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>874daf4a765d416d8fcc2a50fedb1a12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fd9434abf8ec0f68519ed144823fbc3b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b005ebf1f00612e86751bad1489f0abb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b4ed29c9dc052cf091e67f5eba7fca78</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LSUPER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6ea9a5d9d544cd87e03a8a991e2a9bd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_RSUPER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>73fd2c2c3b21318157c8af8a0f11ee57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_MODE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>38647b20723059f60e5906ba3388d49e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_COMPOSE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7065dbdc83e345db98735330ba3d90f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_HELP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9201c2cd2a74b6bd342eb1cd036e9435</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_PRINT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d52f10e8216b08cc81e05795287dc734</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_SYSREQ</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f0be4c9c4acb6bf229903b89caba611a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_BREAK</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8b67d7be286e3b295d5af47238027b96</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_MENU</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9d9fbc19a5bff566e8a82c837d9e34a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_POWER</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>0a68789357a1fd352865d707d01418b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_EURO</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>511916a719cef9fa2f92d7b5d5a408ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_UNDO</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>b1fb1d5b8edcf82de9da04554b86c330</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SDLK_LAST</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ed1136fae2188ae8ee417d41c80b5c02</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_NONE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7bc6e0cc831d35983e74950b00e3ccfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6a152ed7851f1555fbb5401253eef58a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RSHIFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>520f6e7f01b96ceb66123dfb92f228c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1a732ff9d2a52d8ce8545ff38fa27056</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RCTRL</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c1e38d5ff4b134021f041fb3265f7c30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8b14e6dcbc2920bd7dd555ddce7e077d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RALT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f3933ebec822161e5bd0c4b0526b163f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_LMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>54ecb480005aa8b24259e39e6616cc9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RMETA</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>56a5aef70b45d0799cd9ea4537e2fab8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_NUM</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>69a9fa48afcbee2697db10f0d024de2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_CAPS</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8ca6f439b7718c4097265e5b894c6959</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_MODE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2b0997db8885d01743105643d9351829</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KMOD_RESERVED</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9263d358b5a91ce7659b77d629610b0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Vector_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>10d4b22828d2aee2f249081d505a3371</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Color_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>47ebab2a1099e68dcd6f9787755b3541</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>EMPTY</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7e8ca0bad8209a8060beb9dbcd39d07a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MOUSEMOTION</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>41cbdb02e36d347499736292662445e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MOUSEBUTTONDOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>620afe38ba070507a6ee674e6b81991e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MOUSEBUTTONUP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>aa47b0e1da7db633365e0e68d3724ff6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KEYDOWN</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c2a6e3dc4bc26bc9c4779ce8b7c087b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>KEYUP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e3f50bfdaf49ecab277b2cac7da5ea49</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>REPAINT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>54c1896b4db8389ef21f51e72dda363d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>QUIT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>457dce6ba5f3eee71e5801da1f8357a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LEFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6565d3237efa1e47f00c39d55d342f00</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MIDDLE</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ef7719ae4ac49c246dcd0e1215d69c39</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>RIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2b0e8c025b3edd3ab325c9f0655e45ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ViewportEvent_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9b9c289a7bd2c49c6dc0364ab6a94e8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Viewport_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5c165d149f28de0427866e8b709b4d55</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Object_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>33344f3bf97a46b02997a0ddf40dbe58</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Environment_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>98e865ecc6f9da6af5ac58708cc6e851</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Spline_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>01efb9b02eb9f85f2a06bba8f695067b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneInterferenceDesc_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1ebfeb0911e9b9dc621bdac2a90ff2de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Lane_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>401d4e379099895987db552cef942d67</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>MultiLane_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ab912def6f4a93ddd735488897f0a49c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ROW_GO</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>8f2a7a26179490e1cc971161edf95143</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ROW_GAP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>cd5b7fa5750f122545117fc54d39a5b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CROSS_LEFT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>553a62ba8065540d6cb601ff71caf349</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CROSS_RIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>42e8c2a78cdfdbeadc09fb884ff45eb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CROSS_STRAIGHT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9d17401e31294a439d62d6091004e7b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ManeuverDestMap_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c09d9b2076aca6819a8ad67f8d1904af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ManeuverLaneMap_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>e490c067d0dacfefbd720c834a8bec87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Queue_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d936d75d37203d6279f20a5d2c2ba297</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>IntersectionGeometryParams_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4083eaf1e3f28c2841ad700151ba0a9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Intersection_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>738e660d2179ee819dbeb809c174d132</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>DeadEnd_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>03ecaa6df39f791dca8f6b49d483bf47</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarProperties_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>bc4769afe3e23bccaee923b1c374c44d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarInterface_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f956904e2a8fc9b17c6cb33ba516756a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Car_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1602629ae42353b306f2acc7c213cb53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ExternalCar_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9269788aadf386bdbe4f74145c4b285d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CF_FAIL_WAIT</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f86092e62d8f24bbaeb289e4723a0cbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CF_FAIL_SKIP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>9a7aec302ae0919616df4ff7523695cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CF_FAIL_OVERLAP</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2a272ed2ba1a2e4e53f1d40b9d031808</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarFactory_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5ff1c1237e8898abb96700abd6374104</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Road_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4218d6a056896f6c3e14860e1e1aab8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>XIntersection_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d7a90dd432b622d98fa45be01c93fb1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>YIntersection_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fb5661999609ce6669d78c54f2e92116</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneGrid_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f0e12c2dd9be46f075bf80f0bd1549f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>APPEND_START</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>6564d07117e9da8fd65f656ba6d8755a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>APPEND_END</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>1f021849b933b3df1b5684b2d11687c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>APPEND_DIST</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d4fc29143c53a392d865f658d08c9929</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SegInfo_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>acb9b476b4d20dc6cd109c36a6038119</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>IntersectionInfo_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>61ec4d0c421b97080f49620cf24c3d6e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>RNDFSimIF_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>415ff325340fa6057d08f8dcf09f4154</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Obstacle_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4d895baaa55a2876a1bd4a7e672c496b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Line_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>5f8bc25ec9842a245321537be71cd50e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>Event_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>10ac9c5631ef6462d754a43252ed2f98</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SplineCar_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>d6ea8b3108046118effcd68d31af7442</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ObjectDictPair_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>4e3720fcaacd764345dc29336e676029</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ObjectDict_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ded12c530c3e1678607bbd3fc36204a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>VectorArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>7b93ffa673e5f19f88ac8cc8958e94a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneObject_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f41b067e15ffe3014ebab27f7cf5d2d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>JoinDesc_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>c2b1ea6cbc29dfd8d134dd70360523bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneSegment_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ae7cf5716fda59aea987962f3ee9de93</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>JoinDescArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>2bacc4952cd391fbf42b4578de2e42f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>LaneSegmentArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>f068daecfed87dcd3b42d0f781772c97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>CarPath_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>ebd5a70b7568bc5e313edb0f6c150df3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>SplineArray_swigregister</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>fe7a70d56aab6b9c80ffad53f2e6ef99</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>__naked_CLASS_METHOD</name>
      <anchorfile>namespacelibtrafsim__Py.html</anchorfile>
      <anchor>daac502a4b34306d98af1e4285504b23</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::_object</name>
    <filename>classlibtrafsim__Py_1_1__object.html</filename>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::PySwigIterator</name>
    <filename>classlibtrafsim__Py_1_1PySwigIterator.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>4fcbdc9f70d72a49174f243c250ad824</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>value</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>47fce10c7958c063540d9e2894b6da94</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>incr</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>28b7d0bb9ddaa19570499117b5bf5ef0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>decr</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>b93a828af81c3c39293c50aa6758ac2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>distance</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>d2efdc8e5fccccc2e0207d30d380a2ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>equal</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>61c61858e24d84e8a234abe875e3ca27</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>copy</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>9ee030295d8b5948ddf82c95caea1d7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>next</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>0bf2643fbcaea2fd352dabd5ed2d5715</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>previous</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>668db7f14fa4a0f6adcf7742ebcae521</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>advance</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>40b72a49ce7e62187b676f3a270304a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__eq__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>6a71e50d753622c4dcc1c0634282893a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__ne__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>386dc0b0149ff7028d1420c96d08c2c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iadd__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>f7457c4dbdd1e474788ac6436b543986</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__isub__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>ab6ff87d2e43086525deb37ea2261847</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__add__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>974991869030b1828355ff75aa641d56</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__sub__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>9d175d22cb4d2e37059c0c713065c87b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>b7017d10fac5d97d83af0a1e4db86756</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>4e77a96a4a44db7e7d5867c5e480b3f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>c860abaaa0c19347f6d3d1384b9c7243</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>cea17dc86957fdc04bd4e07cc04f7412</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>d2f23f749abcca5e8c396386e6f38db3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>1ba1122b1664da2c7c25c943bf377dd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>cce20f51de87277815e39e854ab73045</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1PySwigIterator.html</anchorfile>
      <anchor>9a344e92496ca92d13fbf02eaf975826</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Vector</name>
    <filename>classlibtrafsim__Py_1_1Vector.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>1b633f4d1a7ab45aebc6045575d201b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__add__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>320b89b97b8cc999201a5e02d78b284e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__sub__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>3d6fedf53a284bd0c9e5b48473d75b2e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__mul__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>d27edc60081b3cbba75342a20e546a80</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__div__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>f611425ad956b67c9a360720b3d0300d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>dot</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>8702a149861a191b5a5ee737ba3b2c25</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>dist2</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>bf8adf76a62a82a762868089246edab2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__neg__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>bff2bd83bd4767aa29f094a2ab2f6318</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iadd__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>fed2bb53cd3ace0ca09b38e8e4fecbdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__isub__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>7dd6724a4edf702249691c6be82323b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__imul__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>d1bec0e3505be64ae2ff4dd7fb7d2179</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__idiv__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>cd6f0e7c5f2e60696d37acbb1c7a92ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>length</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>d6c629d8d54bb834ff9a030e3eaf6117</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>normalized</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>ba8712e87b53623b9a0a16d8f8cf42d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>perpendicular</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>aab5cec2ff74589bf7cabe5a157e35b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>normalize</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>63be509d5c3794993d7860698a72144c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>f8763ba8cd6213a66485a6c7f2aa386a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>intersection</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>28913a6db43811c50e6a1cd0f4400a49</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>convertToPoint2</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>f79f5670f6079a3257c9e847f62c2cea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__str__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>411d5c14804060ccebb8516b5d8d968c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parse</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>c7394bf760f4f7bdf81fdf7357a22dff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>d44946f01d088274db44aa66cb9d161a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>4031349124e5ab0b6ff68301e598047a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>d728cb2e5dc435eafe6498bb778acc01</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>fb359dbec43c2a5f20f6250b5c177771</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>9f4684648e2bc98b5872d89f3634fb62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Vector.html</anchorfile>
      <anchor>6481fe11584c17b697b051a6c2f6c3bd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Color</name>
    <filename>classlibtrafsim__Py_1_1Color.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>84378eb58258a852a7edecf9d80d97ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__add__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>0673e4b738b16a2ee6edf558496015c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__sub__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>d35fae23eab7d2180f88f87ff3960702</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__mul__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>6883c9727ebdc7d8705a79286bbdd0f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__div__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>9a73e5e0bfa0c46d760993072f3f1ac5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iadd__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>3bab2be4b344c2d8ac9b99b98f51aba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__isub__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>de0d49295cd9b1fc449e13724dd8f2ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__imul__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>7b63822cb060976b99328ab25c7feadf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__idiv__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>78b47088e327d0149388a08bc5c9687d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__str__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>950ba9754b1f2e7d2f9512fdce5bf073</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parse</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>5c0590002fdef9043b18f18ae8c5e76e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>786fbab8b98d6f1b7ad04600c2e3900a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>d56e49b490e5705930369486554a789f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>f9b04318b71427dd7f0479becf00f96e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>e666b1e0d60dd0bc56daffe833febe30</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>8d9772941975e701594df42dd5a2012b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Color.html</anchorfile>
      <anchor>b794192972f41db53a87753246b69ac2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::ViewportEvent</name>
    <filename>classlibtrafsim__Py_1_1ViewportEvent.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>4991fb5b8b59cdc6cd6926fc07c5c9cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>d428862c7c663a186ac7c3daabcab71a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>5d49649063a626923486030f7144eb9c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>04b4184dae29f3a9d472723a115d3cdc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>1eddc796f5846fce04421cb5a1757329</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>30a71341b64bad37f4dd9407b9c44710</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1ViewportEvent.html</anchorfile>
      <anchor>4cf1c1529425a1b1978371312f9ee239</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Viewport</name>
    <filename>classlibtrafsim__Py_1_1Viewport.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>d8b436f3003c85cf867f92f974f98411</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>open</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>2023797b72f9ea3826f21bdf6e5b5d04</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>close</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>a6b92dc87885918f2383d1ef0e1e5278</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getScreenWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>801972efcf3d630c057edb4da53263df</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getScreenHeight</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>32f57523039aedd9b3c180f834068970</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPhysicalSize</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>16811957dea74f33b47b4d73753a65f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPhysicalPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>019d3c2a06d916d238618f7217aaefdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isPanZoomEnabled</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>6a81d3bfff4f7226fd316794dc1b8f7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>resize</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>3d42a0e54d68b617056da64f2f0f888e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>enablePanZoom</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>89f32b86f6098026573189d4d40bdcfb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>disablePanZoom</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>d4a90cd1b3f432ee08e74651d2f0777c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPhysicalSize</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>7e88bc3e90735c7b9504f87052c5fc27</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPhysicalPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>1d83235fbfbe40778470bfe45df8c1dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setBackgroundColor</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>93e764d33452f5fa8f589f245e7a2a5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pollEvents</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>5f73916de2b50cb42245921ba3c07549</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>repaint</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>aa58bb2e05cb78d895ad0830520574de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>097a798f279c219c4865e55d4d2f0c83</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>bd1e74fcba2e3e2f56b6929c1ec0ea16</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>72bdea8c36cad57da56f9bc86952ab76</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>b82f59bb56256f8ac3d178c4089c4acf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>d92783f4dec00c9429d8e2c4ae11afd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>a681465951cad8a023eff48e77b8c03b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Viewport.html</anchorfile>
      <anchor>5bfb5dbb8477b1bd6286e00501308327</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Object</name>
    <filename>classlibtrafsim__Py_1_1Object.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>d56093bc0d218915e33a489ef743160f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>removeSelf</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>1dae7bbaec197dd15a592d9ed6772f2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getName</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>7546ecbeab992448a293ef2280066186</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>39d10cebb9fd4540388c30fb7042c42a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getParent</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>1cfdafc54eaddad4b406e62ffd2ae5f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setParent</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>8993cbc1c913e2bef824b9d306ed057d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__eq__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>6701bc773a3950ff3c832d0cd494267c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>641a30f2a1c3955a24b79780fa9dd7d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>3b52dcb340107a6ab43e7cbf20d8c8f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>ddcb5e0ab09b6c86e820ca91a2657000</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>d150f17225d0406130ac9c36a566908d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>ed0f212a02bf5d61a9ccf7034babe545</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>6e7ad6e445a6bc12008e6e3887738928</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>24193e61d3c27a8c3bc6be7320069769</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>8f75949b13d801203ea082abca6b7336</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>invertColor</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>c931b5edf6fdad5846a9cc18c9fdcff7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>4daa2bedd5a24741c3809b32cd757a26</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCurrentRoad</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>3b1bcc4895b13206384795133fc361b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__str__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>27388df49c40d508adbbf5a6b38094fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parse</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>2707f7570e95c6ae2044849486dcd3ce</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>a89112ef58cd19c549e25eb0effeeb2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>9ebf31111f0293ef68589cbf7c05f1e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>18b697179d6e1942bcab3c31e416d0b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>985a40c7fdb482ea12266f57f4cdd0f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>ab0a2d891b4b6ea7d057a12185ab173b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>93c4e82d2c60dc62844ca72eb2ab919c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Object.html</anchorfile>
      <anchor>de1549b4cad69da815c35cd1570808db</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Environment</name>
    <filename>classlibtrafsim__Py_1_1Environment.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>35a4eb504997e09a1ce0f8cdbc258669</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>d86ffd3b71aebec43aa6bc3baca56b80</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>storeIntersectionBoundaries</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>c9a4f5d269e2eaa6ecbd051eb59d06d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>removeObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>bd1b8d8dff3a8d2a9db49593b9fd8388</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>renameObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>c946a27c516bc56b71f1f86fcce47fe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>61fca0e7662f77d5ce1ed9ed2929ecc8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObjectName</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>1d5de626e7d20a79e555add209ee77d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObjectsByClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>4eec2fd8a2fb10955c75faa04b91ecbc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clearObjectsByClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>a7b3bdb790a464b06c747e75b82b089e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pauseAllCars</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>6d13f76700f21ae42e702507de62cd51</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clearObjects</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>e0cf747741e24c164c7063e7c25666fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBottomLeftBound</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>ba0146930acda9a12c0bf9baeb4467f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getTopRightBound</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>099df06bacbe554f17a43f0cf82bb481</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setBounds</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>8e7fcbc6876b5f0a4584568fb1dcc799</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>a8fff4a4be0193afa14a9844bde3b1be</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>109d86be5f2b7930461b1c93696b0dd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>toLocal</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>1b4e8227c8fea3eee47fe7bfc960ea5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>toGlobal</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>19da666b4484905fcaa305d3b48fbbd0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>updateAliceState</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>a8195d36a34695825ff4819fd93f1107</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exportEnv</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>1a6bbfa3527aeb3ffc71c578f09aa2ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exportCars</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>3ade448094e0b94ac7ff89e289e9c3ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exportObstacles</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>a8c810ebf623b537fc38173c1e7420e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exportRoadLines</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>49d75a94fa349fcbecd2687a39549e8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>testClass</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>d3202a741ad2ce1d82325be69eff8b33</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setTranslation</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>aed8fa512c13c80fe86d5b6d2bc8ff42</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>openNewLog</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>678c2870e753fdf9ee03051422477b9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>writeToLog</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>da8adee6606fe72abc24064e8b5b7820</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>closeLogs</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>fcdcee468119581797a0b53e52c6d395</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__str__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>618de53fe78879a339f05963ece607c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parse</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>68373359acf747e205bef7f9bb1435de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>50fb1b8fd32242eb59497249520a3c85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>87946a81fd4f8d2d7d137f2db67fd67f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>294c27ada5ae796db52b4b6be2f32f2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>8f938ab38f406d25fb899d9e6a618dbe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>7f066b7b6c368794ec6d80ca7b0078bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>ad2288bcfe68c82bc7f0fb51a91b26fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Environment.html</anchorfile>
      <anchor>0f15559e355fc905df790596975b43b9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Spline</name>
    <filename>classlibtrafsim__Py_1_1Spline.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>name</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>2fec38d6cd5dcec26fc516d280ed4ca9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>42b13934431f9894613c57b1e0e8558c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isStart</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>f81349dd1c8537c4c8d6758c789b2342</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>c338491e189a430da93bdf45efde92ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initialize</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>a6303c665e7e9bfab2de59175f4f5b6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>cacheData</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>ada70156fcbaf0828391e8df2adb4c58</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isCached</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>8382f875b6470318373e85c2b8a0354e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>evalCoordinate</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>2650875ffe1d1df6e847482708f222f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>evalNormal</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>84ab6bf4969ebc536995d678f723a60d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>evalCurvature</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>1716cf356a4063b0b5dc40d771c623e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>evalLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>91c0e7930feff80164bad196e6941f37</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>evalDisplacement</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>1ac5876a729ba503301a60ba4e026702</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>closestToPoint</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>35b17759d9dc6c9780c248611909a626</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getInterference</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>a122b7f35e47a681136ee1834729dde6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getResolution</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>bf8810ca6e3f27af3474064c77a46352</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCoordinateData</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>7f52f46c78b95a464583f60c91e12da7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getNormalData</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>d526a4f448384f0f511b098ad61a862a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLengthData</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>b4264a16d22184458b632c4dddd29dab</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCurvatureData</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>51a5c83a1a6c42be15ae8a2e66070aae</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>b929e55308734c3a774c475b3286a9fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>b35ddbb90059796080576ab7e6890304</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>dc7085c87d6b579f537c3eb35f85a3c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>1f08e501b31870ebdb955cfb8d13d5da</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>a5ea9de667cab85ba2f5a9c9692fadf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>2655a32072f5f80129a86a08d007e166</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>f88ca0b3de98ef4b2deaa2ce8d103367</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>c217b070ddb6167f4571acb1d1f76f32</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>ef800e3af53a91f62025b20ca3862d74</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>5bba1d1e4878c6c8e2c6524cbe6702d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__str__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>afd683eb5ea20e8e235405bd5ac002a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parse</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>edd90ef41e19bbb2cd9fe7b7ffdbcde6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_mapTCoord</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>a3598c359cb08210ea3003273436453d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>a519661a03a999980542eb7264b21c88</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>0b25490e6dd4289b71c71fbc7e9e92e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>f577d6f6af867c0221d0dc9e2fbee529</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>193eff7d4573e43224a75edb92be50f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>e91b054ff17621d39c57120a84ad376c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>b74a67d2d303cabcbddb3889fc449252</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Spline.html</anchorfile>
      <anchor>44eb490189d811a243747aa82794dec8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::LaneInterferenceDesc</name>
    <filename>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>2fde251f16817d68a32a86669adbb1e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>197ad0c09613e7137f93e89926968d35</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>0b7beaef580911e03602e6c2f55f1bbe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>9d3af5626ed78949a1f50c76a3074522</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>53f0964f85b97bf32727d6f1bbb2431b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>3fb688b2e423f1ac109268d964787161</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>81e03e97bcaaac18c1bef863e30b166b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Lane</name>
    <filename>classlibtrafsim__Py_1_1Lane.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>1e879a5d292f45ad72a8b12aa72df422</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initialize</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>e47df1a575d0f5099db732d17b2cf9e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>0ec902c0305723c604955a2c1d2c3843</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>b79a3de593448a69c4c6f183313fd2f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>1e1b8301b7a71ae9579bee53a3ef3cc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>860adaf9efe4c7c3b3a5c8d9380f0ab0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>b71276453a00677c756b62a49130c4e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>removeObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>de59649b1b57d1ba96250cc5a8cf9022</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setT</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>b66a2b4b74fb0b515fac77ddb0c3fc6e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getT</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>b1d2f2058cd853ca20bf0a79db3463b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>mapT</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>bf13925a73fdbd3ade722bc72859e495</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>mapTangent</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>67c5d265f78ada9928886413ba48fa4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addInterference</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>e34bd39d2908c438cc6ae640d23bc537</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getInterferenceAfter</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>eb1bb223f1eb9b01a59b6ea3ff9878b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>interferesWith</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>408668d080d90d547ce19010cd6fa04e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>contains</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>ff6d10027c955afa8bc80b4c3f6a35b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObjectBefore</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>ccb240bb559692a2a949462230939083</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObjectAfter</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>cdac3409c53f71c9a1cb6b543e812f75</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>125edb0753f6394f0f25a7de432758be</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>69f86e2820fd9feaae17ab2ad11ca6b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>cfd76139e46c058134ac4aa4a99108b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>b3bd1205ea58cff59c11f2e3615d879f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>095ce5a8818a1f5d13ff23d58c365edd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>ec32083f20fc51e5266f509546cec205</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>7ccb9d97c13866b4c3c33f05b262aa59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>f697d5035602f81126f0a66834e71192</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>1be5d3bc12b8999245e1e1bb3f5775df</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>4b559cee5f90bb4ec4bc019583e37285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>3a05d4aed25399ef1853ffe0a7c04095</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>3ceccd78bf891e7b28404eb0e633caa7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>de49770b26efc5f90df58a70a69fefaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Lane.html</anchorfile>
      <anchor>9dd26c75d728ea5062444a5590a83809</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::MultiLane</name>
    <filename>classlibtrafsim__Py_1_1MultiLane.html</filename>
    <base>libtrafsim_Py::Lane</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>bb14184d307d02bc7ced0791641fc496</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>4133a71bea90e870f7d8235a1c7d8623</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initialize</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>bbdd342bac80a19ce826b1b40b8b7f58</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>1f833a2ff9e8e9191d65c2a2abc933e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>ce3f1638e7dc25aac6e5a3934ebe6273</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>aa0ba5919d4cddc65d07b456c9d0422d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addObject</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>1f6dca486e49c139004baced3129c9df</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>removeObject</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>c09b79cfbb3457f19dccf792fd189231</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setT</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>a7c8d0918e9b663e3575292b68b4b6e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getT</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>e0f4517149572af111fe8714b3fd4a9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>mapT</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>7fb194b74a43c2a8844d97c7658b94c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addInterference</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>0457b19fb5f62ce9ea2355bb16a1965e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getInterferenceAfter</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>4b859991254c69e0ca0ee50137f931ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>interferesWith</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>ad0f02d67749245187a839b945b7da70</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>contains</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>1c35daec2b0d14ea392ca56d3e136b15</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObjectBefore</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>73b64c97864537bbe3342f033fd86218</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getObjectAfter</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>c33e9c974c76c4b7013f7158e38349dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>ef63b14d01f8fd86d000e2d5f0140549</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>2bf4343291e787e7de8cfb5166686af2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>55ffb43c5682d17912e5f7f90fd75edb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>9f8b6db94bdd2dad713dd775144c33db</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>fc4205dbf29170de963f79b07f40ac9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>ebe0f56cc5ce1611b19e9b91a15f04a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1MultiLane.html</anchorfile>
      <anchor>e719758cecef3ebd59d608414e933fed</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::ManeuverDestMap</name>
    <filename>classlibtrafsim__Py_1_1ManeuverDestMap.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>5f392d84c4339049ba3a07729d6d210f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>ab6a16c851e7d5d5a303650b47f0d387</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>30584dc3cf25e8e7629adb927d23ba0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>08f45b79cf907485018309b5b3020415</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>51ea2e31a0223c90a2d2e58444b555ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>46c28a47760279fc611e5bb803d540f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverDestMap.html</anchorfile>
      <anchor>d755a3910e2c6184854186e4f13b3141</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::ManeuverLaneMap</name>
    <filename>classlibtrafsim__Py_1_1ManeuverLaneMap.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>a3cde1c827101019654c5350432b0d73</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>c5c9bc072e17f4c76d513312a0910a84</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>3904360ba47cb59d971f3b80e5a4167d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>faead641a6bd3d4471c2860ed98381b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>256a09b8c05b28db84ae4055620ed0d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>0f9380288f5b1560ba268f35ae564def</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>f7448ee02690ec58c5326049f7146f70</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Queue</name>
    <filename>classlibtrafsim__Py_1_1Queue.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>7691cc11ebefedefb399cbffcd75ebbd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>150ad2f737b3c9ef829047f7aea36a7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>a23463df289d01db2317d57d533d07b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>88e25f90befd50487976b74c8ff2a732</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>fcdf89b94ba8defdfe4e2e2879d589c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>92ed4fc40f1476bab2a430af32dc116e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Queue.html</anchorfile>
      <anchor>35ce92ced73a04e508cdd1675f4e49df</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::IntersectionGeometryParams</name>
    <filename>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>275a1b529836b476d89c764c0f53538d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>39d153ff66f49b5a57655ec71f236441</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>f67a989e45e2f1cc02594b5071519916</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>0e7d3f8d31d4c1cd1fb30a37cd73de57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>34306acc2e14c8b397ab727d05a0a688</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>e2a52de10551ebd8c074e119adfcff04</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>6074983d550aa7f22234f3a76829e153</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Intersection</name>
    <filename>classlibtrafsim__Py_1_1Intersection.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>d97d5abd25251ef74722c688887881c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>038ed50a1491606dd44efb8d5446c813</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>numRoads</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>67995fa26787848fd5f7610f311d1cd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addSpline</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>f1eaecb77c2d5ec985f0da0967120df5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>b9a0fb54f69ed6209139ea1eb8421b65</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>hookStart</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>393a9dcae8aff76b6c44586719866b56</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>hookEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>6d9534563ae87039ff97265539734834</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>blockManeuver</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>342ce390136dd727ba1bb213eb03b20f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>unblockManeuver</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>5b5e7a805c2a11dddabe8fec5d346af6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>computeGeometry</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>827c9937536c27f85e8eeda85462eace</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDestination</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>52fe0f55cf0b527fa302abee5c5c440f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLane</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>6a63a4154cb3173e5279c3f1179b9f7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setStop</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>5a6165f0af5ebcf39fb70bea4c1a21bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isStop</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>58db7ed216a34d2437f0a876971d8177</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rightOfWay</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>6dc75ff3af2a9d61dc35792aa3ec00b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>enterQueue</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>90fbb3fa8a20282a16a7eb3c4cdfb781</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exitIntersection</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>640213346ba9f8ea3207140d7506b62c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOpposingTraffic</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>88055ec7b922ddcfb4d09dc2701f7b47</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundaries</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>42bbb53f0cf41aae2319c06f819fe73c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getParams</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>d79ca38782cf50cd53ae520b33d0fae9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setParams</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>52dc6b3752421e77ddcf060a4b7ed8c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>b8cd3577989f94b6e68f3eb51a935858</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>a589be392b0e10de7c17cb7869383ccc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>68d907dafe6f774f4ef7da171c1e410b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>0a95881e80c2e1e6779a95d293371b40</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>2c3bd7eee3b828e49e28dd163d86fe14</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>bd5ba61ceba4d4a83efc8c7f356860e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>047e8c806a52b3fd4a4e74be2092fcae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Intersection.html</anchorfile>
      <anchor>a93e39b3168e2579341e61d8a3a29529</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::DeadEnd</name>
    <filename>classlibtrafsim__Py_1_1DeadEnd.html</filename>
    <base>libtrafsim_Py::Intersection</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>419698721bdbc5f3176a88f43904cd88</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>f64c2a335c282365796b0a16c239d554</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>f521ef1bb79742245f4e71b6a9647a21</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>f467632326e166d4371bab44d1496389</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>b811a70b4e342d678c3a068c959351b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>82202197f4d613cc036fd799f669dcb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>e65b48a764ce91afaf59a9f70c3e2b75</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>7e464a56075041dabe0adaad2c8522e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>a4f3ec49cf42490c1d959b63af82156d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>b95835611470c5b9ac22c114c94758a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>881c82a794f610fa567b3c23b908bf1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>a6115b00a7bb3a19d3652b1ccb01b3af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>f8201d8f04e8321b1385d02c82e0bfca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>ef878b809cc746111be74bb292644157</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>60d62ff7dc719a80d9690ea91ae79446</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1DeadEnd.html</anchorfile>
      <anchor>26c3b3a614d11eea9bc877f4b804196b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::CarProperties</name>
    <filename>classlibtrafsim__Py_1_1CarProperties.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>d2624ddc0e09575893979a435846fd86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>3520d7234aae07036b006d4562a56be5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>367266d3d4e3b48abbe2dceda3e27f1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>b41dab53c2a207204988967f7bdd0468</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>e06a277b2e3c0be88c96e46a485a5944</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>b08d6bcbd6c69aaec4606786ceb9ae99</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarProperties.html</anchorfile>
      <anchor>11dbd13c35dc5e153da3921b6ab3542d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::CarInterface</name>
    <filename>classlibtrafsim__Py_1_1CarInterface.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>00bf7d5a0bebb956cb585653c3cbe63b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>df39df75508940f5a609a3af1ec7a89f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>9127c82e9612091d0f176ed743f444d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCurrentLane</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>2513fcf9ee7b7825a6421828d69a7d5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setProperties</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>895f80bdbef881a98effcb50f70405f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setColor</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>56c5684693267c27a3d3d2949a710e29</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getProperties</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>d6be831956532cb3fcb2188fb0e5b67e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>ab8d7c23f9ed74512a2d2a1ac0f8467b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>cce2b2d3cf1c698103e60880f26c3759</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>9c36605fa240a8f5c884d3dab461092c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>1066544c8dd07df74cbfbdecbb051af7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>eace4e9f711462b7485aadc7335686d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarInterface.html</anchorfile>
      <anchor>d5760b799baccf0b8deeee96b4649c76</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Car</name>
    <filename>classlibtrafsim__Py_1_1Car.html</filename>
    <base>libtrafsim_Py::CarInterface</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>47628c8fe32e4c36020f99a7b8bbecf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setMaxVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>efd6541bbfc1e115fb1b3df1fca497a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPathIndex</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>35a23351d1bfc2f49b4e5e40abcf2568</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>removeSelf</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>dd2c774a39e7f50b6006b7b23866c5d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>4ab86d1c872aa7f1b1270b2d08a48c8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>invertColor</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>ecc06ea33819fe167be8f0f47c0c40c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>2b09876c2b1e7b1a36cf1e5e199c1299</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>7fda75a87c51beebf2c70d5351e05795</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>67f486ea131bae340cf0555695b8808d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>switchLane</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>81f38303d8eed667bfe6c5e6422a0404</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPath</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>4963a3f497a22d9f6e1e4b6068c68189</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>3b6c29606967e683dc664852918ab557</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>f2af39b9245974149adcbd4832559786</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>871d9310256a82475b58bbb02b8b2741</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>b39815752bc0e1b93908a0d78632a3c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>33f05a238a971f3f525d67136f4d6048</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>1ffe3fb06647078d0aea0a3f0af23d0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>8a53ef9a0810a4f7e3796b6dd715ec73</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>556de26819ee7a767a98d968cd84d6c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCurrentLane</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>1dda52dc164120fd13b7ef75c2f68138</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>c1eb414fd016dd967f99afb8b18cd695</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>fb205d833d7fae2d27b1d0e820680d13</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>af4b4e88f463b41e01fdef0ecb8e7c89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>45ab9c36d9e51b014eb8543a6617aaaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>3e9ec5269c576b181240651c8728cb55</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>9636fc2890453d7756cbd423342e36ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>826420e0993bd98c184b0592d3b4b2bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Car.html</anchorfile>
      <anchor>643ea718c46465d153f9d4eba2430843</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::ExternalCar</name>
    <filename>classlibtrafsim__Py_1_1ExternalCar.html</filename>
    <base>libtrafsim_Py::CarInterface</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>e9d4351297c4dbf47e7c4f046593b0b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initialize</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>ea3ac1725a2ffbb0c576595d9426e2d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>c5c53113fb9df413f5094a65e0d26482</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setTranslation</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>5ba69f6287a565c8c84bc93216a2d3c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>b95bf83490d05990f5626354224b27e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>invertColor</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>2fc975417285562819e35d49d62c99e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>159d87a2d4632247c6b5fc71cc08861c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>checkIntersection</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>5829bb7b894c9897ce0c65fd91b3687e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>846bee0bf1448139da6b91189e203434</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCurrentLane</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>91ca6c4b6f75c8db8141de6895b7839f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getContainerObject</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>ce3b75814bf6d839ece651302562304c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCurrentRoad</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>6bd4ce1d0e8637d51ff5aec912c7f487</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneTCoord</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>b06a0f467685167cd3550594b2f808ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setWorldPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>fea7737cbbfa3cbc28a2742f4ebc0de8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWorldPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>620ba9c48a2eb480a471e7541373e352</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>00194a690b8273a3737211c5de394229</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setAcceleration</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>90edc50518e377ace85cdb9b7c899c66</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getAcceleration</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>a165403dcd9c40d9de2703bf1b2dfeb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>d971759c20fd32e75ca43b4a8f03f939</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>bf921fc83b908a3228a8f251afc70526</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getX</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>1a93f2d83789baa12e4d8ec134de6050</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getY</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>c8b40d30457bd585a03eb2ff50d69cb5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>ac353e6b6cb823b13c7198d6f3f242a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>dd8f79cf9de88f505d361c4174ad3e5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>a106a25d9f409d3de01f9cd0f3d0e397</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>361a1434302bbba5bbc4c22a02029e55</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>0aca0d216bf04f678e6f0771b667a9be</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>8ff1ff5aa3125aabbbcecab313623ecd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>afcf5c689a03f90f8d3819309ae67606</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>aa9d124fc7d3ce50038ae8b92e7b14e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>f7743d55ce5216f26418eacf23b09caf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>fbc82ffd9819ddc01b89e7f01803fac9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>412e210cabe9e15484e92116e352d79d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>0d6bd6120cb522ab134f12e0cd2c0190</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1ExternalCar.html</anchorfile>
      <anchor>5df5aa518b274a3296dc8fddf01c864e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::CarFactory</name>
    <filename>classlibtrafsim__Py_1_1CarFactory.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>0d407050defa85a59332ee104321e967</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>ef3fcca902ab065feed408eace4acfa1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>282c3c4f188d75dc66c310e9672546fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>dbf36d0b50293fb989b55129e0acf188</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setRemainingCars</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>5cc2a3e96be1baf696217238d6d8298d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getRemainingCars</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>ad2d9543206b79a0714477157b261129</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCreationRate</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>1d24ac4fcf0891e002ef6b596809f0e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCreationRate</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>f1d6f67a68213a0a1bc19d8aeca6c625</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCRRandomDelta</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>c6ebda8200d4aa2e37cf284e7bf2a411</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCRRandomDelta</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>6e1a13c7874ff019a775aab2c961de74</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCarProperties</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>32c141ecf5f82a06db8a7cdf7a1d259a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCarProperties</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>6c0e3b9f5679ced0b97ba7659d7fabb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setRandomSeed</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>22d6413f1e0dd4a63bcd3e24928dadd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getRandomSeed</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>a16b74723fe74899a9c49da57d4a8407</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setFailMode</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>07fbe7f385cc15cfd3077751b4f6ce81</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getFailMode</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>3c76c64198a17562c56417dd1c456e95</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPath</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>35686deaee1a6f531f1c784a4bd5a0e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPath</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>b53d2e1f9631cd3a9115296b9d148845</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>984ed9c26445659c8b3af3aea6b07407</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getRoad</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>d27347389b62d78488d501039103fb43</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneIndex</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>06b7731163eb8f3bf6ccb1b01d44401d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getT</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>8f80b5bdff2548ed1f54e9842f3f7564</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>5e48fd6441f0f2cad33d9faad86f1f66</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>115ec66f413c19a5e69c1ae3b1e7725b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>6195b250bb358df93e7ff342bdf9b83d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>c3befddcff8d50387ff01b17522bd88f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>c00735b133d3e847720a11570bb56977</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>2ae8316b3035b66a159fa8aefef5ca1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>519f8f39f1c001b8f6f78c276130b60b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>e542917849a4db0b7e1b4a5a11a89d89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>5012eb1921598d0e8ec1113af894c344</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>3f00dbef62d66fe9e2de28afaba75f15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>5240d5b4e54b373b9f5ac8303c03f99a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>447f8a3b41d2b92c5f3c1327313be992</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>63bf0885646a4c0a370ac068e09941cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>c653ebed1e6ec7929b63eb1115d91de3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarFactory.html</anchorfile>
      <anchor>470a389701c230c7dea99d1224392edc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Road</name>
    <filename>classlibtrafsim__Py_1_1Road.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>3cda662fbb21ff253503e63d4b169117</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>16fdaecf87761f274c1865c3ecd8dcb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>abd342c446d8705ab0cfa28f8fc890db</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>9f891ed3cc7f4c3049828822e99825d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initialize</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>fb449296f0edefc5ce2251a938ced04f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLane</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>553751d525784cc19f11b70251397188</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneIndex</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>a2add387c39c494561227173b475e333</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getForwardLane</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>c2b2ab4bb629c6c2de4a7adfbba452e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getReverseLane</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>23af170cbb98797e069fbe4dee105f24</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>numLanes</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>7ee81e64da0f7489d2c00d14cd8648f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>numForwardLanes</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>311eaa2f691fb60f12ce647dc1d95f41</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>numReverseLanes</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>8a6ee1252964a79d0a638468079cfd07</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isReverse</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>9e7b48be4159ce93ad0a8deb08e634f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getSpline</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>c31ec68af1b7ac196a916624e5f0b25c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getStart</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>3663a09859d34eea6f8002c5b6317b42</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>3b48b22a9b766fd32a9a9029325becd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setStart</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>45fc7c0b808f414e4b008f787dff89e4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>0fe263d5b0396339c0f2cca292aa22a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>5460c19d27301763175ef6edba161a8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getSpeedLimit</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>4f41965cf2cc6e1e97fe133f511c0501</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setSpeedLimit</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>b3ed043263359dfe3a0e1d9823f3a82c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>f023f7e055654bb262ba711ebd8f0ebe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getRoadBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>d95547062861a45c9eb57e4f5c74d718</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneBoundaries</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>a56897eff59442bafa70dd33d7741318</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>e1adf1810db6d9f81549c774364dbfb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>a9588056400d7a723c60c6694a77cd45</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>444e0bd6928da117dcb25cc93623919d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>4166809b21d5aa8570dbc7dc17ca289f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>4b40f7c847ed1f4d76e5b62383e16002</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>6cdf07a8f5fc1ae5224f8fcb3f23aba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>48f496529f1b18feeecc3e3cbdedcd47</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>d67184889e07dbee4700a10b3e485bbe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>5f7cdb0de2b595023af09d92286e9c95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>6f96cd4c0f90acab0f1c06edfc90b820</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>13eeaf7fb45770e0a9ed4cdf4dca36aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>94e8ed2f047abdaac35973524dbe1b90</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>1161f5677c9a410576ff456ff35d5f9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Road.html</anchorfile>
      <anchor>2b5f7099c57861f39c3114501c621e45</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::XIntersection</name>
    <filename>classlibtrafsim__Py_1_1XIntersection.html</filename>
    <base>libtrafsim_Py::Intersection</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>eb8c1d2c9f05a6378241e73761894297</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>d557866700829a90ee2c5bf074cce124</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>totalRoads</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>a7c74f5eddbd3c28233e439f1167f66e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>7df0c536392d83a6e62be29efd2a3a7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>hookStart</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>be6c7fcc2a4106911ef7d130e7c2554d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>hookEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>8e2d52425c5d3c4bc651c22201bcfd45</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>blockManeuver</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>f5877e604223367d75df7d889a47b449</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>unblockManeuver</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>9cae62c9c523e88632be9cafddaee9ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>computeGeometry</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>311054de9c96308c34b42ad8c5210aa4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDestination</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>5c0785343b043f267d9624f4f5f55a4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLane</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>988566b547117a8d7c36333bba2539e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setStop</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>7e3eeabf3bb038d380a9b60b62bff16e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isStop</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>0b4bd08aa5f749782464f862e94f3237</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rightOfWay</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>b8a8aabb9326d341b3bd74df44ecede7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>enterQueue</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>987cf5f2f8185c82bfccb71c0109384c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exitIntersection</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>e0b096c6c371a9fb3fe167ab86fa704f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOpposingTraffic</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>6196d33ae33ef9002ef2d5e9deb37f10</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundaries</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>d971bd5cc9407cbc48db58f2e2541df8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>bbb402d09467bcb64676bf457cf1e976</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>54e121a398226b4c8e7db12553f8e588</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>0195627388a57955d230d1878ec40e22</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>49f166366d41f0603fe5c18ff4b1f67e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>a55312fb993e45af73f4dcfd23e750ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>608535de418cd4dc21a8ef6dcf41bc59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>3471da67bea2b9f9e7ec6a598081a4c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>87d07b5f11bb928cb110582dc9c00b7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>f33ff68ecd7ed88cf2e8777b6820eaa7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>5d524b70b86dfd4b111a1a6631dae27c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>3b0ab9b0b765fbc850565608319b8ae4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>d335229e2bbce4a2228889b311d0e143</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>21965b3f8ab43166f5d6d3b32c7d3798</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>b63fc19aeafc1b750de47c9ec0f5423b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1XIntersection.html</anchorfile>
      <anchor>3d74188f1e00514b704f04669f64258b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::YIntersection</name>
    <filename>classlibtrafsim__Py_1_1YIntersection.html</filename>
    <base>libtrafsim_Py::Intersection</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>eb700ea66aea1750cb2385cd8d0b6045</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>029c1c214c04c25522f26c6789f64998</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>totalRoads</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>3a9163f6cd339842d8219bc066d82ac4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>f20c0819e1c7cca61cf42aac9c710b1e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>hookStart</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>615dd7ee743f653a76a82322724f6506</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>hookEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>bd9765c63beadcfb1364775f001f48a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>blockManeuver</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>a7608cd9f4c0483c58bfba2ce56510a0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>unblockManeuver</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>5a54897f11c3a9781e10e460363dffa0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>computeGeometry</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>156981728d186a7ea274aaf2b4cb0670</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDestination</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>7e1f4c566747fe11802cc367a1d5e2b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLane</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>8f330db1d513aba03e1276b357892c64</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setStop</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>4ac4e21d3b7cbdcd8f4008ddf839d31a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isStop</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>51d933ec15b593b0a332693a8f458ddc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rightOfWay</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>ea233034fdcaf83b6a77f853e5dcff19</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>enterQueue</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>3cd344b1a5ea2abb74af0baaaddf6633</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>exitIntersection</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>992cd050c48ed5e6ea6b6dbb972db720</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOpposingTraffic</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>f5f7d82d22a5c23540c24c929c717865</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundaries</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>6844ad33d8c352ddfa8d19d6b01e19b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>96a3d162f92de9596068761e28347da4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>a25c64548281c3ce2597990ad9490761</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>caeece620b17b060c52faa87019d5c7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>5f54f1cafb86a090df707176c2cd5a37</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>54e71052bee6876777e5b9a6fbbe51cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>c5121a8fae58bb145d5ed27080661afe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>5839ba6d96fa58dd0f9ea107f49a5dda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>c9a1ed51c9684633763f2c43631b5bda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>bc0a99066452debf52e2c21b208796f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>1396e19d345104544cd35696ef74f5a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>55a0b90ded0b52ee84c836d918e68f80</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>3245e2dddcd1efae4fc0e78e8fa77e4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>1777c04772f2c4383f89c1a9d1c8ee18</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>9291f8371287a5e0bbdfbf796c48ede2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1YIntersection.html</anchorfile>
      <anchor>18adc1b6a54e899dd21dcad0511c14d8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::LaneGrid</name>
    <filename>classlibtrafsim__Py_1_1LaneGrid.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>getLane</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>f0a2dbbf23966f5905cac73919d3dee5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>a696b1919151838bf3207d7712e96039</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>99e5a6ac0aabbc99088679138785863b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>partition</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>f1708ac955745dec183c1edf4718d92e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLanePosition</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>19e878bf4fdd9f6ca33d4d39eb8fe803</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>73f1af64cb2cad35f6159070f28f6d42</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>febaee2c37ba1f7745443f76b0a50028</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>77a837589e069c7f5f319a6de116cc98</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>a1dee2c6769973668d3f7d47131660a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>10bc1db54f7873bfc081df081cd36acc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>2de0f4f41d11889f881eab2208057d54</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>b18c6df8a1b062a86cdfa58537528af3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>10de552ff6c7895cd511b1e33476465d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>4a2f293f91aad0d541cb5bb866be185e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>570099e3f23ae900339a408d4951401e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>a257547289f831a382032a55ba6cd2ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>56026b9986211486401b1cf214a16959</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>4920683224f24cdc3a276bb8ddb7943c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>a55e9a140f8f6e22871ad0a59b7108c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneGrid.html</anchorfile>
      <anchor>7f5a36a1de39f9ecfb59c63076764968</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::SegInfo</name>
    <filename>classlibtrafsim__Py_1_1SegInfo.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>setLaneInfo</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>f13429f9919fcb9bd72e3ccb1c6f66fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPointsFromLane</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>12e053106f8f9bdb6a2307ed4a5e4e66</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCenterLine</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>a38c26f91b9ad4f148d460778a351818</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenterLane</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>4f78cf1ac500fb2af4c0fd0592543d04</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>e8fdf5d9bfd82ee287bc18ec8dfa020e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>eee148ffed227d5c3264f979d62347b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>85f5ab45ab0d5eae42cd5a330374f63d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>62755ea7482a8c0ad34f599432f75bec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>bbf89142a3bf41a8cd505fedbee9729e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>15ed30d52e7b2c565ab30016b16fb7a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1SegInfo.html</anchorfile>
      <anchor>4d7039350b70a49f775fe9227f00e5b1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::IntersectionInfo</name>
    <filename>classlibtrafsim__Py_1_1IntersectionInfo.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>67ef14298d49b46c7571cc5d4e60dfb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>16ba76eb353783ae50a26fb6aced29ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>b01dbda73ab4aee9430fab9fc69c3d34</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>a456692c77ae2f157cba3d9783fa070e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>e0c9198ebc5f14cc0b2c00843108d6fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>e0e031f1df80bd5be1b738bc34e71bd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1IntersectionInfo.html</anchorfile>
      <anchor>c92de28bfeed60df06e1e21335ab13cf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::RNDFSimIF</name>
    <filename>classlibtrafsim__Py_1_1RNDFSimIF.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>0ee95b08bbfa5bbffa80a192c589c79c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>loadFile</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>c30c7c42dae5f0b5713dfdfeb6f614f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getNumSegs</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>905702357ec62b1adf887553ef1d78c5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getNumLanes</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>68722b1fdaeac9768656b29364557dec</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLaneInfos</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>47cd24de0e9ce90f494ae9eab7853f5d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getxTranslate</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>c94005a21d66c4c6731012e2a87107a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getyTranslate</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>7fab988ca33aad6fbaa7a76ddbd084b8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenterLineOfSeg</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>3cb24d954ba50b0a84718b20ede4258c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLineFromZone</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>1fbb00e7d3267df8c4dedc47a14eca3d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildSegInfos</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>a0f84ad31b16f3f1bb1cc34d510596fc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>centerAllWaypoints</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>ea430357707c9513fa75bd85106fd48c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>appendStartEnd</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>6325db8a73bdc9e7c6295dd646da1a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>numSegs</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>65fbf684465205b45ef5f646222df9d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getNumZoneLines</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>3f02413cf1887624b8d3efc6206f1e68</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildZoneInfos</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>bd3c18073dc5a7cd907c3fd7c2585523</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>16c119d9bf9068f8acd562a9a496801c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>43b34f4a7be0f4cbb33a15791ed5fabd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>82b2f276f47f2329d373572c5ff57e2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>769a11ddc6874bbdefac1d2486eada2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>e6a8ed0645a334080576c6bfddef22d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>98fdcf34911b969476aa48deefe81f59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1RNDFSimIF.html</anchorfile>
      <anchor>5ff68b84cfc63dcb1db175571b5aeb81</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Obstacle</name>
    <filename>classlibtrafsim__Py_1_1Obstacle.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>5f81313c5d136841932a7e248331766e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>5d1b3c11e51a866048380a43dc7ca66b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>8317d372bccbe11ed6dc619a89e4e1cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>invertColor</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>439e06c84db869b89cfe3e58b5b9d660</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPosition</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>6dc0ca0ba8f585dff0e01800427a2700</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>4a2260908eb8819a8ba8a22095c6fa6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setBlockObs</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>2c85b80ae8b59ce8ceca79f58700f02f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setCircleObs</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>fc4be21b1cfceaf293e1f3cfcd0e14b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>c78eb716a4465dbc4e2a6b2336113085</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>4a51ab27531a3d7724dd7f921e32eae7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>03e90dc541a6119b81fdebb6e6110dac</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>25b42e530f425cc00da12fcebc88a338</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>820573ccbeaf61ebdeaccb4f11cadb95</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>67f1a58dd1019496105838ecb113fee2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>867e97638626e928ba56d386d3bc73d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>f9eec08f98bc37afe395b20bb0dd4f6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>3c7bd78a8a8aaa940bd92f390071ab4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>6bd002bfba0040c1cb399062d7e75116</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>7cf0fbf7f0fa44425c70ad8bee032ff5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>7501c37111ea87f8abf798f22126ec49</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>9ef1bd831314087413082c42130f1417</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>d6dac22a8d978d3144e37da34977112c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>c6bdc52458fdf11925fd62c63304ad0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Obstacle.html</anchorfile>
      <anchor>0868f9bf0cad744ccb456e0c5494e91d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Line</name>
    <filename>classlibtrafsim__Py_1_1Line.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>05a158ebb689da45e893ef594adf5753</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>9b6a35e5c9300d39c085be5eecad942a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>f4d777b14b02878bcf191e4bedc55972</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setColor</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>2a916287b765230b24d67e2254a451a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>61845fecaf9bf4110ad4e42d606376d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>7d285380803f69765d3937f3bbf049a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>953f8aa75afd0769fdb7c317bc4e98ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>fa6c532ab088fb7f5b26ed10b21692ba</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>b41024a6fd181b9b645b67305a234fcb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>5c4c575a14543aa177ced45f2c0fc272</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>9d465ba1a238e36882ca5b21f11e6747</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>abe1814ef228edf52877cd05542e8d46</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>79dc629f218f7b48b6a0f9e9e5983198</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>01c36c9ffe1d53b366a715130f7ab15e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>9d95450b5e113cb48141578d772bf394</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>67891ec812f67e6b2f9e26e103130e40</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>7871b0256b78ada14130f9042233aef9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>1569fef590c1d024cc0b41fd9349bfe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>db4704bab95af65b92f026e1dc847470</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Line.html</anchorfile>
      <anchor>c37fc42827144933ea1afb42d53656fc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::Event</name>
    <filename>classlibtrafsim__Py_1_1Event.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>38157cf86bfe7fb7576b264937fb1809</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>8ae251154c6b15b0f4219dc92345ebbf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>3304c6f7b82c9d6cebd461f07d4883ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>invertColor</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>8569e4f8e1ff58c4811924341616bd8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setPositions</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>9467c474d7c34cb4b1e23dc633c656bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>35827ac8f342e80e8c62d003a66e7efe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>6bbe9a843bd1717a6c31adc816b10b1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>38eaad8221e8627a229d36d1418be668</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>53cfd80b5b9055ae096afe380994d25b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>distanceFrom</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>5cebb82a462374872b3b4d43147fcb44</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>activate</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>4a32f6a63847a3dac16590367179c129</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>3fe6938b8a80b2c4383a12cc74ec8b7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>407f4547e18ac64534f29953d691b924</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>a8d3e0d1767e9261879ef45581d88c32</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>2121debf10bac0b2c5cd2fc10756a82a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>340bc71d11eb441ed732824a48a0bcae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>72fe44f1f85bcbb8074b06c0391a046a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1Event.html</anchorfile>
      <anchor>0428bb84d6865fbd0e35fc01f5d90be2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::SplineCar</name>
    <filename>classlibtrafsim__Py_1_1SplineCar.html</filename>
    <base>libtrafsim_Py::Object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>2775bbf3657985b76755297c68c62596</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initialize</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>18b20fb5311a02734a7bd456a4f1c83d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getClassID</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>94ea8629d6bd31510d2cd71433608316</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>draw</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>64e7ac786d44176151843c15d4aee468</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simulate</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>4078a4a4af9bdbee50b58903c2dc5a73</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCenter</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>410965964ff46a54d5686bf8d46af81f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getVelocity</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>54ee1ba3503e7b745a797376192f6a7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getOrientation</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>4d442fd4059944f7a283c3bd5e0a787a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLength</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>f443e58dc9a83a08c70f5d82d3dfb98e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getWidth</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>249bef6f8c802f9315b5804af12c6820</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getBoundary</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>d2e7430b38c7d288e831611ab0e9f842</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>containsObject</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>7d1a8850aae27b63a5d26b1bb5006b55</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>invertColor</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>1e0212d5e8bea50e0e5d19a183df8377</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>serialize</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>456e05c8093534c616f00b035a8a5779</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>deserialize</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>5591d6e8766a160ebdc91befb0ddaf41</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>downcast</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>7ad69d96cfc1fc658a3cb978e8cf908d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>ebf5032a90f65c28d7c1604f41ef0d13</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>3fdf00c74bc1fd3c777127a5666204bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>a58a2840170a322ff1c1f0ed5ce297ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>dd90129bbbd2f9992c592be5831ff525</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>37f2c2e9116c7decf033846f64ce59d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>4f87e31ad84498525f4c4cd22625e61a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__del__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineCar.html</anchorfile>
      <anchor>013ac8f22cf60518c2550f031247a8ed</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::ObjectDictPair</name>
    <filename>classlibtrafsim__Py_1_1ObjectDictPair.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>079cfcad873422bd8a3aea7b93592210</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>9986f758d98c1b8adc7cddb6f621e25d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>f31f6444dcbff342bcd91ed0f9e75ec3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>514ffa1a4bd6c3cd485cc9b86448649e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>b34f0199d250712e65454136690257e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>first</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>c0bf7cdb34c9fc2d2dc9aace81c366a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>second</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>d9fa84dca3fd82e41048d1429bcb935c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>2916d982a55a95aafe20388d6eb62595</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>73a404f1c19f6ffb749cc4fd19122928</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>ec214d8f2cf6c4fed5fdb333f3b5af25</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>40981725aa0bace976a3cc00a4fc78e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>a45eedb3d8beda93b33e6fc92f29a35a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDictPair.html</anchorfile>
      <anchor>f06364e5201f09ce03482c9c08aa9eb1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::ObjectDict</name>
    <filename>classlibtrafsim__Py_1_1ObjectDict.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>f25e12244035bf1764e514765081bbdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>20ce9d27d1fff8aa2b87db807068773d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>5c302138a5e3546667692b8e8c428949</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>3792f944683959a35839deaf03d2baa9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>63baf6b8059e1b254e3eeb1c69083812</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>4ca9140771b94aed8440c329b3d64079</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>has_key</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>714f13807fb2dc65d40b99ef992369d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>keys</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>315da847dcc34661aa6486c5f6b4fdc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>values</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>e5d9932074f006c28c299cf65e4c5867</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>items</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>8b396d95c707300dfa9038d67d97ced5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__contains__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>a26870644c261f2aa01c2e404d500e06</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>key_iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>e32bf684c586061eb4bebe7547326733</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>value_iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>9e1ef2e34ffa82f44a07a1d333877331</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>20ce9d27d1fff8aa2b87db807068773d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>iterkeys</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>3ef61d0e6e2ef2e3028da685676e8cce</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>itervalues</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>ed9c43547dfd220c621e3a54b90d07c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>iteritems</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>1a89b24559c6eddbe50b5df4a1a51a21</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>76dbdd0deb69a3af8a752c107d790be9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>16d15e73bfb267009d5359cfcc1cd64d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>empty</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>57d37af9bdbd0badcf534c803f02bb7c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>size</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>53dc717f1b2311053eb6d590fea033ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>17e29c92fea8180fb10f2250574e3ed9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>swap</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>77277b2a1e416ccacbb4b33e4296695e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get_allocator</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>21b5bc975f8aec4bcdea6997206ec764</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>begin</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>fa2b04fb8d4803fdd484fe08981751a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>end</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>b317a84d50fdda0a0358d4445b713c5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rbegin</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>b154eeb4ec6f6fedbb98b0e228136d41</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rend</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>b636484c5c7387189d6277fb94b03edb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>count</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>24bced3560e7fd41f0666312fb42f9d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>erase</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>f57c1f0667b9551f857d9ca473dfeb52</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>find</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>b56bbef05ed11a61ae2237f1e4af1869</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>lower_bound</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>93cb09a23461e02b8f44468c8c3705c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>upper_bound</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>a5252e638cbde2ee49f61a249299637f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>c3f52c4956383088bb409e993c5267f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>d746d6496f50d5cab8582417eb38a872</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>145730bfd3ffc7ef218219686a20adfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>c29c409b802c73b6be054a896b24b114</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>7f135847f00d4c03924018c4131f9692</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1ObjectDict.html</anchorfile>
      <anchor>8418dee4791baae220fc5b8fabbe7eab</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::VectorArray</name>
    <filename>classlibtrafsim__Py_1_1VectorArray.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>53a9769bdbf80cccc17cf12f4f1d7fa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>4d97d9aa4e89cb751e4fc9ca94ed2c7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>6f5d0f237ffc1ded0604a4675cec2184</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>3048c9d524b3856128c2f2277a0237d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>3b737a194fc9326825d13d5d2e661bc3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>16af64f66a4c25c924066aa707041813</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>03675b421ba0427ac78cad7a49a03971</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>5a200a74d6f8e78491744e0e72f94819</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>566600be3a9ccd3b3c5e91cff82628ea</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>656a74517790d0e31ae386b2e58b26e7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>0d0de9187cd3543857c9cac6417dd2eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>append</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>1ee241e81efed8b7fbd364e16ca7d2ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>empty</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>9b023bb29348a69f55f41eb8a48d6d1c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>size</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>8709d5632d61d2e95ab15550564c499d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>c4f057c94f16a27deb09124c83d2efcc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>swap</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>d209b981c62a984fc20bc1ca5a115f9e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get_allocator</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>62bca0e829f6cfab078c92374444bc37</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>begin</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>af67c8842552e6c4ef1415e649f6364b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>end</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>29c7e13184c3f22a6e788f5962d7d3d8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rbegin</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>8b73d5b4eef84d167eaaf5e35f63b9d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rend</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>1e30ad0c49655174b021d24d754c2aee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop_back</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>21fcc9678fe4c063e6276010281bfbf1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>erase</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>d780e506d9738961b47d9ac4304f2994</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>4d49ca56108b5d95c660e8be37910a79</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>push_back</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>9932ce10fe5739d04505df8c0097ae26</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>front</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>5b6d7410b7a0d9d904000ea60b8ffe0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>back</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>66f97d99aeadd8184f9d4efd8b72cffa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assign</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>e82c6cbd5bf7080811cc189c836d1426</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>resize</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>3bdb773489e26f0a7e39ea4996399e5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>insert</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>f669df9984014c632a6588905a6e686d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>reserve</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>98c62e33f06a9e2faa18862b95455993</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>capacity</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>663f641ead445d28d78cec13ef80ff64</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>9c64e253cdbb6083ee1f711fa9a0cc70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>6e744e95bd325801856db71f2d46ef11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>313d9f1ae380e8045c5c5e087a7f4a40</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>55f8523c84e9156e292a5846c3f64744</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>ce5acee43a68eb56a17922413c5c4c4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1VectorArray.html</anchorfile>
      <anchor>16eff0a682fa776746c5a8ff182e064e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::LaneObject</name>
    <filename>classlibtrafsim__Py_1_1LaneObject.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>65d70924b76aeb5c6d446b1022cc88de</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>1ace318f8f85ec561560620597da57d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>df355ece70bcb48b61867dbb35e7a66d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>bd3901969e45fa826f38ab845ace4fec</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>5adf9aaf03dad837e314d2403dd52023</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>first</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>897b4fba13357a3923bb04ade6ef40da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>second</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>8207e89e30afe3beb63d998a9fb545db</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>f76e26fe203cffbc8061489b2ace4c77</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>29c7493576c0c16f69227f1861ce47a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>89490476e5c4bf45ae95607c8307dd29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>891a1ac22629fa19a5eb5d3db53bf69f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>a96b3cc6f9aff360ec264f085a3de62a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneObject.html</anchorfile>
      <anchor>206f2ad494509f5cbf5edf2a73f1ff11</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::JoinDesc</name>
    <filename>classlibtrafsim__Py_1_1JoinDesc.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>025b3e2e7586ebc12d818c2fb89c8f48</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>8abc766528cbdb861715f6d03f8a242a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>e3925b3e0a7fac5430eaa2e124e9467f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>0eb49b7ec92908862352b6ff86b0142d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>b8e5a5b7608b9f280c092fcb540d2304</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>483fefa99468d22a7d0b439411ebedd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDesc.html</anchorfile>
      <anchor>523ca14ee214dc7186f3e87867637a6a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::LaneSegment</name>
    <filename>classlibtrafsim__Py_1_1LaneSegment.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>f56f844333d3c7ef366f906e04089d86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>db776315ec281dd29b9381deda260962</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>325b04e64abd45f7edad1bd078b94231</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>2effb08b5f629d291a0ea28382683540</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>a729634496ec9c7fc6a4f5a1930a6425</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>ff6998ea70052a86f667a81e6ce3bf76</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegment.html</anchorfile>
      <anchor>a36a1711f3c9bebbbbf63242fa4b24d9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::JoinDescArray</name>
    <filename>classlibtrafsim__Py_1_1JoinDescArray.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>aaaedeb396c2823d582b5d50d6328f5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>232e246132c2269b11818903ef08e117</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>5d70c2f5d413f266afa3448f7075b48f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>ef8732d1a82db86b2f9038ba7d2692e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>7df9e554d49e5afdf0b2d563018bbbd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>d21e8acf7b254f1e1262794953c03207</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>f9e432a3ba151bea03ead00fe58f5218</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>b17386abf43a552d320964e1fa5cece3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>59f8ca98741f3840882de99dd7a0c81e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>aeba4e36f69cda25692c808e094442fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>95b09b6c43b17dfc6fd7a8f20f07d88d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>append</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>b7cacbb24a6023b5e7a6f9c9be0d617d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>empty</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>6d125030b729d7802757ea4ac76e2828</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>size</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>fd7d26519b78f081e00e2087a7453190</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>f4b2a4e92bea64dd0a53de5d3af02338</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>swap</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>92ba0bf55e063564be020a518ffa6a6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get_allocator</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>b62ddc00601b8b66719a49f9d5960d40</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>begin</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>b6e8f34c07260cb76a51bd1a1cb448ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>end</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>f4c8a4db98e1f14135297580227f5537</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rbegin</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>bf6b69d9f03f31f04d8f08faca852c84</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rend</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>fce86a7bda45b0f038998c9e70202ad4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop_back</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>a2b5725ef29ac5210b4fc63969ea9d49</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>erase</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>5ffe4005c0d0d7c68c0eac4fe9b9db9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>70367d0899938c413522c8a3b348484b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>push_back</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>754de9b10ab09cddf3a56c4e9be1e76c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>front</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>d15f4ed10bd19ea4d0af5ab1899da523</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>back</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>bc9460aa7a5fea3797744f21076eb522</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assign</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>e9ce452420c3db5ee45644d021699489</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>resize</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>2e175807a8cf47234bee3f3d5154812c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>insert</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>0194eb67e971ede23e62e3eb5f05b980</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>reserve</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>6c50520c4f6831eb8d8e9b01f1c29869</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>capacity</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>a686bc8b18dcb303c59aef988dc6a8f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>61de5395e1eb15e23b8db9c13d2a7c7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>d1e7b06f429faf184140600462e17ec9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>8b60e08965b97033b8c96edc5ebd5ea0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>6adf468698dbf25346bd6da5733339e9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>e32fadf5d45c8e54603192114784ae34</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1JoinDescArray.html</anchorfile>
      <anchor>55c49fbc4e4c9f283a2aedaf3df09d46</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::LaneSegmentArray</name>
    <filename>classlibtrafsim__Py_1_1LaneSegmentArray.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>15445978e1b30503cef53ce34f0a781c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>6cda5b04134eb5729e90b46bf6953b13</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>9f24fd27c6d426b91b34b978a343dc0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>ebce8170bdec39a8af35999da863b6b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>38699d2484ce9c1594be2508ad1c169e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>8d0bfb644512c835deb138e9e06d9846</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>51954f83e1ac1ac0086717d3e0c75200</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>d99a34df04674ee24b5b171a5ff8f678</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>e5ca7732ed05994c292350c4bf86ce02</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>b10338990e70cc185b6f77a64f359776</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>9d92df367e24122a5f9703a9f85d7e7d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>append</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>4bb0b402a79d6d870c6db8b259cfff4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>empty</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>ac347be555ebd1da82ffc7a43bd58049</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>size</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>afb52e2a57e8646d2a484b24935a0a07</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>2bc5e4b3e3772fe32644f1770317605c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>swap</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>f759e1acc93007c655be2c573d41e864</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get_allocator</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>1bde2d675d1839b1773637799071f1a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>begin</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>dc388f6a1883e491fb71bd6c7b91a904</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>end</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>2b212f82107876864418da364615cbaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rbegin</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>cf9f094a9dd63e9aae6ab459543f9667</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rend</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>3a6c32adedd70fd4185e4c65f7e6cdb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop_back</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>feb1c84d3478cff6550740c4c720e450</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>erase</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>32a7a3b23affc008037dd7b93ca9e72d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>a2c2387981fa5723998780bd11c68f5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>push_back</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>d4e5739c06eab6e4b77de9af17caa43f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>front</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>6c373ebd87c92088f620be2a871be35e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>back</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>1c31534eb9ec79c6ceba0a13802a065b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assign</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>0487f849ffcc83564a8e490097ea19e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>resize</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>a3b4a640bc4c1c0ce8d9e18e80088106</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>insert</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>ff46dfafc3fa23eac3d3fb34315c8b6c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>reserve</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>4659860095123199db81eb4eba86e50e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>capacity</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>af334530fe9f45b82141d00c688a080c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>8cb6e08a6424c31ba3b687eda8282b89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>7e6c449646f6b3b89b399a31a6ae038c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>7cc6e6bbf665801205675fbe6651086a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>e8712d889baf0d7e29e5ae027aece058</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>a7b2e376dc1c548dbefb4244c2b9788c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1LaneSegmentArray.html</anchorfile>
      <anchor>5379062a896fd9c0a5ab091c4efa47cd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::CarPath</name>
    <filename>classlibtrafsim__Py_1_1CarPath.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>b2fb12522475caf8b3449695b1b8badb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>af624d8aba85938180838b4bc62a27a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>d3f6ee7612b6bf5e5de757abf8d9e053</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>792db1ffcd68012f306a0cffdac9439c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>fbbdebe1d9fe461fc2700103bee2e972</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>51bd585dc95e002d9b6c64eb018cf61c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>f2211ec33b85f2e7d088f4b935b06003</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>1ea784b5f236c0066d111e490d61a992</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>14b937048067809e94ef851fe7d1d92f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>12ea6de338644abcd17d73b339f1099f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>df15d5a96929d509ccfd6d2e0ce97462</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>append</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>1cf96d20196ac4a5be7b1959391411ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>empty</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>0c9df696e48cc7cd359b5083bf6c967a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>size</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>64503342f02c7fe06511f3d7fb96aadb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>285fb1e41c25d52cf062608320653a34</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>swap</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>a9f4679961d7cc5cd2f6b762ed7647c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get_allocator</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>1954f383a5507659ee81916a4b07f9b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>begin</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>201ed28e85f37dedf1406e608c63e824</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>end</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>fd007b158d1538e5fc281090eeab7778</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rbegin</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>1691b53a673c66e37439fa8d2595709e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rend</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>a8e14c2abb6e18e327ddaccb72b435a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop_back</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>4bf55ee1192ddcbc9ffad1e7dab41340</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>erase</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>f1bb023e403aaeb92968c5b78ee42fc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>4f5fd159455d2c55281d846ed7a0817c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>push_back</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>fd0cb9387b09b34520f7ba83a7cf9944</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>front</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>8d4601e46be42badaacb1be06a0cc9d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>back</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>7651b387b42d89a3c385f2e97894c685</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assign</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>da3ad0453d7755fcef9426f4cec7af01</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>resize</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>dcbe42e15e7cff6bd85e2789057161f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>insert</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>bc0c6c77301cdbd52604506862fb8a59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>reserve</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>72c1b9f2b75af477a604f20adf858d26</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>capacity</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>35ada42b421676a59fea3ee1b81d6373</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>baacca2bd98fe1351d9a2cc2194bceb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>1920de6e691166c1d928c97d4f560282</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>2fc07a932d78c8fc16668595c33c1282</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>a09ff2f65b482fac7883fa9f0aac864c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>588e4443c62c9bcfccdf6bc8f3bdb704</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1CarPath.html</anchorfile>
      <anchor>aa86cb4bf01f6db411beae4793337704</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>libtrafsim_Py::SplineArray</name>
    <filename>classlibtrafsim__Py_1_1SplineArray.html</filename>
    <base>libtrafsim_Py::_object</base>
    <member kind="function">
      <type>def</type>
      <name>iterator</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>df408ccdb676bb82a76c7a0f5a3b54e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>ce0a9c1777b85e1f73568df31057f12e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>1592471ecae146611e8f4f38f5e05556</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>0ecb10d684e59c86735de2a3ef94bebf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>9f8673f472f23be0cd55bd3b344dbcc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>ac72c0117ac2e047e632264d2b733518</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>b9833800554092acbb500cf344cb6799</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delslice__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>606f4ac79f51597f6a1d302a4321bd52</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>ccf1fd1cbd964e69a1e8818db7eefdfc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>ca7215dc73e524904db1faa872359447</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>cb70443998ca69ce6704864d600dfc2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>append</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>a2fbc5de406c7abdf9fb07ae37a46a46</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>empty</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>20b8d57f9ac0adb4a34d2a1ac650d30a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>size</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>593e6a74c8d0e980a381da70cebe4b45</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>clear</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>2bde19fa545fb5ae921395b05a722952</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>swap</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>c6429999ade5fe649ae76e0b7e35a98e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get_allocator</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>76c9b20b4744caf692cebcd7b4681bfe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>begin</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>aced1b56999c6bb021bc87bf52b57c65</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>end</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>1d32fa26106e209e166bcb934a4de9f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rbegin</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>984e6aca3166aef8af33a350616dc81c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>rend</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>34ed2b49378fa41da37df163beeb1b3c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>pop_back</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>9e218e0f00dacc6e4e05dfb9df350061</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>erase</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>05b637da2e0a6db072cd12b7ffb877af</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>0ac7f0964082afe309c555407ce53a7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>push_back</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>3780389675bad3b99c00277270c3eea3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>front</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>7895ab641fe4ee4a07c1ac9f03b1062b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>back</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>17a33a8fe9bfddca5fe24a604fb0d841</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assign</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>9c513254020951e8556eba074d318d11</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>resize</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>4ce76640dfc0a762f62958418fedb96b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>insert</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>d51f1ea92546ea086b54dd7f5671a9da</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>reserve</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>73613cb9175a83b35744f476b5aa1ad5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>capacity</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>62043bb1a1d523c455f9b13a035da45f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_setmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>c626a5525849324b4bf495c9bf68c47f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__setattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>e1b850559c035578f679a321086694f9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>dictionary</type>
      <name>__swig_getmethods__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>3dad528c913d2c9461df4f5b6aee17eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>tuple</type>
      <name>__getattr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>cde5ee62c33621c87e999c783959f885</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__repr__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>3ca2d161edff0a7fac4cee9805927464</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type></type>
      <name>__swig_destroy__</name>
      <anchorfile>classlibtrafsim__Py_1_1SplineArray.html</anchorfile>
      <anchor>0b9aaa85468b60fece3262c9f72447ea</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>libtrafsim_Py::*</name>
    <filename>namespacelibtrafsim__Py_1_1_5.html</filename>
  </compound>
  <compound kind="namespace">
    <name>ScenarioDictionary</name>
    <filename>namespaceScenarioDictionary.html</filename>
    <class kind="class">ScenarioDictionary::ScenarioDictionary</class>
  </compound>
  <compound kind="class">
    <name>ScenarioDictionary::ScenarioDictionary</name>
    <filename>classScenarioDictionary_1_1ScenarioDictionary.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classScenarioDictionary_1_1ScenarioDictionary.html</anchorfile>
      <anchor>10a26adc01c3938e3a914ce175b36a87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>scenarios</name>
      <anchorfile>classScenarioDictionary_1_1ScenarioDictionary.html</anchorfile>
      <anchor>036754c790530395703b486086659218</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>function</name>
      <anchorfile>classScenarioDictionary_1_1ScenarioDictionary.html</anchorfile>
      <anchor>e31f17a03b34b916c96facb18e81b56c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>Scenarios</name>
    <filename>namespaceScenarios.html</filename>
    <class kind="class">Scenarios::Scenarios</class>
  </compound>
  <compound kind="class">
    <name>Scenarios::Scenarios</name>
    <filename>classScenarios_1_1Scenarios.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>cb1522b4b2e56905f2e925734a91dc08</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>UT_tplanner_RR_SO_LegalPass</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>69b2b4923ddb7dbbaf2ac8c0d88829bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>UT_tplanner_RR_SO_IllegalPass</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>3f9ba0f882a06ef23c6f91bb8e2b741a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>UT_tplanner_RR_SO_IllegalUturn</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>2a3348adaa4e0a24229814029cedb2e1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>UT_tplanner_RR_SO_Partial</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>838ada1793c1ce001c4f1f83c657c405</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simple_straightline</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>d9b3ba7824db65190c65ac374cf360bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simple_turns</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>2b1793c68190bc1340c5cf7b7a95d32b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simple_staticobstacle</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>b4745c4615bd12f06257f4cfec0af897</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simple_roadblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>7d6933aa9011b0af9a41e909d01239bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>simple_circleblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>093d044507391c5c162c85ca91be9d92</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_roadblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>3918b998920f33b7229b41322bd219fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_partlaneblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>50bcead5bb0228fad46336fee9129bde</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_laneblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>c662b74153341be95879bc744bebe8ff</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_small_partlaneblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>f81f99b8b0f16601a9a63943dee2342d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_roadblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>b1b7ea66ae9356a9581ec53dd6cb4cb2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_passblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>715b515bea67d5a6585671b3237936e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_block</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>a7e355a3a91a87c3503551dd4d418046</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_partlaneblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>38cfa521dbb905a1876c06788187e4b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_sparseroadblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>08648871b251f10c3c502059ddf2851f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_turnroadblock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>519fe2fbf94c00f94c0926d76eaefd8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_simpleintersection</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>20fd614a3fb72881f030554be48498f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_bigIntersectionBlock</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>2ce5d15bfee9e499012881e8bda48b46</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_fakeIntersectionOrdering</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>d725e232b242c358c1d47d9c49b539d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_directedDriving</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>44fa0dab5236cd5c726a3ec0ade3f3a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_laneSeparation</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>74da0bc39df8ded2f64bfb7c22a40807</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_centercircles</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>d587a0bb8dd57a8803ae9b1477f5f49c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>greenscreen</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>2d05ec9d29e25641ccdcdcfbcc7fb84a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_slowIntersection</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>4c0a53cf186976f7c96144706d102b90</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_following</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>17c50a83e3a608a688f349c0b1ed2e3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_intersection</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>93779e49215428092fa119a17149a1ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_simpleintersection</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>20fd614a3fb72881f030554be48498f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_sparseroadblock2</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>70d37b11fea949d4d3d1be64f378f881</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_sparseroadblock3</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>13868c0ee15d144ed3ba4951fad461db</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_bigintersectionblock2</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>77e39816d41eee7781f50f7e6136f288</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanitaCarOppositeLane</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>558734474680f61efad174a2f8b6c1dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_fakeintersectionordering</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>0e242849cf755d55cea5c2c96d9a96de</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_laneseparation</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>7212e9b3a211d7f2baa36a5c60cf65ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_waittopassobstacle</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>7d0a140f969b34c706ac5ce1dd1316d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_longwaittopassobstacle</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>6ed661d506f4ebb120c8a55dfc030a79</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_small_caroppositelane</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>f031b245fec9d4653e366abdab8e1015</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_small_carfollowing</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>4f3a9f46a4d0fe735e847ad16f15b4ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_small_waittopassobstacle</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>ceee17c1777697cda24020d425b8c157</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_small_circlingcars</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>84706dadd7f9ac7c9b3d4c64a245c4b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_intersection_scripted</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>e3db9e6db59479cdad82b5bb5c0dab01</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended_intersection</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>e550d64ed30f21d67da5866799bd99f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended_obspassing</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>43d3c0bbc1945a0c2d91367664b68876</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended_obspassing2</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>e648f7e97161d65914adb7fa56f4610d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>df69d741b02263a4824919608bb18a4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended2_beginning</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>6b1c59235b844d3398a1a0c457efd0fa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended2_following</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>faebde4021f8a5f0eb1cb18e1a8db792</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended2_intersection</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>50de2bc04df746041f56e727530ca89b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_extended2</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>0438d86c84e9396bfd8dd87d1b784451</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>santaanita_sitevisit_onecirclingcar</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>d1fd05af9931079cb99fd51c734a64ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_small_TESTcarcontrol</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>563151f0669fcb17333950d53fa11811</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_carlauncher</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>629b8444ae57f815f6216bd5a0ebe0e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_TESTcarlauncher_launcher</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>f23a4a37cfaa2ee419d2622aba447b4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_TESTcarlauncher</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>8b15f9bd3969ce0c86fb1b698873552b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_TESTcarlauncher2_launcher2</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>7bb62c1258b851b5c4e4a08d9a1db6dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stluke_singleroad_TESTcarlauncher2</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>1ab8d62aa2bde1f530a2d3425ad08414</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>NULL</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>af6158fe05e7cd92f97fd8e5eb6903ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>environment</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>dc6b7d5202a2820b9fe3819bba0ce8aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>editor</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>13c719a8641f4f8900b86858156eec6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>sel</name>
      <anchorfile>classScenarios_1_1Scenarios.html</anchorfile>
      <anchor>b420905b96602650e357d81881c03e22</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>SplineEditor</name>
    <filename>namespaceSplineEditor.html</filename>
    <class kind="class">SplineEditor::SplineEditor</class>
  </compound>
  <compound kind="class">
    <name>SplineEditor::SplineEditor</name>
    <filename>classSplineEditor_1_1SplineEditor.html</filename>
    <base>SuperDynamic::SuperDynamic</base>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>245173f1299d0fb3a4e5372d866ac455</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onMouseMotion</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>8301e6d759388def75c9201a86c169c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildFromPoints</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>1e628a211d8d40cc82bd76927ff6296b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildLineFromPoints</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>af1e3b292bf21f36d7b3b7fabd32c1a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addCar</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>ff7c396860234a1b8be12174d0a70cfd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>buildSplineCar</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>b446165dfdafd59acb7a137643efc660</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addCircleObs</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>1cde5f2bd95a40fa2986f62c56ef3e64</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addBlockObs</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>a3f227d25c523a0e3d6ae80354d55353</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addEvent</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>fccced393829c1141600cc2aad2523e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>addSelect</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>0a4ccdd02d7a057fd7c5099bd0c66db4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>select</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>c8a37308d154321980c2e273412e1034</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getEvent</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>6f40d767ae25969557f809b63ad85859</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onMouseClick</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>3dd8de68aeac343ee93545d54409f287</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onMouseUnclick</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>23a3d4eef353281d07873406cda28e00</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onKeyDown</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>d58dc9589ccbed9ab1b965993597c4f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>onKeyUp</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>20dcdeb9fe39a444e9df39fd88e97613</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>environment</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>db3f4d0e533b28aeca37bbd2c8811405</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>selectedSpline</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>b029901652d9761a4013a5d5c7f3c3af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>selectedPoint</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>4f7727809f5d16b8b187b72597c36027</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>selectedIntersection</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>4708e2d6721aa0093d95e42b65ba8341</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>state</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>2493cfee8f6ac29d05aad189305aa678</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>clickedCars</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>0d4d9e7b6a43664286638cff1e9a4412</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>numCars</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>e975e7e0c1ff5f2cde1f0749b4b559e5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>carMaxVelocity</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>678dbbc79c68477b33b9973ce2021b57</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>carPath</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>2ddfabd00831e9f09c3e47361f5f2ad1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>obstacles</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>d36e87148ee8f5c979fa4a935c55298d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>numObs</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>237dcfdf16c43a3a7254cb7767a6c147</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>length</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>6145036eabee65bce3db0b2174d83d53</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>width</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>bc14bb58dcb5ebec94c1d89fdf0d5151</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>radius</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>52865276ecd44f9e244d689d34941cff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>orientation</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>1a6d33eab421f6301113931ae2347287</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>enabled</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>dbc71e9b210f4474dd075c07f4c91cb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>obsFile</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>16dbecb26d036855a6501c5826275e41</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>string</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>55cb67d525589e802f2e3c0e9e868c73</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>makingEvents</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>bdb97a5e485715dc37f22026415dbdb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>eventStrings</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>e0225646f44bee03ab87ade6a5527615</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>events</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>e1b99b523b9ea4521cce257d4719161f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>eventScenario</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>f26bc549ed398b1e7e743f4e97a066a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>eventDistance</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>432adaf71cbaa598b542f9f98f6b3baf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>eventDelay</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>3ce4c0306dc61a9b25fe9174a48ba7ca</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>selected</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>f964af537f761e0cdda01dcb548b2eb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>selectedName</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>32b51e20e1cb86b568a3bccff7b0432d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>sel</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>3650a0f55f17c394983ab986a06f702d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>selName</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>9d9929de523a07c8c4d78d501b390a5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>X</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>2ec1253cab668bb264ee34e425d9ebdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>Tcoord</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>0ada49b688cc30ce708491a994253b58</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>carPos</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>00e1c2b0864215987bf80201edbfa53d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>lastCar</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>cca7abd944df32b1515b92e36549eb38</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_switchSpline</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>bd1683dcbd092c0349095e815055a578</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_selectSplineAt</name>
      <anchorfile>classSplineEditor_1_1SplineEditor.html</anchorfile>
      <anchor>b09cc58a2c59c5df843c3c27cc31c959</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
  </compound>
  <compound kind="namespace">
    <name>SuperDynamic</name>
    <filename>namespaceSuperDynamic.html</filename>
    <class kind="class">SuperDynamic::SuperDynamic</class>
  </compound>
  <compound kind="class">
    <name>SuperDynamic::SuperDynamic</name>
    <filename>classSuperDynamic_1_1SuperDynamic.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classSuperDynamic_1_1SuperDynamic.html</anchorfile>
      <anchor>e707759571c3c6323554b45d3ab80681</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>reload</name>
      <anchorfile>classSuperDynamic_1_1SuperDynamic.html</anchorfile>
      <anchor>20bd77886cac7799aa7d804a41a02896</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>list</type>
      <name>class_list</name>
      <anchorfile>classSuperDynamic_1_1SuperDynamic.html</anchorfile>
      <anchor>54414efa2c04b36865a8e39ee9242d69</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>def</type>
      <name>_qualified_name</name>
      <anchorfile>classSuperDynamic_1_1SuperDynamic.html</anchorfile>
      <anchor>34c2ecaae1698a0c8a9106b072e8f083</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>TrafSim</name>
    <filename>namespaceTrafSim.html</filename>
    <class kind="class">TrafSim::CarInterface</class>
    <class kind="class">TrafSim::Car</class>
    <class kind="class">TrafSim::CarFactory</class>
    <class kind="class">TrafSim::CarProperties</class>
    <class kind="class">TrafSim::Color</class>
    <class kind="class">TrafSim::Environment</class>
    <class kind="class">TrafSim::Event</class>
    <class kind="class">TrafSim::ExternalCar</class>
    <class kind="struct">TrafSim::ManeuverDestMap</class>
    <class kind="struct">TrafSim::ManeuverLaneMap</class>
    <class kind="struct">TrafSim::Queue</class>
    <class kind="struct">TrafSim::IntersectionGeometryParams</class>
    <class kind="class">TrafSim::Intersection</class>
    <class kind="class">TrafSim::DeadEnd</class>
    <class kind="struct">TrafSim::LaneInterferenceDesc</class>
    <class kind="class">TrafSim::Lane</class>
    <class kind="class">TrafSim::MultiLane</class>
    <class kind="class">TrafSim::LaneGrid</class>
    <class kind="class">TrafSim::Line</class>
    <class kind="struct">TrafSim::LinearSegmentDesc</class>
    <class kind="class">TrafSim::Object</class>
    <class kind="class">TrafSim::Obstacle</class>
    <class kind="class">TrafSim::SegInfo</class>
    <class kind="class">TrafSim::IntersectionInfo</class>
    <class kind="class">TrafSim::RNDFSimIF</class>
    <class kind="class">TrafSim::Road</class>
    <class kind="class">TrafSim::ParseError</class>
    <class kind="struct">TrafSim::enable_if_c</class>
    <class kind="struct">TrafSim::enable_if_c&lt; false, T &gt;</class>
    <class kind="struct">TrafSim::enable_if</class>
    <class kind="struct">TrafSim::non_string_iterable</class>
    <class kind="class">TrafSim::Spline</class>
    <class kind="class">TrafSim::SplineCar</class>
    <class kind="class">TrafSim::Vector</class>
    <class kind="struct">TrafSim::ViewportEvent</class>
    <class kind="class">TrafSim::Viewport</class>
    <class kind="class">TrafSim::XIntersection</class>
    <class kind="class">TrafSim::YIntersection</class>
    <class kind="struct">TrafSim::non_string_iterable&lt; std::basic_string&lt; T &gt; &gt;</class>
    <member kind="typedef">
      <type>std::map&lt; std::string, Object * &gt;</type>
      <name>ObjectMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c5a24828073ec4b549ef717e3c88cff0</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, ObjectMap &gt;</type>
      <name>EnvironmentMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b73d3ba98865b191467b92a3abe8a048</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, unsigned int &gt;</type>
      <name>NameMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b9e825e599a18a2caaaf15cb82ad996c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, std::vector&lt; std::vector&lt; point2 &gt; &gt; &gt;</type>
      <name>BoundaryMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c5e073fd109afbcf5abc521192bd6436</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::pair&lt; Road *, unsigned int &gt;</type>
      <name>RoadLane</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>58408b9657f0c7e789a3be517dfc9306</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; Object *, float &gt;::iterator</type>
      <name>OtfIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>2fba76fc7fe8d581578a7c71c18b9cee</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::multimap&lt; float, Object * &gt;::iterator</type>
      <name>FtoIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b643ee3f3a5624c82adf16e3623ea63e</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::multimap&lt; float, LaneInterferenceDesc &gt;::iterator</type>
      <name>InterfIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>2abb93d1f6c1eb34d074ff61fb1739e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; LaneSegment &gt;::iterator</type>
      <name>SubLaneIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b2e9fdd2bc60ff2c7077eae4945026aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; LaneSegment &gt;::const_iterator</type>
      <name>ConstSubLaneIter</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b9a76a5333a8ee63ff2e7f7c6417c61b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TrafSim::LinearSegmentDesc&lt; Lane &gt;</type>
      <name>LaneSegment</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>63c0d2f5bd275e219a9475f6d6e38896</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::multimap&lt; std::string, Object ** &gt;</type>
      <name>DependencyMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>38338ffa3c131debde6b5142eba3c3fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::map&lt; std::string, Object *(*)()&gt;</type>
      <name>DispatchMap</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>3c047e5ea20ab22bf1d85dca0fd6c8b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>TrafSim::LinearSegmentDesc&lt; Spline &gt;</type>
      <name>JoinDesc</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>6d65d2eecf499fc771cf3d9d43bf4dd9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::pair&lt; JoinDesc, JoinDesc &gt;</type>
      <name>SplineInterferenceDesc</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>b4e4a153a60813a6e6dc59ec38d3c287</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_NORMAL</name>
      <anchor>118938ef36054ba328d21fcb760d46f7cd33e3630ac1d2db26fa9cc95750f604</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_APPROACH_STOP</name>
      <anchor>118938ef36054ba328d21fcb760d46f76ed23e6f008a193ef49564376913ebc4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_WAIT_CAR</name>
      <anchor>118938ef36054ba328d21fcb760d46f77ad4b9e50828d30c8ac4a3996f1ac2d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_WAIT_GAP</name>
      <anchor>118938ef36054ba328d21fcb760d46f7f75a11f8147f747ca7818b6890721118</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_ENTER_INT</name>
      <anchor>118938ef36054ba328d21fcb760d46f7e9a36b7c7e18ae08dafe2ad57e1ad620</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STATE_CROSSING_INT</name>
      <anchor>118938ef36054ba328d21fcb760d46f7f19af5e9a559dc0847b91d1d99c21f1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>CarFactoryFailMode</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CF_FAIL_WAIT</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86d17799a62a2c24faac32fb138698570e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CF_FAIL_SKIP</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86c9708ecd80e1bea8e59d72c5e993a3cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CF_FAIL_OVERLAP</name>
      <anchor>5d0a0034cfd77d41cbe231339eb27b86b4258922313ed5909ea0b64663a8809b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>RightOfWay</name>
      <anchor>efba763deb151aa5e18072cfcff1064f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ROW_GO</name>
      <anchor>efba763deb151aa5e18072cfcff1064febbac998fc9462476d3e87a6d3c81c56</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ROW_GAP</name>
      <anchor>efba763deb151aa5e18072cfcff1064f6037de699e8a9bd6a98f5b5eafc3d0a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>CrossManeuver</name>
      <anchor>01761d7c70c6c6683036c3436743973d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CROSS_LEFT</name>
      <anchor>01761d7c70c6c6683036c3436743973db4a9eceb412ba0167d55a12dd75dc798</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CROSS_RIGHT</name>
      <anchor>01761d7c70c6c6683036c3436743973d1f8cc67545fe0fbaf98ccf199cf8fd91</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>CROSS_STRAIGHT</name>
      <anchor>01761d7c70c6c6683036c3436743973d6e93787da572979dadd8d538ed226798</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ViewportEventType</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>EMPTY</name>
      <anchor>e56e3e21dac6f23a82119589ffde635dfa19472d230c281b2be86947918bee44</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MOUSEMOTION</name>
      <anchor>e56e3e21dac6f23a82119589ffde635dc588df8d990959af9a884d6e270d7b2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MOUSEBUTTONDOWN</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d4aea8e33c2da8e975c2170fa7b51ba6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MOUSEBUTTONUP</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d3c54336c07c447ce6939d6c1f8da621f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>KEYDOWN</name>
      <anchor>e56e3e21dac6f23a82119589ffde635da4dc9ecb8c724335ba669c2addd3e956</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>KEYUP</name>
      <anchor>e56e3e21dac6f23a82119589ffde635dbe90e6943e0813571a861a9226616b57</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>REPAINT</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d802ab113d387be5b07a81b657b60f829</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>QUIT</name>
      <anchor>e56e3e21dac6f23a82119589ffde635d68b82d03d6a7d5fce989ee448149ec54</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>ViewportEventButton</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LEFT</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e96b105c29ad61c3d5f648a435a9c8811</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MIDDLE</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e696ce63bea8cd757736987b44af81c61</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>RIGHT</name>
      <anchor>0aa36468967cb5f4053ad9e581aec16e798d1c59d410950cc290311508dbace5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isObstructed</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>bf389411fc611f79a746d3f1c35645b2</anchor>
      <arglist>(Lane *lane, float t, float threshold)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>f2253a992e82639cfca1c1f7d89e770d</anchor>
      <arglist>(std::ostream &amp;os, CarProperties const &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>1a017d6feec4e48d89f709c5b38ca698</anchor>
      <arglist>(std::istream &amp;is, CarProperties &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>glColor4f</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>610e9609d1e15a65939f7fb5fed864b6</anchor>
      <arglist>(float, float, float, float)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>glSetColor</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>03ed727b566d08df4bdb29dafc9542db</anchor>
      <arglist>(Color const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>edc3bbb875348c83843d897ae9d56626</anchor>
      <arglist>(std::ostream &amp;os, Color const &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>2373e5ed11c04237e4500b7931c57ff0</anchor>
      <arglist>(std::istream &amp;is, Color &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>curveConstraint</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ceedfff07db33764431ef7195fa49637</anchor>
      <arglist>(Spline const &amp;spline, float t, float vel, float linearAccel, float angularAccel)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>273fb26838444932c42b2f0ad2a897c7</anchor>
      <arglist>(std::ostream &amp;os, Environment const &amp;env)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fb8c1c6373fdea81b61986b9b7e5b151</anchor>
      <arglist>(std::istream &amp;is, Environment &amp;env)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>evalIDMAccel</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>35b3f51ad45f372da9102edf891d0b82</anchor>
      <arglist>(float myVel, float otherVel, float gap, float targetVel, CarProperties const &amp;myProps, CarProperties const &amp;otherProps)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>cc58a736b80d7d6ec75594245f32ae40</anchor>
      <arglist>(std::ostream &amp;os, LaneInterferenceDesc const &amp;desc)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>7ce1691595eacca2cab5a9b0232aa4e2</anchor>
      <arglist>(std::istream &amp;is, LaneInterferenceDesc &amp;desc)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>6945070048ef9d72e476a2783101e059</anchor>
      <arglist>(std::ostream &amp;os, Object *obj)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>4f6b0d0671267891ee6726f19b79ce65</anchor>
      <arglist>(std::istream &amp;is, Object *&amp;obj)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>_drawOffset</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>594680fe336397087787a70358664c4d</anchor>
      <arglist>(Spline const &amp;s, Color const &amp;c, float offset)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>discardWhitespace</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>14f75f080b7412f2293d00bab3520286</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>trim</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c2c73088893cd8a29affe8f567c073b4</anchor>
      <arglist>(std::string const &amp;str, char const *delims)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>matchString</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>54df508bdb3d63fd1e3079bca901b784</anchor>
      <arglist>(std::istream &amp;is, std::string const &amp;str)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>readDelim</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>3c4d671e9d0e4b4c1eb9bb3653e48760</anchor>
      <arglist>(std::istream &amp;is, char delim)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>33590be66761c46491cfc63aec8e73a7</anchor>
      <arglist>(std::ostream &amp;os, std::pair&lt; T1, T2 &gt; value)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>c44b2ffb5e4b2104b57fca50c3cba3fa</anchor>
      <arglist>(std::ostream &amp;os, std::pair&lt; T1 *, int &gt; array_desc)</arglist>
    </member>
    <member kind="function">
      <type>enable_if&lt; non_string_iterable&lt; Container &gt;, std::ostream &gt;::type &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>76f266af8d6d9532f568bdf4269afe3b</anchor>
      <arglist>(std::ostream &amp;os, Container const &amp;container)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>049474cf761f78b5fea8bebbe8fe26aa</anchor>
      <arglist>(std::istream &amp;is, std::pair&lt; T1, T2 &gt; &amp;result)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>d3f10b9dfc7d21b917d3174057d83405</anchor>
      <arglist>(std::istream &amp;is, T **result)</arglist>
    </member>
    <member kind="function">
      <type>enable_if&lt; non_string_iterable&lt; Container &gt;, std::istream &gt;::type &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>32f0f07df1061f5d3e5c4fc80cbbc221</anchor>
      <arglist>(std::istream &amp;is, Container &amp;container)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>_displace</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>93dd84adbd08f7f3bf0c60e52c4d03bc</anchor>
      <arglist>(Vector const &amp;p0, Vector const &amp;p1, float shorten, float displacement)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>perpSegDrop</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fd976ce7a6376cfeb8354bf194096db0</anchor>
      <arglist>(Vector p0, Vector p1, Vector x, Vector &amp;result)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>66b5ff80d25ef27b9aac402fbf7990c0</anchor>
      <arglist>(std::ostream &amp;os, Vector const &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>aa45115f1cbf3fab34c87f73d104ff71</anchor>
      <arglist>(std::istream &amp;is, Vector &amp;v)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>sortByKey</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>fadb650b4ec09edb17b70dd4b9bce48b</anchor>
      <arglist>(T &amp;values, unsigned int keys[], const unsigned int n)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>indexOf</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>af242294b69c9d63a5687ed6561608df</anchor>
      <arglist>(T value, T array[], const unsigned int n)</arglist>
    </member>
    <member kind="variable">
      <type>const unsigned int</type>
      <name>NUM_ROADS</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ae84f6e9f885dd9ec2f1363c9d75a794</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const unsigned int</type>
      <name>NUM_ROADS</name>
      <anchorfile>namespaceTrafSim.html</anchorfile>
      <anchor>ae84f6e9f885dd9ec2f1363c9d75a794</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::CarInterface</name>
    <filename>classTrafSim_1_1CarInterface.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>1079ed24c3748593edf7bf655cc7c9a9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual float</type>
      <name>getLaneVelocity</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>79c3125b6ca9cc0eae5d5003913048bc</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual Lane *</type>
      <name>getCurrentLane</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>e913f3a7e5d17ca9f073f727d7f6eed9</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setProperties</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>dfc2bbb679c46837e205ed918b27eeb1</anchor>
      <arglist>(CarProperties const &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColor</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>6cce0b6009f0e1225321220018e6851d</anchor>
      <arglist>(float red, float green, float blue, float alpha)</arglist>
    </member>
    <member kind="function">
      <type>CarProperties</type>
      <name>getProperties</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>c70f0b5f2c16fe1d33a4af9171124954</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>CarProperties</type>
      <name>props</name>
      <anchorfile>classTrafSim_1_1CarInterface.html</anchorfile>
      <anchor>cec265b4864be47c7a57c47357bb631c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Car</name>
    <filename>classTrafSim_1_1Car.html</filename>
    <base>TrafSim::CarInterface</base>
    <member kind="function">
      <type></type>
      <name>Car</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>13182b5d22a3debb608e79300ea8766a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Car</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>f226725daaad6083410c972e3fda0af7</anchor>
      <arglist>(bool isStopped)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Car</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>fed8faf356be6b340133670de30e70a8</anchor>
      <arglist>(float maxV)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Car</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>d4cf179e4d4acbe2f710abf9884e2412</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setMaxVelocity</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>17fc76af3847e83bc8ae25308fa23606</anchor>
      <arglist>(float maxV)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getPathIndex</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>2947285a6357d70719c5ff48c038d8de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>removeSelf</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>7fb47f00c1053dfad968b7fb718fe86c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>928086d372f39f9e5aaa48ed294cf08d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invertColor</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>39fbf9f39d77bbf99677d579afa7f34d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPosition</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>50be8840e3abba8cbd64869b6ed648c5</anchor>
      <arglist>(float x, float y)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setOrientation</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>abd735fe23e03590b692539bf87ff412</anchor>
      <arglist>(float orientation)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>725dbde99e441b8acb86c82a719630c2</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>switchLane</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>4faa5fc401073bdf8407fc2cc75846f8</anchor>
      <arglist>(Road *newRoad, unsigned int newLaneIndex, float startT=0.0f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPath</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>3cee7ef283be6e469397a0698f2d97fc</anchor>
      <arglist>(std::vector&lt; CrossManeuver &gt; const &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPath</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>b4d7eac8359ee4180f28f594173bd144</anchor>
      <arglist>(std::vector&lt; unsigned int &gt; const &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>69b0bb8713f6991197dda5095730d404</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>2e9e192c57db9aa926bde203c0839ee8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>8a046082fcfc45008ef9acaf4a6aa523</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>4e767cfa163ffa2d399e4d368dc91153</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>e225bf0057f7765111dc5d809341c592</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>e0c19736d57097f1b2c9d9e5918e71aa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>191c9dc760e361d7a850b8a0d76e911c</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getLaneVelocity</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>73969af11473aed69602c6f8623920d6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Lane *</type>
      <name>getCurrentLane</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>ceb2b4b949780a4c876ad5cee701f783</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Car *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>40291f1b9cc6b880d402ae908a6b2fec</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>maxVel</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>8bbaa5f601132748c890cb1b6b3f13a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type>Lane *</type>
      <name>createMultiLane</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>760891e05cfcb1bb369e84cefc1fc44f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>switchRoad</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>ae878af5da8e027a5e8916a518935474</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>updateTargetVelocity</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>eeff54aa934b433183538481f7173427</anchor>
      <arglist>(float t)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>bool</type>
      <name>acceptGap</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>e401a52e6fb0dc52f7ea29aa07e0b794</anchor>
      <arglist>(Lane *opposingLane)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>2c9e10c38870268860d3f70c8daae789</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>85fe3a495e2189eee286c81a947535ad</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>float</type>
      <name>orientation</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>a4f4d128f1efe0a1d4d046ebd17da096</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>position</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>b6f1f3e08d898fcc76a4042db68e7c9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>velocity</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>bb609d9b933ba967ffbcee6479a67f34</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>targetVelocity</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>3c910deebe66eeae6037fa9d4a46147c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>nextCurveEvalPoint</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>685940cbae027aa778398b6b493b0dc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; CrossManeuver &gt;</type>
      <name>path</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>525bee5a5392c3ddfbd9649743dc4c4c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>pathIndex</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>c3c43de174f5ae46827101681f73ea29</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CrossManeuver</type>
      <name>nextManeuver</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>ffe3bfe8ce5a8291b7f6ec0301a3bee4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Road *</type>
      <name>currentRoad</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>46fea291e4bf3b7671c6cb8f5fd16300</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Lane *</type>
      <name>currentLane</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>913f31cdfb96bc48c25bae97c5f99fe2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>laneIndex</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>0584b1a72bedb4de7af6c48ea6303036</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>state</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>4229dbe993448f6239dd5a9c903de9e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Car *</type>
      <name>followingCar</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>77d2c6941b1e7cf5e0e55373069bec5d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; Lane * &gt;</type>
      <name>opposingLanes</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>715c55a6b23485e7d2e8b546b0b2dcfa</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>stopped</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>5b6f478e6f1aa6acdfa82576bd2bf5a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>dead</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>fe62f1f94bd966386dfc122fb4bbff9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>initCarsAhead</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>089d632da5bd925eaa4330a003295b45</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>initCarsExited</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>478a5c5851e5300786a0b05f6edf2f6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>currCarsExited</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>215c6d0e26a9cf3bcf517ec3f13c053c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>tickMultiplier</name>
      <anchorfile>classTrafSim_1_1Car.html</anchorfile>
      <anchor>6f369edff722019b3f6f5dae0d1e023e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::CarFactory</name>
    <filename>classTrafSim_1_1CarFactory.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>CarFactory</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>d4e35badbdbf12b145640d038e77a2e4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CarFactory</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>21294d274b14fa5ec3ee406610a85da0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>5bd1558815837aa6818b2d45e83e5f2b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>53cae2f876be5b77f06e60b661c108ff</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>65dd7716fb4c7ce4ed7525f8ef042e48</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRemainingCars</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>14d7721fb45d60aa74d57522d895e1b5</anchor>
      <arglist>(int nCars)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getRemainingCars</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>3c595d8c12a79053585603cbf0958249</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCreationRate</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>4abf8ba2a1a403d3a81a51839c2f5e56</anchor>
      <arglist>(double rate)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getCreationRate</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>6dd0ddde07d9325d6db26a21b31ca343</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCRRandomDelta</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>5c88b54eae0d6c12215d5a1cf3a36a6c</anchor>
      <arglist>(double delta)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getCRRandomDelta</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>d423e7e17b2d5296ff12ba8103bca278</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCarProperties</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>f5fd97e624c7974b1861b5a7bdd24e79</anchor>
      <arglist>(CarProperties const &amp;low, CarProperties const &amp;high)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCarProperties</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>7bc773e1f7a656c9345b88c89ffc2766</anchor>
      <arglist>(CarProperties const &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>std::pair&lt; CarProperties, CarProperties &gt;</type>
      <name>getCarProperties</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>24f88a33243ecd35f7f83cc6f0b3475c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRandomSeed</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>271de363870096968dabc6f0e6e8b4df</anchor>
      <arglist>(unsigned int seed)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getRandomSeed</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>0c5e64b732176e4e11780c7d00612870</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFailMode</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>9b6cad2b342df2533a521fc89a947a6f</anchor>
      <arglist>(CarFactoryFailMode mode)</arglist>
    </member>
    <member kind="function">
      <type>CarFactoryFailMode</type>
      <name>getFailMode</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>84f56d09eedda2d8b1a8330e23366c8f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPath</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>49031db9c70c31b75805f6e8c44f3b26</anchor>
      <arglist>(std::vector&lt; CrossManeuver &gt; const &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPath</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>85623e05f3ec0704d385b495f61b55f1</anchor>
      <arglist>(std::vector&lt; unsigned int &gt; const &amp;path)</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; CrossManeuver &gt; const &amp;</type>
      <name>getPath</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>b3781e022a638586ca18a61931b39b6d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPosition</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>2b3f8071a711d95dc53b2b57d11fa5a7</anchor>
      <arglist>(Road *road, unsigned int laneIndex, float t)</arglist>
    </member>
    <member kind="function">
      <type>Road *</type>
      <name>getRoad</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>167738b29fc273bd7aa0e677589d5851</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getLaneIndex</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>5d686a9d4e128d2bf81ace69dd9d479c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getT</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>9231dbfbaeda2ed3030b24851f1b7564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>28c7278cf9eea001568ccc1833d993cd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>818049ace6977e13534e2b89aefaf8cb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>ec839607da022283b8a85f4ce39032dd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>dc333c14de8f2a8b1ec20929bbd6cf15</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>f80f9ec97a19fdb3b64cd9d665d97775</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>9f2f1b426644da54d9672cbdeed96be6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>1c9b72eb5a905057e19ddbeb4dae3d5c</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static CarFactory *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>dccae27eff9df26f87a821f4143b7c3f</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>eec25663237c076042e0cf646631964a</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>0bd5d573776178f79162b5535897700e</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Road *</type>
      <name>spawnRoad</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>4070920ba4571aeebe3fbafe95c2a488</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>spawnLaneIndex</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>c42a42ee66bbcbae8087b3a2cbc63f6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>spawnT</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>088431ace2009a9f043edaa010463fa2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>position</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>e3326402b21da56949b6ca0900ebae74</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; CrossManeuver &gt;</type>
      <name>carPath</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>52b2d6dab78de05cb58c9db167946d86</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>remainingCars</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>235010abd6220bd96160d2b136659ba0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>rate</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>1e976564a89468c4e2ed9e27685c8292</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>delta</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>7e7e13ca0d94d055e86870c60330f3de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CarProperties</type>
      <name>lowProps</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>dedf83753ebc0da877e3d0f5177c52c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CarProperties</type>
      <name>highProps</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>7550601af824ea816745c5ca8bfda4f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>seed</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>a2e71258c2a9a397328e02590b39434d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>CarFactoryFailMode</type>
      <name>failMode</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>64e05237db90a06705b6850af5fab390</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>timeLeft</name>
      <anchorfile>classTrafSim_1_1CarFactory.html</anchorfile>
      <anchor>afc10f2db05d4051a0851beb2b3e9cd0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::CarProperties</name>
    <filename>classTrafSim_1_1CarProperties.html</filename>
    <member kind="function">
      <type></type>
      <name>CarProperties</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>24054697106a8bb98927143a06fd60cb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>Vector</type>
      <name>size</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>b763dca3c9cb460c19a31272f6c017f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>height</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>5c5e4c2df1a7cdf7980c15e9ca536828</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Color</type>
      <name>color</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>836d452204e411670f916267584c3273</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>comfyAccel</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>281cf8f4d419196f396f5a226b4e6dda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>maxAccel</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>e777c7ee53e9ed059a4a3eaf9e6b1478</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>maxAngularAccel</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>d396946eef627627935683724da6c200</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>safetyTime</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>ba50da3452917618b2074eee9b903312</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>minDistance</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>6fbcb98daae6fb7bec8673b519a78177</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>carWaitFactor</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>6c0abd90166206aa30aee9325b054009</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>gapAcceptFactor</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>800a707835a774de22e3eb20355681fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>accelDelta</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>bc06e9ca2cde70db523ffff21c6edfbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>curveEvalInterval</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>cb70f34a5e00cfcc9c92efc1432ac349</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>b5186d43293cc48a69b433fadfbe4d8f</anchor>
      <arglist>(std::ostream &amp;os, CarProperties const &amp;desc)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>classTrafSim_1_1CarProperties.html</anchorfile>
      <anchor>e9d5700ccb7d03a317f69046e22ebb96</anchor>
      <arglist>(std::istream &amp;is, CarProperties &amp;desc)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Color</name>
    <filename>classTrafSim_1_1Color.html</filename>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>0da7d01200398d98e4f0ae8d8c815622</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>aadbeb37f7ecffa5968823c3a0430e56</anchor>
      <arglist>(float r, float g, float b, float a)</arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>operator+</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>848d2628a21eb676f946426b5e2dce1d</anchor>
      <arglist>(Color const &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>operator-</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>1e67e548581aae5dffc141a6285a4c0c</anchor>
      <arglist>(Color const &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>operator *</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>81454ff4846906116b43a7c5bbe482a5</anchor>
      <arglist>(Color const &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>operator/</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>1cfaa24f2c3b9681e1a61d699bfee2d6</anchor>
      <arglist>(Color const &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>operator *</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>965751d6e7c541432176f8cdba2631ee</anchor>
      <arglist>(float rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>operator/</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>ebee055ad41b3341693944a2dccd7e5d</anchor>
      <arglist>(float rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator+=</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>792b2658a6979af5f365ca1e3168b9f0</anchor>
      <arglist>(Color const &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator-=</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>aa1c49e86bb1389d59f064e9adafa4a2</anchor>
      <arglist>(Color const &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator *=</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>fb1a3f729f008ed7ea48442aeb2615fc</anchor>
      <arglist>(Color const &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator/=</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>3dbf94be255f3106b96f3f23b5ce9ad5</anchor>
      <arglist>(Color const &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator *=</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>d71dc70272aef76784615b237766a768</anchor>
      <arglist>(float rhs)</arglist>
    </member>
    <member kind="function">
      <type>Color &amp;</type>
      <name>operator/=</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>cb7763758817a6c0cffbca1c5ec73421</anchor>
      <arglist>(float rhs)</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>r</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>f68fae462d5e96444a3e4a9014b9ddea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>g</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>ef4a413b83f3f6de74763f4f87088777</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>b</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>454a473b447578ba9ab9bab9363b403d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>a</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>46cc989ce66b877b9ffbf15a0d9f68af</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>c1fe1aac6e17db20e0e6db362d2062b0</anchor>
      <arglist>(std::ostream &amp;os, Color const &amp;c)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>classTrafSim_1_1Color.html</anchorfile>
      <anchor>c0c198f947967956a54619cd344a40e0</anchor>
      <arglist>(std::istream &amp;is, Color &amp;c)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Environment</name>
    <filename>classTrafSim_1_1Environment.html</filename>
    <member kind="function">
      <type></type>
      <name>Environment</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>46007ed0bf477a36f312d9eee9843e03</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Environment</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>4d642aa79ca3ef13a4934e8a7772b3e4</anchor>
      <arglist>(int skynet_key)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Environment</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>8809af2a771d819504babfe71b011f4e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string const &amp;</type>
      <name>addObject</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>c870d534123684575e05b97ae93394a4</anchor>
      <arglist>(Object *object, std::string const &amp;proposedName=&quot;&quot;, bool addBoundary=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>storeIntersectionBoundaries</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>616946560f5c53868ae4e99dc797c59c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>removeObject</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>15b2c0426cba0a70270d5c57479afdde</anchor>
      <arglist>(std::string const &amp;objName, bool canDelete=true)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>renameObject</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>11e518f793b6e4a56ff4db2c8d4a418b</anchor>
      <arglist>(std::string const &amp;oldName, std::string const &amp;newName)</arglist>
    </member>
    <member kind="function">
      <type>Object *</type>
      <name>getObject</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>bc29fb853e9db91b08b2578c3f1315c5</anchor>
      <arglist>(std::string const &amp;objName)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getObjectName</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>9ead03fa9cbec1069563f37fcec9bb79</anchor>
      <arglist>(float x, float y, float distance=20.0f)</arglist>
    </member>
    <member kind="function">
      <type>ObjectMap &amp;</type>
      <name>getObjectsByClassID</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>f17560a2f839e0640721433ae33bc521</anchor>
      <arglist>(std::string const &amp;classID)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearObjectsByClassID</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>1f496af1e1ee9e4a0db6bf2478bdc5d0</anchor>
      <arglist>(std::string const &amp;classID, bool canDelete=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pauseAllCars</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>52edb23df67bef8620d3d4f06ddf88a8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>clearObjects</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>6a0c2afb9834ba8afbf8ef866e9e6c42</anchor>
      <arglist>(bool canDelete=true)</arglist>
    </member>
    <member kind="function">
      <type>Vector const &amp;</type>
      <name>getBottomLeftBound</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>efef50d1e4a62a08a1fe8f6206297c13</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector const &amp;</type>
      <name>getTopRightBound</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>92ffed2c063b4a9dc78349858ae93b48</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBounds</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>de3fe5d95f879fc73c582b672763e225</anchor>
      <arglist>(Vector const &amp;bottomLeft, Vector const &amp;topRight)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>2044e109a8ebfdf498805609c62d0296</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>0c33c96620f5a3558042f9b9d840c2cc</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>toLocal</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>586aa74f1cd071515e2c7dc68bd9394a</anchor>
      <arglist>(point2 p)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>toGlobal</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>d2d1c6ab072c75a47e32ceabfdd92c60</anchor>
      <arglist>(point2 p)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>updateAliceState</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>bdb8b44e4157285ebb99950bd641c40c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>exportEnv</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>5c72a91130b26b6f9d41218acf51cce0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>exportCars</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>7dd6cece02eddcbf49a54c8ffb6da282</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>exportObstacles</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>ece53c75bc28f5678f38d0ff793ebfaf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>exportRoadLines</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>64fbee17c19ff971e8bdf93cd32c41a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>testClass</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>6c93a6e25dd896339f7fd7282554375b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTranslation</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>b63783fcb7670d5ecf431072a6479abc</anchor>
      <arglist>(double xTrans, double yTrans)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>openNewLog</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>f056bfb06e2ce91cf5b3d3e218b9c494</anchor>
      <arglist>(string name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>writeToLog</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>002b5b4e6a081b7b015a6681ef36f1ad</anchor>
      <arglist>(point2 p, string name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>closeLogs</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>bfa7f04340e6bde1595f4e00c984d47b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>skynetKey</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>76eac3d792749d1291350bf9980222fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>xTranslate</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>27aea5290c01a41a1bfc29e092659211</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>yTranslate</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>d79b9350b61aa9c7e9361cc330660b7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>xInit</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>33f4f76b353f7be8a468ff0f2f354d56</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>double</type>
      <name>yInit</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>4e188b4ceb7ab294e22432015bb931bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>debug</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>0db137130ad2babd3607a8d5bfbd5120</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>paused</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>62c57fc59d96b18fc13d1ffa4a72a259</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>trafficPaused</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>d30541aeab8cb51ac1be4da982ab552c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>makingScenario</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>cc7c05f5a3fdabf347d83a28cc655792</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>speedUpTraffic</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>4e39e9d62898ba41248761011cd734e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>slowDownTraffic</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>e8e8a15f4eac3e53763a7925d969ae39</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>resumeTraffic</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>a247c2337c82ce61b6e7895f05b86195</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>subgroup</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>47a2d069a181debc140ef08e7f811dd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>baseId</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>f180f40d1c211a2f2bd8176e879cfe0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>std::string</type>
      <name>_generateName</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>655a211cc5d4c026ffe3d5237f932b41</anchor>
      <arglist>(std::string const &amp;proposedName)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>bottomLeftBound</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>a50c0da89f880e3a3ed0ef553e045ebb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>topRightBound</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>582f443d925daf054377771b9250560d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>EnvironmentMap</type>
      <name>typeDirectory</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>764aa9f9c7869a70e8384f2aa65efd5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>ObjectMap</type>
      <name>nameDirectory</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>46b838ab742187694b245cf0deb8f19c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>NameMap</type>
      <name>nameList</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>5611684b621f9e13928489f86fd66cd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>BoundaryMap</type>
      <name>roadMarkings</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>5e6bbffd69af89dee5b018852d558b6a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>VehicleState</type>
      <name>aliceState</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>f8a7cc29db780be9c3a971c678f4c36d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>currentTime</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>1f83825dd33c8139c6eb4b402aa5cf03</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>map&lt; string, ofstream * &gt;</type>
      <name>openLogs</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>2404bcffff694383d53dddf804bf3d8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>string</type>
      <name>logFileNameBase</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>e979cfea04e81eee31b493e330c5857b</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>bb8ee2514cbd92b8a88861bb2389ba46</anchor>
      <arglist>(std::ostream &amp;os, Environment const &amp;env)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>classTrafSim_1_1Environment.html</anchorfile>
      <anchor>3143b17f5b3b2f969b26dd4379398a3e</anchor>
      <arglist>(std::istream &amp;is, Environment &amp;env)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Event</name>
    <filename>classTrafSim_1_1Event.html</filename>
    <member kind="function">
      <type></type>
      <name>Event</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>7bfcc0b67c8c01fdb1f4b67437e4efed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Event</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>57b362a092cc395e3a5a466704ff2592</anchor>
      <arglist>(string Function, float X, float Y, float Distance=1.0, float Delay=0.0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Event</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>00f5443fb0594e53d93e8dc5a3be3524</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>3e961ac047336948f32a179e0b5edc5f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>74730c27f35d2db3898cc8d463a4c291</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invertColor</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>d42b4292542f6a49e66bea821822ab15</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPositions</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>adfc8837b50ad88be7f56a65b614e059</anchor>
      <arglist>(float X, float Y)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPositions</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>cee9aed2634ead636f8f2520cb83db2b</anchor>
      <arglist>(point2 pos)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>6235ab1d6348477e013ecbb0ca323cc4</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>bba44f7a66fa45015369a6617b0e4777</anchor>
      <arglist>(float X, float Y)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>b205b5307c000463b9dae662c4d129d8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>a8da6f84d0e3a73032f0d41d5b717503</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>20acf7a874105629b51de9ae55b450de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>distanceFrom</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>1d6d7ec7be99f4f92207e458becff17e</anchor>
      <arglist>(float X, float Y)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>activate</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>306c8de1333aa2412f8302cb8f55bf3b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>function</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>3802f17030ced989f29d62fa8ee9188c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>x</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>1e5bc8e4c58eb6c71663f0e48b8b84dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>y</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>1f9ea7d787258acb948e5ee6f2dcc36d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>distance</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>50266b2093458ec191eb6e6c6eaedb91</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>delay</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>ad0575deb1d0c43748dd0928eab8af36</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>time_t</type>
      <name>startTime</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>787fcdddaba1d907a88ebb7cd9a20eb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>waiting</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>bb7677e1f2e6bcf9a6a57f6f0ff84350</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>active</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>d1e50dac925fb844dfb8d2f6243dfcf4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Color</type>
      <name>color</name>
      <anchorfile>classTrafSim_1_1Event.html</anchorfile>
      <anchor>a2260360bd69f9fa7d21a85320021d88</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::ExternalCar</name>
    <filename>classTrafSim_1_1ExternalCar.html</filename>
    <base>TrafSim::CarInterface</base>
    <member kind="function">
      <type></type>
      <name>ExternalCar</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>fdeeec09cc6c54240887350a60c66dc5</anchor>
      <arglist>(int skynetKey, bool debug=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ExternalCar</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>6fb70d5c667b72804a8714561559657a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>4b7439bdcb3415bb4e166a4f92586700</anchor>
      <arglist>(Environment &amp;env, std::string gridName=&quot;&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>ee4f4132b94694f38dbee0b8b18be977</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTranslation</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>a32c8dffb212b874021f04e39f5cfef6</anchor>
      <arglist>(double xTranslate, double yTranslate)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>987e56af92a188e6c04e483c85d57dcf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invertColor</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>e0ca970de2181ce603f1463531634e86</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>d907aeb37acecc7b65023b657ba3e841</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>9d15bb32a99e3cb3324ac93b2cda0f66</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>checkIntersection</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>b3c993c5e64345979c7e054503f1aa0f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getLaneVelocity</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>49e81d4aaa85be655694c9409a7f930b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Lane *</type>
      <name>getCurrentLane</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>2bec31688e7a32a915b348e146115b98</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Object *</type>
      <name>getContainerObject</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>4abd84814967bfc989bcb639c788db00</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setCurrentRoad</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>76fe2a3759debd2646f4504d919b9c23</anchor>
      <arglist>(map&lt; string, Object * &gt; &amp;roads)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLaneTCoord</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>b74715108f15b5a8e2eac5092b4bee8b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setWorldPosition</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>c72cddaa65dafe0e452cf2833b15836e</anchor>
      <arglist>(Vector position)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>getWorldPosition</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>91f4dea35c0da2d184087c453629f550</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVelocity</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>f94455c3c6f88b6a349e539fb08538f4</anchor>
      <arglist>(Vector velVector)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>47bade39e9878f98ce062b6efae38d1f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setAcceleration</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>7112dcb21bb3d3348aeb2f4aba829fc6</anchor>
      <arglist>(Vector accelVector)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>getAcceleration</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>c5e239f22e30adea337a07d6865c6efc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>7dabdd29248342b9bb65de0afa299aa3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getX</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>b9a485f45f6042bca3cbb3563929e6dc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getY</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>38717345698853c5b461f3a3122aaa9f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>131680075087e13f80d9d14a8d84d2e3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>6829c3d7cec0fa2eb4e4bacd32b4d36a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>9398434a179f202652b03ec398b302c3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>db572df7ab2254bbea339f29892bc83e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>1c27052a4e066dc35a7e3ad60a3b7c45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>a21b3b503201f9db111d8d5e29a5deeb</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ExternalCar *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>93bc683f5b2b2bc01ebc7dbba1293c13</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>debug</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>8cbe3d9579be140d17fca3d9b77945bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>261597bb90bd9c0a7108e3291718ae1a</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>b89636cbbf2417465d599de97722c538</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>updateLanePos</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>f0551c1de0e10b4c193e27bb984ee291</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>position</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>f66021a2d7a1d64488bbe85280d01d18</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>velocity</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>afde3ba21c6af626552f9b85e4832aa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>accel</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>d02126efce93984d8ee59c468ade2836</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>orientation</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>7d5797042b6d3c0bb6e3aa94a0551d77</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>xTrans</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>dbd59550d15114a8be96ca4a0e92095c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>yTrans</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>c2f61c1c1054cbc0212ec7f3a22ed841</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Lane *</type>
      <name>lane</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>1eca91bb57ca5b8532839b606d14d17e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>laneT</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>292bbb17e1886161ae72bdd2106fa365</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>laneIndex</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>199007ffc4b57ca20ee5553d1173d387</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>LaneGrid *</type>
      <name>laneGrid</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>6c83b7ed1f18a8219643ef5aaafe8e5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Intersection *</type>
      <name>inter</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>efd297a217db80698234153982b0ca6d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Road *</type>
      <name>currentRoad</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>83dd15a91340ed6bdedaf27377cc30cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>approachingIntersection</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>ce58fa3cf35dac031a055472309d21eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>crossingIntersection</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>3f180aa91158487df8aecfff6394270b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>firstRun</name>
      <anchorfile>classTrafSim_1_1ExternalCar.html</anchorfile>
      <anchor>16fffe40d7347111ab7239793682cd41</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::ManeuverDestMap</name>
    <filename>structTrafSim_1_1ManeuverDestMap.html</filename>
    <member kind="variable">
      <type>RoadLane</type>
      <name>data</name>
      <anchorfile>structTrafSim_1_1ManeuverDestMap.html</anchorfile>
      <anchor>1bcd94603714fc1938e530beefc9a5de</anchor>
      <arglist>[3]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::ManeuverLaneMap</name>
    <filename>structTrafSim_1_1ManeuverLaneMap.html</filename>
    <member kind="variable">
      <type>Lane *</type>
      <name>data</name>
      <anchorfile>structTrafSim_1_1ManeuverLaneMap.html</anchorfile>
      <anchor>3efccb13875b4a454f28224024331eb2</anchor>
      <arglist>[3]</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::Queue</name>
    <filename>structTrafSim_1_1Queue.html</filename>
    <member kind="variable">
      <type>unsigned int</type>
      <name>numCars</name>
      <anchorfile>structTrafSim_1_1Queue.html</anchorfile>
      <anchor>334242cbe9fb4c2adb5fe86e4ac63292</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>carsExited</name>
      <anchorfile>structTrafSim_1_1Queue.html</anchorfile>
      <anchor>cf32b039baee3a22b8770aec1b3cd78e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::IntersectionGeometryParams</name>
    <filename>structTrafSim_1_1IntersectionGeometryParams.html</filename>
    <member kind="variable">
      <type>float</type>
      <name>narrowing</name>
      <anchorfile>structTrafSim_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>64123f984571401a1205af6fde76176a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>sharpness</name>
      <anchorfile>structTrafSim_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>ad5df965917afe875a8c2d6967583e84</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>leftTightening</name>
      <anchorfile>structTrafSim_1_1IntersectionGeometryParams.html</anchorfile>
      <anchor>9fc53f9366814e389c200832cb61b5e6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Intersection</name>
    <filename>classTrafSim_1_1Intersection.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>Intersection</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>826380f1a2c78747a957ba51f55f3d34</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Intersection</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>49d4820ab23cf1459ac2516ea5c609e9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>bfb994ffc69f427d5ebabfceb0295128</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numRoads</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>c9ad4cadece955ae006630f1470a2900</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addSpline</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>58e58c71db04375598c79aec883e91aa</anchor>
      <arglist>(Spline *spline, bool start)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>1889e3b27c6a042d204a9af6c712609f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>hookStart</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>294e64d98d34da043a34d6fdcad05a7f</anchor>
      <arglist>(Road &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>hookEnd</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>6898c2dc8a38188dc53a542a360000e0</anchor>
      <arglist>(Road &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>blockManeuver</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>b3e9d845022f808cf0bf125546f128ae</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>unblockManeuver</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>8981685406eea6f3bc1bde8895c52cd2</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>computeGeometry</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>b80381665bf16f761f504538cacd9081</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual RoadLane</type>
      <name>getDestination</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>3835de8da496b87612bb2e0129bd26de</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Lane *</type>
      <name>getLane</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>82f32089d6b1f9f8a9cc1e6873824b3f</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setStop</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>08ab29ad28053367ea0b5baeb5ea97a2</anchor>
      <arglist>(Road const &amp;road, bool stopSign)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isStop</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>9472757cf6bd8110f46ef985c456e589</anchor>
      <arglist>(Road const &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual RightOfWay</type>
      <name>rightOfWay</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>8a92c96a9ee52a2d3c53d9e1de5d3c68</anchor>
      <arglist>(Car *thisCar, Road const &amp;road, Car **followCar)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>exitIntersection</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>377c6d96a1e60a63c360942ca6e86c80</anchor>
      <arglist>(Car *thisCar)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enterQueue</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>d588d8b38ecce0b9917ac6a6f1f193b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>exitIntersection</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>0036b74a42eb3945885c7bb88a6eb141</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getOpposingTraffic</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>92e71c510d90274dfcd22b9bac9fcc32</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver, std::vector&lt; Lane * &gt; &amp;result)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBoundaries</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>b6685c6312fd3a793773f8adb7649f93</anchor>
      <arglist>(std::vector&lt; Spline * &gt; &amp;boundsArray)</arglist>
    </member>
    <member kind="function">
      <type>IntersectionGeometryParams const &amp;</type>
      <name>getParams</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>0c205dd34cfd686d4f5dfe58ebf2c9e5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setParams</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>1a4002d261c056fc01c6b7d55545cf70</anchor>
      <arglist>(IntersectionGeometryParams const &amp;newParams)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Intersection *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>999cee4c176180a9d82ef2c9be56eaaa</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; Spline * &gt;</type>
      <name>endSplines</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>285f0f1d2e5ba5a66672582a91ffc6e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; Spline * &gt;</type>
      <name>startSplines</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>927738992507592b443754b697bd7d8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Queue</type>
      <name>currentQueue</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>9af28080b64d5fd9591b165774c37845</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>61204d6d97b939990d74f0c69c08285e</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>b9002a99c5ef453b912c9e028d3be9ea</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>IntersectionGeometryParams</type>
      <name>params</name>
      <anchorfile>classTrafSim_1_1Intersection.html</anchorfile>
      <anchor>2456fd1e44f6f86322b925e4065d67f3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::DeadEnd</name>
    <filename>classTrafSim_1_1DeadEnd.html</filename>
    <base>TrafSim::Intersection</base>
    <member kind="function">
      <type></type>
      <name>DeadEnd</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>8cd61f1fb68bfee5883ecec9aacff6bc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DeadEnd</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>526327c55bc3517f66029a709e51b37c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>77fb40b72b1387cc8d5adcf7aeb0d7ec</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>b19dc0877d62bccf0c14a3fdbf55360e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>e7dc5911e1700b09852b780557ad784d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>7aeabb60adc67cd00e8f6a92600503e9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>bb58c08b76ee1516eb9f4538c7d14f1c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>dcd3e31856b41d2192a5ab312744fc52</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>558bf864b1a6f49266e58d8be241e857</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1DeadEnd.html</anchorfile>
      <anchor>0ad8a3fbfc9488e510b6b8730ddfb58b</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::LaneInterferenceDesc</name>
    <filename>structTrafSim_1_1LaneInterferenceDesc.html</filename>
    <member kind="function">
      <type></type>
      <name>LaneInterferenceDesc</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>9b8e4ed0e9673e14f35f027a5e889da2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LaneInterferenceDesc</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>e5712db5723ee60d528418e221fdbae3</anchor>
      <arglist>(LaneSegment const &amp;_mySeg, LaneSegment const &amp;_otherSeg, float _lengthRatio)</arglist>
    </member>
    <member kind="variable">
      <type>LaneSegment</type>
      <name>mySeg</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>100f9e0d9260f394ddd73e0ef01ffa9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>LaneSegment</type>
      <name>otherSeg</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>b79c556f0e5337d42180f7efea9ab32c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>lengthRatio</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>3faf27102d4500c30a1a6dbb9cc31147</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>c32d8aafc7251dd400360606886eb566</anchor>
      <arglist>(std::ostream &amp;os, LaneInterferenceDesc const &amp;desc)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>structTrafSim_1_1LaneInterferenceDesc.html</anchorfile>
      <anchor>72bfcb95e96e52312b42d1a9b4706d4a</anchor>
      <arglist>(std::istream &amp;is, LaneInterferenceDesc &amp;desc)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Lane</name>
    <filename>classTrafSim_1_1Lane.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>Lane</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>6aa0f0cb50d2c18aa728d60a5591abab</anchor>
      <arglist>(float lane_width=5.0f)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Lane</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>b427da93c041e11890e46378ca917e15</anchor>
      <arglist>(Spline const &amp;spline, float lane_width=5.0f)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Lane</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>256e1d467452b1c041be9a79d44dd340</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>b4eedcac81b5051fc259da99b6fa726c</anchor>
      <arglist>(Spline const &amp;spline)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>427d34d7b5b7bd6c365c19a306884f88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>4a535f5d94b57cc18caf336f50a2e268</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>549838567c7cbc18ce7e6258f1f62a11</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>addObject</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>0c3cdcb583cd062923c8bbb19d61b489</anchor>
      <arglist>(Object *object, float t=0.0f)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>removeObject</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>81119c89364ba73d9d4e14d0c86e79ea</anchor>
      <arglist>(Object *object)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setT</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>04523f08faf9669d859bb08562f9f750</anchor>
      <arglist>(Object *object, float newT)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getT</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>92431ec8928ac2cc9ac31aaafe0dc9da</anchor>
      <arglist>(Object *object)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>mapT</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>21f5d0ebf174eb63bb833aef5512b41c</anchor>
      <arglist>(Lane *otherLane, float t)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>mapTangent</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>533b099b3bf8f4094d10b579ec60029b</anchor>
      <arglist>(Lane *otherLane, float t, float mappedT)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>addInterference</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>e5e1adf4308dd609541fe698a8723229</anchor>
      <arglist>(Lane *otherLane, float threshold, unsigned int subResolution=10)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual LaneInterferenceDesc *</type>
      <name>getInterferenceAfter</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>d902d10ee79050deb16c1c1709a93190</anchor>
      <arglist>(Lane *otherLane, float t)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>interferesWith</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>980b086b4214253f49496ad8d082ef42</anchor>
      <arglist>(Lane *otherLane)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>contains</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>ab5bf076d4ba39f48b51c261defe4384</anchor>
      <arglist>(Lane *otherLane)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::pair&lt; float, Object * &gt;</type>
      <name>getObjectBefore</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>097a2d9be89df6892a48cc59dee26ead</anchor>
      <arglist>(float t, std::string const &amp;classID=&quot;object&quot;, bool interference=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::pair&lt; float, Object * &gt;</type>
      <name>getObjectAfter</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>7c3bb24af3f472752b5288464f330076</anchor>
      <arglist>(float t, std::string const &amp;classID=&quot;object&quot;, bool interference=true)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>707f72517f9c93ec06efca2bc38e87c9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>90305043794b77d3df15a36509ef24f9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>047336122514bc7d42234e82acbd4ab3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>86110a953bbafd48e9ab6e6a2c3821c2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>2689e5bfa5a2b96e553ab5fd8b8031fb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>7b63ef06c8bc5d2b8eb7f8fa1605595c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>7e5689df7926e97b26b51bf9cbe71570</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Lane *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>aafcce546194fc75b84cdd63d9333203</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="variable">
      <type>Spline *</type>
      <name>spline</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>4b6b38c3a04f94dd0b32125c10154196</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>laneWidth</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>a0f6dda80a947851b560cfb12e642fd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>inIntersection</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>c93edce26d28744eea34ca3e5c78cf24</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual float</type>
      <name>auxMapT</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>386830ebb3335ca6c57b2000e625ea29</anchor>
      <arglist>(Lane *otherLane, float t)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual float</type>
      <name>mapInterferenceT</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>bd71a26457304c0f916c851fc1c52675</anchor>
      <arglist>(LaneInterferenceDesc const &amp;idesc, float t)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>74ae8ad2c869b60c605f2232ee5726b1</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>dcf5946f4de905c23cd67eda0362d809</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::map&lt; Object *, float &gt;</type>
      <name>objToT</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>1a8ff4fa915c71031caa739a33d19432</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::multimap&lt; float, Object * &gt;</type>
      <name>tToObj</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>e6c5aedcea6658f5ffb7fe8e7a956228</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::multimap&lt; float, LaneInterferenceDesc &gt;</type>
      <name>interferences</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>ab9fca865597d90e25f12b83547b30bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::set&lt; Lane * &gt;</type>
      <name>interferingLanes</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>6e12c0b816b46fba05c3d7be794e6adc</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend class</type>
      <name>MultiLane</name>
      <anchorfile>classTrafSim_1_1Lane.html</anchorfile>
      <anchor>3207f4ca8b41612707f227b42b56bd1a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::MultiLane</name>
    <filename>classTrafSim_1_1MultiLane.html</filename>
    <base>TrafSim::Lane</base>
    <member kind="function">
      <type></type>
      <name>MultiLane</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>6ebcab35a229be4d403073742723ed2f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>MultiLane</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>3327293928d26cb7d5d069f2fad5f929</anchor>
      <arglist>(std::vector&lt; LaneSegment &gt; const &amp;subLanes)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~MultiLane</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>1a8e4a0e17726159f79078bb27b8493f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>f05351deefc73620155b225c501cf2bc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>3e4900625a5360ee4d6d1ebbeff75a3f</anchor>
      <arglist>(std::vector&lt; LaneSegment &gt; const &amp;subLanes)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>05d7ced95e851ab7388ec8ca47321917</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>7391793ed2c98d61c1c6974dd2a4195c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>addObject</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>20f61594cc90859d57ff0d77b2d94422</anchor>
      <arglist>(Object *object, float t=0.0f)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>removeObject</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>ba43acf146c5e54d36b33af1607e2b02</anchor>
      <arglist>(Object *object)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setT</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>be165582605c73759ca2528a2bc64e35</anchor>
      <arglist>(Object *object, float newT)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getT</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>f2b515c873eabfd73f0b916cbf86ea4d</anchor>
      <arglist>(Object *object)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>mapT</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>f9cecf9f408c42c89b29938789b4d12e</anchor>
      <arglist>(Lane *otherLane, float t)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>mapT</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>089dd779bd4e0dc11cc6ef337de6b562</anchor>
      <arglist>(MultiLane *otherLane, float t)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>addInterference</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>7743cc059b4d3b1b6d9a99eb0dabf4b4</anchor>
      <arglist>(Lane *otherLane, float threshold, unsigned int subResolution=10)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual LaneInterferenceDesc *</type>
      <name>getInterferenceAfter</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>b45673aba280403ec0b187a43c2cdd8a</anchor>
      <arglist>(Lane *otherLane, float t)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>interferesWith</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>572902f69ec5e0cae74e4332780a4b99</anchor>
      <arglist>(Lane *otherLane)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>contains</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>83f4888d5239ab8e91e51f84c15d5070</anchor>
      <arglist>(Lane *otherLane)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::pair&lt; float, Object * &gt;</type>
      <name>getObjectBefore</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>80de05c5b484be9cdea6b3d82b31c3e1</anchor>
      <arglist>(float t, std::string const &amp;classID=&quot;object&quot;, bool interference=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::pair&lt; float, Object * &gt;</type>
      <name>getObjectAfter</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>71d31ddd6ef05c13d3ef11e5adc8b2af</anchor>
      <arglist>(float t, std::string const &amp;classID=&quot;object&quot;, bool interference=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static MultiLane *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>e7e39daf800bd28afeadff4c6dd2afa3</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual float</type>
      <name>auxMapT</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>6b5e41c1960b1cac2e8610746da0f99e</anchor>
      <arglist>(Lane *otherLane, float t)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>std::pair&lt; unsigned int, float &gt;</type>
      <name>mapToSubLane</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>630c0e4b7e31797b102d364794761ffd</anchor>
      <arglist>(float t)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>float</type>
      <name>mapFromSubLane</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>f0d24db6ccc1bceb785e981030f33840</anchor>
      <arglist>(unsigned int index, float t)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>1cb4e1a25d757f486c1d4485ba76c043</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>389c285c15d994e9a1dd31307bf65341</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>float</type>
      <name>totalSize</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>242f9e2c1a17756690d621667bcaafa6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; LaneSegment &gt;</type>
      <name>subLanes</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>2bfcf633d22f01014a3b514cb333298d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; unsigned int &gt;</type>
      <name>chunks</name>
      <anchorfile>classTrafSim_1_1MultiLane.html</anchorfile>
      <anchor>4abc3f5dbef18da44364ba719b1ac2fe</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::LaneGrid</name>
    <filename>classTrafSim_1_1LaneGrid.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type>Lane *</type>
      <name>getLane</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>7c08a21267b7a4de6c7361836f073d49</anchor>
      <arglist>(Vector pos)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getPosition</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>2bee3077cd43612d043e404b4aef10fb</anchor>
      <arglist>(Vector pos)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LaneGrid</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>6dc00fefcd3012994726ea3ba4f7cfc0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LaneGrid</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>cd9c21cb1eec1b1697b8c43e6bca0281</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>partition</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>3131f4e80682d787fa492131e0e65336</anchor>
      <arglist>(Environment &amp;env, float tileSize, Vector lowerLeft, Vector topRight)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLanePosition</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>cec522efb38e2c364326395a0ba1b8c8</anchor>
      <arglist>(Vector worldPos, Lane **laneOut)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>a404c6da42e1f5acfd6f4f50b81aa269</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>2ea42df0c4d729ed32b91b5f9f77b087</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>dbea51efbfe6b86011fd8f1e25e4fc57</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>33139e9783e7f27d3100a612834634e2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>637dde05b1bbcd23d8c4983fecc1cec7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>ba9b931a7f5aae23a2866e7107142a2a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>36e8884fe659d1ff55b6a20b2af32f4f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>5066a87aa95c8a94ebedc7850a5c1bd6</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>ee692852f90c139f8e360cba4599e86a</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>6aff04a9f480326c60366a4eb71f0e9e</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>mapLane</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>beb64df868bac7cf76bb5a297c6a18c7</anchor>
      <arglist>(Lane *lane)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; LaneSegment &gt; *</type>
      <name>grid</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>7555e00d7483af9526df11c0119ae5b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>nTilesX</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>186b7e7cd93ed0936199f24f2aa9ae24</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>nTilesY</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>970c118e68fa2685736241fb33366f37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>lowerLeft</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>28250ce838fa65d147363afcf49d7c5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>topRight</name>
      <anchorfile>classTrafSim_1_1LaneGrid.html</anchorfile>
      <anchor>51d698508a3e0857f744d195c9bba078</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Line</name>
    <filename>classTrafSim_1_1Line.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>Line</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>13cb16c330d1dddf6050bcfb184ea2d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Line</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>ee0c475c9ae6efb8f8610739d197b084</anchor>
      <arglist>(point2 p1, point2 p2)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Line</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>e54aa2dbb90d5d5f5629289c72a24965</anchor>
      <arglist>(vector&lt; point2 &gt; points)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Line</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>31c599d76ea4000fffec5a67af0be8bf</anchor>
      <arglist>(vector&lt; Vector &gt; points)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Line</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>a25106d7e7a1aca31bb166eca8916897</anchor>
      <arglist>(Spline s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Line</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>68426d75b7f599b37ba4e842df2a543f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>44c7e2d308b9254b4313f7a57ea56587</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>75ff898d8e300a338f7ed639ef7490a9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColor</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>505f2c55e237bcacc7261e7b1b21a9d9</anchor>
      <arglist>(float r, float g, float b, float a)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>70c987bbbc392026d83cc618db0f11a0</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>6b66a3c5587db0232873814f8f010dca</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>501326c28eb63565dcf36e6956299c3e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>8a64a1064082af39f7f2a6048dcc4c9f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>e73a2e22a6388a035ec9df75b798ee70</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>8260d832ea047ab307608453acdbaee2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>5e4e089828764de4d4555a21f275bbc4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>29b9bc0f0210b15cce9a5370d51f5404</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Line *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>a7d983e914839b9efa2edd4a2dae0fd7</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>c38687186b69dd4ebf001c7e68d2341d</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>668fd4242386e9672e5316237976ba61</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>float</type>
      <name>width</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>aa77370d9391fe56347f6691f61b7121</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Color</type>
      <name>color</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>e12a29a164eccb1594e4c80d76ba7ebe</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Spline</type>
      <name>spline</name>
      <anchorfile>classTrafSim_1_1Line.html</anchorfile>
      <anchor>a39c7d5697ab6bb262cabe3c290eeb86</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::LinearSegmentDesc</name>
    <filename>structTrafSim_1_1LinearSegmentDesc.html</filename>
    <templarg>SegmentType</templarg>
    <member kind="function">
      <type></type>
      <name>LinearSegmentDesc</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>ea69b9636dbdc61bd22b31d3f43730eb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LinearSegmentDesc</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>5bdbbbb64027dad9eb90b24db1a5c8d8</anchor>
      <arglist>(SegmentType *_segment, float _t_start, float _t_end)</arglist>
    </member>
    <member kind="variable">
      <type>SegmentType *</type>
      <name>segment</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>fb54a48edb4532d5ca953ab23eaeecdb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>segmentName</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>507733d993441bd6472dd018a28e3ab1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>t_start</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>5b296c5b302a787eb6a8c7bc1696b717</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>t_end</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>e78ddcb53a0c776d7265634d3374583a</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>28aba9206f4400b4757bc5d9f0fbe89c</anchor>
      <arglist>(std::ostream &amp;os, LinearSegmentDesc&lt; SegmentType &gt; const &amp;desc)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>structTrafSim_1_1LinearSegmentDesc.html</anchorfile>
      <anchor>7db57aac947e5c92a9d2d9652c80caeb</anchor>
      <arglist>(std::istream &amp;is, LinearSegmentDesc&lt; SegmentType &gt; &amp;desc)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Object</name>
    <filename>classTrafSim_1_1Object.html</filename>
    <member kind="function">
      <type></type>
      <name>Object</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>cfb3f58e3c0a250120a9767c1859aae4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Object</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>dfb2d9367b2f3c88866bbea3251d553a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>removeSelf</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>3d4d867951d039b176be5b94295dec54</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string const &amp;</type>
      <name>getName</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>c5550631b9772b315eb14d805f9ce999</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>46e3f69bb50cc08db041a31dac19a9c2</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function">
      <type>std::string const &amp;</type>
      <name>getParent</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>dbbab7c06e3a082225afd70601496a35</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setParent</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>de5114c86a1e5f211008c3928f5c6a73</anchor>
      <arglist>(std::string const &amp;parentName)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>f21c72e469cbc71d33facba0c6fb0b7f</anchor>
      <arglist>(Object *rhs) const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>d2a88c2c45ebe900a06a5a1abbc0eda3</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>847c5023eb504997d1f312ac1e6d0ddb</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>b2cda53120875d4262f36a07733f46b0</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>3139579d528633942b8520d9b96ac52b</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>fa35752910195d2f8ddf915ce99f6372</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>609d5bcfa177fe6c309079e0a59947a5</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>33a9af2a467f701b5ddf1d8e1286744a</anchor>
      <arglist>(Object *obj)=0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>872693a9765770f9751e517393be4495</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invertColor</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>119cb8856571594dadd4a1ae22632908</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>803e4e5f7da3766fa0f4f643267b8dc5</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setCurrentRoad</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>c671b61cba8e6a7351a0c9623e291b0b</anchor>
      <arglist>(map&lt; string, Object * &gt; &amp;roads)</arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>paused</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>dc3aa4f5867fc77a102626a7776c1e27</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>speedingUp</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>5bab3b2ccb509777fa8e3fa3a64dce8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>slowingDown</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>df5a42e71799699932723ffb8a06a012</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>reversed</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>8f23127423634286d1a7f23912db4bd3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>c991291b1b53d206a764969933fe3211</anchor>
      <arglist>(std::ostream &amp;os) const =0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>fee8c01e26369343c1999aa1507e0292</anchor>
      <arglist>(std::istream &amp;is)=0</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_addReference</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>5a9a36bd4cb0be81854763bda9defdab</anchor>
      <arglist>(std::string const &amp;name, Object **object)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>name</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>8a614a66d8b2f22f6a34b18b56579180</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>parent</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>d20a4070e8e9e35612f86d8316438f4a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>_resolveDependencies</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>b659655c843d13d8f0fb6b4c24e92a54</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" protection="private" virtualness="virtual">
      <type>virtual void</type>
      <name>_postResolutionCallback</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>be0a80ae5de038d5dae3657d44e7981a</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" protection="private" static="yes">
      <type>static Object *</type>
      <name>_classFactory</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>7111f274f8e0362d51541fce03ca480c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private" static="yes">
      <type>static void</type>
      <name>_registerObjects</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>ccfb8dd9f9c09f64a9cc44cbeceef17c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>DependencyMap</type>
      <name>dependencyMap</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>d1f5847f391a52585295e5d72bf5db4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static DispatchMap</type>
      <name>dispatchMap</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>99792153c3780e396b75d8f24f12fd97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private" static="yes">
      <type>static bool</type>
      <name>_registered</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>fe82f805b7e71f72a5da5140372ad03c</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend class</type>
      <name>Environment</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>d07f4de926e4e68b49b17ab4d13369d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>233629002f21588035f9564290a12e2e</anchor>
      <arglist>(std::ostream &amp;os, Object *obj)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>8c277cac99246a10ab9338c0c0613e67</anchor>
      <arglist>(std::istream &amp;is, Object *&amp;obj)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>bb8ee2514cbd92b8a88861bb2389ba46</anchor>
      <arglist>(std::ostream &amp;os, Environment const &amp;env)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>classTrafSim_1_1Object.html</anchorfile>
      <anchor>3143b17f5b3b2f969b26dd4379398a3e</anchor>
      <arglist>(std::istream &amp;is, Environment &amp;env)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Obstacle</name>
    <filename>classTrafSim_1_1Obstacle.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>Obstacle</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>6792e58f8c7c25e041795e0d268625f3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Obstacle</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>76f4633ae4393facccffd42ea96da2d0</anchor>
      <arglist>(float x, float y, float l, float w, float o=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Obstacle</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>e92b04e1db1ab47619f42087eccfbfaf</anchor>
      <arglist>(float x, float y, float radius)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Obstacle</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>8fcaa29a11a8c6c9ff765f61068a358b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>e850f4d0d168062b640de54e2f682a52</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>54a589a35cfb09e3bc459bbd8e367c7e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invertColor</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>439ccd50bd5cd0250a2e2e689a78f126</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPosition</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>62d222585db4b31b82f917c17635c2b6</anchor>
      <arglist>(float x, float y)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPosition</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>abd2b93119e9e21bad2fa9a2785b6b78</anchor>
      <arglist>(point2 pos)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setOrientation</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>f5661fba8fcec345de2e657d6e6b8b6a</anchor>
      <arglist>(float newOrientation)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBlockObs</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>7a48bbe164e36a416d43e16855ce6a01</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCircleObs</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>26ad8252e750d82fb9e463ea15bea7e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>e3a83cb2a703e5c2355beb36405167ef</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>7a5bee9b8a88d54ef71ec37940d9e399</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>7b07bf1a31d0a934a6f007f347484993</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>5d8c56527f551622d3e998fffc10484a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>921ee26a27971fbcb0a2bf05ea8e3db5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>ac0be032668bb56ea8aa925b0a19d49e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>00bfbf66a73b4e0cf21d82a05755f306</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>8182cdbdcfad6d7588e46e3f0033fa0e</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Obstacle *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>615dd3733dfb23f9c643bc4bedfb95b2</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>256ff698ba1d5f2708471db133b468f9</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>5a5109982b0e863283d52d986b7d9e9a</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>float</type>
      <name>orientation</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>69d3772d316c64dcaf09964daf025ee5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>point2</type>
      <name>position</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>a6df671aa4f8639e3ff8ce6abaf9b3c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>float</type>
      <name>length</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>40885e9d44c4b1b05fed750a045f2270</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>float</type>
      <name>width</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>f1dd1d1bc4ea875eba65fe0fb73a7843</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>circleObs</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>93dfbc8a6cbba71a964d306cf2fc7bb9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Color</type>
      <name>color</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>b6cb680ba33cc4b3acb2bb1bb00fc527</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>vector&lt; point2 &gt;</type>
      <name>boundary</name>
      <anchorfile>classTrafSim_1_1Obstacle.html</anchorfile>
      <anchor>24af9075f2c3f7122afb7dab0ff4c4e6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::SegInfo</name>
    <filename>classTrafSim_1_1SegInfo.html</filename>
    <member kind="function">
      <type>void</type>
      <name>setLaneInfo</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>90105b00ff82ab0060014923466f8672</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>vector&lt; Vector &gt;</type>
      <name>getPointsFromLane</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>fea3559e5ea5161e7db38e0b07d0e806</anchor>
      <arglist>(int lane)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setCenterLine</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>188e13af45a7d69824dc04359665dfbc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>vector&lt; Vector &gt;</type>
      <name>getCenterLane</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>30e99d6390b83c60d425a25b5dc80060</anchor>
      <arglist>(bool forward)</arglist>
    </member>
    <member kind="variable">
      <type>Segment *</type>
      <name>seg</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>f84f03b7cc81395633caf248b6392c2a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; Lane * &gt;</type>
      <name>rawLanes</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>4a561f9690c60ace2ee04bd8bb1401a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; vector&lt; Vector &gt; &gt;</type>
      <name>forwardLanes</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>b76a928349a58c28044884ebfe4d2aae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; vector&lt; Vector &gt; &gt;</type>
      <name>reverseLanes</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>1a79bb9944fcf7035d591fdbdc05dfc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Vector</type>
      <name>laneInfo</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>4fc7555bb819b71dbc22bdd7852d47d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; Vector &gt;</type>
      <name>centerLine</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>f56a562e157f13d6dd6f998ed2cd6403</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; IntersectionInfo * &gt;</type>
      <name>intersections</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>1a8d590bd1be90454e7c85555e937c2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>laneWidth</name>
      <anchorfile>classTrafSim_1_1SegInfo.html</anchorfile>
      <anchor>d2674f30ec302689469bd7e5b2460ab3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::IntersectionInfo</name>
    <filename>classTrafSim_1_1IntersectionInfo.html</filename>
    <member kind="variable">
      <type>Vector</type>
      <name>intersectLoc</name>
      <anchorfile>classTrafSim_1_1IntersectionInfo.html</anchorfile>
      <anchor>4d8ac8bbf6fb6109ae78b23983c32fe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>vector&lt; SegInfo * &gt;</type>
      <name>relevantSegs</name>
      <anchorfile>classTrafSim_1_1IntersectionInfo.html</anchorfile>
      <anchor>604fd6557295dc4ea82f6052785af597</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::RNDFSimIF</name>
    <filename>classTrafSim_1_1RNDFSimIF.html</filename>
    <member kind="function">
      <type></type>
      <name>RNDFSimIF</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>726d0077a5ab2b9a8a60bf64008d1102</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~RNDFSimIF</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>09410572f4575367a97b48d2616372fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>loadFile</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>ba462ca33b8d63ce9b792ed3cb197326</anchor>
      <arglist>(char *filename)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumSegs</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>0916ad13e91557bdd2de3e48a02b7c6f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumLanes</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>de8d8de9a6ef2c9be367ae23b6e9b015</anchor>
      <arglist>(int segment)</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; Vector &gt;</type>
      <name>getLaneInfos</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>02c0d57ebee08f08cdaebd73d600ccec</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getxTranslate</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>481ec7ef7f94ba79639a09eb174eacb4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getyTranslate</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>e48244e10d551b4520470f075c9a66cb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; Vector &gt;</type>
      <name>getCenterLineOfSeg</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>a5d2dfe58d3fe827dbef57959eaca387</anchor>
      <arglist>(int seg)</arglist>
    </member>
    <member kind="function">
      <type>vector&lt; Vector &gt;</type>
      <name>getLineFromZone</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>dfc08059a68b68809a00e98c7ecd2992</anchor>
      <arglist>(int line)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>buildSegInfos</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>2cf1968ee9eec89bcf343c2794036873</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>centerAllWaypoints</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>34c24ad9a077d076173e8cda2721a047</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>appendStartEnd</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>bb2c1e896273f408c521e7106d76141c</anchor>
      <arglist>(std::vector&lt; Vector &gt; *points, int flags=APPEND_START|APPEND_END)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>numSegs</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>c3f373cbf073548735198ae0814985c6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNumZoneLines</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>7f3a558ca2f2efc4206491aab40fb23d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>buildZoneInfos</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>dbda77d270351aa74ef1c569fb141fe2</anchor>
      <arglist>(double xTrans=0, double yTrans=0, bool useTrans=false)</arglist>
    </member>
    <member kind="variable">
      <type>RNDF *</type>
      <name>rndf</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>7a428669a885649e56122e7516bc1819</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>bool</type>
      <name>warn</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>8a741b6bed2a3c7593dbe57f5917a720</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>loaded</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>6c81d7122982398bcda702e459502a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>xTranslate</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>7bb85ffde333020dc9bd8f69b29e8a4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>yTranslate</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>63c329e1aba4feb3694b0564d562dfb8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; std::vector&lt; Vector &gt; &gt;</type>
      <name>centerLines</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>6f0769cfa6c12e61062e7a7fdee8bbd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>vector&lt; SegInfo &gt;</type>
      <name>allSegs</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>9ef5f67b06cfafba2ae69b57176da7c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>vector&lt; IntersectionInfo &gt;</type>
      <name>intersections</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>dea3a2a1e1d7784d036eb4f3278ec546</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>vector&lt; vector&lt; Vector &gt; &gt;</type>
      <name>zoneBoundaries</name>
      <anchorfile>classTrafSim_1_1RNDFSimIF.html</anchorfile>
      <anchor>90e6d054e4781a82a05a67425156ca90</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Road</name>
    <filename>classTrafSim_1_1Road.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>Road</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>3972fb14fea03980a0d9c184b9e8a214</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Road</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>c08c7bf96cfa8ce8013e43e547bfac3c</anchor>
      <arglist>(Environment &amp;env, Spline &amp;source, unsigned int numForwardLanes, unsigned int numReverseLanes, float laneWidth=5.0f)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Road</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>b8d703ba12d57f552812a887662ef97a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>7170c11ac135f1e36b3a70906e463926</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>1e9e95f9f1da8296aeabaec7429463b7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>dfdc9066e5065f19b759cbcd5b043a8a</anchor>
      <arglist>(Environment &amp;env, Spline &amp;source, unsigned int numForwardLanes, unsigned int numReverseLanes, float laneWidth=5.0f)</arglist>
    </member>
    <member kind="function">
      <type>Lane *</type>
      <name>getLane</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>a3f84a99cd3f40d79e306b121d13697d</anchor>
      <arglist>(unsigned int index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getLaneIndex</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>dc463332e61bde018d25a35b32fe3e53</anchor>
      <arglist>(Lane *lane)</arglist>
    </member>
    <member kind="function">
      <type>Lane *</type>
      <name>getForwardLane</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>eaa17381d0e389fcc29bb1a0061830e4</anchor>
      <arglist>(unsigned int index)</arglist>
    </member>
    <member kind="function">
      <type>Lane *</type>
      <name>getReverseLane</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>cc5c889ca080b31d259bb60d3930116f</anchor>
      <arglist>(unsigned int index)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>numLanes</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>66278d057c879d84b944e7135ba7c119</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>numForwardLanes</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>7f8252151887d6c6264df0ca4ae11fcb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>numReverseLanes</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>08353392ab89d609046c8f3a606b9d7a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isReverse</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>c151adb0d688f20b66ebfa38efe97b8f</anchor>
      <arglist>(unsigned int lane) const </arglist>
    </member>
    <member kind="function">
      <type>Spline &amp;</type>
      <name>getSpline</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>d7dcfac8ae27e3cf1bad6fb4da4659f8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Intersection *</type>
      <name>getStart</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>12f659ac30af0cb19fbc3847c2152dd6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Intersection *</type>
      <name>getEnd</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>cdac10ae4b0b200f1d0a2cc59f553886</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStart</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>b75d01fe647cff6ee27f095459b7a934</anchor>
      <arglist>(Intersection *newStart)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setEnd</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>61f1668f3f68db4b37226ec1bbb11053</anchor>
      <arglist>(Intersection *newEnd)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLaneWidth</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>b1c24f3c7823634a3e59f0e69a8aa42b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getSpeedLimit</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>9502bf63664e0e2bce88e99d667bb4e6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setSpeedLimit</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>3f1a24a38d0a1e78a0418f2e26c6baa4</anchor>
      <arglist>(float limit)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>caaf3c718b636e3a69d9020b01c27c57</anchor>
      <arglist>(float t, int side, bool reversed=false)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>getLaneBoundary</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>9f0506ba84efee323930f09e2dc5e06b</anchor>
      <arglist>(float t, unsigned int laneNumber)</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; point2 &gt;</type>
      <name>getRoadBoundary</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>fd8419fa7d057ffddc5e58f81239225f</anchor>
      <arglist>(int side, double spacing, bool reversed=false)</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::vector&lt; point2 &gt; &gt;</type>
      <name>getLaneBoundaries</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>7a568b573e51b2ae4a3141ebf898f2d4</anchor>
      <arglist>(double spacing)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>acbe588f65cbdc36658f8349fd096f5f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>95bcc624b4ed23c2d0f25a336d7813c0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>a70e5b918ef976a17807a850b1e3490a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>407413eadbb8ed83818ce81113d0ff40</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>f0259551cca72d17b1bba68cf89ba32b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>1fbc5842d0d09dfcef773ad97e0fd668</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>d0894fab8871536823ac1f575da3631a</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Road *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>7b913be9f01120a92e0fb5ae590557ea</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>6f2f8780572241c11a627e8b7b436e0d</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>bb8054a7418752d89e52e9ec338cc2c8</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Spline</type>
      <name>source</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>4febc91c8bcf1a5e1a86dd0f05c00b9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>std::vector&lt; Lane * &gt;</type>
      <name>lanes</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>1df78a493c1e0173021c0d3847ad206e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>laneWidth</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>951c98be9b2817cb6f8fd62bd627191f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>nFwdLanes</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>3fc330813bc812d2d69390f7654c694a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>nBackLanes</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>6b8ce3703bf6b7f2315b78426e8e723c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Intersection *</type>
      <name>start</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>a2e6d1620de88b8df1c534c58a8ac8da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Intersection *</type>
      <name>end</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>b03bf20f6ddcdc4cc1253128324f5fef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>DeadEnd *</type>
      <name>deadEnd</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>9828ddd8a7b7a1d96e5dd4b62c9929f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>initialized</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>d781899709f3077a6ad41f4af60b5eee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>speedLimit</name>
      <anchorfile>classTrafSim_1_1Road.html</anchorfile>
      <anchor>2884e871ecce093d8d2a567fd855b961</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::ParseError</name>
    <filename>classTrafSim_1_1ParseError.html</filename>
    <member kind="function">
      <type></type>
      <name>ParseError</name>
      <anchorfile>classTrafSim_1_1ParseError.html</anchorfile>
      <anchor>0807753973c9820d07ae9e13a408a271</anchor>
      <arglist>(std::string info)</arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>info</name>
      <anchorfile>classTrafSim_1_1ParseError.html</anchorfile>
      <anchor>d24eebc213207be126131e8e710d6864</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::enable_if_c</name>
    <filename>structTrafSim_1_1enable__if__c.html</filename>
    <templarg>B</templarg>
    <templarg>T</templarg>
    <member kind="typedef">
      <type>T</type>
      <name>type</name>
      <anchorfile>structTrafSim_1_1enable__if__c.html</anchorfile>
      <anchor>f734bc15e44e1c7134e71f1a8cb2745f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::enable_if_c&lt; false, T &gt;</name>
    <filename>structTrafSim_1_1enable__if__c_3_01false_00_01T_01_4.html</filename>
    <templarg>T</templarg>
  </compound>
  <compound kind="struct">
    <name>TrafSim::enable_if</name>
    <filename>structTrafSim_1_1enable__if.html</filename>
    <templarg>Cond</templarg>
    <templarg>T</templarg>
    <base>TrafSim::enable_if_c&lt; Cond::value, T &gt;</base>
  </compound>
  <compound kind="struct">
    <name>TrafSim::non_string_iterable</name>
    <filename>structTrafSim_1_1non__string__iterable.html</filename>
    <templarg>T</templarg>
    <member kind="function" static="yes">
      <type>static long</type>
      <name>test_iterator</name>
      <anchorfile>structTrafSim_1_1non__string__iterable.html</anchorfile>
      <anchor>6aa243321879a371fe62e543d8d40b7e</anchor>
      <arglist>(U *, typename U::iterator *iter=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static short</type>
      <name>test_iterator</name>
      <anchorfile>structTrafSim_1_1non__string__iterable.html</anchorfile>
      <anchor>a42d2f4274be84b5e4ca8275e89e050c</anchor>
      <arglist>(...)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const bool</type>
      <name>value</name>
      <anchorfile>structTrafSim_1_1non__string__iterable.html</anchorfile>
      <anchor>1713d9b327c5dff77210d336863d8eeb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Spline</name>
    <filename>classTrafSim_1_1Spline.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type>string</type>
      <name>name</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>83542291bb9730f29915772ab9a4d3d2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isEnd</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>1a871ebbd43c4559f102e33f1bc406be</anchor>
      <arglist>(Vector point)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isStart</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>b9bd09fcbfc5abcd653f6c5fcb49b775</anchor>
      <arglist>(Vector point)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Spline</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>a365082fedc35e4367fdf88d61233c76</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Spline</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>789b4cb21cf9af713ef54d6adf5f411b</anchor>
      <arglist>(std::vector&lt; Vector &gt; const &amp;points, unsigned int resolution=100)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Spline</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>7aace0611fc8d1d88cd775179dd064a2</anchor>
      <arglist>(Spline &amp;original, float displacement, float relativeResolution=0.05f, bool reversed=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Spline</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>99b5552dfa559e1948a34a46969faf36</anchor>
      <arglist>(std::vector&lt; JoinDesc &gt; const &amp;parts)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Spline</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>43af670f9827159235193424579660c9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>dd36eb2435bd43fdcf3b8855932c7851</anchor>
      <arglist>(std::vector&lt; Vector &gt; const &amp;points, unsigned int resolution=100)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>45551a32b74a5ee757f83fa6d2c76423</anchor>
      <arglist>(Spline &amp;original, float displacement, float relativeResolution=0.05f, bool reversed=false)</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; unsigned int &gt;</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>86585b113c2b38da85e5bf4fe7dfd58d</anchor>
      <arglist>(std::vector&lt; JoinDesc &gt; const &amp;parts)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>cacheData</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>93f36d405222587ebc6adafb60cec152</anchor>
      <arglist>(bool regenerate=false)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isCached</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>891787fcec343e9314297616ec55b690</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>evalCoordinate</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>b2861e6242a711268176700069ed7356</anchor>
      <arglist>(float t) const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>evalNormal</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>d4a3521aa35d731c1b3f27311ea36a6c</anchor>
      <arglist>(float t) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>evalCurvature</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>0048bb86ed15c39e16d7f5c32a0e6861</anchor>
      <arglist>(float t) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>evalLength</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>e7a99d770cc97f4a1a7cc219ded50111</anchor>
      <arglist>(float t1, float t2) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>evalDisplacement</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>b80d0a5180c6f5bee0dfd84f09d68e05</anchor>
      <arglist>(float t0, float dist) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>closestToPoint</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>866edb763839da51e156c232902e1b5c</anchor>
      <arglist>(Vector const &amp;point, unsigned int startIndex=0, unsigned int endIndex=-1) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>getInterference</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>1fee1463d9cd4fbf217d7d8deda2fc57</anchor>
      <arglist>(Spline &amp;otherSpline, std::vector&lt; SplineInterferenceDesc &gt; &amp;result, float threshold, float subResolution=10.0f)</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getResolution</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>060c8b432ebe550dad2601c18c3d3a77</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Vector *</type>
      <name>getCoordinateData</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>381697f9c3e83b9b63c99af840f68d12</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const Vector *</type>
      <name>getNormalData</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>4d9d7ee9b30d1ec17fc267bd04233e9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const float *</type>
      <name>getLengthData</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>11563c2d42c5972f2646cbab07de1d85</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const float *</type>
      <name>getCurvatureData</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>b4c6135de9ec10b24cb51b5684e25cef</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::pair&lt; unsigned int, float &gt;</type>
      <name>_mapTCoord</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>014a6691a4569804d94bbbf781a83455</anchor>
      <arglist>(float t) const </arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>b3016912d0b3075718dce245b332f564</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>c161c54e24b8542455c53563187f35fa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>82917a7e943c7302b1eade0c0d4c46ee</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>873d6db25f6325764336532be080cf37</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>a8ae2c5b9c3970c936e57ec8e2ec94f3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>e2ec1e3cdcf74eb826b087d6698100ce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>f9606d3ea931f6cedec47d97394f3711</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>90e4cc4838a782fa753eaf3869330a34</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>263d3b27902c506ddb9bb8a9d0235e6e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>d5077a94c14b67e0c924f92fec2352f2</anchor>
      <arglist>(float t0, float t1)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Spline *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>fc6ad18e5976b1c5e3a07aee3cefdae0</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numForwardLanes</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>efcdd449bd4855be80c1444fda5d0a9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>numReverseLanes</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>9150ba040a5702aa2085a30c5f217843</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Object *</type>
      <name>roadBuilt</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>0950ec54ec2cae1522ded38c0eb64828</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>roadName</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>ee6ad2486b43c42737173e57fa922b4f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::vector&lt; Vector &gt;</type>
      <name>points</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>bac2dc7057c0d4c018885e7459dc7908</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Color</type>
      <name>color</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>08ad5dc9027089cbcc719b0b4ef08741</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>26cfe62a2ab2b0f545f421c6a4e532d7</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="private" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>4b27330ffdb21a89ff70084078dec0a6</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>Vector</type>
      <name>_catmullRomEval</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>7ff3d078740f3f6e76ed305a55b330c5</anchor>
      <arglist>(float t) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>float</type>
      <name>_basis</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>db281b75912d4496170e2da0d4de6a9f</anchor>
      <arglist>(unsigned int i, float t) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>float</type>
      <name>_mapIndex</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>434d28b583914462d0aa359fa911ec42</anchor>
      <arglist>(unsigned int index) const </arglist>
    </member>
    <member kind="function" protection="private">
      <type>T</type>
      <name>_interpolate</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>e01db4d8721496ea80b5cd18cf04c491</anchor>
      <arglist>(T *field, float t) const </arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>cacheResolution</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>1565cf3f915afcfc4c3ad11d19ea1157</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector *</type>
      <name>coordinates</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>bb7f58d635785cd2a71af34c0be8ee7e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector *</type>
      <name>normals</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>c411587c3e8a46e9abd9da596fb6115e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float *</type>
      <name>lengths</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>a8f955a390ec4fecc260154d18457574</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float *</type>
      <name>curvature</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>7f509f0a6fe6c21fb643e5d3e7c1fd14</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend float</type>
      <name>curveConstraint</name>
      <anchorfile>classTrafSim_1_1Spline.html</anchorfile>
      <anchor>758c0964ff0286df6c94237ba1a47fb8</anchor>
      <arglist>(Spline const &amp;spline, float t, float vel, float linearAccel, float angularAccel)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::SplineCar</name>
    <filename>classTrafSim_1_1SplineCar.html</filename>
    <base>TrafSim::Object</base>
    <member kind="function">
      <type></type>
      <name>SplineCar</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>147aab500f1a3d0b9eb48ae3e6c600a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~SplineCar</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>b93b38050ad474356dce4888781c967e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>initialize</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>c8090908923a0b77368ccacb4beab45c</anchor>
      <arglist>(Spline s)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>741183a6b63b4dd731d5245dfc2d3238</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>ef85374e97fa28616a879ccc0c003b45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>simulate</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>dfae7b0ce1e36341474e99474312f6e5</anchor>
      <arglist>(float ticks)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>f1dae70759f5dd4048776655c820f0b6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>234598b787d1cbc458c6fc37c7f2f0ef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>2fb774545e6b86b656a28ea317e851c4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>a3e645cf57004a491769764ed65c557a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>360a6bf7d03a20c40645fc8c2a214874</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::vector&lt; vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>c3280e9aec97f043fa491c676524011f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>80559fc7e0f7c9bc371bd39c49189942</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>invertColor</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>c46dcbb7d8a54d111f444fb3ed7352c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>bc112906494723306f87bbeb85bb5bb4</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>6815edbb424825ec42c7b8b1ec0333fd</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static SplineCar *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>a86a10a26bc2c9fc479576753fa392f8</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Color</type>
      <name>color</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>432ef876dc8b439459cdf615ecc414f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>size</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>c685ff3237835e90264e461ed01cee04</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Spline</type>
      <name>spline</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>6b2d7002ca89c85da3e446b1df08bb42</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>splineLength</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>282d1932b9bfdb72ba5f7bd1cd63b943</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>position</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>9c8a94c4de26e8b30a59102cb93ccc91</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>orientationVector</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>50bf975c0a94ffa1f2983c292a9d4e52</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>orientation</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>cb247041a6e17a24b715282f52cda177</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>velocity</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>debb65ceb517d63d83d062807d43edc8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>float</type>
      <name>splineT</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>ad0dc34b243c17a318fcc1a58a38e059</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>hasSpline</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>0f26121b556c8594119b8242fb45becc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>double</type>
      <name>tickMultiplier</name>
      <anchorfile>classTrafSim_1_1SplineCar.html</anchorfile>
      <anchor>adf28e5c0cc7d1e5dc13cb213000f4e7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Vector</name>
    <filename>classTrafSim_1_1Vector.html</filename>
    <member kind="function">
      <type></type>
      <name>Vector</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>bf04c192c4dddeac533927a29774903e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Vector</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>f78f93a5c6f6933750674aaaae792c1e</anchor>
      <arglist>(float x, float y)</arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>operator+</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>40b78ff6fa60f644fa3104ea52542945</anchor>
      <arglist>(Vector const &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>operator-</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>fcbba77794c2ff2b747686e9452de5c0</anchor>
      <arglist>(Vector const &amp;rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>operator *</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>353aa31c8ed52b4ddb78173f4aa54c52</anchor>
      <arglist>(float rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>operator/</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>7d460ccc44a8f2f03d2377031510a9ea</anchor>
      <arglist>(float rhs) const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>operator-</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>e955f6dbb96700a3844ae3229faa05ed</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector &amp;</type>
      <name>operator+=</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>83c21ddbac9ff8f93879f017b08be329</anchor>
      <arglist>(Vector const &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>Vector &amp;</type>
      <name>operator-=</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>34cb0110495396f3e0ae3ef3f74459e6</anchor>
      <arglist>(Vector const &amp;rhs)</arglist>
    </member>
    <member kind="function">
      <type>Vector &amp;</type>
      <name>operator *=</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>fe9943cbb5189e2479c2bbf76486e643</anchor>
      <arglist>(float rhs)</arglist>
    </member>
    <member kind="function">
      <type>Vector &amp;</type>
      <name>operator/=</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>f18c8dcf58c847fa778fd518d53c3efd</anchor>
      <arglist>(float rhs)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>length</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>90d6d8f9a7c218ef9c788b87a3fae7f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>normalized</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>1d8215a77dc57508b67146af324f35a8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector</type>
      <name>perpendicular</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>469790de8da1e64cf058df3ea8e0a797</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>normalize</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>2580afc996ce45bda19d38b7f0cd6f77</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>6e97ac472f827e29997e915fe04c365f</anchor>
      <arglist>(Vector const &amp;origin=Vector(), float arrowSize=1.0f) const </arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>convertToPoint2</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>42407a2aaadf9cb5b1d6a788792e76e4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static float</type>
      <name>dot</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>5bca836a528a3378ed8f37581280cc63</anchor>
      <arglist>(Vector const &amp;v1, Vector const &amp;v2)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static float</type>
      <name>dist2</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>97560d9f38a390bd4793ccb0453480ca</anchor>
      <arglist>(Vector const &amp;v1, Vector const &amp;v2)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>intersection</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>950ece3514b53cb875f5741f285c261d</anchor>
      <arglist>(Vector const &amp;seg1pt1, Vector const &amp;seg1pt2, Vector const &amp;seg2pt1, Vector const &amp;seg2pt2, Vector &amp;output)</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>x</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>2532964e0a945c21fd016d0b176779c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>y</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>65d556588c950ff50f93b26d33b9111c</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>3a81e5595a879b45669a83b4242759bd</anchor>
      <arglist>(std::ostream &amp;os, Vector const &amp;v)</arglist>
    </member>
    <member kind="friend">
      <type>friend std::istream &amp;</type>
      <name>operator&gt;&gt;</name>
      <anchorfile>classTrafSim_1_1Vector.html</anchorfile>
      <anchor>c198cff0f4196c66649278458eebf227</anchor>
      <arglist>(std::istream &amp;is, Vector &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::ViewportEvent</name>
    <filename>structTrafSim_1_1ViewportEvent.html</filename>
    <member kind="variable">
      <type>ViewportEventType</type>
      <name>type</name>
      <anchorfile>structTrafSim_1_1ViewportEvent.html</anchorfile>
      <anchor>df7ce95a318669569def23439b9fe55b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned int</type>
      <name>button</name>
      <anchorfile>structTrafSim_1_1ViewportEvent.html</anchorfile>
      <anchor>0a6273afb5bc8e0b6f79a59944103fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>Vector</type>
      <name>pos</name>
      <anchorfile>structTrafSim_1_1ViewportEvent.html</anchorfile>
      <anchor>7059f5d5ec2c7bc7685fee53d620609b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::Viewport</name>
    <filename>classTrafSim_1_1Viewport.html</filename>
    <member kind="function">
      <type></type>
      <name>Viewport</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>f22cde492b4c4584e800aee7acfdfc1a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Viewport</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>9c71a877ebcf52515b63a791db4eade2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>open</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>ec7cadfc421497a0c021eaebd90ec8de</anchor>
      <arglist>(unsigned int width, unsigned int height, std::string const &amp;caption)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>close</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>55199edb36033d697fcb161d4975e010</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getScreenWidth</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>a686e25b64f121d144fc7fcb3b3b81b1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getScreenHeight</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>6bcbda261740213ae6a529c680dbb7c9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector const &amp;</type>
      <name>getPhysicalSize</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>dd645c98772cbf506b028eef970e56f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Vector const &amp;</type>
      <name>getPhysicalPosition</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>7aa6a7aa8a6c3e1a8183a4135a175608</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isPanZoomEnabled</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>64b26bf82c0307227081d8842cc3778e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>resize</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>26013dcad19a00b0ecff2411e434d455</anchor>
      <arglist>(unsigned int width, unsigned int height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>enablePanZoom</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>f6f7e3ca46c319c1d4b466367dd94ec8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>disablePanZoom</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>d6c9ed4b2f6d397061fb30ece51215fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setPhysicalSize</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>f420168ac153bde53ee81c95119e0122</anchor>
      <arglist>(Vector const &amp;size)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setPhysicalPosition</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>9e1b2eda443d6e9c8f72b19197caf6b2</anchor>
      <arglist>(Vector const &amp;position)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>setBackgroundColor</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>8da0498d684ed1f1c283d65f497b06e1</anchor>
      <arglist>(Color const &amp;c)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>pollEvents</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>f6b4fba793b56701b085232bd8bcc49f</anchor>
      <arglist>(ViewportEvent &amp;viewportEvent)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>repaint</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>b9d2a7aa6bb4c900e16379ed3b81f9c0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="enumvalue" protection="private">
      <type>@</type>
      <name>NONE</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>44edd01c75f547a788569a7ffb9ec76c258bc089adeea54665cfc8e5aad26b43</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue" protection="private">
      <type>@</type>
      <name>PAN</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>44edd01c75f547a788569a7ffb9ec76c50d1394f8ab0ba8ad6d39e9dc15ca9f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue" protection="private">
      <type>@</type>
      <name>ZOOM</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>44edd01c75f547a788569a7ffb9ec76ccca8cfef1317aba41da901615f2d8716</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>PanZoomMode</name>
      <anchor>44edd01c75f547a788569a7ffb9ec76c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NONE</name>
      <anchor>44edd01c75f547a788569a7ffb9ec76c258bc089adeea54665cfc8e5aad26b43</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PAN</name>
      <anchor>44edd01c75f547a788569a7ffb9ec76c50d1394f8ab0ba8ad6d39e9dc15ca9f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ZOOM</name>
      <anchor>44edd01c75f547a788569a7ffb9ec76ccca8cfef1317aba41da901615f2d8716</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>_handleMouseMotion</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>26b5643802cc55ec519165f4ba3f0d37</anchor>
      <arglist>(float xrel, float yrel)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>_handleMouseButtonDown</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>d8751de023871dfd54fee94cbf33c9e8</anchor>
      <arglist>(unsigned int button, unsigned int x, unsigned int y)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>_handleMouseButtonUp</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>f1e6ad006ac2cc8619fdea92a1bf3bae</anchor>
      <arglist>(unsigned int button, unsigned int x, unsigned int y)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>_handleKeyUp</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>211ddaf26e6bc1767b47b1930f763711</anchor>
      <arglist>(SDLKey key)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>_handleKeyDown</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>f325f21816b924162047ab8599de87a5</anchor>
      <arglist>(SDLKey key)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>Vector</type>
      <name>_translateCoords</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>9bc6651553fcc9ff9fd25d197a28c3c2</anchor>
      <arglist>(int x, int y)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>SDL_Surface *</type>
      <name>screen</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>964fa05dcb5027ae758bdf9f2052e6bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>screenWidth</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>eac96a87df49f693e6d897613c83f6d9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>unsigned int</type>
      <name>screenHeight</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>c9ec5220f7b2dd788a4670b52f793a41</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>const Vector</type>
      <name>defaultPhysicalSize</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>44d68582cf62ee93f320ab3ea56d06b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>const Vector</type>
      <name>defaultPhysicalPos</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>e7974f3d133718ac764ade2645677f10</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>physicalSize</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>ea5f44405cb24f56321a90b1d397b3b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Vector</type>
      <name>physicalPos</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>4d811edd65c7be6acf6acc04b9ec2b1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>PanZoomMode</type>
      <name>panZoomMode</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>d5681f431e655730176760e6c433271f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>panZoomEnabled</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>529f661a96954350034cff7d5afbe7d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>altDown</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>e8326a5809db695b9732128e479546d3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>closed</name>
      <anchorfile>classTrafSim_1_1Viewport.html</anchorfile>
      <anchor>cc7462f5f8dcdb1c7d3345a5d89eeba5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::XIntersection</name>
    <filename>classTrafSim_1_1XIntersection.html</filename>
    <base>TrafSim::Intersection</base>
    <member kind="function">
      <type></type>
      <name>XIntersection</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>343a1365fff6e36a6eedd313bb9bb83a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~XIntersection</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>68b4c2054a2f5cba872f163aa3b6f015</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>5e88556ab558b7361b5a4d216b651e2a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>totalRoads</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>e97ac33742026e3fdd1273ba509ef240</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>11b2dc60b153d61f7f556d25c80bf787</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>hookStart</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>eaa6b126a942e4673802ebe47d572f46</anchor>
      <arglist>(Road &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>hookEnd</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>5244e4b9d94bda9b8259997d45e3bc79</anchor>
      <arglist>(Road &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>blockManeuver</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>686027663cf5dfa2aa76a68319c794cb</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>unblockManeuver</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>7b43ba35d095bf399c9e6a0010b78755</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>computeGeometry</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>349b41e79f805ae8c1d0c7b23dcf54d5</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual RoadLane</type>
      <name>getDestination</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>abcb82c7ccd8ff17e731a29b343f3496</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Lane *</type>
      <name>getLane</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>5974866c385a0a4e54a9a7dbc0a3e617</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setStop</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>7310114a660c5e33615660aba1a9041c</anchor>
      <arglist>(Road const &amp;road, bool stopSign)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isStop</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>ac133807e54a0c31b714b3708322774f</anchor>
      <arglist>(Road const &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual RightOfWay</type>
      <name>rightOfWay</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>075729d9d600285c2cced3de1eba825a</anchor>
      <arglist>(Car *thisCar, Road const &amp;road, Car **followCar)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enterQueue</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>5025f19d729d3156a1d2c06a1553ce95</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>exitIntersection</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>58e90a3cebc53cf6036ee1db81c8f4d9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>exitIntersection</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>f4c4caf6b9edb669ce57e5dd1cb7e084</anchor>
      <arglist>(Car *thisCar)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getOpposingTraffic</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>6dc93624b10971f747f6f4adaf48debc</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver, std::vector&lt; Lane * &gt; &amp;result)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBoundaries</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>4a732cf88b01801a9ec6d21adfcd9866</anchor>
      <arglist>(std::vector&lt; Spline * &gt; &amp;boundsArray)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>1850170e380b3ac98779e1110c6c8072</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>129f869969701fb579500ef5133bbe2d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>badae0e14bcca0aaf73803d0624a5197</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>a3bb094d29bcb655714a216957834ea8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>2ba0bb0fba9da388c42a715630e9b7ac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>2d153e940cb7481d316968ce41bd56c2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>d757fce9f18ca865c37cf9cb3e976c53</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static XIntersection *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>bba777228883de3a7469eb65a1d08b28</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>999bfafb972909b82380063e30184f38</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>2a8382898aeaf3a5d399316fbf399f1d</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>hook</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>75b9d5234176725e14ef46c3cb595ccc</anchor>
      <arglist>(Road &amp;road, bool reverse)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>sortRoads</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>43b4db7edd06929df6901978c2b1941f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>computeBoundaries</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>875337e4b832c5cf4193fee48bc6f34a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>computeLanes</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>11fecaeb98e6590ec714299047200044</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>computeInterference</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>c6d74c29f3b0b5716679b1dbcb9279f1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>calculateCurve</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>ad1b040ed53d83dbbf8e2eb43ce2fa82</anchor>
      <arglist>(Vector const &amp;pt1, Vector const &amp;tan1, Vector const &amp;pt2, Vector const &amp;tan2, float sharpness, float narrowing, Spline &amp;result)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Spline</type>
      <name>boundaries</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>dd4f24cfb1b158d9349db7dc1955f895</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Road *</type>
      <name>roads</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>6288a47d0919fe53fa378aacd58fe151</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; ManeuverDestMap &gt; *</type>
      <name>maneuverData</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>2b73a4cf3107283f764901063b476d0e</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; ManeuverLaneMap &gt; *</type>
      <name>laneData</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>0af205c31bdc96fbde7ccf9417855176</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>reversed</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>c58a6fe20f26141194c5d63534a2b090</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>stops</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>62ccc72a62fb63537453ac424dfdc238</anchor>
      <arglist>[4]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Vector</type>
      <name>center</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>126435ee106edf843389f7bcc719e37e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>fourWayStop</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>4faff599b222780f6d735953145fafc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Car *</type>
      <name>lastCar</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>dc42dd0a0ac208a4325ccd96f308618c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::map&lt; Lane *, Lane * &gt;</type>
      <name>multiLanes</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>ef7b68062c28547c6b4cbdb842d18c15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::map&lt; std::string, Object * &gt;</type>
      <name>namedMap</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>05a20711b683d33c5e1c816f9935d470</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>computed</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>c10460c781d84ae040b314df5320797b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private" virtualness="virtual">
      <type>virtual void</type>
      <name>_postResolutionCallback</name>
      <anchorfile>classTrafSim_1_1XIntersection.html</anchorfile>
      <anchor>9495538a86f001789edf6fdb3b1d0253</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TrafSim::YIntersection</name>
    <filename>classTrafSim_1_1YIntersection.html</filename>
    <base>TrafSim::Intersection</base>
    <member kind="function">
      <type></type>
      <name>YIntersection</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>6a400b35d30e52d471e5d596d65804dd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~YIntersection</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>ebec281debda4b7323b022e35c92bec9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getClassID</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>bd0fbb42cf718396e906f187542ac1a6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>totalRoads</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>b0ce87d43b042a9f05bca5ca7bfc4a14</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>draw</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>7d906e87ee440fdc72bb5716e5a8d547</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>hookStart</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>4c00f449edad2a6080d5c3cd07e413a0</anchor>
      <arglist>(Road &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>hookEnd</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>038e5d6ca0e972579b4949d90ae350a0</anchor>
      <arglist>(Road &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>blockManeuver</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>5d0593874664972272e6ed8a1e8181af</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>unblockManeuver</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>485b65078e53f530eda5e86ef3e7d77b</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>computeGeometry</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>c888941b9e9a1af49bc253df27330b45</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual RoadLane</type>
      <name>getDestination</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>1e910142c168d55c2c963b89389e383b</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Lane *</type>
      <name>getLane</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>ae6692ff1b66214b6fed43717abfa44d</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>setStop</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>b757a364f3b6605f704869a116818c95</anchor>
      <arglist>(Road const &amp;road, bool stopSign)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isStop</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>17bb77bdc73e8a44290283dae8b502a0</anchor>
      <arglist>(Road const &amp;road)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual RightOfWay</type>
      <name>rightOfWay</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>3a8845cc9139152371372fac1d8cc591</anchor>
      <arglist>(Car *thisCar, Road const &amp;road, Car **followCar)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>enterQueue</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>f6a3c79c4b896f2ca90816064fd12c3b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>exitIntersection</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>2043ff4b571bc2d64f52fed818868309</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>exitIntersection</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>623676931265ba77f5ac7dcf4e506e21</anchor>
      <arglist>(Car *thisCar)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getOpposingTraffic</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>43bd28ae0db1e17bfb6f9639e50bf8c3</anchor>
      <arglist>(Road const &amp;startRoad, unsigned int startLane, CrossManeuver maneuver, std::vector&lt; Lane * &gt; &amp;result)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>getBoundaries</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>5fbc212eb5a193be9819129fc66be52f</anchor>
      <arglist>(std::vector&lt; Spline * &gt; &amp;boundsArray)</arglist>
    </member>
    <member kind="function">
      <type>point2</type>
      <name>getCenter</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>28ff11e0a80868dce45fa7fec99f17a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getVelocity</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>176a4a091bdfc1ac21317ad288f41af8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getOrientation</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>0fb19b3fa2a82e640c1c7c2a6f04c02c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getLength</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>a8b524a5684f732ebf2a2774e8cec78a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getWidth</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>0c65ea6444bdf471f62d26b921a97727</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::vector&lt; point2 &gt; &gt;</type>
      <name>getBoundary</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>3d9f3639782f6801bcd1b2bdbe4d10f2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>containsObject</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>fd3fd0befc108633bfffcdef97bde684</anchor>
      <arglist>(Object *obj)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static YIntersection *</type>
      <name>downcast</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>050f8ab008699bd51d75e0ba4cb7d75e</anchor>
      <arglist>(Object *source)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>serialize</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>55c6f07eff88662b7d788010eb4ad647</anchor>
      <arglist>(std::ostream &amp;os) const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>deserialize</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>6190aac09b9c3a93c560efe42cb696ee</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>int</type>
      <name>hook</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>3c321a2585646835e91f796d88acb221</anchor>
      <arglist>(Road &amp;road, bool reversed)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>sortRoads</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>20f416bd2cacc52998af3077fae44524</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>computeBoundaries</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>bbe0520e51cc9cbe0766f5b417103298</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>computeLanes</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>4c2f9d9fd2bdd6fd1a5f4edc74915855</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>computeInterference</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>853efc4c9db1c92c39a0122b93017bdf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>calculateCurve</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>5034331f63c37a60a8682c24c76654aa</anchor>
      <arglist>(Vector const &amp;pt1, Vector const &amp;tan1, Vector const &amp;pt2, Vector const &amp;tan2, float sharpness, float narrowing, Spline &amp;result)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Spline</type>
      <name>boundaries</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>1450aae868fcedf350ed0cc22786b68f</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Road *</type>
      <name>roads</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>b5e55cd5d5cff3481f63e9d6b1bdad32</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; ManeuverDestMap &gt; *</type>
      <name>maneuverData</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>d47a310a9979b19b95dab929b50d7242</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; ManeuverLaneMap &gt; *</type>
      <name>laneData</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>d8f3fa8040c02f374c554c77239fc705</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>reversed</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>0987977ad1d8bfd449313747e2bec841</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>stops</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>3deee32c8adf11419dfbd68aa0030218</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Vector</type>
      <name>center</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>e87ef173a61599f675e6238d8c984d8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Car *</type>
      <name>lastCar</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>dc28bc7a548e3d453ed6e3a6e99352c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::map&lt; Lane *, Lane * &gt;</type>
      <name>multiLanes</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>66267f20f6098e3454c6bb281a80bc45</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::map&lt; std::string, Object * &gt;</type>
      <name>namedMap</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>a1a07c956a9773f07d42f10669aa845e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>computed</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>56be22a8911a09feeebceb99b2d0afd5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="private" virtualness="virtual">
      <type>virtual void</type>
      <name>_postResolutionCallback</name>
      <anchorfile>classTrafSim_1_1YIntersection.html</anchorfile>
      <anchor>f4ebcf49fa35892f691aed1d4dcd3bdd</anchor>
      <arglist>(Environment &amp;env)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>TrafSim::non_string_iterable&lt; std::basic_string&lt; T &gt; &gt;</name>
    <filename>structTrafSim_1_1non__string__iterable_3_01std_1_1basic__string_3_01T_01_4_01_4.html</filename>
    <templarg>T</templarg>
    <member kind="variable" static="yes">
      <type>static const bool</type>
      <name>value</name>
      <anchorfile>structTrafSim_1_1non__string__iterable_3_01std_1_1basic__string_3_01T_01_4_01_4.html</anchorfile>
      <anchor>3d62542e8af0b172dfd33878ed6749b0</anchor>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>
