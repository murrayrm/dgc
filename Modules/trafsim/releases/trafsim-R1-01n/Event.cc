#include "Event.hh"

#include <GL/gl.h>

namespace TrafSim {

  Event::Event() :
	function("NULL"), x(0.0f), y(0.0f), distance(0.0f), delay(0.0f),
	startTime(time(NULL)), waiting(false), active(false),
	color(0.5f, 0.5f, 0.5f, 0.0f) { }

  Event::Event(string Function, float X, float Y, float Distance, float Delay):
	function(Function), x(X), y(Y), distance(Distance), delay(Delay),
	startTime(time(NULL)), waiting(false), active(false),
	color(0.0f, 0.5f, 0.0f, 0.2f) { 
	  if (distance < 0) {
	    waiting = true;
	  }
	}


  void Event::draw() {
	std::vector<point2> boundary;
	double a = cos(M_PI/4);
	double r = distance;
	boundary.push_back(point2(r+x, 0+y));
	boundary.push_back(point2(r*a+x, r*a+y));
	boundary.push_back(point2(0+x, r+y));
	boundary.push_back(point2(-r*a+x, r*a+y));
	boundary.push_back(point2(-r+x, 0+y));
	boundary.push_back(point2(-r*a+x, -r*a+y));
	boundary.push_back(point2(0+x, -r+y));
	boundary.push_back(point2(r*a+x, -r*a+y));

	glPushMatrix();
	glSetColor(color);
	glBegin(GL_POLYGON);
	for(int i = 0 ; i < boundary.size() ; i++) {
	  glVertex3f(boundary[i].x, boundary[i].y, 0.0f);
	}
	glEnd();
	glPopMatrix();
  }

  bool Event::invertColor() {
	color.r = 1.0f - color.r;
	color.g = 1.0f - color.g;
	color.b = 1.0f - color.b;
	color.a = 1.0f - color.a;
	return true;
  }

  bool Event::simulate(float X, float Y) {
	if (waiting || active) {
	  if (difftime(time(NULL), startTime) > delay) {
	    active = true;
	    return true;
	  }
	}
	else {
	  float separation = sqrt(pow(x-X, 2) + pow(y-Y, 2));
	  if (separation < distance) {
	    waiting = true;
	    startTime = time(NULL);
	    if (delay == 0.0f) {
		active = true;
		return true;
	    }
	  }
	}
	return false;
  }
}
