import string
import time

from libtrafsim_Py import *

from threading import Thread                        
from SuperDynamic import SuperDynamic
from SplineEditor import SplineEditor
from CarDriver import CarDriver
from Queue import Queue
from Scenarios import Scenarios
from ScenarioDictionary import ScenarioDictionary

class GenericApp(SuperDynamic, Thread):
    def __init__(self):
        Thread.__init__(self)
        SuperDynamic.__init__(self)

	# get the skynet key from the environment
	import os
	skynetKey = 0
        try:
            skynetKey = int(os.environ['SKYNET_KEY'])
        except KeyError:
            print "Error: no skynet key set."
            skynetKey = input("Please enter your skynet key: ")

	self.timestep = 0.1
        self.viewport = Viewport()
	self.environment = Environment(skynetKey)
        self.sel = dict()
	self.editor = SplineEditor(self.environment, self.sel)
        self.rndfIF = RNDFSimIF()
        self.scenarios = Scenarios(self.environment, self.editor, self.sel)
        self.scenarioname = ""
        self.dictionary = ScenarioDictionary(self.scenarios)
        self.environment.makingScenario = False


	# store roads so we can access them to place cars later
	self.ROADS = 0
        
        self.callQueue = Queue()
        self.timingFactor = 1.0

	self.aliceBuilt = False
	self.events = []

        self.quitMessage = 0
	print ""
	print "Welcome to trafsim. For help, type 'app.help()'. To quit, type 'app.exit()'."

    def rotate(self,newOrientation):
	self.editor.rotateCar(newOrientation)

    def addRoad(self,fLanes=1,bLanes=1):
	try:
	    spline = self.editor.selectedSpline
	    spline.numForwardLanes = fLanes
	    spline.numReverseLanes = bLanes
	    road = Road()
	    self.environment.addObject(road, 'MyRoad')
	    road.initialize(self.environment,spline,fLanes,bLanes,5.0)
	    spline.roadBuilt = road
	except ValueError:
	    print "Error: You must create a spline before adding a road. See app.help() for more info."

    def buildExternalCar(self, skynet_key):
	if not self.aliceBuilt:
	    print "Error: you must build Alice first!"
	else:
	    ec2 = ExternalCar(skynet_key,True)
	    ec2.setTranslation(self.environment.yTranslate,self.environment.xTranslate)
	    print self.environment.yTranslate
	    print self.environment.xTranslate
	    ec2.initialize(self.environment)
	    self.environment.addObject(ec2,'ExternCar')
	    print "Added external car on skynet key " + `skynet_key` + "."

    def buildAlice(self, wait = False):
        if not self.aliceBuilt:
            lg = LaneGrid()
	    lg.partition(self.environment,5.0,Vector(-700,-700), Vector(700, 700))  
	    self.environment.addObject(lg)
            self.editor.lg = lg
            
	    if self.environment.debug:
		ec = ExternalCar(self.environment.skynetKey,True)
	    else:
	        ec = ExternalCar(self.environment.skynetKey)
	    ec.setTranslation(self.environment.yTranslate,self.environment.xTranslate)
	    print self.environment.yTranslate
	    print self.environment.xTranslate
	    ec.initialize(self.environment)
	    self.environment.addObject(ec,'Alice')
	    self.aliceBuilt = True
	    self.Alice = ec
	    if wait:
	        print "Waiting for Alice to move"
	        Xo = ec.getX()
	        Yo = ec.getY()
	        while True:
		    x = abs(ec.getX() - Xo)
		    y = abs(ec.getY() - Yo)
                    if x > 100 or y > 100:
		        print "  Alice jumped: " + `x` + ", " + `y`
		        Xo = ec.getX()
			Yo = ec.getY()
		    elif x > .5: break
		    elif y > .5: break
	        print "It lives!" + " (" + `x` + ", " + `y` + ")"
        else:
	    print("Alice already built.")

    def save(self, filename):
        f = open(filename, 'w')
	print "A"
        f.write(str(self.environment))
	print "B"
        f.close()
        
    def load(self, filename, wait = False):
	print "Loading: " + filename + "  (Waiting = " + `wait` + ")"
        self.clear()
        f = open(filename)
	self.environment = Environment.parse(f.read())
        self.editor.environment = self.environment
        self.scenarios.environment = self.environment
	f.close()
        self.aliceBuilt=False
        self.buildAlice(wait)
    
    def loadRNDF(self, filename):
	self.rndfIF.loadFile(filename)	
        self.rndfIF.centerAllWaypoints()
	self.environment.setTranslation(self.rndfIF.getxTranslate(),self.rndfIF.getyTranslate())
        self.rndfIF.buildSegInfos()
	numSegs = self.rndfIF.getNumSegs()
	laneInfo = self.rndfIF.getLaneInfos()
	pointsForSplines = [self.rndfIF.getCenterLineOfSeg(i) for i in range(numSegs)]
        for points in range(numSegs): self.editor.buildFromPoints(pointsForSplines[points], laneInfo[points])
        #self.buildRoads()

    def buildRoads(self):
	#rds = [Road.downcast(r) for r in self.environment.getObjectsByClassID('Road').values()]
	#for rd in rds: self.environment.removeObject(rd.getName())
        self.environment.clearObjectsByClassID("Road")
        splines = [Spline.downcast(s) for s in self.environment.getObjectsByClassID('Spline').values()]
        #self.ROADS = [Road() for s in splines]
	roads = [Road() for s in splines]
        for road in roads: self.environment.addObject(road)
        for road, spline in zip(roads, splines): 
	    road.initialize(self.environment, spline, spline.numForwardLanes, spline.numReverseLanes, 6.0)
	    spline.roadBuilt = road
	    spline.roadName = road.getName()

	#HACK. THIS MIGHT BREAK OTHER THINGS. FIXES ROAD POSITION INFO.
        lg = LaneGrid()
	lg.partition(self.environment, 5.0, Vector(-700, -700), Vector(700,700))
	self.environment.addObject(lg)
	self.editor.lg = lg
	    
    def buildZones(self,filename):
	self.rndfIF.loadFile(filename)
	self.rndfIF.buildZoneInfos(self.environment.xTranslate,self.environment.yTranslate,True)
	numZoneLines = self.rndfIF.getNumZoneLines()
	pointsForSplines = [self.rndfIF.getLineFromZone(i) for i in range(numZoneLines)]
	for points in range(numZoneLines): self.editor.buildLineFromPoints(pointsForSplines[points])

    def addXIntersection(self):
    	self.editor.selectedIntersection = XIntersection()
	self.environment.addObject(self.editor.selectedIntersection)
	print "Hold down 'i' and click each of the endpoints of the four splines to add to the intersection."

    def addYIntersection(self):
        self.editor.selectedIntersection = YIntersection()
	self.environment.addObject(self.editor.selectedIntersection)
	print "Hold down 'i' and click each of the endpoints of the three splines to add to the intersection."	
    
    def clear(self):
	self.environment.clearObjects()
	del self.rndfIF
	self.rndfIF = RNDFSimIF()
	self.aliceBuilt = False

    def clearCars(self):
        self.environment.clearObjectsByClassID('Car')

    def export(self):
	self.environment.exportEnv()

    def addCar(self, maxVelocity=1.0, carPath = [CROSS_STRAIGHT]):
	self.editor.addCar(maxVelocity, carPath)

    def addBlock(self,length,width,orientation=0):
	self.editor.addBlockObs(length,width,orientation)

    def addCircle(self,radius):
	self.editor.addCircleObs(radius)

    def addEvent(self, scenario, distance = 1.0, delay = 0.0):
        self.editor.addEvent(scenario, distance, delay)

    def addLine(self):
	try:
	    spline = self.editor.selectedSpline
	    line = Line(spline)
	    self.environment.addObject(line)
	    print "Line added."
	except ValueError:
	    print "Error: You must create or select a spline before adding a line. See app.help() for more info."

    def addSelect(self, name):
        self.editor.addSelect(name)

    def addToScenario(self, string):
        line = "        " + string + "\n"
        if len(self.editor.makingEvents) > 0:
	    self.editor.eventStrings[len(self.editor.makingEvents)-1] += line
	    print line
        elif self.environment.makingScenario:
	    self.editor.string += line
	    print line
        else:
	    print "No scenario being written.  Ignoring command"

    def select(self, object):
        if self.editor.selected != None:
	    (self.editor.selected).invertColor()
        self.editor.selected = object
        if object != None:
	    object.invertColor()

    def debug(self):
	if self.environment.debug == True:
	    self.environment.debug = False
	    print "Debugging messages off"
	elif self.environment.debug == False:
	    self.environment.debug = True
	    print "Debugging messages on"

    def makeEvent(self, scenario, distance = 1.0, delay = 0.0):
        if not self.environment.makingScenario:
	    print "Not currently making scenario.  However, adding input to addEvent(s,f,f)"
	    self.addCar(scenario, distance, delay)
        else:
	    self.addCar(scenario, distance, delay)
	    self.editor.makingEvents.append(scenario)
	    self.editor.eventStrings.append("\n    def " + scenario + "(self):\n")
	    print "Making new event within scenario.  This can be nested."
	    print "Commit events with commitEvent()"

    def commitEvent(self):
        if len(self.editor.makingEvents) > 0:
	    i = len(self.editor.makingEvents)-1
	    f = open("Scenarios.py", mode='a+')
	    f.write(self.editor.eventStrings[i])
	    f.close()
	    f.open("ScenarioDictionary.py", mode='r+')
	    f.seek(-2,2)
	    ss = ", \n            \"" + self.editor.makingEvents[i] + "\" : self.scenarios." + self.editor.makingEvents[i] + " }"
	    f.write(ss)
	    f.close()
	    print "Committed event scenario : " + self.editor.makingEvents[i] + " (Level: " + `i` + ")"
	    del self.editor.makingEvents[i]
	    del self.editor.eventStrings[i]

    def makeScenario(self, name):
        self.scenarioname = name
        self.editor.string = "\n    def " + name + "(self):\n"
        self.editor.obsFile = ""
        print "Making scenario: " + name
        print "  Add desired obstacles and, when finished, enter"
        print "  app.commitScenario(), or app.CommitScenario(s,s,s,s,s,s)"
        print "If you want to start over or to exit this mode, enter"
        print "  cancelScenario()"
        self.environment.makingScenario = True
        self.timingFactor = 0.0000001
        print "Slowing environment to faciliate adding car"
        print "Disabling events"

    def cancelScenario(self):
        self.timingFactor = 1.0
        print "Returning cars to original speeds and enabling events"
        self.editor.string = ""
        self.editor.obsFile = ""
        self.editor.environment.makingScenario = False
        print "Scenario cancelled"

    def commitScenario(self):
        if len(self.editor.makingEvents) > 0:
	    print "Close all events before committing scenario"
	    return
        if self.environment.makingScenario == True:
	    f = open("Scenarios.py", mode='a+')
#	    self.editor.string += "\n"
            f.write(self.editor.string)
            f.close()
            f = open("ScenarioDictionary.py", mode='r+')
            f.seek(-2,2)
            ss = ",\n            \"" + self.scenarioname + "\" : self.scenarios." + self.scenarioname + " }"
            f.write(ss)
            f.close()
            self.environment.makingScenario = False
            print "Added scenario."
            self.timingFactor = 1
        elif self.environment.makingScenario == False:
	    print "Not currently making a scenario."

    def CommitScenario(self, start, scenario, rndf, mdf, obsFile):
        ss = "#! /usr/bin/python -i\n"
        ss +="\n"
        ss +="import os;\n"
        ss +="import sys;\n"
        ss +="\n"
        ss +="import sysTestLaunch\n"
        ss +="\n"
	ss +="start = '" + start + "'\n"
        ss +="scenario = '"+scenario+"'\n"
        ss +="rndf = '"+rndf+"'\n"
        ss +="mdf = '"+mdf+"'\n"
        ss +="obsFile = '"+obsFile+"'\n"
        ss +="sceneFunc = '"+self.scenarioname+"'\n"
        ss +="\n"
        ss +="sysTestLaunch.runPlanners( start, scenario, obsFile, rndf, mdf, sceneFunc)\n"
        filename="../system-tests/"+scenario+".py"
        f = open(filename, mode='w+')
        f.write(ss)
        f.close()
        print ss
	obsName="../system-tests/"+obsFile
	f = open(obsName, mode='w+')
	f.write(self.editor.obsFile)
	f.close()
        print self.editor.obsFile
        print "Scenario script written to " + filename
        self.commitScenario()

    def simulateEvents(self):
        i = 0
        events = self.editor.events
        x = self.Alice.getX()
        y = self.Alice.getY()
        if self.environment.makingScenario == True:
	    return False
        while i < len(events):
	    event = events[i]
	    if (event.simulate(x, y)):
	        function = event.function
	        print "Activated event: " + `i`
	        del events[i]
	        print "Loading function: " + function
	        self.dictionary.function[function]()
	    else:
	        i = i + 1

    def drawEvents(self):
        i = 0
        events = self.editor.events
        while i < len(events):
	    event = events[i]
	    event.draw()
	    i = i + 1

    def exit(self):
        print "Exiting Trafsim. Please mouse over the Trafsim display."
	import sys
	self.clear()
	self.quitMessage = 1
	sys.exit()
   
    def help(self):
	print ""
	print "TRAFSIM QUICK REFERENCE \n-----------------------"
	print "For more detailed information and tutorials, see the [[trafsim]] wiki page."
	print "\nOpening, Closing, and Saving Maps"
	print "     >>> app.load(\"tsFile\")"
	print "     >>> app.save(\"tsFile\")"
	print "     >>> app.loadRNDF(\"rndfFile\")"
	print "     >>> app.exit()"
	print "\nAdding Objects"
        print "    >>> app.addBlock((float)length, (float)width, (float)orientation)"
        print "    >>> app.addCar((float)maxVelocity)"
        print "    >>> app.addCircle((float)diameter)"
        print "    >>> app.addEvent((string)functionName, (float)distance=1.0, (float)delay=0.0)"
        print "    >>> app.addSelect((string)assignName)"
        print "    >>> app.addToScenario((string)line)"
        print "    >>> app.select((string)assignedName)"
        print "    >>> app.buildAlice()"
        print "    >>> app.buildRoads()"
        print "    >>> app.clear()"
        print "    >>> app.clearCars()"
        print "    >>> app.environment.clearObjectsByClassID(' ObjectType ')"
	print "\nScenarios"
        print "    >>> app.scenarios.Func()"
        print "    >>> app.makeEvent((string)FuncName, (float)distance=1.0, (float)delay=0.0)"
        print "    >>> app.commitEvent()"
        print "    >>> app.makeScenario((string)FuncName)"
        print "    >>> app.cancelScenario()"
        print "    >>> app.CommitScenario((string)start, (string)SimName, (string)rndf), (string)mdf, (string)ObsFile)"
	print "\nKey Mappings\n(to use these, hold the key and then click on the trafsim window)"
        print "    'a' - location to add Alice"
        print "    'b' - add block at click"
        print "    'c' - add car at click"
        print "    'i' - for intersection making"
        print "    'o' - add circle at click"
        print "    'p' - pause/resume all traffic"
        print "    'x' - pause/unpause nearest car"
        print "    '=' - speed up all traffic"
        print "    '-' - slow down all traffic"
        print "    '1' - resume normal speed for all traffic"
        print "    'r' - get road data"
        print "    's' - select nearest Object/Event"
        print "    'Ctrl' - Create a spline (places first point for a spline)"
        print "    'Shift' - Append to a spline (adds another point to the selected spline)"
        print "    'z' + 'Left Click' - Drag Map (move in the map)"
        print "    'z' + 'Right Click' - Zoom"
	print "\nMiscellaneous"
	print "    >>> app.debug()"
	print "    >>> app.TimingFactor = (float)#"
	print ""


    def run(self):
        self.viewport.open(800, 800, 'trafsim')
        
        lastTicks = time.time()
        
        try:
            while 1:
                event = ViewportEvent()
                while self.viewport.pollEvents(event):
                    if event.type == QUIT:
                        return
		    elif self.quitMessage == 1:
		        event.type = QUIT
		        return
                    else:
                        { EMPTY            : lambda event: None, 
                          REPAINT          : lambda event: None, 
                          MOUSEMOTION      : self.editor.onMouseMotion,
                          MOUSEBUTTONUP    : self.editor.onMouseUnclick,
                          MOUSEBUTTONDOWN  : self.editor.onMouseClick,
                          KEYUP            : self.editor.onKeyUp,
                          KEYDOWN          : self.editor.onKeyDown,
                         }[event.type](event) 
                 
                # call the functions passed into the thread
                while not self.callQueue.empty():
                    fn, args, kwds = self.callQueue.get()
                    fn(*args, **kwds)
                
                # update time
                newTicks = time.time()
                ticks = newTicks - lastTicks
                lastTicks = newTicks
             
                try: self.editor.perFrame(ticks * self.timingFactor)
                except AttributeError: pass

		time.sleep(self.timestep)
        #       if(ticks > .1):

		# see if the traffic needs to be sped up or slowed down
		if self.environment.speedUpTraffic == True:
		    self.timingFactor = self.timingFactor*2
		    self.environment.speedUpTraffic = False
		elif self.environment.slowDownTraffic == True:
		    self.timingFactor = self.timingFactor*0.5
		    self.environment.slowDownTraffic = False
		elif self.environment.resumeTraffic == True:
		    self.timingFactor = 1
		    self.environment.resumeTraffic = False

                self.environment.simulate(ticks * self.timingFactor)
		if self.aliceBuilt:
		    self.simulateEvents()
		        
		self.drawEvents()
		self.environment.draw()
            
                self.viewport.repaint()
        finally:
            # unload the graphics in advance, just in case
            del self.viewport
        
    def call(self, fn, *args, **kwds):
        self.callQueue.put( (fn, args, kwds) )

if __name__ == '__main__':
    import GenericApp
    import sys

    app = GenericApp.GenericApp()


    loadRNDF = False
    loadFunction = False
    loadFile = False
    subgroup = 0 
    wait = False
    sleeptime = 0.0
    
    for arg in range(len(sys.argv)):
        if sys.argv[arg] == '--loadRNDF':
            rndfname = sys.argv[arg + 1]
            loadRNDF = True
        elif sys.argv[arg] == '--loadFunction':
            functionname = sys.argv[arg + 1]
            loadFunction = True
        elif sys.argv[arg] == '--loadFile':
	    filename = sys.argv[arg + 1]
	    loadFile = True
        elif sys.argv[arg] == '--send-subgroup':
            subgroup = sys.argv[arg+1]
	    print sys.argv[arg+1]
        elif sys.argv[arg] == '--wait':
	    print "Will wait for Alice to move before starting"
            wait = True
        elif sys.argv[arg] == '--sleep':
	    sleeptime = string.atof(sys.argv[arg+1])
    
    if sleeptime > 0:
        print "Sleeping for " + `sleeptime` + " seconds"        
        time.sleep(sleeptime)
    
    app.start()

    if loadRNDF:
        app.loadRNDF(rndfname)
    if loadFile:
        app.load(filename, wait)
    if loadFunction:
        app.dictionary.function[functionname]()

    app.environment.subgroup = int(subgroup)

            
    
