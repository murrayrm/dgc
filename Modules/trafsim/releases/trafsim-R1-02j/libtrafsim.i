%module libtrafsim
%{
#include <sstream>
#include <map>
#include <SDL/SDL_keysym.h>
#include "skynettalker/StateClient.hh"
#include "skynet/sn_msg.hh"

#include "Serialization.hh"
#include "Vector.hh"
#include "Color.hh"
#include "Viewport.hh"
#include "Object.hh"
#include "Environment.hh"
#include "Spline.hh"
#include "Lane.hh"
#include "CarProperties.hh"
#include "Car.hh"
#include "ExternalCar.hh"
#include "CarFactory.hh"
#include "LinearSegmentDesc.hh"
#include "Road.hh"
#include "Intersection.hh"
#include "XIntersection.hh"
#include "YIntersection.hh"
#include "LaneGrid.hh"
#include "RNDFSimIF.hh"
#include "Obstacle.hh"
#include "Line.hh"
#include "Event.hh"
#include "SplineCar.hh"
%}

%ignore operator>>;
%ignore operator<<;
%ignore curveConstraint;

%include "std_string.i"
%include "std_map.i"
%include "std_vector.i"

%include "doctest/Vector.i"
%include "doctest/Color.i"
%include "doctest/Viewport.i"
%include "doctest/Environment.i"
%include "doctest/Object.i"
%include "doctest/Spline.i"

%feature("autodoc", "1");
%include "/usr/include/SDL/SDL_keysym.h"

%include "Vector.hh"
%include "Color.hh"
%include "Viewport.hh"
%include "Object.hh"
%include "Environment.hh"
%include "Spline.hh"
%include "Lane.hh"
%include "Intersection.hh"
%include "CarProperties.hh"
%include "Car.hh"
%include "ExternalCar.hh"
%include "CarFactory.hh"
%include "LinearSegmentDesc.hh"
%include "Road.hh"
%include "XIntersection.hh"
%include "YIntersection.hh"
%include "LaneGrid.hh"
%include "RNDFSimIF.hh"
%include "Obstacle.hh"
%include "Line.hh"
%include "Event.hh"
%include "SplineCar.hh"

%template(ObjectDictPair) std::pair<std::string, TrafSim::Object*>;
%template(ObjectDict) std::map<std::string, TrafSim::Object*>;

%template(VectorArray) std::vector<TrafSim::Vector>;
%template(LaneObject) std::pair<float, TrafSim::Object*>; 

%template(JoinDesc) TrafSim::LinearSegmentDesc<TrafSim::Spline>;
%template(LaneSegment) TrafSim::LinearSegmentDesc<TrafSim::Lane>;
%template(JoinDescArray) std::vector<TrafSim::JoinDesc>;
%template(LaneSegmentArray) std::vector<TrafSim::LaneSegment>;

%template(CarPath) std::vector<unsigned int>;
%template(SplineArray) std::vector<TrafSim::Spline*>;

%exception
{
	try { $action }
	catch (TrafSim::ParseError& e) 
	{
		PyErr_SetString(PyExc_Exception, e.info.c_str());
		return NULL;
	}
	catch (...)
	{
		PyErr_SetString(PyExc_Exception, "Unknown exception");
		return 0;
	}
}

%define FORCE_DISOWN(CLASS, METHOD, ARG_N)
%pythoncode
%{
__naked_CLASS_METHOD = CLASS.METHOD

def __disown_METHOD(*args):
	args[ARG_N].thisown = 0
	return __naked_CLASS_METHOD(*args)

__disown_METHOD.__doc__ = __naked_CLASS_METHOD.__doc__
CLASS.METHOD = __disown_METHOD
%}
%enddef

FORCE_DISOWN(Environment, addObject, 1)

%pythoncode 
%{
if __name__ == '__main__':
	import doctest
	doctest.testmod()
%}

%define PYTHON_IO(class)
%newobject class::parse;
%extend class
{
	std::string __str__()
	{
		std::ostringstream output;
		output << *self;
		return output.str();
	}
	
	static class* parse(std::string const& str)
	{
		class* new_obj = new class();
		
		std::istringstream input(str);	
		input >> *new_obj;
		
		return new_obj;
	}
};
%enddef

%define PYTHON_OBJ_IO(class)
%newobject class::parse;
%extend class
{
	std::string __str__()
	{
		std::ostringstream output;
		output << self;
		return output.str();
	}
	
	static class* parse(std::string const& str)
	{
		TrafSim::Object* new_obj;
		
		std::istringstream input(str);	
		input >> new_obj;
		
		return (class*)new_obj;
	}
};
%enddef

PYTHON_IO(TrafSim::Vector)
PYTHON_IO(TrafSim::Color)
PYTHON_IO(TrafSim::Environment)

PYTHON_OBJ_IO(TrafSim::Object)
PYTHON_OBJ_IO(TrafSim::TestObject)
PYTHON_OBJ_IO(TrafSim::Spline)

