#include "Object.hh"
#include "Serialization.hh"
#include "Environment.hh"

#include "Spline.hh"
#include "Lane.hh"
#include "Car.hh"
#include "Road.hh"
#include "Intersection.hh"
#include "XIntersection.hh"
#include "YIntersection.hh"
#include "CarFactory.hh"
#include "LaneGrid.hh"

namespace TrafSim
{

void Object::_registerObjects()
{
  //	dispatchMap["TestObject"] = _classFactory<TestObject>;
	dispatchMap["Spline"] = _classFactory<Spline>;
	dispatchMap["Lane"] = _classFactory<Lane>;
	dispatchMap["MultiLane"] = _classFactory<MultiLane>;
	dispatchMap["Car"] = _classFactory<Car>;
	dispatchMap["Road"] = _classFactory<Road>;
	dispatchMap["XIntersection"] = _classFactory<XIntersection>;
	dispatchMap["YIntersection"] = _classFactory<YIntersection>;
	dispatchMap["DeadEnd"] = _classFactory<DeadEnd>;
	dispatchMap["CarFactory"] = _classFactory<CarFactory>;
	dispatchMap["LaneGrid"] = _classFactory<LaneGrid>;

	_registered = true;
}

/* ****** STATIC DEFINITIONS ****** */
	
DispatchMap Object::dispatchMap;
bool Object::_registered = false;

/* ****** CONSTRUCTORS AND DESTRUCTORS ****** */

Object::Object(): name("object"), paused(false), timeStopped(0) {}

Object::~Object() {}

/* ****** NAME AND ID FUNCTIONS ****** */

std::string const& Object::getName() const
{
	return name;
}

std::string const& Object::getParent() const
{
	return parent;
}

void Object::setParent(std::string const& parentName)
{
	parent = parentName;
}

bool Object::operator==(Object* rhs) const
{
	return this == rhs;
}

/* ****** SIMULATION FUNCTIONS ****** */

void Object::draw() {}
bool Object::simulate(float ticks) { return true; }

/* ****** PRIVATE FUNCTIONS ****** */

void Object::_addReference(std::string const& name, Object** object)
{
	dependencyMap.insert(std::make_pair(name, object));
}

void Object::_resolveDependencies(Environment& env)
{
	for (DependencyMap::iterator i = dependencyMap.begin();
		 i != dependencyMap.end();
		 ++i)
	{
		if (i->first == "None")
			*(i->second) = 0;
		else
			*(i->second) = env.getObject(i->first);
	}
	
	dependencyMap.clear();
	_postResolutionCallback(env);
}

void Object::_postResolutionCallback(Environment& env)
{
	// this function gets called once all dependencies are resolved.
	// useful for complex structures where not all initialization can be done
	// in a single "pass" by the _resolveDependencies function.	
}
	
/* ****** SERIALIZATION FUNCTIONS ****** */

std::ostream& operator<< (std::ostream& os, Object* obj)
{
	 os << obj->getClassID() << "( ";
	 obj->serialize(os);
	 return os << " )";
}

std::istream& operator>> (std::istream& is, Object*& obj)
{
	is.exceptions ( std::istream::eofbit | std::istream::failbit | std::istream::badbit );
	
	if (!Object::_registered) 
	{
		Object::_registerObjects();
		Object::_registered = true;
	}
		
	try
	{
		obj = 0;
		std::string classID = readDelim(is, '('); // find the class name
		Object*(*createObject)() = Object::dispatchMap[classID]; // grab the appropriate class factory
		
		if (!createObject)
			throw ParseError("expected Class ID");
		
		obj = createObject(); // use the factory to create the correct subclass
		obj->deserialize(is); // use the subclass's deserialization capabilities
		matchString(is, ")"); 
		return is;			  // and we're done.
	}
	catch(ParseError& e)
	{
		if (obj)
			e.info = "Failed to parse " + obj->getClassID() + ":\n" + e.info;
		else
			e.info = "Failed to parse Object:\n" + e.info;
		throw;
	}
	catch(std::exception& e)
	{
		throw ParseError("Failed to parse Object:\n" + std::string(e.what()) );
	}
}

}

