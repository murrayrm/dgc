              Release Notes for "trafsim" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "trafsim" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "trafsim" module can be found in
the ChangeLog file.

Release R1-00y (Mon May  7 13:36:56 2007):
	Modified so that trafsim sends out everything in local coords. This matches up better with mapper. This is now a bug in trafsim where alice is visualized in the wrong location... this is not crucial however because you can just look at mapviewer for a correct visualization. 

Release R1-00x (Thu Apr 26 20:37:19 2007):
	Accidentally left a bug in ExternalCar.hh's constructor, fixed 
in this version.

Release R1-00w (Thu Apr 26 19:55:55 2007):
	Improved loading and saving functionality. Can now cleanly save 
and load any trafsim environment; RNDF does not have to be directly 
loaded if a trafsim file is loaded because the translations are stored 
in new save. Added app.exit() helper function to trafsim for a clean 
exit from python. Added functionality to app.clear() such that it 
completely clears all data from memory, including RNDF data. This can be 
used so trafsim does not have to be reset to load/reload new 
rndf's/other trafsim files. Added more information to app.help().

Release R1-00v (Wed Apr 25 23:54:20 2007):
	Added unit tests for navigation - static obstacles

Release R1-00u (Wed Apr 25 22:36:20 2007):
	Fix for frames discrepancy when exporting road lines

Release R1-00t (Tue Apr 24 21:07:34 2007):
	Added a generic line object for creating stop lines, parking spaces, etc.

Release R1-00s (Tue Apr 24 13:06:16 2007):
	Fixed a few bugs with exporting obstacles

Release R1-00r (Thu Apr 19 18:15:51 2007):
	Added methods to the segment class in the RNDF interface such 
that, based on several approximations, the roads will be built such that 
the lanes correctly line up with the lanes in the RNDF file. Need to 
verify the lane width.

Release R1-00q (Tue Apr 17 14:37:21 2007):
	Modified the internal structure of the RNDF interface in order 
to have a structure capable of handling intersections and multiple lanes 
more cleanly. User interface/implementation is exactly the same.

Release R1-00p (Sun Apr 15 20:05:11 2007):
	Can now add obstacles (circle or block) of arbitrary size to the environment

Release R1-00o (Tue Apr 10 21:09:57 2007):
	Cleaned up the lane boundaries so they don't look so messy on export. trafsim now calculates road markings only once, not every time they're exported. A few more small additions, see trafsim help for info.

Release R1-00n (Mon Apr  9  0:21:40 2007):
	Fixed a bug regarding exporting lane lines. trafsim now exports intersection information. added more help messages and command to toggle debugging messages.

Release R1-00m (Sun Apr  8 16:44:15 2007):
	Added some new functionality which makes it easy to add arbitrarily sized static obstacles to the environment. Also added capability to move Alice to a new, arbitrary location so we can mimic what would happen at the Site Visit if DARPA paused Alice and moved her.

Release R1-00l (Fri Apr  6  2:24:28 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>


Release R1-00k (Sun Apr  1 20:27:24 2007):
	Mostly cleanup, no new functionality yet. Made the classes inherited from Object more consistent. Also added a script to generate doxygen docs until a YaM solution comes up.

Release R1-00j (Fri Mar  9 15:40:13 2007):
	Updated trafsim to work with latest version of mapper. Currently exports northing as x and easting as y, but the internal trafsim 
representation of northing/easting in trafsim is still switched.

Release R1-00i (Sat Mar  3 16:34:01 2007):
	More functionality when loading RNDFs. This version should work 
for testing. Release for Implementation Test #2.
Release R1-00h (Sun Feb 25 17:22:47 2007):
	Fixed trafsim header files to work with new skynet/interfaces/etc rearrangement. Added ability to export the road lines for the road Alice is currently on. Tested this with the new mapper (skynettalker/testRecvMapElement).

Release R1-00g (Sun Feb 18 22:28:55 2007):
	Added RNDF wrapper class with (minimal) ability to parse RNDF 
files into trafsim. Minor useability changes for updated functionality.

Release R1-00f (Sun Feb 18 21:38:35 2007):
	Added MapElement interface to trafsim. trafsim can now export car objects to the mapper via skynet. Also added ability to create a 'stopped car' to trafsim.

Release R1-00e (Fri Feb  9 18:31:47 2007):
	Added ability to receive asim messages to ExternalCar objects. 
	
	To test out this capability, run the following (with XXXXXX replaced
	with some number):
	export SKYNET_KEY=XXXXXX
	bin/Drun asim
	
	New terminal:
	export SKYNET_KEY=XXXXXX
	bin/Drun python -i trafsim/GenericApp.py
	
	In the python prompt:
	lg = LaneGrid()
	lg.partition(app.environment,5.0,Vector(-100,-100), Vector(100, 100))  
	app.environment.addObject(lg)
	ec = ExternalCar(app.environment.skynetKey)
	ec.initialize(app.environment)
	app.editor = CarDriver(ec)
	ec.simulate()
	
	Note that ec.simulate() does not yet run automatically.
	Also make sure to unpause asim.	

Release R1-00d (Thu Feb  8 20:47:28 2007):
	Fixed a bunch of python makefile stuff (Abhi).

Release R1-00c (Sat Feb  3 23:23:59 2007):
	Moved files out of redundant trafsim/src directory into trafsim/ and
	fixed Makefile.yam to reflect these changes.

Release R1-00b (Thu Feb  1 20:18:02 2007):
	Renamed files from .cpp and .h to .cc and .hh, as required by our coding
	standards. Written Makefile.yam, it creates the python bindings library
	in <target>/_libtrafsim.so (e.g. i486-gentoo-linux/_libtrafsim.so)
	and creates a link in $YAM_ROOT/lib/PYTHON/_libtrafsim.so (not sure if
	this is the best way, but it was the simples one).

Release R1-00 (Sun Jan 21 15:45:32 2007):
	Created.

























