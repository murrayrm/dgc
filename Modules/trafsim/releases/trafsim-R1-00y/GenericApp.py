import time
from libtrafsim_Py import *

from threading import Thread                        
from SuperDynamic import SuperDynamic
from SplineEditor import SplineEditor
from CarDriver import CarDriver
from Queue import Queue

class GenericApp(SuperDynamic, Thread):
    def __init__(self):
        Thread.__init__(self)
        SuperDynamic.__init__(self)
	
	# get the skynet key from the environment
	import os
	skynetKey = 0
        try:
            skynetKey = int(os.environ['SKYNET_KEY'])
        except KeyError:
            print "Error: no skynet key set."
            skynetKey = input("Please enter your skynet key: ")

	self.timestep = 0.1
        self.viewport = Viewport()
        self.environment = Environment(skynetKey)
        self.editor = SplineEditor(self.environment)
        self.rndfIF = RNDFSimIF()
	self.ec = ExternalCar(skynetKey)
        
        self.callQueue = Queue()
        self.timingFactor = 1.0

        self.quitMessage = 0
	print ""
	print "Welcome to trafsim. For help, type 'app.help()'"

    def buildTestA(self):
	self.loadRNDF("TRAFSIM_TEST_RNDF.txt")
	self.buildRoadsIntersect()
        self.environment.setTranslation(self.rndfIF.getxTranslate(),self.rndfIF.getyTranslate())

    def buildTPlanUnit(self):
	self.loadRNDF("tplanner_unittest.rndf")
	self.environment.clearObjects()
        self.load("tplanUnit.tsm")
	self.environment.setTranslation(self.rndfIF.getxTranslate(),self.rndfIF.getyTranslate())

    def runCarTest(self):
	spline = self.editor.selectedSpline
	road = Road()
	self.environment.addObject(road, 'MyRoad')
	road.initialize(self.environment,spline,1,2,5.0)
	c = Car(1)
	self.environment.addObject(c, 'Car')	
    	c.switchLane(road,1,0.6)
	c2 = Car(1)
	self.environment.addObject(c2, 'Car')
	c2.switchLane(road,1,0.1)
	c3 = Car()
	self.timingFactor = .1;
	self.environment.addObject(c3, 'Car')
	c3.switchLane(road,0,0.0)

    def rotate(self,newOrientation):
	self.editor.rotateCar(newOrientation)

    def addRoad(self,fLanes=1,bLanes=1):
	try:
	    spline = self.editor.selectedSpline
	    road = Road()
	    self.environment.addObject(road, 'MyRoad')
	    road.initialize(self.environment,spline,fLanes,bLanes,5.0)
	except ValueError:
	    print "Error: You must create a spline before adding a road. See app.help() for more info."

    def buildYInter(self):
	splines = [Spline.downcast(s) for s in self.environment.getObjectsByClassID('Spline').values()]
	roads = [Road() for s in splines]
	for road in roads: self.environment.addObject(road)

	for road, spline in zip(roads, splines):
    	    road.initialize(self.environment, spline, 1, 1, 5.0)

	yi = YIntersection() 
	self.environment.addObject(yi, 'MyIntersection')
	for road in roads: yi.hookStart(road)

	yi.computeGeometry(self.environment)
	self.environment.storeIntersectionBoundaries()

    def buildAlice(self):
	lg = LaneGrid()
	lg.partition(self.environment,5.0,Vector(-700,-700), Vector(700, 700))  
	self.environment.addObject(lg)
	self.setTrans()
	self.ec.initialize(self.environment)
	self.environment.addObject(self.ec,'Alice')

    def setTrans(self):
	print "setting alice translation:"
	self.ec.setTranslation(self.environment.yTranslate,self.environment.xTranslate)
	print self.environment.xTranslate
	print self.environment.yTranslate

    def save(self, filename):
        f = open(filename, 'w')
        f.write(str(self.environment))
        f.close()
        
    def load(self, filename):
        f = open(filename)
#	tempEnv = Environment(self.environment.skynetKey)
	self.environment = Environment.parse(f.read())
        self.editor.environment = self.environment
        
	f.close()
    
    def getSegs(self):
        self.environment.clearObjectsByClassID('Road')
        splines = [Spline.downcast(s) for s in self.environment.getObjectsByClassID('Spline').values()]
	self.segs = [Road() for s in splines]
	for road in self.segs: self.environment.addObject(road)
	for road, spline in zip(self.segs, splines): road.initialize(self.environment, spline, 1, 1, 6.0)
    
    def loadRNDF(self, filename):
	self.rndfIF.loadFile(filename)	
        self.rndfIF.centerAllWaypoints()
	self.environment.setTranslation(self.rndfIF.getxTranslate(),self.rndfIF.getyTranslate())
        self.rndfIF.buildSegInfos()
	numSegs = self.rndfIF.getNumSegs()
	laneInfo = self.rndfIF.getLaneInfos()
	pointsForSplines = [self.rndfIF.getCenterLineOfSeg(i) for i in range(numSegs)]
        for points in range(numSegs): self.editor.buildFromPoints(pointsForSplines[points], laneInfo[points])
        splines = [Spline.downcast(s) for s in self.environment.getObjectsByClassID('Spline').values()]
        roads = [Road() for s in splines]
        for road in roads: self.environment.addObject(road)
        for road, spline in zip(roads, splines): 
	    road.initialize(self.environment, spline, spline.numForwardLanes, spline.numReverseLanes, 6.0)

    def buildRoads(self):
        self.environment.clearObjectsByClassID("Road")
        splines = [Spline.downcast(s) for s in self.environment.getObjectsByClassID('Spline').values()]
        roads = [Road() for s in splines]
        for road in roads: self.environment.addObject(road)
        for road, spline in zip(roads, splines): road.initialize(self.environment, spline, spline.numForwardLanes, spline.numReverseLanes, 6.0)

    def buildRoadsIntersect(self):
        self.environment.clearObjectsByClassID("Road")
	splines = [Spline.downcast(s) for s in self.environment.getObjectsByClassID('Spline').values()]
	roads = [Road() for s in splines]
	for road in roads: self.environment.addObject(road)
        for road, spline in zip(roads, splines): road.initialize(self.environment, spline, 1, 1, 6.0)
	xi = XIntersection()
	self.environment.addObject(xi, 'MyIntersection')
	xi.hookEnd(roads[1])
	xi.hookEnd(roads[3])
	xi.hookStart(roads[0])
	xi.hookStart(roads[2])
	xi.computeGeometry(self.environment)
 
    def clear(self):
	self.environment.clearObjects()
	del self.rndfIF
	self.rndfIF = RNDFSimIF()

    def export(self):
	self.environment.exportEnv()

    def addCar(self,orientation=0):
	self.editor.addCar(orientation)

    def addBlock(self,length,width,orientation=0):
	self.editor.addBlockObs(length,width,orientation)

    def addCircle(self,radius):
	self.editor.addCircleObs(radius)

    def addLine(self):
	try:
	    spline = self.editor.selectedSpline
	    line = Line(spline)
	    self.environment.addObject(line)
	    print "Line added."
	except ValueError:
	    print "Error: You must create or select a spline before adding a line. See app.help() for more info."

    def debug(self):
	if self.environment.debug == True:
	    self.environment.debug = False
	    print "Debugging messages off"
	elif self.environment.debug == False:
	    self.environment.debug = True
	    print "Debugging messages on"

    def UT_tplanner_RR_SO_LegalPass(self):
	self.buildAlice()
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalPass(self):
	self.buildAlice()
	b = Obstacle(398999.63147800881-self.environment.xTranslate,3781263.8876663083-self.environment.yTranslate,2,5)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_IllegalUturn(self):
	self.buildAlice()
	b = Obstacle(398992.68318241555-self.environment.xTranslate,3781259.8189164037-self.environment.yTranslate,2,15)
	self.environment.addObject(b)

    def UT_tplanner_RR_SO_Partial(self):
	self.buildAlice()
	b = Obstacle(399000.68318241555-self.environment.xTranslate,3781267.8189164037-self.environment.yTranslate,3)
	self.environment.addObject(b)

    def exit(self):
        print "Exiting Trafsim. Please mouse over the Trafsim display."
	import sys
	self.clear()
	self.quitMessage = 1
	sys.exit()
   
    def help(self):
	print ""
	print "TRAFSIM HELP \n-----------"
	print "\n** Parameters in brackets, [], are optional **"
	print "\nNavigation:"
	print "Use z+left mouse to move."
	print "Use z+right mouse to zoom in and out."
	print "To pause, hold 'p' and click anywhere on the screen (make sure trafsim window is selected)"
	print "\nRNDFs:"
	print "To load an RNDF: \n    app.loadRNDF('filename')"
	print "Once loaded, to modify the segments to clean up messy segments, click and drag the waypoints of a segment until the spline follows the desired path. The rebuild the roads by typing:\n    app.buildRoads()"
	print "\nRoads:"
	print "To make a road, you must first make a spline. Press ctrl+left mouse to start the spline, and then shift+left mouse to continue it.\nAfter you have the points in place, type:\n    app.addRoad([number forward lanes],[number reverse lanes])"
	print "At any point in time the roads can be rebuilt:\n    app.buildRoads()"
        print "\nAlice:"
	print "To load Alice: \n    app.buildAlice()"
	print "To move Alice, hold 'a' and click the desired location on the screen."
	print "\nCars/Obstacles:"
	print "To add a stopped car, type:\n    app.addCar([orientation])"
	print "To add an obstacle, run:\n    app.addBlock(length,width,[orientation])\n or\n    app.addCircle(radius)"
	print "\nLines:"
	print "To add a line, use ctrl+click and shift+click to create a spline (only 2 points are necessary). Or you can select a spline that has already been created. Then run:\n    app.addLine()"
	print "\nMISC:"
	print "To clear all objects including the RNDF loaded into memory:\n     run app.clear()"
	print "To toggle debugging messages:\n    app.debug()"
	print "To change how often environment information is sent to mapper:\n    app.timestep = new timestep"
	print "\nSaving and Loading:"
	print "The standard file extension for a trafsim scenario/map is .ts"
	print "At any point you can save a trafsim scenario by typing:\n    app.save('filename.ts')"
	print "To load a saved .ts file:\n    app.load('filename.ts')"
	print "\nTo exit Trafsim: \n    app.exit()"
	print "\nFor more information, see the [[trafsim]] wiki page."
	print ""

    def run(self):
        self.viewport.open(800, 800, 'trafsim')
        
        lastTicks = time.time()
        
        try:
            while 1:
                event = ViewportEvent()
                while self.viewport.pollEvents(event):
                    if event.type == QUIT:
                        return
		    elif self.quitMessage == 1:
		        event.type = QUIT
		        return
                    else:
                        { EMPTY            : lambda event: None, 
                          REPAINT          : lambda event: None, 
                          MOUSEMOTION      : self.editor.onMouseMotion,
                          MOUSEBUTTONUP    : self.editor.onMouseUnclick,
                          MOUSEBUTTONDOWN  : self.editor.onMouseClick,
                          KEYUP            : self.editor.onKeyUp,
                          KEYDOWN          : self.editor.onKeyDown,
                         }[event.type](event) 
                 
                # call the functions passed into the thread
                while not self.callQueue.empty():
                    fn, args, kwds = self.callQueue.get()
                    fn(*args, **kwds)
                
                # update time
                newTicks = time.time()
                ticks = newTicks - lastTicks
                lastTicks = newTicks
             
                try: self.editor.perFrame(ticks * self.timingFactor)
                except AttributeError: pass

		time.sleep(self.timestep)
        #       if(ticks > .1):
		if not self.environment.paused:
                    self.environment.simulate(ticks * self.timingFactor)
                
		self.environment.draw()
            
                self.viewport.repaint()
        finally:
            # unload the graphics in advance, just in case
            del self.viewport
        
    def call(self, fn, *args, **kwds):
        self.callQueue.put( (fn, args, kwds) )

if __name__ == '__main__':
    import GenericApp

    app = GenericApp.GenericApp()
    app.start()

            
    
