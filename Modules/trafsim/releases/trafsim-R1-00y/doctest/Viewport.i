
%define DOC_VIEWPORT(method, string)
%feature("docstring", string) TrafSim::Viewport::method;
%enddef

%feature("docstring",
"""
This class encapsulates the functionality of a window that displays some portion of a 2D
OpenGL rendering environment. (see doxygen docs for more details).

>>> v = Viewport()
>>> v.open(100, 100, 'Test Viewport')
True
>>> # the following should be done in a loop:
>>> event = ViewportEvent()
>>> while v.pollEvents(event): pass  # in a real program, process events here
>>> # do some drawing or some such
>>> v.repaint()
True
>>> v.close()
True

""") TrafSim::Viewport;

DOC_VIEWPORT( open,
"""
Viewport.open() creates a new viewport window. Note that multiple calls to open() 
will only reset the parameters of the first window rather than opening a new one.
Note that most calls to Viewport functions will have to take place in the same thread
as the original call to Viewport.open().

>>> v = Viewport()
>>> v.repaint()    # can't do this before calling open()
False
>>> v.open(100, 100, 'Test Viewport')
True
>>> v.repaint()
True
>>> print v.getPhysicalPosition()
Vector(0, 0)
>>> v.setPhysicalPosition( Vector(100, 100) )
True
>>> print v.getPhysicalPosition()
Vector(100, 100)
>>> v.open(200, 200, 'Test Viewport')
True
>>> print v.getPhysicalPosition()
Vector(0, 0)
>>> v.close()
True

""")

DOC_VIEWPORT( close,
"""
This function can be called at any time to shutdown a viewport. However,
it should be made from the same thread from which the call to Viewport.open()
was made.

>>> v = Viewport()
>>> v.close()
True
>>> v.open(100, 100, 'Test Viewport')
True
>>> v.repaint()
True
>>> v.close()
True
>>> v.repaint()
False

""")

DOC_VIEWPORT( resize,
"""
Changes the on-screen dimensions of the viewport window. Also scales the physical dimensions accordingly,
so that there is no aspect ratio distortion. It is not safe to call this function from a different thread
than the one the viewport was opened in. This call will also fail if the window is not currently open.

>>> v = Viewport()
>>> v.resize(200, 200)
False
>>> v.open(300, 300, 'Test Viewport')
True
>>> v.getScreenWidth(), v.getScreenHeight()
(300, 300)
>>> print v.getPhysicalSize()
Vector(100, 100)
>>> v.resize(150, 600)
True
>>> v.getScreenWidth(), v.getScreenHeight()
(150, 600)
>>> print v.getPhysicalSize()
Vector(50, 200)
>>> v.close()
True

""")

DOC_VIEWPORT( setPhysicalSize,
"""
Changes the 'zoom' setting of the viewport. It is not safe to call this function from a different thread
than the one the viewport was opened in. This call will also fail if the window is not currently open.
	
>>> v = Viewport()
>>> v.setPhysicalSize( Vector(1,1) )
False
>>> v.open(100, 100, 'Test Viewport')
True
>>> v.setPhysicalSize( Vector(1,1) )
True
>>> print v.getPhysicalSize()
Vector(1, 1)
>>> v.close()
True

""")

DOC_VIEWPORT( setPhysicalPosition,
"""
Changes the 'pan' setting of the viewport. It is not safe to call this function from a different thread
than the one the viewport was opened in. This call will also fail if the window is not currently open.
	
>>> v = Viewport()
>>> v.setPhysicalPosition( Vector(100,100) )
False
>>> v.open(100, 100, 'Test Viewport')
True
>>> v.setPhysicalPosition( Vector(100,100) )
True
>>> print v.getPhysicalPosition()
Vector(100, 100)
>>> v.close()
True

""")

