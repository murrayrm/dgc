/**********************************************************
 **
 **  MAPPERMAIN.CC
 **
 **    Time-stamp: <2007-10-06 02:09:13 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Aug 18 05:24:04 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/



#include "Mapper.hh"
#include "MapperMain.hh"


using namespace std;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)



MapperMain::MapperMain()
{
  // initialize the console to NULL
  console = NULL;
  pause = 0;
  quit = 0;
	reset = 0;
  processTime = 0;
	memset(&state, 0, sizeof(state));
}

MapperMain::~MapperMain()
{}


int MapperMain::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
  this->moduleId = MODmapping;

  this->debugSubGroup = this->options.debug_subgroup_arg;
  this->recvSubGroup = this->options.recv_subgroup_arg;
  this->sendSubGroup = this->options.send_subgroup_arg;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");

  if (this->options.rndf_given){
    this->RNDFfilename.assign(this->options.rndf_arg);
  }else{
    return ERROR("Please specify an RNDF file using the option --rndf=filename");
  }
      
 
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  //cerr << "Line fusion " << (this->options.disable_line_fusion_flag ?
	//                          "disabled" : "enabled") << endl;
	
  return 0;
}

static char consoleTemplateMain[] =
"Mapper                                                                \n"
"                                                                           \n"
" Size of map:                %mapsize%                                     \n"
" Elements sent to planner:   %2planner%                                    \n"
" Clears sent to planner:     %clear2planner%                               \n"
" Avg time to add el:         %avg_add%  (%num_add% total)                  \n"
" Avg time to fuse el:        %avg_fuse% (%num_fuse% total)                 \n"
" Avg time to assoc el:       %avg_assoc% (%num_assoc% total)               \n"
" Site frame offset:          %siteXoffset%, %siteYoffset%, %siteAoffset%   \n"
"                                                                           \n"
"  PERCEPTOR      total     IDtoINDEX     assoc.    new     clear           \n"
" Left Bumper:    %total0%  %IDtoINDEX0%  %assoc0%  %new0%  %clear0%        \n"
" Center Bumper:  %total1%  %IDtoINDEX1%  %assoc1%  %new1%  %clear1%        \n"
" Right Bumper:   %total2%  %IDtoINDEX2%  %assoc2%  %new2%  %clear2%        \n"
" Rear Bumper:    %total3%  %IDtoINDEX3%  %assoc3%  %new3%  %clear3%        \n"
" Left Roof:      %total4%  %IDtoINDEX4%  %assoc4%  %new4%  %clear4%        \n"
" Riegl:          %total5%  %IDtoINDEX5%  %assoc5%  %new5%  %clear5%        \n"
" Med Stereo:     %total6%  %IDtoINDEX6%  %assoc6%  %new6%  %clear6%        \n"
" Long Stereo:    %total7%  %IDtoINDEX7%  %assoc7%  %new7%  %clear7%        \n"
" RADAR:          %total8%  %IDtoINDEX8%  %assoc8%  %new8%  %clear8%        \n"
" total:          %total9%  %IDtoINDEX9%  %assoc9%  %new9%  %clear9%        \n"
"                                                                           \n";

static char consoleTemplateTimings[] = 
" Line fusion time:           %time_linefus%                                \n"
" Update graph time:          %time_updgraph%                               \n"
" Groundstrike time:          %time_gsfilt%                                 \n"
" Update map time:            %time_updmap%                                 \n"
" Send RNDF time:             %time_rndf%                                   \n"
"                                                                           \n";

static char consoleTemplateUI[] = 
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%RESET%]                                                   \n"
"                                                                           \n"
"%stdout%                                                                   \n";

static char consoleTemplate[sizeof(consoleTemplateMain) +
                            sizeof(consoleTemplateTimings) +
                            sizeof(consoleTemplateUI)];

// Initialize console display
int MapperMain::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // conditionally add the timings to the console template
  unsigned long templSize = sizeof(consoleTemplateMain) - 1;
  memcpy(consoleTemplate, consoleTemplateMain, templSize);
  if (!options.no_timings_flag) {
      memcpy(consoleTemplate + templSize, consoleTemplateTimings, sizeof(consoleTemplateTimings));
      templSize += sizeof(consoleTemplateTimings) - 1;
  }
  memcpy(consoleTemplate + templSize, consoleTemplateUI, sizeof(consoleTemplateUI));

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%RESET%", " RESET ", "Rr",
									 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, 
							"Key = %d, Send Subgroup = %d, Receive Subgroup = %d",
							this->skynetKey, this->sendSubGroup, this->recvSubGroup);
 
  return 0;
}


// Finalize sparrow display
int MapperMain::finiConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}

// Initialize skynet
int MapperMain::initComm()
{

  //--------------------------------------------------
  // initializing state talker
  //--------------------------------------------------
  if(!options.no_state_flag) {
    stateTalker = new CStateWrapper(skynetKey);
		state = stateTalker->getstate();
  }

 
  //--------------------------------------------------
  // join sensnet for process handling
  //--------------------------------------------------
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");
   


 
  return 0;
}

int MapperMain::finiComm()
{
	if(!options.no_state_flag) {
		delete stateTalker;
	}

  if (this->sensnet)
    {
      sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
      sensnet_disconnect(this->sensnet);
      sensnet_free(this->sensnet);
      this->sensnet = NULL;
    }


  return 0;
}

int MapperMain::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  if (request.quit) {
    MSG("remote quit request");
  }
  return 0;
}


// Handle button callbacks
int MapperMain::onUserQuit(cotk_t *console, MapperMain *self, const char *token) 
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int MapperMain::onUserPause(cotk_t *console, MapperMain *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle button callbacks
int MapperMain::onUserReset(cotk_t *console, MapperMain *self, const char *token)
{
  self->reset = true;
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{

	Mapper  mapper;
	MapperMain mappermain;
	
	if (mappermain.parseCmdLine(argc, argv) != 0)
      return -1;
	cout <<"starting mapper" << endl;

	if (mappermain.options.send_subgroup_arg ==mappermain.options.recv_subgroup_arg){
		cerr <<"Error in Mapper: must specify different send and receive subgroups." << endl;
		return -1;
	}
		

  MSG("Init Comm");
	mappermain.initComm();

  // Fill out the send subgroup number
  mapper.sendSubGroup = mappermain.sendSubGroup;
  // Fill out the recv subgroup number
  mapper.recvSubGroup = mappermain.recvSubGroup;
  // Fill out the recv subgroup number
  mapper.debugSubGroup = mappermain.debugSubGroup;
  mapper.initComm(mappermain.skynetKey);
	

  mapper.enableGroundstrikeFiltering(mappermain.options.enable_groundstrike_filtering_flag);
  mapper.enableGroundstrikeDisplay(mappermain.options.show_groundstrike_map_flag);

  // initialize mapper

  // need to enable/disable line fusion before init()
  mapper.disableLineFusion(mappermain.options.disable_line_fusion_flag);
  mapper.enableLineFusionCompat(mappermain.options.line_fusion_compat_flag);

  mapper.init(mappermain.RNDFfilename);
  mapper.setLocalFrameOffset(mappermain.state);
  mapper.disableObsFusion(mappermain.options.disable_obs_fusion_flag);
  mapper.setFusionTimeThresh(mappermain.options.fusion_time_thresh_arg);
  mapper.plannerOutputDisabled=false;
  mapper.decayAgeThresh = mappermain.options.decay_thresh_arg;
  mapper.assocTimeThresh = mappermain.options.assoc_thresh_arg;
  mapper.fusionRate = mappermain.options.fusion_rate_arg;
  mapper.sendGraphEnabled = mappermain.options.send_graph_flag;


  mapper.logging = mappermain.options.log_flag;
  mapper.logLevel = mappermain.options.log_level_arg;
  mapper.logPath = mappermain.options.log_path_arg;



  if (!mappermain.options.disable_console_flag ){
    MSG("Init Console");
    if (mappermain.initConsole() != 0){
      return -1;
    }
  }


  cerr << "Listening for map elements" << endl;

  while (!mappermain.quit){
    // Do heartbeat occasionally
    if (DGCgettime() - mappermain.processTime > 500000)
      {
        mappermain.processTime = DGCgettime();
        mappermain.updateProcessState();  
      }
 
      

    // update the console

    if (mappermain.console){
      if (DGCgettime() - mappermain.consoleTime > 100000){
        cotk_update(mappermain.console);
	char token[64];

	//print general stats
	  snprintf(token, sizeof(token), "%%mapsize%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.map.usedIndices.size());

	  snprintf(token, sizeof(token), "%%2planner%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.numSentToPlanner);

	  snprintf(token, sizeof(token), "%%clear2planner%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.clearSentToPlanner);

	  snprintf(token, sizeof(token), "%%avg_add%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      1.0*mapper.totalTimeAdd/mapper.totalElAdd);

	  snprintf(token, sizeof(token), "%%num_add%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.totalElAdd);

	  snprintf(token, sizeof(token), "%%max_add%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.maxTimeAdd);

	  snprintf(token, sizeof(token), "%%avg_fuse%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      1.0*mapper.totalTimeFuse/mapper.totalElFuse);

	  snprintf(token, sizeof(token), "%%num_fuse%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.totalElFuse);

	  snprintf(token, sizeof(token), "%%max_fuse%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.maxTimeFuse);

	  snprintf(token, sizeof(token), "%%avg_assoc%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      1.0*mapper.totalTimeAssoc/mapper.totalElAssoc);

	  snprintf(token, sizeof(token), "%%num_assoc%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.totalElAssoc);

	  snprintf(token, sizeof(token), "%%max_assoc%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.maxTimeAssoc);

	  snprintf(token, sizeof(token), "%%avg_clear%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      1.0*mapper.totalTimeClear/mapper.totalElClear);

	  snprintf(token, sizeof(token), "%%num_clear%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%7d",
		      mapper.totalElClear);

	  snprintf(token, sizeof(token), "%%max_clear%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%6d",
		      mapper.maxTimeClear);

	  snprintf(token, sizeof(token), "%%siteXoffset%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      mapper.map.loc2site.x);

	  snprintf(token, sizeof(token), "%%siteYoffset%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      mapper.map.loc2site.y);

	  snprintf(token, sizeof(token), "%%siteAoffset%%");
	  cotk_printf(mappermain.console, token, A_NORMAL, "%3.3f",
		      mapper.map.loc2site.ang);


          // show benchmarks
          if (!mappermain.options.no_timings_flag) {
              cotk_printf(mappermain.console, "%time_linefus%", A_NORMAL, "%.6g ms",
                          double(mapper.elapsedLineFusion)/1e3);

              cotk_printf(mappermain.console, "%time_updgraph%", A_NORMAL, "%.6g ms",
                          double(mapper.elapsedUpdateGraph)/1e3);

              cotk_printf(mappermain.console, "%time_gsfilt%", A_NORMAL, "%.6g ms",
                          double(mapper.elapsedGroundStrike)/1e3);

              cotk_printf(mappermain.console, "%time_updmap%", A_NORMAL, "%.6g ms",
                          double(mapper.elapsedUpdateMap)/1e3);

              cotk_printf(mappermain.console, "%time_rndf%", A_NORMAL, "%.6g ms",
                          double(mapper.elapsedSendRndf)/1e3);
          }


	  int sum0, sum1, sum2, sum3;
	  sum0 = 0;
	  sum1 = 0;
	  sum2 = 0;
	  sum3 = 0;
	  for(int i = 0; i < FUSED_FROM_MAPPER; i ++) {
	    sum0 += mapper.statArray[i][0];
	    sum1 += mapper.statArray[i][1];
	    sum2 += mapper.statArray[i][2];
	    sum3 += mapper.statArray[i][3];
	  }
	  mapper.statArray[FUSED_FROM_MAPPER][0] = sum0;
	  mapper.statArray[FUSED_FROM_MAPPER][1] = sum1;
	  mapper.statArray[FUSED_FROM_MAPPER][2] = sum2;
	  mapper.statArray[FUSED_FROM_MAPPER][3] = sum3;


	//print the data per perceptor			
	for(int i=0; i<NUM_PERCEPTORS; i++) {
	  snprintf(token, sizeof(token), "%%total%d%%", i);
	  cotk_printf(mappermain.console, token, A_NORMAL, "%d",
		      mapper.statArray[i][0]);

	  snprintf(token, sizeof(token), "%%IDtoINDEX%d%%", i);
	  cotk_printf(mappermain.console, token, A_NORMAL, "%d",
		      mapper.statArray[i][1]);

	  snprintf(token, sizeof(token), "%%assoc%d%%", i);
	  cotk_printf(mappermain.console, token, A_NORMAL, "%d",
		      mapper.statArray[i][2]);

	  snprintf(token, sizeof(token), "%%new%d%%", i);
	  cotk_printf(mappermain.console, token, A_NORMAL, "%d",
		      mapper.statArray[i][3]);

	  snprintf(token, sizeof(token), "%%clear%d%%", i);
	  cotk_printf(mappermain.console, token, A_NORMAL, "%d",
		      mapper.statArray[i][4]);

	}

      }
    }
    if (mappermain.pause){
      // disable pause for now as it causes spread errors
      //   usleep(100000);
      //    continue;
    }

    if (DGCgettime() - mappermain.stateTime > 100000){
      if (mappermain.options.no_state_flag) {
				// update MapPrior delta using the state in the current object
				if (mapper.sensedEl.state.timestamp!=0){
					mappermain.state = mapper.sensedEl.state;
				}
      }else{
				mappermain.state = mappermain.stateTalker->getstate();
      }
      mapper.setLocalFrameOffset(mappermain.state);			
    }

    mapper.mainLoop();

  
  }
    

  
	
mapper.fini();
mapper.finiComm();
mappermain.finiConsole();
mappermain.finiComm();
	
return 0;
}

