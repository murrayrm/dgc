/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-03-18 17:13:23 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;


Mapper::Mapper()
{}


Mapper::~Mapper()
{}

int Mapper::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
	this->noconsole_flag = this->options.noconsole_flag;
	
	this->RNDFfilename = this->options.rndf_arg;
	cout << "Filename in = "  << this->RNDFfilename << endl;

  // Fill out the send subgroup number
  this->sendSubGroup = this->options.send_subgroup_arg;
 
  // Fill out the recv subgroup number
  this->recvSubGroup = this->options.recv_subgroup_arg;
 
  // Fill out the skynet key
  if (this->options.snkey_given)
    this->skynetKey = this->options.snkey_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
	
	return 0;
}

static char *consoleTemplate =
"Mapper Lite                                                                \n"
"                                                                           \n"
"Skynet Info:   %spread%                                                    \n"
"Message Stats: %elements%                                                  \n"
"Filter Info: %filter%                                                      \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%|%RESET%]                                                    \n"
"                                                                           \n"
"%stdout%                                                                   \n";

// Initialize console display
int Mapper::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
	cotk_bind_button(this->console, "%RESET%", " RESET ", "Rr",
									 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, 
							"Key = %d, Send Subgroup = %d, Receive Subgroup = %d",
							this->skynetKey, this->sendSubGroup, this->recvSubGroup);
  cotk_printf(this->console, "%filter%", A_NORMAL, 
              "no filter now");

  return 0;
}


// Finalize sparrow display
int Mapper::closeConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}


// Initialize skynet
int Mapper::initComm()
{

	this->initRecvMapElement(skynetKey);
	this->initSendMapElement(skynetKey);
  return 0;
}

int Mapper::send(MapElement* p_el)
{
	totalSent++;
	return(this->sendMapElement(p_el,sendSubGroup));
}

int Mapper::recv(MapElement* p_el)
{
	int numbytes = recvMapElementNoBlock(p_el,recvSubGroup);
	if (numbytes>0){
    totalReceived++;
    return numbytes;
	}
	return 0;
}


// Handle button callbacks
int Mapper::onUserQuit(cotk_t *console, Mapper *self, const char *token) 
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int Mapper::onUserPause(cotk_t *console, Mapper *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle button callbacks
int Mapper::onUserReset(cotk_t *console, Mapper *self, const char *token)
{
  self->resetMap();
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle fake stop button
int Mapper::onUserFake(cotk_t *console, Mapper *self, const char *token)
{
      
  //MSG("creating element data");
	MapElement el;
	self->send(&el);
  return 0;
}

int Mapper::resetMap()
{
  map.data.clear();
	return 0;
}




// Main program thread
int main(int argc, char **argv)
{
	bool DEBUG=0;

	Mapper *mapper;
	mapper = new Mapper();
  assert(mapper);

	mapper->parseCmdLine(argc, argv);
	if (mapper->sendSubGroup ==mapper->recvSubGroup){
		cerr <<"Error in Mapper: must specify different send and receive subgroups." << endl;
		return -1;
	}
		

  cout << "Connecting to State " << endl;
  CStateWrapper stateclient(mapper->skynetKey);
  cout << "Got State " << endl;

	mapper->initComm();
	mapper->resetMap();
	
	if (!mapper->noconsole_flag)
		if (mapper->initConsole() != 0)
			return -1;


	MapElement el;

	
	mapper->graphTalkerPtr = new SkynetTalker<Graph>(mapper->skynetKey,SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner); 
	mapper->map.prior.loadRNDF(mapper->RNDFfilename.c_str());
	RNDF rndf;

	char* fname = (char*) mapper->RNDFfilename.c_str();
	if (!rndf.loadFile(fname)){
    cerr << "Error:  Unable to load RNDF file " << mapper->RNDFfilename
         << ", exiting program" << endl;
    exit(1);
	}

	rndf.assignLaneDirection();
	
	mapper->rndfGraphPtr = new Graph(&rndf);
	cout << "about to send a graph" << endl;
	mapper->rndfGraphPtr->print();
	cout << "graph printed" << endl;
	mapper->graphTalkerPtr->send(mapper->rndfGraphPtr);
	cout << endl << "sent a graph!" << endl;
	
		


  
  VehicleState state;

  state = stateclient.getstate();
 
  point2 statedelta, stateNE, statelocal;
  
  statelocal.set(state.localX,state.localY);
  stateNE.set(state.utmNorthing,state.utmEasting);
  statedelta = stateNE-statelocal;
  cout << "state delta = " << statedelta << " northing = " << stateNE << " local = " << statelocal << endl;
  cout << "predelta = " << mapper->map.prior.delta << endl;
  mapper->map.prior.delta = statedelta;
  cout << "postdelta = " << mapper->map.prior.delta << endl;

	
	int numelements = mapper->map.prior.data.size();
	cout << "elements = " << numelements << endl;
	for (int i = 0 ; i < numelements; ++i){
		mapper->map.prior.getEl(i,el);
    //TEMP
    //el = mapper->map.prior.data[i];
    //	mapper->send(&el);
    mapper->sendMapElement(&el,-3);
	}
  unsigned long long thistime, lasttime ;
  DGCgettime(lasttime);
  
  int dtime;
  point2 cpt;
  PointLabel ptlabel;
	cerr << "Listening for map elements" << endl;
	bool recvflag = false;
	while (!mapper->quit){
		if (mapper->recv(&el)){ 
      mapper->map.addEl(el);
			mapper->send(&el);
			recvflag = true;
		}else{
		
			if (recvflag){
				if (DEBUG){
					el.print();
				}

				recvflag = false;
			}
			
      DGCgettime(thistime);
      dtime = thistime-lasttime; 
  
      if (dtime>4000000){
        lasttime = thistime;

        for (unsigned i=0;i< mapper->map.data.size();++i){
          
          
          el=mapper->map.data[i];
          mapper->sendMapElement(&el,-3);
            cpt = el.center;
          if (mapper->map.getLastPointID(ptlabel,cpt)==0){
            mapper->rndfGraphPtr->addObstacle(ptlabel.segment,ptlabel.lane,ptlabel.point);
            cout << "-Adding cost to graph after waypt " << ptlabel<< endl;
          }
        }
      	mapper->graphTalkerPtr->send(mapper->rndfGraphPtr);
        cout << endl << "sent a graph!" << endl;
	
      }

			usleep(1000);
			if (mapper->console){
				cotk_update(mapper->console);			
				cotk_printf(mapper->console, "%elements%", A_NORMAL, "%d received, %d sent" 
										, mapper->totalReceived, mapper->totalSent);
				if (mapper->pause){
					usleep(0);
					continue;
				}
			}
		}	
	}

	mapper->closeConsole();
	return 0;
}







	
	

	
		
	
	
	
		


