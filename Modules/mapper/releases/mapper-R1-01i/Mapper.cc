/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-07-17 06:15:45 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


Mapper::Mapper()
{ //--------------------------------------------------
  // initialize time offsets
  //--------------------------------------------------
  graphTime = 0;
  processTime = 0;

  // initialize the console to NULL
  console = NULL;
  pause = 0;
  quit = 0;
  
}

Mapper::~Mapper()
{}

int Mapper::sendMapElementToBoth(MapElement * el)
{
  int ret = mapTalker.sendMapElement(el,sendSubGroup);
  if (ret > 0 && debugSubGroup != 0)
    ret = mapTalker.sendMapElement(el,debugSubGroup);
  return ret;
}

int Mapper::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
   this->moduleId = MODmapping;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");

  if (this->options.rndf_given){
    this->RNDFfilename.assign(this->options.rndf_arg);
  }else{
    return ERROR("Please specify an RNDF file using the option --rndf=filename");
  }
  cout << "Filename in = "  << this->RNDFfilename<< endl;
    
  // Fill out the send subgroup number
  this->sendSubGroup = this->options.send_subgroup_arg;
 
  // Fill out the recv subgroup number
  this->recvSubGroup = this->options.recv_subgroup_arg;

  // Fill out the recv subgroup number
  this->debugSubGroup = this->options.debug_subgroup_arg;
 
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
	
	return 0;
}

static char *consoleTemplate =
"Mapper                                                                \n"
"                                                                           \n"
"Skynet Info:   %spread%                                                    \n"
"Message Stats: %elements%                                                  \n"
"Filter Info: %filter%                                                      \n"
"Map Info: %map%                                                            \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%|%RESET%]                                                    \n"
"                                                                           \n"
"%stdout%                                                                   \n";

// Initialize console display
int Mapper::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
	cotk_bind_button(this->console, "%RESET%", " RESET ", "Rr",
									 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, 
							"Key = %d, Send Subgroup = %d, Receive Subgroup = %d",
							this->skynetKey, this->sendSubGroup, this->recvSubGroup);
 
  return 0;
}


// Finalize sparrow display
int Mapper::finiConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}


// Initialize skynet
int Mapper::initComm()
{

  //--------------------------------------------------
  // initializing state talker
  //--------------------------------------------------
  stateTalker = new CStateWrapper(skynetKey);

  //--------------------------------------------------
  // initialize graphtalker
  //--------------------------------------------------
  graphTalkerPtr = new SkynetTalker<Graph>(skynetKey,SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner); 
	
  
  //--------------------------------------------------
  // join sensnet for process handling
  //--------------------------------------------------
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");
  cout << "Done with initComm " << endl;
 

 //--------------------------------------------------
  // initialize map elment talker
  //--------------------------------------------------
	this->mapTalker.initRecvMapElement(skynetKey,recvSubGroup);
	this->mapTalker.initSendMapElement(skynetKey);

 
  return 0;
}

int Mapper::finiComm()
{
  delete graphTalkerPtr;
  delete stateTalker;

  if (this->sensnet)
  {
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
    sensnet_disconnect(this->sensnet);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  return 0;
}


int Mapper::init()
{	
 
  
  //--------------------------------------------------
  // initialize the map prior data
  //--------------------------------------------------
  if (!map.loadRNDF(RNDFfilename.c_str())){
    cerr << "Error:  Unable to load RNDF file to map " << RNDFfilename
         << ", exiting program" << endl;
    exit(1);
    }


  //--------------------------------------------------
  // initialize the rndf graph data
  //--------------------------------------------------
	char* fname = (char*) RNDFfilename.c_str();
	if (!rndf.loadFile(fname)){
    cerr << "Error:  Unable to load RNDF file to graph" << RNDFfilename
         << ", exiting program" << endl;
    exit(1);
	}
	rndf.assignLaneDirection();
  //initialize graph interface with mplanner
	rndfGraphPtr = new Graph(&rndf);
  //send a graph
  graphTalkerPtr->send(rndfGraphPtr);


  
  //--------------------------------------------------
  // Set prior map data offset to local frame from state
  //--------------------------------------------------
  state = stateTalker->getstate();

  point2 statedelta, stateNE, statelocal;
  
  statelocal.set(state.localX,state.localY);
  stateNE.set(state.utmNorthing,state.utmEasting);
  statedelta = stateNE-statelocal;
  //  cout << "state delta = " << statedelta << " northing = " << stateNE << " local = " << statelocal << endl;
   map.setTransform( statedelta);
 


  
  return 0;
}		

int Mapper::fini()
{
  delete rndfGraphPtr;
  return 0;
}


int Mapper::updateGraph()
{
  point2 cpt;
  PointLabel ptlabel;

  unsigned int i;
  for (i=0;i< map.data.size();++i){
    outputEl=map.data[i];
    cpt = outputEl.center;
    if (map.getLastPointID(ptlabel,cpt)==0){
      rndfGraphPtr->addObstacle(ptlabel.segment,ptlabel.lane,ptlabel.point);
    }
  }
  graphTalkerPtr->send(rndfGraphPtr);
  return 0;
}

int Mapper::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


int Mapper::resetMap()
{
  map.data.clear();
	return 0;
}


// Handle button callbacks
int Mapper::onUserQuit(cotk_t *console, Mapper *self, const char *token) 
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int Mapper::onUserPause(cotk_t *console, Mapper *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle button callbacks
int Mapper::onUserReset(cotk_t *console, Mapper *self, const char *token)
{
  self->resetMap();
  return 0;
}

// Handle fake stop button
int Mapper::onUserFake(cotk_t *console, Mapper *self, const char *token)
{
      
  //MSG("creating element data");
	MapElement el;
	self->sendMapElementToBoth(&el); // send to sendSubGroup and debugSubGroup
  return 0;
}


int Mapper::updateMap()
{

  //get map elements
		if (mapTalker.recvMapElementBlock(&inputEl,recvSubGroup) <=0){
      // keep going if there's bad data, but don't add to map
      return 0;
    }    
    totalReceived++;

    if (inputEl.isObstacle() && !options.disable_fusion_flag){
      fuseMapElement(inputEl);
      sendMapElementToBoth(&inputEl); // send to sendSubGroup and debugSubGroup
    } else {
      sendMapElementToBoth(&inputEl); // send to sendSubGroup and debugSubGroup
      map.addEl(inputEl); 
    }
    return 0;
}


int Mapper::fuseMapElement(MapElement & fusedEl)
{
  int i;
  int mapsize = (int) map.data.size();
  point2 basePos;
  point2 fusedPos(fusedEl.position);
  

  double mindist = options.fusion_dist_thresh_arg;
  int minindex = -1;
 
  for (i=0; i<mapsize;++i){
    
    basePos = map.data[i].position;
    double fusedDist = fusedPos.dist(basePos);
    if (fusedDist < mindist){
      minindex = i;
      mindist = fusedDist;
    }
  }

  if (minindex>=0){
    // only fuse obstacles at the moment (fusing static with dynamic obs is ok)
    if (fusedEl.id != map.data[minindex].id && map.data[minindex].isObstacle()){
      if (fusedEl.isOverlap(map.data[minindex])){
        // just make sure we did things right. REMOVE ME when fusing lines too
        assert(fusedEl.isObstacle());
        assert(map.data[minindex].isObstacle());

        // use the existing id in the map
        fusedEl.id = map.data[minindex].id;
        // increase confidence for the fused element
        fusedEl.conf = fusedEl.conf+map.data[minindex].conf;

        // compute the difference in times sensed
        fusedDeltaTime = (int)((fusedEl.state.timestamp-map.data[minindex].state.timestamp )/1000); 
        // if the time difference is small, we can be fairly sure that the 
        // older data isn't stale, and can use it to fuse
        if (fusedDeltaTime < options.fusion_time_thresh_arg){
          // use the velocity measurement with the lower uncertainty
          if (fusedEl.velocity.max_var > map.data[minindex].velocity.max_var){
            fusedEl.velocity = map.data[minindex].velocity;
          }

          // use the geometry estimate with the lower uncertainty
          if (fusedEl.position.max_var > map.data[minindex].position.max_var){
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
          }
        }
        // update the local map
        map.data[minindex] = fusedEl;
        
        totalFused++;
        return 1;
      }
    }
  }
  map.addEl(fusedEl);
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
	Mapper *mapper;
	mapper = new Mapper();
  assert(mapper);

	mapper->parseCmdLine(argc, argv);


	if (mapper->sendSubGroup ==mapper->recvSubGroup){
		cerr <<"Error in Mapper: must specify different send and receive subgroups." << endl;
		return -1;
	}
		

   MSG("Init Comm");
	mapper->initComm();
	mapper->resetMap();
	if (!mapper->options.disable_console_flag ){
    MSG("Init Console");
		if (mapper->initConsole() != 0){
			return -1;
    }
  }
  
  // initialize mapper
  mapper->init();



	cerr << "Listening for map elements" << endl;

	while (!mapper->quit){
    // Do heartbeat occasionally
    if (DGCgettime() - mapper->processTime > 500000)
      {
        mapper->processTime = DGCgettime();
        mapper->updateProcessState();
      }
    // update and send the graph roughly every 4 seconds
    if (DGCgettime() - mapper->graphTime > 4000000){
      mapper->graphTime = DGCgettime();
      mapper->updateGraph();

      if (mapper->debugSubGroup!=0){
        for (unsigned int i =0;i<mapper->map.prior.fulldata.size();++i){
          if(mapper->map.prior.getElFull(mapper->outputEl,i))
            mapper->mapTalker.sendMapElement(&mapper->outputEl, mapper->debugSubGroup);
        }
      }
    }

      
    // update the console

    if (mapper->console){
      if (DGCgettime() - mapper->consoleTime > 100000){
        cotk_update(mapper->console);			
        if (mapper->debugSubGroup !=0)
          cotk_printf(mapper->console, "%elements%", A_NORMAL, "%d received, debug to subgroup %d ", mapper->totalReceived, mapper->debugSubGroup);
        else
          cotk_printf(mapper->console, "%elements%", A_NORMAL, "%d received, no debug ", mapper->totalReceived);


        cotk_printf(mapper->console, "%filter%", A_NORMAL, "%d fused, %d msec               " 
                    , mapper->totalFused, mapper->fusedDeltaTime);

        cotk_printf(mapper->console, "%map%", A_NORMAL, " size = %d    " 
                    , mapper->map.data.size());


      }
    }
    if (mapper->pause){
      // disable pause for now as it causes spread errors
      //   usleep(100000);
      //    continue;
    }

    // update the map
    if (mapper->updateMap() !=0)
      break;
    
  }	
	
  mapper->fini();
	mapper->finiConsole();
  mapper->finiComm();
	return 0;
}







	
	

	
		
	
	
	
		


