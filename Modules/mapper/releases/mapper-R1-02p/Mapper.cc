/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-10-24 15:03:49 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;
/*
// Useful message macro
#define MSG(fmt, ...) (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
// Useful error macro
#define ERROR(fmt, ...) (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
*/

Mapper::Mapper()
{ //--------------------------------------------------
  // initialize time offsets
  //--------------------------------------------------
  obsFusionDisabled = false;
  lineFusionDisabled = false;
  lineFusionCompat = false;
  groundstrikeFilteringEnabled = false;
  groundstrikeDisplayEnabled = false;
  senseSegment = false;
  graphTime = 0;
  decayTime = 0;
  fusionTime = 0;
  fusionTimeThresh = 500;
  fusionRate = 250000;
  sendSubGroup = 1;
  recvSubGroup = 0;
  debugSubGroup = -2;
	sendGraphEnabled = false;
  debugPriorIndex = 0;
  //initialize console variables
  for(int i = 0; i < NUM_PERCEPTORS; i++) {
    for(int j = 0; j < 6; j++) {
      statArray[i][j] = 0;
    }
  }
	logging = false;
 
  totalTimeAdd = 0;
  totalTimeFuse = 0;
  totalTimeClear = 0;
  totalTimeAssoc = 0;
  lastClearedStats = DGCgettime();
  totalElAdd = 0;
  maxTimeAdd = 0;
  totalElFuse = 0;
  maxTimeFuse = 0;
  totalElClear = 0;
  maxTimeClear = 0;
  totalElAssoc = 0;
  maxTimeAssoc = 0;
  numSentToPlanner = 0;
  clearSentToPlanner = 0;

  // benchmark variables
  elapsedUpdateGraph = 0;
  elapsedLineFusion = 0;
  elapsedGroundStrike = 0;
  elapsedUpdateMap = 0;
  elapsedSendRndf = 0;

}

Mapper::~Mapper()
{}


// Initialize comm
int Mapper::initComm(int snkey)
{
  //--------------------------------------------------
  // initialize graphtalker
  //--------------------------------------------------
  graphTalkerPtr = new SkynetTalker<Graph>(snkey,SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner); 
  //--------------------------------------------------
  // initialize map elment talker
  //--------------------------------------------------
  this->mapTalker = new CMapElementTalker();  
  this->mapTalker->initRecvMapElement(snkey,recvSubGroup);
  this->mapTalker->initSendMapElement(snkey);

  return 0;  
}

int Mapper::finiComm()
{

  delete mapTalker;
  delete graphTalkerPtr;
  return 0;
}

// Initialize skynet

void Mapper::setLocalFrameOffset(VehicleState &state){
  map.setLocalToGlobalOffset(state);
}



void Mapper::disableLineFusion(bool disableflag = true)
{
  if (lineFusionCompat)
    map.enableLineFusion(!disableflag);
  else 
    map.enableLineFusion(false); /* done in mapper main loop, not in map */
    
  map.prior.internalLineFusion = !disableflag;
  lineFusionDisabled = disableflag;
}

void Mapper::enableLineFusionCompat(bool enableflag)
{
  lineFusionCompat = enableflag;
  if (enableflag)
    map.enableLineFusion(!lineFusionDisabled);
  else
    map.enableLineFusion(false); /* done in mapper main loop, not in map */
}


void Mapper::disableObsFusion(bool disableflag = true)
{
  obsFusionDisabled = disableflag;
}

void Mapper::enableGroundstrikeFiltering(bool enableflag = false)
{
  groundstrikeFilteringEnabled = enableflag;
}

void Mapper::enableGroundstrikeDisplay(bool enableflag = false)
{
  groundstrikeDisplayEnabled = enableflag;
}

void Mapper::enableSenseSegment(bool enableflag = false)
{
  senseSegment = enableflag;
}

int Mapper::init(string RNDFname)
{ 

  // enable or disable line fusion
  //initialize logging (stolen from planner code)
  if(logging) {
    ostringstream oss; 
    struct stat st;
    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t)); 
    oss << logPath << "mapper." << timestr << ".log";
    logFilename = oss.str();
    string suffix = "";

    /* if it exists already, append .1, .2, .3 ... */
    for (int i = 1; stat((logFilename + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFilename += suffix;

    Log::setGenericLogFile(logFilename.c_str());
    Log::setVerboseLevel(logLevel);
    Log::getStream(4)<<"opened file"<<endl;
  }

 
  
  //--------------------------------------------------
  // initialize the map prior data
  //--------------------------------------------------
  if (!map.loadRNDF(RNDFname.c_str())){
    cerr << "Error:  Unable to load RNDF file to map " << RNDFname
         << ", exiting program" << endl;
    exit(1);
  }


  //--------------------------------------------------
  // initialize the rndf graph data
  //--------------------------------------------------
  char* fname = (char*) RNDFname.c_str();
  if (!rndf.loadFile(fname)){
    cerr << "Error:  Unable to load RNDF file to graph" << RNDFname
         << ", exiting program" << endl;
    exit(1);
  }
  rndf.assignLaneDirection();
  //initialize graph interface with mplanner
  rndfGraphPtr = new Graph(&rndf);
  //send a graph
  graphTalkerPtr->send(rndfGraphPtr);

  //initialize the elevation map
  if(groundstrikeFilteringEnabled) {
    roadMap = new RoadMap();

    //TODO: actually have this initialize off of mapper's moduleID, snKey, etc...
    //TODO: actually do clean up for these...
    MSG("intializing road map");
    roadMap->Init();
    MSG("initializing elevation map");
    roadMap->roadMapInit();
    MSG("initializing sensnet");
    roadMap->initSensnet();
    DGCgettime(timeRoadMapUpdated);

    //TODO: get this working
    if(groundstrikeDisplayEnabled) {
      //roadMap->initDisplay();
    }
  }
  return 0;
}   

int Mapper::fini()
{
  delete rndfGraphPtr;
  return 0;
}


int Mapper::updateGraph()
{
  point2 cpt;
  PointLabel ptlabel;
  MapElement tmpEl;

  unsigned int i;
  for (i=0;i< map.data.size();++i){
    tmpEl=map.data[i];
    cpt = tmpEl.center;
    if (map.getLastPointID(ptlabel,cpt)==0){
      rndfGraphPtr->addObstacle(ptlabel.segment,ptlabel.lane,ptlabel.point);
    }
  }
  graphTalkerPtr->send(rndfGraphPtr);
  return 0;
}


int Mapper::resetMap()
{
  map.data.clear();
  return 0;
}

int Mapper::decayMap()
{

  unsigned long long currentTime = DGCgettime();
  long long dtime = 0;
  long long dtime2 = 0;
  int i, j;
  MapElement mapEl;
  bool perceptorsClear, dataOld;

  for(i = map.usedIndices.size()-1; i >=0; i--) {
    j = map.usedIndices.at(i);

    perceptorsClear = true;
    dataOld = true;

    int perceptorNotClear = 0;

    for(int k = 0; k < NUM_PERCEPTORS; k++) {
      if(map.newData[j].trackAlive[k] == 1){
        perceptorsClear = false;
        perceptorNotClear = k;
        //  MSG("for object %d, perceptor %d still alive", j, k);
      }

      dtime = (currentTime - map.newData[j].inputData[k].lastSeen);
      if((decayAgeThresh > 0) && (dtime < 1000000*decayAgeThresh)) {
        dataOld = false;
        dtime2 = dtime;
				//      MSG("for element %d, dtime=%d, currentTime=%llu, lastSeen=%llu, perceptorsClear=%d ", j, dtime, perceptorsClear, currentTime, map.newData[j].inputData[k].lastSeen );
      }
    }

    if(!dataOld) {
			//      MSG("dtime = %d",dtime2);
    } 


    if(perceptorsClear && !dataOld) {
			//      MSG("dtime = %llu, perceptors clear but data not old...", dtime2);
    }
    //we want to remove element
    //    if(perceptorsClear && dataOld) {
    if(dataOld) {
      //FIXME: do I need to reset the fields in newData[j]?
      if(!perceptorsClear) {
				//      MSG("removing object %d, but perceptor %d not clear", j, perceptorNotClear);
      }

      removeObject(i);

    }
  }

  //now, decay non-object elements
  // back to front so erase doesn't mess with the index
  for (i=map.data.size()-1;i>=0;--i){
    if (map.data[i].timestamp==0)
      continue;

    if (map.data[i].type == ELEMENT_STOPLINE)
      continue;

    dtime = (currentTime-map.data[i].timestamp);
    if (dtime > 1000000*decayAgeThresh && decayAgeThresh>0){
      priorEl.clear();
      priorEl.setTypeClear();
      priorEl.setId(map.data[i].id);      
      if (debugSubGroup!=0){
        mapTalker->sendMapElement(&priorEl, debugSubGroup);
      }
      if (!plannerOutputDisabled){
        mapTalker->sendMapElement(&priorEl,sendSubGroup);
      }
      map.data.erase(map.data.begin()+i);
    }
  }
  return 0;
}

int Mapper::mainLoop(VehicleState &vehState)
{
  
  bool updated = false;
  // update and send the graph roughly every 4 seconds
  if (sendGraphEnabled && (DGCgettime() - graphTime > 4000000)){
    graphTime = DGCgettime();
    updateGraph();
    elapsedUpdateGraph = DGCgettime() - graphTime;
  }

  if (!lineFusionDisabled && !lineFusionCompat) {
		if (DGCgettime() - lineFusionTime > 100000){ // 10Hz
			lineFusionTime = DGCgettime();
			map.fuseAllLines();
			updated = true;
			elapsedLineFusion = DGCgettime() - lineFusionTime;
		}
  }

  
  //TESTING DISTORTED LANE LINE FUSION
 
  if(senseSegment)
  {
    sensedSegment seg = map.getSensedSegment(vehState);
    

    if(seg.lines.size() > 0 )
      {
	for(unsigned int i = 0; i < seg.laneLines.size(); i++)
	  {      
	    map.transformElLocalToSite(seg.laneLines[i].line);
	    if(seg.laneLines[i].rightLine)
	      (seg.laneLines[i].line).plotColor = MAP_COLOR_RED;
	    else if (seg.laneLines[i].centerLine)
	      (seg.laneLines[i].line).plotColor = MAP_COLOR_GREEN;
	    else
	      (seg.laneLines[i].line).plotColor = MAP_COLOR_ORANGE;
	    
	    mapTalker->sendMapElement(&seg.laneLines[i].line, -6);
	  }

	Log::getStream(1) << "GET SENSED SEGMENT" << endl;
	Log::getStream(1) << "seg.lines.size() = " << seg.lines.size() << endl;
	Log::getStream(1) << "seg.aveAngle = " << seg.aveAngle << endl;
	
	for(unsigned int j = 0; j < seg.laneLines.size(); j++)
	  {
	    Log::getStream(1) << seg.laneLines[j] << endl;
	  }
      }
  }

  // send the rndf data out for visualization
  if (debugSubGroup!=0){
    if (DGCgettime() - debugTime > 100000){
      debugTime = DGCgettime();
      
      if (debugPriorIndex>=map.prior.fulldata.size()){
        debugPriorIndex = 0;
      }
      
      
      if(map.prior.getElFull(priorEl,debugPriorIndex)){
        mapTalker->sendMapElement(&priorEl, debugSubGroup); 
      }
      debugPriorIndex++;
    }
    elapsedSendRndf = DGCgettime() - debugTime;
  }

  //every 5s, clear the stats displayed on cotk
  /**
		 if((DGCgettime() - lastClearedStats) > 5000000) {
		 totalTimeAdd = 0;
		 totalTimeFuse = 0;
		 totalTimeClear = 0;

		 totalElAdd = 0;
		 totalElFuse = 0;
		 totalElClear = 0;

		 maxTimeAdd = 0;
		 maxTimeFuse = 0;
		 maxTimeClear = 0;

		 lastClearedStats = DGCgettime();
		 }
  **/

  // decay the map
  if ((decayAgeThresh > 0) && ((DGCgettime() - decayTime) > 1000000*decayAgeThresh)){
    decayMap();
    //    MSG("decaying the map");
    decayTime = DGCgettime();
    updated = true;
  }  


  //update the road map
  if(groundstrikeFilteringEnabled) {
    //don't want this to run faster than ladars...
    if(DGCgettime() - timeRoadMapUpdated > 10000) {
      DGCgettime(timeRoadMapUpdated);
      roadMap->update();

      if(groundstrikeDisplayEnabled) {
        //      roadMap->updateDisplay();
      }
      updated = true;
      elapsedGroundStrike = DGCgettime() - timeRoadMapUpdated;
    }

  }

  // update the map
  uint64_t tm = DGCgettime();

  //get map elements
  if (mapTalker->recvMapElementNoBlock(&sensedEl,recvSubGroup) >0){
    totalReceived++;
    sensedEl.timestamp = DGCgettime();
    
    //sending has been added to the update map... 
    updateMap();
    updated = true;
    elapsedUpdateMap = DGCgettime() - tm;
    //		debugEl.clear();
    //	debugEl.setTypeDebug();
		//debugEl.setId(-1,0);
		//debugEl.setLabel(0,"Update Map time = ", elapsedUpdateMap);
		//mapTalker->sendMapElement(&debugEl,-3);
  }else{

		// this is necessary for performance reasons until
    // we can get a timed out blocking read above
		usleep(10);
  }
  if (updated){
		return 1;
  }
    
  return 0;
}

int Mapper::updateMap()
{

  
  ///////////for all obstacles, groundstrike filtering and fusion happens here////////

  int index =-1;
  int action = 5;
  int elFused = -2; //will be changed to -1 or 0 when fuseMapElement is called
  //if this is above .8, we don't send obstacle
	double fracGS = 0.0;

  int originatingPerceptorID = sensedEl.id.dat[0];
  int idFromPerceptor = sensedEl.id.dat[1];
  int originatingPerceptor;


  switch(originatingPerceptorID) {

  case 87: //MODladarCarPerceptorLeft: 
    originatingPerceptor = LF_LADAR;
    break;

  case 94: //MODladarCarPerceptorRoofLeft: 
    originatingPerceptor = LF_ROOF_LADAR;
    break;

  case 88: //MODladarCarPerceptorCenter: 
    originatingPerceptor = MF_LADAR;
    break;

  case 89: //MODladarCarPerceptorRight: 
    originatingPerceptor = RF_LADAR;
    break;

  case 90: //MODladarCarPerceptorRear: 
    originatingPerceptor = REAR_LADAR;
    break;

  case 75:  //MODladarCarPerceptor
    originatingPerceptor = ALL_LADAR;
    break;

  case 70: //MODladarObsPerceptor: 
    originatingPerceptor = LADAR_OBS;
    break;

  case 95: //MODladarCarPerceptorRiegl: 
    originatingPerceptor = RIEGL;
    break;

  case 79: //MODradarObsPerceptor: 
    originatingPerceptor = MF_RADAR;
    break;

  case 83: //MODstereoObsPerceptorLong: 
    originatingPerceptor = LONG_STEREO_OBS;
    break;

  case 82: //MODstereoObsPerceptorMedium: 
    originatingPerceptor = MEDIUM_STEREO_OBS;
    break;

  case 102: //MODstereoObsPerceptorRear: 
    originatingPerceptor = REAR_STEREO_OBS;
    break;

  default:
    //unrecognized perceptor
    originatingPerceptor = NUM_PERCEPTORS;
  }

  // for all lanelines, fusion happens in map
  if(originatingPerceptor == NUM_PERCEPTORS) {
    map.addEl(sensedEl);
    if (!plannerOutputDisabled){
      mapTalker->sendMapElement(&sensedEl,sendSubGroup);
    }
    if (debugSubGroup != 0){
      debugEl =sensedEl;
      map.transformElLocalToSite(debugEl);
      mapTalker->sendMapElement(&debugEl,debugSubGroup);
    }
    return 0;
  }


  //range checking before we use these for array indices
  if(originatingPerceptor == NUM_PERCEPTORS || idFromPerceptor < 0 || idFromPerceptor >= MAXNUMPERCEPTOROBJECTS) {
    return -1;
  }

  statArray[originatingPerceptor][0]++;

  if(sensedEl.type == ELEMENT_CLEAR) {
    //    MSG("processing type_clear");
    DGCgettime(timingTime);
    action = map.addEl(sensedEl, obsFusionDisabled); 
    diffTime = DGCgettime() - timingTime;
    maxTimeClear = max(maxTimeClear, diffTime);
    totalElClear++;
    totalTimeClear += diffTime;

    statArray[originatingPerceptor][4] ++;

  }

  //if it's an obstacle, we'll check for groundstrikes, then do fusion
  if(sensedEl.isObstacle() && (ELEMENT_CLEAR != sensedEl.type)){
    int sendID;
    sendID = sensedEl.id.dat.at(0);
    if(MODradarObsPerceptor == sendID) {
      sensedEl.type = ELEMENT_OBSTACLE_RADAR; 
    }

    //if filtering is enabled, and originating sensor is a ladar check groundstrikes
    if(groundstrikeFilteringEnabled) {
   
      if((MODladarCarPerceptor == sendID)||(MODladarCarPerceptorLeft == sendID)||(MODladarCarPerceptorCenter == sendID)||(MODladarCarPerceptorRight == sendID)||(MODladarCarPerceptorRear == sendID)||(MODladarCarPerceptorRoofLeft == sendID)||(MODladarCarPerceptorRoofRight == sendID)||(MODladarCarPerceptorRiegl == sendID)) {

				fracGS = roadMap->checkGroundstrike(sensedEl.geometry);
				//        MSG("testing element from mapelement %d, %d, %d: prob = %f", sendID, sensedEl.id.dat.at(1), sensedEl.id.dat.at(2), fracGS);
      } 
    }  // end checking if groundstrike filtering enabled 

    //if fusion is enabled AND it's not a groundstrike
#warning "magic #"
    if(fracGS < GROUNDSTRIKEPROB) {
      //      MSG("adding el to map: sendID=%d, elemID=%d, type=%d, size=%d, currtime=%llu", sendID, sensedEl.id.dat.at(1),sensedEl.type, sensedEl.geometry.size(), DGCgettime() );

      DGCgettime(timingTime);
      //      MSG("trying to add el");
      sensedEl.timestamp = DGCgettime();
      action = map.addEl(sensedEl, obsFusionDisabled); 

      //      MSG("added el. action = %d", action);
      diffTime = DGCgettime() - timingTime;
      totalElAdd++;
      totalTimeAdd += diffTime;
      maxTimeAdd = max(maxTimeAdd, diffTime);

      //check bounds before using as index
      if(action < 0 || action > 5) {
				action = 5; //throwaway value
      }
      int tmpval = statArray[originatingPerceptor][action];
      statArray[originatingPerceptor][action] = tmpval + 1;
      index = map.IDtoINDEX[originatingPerceptor][sensedEl.id.dat[1]];

      if(index == -1) { //for some reason, couldn't add element
				///MSG("can't updateMap, ind = -1, act = %d, oP = %d, ID = %d", action , originatingPerceptor, sensedEl.id.dat[1]);
				return -1;
      }



      // associate the map
      currentTime = DGCgettime();
      if ((assocTimeThresh > 0) && 
					((currentTime - map.newData[index].lastAssoc) > 1000000*assocTimeThresh)){

        for(unsigned int i=map.usedIndices.size()-1; i>=0; i--) {
					int j = map.usedIndices.at(i);
					if(j == index)
						break;
        
					if(map.newData[index].mergedMapElement.isOverlap(map.newData[j].mergedMapElement)) {
						///MSG("elements %d and %d overlap", index, j);
						for(int p = LF_LADAR; p < RIEGL; p++) {
							if(map.newData[j].trackAlive[p] == 0)
								break;
							else if((map.newData[index].trackAlive[p] == 0) || 
											((map.newData[index].trackAlive[p] == 1) && 
											 (map.newData[j].inputData[p].lastSeen > map.newData[index].inputData[p].lastSeen))) {
								//if object 'index' doesn't have readings from this perceptor, or they're older, use object 'j'

								map.newData[index].inputData[p].timesSeen += map.newData[j].inputData[p].timesSeen;
								map.newData[index].inputData[p].lastSeen = map.newData[j].inputData[p].lastSeen;
								map.newData[index].inputData[p].geometry = map.newData[j].inputData[p].geometry;
								map.newData[index].inputData[p].velocity = map.newData[j].inputData[p].velocity;
								map.newData[index].inputData[p].type = map.newData[j].inputData[p].type;
								map.newData[index].inputData[p].timeStopped = map.newData[j].inputData[p].timeStopped;
							}
						}
					}
					//maintain IDtoINDEX lookup; FIXME: why does this break EVERYTHING?/
					/**
						 for(int m = 0; m < NUM_PERCEPTORS; m++) {
						 for(int n = 0; n < MAXNUMPERCEPTOROBJECTS; n++) {
						 if(map.IDtoINDEX[m][n] == j)
						 map.IDtoINDEX[m][n] = index;
						 }
						 }
					**/
					DGCgettime(currentTime);
					elFused = fuseMapElement(index);
					map.newData[index].lastFused = currentTime;
					removeObject(i);
				}
    
				diffTime = DGCgettime() - currentTime;
        totalElAssoc++;
        totalTimeAssoc += diffTime;
        maxTimeAssoc = max(maxTimeAssoc, diffTime);
				map.newData[index].lastAssoc = currentTime;

      } //end of internal association

      //only run fusion if didn't run it for association
      if((-2 == elFused) && !obsFusionDisabled) {
        currentTime = DGCgettime();
        if((currentTime - map.newData[index].lastFused) > fusionRate) {
          //    MSG("fusing map element: %d", index);
          DGCgettime(timingTime);
          elFused = fuseMapElement(index);

          diffTime = DGCgettime() - timingTime;
          totalElFuse++;
          totalTimeFuse += diffTime;
          maxTimeFuse = max(maxTimeFuse, diffTime);

          map.newData[index].lastFused = currentTime;
        }//
      }


      sensedEl = map.newData[index].mergedMapElement;

    } //end checking not groundstrike

  } //end checking if obstacle
 
  //colorcode sent elements for help in debugging
  switch(sensedEl.type) {
  case ELEMENT_VEHICLE:
    sensedEl.plotColor = MAP_COLOR_DARK_GREEN;
    break;
  case ELEMENT_OBSTACLE:
    sensedEl.plotColor = MAP_COLOR_MAGENTA;
    break;
  case ELEMENT_OBSTACLE_LADAR:
    sensedEl.plotColor = MAP_COLOR_GREY;
    break;
  case ELEMENT_OBSTACLE_RADAR:
    sensedEl.plotColor = MAP_COLOR_PINK;
    break;
  case ELEMENT_OBSTACLE_STEREO:
    sensedEl.plotColor = MAP_COLOR_LIGHT_BLUE;
    break;
  default:
    //change nothing
    break;
  }

  //Planner wants obstacle/vehicle classification, where obstacle is something we're
  //sure is stopped, and vehicle is everything else
  if(sensedEl.isObstacle() && (sensedEl.type != ELEMENT_VEHICLE)) {
    sensedEl.type = ELEMENT_OBSTACLE;
  }



  //for debugging only - send to map this color
      
  if (debugSubGroup != 0){
    debugEl =sensedEl;
    map.transformElLocalToSite(debugEl);
    if(fracGS > GROUNDSTRIKEPROB) {
        
      debugEl.plotColor = MAP_COLOR_YELLOW;
      mapTalker->sendMapElement(&debugEl,debugSubGroup);
    }else{
      debugEl.setId(MODmapping,index);
      mapTalker->sendMapElement(&debugEl,debugSubGroup);
    }
  }

  //if IS groundstrike, need to send type Clear to planner
  if(fracGS > GROUNDSTRIKEPROB) {
    //    MSG("sensed el is groundstrike");
    sensedEl.setTypeClear();

    DGCgettime(timingTime);
    map.addEl(sensedEl, obsFusionDisabled);
    diffTime = DGCgettime() - timingTime;
    totalElClear++;
    totalTimeClear += diffTime;
    maxTimeClear = max(maxTimeClear, diffTime);

  }else{


    //add to local map representation if not gs
    sensedEl.setId(MODmapping, index);

    //    MSG("sensed el's id: %d (id size %d) and type: %d", sensedEl.id.dat.at(0), sensedEl.id.dat.size(), sensedEl.type);

    //only send if have updated fusion
    if ((elFused == 0) && !plannerOutputDisabled){
      mapTalker->sendMapElement(&sensedEl,sendSubGroup);
      numSentToPlanner ++;
    }
  }


  
  return 0;
}

/**
 * returns -1 if didn't fuse (no recent enough data)
 *          0 if did fuse, and we need to send to planner
 **/
int Mapper::fuseMapElement(int index)
{
  
  point2arr mergedGeometry;
  mergedGeometry.clear();
  point2 mergedVelocity = point2(0.0, 0.0);
  MapElementType mergedType = map.newData[index].mergedMapElement.type;
  float mergedTimeStopped = 0;

  vector<point2arr> GPCclippedPolygons;

  int perceptorsUsed = 0;
  DGCgettime(currentTime);

  //if we have ladar returns, want to fuse them
  //if a ladar return is type vehicle, this has preference; then obstacle
  int p;
  for(p=LF_LADAR; p<= RIEGL; p++) {
    //if the data is recent enough, use it
    if((decayAgeThresh == 0) || ((currentTime - map.newData[index].inputData[p].lastSeen) < 1000000*decayAgeThresh)) {
      perceptorsUsed++;
      mergedTimeStopped += map.newData[index].inputData[p].timeStopped;

      GPCclippedPolygons.clear();
      mergedGeometry.get_poly_union(map.newData[index].inputData[p].geometry, GPCclippedPolygons);
      int gpcsize = GPCclippedPolygons.size();
      if(gpcsize != 1) {
				//        MSG("error (index %d, perceptor %d) - merging polygons returned %d separate poitn2arr", index, p, gpcsize);
				if (logging){
					Log::getStream(4)<<"error (index "<<index<<", perceptor "<<p<<") - merging polygons returned "<<gpcsize <<" separate poitn2arr"<<endl;
					for(int i = 0; i < gpcsize; i++) {
						Log::getStream(4)<<"Polygon: "<<i<<" geometry = ";
						for(unsigned int j = 0; j < GPCclippedPolygons.at(0).arr.size(); j++) {
							Log::getStream(4)<<GPCclippedPolygons.at(0).arr[j];
						}
						Log::getStream(4)<<endl;
					}
					Log::getStream(4)<<endl;
				} 
			}
      if(gpcsize == 0) {
        mergedGeometry = map.newData[index].inputData[p].geometry;
      } else {
        mergedGeometry = GPCclippedPolygons.at(0);
      }


			mergedVelocity = mergedVelocity + map.newData[index].inputData[p].velocity;

			if(mergedType == ELEMENT_VEHICLE || map.newData[index].inputData[p].type == ELEMENT_VEHICLE) {
				mergedType = ELEMENT_VEHICLE;
			} else if(mergedType == ELEMENT_OBSTACLE || map.newData[index].inputData[p].type == ELEMENT_OBSTACLE) {
				mergedType = ELEMENT_OBSTACLE;
			} else {
				mergedType = ELEMENT_OBSTACLE_LADAR;
			}
    }
  } // end cycling through ladars

  if(perceptorsUsed == 0) { //obstacle not seen by ladar

    for(p = LONG_STEREO_OBS; p <= MEDIUM_STEREO_OBS; p++) {
			if((decayAgeThresh == 0) || ((currentTime - map.newData[index].inputData[p].lastSeen) < 1000000*decayAgeThresh)) {
        perceptorsUsed++;
        mergedGeometry.get_poly_union(map.newData[index].inputData[p].geometry, GPCclippedPolygons);
        if(GPCclippedPolygons.size() != 1) {
					///MSG("error (index %d, perceptor %d) - merging polygons returned %d separate poitn2arr", index, p, GPCclippedPolygons.size());
        }
				if(GPCclippedPolygons.size() == 0) {
					mergedGeometry = map.newData[index].inputData[p].geometry;
				} else {
          mergedGeometry = GPCclippedPolygons.at(0);
				}

				if(mergedType != ELEMENT_OBSTACLE && mergedType != ELEMENT_VEHICLE)
          mergedType = ELEMENT_OBSTACLE_STEREO;
      }
    }//end cycling through stereos
  }

  //FIXME: add in fusing between radar/ladar
  if(perceptorsUsed == 0) { //obstacle not seen by ladar OR stereo
    p = MF_RADAR;
    if((decayAgeThresh == 0 ) || ((currentTime - map.newData[index].inputData[p].lastSeen) < 1000000*decayAgeThresh)) {
			perceptorsUsed++;

			mergedType = map.newData[index].inputData[MF_RADAR].type;
			mergedVelocity = map.newData[index].inputData[MF_RADAR].velocity;
			mergedGeometry = map.newData[index].inputData[MF_RADAR].geometry;
    }
  }

  if(perceptorsUsed == 0) {
    //    MSG("No recent enough data to update");
    return -1;
  }

  //  MSG("merged object %d, w/ %d perceptors. size=%d, velocity=(%f, %f), type=%d", index, perceptorsUsed, mergedGeometry.size(), mergedVelocity.x, mergedVelocity.y, mergedType);

  map.newData[index].mergedMapElement.setGeometry(mergedGeometry);
  map.newData[index].mergedMapElement.setType(mergedType);
  map.newData[index].mergedMapElement.velocity = mergedVelocity/perceptorsUsed; //summed velocity in for-loop

  map.newData[index].mergedMapElement.timeStopped = mergedTimeStopped/perceptorsUsed;


  return 0;
}

int Mapper::removeObject(int i)
{
  int j = map.usedIndices.at(i);
  MapElement mapEl = MapElement();
  mapEl.clear();
  ///mapEl = map.newData[j].mergedMapElement;
  mapEl.setTypeClear();
  mapEl.setId(MODmapping, j);

  if (debugSubGroup != 0){
    mapTalker->sendMapElement(&mapEl,debugSubGroup);
  }

  if (!plannerOutputDisabled){
    mapTalker->sendMapElement(&mapEl,sendSubGroup);
    clearSentToPlanner ++;
  }

	//since we don't require a perceptor to send a clear message 
	//before removing an obstacle,
	//we need to check that the IDtoINDEX lookup is entirely reset
  for(int m = 0; m < NUM_PERCEPTORS; m++) {
    for(int n = 0; n < MAXNUMPERCEPTOROBJECTS; n++) {
      if(map.IDtoINDEX[m][n]==j)
        map.IDtoINDEX[m][n] = -1;
    }
  }

  map.openIndices.push_back(j);
  mapIterator =  map.usedIndices.begin();
  mapIterator += i;
  map.usedIndices.erase(mapIterator);

  return 0;
}

