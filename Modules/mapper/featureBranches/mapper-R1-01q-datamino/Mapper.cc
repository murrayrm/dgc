/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-08-21 05:35:40 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


Mapper::Mapper()
{ //--------------------------------------------------
  // initialize time offsets
  //--------------------------------------------------
	obsFusionDisabled = false;
	lineFusionDisabled = false;
  graphTime = 0;
  debugTime = 0;
  decayTime = 0;
  fusionTime = 0;
	fusionTimeThresh = 500;
  sendSubGroup = 1;
  recvSubGroup = 0;
  debugSubGroup = -2;
}

Mapper::~Mapper()
{}


// Initialize comm
int Mapper::initComm(int snkey)
{
  //--------------------------------------------------
  // initialize graphtalker
  //--------------------------------------------------
  graphTalkerPtr = new SkynetTalker<Graph>(snkey,SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner); 
  //--------------------------------------------------
  // initialize map elment talker
  //--------------------------------------------------
	this->mapTalker = new CMapElementTalker();	
	this->mapTalker->initRecvMapElement(snkey,recvSubGroup);
	this->mapTalker->initSendMapElement(snkey);

	return 0;  
}

int Mapper::finiComm()
{

	delete mapTalker;
  delete graphTalkerPtr;
  return 0;
}

// Initialize skynet

void Mapper::setLocalFrameOffset(VehicleState state){
	point2 statedelta,stateNE, statelocal;
  
	statelocal.set(state.localX,state.localY);
	stateNE.set(state.utmNorthing,state.utmEasting);
	statedelta = stateNE-statelocal;
  map.setTransform(statedelta);
}



void Mapper::disableLineFusion(bool disableflag = true)
{
  map.enableLineFusion(!disableflag);
  lineFusionDisabled = disableflag;
}

void Mapper::disableObsFusion(bool disableflag = true)
{
  obsFusionDisabled = disableflag;
}

int Mapper::init(string RNDFname)
{	

  // enable or disable line fusion
 
 
  
  //--------------------------------------------------
  // initialize the map prior data
  //--------------------------------------------------
  if (!map.loadRNDF(RNDFname.c_str())){
    cerr << "Error:  Unable to load RNDF file to map " << RNDFname
         << ", exiting program" << endl;
    exit(1);
  }


  //--------------------------------------------------
  // initialize the rndf graph data
  //--------------------------------------------------
	char* fname = (char*) RNDFname.c_str();
	if (!rndf.loadFile(fname)){
    cerr << "Error:  Unable to load RNDF file to graph" << RNDFname
         << ", exiting program" << endl;
    exit(1);
	}
	rndf.assignLaneDirection();
  //initialize graph interface with mplanner
	rndfGraphPtr = new Graph(&rndf);
  //send a graph
  graphTalkerPtr->send(rndfGraphPtr);


  
 
 


  
  return 0;
}		

int Mapper::fini()
{
  delete rndfGraphPtr;
  return 0;
}


int Mapper::updateGraph()
{
  point2 cpt;
  PointLabel ptlabel;
	MapElement tmpEl;

  unsigned int i;
  for (i=0;i< map.data.size();++i){
    tmpEl=map.data[i];
    cpt = tmpEl.center;
    if (map.getLastPointID(ptlabel,cpt)==0){
      rndfGraphPtr->addObstacle(ptlabel.segment,ptlabel.lane,ptlabel.point);
    }
  }
  graphTalkerPtr->send(rndfGraphPtr);
  return 0;
}


int Mapper::resetMap()
{
  map.data.clear();
	return 0;
}

int Mapper::decayMap()
{
  uint64_t currentTime = DGCgettime();
  int dtime = 0;
  int i;

  // back to front so erase doesn't mess with the index
  for (i=map.data.size()-1;i>=0;--i){
    if (map.data[i].timestamp==0)
      continue;
    
    dtime = (int)(currentTime-map.data[i].timestamp);
    if (dtime > 1000000*decayAgeThresh && decayAgeThresh>0){
			priorEl.clear();
			priorEl.setTypeClear();
			priorEl.setId(map.data[i].id);      
			if (debugSubGroup!=0){
				mapTalker->sendMapElement(&priorEl, debugSubGroup);
      }
			if (!plannerOutputDisabled){
				mapTalker->sendMapElement(&priorEl,sendSubGroup);
			}
      map.data.erase(map.data.begin()+i);
    }
  }
	return 0;
}

int Mapper::mainLoop()
{
  
  
  // update and send the graph roughly every 4 seconds
  if (DGCgettime() - graphTime > /*4000000*/500000){
    graphTime = DGCgettime();
    updateGraph();
    
  }

  // send the rndf data out for visualization
  if (DGCgettime() - debugTime > 500000){
    debugTime = DGCgettime();
      
    if (debugSubGroup!=0){
      for (unsigned int i =0;i<map.prior.fulldata.size();++i){
        if(map.prior.getElFull(priorEl,i))
          mapTalker->sendMapElement(&priorEl, debugSubGroup);
      }
    }
  }


  // decay the map
  if (DGCgettime() - decayTime > 500000){
    decayTime = DGCgettime();
    decayMap();
  }  


  // update the map
  //get map elements
  if (mapTalker->recvMapElementNoBlock(&sensedEl,recvSubGroup) >0){
    totalReceived++;
    sensedEl.timestamp = DGCgettime();
    updateMap();
    
    if (!plannerOutputDisabled){
      mapTalker->sendMapElement(&sensedEl,sendSubGroup);
    }
    if (debugSubGroup != 0){
      mapTalker->sendMapElement(&sensedEl,debugSubGroup);
    }
  }else{
    // this is necessary for performance reasons until
    // we can get a timed out blocking read above
		usleep(100);
	}
  return 0;
}

int Mapper::updateMap()
{

  
  //fusion is now something that simply acts on the new element 
  //(trying to keep it as separate from the rest of map as possible)
  if (sensedEl.isObstacle() && !obsFusionDisabled && (ELEMENT_CLEAR != sensedEl.type)){
    int sendID;
    sendID = sensedEl.id.dat.at(0);
    if(MODradarObsPerceptor == sendID) {
      //for now, use this to keep track of which map elements come from this sensor
      sensedEl.type = ELEMENT_OBSTACLE_RADAR; 
    }
    fuseMapElement(sensedEl);
  }

 

  //colorcode sent elements for help in debugging
  switch(sensedEl.type) {
  case ELEMENT_VEHICLE:
    sensedEl.plotColor = MAP_COLOR_DARK_GREEN;
    break;
  case ELEMENT_OBSTACLE:
    sensedEl.plotColor = MAP_COLOR_MAGENTA;
    break;
  case ELEMENT_OBSTACLE_LADAR:
    sensedEl.plotColor = MAP_COLOR_GREY;
    break;
  case ELEMENT_OBSTACLE_RADAR:
    sensedEl.plotColor = MAP_COLOR_PINK;
    break;
  case ELEMENT_OBSTACLE_STEREO:
    sensedEl.plotColor = MAP_COLOR_LIGHT_BLUE;
    break;
  default:
    //change nothing
    break;
  }

  //Planner wants obstacle/vehicle classification, where obstacle is something we're
  //sure is stopped, and vehicle is everything else
  if(sensedEl.isObstacle() && (sensedEl.type != ELEMENT_VEHICLE)) {
    sensedEl.type = ELEMENT_OBSTACLE;
  }

	//add to local map representation
  map.addEl(sensedEl); 

	
  return 0;
}


int Mapper::fuseMapElement(MapElement & fusedEl)
{
  int i;
  int mapsize = (int) map.data.size();
  point2 basePos;
  point2 fusedPos(fusedEl.position);
  
  whichPerceptor originatingSensor;

  //FIXME - it's silly to have this parameter, since it's not really used
  //  double mindist = options.fusion_dist_thresh_arg;
  double mindist = 100;
  int minindex = -1;

  for (i=0; i<mapsize;++i){
    basePos = map.data[i].position;
    double fusedDist = fusedPos.dist(basePos);
    //if old map element at i is closer than previous minimum AND is an obstacle
    //TODO: still does no fusion for road features
    if ((fusedDist < mindist) && map.data[i].isObstacle()){
      minindex = i;
      mindist = fusedDist;
    }
  }

  //label the map elements according to their originating module (since the
  //   id will be overwritten during fusion)
  int sendID;
  sendID = fusedEl.id.dat.at(0);


  if((MODladarCarPerceptor == sendID)||(MODladarCarPerceptorLeft == sendID)||(MODladarCarPerceptorCenter == sendID)||(MODladarCarPerceptorRight == sendID)||(MODladarCarPerceptorRear == sendID)||(MODladarCarPerceptorRoofLeft == sendID)||(MODladarCarPerceptorRoofRight == sendID)||(MODladarCarPerceptorRiegl == sendID)) {
    originatingSensor = LADAR_CAR;
  } else if(MODradarObsPerceptor == sendID) {
    originatingSensor = RADAR_CAR;
    fusedEl.type = ELEMENT_OBSTACLE_RADAR;
  } else if((MODstereoObsPerceptorLong == sendID) || (MODstereoObsPerceptorMedium == sendID)) {
    originatingSensor = STEREO_OBS; 
    fusedEl.type = ELEMENT_OBSTACLE_STEREO;
  } else {
    originatingSensor = PERCEPTOR_UNKNOWN;
    MSG("Unrecognized perceptor sending to fusion");
  }

  //  if (minindex < 0){
  //   return 0; //no matching object; do not change the input map element

  if (minindex>=0){
    // only fuse overlapping obstacles at the moment (fusing static with dynamic obs is ok)
    if (fusedEl.isOverlap(map.data[minindex]) || map.data[minindex].isOverlap(fusedEl)){
      // just make sure we did things right. REMOVE ME when fusing lines too
      assert(fusedEl.isObstacle());
      assert(map.data[minindex].isObstacle());

      // use the existing id in the map
      fusedEl.id = map.data[minindex].id;

      // compute the difference in times sensed
      fusedDeltaTime = (int)((fusedEl.state.timestamp-map.data[minindex].state.timestamp )/1000); 
      //	MSG("fusing elements with deltaTime = %d; incoming confidence: %f", fusedDeltaTime, fusedEl.conf);

      // if the time difference is small, we can be fairly sure that the 
      // older data isn't stale, and can use it to fuse
      if (fusedDeltaTime < fusionTimeThresh){

        // increase confidence for the fused element
        //        fusedEl.conf = fusedEl.conf+map.data[minindex].conf;
        //          fusedEl.conf = 2*map.data[minindex].conf;

        //for now, since perceptors don't report uncertainties to be trusted,
        //use my previous knowledge about sensors to determine which to trust

        /**things to update:
         *     ID - use old
         *     velocity - average old and new
         *     position - use new; TODO: actually merge somehow
         * 
         *     object type -> from stereo and radar should both be car; only ladar can make distinction?
         *     time stationary? is stationary?
         *     
         *     keep track of #elements fused from ladar/radar/stereo
         **/

        switch(originatingSensor) {
        case LADAR_CAR:
          //use newest position UNLESS previously seen obstacle was car
          if((ELEMENT_VEHICLE == map.data[minindex].type)&&(ELEMENT_VEHICLE != fusedEl.type)) {
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
            fusedEl.type = map.data[minindex].type;
          }
          //update velocity
          fusedEl.velocity = (map.data[minindex].velocity + fusedEl.velocity)/2;
          //trust classification only from ladar-car-perceptor
          if((ELEMENT_VEHICLE == fusedEl.type) || (ELEMENT_VEHICLE == map.data[minindex].type)) {
            fusedEl.type = ELEMENT_VEHICLE;
          } else if((ELEMENT_OBSTACLE == fusedEl.type) || (ELEMENT_OBSTACLE == map.data[minindex].type)) {
            fusedEl.type = ELEMENT_OBSTACLE;
            //   MSG("merging geometries!");
            //	      fusedEl.geometry.merge(map.data[minindex].geometry);
            //	      fusedEl.updateFromGeometry();
          } 	      
          //	    MSG("fused ladar data into %d", map.data[minindex].type);
          break;

        case RADAR_CAR:
          //trust velocity regardless
          fusedEl.velocity = (map.data[minindex].velocity + fusedEl.velocity)/2;
          //trust new position ONLY if radar is only sensor to see this object
          if(ELEMENT_OBSTACLE_RADAR != map.data[minindex].type) {
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
            fusedEl.type = map.data[minindex].type;
          } else {
            //only observations of the object have been from radar, so can update geometry
          }
          if(ELEMENT_OBSTACLE == map.data[minindex].type) {
            fusedEl.type = ELEMENT_OBSTACLE;
          }
          // 	    MSG("fused radar data into %d", map.data[minindex].type);
          break;

        case STEREO_OBS:
          //use newest position UNLESS previously seen obstacle was car
          if(ELEMENT_VEHICLE == map.data[minindex].type) {
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
            fusedEl.type = map.data[minindex].type;
          }
          if(ELEMENT_OBSTACLE_STEREO != map.data[minindex].type) {
            fusedEl.velocity = map.data[minindex].velocity;
          }
          //if we've observed it to be stationary, keep that classification
          if(ELEMENT_OBSTACLE == map.data[minindex].type) {
            fusedEl.type = ELEMENT_OBSTACLE;
          }
          //  MSG("fused stereo data into %d", map.data[minindex].type);
          break;

        case PERCEPTOR_UNKNOWN:
          ERROR("trying to fuse unknown perceptor");
          break;
        } // end switch statement checking which sensor new data comes from
      } //end checking delta time
        // update the local map - this happens in 
      //        map.data[minindex] = fusedEl;
      totalFused++;
    } //end checking overlap (if no overlap, do nothing)
  } //end checking if there was a minimum
  return 0;
}

