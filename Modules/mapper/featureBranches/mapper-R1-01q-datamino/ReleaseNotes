              Release Notes for "mapper" module

Release R1-01q (Tue Aug 21  6:12:40 2007):
	Enabled optional decay of elements such that if the element isn't
	updated for a given number of seconds, the element is removed from
	all copies of the map.	By default the decay is off, but can turned
	on by setting the command line option --decay-thresh=#of secs from
	mapper or --mapper-decay-thresh=# of secs if run from planner.

Release R1-01p (Mon Aug 20 21:55:35 2007):
	updated mapper to enable it to be instantiated as an object and run
	inside tplanner. Mapper still works as a standalone program.

Release R1-01o (Mon Aug 20 17:03:09 2007):
removing debugging fprintf's (was going to make build release anyways)

Release R1-01n (Tue Aug 14 23:09:12 2007):
	Added a commandline option to disable line fusion
        (--disable-line-fusion). This is only useful (not) to see the
        fused rndf in the mapviewer, but it doesn't get sent to the planner.
        The planner has its own Map object and cmdline option for that.

Release R1-01m (Tue Aug 14 12:59:33 2007):
	When using the --no-state option, use the state from the
	MapElements to set the global <-> local delta in MapPrior. This
	allows to run sensnet log files and have the rndf overlap correctly
	with alice (needed to test lane fusion).

Release R1-01l (Mon Aug  6 21:12:28 2007):
	changing default object type from vehicle to obstacle, at noel's request

Release R1-01k (Sun Aug  5 22:07:30 2007):
	committing field changes to mapper - small change to the logic in obstacle fusion

Release R1-01j (Tue Jul 31 14:58:43 2007):
	first cut at fusion, with improved logic for which data to use from each perceptor. 

Release R1-01i (Thu Jul 19 18:17:47 2007):
	- Fixed minimum distance check (was not updating mindist).
        - Only fuse an obstacle with another obstacle (and do not
        fuse other map elements at all, such as lane lines).
	- Send map elements to both the send subgroup and the debug
        subgroup, if this latest one is not zero.

Release R1-01h (Tue Jul 17  8:06:56 2007):
	Improved comments and internal structure.  Added basic fusion
	capability and command line options to adjust fusion thresholds. 
	Implemented process control interface.	Warning for the navigation
	package.  Mapper now depends on sensnet for the process control
	interface, so that might be an issue and need to bee added to the
	nav package.  Implemented --debug-subgroup=num commandline option
	to specify where to visualize RNDF and other debugging info

Release R1-01g (Thu Jun 28  2:49:04 2007):
	updated testMapper test 1 to work with no asim or astate running
	and pulls state info from map elements sent to debugging channel -2

Release R1-01f (Mon Jun  4 20:20:01 2007):
	updated testMapper to include noisy measurements for the placed
	obstacles.  This is to help testing in a more realistic map
	situation where the obstacles are not perfectly stable and can
	drop in and out at longer range.  The obstacles will always be
	seen at less than 20 meters away, and will increasingly drop in
	and out up to 30 meters which is set as the outer range.  The
	noise value for position and radius will not be more than .3
	meters in any direction.  "testMapper rndffile 3" will allow
	placement of noisy obstacles and should be used as much as
	possible.  "testMapper rndffile 2" is the old test which places
	obstacles without noise, but the max range to see them is reduced
	from 50 meters to 30 meters which is more accurate, and the color
	will change to yellow when the obstacle is visible.

Release R1-01d (Tue May 22 17:16:55 2007):
	updated testMapper test 2 to not send obstacles at 0,0 at startup. 
	Obstacles won't be sent until they're placed

Release R1-01c (Fri May 18 13:58:41 2007):
	fixed bug in testMapper.cc where placed obstacles had a zero height

Release R1-01b (Tue May 15 17:03:24 2007):
	Updated mapper to use with new map module.  Forgot to release this
	earlier.

Release R1-01a (Mon Apr 30 20:45:15 2007):
	fixed testMapper to work with new map element talker

Release R1-01 (Mon Apr 30 12:36:30 2007):
	Updated mapper to work with new map module.  Implements sub channel
	communication and variable length messages

Release R1-00s (Tue Mar 20  9:12:42 2007):
	Fixed rndf initialization error.  Now uses state to initialize global->local frame offset.

Release R1-00r (Sun Mar 18  9:23:39 2007):
	Added travgraph update to mapper. 

Release R1-00q (Sun Mar 18  8:56:13 2007):
	Minor update to remove SkynetTalker.cc

Release R1-00p (Sat Mar 17 13:18:33 2007):
	Updated COLOR_ prefix for map elements to MAP_COLOR_ to work with
	map changes

Release R1-00o (Sat Mar 17 10:15:29 2007):
	updated mapper to send a graph at every 5 seconds

Release R1-00n (Sat Mar 17  0:43:23 2007):
	cleaned up some testMapper interface.

Release R1-00m (Thu Mar 15 15:21:33 2007):
	updated some testMapper functionality.	Added ability to add up to
	4 obstacles and remove them as well

Release R1-00l (Wed Mar 14 23:14:22 2007):
	added basic simulation of static obstacles to mapper via command
	testMapper.  Can extend to dynamic obstacles as needed.


Release R1-00k (Fri Mar  9 17:08:00 2007):
		updated mapper to work with new MapElementTalker location

Release R1-00j (Sun Mar  4 17:42:05 2007):
	Mapper now sends graph to mplanner.  Also fixed a bug where rndf
	commandline option was being ignored.

Release R1-00i (Thu Mar  1 19:32:43 2007):
	added -lframes to Makefile.  Updated main loop in Mapper.cc to
	better handle large numbers of received messages without delays.

Release R1-00h (Tue Feb 27  8:48:46 2007):
Separated new map object into Map module.

Release R1-00g (Wed Feb 21 20:00:25 2007):
	Mapper now parses RNDF file and sends RNDF data out.  Also reads in
	sensor based map elements and passes them through.  No fusion or
	correlation between sensed data and prior RNDF data yet.  This
	should be good enough for RNDF lane + sensor-based obstacle testing
	for now.


Release R1-00f (Sun Feb  4 18:55:37 2007):
	Checking in changes from field test.  Also fixing to work with
	updated cotk module

Release R1-00e (Sat Feb  3 23:17:58 2007):
		added temporary frame transformation to output lines in global frame

Release R1-00d (Sat Feb  3  9:54:45 2007):
added basic filtering methods

Release R1-00c (Fri Feb  2 14:50:19 2007):
Added basic command line display for mapper using cotk. You can now
	output spoof test messages to planner from this interface.   Added
	mapper-test-input command lineprogram to spoof messages from lineperceptor 
	for testing and debugging the input to mapper and pathway to planner.  
	Updated Makefile.yam to compile all.

Release R1-00b (Fri Feb  2 11:01:30 2007):
	Updated mapper to receive stopline data from lineperceptor and pass data to tplanner.  Right now the lines filter is just a passthrough.  

Release R1-00a (Sat Jan 27 18:31:26 2007):
	Import basic mapper module files

Release R1-00 (Sat Jan 27 16:33:49 2007):
	Created.




































