/**********************************************************
 **
 **  TESTMAPPER.CC
 **
 **    Time-stamp: <2007-03-15 13:53:26 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Mar 14 16:40:15 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "map/Map.hh"
#include <iostream>
#include <string>
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "skynettalker/StateClient.hh"
#include "interfaces/sn_types.h"
using namespace std;

class CStateWrapper : public CStateClient
{
public:
  CStateWrapper(int snkey) : CSkynetContainer(MODmapping,snkey),CStateClient(true) { };
  ~CStateWrapper() {};
  
  VehicleState getstate() {UpdateState();return m_state;}

};

int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroupMapper = 0;
  int sendSubGroupViewer = -2;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testMap [rndf filename] [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  if(argc >1)
    fname = argv[1];
  if(argc >2)
    testnum = atoi(argv[2]);
  if(argc >3)
    sendSubGroupViewer = atoi(argv[3]);
  if(argc >4)
    recvSubGroup = atoi(argv[4]);
  if(argc >5)
    skynetKey = atoi(argv[5]);
  
  cout << "RNDF filename = " << fname 
       << "  Test Number = " << testnum << endl 
       << "  Send Subgroup Viewer = "  << sendSubGroupViewer 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey);

  Map mapdata;
  //  mapdata.loadRNDF("rn.txt");

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };
 
  MapElement el;
	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;  
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getEl(i,el);
    //TEMP 
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,sendSubGroupMapper);
    maptalker.sendMapElement(&el,sendSubGroupViewer);
	}
    
  cout <<"========================================" << endl;
  testname = "Testing State";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
      
    MapElement recvEl;
    MapElement sendEl1, sendEl2;
  
    int objnum =4;
    vector<MapElement> sendElarr;

      sendEl1.set_circle_obs();
      sendEl1.plot_color = COLOR_MAGENTA;
    for (unsigned i = 0 ; i< objnum; ++i){
      sendEl1.set_id(-i-1);
      sendElarr.push_back(sendEl1);
    }
  


    CStateWrapper stateclient(skynetKey);
    VehicleState state;
    point2 statept,cpt;
    double range = 10;
  
      int id = 0;
    while (true){

      state = stateclient.getstate();
      // cout << "State time = " << state.timestamp << endl;

      sendEl2.set_alice(state);
      maptalker.sendMapElement(&sendEl2,sendSubGroupViewer);
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          sendElarr[id].set_geometry(statept,2);
          cout << "Setting obstacle = " << endl;  
          maptalker.sendMapElement(&sendElarr[id],sendSubGroupViewer);
          id++;
          if (id>=objnum)
            id = 0;
      }
      
      statept.set(state.localX,state.localY);
      for (unsigned i = 0; i< objnum; ++i){
        cpt = sendElarr[i].center;
        if (statept.dist(cpt)< range){
          maptalker.sendMapElement(&sendElarr[i],sendSubGroupMapper);
        }else {
          sendEl1 = sendElarr[i];
          sendEl1.type = ELEMENT_CLEAR;
          maptalker.sendMapElement(&sendEl1,sendSubGroupMapper);
        }
        
      }
      usleep(100000);      
    }

  }
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
