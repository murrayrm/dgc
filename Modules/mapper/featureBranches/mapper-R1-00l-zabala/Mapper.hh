/**********************************************************
 **
 **  MAPPER.HH
 **
 **    Time-stamp: <2007-03-09 15:13:10 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:39 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <assert.h>
#include <vector>
#include <math.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
// local headers
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/SkynetTalker.cc" // hack to instantiate templates

#include "travgraph/Graph.hh"
#include "rndf/RNDF.hh"

#include <ncurses.h>
// CLI support
#include <cotk/cotk.h>
// Cmd-line handling
#include "cmdline.h"

#include "map/Map.hh"

using namespace std;
//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class Mapper : public CMapElementTalker
{
	
public:
	
	//! A Constructor 
	/*! DETAILS */
	Mapper();

	
	//! A Destructor 
	/*! DETAILS */
	~Mapper();
	
public:
	
	int parseCmdLine(int argc, char**argv);

	int send(MapElement* p_el);
	int recv(MapElement* p_el);

	int initComm();

	int initConsole();
	int closeConsole();


	static int onUserQuit(cotk_t *console, Mapper *self, const char *token);
static int onUserPause(cotk_t *console, Mapper *self, const char *token);
static int onUserFake(cotk_t *console, Mapper *self, const char *token);
static int onUserReset(cotk_t *console, Mapper *self, const char *token);

	cotk_t *console;
	bool quit;
	bool pause;
	bool show;
	int totalReceived;
	int totalSent;
	int totalFakes;

	
	gengetopt_args_info options;

	//Skynet settings
	modulename moduleId;
	int skynetKey;
	int sendSubGroup;
	int recvSubGroup;

	string RNDFfilename;
	bool noconsole_flag;


	int resetMap();



	// Incoming line message
	MapElement el_in;

	// Outgoing line message
	MapElement el_out;


	Map map;

	SkynetTalker<Graph> *graphTalkerPtr;
	Graph* rndfGraphPtr;

};




#endif
