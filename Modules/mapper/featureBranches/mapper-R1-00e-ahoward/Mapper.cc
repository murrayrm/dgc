/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-02-03 22:56:01 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;


Mapper::Mapper()
{}

Mapper::~Mapper()
{}

int Mapper::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
 if (this->options.countthresh_given)
    this->countThresh = this->options.countthresh_arg;
 else
	 this->countThresh = 5;

 if (this->options.distthresh_given)
    this->distThresh = this->options.distthresh_arg;
 else
	 this->distThresh = 1;
 // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
}

static char *consoleTemplate =
"Mapper Lite (Stopline version)                                             \n"
"                                                                           \n"
"Skynet Info:   %spread%                                                    \n"
"Message Stats: %lines%                                                     \n"
"Filter Info: %filter%                                                      \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%|%RESET%]                                                    \n"
"                                                                           \n"
"%stdout%                                                                   \n";

// Initialize console display
int Mapper::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
	cotk_bind_button(this->console, "%RESET%", " RESET ", "Rr",
									 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(this->console);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "Key = %d",
							this->skynetKey);
cotk_printf(this->console, "%filter%", A_NORMAL, 
						"dist thresh = %d  count thresh = %d",
							this->distThresh, this->countThresh);

  return 0;
}


// Finalize sparrow display
int Mapper::closeConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize skynet
int Mapper::initComm()
{
	this->m_skynet = new skynet(MODmapping, this->skynetKey, NULL);
	this->recSocket = this->m_skynet->listen(SNroadLine,  MODmapping);
	this->sndSocket = this->m_skynet->get_send_sock(SNtrafficLocalMap);

  return 0;
}


// Clean up skynet
int Mapper::closeComm()
{
	delete this->m_skynet;
  return 0;
}

int Mapper::checkRead()
{
	return (this->m_skynet->is_msg(this->recSocket));
}

int Mapper::readLines()
{
	int readval = (this->m_skynet->get_msg(this->recSocket, &this->lineMsgIn, sizeof(this->lineMsgIn),0));
	this->printLinesIn();
	this->totalReceived++;
	return 0;
}

int Mapper::writeLines()
{
	int bytesSent;
	bytesSent = this->m_skynet->send_msg(this->sndSocket, &this->lineMsgOut, sizeof(this->lineMsgOut), 0);

	this->printLinesOut();
	this->totalSent++;
	return 0;

}

int Mapper::printLinesIn()
{
	for (int i= 0; i <this->lineMsgIn.num_lines;++i){
		cerr<< "Line In " << i << " = (" 
				<< this->lineMsgIn.lines[i].a[0] << ","
				<<this->lineMsgIn.lines[i].a[1] << ","
				<<this->lineMsgIn.lines[i].a[2] << ")" 
				<< ", (" << this->lineMsgIn.lines[i].b[0] << ","
				<<this->lineMsgIn.lines[i].b[1] << ","
				<<this->lineMsgIn.lines[i].b[2] << ")" << endl;
	}

	cout <<"Northing = " << this->lineMsgIn.state.utmNorthing <<  endl;
	cout <<"Easting = " << this->lineMsgIn.state.utmEasting <<  endl;
	cout <<"Altitude = " << this->lineMsgIn.state.utmAltitude <<  endl;
	cout <<"Roll = " << this->lineMsgIn.state.utmRoll <<  endl;
	cout <<"Pitch = " << this->lineMsgIn.state.utmPitch <<  endl;
	cout <<"Yaw = " << this->lineMsgIn.state.utmYaw <<  endl;
	
cout <<"localX = " << this->lineMsgIn.state.localX <<  endl;
	cout <<"localY = " << this->lineMsgIn.state.localY <<  endl;
	cout <<"localZ = " << this->lineMsgIn.state.localZ <<  endl;
	cout <<"localRoll = " << this->lineMsgIn.state.localRoll <<  endl;
	cout <<"localPitch = " << this->lineMsgIn.state.localPitch <<  endl;
	cout <<"localYaw = " << this->lineMsgIn.state.localYaw <<  endl;
}

int Mapper::printLinesOut()
{
	for (int i= 0; i <this->lineMsgOut.num_lines;++i){
		cerr<< "Line Out " << i << " = (" 
				<< this->lineMsgOut.lines[i].a[0] << ","
				<<this->lineMsgOut.lines[i].a[1] << ","
				<<this->lineMsgOut.lines[i].a[2] << ")" 
				<< ", (" << this->lineMsgOut.lines[i].b[0] << ","
				<<this->lineMsgOut.lines[i].b[1] << ","
				<<this->lineMsgOut.lines[1].b[2] << ")" << endl;
	}
}

// Handle button callbacks
int Mapper::onUserQuit(cotk_t *console, Mapper *self, const char *token)
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int Mapper::onUserPause(cotk_t *console, Mapper *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle button callbacks
int Mapper::onUserReset(cotk_t *console, Mapper *self, const char *token)
{
  self->resetLines();
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle fake stop button
int Mapper::onUserFake(cotk_t *console, Mapper *self, const char *token)
{
  float px, py, pz;
    
  //MSG("creating fake stop line data");

	double simX = 3778086;
	double simY = 396404;;

	self->lineMsgOut.lines[0].a[0] = simX;
	self->lineMsgOut.lines[0].b[0] = simX;
	self->lineMsgOut.lines[0].a[1] = simY;
	self->lineMsgOut.lines[0].b[1] = simY;      
    
  self->lineMsgOut.num_lines = 1;
  
  self->totalFakes += 1;
	self->writeLines();
  return 0;
}

int Mapper::resetLines()
{
	this->tmpVotes.clear();
	this->tmpLineX.clear();
	this->tmpLineY.clear();
	this->votes.clear();
	this->lineX.clear();
	this->lineY.clear();

}

int Mapper::filterLines()
{
	int num_lines_in = this->lineMsgIn.num_lines;
	//Passthrough for now



	if (this->options.passthrough_mode_flag){
		cerr <<"Received and passing through " << num_lines_in
				 << " lines " <<endl;
		this->lineMsgOut = this->lineMsgIn;
	}else{
		// some filtering
		int num_lines_out = this->lineMsgOut.num_lines;
		double thisLineX, thisLineY;
		//double thisLineX, thisLineY;
		double thisdist;
		double mindist = 9999999;
	
		bool usedflag = false;

		cerr <<"Received and filtering " << num_lines_in
				 << " lines " <<endl;

		for (int i=0; i<num_lines_in; ++i){
			usedflag = false;
			thisLineX = 0.5*(this->lineMsgIn.lines[i].a[0] 
											 + this->lineMsgIn.lines[i].b[0]);
			thisLineY = 0.5*(this->lineMsgIn.lines[i].a[1] 
											 + this->lineMsgIn.lines[i].b[1]);


			for (int j=0; j<this->votes.size(); ++j){
				thisdist = (pow(thisLineX-this->lineX[j],2) 
										+ pow(thisLineY-this->lineY[j],2));
				if (thisdist < this->distThresh){
					this->votes[j]++;
					this->lineX[j] = thisLineX;
					this->lineY[j] = thisLineY;
					usedflag = true;
					cerr << "Input line " << i  << " matches current line  " << j 
					<< "Count =  " << this->votes[j] << endl;
				}
				
			}
			
			if(usedflag)
				continue;
			
			// put in temp line arr
			for (int j=0; j<this->tmpVotes.size(); ++j){
				thisdist = pow(thisLineX-this->tmpLineX[j],2) 
					+ pow(thisLineY-this->tmpLineY[j],2);
				if (thisdist < this->distThresh){
					this->tmpVotes[j]++;
					this->tmpLineX[j] = thisLineX;
					this->tmpLineY[j] = thisLineY;
					usedflag = true;
					cerr << "Input line " << i  << " matches candidate line  " << j 
					<< "  Count =  " << this->tmpVotes[j] << endl;
					//add to main list if seen lots before
					if (this->tmpVotes[j]> this->countThresh){
				 		this->votes.push_back(1);
						this->lineX.push_back(thisLineX);
						this->lineY.push_back(thisLineY);
						cout << "New line Added to map" << endl;
					}
				}
							}
			if(!usedflag){
				cerr << "New candidate line detected" << endl << endl;
 
				this->tmpVotes.push_back(1);
				this->tmpLineX.push_back(thisLineX);
				this->tmpLineY.push_back(thisLineY);
					
			}
			
		}
		for (int i=0; i<this->votes.size(); ++i){
			this->lineMsgOut.lines[i].a[0] = this->lineX[i];
			this->lineMsgOut.lines[i].b[0] = this->lineX[i];
			this->lineMsgOut.lines[i].a[1] = this->lineY[i];
			this->lineMsgOut.lines[i].b[1] = this->lineY[i];
			this->lineMsgOut.lines[i].a[2] = 0;
			this->lineMsgOut.lines[i].b[2] = 0;
		}
		this->lineMsgOut.num_lines = this->votes.size();
	}


	//temp frame transformation 

	double dx =  this->lineMsgIn.state.utmEasting -this->lineMsgIn.state.localX;
	double dy =  this->lineMsgIn.state.utmNorthing -this->lineMsgIn.state.localY;
	double dz =  this->lineMsgIn.state.utmAltitude -this->lineMsgIn.state.localZ;

	dx = 100;

	for (int i=0; i<this->lineMsgOut.num_lines;++i){
		this->lineMsgOut.lines[i].a[0] += dx;
		this->lineMsgOut.lines[i].b[0] += dx;
		this->lineMsgOut.lines[i].a[1] += dy;
		this->lineMsgOut.lines[i].b[1] += dy;
		this->lineMsgOut.lines[i].a[2] += dz;
		this->lineMsgOut.lines[i].b[2] += dz;
	}


	return 0;
}








const bool DEBUG=1;
// Main program thread
int main(int argc, char **argv)
{
	bool DEBUG=1;

	Mapper *mapper;
	int msgId, msgLen;
	mapper = new Mapper();
  assert(mapper);

	mapper->parseCmdLine(argc, argv);
	mapper->initComm();
	mapper->resetLines();
	
	if (!mapper->options.disable_console_flag)
		if (mapper->initConsole() != 0)
			return -1;

	cerr << "Listening for lines" << endl;

	while (!mapper->quit){
		
		if (mapper->console)
			cotk_update(mapper->console);

		if (mapper->pause){
			usleep(0);
			continue;
		}

		if (mapper->checkRead()){
			mapper->readLines();
			mapper->filterLines();
			mapper->writeLines();

		}else{
			usleep(1000);
			if (mapper->console){
				cotk_printf(mapper->console, "%lines%", A_NORMAL, "%d received, %d sent" 
										, mapper->totalReceived, mapper->totalSent);
			}
		}
	}
	mapper->closeConsole();
	mapper->closeComm();

	return 0;
}


	
	

	
		
	
	
	
		


