/**********************************************************
 **
 **  MAPPER_TEST_INPUT.CC
 **
 **    Time-stamp: <2007-02-02 14:27:23 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Jan 31 12:31:30 2007
 **
 **
 **********************************************************
 **
 ** This program sends a message containing a single line 
 ** centered at the given point.  It can used to spoof 
 ** messages that would be sent from linePerceptor to mapper 
 **
 **********************************************************/


#include <iostream>
#include "skynet/sn_types.h"
#include "interfaces/RoadLineMsg.h"
#include "skynet/sn_msg.hh"

using namespace std;

int main(int argc, char **argv)
{

	double simX;
	double simY;

	if (argc !=3){
		cout << endl
				 << "Description:" << endl
				 << "This program sends a message containing a single line centered at the given point." << endl
				 << "It can used to spoof messages that would be sent from linePerceptor to mapper " << endl
				 << endl
			
				 << "Usage: "<< endl
				 << "mapper_test_input <northing> <easting>" << endl
				 << endl
				 << "Example: " << endl
				 << "mapper_test_input 3778086 396404" << endl 
				 << endl;
		return(0);
	}
	
	simX=atof(argv[1]);
	simY=atof(argv[2]);
		
	cout << "Northing = " << simX 
			 << "  Easting = " << simY << endl;

		

	// Spread settings
	char *spreadDaemon; int skynetKey;
	int localMapSocket;
	int bytesSent = 0;
	RoadLineMsg lineMsg;
	
	skynetKey = atoi(getenv("SKYNET_KEY"));

	skynet m_skynet(MODmapping, skynetKey, NULL);


	localMapSocket = m_skynet.get_send_sock(SNroadLine);
	
		

		lineMsg.lines[0].a[0] = simX;
		lineMsg.lines[0].b[0] = simX;
		lineMsg.lines[0].a[1] = simY;
		lineMsg.lines[0].b[1] = simY;
		lineMsg.lines[0].a[2] = 0;
		lineMsg.lines[0].b[2] = 0;

		lineMsg.num_lines = 1;
 bytesSent = m_skynet.send_msg(localMapSocket, &lineMsg, sizeof(lineMsg), 0);

 cout << "bytes sent = " << bytesSent << endl;
		}
