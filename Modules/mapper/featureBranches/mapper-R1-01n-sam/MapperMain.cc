/**********************************************************
 **
 **  MAPPERMAIN.CC
 **
 **    Time-stamp: <2007-08-18 05:56:35 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Aug 18 05:24:04 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/



#include "Mapper.hh"
#include "MapperMain.hh"


using namespace std;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)



MapperMain::MapperMain()
{
	cout <<"starting mappermain" << endl;
  // initialize the console to NULL
  console = NULL;
  pause = 0;
  quit = 0;
	reset = 0;
  processTime = 0;
	cout <<"starting mappermain" << endl;
	//	memset(&state, 0, sizeof(state));
  	cout <<"starting mappermain" << endl;
}

MapperMain::~MapperMain()
{}


int MapperMain::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
   this->moduleId = MODmapping;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");

  if (this->options.rndf_given){
    this->RNDFfilename.assign(this->options.rndf_arg);
  }else{
    return ERROR("Please specify an RNDF file using the option --rndf=filename");
  }
      
  // Fill out the send subgroup number
  this->sendSubGroup = this->options.send_subgroup_arg;
 
  // Fill out the recv subgroup number
  this->recvSubGroup = this->options.recv_subgroup_arg;

  // Fill out the recv subgroup number
  this->debugSubGroup = this->options.debug_subgroup_arg;
 
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

  //cerr << "Line fusion " << (this->options.disable_line_fusion_flag ?
	//                          "disabled" : "enabled") << endl;
	
  return 0;
}

static char *consoleTemplate =
"Mapper                                                                \n"
"                                                                           \n"
"Skynet Info:   %spread%                                                    \n"
"Message Stats: %elements%                                                  \n"
"Filter Info: %filter%                                                      \n"
"Map Info: %map%                                                            \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%RESET%]                                                    \n"
"                                                                           \n"
"%stdout%                                                                   \n";

// Initialize console display
int MapperMain::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  	cotk_bind_button(this->console, "%RESET%", " RESET ", "Rr",
									 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, 
							"Key = %d, Send Subgroup = %d, Receive Subgroup = %d",
							this->skynetKey, this->sendSubGroup, this->recvSubGroup);
 
  return 0;
}


// Finalize sparrow display
int MapperMain::finiConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}

// Initialize skynet
int MapperMain::initComm()
{

  //--------------------------------------------------
  // initializing state talker
  //--------------------------------------------------
  if(!options.no_state_flag) {
    stateTalker = new CStateWrapper(skynetKey);
		state = stateTalker->getstate();
  }

 
  //--------------------------------------------------
  // join sensnet for process handling
  //--------------------------------------------------
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");
   

 //--------------------------------------------------
  // initialize map elment talker
  //--------------------------------------------------
	this->mapTalker = new CMapElementTalker();	
	this->mapTalker->initRecvMapElement(skynetKey,recvSubGroup);
	this->mapTalker->initSendMapElement(skynetKey);

 
  return 0;
}

int MapperMain::finiComm()
{
	if(!options.no_state_flag) {
		delete stateTalker;
	}

  if (this->sensnet)
  {
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
    sensnet_disconnect(this->sensnet);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }

	delete mapTalker;

  return 0;
}

int MapperMain::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  if (request.quit) {
    MSG("remote quit request");
  }
  return 0;
}


// Handle button callbacks
int MapperMain::onUserQuit(cotk_t *console, MapperMain *self, const char *token) 
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int MapperMain::onUserPause(cotk_t *console, MapperMain *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle button callbacks
int MapperMain::onUserReset(cotk_t *console, MapperMain *self, const char *token)
{
  self->reset = true;
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{

	Mapper  mapper;
	MapperMain mappermain;
	
	mappermain.parseCmdLine(argc, argv);
	cout <<"starting mapper" << endl;

	if (mappermain.sendSubGroup ==mappermain.recvSubGroup){
		cerr <<"Error in Mapper: must specify different send and receive subgroups." << endl;
		return -1;
	}
		

   MSG("Init Comm");
	mappermain.initComm();
	mapper.initComm(mappermain.skynetKey);
	

	if (!mappermain.options.disable_console_flag ){
    MSG("Init Console");
		if (mappermain.initConsole() != 0){
			return -1;
    }
  }
  
  // initialize mapper
  mapper.init(mappermain.RNDFfilename);
	mapper.setLocalFrameOffset(mappermain.state);
	mapper.disableLineFusion(mappermain.options.disable_line_fusion_flag);
	mapper.disableObsFusion(mappermain.options.disable_obs_fusion_flag);
	mapper.setFusionTimeThresh(mappermain.options.fusion_time_thresh_arg);


	cerr << "Listening for map elements" << endl;

	while (!mappermain.quit){
    // Do heartbeat occasionally
    if (DGCgettime() - mappermain.processTime > 500000)
      {
        mappermain.processTime = DGCgettime();
        mappermain.updateProcessState();
      }
    // update and send the graph roughly every 4 seconds
    if (DGCgettime() - mapper.graphTime > /*4000000*/500000){
      mapper.graphTime = DGCgettime();
      mapper.updateGraph();

      if (mappermain.debugSubGroup!=0){
        for (unsigned int i =0;i<mapper.map.prior.fulldata.size();++i){
          if(mapper.map.prior.getElFull(mapper.priorEl,i))
            mappermain.mapTalker->sendMapElement(&mapper.priorEl, mappermain.debugSubGroup);
        }
      }
    }

      
    // update the console

    if (mappermain.console){
      if (DGCgettime() - mappermain.consoleTime > 100000){
        cotk_update(mappermain.console);			
        if (mappermain.debugSubGroup !=0)
          cotk_printf(mappermain.console, "%elements%", A_NORMAL, "%d received, debug to subgroup %d ", mapper.totalReceived, mappermain.debugSubGroup);
        else
          cotk_printf(mappermain.console, "%elements%", A_NORMAL, "%d received, no debug ", mapper.totalReceived);


        cotk_printf(mappermain.console, "%filter%", A_NORMAL, "%d fused, %d msec               " 
                    , mapper.totalFused, mapper.fusedDeltaTime);

        cotk_printf(mappermain.console, "%map%", A_NORMAL, " size = %d    " 
                    , mapper.map.data.size());


      }
    }
    if (mappermain.pause){
      // disable pause for now as it causes spread errors
      //   usleep(100000);
      //    continue;
    }

		if (mappermain.mapTalker->recvMapElementBlock(&mapper.sensedEl,mappermain.recvSubGroup) >0){
      
			mapper.totalReceived++;
			
			if (mappermain.options.no_state_flag) {
				// update MapPrior delta using the state in the current object
				if (mapper.sensedEl.state.timestamp!=0){
					mappermain.state = mapper.sensedEl.state;
				}
			}else{
				mappermain.state = mappermain.stateTalker->getstate();
			}
			mapper.setLocalFrameOffset(mappermain.state);				



			// update the map
			if (mapper.updateMap() !=0)
				break;

			mappermain.mapTalker->sendMapElement(&mapper.sensedEl,mappermain.sendSubGroup);
    	if (mappermain.debugSubGroup != 0){
				mappermain.mapTalker->sendMapElement(&mapper.sensedEl,mappermain.debugSubGroup);
			}

		}
  }	
	
  mapper.fini();
	mapper.finiComm();
	mappermain.finiConsole();
  mappermain.finiComm();
	
	return 0;
}

