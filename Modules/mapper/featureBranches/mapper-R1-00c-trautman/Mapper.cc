/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-02-02 14:39:26 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;


Mapper::Mapper()
{}

Mapper::~Mapper()
{}

int Mapper::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 

 // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
}

static char *consoleTemplate =
"Mapper Lite (Stopline version)                                             \n"
"                                                                           \n"
"Skynet Info:   %spread%                                                    \n"
"Message Stats: %lines%                                                     \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%]                                                    \n"
"                                                                           \n"
"%stdout%                                                                   \n";

// Initialize console display
int Mapper::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
    
  // Initialize the display
  cotk_open(this->console);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "Key = %d",
							this->skynetKey);

  return 0;
}


// Finalize sparrow display
int Mapper::closeConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize skynet
int Mapper::initComm()
{
	this->m_skynet = new skynet(MODmapping, this->skynetKey, NULL);
	this->recSocket = this->m_skynet->listen(SNroadLine,  MODmapping);
	this->sndSocket = this->m_skynet->get_send_sock(SNtrafficLocalMap);

  return 0;
}


// Clean up skynet
int Mapper::closeComm()
{
	delete this->m_skynet;
  return 0;
}

int Mapper::checkRead()
{
	return (this->m_skynet->is_msg(this->recSocket));
}

int Mapper::readLines()
{
	int readval = (this->m_skynet->get_msg(this->recSocket, &this->lineMsgIn, sizeof(this->lineMsgIn),0));
	this->printLinesIn();
	this->totalReceived++;
	return 0;
}

int Mapper::writeLines()
{
	int bytesSent;
	bytesSent = this->m_skynet->send_msg(this->sndSocket, &this->lineMsgOut, sizeof(this->lineMsgOut), 0);

	this->printLinesOut();
	this->totalSent++;
	return 0;

}

int Mapper::printLinesIn()
{
	for (int i= 0; i <this->lineMsgIn.num_lines;++i){
		cerr<< "Line In " << i << " = (" 
				<< this->lineMsgIn.lines[i].a[0] << ","
				<<this->lineMsgIn.lines[i].a[1] << ","
				<<this->lineMsgIn.lines[i].a[2] << ")" 
				<< ", (" << this->lineMsgIn.lines[i].b[0] << ","
				<<this->lineMsgIn.lines[i].b[1] << ","
				<<this->lineMsgIn.lines[i].b[2] << ")" << endl;
	}
}

int Mapper::printLinesOut()
{
	for (int i= 0; i <this->lineMsgOut.num_lines;++i){
		cerr<< "Line Out " << i << " = (" 
				<< this->lineMsgOut.lines[i].a[0] << ","
				<<this->lineMsgOut.lines[i].a[1] << ","
				<<this->lineMsgOut.lines[i].a[2] << ")" 
				<< ", (" << this->lineMsgOut.lines[i].b[0] << ","
				<<this->lineMsgOut.lines[i].b[1] << ","
				<<this->lineMsgOut.lines[1].b[2] << ")" << endl;
	}
}

// Handle button callbacks
int Mapper::onUserQuit(cotk_t *console, Mapper *self, const char *token)
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int Mapper::onUserPause(cotk_t *console, Mapper *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



// Handle fake stop button
int Mapper::onUserFake(cotk_t *console, Mapper *self, const char *token)
{
  float px, py, pz;
    
  //MSG("creating fake stop line data");

	double simX = 3778086;
	double simY = 396404;;

	self->lineMsgOut.lines[0].a[0] = simX;
	self->lineMsgOut.lines[0].b[0] = simX;
	self->lineMsgOut.lines[0].a[1] = simY;
	self->lineMsgOut.lines[0].b[1] = simY;      
    
  self->lineMsgOut.num_lines = 1;
  
  self->totalFakes += 1;
	self->writeLines();
  return 0;
}



int Mapper::filterLines()
{

	//Passthrough for now
	
	int num_lines_in = this->lineMsgIn.num_lines;
	cerr <<"Received and filtering " << num_lines_in<< " lines " <<endl;
	for (int i=0; i<num_lines_in; ++i){
		this->lineMsgOut.lines[i] = this->lineMsgIn.lines[i] ;

	}
	if (num_lines_in>0)
		this->lineMsgOut.num_lines = 1;
	else 
		this->lineMsgOut.num_lines = 0;

	return 0;
}








const bool DEBUG=1;
// Main program thread
int main(int argc, char **argv)
{
	bool DEBUG=1;

	Mapper *mapper;
	int msgId, msgLen;
	mapper = new Mapper();
  assert(mapper);

	mapper->parseCmdLine(argc, argv);
	mapper->initComm();

	
	if (!mapper->options.disable_console_flag)
		if (mapper->initConsole() != 0)
			return -1;

	cerr << "Listening for lines" << endl;

	while (!mapper->quit){
		
		if (mapper->console)
			cotk_update(mapper->console);

		if (mapper->pause){
			usleep(0);
			continue;
		}

		if (mapper->checkRead()){
			mapper->readLines();
			mapper->filterLines();
			mapper->writeLines();

		}else{
			usleep(1000);
			if (mapper->console){
				cotk_printf(mapper->console, "%lines%", A_NORMAL, "%d received, %d sent" 
										, mapper->totalReceived, mapper->totalSent);
			}
		}
	}
	mapper->closeConsole();
	mapper->closeComm();

	return 0;
}


	
	

	
		
	
	
	
		


