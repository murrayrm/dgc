/**
 * File: RoadMap.hh
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: Jul 12
 **/


#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>
//#include <GL/glut.h>

#undef border //this is defined in ncurses and conflicts with some functions.


#include <alice/AliceConstants.h>
#include <cotk/cotk.h>
#include <dgcutils/DGCutils.hh>
#include <emap/emap.hh>

#include <frames/pose3.h>
#include <frames/coords.hh>
#include <frames/ellipse.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/MapElementMsg.h>
#include <interfaces/ProcessState.h>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>

using namespace std;

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define MINROADWIDTH 3 //any segment narrower than this is not "road"
#define GAPDISTANCE_ROAD .03 //consecutive vertial discontinuity to call non-road
#define VERTICALRESOLUTION .01 // (m) size of one increment in cell
#define HORIZONTALRESOLUTION 1.0 //(m) cell size
#define NODATA 0x0000 // if we have no data ... 
#define VERTICALCENTER 0x8888 //center of map...
#define MAPRADIUS 80 //#cells to each side of Alice
#define GAPDISTANCE_GROUNDSTRIKE .2  //(m) how far off the road map obj can be and be called groundstrike

#define MINNUMPOINTS 20

#define MAXRANGE 80.0 //any range greater than this is no-return point
#define MINRANGE .1 //used in case we get returns off the ladar box

#define NUMSCANPOINTS 181

//for accessing the rawData array
enum {
  ANGLE,
  RANGE
};

struct myState {
  double X;
  double Y;
  double Z;
  double Theta;
};

// LADAR blob perceptor class
class RoadMap : public CMapElementTalker
{
public:

  // Constructor
  RoadMap();

  // Destructor
  ~RoadMap();
  
  // Parse the command line
  int Init();
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Update the map
  int update();

  //update the display
  int updateDisplay();

  //init the display
  int initDisplay();

  //useful function to calculate dist between 2 pts
  double dist(double x1, double y1, double x2, double y2) {
    return sqrt(pow(x1-x2,2)+pow(y1-y2,2));
  }


  /*************TRACKING FUNCTIONS****************/


  /**
   * Initializes all tracking variables:
   *   matrices for KF calculations
   *   stack of indices for objects & cars
   *   sets up logfile
   *   sets counters to 0
   **/
  int roadMapInit();

  /**
   * deals with incoming range data from feeder
   * (called by main loop when new scan arrives)
   * calls the sequence of fxns that perform 
   *   operations on the new data
   **/
  void processScan(int ladarID);

  /**
   * segments a single scan, using the discontinuity 
   * requirement does not yet impose convex requirement.
   * TODO: add Convex requirement, or better threshold
   **/
  void segmentScan();

  void updateMap();

  double checkGroundstrike(point2arr ptarr);

public:
  
  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;

  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet replay module
  sensnet_replay_t *replay;

  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  unsigned long long startTime, origStartTime;
  unsigned long long endTime;
  unsigned long long currTime;
  double diffTime;



  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //how many times we've looped in the main program
  int loopCount;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];


  /************ defines from trackMO ***************/

  //angle, range data
  double rawData[NUMSCANPOINTS][2];

  //x,y,z data (in local frame)
  double xyzData[NUMSCANPOINTS+1][3];

  //vehicle state at time of scan, used for figuring 
  //which corner to add
  myState currState;
  myState ladarState;
  VehicleState rawState;

  //variables for the maptalker stuff
  int subgroup; 
  int mapCarID, mapObjID;
  int outputSubgroup;
  int debugSubgroup;
  int numSensorIds;  



  //vars for segmentation
  int posSegments[NUMSCANPOINTS][2]; 
  int sizeSegments[NUMSCANPOINTS]; 
  int numSegments; //number of objects found by segmentScan
  int numFrames; //how many frames we've processed
  double diffs[NUMSCANPOINTS-1];

  //vars for elevation map
  
  EMap *tamasMap;
  double mapCenterX, mapCenterY;
  double origMapHeight; //set at initialization (used to scale the map)

  int currSensor;

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


