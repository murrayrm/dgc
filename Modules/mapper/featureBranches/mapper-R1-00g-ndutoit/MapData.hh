/**********************************************************
 **
 **  MAPDATA.HH
 **
 **    Time-stamp: <2007-02-20 12:08:50 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPDATA_H
#define MAPDATA_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "interfaces/MapElement.hh"


class MapData
{
	
public:
	
	MapData();
	
	~MapData();


	vector<MapElement> data;
	MapPrior prior;


};



#endif
