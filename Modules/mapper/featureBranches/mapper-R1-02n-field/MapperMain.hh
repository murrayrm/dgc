/**********************************************************
 **
 **  MAPPERMAIN.HH
 **
 **    Time-stamp: <2007-08-21 04:48:27 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Aug 18 05:25:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPERMAIN_H
#define MAPPERMAIN_H


#include <iostream>
#include <assert.h>
#include <vector>
#include <math.h>

#include "dgcutils/DGCutils.hh"

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <interfaces/ProcessState.h>
#include <interfaces/SensnetTypes.h>

#include <map/MapElement.hh>

// local headers
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/StateClient.hh"

#include <sensnet/sensnet.h>


//GRAPH #include "travgraph/Graph.hh"
#include "rndf/RNDF.hh"

#include <ncurses.h>
// CLI support
#include <cotk/cotk.h>
// Cmd-line handling
#include "cmdline.h"

#include "map/Map.hh"

using namespace std;

class CStateWrapper;


class MapperMain 
{

public:
	

	MapperMain();

	~MapperMain();
	
public:

 

	/// Parse the command line
	int parseCmdLine(int argc, char**argv);

  /// Initilize communications 
	int initComm();
  /// Clean up communications
	int finiComm();

  /// Update the process state
  int updateProcessState();


  /// Initialize console
	int initConsole();
  /// Clean up console
	int finiConsole();



  /// Console button callback
	static int onUserQuit(cotk_t *console, MapperMain *self, const char *token);
  /// Console button callback  
  static int onUserPause(cotk_t *console, MapperMain *self, const char *token);
  	  /// Console button callback  
  static int onUserReset(cotk_t *console, MapperMain *self, const char *token);
  /// Time that the last process heartbeat was sent
  uint64_t processTime;

  /// Time that the console is updated
  uint64_t consoleTime;

  /// Time that the frame offset is updated from state
  uint64_t stateTime;

  /// map element talker subgroup to send map data
  int sendSubGroup;
  /// map element talker subgroup to receive sensed data
	int recvSubGroup;
  /// map element talker subgroup to visualize debugging data
	int debugSubGroup;


  /// RNDF filename given on startup
	string RNDFfilename;

 /// Console interface
	cotk_t *console;
	/// Should we quit?
  bool quit;
	/// Should we pause?
	bool pause;
	/// Should we reset?
	bool reset;

 /// Talker to monitor state
  CStateWrapper * stateTalker;

 
  /// Sensnet module to handle process messages
  sensnet_t *sensnet;
	/// Program options
	gengetopt_args_info options;

	//Skynet settings
	modulename moduleId;
  /// The skynet key
	int skynetKey;
  ///Spread daemon
  char *spreadDaemon;
  	
	/// Vehicle state
  VehicleState state;

};
 
/// Wraps a state client into an object
class CStateWrapper : public CStateClient
{
public:
  CStateWrapper(int snkey) : CSkynetContainer(MODmapping,snkey),CStateClient(true) { };
  ~CStateWrapper() {};
  
  VehicleState getstate() {UpdateState();return m_state;}

};

#endif
