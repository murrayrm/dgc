/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-02-02 10:56:58 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;


Mapper::Mapper()
{}

Mapper::~Mapper()
{}

int Mapper::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 

 // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
}

static char *consoleTemplate =
"Mapper $Revision$                                                          \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Lines : %stops%                                                            \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%]                                                    \n";

// Initialize console display
int Mapper::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
    
  // Initialize the display
  cotk_open(this->console);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "Key = %d",
							this->skynetKey);

  return 0;
}


// Finalize sparrow display
int Mapper::closeConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Initialize skynet
int Mapper::initComm()
{
	this->m_skynet = new skynet(MODmapping, this->skynetKey, NULL);
	this->recSocket = this->m_skynet->listen(SNroadLine,  MODmapping);
	this->sndSocket = this->m_skynet->get_send_sock(SNtrafficLocalMap);

  return 0;
}


// Clean up skynet
int Mapper::closeComm()
{
	delete this->m_skynet;
  return 0;
}

int Mapper::readLines()
{
	int readval = (this->m_skynet->get_msg(this->recSocket, &this->lineMsgIn, sizeof(this->lineMsgIn),0));
	
	return 0;
}

int Mapper::writeLines()
{
	int bytesSent;
	bytesSent = this->m_skynet->send_msg(this->sndSocket, &this->lineMsgOut, sizeof(this->lineMsgOut), 0);

 cout << "lines object sent of size " << bytesSent << endl;
	return 0;

}


// Handle button callbacks
int Mapper::onUserQuit(cotk_t *console, Mapper *self, const char *token)
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int Mapper::onUserPause(cotk_t *console, Mapper *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}



// Handle fake stop button
int Mapper::onUserFake(cotk_t *console, Mapper *self, const char *token)
{
  float px, py, pz;
    
  //MSG("creating fake stop line data");

	double simX = 3778086;
	double simY = 396404;;

	self->lineMsgOut.lines[0].a[0] = simX;
	self->lineMsgOut.lines[0].b[0] = simX;
	self->lineMsgOut.lines[0].a[1] = simY;
	self->lineMsgOut.lines[0].b[1] = simY;      
    
  self->lineMsgOut.num_lines = 1;
  
  self->totalFakes += 1;

  return 0;
}



int Mapper::filterLines()
{

	//Passthrough for now
	double simX = 3778086;

	int num_lines_in = this->lineMsgIn.num_lines;
	cout <<"Received and filtering " << num_lines_in<< " lines " <<endl;
	for (int i=0; i<num_lines_in; ++i){
		this->lineMsgOut.lines[i] = this->lineMsgIn.lines[i] ;

	}
	if (num_lines_in>0)
		this->lineMsgOut.num_lines = 1;
	else 
		this->lineMsgOut.num_lines = 0;

	return 0;
}








const bool DEBUG=1;
// Main program thread
int main(int argc, char **argv)
{
	bool DEBUG=1;

	Mapper *mapper;
	int msgId, msgLen;
	mapper = new Mapper();
  assert(mapper);

	mapper->parseCmdLine(argc, argv);
	mapper->initComm();

	
	//	if (!mapper->options.disable_console_flag)
	//	if (mapper->initConsole() != 0)
	//		return -1;

	while (!mapper->quit){
		
		//if (mapper->console)
		//	cotk_update(mapper->console);

		//if (mapper->pause){
		//	usleep(0);
		//	continue;
		//}

		cout << "Listening" << endl;
		if (mapper->readLines()==0){
			cout << "Read "<< mapper->lineMsgIn.num_lines << " lines" << endl;
			if (DEBUG){
				for (int i= 0; i <mapper->lineMsgIn.num_lines;++i){
					cout<< "Line " << i << " = (" 
							<< mapper->lineMsgIn.lines[0].a[0] << ","
							<<mapper->lineMsgIn.lines[0].a[1] << ","
							<<mapper->lineMsgIn.lines[0].a[2] << ")" 
							<< ", (" << mapper->lineMsgIn.lines[1].a[0] << ","
							<<mapper->lineMsgIn.lines[1].a[1] << ","
							<<mapper->lineMsgIn.lines[1].a[2] << ")" << endl;
				}
			}
			mapper->filterLines();
			mapper->writeLines();

		}
			
		if (mapper->console){
			cotk_printf(mapper->console, "%lines%", A_NORMAL, "%d received, %d sent" , mapper->totalReceived, mapper->totalSent);
		}
	}

	mapper->closeConsole();
	mapper->closeComm();

	return 0;
}


	
	

	
		
	
	
	
		


