              Release Notes for "mapper" module

Release R1-02-tamas (Tue Sep 11 21:09:48 2007):
	Two one-line changes: removed sensnet_wait() from PTU ladar read cycle (for groundstrike filtering), since it was causing spread errors when 
the PTU ladarfeeder wasn't running, and added the RF_ROOF ladar to the list of ladars to use for groundstrike filtering.

Release R1-02 (Tue Sep 11  2:23:01 2007):
	adding updateFromGeometry() call, now that isOverlap actually uses
	that data...

Release R1-01z (Sun Sep  9 16:52:53 2007):
	adding range check on incoming mapElement IDs

	now resets the avg/max stats every 5 secs; removing deprecated
	function

Release R1-01y (Wed Sep  5 12:01:40 2007):	
	Changes from el toro: 
	*properly handles case when there's no more space in the map 
	* using unsigned long long for all DGCgettime operations 
	* handles case when vector of GPCclipped polygons is
	size 0 (not sure why this ever happens; looking into it) 
	* fixed	clear msgs to mapviewer 

Release R1-01x (Tue Sep  4  2:47:39 2007):
	Added line fusion to the main mapper loop, scheduled at 10Hz.
        Added commandline option to the mapper to revert back to
        the old behavior (--line-fusion-compat).

Release R1-01w (Tue Sep  4  0:19:56 2007):
	Some changes, made and tested @ Santa Anita Monday evening: 

	*mapper has a more informative cotk display (only works running
	external to planner)  
	* mapper sends updates to planner more infrequently: only 
	when objects are fused, rather than every time an update 
	comes from a perceptor.  

	* command line option controlling rate of fusion (also 
	controls update rate to planner) 

	* removed some debugging messages

	* now properly handles fusing the timeStopped field  in MapElements,
	and removes unused fields from the objectData struct


Release R1-01v (Sun Sep  2 16:11:09 2007):
	This version has been tested in simulation (stlukeSmallStandard.py)
	It properly handles running internal or external to planner, with
	cars and objects being added and removed. Didn't seem too slow, 
	but we'll test w/ real data on Alice this evening.

	*adding roof left ladar to perceptor enum (allows us to run w/
	MODladarCarPerceptorRoofLeft, fixing bug found by Jessica) 

	*fixing bug - now actually calls fusion at right timesteps 

	*only requires old data to remove object (perceptors don't have to send
	clear)

	* properly handle non-obstacle elements (thanks Daniele for catching this)

	*fixed interface w/ planner for running mapper external. 


Release R1-01u (Sat Sep  1  8:03:10 2007):
	using new map data structure - the decayMap function now works,
	clear is handled correctly, and a few other changes...	Tested in
	w/ logged data + ladarperceptors, and w/
	system-test/stlukeSmallStandard.py


Release R1-01t (Mon Aug 27 15:21:28 2007):
	When element from ladar-car-perceptor is classified as groundstrike, it is set to type clear and sent to internal map and planner

Release R1-01s (Thu Aug 23 13:41:16 2007):
	added line to Makefile in mapper to link RoadMap.hh against the mapper library.

Release R1-01r (Thu Aug 23  5:10:43 2007):
	groundstrike detection now works in mapper. turn on using
	--enable-groundstrike-filtering (it's off by default)  This was
	tested on data from el toro (short, steep hills); it will probably
	need tuning to work well @ st luke (plan to collect logs today)


Release R1-01q (Tue Aug 21  6:12:40 2007):
	Enabled optional decay of elements such that if the element isn't
	updated for a given number of seconds, the element is removed from
	all copies of the map.	By default the decay is off, but can turned
	on by setting the command line option --decay-thresh=#of secs from
	mapper or --mapper-decay-thresh=# of secs if run from planner.

Release R1-01p (Mon Aug 20 21:55:35 2007):
	updated mapper to enable it to be instantiated as an object and run
	inside tplanner. Mapper still works as a standalone program.

Release R1-01o (Mon Aug 20 17:03:09 2007):
removing debugging fprintf's (was going to make build release anyways)

Release R1-01n (Tue Aug 14 23:09:12 2007):
	Added a commandline option to disable line fusion
        (--disable-line-fusion). This is only useful (not) to see the
        fused rndf in the mapviewer, but it doesn't get sent to the planner.
        The planner has its own Map object and cmdline option for that.

Release R1-01m (Tue Aug 14 12:59:33 2007):
	When using the --no-state option, use the state from the
	MapElements to set the global <-> local delta in MapPrior. This
	allows to run sensnet log files and have the rndf overlap correctly
	with alice (needed to test lane fusion).

Release R1-01l (Mon Aug  6 21:12:28 2007):
	changing default object type from vehicle to obstacle, at noel's request

Release R1-01k (Sun Aug  5 22:07:30 2007):
	committing field changes to mapper - small change to the logic in obstacle fusion

Release R1-01j (Tue Jul 31 14:58:43 2007):
	first cut at fusion, with improved logic for which data to use from each perceptor. 

Release R1-01i (Thu Jul 19 18:17:47 2007):
	- Fixed minimum distance check (was not updating mindist).
        - Only fuse an obstacle with another obstacle (and do not
        fuse other map elements at all, such as lane lines).
	- Send map elements to both the send subgroup and the debug
        subgroup, if this latest one is not zero.

Release R1-01h (Tue Jul 17  8:06:56 2007):
	Improved comments and internal structure.  Added basic fusion
	capability and command line options to adjust fusion thresholds. 
	Implemented process control interface.	Warning for the navigation
	package.  Mapper now depends on sensnet for the process control
	interface, so that might be an issue and need to bee added to the
	nav package.  Implemented --debug-subgroup=num commandline option
	to specify where to visualize RNDF and other debugging info

Release R1-01g (Thu Jun 28  2:49:04 2007):
	updated testMapper test 1 to work with no asim or astate running
	and pulls state info from map elements sent to debugging channel -2

Release R1-01f (Mon Jun  4 20:20:01 2007):
	updated testMapper to include noisy measurements for the placed
	obstacles.  This is to help testing in a more realistic map
	situation where the obstacles are not perfectly stable and can
	drop in and out at longer range.  The obstacles will always be
	seen at less than 20 meters away, and will increasingly drop in
	and out up to 30 meters which is set as the outer range.  The
	noise value for position and radius will not be more than .3
	meters in any direction.  "testMapper rndffile 3" will allow
	placement of noisy obstacles and should be used as much as
	possible.  "testMapper rndffile 2" is the old test which places
	obstacles without noise, but the max range to see them is reduced
	from 50 meters to 30 meters which is more accurate, and the color
	will change to yellow when the obstacle is visible.

Release R1-01d (Tue May 22 17:16:55 2007):
	updated testMapper test 2 to not send obstacles at 0,0 at startup. 
	Obstacles won't be sent until they're placed

Release R1-01c (Fri May 18 13:58:41 2007):
	fixed bug in testMapper.cc where placed obstacles had a zero height

Release R1-01b (Tue May 15 17:03:24 2007):
	Updated mapper to use with new map module.  Forgot to release this
	earlier.

Release R1-01a (Mon Apr 30 20:45:15 2007):
	fixed testMapper to work with new map element talker

Release R1-01 (Mon Apr 30 12:36:30 2007):
	Updated mapper to work with new map module.  Implements sub channel
	communication and variable length messages

Release R1-00s (Tue Mar 20  9:12:42 2007):
	Fixed rndf initialization error.  Now uses state to initialize global->local frame offset.

Release R1-00r (Sun Mar 18  9:23:39 2007):
	Added travgraph update to mapper. 

Release R1-00q (Sun Mar 18  8:56:13 2007):
	Minor update to remove SkynetTalker.cc

Release R1-00p (Sat Mar 17 13:18:33 2007):
	Updated COLOR_ prefix for map elements to MAP_COLOR_ to work with
	map changes

Release R1-00o (Sat Mar 17 10:15:29 2007):
	updated mapper to send a graph at every 5 seconds

Release R1-00n (Sat Mar 17  0:43:23 2007):
	cleaned up some testMapper interface.

Release R1-00m (Thu Mar 15 15:21:33 2007):
	updated some testMapper functionality.	Added ability to add up to
	4 obstacles and remove them as well

Release R1-00l (Wed Mar 14 23:14:22 2007):
	added basic simulation of static obstacles to mapper via command
	testMapper.  Can extend to dynamic obstacles as needed.


Release R1-00k (Fri Mar  9 17:08:00 2007):
		updated mapper to work with new MapElementTalker location

Release R1-00j (Sun Mar  4 17:42:05 2007):
	Mapper now sends graph to mplanner.  Also fixed a bug where rndf
	commandline option was being ignored.

Release R1-00i (Thu Mar  1 19:32:43 2007):
	added -lframes to Makefile.  Updated main loop in Mapper.cc to
	better handle large numbers of received messages without delays.

Release R1-00h (Tue Feb 27  8:48:46 2007):
Separated new map object into Map module.

Release R1-00g (Wed Feb 21 20:00:25 2007):
	Mapper now parses RNDF file and sends RNDF data out.  Also reads in
	sensor based map elements and passes them through.  No fusion or
	correlation between sensed data and prior RNDF data yet.  This
	should be good enough for RNDF lane + sensor-based obstacle testing
	for now.


Release R1-00f (Sun Feb  4 18:55:37 2007):
	Checking in changes from field test.  Also fixing to work with
	updated cotk module

Release R1-00e (Sat Feb  3 23:17:58 2007):
		added temporary frame transformation to output lines in global frame

Release R1-00d (Sat Feb  3  9:54:45 2007):
added basic filtering methods

Release R1-00c (Fri Feb  2 14:50:19 2007):
Added basic command line display for mapper using cotk. You can now
	output spoof test messages to planner from this interface.   Added
	mapper-test-input command lineprogram to spoof messages from lineperceptor 
	for testing and debugging the input to mapper and pathway to planner.  
	Updated Makefile.yam to compile all.

Release R1-00b (Fri Feb  2 11:01:30 2007):
	Updated mapper to receive stopline data from lineperceptor and pass data to tplanner.  Right now the lines filter is just a passthrough.  

Release R1-00a (Sat Jan 27 18:31:26 2007):
	Import basic mapper module files

Release R1-00 (Sat Jan 27 16:33:49 2007):
	Created.















































