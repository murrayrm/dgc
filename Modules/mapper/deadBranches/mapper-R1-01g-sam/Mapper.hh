/**********************************************************
 **
 **  MAPPER.HH
 **
 **    Time-stamp: <2007-07-17 04:45:17 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:39 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <assert.h>
#include <vector>
#include <math.h>

#include "dgcutils/DGCutils.hh"

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <interfaces/ProcessState.h>
#include <interfaces/SensnetTypes.h>

#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

// needed for mplanner interface
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
// local headers
#include "skynettalker/SkynetTalker.hh"
#include "skynettalker/StateClient.hh"

#include <sensnet/sensnet.h>


#include "travgraph/Graph.hh"
#include "rndf/RNDF.hh"

#include <ncurses.h>
// CLI support
#include <cotk/cotk.h>
// Cmd-line handling
#include "cmdline.h"

#include "map/Map.hh"

using namespace std;

class CStateWrapper;

class Mapper 
{
	
public:
	

	Mapper();

	~Mapper();
	
public:

	/// Parse the command line
	int parseCmdLine(int argc, char**argv);

  /// Initilize communications 
	int initComm();
  /// Clean up communications
	int finiComm();



  /// Initialize console
	int initConsole();
  /// Clean up console
	int finiConsole();

 /// Initalize map
  int init();

  /// Clean up map
  int fini();

  /// Update the graph and send it to mplanner
  int updateGraph();

  /// Update the process state
  int updateProcessState();

  /// Clears the map
	int resetMap();

  /// Updates the map with new data
	int updateMap();

  /// Takes in a map element and checks if it can be reasonably associated 
  /// with other existing elements in the map.  If so, the element is 
  /// modified and fused into the map.

	int fuseMapElement(MapElement & fusedEl);

  /// Console button callback
	static int onUserQuit(cotk_t *console, Mapper *self, const char *token);
  /// Console button callback  
  static int onUserPause(cotk_t *console, Mapper *self, const char *token);
  /// Console button callback  
  static int onUserFake(cotk_t *console, Mapper *self, const char *token);
  /// Console button callback  
  static int onUserReset(cotk_t *console, Mapper *self, const char *token);

  /// Console interface
	cotk_t *console;
	/// Should we quit?
  bool quit;
	/// Should we pause?
	bool pause;

  /// total number of map elements received
	int totalReceived;
  /// total map size
	int totalMapSize;
  /// total number of map elements fused
	int totalFused;
  /// the time difference between fused elements
  int fusedDeltaTime;

  /// total fake map element sent
	int totalFakes;

	/// Program options
	gengetopt_args_info options;

	//Skynet settings
	modulename moduleId;
  /// The skynet key
	int skynetKey;
  ///Spread daemon
  char *spreadDaemon;
  /// map element talker subgroup to send map data
	int sendSubGroup;
  /// map element talker subgroup to receive sensed data
	int recvSubGroup;
  /// map element talker subgroup to visualize debugging data
	int debugSubGroup;



  /// Talker to send RNDF graphs to mplanner
	SkynetTalker<Graph> *graphTalkerPtr;
  /// RNDF graph to send to mplanner
	Graph* rndfGraphPtr;
  /// RNDF structure used by mplanner graph
  RNDF rndf;

  /// Talker to monitor state
  CStateWrapper * stateTalker;

  /// Talker to send and receive map elements
  CMapElementTalker mapTalker;

  /// Sensnet module to handle process messages
  sensnet_t *sensnet;

  /// Vehicle state
  VehicleState state;

  /// Time that the last graph was sent
  uint64_t graphTime;

  /// Time that the last process heartbeat was sent
  uint64_t processTime;

  /// Time that the console is updated
  uint64_t consoleTime;


  /// RNDF filename given on startup
	string RNDFfilename;

	/// Incoming line message
	MapElement inputEl;

	/// Outgoing line message
	MapElement outputEl;

  /// Main data structure which stores the prior and sensed map
	Map map;

};


/// Wraps a state client into an object
class CStateWrapper : public CStateClient
{
public:
  CStateWrapper(int snkey) : CSkynetContainer(MODmapping,snkey),CStateClient(true) { };
  ~CStateWrapper() {};
  
  VehicleState getstate() {UpdateState();return m_state;}

};



#endif
