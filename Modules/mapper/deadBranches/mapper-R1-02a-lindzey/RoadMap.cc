/**
 * File: RoadMap.cc
 * Description:
 *   Uses bumper ladar to track objects.
 *   This file has the functions handling all
 *   LADAR interfaces, as well as initializing
 *   everything and running the main loop
 * Based on the Obsperceptor template by Andrew
 * Last Changed: March 9
 **/

#include "RoadMap.hh"


//#include "emapViewer.cc"
// Default constructor
RoadMap::RoadMap()
{
  memset(this, 0, sizeof(*this));

 
  return;
}


// Default destructor
RoadMap::~RoadMap()
{
}

/**
  int RoadMap::updateDisplay() {
    setEMap(this->tamasMap, 0, 0xFFFF);
  }

  //init the display
  int RoadMap::initDisplay() {
    startViewer(NULL, NULL,2*MAPRADIUS+1);
  }
**/

// Parse the command line
int RoadMap::Init()
{
  if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;

     debugSubgroup = -3;

     outputSubgroup = 0;


  // Fill out module id
  this->moduleId = modulenamefromString("MODmapperRoadMap");

  mapCarID = this->moduleId;

  return 0;
}



// Initialize sensnet
int RoadMap::initSensnet()
{
  sensnet_id_t sensorIds[16];
  Ladar *ladar;
    
  // Create sensnet interface
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Default ladar set
  numSensorIds = 0;
    sensorIds[numSensorIds++] = SENSNET_PTU_LADAR;
    sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;

  // Initialize ladar list
  for (int i = 0; i < numSensorIds; i++)
    {
      assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
      ladar = this->ladars + this->numLadars++;

      // Initialize ladar data
      ladar->sensorId = sensorIds[i];

      // Join the ladar data group
      if (this->sensnet)
        {
          if (sensnet_join(this->sensnet, ladar->sensorId,
                           SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
            return ERROR("unable to join %d", ladar->sensorId);
        }
    }
  
  // Subscribe to process control messages
  if(sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  return 0;
}


// Clean up sensnet
int RoadMap::finiSensnet()
{
  if (this->sensnet)
    {
      int i;
      Ladar *ladar;
      for (i = 0; i < this->numLadars; i++)
        {
          ladar = this->ladars + i;
          sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
        }
      sensnet_free(this->sensnet);
      this->sensnet = NULL;
    }
  else if (this->replay)
    {
      sensnet_replay_close(this->replay);
      sensnet_replay_free(this->replay);
      this->replay = NULL;
    }


  return 0;
}


// Update the map with new range data
int RoadMap::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  for (i = 0; i < this->numLadars; i++)
    {
      ladar = this->ladars + i;
      currSensor = i;

      if (this->sensnet)
	{
	  // Check the latest blob id
	  if (sensnet_peek(this->sensnet, ladar->sensorId,
			   SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
	    continue;

	  // Is this a new blob?
	  if (blobId == ladar->blobId)
	    continue;

	  // If this is a new blob, read it    
	  if (sensnet_read(this->sensnet, ladar->sensorId,
			   SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
	    break;
	}

      //transformed to sensor frame
      float sfx, sfy, sfz; //sensor frame vars
      LadarRangeBlobScanToSensor(&blob, blob.points[45][0], blob.points[45][1],&sfx, &sfy, &sfz);

      //transformed to vehicle frame
      float vfx, vfy, vfz; //vehicle frame vars
      LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);

      //transformed to local frame
      float lfx, lfy, lfz; //local frame vars
      LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);

      for(int j=0; j < NUMSCANPOINTS; j++) 
        {
          rawData[j][ANGLE] = blob.points[j][ANGLE];
          rawData[j][RANGE] = blob.points[j][RANGE];
          LadarRangeBlobScanToSensor(&blob, blob.points[j][ANGLE], blob.points[j][RANGE],&sfx, &sfy, &sfz);
          LadarRangeBlobSensorToVehicle(&blob, sfx, sfy, sfz, &vfx, &vfy, &vfz);
          LadarRangeBlobVehicleToLocal(&blob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
          xyzData[j][0] = lfx;
          xyzData[j][1] = lfy;
          xyzData[j][2] = lfz;
        }

      currState.X = blob.state.localX;
      currState.Y = blob.state.localY;
      currState.Z = blob.state.localZ;

      // Setting full state for mapper purposes
      rawState = blob.state;

      //call all the tracking functions
      processScan(blob.sensorId);

      if (this->console)
        {
	  char token[64];
          snprintf(token, sizeof(token), "%%ladar%d%%", i);
          cotk_printf(this->console, token, A_NORMAL, "%s %d %8.3f",
                      sensnet_id_to_name(ladar->sensorId),
                      blob.scanId, fmod((double) blob.timestamp * 1e-6, 10000));

	  snprintf(token, sizeof(token), "%%timeelapsed%%");
	  cotk_printf(this->console, token, A_NORMAL, "%f",
		      diffTime);

        }
    } //end cycling through ladars

  return 0;
} //end update()


int RoadMap::roadMapInit()
{

  numFrames = 0;

  initSendMapElement(skynetKey);

  tamasMap = new EMap(2*MAPRADIUS + 1);
  tamasMap->clear(NODATA);
  //  startViewer(NULL, NULL,2*MAPRADIUS+1);
  mapCenterX = 0;
  mapCenterY = 0;


  return 0;
}


void RoadMap::processScan(int ladarID)
{
  //  MSG("processScan called for frame #%d", numFrames);

  //  fprintf(stderr, "\n");
  if(0 == numFrames) {
    origMapHeight = currState.Z;
    mapCenterX = currState.X;
    mapCenterY = currState.Y;
    MSG("setting starting elevation to: %f", currState.Z);
  }

  numFrames++;

  DGCgettime(origStartTime);

  // shift the map
  while(HORIZONTALRESOLUTION < (currState.X - mapCenterX))
    {
      tamasMap->shiftClear(-1,0,NODATA);
      mapCenterX += HORIZONTALRESOLUTION;
      //      MSG("shifting map left");
    }
  while(-HORIZONTALRESOLUTION > (currState.X - mapCenterX))
    {
      tamasMap->shiftClear(1,0, NODATA);
      mapCenterX -= HORIZONTALRESOLUTION;
      //      MSG("shifting map right");
    }
  while(HORIZONTALRESOLUTION < (currState.Y - mapCenterY))
    { 
      tamasMap->shiftClear(0,-1, NODATA);
      mapCenterY += HORIZONTALRESOLUTION;
      //      MSG("shifting map down");
    }
  while(-HORIZONTALRESOLUTION > (currState.Y - mapCenterY))
    {
      tamasMap->shiftClear(0,1, NODATA);
      mapCenterY -= HORIZONTALRESOLUTION;
      //      MSG("shifting map up");
    }



  segmentScan(); 
  updateMap();
           
  DGCgettime(endTime);
  diffTime = 1.0*(endTime - origStartTime);
  if(numFrames % 75 == 0) {
    //    fprintf(stderr, "time elapsed for whole process = %f \n \n", diffTime);
  }


  return;
}



/**
 * This code goes through all the new segments
 * and creates new objects for them. 
 * This includes calculating the occlusions at each edge,
 * calculating the center (excluding no-return points),
 * and storing the relevant x,y,z/range,theta data
 *
 * Fields filled in for objectRepresentation:
 *   dStart, dEnd, seenStart, seenEnd, startPoint, endPoint
 *   lastScan, center, el
 *
 * Parameters defined in .hh file that change operation:
 *   ONE_END_IN_FOREGROUND vs. BOTH_ENDS_IN_FOREGROUND
 **/
void RoadMap::updateMap() {

  int objStart, objEnd;

  point2 startPt, endPt;
  double segLen;
  int zval, xval, yval;
  double xpt, ypt, zpt;
  int mincoord, maxcoord;
  for(int i=0; i<numSegments; i++) 
    {
      int objectsize = sizeSegments[i];

      objStart = posSegments[i][0];
      objEnd = posSegments[i][1];

      startPt = point2(xyzData[objStart][0],xyzData[objStart][1]);
      endPt = point2(xyzData[objStart + objectsize - 1][0],xyzData[objStart+objectsize - 1][1]);

      segLen = dist(startPt.x, startPt.y, endPt.x, endPt.y);

      if(segLen > MINROADWIDTH) {
	for(int j=0; j<objectsize-1; j++) {

	  //determine elevation value
	  zpt = xyzData[objStart + j][2];
	  zval = int((zpt - origMapHeight)/VERTICALRESOLUTION) + VERTICALCENTER;

	  //determine cell it belongs in
	  xpt = xyzData[objStart + j][0];
	  ypt = xyzData[objStart + j][1];

	  xval = (int)(xpt - mapCenterX)/HORIZONTALRESOLUTION + MAPRADIUS;
	  yval = (int)(ypt - mapCenterY)/HORIZONTALRESOLUTION + MAPRADIUS;


	  mincoord = min(xval, yval);
	  maxcoord = max(xval, yval);
	  if((zval > NODATA) && (mincoord > 0) && (maxcoord < (2*MAPRADIUS+1))) {
            //put data in cell
            emap_t *curPixel = tamasMap->getData() + yval * tamasMap->getWidth() + xval;
	    *curPixel = zval;
	  }
	}
      }

    } // end of cycling through segments



} //end of update map



/**
 * This function segments the scan, breaking wherever there is 
 * a discontinuity. (GAPDISTANCE is the parameter in the .hh
 * file controlling how large a discontinuity is permissible)
 * The code will skip discontinuities caused by up to two 
 * no-return points, and only reports segments consising of 
 * MINNUMPOINTS or more points.
 **/
void RoadMap::segmentScan()
{

  for(int i=0; i<NUMSCANPOINTS-1; i++) {
    diffs[i] = xyzData[i+1][2] - xyzData[i][2];
  }

  //number objects segmented from curr scan
  int tempNum = 0; 
  //size of current object
  int currSize = 1; 

  int tempPos[NUMSCANPOINTS][2];
  int tempSize[NUMSCANPOINTS];

  tempSize[0] = 0;

  int noReturns = 0;

  //can't check last 2 points - they wouldn't become an obj anyways
  for(int i=0; i<NUMSCANPOINTS-3; i++) {
    if((rawData[i][RANGE] < MINRANGE) &&(rawData[i][RANGE] > MAXRANGE))
      noReturns++;

    bool thisPtHasReturn;
    thisPtHasReturn = (rawData[i][RANGE] < MAXRANGE ) && (rawData[i][RANGE] > MINRANGE);

    bool thisDiffInRange;
    thisDiffInRange = fabs(diffs[i]) < GAPDISTANCE_ROAD;

    if(!thisPtHasReturn || !thisDiffInRange) { //create new segment
      currSize = 1;
      tempNum++;
 
      //set endpoint of previous object
      tempPos[tempNum-1][1] = i; 

      //set beginning point of new object
      tempPos[tempNum][0] = i+1;

      //initialize size of new object
      tempSize[tempNum] = currSize;


    } else { //this point belongs to current object

      currSize++;
      tempSize[tempNum] = currSize;

    } //end checking diff

  } //end cycling through points
  //  fprintf(stderr, "There were %d no returns in the scan \n", noReturns);


  tempPos[0][0] = 0;
  tempPos[tempNum][1] = NUMSCANPOINTS-1;

  int st,en;
  numSegments = 0;

  //checking that this isn't a group of no-return points
  for(int i=0; i<=tempNum; i++) {
    if(tempSize[i] >= MINNUMPOINTS) {
      st = tempPos[i][0];
      en = tempPos[i][1];
      posSegments[numSegments][0]=st;
      posSegments[numSegments][1]=en;
      sizeSegments[numSegments] = tempSize[i];
      numSegments++;
    }
  }

  return;
}

double RoadMap::checkGroundstrike(point2arr ptarr) 
{
  unsigned int numpts = ptarr.size();
  int numgroundstrikes = 0;

  int gs; //whether this point is a groundstrike

  int zval, xval, yval;
  double xpt, ypt, zpt;
  int mincoord, maxcoord;
  int zmap;

  //  fprintf(stderr, "zvalues for point, and zvalue for map: ");
  for(int i = 0; i < numpts; i++) {

    xpt = ptarr[i].x;
    ypt = ptarr[i].y;
    zpt = ptarr[i].z;

    xval = (int)(xpt - mapCenterX)/HORIZONTALRESOLUTION + MAPRADIUS;
    yval = (int)(ypt - mapCenterY)/HORIZONTALRESOLUTION + MAPRADIUS;
    zval = int((zpt - origMapHeight)/VERTICALRESOLUTION) + VERTICALCENTER;

    mincoord = min(xval, yval);
    maxcoord = max(xval, yval);


    if((zval < 1) || zval > 0xFFFF) {
      gs = 0; //zvalue out of range of map...can't check it
      MSG("zvalue out of range");
    } else if((mincoord > 0) && (maxcoord < (2*MAPRADIUS+1))) {
      zmap = *(tamasMap->getData() + yval * tamasMap->getWidth() + xval);
      //      fprintf(stderr, "(%d, %d)  ", zval, zmap);
      //FIXME: magic number
      if((GAPDISTANCE_GROUNDSTRIKE/VERTICALRESOLUTION) > abs(zmap-zval)) {
	gs = 1; //there's less than 20 cm between observed ground and observed obstacle
      } else { 
	gs = 0;

      }
    } else {
      gs = 0;
      //      MSG("x or y value out of range - (%f, %f)",xpt, ypt);
    }

    numgroundstrikes += gs;

  }
  //  fprintf(stderr, "\n");
  //  MSG("check groundstrike called. %d out of %d were at the ground", numgroundstrikes, numpts);
  double retval = (1.0*numgroundstrikes)/(1.0*numpts);
 return retval;

}
