/**********************************************************
 **
 **  MAPPER.HH
 **
 **    Time-stamp: <2007-02-02 13:39:59 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:39 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <assert.h>
// Sensnet/Skynet support
#include <skynet/sn_types.h>
#include <skynet/sn_msg.hh>
#include <interfaces/RoadLineMsg.h>

#include <ncurses.h>
// CLI support
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"


using namespace std;
//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class Mapper
{
	
public:
	
	//! A Constructor 
	/*! DETAILS */
	Mapper(); 
	
	//! A Destructor 
	/*! DETAILS */
	~Mapper();
	
public:
	
	int parseCmdLine(int argc, char**argv);


	//skynet
	int initComm();
	int closeComm();

	int readLines();
	int checkRead();
	int filterLines();
	int writeLines();
	int printLinesIn();
	int printLinesOut();


	int initConsole();
	int closeConsole();

	static int onUserQuit(cotk_t *console, Mapper *self, const char *token);
static int onUserPause(cotk_t *console, Mapper *self, const char *token);
static int onUserFake(cotk_t *console, Mapper *self, const char *token);
	
	gengetopt_args_info options;

	//Skynet settings
	skynet *m_skynet;
	modulename moduleId;
	int skynetKey;
	int recSocket;
	int sndSocket;


	// Incoming line message
	RoadLineMsg lineMsgIn;

	// Outgoing line message
	RoadLineMsg lineMsgOut;

	cotk_t *console;
	
	bool quit;
	bool pause;
	bool show;
	int totalReceived;
	int totalSent;
	int totalFakes;
};




#endif
