/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-07-17 06:15:45 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


Mapper::Mapper()
{ //--------------------------------------------------
  // initialize time offsets
  //--------------------------------------------------
  graphTime = 0;
  processTime = 0;

  // initialize the console to NULL
  console = NULL;
  pause = 0;
  quit = 0;
  
}

Mapper::~Mapper()
{}

int Mapper::sendMapElementToBoth(MapElement * el)
{
  int ret = mapTalker.sendMapElement(el,sendSubGroup);
  if (ret > 0 && debugSubGroup != 0)
    ret = mapTalker.sendMapElement(el,debugSubGroup);
  return ret;
}

int Mapper::parseCmdLine(int argc, char**argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;
 
   this->moduleId = MODmapping;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");

  if (this->options.rndf_given){
    this->RNDFfilename.assign(this->options.rndf_arg);
  }else{
    return ERROR("Please specify an RNDF file using the option --rndf=filename");
  }
  cout << "Filename in = "  << this->RNDFfilename<< endl;
    
  // Fill out the send subgroup number
  this->sendSubGroup = this->options.send_subgroup_arg;
 
  // Fill out the recv subgroup number
  this->recvSubGroup = this->options.recv_subgroup_arg;

  // Fill out the recv subgroup number
  this->debugSubGroup = this->options.debug_subgroup_arg;
 
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
	
	return 0;
}

static char *consoleTemplate =
"Mapper                                                                \n"
"                                                                           \n"
"Skynet Info:   %spread%                                                    \n"
"Message Stats: %elements%                                                  \n"
"Filter Info: %filter%                                                      \n"
"Map Info: %map%                                                            \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%FAKE%|%RESET%]                                                    \n"
"                                                                           \n"
"%stdout%                                                                   \n";

// Initialize console display
int Mapper::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_button(this->console, "%FAKE%", " FAKE ", "Ff",
                   (cotk_callback_t) onUserFake, this);
	cotk_bind_button(this->console, "%RESET%", " RESET ", "Rr",
									 (cotk_callback_t) onUserReset, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, 
							"Key = %d, Send Subgroup = %d, Receive Subgroup = %d",
							this->skynetKey, this->sendSubGroup, this->recvSubGroup);
 
  return 0;
}


// Finalize sparrow display
int Mapper::finiConsole()
{
  // Clean up the CLI
  if (this->console)
    {
      cotk_close(this->console);
      cotk_free(this->console);
      this->console = NULL;
    }
  
  return 0;
}


// Initialize skynet
int Mapper::initComm()
{

  //--------------------------------------------------
  // initializing state talker
  //--------------------------------------------------
  if(!options.no_state_flag) {
    stateTalker = new CStateWrapper(skynetKey);
  }

  //--------------------------------------------------
  // initialize graphtalker
  //--------------------------------------------------
  graphTalkerPtr = new SkynetTalker<Graph>(skynetKey,SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner); 
	
  
  //--------------------------------------------------
  // join sensnet for process handling
  //--------------------------------------------------
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return -1;

  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");
  cout << "Done with initComm " << endl;
 

 //--------------------------------------------------
  // initialize map elment talker
  //--------------------------------------------------
	this->mapTalker.initRecvMapElement(skynetKey,recvSubGroup);
	this->mapTalker.initSendMapElement(skynetKey);

 
  return 0;
}

int Mapper::finiComm()
{
  delete graphTalkerPtr;
  delete stateTalker;

  if (this->sensnet)
  {
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
    sensnet_disconnect(this->sensnet);
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  return 0;
}


int Mapper::init()
{	
 
  
  //--------------------------------------------------
  // initialize the map prior data
  //--------------------------------------------------
  if (!map.loadRNDF(RNDFfilename.c_str())){
    cerr << "Error:  Unable to load RNDF file to map " << RNDFfilename
         << ", exiting program" << endl;
    exit(1);
    }


  //--------------------------------------------------
  // initialize the rndf graph data
  //--------------------------------------------------
	char* fname = (char*) RNDFfilename.c_str();
	if (!rndf.loadFile(fname)){
    cerr << "Error:  Unable to load RNDF file to graph" << RNDFfilename
         << ", exiting program" << endl;
    exit(1);
	}
	rndf.assignLaneDirection();
  //initialize graph interface with mplanner
	rndfGraphPtr = new Graph(&rndf);
  //send a graph
  graphTalkerPtr->send(rndfGraphPtr);


  
  //--------------------------------------------------
  // Set prior map data offset to local frame from state
  //--------------------------------------------------
  point2 statedelta;
  if(options.no_state_flag) {
    statedelta = point2(0.0,0.0);
  } else {

    state = stateTalker->getstate();

    point2 stateNE, statelocal;
  
    statelocal.set(state.localX,state.localY);
    stateNE.set(state.utmNorthing,state.utmEasting);
    statedelta = stateNE-statelocal;
  }
  //  cout << "state delta = " << statedelta << " northing = " << stateNE << " local = " << statelocal << endl;
   map.setTransform( statedelta);
 


  
  return 0;
}		

int Mapper::fini()
{
  delete rndfGraphPtr;
  return 0;
}


int Mapper::updateGraph()
{
  point2 cpt;
  PointLabel ptlabel;

  unsigned int i;
  for (i=0;i< map.data.size();++i){
    outputEl=map.data[i];
    cpt = outputEl.center;
    if (map.getLastPointID(ptlabel,cpt)==0){
      rndfGraphPtr->addObstacle(ptlabel.segment,ptlabel.lane,ptlabel.point);
    }
  }
  graphTalkerPtr->send(rndfGraphPtr);
  return 0;
}

int Mapper::updateProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = 0;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  if (request.quit) {
    MSG("remote quit request");
  }
  return 0;
}


int Mapper::resetMap()
{
  map.data.clear();
	return 0;
}


// Handle button callbacks
int Mapper::onUserQuit(cotk_t *console, Mapper *self, const char *token) 
{
  //MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int Mapper::onUserPause(cotk_t *console, Mapper *self, const char *token)
{
  self->pause = !self->pause;
  //MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

// Handle button callbacks
int Mapper::onUserReset(cotk_t *console, Mapper *self, const char *token)
{
  self->resetMap();
  return 0;
}

// Handle fake stop button
int Mapper::onUserFake(cotk_t *console, Mapper *self, const char *token)
{
      
  //MSG("creating element data");
	MapElement el;
	self->sendMapElementToBoth(&el); // send to sendSubGroup and debugSubGroup
  return 0;
}


int Mapper::updateMap()
{

  //get map elements
    if (mapTalker.recvMapElementBlock(&inputEl,recvSubGroup) <=0){
      // keep going if there's bad data, but don't add to map
      return 0;
    }    
    totalReceived++;

    if (options.no_state_flag) {
      // update MapPrior delta using the state in the current object
      point2 stateNE, statelocal;
      VehicleState& state = inputEl.state;
      statelocal.set(state.localX,state.localY);
      stateNE.set(state.utmNorthing,state.utmEasting);
      if (stateNE != point2(0, 0)) { // state is valid?
        point2 statedelta = stateNE-statelocal;
        //  cout << "MapEl state delta = " << statedelta << " northing = " << stateNE << " local = " << statelocal << endl;
        map.setTransform( statedelta);
      }
    }

    //fusion is now something that simply acts on the new element 
    //(trying to keep it as separate from the rest of map as possible)
    if (inputEl.isObstacle() && !options.disable_fusion_flag && (ELEMENT_CLEAR != inputEl.type)){
      int sendID;
      sendID = inputEl.id.dat.at(0);
      if(MODradarObsPerceptor == sendID) {
	//for now, use this to keep track of which map elements come from this sensor
	inputEl.type = ELEMENT_OBSTACLE_RADAR; 
      }
      fuseMapElement(inputEl);
    }

    //add to local map representation
    map.addEl(inputEl); 

    //colorcode sent elements for help in debugging
    switch(inputEl.type) {
    case ELEMENT_VEHICLE:
      inputEl.plotColor = MAP_COLOR_DARK_GREEN;
      break;
    case ELEMENT_OBSTACLE:
      inputEl.plotColor = MAP_COLOR_MAGENTA;
      break;
    case ELEMENT_OBSTACLE_LADAR:
      inputEl.plotColor = MAP_COLOR_GREY;
      break;
    case ELEMENT_OBSTACLE_RADAR:
      inputEl.plotColor = MAP_COLOR_PINK;
      break;
    case ELEMENT_OBSTACLE_STEREO:
      inputEl.plotColor = MAP_COLOR_LIGHT_BLUE;
      break;
    default:
      //change nothing
      break;
    }

    //Planner wants obstacle/vehicle classification, where obstacle is something we're
    //sure is stopped, and vehicle is everything else
    if(inputEl.isObstacle() && (inputEl.type != ELEMENT_VEHICLE)) {
      inputEl.type = ELEMENT_OBSTACLE;
    }

    // send off to planner and debugging subgroup
    sendMapElementToBoth(&inputEl); 
    return 0;
}


int Mapper::fuseMapElement(MapElement & fusedEl)
{
  int i;
  int mapsize = (int) map.data.size();
  point2 basePos;
  point2 fusedPos(fusedEl.position);
  
  whichPerceptor originatingSensor;

  //FIXME - it's silly to have this parameter, since it's not really used
  //  double mindist = options.fusion_dist_thresh_arg;
  double mindist = 100;
  int minindex = -1;

  for (i=0; i<mapsize;++i){
    basePos = map.data[i].position;
    double fusedDist = fusedPos.dist(basePos);
    //if old map element at i is closer than previous minimum AND is an obstacle
    //TODO: still does no fusion for road features
    if ((fusedDist < mindist) && map.data[i].isObstacle()){
      minindex = i;
      mindist = fusedDist;
    }
  }

  //label the map elements according to their originating module (since the
  //   id will be overwritten during fusion)
  int sendID;
  sendID = fusedEl.id.dat.at(0);


  if((MODladarCarPerceptor == sendID)||(MODladarCarPerceptorLeft == sendID)||(MODladarCarPerceptorCenter == sendID)||(MODladarCarPerceptorRight == sendID)||(MODladarCarPerceptorRear == sendID)||(MODladarCarPerceptorRoofLeft == sendID)||(MODladarCarPerceptorRoofRight == sendID)||(MODladarCarPerceptorRiegl == sendID)) {
    originatingSensor = LADAR_CAR;
  } else if(MODradarObsPerceptor == sendID) {
    originatingSensor = RADAR_CAR;
    fusedEl.type = ELEMENT_OBSTACLE_RADAR;
  } else if((MODstereoObsPerceptorLong == sendID) || (MODstereoObsPerceptorMedium == sendID)) {
    originatingSensor = STEREO_OBS; 
    fusedEl.type = ELEMENT_OBSTACLE_STEREO;
  } else {
     originatingSensor = PERCEPTOR_UNKNOWN;
     MSG("Unrecognized perceptor sending to fusion");
  }

  //  if (minindex < 0){
    //   return 0; //no matching object; do not change the input map element

  if (minindex>=0){
    // only fuse overlapping obstacles at the moment (fusing static with dynamic obs is ok)
    if (fusedEl.isOverlap(map.data[minindex]) || map.data[minindex].isOverlap(fusedEl)){
        // just make sure we did things right. REMOVE ME when fusing lines too
        assert(fusedEl.isObstacle());
        assert(map.data[minindex].isObstacle());

        // use the existing id in the map
        fusedEl.id = map.data[minindex].id;

        // compute the difference in times sensed
        fusedDeltaTime = (int)((fusedEl.state.timestamp-map.data[minindex].state.timestamp )/1000); 
	//	MSG("fusing elements with deltaTime = %d; incoming confidence: %f", fusedDeltaTime, fusedEl.conf);

        // if the time difference is small, we can be fairly sure that the 
        // older data isn't stale, and can use it to fuse
        if (fusedDeltaTime < options.fusion_time_thresh_arg){

          // increase confidence for the fused element
 	  //        fusedEl.conf = fusedEl.conf+map.data[minindex].conf;
	  //          fusedEl.conf = 2*map.data[minindex].conf;

	  //for now, since perceptors don't report uncertainties to be trusted,
	  //use my previous knowledge about sensors to determine which to trust

	  /**things to update:
	   *     ID - use old
	   *     velocity - average old and new
	   *     position - use new; TODO: actually merge somehow
	   * 
	   *     object type -> from stereo and radar should both be car; only ladar can make distinction?
	   *     time stationary? is stationary?
	   *     
	   *     keep track of #elements fused from ladar/radar/stereo
	   **/

	  switch(originatingSensor) {
	  case LADAR_CAR:
	    //use newest position UNLESS previously seen obstacle was car
	    if((ELEMENT_VEHICLE == map.data[minindex].type)&&(ELEMENT_VEHICLE != fusedEl.type)) {
              fusedEl.geometry = map.data[minindex].geometry;
              fusedEl.updateFromGeometry();
	      fusedEl.type = map.data[minindex].type;
	    }
	    //update velocity
            fusedEl.velocity = (map.data[minindex].velocity + fusedEl.velocity)/2;
	    //trust classification only from ladar-car-perceptor
	    if((ELEMENT_VEHICLE == fusedEl.type) || (ELEMENT_VEHICLE == map.data[minindex].type)) {
	      fusedEl.type = ELEMENT_VEHICLE;
	    } else if((ELEMENT_OBSTACLE == fusedEl.type) || (ELEMENT_OBSTACLE == map.data[minindex].type)) {
	      fusedEl.type = ELEMENT_OBSTACLE;
	      MSG("merging geometries!");
	      //	      fusedEl.geometry.merge(map.data[minindex].geometry);
	      //	      fusedEl.updateFromGeometry();
	    } 	      
	    //	    MSG("fused ladar data into %d", map.data[minindex].type);
	    break;

	  case RADAR_CAR:
	    //trust velocity regardless
            fusedEl.velocity = (map.data[minindex].velocity + fusedEl.velocity)/2;
	    //trust new position ONLY if radar is only sensor to see this object
	    if(ELEMENT_OBSTACLE_RADAR != map.data[minindex].type) {
              fusedEl.geometry = map.data[minindex].geometry;
              fusedEl.updateFromGeometry();
	      fusedEl.type = map.data[minindex].type;
	    } else {
	      //only observations of the object have been from radar, so can update geometry
	    }
	    if(ELEMENT_OBSTACLE == map.data[minindex].type) {
	      fusedEl.type = ELEMENT_OBSTACLE;
	    }
       	    MSG("fused radar data into %d", map.data[minindex].type);
	    break;

	  case STEREO_OBS:
	    //use newest position UNLESS previously seen obstacle was car
	    if(ELEMENT_VEHICLE == map.data[minindex].type) {
              fusedEl.geometry = map.data[minindex].geometry;
              fusedEl.updateFromGeometry();
	      fusedEl.type = map.data[minindex].type;
	    }
	    if(ELEMENT_OBSTACLE_STEREO != map.data[minindex].type) {
	      fusedEl.velocity = map.data[minindex].velocity;
	    }
	    //if we've observed it to be stationary, keep that classification
	    if(ELEMENT_OBSTACLE == map.data[minindex].type) {
	      fusedEl.type = ELEMENT_OBSTACLE;
	    }
	    MSG("fused stereo data into %d", map.data[minindex].type);
	    break;

	  case PERCEPTOR_UNKNOWN:
	    ERROR("trying to fuse unknown perceptor");
	    break;
	  } // end switch statement checking which sensor new data comes from
        } //end checking delta time
        // update the local map - this happens in 
	//        map.data[minindex] = fusedEl;
        totalFused++;
    } //end checking overlap (if no overlap, do nothing)
  } //end checking if there was a minimum
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
	Mapper *mapper;
	mapper = new Mapper();
  assert(mapper);

	mapper->parseCmdLine(argc, argv);


	if (mapper->sendSubGroup ==mapper->recvSubGroup){
		cerr <<"Error in Mapper: must specify different send and receive subgroups." << endl;
		return -1;
	}
		

   MSG("Init Comm");
	mapper->initComm();
	mapper->resetMap();
	if (!mapper->options.disable_console_flag ){
    MSG("Init Console");
		if (mapper->initConsole() != 0){
			return -1;
    }
  }
  
  // initialize mapper
  mapper->init();



	cerr << "Listening for map elements" << endl;

	while (!mapper->quit){
    // Do heartbeat occasionally
    if (DGCgettime() - mapper->processTime > 500000)
      {
        mapper->processTime = DGCgettime();
        mapper->updateProcessState();
      }
    // update and send the graph roughly every 4 seconds
    if (DGCgettime() - mapper->graphTime > /*4000000*/500000){
      mapper->graphTime = DGCgettime();
      mapper->updateGraph();

      if (mapper->debugSubGroup!=0){
        for (unsigned int i =0;i<mapper->map.prior.fulldata.size();++i){
          if(mapper->map.prior.getElFull(mapper->outputEl,i))
            mapper->mapTalker.sendMapElement(&mapper->outputEl, mapper->debugSubGroup);
        }
      }
    }

      
    // update the console

    if (mapper->console){
      if (DGCgettime() - mapper->consoleTime > 100000){
        cotk_update(mapper->console);			
        if (mapper->debugSubGroup !=0)
          cotk_printf(mapper->console, "%elements%", A_NORMAL, "%d received, debug to subgroup %d ", mapper->totalReceived, mapper->debugSubGroup);
        else
          cotk_printf(mapper->console, "%elements%", A_NORMAL, "%d received, no debug ", mapper->totalReceived);


        cotk_printf(mapper->console, "%filter%", A_NORMAL, "%d fused, %d msec               " 
                    , mapper->totalFused, mapper->fusedDeltaTime);

        cotk_printf(mapper->console, "%map%", A_NORMAL, " size = %d    " 
                    , mapper->map.data.size());


      }
    }
    if (mapper->pause){
      // disable pause for now as it causes spread errors
      //   usleep(100000);
      //    continue;
    }

    // update the map
    if (mapper->updateMap() !=0)
      break;
    
  }	
	
  mapper->fini();
	mapper->finiConsole();
  mapper->finiComm();
	return 0;
}

