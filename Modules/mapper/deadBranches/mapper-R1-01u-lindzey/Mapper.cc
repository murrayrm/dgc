/**********************************************************
 **
 **  MAPPER.CC
 **
 **    Time-stamp: <2007-08-24 13:11:14 team> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:30 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Mapper.hh"


using namespace std;

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )
// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


Mapper::Mapper()
{ //--------------------------------------------------
  // initialize time offsets
  //--------------------------------------------------
	obsFusionDisabled = false;
	lineFusionDisabled = false;
	groundstrikeFilteringEnabled = false;
	groundstrikeDisplayEnabled = false;
  graphTime = 0;
  debugTime = 0;
  decayTime = 0;
  fusionTime = 0;
	fusionTimeThresh = 500;
  sendSubGroup = 1;
  recvSubGroup = 0;
  debugSubGroup = -2;
}

Mapper::~Mapper()
{}


// Initialize comm
int Mapper::initComm(int snkey)
{
  //--------------------------------------------------
  // initialize graphtalker
  //--------------------------------------------------
  graphTalkerPtr = new SkynetTalker<Graph>(snkey,SNglobalGloNavMapFromGloNavMapLib, MODmissionplanner); 
  //--------------------------------------------------
  // initialize map elment talker
  //--------------------------------------------------
	this->mapTalker = new CMapElementTalker();	
	this->mapTalker->initRecvMapElement(snkey,recvSubGroup);
	this->mapTalker->initSendMapElement(snkey);

	return 0;  
}

int Mapper::finiComm()
{

	delete mapTalker;
  delete graphTalkerPtr;
  return 0;
}

// Initialize skynet

void Mapper::setLocalFrameOffset(VehicleState state){
	point2 statedelta,stateNE, statelocal;
  
	statelocal.set(state.localX,state.localY);
	stateNE.set(state.utmNorthing,state.utmEasting);
	statedelta = stateNE-statelocal;
  map.setTransform(statedelta);
}



void Mapper::disableLineFusion(bool disableflag = true)
{
  map.enableLineFusion(!disableflag);
  lineFusionDisabled = disableflag;
}

void Mapper::disableObsFusion(bool disableflag = true)
{
  obsFusionDisabled = disableflag;
}

void Mapper::enableGroundstrikeFiltering(bool enableflag = false)
{
  groundstrikeFilteringEnabled = enableflag;
}

void Mapper::enableGroundstrikeDisplay(bool enableflag = false)
{
  groundstrikeDisplayEnabled = enableflag;
}

int Mapper::init(string RNDFname)
{	

  // enable or disable line fusion
 
 
  
  //--------------------------------------------------
  // initialize the map prior data
  //--------------------------------------------------
  if (!map.loadRNDF(RNDFname.c_str())){
    cerr << "Error:  Unable to load RNDF file to map " << RNDFname
         << ", exiting program" << endl;
    exit(1);
  }


  //--------------------------------------------------
  // initialize the rndf graph data
  //--------------------------------------------------
	char* fname = (char*) RNDFname.c_str();
	if (!rndf.loadFile(fname)){
    cerr << "Error:  Unable to load RNDF file to graph" << RNDFname
         << ", exiting program" << endl;
    exit(1);
	}
	rndf.assignLaneDirection();
  //initialize graph interface with mplanner
	rndfGraphPtr = new Graph(&rndf);
  //send a graph
  graphTalkerPtr->send(rndfGraphPtr);

  //initialize the elevation map
  if(groundstrikeFilteringEnabled) {
    roadMap = new RoadMap();

    //TODO: actually have this initialize off of mapper's moduleID, snKey, etc...
    //TODO: actually do clean up for these...
    MSG("intializing road map");
    roadMap->Init();
    MSG("initializing elevation map");
    roadMap->roadMapInit();
    MSG("initializing sensnet");
    roadMap->initSensnet();
    DGCgettime(timeRoadMapUpdated);

    //TODO: get this working
    if(groundstrikeDisplayEnabled) {
      //roadMap->initDisplay();
    }
  }
  return 0;
}		

int Mapper::fini()
{
  delete rndfGraphPtr;
  return 0;
}


int Mapper::updateGraph()
{
  point2 cpt;
  PointLabel ptlabel;
	MapElement tmpEl;

  unsigned int i;
  for (i=0;i< map.data.size();++i){
    tmpEl=map.data[i];
    cpt = tmpEl.center;
    if (map.getLastPointID(ptlabel,cpt)==0){
      rndfGraphPtr->addObstacle(ptlabel.segment,ptlabel.lane,ptlabel.point);
    }
  }
  graphTalkerPtr->send(rndfGraphPtr);
  return 0;
}


int Mapper::resetMap()
{
  map.data.clear();
	return 0;
}

int Mapper::decayMap()
{

  unsigned long long currentTime = DGCgettime();
  int dtime = 0;
  int dtime2 = 0;
  int i, j;
  MapElement mapEl;
  bool perceptorsClear, dataOld;

  for(i = map.usedIndices.size()-1; i >=0; i--) {
    j = map.usedIndices.at(i);

    perceptorsClear = true;
    dataOld = true;

    int perceptorNotClear = 0;

    for(int k = 0; k < NUM_PERCEPTORS; k++) {
      if(map.newData[j].trackAlive[k] == 1){
	perceptorsClear = false;
	perceptorNotClear = k;
	//	MSG("for object %d, perceptor %d still alive", j, k);
      }

      dtime = (int)(currentTime - map.newData[j].inputData[k].lastSeen);
      if((decayAgeThresh > 0) && (dtime < 1000000*decayAgeThresh)) {
	dataOld = false;
	dtime2 = dtime;
	//        MSG("for element %d, dtime=%d, currentTime=%llu, lastSeen=%llu, perceptorsClear=%d ", j, dtime, perceptorsClear, currentTime, map.newData[j].inputData[k].lastSeen );
      }
    }

    if(!dataOld) {
      //      MSG("dtime = %d",dtime2);
    } 


    //we want to remove element
    //    if(perceptorsClear && dataOld) {
    if(dataOld) {
      //FIXME: do I need to reset the fields in newData[j]?
      if(!perceptorsClear) {
	//	MSG("removing object %d, but perceptor %d not clear", j, perceptorNotClear);
      }


      mapEl.clear();
      mapEl = map.newData[j].mergedMapElement;
      mapEl.setTypeClear();

      //      MSG("removing map element %d (too old)", map.newData[j].id);

      if (debugSubGroup != 0){
        mapTalker->sendMapElement(&mapEl,debugSubGroup);
      }

      if (!plannerOutputDisabled){
        mapEl.setId(MODmapping, j);
	//       MSG("sending element to planner: id = (%d %d)", sensedEl.id.dat.at(0), sensedEl.id.dat.at(1));
        mapTalker->sendMapElement(&mapEl,sendSubGroup);
      }

      //since we don't require a perceptor to send a clear message 
      //before removing an obstacle,
      //we need to check that the IDtoINDEX lookup is entirely reset
      DGCgettime(currentTime);
      for(int m = 0; m < NUM_PERCEPTORS; m++) {
	for(int n = 0; n < MAXNUMPERCEPTOROBJECTS; n++) {
	  if(map.IDtoINDEX[m][n]==j)
	    map.IDtoINDEX[m][n] = -1;
	}
      }
      //      MSG("time to cycle through IDtoINDEX array: %llu", DGCgettime() - currentTime);
      map.openIndices.push_back(j);
      mapIterator =  map.usedIndices.begin();
      mapIterator += i;
      map.usedIndices.erase(mapIterator);
    }

  }

  //now, decay non-object elements
  // back to front so erase doesn't mess with the index
  for (i=map.data.size()-1;i>=0;--i){
    if (map.data[i].timestamp==0)
      continue;
    
    dtime = (int)(currentTime-map.data[i].timestamp);
    if (dtime > 1000000*decayAgeThresh && decayAgeThresh>0){
			priorEl.clear();
			priorEl.setTypeClear();
			priorEl.setId(map.data[i].id);      
			if (debugSubGroup!=0){
				mapTalker->sendMapElement(&priorEl, debugSubGroup);
      }
			if (!plannerOutputDisabled){
				mapTalker->sendMapElement(&priorEl,sendSubGroup);
			}
      map.data.erase(map.data.begin()+i);
    }
  }
	return 0;
}

int Mapper::mainLoop()
{
  
  
  // update and send the graph roughly every 4 seconds
  if (DGCgettime() - graphTime > /*4000000*/500000){
    graphTime = DGCgettime();
    updateGraph();
    
  }

  // send the rndf data out for visualization
  if (DGCgettime() - debugTime > 500000){
    debugTime = DGCgettime();
      
    if (debugSubGroup!=0){
      for (unsigned int i =0;i<map.prior.fulldata.size();++i){
        if(map.prior.getElFull(priorEl,i))
          mapTalker->sendMapElement(&priorEl, debugSubGroup);
      }
    }
  }


  // decay the map
  if ((decayAgeThresh > 0) && (DGCgettime() - decayTime > 1000000*decayAgeThresh)){
    decayMap();
    //    MSG("decaying the map");
    decayTime = DGCgettime();
  }  

  //update the road map
  if(groundstrikeFilteringEnabled) {
    //don't want this to run faster than ladars...
    if(DGCgettime() - timeRoadMapUpdated > 10000) {
      roadMap->update();
      DGCgettime(timeRoadMapUpdated);

      if(groundstrikeDisplayEnabled) {
        //      roadMap->updateDisplay();
      }
    }

  }

  // update the map
  //get map elements
  if (mapTalker->recvMapElementNoBlock(&sensedEl,recvSubGroup) >0){
    totalReceived++;
    sensedEl.timestamp = DGCgettime();

    //sending has been added to the update map... 
     updateMap();
 
  }else{
    // this is necessary for performance reasons until
    // we can get a timed out blocking read above
		usleep(10);
	}
  return 0;
}

int Mapper::updateMap()
{

  // for all lanelines, fusion happens in map
  if(!sensedEl.isObstacle()) {
    map.addEl(sensedEl);
    if (!plannerOutputDisabled){
      mapTalker->sendMapElement(&sensedEl,sendSubGroup);
    }
    if (debugSubGroup != 0){
      mapTalker->sendMapElement(&sensedEl,debugSubGroup);
    }
    return 0;
  }

  ///////////for all obstacles, groundstrike filtering and fusion happens here////////

  int index;
  //if this is above .8, we don't send obstacle
    double fracGS = 0.0;

  int originatingPerceptorID = sensedEl.id.dat[0];
  int idFromPerceptor = sensedEl.id.dat[1];
  int originatingPerceptor;


  switch(originatingPerceptorID) {

  case 87: //MODladarCarPerceptorLeft: 
    originatingPerceptor = LF_LADAR;
    break;

  case 94: //MODladarCarPerceptorRoofLeft: 
    originatingPerceptor = LF_ROOF_LADAR;
    break;

  case 88: //MODladarCarPerceptorCenter: 
    originatingPerceptor = MF_LADAR;
    break;

  case 89: //MODladarCarPerceptorRight: 
    originatingPerceptor = RF_LADAR;
    break;

  case 90: //MODladarCarPerceptorRear: 
    originatingPerceptor = REAR_LADAR;
    break;

  case 95: //MODladarCarPerceptorRiegl: 
    originatingPerceptor = RIEGL;
    break;

  case 79: //MODradarObsPerceptor: 
    originatingPerceptor = MF_RADAR;
    break;

  case 83: //MODstereoObsPerceptorLong: 
    originatingPerceptor = LONG_STEREO_OBS;
    break;

  case 82: //MODstereoObsPerceptorMedium: 
    originatingPerceptor = MEDIUM_STEREO_OBS;
    break;

  default:
    //unrecognized perceptor
    originatingPerceptor = NUM_PERCEPTORS;
  }

  //if 
  if(sensedEl.type == ELEMENT_CLEAR && NUM_PERCEPTORS != originatingPerceptor) {
    //    MSG("processing type_clear");
    index = map.addEl(sensedEl); 
  }

  //if it's an obstacle, we'll check for groundstrikes, then do fusion
  if(sensedEl.isObstacle() && (ELEMENT_CLEAR != sensedEl.type)){
    int sendID;
    sendID = sensedEl.id.dat.at(0);
    if(MODradarObsPerceptor == sendID) {
      sensedEl.type = ELEMENT_OBSTACLE_RADAR; 
    }

    //if filtering is enabled, and originating sensor is a ladar check groundstrikes
    if(groundstrikeFilteringEnabled) {
      if((MODladarCarPerceptor == sendID)||(MODladarCarPerceptorLeft == sendID)||(MODladarCarPerceptorCenter == sendID)||(MODladarCarPerceptorRight == sendID)||(MODladarCarPerceptorRear == sendID)||(MODladarCarPerceptorRoofLeft == sendID)||(MODladarCarPerceptorRoofRight == sendID)||(MODladarCarPerceptorRiegl == sendID)) {

    	fracGS = roadMap->checkGroundstrike(sensedEl.geometry);
	//      	MSG("testing element from mapelement %d, %d, %d: prob = %f", sendID, sensedEl.id.dat.at(1), sensedEl.id.dat.at(2), fracGS);
      } 
    }  // end checking if groundstrike filtering enabled 

    //if fusion is enabled AND it's not a groundstrike
#warning "magic #"
    if(fracGS < GROUNDSTRIKEPROB) {
      //      MSG("adding el to map: sendID=%d, elemID=%d, type=%d, size=%d, currtime=%llu", sendID, sensedEl.id.dat.at(1),sensedEl.type, sensedEl.geometry.size(), DGCgettime() );
      index = map.addEl(sensedEl); 
 
      if(!obsFusionDisabled) {
	//        fuseMapElement(sensedEl);
	currentTime = DGCgettime();
	if(currentTime - map.newData[index].lastFused > FUSIONRATE) {
	  //	  MSG("fusing map element: %d", index);
          fuseMapElement2(index);
          map.newData[index].lastFused = currentTime;
	}

      }
      sensedEl = map.newData[index].mergedMapElement;
      //      MSG("sensed el's id: %d (id size %d)", sensedEl.id.dat.at(0), sensedEl.id.dat.size());
    } //end checking not groundstrike

  } //end checking if obstacle
 

  //colorcode sent elements for help in debugging
  switch(sensedEl.type) {
  case ELEMENT_VEHICLE:
    sensedEl.plotColor = MAP_COLOR_DARK_GREEN;
    break;
  case ELEMENT_OBSTACLE:
    sensedEl.plotColor = MAP_COLOR_MAGENTA;
    break;
  case ELEMENT_OBSTACLE_LADAR:
    sensedEl.plotColor = MAP_COLOR_GREY;
    break;
  case ELEMENT_OBSTACLE_RADAR:
    sensedEl.plotColor = MAP_COLOR_PINK;
    break;
  case ELEMENT_OBSTACLE_STEREO:
    sensedEl.plotColor = MAP_COLOR_LIGHT_BLUE;
    break;
  default:
    //change nothing
    break;
  }

  //Planner wants obstacle/vehicle classification, where obstacle is something we're
  //sure is stopped, and vehicle is everything else
  if(sensedEl.isObstacle() && (sensedEl.type != ELEMENT_VEHICLE)) {
    sensedEl.type = ELEMENT_OBSTACLE;
  }



  //for debugging only - send to map this color
  if(fracGS > GROUNDSTRIKEPROB) {
    sensedEl.plotColor = MAP_COLOR_YELLOW;
    if (debugSubGroup != 0){
      mapTalker->sendMapElement(&sensedEl,debugSubGroup);
    }
  }

  //if IS groundstrike, need to send type Clear to planner
  if(fracGS > GROUNDSTRIKEPROB) {
    //    MSG("sensed el is groundstrike");
    sensedEl.setTypeClear();
    map.addEl(sensedEl);
    //    if (!plannerOutputDisabled){
    //      mapTalker->sendMapElement(&sensedEl,sendSubGroup);
    //    }
  }


  //add to local map representation if not gs
  if(fracGS < GROUNDSTRIKEPROB) {

    //    MSG("sensed el's id: %d (id size %d) and type: %d", sensedEl.id.dat.at(0), sensedEl.id.dat.size(), sensedEl.type);
    if (debugSubGroup != 0){
      mapTalker->sendMapElement(&sensedEl,debugSubGroup);
    }

    if (!plannerOutputDisabled){
      sensedEl.setId(MODmapping, index);
      //      MSG("sending element to planner: id = (%d %d)", sensedEl.id.dat[0], sensedEl.id.dat[1]);
      mapTalker->sendMapElement(&sensedEl,sendSubGroup);
    }

  }


	
  return 0;
}

int Mapper::fuseMapElement2(int index)
{
  point2arr mergedGeometry;
  mergedGeometry.clear();
  point2 mergedVelocity = point2(0.0, 0.0);
  MapElementType mergedType = ELEMENT_OBSTACLE_LADAR;

  vector<point2arr> GPCclippedPolygons;

  int perceptorsUsed = 0;
  DGCgettime(currentTime);

  //if we have ladar returns, want to fuse them
  //if a ladar return is type vehicle, this has preference; then obstacle
  int p;
  for(p=LF_LADAR; p<= RIEGL; p++) {
    //if the data is recent enough, use it
    if((decayAgeThresh == 0) || (currentTime - map.newData[index].inputData[p].lastSeen < 1000000*decayAgeThresh)) {
      perceptorsUsed++;
      GPCclippedPolygons.clear();
      mergedGeometry.get_poly_union(map.newData[index].inputData[p].geometry, GPCclippedPolygons);
      if(GPCclippedPolygons.size() != 1) {
	MSG("error (index %d, perceptor %d) - merging polygons returned %d separate poitn2arr - aborting merge", index, p, GPCclippedPolygons.size());
      } else {
        mergedGeometry = GPCclippedPolygons.at(0);
	//        MSG("merged data for element %d: %d points", map.newData[index].id, GPCclippedPolygons.at(0).size());
  
        mergedVelocity = mergedVelocity + map.newData[index].inputData[p].velocity;

        if(mergedType == ELEMENT_VEHICLE || map.newData[index].inputData[p].type == ELEMENT_VEHICLE) {
          mergedType = ELEMENT_VEHICLE;
        } else if(mergedType == ELEMENT_OBSTACLE || map.newData[index].inputData[p].type == ELEMENT_OBSTACLE) {
	  mergedType = ELEMENT_OBSTACLE;
        }
      }
    }
  } // end cycling through ladars

  if(perceptorsUsed == 0) { //obstacle not seen by ladar
    mergedType = ELEMENT_OBSTACLE_STEREO;
    for(p = LONG_STEREO_OBS; p <= MEDIUM_STEREO_OBS; p++) {
       if((decayAgeThresh == 0) || (currentTime - map.newData[index].inputData[p].lastSeen < 1000000*decayAgeThresh)) {
        perceptorsUsed++;
        mergedGeometry.get_poly_union(map.newData[index].inputData[p].geometry, GPCclippedPolygons);
        if(GPCclippedPolygons.size() != 1) {
          MSG("error - merging polygons returned %d separate poitn2arr", GPCclippedPolygons.size());
        }
        mergedGeometry = GPCclippedPolygons.at(0);
      }
    }//end cycling through stereos
  }

  //FIXME: add in fusing between radar/ladar
  if(perceptorsUsed == 0) { //obstacle not seen by ladar OR stereo
    p = MF_RADAR;

    if((decayAgeThresh == 0 ) || (currentTime - map.newData[index].inputData[p].lastSeen < 1000000*decayAgeThresh)) {
        perceptorsUsed++;

	mergedType = map.newData[index].inputData[MF_RADAR].type;
	mergedVelocity = map.newData[index].inputData[MF_RADAR].velocity;
	mergedGeometry = map.newData[index].inputData[MF_RADAR].geometry;
    }
  }

  if(perceptorsUsed == 0) {
    //    MSG("No recent enough data to update");
    return -1;
  }

  //  MSG("merged object %d, w/ %d perceptors. size=%d, velocity=(%f, %f), type=%d", index, perceptorsUsed, mergedGeometry.size(), mergedVelocity.x, mergedVelocity.y, mergedType);

  map.newData[index].mergedMapElement.geometry = mergedGeometry;
  map.newData[index].mergedMapElement.velocity = mergedVelocity;
  map.newData[index].mergedMapElement.type = mergedType;

  map.newData[index].mergedGeometry = mergedGeometry;
  map.newData[index].mergedVelocity = mergedVelocity;
  map.newData[index].mergedType = mergedType;


  return 0;
}

int Mapper::fuseMapElement(MapElement & fusedEl)
{
  int i;
  int mapsize = (int) map.data.size();
  point2 basePos;
  point2 fusedPos(fusedEl.position);
  
  whichPerceptor originatingSensor;

  //FIXME - it's silly to have this parameter, since it's not really used
  //  double mindist = options.fusion_dist_thresh_arg;g
  double mindist = 100;
  int minindex = -1;

  for (i=0; i<mapsize;++i){
    basePos = map.data[i].position;
    double fusedDist = fusedPos.dist(basePos);
    //if old map element at i is closer than previous minimum AND is an obstacle
    //TODO: still does no fusion for road features
    if ((fusedDist < mindist) && map.data[i].isObstacle()){
      minindex = i;
      mindist = fusedDist;
    }
  }

  //label the map elements according to their originating module (since the
  //   id will be overwritten during fusion)
  int sendID;
  sendID = fusedEl.id.dat.at(0);


  if((MODladarCarPerceptor == sendID)||(MODladarCarPerceptorLeft == sendID)||(MODladarCarPerceptorCenter == sendID)||(MODladarCarPerceptorRight == sendID)||(MODladarCarPerceptorRear == sendID)||(MODladarCarPerceptorRoofLeft == sendID)||(MODladarCarPerceptorRoofRight == sendID)||(MODladarCarPerceptorRiegl == sendID)) {
    originatingSensor = LADAR_CAR;
  } else if(MODradarObsPerceptor == sendID) {
    originatingSensor = RADAR_CAR;
    fusedEl.type = ELEMENT_OBSTACLE_RADAR;
  } else if((MODstereoObsPerceptorLong == sendID) || (MODstereoObsPerceptorMedium == sendID)) {
    originatingSensor = STEREO_OBS; 
    fusedEl.type = ELEMENT_OBSTACLE_STEREO;
  } else {
    originatingSensor = PERCEPTOR_UNKNOWN;
    MSG("Unrecognized perceptor sending to fusion, modID: %d, geomType: %d", sendID, fusedEl.type);
  }

  //  if (minindex < 0){
  //   return 0; //no matching object; do not change the input map element

  if (minindex>=0){
    // only fuse overlapping obstacles at the moment (fusing static with dynamic obs is ok)
    if (fusedEl.isOverlap(map.data[minindex]) || map.data[minindex].isOverlap(fusedEl)){
      // just make sure we did things right. REMOVE ME when fusing lines too
      assert(fusedEl.isObstacle());
      assert(map.data[minindex].isObstacle());

      // use the existing id in the map
      fusedEl.id = map.data[minindex].id;

      // compute the difference in times sensed
      fusedDeltaTime = (int)((fusedEl.state.timestamp-map.data[minindex].state.timestamp )/1000); 
      //	MSG("fusing elements with deltaTime = %d; incoming confidence: %f", fusedDeltaTime, fusedEl.conf);

      // if the time difference is small, we can be fairly sure that the 
      // older data isn't stale, and can use it to fuse
      if (fusedDeltaTime < fusionTimeThresh){

        // increase confidence for the fused element
        //        fusedEl.conf = fusedEl.conf+map.data[minindex].conf;
        //          fusedEl.conf = 2*map.data[minindex].conf;

        //for now, since perceptors don't report uncertainties to be trusted,
        //use my previous knowledge about sensors to determine which to trust

        /**things to update:
         *     ID - use old
         *     velocity - average old and new
         *     position - use new; TODO: actually merge somehow
         * 
         *     object type -> from stereo and radar should both be car; only ladar can make distinction?
         *     time stationary? is stationary?
         *     
         *     keep track of #elements fused from ladar/radar/stereo
         **/

        switch(originatingSensor) {
        case LADAR_CAR:
          //use newest position UNLESS previously seen obstacle was car
          if((ELEMENT_VEHICLE == map.data[minindex].type)&&(ELEMENT_VEHICLE != fusedEl.type)) {
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
            fusedEl.type = map.data[minindex].type;
          }
          //update velocity
          fusedEl.velocity = (map.data[minindex].velocity + fusedEl.velocity)/2;
          //trust classification only from ladar-car-perceptor
          if((ELEMENT_VEHICLE == fusedEl.type) || (ELEMENT_VEHICLE == map.data[minindex].type)) {
            fusedEl.type = ELEMENT_VEHICLE;
          } else if((ELEMENT_OBSTACLE == fusedEl.type) || (ELEMENT_OBSTACLE == map.data[minindex].type)) {
            fusedEl.type = ELEMENT_OBSTACLE;
            //   MSG("merging geometries!");
            //	      fusedEl.geometry.merge(map.data[minindex].geometry);
            //	      fusedEl.updateFromGeometry();
          } 	      
          //	    MSG("fused ladar data into %d", map.data[minindex].type);
          break;

        case RADAR_CAR:
          //trust velocity regardless
          fusedEl.velocity = (map.data[minindex].velocity + fusedEl.velocity)/2;
          //trust new position ONLY if radar is only sensor to see this object
          if(ELEMENT_OBSTACLE_RADAR != map.data[minindex].type) {
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
            fusedEl.type = map.data[minindex].type;
          } else {
            //only observations of the object have been from radar, so can update geometry
          }
          if(ELEMENT_OBSTACLE == map.data[minindex].type) {
            fusedEl.type = ELEMENT_OBSTACLE;
          }
          // 	    MSG("fused radar data into %d", map.data[minindex].type);
          break;

        case STEREO_OBS:
          //use newest position UNLESS previously seen obstacle was car
          if(ELEMENT_VEHICLE == map.data[minindex].type) {
            fusedEl.geometry = map.data[minindex].geometry;
            fusedEl.updateFromGeometry();
            fusedEl.type = map.data[minindex].type;
          }
          if(ELEMENT_OBSTACLE_STEREO != map.data[minindex].type) {
            fusedEl.velocity = map.data[minindex].velocity;
          }
          //if we've observed it to be stationary, keep that classification
          if(ELEMENT_OBSTACLE == map.data[minindex].type) {
            fusedEl.type = ELEMENT_OBSTACLE;
          }
          //  MSG("fused stereo data into %d", map.data[minindex].type);
          break;

        case PERCEPTOR_UNKNOWN:
          ERROR("trying to fuse unknown perceptor");
          break;
        } // end switch statement checking which sensor new data comes from
      } //end checking delta time
        // update the local map - this happens in 
      //        map.data[minindex] = fusedEl;
      totalFused++;
    } //end checking overlap (if no overlap, do nothing)
  } //end checking if there was a minimum
  return 0;
}

