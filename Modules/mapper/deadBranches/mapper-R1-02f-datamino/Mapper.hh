/**********************************************************
 **
 **  MAPPER.HH
 **
 **    Time-stamp: <2007-08-20 23:49:13 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:39 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <assert.h>
#include <vector>
#include <math.h>
#include <sys/stat.h>

#include "dgcutils/DGCutils.hh"

#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

// needed for mplanner interface
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
// local headers
#include "skynettalker/SkynetTalker.hh"


#include "travgraph/Graph.hh"
#include "rndf/RNDF.hh"

#include "map/Map.hh"
#include "RoadMap.hh"

#include "Log.hh"

using namespace std;

#define GROUNDSTRIKEPROB .4
#define FUSIONRATE       250000

enum whichPerceptor{
  LADAR_CAR,
  RADAR_CAR,
  STEREO_OBS,
  PERCEPTOR_UNKNOWN
};
  

class Mapper 
{
	
public:
	

	Mapper();

	~Mapper();
	
public:


 /// Initalize map
  int init(string fname);

  /// Clean up map
  int fini();

  /// Initilize communications 
	int initComm(int snkey);
  /// Clean up communications
	int finiComm();

  /// Update the graph and send it to mplanner
  int updateGraph();

  

  /// Clears the map
	int resetMap();

  /// Decays the map
	int decayMap();

  /// removes an object from map; arg index into mapData
  int removeObject(int index);

  /// The function that will be called in the main program loop
  int mainLoop();

  /// Updates the map with new data
	int updateMap();

	/// Sets the Global to Local frame offset for the internal map
	void setLocalFrameOffset(VehicleState state);


  /// Takes in a map element and checks if it can be reasonably associated 
  /// with other existing elements in the map.  If so, the element is 
  /// modified and fused into the map.

	int fuseMapElement(int index);

	/// Disables fusion of lines inside mapper
	void disableLineFusion(bool disableflag);
	/// Disables fusion of obstacles inside mapper
	void disableObsFusion(bool disableflag);

	/// Compatibility mode: Do line fusion the old way (in Map::addEl())
  /// instead of Mapper::mainLoop().
	void enableLineFusionCompat(bool enableflag);

  ///enables filtering of obstacles based on groundstrikes
  void enableGroundstrikeFiltering(bool enableflag);

  ///enables debugging emapViewer for road map
  void enableGroundstrikeDisplay(bool enableflag);
  
	/// sets the fusedDeltaTime parameter in ms
	void setFusionTimeThresh(int timethresh)
	{fusionTimeThresh = timethresh;}


 /// Talker to send and receive map elements
  CMapElementTalker * mapTalker;
  
  /// map element talker subgroup to send map data
  int sendSubGroup;
  /// map element talker subgroup to receive sensed data
	int recvSubGroup;
  /// map element talker subgroup to visualize debugging data
	int debugSubGroup;
  uint64_t debugTime;

  /// total number of map elements received
	int totalReceived;
  /// total map size
	int totalMapSize;
  /// total number of map elements fused
	int totalFused;
  /// the time difference between fused elements in ms
  int fusedDeltaTime;

  /// total fake map element sent
    int totalFakes;
    
 
    /// is obstacle fusion disabled
    bool obsFusionDisabled;

  ///is groundstrike checking enabled
  bool groundstrikeFilteringEnabled;

  ///is emapViewer shown
  bool groundstrikeDisplayEnabled;

    //do we send the RNDF graph object? (way way slow for large rndfs)
    bool sendGraphEnabled;

	/// is line fusion disabled
	bool lineFusionDisabled;
  /// fuse elements as they are added with addEl instead of in the main loop
  bool lineFusionCompat;

  /// is output to planner disabled (mapper run locally in planner)
  bool plannerOutputDisabled;

  /// Talker to send RNDF graphs to mplanner
	SkynetTalker<Graph> *graphTalkerPtr;
  /// RNDF graph to send to mplanner
  Graph* rndfGraphPtr;
  /// RNDF structure used by mplanner graph
  RNDF rndf;

 
  /// Time that the last graph was sent
  uint64_t graphTime;

 /// Time that the last decay check was run
  uint64_t decayTime;

  /// Age at which unupdated map elements will be removed
  int decayAgeThresh;

  /// how often to run internal association
  int assocTimeThresh;



  /// time (us) to wait between fusion steps
  int fusionRate;

 /// Time that the last fusion calculations were run
  uint64_t fusionTime;

    /// Fusion parameter for max time difference between elements to be fused
	int fusionTimeThresh;

  /// Time that the last line fusion calculations were run
  uint64_t lineFusionTime;


  /// RNDF filename given on startup
	string RNDFfilename;

    /// Map element used to send prior to debugging channel
    MapElement priorEl;
    
    /// Map element used to send and receive sensed data
    MapElement sensedEl;

  /// Main data structure which stores the prior and sensed map
	Map map;

  /// data structure that maintains the elevation map
  RoadMap *roadMap;
  /// time at which map was last updated
  uint64_t timeRoadMapUpdated;
  uint64_t currentTime;
  //used for removing elements from new map
  vector<int>::iterator mapIterator;

  unsigned long long timingTime;
  unsigned long long statArray[NUM_PERCEPTORS][6];
  unsigned long long lastClearedStats;
  unsigned long long totalTimeAdd, totalTimeFuse, totalTimeClear, totalTimeAssoc;
  unsigned long long totalElAdd, totalElFuse, totalElClear, totalElAssoc;
  unsigned long long maxTimeAdd, maxTimeFuse, maxTimeClear, maxTimeAssoc;
  unsigned long long diffTime;

  unsigned long long numSentToPlanner, clearSentToPlanner;

  //logging options
  bool logging;
  int logLevel;
  string logPath;
  string logFilename;

  //benchmark variables (time spent in some parts of the code)

  uint64_t elapsedUpdateGraph; // time spend in updateGraph
  uint64_t elapsedLineFusion; // time elapsed in line fusion code
  uint64_t elapsedGroundStrike; // time elapsed in groundstrike filtering
  uint64_t elapsedUpdateMap; // time elapsed in updateMap(), including time to recv and send MapElements
  uint64_t elapsedSendRndf; // time elapsed sending rndf1

};



#endif
