/**********************************************************
 **
 **  MAPPER.HH
 **
 **    Time-stamp: <2007-08-20 21:23:13 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Jan 30 11:10:39 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPER_H
#define MAPPER_H

#include <iostream>
#include <assert.h>
#include <vector>
#include <math.h>

#include "dgcutils/DGCutils.hh"

#include <map/MapElement.hh>

// needed for mplanner interface
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
// local headers
#include "skynettalker/SkynetTalker.hh"


#include "travgraph/Graph.hh"
#include "rndf/RNDF.hh"

#include "map/Map.hh"

using namespace std;

enum whichPerceptor{
  LADAR_CAR,
  RADAR_CAR,
  STEREO_OBS,
  PERCEPTOR_UNKNOWN
};
  

class Mapper 
{
	
public:
	

	Mapper();

	~Mapper();
	
public:


 /// Initalize map
  int init(string fname);

  /// Clean up map
  int fini();

  /// Initilize communications 
	int initComm(int snkey);
  /// Clean up communications
	int finiComm();

  /// Update the graph and send it to mplanner
  int updateGraph();


  /// Clears the map
	int resetMap();

  /// Decays the map
	int decayMap();

  /// Updates the map with new data
	int updateMap();

	/// Sets the Global to Local frame offset for the internal map
	void setLocalFrameOffset(VehicleState state);


  /// Takes in a map element and checks if it can be reasonably associated 
  /// with other existing elements in the map.  If so, the element is 
  /// modified and fused into the map.

	int fuseMapElement(MapElement & fusedEl);

	/// Disables fusion of lines inside mapper
	void disableLineFusion(bool disableflag);
	/// Disables fusion of obstacles inside mapper
	void disableObsFusion(bool disableflag);
  
	/// sets the fusedDeltaTime parameter in ms
	void setFusionTimeThresh(int timethresh)
	{fusionTimeThresh = timethresh;}

  /// total number of map elements received
	int totalReceived;
  /// total map size
	int totalMapSize;
  /// total number of map elements fused
	int totalFused;
  /// the time difference between fused elements in ms
  int fusedDeltaTime;

  /// total fake map element sent
	int totalFakes;

 
	/// is obstacle fusion disabled
	bool obsFusionDisabled;

	/// is line fusion disabled
	bool lineFusionDisabled;


  /// Talker to send RNDF graphs to mplanner
	SkynetTalker<Graph> *graphTalkerPtr;
  /// RNDF graph to send to mplanner
  Graph* rndfGraphPtr;
  /// RNDF structure used by mplanner graph
  RNDF rndf;

 
  /// Time that the last graph was sent
  uint64_t graphTime;

 /// Time that the last debugging info was sent
  uint64_t debugTime;

 /// Time that the last decay check was run
  uint64_t decayTime;

  /// Age at which unupdated map elements will be removed
  int decayAgeThresh;

 /// Time that the last fusion calculations were run
  uint64_t fusionTime;

	/// Fusion parameter for max time difference between elements to be fused
	int fusionTimeThresh;

  /// RNDF filename given on startup
	string RNDFfilename;

	/// Map element used to send prior to debugging channel
	MapElement priorEl;

	/// Map element used to send and receive sensed data
	MapElement sensedEl;

  /// Main data structure which stores the prior and sensed map
	Map map;


};



#endif
