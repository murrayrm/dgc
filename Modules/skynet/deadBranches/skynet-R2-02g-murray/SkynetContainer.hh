/*!
 * \file SkynetContainer.hh
 * \brief Parent class for a skynet object
 *
 * \author Unknown
 * \date 2005
 *
 * This is a dummy class that just contains a skynet object. All
 * module helpers virtually derive from this to get the skynet
 * object. This class exists to avoid deriving directly from skynet,
 * which would be unclean.
 *
 * SKYNET_V3: Under version 3 of skynet, there is a single connection
 * to the spred server.  To implement this, the SkynetContainer
 * classes uses static member variables and only opens the connection
 * to skynet on the first call.  It uses a "compatibility class" to
 * implement the skynet version 2 member functions.
 *
 */
 
#ifndef _SKYNETCONTAINER_H_
#define _SKYNETCONTAINER_H_

#include <iostream>
using namespace std;

#include "skynet/sn_msg.hh"
#include "skynet.h"

#ifndef SKYNET_V2
/*!
 * \class SkynetV2Compatible
 * \brief Compatibility class for skynet version 2
 * 
 * This class provides a set of member functions that match the
 * version 2 interface to skynet.  These functions are all calls into
 * the version 3 library and are defined in SkynetContainer.cc.
 *
 */

class SkynetV2Compatible {
public:
  int listen(int type, int somemodule, int channel=0);
  int listen_channel(int type, int channel);

  int get_send_sock(int type);
  void sn_select(int mboxidx);

  bool is_msg(int mboxidx);
  int get_msg_size(int mboxidx, int16 * pMessType = NULL);

  size_t get_msg(int mboxidx, void *mybuf, size_t bufsize, int options = 0);
  size_t get_msg(int mboxidx, void *mybuf, size_t bufsize, int options,
		 pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true,
		 int numMutices = 1);

  size_t send_msg(int type, void* msg, size_t msgsize, int options = 0);
  size_t send_msg(int type, void* msg, size_t msgsize, int options,
		  pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true,
		  int numMutices = 1);

  size_t send_msg(int type, const scatter* msgs, int options = 0);
  size_t send_msg(int type, const scatter* msgs, int options,
		  pthread_mutex_t** ppMutex, bool bReleaseMutexWhenDone = true,
		  int numMutices = 1);

  size_t send_msg_channel(int type, int channel, void* msg, size_t msgsize,
			  int options = 0);
  size_t send_msg_channel(int type, int channel, void* msg, size_t msgsize,
			  int options, pthread_mutex_t** ppMutex,
			  bool bReleaseMutexWhenDone = true,
			  int numMutices = 1);

  size_t send_msg_channel(int type, int channel, const scatter* msgs,
			  int options = 0);
  size_t send_msg_channel(int type, int channel,const scatter* msgs,
			  int options, pthread_mutex_t** ppMutex,
			  bool bReleaseMutexWhenDone = true,
			  int numMutices = 1);

  int status();
  int name();
  mailbox sendMailbox();
};
#endif

/*! Container class for skynet objects */
class CSkynetContainer
{
#ifdef SKYNET_V2
public:
  skynet m_skynet;
  CSkynetContainer() : m_skynet(-1, -1)
  {
    cerr << "CSkynetContainer(): WE SHOULD NOT BE HERE." << endl;
  }
  CSkynetContainer(int snname, int snkey, int* status=NULL)
    : m_skynet(snname, snkey, status)
  {}
#else
private:
  static skynet_server_t *snserver;
  pthread_t thread_id;		// thread for skynet_main()

public:
  /*! Member variable to provide V2 compatibility */
  SkynetV2Compatible m_skynet;

  /* Constructor and deconstructor functions */
  CSkynetContainer();
  CSkynetContainer(int snname, int snkey, int *status=NULL);
  ~CSkynetContainer();

  /*! Function to get the skynet server for this container */
  static skynet_server_t *getServer() { return snserver; }
#endif
};

#endif // _SKYNETCONTAINTER_H_
