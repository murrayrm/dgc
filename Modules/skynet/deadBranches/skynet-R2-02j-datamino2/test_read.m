%logname='/dgc/test_logs/2007-08-18_spread-tests/skynetlogger-stlukeSmallStandard-2007-08-18-Sat-17-27.log';
%logname='/dgc/test_logs/2007-08-21-skynetlogger/skynetlogger-stlukeSmallStandard-2007-08-21-Tue-02-55.log';
logname='/tmp/stereo-test-skynet.log';
mapelVersion = 1;
display(logname);

%skynet_sntype
skynet_sntype_list = {
  'SNpointcloud',
  'SNdeltamap',
  'SNdrivecmd',
  'SNdrivecom',
  'SNactuatorstate',
  'SNmodlist',
  'skynetcom',
  'rmulti',
  'SNtraj',
  'SNstate',
  'SNgetmetastate',
  'SNmetastate',
  'SNGuiMsg_deprecated',
  'SNmodcom_deprecated',
  'SNroadtraj',
  'SNplannertraj',
  'SNmodemantraj_deprecated',
  'SNtrajPlannerSeed',
  'SNtrajPlannerInterm',
  'SNreactiveTraj',
  'SNfusiondeltamap',
  'SNladardeltamap',
  'SNstereodeltamap',
  'SNstaticdeltamap',
  'SNroad2map',
  'SNtimberstring_deprecated',
  'SNmark1',
  'SNmark2',
  'SNladardeltamap_roof',
  'SNladardeltamap_bumper',
  'SNfullmaprequest',
  'SNladarmeas_roof',
  'SNladarmeas_bumper',
  'SNplannerTabInput',
  'SNplannerTabOutput',
  'SNSampleTabOutput',
  'SNSampleTabInput',
  'SNSamplePlotInput',
  'SNreactiveTabOutput',
  'SNreactiveTabInput',
  'SNadrive_command',
  'SNadrive_response',
  'SNtrajfollowerTabOutput_deprecated',
  'SNtrajfollowerTabInput_deprecated',
  'SNasimActuatorState',
  'SNastateTabInput_deprecated',
  'SNfusionmapperTabOutput_deprecated',
  'SNfusionmapperTabInput_deprecated',
  'SNladarfeederTabOutput_deprecated',
  'SNladarfeederTabInput_deprecated',
  'SNladarmeas',
  'SNsuperconMessage',
  'SNsuperconResume',
  'SNsuperconReverse',
  'SNfusionElevDeltaMap',
  'SNladarDeltaMapFused',
  'SNladarDeltaMapStdDev',
  'SNtrajReverse',
  'SNladarDeltaMapNum',
  'SNfusionDeltaMapStdDev',
  'SNguiToTimberMsg',
  'SNselectorTraj',
  'SNcheckerTraj',
  'SNstereoDeltaMapMean',
  'SNstereoDeltaMapFused',
  'SNsuperconTrajfCmd',
  'SNtrajFstatus',
  'SNtrajFspeedCapCmd',
  'SNlowResSpeedDeltaMap',
  'SNsuperConMapAction',
  'SNsuperconPlnCmd',
  'SNsuperconAstateCmd',
  'SNladarRoofDeltaMap',
  'SNladarRoofDeltaMapCost',
  'SNladarRoofDeltaMapElev',
  'SNladarRoofDeltaMapStdDev',
  'SNladarRoofDeltaMapNum',
  'SNladarRoofDeltaMapPitch',
  'SNladarSmallDeltaMap',
  'SNladarSmallDeltaMapCost',
  'SNladarSmallDeltaMapElev',
  'SNladarSmallDeltaMapStdDev',
  'SNladarSmallDeltaMapNum',
  'SNladarSmallDeltaMapPitch',
  'SNladarRieglDeltaMap',
  'SNladarRieglDeltaMapCost',
  'SNladarRieglDeltaMapElev',
  'SNladarRieglDeltaMapStdDev',
  'SNladarRieglDeltaMapNum',
  'SNladarRieglDeltaMapPitch',
  'SNladarBumperDeltaMap',
  'SNladarBumperDeltaMapCost',
  'SNladarBumperDeltaMapElev',
  'SNladarBumperDeltaMapStdDev',
  'SNladarBumperDeltaMapNum',
  'SNladarBumperDeltaMapPitch',
  'SNladarFrontDeltaMap',
  'SNladarFrontDeltaMapCost',
  'SNladarFrontDeltaMapElev',
  'SNladarFrontDeltaMapStdDev',
  'SNladarFrontDeltaMapNum',
  'SNladarFrontDeltaMapPitch',
  'SNstereoShortDeltaMap',
  'SNstereoLongDeltaMap',
  'SNstereoShortDeltaMapElev',
  'SNstereoShortDeltaMapStdDev',
  'SNstereoShortDeltaMapCost',
  'SNstereoLongDeltaMapCost',
  'SNstereoLongDeltaMapElev',
  'SNstereoLongDeltaMapStdDev',
  'SNdeltaMapCorridor',
  'SNsuperConMapDelta',
  'SNmovingObstacle',
  'SNbumperLadarNED',
  'SNsegGoals',
  'SNrddf',
  'SNglobalGloNavMapFromMission',
  'SNglobalGloNavMapFromGloNavMapLib',
  'SNlocalGloNavMap',
  'SNglobalGloNavMapRequest',
  'SNlocalGloNavMapRequest',
  'SNtplannerStatus',
  'SNdplannerStatus',
  'SNdeltaElevationMap',
  'SNtrafficLocalMap',
  'SNgimbalCommand',
  'SNroadLine',
  'SNladarRoofRightDeltaMap',
  'SNladarRoofRightDeltaMapCost',
  'SNladarRoofRightDeltaMapElev',
  'SNladarRoofRightDeltaMapStdDev',
  'SNladarRoofRightDeltaMapNum',
  'SNladarRoofRightDeltaMapPitch',
  'SNladarRoofLeftDeltaMap',
  'SNladarRoofLeftDeltaMapCost',
  'SNladarRoofLeftDeltaMapElev',
  'SNladarRoofLeftDeltaMapStdDev',
  'SNladarRoofLeftDeltaMapNum',
  'SNladarRoofLeftDeltaMapPitch',
  'SNladarBumpRightDeltaMap',
  'SNladarBumpRightDeltaMapCost',
  'SNladarBumpRightDeltaMapElev',
  'SNladarBumpRightDeltaMapStdDev',
  'SNladarBumpRightDeltaMapNum',
  'SNladarBumpRightDeltaMapPitch',
  'SNladarBumpLeftDeltaMap',
  'SNladarBumpLeftDeltaMapCost',
  'SNladarBumpLeftDeltaMapElev',
  'SNladarBumpLeftDeltaMapStdDev',
  'SNladarBumpLeftDeltaMapNum',
  'SNladarBumpLeftDeltaMapPitch',
  'SNladarRearDeltaMap',
  'SNladarRearDeltaMapCost',
  'SNladarRearDeltaMapElev',
  'SNladarRearDeltaMapStdDev',
  'SNladarRearDeltaMapNum',
  'SNladarRearDeltaMapPitch',
  'SNladarmeas_bumpleft',
  'SNladarmeas_bumpright',
  'SNladarmeas_roofleft',
  'SNladarmeas_roofright',
  'SNladarmeas_rear',
  'SNmapElement',
  'SNtplannerStaticCostMap',
  'SNtplannerStaticCostMapRequest',
  'SNocpParams',
  'SNocpObstacles',
  'SNstatePrecision',
  'SNdplStatus',
  'SNprocessRequest',
  'SNprocessResponse',
  'SNastateHealth',
  'SNroaFlag',
  'SNpolyCorridor',
  'SNcorridorCreate',
  'SNcorridorStatus',
  'SNtrajCreate',
  'SNtrajCreateStatus',
  'SNtrajPause',
  'SNtrajPauseStatus',
  'SNtrajEndMission',
  'SNtrajEndMissionStatus',
  'SNbitmapParams',
  'SNptuCommand',
  'SNleadVehicleInfo',
  'SNvehicleCapability',
  'SNgist',
  'SNprocessHealth',
  'SNfollowDir',
  'SNfollowResponse',
  'SNplannerState',
  'SNattentionResponse'
};

if ~exist('fd', 'var')
    fd = fopen(logname);
end

%stereoTime = zeros(10000, 1);
n = 1;
tot = 0;
while ~feof(fd)
    msg = skynet_log_read(fd);
    %fprintf(1, 'MSG: tstamp=%.20g, type=%s, channel=%d, size=%d\n', ...
    %    msg.timestamp, skynet_sntype_list{msg.type}, msg.channel, msg.size);
    if (strcmp(skynet_sntype_list(msg.type), 'SNmapElement'))
        mapel = read_map_element(msg.data, mapelVersion);
        if (length(mapel.id) > 0 && (mapel.id(1) == 81 || mapel.id(1) == 82))
            fprintf('*');
            %fprintf(1, 'MapElement: id=');
            %for k = 1:mapel.numIds
            %    fprintf(1, '%d ', mapel.id(k));
            %end
            %fprintf(1, ', type=%d, channel=%d (subgroup=%d)\n', mapel.type, msg.channel, mapel.subgroup);
            %pause(0.1);
            stereoTime(n) = mapel.state.timestamp;
            n = n + 1;
        else
            fprintf('.');
        end
        tot = tot + 1;
        if mod(tot, 80) == 0
            fprintf(1, '\n');
        end
    end
end
fprintf(1, 'end of log file!\n');
fclose(fd);


