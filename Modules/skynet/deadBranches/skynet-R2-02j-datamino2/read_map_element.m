function mapel = read_map_element(data, version)
% given the raw message data (uint8 format), read a MapElement message
% the version should be 0 for logs before the addition of point2::z,
% or 1 for messages with the z coordinate.

idx = 1;
mapel.subgroup = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
MAP_ELEMENT_ID_LENGTH = 10;
mapel.numIds = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
if (mapel.numIds > MAP_ELEMENT_ID_LENGTH)
    error('mapel:numIds', 'Corrupt MapElement: numIds >= 10!!!');
end
mapel.id = typecast(data(idx:idx+4*mapel.numIds-1), 'int32');
idx = idx + 4*MAP_ELEMENT_ID_LENGTH;
[mapel.conf, idx] = read_float(data, idx, version);
mapel.type = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
[mapel.typeConf, idx] = read_float(data, idx, version);
[mapel.position, idx] = read_point2(data, idx, version);
[mapel.center, idx] = read_point2(data, idx, version);

[mapel.length, idx] = read_float(data, idx, version);
[mapel.width, idx] = read_float(data, idx, version);
[mapel.orientation, idx] = read_float(data, idx, version);

[mapel.lengthVar, idx] = read_float(data, idx, version);
[mapel.widthVar, idx] = read_float(data, idx, version);
[mapel.orientationVar, idx] = read_float(data, idx, version);

[mapel.height, idx] = read_float(data, idx, version);
[mapel.heightVar, idx] = read_float(data, idx, version);

[mapel.elevation, idx] = read_float(data, idx, version);
[mapel.elevationVar, idx] = read_float(data, idx, version);

[mapel.velocity, idx] = read_point2(data, idx, version);

[mapel.timeStopped, idx] = read_float(data, idx, version);
[mapel.peakSpeed, idx] = read_float(data, idx, version);
[mapel.peakSpeedVar, idx] = read_float(data, idx, version);
[mapel.peakAccel, idx] = read_float(data, idx, version);
[mapel.peakAccelVar, idx] = read_float(data, idx, version);

mapel.frameType = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
mapel.timestamp = double(typecast(data(idx:idx+7), 'int64')); idx = idx + 8;

[mapel.state, idx] = read_vehicle_state(data, idx);

mapel.plotColor = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
mapel.plotValue = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;

% number of lines in the label???
mapel.numLines = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
MAP_ELEMENT_LABEL_NUMLINES = 3;
MAP_ELEMENT_LABEL_LENGTH = 50;
if mapel.numLines > MAP_ELEMENT_LABEL_NUMLINES
   warning('mapel:numLines', 'Number of label lines > 3, Corrupted data?'); 
else
    mapel.labels = cell(mapel.numLines, 1);
    for k = 1:mapel.numLines
        startIdx = idx + (k-1)*MAP_ELEMENT_LABEL_LENGTH;
        endIdx = startIdx + MAP_ELEMENT_LABEL_LENGTH;
        mapel.labels{k} = char(data(startIdx:endIdx));
    end
end
idx = idx + MAP_ELEMENT_LABEL_NUMLINES*MAP_ELEMENT_LABEL_LENGTH;

[mapel.geometryMin, idx] = read_point2(data, idx, version);
[mapel.geometryMax, idx] = read_point2(data, idx, version);

mapel.numPts = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
mapel.geometryType = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
MAP_ELEMENT_NUMPTS = 2048;
if (mapel.numPts > MAP_ELEMENT_NUMPTS)
    warning('mapel:numPts', 'Number of geometry points > 2048! Corrupted Data?');
else
    mapel.geometry = cell(mapel.numPts, 1);
    for k = 1:mapel.numPts
        [mapel.geometry{k}, idx] = read_point2(data, idx, version);
    end
end
% Finally, done!!! MapElement is HUGE!!!!!!!!!



function [value, idx] = read_float(data, idx1, version)
if (version > 0)
    float_type = 'single';
    size = 4;
else
    float_type = 'double';
    size = 8;
end
idx = idx1;
value = double(typecast(data(idx:idx+size-1), float_type));
idx = idx + size;




function [pos, idx] = read_point2(data, idx1, version)
idx = idx1;

[pos.x, idx] = read_float(data, idx, version);
[pos.y, idx] = read_float(data, idx, version);
if (version > 0)
    [pos.z, idx] = read_float(data, idx, version);
end
[pos.max_var, idx] = read_float(data, idx, version);
[pos.min_var, idx] = read_float(data, idx, version);
[pos.axis, idx] = read_float(data, idx, version);
