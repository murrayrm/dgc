use 5.008008;
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME => 'Skynet::Spread',
    VERSION_FROM => 'Spread.pm',
    LIBS => ['-lspread'],
);
