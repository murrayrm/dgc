% analyze_run.m - analyze run with spread error
% RMM, 19 Aug 07

% Paths to various files that we need
sandbox = '~murray/tmp/navigation-murray04';
runname = 'stlukeSmallStandard';
rundate = '2007-08-21-Tue';
runtime = '03-01';

logfile = ['skynetlogger-' runname '-' rundate '-' runtime '.log'];
evtfile = ['events-' rundate '-' runtime '.dat'];
pdfbase = ['events-' rundate '-' runtime '_'];

% Add path for skynet scripts
addpath([sandbox '/src/skynet']);

% Load the skynet log file
if (~exist('events'))
  fprintf(1, 'Extracting %s\n', logfile);
  cmd = [sandbox '/bin/Drun skynet-extract --verbose=4 --stop-on-error ' ...
         logfile ' > ' evtfile];
  system(cmd);
  events = skynet_load_log(evtfile);
end

% Process the data in the log file
[type, count] = skynet_type_count(events);
[time, msg, bytes] = skynet_rate_analysis(events, 1);

% Plot out the overall skynet traffic
figure(1); clf;
plot(time, msg, time, bytes/1000);
legend('messages', 'kbytes');

title(logfile);
print(gcf, '-dpng', [pdfbase 'messages.png']);

% Break out MapElement traffic
figure(2); clf;
chnlist = unique(events(:,4));		% get channel numbers
for chn = 1:length(chnlist)
  [ctime, cmsg, cbytes] = ...
    skynet_rate_analysis(events, 1, [163], [chnlist(chn)]);
  chnmsg(:,chn) = cmsg;
  leglist{chn} = ['Ch ' num2str(chnlist(chn))];
end
h = plot(ctime, chnmsg);
legend(h, leglist);
title([logfile ', MapElements']);
print(gcf, '-dpng', [pdfbase 'mapelements.png']);
