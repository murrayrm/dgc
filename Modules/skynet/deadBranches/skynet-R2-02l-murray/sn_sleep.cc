/*
 * \file SN_sleep.cc
 * \brief Safe sleep functions for use with skynet
 *
 * Richard M. Murray
 * 19 December 2006 (from DGCutils.cc)
 *
 * This file contains functions for sleeping a process for a given
 * amount of time.  Unlike the standard sleep functions, the SN_
 * versions of the functions do not wake up on interrupts.
 */

#include <time.h>
#include <errno.h>
#include "skynet.hh"

// Sleeps the specified amount of microseconds
void skynet_usleep(unsigned long microseconds)
{
	timespec req, rem;
	int res;

	req.tv_sec  = microseconds / 1000000;
	req.tv_nsec = (microseconds - req.tv_sec*1000000)*1000;

	do
	{
		res = nanosleep(&req, &rem);
		memcpy(&req, &rem, sizeof(req));
	}
	while(res == -1 && errno == EINTR);
}
