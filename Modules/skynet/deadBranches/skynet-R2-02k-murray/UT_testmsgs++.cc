/*!
 * \file UT_testmsgs++.cc
 * \brief Unit test for receiving messages with C++ V3 interface
 *
 * \author Richard M. Murray
 * \date 23 July 2007
 *
 * This unit test recvs out a bunch of different messages that can be
 * received by the UT_recvmsgs program.  This file uses the C++ interface.
 *
 */

#include <iostream>
#include <assert.h>
#include <pthread.h>
#include "skynet.hh"

/* Thread handlers and callbacks */
void *msg2_thread(void *);
int msg3_callback(char *, int);
void *msg4_thread(void *);
void *msg5_thread(void *);

/* Global variables */
char *msg2_buf;

int main(int argc, char **argv)
{
  /* Get the skynet key */
  int snkey = skynet_findkey(argc, argv, 2);
  assert(snkey != -1);

  SkynetServer *snserver = new SkynetServer(snkey, -3);
  cerr << "Connected to spread server" << endl;

  /*
   * Subscribe to some spread groups in different ways 
   */

  /* Receive messages but don't do anything with them */
  SkynetGroup *msg1 = snserver->listen(0x01);

  /* Receive messages in a buffer and read from a thread */
  msg2_buf = (char *) calloc(1, 10);  assert(msg2_buf != NULL);
  SkynetGroup *msg2 = snserver->listen(0x02, msg2_buf, 10);
  pthread_t msg2_thread_id;
  pthread_create(&msg2_thread_id, NULL, msg2_thread, msg2);

  /* Receive messages via a callback */
  SkynetGroup *msg3 = snserver->listen(0x02, NULL, 0, msg3_callback, 
				      NULL, NULL, 1);

  /* Use get_message */
  SkynetGroup *msg4 = snserver->listen(0x04, NULL, 32, NULL, NULL, NULL, 1);
  pthread_t msg4_thread_id;
  pthread_create(&msg4_thread_id, NULL, msg4_thread, msg4);

  /* Use get_latest */
  SkynetGroup *msg5 = snserver->listen(0x05, NULL, 32);
  pthread_t msg5_thread_id;
  pthread_create(&msg5_thread_id, NULL, msg5_thread, msg5);

  /* Start up the server part way through the listen messages */
  pthread_t thread_id = snserver->start();

  /* Go through and print out when messages are received */
  while (1) {
    int len = msg1->getNextMessage(NULL, 0);
    printf("Received message of type %d, length %d\n", msg1->getType(), len);

    /* Try sending a message */
    snserver->sendMessage(0x04, "UT_testmsgs++", 13, 1);
  }

  /* All done; disconnect from the server */
  delete snserver;
  cerr << "Disconnected from spread server" << endl;
  return 0;
}

void *msg2_thread(void *arg)
{
  SkynetGroup *msg2 = (SkynetGroup *) arg;
  while (1) {
    int len = msg2->getNextMessage();
    printf("Received message of type %d, length %d: ", msg2->getType(), len);
    int i = 0;
    while (i < len && i < 10) putchar(msg2_buf[i++]);
    printf("\n");
  }
}

int msg3_callback(char *data, int len)
{
    int i = 0;
    printf("Received callback of length %d: ", len);
    for (i = 0; i < len; ++i) putchar(data[i]);
    printf("\n");
    return 0;
}

void *msg4_thread(void *arg)
{
  SkynetGroup *msg = (SkynetGroup *) arg;
  char buffer[20];

  while (1) {
    int len = msg->getMessage(buffer, 19);
    buffer[len] = 0;		// terminate the string
    printf("Received message of type %d, length %d: %s\n",
	   msg->getType(), len, buffer);
  }
}

void *msg5_thread(void *arg)
{
  SkynetGroup *msg = (SkynetGroup *) arg;
  char buffer[20];

  while (1) {
    int len = msg->getLatestMessage(buffer, 19);
    buffer[len] = 0;		// terminate the string
    printf("Latest message of type %d, length %d: %s\n",
	   msg->getType(), len, buffer);
    sleep(1);
  }
}
