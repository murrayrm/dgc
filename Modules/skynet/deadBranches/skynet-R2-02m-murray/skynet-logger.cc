/*!
 * \file skynet-logger.cc
 * \brief Log skynet messages
 *
 * \author Richard M. Murray
 * \date 20 May 2007
 *
 */

#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <iostream>
#include <sp.h>
#ifdef MACOSX
#define O_LARGEFILE 0x00		// not used for OSX
#endif

#include "skynet/skynet.hh"		// skynet_findkey()
#include "interfaces/sn_types.h"	// skynet message types
#include "logger.h"			// header types
#include "cmdline_skynet_player.h"

int verbose = 0;			// print verbose error messages

int main(int argc, char **argv)
{
  /* Process command line arguments */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  verbose = cmdline.verbose_arg;

  if (cmdline.inputs_num != 1) {
    cerr << "skynet-logger: wrong number of input arguments" 
	 << " (" << cmdline.inputs_num << ")" << endl;
    cmdline_parser_print_help();
    exit(1);
  }

  /* If we want to store only header information, reset header type */
  if (!cmdline.header_version_given && cmdline.header_only_flag)
    cmdline.header_version_arg = 2;
  if (verbose >= 2) 
    cerr << "Using header version " << cmdline.header_version_arg << endl;

  /* Get the location of the spread server */
  const char *spread_daemon;
  char *pDaemonEnv = getenv("SPREAD_DAEMON");
  if(pDaemonEnv == NULL) {
    if (verbose)
      cerr << "Using default SPREAD_DAEMON: 4803@192.168.0.59" << endl;
    spread_daemon ="4803@192.168.0.59";
  } else {
    spread_daemon = pDaemonEnv;
  }

  /* Get the skynet key */
  /* Open up log file */
  char *logname = cmdline.inputs[0];
  int handle = open(logname, O_WRONLY | O_CREAT | O_LARGEFILE | O_TRUNC);
  if (handle < 0) {
    cerr << "Couldn't open " << logname << "..... exiting" << endl;
    exit(-1);
  }

  /* Set the permissions properly */
  chmod(logname, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
  if (verbose >= 2) cerr << "Opened log file " << logname << endl;

  /* Initialize connection to skynet (in while loop in case we crash) */
  while (1) {

    int snkey = skynet_findkey(argc, argv);
    /* Now connect to the spread server */
    int module_id = MODtimber;
    char private_name[MAX_GROUP_NAME];
    char private_group[MAX_GROUP_NAME];
    mailbox mbox;
    int status;

    snprintf(private_name, sizeof(private_name), "m:%X:%X", snkey, module_id);
    status = SP_connect(spread_daemon, private_name, 0, 0, &mbox, private_group);
    if (verbose) {
      cerr << "Connected to " << spread_daemon << " as " << private_name 
	   << " with group " << private_group << "; status = " << status << endl;
    }

    /* Join all of the groups */
    char group[MAX_GROUP_NAME];
    for (int i = 0; i < last_type; i++) {
      snprintf(group, sizeof(group),
	       "sn:%05d:%05d:%05d", snkey, i, 0);
      status = SP_join(mbox, group);
      if (status != 0 && verbose) cerr << "Error joining group " << i << endl;
    }
    if (verbose) cerr << "Joined " << last_type << " groups" << endl;

    if (cmdline.header_version_arg > 0) {
      /* Join groups that have multiple channels (hardcoded) */
      int subgroup_count = 0;
      struct {
	int group;
	int subgroup_min, subgroup_max;
      } channel[] = {
	{SNmapElement, -10, 10},
	{-1, 0, 0}
      };
      for (int i = 0; channel[i].group != -1; ++i) {
	for (int j = channel[i].subgroup_min; 
	     j <= channel[i].subgroup_max; ++j) {
	  snprintf(group, sizeof(group),
		   "sn:%05d:%05d:%05d", snkey, channel[i].group, j);
	  status = SP_join(mbox, group);
	  if (status != 0 && verbose) 
	    cerr << "Error joining group " << i << endl;
	  else
	    ++subgroup_count;
	}
      }
      if (verbose) cerr << "Joined " << subgroup_count << " channels" << endl;
    }

    /* Read all messages */
    while (1) {
      int num_groups, msgsize, endian, status;
      int16_t mess_type, dummy;
      service service_type;
      char sender[MAX_GROUP_NAME], group[MAX_GROUP_NAME];
#     define MAX_BUFFER_SIZE 1000000
      char packet[MAX_BUFFER_SIZE];

      service_type = dummy = endian = 0;  
      msgsize = SP_receive(mbox, &service_type, sender, 1, &num_groups, &group,
			   &mess_type, &endian, sizeof(packet), packet);

      /* Check for spread errors */
      if (msgsize < 0) {
	if (verbose) {
	  cerr << "skynet-logger: received spread error; aborting" << endl;
	  SP_error(msgsize);
	}
	break;			// break out of logging loop
      }

      /* Mark the time that we received the message */
      timeval tv; gettimeofday(&tv, NULL);
      unsigned long long stamp =
	(unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;

      /* Extract the message type from the mailbox name */
      int msgtype, channel;
      if ((status = sscanf(group, "sn:%*d:%d:%d", &msgtype, &channel)) != 2) {
	/* Not the right type of message */
	if (verbose >= 4)
	  cerr << "wrong message type (" << status << "): " << group << endl;
	continue;
      }

      /* Print out information on the message we received */
      if (verbose >= 3)
	cerr << sn_msg_asString((sn_msg) msgtype) << ": " 
	     << msgsize << " bytes" << endl;

      /* Write the information to file (depending on version) */
      struct skynet_header_type1 type1;
      switch (cmdline.header_version_arg) {
      case 1:
	type1.msgtype = msgtype;		// save the message information
	type1.msgsize = msgsize;
	type1.channel = channel;
	msgtype = SKYNET_HEADER_TYPE1;	// reset message type
	write(handle, (char *) &stamp, sizeof(stamp));
	write(handle, (char *) &msgtype, sizeof(msgtype));
	write(handle, (char *) &type1, sizeof(struct skynet_header_type1));
	write(handle, (char *) packet, msgsize);
	break;

      case 2:
	type1.msgtype = msgtype;		// save the message information
	type1.msgsize = msgsize;
	type1.channel = channel;
	msgtype = SKYNET_HEADER_TYPE2;	// reset message type
	write(handle, (char *) &stamp, sizeof(stamp));
	write(handle, (char *) &msgtype, sizeof(msgtype));
	write(handle, (char *) &type1, sizeof(struct skynet_header_type1));
	/* Don't write data */
	break;

      default:
	write(handle, (char *) &stamp, sizeof(stamp));
	write(handle, (char *) &msgtype, sizeof(msgtype));
	write(handle, (char *) &msgsize, sizeof(msgsize));
	write(handle, (char *) packet, msgsize);
	break;
      }
    }
    /* We only get here on a skynet failure */
    SP_disconnect(mbox);

    /* Loop back and reinitialize connection */
    time_t reset_time = time(NULL);
    cerr << "Resetting at " << ctime(&reset_time) << endl;
  }

  return 0;
}
