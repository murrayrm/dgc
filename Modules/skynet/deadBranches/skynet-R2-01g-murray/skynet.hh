/*!
 * \file skynet.hh
 * \brief Header file for skynet messaging library
 *
 * Richard M. Murray
 * 19 December 2006
 *
 * This file defines all of the functions required to use the skynet library.
 */
#ifndef __SKYNET_HH__ 
#define __SKYNET_HH__

#include "sn_msg.hh"			// skynet class definition

extern int skynet_findkey(int, char **, int debug = 0);
extern void skynet_usleep(unsigned long);

#endif
