function msg = skynet_log_read(fd)

SKYNET_HEADER_TYPE1 = -1;
HEADER_SIZE_0 = 12;
HEADER_SIZE_1 = 12;

% read header
data = uint8(fread(fd, HEADER_SIZE_0));
msg.timestamp = double(typecast(data(1:8), 'int64'));
msg.type = typecast(data(9:12), 'int32');
if (msg.type == SKYNET_HEADER_TYPE1)
    data = uint8(fread(fd, HEADER_SIZE_1));
    msg.type = typecast(data(1:4), 'int32');
    msg.size = typecast(data(5:8), 'int32');
    msg.channel = typecast(data(9:12), 'int32');
else
    msg.size = typecast(uint8(fread(fd, 4)), 'int32');
    msg.channel = 0; % default channel
end

msg.data = uint8(fread(fd, double(msg.size)));
