/**********************************************************
 **
 **  SKYNET-TEST.CC
 **
 **    Time-stamp: <2007-04-24 01:17:30 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Apr 23 23:05:20 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "skynet/sn_msg.hh"//"SkynetContainer.hh"

using namespace std;


uint64_t get_time()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Test writing
int test_write(int argc, const char *argv[])
{
  int snkey, sntype, snchan;
  skynet *skynet_ptr;
  uint64_t time;

  if (argc >= 3)
    snkey = atoi(argv[2]);
  else
    snkey = atoi(getenv("SKYNET_KEY"));

  if (argc >= 4)
    sntype = atoi(argv[3]);
  else
    sntype = 32;

  if (argc >= 5)
    snchan = atoi(argv[4]);
  else
    snchan = 0;

  cout << "Skynet key= " << snkey 
       << " type= " << sntype 
       << " channel= " << snchan << endl;


  skynet_ptr = new skynet(sntype, snkey);

  time = get_time();
 int retval = skynet_ptr->send_msg_channel(sntype,snchan,&time,sizeof(time));
 if (retval<0){
   cerr << "error in sending message " << endl;
 }else {
   cout << "sent message with time " << time << endl;
 }
   
 //retval = skynet_ptr->send_msg(sntype,&time,sizeof(time));
 //if (retval<0){
 //  cerr << "error in sending message " << endl;
 //}else {
 //  cout << "sent message with time " << time << endl;
 // }
  
  return 0;
}


// Test wait and cache read
int test_read(int argc, const char *argv[])
{
  int snkey, sntype, snchan;
  uint64_t time;
  int timedelta;
  skynet *skynet_ptr;
  
  
  if (argc >= 3)
    snkey = atoi(argv[2]);
  else
    snkey = atoi(getenv("SKYNET_KEY"));

  if (argc >= 4)
    sntype = atoi(argv[3]);
  else
    sntype = 32;

  if (argc >= 5)
    snchan = atoi(argv[4]);
  else
    snchan = 0;

  cout << "Skynet key= " << snkey 
       << " type= " << sntype
       << " channel= " << snchan << endl;
  

  skynet_ptr = new skynet(sntype, snkey);
 
      

  
  int id1 = skynet_ptr->listen_channel(sntype,snchan);
  int id2 = skynet_ptr->listen(sntype,0);

  skynet_ptr->get_msg(id1,&time,sizeof(time));
  timedelta = get_time()-time;
  cout << "received message with time " << time << endl;
  cout << " time delta =  " << timedelta << endl;

  skynet_ptr->get_msg(id2,&time,sizeof(time));
  timedelta = get_time()-time;
  cout << "received message with time " << time << endl;
  cout << " time delta =  " << timedelta << endl;

  return 0;
}


int main(int argc, const char *argv[])
{

  if (argc < 2){
    cout << "usage: " << argv[0] << "  [r|w]" 
         << " key type channel " <<endl;
    return 0;
  }
  if (argv[1][0] == 'w')
    test_write(argc, argv);
  else if (argv[1][0] == 'r')
    test_read(argc, argv);
  
  return 0;
}
  
    
  
 
