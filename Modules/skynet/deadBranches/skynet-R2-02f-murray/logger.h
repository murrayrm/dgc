/*!
 * \file logger.h
 * \brief Header definitions for skynet log messages
 *
 * \author Richard M. Murray
 * \date 19 August 2007
 *
 */

#define SKYNET_HEADER_TYPE1 -1
struct skynet_header_type1 {
  int msgtype;				/* skynet message type */
  int msgsize;				/* skynet message size */
  int channel;				/* skynet message channel */
};
