function [state, idx]=read_vehicle_state(data, idx1)
if nargin < 2
    idx = 1;
else
    idx = idx1;
end

state.timestamp = double(typecast(data(idx:idx+7), 'int64')); idx = idx + 8;

state.utmNorthing = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmEasting = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmAltitude = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

%state.GPS_Northing_deprecated = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
%state.GPS_Easting_deprecated = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
idx = idx + 8*2; % skip deprecated fields

state.utmNorthVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmEastVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmAltitudeVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

%state.Acc_N_deprecated = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
%state.Acc_E_deprecated = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
%state.Acc_D_deprecated = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
idx = idx + 8*3; % skip deprecated fields

state.utmRoll = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmPitch = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmYaw = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.utmRollRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmPitchRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmYawRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

idx = idx + 8*4; % other deprecated fields

state.utmNorthConfidence = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmEastConfidence = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.utmAltitudeConfidence = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.rollConfidence = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.pitchConfidence = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.yamConfidence = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.vehSpeed = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.utmZone = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;
state.utmLetter = typecast(data(idx:idx+3), 'int32'); idx = idx + 4;

state.vehXVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.vehYVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.vehZVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.vehRollRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.vehPitchRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.vehYawRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.localX = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localY = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localZ = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.localRoll = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localPitch = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localYaw = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.localXVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localYVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localZVel = typecast(data(idx:idx+7), 'double'); idx = idx + 8;

state.localRollRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localPitchRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
state.localYawRate = typecast(data(idx:idx+7), 'double'); idx = idx + 8;
