/*!
 * \file skynet.hh
 * \brief Header file for skynet messaging library
 *
 * Richard M. Murray
 * 19 December 2006
 *
 * This file defines all of the functions required to use the skynet
 * library.  For skynet V3, this file describes a Skynet class that
 * can be used as a more convenient interface to the skynet API.
 */
#ifndef __SKYNET_HH__ 
#define __SKYNET_HH__

#include "sn_msg.hh"			// skynet class definition

extern int skynet_findkey(int, char **, int debug = 0);
extern void skynet_usleep(unsigned long);

#ifndef SKYNET_V2
#include <assert.h>
#include "skynet.h"		// C API

/*!
 * \class SkynetGroup
 * \brief C++ class for maintaining skynet communications group
 *
 * The SkynetGroup class is a shell for the skynet_group structure.
 * It provides C++ access to send and receive messages.
 *
 */

class SkynetGroup {
private:
  skynet_group_t *sngroup;

public:
  /*! Generate a skynet group */
  SkynetGroup(skynet_server_t *snserver, int type, char *buf = NULL,
	      int len = SKYNET_MAX_MESSAGE_LENGTH,
	      int (*handler)(char *, int) = NULL,
	      pthread_mutex_t *mutex = NULL, pthread_cond_t *cond = NULL,
	      int channel = 0) {
    sngroup = skynet_listen(snserver, type, buf, len, handler,
			    mutex, cond, channel);
    assert(sngroup != NULL);
  };

  /*! Remove a skynet group */
  ~SkynetGroup() {
    skynet_ignore(sngroup);
  };

  /* Calls to the C API library */
  int getMessage(char *msgbuf = NULL, int buflen = 0) {
    return skynet_get_message(sngroup, msgbuf, buflen);
  }
  int getNextMessage(char *msgbuf = NULL, int buflen = 0) {
    return skynet_get_next(sngroup, msgbuf, buflen);
  }
  int getLatestMessage(char *msgbuf = NULL, int buflen = 0) {
    return skynet_get_latest(sngroup, msgbuf, buflen);
  }
  int peek(int *sizep = NULL) {
    return skynet_peek(sngroup, sizep);
  }

  /* Accessor functions */
  skynet_group_t *getGroup() { return sngroup; }
  int getType() { return sngroup->msgtype; }
};

/*!
 * \class SkynetServer
 * \brief C++ class for a skynet server 
 *
 * The SkynetServer class is a shell for the skynet_server structure.
 * It provides C++ access to establish a connection and join skynet
 * communication groups.
 *
 * \function SkynetServer::listen
 * \brief Establish a connection to a skynet group
 *
 * The listen() member function establishes a connection to a given
 * skynet message type and channel across an existing conection.  It
 * can be called in multiple forms, depending on what type of access
 * you would like to the skynet messages:
 *
 *   snserver.listen(type) - basic access via get*Message
 *   snserver.listen(type, callback) - callback access to message
 *
 * Additional arguments are described in the skynet_listen() function.
 */

class SkynetServer {
private:
  skynet_server_t *snserver;

public:
  /*! Generate a connection to the server */
  SkynetServer(int snkey, int module) {
    snserver = skynet_connect(snkey, module);
    assert(snserver != NULL);
  };

  /*! Disconnect from the spread server */
  ~SkynetServer() {
    skynet_disconnect(snserver);
  };

  /*! Listen for messages on a skynet channel */
  SkynetGroup *listen(int type, char *buf = NULL,
		      int len = SKYNET_MAX_MESSAGE_LENGTH,
		      int (*handler)(char *, int) = NULL,
		      pthread_mutex_t *mutex = NULL, 
		      pthread_cond_t *cond = NULL,
		      int channel = 0) {
    SkynetGroup *
      sngroup = new SkynetGroup(snserver, type, buf ,len, handler,
				mutex, cond, channel);
    assert(sngroup != NULL);
    return sngroup;
  };

  /*! Listen for skynet messages using a callback function */
  SkynetGroup *listen(int type, int (*handler)(char *, int),
		      pthread_mutex_t *mutex = NULL, 
		      pthread_cond_t *cond = NULL,
		      int channel = 0) {
    return listen(type, NULL, 0, handler, mutex, cond, channel);
  };


  /*! Ingore messages on a skynet channel */
  void ignore(SkynetGroup *sngroup) {
    delete sngroup;
  }

  /*! Start a thread to listen to messages */
  pthread_t start() {
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, skynet_main_thread, (void *) snserver);
    return thread_id;
  }

  int sendMessage(int type, char *msgbuf, int buflen, int channel=0) {
    return skynet_send_message(snserver, type, msgbuf, buflen, channel);
  }

  /* Accessor functions */
  skynet_server_t *getServer() { return snserver; }
};
#endif

#endif
