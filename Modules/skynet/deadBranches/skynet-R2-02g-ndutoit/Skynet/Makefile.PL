use 5.008008;
use ExtUtils::MakeMaker;

$CC = "g++";

# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    NAME => 'Skynet',
    DIR => ['Spread', 'Parse'],
	# Un-comment this if you add C files to link with later:
    # OBJECT            => '$(O_FILES)', # link all the C files too
);
