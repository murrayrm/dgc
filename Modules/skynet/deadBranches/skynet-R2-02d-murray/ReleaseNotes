              Release Notes for "skynet" module

Release R2-02d-murray (Sun Aug 19 14:07:54 2007):
	Added channel information to logger, player and extract.  This will
	allow proper logging, replay and analysis of MapElement data, which
	makes use of channels.  Not fully tested, but going into the field
	this afternoon.  Use --header-version=0 to use old header format
	(but should be backward compatible). 

Release R2-02d (Sun Aug 19 10:27:41 2007):
	Added some tools for analyzing skynet logs:
	  * skynet-extract parses a log file and creates a MATLAB 
	    compatible event file
	  * skynet_load_log.m - load (event) log file in MATLAB
	  * skynet_type_count.m - count number of messages of each type
	  * skynet_rate_analysis.m - analyze rate of messages from log

	Documentation for the MATLAB scripts is available using the MATLAB
	help function.  For an example of how to use the files, see
	'/dgc/test_logs/2007-08-18_spread-tests/analysis_run2.m'.

Release R2-02c (Fri Aug  3 19:53:04 2007):
	Added empty SkynetV2Compatible::send_msg() for scatter messages.
	This is only used in cmap for sending DeltaCMaps, so the function
	just prints an error message and aborts.  Allows cmap module test
	function to compile.  Shouldn't cause any problems with any modules
	(but if you get an abort, let me know and I can implement the
	function). 

Release R2-02b (Sat Jul 28 23:21:15 2007):
	Fixed a bug in some assertion logic that was causing all modules
	that used SkynetContainer to abort.

Release R2-02a (Sat Jul 28 20:42:31 2007):
	This release implements the skynet v3 functionality as the default
	behavior for SkynetContainer class.  In addition to the C API
	defined on the [[skynet]] wiki page, a C++ API is now available that
	provides access to the full version 3 functionality.  Doxygen
	documentation of the C and C++ APIs is available at

	  http://gc.caltech.edu/yam/DGCdocs/modules/skynet/html

	The main new features of version 3 are:
	  * Single connection to the spread server
	  * Better support for has-a dependencies
	  * Legacy support for version 2 interface

	In order to use these functions, you need to recompile all modules
	that use CSkynetContainer (just about anything that talks to
	skynet).  New builds of these modules will be released later tonight
	(so if you are using link modules, you should just be able to update
	the links).  Pre-compiled made with version 2 are compatible with
	those using version 3.

Release R2-01h (Wed Jul 25  8:59:35 2007):
	This is the first release of the skynet v3 functionality, as
	described on the [[skynet]] wiki page.  This v3 API is not currently
	used by any of the v2 functions, so this will not require changes
	(or recompiles) to any existing modules.

	All of the functions described in the v3 spec on the wiki page have
	been implemented except skynet_update().  There are some simple unit
	tests in place, but rigorous testing has not yet been done.  The v3
	interface should be considered unstable at this point since changes
	may be required as we start using v3 for CStateClient and other
	functions.  

	No changes to skynet functionality will take place before the 28 Jul
	field test and changes after that will be backward compatible with
	the v2 API, so no changes will be required to run old code.  More
	info on changes on bug 3327 in bugzilla.

Release R2-01g (Wed Jul 11 17:02:59 2007):
	Added Perl module interface to skynet.

Release R2-01f (Tue Jul  3 14:50:20 2007):
        Added gengetopt cmdline parsing to skynet-player. Added cmdline option for continuous replay and skynet key. Removed continuous replay bug. 

Release R2-01e (Tue Jul  3 11:33:57 2007):
	My stupid mistake... don't indicate error where nothing failed.
	Sorry, everyone.

Release R2-01d (Tue Jul  3 11:23:48 2007):
	Saner error handling - don't exit(-1) for every little thing.
	Errors in the formatting of skynet key etc. are still fatal,
	as they aren't dependent on vagaries of the network. But it'll
	now try to reconnect to Spread when a connection goes down,
	so things should be slightly more robust now.

Release R2-01c (Wed May 23 22:51:00 2007):
	Skynet-logger now logs skynet messages of any type, allowing better
	playback through viewers.  Also added gengetopt command line parsing
	plus streamlined the code by calling spread directly instead of
	creating a bunch of skynet threads.

Release R2-01b (Wed May 23  7:49:19 2007):
	Fixed permissions on skynet-logger so that log file is 0x664.
	Currently only logs navigation-style messages; will work on more
	messages later today.

Release R2-01a (Fri May  4 14:54:10 2007):
  Minor tweaks for has-a semantics; no changes to existing APIs.

Release R2-01 (Thu Apr 26 12:00:05 2007):
  Added the functionality to allow messages of the same type to be
sent to and received by isolated groups.  This is done on the
receiving side by extending the listen() function to add an optional
channel parameter, as well as defining a listen_channel() function.
See the comments in the header file for detail.s To send messages
which specify a channel the send_msg_channel() functions have been
added. There should be no change in use of skynet if channels aren't
used.  The spread group name is formatted as sn:KKKKK:TTTTT:CCCCC
where KKKKK is the skynet key, TTTTT is the type and CCCCC is the
channel. The fields have fixed widths and the channel can take
positive or negative values, for example sn:00092:00010;-0001.

Release R2-00f (Fri Mar 16 20:14:37 2007):
        Modified skynet-logger to log a subset of messages  (fewer threads created). You can change the list of logged messages in the file skynet-logger.c
	c.
Release R2-00e (Fri Mar 16 16:05:52 2007):
	Added skynet-logger and skynet-player programs.  These are copies of
	the 'author' and 'logplayer' programs from dgc/trunk, with headers
	updated to match YaM locations.  Stefano is going to work on getting
	these to be useful for logging and playback of skynet messages.

Release R2-00d (Sat Mar 10 17:29:22 2007):
        Added conditional definition in the header file.	

Release R2-00c (Sat Mar  3 14:02:41 2007):
	Minor update to fix Makefile for trunk/rddfplanner

Release R2-00b (Sat Mar  3  9:25:51 2007):
	Added processing of --skynet-key and -S, for compatibility with
	command line option standards (bug 3151).  Everything is backward
	compatible, so --snkey is still processed.

Release R2-00a (Tue Feb 20 15:26:57 2007):
	Moved sn_types to interfaces. Added SkynetContainer and got rid of unnecessary dependencies.

Release R1-00e (Thu Feb  8 19:10:43 2007):
	Added SNmapElement message type to sn_types.h

Release R1-00d (Tue Feb  6 19:28:57 2007):
	Added method skynet::get_msg_size() to get the size of a message
	before actually reading it, to allow allocating a buffer of the
	exact size. The new generic talker SkynetTalker needs this.

Release R1-00c (Fri Feb  2 15:41:49 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-00b (Wed Jan 10 22:42:56 2007):
	Fixed up Makefile.yam to link all of the necessary include files.
	This version has been tested by compiling against adrive, running
	with simulator and trajFollower and seeing that messages are
	recieved.  Don't yet have enough code integrated into YaM to 
	check more than that at this point.

Release R1-00a (Wed Jan 10 16:17:13 2007):
	This is the first release of the skynet code for YaM.  This
	module is now independent of DGCutils (replicated the safe
	sleep code) and has Doxygen documentation plus a YaM makefile.

	The basic functionality is just copied over from dgc/trunk.

Release R1-00 (Wed Jan 10 13:05:53 2007):
	Created.
























