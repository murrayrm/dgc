/*!
 * \file skynet.c
 * \brief Skynet library API (C)
 *
 * \author Richard M. Murray
 * \date 23 July 2007
 *
 * This file has the main functions for the skynet API in C.  This is
 * mainly a wrapper around spread that provides some functionality for
 * different skynet keys and message channels.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "skynet.h"

/* Connect to the spread server */
skynet_server_t *skynet_connect(int snkey, int modid)
{
  char private_name[MAX_GROUP_NAME];
  char private_group[MAX_GROUP_NAME];
  mailbox mbox;
  int status;

  /* Get the location of the spread server */
  char *spread_daemon = getenv("SPREAD_DAEMON");
  assert(spread_daemon != NULL);

  /* Connect up to the spread server */
  int module_id_count = 0;
  do {
    if (modid >= 0) {
      /* Try to generate unique module ID by cycling until we find something */
      snprintf(private_name, sizeof(private_name), "m:%X:%X:%d", 
	       snkey, modid, module_id_count++);

      /* Give up if we don't get a connection after a reasonable time */
      assert(module_id_count <= 100);
    } else {
      /* If modid is negative, don't allow multiple connections */
      snprintf(private_name, sizeof(private_name), "m:%X:%X", snkey, -modid);
    }

    /* Attempt to connect to the spread server */
    status = SP_connect(spread_daemon, private_name, 0, 0, 
			&mbox, private_group);
  } while (status == REJECT_NOT_UNIQUE && modid >= 0);
  

  /* If we didn't connect, return NULL */
  if (status != ACCEPT_SESSION) {
    SP_error(status);
    return NULL;
  }

 /* Allocate space for storing information about this connection */
  skynet_server_t *snserver = 
    (skynet_server_t *) calloc(1, sizeof(skynet_server_t));
  assert(snserver != NULL);

  snserver->data = (char *) malloc(SKYNET_MAX_MESSAGE_LENGTH);
  assert(snserver->data != NULL);

  snserver->mutex = (pthread_mutex_t *) calloc(1, sizeof(pthread_mutex_t));
  assert(snserver->mutex != NULL);
  pthread_mutex_init(snserver->mutex, NULL);

  /* Save the information that we need for this connection */
  snserver->skynet_key = snkey;
  snserver->module_id = modid < 0 ? -modid : modid;
  snserver->spread_mbox = mbox;

  /* All done; send back the pointer to the information */
  return snserver;
}

/* Disconnect from the spread server */
int skynet_disconnect(skynet_server_t *snserver)
{
  if (snserver == NULL) return -1;

  /* Tell skynet_main to exit, if it is running */
  snserver->quit = 1;

  /* Disconnect from the spread server */
  return SP_disconnect(snserver->spread_mbox);
}

/* start listening to messages of a given type */
skynet_group_t *skynet_listen(skynet_server_t *snserver, 
			      int msgtype, char *msgbuf, int msglen,
			      int (*handler)(char *, int),
			      pthread_mutex_t *mutex,
			      pthread_cond_t *cond, int channel)
{
  /* Do some sanity checking on the arguments */
  assert(!(cond != NULL && mutex == NULL));

  /* Allocate space to store the message information */
  skynet_group_t *sngroup = calloc(1, sizeof(skynet_group_t));
  if (sngroup == NULL) return NULL;

  /* Store the information */
  sngroup->server = snserver;
  sngroup->spread_mbox = snserver->spread_mbox;
  sngroup->msgtype = msgtype;
  sngroup->channel = channel;
  sngroup->handler = handler;

  /* Construct the group name to use for this message type */
  char group[MAX_GROUP_NAME];
  snprintf(group, sizeof(group), "sn:%05d:%05d:%05d", snserver->skynet_key, 
	   msgtype, channel);
  sngroup->name = strdup(group);

  /* Decide whether we need to allocate internal buffer space */
  if (msgbuf == NULL && msglen > 0) msgbuf = (char *) malloc(msglen);

  /* Store the information about the group */
  sngroup->buflen = msglen;
  sngroup->msgbuf = msgbuf;
  sngroup->mutex = mutex;
  sngroup->cond = cond;

  /* If we weren't given a condition, set one for use internally */
  if (cond == NULL) {
    /* If there wasn't a mutex, then create one */
    if (mutex == NULL) {
      sngroup->mutex = (pthread_mutex_t *) calloc(1, sizeof(pthread_mutex_t));
      assert(sngroup->mutex != NULL);
      pthread_mutex_init(sngroup->mutex, NULL);
    }

    /* Now create the conditional */
    sngroup->cond = (pthread_cond_t *) calloc(1, sizeof(pthread_cond_t));
    assert(sngroup->cond != NULL);
    pthread_cond_init(sngroup->cond, NULL);
  }

  /* Join the appropriate spread group */
  int status = SP_join(snserver->spread_mbox, group);
  if (status != 0) {
    /* Something went wrong; release resources and go away */
    if (status != 0) SP_error(status);
    skynet_ignore(sngroup);
    return NULL;
  }

  /* Add the group to our list of ones to watch */
  pthread_mutex_lock(snserver->mutex);
  assert(snserver->ngroups < SKYNET_MAXGROUPS);

  /* Find an empty slot */
  int i;
  for (i = 0; i < snserver->ngroups && snserver->groups[i] != NULL; ++i); 
  snserver->groups[i] = sngroup;

  /* Increment the number of groups */
  snserver->ngroups++;
  pthread_mutex_unlock(snserver->mutex);

  /* Return the group we just created */
  return sngroup;
}

/* Disconnect from a skynet channel */
int skynet_ignore(skynet_group_t *sngroup)
{
  if (sngroup == NULL) return -1;

  /* Leave the spread group */
  int status = -1;
  if (sngroup->name != NULL) {
    status = SP_leave(sngroup->spread_mbox, sngroup->name);
    if (status != 0) SP_error(status);
  }

  /* Free up the resources */
  if (sngroup->name != NULL) free(sngroup);
  free(sngroup);

  return status;
}

/* Read messages and process them */
void skynet_main(skynet_server_t *snserver)
{  
  while (!snserver->quit) {
    /* Declare the variables needed to receive a packet */
    int num_groups, endian = 0, msg_len, status;
    int16_t msg_type;
    service service_type = 0;
    char sender[MAX_GROUP_NAME];
    char group[MAX_GROUP_NAME];
    char *data = snserver->data;
#   define SKYNET_MAX_MESSAGE_LEN = 0x10000;

    /* Read the next message (blocking) */
    status = SP_receive(snserver->spread_mbox, &service_type, sender, 1,
			&num_groups, &group, &msg_type, &endian,
			SKYNET_MAX_MESSAGE_LENGTH, data);

    if (status < 0) {
      if (status == BUFFER_TOO_SHORT) {
	/* Return the portion of the message we received */
	msg_len = SKYNET_MAX_MESSAGE_LENGTH;
      } else {
	fprintf(stderr, "skynet::main - spread error (%d)\n", status);
	SP_error(status);
	exit(1);
      }
      continue;
    } else 
	msg_len = status;

    /* If this is a membership message, ignore it */
    if (service_type & MEMBERSHIP_MESS) continue;

    /* Look at the message type and process accordingly */
    int i; skynet_group_t *sngroup;
    pthread_mutex_lock(snserver->mutex);
    for (i = 0; i < snserver->ngroups; ++i) {
      /* Skip over groups that have been deleted */
      if (snserver->groups[i] == NULL) continue;

      /* Skip over groups that don't match */
      if (strcmp(snserver->groups[i]->name, group) != 0) continue;

      /* Process the message (mutex locked) */
      sngroup = snserver->groups[i];
      pthread_mutex_lock(sngroup->mutex);
      sngroup->msglen = msg_len;	/* save received message length */
      sngroup->msg_ready = 1;		/* mark message as ready */
      sngroup->msg_count++;		/* increment message count */
      if (sngroup->msgbuf != NULL) 
	memcpy(sngroup->msgbuf, data, sngroup->buflen < sngroup->msglen ?
	       sngroup->buflen : sngroup->msglen);

      /* Call the callback function if it exists */
      if (sngroup->handler != NULL) (*sngroup->handler)(data, msg_len);

      /* Signal to everyone that we have received a message */
      if (sngroup->cond != NULL) pthread_cond_broadcast(sngroup->cond);

      /* Done processing protected data */
      pthread_mutex_unlock(sngroup->mutex);
    }
    pthread_mutex_unlock(snserver->mutex);
  }
}

/* Call skynet_main() in a thread */
void *skynet_main_thread(void *snserver)
{
  skynet_main((skynet_server_t *) snserver);
  return NULL;
}

/*
 * Functions to read data from skynet
 *
 * The functions below read skynet messages in various ways.  Each of
 * these functions must do the following:
 *
 *   + Lock the sngroup->mutex when accessing data
 *   + Reset the sngroup->msg_ready flag when done processing data
 *   + Unlock the sngroup->mutex before returning
 *
 */

/* Get the most recent unread message of a given type */
int skynet_get_message(skynet_group_t *sngroup, char *msgbuf, int buflen)
{
  int len;

  /* If no message is ready, block */
  pthread_mutex_lock(sngroup->mutex);
  while (!sngroup->msg_ready) pthread_cond_wait(sngroup->cond, sngroup->mutex);
  
  /* Copy the message into the supplied buffer, if available */
  if (msgbuf != NULL && sngroup->msgbuf != NULL) {
    memcpy(msgbuf, sngroup->msgbuf, buflen < sngroup->msglen ? 
	   buflen : sngroup->msglen);
  }
  len = sngroup->msglen;		/* record message length */

  /* Mark the message as read */
  sngroup->msg_ready = 0;
  pthread_mutex_unlock(sngroup->mutex);

  /* Return the length of the message that was sent; 0 if no buffer avail */
  return sngroup->msgbuf == NULL ? 0 : len;
}

/* Wait until a message of the given type arrives (blocking) */
int skynet_get_next(skynet_group_t *sngroup, char *msgbuf, int buflen)
{
  /* Reset the message ready flag so that we want for next message */
  pthread_mutex_lock(sngroup->mutex);
  sngroup->msg_ready = 0;
  pthread_mutex_unlock(sngroup->mutex);

  return skynet_get_message(sngroup, msgbuf, buflen);
}

/* Copy the most recently received message into a supplied buffer */
int skynet_get_latest(skynet_group_t *sngroup, char *msgbuf, int buflen)
{
  int len;

  /* Check to see if we have received any messages yet */
  if (sngroup->msg_count == 0) {
    /* No messages received, so block and wait */
    return skynet_get_message(sngroup, msgbuf, buflen);
  }

  /* Lock access to the message buffer */
  pthread_mutex_lock(sngroup->mutex);

  /* Copy the message into the supplied buffer, if available */
  if (msgbuf != NULL && sngroup->msgbuf != NULL) {
    memcpy(msgbuf, sngroup->msgbuf, buflen < sngroup->msglen ? 
	   buflen : sngroup->msglen);
  }
  len = sngroup->msglen;		/* record length of message */

  /* Mark the message as read */
  sngroup->msg_ready = 0;
  pthread_mutex_unlock(sngroup->mutex);

  return len;
}

/* Check to see if there is a new message available */
int skynet_peek(skynet_group_t *sngroup, int *msglenp)
{
  int ready;

  /* See if the message is ready and save the length */
  pthread_mutex_lock(sngroup->mutex);
  ready = sngroup->msg_ready;
  if (msglenp != NULL) *msglenp = sngroup->msglen;
  pthread_mutex_unlock(sngroup->mutex);

  /* Let the user know if there is a message ready */
  return ready;
}

/*
 * Functions to send data through skynet
 *
 * Right now there is just one function that sends data
 *
 */

/* Send a message */
int skynet_send_message(skynet_server_t *snserver, int msgtype, char *msgbuf,
		    int msglen, int channel)
{
  int service_type = FIFO_MESS;

  /* Construct the group name to use for this message type */
  char group[MAX_GROUP_NAME];
  snprintf(group, sizeof(group), "sn:%05d:%05d:%05d", snserver->skynet_key, 
	   msgtype, channel);

  return SP_multicast(snserver->spread_mbox, service_type, group, 0,
		      msglen, msgbuf);
}

/*
 * Utility functions
 *
 * skynet_get_group_index - return index to skynet group
 * skynet_get_group_pointer - return pointer to skynet group
 *
 */

int skynet_get_group_index(skynet_server_t *snserver, skynet_group_t *sngroup)
{
  int i;

  /* Go through the list and look for the group pointer */
  pthread_mutex_lock(snserver->mutex);
  for (i = 0; i < snserver->ngroups; ++i) {
    if (snserver->groups[i] == sngroup) break;
  }
  if (i == snserver->ngroups) i = -1;
  pthread_mutex_unlock(snserver->mutex);

  /* Return the index (or -1 on error) */
  return i;
}

skynet_group_t *skynet_get_group_pointer(skynet_server_t *snserver, int index)
{
  return (index < 0 || index >= snserver->ngroups) ?
    NULL : snserver->groups[index];
}
