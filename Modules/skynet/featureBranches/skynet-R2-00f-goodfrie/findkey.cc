/*!
 * \file findkey.c
 * \brief Determine what the skynet key is
 *
 * \author Richard M. Murray
 * \date 30 December 2006
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*!
 * \fn int skynet_findkey(int argc, char **argv, int debug)
 * \brief Determine the skynet key from command line arguments
 *
 * This function searches through command line arguments and looks for
 * the --skynet-key argument or uses the SKYNET_KEY environment
 * variable.  If no key is found in either place, zero is returned.
 * -S can also be used to specify the key.
 *
 * For backward compatibility, the --snkey argument can also be usd.
 *
 */

int skynet_findkey(int argc, char **argv, int debug)
{
  char *skynet_key;
  int snkey = -1;
  int i;

  /* Start by looking for the SKYNET_KEY environment variable */
  if ((skynet_key = getenv("SKYNET_KEY")) != NULL) snkey = atoi(skynet_key);

  for (i = 1; i < argc; ++ i) {
    /* Look for the skynet option as a single argument */
    if (sscanf(argv[i], "--skynet-key=%d", &snkey) == 1) break;
    else if (sscanf(argv[i], "-S=%d", &snkey) == 1) break;

    /* Also check for skynet key as arguments */
    if ((strcmp("--skynet-key", argv[i]) == 0) && ++i < argc) {
      snkey = atoi(argv[i]);
      break;
    } else if ((strcmp("-S", argv[i]) == 0) && ++i < argc) {
      snkey = atoi(argv[i]);
      break;
    }

    /* For backward compatibility, also earch for the skney argument */
    /* Look for the skynet option as a single argument */
    if (sscanf(argv[i], "--snkey=%d", &snkey) == 1) break;

    /* Also check for skynet key as arguments */
    if ((strcmp("--snkey", argv[i]) == 0) && ++i < argc) {
      snkey = atoi(argv[i]);
      break;
    }
  }

  /* Make sure someone set the key */
  if (snkey == -1) {
    if (debug > 0) fprintf(stderr, "No skynet key specified; assuming 0\n");
    snkey = 0;
  } else if (debug > 1) {
    /* Print out the final skynet key being used */
    fprintf(stderr, "skynet key = %d\n", snkey);
  }

  return snkey;
}
