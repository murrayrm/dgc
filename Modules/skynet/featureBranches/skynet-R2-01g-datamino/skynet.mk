SKYNET_PATH = $(DGC)/util/skynet

SKYNET_DEPEND_SOURCES = \
	$(SKYNET_PATH)/sn_msg.cc \
	$(SKYNET_PATH)/sn_sleep.cc \
	$(SKYNET_PATH)/sn_msg.hh \
	$(SKYNET_PATH)/skynet.hh \
	$(SKYNET_PATH)/findkey.cc \
	$(SKYNET_PATH)/sn_types.h 

SKYNET_DEPEND_LIBS = 

SKYNET_DEPEND = $(SKYNET_DEPEND_LIBS) $(SKYNET_DEPEND_SOURCES)
