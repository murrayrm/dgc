#include <iostream>
#include <sstream>
using namespace std;

#include <fcntl.h>
#include <stdlib.h>
#include <math.h>

#include "sn_msg.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "cmdline_skynet_player.h"

#define MAX_BUFFER_SIZE 1000000
#ifdef MACOSX
#define O_LARGEFILE 0x00	// not defined (needed?) in OS X
#endif

struct SContext
{
  skynet*          pSkynet;
  int              handle;
  int*             sockets;
  char*            pBuffer;
  double           speed;
  int*             pPosition;
};

/******************************************************************************/
/******************************************************************************/
void prompt()
{
  cout << "> ";
  cout.flush();
}


/******************************************************************************/
/******************************************************************************/
void print_help()
{
  cout << "\n Enter the number of messages you want to playback.\n";
  cout << " Precede with + to FF a certain number of messages.\n";
  cout << " Precede with = to FF to a specific position in the future.\n";
  cout << " s# plays back at speed # (1.0 = real time).\n";
  cout << " 'h' to print this help message; '0' to quit.\n";
}


/******************************************************************************/
/******************************************************************************/
bool readHdr(int handle, int& msgtype, int& msgsize, unsigned long long& 
                timestamp)
{
  if(read(handle,(char*)&timestamp, sizeof(timestamp)) != sizeof(timestamp) ||
     read(handle,(char*)&msgtype, sizeof(msgtype))     != sizeof(msgtype)   ||
     read(handle,(char*)&msgsize, sizeof(msgsize))     != sizeof(msgsize))
  {
    cerr << "reached end of file" << endl;
    return false;
  }
  if(msgtype < 0 || msgtype >= last_type)
  {
    cerr << "msgtype too large; corrupt data;" << endl;
    return false;
  }

  // Code for writing table of message types and sizes
  //cerr << timestamp << "\t" << msgtype << "\t" << msgsize << endl;

  return true;
} // end readHdr()


/******************************************************************************/
/******************************************************************************/
void* play(void *pArg)
{
  int old;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &old);
  pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, &old);

  SContext *pContext = (SContext*)pArg;
  unsigned long long starttime;
  DGCgettime(starttime);

  unsigned long long timestampPlayStart = 0ULL;

  while(true)
  {
    int msgtype;
    int msgsize;
    unsigned long long timestamp;
    if(!readHdr(pContext->handle, msgtype, msgsize, timestamp))
    {
      // should delete the pDatabuffer, but whatever
      exit(0);
    }
    if(read(pContext->handle,(char*)pContext->pBuffer, msgsize) != msgsize)
    {
      cerr << "reached end of file" << endl;
      // should delete the pDatabuffer, but whatever
      exit(0);
    }

    if(timestampPlayStart == 0ULL)
    {
      timestampPlayStart = timestamp;
    }

    unsigned long long now;
    DGCgettime(now);

    long long diffsim, diffreal;
    diffsim  = timestamp- timestampPlayStart;
    diffreal = now      - starttime;
    diffreal = llround(pContext->speed * (double)diffreal);

    if(diffsim > diffreal)
      DGCusleep(diffsim - diffreal);

    int numsent = pContext->pSkynet->send_msg(pContext->sockets[msgtype],
                                              pContext->pBuffer,
                                              msgsize, 0);

    (*pContext->pPosition)++;

    if (*pContext->pPosition % 1000 == 0)
    {
      // Get the time of day 
      const time_t tval = timestamp/1000000;
      struct tm * tloc = localtime(&tval);
      char tstr[30];
      strftime(tstr, 30, " (%a %b %d, %H:%M:%S, %Y)", tloc);

      // Print out relevant information
      cout << "\rPosition: " << *pContext->pPosition;
      cout << ", Timestamp: " << timestamp << tstr;
      cout << ", msgtype: " << sn_msg_asString((sn_msg) msgtype) << endl;
      prompt();
    }
                    
    if(numsent != msgsize)
    {
      cerr << "tried sending " << msgsize << " bytes; sent " 
           << numsent << " bytes." << endl;
    }
  }
} // end play()


/******************************************************************************/
/******************************************************************************/
int main(int argc, char *argv[])
{
  char*                pDatabuffer;
  int                  msgtype;
  int                  msgsize;
  unsigned long long   timestamp;
  int bytesSent;

  int sockets[last_type];
  int i;
  int position = 0;

  pDatabuffer = new char[MAX_BUFFER_SIZE];

  // Set input stream/file
  bool bPlayall = false;
  int handle;


  gengetopt_args_info cmdline;
  
  if (cmdline_parser(argc, argv, &cmdline) != 0) {
    exit(1);
  }

  if (cmdline.inputs_num != 1) {
    cmdline_parser_print_help();
    exit(1);
  }
  
  handle = open(cmdline.inputs[0], O_RDONLY | O_LARGEFILE);


  bool justPlayIt = false;
  if (cmdline.just_play_given) {
    justPlayIt = true;
  }

  // Set up communications
  int sn_key;
  cout << "Searching for skynet KEY " << endl;
  char* pSkynetkey = getenv("SKYNET_KEY");
  
  if (cmdline.skynet_key_given) { 
    sn_key = cmdline.skynet_key_arg;
  } else if( pSkynetkey == NULL ) {
    cerr << "SKYNET_KEY environment variable isn't set" << endl;
    exit(1);
  }
  else {
    sn_key = atoi(pSkynetkey);
  }
  
  cout << "Construting skynet with key " << sn_key << endl;  
  skynet skynetobject(SNguilogplayer, sn_key);
  
  for(i=0; i<last_type; i++)
  {
    sockets[i] = skynetobject.get_send_sock((sn_msg)i);
  }

  pthread_t threadid;
  SContext context;

  string line;
  int numToPlayback;

  int lastPlayedBack = 1;
  bool doPlay;
  bool doContinuousPlay = false;
  bool countMessages = false;
  bool firstRun = true;

  // Print the help message (instructions)
  if (!justPlayIt) {
    print_help();
  }
  
  // Play back messages and handle user input
  while(true)
  {
    doPlay = true;

    // get number of messages to play (or ff)
    if(!bPlayall)
    {
      if(!doContinuousPlay && !justPlayIt)
      {
        cout << endl << "Position: " << position << endl;
        prompt();
      }

      if (!justPlayIt) {
        cin.getline(pDatabuffer, MAX_BUFFER_SIZE);
      }

      // Pressing enter will play back the same number as previous
      if(pDatabuffer[0] == '\0' && !justPlayIt)
      {
        numToPlayback = lastPlayedBack;
      }
      // Option to skip a number of messages
      else if(pDatabuffer[0] == '+' && !justPlayIt)
      {
        doPlay = false;
        istringstream linestream(pDatabuffer+1);
        linestream >> numToPlayback;
      }
      // Option to skip to a specific (absolute) message number
      else if(pDatabuffer[0] == '=' && !doContinuousPlay && !justPlayIt)
      {
        doPlay = false;
        int numToGoto;
        istringstream linestream(pDatabuffer+1);
        linestream >> numToGoto;
        if (numToGoto < position)
        {
          cerr << "Error: Can't seek backwards yet (sorry)." << endl;
          numToPlayback = 0;
        }
        else
        {
          numToPlayback = numToGoto - position;
        }
      }
      // Option to play back at some continuous speed
      else if((pDatabuffer[0] == 's' && !doContinuousPlay) || (justPlayIt && firstRun ))
      {
        if (justPlayIt) {
          context.speed   = 1;
          firstRun = false;
        } else {
          istringstream linestream(pDatabuffer+1);
          linestream >> context.speed;
        }
        context.pSkynet   = &skynetobject;
        context.handle    = handle;
        context.sockets   = sockets;
        context.pBuffer   = pDatabuffer;
        context.pPosition = &position;

        pthread_create(&threadid, NULL, &play, (void*)&context);
        cout << "continuous play at speed " << context.speed 
                << ". p to stop." << endl;
        //prompt();
        doContinuousPlay = true;
      }
      // Option to pause
      else if(pDatabuffer[0] == 'p' && !justPlayIt)
      {
        doContinuousPlay = false;
        pthread_cancel(threadid);
        continue;
      }
      // Option to count message types and sizes
      else if(pDatabuffer[0] == 'c' && !justPlayIt)
      {
        countMessages = !countMessages;
        cout << "countMessages is now " << countMessages << endl;
        continue;
      }
      // Option to print help message
      else if(pDatabuffer[0] == 'h' && !justPlayIt)
      {
        print_help();
        cout << "here" << endl;
        continue;
      }
      else if (!justPlayIt)
      {
        istringstream linestream(pDatabuffer);
        linestream >> numToPlayback;
        lastPlayedBack = numToPlayback;
      }

      if(numToPlayback == 0)
      {
        break;
      }
    }
    else // (continuous playback)
    {
      numToPlayback = 1000000000;
    }

    if(doContinuousPlay)
    {
      continue;
    }

    for(i = 0; i<numToPlayback; i++) // i not used
    {
      if(!readHdr(handle, msgtype, msgsize, timestamp))
      {
        printf("[%s:%d] Could not read header. Exiting.\n", __FILE__, __LINE__);
        delete pDatabuffer;
        return -1;
      }
      if(doPlay)
      {
        if(msgsize > MAX_BUFFER_SIZE)
        {
          cerr << "message too large: size = " << msgsize << ". skipping...\n";
          lseek(handle, msgsize, SEEK_CUR);
        }
        else
        {
          if(read(handle, pDatabuffer, msgsize) != msgsize)
          {
            cerr << "Reached end of log file" << endl;
            delete pDatabuffer;
            return 0;
          }
        }

        // Get the time of day 
        const time_t tval = timestamp/1000000;
        struct tm * tloc = localtime(&tval);
        char tstr[30];
        strftime(tstr, 30, " (%a %b %d, %H:%M:%S, %Y)", tloc);

        cout << "Position: " << position+i;
        cout << ", Timestamp: " << timestamp << tstr;
        cout << ", msgtype: " << sn_msg_asString((sn_msg) msgtype) << endl;

        //cerr << "about to send " << msgsize << " bytes" << endl;
        //if(msgtype == SNstate) {
        //cerr << "Northing = " << ((double*)pDatabuffer)[1] << endl;
        //}

        // send the message
        bytesSent = skynetobject.send_msg(sockets[msgtype], pDatabuffer, msgsize, 0);

        if(bytesSent != msgsize)
        {
          cerr << "Tried to send " << msgsize << " but sent " << bytesSent << " bytes." << endl;
          delete pDatabuffer;
          return -1;
        }
      }
      else // (!doPlay)
      {
        // reposition file offset
        lseek(handle, msgsize, SEEK_CUR);
        
        //cout << "Skipped: Timestamp: " << timestamp << ", msgtype: " << 
        //sn_msg_asString(msgtype) << endl;
      }
    } // end for()

    // Get the current file position
    // Note: This does not work past 2^31 bytes for large files
    //off_t curpos = lseek(handle, 0, SEEK_CUR);
    //cout << "Current file position is " << curpos << " bytes (" 
    //     << double(curpos)/pow(2.0, 30) << " GB)." << endl;

    // advance position indicator all at once
    position += numToPlayback;

  } // end while()

  delete pDatabuffer;
}
