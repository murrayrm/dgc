/*!
 * \file skynet.hh
 * \brief Header file for skynet messaging library
 *
 * Richard M. Murray
 * 19 December 2006
 *
 * This file defines all of the functions required to use the skynet library.
 */

#include "sn_msg.hh"			// skynet class definition

/* Utility functions */
extern void skynet_usleep(unsigned long);	// sleep through interrupts
extern int skynet_findkey(int argc, char **argv, int debug = 1);
