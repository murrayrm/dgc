/*-*- indent-tabs-mode: t; tab-width: 4; -*-*/

#ifdef __cplusplus
extern "C" {
#endif
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include "ppport.h"
#ifdef __cplusplus
}
#endif

#include "gcinterfaces/SegGoals.hh"
#include "gcinterfaces/SegGoalsStatus.hh"
#include "interfaces/TpDpInterface.hh"
//#include "bitmap/BitmapParams.hh"
#include "trajutils/TrajTalker.hh"
#include "interfaces/ActuatorState.h"
#include "interfaces/VehicleState.h"
#include "interfaces/StatePrecision.hh"
#include "interfaces/LeadingVehicleInfo.hh"
#undef Pause /* used to be defined to pause => causes conflict with unistd.h:int pause() */
#include "gcinterfaces/AdriveCommand.hh"
#include "skynettalker/SkynetTalker.hh"
#include <boost/serialization/vector.hpp>

template <class Message>
void parseMessage (char *data, int datalen, Message &parsed) 
{
    try {
        skynettalker::SkynetBufferSource src (&data, (int *)&datalen);
        boost::iostreams::stream <skynettalker::SkynetBufferSource> istr (src);
        boost::archive::binary_iarchive ar (istr);
        ar >> parsed;
	} catch (...) {
		fprintf (stderr, "Error unserializing Skynet message!\n");
	}
}

MODULE = Skynet::Parse		PACKAGE = Skynet::Parse

SV *
decode_SegGoals(svdata)
		SV *svdata
	INIT:
		SegGoals parsed;
		HV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "timeSent",               8, newSVnv(parsed.timeSent / 1000000.0), 0);
		hv_store (ret, "goalID",                 6, newSViv(parsed.goalID), 0);
		hv_store (ret, "segmentType",           11, newSViv((int)parsed.segment_type), 0);
		hv_store (ret, "intersectionType",      16, newSViv((int)parsed.intersection_type), 0);
		hv_store (ret, "entry",                  5, newSVpvf("%d.%d.%d", parsed.entrySegmentID, parsed.entryLaneID, parsed.entryWaypointID), 0);
		hv_store (ret, "exit",                   4, newSVpvf("%d.%d.%d", parsed.exitSegmentID, parsed.exitLaneID, parsed.exitWaypointID), 0);
		hv_store (ret, "minSpeedLimit",         13, newSVnv(parsed.minSpeedLimit), 0);
		hv_store (ret, "maxSpeedLimit",         13, newSVnv(parsed.maxSpeedLimit), 0);
		hv_store (ret, "illegalPassingAllowed", 21, (parsed.illegalPassingAllowed? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (ret, "stopAtExit",            10, (parsed.stopAtExit? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (ret, "isExitCheckpoint",      16, (parsed.isExitCheckpoint? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (ret, "perfLevel",              9, newSViv(parsed.perf_level), 0);
		hv_store (ret, "distance",               8, newSVnv(parsed.distance), 0);
		std::stringstream oss;
		parsed.print (oss);
		hv_store (ret, "string",                 6, newSVpv(oss.str().c_str(), oss.str().length()), 0);
		
		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_SegGoalsStatus(svdata)
		SV *svdata
	INIT:
		SegGoalsStatus parsed;
		HV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "timeSent",               8, newSVnv(parsed.timeSent / 1000000.0), 0);
		hv_store (ret, "id",                     2, newSViv(parsed.id), 0);
		hv_store (ret, "status",                 6, newSViv((int)parsed.status), 0);
		hv_store (ret, "goalID",                 6, newSViv(parsed.goalID), 0);
		hv_store (ret, "current",                7, newSVpvf("%d.%d.%d", parsed.currentSegmentID, parsed.currentLaneID, parsed.lastWaypointID), 0);
		hv_store (ret, "failureReason",         13, newSViv((int)parsed.reason), 0);
		std::string str = parsed.toString();
		hv_store (ret, "string",                 6, newSVpv(str.c_str(), str.length()), 0);
		
		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_sendCorr(svdata)
		SV *svdata
	INIT:
		sendCorr parsed;
		AV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (AV *)sv_2mortal((SV *)newAV());
		for (sendCorr::iterator cit = parsed.begin(); cit != parsed.end(); ++cit) {
			AV *poly = (AV *)sv_2mortal((SV *)newAV());
			for (sendPoly::iterator pit = cit->begin(); pit != cit->end(); ++pit) {
				AV *point = (AV *)sv_2mortal((SV *)newAV());
				av_push (point, newSVnv (pit->x));
				av_push (point, newSVnv (pit->y));
				av_push (poly, newRV ((SV *)point));
			}
			av_push (ret, newRV ((SV *)poly));
		}
		
		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_OCPparams(svdata)
		SV *svdata
	INIT:
		OCPparams parsed;
		HV *ret, *params, *cond;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "reverse",     7, (parsed.mode == md_REV? &PL_sv_yes : &PL_sv_no), 0);
		
		params = (HV *)sv_2mortal((SV *)newHV());
		hv_store (params, "L",        1, newSVnv (parsed.parameters[0]), 0);
		hv_store (params, "W",        1, newSVnv (parsed.parameters[1]), 0);
		hv_store (params, "hcg",      3, newSVnv (parsed.parameters[2]), 0);
		hv_store (params, "v_min",    5, newSVnv (parsed.parameters[3]), 0);
		hv_store (params, "v_max",    5, newSVnv (parsed.parameters[4]), 0);
		hv_store (params, "a_min",    5, newSVnv (parsed.parameters[5]), 0);
		hv_store (params, "a_max",    5, newSVnv (parsed.parameters[6]), 0);
		hv_store (params, "phi_min",  7, newSVnv (parsed.parameters[7]), 0);
		hv_store (params, "phi_max",  7, newSVnv (parsed.parameters[8]), 0);
		hv_store (params, "phid_min", 8, newSVnv (parsed.parameters[9]), 0);
		hv_store (params, "phid_max", 8, newSVnv (parsed.parameters[10]), 0);
		hv_store (params, "g",        1, newSVnv (parsed.parameters[11]), 0);

		hv_store (ret, "params",      6, newRV ((SV *)params), 0);

		cond = (HV *)sv_2mortal((SV *)newHV());
		hv_store (cond, "easting",    7, newSVnv (parsed.initialConditionLB[0]), 0);
		hv_store (cond, "northing",   8, newSVnv (parsed.initialConditionLB[1]), 0);
		hv_store (cond, "heading",    7, newSVnv (parsed.initialConditionLB[2]), 0);
		hv_store (cond, "velocity",   8, newSVnv (parsed.initialConditionLB[3]), 0);
		hv_store (cond, "acceleration", 12, newSVnv (parsed.initialConditionLB[4]), 0);
		hv_store (cond, "steeringAngle", 13, newSVnv (parsed.initialConditionLB[5]), 0);

		hv_store (ret, "initialMin", 10, newRV ((SV *)cond), 0);

		cond = (HV *)sv_2mortal((SV *)newHV());
		hv_store (cond, "easting",    7, newSVnv (parsed.initialConditionUB[0]), 0);
		hv_store (cond, "northing",   8, newSVnv (parsed.initialConditionUB[1]), 0);
		hv_store (cond, "heading",    7, newSVnv (parsed.initialConditionUB[2]), 0);
		hv_store (cond, "velocity",   8, newSVnv (parsed.initialConditionUB[3]), 0);
		hv_store (cond, "acceleration", 12, newSVnv (parsed.initialConditionUB[4]), 0);
		hv_store (cond, "steeringAngle", 13, newSVnv (parsed.initialConditionUB[5]), 0);

		hv_store (ret, "initialMax", 10, newRV ((SV *)cond), 0);

		cond = (HV *)sv_2mortal((SV *)newHV());
		hv_store (cond, "easting",    7, newSVnv (parsed.finalConditionLB[0]), 0);
		hv_store (cond, "northing",   8, newSVnv (parsed.finalConditionLB[1]), 0);
		hv_store (cond, "heading",    7, newSVnv (parsed.finalConditionLB[2]), 0);
		hv_store (cond, "velocity",   8, newSVnv (parsed.finalConditionLB[3]), 0);
		hv_store (cond, "acceleration", 12, newSVnv (parsed.finalConditionLB[4]), 0);
		hv_store (cond, "steeringAngle", 13, newSVnv (parsed.finalConditionLB[5]), 0);

		hv_store (ret, "finalMin",    8, newRV ((SV *)cond), 0);

		cond = (HV *)sv_2mortal((SV *)newHV());
		hv_store (cond, "easting",    7, newSVnv (parsed.finalConditionUB[0]), 0);
		hv_store (cond, "northing",   8, newSVnv (parsed.finalConditionUB[1]), 0);
		hv_store (cond, "heading",    7, newSVnv (parsed.finalConditionUB[2]), 0);
		hv_store (cond, "velocity",   8, newSVnv (parsed.finalConditionUB[3]), 0);
		hv_store (cond, "acceleration", 12, newSVnv (parsed.finalConditionUB[4]), 0);
		hv_store (cond, "steeringAngle", 13, newSVnv (parsed.finalConditionUB[5]), 0);

		hv_store (ret, "finalMax",    8, newRV ((SV *)cond), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_VehiclePrecisionPlus(svdata)
		SV *svdata
	INIT:
		VehiclePrecisionPlus parsed;
		HV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "utmNorthConfidence",    18, newSVnv (parsed.utmNorthConfidence), 0);
		hv_store (ret, "utmEastConfidence",     17, newSVnv (parsed.utmEastConfidence), 0);
		hv_store (ret, "utmAltitudeConfidence", 21, newSVnv (parsed.utmAltitudeConfidence), 0);
		hv_store (ret, "rollConfidence",        14, newSVnv (parsed.rollConfidence), 0);
		hv_store (ret, "pitchConfidence",       15, newSVnv (parsed.pitchConfidence), 0);
		hv_store (ret, "yawConfidence",         13, newSVnv (parsed.yawConfidence), 0);
		hv_store (ret, "velNorthRMS",           11, newSVnv (parsed.velNorthRMS), 0);
		hv_store (ret, "velEastRMS",            10, newSVnv (parsed.velEastRMS), 0);
		hv_store (ret, "velDownRMS",            10, newSVnv (parsed.velDownRMS), 0);
		hv_store (ret, "errEllipMajor",         13, newSVnv (parsed.errEllipMajor), 0);
		hv_store (ret, "errEllipMinor",         13, newSVnv (parsed.errEllipMinor), 0);
		hv_store (ret, "errEllipAngle",         13, newSVnv (parsed.errEllipAngle), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_BLAH()
	CODE:
		RETVAL = &PL_sv_undef;
	OUTPUT:
		RETVAL

SV *
decode_CTraj(svdata)
		SV *svdata
	INIT:
		CTraj traj;
		HV *ret;
		AV *diffs, *points, *point;
		char *data;
		STRLEN datalen;
	CODE:

		data = SvPV (svdata, datalen);

		memcpy (traj.getHeader(), data, traj.getHeaderSize());
		data += traj.getHeaderSize();

		// Bit of magic to handle old traj format.
		if (traj.getDirection() < -1 || traj.getDirection() > 1) {
			traj.setDirection (1);
			data -= sizeof(int);
		}

		for (int i = 0; i < traj.getOrder(); i++) {
			memcpy (traj.getNdiffarray(i), data, traj.getNumPoints() * sizeof(double));
			data += traj.getNumPoints() * sizeof(double);

			memcpy (traj.getEdiffarray(i), data, traj.getNumPoints() * sizeof(double));
			data += traj.getNumPoints() * sizeof(double);
		}

		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "npoints",      7, newSViv (traj.getNumPoints()), 0);
		hv_store (ret, "order",        5, newSViv (traj.getOrder()), 0);
		hv_store (ret, "direction",    9, newSViv (traj.getDirection()), 0);

		// Northing:
		diffs = (AV *)sv_2mortal((SV *)newAV());
		for (int d = 0; d < traj.getOrder(); d++) {
			points = (AV *)sv_2mortal((SV *)newAV());
			for (int i = 0; i < traj.getNumPoints(); i++) {
				av_push (points, newSVnv (traj.getNorthingDiff (i, d)));
			}
			av_push (diffs, newRV ((SV *)points));
		}
		hv_store (ret, "N", 1, newRV ((SV *)diffs), 0);
		
		// Easting:
		diffs = (AV *)sv_2mortal((SV *)newAV());
		for (int d = 0; d < traj.getOrder(); d++) {
			points = (AV *)sv_2mortal((SV *)newAV());
			for (int i = 0; i < traj.getNumPoints(); i++) {
				av_push (points, newSVnv (traj.getEastingDiff (i, d)));
			}
			av_push (diffs, newRV ((SV *)points));
		}
		hv_store (ret, "E", 1, newRV ((SV *)diffs), 0);

		// Points:
		points = (AV *)sv_2mortal((SV *)newAV());
		for (int i = 0; i < traj.getNumPoints(); i++) {
			diffs = (AV *)sv_2mortal((SV *)newAV());
			for (int d = 0; d < traj.getOrder(); d++) {
				point = (AV *)sv_2mortal((SV *)newAV());
				av_push (point, newSVnv (traj.getNorthingDiff (i, d)));
				av_push (point, newSVnv (traj.getEastingDiff (i, d)));
				av_push (diffs, newRV ((SV *)point));
			}
			av_push (points, newRV ((SV *)diffs));
		}
		hv_store (ret, "points", 6, newRV ((SV *)points), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_leadVehInfo(svdata)
		SV *svdata
	INIT:
		leadVehInfo parsed;
		HV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "velocity",            8, newSVnv (parsed.velocity), 0);
		hv_store (ret, "distance",            8, newSVnv (parsed.distance), 0);
		hv_store (ret, "separationDistance", 18, newSVnv (parsed.separationDistance), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_AdriveDirective(svdata)
		SV *svdata
	INIT:
		AdriveDirective parsed;
		HV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "timeSent",            8, newSVnv (parsed.timeSent / 1000000.0), 0);
		hv_store (ret, "id",                  2, newSVuv (parsed.id), 0);
		hv_store (ret, "source",              6, newSViv (parsed.source), 0);
		hv_store (ret, "actuator",            8, newSViv ((int)parsed.actuator), 0);
		hv_store (ret, "command",             7, newSViv ((int)parsed.command), 0);
		hv_store (ret, "arg",                 3, newSVnv (parsed.arg), 0);
		std::string str = parsed.toString();
		hv_store (ret, "string",              6, newSVpv (str.c_str(), str.length()), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_AdriveResponse(svdata)
		SV *svdata
	INIT:
		AdriveResponse parsed;
		HV *ret;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		parseMessage (data, datalen, parsed);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "timeSent",            8, newSVnv (parsed.timeSent / 1000000.0), 0);
		hv_store (ret, "id",                  2, newSViv (parsed.id), 0);
		hv_store (ret, "status",              6, newSViv ((int)parsed.status), 0);
		hv_store (ret, "source",              6, newSViv (parsed.source), 0);
		hv_store (ret, "failureReason",      13, newSViv ((int)parsed.reason), 0);
		std::string str = parsed.toString();
		hv_store (ret, "string",              6, newSVpv (str.c_str(), str.length()), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_ActuatorState(svdata)
		SV *svdata
	INIT:
		ActuatorState parsed;
		HV *ret, *sub;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		memcpy (&parsed, data, datalen);
		
		ret = (HV *)sv_2mortal((SV *)newHV());

		sub = (HV *)sv_2mortal((SV *)newHV());
		hv_store (sub, "on",          2, (parsed.m_steerstatus? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "timestamp",   9, newSVnv (parsed.m_steer_update_time / 1000000.0), 0);
		hv_store (sub, "current",     7, newSVnv (parsed.m_steerpos), 0);
		hv_store (sub, "commanded",   9, newSVnv (parsed.m_steercmd), 0);
		hv_store (ret, "steering",    8, newRV ((SV *)sub), 0);

		sub = (HV *)sv_2mortal((SV *)newHV());
		hv_store (sub, "on",          2, (parsed.m_gasstatus? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "timestamp",   9, newSVnv (parsed.m_gas_update_time / 1000000.0), 0);
		hv_store (sub, "current",     7, newSVnv (parsed.m_gaspos), 0);
		hv_store (sub, "commanded",   9, newSVnv (parsed.m_gascmd), 0);
		hv_store (ret, "throttle",    8, newRV ((SV *)sub), 0);
		
		sub = (HV *)sv_2mortal((SV *)newHV());
		hv_store (sub, "on",          2, (parsed.m_transstatus? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "timestamp",   9, newSVnv (parsed.m_trans_update_time / 1000000.0), 0);
		hv_store (sub, "current",     7, newSVnv (parsed.m_transpos), 0);
		hv_store (sub, "commanded",   9, newSVnv (parsed.m_transcmd), 0);
		hv_store (ret, "transmission", 12, newRV ((SV *)sub), 0);
		
		sub = (HV *)sv_2mortal((SV *)newHV());
		hv_store (sub, "on",          2, (parsed.m_brakestatus? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "timestamp",   9, newSVnv (parsed.m_brake_update_time / 1000000.0), 0);
		hv_store (sub, "current",     7, newSVnv (parsed.m_brakepos), 0);
		hv_store (sub, "commanded",   9, newSVnv (parsed.m_brakecmd), 0);
		hv_store (sub, "pressure",    8, newSVnv (parsed.m_brakepressure), 0);
		hv_store (ret, "brake",    5, newRV ((SV *)sub), 0);

		sub = (HV *)sv_2mortal((SV *)newHV());
		hv_store (sub, "on",          2, (parsed.m_estopstatus? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "timestamp",   9, newSVnv (parsed.m_estop_update_time / 1000000.0), 0);
		hv_store (sub, "status",      6, newSViv (parsed.m_estoppos), 0);
		hv_store (sub, "fromDARPA",   9, (parsed.m_dstoppos? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "fromAdrive", 10, (parsed.m_astoppos? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "fromControlled", 14, (parsed.m_cstoppos? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (ret, "estop",       5, newRV ((SV *)sub), 0);

		hv_store (ret, "aboutToUnpause", 14, (parsed.m_about_to_unpause? &PL_sv_yes : &PL_sv_no), 0);
		
		sub = (HV *)sv_2mortal((SV *)newHV());
		hv_store (sub, "on",          2, (parsed.m_obdiistatus? &PL_sv_yes : &PL_sv_no), 0);
		hv_store (sub, "timestamp",   9, newSVnv (parsed.m_obdii_update_time / 1000000.0), 0);
		hv_store (sub, "engineRPM",   9, newSVnv (parsed.m_engineRPM), 0);
		hv_store (sub, "timeSinceEngineStart", 20, newSViv (parsed.m_TimeSinceEngineStart), 0);
		hv_store (sub, "vehicleWheelSpeed",    17, newSVnv (parsed.m_VehicleWheelSpeed), 0);
		hv_store (sub, "engineCoolantTemp",    17, newSVnv (parsed.m_EngineCoolantTemp), 0);
		hv_store (sub, "wheelForce",           10, newSVnv (parsed.m_WheelForce), 0);
		hv_store (sub, "glowPlugLampTime",     16, newSViv (parsed.m_GlowPlugLampTime), 0);
		hv_store (sub, "throttlePosition",     16, newSVnv (parsed.m_ThrottlePosition), 0);
		hv_store (sub, "currentGearRatio",     16, newSVnv (parsed.m_CurrentGearRatio), 0);
		hv_store (ret, "obdii",       5, newRV ((SV *)sub), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

SV *
decode_VehicleState(svdata)
		SV *svdata
	INIT:
		VehicleState parsed;
		HV *ret, *type, *coord;
		char *data;
		STRLEN datalen;
	CODE:
		data = SvPV (svdata, datalen);
		memcpy (&parsed, data, datalen);
		
		ret = (HV *)sv_2mortal((SV *)newHV());
		hv_store (ret, "timestamp",   9, newSVnv (parsed.timestamp / 1000000.0), 0);

		type = (HV *)sv_2mortal((SV *)newHV());

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.utmNorthing), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.utmNorthVel), 0);
		hv_store (coord, "confidence", 10, newSVnv (parsed.utmNorthConfidence), 0);
		hv_store (type, "northing", 8, newRV ((SV *)coord), 0);
		hv_store (type, "y", 1, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.utmEasting), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.utmEastVel), 0);
		hv_store (coord, "confidence", 10, newSVnv (parsed.utmEastConfidence), 0);
		hv_store (type, "easting", 7, newRV ((SV *)coord), 0);
		hv_store (type, "x", 1, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.utmAltitude), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.utmAltitudeVel), 0);
		hv_store (coord, "confidence", 10, newSVnv (parsed.utmAltitudeConfidence), 0);
		hv_store (type, "altitude", 8, newRV ((SV *)coord), 0);
		hv_store (type, "z", 1, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.utmRoll), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.utmRollRate), 0);
		hv_store (coord, "confidence", 10, newSVnv (parsed.rollConfidence), 0);
		hv_store (type, "roll", 4, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.utmPitch), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.utmPitchRate), 0);
		hv_store (coord, "confidence", 10, newSVnv (parsed.pitchConfidence), 0);
		hv_store (type, "pitch", 5, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.utmYaw), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.utmYawRate), 0);
		hv_store (coord, "confidence", 10, newSVnv (parsed.yawConfidence), 0);
		hv_store (type, "yaw", 3, newRV ((SV *)coord), 0);

		hv_store (type, "zone", 4, newSViv (parsed.utmZone), 0);
		hv_store (type, "letter", 6, newSVpvf ("%c", parsed.utmLetter), 0);

		hv_store (ret, "utm", 3, newRV ((SV *)type), 0);

		type = (HV *)sv_2mortal((SV *)newHV());

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.localX), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.localXVel), 0);
		hv_store (type, "x", 1, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.localY), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.localYVel), 0);
		hv_store (type, "y", 1, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.localZ), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.localZVel), 0);
		hv_store (type, "z", 1, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.localRoll), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.localRollRate), 0);
		hv_store (type, "roll", 4, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.localPitch), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.localPitchRate), 0);
		hv_store (type, "pitch", 5, newRV ((SV *)coord), 0);

		coord = (HV *)sv_2mortal((SV *)newHV());
		hv_store (coord, "pos", 3, newSVnv (parsed.localYaw), 0);
		hv_store (coord, "vel", 3, newSVnv (parsed.localYawRate), 0);
		hv_store (type, "yaw", 3, newRV ((SV *)coord), 0);

		hv_store (ret, "local", 5, newRV ((SV *)type), 0);

		type = (HV *)sv_2mortal((SV *)newHV());
		hv_store (type, "x", 1, newSVnv (parsed.vehXVel), 0);
		hv_store (type, "y", 1, newSVnv (parsed.vehYVel), 0);
		hv_store (type, "z", 1, newSVnv (parsed.vehZVel), 0);
		hv_store (type, "roll", 4, newSVnv (parsed.vehRollRate), 0);
		hv_store (type, "pitch", 5, newSVnv (parsed.vehPitchRate), 0);
		hv_store (type, "yaw", 3, newSVnv (parsed.vehYawRate), 0);
		hv_store (type, "speed", 5, newSVnv (parsed.vehSpeed), 0);

		hv_store (ret, "vehvel", 6, newRV ((SV *)type), 0);
		hv_store (ret, "speed", 5, newSVnv (parsed.vehSpeed), 0);

		RETVAL = newRV((SV *)ret);
	OUTPUT:
		RETVAL

