/*!
 * \file skynet-logger.cc
 * \brief Log skynet messages
 *
 * \author Richard M. Murray
 * \date 20 May 2007
 *
 */

#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <iostream>
#include <sp.h>
#ifdef MACOSX
#define O_LARGEFILE 0x00		// not used for OSX
#endif

#include "skynet/skynet.hh"		// skynet_findkey()
#include "interfaces/sn_types.h"	// skynet message types
#include "cmdline.h"

int verbose = 0;			// print verbose error messages

int main(int argc, char **argv)
{
  /* Process command line arguments */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  verbose = cmdline.verbose_arg;

  if (cmdline.inputs_num != 1) {
    cmdline_parser_print_help();
    exit(1);
  }

  /* Get the location of the spread server */
  const char *spread_daemon;
  char *pDaemonEnv = getenv("SPREAD_DAEMON");
  if(pDaemonEnv == NULL) {
    if (verbose)
      cerr << "Using default SPREAD_DAEMON: 4803@192.168.0.59" << endl;
    spread_daemon ="4803@192.168.0.59";
  } else {
    spread_daemon = pDaemonEnv;
  }

  /* Now connect to the spread server */
  int snkey = skynet_findkey(argc, argv);
  int module_id = MODtimber;
  char private_name[MAX_GROUP_NAME];
  char private_group[MAX_GROUP_NAME];
  mailbox mbox;
  int status;

  snprintf(private_name, sizeof(private_name), "m:%X:%X", snkey, module_id);
  status = SP_connect(spread_daemon, private_name, 0, 0, &mbox, private_group);
  if (verbose) {
    cerr << "Connected to " << spread_daemon << " as " << private_name 
	 << " with group " << private_group << "; status = " << status << endl;
  }

  /* Join all of the groups */
  char group[MAX_GROUP_NAME];
  for (int i = 0; i < last_type; i++) {
    snprintf(group, sizeof(group),
             "sn:%05d:%05d:%05d", snkey, i, 0);
    status = SP_join(mbox, group);
    if (status != 0 && verbose) cerr << "Error joining group " << i << endl;
  }
  if (verbose) cerr << "Joined " << last_type << " groups" << endl;

  /* Open up log file */
  char *logname = cmdline.inputs[0];
  int handle = open(logname, O_WRONLY | O_CREAT | O_LARGEFILE | O_TRUNC);
  if (handle < 0) {
    cerr << "Couldn't open " << logname << "..... exiting" << endl;
    exit(-1);
  }

  /* Set the permissions properly */
  chmod(logname, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
  if (verbose >= 2) cerr << "Opened log file " << logname << endl;

  /* Read all messages */
  while (1) {
    int num_groups, msgsize, endian, status;
    int16_t mess_type, dummy;
    service service_type;
    char sender[MAX_GROUP_NAME], group[MAX_GROUP_NAME];
#   define MAX_BUFFER_SIZE 1000000
    char packet[MAX_BUFFER_SIZE];

    service_type = dummy = endian = 0;  
    msgsize = SP_receive(mbox, &service_type, sender, 1, &num_groups, &group,
                          &mess_type, &endian, sizeof(packet), packet);

    /* Mark the time that we received the message */
    timeval tv; gettimeofday(&tv, NULL);
    unsigned long long stamp =
      (unsigned long long)tv.tv_usec + 1000000ULL * tv.tv_sec;

    /* Extract the message type from the mailbox name */
    int msgtype;
    if ((status = sscanf(group, "sn:%*d:%d:%*d", &msgtype)) != 1) {
      /* Not the right type of message */
      if (verbose >= 4)
	cerr << "wrong message type (" << status << "): " << group << endl;
      continue;
    }

    /* Print out information on the message we received */
    if (verbose >= 3)
      cerr << sn_msg_asString((sn_msg) msgtype) << ": " 
	   << msgsize << " bytes" << endl;

    /* Write the information to file */
    write(handle, (char *) &stamp, sizeof(stamp));
    write(handle, (char *) &msgtype, sizeof(msgtype));
    write(handle, (char *) &msgsize, sizeof(msgsize));
    write(handle, (char *) packet, msgsize);
  }

  return 0;
}
