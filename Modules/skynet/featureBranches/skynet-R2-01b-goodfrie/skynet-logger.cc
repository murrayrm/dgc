/*!
 * \file skynet-logger.cc
 * \brief log skynet messages for later playback
 *
 * \author Dima Kogan
 * \date 2004
 *
 */

#include <iostream>
using namespace std;

#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "sn_msg.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

#define MAX_BUFFER_SIZE 1000000
#define MAX_BUFFER_SIZE 1000000
#ifdef MACOSX
#define O_LARGEFILE 0x00	// not defined (needed?) in OS X
#endif

#define SELECTED_ONLY 1		// only display selected messages

struct SContext {
  skynet         *pSkynet;
  int             msgtype;
  int             handle;
  pthread_mutex_t *pMutex;
  int             socket;
};

/*
 * List of messages to log
 *
 * If you need to log more msgs add a name to this list BEFORE
 * "selectedLastMsg" then change the SWITCH - CASE command in the
 * bottom of the main function
 */
enum {
    scmap, params, obstacles, rddf, state, actuatorState, adriveCmd, 
    adriveCom, RDDFtraj, pseudocon, trajFollCap, selectedLastMsg
};

void *listenToType(void *pArg)
{
  int             old;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &old);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &old);

  SContext       *pContext = (SContext *) pArg;
  char           *pBuffer = new char[MAX_BUFFER_SIZE];

  pContext->socket = pContext->pSkynet->listen(pContext->msgtype, ALLMODULES);

  while (true) {
    int msgsize = pContext->pSkynet->get_msg(pContext->socket, pBuffer,
					     MAX_BUFFER_SIZE, 0);
    unsigned long long stamp;
    DGCgettime(stamp);

    DGClockMutex(pContext->pMutex);
    write(pContext->handle, (char *) &stamp, sizeof(stamp));
    write(pContext->handle, (char *) &pContext->msgtype,
	  sizeof(pContext->msgtype));
    write(pContext->handle, (char *) &msgsize, sizeof(msgsize));
    write(pContext->handle, (char *) pBuffer, msgsize);
    DGCunlockMutex(pContext->pMutex);
  }
  delete          pBuffer;
}


int main(int argc, char *argv[])
{
  int             i;
  int             lastMsg;
  bool            bStarted = false;

  if (SELECTED_ONLY) {
    lastMsg = selectedLastMsg;
  } else {
    lastMsg = last_type;
  }

  int handle;
  if (argc == 2) {
    if (strcmp(argv[1], "-") == 0)
      handle = STDOUT_FILENO;
    else {
      /* Open the file where we will log information */
      handle = open(argv[1], O_WRONLY | O_CREAT | O_LARGEFILE | O_TRUNC);
      if (handle < 0) {
	cerr << "Couldn't open " << argv[1] << "..... exiting" << endl;
	exit(-1);
      }

      /* Set the permissions properly */
      chmod(argv[1], S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
    }
  } else if (argc == 1) {
    char            buffer[50];
    char            tmpbuf[10];

    time_t          timestamp = time(NULL);
    struct tm      *tmstruct = localtime(&timestamp);

    strcpy(buffer, "/tmp/logs/tome.");
    sprintf(tmpbuf, "%04d", tmstruct->tm_year + 1900);
    strcat(buffer, tmpbuf);
    strcat(buffer, "_");

    sprintf(tmpbuf, "%02d", tmstruct->tm_mon + 1);
    strcat(buffer, tmpbuf);
    strcat(buffer, "_");

    sprintf(tmpbuf, "%02d", tmstruct->tm_mday);
    strcat(buffer, tmpbuf);
    strcat(buffer, ".");

    sprintf(tmpbuf, "%02d", tmstruct->tm_hour);
    strcat(buffer, tmpbuf);
    strcat(buffer, "_");

    sprintf(tmpbuf, "%02d", tmstruct->tm_min);
    strcat(buffer, tmpbuf);
    strcat(buffer, "_");

    sprintf(tmpbuf, "%02d", tmstruct->tm_sec);
    strcat(buffer, tmpbuf);

    cerr << "Starting writing " << buffer << endl;
    handle = open(buffer, O_WRONLY | O_CREAT | O_LARGEFILE | O_TRUNC);
    if (handle < 0) {
      cerr << "Couldn't open " << buffer << "..... Doing nothing" << endl;
      return 0;
    }

      /* Set the permissions properly */
      chmod(argv[1], S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);

  } else {  
    cerr << "usage: ./skynet-logger [file]" << endl;
    cerr << "logs to file if specified; or to /tmp/logs/tome.date.time" << endl;
    return 0;
  }

  int             sn_key;
  char *pSkynetkey = getenv("SKYNET_KEY");
  sn_key = pSkynetkey == NULL ? 0 : atoi(pSkynetkey);
  cerr << "skynet-logger: using snkey " << sn_key << endl;

  skynet          skynetobject(SNguilogwriter, sn_key);
  pthread_mutex_t filemutex;
  DGCcreateMutex(&filemutex);
  SContext        contexts[lastMsg];
  pthread_t       thread_id[lastMsg];

  while (true) {
    cerr << endl;
    bStarted = true;

    //listen to every message except logging control messages
    for (i = 0; i < lastMsg; i++) {
      if (i != SNguiToTimberMsg) {


	contexts[i].pSkynet = &skynetobject;
	cout << "Starting Thread: " << i;
	if (SELECTED_ONLY)
	  switch (i) {
	  case scmap:
	    contexts[i].msgtype = SNtplannerStaticCostMap;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case params:
	    contexts[i].msgtype = SNocpParams;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case obstacles:
	    contexts[i].msgtype = SNocpObstacles;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case rddf:
	    contexts[i].msgtype = SNrddf;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case state:
	    contexts[i].msgtype = SNstate;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case actuatorState:
	    contexts[i].msgtype = SNactuatorstate;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case adriveCmd:
	    contexts[i].msgtype = SNdrivecmd;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case adriveCom:
	    contexts[i].msgtype = SNdrivecom;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case RDDFtraj:
	    contexts[i].msgtype = SNRDDFtraj;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case pseudocon:
	    contexts[i].msgtype = SNsuperconTrajfCmd;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;
	  case trajFollCap:
	    contexts[i].msgtype = SNtrajFspeedCapCmd;
	    cout << " skynet group: " << contexts[i].msgtype << endl;
	    break;

	    //WHEN YOU ADD MORE MSGS TO SELECTED ONLY CODE THEM HERE
	  default:
	    cerr << "When listening for SELECTED_ONLY you should not arrive here..." << endl;
	    exit(1);
	    break;
	  }
	else {
	  contexts[i].msgtype = (sn_msg) i;
	}
	contexts[i].handle = handle;
	contexts[i].pMutex = &filemutex;
	pthread_create(&thread_id[i], NULL, &listenToType, (void *) &contexts[i]);
      }
    }
    pthread_join(thread_id[0], NULL);
  }

  DGCdeleteMutex(&filemutex);
}
