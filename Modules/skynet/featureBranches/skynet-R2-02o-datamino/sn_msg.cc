/*!
 * \file sn_msg.cc
 * \brief Skynet message handling code
 *
 * Richard M. Murray
 * 19 December 2006 (based on previous code)
 *
 * This file contains the main code for skynet messaging.  Skynet is a
 * thin layer on top of spread, so most of what we do here is set up
 * the message types and then use spread for delivery.
 *
 */

#include "skynet.hh"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
using namespace std;

#include <assert.h>

#define MAX_NUM_MAILBOXES 150
#define I_AM_HERE_PERIOD  1000000

// Comment these out for now to get rid of the sn_types dependency.
#ifdef UNUSED
/* Static function for broadcasting thread ID */
static void *SN_bcastID(void *);
#endif

/*!
 * Function: skynet::skynet
 * \brief Skynet class constructor
 *
 * The class constructor connects to the skynet daemon and initializes
 * a thread for broadcasting module identification information.
 *
 */
skynet::skynet (int snname, int snkey, int* pStatus)
{
  m_key          = snkey;
  m_name         = snname;
  m_pStatus      = pStatus;

  m_pMailboxes   = new mailbox[MAX_NUM_MAILBOXES];
  m_pGroupnames  = new string[MAX_NUM_MAILBOXES];
  m_numMailboxes = 0;
  m_highestOpenMailboxIndex = 0;

  m_sendMailbox = spreadConnect();

  if (pthread_mutex_init(&m_mailboxMutex, NULL) != 0)
    cerr << "skynet: couldn't create_mutex" << endl;

  // Comment these out for now to get rid of the sn_types dependency.
#ifdef UNUSED
  // start the id-broadsting threads
  if (pthread_create(&m_idthread, NULL, SN_bcastID, this) != 0)
    cerr << "skynet: coudn't start ID broadcast thread" << endl;
#endif
}

skynet::~skynet()
{
  /* Leave all groups that we are part of */
  for(int i=0; i<m_numMailboxes; i++) {
    SP_leave(m_pMailboxes[i], m_pGroupnames[i].c_str());
    SP_disconnect(m_pMailboxes[i]);
  }

  /* Disconnect from the spread daemon */
  SP_disconnect(m_sendMailbox);

  /* Delete all storage associated with pthreads */
  delete[] m_pMailboxes;
  delete[] m_pGroupnames;
  if(pthread_mutex_destroy(&m_mailboxMutex) != 0)
    cerr << "skynet: error deleting mutex" << endl;
}

/*!
 * Function: skynet::spreadConnect
 * \brief Connect to spread daemon
 *
 * This function opens a spread mailbox and returns the index of the
 * mailbox. It cycles through various private connection names until
 * spread accepts one
 */
mailbox skynet::spreadConnect(void)
{
  int res;
  ostringstream privateNameStream;
  char privateGroup[MAX_GROUP_NAME];

  mailbox mbox;

  /* Connect to the spread daemon */
  const char *pDaemon;
  char *pDaemonEnv = getenv("SPREAD_DAEMON");
  if(pDaemonEnv == NULL) {
      cerr << "Using default SPREAD_DAEMON: 4803@192.168.0.59" << endl;
      pDaemon ="4803@192.168.0.59";
  } else
    pDaemon = pDaemonEnv;

  do {
      privateNameStream.str("");
      privateNameStream << (++m_highestOpenMailboxIndex);
      res = SP_connect(pDaemon, privateNameStream.str().c_str(),
		       0, 0, &mbox, privateGroup);
  } while(res == REJECT_NOT_UNIQUE);

  if (res != ACCEPT_SESSION) {
    cerr << "skynet::spreadConnect: couldn't SP_connect(): " << endl;
    SP_error(res);
    cerr << "  SPREAD_DAEMON=" << pDaemon
	 << ", private= " << privateNameStream.str() << endl;
    // Let's get a core file when this happens.
    assert(false);
    //exit(-1);
  }

  return mbox;
}

/**
 * Function: skynet::listen
 * \brief Listen for skynet messages
 *
 * The listen function takes as an argument the type of messages you
 * want to listen to and the name of the module that is sending them.
 * You can't listen to all messages of a all types.  Return value is
 * the mailbox id to use.  return value of -1 indicates error.  This
 * returns an index into the m_pMailboxes array.  The optional channel
 * value defaults to zero and allows for messages of the same type to
 * be sent to isolated groups.
*/
int skynet::listen(int type, int somemodule, int channel)
{
  if(m_numMailboxes == MAX_NUM_MAILBOXES) {
    cerr << "skynet::listen(): too many mailboxes" << endl;
    return -1;
  }

  pthread_mutex_lock(&m_mailboxMutex);

  m_pMailboxes[m_numMailboxes] = spreadConnect();

  string groupName;
  makeGroupName(&groupName, type, channel);

  int res = SP_join(m_pMailboxes[m_numMailboxes], groupName.c_str());
  if(res == 0) {
    m_pGroupnames[m_numMailboxes] = groupName;
    res = m_numMailboxes++;
    pthread_mutex_unlock(&m_mailboxMutex);
    return res;
  }

  cerr << "spread: SP_join failed" << endl;
  SP_error(res);
  pthread_mutex_unlock(&m_mailboxMutex);
  return -1;
}
/**
 * Function: skynet::listen_channel
 * \brief Listen for skynet messages on a specified channel
 *
 * Same as the listen function but can specify a specific mailbox to
  listen to.  The listen function defaults to channel 0.  A return value
  of -1 indicates error.  This returns an index into the
  m_pMailboxes array
*/
int skynet::listen_channel(int type,int channel)
{
  if(m_numMailboxes == MAX_NUM_MAILBOXES) {
    cerr << "skynet::listen(): too many mailboxes" << endl;
    return -1;
  }

  pthread_mutex_lock(&m_mailboxMutex);

  m_pMailboxes[m_numMailboxes] = spreadConnect();

  string groupName;
  makeGroupName(&groupName, type, channel);

  int res = SP_join(m_pMailboxes[m_numMailboxes], groupName.c_str());
  if(res == 0) {
    m_pGroupnames[m_numMailboxes] = groupName;
    res = m_numMailboxes++;
    pthread_mutex_unlock(&m_mailboxMutex);
    return res;
  }

  cerr << "spread: SP_join failed" << endl;
  SP_error(res);
  pthread_mutex_unlock(&m_mailboxMutex);
  return -1;
}
/*!
 * Function: skynet::get_send_sock
 * \brief Get socket for transmitting data
 *
 * The get_send_sock function gets a socket to send stuff to.  It
 * returns the type. The assumption is that the same skynet object
 * will be used to both get the send socket and to send the messages.
 */
int skynet::get_send_sock(int type)
{
  return type;
}

/*!
 * Function: skynet::sn_select
 * \brief Wait for a message from a reciever
 *
 * The sn_select function waits for a message from a given mailbox and
 * then return.
 */
void skynet::sn_select(int mboxidx)
{
  fd_set readfds;
  int tries = 0;
  FD_ZERO(&readfds);
  FD_SET(m_pMailboxes[mboxidx], &readfds);

  while (select(m_pMailboxes[mboxidx] + 1, &readfds, NULL, NULL, NULL) < 0 && tries < 20) {
    perror("skynet::sn_select(): select failed");
    if (errno == EBADF) {
      // Try to reconnect the bad mailbox.
      m_pMailboxes[mboxidx] = spreadConnect();
      if (SP_join(m_pMailboxes[mboxidx], m_pGroupnames[mboxidx].c_str()) != 0)
        close (m_pMailboxes[mboxidx]); // trigger another EBADF -> another connect
    } else if (errno != EINTR) {
      // ENOMEM or EINVAL - both unrecoverable.
      perror ("unrecoverable error in skynet::sn_select()");
      exit (-1);
    }
    // Yes, we hate sleeping, but if the spread server's hiccuped then we'd better sleep.
    // This will do one retry immediately, another after a second, another two seconds
    // after that, etc. It'll go a total of three and a half minutes before it gives up
    // the ghost. If more is needed, change the 20 in "tries < 20" above.
    sleep (tries);
    tries++;
  }

  if (tries >= 20) {
    // It's hopeless. Goodbye.
    perror ("skynet::sn_select() failed repeatedly");
    exit (-1);
  }
}

/*! check to see if more messages are available */
bool skynet::is_msg(int mboxidx)
{
  return SP_poll(m_pMailboxes[mboxidx]) > 0;
}

/* Returns size of the next message available in the mailbox, if any, or
 * 0 of no message is currently in the mailbox, and <0 if an error occurs.
 */
int skynet::get_msg_size(int mboxidx, int16 * pMessType)
{
  service messageService = 0;
  char    sender[MAX_GROUP_NAME];
  char    group[1][MAX_GROUP_NAME];
  int     numGroups;
  int16   messType;
  int     requiredSize; // "endianMismatch" if you read SP_receive() doc
  int     ret;
  char    dummyBuf; // just to avoid passing a NULL buffer

  if (pMessType == NULL) {
      pMessType = &messType;
  }

  // Try to read a message with a buffer of size zero. This will (almost) always
  // return a BUFFER_TOO_SHORT error and the required size in the "endianMismatch"
  // parameter (ask the spread developers about this weird choice ...)
  // "Almost" always because there may be a message of size zero ...
  ret = SP_receive(m_pMailboxes[mboxidx], &messageService,
                   sender, 1, &numGroups, group, pMessType,
                   &requiredSize, 0, &dummyBuf);

  if(ret < 0) {
      if (ret == BUFFER_TOO_SHORT) {
          // this is what we want to happen
          ret = -requiredSize;
      } else {
          // a real error
          cerr << "skynet::get_msg(): spread error" << endl;
          SP_error(ret);
          if (ret == GROUPS_TOO_SHORT) {
              cerr << "required " << -numGroups << " groups!!!" << endl;
          }
      }
  } else {
      // there was a message of size 0 in the queue, and we just removed it ...
      // not a problem, whoever needs a zero-size message? ;-)
      // seriously, we should NOT send zero-size messages around, or bad things will happen!
      assert(ret == 0); // just check this is actually the case
  }

  return ret;
}


/*!
 * Function: skynet:sn_get_msg
 * \brief Get a message from a mailbox
 *
 * The sn_get_msg function gets a message.  mboxidx is what you got
 * from sn_listen.  mybuf is a buffer that the user must allocate.
 * bufsize is the size in bytes of that buffer.  options
 * ignored. pMutex, if specified, will lock before the data is written
 * and unlock immediately after. If bReleaseMutexWhenDone is false,
 * the mutex will not be unlocked. Return value is #of bytes read
*/
size_t skynet::get_msg (int mboxidx, void *mybuf, size_t bufsize, int options)
{
  return get_msg(mboxidx, mybuf, bufsize, options, NULL, false, 0);
}

size_t skynet::get_msg (int mboxidx, void *mybuf, size_t bufsize,
			int options, pthread_mutex_t** ppMutex,
			bool bReleaseMutexWhenDone, int numMutices)
{
  service messageService = DROP_RECV;
  char    sender[MAX_GROUP_NAME];
  int     numGroups;
  int16   messtype;
  int     endianMismatch;
  int     numreceived;

  if(numMutices != 0) {
    sn_select(mboxidx);			// wait for message before locking
    for (int i=0; i<numMutices; i++)
      if (pthread_mutex_lock(ppMutex[i]) != 0)
	cerr << "skynet::sn_msg: failed to lock mutex" << endl;
  }

  numreceived = SP_receive(m_pMailboxes[mboxidx], &messageService,
			   sender, 0, &numGroups, NULL, &messtype,
			   &endianMismatch, bufsize, (char*)mybuf);

  if(bReleaseMutexWhenDone)
    for(int i=0; i<numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  if(numreceived < 0) {
    cerr << "skynet::get_msg(): spread error:" << endl;
    SP_error(numreceived);

    /* Exit on fatal errors so that the system restarts itself */
    switch (numreceived) {
    case BUFFER_TOO_SHORT:
    case GROUPS_TOO_SHORT:
    case NET_ERROR_ON_SESSION:
      /* Ignore these errors and try again */
      break;

    case CONNECTION_CLOSED:
      /* Spread is hosed; exit and hope for a reset */
      exit(-1);

    case ILLEGAL_SPREAD:
    case COULD_NOT_CONNECT:
    case REJECT_QUOTA:
    case REJECT_NO_NAME:
    case REJECT_ILLEGAL_NAME:
    case REJECT_NOT_UNIQUE:
    case REJECT_VERSION:
    case REJECT_AUTH:
    case ILLEGAL_SESSION:
    case ILLEGAL_SERVICE:
    case ILLEGAL_MESSAGE:
    case ILLEGAL_GROUP:
    case MESSAGE_TOO_LONG:
      /* These messages shouldn't happen */
      abort();
    }
  }

  return numreceived;
}

/*!
 * Function: skynet::send_msg
 * \brief Send a skynet message
 *
 * sn_send_msg sends a message.  You cannot choose to whom to send a
 * message, you may only send it.  return value is the number of bytes
 * sent. options are ignored. pMutex, if specified, will lock before
 * the data is sent and unlock immediately after. If
 * bReleaseMutexWhenDone is false, the mutex will not be unlocked.
*/
size_t skynet::send_msg (int type, void *msg, size_t msgsize, int options)
{
  return send_msg (type, msg, msgsize, options, NULL, false, 0);
}

/*!
 * Function: skynet::send_msg_channel
 * \brief Send a skynet message across a specified channel
 *
 * sn_send_msg sends a message to the specified channel.  return value
 * is the number of bytes sent. options are ignored. pMutex, if
 * specified, will lock before the data is sent and unlock immediately
 * after. If bReleaseMutexWhenDone is false, the mutex will not be
 * unlocked.
 */
size_t skynet::send_msg_channel (int type, int channel, void *msg, size_t msgsize, int options)
{
  return send_msg_channel (type, channel, msg, msgsize, options, NULL, false, 0);
}

size_t skynet::send_msg (int type, void *msg, size_t msgsize, int options,
			 pthread_mutex_t** ppMutex,
			 bool bReleaseMutexWhenDone, int numMutices)
{
  for (int i=0; i<numMutices; i++)
    if (pthread_mutex_lock(ppMutex[i]) != 0)
      cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  int retval;
  string groupName;
  makeGroupName(&groupName, type);

  int numsent = SP_multicast(m_sendMailbox, FIFO_MESS | SELF_DISCARD,
			     groupName.c_str(), (int16) 0, msgsize,
			     (const char*)msg);
  if (numsent == (int)msgsize)
    retval = msgsize;
  else {
    cerr << "skynet::send_msg failed: "<< endl;
    SP_error(numsent);
    retval = 0;
    if(numsent == CONNECTION_CLOSED) {
      cerr << "lost spread connection, trying to reconnect..." << endl;
      m_sendMailbox = spreadConnect();
    }
  }

  if(bReleaseMutexWhenDone)
    for(int i=0; i<numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  return retval;
}


size_t skynet::send_msg_channel (int type, int channel, void *msg, size_t msgsize, int options,
			 pthread_mutex_t** ppMutex,
			 bool bReleaseMutexWhenDone, int numMutices)
{
  for (int i=0; i<numMutices; i++)
    if (pthread_mutex_lock(ppMutex[i]) != 0)
      cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  int retval;
  string groupName;
  makeGroupName(&groupName, type, channel);

  int numsent = SP_multicast(m_sendMailbox, FIFO_MESS | SELF_DISCARD,
			     groupName.c_str(), (int16) 0, msgsize,
			     (const char*)msg);
  if (numsent == (int)msgsize)
    retval = msgsize;
  else {
    cerr << "skynet::send_msg failed: "<< endl;
    SP_error(numsent);
    retval = 0;
    if(numsent == CONNECTION_CLOSED) {
      cerr << "lost spread connection, trying to reconnect..." << endl;
      m_sendMailbox = spreadConnect();
    }
  }

  if(bReleaseMutexWhenDone)
    for(int i=0; i<numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  return retval;
}

size_t skynet::send_msg (int type, const scatter* msgs, int options)
{
  return send_msg (type, msgs, options, NULL, false, 0);
}
size_t skynet::send_msg_channel (int type, int channel,const scatter* msgs, int options)
{
  return send_msg_channel (type, channel, msgs, options, NULL, false, 0);
}

size_t skynet::send_msg (int type, const scatter* msgs, int options,
			 pthread_mutex_t** ppMutex,
			 bool bReleaseMutexWhenDone, int numMutices)
{
  for(int i=0; i<numMutices; i++)
    if (pthread_mutex_lock(ppMutex[i]) != 0)
      cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  int retval;
  string groupName;
  makeGroupName(&groupName, type);

  int numsent = SP_scat_multicast(m_sendMailbox,
				  FIFO_MESS | SELF_DISCARD,
				  groupName.c_str(), 0, msgs);
  int msgsize = 0;
  for(int i=0; i<msgs->num_elements; i++) {
    msgsize+=msgs->elements[i].len;
  }

  if(numsent == msgsize)
    retval = msgsize;
  else {
    cerr << "skynet::send_msg failed: "<< endl;
    SP_error(numsent);
    retval = 0;
    if(numsent == CONNECTION_CLOSED) {
      cerr << "lost spread connection, trying to reconnect..." << endl;
      m_sendMailbox = spreadConnect();
    }
  }

  if (bReleaseMutexWhenDone)
    for (int i=0; i<numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  return retval;
}
size_t skynet::send_msg_channel (int type, int channel, const scatter* msgs, int options,
			 pthread_mutex_t** ppMutex,
			 bool bReleaseMutexWhenDone, int numMutices)
{
  for(int i=0; i<numMutices; i++)
    if (pthread_mutex_lock(ppMutex[i]) != 0)
      cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  int retval;
  string groupName;
  makeGroupName(&groupName, type,channel);

  int numsent = SP_scat_multicast(m_sendMailbox,
				  FIFO_MESS | SELF_DISCARD,
				  groupName.c_str(), 0, msgs);
  int msgsize = 0;
  for(int i=0; i<msgs->num_elements; i++) {
    msgsize+=msgs->elements[i].len;
  }

  if(numsent == msgsize)
    retval = msgsize;
  else {
    cerr << "skynet::send_msg failed: "<< endl;
    SP_error(numsent);
    retval = 0;
    if(numsent == CONNECTION_CLOSED) {
      cerr << "lost spread connection, trying to reconnect..." << endl;
      m_sendMailbox = spreadConnect();
    }
  }

  if (bReleaseMutexWhenDone)
    for (int i=0; i<numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "skynet::sn_msg: failed to lock mutex" << endl;

  return retval;
}
/*!
 * Function: skynet::makeGroupName
 * \brief Construct group name from key and message type
 *
 * This function constructs a group name (string) from the skynet key
 * and the message type.  It basically creates a string that contains
 * both of these numbers separated by an underscore.
 *
 */

// void skynet::makeGroupName(string* pGroupName, int type)
// {
//   ostringstream groupNameStream;
//   groupNameStream << m_key << '_' << type;
//   *pGroupName = groupNameStream.str();
//   if(strlen(pGroupName->c_str()) > MAX_GROUP_NAME) {
//     cerr << "skynet key too long: " << m_key << endl;
//     exit(-1);
//   }
// }
void skynet::makeGroupName(string* pGroupName, int type, int channel)
{
  ostringstream groupNameStream;
  //groupNameStream << m_key << '_' << type << '_' << channel;
  if (m_key>99999){
    cerr << "Error in skynet::makeGroupName - skynet key value of "
         << m_key << " too many characters, max value = 99999" << endl;
    exit(-1);
  }else if (m_key < 0){
    cerr << "Error in skynet::makeGroupName - skynet key value of "
         << m_key << " less than zero" << endl;
    exit(-1);
  }

  if (type>99999){
    cerr << "Error in skynet::makeGroupName - skynet type value of "
         << type << " too many characters, max value = 99999" << endl;
    exit(-1);
  }else if (type < 0){
    cerr << "Error in skynet::makeGroupName - skynet type value of "
         << type << " less than zero" << endl;
    exit(-1);
  }

  if (channel>99999){
    cerr << "Error in skynet::makeGroupName - skynet channel value of "
         << channel << " too many characters, max value = 99999" << endl;
    exit(-1);
  }else if (channel < -9999){
    cerr << "Error in skynet::makeGroupName - skynet channel value of "
         << channel << " too many characters, min value = -9999" << endl;
    exit(-1);
  }


  if (channel <0){
    groupNameStream <<"sn:"<< setw(5)<<setfill('0')<< m_key
                    << ":"<< setw(5) << setfill('0') << type
                    << ":-"<< setw(4) << setfill('0') << -channel;
  }else{
    groupNameStream <<"sn:"<< setw(5)<<setfill('0')<< m_key
                    << ":"<< setw(5) << setfill('0') << type
                    << ":"<< setw(5) << setfill('0') << channel;
  }

  *pGroupName = groupNameStream.str();


  if(strlen(pGroupName->c_str()) > MAX_GROUP_NAME) {
    cerr << "skynet key too long: " << m_key << endl;
    exit(-1);
  }
}

// Comment these out for now to get rid of the sn_types dependency.
#ifdef UNUSED
/*!
 * Function: SN_bcastID
 * \brief Thread function to broad cast the skynet ID
 *
 * This function can be set up as a thread that sends out the skynet
 * ID at a period of I_AM_HERE_PERIOD.
 *
 */

/* Function to broadcast ID */
static void *SN_bcastID(void *arg)
{
  skynet &psn = *((skynet *) arg);
  int sendsock = psn.get_send_sock(SNmodlist);

  SSkynetID id;
  memset(id.host, 0, MAX_SIZE_HOSTNAME);
  if(	gethostname(id.host, MAX_SIZE_HOSTNAME) != 0 ) {
    cerr << "skynet::I_am_here() failed trying to get the hostname" << endl;
    return NULL;
  }

  id.name = psn.name();
  id.unique = psn.sendMailbox();
  while (true) {
    skynet_usleep(I_AM_HERE_PERIOD);

    id.status = psn.status();
    psn.send_msg(sendsock, &id, sizeof(id));
  }

  return NULL;
}
#endif
