/*!
 * \file UT_sendmsgs.cc
 * \brief Unit test for sending messages with V3 interface
 *
 * \author Richard M. Murray
 * \date 23 July 2007
 *
 * This unit test sends out a bunch of different messages that can be
 * received by the UT_recvmsgs program.
 *
 */

#include <iostream>
#include <assert.h>
#include "skynet.h"
#include "skynet.hh"

int main(int argc, char **argv)
{
  /* Get the skynet key */
  int snkey = skynet_findkey(argc, argv, 2);
  assert(snkey != -1);
  
  skynet_server_t *snserver = skynet_connect(snkey, 1);
  assert(snserver != NULL);
  cerr << "Connected to spread server" << endl;

  /* Send some test messages that we can read */
  char *buf = "hello world";
  skynet_send_message(snserver, 0x01, buf, strlen(buf), 0);
  skynet_send_message(snserver, 0x02, "anyone listening?", 14, 0);
  skynet_send_message(snserver, 0x02, "jello world!", 12, 1);
  skynet_send_message(snserver, 0x03, "should not be seen", 18, 1);
  skynet_send_message(snserver, 0x04, "channel 4 here", 14, 1);

  /* Time tagged message */
  time_t clock = time(NULL);
  printf("sending message type 5 @ %s\n", ctime(&clock));
  skynet_send_message(snserver, 0x05, ctime(&clock), 19, 0);

  /* All done; disconnect from the server */
  int status = skynet_disconnect(snserver);
  cerr << "Disconnected from spread server (" << status << ")" << endl;
  return 0;
}
