use 5.008008;
use ExtUtils::MakeMaker;

$CC = "g++";

WriteMakefile(
    NAME => 'Skynet::Parse',
    VERSION_FROM => 'Parse.pm',
    LIBS => ['-L../../../../lib/i486-gentoo-linux-static -ltrajutils -lskynettalker -lskynet -lboost_iostreams -lboost_serialization'],
    INC => '-I. -I../../../../include',
    CC => $CC,
    LD => '$(CC)',
    XSOPT => '-C++',
    XSPROTOARG => '-prototypes',
);
