/*!
 * \file UT_recvmsgs.cc
 * \brief Unit test for receiving messages with V3 interface
 *
 * \author Richard M. Murray
 * \date 23 July 2007
 *
 * This unit test recvs out a bunch of different messages that can be
 * received by the UT_recvmsgs program.
 *
 */

#include <iostream>
#include <assert.h>
#include <pthread.h>
#include "skynet.h"
#include "skynet.hh"

/* Thread handlers and callbacks */
void *msg2_thread(void *);
int msg3_callback(char *, int);
void *msg4_thread(void *);
void *msg5_thread(void *);

/* Global variables */
char *msg2_buf;

int main(int argc, char **argv)
{
  /* Get the skynet key */
  int snkey = skynet_findkey(argc, argv, 2);
  assert(snkey != -1);
  
  skynet_server_t *snserver = skynet_connect(snkey, 2);
  assert(snserver != NULL);
  cerr << "Connected to spread server" << endl;

  /*
   * Subscribe to some spread groups in different ways 
   */

  /* Receive messages but don't do anything with them */
  skynet_group_t *msg1 = 
    skynet_listen(snserver, 0x01, NULL, 0, NULL, NULL, NULL, 0);

  /* Receive messages in a buffer and read from a thread */
  msg2_buf = (char *) calloc(1, 10);  assert(msg2_buf != NULL);
  skynet_group_t *msg2 = 
    skynet_listen(snserver, 0x02, msg2_buf, 10, NULL, NULL, NULL, 0);
  pthread_t msg2_thread_id;
  pthread_create(&msg2_thread_id, NULL, msg2_thread, msg2);

  /* Receive messages via a callback */
  skynet_group_t *msg3 =
    skynet_listen(snserver, 0x02, NULL, 0, msg3_callback, NULL, NULL, 1);

  /* Use get_message */
  skynet_group_t *msg4 =
    skynet_listen(snserver, 0x04, NULL, 32, NULL, NULL, NULL, 1);
  pthread_t msg4_thread_id;
  pthread_create(&msg4_thread_id, NULL, msg4_thread, msg4);

  /* Use get_latest */
  skynet_group_t *msg5 =
    skynet_listen(snserver, 0x05, NULL, 32, NULL, NULL, NULL, 0);
  pthread_t msg5_thread_id;
  pthread_create(&msg5_thread_id, NULL, msg5_thread, msg5);

  /* Start up the server part way through the listen messages */
  pthread_t thread_id;
  pthread_create(&thread_id, NULL, skynet_main_thread, (void *) snserver);

  /* Go through and print out when messages are received */
  cerr << "Send message type 1 five times" << endl;
  for (int i = 0; i < 5; ++i) {
    int len = skynet_get_next(msg1, NULL, 0);
    printf("Received message of type %d, length %d\n", msg1->msgtype, len);
  }

  /* Unsubscribe from a group */
  cerr << "Message type 1 now ignored; waiting 5 seconds" << endl;
  skynet_ignore(msg1);
  sleep(5);

  /* All done; disconnect from the server */
  int status = skynet_disconnect(snserver);
  cerr << "Disconnected from spread server (" << status << ")" << endl;
  return 0;
}

void *msg2_thread(void *arg)
{
  skynet_group_t *msg2 = (skynet_group_t *) arg;
  while (1) {
    int len = skynet_get_next(msg2, NULL, 0);
    printf("Received message of type %d, length %d: ", msg2->msgtype, len);
    int i = 0;
    while (i < len && i < 10) putchar(msg2_buf[i++]);
    printf("\n");
  }
}

int msg3_callback(char *data, int len)
{
    int i = 0;
    printf("Received callback of length %d: ", len);
    for (i = 0; i < len; ++i) putchar(data[i]);
    printf("\n");
    return 0;
}

void *msg4_thread(void *arg)
{
  skynet_group_t *msg = (skynet_group_t *) arg;
  char buffer[20];

  while (1) {
    int len = skynet_get_message(msg, buffer, 19);
    buffer[len] = 0;		// terminate the string
    printf("Received message of type %d, length %d: %s\n",
	   msg->msgtype, len, buffer);
  }
}

void *msg5_thread(void *arg)
{
  skynet_group_t *msg = (skynet_group_t *) arg;
  char buffer[20];

  while (1) {
    int len = skynet_get_latest(msg, buffer, 19);
    buffer[len] = 0;		// terminate the string
    printf("Latest message of type %d, length %d: %s\n",
	   msg->msgtype, len, buffer);
    sleep(1);
  }
}
