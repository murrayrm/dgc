/*!
 * \file snlisten.cc 
 * \brief short utility to listen to SKYNET messages
 * 
 * \author Richard M. Murray
 * \date 27 Nov 06 (based on modules/author code)
 * 
 * This utility can be used to listen to skynet messages.  It is based
 * on the 'author' code written by Dima Kogan.  It works by setting up
 * a thread to listen to all messages of a given type.
 *
 * Usage: snlisten
 */

#include <stdlib.h>
#include <iostream>
#include <pthread.h>
#include <sys/time.h>		// for gettimeofday
using namespace std;

/* Skynet headers */
#include "skynet.hh"
#include "sn_types.h"
#include "sn_msg.hh"
const unsigned int MAX_BUFFER_SIZE = 100000; // max message length

/* A few global variables to control operation */
long long timeoffset = 0;	// offset time

/* Structure to keep track of what to do with each message type */
struct SContext
{
  skynet*          pSkynet;
  sn_msg           msgtype;
  int              handle;
  pthread_mutex_t* pMutex;
  int              socket;
  bool		   stdoutf;	// send output to standard output?
  void (*print)(char *, int);	// function to print out message data
};

/* Thread function to listen to each message type */
void* listenToType(void *pArg)
{
  int old;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &old);
  pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, &old);

  SContext *pContext = (SContext*) pArg;
  char* pBuffer = new char[MAX_BUFFER_SIZE];

  while(true) {
    int msgsize = pContext->pSkynet->get_msg(pContext->socket,
					      pBuffer, MAX_BUFFER_SIZE, 0);

    /* Get time stamp */
    timeval tv; gettimeofday(&tv, NULL);
    tv.tv_sec -= timeoffset;

    /* Lock Mutext so that we don't overwrite each other */
    pthread_mutex_lock(pContext->pMutex);

    /* Write the data out to the appropriate file */
#ifdef UNUSED
    write(pContext->handle,(char*)&stamp, sizeof(stamp));
    write(pContext->handle, (char*) &pContext->msgtype,
	  sizeof(pContext->msgtype));
    write(pContext->handle, (char*)&msgsize, sizeof(msgsize));
    write(pContext->handle,(char*)pBuffer, msgsize);
#endif
    if (pContext->stdoutf) {
      /* Print a message to stdout */
      fprintf(stdout, "%ld.%d: %s (%d)", tv.tv_sec, tv.tv_usec,
	      sn_msg_asString(pContext->msgtype), msgsize);
      if (pContext->print != NULL) {
	fprintf(stdout, " - ");
	(*pContext->print)(pBuffer, msgsize);
      }	
      fprintf(stdout, "\n");
    }

    /* Release mutex */
    pthread_mutex_unlock(pContext->pMutex);
  }
  delete pBuffer;
}


int main(int argc, char *argv[])
{
  /* Get the skynet key */
  int sn_key = skynet_findkey(argc, argv, -1);

  /* Messages types that we want to listen for */
  bool msg[last_type];
  for (int i = 0; i < last_type; ++i) msg[i] = true;

  /* Parse command line arguments */
  /*! Not implemented !*/

  /* Connected to skynet; warn if we didn't get a skynet key */
  if (sn_key == -1) {
    fprintf(stderr, "snlisten: no skynet key specified; using key = 0\n");
    sn_key = 0;
  }
  skynet skynetobject(SNguilogwriter, sn_key);

  /* Check for specific message types */
  /*! Not implemented; hard code in for now !*/
  msg[SNstate] = false;
  msg[SNactuatorstate] = false;
  msg[SNmodlist] = false;

  /* Listen for messages and print out information on each */
  SContext contexts[last_type];		// message information
  pthread_mutex_t filemutex;		// avoid overwriting ourself
  pthread_t thread_id[last_type];	// vector of thread IDs
  pthread_mutex_init(&filemutex, NULL);	// initialize mutex

  int cnt = 0;				// thread counter
  cout << "Setting up listeners (" << last_type << "): ";
  for (int i = 0; i < last_type; ++i) {
    if (!msg[i]) continue;	// only listen to specified messages
    cout << i << " ";

    /* Set up context information */
    contexts[i].socket = skynetobject.listen((sn_msg) i, ALLMODULES);
    contexts[i].pSkynet = &skynetobject;
    contexts[i].msgtype = (sn_msg) i;
    contexts[i].handle = STDOUT_FILENO;;
    contexts[i].pMutex = &filemutex;
    contexts[i].stdoutf = 1;	// !always print output for now
    contexts[i].print = NULL;	// no printing support yet

    /* Start thread to listen to messages */
    pthread_create(&thread_id[cnt++], NULL, &listenToType,
		   (void*) &contexts[i]);
  }
  cout << endl;

  /* Wait until threads end (shouldn't ever happen) */
  pthread_join(thread_id[0], NULL);

  return 0;
}
