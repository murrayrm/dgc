/*!
 * \file SkynetContainer.cc
 * \brief Container class for skynet objects (SKYNET_V3 only)
 *
 * \author Richard M. Murray
 * \date 27 July 2007
 *
 * This file contains the functions required to implement the skynet
 * container class for skynet v3.  The only real functionality is the
 * constructor function.
 *
 */

#ifndef SKYNET_V2
#include <assert.h>
#include "SkynetContainer.hh"

/* Initialize the pointer to the static skynet server structure */
skynet_server_t *CSkynetContainer::snserver = NULL;

/*! Default constructure functions */
CSkynetContainer::CSkynetContainer()
{
  cerr << "CSkynetContainer() warning: should not be called" << endl;
  abort();
}

/*! Constructor function */
CSkynetContainer::CSkynetContainer(int module_id, int skynet_key, int *status)
{
  /* Check to see if we have already initialized */
  if (snserver != NULL) return;

  /* Instantiate a skynet server */
  snserver = skynet_connect(skynet_key, module_id);
  assert(snserver != NULL);

  /* Start a thread to read messages */
  int thread_status = 
    pthread_create(&thread_id, NULL, skynet_main_thread, (void *) snserver);

  /* Return success (if asked) */
  if (status != NULL) *status = thread_status;
}

/*! Deconstructor */
CSkynetContainer::~CSkynetContainer()
{
  /* TODO: keep track of whether anyone is connected and disconnect */
}

/*
 * Compatibility functions
 *
 * To allow legacy (V2) skynet programs make use of a single
 * connection to spread, we define the functions that the old skynet
 * object provided.
 *
 */

/*! Join a skynet group */
int SkynetV2Compatible::listen(int type, int module, int channel)
{
  skynet_server_t *snserver = CSkynetContainer::getServer();
  skynet_group_t *sngroup;

  /* Listen for messages in this group/channel (assume max length) */
  sngroup = skynet_listen(snserver, type, NULL, SKYNET_MAX_MESSAGE_LENGTH,
			  NULL, NULL, NULL, channel);

  /* Return an index into the group */
  return skynet_get_group_index(snserver, sngroup);
}

/*! Get a message (blocking) */
size_t SkynetV2Compatible::get_msg(int index, void *msgbuf, size_t buflen,
			    int options) 
{
  skynet_server_t *snserver = CSkynetContainer::getServer();

  /* Get the skynet group we are listening to */
  skynet_group_t *sngroup = skynet_get_group_pointer(snserver, index);
  assert(sngroup != NULL);

  /* Read the message and return the size */
  return skynet_get_message(sngroup, (char *) msgbuf, buflen);
}

/*! Get a message with optional mutices (blocking) */
size_t SkynetV2Compatible::get_msg (int mboxidx, void *msgbuf, size_t buflen,
				    int options, pthread_mutex_t** ppMutex, 
				    bool bReleaseMutexWhenDone, int numMutices)
{
  skynet_server_t *snserver = CSkynetContainer::getServer();
  skynet_group_t *sngroup = skynet_get_group_pointer(snserver, mboxidx);

  if(numMutices != 0) {
    /* Wait until there is a message ready to read */
    pthread_mutex_lock(sngroup->mutex);  
    pthread_cond_wait(sngroup->cond, sngroup->mutex);  
    pthread_mutex_unlock(sngroup->mutex);  

    /* Now lock all user supplied mutices */
    for (int i=0; i<numMutices; i++)
      if (pthread_mutex_lock(ppMutex[i]) != 0)
	cerr << "SkynetV2::get_msg: failed to lock mutex" << endl;
  }

  /* Get the message */
  int status = skynet_get_latest(sngroup, (char *) msgbuf, buflen);

  if (bReleaseMutexWhenDone) {
    for(int i=0; i<numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "SkynetV2::sn_msg: failed to lock mutex" << endl;
  }

  return status;
}

/*! Send a message to a skynet group */
size_t SkynetV2Compatible::send_msg(int type, void *msgbuf, size_t buflen,
				    int options)
{
  skynet_server_t *snserver = CSkynetContainer::getServer();
  return skynet_send_message(snserver, type, (char *) msgbuf, buflen, 0);
}

/*! Get a message with optional mutices (blocking) */
size_t SkynetV2Compatible::send_msg(int type, void *msgbuf, size_t buflen,
				    int options, pthread_mutex_t** ppMutex, 
				    bool bReleaseMutexWhenDone, int numMutices)
{
  skynet_server_t *snserver = CSkynetContainer::getServer();

  /* See whether we need to lock any user mutices */
  if(numMutices != 0) {
    /* Lock all user supplied mutices */
    for (int i = 0;  i < numMutices; i++)
      if (pthread_mutex_lock(ppMutex[i]) != 0)
	cerr << "SkynetV2::send_msg: failed to lock mutex" << endl;
  }

  /* Send the message */
  int status = skynet_send_message(snserver, type, (char *) msgbuf, buflen, 0);

  /* Release the user mutices */
  if (bReleaseMutexWhenDone) {
    for (int i = 0; i < numMutices; i++)
      if (pthread_mutex_unlock(ppMutex[i]) != 0)
	cerr << "SkynetV2::sn_msg: failed to lock mutex" << endl;
  }

  /* Return the status from skynet */
  return status;
}

/*! Get the sending socket id (no-op) */
int SkynetV2Compatible::get_send_sock(int type)
{
  return type;
}

/*! Wait for a message to arrive on a channel */
void SkynetV2Compatible::sn_select(int mboxidx)
{
  skynet_server_t *snserver = CSkynetContainer::getServer();
  skynet_group_t *sngroup = skynet_get_group_pointer(snserver, mboxidx);

  /* Wait until a message comes through, then return */
  pthread_mutex_lock(sngroup->mutex);
  while (!sngroup->msg_ready) pthread_cond_wait(sngroup->cond, sngroup->mutex);
  pthread_mutex_unlock(sngroup->mutex);
}

/*! Check to see if more messages are available */
bool SkynetV2Compatible::is_msg(int mboxidx) {
  skynet_server_t *snserver = CSkynetContainer::getServer();
  skynet_group_t *sngroup = skynet_get_group_pointer(snserver, mboxidx);

  return skynet_peek(sngroup, NULL);
}


#endif	// SKYNET_V2
