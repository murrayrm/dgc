/*!
 * \file skynet.h
 * \brief Main header file for skynet API (C)
 *
 * \author Richard M. Murray
 * \date 23 July 2007
 *
 */

#ifndef __SKYNET_H__
#define __SKYNET_H__

#include <sp.h>
#include <pthread.h>			/* Process thread library */

#define SKYNET_MAXGROUPS 64		/* Max number of groups */

/*! 
 * \struct skynet_server
 * \brief Skynet server information
 *
 * The skynet_server struct is used to keep track of a connection to
 * the spread server.  Each server has a set of messages groups that
 * it maintains, which are created using the skynet_listen() function.
 * Each connection uses a skynet key to identify which processes it
 * will communicate with (all processes using the same skynet key can
 * see each other's messages).
 *
 * A skynet connection is a spread connection that uses a private
 * group name of the form sn:SSSSS:MMMMMM:CCCCC, where SSSSS is the
 * skynet key, MMMMM is the module identifier and CCCCC is the
 * channel number.
 *
 * Each skynet server connection uses an internal buffer to store
 * incoming messages from spread and then distributes this information
 * to any skynet groups that are attached to the server.  The
 * skynet_main() function can be used to process incoming messages.
 * The method of processing is determined by the skynet_listen()
 * function call, which creates skynet groups.
 *
 * \param skynet_key
 * \brief Skynet key for this connection.  Integer between 1 and 99999
 * 
 * \param module_id
 * \brief Module identification number.  Integer between 1 and 99999
 *
 * \param spread_mbox
 * \brief Spread mailbox number (from SP_connect)
 *
 * \param data
 * \brief Data buffer for storing data (SKYNET_MAX_MESSAGE_SIZE bytes)
 *
 * \param groups
 * \brief List of all groups we are listening to across this connection
 *
 * \param ngroups
 * \brief Number of active groups for this connection
 *
 * \param mutex
 * \brief Mutex for accessing the structure data elements
 *
 * \param quit
 * \brief Flag to for skynet_main() to terminate
 *
 */

typedef struct skynet_server {
  int skynet_key;			/* Skynet key for this connection */
  int module_id;			/* Module identification number */
  mailbox spread_mbox;			/* Spread mailbox */
  char *data;				/* Data buffer for incoming messages */

  /* List of skynet groups that we are subscribed to */
  struct skynet_group *groups[SKYNET_MAXGROUPS];
  int ngroups;				/* Number of groups in use */

  /* Objects for controlling the server data processing */
  pthread_mutex_t *mutex;		/* Mutex for protecting access */
  int quit;				/* Flag to cause skynet_main to end */
} skynet_server_t;

/*
 * \struct skynet_group
 * \brief Skynet group membership
 *
 * The skynet_group struct is used to maintain information about a
 * connection to a skynet message group/channel.  Each connection is
 * initiated by calling the skynet_listen() function, which
 * initializes the data structure.  Depending on the arguments passed
 * to skynet_listen(), messages sent to the specified group can be
 * retrieved using a user-specified buffer, with mutex conditions or
 * via a callback.
 *
 * \param server
 * \brief Skynet server connection for this group
 * 
 * \param spread_mbox
 * \brief Spread mailbox number (copied from server)
 *
 * \param msgtype
 * \brief Skynet message type (defined in interfaces/sn_types.h)
 *
 * \param channel
 * \brief Skynet channel number (default = 0)
 *
 * \param name
 * \brief Spread group name ('sn:<key>:<typ>:<chn>')
 *
 * \param msgbuf
 * \brief Buffer for storing data locally
 *
 * \param buflen
 * \brief Length of the local storage buffer
 *
 * \param msglen
 * \brief Length of the most recent skynet message (may be > buflen)
 *
 * \param handler
 * \brief Callback function, used whenever a message is received
 *
 * \param mutex
 * \brief Mutex for protecting access to local data
 *
 * \param cond
 * \brief Conditional used to signal that new data is available
 *
 * \param msg_ready
 * \brief Internal flag indicating that data is available to be read
 *
 * \param msg_count
 * \brief Internal count of the number of messages that have been recv'd
 *
 */

typedef struct skynet_group {
  skynet_server_t *server;		/* Skynet server information */
  mailbox spread_mbox;			/* Spread mailbox */
  int msgtype;				/* Message type */
  int channel;				/* Message channel number */
  char *name;				/* Spread group name */
  char *msgbuf;				/* Buffer for storing latest message */
  int buflen;				/* Length of message buffer */
  int msglen;				/* Length of last received message */
  int (*handler)(char *, int);		/* Callback function */
  pthread_mutex_t *mutex;		/* Mutex for blocking buffer access */
  pthread_cond_t *cond;			/* Pthread conditional variable */

  int msg_ready;			/* Message is ready to be read */
  int msg_count;			/* Number of messages received */
} skynet_group_t;

/*! Maximum length of a skynet message */
#define SKYNET_MAX_MESSAGE_LENGTH 0x10000

/* Function declarations */
#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * \function skynet_connect
 * \brief Connect to the spread server
 *
 * \param snkey
 * \brief Skynet key to use for the connection
 *
 * \param modid
 * \brief Module ID number (pass as negative for unique connection)
 *
 * The skynet_connect() function is used to initialize a connection to
 * the spread server.  It takes a skynet key and a module ID as
 * arguments.  The key and module ID are used to construct the private
 * group name that is used for maintaining the connection to spread.
 *
 * The module ID is a positive number that is used to establish a
 * conection with the spread server.  To insure that only one copy of
 * a module is running, you can pass the module number as a negative
 * number.  In this case, if you try to connect to spread from two
 * processes using the same module ID, skynet_connect will abort.
 * This is intended to insure that only one copy of a module is
 * running.  In all cases, the absolute value of the module ID will be
 * used (so the negative will be inverted).
 *
 */

extern skynet_server_t *skynet_connect(int snkey, int modid);

/*!
 * \function skynet_disconect
 * \brief Disconnect from the spread server
 * 
 * \param snserver
 * \brief Pointer to skynet server structure
 *
 * The skynet_disconnect() function disconnects from Spread and frees
 * up all memory associated with the skynet conneciton.
 *
 */
extern int skynet_disconnect(skynet_server_t *);

/*!
 * \function skynet_listen
 * \brief start listening to messages of a given type
 *
 * \param snserver
 * \brief Skynet server to use for this group
 *
 * \param type
 * \brief Message type (group ID)
 *
 * \param msgbuf
 * \brief User supplied message buffer (optional)
 *
 * \param buflen
 * \brief Length of message buffer
 *
 * \param handler
 * \brief Callback hander to process incoming messages
 *
 * \param mutex
 * \brief Mutex for controlling access to buffer
 *
 * \param cond
 * \brief Conditional to signal when new data is available
 *
 * \param channel
 * \brief Channel number to use for this message type
 *
 * The skynet_listen() function is the main function used to join a
 * skynet group and begin to receive messages.  Its arguments can be
 * used in a number of different ways depending on how messages will
 * be processed.
 * 
 * Message type and channel: the msgtype and channel arguments are
 * used to define the types of messages that should be received.  The
 * msgtype/channel pair describe a unique spread group, so in
 * principle only one is required.  However, it is convenient to
 * interpret the messge type as a given representation of a data
 * stream (eg, MapElements) and the channel allows the data to be
 * directed to a specific receiver.
 *
 * Buffers: if the msgbuf argument is non-NULL, it will be used to
 * store copies of all messages as they arrive.  The buflen argument
 * is required in this case and must indicate the maximum amount of
 * data that can be copied into the buffer.  If msglen is non-zero but
 * msgbuf is NULL, an internal buffer of length msglen will be
 * allocated.  If msgbuf is zero and buflen is zero, an internal
 * buffer equal to the maximum allowable message size (about 64K) will
 * be allocated.  
 *
 * Mutex: if the mutex argument is given, the user-supplied mutex will
 * be locked before any access to the data buffer.  If no mutex is
 * given, an internal mutex will be allocated and used by functions
 * that access the message data.
 *
 * Callback: if the callback function is specified, it will be called
 * with a pointer to the message and the length of the message when
 * the data is recieved.  The callback is called within the main
 * receiving loop, so no messages will be processed while the callback
 * is active.
 *
 * Conditional: if the cond argument is given, the user-supplied
 * conditional will be released whenever a message is received.  This
 * allows a process to block until new data is available.  If a
 * conditional is specified, a mutex must also be given (as required
 * by pthreads).  If no user-specified conditional is given, an
 * internal conditional is created and used to provide blocking for
 * the skynet_get_* function calls.
 *
 * The processing of incoming packets is done as follows:
 *
 *   + The mutex is locked
 *   + The message is copied to the user-supplied buffer, if available
 *   + The handler is called with a pointer to the messge data and length
 *   + The conditional is broadcast, releasing any threads blocking on it
 *   + The mutex is unlocked
 *
 */

extern skynet_group_t *skynet_listen(skynet_server_t *, int, char *, int,
				     int (*)(char *, int),
				     pthread_mutex_t *, pthread_cond_t *, int);

/*!
 * \function skynet_ignore
 * \brief Disconnect from a skynet channel
 *
 * The skynet_ignore() function is used to stop listening to messages
 * of a given type (and on a given channel).  It frees all resources
 * associated with that group.
 */

extern int skynet_ignore(skynet_group_t *);

/*! 
 * \function skynet_main
 * \brief Receive skynet messages and process them
 *
 * The skynet_main() function is used to process incoming messages.
 * It reads all messages for groups that are currently active and then
 * processes the data according to the protocol described in
 * skynet_listen().  The following steps are performed for each
 * message that arrives:
 *
 *   + The mutex is locked
 *   + The message is copied to the user-supplied buffer, if available
 *   + The handler is called with a pointer to the messge data and length
 *   + The conditional is broadcast, releasing any threads blocking on it
 *   + The mutex is unlocked
 *
 */

extern void skynet_main(skynet_server_t *);

/*!
 * \function skynet_main_thread
 * \brief Call skynet_main() in a thread
 *
 * The skynet_main_thread() function calls the skynet_main() program.
 * It is typed so that it can be used as an argument to the
 * pthread_create function.  The argument for pthread_create should be
 * the skynet server pointer.
 *
 */
extern void *skynet_main_thread(void *);

/*!
 * \function skynet_get_message
 * \brief Get the most recent unread message of a given type
 *
 * \param snserver
 * \brief Skynet server
 *
 * \param msgbuf
 * \brief Buffer containing message data
 *
 * \param buflen
 * \brief Length of message buffer
 *
 * The skynet_get_message() function is the main function to read an
 * incoming message.  The function blocks until a message is received
 * and then copies the message into the user-specified buffer (up to
 * buflen).  The function returns the actual amount of data in the
 * message (which may be different than buflen).
 * 
 * This function will block if no new messages have been received
 * since the last call to skynet_get_message(). It will return
 * immediately if an unread message is available, however it does not
 * queue messages so message delivery is not guaranteed to be reliable
 * (use callbacks if you need reliable message delivery).
 *
 * The skynet_get_message() function accepts a NULL pointer for the
 * message buffer, in which case it doesn't copy the data.  If a
 * user-supplied buffer was provided to skynet_listen(), the data can
 * be accessed from there, otherwise it is not available.
 *
 */

extern int skynet_get_message(skynet_group_t *, char *, int);

/*!
 * \function skynet_get_next
 * \brief Wait until a message of the given type arrives (blocking)
 *
 * Copies the most recently received skynet message into the provided
 * buffer.  This function blocks if no messages have been received on
 * the given channel, otherwise returns immediately.  If it is called
 * multiple times between receipt of a new message, it will return the
 * most recent copy of the message.
 */
extern int skynet_get_next(skynet_group_t *, char *, int);

/*!
 * \function skynet_get_latest
 * \brief Copy the most recently received message into a supplied buffer
 *
 * Copies the most recently received skynet message into the provided
 * buffer.  This function blocks if no messages have been received on
 * the given channel, otherwise returns immediately.  If it is called
 * multiple times between receipt of a new message, it will return the
 * most recent copy of the message.
 */

extern int skynet_get_latest(skynet_group_t *, char *, int);

/*!
 * \function skynet_send message
 * \brief Send a message
 *
 * \param snserver
 * \brief Skynet server pointer
 *
 * \param type
 * \brief Message type
 *
 * \param msgbuf
 * \brief Data to be sent
 *
 * \param msglen
 * \brief Length of data in buffer
 *
 * \param channel
 * \brief Channel numberm to send the message
 *
 * Sends a message to the specified group using the snserver
 * connnection.
 */

extern int skynet_send_message(skynet_server_t *, int, char *, int, int);

/*!
 * \function skynet_peek
 * \brief Check to see if there is a new message available
 *
 * If a new message of the specified type is available, stores its
 * size in msgsizep (if not NULL) without reading the message, and
 * returns true (1), else it returns false (0). This function never
 * blocks.  This function can be used to check for the presence of a
 * new message too by passing msgsizep as NULL.
 *
 */

extern int skynet_peek(skynet_group_t *, int *);

/* Utility functions (mainly for v2 compatibility) */
extern int skynet_get_group_index(skynet_server_t *, skynet_group_t *);
extern skynet_group_t *skynet_get_group_pointer(skynet_server_t *, int);

#ifdef __cplusplus
}
#endif

#endif // __SKYNET_H__
