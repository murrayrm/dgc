#include <iostream>
#include <sstream>
using namespace std;

#include <fcntl.h>
#include <stdlib.h>
#include <math.h>

#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"
#include "skynet.hh"
#include "logger.h"
#include "cmdline_skynet_player.h"

#define MAX_BUFFER_SIZE 1000000
#ifdef MACOSX
#define O_LARGEFILE 0x00	// not defined (needed?) in OS X
#endif

/* Global variables used to control execution */
int pause_flag = 0;		// pause playback
unsigned long msgcnt[last_type];
unsigned int msglen[last_type];
int pause_playback_cb(long arg);
int resume_playback_cb(long arg);

struct SContext
{
  skynet*          pSkynet;
  int              handle;
  int*             sockets;
  char*            pBuffer;
  double           speed;
  unsigned long*   pPosition;
  int		   sparrow;
  int hour, min, sec;		// timestamp info
};

/* Sparrow display */
#include "sparrow/display.h"
#include "skynet-player.h"

/******************************************************************************/
/******************************************************************************/
void prompt()
{
  cout << "> ";
  cout.flush();
}


/******************************************************************************/
/******************************************************************************/
void print_help()
{
  cout << "\n Enter the number of messages you want to playback.\n";
  cout << " Precede with + to FF a certain number of messages.\n";
  cout << " Precede with = to FF to a specific position in the future.\n";
  cout << " s# plays back at speed # (1.0 = real time).\n";
  cout << " 'h' to print this help message; '0' to quit.\n";
}


/******************************************************************************/
/******************************************************************************/
bool readHdr(int handle, int& logtype, int& msgtype, int& msgsize,
	     unsigned long long& timestamp, int &channel)
{
  if (read(handle, (char*)&timestamp, 
	   sizeof(timestamp)) != sizeof(timestamp) ||
      read(handle, (char*)&logtype, sizeof(logtype)) != sizeof(logtype)) {
    cerr << "reached end of file" << endl;
    return false;
  }

  /* Remaining information depends on the message type */
  switch (logtype) {
    struct skynet_header_type1 type1;

  case SKYNET_HEADER_TYPE1:
  case SKYNET_HEADER_TYPE2:
    if (read(handle, (char*) &type1, sizeof(type1)) != sizeof(type1))
      return false;
    msgtype = type1.msgtype;
    msgsize = type1.msgsize;
    channel = type1.channel;
    break;

  default:
    msgtype = logtype;
    if (read(handle, (char*) &msgsize, sizeof(msgsize)) != sizeof(msgsize))
      return false;
    channel = 0;		// No channel information for older types
    break;
  }

  if (msgtype < 0 || msgtype >= last_type) {
    cerr << "msgtype too large; corrupt data;" << endl;
    return false;
  } else {
    msgcnt[msgtype]++;
    msglen[msgtype] = msgsize;
  }

  return true;
}


/******************************************************************************/
/******************************************************************************/
void* play(void *pArg)
{
  int old;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &old);
  pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, &old);

  SContext *pContext = (SContext*)pArg;
  unsigned long long starttime;
  DGCgettime(starttime);

  unsigned long long timestampPlayStart = 0ULL;

  while(true) {
    int logtype, msgtype, msgsize, channel;
    unsigned long long timestamp;

    /* See if we are paused */
    if (pause_flag) {
#     define PAUSE_TIME 100000
      DGCusleep(PAUSE_TIME);
      starttime += PAUSE_TIME;
      continue;
    }

    if(!readHdr(pContext->handle, logtype, msgtype, msgsize, 
		timestamp, channel)) {
      // should delete the pDatabuffer, but whatever
      cerr << "Unable to read header at position " << *(pContext->pPosition)
	   << endl;
      exit(0);
    }

    /* Make sure this is a message type that we handle */
    switch (logtype) {
    case SKYNET_HEADER_TYPE2:
      cerr << "Log file does not contain message data" << endl;
      exit(0);

    default:
      break;
    }

    /* Parse the timestamp */
    const time_t tval = timestamp/1000000;
    struct tm * tloc = localtime(&tval);
    pContext->hour = tloc->tm_hour;
    pContext->min = tloc->tm_min;
    pContext->sec = tloc->tm_sec;

    if(read(pContext->handle,(char*)pContext->pBuffer, msgsize) != msgsize) {
      cerr << "reached end of file" << endl;
      // should delete the pDatabuffer, but whatever
      exit(0);
    }

    /* Figure out how long to wait before sending message */
    if(timestampPlayStart == 0ULL) timestampPlayStart = timestamp; 
    unsigned long long now = DGCgettime();
    unsigned long long diffsim = timestamp- timestampPlayStart;
    unsigned long long diffreal = now - starttime;
    diffreal = llround(pContext->speed * (double)diffreal);
    if(diffsim > diffreal)
      DGCusleep(diffsim - diffreal);

    /* Send the message out */
    int numsent = 
      pContext->pSkynet->send_msg_channel(pContext->sockets[msgtype], channel,
					  pContext->pBuffer, msgsize, 0);
    (*pContext->pPosition)++;

    if ((*pContext->pPosition % 1000 == 0) && !pContext->sparrow)
    {
      // Get the time of day 
      char tstr[30];
      strftime(tstr, 30, " (%a %b %d, %H:%M:%S, %Y)", tloc);

      // Print out relevant information
      cout << "\rPosition: " << *pContext->pPosition;
      cout << ", Timestamp: " << timestamp << tstr;
      cout << ", msgtype: " << sn_msg_asString((sn_msg) msgtype) << endl;
      prompt();
    }
                    
    if(numsent != msgsize) {
      cerr << "tried sending " << msgsize << " bytes; sent " 
           << numsent << " bytes." << endl;
    }
  }
} // end play()


/******************************************************************************/
/******************************************************************************/
int main(int argc, char *argv[])
{
  char*                pDatabuffer;
  int		       logtype;
  int                  msgtype;
  int                  msgsize;
  int		       channel;
  unsigned long long   timestamp;
  int bytesSent;

  int sockets[last_type];
  int i;
  unsigned long position = 0;

  pDatabuffer = new char[MAX_BUFFER_SIZE];

  // Set input stream/file
  bool bPlayall = false;
  int handle;


  /* Parse command line options */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) {
    exit(1);
  }

  /* Read the input file */
  if (cmdline.inputs_num != 1) {
    cmdline_parser_print_help();
    exit(1);
  }
  handle = open(cmdline.inputs[0], O_RDONLY | O_LARGEFILE);

  /* See if we should just replay right away */
  bool justPlayIt = false;
  if (cmdline.just_play_given) {
    justPlayIt = true;
  }


  // Set up communications
  int sn_key = skynet_findkey(argc, argv);
  cerr << "Constructing skynet with key " << sn_key << endl;  
  skynet skynetobject(SNguilogplayer, sn_key);

  /* Create a socket for sending out each message type */
  for(i=0; i<last_type; i++) {
    sockets[i] = skynetobject.get_send_sock((sn_msg)i);
  }

  /* 
   * Playback messages
   *
   * At this point we have opened the log file and initialized skynet,
   * so we can start processing messages.
   *
   */
  pthread_t threadid;
  SContext context;

  if (!cmdline.disable_console_flag) {
    /* Initialize sparrow */
    dd_open(); dd_usetbl(player_tbl);

    /* Set up bindings */
    dd_rebind_tbl("snkey", &sn_key, player_tbl);
    dd_rebind_tbl("position", &position, player_tbl);

    /* Bind some useful keys */
    dd_bindkey('p', pause_playback_cb);
    dd_bindkey('u', resume_playback_cb);

    /* Start thread to play messages */
    context.pSkynet   = &skynetobject;
    context.handle    = handle;
    context.sockets   = sockets;
    context.pBuffer   = pDatabuffer;
    context.pPosition = &position;
    context.sparrow   = 1;
    context.speed   = 1;	
    dd_rebind_tbl("speed", &context.speed, player_tbl);
    dd_rebind_tbl("hour", &context.hour, player_tbl);
    dd_rebind_tbl("min", &context.min, player_tbl);
    dd_rebind_tbl("sec", &context.sec, player_tbl);
    pthread_create(&threadid, NULL, &play, (void*)&context);

    dd_loop();
    dd_close();

  } else {
    /* Old interface */
    string line;
    long numToPlayback;

    long lastPlayedBack = 1;
    bool doPlay;
    bool doContinuousPlay = false;
    bool countMessages = false;
    bool firstRun = true;

    // Print the help message (instructions)
    if (!justPlayIt) {
      print_help();
    }

    // Play back messages and handle user input
    while(true) {
      doPlay = true;

      // get number of messages to play (or ff)
      if(!bPlayall) {
	if(!doContinuousPlay && !justPlayIt) {
	  cout << endl << "Position: " << position << endl;
	  prompt();
	}

	if (!justPlayIt) {
	  cin.getline(pDatabuffer, MAX_BUFFER_SIZE);
	}

	// Pressing enter will play back the same number as previous
	if(pDatabuffer[0] == '\0' && !justPlayIt) {
	  numToPlayback = lastPlayedBack;
	}

	// Option to skip a number of messages
	else if(pDatabuffer[0] == '+' && !justPlayIt) {
	  doPlay = false;
	  istringstream linestream(pDatabuffer+1);
	  linestream >> numToPlayback;
	}

	// Option to skip to a specific (absolute) message number
	else if(pDatabuffer[0] == '=' && !doContinuousPlay && !justPlayIt) {
	  doPlay = false;
	  unsigned long numToGoto;
	  istringstream linestream(pDatabuffer+1);
	  linestream >> numToGoto;
	  if (numToGoto < position) {
	    cerr << "Error: Can't seek backwards yet (sorry)." << endl;
	    numToPlayback = 0;
	  } else {
	    numToPlayback = numToGoto - position;
	  }
	}

	// Option to play back at some continuous speed
	else if((pDatabuffer[0] == 's' && !doContinuousPlay) || 
		(justPlayIt && firstRun )) {
	  if (justPlayIt) {
	    context.speed   = 1;
	    firstRun = false;
	  } else {
	    istringstream linestream(pDatabuffer+1);
	    linestream >> context.speed;
	  }
	  context.pSkynet   = &skynetobject;
	  context.handle    = handle;
	  context.sockets   = sockets;
	  context.pBuffer   = pDatabuffer;
	  context.pPosition = &position;

	  pthread_create(&threadid, NULL, &play, (void*)&context);
	  cout << "continuous play at speed " << context.speed 
	       << ". p to stop." << endl;
	  //prompt();
	  doContinuousPlay = true;
	}

	// Option to pause
	else if(pDatabuffer[0] == 'p' && !justPlayIt) {
	  doContinuousPlay = false;
	  pthread_cancel(threadid);
	  continue;
	}

	// Option to count message types and sizes
	else if(pDatabuffer[0] == 'c' && !justPlayIt) {
	  countMessages = !countMessages;
	  cout << "countMessages is now " << countMessages << endl;
	  continue;
	}

	// Option to print help message
	else if(pDatabuffer[0] == 'h' && !justPlayIt) {
	  print_help();
	  cout << "here" << endl;
	  continue;
	}

	else if (!justPlayIt) {
	  istringstream linestream(pDatabuffer);
	  linestream >> numToPlayback;
	  lastPlayedBack = numToPlayback;
	}

	if(numToPlayback == 0) {
	  break;
	}
      }
      
      else {
	// (continuous playback)
	numToPlayback = 1000000000;
      }

      if(doContinuousPlay) {
	continue;
      }

      for(i = 0; i<numToPlayback; i++) {
	// i not used     
	if(!readHdr(handle, logtype, msgtype, msgsize, timestamp, channel)) {
	  fprintf(stderr, "[%s:%d] Could not read header. Exiting.\n", 
		  __FILE__, __LINE__);
	  delete pDatabuffer;
	  return -1;
	}

	if(doPlay) {
	  /* Make sure this is a message type that we handle */
	  switch (logtype) {
	  case SKYNET_HEADER_TYPE2:
	    cerr << "Log file does not contain message data" << endl;
	    exit(0);

	  default:
	    break;
	  }

	  if(msgsize > MAX_BUFFER_SIZE)
	    {
	      cerr << "message too large: size = " << msgsize << ". skipping...\n";
	      lseek(handle, msgsize, SEEK_CUR);
	    }
	  else
	    {
	      if(read(handle, pDatabuffer, msgsize) != msgsize)
		{
		  cerr << "Reached end of log file" << endl;
		  delete pDatabuffer;
		  return 0;
		}
	    }

	  // Get the time of day 
	  const time_t tval = timestamp/1000000;
	  struct tm * tloc = localtime(&tval);
	  char tstr[30];
	  strftime(tstr, 30, " (%a %b %d, %H:%M:%S, %Y)", tloc);

	  cout << "Position: " << position+i;
	  cout << ", Timestamp: " << timestamp << tstr;
	  cout << ", msgtype: " << sn_msg_asString((sn_msg) msgtype) << endl;

	  //cerr << "about to send " << msgsize << " bytes" << endl;
	  //if(msgtype == SNstate) {
	  //cerr << "Northing = " << ((double*)pDatabuffer)[1] << endl;
	  //}

	  // send the message
	  bytesSent = skynetobject.send_msg_channel(sockets[msgtype], channel, 
						    pDatabuffer, msgsize, 0);

	  if(bytesSent != msgsize) {
	    cerr << "Tried to send " << msgsize << " but sent " 
		 << bytesSent << " bytes." << endl;
	    delete pDatabuffer;
	    return -1;
	  }
	}

	else {
	  // (!doPlay)
	  // reposition file offset
	  lseek(handle, msgsize, SEEK_CUR);
        
	  //cout << "Skipped: Timestamp: " << timestamp << ", msgtype: " << 
	  //sn_msg_asString(msgtype) << endl;
	}
      } // end for()

      // Get the current file position
      // Note: This does not work past 2^31 bytes for large files
      // off_t curpos = lseek(handle, 0, SEEK_CUR);
      // cout << "Current file position is " << curpos << " bytes (" 
      //     << double(curpos)/pow(2.0, 30) << " GB)." << endl;

      // advance position indicator all at once
      position += numToPlayback;

    } // end while()
  }

  delete pDatabuffer;
}

/*
 * Sparrow callbacks
 *
 */

/* Pause playback */
int pause_playback_cb(long arg) { pause_flag = 1; return 0; }
int resume_playback_cb(long arg) { pause_flag = 0; return 0; }
