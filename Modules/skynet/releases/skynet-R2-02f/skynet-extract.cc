/*!
 * \file skynet-extract
 * \brief Program to extract information from skynet log files
 *
 * \author Richard M. Murray
 * \date 19 August 2007
 *
 */

#include <iostream>
#include <fcntl.h>
#include <stdlib.h>
using namespace std;

#include "interfaces/sn_types.h"
#include "skynet.h"
#include "logger.h"
#include "cmdline_skynet_player.h"

/* Comand line options */
gengetopt_args_info cmdline;		// global command line flags
int &verbose = cmdline.verbose_arg;	// verbose messages

#ifdef MACOSX
#define O_LARGEFILE 0x00	// not defined (needed?) in OS X
#endif

/* Read header from log file */
bool readHdr(int handle, int& msgtype, int& msgsize,
	     unsigned long long& timestamp, int &channel)
{
  if (read(handle, (char*)&timestamp, sizeof(timestamp)) != sizeof(timestamp) ||
      read(handle, (char*)&msgtype, sizeof(msgtype)) != sizeof(msgtype)) {
    return false;
  }

  /* Remaining information depends on the message type */
  switch (msgtype) {
    struct skynet_header_type1 type1;

  case SKYNET_HEADER_TYPE1:
    if (read(handle, (char*) &type1, sizeof(type1)) != sizeof(type1))
      return false;
    msgtype = type1.msgtype;
    msgsize = type1.msgsize;
    channel = type1.channel;
    break;

  default:
    if (read(handle, (char*)&msgsize, sizeof(msgsize)) != sizeof(msgsize))
      return false;
    channel = 0;		// No channel information for older types
    break;
  }

  /* Check to make sure message type looks OK */
  if (msgtype < 0 || msgtype >= last_type)
    cerr << "Warning: unknown message type " << msgtype << endl;

  /* Check to make sure message size looks OK */
  if (msgsize < 0) {
    if (verbose)
      cerr << "Warning: invalid messages size (" << msgsize << ")" << endl;

    /* Try to recover if the user wants me to */
    return cmdline.auto_recover_flag ? true : false;
  }

  return true;
}

/* Extract information from a log file */
void extract(gengetopt_args_info &cmdline)
{
  int handle;
  char* pBuffer = new char[SKYNET_MAX_MESSAGE_LENGTH];

  /* Open the log file */  
  handle = open(cmdline.inputs[0], O_RDONLY | O_LARGEFILE);

  /* Read the contents of the log file */
  while(true) {
    int msgtype, msgsize, channel;
    unsigned long long timestamp;

    /* Read the header information; return if done with file */
    if (!readHdr(handle, msgtype, msgsize, timestamp, channel)) return;

    /* Read in the message so that we can process the data */
    if (read(handle, (char*)pBuffer, msgsize) != msgsize) {
      if (verbose) 
	cerr << "Warning: reached end of file while reading message" << endl;
      if (!cmdline.auto_recover_flag) return;
    }

    /*
     * Process the data in the packet 
     *
     * The default action is to just print out the information in the
     * header.
     */
    cout << msgtype << " " << msgsize << " " << timestamp << " "
	 << channel << endl;
  }
}

int main(int argc, char **argv)
{
  /* Parse command line arguments */
  if (cmdline_parser(argc, argv, &cmdline) != 0) {
    exit(1);
  }

  /* Make sure that we got an input file */
  if (cmdline.inputs_num != 1) {
    cerr << "skynet-extract: wrong number of input arguments"
	 << " (" << cmdline.inputs_num << ")" << endl;
    cmdline_parser_print_help();
    exit(1);
  }

  /* Extract the data */
  extract(cmdline);
}
