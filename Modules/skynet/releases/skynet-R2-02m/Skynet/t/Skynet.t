# -*- perl -*-
# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl Skynet.t'

use lib "$ENV{HOME}/Spread-3.17.3-1.07/blib/arch";
use lib "$ENV{HOME}/Spread-3.17.3-1.07/blib/lib";
use Skynet;
use Data::Dumper 'Dumper';

my $sky = new Skynet;
#$sky->listen ([ qw(SNsegGoals SNtplannerStatus SNpolyCorridor SNocpParams
#                   SNstatePrecision SNtraj SNactuatorstate SNleadVehicleInfo
#                   SNadrive_command SNadrive_response SNstate) ] => sub {
$sky->listen (SNtraj => sub {
    my($self, $info) = @_;
    my $mess = $self->decode ($info);
    print "---- Type: ", $info->type, " ----\n";
    print Dumper($mess), "\n";
});
$sky->start;
