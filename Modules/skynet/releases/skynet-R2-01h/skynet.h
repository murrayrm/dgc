/*!
 * \file skynet.h
 * \brief Main header file for skynet API (C)
 *
 * \author Richard M. Murray
 * \date 23 July 2007
 *
 */

#include <sp.h>
#include <pthread.h>			/* Process thread library */

#define SKYNET_MAXGROUPS 64		/* Max number of groups */

/*! Connection to a skynet server */
typedef struct skynet_server {
  int skynet_key;			/* Skynet key for this connection */
  int module_id;			/* Module identification number */
  mailbox spread_mbox;			/* Spread mailbox */
  char *data;				/* Data buffer for incoming messages */
  int quit;				/* Flag to cause skynet_main to end */

  /* List of skynet groups that we are subscribed to */
  struct skynet_group *groups[SKYNET_MAXGROUPS];
  int ngroups;				/* Number of groups in use */
} skynet_server_t;

/*! Skynet message group */
typedef struct skynet_group {
  skynet_server_t *server;		/* Skynet server information */
  mailbox spread_mbox;			/* Spread mailbox */
  int msgtype;				/* Message type */
  int channel;				/* Message channel number */
  char *name;				/* Spread group name */
  char *msgbuf;				/* Buffer for storing latest message */
  int buflen;				/* Length of message buffer */
  int msglen;				/* Length of last received message */
  int (*handler)(char *, int);		/* Callback function */
  pthread_mutex_t *mutex;		/* Mutex for blocking buffer access */
  pthread_cond_t *cond;			/* Pthread conditional variable */

  int msg_ready;			/* Message is ready to be read */
  int msg_count;			/* Number of messages received */
} skynet_group_t;

/*! Maximum length of a skynet message */
#define SKYNET_MAX_MESSAGE_LENGTH 0x10000

/* Function declarations */
#ifdef __cplusplus
extern "C"
{
#endif

extern skynet_server_t *skynet_connect(int, int);
extern int skynet_disconnect(skynet_server_t *);
extern skynet_group_t *skynet_listen(skynet_server_t *, int, char *, int,
				     int (*)(char *, int),
				     pthread_mutex_t *, pthread_cond_t *, int);
extern int skynet_ignore(skynet_group_t *);
extern void skynet_main(skynet_server_t *);
extern void *skynet_main_thread(void *);
extern int skynet_get_message(skynet_group_t *, char *, int);
extern int skynet_get_next(skynet_group_t *, char *, int);
extern int skynet_get_latest(skynet_group_t *, char *, int);
extern int skynet_send_message(skynet_server_t *, int, char *, int, int);

#ifdef __cplusplus
}
#endif

/* Define some alternative forms for calling skynet functions */

/* Use buffer for receive messages */
#define skynet_listen_buffered(srv, typ, buf, len, chn)	\
  skynet_listen(srv, typ, buf, len, NULL, NULL, NULL, chn)
