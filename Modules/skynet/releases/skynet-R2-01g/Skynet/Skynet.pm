use Class::Struct;
struct "Skynet::MessageInfo" => { timestamp => '$',
                                  typeid    => '$',
                                  type      => '$',
                                  channel   => '$',
                                  message   => '$' };

package Skynet;

use 5.008008;
use strict;
use warnings;
use Skynet::Parse;
use Skynet::Spread qw(:MESS $sperrno);
use Carp;
use Time::HiRes qw(time sleep);

require Exporter;

our @ISA = qw(Exporter);

################################################################################

# Each of these serves as a number-to-name mapping as well as a list of the names.

our @MessageTypes = qw(
                       SNpointcloud
                       SNdeltamap
                       SNdrivecmd
                       SNdrivecom
                       SNactuatorstate
                       SNmodlist
                       skynetcom
                       rmulti
                       SNtraj
                       SNstate
                       SNgetmetastate
                       SNmetastate
                       SNGuiMsg_deprecated
                       SNmodcom_deprecated
                       SNRDDFtraj
                       SNroadtraj
                       SNplannertraj
                       SNmodemantraj_deprecated
                       SNtrajPlannerSeed
                       SNtrajPlannerInterm
                       SNreactiveTraj
                       SNfusiondeltamap
                       SNladardeltamap
                       SNstereodeltamap
                       SNstaticdeltamap
                       SNroad2map
                       SNtimberstring_deprecated
                       SNmark1
                       SNmark2
                       SNladardeltamap_roof
                       SNladardeltamap_bumper
                       SNfullmaprequest
                       SNladarmeas_roof
                       SNladarmeas_bumper
                       SNplannerTabInput
                       SNplannerTabOutput
                       SNSampleTabOutput
                       SNSampleTabInput
                       SNSamplePlotInput
                       SNreactiveTabOutput
                       SNreactiveTabInput
                       SNadrive_command
                       SNadrive_response
                       SNtrajfollowerTabOutput_deprecated
                       SNtrajfollowerTabInput_deprecated
                       SNasimActuatorState
                       SNastateTabInput_deprecated
                       SNfusionmapperTabOutput_deprecated
                       SNfusionmapperTabInput_deprecated
                       SNladarfeederTabOutput_deprecated
                       SNladarfeederTabInput_deprecated
                       SNladarmeas
                       SNsuperconMessage
                       SNsuperconResume
                       SNsuperconReverse
                       SNfusionElevDeltaMap
                       SNladarDeltaMapFused
                       SNladarDeltaMapStdDev
                       SNtrajReverse
                       SNladarDeltaMapNum
                       SNfusionDeltaMapStdDev
                       SNguiToTimberMsg
                       SNselectorTraj
                       SNcheckerTraj
                       SNstereoDeltaMapMean
                       SNstereoDeltaMapFused
                       SNsuperconTrajfCmd
                       SNtrajFstatus
                       SNtrajFspeedCapCmd
                       SNlowResSpeedDeltaMap
                       SNsuperConMapAction
                       SNsuperconPlnCmd
                       SNsuperconAstateCmd
                       SNladarRoofDeltaMap
                       SNladarRoofDeltaMapCost
                       SNladarRoofDeltaMapElev
                       SNladarRoofDeltaMapStdDev
                       SNladarRoofDeltaMapNum
                       SNladarRoofDeltaMapPitch
                       SNladarSmallDeltaMap
                       SNladarSmallDeltaMapCost
                       SNladarSmallDeltaMapElev
                       SNladarSmallDeltaMapStdDev
                       SNladarSmallDeltaMapNum
                       SNladarSmallDeltaMapPitch
                       SNladarRieglDeltaMap
                       SNladarRieglDeltaMapCost
                       SNladarRieglDeltaMapElev
                       SNladarRieglDeltaMapStdDev
                       SNladarRieglDeltaMapNum
                       SNladarRieglDeltaMapPitch
                       SNladarBumperDeltaMap
                       SNladarBumperDeltaMapCost
                       SNladarBumperDeltaMapElev
                       SNladarBumperDeltaMapStdDev
                       SNladarBumperDeltaMapNum
                       SNladarBumperDeltaMapPitch
                       SNladarFrontDeltaMap
                       SNladarFrontDeltaMapCost
                       SNladarFrontDeltaMapElev
                       SNladarFrontDeltaMapStdDev
                       SNladarFrontDeltaMapNum
                       SNladarFrontDeltaMapPitch
                       SNstereoShortDeltaMap
                       SNstereoLongDeltaMap
                       SNstereoShortDeltaMapElev
                       SNstereoShortDeltaMapStdDev
                       SNstereoShortDeltaMapCost
                       SNstereoLongDeltaMapCost
                       SNstereoLongDeltaMapElev
                       SNstereoLongDeltaMapStdDev
                       SNdeltaMapCorridor
                       SNsuperConMapDelta
                       SNmovingObstacle
                       SNbumperLadarNED
                       SNsegGoals
                       SNrddf
                       SNglobalGloNavMapFromMission
                       SNglobalGloNavMapFromGloNavMapLib
                       SNlocalGloNavMap
                       SNglobalGloNavMapRequest
                       SNlocalGloNavMapRequest
                       SNtplannerStatus
                       SNdplannerStatus
                       SNdeltaElevationMap
                       SNtrafficLocalMap
                       SNgimbalCommand
                       SNroadLine
                       SNladarRoofRightDeltaMap
                       SNladarRoofRightDeltaMapCost
                       SNladarRoofRightDeltaMapElev
                       SNladarRoofRightDeltaMapStdDev
                       SNladarRoofRightDeltaMapNum
                       SNladarRoofRightDeltaMapPitch
                       SNladarRoofLeftDeltaMap
                       SNladarRoofLeftDeltaMapCost
                       SNladarRoofLeftDeltaMapElev
                       SNladarRoofLeftDeltaMapStdDev
                       SNladarRoofLeftDeltaMapNum
                       SNladarRoofLeftDeltaMapPitch
                       SNladarBumpRightDeltaMap
                       SNladarBumpRightDeltaMapCost
                       SNladarBumpRightDeltaMapElev
                       SNladarBumpRightDeltaMapStdDev
                       SNladarBumpRightDeltaMapNum
                       SNladarBumpRightDeltaMapPitch
                       SNladarBumpLeftDeltaMap
                       SNladarBumpLeftDeltaMapCost
                       SNladarBumpLeftDeltaMapElev
                       SNladarBumpLeftDeltaMapStdDev
                       SNladarBumpLeftDeltaMapNum
                       SNladarBumpLeftDeltaMapPitch
                       SNladarRearDeltaMap
                       SNladarRearDeltaMapCost
                       SNladarRearDeltaMapElev
                       SNladarRearDeltaMapStdDev
                       SNladarRearDeltaMapNum
                       SNladarRearDeltaMapPitch
                       SNladarmeas_bumpleft
                       SNladarmeas_bumpright
                       SNladarmeas_roofleft
                       SNladarmeas_roofright
                       SNladarmeas_rear
                       SNmapElement
                       SNtplannerStaticCostMap
                       SNtplannerStaticCostMapRequest
                       SNocpParams
                       SNocpObstacles
                       SNstatePrecision
                       SNdplStatus
                       SNprocessRequest
                       SNprocessResponse
                       SNastateHealth
                       SNroaFlag
                       SNpolyCorridor
                       SNcorridorCreate
                       SNcorridorStatus
                       SNtrajCreate
                       SNtrajCreateStatus
                       SNtrajPause
                       SNtrajPauseStatus
                       SNtrajEndMission
                       SNtrajEndMissionStatus
                       SNbitmapParams
                       SNptuCommand
                       SNleadVehicleInfo
                       last_type
                      );

# SegGoals::SegmentType
our @SegmentType = qw(
    ROAD_SEGMENT
    PARKING_ZONE
    INTERSECTION
    PREZONE
    UTURN
    PAUSE
    DRIVE_AROUND
    BACK_UP
    END_OF_MISSION
    UNKNOWN
);

# SegGoals::IntersectionType
our @IntersectionType = qw(
    INTERSECTION_STRAIGHT
    INTERSECTION_LEFT
    INTERSECTION_RIGHT
);


# GcInterfaceDirectiveStatus::Status
our @GcStatus = qw(
    ACCEPTED
    REJECTED
    COMPLETED
    FAILED
    PENDING
);

# SegGoalsStatus::ReasonForFailure
our @TplannerFailure = qw(
    OBSTACLES
    KNOWLEDGE
    OUTSIDE_CORRIDOR
);

# AdriveActuator
our @AdriveActuator = qw(
    Steering
    Acceleration
    Transmission
    Ignition
    TurnSignal
);

# AdriveCommandType
our @AdriveCommandType = qw(
    SetPosition
    Reset
    Pause
    ReleasePause
    Disable
);

# AdriveFailure
our @AdriveFailure = qw(
    NotInitialized
    OutOfRange
    VehiclePaused
    VehicleNotStopped
    CommunicationError
    InitializationError
);

# EstopStatus
our @EstopStatus = qw(
    EstopDisable
    EstopPause
    EstopRun
);

our (%MessageTypes, %SegmentType, %IntersectionType, %GcStatus, %TplannerFailure,
     %AdriveActuator, %AdriveCommandType, %AdriveFailure, %EstopStatus);

our @ConstantLists = qw(MessageTypes SegmentType IntersectionType GcStatus TplannerFailure
                        AdriveActuator AdriveCommandType AdriveFailure EstopStatus);
our @ConstantRefsA = (\@MessageTypes, \@SegmentType, \@IntersectionType, \@GcStatus, \@TplannerFailure,
                      \@AdriveActuator, \@AdriveCommandType, \@AdriveFailure, \@EstopStatus);
our @ConstantRefsH = (\%MessageTypes, \%SegmentType, \%IntersectionType, \%GcStatus, \%TplannerFailure,
                      \%AdriveActuator, \%AdriveCommandType, \%AdriveFailure, \%EstopStatus);

# Generate name-to-number mappings, both hash...
for my $list (@ConstantLists) {
    my($i) = 0;
    no strict 'refs';
    %$list = map { $_ => $i++ } @$list;
}

# ... and constant.

sub constant {
    my($const) = shift;
    for my $ref (@ConstantRefsH) {
        if (exists $ref->{$const}) {
            return $ref->{$const};
        }
    }
    croak "Constant `$const' not defined";
}

sub AUTOLOAD {
    my $constname;
    our $AUTOLOAD;
    ($constname = $AUTOLOAD) =~ s/.*:://;
    croak "&Skynet::constant not defined" if $constname eq 'constant';
    my $val = constant($constname);
    {
        no strict 'refs';
        *$AUTOLOAD = sub { $val };
    }
    goto &$AUTOLOAD;
}

################################################################################

our %EXPORT_TAGS = ( lookup => [ map { '@'.$_, '%'.$_ } @ConstantLists ],
                     const  => [ map { @$_ } @ConstantRefsA ] );
our @EXPORT_OK = ( @{$EXPORT_TAGS{const}}, @{$EXPORT_TAGS{lookup}} );
our @EXPORT = qw();
our $VERSION = '1.00';

################################################################################
# Public interface: constructor,

sub new {
    my($class, %opts) = @_;
    my($self) = {};
    $self->{CALLBACKS} = {};
    $self->{DONE} = 0;
    bless $self => $class;
    $self->connect (%opts) or return;

    return $self;
}

################################################################################
# type-lookup-er,

sub typelookup {
    my($self, $type) = @_;
    if ($type =~ /^\d+$/) {
        return $MessageTypes[$type];
    } else {
        return $MessageTypes{$type};
    }
}

################################################################################
# callback setter,

sub listen {
    my($self) = shift;

    my($typearg) = shift;
    $typearg = [ 0 .. last_type() ] if $typearg eq "*";
    my(@types);
    if (ref $typearg eq "ARRAY") {
        @types = @$typearg;
    } else {
        @types = ($typearg);
    }

    my($cb) = shift;
    croak "Non-code reference passed for callback" unless "CODE" eq ref $cb;

    my($channel) = shift || 0;
    my(@channels);
    if (ref $channel eq "ARRAY") {
        @channels = @$channel;
    } else {
        @channels = ($channel);
    }

    for my $type (@types) {
        $type = $MessageTypes{$type} if $type =~ /^SN/;
        unless (defined $type) {
            carp "Undefined message type passed to Skynet::listen()";
            next;
        }

        $self->{CALLBACKS}{$type} = $cb;

        for (@channels) {
            $self->subscribe ($type, $channel);
        }
    }
}

################################################################################
# event loop.

sub process_one {
    my($self, $type, $chan, $message, $time) = @_;

    return undef unless defined $type;
    return 1 unless exists $self->{CALLBACKS}{$type};
    $self->{CALLBACKS}{$type}($self, Skynet::MessageInfo->new (timestamp => $time,
                                                               typeid    => $type,
                                                               type      => $MessageTypes[$type],
                                                               channel   => $chan,
                                                               message   => $message));
    return 1;
}

sub start {
    my($self) = shift;

    carp "No message type callbacks set when Skynet was started" unless %{$self->{CALLBACKS}};

    while (!$self->{DONE}) {
        $self->process_one ($self->fetch()) or last;
    }
}

################################################################################
# Decodes some binary messages.

sub decode {
    my($self, $info, $symbolic) = @_;
    my $ret;
    $symbolic = 0 unless defined $symbolic;

    if ($info->typeid == SNsegGoals()) {
        $ret = Skynet::Parse::decode_SegGoals ($info->message);
        if ($symbolic) {
            $ret->{segmentType} = $SegmentType[$ret->{segmentType}];
            $ret->{intersectionType} = $IntersectionType[$ret->{intersectionType}];
        }
    }
    elsif ($info->typeid == SNtplannerStatus()) {
        $ret = Skynet::Parse::decode_SegGoalsStatus ($info->message);
        if ($symbolic) {
            $ret->{status} = $GcStatus[$ret->{status}];
            $ret->{failureReason} = $TplannerFailure[$ret->{failureReason}];
        }
    }
    elsif ($info->typeid == SNpolyCorridor()) {
        $ret = Skynet::Parse::decode_sendCorr ($info->message);
    }
    elsif ($info->typeid == SNocpParams()) {
        $ret = Skynet::Parse::decode_OCPparams ($info->message);
    }
    elsif ($info->typeid == SNstatePrecision()) {
        $ret = Skynet::Parse::decode_VehiclePrecisionPlus ($info->message);
    }
    elsif ($info->typeid == SNtraj()) {
        $ret = Skynet::Parse::decode_CTraj ($info->message);
    }
    elsif ($info->typeid == SNactuatorstate()) {
        $ret = Skynet::Parse::decode_ActuatorState ($info->message);
        if ($symbolic) {
            $ret->{estop}{status} = $EstopStatus[$ret->{estop}{status}];
        }
    }
    elsif ($info->typeid == SNleadVehicleInfo()) {
        $ret = Skynet::Parse::decode_leadVehInfo ($info->message);
    }
    elsif ($info->typeid == SNadrive_command()) {
        $ret = Skynet::Parse::decode_AdriveDirective ($info->message);
        if ($symbolic) {
            $ret->{actuator} = $AdriveActuator[$ret->{actuator}];
            $ret->{command}  = $AdriveCommandType[$ret->{command}];
        }
    }
    elsif ($info->typeid == SNadrive_response()) {
        $ret = Skynet::Parse::decode_AdriveResponse ($info->message);
        if ($symbolic) {
            $ret->{status}        = $GcStatus[$ret->{status}];
            $ret->{failureReason} = $AdriveFailure[$ret->{failureReason}];
        }
    }
    elsif ($info->typeid == SNstate()) {
        $ret = Skynet::Parse::decode_VehicleState ($info->message);
    }

    return $ret;
}

sub decode_symbolic {
    decode ($_[0], $_[1], 1);
}

################################################################################
# Stops the event loop.

sub finish {
    my($self) = shift;

    $self->{DONE} = 1;
}

################################################################################
# Spread-specific funcs (private):

sub connect {
    my($self, %opts) = @_;
    my($spread_addr) = $ENV{SPREAD_DAEMON} || $opts{SpreadDaemon} || "4803\@192.168.0.59";
    $self->{KEY} = $ENV{SKYNET_KEY} || $opts{SkynetKey} || 0;
    $self->{KEY} = int rand 100_000 unless $self->{KEY} < 100000;
    @{$self}{qw(MBOX PRIVGROUP)} = Skynet::Spread::connect {
        spread_name => $spread_addr,
        private_name => "skynet-$self->{KEY}-$$",
    };
    $self->{GROUPS} = {};
    unless (defined $self->{MBOX}) { # Spread error - get outta there
        carp "Error connecting to Spread ($sperrno)";
        return;
    }
    return 1;
}

sub groupname {
    my($self, $type, $channel) = @_;
    $channel = 0 unless defined $channel;
    $type = $MessageTypes{$type} if $type =~ /^SN/;
    return sprintf "sn:%05d:%05d:%05d", $self->{KEY}, $type, $channel;
}

sub subscribe {
    my($self, $type, $channel) = @_;
    my($group) = $self->groupname ($type, $channel);
    $self->{GROUPS}{$group} = 1;
    Skynet::Spread::join $self->{MBOX}, $group;
}

sub fetch {
    my($self) = @_;
    my($stype, $sender, $groups, $mtype, $endmis, $message);

    do {
        ($stype, $sender, $groups, $mtype, $endmis, $message) = Skynet::Spread::receive $self->{MBOX};
    } until (!defined $stype or $stype & REGULAR_MESS); # we ignore membership messages

    return unless defined $stype;

    carp "Multi-group message (@$groups) - all groups discarded except first" if scalar @$groups > 1;

    for (@$groups) {
        my($key, $type, $chan) = ($groups->[0] =~ /^sn:(\d+):(\d+):(\d+)$/);
        unless ($key == $self->{KEY} and defined $type and defined $chan) {
            warn "Message received from an invalid group (".$groups->[0].")";
            return;
        }

        return (int $type, int $chan, $message, time());
    }
    return undef;
}

################################################################################
# Disconnects from Spread (harmless if not connected).

sub DESTROY {
    my($self) = shift;
    if (defined $self->{MBOX}) {
        Skynet::Spread::leave $self->{MBOX}, $_ for keys %{$self->{GROUPS}};
        Skynet::Spread::disconnect $self->{MBOX};
    }
}

################################################################################
################################################################################
# Class that does the above, but from a log file.

package Skynet::Logged;
use Skynet::Spread qw(:MESS);
use Carp;
use Fcntl qw(:seek);

our @ISA = qw(Skynet);

sub connect {
    my($self, %opts) = @_;
    croak "You must specify a log file name" unless exists $opts{Log};
    open $self->{LOG}, "<", $opts{Log} or croak "Opening $opts{Log}: $!";
    $self->{PLAYBACK} = $opts{Playback} || 0;
    $self->{SPEED} = $opts{Speed} || 1.0;
    $self->{POS} = 0;
    $self->{OFFSETS}{0} = 0;

    if ($self->{PLAYBACK}) {
        # Get a Spread connection.
        my($spread_addr) = $ENV{SPREAD_DAEMON} || $opts{SpreadDaemon} || "4803\@192.168.0.59";
        $self->{KEY} = $ENV{SKYNET_KEY} || $opts{SkynetKey} || 0;
        @{$self}{qw(MBOX PRIVGROUP)} = Skynet::Spread::connect {
            spread_name => $spread_addr,
            private_name => "skynet-player-$self->{KEY}-$$",
        };
        for (0 .. last_type()) {
            $self->subscribe ($_, 0);
        }
    }
}

sub fetch {
    my($self) = shift;
    my($bytes);

    if (defined $self->{MBOX}) { # if we're playing back, don't get bogged down by other things
        1 while Skynet::Spread::receive $self->{MBOX}, 0;
    }

    if ($self->{POS} % 1000 == 0) {
        $self->{OFFSETS}{$self->{POS}} = sysseek $self->{LOG}, 0, SEEK_CUR;
    }

    sysread $self->{LOG}, $bytes, 16 or return undef;
    length $bytes == 16 or croak "Truncated logfile (16-byte header only read ".length($bytes)." bytes)";

    my($ts, $type, $size) = unpack "QII", $bytes;
    croak "Corrupt logfile" unless defined $ts and defined $type and defined $size;

    my($message);
    sysread $self->{LOG}, $message, $size or croak "I/O error or premature end of file reading log";
    croak "Truncated logfile (wanted $size bytes, got ".length($message).")"
         unless length($message) == $size;

    $self->{POS}++;

    return ($type, 0, $message, $ts);
}

sub seek {
    my($self, $msgnr, $whence) = @_;
    $msgnr = 0 unless defined $msgnr;
    $whence = SEEK_SET unless defined $whence;

    my($marker) = $msgnr - ($msgnr % 1000);
    while (!exists $self->{OFFSETS}{$marker} and $marker >= 0) {
        $marker -= 1000;
    }

    if ($self->{POS} >= $marker) { # just seeking forward some
        1 while $self->{POS} < $msgnr and defined $self->fetch();
        return $self->{POS} < $msgnr;
    }

    my($startloc) = $self->{OFFSETS}{$marker} || 0;

    $! = 0;
    sysseek $self->{LOG}, $startloc, SEEK_SET;
    if ($! != 0) {
        croak "Log file does not support seeking: $!\n";
    }

    my($startpos) = $marker || 0;
    $self->{POS} = $startpos;
    1 while $self->{POS} < $msgnr and defined $self->fetch();
}

sub blast {
    my($self, $nmess) = @_;
    $nmess = 1 unless defined $nmess;
    my($blasted) = 0;
    # $nmess < 0 means "go till the end of the log".
    while ($blasted < $nmess and $nmess > 0) {
        $self->process_one ($self->fetch()) or last;
    }
}

sub run {
    my($self) = shift;

    $self->{DONE} = 0;
    my($type, $chan, $message, $ts) = $self->fetch();
    my($tmstart, $tsstart) = (time(), $ts); # difference between log-time and system-time
    while (!$self->{DONE}) {
        last unless defined $type;

        my($tm) = time();
        my($diffsim, $diffreal) = ($ts - $tsstart, ($tm - $tmstart) * $self->{SPEED});
        sleep ($diffsim - $diffreal) if $diffsim - $diffreal > 0.01;

        $self->process_one ($type, $chan, $message, $ts);
    } continue {
        ($type, $chan, $message, $ts) = $self->fetch();
    }
}

sub process_one {
    my($self, $type, $chan, $message, $ts) = @_;
    if ($self->{PLAYBACK} and defined $self->{MBOX}) {
        my($group) = $self->groupname ($type, $chan);
        Skynet::Spread::multicast $self->{MBOX}, FIFO_MESS, $group, 0, $message;
    }
    $self->SUPER::process_one (@_);
}

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Skynet - Perl extension for blah blah blah

=head1 SYNOPSIS

  use Skynet;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for Skynet, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Joshua Oreman, E<lt>oremanj@homeE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007 by Joshua Oreman

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
