function [type, count] = skynet_type_count(data, varargin)
% SKYNET_TYPE_COUNT	Count the number of messages of each type
%
% SKYNET_TYPE_COUNT(data) returns a list of all data types that were
% found in the records and the number of records for each.  This list 
% is sorted by message type.

type = unique(data(:,1));

% Go through the list and count the number of messages of each type
for i = 1:length(type)
  count(i,1) = nnz(data(:,1) == type(i));
end

% Summarize what we read
fprintf(1, 'Found %d unique types\n', length(type));
