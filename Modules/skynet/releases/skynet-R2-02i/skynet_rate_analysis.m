function [time, msgs, bytes] = skynet_rate_analysis(data, delta, varargin)
% SKYNET_RATE_ANALYSIS	Analyze data rates in a log file
%
% SKYNET_RATE_ANALYSIS(data, delta) will analyze a data set and return
% the number of messages and number of bytes sent in uniformly spaced
% time windows of length 'delta'.
%
% SKYNET_RATE_ANALYSIS(data, delta, tlist, clist) will analyze data
% that whose type and channel are in the optional lists 'tlist' and
% 'clist'.  If one or both are not specified, all types or channels are
% listed.

%
% Written by Richard Murray, 20 August 2007
%

% Figure out what message types to look at
type = [];
if (nargin > 2) 
  type = varargin{1};
end;

% Figure out what channels to look for
channel = [];
if (nargin > 3)
  channel = varargin{2};
end;

% Initialize variables that keep track of windows
bin = 0;
tend = 0;

% Go through data and count the messages in each time slice
for i = 1:length(data)
  % See if it is time to increment the bin number
  while (data(i,3) >= tend)
    bin = bin+1;			% move to the next bin
    time(bin) = tend;			% keep track of start time for bin
    msgs(bin) = 0;			% initialize message count
    bytes(bin) = 0;			% initialize byte count
    tend = tend + delta;		% update end time for window
  end
  
  % See if this data element is one we should look at
  if (length(type) == 0 || sum(type == data(i,1)) > 0)
    if (length(channel) == 0 || sum(channel == data(i,4)) > 0)
      % Increment counters for this bin
      msgs(bin) = msgs(bin) + 1;
      bytes(bin) = bytes(bin) + data(i, 2);
    end
  end
end
