/*!
 * \file skynet-extract
 * \brief Program to extract information from skynet log files
 *
 * \author Richard M. Murray
 * \date 19 August 2007
 *
 */

#include <iostream>
#include <fcntl.h>
#include <stdlib.h>
using namespace std;

#include "interfaces/sn_types.h"
#include "skynet.h"
#include "cmdline_skynet_player.h"

#ifdef MACOSX
#define O_LARGEFILE 0x00	// not defined (needed?) in OS X
#endif

/* Read header from log file */
bool readHdr(int handle, int& msgtype, int& msgsize,
	     unsigned long long& timestamp)
{
  if (read(handle, (char*)&timestamp, sizeof(timestamp)) != sizeof(timestamp) ||
      read(handle, (char*)&msgtype, sizeof(msgtype)) != sizeof(msgtype) ||
      read(handle, (char*)&msgsize, sizeof(msgsize)) != sizeof(msgsize)) {
    return false;
  }

  if (msgtype < 0 || msgtype >= last_type)
    cerr << "Warning: unknown message type " << msgtype << endl;
  return true;
}

/* Extract information from a log file */
void extract(gengetopt_args_info &cmdline)
{
  int handle;
  char* pBuffer = new char[SKYNET_MAX_MESSAGE_LENGTH];

  /* Open the log file */  
  handle = open(cmdline.inputs[0], O_RDONLY | O_LARGEFILE);

  /* Read the contents of the log file */
  while(true) {
    int msgtype, msgsize;
    unsigned long long timestamp;

    /* Read the header information; return if done with file */
    if (!readHdr(handle, msgtype, msgsize, timestamp)) return;

    /* Read in the message so that we can process the data */
    if (read(handle, (char*)pBuffer, msgsize) != msgsize) {
      cerr << "Warning: reached end of file while reading message" << endl;
      return;
    }

    /*
     * Process the data in the packet 
     *
     * The default action is to just print out the information in the
     * header.
     */
    cout << msgtype << " " << msgsize << " " << timestamp << endl;
  }
}

int main(int argc, char **argv)
{
  /* Parse command line arguments */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) {
    exit(1);
  }

  /* Make sure that we got an input file */
  if (cmdline.inputs_num != 1) {
    cerr << "skynet-extract: wrong number of input arguments"
	 << " (" << cmdline.inputs_num << ")" << endl;
    cmdline_parser_print_help();
    exit(1);
  }

  /* Extract the data */
  extract(cmdline);
}
