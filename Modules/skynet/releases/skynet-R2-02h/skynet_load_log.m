function data = skynet_load_log(file, varargin)
% SKYNET_LOAD_LOG   Load a skynet log file
%
% SKYNET_LOAD_LOG('file') loads a skynet log file that has been
% processed by skynet-extract and creates a data file with each
% message that was sent.

%
% Written by Richard Murray, 20 August 2007
%

% Load the data from a file
data = load(file);

% Process the data so that units are more useful
unixstart = data(1,3);
data(:,3) = (data(:,3) - unixstart) / 1000000;	% time in secs, starting at 0

% Summarize what we read
fprintf(1, 'Read %d records covering %g seconds\n', length(data), ...
  data(length(data), 3));

% Figure out what the starting time was
daytime = mod(unixstart, 24*60*60);
hours = floor(daytime/60/60);
mins = floor(mod(daytime, 60*60) / 60);
secs = floor(mod(daytime, 60));
fprintf(1, 'Log start: %02d:%02d:%02d\n', hours, mins, secs);
