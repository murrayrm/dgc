/*!
 * \file skynet.dox
 * \brief Main documentation page
 *
 * \author Richard M. Murray
 * \date 28 July 2007
 *
 * This file contains the mainpage documentation for the skynet module.
 *
 */

/*!
 * \mainpage Skynet Documentation
 * \section Introduction
 *
 * Skynet is the message system used by Alice for modules to
 * communicate each other. It works by allowing modules to listen to
 * different message types. All messages of a given type can be read
 * by all modules listening for messages of that type. Alternatively,
 * it is possible to create channels of one message type, such that
 * messages sent to one channel do not show up on others. Skynet is
 * implemented on top of the Spread group communications system. This
 * page discusses only the skynet interface; details of how to
 * configure Spread are described elsewhere.
 *
 * Main functions:
 * <ul>
 * <li> @ref skynet_connect - connect to server
 * <li> @ref skynet_listen - list for message of a given type
 * <li> @ref skynet_get_message - read message when available
 * <li> @ref skynet_send_message - send a message to other processes
 * </ul>
 *
 * C++ classes:
 * <ul>
 * <li> @ref SkynetServer - Server connection
 * <li> @ref SkynetGroup - message type/channel
 * <li> @ref skynet - Version 2 interface (deprecated)
 * </ul>
 *
 * <a href="http://gc.caltech.edu/wiki/skynet">User documentation</a>
 * for @ref gcdrive is available on the Team Caltech wiki.
 * 
 */
