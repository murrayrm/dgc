
/* 
 * Desc: Camera interface
 * Date: 8 September 2006
 * Author: Steve Goldberg
 * CVS: $Id: bb_camera.c,v 1.18 2006/11/15 22:45:54 abhoward Exp $
*/

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <malloc.h>
#include <stdio.h>
#include <libdc1394/dc1394_control.h>

#ifdef __MMX__
#include <mmintrin.h>
#endif
#ifdef __SSE__
#include <xmmintrin.h>
#endif

#include "bb_camera.h"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Create object
bb_camera_t *bb_camera_alloc(const char *camera_string)
{
  bb_camera_t *self;

  self = (bb_camera_t*) calloc(1, sizeof(bb_camera_t));

  self->shutter_limit = 80;
  self->gain_p = 0.02;
  self->shutter_p = 0.001;

  assert(camera_string);
  if ((self->camera = vis_dev_alloc(camera_string, NULL)) == NULL)
  {
    ERROR("unable to create camera");
    free (self);
    return NULL;
  }

  return self;
}


// Destroy object
void bb_camera_free(bb_camera_t *self)
{
  free(self);
  return;
}


// Initialize camera 
int bb_camera_init(bb_camera_t *self, const char *config_string)
{
  // Configure libdc1394 camera via vis_dev
  assert(config_string);
  MSG("camera config string: %s", config_string);
  if (vis_dev_config(self->camera, config_string, strlen(config_string)))
    return ERROR("failed to configure camera with %s", config_string);
  
  if (vis_dev_img_header(self->camera, &(self->image)))
    return ERROR("failed to read image header");

  if (true)
  {
    dc1394_feature_info gain, shutter;

    // Get gain range
    dc_1394_command(self->camera, "gain", 4, &gain, sizeof(gain));

    // Get shutter range
    dc_1394_command(self->camera, "shutter", 4, &shutter, sizeof(shutter));

    if (gain.feature_id == FEATURE_GAIN &&
        shutter.feature_id == FEATURE_SHUTTER)
    {
      self->gain_min = gain.abs_min;
      self->gain_max = gain.abs_max;
      self->gain_value = gain.abs_value;

      MSG("gain range: %.0f %.0f %.0f",
          self->gain_min, self->gain_max, self->gain_value);

      // Turn off Auto gain
      bb_camera_set_gain(self, self->gain_value);
    
      self->shutter_min = shutter.abs_min*1000; // HACK
      self->shutter_max = shutter.abs_max*1000;
      self->shutter_value = shutter.abs_value*1000;

      MSG("shutter range: %.2f [%.2f %.2f]",
          self->shutter_value, self->shutter_min, self->shutter_max);

      // Turn off auto-shutter
      bb_camera_set_shutter(self, self->shutter_value);

      self->gain_valid = true;
    }
    else
    {
      self->gain_valid = false;
      MSG("Failed to read shutter and/or gain bounds");
    }
  }
  
  // Start capturing
  vis_dev_start(self->camera);

  MSG("capture enabled");

  return 0;
}


// Stop camera
int bb_camera_fini(bb_camera_t *self)
{
  MSG("capture disabled");
  vis_dev_stop(self->camera);
  vis_dev_free(self->camera);

  return 0;
}


// Capture image pair
int bb_camera_capture(bb_camera_t *self, jplv_image_t *raw_image)
{
  self->image.pixels = raw_image->data;

  vis_dev_read_img(self->camera, &(self->image));

  self->timestamp =
    (uint64_t) self->image.fill_time.tv_sec * 1000000 +
    (uint64_t) self->image.fill_time.tv_usec;    
  self->num_frames++;
  
  return 0;
}


// Simple de-mux from interlevaed monochrome images
int bb_camera_demux_mono(const jplv_image_t *src, int level, int *exp_level,
                         jplv_image_t *left, jplv_image_t *right)
{
  int i, j;
  const uint8_t *src0, *src1, *src2, *src3;
  uint8_t *dst1, *dst2;
  unsigned long exp, exp_count;

  //MSG("level %d %dx%d %dx%d", level, src->cols, src->rows, left->cols, left->rows);
    
  if (src->rows >> level != left->rows ||
      src->cols >> level != left->cols)
    return -1;

  src0 = src->data;
  src1 = src0 + src->cols*2;
  src2 = src1 + src->cols*2;
  src3 = src2 + src->cols*2;
  dst1 = left->data;
  dst2 = right->data;
  
  exp = 0;
  exp_count = src->cols >> level;
  exp_count *= src->cols >> level;
  if (level == 0)
  {
    for (j = 0; j < src->rows; j++)
    {
#ifdef __MMX__
      __m64 mm7 = _mm_setzero_si64();
      for (i = 0; i < src->cols; i+=1*8)
      {
        __m64 mm0, mm1, mm2, mm3;
        // mm0 = 0 1 2 3 4 5 6 7    
        // mm1 = 8 9 A B C D E F
        mm0 = *(__m64 *)src0;
        mm1 = *(__m64 *)(src0 + 8);

        // mm2 = 0 8 1 9 2 A 3 B
        // mm3 = 4 C 5 D 6 E 7 F
        mm2 = _mm_unpacklo_pi8(mm0, mm1);
        mm3 = _mm_unpackhi_pi8(mm0, mm1);

        // mm0 = 0 4 8 C 1 5 9 D
        // mm1 = 2 6 A E 3 7 B F
        mm0 = _mm_unpacklo_pi8(mm2, mm3);
        mm1 = _mm_unpackhi_pi8(mm2, mm3);

        // dst1 = 0 2 4 6 8 A C E
        // dst2 = 1 3 5 7 9 B D F
        *(__m64 *)dst1 = mm2 = _mm_unpacklo_pi8(mm0, mm1);
        *(__m64 *)dst2 = mm3 = _mm_unpackhi_pi8(mm0, mm1);

        mm0 = _mm_sad_pu8(mm2, mm7);
        mm1 = _mm_sad_pu8(mm3, mm7);
        exp += _mm_cvtsi64_si32(_mm_add_pi32(mm0, mm1));
        src0 += 2*8;
        dst1 += 1*8;
        dst2 += 1*8;
      }
#else
      for (i = 0; i < src->cols; i++)
      {
        exp += *dst1 = src0[0];
        exp += *dst2 = src0[1];
        src0 += 2;
        dst1 += 1;
        dst2 += 1;
      }
#endif
    }
  }
  else if (level == 1)
  {
    for (j = 0 ; j < src->rows /2 ; j++)
    {
      for (i = 0 ; i < src->cols /2 ; i++)
      {
        exp += *dst1 = (src0[0] + src0[2] + src1[0] + src1[2])>>2;
        exp += *dst2 = (src0[1] + src0[3] + src1[1] + src1[3])>>2;
        src0 += 4;
        src1 += 4;
        dst1 += 1;
        dst2 += 1;
      }
      src0 = src1;
      src1+= src->cols*2;
    }
  }
  else if (level == 2)
  {
    for (j = 0 ; j < src->rows /4 ; j++)
    {
      for (i = 0 ; i < src->cols /4 ; i++)
      {
#if defined __MMX__ && defined __SSE__
        __m64 m0, m1, m2, m3;
        unsigned char __attribute__ ((aligned (16))) avgs[8];
        m0 = *(__m64 *)src0;
        m1 = *(__m64 *)src1;
        m2 = *(__m64 *)src2;
        m3 = *(__m64 *)src3;

        m0 = _mm_avg_pu8(m0, m1);
        m2 = _mm_avg_pu8(m2, m3);
        m0 = _mm_avg_pu8(m0, m2);

        *(__m64*)avgs = m0;

        exp += *dst1 = (avgs[0] + avgs[2] + avgs[4] + avgs[6]) >> 2;
        exp += *dst2 = (avgs[1] + avgs[3] + avgs[5] + avgs[7]) >> 2;
#else

        exp += 
        *dst1 = (src0[0] + src0[2] + src0[4] + src0[6] +
                 src1[0] + src1[2] + src1[4] + src1[6] +
                 src2[0] + src2[2] + src2[4] + src2[6] +
                 src3[0] + src3[2] + src3[4] + src3[6]) >> 4;
        exp+= 
        *dst2 = (src0[1] + src0[3] + src0[5] + src0[7] +
                 src1[1] + src1[3] + src1[5] + src1[7] +
                 src2[1] + src2[3] + src2[5] + src2[7] +
                 src3[1] + src3[3] + src3[5] + src3[7]) >> 4;
#endif
        src0 += 8;
        src1 += 8;
        src2 += 8;
        src3 += 8;
        dst1 += 1;
        dst2 += 1;
      }
      src0+=src->cols*6;
      src1+=src->cols*6;
      src2+=src->cols*6;
      src3+=src->cols*6;
    }
  }
#ifdef __MMX__
  _mm_empty();
#endif

  if (exp_level != NULL)
    *exp_level = exp / exp_count;

  return 0;
}


// Set the gain
int bb_camera_set_gain(bb_camera_t *self, float gain)
{
  char command[32];

  if (gain < self->gain_min)
    gain = self->gain_min;
  if (gain > self->gain_max)
    gain = self->gain_max;

  self->gain_value = gain;
  
  snprintf(command, sizeof(command), "%fdb", self->gain_value);
  if (dc_1394_config(self->camera, command, 0))
    ERROR("dc_1394_config(%s) failed", command);
  return 0;
}


// Set the shutter speed
int bb_camera_set_shutter(bb_camera_t *self, float shutter)
{
  char command[32];
    
  if (shutter < self->shutter_min)
    shutter = self->shutter_min;
  if (shutter > self->shutter_max)
    shutter = self->shutter_max;
  
  self->shutter_value = shutter;

  snprintf(command, sizeof(command), "%fms", self->shutter_value);
  if (dc_1394_config(self->camera, command, 0))
    ERROR("dc_1394_config(%s) failed", command);

  return 0;
}


// Auto-gain on the exposure
int bb_camera_auto_gain(bb_camera_t *self, int goal, int meas)
{
  float err, step, gain, shutter;

  if (!self->gain_valid)
    return 0;
  
  // Compute current exposure value
  err = goal - meas;

  shutter = self->shutter_p * err; 

  // Limit the rate at which the shutter changes
  step = (self->shutter_max - self->shutter_min) * 0.01;
  //MSG("%f %f", shutter, step);
  if (shutter > +step)
    shutter = +step;
  if (shutter < -step)
    shutter = -step;  
  shutter = self->shutter_value + shutter;

  if (shutter > self->shutter_limit)
    shutter = self->shutter_limit;
  
  gain = self->gain_p * err;

  // Limit the rate at which the gain changes
  step = (self->gain_max - self->gain_min) * 0.05;
  if (gain > +step)
    gain = +step;
  if (gain < -step)
    gain = -step;
  gain = self->gain_value + gain;

  bb_camera_set_gain(self, gain);
  bb_camera_set_shutter(self, shutter);
      
  //MSG("exposure %.0d %.0d %.3f %.3f %.3f %.3f %.3f",
  //    goal, meas, err,
  //    shutter, self->shutter_value, gain, self->gain_value);
    
  return 0;
}
