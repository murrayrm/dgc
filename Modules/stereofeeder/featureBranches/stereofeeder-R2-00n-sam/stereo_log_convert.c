
/* Desc: Convert stereo logs
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 *
 * Build with: gcc -o stereo-log-convert -I../../include stereo_log_convert.c -L../../lib/i486-gentoo-linux-static -lsensnetlog
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <frames/mat44.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/StereoImageBlob.h>

  

/// @brief Stereo Camera data
typedef struct 
{
  /// Camera calibration data: (cx, cy) is the image center in pixels,
  /// (sx, sy) is the focal length in pixels.
  float cx, cy, sx, sy;

  /// Camera-to-vehicle transformation (homogeneous matrix).
  float sens2veh[4][4];

  /// Camera gain setting (dB) 
  float gain;

  /// Camera shutter speed (ms)
  float shutter;
  
} __attribute__((packed)) StereoImageCameraV3;
  
  
/// @brief Stereo image blob.
///
/// Contains a rectified left/right stereo image pair and (optionally)
/// the left disparity image.  Disparity values are scaled up and
/// stored as 16-bit integers.
///
typedef struct
{  
  /// Blob type (must be SENSNET_STEREO_IMAGE_BLOB)
  int32_t blobType;

  /// Version number (must be STEREO_IMAGE_BLOB_VERSION)
  int32_t version;
  
  /// ID for originating sensor.
  int32_t sensorId;

  /// Unique frame id (increased monotonically)
  int32_t frameId;

  /// Image timestamp
  uint64_t timestamp;

  /// Left camera parameters
  StereoImageCameraV3 leftCamera;

  /// Right camera parameters
  StereoImageCameraV3 rightCamera;

  /// Stereo baseline (m).
  float baseline;

  /// Vehicle state data.  Note that the state data may not be
  /// perfectly sync'ed with the image data, and that the image
  /// timestamp may differ slightly from the state timestamp.
  VehicleState state;

  /// Vehicle-to-local transformation (homogeneous matrix)
  float veh2loc[4][4];

  /// Reserved for future use; must be all zero.
  uint32_t reserved[16];
  
  /// Image dimensions 
  int32_t cols, rows;

  /// Number of channels in rectified image (1 = grayscale, 3 = RGB)
  int32_t channels;
    
  /// Offset to start of left rectified image data
  uint32_t leftOffset;

  /// Size of left image data 
  uint32_t leftSize;

  /// Offset to start of right rectified image data
  uint32_t rightOffset;

  /// Size of right image data
  uint32_t rightSize;

  /// Disparity scaling factor
  float dispScale;

  /// Offset to start of disparity data
  uint32_t dispOffset;

  /// Size of disparity data
  uint32_t dispSize;
  
  /// Packed data for all images.  This is large enough to contain
  /// left and right rectified images (6 bytes/pixel) and the
  /// disparity image (2 bytes/pixel).
  uint8_t imageData[STEREO_IMAGE_BLOB_MAX_COLS * STEREO_IMAGE_BLOB_MAX_ROWS * 8];
  
} __attribute__((packed)) StereoImageBlobV3;



// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log self-test.
int main(int argc, char **argv)
{
  char *srcfilename, *dstfilename;
  sensnet_log_t *srclog, *dstlog;
  sensnet_log_header_t header;

  StereoImageBlobV3 src;
  StereoImageBlob dst;
    
  if (argc < 3)
  {
    printf("usage: %s <SRC> <DST>\n", argv[0]);
    return -1;
  }
  srcfilename = argv[1];
  dstfilename = argv[2];

  // Create source log
  srclog = sensnet_log_alloc();
  assert(srclog);
  MSG("opening source %s", srcfilename);
  if (sensnet_log_open_read(srclog, srcfilename, &header) != 0)
    return -1;

  // Create destination log
  dstlog = sensnet_log_alloc();
  assert(dstlog);
  MSG("opening source %s", dstfilename);
  if (sensnet_log_open_write(dstlog, dstfilename, &header) != 0)
    return -1;

  while (true)
  {
    uint64_t timestamp;
    int sensor_id, blob_type, blob_id, blob_size;
    int image_size;

    // Read blob
    blob_size = sizeof(src);
    if (sensnet_log_read(srclog, &timestamp, &sensor_id,
                         &blob_type, &blob_id, &blob_size, &src) != 0)
      return -1;

    MSG("read blob %d %d bytes", blob_id, blob_size);

    memset(&dst, 0, sizeof(dst));
        
    // Copy data
    dst.blobType = src.blobType;
    dst.version = STEREO_IMAGE_BLOB_VERSION;
    dst.sensorId = src.sensorId;
    dst.frameId = src.frameId;
    dst.timestamp = src.timestamp;

    dst.leftCamera.cx = src.leftCamera.cx;
    dst.leftCamera.cy = src.leftCamera.cy;
    dst.leftCamera.sx = src.leftCamera.sx;
    dst.leftCamera.sy = src.leftCamera.sy;
    mat44f_setf(dst.leftCamera.sens2veh, src.leftCamera.sens2veh);
    mat44f_inv(dst.leftCamera.veh2sens, src.leftCamera.sens2veh);
    dst.leftCamera.gain = src.leftCamera.gain;
    dst.leftCamera.shutter = src.leftCamera.shutter;

    dst.rightCamera.cx = src.rightCamera.cx;
    dst.rightCamera.cy = src.rightCamera.cy;
    dst.rightCamera.sx = src.rightCamera.sx;
    dst.rightCamera.sy = src.rightCamera.sy;
    mat44f_setf(dst.rightCamera.sens2veh, src.rightCamera.sens2veh);
    mat44f_inv(dst.rightCamera.veh2sens, src.rightCamera.sens2veh);
    dst.rightCamera.gain = src.rightCamera.gain;
    dst.rightCamera.shutter = src.rightCamera.shutter;

    dst.baseline = src.baseline;    
    dst.state = src.state;
    
    mat44f_setf(dst.veh2loc, src.veh2loc);
    mat44f_inv(dst.loc2veh, src.veh2loc);

    dst.cols = src.cols;
    dst.rows = src.rows;
    dst.channels = src.channels;
    
    dst.leftOffset = src.leftOffset;
    dst.leftSize = src.leftSize;
    dst.rightOffset = src.rightOffset;
    dst.rightSize = src.rightSize;
    dst.dispScale = src.dispScale;
    dst.dispOffset = src.dispOffset;
    dst.dispSize = src.dispSize;

    image_size = dst.leftSize + dst.rightSize + dst.dispSize;
    blob_size = sizeof(dst) - sizeof(dst.imageData) + image_size;

    memcpy(dst.imageData, src.imageData, image_size);

    MSG("writing %d %d %d", blob_id, blob_size, image_size);

    // Write new blob
    if (sensnet_log_write(dstlog, timestamp, sensor_id,
                          blob_type, blob_id, blob_size, &dst) != 0)
      return -1;
  }

  // Clean up
  sensnet_log_close(dstlog);
  sensnet_log_free(dstlog);
  sensnet_log_close(srclog);
  sensnet_log_free(srclog);
  
  return 0;
}

