
/* 
 * Desc: Stereo feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
  
#if USE_FB
#include <vis-tools/frame_buffer.h>
#endif

#include <jplv/jplv_stereo.h>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/StereoImageBlob.h>

#include <ncurses.h>
#include <cotk/cotk.h>

// Camera drivers
#include "StereoCamera.hh"
#include "SimStereoCamera.hh"
#include "BBStereoCamera.hh"
#include "PGRStereoCamera.hh"

// Raw image logging
#include "stereo_log.h"

#include "cmdline.h"


/// @brief Stereo feeder class
class StereoFeeder
{
  public:   

  /// Default constructor
  StereoFeeder();

  /// Default destructor
  ~StereoFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Parse the config file
  int parseConfigFile(const char *configPath);

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;

  // What mode are we in?
  enum {modeSim, modeBB, modePGR} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Is logging enabled?
  bool enableLog;

  public:
  
  /// Initialize camera
  int initCamera();
  
  /// Finalize camera
  int finiCamera();

  /// Capture a frame 
  int captureCamera();

  // Stereo camera driver
  StereoCamera *camera;

  // Pyramid level for downsampling input image
  int imageLevel;
  
  // Raw image dimensions
  int imageCols, imageRows, imageChannels;

  // Camera models
  jplv_cmod_t leftModel, rightModel;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];
  
  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;

  public:

  // Initialize image logging
  int initImageLog();

  /// Finalize image logging
  int finiImageLog();

  /// Write current data to image log
  int writeImageLog();

  // Raw image log
  stereo_log_t *imageLog;

  // Raw image log stats
  int imageLogCount, imageLogSize;
  
  public:

  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data over sensnet
  int writeSensnet();

  // SensNet handle
  sensnet_t *sensnet;

  // Sensnet log file name
  char logName[1024];

  // Sensnet logging stats
  int logCount, logSize;

  public:
  
  /// Initialize everything except capture
  int initStereo();

  /// Finalize everything except capture
  int finiStereo();
  
  /// Process a frame
  int process();
  
  /// Get the predicted vehicle state
  int getState(double timestamp);

  // Run fake stereo using ground-plane constraint
  int calcPlaneDisp();
  
  // Current state data
  VehicleState state;

  // Stereo handle
  jplv_stereo_t *stereo;

  // Current frame id
  int frameId;

  // Current frame time (microseconds)
  uint64_t frameTime;
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Stereo stats
  uint64_t stereoTime;

  public:
  
  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, StereoFeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, StereoFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, StereoFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserDisplay(cotk_t *console, StereoFeeder *self, const char *token);

  /// Console button callback
  static int onUserExport(cotk_t *console, StereoFeeder *self, const char *token);

  // Display current image to framebuffer
  int displayImage();

  // Write the current images to a file
  int saveImage();

  // Console text display
  cotk_t *console;

  // Current export frame id
  int exportId;

  // Display mode for the frame buffer (0 = none, 1 = left, 2 = right, 3 = disparity)
  int fbMode;
  
#if USE_FB
  // Console frame buffer
  struct frame_buffer_t *fb;
#endif

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
StereoFeeder::StereoFeeder()
{
  memset(this, 0, sizeof(*this));

  this->frameId = -1;

  return;
}


// Default destructor
StereoFeeder::~StereoFeeder()
{
  return;
}


// Parse the command line
int StereoFeeder::parseCmdLine(int argc, char **argv)
{  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Use raw image logging or sensnet logging, but not both
  if (this->options.enable_log_flag && this->options.enable_imagelog_flag)
    return ERROR("sensnet logging and image logging cannot be enabled at the same time");
  
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
    
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Fill out the default config path from the environment if not
  // given on the command line/
  if (!this->options.config_path_given)
  {
    char configPath[256];      
    if (getenv("DGC_CONFIG_PATH") == NULL)
      return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");
    snprintf(configPath, sizeof(configPath), "%s/stereofeeder", getenv("DGC_CONFIG_PATH"));
    this->options.config_path_arg = strdup(configPath);
  }
  
  // Load configuration file
  if (this->parseConfigFile(this->options.config_path_arg) != 0)
    return -1;

  return 0;
}


// Parse the config file
int StereoFeeder::parseConfigFile(const char *configPath)
{
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id (may be in config file)
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz);
  sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz);

  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  return 0;
}


// Initialize cameras
int StereoFeeder::initCamera()
{
  char filename[256];

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%s-left.cahvor",
           this->options.config_path_arg, this->options.left_camera_arg);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%s-right.cahvor",
           this->options.config_path_arg, this->options.right_camera_arg);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  if (this->options.sim_flag)
  {
    SimStereoCamera *simCamera;
    
    // Initialize simulated camera driver
    this->mode = modeSim;
    this->camera = simCamera = new SimStereoCamera();
    simCamera->open(this->leftModel.core.xdim, this->leftModel.core.ydim);
  }
  else 
  {
    // Create appropriate driver
    if (strcasecmp(this->options.driver_arg, "bb") == 0)
    {
      this->mode = modeBB;
      this->camera = new BBStereoCamera();
    }
    else if (strcasecmp(this->options.driver_arg, "pgr") == 0)
    {
      this->mode = modePGR;
      this->camera = new PGRStereoCamera();
    }
    else
      assert(false);

    // Initialize real camera
    if (this->camera->open(this->options.left_camera_arg, this->options.left_config_arg,
                           this->options.right_camera_arg, this->options.right_config_arg) != 0)
      return ERROR("unable to open cameras %s %s",
                   this->options.left_camera_arg, this->options.right_camera_arg);

    // Initialize AGC settings
    this->camera->initAGC(this->options.camera_exposure_arg);
  }

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = this->camera->imageCols >> this->imageLevel;
  this->imageRows = this->camera->imageRows >> this->imageLevel;
  this->imageChannels = this->camera->imageChannels;

  MSG("camera image %dx%dx%d",
      this->camera->imageCols, this->camera->imageRows, this->camera->imageChannels);  
  MSG("feeder image %dx%dx%d L%d",
      this->imageCols, this->imageRows, this->imageChannels, this->imageLevel);
         
  return 0;
}


// Finalize camera
int StereoFeeder::finiCamera()
{
  assert(this->camera);
  this->camera->close();
  delete this->camera;
  this->camera = NULL;

  return 0;
}


// Capture a frame 
int StereoFeeder::captureCamera()
{
  uint64_t cycleTime;

  if (this->mode == modeSim)
  {
    SimStereoCamera *simCamera;
    float baseline;
    jplv_cmod_t cmod;
    jplv_image_t *rectImage, *dispImage;
    
    // Simulate 15Hz
    usleep(75000);

    // Get the current state data
    if (this->getState(DGCgettime()) != 0)
      return -1;
    
    // Camera model
    jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &cmod);    
    baseline = jplv_stereo_get_baseline(this->stereo);

    // Get pointers to the image buffers
    rectImage = jplv_stereo_get_image(this->stereo,
                                      JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
    dispImage = jplv_stereo_get_image(this->stereo,
                                      JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

    // Capture simulated image
    simCamera = (SimStereoCamera*) this->camera;
    simCamera->captureSim(&this->state, this->sens2veh, &cmod, baseline, rectImage, dispImage);
  }
  else
  {
    jplv_image_t *leftImage, *rightImage;
      
    // Get pointers to the raw image buffers
    leftImage = jplv_stereo_get_image(this->stereo,
                                      JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
    rightImage = jplv_stereo_get_image(this->stereo,
                                       JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

    // Capture an image, or timeout
    if (this->camera->capture(100, leftImage, rightImage) != 0)
      return MSG("unable to capture image");

    // Get the matching state data
    if (this->getState(this->frameTime) != 0)
      return -1;
  }

  // Get timestamp
  this->frameId = this->camera->frameId;  
  this->frameTime = this->camera->timestamp;

  // Compute some stats
  if (this->frameId > 0)
    cycleTime = (DGCgettime() - this->startTime) / this->frameId;
  else
    cycleTime = 0;

  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %.3f",
                this->frameId, fmod(1e-6 * (double) this->frameTime, 10000));
    if (cycleTime < 1000000)
      cotk_printf(this->console, "%capstats%", A_NORMAL,
                  "%03dms/%4.1fHz %3d/%3d %3.1f/%3.1fdB %5.2f/%5.2fms ",
                  (int) (cycleTime / 1000), (float) (1e6 / cycleTime), 
                  this->camera->exposure[0], this->camera->exposure[1],
                  this->camera->gain[0], this->camera->gain[1],
                  this->camera->shutter[0] * 1e3, this->camera->shutter[1] * 1e3);
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+6dms", (int) (this->state.timestamp - this->frameTime) / 1000);
  }

  return 0;
}


// Initialize image logging
int StereoFeeder::initImageLog()
{
  time_t t;
  char timestamp[64];
  char cmd[256];
  char logName[1024];
  stereo_log_header_t header;

  if (!this->options.enable_imagelog_flag)
    return 0;

  // Construct log name
  t = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
  snprintf(logName, sizeof(logName), "%s/%s-%s-RAW",
           this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

  MSG("opening image log %s", logName);

  // Set up header using stereo image sizes; we will copy the data
  // from the stereo raw buffers.
  assert(this->stereo);
  header.left_camera = atoi(this->options.left_camera_arg);
  header.right_camera = atoi(this->options.right_camera_arg);
  header.cols = this->stereo->image_cols;
  header.rows = this->stereo->image_rows;
  header.channels = this->stereo->image_channels;
  
  // Initialize image logging
  this->imageLog = stereo_log_alloc();
  assert(this->imageLog);
  if (stereo_log_open_write(this->imageLog, logName, &header) != 0)
    return ERROR("unable to open log: %s", logName);
    
  // Copy configuration files
  snprintf(cmd, sizeof(cmd), "cp %s/%s-left.cahvor %s",
           this->options.config_path_arg, this->options.left_camera_arg, logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/%s-right.cahvor %s",
           this->options.config_path_arg, this->options.right_camera_arg, logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
           this->options.config_path_arg, sensnet_id_to_name(this->sensorId), logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/stereo_log.h %s",
           this->options.config_path_arg, logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/stereo_log.c %s",
           this->options.config_path_arg, logName);
  system(cmd);

  return 0;
}


// Finalize image logging
int StereoFeeder::finiImageLog()
{
  if (this->imageLog)
  {
    stereo_log_close(this->imageLog);
    stereo_log_free(this->imageLog);
    this->imageLog = NULL;
  }  
  return 0;
}


// Write current data to image log
int StereoFeeder::writeImageLog()
{
  stereo_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  if (!this->imageLog)
    return 0;
  if (!this->enableLog)
    return 0;

  // Construct the image tag
  tag.frameid = this->frameId;
  tag.timestamp = this->frameTime;
  tag.state = this->state;
  memcpy(tag.sens2veh, this->sens2veh, sizeof(tag.sens2veh));

  // Copy camera settings
  tag.gain[0] = this->camera->gain[0];
  tag.gain[1] = this->camera->gain[1];
  tag.shutter[0] = this->camera->shutter[0];
  tag.shutter[1] = this->camera->shutter[1];

  // Reset reseved values
  memset(tag.reserved, 0, sizeof(tag.reserved));

  // Get current raw images
  leftImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(this->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

  // Write to log
  if (stereo_log_write(this->imageLog, &tag,
                       leftImage->data_size, leftImage->data, rightImage->data) != 0)
    return ERROR("unable to write to log");

  // Update stats
  this->imageLogCount += 1;
  this->imageLogSize += leftImage->data_size + rightImage->data_size;

  // Update console
  if (this->console)
  {
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->imageLogCount, this->imageLogSize / 1024 / 1024);
  }
  
  return 0;
}
  

// Initialize sensnet
int StereoFeeder::initSensnet()
{
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return ERROR("unable to connect to sensnet");

  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR,
                   SNstate, sizeof(VehicleState), 100) != 0)
    return ERROR("unable to join state group");

  // Initialize sensnet logging
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);
        
    // Initialize sensnet logging
    assert(this->sensnet);
    if (sensnet_open_record(this->sensnet, this->logName) != 0)
      return ERROR("unable to open log: %s", this->logName);
    
    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s-left.cahvor %s",
             this->options.config_path_arg, this->options.left_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s-right.cahvor %s",
             this->options.config_path_arg, this->options.right_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             this->options.config_path_arg, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
  }
 
  return 0;
}


// Finalize sensnet
int StereoFeeder::finiSensnet()
{  
  // Clean up SensNet
  sensnet_close_record(this->sensnet);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Publish data
int StereoFeeder::writeSensnet()
{
  pose3_t pose;
  StereoImageBlob blob;
  jplv_image_t *image;
  
  blob.blobType = SENSNET_STEREO_BLOB;
  blob.version = STEREO_IMAGE_BLOB_VERSION;
  blob.sensorId = this->sensorId;
  blob.frameId = this->frameId;
  blob.timestamp = this->frameTime;
  blob.state = this->state;

  // Get the rectified model and copy to blob
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  blob.cx = model.ext.cahv.hc;
  blob.cy = model.ext.cahv.vc;
  blob.sx = model.ext.cahv.hs;
  blob.sy = model.ext.cahv.vs;    
  blob.dispScale = JPLV_STEREO_DISP_SCALE;
  blob.baseline = jplv_stereo_get_baseline(this->stereo);
  assert(blob.baseline > 0);

  // Copy camera transform
  assert(sizeof(this->sens2veh) == sizeof(blob.sens2veh));
  memcpy(blob.sens2veh, this->sens2veh, sizeof(this->sens2veh));

  // Vehicle to local transform
  pose.pos = vec3_set(blob.state.localX,
                      blob.state.localY,
                      blob.state.localZ);
  pose.rot = quat_from_rpy(blob.state.localRoll,
                           blob.state.localPitch,
                           blob.state.localYaw);  
  pose3_to_mat44f(pose, blob.veh2loc);  

  // Copy camera settings
  blob.gain[0] = this->camera->gain[0];
  blob.gain[1] = this->camera->gain[1];
  blob.shutter[0] = this->camera->shutter[0];
  blob.shutter[1] = this->camera->shutter[1];

  // Reset reseved values
  memset(blob.reserved, 0, sizeof(blob.reserved));
  
  blob.cols = this->stereo->image_cols;
  blob.rows = this->stereo->image_rows;

  // Copy rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  blob.colorChannels = image->channels;
  blob.colorSize = image->data_size;
  assert(sizeof(blob.colorData) >= (size_t) image->data_size);
  memcpy(blob.colorData, image->data, image->data_size);
  
  // Copy disparity data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  blob.dispSize = image->data_size;
  assert(sizeof(blob.dispData) >= (size_t) image->data_size);
  memcpy(blob.dispData, image->data, image->data_size);
  
  // Set sensnet logging state to on or off
  sensnet_enable_record(this->sensnet, this->options.enable_log_flag && this->enableLog);

  // Send rectified color image
  if (sensnet_write(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB,
                    this->frameId, sizeof(blob), &blob) != 0)
    return ERROR("unable to write blob");
  
  // Keep some stats
  if (this->options.enable_log_flag && this->enableLog && this->console)
  {
    this->logCount += 1;
    this->logSize += sizeof(blob);
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->logCount, this->logSize / 1024 / 1024);
  }

  return 0;
}


// Initialize everything except image capture
int StereoFeeder::initStereo()
{
  if (true)
  {
    MSG("initializing stereo");
    
    // Create stereo context
    this->stereo = jplv_stereo_alloc(this->imageCols, this->imageRows, this->imageChannels);
    if (!this->stereo)
      return ERROR("unable to allocate stereo context: JPLV %s", jplv_error_str());
    
    // Re-scale the models if ncessary
    jplv_cmod_scale_dims(&this->leftModel, this->imageCols, this->imageRows);
    jplv_cmod_scale_dims(&this->rightModel, this->imageCols, this->imageRows);

    // Set the model in the stereo context
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_LEFT, &this->leftModel);
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &this->rightModel);

    // Set defaults
    jplv_stereo_set_disparity(this->stereo, 0, 40, 7, 7);

    // Prepare for action
    jplv_stereo_prepare(this->stereo);

    MSG("done initializing stereo");
  }

  return 0;
}


// Finalize everything except capture
int StereoFeeder::finiStereo()
{
  // Clean up stereo
  jplv_stereo_free(this->stereo);
  this->stereo = NULL;
  
  return 0;
}


// Get the predicted vehicle state
int StereoFeeder::getState(double timestamp)
{
  VehicleState state;

  memset(&this->state, 0, sizeof(this->state));
      
  // Get the state newest data in the cache
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, -1, sizeof(state), &state) != 0)
    return MSG("unable to read state");
  
  // TODO: do prediction by comparing image/state timestamps
  
  this->state = state;
  
  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
                fmod((double) state.timestamp * 1e-6, 10000));
    cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
                state.localX, state.localY, state.localZ);
    cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
                state.localRoll*180/M_PI, state.localPitch*180/M_PI, state.localYaw*180/M_PI);
  }

  return 0;
}


// Process a frame
int StereoFeeder::process()
{
  uint64_t cpuTime, cycleTime;
  float frac;

  cpuTime = DGCgettime();
  
  if (this->mode != modeSim)
  {
    // Run regular stereo pipeline
    jplv_stereo_rectify(this->stereo);  

    if (this->options.force_plane_flag)
    {
      // Run fake stereo using ground-plane constraint
      this->calcPlaneDisp();
    }
    else
    {
      // Run real stereo
      jplv_stereo_laplace(this->stereo);
      jplv_stereo_disparity(this->stereo);
    }
  }
  
  jplv_stereo_range(this->stereo);  
  jplv_stereo_diag(this->stereo);

  // Compute elapsed time for update
  cpuTime = DGCgettime() - cpuTime;
  cycleTime = DGCgettime() - this->stereoTime;
  this->stereoTime = DGCgettime();
    
  // Compute some diagnostics
  frac = (float) this->stereo->diag_num_valid /
    (this->stereo->image_cols * this->stereo->image_rows);

  if (this->console)
  {
    cotk_printf(this->console, "%stid%", A_NORMAL, "%5d %.3f",
                this->frameId,  fmod(1e-6 * (double) this->frameTime, 10000));
    if (cycleTime < 1000000)
    {
      cotk_printf(this->console, "%ststats%", A_NORMAL, "%03dms/%.1fHz %3dms %3d%%",
                  (int) (cycleTime / 1000), (float) (1e6 / cycleTime), 
                  (int) (cpuTime / 1000), (int) (frac * 100));
    }
  }

  return 0;
}


// Run fake stereo using ground-plane constraint
int StereoFeeder::calcPlaneDisp()
{
  jplv_image_t *dispImage;
  uint16_t *dpix;
  int pi, pj;
  float px, py, pz;
  float cx, cy, sx, sy, baseline;
  float mv[4][4];

  // Disparity image
  dispImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

  // Camera model
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  cx = model.ext.cahv.hc;
  cy = model.ext.cahv.vc;
  sx = model.ext.cahv.hs;
  sy = model.ext.cahv.vs;    
  baseline = jplv_stereo_get_baseline(this->stereo);

  // Camera-to-vehicle transform
  assert(sizeof(mv) == sizeof(this->sens2veh));
  memcpy(mv, this->sens2veh, sizeof(mv));

  // TODO: consider robot attitude
  
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < dispImage->rows; pj++)
  {
    for (pi = 0; pi < dispImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      dpix += 1;
    }
  }

  return 0;
}


// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"StereoFeeder $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"                                                                           \n"
"Capture                                State                               \n"
"Mode  : %mode%                         Time   : %stime% %slat%             \n"
"Camera: %cam%                          Pos    : %spos%                     \n"
"Frame : %capid%                        Rot    : %srot%                     \n"
"Stats : %capstats%                                                         \n"
"                                                                           \n"
"Stereo  %stereo%                       Export: %export%                    \n"
"Frame : %stid%                         Log   : %log%                       \n"
"Stats : %ststats%                      %logname%                           \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%LOG%|%DISPLAY%|%EXPORT%]                                  \n";



// Initialize console display
int StereoFeeder::initConsole()
{
  char filename[1024];

#if USE_FB
  if (this->options.enable_fb_given)
  {
    // Initialize the frame buffer
    this->fb = frame_buffer_open("/dev/fb0");
    if (this->fb == NULL)
      return ERROR("unable to open frame buffer");
  }
#endif
  
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);
  cotk_bind_button(this->console, "%DISPLAY%", " DISPLAY ", "Dd",
                   (cotk_callback_t) onUserDisplay, this);
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);
    
  // Initialize the display
  snprintf(filename, sizeof(filename), "%s/%s.msg",
           this->options.log_path_arg, sensnet_id_to_name(this->sensorId));
  if (cotk_open(this->console, filename) != 0)
    return -1;

  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));

  if (this->camera)
    cotk_printf(this->console, "%cam%", A_NORMAL, "%dx%dx%d",
                this->camera->imageCols, this->camera->imageRows, this->camera->imageChannels);
  if (this->stereo)
    cotk_printf(this->console, "%stereo%", A_NORMAL, "%dx%dx%d L%d",
                this->imageCols, this->imageRows, this->imageChannels, this->imageLevel);

  if (this->mode == modeSim)
    cotk_printf(this->console, "%mode%", A_NORMAL, "sim   ");
  else if (this->mode == modeBB)
    cotk_printf(this->console, "%mode%", A_NORMAL, "bb %s ", this->options.left_camera_arg);
  else if (this->mode == modePGR)
    cotk_printf(this->console, "%mode%", A_NORMAL, "pgr %s %s",
                this->options.left_camera_arg, this->options.right_camera_arg);

  cotk_printf(this->console, "%logname%", A_NORMAL, this->logName);
      
  return 0;
}


// Finalize console display
int StereoFeeder::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }

  if (this->fb)
  {
    frame_buffer_close(this->fb);
  }
  
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserQuit(cotk_t *console, StereoFeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserPause(cotk_t *console, StereoFeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events
int StereoFeeder::onUserLog(cotk_t *console, StereoFeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Handle user events
int StereoFeeder::onUserDisplay(cotk_t *console, StereoFeeder *self, const char *token)
{
  // Toggle through the display modes
  assert(self);
  self->fbMode = (self->fbMode + 1) % 4;
  MSG("frame buffer %d", self->fbMode);
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserExport(cotk_t *console, StereoFeeder *self, const char *token)
{
  // Write out the current image to a file
  MSG("exporting image");
  self->saveImage();
  return 0;
}



// Display image to framebuffer
int StereoFeeder::displayImage()
{
  jplv_image_t *image;

  // TODO?
  // Select image to display
  if (this->fbMode % 2 == 0)
    image = jplv_stereo_get_image(this->stereo,
                                  JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  else if (this->fbMode % 2 == 1)
    image = jplv_stereo_get_image(this->stereo,
                                  JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
  //else if (this->fbMode == 3)
  //  image = jplv_stereo_get_image(this->stereo,
  //                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  else
    return 0;

#if USE_FB
  if (this->fb && this->console)
  {
    struct fb_var_screeninfo vinfo;

    // Get the screen dimensions
    frame_buffer_get_vinfo(this->fb, &vinfo);

    // Draw to upper right of screen
    frame_buffer_draw(this->fb, vinfo.xres - image->cols, 0, image->cols, image->rows,
                      image->channels, image->bits/8, image->data);
  }
#endif
  
  return 0;
}


// Write the current images to a file
int StereoFeeder::saveImage()
{
  jplv_image_t *image;
  char filename[256];
  
  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_LEFT.pnm", sensnet_id_to_name(this->sensorId), this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_RIGHT.pnm", sensnet_id_to_name(this->sensorId), this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  this->exportId++;
  
  if (this->console)
    cotk_printf(this->console, "%export%", A_NORMAL, "%d", this->exportId);

  return 0;
}



// Main program thread
int main(int argc, char **argv)
{
  int status;
  uint64_t lastTime;
  StereoFeeder *feeder;

  // Create feeder
  feeder = new StereoFeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize camera
  if (feeder->initCamera() != 0)
    return -1;

  // Initialize sensnet
  if (feeder->initSensnet() != 0)
    return -1;

  // Initialize algorithms
  if (feeder->initStereo() != 0)
    return -1;

  // Initialize image logging
  if (feeder->initImageLog() != 0)
    return -1;

  // Initialize console
  if (!feeder->options.disable_console_flag)
  {
    if (feeder->initConsole() != 0)
      return -1;
  }

  lastTime = 0;
  feeder->startTime = DGCgettime();
  
  // Start processing
  while (!feeder->quit)
  {
    // Capture incoming frame directly into the stereo buffers
    if (feeder->captureCamera() != 0)
      break;

    // Display current image on framebuffer
    feeder->displayImage();
    
    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);

    // If paused, or if we are running too fast, give up our time
    // slice.
    if (feeder->pause || DGCgettime() - lastTime < (uint64_t) (1e6/feeder->options.rate_arg))
    {
      usleep(20000);
      continue;
    }
    lastTime = DGCgettime();
    
    // Capture and process one frame
    status = feeder->process();
    if (status != 0)
      break;

    // Publish data
    if (feeder->writeSensnet() != 0)
      break;
    feeder->writeImageLog();

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);
  }
  
  // Clean up
  feeder->finiConsole();
  feeder->finiImageLog();
  feeder->finiStereo();
  feeder->finiSensnet();
  feeder->finiCamera();
  cmdline_parser_free(&feeder->options);
  delete feeder;

  MSG("program exited cleanly");
  
  return 0;
}
