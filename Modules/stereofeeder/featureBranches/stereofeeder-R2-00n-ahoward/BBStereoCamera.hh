/* 
 * Desc: PGR Bumblebee stereo camera
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef BB_STEREO_CAMERA_HH
#define BB_STEREO_CAMERA_HH

#include "StereoCamera.hh"

#define DC_1394
#include <vis-tools/vis_devices.h>


/// @brief Bumblebee stereo camera interface
class BBStereoCamera : public StereoCamera
{
  public:

  // Default constructor
  BBStereoCamera();

  // Destructor
  virtual ~BBStereoCamera();
  
  public:

  /// Initialize the camera
  virtual int open(const char *leftCameraId, const char *leftCameraConfig,
                   const char *rightCameraId, const char *rightCameraConfig, int level);

  /// Close the camera
  virtual int close();

  /// Capture raw left/right image frames.
  /// @param[in] timeout Timeout in milliseconds.
  /// @param[out] leftImage,rightImage Left/right image buffers.
  virtual int capture(int timeout, jplv_image_t *leftImage, jplv_image_t *rightImage);

  private:

  // Simple de-mux from interlevaed monochrome images
  int demux(const vis_dev_img_t *src, int *exp, jplv_image_t *left, jplv_image_t *right);

  private:
  
  /// Camera handle
  vis_dev_t *camera;

  /// Image data
  vis_dev_img_t image;
};


#endif
