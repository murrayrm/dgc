/* 
 * Desc: Stereo camera pair with discrete PGR cameras.
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <libdc1394/dc1394_control.h>
#include <jplv/filters.h>
#include <dgcutils/DGCutils.hh>

#include "PGRStereoCamera.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
PGRStereoCamera::PGRStereoCamera()
{
  memset(this->cameras, 0, sizeof(this->cameras));
  memset(this->images, 0, sizeof(this->images));  
  return;
}


// Destructor
PGRStereoCamera::~PGRStereoCamera()
{
  return;
}
  

// Initialize the camera
int PGRStereoCamera::open(const char *leftCameraId, const char *leftCameraConfig,
                          const char *rightCameraId, const char *rightCameraConfig, int level)
{
  // Initialize cameras
  MSG("camera: %s", leftCameraId);
  if ((this->cameras[0] = vis_dev_alloc(leftCameraId, NULL)) == NULL)
    return ERROR("unable to create camera %s", leftCameraId);
  MSG("camera: %s", rightCameraId);
  if ((this->cameras[1] = vis_dev_alloc(rightCameraId, NULL)) == NULL)
    return ERROR("unable to create camera %s", rightCameraId);

  // Configure cameras
  MSG("camera config: %s", leftCameraConfig);
  if (vis_dev_config(this->cameras[0], leftCameraConfig, strlen(leftCameraConfig)))
    return ERROR("failed to configure camera with %s", leftCameraConfig);
  MSG("camera config: %s", rightCameraConfig);
  if (vis_dev_config(this->cameras[1],  rightCameraConfig, strlen(rightCameraConfig)))
    return ERROR("failed to configure camera with %s", rightCameraConfig);
  
  // Initialize image
  if (vis_dev_img_header(this->cameras[0], this->images + 0))
    return ERROR("failed to read image header");
  if (vis_dev_img_header(this->cameras[1], this->images + 1))
    return ERROR("failed to read image header");

  // Record key image dimensions
  MSG("left  %s %dx%dx%d",
      leftCameraId, this->images[0].width, this->images[0].height, this->images[0].channels);
  MSG("right %s %dx%dx%d",
      rightCameraId, this->images[1].width, this->images[1].height, this->images[1].channels);

  this->imageCols = this->images[0].width;
  this->imageRows = this->images[0].height;

  // Level 0 corresponds to the bayer-pattern image; all other levels
  // are debayered.
  if (level == 0)
    this->imageChannels = 1;
  else
    this->imageChannels = 3;

  // Allocate space for raw images
  this->images[0].pixels = malloc(this->images[0].img_size);
  this->images[1].pixels = malloc(this->images[1].img_size);

  // Start capturing
  vis_dev_start(this->cameras[0]);
  vis_dev_start(this->cameras[1]);
  MSG("capture enabled");
  
  return 0;
}


// Close the camera
int PGRStereoCamera::close()
{
  MSG("capture disabled");
  vis_dev_stop(this->cameras[0]);
  vis_dev_stop(this->cameras[1]);
  free(this->images[0].pixels);
  free(this->images[1].pixels);
  this->images[0].pixels = NULL;
  this->images[1].pixels = NULL;
  vis_dev_free(this->cameras[0]);
  vis_dev_free(this->cameras[1]);
  this->cameras[0] = NULL;
  this->cameras[1] = NULL;  

  return 0;
}


// Capture a frame
int PGRStereoCamera::capture(int timeout, jplv_image_t *leftImage, jplv_image_t *rightImage)
{
  int i;
  uint64_t timestamp, timestamps[2];
  dc1394_feature_info gain, shutter;

  // Check for new images
  vis_dev_read_imgs(2, this->cameras, this->images);  

  // See if we have new data
  timestamps[0] =
    (uint64_t) this->images[0].fill_time.tv_sec * 1000000 +
    (uint64_t) this->images[0].fill_time.tv_usec;
  timestamps[1] =
    (uint64_t) this->images[1].fill_time.tv_sec * 1000000 +
    (uint64_t) this->images[1].fill_time.tv_usec;
  //MSG("%lld %lld", timestamps[0], timestamps[1]);
  timestamp = (timestamps[0] < timestamps[1] ? timestamps[0] : timestamps[1]);    
  
  // Update current frame info
  this->frameId += 1;
  this->timestamp = timestamp;  

  // Demux the image into left and right
  //this->copy(this->images + 0, leftImage);
  //this->copy(this->images + 1, rightImage);
  this->debayer(this->images + 0, leftImage);
  this->debayer(this->images + 1, rightImage);

  for (i = 0; i < 2; i++)
  {
    // Get current gain settings
    dc_1394_command(this->cameras[i], "gain", 0, &gain, sizeof(gain));
    if (gain.feature_id == FEATURE_GAIN)
      this->gain[i] = gain.abs_value;
    
    // Get current shutter settings and convert to milliseconds
    dc_1394_command(this->cameras[i], "shutter", 0, &shutter, sizeof(shutter));
    if (shutter.feature_id == FEATURE_SHUTTER)
      this->shutter[i] = shutter.abs_value;
  }

  // Measure current exposure in ROI
  this->exposure[0] = this->measureAGC(leftImage);
  this->exposure[1] = this->measureAGC(rightImage);

  // Update the AGC
  //this->controlAGC(this->goalExp, this->exposure);
  
  return 0;
}


// Initialize software exposure control
int PGRStereoCamera::initAGC(int exposure)
{
  int i;
  dc1394_feature_info gain, shutter;
  
  this->enableAGC = true;
  this->goalExp = exposure;

  for (i = 0; i < 2; i++)
  {  
    // Get gain range
    memset(&gain, 0, sizeof(gain));
    dc_1394_command(this->cameras[i], "gain", 4, &gain, sizeof(gain));
    if (gain.feature_id == FEATURE_GAIN)
    {
      this->gainMin = gain.abs_min;
      this->gainMax = gain.abs_max;
      this->gain[i] = gain.abs_value;
      MSG("gain: %.0f [%.0f %.0f]", this->gain[i], this->gainMin, this->gainMax);
    }
    else
    {
      this->enableAGC = false;
      MSG("unable to read gain; softwareAGC disabled");
    }
  
    // Get shutter range
    memset(&shutter, 0, sizeof(shutter));
    dc_1394_command(this->cameras[i], "shutter", 4, &shutter, sizeof(shutter));
    if (shutter.feature_id == FEATURE_SHUTTER)
    {
      this->shutterMin = shutter.abs_min;
      this->shutterMax = shutter.abs_max;
      this->shutter[i] = shutter.abs_value;
      MSG("shutter: %.2f [%.2f %.2f]",
          this->shutter[i] * 1000, this->shutterMin * 1000, this->shutterMax * 1000);
    }
    else
    {
      this->enableAGC = false;
      MSG("unable to read gain; softwareAGC disabled");
    }
  }

  return 0;
}


// Measure exposure over ROI on current images
int PGRStereoCamera::measureAGC(jplv_image_t *src)
{
  int i, j, step, exp, n;
  int min_row, max_row;
  uint8_t *pix;

  step = 1;
  min_row = 2 * src->rows / 3;
  max_row = src->rows;

  n = 0;
  exp = 0;
  
  for (j = min_row; j < max_row; j += step)
  {
    pix = jplv_image_pixel(src, 0, j);
    
    for (i = 0; i < src->cols; i += step)
    {
      n += 1;
      exp += (int) (uint32_t) pix[0];      
      pix += src->channels;
    }
  }
  
  return exp / n;
}


// Do software exposure control
int PGRStereoCamera::controlAGC(int goalExp, int measExp)
{
  /* TODO
  float gainP, shutterP;
  float err, step, gain, shutter;
  char command[32];

  // PID control terms
  gainP = 0.02;
  shutterP = 0.001;

  if (!this->enableAGC)
    return 0;
  
  // Compute current exposure value
  err = goalExp - measExp;

  // Simple P controller on shutter
  shutter = shutterP * err; 

  // MAGIC
  // Limit the rate at which the shutter changes
  step = (this->shutterMax - this->shutterMin) * 0.001;
  if (shutter > +step)
    shutter = +step;
  if (shutter < -step)
    shutter = -step;  
  shutter = this->shutter + shutter;

  if (shutter < this->shutterMin)
    shutter = this->shutterMin;
  if (shutter > this->shutterMax)
    shutter = this->shutterMax; 
  
  // Simple P controller on gain
  gain = gainP * err;

  // MAGIC
  // Limit the rate at which the gain changes
  step = (this->gainMax - this->gainMin) * 0.001;
  if (gain > +step)
    gain = +step;
  if (gain < -step)
    gain = -step;

  gain = this->gain + gain;

  // Bound the gain
  if (gain < this->gainMin)
    gain = this->gainMin;
  if (gain > this->gainMax)
    gain = this->gainMax;

  // Set the shutter on the cameras
  snprintf(command, sizeof(command), "%fms", shutter);
  if (dc_1394_config(this->cameras[0], command, 0))
    ERROR("dc_1394_config(%s) failed", command);
  if (dc_1394_config(this->cameras[1], command, 0))
    ERROR("dc_1394_config(%s) failed", command);

  this->shutter = shutter;
  
  // Set the gain on the cameras
  snprintf(command, sizeof(command), "%fdb", gain);
  if (dc_1394_config(this->cameras[0], command, 0))
    ERROR("dc_1394_config(%s) failed", command);
  if (dc_1394_config(this->cameras[1], command, 0))
    ERROR("dc_1394_config(%s) failed", command);

  this->gain = gain;
  
  //bb_camera_set_gain(self, gain);
  //bb_camera_set_shutter(self, shutter);
      
  //MSG("exposure %.0d %.0d %.3f %.3f %.3f %.3f %.3f",
  //    goal, meas, err,
  //    shutter, self->shutter_value, gain, self->gain_value);  
  */
  
  return 0;
}


// Create RGB from Bayer-pattern image
int PGRStereoCamera::debayer(const vis_dev_img_t *src, jplv_image_t *dst)
{
  int level;
  int i, j;
  uint8_t *dstP, *srcR, *srcB, *srcG, *srcH;

  // Determine pyramid level of destination images
  if (src->width == 1 * dst->cols)
    level = 0;
  else if (src->width == 2 * dst->cols)
    level = 1;
  else if (src->width == 4 * dst->cols)
    level = 2;
  else
    assert(false);

  if (level == 0)
  {
    // At level 0, copy the entire bayer-pattern image
    memcpy(dst->data, src->pixels, dst->data_size);
  }
  else if (level == 1)
  {
    assert(dst->channels == 3);
      
    dstP = jplv_image_pixel(dst, 0, 0);
    srcR = (uint8_t*) src->pixels;
    srcB = (uint8_t*) src->pixels + src->width + 1;
    srcG = (uint8_t*) src->pixels + 1;
    srcH = (uint8_t*) src->pixels + src->width;

    for (j = 0; j < dst->rows; j++)
    {
      for (i = 0; i < dst->cols; i++)
      {
        if (dst->channels == 1)
        {
          dstP[0] = *srcG;
        }
        else
        {
          dstP[0] = *srcR;
          dstP[1] = *srcG; // TODO use other G as well
          dstP[2] = *srcB;
        }
        dstP += dst->channels;
        srcR += 2;
        srcB += 2;
        srcG += 2;
        srcH += 2;
      }
      srcR += src->width;
      srcB += src->width;
      srcG += src->width;
      srcH += src->width;    
    }
  }
  else
  {
    MSG("only level 0,1 images are currently supported");
    return -1;
  }
  
  return 0;
}


// Copy captured images to target buffers
int PGRStereoCamera::copy(const vis_dev_img_t *src, jplv_image_t *dst)
{
  int level;
  int i;
  quick_pyramid_t *pyr;
  int cols, rows, size;
  uint8_t *la, *lb;

  // Determine pyramid level of destination images
  if (src->width == 1 * dst->cols)
    level = 0;
  else if (src->width == 2 * dst->cols)
    level = 1;
  else if (src->width == 4 * dst->cols)
    level = 2;
  else
    assert(false);
  
  cols = src->width;
  rows = src->height;
  size = src->img_size;
  la = (uint8_t*) malloc(size);
  lb = (uint8_t*) malloc(size);
  memcpy(la, src->pixels, size);
    
  pyr = quick_pyramid_start(cols);
        
  for (i = 0; i < level; i++)
  {
    quick_pyramid(pyr, true, cols, rows, la,
                  0, 0, cols, rows, 5, 5, 1, 1, false, lb, NULL);
    cols /= 2;
    rows /= 2;
    size /= 4;
    memcpy(la, lb, size);      
  }
      
  assert(size == dst->data_size);
  memcpy(dst->data, la, size);
    
  quick_pyramid_stop(pyr);
  free(la);
  free(lb);

  return 0;
}
