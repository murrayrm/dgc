/* 
 * Desc: Abstract base class for a generic stereo camera
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef STEREO_CAMERA_HH
#define STEREO_CAMERA_HH


#include <jplv/jplv_image.h>


/// @brief Abstract base class for a stereo camera.
class StereoCamera
{
  public:

  StereoCamera()
  {
    memset(this, 0, sizeof(*this));
  }

  virtual ~StereoCamera() {}

  public:

  /// Initialize the camera
  virtual int open(const char *leftCameraId, const char *rightCameraId,
                   const char *leftCameraConfig, const char *rightCameraConfig, int level) {return 0;}

  /// Close the camera
  virtual int close() {return 0;}

  /// @brief Capture raw left/right image frames.
  ///
  /// @param[in] timeout Timeout in milliseconds.
  /// @param[out] leftImage,rightImage Left/right image buffers.
  virtual int capture(int timeout, jplv_image_t *leftImage, jplv_image_t *rightImage) {return 0;}

  public:
  
  /// @brief Initialize AGC
  ///
  /// @param[in] exposure Target exposure value (i.e., average intensity of ROI).
  virtual int initAGC(int exposure) {return 0;}
  
  public:

  /// Native camera image dimensions
  int imageCols, imageRows, imageChannels;
  
  /// Frame id of current image
  int frameId;
  
  /// Timestamp of current image (microseconds since epoch)
  uint64_t timestamp;

  /// Current gain and shutter values (dB and seconds)
  float gain[2], shutter[2];

  /// Average exposure of current image
  int exposure[2];
};


#endif
