/* 
 * Desc: Bumblebee stereo camera driver
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __MMX__
#include <mmintrin.h>
#endif
#ifdef __SSE__
#include <xmmintrin.h>
#endif

#include <libdc1394/dc1394_control.h>
#include <dgcutils/DGCutils.hh>
#include "BBStereoCamera.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
BBStereoCamera::BBStereoCamera()
{
  this->camera = NULL;
  memset(&this->image, 0, sizeof(this->image));  
  return;
}


// Destructor
BBStereoCamera::~BBStereoCamera()
{
  return;
}
  

// Initialize the camera
int BBStereoCamera::open(const char *leftCameraId, const char *leftCameraConfig,
                         const char *rightCameraId, const char *rightCameraConfig, int level)
{
  // Initialize vis_dev camera
  MSG("camera: %s", leftCameraId);
  if ((this->camera = vis_dev_alloc(leftCameraId, NULL)) == NULL)
    return ERROR("unable to create camera %s", leftCameraId);

  MSG("camera config: %s", leftCameraConfig);
  if (vis_dev_config(this->camera, leftCameraConfig, strlen(leftCameraConfig)))
    return ERROR("failed to configure camera with %s", leftCameraConfig);

  // Initialize vis_dev image
  if (vis_dev_img_header(this->camera, &(this->image)))
    return ERROR("failed to read image header");

  // Record key image dimensions
  MSG("camera %dx%dx%d %d",
      this->image.width, this->image.height, this->image.channels, this->image.img_size);
  this->imageCols = this->image.width;
  this->imageRows = this->image.height;
  this->imageChannels = this->image.channels;

  // Allocate space for raw image
  this->image.pixels = malloc(this->image.img_size);

  // TODO
  // Software gain

  // Start capturing
  vis_dev_start(this->camera);
  MSG("capture enabled");
  
  return 0;
}


// Close the camera
int BBStereoCamera::close()
{
  MSG("capture disabled");
  vis_dev_stop(this->camera);
  free(this->image.pixels);
  vis_dev_free(this->camera);
  this->camera = NULL;

  return 0;
}


// Capture a frame
int BBStereoCamera::capture(int timeout, jplv_image_t *leftImage, jplv_image_t *rightImage)
{
  int exposure;
  uint64_t timestamp;
  dc1394_feature_info gain, shutter;
      
  // Check for new images
  vis_dev_read_img(this->camera, &(this->image));  

  // Get the timestamp
  timestamp =
    (uint64_t) this->image.fill_time.tv_sec * 1000000 +
    (uint64_t) this->image.fill_time.tv_usec;

  // Get current gain settings
  dc_1394_command(this->camera, "gain", 4, &gain, sizeof(gain));
  if (gain.feature_id == FEATURE_GAIN)
    this->gain[0] = this->gain[1] = gain.abs_value;
    
  // Get current shutter settings and convert to milliseconds
  dc_1394_command(this->camera, "shutter", 4, &shutter, sizeof(shutter));
  if (shutter.feature_id == FEATURE_SHUTTER)
    this->shutter[0] = this->shutter[1] = shutter.abs_value;

  // TODO: break out left/right exposure
  // Demux the image into left and right
  this->demux(&this->image, &exposure, leftImage, rightImage);
  this->exposure[0] = this->exposure[1] = exposure;

  // Update current frame info
  this->frameId += 1;
  this->timestamp = timestamp;  

  return 0;
}


// Simple de-mux from interlevaed monochrome images
int BBStereoCamera::demux(const vis_dev_img_t *src, int *exp,
                          jplv_image_t *left, jplv_image_t *right)
{
  int i, j;
  int level;
  const uint8_t *src0, *src1, *src2, *src3;
  uint8_t *dst1, *dst2;
  unsigned long nexp;
  
  // Determine pyramid level of destination images
  if (src->width == 1 * left->cols)
    level = 0;
  else if (src->width == 2 * left->cols)
    level = 1;
  else if (src->width == 4 * left->cols)
    level = 2;
  else
    assert(false);
  
  src0 = (uint8_t*) src->pixels;
  src1 = src0 + src->width*2;
  src2 = src1 + src->width*2;
  src3 = src2 + src->width*2;
  dst1 = left->data;
  dst2 = right->data;

  nexp = 0;
    
  if (level == 0)
  {
    for (j = 0; j < src->height; j++)
    {
#ifdef __MMX__
      __m64 mm7 = _mm_setzero_si64();
      for (i = 0; i < src->width; i+=1*8)
      {
        __m64 mm0, mm1, mm2, mm3;
        // mm0 = 0 1 2 3 4 5 6 7    
        // mm1 = 8 9 A B C D E F
        mm0 = *(__m64 *)src0;
        mm1 = *(__m64 *)(src0 + 8);

        // mm2 = 0 8 1 9 2 A 3 B
        // mm3 = 4 C 5 D 6 E 7 F
        mm2 = _mm_unpacklo_pi8(mm0, mm1);
        mm3 = _mm_unpackhi_pi8(mm0, mm1);

        // mm0 = 0 4 8 C 1 5 9 D
        // mm1 = 2 6 A E 3 7 B F
        mm0 = _mm_unpacklo_pi8(mm2, mm3);
        mm1 = _mm_unpackhi_pi8(mm2, mm3);

        // dst1 = 0 2 4 6 8 A C E
        // dst2 = 1 3 5 7 9 B D F
        *(__m64 *)dst1 = mm2 = _mm_unpacklo_pi8(mm0, mm1);
        *(__m64 *)dst2 = mm3 = _mm_unpackhi_pi8(mm0, mm1);

        mm0 = _mm_sad_pu8(mm2, mm7);
        mm1 = _mm_sad_pu8(mm3, mm7);
        nexp += _mm_cvtsi64_si32(_mm_add_pi32(mm0, mm1));
        src0 += 2*8;
        dst1 += 1*8;
        dst2 += 1*8;
      }
#else
      for (i = 0; i < src->width; i++)
      {
        nexp += *dst1 = src0[0];
        nexp += *dst2 = src0[1];
        src0 += 2;
        dst1 += 1;
        dst2 += 1;
      }
#endif
    }
  }
  else if (level == 1)
  {
    for (j = 0 ; j < src->height /2 ; j++)
    {
      for (i = 0 ; i < src->width /2 ; i++)
      {
        nexp += *dst1 = (src0[0] + src0[2] + src1[0] + src1[2])>>2;
        nexp += *dst2 = (src0[1] + src0[3] + src1[1] + src1[3])>>2;
        src0 += 4;
        src1 += 4;
        dst1 += 1;
        dst2 += 1;
      }
      src0 = src1;
      src1+= src->width*2;
    }
  }
  else if (level == 2)
  {
    for (j = 0 ; j < src->height /4 ; j++)
    {
      for (i = 0 ; i < src->width /4 ; i++)
      {
#if defined __MMX__ && defined __SSE__
        __m64 m0, m1, m2, m3;
        unsigned char __attribute__ ((aligned (16))) avgs[8];
        m0 = *(__m64 *)src0;
        m1 = *(__m64 *)src1;
        m2 = *(__m64 *)src2;
        m3 = *(__m64 *)src3;

        m0 = _mm_avg_pu8(m0, m1);
        m2 = _mm_avg_pu8(m2, m3);
        m0 = _mm_avg_pu8(m0, m2);

        *(__m64*)avgs = m0;

        nexp += *dst1 = (avgs[0] + avgs[2] + avgs[4] + avgs[6]) >> 2;
        nexp += *dst2 = (avgs[1] + avgs[3] + avgs[5] + avgs[7]) >> 2;
#else

        nexp += 
        *dst1 = (src0[0] + src0[2] + src0[4] + src0[6] +
                 src1[0] + src1[2] + src1[4] + src1[6] +
                 src2[0] + src2[2] + src2[4] + src2[6] +
                 src3[0] + src3[2] + src3[4] + src3[6]) >> 4;
        nexp+= 
        *dst2 = (src0[1] + src0[3] + src0[5] + src0[7] +
                 src1[1] + src1[3] + src1[5] + src1[7] +
                 src2[1] + src2[3] + src2[5] + src2[7] +
                 src3[1] + src3[3] + src3[5] + src3[7]) >> 4;
#endif
        src0 += 8;
        src1 += 8;
        src2 += 8;
        src3 += 8;
        dst1 += 1;
        dst2 += 1;
      }
      src0+=src->width*6;
      src1+=src->width*6;
      src2+=src->width*6;
      src3+=src->width*6;
    }
  }
#ifdef __MMX__
  _mm_empty();
#endif

  // Compute average exposure across both images
  if (exp)
    *exp = nexp / (src->width >> level) / (src->height >> level) / 2;
      
  return 0;
}
