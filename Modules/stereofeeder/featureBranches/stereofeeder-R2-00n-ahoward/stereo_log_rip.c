
/* Desc: Rip sensnet logs to JPLV format.
 * Author: Andrew Howard
 * Date: 13 Sep 2007
 * CVS: $Id$
 *
 * Build with:
 *  gcc -o stereo-log-rip -Wall -I../../include stereo_log_rip.c -L../../lib/i486-gentoo-linux-static -lsensnet -lsensnetlog -ljplv -lm
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <jplv/jplv_cmod.h>
#include <jplv/jplv_image.h>
#include <interfaces/StereoImageBlob.h>
#include <sensnet/sensnet_log.h>


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log ripping
int main(int argc, char **argv)
{
  char *filename;
  sensnet_log_t *log;
  sensnet_log_header_t header;
  StereoImageBlob *blob;
  int count;
    
  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  // Create log
  log = sensnet_log_alloc();
  assert(log);

  // Open log
  MSG("opening %s", filename);
  if (sensnet_log_open_read(log, filename, &header) != 0)
    return -1;

  count = 0;
  blob = malloc(sizeof(*blob));
  
  while (true)
  {
    int blob_id;
    
    // Read blob
    if (sensnet_log_read(log, NULL, NULL, NULL, &blob_id, sizeof(*blob), blob) != 0)
      return -1;

    MSG("read blob %d frame %d time %.3f",
        blob_id, blob->frameId, (double) blob->timestamp * 1e-6);

    // Write out the camera models on the first iteration
    if (count++ == 0)
    {
      char filename[1024];
      jplv_cmod_t cmod;

      cmod.core.xdim = blob->cols;
      cmod.core.ydim = blob->rows;
      cmod.core.mclass = CMOD_CLASS_CAHV;
      cmod.core.u.cahv.c[0] = 0;
      cmod.core.u.cahv.c[1] = 0;
      cmod.core.u.cahv.c[2] = 0;
      cmod.core.u.cahv.a[0] = 0;
      cmod.core.u.cahv.a[1] = 0;
      cmod.core.u.cahv.a[2] = 1;
      cmod.core.u.cahv.h[0] = blob->leftCamera.sx;
      cmod.core.u.cahv.h[1] = 0;
      cmod.core.u.cahv.h[2] = blob->leftCamera.cx;
      cmod.core.u.cahv.v[0] = 0;
      cmod.core.u.cahv.v[1] = blob->leftCamera.sy;
      cmod.core.u.cahv.v[2] = blob->leftCamera.cy;
      cmod.ext.cahv.hc = blob->leftCamera.cx;
      cmod.ext.cahv.vc = blob->leftCamera.cy;
      cmod.ext.cahv.hs = blob->leftCamera.sx;
      cmod.ext.cahv.vs = blob->leftCamera.sy;

      snprintf(filename, sizeof(filename), "left.cahv");
      MSG("writing %s", filename);
      jplv_cmod_write(&cmod, filename, NULL);

      cmod.core.xdim = blob->cols;
      cmod.core.ydim = blob->rows;
      cmod.core.mclass = CMOD_CLASS_CAHV;
      cmod.core.u.cahv.c[0] = blob->baseline;
      cmod.core.u.cahv.c[1] = 0;
      cmod.core.u.cahv.c[2] = 0;
      cmod.core.u.cahv.a[0] = 0;
      cmod.core.u.cahv.a[1] = 0;
      cmod.core.u.cahv.a[2] = 1;
      cmod.core.u.cahv.h[0] = blob->rightCamera.sx;
      cmod.core.u.cahv.h[1] = 0;
      cmod.core.u.cahv.h[2] = blob->rightCamera.cx;
      cmod.core.u.cahv.v[0] = 0;
      cmod.core.u.cahv.v[1] = blob->rightCamera.sy;
      cmod.core.u.cahv.v[2] = blob->rightCamera.cy;
      cmod.ext.cahv.hc = blob->rightCamera.cx;
      cmod.ext.cahv.vc = blob->rightCamera.cy;
      cmod.ext.cahv.hs = blob->rightCamera.sx;
      cmod.ext.cahv.vs = blob->rightCamera.sy;

      snprintf(filename, sizeof(filename), "right.cahv");
      MSG("writing %s", filename);
      jplv_cmod_write(&cmod, filename, NULL);
    }
       
    // Write blob as PPM file
    if (blob->channels == 3)
    {
      char filename[1024];
      uint8_t *im;
      jplv_image_t *image;
  
      // Write left image
      im = StereoImageBlobGetLeft(blob, 0, 0);
      image = jplv_image_alloc(blob->cols, blob->rows, blob->channels, 8, 0, im);      
      snprintf(filename, sizeof(filename), "%06d-left.ppm", blob->frameId);
      MSG("writing %s", filename);
      jplv_image_write_pnm(image, filename, NULL);
      jplv_image_free(image);

      // Write right image
      im = StereoImageBlobGetRight(blob, 0, 0);
      image = jplv_image_alloc(blob->cols, blob->rows, blob->channels, 8, 0, im);      
      snprintf(filename, sizeof(filename), "%06d-right.ppm", blob->frameId);
      MSG("writing %s", filename);
      jplv_image_write_pnm(image, filename, NULL);
      jplv_image_free(image);
    }
  }

  // Clean up
  free(blob);
  sensnet_log_close(log);
  sensnet_log_free(log);
  
  return 0;
}

