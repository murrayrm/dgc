/* 
 * Desc: Stereo camera pair with discrete PGR cameras.
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef PGR_STEREO_CAMERA_HH
#define PGR_STEREO_CAMERA_HH

#include "StereoCamera.hh"

#define DC_1394
#include <vis-tools/vis_devices.h>


/// @brief PGR 1394 stereo camera interface
class PGRStereoCamera : public StereoCamera
{
  public:

  // Default constructor
  PGRStereoCamera();

  // Destructor
  virtual ~PGRStereoCamera();
  
  public:

  /// Initialize the camera
  virtual int open(const char *leftCameraId, const char *leftCameraConfig,
                   const char *rightCameraId, const char *rightCameraConfig);

  /// Close the camera
  virtual int close();

  /// @brief Capture raw left/right image frames.
  ///
  /// @param[in] timeout Timeout in milliseconds.
  /// @param[out] leftImage,rightImage Left/right image buffers.
  virtual int capture(int timeout, jplv_image_t *leftImage, jplv_image_t *rightImage);

  private:

  /// @brief Initialize AGC
  ///
  /// @param[in] exposure Target exposure value (i.e., average intensity of ROI).
  virtual int initAGC(int exposure);

  /// Measure exposure over ROI on image
  int measureAGC(jplv_image_t *src);

  /// Do software exposure control
  int controlAGC(int goalExp, int measExp);

  /// Is software AGC enabled?
  bool enableAGC;

  /// Goal exposure for software AGC
  int goalExp;
  
  /// Gain settings
  float gainMin, gainMax;

  /// Shutter settings
  float shutterMin, shutterMax;

  private:

  // Create RGB from Bayer-pattern image
  int debayer(const vis_dev_img_t *src, jplv_image_t *dst);

  /// Copy captured images to target buffers.
  /// This will resize the images if necessary.
  int copy(const vis_dev_img_t *src, jplv_image_t *dst);

  private:
  
  /// Camera handles
  vis_dev_t *cameras[2];

  /// Image data
  vis_dev_img_t images[2];
};


#endif
