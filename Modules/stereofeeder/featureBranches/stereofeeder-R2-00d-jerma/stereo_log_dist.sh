#!/bin/bash

# Desc: Make a mini-distribution with the files needed to read ladar logs.
# Author: Andrew H
# Date: 14 March 2007

# This script expects to be run from the stereofeeder directory.

USAGE="USAGE: stereo_log_dist.sh <version>"
if [ $# -ne 1 ]; then
  echo $USAGE
  exit
fi

DST=stereo-log
VER=$1

README="Simple stereo log test program.\n"
README=$README"Build with : gcc -o stereo-log-test stereo_log_test.c sensnet_log.c\n"
README=$README"Run with   : ./stereo-log-test <LOGFILE>\n"

mkdir $DST

# Do some replacements on the test to make paths work out
sed -e "s|<interfaces/|<|" -e "s|<sensnet/|<|" stereo_log_test.c > $DST/stereo_log_test.c

# Copy additional files
cp ../sensnet/sensnet_log.[ch] $DST
cp ../interfaces/StereoImageBlob.h $DST
cp ../interfaces/VehicleState.h $DST

# Add a README
echo -e $README > $DST/README

# Make a tarball
tar cvzf stereo-log-dist-$VER.tar.gz stereo-log
