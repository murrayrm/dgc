/* 
 * Desc: Simulated stereo camera
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#ifndef SIM_STEREO_CAMERA_HH
#define SIM_STEREO_CAMERA_HH

#include <jplv/jplv_cmod.h>
#include <interfaces/VehicleState.h> 

#include "StereoCamera.hh"


/// @brief Simulated stereo camera.
///
/// This has a different API compared to real cameras, since it
/// generates rectified and disparity images directly.
class SimStereoCamera : public StereoCamera
{
  public:

  // Default constructor
  SimStereoCamera();

  // Destructor
  virtual ~SimStereoCamera();

  public:

  /// Initialize the camera
  virtual int open(int cols, int rows, int channels);

  /// Close the camera
  virtual int close();

  /// Capture a simulated frame
  virtual int captureSim(const VehicleState *state,
                         const float sens2veh[4][4],
                         const jplv_cmod_t *cmod, float baseline,
                         jplv_image_t *rectImage, jplv_image_t *dispImage);

  private:
  
  // Capture a simulated frame from a pattern on the ground
  virtual int captureSimPattern(const VehicleState *state,
                                const float sens2veh[4][4],
                                const jplv_cmod_t *cmod, float baseline,
                                jplv_image_t *rectImage, jplv_image_t *dispImage);

  public:
  
  // Load a data from a KML file
  int loadKml(const char *filename);

  // Capture a simulated frame using KML model 
  int captureSimKml(const VehicleState *state, const float sens2veh[4][4],
                    const jplv_cmod_t *cmod, float baseline,
                    jplv_image_t *rectImage, jplv_image_t *dispImage);

  private:

  // Query the value at a pixel
  int getKmlPixel(double gx, double gy);

  private:

  // Simulation mode
  enum {modePattern, modeKML} mode;
  
  // Simulated road line data
  int numLines, maxLines;
  struct Line {double ax, ay, bx, by, len;} *lines;
  
};


#endif
