
/* 
 * Desc: Stereo feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
  
#if USE_FB
#include <vis-tools/frame_buffer.h>
#endif

#if USE_AA
#include <aalib.h>
#endif

#include <jplv/jplv_stereo.h>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <skynet/sn_msg.hh>
#include <skynet/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/StereoImageBlob.h>
#include <ncurses.h>
#include <cotk/cotk.h>

#include "bb_camera.h"
#include "stereo_log.h"
#include "cmdline.h"


/// @brief Stereo feeder class
class StereoFeeder
{
  public:   

  /// Default constructor
  StereoFeeder();

  /// Default destructor
  ~StereoFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize feeder for simulated capture
  int initSim(const char *configPath);
  
  /// Finalize feeder for simulated capture
  int finiSim();

  /// Capture a simulated frame 
  int captureSim();

  /// Initialize feeder for live capture
  int initLive(const char *configPath);
  
  /// Finalize feeder for live capture
  int finiLive();

  /// Capture a frame 
  int captureLive();
  
  /// Get the predicted vehicle state
  int getState(double timestamp);

  /// Initialize everything except capture
  int init();

  /// Finalize everything except capture
  int fini();
  
  /// Process a frame
  int process();

  // Run fake stereo using ground-plane constraint
  int calcPlaneDisp();
  
  /// Publish data over sensnet
  int writeSensnet();

  // Display current image to framebuffer
  int displayImage();

  // Write the current images to a file
  int writeFile();
  
  public:
  
  /// Initialize log for writing
  int openLog(const char *configPath);

  /// Finalize log
  int closeLog();

  /// Write the current images to the log
  int writeLog();

  public:
  
  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, StereoFeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, StereoFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, StereoFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserDisplay(cotk_t *console, StereoFeeder *self, const char *token);

  /// Console button callback
  static int onUserExport(cotk_t *console, StereoFeeder *self, const char *token);

  public:

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char defaultConfigPath[256];

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;

  // What mode are we in?
  enum {modeLive, modeSim} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Is logging enabled
  bool enableLog;

  // Id of the camera
  int cameraId;
  
  // Camera interface
  bb_camera_t *camera;

  // Buffer for raw image capture
  jplv_image_t *capImage;

  // Log file 
  stereo_log_t *log;

  // Pyramid level for downsampling input image
  int imageLevel;
  
  // Raw image dimensions
  int imageCols, imageRows, imageChannels;

  // Camera models
  jplv_cmod_t leftModel, rightModel;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];
  
  // Calibration stats (display only).
  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).
  vec3_t sensPos, sensRot;

  // Stereo context
  jplv_stereo_t *stereo;
  
  // SensNet context
  sensnet_t *sensnet;
  
  // Console text display
  cotk_t *console;

  // Current frame id
  int frameId;

  // Current frame time (microseconds)
  int64_t frameTime;

  // Current state data
  VehicleState state;
  
  // Current export frame id
  int exportId;

  // Start time for computing stats
  double startTime;
  
  // Capture stats
  uint64_t capTime;

  // Stereo stats
  uint64_t stereoTime;

  // Logging stats
  int logCount, logSize;

  // Enable the frame buffer display
  bool enableDisplay;
  
#if USE_FB
  // Console frame buffer
  struct frame_buffer_t *fb;
#endif

#if USE_AA
  // Ascii rendering context
  aa_context *aa;  
#endif
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
StereoFeeder::StereoFeeder()
{
  memset(this, 0, sizeof(*this));

  this->frameId = -1;

  return;
}


// Default destructor
StereoFeeder::~StereoFeeder()
{
  return;
}


// Parse the command line
int StereoFeeder::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the default config path
  if (getenv("DGC_CONFIG_PATH"))
    snprintf(this->defaultConfigPath, sizeof(this->defaultConfigPath),
             "%s/stereofeeder", getenv("DGC_CONFIG_PATH"));
  else
    return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");    

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);
  
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);
  
  return 0;
}


// Parse the config file
int StereoFeeder::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG", configPath, sensnet_id_to_name(this->sensorId));
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz);
  sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz);

  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  // Record euler angles for display only
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  // Fill out the camera id
  this->cameraId = atoi(this->options.camera_arg);
  if (this->cameraId <= 0)
    return ERROR("invalid camera id: %s", this->options.camera_arg);

  return 0;
}


// Initialize feeder for simulated capture
int StereoFeeder::initSim(const char *configPath)
{
  char filename[256];

  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = 640 >> this->imageLevel; // MAGIC
  this->imageRows = 480 >> this->imageLevel; // MAGIC
  this->imageChannels = 1;

  MSG("image %dx%dx%d", this->imageCols, this->imageRows, this->imageChannels);
      
  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", configPath, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", configPath, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());
  
  this->mode = modeSim;
  
  return 0;
}


// Finalize feeder for simulated capture
int StereoFeeder::finiSim()
{    
  return 0;
}


// Capture a simulated frame 
int StereoFeeder::captureSim()
{
  uint64_t timestamp;
  jplv_image_t *rectImage, *dispImage;
  uint8_t *rpix;
  uint16_t *dpix;
  float cx, cy, sx, sy, baseline;
  pose3_t pose;
  float mv[4][4];
  double ml[4][4];
  int pi, pj;
  int qi, qj;
  float px, py, pz;
  float vx, vy, vz;
  double qx, qy;

  // Simulate 15Hz
  usleep(75000);

  DGCgettime(timestamp);
  
  // Get the current state data
  if (this->getState(timestamp) != 0)
    return -1;
  
  // Get pointers to the rectified image buffers
  rectImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  dispImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

  // Camera model
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  cx = model.ext.cahv.hc;
  cy = model.ext.cahv.vc;
  sx = model.ext.cahv.hs;
  sy = model.ext.cahv.vs;
  baseline = jplv_stereo_get_baseline(this->stereo);

  // Camera-to-vehicle transform
  assert(sizeof(mv) == sizeof(this->sens2veh));
  memcpy(mv, this->sens2veh, sizeof(mv));

  // Vehicle-to-local transform
  pose.pos = vec3_set(this->state.localX,
                      this->state.localY,
                      this->state.localZ);
  pose.rot = quat_from_rpy(this->state.localRoll,
                           this->state.localPitch,
                           this->state.localYaw);
  pose3_to_mat44d(pose, ml);

  rpix = (uint8_t*) jplv_image_pixel(rectImage, 0, 0);
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < rectImage->rows; pj++)
  {
    for (pi = 0; pi < rectImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);
      px = px * pz;
      py = py * pz;

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      // Convert to vehicle coordinates
      vx = mv[0][0] * px + mv[0][1] * py + mv[0][2] * pz + mv[0][3];
      vy = mv[1][0] * px + mv[1][1] * py + mv[1][2] * pz + mv[1][3];
      vz = mv[2][0] * px + mv[2][1] * py + mv[2][2] * pz + mv[2][3];

      // Convert to local coordinates
      qx = ml[0][0] * vx + ml[0][1] * vy + ml[0][2] * vz + ml[0][3];
      qy = ml[1][0] * vx + ml[1][1] * vy + ml[1][2] * vz + ml[1][3];

      // Checker board
      if (false)
      {
        qi = (int) ((qx + 100000) / 1.00);
        qj = (int) ((qy + 100000) / 1.00);
        rpix[0] = 0x80 + ((qi + qj) % 2) * 0x7F;
      }

      // Lines every 20m
      if (true)
      {
        rpix[0] = 0x20;
        if (fmod(qx + 1e6, 20) < 0.12)
          rpix[0] = 0xD0;
        if (fmod(qy + 1e6, 20) < 0.12)
          rpix[0] = 0xD0;
      }

      rpix += rectImage->channels;
      dpix += 1;
    }
  }
  
  this->frameId += 1;
  this->frameTime = timestamp;

  if (this->console)
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %.3f",
                this->frameId, fmod(1e-6 * (double) this->frameTime, 10000));
  
  return 0;
}


// Initialize feeder for live capture
int StereoFeeder::initLive(const char *configPath)
{
  char filename[256];

  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;
  
  // Initialize the camera
  this->camera = bb_camera_alloc(this->options.camera_arg);
  if (!this->camera)
    return ERROR("unable to connect to camera %s", this->options.camera_arg);
  if (bb_camera_init(this->camera, this->options.camera_config_arg) != 0)
    return -1;

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = this->camera->image.width >> this->imageLevel;
  this->imageRows = this->camera->image.height >> this->imageLevel;
  this->imageChannels = 1; // HACK this->camera->image.channels;

  MSG("image %dx%dx%d", this->imageCols, this->imageRows, this->imageChannels);
      
  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-left.cahvor", configPath, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%d-right.cahvor", configPath, this->cameraId);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Create buffer for raw image capture
  this->capImage = jplv_image_alloc(this->imageCols << this->imageLevel,
                                    this->imageRows << this->imageLevel,
                                    this->imageChannels, 16, 0, NULL);
  
  this->mode = modeLive;
  
  return 0;
}


// Finalize feeder for live capture
int StereoFeeder::finiLive()
{  
  bb_camera_fini(this->camera);
  bb_camera_free(this->camera);
  this->camera = NULL;

  jplv_image_free(this->capImage);
    
  return 0;
}


// Capture a frame 
int StereoFeeder::captureLive()
{
  int exp;
  uint64_t cycleTime;
  jplv_image_t *leftImage, *rightImage;
  
  // Get pointers to the raw image buffers
  leftImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(this->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
    
  // Read images off the camera
  bb_camera_capture(this->camera, this->capImage);

  // Demultiplex the image and measure overall image exposure
  bb_camera_demux_mono(this->capImage, this->imageLevel, &exp, leftImage, rightImage);
  
  // Set measured exposure
  bb_camera_auto_gain(this->camera, 127, exp);

  // Get timestamp
  this->frameId += 1;  
  this->frameTime = this->camera->timestamp;

  // Get the matching state data
  if (this->getState(this->frameTime) != 0)
    return -1;

  // Compute some stats
  cycleTime = DGCgettime() - this->capTime;
  this->capTime = DGCgettime();

  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %.3f",
                this->frameId, fmod(1e-6 * (double) this->frameTime, 10000), exp);
    if (cycleTime < 1000000)
      cotk_printf(this->console, "%capstats%", A_NORMAL, "%.1fHz/%03dms %3d",
                  (float) (1e6 / cycleTime), (int) (cycleTime / 1000), exp);
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+06dms", (int) (this->state.timestamp - this->frameTime) / 1000);
  }

  return 0;
}


// Initialize everything except image capture
int StereoFeeder::init()
{
  if (true)
  {    
    // Create stereo context
    this->stereo = jplv_stereo_alloc(this->imageCols, this->imageRows, this->imageChannels);
    if (!this->stereo)
      return ERROR("unable to allocate stereo context: JPLV %s", jplv_error_str());
    
    // Re-scale the models if ncessary
    jplv_cmod_scale_dims(&this->leftModel, this->imageCols, this->imageRows);
    jplv_cmod_scale_dims(&this->rightModel, this->imageCols, this->imageRows);

    // Set the model in the stereo context
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_LEFT, &this->leftModel);
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &this->rightModel);

    // Set defaults
    jplv_stereo_set_disparity(this->stereo, 0, 40, 7, 7);

    // Prepare for action
    jplv_stereo_prepare(this->stereo);
  }
    
  if (true)
  {
    // Initialize SensNet
    this->sensnet = sensnet_alloc();
    if (sensnet_connect(this->sensnet,
                        this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return ERROR("unable to connect to sensnet");

    // Subscribe to vehicle state messages
    if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState), 100) != 0)
      return ERROR("unable to join state group");
  }

#if USE_FB
  if (this->options.enable_fb_given)
  {
    struct fb_var_screeninfo vinfo;

    // Initialize the frame buffer
    this->fb = frame_buffer_open("/dev/fb0");
    if (this->fb == NULL)
      return ERROR("unable to open frame buffer");

    // Check the screen dimensions
    frame_buffer_get_vinfo(this->fb, &vinfo);
    if (vinfo.bits_per_pixel != 32)
      return ERROR("frame buffer must be 32bpp, not %dbpp", vinfo.bits_per_pixel);
  }
#endif

  return 0;
}


// Finalize everything except capture
int StereoFeeder::fini()
{  
  // Clean up SensNet
  if (sensnet_disconnect(this->sensnet) != 0)
    return ERROR("unable to connect to sensnet");
  sensnet_free(this->sensnet);
  this->sensnet = NULL;

  // Clean up stereo
  jplv_stereo_free(this->stereo);
  this->stereo = NULL;
  
  return 0;
}


// Get the predicted vehicle state
int StereoFeeder::getState(double timestamp)
{
  VehicleState state;

  memset(&this->state, 0, sizeof(this->state));
      
  // Get the state newest data in the cache
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, -1, sizeof(state), &state) != 0)
    return MSG("unable to read state");
  
  // TODO: do prediction by comparing image/state timestamps
  
  this->state = state;
  
  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
                fmod((double) state.timestamp * 1e-6, 10000));
    cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
                state.localX, state.localY, state.localZ);
    cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
                state.localRoll*180/M_PI, state.localPitch*180/M_PI, state.localYaw*180/M_PI);
  }

  return 0;
}


// Process a frame
int StereoFeeder::process()
{
  uint64_t cpuTime, cycleTime;
  float frac;

  cpuTime = DGCgettime();
  
  if (this->mode != modeSim)
  {
    // Run regular stereo pipeline
    jplv_stereo_rectify(this->stereo);  
    jplv_stereo_laplace(this->stereo);

    if (this->options.force_plane_flag)
    {
      // Run fake stereo using ground-plane constraint
      this->calcPlaneDisp();
    }
    else
    {
      // Run real stereo
      jplv_stereo_disparity(this->stereo);
    }
  }
  
  jplv_stereo_range(this->stereo);  
  jplv_stereo_diag(this->stereo);

  // Compute elapsed time for update
  cpuTime = DGCgettime() - cpuTime;
  cycleTime = DGCgettime() - this->stereoTime;
  this->stereoTime = DGCgettime();
    
  // Compute some diagnostics
  frac = (float) this->stereo->diag_num_valid /
    (this->stereo->image_cols * this->stereo->image_rows);

  if (this->console)
  {
    cotk_printf(this->console, "%stid%", A_NORMAL, "%5d %.3f",
                this->frameId,  fmod(1e-6 * (double) this->frameTime, 10000));
    if (cycleTime < 1000000)
    {
      cotk_printf(this->console, "%ststats%", A_NORMAL, "%.1fHz/%03dms %3dms %3d%%",
                  (float) (1e6 / cycleTime), (int) (cycleTime / 1000),
                  (int) (cpuTime / 1000), (int) (frac * 100));
    }
  }

  return 0;
}


// Run fake stereo using ground-plane constraint
int StereoFeeder::calcPlaneDisp()
{
  jplv_image_t *dispImage;
  uint16_t *dpix;
  int pi, pj;
  float px, py, pz;
  float cx, cy, sx, sy, baseline;
  float mv[4][4];

  // Disparity image
  dispImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

  // Camera model
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  cx = model.ext.cahv.hc;
  cy = model.ext.cahv.vc;
  sx = model.ext.cahv.hs;
  sy = model.ext.cahv.vs;    
  baseline = jplv_stereo_get_baseline(this->stereo);

  // Camera-to-vehicle transform
  assert(sizeof(mv) == sizeof(this->sens2veh));
  memcpy(mv, this->sens2veh, sizeof(mv));

  // TODO: consider robot attitude
  
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < dispImage->rows; pj++)
  {
    for (pi = 0; pi < dispImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      dpix += 1;
    }
  }

  return 0;
}


// Display image to framebuffer
int StereoFeeder::displayImage()
{
#if USE_FB
  jplv_image_t *image;
  struct fb_var_screeninfo vinfo;

  // Get the screen dimensions
  frame_buffer_get_vinfo(this->fb, &vinfo);

  // Draw raw left on upper right corner
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  frame_buffer_draw(this->fb, vinfo.xres - image->cols, 0, image->cols, image->rows,
                    image->channels, image->bits/8, image->data);

  /* TODO: fix this (causes display to fail)
  // Draw diagnostics on lower right corner
  image = jplv_stereo_get_image(this->stereo,
  JPLV_STEREO_STAGE_DIAG, JPLV_STEREO_CAMERA_LEFT);
  frame_buffer_draw(this->fb,
  vinfo.xres - image->cols, image->rows, image->cols, image->rows,
  image->channels, image->bits/8, image->data);
  */
#endif


#if USE_AA
  int i, j;
  uint8_t *src;
  jplv_image_t *image;

  if (!this->aa)
    return 0;
  if (!this->console)
    return 0;

  // Draw rescaled image
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  for (j = 0; j < image->rows; j += 8)
  {
    src = jplv_image_pixel(image, 0, j);
    for (i = 0; i < image->cols; i += 8)
    {
      aa_putpixel(this->aa, i / 8, j / 8, src[0]);
      src += 8 * image->channels;
    }
  }
    
  // Render to memory
  aa_fastrender(this->aa, 0, 0, aa_scrwidth(this->aa), aa_scrheight(this->aa));
  aa_flush(this->aa);

  // Render to console
  cotk_draw_image(this->console, 80 - aa_scrwidth(this->aa), 0,
                  aa_scrwidth(this->aa), aa_scrheight(this->aa),
                  aa_text(this->aa), aa_attrs(this->aa));
#endif
  
  return 0;
}


// Write the current images to a file
int StereoFeeder::writeFile()
{
  jplv_image_t *image;
  char filename[256];
  
  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_LEFT.pnm", sensnet_id_to_name(this->sensorId), this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_RIGHT.pnm", sensnet_id_to_name(this->sensorId), this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  this->exportId++;
  
  if (this->console)
    cotk_printf(this->console, "%export%", A_NORMAL, "%d", this->exportId);

  return 0;
}


// Publish data
int StereoFeeder::writeSensnet()
{
  pose3_t pose;
  StereoImageBlob blob;
  jplv_image_t *image;
  
  blob.blobType = SENSNET_STEREO_BLOB;
  blob.sensorId = this->sensorId;
  blob.frameId = this->frameId;
  blob.timestamp = this->frameTime;
  blob.state = this->state;

  blob.cols = this->stereo->image_cols;
  blob.rows = this->stereo->image_rows;

  // Get the rectified model and copy to blob
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  blob.cx = model.ext.cahv.hc;
  blob.cy = model.ext.cahv.vc;
  blob.sx = model.ext.cahv.hs;
  blob.sy = model.ext.cahv.vs;    
  blob.dispScale = JPLV_STEREO_DISP_SCALE;
  blob.baseline = jplv_stereo_get_baseline(this->stereo);
  assert(blob.baseline > 0);

  // Copy camera transform
  assert(sizeof(this->sens2veh) == sizeof(blob.sens2veh));
  memcpy(blob.sens2veh, this->sens2veh, sizeof(this->sens2veh));

  // Vehicle to local transform
  pose.pos = vec3_set(blob.state.localX,
                      blob.state.localY,
                      blob.state.localZ);
  pose.rot = quat_from_rpy(blob.state.localRoll,
                           blob.state.localPitch,
                           blob.state.localYaw);  
  pose3_to_mat44f(pose, blob.veh2loc);  

  // Copy rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  blob.colorChannels = image->channels;
  blob.colorSize = image->data_size;
  assert(sizeof(blob.colorData) >= (size_t) image->data_size);
  memcpy(blob.colorData, image->data, image->data_size);
  
  // Copy disparity data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  blob.dispSize = image->data_size;
  assert(sizeof(blob.dispData) >= (size_t) image->data_size);
  memcpy(blob.dispData, image->data, image->data_size);

  // Send rectified color image
  if (sensnet_write(this->sensnet, this->sensorId, SENSNET_STEREO_BLOB,
                    this->frameId, sizeof(blob), &blob) != 0)
    return ERROR("unable to write blob");

  return 0;
}


// Initialize logging for writing
int StereoFeeder::openLog(const char *configPath)
{
  time_t t;
  char timestamp[64];
  char logDir[256];
  char cmd[256];
  stereo_log_header_t header;

  // Construct log name
  t = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M-%S", localtime(&t));
  snprintf(logDir, sizeof(logDir), "%s/%s-%s",
           this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));
    
  // Create log file
  this->log = stereo_log_alloc();
  assert(this->log);

  // Construct log header
  memset(&header, 0, sizeof(header));
  header.camera_id = this->cameraId;
  header.cols = this->imageCols;
  header.rows = this->imageRows;
  header.channels = this->imageChannels;

  // Open log file for writing
  if (stereo_log_open_write(this->log, logDir, &header) != 0)
    return ERROR("unable to open log: %s", logDir);

  // Copy configuration files
  snprintf(cmd, sizeof(cmd), "cp %s/%d-*.cahvor %s",
           configPath, this->cameraId, logDir);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
           configPath, sensnet_id_to_name(this->sensorId), logDir);
  system(cmd);

  return 0;
}


// Finalize log
int StereoFeeder::closeLog()
{
  if (!this->log)
    return 0;

  stereo_log_close(this->log);
  stereo_log_free(this->log);
  this->log = NULL;
  
  return 0;
}


// Write the current images to the log
int StereoFeeder::writeLog()
{
  stereo_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  if (!this->log)
    return 0;

  // Construct the image tag
  tag.frameid = this->frameId;
  tag.timestamp = this->frameTime;
  tag.state = this->state;  
  memcpy(tag.sens2veh, this->sens2veh, sizeof(tag.sens2veh));
  
  // Get current raw images
  leftImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(this->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

  // Write to log
  if (stereo_log_write(this->log, &tag,
                       leftImage->data_size, leftImage->data, rightImage->data) != 0)
    return ERROR("unable to write to log");

  // Update stats
  this->logCount += 1;
  this->logSize += leftImage->data_size + rightImage->data_size;

  // Update console
  if (this->console)
  {
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->logCount, this->logSize / 1024 / 1024);
  }
  
  return 0;
}



// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"StereoFeeder $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"                                                                           \n"
"Capture                                State                               \n"
"Mode  : %mode%                         Time   : %stime%                    \n"
"Camera: %cam%                          Pos    : %spos%                     \n"
"Frame : %capid%                        Rot    : %srot%                     \n"
"Stats : %capstats%                     Latency: %slat%                     \n"
"                                                                           \n"
"Stereo                                 Misc                                \n"
"Frame : %stid%                         Log   : %log%                       \n"
"Stats : %ststats%                      Export: %export%                    \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%LOG%|%DISPLAY%|%EXPORT%]                                  \n";



// Initialize console display
int StereoFeeder::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);
  cotk_bind_toggle(this->console, "%DISPLAY%", " DISPLAY ", "Dd",
                   (cotk_callback_t) onUserDisplay, this);
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);
    
  // Initialize the display
  cotk_open(this->console);

  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));
  cotk_printf(this->console, "%cam%", A_NORMAL, "%5d %dx%dx%d L%d", this->cameraId,
              this->imageCols, this->imageRows, this->imageChannels, this->imageLevel);
    
  if (this->mode == modeSim)
    cotk_printf(this->console, "%mode%", A_NORMAL, "sim   ");
  else if (this->mode == modeLive)
    cotk_printf(this->console, "%mode%", A_NORMAL, "live  ");

#if USE_AA
  if (true)
  {
    struct aa_hardware_params params;
    params = aa_defparams;
    params.supported = AA_NORMAL_MASK;
    params.width = this->imageCols / 8 / 2;
    params.height = this->imageRows / 8 / 2;
    this->aa = aa_init(&mem_d, &params, NULL);
    if (!this->aa)
      ERROR("aalib initialization failed");
  }
#endif
  
  return 0;
}


// Finalize console display
int StereoFeeder::finiConsole()
{
#if USE_AA
  if (this->aa)
    aa_close(this->aa);
#endif

  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserQuit(cotk_t *console, StereoFeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserPause(cotk_t *console, StereoFeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserLog(cotk_t *console, StereoFeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Handle user events; occurs in sparrow thread
int StereoFeeder::onUserDisplay(cotk_t *console, StereoFeeder *self, const char *token)
{
  assert(self); 
  self->enableDisplay = !self->enableDisplay;
  MSG("display %s", (self->enableDisplay ? "on" : "off"));
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserExport(cotk_t *console, StereoFeeder *self, const char *token)
{
  // Write out the current image to a file
  MSG("exporting image");
  self->writeFile();
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  int status;
  uint64_t lastTime;
  StereoFeeder *feeder;

  // Create feeder
  feeder = new StereoFeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  if (feeder->options.sim_flag)
  {
    // Initialize for simulated capture
    if (feeder->initSim(feeder->defaultConfigPath) != 0)
      return -1;
  }
  else
  {
    // Initialize for live capture
    if (feeder->initLive(feeder->defaultConfigPath) != 0)
      return -1;
  }

  // Initialize logging
  if (feeder->options.enable_log_flag)
    if (feeder->openLog(feeder->defaultConfigPath) != 0)
      return -1;

  // Initialize algorithms
  if (feeder->init() != 0)
    return -1;

  // Initialize console
  if (!feeder->options.disable_console_flag)
    feeder->initConsole();

  lastTime = 0;
  feeder->startTime = DGCgettime();
  
  // Start processing
  while (!feeder->quit)
  {
    // Capture incoming frame directly into the stereo buffers
    if (feeder->mode == StereoFeeder::modeSim)
    {
      if (feeder->captureSim() != 0)
        break;
    }
    else
    {
      if (feeder->captureLive() != 0)
        break;
    }

    // Display current image on framebuffer
    if (feeder->options.enable_fb_flag && feeder->enableDisplay)
      feeder->displayImage();
    
    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);

    // If paused, or if we are running too fast, give up our time
    // slice.
    if (feeder->pause || DGCgettime() - lastTime < (uint64_t) (1e6/feeder->options.rate_arg))
    {
      usleep(20000);
      continue;
    }
    lastTime = DGCgettime();
    
    // Capture and process one frame
    status = feeder->process();
    if (status != 0)
      break;

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);

    // Publish data
    if (feeder->writeSensnet() != 0)
      break;
        
    // Write to the log
    if (feeder->options.enable_log_flag &&
        (feeder->enableLog || feeder->options.disable_console_flag))
      feeder->writeLog();
  }
  
  // Clean up
  feeder->finiConsole();
  feeder->fini();
  if (feeder->log)
    feeder->closeLog();
  if (feeder->mode == StereoFeeder::modeSim)
    feeder->finiSim();
  else
    feeder->finiLive();  
  delete feeder;

  MSG("program exited cleanly");
  
  return 0;
}
