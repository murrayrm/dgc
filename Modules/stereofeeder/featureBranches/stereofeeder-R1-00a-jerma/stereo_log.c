
/* Desc: DGC stereo data logging
 * Author: Andrew Howard
 * Date: 9 Nov 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h> // for pool management
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <rpc/xdr.h>

// GNU extensions
#ifndef O_DIRECT
#define O_DIRECT         040000 /* direct disk access hint */
#endif

#include "stereo_log.h"


// Logging context.  
struct stereo_log
{
  // Log directory
  char *logdir;

  // Metadata file
  FILE *file;

  // Metadata stream
  XDR xdrs;

  // Number of frames written/read
  int num_frames;
};


// Serialization functions
static bool xdr_header(XDR *xdrs, stereo_log_header_t *header);
static bool xdr_state(XDR *xdrs, stereo_log_state_t *state);
static bool xdr_tag(XDR *xdrs, stereo_log_tag_t *tag);

// Fast image read/write
int read_bytes(char *filename, off_t offset, int count, uint8_t *data);
int write_bytes(char *filename, off_t offset, int count, const uint8_t *data);


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stdout, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Allocate object
stereo_log_t *stereo_log_alloc()
{
  stereo_log_t *self;

  self = calloc(1, sizeof(stereo_log_t));

  return self;
}


// Free object
void stereo_log_free(stereo_log_t *self)
{
  if (self->logdir)
    free(self->logdir);
  free(self);

  return;
}


// Open file for writing
int stereo_log_open_write(stereo_log_t *self,
                              const char *logdir, stereo_log_header_t *header)
{
  char filename[1024];
  
  self->logdir = strdup(logdir);

  // Create directory
  if (mkdir(self->logdir, 0x1ED) != 0) 
    return ERROR("unable to create: %s %s", self->logdir, strerror(errno));

  // Create index file
  snprintf(filename, sizeof(filename), "%s/index.xdr", self->logdir);  
  self->file = fopen(filename, "w");
  if (!self->file)
    return ERROR("unable to open: %s %s", filename, strerror(errno));

  // Overwrite version
  header->version = STEREO_LOG_VERSION;

  // Create the encoding stream
  xdrstdio_create(&self->xdrs, self->file, XDR_ENCODE);

  // Encode the header
  if (!xdr_header(&self->xdrs, header))
    return ERROR("unable to encode log header");
      
  return 0;
}


// Open file for reading
int stereo_log_open_read(stereo_log_t *self,
                             const char *logdir, stereo_log_header_t *header)
{
  char filename[1024];
  
  self->logdir = strdup(logdir);

  // Open index file
  snprintf(filename, sizeof(filename), "%s/index.xdr", self->logdir);  
  self->file = fopen(filename, "r");
  if (!self->file)
    return ERROR("unable to open: %s %s", filename, strerror(errno));

  // Create the decoding stream
  xdrstdio_create(&self->xdrs, self->file, XDR_DECODE);

  // Decode the header
  if (!xdr_header(&self->xdrs, header))
    return ERROR("unable to decode log header");

  // Check the version
  if (header->version != STEREO_LOG_VERSION)
    return ERROR("log is version %d, code is version %d", header->version, STEREO_LOG_VERSION);
  
  return 0;
}


// Close the log
int stereo_log_close(stereo_log_t *self)
{
  xdr_destroy(&self->xdrs);
  fclose(self->file);
  return 0;
}


// Write an image to the log
int stereo_log_write(stereo_log_t *self, stereo_log_tag_t *tag,
                         int data_size, const uint8_t *left_data, const uint8_t *right_data)
{
  int index;
  off_t offset;
  char filename[512];
  const unsigned long max_size = 2ul*1024*1024*1024;

  // Overwrite the index with our reference
  tag->index = self->num_frames++;
  
  // Write tag data
  if (!xdr_tag(&self->xdrs, tag))
    return ERROR("unable to write image tag");
  fflush(self->file);

  // Figure out which 2GB file the frame is in, and at what offset
  assert(tag->index >= 0);
  index = (unsigned long long) tag->index * data_size / max_size;
  offset = (unsigned long long) tag->index * data_size % max_size;

  // Write left bytes
  snprintf(filename, sizeof(filename), "%s/left-%02d.rgb", self->logdir, index);  
  if (write_bytes(filename, offset, data_size, left_data))
    return ERROR("unable to write image: %s", filename);

  // Write right bytes
  snprintf(filename, sizeof(filename), "%s/right-%02d.rgb", self->logdir, index);  
  if (write_bytes(filename, offset, data_size, right_data))
    return ERROR("unable to write image: %s", filename);

  return 0;
}


// Read an image from the log
int stereo_log_read(stereo_log_t *self, stereo_log_tag_t *tag,
                        int data_size, uint8_t *left_data, uint8_t *right_data)
{
  int index;
  off_t offset;
  char filename[1024];
  const unsigned long long max_size = 2ul*1024*1024*1024;

  if (feof(self->file))
    return ERROR("end-of-file");

  // Read tag data
  if (!xdr_tag(&self->xdrs, tag))
    return ERROR("unable to read image tag");

  // Record number of frames read
  self->num_frames = tag->index + 1;
  
  // Figure out which 2GB file the frame is in, and at what offset
  assert(tag->index >= 0);
  index = (unsigned long long) tag->index * data_size / max_size;
  offset = (unsigned long long) tag->index * data_size % max_size;

  // Read bytes
  snprintf(filename, sizeof(filename), "%s/left-%02d.rgb", self->logdir, index);  
  if (read_bytes(filename, offset, data_size, left_data))
    return ERROR("unable to read image: %s", filename);

  // Read bytes
  snprintf(filename, sizeof(filename), "%s/right-%02d.rgb", self->logdir, index);  
  if (read_bytes(filename, offset, data_size, right_data))
    return ERROR("unable to read image: %s", filename);

  return 0;
}


// Serialize header
bool xdr_header(XDR *xdrs, stereo_log_header_t *header)
{
  if (!xdr_int(xdrs, &header->version))
    return false;
  if (!xdr_int(xdrs, &header->camera_id))
    return false;
  if (!xdr_int(xdrs, &header->cols))
    return false;
  if (!xdr_int(xdrs, &header->rows))
    return false;
  if (!xdr_int(xdrs, &header->channels))
    return false;
  
  return true;
}


// Serialize state 
bool xdr_state(XDR *xdrs, stereo_log_state_t *state)
{
  int i;

  // Timestamp
  if (!xdr_double(xdrs, &state->timestamp))
    return false;
  
  // Local pose
  for (i = 0; i < 6; i++)
    if (!xdr_double(xdrs, &state->pose_local[i]))
      return false;

  // Global pose
  for (i = 0; i < 6; i++)
    if (!xdr_double(xdrs, &state->pose_global[i]))
      return false;

  // Rates
  for (i = 0; i < 6; i++)
    if (!xdr_double(xdrs, &state->vel_vehicle[i]))
      return false;

  return true;
}


// Serialize tag
bool xdr_tag(XDR *xdrs, stereo_log_tag_t *tag)                
{
  int i, j;
  
  if (!xdr_int(xdrs, &tag->index))
    return false;
  if (!xdr_int(xdrs, &tag->frameid))
    return false;
  if (!xdr_double(xdrs, &tag->timestamp))
    return false;
  if (!xdr_state(xdrs, &tag->state))
    return false;
  
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      if (!xdr_float(xdrs, &tag->sens2veh[j][i]))
        return false;

  return true;
}


// Write bytes directly to disk
int write_bytes(char *filename, off_t offset, int count, const uint8_t *data)
{
  int page_size;
  int fid = -1, direct = 0;
  int wrote = 0;

  page_size = sysconf(_SC_PAGESIZE);
  if (((unsigned long)data % page_size) == 0 && (count%page_size) == 0)
  { // We can write directly to disk
    fid = open(filename, O_CREAT | O_WRONLY | O_APPEND | O_DIRECT, 00666 );
    if (fid != -1)
      direct = 1;
  }
  if (fid == -1)
  {
    // TODO fix O_DIRECT method fprintf(stderr, "Opening %s O_DIRECT failed\n", filename);
    fid = open(filename, O_CREAT | O_WRONLY | O_APPEND , 00666 );
  }
  if (fid == -1)
    return ERROR("open failed on %s: %s", filename, strerror(errno));

  if (lseek(fid, offset, SEEK_SET) < 0)
    return ERROR("seek failed on %s : %s", filename, strerror(errno));

  wrote = write(fid, data, count);
  if (wrote != count)
  {
    close(fid);
    return ERROR("write failed on: %s (%d of %d)", 
                 strerror(errno), wrote, count);
  }
  if (!direct) fsync(fid);
  close(fid);
  return 0;
}


// Read bytes directly from disk
int read_bytes(char *filename, off_t offset, int count, uint8_t *data)
{
  int fid = -1;
  // Don't open direct here.  Read cache can help performance
  fid = open(filename, O_RDONLY);
  if (fid == -1)
    return ERROR("open failed on %s : %s", filename, strerror(errno));
  if (lseek(fid, offset, SEEK_SET) < 0)
    return ERROR("seek failed on %s : %s", filename, strerror(errno));
  if (read(fid, data, count) != count)
    return ERROR("read failed on %s offset %ld", filename, offset);
  close(fid);
  return 0;
}




#ifdef STEREO_LOG_TEST

// Simple self-test.
//
// Build with:
// gcc -Wall -DSTEREO_LOG_TEST stereo_log.c -o stereo-log-test
int main(int argc, char **argv)
{
  char *filename;
  stereo_log_t *log;
  stereo_log_header_t header;
  stereo_log_tag_t tag;
  int image_size;
  uint8_t *left, *right;

  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  log = stereo_log_alloc();
  assert(log);

  if (stereo_log_open_read(log, filename, &header) != 0)
    return -1;

  printf("version: %X\n", header.version);
  printf("image: %d %d %d\n", header.cols, header.rows, header.channels);

  image_size = header.cols * header.rows * header.channels;
  left = malloc(image_size);
  right = malloc(image_size);

  while (true)
  {
    if (stereo_log_read(log, &tag, image_size, left, right) != 0)
      break;

    printf("image %d %.3f\n",
           tag.frameid, tag.timestamp);
  }

  free(left);
  free(right);
  
  stereo_log_free(log);
  
  return 0;
}

#endif
