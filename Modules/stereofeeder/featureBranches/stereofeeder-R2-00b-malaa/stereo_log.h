
/* Desc: DGC stereo data logging
 * Author: Andrew Howard
 * Date: 9 Nov 2005
 * CVS: $Id$
 */

#ifndef STEREO_LOG_H
#define STEREO_LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>
#include <interfaces/VehicleState.h>  

  
/// @brief Log version number
#define STEREO_LOG_VERSION 0x05


/// @brief Log header data
typedef struct
{
  /// File version (read-only)
  int version;

  /// Camera ids
  int left_camera, right_camera;
  
  /// Image dimensions
  int cols, rows, channels;
  
} stereo_log_header_t;
   

/// @brief Image tag data
typedef struct
{
  // Image index (read-only internal variable)
  int index;

  /// Frame id
  int frameid;

  /// Frame timestamp (number of microseconds since the epoch)
  uint64_t timestamp;
  
  /// Sensor-to-vehicle transform
  float sens2veh[4][4];

  /// Camera exposure settings
  float gain[2], shutter[2];

  /// Reserved space (all zeros)
  uint32_t reserved[16];

  /// Vehicle state data
  VehicleState state;

} stereo_log_tag_t;


/// @brief Stereo log context (opaque).
typedef struct stereo_log stereo_log_t;

/// @brief Allocate object
stereo_log_t *stereo_log_alloc();

/// @brief Free object
void stereo_log_free(stereo_log_t *self);

/// @brief Open file for writing
int stereo_log_open_write(stereo_log_t *self,
                          const char *logdir, stereo_log_header_t *header);

/// @brief Open file for reading
int stereo_log_open_read(stereo_log_t *self,
                         const char *logdir, stereo_log_header_t *header);

/// @brief Close the log
int stereo_log_close(stereo_log_t *self);

/// @brief Write an image to the log
///
/// @param[in] tag Image tag.
/// @param[in] data_size Size of image data, in bytes.
/// @param[in] left_data Left image data.
/// @param[in] right_data Right image data.
int stereo_log_write(stereo_log_t *self, stereo_log_tag_t *tag,
                     int data_size, const uint8_t *left_data, const uint8_t *right_data);

/// @brief Read an image from the log
///
/// @param[out] tag Image tag.
/// @param[in] data_size Size of image data, in bytes.
/// @param[out] left_data Left image data.
/// @param[out] right_data Right image data.
int stereo_log_read(stereo_log_t *self, stereo_log_tag_t *tag,
                    int data_size, uint8_t *left_data, uint8_t *right_data);


#ifdef __cplusplus
}
#endif

#endif
