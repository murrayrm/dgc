/* 
 * Desc: Stereo camera pair with discrete PGR cameras.
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <libdc1394/dc1394_control.h>
#include <jplv/filters.h>
#include <dgcutils/DGCutils.hh>

#include "PGRStereoCamera.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
PGRStereoCamera::PGRStereoCamera()
{
  memset(this->cameras, 0, sizeof(this->cameras));
  memset(this->images, 0, sizeof(this->images));  
  return;
}


// Destructor
PGRStereoCamera::~PGRStereoCamera()
{
  return;
}
  

// Initialize the camera
int PGRStereoCamera::open(const char *leftCameraId, const char *leftCameraConfig,
                          const char *rightCameraId, const char *rightCameraConfig)
{
  // Initialize cameras
  MSG("camera: %s", leftCameraId);
  if ((this->cameras[0] = vis_dev_alloc(leftCameraId, NULL)) == NULL)
    return ERROR("unable to create camera %s", leftCameraId);
  MSG("camera: %s", rightCameraId);
  if ((this->cameras[1] = vis_dev_alloc(rightCameraId, NULL)) == NULL)
    return ERROR("unable to create camera %s", rightCameraId);

  // Configure cameras
  MSG("camera config: %s", leftCameraConfig);
  if (vis_dev_config(this->cameras[0], (unsigned char*)leftCameraConfig, strlen(leftCameraConfig)))
    return ERROR("failed to configure camera with %s", leftCameraConfig);
  MSG("camera config: %s", rightCameraConfig);
  if (vis_dev_config(this->cameras[1],  (unsigned char*)rightCameraConfig, strlen(rightCameraConfig)))
    return ERROR("failed to configure camera with %s", rightCameraConfig);
  
  // Initialize image
  if (vis_dev_img_header(this->cameras[0], this->images + 0))
    return ERROR("failed to read image header");
  if (vis_dev_img_header(this->cameras[1], this->images + 1))
    return ERROR("failed to read image header");

  // Record key image dimensions
  MSG("left  %s %dx%dx%d",
      leftCameraId, this->images[0].width, this->images[0].height, this->images[0].channels);
  MSG("right %s %dx%dx%d",
      rightCameraId, this->images[1].width, this->images[1].height, this->images[1].channels);

  this->imageCols = this->images[0].width;
  this->imageRows = this->images[0].height;
  this->imageChannels = this->images[0].channels;

  // Allocate space for raw images
  this->images[0].pixels = malloc(this->images[0].img_size);
  this->images[1].pixels = malloc(this->images[1].img_size);

  // Start capturing
  vis_dev_start(this->cameras[0]);
  vis_dev_start(this->cameras[1]);
  MSG("capture enabled");
  
  return 0;
}


// Close the camera
int PGRStereoCamera::close()
{
  MSG("capture disabled");
  vis_dev_stop(this->cameras[0]);
  vis_dev_stop(this->cameras[1]);
  free(this->images[0].pixels);
  free(this->images[1].pixels);
  this->images[0].pixels = NULL;
  this->images[1].pixels = NULL;
  vis_dev_free(this->cameras[0]);
  vis_dev_free(this->cameras[1]);
  this->cameras[0] = NULL;
  this->cameras[1] = NULL;  

  return 0;
}


// Capture a frame
int PGRStereoCamera::capture(int timeout, jplv_image_t *leftImage, jplv_image_t *rightImage)
{
  uint64_t timestamp, timestamps[2];
  uint64_t timeover;

  // Compute time at which we will give up
  timeover = DGCgettime() + (uint64_t) (timeout * 1000);

  while (true)
  {
    // Check for new images
    vis_dev_read_imgs(2, this->cameras, this->images);  

    // See if we have new data
    timestamps[0] =
      (uint64_t) this->images[0].fill_time.tv_sec * 1000000 +
      (uint64_t) this->images[0].fill_time.tv_usec;
    timestamps[1] =
      (uint64_t) this->images[1].fill_time.tv_sec * 1000000 +
      (uint64_t) this->images[1].fill_time.tv_usec;
    //MSG("%lld %lld", timestamps[0], timestamps[1]);
    timestamp = (timestamps[0] < timestamps[1] ? timestamps[0] : timestamps[1]);    
    if (timestamp != this->timestamp)
      break;

    // See if we have timedout
    if (DGCgettime() > timeover)
      return ERROR("timeout on image capture");
    
    // Yield the CPU
    usleep(0);
  }
  
  // Update current frame info
  this->frameId += 1;
  this->timestamp = timestamp;  

  // Demux the image into left and right
  this->copy(this->images + 0, leftImage);
  this->copy(this->images + 1, rightImage);

  // Measure current exposure in ROI
  this->exposure = 0;
  this->exposure += this->measureAGC(leftImage);
  // TODO this->exposure += this->measureAGC(rightImage);
  // this->exposure /= 2;

  // Update the AGC
  this->controlAGC(this->goalExp, this->exposure);
  
  return 0;
}


// Initialize software exposure control
int PGRStereoCamera::initAGC(int exposure)
{
  int i;
  dc1394_feature_info gain, shutter;
  
  this->enableAGC = true;
  this->goalExp = exposure;

  // Get readings for both cameras
  for (i = 0; i < 2; i++)
  {  
    // Get gain range
    memset(&gain, 0, sizeof(gain));
    dc_1394_command(this->cameras[i], (unsigned char *)"gain", 4, &gain, sizeof(gain));
    if (gain.feature_id == FEATURE_GAIN)
    {
      this->gainMin = gain.abs_min;
      this->gainMax = gain.abs_max;
      this->gain = gain.abs_value;
      MSG("gain: %.0f [%.0f %.0f]", this->gain, this->gainMin, this->gainMax);
    }
    else
    {
      this->enableAGC = false;
      MSG("unable to read gain; softwareAGC disabled");
    }
  
    // Get shutter range
    memset(&shutter, 0, sizeof(shutter));
    dc_1394_command(this->cameras[i], (unsigned char*)"shutter", 4, &shutter, sizeof(shutter));
    if (shutter.feature_id == FEATURE_SHUTTER)
    {
      this->shutterMin = shutter.abs_min*1000; // HACK
      this->shutterMax = shutter.abs_max*1000;
      this->shutter = shutter.abs_value*1000;
      MSG("shutter: %.2f [%.2f %.2f]", this->shutter, this->shutterMin, this->shutterMax);
    }
    else
    {
      this->enableAGC = false;
      MSG("unable to read gain; softwareAGC disabled");
    }
  }

  return 0;
}


// Measure exposure over ROI on current images
int PGRStereoCamera::measureAGC(jplv_image_t *src)
{
  int i, j, step, exp, n;
  int min_row, max_row;
  uint8_t *pix;

  step = 1;
  min_row = 2 * src->rows / 3;
  max_row = src->rows;

  n = 0;
  exp = 0;
  
  for (j = min_row; j < max_row; j += step)
  {
    pix = jplv_image_pixel(src, 0, j);
    
    for (i = 0; i < src->cols; i += step)
    {
      n += 1;
      exp += (int) (uint32_t) pix[0];      
      pix += src->channels;
    }
  }
  
  return exp / n;
}


// Do software exposure control
int PGRStereoCamera::controlAGC(int goalExp, int measExp)
{
  float gainP, shutterP;
  float err, step, gain, shutter;
  char command[32];

  // PID control terms
  gainP = 0.02;
  shutterP = 0.001;

  if (!this->enableAGC)
    return 0;
  
  // Compute current exposure value
  err = goalExp - measExp;

  // Simple P controller on shutter
  shutter = shutterP * err; 

  // MAGIC
  // Limit the rate at which the shutter changes
  step = (this->shutterMax - this->shutterMin) * 0.001;
  if (shutter > +step)
    shutter = +step;
  if (shutter < -step)
    shutter = -step;  
  shutter = this->shutter + shutter;

  if (shutter < this->shutterMin)
    shutter = this->shutterMin;
  if (shutter > this->shutterMax)
    shutter = this->shutterMax; 
  
  // Simple P controller on gain
  gain = gainP * err;

  // MAGIC
  // Limit the rate at which the gain changes
  step = (this->gainMax - this->gainMin) * 0.001;
  if (gain > +step)
    gain = +step;
  if (gain < -step)
    gain = -step;

  gain = this->gain + gain;

  // Bound the gain
  if (gain < this->gainMin)
    gain = this->gainMin;
  if (gain > this->gainMax)
    gain = this->gainMax;

  // Set the shutter on the cameras
  snprintf(command, sizeof(command), "%fms", shutter);
  if (dc_1394_config(this->cameras[0],  (unsigned char*)command, 0))
    ERROR("dc_1394_config(%s) failed", command);
  if (dc_1394_config(this->cameras[1],  (unsigned char*)command, 0))
    ERROR("dc_1394_config(%s) failed", command);

  this->shutter = shutter;
  
  // Set the gain on the cameras
  snprintf(command, sizeof(command), "%fdb", gain);
  if (dc_1394_config(this->cameras[0],  (unsigned char*)command, 0))
    ERROR("dc_1394_config(%s) failed", command);
  if (dc_1394_config(this->cameras[1],  (unsigned char*)command, 0))
    ERROR("dc_1394_config(%s) failed", command);

  this->gain = gain;
  
  //bb_camera_set_gain(self, gain);
  //bb_camera_set_shutter(self, shutter);
      
  //MSG("exposure %.0d %.0d %.3f %.3f %.3f %.3f %.3f",
  //    goal, meas, err,
  //    shutter, self->shutter_value, gain, self->gain_value);  
  
  return 0;
}


// Copy captured images to target buffers
int PGRStereoCamera::copy(const vis_dev_img_t *src, jplv_image_t *dst)
{
  int level;
  int i;
  quick_pyramid_t *pyr;
  int cols, rows, size;
  uint8_t *la, *lb;

  // Determine pyramid level of destination images
  if (src->width == 1 * dst->cols)
    level = 0;
  else if (src->width == 2 * dst->cols)
    level = 1;
  else if (src->width == 4 * dst->cols)
    level = 2;
  else
    assert(false);
  
  cols = src->width;
  rows = src->height;
  size = src->img_size;
  la = (uint8_t*) malloc(size);
  lb = (uint8_t*) malloc(size);
  memcpy(la, src->pixels, size);
    
  pyr = quick_pyramid_start(cols);
        
  for (i = 0; i < level; i++)
  {
    quick_pyramid(pyr, true, cols, rows, la,
                  0, 0, cols, rows, 5, 5, 1, 1, false, lb, NULL);
    cols /= 2;
    rows /= 2;
    size /= 4;
    memcpy(la, lb, size);      
  }
      
  assert(size == dst->data_size);
  memcpy(dst->data, la, size);
    
  quick_pyramid_stop(pyr);
  free(la);
  free(lb);

  return 0;
}
