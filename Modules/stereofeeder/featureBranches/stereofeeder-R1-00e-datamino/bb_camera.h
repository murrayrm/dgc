
/* 
 * Desc: Bumblebee camera interface
 * Date: 14 September 2005
 * Author: Andrew Howard
 * CVS: $Id: bb_camera.h,v 1.11 2006/11/15 22:45:54 abhoward Exp $
*/

#ifndef BB_CAMERA_H
#define BB_CAMERA_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <jplv/jplv_image.h>
#define DC_1394
#include <vis-tools/vis_devices.h>


/// Camera interface
typedef struct
{
  /// Do we have valid gain/shutter settings
  bool gain_valid;
  
  /// Gain settings
  float gain_min, gain_max, gain_value;

  /// Shutter settings
  float shutter_min, shutter_max, shutter_value;

  /// Slowest allowed shutter speed
  float shutter_limit;

  /// Auto-exposure controller gain
  float gain_p, shutter_p;

  /// Camera handle
  struct vis_dev_t *camera;

  /// Image data
  vis_dev_img_t image;

  /// Frame counter
  int num_frames;
  
  /// Image timstamp
  uint64_t timestamp;

} bb_camera_t;


/// Create object
bb_camera_t *bb_camera_alloc(const char *camera_string);

/// Destroy object
void bb_camera_free(bb_camera_t *self);

/// Initialize camera
int bb_camera_init(bb_camera_t *self, const char *config_string);

/// Stop camera
int bb_camera_fini(bb_camera_t *self);

/// @brief Capture image pair (non-blocking)
///
/// @param[out] raw_image Captured images are written to this buffer,
/// which must be pre-allocated with the correct dimensions.  The images
/// are interleaved.
/// @returns Returns 1 if an image was captures, 0 on timout, -1 on error.
int bb_camera_capture(bb_camera_t *self, jplv_image_t *raw_image);

/// Set the gain
/// @param[in] gain Gain term (dB)
int bb_camera_set_gain(bb_camera_t *self, float gain);

/// Set the shutter speed
/// @param[in] shutter Shutter speed (seconds)
int bb_camera_set_shutter(bb_camera_t *self, float shutter);

/// Apply automatic gain and shutter control
int bb_camera_auto_gain(bb_camera_t *self, int goal, int meas);

/// Simple de-mux from interlevaed monochrome images
int bb_camera_demux_mono(const jplv_image_t *src, int level, int *exp_level,
                         jplv_image_t *left, jplv_image_t *right);

#ifdef __cplusplus
}
#endif

#endif



