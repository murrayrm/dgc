
/* Desc: Tweak stereo logs
 * Author: Andrew Howard
 * Date: 17 Mar 2005
 * CVS: $Id$
 *
 * Build with: gcc -o stereo-log-tweak -I../../include stereo_log_tweak.c -L../../lib/i486-gentoo-linux-static -lsensnetlog -ljplv -lm
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <jplv/jplv_stereo.h>

#include <alice/AliceConstants.h>
#include <frames/mat44.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/StereoImageBlob.h>



// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log self-test.
int main(int argc, char **argv)
{
  char *srcfilename, *dstfilename;
  sensnet_log_t *srclog, *dstlog;
  sensnet_log_header_t header;
  int cols, rows;
  jplv_stereo_t *stereo;
  
  StereoImageBlob *src;
  StereoImageBlob *dst;
    
  if (argc < 3)
  {
    printf("usage: %s <SRC> <DST>\n", argv[0]);
    return -1;
  }
  srcfilename = argv[1];
  dstfilename = argv[2];

  // Create source log
  srclog = sensnet_log_alloc();
  assert(srclog);
  MSG("opening source %s", srcfilename);
  if (sensnet_log_open_read(srclog, srcfilename, &header) != 0)
    return -1;

  // Create destination log
  dstlog = sensnet_log_alloc();
  assert(dstlog);
  MSG("opening source %s", dstfilename);
  if (sensnet_log_open_write(dstlog, dstfilename, &header, false) != 0)
    return -1;

  // MAGIC
  cols = 320;
  rows = 240;
   
  // Open stereo
  stereo = jplv_stereo_alloc(cols, rows, 1);

  // Load camera models
  if (true)
  {
    char tmp[1024];
    jplv_cmod_t left_model, right_model;
    
    // MAGIC
    snprintf(tmp, sizeof(tmp), "%s/%d-left.cahvor", srcfilename, 6450825);
    MSG("loading %s", tmp);
    if (jplv_cmod_read(&left_model, tmp) != 0)
      return ERROR("unable to load camera model %s : %s", tmp, jplv_error_str());

    // MAGIC
    snprintf(tmp, sizeof(tmp), "%s/%d-right.cahvor", srcfilename, 6450825);
    MSG("loading %s", tmp);
    if (jplv_cmod_read(&right_model, tmp) != 0)
      return ERROR("unable to load camera model %s : %s", tmp, jplv_error_str());

    // Re-scale the models if ncessary
    jplv_cmod_scale_dims(&left_model, cols, rows);
    jplv_cmod_scale_dims(&right_model, cols, rows);

    // Set the model in the stereo context
    jplv_stereo_set_cmod(stereo, JPLV_STEREO_CAMERA_LEFT, &left_model);
    jplv_stereo_set_cmod(stereo, JPLV_STEREO_CAMERA_RIGHT, &right_model);
  }

  jplv_stereo_prepare(stereo);

  src = malloc(sizeof(*src));
  dst = malloc(sizeof(*dst));
  
  while (true)
  {
    uint64_t timestamp;
    int sensor_id, blob_type, blob_id, blob_size;
    jplv_cmod_t model;
    double md[4][4];
    float mf[4][4];

    // MAGIC This is the new sensor-to-vehicle transform
    float sens2veh[4][4] = {{0.29030501, -0.4469202, 0.84615917, 4.37288657},
                            {0.9546007, 0.19696502, -0.22347771, -0.86003481},
                            {-0.06678705, 0.87262083, 0.48381027, -0.91431609},
                            {0, 0, 0, 1}};

    // Read blob
    blob_size = sizeof(*src);
    if (sensnet_log_read(srclog, &timestamp, &sensor_id,
                         &blob_type, &blob_id, &blob_size, src) != 0)
      return -1;

    MSG("read blob %d %d bytes", blob_id, blob_size);

    // Copy data
    memcpy(dst, src, sizeof(*dst));

    // Get the rectified model and copy to blob
    jplv_stereo_get_cmod_rect(stereo, JPLV_STEREO_CAMERA_LEFT, &model);    

    // Compute left camera transform (and inverse)
    jplv_cmod_get_transform(&model, md);
    mat44f_setd(mf, md);
    mat44f_mul(dst->leftCamera.sens2veh, sens2veh, mf);
    mat44f_inv(dst->leftCamera.veh2sens, dst->leftCamera.sens2veh);

    // Get the rectified model and copy to blob
    jplv_stereo_get_cmod_rect(stereo, JPLV_STEREO_CAMERA_RIGHT, &model);    

    // Compute right camera transform (and inverse)
    jplv_cmod_get_transform(&model, md);
    mat44f_setd(mf, md);
    mat44f_mul(dst->rightCamera.sens2veh, sens2veh, mf);
    mat44f_inv(dst->rightCamera.veh2sens, dst->rightCamera.sens2veh);

    // Modify the disparity image
    {
      uint16_t *dpix;
      int pi, pj;
      float px, py, pz;
      float cx, cy, sx, sy, baseline;
      float mv[4][4];

      // Camera model
      jplv_stereo_get_cmod_rect(stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
      cx = model.ext.cahv.hc;
      cy = model.ext.cahv.vc;
      sx = model.ext.cahv.hs;
      sy = model.ext.cahv.vs;    
      baseline = dst->baseline;

      // Camera-to-vehicle transform
      assert(sizeof(mv) == sizeof(sens2veh));
      memcpy(mv, sens2veh, sizeof(mv));

      // TODO: consider robot attitude
  
      dpix = (uint16_t*) StereoImageBlobGetDisp(dst, 0, 0);
      for (pj = 0; pj < rows; pj++)
      {
        for (pi = 0; pi < cols; pi++)
        {
          // Compute the ray/z-plane intersection in camera coordinates;
          // the matrix m specifies the camera pose relative to the plane.
          px = (pi - cx) / sx;
          py = (pj - cy) / sy;
          pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);

          // Compute disparity
          dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
          dpix += 1;
        }
      }
    }
    
    MSG("writing %d %d", blob_id, blob_size);

    // Write new blob
    if (sensnet_log_write(dstlog, timestamp, sensor_id,
                          blob_type, blob_id, blob_size, dst) != 0)
      return -1;
  }

  // Clean up
  sensnet_log_close(dstlog);
  sensnet_log_free(dstlog);
  sensnet_log_close(srclog);
  sensnet_log_free(srclog);
  
  return 0;
}

