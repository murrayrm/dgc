/* 
 * Desc: Converter utility for raw stereo logs.
 * Date: 07 March 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jplv/jplv_stereo.h>
#include <frames/mat44.h>
#include <frames/pose3.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/StereoImageBlob.h>

#include "raw_log.h"

// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


int process(int sensorid, const char *filename, const char *ofilename)
{
  raw_log_t *image_log;
  raw_log_header_t header;
  raw_log_tag_t tag;
  sensnet_log_t *olog;
  sensnet_log_header_t oheader;
  jplv_stereo_t *stereo;
  
  StereoImageBlob *blob;

  // Open the input log
  MSG("opening %s", filename);
  image_log = raw_log_alloc();
  assert(image_log);
  if (raw_log_open_read(image_log, filename, &header) != 0)
    return ERROR("unable to open %s", filename);

  // Open the output log
  olog = sensnet_log_alloc();
  assert(olog);
  memset(&oheader, 0, sizeof(oheader));
  if (sensnet_log_open_write(olog, ofilename, &oheader, false) != 0)
    return ERROR("uanble to open %s", ofilename);

  // Create the output blob
  blob = calloc(1, sizeof(*blob));

  // Open stereo
  stereo = jplv_stereo_alloc(header.cols, header.rows, header.channels, 0, JPLV_STEREO_SAD5_PLUS);

  // Load camera models
  if (true)
  {
    char tmp[1024];
    jplv_cmod_t left_model, right_model;
    
    snprintf(tmp, sizeof(tmp), "%s/%d-left.cahvor", filename, header.left_camera);
    MSG("loading %s", tmp);
    if (jplv_cmod_read(&left_model, tmp) != 0)
      return ERROR("unable to load camera model %s : %s", tmp, jplv_error_str());
  
    snprintf(tmp, sizeof(tmp), "%s/%d-right.cahvor", filename, header.right_camera);
    MSG("loading %s", tmp);
    if (jplv_cmod_read(&right_model, tmp) != 0)
      return ERROR("unable to load camera model %s : %s", tmp, jplv_error_str());

    // Re-scale the models if ncessary
    jplv_cmod_scale_dims(&left_model, header.cols, header.rows);
    jplv_cmod_scale_dims(&right_model, header.cols, header.rows);

    // Set the model in the stereo context
    jplv_stereo_set_cmod(stereo, JPLV_STEREO_CAMERA_LEFT, &left_model);
    jplv_stereo_set_cmod(stereo, JPLV_STEREO_CAMERA_RIGHT, &right_model);
  }

  // Prepare for action
  jplv_stereo_set_disparity(stereo, 0, header.cols/8, 7, 7);
  jplv_stereo_prepare(stereo);
  
  while (true)
  {
    jplv_image_t *left_image, *right_image;
    
    // Read raw images
    left_image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
    right_image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
    if (raw_log_read(image_log, &tag, left_image->data_size,
                        left_image->data, right_image->data) != 0)
      break;
    MSG("tag %d", tag.frameid);

    // Overwrite incorrect data
    if (true)
    {
      // MF long stereo
      float m[4][4] = {{+0.008461, -0.151081, +0.988485, +3.279699},
                       {+0.999964, +0.001882, -0.008272, -0.757728},
                       {-0.000611, +0.988520, +0.151091, -1.673604},
                       {+0.000000, +0.000000, +0.000000, +1.000000}};
      // RF short stereo
      //float m[4][4] = {{-0.145812, -0.511238, +0.846980, +4.425278}, 
      //                 {+0.989312, -0.074301, +0.125467, +0.698895},
      //                 {-0.001212, +0.856222, +0.516607, -0.918228},
      //                 {+0.000000, +0.000000, +0.000000, +1.000000}};
      mat44f_setf(tag.sens2veh, m);
    }

    // Do stereo
    jplv_stereo_rectify(stereo);
    jplv_stereo_prefilter(stereo);
    jplv_stereo_disparity(stereo);
    jplv_stereo_postfilter(stereo);
    
    // Write individual images
    if (false)
    {
      char tmp[1024];

      // Get rectified images
      left_image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
      right_image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_RIGHT);
      snprintf(tmp, sizeof(tmp), "%s/%06d-left.pnm", ofilename, tag.frameid);
      jplv_image_write_pnm(left_image, tmp, NULL);
      snprintf(tmp, sizeof(tmp), "%s/%06d-right.pnm", ofilename, tag.frameid);
      jplv_image_write_pnm(right_image, tmp, NULL);
    }

    // Create blob
    if (true)
    {
      int imageSize, blobSize;
      jplv_cmod_t model;
      double md[4][4];
      float mf[4][4];
      pose3_t pose;
      jplv_image_t *image;

      memset(blob, 0, sizeof(*blob));
      
      blob->blobType = SENSNET_STEREO_IMAGE_BLOB;
      blob->version = STEREO_IMAGE_BLOB_VERSION;
      blob->sensorId = sensorid;
      blob->frameId = tag.frameid;
      blob->timestamp = tag.timestamp;
  
      // Get the rectified model and copy to blob
      jplv_stereo_get_cmod_rect(stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
      blob->leftCamera.cx = model.ext.cahv.hc;
      blob->leftCamera.cy = model.ext.cahv.vc;
      blob->leftCamera.sx = model.ext.cahv.hs;
      blob->leftCamera.sy = model.ext.cahv.vs;    

      // Compute left camera transform
      jplv_cmod_get_transform(&model, md);
      mat44f_setd(mf, md);
      mat44f_mul(blob->leftCamera.sens2veh, tag.sens2veh, mf);

      // Get the rectified model and copy to blob
      jplv_stereo_get_cmod_rect(stereo, JPLV_STEREO_CAMERA_RIGHT, &model);    
      blob->rightCamera.cx = model.ext.cahv.hc;
      blob->rightCamera.cy = model.ext.cahv.vc;
      blob->rightCamera.sx = model.ext.cahv.hs;
      blob->rightCamera.sy = model.ext.cahv.vs;    

      // Compute right camera transform
      jplv_cmod_get_transform(&model, md);
      mat44f_setd(mf, md);
      mat44f_mul(blob->rightCamera.sens2veh, tag.sens2veh, mf);

      // Camera baseline
      blob->baseline = jplv_stereo_get_baseline(stereo);
      assert(blob->baseline > 0);

      // Copy camera settings
      blob->leftCamera.gain = tag.gain[0];
      blob->leftCamera.shutter = tag.shutter[0];
      blob->rightCamera.gain = tag.gain[1];
      blob->rightCamera.shutter = tag.shutter[1];

      // Copy vehicle state
      blob->state = tag.state;

      // Vehicle to local transform
      pose.pos = vec3_set(blob->state.localX,
                          blob->state.localY,
                          blob->state.localZ);
      pose.rot = quat_from_rpy(blob->state.localRoll,
                               blob->state.localPitch,
                               blob->state.localYaw);  
      pose3_to_mat44f(pose, blob->veh2loc);  

      // Reset reseved values
      memset(blob->reserved, 0, sizeof(blob->reserved));

      // Set image data
      jplv_stereo_get_dims(stereo, &blob->cols, &blob->rows, &blob->channels);

      // Keep track of total image data stored
      imageSize = 0;
  
      // Copy left rectified data
      image = jplv_stereo_get_image(stereo,
                                    JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
      blob->leftOffset = imageSize;
      blob->leftSize = image->data_size;
      imageSize += image->data_size;  
      memcpy(blob->imageData + blob->leftOffset, image->data, image->data_size);
  
      // Copy right rectified data
      image = jplv_stereo_get_image(stereo,
                                    JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_RIGHT);
      blob->rightOffset = imageSize;
      blob->rightSize = image->data_size;
      imageSize += image->data_size;
      memcpy(blob->imageData + blob->rightOffset, image->data, image->data_size);

      // Copy disparity data
      image = jplv_stereo_get_image(stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
      blob->dispScale = JPLV_STEREO_DISP_SCALE;
      blob->dispOffset = imageSize;
      blob->dispSize = image->data_size;
      imageSize += image->data_size;
      memcpy(blob->imageData + blob->dispOffset, image->data, image->data_size);
  
      // Compute the size of the blob that was actually used
      blobSize = sizeof(*blob) - sizeof(blob->imageData) + imageSize;

      // Write to log file
      if (sensnet_log_write(olog, blob->timestamp, blob->sensorId, blob->blobType,
                            blob->frameId, blobSize, blob) != 0)
        return ERROR("log write failed");
    }

    // TESTING
    //if (blob->frameId > 400)
    //  break;
  }

  // Tidy up
  jplv_stereo_free(stereo);
  free(blob);
  sensnet_log_close(olog);
  raw_log_close(image_log);

  return 0;
}



int main(int argc, const char **argv)
{
  int sensorid;
  assert(argc >= 4);

  sensorid = sensnet_id_from_name(argv[1]);
  if (sensorid == SENSNET_NULL_SENSOR)
    return ERROR("%s is not a valid sensor id", argv[1]);
  
  process(sensorid, argv[2], argv[3]);
  
  return 0;
}
