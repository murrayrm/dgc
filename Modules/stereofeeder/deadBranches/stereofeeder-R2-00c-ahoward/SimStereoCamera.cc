/* 
 * Desc: Simulated stereo camera
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include <alice/AliceConstants.h>
#include "SimStereoCamera.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
SimStereoCamera::SimStereoCamera()
{
  return;
}


// Destructor
SimStereoCamera::~SimStereoCamera()
{
  return;
}
  

// Initialize the camera
int SimStereoCamera::open(int cols, int rows)
{
  // Record key image dimensions
  this->imageCols = cols;
  this->imageRows = rows;
  this->imageChannels = 1;

  return 0;
}


// Close the camera
int SimStereoCamera::close()
{
  return 0;
}


// Capture a simulated frame
int SimStereoCamera::captureSim(const VehicleState *state, const float sens2veh[4][4],
                                const jplv_cmod_t *cmod, float baseline,
                                jplv_image_t *rectImage, jplv_image_t *dispImage)
{
  uint8_t *rpix;
  uint16_t *dpix;
  float cx, cy, sx, sy;
  pose3_t pose;
  float mv[4][4];
  double ml[4][4];
  int pi, pj;
  int qi, qj;
  float px, py, pz;
  float vx, vy, vz;
  double qx, qy;

  // Camera model
  cx = cmod->ext.cahv.hc;
  cy = cmod->ext.cahv.vc;
  sx = cmod->ext.cahv.hs;
  sy = cmod->ext.cahv.vs;

  // Camera-to-vehicle transform
  memcpy(mv, sens2veh, sizeof(mv));

  // Vehicle-to-local transform
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);
  pose3_to_mat44d(pose, ml);

  rpix = (uint8_t*) jplv_image_pixel(rectImage, 0, 0);
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < rectImage->rows; pj++)
  {
    for (pi = 0; pi < rectImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);
      px = px * pz;
      py = py * pz;

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      // Convert to vehicle coordinates
      vx = mv[0][0] * px + mv[0][1] * py + mv[0][2] * pz + mv[0][3];
      vy = mv[1][0] * px + mv[1][1] * py + mv[1][2] * pz + mv[1][3];
      vz = mv[2][0] * px + mv[2][1] * py + mv[2][2] * pz + mv[2][3];

      // Convert to local coordinates
      qx = ml[0][0] * vx + ml[0][1] * vy + ml[0][2] * vz + ml[0][3];
      qy = ml[1][0] * vx + ml[1][1] * vy + ml[1][2] * vz + ml[1][3];

      // Checker board
      if (false)
      {
        qi = (int) ((qx + 100000) / 1.00);
        qj = (int) ((qy + 100000) / 1.00);
        rpix[0] = 0x80 + ((qi + qj) % 2) * 0x7F;
      }

      // Lines every 20m
      if (true)
      {
        rpix[0] = 0x20;
        if (fmod(qx + 1e6, 20) < 0.12)
          rpix[0] = 0xD0;
        if (fmod(qy + 1e6, 20) < 0.12)
          rpix[0] = 0xD0;
      }

      rpix += rectImage->channels;
      dpix += 1;
    }
  }

  this->frameId += 1;
  this->timestamp = state->timestamp;
  
  return 0;
}
