
/* Desc: DGC stereo data logging
 * Author: Andrew Howard
 * Date: 9 Nov 2005
 * CVS: $Id$
 */

#ifndef STEREO_LOG_H
#define STEREO_LOG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>
  

/// @brief Log version number
#define STEREO_LOG_VERSION 0x04


/// @brief Log header data
typedef struct
{
  /// File version (read-only)
  int version;

  /// Camera id 
  int camera_id;
  
  /// Image dimensions
  int cols, rows, channels;
  
} stereo_log_header_t;

    
/// @brief Vehicle state data.
///
/// Do not replace this with the state structures defined elsewhere,
/// since logging code should be self-contained.
typedef struct
{
  /// System timestamp: number of microseconds since the epoch
  uint64_t timestamp;
  
  /// Vehicle velocity in vehicle frame.  Stored as (x,y,z; roll,pitch,yaw).
  /// Translations are in meters/sec, rotations in radians/sec.
  double veh_vel[6];

  /// Vehicle pose in local frame.  Stored as (x,y,z; roll,pitch,yaw).
  /// Translations are in meters, rotations in radians.
  double local_pose[6];

  /// Vehicle pose in UTM frame.  Stored as (x,y,z; roll,pitch,yaw).
  /// Translations are in meters, rotations in radians.
  double utm_pose[6];

} stereo_log_state_t;


/// @brief Image tag data
typedef struct
{
  // Image index (read-only internal variable)
  int index;

  /// Frame id
  int frameid;

  /// Frame timestamp (number of microseconds since the epoch)
  uint64_t timestamp;

  /// Vehicle state data
  stereo_log_state_t state;

  /// Sensor-to-vehicle transform
  float sens2veh[4][4];

} stereo_log_tag_t;


/// @brief Stereo log context (opaque).
typedef struct stereo_log stereo_log_t;

/// @brief Allocate object
stereo_log_t *stereo_log_alloc();

/// @brief Free object
void stereo_log_free(stereo_log_t *self);

/// @brief Open file for writing
int stereo_log_open_write(stereo_log_t *self,
                          const char *logdir, stereo_log_header_t *header);

/// @brief Open file for reading
int stereo_log_open_read(stereo_log_t *self,
                         const char *logdir, stereo_log_header_t *header);

/// @brief Close the log
int stereo_log_close(stereo_log_t *self);

/// @brief Write an image to the log
///
/// @param[in] tag Image tag.
/// @param[in] data_size Size of image data, in bytes.
/// @param[in] left_data Left image data.
/// @param[in] right_data Right image data.
int stereo_log_write(stereo_log_t *self, stereo_log_tag_t *tag,
                     int data_size, const uint8_t *left_data, const uint8_t *right_data);

/// @brief Read an image from the log
///
/// @param[out] tag Image tag.
/// @param[in] data_size Size of image data, in bytes.
/// @param[out] left_data Left image data.
/// @param[out] right_data Right image data.
int stereo_log_read(stereo_log_t *self, stereo_log_tag_t *tag,
                    int data_size, uint8_t *left_data, uint8_t *right_data);


#ifdef __cplusplus
}
#endif

#endif
