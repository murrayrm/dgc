
/* Desc: Simple test for SensNet logging of stereo data.
 *       Note that this exercises SensNet logs, not raw image logs.
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <interfaces/StereoImageBlob.h>
#include <sensnet/sensnet_log.h>


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Simple log self-test.
int main(int argc, char **argv)
{
  char *filename;
  sensnet_log_t *log;
  sensnet_log_header_t header;
 
  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  // Create log
  log = sensnet_log_alloc();
  assert(log);

  // Open log
  MSG("opening %s", filename);
  if (sensnet_log_open_read(log, filename, &header) != 0)
    return -1;

  while (true)
  {
    int blob_id, blob_len;
    StereoImageBlob blob;

    // Read blob
    blob_len = sizeof(blob);
    if (sensnet_log_read(log, NULL, NULL, NULL, &blob_id, &blob_len, &blob) != 0)
      return -1;

    MSG("read blob %d %d bytes frame %d time %.3f",
        blob_id, blob_len,
        blob.frameId, (double) blob.timestamp * 1e-6);
  }

  // Clean up
  sensnet_log_close(log);
  sensnet_log_free(log);
  
  return 0;
}

