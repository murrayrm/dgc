
# Desc: calibration script
# Author: Andrew Howard
# Date: 11 March 2007

from sys import *
from math import *
from numpy import *
from numpy.linalg import inv
from scipy.optimize.optimize import *


def mat44_set_euler((r, p, h, x, y, z)):
    """Create transform matrix from euler angles and translation"""

    # TODO check sign convention
    R = matrix([[1, 0, 0, 0],
                [0, cos(r), -sin(r), 0],
                [0, sin(r), +cos(r), 0],
                [0, 0, 0, 1]])
    P = matrix([[cos(p), 0, +sin(p), 0],
                [0, 1, 0, 0],
                [-sin(p), 0, +cos(p), 0],
                [0, 0, 0, 1]])
    H = matrix([[cos(h), -sin(h), 0, 0],
                [sin(h), +cos(h), 0, 0],
                [0, 0, 1, 0],
                [0, 0, 0, 1]])
    T = matrix([[1, 0, 0, x],
                [0, 1, 0, y],
                [0, 0, 1, z],
                [0, 0, 0, 1]])

    M = T * H * P * R
    
    return M


def mat41_set_xyz((x, y, z)):
    """Create a 4x1 homogeneous coordinate from XYZ values (right-handed)."""

    return transpose(matrix((x, y, z, 1)));


def mat41_set_neh((n, e, h)):
    """Create a 4x1 homogeneous coordinate from NEH values (left-handed)."""

    return transpose(matrix((e, n, h, 1)));


def mat31_set_xy((x, y)):
    """Create a 3x1 homogeneous coordinate from XY values (right-handed)."""

    return transpose(matrix((x, y, 1)));


def err3d(T, P, Q):
    """Error function for matching 3D point sets."""

    # Construct transform matrix
    M = mat44_set_euler(T)

    # Construct distance matrix
    E = M * P - Q

    # Construct error matrix
    E = multiply(E, E)

    # Compute sum of squared errors
    err = sum(sum(array(E)))
    
    return err


def calc_neh2veh(veh_neh, veh_veh):
    """Compute transform from NEH frame to vehicle frame."""

    # Points in NEH frame (left-handed).
    # Create matrix from the column vectors.
    P = hstack((mat41_set_neh(veh_neh[0]),
                mat41_set_neh(veh_neh[1]),
                mat41_set_neh(veh_neh[2])))

    # Points in vehicle frame.
    # Create matrix from the column vectors.
    Q = hstack((mat41_set_xyz(veh_veh[0]),
                mat41_set_xyz(veh_veh[1]),
                mat41_set_xyz(veh_veh[2])))

    # Initial transform
    T = (0, 0, 0, 0, 0, 0)
    
    # Optimize
    T = fmin_bfgs(err3d, T, None, (P, Q))

    #print 'P = \n', P
    #print 'Q = \n', Q
    print 'T = ', T

    return mat44_set_euler(T)



def err2d(T, pos, model, P, Q):
    """Error function for matching 2D point sets."""

    # Construct transform matrix, varying rotation but not translation
    M = mat44_set_euler((T[0], T[1], T[2], pos[0], pos[1], pos[2]))
    M = inv(M)

    # Transform points into sensor frame
    V = M * P

    # Extract model parameters
    (hc, vc, hs, vs) = model

    # TODO convert to matrix form
    err = 0
    for i in range(shape(V)[1]):

        # Predicted point
        (px, py, pz) = (V[0,i], V[1,i], V[2,i])
        #print px, py, pz

        # Project to image plane
        pi = px/pz * hs + hc
        pj = py/pz * vs + vc
        #print pi, pj

        # Measured point in image plane
        (qi, qj) = (Q[0,i], Q[1,i])
        #print qi, qj
        
        # Image error
        err += (pi - qi)**2 + (pj - qj)**2

    #print err    
    return err


def calc_sens2veh(pos, model, dots_veh, dots_img):
    """Compute sensor-to-vehicle transform, given a camera model, a
    set of dots in the vehicle frame, a set of dots in the image."""

    # Initial rotation; assume facing forward in vehicle frame
    T = (pi/2, 0, pi/2)
    
    # Optimize rotation
    T = fmin(err2d, T, (pos, model, dots_veh, dots_img))

    return T


# Tire radius
tire = 0.3937
    
# Vehicle points in vehicle frame.  This comes from the Wiki, where
# the coordinates are specified with respect to the ground.
veh_veh = ((4.09, -0.89, -1.30 + tire), # Lower left
           (3.00, -0.72, -2.19 + tire), # Upper left
           (3.00, +0.72, -2.19 + tire), # Upper right
           (4.09, +0.89, -1.30 + tire)) # Lower right


# Calibration 2007-03-11
if False:

    # Vehicle points in NEH frame (left-handed)
    veh_neh = ((4.149, 0.592, -0.185),
               (5.090, 1.250,  0.684),
               (6.054, 0.250,  0.686),
               None)

    # Dot locations in NEH frame (left-handed)
    dots_neh = ((-0.377, -4.407, -1.700),
                (-0.660, -4.703, -0.765),
                ( 0.071, -5.410, -0.768))

    # Long-range stereo in NEH frame (left-handed)
    long_neh = ((4.838,  1.090, 0.563), # Left camera
                (5.887, -0.001, 0.571)) # Right camera

    # Long-range stereo dots in rectified image
    long_dots = ((365.040924, 388.467163),             
                 (365.716522, 302.134674),
                 (450.778046, 303.485718))

    # Long-range rectified camera parameters (left camera)
    long_model = (322.692338, 257.172191, 832.750300, 843.553183) 
            
    # Compute transform from NEH to vehicle frame
    M = calc_neh2veh(veh_neh, veh_veh)

    # Compute dot locations in vehicle frame
    dots_veh = hstack(map(mat41_set_neh, dots_neh))
    dots_veh = M * dots_veh
    #print "dots = "
    #print dots_veh
    
    # Compute long-range stereo location in vehicle frame
    long_veh = hstack(map(mat41_set_neh, long_neh))
    long_veh = M * long_veh
    #print "long = "
    #print long_veh

    # Compute long-range stereo rotation in vehicle frame
    long_dots = hstack(map(mat31_set_xy, long_dots))
    long_pos = array((long_veh[0,0], long_veh[1,0], long_veh[2,0]))
    long_rot = calc_sens2veh(long_pos, long_model, dots_veh, long_dots)
    print 'long_pos = %+f, %+f, %+f' % (long_pos[0], long_pos[1], long_pos[2])
    print 'long_rot = %+f, %+f, %+f' % (long_rot[0], long_rot[1], long_rot[2])
    #print 'long_M ='
    #print mat44_set_euler((long_rot[0], long_rot[1], long_rot[2],
    #                       long_pos[0], long_pos[1], long_pos[2]))


    # Right-front short stereo in NEH frame (left-handed)
    rf_neh = ((5.025, -0.772, -0.146),)

    # Right-front stereo dots in rectified image
    rf_dots = ((89.120850, 53.069569),
               (84.792816, 19.160614),
               (115.975754, 16.873749))

    # Right-front rectified camera parameters (left camera)
    rf_model = (164.607284, 124.997595, 229.857293, 245.549931)

    # Compute rf-short stereo location in vehicle frame
    rf_veh = hstack(map(mat41_set_neh, rf_neh))
    rf_veh = M * rf_veh
    #print "rf = "
    #print rf_veh

    # Compute rf-short rotation in vehicle frame
    rf_dots = hstack(map(mat31_set_xy, rf_dots))
    rf_pos = array((rf_veh[0,0], rf_veh[1,0], rf_veh[2,0]))
    rf_rot = calc_sens2veh(rf_pos, rf_model, dots_veh, rf_dots)
    print 'rf_pos = %+f, %+f, %+f' % (rf_pos[0], rf_pos[1], rf_pos[2])
    print 'rf_rot = %+f, %+f, %+f' % (rf_rot[0], rf_rot[1], rf_rot[2])

# Calibration 2006-03-17
elif False:

    # Vehicle points in NEH frame (left-handed)
    veh_neh = ((-2.889, -3.231, -0.094),
               (-3.193, -4.336, 0.778),
               (-4.573, -4.177, 0.786))

    # Dot locations in NEH frame (left-handed)
    dots_neh = ((-0.490,3.782,-1.596),
                (-0.402,4.122,-0.664),
                (-1.343,4.428,-0.688))

    # Compute transform from NEH to vehicle frame
    M = calc_neh2veh(veh_neh, veh_veh)

    # Compute dot locations in vehicle frame
    dots_veh = hstack(map(mat41_set_neh, dots_neh))
    dots_veh = M * dots_veh
    #print "dots = "
    #print dots_veh

    # Left-front short stereo in NEH frame (left-handed)
    lf_neh = ((-2.888,-2.954,-0.072),)

    # Left-front stereo dots in rectified image.
    # To get these numbers, run jplv-stereo to produce rectified images,
    # then run jplv-dot-find on the rectified images
    lf_dots = ((175.694031, 51.538727),
               (178.990952, 21.601181),
               (205.504059, 24.755407))

    # Left-front rectified camera parameters (center, scale).
    # Get these from the CAHV model produced by jplv-stereo
    lf_model = (163.521998, 124.141254, 228.086661, 250.159411) 

    # Compute lf-short stereo location in vehicle frame
    lf_veh = hstack(map(mat41_set_neh, lf_neh))
    lf_veh = M * lf_veh
    #print "lf = "
    #print lf_veh

    # Compute lf-short rotation in vehicle frame
    lf_dots = hstack(map(mat31_set_xy, lf_dots))
    lf_pos = array((lf_veh[0,0], lf_veh[1,0], lf_veh[2,0]))
    lf_rot = calc_sens2veh(lf_pos, lf_model, dots_veh, lf_dots)
    print 'lf_pos = %+f, %+f, %+f' % (lf_pos[0], lf_pos[1], lf_pos[2])
    print 'lf_rot = %+f, %+f, %+f' % (lf_rot[0], lf_rot[1], lf_rot[2])
    print 'lf ='
    print mat44_set_euler((lf_rot[0], lf_rot[1], lf_rot[2],
                           lf_pos[0], lf_pos[1], lf_pos[2]))


# Calibration 2007-05-29
elif True:

    # Tire radius
    tire = 0.3937
    
    # Vehicle points in vehicle frame.  This comes from the Wiki, where
    # the coordinates are specified with respect to the ground.
    veh_veh = ((4.09, -0.89, -1.30 + tire), # Lower left 
               (3.00, -0.72, -2.19 + tire), # Upper left
               (3.00, +0.72, -2.19 + tire), # Upper right
               (4.09, +0.89, -1.30 + tire)) # Lower right

    # Vehicle points in NEH frame (left-handed)
    veh_neh = ((-1.657,-1.953,-0.085),
               (-1.349,-3.061,0.783),
               (-2.611,-3.629,0.790))

    # Compute transform from NEH to vehicle frame
    M = calc_neh2veh(veh_neh, veh_veh)
    print "neh2veh = "
    print M


    # Left-front short stereo in NEH frame (left-handed)
    lf_neh = ((-1.868,-1.714,-0.076),)

    # Compute lf-short stereo location in vehicle frame
    lf_veh = hstack(map(mat41_set_neh, lf_neh))
    lf_veh = M * lf_veh
    print "lf_veh = "
    print lf_veh

    # Dot locations in NEH frame (left-handed)
    lf_dots_neh = ((-2.071,0.864,-1.412),
                   (-2.199,1.704,-0.842),
                   (-3.205,1.561,-0.857))

    # Compute dot locations in vehicle frame
    lf_dots_veh = hstack(map(mat41_set_neh, lf_dots_neh))
    lf_dots_veh = M * lf_dots_veh
    print "lf_dots_veh = "
    print lf_dots_veh

    # Left-front stereo dots in rectified image.
    # To get these numbers, run jplv-stereo to produce rectified images,
    # then run jplv-dot-find on the rectified images
    lf_dots_img = ((156.557037, 70.90972),
                   (159.601410, 14.201875),
                   (217.223373, 19.551888))

    # Left-front rectified camera parameters (center, scale).
    # Get these from the CAHV model produced by jplv-stereo
    lf_model = (163.521998, 124.141254, 228.086661, 250.159411) 

    # Compute lf-short rotation in vehicle frame
    lf_dots_img = hstack(map(mat31_set_xy, lf_dots_img))
    lf_pos = array((lf_veh[0,0], lf_veh[1,0], lf_veh[2,0]))
    lf_rot = calc_sens2veh(lf_pos, lf_model, lf_dots_veh, lf_dots_img)
    print 'lf_pos = %+f, %+f, %+f' % (lf_pos[0], lf_pos[1], lf_pos[2])
    print 'lf_rot = %+f, %+f, %+f' % (lf_rot[0], lf_rot[1], lf_rot[2])
    print 'lf ='
    print mat44_set_euler((lf_rot[0], lf_rot[1], lf_rot[2],
                           lf_pos[0], lf_pos[1], lf_pos[2]))


    # Right-front short stereo in NEH frame (left-handed)
    rf_neh = ((-3.222,-2.318,-0.037),)

    # Compute rf-short stereo location in vehicle frame
    rf_veh = hstack(map(mat41_set_neh, rf_neh))
    rf_veh = M * rf_veh
    print "rf_veh = "
    print rf_veh

    # Dot locations in NEH frame (left-handed)
    rf_dots_neh = ((-3.916,-0.070,-1.410),
                   (-4.268,0.643,-0.772),
                   (-5.182,0.193,-0.767))

    # Compute dot locations in vehicle frame
    rf_dots_veh = hstack(map(mat41_set_neh, rf_dots_neh))
    rf_dots_veh = M * rf_dots_veh
    print "rf_dots_veh = "
    print rf_dots_veh

    # Right-front stereo dots in rectified image.
    # To get these numbers, run jplv-stereo to produce rectified images,
    # then run jplv-dot-find on the rectified images
    rf_dots_img = ((117.598557, 94.086403),
                   (112.014969, 29.614117),
                   (175.940613, 23.429197))

    # Right-front rectified camera parameters (center, scale).
    # Get these from the CAHV model produced by jplv-stereo
    rf_model = (164.651640, 124.994041, 229.738064, 245.420071)

    # Compute rf-short rotation in vehicle frame
    rf_dots_img = hstack(map(mat31_set_xy, rf_dots_img))
    rf_pos = array((rf_veh[0,0], rf_veh[1,0], rf_veh[2,0]))
    rf_rot = calc_sens2veh(rf_pos, rf_model, rf_dots_veh, rf_dots_img)
    print 'rf_pos = %+f, %+f, %+f' % (rf_pos[0], rf_pos[1], rf_pos[2])
    print 'rf_rot = %+f, %+f, %+f' % (rf_rot[0], rf_rot[1], rf_rot[2])
    print 'rf ='
    print mat44_set_euler((rf_rot[0], rf_rot[1], rf_rot[2],
                           rf_pos[0], rf_pos[1], rf_pos[2]))





