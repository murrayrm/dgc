
/* Desc: Tweak stereo logs
 * Author: Andrew Howard
 * Date: 17 Mar 2005
 * CVS: $Id$
 *
 * Build with: gcc -o stereo-log-tweak -I../../include stereo_log_tweak.c -L../../lib/i486-gentoo-linux-static -lsensnetlog -ljplv -lm
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>

#include <jplv/jplv_stereo.h>

#include <sensnet/sensnet_log.h>
#include <interfaces/StereoImageBlob.h>



// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Get system time
uint64_t get_time()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (uint64_t) tv.tv_usec + 1000000ULL * tv.tv_sec;
}


// Simple log self-test.
int main(int argc, char **argv)
{
  char *srcfilename, *dstfilename;
  sensnet_log_t *srclog, *dstlog;
  sensnet_log_header_t header;
  uint64_t timestamp, stereo_time;
  int sensor_id, blob_type, blob_id;
  jplv_stereo_t *stereo;
  jplv_cmod_t left_model, right_model;
  jplv_image_t *image;
  
  StereoImageBlob *src;
  StereoImageBlob *dst;
    
  if (argc < 3)
  {
    printf("usage: %s <SRC> <DST>\n", argv[0]);
    return -1;
  }
  srcfilename = argv[1];
  dstfilename = argv[2];

  // Create source log
  srclog = sensnet_log_alloc();
  assert(srclog);
  MSG("opening source %s", srcfilename);
  if (sensnet_log_open_read(srclog, srcfilename, &header) != 0)
    return -1;

  // Create destination log
  dstlog = sensnet_log_alloc();
  assert(dstlog);
  MSG("opening source %s", dstfilename);
  if (sensnet_log_open_write(dstlog, dstfilename, &header, false) != 0)
    return -1;

  // Create blobs
  src = malloc(sizeof(*src));
  dst = malloc(sizeof(*dst));

  // Read blob from source
  if (sensnet_log_read(srclog, &timestamp, &sensor_id,
                       &blob_type, &blob_id, sizeof(*src), src) != 0)
    return -1;
   
  // Create stereo module
  stereo = jplv_stereo_alloc(src->cols, src->rows, src->channels);
  assert(stereo);

  // Create camera models
  memset(&left_model, 0, sizeof(left_model));
  left_model.core.xdim = src->cols;
  left_model.core.ydim = src->rows;
  left_model.core.mclass = CMOD_CLASS_CAHV;
  left_model.core.u.cahv.c[0] = 0;
  left_model.core.u.cahv.c[1] = 0;
  left_model.core.u.cahv.c[2] = 0;
  left_model.core.u.cahv.a[0] = 0;
  left_model.core.u.cahv.a[1] = 0;
  left_model.core.u.cahv.a[2] = 1;
  left_model.core.u.cahv.h[0] = src->leftCamera.sx;
  left_model.core.u.cahv.h[1] = 0;
  left_model.core.u.cahv.h[2] = src->leftCamera.cx;
  left_model.core.u.cahv.v[0] = 0;
  left_model.core.u.cahv.v[1] = src->leftCamera.sy;
  left_model.core.u.cahv.v[2] = src->leftCamera.cy;
  jplv_stereo_set_cmod(stereo, JPLV_STEREO_CAMERA_LEFT, &left_model);
    
  memset(&right_model, 0, sizeof(right_model));
  right_model.core.xdim = src->cols;
  right_model.core.ydim = src->rows;
  right_model.core.mclass = CMOD_CLASS_CAHV;
  right_model.core.u.cahv.c[0] = src->baseline;
  right_model.core.u.cahv.c[1] = 0;
  right_model.core.u.cahv.c[2] = 0;
  right_model.core.u.cahv.a[0] = 0;
  right_model.core.u.cahv.a[1] = 0;
  right_model.core.u.cahv.a[2] = 1;
  right_model.core.u.cahv.h[0] = src->rightCamera.sx;
  right_model.core.u.cahv.h[1] = 0;
  right_model.core.u.cahv.h[2] = src->rightCamera.cx;
  right_model.core.u.cahv.v[0] = 0;
  right_model.core.u.cahv.v[1] = src->rightCamera.sy;
  right_model.core.u.cahv.v[2] = src->rightCamera.cy;
  jplv_stereo_set_cmod(stereo, JPLV_STEREO_CAMERA_RIGHT, &right_model);
 
  // TODO: set stereo parameters
  jplv_stereo_set_disparity(stereo, 0, 64, 9, 3);
  jplv_stereo_set_blob(stereo, 1.0, 2.0, 0);
  jplv_stereo_set_subimage(stereo, 0, 0, src->cols, src->rows/2);
  
  // Prepare for action
  jplv_stereo_prepare(stereo);
  
  while (true)
  {
    // Copy rectified image data
    image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
    memcpy(image->data, src->imageData + src->leftOffset, image->data_size);
    image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
    memcpy(image->data, src->imageData + src->rightOffset, image->data_size);
    
    // Run stereo pipeline
    stereo_time = get_time();
    jplv_stereo_rectify(stereo);
    jplv_stereo_laplace(stereo);
    jplv_stereo_disparity(stereo);
    jplv_stereo_range(stereo);
    jplv_stereo_diag(stereo);
    stereo_time = get_time() - stereo_time;

    MSG("blob %d stereo %dms",
        blob_id, (int) stereo_time / 1000);
     
    // TESTING
    //image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_DIAG, 0);
    //jplv_image_write_pnm(image, "diag.pnm", NULL);
        
    // Copy data to destination
    memcpy(dst, src, sizeof(*dst));

    // Over-write the disparity
    assert(dst->dispSize > 0);
    image = jplv_stereo_get_image(stereo, JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
    memcpy(dst->imageData + dst->dispOffset, image->data, dst->dispSize);

    // Write new blob.
    // TODO: dont write the entire blob.
    //MSG("writing %d", blob_id);
    if (sensnet_log_write(dstlog, timestamp, sensor_id,
                          blob_type, blob_id, sizeof(*dst), dst) != 0)
      return -1;

    
    // Read next blob
    if (sensnet_log_read(srclog, &timestamp, &sensor_id,
                         &blob_type, &blob_id, sizeof(*src), src) != 0)
      return -1;    
  }

  // Clean up
  sensnet_log_close(dstlog);
  sensnet_log_free(dstlog);
  sensnet_log_close(srclog);
  sensnet_log_free(srclog);
  
  return 0;
}

