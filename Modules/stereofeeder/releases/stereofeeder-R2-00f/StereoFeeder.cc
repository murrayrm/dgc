
/* 
 * Desc: Stereo feeder module using JPLV
 * Date: 09 November 2006
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
  
#if USE_FB
#include <vis-tools/frame_buffer.h>
#endif

#include <jplv/jplv_stereo.h>

#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_log.h>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/StereoImageBlob.h>

#include <ncurses.h>
#include <cotk/cotk.h>

// Camera drivers
#include "StereoCamera.hh"
#include "SimStereoCamera.hh"
#include "BBStereoCamera.hh"
#include "PGRStereoCamera.hh"

// Raw image logging
#include "raw_log.h"

#include "cmdline.h"


/// @brief Stereo feeder class
class StereoFeeder
{
  public:   

  /// Default constructor
  StereoFeeder();

  /// Default destructor
  ~StereoFeeder();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Parse the config file
  int parseConfigFile(const char *configPath);

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // Our module id (SkyNet)
  modulename moduleId;

  // Our sensor id (SensNet)
  sensnet_id_t sensorId;

  // What mode are we in?
  enum {modeSim, modeBB, modePGR} mode;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Is logging enabled?
  bool enableLog;

  public:
  
  /// Initialize camera
  int initCamera();
  
  /// Finalize camera
  int finiCamera();

  /// Capture a frame 
  int captureCamera();

  // Stereo camera driver
  StereoCamera *camera;

  // Pyramid level for downsampling input image
  int imageLevel;
  
  // Raw image dimensions
  int imageCols, imageRows, imageChannels;

  // Camera models
  jplv_cmod_t leftModel, rightModel;

  // Sensor position (x,y,z) and rotation (roll,pitch,yaw).  Used for
  // display only.
  vec3_t sensPos, sensRot;

  // Sensor-to-vehicle transform
  float sens2veh[4][4];

  public:

  // Initialize image logging
  int initRawLog();

  /// Finalize image logging
  int finiRawLog();

  /// Write current data to image log
  int writeRawLog();

  // Raw image log
  raw_log_t *imageLog;

  // Raw image log stats
  int imageLogCount, imageLogSize;
  
  public:

  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();
  
  /// Publish data
  int writeSensnet();

  // SensNet handle
  sensnet_t *sensnet;

  // SensNet log file
  sensnet_log_t *sensnet_log;
  
  // Sensnet log file name
  char logName[1024];

  // Sensnet logging stats
  int logCount, logSize;

  // Workspace for stereo blob
  StereoImageBlob *blob;

  public:
  
  /// Initialize everything except capture
  int initStereo();

  /// Finalize everything except capture
  int finiStereo();
  
  /// Process a frame
  int process();
  
  /// Get the predicted vehicle state
  int getState(double timestamp);
  
  /// Get the process state
  int getProcessState();

  // Run fake stereo using ground-plane constraint
  int calcPlaneDisp();
  
  // Current state data
  VehicleState state;

  // Stereo handle
  jplv_stereo_t *stereo;

  // Current frame id
  int frameId;

  // Current frame time (microseconds)
  uint64_t frameTime;
  
  // Start time for computing stats
  uint64_t startTime;
  
  // Stereo stats
  uint64_t stereoTime;

  public:
  
  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, StereoFeeder *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, StereoFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserLog(cotk_t *console, StereoFeeder *self, const char *token);
  
  /// Console button callback
  static int onUserDisplay(cotk_t *console, StereoFeeder *self, const char *token);

  /// Console button callback
  static int onUserExport(cotk_t *console, StereoFeeder *self, const char *token);

  // Display current image to framebuffer
  int displayImage();

  // Write the current images to a file
  int saveImage();

  // Console text display
  cotk_t *console;

  // Current export frame id
  int exportId;

  // Display mode for the frame buffer (0 = none, 1 = left, 2 = right, 3 = disparity)
  int fbMode;
  
#if USE_FB
  // Console frame buffer
  struct frame_buffer_t *fb;
#endif

};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
StereoFeeder::StereoFeeder()
{
  memset(this, 0, sizeof(*this));

  this->frameId = -1;

  return;
}


// Default destructor
StereoFeeder::~StereoFeeder()
{  
  return;
}


// Parse the command line
int StereoFeeder::parseCmdLine(int argc, char **argv)
{  
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Use raw image logging or sensnet logging, but not both
  if (this->options.enable_log_flag && this->options.enable_imagelog_flag)
    return ERROR("sensnet logging and image logging cannot be enabled at the same time");
  
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
    
  // Fill out sensor id
  this->sensorId = sensnet_id_from_name(this->options.sensor_id_arg);
  if (this->sensorId <= SENSNET_NULL_SENSOR)
    return ERROR("invalid sensor id: %s", this->options.sensor_id_arg);

  // Fill out the default config path from the environment if not
  // given on the command line.
  if (!this->options.config_path_given)
  {
    char configPath[256];      
    if (getenv("DGC_CONFIG_PATH") == NULL)
      return ERROR("unknown configuration path: please set DGC_CONFIG_PATH");
    snprintf(configPath, sizeof(configPath), "%s/stereofeeder", getenv("DGC_CONFIG_PATH"));
    this->options.config_path_arg = strdup(configPath);
  }
  
  // Load configuration file
  if (this->parseConfigFile(this->options.config_path_arg) != 0)
    return -1;

  return 0;
}


// Parse the config file
int StereoFeeder::parseConfigFile(const char *configPath)
{
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, sensnet_id_to_name(this->sensorId));
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id (may be in config file)
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Parse transform
  float px, py, pz;
  float rx, ry, rz;
  sscanf(this->options.sens_pos_arg, "%f, %f, %f", &px, &py, &pz);
  sscanf(this->options.sens_rot_arg, "%f, %f, %f", &rx, &ry, &rz);

  // Record pose (for display only)
  this->sensPos = vec3_set(px, py, pz);
  this->sensRot.x = rx * 180/M_PI;
  this->sensRot.y = ry * 180/M_PI;
  this->sensRot.z = rz * 180/M_PI;

  // Compute sensor-to-vehicle transform
  pose3_t pose;
  pose.pos = vec3_set(px, py, pz);
  pose.rot = quat_from_rpy(rx, ry, rz);
  pose3_to_mat44f(pose, this->sens2veh);
  
  return 0;
}


// Initialize cameras
int StereoFeeder::initCamera()
{
  char filename[256];

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%s-left.cahvor",
           this->options.config_path_arg, this->options.left_camera_arg);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->leftModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  // Load camera model
  snprintf(filename, sizeof(filename), "%s/%s-right.cahvor",
           this->options.config_path_arg, this->options.right_camera_arg);
  MSG("loading %s", filename);
  if (jplv_cmod_read(&this->rightModel, filename) != 0)
    return ERROR("unable to load camera model %s : %s", filename, jplv_error_str());

  if (this->options.sim_flag)
  {
    SimStereoCamera *simCamera;
    
    // Initialize simulated camera driver
    this->mode = modeSim;
    this->camera = simCamera = new SimStereoCamera();
    simCamera->open(this->leftModel.core.xdim, this->leftModel.core.ydim);
  }
  else 
  {
    // Create appropriate driver
    if (strcasecmp(this->options.driver_arg, "bb") == 0)
    {
      this->mode = modeBB;
      this->camera = new BBStereoCamera();
    }
    else if (strcasecmp(this->options.driver_arg, "pgr") == 0)
    {
      this->mode = modePGR;
      this->camera = new PGRStereoCamera();
    }
    else
      assert(false);

    // Initialize real camera
    if (this->camera->open(this->options.left_camera_arg, this->options.left_config_arg,
                           this->options.right_camera_arg, this->options.right_config_arg) != 0)
      return ERROR("unable to open cameras %s %s",
                   this->options.left_camera_arg, this->options.right_camera_arg);

    // Initialize AGC settings
    this->camera->initAGC(this->options.camera_exposure_arg);
  }

  // Record key image dimensions
  this->imageLevel = this->options.level_arg;  
  this->imageCols = this->camera->imageCols >> this->imageLevel;
  this->imageRows = this->camera->imageRows >> this->imageLevel;
  this->imageChannels = this->camera->imageChannels;

  MSG("camera image %dx%dx%d",
      this->camera->imageCols, this->camera->imageRows, this->camera->imageChannels);  
  MSG("feeder image %dx%dx%d L%d",
      this->imageCols, this->imageRows, this->imageChannels, this->imageLevel);
         
  return 0;
}


// Finalize camera
int StereoFeeder::finiCamera()
{
  assert(this->camera);
  this->camera->close();
  delete this->camera;
  this->camera = NULL;

  return 0;
}


// Capture a frame 
int StereoFeeder::captureCamera()
{
  uint64_t cycleTime;

  if (this->mode == modeSim)
  {
    SimStereoCamera *simCamera;
    float baseline;
    jplv_cmod_t cmod;
    jplv_image_t *rectImage, *dispImage;
    
    // Simulate 15Hz
    usleep(75000);
 
    // Get the current state data
    if (this->getState(DGCgettime()) != 0)
    {
      MSG("unable to get state; using dummy values");
      memset(&this->state, 0, sizeof(this->state));
      this->state.timestamp = DGCgettime();
    }
    
    // Camera model
    jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &cmod);    
    baseline = jplv_stereo_get_baseline(this->stereo);

    // Get pointers to the image buffers
    rectImage = jplv_stereo_get_image(this->stereo,
                                      JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
    dispImage = jplv_stereo_get_image(this->stereo,
                                      JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

    // Capture simulated image
    simCamera = (SimStereoCamera*) this->camera;
    simCamera->captureSim(&this->state, this->sens2veh, &cmod, baseline, rectImage, dispImage);
  }
  else
  {
    jplv_image_t *leftImage, *rightImage;
      
    // Get pointers to the raw image buffers
    leftImage = jplv_stereo_get_image(this->stereo,
                                      JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
    rightImage = jplv_stereo_get_image(this->stereo,
                                       JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

    // Capture an image, or timeout
    MSG("capture image");      
    if (this->camera->capture(100, leftImage, rightImage) != 0)
      return MSG("unable to capture image");

    // Get the matching state data
    MSG("get state");
    if (this->getState(this->frameTime) != 0)
      return MSG("unable to get state");
  }

  // Get timestamp
  this->frameId = this->camera->frameId;  
  this->frameTime = this->camera->timestamp;

  // Compute some stats
  if (this->frameId > 0)
    cycleTime = (DGCgettime() - this->startTime) / this->frameId;
  else
    cycleTime = 0;

  if (this->console)
  {
    cotk_printf(this->console, "%capid%", A_NORMAL, "%5d %.3f",
                this->frameId, fmod(1e-6 * (double) this->frameTime, 10000));
    if (cycleTime < 1000000)
      cotk_printf(this->console, "%capstats%", A_NORMAL,
                  "%03dms/%4.1fHz %3d/%3d %3.1f/%3.1fdB %5.2f/%5.2fms ",
                  (int) (cycleTime / 1000), (float) (1e6 / cycleTime), 
                  this->camera->exposure[0], this->camera->exposure[1],
                  this->camera->gain[0], this->camera->gain[1],
                  this->camera->shutter[0] * 1e3, this->camera->shutter[1] * 1e3);
    if (this->state.timestamp > 0)
      cotk_printf(this->console, "%slat%", A_NORMAL,
                  "%+6dms", (int) (this->state.timestamp - this->frameTime) / 1000);
  }

  return 0;
}


// Initialize image logging
int StereoFeeder::initRawLog()
{
  time_t t;
  char timestamp[64];
  char cmd[256];
  char logName[1024];
  raw_log_header_t header;

  if (!this->options.enable_imagelog_flag)
    return 0;

  // Construct log name
  t = time(NULL);
  strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
  snprintf(logName, sizeof(logName), "%s/%s-%s-RAW",
           this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

  MSG("opening image log %s", logName);

  // Set up header using stereo image sizes; we will copy the data
  // from the stereo raw buffers.
  assert(this->stereo);
  header.left_camera = atoi(this->options.left_camera_arg);
  header.right_camera = atoi(this->options.right_camera_arg);
  jplv_stereo_get_dims(this->stereo, &header.cols, &header.rows, &header.channels);
  
  // Initialize image logging
  this->imageLog = raw_log_alloc();
  assert(this->imageLog);
  if (raw_log_open_write(this->imageLog, logName, &header) != 0)
    return ERROR("unable to open log: %s", logName);
    
  // Copy configuration files
  snprintf(cmd, sizeof(cmd), "cp %s/%s-left.cahvor %s",
           this->options.config_path_arg, this->options.left_camera_arg, logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/%s-right.cahvor %s",
           this->options.config_path_arg, this->options.right_camera_arg, logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
           this->options.config_path_arg, sensnet_id_to_name(this->sensorId), logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/raw_log.h %s",
           this->options.config_path_arg, logName);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "cp %s/raw_log.c %s",
           this->options.config_path_arg, logName);
  system(cmd);

  return 0;
}


// Finalize image logging
int StereoFeeder::finiRawLog()
{
  if (this->imageLog)
  {
    raw_log_close(this->imageLog);
    raw_log_free(this->imageLog);
    this->imageLog = NULL;
  }  
  return 0;
}


// Write current data to image log
int StereoFeeder::writeRawLog()
{
  raw_log_tag_t tag;
  jplv_image_t *leftImage, *rightImage;
  
  if (!this->imageLog)
    return 0;
  if (!this->enableLog)
    return 0;

  // Construct the image tag
  tag.frameid = this->frameId;
  tag.timestamp = this->frameTime;
  tag.state = this->state;
  memcpy(tag.sens2veh, this->sens2veh, sizeof(tag.sens2veh));

  // Copy camera settings
  tag.gain[0] = this->camera->gain[0];
  tag.gain[1] = this->camera->gain[1];
  tag.shutter[0] = this->camera->shutter[0];
  tag.shutter[1] = this->camera->shutter[1];

  // Reset reseved values
  memset(tag.reserved, 0, sizeof(tag.reserved));

  // Get current raw images
  leftImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  rightImage = jplv_stereo_get_image(this->stereo,
                                     JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);

  // Write to log
  if (raw_log_write(this->imageLog, &tag,
                    leftImage->data_size, leftImage->data, rightImage->data) != 0)
    return ERROR("unable to write to log");

  // Update stats
  this->imageLogCount += 1;
  this->imageLogSize += leftImage->data_size + rightImage->data_size;

  // Update console
  if (this->console)
  {
    cotk_printf(this->console, "%log%", A_NORMAL, "%df %dMb",
                this->imageLogCount, this->imageLogSize / 1024 / 1024);
  }
  
  return 0;
}
  

// Initialize sensnet
int StereoFeeder::initSensnet()
{
  // Check that blob size is a multiple of [something].  This allows
  // for DMA transfers.
  if (sizeof(StereoImageBlob) % 512 != 0)
    return ERROR("invalid blob size %d; needs padding of %d",
                 sizeof(StereoImageBlob), 512 - sizeof(StereoImageBlob) % 512);

  // Create page-aligned blob to enable DMA logging
  this->blob = (StereoImageBlob*) valloc(sizeof(StereoImageBlob));
 
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
    return ERROR("unable to connect to sensnet");

  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to process state messages
  if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process group");

  // Initialize sensnet logging
  if (this->options.enable_log_flag)
  {
    time_t t;
    char timestamp[64];
    char cmd[256];
    sensnet_log_header_t header;

    // Construct log name
    t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%F-%a-%H-%M", localtime(&t));
    snprintf(this->logName, sizeof(this->logName), "%s/%s-%s",
             this->options.log_path_arg, timestamp, sensnet_id_to_name(this->sensorId));

    MSG("opening log %s", this->logName);
    
    // Initialize sensnet logging
    this->sensnet_log = sensnet_log_alloc();
    assert(this->sensnet_log);
    memset(&header, 0, sizeof(header));
    if (sensnet_log_open_write(this->sensnet_log, this->logName, &header, true) != 0)
      return ERROR("unable to open log: %s", this->logName);
    
    // Copy configuration files
    snprintf(cmd, sizeof(cmd), "cp %s/%s-left.cahvor %s",
             this->options.config_path_arg, this->options.left_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s-right.cahvor %s",
             this->options.config_path_arg, this->options.right_camera_arg, this->logName);
    system(cmd);
    snprintf(cmd, sizeof(cmd), "cp %s/%s.CFG %s",
             this->options.config_path_arg, sensnet_id_to_name(this->sensorId), this->logName);
    system(cmd);
  }
 
  return 0;
}


// Finalize sensnet
int StereoFeeder::finiSensnet()
{  
  if (this->sensnet_log)
  {
    sensnet_log_close(this->sensnet_log);
    sensnet_log_free(this->sensnet_log);
    this->sensnet_log = NULL;
  }

  sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  free(this->blob);
  this->blob = NULL;
  
  return 0;
}


// Publish image data
int StereoFeeder::writeSensnet()
{
  pose3_t pose;
  jplv_cmod_t model;
  jplv_image_t *image;
  float mf[4][4];
  double md[4][4];
  StereoImageBlob *blob;
  int imageSize, blobSize;

  blob = this->blob;

  blob->blobType = SENSNET_STEREO_IMAGE_BLOB;
  blob->version = STEREO_IMAGE_BLOB_VERSION;
  blob->sensorId = this->sensorId;
  blob->frameId = this->frameId;
  blob->timestamp = this->frameTime;
  
  // Get the rectified model and copy to blob
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  blob->leftCamera.cx = model.ext.cahv.hc;
  blob->leftCamera.cy = model.ext.cahv.vc;
  blob->leftCamera.sx = model.ext.cahv.hs;
  blob->leftCamera.sy = model.ext.cahv.vs;    

  // Compute left camera transform (and inverse)
  jplv_cmod_get_transform(&model, md);
  mat44f_setd(mf, md);
  mat44f_mul(blob->leftCamera.sens2veh, this->sens2veh, mf);
  mat44f_inv(blob->leftCamera.veh2sens, blob->leftCamera.sens2veh);

  // Get the rectified model and copy to blob
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &model);    
  blob->rightCamera.cx = model.ext.cahv.hc;
  blob->rightCamera.cy = model.ext.cahv.vc;
  blob->rightCamera.sx = model.ext.cahv.hs;
  blob->rightCamera.sy = model.ext.cahv.vs;    

  // Compute right camera transform (and inverse)
  jplv_cmod_get_transform(&model, md);
  mat44f_setd(mf, md);
  mat44f_mul(blob->rightCamera.sens2veh, this->sens2veh, mf);
  mat44f_inv(blob->rightCamera.veh2sens, blob->rightCamera.sens2veh);

  // Camera baseline
  blob->baseline = jplv_stereo_get_baseline(this->stereo);
  assert(blob->baseline > 0);

  // Copy camera settings
  blob->leftCamera.gain = this->camera->gain[0];
  blob->leftCamera.shutter = this->camera->shutter[0];
  blob->rightCamera.gain = this->camera->gain[1];
  blob->rightCamera.shutter = this->camera->shutter[1];

  // Copy vehicle state
  blob->state = this->state;

  // Vehicle to local transform
  pose.pos = vec3_set(blob->state.localX,
                      blob->state.localY,
                      blob->state.localZ);
  pose.rot = quat_from_rpy(blob->state.localRoll,
                           blob->state.localPitch,
                           blob->state.localYaw);  
  pose3_to_mat44f(pose, blob->veh2loc);
  mat44f_inv(blob->loc2veh, blob->veh2loc);  

  // Reset reseved values
  memset(blob->reserved, 0, sizeof(blob->reserved));

  // Set image data
  jplv_stereo_get_dims(this->stereo, &blob->cols, &blob->rows, &blob->channels);

  // Keep track of total image data stored
  imageSize = 0;
  
  // Copy left rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  blob->leftOffset = imageSize;
  blob->leftSize = image->data_size;
  imageSize += image->data_size;  
  memcpy(blob->imageData + blob->leftOffset, image->data, image->data_size);
  
  // Copy right rectified data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_RIGHT);
  blob->rightOffset = imageSize;
  blob->rightSize = image->data_size;
  imageSize += image->data_size;
  memcpy(blob->imageData + blob->rightOffset, image->data, image->data_size);

  // Copy disparity data
  image = jplv_stereo_get_image(this->stereo,
                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  blob->dispScale = JPLV_STEREO_DISP_SCALE;
  blob->dispOffset = imageSize;
  blob->dispSize = image->data_size;
  imageSize += image->data_size;
  memcpy(blob->imageData + blob->dispOffset, image->data, image->data_size);

  // Compute the size of the blob that was actually used
  blobSize = sizeof(*blob) - sizeof(blob->imageData) + imageSize;
    
  // Send blob, but over the shmem channel only
  if (sensnet_write(this->sensnet, SENSNET_METHOD_SHMEM, this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                    this->frameId, blobSize, blob) != 0)
    return ERROR("unable to write blob");

  // Write to log
  if (this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    if (sensnet_log_write(this->sensnet_log, DGCgettime(),
                          this->sensorId, SENSNET_STEREO_IMAGE_BLOB,
                          this->frameId, blobSize, blob) != 0)
      return ERROR("unable to write blob");
  }
  
  // Keep some stats
  if (this->console && this->sensnet_log && (this->enableLog || this->options.always_log_flag))
  {
    this->logCount += 1;
    this->logSize += blobSize / 1024;
    cotk_printf(this->console, "%log%", A_NORMAL,
                "%df %dMb", this->logCount, this->logSize / 1024);
  }

  return 0;
}


// Initialize everything except image capture
int StereoFeeder::initStereo()
{
  if (true)
  {
    MSG("initializing stereo");
    
    // Create stereo context
    this->stereo = jplv_stereo_alloc(this->imageCols, this->imageRows, this->imageChannels);
    if (!this->stereo)
      return ERROR("unable to allocate stereo context: JPLV %s", jplv_error_str());
    
    // Re-scale the models if ncessary
    jplv_cmod_scale_dims(&this->leftModel, this->imageCols, this->imageRows);
    jplv_cmod_scale_dims(&this->rightModel, this->imageCols, this->imageRows);

    // Set the model in the stereo context
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_LEFT, &this->leftModel);
    jplv_stereo_set_cmod(this->stereo, JPLV_STEREO_CAMERA_RIGHT, &this->rightModel);

    // Set defaults
    jplv_stereo_set_disparity(this->stereo, 0, 40, 7, 7);

    // Prepare for action
    jplv_stereo_prepare(this->stereo);

    MSG("done initializing stereo");
  }

  return 0;
}


// Finalize everything except capture
int StereoFeeder::finiStereo()
{
  // Clean up stereo
  jplv_stereo_free(this->stereo);
  this->stereo = NULL;
  
  return 0;
}


// Get the predicted vehicle state
int StereoFeeder::getState(double timestamp)
{
  int blobId;

  // Default to all zeros in state
  memset(&this->state, 0, sizeof(this->state));

  // Get the state newest data in the cache
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR,
                   SNstate, &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state");
  if (blobId < 0)
    return ERROR("state is invalid"); 
   
  // TODO: do prediction by comparing image/state timestamps
      
  // Update the display
  if (this->console)
  {
    cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
                fmod((double) this->state.timestamp * 1e-6, 10000));
    cotk_printf(this->console, "%spos%", A_NORMAL, "%+09.3f %+09.3f %+09.3f",
                this->state.localX, this->state.localY, this->state.localZ);
    cotk_printf(this->console, "%srot%", A_NORMAL, "%+06.1f %+06.1f %+06.1f",
                this->state.localRoll*180/M_PI,
                this->state.localPitch*180/M_PI,
                this->state.localYaw*180/M_PI);
  }

  return 0;
}


// Get the process state
int StereoFeeder::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  response.logSize = this->logSize;
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;
  this->enableLog = request.enableLog;  
  
  return 0;
}


// Process a frame
int StereoFeeder::process()
{
  uint64_t cpuTime, cycleTime;
  jplv_stereo_stats_t stats;
  float frac;

  cpuTime = DGCgettime();
  
  if (this->mode != modeSim)
  {
    // Run regular stereo pipeline
    jplv_stereo_rectify(this->stereo);  

    if (this->options.force_plane_flag)
    {
      // Run fake stereo using ground-plane constraint
      this->calcPlaneDisp();
    }
    else
    {
      // Run real stereo
      jplv_stereo_laplace(this->stereo);
      jplv_stereo_disparity(this->stereo);
    }
  }
  
  jplv_stereo_range(this->stereo);  
  jplv_stereo_diag(this->stereo);

  // Compute elapsed time for update
  cpuTime = DGCgettime() - cpuTime;
  cycleTime = DGCgettime() - this->stereoTime;
  this->stereoTime = DGCgettime();

  jplv_stereo_get_stats(this->stereo, &stats);
  
  // Compute some diagnostics
  frac = (float) stats.valid_pixel_count / stats.total_pixel_count;

  if (this->console)
  {
    cotk_printf(this->console, "%stid%", A_NORMAL, "%5d %.3f",
                this->frameId,  fmod(1e-6 * (double) this->frameTime, 10000));
    if (cycleTime < 1000000)
    {
      cotk_printf(this->console, "%ststats%", A_NORMAL, "%03dms/%.1fHz %3dms %3d%%",
                  (int) (cycleTime / 1000), (float) (1e6 / cycleTime), 
                  (int) (cpuTime / 1000), (int) (frac * 100));
    }
  }

  return 0;
}


// Run fake stereo using ground-plane constraint
int StereoFeeder::calcPlaneDisp()
{
  jplv_image_t *dispImage;
  uint16_t *dpix;
  int pi, pj;
  float px, py, pz;
  float cx, cy, sx, sy, baseline;
  float mv[4][4];

  // Disparity image
  dispImage = jplv_stereo_get_image(this->stereo,
                                    JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);

  // Camera model
  jplv_cmod_t model;
  jplv_stereo_get_cmod_rect(this->stereo, JPLV_STEREO_CAMERA_LEFT, &model);    
  cx = model.ext.cahv.hc;
  cy = model.ext.cahv.vc;
  sx = model.ext.cahv.hs;
  sy = model.ext.cahv.vs;    
  baseline = jplv_stereo_get_baseline(this->stereo);

  // Camera-to-vehicle transform
  assert(sizeof(mv) == sizeof(this->sens2veh));
  memcpy(mv, this->sens2veh, sizeof(mv));

  // TODO: consider robot attitude
  
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < dispImage->rows; pj++)
  {
    for (pi = 0; pi < dispImage->cols; pi++)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      dpix += 1;
    }
  }

  return 0;
}


// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"StereoFeeder $Revision$                                                    \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"Sensor: %sensor%                                                           \n"
"                                                                           \n"
"Capture                                State                               \n"
"Mode  : %mode%                         Time   : %stime% %slat%             \n"
"Camera: %cam%                          Pos    : %spos%                     \n"
"Frame : %capid%                        Rot    : %srot%                     \n"
"Stats : %capstats%                                                         \n"
"                                                                           \n"
"Stereo  %stereo%                       Export: %export%                    \n"
"Frame : %stid%                         Log   : %log%                       \n"
"Stats : %ststats%                      %logname%                           \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%|%LOG%|%DISPLAY%|%EXPORT%]                                  \n";



// Initialize console display
int StereoFeeder::initConsole()
{
  char filename[1024];

#if USE_FB
  if (this->options.enable_fb_given)
  {
    // Initialize the frame buffer
    this->fb = frame_buffer_open("/dev/fb0");
    if (this->fb == NULL)
      return ERROR("unable to open frame buffer");
  }
#endif
  
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
  cotk_bind_toggle(this->console, "%LOG%", " LOG ", "Ll",
                   (cotk_callback_t) onUserLog, this);
  cotk_bind_button(this->console, "%DISPLAY%", " DISPLAY ", "Dd",
                   (cotk_callback_t) onUserDisplay, this);
  cotk_bind_button(this->console, "%EXPORT%", " EXPORT ", "Ee",
                   (cotk_callback_t) onUserExport, this);
    
  // Initialize the display
  snprintf(filename, sizeof(filename), "%s/%s.msg",
           this->options.log_path_arg, sensnet_id_to_name(this->sensorId));
  if (cotk_open(this->console, filename) != 0)
    return -1;

  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
  cotk_printf(this->console, "%sensor%", A_NORMAL, sensnet_id_to_name(this->sensorId));

  if (this->camera)
    cotk_printf(this->console, "%cam%", A_NORMAL, "%dx%dx%d",
                this->camera->imageCols, this->camera->imageRows, this->camera->imageChannels);
  if (this->stereo)
    cotk_printf(this->console, "%stereo%", A_NORMAL, "%dx%dx%d L%d",
                this->imageCols, this->imageRows, this->imageChannels, this->imageLevel);

  if (this->mode == modeSim)
    cotk_printf(this->console, "%mode%", A_NORMAL, "sim   ");
  else if (this->mode == modeBB)
    cotk_printf(this->console, "%mode%", A_NORMAL, "bb %s ", this->options.left_camera_arg);
  else if (this->mode == modePGR)
    cotk_printf(this->console, "%mode%", A_NORMAL, "pgr %s %s",
                this->options.left_camera_arg, this->options.right_camera_arg);

  cotk_printf(this->console, "%logname%", A_NORMAL, this->logName);
      
  return 0;
}


// Finalize console display
int StereoFeeder::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }

  if (this->fb)
  {
    frame_buffer_close(this->fb);
  }
  
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserQuit(cotk_t *console, StereoFeeder *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserPause(cotk_t *console, StereoFeeder *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Handle user events
int StereoFeeder::onUserLog(cotk_t *console, StereoFeeder *self, const char *token)
{
  assert(self);
  self->enableLog = !self->enableLog;
  MSG("log %s", (self->enableLog ? "on" : "off"));
  return 0;
}


// Handle user events
int StereoFeeder::onUserDisplay(cotk_t *console, StereoFeeder *self, const char *token)
{
  // Toggle through the display modes
  assert(self);
  self->fbMode = (self->fbMode + 1) % 4;
  MSG("frame buffer %d", self->fbMode);
  return 0;
}


// Handle button callbacks
int StereoFeeder::onUserExport(cotk_t *console, StereoFeeder *self, const char *token)
{
  // Write out the current image to a file
  MSG("exporting image");
  self->saveImage();
  return 0;
}



// Display image to framebuffer
int StereoFeeder::displayImage()
{
  jplv_image_t *image;

  // TODO?
  // Select image to display
  if (this->fbMode % 2 == 0)
    image = jplv_stereo_get_image(this->stereo,
                                  JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_LEFT);
  else if (this->fbMode % 2 == 1)
    image = jplv_stereo_get_image(this->stereo,
                                  JPLV_STEREO_STAGE_RECT, JPLV_STEREO_CAMERA_RIGHT);
  //else if (this->fbMode == 3)
  //  image = jplv_stereo_get_image(this->stereo,
  //                                JPLV_STEREO_STAGE_DISP, JPLV_STEREO_CAMERA_LEFT);
  else
    return 0;

#if USE_FB
  if (this->fb && this->console)
  {
    struct fb_var_screeninfo vinfo;

    // Get the screen dimensions
    frame_buffer_get_vinfo(this->fb, &vinfo);

    // Draw to upper right of screen
    frame_buffer_draw(this->fb, vinfo.xres - image->cols, 0, image->cols, image->rows,
                      image->bits/8, image->channels, image->data);
  }
#endif
  
  return 0;
}


// Write the current images to a file
int StereoFeeder::saveImage()
{
  jplv_image_t *image;
  char filename[256];
  
  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_LEFT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_LEFT.pnm", sensnet_id_to_name(this->sensorId), this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  image = jplv_stereo_get_image(this->stereo, JPLV_STEREO_STAGE_RAW, JPLV_STEREO_CAMERA_RIGHT);
  snprintf(filename, sizeof(filename),
           "%s_%04d_RIGHT.pnm", sensnet_id_to_name(this->sensorId), this->exportId);
  if (jplv_image_write_pnm(image, filename, NULL) != 0)
    return ERROR("unable to write image: %s", jplv_error_str());

  this->exportId++;
  
  if (this->console)
    cotk_printf(this->console, "%export%", A_NORMAL, "%d", this->exportId);

  return 0;
}



// Main program thread
int main(int argc, char **argv)
{
  int status;
  uint64_t lastTime;
  StereoFeeder *feeder;

  // Create feeder
  feeder = new StereoFeeder();
  assert(feeder);
 
  // Parse command line options
  if (feeder->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize camera
  if (feeder->initCamera() != 0)
    return -1;

  // Initialize sensnet
  if (feeder->initSensnet() != 0)
    return -1;

  // Initialize algorithms
  if (feeder->initStereo() != 0)
    return -1;

  // Initialize image logging
  if (feeder->initRawLog() != 0)
    return -1;

  // Initialize console
  if (!feeder->options.disable_console_flag)
  {
    if (feeder->initConsole() != 0)
      return -1;
  }

  lastTime = 0;
  feeder->startTime = DGCgettime();
  
  // Start processing
  while (!feeder->quit)
  {
    // Do heartbeat occasionally
    if (feeder->frameId % 2 == 0)
      feeder->getProcessState();

    // Capture incoming frame directly into the stereo buffers
    if (feeder->captureCamera() != 0)
      break;

    // Display current image on framebuffer
    feeder->displayImage();
    
    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);

    // If paused, or if we are running too fast, give up our time
    // slice.
    if (feeder->pause || DGCgettime() - lastTime < (uint64_t) (1e6/feeder->options.rate_arg))
    {
      usleep(0);
      continue;
    }
    lastTime = DGCgettime();
    
    // Capture and process one frame
    status = feeder->process();
    if (status != 0)
      break;

    // Publish data
    if (feeder->writeSensnet() != 0)
      break;
    feeder->writeRawLog();

    // Update the console
    if (feeder->console)
      cotk_update(feeder->console);
  }
  
  // Clean up
  feeder->finiConsole();
  feeder->finiRawLog();
  feeder->finiStereo();
  feeder->finiSensnet();
  feeder->finiCamera();
  cmdline_parser_free(&feeder->options);
  delete feeder;

  MSG("program exited cleanly");
  
  return 0;
}
