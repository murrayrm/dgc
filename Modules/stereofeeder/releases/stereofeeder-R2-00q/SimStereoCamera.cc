/* 
 * Desc: Simulated stereo camera
 * Date: 09 February 2007
 * Author: Andrew Howard
 * CVS: $Id$
*/

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include <frames/pose3.h>
#include <frames/mat44.h>
#include <dgcutils/ggis.h>
#include <alice/AliceConstants.h>
#include "SimStereoCamera.hh"


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
SimStereoCamera::SimStereoCamera()
{
  this->mode = modePattern;
  
  return;
}


// Destructor
SimStereoCamera::~SimStereoCamera()
{
  return;
}
  

// Initialize the camera
int SimStereoCamera::open(int cols, int rows, int channels)
{
  // Record key image dimensions
  this->imageCols = cols;
  this->imageRows = rows;
  this->imageChannels = channels;

  return 0;
}


// Close the camera
int SimStereoCamera::close()
{
  return 0;
}


// Capture a simulated frame
int SimStereoCamera::captureSim(const VehicleState *state, const float sens2veh[4][4],
                                const jplv_cmod_t *cmod, float baseline,
                                jplv_image_t *rectImage, jplv_image_t *dispImage)
{
  // Generate basic pattern first
  this->captureSimPattern(state, sens2veh, cmod, baseline, rectImage, dispImage);

  // Generate KML lines if loaded
  if (this->mode == modeKML)
    this->captureSimKml(state, sens2veh, cmod, baseline, rectImage, dispImage);
  
  return 0;
}


// Capture a simulated frame using a pattern on the groun
int SimStereoCamera::captureSimPattern(const VehicleState *state, const float sens2veh[4][4],
                                       const jplv_cmod_t *cmod, float baseline,
                                       jplv_image_t *rectImage, jplv_image_t *dispImage)
{
  uint8_t *rpix;
  uint16_t *dpix;
  float cx, cy, sx, sy;
  pose3_t pose;
  float mv[4][4];
  double ml[4][4];
  double mg[4][4];
  int pi, pj;
  int qi, qj;
  float px, py, pz;
  float vx, vy, vz;
  double lx, ly;

  // Camera model
  cx = cmod->ext.cahv.hc;
  cy = cmod->ext.cahv.vc;
  sx = cmod->ext.cahv.hs;
  sy = cmod->ext.cahv.vs;

  // Camera-to-vehicle transform
  memcpy(mv, sens2veh, sizeof(mv));

  // Vehicle-to-local transform
  pose.pos = vec3_set(state->localX, state->localY, state->localZ);
  pose.rot = quat_from_rpy(state->localRoll, state->localPitch, state->localYaw);
  pose3_to_mat44d(pose, ml);

  // Vehicle-to-global transform
  pose.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  pose.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);
  pose3_to_mat44d(pose, mg);

  rpix = (uint8_t*) jplv_image_pixel(rectImage, 0, 0);
  dpix = (uint16_t*) jplv_image_pixel(dispImage, 0, 0);
  for (pj = 0; pj < rectImage->rows; pj++)
  {
    for (pi = 0; pi < rectImage->cols; pi++, rpix += rectImage->channels, dpix += 1)
    {
      // Compute the ray/z-plane intersection in camera coordinates;
      // the matrix m specifies the camera pose relative to the plane.
      px = (pi - cx) / sx;
      py = (pj - cy) / sy;
      pz = -(mv[2][3] - VEHICLE_TIRE_RADIUS) / (mv[2][0] * px + mv[2][1] * py + mv[2][2]);
      px = px * pz;
      py = py * pz;

      if (pz < 0)
      {
        rpix[0] = 0xC0;
        if (rectImage->channels == 3)
          rpix[2] = rpix[1] = rpix[0];
        continue;
      }

      // Compute disparity
      dpix[0] = (uint16_t) (unsigned int) (int) (sx / pz * JPLV_STEREO_DISP_SCALE * baseline);
      
      if (this->mode == modePattern)
      {
        // Convert to vehicle coordinates
        vx = mv[0][0] * px + mv[0][1] * py + mv[0][2] * pz + mv[0][3];
        vy = mv[1][0] * px + mv[1][1] * py + mv[1][2] * pz + mv[1][3];
        vz = mv[2][0] * px + mv[2][1] * py + mv[2][2] * pz + mv[2][3];

        // Convert to local coordinates
        lx = ml[0][0] * vx + ml[0][1] * vy + ml[0][2] * vz + ml[0][3];
        ly = ml[1][0] * vx + ml[1][1] * vy + ml[1][2] * vz + ml[1][3];

        // Checker board
        if (true)
        {
          qi = (int) ((lx + 100000) / 1.00);
          qj = (int) ((ly + 100000) / 1.00);
          rpix[0] = 0x80 + ((qi + qj) % 2) * 0x7F;
        }

        // Lines every 20m
        if (false)
        {
          rpix[0] = 0x20;
          if (fmod(lx + 1e6, 20) < 0.12)
            rpix[0] = 0xD0;
          if (fmod(ly + 1e6, 20) < 0.12)
            rpix[0] = 0xD0;
        }
      }

      if (rectImage->channels == 3)
        rpix[2] = rpix[1] = rpix[0];
    }
  }

  this->frameId += 1;
  this->timestamp = state->timestamp;
  
  return 0;
}



// Load a data from a KML file
int SimStereoCamera::loadKml(const char *filename)
{
  const char *ch;
  double ax, ay, bx, by;
  Line *nline;
  xmlDocPtr doc;
  xmlNode *root, *folder, *place, *line, *coord;
   
  this->mode = modeKML;
  this->numLines = 0;

  // Load the file
  doc = xmlReadFile(filename, NULL, 0);
  if (doc == NULL)
    return ERROR("unable to open/parse %s", filename);

  // Get the root element node
  root = xmlDocGetRootElement(doc);

  // Get the document node
  for (root = root->children; root != NULL; root = root->next)
  {
    if (root->type == XML_ELEMENT_NODE &&
        strcmp((const char*) root->name, "Document") == 0)
      break;
  }
  assert(root);

  // Parse each folder node
  for (folder = root->children; folder != NULL; folder = folder->next)
  {
    if (folder->type != XML_ELEMENT_NODE)
      continue;
    if (strcmp((const char*) folder->name, "Folder") != 0)
      continue;

    // Parse each placemark
    for (place = folder->children; place != NULL; place = place->next)
    {
      if (place->type != XML_ELEMENT_NODE)
        continue;
      if (strcmp((const char*) place->name, "Placemark") != 0)
        continue;

      // Parse each line
      for (line = place->children; line != NULL; line = line->next)
      {
        if (line->type != XML_ELEMENT_NODE)
          continue;
        if (strcmp((const char*) line->name, "LineString") != 0)
          continue;

        // Parse each coordinate list
        for (coord = line->children; coord != NULL; coord = coord->next)
        {
          if (coord->type != XML_ELEMENT_NODE)
            continue;
          if (strcmp((const char*) coord->name, "coordinates") != 0)
            continue;
        
          // Parse out a set of lines.  HACK this is fragile.
          ch = (const char*) xmlNodeGetContent(coord);
          while (ch)
          {
            GisCoordUTM utm;
            GisCoordLatLon geo;

            // Read out two points
            if (sscanf(ch, "%lf,%lf,0 %lf,%lf,0", &ax, &ay, &bx, &by) < 4)
              break;

            //MSG("point %f,%f %f,%f", ax, ay, bx, by);

            // Convert to UTM
            geo.latitude = ay;
            geo.longitude = ax;
            gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
            ax = utm.n;
            ay = utm.e;

            geo.latitude = by;
            geo.longitude = bx;
            gis_coord_latlon_to_utm(&geo, &utm, GIS_GEODETIC_MODEL_DEFAULT);
            bx = utm.n;
            by = utm.e;

            MSG("point %f,%f %f,%f", ax, ay, bx, by);
            
            // Create line
            assert((size_t) this->numLines < sizeof(this->lines)/sizeof(this->lines[0]));
            nline = this->lines + this->numLines++;
            nline->ax = ax;
            nline->ay = ay;
            nline->bx = bx;
            nline->by = by;
            nline->len = sqrtf(pow(nline->bx - nline->ax, 2) + pow(nline->by - nline->ay, 2));

            // Advance one point in the coordinates list
            ch = strchr(ch + 1, ' ');
          }
        }
      }
    }
  }

  xmlFreeDoc(doc);
  
  return 0;
}


// Capture a simulated frame using KML model 
int SimStereoCamera::captureSimKml(const VehicleState *state, const float sens2veh[4][4],
                                   const jplv_cmod_t *cmod, float baseline,
                                   jplv_image_t *rectImage, jplv_image_t *dispImage)
{
  int i, j, k;
  uint8_t *cpix;
  float cx, cy, sx, sy;
  pose3_t pose;
  double mv[4][4], m[4][4];
  double gx, gy, gz;
  double px, py, pz;
  int ic, ir, id;
  Line *line;
  int hits;
  int stepCount;
  double stepSize;

  // Camera model
  cx = cmod->ext.cahv.hc;
  cy = cmod->ext.cahv.vc;
  sx = cmod->ext.cahv.hs;
  sy = cmod->ext.cahv.vs;

  // Camera-to-vehicle transform
  mat44d_setf(mv, sens2veh);

  // Vehicle-to-global transform
  pose.pos = vec3_set(state->utmNorthing, state->utmEasting, state->utmAltitude);
  pose.rot = quat_from_rpy(state->utmRoll, state->utmPitch, state->utmYaw);
  pose3_to_mat44d(pose, m);

  // Camera-to-global transform
  mat44d_mul(m, m, mv);
  
  // Global to camera transform
  mat44d_inv(m, m);
  
  // Draw each line into the image; this is faster than projecting
  // each ray.
  for (i = 0; i < this->numLines; i++)
  {
    line = this->lines + i;

    hits = 0;
        
    // First pass at coarse resolution
    stepCount = (int) (line->len / 0.10); // MAGIC
    stepSize = 1.0 / stepCount;   
    for (j = 0; j < stepCount; j++)
    {
      gx = line->ax + j*stepSize*(line->bx - line->ax);
      gy = line->ay + j*stepSize*(line->by - line->ay);
      gz = state->utmAltitude + VEHICLE_TIRE_RADIUS;

      // Transform into sensor frame
      px = m[0][0] * gx + m[0][1] * gy + m[0][2] * gz + m[0][3];
      py = m[1][0] * gx + m[1][1] * gy + m[1][2] * gz + m[1][3];
      pz = m[2][0] * gx + m[2][1] * gy + m[2][2] * gz + m[2][3];

      // Project into camera
      ic = (int) (px / pz * sx + cx);
      ir = (int) (py / pz * sy + cy);
      id = (int) (baseline / pz * sx);
      if (id < 0)
        continue;

      // Get pixel
      cpix = jplv_image_pixel(rectImage, ic, ir);
      if (!cpix)
        continue;
      cpix[0] = 0xFF;
      if (rectImage->channels == 3)
        cpix[2] = cpix[1] = cpix[0];

      hits++;
    }

    // If there were no points projected into the image, give up
    if (hits == 0)
      continue;

    // First pass at fine resolution
    stepCount = (int) (line->len / 0.01); // MAGIC
    stepSize = 1.0 / stepCount;   
    for (j = 0; j < stepCount; j++)
    {
      gx = line->ax + j*stepSize*(line->bx - line->ax);
      gy = line->ay + j*stepSize*(line->by - line->ay);
      gz = state->utmAltitude + VEHICLE_TIRE_RADIUS;

      // Transform into sensor frame
      px = m[0][0] * gx + m[0][1] * gy + m[0][2] * gz + m[0][3];
      py = m[1][0] * gx + m[1][1] * gy + m[1][2] * gz + m[1][3];
      pz = m[2][0] * gx + m[2][1] * gy + m[2][2] * gz + m[2][3];

      // Project into camera
      ic = (int) (px / pz * sx + cx);
      ir = (int) (py / pz * sy + cy);
      id = (int) (baseline / pz * sx);
      if (id < 0)
        continue;

      // Update pixel
      for (k = -3; k <= +3; k++)
      {
        cpix = jplv_image_pixel(rectImage, ic + k, ir);
        if (!cpix)
          continue;
        cpix[0] = 0xFF;
        if (rectImage->channels == 3)
          cpix[2] = cpix[1] = cpix[0];
      }

      hits++;
    }
  }

  return 0;
}


// Query the value at a pixel
int SimStereoCamera::getKmlPixel(double gx, double gy)
{
  int i;
  Line *line;
  double d;
  
  for (i = 0; i < this->numLines; i++)
  {
    line = this->lines + i;

    d = (line->bx - line->ax)*(line->ay - gy) - (line->ax - gx)*(line->by - line->ay);
    d = fabs(d)/line->len;

    //printf("%d %f %f %f\n", i, gx, gy, d);
    
    if (d < 0.10) // MAGIC
      return 0xFF;
  }
  
  return 0x80;
}
