
/* Desc: Simple test for SensNet logging of stereo data.
 *       Note that this exercises SensNet logs, not raw image logs.
 * Author: Andrew Howard
 * Date: 12 Mar 2005
 * CVS: $Id$
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <interfaces/StereoImageBlob.h>
#include <sensnet/sensnet_log.h>


// Error macros
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


int write_ppm(int cols, int rows, uint8_t *data, const char *filename)
{
  int i, j;
  FILE *fp;
  int r, g, b;

  MSG("writing %s", filename);
  fp = fopen(filename, "w");
  fprintf(fp, "P3\n%i %i\n255\n", cols, rows);
  for (j = 0; j < rows; j++) {
    for (i = 0; i < cols; i++) {
      r = data[3*(j*cols+i)];
      g = data[3*(j*cols+i)+1];
      b = data[3*(j*cols+i)+2];
      fprintf(fp, "%d %d %d ", r, g, b);
    }
    fprintf(fp, "\n");
  }
  fclose(fp);

  return 0;
}


// Simple log self-test.
int main(int argc, char **argv)
{
  char *filename;
  sensnet_log_t *log;
  sensnet_log_header_t header;
  StereoImageBlob *blob;
    
  if (argc < 2)
  {
    printf("usage: %s <FILENAME>\n", argv[0]);
    return -1;
  }
  filename = argv[1];

  // Create log
  log = sensnet_log_alloc();
  assert(log);

  // Open log
  MSG("opening %s", filename);
  if (sensnet_log_open_read(log, filename, &header) != 0)
    return -1;

  blob = malloc(sizeof(*blob));
      
  while (true)
  {
    int blob_id;
    
    // Read blob
    if (sensnet_log_read(log, NULL, NULL, NULL, &blob_id, sizeof(blob), blob) != 0)
      return -1;

    MSG("read blob %d frame %d time %.3f",
        blob_id, blob->frameId, (double) blob->timestamp * 1e-6);
       
    // Write blob as PPM file
    if (blob->channels == 3)
    {
      char filename[1024];
      uint8_t *im;

      // Write left image
      im = StereoImageBlobGetLeft(blob, 0, 0);
      snprintf(filename, sizeof(filename), "%06d-left.ppm", blob->frameId);
      write_ppm(blob->cols, blob->rows, im, filename);

      // Write right image
      im = StereoImageBlobGetRight(blob, 0, 0);
      snprintf(filename, sizeof(filename), "%06d-right.ppm", blob->frameId);
      write_ppm(blob->cols, blob->rows, im, filename);
    }
  }

  // Clean up
  free(blob);
  sensnet_log_close(log);
  sensnet_log_free(log);
  
  return 0;
}

