# This program will save the load averages it gets from uptime in a file, loads
# format: ./loadAverage.sh host1 host2 ...

# Catch an interrupt and or hangup and just 
# make sure that tmpFile is deleted
del_file()
{
    rm -f $tmpFile
    exit 7
}

trap del_file 1
trap del_file 2
#trap del_file 3


filePath=$DGC_CONFIG_PATH/startup
outFile=$filePath/loads
tmpFile=$filePath/tmpFile

# if tmpFile exists, this program is already running, so exit
#if [ -f $tmpFile ]
#then
#    exit 2
#fi

# if no arguments were passed, then just exit
if [ "$#" == "0" ] 
then
    exit 1
fi

# clear the tmpFile, and remove the outFile, so that
# no one tries to read it while this program is running
: > $tmpFile
rm $outFile >/dev/null 2>&1


# read in all of the hosts
numHosts=0
while [ "$#" != "0" ]
do
    array[numHosts]=$1
    numHosts=`expr $numHosts + 1`
    shift
done

# get the uptimes for each of the host and output it to a file
for(( i=0; i<numHosts; i++ ))
do
  if [ "${array[i]}" = "localhost" ]; then
      data=`uptime`
  else
      data=`ssh -o ConnectTimeout=5 ${array[i]} uptime`
  fi
  
  # if we got data, then output just the uptime to the file
  if [ "$data" != "" ]; then
      echo $data | sed 's/.*load average: //' | awk '{print $1}' | sed 's/,//' >> $tmpFile
  else # if no output, then that means the host could not be reached
      echo 99.99 >> $tmpFile
  fi
done

mv $tmpFile $outFile