# This will tell you what height to start the process-control window with, as
# it will count the number of processes running and give you the exact height

# first go through and figure out what 
#  files contain the modules we are running
while [ "$#" != "0" ]
do
  if [ "$1" == "--sim" ]; then
      moduleList="sim.CFG"
  elif [ "$1" == "--alicesim" ];  then
      moduleList="alicesim.CFG"
  elif [ "$1" == "--field" ]; then
      moduleList="moduleList.CFG"
  elif [ "$1" == "--nqe" ]; then
      moduleList="nqe.CFG"
  elif [ "$1" == "--race" ]; then
      moduleList="race.CFG"
  elif [ `echo $1 | grep "\-\-cfg="` ]; then
      moduleList=`echo $1 | sed 's/--cfg=//'`
  elif [ `echo $1 | grep "\-\-otherProcs="` ]; then
      otherModules=`echo $1 | sed 's/--otherProcs=//'`
  fi
  shift
done

# if no otherProcs file specified, process-control will use default.CFG
if [ "$otherModules" == "" ]; then
    otherModules="default.CFG"
fi


# now go through and count the modules that will run
filePath=$DGC_CONFIG_PATH/startup
numModules=0

# special case if in field configuration.
if [ "$moduleList" == "moduleList.CFG" ]
then
     while read line
     do
       if [ `echo $line | grep "^[[:alpha:]]"` ]
       then
	   let numModules=$numModules+1
       fi
     done < $filePath/moduleList.CFG
elif [ "$moduleList" != "" ]
then
    while read line
      do
      echo $line | grep "^#" >/dev/null 2>&1
      if [ "$?" != "0" ] && [ `echo $line | grep module-id` ]
      then
	  let numModules=$numModules+1
      fi
    done < $filePath/$moduleList
fi

# now count the otherProcs file as well
while read line
  do
  echo $line | grep "^#" >/dev/null 2>&1
  if [ "$?" != "0" ] && [ `echo $line | grep module-id` ]
      then
      let numModules=$numModules+1
  fi
done < $filePath/$otherModules

#echo $numModules

let height=$numModules/2+$numModules%2+23

# return what the height of the window should be
exit $height