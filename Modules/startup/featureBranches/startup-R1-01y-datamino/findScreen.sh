# Finds out if a screen exists or not. If grep is successful,
# then the screen exists, if not, then screen does not exist

screen -ls | grep $1 >/dev/null