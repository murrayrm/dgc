# This program will return the load average that it gets from uptime
# format: ./loadAverage.sh host1 host2 ...

#filePath=$DGC_CONFIG_PATH/startup
#outFile=$filePath/loads
outFile=/tmp/loads

if [ -f /tmp/tmpFile ]
then
    exit 1
fi

if [ "$#" == "0" ] 
then
    exit 1
fi

: > /tmp/tmpFile

numHosts=0
while [ "$#" != "0" ]
do
    array[numHosts]=$1
    numHosts=`expr $numHosts + 1`
    shift
done

: > $outFile

for(( i=0; i<numHosts; i++ ))
do

  if [ "${array[i]}" = "localhost" ]
  then
      data=`uptime`
  else
      data=`ssh ${array[i]} uptime`
  fi
  #echo $data | sed 's/.*load average: //' | awk '{print $1}' | sed 's/,//'
  echo $data | sed 's/.*load average: //' | awk '{print $1}' | sed 's/,//' >> $outFile
done

rm /tmp/tmpFile