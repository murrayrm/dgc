              Release Notes for "startup" module

Release R1-03p (Fri Aug  1 17:32:26 2008):
	Doubled the allowable length of command line strings.

Release R1-03o (Sat Jul 19 15:41:49 2008):
	Added defaultVrSim.CFG that specifies the modules to be run in
	simulation with vrsim instead of trafsim. Modified ProcessControl
	so it properly starts vrsim and gazebo when a command line argument
	'vrsim' is passed to the system-tests script.

Release R1-03n (Thu Jun 19 15:06:48 2008):
	Trying to fix the problem with YAM when making a release. (It complained that R1-03 already exists.)

Release R1-03m (Thu Jun 19 14:58:40 2008):
	Reduced the noise in asim (gps-stddev=0.02, gyro-stddev=0.005)

Release R1-03m (Mon Nov 12 10:10:01 2007):
	* Danielle's attempt to allow blades to run all simulations.
	* updated flags for sim.CFG
	* don't stop two far away from stop lines

Release R1-03l (Thu Oct 25  2:21:20 2007):
	Updated nqe.CFG and race.CFG.

Release R1-03k (Tue Oct 23 14:47:53 2007):
	Added nqe and race configuration

Release R1-03j (Sun Oct 21  2:55:42 2007):
	Changed command line options for astate so it runs with the 
local frame drifting with respect to the site frame.

Release R1-03i (Sat Oct 20 22:24:47 2007):
	Took out --disable-status-check from gcdrive command line in
	fieldMaster (and field) configuration files.  This option allows
	Alice to move when steering, brake or estop are not working => not
	the desired behavior.  This is why we were moving when steering was
	not working.  With this change, Alice will not go into run if
	steering is not working. 

Release R1-03h (Wed Oct 17 23:29:33 2007):
	Fixed the planner cmd-line. (Somehow there were other options after
	2> /dev/null.)

Release R1-03g (Tue Oct 16 15:55:57 2007):
        Changed command line arguments for follower to use different
	parameter file for simulations (so that ROA does not stop use while
	simulating).  Noel made changes so that fused perceptor is not used
	in simulation. Also lane lines are not updated in simulation.

Release R1-03f (Mon Oct 15 23:16:22 2007):
	Added --start-chute option. If given, process-control will start
	mplanner with --start-chute option. But it will not restart
	mplanner with this option.

Release R1-03e (Mon Oct 15 10:01:50 2007):
	* Added astate to moduleList and fieldMaster. (It needs --rndf
	argument so we need to make sure that it's consistent with the rest
	of the system.) Process control will listen to SNstate to determine
	if it's running. 
	* Commented out mapper from moduleList. (We're
	running mapper internal to planner.)

Release R1-03d (Sat Oct 13 15:32:24 2007):
	Only check that the planner actually dies before restarting it.
	Other processes are restarted right away if no heartbeat message
	has been received for 1 second. (Checking takes a long time and we
	don't want to keep the car moving without processes like gcdrive
	running.)

Release R1-03c (Thu Oct 11 12:37:13 2007):
	Handle --rndf-start for mplanner. (If mplanner needs to be
	restarted by process control, it won't restart with the
	--rndf-start option.)

Release R1-03b (Tue Oct  9  4:34:11 2007):
	Small change to which binary is executed for the attention 
module in fieldMaster.CFG

Release R1-03a (Mon Oct  8 23:30:07 2007):
	* Keep track of how many times process control restarts each 
	process and log it in the log analysis file.
	* Added --fake-wheelspeed option to gcdrive. 

Release R1-03 (Mon Oct  8 10:45:22 2007):
	Adding the correct flags to trajFollower in sim.CFG. Had previously been removed by nok in 
the previous release. This should now allow alice to behave normally and not all out of control due 
to some strange integrator windup. 

Release R1-02z (Fri Oct  5 19:28:59 2007):
	Added the 'monitored' option. If this is set to 1, process control
	will restart this module if it dies. Otherwise, even we run process
	control with auto-restart option, it will not restart it

Release R1-02y (Fri Oct  5 10:49:38 2007):
        Changed sim.CFG. This release is mainly intended for use by the navigation team to resolve bugs from the 2007-10-04 el Toro tests.

Release R1-02x (Thu Oct  4  5:08:12 2007):
	* 'a' now logs rigel & PTU ladars as well 
	* default config runs ladar-obs-perceptor instead of the car-perceptors
	(and changed the cmd line for mapper to not try to do fusion on the 
	obs-perceptor's output yet)

Release R1-02w (Thu Oct  4  4:53:04 2007):
	Sensing updates to the fieldMaster and moduleList config files.

Release R1-02v (Wed Oct  3  1:33:56 2007):
	Took out --trajRecvTimeout flag from follower

Release R1-02u (Sun Sep 30  9:12:12 2007):
	Added a alicesim-lab.CFG file to simulate using multiple
	machines in the lab.  This file should be manually
	copied/linked to alicesim.CFG to use it.

Release R1-02t (Sat Sep 29 10:14:26 2007):
	I accidentally committed a version of sim.CFG that set the
	planner nad mapper to run on zoidberg. Reverted back to
	previous version where they run on localhost

Release R1-02t-nok (Sun Sep 30  8:55:13 2007):
	alicesim.CFG is now a sym link to either alicesim-lab.CFG or
	alicesim-alice.CFG

Release R1-02s (Sat Sep 29  9:41:40 2007):
	Changed the planner cmd args in sim.CFG to reflect recent changes.

Release R1-02r (Sat Sep 29  9:25:03 2007):
	Added ability to turn off sending road lines from trafsim. To turn this on, uncomment the "#cmd-line="--noRoadLines"" line in default.CFG. Be careful because process control is very picky about how to read in trafsim command line opts--it needs to look exactly like the line in default.CFG

Release R1-02q (Fri Sep 28  7:12:17 2007):
	Cleaned up options to run mapper internal to planner, but mapper is
	still run external by default for now

Release R1-02p (Tue Sep 25 22:22:49 2007):
	* Increased the timeout in mplanner to 60s to make sure that the
	planner has enough time to decide to do something before mplanner
	jumps in to make sure that we keep moving. Changed verbose level in
	planner to 5.
	* Removed blank.CFG

Release R1-02o (Tue Sep 25 15:20:53 2007):
	More re-arranging of where processes run. Some clean up as well 
of options for stereo-obs-perceptor.

Release R1-02n (Fri Sep 21 23:40:45 2007):
	Changing which processes run where on Alice. This is more to handle the problems we're facing with various 
kernel builds on the blades (CANbus support, no SMP support, etc.)

Release R1-02m (Thu Sep 20 15:05:29 2007):
	Removed --log-path=/tmp option from radarFeederPTU and radarFeeder
	so logs go to /logs

Release R1-02l (Tue Sep 18 19:54:52 2007):
	Fixed some bugs based on the coverity results.

Release R1-02k (Tue Sep 18  1:49:35 2007):
	Get PID from env variable when starting a process.
	(Thanks to Josh D for figureing out how to do this.) Then 
	write to the file bin/pid/procName. Check, using PID instead of
	process name, that a process actually dies before restarting it.
	Autorestart should work properly now even when a process is 
	slow and doesn't send heartbeat messages often enough.

Release R1-02j (Mon Sep 17  9:36:27 2007):
	Added --nomap to mplanner in sim.CFG

Release R1-02i (Sun Sep 16 11:01:06 2007):
	Added --auto-enable flag for gcdrive and --listen-to-estop and
	--keep-forward-progress for mplanner

Release R1-02h (Fri Sep 14 22:38:24 2007):
	The shell script that finds all of the hosts load averages now 
no longer checks to see if tmpFile exists or not. Load averages should 
now always be displayed, whether all the machines in alice die or not.

	Added comments, cleaned up code.

Release R1-02g (Thu Sep 13 14:25:43 2007):
	Logging is now in the otherProcs files. You can now also choose 
what machine to run the logging, or any other processes in the 
otherProcs file on. In the field configuration, skynet-logger is set to 
run on bunker.

Release R1-02f (Thu Sep 13 13:54:47 2007):
	Since sensor-sim now has a config file, removed command line opts from startup file

Release R1-02e (Tue Sep 11 13:53:31 2007):
	Changed --output-rate to 100000 for ladarCarPerceptor in
	fieldMaster.CFG. This might cause Tosin to be so small in mapviewer when we were testing yesterday. (We changed the rate and did the same run at St Luke today and didn't see this problem again.)

Release R1-02d (Tue Sep 11 12:29:44 2007):
	Auto-restart:
	- Since planner sometimes does not send heart beat messages for 
a long time, when process-control thinks about restarting planner, it 
makes sure that the process 'planner' is not running on its host 
computer before restarting it.
	- If a process is killed in the console, process-control will 
not try to restart it
	
	Other Changes:
	- Added timeouts for all ssh commands that didn't previously 
have them.

Release R1-02c (Tue Sep 11  9:33:40 2007):
	Added line to trafsim startup to load stop lines

Release R1-02b (Mon Sep 10 15:58:41 2007):
	- Improved keyboard responsiveness.
        - Fixed compiler warning, which was an actual bug in this case.
	- Added an option to log the debugging output of process-control.
        - One less host per line in load averages.

Release R1-02a (Mon Sep 10 14:26:45 2007):
	Fixed process-control screen so that it is narrower, so now it 
should fit on the lower resolution screens in alice. 

	There is now no delay when scrolling around in the 
process-control window. This was really, really annoying and thanks to 
Daniele for figuring out how to fix it.

	If any changes are made to moduleList.CFG while process-control 
is running, the screen now gets updated as well when you try to restart 
everything, in case you added or removed a module. This way the console 
will actually correspond to the correct modules.

Release R1-02 (Fri Sep  7 19:59:15 2007):
	Fixed small bug where file containing load averages was deleted 
before process-control could read it. Only happens when there is almost 
no load on any of the machines, so not really that big of a deal.

Release R1-01z (Fri Sep  7 18:56:55 2007):
	Made process-control wider, so that there are 2 columns of 
processes instead of one. This way we can see everything in the field, 
even if we are running 40-50 processes. 
	Also changed some exit-handling stuff with the shell script that 
was calculating load averages.
	Wrote a script that figures out the number of lines 
process-control will use before process-control is started. System-tests 
will use this to create the correct size window.

Release R1-01y (Thu Sep  6 13:42:49 2007):
	Updated fieldMaster.CFG so we run mapper on bunker, planner on
	blade11. Run mapper in sim.CFG

Release R1-01x (Wed Sep  5 16:27:05 2007):
	Core files should now actually be generated when running in the 
field. Basically what was happening was that for some reason when you 
ssh and then run a process on the same line, the space alotted for core 
files is 0. So now, whenever I ssh, I just set that space to unlimited, 
as is the default everywhere else..

	Also added a restart button for each process, which does what it 
says, and I switched the buttons around so view is the first one, as we 
use this one the most.

Release R1-01w (Tue Sep  4  1:44:54 2007):
	Configuration used in the 09/03 evening testing. Didn't see spread
	error in mapper for 20-25 mins.

Release R1-01v (Tue Sep  4  0:43:22 2007):
	Added a timeout for ssh when starting a process, and when 
looking at load averages. Load averages should now show up on the 
display.

	Command line options and config file options are now read in 
separately so that random errors dont occur when you try to only read in 
the config file again.

	Changed cmd line option from startall to no-start, which in case 
you can't guess, makes it so that all processes do not start 
automatically.

Release R1-01u (Sun Sep  2 16:47:58 2007):
	adding --decay-thresh=2 to default simulation configs for mapper

Release R1-01t (Sun Sep  2 14:03:06 2007):
	* Changes in the field made by Robbie -- fixed the display when a lot
	of modules are running.
	* By default, mapper is internal to planner
	* Do not run mapviewer on gcfield

Release R1-01s (Fri Aug 31  1:54:51 2007):
	Added attention to startup scripts; also added roadperceptor (though disabled by default until 
I can verify the command line options with Humberto)

Release R1-01r (Thu Aug 30 23:45:07 2007):
	process-control now monitors load averages of any machines you 
use and outputs this to the screen. 
	
	Now creates screens skynetkey appended to names of the processes 
so multiple process-controls can be run on 1 computer. (although this is 
NOT advisable, unless you are using some beast computer)

	Log ladars button now only shows up in field configuration.	

Release R1-01q (Wed Aug 29 15:00:55 2007):
	Added PTU radar (MODradarFeederPTU) to startup scripts.

Release R1-01p (Wed Aug 29 11:11:07 2007):
	Added a polysim.CFG file for runningonly the software polysim 
needs (no trafsim...) Corrected some behavior with reloading the config 
files.

Release R1-01o (Tue Aug 28 18:39:44 2007):
	This release is mostly for polysim, update if you wish, but more changes 
are on the way in the next couple of days... (like less brutal colors)

	Added colors to process-control so you can see which line you are on when 
you have like 20 sensors displayed. 

Added northing, easting, and yaw option for 
polysim; process-control also parses -N, -E, -Y options that are in the cmd-line 
argument in the config files.

Added log ladars option, as of right now, logs anything that has "adar" in it but 
this will be changed soon.

Release R1-01n (Sat Aug 25 11:04:38 2007):
	Changes made in the field on Fri
	* add --continue flag so process-control will not remove mission.log and we can continue the mission without starting from the first checkpoint.
	* planner: add --mapper-use-internal --mapper-decay-thresh=3 
	* run radarfeeder on blade07 instead of leela

Release R1-01m (Thu Aug 23 17:39:43 2007):
	Added a otherProcsField.CFG, which is run when we are in the 
field and doesn't start trafsim and sensor-sim. 

Also changed createConfig.py so that it goes to the correct path even if 
it is run from system-tests. Running process-control will now
automatically call createConfig.py as well, and update any changes you 
made to fieldMaster.CFG

Also added --continue flag on mplanner so that if it dies and is 
restarted, it will continue from where it left off. This requires the 
removal of mission.log everytime you start the entire process-control 
stack, which is also done.

Release R1-01l (Thu Aug 23 16:52:02 2007):
	Updated sim.CFG to reflect latest planner flags.
	Put the field.CFG file back in subversion control and creates a 
link in the etc dir.

Release R1-01k (Thu Aug 23 14:03:10 2007):
	Changes based on what was run in field test this morning: 
	*ladarCarPerceptorRear run by default 
	* linePerceptorMFLong NOT run by default (config file doesn't exist) 
	* ptuFeeder run on correct machine (leela) 
	* mapper runs on blade15, runs w/ groundstrike filtering, and uses --decay-thresh=2 
	* MFbumperLadar no longer crops the scan (this was a hack to avoid groundstrikes, but now we filter them out at the intersections)

Release R1-01j (Thu Aug 23 11:38:59 2007):
	Automatically limit length of traversed path in planviewer

Release R1-01i (Thu Aug 23  5:34:31 2007):
	mapper now runs with groundstrike filtering
	makefile no longer creates links to non-existent files (so yam doesn't complain...)

Release R1-01h (Wed Aug 22 17:05:56 2007):
	Changed some navigation cmd line args.
	Removed some deprecated files.

Release R1-01g (Wed Aug 22  2:56:08 2007):
	for ladar-curb-perceptor module changing from MODladarRoadPerceptor
	to MODladarCurbPerceptor

Release R1-01f (Tue Aug 21 12:09:18 2007):
	Since sensorsim kept crashing with spread error -18, so I disabled the roof ladars and riegl by default. hopefully this quick fix will work until I can get the main loop running faster

Release R1-01e (Mon Aug 20 22:40:35 2007):
	* enable line fusion by default
	* Run mapper on blade11
	* Run MODladarFeederMFBumper with --scan-min=-45 and --scan-max=+45
	* Run MODladarCarPerceptorLeft, MODladarCarPerceptorCenter, MODladarCarPerceptorRight with --use-map flag
	* Also run
	** MODladarFeederRearBumper
	** MODladarFeederLFRoof
	** MODladarFeederRFRoof
	** MODladarFeederRiegl
	(They were run on Sunday.)

Release R1-01d (Mon Aug 20 17:42:13 2007):
	Updated field.CFG and fieldMaster.CFG for lineperceptor flags

Release R1-01c (Mon Aug 20 17:29:22 2007):
		Added flag -X to ssh to allow X-11 forwarding

Release R1-01b (Fri Aug 17  5:01:38 2007):
	createConfig.py now also generates a .CFG file in the format desired by Chris's health-monitor

Release R1-01a (Thu Aug 16 17:54:10 2007):
	Added the option 'open' in the config files, and changed window sizes for a couple 
of modules which didn't seem to want to conform to 80x24. If you set open to 1 for any of 
the modules, whenever you start it, it will open a window with that process automatically. 

With this in place, it should be easier for people to customize process-control to their 
liking, opening just the modules they care about and not having to hit 'view' each time.

If you have trouble viewing some of the processes because it looks like half of the
sparrow display is off the screen, try setting open to 1 for that module, and you should be 
able to view the whole thing. 
	
Release R1-01 (Wed Aug 15 22:06:40 2007):
	Another attempt at reducing the amount of time spent in the field adjusting the field.CFG file for different runs. I realized that having separate files for different scenarios is unmaintainable... 

The new setup:
fieldMaster.CFG - this file has all the options, for any module that we should ever run live on Alice. If it's missing yours, let me (Laura) know, and I'll add it. Before committing a changed version of this file, talk to Laura for changes to the sensing modules, or Noel for changes to the planning stack. 
note: the separate entries in this file must be separated by a blank line, and the '#' character is a comment (conventions necessary for the parser to work)

moduleList.CFG - a list of the modules you want to run, each on their own line. Again, '#' indicates a comment. This makes it possible to edit the list of modules and see what's running without haveing to scroll through a long file

createConfig.py - python script that reads in moduleList.CFG and fieldMaster.CFG and outputs the corresponding field.CFG file in the format process-control wants. Note that every time you change one of the other two files you'll need to run "python createConfig.py" in src/startup - even with this extra step, the new setup proved significantly faster to use in the field. 

Release R1-00z (Wed Aug 15 20:21:51 2007):
	Removed a bunch of useless command line options, cleaned up the 
code. Made auto restart a default option. I just noticed that mapper and 
planner seem to stop sending messages for a couple seconds at 
intersections, so if process-control tries to automatically restart 
them, probelms might arise. To not auto restart, type the following:

python stlukeSmallStandard.py no-restart

If people hate the idea of automatically restarting processes that don't 
communicate for a certain amount of time, I can change it back to not 
be default. 

Process-control now also re-reads the CFG files when you go to start a 
process again. So, if you want to change a command line option for a 
process, change the CFG file, kill the process in process-control, and 
start it again, and the change should be reflected.

I also now have system-tests and startup both using the same timestamp 
for log files, so analyzer doesn't cry when it is expecting a log file 
that is 1 minute off.

Release R1-00y (Tue Aug 14 23:12:17 2007):
	* field_all_sensors.CFG that was used in the 8/14 testing. 
	* Removed testMapper from default.CFG. 
	* Added --update-from-map flag to planner.
	* Added field_line_perceptor.CFG

Release R1-00x (Mon Aug 13  1:07:32 2007):
	adding config files for common sensing scenarios.

	I've noticed that a large amount of testing time seems 
to be spent changing around the sensing config files - I believe 
that Robbie is working on a better solution for this, but for now, 
this release should help. I've set it up such that field.CFG is 
simply a symbolic link to a specific scenario's config file 
(ymk all resets the symbolic link to field_all_sensors.CFG, 
which *should* be our default testing setup by now). 
Changing the sensor configuration that we run with only requires 
changing the link, rather than editing the long config file.  

I'm less familiar with the configurations for stereo obstacle 
and line perception - if there are different subsets of those 
perceptors that would be useful to have, we can add more .CFG files.

Release R1-00w (Tue Aug  7  1:42:16 2007):
	Changed commandline options for some sensing module in field.CFG.

Release R1-00v (Mon Aug  6 22:19:40 2007):
	adding correct command line options to ladar-car-perceptor for
	field test tomorrow (turning off debug output, enabling console and
	adding depth to obstacles)  
	
	I also enabled the radar - I see no reason not to run it tomorrow. 
	All the cameras are still commented out - I will leave it to
	Jeremy/Mohamed/Daniele to determine what combination is best there

Release R1-00u (Mon Aug  6 13:47:40 2007):
	Added a flag in sim.CFG and alicesim.CFG for follower

Release R1-00t (Sat Aug  4  1:18:57 2007):
	Added --disable-status-check flag to gcdrive and --noprediction to
	planner

Release R1-00s (Fri Aug  3  1:27:24 2007):
	Fixed the command for running skynet-logger and state-logger.
	Updated field.CFG

Release R1-00r (Tue Jul 31 18:08:57 2007):
	Changed field.CFG to have correct gcdrive cmd-line arguments, now 
running all programs that run on localhost from Drun without ssh-ing to 
localhost, also everything can be shut down when you press 'c' in 
system-tests, ad this is now handled correctly.

Release R1-00q (Tue Jul 31  2:56:17 2007):
adding config files for the sensing team.
	sensingFusionAlice has all the feeders/perceptors (except for the PTU), and the mapper -> tested this morning on alice
	sensingFusionReplay has a sample sensnet-replay entry, as well as all feeders and the mapper

Release R1-00p (Sat Jul 28  1:43:45 2007):
	Changed field.CFG to use correct parameters for a couple of the 
sensing modules. We just tested everything except gcdrive on alice, and 
everything seems to work fine!

Release R1-00o (Fri Jul 27 17:06:37 2007):
	Updated the config files so that there are several different 
configurations that you can choose from, all of them commented out 
except the default one (Noel's idea)
	
	This is hopefully the last release of the day... =(

Release R1-00n (Fri Jul 27 16:09:41 2007):
	Moved the shell scripts to the etc folder, so now startup should 
still work even if it is just a link module

Release R1-00m (Fri Jul 27  6:08:54 2007):
	forgot to add shell scripts an the other procedures config file, 
so added those, commiting again

Release R1-00l (Fri Jul 27  5:24:34 2007):
	Tried to make process-control a little more user 
friendly. Changed process-control to parse cmd-line options in the CFG 
files much better, so that when anything needs to be added, all that 
needs to be changed is the CFG file, nothing else. Putting --rndf 
is enough to get the actual rndf added, no need to put --rndf=%s. Added 
an other processes config file so that you can choose options on 
processes such as mapviewer. Added a shell script to test if you can ssh 
somewhere before process-control actually tries to run the program 
there. Added a kill capability for each individual process. 

	Process control also now communicates with HealthMonitor as to 
the overall health of processes. In the CFG files, there is a 4th option 
called monitored, if this is set to 1, process-control will consider it 
when it is looking at the overall health of the system. For example, we 
really want process-control to make sure that if gcdrive dies, we 
immediately change the overall health of the system to BAD, whereas if 
mapviewer dies, that's not integral for the overall system health. If 
all of the monitored processes are good, then process-control sends out 
over sensnet a SNprocessHealth message of GOOD, but if any of the 
monitored processes are down, then the message is BAD. Also added an 
auto restart option so that processes will automatically be restarted if 
they are detected to have gone down.

Release R1-00k (Fri Jul 20  3:56:24 2007):
	Do not listen to SNSegGoals for mission planner becasue
	it now sends heartbeat messages (SNprocessResponse) to 
	process-control so it's not a special case anymore.

Release R1-00j (Fri Jul 20  0:14:27 2007):
	Modified the field.CFG file to reflect the correct machines for 
the stereofeeders and radarfeeder to run from. They were originally run 
from zoidberg but since it recently died, I've switched some cables 
around to make use of the new firewire ports on leela and amy.

Release R1-00i (Thu Jul 19 16:46:05 2007):
	Fixed the field.CFG file to work properly (there was a typo) and 
added radar sensors to list of modules to start. Also changed some 
funciton names in ProcessControl.cc
	
Release R1-00h (Wed Jul 18 13:14:47 2007):
	Can now view crashed processes, also added mapping and logging capabilities that used to be done in system-tests

Release R1-00g (Thu Jul 12 20:04:36 2007):
	Updated process-control to be called from system-tests module. It uses a config file to call planner modules based on 
what kind of test you want to do. Can also take command line arguments.

Release R1-00f (Wed Jun 13 18:12:05 2007):
  Tweaks for the site visit.

Release R1-00e (Sat Jun  2 14:22:23 2007):
  Added the config file used for GraphPlanner (static obstacle) testing.

Release R1-00d (Sun May  6 14:03:04 2007):

  Some minor performance tweaks.

Release R1-00c (Fri Apr 27 22:45:53 2007):
  
  Added RadarFeeder to the process list and tweaked the path handling.

Release R1-00b (Sun Apr 15 22:38:35 2007):

The process-control utility will start/stop processes on remote machines.
The processes are described in a single configuration file, and can be
started or stopped through an ncurses interfaces.  See, for example,
the test configuration PROCESS_TEST.CFG, which can be run with:

  $ ./process-control $DGC_CONFIG_PATH/startup/PROCESS_TEST.CFG

This will allow the user to control a set of dummy processes running
on the local host.

Release R1-00a (Tue Apr 10 21:21:28 2007):
	This release has a copy of the startup scripts from dgc/trunk.  It
	is intended to be used as a starting point for YaM-compatible
	startup scripts.

Release R1-00 (Sat Mar  3  8:30:37 2007):
	Created.














































































































