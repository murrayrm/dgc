# this is a VERY simple shell script that simply waits for a response, 
# and if the response is "y" then returns 0, and if not, returns 1

read response
if [ $response = "y" ]
then
	exit 0
else
	exit 1
fi
