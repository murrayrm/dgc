#! /bin/sh

# This script will open the navigation stack for testing purposes.
# This script assumes that is it executed from the navigation package 
# home directory.
# example: ~/navigation-cschantz01/

# usage: navstack.sh skynet_key rndf_file mdf_file asim_initial_waypoint
# exmaple: navstack.sh 314 blah.rndf blah.mdf 1.2.2

SKYNET_KEY=$1
DEFAULT_BIN=$PWD/bin/i486-gentoo-linux

# Locations of rndf and mdf should be specified explicitly if not in
# default bin directory

# Rndf file: default = $PWD/bin/i486-gentoo-linux/
RNDF=$DEFAULT_BIN/$2

# Mdf file: default = $PWD/bin/i486-gentoo-linux/
MDF=$DEFAULT_BIN/$3

# PlanViewer:
gnome-terminal --title=planviewer -e "/bin/sh -c 'export SPREAD_DAEMON=$SPREAD_DAEMON; $DEFAULT_BIN/planviewer --skynet-key=$SKYNET_KEY'" &

# Mplanner: -nomap flag for no sensor data
gnome-terminal --title=mplanner -e "/bin/sh -c 'export SPREAD_DAEMON=$SPREAD_DAEMON; $DEFAULT_BIN/mplanner --skynet-key=$SKYNET_KEY --rndf=$RNDF --mdf=$MDF --nomap'" &

# Tplanner:
gnome-terminal --title=tplanner -e "/bin/sh -c 'export SPREAD_DAEMON=$SPREAD_DAEMON; $DEFAULT_BIN/tplanner --skynet-key=$SKYNET_KEY --rndf=$RNDF'" &

# Rddfplanner:
gnome-terminal --title=rddfplanner -e "/bin/sh -c 'export SPREAD_DAEMON=$SPREAD_DAEMON; $DEFAULT_BIN/rddfplanner --skynet-key=$SKYNET_KEY c2 --use-final'" &

# Trajfollower: needs *Gains.dat file in its operating directory
TRAJFOLLOWER_DIR=$PWD/src/trajfollower
gnome-terminal --title=trajfollower -e "/bin/sh -c 'export SKYNET_KEY=$SKYNET_KEY; export SPREAD_DAEMON=$SPREAD_DAEMON; cd $TRAJFOLLOWER_DIR; ln -s i486-gentoo-linux/trajfollower .; ./trajfollower'" &

# Asim: add --nonoise for no noise
gnome-terminal --title=asim -e "/bin/sh -c 'export SPREAD_DAEMON=$SPREAD_DAEMON; $DEFAULT_BIN/asim --skynet-key=$SKYNET_KEY --rndf=$RNDF --rndf-start=$4'"
