#! /usr/bin/env python

listFile = open('moduleList.CFG','r')

outputFile = open('field.CFG', 'w')

module = listFile.readline()

while module!='#END#\n':
	if (len(module) > 0) & (module[0] != '#') & (module != '\n'):
		#open master config file, look for that module
		searchPattern = 'module-id=' + module
		masterFile = open('fieldMaster.CFG','r')		
		s = masterFile.readline()
		while (s != searchPattern) & (len(s) > 0):
		       	s = masterFile.readline()
		if ''==s: #never found pattern
			errorMSG = "Didn't find module-id: " + module
			print errorMSG
		else: #found pattern
			outputFile.write(s)
		       	while (s!='\n') & (s!=''): #blank line in cfg file means done wih that module
	       			s = masterFile.readline()
       				outputFile.write(s)
       			outputFile.write(s) #and, finish off each module with a blank line
	module = listFile.readline()
