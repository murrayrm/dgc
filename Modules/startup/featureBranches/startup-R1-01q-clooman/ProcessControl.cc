/* 
 * Process Control Module for starting/stoping/logging modules remotely. 
 * Date: 15 March, 2007
 * Author: David Trotz
 * CVS: $Id$
 *
 * Modified: Robbie Paolini 7/12/07
*/

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>

#include <signal.h>
#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>

#include "cmdline.h"

volatile sig_atomic_t quit = 0;

void sigintHandler(int);


/// @brief ProcessControl class
class ProcessControl
{
  public:   

  /// Default constructor
  ProcessControl();

  /// Default destructor
  ~ProcessControl();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Read all of the config files
  int readConfigFiles();
  
  /// Parse the config file
  int parseConfigFile();

  /// Parse the route file
  int parseRouteFile();

  /// Parse the otherProcesses file
  int parseOtherProcsFile();

  /// Determine blobType from process id
  int blobTypeFromId(int id);
  
  /// Parse the cmd line in the config file
  char *parseConfigCmd(char *cmdline);
  
  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  /// Console button callback
  static int onUserQuit(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStartAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStopAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserKillAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserViewLoggers(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogLadars(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStartModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserKillModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStopModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserViewModule(cotk_t *console, ProcessControl *self, const char *token);
  

  // Gets the index associated with a given token i.e. %START12% returns 12
  static int getTokenIndex(const char *token);
  
  // Start the given process.
  int startProcess(int index);

  // Send requests
  int sendQuitRequest(int index);
  int sendLogRequest(int index);

  // Update the display with the current process response state
  int updateProcessStatus();

  // Update window status on the screen
  void updateWindowStatus(int index, bool open);
  
  // send out overall health
  // int sendProcessHealth();

  // Kill the given process
  int killProcess(int index);
  
  // View the Given Process
  int viewProcess(int index);

  // Run vizualizer programs
  //int runVisualizers();
  // view visualizer programs
  //int viewVisualizers();

  // run logging programs
  int runLoggers();
  // view logger programs
  int viewLoggers();
  // kill logger programs
  int killLoggers();

  // function to handle interupts
  //void sigintHandler(int);

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  // SensNet handle
  sensnet_t *sensnet;

  // rndf file
  char *rndf;
  // mdf file
  char *mdf;
  // simulation start string
  char *start;
  // scenario name
  char *scenario;
  // obsFile name
  char *obsFile;
  // sceneFunc
  char *sceneFunc;
	
  // Prefix for executables (e.g., navigation-rpaolini01/bin/)
  char binPrefix[1024];
  // start all option
  bool startall;
  // continue option. If this option is added, then whenever you go to start everything, mission.log will NOT be deleted
  bool cont;
  // auto restart option
  bool autoRestart;
  // costmap option
  bool costmap;

  // variable to hold whether we are 
  // starting all processes at this time or not
  bool startingAll;
  // Variable to hold whether we 
  // are killing all processes or not
  bool killingAll;

  //configuration (field, alicesim, sim)
  char *configuration;

  // configuration file names
  char *routeFile;
  char *configFile;
  char *otherProcsFile;
  char *otherProcsFileName;

  // route file name
  char *route;

  //variables for asim:
  double northing;
  double easting;
  double yaw;
  
  //tsfile
  char *tsfile;
  // log path
  char *logpath;
  // time stamp for all log files
  char *timestamp;
  
  // holds number of visualizer programs
  //int numVisualizers;
  // list of visualizers to run
  //char *visualizers[2];
  
  // holds number of logger programs
  int numLoggers;
  // list of loggers to run;
  char *loggers[2];

  // Data for each process
  struct ProcessData
  {
    // Process name
    char *name;
    
    // Process id 
    int id;

    // Expected blob type
    int blobType;

    // Last blob id
    int blobId;

    // Host
    char *host;

    // Command-line
    char cmd[1024];

    // whether process is running or not
    bool running;

    // whether a command to start the process was issued or not
    bool started;

    // Whether this process was killed because the user killed all of the processes
    bool killAlled;

    // whether window for the process is open or not
    bool windowOpen;

    // This option can be specified by the user to 
    // automatically open the process whenever it is started
    bool autoOpen;
    
    // Current process request
    ProcessRequest request;

    //Current process health
    //int healthStatus;
    // Whether or not process is monitored 1=yes, 0=no
    // int monitored;

  };

  // Process list
  int numProcs;
  int numSNProcs, numOtherProcs;

  //int numMonitored; //number of monitored procs
  ProcessData procs[64];
  
  // Console text display
  cotk_t *console;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
ProcessControl::ProcessControl()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
ProcessControl::~ProcessControl()
{
  return;
}

//****************************************************
//****************************************************
// Parse the command line
//****************************************************
//****************************************************
int ProcessControl::parseCmdLine(int argc, char **argv)
{
  //*******************************************************
  // Parse command line options (there's a lot of them =P )
  //*******************************************************

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  cout << "skynetkey=" << skynetKey << endl;
  

  //get the configuration
  if(this->options.field_flag)
    this->configuration="field";
  else if(this->options.alicesim_flag)
    this->configuration="alicesim";
  else if(this->options.sim_flag)
    this->configuration="sim";
  else
    this->configuration="";

  
  // get the rndf file
  if (this->options.rndf_given)
    this->rndf = this->options.rndf_arg;
  else
    this->rndf="";

  // get the mdf file
  if (this->options.mdf_given)
    this->mdf = this->options.mdf_arg;
  else
    this->mdf="";

  // get the start location for simulation
  if (this->options.start_given)
    this->start = this->options.start_arg;
  else
    this->start="";
  
  // get the observation file
  if(this->options.obsFile_given)
    this->obsFile = this->options.obsFile_arg;
  else 
    this->obsFile = "";
  
  // get the scene function
  if(this->options.sceneFunc_given)
    this->sceneFunc = this->options.sceneFunc_arg;
  else 
    this->sceneFunc = "";

  // get the scenario function
  if(this->options.scenario_given)
    this->scenario = this->options.scenario_arg;
  else
    this->scenario = "";

  // get the start all option
  if(this->options.startall_flag)
    this->startall=true;

  if(this->options.continue_flag)
    this->cont=true;

  // get the auto restart option
  if(this->options.no_restart_flag)
    this->autoRestart=false;
  else
    this->autoRestart=true;

  // get the costmap option
  if(this->options.costmap_flag)
    this->costmap=true;
  
  
  // Get optional asim options
  if(this->options.northing_given)
    this->northing = this->options.northing_arg;
  if(this->options.easting_given)
    this->easting = this->options.easting_arg;
  if(this->options.yaw_given)
    this->yaw = this->options.yaw_arg;


  // get the tsfile
  if(this->options.tsfile_given)
    this->tsfile = this->options.tsfile_arg;
  else
    this->tsfile = "";

  // get the tplanner log name
  if (this->options.logpath_given)
    this->logpath = this->options.logpath_arg;
  else
    this->logpath = "";

  // get the time stamp for log files
  if(this->options.timestamp_given)
    this->timestamp = this->options.timestamp_arg;
  else
    this->timestamp = "";
  
  // Get the prefix for executables
  if (this->options.bin_prefix_given)
    snprintf(this->binPrefix,sizeof(binPrefix),this->options.bin_prefix_arg);
  else if (getenv("PWD"))
  {
    //snprintf(this->binPrefix,sizeof(this->binPrefix),"%s/../../bin/i486-gentoo-linux",getenv("PWD"));
    snprintf(this->binPrefix,sizeof(this->binPrefix),"%s/../../bin/",getenv("PWD"));
    //cout<<this->binPrefix<<endl;
  }
  else
    return ERROR("--bin-prefix must be specified");


  // Get route file from command line
  if(this->options.route_given)
  {
    this->routeFile = dgcFindConfigFile(this->options.route_arg, "startup");
    // load the route file
    if(parseRouteFile() != 0)
      return -1;
    //free(this->routeFile);
  }

  //****************************************
  // Get configuration files
  //****************************************
  // save the otherProcs file name before it is wiped
  otherProcsFileName = this->options.otherProcs_arg;
  readConfigFiles();
  
  return 0;
}


//****************************************************
//****************************************************
// Read the config files
//****************************************************
//****************************************************
int ProcessControl::readConfigFiles()
{  
 
  // Get the main configuration file
  if(strcmp(this->configuration,"")!=0 || this->options.cfg_given)
  {
    char *fileName;
    if(strcmp(this->configuration,"field")==0)
    {
      fileName="field.CFG";
      // We need to create the field.CFG 
      // with the following python script
      char cmd[512];
      snprintf(cmd, sizeof(cmd), 
	       "python %s/../etc/startup/createConfig.py", this->binPrefix);
      system(cmd);
    }
    else if(strcmp(this->configuration,"alicesim")==0)
      fileName="alicesim.CFG";
    else if(strcmp(this->configuration,"sim")==0)
      fileName="sim.CFG";
    else
      fileName=this->options.cfg_arg;

    this->configFile = dgcFindConfigFile(fileName, "startup"); 
    // Load the configuration file
    if (parseConfigFile() != 0)
      return -1;
  }
  

  // Get other procedures config file from command line, default is defualt.CFG (duh)

  this->otherProcsFile = dgcFindConfigFile(otherProcsFileName, "startup");
  // Load the other procedures file
  if (parseOtherProcsFile() != 0)
    return -1;
  //free(this->otherProcsFile);
  return 0;
}


//****************************************************
//****************************************************
// Parse the config file
//****************************************************
//****************************************************
int ProcessControl::parseConfigFile()
{
  int i;
  ProcessData *proc;
    
  // Load options from the configuration file
  if (cmdline_parser_configfile(this->configFile, &this->options, false, true, false) != 0)
    MSG("unable to process configuration file %s", this->configFile);

  // If this function is read more than once, 
  // we need to reset the number of processes running
  numSNProcs=0;
  numProcs=0;

  // Loop through the modules in the configuration file and
  // create corresponding entries in the internal process list.
  for (i = 0; i < (int) this->options.module_id_given; i++)
  {
    MSG("reading option");
    // increment number of processes, assign host and module id
    proc = &this->procs[this->numProcs++];
    this->numSNProcs++;
    proc->name=this->options.module_id_arg[i];
    proc->id = modulenamefromString(proc->name);
    proc->host = this->options.hostname_arg[i];    
    proc->autoOpen = this->options.open_arg[i];
    //proc->monitored = this->options.monitored_arg[i];
    //numMonitored+=proc->monitored;
    
    // get the blobType and parse the command line
    proc->blobType=blobTypeFromId(proc->id);
    snprintf(proc->cmd,sizeof(proc->cmd),parseConfigCmd(this->options.cmd_line_arg[i]));

    // Add this host to the list of hosts
  }
  
  return 0;
}

// get the blobType from the id of the process
int ProcessControl::blobTypeFromId(int id)
{
  switch(id)
  {
  case SNasim:
  case SNastate: return SNstate;
  case SNadrive: return SNactuatorstate;
  case SNtrajfollower: return SNadrive_command;
    //case MODmapping: return SNglobalGloNavMapFromGloNavMapLib;
    //case MODmapping: return SNmapElement;
    //case MODmissionplanner: return SNsegGoals;
  case MODtrafficplanner: 
  case MODdynamicplanner: return SNtraj;
  case MODhealthMonitor: return SNvehicleCapability;
    default: return SNprocessResponse;
  }
}


// Parse the cmd-line argument in the CFG file
char *ProcessControl::parseConfigCmd(char *cmdline)
{
  // first separate all of the options into separate slots in an array
  char command[1024]; // actual command that will be run
  char options[10][128]; // will hold each of the options
  char *beg;
  char *end;
  char temp[1024];
  int k=0;
  beg=cmdline;
  end=strchr(cmdline,' ');
  
  // special case if no arguments passed with process
  if(end==NULL)
    return cmdline;

  while(true)
  {
    strncpy(options[k],beg,end-beg);
    options[k][end-beg]='\0'; // add end of string character
    k++;

    beg=end+1;
    end=strchr(beg, ' ');
    
    // special action for last option
    if(end==NULL)
    {
      end=strchr(beg, '\0');
      strncpy(options[k],beg,end-beg);
      options[k][end-beg]='\0';
      break;
    }
  }

  // first option is the actual process, leave that
  strcpy(command,options[0]);
  
  // Wierd things happen for testMapper and mapviewer
  if(strcmp(command, "testMapper")==0)
  {
    strcat(command, " ");
    strcat(command, rndf);
  }
  else if(strcmp(command,"mapviewer")==0 && costmap)
  {
    strcat(command, " --show-costmap");
  }


  // now, go through each of the options, adding a space before
  // if the option is something special, such as an rndf, mdf, 
  // etc, then add to the string setting it equal to internal variables
  for(int i=1; i<=k; i++)
  {
    if(strcmp(options[i],"--rndf")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], rndf);
    }
    else if(strcmp(options[i],"--rndf-start")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], start);
    }
    else if(strcmp(options[i],"--mdf")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], mdf);
    }
    else if(strcmp(options[i],"--log-path")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], logpath);
    }
    else if(strcmp(options[i],"--skynet-key")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp),"=%d",skynetKey);
      strcat(options[i], temp);
    }
    // special case, s1planner, polysim's config file
    else if(strcmp(options[i],"tee")==0)
    {
      char s1plogname[512];
      snprintf(s1plogname, sizeof(s1plogname), " %ss1planner-%s.%s.log",logpath, scenario, timestamp);

      strcat(options[i], s1plogname);
    }
    else if(strcmp(options[i],"-N")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp)," %f",northing);
      strcat(options[i], temp);
    }
    else if(strcmp(options[i],"-E")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp)," %f",easting);
      strcat(options[i], temp);
    }  
    else if(strcmp(options[i],"-Y")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp)," %f",yaw);
      strcat(options[i], temp);
    }
    
    snprintf(temp,sizeof(temp)," %s",options[i]);
    strcat(command,temp);
  }

  return command;
}

//****************************************************
//****************************************************
// Parse the route file
//****************************************************
//****************************************************
int ProcessControl::parseRouteFile()
{
  if (cmdline_parser_configfile(this->routeFile, &this->options, false, false, false) != 0)
    MSG("unable to process route file %s", this->routeFile);
  
  this->start = this->options.start_arg;
  this->scenario = this->options.scenario_arg;
  this->rndf = this->options.rndf_arg;
  this->mdf = this->options.mdf_arg;
  this->obsFile = this->options.obsFile_arg;
  this->sceneFunc = this->options.sceneFunc_arg;
  return 0;
}

//****************************************************
//****************************************************
// Parse the other procedures config file
//****************************************************
//****************************************************
int ProcessControl::parseOtherProcsFile()
{
  if (cmdline_parser_configfile(this->otherProcsFile, &this->options, false, false, false) != 0)
    MSG("unable to process other procedures file %s", this->otherProcsFile);
    
  ProcessData *proc;
  for (int i = numSNProcs; i < (int) this->options.module_id_given; i++)
  {
    MSG("reading option");
    // increment number of processes, assign host and module id
    proc = &this->procs[this->numProcs++];
    this->numOtherProcs++;
    proc->name = this->options.module_id_arg[i];
     
    // parse the command line
    snprintf(proc->cmd,sizeof(proc->cmd),parseConfigCmd(this->options.cmd_line_arg[i]));
    
    // special case for trafsim
    if(strcmp(proc->name,"trafsim")==0)
    {
      snprintf(proc->cmd,sizeof(proc->cmd), "python -i %s/../lib/PYTHON/trafsim/GenericApp.py --loadFile %s --send-subgroup -12 --loadZones %s",this->binPrefix, this->tsfile, this->rndf);

      if(this->sceneFunc!=NULL && strcmp(this->sceneFunc,"")!=0)
      {
	strcat(proc->cmd, " --loadFunction ");
	strcat(proc->cmd, sceneFunc);
	strcat(proc->cmd, " --sleep 8");
      }
    }
  }

  return 0;
}

// Initialize sensnet
int ProcessControl::initSensnet(/*const char *configPath*/)
{
  int i;
  ProcessData *proc;
  
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, MODprocessControl) != 0)
    return ERROR("unable to connect to sensnet");
      
  // Subscribe to groups (sensnet gives each process has a unique group)
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];

    if (proc->blobType == SNprocessResponse)
    {
      if (sensnet_join(this->sensnet,
                       proc->id, SNprocessResponse, sizeof(ProcessResponse)) != 0)
        return ERROR("unable to join process response");
    }
    else
    {
      if (sensnet_join(this->sensnet,
                       SENSNET_SKYNET_SENSOR, proc->blobType, 0x15000) != 0)
	return ERROR("unable to join astate");
    }
  }
  
  return 0;
}


// Finalize sensnet
int ProcessControl::finiSensnet()
{  
  int i;
  ProcessData *proc;

  // Clean up SensNet
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];
    if (proc->blobType == SNprocessResponse)
      sensnet_leave(this->sensnet, proc->id, SNprocessResponse);
    else
      sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, proc->blobType);
  }
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Initialize console display
int ProcessControl::initConsole()
{
  const char *header =
    "-----------------------------------------------------------------------------------------\n"
    " ProcessControl $Revision$ \n"
    "-----------------------------------------------------------------------------------------\n"
    "                                                                                         \n"
    " Process Name                  Logsize  Health  Lat                        %OVHEALTH%    \n"
    "-----------------------------------------------------------------------------------------\n";

  const char *body =
    " %%_desc_%02d%%                   %%_status_%02d%%           [%%STR%02d%%|%%VW%02d%%|%%KL%02d%%|%%L%02d%%]\n";
  
  const char *body2 =
    " %%_desc_%02d%%                   %%s%02d%%                  [%%STR%02d%%|%%VW%02d%%|%%KL%02d%%]\n";

  const char *footer = 
    "                                                                                         \n"
    "-----------------------------------------------------------------------------------------\n"
    "                                                                                         \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    " %stderr%                                                                                \n"
    "                                                                                         \n"
    "                                                                                         \n"
    "                                                                                         \n"
    "-----------------------------------------------------------------------------------------\n"
    "[%QUIT%]         [%START_ALL%|%KILL_ALL%|%LOG_ALL%|%VIEW_LOGGERS%]  %_LOG_LADARS_%\n";

  int i;
  char temp[8192];
  char buf[1024];
  ProcessData *proc;

  // Initialize the console template
  memset(temp, 0, sizeof(temp)); 
  strcpy(temp, header);

  // Loop through the processes and create corresponding entries in
  // the template.
  for (i = 0; i < this->numSNProcs; i++)
  {
    proc = &this->procs[i];    
    snprintf(buf,sizeof(buf), body, i, i, i, i, i, i);
    strcat(temp, buf);
  }
  for(; i < this->numProcs; i++)
  {
  	proc = &this->procs[i];
  	snprintf(buf, sizeof(buf), body2, i, i, i, i, i);
  	strcat(temp, buf);
  }
  strcat(temp, footer);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, temp);
  
  for(i = 0; i < this->numProcs; i++)
  {
    snprintf(buf,sizeof(buf),"%%STR%02d%%",i);
    cotk_bind_button(this->console, buf, " START ", "",
                     (cotk_callback_t) onUserStartModule, this);
    snprintf(buf,sizeof(buf),"%%SP%02d%%",i);
    cotk_bind_button(this->console, buf, " STOP ", "",
                     (cotk_callback_t) onUserStopModule, this);
    snprintf(buf,sizeof(buf),"%%KL%02d%%",i);
    cotk_bind_button(this->console, buf, " KILL ", "",
                     (cotk_callback_t) onUserKillModule, this);
    snprintf(buf,sizeof(buf),"%%L%02d%%",i);
    cotk_bind_button(this->console, buf, " LOG ", "",
                     (cotk_callback_t) onUserLogModule, this);
    snprintf(buf,sizeof(buf),"%%VW%02d%%",i);
    cotk_bind_button(this->console, buf, " VIEW ", "",
		     (cotk_callback_t) onUserViewModule,this);
  }
  for(; i < this->numProcs; i++)
  {
    snprintf(buf,sizeof(buf),"%%STR%02d%%",i);
    cotk_bind_button(this->console, buf, " START ", "",
                     (cotk_callback_t) onUserStartModule, this);
    snprintf(buf,sizeof(buf),"%%KL%02d%%",i);
    cotk_bind_button(this->console, buf, " KILL ", "",
                     (cotk_callback_t) onUserKillModule, this);
    snprintf(buf,sizeof(buf),"%%VW%02d%%",i);
    cotk_bind_button(this->console, buf, " VIEW ", "",
		     (cotk_callback_t) onUserViewModule,this);
  }
  	
  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_button(this->console, "%START_ALL%", " START ALL ", "Ss",
                   (cotk_callback_t) onUserStartAll, this);
  cotk_bind_button(this->console, "%STOP_ALL%", " STOP ALL ", "Tt",
                   (cotk_callback_t) onUserStopAll, this);
  cotk_bind_button(this->console, "%KILL_ALL%", " KILL ALL ", "Kk",
                   (cotk_callback_t) onUserKillAll, this);
  cotk_bind_button(this->console, "%LOG_ALL%", " LOG ALL ", "Ll",
                   (cotk_callback_t) onUserLogAll, this);
  cotk_bind_button(this->console, "%VIEW_LOGGERS%", " VIEW LOGGERS ", "Oo",
                   (cotk_callback_t) onUserViewLoggers, this);
  cotk_bind_button(this->console, "%_LOG_LADARS_%", "[ LOG LADARS ]", "Aa",
		   (cotk_callback_t) onUserLogLadars, this);
  
  // TODO
  //snprintf(filename, sizeof(filename), "%s/startup.msg",
  //         this->options.log_path_arg);

  // Initialize the display
  if (cotk_open(this->console, NULL) != 0)
    return -1;

  // Create colors

  cotk_set_color_pair(this->console, 7, COLOR_RED, -1);
  cotk_set_color_pair(this->console, 1, -1, COLOR_YELLOW);  
  cotk_set_color_pair(this->console, 2, -1, COLOR_CYAN);  
  cotk_set_color_pair(this->console, 3, -1, COLOR_MAGENTA); 

  // Display some fixed values
  
  char name[128];
  char status[128];
  char namePlusSpace[128];

  for(i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];    
    snprintf(name,sizeof(name),"%%_desc_%02d%%",i);
    snprintf(namePlusSpace, sizeof(namePlusSpace),"%s                       ",proc->name);
    snprintf(status, sizeof(status), "%%_status_%02d%%",i);
    

    cotk_printf(this->console, name, COLOR_PAIR(i%3+1), namePlusSpace);
    cotk_printf(this->console, status, COLOR_PAIR(i%3+1), "                       ");
  }

  //cotk_printf(this->console, "%log_name%", A_NORMAL, this->logName);

  return 0;
}


// Finalize console display
int ProcessControl::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Gets the index associated with a given token i.e. %START12% returns 12
int ProcessControl::getTokenIndex(const char *token)
{  
  char tid[8];
  int j = 0;
  
  for(int i = 0; i < (int) strlen(token); i++)
  {
    if(isdigit(token[i]))
    {
      while(isdigit(token[i]))
        tid[j++] = token[i++];
      tid[j++] = 0;
      break;         
    }
  }

  return atoi(tid);
}


// Handle button callbacks
int ProcessControl::onUserQuit(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user quit");
  quit = 1;
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStartAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user start all");
  
  // re-read the config file incase we have made
  // any changes while process-control was running

  if(self->readConfigFiles()!=0)
    return -1;
  
  // If we are starting everything, make sure to remove mission.log, as we are starting over.
  if(!self->cont) // If we are not in "continue" mode
  {
    char cmd[512]; 
    if(self->configuration!=NULL && strcmp(self->configuration,"field")==0)
      snprintf(cmd, sizeof(cmd), "rm %s/i486-gentoo-linux/mission.log",self->binPrefix);
    else
      snprintf(cmd, sizeof(cmd), "rm %s/../src/system-tests/mission.log",self->binPrefix);
    system(cmd);
  }
  //cout<< "numProcs=" << self->numProcs << endl;
  // When starting all processes, make sure that the startProcess 
  // function knows that all processes are being restarted
  self->startingAll=true;
  for(int i = 0; i < self->numProcs; i++)
  {
    self->startProcess(i);
  }
  self->runLoggers();
  self->startingAll=false;
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStopAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user stop all");
  for(int i = 0; i < self->numSNProcs; i++)
  {
    self->sendQuitRequest(i);
  }
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserKillAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user kill all");
  //system("killall screen");
  self->killingAll=true;
  for(int i = 0; i < self->numProcs; i++)
  {
    self->killProcess(i);
  }
  self->killLoggers();
  self->killingAll=false;
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserLogAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user log all");
  for(int i = 0; i < self->numSNProcs; i++)
  {
    self->sendLogRequest(i);
  }
  return 0;
}

/*
// Handle button callbacks
int ProcessControl::onUserViewVisualizers(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user view visualizers");
  self->viewVisualizers();
  return 0;
}*/

// Handle button callbacks
int ProcessControl::onUserViewLoggers(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user view loggers");
  self->viewLoggers();
  return 0;
}


int ProcessControl::onUserLogLadars(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user log ladars");
  for(int i=0; i< self->numSNProcs; i++)
  {
    if(strstr(self->procs[i].name,"adar")!=NULL)
      self->sendLogRequest(i);
  }
  return 0;
}  

// Handle button callbacks
int ProcessControl::onUserLogModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);  
  MSG("user log module-id: %s", self->procs[tid].name);
  self->sendLogRequest(tid);  
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStartModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user start module-id: %s", self->procs[tid].name);
  self->startProcess(tid);
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStopModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user stop module-id: %s", self->procs[tid].name);
  self->sendQuitRequest(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserKillModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user kill module-id: %s", self->procs[tid].name);
  self->killProcess(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserViewModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user view module-id: %s", self->procs[tid].name);
  self->viewProcess(tid);
  return 0;
}

// Start process in a detached screen
int ProcessControl::startProcess(int index)
{
  // Before we do anything, let us reload the configuration file to 
  // reflect any changes that might have been made while process control was running
  if(!this->startingAll) //As long as we are not starting all processes
  {
    if(readConfigFiles()!=0)
      return -1;
  }
  
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.quit = false;
  
  // the process has now been started
  proc->started = true;

  // the process has now definitely not been killAlled
  proc->killAlled=false;

  // Check if the window is already open
  char windowTest[256];
  snprintf(windowTest, sizeof(windowTest), "%s/../etc/startup/./findScreen.sh %s", this->binPrefix, proc->name);
  proc->windowOpen=(system(windowTest)==0);
       
  if(!proc->windowOpen)
  {
    char cmd[1024];
    char *buf;
    // if you are running a process not on this machine
    if(index<numSNProcs && strcmp(proc->host,"localhost")!=0)
    {
      char sshTest[256];
      buf = "screen -d -m -S %s \"\"ssh -X -t %s \"cd %s/i486-gentoo-linux; ./%s\"\"; echo press enter to close; read\"";
      //buf = "screen -d -m -S %s \"\"ssh -t %s \"%s/./Drun %s\"\"; echo press enter to close; read\"";
      //snprintf(sshTest,sizeof(sshTest), "%s/../../etc/startup/./sshTest.sh %s",this->binPrefix, proc->host);
      snprintf(sshTest,sizeof(sshTest), "%s/../etc/startup/./sshTest.sh %s",this->binPrefix, proc->host);

      // check to see if it is possible to ssh
      if(system(sshTest)==0)
      {
	snprintf(cmd, sizeof(cmd), buf, proc->name,
		 proc->host, this->binPrefix, proc->cmd);
	MSG("running: %s", cmd);
	system(cmd);
	proc->windowOpen=true;
	updateWindowStatus(index, true);
	if(proc->autoOpen)
	  viewProcess(index);
      }
      else
      {
	ERROR("cannot ssh to: %s", proc->host);
	updateWindowStatus(index, false);
      }
    }
    else
    {
      buf="screen -d -m -S %s \"\"%s/./Drun %s\"; echo press enter to close; read\"";

      snprintf(cmd, sizeof(cmd), buf, proc->name, this->binPrefix, proc->cmd);
      system(cmd);
      MSG("running: %s",cmd);

      proc->windowOpen=true;
      updateWindowStatus(index, true);
      if(proc->autoOpen)
	viewProcess(index);
    }
  }
  else
    MSG("%s already open", proc->name);
  
  return 0;
}


// Send the quit request 
int ProcessControl::sendQuitRequest(int index)
{
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.moduleId = proc->id;
  proc->request.quit = true;

  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, proc->id, SNprocessRequest,
                    0, sizeof(proc->request), &proc->request) != 0)
    return MSG("unable to send quit request");

  return 0;
}


// kills given process
int ProcessControl::killProcess(int index)
{
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.quit = false;
  
  if(proc->windowOpen || proc->running || proc->started)
  {
    if(killingAll)
      proc->killAlled=true;
    MSG("killing: %s", proc->name);
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "%s/../etc/startup/killScreen.sh %s", binPrefix, proc->name);
    system(cmd);
    proc->running=false;
    proc->windowOpen=false;
    proc->started=false;
    updateWindowStatus(index, false);
  }
  else
    ERROR("%s not open, cannot kill", proc->name);

  return 0;
}

// Toggle the logging state
int ProcessControl::sendLogRequest(int index)
{
  ProcessData *proc;
  
  proc = &this->procs[index];  
  proc->request.moduleId = proc->id;
  proc->request.enableLog = !proc->request.enableLog;

  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, proc->id, SNprocessRequest,
                    0, sizeof(proc->request), &proc->request) != 0)
    return MSG("unable to send log request");

  return 0;
}


// View Process that was in a detached screen
int ProcessControl::viewProcess(int index)
{
  ProcessData *proc;
  
  proc = &this->procs[index];
  proc->request.quit = false;

  if(!proc->windowOpen)
    ERROR("%s window not open, cannot view", proc->name);
  else
  {  
    char cmd[1024];
    char *buf;

    // Set the window size before you open the process
    //TODO: Standardize Window Size (80x24 would be reaaaly nice)
    char *windowSize;
    buf="xterm%s -rightbar -sb -T %s -e screen -r %s &";
    switch(proc->id)
    {
    case MODtrafficplanner: windowSize=" -geometry 80x26"; break;
    case MODmissionplanner: windowSize=" -geometry 80x27"; break;  
    case SNtrajfollower: windowSize=" -geometry 80x34"; break;
    default: windowSize="";
    }
  
    snprintf(cmd, sizeof(cmd),buf, windowSize, proc->name, proc->name);
  
    MSG("viewing: %s",cmd);
    system(cmd);
  }
  
  return 0;
}

/*
int ProcessControl::runVisualizers()
{
  // declare visualizers
  this->numVisualizers=2; 
  this->visualizers[0]="planviewer2";
  this->visualizers[1]="testMapper";
  
  char cmd[1024];
  char *buf="screen -d -m -S %s \"\"%s/./../Drun planviewer2\"; echo press enter to close; read\"";
  snprintf(cmd, sizeof(cmd), buf, "planviewer2", this->binPrefix);
  system(cmd);
  
  buf = "screen -d -m -S %s \"\"%s/./../Drun testMapper %s 2\"; echo press enter to close; read;\"";
  snprintf(cmd, sizeof(cmd), buf, "testMapper", this->binPrefix, this->rndf);
  
  system(cmd);

  buf = "screen -d -m -S %s \"\"%s/./../Drun mapviewer --recv-subgroup=-2 %s\"; echo press enter to close; read\"";
  if(this->costmap)
    snprintf(cmd, sizeof(cmd), buf, "mapviewer", this->binPrefix, "--show-costmap");
  else
    snprintf(cmd, sizeof(cmd), buf, "mapviewer", this->binPrefix, "");

  system(cmd);
  return 0;
}

int ProcessControl::viewVisualizers()
{
  int i;
  char cmd[1024];
  char *buf ="xterm -rightbar -sb -T %s -e screen -r %s &";
  for(i = 0; i < this->numVisualizers; i++)
  {
    snprintf(cmd, sizeof(cmd), buf, this->visualizers[i], this->visualizers[i]);
    system(cmd);
  }
  return 0;
}
*/

int ProcessControl::runLoggers()
{  
  // declare loggers
  this->loggers[0] = "statelogger";
  this->numLoggers = 1;
  if(strcmp(this->configuration,"field")==0)
  {
    this->loggers[1] = "skynet-logger";
    this->numLoggers = 2;
  }

  /*
  // setup the logfile name
  time_t temp;
  struct tm *timeptr;
  
  
  temp = time(NULL);
  timeptr = localtime(&temp);
  
  char timestamp[1024];

  strftime(timestamp,sizeof(timestamp),"%Y-%m-%d-%a-%H-%M", timeptr);
  */
  
  char logname[1024];

  snprintf(logname, sizeof(logname), "%s/systemtest-%s-%s.log", this->logpath, this->timestamp, this->scenario);

  char cmd[1024];
  char *buf = "screen -d -m -S %s \"\"%s/./Drun statelogger --file=%s\"; echo press enter to close; read\"";
  snprintf(cmd, sizeof(cmd), buf, "statelogger", this->binPrefix, logname);

  system(cmd);

  if(strcmp(this->configuration,"field")==0)
  {
    snprintf(logname, sizeof(logname), "%s/skynetlogger-%s-%s.log", this->logpath, this->scenario, this->timestamp);
    buf = "screen -d -m -S %s \"\"%s/./Drun skynet-logger %s\"; echo press enter to close; read\"";
    snprintf(cmd, sizeof(cmd), buf, "skynet-logger", this->binPrefix, logname);
    system(cmd);
  }
  
  return 0;
}

int ProcessControl::viewLoggers()
{
  int i;
  char cmd[1024];
  char *buf ="xterm -rightbar -sb -T %s -e screen -r %s &";
  MSG("numLoggers= %d", numLoggers);
  for(i = 0; i < this->numLoggers; i++)
  {
    snprintf(cmd, sizeof(cmd), buf, this->loggers[i], this->loggers[i]);
    system(cmd);
  }
  return 0;
}

int ProcessControl::killLoggers()
{
  char cmd[256];
  char *buf = "%s/../etc/startup/killScreen.sh %s";
  for(int i = 0; i< this->numLoggers; i++)
  {
    snprintf(cmd, sizeof(cmd), buf, this->binPrefix, this->loggers[i]);
    system(cmd);
  }
  return 0;
}


// Check for changes to the process state
int ProcessControl::updateProcessStatus()
{
  int i;
  int status;
  int blobId;
  ProcessData *proc;
  ProcessResponse response;
  char token[128];

  // Don't run too fast
  usleep(100000);
  
  // Wait for new updates (with timeout)
  status = sensnet_wait(sensnet, 100);
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));

  // Update all entries to show time since last heartbeat
  for (i = 0; i < this->numSNProcs; i++)
  {
    proc = &this->procs[i];

    memset(&response, 0, sizeof(response));
    if (proc->blobType == SNprocessResponse)
    {
      // Read process response message
      if (sensnet_read(sensnet, proc->id, SNprocessResponse,
                       &blobId, sizeof(response), &response) != 0)
        return -1;
    }
    else
    {
      // For non-process messages, take a peek at the timestamp
      uint64_t time_stamp;
      if (sensnet_peek_ex(sensnet, SENSNET_SKYNET_SENSOR, proc->blobType,
                          &blobId, NULL, &time_stamp) != 0)
        return -1;
      response.timestamp = time_stamp;
    }

    // if blobId is < 0, process hasn't 
    // started yet, don't print anything out
    if (blobId < 0)
      continue;
    

    // update health status of process
    //proc->healthStatus = response.healthStatus;
    
    if (this->console)
    {
      double time, lat;
      char size[64];

      // Latency (i.e., how long since we last saw a response).
      // We have to be careful with the unsigned type conversion.
      lat = ((double) DGCgettime() - (double) response.timestamp) / 1e6;

      // Process timestamp
      time = fmod((double) response.timestamp / 1e6, 1000);
      
      // Log size in human-readable format
      if (response.logSize < 1024)
        //snprintf(size, sizeof(size), "%4dKB", response.logSize);
	snprintf(size, sizeof(size), "      ");
      else
        snprintf(size, sizeof(size), "%4dMB", response.logSize / 1024);
      
      
      // get the correct token to change for this index
      snprintf(token, sizeof(token), "%%_status_%02d%%", i);
      

      // if latency is less than 1, everything is "ok", 
      // if not, we will consider the process dead
      if(lat < 1.0)
      {
	// if the process was just started, and now we know 
	// it is running, set running to true and started to false, 
	// so that we can attempt to start it again later if needed
	if(proc->started)
	{
	  //proc->running=true;
	  proc->started=false;
	}
	proc->running=true;
      }
      else
	proc->running=false;

      
      // Now it's time to paint the screen, also try and autorestart if neccessary
      if (proc->running)
      	cotk_printf(this->console, token, COLOR_PAIR(i%3+1), "%s     %s        ", size, "GOOD");
      else
      {
	cotk_printf(this->console, token, COLOR_PAIR(7), "%s     %s (%3.0f)", size, " BAD", lat);
	
	//MSG("%s failing", proc->name);

	// If process is not running, has not yet been started, 
	// and auto restart is on, restart the process
	// Don't auto restart if you killed everything at once
	if(!proc->started && autoRestart && !proc->killAlled)
	{
	  MSG("automatically restarting %s",proc->name);
	  killProcess(i);
	  startProcess(i);
       	}
      }
      


      /*if (lat < 1.0)
        cotk_printf(this->console, token, A_NORMAL, "%8.3f %s      ", time, size);
      else
      cotk_printf(this->console, token, COLOR_PAIR(1), "%8.3f %s (%3.0f)", time, size, lat);*/
    }
  }
  
  return 0;
}

void ProcessControl::updateWindowStatus(int index, bool open)
{
  char token[16];
  
  // get the correct token to change for this index
  snprintf(token, sizeof(token), "%%s%02d%%", index);

  if(open)
    cotk_printf(this->console, token, COLOR_PAIR(index%3+1), "           OPEN        ");
  else
    cotk_printf(this->console, token, COLOR_PAIR(index%3+1), "                       ");
}


/*
int ProcessControl::sendProcessHealth()
{
  
  ProcessHealth procHealth;
  int temp=0;
  for(int i=0; i < numProcs; i++)
  {
    // add 1 to temp if process is running
    // and the process is monitored
    if(this->procs[i].monitored)
      temp+=this->procs[i].running;
 
    //MSG("%s, %d",this->procs[i].name, this->procs[i].running);
  }
 
  // if all processes are running, health is good (2)
  // if not, then health is temporarily bad (1)
  if(temp==numMonitored)
  {
    procHealth.health=2;
    cotk_printf(this->console, "%OVHEALTH%", A_NORMAL, "HEALTH: %d", procHealth.health);
  }
  else
  {
    procHealth.health=1;
    cotk_printf(this->console, "%OVHEALTH%", COLOR_PAIR(1), "HEALTH: %d", procHealth.health);
  }
 
  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, MODprocessControl, SNprocessHealth,
                    0, sizeof(procHealth), &procHealth) != 0)
    return MSG("unable to send health status");
  return 0;
}
*/

// Main program thread
int main(int argc, char **argv)
{
  // catch Ctrl-C and SIGTERM, handle them cleanly
  signal(SIGINT, sigintHandler);
  signal(SIGTERM, sigintHandler);
  signal(SIGHUP, sigintHandler);

  ProcessControl *proctl; 

  // Create module
  proctl = new ProcessControl();
  assert(proctl);
  
  // Parse command line options
  if (proctl->parseCmdLine(argc, argv) != 0)
    return -1;
  
  //set permissions for shell scripts
  char cmd[512];
  
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/sshTest.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/killScreen.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/findScreen.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/yn.sh",proctl->binPrefix);
  system(cmd);
  

  if(strcmp(proctl->configuration,"sim")!=0) // if not in sim mode
  {
    // check to see if time is synchronized
    MSG("should be checking time synchronization here");
    if(false) // if time is not synchronized
    {
      MSG("WARNING: time is not synchronized on other machines. Continue? (y or n)");

      snprintf(cmd, sizeof(cmd), "%s/../etc/startup/./yn.sh", proctl->binPrefix);

      // return whether user selected yes or not
      if(!system(cmd)==0) // if user selected no
      {
	cmdline_parser_free(&proctl->options);  
	delete proctl;
	MSG("program exited cleanly");
	exit(0);
      }
    }
  }

  if (proctl->initSensnet() != 0)
    return -1;
    
  // Initialize console
  if (!proctl->options.disable_console_flag)
  {
    if (proctl->initConsole() != 0)
      return -1;
  }

  // start all processes if it was requested
  if(proctl->startall)
    ProcessControl::onUserStartAll(proctl->console, proctl, "");
  else if(!proctl->cont) // if we are not in "continue" mode
  {
    // Even if we don't start everything, still remove 
    // the mission.log for mplanner in case we start it later
    if(proctl->configuration!=NULL && strcmp(proctl->configuration,"field")==0)
      snprintf(cmd, sizeof(cmd), "rm %s/i486-gentoo-linux/mission.log",proctl->binPrefix);
    else
      snprintf(cmd, sizeof(cmd), "rm %s/../src/system-tests/mission.log",proctl->binPrefix);
    system(cmd);
  }

  // Run mappers
  //proctl->runVisualizers();
  
  // Run loggers
  //proctl->runLoggers();

  while (quit==0)
  {
    if (proctl->updateProcessStatus() != 0)
      break;
    // if(proctl->sendProcessHealth() != 0)
    //break;
    if (proctl->console)
      cotk_update(proctl->console);
  }

  // Clean up
  ProcessControl::onUserKillAll(proctl->console, proctl, "");
  proctl->finiConsole();
  proctl->finiSensnet();

  cmdline_parser_free(&proctl->options);  
  delete proctl;
  MSG("program exited cleanly");
  
  return 0;
}

void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit >= 2) {
    abort();
  }
  //system("killall screen");
  quit++;
}
