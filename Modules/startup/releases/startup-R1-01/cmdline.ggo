# Command-line options (use gengetopt to generate C the source code)

package "process-control"
version "$Revision$"

purpose 
"This utility will start/stop processes on remote machines.

The processes are described in a single configuration file, and can be
started or stopped through an ncurses interfaces.  See, for example,
the test configuration PROCESS_TEST.CFG, which can be run with:

  $ ./process-control $DGC_CONFIG_PATH/startup/PROCESS_TEST.CFG

This will allow the user to control a set of dummy processes running
on the local host.

How it works: for each process, process-control creates a detached
screen, then uses ssh to execute the remote process.  Once started,
processes are controlled via the ProcessRequest message, and monitored
via the ProcessResponse message.  Currently, only SensNet enabled
applications support these message types.

Note that ssh *must* be configured for password-less access in order
to run processes on remote machines.

Screen cheat sheet: To see the set of screens created by
process-control, use:

  $ screen -ls

To re-attach one of these screens (e.g., to take a closer look at
a process), use:

  $ screen -rd <NUMBER>

To detach the screen (without killing the process), use the
key-combination <ctrl-a> <d>.  Type:

  $ info screen

for more information on the screen utility.
"


section "Basic options"

option "spread-daemon" -
  "Spread daemon" string no

option "skynet-key" -
  "Skynet key" int default="0" no

option "bin-prefix" -
  "Prefix for executables (defaults to $PWD)" string no

option "disable-console" -
  "Disable console display" flag off

option "startall" -
	"starts all modules automatically (default is on)" flag on

option "no-restart" -
	"If modules go down, do not restart them automatically" flag off

option "costmap" -
	"shows costmap" flag off

option "tsfile" -
	"tsfile for trafsim (duh)" string no

option "logpath" -
	"logpath for tplanner" string no

option "timestamp" -
	"time stamp for all log files" string no


section "possible configurations"

option "field" -
	"Configuration when running on alice, in the field (field.CFG)" flag off
option "alicesim" -
	"Configuration when running everything on alice, but still in simulation (alicesim.CFG)" flag off
option "sim" -
	"Configuration when running in simulation (sim.CFG)" flag off


section "configuration files"

option "cfg" -
  "cfg file (at this time is not being used, use configuration instead" string no

option "route" -
	"route config file" string no

option "otherProcs" -
	"other procedures config file" default="default.CFG" string no


section "Config file options"

option "module-id" -
  "Module id of process" string no multiple

option "hostname" -
  "User name and hostname (or IP) of the process host e.g. dtrotz@amy" string no multiple

option "cmd-line" -
  "Command to issue to begin module's process on host" string no multiple

option "monitored" -
  "Whether or not process should be monitored by process-control when considering overall health, 1 is yes, 0 is no" int no multiple



section "Route file options"

option "rndf" -
  "rndf" string no

option "mdf" - 
  "mdf" string no

option "start" -
  "start location for simulation" string no
  
option "scenario" -
	"scenario name" string no
	
option "sceneFunc" -
	"scene function" string no

option "obsFile" -
	"obs file" string no
