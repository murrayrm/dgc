

Release R1-01 (Wed Aug 15 22:06:40 2007):
	Another attempt at reducing the amount of time spent in the field adjusting the field.CFG file for different runs. I realized that having separate files for different scenarios is unmaintainable... 

The new setup:
fieldMaster.CFG - this file has all the options, for any module that we should ever run live on Alice. If it's missing yours, let me (Laura) know, and I'll add it. Before committing a changed version of this file, talk to Laura for changes to the sensing modules, or Noel for changes to the planning stack. 
note: the separate entries in this file must be separated by a blank line, and the '#' character is a comment (conventions necessary for the parser to work)

moduleList.CFG - a list of the modules you want to run, each on their own line. Again, '#' indicates a comment. This makes it possible to edit the list of modules and see what's running without haveing to scroll through a long file

createConfig.py - python script that reads in moduleList.CFG and fieldMaster.CFG and outputs the corresponding field.CFG file in the format process-control wants. Note that every time you change one of the other two files you'll need to run "python createConfig.py" in src/startup - even with this extra step, the new setup proved significantly faster to use in the field. 

Release R1-00z (Wed Aug 15 20:21:51 2007):
	Removed a bunch of useless command line options, cleaned up the 
code. Made auto restart a default option. I just noticed that mapper and 
planner seem to stop sending messages for a couple seconds at 
intersections, so if process-control tries to automatically restart 
them, probelms might arise. To not auto restart, type the following:

python stlukeSmallStandard.py no-restart

If people hate the idea of automatically restarting processes that don't 
communicate for a certain amount of time, I can change it back to not 
be default. 

Process-control now also re-reads the CFG files when you go to start a 
process again. So, if you want to change a command line option for a 
process, change the CFG file, kill the process in process-control, and 
start it again, and the change should be reflected.

I also now have system-tests and startup both using the same timestamp 
for log files, so analyzer doesn't cry when it is expecting a log file 
that is 1 minute off.

Release R1-00y (Tue Aug 14 23:12:17 2007):
	* field_all_sensors.CFG that was used in the 8/14 testing. 
	* Removed testMapper from default.CFG. 
	* Added --update-from-map flag to planner.
	* Added field_line_perceptor.CFG

Release R1-00x (Mon Aug 13  1:07:32 2007):
	adding config files for common sensing scenarios.

	I've noticed that a large amount of testing time seems 
to be spent changing around the sensing config files - I believe 
that Robbie is working on a better solution for this, but for now, 
this release should help. I've set it up such that field.CFG is 
simply a symbolic link to a specific scenario's config file 
(ymk all resets the symbolic link to field_all_sensors.CFG, 
which *should* be our default testing setup by now). 
Changing the sensor configuration that we run with only requires 
changing the link, rather than editing the long config file.  

I'm less familiar with the configurations for stereo obstacle 
and line perception - if there are different subsets of those 
perceptors that would be useful to have, we can add more .CFG files.

Release R1-00w (Tue Aug  7  1:42:16 2007):
	Changed commandline options for some sensing module in field.CFG.

Release R1-00v (Mon Aug  6 22:19:40 2007):
	adding correct command line options to ladar-car-perceptor for
	field test tomorrow (turning off debug output, enabling console and
	adding depth to obstacles)  
	
	I also enabled the radar - I see no reason not to run it tomorrow. 
	All the cameras are still commented out - I will leave it to
	Jeremy/Mohamed/Daniele to determine what combination is best there

Release R1-00u (Mon Aug  6 13:47:40 2007):
	Added a flag in sim.CFG and alicesim.CFG for follower

Release R1-00t (Sat Aug  4  1:18:57 2007):
	Added --disable-status-check flag to gcdrive and --noprediction to
	planner

Release R1-00s (Fri Aug  3  1:27:24 2007):
	Fixed the command for running skynet-logger and state-logger.
	Updated field.CFG

Release R1-00r (Tue Jul 31 18:08:57 2007):
	Changed field.CFG to have correct gcdrive cmd-line arguments, now 
running all programs that run on localhost from Drun without ssh-ing to 
localhost, also everything can be shut down when you press 'c' in 
system-tests, ad this is now handled correctly.

Release R1-00q (Tue Jul 31  2:56:17 2007):
adding config files for the sensing team.
	sensingFusionAlice has all the feeders/perceptors (except for the PTU), and the mapper -> tested this morning on alice
	sensingFusionReplay has a sample sensnet-replay entry, as well as all feeders and the mapper

Release R1-00p (Sat Jul 28  1:43:45 2007):
	Changed field.CFG to use correct parameters for a couple of the 
sensing modules. We just tested everything except gcdrive on alice, and 
everything seems to work fine!

Release R1-00o (Fri Jul 27 17:06:37 2007):
	Updated the config files so that there are several different 
configurations that you can choose from, all of them commented out 
except the default one (Noel's idea)
	
	This is hopefully the last release of the day... =(

Release R1-00n (Fri Jul 27 16:09:41 2007):
	Moved the shell scripts to the etc folder, so now startup should 
still work even if it is just a link module

Release R1-00m (Fri Jul 27  6:08:54 2007):
	forgot to add shell scripts an the other procedures config file, 
so added those, commiting again

Release R1-00l (Fri Jul 27  5:24:34 2007):
	Tried to make process-control a little more user 
friendly. Changed process-control to parse cmd-line options in the CFG 
files much better, so that when anything needs to be added, all that 
needs to be changed is the CFG file, nothing else. Putting --rndf 
is enough to get the actual rndf added, no need to put --rndf=%s. Added 
an other processes config file so that you can choose options on 
processes such as mapviewer. Added a shell script to test if you can ssh 
somewhere before process-control actually tries to run the program 
there. Added a kill capability for each individual process. 

	Process control also now communicates with HealthMonitor as to 
the overall health of processes. In the CFG files, there is a 4th option 
called monitored, if this is set to 1, process-control will consider it 
when it is looking at the overall health of the system. For example, we 
really want process-control to make sure that if gcdrive dies, we 
immediately change the overall health of the system to BAD, whereas if 
mapviewer dies, that's not integral for the overall system health. If 
all of the monitored processes are good, then process-control sends out 
over sensnet a SNprocessHealth message of GOOD, but if any of the 
monitored processes are down, then the message is BAD. Also added an 
auto restart option so that processes will automatically be restarted if 
they are detected to have gone down.

Release R1-00k (Fri Jul 20  3:56:24 2007):
	Do not listen to SNSegGoals for mission planner becasue
	it now sends heartbeat messages (SNprocessResponse) to 
	process-control so it's not a special case anymore.

Release R1-00j (Fri Jul 20  0:14:27 2007):
	Modified the field.CFG file to reflect the correct machines for 
the stereofeeders and radarfeeder to run from. They were originally run 
from zoidberg but since it recently died, I've switched some cables 
around to make use of the new firewire ports on leela and amy.

Release R1-00i (Thu Jul 19 16:46:05 2007):
	Fixed the field.CFG file to work properly (there was a typo) and 
added radar sensors to list of modules to start. Also changed some 
funciton names in ProcessControl.cc
	
Release R1-00h (Wed Jul 18 13:14:47 2007):
	Can now view crashed processes, also added mapping and logging capabilities that used to be done in system-tests

Release R1-00g (Thu Jul 12 20:04:36 2007):
	Updated process-control to be called from system-tests module. It uses a config file to call planner modules based on 
what kind of test you want to do. Can also take command line arguments.

Release R1-00f (Wed Jun 13 18:12:05 2007):
  Tweaks for the site visit.

Release R1-00e (Sat Jun  2 14:22:23 2007):
  Added the config file used for GraphPlanner (static obstacle) testing.

Release R1-00d (Sun May  6 14:03:04 2007):

  Some minor performance tweaks.

Release R1-00c (Fri Apr 27 22:45:53 2007):
  
  Added RadarFeeder to the process list and tweaked the path handling.

Release R1-00b (Sun Apr 15 22:38:35 2007):

The process-control utility will start/stop processes on remote machines.
The processes are described in a single configuration file, and can be
started or stopped through an ncurses interfaces.  See, for example,
the test configuration PROCESS_TEST.CFG, which can be run with:

  $ ./process-control $DGC_CONFIG_PATH/startup/PROCESS_TEST.CFG

This will allow the user to control a set of dummy processes running
on the local host.

Release R1-00a (Tue Apr 10 21:21:28 2007):
	This release has a copy of the startup scripts from dgc/trunk.  It
	is intended to be used as a starting point for YaM-compatible
	startup scripts.

Release R1-00 (Sat Mar  3  8:30:37 2007):
	Created.






























