# Command-line options (use gengetopt to generate C the source code)

package "process-control"
version "$Revision$"

purpose 
"This utility will start/stop processes on remote machines.

The processes are described in a single configuration file, and can be
started or stopped through an ncurses interfaces.  See, for example,
the test configuration PROCESS_TEST.CFG, which can be run with:

  $ ./process-control $DGC_CONFIG_PATH/startup/PROCESS_TEST.CFG

This will allow the user to control a set of dummy processes running
on the local host.

How it works: for each process, process-control creates a detached
screen, then uses ssh to execute the remote process.  Once started,
processes are controlled via the ProcessRequest message, and monitored
via the ProcessResponse message.  Currently, only SensNet enabled
applications support these message types.

Note that ssh *must* be configured for password-less access in order
to run processes on remote machines.

Screen cheat sheet: To see the set of screens created by
process-control, use:

  $ screen -ls

To re-attach one of these screens (e.g., to take a closer look at
a process), use:

  $ screen -rd <NUMBER>

To detach the screen (without killing the process), use the
key-combination <ctrl-a> <d>.  Type:

  $ info screen

for more information on the screen utility.
"


section "Basic options"

option "spread-daemon" -
  "Spread daemon" string no

option "skynet-key" -
  "Skynet key" int default="0" no

option "bin-prefix" -
  "Prefix for executables (defaults to $PWD)" string default="" no

option "disable-console" -
  "Disable console display" flag off


section "Process control options"

option "module-id" -
  "Module id of process" string no multiple

option "hostname" -
  "User name and hostname (or IP) of the process host e.g. dtrotz@amy" string no multiple

option "cmd-line" -
  "Command to issue to begin module's process on host" string no multiple

option "rndf" -
  "rndf" string no

option "mdf" - 
  "mdf" string no

option "cfg" -
  "cfg file" string default="PROCESS_CONTROL.CFG" no

option "start" -
  "start location for simulation" string no
  
option "logpath" -
	"logpath for tplanner" string no

option "s1plogname" -
	"s1plannerlogname" string no

option "startall" -
	"starts all modules automatically" flag off
