/* cmdline.h */

/* File autogenerated by gengetopt version 2.18  */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
#define CMDLINE_PARSER_PACKAGE "process-control"
#endif

#ifndef CMDLINE_PARSER_VERSION
#define CMDLINE_PARSER_VERSION "$Revision: 19743 $"
#endif

struct gengetopt_args_info
{
  const char *help_help; /* Print help and exit help description.  */
  const char *version_help; /* Print version and exit help description.  */
  char * spread_daemon_arg;	/* Spread daemon.  */
  char * spread_daemon_orig;	/* Spread daemon original value given at command line.  */
  const char *spread_daemon_help; /* Spread daemon help description.  */
  int skynet_key_arg;	/* Skynet key (default='0').  */
  char * skynet_key_orig;	/* Skynet key original value given at command line.  */
  const char *skynet_key_help; /* Skynet key help description.  */
  char * bin_prefix_arg;	/* Prefix for executables (defaults to $PWD) (default='').  */
  char * bin_prefix_orig;	/* Prefix for executables (defaults to $PWD) original value given at command line.  */
  const char *bin_prefix_help; /* Prefix for executables (defaults to $PWD) help description.  */
  int disable_console_flag;	/* Disable console display (default=off).  */
  const char *disable_console_help; /* Disable console display help description.  */
  char ** module_id_arg;	/* Module id of process.  */
  char ** module_id_orig;	/* Module id of process original value given at command line.  */
  int module_id_min; /* Module id of process's minimum occurreces */
  int module_id_max; /* Module id of process's maximum occurreces */
  const char *module_id_help; /* Module id of process help description.  */
  char ** hostname_arg;	/* User name and hostname (or IP) of the process host e.g. dtrotz@amy.  */
  char ** hostname_orig;	/* User name and hostname (or IP) of the process host e.g. dtrotz@amy original value given at command line.  */
  int hostname_min; /* User name and hostname (or IP) of the process host e.g. dtrotz@amy's minimum occurreces */
  int hostname_max; /* User name and hostname (or IP) of the process host e.g. dtrotz@amy's maximum occurreces */
  const char *hostname_help; /* User name and hostname (or IP) of the process host e.g. dtrotz@amy help description.  */
  char ** cmd_line_arg;	/* Command to issue to begin module's process on host.  */
  char ** cmd_line_orig;	/* Command to issue to begin module's process on host original value given at command line.  */
  int cmd_line_min; /* Command to issue to begin module's process on host's minimum occurreces */
  int cmd_line_max; /* Command to issue to begin module's process on host's maximum occurreces */
  const char *cmd_line_help; /* Command to issue to begin module's process on host help description.  */
  
  int help_given ;	/* Whether help was given.  */
  int version_given ;	/* Whether version was given.  */
  int spread_daemon_given ;	/* Whether spread-daemon was given.  */
  int skynet_key_given ;	/* Whether skynet-key was given.  */
  int bin_prefix_given ;	/* Whether bin-prefix was given.  */
  int disable_console_given ;	/* Whether disable-console was given.  */
  unsigned int module_id_given ;	/* Whether module-id was given.  */
  unsigned int hostname_given ;	/* Whether hostname was given.  */
  unsigned int cmd_line_given ;	/* Whether cmd-line was given.  */

  char **inputs ; /* unamed options */
  unsigned inputs_num ; /* unamed options number */
} ;

extern const char *gengetopt_args_info_purpose;
extern const char *gengetopt_args_info_usage;
extern const char *gengetopt_args_info_help[];

int cmdline_parser (int argc, char * const *argv,
  struct gengetopt_args_info *args_info);
int cmdline_parser2 (int argc, char * const *argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

void cmdline_parser_print_help(void);
void cmdline_parser_print_version(void);

void cmdline_parser_init (struct gengetopt_args_info *args_info);
void cmdline_parser_free (struct gengetopt_args_info *args_info);

int cmdline_parser_configfile (char * const filename,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);

int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
