# This program kills a screen with the specified name

kill `screen -ls | grep $1 | sed "s/\.$1//" | awk '{print $1}'`