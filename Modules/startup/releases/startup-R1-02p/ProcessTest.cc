
/* 
 * Process Fake a process (for testing process control).
 * Date: 15 April 2007
 * Author: Andrew H
 * CVS: $Id$
*/

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dgcutils/DGCutils.hh>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/sn_types.h>
#include <interfaces/ProcessState.h>


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)



// Main program thread
int main(int argc, char **argv)
{
  char *spreadDaemon;
  int skynetKey;
  int moduleId;
  sensnet_t *sensnet;
  int logSize;
  ProcessRequest request;
  ProcessResponse response;

  if (argc < 2)
  {
    fprintf(stderr, "usage: %s <MODULENAME>\n", argv[0]);
    return -1;
  }
  
  spreadDaemon = "4803";
  skynetKey = 0;
  
  if (getenv("SPREAD_DAEMON"))
    spreadDaemon = getenv("SPREAD_DAEMON");
  if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));

  assert(argc > 1);
  moduleId = modulenamefromString(argv[1]);
  
  sensnet = sensnet_alloc();

  // Connect to sensnet
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey, moduleId) != 0)
    return ERROR("unable to connect to sensnet");
      
  // Subscribe to the process request group
  if (sensnet_join(sensnet, moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    return ERROR("unable to join process request group");

  logSize = 0;
  memset(&request, 0, sizeof(request));
  memset(&response, 0, sizeof(response));  

  MSG("starting");
  
  // Run
  while (true)
  {
    MSG("running");

    // Wait a while
    sensnet_wait(sensnet, 500);

    // Read request
    sensnet_read(sensnet,
                 moduleId, SNprocessRequest, NULL, sizeof(request), &request);

    MSG("request %d %d", request.quit, request.enableLog);
    
    // Check for quit
    if (request.quit)
      break;

    // Do some fake logging
    if (request.enableLog)
      logSize += 1;

    // Write heartbeat
    response.moduleId = moduleId;
    response.timestamp = DGCgettime();
    response.logSize = logSize;
    sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                  moduleId, SNprocessResponse, 0, sizeof(response), &response);
  }

  MSG("exiting");

  // Clean up
  sensnet_leave(sensnet, SENSNET_SKYNET_SENSOR, SNprocessRequest);
  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  
  return 0;
}
