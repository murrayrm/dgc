#! /usr/bin/env python

listFile = open('moduleList.CFG','r')

outputFile = open('field.CFG', 'w')
hmOutput = open('SENSOR_FEEDER.CFG', 'w')

module = listFile.readline()

hmOutput.write('##############\n')
hmOutput.write('#SENSING APPS#\n')
hmOutput.write('##############\n\n')

str = 'sensor-id='

while module!='#END#\n':
	if (len(module) > 0) & (module[0] != '#') & (module != '\n'):
		#check if module is a feeder. if it is, chris wants it in his output file
		if -1 != module.find('Feeder'):
			hm = str + module
			hmOutput.write(hm)

		#open master config file, look for that module
		searchPattern = 'module-id=' + module
		masterFile = open('fieldMaster.CFG','r')		
		s = masterFile.readline()
		while (s != searchPattern) & (len(s) > 0):
		       	s = masterFile.readline()
		if ''==s: #never found pattern
			errorMSG = "Didn't find module-id: " + module
			print errorMSG
		else: #found pattern
			outputFile.write(s)
		       	while (s!='\n') & (s!=''): #blank line in cfg file means done wih that module
	       			s = masterFile.readline()
       				outputFile.write(s)
       			outputFile.write(s) #and, finish off each module with a blank line
	module = listFile.readline()
