# This program waits until the file loads is created, and then immediately deletes it.

file=$DGC_CONFIG_PATH/startup/loads
tmpFile=$DGC_CONFIG_PATH/startup/tmpFile

# if the file doesn't exist but there is a tmpFile, 
# that means that loadAverage.sh is still running
while [ ! -f $file ] && [ -f $tmpFile ]
do
sleep 1
done

rm $file >/dev/null 2>&1