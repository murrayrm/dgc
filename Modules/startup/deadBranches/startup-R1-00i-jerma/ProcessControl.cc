/* 
 * Process Control Module for starting/stoping/logging modules remotely. 
 * Date: 15 March, 2007
 * Author: David Trotz
 * CVS: $Id$
 *
 * Modified: Robbie Paolini 7/12/07
*/

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>

#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>

#include "cmdline.h"

/// @brief ProcessControl class
class ProcessControl
{
  public:   

  /// Default constructor
  ProcessControl();

  /// Default destructor
  ~ProcessControl();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(char *configFile);

  /// Parse the route file
  int parseRouteFile(char *routeFile);

  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  /// Console button callback
  static int onUserQuit(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStartAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStopAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserKillAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserViewVisualizers(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserViewLoggers(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStartModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserKillModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStopModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserViewModule(cotk_t *console, ProcessControl *self, const char *token);

  // Gets the index associated with a given token i.e. %START12% returns 12
  static int getTokenIndex(const char *token);
  
  // Start the given process.
  int startProcess(int index);

  // Send requests
  int sendQuitRequest(int index);
  int sendLogRequest(int index);
    
  // Update the display with the current process response state
  int updateProcessStatus();
  
  // Kill the current process
  int killProcess(int index);
  
  // View the Given Process
  int viewProcess(int index);

  // Run vizualizer programs
  int runVisualizers();

  // view visualizer programs
  int viewVisualizers();

  // run logging programs
  int runLoggers();
  
  // view logger programs
  int viewLoggers();

  // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // rndf file];
  char *rndf;

  // mdf file
  char *mdf;

  // simulation start string
  char *start;

  // scenario name
  char *scenario;

  // obsFile name
  char *obsFile;

  // sceneFunc
  char *sceneFunc;

  // start all option
  bool startall;

  // costmap option
  bool costmap;
  
  // Run Trafsim option
  bool bRunTrafsim;

  // route file name
  char *route;

  // log path
  char *logpath;
  
  //slplannerlogname
  char *s1plogname;

  // Prefix for executables (e.g., bin/i486-gentoo-linux)
  char binPrefix[1024];
  
  // holds number of visualizer programs
  int numVisualizers;
  
  // list of visualizers to run
  char *visualizers[2];
  
  // holds number of logger programs
  int numLoggers;

  // list of loggers to run;
  char *loggers[2];

  // Should we quit?
  bool quit;
  
  // SensNet handle
  sensnet_t *sensnet;

  // Data for each process
  struct ProcessData
  {
    // Process id 
    int id;

    // Expected blob type
    int blobType;

    // Last blob id
    int blobId;

    // Host
    char *host;

    // Command-line
    char cmd[1024];    
    
    // Current process request
    ProcessRequest request;
  };

  // Process list
  int numProcs;
  ProcessData procs[64];
  
  // Console text display
  cotk_t *console;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
ProcessControl::ProcessControl()
{
  memset(this, 0, sizeof(*this));
  
  return;
}


// Default destructor
ProcessControl::~ProcessControl()
{
  return;
}


// Parse the command line
int ProcessControl::parseCmdLine(int argc, char **argv)
{
  char *configFile;
  char *routeFile;
    

  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Get route file from command line, default is blank.CFG
  routeFile = dgcFindConfigFile(this->options.route_arg, "startup");

  // Get config file from the command line, default is PROCESS_CONTROL.CFG
  configFile = dgcFindConfigFile(this->options.cfg_arg, "startup"); 

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // get the rndf file
  if (this->options.rndf_given)
    this->rndf = this->options.rndf_arg;
  else
    this->rndf="";

  // get the mdf file
  if (this->options.mdf_given)
    this->mdf = this->options.mdf_arg;
  else
    this->mdf="";

  // get the start location for simulation
  if (this->options.start_given)
    this->start = this->options.start_arg;
  else
    this->start="";
  
  // get the observation file
  if(this->options.obsFile_given)
    this->obsFile = this->options.obsFile_arg;
  else 
    this->obsFile = "";
  
  // get the scene function
  if(this->options.sceneFunc_given)
    this->sceneFunc = this->options.sceneFunc_arg;
  else 
    this->sceneFunc = "";

  // get the start all option
  if(this->options.startall_flag)
    this->startall=true;

  // get the costmap option
  if(this->options.costmap_flag)
    this->costmap=true;

  // get the run trafsim option
  if(this->options.bRunTrafsim_flag)
    this->bRunTrafsim = true;

  // get the tplanner log name
  if (this->options.logpath_given)
    this->logpath = this->options.logpath_arg;
  else
    this->logpath = "";

  // get the slplanner log name
  if (this->options.s1plogname_given)
    this->s1plogname = this->options.s1plogname_arg;
  else
    this->s1plogname = "";
  
 
  // load the route file
  if(parseRouteFile(routeFile) != 0)
    return -1;

  // Load the configuration file
  if (parseConfigFile(configFile) != 0)
    return -1;

  // Get the prefix for executables
  if (this->options.bin_prefix_given)
    snprintf(this->binPrefix,sizeof(binPrefix),this->options.bin_prefix_arg);
  else if (getenv("PWD"))
  {
    snprintf(this->binPrefix,sizeof(this->binPrefix),"%s/../../bin/i486-gentoo-linux",getenv("PWD"));
    cout<<this->binPrefix<<endl;
  }
  else
    return ERROR("--bin-prefix must be specified");

  free(configFile);

  return 0;
}

// Parse the config file
int ProcessControl::parseConfigFile(char *configFile)
{
  int i;
  ProcessData *proc;
    
  // Load options from the configuration file
  if (cmdline_parser_configfile(configFile, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", configFile);

  // Loop through the modules in the configuration file and
  // create corresponding entries in the internal process list.
  for (i = 0; i < (int) this->options.module_id_given; i++)
  {
    MSG("reading option");
    // increment number of processes, assign host and module id
    proc = &this->procs[this->numProcs++];
    proc->id = modulenamefromString(this->options.module_id_arg[i]);
    proc->host = this->options.hostname_arg[i];

    // Special cases
    if (proc->id == SNasim)
      proc->blobType = SNstate;
    else if (proc->id == SNastate)
      proc->blobType = SNstate;
    else if (proc->id == SNadrive)
      proc->blobType = SNactuatorstate;
    else if (proc->id == SNtrajfollower)
      // TESTING
      //proc->blobType = SNdrivecmd; 
      proc->blobType = SNadrive_command;
    else if (proc->id == MODmapping)
    	proc->blobType = SNglobalGloNavMapFromGloNavMapLib;
    else if (proc->id == MODmissionplanner)
    	proc->blobType = SNsegGoals;
    else if (proc->id == MODtrafficplanner)
    	proc->blobType = SNocpParams;
    else if (proc->id == MODdynamicplanner)
    	proc->blobType = SNtraj;
    else
      proc->blobType = SNprocessResponse;
    
    // complete the cmd commands by adding rndf, mdf, start, and log files
    if(proc->id == SNasim)
    	snprintf(proc->cmd, sizeof(proc->cmd), this->options.cmd_line_arg[i], rndf, start);
    else if(proc->id == MODmapping)
    	snprintf(proc->cmd, sizeof(proc->cmd), this->options.cmd_line_arg[i], rndf);
    else if(proc->id == MODmissionplanner)
    	snprintf(proc->cmd, sizeof(proc->cmd), this->options.cmd_line_arg[i], rndf, mdf, logpath);
    else if(proc->id == MODtrafficplanner)
    	snprintf(proc->cmd, sizeof(proc->cmd), this->options.cmd_line_arg[i], rndf, logpath);
    else if(proc->id == MODdynamicplanner)
    	snprintf(proc->cmd, sizeof(proc->cmd), this->options.cmd_line_arg[i], s1plogname);
    else snprintf(proc->cmd,sizeof(proc->cmd),this->options.cmd_line_arg[i]);
  }
  
  return 0;
}

int ProcessControl::parseRouteFile(char *routeFile)
{
  if (cmdline_parser_configfile(routeFile, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", routeFile);
  
  this->start = this->options.start_arg;
  this->scenario = this->options.scenario_arg;
  this->rndf = this->options.rndf_arg;
  this->mdf = this->options.mdf_arg;
  this->obsFile = this->options.obsFile_arg;
  this->sceneFunc = this->options.sceneFunc_arg;
  return 0;
}

// Initialize sensnet
int ProcessControl::initSensnet(/*const char *configPath*/)
{
  int i;
  ProcessData *proc;
  
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, MODprocessControl) != 0)
    return ERROR("unable to connect to sensnet");
      
  // Subscribe to groups (sensnet gives each process has a unique group)
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];

    if (proc->blobType == SNprocessResponse)
    {
      if (sensnet_join(this->sensnet,
                       proc->id, SNprocessResponse, sizeof(ProcessResponse)) != 0)
        return ERROR("unable to join process response");
    }
    else
    {
      if (sensnet_join(this->sensnet,
                       SENSNET_SKYNET_SENSOR, proc->blobType, 0x15000) != 0)
	return ERROR("unable to join astate");
    }
  }
  
  return 0;
}


// Finalize sensnet
int ProcessControl::finiSensnet()
{  
  int i;
  ProcessData *proc;

  // Clean up SensNet
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];
    if (proc->blobType == SNprocessResponse)
      sensnet_leave(this->sensnet, proc->id, SNprocessResponse);
    else
      sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, proc->blobType);
  }
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Initialize console display
int ProcessControl::initConsole()
{
  const char *header =
    "-------------------------------------------------------------------------------\n"
    " ProcessControl $Revision$ \n"
    "-------------------------------------------------------------------------------\n"
    "                                                                               \n";

  const char *body = 
    " %%_desc_%02d%%         %%_status_%02d%%          [%%START%02d%%|%%STOP%02d%%|%%LOG%02d%%|%%VIEW%02d%%]\n";
  
  const char *footer = 
    "                                                                               \n"
    "-------------------------------------------------------------------------------\n"
    "                                                                               \n"
    " %stderr%                                                                      \n"
    " %stderr%                                                                      \n"
    " %stderr%                                                                      \n"
    " %stderr%                                                                      \n"
    " %stderr%                                                                      \n"
    "                                                                               \n"
    "                                                                               \n"
    "                                                                               \n"
    "-------------------------------------------------------------------------------\n"
    "[%QUIT%|%START_ALL%|%STOP_ALL%|%KILL_ALL%|%LOG_ALL%|%VIEW_VISUAL%|%VIEW_LOGGRS%]\n";

  int i;
  char temp[8192];
  char buf[1024];
  ProcessData *proc;

  // Initialize the console template
  memset(temp, 0, sizeof(temp)); 
  strcpy(temp, header);

  // Loop through the processes and create corresponding entries in
  // the template.
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];    
    snprintf(buf,sizeof(buf), body, i, i, i, i, i, i);
    strcat(temp, buf);
  }
  strcat(temp, footer);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, temp);
  
  for(i = 0; i < this->numProcs; i++)
  {
    snprintf(buf,sizeof(buf),"%%START%02d%%",i);
    cotk_bind_button(this->console, buf, "  START  ", "",
                     (cotk_callback_t) onUserStartModule, this);
    snprintf(buf,sizeof(buf),"%%STOP%02d%%",i);
    cotk_bind_button(this->console, buf, "  STOP  ", "",
                     (cotk_callback_t) onUserStopModule, this);
    snprintf(buf,sizeof(buf),"%%LOG%02d%%",i);
    cotk_bind_button(this->console, buf, "  LOG  ", "",
                     (cotk_callback_t) onUserLogModule, this);
    snprintf(buf,sizeof(buf),"%%VIEW%02d%%",i);
    cotk_bind_button(this->console, buf, "  VIEW  ", "",
		     (cotk_callback_t) onUserViewModule,this);
  }

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_button(this->console, "%START_ALL%", " START ALL ", "Ss",
                   (cotk_callback_t) onUserStartAll, this);
  cotk_bind_button(this->console, "%STOP_ALL%", " STOP ALL ", "Tt",
                   (cotk_callback_t) onUserStopAll, this);
  cotk_bind_button(this->console, "%KILL_ALL%", " KILL ALL ", "Kk",
                   (cotk_callback_t) onUserKillAll, this);
  cotk_bind_button(this->console, "%LOG_ALL%", " LOG ALL ", "Ll",
                   (cotk_callback_t) onUserLogAll, this);
  cotk_bind_button(this->console, "%VIEW_VISUAL%", " VIEW VISUAL ", "Vv",
                   (cotk_callback_t) onUserViewVisualizers, this);
  cotk_bind_button(this->console, "%VIEW_LOGGRS%", " VIEW LOGGRS ", "Oo",
                   (cotk_callback_t) onUserViewLoggers, this);  
  // TODO
  //snprintf(filename, sizeof(filename), "%s/startup.msg",
  //         this->options.log_path_arg);

  // Initialize the display
  if (cotk_open(this->console, NULL) != 0)
    return -1;

  // Create colors
  cotk_set_color_pair(this->console, 1, COLOR_RED, -1);
  
  // Display some fixed values
  for(i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];    
    snprintf(buf,sizeof(buf),"%%_desc_%02d%%",i);
    cotk_printf(this->console, buf, A_NORMAL, modulename_asString((modulename) proc->id));
  }

  //cotk_printf(this->console, "%log_name%", A_NORMAL, this->logName);

  return 0;
}


// Finalize console display
int ProcessControl::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Gets the index associated with a given token i.e. %START12% returns 12
int ProcessControl::getTokenIndex(const char *token)
{  
  char tid[8];
  int j = 0;
  
  for(int i = 0; i < (int) strlen(token); i++)
  {
    if(isdigit(token[i]))
    {
      while(isdigit(token[i]))
        tid[j++] = token[i++];
      tid[j++] = 0;
      break;         
    }
  }

  return atoi(tid);
}


// Handle button callbacks
int ProcessControl::onUserQuit(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStartAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user start all");
  for(int i = 0; i < self->numProcs; i++)
  {
    self->startProcess(i);
  }
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStopAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user stop all");
  for(int i = 0; i < self->numProcs; i++)
  {
    self->sendQuitRequest(i);
  }
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserKillAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user kill all");
  system("killall screen");
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserLogAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user log all");
  for(int i = 0; i < self->numProcs; i++)
  {
    self->sendLogRequest(i);
  }
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserViewVisualizers(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user view visualizers");
  self->viewVisualizers();
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserViewLoggers(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user view loggers");
  self->viewLoggers();
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserLogModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);  
  MSG("user log module-id: %s", modulename_asString((modulename) self->procs[tid].id));
  self->sendLogRequest(tid);  
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStartModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user start module-id: %s", modulename_asString((modulename) self->procs[tid].id));
  self->startProcess(tid);
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStopModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user stop module-id: %s", modulename_asString((modulename) self->procs[tid].id));
  self->sendQuitRequest(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserKillModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user kill module-id: %s", modulename_asString((modulename) self->procs[tid].id));
  self->killProcess(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserViewModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user view module-id: %s",modulename_asString((modulename) self->procs[tid].id));
  self->viewProcess(tid);
  return 0;
}

// Start process in a detached screen
int ProcessControl::startProcess(int index)
{
  char cmd[1024];
  char *buf = "screen -d -m -S %s \"\"ssh -t %s \"cd %s; ./%s\"\"; echo press enter to close; read\"";

  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.quit = false;
  
  snprintf(cmd, sizeof(cmd), buf,
           modulename_asString((modulename) proc->id),
           proc->host, this->binPrefix, proc->cmd);
  
  //snprintf(cmd, sizeof(cmd),"cd %s; ./%s", this->binPrefix, proc->cmd);

  MSG("running: %s", cmd);
  system(cmd);

  return 0;
}


// Send the quit request 
int ProcessControl::sendQuitRequest(int index)
{
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.moduleId = proc->id;
  proc->request.quit = true;

  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, proc->id, SNprocessRequest,
                    0, sizeof(proc->request), &proc->request) != 0)
    return MSG("unable to send quit request");

  return 0;
}


// kills given process
int ProcessControl::killProcess(int index)
{
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.quit = false;
  char cmd[1024];
  char *buf ="xterm -rightbar -sb -T %s -e screen -r %s";
  
  snprintf(cmd, sizeof(cmd),buf, modulename_asString((modulename) proc->id),modulename_asString((modulename) proc->id));
  
  MSG("killing: %s", cmd);
  system(cmd);

  return 0;
}

// Toggle the logging state
int ProcessControl::sendLogRequest(int index)
{
  ProcessData *proc;
  
  proc = &this->procs[index];  
  proc->request.moduleId = proc->id;
  proc->request.enableLog = !proc->request.enableLog;

  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, proc->id, SNprocessRequest,
                    0, sizeof(proc->request), &proc->request) != 0)
    return MSG("unable to send log request");

  return 0;
}


// View Process that was in a detached screen
int ProcessControl::viewProcess(int index)
{
  ProcessData *proc;
  
  proc = &this->procs[index];
  proc->request.quit = false;
  char cmd[1024];
  char *buf ="xterm -rightbar -sb -T %s -e screen -r %s &";
  
  snprintf(cmd, sizeof(cmd),buf, modulename_asString((modulename) proc->id),modulename_asString((modulename) proc->id));
  
  MSG("viewing: %s",cmd);
  system(cmd);
  
  return 0;
}

int ProcessControl::runVisualizers()
{
  // declare visualizers
  this->numVisualizers=2;
  this->visualizers[0]="planviewer2";
  this->visualizers[1]="testMapper";
  
  char cmd[1024];
  char *buf="screen -d -m -S %s \"\"%s/./../Drun planviewer2\"; echo press enter to close; read\"";
  snprintf(cmd, sizeof(cmd), buf, "planviewer2", this->binPrefix);
  system(cmd);
  
  buf = "screen -d -m -S %s \"\"%s/./../Drun testMapper %s 2\"; echo press enter to close; read;\"";
  snprintf(cmd, sizeof(cmd), buf, "testMapper", this->binPrefix, this->rndf);
  
  system(cmd);

  buf = "screen -d -m -S %s \"\"%s/./../Drun mapviewer --recv-subgroup=-2 %s\"; echo press enter to close; read\"";
  if(this->costmap)
    snprintf(cmd, sizeof(cmd), buf, "mapviewer", this->binPrefix, "--show-costmap");
  else
    snprintf(cmd, sizeof(cmd), buf, "mapviewer", this->binPrefix, "");

  system(cmd);
  return 0;
}

int ProcessControl::viewVisualizers()
{
  int i;
  char cmd[1024];
  char *buf ="xterm -rightbar -sb -T %s -e screen -r %s &";
  for(i = 0; i < this->numVisualizers; i++)
  {
    snprintf(cmd, sizeof(cmd), buf, this->visualizers[i], this->visualizers[i]);
    system(cmd);
  }
  return 0;
}


int ProcessControl::runLoggers()
{
  // declare loggers
  this->loggers[0] = "statelogger";
  this->numLoggers = 1;
  if(!this->bRunTrafsim)
  {
    this->loggers[1] = "skynet-logger";
    this->numLoggers = 2;
  }

  // setup the logfile name
  time_t temp;
  struct tm *timeptr;
  
  
  temp = time(NULL);
  timeptr = localtime(&temp);
  
  char timestamp[1024];

  strftime(timestamp,sizeof(timestamp),"%Y-%m-%d-%a-%H-%M", timeptr);
  
  char logname[1024];

  snprintf(logname, sizeof(logname), "%s/systemtest-%s-%s.log", this->logpath, timestamp, this->scenario);

  char cmd[1024];
  char *buf = "screen -d -m -S %s \"\"%s/./../Drun statelogger --file=%s\"; echo press enter to close; read\"";
  snprintf(cmd, sizeof(cmd), buf, "statelogger", this->binPrefix, logname);

  system(cmd);

  if(!this->bRunTrafsim)
  {    snprintf(logname, sizeof(logname), "%s/skynetlogger-%s-%s.log", this->logpath, this->scenario, timestamp);

    buf = "screen -d -m -S %s \"\"%s/./../Drun skynet-logger %s\"; echo press enter to close; read\"";
    snprintf(cmd, sizeof(cmd), buf, "skynet-logger", this->binPrefix, logname);
    system(cmd);
  }

  return 0;
}

int ProcessControl::viewLoggers()
{
  int i;
  char cmd[1024];
  char *buf ="xterm -rightbar -sb -T %s -e screen -r %s &";
  MSG("numLoggers= %d", numLoggers);
  for(i = 0; i < this->numLoggers; i++)
  {
    snprintf(cmd, sizeof(cmd), buf, this->loggers[i], this->loggers[i]);
    system(cmd);
  }
  return 0;
}


// Check for changes to the process state
int ProcessControl::updateProcessStatus()
{
  int i;
  int status;
  int blobId;
  ProcessData *proc;
  ProcessResponse response;
  char token[128];

  // Don't run too fast
  usleep(100000);
  
  // Wait for new updates (with timeout)
  status = sensnet_wait(sensnet, 100);
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));

  // Update all entries to show time since last heartbeat
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];    

    memset(&response, 0, sizeof(response));

    if (proc->blobType == SNprocessResponse)
    {
      // Read process response message
      if (sensnet_read(sensnet, proc->id, SNprocessResponse,
                       &blobId, sizeof(response), &response) != 0)
        return -1;
    }
    else
    {
      // For non-process messages, take a peek at the timestamp
      uint64_t timestamp;
      if (sensnet_peek_ex(sensnet, SENSNET_SKYNET_SENSOR, proc->blobType,
                          &blobId, NULL, &timestamp) != 0)
        return -1;
      response.timestamp = timestamp;
    }

    if (blobId < 0)
      continue;

    if (this->console)
    {
      double time, lat;
      char size[64];

      // Latency (i.e., how long since we last saw a response).
      // We have to be careful with the unsigned type conversion.
      lat = ((double) DGCgettime() - (double) response.timestamp) / 1e6;

      // Process timestamp
      time = fmod((double) response.timestamp / 1e6, 1000);
      
      // Log size in human-readable format
      if (response.logSize < 1024)
        snprintf(size, sizeof(size), "%4dKB", response.logSize);
      else
        snprintf(size, sizeof(size), "%4dMB", response.logSize / 1024);
      
      snprintf(token, sizeof(token), "%%_status_%02d%%", i);
      if (lat < 1.0)
        cotk_printf(this->console, token, A_NORMAL, "%8.3f %s      ", time, size);
      else
        cotk_printf(this->console, token, COLOR_PAIR(1), "%8.3f %s (%3.0f)", time, size, lat);
    }
  }

  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  ProcessControl *proctl; 

  // Create module
  proctl = new ProcessControl();
  assert(proctl);

  // Parse command line options
  if (proctl->parseCmdLine(argc, argv) != 0)
    return -1;
  
  if (proctl->initSensnet() != 0)
    return -1;
    
  // Initialize console
  if (!proctl->options.disable_console_flag)
  {
    if (proctl->initConsole() != 0)
      return -1;
  }

  if(proctl->startall)
    ProcessControl::onUserStartAll(proctl->console, proctl, "");

  // Run mappers
  proctl->runVisualizers();

  // Run loggers
  proctl->runLoggers();

  while (!proctl->quit)
  {
    if (proctl->updateProcessStatus() != 0)
      break;
    if (proctl->console)
      cotk_update(proctl->console);
  }

  // Clean up
  ProcessControl::onUserKillAll(proctl->console, proctl, "");
  proctl->finiConsole();
  proctl->finiSensnet();

  cmdline_parser_free(&proctl->options);  
  delete proctl;
  MSG("program exited cleanly");
  
  return 0;
}
