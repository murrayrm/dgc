# This program will return the load average that it gets from uptime
# format: ./loadAverage.sh host1 host2 ...

filePath=/tmp
outFile=$filePath/loads
tmpFile=$filePath/tmpFile

if [ -f $tmpFile ]
then
    exit 1
fi

if [ "$#" == "0" ] 
then
    exit 1
fi

: > $tmpFile
rm $outFile >/dev/null 2>&1

numHosts=0
while [ "$#" != "0" ]
do
    array[numHosts]=$1
    numHosts=`expr $numHosts + 1`
    shift
done

for(( i=0; i<numHosts; i++ ))
do

  if [ "${array[i]}" = "localhost" ]
  then
      data=`uptime`
  else
      data=`ssh -o ConnectTimeout=5 ${array[i]} uptime`
  fi
  if [ "$data" != "" ]
  then
      echo $data | sed 's/.*load average: //' | awk '{print $1}' | sed 's/,//' >> $tmpFile
  else
      echo 99.99 >> $tmpFile
  fi
done

mv $tmpFile $outFile