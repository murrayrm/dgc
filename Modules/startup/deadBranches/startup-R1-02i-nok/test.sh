#!/bin/bash

UP_COUNT=1
ACCOUNT=team
KEEP_ALIVE=0
SKYNET_KEY=10
TAB_COUNT=0
GEOMETRY=100x3
NOW=--now
MODE=1

setup()
{
    if [ $MODE = 0 ] ; then
        SCMODE="-D -m"
    else
        SCMODE=""
    fi
    if [ $TAB_COUNT = 0 ] ; then
	CMD=" --window --geometry $GEOMETRY --hide-menubar -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen $SCMODE -S $NAME /home/team/dgc/bin/run_here.sh $SKYNET_KEY $KEEP_ALIVE ./$PROC $ARGS \" "
    else
	NEW_CMD=" --tab -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen $SCMODE -S $NAME /home/team/dgc/bin/run_here.sh $SKYNET_KEY $KEEP_ALIVE ./$PROC $ARGS \" "
	CMD="$CMD $NEW_CMD "
    fi
    TAB_COUNT=1
}

watch()
{
    if [ $TAB_COUNT = 0 ] ; then
	CMD=" --window --geometry $GEOMETRY --hide-menubar -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen -S $NAME -x \" "
    else
	NEW_CMD=" --tab -t $NAME -e \
                 \" ssh -x -t $ACCOUNT@$HOST screen -S $NAME -x \" "
	CMD="$CMD $NEW_CMD "
    fi
    TAB_COUNT=1
}

Astate()
{
    HOST=skynet5
    NAME=Astate
    PROC=astate
    ARGS="$NOW"
}

roofLadar()
{
    HOST=skynet5
    NAME=roofLadar
    PROC=ladarFeeder
    ARGS="--ladar roof $NOW"
}

rieglLadar()
{
    HOST=skynet5
    NAME=rieglLadar
    PROC=ladarFeeder
    ARGS="--ladar riegl $NOW"
}

smallLadar()
{
    HOST=skynet5
    NAME=smallLadar
    PROC=ladarFeeder
    ARGS="--ladar small $NOW"
}

frontLadar()
{
    HOST=skynet5
    NAME=frontLadar
    PROC=ladarFeeder
    ARGS="--ladar front $NOW"
}

bumperLadar()
{
    HOST=skynet5
    NAME=bumperLadar
    PROC=ladarFeeder
    ARGS="--ladar bumper $NOW"
}

fusionMapper()
{
    HOST=sitka 
    NAME=fusionMapper 
    PROC=fusionMapper 
    ARGS="$NOW"
}

planner()
{
    HOST=sitka
    NAME=Planner 
    PROC=plannerModule
}

longStereoFeeder()
{
    HOST=skynet3 
    NAME=longStereoFeeder 
    PROC=stereoFeeder 
    ARGS="--pair long $NOW" 
}

shortStereoFeeder()
{
    HOST=skynet6
    NAME=shortStereoFeeder 
    PROC=stereoFeader 
    ARGS="--pair short $NOW" 
}

aDrive()
{
    HOST=skynet1 
    NAME=Adrive
    PROC=adrive
}

DBS()
{
    HOST=skynet1
    NAME=DBS 
    PROC=DBS
}

trajectoryFollower()
{
    HOST=skynet1
    NAME=TrajectorFollower 
    PROC=trajFollower
}

superCon()
{
    HOST=skynet1
    NAME=SuperCon 
    PROC=superCon 
    ARGS="--log 6"
}

timber()
{
    HOST=skynet1
    NAME=Timber
    PROC=timber
}

start_skynet5()
{
    echo Staring all processes
    echo Use Ctrl-\\ to terminate remote processes
    #SKYNET 5 Modules in separate window
    Astate 
    setup
    # Ladar Feaders   
    roofLadar 
    setup
    rieglLadar 
    setup
    smallLadar 
    setup
    frontLadar 
    setup
    bumperLadar 
    setup
    `echo $CMD | xargs gnome-terminal` &
    #Reset tab count
    TAB_COUNT=0

    echo Waiting 30 seconds  
    echo If astate is not running, Ctrl-C now.
    sleep 30
}

start_other()
{
    fusionMapper
    setup
    planner
    setup
    longStereoFeeder
    setup
    shortStereoFeeder
    setup

    #SKYNET 7 Modules
    # -SBG- Don't know about this one
    # -SBG- road may need to start on skynet 7 X console
    #HOST=skynet7
    #PROC=road setup

    # Run Sitka, Skynet 3, 6 & 7 in one window
    `echo $CMD | xargs gnome-terminal ` &
    #Reset Tab Count
    TAB_COUNT=0
}

start_skynet1()
{
    #SKYNET 1 Modules
    aDrive
    setup
    DBS
    setup
    trajectoryFollower
    setup
    superCon
    setup
    timber
    setup
    #Start SKYNET 1 Modules in single window
    `echo $CMD | xargs gnome-terminal ` &
    #Reset Tab Count
    TAB_COUNT=0
}



start_skynet5()
{
    echo Staring all processes
    echo Use Ctrl-\\ to terminate remote processes
    #SKYNET 5 Modules in separate window
    Astate 
    watch
    # Ladar Feaders   
    roofLadar 
    watch
    rieglLadar 
    watch
    smallLadar 
    watch
    frontLadar 
    watch
    bumperLadar 
    watch
    `echo $CMD | xargs gnome-terminal` &
    #Reset tab count
    TAB_COUNT=0

    echo Waiting 30 seconds  
    echo If astate is not running, Ctrl-C now.
    sleep 30
}

start_other()
{
#    fusionMapper
#    watch
#    planner
#    watch
    longStereoFeeder
    watch
    shortStereoFeeder
    watch

    #SKYNET 7 Modules
    # -SBG- Don't know about this one
    # -SBG- road may need to start on skynet 7 X console
    #HOST=skynet7
    #PROC=road watch

    # Run Sitka, Skynet 3, 6 & 7 in one window
    `echo $CMD | xargs gnome-terminal ` &
    #Reset Tab Count
    TAB_COUNT=0
}

start_skynet1()
{
    #SKYNET 1 Modules
    aDrive
    watch
    DBS
    watch
    trajectoryFollower
    watch
    superCon
    watch
    timber
    watch
    #Start SKYNET 1 Modules in single window
    `echo $CMD | xargs gnome-terminal ` &
    #Reset Tab Count
    TAB_COUNT=0
}

for arg in $*
do
        case "${arg}" in
        stop)
                stop
                ;;
        watch)
                attach
                ;;
        start)
                start_other
                ;;
	restart)
		restart
                ;;
	esac
done
