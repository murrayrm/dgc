/*!
 * \file ProcessControl.cc
 * \brief Control Module for processes
 *
 * \author David Trotz, modified by Robbie Paolini
 * \date 9/15/07
 *
 * ProcessControl.cc runs any processes you choose in detached screens, so that
 * when you want to start a lot of processes at once, you don't have useless 
 * program windows crowding the screen.
 *
 * The program starts a cotk display that allows you to control any 
 * processes you choose to run. Processes that need their sensnet messages 
 * monitored should be stored in a config file, most notably sim.CFG, 
 * alicesim.CFG or field.CFG, however any config file can be written and fed in
 * to the program with the option --cfg. Processes that you want to start but
 * do not want monitored can go in the otherProcs file, which is default.CFG.
 * 
 * On all the processes, there are at least 4 options: start, kill, restart, 
 * and view. On the processes that have sensnet messaging, there is a 
 * toggled log switch as well.
 *
*/ 


#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <ctype.h>
#include <time.h>
#include <fstream>
#include <sys/stat.h>

#include <signal.h>
#include <cotk/cotk.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/ProcessState.h>

#include "cmdline.h"

volatile sig_atomic_t quit = 0; // flag for determining when to quit
void sigintHandler(int);        // function to handle interrupts

// function that waits for the specified time 
// unless it is interrupted by keyboard input
int uwait(uint64_t timeout);

/*! \brief ProcessControl class */
class ProcessControl
{
  public:   

  /// Default constructor
  ProcessControl();

  /// Default destructor
  ~ProcessControl();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);

  /// Read all of the config files
  int readConfigFiles();
  
  /// Parse the config file
  int parseConfigFile();

  /// Parse the route file
  int parseRouteFile();

  /// Parse the otherProcesses file
  int parseOtherProcsFile();

  /// Determine blobType from process id
  int blobTypeFromId(int id);
  
  /// Parse the cmd line in the config file
  char *parseConfigCmd(char *cmdline);
  
  /// Initialize sensnet
  int initSensnet();

  /// Finalize sensnet
  int finiSensnet();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();
  
  /// Console button callback
  static int onUserQuit(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStartAll(cotk_t *console, ProcessControl *self, const char *token);
  //static int onUserStopAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserKillAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogAll(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogLadars(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserStartModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserRestartModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserKillModule(cotk_t *console, ProcessControl *self, const char *token);
  //static int onUserStopModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserLogModule(cotk_t *console, ProcessControl *self, const char *token);
  static int onUserViewModule(cotk_t *console, ProcessControl *self, const char *token);
  

  // Gets the index associated with a given token i.e. %START12% returns 12
  static int getTokenIndex(const char *token);
  
  // Start the given process.
  int startProcess(int index);

  // Restart the given process.
  int restartProcess(int index);

  // Send requests
  //int sendQuitRequest(int index);
  int sendLogRequest(int index);

  // Update the display with the current process response state
  int updateProcessStatus();

  // Update window status on the screen
  void updateWindowStatus(int index, bool open);

  // Update load averages
  void updateLoadAverages();
  
  // Kill the given process
  int killProcess(int index);
  
  // View the Given Process
  int viewProcess(int index);

  // Program options
  gengetopt_args_info cmdOptions;
  gengetopt_args_info cfgFileOptions;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  // SensNet handle
  sensnet_t *sensnet;

  // Ticker: keep track of updateProcessStatus
  short ticker;
  // boolean to keep track of whether or not updateLoadAverages 
  // has read in the file containing load avgs or not
  bool fileRead;

  // rndf file
  char *rndf;
  // mdf file
  char *mdf;
  // simulation start string
  char *start;
  // scenario name
  char *scenario;
  // obsFile name
  char *obsFile;
  // sceneFunc
  char *sceneFunc;
	
  // Prefix for executables (e.g., navigation-rpaolini01/bin/)
  char binPrefix[1024];
  // start all option
  bool startall;
  // continue option. If this option is added, then whenever you go to 
  // start everything, mission.log will NOT be deleted
  bool cont;
  // auto restart option
  bool autoRestart;
  // costmap option
  bool costmap;

  // variable to hold whether we are 
  // starting all processes at this time or not
  bool startingAll;

  //configuration (field, alicesim, sim)
  char *configuration;

  // configuration file names
  char *routeFile;
  char *configFile;
  char *otherProcsFile;

  // route file name
  char *route;

  //variables for asim:
  double northing;
  double easting;
  double yaw;
  
  //tsfile
  char *tsfile;
  // log path
  char *logpath;
  // time stamp for all log files
  char *timestamp;
  
  // Data for each process
  struct ProcessData
  {
    // Process name
    char *name; 
    // Process id 
    int id;
    // Expected blob type
    int blobType;
    // Last blob id
    int blobId;
    // Host
    char *host;
    // Command-line
    char cmd[1024];
    // whether process is running or not
    bool running;
    // whether a command to start the process was issued or not
    bool started;
    // Whether this process was killed by the user
    bool killed;
    // whether window for the process is open or not
    bool windowOpen;
    // This option can be specified by the user to 
    // automatically open the process whenever it is started
    bool autoOpen;
    // Current process request
    ProcessRequest request;
  };

  // Process list
  int numProcs;
  int numSNProcs, numOtherProcs;
  ProcessData procs[64];
  
  // keep track of hosts
  int numHosts;
  char *hosts[64];
  double load[64];

  // Console text display
  cotk_t *console;
};


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


// Default constructor
ProcessControl::ProcessControl()
{
  memset(this, 0, sizeof(*this));
  return;
}


// Default destructor
ProcessControl::~ProcessControl()
{
  return;
}

//****************************************************
// Parse the command line
//****************************************************
int ProcessControl::parseCmdLine(int argc, char **argv)
{
  // Parse command line options (there's a lot of them =P )

  // Load options
  if (cmdline_parser(argc, argv, &this->cmdOptions) < 0)
    return -1;

  // Fill out the spread name
  if (this->cmdOptions.spread_daemon_given)
    this->spreadDaemon = this->cmdOptions.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  // Fill out the skynet key
  if (this->cmdOptions.skynet_key_given)
    this->skynetKey = this->cmdOptions.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  

  //get the configuration
  if(this->cmdOptions.field_flag)
    this->configuration="field";
  else if(this->cmdOptions.alicesim_flag)
    this->configuration="alicesim";
  else if(this->cmdOptions.sim_flag)
    this->configuration="sim";
  else
    this->configuration="";

  
  // get the rndf file
  if (this->cmdOptions.rndf_given)
    this->rndf = this->cmdOptions.rndf_arg;
  else
    this->rndf="";

  // get the mdf file
  if (this->cmdOptions.mdf_given)
    this->mdf = this->cmdOptions.mdf_arg;
  else
    this->mdf="";

  // get the start location for simulation
  if (this->cmdOptions.start_given)
    this->start = this->cmdOptions.start_arg;
  else
    this->start="";
  
  // get the observation file
  if(this->cmdOptions.obsFile_given)
    this->obsFile = this->cmdOptions.obsFile_arg;
  else 
    this->obsFile = "";
  
  // get the scene function
  if(this->cmdOptions.sceneFunc_given)
    this->sceneFunc = this->cmdOptions.sceneFunc_arg;
  else 
    this->sceneFunc = "";

  // get the scenario function
  if(this->cmdOptions.scenario_given)
    this->scenario = this->cmdOptions.scenario_arg;
  else
    this->scenario = "";

  // get the start all option
  if(this->cmdOptions.no_start_flag)
    this->startall=false;
  else
    this->startall=true;

  if(this->cmdOptions.continue_flag)
    this->cont=true;

  // get the auto restart option
  if(this->cmdOptions.no_restart_flag)
    this->autoRestart=false;
  else
    this->autoRestart=true;

  // get the costmap option
  if(this->cmdOptions.costmap_flag)
    this->costmap=true;
  
  
  // Get optional asim cmdOptions
  if(this->cmdOptions.northing_given)
    this->northing = this->cmdOptions.northing_arg;
  if(this->cmdOptions.easting_given)
    this->easting = this->cmdOptions.easting_arg;
  if(this->cmdOptions.yaw_given)
    this->yaw = this->cmdOptions.yaw_arg;


  // get the tsfile
  if(this->cmdOptions.tsfile_given)
    this->tsfile = this->cmdOptions.tsfile_arg;
  else
    this->tsfile = "";

  // get the tplanner log name
  if (this->cmdOptions.logpath_given)
    this->logpath = this->cmdOptions.logpath_arg;
  else
    this->logpath = "";

  // get the time stamp for log files
  if(this->cmdOptions.timestamp_given)
    this->timestamp = this->cmdOptions.timestamp_arg;
  else
    this->timestamp = "";
  
  // Get the prefix for executables
  if (this->cmdOptions.bin_prefix_given)
    snprintf(this->binPrefix,sizeof(binPrefix),this->cmdOptions.bin_prefix_arg);
  else if (getenv("DGC_CONFIG_PATH"))
  {
    snprintf(this->binPrefix,sizeof(this->binPrefix),"%s/../bin/",getenv("DGC_CONFIG_PATH"));
  }
  else
    return ERROR("--bin-prefix must be specified");


  // Get route file from command line
  if(this->cmdOptions.route_given)
  {
    this->routeFile = dgcFindConfigFile(this->cmdOptions.route_arg, "startup");
    // load the route file
    if(parseRouteFile() != 0)
      return -1;
  }

  // Load configuration files
  readConfigFiles();
  
  return 0;
}


//****************************************************
// Parse the route file
//****************************************************
int ProcessControl::parseRouteFile()
{
  if (cmdline_parser_configfile(this->routeFile, &this->cmdOptions, false, false, false) != 0)
    MSG("unable to process route file %s", this->routeFile);
  
  this->start = this->cmdOptions.start_arg;
  this->scenario = this->cmdOptions.scenario_arg;
  this->rndf = this->cmdOptions.rndf_arg;
  this->mdf = this->cmdOptions.mdf_arg;
  this->obsFile = this->cmdOptions.obsFile_arg;
  this->sceneFunc = this->cmdOptions.sceneFunc_arg;
  return 0;
}

//****************************************************
// Read in the list of processes from the config files
//****************************************************
int ProcessControl::readConfigFiles()
{  
  // If this function is read more than once, 
  // we need to reset the number of processes running
  numSNProcs=0;
  numProcs=0;
 
  // Load the main configuration file
  if(strcmp(this->configuration,"")!=0 || this->cmdOptions.cfg_given)
  {
    // Get the name of the file first
    char *fileName;
    if(strcmp(this->configuration,"field")==0)
    {
      fileName="field.CFG";
      // We need to create the field.CFG 
      // with the following python script
      char cmd[512];
      snprintf(cmd, sizeof(cmd), 
	       "python %s/../etc/startup/createConfig.py", this->binPrefix);
      system(cmd);
    }
    else if(strcmp(this->configuration,"alicesim")==0)
      fileName="alicesim.CFG";
    else if(strcmp(this->configuration,"sim")==0)
      fileName="sim.CFG";
    else
      fileName=this->cmdOptions.cfg_arg;

    this->configFile = dgcFindConfigFile(fileName, "startup"); 
    // Load the configuration file
    if (parseConfigFile() != 0)
      return -1;
  }
  

  // Get other procedures config file from command line, default is defualt.CFG (duh)
  this->otherProcsFile = dgcFindConfigFile(this->cmdOptions.otherProcs_arg, "startup");
  // Load the other procedures file
  if (parseOtherProcsFile() != 0)
    return -1;
  return 0;
}


//****************************************************
// Parse the config file, getting the name, command, 
// host, and whether to automatically open it or not
//****************************************************
int ProcessControl::parseConfigFile()
{
  int j;
  ProcessData *proc;
    
  // Load options from the configuration file
  if (cmdline_parser_configfile(this->configFile, &this->cfgFileOptions, false, true, false) != 0)
    MSG("unable to process configuration file %s", this->configFile);

  // Loop through the modules in the configuration file and
  // create corresponding entries in the internal process list.
  for (int i = 0; i < (int) this->cfgFileOptions.module_id_given; i++)
  {
    MSG("reading option");
    // increment number of processes, assign host and module id
    proc = &this->procs[this->numProcs++];
    this->numSNProcs++;
    proc->name=this->cfgFileOptions.module_id_arg[i];
    proc->id = modulenamefromString(proc->name);
    proc->host = this->cfgFileOptions.hostname_arg[i];    
    proc->autoOpen = this->cfgFileOptions.open_arg[i];
    
    // get the blobType and parse the command line
    proc->blobType=blobTypeFromId(proc->id);
    snprintf(proc->cmd,sizeof(proc->cmd),parseConfigCmd(this->cfgFileOptions.cmd_line_arg[i]));

    // Add this host to the list of hosts, if not already there
    for(j=0; j<numHosts; j++)
    {
      if(strcmp(proc->host, hosts[j])==0)
	break;
    }
    if(j==numHosts)
    {
      hosts[j]=proc->host;
      numHosts++;
    }
  }
  return 0;
}

//****************************************************
// Parse the other procedures config file
//****************************************************
int ProcessControl::parseOtherProcsFile()
{
  if (cmdline_parser_configfile(this->otherProcsFile, &this->cfgFileOptions, false, true, false) != 0)
    MSG("unable to process other procedures file %s", this->otherProcsFile);
    
  ProcessData *proc;
  for (int i = 0; i < (int) this->cfgFileOptions.module_id_given; i++)
  {
    MSG("reading option");
    // increment number of processes, assign host and module id
    proc = &this->procs[this->numProcs++];
    this->numOtherProcs++;
    proc->name = this->cfgFileOptions.module_id_arg[i];
    proc->host = this->cfgFileOptions.hostname_arg[i];
     
    // parse the command line
    snprintf(proc->cmd,sizeof(proc->cmd),parseConfigCmd(this->cfgFileOptions.cmd_line_arg[i]));

    // Add this host to the list of hosts
    int j;
    for(j=0; j<numHosts; j++)
    {
      if(strcmp(proc->host, hosts[j])==0)
	break;
    }
    if(j==numHosts)
    {
      hosts[j]=proc->host;
      numHosts++;
    }
    
    // special case for trafsim
    if(strcmp(proc->name,"trafsim")==0)
    {
#warning "hack: bad command line option checking for trafsim"
      string givenCmd = "";
      stringstream s;
      s << proc->cmd;
      givenCmd = s.str();
      if (givenCmd == "--noRoadLines") {
	snprintf(proc->cmd,sizeof(proc->cmd), "python -i %s/../lib/PYTHON/trafsim/GenericApp.py --loadFile %s --send-subgroup -12 --loadZones %s --loadStopLines %s --noRoadLines",this->binPrefix, this->tsfile, this->rndf, this->rndf);
      }
      else {
	snprintf(proc->cmd,sizeof(proc->cmd), "python -i %s/../lib/PYTHON/trafsim/GenericApp.py --loadFile %s --send-subgroup -12 --loadZones %s --loadStopLines %s",this->binPrefix, this->tsfile, this->rndf, this->rndf);
      }
      if(this->sceneFunc!=NULL && strcmp(this->sceneFunc,"")!=0)
      {
	strcat(proc->cmd, " --loadFunction ");
	strcat(proc->cmd, sceneFunc);
	strcat(proc->cmd, " --sleep 8");
      }
    }
  }

  return 0;
}

//****************************************************
// get the blobType from the id of the process
//****************************************************
int ProcessControl::blobTypeFromId(int id)
{
  switch(id)
  {
  case SNasim:
  case SNastate: return SNstate;
  case SNadrive: return SNactuatorstate;
  case SNtrajfollower: return SNadrive_command;
  case MODtrafficplanner: 
  case MODdynamicplanner: return SNtraj;
  case MODhealthMonitor: return SNvehicleCapability;
    default: return SNprocessResponse;
  }
}

//****************************************************
// Parse the cmd-line argument in the CFG file
//****************************************************
char *ProcessControl::parseConfigCmd(char *cmdline)
{
  static char command[1024]; // actual command that will be run
  char options[20][128]; // will hold each of the options
  char *beg;
  char *end;
  char temp[1024];
  int k=0;
  bool only1arg;

  beg=cmdline;
  end=strchr(cmdline,' ');
  
  // special case if no arguments passed with process
  if(end==NULL)
  {
    strcpy(options[0],cmdline);
    only1arg=true;
  }

  while(k<19 && !only1arg)
  {
    strncpy(options[k],beg,end-beg);
    options[k][end-beg]='\0'; // add end of string character
    k++;

    beg=end+1;
    end=strchr(beg, ' ');
    
    // special action for last option
    if(end==NULL)
    {
      end=strchr(beg, '\0');
      strncpy(options[k],beg,end-beg);
      options[k][end-beg]='\0';
      break;
    }
  }

  // first option is the actual process, leave that
  strcpy(command,options[0]);
  
  // Wierd things happen for testMapper and mapviewer, skynet-logger
  if(strcmp(command, "testMapper")==0)
  {
    strcat(command, " ");
    strcat(command, rndf);
  }
  else if(strcmp(command,"mapviewer")==0 && costmap)
  {
    strcat(command, " --show-costmap");
  }
  else if(strcmp(command, "skynet-logger")==0)
  {
    char logname[1024];
    snprintf(logname, sizeof(logname), " %s/skynetlogger-%s-%s.log", this->logpath, this->scenario, this->timestamp);
    strcat(command, logname);
  }


  // now, go through each of the options, adding a space before it.
  // If the option is something special, such as an rndf, mdf, 
  // etc, then add to the string setting it equal to internal variables
  for(int i=1; i<=k; i++)
  {
    if(strcmp(options[i],"--rndf")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], rndf);
    }
    else if(strcmp(options[i],"--rndf-start")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], start);
    }
    else if(strcmp(options[i],"--mdf")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], mdf);
    }
    else if(strcmp(options[i],"--log-path")==0)
    {
      strcat(options[i],"=");
      strcat(options[i], logpath);
    }
    else if(strcmp(options[i],"--skynet-key")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp),"=%d",skynetKey);
      strcat(options[i], temp);
    }
    else if(strcmp(options[i],"--file")==0)
    {
      char logname[1024];
      snprintf(logname, sizeof(logname), "%s/systemtest-%s-%s.log", this->logpath, this->timestamp, this->scenario);
      strcat(options[i], "=");
      strcat(options[i],logname);      
    }
    // special case, s1planner, polysim's config file
    else if(strcmp(options[i],"tee")==0)
    {
      char s1plogname[512];
      snprintf(s1plogname, sizeof(s1plogname), " %ss1planner-%s.%s.log",logpath, scenario, timestamp);

      strcat(options[i], s1plogname);
    }
    else if(strcmp(options[i],"-N")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp)," %f",northing);
      strcat(options[i], temp);
    }
    else if(strcmp(options[i],"-E")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp)," %f",easting);
      strcat(options[i], temp);
    }

    else if(strcmp(options[i],"-Y")==0)
    {
      char temp[16];
      snprintf(temp, sizeof(temp)," %f",yaw);
      strcat(options[i], temp);
    }
    
    snprintf(temp,sizeof(temp)," %s",options[i]);
    strcat(command,temp);
  }

  return command;
}

// Initialize sensnet
int ProcessControl::initSensnet(/*const char *configPath*/)
{
  int i;
  ProcessData *proc;
  
  // Initialize SensNet
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet,
                      this->spreadDaemon, this->skynetKey, MODprocessControl) != 0)
    return ERROR("unable to connect to sensnet");
      
  // Subscribe to groups (sensnet gives each process has a unique group)
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];

    if (proc->blobType == SNprocessResponse)
    {
      if (sensnet_join(this->sensnet,
                       proc->id, SNprocessResponse, sizeof(ProcessResponse)) != 0)
        return ERROR("unable to join process response");
    }
    else
    {
      if (sensnet_join(this->sensnet,
                       SENSNET_SKYNET_SENSOR, proc->blobType, 0x15000) != 0)
	return ERROR("unable to join astate");
    }
  }
  
  return 0;
}


// Finalize sensnet
int ProcessControl::finiSensnet()
{  
  int i;
  ProcessData *proc;

  // Clean up SensNet
  for (i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];
    if (proc->blobType == SNprocessResponse)
      sensnet_leave(this->sensnet, proc->id, SNprocessResponse);
    else
      sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, proc->blobType);
  }
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}


// Initialize console display
int ProcessControl::initConsole()
{
  const char *header =
    "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
    " ProcessControl $Revision$\n"
    "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
    "\n"
    "Process Name           Logsize  Health  Lat                        %OVHEALTH%     Process Name           Logsize  Health  Lat                        %OVHEALTH%    \n"
    "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

  const char *body =
    "%%_desc_%02d%%            %%_status_%02d%%           [%%VW%02d%%|%%KL%02d%%|%%RT%02d%%|%%STR%02d%%|%%L%02d%%]";
  
  const char *body2 =
    "%%_desc_%02d%%            %%s%02d%%                  [%%VW%02d%%|%%KL%02d%%|%%RT%02d%%|%%STR%02d%%]      ";

  const char *footer = 
    "\n"
    "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
    " load averages:                                                                                                                                                    \n"
    " %loadavg%                                                                                                                                                         \n"
    "                                                                                                                                                                   \n"
    "                                                                                                                                                                   \n"
    "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
    "                                                                                                                                                                   \n"
    "  %stderr%                                                                                                                                                         \n"
    "  %stderr%                                                                                                                                                         \n"
    "  %stderr%                                                                                                                                                         \n"
    "  %stderr%                                                                                                                                                         \n"
    "  %stderr%                                                                                                                                                         \n"
    "                                                                                                                                                                   \n"
    "                                                                                                                                                                   \n"
    "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
    "                                           [%QUIT(Q)%]         [%START_ALL(S)%|%KILL_ALL(K)%|%LOG_ALL(L)%]  %_LOG_LADARS_(A)%\n";

  int i,j;
  char temp[8192];
  char buf[1024];
  char buf2[1024];
  ProcessData *proc;

  // Initialize the console template
  memset(temp, 0, sizeof(temp)); 
  strcpy(temp, header);

  // Loop through the processes and create corresponding entries in
  // the template.
  int atLeastHalf=numProcs/2+numProcs%2;
  for (i = 0; i < this->numProcs/2; i++)
  {
    // Get the first column
    if(i<numSNProcs)
      snprintf(buf,sizeof(buf), body, i, i, i, i, i, i, i);
    else
      snprintf(buf,sizeof(buf), body2, i, i, i, i, i, i);

    // Get the second column
    j=i+atLeastHalf;
    if(j<numSNProcs)
      snprintf(buf2,sizeof(buf2), body, j, j, j, j, j, j, j);
    else
      snprintf(buf2,sizeof(buf2), body2, j, j, j, j, j, j);
    
    // connect the 2 columns together
    strcat(buf, " ");
    strcat(buf,buf2);
    strcat(buf,"\n");
    strcat(temp, buf);
  }

  // If there was an odd number of processes, there 
  // should be one more process on the next line
  if(numProcs%2==1)
  {
    i=numProcs/2;
    if(numSNProcs>=numProcs/2+1)
      snprintf(buf,sizeof(buf), body, i, i, i, i, i, i, i);
    else
      snprintf(buf,sizeof(buf), body2, i, i, i, i, i, i);
    strcat(buf,"\n");
    strcat(temp,buf);
  }
  strcat(temp, footer);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, temp);
  
  for(i = 0; i < this->numSNProcs; i++)
  {
    snprintf(buf,sizeof(buf),"%%VW%02d%%",i);
    cotk_bind_button(this->console, buf, " VIEW ", "",
		     (cotk_callback_t) onUserViewModule,this);
    snprintf(buf,sizeof(buf),"%%KL%02d%%",i);
    cotk_bind_button(this->console, buf, " KILL ", "",
                     (cotk_callback_t) onUserKillModule, this);
    snprintf(buf,sizeof(buf),"%%RT%02d%%",i);
    cotk_bind_button(this->console, buf, "RESTRT", "",
                     (cotk_callback_t) onUserRestartModule, this);
    snprintf(buf,sizeof(buf),"%%STR%02d%%",i);
    cotk_bind_button(this->console, buf, " START ", "",
                     (cotk_callback_t) onUserStartModule, this);
    //snprintf(buf,sizeof(buf),"%%SP%02d%%",i);
    //cotk_bind_button(this->console, buf, " STOP ", "",
    //               (cotk_callback_t) onUserStopModule, this);
    snprintf(buf,sizeof(buf),"%%L%02d%%",i);
    cotk_bind_button(this->console, buf, " LOG ", "",
                     (cotk_callback_t) onUserLogModule, this);
  }
  for(; i < this->numProcs; i++)
  {
    snprintf(buf,sizeof(buf),"%%VW%02d%%",i);
    cotk_bind_button(this->console, buf, " VIEW ", "",
		     (cotk_callback_t) onUserViewModule,this);
    snprintf(buf,sizeof(buf),"%%KL%02d%%",i);
    cotk_bind_button(this->console, buf, " KILL ", "",
                     (cotk_callback_t) onUserKillModule, this);
    snprintf(buf,sizeof(buf),"%%RT%02d%%",i);
    cotk_bind_button(this->console, buf, "RESTRT", "",
                     (cotk_callback_t) onUserRestartModule, this);
    snprintf(buf,sizeof(buf),"%%STR%02d%%",i);
    cotk_bind_button(this->console, buf, " START ", "",
                     (cotk_callback_t) onUserStartModule, this);
  }
  	
  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT(Q)%", " QUIT(Q) ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_button(this->console, "%START_ALL(S)%", " START ALL(S) ", "Ss",
                   (cotk_callback_t) onUserStartAll, this);
  //cotk_bind_button(this->console, "%STOP_ALL%", " STOP ALL ", "Tt",
  //               (cotk_callback_t) onUserStopAll, this);
  cotk_bind_button(this->console, "%KILL_ALL(K)%", " KILL ALL(K) ", "Kk",
                   (cotk_callback_t) onUserKillAll, this);
  cotk_bind_button(this->console, "%LOG_ALL(L)%", " LOG ALL(L) ", "Ll",
                   (cotk_callback_t) onUserLogAll, this);
  if(strcmp(this->configuration,"field")==0)
    cotk_bind_button(this->console, "%_LOG_LADARS_(A)%", "[ LOG LADARS(A) ]", "Aa",
		   (cotk_callback_t) onUserLogLadars, this);
  
  // Initialize the display, using a log file if required 
  if (this->cmdOptions.debug_log_path_given)
  {
    char filename[256];
    snprintf(filename, sizeof(filename), "%s/startup.msg",
	     this->cmdOptions.debug_log_path_arg);
    if (cotk_open(this->console, filename) != 0)
      return -1;
  } 
  else 
  {
    if (cotk_open(this->console, NULL) != 0)
      return -1;
  }

  // Create colors
  cotk_set_color_pair(this->console, 7, COLOR_RED, -1);
  cotk_set_color_pair(this->console, 1, -1, COLOR_YELLOW);  
  cotk_set_color_pair(this->console, 2, -1, COLOR_CYAN);  
  cotk_set_color_pair(this->console, 3, -1, COLOR_MAGENTA); 

  // Display some fixed values
  char name[128];
  char status[128];
  char namePlusSpace[128];

  for(i = 0; i < this->numProcs; i++)
  {
    proc = &this->procs[i];    
    snprintf(name,sizeof(name),"%%_desc_%02d%%",i);
    snprintf(namePlusSpace, sizeof(namePlusSpace),"%s                ",proc->name);
    snprintf(status, sizeof(status), "%%_status_%02d%%",i);
    
    cotk_printf(this->console, name, COLOR_PAIR(i%3+1), namePlusSpace);
    cotk_printf(this->console, status, COLOR_PAIR(i%3+1), "                       ");
  }

  return 0;
}


// Finalize console display
int ProcessControl::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Gets the index associated with a given token i.e. %START12% returns 12
int ProcessControl::getTokenIndex(const char *token)
{  
  char tid[8];
  int j = 0;
  
  for(int i = 0; i < (int) strlen(token); i++)
  {
    if(isdigit(token[i]))
    {
      while(isdigit(token[i]))
        tid[j++] = token[i++];
      tid[j++] = 0;
      break;         
    }
  }

  return atoi(tid);
}


// Handle button callbacks
int ProcessControl::onUserQuit(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user quit");
  quit = 1;
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStartAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user start all");
  
  // Re-read the config file incase we have made
  // any changes while process-control was running.
  // Also refresh the console, in case the 
  // number of processes has changed
  if(self->readConfigFiles()!=0)
    return -1;
  if(self->initConsole()!=0)
    return -1;
  
  // If we are starting everything, make sure to 
  // remove mission.log, as we are starting over.
  if(!self->cont) // But only if we are not in "continue" mode
  {
    char cmd[512]; 
    if(self->configuration!=NULL && strcmp(self->configuration,"field")==0)
      snprintf(cmd, sizeof(cmd), "rm %s/i486-gentoo-linux/mission.log",self->binPrefix);
    else
      snprintf(cmd, sizeof(cmd), "rm %s/../src/system-tests/mission.log",self->binPrefix);
    system(cmd);
  }

  // When starting all processes, make sure that the startProcess 
  // function knows that all processes are being restarted
  self->startingAll=true;
  for(int i = 0; i < self->numProcs; i++)
  {
    self->startProcess(i);
  }
  self->startingAll=false;
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserKillAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user kill all");
  for(int i = 0; i < self->numProcs; i++)
  {
    self->killProcess(i);
  }
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserLogAll(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user log all");
  for(int i = 0; i < self->numSNProcs; i++)
  {
    self->sendLogRequest(i);
  }
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserLogLadars(cotk_t *console, ProcessControl *self, const char *token)
{
  MSG("user log ladars");
  for(int i=0; i< self->numSNProcs; i++)
  {
    if(strstr(self->procs[i].name,"adar")!=NULL)
      self->sendLogRequest(i);
  }
  return 0;
}  

// Handle button callbacks
int ProcessControl::onUserLogModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);  
  MSG("user log module-id: %s", self->procs[tid].name);
  self->sendLogRequest(tid);  
  return 0;
}


// Handle button callbacks
int ProcessControl::onUserStartModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user start module-id: %s", self->procs[tid].name);
  self->startProcess(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserRestartModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user restart module-id: %s", self->procs[tid].name);
  self->restartProcess(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserKillModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user kill module-id: %s", self->procs[tid].name);
  self->killProcess(tid);
  return 0;
}

// Handle button callbacks
int ProcessControl::onUserViewModule(cotk_t *console, ProcessControl *self, const char *token)
{
  int tid = getTokenIndex(token);
  MSG("user view module-id: %s", self->procs[tid].name);
  self->viewProcess(tid);
  return 0;
}

// Start process in a detached screen
int ProcessControl::startProcess(int index)
{
  // Before we do anything, let us reload the configuration file to 
  // reflect any changes that might have been made while process control was running
  if(!this->startingAll) //As long as we are not starting all processes
  {
    if(readConfigFiles()!=0)
      return -1;
  }
  
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.quit = false;
  
  // the process has now been started
  proc->started = true;

  // the process has now definitely not been killed
  proc->killed=false;

  // Check if the window is already open
  char windowTest[256];
  snprintf(windowTest, sizeof(windowTest), "%s/../etc/startup/./findScreen.sh %s%d", this->binPrefix, proc->name, skynetKey);
  proc->windowOpen=(system(windowTest)==0);
       
  if(!proc->windowOpen)
  {
    /* Check if ~/pid directory exists */
    struct stat st;
    stringstream pidPath("");
    pidPath << this->binPrefix << "/pid";
    int ret = stat(pidPath.str().c_str(), &st);
    if (errno == ENOENT) {
      MSG("Creating %s folder", pidPath.str().c_str());
      if(mkdir(pidPath.str().c_str(), 0755) != 0)
	MSG("startProcess: cannot create %s folder", pidPath.str().c_str());
    }

    char cmd[1024];
    char *buf;
    // if you are running a process not on this machine
    if(strcmp(proc->host,"localhost")!=0)
    {
      char sshTest[256];
      // the following command will create a screen in the background, 
      // ssh to the specified host, set unlimited size for core files, and run the process
      buf = "screen -d -m -S %s%d ssh -o ConnectTimeout=5 -X -t %s 'bash -i -l -c \"{  cd %s/i486-gentoo-linux; ulimit -c unlimited; ./%s & export LAST_DGC_MODULE_PID=\\$! ; echo \\$LAST_DGC_MODULE_PID > %s/%s ; fg ; echo press enter to close; read ; }\" ; read ' ";
      //buf = "screen -d -m -S %s%d \"\"ssh -o ConnectTimeout=5 -X -t %s \"cd %s/i486-gentoo-linux; ulimit -c unlimited; ./%s & export LAST_DGC_MODULE_PID=$! ; fg ; echo $LAST_DGC_MODULE_PID > pid/%s\"\"; echo press enter to close; read\"";
      snprintf(sshTest,sizeof(sshTest), "%s/../etc/startup/./sshTest.sh %s",this->binPrefix, proc->host);

      // check to see if it is possible to ssh
      if(system(sshTest)==0)
      {
	snprintf(cmd, sizeof(cmd), buf, proc->name, skynetKey,
		 proc->host, this->binPrefix, proc->cmd, pidPath.str().c_str(), proc->name);
	MSG("running: %s", cmd);
	system(cmd);
	proc->windowOpen=true;
	updateWindowStatus(index, true);
	if(proc->autoOpen)
	  viewProcess(index);
      }
      else
      {
	ERROR("cannot ssh to: %s", proc->host);
	updateWindowStatus(index, false);
      }
    }
    else
    {
      //buf="screen -d -m -S %s%d \"\"%s/./Drun %s & export LAST_DGC_MODULE_PID=$! ; fg ; echo $LAST_DGC_MODULE_PID > pid/%s \"; echo press enter to close; read\"";
      buf="screen -d -m -S %s%d bash -i -l -c \"{ cd - ; %s/./Drun %s & export LAST_DGC_MODULE_PID=\\$! ; echo \\$LAST_DGC_MODULE_PID > %s/%s ; fg ; echo press enter to close; read ; }\"";

      snprintf(cmd, sizeof(cmd), buf, proc->name, skynetKey, this->binPrefix, proc->cmd, 
	       pidPath.str().c_str(), proc->name);
      system(cmd);
      MSG("running: %s",cmd);

      proc->windowOpen=true;
      updateWindowStatus(index, true);
      if(proc->autoOpen)
	viewProcess(index);
    }
  }
  else
    MSG("%s already open", proc->name);
  
  return 0;
}

// restarts given process
int ProcessControl::restartProcess(int index)
{
  killProcess(index);
  sleep(1);
  startProcess(index);
  return 0;
}

// kills given process
int ProcessControl::killProcess(int index)
{
  ProcessData *proc;

  proc = &this->procs[index];  
  proc->request.quit = false;
  
  if(proc->windowOpen || proc->running || proc->started)
  {
    proc->killed=true;
    MSG("killing: %s", proc->name);
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "%s/../etc/startup/killScreen.sh %s%d", binPrefix, proc->name, skynetKey);
    system(cmd);
    proc->running=false;
    proc->windowOpen=false;
    proc->started=false;
    updateWindowStatus(index, false);
  }
  else
    ERROR("%s not open, cannot kill", proc->name);

  return 0;
}

// Toggle the logging state
int ProcessControl::sendLogRequest(int index)
{
  ProcessData *proc;
  
  proc = &this->procs[index];  
  proc->request.moduleId = proc->id;
  proc->request.enableLog = !proc->request.enableLog;

  if (sensnet_write(this->sensnet, SENSNET_METHOD_CHUNK, proc->id, SNprocessRequest,
                    0, sizeof(proc->request), &proc->request) != 0)
    return MSG("unable to send log request");

  return 0;
}


// View Process that was in a detached screen
int ProcessControl::viewProcess(int index)
{
  ProcessData *proc;
  
  proc = &this->procs[index];
  proc->request.quit = false;

  if(!proc->windowOpen)
    ERROR("%s window not open, cannot view", proc->name);
  else
  {  
    char cmd[1024];
    char *buf;

    // Set the window size before you open the process
    // TODO: Standardize Window Size (80x24 would be reaaaly nice)
    char *windowSize;
    buf="xterm%s -rightbar -sb -T %s -e screen -r %s%d &";
    switch(proc->id)
    {
    case MODtrafficplanner: windowSize=" -geometry 80x30"; break;
    case MODmissionplanner: windowSize=" -geometry 80x27"; break;  
    case SNtrajfollower: windowSize=" -geometry 80x34"; break;
    default: windowSize="";
    }
  
    snprintf(cmd, sizeof(cmd),buf, windowSize, proc->name, proc->name, skynetKey);
  
    MSG("viewing: %s",cmd);
    system(cmd);
  }
  
  return 0;
}

// Check for changes to the process state
int ProcessControl::updateProcessStatus()
{
  int i;
  int status;
  int blobId;
  ProcessData *proc;
  ProcessResponse response;
  char token[128];  
  
  // Wait for new updates (with timeout)
  status = sensnet_wait(sensnet, 100);
  if (status != 0 && status != ETIMEDOUT)
    return ERROR("wait failed %s", strerror(status));

  // Update all entries to show time since last heartbeat
  for (i = 0; i < this->numSNProcs; i++)
  {
    proc = &this->procs[i];

    memset(&response, 0, sizeof(response));
    if (proc->blobType == SNprocessResponse)
    {
      // Read process response message
      if (sensnet_read(sensnet, proc->id, SNprocessResponse,
                       &blobId, sizeof(response), &response) != 0)
        return -1;
    }
    else
    {
      // For non-process messages, take a peek at the timestamp
      uint64_t time_stamp;
      if (sensnet_peek_ex(sensnet, SENSNET_SKYNET_SENSOR, proc->blobType,
                          &blobId, NULL, &time_stamp) != 0)
        return -1;
      response.timestamp = time_stamp;
    }

    // if blobId is < 0, process hasn't 
    // started yet, don't print anything out
    if (blobId < 0)
      continue;
    
    
    if (this->console)
    {
      double time, lat;
      char size[64];

      // Latency (i.e., how long since we last saw a response).
      // We have to be careful with the unsigned type conversion.
      lat = ((double) DGCgettime() - (double) response.timestamp) / 1e6;

      // Process timestamp
      time = fmod((double) response.timestamp / 1e6, 1000);
      
      // Log size in human-readable format
      if (response.logSize < 1024)
	snprintf(size, sizeof(size), "      ");
      else
        snprintf(size, sizeof(size), "%4dMB", response.logSize / 1024);
      
      
      // get the correct token to change for this index
      snprintf(token, sizeof(token), "%%_status_%02d%%", i);
      

      // if latency is less than 1, everything is "ok", 
      // if not, we will consider the process dead
      if(lat < 1.0)
      {
	// if the process was just started, and now we know 
	// it is running, set running to true and started to false, 
	// so that we can attempt to start it again later if needed
	if(proc->started)
	  proc->started=false;
	proc->running=true;
      }
      else
	proc->running=false;

      
      // Now it's time to paint the screen, also try and autorestart if neccessary
      if (proc->running)
      	cotk_printf(this->console, token, COLOR_PAIR(i%3+1), "%s     %s        ", size, "GOOD");
      else
      {
	cotk_printf(this->console, token, COLOR_PAIR(7), "%s     %s (%3.0f)", size, " BAD", lat);

	// If process is not running, has not yet been started, 
	// and auto restart is on, restart the process
	// Don't auto restart if you killed everything at once
	if(!proc->started && autoRestart && !proc->killed)
	{
	  stringstream pidFileName("");
	  pidFileName << this->binPrefix << "/pid/" << proc->name;
	  ifstream pidFile;
	  string line;
	  string pid;
	  pidFile.open(pidFileName.str().c_str(), ios::in);
	  
	  if(!pidFile)
	    return ERROR("updateProcessStatus: Cannot open PID file for %s", proc->name);

	  getline(pidFile, line);
	  istringstream lineStream(line, ios::in);
	  lineStream >> pid;
	  char cmd[1024];

	  if(strcmp(proc->host,"loaclhost")!=0)
	    snprintf(cmd, sizeof(cmd), "ssh -o ConnectTimeout=5 %s ps aux | grep %s >/dev/null 2>&1", proc->host, pid.c_str());
	  else
	    snprintf(cmd, sizeof(cmd), "ps aux | grep %s >/dev/null 2>&1", pid.c_str());
	  
	  // If grep is not successful, then planner actually died.
	  if(system(cmd)!=0) {
	    MSG("automatically restarting %s",proc->name);
	    restartProcess(i);
	  }

	  /*
	  if(proc->id!=MODtrafficplanner)
	  {
	    MSG("automatically restarting %s",proc->name);
	    restartProcess(i);
	  }
	  else // if this is planner that has latency issues, lets first make sure it's actually dead.
	  {
	    char cmd[128];
	    if(strcmp(proc->host,"loaclhost")!=0)
	      snprintf(cmd, sizeof(cmd), "ssh -o ConnectTimeout=5 %s ps aux | grep planner >/dev/null 2>&1", proc->host);
	    else
	      snprintf(cmd, sizeof(cmd), "ps aux | grep planner >/dev/null 2>&1");
	    
	    // If grep is not successful, then planner actually died.
	    if(system(cmd)!=0)
	    {
	      MSG("automatically restarting planner");
	      restartProcess(i);
	    }
	  }
	  */
       	}
      }
    }
  }
  
  return 0;
}

// This function will attempt to find the 
// load average of each host every 10 cycles
void ProcessControl::updateLoadAverages()
{  
  const int freq=10;
  
  if(ticker%(freq/2)==0)
  {
    // in this cycle, we will run a shell 
    // script to get all of the load averages
    if(ticker%freq==0)
    {
      // only run this shell script if the file 
      // it generated has already been read in
      if(fileRead)
      {
	char cmd[512];
	// run loadAverage.sh to get loadAverages and write it to a file
	snprintf(cmd, sizeof(cmd), "%s/../etc/startup/./loadAverage.sh ",binPrefix);
	for(int i=0; i<numHosts; i++)
	{
	  strcat(cmd, hosts[i]);
	  strcat(cmd, " ");
	}
	strcat(cmd, ">/dev/null 2>&1 &");
	system(cmd);
	fileRead=false;
      }
    }
    else
    {
      // In this cycle, we will try and read the file containing the load averages
      char output[1024]="";
      char temp[128];
      ifstream inFile;
      char fileName[512];
      snprintf(fileName, sizeof(fileName), "%s/../etc/startup/loads", binPrefix);
      
      inFile.open(fileName);
      
      // if we were able to open the file (file exists)
      if(inFile.is_open())
      {
	int i=0;
	while(!inFile.eof() && i<numHosts)
	{
	  inFile.getline(temp,sizeof(temp));
	  load[i]=atof(temp);
	  snprintf(temp, sizeof(temp), "%s: %2.2f    ", hosts[i], load[i]);
	  strcat(output,temp);
	  if(i%9==8)
	    strcat(output,"\n ");
	  i++;
	}
	if(i==numHosts)
	{
	  // print out the load averages to the screen
	  cotk_printf(this->console, "%loadavg%", A_NORMAL,output);
	  fileRead=true;
	}
      }
      inFile.close();
    }
  }

  // Increment ticker, reset after a certain value
  if(ticker==freq)
    ticker=1;
  else
    ticker++;
  
}

// This function merely sets the window status for 
// non SNProcs to OPEN or blank
void ProcessControl::updateWindowStatus(int index, bool open)
{
  char token[16];
  
  // get the correct token to change for this index
  snprintf(token, sizeof(token), "%%s%02d%%", index);

  if(open)
    cotk_printf(this->console, token, COLOR_PAIR(index%3+1), "           OPEN        ");
  else
    cotk_printf(this->console, token, COLOR_PAIR(index%3+1), "                       ");
}

//********************************
// Main program thread (finally!)
//********************************
int main(int argc, char **argv)
{
  // catch Ctrl-C and SIGTERM, handle them cleanly
  signal(SIGINT, sigintHandler);
  signal(SIGTERM, sigintHandler);
  signal(SIGHUP, sigintHandler);

  ProcessControl *proctl; 

  // Create module
  proctl = new ProcessControl();
  assert(proctl);
  
  // Parse command line options
  if (proctl->parseCmdLine(argc, argv) != 0)
    return -1;
  
  //set permissions for shell scripts
  char cmd[512];
  
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/sshTest.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/killScreen.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/findScreen.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/yn.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/loadAverage.sh",proctl->binPrefix);
  system(cmd);
  snprintf(cmd, sizeof(cmd), "chmod 755 %s/../etc/startup/removeLoads.sh",proctl->binPrefix);
  system(cmd);
  
  
  
  // TODO: get this working, or remove it
  // check to see if time is synchronized
  if(proctl->numHosts>1) // if more than one host
  {
    MSG("should be checking time synchronization here");
    if(false) // if time is not synchronized
    {
      MSG("WARNING: time is not synchronized on other machines. Continue? (y or n)");

      snprintf(cmd, sizeof(cmd), "%s/../etc/startup/./yn.sh", proctl->binPrefix);

      // return whether user selected yes or not
      if(!system(cmd)==0) // if user selected no
      {
	cmdline_parser_free(&proctl->cfgFileOptions);
	cmdline_parser_free(&proctl->cmdOptions);
	delete proctl;
	MSG("program exited cleanly");
	exit(0);
      }
    }
  }

  if (proctl->initSensnet() != 0)
    return -1;
    
  // Initialize console
  if (!proctl->cmdOptions.disable_console_flag)
  {
    if (proctl->initConsole() != 0)
      return -1;
  }
  
  // Before starting any processes, start checking for host load averages
  proctl->fileRead=true;
  proctl->updateLoadAverages();

  
  // start all processes if it was requested
  if(proctl->startall)
    ProcessControl::onUserStartAll(proctl->console, proctl, "");

  // remove mission.log, so that mplanner may start anew
  else if(!proctl->cont) // if we are not in "continue" mode
  {
    if(proctl->configuration!=NULL && strcmp(proctl->configuration,"field")==0)
      snprintf(cmd, sizeof(cmd), "rm %s/i486-gentoo-linux/mission.log",proctl->binPrefix);
    else
      snprintf(cmd, sizeof(cmd), "rm %s/../src/system-tests/mission.log",proctl->binPrefix);
    system(cmd);
  }

  uint64_t time, endtime;
  while (quit==0)
  {
    // update process status and console window
    if (proctl->updateProcessStatus() != 0)
      break;
    if (proctl->console)
      cotk_update(proctl->console);
    // update the load averages
    proctl->updateLoadAverages();

    //********************************************************************
    // The following portion of the code waits for a 
    // 10th of a second before proceeding. If there is keyboard input, 
    // then that is handled immediately, and then the wait for 
    // the 10th of a second continues
    time = DGCgettime();
    endtime = time + 100000L;
    do
    {
      if (uwait(endtime - time) == 1)
      {
	if (proctl->console)
	  cotk_update(proctl->console);
	time = DGCgettime();
	//MSG("DBG: %g: keypressed, interrupted sleep", double(time)/1e6);
      } 
      else break;
    }while (endtime > time);
    //MSG("DBG: %g, timeout expired, processing", double(time)/1e6);
    //*********************************************************************
  }

  // Clean up
  ProcessControl::onUserKillAll(proctl->console, proctl, "");
  proctl->finiConsole();
  proctl->finiSensnet();

  // remove the file holding the load averages
  snprintf(cmd, sizeof(cmd), "%s/../etc/startup/./removeLoads.sh &", proctl->binPrefix);
  system(cmd);

  cmdline_parser_free(&proctl->cfgFileOptions);
  cmdline_parser_free(&proctl->cmdOptions);
  delete proctl;
  MSG("program exited cleanly");
  
  return 0;
}

// Handle Interrupts
void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit >= 2)
    abort();
  quit++;
}


//*******************************************************************
// following is credited to Daniele Tamino
// I shamelessly coppied the following function from him
//*******************************************************************

// wait for a keypress or for the timeout to pass, whichever comes first.
// timeout is in microseconds.
// Returns 0 if timeout expires, or != 0 if a key is pressed.
int uwait(uint64_t timeout)
{
    uint64_t endTime = DGCgettime() + timeout;
    if (timeout > 10000L) // one hundreth of a second
    {
        long hundreths = timeout / 10000L;
        long msecs = hundreths * 10;
        // wait for a keypress
        timeout(msecs);
        int c = getch();
        timeout(0);
        if (c != ERR) {
            ungetch(c);
            return 1;
        }
    }
    uint64_t now = DGCgettime();
    if (endTime > now)
        usleep(endTime - now);
    return 0;
}
