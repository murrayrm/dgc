#ifndef GRAPHUPDATER_HH_
#define GRAPHUPDATER_HH_

#include <frames/pose3.h>
#include <map/Map.hh>
#include <frames/point2.hh>
#include <frames/mat44.h>
#include <interfaces/VehicleState.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/GraphPlanner.hh>
#include <temp-planner-interfaces/CmdArgs.hh>

#include "Quadtree.hh"
#include "GraphUtils.hh"

class GraphUpdater {

public: 

  static int init(Graph_t** graph, Map* map);
  static void destroy(Graph_t* graph);
  static GraphPlanner* getGraphPlanner();
  static Graph* getGraph();
  static Err_t genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState);
  static Err_t genVehicleSubGraph(Graph_t *graph, StateProblem_t &problem, VehicleState &vehState, ActuatorState &actState);
  static Err_t updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map);
  static void display(int sendSubgroup, Graph_t* graph, VehicleState vehState);
private :
  static Graph *underlyingGraph;
  static GraphPlanner *graphPlanner;
  static CMapElementTalker meTalker;
  static Quadtree quadtree;

};

#endif /*GRAPHUPDATER_HH_*/




