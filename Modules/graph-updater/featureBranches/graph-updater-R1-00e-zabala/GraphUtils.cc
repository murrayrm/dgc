#include "GraphUtils.hh"

#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

#define MAXSTEER 45
#define MAXTURN 90

// Getter mehtod for vehicle properties (kinematics)
void GraphUtils::getPlannerKin( PlannerKinematics *kin_pt ){
  kin_pt->wheelBase = VEHICLE_WHEELBASE;
  kin_pt->maxSteer = MAXSTEER * M_PI/180;
  kin_pt->maxTurn = MAXTURN * M_PI/180;
}

// Getter method for vehicle properties (constraints)
void GraphUtils::getPlannerConstraints( PlannerConstraints *planConst_pt ){
  planConst_pt->enableReverse = 0;
  planConst_pt->centerCost = 1000;
  planConst_pt->headOnCost =   100000;
  planConst_pt->offRoadCost =   10000;
  planConst_pt->reverseCost =   10000;
  planConst_pt->changeCost =    50000;
  planConst_pt->obsCost =     1000000;
  planConst_pt->carCost = 0;
}


// Load an RNDF and initialize the graph.
int GraphUtils::loadRndf( Graph_t *graph, const char *filename )
{
  std::RNDF *rndf;
    
  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);

  // Load RNDF from file
  if (!rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);

  if (rndf->getNumOfSegments() <= 0)
    return ERROR("RNDF has no segments");

  // Create lane nodes
  if (GraphUtils::genLanes(graph, rndf) != 0)
    return -1;      

  // Create turn maneuvers
  if (GraphUtils::genTurns(graph,rndf) != 0)
    return -1;

  // Create lane-change nodes
  if ( GraphUtils::genChanges(graph,rndf) != 0 )
    return -1;

  // Create k-turn nodes. Not implemented at this time.
  // if (GraphUtils::genKTurns() != 0)
  //   return -1;

  // Fix the static portion of the map so we can add volatile bits
  // for the vehicle.
  graph->freezeStatic();

  // Clean up
  delete rndf;

  MSG( "created %d nodes %d arcs",
       graph->getNodeCount(),graph->getArcCount() );
  
  return 0;
}

int GraphUtils::genVehicleSubGraph( Graph_t *graph,const VehicleState *vehState,const ActuatorState *actState )
{
  int i;
  GraphNode *nodeA, *nodeB;
  vec3_t p;
  double dm;
  GraphUtils::PlannerKinematics kin;
  GraphUtils::PlannerConstraints cons;

  // Clear the volatile portion of the graph
  graph->clearVolatile();
    
  // Create vehicle node 
  nodeA = graph->createNode();
  if (!nodeA)
    return ERROR("unable to create node; try increasing maxNodes");
  nodeA->type = GRAPH_NODE_VOLATILE;    

  // Remember the node for later use.  
  graph->vehicleNode = nodeA;

  // Set the node pose
  nodeA->pose.pos = vec3_set(vehState->utmNorthing,
                             vehState->utmEasting,
                             0);
  nodeA->pose.rot = quat_from_rpy(vehState->utmRoll,
                                  vehState->utmPitch,
                                  vehState->utmYaw);

  // Construct transforms
  pose3_to_mat44d(nodeA->pose, nodeA->transGN);
  mat44d_inv(nodeA->transNG, nodeA->transGN);

  // Compute the current steering angle for maneuver generation
  GraphUtils::getPlannerKin( &kin );
  nodeA->steerAngle = actState->m_steerpos * kin.maxSteer;

  // Set RNDF info
  GraphUtils::genVolatileNode( graph,nodeA );

  int err;
  int numFeasible = 0;
  int numUnfeasible = 0;
  
  // Find destination nodes on the static graph.
  //
  // TODO: This should really be checking things like whether or not
  // lanes are adjacent, and also doing a quick rejection of unfeasible
  // destinations (e.g., on distance and minimum turning radius).    
  for (i = 0; i < graph->getNodeCount(); i++)
  {
    nodeB = graph->getNode(i);
    if (nodeB == nodeA)
      continue;

    // Must be lane/turn nodes 
    if (!(nodeB->type == GRAPH_NODE_LANE ||
          nodeB->type == GRAPH_NODE_TURN ||
          nodeB->type == GRAPH_NODE_KTURN))
      continue;
          
    // Nodes must be reasonably close, but not too close.
    dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
    if (dm < 2 || dm > 20) // MAGIC
      continue;

    // The destination must be ahead of the source
    p = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
    if (p.x < 0)
      continue;

    if (true)
    {
      // Generate the maneuver from A to B
      err = genManeuver( graph,GRAPH_NODE_VOLATILE,nodeA,nodeB,kin.maxSteer );
      if (err == ENOMEM) 
        break;
      if (err == 0)
        numFeasible += 1;
      else
        numUnfeasible += 1;
    }

    GraphUtils::getPlannerConstraints( &cons );
    if (cons.enableReverse)
    {
      // Generate the maneuver from B to A (for driving in reverse)
      err = genManeuver( graph,GRAPH_NODE_VOLATILE,nodeB,nodeA,kin.maxSteer );
      if (err == ENOMEM)
        break;
      if (err == 0)
        numFeasible += 1;
      else
        numUnfeasible += 1;
    }
  }

  //MSG("feasable ratio %d : %d (%0.f%%)",
  //    numFeasible, numUnfeasible, 100.0 * numFeasible / (numFeasible + numUnfeasible));
  
  return true;
}


int GraphUtils::genVolatileNode( Graph_t *graph,GraphNode *node )
{
  GraphNode *lanel;
  double ax, ay, bx, by;

  // Get the nearest lane or turn element.
  lanel = graph->getNearestNode( node->pose.pos,            
                                 GRAPH_NODE_LANE | GRAPH_NODE_TURN,
                                 10);                  // MAGIC
  
  if (!lanel)
    return 0;
  
  // Compute node position relative to lane element
  ax = node->pose.pos.x;
  ay = node->pose.pos.y;
  bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
  by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

  // Record distance from lane centerline
  node->centerDist = sqrtf(bx * bx + by * by);

  // See if the node is in the lane in a strict euclidean sense
  if (bx > -lanel->laneLength/2 && bx < +lanel->laneLength/2 &&
      by > -lanel->laneWidth/2 && by < +lanel->laneWidth/2)
  {
    // Assign RNDF data
    node->segmentId = lanel->segmentId;
    node->laneId = lanel->laneId;
    node->isEntry = lanel->isEntry;
    node->isExit = lanel->isExit;
    node->isStop = lanel->isStop;
  }

  return 0;
}

// Generate a maneuver linking two nodes
int GraphUtils::genManeuver( Graph_t *graph,int nodeType,GraphNode *nodeA,GraphNode *nodeB,double maxSteer )
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseB;
  VehicleConfiguration configA, config;
  double roll, pitch, yaw;
  double stepSize, dm, s;
  int i, numSteps;
  double theta;
  bool feasible;
  GraphNode *src, *dst;
  GraphArc *arc;
  PlannerKinematics kin;

  /* Define the vehicle properties. */
  // TODO:
  assert(nodeA);
  assert(nodeB);
 
  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  // Final vehicle pose
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.theta = yaw;

  //MSG("man %f %f : %f %f", poseA.x, poseA.y, poseB.x, poseB.y);

  // Vehicle properties
  GraphUtils::getPlannerKin( &kin );
  vp = maneuver_create_vehicle( kin.wheelBase, maxSteer );
  
  // Create maneuver object
  mp = maneuver_config2pose(vp, &configA, &poseB);

  // Some maneuvers may not be possible, so trap this.
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // MAGIC
  stepSize = 1.0;
  
  // Distance between nodes
  dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / stepSize);

  // Dont do very, very short maneuvers.
  if (numSteps < 1)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  //MSG("%f %f %f : %f %f %f",
  //    poseA.x, poseA.y, poseA.theta, poseB.x, poseB.y, poseB.theta);

  // Check for unfeasable maneuvers. Note the iteration includes the final
  // step in the trajectory (we check 0 to 1 inclusive), to trap the case
  // where the final step has a huge change in theta.
  feasible = true;
  theta = configA.theta;
  for (i = 1; i < numSteps + 1; i++) 
  {
    s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    //fprintf(stdout, "config %d %f %f %f %f %f\n",
    //        i, s, config.x, config.y, config.theta, config.phi);
    
    // Discard turns outside the steering limit
    if (fabs(config.phi) > vp->steerlimit)
      feasible = false;

    // Discard big changes in yaw that occur too fast to be
    // caught by the steering limit test at a course step size.
    if (acos(cos(config.theta - theta)) > 45 * M_PI/180) // MAGIC
      feasible = false;

    theta = config.theta;
  }

  if (!feasible)
  {
    maneuver_free(mp);
    free(vp);
    return -1;      
  }

  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a maneuver node
    dst = graph->createNode();
    if (!dst)
    {
      //ERROR("unable to create node; try increasing maxNodes");
      return ENOMEM; 
    }

    // Fill out basic node data
    dst->type = nodeType;
    dst->direction = +1;
    dst->pose.pos = vec3_set(config.x, config.y, 0);
    dst->pose.rot = quat_from_rpy(0, 0, config.theta);
    dst->steerAngle = config.phi;
    pose3_to_mat44d(dst->pose, dst->transGN);
    mat44d_inv(dst->transNG, dst->transGN);

    // If we are creating turns, create the lane boundaries
    // and update the spatial index.
    if (nodeType == GRAPH_NODE_TURN)
    {
      dst->laneLength = stepSize * 1.35; // MAGIC HACK
      dst->laneWidth = (1 - s) * nodeA->laneWidth + s * nodeB->laneWidth;
      dst->laneRadius = sqrtf(pow(dst->laneLength,2) + pow(dst->laneWidth,2)) / 2;
      graph->setNodePose(dst, dst->pose);
    }
    
    // If we are generating a lane change, set the
    // RNDF data and center-line distance.
    if (nodeType == GRAPH_NODE_CHANGE)
    {
      genChangeNode( graph,dst );
      assert(dst->segmentId == nodeA->segmentId && dst->segmentId == nodeB->segmentId);
      assert(dst->laneId == nodeA->laneId || dst->laneId == nodeB->laneId);
    }
      
    // If we are generating volatile, set the RNDF data and
    // center-line distance.
    if (nodeType == GRAPH_NODE_VOLATILE)
      GraphUtils::genVolatileNode( graph,dst );
    
    // Create an arc from the previous node to the new node
    arc = graph->createArc(src->index, dst->index);
    if (!arc)
    {
      //ERROR("unable to create arc; try increasing maxArcs");
      return ENOMEM;
    }
    arc->type = GRAPH_ARC_NODE;

    src = dst;
  }

  // Create an arc to the final node
  dst = nodeB;
  arc = graph->createArc(src->index, dst->index);
  if (!arc)
  {
    //ERROR("unable to create arc; try increasing maxArcs");
    return ENOMEM;
  }
  arc->type = GRAPH_ARC_NODE;
  
  maneuver_free(mp);
  free(vp);
  
  return 0;
}

// Set the RNDF data on a change node
int GraphUtils::genChangeNode( Graph_t *graph,GraphNode *node )
{
  GraphNode *lanel;
  double ax, ay, bx, by;
      
  // Get the nearest lane.
  lanel = graph->getNearestNode( node->pose.pos,GRAPH_NODE_LANE,10 ); // MAGIC
  assert(lanel);

  // Compute node position relative to lane element
  ax = node->pose.pos.x;
  ay = node->pose.pos.y;
  bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
  by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

  // Record distance from lane centerline
  node->centerDist = sqrtf(bx * bx + by * by);
  node->segmentId = lanel->segmentId;
  node->laneId = lanel->laneId;      
  return 0;
}
// Generate lane nodes
int GraphUtils::genLanes( Graph_t *graph,std::RNDF *rndf )
{
  int sn, ln;
  std::Segment *segment;
  std::Lane *lane;

  // Iterate through segments and lanes, adding intermediate
  // nodes between waypoints in the same lane.
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];

      // Generate nodes for driving with traffic
      if (GraphUtils::genLane( graph,lane, +1 ) != 0)
        return -1;
      if (GraphUtils::genLaneTangents( graph,lane ) != 0)
        return -1;

      // Generate nodes for driving against traffic
      if (GraphUtils::genLane( graph,lane, -1 ) != 0)
        return -1;
      if (GraphUtils::genLaneTangents( graph,lane ) != 0)
        return -1;
    }
  }

  return 0;
}

// Generate a lane
int GraphUtils::genLane( Graph_t *graph,std::Lane *lane, int direction )
{
  int wn;
  int size;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wa, *wb, *wc, *wd, *we, *wf;
  GraphNode *node;
  GraphNode *previousWaypointNode = NULL;
  GraphArc *arc;
  double stepSize;
  int i, numSteps;
  double dx, dy, dm, s;
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseC, poseD, poseS;    
  GraphUtils::PlannerKinematics kin;

  // MAGIC
  // Spacing for interpolated lane nodes
  stepSize = 1.0;

  // Vehicle properties
  GraphUtils::getPlannerKin( &kin );
  vp = maneuver_create_vehicle( kin.wheelBase,1000 ); // MAGIC
  
  // List of all waypoints in the lane
  waypoints = lane->getAllWaypoints();
  size = (int) waypoints.size();
  
  for (wn = 0; wn < size; wn++)
  {
    // Pick out six waypoints for interpolation.  The lane nodes will
    // be generated between c and d.
    wa = wb = wc = wd = we = wf = NULL;
    if (direction > 0)
    {
      if (wn - 2 >= 0)
        wa = waypoints[wn - 2];
      if (wn - 1 >= 0)
        wb = waypoints[wn - 1];
      if (true)
        wc = waypoints[wn + 0];
      if (wn + 1 < (int) waypoints.size())
        wd = waypoints[wn + 1];
      if (wn + 2 < (int) waypoints.size())
        we = waypoints[wn + 2];
      if (wn + 3 < (int) waypoints.size())
        wf = waypoints[wn + 3];
    }
    else if (direction < 0)
    {
      if (wn - 2 >= 0)
        wa = waypoints[size - 1 - (wn - 2)];
      if (wn - 1 >= 0)
        wb = waypoints[size - 1 - (wn - 1)];
      if (true)
        wc = waypoints[size - 1 - (wn + 0)];
      if (wn + 1 < (int) waypoints.size())
        wd = waypoints[size - 1 - (wn + 1)];
      if (wn + 2 < (int) waypoints.size())
        we = waypoints[size - 1 - (wn + 2)];
      if (wn + 3 < (int) waypoints.size())
        wf = waypoints[size - 1 - (wn + 3)];
    }
    else
    {
      assert(false);
    }

    // Create a node for waypoint c
    node = graph->createNode();
    if (!node)
      return ERROR("unable to create node; try increasing maxNodes");
    assert(node);
    node->type = GRAPH_NODE_LANE;
    node->direction = direction;
    node->isWaypoint = (direction > 0);
    node->isEntry = wc->isEntry();
    node->isExit = wc->isExit();
    node->isStop = wc->isStopSign();
    node->isCheckpoint = wc->isCheckpoint();        
    node->segmentId = wc->getSegmentID();
    node->laneId = wc->getLaneID();
    node->waypointId = wc->getWaypointID();
    node->checkpointId = wc->getCheckpointID();
    node->laneLength = stepSize * 1.35; // MAGIC HACK
    node->laneWidth = lane->getLaneWidth() * 0.0254 * 12; // Width is in feet, urgh
    node->laneRadius = sqrtf(pow(node->laneLength,2) + pow(node->laneWidth,2)) / 2;
    node->pose.pos = vec3_set(wc->getNorthing(), wc->getEasting(), 0);

    // Add to spatial index
    graph->setNodePose(node, node->pose);
      
    // Create an arc from the previous node to the new node
    if (wb)
    {
      arc = graph->createArc(node->index - 1, node->index);
      if (!arc)
        return ERROR("unable to create arc; try increasing maxArcs");
      arc->type = GRAPH_ARC_NODE;

      // Create an arc from the previous waypoint to the new waypoint
      if (direction > 0) {
        arc = graph->createArc(previousWaypointNode->index, node->index);
        if (!arc)
          return ERROR("unable to create arc; try increasing maxArcs");
        arc->type = GRAPH_ARC_WAYPOINT;
      }
    }
    // Set waypoint c to be the previous waypoint
    previousWaypointNode = node;

    // Stop here if d is the last waypoint in the lane.
    if (!wd)
      continue;

    // Distance between nodes
    dx = wd->getNorthing() - wc->getNorthing();
    dy = wd->getEasting() - wc->getEasting();
    dm = sqrt(dx*dx + dy*dy);

    // Number of intermediate nodes
    numSteps = (int) ceil(dm / stepSize);

    if (true && wa && wb && we && wf)
    {
      double tba, tcb, tdc, tdb;
      double ted, tfe, tec;

      tba = atan2(wb->getEasting() - wa->getEasting(),
                  wb->getNorthing() - wa->getNorthing());
      tcb = atan2(wc->getEasting() - wb->getEasting(),
                  wc->getNorthing() - wb->getNorthing());
      tdc = atan2(wd->getEasting() - wc->getEasting(),
                  wd->getNorthing() - wc->getNorthing());
      tdb = atan2(wd->getEasting() - wb->getEasting(),
                  wd->getNorthing() - wb->getNorthing());

      ted = atan2(we->getEasting() - wd->getEasting(),
                  we->getNorthing() - wd->getNorthing());
      tfe = atan2(wf->getEasting() - we->getEasting(),
                  wf->getNorthing() - we->getNorthing());
      tec = atan2(we->getEasting() - wc->getEasting(),
                  we->getNorthing() - wc->getNorthing());
      
      // Initial vehicle pose
      poseC.x = wc->getNorthing();
      poseC.y = wc->getEasting();
      poseC.theta = tdb;
 
      // Final vehicle pose
      poseD.x = wd->getNorthing();
      poseD.y = wd->getEasting();
      poseD.theta = tec;

      /*
        if (acos(cos(tcb - tba)) < 15 * M_PI/180 ||
        acos(cos(tfe - ted)) < 15 * M_PI/180) // MAGIC
        {
        poseC.theta = tcb;
        poseD.theta = ted;        
        }
      */

      // UTTER HACK FOR SITE VISIT
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 10)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 10)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 11)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 11)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 10)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 10)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 11)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 11)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 16)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 16)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 17)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 17)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 16)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 16)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 17)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 17)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 22)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 22)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 23)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 23)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 22)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 22)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 2 && wc->getWaypointID() == 23)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 2 && wd->getWaypointID() == 23)
          poseD.theta = tdc;
      }

      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 18)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 18)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 19)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 19)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 18)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 18)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 19)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 19)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 12)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 12)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 13)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 13)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 12)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 12)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 13)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 13)
          poseD.theta = tdc;
      }
      if (direction > 0)
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 6)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 6)
          poseD.theta = tdc;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 7)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 7)
          poseD.theta = ted;
      }
      else
      {
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 6)
          poseC.theta = tdc;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 6)
          poseD.theta = ted;
        if (wc->getSegmentID() == 2 && wc->getLaneID() == 1 && wc->getWaypointID() == 7)
          poseC.theta = tcb;
        if (wd->getSegmentID() == 2 && wd->getLaneID() == 1 && wd->getWaypointID() == 7)
          poseD.theta = tdc;
      }

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseC, &poseD);
    }
    else if (wb && we)
    {      
      // Initial vehicle pose
      poseC.x = wc->getNorthing();
      poseC.y = wc->getEasting();
      poseC.theta = atan2(wd->getEasting() - wb->getEasting(),
                          wd->getNorthing() - wb->getNorthing());

      // Final vehicle pose
      poseD.x = wd->getNorthing();
      poseD.y = wd->getEasting();
      poseD.theta = atan2(we->getEasting() - wc->getEasting(),
                          we->getNorthing() - wc->getNorthing());      

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseC, &poseD);
    }
    else
    {
      mp = NULL;
    }

    // Step along the maneuver
    for (i = 1; i < numSteps; i++)
    {
      s = (double) i / numSteps;  

      if (mp)
      {
        // Get the vehicle configuration (including steer angle) at this step.
        poseS = maneuver_evaluate_pose(vp, mp, s);
      }
      else
      {
        // Linear interpolation
        poseS.x = s * dx + wc->getNorthing();
        poseS.y = s * dy + wc->getEasting();
      }

      //MSG("pose %d %f %f %f %f", i, s, poseS.x, poseS.y, acos(cos(poseS.theta)));
              
      // Create a interpolated node between waypoints b and c
      node = graph->createNode();
      if (!node)
        return ERROR("unable to create node; try increasing maxNodes");      
      node->type = GRAPH_NODE_LANE;
      node->direction = direction;
      node->segmentId = wc->getSegmentID();
      node->laneId = wc->getLaneID();
      node->laneLength = stepSize * 1.35; // MAGIC HACK
      node->laneWidth  = lane->getLaneWidth() * 0.0254 * 12; // Width in in feet, urgh
      node->laneRadius = sqrtf(pow(node->laneLength,2) + pow(node->laneWidth,2)) / 2;
      node->pose.pos.x = poseS.x;
      node->pose.pos.y = poseS.y;
      node->pose.pos.z = 0;

      // Add to spatial index
      graph->setNodePose(node, node->pose);

      // Create an arc frome the previous node to the new node
      arc = graph->createArc(node->index - 1, node->index);
      if (!arc)
        return ERROR("unable to create arc; try increasing maxArcs");
      arc->type = GRAPH_ARC_NODE;
    }

    if (mp)
    {
      maneuver_free(mp);
      mp = NULL;
    }    
  }

  // Clean up
  free(vp);

  return 0;
}


// Generate lane tangents
int GraphUtils::genLaneTangents( Graph_t *graph,std::Lane *lane )
{
  int i;
  GraphNode *na, *nb, *nc;
  double th;

  for (i = 0; i < graph->getNodeCount(); i++)
  {
    nb = graph->getNode(i);
    if (nb->laneId != lane->getLaneID())
      continue;

    na = graph->getPrevLaneNode(nb);
    nc = graph->getNextLaneNode(nb);

    // Crude tangent calculation across nearby points.
    if (na && nc)
    {
      th = atan2(nc->pose.pos.y - na->pose.pos.y,
                 nc->pose.pos.x - na->pose.pos.x);
    }
    else if (na)
    {
      th = atan2(nb->pose.pos.y - na->pose.pos.y,
                 nb->pose.pos.x - na->pose.pos.x);
    }
    else if (nc)
    {
      th = atan2(nc->pose.pos.y - nb->pose.pos.y,
                 nc->pose.pos.x - nb->pose.pos.x);
    }
    else
      assert(false);

    // Set the node orientation
    nb->pose.rot = quat_from_rpy(0, 0, th);

    // Construct transforms
    pose3_to_mat44d(nb->pose, nb->transGN);
    mat44d_inv(nb->transNG, nb->transGN);
  }

  return 0;
}


// Generate turn maneuvers
int GraphUtils::genTurns( Graph_t *graph,std::RNDF *rndf )
{
  int sn, ln, wn, vn;
  std::Segment *segmentA, *segmentB;
  std::Lane *laneA, *laneB;
  std::Waypoint *wpA;
  std::GPSPoint *wpB;
  GraphNode *nodeA, *nodeB;
  GraphArc *arc;
  GraphUtils::PlannerKinematics kin;

  // Iterate through waypoints in the RNDF
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segmentA = rndf->getAllSegments()[sn];    
    for (ln = 0; ln < (int) segmentA->getAllLanes().size(); ln++)
    {
      laneA = segmentA->getAllLanes()[ln];
      for (wn = 0; wn < (int) laneA->getAllWaypoints().size(); wn++)
      {
        wpA = laneA->getAllWaypoints()[wn];

        for (vn = 0; vn < (int) wpA->getEntryPoints().size(); vn++)
        {
          wpB = wpA->getEntryPoints()[vn];
          
          segmentB = rndf->getSegment(wpB->getSegmentID());

          // TODO: handle zones
          if (!segmentB)
            continue;          
          assert(segmentB);
          
          laneB = segmentB->getLane(wpB->getLaneID());
          assert(laneB);

          MSG("turn %d.%d.%d %d.%d.%d",
              wpA->getSegmentID(), wpA->getLaneID(), wpA->getWaypointID(),
              wpB->getSegmentID(), wpB->getLaneID(), wpB->getWaypointID());
          
          nodeA = graph->getNodeFromRndfId(wpA->getSegmentID(),
                                                 wpA->getLaneID(),
                                                 wpA->getWaypointID());
          assert(nodeA);

          nodeB = graph->getNodeFromRndfId(wpB->getSegmentID(),
                                                 wpB->getLaneID(),
                                                 wpB->getWaypointID());

          // Skip turns from zones
          if (!nodeB)
            continue;

          // Create arc to connect waypoints
          arc = graph->createArc(nodeA->index, nodeB->index);
          if (!arc)
            return ERROR("unable to create arc; try increasing maxArcs");
          arc->type = GRAPH_ARC_WAYPOINT;

          // Try generating a feasible maneuver.  If it works, we are
          // done for this turn.
          GraphUtils::getPlannerKin( &kin );
          if ( GraphUtils::genManeuver( graph,GRAPH_NODE_TURN,nodeA,nodeB,kin.maxSteer ) == 0 )
            continue;
          
          // Generate a tight-turn maneuver (which may not be
          // feasible).  This allows the robot to take the turn by
          // veering outside the lane.
          GraphUtils::genManeuver( graph,GRAPH_NODE_TURN, nodeA, nodeB, kin.maxTurn );

          /* REMOVE?
          // Generate a wide-turn maneuver by increasing the turn radius.
          // We are done as soon as we can find a feasible maneuver.
          while (true)
          {
            if (this->genManeuver(GRAPH_NODE_TURN, nodeA, nodeB, this->kin.maxSteer) == 0)
              break;
            nodeA = this->graph->getPrevLaneNode(nodeA);
            if (!nodeA)
              break;
            nodeB = this->graph->getNextLaneNode(nodeB);
            if (!nodeB)
              break;
          }
          */
        }
      }
    }
  }
  
  return 0;
}

// Generate lane-change maneuvers
int GraphUtils::genChanges( Graph_t *graph,std::RNDF *rndf )
{
  int numNodes;
  int na, nb;
  GraphNode *nodeA, *nodeB;
  vec3_t pa, pb;
  double dm;
  GraphUtils::PlannerKinematics kin;

  numNodes = graph->getNodeCount();

  // Consider all pairs of lane nodes
  for (nb = 0; nb < numNodes; nb++)
  {    
    for (na = 0; na < numNodes; na++)
    {
      nodeA = graph->getNode(na);
      nodeB = graph->getNode(nb);

      // Consider lane nodes only
      if (!(nodeA->type == GRAPH_NODE_LANE && nodeB->type == GRAPH_NODE_LANE))
        continue;

      // Look for nodes that are the same segment, but different lanes.
      if (nodeA->segmentId != nodeB->segmentId)
        continue;
      if (nodeA->laneId == nodeB->laneId)
        continue;

      // Space out the lane-changes so we dont overload the graph
      if (!(nodeA->index % 4) == 0) // MAGIC
        continue;

      // Nodes must have similar alignment
      pa = vec3_rotate(nodeA->pose.rot, vec3_set(1, 0, 0));
      pb = vec3_rotate(nodeB->pose.rot, vec3_set(1, 0, 0));
      if (acos(vec3_dot(pa, pb)) > 45 * M_PI/180) // MAGIC
        continue;
      
      // Nodes must be close, but not too close
      dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
      if (!(dm > 15 && dm < 16)) // MAGIC HACK
        continue;

      // The destination must be ahead of the source
      pb = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
      if (pb.x < 0)
        continue;

      //MSG("change %d %d.%d %d %d.%d %d",
      //    na, nodeA->segmentId, nodeA->laneId,
      //    nb, nodeB->segmentId, nodeB->laneId,
      //    this->graph->getNodeCount());
      
      // Generate the lane-change maneuver.  It is generally ok
      // if we cannot make a particular maneuver here.
      GraphUtils::getPlannerKin( &kin );
      if (GraphUtils::genManeuver( graph,GRAPH_NODE_CHANGE, nodeA, nodeB, kin.maxSteer ) == ENOMEM )
        return -1;
    }
  }
    
  return 0;
}

// Generate K-turn maneuvers
int GraphUtils::genKTurns()
{
  return 0;
}
