#ifndef GRAPHUTILS_HH
#define GRAPHUTILS_HH

#include <assert.h>
#include <float.h>
#include <string.h>
#include <errno.h>

#include <alice/AliceConstants.h>
#include <frames/point2.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <rndf/RNDF.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <trajutils/maneuver.h>
#include <map/Map.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Quadtree.hh>

typedef enum {
  UTURN_DARPA = 1,
  UTURN_OBS = 2,
} Uturn_t;

class GraphUtils
{
public:
  struct PlannerKinematics
  {
    /// Distance between front and rear wheels (m).
    double wheelBase;

    /// Maximum steering angle (degrees).  This is the largest angle
    /// that the lowel-level will command.  This value is also used to
    /// scale the measured steering position in ActuatorState (which is
    /// in the domain [-1, +1]) to a steering angle.
    double maxSteer;

    /// Maximum allowed steering angle for generating turning maneuvers.
    /// This can be higher than maxSteer, in which case the vehicle will
    /// attempt turns that are beyond its actual steering capability (and
    /// swing wide as a result).
    double maxTurn;
  };

  public:
  // Initialize the graph from an RNDF file.
  static int loadRndf( Graph_t *graph,const char *filename);
  static int loadRndf( Graph_t *graph,const char *filename, vector<int> segments, vector<int> new_segments);
  static void unload_segment(Graph_t *graph, int segment);
  static int save_segment(Graph_t *graph, string folder, int segment);
  static int load_segment(Graph_t *graph, string folder, int segment);
  static int genVehicleSubGraph( Graph_t *graph,const VehicleState *vehicleState,const ActuatorState *actuatorState , const bool enableReverse);
  static int genVolatileNode( Graph_t *graph,GraphNode *node );
  static int genChangeNode( Graph_t *graph,GraphNode *node, GraphNode *nodeA, GraphNode *nodeB);
  // Generate a maneuver linking two nodes
  static int genManeuver( Graph_t *graph,int nodeType,GraphNode *nodeA,GraphNode *nodeB,double maxSteer );
  static int genLanes( Graph_t *graph,std::RNDF *rndf, int segment);
  static int genLane( Graph_t *graph,std::Lane *lane, int direction );
  static int genRails( Graph_t *graph,std::Lane *lane, int direction);
  static int genLaneTangents( Graph_t *graph,std::Lane *lane, int direction);
  static int genTurns( Graph_t *graph,std::RNDF *rndf, vector<int> segments, vector<int> new_segments);
  static int genChanges( Graph_t *graph,std::RNDF *rndf, int segment);
  static int genZones(Graph_t *graph, std::RNDF *rndf, vector<int> segments, vector<int> new_segments);
  static vector<std::Waypoint*> getSegmentsExitPts(std::RNDF* rndf, int zoneId);
  static double getAngleInRange(double angle);

  // static function calling non-static function
  static GraphNode *getLaneNode(Graph_t *graph, const GraphNode *node, int direction);
  static GraphNode *getChangeNode(Graph_t *graph, const GraphNode *node, int direction);
  static int getChangeCount(Graph_t *graph, const GraphNode *node, int direction);
  static void getChangeNodes(vector<GraphNode *> &nodes, GraphNode **nodeA, GraphNode **nodeB, Graph_t *graph, const GraphNode *node, int direction, int index);

  // useful for updating lane information
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);

  static bool contains(vector<int> segments, int x);

  // REMOVE static point2 delta; 

  static void getPlannerKin(PlannerKinematics *kin_pt);
  static double getNearestObsPoint(point2 &pt, Map *map, VehicleState &vehState, LaneLabel lane, double offset);
  static double getNearestObsInLane(MapElement &me, VehicleState &vehState, Map *map, LaneLabel &lane);
  static double getNearestObsDist(VehicleState &vehState, Map *map, LaneLabel &lane);

  static int genUturnGraph(Graph_t **gUturn, Map *map, pose3_t &finalPose, point2 &ref_pt,
                           GraphNode **nodeVeh, VehicleState &vehState, ActuatorState &actState,
                           const Uturn_t uTurn);

  static int genUturnManeuvers(Graph_t *gUturn, GraphNode **nodeVeh,
                               VehicleState &vehState, ActuatorState &actState);
};

#endif
