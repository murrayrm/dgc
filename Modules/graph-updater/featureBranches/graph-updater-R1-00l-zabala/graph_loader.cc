/**
 * Saves a graph from an RNDF
 */

#include <map/Map.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <rndf/RNDF.hh>
#include <dgcutils/DGCutils.hh>
#include <skynet/skynet.hh>

#include "GraphUpdater.hh"

using namespace std;

int main(int argc, char **args)
{
  if (argc < 4) {
    printf("Usage: %s RNDF_file file loading segment1 [segment2 segment3 ...]\n\nIf segment1 is equal to 0, the segment will be loaded in bulk\n\n", args[0]);
    return -1;
  }

  // Create a map
  Map* map = new Map();
  map->loadRNDF(args[1]);

  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  if (args[2][0] != '0') CmdArgs::load_graph_files = true;
  if (args[3][0] != '0' || argc > 4) CmdArgs::stepbystep_load = true;

  vector<int> segments;
  int segment;
  for (int i=3; i<argc; i++) {
    segment = atoi(args[i]);
    if (segment > 0) segments.push_back(segment);
  }

  double total_time;
  unsigned long long time1, time2;

  /* Create graph */
  Graph_t *graph;
  DGCgettime(time1);
  GraphUpdater::init(&graph, map);
  DGCgettime(time2);
  if (!CmdArgs::stepbystep_load) {
    printf("Bulk loading time is %.2f seconds\n", (time2-time1)/1000000.0);
  }
  vector<int> new_segments;
 
  if (CmdArgs::stepbystep_load) {
    if (args[3][0] == '0') {
      DGCgettime(time1);
      GraphUpdater::load(graph, map, segments);
      DGCgettime(time2);
      total_time += (time2-time1)/1000000.0;
      printf("Bulk loading time (step-by-step) is %.2f seconds\n", (time2-time1)/1000000.0);
    } else {
      for (unsigned int i=0; i<segments.size(); i++) {
        segment = segments[i];
        new_segments.clear();
        new_segments.push_back(segment);
        DGCgettime(time1);
        GraphUpdater::load(graph, map, new_segments);
        DGCgettime(time2);
        total_time += (time2-time1)/1000000.0;
        printf("Loading time (step-by-step) for segment %d is %.2f seconds\n", segment, (time2-time1)/1000000.0);
      }
      printf("Total loading time is %.2f seconds\n", total_time);
    }
  }

  system("rm plot_quad.m");
  graph->quadtree->print();

  /* print arcs */
  ofstream file_out;
  file_out.open("plot_quad.m", ios_base::app);
  for (int i = 0; i<graph->getArcCount(); i++) {
    double xA = graph->getArc(i)->nodeA->pose.pos.x;
    double yA = graph->getArc(i)->nodeA->pose.pos.y;
    double xB = graph->getArc(i)->nodeB->pose.pos.x;
    double yB = graph->getArc(i)->nodeB->pose.pos.y;
    file_out << "plot(["<<xA<<" "<<xB<<"], ["<<yA<<" "<<yB<<"], 'm-'); hold on;" << endl;
  }
  file_out.close();

  /* destroy */
  GraphUpdater::destroy(graph);

  return 0;
}
