#ifndef GRAPHUPDATER_HH_
#define GRAPHUPDATER_HH_

#include <frames/pose3.h>
#include <map/Map.hh>
#include <frames/point2.hh>
#include <frames/mat44.h>
#include <interfaces/VehicleState.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/GraphPlanner.hh>
#include <temp-planner-interfaces/Graph.hh> //included also in PlannerInterfaces.h
#include <planner/CmdArgs.hh>

#include "GraphUtils.hh"

class GraphUpdater {

public: 

  static int init(Graph_t** graph, Map* map);
  static void destroy(Graph_t* graph);
  static GraphPlanner* getGraphPlanner();
  static Graph* getGraph();
  static Err_t genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState);

private :
  static Graph *underlyingGraph;
  static GraphPlanner *graphPlanner;
  static CMapElementTalker meTalker;

  // Initialize the graph from an RNDF file.
  static int loadRndf(const char *filename);


/* New functionality. FZ.*/
public:
  /* First update of the Graph. We update the graph for collision with
   * sensed lanes. The arguments passed are a pointer-to-Graph_t_object, and 
   * a pointer-to-Map reference. */
  static Err_t updateGraphSensedLanes( Graph_t *graph,Map *map );

  /* Second update of the Graph. We update the graph for collision with
   * obstacles. The arguments passed are a pointer-to-Graph_t_object, a
   * the address of the state problem, and a pointer-to-Map reference. */
  static Err_t updateGraphStateProblem( Graph_t *graph,StateProblem_t &problem,Map *map );

  /* Display for the unit test. For now, it displays the graph and the RNDF. */
  static void display(int sendSubgroup, Graph_t* graph, VehicleState vehState);
};

#endif /*GRAPHUPDATER_HH_*/
