#include "GraphUpdater.hh"
#include <planner/Log.hh>
#include <alice/AliceConstants.h>
#include <math.h>

#define MAXNODENUM 50000
#define MAXARCSNUM 50000
#define MAXSTEER 30
#define MAXTURN 45

CMapElementTalker GraphUpdater::meTalker;
Graph* GraphUpdater::underlyingGraph;
/* Method to initialize the Graph. The arguments are a reference to (address of) 
 * a pointer-to-Graph_t_object and a pointer-to-Map. */
int GraphUpdater::init( Graph_t** graph, Map* map )
{
  // Initialize underlying graph
  // Now using Graph as Graph_t -- there are no reasons to do something else.
  *graph = new Graph(MAXNODENUM, MAXARCSNUM);
  
  /* Save a copy of the graph for the getter method. Unsure if necessary. */
  underlyingGraph = *graph;

  /* Load the RNDF from the GraphUtils helper class. */
  if ( GraphUtils::loadRndf( *graph,CmdArgs::RNDF_file.c_str() ) != 0 )
    return -1;

  /* Check to see if the graph contains nodes. Return an error if this node
   * count is 0. */
  if ((*graph)->getNodeCount() == 0)
    return -1;

  // Initialize map element talker
  meTalker.initSendMapElement(CmdArgs::sn_key);
  return 0;
  
  
  // Shift graph to local coordinates
  GraphNode* node;

  /* Sven's hack to force the shifting from global coordinates (as initialized)
   * onto local. */
  (*graph)->gridReady = false;
  for (int i=0; i<(*graph)->getNodeCount(); i++) {
    node = (*graph)->getNode(i);
    node->pose.pos.x -= map->prior.delta.x;
    node->pose.pos.y -= map->prior.delta.y;
    pose3_to_mat44d(node->pose, node->transGN);
    mat44d_inv(node->transNG, node->transGN);
    (*graph)->setNodePose(node, node->pose);
  }

}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete graph;
  return;
}

/* Not to sure if this is necessary. */
Graph* GraphUpdater::getGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph( Graph_t *graph,VehicleState &vehState,ActuatorState &actState )
{
  // Generate subgraph from our current pose to nearby nodes
  if( GraphUtils::genVehicleSubGraph( graph,&vehState,&actState ) );
    return GU_OK;
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 *
 * Temporary:  For each node in the graph if it collides with an obstacle,
 * mark it as being occupied by an obstacle
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  GraphNode *node;
  MapElement *mapEl;
  point2 point, obs_pt, vect;
  point2arr new_geom;

  // Reset the collision flags for all nodes
  for (int i = 0; i < graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }

  // Check each node against each object in the map for a possible
  // collision.
  for (int j = 0; j < (int)map->data.size(); j++) {
    new_geom.clear();
    mapEl = &map->data[j];
    if ((mapEl->type != ELEMENT_OBSTACLE) || (mapEl->type != ELEMENT_VEHICLE))
      continue;

    // Grow obstacle
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);
      vect = obs_pt-mapEl->center; vect = vect / vect.norm();
      new_geom.push_back(mapEl->geometry[k] + vect*(1+VEHICLE_WIDTH/2));
    }

    // Check each node.  If a node is aleady in collision, dont waste
    // time checking it again.
    for (int i = 0; i < graph->getNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->collideObs)
        continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      if (mapEl->type == ELEMENT_OBSTACLE) {
        node->collideObs = new_geom.is_inside_poly(point);
      } else {
        node->collideCar = new_geom.is_inside_poly(point);
        if (node->collideCar) {
          // TODO: change this vel to the velocity along the lane
          node->carVel = sqrt(pow(mapEl->velocity.x,2) + pow(mapEl->velocity.y,2));
        }
      }
    }
  }

  return GU_OK;
}

Err_t GraphUpdater::updateGraphSensedLanes( Graph_t *graph,Map *map ) 
{
#if 0
  /* Pointer to a GraphNode structure. Needed to update the node information. */
  GraphNode *node;
  /* Pointer to a map element. Needed to interface with the map. */
  MapElement *mapEl;
  /* New geometry variable. */
  point2arr new_geom;

  // Reset the collision flags for all nodes
  for (int i = 0; i < graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    node->collideLane = false;
  }
  // Check each node against both lanes in the map for a possible
  // collision.
  // TODO: check against ELEMENT_LINE, ELEMENT_STOPLINE(?).
  for (int j = 0; j < (int)map->data.size(); j++) {
    new_geom.clear();
    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_LANELINE)
      continue;

  }
#endif
  return GU_OK;
}

void GraphUpdater::display(int sendSubgroup, Graph_t* graph, VehicleState vehState)
{
    MapElement me;
    
    // DISPLAY THE GRAPH
    point2 point, vehPos;
    vector<point2> free_points;
    vector<point2> obs_points;
    MapId free_mapId, obs_mapId;
    GraphNode *node;

    double dotProd, dist;
    double heading = vehState.localYaw;
    vehPos.set(vehState.localX, vehState.localY);
    free_mapId = 10000;
    obs_mapId = 11000;

    for (int i=0; i<graph->getStaticNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->type != GRAPH_NODE_LANE && node->type != GRAPH_NODE_TURN && node->type != GRAPH_NODE_VOLATILE) continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
      dist = point.dist(vehPos);
      if ( dotProd>0 && dist<100 ) {
        //if (1) {
        if ( node->collideObs )
          obs_points.push_back(point);
        else
          free_points.push_back(point);
      }
    }

    Map mapdata;
    
    // Send free nodes
    me.setId(free_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(free_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by an obstacle
    me.setId(obs_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_YELLOW,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,sendSubgroup);
}
