#include "GraphUpdater.hh"
#include <planner/Log.hh>
#include <alice/AliceConstants.h>
#include <math.h>

#define MAXNODENUM 50000
#define MAXARCSNUM 50000
#define MAXSTEER 30
#define MAXTURN 45

Graph* GraphUpdater::underlyingGraph;
CMapElementTalker GraphUpdater::meTalker;

int GraphUpdater::init(Graph_t** graph, Map* map)
{
  //Initialize underlying graph
  underlyingGraph = new Graph(MAXNODENUM, MAXARCSNUM);
  // Initialize graph which include the underlying graph and the operations defined on that graph
  // Right now those operations are defined in GraphPlanner
  // TODO: Need to move that out to make it less confusing
  *graph = new GraphPlanner(underlyingGraph);
  
  // Set vehicle properties
  if (true)
    {
      GraphPlannerKinematics kin;
      (*graph)->getKinematics(&kin);
      kin.wheelBase = VEHICLE_WHEELBASE;
      kin.maxSteer = MAXSTEER * M_PI/180;
      kin.maxTurn = MAXTURN * M_PI/180;
      (*graph)->setKinematics(&kin);
    }
  
  
  // Load up the RNDF and initialize the graph
  if ((*graph)->loadRndf(CmdArgs::RNDF_file.c_str()) != 0)
    return -1;
    
  if (underlyingGraph->getNodeCount() == 0)
    return -1;
  
  // Shift graph to local coordinates
  GraphNode* node;
  underlyingGraph->gridReady = false;
  for (int i=0; i<underlyingGraph->getNodeCount(); i++) {
    node = underlyingGraph->getNode(i);
    node->pose.pos.x -= map->prior.delta.x;
    node->pose.pos.y -= map->prior.delta.y;
    pose3_to_mat44d(node->pose, node->transGN);
    mat44d_inv(node->transNG, node->transGN);
    underlyingGraph->setNodePose(node, node->pose);
  }

  // Initialize map element talker
  meTalker.initSendMapElement(CmdArgs::sn_key);
  return 0;
}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete underlyingGraph;
  delete graph;
  return;
}


Graph* GraphUpdater::getUnderlyingGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState)
{
  // Force graph planner to use local coordinates
  vehState.utmNorthing = vehState.localX;
  vehState.utmEasting = vehState.localY;
  vehState.utmAltitude = vehState.localZ;
  vehState.utmYaw = vehState.localYaw;
  // Generate subgraph from our current pose to nearby nodes
  graph->genVehicleSubGraph(&vehState, &actState);

  return GU_OK;
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 *
 * Temporary:  For each node in the graph if it collides with an obstacle,
 * mark it as being occupied by an obstacle
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  GraphNode *node;
  MapElement *mapEl;
  point2 point, obs_pt, vect;
  point2arr new_geom;

  // Reset the collision flags for all nodes
  for (int i = 0; i < underlyingGraph->getNodeCount(); i++) {
    node = underlyingGraph->getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }

  // Check each node against each object in the map for a possible
  // collision.
  for (int j = 0; j < (int)map->data.size(); j++) {
    new_geom.clear();
    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_OBSTACLE)
      continue;

    // Grow obstacle
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);
      vect = obs_pt-mapEl->center; vect = vect / vect.norm();
      new_geom.push_back(mapEl->geometry[k] + vect*(1+VEHICLE_WIDTH/2));
    }

    // Check each node.  If a node is aleady in collision, dont waste
    // time checking it again.
    for (int i = 0; i < underlyingGraph->getNodeCount(); i++) {
      node = underlyingGraph->getNode(i);
      if (node->collideObs)
        continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      node->collideObs = new_geom.is_inside_poly(point);
    }
  }

  return GU_OK;
}

void GraphUpdater::display(int sendSubgroup, Graph_t* graph, VehicleState vehState)
{
    MapElement me;
    
    // DISPLAY THE GRAPH
    point2 point, vehPos;
    vector<point2> free_points;
    vector<point2> obs_points;
    MapId free_mapId, obs_mapId;
    GraphNode* node;

    double dotProd, dist;
    double heading = vehState.localYaw;
    vehPos.set(vehState.localX, vehState.localY);
    free_mapId = 10000;
    obs_mapId = 11000;
    for (int i=0; i<underlyingGraph->getStaticNodeCount(); i++) {
      node = underlyingGraph->getNode(i);
      if (node->type != GRAPH_NODE_LANE && node->type != GRAPH_NODE_TURN && node->type != GRAPH_NODE_VOLATILE) continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
      dist = point.dist(vehPos);
      if (dotProd>0 && dist<25) {
        if (node->collideObs)
          obs_points.push_back(point);
        else
          free_points.push_back(point);
      }
    }

    // Send free nodes
    me.setId(free_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(free_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by an obstacle
    me.setId(obs_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_YELLOW,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,sendSubgroup);
}
