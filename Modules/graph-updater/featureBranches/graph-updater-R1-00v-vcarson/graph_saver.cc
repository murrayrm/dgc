/**
 * Saves a graph from an RNDF
 */

#include <map/Map.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <rndf/RNDF.hh>
#include <dgcutils/DGCutils.hh>
#include <skynet/skynet.hh>

#include "GraphUpdater.hh"

using namespace std;

int main(int argc, char **args)
{
  if (argc < 2) {
    printf("Usage: %s RNDF_file\n\n", args[0]);
    return -1;
  }

  // Create a map
  Map* map = new Map();
  map->loadRNDF(args[1]);

  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  CmdArgs::stepbystep_load = true;

  // Load RNDF
  std::RNDF *rndf;
    
  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);
    
  // Load RNDF from file
  if (!rndf->loadFile(args[1]))
    return -1;
    
  if (rndf->getNumOfSegments() <= 0)
    return -1;

  int segment;
  double total_time;
  for (unsigned int sn = 0; sn < rndf->getAllSegments().size(); sn++) {
    segment = rndf->getAllSegments()[sn]->getSegmentID();

    unsigned long long time1, time2;

    /* Create graph */
    Graph_t *graph;
    GraphUpdater::init(&graph, map);
  
    vector<int> segments;
    segments.push_back(segment);
    DGCgettime(time1);
    GraphUpdater::load(graph, map, segments);
    DGCgettime(time2);
    total_time += (time2-time1)/1000000.0;
    printf("Construction time for segment %d is %.2f seconds\n", segment, (time2-time1)/1000000.0);
    DGCgettime(time1);
    GraphUpdater::save(graph, segments);
    DGCgettime(time2);
    printf("Save time for segment %d is %.2f seconds\n", segment, (time2-time1)/1000000.0);

    /* destroy */
    GraphUpdater::destroy(graph);
  }

  printf("Total construction time is %.2f seconds\n", total_time);

  return 0;
}
