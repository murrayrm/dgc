#include "GraphUpdater.hh"
#include <planner/Log.hh>
#include <alice/AliceConstants.h>
#include <math.h>

#define MAXNODENUM 50000
#define MAXARCSNUM 50000
#define MAXSTEER 45
#define MAXTURN 90

GraphPlanner* GraphUpdater::graphPlanner;
Graph* GraphUpdater::underlyingGraph;
CMapElementTalker GraphUpdater::meTalker;

int GraphUpdater::init(Graph_t** graph, Map* map)
{
  //Initialize underlying graph
  // Now using Graph as Graph_t -- there are no reasons to do something else.
  *graph = new Graph(MAXNODENUM, MAXARCSNUM);

  // Save the underlying graph for vel-planner -- should not be used in the
  // future
  underlyingGraph = *graph;

  // Saving a GraphPlanner object -- should not be used in the future
  graphPlanner = new GraphPlanner(*graph);
  
  // Set vehicle properties
  GraphPlannerKinematics kin;
  graphPlanner->getKinematics(&kin);
  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;
  graphPlanner->setKinematics(&kin);
  
  // Load up the RNDF and initialize the graph
  if (graphPlanner->loadRndf(CmdArgs::RNDF_file.c_str()) != 0)
    return -1;
    
  if ((*graph)->getNodeCount() == 0)
    return -1;
  
  // Shift graph to local coordinates
  GraphNode* node;
  (*graph)->gridReady = false;
  for (int i=0; i<(*graph)->getNodeCount(); i++) {
    node = (*graph)->getNode(i);
    node->pose.pos.x -= map->prior.delta.x;
    node->pose.pos.y -= map->prior.delta.y;
    pose3_to_mat44d(node->pose, node->transGN);
    mat44d_inv(node->transNG, node->transGN);
    (*graph)->setNodePose(node, node->pose);
  }

  // Initialize map element talker
  meTalker.initSendMapElement(CmdArgs::sn_key);
  return 0;
}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete graph;
  delete graphPlanner;
  return;
}


GraphPlanner* GraphUpdater::getGraphPlanner()
{
  return graphPlanner;
}

Graph* GraphUpdater::getGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState)
{
  // Force graph planner to use local coordinates
  vehState.utmNorthing = vehState.localX;
  vehState.utmEasting = vehState.localY;
  vehState.utmAltitude = vehState.localZ;
  vehState.utmYaw = vehState.localYaw;
  // Generate subgraph from our current pose to nearby nodes
  graphPlanner->genVehicleSubGraph(&vehState, &actState);

  return GU_OK;
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 *
 * Temporary:  For each node in the graph if it collides with an obstacle,
 * mark it as being occupied by an obstacle
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  GraphNode *node;
  MapElement *mapEl;
  point2 point, obs_pt, vect;
  point2arr new_geom;

  // Reset the collision flags for all nodes
  for (int i = 0; i < graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }

  // Check each node against each object in the map for a possible
  // collision.
  for (int j = 0; j < (int)map->data.size(); j++) {
    new_geom.clear();
    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_OBSTACLE && mapEl->type != ELEMENT_VEHICLE)
      continue;

    // Grow obstacle
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);
      vect = obs_pt-mapEl->center; vect = vect / vect.norm();
      new_geom.push_back(mapEl->geometry[k] + vect*(1+VEHICLE_WIDTH/2));
    }

    // Check each node.  If a node is aleady in collision, dont waste
    // time checking it again.
    for (int i = 0; i < graph->getNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->collideObs)
        continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      if (mapEl->type == ELEMENT_OBSTACLE)
        node->collideObs = new_geom.is_inside_poly(point);
      else
        node->collideCar = new_geom.is_inside_poly(point);
    }
  }

  return GU_OK;
}

void GraphUpdater::display(int sendSubgroup, Graph_t* graph, VehicleState vehState)
{
    MapElement me;
    
    // DISPLAY THE GRAPH
    point2 point, vehPos;
    vector<point2> free_points;
    vector<point2> obs_points;
    vector<point2> car_points;
    MapId free_mapId, obs_mapId, car_mapId;
    GraphNode* node;

    double dotProd, dist;
    double heading = vehState.localYaw;
    vehPos.set(vehState.localX, vehState.localY);
    free_mapId = 10000;
    obs_mapId = 10001;
    car_mapId = 10002;
    for (int i=0; i<graph->getStaticNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->type != GRAPH_NODE_LANE && node->type != GRAPH_NODE_TURN && node->type != GRAPH_NODE_VOLATILE) continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
      dist = point.dist(vehPos);
      if (dotProd>0 && dist<25) {
        if (node->collideObs)
          obs_points.push_back(point);
        else if (node->collideCar)
          car_points.push_back(point);
        else
          free_points.push_back(point);
      }
    }

    // Send free nodes
    me.setId(free_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(free_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by an obstacle
    me.setId(obs_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_YELLOW,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by a car
    me.setId(car_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_ORANGE,100);
    me.setGeometry(car_points);
    meTalker.sendMapElement(&me,sendSubgroup);
}
