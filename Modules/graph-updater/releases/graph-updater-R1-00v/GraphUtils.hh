#ifndef GRAPHUTILS_HH
#define GRAPHUTILS_HH

#include <assert.h>
#include <float.h>
#include <string.h>
#include <errno.h>

#include <alice/AliceConstants.h>
#include <frames/point2.hh>
#include <frames/pose3.h>
#include <frames/mat44.h>
#include <rndf/RNDF.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <trajutils/maneuver.h>
#include <map/Map.hh>
#include <temp-planner-interfaces/AliceStateHelper.hh>
#include <temp-planner-interfaces/Quadtree.hh>

class GraphUtils
{
public:
  struct PlannerKinematics
  {
    /// Distance between front and rear wheels (m).
    double wheelBase;

    /// Maximum steering angle (degrees).  This is the largest angle
    /// that the lowel-level will command.  This value is also used to
    /// scale the measured steering position in ActuatorState (which is
    /// in the domain [-1, +1]) to a steering angle.
    double maxSteer;

    /// Maximum allowed steering angle for generating turning maneuvers.
    /// This can be higher than maxSteer, in which case the vehicle will
    /// attempt turns that are beyond its actual steering capability (and
    /// swing wide as a result).
    double maxTurn;
  };
  /// @brief Plan constraints.
  ///
  /// These are the weights used to construct plans.  A negative value
  /// inidicates a hard constraint.
  struct PlannerConstraints
  {
    /// Enable driving in reverse.
    bool enableReverse;

    /// Cost multiplier for driving off the lane center-line.
    int centerCost;
    
    /// Cost for driving in an on-coming lane (and risking a head-on
    /// crash). This value should be much larger than laneCost.
    int headOnCost;

    /// Cost for changing lanes.
    int changeCost;  

    /// Cost for drivig off-road
    int offRoadCost;

    /// Cost for driving in reverse.
    int reverseCost;

    /// Cost for driving through a static obstacle.  Can be set to -1 to
    /// denote a hard constraint.
    int obsCost;

    /// Cost for driving through another vehicle. This can be set
    /// to zero to produce queuing behavior at intersections (the
    /// trajectory generation step will set the velocity profile
    /// such that we dont crash into other vehicles).
    int carCost;
  };

  public:
  // Initialize the graph from an RNDF file.
  static int loadRndf( Graph_t *graph,const char *filename);
  static int loadRndf( Graph_t *graph,const char *filename, vector<int> segments, vector<int> new_segments);
  static void unload_segment(Graph_t *graph, int segment);
  static int save_segment(Graph_t *graph, string folder, int segment);
  static int load_segment(Graph_t *graph, string folder, int segment);
  static int genVehicleSubGraph( Graph_t *graph,const VehicleState *vehicleState,const ActuatorState *actuatorState );
  static int genVolatileNode( Graph_t *graph,GraphNode *node );
  static int genChangeNode( Graph_t *graph,GraphNode *node, GraphNode *nodeA, GraphNode *nodeB);
  // Generate a maneuver linking two nodes
  static int genManeuver( Graph_t *graph,int nodeType,GraphNode *nodeA,GraphNode *nodeB,double maxSteer );
  static int genLanes( Graph_t *graph,std::RNDF *rndf, int segment);
  static int genLane( Graph_t *graph,std::Lane *lane, int direction );
  static int genRails( Graph_t *graph,std::Lane *lane, int direction);
  static int genLaneTangents( Graph_t *graph,std::Lane *lane, int direction);
  static int genTurns( Graph_t *graph,std::RNDF *rndf, vector<int> segments, vector<int> new_segments);
  static int genChanges( Graph_t *graph,std::RNDF *rndf, int segment);
  static int genZones(Graph_t *graph, std::RNDF *rndf, vector<int> segments, vector<int> new_segments);
  static vector<std::Waypoint*> getSegmentsExitPts(std::RNDF* rndf, int zoneId);
  static double getAngleInRange(double angle);

  // static function calling non-static function
  static GraphNode *getLaneNode(Graph_t *graph, const GraphNode *node, int direction);
  static GraphNode *getChangeNode(Graph_t *graph, const GraphNode *node, int direction);
  static int getChangeCount(Graph_t *graph, const GraphNode *node, int direction);
  static void getChangeNodes(vector<GraphNode *> &nodes, GraphNode **nodeA, GraphNode **nodeB, Graph_t *graph, const GraphNode *node, int direction, int index);

  // useful for updating lane information
  static bool isReverse(VehicleState &vehState, Map *map, LaneLabel &lane);

  static bool contains(vector<int> segments, int x);
  static point2 delta;
};

#endif
