/**
 * Saves a graph from an RNDF
 */

#include <map/Map.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <rndf/RNDF.hh>
#include <dgcutils/DGCutils.hh>
#include <skynet/skynet.hh>

#include "GraphUpdater.hh"

using namespace std;

/// @brief Class for storing a single node in the graph.
struct NewGraphNode
{
  // Node type
  char type;

  // Unique node index
  int index;

  // Start and end of the linked list of outward arcs
  GraphArc *outFirst, *outLast;

  // Start and end of the linked list of inwards arcs
  GraphArc *inFirst, *inLast;

  // For maneuvers node (start and end node of the maneuver)
  GraphNode *startNode, *endNode;

  // For lane node (left and right nodes, for rails)
  GraphNode *leftNode, *rightNode;

  // Direction (with traffic +1 or against traffic -1)
  signed char direction;

  // Path direction that we want to drive (fwd or rev)
  PathDirection pathDir;

  // Segment id
  uint8_t segmentId;

  // Lane id
  uint8_t laneId;

  // Rail id
  signed char railId;

  // Waypoint id (waypoints only)
  uint16_t waypointId;

  // Is this a waypoint?
  bool isWaypoint;

  // Is this an entry point?
  bool isEntry;

  // Is this an exit point?
  bool isExit;

  // Is this a stop line?
  bool isStop;

  // Dimensions of the lane element (width across the lane, length
  // along the lane, and maximum radius for bounding box checks).
  float laneWidth;

  // Node pose 
  float x, y, heading;

  // Transform from global to node frame.  This is an optimization
  // for configuration space computations.
  float transNG[3][3];

  // Transform from node to global frame.  This is an optimization
  // for configuration space computations.
  float transGN[3][3];

  // Steering angle
  float steerAngle;

  // Does this node collide with an static obstacle?
  uint8_t collideObs;

  // Does this node collide with a non-static car?
  uint8_t collideCar;

  // How fast is the car driving along the lane (>0 means in lane dir)
  double carVel;

  // Plan cost
  int planCost;

  // If isStop is true, distToStop is set to be the distance from the node to the actual stopline
  float distToStop;
} __attribute__((packed));

int main(int argc, char **args)
{
  if (argc < 2) {
    printf("Usage: %s RNDF_file\n\n", args[0]);
    return -1;
  }

  // Create a map
  Map* map = new Map();
  map->loadRNDF(args[1]);

  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  CmdArgs::stepbystep_load = true;

  // Load RNDF
  std::RNDF *rndf;
    
  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);
    
  // Load RNDF from file
  if (!rndf->loadFile(args[1]))
    return -1;
    
  if (rndf->getNumOfSegments() <= 0)
    return -1;

  int segment;
  double total_time = 0;
  int numNodes = 0;
  int numArcs = 0;
  for (unsigned int sn = 0; sn < rndf->getAllSegments().size(); sn++) {
    segment = rndf->getAllSegments()[sn]->getSegmentID();

    unsigned long long time1, time2;

    /* Create graph */
    Graph_t *graph;
    GraphUpdater::init(&graph, map);
  
    vector<int> segments;
    segments.push_back(segment);
    DGCgettime(time1);
    GraphUpdater::load(graph, map, segments);
    DGCgettime(time2);
    total_time += (time2-time1)/1000000.0;
    printf("Construction time for segment %d is %.2f seconds\n", segment, (time2-time1)/1000000.0);
    DGCgettime(time1);
    GraphUpdater::save(graph, segments);
    DGCgettime(time2);
    printf("Save time for segment %d is %.2f seconds\n", segment, (time2-time1)/1000000.0);

    numNodes += graph->numNodes;
    numArcs += graph->numArcs;

    /* destroy */
    GraphUpdater::destroy(graph);
  }

  printf("Total construction time is %.2f seconds\n", total_time);
  printf("Total memory consumption is %d bytes for %d nodes and %d arcs\n", numNodes*sizeof(GraphNode) + numNodes*sizeof(GraphArc), numNodes, numArcs);
  printf("  %.2f MB or %.2f GB\n", (numNodes*sizeof(GraphNode) + numNodes*sizeof(GraphArc))/(1024.0*1024.0), (numNodes*sizeof(GraphNode) + numNodes*sizeof(GraphArc))/(1024.0*1024.0*1024.0));
  printf("  sizeof(GraphNode) = %d bytes\n", sizeof(GraphNode));
  printf("  sizeof(GraphArc)  = %d bytes\n", sizeof(GraphArc));
  printf("  The new GraphNode would make the graph %d bytes (%d B each)\n", numNodes*sizeof(NewGraphNode) + numNodes*sizeof(GraphArc), sizeof(NewGraphNode));

  return 0;
}
