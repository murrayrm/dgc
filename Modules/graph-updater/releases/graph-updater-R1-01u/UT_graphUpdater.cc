 /*
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>
#include <path-planner/PathPlanner.hh>

#include "GraphUpdater.hh"

using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5+2846, 403942.3-4920);


  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey( argc,args );
  CmdArgs::stepbystep_load = false;
  CmdArgs::load_graph_files = false;

  VehicleState vehState;
  memset(&vehState, 0, sizeof(vehState));

  // Initialize the graph, using the map for the site offset
  Graph_t *graph;
  GraphUpdater::init(&graph, vec3_set(map->prior.delta.x, map->prior.delta.y, 0), map);

  // get the first wayppint
  GraphNode* initNode = graph->getNodeFromRndfId(1,1,1);

  // set the vehicle state to the first waypoint
  double roll, pitch, yaw;
  vehState.localX = initNode->pose.pos.x;
  vehState.localY = initNode->pose.pos.y;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  vehState.localYaw = yaw;

  GraphUpdater::display(10, graph, vehState);
  graph->quadtree->print();

  if (0) {
    system("rm plot_quad.m");
    graph->quadtree->print();
    
    vector<GraphNode *> nodes;
    double x_min = -10;
    double x_max = -3;
    double y_min = -142;
    double y_max = -130;
    graph->quadtree->get_all_nodes(nodes, x_min, x_max, y_min, y_max);
    cout << "Number of nodes to check is " << nodes.size() << endl;
    
    ofstream file_out;
    file_out.open("plot_quad.m", ios_base::app);
    
    ostringstream nodes_x, nodes_y;
    nodes_x << "nodes_x = [ ";
    nodes_y << "nodes_y = [ ";
    for (unsigned int i = 0; i<nodes.size(); i++) {
      nodes_x << nodes[i]->pose.pos.x << " ";
      nodes_y << nodes[i]->pose.pos.y << " ";
    }
    nodes_x << " ];" << endl;
    nodes_y << " ];" << endl;
    
    file_out << nodes_x.str() << nodes_y.str();
    file_out << "plot(nodes_x, nodes_y, 'r.'); hold on;" << endl;
    file_out << "x = [ " << x_min << " " << x_max << " " << x_max << " " << x_min << " " << x_min << " ];" << endl;
    file_out << "y = [ " << y_min << " " << y_min << " " << y_max << " " << y_max << " " << y_min << " ];" << endl;
    file_out << "plot(x, y, 'g-'); hold on;" << endl << endl;
    
    vec3_t pos = { -24.75, 0, 0 };
    GraphNode *node = graph->getNearestNode(pos, GRAPH_NODE_LANE, 10);
    if (node)
      file_out << "plot([" << node->pose.pos.x << "], [" << node->pose.pos.y << "], 'm.'); hold on;" << endl;
  
    for (int i = 0; i<graph->getArcCount(); i++) {
      double xA = graph->getArc(i)->nodeA->pose.pos.x;
      double yA = graph->getArc(i)->nodeA->pose.pos.y;
      double xB = graph->getArc(i)->nodeB->pose.pos.x;
      double yB = graph->getArc(i)->nodeB->pose.pos.y;
      file_out << "plot(["<<xA<<" "<<xB<<"], ["<<yA<<" "<<yB<<"], 'm-'); hold on;" << endl;
    }
    
    file_out.close();
  }
}
