 /*
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>
#include <path-planner/PathPlanner.hh>

#include "GraphUpdater.hh"

using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  //map->prior.delta = point2(3778410.5, 403942.3);

  // INITIALIZE THE GRAPH
  Graph_t* graph;
    
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey( argc,args );

  GraphUpdater::init( &graph, map );

  // PLAN PATH
  VehicleState vehState;
  memset( &vehState,0,sizeof( vehState ) );
  ActuatorState actState;
  memset( &actState,0,sizeof( actState ) );

  GraphUpdater::genVehicleSubGraph( graph,vehState,actState );

  // DISPLAY INFORMATION IN MAPVIEWER
  if (1) {
    // DISPLAY GRAPH
    GraphUpdater::display(10, graph,vehState);
  }

  Quadtree quadtree;
  quadtree.create_tree(graph);
  quadtree.print();

  vector<GraphNode *> nodes;
  double x_min = -50;
  double x_max = -10;
  double y_min = -50;
  double y_max = 0;
  quadtree.get_all_nodes(nodes, x_min, x_max, y_min, y_max);
  cout << "Number of nodes to check is " << nodes.size() << endl;
  
  ofstream file_out;
  file_out.open("plot_quad.m", ios_base::app);

  ostringstream nodes_x, nodes_y;
  nodes_x << "nodes_x = [ ";
  nodes_y << "nodes_y = [ ";
  for (unsigned int i = 0; i<nodes.size(); i++) {
    nodes_x << nodes[i]->pose.pos.x << " ";
    nodes_y << nodes[i]->pose.pos.y << " ";
  }
  nodes_x << " ];" << endl;
  nodes_y << " ];" << endl;

  file_out << nodes_x.str() << nodes_y.str();
  file_out << "plot(nodes_x, nodes_y, 'r.'); hold on;" << endl;
  file_out << "x = [ " << x_min << " " << x_max << " " << x_max << " " << x_min << " " << x_min << " ];" << endl;
  file_out << "y = [ " << y_min << " " << y_min << " " << y_max << " " << y_max << " " << y_min << " ];" << endl;
  file_out << "plot(x, y, 'g-'); hold on;" << endl << endl;
  file_out.close();
}
