#include "GraphUtils.hh"
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>

#include <iostream>
#include <dgcutils/DGCutils.hh>

#define MSG(fmt, ...) \
  (fprintf(stderr, "msg   %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

#define MAXSTEER 30
#define MAXTURN 90
#define MANEUVER_STEP_SIZE 3.0
#define LANE_STEP_SIZE 4.0
#define CHANGES_SPACING 2
#define RAIL_SHIFT 1.0
#define MAXNODENUM_UTURN 5000
#define MAXARCSNUM_UTURN 10000

// Temporary location for these
int* m_sectorNodes = new int[5];      // store the number of nodes in each sector and the total
int* m_sectorIndex = new int[4];      // store the indexes of nodes in each sector 
int m_numNodes;

// Getter method for vehicle properties (kinematics)
void GraphUtils::getPlannerKin(PlannerKinematics *kin_pt) {
  kin_pt->wheelBase = VEHICLE_WHEELBASE;
  kin_pt->maxSteer  = MAXSTEER * M_PI/180;
  kin_pt->maxTurn   = MAXTURN * M_PI/180;
} 
  
// Getter method for vehicle properties (constraints)
void GraphUtils::getPlannerConstraints(PlannerConstraints *planConst_pt) {
  planConst_pt->enableReverse = 0;
  planConst_pt->centerCost    = 1000;
  planConst_pt->headOnCost    = 100000;
  planConst_pt->offRoadCost   = 10000;
  planConst_pt->reverseCost   = 10000;
  planConst_pt->changeCost    = 50000;
  planConst_pt->obsCost       = 1000000;
  planConst_pt->carCost       = 0;
} 

double GraphUtils::getNearestObsPoint(point2 &pt, Map *map, VehicleState &vehState, LaneLabel lane, double offset) {
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);
  MapElement obstacle;
  double dist_obs = getNearestObsInLane(obstacle, vehState, map, lane);
  double dist;
  double min_dist = INFINITY;
  point2 obs_pt;
  for (unsigned int j=0; j<obstacle.geometry.size(); j++) {
    obs_pt.set(obstacle.geometry[j]);
     map->getDistAlongLine(dist, centerline, obs_pt);
    if (dist > 0.0 && dist < min_dist) {
      min_dist = dist;
    }
  }
  if (offset > min_dist) return -1;
  map->getPointAlongLine(pt, centerline, min_dist - offset);
  return dist_obs;
}

double GraphUtils::getNearestObsInLane(MapElement &me, VehicleState &vehState, Map *map, LaneLabel &lane) {
  vector<MapElement> obstacles;
  point2 currFrontPos = AliceStateHelper::getPositionFrontBumper(vehState);
  bool is_reverse = isReverse(vehState, map, lane);
  int obsErr = map->getObsInLane(obstacles, lane);
  if (obsErr < 1) //no obstacles
    return -1;
  int obs_index = -1;
  double min_dist = INFINITY;
  double dist;
  point2 obs_pt;
  point2arr centerline;
  map->getLaneCenterLine(centerline, lane);
  for (unsigned int i=0; i<obstacles.size(); i++){
    for (unsigned int j=0; j<obstacles[i].geometry.size(); j++){
      obs_pt.set(obstacles[i].geometry[j]);
      map->getDistAlongLine(dist, centerline, obs_pt, currFrontPos);
      if (is_reverse) dist = -dist;
      if (dist > 0.0 && dist < min_dist) {
        min_dist = dist;
        obs_index = i;
      }
    }
  }
  if (min_dist == INFINITY)
    return -1;
  me = obstacles[obs_index];
  return min_dist;
}

double GraphUtils::getNearestObsDist(VehicleState &vehState, Map *map, LaneLabel &lane) {
  MapElement obstacle;
  return getNearestObsInLane(obstacle, vehState, map, lane);
}

point2 GraphUtils::delta;

// Load an RNDF and initialize the graph.
int GraphUtils::loadRndf( Graph_t *graph, const char *filename )
{
  std::RNDF *rndf;
    
  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);

  // Load RNDF from file
  if (!rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);

  if (rndf->getNumOfSegments() <= 0)
    return ERROR("RNDF has no segments");

  /* Loop through all segment and load them */
  vector<int> segments;
  for (unsigned int sn = 0; sn < rndf->getAllSegments().size(); sn++) {
    int segment = rndf->getAllSegments()[sn]->getSegmentID();
    segments.push_back(segment);
  }
  loadRndf(graph, filename, segments, segments);

  // Clean up
  delete rndf;

  return 0;
}

int GraphUtils::loadRndf(Graph_t *graph, const char *filename, vector<int> segments, vector<int> new_segments)
{
  std::RNDF *rndf;
  int segment;

  // Create RNDF object
  rndf = new std::RNDF();
  assert(rndf);

  // Load RNDF from file
  if (!rndf->loadFile(filename))
    return ERROR("unable to load %s", filename);

  if (rndf->getNumOfSegments() <= 0)
    return ERROR("RNDF has no segments");

  graph->clearVolatile();

  if (CmdArgs::load_graph_files) {

    // generate the folder directory
    ostringstream oss;
    string folder;
    folder.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
    oss << folder << ".d";
    folder = oss.str();

    // Load from files
    for (unsigned int i = 0; i<new_segments.size(); i++) {
      segment = new_segments[i];
      if (load_segment(graph, folder, segment) != 0)
        return -1;
    }

  } else {
    
    for (unsigned int i=0; i<new_segments.size(); i++) {
      segment = new_segments[i];

      // Create lane nodes
      if (GraphUtils::genLanes(graph, rndf, segment) != 0)
        return -1;

      // Initialize quadtree
      if (graph->is_quadtree_initialized) delete graph->quadtree;
      graph->quadtree = new Quadtree();
      graph->quadtree->create_tree(graph);
      graph->is_quadtree_initialized = true;

      // Create lane-change nodes
      if ( GraphUtils::genChanges(graph,rndf, segment) != 0 )
        return -1;
    }
  }

  // Create turn maneuvers. Need to pass the graph since this method calls on the GraphUtils::genManeuver
  // which needs the graph. Unsure how to do this.
  if (GraphUtils::genTurns(graph,rndf, segments, new_segments) != 0)
    return -1;

  // Create zone nodes
  if (GraphUtils::genZones(graph, rndf, segments, new_segments) != 0)
    return -1;

  // Fix the static portion of the map so we can add volatile bits
  // for the vehicle.
  graph->freezeStatic();

  // Clean up
  delete rndf;

  // Temporary: recreate the quadtree
  if (graph->is_quadtree_initialized) delete graph->quadtree;
  graph->quadtree = new Quadtree();
  graph->quadtree->create_tree(graph);
  graph->is_quadtree_initialized = true;

  MSG( "created %d nodes %d arcs",
       graph->getNodeCount(),graph->getArcCount() );

  return 0;
}

int GraphUtils::save_segment(Graph_t *graph, string folder, int segment)
{
  GraphNode *node;
  GraphNode tmp_node;
  GraphArc *arc;
  int stage = 0;
  int i, index_first_node, index_last_node, index_nodeA, index_nodeB, index_nodeLeft, index_nodeRight;

  /* Find the first node of the segment */
  if (graph->getStaticNodeCount() == 0) return -1;
  node = graph->getNode(0);
  i = 0;
  while (node->segmentId != segment) {
    node = graph->getNode(i++);
    if (i == graph->getStaticNodeCount()) return -1;
  }
  index_first_node = i;

  /* Open binary file */
  ostringstream oss;
  string filename;
  oss << folder << "/segment_" << segment;
  filename = oss.str();
  ofstream file;
  file.open(filename.c_str(), ios::out | ios::binary);

  /* Save delta information */
  double x = delta.x;
  double y = delta.y;
  file.write(reinterpret_cast<char *>(&x), sizeof(double));
  file.write(reinterpret_cast<char *>(&y), sizeof(double));

  /* Save all nodes of type LANE and CHANGE that are contiguous in memory */
  while ((stage == 0 && (node->type & GRAPH_NODE_LANE)) || (stage == 1 && (node->type & GRAPH_NODE_CHANGE))) {
    // Save it
    file.write(reinterpret_cast<char *>(node), sizeof(GraphNode));
    if (node->startNode == NULL || node->endNode == NULL) {
      index_nodeA = -1;
      index_nodeB = -1;
    } else {
      index_nodeA = node->startNode->index - index_first_node;
      index_nodeB = node->endNode->index - index_first_node;
    }
    file.write(reinterpret_cast<char *>(&index_nodeA), sizeof(int));
    file.write(reinterpret_cast<char *>(&index_nodeB), sizeof(int));
    if (node->leftNode == NULL || node->rightNode == NULL) {
      index_nodeLeft = -1;
      index_nodeRight = -1;
    } else {
      index_nodeLeft = node->leftNode->index - index_first_node;
      index_nodeRight = node->rightNode->index - index_first_node;
    }
    file.write(reinterpret_cast<char *>(&index_nodeLeft), sizeof(int));
    file.write(reinterpret_cast<char *>(&index_nodeRight), sizeof(int));

    // Get next node
    node = graph->getNode(++i);
    // We've reached the end
    if (!node) break;
    if (stage == 0 && (node->type & GRAPH_NODE_CHANGE)) stage = 1;
  }
  index_last_node = i-1;

  /* Write separation node */
  tmp_node.index = -1;
  file.write(reinterpret_cast<char *>(&tmp_node), sizeof(GraphNode));

  /* Save arcs */
  for (i=0; i<graph->getArcCount(); i++) {
    arc = graph->getArc(i);
    if (arc->nodeA->index < index_first_node || arc->nodeA->index > index_last_node ||
        arc->nodeB->index < index_first_node || arc->nodeB->index > index_last_node) {
      continue;
    }
    index_nodeA = arc->nodeA->index - index_first_node;
    index_nodeB = arc->nodeB->index - index_first_node;
    // Ignore arc connection outside the segment
    file.write(reinterpret_cast<char *>(arc), sizeof(GraphArc));
    file.write(reinterpret_cast<char *>(&index_nodeA), sizeof(int));
    file.write(reinterpret_cast<char *>(&index_nodeB), sizeof(int));
  }

  file.close();

  return 0;
}

void GraphUtils::unload_segment(Graph_t *graph, int segment)
{
  GraphNode *node, *tmpNode;
  GraphArc *arc, *tmpArc;
  int stage = 0;
  int i;

  graph->clearVolatile();

  /* Find the first node of the segment */
  if (graph->getStaticNodeCount() == 0) return;
  node = graph->getNode(0);
  i = 0;
  while (node->segmentId != segment) {
    node = graph->getNode(i++);
    if (i == graph->getStaticNodeCount()) return;
  }

  /* Remove all nodes of type LANE and CHANGE that are contiguous in memory */
  while ((stage == 0 && (node->type & GRAPH_NODE_LANE)) || (stage == 1 && (node->type & GRAPH_NODE_CHANGE))) {
    // Mark it and its arcs for removal
    for (arc = node->inFirst; arc != NULL; arc = arc->inNext) {
      if (arc->nodeA != NULL) graph->removeOutArc(arc->nodeA, arc);
      arc->nodeB = NULL;
    }
    for (arc = node->outFirst; arc != NULL; arc = arc->outNext) {
      if (arc->nodeB != NULL) graph->removeInArc(arc->nodeB, arc);
      arc->nodeA = NULL;
    }
    node->outFirst = NULL;
    node->inFirst = NULL;

    // Get next node
    node = graph->getNode(++i);
    // We've reached the end
    if (!node) break;
    if (stage == 0 && (node->type & GRAPH_NODE_CHANGE)) stage = 1;
  }

  /* Remove nodes in turns (keep in mind that turn maneuver for a segment are always generated after the creation of the lane itself) */
  for (; i<graph->getStaticNodeCount(); i++) {
    node = graph->getNode(i);
    if (!(node->type & GRAPH_NODE_TURN)) continue;
    if (node->startNode == NULL || node->endNode == NULL) continue;
    if (node->startNode->segmentId == segment || node->endNode->segmentId == segment) {
      for (arc = node->inFirst; arc != NULL; arc = arc->inNext) {
        if (arc->nodeA != NULL) graph->removeOutArc(arc->nodeA, arc);
        arc->nodeB = NULL;
      }
      for (arc = node->outFirst; arc != NULL; arc = arc->outNext) {
        if (arc->nodeB != NULL) graph->removeInArc(arc->nodeB, arc);
        arc->nodeA = NULL;
      }
      node->outFirst = NULL;
      node->inFirst = NULL;
    }
  }

  /* Remove all nodes that do not have any arc going out/in from them */
  int next_index = -1;
  // find first next index
  for (i=0; i<graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    if (node->outFirst != NULL || node->inFirst != NULL) continue;
    next_index = i;
    break;
  }
  // move nodes around
  for (i=next_index; i<graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    if (node->outFirst == NULL && node->inFirst == NULL) continue;
    // update index
    node->index = next_index;
    graph->nodes[next_index] = *node;
    // update startNode and endNode
    for (int j = 0; j<graph->getNodeCount(); j++) {
      tmpNode = graph->getNode(j);
      if (tmpNode->startNode == node) tmpNode->startNode = &graph->nodes[next_index];
      if (tmpNode->endNode == node) tmpNode->startNode = &graph->nodes[next_index];
    }
    // update nodeA and nodeB
    for (int j = 0; j<graph->getArcCount(); j++) {
      tmpArc = graph->getArc(j);
      if (tmpArc->nodeA == node) tmpArc->nodeA = &graph->nodes[next_index];
      if (tmpArc->nodeB == node) tmpArc->nodeB = &graph->nodes[next_index];
    }
    next_index++;
  }
  graph->numNodes = next_index;

  /* Remove all arcs that do not link to/from any node */
  // find first index
  for (i=0; i<graph->getArcCount(); i++) {
    arc = graph->getArc(i);
    if (arc->nodeA != NULL && arc->nodeB != NULL) continue;
    next_index = i;
    break;
  }
  // move arcs around
  for (i=next_index; i<graph->getArcCount(); i++) {
    arc = graph->getArc(i);
    if (arc->nodeA == NULL || arc->nodeB == NULL) continue;
    // update index
    arc->index = next_index;
    graph->arcs[next_index] = *arc;
    // update inFirst, inLast, outFirst, outLast
    for (int j = 0; j<graph->getNodeCount(); j++) {
      tmpNode = graph->getNode(j);
      if (tmpNode->inFirst == arc) tmpNode->inFirst = &graph->arcs[next_index];
      if (tmpNode->outFirst == arc) tmpNode->outFirst = &graph->arcs[next_index];
      if (tmpNode->inLast == arc) tmpNode->inLast = &graph->arcs[next_index];
      if (tmpNode->outLast == arc) tmpNode->outLast = &graph->arcs[next_index];
    }
    // update outPrev, outNext, inPrev, inNext
    for (int j = 0; j<graph->getArcCount(); j++) {
      tmpArc = graph->getArc(j);
      if (tmpArc->outPrev == arc) tmpArc->outPrev = &graph->arcs[next_index];
      if (tmpArc->inPrev == arc) tmpArc->inPrev = &graph->arcs[next_index];
      if (tmpArc->outNext == arc) tmpArc->outNext = &graph->arcs[next_index];
      if (tmpArc->inNext == arc) tmpArc->inNext = &graph->arcs[next_index];
    }
    next_index++;
  }
  graph->numArcs = next_index;

  graph->freezeStatic();

  // Temporary: recreate the quadtree
  if (graph->is_quadtree_initialized) delete graph->quadtree;
  graph->quadtree = new Quadtree();
  graph->quadtree->create_tree(graph);
  graph->is_quadtree_initialized = true;
}

int GraphUtils::load_segment(Graph_t *graph, string folder, int segment)
{
  GraphNode tmp_node;
  GraphNode *node;
  GraphArc tmp_arc;
  GraphArc *arc;
  int index_offset_node = graph->getNodeCount();
  int index_nodeA, index_nodeB, index_nodeLeft, index_nodeRight;

  /* Open binary file */
  ostringstream oss;
  string filename;
  oss << folder << "/segment_" << segment;
  filename = oss.str();
  ifstream file;
  file.open(filename.c_str(), ios::in | ios::binary);
  if (!file.is_open()) {
    Log::getStream(1) << "Unable to open " << filename << endl;
    Console::addMessage("Unable to open graph file for segment %d", segment);
    return -1;
  }
  Console::addMessage("Loading file: %s", filename.c_str());

  /* Get delta information */
  double x;
  double y;
  file.read(reinterpret_cast<char *>(&x), sizeof(double));
  file.read(reinterpret_cast<char *>(&y), sizeof(double));

  /* Load node until separation node */
  while(1) {
    file.read(reinterpret_cast<char *>(&tmp_node), sizeof(GraphNode));
    if (file.eof() || tmp_node.index == -1) break;
    file.read(reinterpret_cast<char *>(&index_nodeA), sizeof(int));
    file.read(reinterpret_cast<char *>(&index_nodeB), sizeof(int));
    file.read(reinterpret_cast<char *>(&index_nodeLeft), sizeof(int));
    file.read(reinterpret_cast<char *>(&index_nodeRight), sizeof(int));
    node = graph->createNode();
    if (!node) {
      Log::getStream(1) << "Not enough memory available, try to increase the number of nodes" << endl;
      Console::addMessage("Not enough memory");
      break;
    }
    // Set initial value
    tmp_node.index = node->index;
    tmp_node.quadtree = NULL;
    tmp_node.outFirst = NULL;
    tmp_node.outLast = NULL;
    tmp_node.inFirst = NULL;
    tmp_node.inLast = NULL;
    tmp_node.startNode = NULL;
    tmp_node.endNode = NULL;
    tmp_node.leftNode = NULL;
    tmp_node.rightNode = NULL;
    if (index_nodeA != -1 && index_nodeB != -1) {
      tmp_node.startNode = graph->getNode(index_offset_node) + index_nodeA;
      tmp_node.endNode = graph->getNode(index_offset_node) + index_nodeB;
    }
    if (index_nodeLeft != -1 && index_nodeRight != -1) {
      tmp_node.leftNode = graph->getNode(index_offset_node) + index_nodeLeft;
      tmp_node.rightNode = graph->getNode(index_offset_node) + index_nodeRight;
    }
    // Update position
    tmp_node.pose.pos.x += x - delta.x;
    tmp_node.pose.pos.y += y - delta.y;
    *node = tmp_node;
    pose3_to_mat44d(node->pose, node->transGN);
    mat44d_inv(node->transNG, node->transGN);
  }

  /* Load of arcs */
  while(1) {
    file.read(reinterpret_cast<char *>(&tmp_arc), sizeof(GraphArc));
    file.read(reinterpret_cast<char *>(&index_nodeA), sizeof(int));
    file.read(reinterpret_cast<char *>(&index_nodeB), sizeof(int));
    if (file.eof()) break;
    arc = graph->createArc(index_offset_node+index_nodeA, index_offset_node+index_nodeB);
    if (!arc) {
      Log::getStream(1) << "Not enough memory available, try to increase the number of arcs" << endl;
      Console::addMessage("Not enough memory");
      break;
    }
    arc->type = tmp_arc.type;
  }

  /* Close file */
  file.close();

  return 0;
}

int GraphUtils::genVehicleSubGraph( Graph_t *graph,const VehicleState *vehState,const ActuatorState *actState )
{
  int i;
  GraphNode *nodeA, *nodeB;
  vec3_t p;
  double dm;
  GraphUtils::PlannerKinematics kin;
  GraphUtils::PlannerConstraints cons;

  /* Define the vehicle properties. */
  // Temporary location for these constants, need to define getters and setters.
  // TODO:
  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;

  cons.enableReverse = 0;
  cons.centerCost = 1000;
  cons.headOnCost =   100000;
  cons.offRoadCost =   10000;
  cons.reverseCost =   10000;
  cons.changeCost =    50000;
  cons.obsCost =     1000000;
  cons.carCost = 0;

  // Clear the volatile portion of the graph
  graph->clearVolatile();
    
  // Create vehicle node 
  nodeA = graph->createNode();
  if (!nodeA)
    return ERROR("unable to create node; try increasing maxNodes");
  nodeA->type = GRAPH_NODE_VOLATILE;    

  // Remember the node for later use.  
  graph->vehicleNode = nodeA;

  // Set the node pose
  nodeA->pose.pos = vec3_set(vehState->localX,
                             vehState->localY,
                             0);
  nodeA->pose.rot = quat_from_rpy(vehState->localRoll,
                                  vehState->localPitch,
                                  vehState->localYaw);

  // Construct transforms
  pose3_to_mat44d(nodeA->pose, nodeA->transGN);
  mat44d_inv(nodeA->transNG, nodeA->transGN);

  // Compute the current steering angle for maneuver generation
  nodeA->steerAngle = actState->m_steerpos * kin.maxSteer;

  // Set RNDF info
  GraphUtils::genVolatileNode( graph,nodeA );

  int err;
  int numFeasible = 0;
  int numUnfeasible = 0;
  
  // Find destination nodes on the static graph.
  //
  // TODO: This should really be checking things like whether or not
  // lanes are adjacent, and also doing a quick rejection of unfeasible
  // destinations (e.g., on distance and minimum turning radius).    
  for (i = 0; i < graph->getNodeCount(); i++)
  {
    nodeB = graph->getNode(i);
    if (nodeB == nodeA)
      continue;

    // Must be lane/turn nodes 
    if (!((nodeB->type & GRAPH_NODE_LANE) ||
          (nodeB->type & GRAPH_NODE_TURN) ||
          (nodeB->type & GRAPH_NODE_KTURN)))
      continue;
          
    // Nodes must be reasonably close, but not too close.
    dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
    if (dm < 2 || dm > 20) // MAGIC
      continue;

    // The destination must be ahead of the source
    p = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
    if (p.x < 0)
      continue;

    if (true)
    {
      // Generate the maneuver from A to B
      err = genManeuver( graph,GRAPH_NODE_VOLATILE,nodeA,nodeB,kin.maxSteer );
      if (err == ENOMEM)
        break;
      if (err == 0)
        numFeasible += 1;
      else
        numUnfeasible += 1;
    }

    if (cons.enableReverse)
    {
      // Generate the maneuver from B to A (for driving in reverse)
      err = genManeuver( graph,GRAPH_NODE_VOLATILE,nodeB,nodeA,kin.maxSteer );
      if (err == ENOMEM)
        break;
      if (err == 0)
        numFeasible += 1;
      else
        numUnfeasible += 1;
    }
  }

  //MSG("feasable ratio %d : %d (%0.f%%)",
  //    numFeasible, numUnfeasible, 100.0 * numFeasible / (numFeasible + numUnfeasible));
  
  return true;
}


int GraphUtils::genVolatileNode( Graph_t *graph,GraphNode *node )
{
  GraphNode *lanel;
  double ax, ay, bx, by;

  // Get the nearest lane or turn element.
  lanel = graph->getNearestNode( node->pose.pos,            
                                 GRAPH_NODE_LANE | GRAPH_NODE_TURN,
                                 10);                  // MAGIC
  
  if (!lanel)
    return 0;
  
  // Compute node position relative to lane element
  ax = node->pose.pos.x;
  ay = node->pose.pos.y;
  bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
  by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

  // Record distance from lane centerline
  node->centerDist = sqrtf(bx * bx + by * by);

  // See if the node is in the lane in a strict euclidean sense
  if (bx > -lanel->laneLength/2 && bx < +lanel->laneLength/2 &&
      by > -lanel->laneWidth/2 && by < +lanel->laneWidth/2)
  {
    // Assign RNDF data
    node->segmentId = lanel->segmentId;
    node->laneId = lanel->laneId;
    node->isEntry = lanel->isEntry;
    node->isExit = lanel->isExit;
    node->isStop = lanel->isStop;
    node->distToStop = 0.0;
  }

  return 0;
}

// Generate a maneuver linking two nodes
int GraphUtils::genManeuver( Graph_t *graph,int nodeType,GraphNode *nodeA,GraphNode *nodeB,double maxSteer )
{
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseB;
  VehicleConfiguration configA, config;
  double roll, pitch, yaw;
  double stepSize, dm, s;
  int i, numSteps;
  double theta;
  bool feasible;
  GraphNode *src, *dst;
  GraphArc *arc;
  GraphUtils::PlannerKinematics kin;

  /* Define the vehicle properties. */
  // Temporary location for these constants, need to define getters and setters.
  // TODO:
  assert(nodeA);
  assert(nodeB);
 
  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;

  // Initial vehicle pose
  configA.x = nodeA->pose.pos.x;
  configA.y = nodeA->pose.pos.y;
  quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
  configA.theta = yaw;
  configA.phi = nodeA->steerAngle;

  // Final vehicle pose
  poseB.x = nodeB->pose.pos.x;
  poseB.y = nodeB->pose.pos.y;
  quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
  poseB.theta = yaw;

  //MSG("man %f %f : %f %f", poseA.x, poseA.y, poseB.x, poseB.y);

  // Vehicle properties
  vp = maneuver_create_vehicle( kin.wheelBase, maxSteer );
  
  // Create maneuver object
  mp = maneuver_config2pose(vp, &configA, &poseB);

  // Some maneuvers may not be possible, so trap this.
  if (!mp)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  // MAGIC
  stepSize = MANEUVER_STEP_SIZE;
  
  // Distance between nodes
  dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
  
  // Number of intermediate nodes
  numSteps = (int) ceil(dm / stepSize);

  // Dont do very, very short maneuvers.
  if (numSteps < 1)
  {
    maneuver_free(mp);
    free(vp);
    return -1;
  }

  //MSG("%f %f %f : %f %f %f",
  //    poseA.x, poseA.y, poseA.theta, poseB.x, poseB.y, poseB.theta);

  // Check for unfeasable maneuvers. Note the iteration includes the final
  // step in the trajectory (we check 0 to 1 inclusive), to trap the case
  // where the final step has a huge change in theta.
  feasible = true;
  theta = configA.theta;

  for (i = 1; i < numSteps + 1; i++) 
  {
    s = (double) i / numSteps;

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    //fprintf(stdout, "config %d %f %f %f %f %f\n",
    //        i, s, config.x, config.y, config.theta, config.phi);
    
    // Discard turns outside the steering limit
    if (fabs(config.phi) > vp->steerlimit)
      feasible = false;

    // Discard big changes in yaw that occur too fast to be
    // caught by the steering limit test at a course step size.
    if (acos(cos(config.theta - theta)) > MAXTURN * M_PI/180) // MAGIC
      feasible = false;

    theta = config.theta;
  }

  if (!feasible)
  {
    maneuver_free(mp);
    free(vp);
    return -1;      
  }

  // Create the intermediate nodes for this maneuver
  src = nodeA;  
  for (i = 1; i < numSteps; i++)
  {
    s = (double) i / numSteps;  

    // Get the vehicle configuration (including steer angle) at this step.
    config = maneuver_evaluate_configuration(vp, mp, s);    
    
    // Create a maneuver node
    dst = graph->createNode();
    if (!dst)
    {
      return -1;
    }

    // Fill out basic node data
    dst->type = nodeType;
    dst->direction = +1;
    dst->pose.pos = vec3_set(config.x, config.y, 0);
    dst->pose.rot = quat_from_rpy(0, 0, config.theta);
    dst->steerAngle = config.phi;
    pose3_to_mat44d(dst->pose, dst->transGN);
    mat44d_inv(dst->transNG, dst->transGN);
    dst->startNode = nodeA;
    dst->endNode = nodeB;

    // If we are creating turns, create the lane boundaries
    // and update the spatial index.
    if (nodeType & GRAPH_NODE_TURN)
    {
      dst->laneLength = stepSize * 1.35; // MAGIC HACK
      dst->laneWidth = (1 - s) * nodeA->laneWidth + s * nodeB->laneWidth;
      dst->laneRadius = sqrtf(pow(dst->laneLength,2) + pow(dst->laneWidth,2)) / 2;
    }

    // If we are generating a lane change, set the
    // RNDF data and center-line distance.
    if (nodeType & GRAPH_NODE_CHANGE)
    {
      genChangeNode(graph, dst, nodeA, nodeB);
      if (nodeA->direction == nodeB -> direction) dst->type |= GRAPH_NODE_LOCAL_CHANGE;
      if (dst->segmentId != nodeA->segmentId || dst->segmentId != nodeB->segmentId) {
        return -1;
      }
    }
      
    // If we are generating volatile, set the RNDF data and
    // center-line distance.
    if (nodeType & GRAPH_NODE_VOLATILE)
      GraphUtils::genVolatileNode( graph,dst );

    // Create an arc from the previous node to the new node
    // MSG("Creating arc while generating maneuver %.2f,%.2f -> %.2f,%.2f", configA.x, configA.y, poseB.x, poseB.y);
    arc = graph->createArc(src->index, dst->index);
    if (!arc)
    {
      return -1;
    }
    arc->type = GRAPH_ARC_NODE;

    src = dst;
  }

  // Create an arc to the final node
  dst = nodeB;
  // MSG("Creating arc while generating maneuver %.2f,%.2f -> %.2f,%.2f", configA.x, configA.y, poseB.x, poseB.y);
  arc = graph->createArc(src->index, dst->index);
  if (!arc)
  {
    return -1;
  }
  arc->type = GRAPH_ARC_NODE;

  /* If we created a volatile maneuver passing through a stopline
   * we have to be careful to add that stopline information to the maneuver */
  if (nodeType & GRAPH_NODE_VOLATILE) {
    /* If we connect to a TURN maneuver, there might be a stopline */
    if (nodeB->type & GRAPH_NODE_TURN) {
      if (nodeB->startNode && nodeB->startNode->isStop) {
        GraphNode *node = src;
        GraphNode *min_node = NULL;
        double min_dist = INFINITY;
        double dist;
        while (node) {
          dist = pow(node->pose.pos.x - nodeB->startNode->pose.pos.x, 2) + pow(node->pose.pos.y - nodeB->startNode->pose.pos.y, 2);
          if (dist < min_dist) {
            min_dist = dist;
            min_node = node;
          }

          /* Get previous node on maneuver */
          if (node->inFirst && node->inFirst->nodeA) {
            node = node->inFirst->nodeA;
          } else {
            node = NULL;
          }
        }

        if (min_node) {
          double rn, pn, yn;
          double rs, ps, ys;
          double real_dist;

          min_node->isStop = true;

          /* Correctly assign the dist to the stopline */
          quat_to_rpy(min_node->pose.rot, &rn, &pn, &yn);
          quat_to_rpy(nodeB->startNode->pose.rot, &rs, &ps, &ys);
          real_dist = sqrt(min_dist)*(cos(yn)*cos(ys) + sin(yn)*sin(ys));
          min_node->distToStop = real_dist;
        }
      }
    }
  }

  maneuver_free(mp);
  free(vp);
  
  return 0;
}

// Set the RNDF data on a change node
int GraphUtils::genChangeNode(Graph_t *graph,GraphNode *node, GraphNode *nodeA, GraphNode *nodeB)
{
  GraphNode *lanel;
  double ax, ay, bx, by, cx, cy;
      
  // Get the nearest lane.
  // lanel = graph->getNearestNode(node->pose.pos,GRAPH_NODE_LANE, 10); // MAGIC
  ax = node->pose.pos.x;
  ay = node->pose.pos.y;
  bx = nodeA->pose.pos.x;
  by = nodeA->pose.pos.y;
  cx = nodeB->pose.pos.x;
  cy = nodeB->pose.pos.y;
  if (pow(ax-bx,2)+pow(ay-by,2) < pow(ax-cx,2)+pow(ay-cy,2)) {
    lanel = nodeA;
  } else {
    lanel = nodeB;
  }
  assert(lanel);

  // Compute node position relative to lane element
  bx = lanel->transNG[0][0] * ax + lanel->transNG[0][1] * ay + lanel->transNG[0][3];
  by = lanel->transNG[1][0] * ax + lanel->transNG[1][1] * ay + lanel->transNG[1][3];

  // Record distance from lane centerline
  node->centerDist = sqrtf(bx * bx + by * by);
  node->segmentId = lanel->segmentId;
  node->laneId = lanel->laneId;      
  return 0;
}

// Generate lane nodes
int GraphUtils::genLanes(Graph_t *graph,std::RNDF *rndf, int seg)
{
  int sn, ln;
  std::Segment *segment;
  std::Lane *lane;
  int wn;
  int size;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wa;

  // Iterate through segments and lanes, adding intermediate
  // nodes between waypoints in the same lane.
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segment = rndf->getAllSegments()[sn];
    if (seg != segment->getSegmentID()) continue;
    for (ln = 0; ln < (int) segment->getAllLanes().size(); ln++)
    {
      lane = segment->getAllLanes()[ln];

      // List of all waypoints in the lane
      waypoints = lane->getAllWaypoints();
      size = (int) waypoints.size();

      // convert to local coordinates
      for (wn = 0; wn < size; wn++) {
        wa = waypoints[wn];
        wa->setNorthingEasting(wa->getNorthing()-delta.x, wa->getEasting()-delta.y);
      }

      // Generate nodes for driving with traffic
      if (GraphUtils::genLane(graph,lane, +1) != 0)
        return -1;
      if (GraphUtils::genLaneTangents(graph,lane, +1) != 0)
        return -1;
      if (GraphUtils::genRails(graph, lane, +1) != 0)
        return -1;

      // Generate nodes for driving against traffic
      if (GraphUtils::genLane(graph,lane, -1) != 0)
        return -1;
      if (GraphUtils::genLaneTangents(graph,lane, -1) != 0)
        return -1;
      if (GraphUtils::genRails(graph, lane, -1) != 0)
        return -1;
    }
  }

  return 0;
}

// Generate a lane
int GraphUtils::genLane( Graph_t *graph,std::Lane *lane, int direction )
{
  int wn;
  int size;
  bool connection_was_skipped = false;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *wa, *wb, *wc, *wd, *we, *wf;
  GraphNode *node;
  GraphNode *previousWaypointNode = NULL;
  GraphArc *arc;
  double stepSize;
  int i, numSteps;
  double dx, dy, dm, s;
  Vehicle *vp;
  Maneuver *mp;  
  Pose2D poseC, poseD, poseS;    
  GraphUtils::PlannerKinematics kin;

  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;
 
  // MAGIC
  // Spacing for interpolated lane nodes
  stepSize = LANE_STEP_SIZE;

  // Vehicle properties
  vp = maneuver_create_vehicle( kin.wheelBase,1000 ); // MAGIC
  
  // List of all waypoints in the lane
  waypoints = lane->getAllWaypoints();
  size = (int) waypoints.size();

  for (wn = 0; wn < size; wn++)
  {
    // Pick out six waypoints for interpolation.  The lane nodes will
    // be generated between c and d.
    wa = wb = wc = wd = we = wf = NULL;
    if (direction > 0)
    {
      if (wn - 2 >= 0)
        wa = waypoints[wn - 2];
      if (wn - 1 >= 0)
        wb = waypoints[wn - 1];
      if (true)
        wc = waypoints[wn + 0];
      if (wn + 1 < (int) waypoints.size())
        wd = waypoints[wn + 1];
      if (wn + 2 < (int) waypoints.size())
        we = waypoints[wn + 2];
      if (wn + 3 < (int) waypoints.size())
        wf = waypoints[wn + 3];
    }
    else if (direction < 0)
    {
      if (wn - 2 >= 0)
        wa = waypoints[size - 1 - (wn - 2)];
      if (wn - 1 >= 0)
        wb = waypoints[size - 1 - (wn - 1)];
      if (true)
        wc = waypoints[size - 1 - (wn + 0)];
      if (wn + 1 < (int) waypoints.size())
        wd = waypoints[size - 1 - (wn + 1)];
      if (wn + 2 < (int) waypoints.size())
        we = waypoints[size - 1 - (wn + 2)];
      if (wn + 3 < (int) waypoints.size())
        wf = waypoints[size - 1 - (wn + 3)];
    }
    else
    {
      assert(false);
    }

    // Create a node for waypoint c
    node = graph->createNode();
    if (!node)
      return ERROR("unable to create node; try increasing maxNodes");
    assert(node);
    node->type = GRAPH_NODE_LANE;
    node->direction = direction;
    node->isWaypoint = (direction > 0);
    node->isEntry = wc->isEntry();
    node->isExit = wc->isExit();
    node->isStop = wc->isStopSign();
    node->distToStop = 0.0;
    node->isCheckpoint = wc->isCheckpoint();        
    node->segmentId = wc->getSegmentID();
    node->laneId = wc->getLaneID();
    node->waypointId = wc->getWaypointID();
    node->checkpointId = wc->getCheckpointID();
    node->laneLength = stepSize * 1.35; // MAGIC HACK
    if (lane->getLaneWidth() == 0) node->laneWidth = 12 * 0.0254 * 12;
    else node->laneWidth = lane->getLaneWidth() * 0.0254 * 12; // Width is in feet, urgh
    node->laneRadius = sqrtf(pow(node->laneLength,2) + pow(node->laneWidth,2)) / 2;
    node->pose.pos = vec3_set(wc->getNorthing(), wc->getEasting(), 0);
    
    // Create an arc from the previous node to the new node
    if (wb && !connection_was_skipped)
    {
      /* MSG("Creating arc to previous %d.%d -> %d.%d (%.2f, %.2f - %.2f, %.2f)",
          graph->getNode(node->index-1)->segmentId, graph->getNode(node->index-1)->laneId,
          node->segmentId, node->laneId,
          graph->getNode(node->index-1)->pose.pos.x, graph->getNode(node->index-1)->pose.pos.y,
          node->pose.pos.x, node->pose.pos.y); */
      arc = graph->createArc(node->index - 1, node->index);
      if (!arc)
        return ERROR("unable to create arc; try increasing maxArcs");
      arc->type = GRAPH_ARC_NODE;

      // Create an arc from the previous waypoint to the new waypoint
      if (direction > 0) {
        arc = graph->createArc(previousWaypointNode->index, node->index);
        if (!arc)
          return ERROR("unable to create arc; try increasing maxArcs");
        arc->type = GRAPH_ARC_WAYPOINT;
      }
    }
    // Set waypoint c to be the previous waypoint
    previousWaypointNode = node;

    // Stop here if d is the last waypoint in the lane.
    if (!wd)
      continue;

    // Check if we would connect wc with wd, if not skip (direction = 1)
    if (wc->isExit() && direction == 1) {
      std::vector<std::GPSPoint*> entry_waypoints = wc->getEntryPoints();
      int entry_size = entry_waypoints.size();
      bool skip_connection = false;
      for (int entry_index = 0; entry_index < entry_size; entry_index++) {
        std::GPSPoint *entry_wpt = entry_waypoints[entry_index];
        // wc will be connected via turn maneuvers later on
        if (entry_wpt->getSegmentID() == wd->getSegmentID() &&
            entry_wpt->getLaneID() == wd->getLaneID() &&
            entry_wpt->getWaypointID() == wd->getWaypointID()) {
          MSG("Skipping creation of %d.%d %d -> %d within the lane", wd->getSegmentID(), wd->getLaneID(),
              wc->getWaypointID(), wd->getWaypointID());
          skip_connection = true;
        }
      }
      connection_was_skipped = skip_connection;
      if (skip_connection) continue;
    } else {
      connection_was_skipped = false;
    }

    // Check if we would connect wc with wd, if not skip (direction = 0)
    if (wc->isEntry() && direction == -1) {
      std::vector<std::GPSPoint*> entry_waypoints = wd->getEntryPoints();
      int entry_size = entry_waypoints.size();
      bool skip_connection = false;
      for (int entry_index = 0; entry_index < entry_size; entry_index++) {
        std::GPSPoint *entry_wpt = entry_waypoints[entry_index];
        // wd will be connected via turn maneuvers later on
        if (entry_wpt->getSegmentID() == wc->getSegmentID() &&
            entry_wpt->getLaneID() == wc->getLaneID() &&
            entry_wpt->getWaypointID() == wc->getWaypointID()) {
          MSG("Skipping creation of %d.%d %d -> %d within the lane (reverse)", wd->getSegmentID(), wd->getLaneID(),
              wc->getWaypointID(), wd->getWaypointID());
          skip_connection = true;
        }
      }
      connection_was_skipped = skip_connection;
      if (skip_connection) continue;
    } else {
      connection_was_skipped = false;
    }

    // Distance between nodes
    dx = wd->getNorthing() - wc->getNorthing();
    dy = wd->getEasting() - wc->getEasting();
    dm = sqrt(dx*dx + dy*dy);

    // Number of intermediate nodes
    numSteps = (int) ceil(dm / stepSize);

    if (true && wa && wb && we && wf)
    {
      double tba, tcb, tdc, tdb;
      double ted, tfe, tec;

      tba = atan2(wb->getEasting() - wa->getEasting(),
                  wb->getNorthing() - wa->getNorthing());
      tcb = atan2(wc->getEasting() - wb->getEasting(),
                  wc->getNorthing() - wb->getNorthing());
      tdc = atan2(wd->getEasting() - wc->getEasting(),
                  wd->getNorthing() - wc->getNorthing());
      tdb = atan2(wd->getEasting() - wb->getEasting(),
                  wd->getNorthing() - wb->getNorthing());

      ted = atan2(we->getEasting() - wd->getEasting(),
                  we->getNorthing() - wd->getNorthing());
      tfe = atan2(wf->getEasting() - we->getEasting(),
                  wf->getNorthing() - we->getNorthing());
      tec = atan2(we->getEasting() - wc->getEasting(),
                  we->getNorthing() - wc->getNorthing());
      
      // Initial vehicle pose
      poseC.x = wc->getNorthing();
      poseC.y = wc->getEasting();
      poseC.theta = tdb;
 
      // Final vehicle pose
      poseD.x = wd->getNorthing();
      poseD.y = wd->getEasting();
      poseD.theta = tec;

      /*
        if (acos(cos(tcb - tba)) < 15 * M_PI/180 ||
        acos(cos(tfe - ted)) < 15 * M_PI/180) // MAGIC
        {
        poseC.theta = tcb;
        poseD.theta = ted;        
        }
      */

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseC, &poseD);
    }
    else if (wb && we)
    {      
      // Initial vehicle pose
      poseC.x = wc->getNorthing();
      poseC.y = wc->getEasting();
      poseC.theta = atan2(wd->getEasting() - wb->getEasting(),
                          wd->getNorthing() - wb->getNorthing());

      // Final vehicle pose
      poseD.x = wd->getNorthing();
      poseD.y = wd->getEasting();
      poseD.theta = atan2(we->getEasting() - wc->getEasting(),
                          we->getNorthing() - wc->getNorthing());      

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseC, &poseD);
    }
    else
    {
      mp = NULL;
    }

    // Step along the maneuver
    for (i = 1; i < numSteps; i++)
    {
      s = (double) i / numSteps;  

      if (mp)
      {
        // Get the vehicle configuration (including steer angle) at this step.
        poseS = maneuver_evaluate_pose(vp, mp, s);
      }
      else
      {
        // Linear interpolation
        poseS.x = s * dx + wc->getNorthing();
        poseS.y = s * dy + wc->getEasting();
      }

      //MSG("pose %d %f %f %f %f", i, s, poseS.x, poseS.y, acos(cos(poseS.theta)));
              
      // Create a interpolated node between waypoints b and c
      node = graph->createNode();
      if (!node)
        return ERROR("unable to create node; try increasing maxNodes");      
      node->type = GRAPH_NODE_LANE;
      node->direction = direction;
      node->segmentId = wc->getSegmentID();
      node->laneId = wc->getLaneID();
      node->waypointId = 0;
      node->laneLength = stepSize * 1.35; // MAGIC HACK
      if (lane->getLaneWidth() == 0) node->laneWidth = 12 * 0.0254 * 12;
      else node->laneWidth  = lane->getLaneWidth() * 0.0254 * 12; // Width in in feet, urgh
      node->laneRadius = sqrtf(pow(node->laneLength,2) + pow(node->laneWidth,2)) / 2;
      node->pose.pos.x = poseS.x;
      node->pose.pos.y = poseS.y;
      node->pose.pos.z = 0;

      // Create an arc frome the previous node to the new node
      /* MSG("Creating arc from %d.%d -> %d.%d (%.2f, %.2f - %.2f, %.2f)",
          graph->getNode(node->index-1)->segmentId, graph->getNode(node->index-1)->laneId,
          node->segmentId, node->laneId,
          poseC.x, poseC.y, poseD.x, poseD.y); */
      arc = graph->createArc(node->index - 1, node->index);
      if (!arc)
        return ERROR("unable to create arc; try increasing maxArcs");
      arc->type = GRAPH_ARC_NODE;
    }

    if (mp)
    {
      maneuver_free(mp);
      mp = NULL;
    }    
  }

  // Clean up
  free(vp);

  return 0;
}

int GraphUtils::genRails(Graph_t *graph,std::Lane *lane, int direction)
{
  GraphNode *node;
  GraphNode *railNode;
  GraphNode *prevNode;
  GraphArc *arc;
  double p, r, y, dx, dy;
  int numNodes = graph->getNodeCount();

  for (int rail = -1; rail<2; rail += 2) {
    for (int i=0; i<numNodes; i++) {
      node = graph->getNode(i);
      if (!(node->type & GRAPH_NODE_LANE)) continue;
      if (node->railId != 0) continue;
      if (node->laneId != lane->getLaneID() || node->segmentId != lane->getSegmentID()) continue;
      if (node->direction != direction) continue;

      // Create left/rail rail
      railNode = graph->createNode();
      if (!railNode) return ERROR("unable to create node; try increasing maxNodes");
      if (rail == -1) {
        node->leftNode = railNode;
        railNode->leftNode = railNode;
        railNode->rightNode = node;
      } else {
        node->rightNode = railNode;
        railNode->rightNode = railNode;
        railNode->leftNode = node;
      }
      railNode->type = node->type;
      railNode->direction = node->direction;
      railNode->segmentId = node->segmentId;
      railNode->laneId = node->laneId;
      railNode->laneWidth = node->laneWidth;
      if (node->isWaypoint)
        railNode->waypointId = node->waypointId;
      else
        railNode->waypointId = 0;
      railNode->isWaypoint = false;
      railNode->isCheckpoint = false;
      railNode->isEntry = node->isEntry;
      railNode->isExit = node->isExit;
      railNode->isStop = node->isStop;
      railNode->distToStop = node->distToStop;
      railNode->pose = node->pose;
      railNode->steerAngle = node->steerAngle;
      railNode->railId = rail;

      // shift on left/right
      quat_to_rpy(node->pose.rot, &r,&p,&y);
      dx =  rail*sin(y)*((railNode->laneWidth-VEHICLE_WIDTH)/2.0);
      dy = -rail*cos(y)*((railNode->laneWidth-VEHICLE_WIDTH)/2.0);
      railNode->pose.pos.x += dx;
      railNode->pose.pos.y += dy;

      // construct conversion matrix
      pose3_to_mat44d(railNode->pose, railNode->transGN);
      mat44d_inv(railNode->transNG, railNode->transGN);

      // connect the rail
      prevNode = graph->getPrevLaneNode(node);
      if (prevNode) {
        if (rail == -1) arc = graph->createArc(prevNode->leftNode->index, railNode->index);
        else arc = graph->createArc(prevNode->rightNode->index, railNode->index);
        if (!arc) return ERROR("unable to create arc; try increasing maxArcs");
        arc->type = GRAPH_ARC_NODE;
      }
    }
  }

  return 0;
}

// Generate lane tangents
int GraphUtils::genLaneTangents(Graph_t *graph,std::Lane *lane, int direction)
{
  int i;
  GraphNode *na, *nb, *nc;
  double th;

  for (i = 0; i < graph->getNodeCount(); i++)
  {
    nb = graph->getNode(i);
    if (!(nb->type & GRAPH_NODE_LANE)) continue;
    if (nb->laneId != lane->getLaneID() || nb->segmentId != lane->getSegmentID()) continue;
    if (nb->direction != direction) continue;

    na = graph->getPrevLaneNode(nb);
    nc = graph->getNextLaneNode(nb);

    // Crude tangent calculation across nearby points.
    if (na && nc)
    {
      th = atan2(nc->pose.pos.y - na->pose.pos.y,
                 nc->pose.pos.x - na->pose.pos.x);
    }
    else if (na)
    {
      th = atan2(nb->pose.pos.y - na->pose.pos.y,
                 nb->pose.pos.x - na->pose.pos.x);
    }
    else if (nc)
    {
      th = atan2(nc->pose.pos.y - nb->pose.pos.y,
                 nc->pose.pos.x - nb->pose.pos.x);
    }
    else {
      MSG("RNDF is probably malformed on lane %d.%d", nb->segmentId, nb->laneId);
      // Keep old th
    }

    // Set the node orientation
    nb->pose.rot = quat_from_rpy(0, 0, th);

    // Construct transforms
    pose3_to_mat44d(nb->pose, nb->transGN);
    mat44d_inv(nb->transNG, nb->transGN);
  }

  return 0;
}

// Generate lane-change maneuvers
// Generate turn maneuvers
int GraphUtils::genTurns( Graph_t *graph,std::RNDF *rndf, vector<int> segments, vector<int> new_segments)
{
  int sn, ln, wn, vn;
  std::Segment *segmentA, *segmentB;
  std::Lane *laneA, *laneB;
  std::Waypoint *wpA;
  std::GPSPoint *wpB;
  GraphNode *nodeA, *nodeB;
  GraphArc *arc;
  GraphUtils::PlannerKinematics kin;

  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;
 
  // Iterate through waypoints in the RNDF
  for (sn = 0; sn < (int) rndf->getAllSegments().size(); sn++)
  {
    segmentA = rndf->getAllSegments()[sn];
    if (!segmentA)
      continue;          
    assert(segmentA);

    if (!contains(segments, segmentA->getSegmentID())) continue;
    for (ln = 0; ln < (int) segmentA->getAllLanes().size(); ln++)
    {
      laneA = segmentA->getAllLanes()[ln];
      for (wn = 0; wn < (int) laneA->getAllWaypoints().size(); wn++)
      {
        wpA = laneA->getAllWaypoints()[wn];

        for (vn = 0; vn < (int) wpA->getEntryPoints().size(); vn++)
        {
          wpB = wpA->getEntryPoints()[vn];

          // TODO: handle zones                    
          segmentB = rndf->getSegment(wpB->getSegmentID());
          if (!segmentB)
            continue;          
          assert(segmentB);

          if (!contains(segments, segmentB->getSegmentID())) continue;
          if (!contains(new_segments, segmentB->getSegmentID()) && !contains(new_segments, segmentA->getSegmentID())) continue;

          laneB = segmentB->getLane(wpB->getLaneID());
          assert(laneB);

          MSG("turn %d.%d.%d %d.%d.%d",
              wpA->getSegmentID(), wpA->getLaneID(), wpA->getWaypointID(),
              wpB->getSegmentID(), wpB->getLaneID(), wpB->getWaypointID());
          
          nodeA = graph->getNodeFromRndfId(wpA->getSegmentID(),
                                                 wpA->getLaneID(),
                                                 wpA->getWaypointID());
          assert(nodeA);
          nodeB = graph->getNodeFromRndfId(wpB->getSegmentID(),
                                                 wpB->getLaneID(),
                                                 wpB->getWaypointID());
          // Skip turns from zones
          if (!nodeB)
            continue;
          // Skip turns to the same node
          if (nodeA == nodeB) continue;

          bool connect = false;
          GraphNode *nextNode, *prevNode;

          // Generate right-to-right/center-to-center/left-to-left turns
          genManeuver(graph, GRAPH_NODE_TURN, nodeA->leftNode, nodeB->leftNode, kin.maxSteer);
          genManeuver(graph, GRAPH_NODE_TURN, nodeA->rightNode, nodeB->rightNode, kin.maxSteer);
          connect |= (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB->leftNode, kin.maxSteer) == 0);
          connect |= (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB->rightNode, kin.maxSteer) == 0);
          connect |= (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB, kin.maxSteer) == 0);
          // If any of these turn worked then connect at low resolution
          if (connect) goto genTurns_end;

          // If none of the turns could be generated then try with the later nodes on the entry lane
          nextNode = graph->getNextLaneNode(nodeB);
          if (nextNode && !nextNode->isWaypoint) {
            nextNode->waypointId = nodeB->waypointId;
            nextNode->rightNode->waypointId = nodeB->waypointId;
            nextNode->leftNode->waypointId = nodeB->waypointId;
            if (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nextNode->rightNode, kin.maxSteer) == 0) goto genTurns_end;
            if (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nextNode->leftNode, kin.maxSteer) == 0) goto genTurns_end;
            
            nextNode = graph->getNextLaneNode(nextNode);
            if (nextNode && !nextNode->isWaypoint) {
              nextNode->waypointId = nodeB->waypointId;
              nextNode->rightNode->waypointId = nodeB->waypointId;
              nextNode->leftNode->waypointId = nodeB->waypointId;
              if (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nextNode->rightNode, kin.maxSteer) == 0) goto genTurns_end;
              if (genManeuver(graph, GRAPH_NODE_TURN, nodeA, nextNode->leftNode, kin.maxSteer) == 0) goto genTurns_end;
            }
          }

          // If none of the turns could be generated then try with the preivous nodes on the exit lane
          prevNode = graph->getPrevLaneNode(nodeA);
          if (prevNode && !prevNode->isWaypoint) {
            prevNode->isStop = nodeA->isStop;
            prevNode->waypointId = nodeA->waypointId;
            prevNode->distToStop = vec3_mag(vec3_sub(nodeA->pose.pos, prevNode->pose.pos));
            nextNode = graph->getNextLaneNode(nodeB);
            if (nextNode && !nextNode->isWaypoint) {
              if (genManeuver(graph, GRAPH_NODE_TURN, prevNode, nextNode->rightNode, kin.maxSteer) == 0) goto genTurns_end;
              if (genManeuver(graph, GRAPH_NODE_TURN, prevNode, nextNode->leftNode, kin.maxSteer) == 0) goto genTurns_end;
            
              nextNode = graph->getNextLaneNode(nextNode);
              if (nextNode && !nextNode->isWaypoint) {
                if (genManeuver(graph, GRAPH_NODE_TURN, prevNode, nextNode->rightNode, kin.maxSteer) == 0) goto genTurns_end;
                if (genManeuver(graph, GRAPH_NODE_TURN, prevNode, nextNode->leftNode, kin.maxSteer) == 0) goto genTurns_end;
              }
            }
          }

          // Generate a tight-turn maneuver (which may not be
          // feasible).  This allows the robot to take the turn by
          // veering outside the lane.
          if (prevNode && !prevNode->isWaypoint) {
            nextNode = graph->getNextLaneNode(nodeB);
            if (nextNode && !nextNode->isWaypoint && genManeuver(graph, GRAPH_NODE_TURN, prevNode, nextNode, kin.maxTurn) == 0) goto genTurns_end;
            if (genManeuver(graph, GRAPH_NODE_TURN, prevNode, nodeB, kin.maxTurn) == 0) goto genTurns_end;
          } else {
            nextNode = graph->getNextLaneNode(nodeB);
            if (nextNode && !nextNode->isWaypoint && genManeuver(graph, GRAPH_NODE_TURN, nodeA, nextNode, kin.maxTurn) == 0) goto genTurns_end;
            genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB, kin.maxTurn);
          }

genTurns_end:
          if (connect) {
            // Create arc to connect waypoints
            arc = graph->createArc(nodeA->index, nodeB->index);
            if (!arc)
              return ERROR("unable to create arc; try increasing maxArcs");
            arc->type = GRAPH_ARC_WAYPOINT;
          }
        }
      }
    }
  }
  
  return 0;
}

// Generate lane-change maneuvers
int GraphUtils::genChanges( Graph_t *graph,std::RNDF *rndf, int segment)
{
  int numNodes;
  int na, nb;
  GraphNode *nodeA, *nodeB;
  vec3_t pa, pb;
  double dm;
  GraphUtils::PlannerKinematics kin;

  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;
  
  numNodes = graph->getNodeCount();

  // Consider all pairs of lane nodes
  for (nb = 0; nb < numNodes; nb++)
  {
    nodeB = graph->getNode(nb);
    if (nodeB->segmentId != segment) continue;

    for (na = 0; na < numNodes; na++)
    {
      nodeA = graph->getNode(na);
      if (nodeA->segmentId != segment) continue;

      // Space out the lane-changes so we dont overload the graph
      if (!(nodeA->index % CHANGES_SPACING) == 0) // MAGIC
        continue;

      // Consider lane nodes only
      if (!(nodeA->type == GRAPH_NODE_LANE && nodeB->type == GRAPH_NODE_LANE))
        continue;

      // Look for nodes that are the same segment
      if (nodeA->segmentId != nodeB->segmentId)
        continue;

      // Only connect nodes in the same lane if they are on different rails
      if (nodeA->laneId == nodeB->laneId && nodeA->railId == nodeB->railId)
        continue;
      if (nodeA->laneId == nodeB->laneId && nodeA->direction != nodeB->direction)
        continue;
      if (nodeA->laneId != nodeB->laneId && nodeA->direction == nodeB->direction)
        continue;
      // Only connect rail on left and right to zero
      if (nodeA->railId != 0 && nodeB->railId != 0 && nodeA->laneId == nodeB->laneId)
        continue;
      if (nodeB->railId != 0 && nodeA->laneId != nodeB->laneId)
        continue;

      // Nodes must have similar alignment
      pa = vec3_rotate(nodeA->pose.rot, vec3_set(1, 0, 0));
      pb = vec3_rotate(nodeB->pose.rot, vec3_set(1, 0, 0));
      if (acos(vec3_dot(pa, pb)) > 45 * M_PI/180) // MAGIC
        continue;

      // Nodes must be close, but not too close
      dm = vec3_mag(vec3_sub(nodeB->pose.pos, nodeA->pose.pos));
      if (!(dm > 15.5-LANE_STEP_SIZE/2.0 && dm < 15.5+LANE_STEP_SIZE/2.0)) // MAGIC HACK
        continue;

      // The destination must be ahead of the source
      pb = vec3_transform(pose3_inv(nodeA->pose), nodeB->pose.pos);
      if (pb.x < 0)
        continue;

      // MSG("change %d %d.%d %d %d.%d %d",
      //    na, nodeA->segmentId, nodeA->laneId,
      //    nb, nodeB->segmentId, nodeB->laneId,
      //    graph->getNodeCount());

      // Do not create the lane change maneuver if it would bypass an intersection
      bool abort_creation = false;
      GraphNode *node     = nodeA;
      GraphNode *nextNode = graph->getNextLaneNode(nodeA);
      if (nextNode) {
        double distToStop = vec3_mag(vec3_sub(nextNode->pose.pos, node->pose.pos));
        while (nextNode) {
          distToStop += vec3_mag(vec3_sub(nextNode->pose.pos, node->pose.pos));
          if (nextNode->isStop && distToStop < 15.5+LANE_STEP_SIZE/2.0 + 10.0) {
            abort_creation = true;
            break;
          }
          if (distToStop >= 15.5+LANE_STEP_SIZE/2.0 + 10.0) break;
          node = nextNode;
          nextNode = graph->getNextLaneNode(nextNode);
        }
        if (abort_creation) continue;
      } else {
        continue;
      }
     
      // Generate the lane-change maneuver.  It is generally ok
      // if we cannot make a particular maneuver here.
      if (GraphUtils::genManeuver( graph,GRAPH_NODE_CHANGE, nodeA, nodeB, kin.maxSteer ) == ENOMEM )
        return -1;
    }
  }
    
  return 0;
}

// Generate zone nodes
int GraphUtils::genZones(Graph_t *graph, std::RNDF *rndf, vector<int> segments, vector<int> new_segments)
{
  int zn, wn, epn, sn, psn, ppn;
  std::Zone *zone;
  std::vector<std::Waypoint*> waypoints;
  GraphNode *nodeA, *nodeB, *node;
  GraphArc* arc;
  std::vector<std::GPSPoint*> entryPts;
  std::PerimeterPoint* perimeter;
  std::ParkingSpot* pspot;
  std::Waypoint *spot1, *spot2;
  point2 centroid;
  bool already_build = false;

  sn = rndf->getNumOfSegments();
  for (zn=sn+1; zn <= rndf->getNumOfZones()+sn; zn++) {
    zone = rndf->getZone(zn);

    // Check if the zone has already been constructed
    for (int i=0; i<graph->getNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->segmentId == zn) {
        already_build = true;
        break;
      }
    }

    if (!already_build) {
      if (zone->getNumOfPerimeterPoints() < 3) {
        MSG("RNDF is malformed for zone %d", zn);
        continue;
      }

      // Create all the relevant zone nodes
      // calculate the centroid of the zone
      point2 pt1, pt2, pt3, ptg;
      double area, total_area = 0.0;
      pt1.set(zone->getPerimeterPoint(1)->getNorthing()-delta.x, zone->getPerimeterPoint(1)->getEasting()-delta.y);
      for (ppn=2; ppn < zone->getNumOfPerimeterPoints(); ppn++) {
        pt2.set(zone->getPerimeterPoint(ppn)->getNorthing()-delta.x, zone->getPerimeterPoint(ppn)->getEasting()-delta.y);
        pt3.set(zone->getPerimeterPoint(ppn+1)->getNorthing()-delta.x, zone->getPerimeterPoint(ppn+1)->getEasting()-delta.y);
        area = ((pt2.x-pt1.x)*(pt3.y-pt1.y)-(pt2.y-pt1.y)*(pt3.x-pt1.x))/2.0;
        total_area += area;
        ptg.set((pt1.x + pt2.x + pt3.x)/3.0, (pt1.y + pt2.y + pt3.y)/3.0);
        centroid = centroid + area*ptg;
      }
      centroid = centroid/total_area;

      // 1. create the entry, exit and parking nodes in the graph
      for (ppn=1; ppn <= zone->getNumOfPerimeterPoints(); ppn++) {
        perimeter = zone->getPerimeterPoint(ppn);
        if (!(perimeter->isEntry()) && !(perimeter->isExit()))
          continue;

        node = graph->createNode();
        node->pose.pos = vec3_set(perimeter->getNorthing() - delta.x,
                                  perimeter->getEasting() - delta.y,
                                  0);
        double heading = atan2(centroid.y - node->pose.pos.y,
                               centroid.x - node->pose.pos.x);

        if (perimeter->isEntry())
          node->pose.rot = quat_from_rpy(0, 0, heading);
        else
          node->pose.rot = quat_from_rpy(0, 0, getAngleInRange(heading+M_PI));
        node->type = GRAPH_NODE_ZONE;
        node->direction = 1;
        node->isWaypoint = true;
        node->isExit = perimeter->isExit();
        node->isEntry = perimeter->isEntry();
        node->isStop = false;
        node->isCheckpoint = false;
        node->segmentId = perimeter->getSegmentID();
        node->laneId = perimeter->getLaneID();
        node->waypointId = perimeter->getWaypointID();
        pose3_to_mat44d(node->pose, node->transGN);
        mat44d_inv(node->transNG, node->transGN);
      }

      // 2. create the parking spot nodes, but only the ones associated 
      // with the checkpoint is necessary
      for (psn=1; psn<=zone->getNumOfSpots(); psn++) {
        pspot = rndf->getSpot(zn, psn);
        spot1 = pspot->getWaypoint(1);
        spot2 = pspot->getWaypoint(2);
        // create the fwd nodes and arcs
        node = graph->createNode();
        node->pose.pos = vec3_set(spot2->getNorthing() - delta.x,
  								spot2->getEasting() - delta.y,
  								0);
        double heading = atan2(spot2->getEasting() - spot1->getEasting(),
                               spot2->getNorthing() - spot1->getNorthing());
        node->pose.rot = quat_from_rpy(0, 0, heading);
        node->type = GRAPH_NODE_ZONE;
        node->direction = 1;
        node->isWaypoint = true;
        node->isEntry = false;
        node->isExit = false;
        node->isStop = false;
        node->isCheckpoint = true;
        node->segmentId = zn;
        node->laneId = psn;
        node->waypointId = 2;
        pose3_to_mat44d(node->pose, node->transGN);
        mat44d_inv(node->transNG, node->transGN);
      }

      // 3. create internal arcs
      // connect all the entrypts on the perimeter to the parking spots
      for (ppn=1; ppn <= zone->getNumOfPerimeterPoints(); ppn++) {
        perimeter = zone->getPerimeterPoint(ppn);
        if (!perimeter->isEntry())
          continue;
        nodeA = graph->getNodeFromRndfId(zn, 0, ppn);
        assert(nodeA);
        for (psn=1; psn<=zone->getNumOfSpots(); psn++) {
          pspot = rndf->getSpot(zn, psn);
          // spot2 = pspot->getWaypoint(2);
          nodeB = graph->getNodeFromRndfId(zn, psn, 2);
          assert(nodeB);
          GraphUtils::genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB, MAXTURN);
          arc = graph->createArc(nodeA->index, nodeB->index);
          arc->type = GRAPH_ARC_WAYPOINT;
        }
      }
    
      // connect the parking spots to the exit pts
      for (ppn=1; ppn <= zone->getNumOfPerimeterPoints(); ppn++) {
        perimeter = zone->getPerimeterPoint(ppn);
        if (!perimeter->isExit())
          continue;
        nodeB = graph->getNodeFromRndfId(zn,0, ppn);
        assert(nodeB);
        for (psn=1; psn<=zone->getNumOfSpots(); psn++) {
          pspot = rndf->getSpot(zn, psn);
          // spot2 = pspot->getWaypoint(2);
          nodeA = graph->getNodeFromRndfId(zn, psn, 2);
          assert(nodeA);
          GraphUtils::genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB, MAXTURN);
          arc = graph->createArc(nodeA->index, nodeB->index);
          arc->type = GRAPH_ARC_WAYPOINT;
        }
      }
    }

    // Create external arcs
    // 1. find the segments exit pts into this zone.
    waypoints = getSegmentsExitPts(rndf, zn);
    // 2. Connect the road and the zone entry pt (perimeter)
    for (wn=0; wn < (int)waypoints.size(); wn++) {
      // Only connect if the segment has just been loaded
      if (!contains(new_segments, waypoints[wn]->getSegmentID())) continue;

      nodeA = graph->getNodeFromRndfId(waypoints[wn]->getSegmentID(), 
                                       waypoints[wn]->getLaneID(),
                                       waypoints[wn]->getWaypointID());
      assert(nodeA);

      // find all the waypoints in the current zone that this road pt connects to
      entryPts = waypoints[wn]->getEntryPoints();
      for (epn=0; epn < (int) entryPts.size(); epn++) {
        if (entryPts[epn]->getSegmentID() == zn){
          // create the perimeter entry node
          nodeB = graph->getNodeFromRndfId(entryPts[epn]->getSegmentID(),
                                           entryPts[epn]->getLaneID(),
                                           entryPts[epn]->getWaypointID());
          // create the arc from road to zone
          assert(nodeB);
          GraphUtils::genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB, MAXTURN);
          arc = graph->createArc(nodeA->index, nodeB->index);
          arc->type = GRAPH_ARC_WAYPOINT;
        }
      }      
    }
    
    // connect the perimeter exit pt to all its segment entry pts
    for (ppn=1; ppn <= zone->getNumOfPerimeterPoints(); ppn++) {
      perimeter = zone->getPerimeterPoint(ppn);
      if (!perimeter->isExit())
        continue;
      nodeA = graph->getNodeFromRndfId(zn, 0, ppn);
      assert(nodeA);
      entryPts = perimeter->getEntryPoints();
      for (epn=0; epn < (int) entryPts.size(); epn++) {
        // Only connect the newly loaded segments
        if (!contains(new_segments, entryPts[epn]->getSegmentID())) continue;

        nodeB = graph->getNodeFromRndfId(entryPts[epn]->getSegmentID(),
                                         entryPts[epn]->getLaneID(),
                                         entryPts[epn]->getWaypointID());
        assert(nodeB);
        GraphUtils::genManeuver(graph, GRAPH_NODE_TURN, nodeA, nodeB, MAXTURN);
        arc = graph->createArc(nodeA->index, nodeB->index);
        arc->type = GRAPH_ARC_WAYPOINT;
      }
    }
  }
  return 0;
}

double GraphUtils::getAngleInRange(double angle)
{
  double angleInRange = angle;

  while (angleInRange > M_PI) 
    angleInRange -= 2*M_PI;

  while (angleInRange <= -M_PI)
    angleInRange += 2*M_PI;

  return angleInRange;
}

vector<std::Waypoint*> GraphUtils::getSegmentsExitPts(std::RNDF* rndf, int zoneId)
{
  int sn, ln, wn, epn;
  std::Zone *zone;
  std::Segment *segment;
  std::Lane *lane;
  std::vector<std::Waypoint*> waypoints;
  std::Waypoint *waypt;
  std::vector<std::GPSPoint*> entryPts;

  zone = rndf->getZone(zoneId);
  for (sn=1; sn <= rndf->getNumOfSegments(); sn++) {
    segment = rndf->getSegment(sn);
    // 1. Find the entrypts into this zone
    for (ln=1; ln <= segment->getNumOfLanes(); ln++) {
      lane = segment->getLane(ln);
      for (wn=1; wn <= lane->getNumOfWaypoints(); wn++) {
        waypt = lane->getWaypoint(wn);
        if (waypt->isExit()) {
          entryPts = waypt->getEntryPoints();
          for (epn=0; epn < (int) entryPts.size(); epn++) {
            if (entryPts[epn]->getSegmentID() == zoneId) 
              waypoints.push_back(waypt);
          }
        }
      }
    }
  }
  return waypoints;
}

GraphNode *GraphUtils::getChangeNode(Graph_t *graph, const GraphNode *node, int direction)
{
  if (direction == -1) {
    return graph->getPrevChangeNode(node);
  }
  return graph->getNextChangeNode(node);
}

int GraphUtils::getChangeCount(Graph_t *graph, const GraphNode *node, int direction)
{
  GraphArc *arc;
  int cnt = 0;
  
  if (direction == -1) {
    for (arc = node->inFirst; arc != NULL; arc = arc->inNext) {
      if (arc->type != GRAPH_ARC_NODE) continue;
      if (arc->nodeA->type == GRAPH_NODE_CHANGE)
        cnt++;
    }
  } else {
    for (arc = node->outFirst; arc != NULL; arc = arc->outNext) {
      if (arc->type != GRAPH_ARC_NODE) continue;
      if (arc->nodeA->type == GRAPH_NODE_CHANGE)
        cnt++;
    }
  }

  return cnt;
}

void GraphUtils::getChangeNodes(vector<GraphNode *> &nodes, GraphNode **nodeA, GraphNode **nodeB, Graph_t *graph, const GraphNode *node, int direction, int index)
{
  int cnt = 0;
  GraphArc *arc;
  GraphNode *start = NULL;
  GraphNode *end = NULL;

  nodes.clear();
  
  // Find start of the maneuver
  if (direction == -1) {
    for (arc = node->inFirst; arc != NULL; arc = arc->inNext) {
      if (arc->type != GRAPH_ARC_NODE) continue;
      if (arc->nodeA->type == GRAPH_NODE_CHANGE) {
        if (cnt == index) {
          start = arc->nodeA;
          break;
        }
        cnt++;
      }
    }
  } else {
    for (arc = node->outFirst; arc != NULL; arc = arc->outNext) {
      if (arc->type != GRAPH_ARC_NODE) continue;
      if (arc->nodeB->type == GRAPH_NODE_CHANGE) {
        if (cnt == index) {
          start = arc->nodeB;
          break;
        }
        cnt++;
      }
    }
  }

  // Now step through the node until a lane node
  end = start;
  while (end != NULL && !(end->type & GRAPH_NODE_LANE)) {
    nodes.push_back(end);
    if (direction == -1) {
      if (!end->inFirst) {
        end = NULL;
        break;
      }
      end = end->inFirst->nodeA;
    } else {
      if (!end->outFirst) {
        end = NULL;
        break;
      }
      end = end->outFirst->nodeB;
    }
  }
  if (!end) {
    nodes.clear();
    *nodeA = NULL;
    *nodeB = NULL;
    return;
  }
  nodes.push_back(end);

  if (direction == -1) {
    *nodeA = end;
    *nodeB = start;
    vector<GraphNode *> n = nodes;
    nodes.clear();
    for (unsigned int i=0; i<n.size(); i++) {
      nodes.push_back(n[n.size()-i-1]);
    }
  } else {
    *nodeA = start;
    *nodeB = end;
  }
}

GraphNode *GraphUtils::getLaneNode(Graph_t *graph, const GraphNode *node, int direction)
{
  if (direction == -1) {
    return graph->getPrevLaneNode(node);
  }
  return graph->getNextLaneNode(node);
}

bool GraphUtils::isReverse(VehicleState &vehState, Map *map, LaneLabel &lane)
{ 
  double currlane_angle;
  point2 pt;
    
  double alice_angle = AliceStateHelper::getHeading(vehState);
  point2 alice_rearaxle = AliceStateHelper::getPositionRearAxle(vehState);

  map->getHeading(currlane_angle, pt, lane, alice_rearaxle);
  double diff_angle = alice_angle-currlane_angle;
  while (diff_angle > M_PI) diff_angle -= 2*M_PI;
  while (diff_angle < -M_PI) diff_angle += 2*M_PI;
  diff_angle = fabs(diff_angle);
  
  return (diff_angle > M_PI/2);
}

bool GraphUtils::contains(vector<int> segments, int x)
{
  for (unsigned int i = 0; i<segments.size(); i++) {
    if (x == segments[i]) return true;
  }
  return false;
}

int GraphUtils::genUturnGraph(Graph_t **gUturn, Map *map, pose3_t &finalPose, point2 &ref_pt,
                              GraphNode **nodeVeh, VehicleState &vehState, ActuatorState &actState,
                              const Uturn_t uTurn) {

  // TODO list:
  // 1. Need to define step sizes in terms of Alice's width, length
  // 2. Need to give preference (weight) to nodes on the centerline
  // 3. Need to overload Graph's constructor
  // 4. Need to define new node->type (?)

  /* GENERATE GRAPH
   * Generate the Graph to use during Uturn     
   */
  *gUturn = new Graph_t(MAXNODENUM_UTURN, MAXARCSNUM_UTURN);  //*gUturn = new Graph( MAXNODENUM_UTURN, MAXARCSNUM_UTURN, uTurn ); 

  (*gUturn)->type = uTurn;

  // Variables needed
  LaneLabel curr_lane, opp_lane;
  point2 start_pt, tmp_pt, pt, obs_pt;
  point2arr centerline;
  double heading;
  GraphUtils::PlannerKinematics kin;
  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;
  kin.maxTurn = MAXTURN * M_PI/180;

  // Get our current position
  start_pt.x = vehState.localX;
  start_pt.y = vehState.localY;
  
  bool deb = false;
  if(deb) cout << endl << "   ~:: GEN UTURN GRAPH ::~" << endl; //FZ
  if(deb) cout << "1. Done vehicle state: (start_pt.x = " << start_pt.x << ", start_pt.y = " << start_pt.y << ")" << endl; //FZ
  
  // Get the current lane and heading
  map->getLane(curr_lane, start_pt);
  map->getHeading(heading, pt, curr_lane, start_pt); // pt is not used, just getting the lane heading
  
  if(deb) cout << "2. Done getHeading (lane):    heading = " << heading << " [rad]" << endl; //FZ
  if(deb) cout << "   Done getHeading (vehicle): heading = " << vehState.localYaw << " [rad]" << endl; //FZ
  
  // Get the nearest point to the obstacle (Road Block)
  // Set the reference point (Nominal)
  if(uTurn == UTURN_OBS) {
    getNearestObsPoint(obs_pt, map, vehState, curr_lane, 0.0);
    if(deb) cout << "   obs_pt = (" << obs_pt.x << ", " << obs_pt.y << ")" << endl;
  
  } else if(uTurn == UTURN_DARPA) {

    if(deb) cout << "   obs_pt = ref_pt = (" << ref_pt.x << ", " << ref_pt.y << ")" << endl;
    obs_pt = ref_pt;
  }

  if(deb) {
    // Distance between points

    double dx = start_pt.x - obs_pt.x;
    double dy = start_pt.y - obs_pt.y;
    double dm = sqrt(dx*dx + dy*dy);
    cout << "   dm (start & obs) = (" << dm << ")" << endl;
  }
  
  /* GENERATE REFERENCE NODE
   * Create a node at the reference point 
   */
  GraphNode *nodeRef = (*gUturn)->createNode();
    if (!nodeRef)
      return ERROR("unable to create node; try increasing maxNodes");
  
  // Might need a different node type, lane for now. Should be Type obstacle.
  nodeRef->type = GRAPH_NODE_LANE;
  
  // Node collides with an obstacle
  nodeRef->collideObs = true;
  
  // Set obstacle node's pose
  nodeRef->pose.pos = vec3_set(obs_pt.x,
                               obs_pt.y,
                               0);
  // Reference node uses lane heading as opposed to localYaw
  nodeRef->pose.rot = quat_from_rpy(0, 0, heading);
  
  // Reference node, setting direction with traffic
  nodeRef-> direction = 1;
  
  nodeRef->pathDir = GRAPH_PATH_REV; //FZ
  // Add to spatial index
  
  /* GENERATE VEHICLE NODE
   * Create a node at the reference point 
   */
  (*nodeVeh) = (*gUturn)->createNode();
    if (!nodeVeh)
      return ERROR("unable to create node; try increasing maxNodes");
  
  // Might need a different node type, lane for now. Should be Type obstacle.
  (*nodeVeh)->type = GRAPH_NODE_LANE;
  
  // Node collides with an obstacle
  (*nodeVeh)->collideObs = false;

  (*nodeVeh)->pathDir = GRAPH_PATH_FWD; //FZ

  // Set vehicle node's pose. 
  (*nodeVeh)->pose.pos = vec3_set(vehState.localX,
                                  vehState.localY,
                                  0);
  if(deb) cout << "    [GUt]Inside genUTgraph... vehState = (" << vehState.localX << ", " << vehState.localY << ")" << endl;
  if(deb) cout << "    [GUt]Inside genUTgraph... (*nodeVeh)->pose.pos (x = " << (*nodeVeh)->pose.pos.x << ", y = "
                << (*nodeVeh)->pose.pos.y << ")" << endl;

  // Reference node uses lane heading as opposed to localYaw
  (*nodeVeh)->pose.rot = quat_from_rpy(0, 0, heading);

  // Vehicle node, setting direction with traffic
  (*nodeVeh)->direction = 1;

  // (*nodeVeh)->isReverse = true;

  // Add to spatial index

  // Add to the graph as vehicle node
  (*gUturn)->vehicleNode = (*nodeVeh);

  /*
   *  Create densified subgraph by sectors
   *  __________________________________
   *  | sector1                sector3
   *  |  ::::::      A         ::::::     Legend: A  (Alice, position unknown)
   *  |  ::::::                ::::::             x  (exit node)
   *  O----------------------------------        --- (centerline)
   *  |        ::::::::::::::: ......            :.. (nodes)
   *  |        ::::::::::::::: :::::x             O  (obstacle pt, reference)
   *  |            sector2     sector4
   *  |_________________________________
   */

  // Declare the necessary variables
  GraphNode *nodeTmp;

  point2 iter_pt = obs_pt;            // Initialized to the reference point (obs_pt)
  double theta;
  theta = heading;                    // theta = atan2(tmp_pt.y - iter_pt.y, tmp_pt.x - iter_pt.x);  
  double sectorAngle;
  const double xStepSize = 0.50;      // 30cm step size, x direction
  const double yStepSize = 1.25;      // 30cm step size, y direction
  int rail_id;
  rail_id = 0;                        // railId = 0 is a node on the centerline

  /* SECTOR1:
   * Create densified subgraph
   * Current lane, x: 3m -- 9m (from obstacle), y: from 0m -- 2.1m (from centerline)
   * direction: 1 (with traffic), sectorAngle: heading - M_PI/3, reverse 
   */
  sectorAngle = heading - M_PI;
  m_sectorIndex[0] = 2;            //account for obstacle Node, and Vehicle node
  
  for(double dx = 4.0; dx <= 7.0; dx += xStepSize) {
    tmp_pt.x = iter_pt.x - dx*cos(heading);
    tmp_pt.y = iter_pt.y - dx*sin(heading);

    for(double dy = 1.0; dy <= 3.5; dy += yStepSize) {
      tmp_pt.x -= yStepSize*sin(heading);
      tmp_pt.y += yStepSize*cos(heading);

      nodeTmp = (*gUturn)->createNode();
        if (!nodeRef)
          return ERROR("unable to create node; try increasing maxNodes");

      // Might need a different node type, lane for now. Should be Type obstacle.
      nodeTmp->type = GRAPH_NODE_LANE;

      // Node collides with an obstacle
      nodeTmp->collideObs = false;

      // Set obstacle node's pose
      nodeTmp->pose.pos = vec3_set(tmp_pt.x,
                                   tmp_pt.y,
                                   0);
      // Reference node uses lane heading as opposed to localYaw
      nodeTmp->pose.rot = quat_from_rpy(0, 0, sectorAngle);

      // Add to spatial index

      // Setting direction with traffic
      nodeTmp->direction = 1;

      // nodeTmp->isReverse = true;
      nodeTmp->pathDir = GRAPH_PATH_REV;

    }
  }
  m_numNodes = (*gUturn)->getNodeCount();
  m_sectorNodes[0] = m_numNodes - m_sectorIndex[0]; //account for obstacle Node, and Vehicle node
  
  /* SECTOR2:
   * Create densified subgraph
   * Opposite lane, x: 9m -- 21m (from obstacle), y: from 0m -- 2.1m (from centerline)
   * direction: ?, sectorAngle: heading - M_PI/2, forward
   */
  sectorAngle = heading - M_PI/2;
  m_sectorIndex[1] = m_numNodes;
  
  for(double dx = 11.0; dx <= 19.0; dx += xStepSize) {
    tmp_pt.x = iter_pt.x - dx*cos(heading);
    tmp_pt.y = iter_pt.y - dx*sin(heading);

    for(double dy = 1.0; dy <=4.75; dy += yStepSize) {
      tmp_pt.x += yStepSize*sin(heading);
      tmp_pt.y -= yStepSize*cos(heading);

      nodeTmp = (*gUturn)->createNode();
        if (!nodeRef)
          return ERROR("unable to create node; try increasing maxNodes");

      // Might need a different node type, lane for now. Should be Type obstacle.
      nodeTmp->type = GRAPH_NODE_LANE;

      // Node collides with an obstacle
      nodeTmp->collideObs = false;

      // Set obstacle node's pose
      nodeTmp->pose.pos = vec3_set(tmp_pt.x,
                                   tmp_pt.y,
                                   0);
      // Reference node uses lane heading as opposed to localYaw
      nodeTmp->pose.rot = quat_from_rpy(0, 0, sectorAngle);

      // Add to spatial index

      // Setting direction against traffic
      nodeTmp-> direction = -1;

      nodeTmp->pathDir = GRAPH_PATH_FWD;

    }
  }
  m_numNodes = (*gUturn)->getNodeCount();
  m_sectorNodes[1] = m_numNodes - m_sectorIndex[1];

  /* SECTOR3:
   * Create densified subgraph
   * Opposite lane, x: 24 -- 30m (from obstacle), y: from 0m -- 2.1m (from centerline)
   * direction: ?, sectorAngle: heading, reverse
   */
  sectorAngle = heading - M_PI/2;
  m_sectorIndex[2] = m_numNodes;

  for(double dx = 25.0; dx <= 30.0; dx += xStepSize) {
    tmp_pt.x = iter_pt.x - dx*cos(heading);
    tmp_pt.y = iter_pt.y - dx*sin(heading);

    for(double dy = 1.0; dy <= 3.5; dy += yStepSize) {
      tmp_pt.x -= yStepSize*sin(heading);
      tmp_pt.y += yStepSize*cos(heading);

      nodeTmp = (*gUturn)->createNode();
        if (!nodeRef)
      return ERROR("unable to create node; try increasing maxNodes");

      // Might need a different node type, lane for now. Should be Type obstacle.
      nodeTmp->type = GRAPH_NODE_LANE;

      // Node collides with an obstacle
      nodeTmp->collideObs = false;

      // Set obstacle node's pose
      nodeTmp->pose.pos = vec3_set(tmp_pt.x,
                                   tmp_pt.y,
                                   0);

      // Reference node uses lane heading as opposed to localYaw
      nodeTmp->pose.rot = quat_from_rpy(0, 0, sectorAngle);

      // Add to spatial index

      // Setting direction with traffic
      nodeTmp-> direction = 1;

      // This portion is in reverse
      // nodeTmp->isReverse = true;

      nodeTmp->pathDir = GRAPH_PATH_REV;

    }
  }
  m_numNodes = (*gUturn)->getNodeCount();
  m_sectorNodes[2] = m_numNodes - m_sectorIndex[2];
  
  /* SECTOR4:
   * Create densified subgraph
   * Opposite lane, x: 24 -- 30m (from obstacle), y: from 0m -- 2.1m (from centerline)
   * direction: 1 (with traffic), sectorAngle: heading - M_PI, forward
   */
  sectorAngle = heading - M_PI;
  m_sectorIndex[3] = m_numNodes;
  for(double dx = 25.0; dx <= 30.0; dx += xStepSize) {
    tmp_pt.x = iter_pt.x - dx*cos(heading);
    tmp_pt.y = iter_pt.y - dx*sin(heading);

    for(double dy = 1.0; dy <= 3.5; dy += yStepSize) {
      tmp_pt.x += yStepSize*sin(heading);
      tmp_pt.y -= yStepSize*cos(heading);

      nodeTmp = (*gUturn)->createNode();
        if (!nodeRef)
          return ERROR("unable to create node; try increasing maxNodes");

      // Might need a different node type, lane for now. Should be Type obstacle.
    nodeTmp->type = GRAPH_NODE_LANE;

      // Node collides with an obstacle
      nodeTmp->collideObs = false;

      // Set obstacle node's pose
      nodeTmp->pose.pos = vec3_set(tmp_pt.x,
                                   tmp_pt.y,
                                   0);
      // Reference node uses lane heading as opposed to localYaw
      nodeTmp->pose.rot = quat_from_rpy(0, 0, sectorAngle);

      // Add to spatial index

      // Setting direction against traffic
      nodeTmp-> direction = 1;

      nodeTmp->pathDir = GRAPH_PATH_FWD;

    }
  }
  m_numNodes = (*gUturn)->getNodeCount();
  m_sectorNodes[3] = m_numNodes - m_sectorIndex[3];

  if(deb) cout << "SECTOR4: Index = " << m_sectorIndex[3] << " m_sectorNodes = " << m_sectorNodes[3] << " m_numNodes = " << m_numNodes << endl;

  (*gUturn)->freezeStatic();

  if(deb) cout << "    [GUt]Inside genUTgraph... (*nodeVeh)->pose.pos (x = " << (*nodeVeh)->pose.pos.x
                << ", y = " << (*nodeVeh)->pose.pos.y << ")" << endl;

  if(deb) cout << "Graph done, about to call genUTurnManeuvers" << endl;

  // Set the desired final pose (should be set by genUTurnManeuvers) 
  finalPose.pos = nodeTmp->pose.pos;
  finalPose.rot = nodeTmp->pose.rot;

  return 0;
}

int GraphUtils::genUturnManeuvers(Graph_t *gUturn, GraphNode **nodeVeh,
                                  VehicleState &vehState, ActuatorState &actState) {

  bool deb = false;
  /* CHECK FEASIBILITY OF MANEUVERS 
   * Verify sector-to-sector linking of nodes 
   */

  // Variables needed 
  GraphUtils::PlannerKinematics kin;
  GraphUtils::PlannerConstraints cons;

  kin.wheelBase = VEHICLE_WHEELBASE;
  kin.maxSteer = MAXSTEER * M_PI/180;

 cons.enableReverse =       0;
  cons.centerCost    =    1000;
  cons.headOnCost    =  100000;
  cons.offRoadCost   =   10000;
  cons.reverseCost   =   10000;
  cons.changeCost    =   50000;
  cons.obsCost       = 1000000;
  cons.carCost       =       0;

  Vehicle *vp;
  Maneuver *mp;
  Pose2D poseTo, poseFrom;
  // VehicleConfiguration config, configFrom;
  double roll, pitch, yaw;
  GraphNode *nodeFrom, *nodeTo;
  GraphArc *arc;
  VehicleConfiguration configA, config;

  /* SECTOR4:~:SECTOR1
   * Determine feasible links, forward (from 1 TO 4)
   */
  int numRan = 0, numFailed = 0; bool feasible;
  
  if(deb) cout << "About to compare S4 :~: S1" << endl;
  if(deb) cout << "OuterLoop. From: " << m_sectorIndex[0] << " To: " << m_sectorIndex[0]+m_sectorNodes[0] << endl;
  if(deb) cout << "InnerLoop. From: " << m_sectorIndex[3] << " To: " << m_sectorIndex[3]+m_sectorNodes[3] << endl;
  
  for (int i = m_sectorIndex[0]; i < m_sectorIndex[0]+m_sectorNodes[0]; i++) {
    nodeFrom = gUturn->getNode(i);
    for (int j = m_sectorIndex[3]; j < m_sectorIndex[3]+m_sectorNodes[3]; j++) {
      nodeTo = gUturn->getNode(j);
  
      // Initial pose
      poseFrom.x = nodeFrom->pose.pos.x;
      poseFrom.y = nodeFrom->pose.pos.y;
      quat_to_rpy(nodeFrom->pose.rot, &roll, &pitch, &yaw);
      poseFrom.theta = yaw;
      // configA.x = nodeFrom->pose.pos.x;
      // configA.y = nodeFrom->pose.pos.y;
      // quat_to_rpy(nodeFrom->pose.rot, &roll, &pitch, &yaw);
      // configA.theta = yaw;
      // configA.phi = yaw;
  
      // Final pose
      poseTo.x = nodeTo->pose.pos.x;
      poseTo.y = nodeTo->pose.pos.y;
      quat_to_rpy(nodeTo->pose.rot, &roll, &pitch, &yaw);
      poseTo.theta = yaw;
  
      // Vehicle properties
      vp = maneuver_create_vehicle(kin.wheelBase, kin.maxSteer);
  
     // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseFrom, &poseTo);
      // mp = maneuver_config2pose(vp, &configA, &poseTo);
      numRan++;
      // Some maneuvers may not be possible, so trap this.
      if (!mp) {
        maneuver_free(mp);
        free(vp);
        numFailed++;
        continue;
      }

      // config = maneuver_evaluate_configuration(vp, mp, j);

      if(deb && configA.x) cout << "config.phi = " << config.phi << ", vp->steerlimit = " << vp->steerlimit << endl;

      // Discard turns outside the steering limit
      // if (fabs(config.phi) > vp->steerlimit) {
      //   feasible = false;
      //   continue;
      // }

      // Create an arc from the previous node to the new node
      arc = gUturn->createArc(nodeFrom->index, nodeTo->index);
      if (!arc) {
        ERROR("unable to create arc; try increasing maxArcs");
        return ENOMEM;
      }
      arc->type = GRAPH_ARC_NODE;
    }
  }
  if(deb) cout << "numRan = " << numRan << ", numFailed = " << numFailed << endl;
  maneuver_free(mp);
  free(vp);

  /* SECTOR1:~:SECTOR2
   * Determine feasible links, reverse (from 2 TO 1)
   */

  if(deb) cout << "About to compare S4 :~: S1" << endl;
  if(deb) cout << "OuterLoop. From: " << m_sectorIndex[1] << " To: " << m_sectorIndex[1]+m_sectorNodes[1] << endl;
  if(deb) cout << "InnerLoop. From: " << m_sectorIndex[0] << " To: " << m_sectorIndex[0]+m_sectorNodes[0] << endl;

  for (int i = m_sectorIndex[1]; i < m_sectorIndex[1]+m_sectorNodes[1]; i++) {
    nodeFrom = gUturn->getNode(i);
    for (int j = m_sectorIndex[0]; j < m_sectorIndex[0]+m_sectorNodes[0]; j++) {
      nodeTo = gUturn->getNode(j);

      // Initial pose
      poseFrom.x = nodeFrom->pose.pos.x;
      poseFrom.y = nodeFrom->pose.pos.y;
      quat_to_rpy(nodeFrom->pose.rot, &roll, &pitch, &yaw);
      poseFrom.theta = yaw;
      // Final pose
      poseTo.x = nodeTo->pose.pos.x;
      poseTo.y = nodeTo->pose.pos.y;
      quat_to_rpy(nodeTo->pose.rot, &roll, &pitch, &yaw);
      poseTo.theta = yaw;

      // Vehicle properties
      vp = maneuver_create_vehicle(kin.wheelBase, kin.maxSteer);

      // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseFrom, &poseTo);

      // Some maneuvers may not be possible, so trap this.
      if (!mp) {
        maneuver_free(mp);
        free(vp);
        continue;
      }

      // Create an arc from the previous node to the new node
      arc = gUturn->createArc(nodeFrom->index, nodeTo->index);
      if (!arc) {
        ERROR("unable to create arc; try increasing maxArcs");
        return ENOMEM;
      }
      arc->type = GRAPH_ARC_NODE;
    }
  }
  maneuver_free(mp);
  free(vp);
    
  /* SECTOR2:~:SECTOR3
   * Determine feasible links, forward (from 3 TO 2)
   */
  numRan = numFailed = 0;

  for (int i = m_sectorIndex[2]; i < m_sectorIndex[2]+m_sectorNodes[2]; i++) {
    nodeFrom = gUturn->getNode(i);
    for (int j = m_sectorIndex[1]; j < m_sectorIndex[1]+m_sectorNodes[1]; j++) {
      nodeTo = gUturn->getNode(j);
  
      // Initial pose
      poseFrom.x = nodeFrom->pose.pos.x; 
      poseFrom.y = nodeFrom->pose.pos.y;
      quat_to_rpy(nodeFrom->pose.rot, &roll, &pitch, &yaw);
      poseFrom.theta = yaw;
      // Final pose
      poseTo.x = nodeTo->pose.pos.x;
      poseTo.y = nodeTo->pose.pos.y;
      quat_to_rpy(nodeTo->pose.rot, &roll, &pitch, &yaw);
      poseTo.theta = yaw;

      // Vehicle properties
      vp = maneuver_create_vehicle(kin.wheelBase, kin.maxSteer);

     // Create maneuver object
      mp = maneuver_pose2pose(vp, &poseFrom, &poseTo);
      numRan++;
      // Some maneuvers may not be possible, so trap this.
      if (!mp) {
        maneuver_free(mp);
        free(vp);
        numFailed++;
        continue;
      }

      // Distance between nodes
      /*double dm = vec3_mag(vec3_sub(nodeTo->pose.pos, nodeFrom->pose.pos));
      feasible = true;
      if ( dm > sqrt(VEHICLE_MIN_TURNING_RADIUS*VEHICLE_MIN_TURNING_RADIUS + VEHICLE_MIN_TURNING_RADIUS*VEHICLE_MIN_TURNING_RADIUS) ) {
        feasible = false;
        numFailed++;
        continue;
      }*/
      // Create an arc from the previous node to the new node
      arc = gUturn->createArc(nodeFrom->index, nodeTo->index);
      if (!arc) {
        ERROR("unable to create arc; try increasing maxArcs");
        return ENOMEM;
      }
      arc->type = GRAPH_ARC_NODE;
    }
  }
  if(deb) cout << "numRan = " << numRan << ", numFailed = " << numFailed << endl;

  if(mp){
    maneuver_free(mp);
    free(vp);
  }

  //TODO: Check direct route to finalPose
  //      Check direct route to sector3

  /* VEHNODE:~:SECTOR2 (trying to skip VEHNODE:~:SECTOR3)
   * Determine feasible links, forward (from current position TO 3)
   */
  if(deb) MSG( "created %d nodes %d arcs",
               gUturn->getNodeCount(),gUturn->getArcCount() );
  if(deb) cout << "NEW Vehicle SubGraph" << endl;

  nodeFrom = (*nodeVeh);

  // GraphUpdater::genVehicleSubGraph(gUturn, vehState, actState);

  if(deb) cout << "nodeVeh->pose.pos (x = " << (*nodeVeh)->pose.pos.x << ", y = " << (*nodeVeh)->pose.pos.y << ")" << endl;

  for (int j = m_sectorIndex[2]; j < m_sectorIndex[2]+m_sectorNodes[2]; j++) {
    nodeTo = gUturn->getNode(j);

    // Initial pose
    poseFrom.x = vehState.localX;
    poseFrom.y = vehState.localY;
    // quat_to_rpy(nodeFrom->pose.rot, &roll, &pitch, &yaw);
    // poseFrom.theta = vehState.yaw;
    // Final pose
    poseTo.x = nodeTo->pose.pos.x;
    poseTo.y = nodeTo->pose.pos.y;
    quat_to_rpy(nodeTo->pose.rot, &roll, &pitch, &yaw);
    poseTo.theta = yaw;             

    // Vehicle properties
    vp = maneuver_create_vehicle(kin.wheelBase, kin.maxSteer);

    // Create maneuver object
    mp = maneuver_pose2pose(vp, &poseFrom, &poseTo);

    // Some maneuvers may not be possible, so trap this.
    if (!mp) {
      maneuver_free(mp);
      free(vp);
      continue; 
    }

    // Create an arc from the previous node to the new node
    arc = gUturn->createArc(nodeFrom->index, nodeTo->index);
    if (!arc) {
      ERROR("unable to create arc; try increasing maxArcs");
      return ENOMEM;
    }
    arc->type = GRAPH_ARC_NODE;
  } 
  maneuver_free(mp);
  free(vp);

  if(deb) MSG( "created %d nodes %d arcs",
               gUturn->getNodeCount(),gUturn->getArcCount() );

  /* From our current position (nodeVeh) to 
   * a. SECTOR2 (we prefer to go left first if possible)
   * b. SECTOR3
   */
  // Doing genVehicleSubGraph manually for now
  // TODO: genUTurnSubGraph(?)
  // genVehicleSubGraph(gUturn, &vehState, &actState);
  
  // Checking for reverse can be done like this
  /* if (cons.enableReverse) {
    // Generate the maneuver from B to A (for driving in reverse)
    err = genManeuver( gUturn,GRAPH_NODE_VOLATILE,nodeB,nodeA,kin.maxSteer );
    if (err == ENOMEM) // Need to know where this is defined, memset(?)
      break; 
    if (err == 0)
      numFeasible += 1;
    else
      numUnfeasible += 1;
  } */
  if(deb) MSG( "created %d nodes %d arcs",
               gUturn->getNodeCount(),gUturn->getArcCount() );

  return true;
}












