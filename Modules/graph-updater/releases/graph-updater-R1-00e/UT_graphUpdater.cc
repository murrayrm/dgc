 /*
 **
 **    Author: Noel du Toit
 **    Created: Mon Jul 16 16:20
 **
 **
 **********************************************************
 **
 **  Unit test for the planner stack
 **
 **********************************************************/

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <planner/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>
#include "../path-planner/PathPlanner.hh"

#include "GraphUpdater.hh"


using namespace std;

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  //map->prior.delta = point2(3778410.5, 403942.3);

  // INITIALIZE THE GRAPH
  Graph_t* graph;
  //  GraphNode* node;
    
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey( argc,args );

  GraphUpdater::init( &graph, map );

  // PLAN PATH
  VehicleState vehState;
  memset( &vehState,0,sizeof( vehState ) );
  ActuatorState actState;
  memset( &actState,0,sizeof( actState ) );
  pose3_t finPose;
  double roll, pitch, yaw;

  // set the vehicle state to the first waypoint
  GraphNode* initNode = graph->getNodeFromRndfId( 1,1,1 );
  vehState.localX = initNode->pose.pos.x;
  vehState.localY = initNode->pose.pos.y;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy( initNode->pose.rot, &roll, &pitch, &yaw );
  vehState.localYaw = yaw;
  
  // set final pose
  GraphNode* finalNode = graph->getNodeFromRndfId( 1,1,2 );
  finPose = finalNode->pose;
  GraphUpdater::genVehicleSubGraph( graph,vehState,actState );

  // DISPLAY INFORMATION IN MAPVIEWER
  if (1) {
    // DISPLAY GRAPH
    GraphUpdater::display( 10,graph,vehState );
  }
}
