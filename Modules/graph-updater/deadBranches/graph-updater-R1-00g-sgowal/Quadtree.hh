#ifndef QUADTREE_HH_
#define QUADTREE_HH_

#include <frames/point2.hh>
#include <map/MapElement.hh>
#include <temp-planner-interfaces/PlannerInterfaces.h>

#define MAX_NODES_IN_QUAD 100
#define HALF_Y ((this->y_max+this->y_min)/2.0)
#define HALF_X ((this->x_max+this->x_min)/2.0)
#define HALF_QUAD_Y(q) ((q->y_max+q->y_min)/2.0)
#define HALF_QUAD_X(q) ((q->x_max+q->x_min)/2.0)

class Quadtree {

public:
  Quadtree();
  Quadtree(double x_min, double x_max, double y_min, double y_max);
  ~Quadtree();

  void create_tree(Graph_t *graph);
  void add_node(GraphNode *node);
  void add_node(GraphNode *node, double x, double y);
  void remove_node(GraphNode *node);
  void move_node(GraphNode *node, double x, double y);
  bool is_inside(double x_min, double x_max, double y_min, double y_max);
  Quadtree * find_leaf_quad(double x, double y);
  unsigned int find_quad_child(double x, double y);
  void get_all_nodes(vector<GraphNode *> &nodes, double x_min, double x_max, double y_min, double y_max);
  void print();

  double x_min;
  double x_max;
  double y_min;
  double y_max;

  /* +---+---+
   * | 1 | 2 |
   * +---+---+
   * | 3 | 4 |
   * +---+---+
   */
  Quadtree *childs[4];
  bool has_child;
  /* The nodes contained in that quad, if
   * the quad contains childs then this vector is empty
   */
  vector<GraphNode *> nodes;
private :

};

#endif /*QUADTREE_HH_*/




