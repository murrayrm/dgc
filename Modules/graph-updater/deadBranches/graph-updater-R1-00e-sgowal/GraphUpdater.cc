#include "GraphUpdater.hh"
#include <temp-planner-interfaces/Log.hh>
#include <alice/AliceConstants.h>
#include <math.h>
#include <assert.h>

#define MAXNODENUM 50000
#define MAXARCSNUM 50000
#define MAXSTEER 45
#define MAXTURN 90

Graph* GraphUpdater::underlyingGraph;
CMapElementTalker GraphUpdater::meTalker;
Quadtree GraphUpdater::quadtree;

int GraphUpdater::init(Graph_t** graph, Map* map)
{
  //Initialize underlying graph
  // Now using Graph as Graph_t -- there are no reasons to do something else.
  *graph = new Graph(MAXNODENUM, MAXARCSNUM);

  // Save the underlying graph for vel-planner -- should not be used in the
  // future
  underlyingGraph = *graph;

  // Load up the RNDF and initialize the graph
  if (GraphUtils::loadRndf(*graph,CmdArgs::RNDF_file.c_str()) != 0)
    return -1;
    
  if ((*graph)->getNodeCount() == 0)
    return -1;
  
  // Shift graph to local coordinates
  GraphNode* node;
  (*graph)->gridReady = false;
  for (int i=0; i<( *graph)->getNodeCount(); i++ ) {
    node = (*graph)->getNode(i);
    node->pose.pos.x -= map->prior.delta.x;
    node->pose.pos.y -= map->prior.delta.y;
    pose3_to_mat44d(node->pose, node->transGN);
    mat44d_inv(node->transNG, node->transGN);
    (*graph)->setNodePose(node, node->pose);
  }

  // Initialize Quadtree
  quadtree.create_tree(*graph);
  // quadtree.print();

  // Initialize map element talker
  meTalker.initSendMapElement( CmdArgs::sn_key );
  return 0;
}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete graph;
  return;
}


Graph* GraphUpdater::getGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph( Graph_t *graph, VehicleState &vehState, ActuatorState &actState )
{
  // Force graph planner to use local coordinates
  vehState.utmNorthing = vehState.localX;
  vehState.utmEasting = vehState.localY;
  vehState.utmAltitude = vehState.localZ;
  vehState.utmYaw = vehState.localYaw;

  // Generate subgraph from our current pose to nearby nodes
  GraphUtils::genVehicleSubGraph( graph,&vehState, &actState );
  return GU_OK;
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 *
 * Temporary:  For each node in the graph if it collides with an obstacle,
 * mark it as being occupied by an obstacle
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  GraphNode *node;
  MapElement *mapEl;
  point2 point, obs_pt, vect;
  point2arr new_geom;
  double x_min, x_max, y_min, y_max;
  bool collide;
  vector<GraphNode *> nodes;

#if 0
  /* Grow along lane stuff */
  bool is_obs_in_lane;
  LaneLabel obs_lane;
  double obs_lane_heading, obs_pt_heading;
  point2arr obs_center_line, outer_left_line, outer_right_line;
  point2 outer_point_left, outer_point_right, tmp_pt, near_pt, far_pt;
  double max_dist_left, max_dist_right, dist, near_dist, far_dist;
  vector<point2arr> union_set;
  
  /* debugging stuff */
  int obs_id = 10005;
#endif

  // Reset the collision flags for all nodes
  for (int i = 0; i < graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }

  // Check each node against each object in the map for a possible
  // collision.
  for (int j = 0; j < (int)map->data.size(); j++) {
    new_geom.clear();
    x_min = y_min = INFINITY;
    x_max = y_max = -INFINITY;

    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_OBSTACLE && mapEl->type != ELEMENT_VEHICLE)
      continue;

#if 0
    /* Growing along lane stuff */
    is_obs_in_lane = (map->getLane(obs_lane, mapEl->center) != -1);
    if (is_obs_in_lane) {
      map->getHeading(obs_lane_heading, tmp_pt, obs_lane, mapEl->center);
      map->getLaneCenterLine(obs_center_line, obs_lane);
      /* translate center line to center of the obstacle */
      tmp_pt = obs_center_line.project(mapEl->center);
      tmp_pt = mapEl->center - tmp_pt;
      for (unsigned int i = 0; i<obs_center_line.size(); i++) {
        obs_center_line[i].set(obs_center_line[i] + tmp_pt);
      }
      /* Reset variable */
      max_dist_left = -1.0;
      max_dist_right = -1.0;
      near_dist = INFINITY;
      far_dist = -1.0;
    }
#endif

    // Grow obstacle
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);

#if 0
      /* Grow the obstacle in the lane direction */
      if (is_obs_in_lane) {
        // Get exterm points of the obstacle
        dist = obs_center_line.get_min_dist_line(obs_pt);
        tmp_pt = obs_center_line.project(obs_pt);
        tmp_pt = obs_pt - tmp_pt;
        obs_pt_heading = atan2(tmp_pt.y, tmp_pt.x);
        obs_pt_heading -= obs_lane_heading;
        while (obs_pt_heading >= M_PI) obs_pt_heading -= 2*M_PI;
        while (obs_pt_heading < -M_PI) obs_pt_heading += 2*M_PI;
        /* Is the point of left */
        if (obs_pt_heading > 0) {
          if (max_dist_left < dist) {
            max_dist_left = dist;
            outer_point_left = tmp_pt;
          }
        } else {
          if (max_dist_right < dist) {
            max_dist_right = dist;
            outer_point_right = tmp_pt;
          }
        }
        dist = obs_center_line.project_along(obs_pt);
        if (near_dist > dist) {
          near_dist = dist;
          near_pt.set(obs_pt);
        }
        if (far_dist < dist) {
          far_dist = dist;
          far_pt.set(obs_pt);
        }
      }
#endif

      vect = obs_pt-mapEl->center; vect = vect / vect.norm();
      obs_pt.set(mapEl->geometry[k] + vect*(1+VEHICLE_WIDTH/2));
      new_geom.push_back(obs_pt);

#if 1
      if (obs_pt.x < x_min) x_min = obs_pt.x;
      if (obs_pt.y < y_min) y_min = obs_pt.y;
      if (obs_pt.x > x_max) x_max = obs_pt.x;
      if (obs_pt.y > y_max) y_max = obs_pt.y;
#endif
    }

#if 0
    /* Grow obstacle stuff */
    if (is_obs_in_lane) {
      for (unsigned int i = 0; i<obs_center_line.size(); i++) {
        outer_left_line.push_back(obs_center_line[i] + outer_point_left);
        outer_right_line.push_back(obs_center_line[i] + outer_point_right);
      }
      outer_left_line.cut_back(near_pt, VEHICLE_LENGTH);
      outer_right_line.cut_back(near_pt, VEHICLE_LENGTH);
      outer_left_line.cut_front(far_pt, VEHICLE_LENGTH);
      outer_right_line.cut_front(far_pt, VEHICLE_LENGTH);

      // union grown obstacle and separation distance
      outer_right_line.reverse();
      outer_left_line.connect(outer_right_line);
      outer_left_line.get_poly_union(new_geom, union_set);
      new_geom = union_set[0];
    }

    // Create bounding box
    for (unsigned int k = 0; k<new_geom.size(); k++) {
      obs_pt.set(new_geom[k]);
      if (obs_pt.x < x_min) x_min = obs_pt.x;
      if (obs_pt.y < y_min) y_min = obs_pt.y;
      if (obs_pt.x > x_max) x_max = obs_pt.x;
      if (obs_pt.y > y_max) y_max = obs_pt.y;
    }

    /* Send new geometry to mapviewer */ {
      MapId id = obs_id++;
      MapElement me;
      me.setId(id);
      me.setTypePoly();
      me.setColor(MAP_COLOR_ORANGE,100);
      me.setGeometry(new_geom);
      meTalker.sendMapElement(&me,-2);
    }
#endif

    nodes.clear();
    /* Get all possible nodes that might collide from the quadtree */
    quadtree.get_all_nodes(nodes, x_min, x_max, y_min, y_max);
    /* Explicitely add the volatile nodes (that are not in the quadtree) */
    for (int i=graph->getStaticNodeCount(); i < graph->getNodeCount(); i++) {
      nodes.push_back(graph->getNode(i));
    }

    // Check each node.  If a node is aleady in collision, dont waste
    // time checking it again.
    // for (int i = 0; i < graph->getNodeCount(); i++) {
    for (unsigned int i = 0; i < nodes.size(); i++) {
      collide = false;
      // node = graph->getNode(i);
      node = nodes[i];
      if (node->collideObs || node->collideCar)
        continue;

      point.set(node->pose.pos.x, node->pose.pos.y);
      if (point.x > x_min && point.x < x_max && point.y > y_min && point.y < y_max)
        collide = new_geom.is_inside_poly(point);

      if (mapEl->type == ELEMENT_OBSTACLE)
        node->collideObs = collide;
      else if (mapEl->type == ELEMENT_VEHICLE)
        node->collideCar = collide;
    }
  }

  return GU_OK;
}

void GraphUpdater::display(int sendSubgroup, Graph_t* graph, VehicleState vehState)
{
    MapElement me;
    
    // DISPLAY THE GRAPH
    point2 point, vehPos;
    vector<point2> free_points;
    vector<point2> obs_points;
    vector<point2> car_points;
    MapId free_mapId, obs_mapId, car_mapId;
    GraphNode* node;

    double dotProd, dist;
    double heading = vehState.localYaw;
    vehPos.set(vehState.localX, vehState.localY);
    free_mapId = 10000;
    obs_mapId = 10001;
    car_mapId = 10002;
    for (int i=0; i<graph->getStaticNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->type != GRAPH_NODE_LANE && node->type != GRAPH_NODE_TURN && node->type != GRAPH_NODE_VOLATILE) continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
      dist = point.dist(vehPos);
      if (dotProd>0 && dist<25) {
        if (node->collideObs)
          obs_points.push_back(point);
        else if (node->collideCar)
          car_points.push_back(point);
        else
          free_points.push_back(point);
      }
    }

    // Send free nodes
    me.setId(free_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(free_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by an obstacle
    me.setId(obs_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_YELLOW,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by a car
    me.setId(car_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_ORANGE,100);
    me.setGeometry(car_points);
    meTalker.sendMapElement(&me,sendSubgroup);
}
