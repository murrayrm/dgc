#include "GraphUpdater.hh"
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include <alice/AliceConstants.h>
#include <math.h>
#include <assert.h>

#define MAXNODENUM 50000
#define MAXARCSNUM 50000
#define MAXSTEER 45
#define MAXTURN 90
#define ALICE_BOX_FRONT VEHICLE_LENGTH
#define ALICE_BOX_REAR  VEHICLE_LENGTH
#define ALICE_BOX_SIDE  1.0668 // VEHICLE_WIDTH/2
#define VEHICLE_RADIUS  5.6225 // sqrt(VEHICLE_LENGTH^2+(1+VEHICLE_WIDTH/2)^2)

Graph* GraphUpdater::underlyingGraph;
CMapElementTalker GraphUpdater::meTalker;
Quadtree GraphUpdater::quadtree;
point2arr GraphUpdater::alice_box;

int GraphUpdater::init(Graph_t** graph, Map* map)
{
  //Initialize underlying graph
  // Now using Graph as Graph_t -- there are no reasons to do something else.
  *graph = new Graph(MAXNODENUM, MAXARCSNUM);

  // Save the underlying graph for vel-planner -- should not be used in the
  // future
  underlyingGraph = *graph;

  // Load up the RNDF and initialize the graph
  if (GraphUtils::loadRndf(*graph,CmdArgs::RNDF_file.c_str()) != 0)
    return -1;
    
  if ((*graph)->getNodeCount() == 0)
    return -1;
  
  // Shift graph to local coordinates
  GraphNode* node;
  (*graph)->gridReady = false;
  for (int i=0; i<( *graph)->getNodeCount(); i++ ) {
    node = (*graph)->getNode(i);
    node->pose.pos.x -= map->prior.delta.x;
    node->pose.pos.y -= map->prior.delta.y;
    pose3_to_mat44d(node->pose, node->transGN);
    mat44d_inv(node->transNG, node->transGN);
    (*graph)->setNodePose(node, node->pose);
  }

  // Initialize Quadtree
  quadtree.create_tree(*graph);
  // quadtree.print();

  // populate alice box
  alice_box.push_back(point2(ALICE_BOX_FRONT, ALICE_BOX_SIDE));
  alice_box.push_back(point2(ALICE_BOX_FRONT, -ALICE_BOX_SIDE));
  alice_box.push_back(point2(-ALICE_BOX_REAR, -ALICE_BOX_SIDE));
  alice_box.push_back(point2(-ALICE_BOX_REAR, ALICE_BOX_SIDE));
  alice_box.push_back(point2(ALICE_BOX_FRONT, ALICE_BOX_SIDE));

  // Initialize map element talker
  meTalker.initSendMapElement( CmdArgs::sn_key );
  return 0;
}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete graph;
  return;
}


Graph* GraphUpdater::getGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState)
{
  // Generate subgraph from our current pose to nearby nodes
  GraphUtils::genVehicleSubGraph( graph,&vehState, &actState );
  return GU_OK;
}

/**
 * @brief Clear the volatile nodes in the graph
 */
void GraphUpdater::clearVehicleSubGraph(Graph_t *graph)
{
  graph->clearVolatile();
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 *
 * Temporary:  For each node in the graph if it collides with an obstacle,
 * mark it as being occupied by an obstacle
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  GraphNode *node;
  MapElement *mapEl;
  point2 obs_pt, point;
  point2arr new_geom;
  double x_min, x_max, y_min, y_max, x, y, bx_min, bx_max, by_min, by_max;
  double bb_x, bb_y, bb_radius, tmp;
  float m[4][4];
  bool collide;
  vector<GraphNode *> nodes;

  // Reset the collision flags for all nodes
  for (int i = 0; i < graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }

  // Check each node against each object in the map for a possible
  // collision.
  for (int j = 0; j < (int)map->data.size(); j++) {
    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_OBSTACLE && mapEl->type != ELEMENT_VEHICLE)
      continue;

    // Build bounding boxes
    x_min = y_min = INFINITY;
    x_max = y_max = -INFINITY;
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);
      if (obs_pt.x < x_min) x_min = obs_pt.x;
      if (obs_pt.y < y_min) y_min = obs_pt.y;
      if (obs_pt.x > x_max) x_max = obs_pt.x;
      if (obs_pt.y > y_max) y_max = obs_pt.y;
    }
    // Create the big bounding box
    bx_min = x_min - VEHICLE_RADIUS;
    bx_max = x_max + VEHICLE_RADIUS;
    by_min = y_min - VEHICLE_RADIUS;
    by_max = y_max + VEHICLE_RADIUS;

    // Get the nodes that might collide with the obstacle
    nodes.clear();
    // Get all possible nodes that might collide from the quadtree
    quadtree.get_all_nodes(nodes, bx_min, bx_max, by_min, by_max);
    // Add the volatile node only if they can collide with the obstacle
    tmp = sqrt(pow(graph->vehicleNode->pose.pos.x - mapEl->center.x,2) +
               pow(graph->vehicleNode->pose.pos.y - mapEl->center.y,2));
    if (tmp < 20) {
      // Explicitely add the volatile nodes (that are not in the quadtree)
      for (int i=graph->getStaticNodeCount(); i < graph->getNodeCount(); i++) {
        nodes.push_back(graph->getNode(i));
      }
    }

    // Check each node
    for (unsigned int i = 0; i < nodes.size(); i++) {
      collide = false;
      node = nodes[i];
      // Ignore node that already collided (give priority to vehicle collision)
      if (node->collideCar) continue;
      if (mapEl->type == ELEMENT_OBSTACLE && node->collideObs) continue;

      // Check against big bounding box
      x = node->pose.pos.x;
      y = node->pose.pos.y;
      if (x < bx_min || x > bx_max || y < by_min || y > by_max) continue;

      // Transform small bounding box circle into node coordinates
      pose3_to_mat44f(pose3_inv(node->pose), m);
      bb_radius = sqrt(pow((x_max-x_min)/2,2)+pow((y_max-y_min)/2,2));
      bb_x = (x_min+x_max)/2;
      bb_y = (y_min+y_max)/2;
      tmp  = m[0][0] * bb_x + m[0][1] * bb_y + m[0][3];
      bb_y = m[1][0] * bb_x + m[1][1] * bb_y + m[1][3];
      bb_x = tmp;

      // check against bounding box around bounding circle
      if (ALICE_BOX_FRONT < bb_x - bb_radius || -ALICE_BOX_REAR > bb_x + bb_radius ||
          ALICE_BOX_SIDE < bb_y - bb_radius || -ALICE_BOX_SIDE > bb_y + bb_radius) continue;

      // Transform obstacle into node coordinates
      new_geom.clear();
      for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
        obs_pt.set(mapEl->geometry[k]);
        point.x = m[0][0] * obs_pt.x + m[0][1] * obs_pt.y + m[0][3];
        point.y = m[1][0] * obs_pt.x + m[1][1] * obs_pt.y + m[1][3];
        new_geom.push_back(point);
        // Check point with alice bounding box
        if (point.x > -ALICE_BOX_REAR && point.x < ALICE_BOX_FRONT &&
            point.y > -ALICE_BOX_SIDE && point.y < ALICE_BOX_SIDE) {
          collide = true;
          break;
        }
      }
      // Check for intersection between bounding box and obstacle (worst case scenario)
      if (!collide) {
        new_geom.push_back(new_geom[0]);
        collide = new_geom.is_intersect(alice_box);
      }

      if (mapEl->type == ELEMENT_OBSTACLE)
        node->collideObs = collide;
      else if (mapEl->type == ELEMENT_VEHICLE)
        node->collideCar = collide;
    }
  }

  return GU_OK;
}

void GraphUpdater::display(int sendSubgroup, Graph_t* graph, VehicleState vehState)
{
    MapElement me;
    
    // DISPLAY THE GRAPH
    point2 point, vehPos;
    vector<point2> free_points;
    vector<point2> obs_points;
    vector<point2> car_points;
    MapId free_mapId, obs_mapId, car_mapId;
    GraphNode* node;

    double dotProd, dist;
    double heading = vehState.localYaw;
    vehPos.set(vehState.localX, vehState.localY);
    free_mapId = 10000;
    obs_mapId = 10001;
    car_mapId = 10002;
    for (int i=0; i<graph->getStaticNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->type != GRAPH_NODE_LANE && node->type != GRAPH_NODE_TURN) continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
      dist = point.dist(vehPos);
      if (dotProd>0 && dist<25) {
        if (node->collideObs)
          obs_points.push_back(point);
        else if (node->collideCar)
          car_points.push_back(point);
        else
          free_points.push_back(point);
      }
    }

    // Send free nodes
    me.setId(free_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(free_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by an obstacle
    me.setId(obs_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_YELLOW,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by a car
    me.setId(car_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_ORANGE,100);
    me.setGeometry(car_points);
    meTalker.sendMapElement(&me,sendSubgroup);
}
