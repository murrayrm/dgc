#ifndef GRAPHUPDATER_HH_
#define GRAPHUPDATER_HH_

#include <frames/pose3.h>
#include <map/Map.hh>
#include <frames/point2.hh>
#include <frames/mat44.h>
#include <interfaces/VehicleState.h>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <temp-planner-interfaces/GraphPlanner.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Graph.hh>

#include "GraphUtils.hh"

class GraphUpdater {

public: 

  static int init(Graph_t** graph, vec3_t siteOffset, Map* map);
  static int load(Graph_t* graph, Map* map, vector<int> segments);
  static int save(Graph_t *graph);
  static int save(Graph_t *graph, vector<int> segments);
  static void destroy(Graph_t* graph);
  static GraphPlanner* getGraphPlanner();
  static Graph_t* getGraph();
  static Err_t updateGraphMap(Graph_t *graph, VehicleState &vehState, Map *map);
  static Err_t genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState);
  static void clearVehicleSubGraph(Graph_t *graph);
  static Err_t updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map);
  static void display(int sendSubgroup, Graph_t* graph, VehicleState vehState);
  static bool hasNewSegments(vector<int> segments);
  static int genUturnGraph(Graph_t **gUturn, Map *map, pose3_t &finalPose, point2 ref_pt,
                           GraphNode **nodeVeh, VehicleState &vehState, ActuatorState &actState,
                           const Uturn_t uTurn);

private :
  static Graph_t *underlyingGraph;
  static GraphPlanner *graphPlanner;
  static CMapElementTalker meTalker;
  static vector<int> current_segments_loaded;
  static void updateOpposite(Graph_t *graph, GraphNode *node, double x, double y);
  static void updateRails(Graph_t *graph, GraphNode *node);
  static void updateChanges(Graph_t *graph, GraphNode *node);

  static int reverseProjection(point2 &pt, point2 pt, double yaw, point2arr on, double max_dist);
};

#endif /*GRAPHUPDATER_HH_*/




