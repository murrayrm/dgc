#include "GraphUpdater.hh"
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Quadtree.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <temp-planner-interfaces/Utils.hh>
#include <alice/AliceConstants.h>
#include <dgcutils/DGCutils.hh>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <sys/stat.h>

#if 0
  #define MAXNODENUM 500000
  #define MAXARCSNUM 500000
#else
  #define MAXNODENUM 1200000
  #define MAXARCSNUM 1500000
#endif
#define MAXSTEER 45
#define MAXTURN 90
#if 1
  #define ALICE_BOX_FRONT 9.7
  #define ALICE_BOX_REAR  6.5 
  #define ALICE_BOX_SIDE  1.5
  #define VEHICLE_RADIUS  9.8152
#else
  #define ALICE_BOX_FRONT 9.7112 // VEHICLE_LENGTH + DIST_REAR_AXLE_TO_FRONT
  #define ALICE_BOX_REAR  6.5956 // VEHICLE_LENGTH + DIST_REAR_TO_REAR_AXLE 
  #define ALICE_BOX_SIDE  2.0668 // VEHICLE_WIDTH/2 + 1
  #define VEHICLE_RADIUS  9.9287 // sqrt(VEHICLE_BOX_FRONT^2+ALICE_BOX_SIDE^2)
#endif
#define UPDATE_CHANGE_MANEUVERS 1

#define MIN(x,y) (((x) < (y))?(x):(y))

Graph_t* GraphUpdater::underlyingGraph;
vector<int> GraphUpdater::current_segments_loaded;

int GraphUpdater::init(Graph_t** graph, vec3_t siteOffset, Map* map)
{
  // Initialize underlying graph
  // Now using Graph as Graph_t -- there are no reasons to do something else.
  *graph = new Graph_t(MAXNODENUM, MAXARCSNUM);

  // Set translation from site frame to UTM frame
  (*graph)->siteOffset = siteOffset;

  // Save the underlying graph for vel-planner -- should not be used in the
  // future
  underlyingGraph = *graph;
  
  // Initialize segments currently loaded
  current_segments_loaded.clear();

  // Load the complete RNDF only if step-by-step is not enabled
  if (!CmdArgs::stepbystep_load) {
    // Load up the RNDF and initialize the graph
    if (GraphUtils::loadRndf(*graph,CmdArgs::RNDF_file.c_str()) != 0)
      return -1;
    
    if ((*graph)->getNodeCount() == 0)
      return -1;
  }

  // initialize the vehicle node to something else than random
  (*graph)->vehicleNode = NULL;

  return 0;
}

bool GraphUpdater::hasNewSegments(vector<int> segments)
{
  /* Check if we need to reload the RNDF */
  int new_segment;
  for (unsigned int i=0; i<segments.size(); i++) {
    new_segment = segments[i];
    if (!GraphUtils::contains(current_segments_loaded, new_segment)) {
      return true;
    }
  }
  return false;
}

int GraphUpdater::load(Graph_t *graph, Map *map, vector<int> segments)
{
  vector<int> new_segments;
  vector<int> rem_segments;

  /* Check if we need to reload the RNDF */
  bool found, reload = false;
  int new_segment, current_segment;
  for (unsigned int i=0; i<segments.size(); i++) {
    found = false;
    new_segment = segments[i];
    for (unsigned int j=0; j<current_segments_loaded.size(); j++) {
      current_segment = current_segments_loaded[j];
      if (current_segment == new_segment) {
        found = true;
        break;
      }
    }
    if (!found) {
      current_segments_loaded.push_back(new_segment);
      new_segments.push_back(new_segment);
      reload = true;
    }
  }
  if (!reload) return 0;

  /* Check if we can remove segments */
  bool remove = false;
  for (unsigned int i=0; i<current_segments_loaded.size(); i++) {
    found = false;
    current_segment = current_segments_loaded[i];
    for (unsigned int j=0; j<segments.size(); j++) {
      new_segment = segments[j];
      if (new_segment == current_segment) {
        found = true;
        break;
      }
    }
    if (!found) {
      current_segments_loaded.erase(current_segments_loaded.begin()+i);
      rem_segments.push_back(current_segment);
      remove = true;
    }
  }
#if 0
  if (remove) {
    for (unsigned int i=0; i<rem_segments.size(); i++) {
      current_segment = rem_segments[i];
      Console::addMessage("Unloading segment %d from the graph", current_segment);
      GraphUtils::unload_segment(graph, current_segment);
    }
  }
#else
  graph->numNodes = 0;
  graph->numArcs = 0;
  graph->freezeStatic();
  new_segments = current_segments_loaded;

  stringstream ss;
  ss << "Loading " << new_segments.size() << " segments from the RNDF : ";
  copy(new_segments.begin(), new_segments.end(), ostream_iterator<int>(ss, ", "));
  Console::addMessage("%s", ss.str().c_str());

#endif

  // Console::addMessage("Loading %d segments from the RNDF", new_segments.size());

  /* Load up the RNDF and initialize the graph */
  if (GraphUtils::loadRndf(graph,CmdArgs::RNDF_file.c_str(), current_segments_loaded, new_segments) != 0)
    return -1;
 
  return 0;
}

int GraphUpdater::save(Graph_t * graph)
{
  vector<int> segments;

  /* Open the rndf */
  std::RNDF *rndf = new std::RNDF();
  if (!rndf->loadFile(CmdArgs::RNDF_file.c_str())) return -1;
  if (rndf->getNumOfSegments() <= 0) return -1;

  /* Loop through all segment and save them */
  for (unsigned int sn = 0; sn < rndf->getAllSegments().size(); sn++) {
    int segment = rndf->getAllSegments()[sn]->getSegmentID();
    segments.push_back(segment);
  }

  return save(graph, segments);
}

int GraphUpdater::save(Graph_t *graph, vector<int> segments)
{
  /* Generate folder name from RNDF path */
  ostringstream oss;
  string folder;
  folder.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
  oss << folder << ".d";
  folder = oss.str();

  /* create folder if necessary */
  struct stat st;
  stat(folder.c_str(), &st);
  if (errno == ENOENT) {
    if (mkdir(folder.c_str(), 0755) != 0) return -1;
  }

  /* Loop through all segment and save them */
  for (unsigned int sn = 0; sn < segments.size(); sn++) {
    int segment = segments[sn];
    GraphUtils::save_segment(graph, folder, segment);
  }

  return 0;
}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete graph;
  return;
}


Graph_t* GraphUpdater::getGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState)
{
  // Generate subgraph from our current pose to nearby nodes
  GraphUtils::genVehicleSubGraph( graph,&vehState, &actState );
  return GU_OK;
}

/**
 * @brief Clear the volatile nodes in the graph
 */
void GraphUpdater::clearVehicleSubGraph(Graph_t *graph)
{
  graph->clearVolatile();
}

/**
 * @brief Update the graph according to the sensed lane information
 *  - get the current lane we are in (as well as the neighboring lanes)
 *  - for each node on those lane set the distance to the center line
 */
Err_t GraphUpdater::updateGraphMap(Graph_t *graph, VehicleState &vehState, Map *map)
{
  if ( !(CmdArgs::update_lanes) && !(CmdArgs::update_stoplines) )
    return GU_OK;
  Log::getStream(4) << "Updating graph information from map: lanes = " << CmdArgs::update_lanes << " and stoplines = " << CmdArgs::update_stoplines << endl;


  static GraphNode *prevAliceNode = NULL;
  point2 alicePoint = AliceStateHelper::getPositionRearAxle(vehState);
  /* Get alice position on the graph */
  GraphNode* aliceNode = graph->vehicleNode;

  if (!aliceNode || (aliceNode->type & GRAPH_NODE_VOLATILE)) {
    vec3_t alicePos = {alicePoint.x, alicePoint.y, 0};
    aliceNode = graph->getNearestNode(alicePos, GRAPH_NODE_LANE | GRAPH_NODE_TURN, 10);
    if (!aliceNode) return GU_UPDATE_FROM_MAP_ERROR;
  }
  if (aliceNode->type & GRAPH_NODE_TURN) {
    aliceNode = aliceNode->endNode;
    if (!aliceNode) return GU_UPDATE_FROM_MAP_ERROR;
  }
  /* We already updated, return */
  //  if (prevAliceNode == aliceNode)
  //    return 0;
  prevAliceNode = aliceNode;

  /* Get the current lane */
  LaneLabel lane = LaneLabel(aliceNode->segmentId, aliceNode->laneId);
  Log::getStream(1) << "Lane label = " << lane << endl;

  if ( (lane.segment == 0) || (lane.lane == 0) ) // we are at startup or in a zone
    return GU_UPDATE_FROM_MAP_ERROR;

  /* For all the lanes in the current segment update the center dist */
  point2arr centerLine;
  GraphNode *leftNode, *rightNode, *currentNode;
  point2 projectedAlicePoint, nodePoint, prevNodePoint, projectedPoint;
  double distAlongRail, tmpDist;
  double roll, pitch, yaw;
  PointLabel stopline;
  point2_uncertain stopPt;
  int counter = 0;

  double max_dist = 10; // max distance that can be projected before error
  double extension = 20; // how much lanes can be extended during projection

  bool wrongWay = GraphUtils::isReverse(vehState, map, lane);
  int direction = wrongWay?-1:1;

  /* Get the center line of the lane and projection of alice */
  map->getLaneCenterLine(centerLine, lane);
  yaw = AliceStateHelper::getHeading(vehState);
  if (Utils::reverseProjection(projectedAlicePoint, alicePoint, yaw, centerLine, max_dist, extension) != 0)
    Log::getStream(1) << "reverseProjection error" << endl;

  /* Get currentNode */
  vec3_t alicePos = {projectedAlicePoint.x, projectedAlicePoint.y, 0};
  currentNode = graph->getNearestNode(alicePos, GRAPH_NODE_LANE, 2, direction);
  if (!currentNode) return GU_UPDATE_FROM_MAP_ERROR;

  /* Get node on the center rail */
  if (currentNode->railId == 1) currentNode = currentNode->leftNode;
  else if (currentNode->railId == -1) currentNode = currentNode->rightNode;

  /* For some distance in front of Alice update the dist from centerline */
  distAlongRail = 0.0;
  point2 tmpPt;
  prevNodePoint.set(currentNode->pose.pos.x, currentNode->pose.pos.y);

#define DIST_TO_UPDATE_LANE 30
#define MAX_COUNTS 100

  distAlongRail = 0.0;

  while ((distAlongRail < DIST_TO_UPDATE_LANE) && (currentNode != NULL) && (counter < MAX_COUNTS)) {
    // set the centerDist for the current node (on the center rail)
    nodePoint.set(currentNode->pose.pos.x, currentNode->pose.pos.y);
    quat_to_rpy(currentNode->pose.rot, &roll, &pitch, &yaw);

    if (CmdArgs::update_lanes) {
      if (Utils::reverseProjection(projectedPoint, nodePoint, yaw, centerLine, max_dist, extension) != 0)
        Log::getStream(1) << "reverseProjection error" << endl;
      tmpDist = projectedPoint.dist(nodePoint);
      //      Log::getStream(1) << "centerDist (center rail) = " << currentNode->centerDist << endl;
      currentNode->centerDist = (float)tmpDist;
      //      Log::getStream(1) << "centerDist (center rail) = " << currentNode->centerDist << endl;
    }

    if (CmdArgs::update_stoplines) {
      // Update first stopline in this lane
      if (currentNode->isStop) {
        stopline.segment = currentNode->segmentId;
        stopline.lane    = currentNode->laneId;
        stopline.point   = currentNode->waypointId;
        /* This should always to the case */
        if (currentNode->waypointId != 0 && map->getStopLineSensed(stopPt, stopline) >= 0) {
          //          Log::getStream(4) << "Updating stopline information:" << endl << "  Previous = " << currentNode->distToStop << endl;
          double dx, dy;
          dx = stopPt.x-currentNode->pose.pos.x;
          dy = stopPt.y-currentNode->pose.pos.y;
          double dist = cos(yaw)*dx + sin(yaw)*dy;
          currentNode->distToStop = dist;
          //          Log::getStream(4) << "  Node heading = " << yaw << " - Vector to the stopline = " << dx << "," << dy << endl;
          //          Log::getStream(4) << "  Now = " << currentNode->distToStop << endl;
        }
      }
    }

    // update the dist and prev node point
    distAlongRail += nodePoint.dist(prevNodePoint);
    prevNodePoint = nodePoint;
    
    if (CmdArgs::update_lanes) {
      // Calculate the dist to the centerline from the two associated rail nodes
      leftNode = currentNode->leftNode;
      nodePoint.set(leftNode->pose.pos.x, leftNode->pose.pos.y);
      //      quat_to_rpy(leftNode->pose.rot, &roll, &pitch, &yaw);
      //      if (Utils::reverseProjection(projectedPoint, nodePoint, yaw, centerLine, max_dist, extension) != 0)
      //        Log::getStream(1) << "reverseProjection error" << endl;

      // All the rails should have the same projected point, so there is no need to recompute
      tmpDist = projectedPoint.dist(nodePoint);
      //      Log::getStream(1) << "centerDist (left rail) = " << leftNode->centerDist << endl;
      leftNode->centerDist = (float)tmpDist;
      //      Log::getStream(1) << "centerDist (left rail) = " << leftNode->centerDist << endl;

      rightNode = currentNode->rightNode;
      nodePoint.set(rightNode->pose.pos.x, rightNode->pose.pos.y);
      //      quat_to_rpy(rightNode->pose.rot, &roll, &pitch, &yaw);
      //      if (Utils::reverseProjection(projectedPoint, nodePoint, yaw, centerLine, max_dist, extension) != 0)
      //        Log::getStream(1) << "reverseProjection error" << endl;
      tmpDist = projectedPoint.dist(nodePoint);
      //      Log::getStream(1) << "centerDist (right rail) = " << rightNode->centerDist << endl;
      rightNode->centerDist = (float)tmpDist;
      //      Log::getStream(1) << "centerDist (right rail) = " << rightNode->centerDist << endl;
    }
      
    // move on to the next node on the center rail
    currentNode = graph->getNextLaneNode(currentNode);
    // NOTE: the above function returns NULL when we have reached the end of the lane
    counter++;
  }
  return GU_OK;
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  static vector<GraphNode *> markedNodes;

  GraphNode *node;
  MapElement *mapEl;
  point2 obs_pt, point;
  point2arr new_geom, bounds;
  LaneLabel node_lane;
  double x_min, x_max, y_min, y_max, x, y, bx_min, bx_max, by_min, by_max;
  double alice_rear, alice_front, alice_side_left, alice_side_right;
  point2arr alice_box;
  double bb_x, bb_y, bb_radius, tmp;
  float m[4][4];
  bool collide, obs_in_lane;
  vector<GraphNode *> nodes;

  unsigned long long time1 = 0;
  unsigned long long time2;

  nodes.reserve(200);
  markedNodes.reserve(200);

  // Reset the collision flags for all nodes
  DGCgettime(time1);
  for (unsigned int i = 0; i < markedNodes.size(); i++) {
    node = markedNodes[i];
    node->collideObs = false;
    node->collideCar = false;
  }
  markedNodes.clear();
  DGCgettime(time2);
  Log::getStream(9) << "  Resetting nodes: " << (time2-time1)/1000.0 << " ms" << endl;

  // Check each node against each object in the map for a possible
  // collision.
  Log::getStream(1) << "  Number of obstacle in the map: " << map->usedIndices.size() << endl;
  Console::updateMapSize(map->usedIndices.size());

  if (!graph->is_quadtree_initialized) return GU_OK;

  for (int j = 0; j < (int)map->usedIndices.size(); j++) {
    // mapEl = &map->data[j];
    mapEl = &map->newData[map->usedIndices.at(j)].mergedMapElement;

    if (!mapEl->isObstacle())
      continue;

    // Build bounding boxes
    DGCgettime(time1);
    x_min = y_min = INFINITY;
    x_max = y_max = -INFINITY;
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);
      if (obs_pt.x < x_min) x_min = obs_pt.x;
      if (obs_pt.y < y_min) y_min = obs_pt.y;
      if (obs_pt.x > x_max) x_max = obs_pt.x;
      if (obs_pt.y > y_max) y_max = obs_pt.y;
    }
    // Create the big bounding box
    bx_min = x_min - VEHICLE_RADIUS;
    bx_max = x_max + VEHICLE_RADIUS;
    by_min = y_min - VEHICLE_RADIUS;
    by_max = y_max + VEHICLE_RADIUS;
    DGCgettime(time2);
    Log::getStream(9) << "  Find min, max time: " << (time2-time1)/1000.0 << " ms" << endl;

#define SEND_MAPVIEWER 0
#if SEND_MAPVIEWER
    /* DEBUG */
    point2arr obs_points;
    obs_points.push_back(point2(bx_min, by_min));
    obs_points.push_back(point2(bx_max, by_min));
    obs_points.push_back(point2(bx_max, by_max));
    obs_points.push_back(point2(bx_min, by_max));
    MapElement me;
    me.setId(50000+2*j);
    me.setTypePoly();
    me.setColor(MAP_COLOR_ORANGE,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,-2);
#endif

    // Get the nodes that might collide with the obstacle
    nodes.clear();
    // Get all possible nodes that might collide from the quadtree
    graph->quadtree->get_all_nodes(nodes, bx_min, bx_max, by_min, by_max);
    // Add the volatile node only if they can collide with the obstacle
    tmp = sqrt(pow(graph->vehicleNode->pose.pos.x - mapEl->center.x,2) +
               pow(graph->vehicleNode->pose.pos.y - mapEl->center.y,2));
    if (tmp < 20) {
      // Explicitely add the volatile nodes (that are not in the quadtree)
      for (int i=graph->getStaticNodeCount(); i < graph->getNodeCount(); i++) {
        nodes.push_back(graph->getNode(i));
      }
    }

#if SEND_MAPVIEWER
    obs_points.clear();
    for (unsigned int i=0; i<nodes.size(); i++) {
      obs_points.push_back(point2(nodes[i]->pose.pos.x, nodes[i]->pose.pos.y));
    }
    me.setId(50000+2*j+1);
    me.setTypePoints();
    me.setColor(MAP_COLOR_ORANGE,50);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,-2);
#endif

    double t1, t2, t3, t4;
    t1 = t2 = t3 = t4 = 0;
    unsigned long long a1, a2;

    // Check each node
    for (unsigned int i = 0; i < nodes.size(); i++) {
      collide = false;
      node = nodes[i];
      // Ignore node that already collided (give priority to vehicle collision)
      if (node->collideCar) continue;
      if (mapEl->isObstacle() && !mapEl->isVehicle() && node->collideObs) continue;

      // Check against big bounding box
      x = node->pose.pos.x;
      y = node->pose.pos.y;
      if (x < bx_min || x > bx_max || y < by_min || y > by_max) continue;

      // Transform small bounding box circle into node coordinates
      DGCgettime(a1);
      pose3_to_mat44f(pose3_inv(node->pose), m);
      bb_radius = sqrt(pow((x_max-x_min)/2,2)+pow((y_max-y_min)/2,2));
      bb_x = (x_min+x_max)/2;
      bb_y = (y_min+y_max)/2;
      tmp  = m[0][0] * bb_x + m[0][1] * bb_y + m[0][3];
      bb_y = m[1][0] * bb_x + m[1][1] * bb_y + m[1][3];
      bb_x = tmp;
      DGCgettime(a2);
      t1 += (a2-a1)/1000.0;

      // check against unrestricted bounding box around bounding circle
      if (ALICE_BOX_FRONT < bb_x - bb_radius || -ALICE_BOX_REAR > bb_x + bb_radius ||
          ALICE_BOX_SIDE < bb_y - bb_radius || -ALICE_BOX_SIDE > bb_y + bb_radius) continue;

      DGCgettime(a1);
#if 0
      // restrict alice box according to current situation of the node
      obs_in_lane = false;
      node_lane.segment = node->segmentId;
      node_lane.lane = node->laneId;
      // Check if obstacle in the same lane
      if (node->type & GRAPH_NODE_LANE) {
        if (map->getLaneBoundsPoly(bounds, node_lane) < 0) {
          obs_in_lane = true;
        } else {
          obs_in_lane = mapEl->isOverlap(bounds);
        }
      }
#endif
      obs_in_lane = true;
      // Be conservative only in lanes
      if (!(node->type & GRAPH_NODE_LANE) || !(node->type & GRAPH_NODE_CHANGE) || !obs_in_lane) {
        // If the node is not in a lane or the obstacle is not in the same
        // lane than the node then use the 1m separation distance
        alice_front = DIST_REAR_AXLE_TO_FRONT + 1.0;
        alice_rear = DIST_REAR_TO_REAR_AXLE + 1.0;
        alice_side_left = VEHICLE_WIDTH/2.0 + 1.0;
        alice_side_right = alice_side_left;
      } else {
        // get distance to next turn node
        double dist = 1.0 + DIST_REAR_AXLE_TO_FRONT;
        GraphNode *prev_node = node;
        do {
          GraphNode *next_node = graph->getNextLaneNode(prev_node);
          if (next_node == NULL || (next_node->type & GRAPH_NODE_TURN)) break;
          dist += sqrt(pow(prev_node->pose.pos.x - next_node->pose.pos.x,2) + pow(prev_node->pose.pos.y - next_node->pose.pos.y,2));
          prev_node = next_node;
        } while (dist < ALICE_BOX_FRONT);
        // set front
        alice_front = MIN(dist, ALICE_BOX_FRONT);
        // set rear
        alice_rear = ALICE_BOX_REAR;
        // set side
        if (node->railId == 1) {
          alice_side_right = VEHICLE_WIDTH/2.0 + 0.2;
          alice_side_left = ALICE_BOX_SIDE;
        } else if (node->railId == -1) {
          alice_side_left = VEHICLE_WIDTH/2.0 + 0.2;
          alice_side_right = ALICE_BOX_SIDE;
        } else {
          alice_side_left = alice_side_right = MIN(node->laneWidth/2.0 + 0.2, ALICE_BOX_SIDE);
        }
      }
      DGCgettime(a2);
      t2 += (a2-a1)/1000.0;

      // Transform obstacle into node coordinates
      DGCgettime(a1);
      new_geom.clear();
      for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
        obs_pt.set(mapEl->geometry[k]);
        point.x = m[0][0] * obs_pt.x + m[0][1] * obs_pt.y + m[0][3];
        point.y = m[1][0] * obs_pt.x + m[1][1] * obs_pt.y + m[1][3];
        new_geom.push_back(point);

        // Check point with alice bounding box
        if (point.x > -alice_rear && point.x < alice_front &&
            point.y > -alice_side_right && point.y < alice_side_left) {
          collide = true;
          break;
        }
      }
      DGCgettime(a2);
      t3 += (a2-a1)/1000.0;

      // Check for intersection between bounding box and obstacle (worst case scenario)
      DGCgettime(a1);
      if (!collide) {
        new_geom.push_back(new_geom[0]);

        // populate alice box
        alice_box.clear();
        alice_box.push_back(point2(alice_front, alice_side_left));
        alice_box.push_back(point2(alice_front, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, alice_side_left));
        alice_box.push_back(point2(alice_front, alice_side_left));
        

        collide = new_geom.is_intersect(alice_box);
      }
      DGCgettime(a2);
      t4 += (a2-a1)/1000.0;

      if (mapEl->isObstacle() && !mapEl->isVehicle()) {
        node->collideObs = collide;
        markedNodes.push_back(node);
      } else if (mapEl->isVehicle()) {
        node->collideCar = collide;
        markedNodes.push_back(node);
      }
    }

#if 1
    Log::getStream(9) << "  Transform bounding box execution time: " << t1/nodes.size() << " ms" << endl;
    Log::getStream(9) << "  Changing alice box execution time: " << t2/nodes.size() << " ms" << endl;
    Log::getStream(9) << "  Simple overlap execution time: " << t3/nodes.size() << " ms" << endl;
    Log::getStream(9) << "  Complex overlap execution time: " << t4/nodes.size() << " ms" << endl;
#endif
  }

  return GU_OK;
}

void GraphUpdater::updateOpposite(Graph_t *graph, GraphNode *node, double x, double y)
{
  int direction = -node->direction;
  double roll, pitch, yaw;
  vec3_t vec = { node->pose.pos.x, node->pose.pos.y, 0 };
  GraphNode *op_node;

  op_node = graph->getNearestNode(vec, GRAPH_NODE_LANE, 0.1, direction);
  if (!op_node) return;

  graph->quadtree->move_node(op_node, x, y);
  op_node->pose.pos.x = x;
  op_node->pose.pos.y = y;
  quat_to_rpy(node->pose.rot, &roll, &pitch, &yaw);
  op_node->pose.rot = quat_from_rpy(0, 0, yaw+M_PI);
  pose3_to_mat44f(op_node->pose, op_node->transGN);
  mat44f_inv(op_node->transNG, op_node->transGN);
  updateRails(graph, op_node);
}

void GraphUpdater::updateRails(Graph_t *graph, GraphNode *node)
{
  point2 point;
  double r,p,yaw;
  quat_to_rpy(node->pose.rot, &r, &p, &yaw);

  for (int rail = -1; rail < 2; rail+=2) {
    GraphNode *railNode = (rail == -1)?node->leftNode:node->rightNode;
    point.x = node->pose.pos.x + rail*sin(yaw)*((node->laneWidth-VEHICLE_WIDTH)/2.0);
    point.y = node->pose.pos.y - rail*cos(yaw)*((node->laneWidth-VEHICLE_WIDTH)/2.0);
    graph->quadtree->move_node(railNode, point.x, point.y);
    railNode->pose.pos.x = point.x;
    railNode->pose.pos.y = point.y;
    railNode->pose.rot = node->pose.rot;
    pose3_to_mat44f(railNode->pose, railNode->transGN);
    mat44f_inv(railNode->transNG, railNode->transGN);
  }
}

void GraphUpdater::updateChanges(Graph_t *graph, GraphNode *node)
{
#if UPDATE_CHANGE_MANEUVERS == 1
  vector<GraphNode *> nodes;
  VehicleConfiguration configA, config;
  double roll, pitch, yaw, s;
  Pose2D poseB;
  Vehicle *vp;
  Maneuver *mp;

  for (int rail = -1; rail < 2; rail++) {
    GraphNode *nodeA, *nodeB, *tmpNode;
    tmpNode = (rail == -1)?node->leftNode:((rail == 0)?node:node->rightNode);
    /* Check lane change maneuvers (in reverse) */
    for (int direction = -1; direction < 2; direction += 2) {
      unsigned int changes_num = GraphUtils::getChangeCount(graph, tmpNode, direction);
      if (changes_num == 0) continue;
      for (unsigned int change_index = 0; change_index < changes_num; change_index++) {
        GraphUtils::getChangeNodes(nodes, &nodeA, &nodeB, graph, tmpNode, direction, change_index);
        if (nodeA == NULL || nodeB == NULL) continue;

        /* Re-generate the maneuver from nodeA ... */
        configA.x = nodeA->pose.pos.x;
        configA.y = nodeA->pose.pos.y;
        quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
        configA.theta = yaw;
        configA.phi = nodeA->steerAngle;
        /* ... to nodeB */
        poseB.x = nodeB->pose.pos.x;
        poseB.y = nodeB->pose.pos.y;
        quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
        poseB.theta = yaw;
        vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 45*M_PI/180);
        mp = maneuver_config2pose(vp, &configA, &poseB);
        /* if impossible forget about it */
        if (!mp) {
          free(vp);
          continue;
        }
              
        for (unsigned int i = 1; i<nodes.size()-1; i++) {
          tmpNode = nodes[i];
          s = (double) i / nodes.size();
          config = maneuver_evaluate_configuration(vp, mp, s);
          graph->quadtree->move_node(tmpNode, config.x, config.y);
          tmpNode->pose.pos = vec3_set(config.x, config.y, 0);
          tmpNode->pose.rot = quat_from_rpy(0, 0, config.theta);
          tmpNode->steerAngle = config.phi;
          pose3_to_mat44f(tmpNode->pose, tmpNode->transGN);
          mat44f_inv(tmpNode->transNG, tmpNode->transGN);
        }
        maneuver_free(mp);
        free(vp);
      }
    }
  }
#endif
}

int GraphUpdater::genUturnGraph(Graph_t **gUturn, Map *map, pose3_t &finalPose, point2 ref_pt,
                                GraphNode **nodeVeh, VehicleState &vehState, ActuatorState &actState,
                                const Uturn_t uTurn) {

  GraphUtils::genUturnGraph(gUturn, map, finalPose, ref_pt, nodeVeh, vehState, actState, uTurn);

  return 1;

}

