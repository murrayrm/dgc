              Release Notes for "graph-updater" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "graph-updater" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "graph-updater" module can be found in
the ChangeLog file.

Release R1-01v-sgowal (Tue Oct  2 15:48:18 2007):
	Faster genVehicleSubgraph
	Load/Save now correctly places the nodes at the correct UTM coordinates, but planning when loading from the graph files still doesn't work

Release R1-01v (Tue Oct  2 11:57:27 2007):
	Moved display and print functions to Utils.
	Updated function calls for above change.
	Commented out the UT in the PROJ_BINS to speed up compiling. 
	To use UT, just uncomment (easier than maintaining deprecated UT's)

Release R1-01u (Tue Oct  2  0:50:36 2007):
  Switched graph generation to use the site frame.

Release R1-01t (Sun Sep 30 22:05:22 2007):
  Changed graph generation to use offset UTM coordinates (site frame).
  Tweaked the turn generation so that it works with zero rails.  

Release R1-01s (Sat Sep 29 15:26:52 2007):
	Fixed creation of passing maneuvers (passing maneuvers connecting on the wrong rails)

Release R1-01r (Sat Sep 29 12:54:22 2007):
	Fixed bug in updateGraphMap fcn: now check if the current lane is 0, which
	means that we are either starting up, or in a zone.
	Fixed linking problem in UT_gUTurn.

Release R1-01q (Sat Sep 29  9:40:18 2007):
	GraphUpdater can now created as many rails as wanted (at compile time).
	genTurns still needs to be updated accordingly.

Release R1-01p (Thu Sep 27 10:29:50 2007):
	Improved performace of display and updateGraphFromStateProblem

Release R1-01o (Wed Sep 26 20:49:05 2007):
	Implemented using sensed lane information to update the cost on the graph. This lets the sensed data guide the planner, but does not actually change the graph.

Release R1-01n (Wed Sep 26 10:49:55 2007):
	Making more fesasible turns. Propagating changes from temp-planner-interfaces.

Release R1-01m (Tue Sep 25 11:52:26 2007):
	Fixed a library linking problem for UT_gUturn.

Release R1-01l (Mon Sep 24 16:53:08 2007):
	Fixed bug in genChanges that prevented the creation of lane change maneuver between lanes going in the same direction

Release R1-01k (Fri Sep 14  6:03:59 2007):
	Uses MapElement::isObstacle() and MapElement::isVehicle() now

Release R1-01j (Thu Sep 13 17:38:51 2007):
	Outputs the number of planner's map to the Console

Release R1-01i (Wed Sep  5 11:51:45 2007):
	Improved performance of updateGraphFromStateProblem

Release R1-01h (Sun Sep  2 17:35:30 2007):
	Propagated changes in mapper/map
	Fixed genTurns to correctly generate the low resolution connections

Release R1-01g (Sat Sep  1  8:49:22 2007):
	Changing map query to match new structure 

Release R1-01f (Fri Aug 31 17:18:03 2007):
	Progated changes made on the field (fix feasibility check + fix genManeuver for volatile node)

Release R1-01e (Thu Aug 30 17:54:15 2007):
	Fixed genVehicleSubgraph to connect to zones exit waypoints as well

Release R1-01d (Thu Aug 30 14:16:08 2007):
	Updating stopline information from map

Release R1-01c (Wed Aug 29 14:14:19 2007):
	Fixed minor bugs:
	  - Fixed how the turns are created (waypoint information in the nodes could be overwritten)
	  - genVehicleSubgraph will correctly populate the stopline information

Release R1-01b (Wed Aug 29  5:25:06 2007):
	Added functionality for generating a new graph and respective maneuvers, for performing 
	a Uturn based on this new graph. Functions are partially implemented since I am focused 
	more in integrating this basic functionality with the planning stack.

Release R1-01a (Tue Aug 28 20:25:32 2007):
	Fixed segfault when accessing the vehicleNode (vehicleNode wasn't set to NULL at startup, I don't know why it didn't break before)

Release R1-01 (Tue Aug 28 19:54:48 2007):
	Completely modified the way the graph is updated from the map

Release R1-00z (Mon Aug 27 14:39:25 2007):
	The waypointId in the GraphNode structure is set in such a way that every
	path in the graph going through a waypoint will have a node with that waypointId in it.
	If waypointId is equal to 0, the node is not close enough to a waypoint.
	If isWaypoint is true, the node is the actual waypoint.

Release R1-00y (Thu Aug 23 16:52:38 2007):
	Making more 'feasible' turns

Release R1-00x (Wed Aug 22 18:04:31 2007):
	Growing the obstacle more conservatively on the lane changes

Release R1-00w (Wed Aug 22 14:51:48 2007):
	Not aborting anymore when having a 'segment' with only one waypoint
	The center of a zone is now a center of area (rather than center of mass)

Release R1-00v (Tue Aug 21 17:47:34 2007):
	Correctly generating intersection turns when entry and exit point are in the same segment
	Displaying stopline nodes in red

Release R1-00u (Tue Aug 21 16:48:00 2007):
	Fixed a bug in the way the heading of the entry and exit pts on the zone
	perimeter is calculated.
	Added some debug info to be displayed for the important points in zones
	(notably, the heading is displayed for entry, exit and parking spots).

Release R1-00t (Tue Aug 21  6:14:57 2007):
	Minor changes to implement fully the renaming of Graph to Graph_t.

Release R1-00s (Mon Aug 20 22:18:33 2007):
	Minor adjustments

Release R1-00r (Mon Aug 20 19:38:27 2007):
	A step closer to more feasible turns at intersections

Release R1-00q (Thu Aug 16 20:03:56 2007):
	Enabled generation of zones with loading from files

Release R1-00p (Wed Aug 15 17:49:18 2007):
	Added some debugging information and prevented eventual segfaults

Release R1-00o (Tue Aug 14 10:59:53 2007):
	Added multi-rails
	Changed how lane changes are updated from the map

Release R1-00n (Tue Aug 14 12:16:49 2007):
	Implemented zone graph construction.

Release R1-00m (Tue Aug 14  9:33:07 2007):
	Updated makefile to get the new console in place

Release R1-00l (Mon Aug  6 21:17:35 2007):
	Unloading unused segments now

Release R1-00k (Sun Aug  5 22:29:29 2007):
	Corrected the obstacle bounding box size

Release R1-00j (Fri Aug  3 20:33:15 2007):
	Added possiblity to not update the graph according to the map
	Added lane change node type (for broken-white line lane changes)

Release R1-00i (Fri Aug  3  0:24:48 2007):
	Added partial loading of the RNDF (loading only segment that we might go on)
	Able to load/save graph data from/to files
	Propagated temp-planner-interfaces changes
	Still missing: Removing from memory segments that we do not use anymore

Release R1-00h (Tue Jul 31 19:59:09 2007):
	Updating graph according to the lane in map (not updating stopline yet)

Release R1-00g (Mon Jul 30  7:55:58 2007):
	Growing obstacles in a more intelligent way
	Added clearVehicleSubGraph function
	Using local coordinates everywhere now

Release R1-00f (Fri Jul 27  9:40:00 2007):
	Added multi-resolution

Release R1-00e (Wed Jul 25  9:29:19 2007):
	Removed GraphPlanner use in the update of the graph.	

Release R1-00d (Mon Jul 23 19:47:20 2007):
	Added quadtree space partitionning and bounding box check for collision detection with graph nodes.

Release R1-00c (Thu Jul 19 18:37:35 2007):
	Reverted back to a fresh R1-00b release
	Modified maximum steering angle

Release R1-00b (Mon Jul 16 13:41:56 2007):
	Now using Graph as Graph_t

Release R1-00a (Fri Jul 13 16:01:51 2007):
	Added basic functionality in order to start coding

Release R1-00 (Tue Jul 10 17:10:56 2007):
	Created.
























































