#include "GraphUpdater.hh"
#include <temp-planner-interfaces/Log.hh>
#include <temp-planner-interfaces/Console.hh>
#include <temp-planner-interfaces/Quadtree.hh>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <alice/AliceConstants.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <sys/stat.h>

#define MAXNODENUM 150000
#define MAXARCSNUM 150000
#define MAXSTEER 45
#define MAXTURN 90
#define ALICE_BOX_FRONT 8.0
#define ALICE_BOX_REAR  3.0 
#define ALICE_BOX_SIDE  1.5
#define VEHICLE_RADIUS  8.1394
//#define ALICE_BOX_FRONT 9.7112 // VEHICLE_LENGTH + DIST_REAR_AXLE_TO_FRONT
//#define ALICE_BOX_REAR  6.5956 // VEHICLE_LENGTH + DIST_REAR_TO_REAR_AXLE 
//#define ALICE_BOX_SIDE  2.0668 // VEHICLE_WIDTH/2 + 1
//#define VEHICLE_RADIUS  9.9287 // sqrt(VEHICLE_BOX_FRONT^2+ALICE_BOX_SIDE^2)
#define UPDATE_CHANGE_MANEUVERS 1

#define MIN(x,y) (((x) < (y))?(x):(y))

Graph* GraphUpdater::underlyingGraph;
CMapElementTalker GraphUpdater::meTalker;
vector<int> GraphUpdater::current_segments_loaded;

int GraphUpdater::init(Graph_t** graph, Map* map)
{
  // Initialize underlying graph
  // Now using Graph as Graph_t -- there are no reasons to do something else.
  *graph = new Graph(MAXNODENUM, MAXARCSNUM);

  // Save the underlying graph for vel-planner -- should not be used in the
  // future
  underlyingGraph = *graph;

  // Set transformation to local coordinates
  GraphUtils::delta = map->prior.delta;

  // Initialize segments currently loaded
  current_segments_loaded.clear();

  // Load the complete RNDF only if step-by-step is not enabled
  if (!CmdArgs::stepbystep_load) {
    // Load up the RNDF and initialize the graph
    if (GraphUtils::loadRndf(*graph,CmdArgs::RNDF_file.c_str()) != 0)
      return -1;
    
    if ((*graph)->getNodeCount() == 0)
      return -1;
  }

  // Initialize map element talker
  meTalker.initSendMapElement( CmdArgs::sn_key );

  return 0;
}

bool GraphUpdater::hasNewSegments(vector<int> segments)
{
  /* Check if we need to reload the RNDF */
  int new_segment;
  for (unsigned int i=0; i<segments.size(); i++) {
    new_segment = segments[i];
    if (!GraphUtils::contains(current_segments_loaded, new_segment)) {
      return true;
    }
  }
  return false;
}

int GraphUpdater::load(Graph_t *graph, Map *map, vector<int> segments)
{
  vector<int> new_segments;
  vector<int> rem_segments;

  /* Check if we need to reload the RNDF */
  bool found, reload = false;
  int new_segment, current_segment;
  for (unsigned int i=0; i<segments.size(); i++) {
    found = false;
    new_segment = segments[i];
    for (unsigned int j=0; j<current_segments_loaded.size(); j++) {
      current_segment = current_segments_loaded[j];
      if (current_segment == new_segment) {
        found = true;
        break;
      }
    }
    if (!found) {
      current_segments_loaded.push_back(new_segment);
      new_segments.push_back(new_segment);
      reload = true;
    }
  }
  if (!reload) return 0;

  /* Check if we can remove segments */
  bool remove = false;
  for (unsigned int i=0; i<current_segments_loaded.size(); i++) {
    found = false;
    current_segment = current_segments_loaded[i];
    for (unsigned int j=0; j<segments.size(); j++) {
      new_segment = segments[j];
      if (new_segment == current_segment) {
        found = true;
        break;
      }
    }
    if (!found) {
      current_segments_loaded.erase(current_segments_loaded.begin()+i);
      rem_segments.push_back(current_segment);
      remove = true;
    }
  }
  if (remove) {
    for (unsigned int i=0; i<rem_segments.size(); i++) {
      current_segment = rem_segments[i];
      Console::addMessage("Unloading segment %d from the graph", current_segment);
      GraphUtils::unload_segment(graph, current_segment);
    }
  }

  Console::addMessage("Loading %d segments from the RNDF", new_segments.size());

  /* Load up the RNDF and initialize the graph */
  if (GraphUtils::loadRndf(graph,CmdArgs::RNDF_file.c_str(), current_segments_loaded, new_segments) != 0)
    return -1;
 
  return 0;
}

int GraphUpdater::save(Graph_t * graph)
{
  vector<int> segments;

  /* Open the rndf */
  std::RNDF *rndf = new std::RNDF();
  if (!rndf->loadFile(CmdArgs::RNDF_file.c_str())) return -1;
  if (rndf->getNumOfSegments() <= 0) return -1;

  /* Loop through all segment and save them */
  for (unsigned int sn = 0; sn < rndf->getAllSegments().size(); sn++) {
    int segment = rndf->getAllSegments()[sn]->getSegmentID();
    segments.push_back(segment);
  }

  return save(graph, segments);
}

int GraphUpdater::save(Graph_t *graph, vector<int> segments)
{
  /* Generate folder name from RNDF path */
  ostringstream oss;
  string folder;
  folder.assign(CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("/")+1, CmdArgs::RNDF_file.begin()+CmdArgs::RNDF_file.find_last_of("."));
  oss << folder << ".d";
  folder = oss.str();

  /* create folder if necessary */
  struct stat st;
  stat(folder.c_str(), &st);
  if (errno == ENOENT) {
    if (mkdir(folder.c_str(), 0755) != 0) return -1;
  }

  /* Loop through all segment and save them */
  for (unsigned int sn = 0; sn < segments.size(); sn++) {
    int segment = segments[sn];
    GraphUtils::save_segment(graph, folder, segment);
  }

  return 0;
}

void GraphUpdater::destroy(Graph_t* graph)
{
  delete graph;
  return;
}


Graph* GraphUpdater::getGraph()
{
  return underlyingGraph;
}

Err_t GraphUpdater::genVehicleSubGraph(Graph_t *graph, VehicleState &vehState, ActuatorState &actState)
{
  // Generate subgraph from our current pose to nearby nodes
  GraphUtils::genVehicleSubGraph( graph,&vehState, &actState );
  return GU_OK;
}

/**
 * @brief Clear the volatile nodes in the graph
 */
void GraphUpdater::clearVehicleSubGraph(Graph_t *graph)
{
  graph->clearVolatile();
}

/**
 * @brief Update the graph according to the map
 *
 * The strategy is the following:
 *  - get the current lane we are in (as well as the neighboring lanes)
 *  - for each node on those lane check the distance to the center lane
 *  - if any of the node on the lane has a distance larger than some threshold
 *    move the all lane nodes to the centerline
 */
Err_t GraphUpdater::updateGraphMap(Graph_t *graph, VehicleState &vehState, Map *map)
{
  GraphNode *node, *next_node, *alice_node, *end_node;
  vector<GraphNode *> nodes;
  int real_direction = 0;
  double dist, current_dist, total_dist, real_dist;
  double alice_pos_along, dist_to_center, dist_on_center;
  bool wrong_direction;
  point2 point, end_pos, alice_pos, tmp_pt;
  vec3_t alice_vec;
  point2arr centerline;
  LaneLabel current_lane;
  vector<LaneLabel> lanes;
  VehicleConfiguration configA, config;
  Pose2D poseB;
  Vehicle *vp;
  Maneuver *mp;
  double roll, pitch, yaw, s;

  /* Set variables: alice_pos */
  alice_pos = AliceStateHelper::getPositionRearAxle(vehState);

  /* Get all the lanes contiguous to the current lane */
  if (map->getLane(current_lane, alice_pos) == -1) return GU_OK;
  map->getAllDirLanes(lanes, current_lane);

  /* For all the lanes update the points going forward and backward */
  for (unsigned int i = 0; i<lanes.size(); i++) {
    /* Get centerline of the lane */
    map->getLaneCenterLine(centerline, lanes[i]);

    /* Get alice vector on that lane */
    point = centerline.project(alice_pos);
    alice_pos_along = centerline.project_along(point);
    wrong_direction = GraphUtils::isReverse(vehState, map, lanes[i]);
    if (wrong_direction) alice_pos_along = centerline.linelength() - alice_pos_along;
    alice_vec.x = point.x;
    alice_vec.y = point.y;
    alice_vec.z = 0;

    /* Go through all directions (start with forward direction) */
    for (int direction = 1; direction > -2; direction -= 2) {
      /* Get alice node */
      alice_node = graph->getNearestNode(alice_vec, GRAPH_NODE_LANE, 10, direction);
      point.set(alice_node->pose.pos.x, alice_node->pose.pos.y);
      alice_pos_along = centerline.project_along(point);
      wrong_direction = GraphUtils::isReverse(vehState, map, lanes[i]);
      if (wrong_direction) alice_pos_along = centerline.linelength() - alice_pos_along;

      /* Check in which direction we are actually going on the lane */
      if (wrong_direction) {
        real_direction = -1;
      } else {
        real_direction = 1;
      }
      real_direction *= direction;

      /* Get end node as well as the total distance */
      total_dist = 0.0;
      for (node = alice_node, next_node = GraphUtils::getLaneNode(graph,node,real_direction);
           node != NULL && next_node != NULL && total_dist < 40.0;
           node = next_node, next_node = GraphUtils::getLaneNode(graph,node,real_direction)) {
        total_dist += vec3_mag(vec3_sub(node->pose.pos, next_node->pose.pos));
      }
      if (total_dist < 1.0) return GU_OK;
      end_node = node;
      end_pos.set(node->pose.pos.x, node->pose.pos.y);

      /* get real distance */
      real_dist = centerline.project_along(end_pos);
      if (wrong_direction) real_dist = centerline.linelength() - real_dist;
      real_dist -= alice_pos_along;

      /* For all nodes until the next waypoint modify its position if necessary */
      current_dist = 0.0;
      for (node = alice_node, next_node = GraphUtils::getLaneNode(graph,node,real_direction);
           node != NULL && next_node != NULL && node != end_node;
           node = next_node, next_node = GraphUtils::getLaneNode(graph,node,real_direction)) {
        point.set(node->pose.pos.x, node->pose.pos.y);
        dist_to_center = centerline.get_min_dist_line(point);
        dist = alice_pos_along+real_dist*(current_dist/total_dist);
        if (wrong_direction) dist = centerline.linelength() - dist;
        dist_on_center = fabs(current_dist-dist);

        /* The distance to/on the centerline is too big */
        if (dist_to_center > 0.2 /* || dist_on_center > 0.2 */) { // MAGIC

          /* Get correct position */
          point = centerline.get_point_along(dist);
          current_dist += vec3_mag(vec3_sub(node->pose.pos, next_node->pose.pos));
          /* Notify the quadtree */
          graph->quadtree->move_node(node, point.x, point.y);
          /* Update node position */
          node->pose.pos.x = point.x;
          node->pose.pos.y = point.y;
          map->getHeading(yaw, tmp_pt, lanes[i], point);
          if (direction == -1) yaw += M_PI;
          if (yaw > M_PI) yaw -= 2*M_PI;
          node->pose.rot = quat_from_rpy(0, 0, yaw);
          pose3_to_mat44d(node->pose, node->transGN);
          mat44d_inv(node->transNG, node->transGN);

          /* Modify rail position */
          for (int rail = -1; rail < 2; rail+=2) {
            GraphNode *railNode = (rail == -1)?node->leftNode:node->rightNode;
            point.x = node->pose.pos.x + rail*sin(yaw)*((railNode->laneWidth-VEHICLE_WIDTH)/2.0);
            point.y = node->pose.pos.y - rail*cos(yaw)*((railNode->laneWidth-VEHICLE_WIDTH)/2.0);
            graph->quadtree->move_node(node, point.x, point.y);
            railNode->pose.pos.x = point.x;
            railNode->pose.pos.y = point.y;
            railNode->pose.rot = node->pose.rot;
            pose3_to_mat44d(railNode->pose, railNode->transGN);
            mat44d_inv(railNode->transNG, railNode->transGN);
          }

#if UPDATE_CHANGE_MANEUVERS == 1
          for (int rail = -1; rail < 2; rail++) {
            GraphNode *nodeA, *nodeB, *tmpNode;
            tmpNode = (rail == -1)?node->leftNode:((rail == 0)?node:node->rightNode);
            /* Check lane change maneuvers (in reverse) */
            unsigned int changes_num = GraphUtils::getChangeCount(graph, tmpNode, -real_direction);
            if (changes_num == 0) continue;
            for (unsigned int change_index = 0; change_index < changes_num; change_index++) {
              GraphUtils::getChangeNodes(nodes, &nodeA, &nodeB, graph, tmpNode, -real_direction, change_index);
              if (nodeA == NULL || nodeB == NULL) continue;

              /* Re-generate the maneuver from nodeA ... */
              configA.x = nodeA->pose.pos.x;
              configA.y = nodeA->pose.pos.y;
              quat_to_rpy(nodeA->pose.rot, &roll, &pitch, &yaw);
              configA.theta = yaw;
              configA.phi = nodeA->steerAngle;
              /* ... to nodeB */
              poseB.x = nodeB->pose.pos.x;
              poseB.y = nodeB->pose.pos.y;
              quat_to_rpy(nodeB->pose.rot, &roll, &pitch, &yaw);
              poseB.theta = yaw;
              vp = maneuver_create_vehicle(VEHICLE_WHEELBASE, 45*M_PI/180);
              mp = maneuver_config2pose(vp, &configA, &poseB);
              /* if impossible forget about it */
              if (!mp) {
                maneuver_free(mp);
                free(vp);
                continue;
              }
              
              for (unsigned int i = 1; i<nodes.size()-1; i++) {
                tmpNode = nodes[i];
                s = (double) i / nodes.size();
                config = maneuver_evaluate_configuration(vp, mp, s);
                graph->quadtree->move_node(tmpNode, config.x, config.y);
                tmpNode->pose.pos = vec3_set(config.x, config.y, 0);
                tmpNode->pose.rot = quat_from_rpy(0, 0, config.theta);
                tmpNode->steerAngle = config.phi;
                pose3_to_mat44d(tmpNode->pose, tmpNode->transGN);
                mat44d_inv(tmpNode->transNG, tmpNode->transGN);
              }
              maneuver_free(mp);
              free(vp);
            }
          }
#endif
        } else {
          current_dist += vec3_mag(vec3_sub(node->pose.pos, next_node->pose.pos));
        }
      }
    }
  }

  return GU_OK;
}

/**
 * @brief This function modifies the graph according to the map and the current state problem
 *
 * Obstacles and cars in the map are not treated in the same way depending on the current
 * state problem. The role of this function is to update the graph accordingly.
 */
Err_t GraphUpdater::updateGraphStateProblem(Graph_t *graph, StateProblem_t &problem, Map *map)
{
  GraphNode *node;
  MapElement *mapEl;
  point2 obs_pt, point;
  point2arr new_geom, bounds;
  LaneLabel node_lane;
  double x_min, x_max, y_min, y_max, x, y, bx_min, bx_max, by_min, by_max;
  double alice_rear, alice_front, alice_side_left, alice_side_right;
  point2arr alice_box;
  double bb_x, bb_y, bb_radius, tmp;
  float m[4][4];
  bool collide, obs_in_lane;
  vector<GraphNode *> nodes;

  // Reset the collision flags for all nodes
  for (int i = 0; i < graph->getNodeCount(); i++) {
    node = graph->getNode(i);
    node->collideObs = false;
    node->collideCar = false;
  }

  // Check each node against each object in the map for a possible
  // collision.
  for (int j = 0; j < (int)map->data.size(); j++) {
    mapEl = &map->data[j];
    if (mapEl->type != ELEMENT_OBSTACLE && mapEl->type != ELEMENT_VEHICLE)
      continue;

    // Build bounding boxes
    x_min = y_min = INFINITY;
    x_max = y_max = -INFINITY;
    for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
      obs_pt.set(mapEl->geometry[k]);
      if (obs_pt.x < x_min) x_min = obs_pt.x;
      if (obs_pt.y < y_min) y_min = obs_pt.y;
      if (obs_pt.x > x_max) x_max = obs_pt.x;
      if (obs_pt.y > y_max) y_max = obs_pt.y;
    }
    // Create the big bounding box
    bx_min = x_min - VEHICLE_RADIUS;
    bx_max = x_max + VEHICLE_RADIUS;
    by_min = y_min - VEHICLE_RADIUS;
    by_max = y_max + VEHICLE_RADIUS;

    // Get the nodes that might collide with the obstacle
    nodes.clear();
    // Get all possible nodes that might collide from the quadtree
    graph->quadtree->get_all_nodes(nodes, bx_min, bx_max, by_min, by_max);
    // Add the volatile node only if they can collide with the obstacle
    tmp = sqrt(pow(graph->vehicleNode->pose.pos.x - mapEl->center.x,2) +
               pow(graph->vehicleNode->pose.pos.y - mapEl->center.y,2));
    if (tmp < 20) {
      // Explicitely add the volatile nodes (that are not in the quadtree)
      for (int i=graph->getStaticNodeCount(); i < graph->getNodeCount(); i++) {
        nodes.push_back(graph->getNode(i));
      }
    }

    // Check each node
    for (unsigned int i = 0; i < nodes.size(); i++) {
      collide = false;
      node = nodes[i];
      // Ignore node that already collided (give priority to vehicle collision)
      if (node->collideCar) continue;
      if (mapEl->type == ELEMENT_OBSTACLE && node->collideObs) continue;

      // Check against big bounding box
      x = node->pose.pos.x;
      y = node->pose.pos.y;
      if (x < bx_min || x > bx_max || y < by_min || y > by_max) continue;

      // Transform small bounding box circle into node coordinates
      pose3_to_mat44f(pose3_inv(node->pose), m);
      bb_radius = sqrt(pow((x_max-x_min)/2,2)+pow((y_max-y_min)/2,2));
      bb_x = (x_min+x_max)/2;
      bb_y = (y_min+y_max)/2;
      tmp  = m[0][0] * bb_x + m[0][1] * bb_y + m[0][3];
      bb_y = m[1][0] * bb_x + m[1][1] * bb_y + m[1][3];
      bb_x = tmp;

      // check against unrestricted bounding box around bounding circle
      if (ALICE_BOX_FRONT < bb_x - bb_radius || -ALICE_BOX_REAR > bb_x + bb_radius ||
          ALICE_BOX_SIDE < bb_y - bb_radius || -ALICE_BOX_SIDE > bb_y + bb_radius) continue;

      // restrict alice box according to current situation of the node
      obs_in_lane = false;
      node_lane.segment = node->segmentId;
      node_lane.lane = node->laneId;
      // Check if obstacle in the same lane
      if (node->type & GRAPH_NODE_LANE) {
        if (map->getLaneBoundsPoly(bounds, node_lane) < 0) {
          obs_in_lane = true;
        } else {
          obs_in_lane = mapEl->isOverlap(bounds);
        }
      }
      if (!(node->type & GRAPH_NODE_LANE) || !obs_in_lane) {
        // If the node is not in a lane or the obstacle is not in the same
        // lane than the node then use the 1m separation distance
        alice_front = DIST_REAR_AXLE_TO_FRONT + 1.0;
        alice_rear = DIST_REAR_TO_REAR_AXLE + 1.0;
        alice_side_left = VEHICLE_WIDTH/2.0 + 1.0;
        alice_side_right = alice_side_left;
      } else {
        // get distance to next turn node
        double dist = 1.0 + DIST_REAR_AXLE_TO_FRONT;
        GraphNode *prev_node = node;
        do {
          GraphNode *next_node = graph->getNextLaneNode(prev_node);
          if (next_node == NULL || (next_node->type & GRAPH_NODE_TURN)) break;
          dist += sqrt(pow(prev_node->pose.pos.x - next_node->pose.pos.x,2) + pow(prev_node->pose.pos.y - next_node->pose.pos.y,2));
          prev_node = next_node;
        } while (dist < ALICE_BOX_FRONT);
        // set front
        alice_front = MIN(dist, ALICE_BOX_FRONT);
        // set rear
        alice_rear = ALICE_BOX_REAR;
        // set side
        if (node->railId == 1) {
          alice_side_right = VEHICLE_WIDTH/2.0 + 0.2;
          alice_side_left = ALICE_BOX_SIDE;
        } else if (node->railId == -1) {
          alice_side_left = VEHICLE_WIDTH/2.0 + 0.2;
          alice_side_right = ALICE_BOX_SIDE;
        } else {
          alice_side_left = alice_side_right = MIN(node->laneWidth/2.0 + 0.2, ALICE_BOX_SIDE);
        }
      }

      // Transform obstacle into node coordinates
      new_geom.clear();
      for (unsigned int k=0; k<mapEl->geometry.size(); k++) {
        obs_pt.set(mapEl->geometry[k]);
        point.x = m[0][0] * obs_pt.x + m[0][1] * obs_pt.y + m[0][3];
        point.y = m[1][0] * obs_pt.x + m[1][1] * obs_pt.y + m[1][3];
        new_geom.push_back(point);

        // Check point with alice bounding box
        if (point.x > -alice_rear && point.x < alice_front &&
            point.y > -alice_side_right && point.y < alice_side_left) {
          collide = true;
          break;
        }
      }
      // Check for intersection between bounding box and obstacle (worst case scenario)
      if (!collide) {
        new_geom.push_back(new_geom[0]);

        // populate alice box
        alice_box.clear();
        alice_box.push_back(point2(alice_front, alice_side_left));
        alice_box.push_back(point2(alice_front, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, -alice_side_right));
        alice_box.push_back(point2(-alice_rear, alice_side_left));
        alice_box.push_back(point2(alice_front, alice_side_left));

        collide = new_geom.is_intersect(alice_box);
      }

      if (mapEl->type == ELEMENT_OBSTACLE)
        node->collideObs = collide;
      else if (mapEl->type == ELEMENT_VEHICLE)
        node->collideCar = collide;
    }
  }

  return GU_OK;
}

void GraphUpdater::display(int sendSubgroup, Graph_t* graph, VehicleState vehState)
{
    MapElement me;
    
    // DISPLAY THE GRAPH
    point2 point, vehPos;
    vector<point2> free_points;
    vector<point2> obs_points;
    vector<point2> car_points;
    MapId free_mapId, obs_mapId, car_mapId;
    GraphNode* node;

    double dotProd, dist;
    double heading = vehState.localYaw;
    vehPos.set(vehState.localX, vehState.localY);
    free_mapId = 10000;
    obs_mapId = 10001;
    car_mapId = 10002;
    for (int i=0; i<graph->getStaticNodeCount(); i++) {
      node = graph->getNode(i);
      if (node->type != GRAPH_NODE_LANE && node->type != GRAPH_NODE_TURN) continue;
      point.set(node->pose.pos.x, node->pose.pos.y);
      dotProd = (point.x-vehPos.x)*cos(heading) + (point.y-vehPos.y)*sin(heading);
      dist = point.dist(vehPos);
      if (dotProd>0 && dist<25) {
        if (node->collideObs)
          obs_points.push_back(point);
        else if (node->collideCar)
          car_points.push_back(point);
        else
          free_points.push_back(point);
      }
    }

    // Send free nodes
    me.setId(free_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_LIGHT_BLUE,100);
    me.setGeometry(free_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by an obstacle
    me.setId(obs_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_YELLOW,100);
    me.setGeometry(obs_points);
    meTalker.sendMapElement(&me,sendSubgroup);

    // Send nodes occupied by a car
    me.setId(car_mapId);
    me.setTypePoints();
    me.setColor(MAP_COLOR_ORANGE,100);
    me.setGeometry(car_points);
    meTalker.sendMapElement(&me,sendSubgroup);
}
