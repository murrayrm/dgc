/*
 *  File: UT_gUturn.cc
 *  Description: Unit Test for U-turn graph generation
 *  Author: Francisco Zabala
 *  Date: 08-01-2007
 */

#include <temp-planner-interfaces/PlannerInterfaces.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ActuatorState.h>
#include <frames/pose3.h>
#include <temp-planner-interfaces/CmdArgs.hh>
#include <iostream>
#include <sstream>
#include <fstream>
#include <trajutils/traj.hh>
#include <map/MapElement.hh>
#include <map/MapElementTalker.hh>
#include <skynet/skynet.hh>
#include <path-planner/PathPlanner.hh>
#include "../planner/PlannerUtils.hh"

#include "GraphUpdater.hh"
#include "GraphUtils.hh"

using namespace std;

/*
 * This Unit Test validates the generation of 
 * the graph used for making an U-turn.
 */

int main(int argc, char **args)
{
  // INITIALIZE THE MAP
  Map* map = new Map();
  map->loadRNDF(args[1]);
  map->prior.delta = point2(3778410.5, 403942.3);
    
  // INITIALIZE THE GRAPH
  Graph_t* graph;
  Graph_t* gUturn;

  // PLAN PATH
  Path_t path;
  memset(&path, 0 ,sizeof(path));
  VehicleState vehState;      // Allocate resources for vehicle state
  memset(&vehState,0,sizeof(vehState));
  ActuatorState actState;     // Allocate resources for actuator state
  memset(&actState,0,sizeof(actState));
  
  CmdArgs::RNDF_file = args[1];
  CmdArgs::sn_key = skynet_findkey(argc, args);
  
  /* 
   * Insert an obstacle at waypoint 1.1.4
   * to test U-Turn due to road block
   */
  MapElement el;
  point2 tmp_pt;
  PointLabel ptlabel(1,1,4);
  map->getWaypoint(tmp_pt, ptlabel);

  el.setId(-1);
  el.setTypeObstacle();
  el.setGeometry(tmp_pt,3.0);
  map->data.push_back(el);

  // Declare variables
  Cost_t cost = 0;
  pose3_t finPose;
  double roll, pitch, yaw;
  double heading;
  const point2 ref_pt = tmp_pt;
  Uturn_t uTurn = UTURN_DARPA;

  // Initialize the Graph
  GraphUpdater::init(&graph, map); 
  /* 
   * Position the vehicle waypoint 1.1.1
   */
  GraphNode* initNode = graph->getNodeFromRndfId(1,1,3);

  initNode->pathDir = GRAPH_PATH_REV;

  vehState.localX = initNode->pose.pos.x + 1;
  vehState.localY = initNode->pose.pos.y + 1;
  vehState.localZ = initNode->pose.pos.z;
  quat_to_rpy(initNode->pose.rot, &roll, &pitch, &yaw);
  // vehState.localYaw = yaw;

  ptlabel.segment = 1;
  ptlabel.lane = 1;
  ptlabel.point = 3;
  map->getHeading(heading, ptlabel);
  vehState.localYaw = heading;
  
  cout << "    [UT]Before genUturn... vehState.localX = " << vehState.localX << ", vehState.localY = " << vehState.localY << endl;

  GraphNode* nodeVeh;


  // Initialize the Uturn graph
  GraphUpdater::genUturnGraph(&gUturn, map, finPose, ref_pt,
                              &nodeVeh, vehState, actState,
                              uTurn); 

  nodeVeh->pathDir = GRAPH_PATH_REV;

  cout << "    [UT]Before genManeuv... nodeVeh->pose.pos (x = " << nodeVeh->pose.pos.x << ", y = " << nodeVeh->pose.pos.y << ")" << endl;

  GraphUtils::genUturnManeuvers(gUturn, &nodeVeh, vehState, actState);

  // set final pose
  GraphNode* finalNode = graph->getNodeFromRndfId(1,1,4);

  finalNode->pathDir = GRAPH_PATH_FWD;

  // Final pose is set by the Graph Generation algorithm
  // finPose = finalNode->pose; 
  
  // plan the path
  Path_params_t pathParams;
  PathPlanner::init();
 
  pathParams.flag = NO_PASS;
  pathParams.planFromCurrPos = true;
  if (pathParams.planFromCurrPos) {
    /* Update graph to account for the current car position */
    // GraphUpdater::genVehicleSubGraph(graph, vehState, actState);
    // GraphUpdater::genVehicleSubGraph(gUturn, vehState, actState);

  }
  cout << "    [UT]Before planPath... finPose.pos (x = " << finPose.pos.x << ", y = " << finPose.pos.y << ")" << endl;
 
  PathPlanner::planPath(&path, cost, gUturn, vehState, finPose, pathParams);
  Path_t subPath;

  cout << " === " << path.path[0];

  PlannerUtils::extractSubPath(subPath, path, vehState);

  // DISPLAY INFORMATION IN MAPVIEWER
  GraphUpdater::display(10, gUturn, vehState);
  PathPlanner::display(10, &path);

  GraphUpdater::display(11, gUturn, vehState);
  PathPlanner::display(11, &subPath);

  Quadtree quadtree;
  quadtree.create_tree(gUturn);

  for (int i=0; i < gUturn->getStaticNodeCount()/3; i++) {
    quadtree.remove_node(gUturn->getNode(i));
  }

  for (int i=gUturn->getStaticNodeCount()/3; i < (int)(2.0*gUturn->getStaticNodeCount()/3.0); i++) {
    quadtree.move_node(gUturn->getNode(i), -50, -50);
  }

  quadtree.print();

  vector<GraphNode *> nodes;
  double x_min = -50;
  double x_max = -10;
  double y_min = -50;
  double y_max = 0;
  quadtree.get_all_nodes(nodes, x_min, x_max, y_min, y_max);
  cout << "Number of nodes to check is " << nodes.size() << endl;
  
  ofstream file_out;
  file_out.open("plot_quad.m", ios_base::app);

  ostringstream nodes_x, nodes_y;
  nodes_x << "nodes_x = [ ";
  nodes_y << "nodes_y = [ ";
  for (unsigned int i = 0; i<nodes.size(); i++) {
    nodes_x << nodes[i]->pose.pos.x << " ";
    nodes_y << nodes[i]->pose.pos.y << " ";
  }
  nodes_x << " ];" << endl;
  nodes_y << " ];" << endl;

  file_out << nodes_x.str() << nodes_y.str();
  file_out << "plot(nodes_x, nodes_y, 'r.'); hold on;" << endl;
  file_out << "x = [ " << x_min << " " << x_max << " " << x_max << " " << x_min << " " << x_min << " ];" << endl;
  file_out << "y = [ " << y_min << " " << y_min << " " << y_max << " " << y_max << " " << y_min << " ];" << endl;
  file_out << "plot(x, y, 'g-'); hold on;" << endl << endl;
  file_out.close();

}

