/*!
 * \file OBDIISensor.cc
 * \brief Read OBD-II hardware
 *
 * \author Richard Murray
 * \date 22 April 2007
 *
 * \ingroup gcsensor
 *
 * This file contains the routines required to read and update the
 * OBD-II status.  This is not a gcmodule, just a thread that keeps
 * track of the output from the OBD-II unit (roughly the status loop
 * part of an actuator module).
 *
 * NOTE: This version of the sensor spoofs OBD-II data by running as
 * an StateClient and inferring information from the vehicle state.
 */

#include "alice/AliceConstants.h"

#include "OBDIISensor.hh"
#include "ActuationInterface.hh"	// for sleep times

/*
 * OBDIISensor::OBDIISensor
 *
 * This constructor initializes the OBDIISensor class, including
 * establishing connection to the hardware.
 *
 */

OBDIISensor::OBDIISensor(ActuationInterface *actuationInterface, int sn_key, 
			 int verbose_flag)
{
  verbose = verbose_flag;

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * Hardware/simulation initailization
   *
   * All we have to do here is open up the serial port, since the
   * obdii box continuously sends out commands.  If we are running in
   * simulation mode, we don't bother trying to talk to the hardware.
   *
   * If we are using vehicle state to fake the OBD-II, we initialize a
   * state client here.  We check for this case before simulation so
   * that we can test --fake-obd.
   */

  if (adrive->options->fake_obd_flag) {
    /* Use veh state to infer OBD II settings; start thread to read state */  
    if (verbose >= 2) cerr << "OBDII: running with fake OBD" << endl;
    DGCstartMemberFunctionThread(this, &OBDIISensor::getVehicleState);
    initialized = 1;
    connected = 1;

  } else if (adrive->options->simulate_given) {
    /* Simulation mode - no action required */
    initialized = 1;
    connected = 1;

  } else {
#   ifdef HARDWARE
    /* Open the port to the obdii box */
    if (!obdii_open(adrive->options->obdii_port_arg)) {
      if (adrive->options->disable_obdii_flag) {
	/* OBDII has been disabled, so allow the error to pass */
	connected = 0;
	initialized = 1;

      } else {
	/* Don't allow operation without being able to talk to obdii */
	cerr << "OBDIISensor: obdii_open failed" << endl;
	exit(1);
      }
    } else {
      /* Keep track of the fact that we are connected */
      connected = 1;
      initialized = 1;
    }
#  endif
  }

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&sensorMutex);

  /*
   * Status thread startup
   *
   * As part of the OBDIISensor function, we start up a thread to read
   * the status of the obdii box on a periodic basis.  This thread
   * fills in data in the OBDIISensor class, so we have easy access to
   * this information.
   */

  /* Start up the thread */
  statusCount = 0;
  statusSleepTime = STATUS_SLEEP*2;
  DGCstartMemberFunctionThread(this, &OBDIISensor::status);
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void OBDIISensor::initSparrow(DD_IDENT *tbl, int label_id)
{
  /* Save the label for later usage */
  sparrowLabel = label_id;
  sparrowTable = tbl;

  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the OBDII hardware */
      dd_setcolor_tbl(label_id, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to OBDII interface, but couldn't initialize */
      dd_setcolor_tbl(label_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(label_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("obdii.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("obdii.fake_loop_counter", &fakeCount, tbl);
    dd_rebind_tbl("obdii.engine_rpm", &engineRPM, tbl);
    dd_rebind_tbl("obdii.engine_temp", &engineCoolantTemp, tbl);
    dd_rebind_tbl("obdii.wheel_speed", &vehicleWheelSpeed, tbl);
    dd_rebind_tbl("obdii.gear_ratio", &currentGearRatio, tbl);
  }
}

/*
 * OBDIISensor::status
 *
 * This function is called as a thread and is used to read the status
 * of the OBDII hardware.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void OBDIISensor::status()
{
  while (1) {
    /* Check for OBD faking first (so that we can test in simulation) */
    if (adrive->options->fake_obd_flag) {
      /* Use vehicle state to infer OBD II settings */
      // Should put a mutex here to avoid updates (not critical)

      // Gear ratio: set based on currently commanded gear
      double gear_ratio;
      switch (adrive->state.m_transpos) {
      case -1:	gear_ratio = -R_TRANS1; break;	// reverse
      case 1:	gear_ratio = R_TRANS3;	break;	// forward
      default:	gear_ratio = 0;		break;	// park, neutral
      }
      currentGearRatio = gear_ratio;

      // Wheel speed: based on current velocity, converted to rad/sec
      vehicleWheelSpeed = 
	sqrt( pow(vehState.utmNorthVel, 2) + pow(vehState.utmEastVel, 2) ) /
	VEHICLE_TIRE_RADIUS;

      // Engine RPM: if running, base on gear ratio, otherwise 2000 rpm
      double gearboxRPM = gear_ratio == 0 ? 0 :
	vehicleWheelSpeed * 60 *
	(fabs(gear_ratio) * R_DIFF * R_TRANSFER * R_TC) / (2 * M_PI);
      engineRPM = fmax(gearboxRPM, W_IDLE * 60 / (2 * M_PI)); 

    } else if (adrive->options->simulate_given) {
      /* In simulation mode, read state from asim */
      if (adrive->asim_status) {
	engineRPM = adrive->asim_state.m_engineRPM;
	engineCoolantTemp = adrive->asim_state.m_EngineCoolantTemp;
	vehicleWheelSpeed = adrive->asim_state.m_VehicleWheelSpeed;
	currentGearRatio = adrive->asim_state.m_CurrentGearRatio;
      }

    } else {
#ifdef HARDWARE
      if (connected) {
	/* Read the current status */
	pthread_mutex_lock(&sensorMutex);
	int retval = obdii_status();
	pthread_mutex_unlock(&sensorMutex);

	/* Check the status and set state appropriately */
	if (retval == -1) {
	  /* Error condition */
#         warning obdii status error not yet handled
	  cerr << "OBDIISensor::status error reading obdii position"
	       << endl;
	  exit(1);

	} else {
	  currentStatus = retval;
	}
      }
#endif
    }

    /* Now fill up the actuatorState struct with current state */
    adrive->state.m_obdiistatus = initialized && connected;
    adrive->state.m_engineRPM = engineRPM;
    adrive->state.m_EngineCoolantTemp = engineCoolantTemp;
    adrive->state.m_VehicleWheelSpeed = vehicleWheelSpeed;
    adrive->state.m_CurrentGearRatio = currentGearRatio;

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

void OBDIISensor::getVehicleState()
{
  int statesocket = adrive->m_skynet->listen(SNstate, ALLMODULES);

  while (1) {
    /* Read the state data from skynet */
    int count =
      adrive->m_skynet->get_msg(statesocket, &vehState, sizeof(vehState), 0);

    /* Make sure we got the right number of bytes */
    if (count != sizeof(vehState)) fakeCount = -1;

    /* Keep track of the number of times we run */
    ++fakeCount;
  }
}
