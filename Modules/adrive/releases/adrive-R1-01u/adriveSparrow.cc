/*
 * \file adriveSparrow
 * \brief Sparrow interface to adrive
 *
 * \author Richard Murray
 * \date 16 April 2007
 *
 * \ingroup gcdrive
 *
 * The functions below are used by the sparrow display to control the
 * operation of adrive.
 */

#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "gcinterfaces/AdriveCommand.hh"
#include "sparrow/display.h"

#include "adriveSparrow.hh"
#include "ActuationInterface.hh"	// for sleep times
#include "overview.h"

/* 
 * Create the sparrowModule class for use with GcModule 
 * 
 * This class is not actually used, but we need it to create the
 * interface to adrive.  Everything is inlined here since we don't
 * actually require any functionality.
 */
class SparrowModule : public GcModule
{
private:
  ControlStatus m_sparrowStatus;		// unused
  MergedDirective m_sparrowDirective;	// unused

public:
  SparrowModule(int skynet_key) :
    GcModule("adriveSparrow", &m_sparrowStatus, &m_sparrowDirective, 
	     SLOW_SLEEP, SLOW_SLEEP)
  {}

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *) {};
  void control(ControlStatus *, MergedDirective *) {};
};

SparrowModule *sparrowModule = NULL;
AdriveCommand *adriveInterface;
AdriveCommand::Southface *adriveInterfaceSF;
static int id = 0x1000;

/* Initialize the sparrow communications module */
void adrive_initSparrow(int skynet_key)
{
  /* Create the adrive interface and save pointers */
  sparrowModule = new SparrowModule(skynet_key);

  /* Create interface to actuation interface (router) */
  adriveInterfaceSF = 
    AdriveCommand::generateSouthface(skynet_key, sparrowModule);

  sparrowModule->Start();
}

/*
 * Sparrow callbacks
 *
 * There are two main functions that are used for callbacks:
 *   * adrive_set_position - send SetPosition directive
 *   * adrive_reset_actuator - send Reset directive
 *
 */

/*! Send a directive to an actuator */
int adrive_set_position(AdriveActuator actuator, char *name, double arg)
{
  if (sparrowModule == NULL) return 0;

  /* Set up the command and send it off through skynet */
  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = actuator;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = arg;

  cerr << "Sending " << name << " command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  /* Wait for the response */
  for (int i = 0; i < 20; ++i) {
    if (adriveInterfaceSF->haveNewStatus()) break;
    DGCusleep(STATUS_SLEEP); 
  }

  /* Get the response and parse it */
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  if (response == NULL) {
    cerr << "Timeout waiting for response to " << name << " command" << endl;

  } else if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
    cerr << "Command error for " << name << " (" << response->id << "): "
	 << "failure = " << response->reason << endl;
  }

  return 0;
}

/*! Send a directive to an actuator */
int adrive_reset_actuator(AdriveActuator actuator, char *name)
{
  if (sparrowModule == NULL) return 0;

  /* Set up the command and send it off through skynet */
  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = actuator;
  adriveCommand.command = Reset;

  cerr << "Sending reset to " << name << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  /* Wait for the response */
  for (int i = 0; i < 100; ++i) {
    if (adriveInterfaceSF->haveNewStatus()) break;
    DGCusleep(FAST_SLEEP); 
  }

  /* Get the response and parse it */
  int status = -1;
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  if (response == NULL) {
    cerr << "Timeout waiting for response to " << name << " reset" << endl;

  } else if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
    cerr << "Command error for " << name << " (" << response->id << "): "
	 << "failure = " << response->reason << endl;
  } else
    status = 0;

  return status;
}

/*! Set the steering angle from the display */
int adrive_setSteering_cb(long arg)
{
  return adrive_set_position(Steering, "steer", *((double *) arg));
}

/*! Reset the steering interface */
int adrive_resetSteering_cb(long arg)
{
  int status = adrive_reset_actuator(Steering, "steer");
  dd_setcolor(STEER_LABEL, BLACK, status < 0 ? RED : GREEN);
  return status;
}

/*! Set the throttle command from the display */
int adrive_setThrottle_cb(long arg)
{
  return adrive_set_position(Acceleration, "accel", *((double *) arg));
}

/*! Set the brake command from the display */
int adrive_setBrake_cb(long arg)
{
  return adrive_set_position(Acceleration, "accel", -(*((double *) arg)));
}

/*! Reset the acceleration interface */
int adrive_resetAcceleration_cb(long arg)
{
  int status = adrive_reset_actuator(Acceleration, "accel");
  dd_setcolor(GAS_LABEL, BLACK, status < 0 ? RED : GREEN);
  dd_setcolor(BRAKE_LABEL, BLACK, status < 0 ? RED : GREEN);
  return status;
}

/*! Set the transmission gear from the display */
int adrive_setTrans_cb(long arg)
{
  return adrive_set_position(Transmission, "trans", *((int *) arg));
}

/*! Reset the transmission interface */
int adrive_resetTransmission_cb(long arg)
{
  int status = adrive_reset_actuator(Transmission, "trans");
  dd_setcolor(GAS_LABEL, BLACK, status < 0 ? RED : GREEN);
  dd_setcolor(BRAKE_LABEL, BLACK, status < 0 ? RED : GREEN);
  return status;
}

/*! Set the turn signals from the display */
int adrive_setTurn_cb(long arg)
{
  return adrive_set_position(TurnSignal, "turn", *((int *) arg));
}

/*! Reset all actuators */
int adrive_resetActuators_cb(long arg)
{
  adrive_resetAcceleration_cb(arg);
  adrive_resetSteering_cb(arg);
  adrive_resetTransmission_cb(arg);
  return 0;
}
