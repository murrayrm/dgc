/*!
 * \file UT_turnsignals.cc
 * \brief Test turnsignals device interface
 * 
 * \author Jeremy Ma
 * \date May 2007
 *
 * This file is a unit test for the turn signal device driver in
 * turnsignals.cc.  Written by Jeremy Ma, but documented by RMM (ahem).
 */

#include <stdio.h>
#include <stdlib.h>
#include "turnsignals.hh"

void queryStatus(Turnsignal *turnsignal, TurnType* status)
{
  sleep(2);
 // Query status
  for(int i=1;i<20;i++)
    {
      turnsignal->getpos(status);

      if(*status==LEFT)
	printf("status is LEFT\n");
      else if(*status==RIGHT)
	printf("status is RIGHT\n");
      else if(*status==HAZARD)
	printf("status is HAZARD\n");
      else if(*status==HOME)
	printf("status is HOME\n");
      else
     	printf("status is %d \n", *status);      
      //usleep(250000);
    }
  sleep(2);
}

int main(int argc, char *argv[]) 
{

  Turnsignal tsig(9);  		// max verbosity
  TurnType status;
  int NUMCYCLES = 10;

  printf("opening turn signal ... \n");
  if(tsig.ts_open()<0)
    return(ERROR("Could not open port to SmartRelay (Turnsignals)! Aborting."));
  
  printf("initializing turn signal to home\n");
  if(tsig.init()<0)
    return(ERROR("Could not home device. Abort."));

  printf("waiting 3 seconds\n");
  sleep(3);

  ///////////////////////////
  for(int j=0; j<NUMCYCLES; j++)
    {
      // Signal left
      printf("going to signal right\n");
      if(tsig.setpos(RIGHT)<0)
	return(ERROR("Could not signal right. Abort."));
      
      queryStatus(&tsig, &status);
      
      // Signal home
      printf("going to signal home\n");
      if(tsig.setpos(HOME)<0)
	return(ERROR("Could not signal left. Abort."));
      
      queryStatus(&tsig, &status);
      
      // Signal right
      printf("going to signal left\n");
      if(tsig.setpos(LEFT)<0)
	return(ERROR("Could not signal left. Abort."));
      
      queryStatus(&tsig, &status);
      
      // Signal hazard
      printf("going to signal hazard\n");
      if(tsig.setpos(HAZARD)<0)
	return(ERROR("Could not signal hazard. Abort."));
      
      queryStatus(&tsig, &status);
    }
  //////////////////////////

  printf("waiting 3 seconds then shutdown\n");
  sleep(3);

  if(tsig.ts_close()<0)
    return(ERROR("exited uncleanly!!"));

  return 0; 
}
