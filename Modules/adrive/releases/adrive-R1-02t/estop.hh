/*!
 * \file estop.hh
 * \brief definitions for low level estop interface
 * 
 * \author Tully Foote or Jeff Lamb
 * \date 2004-05
 *
 * \ingroup serial
 * 
 */

#include "interfaces/ActuatorState.h"	// for estop status codes

#define ESTOP_INPUT_LENGTH 1
#define ESTOP_BIG_INPUT_LENGTH 300

#define RUN_CHAR 'R'
#define PAUSE_CHAR 'P'
#define DISABLE_CHAR 'D'

// Use the information from ActuatorState.hh for estop status internally
#define RUN	EstopRun
#define EPAUSE  EstopPause
#define DISABLE EstopDisable

int estop_open(int);
void simulator_estop_open(int);
int estop_close(void);
int estop_status(void);

//Chose Serial or SDSD
#define ESTOP_SERIAL

#ifndef ESTOP_SERIAL
#define ESTOP_SDS
#endif //not SERIAL
