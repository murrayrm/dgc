/*!
 * \file steercal.cc
 * \brief Calibrate steering
 * 
 * \author Richard Murray
 * \date 27 May 2007
 *
 * \ingroup utility
 *
 * This short program runs the steering calibration routine on Alice.
 *
 */

#include <stdio.h>
#include <iostream>
#include <assert.h>
#include "parker_steer.hh"
#include "steercal_cmdline.h"

int main(int argc, char **argv)
{
  int port = 0;
  int status;

  /* Process command line arguments */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);

  /* Process command line arguments */
  port = cmdline.port_arg;	// Store the port number to use
  if (cmdline.all_flag) {	// Take all possible actions
    cmdline.calibrate_flag = 1;
    cmdline.load_fault_handler_flag = 1;
    cmdline.status_flag = 1;
  }

  printf("Opening steering on port %d\n", port);
  status = steer_open(port);
  if (status == 0) {
    /* Couldn't open steering, abort unless told otherwise */
    printf("Error: couldn't open steering port; continue (y/n)? ");
    if (getchar() != 'y') exit(1);
  } else if (status < 0) {
    /* Opened port, but couldn't set parameters; try to continue */
    printf("Warning: steer_open returned %d\n", status);
  }

  /* Initialize the steering system */
  if (cmdline.noinit_flag) {
    printf("Skipping initialization\n");
    
  } else {
    printf("Initializing steering\n");
    status = steer_init();
    if (!status) printf("Warning: steer_init returned %d\n", status);
  }

  /* 
   * Decide what to do based on command line arguments 
   *
   * For backward compatibility, we keep track of whether we have
   * applied any actions and if not we calibrate the steering.
   */
  int count = 0;
  
  /* Load fault handling program */
  if (cmdline.load_fault_handler_flag) {
    printf("Loading fault handling program\n");
    status = steer_upload_file(cmdline.fault_handler_file_arg);
    if (!status) printf("Warning: steer_upload_file returned %d\n", status);
    ++count;
  }

  /* Set gains */
  if (cmdline.set_gains_flag) {
    printf("Warning: --set-gains not yet implemented\n");
    ++count;
  }

  /* Steering calibration - reset the center position to zero */
  if (cmdline.calibrate_flag) {
    printf("Calibrating steering\n");
    status = steer_calibrate();
    if (!status) printf("Warning: steer_calibrate returned %d\n", status);
    ++count;
  }

  /* Steering status - return the actuator status */
  if (cmdline.status_flag) {
    long int motor_status;
    if ((status = steer_getstatus("TASX", &motor_status)) < 0)
      printf("Warning: steer_getstatus returned %d\n", status);
    else
      printf("TASX = %lx\n", motor_status);
    ++count;
  }

  /* Make sure that we did some sort of action; if not, then calibrate */
  if (count == 0) {
    printf("No action specified; calibrating steering");
    status = steer_calibrate();
    if (!status) printf("Warning: steer_calibrate returned %d\n", status);
  }

  printf("Closing steering\n");
  status = steer_close();
  if (!status) printf("Warning: steer_close returned %d\n", status);

  return 0;
}
