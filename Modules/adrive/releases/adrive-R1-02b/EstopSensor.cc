/*!
 * \file EstopSensor.cc
 * \brief Read estop hardware
 *
 * \author Richard Murray
 * \date 22 April 2007
 *
 * \ingroup gcsensor
 *
 * This file contains the routines required to read and update the
 * estop status.  This is not a gcmodule, just a thread that keeps
 * track of the output from the estop unit (roughly the status loop
 * part of an actuator module).
 *
 */

#include "EstopSensor.hh"
#include "ActuationInterface.hh"	// for sleep times
#include "estop.hh"
#include "plctools/gcplc.h"

/*
 * EstopSensor::EstopSensor
 *
 * This constructor initializes the EstopSensor class, including
 * establishing connection to the hardware.
 *
 */

EstopSensor::EstopSensor(ActuationInterface *actuationInterface)
{
  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /* Initialize estop status to pause */
  currentStatus = EstopPause;

  /* Use PLC? */
  use_plc = (adrive->options->use_plc_flag || (adrive->options->partial_plc_flag &&
                                               (adrive->options->estop_port_arg < 0)));

  /*
   * Hardware/simulation initailization
   *
   * All we have to do here is open up the serial port, since the
   * estop box continuously sends out commands.  If we are running in
   * simulation mode, we don't bother trying to talk to the hardware.
   */

  if (adrive->options->disable_estop_flag) {
    /* Estop has been disabled, so allow the error to pass */
    connected = 0;
    initialized = 1;

    cerr << "Estop disabled; setting to mode to run" << endl;
    currentStatus = EstopRun;

  } else if (adrive->options->simulate_given) {
    /* Simulation mode - no action required */
    initialized = 1;
    connected = 1;
    
  } else if (use_plc) {
    /* We've already got all the init we need; gcplc_ functions fetch estop state automatically. */
    initialized = 1;
    connected = 1;

  } else {
    /* Open the port to the estop box */
    if (!estop_open(adrive->options->estop_port_arg)) {
      /* Don't allow operation without being able to talk to estop */
      cerr << "EstopSensor: estop_open failed" << endl;
      exit(1);

    } else {
      /* Keep track of the fact that we are connected */
      connected = 1;
      initialized = 1;
    }
  }

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&sensorMutex);

  /*
   * Status thread startup
   *
   * As part of the EstopSensor function, we start up a thread to read
   * the status of the estop box on a periodic basis.  This thread
   * fills in data in the EstopSensor class, so we have easy access to
   * this information.
   */

  /* Start up the thread */
  statusCount = 0;
  statusSleepTime = STATUS_SLEEP;
  DGCstartMemberFunctionThread(this, &EstopSensor::status);
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void EstopSensor::initSparrow(DD_IDENT *tbl, int label_id)
{
  /* Save the label for later usage */
  sparrowLabel = label_id;
  sparrowTable = tbl;

  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the estop box */
      dd_setcolor_tbl(label_id, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to estop box, but couldn't initialize */
      dd_setcolor_tbl(label_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(label_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("estop.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("estop.dstoppos", &currentStatus, tbl);
  }
}

/*
 * EstopSensor::status
 *
 * This function is called as a thread and is used to read the status
 * of the estop box.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void EstopSensor::status()
{
  while (1) {
    if (adrive->options->simulate_given) {
      /* In simulation mode, read state from asim */
      if (adrive->asim_status) 
	currentStatus = adrive->asim_state.m_dstoppos;
      
    } else if (use_plc) {
      int stat = gcplc_read_estop();
      if (stat == GCPLC_ESTOP_DISABLE)
        currentStatus = EstopDisable;
      else if (stat == GCPLC_ESTOP_PAUSE)
        currentStatus = EstopPause;
      else if (stat == GCPLC_ESTOP_RUN)
        currentStatus = EstopRun;
      else {
        cerr << "EstopSensor: invalid estop position return from PLC" << endl;
        currentStatus = EstopPause;
      }

    } else {
      if (connected) {
	/* Read the current status */
	pthread_mutex_lock(&sensorMutex);
	int retval = estop_status();
	pthread_mutex_unlock(&sensorMutex);

	/* Check the status and set state appropriately */
	if (retval == -1) {
	  /* Error condition */
	  cerr << "EstopSensor::error reading estop position"
	       << endl;
	  currentStatus = EstopPause;

	} else {
	  currentStatus = retval;
	}
      }
    }

    /* Now fill up the actuatorState struct with current state */
    adrive->state.m_estopstatus = initialized && connected;
    adrive->state.m_dstoppos = currentStatus;
    DGCgettime(adrive->state.m_estop_update_time);

    ++statusCount;

    /* 
     * Don't do a sleep at this point.  For some reason this screws up
     * the ability to read the estop unit (not sure why).  Since the
     * estop port is blocking, it is OK to just read as fast as we
     * can.
     */
    if (!connected || adrive->options->simulate_given) { 
      DGCusleep(statusSleepTime); 
    }
  }
}

/* Local Variables: */
/* c-basic-offset: 2 */
/* End: */
