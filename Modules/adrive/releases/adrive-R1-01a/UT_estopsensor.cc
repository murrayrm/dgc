/*!
 * \file UT_estopsensor.cc
 * \brief Unit test for the estop interface
 *
 * \author Richard M. Murray
 * \date 28 April 2007
 *
 * This is a simple unit test that initializes the estop interface and
 * then reads from it looking for changes.  This version uses the
 * EstopSensor interface that gcdrive uses.  If you want to talk
 * directly to the hardware, use UT_estop.
 */

#include <stdio.h>
#include <iostream>
#include <assert.h>
using namespace std;

#include "ActuationInterface.hh"
#include "EstopSensor.hh"
#include "cmdline.h"

int main (int argc, char **argv)
{
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0)
    exit(1);
  int skynet_key = skynet_findkey(argc, argv);

  /* Create the actuation interface */
  ActuationInterface *adriveInterface = 
    new ActuationInterface(skynet_key, &cmdline);

  /* Open up the estop interface */
  EstopSensor *estop = new EstopSensor(adriveInterface);

  /* Get the initial status */
  int current_status = estop->getStatus();
  assert(current_status != -1);
  cout << "initial status = " << current_status << endl;

  int count = 0;
  while (1) {
    int status = estop->getStatus();
    assert(status != -1);

    if (status != current_status) {
      /* changed state */
      cout << "changed to status " << status << endl;
      current_status = status;
      count = 0;
    } else if (count++ > 100) {
      cout << "status " << status << " unchanged" << endl;
      count = 0;
    }

    usleep(10000);
  }
}


