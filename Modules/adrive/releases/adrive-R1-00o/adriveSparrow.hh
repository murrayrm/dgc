/* Declare sparrow callbacks as standard C functions */
extern "C" {
  extern int adrive_setSteering_cb(long arg);
  extern int adrive_setBrake_cb(long arg);
  extern int adrive_setThrottle_cb(long arg);
  extern int adrive_setTrans_cb(long arg);
  extern void adrive_initSparrow(int);
}
