/**
 * \file obdii.h
 * \brief Header file for the OBD-II driver
 *
 * \author Joshua Oreman
 * \date 20 July 2007
 *
 * This file contains the declarations for functions that
 * access the OBD-II diagnostic interface to Alice.
 */

#ifndef __OBDII_H__
#define __OBDII_H__

#ifdef __cplusplus
extern "C" {
#endif

/** The IP address of the DM500. */
#define DM500_IP "192.168.0.72"
/** The port to connect to (usually 8000 + index of serial port) */
#define DM500_PORT 8001

/**
 * \brief Sends an AT (configuration) command to the serial-to-OBD-II interface.
 *
 * The complete list of AT commands can be found in the ELM327 datasheet. A few of the
 * most useful:
 * - \c ATZ restarts the device (takes about a second)
 * - \c ATWS performs a "warm start" (like ATZ but instantaneous)
 * - \c ATH1 turns header reporting on, \c ATH0 turns it off (default).
 * - \c ATE1 turns echo on (default), \c ATE0 turns it off.
 * obdii_init() automatically sends <tt>ATWS ATE0 ATH1</tt>.
 *
 * <b>Do not include the <tt>AT</tt> characters in the command you pass to this function.</b>
 *
 * \param cmd The command to send; something like \c E0 or \c WS. Space and newline are ignored.
 *            The actual text sent to the PLC will be the characters \c AT, followed by the
 *            value of \a cmd, followed by a carriage-return character. You may \e not specify
 *            more than one command at once. You may \e not specify the \c AT characters yourself.
 * \return AT_OK if the device replied with "OK", AT_DONE for any other reply, 0 if no prompt showed up
 *         within a second or EOF was read, -1 on any other read error.
 */
int obdii_atcmd (const char *cmd);

#define AT_OK 2                 /**< Return value from obdii_atcmd() indicating an "OK" reply. */
#define AT_DONE 1               /**< Return value from obdii_atcmd() indicating a successful non-"OK" reply. */

/** Source argument for obdii_command() indicating the first ECU. */
#define SOURCE_ECU1  0x7E8
/** Source argument for obdii_command() indicating the second ECU. */
#define SOURCE_ECU2  0x7E9
/** Source argument for obdii_command() indicating the transmission control unit (?). */
#define SOURCE_TRANS 0x7ED

/**
 * \brief Sends the OBD-II command in the \a cmd buffer, waits up to one second for the response,
 * and stores it in \a resp.
 *
 * OBD-II commands consist of between one and seven bytes. The first
 * byte is a \e mode: 0x01 for "show current data", 09 for
 * "request vehicle information", etc. The second byte is usually
 * a \e PID (parameter identification) number that says what's being requested.
 * Anything else depends on the command.
 *
 * OBD-II commands usually solicit multiple replies, at least in Alice;
 * there are at least three controllers on the CAN bus listening for queries,
 * and they all reply independently. As such, you must specify (using the \a source
 * parameter) which one you want to listen to: SOURCE_ECU1, SOURCE_ECU2, or SOURCE_TRANS.
 *
 * Command replies may be split across multiple frames, and the frames may come in random
 * order, but this is all handled internally. You will see a reassembled single stream of
 * bytes.
 *
 * The response will be stripped of any header bytes, \e including the first mode "echo"
 * byte (but not the PID "echo" byte).
 *
 * \param cmd The command to write.
 * \param cmdlen The length in bytes of the \a cmd buffer; must be between 1 and 7, inclusive.
 * \param resp The buffer in which to store the response.
 * \param resplen The length in bytes of \a resp. Longer responses will be silently truncated.
 * \param source The eleven-bit CAN ID of the controller from which replies will be processed.
 *               Any other replies will be ignored. This should be one of the SOURCE_* constants
 *               unless you \e really know what you're doing.
 * \return The number of bytes in the response. On timeout, EOF, or I/O error, returns -1.
 *         A zero-length response is perfectly valid.
 */
int obdii_command (const void *cmd, int cmdlen, void *resp, int resplen, int source);

/**
 * \brief Shortcut for sending a request in mode 1 ("show current data").
 *
 * Mode 1 replies have one superfluous byte prepended to them: the PID echo byte, which just
 * repeats the value of \a pid. So if you're expecting a two-byte reply, provide a three-byte
 * buffer and access buf[1] and buf[2].
 *
 * \param pid The PID of the request to send, e.g. 0x05 for "get engine coolant temperature", etc.
 * \param resp Where to store the response.
 * \param resplen The length of \a resp.
 * \param source The ECU unit to listen for a reply from.
 * \return Number of bytes of response read, or -1 on error.
 */
int obdii_cmd1 (unsigned char pid, void *resp, int resplen, int source);

/**
 * \brief Shortcut for sending a request in mode 0x22 (undocumented, but seems to provide access
 * to Ford-specific values).
 *
 * Mode 0x22 requests are two bytes long, not counting the mode byte, and their replies have
 * two echo bytes prepended to them. So if you're expecting two bytes of worthwhile response,
 * provide a 4-byte buffer and access buf[2] and buf[3].
 *
 * \param first The first byte to send.
 * \param second The second byte to send.
 * \param resp The buffer in which to store the reply.
 * \param resplen Length of \a resp.
 * \param source The ECU unit to listen for a reply from.
 * \return The number of bytes of reply read, or -1 on error.
 */
int obdii_cmd2 (unsigned char first, unsigned char second, void *resp, int resplen, int source);

/**
 * \brief Opens the connection to the OBD-II bridge.
 *
 * This does \e not try to actually perform an OBD-II command. It will succeed even
 * if the vehicle is off, as long as the Ethernet-to-serial bridge is working and
 * the ELM has power.
 *
 * \param vflag The verbosity to use; 3 or above prints a few status messages, 4 or above prints all OBD-II data going over the wire.
 *
 * \return -1 on error, 0 on success.
 */
int obdii_open(int vflag);

/**
 * \brief Initializes the ELM converter and checks operation of the OBD-II bus.
 *
 * This sends the AT commands \c ATWS, \c ATE0, and \c ATH1 (in that order).
 * Then it uses mode 01 PID 1C (check OBD standard) to verify that the vehicle is
 * on.
 *
 * \return 0 if the initialization succeded, -1 if it failed.
 */
int obdii_init();

/** Returns engine RPM, 0.0 on OBD-II failure. */
double obdii_get_rpm();

/** Returns time since engine was started, in seconds; -1 on OBD-II failure. */
int obdii_get_time_since_engine_start();

/** Returns current vehicle speed in meters per second; 0.0 on OBD-II failure. */
double obdii_get_vehicle_speed();

/** Returns current engine coolant temperature in degrees Farenheit; 0.0 on OBD-II failure. */
double obdii_get_engine_coolant_temp();

/** Returns current angle of depression of the gas pedal, in degrees; -1.0 on OBD-II failure. */
double obdii_get_throttle_position();

/** Returns current torque, in newton-meters. I'm not entirely sure what this actually measures. -1 on
    OBD-II failure. */
int obdii_get_torque();

/** Returns the amount of time the glow plug lamp has been on; -1 on OBD-II failure. */
int obdii_get_glow_plug_lamp_time();

/** Returns the current gear ratio; 0.0 on OBD-II failure. */
double obdii_get_current_gear_ratio();

/** Returns the voltage of the vehicle battery; 0.0 on OBD-II failure. */
double obdii_get_battery_voltage();

/** Returns 1 if the glow plug light is on, 0 if it's off, -1 on OBD-II failure. */
int obdii_glow_plug_light_on();

/** Returns the current gear, as an integer. -1 on OBD-II failure. */
int obdii_get_current_gear();

/** Returns the transmission position, as a character: 'P'(ark), 'R'(everse), 'N'(eutral), 'D'(rive),
    or 'X' for none of the above. OBD-II error yields '!'. */
char obdii_get_transmission_position();

/** Get the torque converter control state, which has unknown format. -1 on OBD-II error. */
int obdii_get_torque_converter_control_state();

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* __OBDII_H__ */
