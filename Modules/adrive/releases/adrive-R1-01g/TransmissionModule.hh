/*!
 * \file TransmissionModule.hh
 * \brief Header file for TransmissionModule class
 *
 * \author Nok Wongpiromsarn and Richard Murray
 * \date April 2007
 *
 * This file defines the TransmissionModule class for adrive.  This is
 * used to communicate with the transmission actuator.  Most of this is
 * just a pass through for the directives and responses that are
 * routed through adrive.
 */

#ifndef __TransmissionModule_hh__
#define __TransmissionModule_hh__

#include <pthread.h>
#include <string>
#include "gcmodule/GcModule.hh"
#include "sparrow/display.h"

#include "ActuationInterface.hh"

const int TRANSMISSION_SHIFT_SPEED = 1.0;	// max speed for shifting

class TransControlStatus : public ControlStatus
{
public:
  unsigned id;			// ID for the TransDirective
  AdriveFailure reason;

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " parent_id: " << parent_id << " status: "
      << status << " reason: " << reason;
    return s.str();
  }
};

/*
 * \class TransMergedDirective
 * 
 * This is the directive sent between Transmission Arbitration and
 * Transmission Control.
 */

class TransMergedDirective : public MergedDirective
{
public:
  unsigned id;			///! Identifier for this directive
  AdriveCommandType command;	///! Command to execute
  int arg;			///! Value for the command 

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " command: " << command << " arg: " << arg;
    return s.str();
  }
};

/*
 * \class TransmissionModule
 *
 * This class is the actual GcModule for transmission. 
 */

class TransmissionModule : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  TransControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  TransMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  AdriveTransInterface::Northface* adriveTransInterfaceNF;

  /*!\param Actuation interface */
  ActuationInterface *adrive;

  /*! Arbitration for the transmission control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus *, MergedDirective *);
  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*! Control for the transmissioncontrol module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus *, MergedDirective *);
  int commandGear;
  int controlCount;

  /*! Status for the transmission control module.  This function is called
      as a thread and updates the current status of the transmission
      subsystem */
  void status();
  int statusCount;
  long statusSleepTime;

  /* Keep track of whether we print information messages or not */
  int verbose;

  /*! Mutex controlling access to serial port and data */
  pthread_mutex_t actuatorMutex;

public:
  /* Status information (public) */
  int initialized;		/*!< Transmission is properly initialized */
  int connected;		/*!< Able to talk to the motor controller */
  int currentGear;
  unsigned long long update_time;

  /*! Constructor */
  TransmissionModule(int, ActuationInterface *, int verbose = 0);
  
  void initSparrow(DD_IDENT *, int);
};

#endif
