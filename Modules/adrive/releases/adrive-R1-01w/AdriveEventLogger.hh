#ifndef __ATIMBERBOX_HH__
#define __ATIMBERBOX_HH__

#include <string>
#include <fstream>
using namespace std;

class AdriveEventLogger {
public:
  AdriveEventLogger(char * pathname);
  ~AdriveEventLogger();

  int operator<<(string input_string);
  int enable(void);
  int disable(void);

private:
  unsigned long long current_time;
  fstream logfile;
  int event_enabled;  
};

extern AdriveEventLogger event;
extern AdriveEventLogger estop_log;
#endif //__ATIMBERBOX_HH__
