Sat Apr 21 18:09:58 2007	Richard Murray (murray)

	* version R1-00j
	BUGS: 
	FILES: SteeringModule.cc(20185), SteeringModule.hh(20185)
	fixed small bug in steering status + changed 'heading' to 'angle'

	FILES: UT_adrive.cc(20184)
	cut down steering command for testing

2007-04-21  Richard Murray  <murray@kona.local>

	* SteeringModule.cc (SteeringModule::status): status now fills in
	fields in actuator state if running on hardware, otherwise reads in
	the fields from asim

	* gcdrive.cc (main): updated sparrow initialization so that this
	happens only after all actuators have properly initialized

	* ActuationInterface.hh (ActuationInterface): added status fields
	* ActuationInterface.cc (ActuationInterface::status): Added new
	function to read/write actuator state (depeding on simulation flag)

	* ActuatationInterface.cc (ActuationInterface::initSparrow): added
	sparrow display information for ActuationState (incomplete)
	* overview.dd: updated display to include actuation state

>>>>>>> .merge-right.r20197
Sat Apr 21 11:28:08 2007	Richard Murray (murray)

	* version R1-00i
	BUGS: 
	FILES: ActuationInterface.cc(20138), OBDIIDriver.cc(20138),
		OBDIIDriver.hh(20138), actuators.cc(20138),
		adriveMain.cc(20138), adriveThreads.cc(20138),
		estop.hh(20138), gcdrive.cc(20138)
	documentation fixes

	FILES: ActuationInterface.hh(20136), AdriveCommand.hh(20136),
		Makefile.yam(20136), SteeringModule.cc(20136),
		SteeringModule.hh(20136), UT_adrive.cc(20136),
		adriveSparrow.cc(20136), estop.hh(20136), gcdrive.cc(20136)
	updated to work with latest release of gcmodule + fixed Makefile
	bugs

2007-04-21  Richard Murray  <murray@kona.local>

	* OBDIIDriver.hh, OBDIIDriver.cc: added debug flag to constructor
	(default = 0) 

	* gcdrive.cc: fixed doxygen filename
	* adriveThreads.cc: fixed doxygen filename
	* adriveMain.cc: fixed doxygen filename
	* actuators.cc: fixed doxygen filename
	* ActuationInterface.cc: fixed doxygen filename
	* estop.hh: fixed minor doxygen error

	* UT_adrive.cc (main): removed DEBUG_LEVEL
	* gcdrive.cc (main): removed DEBUG_LEVEL

	* estop.hh: moved definitions of PAUSE, RUN, DISABLE to
	interfaces/ActuatorCommand.h.  These are now available globall as
	EstopPause, EstopRun, EstopDisable.

	* SteeringModule.hh (class SteerControlStatus): removed status
	member variable (now defined in GcInterface)
	* AdriveCommand.hh (struct AdriveResponse): removed custom status
	type from AdriveResponse; using default Interface status type
	* UT_adrive.cc (main): updated status type
	* adriveSparrow.cc (adrive_setSteering_cb): updated status type

	* ActuationInterface.hh: updated AdriveSteerInterface to use
	GCM_IN_PROCESS_DIRECTIVE 

	* Makefile.yam (CDD): adrive.o -> adriveThreads.o to fix problem
	with .dd files

Wed Apr 18 20:58:23 2007	murray (murray)

	* version R1-00h
	BUGS:  
	New files: AccelerationControl.cc AccelerationControl.hh
		ActuationInterface.cc ActuationInterface.hh
		AdriveCommand.hh SteeringModule.cc SteeringModule.hh
		TransmissionControl.cc TransmissionControl.hh UT_adrive.cc
		adriveSparrow.cc adriveSparrow.hh adriveThreads.cc
		adriveThreads.hh gcdrive.cc overview.dd
	Deleted files: Makefile adrive.cc adrive.hh
	FILES: Makefile.yam(19221)
	Added classes that derive from GcModule.

	FILES: Makefile.yam(19606), actuators.cc(19606),
		adriveMain.cc(19606), sninterface.cc(19606)
	renamed adrive.{cc,hh} to adriveThreads + created gcdrive variant

	FILES: Makefile.yam(19625)
	first cut at new gcdrive module

	FILES: Makefile.yam(19686)
	first cut at sending directives from test program to adrive

	FILES: Makefile.yam(19774), adrive.ggo(19774)
	added --simulate flag to send commands to asim

	FILES: Makefile.yam(19811), adrive.ggo(19811)
	added ability to set steering from sparrow

	FILES: Makefile.yam(19868)
	added Reset directive (not used yet) plus some code renaming

	FILES: actuators.cc(19815), parker_steer.cc(19815),
		parker_steer.hh(19815)
	removed extraneous messages + split steer_open/init

2007-04-17  Richard Murray  <murray@kona.local>

	* actuators.cc (execute_steer_init): removed similation capability

	* parker_steer.cc (steer_open): Split into steer_open and
	steer_init, for use with gcdrive

2007-04-14  Richard Murray  <murray@kona.local>

	* Makefile.yam: added gcdrive program
	* gcdrive.cc: new program for adrive using CSA

	* adrive.cc: renamed to adriveThreads.cc
	* adrive.hh: renamed to adriveThreads.hh
	* Makefile.yam: renamed adrive.cc to adriveThreads.cc
	* sninterface.cc: renamed adrive.hh to adriveThreads.hh
	* actuators.cc: renamed adrive.hh to adriveThreads.hh
	* adriveMain.cc: renamed adrive.hh to adriveThreads.hh
	* adriveThreads.cc: renamed adrive.hh to adriveThreads.hh

Sat Mar 17 19:46:41 2007	murray (murray)

	* version R1-00g
	BUGS: 
	New files: ELMscan.cc
	FILES: AdriveEventLogger.cc(18369), adrive.cc(18369)
	improved error logging

	FILES: actuators.cc(18368)
	gas command checks for park only if transition is enabled

2007-03-17  murray  <murray@leela.dgc.caltech.edu>

	* adrive.cc: updated error messages so that they aren't sent quite
	as often

	* AdriveEventLogger.cc: send all events to cerr for logging by
	sparrow.  Removed some cerr outputs that were redundant

	* actuators.cc (execute_gas_command): check for T_PARK only if
	tranmission status is enabled.

Sat Mar  3  9:29:28 2007	murray (murray)

	* version R1-00f
	BUGS: 3151
	FILES: adrive.ggo(16271)
	updated to use --skynet-key (coding standard)

	FILES: sninterface.cc(16270)
	updated to use interfaces/sn_types.h.  Got rid of a few warning
	messages. 

Tue Feb 20 15:45:25 2007	Nok Wongpiromsarn (nok)

	* version R1-00e
	BUGS: 
	FILES: sninterface.cc(15168)
	Modified adrive so it compiles against revision R2-00a of the
	interfaces and skynet modules

Sat Feb 10 19:39:06 2007	murray (murray)

	* version R1-00d
	BUGS: 
	FILES: adrive.cc(14940)
	added prints to stderr on adrive pause

	FILES: adrive.config(14939)
	turned of OBDII by default since it is currently broken

	FILES: adriveMain.cc(14941)
	added call to dgcFindConfigFile

Sat Jan 20 14:22:24 2007	murray (murray)

	* version R1-00c
	BUGS: 3079
	FILES: Makefile.yam(12858)
	fixed bad link of adrive to main bin directory

	FILES: adrive.cc(12859)
	added config file check

2007-01-20  murray  <murray@leela.dgc.caltech.edu>

	* Makefile.yam (PROJ_BINS): commented out to keep from installing
	bad link in main bin directory

	* adrive.cc (read_config_file): added check to make sure config
	file exists

Sat Jan 13 20:49:29 2007	murray (murray)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(12757), adrive.cc(12757)
	small cleanups

	FILES: sninterface.cc(12760)
	updated to include interfaces/Actuator\*.h

2007-01-13  murray  <murray@gclab.dgc.caltech.edu>

	* adrive.cc: fixed filename in header

Wed Jan 10 23:11:57 2007	murray (murray)

	* version R1-00a
	BUGS: 
	New files: AdriveEventLogger.cc AdriveEventLogger.hh Makefile
		OBDIIDriver.cc OBDIIDriver.hh actuators.cc actuators.hh
		addisp.dd adrive.cc adrive.config adrive.ggo adrive.hh
		adriveMain.cc brake.cc brake.hh estop.cc estop.hh
		igntrans.cc igntrans.hh parker_steer.cc parker_steer.hh
		sninterface.cc sninterface.hh summary.dd throttle.cc
		throttle.hh
	FILES: Makefile.yam(12693)
	set up working version in YaM; updated to coding standards

Wed Jan 10 17:32:47 2007	murray (murray)

	* version R1-00
	Created adrive module.

2007-01-03  murray  <murray@gclab.dgc.caltech.edu>

	* .:Partial revert of changes: got rid of astate integration.
	* Makefile: removed astate dependendies
	* adriveMain.c (main): ifdef'd out astate dependendies (ASTATE)

	* summary.dd: copied from adrive-rmm branch (history lost)
	* adrive.ggo: copied from adrive-rmm branch (history lost)
	
2006-12-31  Richard Murray  <murray@kona.local>

	* adriveMain.c (main): set colors for actuators based on whether
	threads are rnning or not.
	* adrive.c (set_actuator_label_color): new function to set color
	* adrive.h: created IDs for trans and estop actuators

2006-12-31  Richard Murray  <murray@cds.caltech.edu>

	* adrive.c (adrive_reset, adrive_manual, adrive_autonomous): Created
	functions for setting driving state.
	* summary.dd: Added buttons for changing modes

2006-12-31  Richard Murray  <murray@kona.local>

	* ChangeLog: created ChangeLog file.  Should have done this a long
	time ago; will attempt to reconstruct changes over the last week
	from svn logs (note that YaM can do this automatically).

2006-12-30  Richard Murray  <murray@kona.local>

	* adrive.c (sparrow_main): added errlog buffering
	* actuators.c, OBDII.c, adrive.c, adriveMain,c, brake.c, estop.c,
	igntrans.c, parker_steer.cc, throttle.c: changed cout to cerr and
	stdout to stderr to allow errors to be handled properly by sparrow.

2006-12-30  Richard Murray  <murray@kona.local>

	* adriveMain.c (main): Added ability to run astate from adrive (beta
	test; off by default) 

	* adriveMain.c (main): Made summary display the default when you
	start adrive 

2006-12-29  Richard Murray  <murray@kona.local>

	* summary.dd: Updated display to show the relevant variables (+ ints
	for gears)

	* Makefile: Added obd test program commands to Makefile

	* actuators.c (executed_trans_command): Fixed bug in transmission
	logic that wasn't reading OBD status properly 
	* acutators.c: Added debugging info for transmission actuation

2006-12-28  Richard Murray  <murray@kona.local>

	* .: Moved drivers/alice to modules/adrive for compactness
	* adriveMain.c (main): Added command line options to adrive
	* .: Renamed OBDIIdriver.{cpp,hpp}} to OBDII.{cc,hh}












