/*
 * overview.dd - overview display for adrive
 *
 * Richard M. Murray
 * 15 April 2007
 *
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/* Callbacks */
extern int adrive_setSteering_cb(long);
extern int adrive_resetSteering_cb(long);
extern int adrive_setBrake_cb(long);
extern int adrive_setThrottle_cb(long);
extern int adrive_resetAcceleration_cb(long);
extern int adrive_setTrans_cb(long);
extern int adrive_setTurn_cb(long);

#ifdef __cplusplus
}
#endif

%%
01  Mode: %mode             Gcdrive Summary Display        SNKEY: %snkey
02
03  Actuator      Cmd   Stat  Command  Current  St  Err          Actions
04  %STEER     %st_cc  %st_sc %st_cmd  %st_pos  %a%ster %st_res  %RESET
05  %GAS       %ga_cc  %ga_sc %ga_cmd  %ga_pos  %c%gaer %ga_res  %MAN
06  %BRAKE     %br_cc  %br_sc %br_cmd  %br_pos  %e%brer %br_res  %AUTO
07  %TRANS     %tr_cc  %tr_sc %tr_cmd  %tr_pos  %g%trer %tr_res  %PAUSE
08  %TURN      %tu_cc  %tu_sc %tu_cmd  %tu_pos  
09
10  %ESTOP %es_sc     | %OBD   %ob_sc   %ob_fc           | %ActuatorState%as_sc
11  DARPA:    %d_stop | RPM:   %ob_rpm  Wheel: %ob_spd	 |	 
12  Estop:    %e_stop | Temp:  %ob_tmp  Gear:  %ob_gear  |
13
14  ActState  %a_state Failure codes
15    0 = Disabled       0 = not initialized
16    1 = Paused         1 = out of range
17    2 = Running        2 = vehicle paused
18    3 = Resuming       3 = communication error
19    4 = Shifting       4 = initialization error
20
21

%QUIT  %ADRIVE  %ASTATE
%%

# Menu buttons
button: %QUIT "QUIT" dd_exit_loop;

# Steering actuator
button: %STEER "Steering" adrive_resetSteering_cb -idname=STEER_LABEL;
double: %st_cmd @steer.command "%6.4f" -callback=adrive_setSteering_cb;
double: %st_pos @steer.position "%6.4f" -ro;
int:	%st_sc	@steer.status_loop_counter "%5d" -ro;
int:	%st_cc	@steer.command_loop_counter "%6d" -ro;
int:	%ster	@steer.error "%5d" -ro;

button: %GAS "Gas" adrive_resetAcceleration_cb -idname=GAS_LABEL;
double: %ga_cmd @throttle.command "%6.4f" -callback=adrive_setThrottle_cb;
double: %ga_pos @throttle.position "%6.4f" -ro;
int:	%ga_sc	@throttle.status_loop_counter "%5d" -ro;
int:	%ga_cc	@throttle.command_loop_counter "%6d" -ro;
int:	%ster	@throttle.error "%5d" -ro;

button: %BRAKE "Brake" adrive_resetAcceleration_cb -idname=BRAKE_LABEL;
double: %br_cmd @brake.command "%6.4f" -callback=adrive_setBrake_cb;
double: %br_pos @brake.position "%6.4f" -ro;
double: %br_pos @brake.pressure "%6.4f" -ro;
int:	%br_sc	@brake.status_loop_counter "%5d" -ro;
int:	%br_cc	@brake.command_loop_counter "%6d" -ro;
int:	%ster	@brake.error "%5d" -ro;

label: %TRANS	  "Trans"     -idname=TRANS_LABEL;
int: %tr_cmd @trans.command "% 6d" -callback=adrive_setTrans_cb;
int: %tr_pos @trans.position "% 6d" -ro;
int:	%tr_sc	@trans.status_loop_counter "%5d" -ro;
int:	%tr_cc	@trans.command_loop_counter "%6d" -ro;
int:	%ster	@trans.error "%5d" -ro;

label: %TURN	  "Turn Sig"     -idname=TURN_LABEL;
int: %tu_cmd @turn.command "% 6d" -callback=adrive_setTurn_cb;
int: %tu_pos @turn.position "% 6d" -ro;
int:	%tu_sc	@turn.status_loop_counter "%5d" -ro;
int:	%tu_cc	@turn.command_loop_counter "%6d" -ro;

# Actuator state broadcast
label: %ActuatorState "ActuatorState" -idname=ACTSTATE_LABEL -fg=RED;
int:   %as_sc @adrive.status_loop_counter "%5d" -ro;
int:   %a_state	@actuation_state "%2d" -ro;

label: %ESTOP	  "Estop"     -idname=ESTOP_LABEL;
int: %es_sc @estop.status_loop_counter "%5d" -ro;
int:   %d_stop	@estop.dstoppos "%2d";
int:   %e_stop	@estop.estoppos "%2d" -ro;

label: %OBD	  "OBD"       -idname=OBDII_LABEL;
int: %ob_sc @obdii.status_loop_counter "%5d" -ro;
int: %ob_fc @obdii.fake_loop_counter "%5d" -ro;
double: %ob_rpm	@obdii.engine_rpm "%5.0f" -ro;
double: %ob_spd	@obdii.wheel_speed "%5.2f" -ro;
double: %ob_tmp	@obdii.engine_temp "%5.2f" -ro;
double: %ob_gear @obdii.gear_ratio "%5.2f" -ro;

# Miscellaneous additional info
int: %snkey @skynet_key "%6d" -ro;

tblname: overview_tbl;
bufname: overview_buf;
