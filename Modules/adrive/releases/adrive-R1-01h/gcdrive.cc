/*!
 * \file gcdrive.cc
 * \brief Actuator interface program for Alice
 *
 * \author Richard Murray
 * \date 14 April 2007
 *
 * Gcdrive is the actuator interface program for Alice.  It is a
 * replacement for the old adrive program, rewritten to be compatible
 * with the canonical software architecture.
 *
 * Note: once gcdrive is up and running, the old adrive will be
 * removed and this program will replace it.
 *
 * \section Execution Overview
 *
 * Adrive consists of a collection of GcModules that are used to
 * manage the operation of the various actuators that are controlled
 * by the program.  The following GcModules are defined:
 *
 *  * ActuationInterface - receives commands via skynet; routes to actuators
 *  * <Actuator>Module - individual actuators: Steering, Acceleration, etc
 *
 * The ActuationInterface module is a shell module that just converts
 * the incoming actuation commands to directives for the individual
 * actuator modules.  
 */

#include <stdlib.h>
#include <iostream>
#include <sys/time.h>
#include <assert.h>
#include <sys/stat.h>
#include <signal.h>
#include "dgcutils/cfgfile.h"
#include "cmdline.h"

/* Sparrow display tables */
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/errlog.h"
#include "overview.h"			// sparrow display table
#include "adriveSparrow.hh"

/* GcModule definitions */
#include "ActuationInterface.hh"	// Actuation interface
#include "SteeringModule.hh"		// Steering control module
#include "AccelerationModule.hh"	// Acceleration control module
#include "TransmissionModule.hh"	// Transmission control module
#include "TurnSignalModule.hh"		// Turn signal control module
#include "EstopSensor.hh"		// Estop sensor module
#include "OBDIISensor.hh"		// OBD-II sensor module

volatile sig_atomic_t quit = 0;		// flag for determining when to quit
void sigintHandler(int);		// function to handle interrupts

int main(int argc, char** argv)
{
  // catch CTRL-C and SIGTERM and exit cleanly, if possible
  signal(SIGINT, sigintHandler);
  signal(SIGTERM, sigintHandler);

  int verbose = 0;

  /*
   * Read command line options and configuration file
   *
   * We use gengetopt to parse command line options, including the
   * possibility of reading arguments from a configuration file.  The
   * legacy adrive.config file is also detected, but not currently
   * read (may put that in later).
   * 
   */

  /* Process command line arguments */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0)
    exit(1);

  /*
   * Process command line options 
   *
   * Now that we have read the config file, process the command line
   * options that control whether certain threads should start up.
   *
   */

  /* Check to see if we want verbose messages */
  if (cmdline.verbose_given) verbose = cmdline.verbose_arg;
  if (verbose > 5) 
    cerr << "verbose messages on (level = " << verbose << ")" <<endl;

  /* Get log file name */
  if (!cmdline.log_file_given) {
    /* Check if the logs directory exists */
    struct stat st;
    int ret = stat("./logs", &st);
    if (ret < 0 && errno == ENOENT) {
      if (mkdir("./logs", 0755) != 0)
	cerr << __FILE__ << ":" << __LINE__ << "cannot create log dir.";
    }
    cmdline.log_file_arg = "./logs/gcdrive";
  }

  /* Skynet initialization */
  int skynet_key = skynet_findkey(argc, argv);
  dd_rebind_tbl("skynet_key", &skynet_key, overview_tbl);
  adrive_initSparrow(skynet_key);

  /*
   * Configure adrive operations
   *
   * At this point we should have initalized all of the information in
   * my_vehicle and cmdline structs and we can proceed to starting up
   * all required modules.  This is done by instantiating each
   * GcModule that we use and starting them up.
   *
   * The ordering of operations is to first instantiate all devices
   * and then start them running.  The constructors for each actuator
   * should fail if we can't talk to the actuator and the proper
   * command line flags are not set, so this insures that we only
   * start running things if everything is configured properly.
   */

  /* Create the main actuator interface */
  ActuationInterface *adriveInterface = 
    new ActuationInterface(skynet_key, &cmdline);

  /* Instantiate each of the actuators */
  if (verbose) cerr << "instantiating steering" << endl;
  SteeringModule *steering = new SteeringModule(skynet_key, adriveInterface);

  if (verbose) cerr << "instantiating acceleration" << endl;
  AccelerationModule *acceleration =
    new AccelerationModule(skynet_key, adriveInterface, verbose);

  if (verbose) cerr << "instantiating transmission" << endl;
  TransmissionModule *transmission = 
    new TransmissionModule(skynet_key, adriveInterface);

  if (verbose) cerr << "instantiating turn signals" << endl;
  TurnSignalModule *turnsignal = 
    new TurnSignalModule(skynet_key, adriveInterface);

  if (verbose) cerr << "instantiating estop" << endl;
  EstopSensor *estop = new EstopSensor(adriveInterface);

  OBDIISensor *obdii = NULL;
  if (!cmdline.disable_obd_flag) {
    if (verbose) cerr << "instantiating obdii" << endl;
    obdii = new OBDIISensor(adriveInterface, skynet_key);
  }

  /* Now start up all of the interfaces */
  if (verbose > 2) cerr << "starting up actuation, sensor interfaces" << endl;

  steering->Start();
  acceleration->Start();
  transmission->Start();
  turnsignal->Start();

  /* 
   * Perform initialization operations
   *
   * Now that all of the actuators are running, we can do some
   * initialization steps.  The two main options here are support for
   * auto-start and auto-shift, which start up the engine and make
   * sure we are in gear.  Both of these operations work by sending
   * directives to the indiviual actuators, bypassing the normal
   * ActuationInterface state machine (this is the reason that the
   * actuation interface is not started until later.
   *
   */

  /* Press the brake on start */
  adriveInterface->sendPauseCommand(Pause);

  /* Auto-start: start the engine if it is not running */
  if (cmdline.auto_start_flag) {
    cerr << "auto-start capability not yet implemented" << endl;
    exit(1);
  }

  /* Auto-shift: shift into gear if we are in park or neutral */
  if (cmdline.auto_shift_flag && adriveInterface->autoshift(verbose) < 0) {
    cerr << "auto-shift failed; aborting" << endl;
    exit(1);
  }

  /* Finally, start up the actuation interface */
  DGCstartMemberFunctionThread(adriveInterface, &ActuationInterface::arbitrate);

  /* Initialize sparrow and set up the display variables for each actuator */
  if (!cmdline.disable_console_given) {
    if (dd_open() < 0) { dbg_error("can't open display"); exit(-1); }
    if (dd_usetbl(overview_tbl) < 0) {
      dbg_error("can't open display table");
      exit(-1);
    }

    adriveInterface->initSparrow(overview_tbl, ACTSTATE_LABEL);
    if (steering) steering->initSparrow(overview_tbl, STEER_LABEL);
    if (acceleration) 
      acceleration->initSparrow(overview_tbl, BRAKE_LABEL, GAS_LABEL);
    if (transmission)
      transmission->initSparrow(overview_tbl, TRANS_LABEL);
    if (turnsignal) turnsignal->initSparrow(overview_tbl, TURN_LABEL);
    if (estop) estop->initSparrow(overview_tbl, ESTOP_LABEL);
    if (obdii) obdii->initSparrow(overview_tbl, OBDII_LABEL);

    /* Turn on error logging in sparrow */
    dd_errlog_init(50); dd_errlog_bindkey();

    /* Rebind keys */
    dd_bindkey(0x03, dd_exit_loop);	// exit on ^C

    /* Stay in sparrow loop until we are finished */
    dd_loop();
    dd_close();
  } else {
    /* Just wait until something causes us to exit */
    if (verbose) cerr << "running" << endl;
    while (!quit) { 
      sleep(1);
      if (cmdline.test_mode_given) break;
    }
  }

  /* All done */
  return 0;
}

/*
 * Function to handle signals - attempt to exit cleanly
 *
 */

void sigintHandler(int /*sig*/)
{
  // if the user presses CTRL-C three or more times, and the program still
  // doesn't terminate, abort
  if (quit > 2) {
    abort();
  }
  quit++;
}

