/*!
 * \file TurnSignalModule.hh
 * \brief Header file for TurnSignalModule class
 *
 * \author Richard Murray
 * \date 18 May 2007
 *
 * \ingroup gcmodule
 *
 * This file defines the TurnSignalModule class for adrive.  This is
 * used to communicate with the transmission actuator.  Most of this is
 * just a pass through for the directives and responses that are
 * routed through adrive.
 */

#ifndef __TurnSignalModule_hh__
#define __TurnSignalModule_hh__

#include <pthread.h>
#include <string>
#include "gcmodule/GcModule.hh"
#include "sparrow/display.h"

#include "ActuationInterface.hh"
#include "turnsignals.hh"

/* Constants used to control switching off the signals */
const double TURN_STEER_LIMIT = 0.4;	///! Outer limit for switch off
const double TURN_STEER_RESET = 0.2;	///! Inner limit for switch off

/*! Source for diretives */
enum TurnSource {
  ExternalSource, InternalSource
};

class TurnControlStatus : public ControlStatus
{
public:
  unsigned id;			// ID for the TurnDirective
  TurnSource source;
  AdriveFailure reason;

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " parent_id: " << parent_id << " status: "
      << status << " reason: " << reason;
    return s.str();
  }
};

/*
 * \class TurnMergedDirective
 * 
 * This is the directive sent between TurnSignal Arbitration and
 * TurnSignal Control.
 */

class TurnMergedDirective : public MergedDirective
{
public:
  unsigned id;			///! Identifier for this directive
  enum TurnSource source;	///! Source of the directive 
  AdriveCommandType command;	///! Command to execute
  int arg;			///! Value for the command 

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " command: " << command << " arg: " << arg;
    return s.str();
  }
};

/*
 * \class TurnSignalModule
 *
 * This class is the actual GcModule for transmission. 
 */

class TurnSignalModule : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  TurnControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  TurnMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  AdriveTurnInterface::Northface* adriveTurnInterfaceNF;

  /*!\param Actuation interface */
  ActuationInterface *adrive;

  /*! Arbitration for the transmission control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus *, MergedDirective *);
  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*! Turn signal device interface */
  Turnsignal *turnsignal;

  /*! Control for the turn signal module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus *, MergedDirective *);
  int commandDir;
  int controlCount;

  /*! Status for the transmission control module.  This function is called
      as a thread and updates the current status of the transmission
      subsystem */
  void status();
  bool m_bStopStatus;
  int statusCount;
  long statusSleepTime;

  /* Status information */
  int initialized;		/*!< TurnSignal is properly initialized */
  int connected;		/*!< Able to talk to the motor controller */
  int currentDir;
  int steerLimitFlag;		/*!< Keep track of how far we turn */
  int verbose;
  unsigned long long update_time;	/*!< Last time status was updated */

  /*! Mutex controlling access to serial port and data */
  pthread_mutex_t actuatorMutex;

public:
  /*! Constructor */
  TurnSignalModule(int, ActuationInterface *, int verbose = 0);
  
  void initSparrow(DD_IDENT *, int);
  bool Stop();
};

#endif
