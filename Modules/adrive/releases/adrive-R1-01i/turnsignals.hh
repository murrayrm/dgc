#ifndef TURNSIGNAL_DRIVER_H
#define TURNSIGNAL_DRIVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h> //ok
#include <arpa/inet.h>  //ok
#include <netdb.h> // ok
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <iostream>
//#include "DGCutils.hh"

using namespace std;

#define SMARTRELAY_MAX_CHAR 512
#define SMARTRELAY_PORT 10001
#define SMARTRELAY_DEFAULT_IP "192.168.0.82"
#define SMARTRELAY_MSG_TIMEOUT 3

#define SMARTRELAY_ERR_RESET -2
#define SMARTRELAY_ERR_GEN -1
#define SMARTRELAY_ERR_TIMO 0 

#define R(x) #x 
#define RELAY_CMD(x) R(x)  R(\x00D) //consider removing the x00A


// assuming Relay 1&2 is Right turn and Relay 3&4 is Left turn
#define TURNSIGNAL_RF_ON RELAY_CMD(1r1) 
#define TURNSIGNAL_RR_ON RELAY_CMD(2r1) 
#define TURNSIGNAL_LF_ON RELAY_CMD(3r1) 
#define TURNSIGNAL_LR_ON RELAY_CMD(4r1) 

#define TURNSIGNAL_RF_OFF RELAY_CMD(1r0)
#define TURNSIGNAL_RR_OFF RELAY_CMD(2r0)
#define TURNSIGNAL_LF_OFF RELAY_CMD(3r0)
#define TURNSIGNAL_LR_OFF RELAY_CMD(4r0)

#define TURNSIGNAL_RF_STATUS RELAY_CMD(1sR)
#define TURNSIGNAL_RR_STATUS RELAY_CMD(2sR)
#define TURNSIGNAL_LF_STATUS RELAY_CMD(3sR)
#define TURNSIGNAL_LR_STATUS RELAY_CMD(4sR)

#define MAX_NUM_CHECKS 10

// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)


enum TurnType{
  RIGHT,
  LEFT,
  HAZARD,
  HOME,
  UNKNOWN
};

/// Class for activating the turn signals
class Turnsignal 
{
private:
  
  char host[SMARTRELAY_MAX_CHAR]; //its always good to know who we talk to 
  int ctrl_sock;
  fd_set ctrl_fdset;
  
  timeval tl; 
  
  int _SendCtrlMsg(char *msg, int size); 
  int _GetCtrlMsg(char *msg, int size);
  int _MakeSocket(int port, int &sock, fd_set &sock_fd);
  void _CloseSocket(int &socket); 
  
public:

  Turnsignal();
  ~Turnsignal();
  int turnsignal_open();
  int turnsignal_init();
  int turnsignal_setpos(TurnType turn);
  int turnsignal_getpos(TurnType *turn);
  int turnsignal_close();

  char reply_buf[SMARTRELAY_MAX_CHAR];
  
};


#endif
