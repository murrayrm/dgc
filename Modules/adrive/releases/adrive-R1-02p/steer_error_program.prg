; the following program defines the error handling procedure
; currently used by the steering system
; this program is downloaded to the controller through
; the Motion Planner program provided with the controller
; the program may be obtained at 
; http://www.compumotor.com/scripts/support_downloads.asp#MP


; one caveat of this controller: most error conditions other
; than torque faults can be clear by commanding a small motion
; in the direction opposite of the direction the motor was
; traveling when the fault occured or by simply issuing a drive1
; command to re-enable the drive.  Here we do both to cover all
; bases


; BUGS: if the engine is off and the wheels on the ground and a
; 	motion is commanded, this error handling will result in
;	repeated spasms of the wheels until the controller stops
;	receiving new motion commands or the torque required
;	becomes within acceptable limits.  This occurs because
;	the commanded motion will result in a torque fault which
; 	is subsequently cleared by this code, thus allowing the
;	controller to accept another command, and fault again.



del prog1  ; delete programs occupying same mem space
def prog1  ; define new program

DRIVE1     ; enable the drive
DCLRLR     ; clear latched fault status bits
D1	   
GO	   ; command small motion left
D-1	   
GO	   ; command small motion right
END	   ; end program definition

ERRORP PROG1       ; set prog1 to be executed on fault condition
ERRORXXX1XXXXX1XX  ; define fault conditions to execute error prog
		   ; bit 4:  drive fault
		   ; bit 10: move not allowed
		   ; bit 12: max position error exceeded
                   ; IMPORTANT NOTE: bit 12 is no longer a fault condition, since we want the motor to be able to move
		   ; when it is turned off - that is, we want to be able to move it out of its error range without it turning
		   ; itself on
