/*!
 * \file ActuationInterface.cc
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This file receives directives for adrives using the GcInterface
 * class and sends them off to the appropriate actuators.
 */

#include <assert.h>
#include <sys/stat.h>
#include "ActuationInterface.hh"
using namespace std;

/* Constructor */
ActuationInterface::ActuationInterface(int skynet_key, 
				       struct gengetopt_args_info *cmdline) 
  //  : GcModule("adriveInterface", &m_controlStatus, &m_mergedDirective,
  //     1000, 1000)
{
  /* Save the command line options that we were passed */
  options = cmdline;

  /* Get the GcPortHandler */
  m_adriveCommandHandler = new GcPortHandler();
  m_steerHandler = new GcPortHandler();
  m_accelHandler = new GcPortHandler();
  m_transHandler = new GcPortHandler();

  /* Get the GcModuleLogger */
  m_logger = NULL;
  if (options->log_level_arg > 0) {
    m_logger = new GcModuleLogger("ActuationInterface");
    ostringstream oss;
    struct stat st;
    oss << options->log_file_arg << "-actuationInterface.log";
    string suffix = "";
    string logFileName = oss.str();
    // if it exists already, append .1, .2, .3 ... 
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
  
    m_logger->addLogfile(logFileName);
    m_logger->setLogLevel(options->log_level_arg);

    /* Get the north face to trajfollower */
    m_adriveCommandNF = AdriveCommand::generateNorthface(skynet_key, m_adriveCommandHandler, m_logger);

    /* Get the the south face to the actuators */
    m_steer = AdriveSteerInterface::generateSouthface(skynet_key, m_steerHandler, m_logger); 
    m_accel = AdriveAccelInterface::generateSouthface(skynet_key, m_accelHandler, m_logger);
    m_trans = AdriveTransInterface::generateSouthface(skynet_key, m_transHandler, m_logger);
  }
  else {
    /* Get the north face to trajfollower */
    m_adriveCommandNF = AdriveCommand::generateNorthface(skynet_key, m_adriveCommandHandler);

    /* Get the the south face to the actuators */
    m_steer = AdriveSteerInterface::generateSouthface(skynet_key, m_steerHandler, m_logger); 
    m_accel = AdriveAccelInterface::generateSouthface(skynet_key, m_accelHandler, m_logger);
    m_trans = AdriveTransInterface::generateSouthface(skynet_key, m_transHandler, m_logger);
  }

  m_adriveCommandNF->setStaleThreshold(10);
  m_steer->setStaleThreshold(10);
  m_accel->setStaleThreshold(10);
  m_trans->setStaleThreshold(10);

  /* Start up the status loop, which reads/sends ActuatorState */
  m_skynet = new skynet(SNadrive, skynet_key);
  if (cmdline->simulate_given) {
    /* In simulation, we communicate with asim */
    m_drivesocket = m_skynet->get_send_sock(SNdrivecmd);
    m_statesocket = m_skynet->listen(SNactuatorstate, ALLMODULES);
  } else {
    /* In hardware, we broadcast actuator state */
    m_statesocket = m_skynet->get_send_sock(SNactuatorstate);
  }
  statusCount = 0;
  statusSleepTime = 25000;
  DGCstartMemberFunctionThread(this, &ActuationInterface::status);

  /* Mutex for controlling access to actuator state */
  DGCcreateMutex(&actuatorStateMutex);
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void ActuationInterface::initSparrow(DD_IDENT *tbl, int ACTSTATE_LABEL)
{
  if (!options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (options->simulate_given) {
      /* In simulation modue */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("adrive.status_loop_counter", &statusCount, tbl);
  }
}

void ActuationInterface::arbitrate()
  //ControlStatus *controlStatus, MergedDirective *mergedDirective)
{
  while (true) {
    m_adriveCommandHandler->pumpPorts();
    m_steerHandler->pumpPorts();
    m_accelHandler->pumpPorts();
    m_transHandler->pumpPorts();


    while (m_adriveCommandNF->haveNewDirective()) {
      m_adriveCommandNF->getNewDirective( &m_adriveDirective );

      /* Pass on to appropriate actuator */
      switch (m_adriveDirective.actuator) {
      case Steering:
	m_steer->sendDirective(&m_adriveDirective);
	break;

      case Acceleration:
	m_accel->sendDirective(&m_adriveDirective);
	break;

      case Transmission:
	m_trans->sendDirective(&m_adriveDirective);
	break;

      default:
	cerr << "ActuationInterface: received command for unknown actuator: "
	     << m_adriveDirective.actuator << endl;
      }
    }

    /* Check for return status message and pass them back to trajFollower */
    if (m_steer->haveNewStatus()) {
      AdriveResponse *steerResponse = m_steer->getLatestStatus();
      m_adriveCommandNF->sendResponse(steerResponse, steerResponse->id);
    }

    if (m_accel->haveNewStatus()) {
      AdriveResponse *accelResponse = m_accel->getLatestStatus();
      m_adriveCommandNF->sendResponse(accelResponse, accelResponse->id);
    }

    if (m_trans->haveNewStatus()) {
      AdriveResponse *transResponse = m_trans->getLatestStatus();
      m_adriveCommandNF->sendResponse(transResponse, transResponse->id);
    }
    usleep(1000);
  }
}

/*
 * ActuationInterface::status
 *
 * This function is called as a thread and is used to read the status
 * from the individual actuators and broadcast this information across
 * the SNactuationstate channel.  If we are in simulation mode, we
 * read this same information across skynet so that the actuators can
 * use the data from there.
 *
 */

void ActuationInterface::status()
{
  if (options->simulate_given) {
    /* Running in simulation - read actuator state from skynet */
    while (1) {
      #warning missing mutex locking for reading simulation state
      if (m_skynet->get_msg(m_statesocket, &state, sizeof(state)) 
	  != sizeof(state)) {
	cerr << "ActuationInterface::status get_msg error" << endl;
      }

      ++statusCount;
    }

  } else {
    /* Running on hardware - broadcast the current state */
    while (1) {
      /* Fill in any part of the state that we are responsible for */
      /* Nothing currently required here */

      /* Broadcast the state out to the world */
      if (m_skynet->send_msg(m_statesocket, &state,
			     sizeof(state), 0) != sizeof(state)) {
	cerr << "ActuationInterface::status send_msg error" << endl;
      }

      ++statusCount;
      DGCusleep(statusSleepTime);
    }
  }
}
