/*
 * summary.dd - summary display (alternative to default display)
 *
 * Richard M. Murray
 * 22 December 2006
 *
 * This display is an attempt to reformat the default adrive display into
 * something that is shorter (24 lines) and more useful.
 *
 */

extern int actuator_reset(long);
extern int trans_reset(long);
extern int estop_reset(long);
extern int adrive_reset(long);
extern int adrive_manual(long);
extern int adrive_autonomous(long);

%%
01  Mode: %mode              Adrive Summary Display        SNKEY: %snkey
02
03  Actuator   C    Cnt S    Cnt Command  Current  St  Err          Actions
04  %STEER     %A%st_cc %B%st_sc %st_cmd  %st_pos  %a%ster %st_res  %RESET
05  %GAS       %C%ga_cc %D%ga_sc %ga_cmd  %ga_pos  %c%gaer %ga_res  %MAN
06  %BRAKE     %E%br_cc %F%br_sc %br_cmd  %br_pos  %e%brer %br_res  %AUTO
07  %TRANS     %G%tr_cc %H%tr_sc %tr_cmd  %tr_pos  %g%trer %tr_res  %PAUSE
08
09  %ESTOP%I%es_sc    | %OBD %J%ob_sc                    | %SUPERVIS %K%su_sc
10  %SUPERCON %es_cmd | Status:  %ob_st	  RPM:  %ob_rpm  | Status:  %sc_st
11  darpa:    %d_stop | Eng spd: %ob_spd  Temp: %ob_temp | I'locks: %locks
12  astop:    %a_stop | Torque:  %ob_tor		 |
13
14  %ASTATE%K%as_sc mode: %asmode  Status flags		Key bindings
15  N: %northing    N': %northvel  1=OK, 0=off, -1=err	e  re-enable actrs
16  E: %easting     E': %eastvel   2=run, 1=pse, 0=dis	s  send steer cmd
17  Y: %heading     R:  %roll				g  send gas cmd
18  V: %velocity    P:  %pitch				b  send brake cmd
19							t  send trans cmd
20  
21 

%QUIT  %ADRIVE  %ASTATE
%%

# Menu buttons
button: %QUIT "QUIT" dd_exit_loop;
button: %ADRIVE "ADRIVE" dd_usetbl_cb -userarg=addisp;
# button: %ASTATE "ASTATE" dd_usetbl_cb -userarg=asdisp;

# Actuator labels
label: %STEER	  "Steering"  -idname=STEER_LABEL;
label: %GAS	  "Gas"	      -idname=GAS_LABEL;
label: %BRAKE	  "Brake"     -idname=BRAKE_LABEL;
label: %TRANS	  "Trans"     -idname=TRANS_LABEL;
label: %OBD	  "OBD"       -idname=OBDII_LABEL;
label: %ESTOP	  "Estop"     -idname=ESTOP_LABEL;
label: %ASTATE    "Astate"    -idname=ASTATE_LABEL -fg=RED;
label: %SUPERCON  "SuperCon:" -idname=SUPERCON_LABEL;
label: %SUPERVIS  "Supervis"  -idname=SUPERVIS_LABEL;

# Action buttons
button: %RESET  "RESET (F1)" adrive_reset;
button: %MAN    "MAN   (F2)" adrive_manual;
button: %AUTO   "AUTO  (F3)" adrive_autonomous;

# Steering
int: %A		my_vehicle.actuator[ACTUATOR_STEER].status_enabled "%1d";
int: %B		my_vehicle.actuator[ACTUATOR_STEER].command_enabled "%1d";
int: %a		my_vehicle.actuator[ACTUATOR_STEER].status "%1d" -ro;
double: %st_cmd my_vehicle.actuator[ACTUATOR_STEER].command "%6.4f";
double: %st_pos my_vehicle.actuator[ACTUATOR_STEER].position "%6.4f" -ro;
int: %st_sc	my_vehicle.actuator[ACTUATOR_STEER].status_loop_counter "%6d" -ro;
int: %st_cc	my_vehicle.actuator[ACTUATOR_STEER].command_loop_counter "%6d" -ro;
int: %ster	my_vehicle.actuator[ACTUATOR_STEER].error "%5d" -ro;
button: %st_res "RESET" actuator_reset -userarg=ACTUATOR_STEER;

# Throttle (gas)
int: %C		my_vehicle.actuator[ACTUATOR_GAS].status_enabled "%1d";
int: %D		my_vehicle.actuator[ACTUATOR_GAS].command_enabled "%1d";
int: %c		my_vehicle.actuator[ACTUATOR_GAS].status "%1d" -ro;
double: %ga_cmd my_vehicle.actuator[ACTUATOR_GAS].command "%6.4f";
double: %ga_pos my_vehicle.actuator[ACTUATOR_GAS].position "%6.4f" -ro;
int: %ga_sc	my_vehicle.actuator[ACTUATOR_GAS].status_loop_counter "%6d" -ro;
int: %ga_cc	my_vehicle.actuator[ACTUATOR_GAS].command_loop_counter "%6d" -ro;
int: %gaer	my_vehicle.actuator[ACTUATOR_GAS].error "%5d" -ro;
button: %ga_res "RESET" actuator_reset -userarg=ACTUATOR_GAS;

# Brake
int: %E		my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled "%1d";
int: %F		my_vehicle.actuator[ACTUATOR_BRAKE].command_enabled "%1d";
int: %e		my_vehicle.actuator[ACTUATOR_BRAKE].status "%1d" -ro;
double: %br_cmd my_vehicle.actuator[ACTUATOR_BRAKE].command "%6.4f";
double: %br_pos my_vehicle.actuator[ACTUATOR_BRAKE].position "%6.4f" -ro;
int: %br_sc	my_vehicle.actuator[ACTUATOR_BRAKE].status_loop_counter "%6d" -ro;
int: %br_cc	my_vehicle.actuator[ACTUATOR_BRAKE].command_loop_counter "%6d" -ro;
int: %brer	my_vehicle.actuator[ACTUATOR_BRAKE].error "%5d" -ro;
button: %br_res "RESET" actuator_reset -userarg=ACTUATOR_BRAKE;

# Transmission
int: %G		my_vehicle.actuator_trans.status_enabled "%1d";
int: %H		my_vehicle.actuator_trans.command_enabled "%1d";
int: %g		my_vehicle.actuator_trans.status "%1d" -ro;
int: %tr_cmd	my_vehicle.actuator_trans.command "%6d";
int: %tr_pos	my_vehicle.actuator_trans.position "%6d" -ro;
int: %tr_sc	my_vehicle.actuator_trans.status_loop_counter "%6d" -ro;
int: %tr_cc	my_vehicle.actuator_trans.command_loop_counter "%6d" -ro;
int: %trer	my_vehicle.actuator_trans.error "%5d" -ro;
button: %tr_res "RESET" trans_reset;

# Estop
int: %I my_vehicle.actuator_estop.status_enabled "%1d";
int:  %es_sc my_vehicle.actuator_estop.status_loop_counter "%6d" -ro;
int: %es_cmd my_vehicle.actuator_estop.command "%4d";
int: %a_stop  my_vehicle.actuator_estop.astop "%4d";
int: %d_stop  my_vehicle.actuator_estop.dstop "%4d";

# OBD II
int: %J		my_vehicle.actuator_obdii.status_enabled "%1d";
int: %ob_st	my_vehicle.actuator_obdii.status "%6d" -ro;
int:  %ob_sc	my_vehicle.actuator_obdii.status_loop_counter "%6d" -ro;
double: %ob_rpm	my_vehicle.actuator_obdii.engineRPM "%6.0f" -ro;
double: %ob_temp my_vehicle.actuator_obdii.EngineCoolantTemp "%6.1f" -ro;
double: %ob_spd my_vehicle.actuator_obdii.VehicleWheelSpeed "% 6.1f" -ro;
double: %ob_tor my_vehicle.actuator_obdii.WheelForce "% 6.1f" -ro;

# Astate variables (rebound after state is created)
int: 	%K		@apxEnabled 		"%1d";
double: %northing	@vsNorthing		"%12.3f";
double: %easting	@vsEasting		"%12.3f";
double: %heading	@vsYaw			"%12.5f";
double: %northvel	@vsVel_N		"%8.3f";
double: %eastvel	@vsVel_E		"%8.3f";
int:	%asmode		@stateMode		"%d";

# Miscellaneous additional info
int: %su_sc my_vehicle.super_count  "%6d" -ro;
int: %locks my_vehicle.protective_interlocks "%6d";
int: %snkey my_vehicle.skynet_key "%6d" -ro;
message: %mode my_vehicle.mode_string "%8s" -ro;

tblname: summary_tbl;
bufname: summary_buf;
