/*!
 * \file AccelerationModule.hh
 * \brief Header file for AccelerationModule class
 *
 * \author Nok Wongpiromsarn and Richard Murray
 * \date April 2007
 *
 * \ingroup gcmodule
 *
 * This file defines the AccelerationModule class for adrive.  This is
 * used to communicate with the throttle and brake actuators.  This
 * module separates the acceleration commands into separate brake and
 * throttle commands.  In addition, the estop pause logic is included
 * in this module.
 */

#ifndef __AccelerationModule_hh__
#define __AccelerationModule_hh__

#include <pthread.h>
#include <string>
#include "gcmodule/GcModule.hh"
#include "sparrow/display.h"

#include "ActuationInterface.hh"

class AccelControlStatus : public ControlStatus
{
public:
  unsigned id;			// ID for the AccelDirective
  AdriveFailure reason;

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " parent_id: " << parent_id << " status: "
      << status << " reason: " << reason;
    return s.str();
  }
};

/*
 * \class AccelMergedDirective
 * 
 * This is the directive sent between Acceleration Arbitration and
 * Acceleration Control.
 */

class AccelMergedDirective : public MergedDirective
{
public:
  unsigned id;			///! Identifier for this directive
  AdriveCommandType command;	///! Command to execute
  double arg;			///! Value for the command 

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " command: " << command << " arg: " << arg;
    return s.str();
  }
};

/*
 * \class AccelerationModule
 *
 * This class is the actual GcModule for acceleration. 
 */

class AccelerationModule : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  AccelControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  AccelMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  AdriveAccelInterface::Northface* adriveAccelInterfaceNF;

  /*!\param Actuation interface */
  ActuationInterface *adrive;

  /*! Arbitration for the acceleration control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus *, MergedDirective *);
  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*! Control for the accelerationcontrol module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus *, MergedDirective *);
  double brake_command, throttle_command;
  int controlCount;
  unsigned long long command_time;	// time cmd last received

  /*! Status for the acceleration control module.  This function is called
      as a thread and updates the current status of the acceleration
      subsystem */
  void status();
  bool m_bStopStatus;
  pthread_t status_thread;	// keep track of the status thread id
  int statusCount;
  long statusSleepTime;

  /* Status information */
  int brake_connected;		/*!< Able to talk to the brake controller */
  int brake_initialized;	/*!< Brake is properly initialized */
  int throttle_connected;	/*!< Able to talk to the throttle controller */
  int throttle_initialized;	/*!< Brake is properly initialized */
  int system_paused;		/*!< Acceleration system is paused */

  DD_IDENT *dd_tbl;		/*!< Display table */
  int m_throttle_id;		/*!< Sparrow throttle label information */
  int m_brake_id;		/*!< Sparrow brake label information */

  double brake_current;
  double brake_pressure;
  double throttle_current;
  int verbose;			/*!< Turn on verbose error messages */

  /* Tunable parameters */
  double pause_braking;		/*!< Amount of braking to apply in pause */

  /*! Mutexes controlling access to serial port and data */
  pthread_mutex_t brakeMutex;
  pthread_mutex_t throttleMutex;

  /*! Use the PLC? */
  bool throttle_use_plc;
  bool brake_use_plc;

public:
  /*! Constructor */
  AccelerationModule(int, ActuationInterface *, int verbose = 0);
  void initSparrow(DD_IDENT *, int, int);
  void initializeBrake();
  void initializeThrottle();
  bool Stop();
};

#endif
