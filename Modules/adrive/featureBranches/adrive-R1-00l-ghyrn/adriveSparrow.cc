/*
 * \file adriveSparrow
 * \brief Sparrow interface to adrive
 *
 * \author Richard Murray
 * \date 16 April 2007
 *
 * The functions below are used by the sparrow display to control the
 * operation of adrive.
 */

#include "gcmodule/GcModule.hh"
#include "gcmodule/GcInterface.hh"
#include "gcinterfaces/AdriveCommand.hh"

#include "adriveSparrow.hh"

/* 
 * Create the sparrowModule class for use with GcModule 
 * 
 * This class is not actually used, but we need it to create the
 * interface to adrive.  Everything is inlined here since we don't
 * actually require any functionality.
 */
class SparrowModule : public GcModule
{
private:
  ControlStatus m_sparrowStatus;		// unused
  MergedDirective m_sparrowDirective;	// unused

public:
  SparrowModule(int skynet_key) :
    GcModule("adriveSparrow", &m_sparrowStatus, &m_sparrowDirective, 
	     100000, 100000)
  {}

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *) {};
  void control(ControlStatus *, MergedDirective *) {};
};

SparrowModule *sparrowModule = NULL;
AdriveCommand *adriveInterface;
AdriveCommand::Southface *adriveInterfaceSF;
static int id = 0x1000;

/* Initialize the sparrow communications module */
void adrive_initSparrow(int skynet_key)
{
  /* Create the adrive interface and save pointers */
  sparrowModule = new SparrowModule(skynet_key);

  adriveInterface = new AdriveCommand(skynet_key, sparrowModule);
  assert(adriveInterface != NULL);
  adriveInterfaceSF = adriveInterface->getSouthface();

  sparrowModule->Start();
}

/*! Set the steering angle from the display */
int adrive_setSteering_cb(long arg)
{
  if (sparrowModule == NULL) return 0;

  /* Set up the command and send it off through skynet */
  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = Steering;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = *((double *) arg);

  cerr << "Sending steer command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  /* Wait for the response */
  for (int i = 0; i < 100; ++i) {
    if (adriveInterfaceSF->haveNewStatus()) break;
    DGCusleep(10000); 
  }

  /* Get the response and parse it */
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  if (response == NULL) {
    cerr << "Steer command timeout waiting for response" << endl;

  } else if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
    cerr << "Steer command error (" << response->id << "): "
	 << "failure = " << response->reason << endl;
  }

  return 0;
}

/*! Set the throttle command from the display */
int adrive_setThrottle_cb(long arg)
{
  if (sparrowModule == NULL) return 0;

  /* Set up the command and send it off through skynet */
  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = *((double *) arg);

  cerr << "Sending accel command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  /* Wait for the response */
  for (int i = 0; i < 100; ++i) {
    if (adriveInterfaceSF->haveNewStatus()) break;
    DGCusleep(10000); 
  }

  /* Get the response and parse it */
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  if (response == NULL) {
    cerr << "Throttle command timeout waiting for response" << endl;

  } else if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
    cerr << "Throttle command error (" << response->id << "): "
	 << "failure = " << response->reason << endl;
  }

  return 0;
}

/*! Set the brake command from the display */
int adrive_setBrake_cb(long arg)
{
  if (sparrowModule == NULL) return 0;

  /* Set up the command and send it off through skynet */
  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = -(*((double *) arg));

  cerr << "Sending accel command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  /* Wait for the response */
  for (int i = 0; i < 100; ++i) {
    if (adriveInterfaceSF->haveNewStatus()) break;
    DGCusleep(10000); 
  }

  /* Get the response and parse it */
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  if (response == NULL) {
    cerr << "Brake command timeout waiting for response" << endl;

  } else if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
    cerr << "Brake command error (" << response->id << "): "
	 << "failure = " << response->reason << endl;
  }

  return 0;
}

/*! Set the transmission gear from the display */
int adrive_setTrans_cb(long arg)
{
  if (sparrowModule == NULL) return 0;

  /* Set up the command and send it off through skynet */
  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = Transmission;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = *((int *) arg);

  cerr << "Sending trans command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  /* Wait for the response */
  for (int i = 0; i < 100; ++i) {
    if (adriveInterfaceSF->haveNewStatus()) break;
    DGCusleep(10000); 
  }

  /* Get the response and parse it */
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  if (response == NULL) {
    cerr << "Trans command timeout waiting for response" << endl;

  } else if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
    cerr << "Trans command error (" << response->id << "): "
	 << "failure = " << response->reason << endl;
  }

  return 0;
}
