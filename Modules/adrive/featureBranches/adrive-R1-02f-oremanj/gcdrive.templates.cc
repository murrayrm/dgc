/*!
 * \file gcdrive.templates.cc
 * \brief force templates to be precompiled
 *
 * \author Josh Doubleday
 * \date July 2007
 *
 * This file should force templates to be precompiled, so that they
 * don't have to be generated at link time (I think).  Documentation
 * by RMM because Josh didn't document his work (ahem).
 *
 */

#ifdef OMIT_TEMPLATE_DEFS
#undef OMIT_TEMPLATE_DEFS
#endif
#include "gcmodule/GcInterface.hh"

#include "gcinterfaces/AdriveCommand.hh"
#include "ActuationInterface.hh"

/// Do not call the following function -- it is here only to cause the 
/// compiler to generate the template code that would support this function
void instantiateTemplateCode() {
  int skynet_key = 0;
  AdriveTurnInterface::generateNorthface(skynet_key, NULL);
  AdriveTransInterface::generateNorthface(skynet_key, NULL);
  AdriveSteerInterface::generateNorthface(skynet_key, NULL);
  AdriveAccelInterface::generateNorthface(skynet_key, NULL);
  AdriveCommand::generateNorthface(skynet_key, NULL );
}

