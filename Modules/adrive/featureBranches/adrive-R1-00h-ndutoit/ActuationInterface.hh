/*!
 * \file ActuationInterface.hh
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This class serves as the GcModule interface to adrive.  It doesn't
 * actually perform any function, but instead passes the directives
 * that it receives directly to one of the actuation modules.
 */

#ifndef __ActuationInterface_HH__
#define __ActuationInterface_HH__

#include "gcmodule/GcInterface.hh"
#include "skynet/skynet.hh"
#include "interfaces/ActuatorCommand.h"		// for use with simulator

#include "AdriveCommand.hh"
#include "cmdline.h"

/*
 * GcInterfaces to the internal adrive modules
 *
 * The interfaces defined here are used to communicate with the
 * internal adrive modules for each actuator.  Since these are only
 * visible within adrive, we define them here (instead of in
 * AdriveCommand.hh).
 */

/*! Interface to the SteeringModule */
#define SNsteer_command SNtrajfollowerTabOutput
#define SNsteer_status SNtrajfollowerTabInput
typedef GcInterface<AdriveDirective, AdriveResponse, SNsteer_command,
		    SNsteer_status, SNadrive> AdriveSteerInterface;

/*! Adrive actuation interface (router) */
class ActuationInterface : public GcModule
{
private:
  /* Input interface (nominally to TrajFollower) */
  AdriveCommand *m_adriveCommand;		// interface to trajfollower
  AdriveCommand::Northface *m_adriveCommandNF;	//   - real interface
  AdriveDirective m_adriveDirective;		// inputs from trajfollower

  /* Steering controller interface */
  AdriveSteerInterface *m_steerInterface;	// interface to steering
  AdriveSteerInterface::Southface *m_steer;	//   - real interface

  /* Required member variables for GcModule */
  ControlStatus m_controlStatus;	// unused
  MergedDirective m_mergedDirective;	// unused

public:
  struct gengetopt_args_info *options;		///! command line options
  skynet *m_skynet;				///! skynet socket for sim
  int m_drivesocket;				///! adrive socket

  ActuationInterface(int, struct gengetopt_args_info *);

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *);
  void control(ControlStatus *, MergedDirective *) {};
};

#endif
