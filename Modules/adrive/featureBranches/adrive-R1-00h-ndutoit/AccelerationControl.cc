#include "AccelerationControl.hh"
using namespace std;

AccelerationControl::AccelerationControl()
  : GcModule( "AccelerationControl", &m_controlStatus, &m_mergedDirective, 100000, 100000 )
{
  int skynetKey = 0;

  acAccCInterfaceNF = NULL;
  acAccCInterface = new ACAccCInterface( skynetKey, this );
  if( acAccCInterface )
    acAccCInterfaceNF = acAccCInterface->getNorthface();
  else
    cerr << "acAccCInterface = NULL" << endl;
}

void AccelerationControl::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  AccCControlStatus* controlStatus = dynamic_cast<AccCControlStatus*>(cs);
  AccCMergedDirective* mergedDirective = dynamic_cast<AccCMergedDirective*>(md);


  // TODO: Report status to actuation control (maybe only when the control
  // status is FAILED or COMPLETED.
  if (controlStatus->status == AccCControlStatus::COMPLETED ||
      controlStatus->status == AccCControlStatus::FAILED)
  {
    AccCResponse response;
    response.id = controlStatus->id;
    
    if (controlStatus->status == AccCControlStatus::COMPLETED)
      response.status = COMPLETED;
    else
    {
      response.status = FAILED;
      response.reason = controlStatus->reason;
    }

    acAccCInterfaceNF->sendResponse( &response );
  }

  // Compute the next mergedDirective
  if (acAccCInterfaceNF->haveNewDirective())
  {
    ACDirective newDirective;
    acAccCInterfaceNF->getNewDirective( &newDirective );

    // TODO: Determine if you want to reject this directive. This
    // includes checking that the id of newDirective is unique.
    bool reject = false;

    // Update the merged directive if newDirective is not rejected.
    if (!reject)
    {
      // TODO: Make sure that this is correct.
      mergedDirective->id = newDirective.id;
      mergedDirective->number_arg = newDirective.number_arg;
    }
    else
    {
      // TODO: Default extension
    }
  }
}

void AccelerationControl::control(ControlStatus* cs, MergedDirective* md)
{
  AccCControlStatus* controlStatus = dynamic_cast<AccCControlStatus*>(cs);
  AccCMergedDirective* mergedDirective = dynamic_cast<AccCMergedDirective*>(md);
  double number_arg = mergedDirective->number_arg;

  // TODO: Determine and send commands to send to throttle and brake

  // TODO: Determine controlStatus. From your template, it seems like
  // most of the directives replace the previous ones. So in the case
  // where the previous directives are not yet completed, they should
  // fail.

}
