/*!
 * \file adriveMain.c
 * \brief adrive main
 *
 * \author Tully Foote
 * \date 2004-05
 */

#include <assert.h>

#include "skynet/skynet.hh"
#include "dgcutils/cfgfile.h"
#include "parker_steer.hh"
#include "adrive.hh"
#include "actuators.hh"
#include "brake.hh"
#include "throttle.hh"
#include "sninterface.hh"
#include "cmdline.h"

extern int run_sparrow_thread;
extern int run_skynet_thread;
extern int run_logging_thread;
extern pthread_cond_t steer_calibrated_flag;
extern int run_obdii_thread;
extern int run_supervisory_thread;
extern int simulation_flag;
extern char simulator_IP[15];

/* Function for setting actuator label color (in adrive.c) */
extern void set_actuator_label_color(int cmdf, int statf, int actuator);

/*! Adrive is a multi threaded program.  For each actuator there will be 
 * 2 threads running one to periodically poll for status, and a second 
 * interupt based thread executing commands. In addition sparrow is run 
 * in its own thread.  Also logging is another thread.  And the skynet 
 * interface is another thread.   
 *
 * The process is first to initialize the vehicle struct and mutexes.
 * Second if sparrow is enabled it must be started first for it waits for 
 * the steering to initialize, if steering is enabled, so that the calibration
 * routine can happen while the user can still see the screen.  
 * After that each enabled actuator thread is created. 
 * Adrive is then done, it waits for all the threads to end. ANd has the 
 * to add additional shutdown functionality.  But nothing is implemented. */
int main(int argc, char** argv)
{
  // Enable estop logging
  estop_log.enable();

#ifdef ASTATE
  /* Initialize astate options */
  astateOpts astate_options;
  astate_options.logRaw=0;
  astate_options.stateReplay=0;
#endif

  /* Initialize the vehicle struct that controls adrive operation */
  init_vehicle_struct( my_vehicle );

  /* Look for the commandline arguments
   *
   * The only one at this point is the help/usage
   */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0)
    exit(1);

  /* Read in the settings from adrive.config */
  if (!cmdline.noconfig_flag) {
    /* Location the configuration file */
    char *configfile = dgcFindConfigFile(cmdline.config_arg, "adrive");
    assert(configfile != NULL);

    read_config_file(my_vehicle, configfile);
  }

  /*
   * Process command line options 
   *
   * Now that we have read the config file, process the command line
   * options that control whether certain threads should start up.
   *
   */
  if (cmdline.disable_steer_given) { 
    my_vehicle.actuator[ACTUATOR_STEER].status_enabled = 0;
    my_vehicle.actuator[ACTUATOR_STEER].command_enabled = 0;
  }

  if (cmdline.disable_brake_given) { 
    my_vehicle.actuator[ACTUATOR_BRAKE].status_enabled = 0;
    my_vehicle.actuator[ACTUATOR_BRAKE].command_enabled = 0;
  }

  if (cmdline.disable_gas_given) { 
    my_vehicle.actuator[ACTUATOR_GAS].status_enabled = 0;
    my_vehicle.actuator[ACTUATOR_GAS].command_enabled = 0;
  }

  if (cmdline.disable_trans_given) {
    my_vehicle.actuator_trans.status_enabled = 0;
    my_vehicle.actuator_trans.command_enabled = 0;
  }

  if (cmdline.disable_obd_given) my_vehicle.actuator_obdii.status_enabled = 0;
  if (cmdline.disable_estop_given) my_vehicle.actuator_estop.status_enabled = 0;

  /* Reset the status of interlocks if specified on the command line */
  if (cmdline.disable_interlocks_given) 
    my_vehicle.protective_interlocks = false;
  if (cmdline.enable_interlocks_given)
    my_vehicle.protective_interlocks = true;

  /* Figure out what skynet key to use */
  my_vehicle.skynet_key = skynet_findkey(argc, argv);

  /*
   * Configure adrive operations
   *
   * At this point we should have initalized all of the information in
   * my_vehicle and cmdline structs and we can proceed to starting up
   * all required threads.
   */

  /* Sparrow thread */
  pthread_t sparrow_handle;
  if(run_sparrow_thread && !cmdline.disable_sparrow_flag)  
    pthread_create( &sparrow_handle, NULL, &sparrow_main, &my_vehicle );
  else 
    cerr << "NO SPARROW DISPLAY" << endl;

  /* 
   * Start the supervisory thread
   * 
   * This thread makes sure all the actuators are working properly
   */
  pthread_t supervisory_handle;  
  if (run_supervisory_thread)
    pthread_create( &supervisory_handle, NULL, &supervisory_thread, 0);

  /* Start threads for actuators if we are using them
   * 
   * This iterates through each of the actuators, checking whether their
   * threads are enabled and starts the threads if they are enabled.
   */ 
  for(int i = 0; i < NUM_ACTUATORS; i++) {
    actuator_t * act = &(my_vehicle.actuator[i]);
    // Check and start the actuator status threads
    if ( act->status_enabled ) 
      pthread_create(& act->pthread_status_handle, NULL, 
		     &actuator_status_thread_function, act );

    // Check and start the actuator command threads
    if ( act->command_enabled ) 
      pthread_create(& act->pthread_command_handle, NULL, 
		     &actuator_command_thread_function, act );

    // Set the label color based on what is running
    set_actuator_label_color(act->command_enabled, act->status_enabled, i);
  }

  /* 
   * Start the skynet thread 
   * 
   * This thread handles all of the skynet communications.  It listens
   * for incoming commands as well as creating an additional thread to
   * broadcast the actuator_state data to fill the CStateClient struct
   * for other modules.
   */
  pthread_t skynet_handle;
  struct skynet_main_args args;
  if( run_skynet_thread ) {
    args.pVehicle = &my_vehicle;
    args.pRun_skynet_thread = &run_skynet_thread;

    pthread_create( &skynet_handle, NULL, &skynet_main, &args );
  }

  /* 
   * Start the OBDII thread
   * 
   * This thread moniters the vehicle through the OBDII interface
   * board.
   */
  if (my_vehicle.actuator_obdii.status_enabled) {
    pthread_create(&my_vehicle.actuator_obdii.pthread_status_handle,
		   NULL, &actuator_obdii_status_thread_function,
		   &my_vehicle.actuator_obdii);
  }
  set_actuator_label_color(1, my_vehicle.actuator_obdii.status_enabled,
			   ACTUATOR_OBDII);

  /* 
   * Start the ESTOP thread
   * 
   * This thread moniters the vehicle through the ESTOP interface
   * board.
   */
  if (my_vehicle.actuator_estop.status_enabled) {
    pthread_create(& my_vehicle.actuator_estop.pthread_status_handle,
		   NULL, &actuator_estop_status_thread_function,
		   &my_vehicle.actuator_estop );
  }
  set_actuator_label_color(1, my_vehicle.actuator_obdii.status_enabled,
			   ACTUATOR_ESTOP);

  /* 
   * Start the TRANS STATUS thread
   * 
   * This thread moniters the vehicle through the TRANS interface
   * board.
   */
  if (my_vehicle.actuator_trans.status_enabled) {
    fprintf(stderr, "Creating TRANS STATUS THREAD\n");
    pthread_create(&my_vehicle.actuator_trans.pthread_status_handle,
		   NULL, &actuator_trans_status_thread_function,
		   &my_vehicle.actuator_trans );
  }

  /* 
   * Start the TRANS command thread
   * 
   * This thread moniters the vehicle through the
   * TRANS interface board. 
   */
  if (my_vehicle.actuator_trans.command_enabled) {
    fprintf(stderr, "Creating TRANS command THREAD\n");
    pthread_create(&my_vehicle.actuator_trans.pthread_status_handle,
		   NULL, &actuator_trans_command_thread_function,
		   &my_vehicle.actuator_trans );
  }

  set_actuator_label_color(my_vehicle.actuator_trans.command_enabled,
			   my_vehicle.actuator_trans.status_enabled,
			   ACTUATOR_TRANS);

  /* 
   * Start the logging thread
   * 
   * This thread simply logs data to file
   */
  pthread_t logging_handle;  
  if (run_logging_thread)
    pthread_create(&logging_handle, NULL, &logging_main, 0);

#ifdef ASTATE
  /* Start up astate if specified on command line */
  if (cmdline.enable_astate_flag) {
    AState *ss = new AState(cmdline.snkey_arg, astate_options);
    astate_sparrow_rebind(ss);
  }
#endif

  /*Done setting up threads; wait until everything terminates */

  /* If display is running, it is the main thread */
  if (run_sparrow_thread) {
    pthread_join(sparrow_handle, NULL);

    /* Other threads don't seem to close gracefully, so exit here */
    exit(0);
  }

  // Join threads to wait until everything ends */
  for(int i = 0; i < NUM_ACTUATORS; i++) {
    actuator_t * act = &(my_vehicle.actuator[i]);
    
    cerr << "adrive: shutting down " << act->name << endl;
    if( act->status_enabled ) 
      pthread_join( act->pthread_status_handle, NULL);
    if( act->command_enabled ) 
      pthread_join( act->pthread_command_handle, NULL);
  }
  if( run_skynet_thread ) pthread_join( skynet_handle, NULL);

  if (my_vehicle.actuator_estop.status_enabled) {  
    estop_t * act = &(my_vehicle.actuator_estop);
    pthread_join( act->pthread_status_handle, NULL);
  }

  if (my_vehicle.actuator_trans.status_enabled) {  
    trans_t * act = &(my_vehicle.actuator_trans);
    pthread_join( act->pthread_status_handle, NULL);
  }

  if (my_vehicle.actuator_trans.command_enabled) {  
    trans_t * act = &(my_vehicle.actuator_trans);
    pthread_join( act->pthread_command_handle, NULL);
  }

  if (my_vehicle.actuator_obdii.status_enabled) {  
    obdii_t * act = &(my_vehicle.actuator_obdii);
    pthread_join( act->pthread_status_handle, NULL);
  }

  if (run_supervisory_thread) pthread_join(supervisory_handle, NULL);
  if (run_logging_thread) pthread_join(logging_handle, NULL);

  /* All done */
  return 0;
}
