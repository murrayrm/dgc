/*!
 * \file UT_adrive.cc 
 * \brief Adrive unit test
 * \author Richard M. Murray
 * \date 14 April 2007
 *
 * This program serves as a basic unit test for the adrive module.  It
 * works by sending commands to adrive through the GcModule interface
 * and verifying that the appropriate response was received.  This
 * test is designed to be used with asim.
 */

#include <iostream>
#include <assert.h>
using namespace std;

#include "skynet/skynet.hh"
#include "gcinterfaces/AdriveCommand.hh"

/* 
 * Create the testModule class for use with GcModule 
 * 
 * This class is not actually used since this is just a test function.
 * However, in order to talk to adrive we need to have a GcModule, so
 * this is it.  Everything is inlined here since we don't actually
 * require any functionality.
 */
class TestModule : public GcModule
{
private:
  ControlStatus m_testStatus;		// unused
  MergedDirective m_testDirective;	// unused

public:
  TestModule(int skynet_key) :
    GcModule("testModule", &m_testStatus, &m_testDirective, 100000, 100000)
  {}

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *) {};
  void control(ControlStatus *, MergedDirective *) {};
};

int main (int argc, char **argv)
{
  cout << "Unit test: " << argv[0] << endl;
  int skynet_key = skynet_findkey(argc, argv);

  /* Create a GcModule for the test program */
  TestModule *testModule = new TestModule(skynet_key);
  testModule->Start();

  /* Set up the interface to adrive */
  AdriveCommand *adriveInterface = 
    new AdriveCommand(skynet_key, testModule);
  assert(adriveInterface != NULL);
  AdriveCommand::Southface *adriveInterfaceSF =
    adriveInterface->getSouthface();

  /* Send a steering ocmmand to adrive */
  AdriveDirective adriveCommand;
  adriveCommand.id = 7;
  adriveCommand.actuator = Steering;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = 0.05;

  cerr << "Sending steer command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  cerr << "Waiting for response: ";
  while (!adriveInterfaceSF->haveNewStatus()) {
    cerr << "."; 
    fflush(stderr);
    sleep(1);
  }

  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  assert(response != NULL);
  assert(response->status == GcInterfaceDirectiveStatus::COMPLETED);
  assert(response->id == adriveCommand.id);
  cerr << "Received " << response->status << endl;

  /*
   * OutOfRange test
   *
   * In this test, we send an out of range steering command and verify
   * that it is rejected.
   */

  adriveCommand.id = 9;
  adriveCommand.actuator = Steering;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = 2;

  cerr << "Sending steer command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  cerr << "Waiting for response: ";
  while (!adriveInterfaceSF->haveNewStatus()) {
    cerr << "."; 
    fflush(stderr);
    sleep(1);
  }

  response = adriveInterfaceSF->getLatestStatus();
  assert(response != NULL);
  assert(response->id == adriveCommand.id);
  assert(response->status == GcInterfaceDirectiveStatus::REJECTED);
  assert(response->reason == OutOfRange);
  cerr << "Received " << response->status << endl;

#ifdef LATER
  /*
   * Recovery test
   *
   * Make sure that if we now send a valid command that it works correctly.
   *
   */

  adriveCommand.id = 12;
  adriveCommand.actuator = Steering;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = -0.2;

  cerr << "Sending steer command: " << adriveCommand.arg << endl;
  adriveInterfaceSF->sendDirective(&adriveCommand);

  cerr << "Waiting for response: ";
  while (!adriveInterfaceSF->haveNewStatus()) {
    cerr << "."; 
    fflush(stderr);
    sleep(1);
  }

  response = adriveInterfaceSF->getLatestStatus();
  assert(response != NULL);
  assert(response->id == adriveCommand.id);
  assert(response->status == GcInterfaceDirectiveStatus::COMPLETED);
  cerr << "Received " << response->status << endl;
#endif

  return 0;
}
