/*!
 * \file TransmissionModule.cc
 * \brief GcModule for controlling Alice's transmission
 * 
 * \author Nok Wongpiromsarn and Richard Murray
 * \date 14 April 2007
 *
 * \ingroup gcmodule
 *
 * This is the GcModule that controls Alice's transmission.  It receives
 * commands from the ActuatorInterface module (router) and sends them
 * to the transmission motor controller.  The initial release assumes
 * correct operation and does minimal checking for errors.
 */

#include <assert.h> 
#include <sys/stat.h>
#include "dgcutils/DGCutils.hh"

#include "TransmissionModule.hh"
#include "igntrans.hh"
#include "plctools/igntrans.h"
#include "plctools/gcplc.h"
#include "plctools/plc.h"

using namespace std;

/*
 * TransmissionModule::TransmissionModule
 *
 * The constructor initializes the GcModule interface, the transmission
 * port and other class data.
 */

TransmissionModule::TransmissionModule(int skynetKey,
				       ActuationInterface *actuationInterface,
				       int verbose_flag)
  : GcModule("Adrive_TransmissionModule", &m_controlStatus, &m_mergedDirective,
	      SLOW_SLEEP, SLOW_SLEEP)
{
  /* Set up the log file if log level is greater than 0 */
  if (actuationInterface->options->log_level_arg > 0) {
    ostringstream oss;
    oss << actuationInterface->options->log_file_arg << "-transmission";
    string logFileName = oss.str();

    /* This is how we use GcModuleLogger. First we tell it to create a log file.
       Then, set the log level of the module so when we use 
       gclog(n) below, the message will be logged if the log level is
       greater than or equal to n. */
    this->addLogfile(logFileName);
    this->setLogLevel(actuationInterface->options->log_level_arg);
  }

  use_plc = (actuationInterface->options->use_plc_flag || (actuationInterface->options->partial_plc_flag &&
                                                           (actuationInterface->options->trans_port_arg < 0)));

  verbose = verbose_flag;
  if (verbose > 8) cerr << "TransmissionModule: verbose = " << verbose << endl;

  /*
   * GcModule initialization
   *
   * The first thing that we do is initialize all of the variables
   * that are required for communicating with other GcModules,
   * including the ActuationInterface.
   */

  /* Get the interface to the transControl module */
  adriveTransInterfaceNF = 
    AdriveTransInterface::generateNorthface(skynetKey, this);

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * TransmissionModule initialization
   *
   * Initialize the member variables used by the control and arbitrate
   * functions.
   */

  /* Initialize the IDs so that we can tell when we get new commands */
  m_latestID = m_currentID = 0;

  /* Initialize the control state to keep from returning a response */
  m_controlStatus.status = ControlStatus::RUNNING;
  controlCount = 0;

  /*
   * Hardware/simulation initailization
   *
   * Now that we have initalized the software interfaces, we
   * initialize the hardware interface.  This consists of opening up
   * the serial port so that we can talk to the motor controller and
   * then initializing the motor controller.  If we can't open the
   * serial port, we abort with an error.  It is possible for the
   * motor controller initialization to fail if transmission is not
   * currently enabled in Alice.  If this fails, we mark the interface
   * as uninitialized and continue on.  This allows us to continue to
   * receive status and also to rest the transmission module manually from
   * sparrow.
   *
   * If the transmission interface is disabled (using the command line
   * flag), then we still try to open the port but we don't generate
   * abort if we can't do it.  This allows the transmission status
   * information to still be run in the case that we are in Alice and
   * just don't want to use transmission.  If this transmission is disabled,
   * we set the inialized flag to true so that it appears to modules
   * that transmission is being commanded.  This is useful for testing.
   *
   * If we are running in simulation mode, we don't bother trying to
   * talk to the hardware. 
   */

  if (adrive->options->disable_trans_flag) {
    /* Transmission has been disabled => pretend initialized, but don't talk */
    connected = 0;
    initialized = 1;

  } else if (adrive->options->simulate_flag) {
    /* Simulation mode - no action required */
    initialized = 1;
    connected = 1;
    
  } else if (use_plc) {
    /* No need to initialize anything for the PLC, but we can sanity check. */
    if (!plc_trans_check()) {
      perror ("TransmissionModule: plc_trans_check failed");
      exit (1);                 // don't allow operation without transmission
    }
    
    connected = 1;
    initialized = 1;

  } else {
    /* Open the port to the actuator */
    if (verbose) cerr << "TransmissionModule: opening igntrans" << endl;
    if (igntrans_open(adrive->options->trans_port_arg) != TRUE) {
      /* Don't allow operation without being able to talk to transmission */
      cerr << "TransmissionModule: igntrans_open failed" << endl;
      exit(1);

    } else {
      /* Keep track of the fact that we are connected */
      connected = 1;
      initialized = 1;
    }
  }

  /*
   * Status thread startup
   *
   * As part of the TransmissionModule function, we start up a thread to
   * read the status of the actuator on a periodic basis.  This thread
   * fills in data in the TransmissionModule class, so we have easy
   * access to this information.  
   */

  statusCount = 0;
  statusSleepTime = STATUS_SLEEP*4;
  DGCstartMemberFunctionThread(this, &TransmissionModule::status);

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&actuatorMutex);

}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void TransmissionModule::initSparrow(DD_IDENT *tbl, int label_id)
{
  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the motor controller (transmission disabled) */
      dd_setcolor_tbl(label_id, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(label_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(label_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("trans.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("trans.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("trans.command", &commandGear, tbl);
    dd_rebind_tbl("trans.position", &currentGear, tbl);
    dd_rebind_tbl("trans.connected", &connected, tbl);
    dd_rebind_tbl("trans.initialized", &initialized, tbl);
  }
}

/*
 * TransmissionModule::arbitrate
 *
 * Arbitrator for TransmissionModule.  The artibiter receives transmission
 * commands from the actuationInterface and sends them to control.  At
 * present, the only directives that are recognized are new transmission
 * commands, which replace old transmission commands.
 *
 */

void TransmissionModule::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  TransControlStatus *controlResponse =
    dynamic_cast<TransControlStatus *>(cs);

  TransMergedDirective *mergedDirective =
    dynamic_cast<TransMergedDirective *>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  AdriveResponse response;

  switch (controlResponse->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::COMPLETED;
    adriveTransInterfaceNF->sendResponse( &response, response.id );
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::FAILED;
    response.reason = controlResponse->reason;
    adriveTransInterfaceNF->sendResponse( &response, response.id );
    gclog(1) << response.toString() << endl;
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */

  /* Check to see if we have rec'd a new directive from actuation interface */
  if (adriveTransInterfaceNF->haveNewDirective()) {
    AdriveDirective newDirective;
    adriveTransInterfaceNF->getNewDirective( &newDirective );
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    gclog(2) << t << "\tReceived acc command: id = " 
	     << newDirective.id << endl;
 
    /* !! Make sure that the directive has a unique ID !! */
    /* Not implemented */

    /* Save the ID to pass back when we are done */
    mergedDirective->parent_id = newDirective.id;

    if (verbose >= 5) {
      cerr << "TransmissionModule: received directive " 
	   << newDirective.command << " with argument "
	   << newDirective.arg << endl;
    }

    switch (newDirective.command) {
    case SetPosition:
    case Disable:
      /* Make sure we are properly initalized */
      if (!initialized) {
	/* Respond with a failure */
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::FAILED;
	response.reason = NotInitialized;
	adriveTransInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Check the range of the command */
      if (newDirective.arg < -1 || newDirective.arg > 2) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = OutOfRange;
	adriveTransInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Make sure that the car is stopped */
      if (adrive->state.m_VehicleWheelSpeed > TRANSMISSION_SHIFT_SPEED) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehicleNotStopped;
	adriveTransInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Generate the directive to trans the car */
      mergedDirective->command = newDirective.command;
      mergedDirective->arg = (int) newDirective.arg;
      mergedDirective->id = ++m_latestID;
      break;

    case Reset:
      /* ActuationInterface meks sure that we are stopped */
      mergedDirective->command = Reset;
      mergedDirective->id = ++m_latestID;
      break;

    default:
      if (verbose) 
	cerr << "TransmissionModule: unrecognized directive: "
	     << newDirective.command << endl;

      response.id = newDirective.id;
      response.status = GcInterfaceDirectiveStatus::REJECTED;
      response.reason = OutOfRange;
      adriveTransInterfaceNF->sendResponse( &response, response.id );
      break;
    }
  }
}

/*
 * TransmissionModule::control
 *
 * Control for TransmissionModule.  The artibiter receives merged
 * directives the TransmissionModule arbitrator.  For transmission, this is
 * just the latest transmission command, which is sent on to the hardware.
 * The m_currentID tag is used to keep track of which directive we are
 * currently excuting.
 *
 */

void TransmissionModule::control(ControlStatus *cs, MergedDirective *md)
{
  TransControlStatus* controlResponse =
    dynamic_cast<TransControlStatus *>(cs);
  TransMergedDirective* mergedDirective =
    dynamic_cast<TransMergedDirective *>(md);

  /* First check to see if we have anything new to do */
  if (mergedDirective->id == m_currentID) {
    /* Tell the arbiter we are still running */
    controlResponse->status = ControlStatus::RUNNING;
    return;
  }

  /* New directive to process: update IDs and counters (for sparrow) */
  m_currentID = mergedDirective->id;
  ++controlCount;

  switch (mergedDirective->command) {
    int status;

  case SetPosition:
    /* Set the command at the front so that we see it on the display */
    commandGear = mergedDirective->arg;
    if (verbose >= 5) {
      cerr << "TransmissionModule: shifting to " << commandGear << endl;
    }

    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_flag) {
      drivecmd_t my_command;
      my_command.my_actuator = trans;
      my_command.my_command_type = set_position;
      my_command.number_arg = mergedDirective->arg;
      adrive->m_skynet->send_msg(adrive->m_drivesocket, &my_command, 
				 sizeof(my_command), 0);

      /* Wait for a few seconds to insure everything is working */
      sleep(2);

    } else {
      /* Send the command to the transmission controller */
      pthread_mutex_lock(&actuatorMutex);
      if (use_plc)
        plc_trans_setposition (mergedDirective->arg);
      else
        trans_setposition(mergedDirective->arg);

#define SHIFT_TIMEOUT  3000000
      /* Wait for the shift to occur. ActuationInterface is in state Shifting during this time,
         and is rejecting all commands. If we don't have OBD-II, just sleep for three seconds;
         that's longer than any shift should ever take. If we do, use 3 seconds as a timeout. */
      if (adrive->options->fake_obd_flag || adrive->options->simulate_flag ||
          adrive->options->disable_obd_flag) {
        if (verbose >= 3)
          cerr << "Transmission INFO: OBD-II not available, faking absurdly long shift delay" << endl;
        usleep (SHIFT_TIMEOUT);
      } else {
        unsigned long long now, end;
        DGCgettime (now);
        end = now + SHIFT_TIMEOUT;
        while (now < end) {
          if (adrive->state.m_TransmissionPosition == mergedDirective->arg)
            break;              // we've shifted
          usleep (STATUS_SLEEP);
        }
        if (now >= end && verbose >= 2)
          cerr << "Transmission WARNING: shift to gear " << mergedDirective->arg << " timed out! "
               << "Vehicle is currently in gear " << adrive->state.m_TransmissionPosition
               << "according to OBD-II." << endl;
      }

      /* Unlock the mutex *after* we finish shifting */
      pthread_mutex_unlock(&actuatorMutex);
    }

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  case Disable:
    cerr << "TransmissionModule: received disable" << endl;

    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_flag) {
      /* Shift to park */
      drivecmd_t my_command;
      my_command.my_actuator = trans;
      my_command.my_command_type = set_position;
      my_command.number_arg = 0;
      adrive->m_skynet->send_msg(adrive->m_drivesocket, &my_command, 
				 sizeof(my_command), 0);

      /* Turn off the igntion */
      if (verbose)
	cerr << "Turning off ignition; not implemented in simulation" << endl;

    } else {
      /* Send the command to the transmission controller to shift to park */
      pthread_mutex_lock(&actuatorMutex);
      if (verbose >= 3) cerr << "Setting  transmittion to park" << endl;
      if (use_plc)
        plc_trans_setposition (T_PARK);
      else
        trans_setposition(0);

      /* Send the command to the ignition controller to turn off engine */
      if (verbose >= 3) cerr << "Turning off ignition" << endl;
      if (use_plc)
        plc_ign_setposition (I_OFF);
      else
        ign_setposition(I_OFF);

      pthread_mutex_unlock(&actuatorMutex);
    }

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    commandGear = 0;
    break;

  case Reset:
    /* Initialize the transmission actuator (if connected) */
    pthread_mutex_lock(&actuatorMutex);
    status = !connected || igntrans_open(adrive->options->trans_port_arg);
    pthread_mutex_unlock(&actuatorMutex);

    /* Make sure that it worked */
    if (status) {
      initialized = 1;
      controlResponse->status = ControlStatus::STOPPED;

    } else {
      initialized = 0;
      controlResponse->status = ControlStatus::FAILED;
      controlResponse->reason = InitializationError;
      if (verbose >= 2) cerr << "TransmissionModule: Reset failed" << endl;
    }      
    controlResponse->id = m_currentID;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  default:
    /* Shouldn't ever get here */
    controlResponse->status = ControlStatus::FAILED;
    controlResponse->reason = OutOfRange;
    break;
  }
}

bool TransmissionModule::Stop()
{
  m_bStopStatus = true;
  DGCusleep( statusSleepTime * 2 );
  return GcModule::Stop();
}

/*
 * TransmissionModule::status
 *
 * This function is called as a thread and is used to read the status
 * of the actuator.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void TransmissionModule::status()
{
  m_bStopStatus = false;
  while (!m_bStopStatus) {
    if (adrive->options->simulate_flag) {
      /* In simulation mode, read state from asim */
      currentGear = adrive->asim_state.m_transpos;
      update_time = DGCgettime();

    } else {
      if (!connected) {
	/* Transmission is currently disabled, so just return zero */
	currentGear = 0;

      } else {
	/* Read the current status */
        int retval;
        if (use_plc) {
          retval = plc_trans_getposition();
          /* Make sure it's not in manual. */
          if (gcplc_read_digital (TRANS_MAN_DI)) {
            retval = -2;
          }
        } else {
          pthread_mutex_lock(&actuatorMutex);
          retval = trans_getposition();
          pthread_mutex_unlock(&actuatorMutex);
        }

	/* Check the status and set state appropriately */
	if (retval == -2 && initialized) {
	  /* Error condition */
	  if (verbose >= 2) 
	    cerr << "TransmissionModule::error reading transmission position"
		 << endl;
	  initialized = 0;

	} else {
	  currentGear = retval;
	  update_time = DGCgettime();
	}

      }
    }

    /* Now fill up the actuatorState struct with current state */
    adrive->state.m_transpos = currentGear;
    adrive->state.m_transcmd = commandGear;
    adrive->state.m_transstatus = initialized && connected;
    adrive->state.m_trans_update_time = update_time;

    /* Now that we have the state, check to see if there are any problems */
    /* TODO: implement something here */

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

/* Local Variables: */
/* c-basic-offset: 2 */
/* End: */
