/*!
 * \file SteeringModule.hh
 * \brief Header file for SteeringModule class
 *
 * \author Nok Wongpiromsarn and Richard Murray
 * \date April 2007
 *
 * This file defines the SteeringModule class for adrive.  This is
 * used to communicate with the steering actuator.  Most of this is
 * just a pass through for the directives and responses that are
 * routed through adrive.
 */

#ifndef __SteeringModule_hh__
#define __SteeringModule_hh__

#include <pthread.h>

#include "gcmodule/GcModule.hh"
#include "sparrow/display.h"

#include "ActuationInterface.hh"

class SteerControlStatus : public ControlStatus
{
public:
  unsigned id;			// ID for the SteerDirective
  AdriveFailure reason;

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
};

/*
 * \class SteerMergedDirective
 * 
 * This is the directive sent between Steering Arbitration and
 * Steering Control.
 */

class SteerMergedDirective : public MergedDirective
{
public:
  unsigned id;			///! Identifier for this directive
  AdriveCommandType command;	///! Command to execute
  double arg;			///! Value for the command 

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
};

/*
 * \class SteeringModule
 *
 * This class is the actual GcModule for steering. 
 */

class SteeringModule : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  SteerControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  SteerMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  AdriveSteerInterface* adriveSteerInterface;
  AdriveSteerInterface::Northface* adriveSteerInterfaceNF;

  /*!\param Actuation interface */
  ActuationInterface *adrive;

  /*! Arbitration for the steering control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus *, MergedDirective *);
  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*! Control for the steeringcontrol module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus *, MergedDirective *);
  double commandHeading;
  int controlCount;

  /*! Status for the steering control module.  This function is called
      as a thread and updates the current status of the steering
      subsystem */
  void status();
  int statusCount;
  long statusSleepTime;

  /* Status information */
  int initialized;
  double currentHeading;

  /*! Mutex controlling access to serial port and data */
  pthread_mutex_t actuatorMutex;

public:
  /*! Constructor */
  SteeringModule(int, ActuationInterface *);
};

#endif
