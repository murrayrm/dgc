/*!
 * \file SteeringModule.cc
 * \brief GcModule for controlling Alice's steering
 * 
 * \author Nok Wongpiromsarn and Richard Murray
 * \date 14 April 2007
 *
 * This is the GcModule that controls Alice's steering.  It receives
 * commands from the ActuatorInterface module (router) and sends them
 * to the steering motor controller.  The initial release assumes
 * correct operation and does minimal checking for errors.
 */

#include <assert.h> 

#include "dgcutils/DGCutils.hh"

#include "SteeringModule.hh"
#include "parker_steer.hh"
#include "overview.h"		// display definitions
using namespace std;

/*
 * SteeringModule::SteeringModule
 *
 * The constructor initializes the GcModule interface, the steering
 * port and the sparrow interface.
 */

SteeringModule::SteeringModule(int skynetKey,
				 ActuationInterface *actuationInterface)
  : GcModule("Adrive_SteeringModule", &m_controlStatus, &m_mergedDirective,
	      100000, 100000 )
{
  /*
   * GcModule initialization
   *
   * The first thing that we do is initialize all of the variables
   * that are required for communicating with other GcModules,
   * including the ActuationInterface.
   */

  /* Get the interface to the steerControl module */
  adriveSteerInterface = new AdriveSteerInterface( skynetKey, this );
  assert(adriveSteerInterface != NULL);
  adriveSteerInterfaceNF = adriveSteerInterface->getNorthface();

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * SteeringModule initialization
   *
   * Initialize the member variables used by the control and arbitrate
   * functions.
   */

  /* Initialize the IDs so that we can tell when we get new commands */
  m_latestID = m_currentID = 0;

  /* Initialize the control state to keep from returning a response */
  m_controlStatus.status = ControlStatus::RUNNING;

  /*
   * Hardware/simulation initailization
   *
   * Now that we have initalized the software interfaces, we
   * initialize the hardware interface.  This consists of opening up
   * the serial port so that we can talk to the motor controller and
   * then attempting to initialize.  If the steering enable switch is
   * turned off, this command can fail.  In that case, just leave the
   * initialize flag off and allow a reset from the adrive program.
   *
   * If we are running in simulation, no initialization is required.
   */
  
  if (adrive->options->simulate_given) {
    /* Simulation mode - no action required */
    initialized = 1;

  } else {
    /* Open the port to the actuator */
    if (steer_open(adrive->options->steer_port_arg) != TRUE) {
      cerr << "SteeringModule: steer_open failed" << endl;
      exit(1);
    }

    /* Attempt to initialize the motor controller */
    if (steer_init() == TRUE) {
      initialized = 1;

    } else {
      cerr << "SteeringModule: steer_init failed" << endl;
    }
  }

  /*
   * Status thread startup
   *
   * As part of the SteeringModule function, we start up a thread to
   * read the status of the actuator on a periodic basis.  This thread
   * fills in data in the SteeringModule class, so we have easy
   * access to this information.  
   */

  DGCstartMemberFunctionThread(this, &SteeringModule::status);
  statusCount = 0;
  statusSleepTime = 25000;

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&actuatorMutex);

  /*
   * Sparrow initialization
   *
   * At this point we should be able to set up variables that are used
   * by sparrow to keep track of the status of this device.  Note that
   * all of these variables should be pointing to memory in the
   * current class.
   */

  if (!adrive->options->disable_console_given) {
    /* Initialize sparrow so that it sees internal variables */
    dd_setcolor_tbl(STEER_LABEL, BLACK, GREEN, overview_tbl);

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("steer.command_loop_counter", &controlCount, overview_tbl);
    dd_rebind_tbl("steer.status_loop_counter", &statusCount, overview_tbl);
    dd_rebind_tbl("steer.command", &commandHeading, overview_tbl);
    dd_rebind_tbl("steer.position", &currentHeading, overview_tbl);
  }
}

/*
 * SteeringModule::arbitrate
 *
 * Arbitrator for SteeringModule.  The artibiter receives steering
 * commands from the actuationInterface and sends them to control.  At
 * present, the only directives that are recognized are new steering
 * commands, which replace old steering commands.
 *
 */

void SteeringModule::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  SteerControlStatus *controlResponse =
    dynamic_cast<SteerControlStatus *>(cs);

  SteerMergedDirective *mergedDirective =
    dynamic_cast<SteerMergedDirective *>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  AdriveResponse response;

  switch (controlResponse->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::COMPLETED;
    adriveSteerInterfaceNF->sendResponse( &response );
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::FAILED;
    response.reason = controlResponse->reason;
    adriveSteerInterfaceNF->sendResponse( &response );
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */

  /* Check to see if we have rec'd a new directive from actuation interface */
  if (adriveSteerInterfaceNF->haveNewDirective()) {
    AdriveDirective newDirective;
    adriveSteerInterfaceNF->getNewDirective( &newDirective );

    /* !! Make sure that the directive has a unique ID !! */
#   warning need to add check for unique directive ID

    /* Save the ID to pass back when we are done */
    mergedDirective->parent_id = newDirective.id;

    switch (newDirective.command) {
    case SetPosition:
      /* Make sure we are properly initalized */
      if (!initialized) {
	/* Respond with a failure */
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::FAILED;
	response.reason = NotInitialized;
	adriveSteerInterfaceNF->sendResponse( &response );
	return;
      }

      /* Check the range of the command */
      if (newDirective.arg < -1 || newDirective.arg > 1) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = OutOfRange;
	adriveSteerInterfaceNF->sendResponse( &response );
	return;
      }

      /* Generate the directive to steer the car */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = newDirective.arg;
      mergedDirective->id = ++m_latestID;
      break;

    case Reset:
      /* !! Check to make sure we are stopped !! */
#     warning missing check for stop on Steer Reset
      mergedDirective->command = Reset;
      mergedDirective->id = ++m_latestID;
      break;
    }
  }
}

/*
 * SteeringModule::control
 *
 * Control for SteeringModule.  The artibiter receives merged
 * directives the SteeringModule arbitrator.  For steering, this is
 * just the latest steering command, which is sent on to the hardware.
 * The m_currentID tag is used to keep track of which directive we are
 * currently excuting.
 *
 */

void SteeringModule::control(ControlStatus *cs, MergedDirective *md)
{
  SteerControlStatus* controlResponse =
    dynamic_cast<SteerControlStatus *>(cs);
  SteerMergedDirective* mergedDirective =
    dynamic_cast<SteerMergedDirective *>(md);

  /* First check to see if we have anything new to do */
  if (mergedDirective->id == m_currentID) {
    /* Tell the arbiter we are still running */
    controlResponse->status = ControlStatus::RUNNING;
    return;
  }

  /* New directive to process: update IDs and counters (for sparrow) */
  m_currentID = mergedDirective->id;
  commandHeading = mergedDirective->arg;
  ++controlCount;

  switch (mergedDirective->command) {
  case SetPosition:
    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_given) {
      drivecmd_t my_command;
      my_command.my_actuator = steer;
      my_command.my_command_type = set_position;
      my_command.number_arg = mergedDirective->arg;
      adrive->m_skynet->send_msg(adrive->m_drivesocket, &my_command, 
				 sizeof(my_command), 0);

    } else {
      /* Send the command to the steering controller */
      pthread_mutex_lock(&actuatorMutex);
      steer_heading(mergedDirective->arg);
      pthread_mutex_unlock(&actuatorMutex);
    }

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  case Reset:
    /* Initialize the steering actuator */
    pthread_mutex_lock(&actuatorMutex);
    int status = steer_init();
    pthread_mutex_unlock(&actuatorMutex);

    /* Make sure that it worked */
    if (status) {
      initialized = 1;
      controlResponse->status = ControlStatus::STOPPED;

    } else {
      initialized = 0;
      controlResponse->status = ControlStatus::FAILED;
      #warning unknown failure status
      controlResponse->reason = InitializationError;
    }      
    controlResponse->id = m_currentID;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;
  }
}

/*
 * SteeringModule::status
 *
 * This function is called as a thread and is used to read the status
 * of the actuator.
 *
 */

void SteeringModule::status()
{
  while (1) {
    /* Read the current status */
    pthread_mutex_lock(&actuatorMutex);
    int retval = (double) steer_getheading();
    pthread_mutex_unlock(&actuatorMutex);

    /* Check the status and set state appropriately */
    if (retval == -2) {
      /* Error condition */
#     warning steering status error not yet handled

    } else {
      currentHeading = retval;
    }

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}
