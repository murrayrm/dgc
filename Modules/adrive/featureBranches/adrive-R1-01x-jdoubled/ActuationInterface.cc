/*!
 * \file ActuationInterface.cc
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 *
 * \ingroup gcdrive
 * 
 * This file receives directives for gcdrive using the GcInterface
 * class and sends them off to the appropriate actuators.
 */

#include <assert.h>
#include <sys/stat.h>
#include "ActuationInterface.hh"
#include "igntrans.hh"
using namespace std;


#ifdef GCMODULE_EVENT_BASED
#define cerr  gclog(1)
/// utiltity function to wait for timeout
void ActuationInterface::timedWait( long nTime ) {
  gclog(8) << "timed wait: " << nTime << endl;
  DGCusleep( nTime );
}
#define DGCusleep  timedWait
#endif


/* Constructor */
#ifndef GCMODULE_EVENT_BASED 
ActuationInterface::ActuationInterface( int skynet_key, 
                                        struct gengetopt_args_info *cmdline,
                                        int verbose_flag)  
#else
ActuationInterface::ActuationInterface( int skynet_key, 
                                        struct gengetopt_args_info *cmdline,
                                        int verbose_flag)  
  : GcModule( "ActuationInterface", &m_controlStatus, &m_mergedDirective,
	      FAST_SLEEP, FAST_SLEEP, true )
#endif
{
  /* Save the command line options that we were passed */
  options = cmdline;
  verbose = verbose_flag;

#ifndef GCMODULE_EVENT_BASED 
  /* Get the GcPortHandler */
  m_adriveCommandHandler = new GcPortHandler();
  m_steerHandler = new GcPortHandler();
  m_accelHandler = new GcPortHandler();
  m_transHandler = new GcPortHandler();
  m_turnHandler = new GcPortHandler();

  /* Get the GcModuleLogger */
  m_logger = NULL;
  if (options->log_level_arg > 0) {
    m_logger = new GcModuleLogger("ActuationInterface");
    ostringstream oss;
    oss << options->log_file_arg << "-actuationInterface";
    string logFileName = oss.str();
    m_logger->addLogfile(logFileName);
    m_logger->setLogLevel(options->log_level_arg);
  }
  /* Get the north face to trajfollower */
  m_adriveCommandNF = AdriveCommand::
    generateNorthface(skynet_key, m_adriveCommandHandler, m_logger);

  /* Get the the south face to the actuators */
  m_steer = AdriveSteerInterface::
    generateSouthface(skynet_key, m_steerHandler, m_logger); 
  m_accel = AdriveAccelInterface::
    generateSouthface(skynet_key, m_accelHandler, m_logger);
  m_trans = AdriveTransInterface::
    generateSouthface(skynet_key, m_transHandler, m_logger);
  m_turn = AdriveTurnInterface::
    generateSouthface(skynet_key, m_turnHandler, m_logger);


#else
  {
    ostringstream oss;
    oss << options->log_file_arg << "-actuationInterface";
    string logFileName = oss.str();
    addLogfile(logFileName);
    setLogLevel(options->log_level_arg);
  }

  /* Get the north face to trajfollower */
  m_adriveCommandNF = AdriveCommand::
    generateNorthface(skynet_key, this);

  /* Get the the south face to the actuators */
  m_steer = AdriveSteerInterface::
    generateSouthface(skynet_key, this);
  m_steerHandler = this;

  m_accel = AdriveAccelInterface::
    generateSouthface(skynet_key, this);
  m_accelHandler = this;

  m_trans = AdriveTransInterface::
    generateSouthface(skynet_key, this);
  m_transHandler = this;

  m_turn = AdriveTurnInterface::
    generateSouthface(skynet_key, this);
  m_turnHandler = this;
#endif

  /* Limit the number of directives and responses stored in GcInterface */
  m_adriveCommandNF->setStaleThreshold(10);
  m_steer->setStaleThreshold(10);
  m_accel->setStaleThreshold(10);
  m_trans->setStaleThreshold(10);
  m_turn->setStaleThreshold(10); 
  /* 
   * Start up the status loop, which reads/sends ActuatorState 
   *
   */
  m_skynet = new skynet(SNadrive, skynet_key);

  /* Check to see if we should start up the simulator */
  asim_status = 0;
  if (cmdline->simulate_given) {
    /* In simulation, we communicate with asim */
    m_drivesocket = m_skynet->get_send_sock(SNdrivecmd);
    m_asimsocket = m_skynet->listen(SNasimActuatorState, ALLMODULES);
    DGCstartMemberFunctionThread(this, 
                                &ActuationInterface::getSimActuatorState);
  }

  /* Initialize the thread that sends out state information */
  m_statesocket = m_skynet->get_send_sock(SNactuatorstate);
  statusCount = 0;
  statusSleepTime = STATUS_SLEEP/2;
  DGCstartMemberFunctionThread(this, &ActuationInterface::status);

  /* Mutex for controlling access to actuator state */
  DGCcreateMutex(&actuatorStateMutex);

  /* Initialize the estop state */
  state.m_astoppos = EstopPause;
  state.m_estoppos = EstopPause;
  actuation_state = Unknown;

  /* Initialize our actuation ID to something unique */
  actuation_id = 0x8000;
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void ActuationInterface::initSparrow(DD_IDENT *tbl, int ACTSTATE_LABEL)
{
  if (!options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (options->simulate_given) {
      /* In simulation modue */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("adrive.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("estop.estoppos", &state.m_estoppos, tbl);
    dd_rebind_tbl("actuation_state", &actuation_state, tbl);
  }
}


/*
 * ActuationInterface::status
 *
 * This function is called as a thread and is used to read the status
 * from the individual actuators and broadcast this information across
 * the SNactuationstate channel.
 *
 */

void ActuationInterface::status()
{
  m_bStopStatus = false;
  /* Broadcast the current state */
  while (!m_bStopStatus) {
    /* Fill in any part of the state that we are responsible for */
    state.m_estoppos = 
      (actuation_state == Running | actuation_state == Shifting) ? EstopRun :
      (actuation_state == Disabled) ? EstopDisable : 
      EstopPause;

    /* Broadcast the state out to the world */
    if (m_skynet->send_msg(m_statesocket, &state,
                          sizeof(state), 0) != sizeof(state)) {
      cerr << "ActuationInterface::status send_msg error" << endl;
    }

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

#ifdef GCMODULE_EVENT_BASED
/// Stop our status thread on top of normal GcModule stopping
bool ActuationInterface::Stop()
{
  m_bStopStatus = true;
  DGCusleep( statusSleepTime * 2 );
  GcModule::Stop();
}

/*! Handle function callbacks when a message is received
 *
 *  Overload the default handleMessageReadyCallback function to 
 *  a) prevent recursion (we call pumpports manually in our arbitrate)
 *  b) add some logging messages                                         
 *
 */
void ActuationInterface::handleMessageReadyCallbacks() {
  if(m_bEventDriven) {
    m_bEventDriven = false;
    gclog(9) << "gcPumpThread: calling mainloopCallback on new msg received" 
	     << endl;
    mainloopCallback();
    m_bEventDriven = true ;
  }
}

/*! Function to process callbacks on events
 * Overload the mainloopCallback for finer control (don't have to recompile
 * gcmodule) and to adding logging
 */
void ActuationInterface::mainloopCallback() 
{
  for(int i=0; i < 5; i++) {
    arbitrate(m_pControlStatusBase, m_pMergedDirectiveBase);
    gclog(9) << " * control status at      " << usecTime() << ": "
	     << *m_pControlStatusBase << endl;
    gclog(9) << " * arbitratedDirective at " << usecTime() << ": "
	     << *m_pMergedDirectiveBase << endl;
    control(m_pControlStatusBase, m_pMergedDirectiveBase);  
  }
}

/*! Function to pump ports for event driven version of code 
 *
 * Here we can manually pump the ports and/or wait for a timeout or a condition
 * signal that a new message is ready to be pumped
 */
void ActuationInterface::pumpPortsLocal(long nTime) {
  /// If a time greater than zero is provided, we will pumpports,
  /// wait for a timeout or signal, then pump-ports again
  if(nTime > 0) {
    gclog(8) << "ActuationInterface::pumpPortsLocal: conditional/timed wait "
	     << "with signal: " << nTime << " us" << endl;
    pumpPorts();
    bool bSignaled = (0 == GCpthread_cond_timedwait( m_condMessageReady.pCond,
                                                     m_condMessageReady.pMutex,
                                                     nTime) );
    gclog(8) << "ActuationInterface::pumpPortsLocal: signaled=" << bSignaled << " at " << usecTime() << endl;
    pumpPorts();

  } else {
    pumpPorts();
  }
}

/* Empty control loop - do everything in control function */
void ActuationInterface::control(ControlStatus* cs, MergedDirective* md)
{ }

#endif // GCMODULE_EVENT_BASED 


#ifdef GCMODULE_EVENT_BASED
void ActuationInterface::arbitrate(ControlStatus *cs, MergedDirective *md)
#else
void ActuationInterface::arbitrate()
#endif
{
  /// actuation_resume_start must be a static variable as arbitrate
  /// is now called in succession rather than an endless loop within
  /// itself (var goes out of scope)
  static unsigned long long actuation_resume_start;

# ifndef GCMODULE_EVENT_BASED
  /* For non-event-based version; arbitrate does looping */
  while (true) {
# endif
    /*
     * Determine the state of the actuation interface
     *
     * At this point, we check the estop status to see if this causes
     * any change in state.  Further processing of the state is done
     * later based on directives and responses (for changing gears).
     *
     * Note: if estop is disabled (from the command line), the status
     * of the actuator has to be set from the display.
     */

    /* Make sure we are initialized to Pause condition */
    if (actuation_state == Unknown) {
      sendPauseCommand(Pause);
      actuation_state = Paused;
    }

    /* Check to see if we have received a Disable command (locking) */
    else if (actuation_state == Disabled || 
	     state.m_dstoppos == EstopDisable) { 
      /* Issue a brake command (repeat each time to be sure) */
      sendPauseCommand(Pause);

      /* Once we come to a stop, shift to park and turn off ignition */
#     warning TRANSMISSION_SHIFT_SPEED belongs in AdriveCommand.hh
      const double TRANSMISSION_SHIFT_SPEED = 1.0;
      if (state.m_VehicleWheelSpeed < TRANSMISSION_SHIFT_SPEED) {
	cerr << "DISABLE: shifting to park and turning off engine" << endl;
	AdriveDirective adriveCommand;
	adriveCommand.id = actuation_id++;
	adriveCommand.actuator = Transmission;
	adriveCommand.command = Disable;
	adriveCommand.arg = 0;
	m_trans->sendDirective(&adriveCommand);
	m_transHandler->pumpPorts();

	/* Give the system some time to respond (20% over trans update) */
	DGCusleep(STATUS_SLEEP*5);
      }

      /* Set the state to disabled (and stay there) */
      actuation_state = Disabled;

      /* Delay for a bit so that we don't overload the actuation drivers */
      DGCusleep(FAST_SLEEP*10);
    } 

    /* Check to see if we have received a Pause command.  We can get
       here from either Running, Paused, Resuming or Shifting. */
    else if (state.m_dstoppos == EstopPause) {
      /* Issue a brake command (repeat each time to be sure) */
      sendPauseCommand(Pause);

      /* Save the state of the system */
      actuation_state = Paused;

      /* Delay for a bit so that we don't overload the actuation drivers */
      DGCusleep(FAST_SLEEP*10);
    }

    /* Check to make sure we are receiving commands; otherwise Pause */
    else if (actuation_state == Running && 
	     !options->disable_command_timeout_flag && 
	     (drive_command_time > 0) && 
	     ((int) (DGCgettime() - drive_command_time)
	      > options->command_timeout_arg)) {
      /* We haven't received a command, force a pause */
      cerr << "Didn't receive command in " << options->command_timeout_arg
	   << " usec" << endl;
      sendPauseCommand(Pause);
      actuation_state = Paused;

      /* Delay for a bit so that we don't overload the actuation drivers */
      DGCusleep(SLOW_SLEEP);
    }

    /* See if we are coming out of a pause mode */
    else if (actuation_state == Paused && state.m_dstoppos == EstopRun) {
      /* Set up a timer to keep track of when it is OK to come out of pause */
      actuation_resume_start = DGCgettime();
      actuation_state = Resuming;
    }

    /* See if we are waiting to come out of pause mode */
    else if (actuation_state == Resuming) {
      unsigned long long time_pausing = DGCgettime() - actuation_resume_start;
      if (DGCtimetosec(time_pausing) > 5) {
	sendPauseCommand(ReleasePause);
	actuation_state = Running;
      }
    }

#   ifdef GCMODULE_EVENT_BASED
    /* Log any long delays */
    const int nLongDelay = 160000;  /// this is approx 100ms (10Hz from follower) 
                                    /// + six 10ms linux timeslices, which should
                                    /// be more than adequate time to deliver a message..
    static unsigned long long last_long_command_time = 0;
    if (actuation_state == Running && (drive_command_time > 0) && 
        ((int) (DGCgettime() - drive_command_time) > nLongDelay) &&
        last_long_command_time != drive_command_time ) {
      /* We haven't received a command */
      last_long_command_time = drive_command_time;
      gclog(2) << "Didn't receive command in " << nLongDelay
        << " usec; its been: " << (int) (DGCgettime() - drive_command_time) << " usec" << endl;
    }
#   endif


    /*
     * At this point the actuation_state is set appropriately and we
     * can decide what to do.  Here are the options:
     *
     *  - Running: send through all commands
     *  - Paused: only send through steering commands; reject others
     *  - Resuming: same as pause
     *  - Shifting: changing gears; reject all commands
     *  - Disabled: reject all commands
     *
     */

    /* Call pumpPorts to receive all the messages */
#   ifdef GCMODULE_EVENT_BASED
    pumpPortsLocal();
#   else
    m_adriveCommandHandler->pumpPorts();
    m_steerHandler->pumpPorts();
    m_accelHandler->pumpPorts();
    m_transHandler->pumpPorts();
    m_turnHandler->pumpPorts();
#   endif

    /* Pass on all the received directives to apropriate actuator. Put
       this in a while loop to make sure that the router can keep up
       with trajfollower. */
    while (m_adriveCommandNF->haveNewDirective()) {
      m_adriveCommandNF->getNewDirective( &m_adriveDirective );

      /* If this is a Steer or Accel command, update our timer */
      if (m_adriveDirective.actuator == Steering ||
	  m_adriveDirective.actuator == Acceleration) {
	DGCgettime(drive_command_time);
      }

      /* If we are disabled or shifting, reject all commands */
      if (actuation_state == Disabled || actuation_state == Shifting) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehiclePaused;
	m_adriveCommandNF->sendResponse(&response, response.id);
	continue;
      }

      /* If we are paused, only accept steering and reet commands */
      if ((actuation_state == Paused || actuation_state == Resuming) &&
	  (m_adriveDirective.actuator != Steering &&
	   m_adriveDirective.command != Reset)) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehiclePaused;
	m_adriveCommandNF->sendResponse(&response, response.id);
	continue;
      }

      /* If we try to reset any acutator, make sure we are stopped */
      /* Make sure that the car is stopped */
      if (m_adriveDirective.command == Reset &&
	  state.m_VehicleWheelSpeed > ACTUATION_RESET_SPEED) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehicleNotStopped;
	m_adriveCommandNF->sendResponse( &response, response.id );
	continue;
      }

      /* Define some variables needed below */
      AdriveDirective brakeCommand;
      const double TRANSMISSION_SHIFT_SPEED = 0.5;

      /* Otherwise, we can process the command we are sent */
      switch (m_adriveDirective.actuator) {
      case Steering:
	m_steer->sendDirective(&m_adriveDirective);
	break;

      case Acceleration:
	m_accel->sendDirective(&m_adriveDirective);
	break;

      case Transmission:
	/* 
	 * Actuating the transmission requires special care to make
	 * sure that the vehicle is stopped.  We also want to make
	 * sure that once we have received a transmission command that
	 * we don't accept any additional commands until we have
	 * completed the shift.
	 *
	 */

	/* Make sure that we are commanding some level of braking */
	if (state.m_brakecmd == 0 || 
	    state.m_VehicleWheelSpeed > TRANSMISSION_SHIFT_SPEED) {
	  AdriveResponse response;
	  response.id = m_adriveDirective.id;
	  response.status = GcInterfaceDirectiveStatus::REJECTED;
	  response.reason = VehicleNotStopped;
	  m_adriveCommandNF->sendResponse(&response, response.id);
	  continue;
	}

#define BRAKE_DURING_SHIFT
#ifdef  BRAKE_DURING_SHIFT
	/* Send brake command, overriding any other acceleration commands */
	brakeCommand.id = actuation_id++;
	brakeCommand.command = SetPosition;
	brakeCommand.actuator = Acceleration;
	brakeCommand.arg = -ADRIVE_PAUSE_BRAKING;
	m_accel->sendDirective(&brakeCommand);
#       ifndef GCMODULE_EVENT_BASED
	m_accelHandler->pumpPorts();
#       endif

	/* Wait for the brake to start having an effect */
	DGCusleep(STATUS_SLEEP);
#endif

	/* Send the command through the transmission */
	m_trans->sendDirective(&m_adriveDirective);

	/* Keep track of the fact that we are now shifting */
	actuation_state = Shifting;
	break;

      case TurnSignal:
	m_turn->sendDirective(&m_adriveDirective);
	break;

      default:
	cerr << "ActuationInterface: received command for unknown actuator: "
	     << m_adriveDirective.actuator << endl;
      }
    }


    /* Check for return status message and pass them back to trajFollower */
    while (m_steer->haveNewStatus()) {
      AdriveResponse *steerResponse = m_steer->getLatestStatus();
      m_adriveCommandNF->sendResponse(steerResponse, steerResponse->id);
    }

    while (m_accel->haveNewStatus()) {
      AdriveResponse *accelResponse = m_accel->getLatestStatus();
      m_adriveCommandNF->sendResponse(accelResponse, accelResponse->id);
    }

    while (m_trans->haveNewStatus()) {
      AdriveResponse *transResponse = m_trans->getLatestStatus();

      /* 
       * Check to see if we can come out of shifting mode
       *
       * The only way we can get into this state is if we were sent a
       * transmission command while in "Running" mode and we received
       * a response.  Independent of what the response says, we are no
       * longer shifting at this point. 
       */

#ifdef RIGOROUS_SHIFT_LOGIC
      /* Only shift back when we see a completed directive */
      if (actuation_state == Shifting && 
	  transResponse->status == GcInterfaceDirectiveStatus::COMPLETED) {
	  actuation_state = Running;
      }
#else
      /* Assume that if we got any response, we are running again */
      if (actuation_state == Shifting) actuation_state = Running;
#endif

      /* Send the response through */
      m_adriveCommandNF->sendResponse(transResponse, transResponse->id);
    }

    while (m_turn->haveNewStatus()) {
      AdriveResponse *turnResponse = m_turn->getLatestStatus();
      m_adriveCommandNF->sendResponse(turnResponse, turnResponse->id);
    }

# ifndef GCMODULE_EVENT_BASED
    /* Sleep for a short while (remember this is in the control loop) */
    DGCusleep(FAST_SLEEP);
  }
# else
  return;
# endif
}

/*! Send pause/release to acceleration module */
void ActuationInterface::sendPauseCommand(AdriveCommandType cmd)
{
  /* Make sure we got passed something sensible */
  assert(cmd == Pause || cmd == ReleasePause);

  AdriveDirective pauseCommand;
  pauseCommand.id = actuation_id++;
  pauseCommand.command = cmd;
  pauseCommand.actuator = Acceleration;

  m_accel->sendDirective(&pauseCommand);
  m_accelHandler->pumpPorts();

  /* Wait until we get a response and read it */
  int i;
  const int MAXITER = 20;
  for (i = 0; i < MAXITER; ++i) {
    if (m_accel->haveNewStatus()) { 
      AdriveResponse *response = m_accel->getLatestStatus();
      assert(response != NULL);

      /* Make sure this response was intended for us */
      if (response->id != pauseCommand.id) {
	/* Response to someone else; pass back to sender */
	m_adriveCommandNF->sendResponse(response, response->id);
#       ifdef GCMODULE_EVENT_BASED
        pumpPortsLocal(-1);
#       endif
      } else
	/* This was our acknowledgement; break out of loop */
	break;
    } else {
#     ifdef GCMODULE_EVENT_BASED
      pumpPortsLocal(FAST_SLEEP*10);
#     else
      usleep(FAST_SLEEP*10);
      m_accelHandler->pumpPorts();
#     endif
    }
  }

  /* Make sure we received a response */
  if (i == MAXITER) {
    cerr << "ActuationInterface: didn't receive response to "
	 << (cmd == Pause ? "pause" : "release") << endl;
  }
}

/*
 * ActuationInterface::getSimActuatorState
 *
 * This thread reads the simulated actuator state from asim.  This
 * information is stored so that it can be accessed by the individual
 * actuators.
 *
 */

void ActuationInterface::getSimActuatorState()
{
  /* Read actuator state from skynet */
  while (1) {
#   warning missing mutex locking for reading simulation state
    if (m_skynet->get_msg(m_asimsocket, &asim_state, sizeof(state)) 
        != sizeof(state)) {
      cerr << "ActuationInterface::status get_msg error" << endl;
    } else {
      /* Let everyone know that they can grab the state now */
      asim_status = 1;
    }
  }
}


/*
 * Startup functions
 *
 * The autostart() and autoshift() functions are used at startup to
 * initilized the state of the vehicle.
 *
 */

# define WAIT_SLEEP 500000		// time to wait between status checks
# define WAIT_COUNT 20  		// maximum number of wait iterations

int ActuationInterface::autoshift(int verbose)
{
  /* Make sure transmission module is running */
  if (options->disable_trans_flag) {
    cerr << "transmission disabled; ";
    return -1;
  }

  /* Wait to make sure that status loop as run before we do anything */
  int i;
  for (i = 0; i < WAIT_COUNT; ++i) {
    if (state.m_trans_update_time > 0) break;
    DGCusleep(WAIT_SLEEP);
  }
  if (i == WAIT_COUNT) {
    cerr << "timed out waiting for transmission status; ";
    exit(1);
  }

  /* Check to see if the transmission is connected */
  if (!state.m_transstatus) {
    cerr << "transmission not connected and/or initialized; ";
    return -1;
  }

  /* Check to see if we are already in gear */
  if (state.m_transpos == 1 || state.m_transpos == -1) {
    if (verbose) cerr << "transmission already in gear" << endl;
  } else {
    /* Shift into drive */
    if (verbose) cerr << "shifting into drive" << endl;

    /* Issue a shift command directly through the transmission actuator */
    AdriveDirective adriveCommand;
    adriveCommand.id = actuation_id++;
    adriveCommand.actuator = Transmission;
    adriveCommand.command = SetPosition;
    adriveCommand.arg = 1;
    m_trans->sendDirective(&adriveCommand);
    m_transHandler->pumpPorts();

    /* Verify that the shift occured */
    if (verbose >= 2) cerr << "checking that shift occured" << endl;
    for (i = 0; i < WAIT_COUNT; ++i) {
      m_transHandler->pumpPorts();
      if (m_trans->haveNewStatus()) {
        AdriveResponse *response = m_trans->getLatestStatus();
        if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
          cerr << "couldn't shift gears; response status = " 
            << response->status;
          return -1;
        }

        /* Successfully shifted gears; break out of loop */
        break;
      }
      DGCusleep(WAIT_SLEEP);
    }
    if (i == WAIT_COUNT) {
      cerr << "timed out waiting for shift; ";
      return -1;
    }

    if (verbose) cerr << "auto-shift complete" << endl;
  }

  return 0;
}

#ifdef GCMODULE_EVENT_BASED 
/// utility function to wait for timeout or signal
void ActuationInterface::signaledWait( long nTime )
{
  gclog(8) << "conditional/timed wait with signal: " << nTime << endl;
  GCpthread_cond_timedwait( m_msgReady.pCond, m_msgReady.pMutex, nTime );
}

/// gcPumpThread
/// 
/// overload the default gcPumpThread for extra logging, and to avoid recompile
/// of gcmodule library.
void ActuationInterface::gcPumpThread()
{
  while (!GcSimplePortHandlerThread::gcMessageEndThreadFlag)
  {
    // Wait for timeout or signal that a message is ready on the port
    gclog(9) << " * gcPumpThread: entering conditionwait/sleep(" << m_nMessageLoopUsleepTime << "): " << usecTime() << ": " << *m_pMergedDirectiveBase << endl;
    bool bSignaled = (0 == GCpthread_cond_timedwait(  m_condMessageReady.pCond,
                               m_condMessageReady.pMutex, 
                               m_nMessageLoopUsleepTime ) );
    gclog(9) << " * gcPumpThread: sleep ended. " << usecTime()
	     << " Signaled: " << bSignaled << endl;
    // pump ports for new messages until there are none to be received
    while ( bool bNew = pumpPorts() ) {
      gclog(9) << " * gcPumpThread: new messages ready " << bNew << endl;
      // if we have a handle to a condition to signal, do so, otherwise, just
      // call the messagecallback handler
      if( m_msgReadySignal ) {
        pthread_cond_signal( &m_msgReadySignal->pCond );
        handleMessageReadyCallbacks();
        //DGCusleep(0);
      }
      else {
        handleMessageReadyCallbacks();
      }
  }
  }
  gclog(9) << " * gcPumpThread: thread ending" << endl;
  DGCSetConditionTrue( GcSimplePortHandlerThread::m_condStopped );
}
#endif

/* Local Variables: */
/* c-basic-offset: 2 */
/* End: */
