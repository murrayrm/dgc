#include "gcmodule/GcModule.hh"
#include "AdriveCommand.hh"

class AccCControlStatus : public ControlStatus
{
public:
  /* Generally, status can be COMPLETED, FAILED, 
     or READY_FOR_NEXT. You probably don't need the whole list or
     you may need other status such as when the control is still
     working on a given directive. 
     TODO: Redefine this. */
  enum Status{ COMPLETED, FAILED };

  unsigned id; // The id of the merged directive that this control status corresponds to.
  Status status;
  AccCResponse::ReasonForFailure reason;
};

class AccCMergedDirective : public MergedDirective
{
public:
  unsigned id;
  double number_arg;
};

class AccelerationControl : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  AccCControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  AccCMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  ACAccCInterface* acAccCInterface;
  ACAccCInterface::Northface* acAccCInterfaceNF;

  /*! Arbitration for the acceleration control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the acceleration control module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);

public:
  /*! Constructor */
  AccelerationControl();
};
