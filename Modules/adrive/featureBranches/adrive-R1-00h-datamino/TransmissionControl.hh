#include "gcmodule/GcModule.hh"
#include "AdriveCommand.hh"

class TCControlStatus : public ControlStatus
{
public:
  /* Generally, status can be COMPLETED, FAILED, 
     or READY_FOR_NEXT. You probably don't need the whole list or
     you may need other status such as when the control is still
     working on a given directive. 
     TODO: Redefine this. */
  enum Status{ COMPLETED, FAILED };

  unsigned id; // The id of the merged directive that this control status corresponds to.
  Status status;
  TCResponse::ReasonForFailure reason;
};

class TCMergedDirective : public MergedDirective
{
public:
  unsigned id;
  double number_arg;
};

class TransmissionControl : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  TCControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  TCMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  ACTCInterface* acTCInterface;
  ACTCInterface::Northface* acTCInterfaceNF;

  /*! Arbitration for the transmission control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);
  
  /*! Control for the acceleration control module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus*, MergedDirective*);

public:
  /*! Constructor */
  TransmissionControl();
};
