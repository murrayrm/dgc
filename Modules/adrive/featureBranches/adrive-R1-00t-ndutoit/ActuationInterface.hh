/*!
 * \file ActuationInterface.hh
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This class serves as the GcModule interface to adrive.  It doesn't
 * actually perform any function, but instead passes the directives
 * that it receives directly to one of the actuation modules.
 */

#ifndef __ActuationInterface_HH__
#define __ActuationInterface_HH__

#include "gcmodule/GcInterface.hh"
#include "skynet/skynet.hh"
#include "interfaces/ActuatorState.h"
#include "interfaces/ActuatorCommand.h"		// for use with simulator
#include "sparrow/display.h"
#include "gcinterfaces/AdriveCommand.hh"

#include "cmdline.h"

/*
 * GcInterfaces to the internal adrive modules
 *
 * The interfaces defined here are used to communicate with the
 * internal adrive modules for each actuator.  Since these are only
 * visible within adrive, we define them here (instead of in
 * AdriveCommand.hh).
 */

/*! Interface to the SteeringModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrive> AdriveSteerInterface;

/*! Interface to the AccelerationModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrive_commander> AdriveAccelInterface;

/*! Interface to the TransmissionModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrivelogger> AdriveTransInterface;

/*! Adrive actuation interface. It is not a gcmodule but use the lightweight
 *  GcInterface to communicate with the internal adrive modules*/
class ActuationInterface
{
private:
  /* Logger */
  GcModuleLogger* m_logger;

  /* Input interface (nominally to TrajFollower) */
  AdriveCommand::Northface *m_adriveCommandNF;	// interface to trajfollower
  AdriveDirective m_adriveDirective;		// inputs from trajfollower

  /* Controller interfaces */
  AdriveSteerInterface::Southface *m_steer;	// interface to steering
  AdriveAccelInterface::Southface *m_accel;	// interface to acceleration
  AdriveTransInterface::Southface *m_trans;	// interface to transmission

  /* Required member variables for GcModule */
  ControlStatus m_controlStatus;		// unused
  MergedDirective m_mergedDirective;		// unused

  /* GcPortHandler for sending/receiving directives and responses */
  GcPortHandler *m_adriveCommandHandler;
  GcPortHandler *m_steerHandler;
  GcPortHandler *m_accelHandler;
  GcPortHandler *m_transHandler;

  /*! Actuation status broadcast thread */
  void status();
  int statusCount;
  long statusSleepTime;
  pthread_mutex_t actuatorStateMutex;

public:
  struct gengetopt_args_info *options;		///! command line options
  skynet *m_skynet;				///! skynet socket for sim
  int m_drivesocket;				///! adrive socket
  int m_statesocket;				///! actuation state socket
  ActuatorState state;				///! current state of actuators

  ActuationInterface(int, struct gengetopt_args_info *);

  virtual ~ActuationInterface() {
    delete m_adriveCommandHandler;
    delete m_steerHandler;
    delete m_accelHandler;
    delete m_transHandler;
    if (m_logger != NULL)
      delete m_logger;
  } 

  void initSparrow(DD_IDENT *, int);

  /* Arbitrate and control functions (unused) */
  void arbitrate(); //(ControlStatus *, MergedDirective *);
};

#endif
