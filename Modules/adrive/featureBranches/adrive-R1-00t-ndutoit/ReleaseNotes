              Release Notes for "adrive" module

Release R1-00t (Fri May  4  7:16:28 2007):
	Added logic to set both throttle and brake appropriately when
	commanding an acceleration in hardware.  This fixes the problem of
	having both the brake and throttle on at the same time.

	Note: this code can only be tested on Alice (since the hardware has
	to be present).  Don't forget to use the '-g' option to disable the
	throttle (gas) and the '-b' option to disable the brake if you are
	running with the switches for one or the other turned off at the
	driver's station.  Failure to do this will cause gcdrive not to
	command changes to either of them.

Release R1-00s (Thu May  3 11:12:47 2007):
	* Added comments and clean up the logger for ActuatationInterface
	* Decrease the sleep in SteeringModule from 5000 to 1000. (5000 is ok when running on my mac but when I ran it on a lab machine, the steering module dropped quite a lot of directives.)

Release R1-00r (Thu May  3  5:25:09 2007):
	* Fixed the delay problem when running with trajfollower --gcdrive. ActuationInterface now uses the lightweight GcInterface and is no longer a gcmodule.
	* Added command line option for setting log level. If log level > 0, 4 log files will be created. By default, the name of the log file looks like 2007-05-03-Thu-05-39-gcdrive-{acceleration, actuationInterface, steering, transmission}.log.
	** Log level 1: all the error messages and responses whose status is either FAILED or REJECTED.
	** Log level 2: all the id's of received direcrtives with timestamp + everything from log level 1
	** Log level 7: clean up in GcInterface + everything from log level 2
	** Log level 8: all the received and output directives and responses with timestamp + everything from log level 7
	** Log level 9: all the merged directives and control status + everything from log level 8 

Release R1-00q (Sun Apr 29 20:17:43 20:07):
	Initial implementation of Pause directive.  Currently fairly limited
	functionality: press the brake and blocks any other commands.  This
	directive is intended primarily for internal use (eg, so that
	Transmission can prevent motion during shifting).

Release R1-00p (Sun Apr 29 14:50:07 2007):
	Fixed estop status; this is now read from hardware and broadcast via
	the ActuatorState.  If you disable the estop (using
	--disable-estop), the status will return as estopRun (2).

Release R1-00o (Thu Apr 26 23:22:52 2007):
	* Fixed all sorts of segfault problems (Yay Josh, we made it!!!)
	* Limit the number of directives/responses stored in GcInterface to 10
	* Use in-process communication
	* Still need to figure out the bad ID problem

Release R1-00n (Thu Apr 26  7:26:12 2007):
	Updated to work with the latest release of GcModule (R1-01a) and
	also to use faster rates for responding to steering and acceleration
	directives (up to 100 Hz).  There is also a new unit test,
	UT_timing, that can be used to send directives at a high rate.  This
	unit test currently produces a core dump, possible due to the fact
	that GcModule is not yet flusing its queue.

Release R1-00m: aborted

Release R1-00l (Sun Apr 22 22:52:10 2007):
	This version has been tested in Alice.  Steering, brake, throttle
	and transmission all work.  There appear to be some issues with
	estop, but these can't be checked until the estop is working again.
	Interlocks are still not implemented, but this version should be
	fine for basic testing of trajfollower.

	This version also has the AdriveCommand interface moved to the
	gcinterfaces module.

Release R1-00k (Sun Apr 22  8:59:50 2007):
	Added the brake, gas and transmission actuators, plus the estop
	"sensor".  There are currently no interlocks, so the actuators just
	do what you tell them.  Directives are rejected if the commands are
	not in range.  All actuators work with asim when the --simulate flag
	is given.  Steering and estop have been tested on Alice (brake, gas
	and transmission will be tested later today).

Release R1-00j (Sat Apr 21 18:10:17 2007):
	This release of adrive has been tested on Alice and is able to
	properly command and read the steering angle.

Release R1-00i (Sat Apr 21 11:29:44 2007):
	This release of the adrive module updates the GcModule 
	implementation (gcdrive) to use the latest release of gcmodule and
	gcinterface (gcmodule-R1-00j and gcinterfaces-R1-00f).  It also
	fixes up some bugs in Makefile.yam so that the display tables get
	handled correctly.  Everything should work the same as before from
	an interface and user perspective.

Release R1-00h (Wed Apr 18 20:58:35 2007):
	This relaese of the adrive module has a first cut at a gcmodule
	implementation of adrive, called 'gcdrive'.  At present, gcdrive can
	only handle steering commands and has not yet been verified to work
	on Alice hardware.  However, it can be used in "simulation" mode, in
	which case it forwards commands from the GcInterface to asim.

	To use 'gcdrive' in this mode, run 'gcdrive --simulate'.  You can
	set the steering angle from the sparrow display or you can use the
	unit test UT_adrive to send a command.  If you run asim at the same
	time, you should see the command come through.

	This version requires gcmodule-R1-00i as well as sparrow-R1-00e in
	order to compile properly.  The 'adrive' program is unchanged and
	should function as before.

Release R1-00g (Sat Mar 17 19:46:47 2007):
	Added better error logging (added cerr output to logger function)
	and also fixed a latent bug in the way gas commands are handled
	(wasn't checking if transmission was enabled before looking at
	whether we are in park; doesn't matter unless interlocks are on).

	This version should be ready for field testing.  Make sure that
	interlocks are off in the config file (this is the default),
	otherwise you won't be able to command throttle.

Release R1-00f (Sat Mar  3  9:29:32 2007):
	Updated to use --skynet-wkey + latest version of interfaces

Release R1-00e (Tue Feb 20 15:45:27 2007):
	Fixed include statements so this revision of adrive compiles against
	revision R2-00a of the interfaces and the skynet modules 

Release R1-00d (Sat Feb 10 19:39:11 2007):
	Added printout of reason for switching into adrive pause.  These
	will print on the bottom of the sparrow screen => should help in
	figuring out why adrive is paused.

	Also put in configuration file search using dgcFindConfigFile.

Release R1-00c (Sat Jan 20 14:22:28 2007):
	This is a 'stable' version of adrive that has been tested on 
	Alice and seen to work.  This version also addresses bug
	3079 (hang on missing config file).

Release R1-00b (Sat Jan 13 20:49:35 2007):
	Minor release to use new interface structure (Actuator*.h)

Release R1-00a (Wed Jan 10 23:12:00 2007):
	Initial working version of adrive under YaM.  This version has
	the actuators copied into the adrive module (they aren't used
	elsewhere) and has the timber logger commented out (will remove
	permanently later).  The module function corresponds to the most
	recent release from dgc/trunk, including a new summary display
	page (old page available via a button at the bottom of screen).

Release R1-00 (Wed Jan 10 17:32:47 2007):
	Created.





















