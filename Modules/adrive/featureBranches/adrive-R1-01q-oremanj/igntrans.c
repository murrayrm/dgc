/*!
 *\file igntrans.c
 *\low level driver for the transmission actuator
 * 
 *\author Noele Norris
 *\date 11 July 2007
 *
 *uses PLC, communicating through Modubs TCP/IP protoc
 */
  

#include <stdlib.h>
#include <errno.h>
#include <pthread.h>

#include "plctools/plc.h"
#include "igntrans.h"

static modbus_t *mb_write; /* modbus object to set transmission positions */
static modbus_t *mb_read;  /* modbus object to read transmission positions */

/*Mutex to control access to mb_write and mb_read*/
static pthread_mutex_t mtx_write = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mtx_read = PTHREAD_MUTEX_INITIALIZER;

/*opens transmission on the Modbus devide connected at the given ip addresss.*/
/*Returns 0 on succes, -1 on failure*/
int igntrans_open (const char *ip_addr) 
{
    /*Creates modbus write object*/
    pthread_mutex_lock (&mtx_write);
    mb_write = modbus_create (ip_addr);
    pthread_mutex_unlock(&mtx_write);
    if (!mb_write) return -1;
    
    /*Creates modbus read object*/
    pthread_mutex_lock (&mtx_read);
    mb_read = modbus_create (ip_addr);
    pthread_mutex_unlock(&mtx_read);
    if(!mb_read) return -1; 
    
    return 0;  /*no errors*/
}

/*initializes communication by transmission by ensuring that getting data*/
int igntrans_init()
{
    if(trans_getposition() == -1)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}


//closes modbus connection to transmission
//returns 0 on succes, -1 on failure
void igntrans_close()
{
    pthread_mutex_lock (&mtx_write);
    if (mb_write) {
        modbus_destroy (mb_write);
    }
    mb_write = NULL;
    pthread_mutex_unlock (&mtx_write);
 
    pthread_mutex_lock (&mtx_read);
    if (mb_read) {
        modbus_destroy (mb_read);
    }
    mb_read = NULL;
    pthread_mutex_unlock(&mtx_read);
}    

        
int ign_setposition (int position)
{
    return -1;
}

int ign_getposition (void)
{
    return -1;
}


/*Sets the position of the transmission
*ERC that controls transmission has four positions based on the signals
*from four wires*/

int trans_setposition (int position)
{
    if(!mb_write) return -1;
    
    unsigned char relays[3];
        
    switch (position)
    {
        case T_PARK:   /*Position 1 on ERC*/
            relays[0] = 1;
            relays[1] = 0;
            relays[2] = 0;              
            break;
        case T_REVERSE: /*Position 2 on ERC*/
            relays[0] = 1;
            relays[1] = 1;
            relays[2] = 0;  
            break;
        case T_NEUTRAL: /*Position 3 on ERC*/
            relays[0] = 1;
            relays[1] = 0;
            relays[2] = 1;  
            break;
        case T_DRIVE:   /*Position 4 on ERC*/
            relays[0] = 1;
            relays[1] = 1;
            relays[2] = 1;  
            break;
        default:
            return -1;
    }
    
    if (!modbus_write_coils (mb_write, MODBUS_COIL_DO(4), 3, relays)) {
        return -1;
    }

    return 0;
}



/*Returns current position of the transmission*/
int trans_getposition()
{
    int position;
    unsigned char relays[3];
    
    if (!modbus_read_coils (mb_read, MODBUS_COIL_DO(4), 3, relays))
        return -1;
 
    if (relays[0] == 1 && relays[1] == 0 && relays[1] == 0) position = T_PARK;
    else if (relays[0] == 1 && relays[1] == 1 && relays[1] == 0) position = T_REVERSE;
    else if (relays[0] == 1 && relays[1] == 0 && relays[1] == 1) position = T_NEUTRAL;
    else if (relays[0] == 1 && relays[1] == 1 && relays[1] == 1) position = T_DRIVE;
    else return -1;

    return position;
}
