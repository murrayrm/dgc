/**
 * \file brake.h
 * \brief Low-level brake interface for Alice.
 *
 * \author Joshua Oreman
 * \date 11 July 2007
 *
 * This file declares functions that access the PLC via Modbus to
 * control Alice's brake. The API is largely based on the old brake
 * code.
 */

#ifndef __BRAKE_H__
#define __BRAKE_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Opens the brake by connecting to the PLC at the given IP address.
 *
 * \param ip_addr The IP address to connect to, in dotted decimal format.
 *                This should probably be PLC_ADDR_LAB ("192.168.1.75")
 *                or PLC_ADDR_ALICE ("192.168.0.75"). You may \e not use
 *                a hostname.
 * \return 0 for success, -1 and sets \c errno on error.
 */
int brake_open (const char *ip_addr);

/**
 * \brief Initializes the brake for operation.
 *
 * This currently just sets the brake position to full and does a sanity check
 * by attempting to read it back. It is provided primarily for compatibility
 * with other actuator drivers.
 *
 * \return 0 if the sanity check passed, -1 and sets \c errno if it failed.
 */
int brake_init (void);

/**
 * \brief Closes the connection to the brake.
 *
 * After calling this function, all brake functions except brake_open() will
 * return an error until brake_open() is called successfully.
 */
void brake_close (void);

/**
 * \brief Sets the brake position by actuating a solenoid to actually push the pedal.
 *
 * \param pos The position to set. This should be a floating-point value between 0.0
 *            and 1.0; 0.0 is no positive acceleration, while 1.0 is full brake.
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int brake_setposition (double pos);

/**
 * \brief Gets the last good brake position.
 *
 * This retrieves the position commanded by the <i>last values sent to the PLC</i> - it
 * does \e not necessarily indicate whether the braking succeeded (though the only
 * exceptions are cases of grievous mechanical error).
 *
 * If the device cannot be accessed, returns BRAKE_POS_ERROR and sets \c errno.
 *
 * It is safe to test the return value for equality against this value.
 */
double brake_getposition (void);

/**
 * \brief Gets the current brake pressure.
 *
 * This queries the PLC for the voltage being put out by the brake pressure sensor.
 * It provides a more accurate view of brake pressure than brake_getposition(), since
 * a high pressure value ensures that the brake was actually pressed.
 *
 * \return A pressure value in the range [0.0, 1.0] on a linear scale where each 0.1 is
 * about 312.5 PSI. Usual pressure values are between 0.059 and 0.784. On error, returns
 * BRAKE_POS_ERROR.
 */
double brake_getpressure (void);

#define BRAKE_POS_ERROR -1.0

#ifdef __cplusplus
}
#endif

#endif /* __BRAKE_H__ */
