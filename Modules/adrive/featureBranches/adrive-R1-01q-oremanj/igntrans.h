/**
* \file igntrans.h
*\interface for low level driver for transmission and ignition
*
*\author Noele Norris
*\date 11 July 2007
*
*The functions use the Modbus protocol to access the PLC, which controls
*Alice's transmission.  The interface follows that of igntrans.hh, old
*transmission code.
*
*IGNITION????
*/

#ifndef __IGNTRANS_H__
#define __IGNTRANS_H__

#ifdef __cplusplus
extern "C" {
#endif

//used for trans_getposition
#define TRANS_ERROR -2

//transmission position states
#define T_PARK 0
#define T_REVERSE -1
#define T_NEUTRAL 2
#define T_DRIVE 1

//ignition position states
#define I_OFF 0
#define I_RUN 1
#define I_START 2


/**
*\Opens access to the transmission by connecting to the PLC at given
*IP address.
*
*\param ip_addr The IP address in dotted decimal format.
 *               USE PLC_ADDR_LAB or PLC_ADDR_ALICE
 *
 *\return 0 for succes, -1 for failure
*/
int igntrans_open (const char *ip_addr);


/**
*\Ensures that communicating with PLC by reading the current position of
*the transmission.
*
*\return 0 for success, -1 for failure
*/
int igntrans_init();

/**
*\Closes connection to the transmission.
*
*/
void igntrans_close();
 
      
int ign_setposition (int position);

int ign_getposition (void);

/**
*\Sets the throttle position to given position (reverse, park, neutral, drive)
*
*\param  position: T_PARK, T_REVERSE, T_NEUTRAL, T_DRIVE
*
*\return 0 on success, -1 on failure
*
*/ 
int trans_setposition (int position);

/**
*\ Gets the throttle position (using outputs from PLC).
*
*\return -1 on failure to receive good position, otherwise returns
*        T_PARK, T_REVERSDE, T_NEUTRAL, T_DRIVE
*/
int trans_getposition();

#ifdef __cplusplus
}
#endif

#endif /* __THROTTLE_H__ */


    
   
