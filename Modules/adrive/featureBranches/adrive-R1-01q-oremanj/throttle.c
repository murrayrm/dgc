/**
 * \file throttle.c
 * \brief Low-level throttle code for Alice.
 *
 * \author Joshua Oreman
 * \date 10 July 2007
 *
 * This file defines functions used by gcdrive to access and manipulate
 * Alice's throttle (gas pedal).
 */

#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <pthread.h>

#include "plctools/plc.h"
#include "throttle.h"

/** Modbus object used for setting voltages for the throttle. */
static modbus_t *mb_command;

/** Modbus object used for getting voltages for the throttle. Two objects
    are used to remove any necessity of blocking between a writer thread
    and a reader thread. (The Modbus API is reentrant, but not thread-safe;
    using one object for both reads and writes would necessitate a mutex.) */
static modbus_t *mb_stat;

/** Mutex to control access to mb_command. */
static pthread_mutex_t mtx_command = PTHREAD_MUTEX_INITIALIZER;

/** Mutex to control access to mb_stat. Note that it is impossible
    for these to deadlock, as a function only holds one at a time. */
static pthread_mutex_t mtx_stat = PTHREAD_MUTEX_INITIALIZER;

int throttle_open (const char *ip_addr) 
{
  /* Create the command object... */
  pthread_mutex_lock (&mtx_command);
  mb_command = modbus_create (ip_addr);
  pthread_mutex_unlock (&mtx_command);
  if (!mb_command) return -1;
  
  /* ... and the status reader object... */
  pthread_mutex_lock (&mtx_stat);
  mb_stat = modbus_create (ip_addr);
  pthread_mutex_unlock (&mtx_stat);
  if (!mb_stat) return -1;
  
  /* Looks like both were created OK. */
  return 0;
}

int throttle_init() 
{
  double pos;
  
  /* Set the position to 0. */
  if (throttle_setposition (0.0) < 0)
    return -1;
  
  /* Read the position back. */
  if ((pos = throttle_getposition()) < 0)
    return -1;
  
  /* Close enough? (these are floats, we can't test equality) */
  if (fabs (pos) > 0.01)
    return -1;
  
  /* Looks good. */
  return 0;
}

void throttle_close() 
{
  /* Just kill off the Modbus objects. */
  pthread_mutex_lock (&mtx_command);
  if (mb_command) modbus_destroy (mb_command);
  mb_command = NULL;
  pthread_mutex_unlock (&mtx_command);
  
  pthread_mutex_lock (&mtx_stat);
  if (mb_stat) modbus_destroy (mb_stat);
  mb_stat = NULL;
  pthread_mutex_unlock (&mtx_stat);
}

/* Reference voltage (Vref). Everything else is a percentage of this. */
#define VREF 5.0
/* Min/max voltages for APPS sensors 1/2/3. These are at the *edges* of the
   acceptability curve, not the centers. Don't use these for setposition! */
#define APPS1_MIN (.1497*VREF)
#define APPS1_MAX (.8285*VREF) /* note that APPS1 is reversed - it gets its max at idle, not full */
#define APPS2_MIN (.2760*VREF)
#define APPS2_MAX (.8012*VREF)
#define APPS3_MIN (.1660*VREF)
#define APPS3_MAX (.6912*VREF)
/* Idle voltages for APPS sensors 1/2/3. */
#define APPS1_IDLE (.8075*VREF)
#define APPS2_IDLE (.2970*VREF)
#define APPS3_IDLE (.1870*VREF)
/* Slope of each APPS sensor's input, in volts per degree of pedal depression. */
#define APPS1_SLOPE (-.0398*VREF)
#define APPS2_SLOPE (.0302*VREF)
#define APPS3_SLOPE (.0302*VREF)
/* Degrees of pedal depression corresponding to full throttle. */
#define PEDAL_MAXANGLE  15.25
/* Voltage range of APPS sensors 1/2/3. */
#define APPS1_RANGE (APPS1_SLOPE * PEDAL_MAXANGLE) /* negative */
#define APPS2_RANGE (APPS2_SLOPE * PEDAL_MAXANGLE)
#define APPS3_RANGE (APPS3_SLOPE * PEDAL_MAXANGLE)
/* Full throttle voltages for APPS sensors 1/2/3. */
#define APPS1_FULL (APPS1_IDLE + APPS1_RANGE)
#define APPS2_FULL (APPS2_IDLE + APPS2_RANGE)
#define APPS3_FULL (APPS3_IDLE + APPS3_RANGE)

static const float APPS_MIN[3] = { APPS1_MIN, APPS2_MIN, APPS3_MIN };
static const float APPS_MAX[3] = { APPS1_MAX, APPS2_MAX, APPS3_MAX };
static const float APPS_IDLE[3] = { APPS1_IDLE, APPS2_IDLE, APPS3_IDLE };
static const float APPS_FULL[3] = { APPS1_FULL, APPS2_FULL, APPS3_FULL };
static const float APPS_RANGE[3] = { APPS1_RANGE, APPS2_RANGE, APPS3_RANGE };
static const float APPS_SLOPE[3] = { APPS1_SLOPE, APPS2_SLOPE, APPS3_SLOPE };

int throttle_setposition (double pos) 
{
  /*
   * Throttle uses three voltages; they have to be synchronized
   * properly for the commanded acceleration to take effect.
   *
   * To avoid the voltages jumping asynchronously and frightening
   * the Power Control Module into "limp-home mode", we write all
   * six contiguous registers (two per voltage) with one Modbus
   * command. The floats are in an array to facilitate this.
   */
  float apps[3];
  int res;
  
  apps[0] = APPS1_IDLE + APPS1_RANGE * pos;
  
  apps[1] = APPS2_IDLE + APPS2_RANGE * pos;
  
  apps[2] = APPS3_IDLE + APPS3_RANGE * pos;
  
  /*
   * Write the registers. The PLC is little-endian just like us, so we can write the
   * floats as-is.
   */
  
  pthread_mutex_lock (&mtx_command);
  if (!mb_command)
    errno = EFAULT, res = 0;
  else
    res = modbus_write_registers (mb_command, MODBUS_REG_AO(0), 6, (modbus_reg_t *)apps);
  pthread_mutex_unlock (&mtx_command);
  
  if (!res)
    return -1;
  
  return 0;
}

double throttle_getposition() 
{
  /* Values read from peeking the APPS output voltages on the PLC. */
  float apps[3];
  /* The average of those values. */
  double apps_avg;
  /* Those values scaled to the [0, 1] range. */
  double pos[3];
  /* Average of the component_pos[i] values. */
  double pos_avg;
  /* Misc. */
  int i, res;
  
  /* Read 'em in. */
  pthread_mutex_lock (&mtx_stat);
  if (!mb_stat)
    errno = EFAULT, res = 0;
  else
    res = modbus_read_registers (mb_stat, MODBUS_REG_AO(0), 6, (modbus_reg_t *)apps);
  pthread_mutex_unlock (&mtx_stat);
  
  if (!res)
    return THROTTLE_POS_ERROR;
  
  /* Various sanity checks. */
  for (i = 0; i < 3; i++) {
    /* Can't be greater than maximum or less than minimum. */
    if (apps[i] > APPS_MAX[i] || apps[i] < APPS_MIN[i])
      return THROTTLE_POS_INVALID;
    
    /* Scale it to [0, 1]. */
    pos[i] = (apps[i] - APPS_IDLE[i]) / APPS_RANGE[i];
  }
  
  /* Average apps and double-check it. */
  apps_avg = (apps[0] + apps[1] + apps[2]) / 3.0;
  for (i = 0; i < 3; i++) {
    /* The difference between apps[i] and apps_avg should not
       exceed 3.2% of Vref. */
    if (fabs ((double)apps[i] - apps_avg) > .032*VREF)
      return THROTTLE_POS_INVALID;
  }
  
  /* Looks good. Average the positions and return them. */
  pos_avg = (pos[0] + pos[1] + pos[2]) / 3.0;
  
  /* Normalize return value to [0, 1]. */
  if (pos_avg < 0.0) pos_avg = 0.0;
  if (pos_avg > 1.0) pos_avg = 1.0;
  
  return pos_avg;
}

/* Local Variables: */
/* mode: c */
/* indent-tabs-mode: nil */
/* c-basic-offset: 2 */
/* End: */
