/**
 * \file brake.c
 * \brief Low-level brake code for Alice.
 *
 * \author Joshua Oreman
 * \date 10 July 2007
 *
 * This file defines functions used by gcdrive to access and manipulate
 * Alice's brake (gas pedal).
 */

#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <pthread.h>

#include "plctools/plc.h"
#include "brake.h"

/** Modbus object used for setting voltages for the brake. */
static modbus_t *mb_command;

/** Modbus object used for getting voltages for the brake. Two objects
    are used to remove any necessity of blocking between a writer thread
    and a reader thread. (The Modbus API is reentrant, but not thread-safe;
    using one object for both reads and writes would necessitate a mutex.) */
static modbus_t *mb_stat;

/** Mutex to control access to mb_command. */
static pthread_mutex_t mtx_command = PTHREAD_MUTEX_INITIALIZER;

/** Mutex to control access to mb_stat. Note that it is impossible
    for these to deadlock, as a function only holds one at a time. */
static pthread_mutex_t mtx_stat = PTHREAD_MUTEX_INITIALIZER;

int brake_open (const char *ip_addr) 
{
  /* Create the command object... */
  pthread_mutex_lock (&mtx_command);
  mb_command = modbus_create (ip_addr);
  pthread_mutex_unlock (&mtx_command);
  if (!mb_command) return -1;
  
  /* ... and the status reader object... */
  pthread_mutex_lock (&mtx_stat);
  mb_stat = modbus_create (ip_addr);
  pthread_mutex_unlock (&mtx_stat);
  if (!mb_stat) return -1;
  
  /* Looks like both were created OK. */
  return 0;
}

#define BRAKE_MIN   0
#define BRAKE_MAX   15
#define BRAKE_RANGE 16

int brake_init()
{
  double pos;
  
  /* Set the position to 1. */
  if (brake_setposition (1.0) < 0)
    return -1;
  
  /* Read the position back. */
  if ((pos = brake_getposition()) < 0)
    return -1;
  
  /* Close enough? (brake only has 16 gradations) */
  if ((int)(fabs (pos) * BRAKE_RANGE) != BRAKE_MAX)
    return -1;
  
  /* Looks good. */
  return 0;
}

void brake_close() 
{
  /* Just kill off the Modbus objects. */
  pthread_mutex_lock (&mtx_command);
  if (mb_command) modbus_destroy (mb_command);
  mb_command = NULL;
  pthread_mutex_unlock (&mtx_command);
  
  pthread_mutex_lock (&mtx_stat);
  if (mb_stat) modbus_destroy (mb_stat);
  mb_stat = NULL;
  pthread_mutex_unlock (&mtx_stat);
}

int brake_setposition (double pos) 
{
  /*
   * Brake uses four digital outputs, encoding pos into a
   * 16-gradation thing.
   */
  unsigned char outs[4];
  int ipos;
  int res;

  if (pos < 0.0 || pos > 1.0) {
    errno = EINVAL;
    return -1;
  }

  /*
   * Note that this causes anything between 0.0 and 0.0625 to be "no braking",
   * and anything between 0.9375 and 1.0 to be "full".
   */
  ipos = (int)(pos * BRAKE_RANGE);
  if (ipos < BRAKE_MIN) ipos = BRAKE_MIN;
  if (ipos > BRAKE_MAX) ipos = BRAKE_MAX;
  
  /* Set outs[0,1,2,3] to bits 0,1,2,3 of ipos, respectively. */
  outs[0] = ipos & 1;
  outs[1] = ipos & 2;
  outs[2] = ipos & 4;
  outs[3] = ipos & 8;
  
  pthread_mutex_lock (&mtx_command);
  if (!mb_command)
    errno = EFAULT, res = 0;
  else
    res = modbus_write_coils (mb_command, MODBUS_COIL_DO(0), 3, outs);
  pthread_mutex_unlock (&mtx_command);
  
  if (!res)
    return -1;
  
  return 0;
}

double brake_getposition() 
{
  unsigned char outs[4];
  int ipos, res;
  
  /* Read 'em in. */
  pthread_mutex_lock (&mtx_stat);
  if (!mb_stat)
    errno = EFAULT, res = 0;
  else
    res = modbus_read_coils (mb_stat, MODBUS_COIL_DO(0), 3, outs);
  pthread_mutex_unlock (&mtx_stat);
  
  if (!res)
    return BRAKE_POS_ERROR;

  /* Convert the four read bits into one four-bit value. */
  ipos = 0;
  if (outs[0]) ipos |= 1;
  if (outs[1]) ipos |= 2;
  if (outs[2]) ipos |= 4;
  if (outs[3]) ipos |= 8;
  
  /* And convert *that* to a float. Note that because of the quantization
     method used, a set value of 1.0 will be returned as 15/16 (0.9375). */
  return (double)ipos / (double)BRAKE_RANGE;
}

double brake_getpressure()
{
  float val;
  int res;
  
  /* Read it. */
  pthread_mutex_lock (&mtx_stat);
  if (!mb_stat)
    errno = EFAULT, res = 0;
  else
    res = modbus_read_analog (mb_stat, 0, &val);
  pthread_mutex_unlock (&mtx_stat);
  
  val /= 5.0;
  if (val < 0.0 || val > 1.0)
    return -1;

  return val;
}

/* Local Variables: */
/* mode: c */
/* indent-tabs-mode: nil */
/* c-basic-offset: 2 */
/* End: */
