/**
 * \file throttle.h
 * \brief Low-level throttle interface for Alice.
 *
 * \author Joshua Oreman
 * \date 10 July 2007
 *
 * This file declares functions that access the PLC via Modbus to
 * control Alice's throttle. The API is largely based on the old
 * throttle code.
 */

#ifndef __THROTTLE_H__
#define __THROTTLE_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Opens the throttle by connecting to the PLC at the given IP address.
 *
 * \param ip_addr The IP address to connect to, in dotted decimal format.
 *                This should probably be PLC_ADDR_LAB ("192.168.1.75")
 *                or PLC_ADDR_ALICE ("192.168.0.75"). You may \e not use
 *                a hostname.
 * \return 0 for success, -1 and sets \c errno on error.
 */
int throttle_open (const char *ip_addr);

/**
 * \brief Initializes the throttle for operation.
 *
 * This currently just sets the throttle position to 0 and does a sanity check
 * by attempting to read it back. It is provided primarily for compatibility
 * with other actuator drivers.
 *
 * \return 0 if the sanity check passed, -1 and sets \c errno if it failed.
 */
int throttle_init (void);

/**
 * \brief Closes the connection to the throttle.
 *
 * After calling this function, all throttle functions except throttle_open() will
 * return an error until throttle_open() is called successfully.
 */
void throttle_close (void);

/**
 * \brief Sets the throttle position, akin to changing the angle of the gas pedal.
 *
 * \param pos The position to set. This should be a floating-point value between 0.0
 *            and 1.0; 0.0 is no positive acceleration, while 1.0 is full throttle.
 * \return 0 on success, -1 and sets \c errno on failure.
 */
int throttle_setposition (double pos);

/**
 * \brief Gets the last good throttle position.
 *
 * This retrieves the position commanded by the <i>last voltages sent to the PLC</i> - it
 * does \e not necessarily indicate whether the acceleration succeeded (though the only
 * exceptions are cases of grievous mechanical error).
 *
 * If the three voltages read are not properly synchronized with each other, returns
 * THROTTLE_POS_INVALID.
 *
 * If the device cannot be accessed, returns THROTTLE_POS_ERROR and sets \c errno.
 *
 * It is safe to test the return value for equality against these values.
 */
double throttle_getposition (void);

#define THROTTLE_POS_ERROR   -1.0
#define THROTTLE_POS_INVALID -2.0

#ifdef __cplusplus
}
#endif

#endif /* __THROTTLE_H__ */
