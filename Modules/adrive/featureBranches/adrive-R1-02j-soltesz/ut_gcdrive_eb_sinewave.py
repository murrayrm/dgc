#! /usr/bin/python -i

import os;
import sys;

import sysTestLaunch

start = '1.1.1'
scenario = 'santaanitaBigIntersectionblock'
rddf = '../../src/rddfplanner/UT_sinewave.rddf'
mdf = '../../etc/routes-santaanita/santaanita_sitevisit_right.mdf'
obsFile = 'santaanitaBigIntersectionblock.obs'
sceneFunc = ''

apps =  [ \
	('asim', '--no-pause --rddf=%(rddf)s --gcdrive' % locals(), 1, 'localhost'), \
	('rddfplanner',  '--verbose=3', 1 , 'localhost'), \
#	('gcfollower',  '', 1 , 'leela'), \
	('gcfollower',  '--use-local', 1 , 'sydney'), \
#	('gcdrive-event-based',  '--auto-shift --steer-speed-threshold=2 --pause-braking=0.9', 1, 'leela'), \
#	('gcdrive-event-based',  '--simulate --auto-shift --steer-speed-threshold=2 --pause-braking=0.9', 1, 'sydney'), \
	('gcdrive-event-based',  '--log-level=10 --log-file=/tmp/logs/gcdrive --simulate --auto-shift --steer-speed-threshold=2 --pause-braking=0.9', 1, 'sydney'), \
	('mapviewer', '--recv-subgroup=-2', 1, 'localhost'), \
#	('planviewer2', '', 1, 'localhost'), \
	('UT_fwdbwd',  '%(rddf)s --use-local' % locals(), 1 , 'localhost'), \
#	('UT_fwdbwd',  '%(rddf)s' % locals(), 1 , 'localhost'), \
]
sysTestLaunch.runApps( apps, scenario, obsFile, rddf, mdf, sceneFunc, False )
