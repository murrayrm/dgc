/*!
 * \file EstopSensor.cc
 * \brief Read estop hardware
 *
 * \author Richard Murray
 * \date 22 April 2007
 *
 * This file contains the routines required to read and update the
 * estop status.  This is not a gcmodule, just a thread that keeps
 * track of the output from the estop unit (roughly the status loop
 * part of an actuator module).
 *
 */

#include "EstopSensor.hh"
#include "estop.hh"

/*
 * EstopSensor::EstopSensor
 *
 * This constructor initializes the EstopSensor class, including
 * establishing connection to the hardware.
 *
 */

EstopSensor::EstopSensor(ActuationInterface *actuationInterface)
{
  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * Hardware/simulation initailization
   *
   * All we have to do here is open up the serial port, since the
   * estop box continuously sends out commands.  If we are running in
   * simulation mode, we don't bother trying to talk to the hardware.
   */

  if (adrive->options->simulate_given) {
    /* Simulation mode - no action required */
    initialized = 1;
    connected = 1;

  } else {
    /* Open the port to the estop box */
    if (!estop_open(adrive->options->estop_port_arg)) {
      if (adrive->options->disable_estop_flag) {
	/* Estop has been disabled, so allow the error to pass */
	connected = 0;
	initialized = 1;
#       warning set currentStatus to run?
      } else {
	/* Don't allow operation without being able to talk to estop */
	cerr << "EstopSensor: estop_open failed" << endl;
	exit(1);
      }
    } else {
      /* Keep track of the fact that we are connected */
      connected = 1;
      initialized = 1;
    }
  }

  /*
   * Status thread startup
   *
   * As part of the EstopSensor function, we start up a thread to read
   * the status of the estop box on a periodic basis.  This thread
   * fills in data in the EstopSensor class, so we have easy access to
   * this information.
   */

  statusCount = 0;
  statusSleepTime = 500000;
  DGCstartMemberFunctionThread(this, &EstopSensor::status);

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&sensorMutex);
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void EstopSensor::initSparrow(DD_IDENT *tbl, int label_id)
{
  /* Save the label for later usage */
  sparrowLabel = label_id;
  sparrowTable = tbl;

  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the estop box */
      dd_setcolor_tbl(label_id, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to estop box, but couldn't initialize */
      dd_setcolor_tbl(label_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(label_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("estop.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("estop.dstoppos", &currentStatus, tbl);
  }
}

/*
 * EstopSensor::status
 *
 * This function is called as a thread and is used to read the status
 * of the estop box.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void EstopSensor::status()
{
  while (1) {
    if (adrive->options->simulate_given) {
      /* In simulation mode, read state from asim */
      currentStatus = adrive->state.m_dstoppos;

      /* Fill the status based on local information */
      adrive->state.m_estopstatus = initialized && connected;

    } else {
      if (!connected) {
	/* Estop is currently disabled, so return disabled */
#       warning check to see if disabled; if so, put in run?
	currentStatus = EstopDisable;

      } else {
	/* Read the current status */
	int retval = estop_status();

	/* Check the status and set state appropriately */
	if (retval == -1) {
	  /* Error condition */
#         warning estop status error not yet handled
	  cerr << "EstopSensor::status error reading steering position"
	       << endl;
	  sleep(1);

	} else {
	  currentStatus = retval;
	}
      }
      /* Now fill up the actuatorState struct with current state */
      adrive->state.m_estopstatus = initialized && connected;
      adrive->state.m_dstoppos = currentStatus;
    }

    /* Now that we have the state, check to see if there are any problems */
    /* TODO: implement something here */

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}
