#include "AdriveEventLogger.hh"
#include "dgcutils/DGCutils.hh"

AdriveEventLogger::AdriveEventLogger(char * logging_path){
    
  logfile.open (logging_path, fstream::out | fstream::app);
  event_enabled = false;
}

AdriveEventLogger::~ AdriveEventLogger(){
  logfile.close();  
}

int AdriveEventLogger::operator<<(string input_string){
  char datestamp[255];
  time_t t = time(NULL);
  tm *local;

  /* Send a copy of the message to stderr (which will show up in sparrow) */
  cerr << input_string << endl;

  if (event_enabled) {
      DGCgettime(current_time);
      local = localtime(&t);
      sprintf(datestamp, "%04d_%02d_%02d %02d:%02d:%02d",
	      local->tm_year+1900, local->tm_mon+1, local->tm_mday, 
	      local->tm_hour, local->tm_min, local->tm_sec);

      logfile << current_time << '\t' << datestamp  << '\t' << input_string << endl;
      return 1;
    }
  else return -1;
}

int AdriveEventLogger::enable(){
  event_enabled = true;
  return 1;
}

int AdriveEventLogger::disable(){
  event_enabled = false;
  return 1;
}


AdriveEventLogger event("adrive.event");
AdriveEventLogger estop_log("adrive.estop");
