#! /bin/bash
# UT_options.sh - unit test for adrive option handling
# Richard Murray, 25 April 2007
# 
# This unit test calls adrive with a number of different command line
# options and verifies that the proper processing occurs.  Since we
# can't really look at the sparrow display, most of the testing is
# just to see whether we get errors when we are supposed to.

ADRIVE="../../bin/Drun gcdrive"
YAM_TARGET=`yamNative.sh`

# Generate a bad call to a serial port and make sure we abort properly
echo "Steering open error: try to open a bad port; should fail -"
$ADRIVE --steer-port=5000 --disable-console --test-mode
if [ $? == 0 ]; then echo "passed"; else echo "FAILED"; fi

# Bypass the steering port and make sure the next port fails
echo "Brake open error: bypass steering, but bad brake port; should fail -"
$ADRIVE --steer-port=5000 -s --brake-port=5001 --disable-console --test-mode
if [ $? == 0 ]; then echo "passed"; else echo "FAILED"; fi

# Basic test: call adrive with simulate option and run simple unit test
echo -n "Simulation test: run simuation + unit test - "
($ADRIVE --simulate --disable-console --test-mode --verbose) &
sleep 1
$YAM_TARGET/UT_adrive
if [ $? == 0 ]; then echo "passed"; else echo "FAILED"; fi
