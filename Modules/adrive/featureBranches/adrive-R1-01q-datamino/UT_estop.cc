/*!
 * \file UT_estop.cc
 * \brief Unit test for the estop interface
 *
 * \author Richard M. Murray
 * \date 28 April 2007
 *
 * This is a simple unit test that initializes the estop interface and
 * then reads from it looking for changes.
 */

#include <stdio.h>
#include <iostream>
#include <assert.h>
using namespace std;

#include "estop.hh"
#include "cmdline.h"

int main (int argc, char **argv)
{
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0)
    exit(1);

  /* Open up the estop interface */
  cout << "opening estop on port /dev/ttyS" << cmdline.estop_port_arg << endl;
  assert(estop_open(cmdline.estop_port_arg));

  /* Get the initial status */
  int current_status = estop_status();
  assert(current_status != -1);
  cout << "initial status = " << current_status << endl;

  int count = 0;
  while (1) {
    int status = estop_status();
    assert(status != -1);

    if (status != current_status) {
      /* changed state */
      cout << "changed to status " << status << endl;
      current_status = status;
      count = 0;
    } else if (count++ > 100) {
      cout << "status " << status << " unchanged" << endl;
      count = 0;
    }
  }
}


