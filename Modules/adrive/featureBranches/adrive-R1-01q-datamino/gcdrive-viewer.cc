/*!
 * \file gcdrive-viewer.cc
 * \brief display log data from gcdrive
 * 
 * \author Richard Murray
 * \date 10 June 2007
 *
 * This program allows you to playback data from skynet log files.  It
 * pops up a standard gcdrive display, but fills in the information
 * based on what it reads from skynet.
 *
 */

#include <iostream>
#include <pthread.h>

#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "skynet/skynet.hh"
#include "interfaces/sn_types.h"
#include "interfaces/VehicleState.h"
#include "interfaces/ActuatorState.h"

#include "overview.h"

/* Global variables */
skynet *skynetp;		// Skynet object
VehicleState vehState;		// Memory for storing the vehicle state
int stateCount = 0;		// number of vehicle state messages received
ActuatorState actState;		// Memory for storing the vehicle state
int actuatorCount = 0;		// number of actuator state messages received

/* Functions defined later in this file */
void *getVehicleState(void *);
void *getActuatorState(void *);

int main(int argc, char **argv)
{
  /* Skynet initialization */
  int skynet_key = skynet_findkey(argc, argv);
  dd_rebind_tbl("skynet_key", &skynet_key, overview_tbl);
  skynetp = new skynet(SNadrive, skynet_key);

  /* Initialize the main display */
  if (dd_open() < 0) { dbg_error("can't open display"); exit(-1); }
  if (dd_usetbl(overview_tbl) < 0) {
    dbg_error("can't open display table");
    exit(-1);
  }

  /* Initialize the displays for the actuators */
  DD_IDENT *tbl = overview_tbl;
  dd_rebind_tbl("steer.command", &actState.m_steercmd, tbl);
  dd_rebind_tbl("steer.position", &actState.m_steerpos, tbl);

  dd_rebind_tbl("trans.command", &actState.m_transcmd, tbl);
  dd_rebind_tbl("trans.position", &actState.m_transpos, tbl);

  dd_rebind_tbl("brake.command", &actState.m_brakecmd, tbl);
  dd_rebind_tbl("brake.position", &actState.m_brakepos, tbl);
  dd_rebind_tbl("brake.pressure", &actState.m_brakepressure, tbl);

  dd_rebind_tbl("throttle.command", &actState.m_gascmd, tbl);
  dd_rebind_tbl("throttle.position", &actState.m_gaspos, tbl);

  dd_rebind_tbl("estop.dstoppos", &actState.m_dstoppos, tbl);
  dd_rebind_tbl("estop.estoppos", &actState.m_estoppos, tbl);

  dd_rebind_tbl("obdii.engine_rpm", &actState.m_engineRPM, tbl);
  dd_rebind_tbl("obdii.engine_temp", &actState.m_EngineCoolantTemp, tbl);
  dd_rebind_tbl("obdii.wheel_speed", &actState.m_VehicleWheelSpeed, tbl);
  dd_rebind_tbl("obdii.gear_ratio", &actState.m_CurrentGearRatio, tbl);

  /* Start up threads to read information */
  pthread_t veh_thread, act_thread;
  if (pthread_create(&veh_thread, NULL, getVehicleState, NULL) != 0)
    cerr << "pthread_create failed for getVehicleState" << endl;

  if (pthread_create(&act_thread, NULL, getActuatorState, NULL) != 0)
    cerr << "pthread_create failed for getActuatorState" << endl;

  /* Display until quit */
  dd_loop();
  dd_close();
}

/*
 * Read adrive state information form skynet state
 *
 * State information is read from the skynet state message and the
 * actuator state from the actuator state message.  We start up
 * independent threads to read this information.
 *
 */

/* Get the vehicle state information */
void *getVehicleState(void *)
{
  int statesocket = skynetp->listen(SNstate, ALLMODULES);

  while (1) {
    /* Read the state data from skynetp */
    int count =
      skynetp->get_msg(statesocket, &vehState, sizeof(vehState), 0);

    /* Make sure we got the right number of bytes */
    if (count != sizeof(vehState)) stateCount = -1;

    /* Keep track of the number of times we run */
    ++stateCount;
  }

  return NULL;
}

/* Get the vehicle actuator information */
void *getActuatorState(void *)
{
  int statesocket = skynetp->listen(SNactuatorstate, ALLMODULES);

  while (1) {
    /* Read the state data from skynetp */
    int count =
      skynetp->get_msg(statesocket, &actState, sizeof(actState), 0);

    /* Make sure we got the right number of bytes */
    if (count != sizeof(actState)) actuatorCount = -1;

    /* Keep track of the number of times we run */
    ++actuatorCount;
  }

  return NULL;
}

/*
 * Unused sparrow callbacks
 *
 * These functions are used by gcdrive but not needed here
 *
 */

int adrive_setSteering_cb(long arg) { return 0; }
int adrive_resetSteering_cb(long arg) { return 0; }
int adrive_setThrottle_cb(long arg) { return 0; } 
int adrive_setBrake_cb(long arg) { return 0; }
int adrive_resetAcceleration_cb(long arg) { return 0; }
int adrive_setTrans_cb(long arg) { return 0; }
int adrive_setTurn_cb(long arg) { return 0; }
