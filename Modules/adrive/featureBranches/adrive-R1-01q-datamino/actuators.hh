/*  This is the header file for actuators.c.  
 * actuators.c is the threading interface for the steering code.  
 */

#ifndef _ACTUATOR_H
#define _ACTUATOR_H

/*! The standadized function call to execute trans command*/
int  execute_trans_command( double command );
/*! The standadized function call to execute trans status*/ 
int  execute_trans_status();
/*! The standadized function call to execute trans initialization*/
void execute_trans_init();
void execute_trans_close();

/*! The standadized function call to execute estop command*/
int execute_estop_command( double command ); 
/*! The standadized function call to execute estop status*/
int execute_estop_status();
/*! The standadized function call to execute estop initialization*/
void execute_estop_init();
void execute_estop_close();

/*! The standadized function call to execute gas command*/
int  execute_gas_command( double command );
/*! The standadized function call to execute gas status*/ 
int  execute_gas_status();
/*! The standadized function call to execute gas initialization*/
void execute_gas_init();
void execute_gas_close();

/*! The standadized function call to execute steer command*/
int  execute_steer_command( double command );
/*! The standadized function call to execute steer command*/
int  execute_steer_velocity( double command );
/*! The standadized function call to execute steer command*/
int  execute_steer_acceleration( double command );
/*! The standadized function call to execute steer status*/
int  execute_steer_status();
/*! The standadized function call to execute steer initialization*/
void execute_steer_init();
void execute_steer_close();

/*! The standadized function call to execute brake command*/
int  execute_brake_command( double command );
/*! The standadized function call to execute brake status*/
int  execute_brake_status();
/*! The standadized function call to execute brake initialization*/
void execute_brake_init();
void execute_brake_close();

/*! The standadized function call to execute obdii command*/
int  execute_obdii_command(double);
/*! The standadized function call to execute obdii status*/
int  execute_obdii_status();
/*! The standadized function call to execute obdii initialization*/
void execute_obdii_init();
/*! A function call to check if the OBDII is valid */
int obdii_is_valid();
/*! properly close the actuator */
void execute_obdii_close();


/* A simple function to return the engine condition enum defined and explained in adrive.h */
int engine_condition();
// Self contained ignition startup procedure for alice
int execute_engine_startup();


#endif //_ACTUATOR_H
