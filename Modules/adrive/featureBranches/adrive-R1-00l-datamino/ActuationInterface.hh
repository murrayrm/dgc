/*!
 * \file ActuationInterface.hh
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This class serves as the GcModule interface to adrive.  It doesn't
 * actually perform any function, but instead passes the directives
 * that it receives directly to one of the actuation modules.
 */

#ifndef __ActuationInterface_HH__
#define __ActuationInterface_HH__

#include "gcmodule/GcInterface.hh"
#include "skynet/skynet.hh"
#include "interfaces/ActuatorState.h"
#include "interfaces/ActuatorCommand.h"		// for use with simulator
#include "sparrow/display.h"
#include "gcinterfaces/AdriveCommand.hh"

#include "cmdline.h"

/*
 * GcInterfaces to the internal adrive modules
 *
 * The interfaces defined here are used to communicate with the
 * internal adrive modules for each actuator.  Since these are only
 * visible within adrive, we define them here (instead of in
 * AdriveCommand.hh).
 */

/*! Interface to the SteeringModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrive> AdriveSteerInterface;

/*! Interface to the AccelerationModule */
typedef GcInterface<AdriveDirective, AdriveResponse, SNmark1,
		    SNmark2, SNadrive> AdriveAccelInterface;

/*! Interface to the TransmissionModule */
typedef GcInterface<AdriveDirective, AdriveResponse, SNSampleTabInput,
		    SNSampleTabOutput, SNadrive> AdriveTransInterface;

/*! Adrive actuation interface (router) */
class ActuationInterface : public GcModule
{
private:
  /* Input interface (nominally to TrajFollower) */
  AdriveCommand *m_adriveCommand;		// interface to trajfollower
  AdriveCommand::Northface *m_adriveCommandNF;	//   - real interface
  AdriveDirective m_adriveDirective;		// inputs from trajfollower

  /* Steering controller interface */
  AdriveSteerInterface *m_steerInterface;	// interface to steering
  AdriveSteerInterface::Southface *m_steer;	//   - real interface

  /* Acceleration controller interface */
  AdriveAccelInterface *m_accelInterface;	// interface to acceleration
  AdriveAccelInterface::Southface *m_accel;	//   - real interface

  /* Transmission controller interface */
  AdriveTransInterface *m_transInterface;	// interface to transmission
  AdriveTransInterface::Southface *m_trans;	//   - real interface

  /* Required member variables for GcModule */
  ControlStatus m_controlStatus;		// unused
  MergedDirective m_mergedDirective;		// unused

  /*! Actuation status broadcast thread */
  void status();
  int statusCount;
  long statusSleepTime;
  pthread_mutex_t actuatorStateMutex;

public:
  struct gengetopt_args_info *options;		///! command line options
  skynet *m_skynet;				///! skynet socket for sim
  int m_drivesocket;				///! adrive socket
  int m_statesocket;				///! actuation state socket
  ActuatorState state;				///! current state of actuators

  ActuationInterface(int, struct gengetopt_args_info *);
  void initSparrow(DD_IDENT *, int);

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *);
  void control(ControlStatus *, MergedDirective *) {};
};

#endif
