/**
 * \file obdii.c
 * \brief Driver for the OBD-II interface
 *
 * \author Joshua Oreman
 * \date 19 July 2007
 *
 * This file contains functions to access the OBD-II onboard diagnostic interface to
 * Alice, returning things like current speed, current gear, engine RPM, etc.
 * It uses an Elmscan RS232-to-OBDII microcontroller, which is controlled through
 * a DM500 Ethernet-to-serial bridge. Commands to and from the controller are
 * human-readable, and it is quite feasible to control it manually.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ctype.h>
#include "obdii.h"

/** File descriptor connected to the DM500. */
static int socket_fd;

/** Dump contents of transmissions? */
static int verbose;

/**
 * \brief Writes a given buffer completely.
 *
 * This just calls write() repeatedly until all the bytes have actually
 * been written. If write() returns 0 or -1, the writing stops at that
 * point. For an EOF on write (return of 0), the function returns the
 * number of bytes it was able to write; for a write error, it returns
 * -1 immediately.
 *
 * \param bufv The buffer to write.
 * \param len Length of \a bufv.
 * \return The number of bytes written, 0 if nothing could be written before
 *         write() returned 0, or -1 on write error.
 */
static int write_all (const void *bufv, int len) 
{
    const char *buf = (const char *)bufv;
    int wrlen = 0;
    while (len > 0) {
        int res = write (socket_fd, buf, len);
        if (res < 0) return -1;
        if (res == 0) break;
        wrlen += res;
        buf += res;
        len -= res;
    }

    return wrlen;
}

/**
 * \brief Reads from socket_fd until a \c &gt; character is read.
 *
 * This function reads everything until a > character, storing up to \a len bytes
 * of response in [buf]. If [len] is shorter than the length
 * received, the length stored will be the length received modulo \a len,
 * and the buffer will contain the \e last part of the received reply.
 *
 * The returned buffer is NUL-terminated.
 *
 * \param bufv The buffer to store the bytes to.
 * \param len Maximum number of bytes to store.
 * \return The length stored, or 0 if timeout or EOF occurred, -1 on read error. */
static int read_result (void *bufv, int len) 
{
    char *origbuf = (char *)bufv;
    char *buf = origbuf;
    int sofar = 0;

    struct pollfd poller;
    poller.fd = socket_fd;
    poller.events = POLLIN;

    while (1) {
        int res;

        /* Wait for data to read. */
        if (!poll (&poller, 1, 1000))
            return 0;           /* timeout */
        
        /* Read in one char. */
        res = read (socket_fd, buf, 1);
        if (res < 0)
            return -1;          /* error */
        if (res == 0)
            return 0;           /* eof */

        /* Reached end yet? */
        if (*buf == '>')
            break;

        /* Advance pointer and counter. */
        buf++;
        sofar++;

        /* Reached end of buffer? */
        if ((sofar % len) == 0)
            buf = origbuf;
    }

    /* Trim whitespace from the end. */
    while (--buf >= origbuf && isspace (*buf))
        ;

    buf++; // now buf points to the char past the end
    *buf = 0; // NUL-terminate it

    return buf - origbuf;       /* length */
}

/* Send an AT command, wait for response. Returns 2 if "OK", 1 if any other text, 0 if timeout, -1 if error. */
int obdii_atcmd (const char *cmd) 
{
    char buf[80];
    int res;
    
    if (strlen (cmd) > 70) {
        errno = ENAMETOOLONG;
        return -1;
    }

    sprintf (buf, "AT%s\r", cmd);
    if ((res = write_all (buf, strlen (buf))) != strlen (buf))
        return (res > 0)? 0 : res;
    
    if ((res = read_result (buf, 80)) <= 0)
        return res;

    if (buf[0] == 'O' && buf[1] == 'K')
        return AT_OK;
    
    return AT_DONE;
}

#define CMD_BUFSIZE 512

/* Send an OBD-II command, wait for response, parse it. Not reentrant. */
int obdii_command (const void *cmdv, int cmdlen, void *respv, int resplen, int source)
{
    static char buf[CMD_BUFSIZE];
    const char *cmd = (const char *)cmdv;
    char *resp = (char *)respv;
    char *line;
    int i, res;
    int len = 0;

    if (cmdlen <= 0 || cmdlen > 7) {
        errno = EINVAL;
        return -1;
    }

    /* Format the command as ASCII bytes. */
    buf[0] = 0;
    for (i = 0; i < cmdlen; i++) {
        sprintf (buf + strlen (buf), "%02X ", cmd[i]);
    }
    /* Get rid of the last space and send it. */
    buf[strlen (buf)] = '\r';
    if ((res = write_all (buf, strlen (buf))) != strlen (buf))
        return (res > 0)? 0 : res;

    /* Read in... */
    if (read_result (buf, CMD_BUFSIZE) <= 0)
        return -1;
    
    /* Error? */
    if (strstr (buf, "NO DATA") || strstr (buf, "CONNECT")) {  /* as in UNABLE TO CONNECT */
        return -1;
    }

    /* Parse it. */
    line = buf;
    do {
        while (isspace (*line))
            line++;
        
        /* Format of each line:
         * SSS PP AA BB CC DD EE FF GG
         * where SSS is the source (3 hex)
         *        PP is the PCI byte
         *           0X -> single frame, this byte is size, next byte is group | 0x40, then echoes PID
         *           10 -> first of multiple frames, next byte is size, then group | 0x40, then echoes PID
         *           2X -> frame number X (X nonzero) in a multi-frame packet, data immediately follows
         * d[i] is data byte i, dp points to the start of the data, dl is how much there is in this frame.
         */
        int src;
        unsigned char p, d[7];
        if (sscanf (line, "%03X %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX %02hhX",
                          &src, &p,    &d[0], &d[1], &d[2], &d[3], &d[4], &d[5], &d[6])) {
            fprintf (stderr, "warning: could not parse OBD-II packet\n");
            continue;
        }

        if (src != source)
            continue;           /* "This is not the car you're looking for." */

        if ((p & 0xF0) == 0x00) {  /* single frame */
            len = p & 0xF;
            if (len > 7) {
                fprintf (stderr, "warning: invalid length (0x%02x) in OBD-II packet\n", len);
                continue;
            }
            if (resplen < len) len = resplen;

            /* Ignore the echo byte. */
            len -= 1;
            memcpy (resp, d + 1, len);
            break; /* don't worry about any more frames */
        }

        if ((p & 0xF0) == 0x10) {  /* start multi-frame */
            /* AA contains the length of the whole message. */
            len = d[0];
            if (resplen < len) len = resplen;

            /* Ignore echo byte BB. */
            len -= 1;

            int thislen = (len > 5)? 5 : len;
            memcpy (resp, d + 2, thislen);
        }
        
        if ((p & 0xF0) == 0x20) {  /* continue multi-frame */
            int seq = p & 0xF;  /* sequence number */
            
            /* If this is frame N, all the first N-1 frames must have been full.
             * Frame 1 contained 4 important bytes; all the others contained 7.
             * Put this one in the right place.
             * And note that we can receive frames out of order!
             */
            int start = 4 + (seq - 1) * 7;
            int thislen = ((len - start) > 7)? 7 : len;
            memcpy (resp + start, d, thislen);
        }
    } while ((line = strchr (line, '\r')) != NULL);

    /* We're done. Get outta here. */
    return len;
}

/* Shortcut to send a one-byte subcommand of group 0x01 out. */
int obdii_cmd1 (unsigned char byte, void *resp, int resplen, int source) 
{
    unsigned char bytes[2];
    bytes[0] = 0x01;
    bytes[1] = byte;
    return obdii_command (bytes, 2, resp, resplen, source);
}

/* Shortcut to send a two-byte subcommand of group 0x22 out. */
int obdii_cmd2 (unsigned char first, unsigned char second, void *resp, int resplen, int source) 
{
    unsigned char bytes[3];
    bytes[0] = 0x22;
    bytes[1] = first;
    bytes[2] = second;
    return obdii_command (bytes, 3, resp, resplen, source);
}

/* Open the OBD-II connection - doesn't do initialization */
int obdii_open (int vflag) 
{
    struct sockaddr_in sin;
    char buf[16];

    verbose = (vflag >= 4);

    socket_fd = socket (PF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
        return -1;

    memset (&sin, 0, sizeof(sin));

    if (vflag >= 3)
        printf ("OBD-II: connecting to %s port %d\n", DM500_IP, DM500_PORT);

    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr (DM500_IP);
    sin.sin_port = htons (DM500_PORT);

    if (connect (socket_fd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
        int err = errno;
        close (socket_fd);
        socket_fd = -1;
        errno = err;
        return -1;
    }

    /* Get it to a prompt, in case it was interrupted in the middle of something else */
    if (write (socket_fd, "\r", 1) != 1) {
        close (socket_fd);
        socket_fd = -1;
        return -1;
    }

    /* Read everything till the prompt. */
    if (read_result (buf, 16) <= 0) {
        close (socket_fd);
        socket_fd = -1;
        return -1;
    }

    if (vflag >= 3)
        printf ("OBD-II: connected\n");

    return 0;
}

/* Does initialization */
int obdii_init()
{
    /* Restart the device. Use WS (warm start), because ATZ takes about a second
       and we'd rather not have a timeout. */
    if (obdii_atcmd ("WS") <= 0)
        return -1;

    // Echo off
    if (obdii_atcmd ("E0") <= 0)  /* != AT_OK is fine - if echo was on, we'll get "ATE0" back before "OK". */
        return -1;

    // Headers on (so we can tell where each thing came from)
    if (obdii_atcmd ("H1") != AT_OK)
        return -1;

    // Try a simple OBD-II command. Just make sure we can send it, now.
    const char *cmd = "\x01\x1C";
    unsigned char resp[4];
    if (obdii_command (cmd, 2, resp, 4, SOURCE_ECU1) <= 0)
        return -1;
    
    return 0;
}

#define CMD1_GET_RPM      0x0C         /* resp 2B, in RPM*4 */
#define CMD1_GET_RUNTIME  0x1F         /* resp 2B, in seconds */
#define CMD1_GET_SPEED    0x0D         /* resp 1B, in kph */
#define CMD1_GET_CTEMP    0x05         /* coolant temperature - resp 1B, in degrees C +40 */
#define CMD2_GET_THRPOS   0x09, 0xD4   /* throttle position - resp X1B, in degrees*12 */
#define CMD2_GET_TORQUE   0x09, 0xCB   /* resp SS, in N*m [???] */
#define CMD2_GET_GPLT     0x14, 0x51   /* glow plug lamp time - resp X2B, in seconds */
#define CMD2_GET_GEAR_RAT 0x11, 0xB7   /* current gear ratio - resp X2B, in 1/16384ths of ??? */
#define CMD2_GET_BATTVOLT 0x11, 0x72   /* battery voltage - resp X1B, in 1/16ths of a volt */
#define CMD2_GET_GPLO     0x16, 0x67   /* glow plug light on - resp X1B, yes/no */
#define CMD2_GET_GEAR     0x11, 0xB3   /* current gear - resp X1B, in increments of 2 (?) */
#define CMD2_GET_TRANSPOS 0x11, 0xB6   /* transmission position - resp X1B, 'P' 'R' 'N' 'D' 'X' */
#define CMD2_GET_TCCDC    0x11, 0xB0   /* torque converter clutch duty cycle - resp X2B, percent */
#define CMD2_GET_TCCS     0x09, 0x71   /* torque converter control state - resp X1B, ?? */

double obdii_get_rpm() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd1 (CMD1_GET_RPM, rawr, 3, SOURCE_ECU1)) != 3) {
        return -1.0;
    }
    return (double)((rawr[1] << 8) | rawr[2]) / 4.0;
}

int obdii_get_time_since_engine_start() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd1 (CMD1_GET_RUNTIME, rawr, 3, SOURCE_ECU1)) != 3) {
        return -1;
    }
    return ((rawr[1] << 8) | rawr[2]);
}

double obdii_get_vehicle_speed() 
{
    unsigned char rawr[2];
    int res;
    if ((res = obdii_cmd1 (CMD1_GET_SPEED, rawr, 2, SOURCE_ECU1)) != 2) {
        return -1.0;
    }
    return (double)rawr[1] * 1000.0 / 3600.0;  /* convert to m/s */
}

double obdii_get_engine_coolant_temp() 
{
    unsigned char rawr[2];
    int res;
    if ((res = obdii_cmd1 (CMD1_GET_CTEMP, rawr, 2, SOURCE_ECU1)) != 2) {
        return -1.0;
    }
    return ((double)rawr[1] - 40.0) * 1.8 + 32.0;  /* convert to Farenheit */
}

double obdii_get_throttle_position() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_THRPOS, rawr, 3, SOURCE_ECU1)) != 3) {
        return -1.0;
    }
    return (double)rawr[2] / 12.0;
}

int obdii_get_torque() 
{
    unsigned char rawr[4];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_TORQUE, rawr, 4, SOURCE_ECU1)) != 4) {
        return -1;
    }
    return (rawr[2] << 8) | rawr[3];
}

int obdii_get_glow_plug_lamp_time() 
{
    unsigned char rawr[4];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_GPLT, rawr, 4, SOURCE_ECU1)) != 4) {
        return -1;
    }
    return (rawr[2] << 8) | rawr[3];
}

double obdii_get_current_gear_ratio() 
{
    unsigned char rawr[4];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_GEAR_RAT, rawr, 4, SOURCE_ECU2)) != 4) {
        return -1.0;
    }
    return (double)((rawr[2] << 8) | rawr[3]) / 16384.0;
}

double obdii_get_battery_voltage() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_BATTVOLT, rawr, 3, SOURCE_ECU1)) != 3) {
        return -1.0;
    }
    return (double)rawr[2] / 16.0;
}

int obdii_glow_plug_light_on() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_GPLO, rawr, 3, SOURCE_ECU1)) != 3) {
        return -1;
    }
    return rawr[2]? 1 : 0;
}

int obdii_get_current_gear() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_GEAR, rawr, 3, SOURCE_ECU2)) != 3) {
        return -1;
    }
    return rawr[2] / 2;
}

char obdii_get_transmission_position() 
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_GEAR, rawr, 3, SOURCE_ECU2)) != 3) {
        return '!';
    }

    rawr[2] /= 2;    
    if (rawr[2] == 70) return 'P';
    if (rawr[2] == 60) return 'R';
    if (rawr[2] == 50) return 'N';
    if (rawr[2] > 40 && rawr[2] < 50) return 'D';
    return 'X';
}

int obdii_get_torque_converter_control_state()
{
    unsigned char rawr[3];
    int res;
    if ((res = obdii_cmd2 (CMD2_GET_TCCS, rawr, 3, SOURCE_ECU1)) != 3) {
        return -1;
    }
    return rawr[2];
}
