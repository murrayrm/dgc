/*!
 * \file gcdrive.cc
 * \brief Actuator interface program for Alice
 *
 * \author Richard Murray
 * \date 14 April 2007
 *
 * Gcdrive is the actuator interface program for Alice.  It is a
 * replacement for the old adrive program, rewritten to be compatible
 * with the canonical software architecture.
 *
 * Note: once gcdrive is up and running, the old adrive will be
 * removed and this program will replace it.
 *
 * \section Execution Overview
 *
 * Adrive consists of a collection of GcModules that are used to
 * manage the operation of the various actuators that are controlled
 * by the program.  The following GcModules are defined:
 *
 *  * ActuationInterface - receives commands via skynet; routes to actuators
 *  * <Actuator>Module - individual actuators: Steering, Acceleration, etc
 *
 * The ActuationInterface module is a shell module that just converts
 * the incoming actuation commands to directives for the individual
 * actuator modules.  
 */

#include <stdlib.h>
#include <assert.h>
#include "dgcutils/cfgfile.h"
#include "cmdline.h"

/* Sparrow display tables */
#include "sparrow/display.h"
#include "sparrow/dbglib.h"
#include "sparrow/errlog.h"
#include "overview.h"			// sparrow display table
#include "adriveSparrow.hh"

/* GcModule definitions */
#include "ActuationInterface.hh"	// Actuation interface
#include "SteeringModule.hh"		// Steering control module
#include "AccelerationModule.hh"	// Acceleration control module
#include "TransmissionModule.hh"	// Transmission control module
#include "EstopSensor.hh"		// Estop sensor module

int main(int argc, char** argv)
{
  int verbose = 0;

  /*
   * Read command line options and configuration file
   *
   * We use gengetopt to parse command line options, including the
   * possibility of reading arguments from a configuration file.  The
   * legacy adrive.config file is also detected, but not currently
   * read (may put that in later).
   * 
   */

  /* Process command line arguments */
  gengetopt_args_info cmdline;
  if (cmdline_parser(argc, argv, &cmdline) != 0) exit (1);
  if (cmdline.argfile_given &&
      cmdline_parser_configfile(cmdline.argfile_arg, &cmdline, 0, 0, 1) != 0)
    exit(1);

  /*
   * Process command line options 
   *
   * Now that we have read the config file, process the command line
   * options that control whether certain threads should start up.
   *
   */

  /* Check to see if we want verbose messages */
  if (cmdline.verbose_given) verbose = cmdline.verbose_arg;
  if (verbose > 5) 
    cerr << "verbose messages on (level = " << verbose << ")" <<endl;

  /* Skynet initialization */
  int skynet_key = skynet_findkey(argc, argv);
  dd_rebind_tbl("skynet_key", &skynet_key, overview_tbl);
  adrive_initSparrow(skynet_key);

  /*
   * Configure adrive operations
   *
   * At this point we should have initalized all of the information in
   * my_vehicle and cmdline structs and we can proceed to starting up
   * all required modules.  This is done by instantiating each
   * GcModule that we use and starting them up.
   *
   * The ordering of operations is to first instantiate all devices
   * and then start them running.  The constructors for each actuator
   * should fail if we can't talk to the actuator and the proper
   * command line flags are not set, so this insures that we only
   * start running things if everything is configured properly.
   */

  /* Create the main actuator interface */
  ActuationInterface *adriveInterface = 
    new ActuationInterface(skynet_key, &cmdline);

  /* Instantiate each of the actuators */
  if (verbose) cerr << "instantiating steering" << endl;
  SteeringModule *steering = new SteeringModule(skynet_key, adriveInterface);

  if (verbose) cerr << "instantiating acceleration" << endl;
  AccelerationModule *acceleration =
    new AccelerationModule(skynet_key, adriveInterface, verbose);

  if (verbose) cerr << "instantiating transmission" << endl;
  TransmissionModule *transmission = 
    new TransmissionModule(skynet_key, adriveInterface);

  if (verbose) cerr << "instantiating estop" << endl;
  EstopSensor *estop = new EstopSensor(adriveInterface);

  /* Now start up all of the interfaces */
  if (verbose > 2) cerr << "starting up actuation, sensor interfaces" << endl;
  adriveInterface->Start();
  steering->Start();
  acceleration->Start();
  transmission->Start();

  /* Initialize sparrow and set up the display variables for each actuator */
  if (!cmdline.disable_console_given) {
    if (dd_open() < 0) { dbg_error("can't open display"); exit(-1); }
    if (dd_usetbl(overview_tbl) < 0) {
      dbg_error("can't open display table");
      exit(-1);
    }
    adriveInterface->initSparrow(overview_tbl, ACTSTATE_LABEL);
    steering->initSparrow(overview_tbl, STEER_LABEL);
    acceleration->initSparrow(overview_tbl, BRAKE_LABEL, GAS_LABEL);
    transmission->initSparrow(overview_tbl, TRANS_LABEL);
    estop->initSparrow(overview_tbl, ESTOP_LABEL);

    /* Turn on error logging in sparrow */
    dd_errlog_init(50); dd_errlog_bindkey();

    /* Stay in sparrow loop until we are finished */
    dd_loop();
    dd_close();
  } else {
    /* Just wait until something causes us to exit */
    while (1) { sleep(10); }
  }

  /* All done */
  return 0;
}
