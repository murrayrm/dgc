#include "TransmissionControl.hh"
using namespace std;

TransmissionControl::TransmissionControl()
  : GcModule( "TransmissionControl", &m_controlStatus, &m_mergedDirective, 100000, 100000 )
{
  int skynetKey = 0;

  acTCInterfaceNF = NULL;
  acTCInterface = new ACTCInterface( skynetKey, this );
  if( acTCInterface )
    acTCInterfaceNF = acTCInterface->getNorthface();
  else
    cerr << "acTCInterface = NULL" << endl;
}

void TransmissionControl::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  TCControlStatus* controlStatus = dynamic_cast<TCControlStatus*>(cs);
  TCMergedDirective* mergedDirective = dynamic_cast<TCMergedDirective*>(md);


  // TODO: Report status to actuation control (maybe only when the control
  // status is FAILED or COMPLETED.
  if (controlStatus->status == TCControlStatus::COMPLETED ||
      controlStatus->status == TCControlStatus::FAILED)
  {
    TCResponse response;
    response.id = controlStatus->id;
    
    if (controlStatus->status == TCControlStatus::COMPLETED)
      response.status = COMPLETED;
    else
    {
      response.status = FAILED;
      response.reason = controlStatus->reason;
    }

    acTCInterfaceNF->sendResponse( &response );
  }

  // Compute the next mergedDirective
  if (acTCInterfaceNF->haveNewDirective())
  {
    ACDirective newDirective;
    acTCInterfaceNF->getNewDirective( &newDirective );

    // TODO: Determine if you want to reject this directive. This
    // includes checking that the id of newDirective is unique.
    bool reject = false;

    // Update the merged directive if newDirective is not rejected.
    if (!reject)
    {
      // TODO: Make sure that this is correct.
      mergedDirective->id = newDirective.id;
      mergedDirective->number_arg = newDirective.number_arg;
    }
    else
    {
      // TODO: Default extension
    }
  }
}

void TransmissionControl::control(ControlStatus* cs, MergedDirective* md)
{
  TCControlStatus* controlStatus = dynamic_cast<TCControlStatus*>(cs);
  TCMergedDirective* mergedDirective = dynamic_cast<TCMergedDirective*>(md);
  double number_arg = mergedDirective->number_arg;

  // TODO: Send command to transmission

  // TODO: Determine controlStatus. From your template, it seems like
  // most of the directives replace the previous ones. So in the case
  // where the previous directives are not yet completed, they should
  // fail.

}
