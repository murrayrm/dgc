/*!
 * \file UT_timing.cc 
 * \brief Adrive unit test - timing
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 *
 * \ingroup unittest
 *
 * This program sends a sequence of directives to adrive at a high
 * rate, testing to make sure that adrive can handle it and also
 * (eventually) analyzing the timing of the system.
 */

#include <iostream>
#include <assert.h>
#include <math.h>
using namespace std;

#include "skynet/skynet.hh"
#include "gcinterfaces/AdriveCommand.hh"

/* 
 * Create the testModule class for use with GcModule 
 * 
 * This class is not actually used since this is just a test function.
 * However, in order to talk to adrive we need to have a GcModule, so
 * this is it.  Everything is inlined here since we don't actually
 * require any functionality.
 */
class TestModule : public GcModule
{
private:
  ControlStatus m_testStatus;		// unused
  MergedDirective m_testDirective;	// unused

public:
  TestModule(int skynet_key) :
    GcModule("testModule", &m_testStatus, &m_testDirective, 100000, 100000)
  {}

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *) {};
  void control(ControlStatus *, MergedDirective *) {};
};

int main (int argc, char **argv)
{
  cout << "Unit test: " << argv[0] << endl;
  int skynet_key = skynet_findkey(argc, argv);

  /* Create a GcModule for the test program */
  TestModule *testModule = new TestModule(skynet_key);
  testModule->Start();

  /* Set up the interface to adrive */
  AdriveCommand::Southface *adriveInterfaceSF =
    AdriveCommand::generateSouthface(skynet_key, testModule);
  adriveInterfaceSF->setStaleThreshold(10);

  /* Generate a directive that we can use for sending commands */
  AdriveDirective adriveCommand;
  adriveCommand.id = 0x0000;
  adriveCommand.actuator = Steering;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = 0.05;

  /* Send a sequence of commands without waiting for response */
  for (int i = 0; i < 10; ++i) {
    adriveCommand.id = 0x2000 + i;
    adriveCommand.arg = 0.05 * sin((double) i);
    adriveInterfaceSF->sendDirective(&adriveCommand);
    usleep(50000);		// 20 Hz
  }

  /* Now read all of the responses */
  for (unsigned int i = 0; i < 10; ++i) {
    while (!adriveInterfaceSF->haveNewStatus()) { usleep(10000); }

    AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
    assert(response != NULL);
    assert(response->status == GcInterfaceDirectiveStatus::COMPLETED);
    if (response->id != 0x2000 + i) {
      fprintf(stderr, 
	      "%s: bad ID on response at i = %d; expected %d, received %d\n",
	      argv[0], i, 0x2000 + i, response->id);
      i = response->id - 0x2000;
    }
  }

  /* Make sure there are no additional responses */
  assert(!adriveInterfaceSF->haveNewStatus());

  cerr << "staring send of 10000 steering cmds" << endl;

  /* Send a long sequence of commands and read response */
  unsigned int recv_id = 0;
  int error_count = 0;
  for (unsigned int i = 0; i < 10000; ++i) {
    adriveCommand.id = 0x3000 + i;
    adriveCommand.arg = 0.05 * sin((double) i);
    adriveInterfaceSF->sendDirective(&adriveCommand);
    usleep(50000);		// 20 Hz

    while (adriveInterfaceSF->haveNewStatus()) {
      AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
      assert(response != NULL);
      assert(response->status == GcInterfaceDirectiveStatus::COMPLETED);
      if (response->id != 0x3000 + recv_id) {
	// Keep track of the number of errors
	++error_count;
	recv_id = response->id - 0x3000;
      }
      assert(response->id == 0x3000 + recv_id++);
    }
  }
  cerr << "read " << recv_id << " directives" 
       << " with " << error_count << "errors" << endl;

  /* Now read any remaining responses */
  for (; recv_id < 10000; ++recv_id) {
    while (!adriveInterfaceSF->haveNewStatus()) { usleep(10000); }

    AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
    assert(response != NULL);
    assert(response->status == GcInterfaceDirectiveStatus::COMPLETED);
    assert(response->id == 0x3000 + recv_id);
  }

  /* Make sure there are no additional responses */
  assert(!adriveInterfaceSF->haveNewStatus());

  return 0;
}
