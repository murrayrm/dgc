/*!
 * \file EstopSensor.hh
 * \brief Header file for EstopSensor class
 *
 * \author Richard Murray
 * \date 22 April 2007
 *
 * This file defines the EstopSensor class for adrive.  This is
 * used to communicate with the estop hardware.
 */

#ifndef __EstopSensor_hh__
#define __EstopSensor_hh__

#include <pthread.h>

#include "sparrow/display.h"

#include "ActuationInterface.hh"

/*
 * \class EstopSensor
 *
 * This class is used to represent the estop object.
 */

class EstopSensor
{
private:
  /*!\param Actuation interface */
  ActuationInterface *adrive;

  void status();
  int statusCount;
  long statusSleepTime;

  /* Status information */
  int initialized;		/*!< Steering is properly initialized */
  int connected;		/*!< Able to talk to the motor controller */
  int currentStatus;

  int sparrowLabel;		/*! Label for sparrow display */
  DD_IDENT *sparrowTable;	/*! Table where our status is kept */

  /*! Mutex controlling access to serial port and data */
  pthread_mutex_t sensorMutex;

public:
  /*! Constructor */
  EstopSensor(ActuationInterface *);
  void initSparrow(DD_IDENT *, int);
};

#endif
