/*!
 * \file AccelerationModule.cc
 * \brief GcModule for controlling Alice's acceleration
 * 
 * \author Nok Wongpiromsarn and Richard Murray
 * \date 14 April 2007
 *
 * This is the GcModule that controls Alice's acceleration.  It receives
 * commands from the ActuatorInterface module (router) and sends them
 * to the acceleration motor controller.  The initial release assumes
 * correct operation and does minimal checking for errors.
 */

#include <assert.h> 

#include "dgcutils/DGCutils.hh"

#include "AccelerationModule.hh"
#include "brake.hh"
#include "throttle.hh"
using namespace std;

/*
 * AccelerationModule::AccelerationModule
 *
 * The constructor initializes the GcModule interface, the acceleration
 * port and other class data.
 */

AccelerationModule::AccelerationModule(int skynetKey,
				       ActuationInterface *actuationInterface,
				       int verbose_flag)
  : GcModule("Adrive_AccelerationModule", &m_controlStatus, &m_mergedDirective,
	      1000, 1000 )
{
  verbose = verbose_flag;
  if (verbose > 5) cerr << "AccelerationModule: verbose = " << verbose << endl;

  /*
   * GcModule initialization
   *
   * The first thing that we do is initialize all of the variables
   * that are required for communicating with other GcModules,
   * including the ActuationInterface.
   */

  /* Get the interface to the accelControl module */
  adriveAccelInterfaceNF = 
    AdriveAccelInterface::generateNorthface(skynetKey, this);
  adriveAccelInterfaceNF->setStaleThreshold(10);

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * AccelerationModule initialization
   *
   * Initialize the member variables used by the control and arbitrate
   * functions.
   */

  /* Initialize the IDs so that we can tell when we get new commands */
  m_latestID = m_currentID = 0;

  /* Initialize the control state to keep from returning a response */
  m_controlStatus.status = ControlStatus::RUNNING;
  controlCount = 0;

  /*
   * Hardware/simulation initailization
   *
   * Now that we have initalized the software interfaces, we
   * initialize the hardware interface.  This consists of opening up
   * the serial port so that we can talk to the motor controller and
   * then initializing the motor controller.  If we can't open the
   * serial port, we abort with an error.  It is possible for the
   * motor controller initialization to fail if acceleration is not
   * currently enabled in Alice.  If this fails, we mark the interface
   * as uninitialized and continue on.  This allows us to continue to
   * receive status and also to rest the acceleration module manually from
   * sparrow.
   *
   * If the acceleration interface is disabled (using the command line
   * flag), then we still try to open the port but we don't generate
   * abort if we can't do it.  This allows the acceleration status
   * information to still be run in the case that we are in Alice and
   * just don't want to use acceleration.  If this acceleration is disabled,
   * we set the inialized flag to true so that it appears to modules
   * that acceleration is being commanded.  This is useful for testing.
   *
   * If we are running in simulation mode, we don't bother trying to
   * talk to the hardware. 
   */

  if (adrive->options->simulate_given) {
    if (verbose > 5) cerr << "AccelerationModule: simulation mode" << endl;
    /* Simulation mode - no action required */
    brake_initialized = 1;    brake_connected = 1;
    throttle_initialized = 1; throttle_connected = 1;

  } else {
    /* Open the port to the brake actuator */
    if (verbose) cerr << "AccelerationModule: opening brake" << endl;
    if (!brake_open(adrive->options->brake_port_arg)) {
      if (adrive->options->disable_brake_flag) {
	/* Braking has been disabled, so allow the error to pass */
	brake_connected = 0;
	brake_initialized = 1;
      } else {
	/* Don't allow operation without being able to talk to acceleration */
	cerr << "AccelerationModule: brake_open failed" << endl;
	exit(1);
      }
    } else {
      /* Keep track of the fact that we are connected */
      brake_connected = 1;
      brake_initialized = 1;
    }
   
    /* Initialize the braking subsystem */
    #warning Braking subsystem not initialized (causes hang)

    /* Open the port to the throttle actuator */
    if (verbose) cerr << "AccelerationModule: opening throttle" << endl;
    if (!throttle_open(adrive->options->gas_port_arg)) {
      if (adrive->options->disable_gas_flag) {
	/* Throttle has been disabled, so allow the error to pass */
	throttle_connected = 0;
	throttle_initialized = 1;
      } else {
	/* Don't allow operation without being able to talk to acceleration */
	cerr << "AccelerationModule: throttle_open failed" << endl;
	exit(1);
      }
    } else {
      /* Keep track of the fact that we are connected */
      throttle_connected = 1;
      throttle_initialized = 1;
    }

    /* Initialize the throttle subsystem */
    #warning Throttle subsystem not initialized (causes hang)

  }

  /*
   * Status thread startup
   *
   * As part of the AccelerationModule function, we start up a thread to
   * read the status of the actuator on a periodic basis.  This thread
   * fills in data in the AccelerationModule class, so we have easy
   * access to this information.  
   */

  statusCount = 0;
  statusSleepTime = 500000;
  DGCstartMemberFunctionThread(this, &AccelerationModule::status);

  /* Mutexes for controlling access to serial port and varaibles */
  DGCcreateMutex(&brakeMutex);
  DGCcreateMutex(&throttleMutex);

}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void AccelerationModule::initSparrow(DD_IDENT *tbl, int brake_id, 
				     int throttle_id)
{
  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!brake_connected) {
      /* No connection to the motor controller (acceleration disabled) */
      dd_setcolor_tbl(brake_id, BLACK, RED, tbl);

    } else if (!brake_initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(brake_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(brake_id, BLACK, GREEN, tbl);
    }

    /* Set the color of the label to reflect our current status */
    if (!throttle_connected) {
      /* No connection to the motor controller (acceleration disabled) */
      dd_setcolor_tbl(throttle_id, BLACK, RED, tbl);

    } else if (!throttle_initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(throttle_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(throttle_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("brake.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("brake.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("brake.command", &brake_command, tbl);
    dd_rebind_tbl("brake.position", &brake_current, tbl);
    dd_rebind_tbl("brake.pressure", &brake_pressure, tbl);

    dd_rebind_tbl("throttle.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("throttle.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("throttle.command", &throttle_command, tbl);
    dd_rebind_tbl("throttle.position", &throttle_current, tbl);
  }
}

/*
 * AccelerationModule::arbitrate
 *
 * Arbitrator for AccelerationModule.  The artibiter receives acceleration
 * commands from the actuationInterface and sends them to control.  At
 * present, the only directives that are recognized are new acceleration
 * commands, which replace old acceleration commands.
 *
 */

void AccelerationModule::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  AccelControlStatus *controlResponse =
    dynamic_cast<AccelControlStatus *>(cs);

  AccelMergedDirective *mergedDirective =
    dynamic_cast<AccelMergedDirective *>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  AdriveResponse response;

  switch (controlResponse->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::COMPLETED;
    adriveAccelInterfaceNF->sendResponse( &response, response.id );
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::FAILED;
    response.reason = controlResponse->reason;
    adriveAccelInterfaceNF->sendResponse( &response, response.id );
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */

  /* Check to see if we have rec'd a new directive from actuation interface */
  if (adriveAccelInterfaceNF->haveNewDirective()) {
    AdriveDirective newDirective;
    adriveAccelInterfaceNF->getNewDirective( &newDirective );

    /* !! Make sure that the directive has a unique ID !! */
#   warning need to add check for unique directive ID

    /* Save the ID to pass back when we are done */
    mergedDirective->parent_id = newDirective.id;

    switch (newDirective.command) {
    case SetPosition:
      /* Make sure we are properly initalized */
      if ((newDirective.arg > 0 && !throttle_initialized) ||
	  (newDirective.arg < 0 && !brake_initialized)) {
	/* Respond with a failure */
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::FAILED;
	response.reason = NotInitialized;
	adriveAccelInterfaceNF->sendResponse( &response, response.id );
	return;
      }

      /* Check the range of the command */
      if (newDirective.arg < -1 || newDirective.arg > 1) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = OutOfRange;
	adriveAccelInterfaceNF->sendResponse( &response, response.id );
	return;
      }

      /* Generate the directive to accel the car */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = newDirective.arg;
      mergedDirective->id = ++m_latestID;
      break;

    case Reset:
      /* !! Check to make sure we are stopped !! */
#     warning missing check for stop on Accel Reset
      mergedDirective->command = Reset;
      mergedDirective->id = ++m_latestID;
      break;
    }
  }
}

/*
 * AccelerationModule::control
 *
 * Control for AccelerationModule.  The artibiter receives merged
 * directives the AccelerationModule arbitrator.  For acceleration, this is
 * just the latest acceleration command, which is sent on to the hardware.
 * The m_currentID tag is used to keep track of which directive we are
 * currently excuting.
 *
 */

void AccelerationModule::control(ControlStatus *cs, MergedDirective *md)
{
  AccelControlStatus* controlResponse =
    dynamic_cast<AccelControlStatus *>(cs);
  AccelMergedDirective* mergedDirective =
    dynamic_cast<AccelMergedDirective *>(md);

  /* First check to see if we have anything new to do */
  if (mergedDirective->id == m_currentID) {
    /* Tell the arbiter we are still running */
    controlResponse->status = ControlStatus::RUNNING;
    return;
  }

  /* New directive to process: update IDs and counters (for sparrow) */
  m_currentID = mergedDirective->id;
  brake_command = mergedDirective->arg < 0 ? -mergedDirective->arg : 0;
  throttle_command = mergedDirective->arg > 0 ? mergedDirective->arg : 0;
  ++controlCount;

  switch (mergedDirective->command) {
  case SetPosition:
    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_given) {
      drivecmd_t my_command;
      my_command.my_actuator = accel;
      my_command.my_command_type = set_position;
      my_command.number_arg = mergedDirective->arg;
      adrive->m_skynet->send_msg(adrive->m_drivesocket, &my_command, 
				 sizeof(my_command), 0);

    } else {
      if (mergedDirective->arg > 0) {
	/* Send the command to the throttle controller */
	pthread_mutex_lock(&throttleMutex);
	throttle_setposition(throttle_command);
	pthread_mutex_unlock(&throttleMutex);
      } else {
	/* Send the command to the brake controller */
	pthread_mutex_lock(&brakeMutex);
	brake_setposition(brake_command);
	pthread_mutex_unlock(&brakeMutex);
      }
    }

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  case Reset:
    /* Initialize the brake actuator (if connected) */
    pthread_mutex_lock(&brakeMutex);
    brake_initialized = !brake_connected || 
      brake_open(adrive->options->brake_port_arg);
    pthread_mutex_unlock(&brakeMutex);

    /* Initialize the brake actuator (if connected) */
    pthread_mutex_lock(&throttleMutex);
    throttle_initialized = !throttle_connected || 
      throttle_open(adrive->options->gas_port_arg);
    pthread_mutex_unlock(&throttleMutex);

    /* Make sure that it worked */
    if (brake_initialized && throttle_initialized) {
      controlResponse->status = ControlStatus::STOPPED;

    } else {
      controlResponse->status = ControlStatus::FAILED;
      controlResponse->reason = InitializationError;
    }      
    controlResponse->id = m_currentID;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;
  }
}

/*
 * AccelerationModule::status
 *
 * This function is called as a thread and is used to read the status
 * of the actuator.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void AccelerationModule::status()
{
  while (1) {
    if (adrive->options->simulate_given) {
      /* In simulation mode, read state from asim */
      brake_pressure = adrive->state.m_brakepressure;
      throttle_current = adrive->state.m_gaspos;
      brake_current = adrive->state.m_brakepos;

    } else {
      /* Fist check the brake actuator */
      if (!brake_connected) {
	/* Acceleration is currently disabled, so just return zero */
	brake_current = 0;
	brake_pressure = 0;

	/* Set status based on local information */
	adrive->state.m_brakestatus = brake_initialized && brake_connected;
	adrive->state.m_gasstatus = 
	  throttle_initialized && throttle_connected;

      } else {
	/* Read the current position */
	pthread_mutex_lock(&brakeMutex);
	double retval = brake_getposition();
	pthread_mutex_unlock(&brakeMutex);

	/* Check the status and set state appropriately */
	if (retval == -2) {
	  /* Error condition */
#         warning brake status error not yet handled
	  cerr << "AccelerationModule::status error reading brake position"
	       << endl;
	  sleep(1);

	} else {
	  brake_current = retval;
	}
	
	/* Read the current pressure */
	pthread_mutex_lock(&brakeMutex);
	retval = brake_getpressure();
	pthread_mutex_unlock(&brakeMutex);

	/* Check the status and set state appropriately */
	if (retval == -1) {
	  /* Error condition */
#         warning acceleration status error not yet handled
	  cerr << "AccelerationModule::status error reading brake pressure"
	       << endl;
	  sleep(1);

	} else {
	  brake_pressure = retval;
	}
      }

      /* Now check the throttle actuator */
      if (!throttle_connected) {
	/* Acceleration is currently disabled, so just return zero */
	throttle_current = 0;

      } else {
	/* Read the current status */
	pthread_mutex_lock(&throttleMutex);
	double retval = throttle_getposition();
	pthread_mutex_unlock(&throttleMutex);

	/* Check the status and set state appropriately */
	if (retval == -2) {
	  /* Error condition */
#         warning acceleration status error not yet handled
	  cerr << "AccelerationModule::status error reading throttle position"
	       << endl;
	  sleep(1);

	} else {
	  throttle_current = retval;
	}

      }
      /* Now fill up the actuatorState struct with current state */
      adrive->state.m_brakestatus = brake_initialized && brake_connected;
      adrive->state.m_brakepos = brake_current;
      adrive->state.m_brakecmd = brake_command;

      adrive->state.m_gasstatus = 
	throttle_initialized && throttle_connected;
      adrive->state.m_gaspos = throttle_current;
    }

    /* Now that we have the state, check to see if there are any problems */
    /* TODO: implement something here */

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}
