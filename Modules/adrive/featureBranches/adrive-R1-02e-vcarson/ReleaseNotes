              Release Notes for "adrive" module

Release R1-02e (Tue Jul 31  1:26:30 2007):
	Multiple bug fixes and feature additions for gcdrive:
	  * Added detection of actuator failures
	  * Steering can be reset from the screen
	  * Added debugging to help with ssh problesm

	Actuator failures: if an actuator is disabled or fails to
	return valid status information, the actuator status flag (in
	ActuationState structure) will be set to zero.  For critical
	actuators (gas, brake, steering, estop), the vehicle will also be
	commanded to pause.  

	Steering reset: the steering actuator can be reset from the sparrow
	display by selecting the "STEER" button and pressing enter.  The
	other actuators do not yet support reset, but will when we get the
	PLC installed.

	Ssh debugging: I have added some additional informational messages
	to debug the problem where gcdrive does not work correctly across
	ssh.  If this happens, run gcdrive with --verbuse=9 and report back
	the error message in bug 3252.

Release R1-02d (Fri Jul 27 19:29:20 2007):
	Minor performance tweaks to event-based, and additional options.
	Inserted code to boost the priority of the ActuationInterface
	message pump thread as this is a bottleneck, however the pthread
	call is returning an error, so the code has been ifdef'd until it
	can be fixed and properly tested.

Release R1-02c (Fri Jul 27  3:31:42 2007):
	Misc bugfixes: non-DM500 steering works now, PLC estop doesn't loop
	infinitely fast, Shifting state lasts only as long as it takes to
	shift as long as OBD-II is working properly. (There's a 3-second
	failsafe timeout in case it's not, and OBD-II is completely ignored
	if it's fake or disabled.)

Release R1-02b (Wed Jul 25 15:35:21 2007):
	The timestamps for steering, gas, brake, estop and obdii are now
	properly set in the ActuatorState structure.

Release R1-02a (Wed Jul 25 14:00:05 2007):
	Passing --steer-port=-1 will use the DM500 for steering control.

Release R1-02 (Wed Jul 25 12:09:01 2007):
	You can now use the PLC for only a subset of the actuators by passing
	--partial-plc (-P) and setting the serial port number of the actuators
	to PLCize to -1 (--gas-port=-1, etc).

Release R1-01z (Wed Jul 25 10:34:17 2007):
	Fixed the core dump when commanding throttle from the display.  There
	can be some initialization issues that generate NULL pointers.  Not
	race critical, so I just put in a check for how (and error
	message). 

Release R1-01y (Mon Jul 23  0:22:03 2007):
	Add OBD-II support.

Release R1-01x (Sat Jul 21 17:13:48 2007):
	When compiling the event-based flavor, the gcmodules will be stopped and 
  deleted properly (no core dump) when clsing gcdrive -- added a stop function
  to all of the gcmodules to stop their status threads.

Release R1-01w (Sat Jul 21 16:55:36 2007):
	Multiple updates to tweak recent releases:
	  * Integrated GCMODULE_EVENT_BASED code with original code
	  * Updated plc code so that init and threads are in gcdrive.cc
	  * Created flavors for original code versus event-based
	  * Small tweaks in comments and formatting for consistency

	Event-based code: the previous version of the code had replicated
	the sendPause and arbitrate functions in ActuationInterface, which
	meant that we had two versions of this code with the same logic but
	different calls to actuators.  I integrated these into a single
	function with more local changes based on GCMODULE_EVENT_BASED.
	This insures that if we change the state machine or other logic, we
	get the same behavior independent of the interface.

	Flavors: there are now two flavors of gcdrive: the 'plain' version is
	the same as before.  The 'events' version uses the new event-driven
	GcModule.  It creates a program called 'gcdrive-event-based' that
	makes calls to that code.  The new flavor has not undergone
	extensive testing yet.

	PLC: moved some of the functionality that was in ActuationInterface
	up to gcdrive.  This is consistent with the design choice of having
	the hardware interfaces instantiated at that level, with Actuation-
	Interface only having a logical link to the actuators.  Also
	modified the code so that if the PLC is not found then the program
	aborts rather than looking for the serial interface.
	
Release R1-01v (Tue Jul 17 20:04:11 2007):
	Fixed bug in autoshift when using the event base gcmodule version

Release R1-01u (Tue Jul 17 16:12:45 2007):
	Added support for the PLC. Nothing changes unless you specify the
	--use-plc option (-p for short)	on the command line. You can set the
	IP to connect to with -P xx.xx.xx.xx; if you don't, 192.168.0.75 is
	used. 

Release R1-01t (Thu Jul 12 18:11:02 2007):
	Modified ActuatorModule to inherit from gcmodule, and to support new
	event-based gcmodule paradigm.  All changes are ifdefed with
	GCMODULE_EVENT_BASED.

Release R1-01s (Wed Jul 11 11:01:20 2007):
	Fixed signal handler: now hitting CTRL-C will quit immediately and
	cleanly.  Also, killing with SIGTERM (i.e. 'killall gcdrive' or
	'kill <pid>') will quit cleanly.  If gcdrive is stuck, hit CTRL-C 3
	or more times to abort(), or kill with SIGKILL (i.e. 'killall -9
	gcdrive') as usual.

Release R1-01r (Tue Jul 10 14:18:29 2007):
	Put group information into the doxygen comments to separate out the
	different parts of the documentation a bit (especially the old
	adrive code, which will eventually go away).  Also added a
	mainpage.  No changes in functionality.

Release R1-01q (Mon Jun 18 15:10:47 2007):
	Added a debugging print to demonstrate how yam works.  No change in
	functionality. 

Release R1-01p (Mon Jun 11  2:13:27 2007):
	Field changes for adrive:
   	  * Disable now shifts to park and turns off engine
 	  * Added extra checks and locks for shifting

	Disable: to comply with DARPA rules, adrive now shifts Alice into
	park and turns off the ignition when it detects a disable command.
	Shifting to park only happens after you have stopped and ignition is
	turned off after that (but pretty much simultaneously).  This
	functionality has been tested in the field and is good to go for the
	site visit.

	Shifting: shifting between gears now does some additional checking
	to make sure it is safe (vehicle not moving) and also applies the
	brake during shifting, to insure we are really stopped.  If you are
	not commanding a brake and going at zero speed, the directive will
	be rejected.  Also modified the code so that the gear position is
	not updated until after the shift has occured (as measured by a
	sleep).  This functionality has been partially tested in the field
	and will get fine tuned tomorrow (11 June).

	gcdrive-viewer: this is a simple program intended to help with
	debugging from logs.  It just listens for whatever commands come
	from the logs (via skynet state messages) and displays the
	corresponding overview display.  More to come as features are
	required for debugging.

Release R1-01o (Sat Jun  9 23:07:37 2007):
	Increased the amount of time that we wait for the transmission to
	shift form 2 seconds to 4 seconds, based on problems in field tests.
	New delay has not yet been tested on Alice.  Note: when we get
	OBD=II working, we need to check to see if the gear has actually
	changed. 

Release R1-01n (Sat Jun  9  9:14:59 2007):
	Updated the turn signal relay information to match latest hardware
	configuration.  Tested and working in Alice. 

Release R1-01m (Fri Jun  8  8:51:12 2007):
	Added a command line argument to control the steering command
	limiter at low speed.  Use the --steer-speed-threshold argument to
	set this value.  By default, it is set to limit the commanded
	steering angle when the wheel speed is less than 5 rad/sec.  If you
	set the limit to a lower value, you will get more turning at low
	speeds.  If you 	set the threshold to 0, it will completely
	disable it, but you should only do this if trajfollower is fixed to
	not command large steering swings when we are stopped.

Release R1-01l (Thu Jun  7  9:18:39 2007):
	Several small updates:
	  * Fixed turn signal status to reflect new mapping of relays
	  * Added better handling of error messages (using --verbose)
	  * Attempted to get actuator resets working (commented out)
	  * Removed some warning messages that were no longer relevant

	This is mainly a maintenance released of 'adrive' that fixes one bug
	(errors in turn signal status) and improves some debugging
	information.  The intended change for this release, getting actuator
	resets working, turns out to be more complicated and won't happen
	before the site visit.

	Turn signals: there was a remapping of relays that occured in
	release R1-01k that was not reflected the the code that checks
	actuators status.  This is now fixed.  There is one piece of
	anomolous behavior: when the turn signals are commanded left (-1),
	the status sometimes returns hazard (2).  This means that the status
	message thinks that the right signals are also on.  There are some
	notes about the relay board not always returning the right status
	and I suspect it has something to do with this.  It is not an issue
	since the only use of the status is to display on the screen.

	Resetting actuators: I tried several approaches to this, but
	couldn't find one that works.  The root problem is that when you
	turn off an actuator, you get stuck in the status read.  This has a
	block mutex and so you can't talk to the actuator to reset it.  I
	tried killing the status thread and also using timeouts for the
	serial read, but for various reasons these didn't work.  The best
	cut code, using timeouts, is what is currently written, but it is
	commented out since it is not working (see code for the reasons
	why).

	Debugging messages: in the process of trying to get actuator reset
	to work, I updated several of the actuation modules so that they
	have more verbose error messages.  These are enabled by using the
	--verbose flag.  The rough meaning of differnt verbosity levels is:

	  * verbose=0	 no messages (external errors still come through)
	  * verbose=1	 internal error messages
	  * verbose=2	 internal warning messages
	  * verbose=3	 internal informational messages
	  * verbose=4	 internal debugging messages

	Turning verbose higher than 4 increases the level of detail you get
	in terms of messages.  Verbose=9 will give you everything.

Release R1-01k (Sat Jun  2 23:41:30 2007):
	Modified the header file to reflect the new mapping of the relays
	that Dom had to change on the relay board this afternoon when doing
	the turnsignal wiring. These mappings haven't been tested yet but I
	don't see any reason why it wouldn't work unless the mappings listed
	were incorrect.

Release R1-01j (Sat Jun  2 14:22:57 2007):
	Several updates:
	  * Added in function calls to control turn signals (in hardware)
	  * Added timeout logic for steering/accel (pause if no cmds recvd)
	  * Added verbose messages for debugging ssh problems

	Turns signals: calls Jeremy's turn signal interface.  Use
	'--disable-turnsig' to disable the calls to hardware (will still set
	values on the screen).  I updated the functional interface in
	turnsignals.cc a bit to be a bit less long winded about function
	names (Turnsignal::turnsignal_setpos seemed redundant).  I also put
	the unit test for the hardware in UT_turnsignals (instead of
	turnsignals_test).  You can also set the address of the hardware
	(IP) port that we talk wto with the '--turnsig-port' argument.

	Timeout logic - by default, gcdrive now goes into pause mode if it
        does not receive either steering or acceleration commands at least
        once every 0.5 seconds.  Use '--disable-command-timeout' to disable
        this and '--command-timeout' to set the amount of time to wait.
        This has been tested in simulation but not in hardware (should be
        the same though).

	Minor things - cleaned up some of the logic that is used when you
        disable an actuator and put in some messages to try to help debug
        the problems with ssh (use --verbose=9 to turn these on).

Release R1-01i (Fri Jun  1 16:59:40 2007):
	(Release notes by jeremy) I noticed I had committed the wrong
	version of the turnsignal source files yesterday and asked Nok to
	make a release for me since she already had the latest check out of
	adrive. She kindly consented. This version has function calls that
	are more consistent with the other actuator interfaces in adrive. 

Release R1-01h (Thu May 31 17:30:25 2007):
	Added the source files for the turnsignal actuation class
	(turnsignals.cc/hh). To see how to make use of this class, look at
	turnsignal_test.cc. I should note that I didn't make any changes to
	the Makefile so when this class is used, the Makefile will need to
	be changed.

	These are the basic command calls to actuate the turnsignals:

	turnsignal_open()
	turnsignal_init()
	turnsignal_setpos(TurnType signal)
	turnsignal_getpos(TurnType *signal)
	turnsignal_close()

	I should note that the turnsignal_getpos() function does not
	accurately return the true setting of the turnsignals. The relay
	switches will sometimes be detected as "on" when in reality they are
	off; this in turn causes a false reading of the relay switch
	state. I've triple checked this phenomena and am quite sure this is
	not a bug in my code. The relay board just doesn't have very
	accurate readings of the relay status.

	Nonetheless, if you send a command to signal, you can be assured
	that the turn signal was commanded so you shouldn't really need to
	check the state of the signal. If in the event you absolutely need
	to check the signal, then about 80% of the time, you'll get an
	accurate reading.

Release R1-01g (Sun May 27  3:49:39 2007):
	A couple of minor updates + new steering calibration program.

	A new 'steercal' program has been written to use for recalibrating
	the steering.  This program spits out verbose output and prompts the
	user for what needs to be done.  Roughly, the wheels should be
	pointed straight and then the encoder position will be reset to zero
	and the motor controller parameters re-initialized.

	Small fixes: the brake is now pressed before transmission is shifted
	(or engine started), to insure that we don't move at startup.  The
	logic for operation when the -b and -g flags are given has been
	improved to allow better operation without gas and brake (for
	testing). 

	Still to do: reset logic for throttle and brake is not yet fixed.
	CPU usage in simulation is high.  Haven't added timouts for when
	commands aren't received.

Release R1-01f (Fri May 25  9:06:20 2007):
	Added --auto-shift option to have adrive automatically shift into
	drive on start up.  First cut at reset options for actuators - press
	the label to reset the appropriate actuation (steering works, others
	don't yet).  

Release R1-01e (Tue May 22 15:28:38 2007):
	Updated the estop logic to make sure that when you put the estop in
	pause that brake is commanded.  We should test this in the field by
	moving in and out of pause while velocities are being commanded from
	trajfollower (the logic seems sound, but just to make sure).

	Also added logic to turn off the turn signals after you turn past
	a limit positon (0.5) and then back past a second lower limit
	(0.2).  This should roughly replicated what happens with physical
	turn signals, although we will probably need to tune the limits
	based on testing (they are in TurnSignalModule.hh).

	Finally, got rid of various magic numbers in sleeps times and tried
	to use a consistent set of constants for various rates.  FAST_SLEEP
	is the sleep time to use for fast actuators (steer, accel); it is
	currently 1 msec.  SLOW_SLEEP is used for slow actuators (trans,
	turn) and is currently set to 10 msec.  Finally, STATUS_SLEEP is
	used for status loops and is 250 msec.  Some actuators use multiples
	of these values (eg, slow actuators do status poling at
	2*STATUS_SLEEP).  Note that only the actuation sleeps are in the
	control loop and only the steer/accel loops are in high bandwidth
	loops (currently 20 Hz = 50 msec => 1 msec is minor phase delay
	contribution). 

Release R1-01d (Mon May 21 23:47:14 2007):
	Added turn signal actuation module to handle turn signals.  This
	module currently accepts directives but doesn't actually do anything
	besides change the commanded value on the display.  As soon as the
	interface is up and running (hopefully this week), the module will
	be updated to actuate the signals. 

	A unit test, UT_turnsig, illustrates how to communicate with the
	turn signal device via a the AdriveCommand GcInterface.

Release R1-01c (Thu May 17 14:18:18 2007):
	Added "fake" OBD-II functionlality to gcdrive, along with gear
	shifting and steering limits based on this information.  By default,
	gcdrive will receive VehicleState messages and use this to determine
	the equivalent OBD-II status information.  Shifting gears now
	requires that the vehicle be stopped (or moving slowly), otherwise
	the gear change directive is rejected.  In addition, steering at
	slow speed is limited based on the speed below a threshold.  In
	particular, at zero speed the commands to turn are basically
	ignored, which should limit wear and tear on Alice's steering
	hardware. 

	Tested in simulation; will test on Alice tonight.

Release R1-01b (Wed May 16 21:45:09 2007):
	Added (nominal) transmission handling.  When a shift is commanded,
	all other directives to Adrive are blocked (return status =
	rejected) until the transmission shift is completed.  Completion is
	is currently decided by a timer [sleep(2)] since we don't yet have
	the OBD-II interface working.  Tested in simulation against the
	UT_fwdbwd unit test in rddfplanner.  Ready for field testing.

Release R1-01a (Wed May 16  7:56:37 2007):
	This is a new release of 'gcdrive' with full estop processing,
	including a five second delay when going from pause to run.  Estop
	status is read from the DARPA estop sensor.  You must be in "Run"
	mode on the estop unit in order for gcdrive to process directives.

	To run in simulation, using the --simulate flag to gcdrive and run
	asim with the --gcdrive option.  This will cause gcdrive to
	broadcast actuator state based on what it reads from the simulator
	and allows full testing of the gcdrive logic.  You can use the
	latest release of asim to set the status of the estop unit for
	testing.

	Details: this release uses a small finite state machine to keep
	track of estop status.  In the "Running" state, gcdrive passes
	through diretives to individual actuators.  In the "Pause" state
	(trigged by estop), only steering commands are accepted.  All other
	directives are rejected with VehiclePaused as the reason.  When
	estop is switched to run, gcdrive entires the "Resuming" state for
	five seconds.  Directive processing is the same as "Pause" mode
	until we switch to "Running".  A "Shifting" mode is also included,
	but not yet implemented (this will be used to make sure we don't
	move while changing gears).

Release R1-00t (Fri May  4  7:16:28 2007):
	Added logic to set both throttle and brake appropriately when
	commanding an acceleration in hardware.  This fixes the problem of
	having both the brake and throttle on at the same time.

	Note: this code can only be tested on Alice (since the hardware has
	to be present).  Don't forget to use the '-g' option to disable the
	throttle (gas) and the '-b' option to disable the brake if you are
	running with the switches for one or the other turned off at the
	driver's station.  Failure to do this will cause gcdrive not to
	command changes to either of them.

Release R1-00s (Thu May  3 11:12:47 2007):
	* Added comments and clean up the logger for ActuatationInterface
	* Decrease the sleep in SteeringModule from 5000 to 1000. (5000 is ok when running on my mac but when I ran it on a lab machine, the steering module dropped quite a lot of directives.)

Release R1-00r (Thu May  3  5:25:09 2007):
	* Fixed the delay problem when running with trajfollower --gcdrive. ActuationInterface now uses the lightweight GcInterface and is no longer a gcmodule.
	* Added command line option for setting log level. If log level > 0, 4 log files will be created. By default, the name of the log file looks like 2007-05-03-Thu-05-39-gcdrive-{acceleration, actuationInterface, steering, transmission}.log.
	** Log level 1: all the error messages and responses whose status is either FAILED or REJECTED.
	** Log level 2: all the id's of received direcrtives with timestamp + everything from log level 1
	** Log level 7: clean up in GcInterface + everything from log level 2
	** Log level 8: all the received and output directives and responses with timestamp + everything from log level 7
	** Log level 9: all the merged directives and control status + everything from log level 8 

Release R1-00q (Sun Apr 29 20:17:43 20:07):
	Initial implementation of Pause directive.  Currently fairly limited
	functionality: press the brake and blocks any other commands.  This
	directive is intended primarily for internal use (eg, so that
	Transmission can prevent motion during shifting).

Release R1-00p (Sun Apr 29 14:50:07 2007):
	Fixed estop status; this is now read from hardware and broadcast via
	the ActuatorState.  If you disable the estop (using
	--disable-estop), the status will return as estopRun (2).

Release R1-00o (Thu Apr 26 23:22:52 2007):
	* Fixed all sorts of segfault problems (Yay Josh, we made it!!!)
	* Limit the number of directives/responses stored in GcInterface to 10
	* Use in-process communication
	* Still need to figure out the bad ID problem

Release R1-00n (Thu Apr 26  7:26:12 2007):
	Updated to work with the latest release of GcModule (R1-01a) and
	also to use faster rates for responding to steering and acceleration
	directives (up to 100 Hz).  There is also a new unit test,
	UT_timing, that can be used to send directives at a high rate.  This
	unit test currently produces a core dump, possible due to the fact
	that GcModule is not yet flusing its queue.

Release R1-00m: aborted

Release R1-00l (Sun Apr 22 22:52:10 2007):
	This version has been tested in Alice.  Steering, brake, throttle
	and transmission all work.  There appear to be some issues with
	estop, but these can't be checked until the estop is working again.
	Interlocks are still not implemented, but this version should be
	fine for basic testing of trajfollower.

	This version also has the AdriveCommand interface moved to the
	gcinterfaces module.

Release R1-00k (Sun Apr 22  8:59:50 2007):
	Added the brake, gas and transmission actuators, plus the estop
	"sensor".  There are currently no interlocks, so the actuators just
	do what you tell them.  Directives are rejected if the commands are
	not in range.  All actuators work with asim when the --simulate flag
	is given.  Steering and estop have been tested on Alice (brake, gas
	and transmission will be tested later today).

Release R1-00j (Sat Apr 21 18:10:17 2007):
	This release of adrive has been tested on Alice and is able to
	properly command and read the steering angle.

Release R1-00i (Sat Apr 21 11:29:44 2007):
	This release of the adrive module updates the GcModule 
	implementation (gcdrive) to use the latest release of gcmodule and
	gcinterface (gcmodule-R1-00j and gcinterfaces-R1-00f).  It also
	fixes up some bugs in Makefile.yam so that the display tables get
	handled correctly.  Everything should work the same as before from
	an interface and user perspective.

Release R1-00h (Wed Apr 18 20:58:35 2007):
	This relaese of the adrive module has a first cut at a gcmodule
	implementation of adrive, called 'gcdrive'.  At present, gcdrive can
	only handle steering commands and has not yet been verified to work
	on Alice hardware.  However, it can be used in "simulation" mode, in
	which case it forwards commands from the GcInterface to asim.

	To use 'gcdrive' in this mode, run 'gcdrive --simulate'.  You can
	set the steering angle from the sparrow display or you can use the
	unit test UT_adrive to send a command.  If you run asim at the same
	time, you should see the command come through.

	This version requires gcmodule-R1-00i as well as sparrow-R1-00e in
	order to compile properly.  The 'adrive' program is unchanged and
	should function as before.

Release R1-00g (Sat Mar 17 19:46:47 2007):
	Added better error logging (added cerr output to logger function)
	and also fixed a latent bug in the way gas commands are handled
	(wasn't checking if transmission was enabled before looking at
	whether we are in park; doesn't matter unless interlocks are on).

	This version should be ready for field testing.  Make sure that
	interlocks are off in the config file (this is the default),
	otherwise you won't be able to command throttle.

Release R1-00f (Sat Mar  3  9:29:32 2007):
	Updated to use --skynet-wkey + latest version of interfaces

Release R1-00e (Tue Feb 20 15:45:27 2007):
	Fixed include statements so this revision of adrive compiles against
	revision R2-00a of the interfaces and the skynet modules 

Release R1-00d (Sat Feb 10 19:39:11 2007):
	Added printout of reason for switching into adrive pause.  These
	will print on the bottom of the sparrow screen => should help in
	figuring out why adrive is paused.

	Also put in configuration file search using dgcFindConfigFile.

Release R1-00c (Sat Jan 20 14:22:28 2007):
	This is a 'stable' version of adrive that has been tested on 
	Alice and seen to work.  This version also addresses bug
	3079 (hang on missing config file).

Release R1-00b (Sat Jan 13 20:49:35 2007):
	Minor release to use new interface structure (Actuator*.h)

Release R1-00a (Wed Jan 10 23:12:00 2007):
	Initial working version of adrive under YaM.  This version has
	the actuators copied into the adrive module (they aren't used
	elsewhere) and has the timber logger commented out (will remove
	permanently later).  The module function corresponds to the most
	recent release from dgc/trunk, including a new summary display
	page (old page available via a button at the bottom of screen).

Release R1-00 (Wed Jan 10 17:32:47 2007):
	Created.




























































