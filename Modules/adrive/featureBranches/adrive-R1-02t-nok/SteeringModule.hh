/*!
 * \file SteeringModule.hh
 * \brief Header file for SteeringModule class
 *
 * \author Nok Wongpiromsarn and Richard Murray
 * \date April 2007
 *
 * \ingroup gcmodule
 *
 * This file defines the SteeringModule class for adrive.  This is
 * used to communicate with the steering actuator.  Most of this is
 * just a pass through for the directives and responses that are
 * routed through adrive.
 */

#ifndef __SteeringModule_hh__
#define __SteeringModule_hh__

#include <pthread.h>
#include <string>
#include "gcmodule/GcModule.hh"
#include "sparrow/display.h"

#include "ActuationInterface.hh"

class SteerControlStatus : public ControlStatus
{
public:
  unsigned id;			// ID for the SteerDirective
  AdriveFailure reason;

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " parent_id: " << parent_id << " status: "
      << status << " reason: " << reason;
    return s.str();
  }
};

/*
 * \class SteerMergedDirective
 * 
 * This is the directive sent between Steering Arbitration and
 * Steering Control.
 */

class SteerMergedDirective : public MergedDirective
{
public:
  unsigned id;			///! Identifier for this directive
  AdriveCommandType command;	///! Command to execute
  double arg;			///! Value for the command 

  /*! ID of the parent directive, used by adrive for its response */
  unsigned parent_id;
  std::string toString() const {
    stringstream s("");
    s << "id: " << id << " command: " << command << " arg: " << arg;
    return s.str();
  }
};

/*
 * \class SteeringModule
 *
 * This class is the actual GcModule for steering. 
 */

class SteeringModule : public GcModule
{
private:
  /*!\param control status sent from control to arbiter */
  SteerControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  SteerMergedDirective m_mergedDirective;

  /*!\param GcInterface variable */
  AdriveSteerInterface::Northface* adriveSteerInterfaceNF;

  /*!\param Actuation interface */
  ActuationInterface *adrive;

  /*! Arbitration for the steering control module. It computes the next
      merged directive based on the directives from actuation control
      and latest control status */
  void arbitrate(ControlStatus *, MergedDirective *);
  unsigned m_latestID;		// ID for most recently received command
  unsigned m_currentID;		// ID for currently executing command

  /*! Control for the steeringcontrol module. It computes and sends
      directives to all its controlled modules based on the 
      merged directive and outputs the control status
      based on all the status from its controlled modules. */
  void control(ControlStatus *, MergedDirective *);
  double commandAngle;
  int controlCount;
  unsigned long long command_time;	// time cmd last received
  double STEER_SPEED_THRESHOLD;		// threshold for setting rate limits

  /*! Status for the steering control module.  This function is called
      as a thread and updates the current status of the steering
      subsystem */
  void status();
  bool m_bStopStatus;
  int statusCount;
  long statusSleepTime;
  DGCcondition m_condStatusSignal;

  /* Status information */
  int initialized;		/*!< Steering is properly initialized */
  int connected;		/*!< Able to talk to the motor controller */
  double currentAngle;
  int enabled;			/*!< Check if drive is enabled */
  long motor_status;		/*!< Motor status bits */
  int verbose;			/*!< Turn on verbose error messages */

  /*! Mutex controlling access to serial port and data */
  pthread_mutex_t actuatorMutex;

public:
  /*! Constructor */
  SteeringModule(int, ActuationInterface *, int verbose = 0);
  void initSparrow(DD_IDENT *, int);
  bool Stop();
  ~SteeringModule() {
    DGCdeleteCondition( &m_condStatusSignal );
    stringstream ss;
    AutoMutex::getWaitLog(ss);
    gclog(2) << ss.str() << endl;
  }
  // used to stop the status thread
  pthread_t m_thrStatus;
};

#endif
