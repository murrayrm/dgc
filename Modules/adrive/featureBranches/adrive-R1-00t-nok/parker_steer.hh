

#ifndef STEERCAR_H
#define STEERCAR_H

#include <math.h>
#include <string>
/* #include "../include/Constants.h" */
/* #include "../Msg/dgc_const.h" */

#define TRUE 1
#define FALSE 0
#define ERROR -1
#define STEER_CALIBRATE 2
using namespace std;


//#define STEER_SERIAL_PORT  0  //kill because passing serial port now  JCL 01/30/04 2250
#define MIN_LINE_LEN    1       // Arbitrary.
#define MAX_LINE_LEN    55      // Limitation of the servo controller
#define MAX_NUM_LINES   3       // Really, shouldn't have more than two.
#define STEER_BUFF_LEN  MAX_LINE_LEN * MAX_NUM_LINES
#define POWERON_TIMEOUT 2       // Seconds to wait until the device has power.
#define SEC_TIMEOUT     5       // Timeout in seconds for serial_read_until().
#define RESET_SLEEP     5
#define VEL_BUFF_LEN    10
#define ACCEL_BUFF_LEN  10


#define STEER_EOL       '\n'
#define STEER_EOT       '\n'

#define ALICE_POS_STEER_LIMIT  80000
#define ALICE_NEG_STEER_LIMIT  -75000

#define ALICE_STEER_MAX_VEL    16
#define ALICE_STEER_MAX_ACCEL  10

#define ALICE_CENTER 10

// use STEER_SERIAL or STEER_SDS
#define STEER_SERIAL

#ifndef STEER_SERIAL
#define STEER_SDS
#endif //serial


//# define blocking         TRUE
    
///////////////////////////////////////////////////////////////////////////////
//              PROTOTYPES
///////////////////////////////////////////////////////////////////////////////
//std embedded systems team function defs
int steer_open(int);
int steer_init(void);
int steer_calibrate(void);
int steer_close(void);
int steer_zero(void);
int steer_setzero(void);
int steer_setposition(float);
int steer_setaccel(float);
int steer_setvel(float);
int steer_OK(void);
//int steer_status(int, struct *steerstatus);  //will need eventually, commented to compile
int steer_getposition(void);
double steer_getheading(void);
double steer_getaccel(void);
double steer_getvel(void);
int steer_pause(void);
int steer_resume(void);
int steer_enable(void);
int steer_disable(void);
int steer_enable_overide(void);
int steer_enabled_check(void);
double steer_get_heading(void);

int steer_fault_reset();
int steer_exec_cmd(int, int);
/*!
 * steer_heading
 *
 * Description:
 *      This function instructs the car to steer the heading, but does not wait
 *      for that heading to actually be achieved until returning.... it assumes
 *      that COMEXC1 has already been issued.
 *
 * Arguments:
 *      float angle     direction: full left is -1, center is 0, full right is
 *                      1.
 *
 * Return Values:
 *      TRUE    in best faith, the code thinks we steered there.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 */
int steer_heading (double);
int steer_set_sw_limits(void);
int steer_test_communication (int);
int steer_upload_file (string);
int steer_state_update (int log = FALSE, int updateAll = FALSE);
int steer_state_moving (int blocking = TRUE);
int steer_state_limit_pos (int blocking = TRUE);
int steer_state_limit_neg (int blocking = TRUE);
int steer_state_home_complete (int blocking = TRUE);
int steer_state_faulted (int blocking = TRUE);
//int steer_state_TPE (int blocking = TRUE);  //replaced by steer_getposition

///////////////////////////////////////////////////////////////////////////////
//              ERROR CODES
///////////////////////////////////////////////////////////////////////////////
enum ERROR_TYPES {
    ERR_NONE,           // No error -- initialization value.
    ERR_ILL_RETVAL,     // Illegal return value encountered.
    ERR_ARG_RANGE,      // Argument is out of range.
    ERR_SERIAL_IO,      // Serial I/O is not working.
    ERR_FILE_IO,        // File I/O error.
    NUM_ERR
};


struct steerCmd {
    string cmd;         // The command to be issued.
    string retVal;      /* The expected return value on no errors, including
                           the echo of the command and the EOL and EOT 
                           characters. */
};

///////////////////////////////////////////////////////////////////////////////
//              STEERING COMMANDS AND EXPECTED REPLIES
//
// The replies include all expected line terminators, etc.
///////////////////////////////////////////////////////////////////////////////
enum {
    // Setting parameters
    S_A10,
    S_A15,
    S_A20,
    S_A40,
    S_COMEXC0,
    S_COMEXC1,
    S_COMEXL1,

    S_DMTLIM4,
    S_DMTLIM8,
    S_DMTLIM16,
    S_DRIVE0,
    S_DRIVE1,
    S_E0,
    S_E1,
    S_ECHO0,
    S_ECHO1,
    S_EOL,
    S_EOT,
    S_ERRLVL0,
    S_LH0,
    S_LH3,
    S_LS0,
    S_LS3,
    S_LSAD,
    S_LSNEG,
    S_LSPOS,
    S_MA0,
    S_MA1,
    S_PSET0,
    
    // available velocity settings
    S_V1,
    S_V2,
    S_V4,
    S_V8,
    S_V12,
    S_V16,
    S_V20,
    S_V32,

    S_ASCII1,
    S_HOMBAC0,
    S_DMLCLR,
 
    //servo ratios
    S_SGIRAT1,
    S_SGIRAT15,
    // Status Checking

    C_ASCII1,
    C_COMEXC0,
    C_COMEXC1,
    C_COMEXL1,
    C_DRIVE0,
    C_DRIVE1,
    C_EOL,
    C_EOT,
    C_ERRLVL0,
    C_LH0,
    C_LH3,
    C_LS0,
    C_LS3,
    C_MA0,
    C_MA1,
    C_TASF,
    C_TREV,



    // COther Commands
    
    IMGO,
    GO,
    HOM0,
    HOM1,
    HOM,
    RESET,
    RFS,
    STOP,
    WAIT_MOVE_DONE,
    WAIT_HOME_REACHED,
    NUM_STEER_CMDS
};

//
// Expected commands and return values.
//
// page 102 of the manual: "[E]ach line line of the response will have the EOL
// characters. The last line in the response will have the EOT characters. If
// the response is only one line long, the EOT characters will be placed after
// the response, not the EOL characters."
//
// COMMENTS:
// Let me point out that that is bullshit.  After testing, it became apparent
// that when the response is only one line long, only the EOL characters are
// returned, flatout contradicting the manual.  I will email cfoster@parker.com
// about this...
//
const steerCmd steerCmds[NUM_STEER_CMDS] = {
    //
    // Setting parameters
    //
    
    // Steering in rev/s
    { string ("A10") + STEER_EOT, "" },
    { string ("A15") + STEER_EOT, "" },
    { string ("A20") + STEER_EOT, "" },
    { string ("A40") + STEER_EOT, "" },
    //    { string ("ASCII1") + STEER_EOT, "" },       // set the controller to use ascii communication mode
    { string ("COMEXC0") + STEER_EOT, ""},      // Use non-continuous command execution mode
    { string ("COMEXC1") + STEER_EOT, ""},      // Use continuous command execution mode (default for this application).
    { string ("COMEXL1") + STEER_EOT, ""},      // Continue Command Execution on Limit.
    // Torque Limit in Nm, limited normally to 13.4
    { string ("DMTLIM4") + STEER_EOT, ""},
    { string ("DMTLIM8") + STEER_EOT, ""},
    { string ("DMTLIM16") + STEER_EOT, ""},
    { string ("DRIVE0") + STEER_EOT, "" },      // Enable the motor.
    { string ("DRIVE1") + STEER_EOT, "" },      // Enable the motor.
    { string ("E0") + STEER_EOT, "" },          // Enable Communication.
    { string ("E1") + STEER_EOT, "" },          // Disable Communication.
    { string ("ECHO0") + STEER_EOT, "" },       // Do not echo commands.
    { string ("ECHO1") + STEER_EOT, "" },       // Do not echo commands.
    { string ("EOL10,0,0") + STEER_EOT, "" },   // Set the end-of-line character.
    { string ("EOT10,0,0") + STEER_EOT, "" },   // Set the end-of-transmission character.
    { string ("ERRLVL0") + STEER_EOT, "" },     // Set the error level to zero, so nothing is prompted.
    { string ("LH0") + STEER_EOT, ""},          // unset hardware limiting.
    { string ("LH3") + STEER_EOT, ""},          // Set the hardware limiting to use both directions.
    { string ("LS0") + STEER_EOT, ""},          // unset software limiting.
    { string ("LS3") + STEER_EOT, ""},          // Set the software limiting to use both directions.
    { string ("LSAD100") + STEER_EOT, ""},      // Set the software decceleration.
    { string ("LSNEG-100") + STEER_EOT, ""},    // Set the negative software limit.
    { string ("LSPOS100") + STEER_EOT, ""},     // Set the positive software limit.
    { string ("MA0") + STEER_EOT, "" },         // Set to relative movement.
    { string ("MA1") + STEER_EOT, "" },         // Set to absolute movement (shoulde be the default).
    { string ("PSET0") + STEER_EOT, "" },       // Set the current position as the home (d=0).
    // Steering Velocities in rev/s
    { string ("V1") + STEER_EOT, "" },
    { string ("V2") + STEER_EOT, "" },
    { string ("V4") + STEER_EOT, "" },
    { string ("V8") + STEER_EOT, "" },
    { string ("V12") + STEER_EOT, "" },
    { string ("V16") + STEER_EOT, "" },
    { string ("V20") + STEER_EOT, "" },
    { string ("V32") + STEER_EOT, "" },

    { string ("ASCII1") + STEER_EOT, "" },
    { string ("HOMBAC0") + STEER_EOT, "" },
    { string ("DMLCLR") + STEER_EOT, "" },
    
    //servo ratios
    { string ("SGIRAT1") + STEER_EOT, "" },
    { string ("SGIRAT1.5") + STEER_EOT, ""},
   //
    // Checking Parameters
    //

    { string ("ASCII") + STEER_EOT, steerCmds[S_ASCII1].cmd },
    { string ("COMEXC") + STEER_EOT, steerCmds[S_COMEXC0].cmd },      // Continuous command execution mode.
    { string ("COMEXC") + STEER_EOT, steerCmds[S_COMEXC1].cmd },      // Non-Continuous command execution mode.
    { string ("COMEXL") + STEER_EOT, steerCmds[S_COMEXL1].cmd },      // Continue Execution on Limit.
    { string ("DRIVE") + STEER_EOT, steerCmds[S_DRIVE0].cmd },        // Is the drive enabled?
    { string ("DRIVE") + STEER_EOT, steerCmds[S_DRIVE1].cmd },        // Is the drive enabled?
    { string ("EOL") + STEER_EOT, steerCmds[S_EOL].cmd },      // This is what we set below.
    { string ("EOT") + STEER_EOT, steerCmds[S_EOT].cmd },      // This is what we set below.
    { string ("ERRLVL") + STEER_EOT, steerCmds[S_ERRLVL0].cmd },      // This errorlevel prompts nothing.
    { string ("LH") + STEER_EOT, steerCmds[S_LH0].cmd },      // The hardware locks are on in both directions.
    { string ("LH") + STEER_EOT, steerCmds[S_LH3].cmd },      // The hardware locks are on in both directions.
    { string ("LS") + STEER_EOT, steerCmds[S_LS0].cmd },      // The software locks are on in both directions.
    { string ("LS") + STEER_EOT, steerCmds[S_LS3].cmd },      // The software locks are on in both directions.
    { string ("MA") + STEER_EOT, steerCmds[S_MA0].cmd },      // Steering commands are absolute.
    { string ("MA") + STEER_EOT, steerCmds[S_MA1].cmd },      // Steering commands are absolute.
    { string ("TASF") + STEER_EOT, string ("") },        // Really long text output, no expected value.
    { string ("TREV") + STEER_EOT, string ("*TREV-GV6-U12E_D2.10_F1.62_T1.41") + STEER_EOT },// Do we have communication with the device?

    //
    // Other Commands
    // 
    { string ("!GO") + STEER_EOT, "" },          // Go ... execute movement comand.
    { string ("GO") + STEER_EOT, "" },          // Go ... execute movement comand.
    { string ("HOM0") + STEER_EOT, "" },        // Home in positive direction.
    { string ("HOM1") + STEER_EOT, "" },        // Home in negative direction.
    { string ("HOM") + STEER_EOT, "" },
    { string ("!RESET") + STEER_EOT, "" },
    { string ("!RFS") + STEER_EOT, "" },
    { string ("!S") + STEER_EOT, "" },           // Stop everything, normall executed as '!'+S
    { string ("WAIT ( AS.0 = b0 )") + STEER_EOT, "" },           
    { string ("WAIT ( AS.5 = b1 )") + STEER_EOT, "" },          


};





#endif
