#!/usr/bin/perl -w

use Socket;

socket SOCK, PF_INET, SOCK_STREAM, 0;
connect SOCK, sockaddr_in 8003, inet_aton "192.168.0.71" or die "connect: $!\n";

select STDOUT; $| = 1;
print ">";

my($rout, $rin);

$rin = '';
vec($rin, fileno(STDIN), 1) = 1;
vec($rin, fileno(SOCK), 1) = 1;

while (select ($rout = $rin, undef, undef, undef)) {
    if (vec($rout, fileno(STDIN), 1)) {
        $_ = <STDIN>;
        s/\s+$//;
        $_ .= "\r";
        syswrite SOCK, $_;
    }
    if (vec($rout, fileno(SOCK), 1)) {
        sysread SOCK, $_, 80;
        s/\r/\n/g;
        print;
    }
}

print "exiting\n";
