/*!
 * \file adrive.dox
 * \brief Main documentation page
 *
 * \author Richard M. Murray
 * \date 9 July 2007
 *
 * This file contains the mainpage documentation for the adrive module.
 *
 */

/*!
 * \mainpage Adrive Module Documentation
 * \section Introduction
 *
 * The adrive module contains the inerfaces to the actuators on Alice.
 * The original version of the program, 'adrive', was used in the 2005
 * DARPA Grand Challenge.  The current veresion of the program,
 * 'gcdrive', uses the
 * <a href="http://gc.caltech.edu/wiki/Canonical_software_structure">
 * canonical software structure</a> for consistency with the planning stack.
 *
 * The adrive module consists of several code groups (doxygen modules):
 * <ul>
 * <li> @ref gcdrive "gcdrive" - user and CSS interface (including FSM)
 * <li> @ref gcmodule "gcmodules" - CSS modules for each actuator
 * <li> @ref gcsensor "sensors" - interface classes for each sensor
 * <li> @ref serial "serial interfaces" - low-level actuator interface code
 * <li> @ref serial "network interfaces" - low-level network interface code
 * <li> @ref unittest "unit tests" - unit test programs and libraries
 * <li> @ref utility "utility programs" - auxilliary programs
 * <li> @ref adrive "adrive" - deprecated actuator interface
 * </ul>
 *
 * <a href="http://gc.caltech.edu/wiki/gcdrive">User documentation</a>
 * for @ref gcdrive is available on the Team Caltech wiki.
 * 
 * @defgroup gcdrive   Gcdrive
 * @defgroup gcmodule  Gcdrive actuation modules
 * @defgroup gcsensor  Gcdrive sensors
 * @defgroup serial    Gcdrive serial devices
 * @defgroup network   Gcdrive network devices
 * @defgroup unittest  Gcdrive Unit tests
 * @defgroup utility   Gcdrive utility programs
 * @defgroup adrive    Adrive (deprecated)
 * 
 */
