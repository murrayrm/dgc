/*!
 * \file parker_steer.cc
 * \brief Serial interface to Parker steering controller
 *
 * \ingroup serial
 *
 */

//Modified 2-3-05 by David Rosen for use with SDS.
//Finalized 2-16-05
// modifid 08 Jun 2005 by JCL - added ifdefs to sw btw sds and serial

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "dgcutils/DGCutils.hh"
#include "parker_steer.hh"

#define FALSE 0
#define TRUE 1

#define STEER_IP "192.168.0.72"
#define STEER_PORT 8000

#include <SerialStream.h>
using namespace std;
using namespace LibSerial;

int steer_negCountLimit;
int steer_posCountLimit;
int STEER_BY_HAND;
int steer_errno;
int steer_cmdangle;		// last commanded steering angle
int steer_state = 0;		// debugging state
int steer_write_count;		// count number of writes

static int verbose = 0;		// set verbosity level of module

int fault_check_count = 0;
int steer_enabled = TRUE;

static int serial_use_dm500 = 0;
static SerialStream serial_port;
static int serial_dm500_port;

int steer_serial_port = -1;

/*!
 * \brief Writes all the given data to the serial port (whether via
 * DM500 or directly), using multiple write calls if necessary.
 *
 * \return 1 if everything was written, 0 otherwise.
 */
static int serial_write (const void *bufv, int len) 
{
  if (serial_use_dm500) {
    const char *buf = (const char *)bufv;
    int wrlen = 0;
    while (len > 0) {
      int res = write (serial_dm500_port, buf, len);
      if (res <= 0) return 0;
      wrlen += res;
      buf += res;
      len -= res;
    }

    return 1;
  } else {
    return (serial_port.write ((char *)bufv, len) != NULL);
  }
}

/**
 * \brief Reads data from the serial port until the given \a endchar is encountered.
 *
 * If \a endchar is zero, reads until a read blocks.
 *
 * \return 1 on success, 0 for any sort of error.
 */
static int serial_read (void *bufv, int len, char endchar = STEER_EOT) 
{
  if (!serial_use_dm500) {
    if (!endchar) {
      serial_port.sync();
      return 1;
    } else {
      return (serial_port.getline ((char *)bufv, len, endchar) != NULL);
    }
  }
  
  char *origbuf = (char *)bufv;
  char *buf = origbuf;
  int sofar = 0;
  
  struct pollfd poller;
  poller.fd = serial_dm500_port;
  poller.events = POLLIN;
  
  while (1) {
    int res;
    
    /* Wait for data to read. */
    if (!poll (&poller, 1, 1000))
      return 0;           /* timeout */
    
    /* Read in one char. */
    res = read (serial_dm500_port, buf, 1);
    if (res < 0)
      return 0;           /* error */
    if (res == 0)
      return 0;           /* eof */
    
    /* Reached end yet? */
    if (*buf == endchar)
      break;
    
    /* Advance pointer and counter. */
    buf++;
    sofar++;
    
    /* Reached end of buffer? */
    if ((sofar % len) == 0)
      buf = origbuf;
  }
  
  /* Trim whitespace from the end. */
  while (--buf >= origbuf && isspace (*buf))
    ;
  
  buf++; // now buf points to the char past the end
  *buf = 0; // NUL-terminate it
  
  return buf - origbuf;       /* length */
}

/*!
 * \function steer_open (int port)
 * 
 * Author: JCL, 1/30/04
 * Revisions:
 *   17 Apr 07  split into steer_open() and steer_init()
 *
 * opens the steering assuming that power has not been shut off
 * performs minor communication setup: ascii mode on, echoing off
 * returns: true means we opened the port and set the parametmers
 *          false means that we couldn't initialize the port
 *          error means that we opened the port, but couldn't set parameters
 *
 */

int steer_open(int port, int verbose_flag)
{
  steer_serial_port = port;
  verbose = verbose_flag;

  if (port < 0) {
    serial_use_dm500 = 1;
    serial_dm500_port = socket(PF_INET, SOCK_STREAM, 0);
    if (serial_dm500_port < 0)
      return ERROR;
    struct sockaddr_in sin;
    memset (&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr (STEER_IP);
    sin.sin_port = htons (STEER_PORT);
    if (connect (serial_dm500_port, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
      close (serial_dm500_port);
      serial_dm500_port = -1;
      return ERROR;
    }
  } else {
    // if the port is open, close it
    if(serial_port.IsOpen()) serial_port.Close();

    ostringstream portname;
    portname << "/dev/ttyS" << port;
  
    // open the port and set it up with the proper parameters
    serial_port.Open(portname.str());
    if ( ! serial_port.good() ) {
      cerr << "steer_open() error: Could not open " << portname.str() << endl ;
      return FALSE;
    }

    // set baud rate = 9600
    serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
    if ( ! serial_port.good() ) {
        cerr << "steer_open() error: Could not set the baud rate." << endl ;
        return ERROR;
    }

    // set parity = none
    serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
    if ( ! serial_port.good() ) {
        cerr << "steer_open error: Could not disable the parity." << endl ;
        return ERROR;
    }

    // set software flow control on
    serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_SOFT ) ;
    if ( ! serial_port.good() ) {
      cerr << "steer_open error: Could not turn on software flow control."
 	   << endl;
      return ERROR;
    }

    // set one stop bit
    serial_port.SetNumOfStopBits( 1 ) ;
    if ( ! serial_port.good() ) {
      cerr << "steer_open error: Could not set the number of stop bits."  << endl;
      return ERROR;
    }
    // set 8-bit character size
    serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
    if ( ! serial_port.good() ) {
      cerr << "steer_open() error: Could not set the char size." << endl ;
      return ERROR;
    }
  }
  return TRUE;
}

/*!
 * \function steer_init()
 * 
 * Author: JCL, 1/30/04
 * Revisions:
 *   02/15/04  added drive disable before enable to clear fault conditions
 *             deleted test code, polished version
 *   03/25/04  removed drive disable before enable, no longer needed due to
 *             new fault checking code
 *             removed wheels forward hack as useless
 *
 * attempts to enable the drive
 * if drive cannot enable, return error
 * if drive can enable but position is 0 +/- 10 counts, assume power failure and
 * call calibration routine * if drive enables and no power failure,
 * then set default velocity (20) and accel (20) and soft limits (+/- 65000)
 * and return true
 *
 * returns: error means drive will not enable or some other problem
 *          false means we should recalibrate the drive
 *          true means we are a go
 *
 * known bugs: if we are withing 10 counts of 0 on init we
               assume a power failure.
 *             this is a good guess but not entirely accurate, it has been shown
 *             to generate false positives on occasion.  should attempt to find
 *             a method to conclusively determine power failure since last open
 */

int steer_init()
{
  int result;
  
  /* hereafter we assume that communications with the steering controller
   * have already been initialized with some previous call to steer_calibrate
   * at some other time.  Therefore, we don't have to do anything but verify
   * some critical parameters that may have changes and then enable the drive */
  
  if (verbose >= 5) cerr << "steer_init - setting parameters" << endl;
  result = steer_exec_cmd(STOP, verbose);
  result = steer_exec_cmd(S_EOL, verbose);
  result = steer_exec_cmd(S_EOT, verbose);
  
  result = steer_exec_cmd(S_E1, verbose);     //enable communication
  result = steer_exec_cmd(S_ECHO0, verbose);  //turn off command echoing
  result = steer_exec_cmd(S_ASCII1, verbose); //turn on ascii comms
  result = steer_exec_cmd(S_ERRLVL0, verbose); //set ERRLVL = 0;

  result = steer_exec_cmd(S_SGIRAT15, verbose); //servo ratio to 1.5
  result = steer_exec_cmd(S_DMTLIM16, verbose); //high torque limits

  result = steer_exec_cmd(S_DRIVE1, verbose);    //send drive1 to enable
    
  result = steer_exec_cmd(C_MA0, verbose);
  result = steer_exec_cmd(C_DRIVE1, verbose);
  if (verbose) 
    cerr << "steer_init - drive enabled check returned: " << result << endl;
    
  if (result != TRUE) {
    //printf("%s [%d] \n", __FILE__, __LINE__);
    if (verbose >= 1) cerr << "steer_init - drive not enabled" << endl;
    return FALSE;

  } else {
    fprintf(stderr, "steer_init - %s [%d] \n", __FILE__, __LINE__);
    if (verbose >= 3) cerr << "steer_init - drive enabled" << endl;
    //steer_calibrate();
    result = steer_exec_cmd(S_MA1, verbose); // enable absolute position mode
    steer_negCountLimit = ALICE_NEG_STEER_LIMIT;    //set soft limits to Alice Default values
    steer_posCountLimit = ALICE_POS_STEER_LIMIT;
    steer_exec_cmd(S_V16, verbose);  // set velocity = 16 rev/s
    steer_exec_cmd(S_A15, verbose);  // set acceleration = 15 rev/s/s
    return TRUE;
  }
}

/*********************************************************************************** 
 *int steer_calibrate(void)
 *
 * Author: Will Coulter
 * Revisions: Jeff Lamb 01/30/04
 *            Jeremy Gillula for Homer 9/2/04
 *
 * attempts to open communications to the steering system
 * used when the steering system has been powered down or moves out of calibration
 * blocking and takes a couple of minutes to executed because the wheels home and then
 * the software limits are set here
 * do not use this function if the current software limits are critical, they will be reset
 * assumes serial port is already open and initialized - run steer_open first
 *
 * returns: ERROR means that something is wrong
 *          FALSE should never be returned
 *          TRUE means things worked fine and the steering has been calibrated
 *
 * known bugs: homing only works with the wheels pointed left to start, unsure
 *             why this is the case but it seems to be a limitation of the controller
 *             as sending the raw command to the controller fails from the right
 *             also.
 ***********************************************************************************/

int steer_calibrate() {

    // Test Communication with the controller, assuming serial works.
    if (!steer_exec_cmd(C_ASCII1, verbose)) {
      printf("steer_calibrate - communication has failed\n\r");
      printf("steer_calibrate - resetting steering controller - waiting 15 seconds\n\r");
      steer_exec_cmd(RESET, verbose);
      DGCusleep(15000000);
      printf("steer_calibrate - trying to reopen the port\n\r");
      return steer_open(steer_serial_port);
    }

    steer_exec_cmd(S_DRIVE0, verbose);   //disable to clear fault conditions
    steer_exec_cmd(S_DMLCLR, verbose);   // clear all other fault conditions
    steer_exec_cmd(S_DRIVE1, verbose);   //enable drive
    usleep(100000);                             //wait MAGIC NUMBER
    if (steer_exec_cmd(C_DRIVE1, verbose) != TRUE) {
      printf("steer_calibrate - drive failed to enable - trying again\n\r");
      steer_exec_cmd(S_DRIVE1, verbose);
      if (steer_exec_cmd(C_DRIVE1, verbose) != TRUE) {
	printf("steer_calibrate - drive failed to enable again\n\r");
	printf("steer_calibrate - resetting steering controller - waiting 15 seconds\n\r");
	steer_exec_cmd(RESET, verbose);
	DGCusleep(15000000);
	printf("steer_calibrate - trying to reopen the port\n\r");
	return steer_open(steer_serial_port);
      }
    }

    if (steer_exec_cmd(C_DRIVE1, verbose) != TRUE) {
      printf("steer_calibrate - Drive failed to enable - return ERROR\n\r");
        steer_errno = ERR_ILL_RETVAL;
        return ERROR;
    }

    // Actually get the software limits.
    printf("steer_calibrate - setting Software Limits\n\r");
    steer_negCountLimit = ALICE_NEG_STEER_LIMIT;
    steer_posCountLimit = ALICE_POS_STEER_LIMIT;
    steer_set_sw_limits();

    // Now, finish up initialization...
    steer_exec_cmd(S_LS3, verbose);     //enable soft limits
    steer_exec_cmd(S_COMEXC1, verbose); //enable continuous command exec
    steer_exec_cmd(S_COMEXL1, verbose); //enable execution on limit
    steer_exec_cmd(S_MA1, verbose);     //absolute mode
    steer_exec_cmd(S_A15, verbose);     //fast accel
    steer_exec_cmd(S_V16, verbose);     //high velocity
    steer_exec_cmd(S_DMTLIM16, verbose); //high torque limits
    steer_zero();  //center the steering best known

    printf("steer_calibrate - steering initialization succeeded\n\r");
    return TRUE;
}






/**********************************************************************************
 * int steer_close (int steer_serial_port)
 * 
 * Author: Jeff Lamb 01/30/04
 * Revisions: NONE
 * closes the steering serial port after use
 * should also unallocate memory taken by the steering systen, but i don't know how
 *********************************************************************************/
int steer_close () {
  if (serial_use_dm500)
    close (serial_dm500_port);
  else
    serial_port.Close();
  return TRUE;
}  






/************************************************************************************
 * int steer_pause()
 * this function does nothing but return TRUE.  We should maintain steering control
 * during a pause e-stop
 ************************************************************************************/
    int steer_pause() {return TRUE;}








/***********************************************************************************
 * int steer_resume()
 * does nothhing because we maintain steering control
 **********************************************************************************/
    int steer_resume() {return TRUE;}







/***********************************************************************************
 * int steer_zero()
 * 
 * Author: Jeff Lamb
 * Revisions:
 * 
 * moves the steering back to zero (centers the steering if out calibration is correct
 * assumes steering is already initiailized and working properly for now
 * should add error checking at a later date
 **********************************************************************************/
int steer_zero() {
  steer_setposition(0);
  return TRUE;
}







/**********************************************************************************
 *int steer_setzero()
 *sets the actuator position to zero given the current position
 *
 ********************************************************************************/
int steer_setzero() {
  steer_exec_cmd(S_PSET0, verbose);
  if (verbose >= 2) 
    cerr << "steering position = " << steer_getposition() << endl;
  return TRUE;
}






/************************************************************************
 * int steer_fault_reset(void)
 * 
 * this function does nothing, fault checking is now built into the controller
 * see steer_error_program.prg in team/vehlib-2.0 for details
 **********************************************************************/
int steer_fault_reset() {
  return 0;
  }






/******************************************************************************
 * steer_exec_cmd
 *
 * Description:
 *      This function executes a command corresponding to the integer passed.
 *      It waits for the response from the servo.  It then compares the return
 *      string with the expected one.
 *
 * Arguments:
 *      verbose         integer that when TRUE causes verbose output to be
 *                      generated.
 *
 * Known Limitations / Bugs
 *      This function is blocking if it cannot write to the serial port.
 *
 * Return Values:
 *      TRUE    returned string matches
 *      FALSE   returned string does not match
 *      ERROR   some error, check the error number
 *****************************************************************************/
int steer_exec_cmd(int cmdIndex, int verbose)
{
    int readLength = 0;                 // Number of chars read from serial.
    char inputBuff[STEER_BUFF_LEN];     // Buffer filled with them chars.
    string inputString;                 // String made from buffer.

    // Check if cmdIndex is within range.
    if (cmdIndex < 0 || cmdIndex >= NUM_STEER_CMDS) {
      if (verbose) 
	cerr << "[ERROR] " << __FILE__ " (" << __LINE__ << "): "
	     << "Command is out of range (" << cmdIndex << ")!" << endl;
        steer_errno = ERR_ARG_RANGE;
        return ERROR;
    }

    if (verbose >= 8) {
      // Be openly happy that we can read.
      cerr << "[TRYING]" << steerCmds[cmdIndex].cmd.c_str();
    }


    //
    // Try to write the command.
    // 

#   ifdef UNUSED
    cerr << "Sending command: " << steerCmds[cmdIndex].cmd.c_str();
#   endif

    if (!serial_write ((char *) steerCmds[cmdIndex].cmd.c_str(), steerCmds[cmdIndex].cmd.length()) ) {
      /* Bad things should happen if we exit without writing the wrong length
         string */
      if (verbose >= 2) 
	cerr << "[ERROR] " << __FILE__ << "(" << __LINE__ << "): "
	     << "Incomplete serial port write!\n",
      steer_errno = ERR_SERIAL_IO;
      return ERROR;
    }

    //
    // We know that the write succeeded, now try to read response if one is
    // expected.
    //

    usleep(10000);

    if (steerCmds[cmdIndex].retVal.length() == 0) {
      // We do not expect any return, but read what's there anyway and trash.
      char trash[16];
      serial_read (trash, 16, 00);
    }
    else {
      // Get the number of characters read and fill buffer with chars.
      //#	ifdef SERIAL2
      //readLength = serial_read_until (steer_serial_port, inputBuff,
      //			(char) STEER_EOT, STEER_BUFF_LEN - 1,
      //			Timeval(SEC_TIMEOUT, 0));
      //#	else
      
      inputBuff[0] = '\0';
      serial_read(inputBuff, STEER_BUFF_LEN - 1, (char) STEER_EOT);
      readLength = strlen(inputBuff);
      inputBuff[readLength++] = STEER_EOT; // pacifying shitty system
//#	endif
      
      //Read as many characters as possible until we run out of buffer space or 10 ms has passed.
      
      //cerr << "readlength is " << readLength << endl;
      // Make this an actual string by appending a terminator.
      inputBuff[readLength] = '\0';
      // Make string 'cause they are cooler:
      inputString = inputBuff;
      
#ifdef UNUSED
      cerr << "Received: " << inputString;
#endif

      if (readLength <= 0) {
	// Nothing returned and we probably timed out.
	//            cerr << steerCmds[cmdIndex].retVal << endl;
	if (verbose >= 2)
	  cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): " 
	       << "No Serial port read (trying %s)" << endl;
	return ERROR;
      } 
      else {
	/* Check if we have the expected return value.  Null (zero-length)
	   string represents no expected return from the device. */
	if (steerCmds[cmdIndex].retVal.compare ( inputString ) == 0
	    || steerCmds[cmdIndex].retVal.length() == 0 )       
	  { 
	    /* We have the data we wanted, so output if desired (the output
	       string has a newline, but no carriage return. */
	    if (verbose == TRUE) {
	      cerr << inputString.substr(0, readLength-1) << endl;;
	    }
	  }
	else {
	  if (verbose >= 2) {
	    // We have something that simply doesn't match exactly.
	    cerr << "steer_exec_cmd - expected "
		 << inputString.substr(0, readLength-1)
		 << ", got "
		 << steerCmds[cmdIndex].retVal.c_str(); 
	  }
	  return FALSE;
	}
      }
    }
    
    return TRUE;
}

/******************************************************************************
 * steer_heading
 *
 * Description:
 *      This function instructs the car to steer the heading, but does not wait
 *      for that heading to actually be achieved until returning.... it assumes
 *      that COMEXC1 has already been issued.
 *
 * Arguments:
 *      float angle     direction: full left is -1, center is 0, full right is
 *                      1.
 *
 * Return Values:
 *      TRUE    in best faith, the code thinks we steered there.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_heading (double angle)
{
    char buff[100];
    //    int fault_status = 0;

    // Make sure that we aren't out of our limits
// !!! use CONSTANTS instead of {-1,1}
    if (angle < -1 || angle > 1) {
      if (verbose >= 3) {
	cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	     << "Illegal steering directive (" << angle << ")" << endl;
        steer_errno = ERR_ARG_RANGE;
        return ERROR;
      }
    }

    /* Negation because the code convention is neg==>left, while for the motor,
       neg==>right */
    angle *= -1;         


    // Make angle be in counts (native controller unit) rather than normalized:
    if (angle < 0) {
        angle *= abs(steer_negCountLimit);
    } else {
        angle *= abs(steer_posCountLimit);
    }

#   ifdef UNUSED
    cerr << "Going to position: " << angle << endl;
#   endif

    //
    // Logic for making sure what we don't overdrive the steering
    //
    // The idea is to check to see if we have already commanded
    // the steering to the given angle and, if so, don't recommand.  This
    // cuts down on the rate we sent serial commands to the controller.
    //
    // A potential problem with this is that we might somehow lose
    // track of a command we sent and so we go ahead and command the
    // "same" angle if we have been locked there for 10 times in a row
    //
    static int wait_count = 0;
    #define STEER_WAIT_MAX 10		// max iterations to skip command
    if ((int) angle == steer_cmdangle && wait_count++ < STEER_WAIT_MAX) 
      return TRUE;

    steer_cmdangle = (int) angle;	// save commanded steering angle
    if (wait_count == 10) steer_state = 2;	// keep track of condition
    wait_count = 0;			// reset wait count

    // Now, convert to a string to send to the controller.
    sprintf(buff, "!D%d\n\r", (int) angle); //cast angle as integer JCL 02-23-04

    if (!serial_write ((char *)buff, strlen (buff))) {
      if (verbose) 
	cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	     << "Not all of string was written" << endl;
      steer_errno = ERR_SERIAL_IO;
      steer_state = -1;
      return ERROR;
    }
    
    if (steer_exec_cmd(IMGO, FALSE) != TRUE) {
      if (verbose) 
	cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	     << "steer_exec_cmd() error" << endl;
      steer_state = -2;
      return ERROR;
    }
    else {
      ++steer_write_count;
    }
    
    //lastSteeringCommanded = (string) (buff+1);
    //    lastSteeringCommanded = (string) (buff+1);
    return TRUE;
}


/***************************************************************************
 * int steer_setposition(float position)
 *
 * ARGUMENTS: the function takes an float in controller controller counts 
 *            (aprrox +/- 80000 counts)
 * 
 **************************************************************************/
int steer_setposition(float position) {
  char buff[100];
  buff[1] = 'D';              // command is of form D<pos>
  buff[0] = '!';
  sprintf(buff+2, "%.0f\n\r", position);
#ifdef UNUSED
  printf("Sending: %s\n", buff);
#endif

  if (!serial_write ((char *) buff, strlen(buff))) {
    cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	 << "Not all of string was written" << endl;
      steer_errno = ERR_SERIAL_IO;
      return ERROR;
    }


  if (steer_exec_cmd(IMGO, FALSE) != TRUE) {
    fprintf(stderr, "[%s %d] steer_exec_cmd() error.\n", __FILE__, __LINE__);
    return ERROR;
  }
  else {
	cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	     << "Steering commanded = " << position << endl;
    return TRUE;
  }
}



/**********************************************************************************
 * int steer_setval(float)
 * Author: Jeff Lamb
 * Date: May 22, 2005
 * Revisions:
 * Inputs: desired velocity
 * Returns: 1 if successful, 0 if failed
 * Functionality: Used to the set the velocity in the parker steering controller.
 *                Takes in a float from 0-1 sets the velocity in the controller and
 *                verifies that the values has been set.
 * Known Bugs: None
 *****************************************************************************/
int steer_setvel(float vel) {

  char write_buff[VEL_BUFF_LEN];
  char buff[VEL_BUFF_LEN];
  char read_buff[VEL_BUFF_LEN];
  double vel_read;
  int readLength = 0;
  
  sprintf(buff, "V\n\r");
  sprintf(write_buff, "!V");
  // fill the first two characters with !V prefix for velocity command
  //write_buff[0] = '!';
  //write_buff[1] = 'V';
  

  // compute the velocity command
  if (vel > 1 || vel < 0) {
    printf("velocity input out of range\n");
    return 0;
  }
  vel  = vel * ALICE_STEER_MAX_VEL;

  // fill the rest of the buffer
  sprintf(write_buff+2, "%.2f\n\r", vel);

  if (!serial_use_dm500) serial_port.clear();
  if (!serial_write ((char *) write_buff, strlen(write_buff))) {
    cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	 << "Not all of string was written" << endl;
    steer_errno = ERR_SERIAL_IO;
    return ERROR;
  }

  if (!serial_write ((char *) buff, strlen(buff))) {
    cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	 << "Not all of string was written" << endl;
    steer_errno = ERR_SERIAL_IO;
    return ERROR;
  }

  
  read_buff[readLength] = '\0';
  serial_read (read_buff, VEL_BUFF_LEN, (char) STEER_EOT);
  readLength = strlen(read_buff);
  read_buff[readLength++] = STEER_EOT;

// pacifying shitty system

  //  cerr << read_buff << "!!!" << endl << endl;
  vel_read = atof(read_buff+1);

  // if the read back velocity = sent velocity, then it got set properly
  if (vel_read == vel) return 1;
  else {
    //printf("ERROR: requested velocity and controller velocity not equal\n");
    return 0;
  }
  return 1;
}
  





/**********************************************************************************
 * int steer_setaccel(float)
 * Author: Jeff Lamb
 * Date: May 22, 2005
 * Revisions:
 * Inputs: desired accel
 * Returns: 1 if successful, 0 if failed
 * Functionality: Used to the set the acceleration in the parker steering controller.
 *                Takes in a float from 0-1 sets the acceleration in the controller and
 *                verifies that the values has been set.
 * Known Bugs: None
 *****************************************************************************/
int steer_setaccel(float accel) {
  char write_buff[ACCEL_BUFF_LEN];
  char buff[ACCEL_BUFF_LEN];
  char read_buff[ACCEL_BUFF_LEN];
  double accel_read;
  int readLength = 0;

  sprintf(buff, "A\n\r");
  sprintf(write_buff, "!A");
  // fill the first two characters with !V prefix for velocity command
  //write_buff[0] = '!';
  //write_buff[1] = 'A';
  

  // compute the velocity command
  if (accel > 1 || accel < 0) {
    printf("accel input out of range\n");
    return 0;
  }
  accel  = accel * ALICE_STEER_MAX_VEL;

  // fill the rest of the buffer
  sprintf(write_buff+2, "%.2f\n\r", accel);

  if (!serial_use_dm500) serial_port.clear();
  if (!serial_write ((char *) write_buff, strlen(write_buff))) {
    cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	 << "Not all of string was written" << endl;
    steer_errno = ERR_SERIAL_IO;
    return ERROR;
  }

  if (!serial_write ((char *) buff, strlen(buff))) {
    cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	 << "Not all of string was written" << endl;
    steer_errno = ERR_SERIAL_IO;
    return ERROR;
  }

  
  read_buff[readLength] = '\0';
  serial_read(read_buff, ACCEL_BUFF_LEN, (char) STEER_EOT);
  readLength = strlen(read_buff);
  read_buff[readLength++] = STEER_EOT; 

// pacifying shitty system




  accel_read = atof(read_buff+1);

  // if the read back velocity = sent velocity, then it got set properly
  if (accel_read == accel) return 1;
  else {
    //printf("ERROR: requested accel and controller accel not equal\n");
    return 0;
  }
  return 1;
}
  




/******************************************************************************
 * steer_test_communication
 *
 * Description:
 *      This function tests to see whether or not we have communication with 
 *      the controller.  It will either wait the poweron time of the device or 
 *      immediately try to read/write it.
 *
 * Arguments:
 *      int wait        This is either TRUE or FALSE and decides whether or not
 *                      to wait the poweron timeout.
 *
 * Known Limitations / Bugs
 *      The first while loop will hang forever if the drive keeps sending data
 *      forever.
 *
 * Return Values:
 *      TRUE    communication is good.
 *      FALSE   cannot talk to the controller, but I/O works
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_test_communication (int wait)
{
  int status;
  
  if (wait == TRUE) {
    // Johnson's code should interrupt us here if we get data.
    sleep(POWERON_TIMEOUT);
  }
  
  // Empty serial buffer to continue in a clear state
  
  //serial_clean_buffer(steer_serial_port);
  
  
  /* Set up communications to be the way we like, so that our command 
     execution gets the right strings back on success (':', '\n', and '\r'
     are legal input command delimiters, see p.4).  
     status is the result of all these commands. */
  status = steer_exec_cmd(STOP, verbose);
  status |= steer_exec_cmd(S_E1, verbose);
  status |= steer_exec_cmd(S_ASCII1, verbose);     //not sure if this is correct, but want ascii comms
  status |= steer_exec_cmd(S_EOL, verbose);
  status |= steer_exec_cmd(S_EOT, verbose);
  status |= steer_exec_cmd(S_ECHO0, verbose);
  status |= steer_exec_cmd(S_ERRLVL0, verbose);
  
  /* Check that nothing stupid happened with the serial communications. No 
     point in checking for FALSE because the status wouldn't work with the 
     ORs. */
  if ( (status & ERROR) == ERROR ) {
    // Keep the same error number and return.
    cerr << "[ERROR] " << __FILE__ << " (" << __LINE__ << "): "
	 << "Communication failed unexpectedly in test" << endl;
    return ERROR;
  }
  
  return TRUE;
}


/******************************************************************************
 * steer_upload_file
 *
 * Description:
 *      This function uploads the given file to the steering controller.
 *
 * Arguments:
 *      string fileName This is the filename... the current directory is
 *                      assumed to be the path.
 *
 * Known Limitations / Bugs:
 *      This is a blocking function if the serial port cannot be written.
 *
 * Return Values:
 *      TRUE    file is uploaded and spotchecked.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_upload_file (string fileName)
{
  string line;
  ifstream infile;
  int readLength;
  char inputBuff[STEER_BUFF_LEN];
  infile.open( fileName.c_str() );
  
  cerr << fileName << endl;
  
  if (infile.good() != true) {
    //        cerr << "File " << fileName << " could not be opened, aborting." << endl;
    steer_errno = ERR_FILE_IO;
    return ERROR;
  } else {
    //        cerr << "File " << fileName << " was opened successfully." << endl;
  }
  
  steer_exec_cmd(S_ECHO1, TRUE);
  while( getline(infile, line) ) {
    line += '\n';
    //cerr << line;
    
    
    if (!serial_write ((char *) line.c_str(),line.length()))
      {
	fprintf(stderr, "[%s %d] Not all of string was written.\n", __FILE__, __LINE__); 
	steer_errno = ERR_SERIAL_IO;
	return ERROR;
      }
    
  }
  
  // 
  // Now, write a blank line in a blocking fashion so we know that everything
  // has been written.
  //
  
  // Assume there are no errors.
  //#   ifdef SERIAL2
  //serial_write (steer_serial_port, (char *) STEER_EOT, 1);
  //#   else
  serial_write ((char *) STEER_EOT, 1);
  
//#   endif

  steer_exec_cmd(S_ECHO0, TRUE);
  
  inputBuff[0] = '\0';
  serial_read (inputBuff, STEER_BUFF_LEN-1);
  readLength = strlen(inputBuff);

    inputBuff[readLength] = '\0';
    //cerr << inputBuff << endl;


    return TRUE;
}



/******************************************************************************
 * steer_set_sw_limits
 *
 * Description:
 *      This function travels to the hardware limits and sets the software
 *      limits inside them.  It assumes that the serial communication is up.
 *      It also sets the limits in this code.
 *
 * Arguments:
 *      [none]
 *
 * Global Variables used:
 *      steer_negCountLimit   Set to the negative sw limit
 *      steer_posCountLimit   Set to the positive sw limit
 *
 * Known Limitations / Bugs:
 *      This is a blocking function if the serial port cannot be written.
 *      lots of magic numbers and little error checking ... actually, pretty
 *      much none.
 *      do NOT call this function when speeds are set high. it might damage
 *      the pow3er steering.
 *
 * Return Values:
 *      TRUE    file is uploaded and spotchecked.
 *      FALSE   [not used]
 *      ERROR   some error, check the error code.
 *****************************************************************************/
int steer_set_sw_limits (void)
{
  string outString;
  stringstream negLimit, posLimit;
  
  //disable hard limits, since there are no limit switches on Alice
  steer_exec_cmd(S_LH0, verbose);  
  
  //disable soft limits while calibrating
  steer_exec_cmd(S_LS0, verbose);  
  
  //zero the system wherever we are
  steer_setzero();
  
  //disable the drive so that we can turn the wheel manually
  steer_exec_cmd(S_DRIVE0, verbose);
  
  char userinput[20];
  int calibration_complete = FALSE;
  
  //New Calibration code - 18 March 2005
  while(calibration_complete != TRUE) {
    // ensure that the drive is disabled
    steer_exec_cmd(S_DRIVE0, verbose);
    printf("Center Alice's steering wheel - Hit enter to engage motor when ready\n");
    cin.getline(userinput, 20);
    
    // engage the drive to lock the wheels in place
    steer_exec_cmd(S_DRIVE1, verbose);
    printf("Please drive forward to verify that the wheels are in fact taking Alice straight ahead\n");
    printf("Press y if the calibration is good, n to repeat\n");
    cin.getline(userinput, 20);
    if (userinput[0] == 'y') calibration_complete = TRUE;
  }
  
  // the setzero function can be flaky, so we loop to make sure
  // that the controller and motor are properly zeroed
  steer_exec_cmd(S_DRIVE0, verbose);
  steer_setzero();
  steer_exec_cmd(S_DRIVE1, verbose);
  
  // set the negative (right limit) in the controller
  negLimit << steer_negCountLimit;
  outString = (string) "LSNEG" + negLimit.str() + STEER_EOT;
  
  serial_write ((char *) outString.c_str(), outString.length());
  
  
  // Set the positive (left limit) in the controller
  posLimit << steer_posCountLimit;
  outString = (string) "LSPOS" + posLimit.str() + STEER_EOT;
  
  serial_write ((char *) outString.c_str(), outString.length());
  
  
  //put the controller back in absolute positioning mode
  steer_exec_cmd(S_MA1, verbose);
  
  cerr << "The software limits are now " << steer_posCountLimit << " and " << steer_negCountLimit  << endl;
  cerr << "The current steering position is " <<steer_getposition() << ".  Press a key to continue..."<< endl;
  cin.getline(userinput, 20);
  return TRUE;
}

/*
 * steerState.cc
 */

int steer_state_blocking (int);


extern int steer_negCountLimit;
extern int steer_posCountLimit;
extern int STEER_BY_HAND;
extern int verbose;
extern int steer_errno;

///////////////////////////THIS IS A HACK/////////////////
extern string lastSteeringCommanded;
//////////////////////////////////////////////////////////

enum {
    TPE,        // Transfer Encoder position
    TAS,        // Transfer Axis State
    TASX,       // Transfer Extended Axis State
    TSS,        // Transfer System Status
    TER,
    TDTEMP,     // Transfer Drive Temp. (higher of DSP/power block)
    TMTEMP,     // Transfer Motor Temp. (predicted motor winding).
    TDHRS,      // Transfer Operating hours (nearest 1/4 hour).
    NUM_STATE_VARS
};
// The actual saved states/values.
string steerState[NUM_STATE_VARS] = {(string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890", (string) "1234567890"};
// Timestamps for each of the saved values.
timeval steerStateTimestamps[NUM_STATE_VARS] = {{0,0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}};
// Time in seconds for each datum to expire.  (.3 works, stresses the system A LOT)
double steerStateExpire[NUM_STATE_VARS] = {0.5, 2.6, 6.7, 8.9, 13, 21, 23, 27};
// Strings sent to the steering controller.
string serialStateQuery[NUM_STATE_VARS] = {(string) "TPE", (string) "TAS", (string) "TASX", (string) "TSS", (string) "TER", (string) "TDTEMP", (string) "TMTEMP", (string) "TDHRS"};

/******************************************************************************
 * steer_state_update
 *
 * Description:
 *      Updates the controller state every time called.  Global strings are
 *      written with the values of the commands read, without the echoing of
 *      the command given.  Alas, indexing is one off on the string versus the
 *      manual, because the manual starts with one instead of zero.  
 *      Even though it's annoying, the code writes everything to the controller
 *      and then reads it all back instead of doing things individually since
 *      communication with the controller is the bottleneck.  Also, there are
 *      those stupid underscores to watch out for, so they are removed for
 *      storage in the global variables.
 *
 * Arguments:
 *      log             integer that causes controller state to be logged when 
 *                      TRUE.
 *      updateAll       integer that spefifies that all parameters should be
 *                      recorded when TRUE.  Otherwise, TPE,TAS are updated.
 *
 * Global Variables:
 * [all of the below are written with the command of the same name]
 [...]
 * 
 * Return Values:
 *      TRUE    Data has been updated and/or logged.
 *      FALSE   Returned if function is called to fast (no new data).
 *      ERROR   either file I/O or serial.
 *****************************************************************************/
int steer_state_update (int log, int updateAll)
{
  unsigned int lastSTEER_EOT;  // The last STEER_EOT parsed (for updating variables).
  int readLength;     // Length Read by the serial code.
  int expectedReplies = NUM_STATE_VARS;
  char inputBuff[STEER_BUFF_LEN];  // Buffer serial code can read into.
  string serialInputString;
  //timeval timeStamp;
  //double elapsed;
  
  //cerr << __FILE__ << __LINE__ << endl; fflush(stdout);
  if ( !updateAll ) 
    expectedReplies = 2;    // Magic number follows.
  
  
  string fileName;    // Filname we log to.
  ofstream logFile;   // File we log to.
  if (log == TRUE) {
    // http://beta.experts-exchange.com/Programming/Programming_Languages/Cplusplus/Q_20650231.html#8751396
    char Time[20];
    time_t time_now = time(NULL);
    strftime( Time, 20, "%Y%m%d", localtime( &time_now ) );
    fileName = (string) "LOG.steering." + Time + ".dat";
    logFile.open( fileName.c_str(), ios::app );
    if (logFile.good() != true) {
      //   cerr << "File " << fileName << " could not be opened to append." << endl;
      steer_errno = ERR_FILE_IO;
      return ERROR;
    }
  }
  
  // Clear any data in the buffer.
  //    serial_clean_buffer(steer_serial_port);
  
  
  
  //
  // Open a big loop to read and write the commands we care about.
  //
  
  for (int i = 0; i < expectedReplies; i++) {
    //
    // Check how long it's been since the last time we tried to update the
    // variables and decide whether or not to proceed
    //
    /*
      gettimeofday(&timeStamp, NULL);
      // Get the elapsed time. 
      elapsed = timeStamp.tv_sec - steerStateTimestamps[i].tv_sec;
      elapsed += ((double) (timeStamp.tv_usec - steerStateTimestamps[i].tv_usec)) / 1000000;
      // Fix Midnight rollover
      if ( elapsed < 0 ) {
      // Add number of seconds in a day
      elapsed += 24 * 3600;
      }
      
      // Don't get new data if the data hasn't expired.
      if ( elapsed < steerStateExpire[i] ) {         
      continue;
      }
      
    */
    //
    // Send commands to the controller.
    // 
    
    if ( !serial_write ((char *) ( (string) "!" + serialStateQuery[i] + (char) STEER_EOT ).c_str(), 
			     serialStateQuery[i].length() + 2) )
    {
      steer_errno = ERR_SERIAL_IO;
      return ERROR;
    }
    
    // 
    // Read the replies
    //

    readLength = 0;

    //#	ifdef SERIAL2
    //readLength = serial_read_until (
    //        steer_serial_port, 
    //        inputBuff + readLength, 
    //        (char) STEER_EOT, 
    //        STEER_BUFF_LEN - 1,
    //        Timeval(SEC_TIMEOUT, 0)
    //        );
    //#	else
    inputBuff[readLength] = '\0';
    serial_read(inputBuff + readLength, STEER_BUFF_LEN - 1, (char) STEER_EOT);
		char *pBuffer = inputBuff + readLength;
    readLength = strlen(pBuffer);
		pBuffer[readLength++] = STEER_EOT; // pacifying shitty system
    //#	endif
    
    // Error Checking
    if ( readLength == 0 || readLength == -1) {
      //  cerr << "Nothing read (trying " << serialStateQuery[i] << "), returning " << endl; fflush(stdout);
      logFile << "Nothing read (trying " << serialStateQuery[i] << "), returning " << endl; fflush(stdout);
      steer_errno = ERR_SERIAL_IO;
      return ERROR;
    }
    
    // Make it a string.
    inputBuff[readLength] = '\0';       
    
    if ( updateAll ) {
      //  cerr << inputBuff;
    }
    //
    // Deal with the data.
    //
    
    // STL is so much cooler.
    serialInputString = (string) inputBuff;
    // Get Rid of underscores
    while ( serialInputString.find('_') != string::npos ) {
      serialInputString.erase(serialInputString.find('_'), 1);
    }
    
    //cerr << "Got: " << serialInputString;
    
    lastSTEER_EOT = serialInputString.find( (char) STEER_EOT, serialStateQuery[i].length() );
    if ( lastSTEER_EOT == string::npos ) {
      cerr << "Parse error in serialInputString (" << serialInputString << ")\n";
      
      cerr << "strin:npos is " << string::npos << " eot is " << lastSTEER_EOT << endl;
      cerr << "STEER_EOT is " << STEER_EOT << endl;
      cerr << "The input is " << serialInputString.c_str() << endl; 
      int lastSTEER_EOL = serialInputString.find((char)STEER_EOL, serialStateQuery[i].length());
      cerr << "Found at " << lastSTEER_EOL << endl;
      /**/
      
      steer_errno = ERR_SERIAL_IO;
      return ERROR;
    }
    steerState[i] = serialInputString.substr(
					     serialStateQuery[i].length(), 
					     lastSTEER_EOT - serialStateQuery[i].length()
					     );
    serialInputString = serialInputString.substr(lastSTEER_EOT + 1);
    
    // Set the time of update.
    gettimeofday( &(steerStateTimestamps[i]), NULL);
    
    if ( log ) {
      logFile << "[" 
	      << ((string) asctime(localtime(&(steerStateTimestamps[i].tv_sec)))).erase(24)
	      << " (+"
	      << ((double) steerStateTimestamps[i].tv_usec) / 1000000
	      << " sec)] " 
	      << serialStateQuery[i] << "\t" << steerState[i] << endl;
      ////////////THIS IS A HACK/////////////
      //            if (i == TPE) {
      //                logFile << ((string) asctime(localtime(&(steerStateTimestamps[i].tv_sec)))).erase(24) << "\t" << steerState[i] << "\t" << lastSteeringCommanded;
      //            }
    }
    
  }
  
  if ( log ) {
    logFile.close();
  }
  
  return TRUE;
}


/******************************************************************************
 * steer_state_limit_pos
 *
 * Description:
 *      Checks if we have reached a positive limit, hardware or software.
 *
 * Global Variables:
 *      steerState      read, not modified
 *
 * Return Values:
 *      TRUE    We are at a limit
 *      FALSE   We are not at a limit
 *****************************************************************************/
int steer_state_limit_pos (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( 
            steerState[TAS][16] == '1' // POS SW
            || steerState[TAS][14] == '1' // POS HW
       ) 
    {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_limit_neg
 *
 * Description:
 *      Checks if we have reached a negative limit, hardware or software.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are at a limit
 *      FALSE   We are not at a limit
 *****************************************************************************/
int steer_state_limit_neg (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR)
            return ERROR;
    }

    if ( 
            steerState[TAS][17] == '1'    // NEG SW
            || steerState[TAS][15] == '1' // NEG HW
            ) 
    {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_home_complete
 *
 * Description:
 *      Checks if we have completed a home sequence
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We have completed the sequence
 *      FALSE   We have not completed the sequence
 *****************************************************************************/
int steer_state_home_complete (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }

    if ( steerState[TAS][4] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_faulted
 *
 * Description:
 *      Checks if the drive is faulted.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are faulted
 *      FALSE   We are not faulted
 *****************************************************************************/
int steer_state_faulted (int blocking)
{
#ifdef UNUSED
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR) 
            return ERROR;
    }
#endif

    if ( steerState[TAS][13] == '1'
	 || steerState[TAS][14] == '1'
	 ) 
      {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_state_moving
 *
 * Description:
 *      Checks if the wheels are moving.
 *
 * Global Variables:
 *      steerState[TAS]   read, not modified
 *
 * Return Values:
 *      TRUE    We are moving
 *      FALSE   We are stopped
 *****************************************************************************/
int steer_state_moving (int blocking)
{
    if ( blocking ) {
        if (steer_state_blocking (TAS) == ERROR) 
            return ERROR;
    }
    else {
        if (steer_state_update(FALSE, FALSE) == ERROR)
            return ERROR;
    }

    if ( steerState[TAS][0] == '1' ) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}


/******************************************************************************
 * steer_getposition
 *
 * Description:
 *      Returns the position of the steering.
 *
 * Global Variables:
 *      None      
 *
 * Return Value:
 *      current encoder position.
 *****************************************************************************/
int steer_getposition (void)
{
  int readLength;
  char write_buff[5];
  char read_buff[10];
  
  sprintf(write_buff, "TPE\n\r");
  
    // write to the controller

  if (!serial_use_dm500) serial_port.clear();
  serial_write(write_buff, strlen(write_buff));
  if (!serial_use_dm500 && !serial_port.good()) return -2;

  read_buff[0] = '\0';
  if (!serial_use_dm500) serial_port.clear();
  serial_read(read_buff, STEER_BUFF_LEN - 1, (char) STEER_EOT);
  char *pBuffer = read_buff;
  readLength = strlen(pBuffer);
  read_buff[readLength++] = STEER_EOT; // pacifying shitty system
  //cerr << read_buff <<  "!!!" << endl << endl << endl;
  
  // check for the proper data prefix
  if (read_buff[0] != 'T' && read_buff[1] != 'P' && read_buff[2] != 'E') {
    fprintf(stderr,"ERROR: did not read back position data correctly\n");
    return -2;
  }
  
  return atoi(read_buff+3);
}





/******************************************************************************
 * double steer_getheading()
 * Author: Jeff Lamb
 * Date: 13 March 05
 * Revisions: None
 * Functionality: Returns the heading of the wheels [-1, 1]
 * Limitations:  steer_state_update(FALSE, FALSE) must be called before calling
 *               this function to update the encoder position in memory
 *****************************************************************************/
double steer_getheading() {
  double position = steer_getposition();
  
  //double position = (double) atoi( steerState[TPE].c_str() );
  //printf("Position in getheading: %f\n",position);
  if (position >= 0) 
    position = position / steer_posCountLimit * (-1);
  else if (position < 0)
    position = position / steer_negCountLimit;
  if (position > 1) position = 1;
  else if (position < -1) position = -1;
  return position;
}



/*****************************************************************************
 * double steer_getvel(void)
 *****************************************************************************/
double steer_getvel(void) {
#ifdef UNUSED
  char write_buff[10];
  char read_buff[10];

  sprintf(write_buff, "V\n\r");
  
  steerport->clean();
  steerport->write((char*) write_buff);
  usleep(5000);
  steerport->read(5, read_buff, 10000);
  
  if (read_buff[0] == 'V') {
    return atof(read_buff+1);
  }
  else {
    printf("ERROR: velocity read failed!\n");
    return -1;
  }
#endif // unused
  return 1;
}



/*****************************************************************************
 * double steer_getaccel(void)
 ****************************************************************************/
double steer_getaccel(void) {
#ifdef UNUSED  
char write_buff[10];
  char read_buff[10];

  sprintf(write_buff, "A\n\r");
  
  steerport->clean();
  steerport->write((char*) write_buff);
  usleep(5000);
  steerport->read(5, read_buff, 10000);
  
  if (read_buff[0] == 'A') {
    return atof(read_buff+1);
  }
  else {
    printf("ERROR: accel read failed!\n");
    return -1;
  }
#endif //unused
  return -1;
}






/******************************************************************************
 * steer_state_blocking
 *
 * Description:
 *      Does not return until the read time of a value is newer than the time
 *      given.
 *
 * Global Variables:
 *      steerState      read, not modified
 *
 * Return Value:
 *      ERROR           Problem with update.
 *      TRUE            Time has elapsed as planned.
 *****************************************************************************/
int steer_state_blocking (int stateIndex)
{
    timeval startTime = {steerStateTimestamps[stateIndex].tv_sec, steerStateTimestamps[stateIndex].tv_usec};

    while ( startTime.tv_sec == steerStateTimestamps[stateIndex].tv_sec
            || startTime.tv_usec == steerStateTimestamps[stateIndex].tv_usec )
    {
        // The updatetime has not changed, so try update again and see.
        if (ERROR == steer_state_update (FALSE, FALSE) ) 
            return ERROR;
    } 
    return TRUE;
}


int steer_enable() {
  steer_exec_cmd(S_DRIVE1, verbose);
  if (verbose >= 6)
    cerr << "[DEBUG] " << __FILE__ << " (" << __LINE__ << "): "
	 << "steer_enable - enabling..." << endl;
  return TRUE;
}

int steer_disable() {
  steer_exec_cmd(S_DRIVE0, verbose);
  if (verbose >= 6)
    cerr << "[DEBUG] " << __FILE__ << " (" << __LINE__ << "): "
	 << "steer_disable - disabling..." << endl;
  return TRUE;
}

// I have added this funciton call to allow the sparrow interface to force
// a reenable of the drive after being diabled by the kill switch.  
int steer_enable_overide() {
  steer_exec_cmd(S_DRIVE1, verbose);
  return TRUE;
}


int steer_enabled_check() {
  int enable_status = steer_exec_cmd(C_DRIVE1, verbose);
  if ( enable_status == 1) {
    if (verbose >= 9) 
      cerr << "[DEBUG] " << __FILE__ << " (" << __LINE__ << "): "
	   << "steer_enabled_check - steering is enabled" << endl;
    return TRUE;
  }
  else if ( enable_status == 0) {
    if (verbose >= 9) 
      cerr << "[DEBUG] " << __FILE__ << " (" << __LINE__ << "): "
	   << "steer_enabled_check - steering is disabled" << endl;
    return FALSE;
  }
  else {
    if (verbose >= 3)
      cerr << "[DEBUG] " << __FILE__ << " (" << __LINE__ << "): "
	   << "steer_enabled_check - returned incorrectly" << endl;
    return ERROR;
  }
}

/* Local Variables: */
/* c-basic-offset: 2 */
/* End: */
