#
# This is the source file for managing adrive command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Lars Cremean, 2006-10-03
# Richard Murray, 2006-12-22
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings
#   (3) Each thread can be disabled from the cmd line
# 
# Implementation:
#   
#   --disable-<option> will disable the specified function
#   --enable-<option> will enable selected functions
#   short options available (lower case for disable, upper for enable)

package "adrive"
purpose "Adrive provides an interface to Alice's actuation hardware,
including the e-stop functionality."
version "1.0"

# Options for turning off actuator threads
option "disable-brake" b "disable brake interface" flag off
option "disable-sparrow" d "turn off sparrow display" flag  off
option "disable-estop" e "disable estop interface" flag off
option "disable-gas" g "disable throttle interface" flag off
option "disable-obd" o "disable OBD II interface" flag off
option "disable-steer" s "disable steering interface" flag off
option "disable-trans" t "disable transmission (and ignition) interface"
  flag off 
option "disable-interlocks" x "disable safety interlocks" flag off
option "enable-interlocks" X "enable safety interlocks" flag off

# Configuration files
# text ""
option "config" - "configuration file"
  string default="adrive.config" no 
option "noconfig" - "don't load configuration file at startup" flag off
option "argfile" - "file containing command line arguments" string no

# Standard options
# text ""
option "snkey" - "skynet key" int default="0" no
option "enable-astate" - "enable astate (from Applanix)" flag off
