/*!
 * \file ActuationInterface.cc
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This file receives directives for adrives using the GcInterface
 * class and sends them off to the appropriate actuators.
 */

#include <assert.h>
#include "ActuationInterface.hh"
#include "overview.h"		// display definitions
using namespace std;

/* Constructor */
ActuationInterface::ActuationInterface(int skynet_key, 
				       struct gengetopt_args_info *cmdline) 
  : GcModule("adriveInterface", &m_controlStatus, &m_mergedDirective,
	     10000, 10000)
{
  /* Get the north face to trajfollower */
  m_adriveCommand = new AdriveCommand(skynet_key, this);
  assert(m_adriveCommand != NULL);
  m_adriveCommandNF = m_adriveCommand->getNorthface();

  /* Get the the south face to the actuators */
  m_steerInterface = new AdriveSteerInterface(skynet_key, this);
  assert(m_steerInterface != NULL);
  m_steer = m_steerInterface->getSouthface();

  /* Determine if we need to initiate connection to the simulator */
  if (cmdline->simulate_given) {
    m_skynet = new skynet(SNadrive, skynet_key);
    m_drivesocket = m_skynet->get_send_sock(SNdrivecmd);
  }

  /* Save the command line options that we were passed */
  options = cmdline;
}

void ActuationInterface::arbitrate(ControlStatus *controlStatus,
				   MergedDirective *mergedDirective)
{
  if (m_adriveCommandNF->haveNewDirective()) {
    m_adriveCommandNF->getNewDirective( &m_adriveDirective );

    /* Pass on to appropriate actuator */
    switch (m_adriveDirective.actuator) {
    case Steering:
      m_steer->sendDirective(&m_adriveDirective);
      break;

    default:
      cerr << "ActuationInterface: received command for unknown actuator: "
	   << m_adriveDirective.actuator << endl;
    }
  }

  /* Check for return status message and pass them back to trajFollower */
  if (m_steer->haveNewStatus()) {
    AdriveResponse *steerResponse = m_steer->getLatestStatus();
    m_adriveCommandNF->sendResponse(steerResponse);
  }
}
