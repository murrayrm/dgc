/*!
 * \file AccelerationModule.cc
 * \brief GcModule for controlling Alice's acceleration
 * 
 * \author Nok Wongpiromsarn and Richard Murray
 * \date 14 April 2007
 *
 * This is the GcModule that controls Alice's acceleration.  It receives
 * commands from the ActuatorInterface module (router) and sends them
 * to the acceleration motor controller.  The initial release assumes
 * correct operation and does minimal checking for errors.
 */

#include <assert.h> 
#include <iostream>
#include <sys/stat.h>

#include "dgcutils/DGCutils.hh"

#include "AccelerationModule.hh"
#include "brake.hh"
#include "throttle.hh"
using namespace std;

/*
 * AccelerationModule::AccelerationModule
 *
 * The constructor initializes the GcModule interface, the acceleration
 * port and other class data.
 */

AccelerationModule::AccelerationModule(int skynetKey,
				       ActuationInterface *actuationInterface,
				       int verbose_flag)
  : GcModule("Adrive_AccelerationModule", &m_controlStatus, &m_mergedDirective,
	      FAST_SLEEP, FAST_SLEEP)
{
  /* Set up the log file if log level is greater than 0 */
  if (actuationInterface->options->log_level_arg > 0) {
    /* First get the name of the log file */
    ostringstream oss;
    oss << actuationInterface->options->log_file_arg << "-acceleration";
    string logFileName = oss.str();

    /* This is how we use GcModuleLogger. First we tell it to create a log file.
       Then, set the log level of the module so when we use 
       gclog(n) below, the message will be logged if the log level is
       greater than or equal to n. */
    this->addLogfile(logFileName);
    this->setLogLevel(actuationInterface->options->log_level_arg);
  }

  verbose = verbose_flag;
  if (verbose > 5) cerr << "AccelerationModule: verbose = " << verbose << endl;

  /*
   * GcModule initialization
   *
   * The first thing that we do is initialize all of the variables
   * that are required for communicating with other GcModules,
   * including the ActuationInterface.
   */

  /* Get the interface to the accelControl module */
  adriveAccelInterfaceNF = 
    AdriveAccelInterface::generateNorthface(skynetKey, this);
  adriveAccelInterfaceNF->setStaleThreshold(10);

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * AccelerationModule initialization
   *
   * Initialize the member variables used by the control and arbitrate
   * functions.
   */

  /* Initialize the IDs so that we can tell when we get new commands */
  m_latestID = m_currentID = 0;

  /* Initialize the control state to keep from returning a response */
  m_controlStatus.status = ControlStatus::RUNNING;
  controlCount = 0;

  /* Initialize the pause status */
  system_paused = 0;
  if (adrive->options->pause_braking_given) {
    pause_braking = adrive->options->pause_braking_arg;
  } else
    pause_braking = ADRIVE_PAUSE_BRAKING;

  /*
   * Hardware/simulation initailization
   *
   * Now that we have initalized the software interfaces, we
   * initialize the hardware interface.  This consists of opening up
   * the serial port so that we can talk to the motor controller and
   * then initializing the motor controller.  If we can't open the
   * serial port, we abort with an error.  It is possible for the
   * motor controller initialization to fail if acceleration is not
   * currently enabled in Alice.  If this fails, we mark the interface
   * as uninitialized and continue on.  This allows us to continue to
   * receive status and also to rest the acceleration module manually from
   * sparrow.
   *
   * If the acceleration interface is disabled (using the command line
   * flag), then we still try to open the port but we don't generate
   * abort if we can't do it.  This allows the acceleration status
   * information to still be run in the case that we are in Alice and
   * just don't want to use acceleration.  If this acceleration is disabled,
   * we set the inialized flag to true so that it appears to modules
   * that acceleration is being commanded.  This is useful for testing.
   *
   * If we are running in simulation mode, we don't bother trying to
   * talk to the hardware. 
   */

  /* Mutexes for controlling access to serial port and varaibles */
  DGCcreateMutex(&brakeMutex);
  DGCcreateMutex(&throttleMutex);

  /* Initialize the hardware */
  initializeBrake();
  initializeThrottle();

  /*
   * Status thread startup
   *
   * As part of the AccelerationModule function, we start up a thread to
   * read the status of the actuator on a periodic basis.  This thread
   * fills in data in the AccelerationModule class, so we have easy
   * access to this information.  
   */

  dd_tbl = NULL;		// don't update flags until sparrow init
  statusCount = 0;
  statusSleepTime = STATUS_SLEEP*2;
  DGCstartMemberFunctionThread(this, &AccelerationModule::status);

}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void AccelerationModule::initSparrow(DD_IDENT *tbl, int brake_id, 
				     int throttle_id)
{
  /* Save information on the table for later update of display */
  dd_tbl = tbl;
  m_brake_id = brake_id;
  m_throttle_id = throttle_id;

  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!brake_connected) {
      /* No connection to the motor controller (acceleration disabled) */
      dd_setcolor_tbl(brake_id, BLACK, RED, tbl);

    } else if (!brake_initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(brake_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(brake_id, BLACK, GREEN, tbl);
    }

    /* Set the color of the label to reflect our current status */
    if (!throttle_connected) {
      /* No connection to the motor controller (acceleration disabled) */
      dd_setcolor_tbl(throttle_id, BLACK, RED, tbl);

    } else if (!throttle_initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(throttle_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(throttle_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("brake.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("brake.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("brake.command", &brake_command, tbl);
    dd_rebind_tbl("brake.position", &brake_current, tbl);
    dd_rebind_tbl("brake.pressure", &brake_pressure, tbl);

    dd_rebind_tbl("throttle.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("throttle.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("throttle.command", &throttle_command, tbl);
    dd_rebind_tbl("throttle.position", &throttle_current, tbl);
  }
}

/*
 * AccelerationModule::arbitrate
 *
 * Arbitrator for AccelerationModule.  The artibiter receives acceleration
 * commands from the actuationInterface and sends them to control.  At
 * present, the only directives that are recognized are new acceleration
 * commands, which replace old acceleration commands.
 *
 */

void AccelerationModule::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  AccelControlStatus *controlResponse =
    dynamic_cast<AccelControlStatus *>(cs);

  AccelMergedDirective *mergedDirective =
    dynamic_cast<AccelMergedDirective *>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  AdriveResponse response;

  switch (controlResponse->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::COMPLETED;
    adriveAccelInterfaceNF->sendResponse( &response, response.id );
    gclog(1) << response.toString() << endl;
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::FAILED;
    response.reason = controlResponse->reason;
    adriveAccelInterfaceNF->sendResponse( &response, response.id );
    gclog(1) << response.toString() << endl;
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */

  /* Check to see if we have rec'd a new directive from actuation interface */
  if (adriveAccelInterfaceNF->haveNewDirective()) {
    /* Keep track of the time when we receive commands */
    DGCgettime(command_time);

    /* Get the latest directive instead of the oldest unread one, so
       we add the second parameter to getNewDirective. */
    AdriveDirective newDirective;
    adriveAccelInterfaceNF->getNewDirective( &newDirective, true );
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    gclog(2) << t << "\tReceived acc command: id = " << newDirective.id << endl;

    /* !! Make sure that the directive has a unique ID !! */
#   warning need to add check for unique directive ID

    /* Save the ID to pass back when we are done */
    mergedDirective->parent_id = newDirective.id;

    /* See if we are in pause mode */
    if (system_paused && newDirective.command != ReleasePause &&
	newDirective.command != Reset) {
      /* Respond with a failure */
      response.id = newDirective.id;
      response.status = GcInterfaceDirectiveStatus::REJECTED;
      response.reason = VehiclePaused;
      adriveAccelInterfaceNF->sendResponse( &response, response.id );
      gclog(1) << response.toString() << endl;
      return;
    }

    switch (newDirective.command) {
    case SetPosition:
      /* Make sure we are properly initalized */
      if ((newDirective.arg > 0 && !throttle_initialized) ||
	  (newDirective.arg < 0 && !brake_initialized)) {
	/* Respond with a failure */
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::FAILED;
	response.reason = NotInitialized;
	adriveAccelInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Check the range of the command */
      if (newDirective.arg < -1 || newDirective.arg > 1) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = OutOfRange;
	adriveAccelInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Generate the directive to accel the car */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = newDirective.arg;
      mergedDirective->id = ++m_latestID;

      /* Mark the system as *not* paused (to be sure we know the status) */
      system_paused = 0;

      break;

    case Pause:
      /* Mark the system as paused */
      system_paused = 1;
      cerr << "Accel: received Pause" << endl;

      /* Generate the directive to decelerate the car */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = -pause_braking;
      mergedDirective->id = ++m_latestID;
      break;
      
    case ReleasePause:
      /* Release the pause state (but leave brake depressed */
      system_paused = 0;
      cerr << "Accel: received ReleasePause" << endl;

      response.id = newDirective.id;
      response.status = GcInterfaceDirectiveStatus::COMPLETED;
      adriveAccelInterfaceNF->sendResponse( &response, response.id );
      break;

    case Reset:
      /* !! Check to make sure we are stopped !! */
      mergedDirective->command = Reset;
      mergedDirective->id = ++m_latestID;
      break;
    }
  }
}

/*
 * AccelerationModule::control
 *
 * Control for AccelerationModule.  The artibiter receives merged
 * directives the AccelerationModule arbitrator.  For acceleration, this is
 * just the latest acceleration command, which is sent on to the hardware.
 * The m_currentID tag is used to keep track of which directive we are
 * currently excuting.
 *
 */

void AccelerationModule::control(ControlStatus *cs, MergedDirective *md)
{
  AccelControlStatus* controlResponse =
    dynamic_cast<AccelControlStatus *>(cs);
  AccelMergedDirective* mergedDirective =
    dynamic_cast<AccelMergedDirective *>(md);

  /* First check to see if we have anything new to do */
  if (mergedDirective->id == m_currentID) {
    /* Tell the arbiter we are still running */
    controlResponse->status = ControlStatus::RUNNING;
    return;
  }

  /* New directive to process: update IDs and counters (for sparrow) */
  m_currentID = mergedDirective->id;
  brake_command = mergedDirective->arg < 0 ? -mergedDirective->arg : 0;
  throttle_command = mergedDirective->arg > 0 ? mergedDirective->arg : 0;
  ++controlCount;

  switch (mergedDirective->command) {
  case SetPosition:
    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_given) {
      drivecmd_t my_command;
      my_command.my_actuator = accel;
      my_command.my_command_type = set_position;
      my_command.number_arg = mergedDirective->arg;
      adrive->m_skynet->send_msg(adrive->m_drivesocket, &my_command, 
				 sizeof(my_command), 0);

    } else {
      /* Decide what to do based on the sign of the acceleration */
      if (mergedDirective->arg > 0) {
	/* Acceleration commanded; First release the brake */
	if (brake_connected) {
	  pthread_mutex_lock(&brakeMutex);
	  brake_setposition(0);
	  pthread_mutex_unlock(&brakeMutex);
	}

	/* Now send the command to the throttle controller */
	if (throttle_connected) {
	  pthread_mutex_lock(&throttleMutex);
	  throttle_setposition(throttle_command);
	  pthread_mutex_unlock(&throttleMutex);
	}
      } else {
	/* Braking commanded; send the command to the brake controller */
	pthread_mutex_lock(&brakeMutex);
	if (brake_connected) brake_setposition(brake_command);
	pthread_mutex_unlock(&brakeMutex);

	/* Release the throttle as well */
	pthread_mutex_lock(&throttleMutex);
	if (throttle_connected) throttle_setposition(0);
	pthread_mutex_unlock(&throttleMutex);
      }
    }

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  case Reset:
    /* Initialize the brake actuator (if connected) */
    int status;
    if (verbose >= 4) cerr << "initializing brake" << endl;
    initializeBrake();

    if (verbose >= 4) cerr << "initializing throttle" << endl;
    initializeThrottle();

    /* Make sure that it worked */
    if (brake_initialized && throttle_initialized) {
      controlResponse->status = ControlStatus::STOPPED;

    } else {
      controlResponse->status = ControlStatus::FAILED;
      controlResponse->reason = InitializationError;
    }      
    controlResponse->id = m_currentID;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;
  }
}

/*
 * AccelerationModule::status
 *
 * This function is called as a thread and is used to read the status
 * of the actuator.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void AccelerationModule::status()
{
  while (1) {

    if (adrive->options->simulate_given) {
      /* In simulation mode, read state from asim */
      brake_pressure = adrive->asim_state.m_brakepressure;
      throttle_current = adrive->asim_state.m_gaspos;
      brake_current = adrive->asim_state.m_brakepos;

    } else {
      /* Fist check the brake actuator */
      if (!brake_connected) {
	/* Acceleration is currently disabled, so just return zero */
	brake_current = 0;
	brake_pressure = 0;

      } else {
	/* Read the current position */
	pthread_mutex_lock(&brakeMutex);
	double retval;
	retval = brake_getposition();
	pthread_mutex_unlock(&brakeMutex);

	/* Check the status and set state appropriately */
	if (retval < 0) {
	  /* Error condition */
#         warning brake status error not yet handled
	  if (verbose) 
	    cerr << "AccelerationModule::error reading brake position"
		 << endl;

	  /* Reset the actuator color to signal there might be a problem */
	  if (dd_tbl != NULL && !adrive->options->disable_console_flag)
	    dd_setcolor_tbl(m_brake_id, BLACK, YELLOW, dd_tbl);
      
	} else {
	  brake_current = retval;
	}
	
	/* Read the current pressure */
	pthread_mutex_lock(&brakeMutex);
	retval = brake_getpressure();
	pthread_mutex_unlock(&brakeMutex);

	/* Check the status and set state appropriately */
	if (retval < 0) {
	  /* Error condition */
	  if (verbose) 
	    cerr << "AccelerationModule::error reading brake pressure"
		 << endl;
	} else {
	  brake_pressure = retval;
	}
      }

      /* Now check the throttle actuator */
      if (!throttle_connected) {
	/* Acceleration is currently disabled, so just return zero */
	throttle_current = 0;

      } else {
	/* Read the current status */
	pthread_mutex_lock(&throttleMutex);
	double retval = throttle_getposition();
	pthread_mutex_unlock(&throttleMutex);

	/* Check the status and set state appropriately */
	if (retval < 0) {
	  /* Error condition */
#         warning acceleration status error not yet handled
	  if (verbose)
	    cerr << "AccelerationModule::error reading throttle position"
		 << endl;

	  /* Reset the actuator color to signal there might be a problem */
	  if (dd_tbl != NULL && !adrive->options->disable_console_flag)
	    dd_setcolor_tbl(m_throttle_id, BLACK, YELLOW, dd_tbl);

	} else {
	  throttle_current = retval;
	}
      }
    }

    /* Now fill up the actuatorState struct with current state */
    adrive->state.m_brakestatus = brake_initialized && brake_connected;
    adrive->state.m_brakepos = brake_current;
    adrive->state.m_brakecmd = brake_command;

    adrive->state.m_gasstatus = 
      throttle_initialized && throttle_connected;
    adrive->state.m_gaspos = throttle_current;

    /* Now that we have the state, check to see if there are any problems */
    /* TODO: implement something here */

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

void AccelerationModule::initializeBrake()
{
  if (adrive->options->simulate_given) {
    if (verbose > 5) cerr << "AccelerationModule: simulation mode" << endl;
    gclog(1) << "AccelerationModule: simulation mode" << endl;

    /* Simulation mode - no action required */
    brake_initialized = 1;    brake_connected = 1;

  } else if (adrive->options->disable_brake_flag) {
    /* Don't bother trying to connect, but act as if initialized */
    brake_connected = 0;
    brake_initialized = 1;

  } else {
    /* Close the port if it is open */
    if (brake_connected) { 
      pthread_mutex_lock(&brakeMutex);
      brake_close(); brake_connected=0; 
      pthread_mutex_unlock(&brakeMutex);
    }

    /* Open the port to the brake actuator */
    if (verbose >= 3) cerr << "AccelerationModule: opening brake" << endl;
    gclog(1) << "AccelerationModule: opening brake" << endl;

    pthread_mutex_lock(&brakeMutex);
    int status = brake_open(adrive->options->brake_port_arg, verbose);
    pthread_mutex_unlock(&brakeMutex);

    if (!status) {
      /* Don't allow operation without being able to talk to acceleration */
      if (verbose) cerr << "AccelerationModule: brake_open failed" << endl;
      gclog(1) << "AccelerationModule: brake_open failed" << endl;
      exit(1);
    }

    /* Keep track of the fact that we are connected */
    brake_connected = 1;

#ifdef CHECK_BRAKE_INIT
    /* See if we are properly initalized */
    pthread_mutex_lock(&brakeMutex);
    int curpos = brake_getposition();
    pthread_mutex_unlock(&brakeMutex);
    brake_initialized = !(curpos < 0);
#else
    brake_initialized = 1;
#endif
  }
}

void AccelerationModule::initializeThrottle()
{
  if (adrive->options->simulate_given) {
    if (verbose > 5) cerr << "AccelerationModule: simulation mode" << endl;
    gclog(1) << "AccelerationModule: simulation mode" << endl;

    /* Simulation mode - no action required */
    throttle_initialized = 1; throttle_connected = 1;

  } else if (adrive->options->disable_gas_flag) {
    /* Don't bother trying to connect, but act as if initialized */
    throttle_connected = 0;
    throttle_initialized = 1;

  } else {
    /* Close the port if it is open */
    if (throttle_connected) { 
      pthread_mutex_lock(&throttleMutex);
      throttle_close(); throttle_connected=0; 
      pthread_mutex_unlock(&throttleMutex);
    }

    /* Open the port to the throttle actuator */
    if (verbose) cerr << "AccelerationModule: opening throttle" << endl;
    gclog(1) << "AccelerationModule: opening throttle" << endl;

    pthread_mutex_lock(&throttleMutex);
    int status = throttle_open(adrive->options->gas_port_arg);
    pthread_mutex_unlock(&throttleMutex);

    if (!status) {
      /* Don't allow operation without being able to talk to acceleration */
      if (verbose) cerr << "AccelerationModule: throttle_open failed" << endl;
      gclog(1) << "AccelerationModule: throttle_open failed" << endl;
      exit(1);
    }

    /* Keep track of the fact that we are connected */
    throttle_connected = 1;

#ifdef CHECK_THROTTLE_INIT
    /* See if we are properly initalized */
    pthread_mutex_lock(&throttleMutex);
    int curpos = throttle_getposition();
    pthread_mutex_unlock(&throttleMutex);
    throttle_initialized = !(curpos < 0);
#else
    throttle_initialized = 1;
#endif
  }
}

