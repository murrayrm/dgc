/*!
 * \file ActuationInterface.cc
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This file receives directives for adrives using the GcInterface
 * class and sends them off to the appropriate actuators.
 */

#include <assert.h>
#include <sys/stat.h>
#include "ActuationInterface.hh"
using namespace std;

/* Constructor */
ActuationInterface::ActuationInterface(int skynet_key, 
				       struct gengetopt_args_info *cmdline) 
{
  /* Save the command line options that we were passed */
  options = cmdline;

  /* Get the GcPortHandler */
  m_adriveCommandHandler = new GcPortHandler();
  m_steerHandler = new GcPortHandler();
  m_accelHandler = new GcPortHandler();
  m_transHandler = new GcPortHandler();
  m_turnHandler = new GcPortHandler();

  /* Get the GcModuleLogger */
  m_logger = NULL;
  if (options->log_level_arg > 0) {
    m_logger = new GcModuleLogger("ActuationInterface");
    ostringstream oss;
    oss << options->log_file_arg << "-actuationInterface";
    string logFileName = oss.str();
    m_logger->addLogfile(logFileName);
    m_logger->setLogLevel(options->log_level_arg);
  }

  /* Get the north face to trajfollower */
  m_adriveCommandNF = AdriveCommand::
    generateNorthface(skynet_key, m_adriveCommandHandler, m_logger);
  
  /* Get the the south face to the actuators */
  m_steer = AdriveSteerInterface::
    generateSouthface(skynet_key, m_steerHandler, m_logger); 
  m_accel = AdriveAccelInterface::
    generateSouthface(skynet_key, m_accelHandler, m_logger);
  m_trans = AdriveTransInterface::
    generateSouthface(skynet_key, m_transHandler, m_logger);
  m_turn = AdriveTurnInterface::
    generateSouthface(skynet_key, m_turnHandler, m_logger);

  /* Limit the number of directives and responses stored in GcInterface */
  m_adriveCommandNF->setStaleThreshold(10);
  m_steer->setStaleThreshold(10);
  m_accel->setStaleThreshold(10);
  m_trans->setStaleThreshold(10);
  m_turn->setStaleThreshold(10);

  /* 
   * Start up the status loop, which reads/sends ActuatorState 
   *
   */
  m_skynet = new skynet(SNadrive, skynet_key);

  /* Check to see if we should start up the simulator */
  asim_status = 0;
  if (cmdline->simulate_given) {
    /* In simulation, we communicate with asim */
    m_drivesocket = m_skynet->get_send_sock(SNdrivecmd);
    m_asimsocket = m_skynet->listen(SNasimActuatorState, ALLMODULES);
    DGCstartMemberFunctionThread(this, 
				 &ActuationInterface::getSimActuatorState);
  }

  /* Initialize the thread that sends out state information */
  m_statesocket = m_skynet->get_send_sock(SNactuatorstate);
  statusCount = 0;
  statusSleepTime = STATUS_SLEEP/2;
  DGCstartMemberFunctionThread(this, &ActuationInterface::status);

  /* Mutex for controlling access to actuator state */
  DGCcreateMutex(&actuatorStateMutex);

  /* Initialize the estop state */
  state.m_astoppos = EstopPause;
  state.m_estoppos = EstopPause;
  actuation_state = Unknown;
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void ActuationInterface::initSparrow(DD_IDENT *tbl, int ACTSTATE_LABEL)
{
  if (!options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (options->simulate_given) {
      /* In simulation modue */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("adrive.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("estop.estoppos", &state.m_estoppos, tbl);
    dd_rebind_tbl("actuation_state", &actuation_state, tbl);
  }
}

/*
 * ActuationInterface::status
 *
 * This function is called as a thread and is used to read the status
 * from the individual actuators and broadcast this information across
 * the SNactuationstate channel.
 *
 */

void ActuationInterface::status()
{
  /* Broadcast the current state */
  while (1) {
    /* Fill in any part of the state that we are responsible for */
    state.m_estoppos = 
      (actuation_state == Running) ? EstopRun :
      (actuation_state == Disabled) ? EstopDisable : 
      EstopPause;

    /* Broadcast the state out to the world */
    if (m_skynet->send_msg(m_statesocket, &state,
			   sizeof(state), 0) != sizeof(state)) {
      cerr << "ActuationInterface::status send_msg error" << endl;
    }

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

void ActuationInterface::arbitrate()
{
  unsigned long long actuation_resume_start;

  while (true) {
    /*
     * Determine the state of the actuation interface
     *
     * At this point, we check the estop status to see if this causes
     * any change in state.  Further processing of the state is done
     * later based on directives and responses (for changing gears).
     *
     * Note: if estop is disabled (from the command line), the status
     * of the actuator has to be set from the display.
     */

    /* Make sure we are initialized to Pause condition */
    if (actuation_state == Unknown) {
      sendPauseCommand(Pause);
      actuation_state = Paused;
    }

    /* Check to see if we have received a Disable command (locking) */
    else if (actuation_state == Disabled || 
	     state.m_dstoppos == EstopDisable) { 
      /* Issue a brake command (repeat each time to be sure) */
      sendPauseCommand(Pause);

      /* Set the state to disabled (and stay there) */
      actuation_state = Disabled;

      /* Delay for a bit so that we don't overload the actuation drivers */
      DGCusleep(FAST_SLEEP*10);
    } 

    /* Check to see if we have received a Pause command.  We can get
       here from either Running, Paused, Resuming or Shifting. */
    else if (state.m_dstoppos == EstopPause) {
      /* Issue a brake command (repeat each time to be sure) */
      sendPauseCommand(Pause);

      /* Save the state of the system */
      actuation_state = Paused;

      /* Delay for a bit so that we don't overload the actuation drivers */
      DGCusleep(FAST_SLEEP*10);
    }

    /* Check to make sure we are receiving commands; otherwise Pause */
    else if (actuation_state == Running && 
	     !options->disable_command_timeout_flag && 
	     (drive_command_time > 0) && (DGCgettime() - drive_command_time 
					  > options->command_timeout_arg)) {
      /* We haven't received a command, force a pause */
      cerr << "Didn't receive command in " << options->command_timeout_arg
	   << " usec" << endl;
      sendPauseCommand(Pause);
      actuation_state = Paused;

      /* Delay for a bit so that we don't overload the actuation drivers */
      DGCusleep(SLOW_SLEEP);
    }

    /* See if we are coming out of a pause mode */
    else if (actuation_state == Paused && state.m_dstoppos == EstopRun) {
      /* Set up a timer to keep track of when it is OK to come out of pause */
      actuation_resume_start = DGCgettime();
      actuation_state = Resuming;
    }

    /* See if we are waiting to come out of pause mode */
    else if (actuation_state == Resuming) {
      unsigned long long time_pausing = DGCgettime() - actuation_resume_start;
      if (DGCtimetosec(time_pausing) > 5) {
	sendPauseCommand(ReleasePause);
	actuation_state = Running;
      }
    }

    /*
     * At this point the actuation_state is set appropriately and we
     * can decide what to do.  Here are the options:
     *
     *  - Running: send through all commands
     *  - Paused: only send through steering commands; reject others
     *  - Resuming: same as pause
     *  - Shifting: changing gears; reject all commands
     *  - Disabled: reject all commands
     *
     */

    /* Call pumpPorts to receive all the messages */
    m_adriveCommandHandler->pumpPorts();
    m_steerHandler->pumpPorts();
    m_accelHandler->pumpPorts();
    m_transHandler->pumpPorts();
    m_turnHandler->pumpPorts();

    /* Pass on all the received directives to apropriate actuator. Put
       this in a while loop to make sure that the router can keep up
       with trajfollower. */
    while (m_adriveCommandNF->haveNewDirective()) {
      m_adriveCommandNF->getNewDirective( &m_adriveDirective );

      /* If this is a Steer or Accel command, update our timer */
      if (m_adriveDirective.actuator == Steering ||
	  m_adriveDirective.actuator == Acceleration) {
	DGCgettime(drive_command_time);
      }

      /* If we are disabled or shifting, reject all commands */
      if (actuation_state == Disabled || actuation_state == Shifting) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehiclePaused;
	m_adriveCommandNF->sendResponse(&response, response.id);
	continue;
      }

      /* If we are paused, only accept steering and reet commands */
      if ((actuation_state == Paused || actuation_state == Resuming) &&
	  (m_adriveDirective.actuator != Steering &&
	   m_adriveDirective.command != Reset)) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehiclePaused;
	m_adriveCommandNF->sendResponse(&response, response.id);
	continue;
      }

      /* If we try to reset any acutator, make sure we are stopped */
      /* Make sure that the car is stopped */
      if (m_adriveDirective.command == Reset &&
	  state.m_VehicleWheelSpeed > ACTUATION_RESET_SPEED) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehicleNotStopped;
	m_adriveCommandNF->sendResponse( &response, response.id );
	return;
      }

      /* Otherwise, we can process the command we are sent */
      switch (m_adriveDirective.actuator) {
      case Steering:
	m_steer->sendDirective(&m_adriveDirective);
	break;

      case Acceleration:
	m_accel->sendDirective(&m_adriveDirective);
	break;

      case Transmission:
	m_trans->sendDirective(&m_adriveDirective);

	/* Keep track of the fact that we are now shifting */
	actuation_state = Shifting;
	break;

      case TurnSignal:
	m_turn->sendDirective(&m_adriveDirective);
	break;

      default:
	cerr << "ActuationInterface: received command for unknown actuator: "
	     << m_adriveDirective.actuator << endl;
      }
    }

    /* Check for return status message and pass them back to trajFollower */
    if (m_steer->haveNewStatus()) {
      AdriveResponse *steerResponse = m_steer->getLatestStatus();
      m_adriveCommandNF->sendResponse(steerResponse, steerResponse->id);
    }

    if (m_accel->haveNewStatus()) {
      AdriveResponse *accelResponse = m_accel->getLatestStatus();
      m_adriveCommandNF->sendResponse(accelResponse, accelResponse->id);
    }

    if (m_trans->haveNewStatus()) {
      AdriveResponse *transResponse = m_trans->getLatestStatus();
      m_adriveCommandNF->sendResponse(transResponse, transResponse->id);

      /* 
       * Check to see if we can come out of shifting mode
       *
       * The only way we can get into this state is if we were sent a
       * transmission command while in "Running" mode and we received
       * a response.  Independent of what the response says, we are no
       * longer shifting at this point. 
       */
      if (actuation_state == Shifting) {
	actuation_state = Running;
      }
    }

    if (m_turn->haveNewStatus()) {
      AdriveResponse *turnResponse = m_turn->getLatestStatus();
      m_adriveCommandNF->sendResponse(turnResponse, turnResponse->id);
    }

    /* Sleep for a short while (remember this is in the control loop) */
    DGCusleep(FAST_SLEEP);
  }
}

/*
 * ActuationInterface::getSimActuatorState
 *
 * This thread reads the simulated actuator state from asim.  This
 * information is stored so that it can be accessed by the individual
 * actuators.
 *
 */

void ActuationInterface::getSimActuatorState()
{
  /* Read actuator state from skynet */
  while (1) {
#   warning missing mutex locking for reading simulation state
    if (m_skynet->get_msg(m_asimsocket, &asim_state, sizeof(state)) 
	!= sizeof(state)) {
      cerr << "ActuationInterface::status get_msg error" << endl;
    } else {
      /* Let everyone know that they can grab the state now */
      asim_status = 1;
    }
  }
}

/*! Send pause/release to acceleration module */
void ActuationInterface::sendPauseCommand(AdriveCommandType cmd)
{
  static int estop_id = 0x8000;

  /* Make sure we got passed something sensible */
  assert(cmd == Pause || cmd == ReleasePause);

  AdriveDirective pauseCommand;
  pauseCommand.id = estop_id++;
  pauseCommand.command = cmd;
  pauseCommand.actuator = Acceleration;

  m_accel->sendDirective(&pauseCommand);
  m_accelHandler->pumpPorts();

  /* Wait until we get a response and read it */
  int i;
  const int MAXITER = 20;
  for (i = 0; i < MAXITER; ++i) {
    if (m_accel->haveNewStatus()) { 
      AdriveResponse *response = m_accel->getLatestStatus();
      assert(response != NULL);

      /* Make sure this response was intended for us */
      if (response->id != pauseCommand.id) {
	/* Response to someone else; pass back to sender */
	m_adriveCommandNF->sendResponse(response, response->id);
      } else
	/* This was our acknowledgement; break out of loop */
	break;
    } else {
      usleep(FAST_SLEEP*10);
      m_accelHandler->pumpPorts();
    }
  }

  /* Make sure we received a response */
  if (i == MAXITER) {
    cerr << "ActuationInterface: didn't receive response to "
	 << (cmd == Pause ? "pause" : "release") << endl;
  }
}

/*
 * Startup functions
 *
 * The autostart() and autoshift() functions are used at startup to
 * initilized the state of the vehicle.
 *
 */

# define WAIT_SLEEP 500000		// time to wait between status checks
# define WAIT_COUNT 20  		// maximum number of wait iterations

int ActuationInterface::autoshift(int verbose)
{
  /* Make sure transmission module is running */
  if (options->disable_trans_flag) {
    cerr << "transmission disabled; ";
    return -1;
  }

  /* Wait to make sure that status loop as run before we do anything */
  int i;
  for (i = 0; i < WAIT_COUNT; ++i) {
    if (state.m_trans_update_time > 0) break;
    DGCusleep(WAIT_SLEEP);
  }
  if (i == WAIT_COUNT) {
    cerr << "timed out waiting for transmission status; ";
    exit(1);
  }

  /* Check to see if the transmission is connected */
  if (!state.m_transstatus) {
    cerr << "transmission not connected and/or initialized; ";
    return -1;
  }

  /* Check to see if we are already in gear */
  if (state.m_transpos == 1 || state.m_transpos == -1) {
    if (verbose) cerr << "transmission already in gear" << endl;
  } else {
    /* Shift into drive */
    if (verbose) cerr << "shifting into drive" << endl;

    /* Issue a shift command directly through the transmission actuator */
    AdriveDirective adriveCommand;
    adriveCommand.id = 0;
    adriveCommand.actuator = Transmission;
    adriveCommand.command = SetPosition;
    adriveCommand.arg = 1;
    m_trans->sendDirective(&adriveCommand);
    m_transHandler->pumpPorts();

    /* Verify that the shift occured */
    if (verbose >= 2) cerr << "checking that shift occured" << endl;
    for (i = 0; i < WAIT_COUNT; ++i) {
      m_transHandler->pumpPorts();
      if (m_trans->haveNewStatus()) {
	AdriveResponse *response = m_trans->getLatestStatus();
	if (response->status != GcInterfaceDirectiveStatus::COMPLETED) {
	  cerr << "couldn't shift gears; response status = " 
	       << response->status;
	  return -1;
	}

	/* Successfully shifted gears; break out of loop */
	break;
      }
      DGCusleep(WAIT_SLEEP);
    }
    if (i == WAIT_COUNT) {
      cerr << "timed out waiting for shift; ";
      return -1;
    }

    if (verbose) cerr << "auto-shift complete" << endl;
  }

  return 0;
}
