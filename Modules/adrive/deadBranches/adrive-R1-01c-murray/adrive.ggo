#
# This is the source file for managing adrive command-line options.  
# It uses gengetopt (http://www.gnu.org/software/gengetopt/gengetopt.html)
#
# Lars Cremean, 2006-10-03
# Richard Murray, 2006-12-22
#
# Specification:
#   (1) no command-line options required if default settings are ok
#   (2) Command-line options will override default settings
#   (3) Each thread can be disabled from the cmd line
# 
# Implementation:
#   
#   --disable-<option> will disable the specified function
#   --enable-<option> will enable selected functions
#   short options available (lower case for disable, upper for enable)

package "adrive"
purpose "Adrive provides an interface to Alice's actuation hardware,
including the e-stop functionality."
version "1.0"

# Options for turning off actuator threads
option "disable-brake" b "disable brake interface" flag off
option "disable-sparrow" d "turn off sparrow display" flag  off
option "disable-estop" e "disable estop interface" flag off
option "disable-gas" g "disable throttle interface" flag off
option "disable-obd" o "disable OBD II interface" flag off
option "disable-steer" s "disable steering interface" flag off
option "disable-trans" t "disable transmission (and ignition) interface"
  flag off 
option "disable-interlocks" x "disable safety interlocks" flag off
option "enable-interlocks" X "enable safety interlocks" flag off
option "fake-obd" - "use vehicle state to infer OBD settings" flag on

# Gcdrive options
option "simulate" - "send commmands to simulator (beat)" flag off
option "log-level" - "set the log level" int default="0" no argoptional
option "log-file" - "use filename as where to save logs. By default, logs will be saved in yyyy-mm-dd-ddd-hh-mm-gcdrive-{acceleration, actuationInterface, steering, transmission}.log" 
       string default="" no

# Configuration files
# text ""
option "config" - "configuration file"
  string default="adrive.config" no 
option "noconfig" - "don't load configuration file at startup" flag off
option "argfile" - "file containing command line arguments" string no

# Standard options
# text ""
option "skynet-key" S "skynet key" int default="0" no
option "enable-astate" - "enable astate (from Applanix)" flag off
option "disable-console" D "turn off sparrow display" flag  off
option "verbose" v "turn on verbose messages" int default="1" no argoptional

# Hidden options - used for configuring low level interface
option "steer-port" - "serial port to use for steering" int default="0" no
option "gas-port" - "serial port to use for throttle" int default="4" no
option "brake-port" - "serial port to use for brake" int default="9" no
option "trans-port" - "serial port to use for transmission" int default="5" no
option "estop-port" - "serial port to use for estop" int default="8" no
option "obdii-port" - "serial port to use for OBD II" int default="7" no
option "test-mode" - "run for 10 seconds then quite" flag off
