/*!
 * \file OBDIISensor.hh
 * \brief Header file for OBDIISensor class
 *
 * \author Richard Murray
 * \date 22 April 2007
 *
 * This file defines the OBDIISensor class for adrive.  This is
 * used to communicate with the OBD-II hardware.
 */

#ifndef __OBDIISensor_hh__
#define __OBDIISensor_hh__

#include <pthread.h>

#include "sparrow/display.h"
#include "skynettalker/StateClient.hh"

#include "ActuationInterface.hh"

/*
 * \class OBDIISensor
 *
 * This class is used to represent the estop object.
 */

class OBDIISensor

{
private:
  /*!\param Actuation interface */
  ActuationInterface *adrive;

  void status();
  int statusCount;
  long statusSleepTime;

  /* Status information */
  int initialized;		/*!< Steering is properly initialized */
  int connected;		/*!< Able to talk to the motor controller */
  double engineRPM;
  double engineCoolantTemp;
  double vehicleWheelSpeed;
  double currentGearRatio;

  /* State client in case we need to fake OBD II information */
  void getVehicleState();	// Thread to obtain vehicle state
  VehicleState vehState;	// Memory for storing the vehicle state
  int fakeCount;		// Keep track of number of times we call it

  int sparrowLabel;		/*! Label for sparrow display */
  DD_IDENT *sparrowTable;	/*! Table where our status is kept */

  /*! Mutex controlling access to serial port and data */
  pthread_mutex_t sensorMutex;

public:
  /*! Constructor */
  OBDIISensor(ActuationInterface *, int);
  void initSparrow(DD_IDENT *, int);
  int getEngineRPM() { return engineRPM; }
  int getVehicleWheelSpeed() { return vehicleWheelSpeed; }
  int getCurrentGearRatio() { return currentGearRatio; }
};

#endif
