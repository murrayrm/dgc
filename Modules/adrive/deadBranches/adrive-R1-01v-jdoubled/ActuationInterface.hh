/*!
 * \file ActuationInterface.hh
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * \ingroup gcdrive
 *
 * This class serves as the GcModule interface to adrive.  It doesn't
 * actually perform any function, but instead passes the directives
 * that it receives directly to one of the actuation modules.
 */

#ifndef __ActuationInterface_HH__
#define __ActuationInterface_HH__

#include "gcmodule/GcInterface.hh"
#include "skynet/skynet.hh"
#include "interfaces/ActuatorState.h"
#include "interfaces/ActuatorCommand.h"		// for use with simulator
#include "sparrow/display.h"
#include "gcinterfaces/AdriveCommand.hh"

#include "cmdline.h"

/*
 * Set a few constants that govern the behavior of the models
 *
 * Rates: Adrive attempts to respond to all commands within a few
 * milliseconds, under the assumption that we could have a control
 * loop running at up to 100 Hz.  The main loop runs with a 1 msec
 * sleep and each of the fast actuators (accel, steering) have
 * additional 1 msec sleep times for the arbitrate and control
 * functions.  The slower actuators (transmission, turn signal) have
 * 100 msec sleep times and the status functions all run with 250-500
 * msec sleep times, to avoid polling the actuators too often.  Note
 * that the actuation status information should *not* be used for
 * closing loops.
 */

const int FAST_SLEEP = 1000;		// sleep time for fast loops
const int SLOW_SLEEP = 100000;		// sleep time for slow loops
const int STATUS_SLEEP = 250000;	// sleep time for status loops

const double ACTUATION_RESET_SPEED = 1.0;	// max speed for reseting

/*
 * GcInterfaces to the internal adrive modules
 *
 * The interfaces defined here are used to communicate with the
 * internal adrive modules for each actuator.  Since these are only
 * visible within adrive, we define them here (instead of in
 * AdriveCommand.hh).
 */

/*! Interface to the SteeringModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrive> AdriveSteerInterface;

/*! Interface to the AccelerationModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrive_commander> AdriveAccelInterface;

/*! Interface to the TransmissionModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNadrivelogger> AdriveTransInterface;

/*! Interface to the TurnSignalModule */
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
		    GCM_IN_PROCESS_DIRECTIVE, SNProcMon> AdriveTurnInterface;

///////////////////////////////////////
// class ActuationInterface
//
#ifdef GCMODULE_EVENT_BASED
/*! Adrive actuation interface. If we are using even-based gcmodules,
 *  then we derive from gcmodule to communicate with the other
 *  sub modules
 */
class ActuationInterface : public GcModule
#else
/*! Adrive actuation interface. It is not a gcmodule but use the lightweight
 *  GcInterface to communicate with the internal adrive modules*/
class ActuationInterface 
#endif
{
private:
  /* Logger */
  GcModuleLogger* m_logger;

  /* Input interface (nominally to TrajFollower) */
  AdriveCommand::Northface *m_adriveCommandNF;	// interface to trajfollower
  AdriveDirective m_adriveDirective;		// inputs from trajfollower

  /* Required member variables for GcModule */
  ControlStatus m_controlStatus;		// unused
  MergedDirective m_mergedDirective;		// unused

  /* GcPortHandler for sending/receiving directives and responses */
  GcPortHandler *m_adriveCommandHandler;
  GcPortHandler *m_steerHandler;
  GcPortHandler *m_accelHandler;
  GcPortHandler *m_transHandler;
  GcPortHandler *m_turnHandler;

  /*! Actuation status broadcast thread */
  void status();
  int statusCount;
  long statusSleepTime;
  pthread_mutex_t actuatorStateMutex;

  /*! Threads for PLC */
  void plcWriter();
  void plcReader();

  /*! ID number for sending commands to actuators */
  int actuation_id;

  /*! Keep track of how verbose messages should be */
  int verbose;

public:
  /* Controller interfaces */
  AdriveSteerInterface::Southface *m_steer;	// interface to steering
  AdriveAccelInterface::Southface *m_accel;	// interface to acceleration
  AdriveTransInterface::Southface *m_trans;	// interface to transmission
  AdriveTransInterface::Southface *m_turn;	// interface to turn signals

  struct gengetopt_args_info *options;		///! command line options
  skynet *m_skynet;				///! skynet socket for sim
  int m_statesocket;				///! actuation state socket
  ActuatorState state;				///! current state of actuators

  /* Variables for supporting simulation mode */
  int m_drivesocket;				///! adrive socket
  int m_asimsocket;				///! actuation state from asim
  ActuatorState asim_state;			///! simulated actuator state
  int asim_status;				///! asim message status
  void getSimActuatorState();

  /* Actuation state */
  enum {
    Running = 2,		// normal mode of operation
    Paused = 1,			// DARPA estop pause detected
    Disabled = 0,		// DARPA disable detected (terminal condition)
    Resuming = 3,		// Resuming from pause (5 second delay)
    Shifting = 4,		// Shifting gears (don't allow motion)
    Unknown = 8			// Initial state
  } actuation_state;

  /*! Keep track of when we were last given a drive (steer, accel) command */
  unsigned long long drive_command_time;

  ActuationInterface(int, struct gengetopt_args_info *, int verbose_flag = 0);


  void initSparrow(DD_IDENT *, int);

  /* Arbitrate and control functions (unused) */
#ifndef GCMODULE_EVENT_BASED
  virtual ~ActuationInterface() {
    delete m_adriveCommandHandler;
    delete m_steerHandler;
    delete m_accelHandler;
    delete m_transHandler;
    if (m_turnHandler) delete m_turnHandler;
    if (m_logger != NULL) delete m_logger;
  } 
  void arbitrate();
  void control();
#else
  virtual ~ActuationInterface() { } 
  void pumpPortsLocal(long nTime = -1) ;
  virtual void gcPumpThread();
  void handleMessageReadyCallbacks();
  void mainloopCallback() ;
  void timedWait( long nTime );
  void signaledWait( long nTime );
  void arbitrate(ControlStatus *, MergedDirective *);
  void control(ControlStatus *, MergedDirective *);
  bool Stop();

  bool m_bStopStatus;
#endif
  /*! Auto start function */
  int autoshift(int);

  /*! Utility function to send pause directive */
  void sendPauseCommand(AdriveCommandType);
};

#endif
