/*!
 * \file steercal.cc
 * \brief Calibrate steering
 * 
 * \author Richard Murray
 * \date 27 May 2007
 *
 * \ingroup utility
 *
 * This short program runs the steering calibration routine on Alice.
 *
 */

#include <stdio.h>
#include <iostream>
#include <assert.h>
#include "parker_steer.hh"

#include "sparrow/dbglib.h"

int main(int argc, char **argv)
{
  int port = 0;
  int status;

  /* Turn sparrow debugging information on */
  dbg_flag = dbg_all = 1;

  /* Get the port number from the command line, if present */
  if (argc > 1) {
    /* Make sure we got something sensible for an argument */
    if (argc > 2 || !isdigit(*argv[1])) {
      printf("Usage: %s [port #]\n", argv[0]);
      exit(1);
    }

    /* Get the steering port number */
    port = atoi(argv[1]);
  }

  printf("Opening steering on port /dev/ttyS%d\n", port);
  status = steer_open(port);
  if (status == 0) {
    /* Couldn't open steering, abort unless told otherwise */
    printf("Error: couldn't open steering port; continue (y/n)? ");
    if (getchar() != 'y') exit(1);
  } else if (status < 0) {
    /* Opened port, but couldn't set parameters; try to continue */
    printf("Warning: steer_open returned %d\n", status);
  }

  printf("Initializing steering\n");
  status = steer_init();
  if (!status) printf("Warning: steer_init returned %d\n", status);

  printf("Calibrating steering\n");
  status = steer_calibrate();
  if (!status) printf("Warning: steer_calibrate returned %d\n", status);

  printf("Closing steering\n");
  status = steer_close();
  if (!status) printf("Warning: steer_close returned %d\n", status);

  return 0;
}
