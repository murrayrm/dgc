// this files contains the drivers for Alice's ignition, transmission, and
// rear status LEDs
#include "igntrans.hh"

#include <iostream>
#include <sys/time.h>
// #include "GlobalConstants.h"
#include "throttle.hh"
// #include "SDSPort.h"
#include "sparrow/dbglib.h"
#include <string>

#ifdef IGNTRANS_SERIAL
#include <sstream>
#include <SerialStream.h>
using namespace LibSerial;
static SerialStream serial_port;
#endif

#ifdef IGNTRANS_SDS
SDSPort* igntransport;
int igntransPortOpen = 0;
extern char simulator_IP[];
int igntrans_serial_port = -1;  //not a valid serial port number, must assign
#endif //IGNTRANS_SDS

using namespace std;


/*************************************************************************
 * int igntrans_open(int port)
 *************************************************************************/
int igntrans_open(int port) {
  int error_value = 0;
  //cout <<"IAMHERE with port: " << port << endl;
#ifdef IGNTRANS_SERIAL
  ostringstream portname;
  portname << "/dev/ttyS" << port;
  
  serial_port.Open(portname.str());
  if ( ! serial_port )
    {
      cerr << "igntrans_open() error: Could not open " << portname.str() << endl ;
      return FALSE;
    }
  
  //make sure that the error bit is clear before attempting initialization.
  if (! serial_port.good() ) {
    serial_port.clear();
  }
  
  serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open() error: Could not set the baud rate." << endl ;
      error_value++;
      //return FALSE;
    }
  serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open error: Could not disable the parity." << endl ;
      error_value++;
      //return FALSE;
    }
  serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open error: Could not turn off hardware flow control." << endl;
      error_value++;
      //return FALSE;
    }
  serial_port.SetNumOfStopBits( 1 ) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open error: Could not set the number of stop bits."  << endl;
      error_value++;
      //return FALSE;
    }
  serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8) ;
  if ( ! serial_port.good() )
    {
      cerr << "igntrans_open() error: Could not set the char size." << endl ;
      error_value++;
      //return FALSE;
    }
  
  if ( error_value > 0 ) {
    return FALSE;
  }
  
  
#endif //IGNTRANS_SERIAL
  
#ifdef IGNTRANS_SDS 
  if(igntransPortOpen == 0)
    {
      igntransport = new SDSPort("192.168.0.60", port);
      igntransPortOpen = 1;
      igntrans_serial_port = port;
    }
  
  igntransport->openPort();
  
  if(igntransport->isOpen() == 1)
    {
      igntransPortOpen = 1;
      igntrans_serial_port = port;
      //return TRUE;
    }
  else
    {
      return FALSE;
    }
#endif //IGNTRANS_SDS
  //cout << "at end of init, before getposition" << endl;

  return TRUE;
}

/* Initialize the ignition/transmisison system */
int igntrans_init()
{
  // perform a read to check that we are actually receiving good data
  if (trans_getposition() == TRANS_ERROR) {
    cout << "trans_getposition failed" << endl;
    return FALSE;
  }
  else {
    //    cout << "trans_getposition succeeded" << endl;
    return TRUE;
  }
}



/**************************************************************************
 * void simulator_igntrans_open(int)
 * Author: David Rosen
 * Functionality: Opens the igntrans port for the simulator
 * Returns: None
 **************************************************************************/
void simulator_igntrans_open(int port)
{
#ifdef IGNTRANS_SDS
  igntransport = new SDSPort(simulator_IP, port);
  igntransPortOpen = 1;
  igntrans_serial_port = port;
#endif //IGNTRANS_SDS
}



/**************************************************************************
 * int igntrans_close(void)
 * Author: Jeff Lamb
 * Functionality: Closes the igntrans port
 * Returns: TRUE  (1) on success
 *          FALSE (0) on failure
 **************************************************************************/
int igntrans_close(){
#ifdef IGNTRANS_SERIAL
  serial_port.Close();
  return TRUE;
#endif //IGNTRANS_serial
  
#ifdef IGNTRANS_SDS
  if(igntransport->closePort() == 0)
    {
      igntransPortOpen = 0;
      return TRUE;
    }
  else
    {
      return FALSE;
    }
#endif //IGNTRANS_SDS
}




/**************************************************************************
 * int trans_setposition(int position)
 **************************************************************************/
int trans_setposition(int position) {
  //cout << "got set_position: " << position << endl;

  char positionString[SET_POS_LEN+1];
  //string positionString;
  // set up the command string with the proper characters
  //positionString[DEVICE_TYPE] = TRANS;
  //positionString[DEVICE_DIRECTION] = SEND;
  //positionString[SEND_EOL_POS] = EOL;
  //positionString[SEND_EOT_POS] = EOT;
  
  // verify the commanded position is valid
  if (position == T_PARK) {
    //positionString[DEVICE_DATA] = TRANS_PARK;
    sprintf(positionString, "TSP\n\r");
  }
  else if (position == T_REVERSE) {
    //positionString[DEVICE_DATA] = TRANS_REVERSE;
    sprintf(positionString, "TSR\n\r");
  }
  else if (position == T_NEUTRAL) {
    //positionString[DEVICE_DATA] = TRANS_NEUTRAL;
    sprintf(positionString, "TSN\n\r");
  }
  else if (position == T_DRIVE) {
    //positionString[DEVICE_DATA] = TRANS_DRIVE;
    sprintf(positionString, "TSD\n\r");
  }
  else return ERROR;

  //cout << "trans position string: " << positionString << endl;
  //write the data
#ifdef IGNTRANS_SERIAL
  if (!serial_port.write(positionString, SET_POS_LEN-1)) return ERROR;
  else return TRUE;
#endif //IGNTRANS_SERIAL

#ifdef IGNTRANS_SDS
  igntransport->clean();
  if (!igntransport->write(positionString)) {
    cout << "write failed!!" << endl;
    return ERROR;
  }
  else return TRUE;
#endif //igntrans_sds
    // to do: building in verification write from actuator to verify reception
}


/**************************************************************************
 * int trans_getposition(void)
 **************************************************************************/
int trans_getposition(void) {
  char positionString[GET_POS_LEN+2];
  int transPosition;
  
  // setup the position query string to send to the device
  //  positionString[SOT_POS] = SOT;
  //positionString[DEVICE_TYPE] = TRANS;
  //positionString[DEVICE_DIRECTION] = RECEIVE;
  //positionString[RECEIVE_EOL_POS] = EOL;
  sprintf(positionString, "TR\n\r");

  //cout << "trans_getposition is sending: " << positionString << endl << endl << endl;
  //write the position query string
#ifdef IGNTRANS_SERIAL
  if (!serial_port.write(positionString, GET_POS_LEN)) return TRANS_ERROR;
  //cout << "finished writing getposition string" << endl;
  //usleep(30000);

  serial_port.clear();
  if (!serial_port.read(positionString, GET_POS_LEN)) return TRANS_ERROR;
  //if (!serial_port.good()) return TRANS_ERROR;
  //serial_port.read(positionString, GET_POS_LEN);
  //cout << "finished getline" << endl;  
#endif //IGNTRANS_SERIAL

#ifdef IGNTRANS_SDS
  igntransport->clean();
  if (!igntransport->write(positionString)) return TRANS_ERROR;
  usleep(30000);
  if (!igntransport->read(GET_POS_LEN, positionString, 10000)) return TRANS_ERROR;
#endif //igntrans_sds

  //cout <<"trans_getposition is receiving: " << positionString << endl;
  //cout << "device type = " << positionString[DEVICE_TYPE] << endl;
  //cout << "position = " << positionString [RECEIVED_POSITION] << endl;
  // make sure we return the right device type
  if (positionString[DEVICE_TYPE] != TRANS) return TRANS_ERROR;

  // parse the response string
  if (positionString[RECEIVED_POSITION] == TRANS_PARK) transPosition = T_PARK;
  else if (positionString[RECEIVED_POSITION] == TRANS_REVERSE) transPosition = T_REVERSE;
  else if (positionString[RECEIVED_POSITION] == TRANS_NEUTRAL) transPosition = T_NEUTRAL;
  else if (positionString[RECEIVED_POSITION] == TRANS_DRIVE) transPosition = T_DRIVE;
  else return TRANS_ERROR;
  //cout << "got get_position: " << transPosition << endl;
  return transPosition;
}


/*****************************************************************************88
 * int ign_setposition(int position)
 ********************************************************************************/
int ign_setposition(int position) {
  char positionString[SET_POS_LEN+1];
  // setup the command string with the proper characters
  positionString[DEVICE_TYPE] = IGN;
  positionString[DEVICE_DIRECTION] = SEND;
  positionString[SEND_EOL_POS] = EOL;

  // verify that the requested command is actually valid
  if (position == I_OFF) {
    //positionString[DEVICE_DATA] = IGN_OFF;
    sprintf(positionString, "ISO\n\r");
  }
  else if (position == I_RUN) {
    //positionString[DEVICE_DATA] = IGN_RUN;
    sprintf(positionString, "ISR\n\r");
  }
  else if (position == I_START) {
    //positionString[DEVICE_DATA] = IGN_START;
    sprintf(positionString, "ISS\n\r");
  }
  else return ERROR;
  
  //write the data
#ifdef IGNTRANS_SERIAL
  if (!serial_port.write(positionString, SET_POS_LEN-1)) return ERROR;
  else return TRUE;
#endif //IGNTRANS_SERIAL

#ifdef IGNTRANS_SDS
  if (!igntransport->write(positionString)) return ERROR;
  else return TRUE;
#endif //igntrans_sds

  // to do: building in verification write from actuator to verify reception
}


/**************************************************************************
 * int ign_getposition(void)
 **************************************************************************/
int ign_getposition(void) {
  char positionString[GET_POS_LEN+2];
  char ignPosition;
  //  positionString[DEVICE_TYPE] = IGN;
  //positionString[DEVICE_DIRECTION] = RECEIVE;
  //positionString[RECEIVE_EOL_POS] = EOL;
  sprintf(positionString, "IR\n\r");
  
#ifdef IGNTRANS_SERIAL
  serial_port.clear();
  if (!serial_port.write(positionString, GET_POS_LEN)) return ERROR;
  if (!serial_port.read(positionString, GET_POS_LEN)) return ERROR;
#endif //IGNTRANS_SERIAL

#ifdef IGNTRANS_SDS
  if (!igntransport->write(positionString)) return ERROR;
  if (!igntransport->read(GET_POS_LEN, positionString, 10000)) return ERROR;
#endif //igntrans_sds

  if (positionString[DEVICE_TYPE] != IGN) return ERROR;
  if (positionString[RECEIVED_POSITION] == IGN_OFF) ignPosition = I_OFF;
  else if (positionString[RECEIVED_POSITION] == IGN_RUN) ignPosition = I_RUN;
  else if (positionString[RECEIVED_POSITION] == IGN_START) ignPosition = I_START;
  else return ERROR;
  return ignPosition;
}

  
/*****************************************************************************88
 * int led_setposition(itn device, int position)
 ********************************************************************************/
int led_setposition(int device, int position) {
  char positionString[SET_POS_LEN+1];
  
  // check which led we want to set
  if (device == LED1) positionString[DEVICE_TYPE] = LED_I;
  else if (device == LED2) positionString[DEVICE_TYPE] = LED_II;
  else if (device == LED3) positionString[DEVICE_TYPE] = LED_III;
  else if (device == LED4) positionString[DEVICE_TYPE] = LED_IV;
  else return ERROR;
  
  // set up the rest of the string ot be sent
  //positionString[DEVICE_DIRECTION] = SEND;
  //positionString[SEND_EOL_POS] = EOL;
  if (position == L_OFF) sprintf(positionString+1, "SO\n\r"); //positionString[DEVICE_DATA] = LED_OFF;
  else if (position == L_ON) sprintf(positionString+1, "SN\n\r"); //positionString[DEVICE_DATA] = LED_ON;
  else return ERROR;
  
  //write the data
#ifdef IGNTRANS_SERIAL
  if (!serial_port.write(positionString, SET_POS_LEN-1)) return ERROR;
  else return TRUE;
#endif //IGNTRANS_SERIAL

#ifdef IGNTRANS_SDS
  if (!igntransport->write(positionString)) return ERROR;
  else return TRUE;
#endif //igntrans_sds

  // to do: building in verification write from actuator to verify reception
}


/**************************************************************************
 * int led_getposition(void)
 **************************************************************************/
int led_getposition(int device) {
  char positionString[GET_POS_LEN];
  char ledPosition;
  if (device == LED1) positionString[DEVICE_TYPE] = LED_I;
  else if (device == LED2) positionString[DEVICE_TYPE] = LED_II;
  else if (device == LED3) positionString[DEVICE_TYPE] = LED_III;
  else if (device == LED4) positionString[DEVICE_TYPE] = LED_IV;
  else return ERROR;

  //positionString[DEVICE_DIRECTION] = RECEIVE;
  //positionString[RECEIVE_EOL_POS] = EOL;
  sprintf(positionString+1, "R\n\r");
#ifdef IGNTRANS_SERIAL
  if (!serial_port.write(positionString, GET_POS_LEN)) return ERROR;
  if (!serial_port.read(positionString, GET_POS_LEN)) return ERROR;
#endif //IGNTRANS_SERIAL

#ifdef IGNTRANS_SDS
  if (!igntransport->write(positionString)) return ERROR;
  if (!igntransport->read(GET_POS_LEN, positionString, 10000)) return ERROR;
#endif //igntrans_sds

  if (positionString[RECEIVED_POSITION] == LED_OFF) ledPosition = L_OFF;
  else if (positionString[RECEIVED_POSITION] == LED_ON) ledPosition = L_ON;
  else return ERROR;

  return ledPosition;
}
  
