/*!
 * \file UT_turnsig.cc
 * \brief Unit test for turn signals
 *
 * \author Richard M. Murray
 * \date 18 May 2007
 *
 * This is a simple unit test for turning the turn signals on and off.
 * It has been written so that the bottom half can be used in other
 * programs to talk to the turn signal module via adrive.
 *
 */

int setTurnSignal(double dir);
int getTurnSignal();

/*
 * Turn signal interface code 
 *
 * The code in this section illustrates how to talk to the turn
 * signals.  This code is written so that it can be cut and pasted
 * into another program.  It defines a simple class that handles the
 * required operations.
 *
 */

#include "gcmodule/GcInterface.hh"
#include "skynet/skynet.hh"
#include "gcinterfaces/AdriveCommand.hh"

class TurnSignalSender 
{
  GcModuleLogger* m_logger;
  GcPortHandler *m_turnHandler;
  AdriveCommand::Southface *m_adriveSF;
  int id;

public:
  TurnSignalSender(int);
  ~TurnSignalSender() {};
  int setTurnSignal(double);
  int getTurnSignal();
};

/*! Constructor */
TurnSignalSender::TurnSignalSender(int skynet_key)
{
  m_logger = new GcModuleLogger("TurnSignalSender");
  m_turnHandler = new GcPortHandler();
  m_adriveSF = AdriveCommand::
    generateSouthface(skynet_key, m_turnHandler, m_logger);
  id = 0;
}

/*! Set the turn signals in a given direction */
int TurnSignalSender::setTurnSignal(double dir)
{
  /* Generate the command to set the turn signals */
  AdriveDirective turnCommand;
  turnCommand.id = id++;
  turnCommand.command = SetPosition;
  turnCommand.actuator = TurnSignal;
  turnCommand.arg = dir;

  /* Send the directive to adrive */
  m_adriveSF->sendDirective(&turnCommand);
  m_turnHandler->pumpPorts();

  /* Wait for the return message */
  for (int i = 0; i < 100; ++i) {
    if (m_adriveSF->haveNewStatus()) {
      /* Get the response from adrive */
      AdriveResponse *turnResponse = m_adriveSF->getLatestStatus();

      /* Set return status based on whether we completed or not */
      return 
	turnResponse->status == GcInterfaceDirectiveStatus::COMPLETED ? 1 : 0;
    }

    /* Wait for a bit and then grab new inputs */
    DGCusleep(100000);		// MAGIC
    m_turnHandler->pumpPorts();
  }

  /* Didn't receive a response */
  return -1;
}

/*! Get the current turn signal direction */
int TurnSignalSender::getTurnSignal()
{
  /* Not implemented */
  return -1;
}

/*
 * Short main program to demonstrate the use of the class
 */

int main(int argc, char **argv)
{
  int status;
  int skynet_key = skynet_findkey(argc, argv);
  TurnSignalSender turnsignal(skynet_key);

  cout << "signalling left turn" << endl;
  status = turnsignal.setTurnSignal(-1); sleep(2);
  cout << "  returned " << status << endl;

  cout << "signalling right turn" << endl;
  status = turnsignal.setTurnSignal(1); sleep(2);
  cout << "  returned " << status << endl;

  cout << "signalling 0 turn" << endl;
  status = turnsignal.setTurnSignal(0); sleep(2);
  cout << "  returned " << status << endl;

  return 0;
}

