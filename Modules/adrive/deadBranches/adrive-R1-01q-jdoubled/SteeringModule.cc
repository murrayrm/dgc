/*!
 * \file SteeringModule.cc
 * \brief GcModule for controlling Alice's steering
 * 
 * \author Nok Wongpiromsarn and Richard Murray
 * \date 14 April 2007
 *
 * This is the GcModule that controls Alice's steering.  It receives
 * commands from the ActuatorInterface module (router) and sends them
 * to the steering motor controller.  The initial release assumes
 * correct operation and does minimal checking for errors.
 */

#include <assert.h> 
#include <sys/stat.h>
#include "dgcutils/DGCutils.hh"

#include "SteeringModule.hh"
#include "parker_steer.hh"
using namespace std;

/*
 * SteeringModule::SteeringModule
 *
 * The constructor initializes the GcModule interface, the steering
 * port and other class data.
 */

#ifdef GCMODULE_EVENT_BASED
SteeringModule::SteeringModule(int skynetKey,
			       ActuationInterface *actuationInterface,
			       int verbose_flag)
  : GcModule("Adrive_SteeringModule", &m_controlStatus, &m_mergedDirective,
	      FAST_SLEEP, FAST_SLEEP, true )
#else
SteeringModule::SteeringModule(int skynetKey,
			       ActuationInterface *actuationInterface,
			       int verbose_flag)
  : GcModule("Adrive_SteeringModule", &m_controlStatus, &m_mergedDirective,
	      FAST_SLEEP, FAST_SLEEP )
#endif
{
  verbose = verbose_flag;
  if (verbose > 5) cerr << "SteeringModule: verbose = " << verbose << endl;

  /*
   * GcModule initialization
   *
   * The first thing that we do is initialize all of the variables
   * that are required for communicating with other GcModules,
   * including the ActuationInterface.
   */

  /* Set up the log file if log level is greater than 0 */
  if (actuationInterface->options->log_level_arg > 0) {
    ostringstream oss;
    oss << actuationInterface->options->log_file_arg << "-steering";
    string logFileName = oss.str();

    /* This is how we use GcModuleLogger. First we tell it to create a log file.
       Then, set the log level of the module so when we use 
       gclog(n) below, the message will be logged if the log level is
       greater than or equal to n. */
    this->addLogfile(logFileName);
    this->setLogLevel(actuationInterface->options->log_level_arg);
  }

  /* Get the interface to the steerControl module */
  adriveSteerInterfaceNF = 
    AdriveSteerInterface::generateNorthface(skynetKey, this);

  adriveSteerInterfaceNF->setStaleThreshold(10);

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * SteeringModule initialization
   *
   * Initialize the member variables used by the control and arbitrate
   * functions.
   */

  /* Initialize the IDs so that we can tell when we get new commands */
  m_latestID = m_currentID = 0;

  /* Initialize the control state to keep from returning a response */
  m_controlStatus.status = ControlStatus::RUNNING;
  controlCount = 0;

  /* Initialize the threshold for engaging the steering rate limiter */
  STEER_SPEED_THRESHOLD = adrive->options->steer_speed_threshold_arg;

  /*
   * Hardware/simulation initailization
   *
   * Now that we have initalized the software interfaces, we
   * initialize the hardware interface.  This consists of opening up
   * the serial port so that we can talk to the motor controller and
   * then initializing the motor controller.  If we can't open the
   * serial port, we abort with an error.  It is possible for the
   * motor controller initialization to fail if steering is not
   * currently enabled in Alice.  If this fails, we mark the interface
   * as uninitialized and continue on.  This allows us to continue to
   * receive status and also to rest the steering module manually from
   * sparrow.
   *
   * If the steering interface is disabled (using the command line
   * flag), then we still try to open the port but we don't generate
   * abort if we can't do it.  This allows the steering status
   * information to still be run in the case that we are in Alice and
   * just don't want to use steering.  If this steering is disabled,
   * we set the inialized flag to true so that it appears to modules
   * that steering is being commanded.  This is useful for testing.
   *
   * If we are running in simulation mode, we don't bother trying to
   * talk to the hardware. 
   */

  if (adrive->options->simulate_given) {
    /* Simulation mode - no action required */
    if (verbose >= 5) cerr << "SteeringModule: simulation mode" << endl;
    initialized = 1;
    connected = 1;

  } else if (adrive->options->disable_steer_flag) {
    if (verbose >= 5) cerr << "SteeringModule: steering disabled" << endl;
    /* Steering has been disabled, so allow the error to pass */
    connected = 0;
    initialized = 1;

  } else {
    /* Open the port to the actuator */
    if (verbose >= 5) cerr << "SteeringModule: calling steer_open" << endl;
    if (steer_open(adrive->options->steer_port_arg, verbose) != TRUE) {
      /* Don't allow operation without being able to talk to steering */
      cerr << "SteeringModule: steer_open failed" << endl;
	exit(1);
    } else {
      /* Keep track of the fact that we are connected */
      if (verbose >= 6) cerr << "SteeringModule: steer_open done" << endl;
      connected = 1;
    }

    /* Attempt to initialize the motor controller */
    if (verbose >= 5) cerr << "SteeringModule: calling steer_init" << endl;
    if (steer_init() == TRUE) {
      if (verbose >= 6) cerr << "SteeringModule: steer_init done" << endl;
      initialized = 1;

    } else {
      /* We are connected to the port, but didn't initialize */
      cerr << "SteeringModule: steer_init failed" << endl;
      initialized = 0;
    }
  }

  /*
   * Status thread startup
   *
   * As part of the SteeringModule function, we start up a thread to
   * read the status of the actuator on a periodic basis.  This thread
   * fills in data in the SteeringModule class, so we have easy
   * access to this information.  
   */

  statusCount = 0;
  statusSleepTime = STATUS_SLEEP;
  DGCstartMemberFunctionThread(this, &SteeringModule::status);

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&actuatorMutex);

}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void SteeringModule::initSparrow(DD_IDENT *tbl, int STEER_LABEL)
{
  if (verbose >= 8) cerr << "SteeringModule::initSparrow called" << endl;
  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the motor controller (steering disabled) */
      dd_setcolor_tbl(STEER_LABEL, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(STEER_LABEL, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(STEER_LABEL, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("steer.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("steer.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("steer.command", &commandAngle, tbl);
    dd_rebind_tbl("steer.position", &currentAngle, tbl);
  }
}

/*
 * SteeringModule::arbitrate
 *
 * Arbitrator for SteeringModule.  The artibiter receives steering
 * commands from the actuationInterface and sends them to control.  At
 * present, the only directives that are recognized are new steering
 * commands, which replace old steering commands.
 *
 */

void SteeringModule::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  SteerControlStatus *controlResponse =
    dynamic_cast<SteerControlStatus *>(cs);

  SteerMergedDirective *mergedDirective =
    dynamic_cast<SteerMergedDirective *>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  AdriveResponse response;

  switch (controlResponse->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::COMPLETED;
    adriveSteerInterfaceNF->sendResponse( &response, response.id );
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.status = GcInterfaceDirectiveStatus::FAILED;
    response.reason = controlResponse->reason;
    adriveSteerInterfaceNF->sendResponse( &response, response.id );
    gclog(1) << response.toString() << endl;
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */

  /* Check to see if we have rec'd a new directive from actuation interface */
  if (adriveSteerInterfaceNF->haveNewDirective()) {
    /* Keep track of the time when we receive commands */
    DGCgettime(command_time);

    /* Get the latest directive instead of the oldest unread one, so
       we add the second parameter to getNewDirective. */
    AdriveDirective newDirective;
    adriveSteerInterfaceNF->getNewDirective( &newDirective, true );
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    gclog(2) << t << "\tReceived steering command: id = " << newDirective.id << endl;


    /* !! Make sure that the directive has a unique ID !! */
    /* Not implemented */

    /* Save the ID to pass back when we are done */
    mergedDirective->parent_id = newDirective.id;

    switch (newDirective.command) {
    case SetPosition:
      /* Make sure we are properly initalized */
      if (!initialized) {
	/* Respond with a failure */
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::FAILED;
	response.reason = NotInitialized;
	adriveSteerInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Check the range of the command */
      if (newDirective.arg < -1 || newDirective.arg > 1) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = OutOfRange;
	adriveSteerInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Generate the directive to steer the car */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = newDirective.arg;
      mergedDirective->id = ++m_latestID;
      break;

    case Reset:
      /* Note: ActuationInterface will check to make sure we are reset */
      mergedDirective->command = Reset;
      mergedDirective->id = ++m_latestID;
      break;

    default:
      if (verbose) 
	cerr << "SteeringModule: unrecognized directive: "
	     << newDirective.command << endl;
      response.id = newDirective.id;
      response.status = GcInterfaceDirectiveStatus::REJECTED;
      response.reason = OutOfRange;
      adriveSteerInterfaceNF->sendResponse( &response, response.id );
      break;
    }
  }
}

/*
 * SteeringModule::control
 *
 * Control for SteeringModule.  The artibiter receives merged
 * directives the SteeringModule arbitrator.  For steering, this is
 * just the latest steering command, which is sent on to the hardware.
 * The m_currentID tag is used to keep track of which directive we are
 * currently excuting.
 *
 */

void SteeringModule::control(ControlStatus *cs, MergedDirective *md)
{
  SteerControlStatus* controlResponse =
    dynamic_cast<SteerControlStatus *>(cs);
  SteerMergedDirective* mergedDirective =
    dynamic_cast<SteerMergedDirective *>(md);

  /* First check to see if we have anything new to do */
  if (mergedDirective->id == m_currentID) {
    /* Tell the arbiter we are still running */
    controlResponse->status = ControlStatus::RUNNING;
    return;
  }

  /* New directive to process: update IDs and counters (for sparrow) */
  m_currentID = mergedDirective->id;
  commandAngle = mergedDirective->arg;
  ++controlCount;

  double steer_command = mergedDirective->arg;
  switch (mergedDirective->command) {
    int status;

  case SetPosition:
    /* Limit the amount of motion based on the speed of the vehicle */
    if (adrive->state.m_obdiistatus && 
	adrive->state.m_VehicleWheelSpeed < STEER_SPEED_THRESHOLD) {

      /* Limit the steering angle based on the speed */
      steer_command = adrive->state.m_steerpos +
	(steer_command - adrive->state.m_steerpos) *
	adrive->state.m_VehicleWheelSpeed / STEER_SPEED_THRESHOLD;
    }

    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_given) {
      drivecmd_t my_command;
      my_command.my_actuator = steer;
      my_command.my_command_type = set_position;
      my_command.number_arg = steer_command;
      adrive->m_skynet->send_msg(adrive->m_drivesocket, &my_command, 
				 sizeof(my_command), 0);

    } else {
      /* Send the command to the steering controller */
      pthread_mutex_lock(&actuatorMutex);
      steer_heading(steer_command);
      pthread_mutex_unlock(&actuatorMutex);
    }

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  case Reset:
    /* Initialize the steering actuator (if connected) */
    pthread_mutex_lock(&actuatorMutex);
    status = adrive->options->simulate_given || 
      !connected || steer_init();
    pthread_mutex_unlock(&actuatorMutex);

    /* Make sure that it worked */
    if (status) {
      initialized = 1;
      controlResponse->status = ControlStatus::STOPPED;

    } else {
      initialized = 0;
      controlResponse->status = ControlStatus::FAILED;
      controlResponse->reason = InitializationError;
    }      
    controlResponse->id = m_currentID;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;

  default:
    /* Shouldn't ever get here */
    controlResponse->status = ControlStatus::FAILED;
    controlResponse->reason = OutOfRange;
    break;
  }
}

/*
 * SteeringModule::status
 *
 * This function is called as a thread and is used to read the status
 * of the actuator.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void SteeringModule::status()
{
  while (1) {
    if (adrive->options->simulate_given) {
      /* In simulation mode, read state from asim */
      currentAngle = adrive->asim_state.m_steerpos;

    } else {
      if (!connected) {
	/* Steering is currently disabled, so just return zero */
	currentAngle = 0;

      } else {
	/* Read the current status */
	pthread_mutex_lock(&actuatorMutex);
	double retval = steer_getheading();
	pthread_mutex_unlock(&actuatorMutex);

	/* Check the status and set state appropriately */
#       warning incorrect error check (see parker_steer)
	if (retval == -2) {
	  /* Error condition */
	  if (verbose)
	    cerr << "SteeringModule::error reading steering position" << endl;
	  gclog(1) << "SteeringModule::status error reading steering position"
		   << endl;
	  currentAngle = 0;
	} else {
	  currentAngle = retval;
	}

      }
    }

    /* Now fill up the actuatorState struct with current state */
    adrive->state.m_steerstatus = initialized && connected;
    adrive->state.m_steerpos = currentAngle;
    adrive->state.m_steercmd = commandAngle;

    /* Now that we have the state, check to see if there are any problems */
    /* TODO: implement something here */

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}
