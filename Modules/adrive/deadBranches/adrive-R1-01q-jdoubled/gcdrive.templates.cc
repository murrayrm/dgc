

#ifdef OMIT_TEMPLATE_DEFS
#undef OMIT_TEMPLATE_DEFS
#endif
#include "gcmodule/GcInterface.hh"

#include "gcinterfaces/AdriveCommand.hh"
#include "ActuationInterface.hh"

/*
 *
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
                    GCM_IN_PROCESS_DIRECTIVE, SNadrive> AdriveSteerInterface;
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
                    GCM_IN_PROCESS_DIRECTIVE, SNadrive_commander> AdriveAccelInterface;
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
                    GCM_IN_PROCESS_DIRECTIVE, SNadrivelogger> AdriveTransInterface;
typedef GcInterface<AdriveDirective, AdriveResponse, GCM_IN_PROCESS_DIRECTIVE,
                    GCM_IN_PROCESS_DIRECTIVE, SNProcMon> AdriveTurnInterface;
                    */


/// Do not call the following function -- it is here only to cause the 
/// compiler to generate the template code that would support this function
void instantiateTemplateCode() {
  int skynet_key = 0;
  AdriveTurnInterface::generateNorthface(skynet_key, NULL);
  AdriveTransInterface::generateNorthface(skynet_key, NULL);
  AdriveSteerInterface::generateNorthface(skynet_key, NULL);
  AdriveAccelInterface::generateNorthface(skynet_key, NULL);
  AdriveCommand::generateNorthface(skynet_key, NULL );
}

