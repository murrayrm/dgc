/*!
 * \file ActuationInterface.cc
 * \brief Interface for receiving adrive directives
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 * 
 * This file receives directives for adrives using the GcInterface
 * class and sends them off to the appropriate actuators.
 */

#include <assert.h>
#include <sys/stat.h>
#include "ActuationInterface.hh"
using namespace std;

/* Constructor */
ActuationInterface::ActuationInterface(int skynet_key, 
				       struct gengetopt_args_info *cmdline) 
{
  /* Save the command line options that we were passed */
  options = cmdline;

  /* Get the GcPortHandler */
  m_adriveCommandHandler = new GcPortHandler();
  m_steerHandler = new GcPortHandler();
  m_accelHandler = new GcPortHandler();
  m_transHandler = new GcPortHandler();

  /* Get the GcModuleLogger */
  m_logger = NULL;
  if (options->log_level_arg > 0) {
    m_logger = new GcModuleLogger("ActuationInterface");
    ostringstream oss;
    struct stat st;
    oss << options->log_file_arg << "-actuationInterface.log";
    string suffix = "";
    string logFileName = oss.str();
    /* if it exists already, append .1, .2, .3 ... */
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
  
    m_logger->addLogfile(logFileName);
    m_logger->setLogLevel(options->log_level_arg);
  }

  /* Get the north face to trajfollower */
  m_adriveCommandNF = AdriveCommand::
    generateNorthface(skynet_key, m_adriveCommandHandler, m_logger);
  
  /* Get the the south face to the actuators */
  m_steer = AdriveSteerInterface::
    generateSouthface(skynet_key, m_steerHandler, m_logger); 
  m_accel = AdriveAccelInterface::
    generateSouthface(skynet_key, m_accelHandler, m_logger);
  m_trans = AdriveTransInterface::
    generateSouthface(skynet_key, m_transHandler, m_logger);

  /* Limit the number of directives and responses stored in GcInterface */
  m_adriveCommandNF->setStaleThreshold(10);
  m_steer->setStaleThreshold(10);
  m_accel->setStaleThreshold(10);
  m_trans->setStaleThreshold(10);

  /* 
   * Start up the status loop, which reads/sends ActuatorState 
   *
   */
  m_skynet = new skynet(SNadrive, skynet_key);

  /* Check to see if we should start up the simulator */
  asim_status = 0;
  if (cmdline->simulate_given) {
    /* In simulation, we communicate with asim */
    m_drivesocket = m_skynet->get_send_sock(SNdrivecmd);
    m_asimsocket = m_skynet->listen(SNasimActuatorState, ALLMODULES);
    DGCstartMemberFunctionThread(this, 
				 &ActuationInterface::getSimActuatorState);
  }

  /* Initialize the thread that sends out state information */
  m_statesocket = m_skynet->get_send_sock(SNactuatorstate);
  statusCount = 0;
  statusSleepTime = 25000;
  DGCstartMemberFunctionThread(this, &ActuationInterface::status);

  /* Mutex for controlling access to actuator state */
  DGCcreateMutex(&actuatorStateMutex);

  /* Initialize the estop state */
  state.m_astoppos = EstopPause;
  state.m_estoppos = EstopPause;
  actuation_state = Paused;
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void ActuationInterface::initSparrow(DD_IDENT *tbl, int ACTSTATE_LABEL)
{
  if (!options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (options->simulate_given) {
      /* In simulation modue */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(ACTSTATE_LABEL, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("adrive.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("estop.estoppos", &state.m_estoppos, tbl);
    dd_rebind_tbl("actuation_state", &actuation_state, tbl);
  }
}

/*
 * ActuationInterface::status
 *
 * This function is called as a thread and is used to read the status
 * from the individual actuators and broadcast this information across
 * the SNactuationstate channel.
 *
 */

void ActuationInterface::status()
{
  /* Broadcast the current state */
  while (1) {
    /* Fill in any part of the state that we are responsible for */
    state.m_estoppos = 
      (actuation_state == Running) ? EstopRun :
      (actuation_state == Disabled) ? EstopDisable : 
      EstopPause;

    /* Broadcast the state out to the world */
    if (m_skynet->send_msg(m_statesocket, &state,
			   sizeof(state), 0) != sizeof(state)) {
      cerr << "ActuationInterface::status send_msg error" << endl;
    }

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

void ActuationInterface::arbitrate()
{
  unsigned long long actuation_resume_start;

  while (true) {
    /*
     * Determine the state of the actuation interface
     *
     * The state of the interface is determined by the estop status.
     */

    /* Check to see if we have received a Disable command (locking) */
    if (actuation_state == Disabled || 
	(state.m_estopstatus && state.m_dstoppos == EstopDisable)) {
      if (actuation_state != Disabled) {
	/* If we are switching into this state, issue a brake command */
	sendPauseCommand(Pause);
      }

      /* Set the state to disabled (and stay there) */
      actuation_state = Disabled;
    } 

    /* Check to see if we have received a Pause command.  We can get
       here from either Running, Paused or Resuming. */
    else if (state.m_estopstatus && state.m_dstoppos == EstopPause) {
      /* If we are switching into this state, issue a brake command */
      if (actuation_state == Running) {
	sendPauseCommand(Pause);
      }

      /* Save the state of the system */
      actuation_state = Paused;
    }

    /* See if we are coming out of a pause mode */
    else if (state.m_estopstatus && actuation_state == Paused &&
	     state.m_dstoppos == EstopRun) {
      /* Set up a timer to keep track of when it is OK to come out of pause */
      actuation_resume_start = DGCgettime();
      actuation_state = Resuming;
    }

    /* See if we are waiting to come out of pause mode */
    else if (actuation_state == Resuming) {
      unsigned long long time_pausing = DGCgettime() - actuation_resume_start;
      if (DGCtimetosec(time_pausing) > 5) {
	sendPauseCommand(ReleasePause);
	actuation_state = Running;
      }
    }

    /*
     * At this point the actuation_state is set appropriately and we
     * can decide what to do.  Here are the options:
     *
     *  - Running: send through all commands
     *  - Paused: only send through steering commands; reject others
     *  - Resuming: same as pause
     *  - Disabled: reject all commands
     *
     */

    /* Call pumpPorts to receive all the messages */
    m_adriveCommandHandler->pumpPorts();
    m_steerHandler->pumpPorts();
    m_accelHandler->pumpPorts();
    m_transHandler->pumpPorts();

    /* Pass on all the received directives to apropriate actuator. Put
       this in a while loop to make sure that the router can keep up
       with trajfollower. */
    while (m_adriveCommandNF->haveNewDirective()) {
      m_adriveCommandNF->getNewDirective( &m_adriveDirective );

      /* If we are disabled, reject all commands */
      if (actuation_state == Disabled) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehiclePaused;
	m_adriveCommandNF->sendResponse(&response, response.id);
	continue;
      }

      /* If we are paused, only accept steering commands */
      if ((actuation_state == Paused || actuation_state == Resuming) &&
	  m_adriveDirective.actuator != Steering) {
	AdriveResponse response;
	response.id = m_adriveDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = VehiclePaused;
	m_adriveCommandNF->sendResponse(&response, response.id);
	continue;
      }

      /* Otherwise, we can process the command we are sent */
      switch (m_adriveDirective.actuator) {
      case Steering:
	m_steer->sendDirective(&m_adriveDirective);
	break;

      case Acceleration:
	m_accel->sendDirective(&m_adriveDirective);
	break;

      case Transmission:
	m_trans->sendDirective(&m_adriveDirective);
	break;

      default:
	cerr << "ActuationInterface: received command for unknown actuator: "
	     << m_adriveDirective.actuator << endl;
      }
    }

    /* Check for return status message and pass them back to trajFollower */
    if (m_steer->haveNewStatus()) {
      AdriveResponse *steerResponse = m_steer->getLatestStatus();
      m_adriveCommandNF->sendResponse(steerResponse, steerResponse->id);
    }

    if (m_accel->haveNewStatus()) {
      AdriveResponse *accelResponse = m_accel->getLatestStatus();
      m_adriveCommandNF->sendResponse(accelResponse, accelResponse->id);
    }

    if (m_trans->haveNewStatus()) {
      AdriveResponse *transResponse = m_trans->getLatestStatus();
      m_adriveCommandNF->sendResponse(transResponse, transResponse->id);
    }

    /* Sleep for a short while (remember this is in the control loop) */
    usleep(1000);
  }
}

/*
 * ActuationInterface::getSimActuatorState
 *
 * This thread reads the simulated actuator state from asim.  This
 * information is stored so that it can be accessed by the individual
 * actuators.
 *
 */

void ActuationInterface::getSimActuatorState()
{
  /* Read actuator state from skynet */
  while (1) {
#   warning missing mutex locking for reading simulation state
    if (m_skynet->get_msg(m_asimsocket, &asim_state, sizeof(state)) 
	!= sizeof(state)) {
      cerr << "ActuationInterface::status get_msg error" << endl;
    } else {
      /* Let everyone know that they can grab the state now */
      asim_status = 1;
    }
  }
}

/*! Send pause/release to acceleration module */
void ActuationInterface::sendPauseCommand(AdriveCommandType cmd)
{
  static int estop_id = 0x8000;

  /* Make sure we got passed something sensible */
  assert(cmd == Pause || cmd == ReleasePause);

  AdriveDirective pauseCommand;
  pauseCommand.id = estop_id++;
  pauseCommand.command = cmd;
  pauseCommand.actuator = Acceleration;

  m_accel->sendDirective(&pauseCommand);
  m_accelHandler->pumpPorts();

  /* Wait until we get a response and read it */
  int i;
  for (i = 0; i < 20; ++i) {
    if (m_accel->haveNewStatus()) { 
      AdriveResponse *response = m_accel->getLatestStatus();
      assert(response != NULL);

      /* Make sure this response was intended for us */
      if (response->id != pauseCommand.id) {
	/* Response to someone else; pass back to sender */
	m_adriveCommandNF->sendResponse(response, response->id);
      } else
	/* This was our acknowledgement; break out of loop */
	break;
    } else {
      usleep(100000);
      m_accelHandler->pumpPorts();
    }
  }

  /* Make sure we received a response */
  if (i == 20) {
    cerr << "ActuationInterface: didn't receive response to "
	 << (cmd == Pause ? "pause" : "release") << endl;
  }
}

