/*! This is the header file to allow other functions to call skynet_main.  
 * Which is the skynet thread for adrive.*/

#ifndef ASKYNET_MONITER_H
#define ASKYNET_MONITER_H

#include "AdriveEventLogger.hh"
void init_vehicle_struct(vehicle_t & new_vehicle);

/* This is the struct to pass everything associated with adrive to a new thread
 */ 

struct skynet_main_args
{
	struct vehicle_t *pVehicle;
	int* pRun_skynet_thread;
	void* pSkynet;
};

/* The main skynet function prototype. */
void* skynet_main(void *arg);


#endif // ASKYNET_MONITER_H
