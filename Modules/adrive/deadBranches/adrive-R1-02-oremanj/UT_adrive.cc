/*!
 * \file UT_adrive.cc 
 * \brief Adrive unit test
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 *
 * \ingroup unittest
 *
 * This program serves as a basic unit test for the adrive module.  It
 * works by sending commands to adrive through the GcModule interface
 * and verifying that the appropriate response was received.  This
 * test is designed to be used with asim.
 */

#include <iostream>
#include <assert.h>
using namespace std;

#include "skynet/skynet.hh"
#include "gcinterfaces/AdriveCommand.hh"

/* 
 * Create the testModule class for use with GcModule 
 * 
 * This class is not actually used since this is just a test function.
 * However, in order to talk to adrive we need to have a GcModule, so
 * this is it.  Everything is inlined here since we don't actually
 * require any functionality.
 */
class TestModule : public GcModule
{
private:
  ControlStatus m_testStatus;		// unused
  MergedDirective m_testDirective;	// unused

public:
  TestModule(int skynet_key) :
    GcModule("testModule", &m_testStatus, &m_testDirective, 10000, 10000)
  {}

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *) {};
  void control(ControlStatus *, MergedDirective *) {};
};

static char *actuator_names[] = 
  {"steer", "accel", "trans", "ign", "turn"};
static char *command_names[] = 
  {"setpos", "reset", "pause", "release", "disable"};
static char *failure_names[] =
  {"noinit", "rangeerr", "paused", "commerr", "initerr"};

AdriveCommand::Southface *adriveInterfaceSF = NULL;

/* Utility function for sending and checking commands */
void send_and_check(AdriveActuator actuator,
		    AdriveCommandType command, double arg)
{
  static int id = 0;

  AdriveDirective adriveCommand;
  adriveCommand.id = id++;
  adriveCommand.actuator = actuator;
  adriveCommand.command = command;
  adriveCommand.arg = arg;

  /* Let the user know what we are doing */
  cerr << "Sending " << command_names[command] << " command" 
       << " to " << actuator_names[actuator] << " actuator";
  if (command == SetPosition) cerr << ": " << arg;
  cerr << endl;

  /* Send the command */
  adriveInterfaceSF->sendDirective(&adriveCommand);
  usleep(30000);

  /* Wait for the response */
  cerr << "  Response: ";
  while (!adriveInterfaceSF->haveNewStatus()) {
    cerr << "."; 
    fflush(stderr);
    sleep(1);
  }

  /* Get the response */
  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  assert(response != NULL);
  assert(response->id == adriveCommand.id);

  /* Display the response */
  switch (response->status) {
  case GcInterfaceDirectiveStatus::COMPLETED:
    cerr << "complete" << endl;
    break;
   
  case GcInterfaceDirectiveStatus::ACCEPTED:
    cerr << "accepted" << endl;
    break;
   
  case GcInterfaceDirectiveStatus::REJECTED:
    cerr << "rejected (" << failure_names[response->reason] << ")" << endl;
    break;

  case GcInterfaceDirectiveStatus::FAILED:
    cerr << "failed (" << failure_names[response->reason] << ")" << endl;
    break;
  }
}

int main (int argc, char **argv)
{
  cout << "Unit test: " << argv[0] << endl;
  int skynet_key = skynet_findkey(argc, argv);

  /* Create a GcModule for the test program */
  TestModule *testModule = new TestModule(skynet_key);
  testModule->Start();

  /* Set up the interface to adrive */
  adriveInterfaceSF = AdriveCommand::generateSouthface(skynet_key, testModule);

  /* Check steering commands */
  cerr << "--- Steering ---" << endl;
  send_and_check(Steering, SetPosition, 0.05);
  send_and_check(Steering, SetPosition, 2);
  send_and_check(Steering, SetPosition, -0.05);
  cerr << endl;

  /* Check acceleration commands */
  cerr << "--- Acceleration ---" << endl;
  send_and_check(Acceleration, SetPosition, 0.05);
  send_and_check(Acceleration, SetPosition, -1.2);
  send_and_check(Acceleration, SetPosition, -0.05);
  cerr << endl;

  return 0;
}
