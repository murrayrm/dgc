/* Declare sparrow callbacks as standard C functions */
extern "C" {
  extern int adrive_setSteering_cb(long arg);
  extern void adrive_initSparrow(int);
}
