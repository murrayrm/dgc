///////////////////////////////////////////////////////////////
//  OBDIIDriver.h
//   
//////////////////////////////////////////////////////////////

#ifndef OBDIIDRIVER_HPP
#define OBDIIDRIVER_HPP

#define OBDII_SERIAL

#ifndef OBDII_SERIAL
#define OBDII_SDS
#endif



//#include "SerialDevice.hpp"

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include <string>
#include <cassert>
#include <fstream>
#include <string.h>

#ifdef OBDII_SERIAL
#include <sstream>
#include <SerialStream.h>
using namespace LibSerial;
#else// #ifdef OBDII_SDS
#include "SDSPort.h"
#endif // OBDII_SDS
using namespace std;

#define DEBUG_DRIVER false
#define READ_TIMEOUT_USEC 500000
#define HALF_READSIZE  14
#define FULL_READSIZE  28

/////////////////////////////////////////////////////////////
// OBDIIDriver CLASS DEFINITION
/////////////////////////////////////////////////////////////

class OBDIIDriver {
public:
  // Variables 
 
  //Functions
  OBDIIDriver() ;
  ~OBDIIDriver()  {;}
  int connect(int port) ;
  int disconnect() ;
  //int init();
	void setdebug(int val) {_debug_flag = val;}




	//============================================================
	//
	// Data is returned by setting 'val' if parsing is successful
	//
	// Function returns are 
	// -1 : port or parsing error
	// 0 : noop returned - engine off
	// 1 : successfully parsed value
	//
	//============================================================

  
  int getRPM(double & val);
  int getTimeSinceEngineStart(int & val);//seconds
  int getVehicleSpeed(double & val);//mps
  int getEngineCoolantTemp(double & val);//degrees F
  int getTorque(double & val);//footpounds
  int getGlowPlugLampTime(int & val);//seconds
  int getThrottlePosition(double & val);//%
  int getCurrentGearRatio(double & val);
  int getBatteryVoltage(double & val);//Volts
  int GlowPlugLightOn(int & val);//UNTESTED
  int getCurrentGear(int & val);//1=first, 2=second, etc.  This is only useful when getTransmissionPosition() returns 'D'
  int getTransmissionPosition(char & val);//P=park, R=reverse, N=neutral, D=drive
  int getTorqueConvClutchDutyCycle(double & val);//% INOP
  int getTorqueConvControlState(int & val);//0=TCC not controlled by OSC, 1=TCC unlocked, 2=TCC locked UNTESTED
  
	//	void clearDTCs();//INOP
	//  void requestDTCs();//INOP


private:

	//Functions

	int isnull(const string& instring);
	int isnoop(const string& instring);

  int readport(string& instring);
	int writeport(const string& outstring);
	// {string out = _serialport.readport();return out;};

  int sendcommand(const string& commandstring, string & returnstring) ;
  void printstring(const string& s);
  unsigned char checksum(const string & s); 
  int parseresponse(const string & response, int & val,int valtype) ;
	// char parseSignedResponse(const string & response);
	// string parsebitmappedresponse(const string & response, int expectedlength);
	//  int parsetwobytesignedresponse(const string & response);
  string buildcommand(char command) ;
   string buildtwobytecommand(char commandMSB, char commandLSB);
  //string buildDTCcommand(char mode);

	//	int getAPP_D();
	//int getAPP_E();
	//int getAPP_F();

  // Variables
	int _debug_flag;
	int _portnum;
#ifdef OBDII_SERIAL
	SerialStream _serialport;
#else //SDS
	SDSPort* _sdsport;
#endif

};

#endif
