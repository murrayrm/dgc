/*!
 * \file UT_pause.cc 
 * \brief Pause unit test
 *
 * \author Richard M. Murray
 * \date 14 April 2007
 *
 * \ingroup unittest
 *
 * This program tests the pause functionality of adrive.
 */

#include <iostream>
#include <assert.h>
using namespace std;

#include "skynet/skynet.hh"
#include "gcinterfaces/AdriveCommand.hh"

#define _COMPLETED GcInterfaceDirectiveStatus::COMPLETED
#define _FAILED GcInterfaceDirectiveStatus::FAILED
#define _REJECTED GcInterfaceDirectiveStatus::REJECTED


/* 
 * Create the testModule class for use with GcModule 
 * 
 * This class is not actually used since this is just a test function.
 * However, in order to talk to adrive we need to have a GcModule, so
 * this is it.  Everything is inlined here since we don't actually
 * require any functionality.
 */
class TestModule : public GcModule
{
private:
  ControlStatus m_testStatus;		// unused
  MergedDirective m_testDirective;	// unused

public:
  TestModule(int skynet_key) :
    GcModule("testModule", &m_testStatus, &m_testDirective, 100000, 100000)
  {}

  /* Arbitrate and control functions (unused) */
  void arbitrate(ControlStatus *, MergedDirective *) {};
  void control(ControlStatus *, MergedDirective *) {};
};

int send_command(AdriveCommand::Southface *adriveInterfaceSF, 
	     AdriveDirective &adriveCommand,
	     GcInterfaceDirectiveStatus::Status finalStatus)
{
  adriveInterfaceSF->sendDirective(&adriveCommand);

  cerr << "Waiting for response: ";
  while (!adriveInterfaceSF->haveNewStatus()) {
    cerr << "."; 
    fflush(stderr);
    sleep(1);
  }

  AdriveResponse *response = adriveInterfaceSF->getLatestStatus();
  assert(response != NULL);
  assert(response->status == finalStatus);
  assert(response->id == adriveCommand.id);
  cerr << "Received " << response->status << endl;

  return 1;
}

int main (int argc, char **argv)
{
  cout << "Unit test: " << argv[0] << endl;
  int skynet_key = skynet_findkey(argc, argv);

  /* Create a GcModule for the test program */
  TestModule *testModule = new TestModule(skynet_key);
  testModule->Start();

  /* Set up the interface to adrive */
  AdriveCommand::Southface *adriveInterfaceSF =
    AdriveCommand::generateSouthface(skynet_key, testModule);

  /* Send an accel command to adrive */
  AdriveDirective adriveCommand;
  adriveCommand.id = 7;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = 0.2;
  cerr << "Sending accel command: " << adriveCommand.arg << endl;
  send_command(adriveInterfaceSF, adriveCommand, _COMPLETED);

  /* Now send a pause command */
  adriveCommand.id++;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = Pause;
  cerr << "Sending pause command" << endl;
  send_command(adriveInterfaceSF, adriveCommand, _COMPLETED);

  /* Try to accelerate */
  adriveCommand.id++;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = SetPosition;
  cerr << "Sending accel command: " << adriveCommand.arg << endl;
  send_command(adriveInterfaceSF, adriveCommand, _REJECTED);

  /* Try to steer */
  adriveCommand.id++;
  adriveCommand.actuator = Steering;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = 0.5;
  cerr << "Sending steer command: " << adriveCommand.arg << endl;
  send_command(adriveInterfaceSF, adriveCommand, _COMPLETED);

  /* Remove the pause condition */
  adriveCommand.id++;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = ReleasePause;
  cerr << "Removing pause condition " << endl;
  send_command(adriveInterfaceSF, adriveCommand, _COMPLETED);

  /* Now try to accelerate */
  adriveCommand.id++;
  adriveCommand.actuator = Acceleration;
  adriveCommand.command = SetPosition;
  adriveCommand.arg = 0.1;
  send_command(adriveInterfaceSF, adriveCommand, _COMPLETED);

  return 0;
}
