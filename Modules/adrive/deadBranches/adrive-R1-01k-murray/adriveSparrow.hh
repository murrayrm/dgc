/* Declare sparrow callbacks as standard C functions */
extern "C" {
  extern int adrive_setSteering_cb(long arg);
  extern int adrive_setBrake_cb(long arg);
  extern int adrive_setThrottle_cb(long arg);
  extern int adrive_setTrans_cb(long arg);
  extern int adrive_setTurn_cb(long arg);
  extern void adrive_initSparrow(int);

  extern int adrive_resetSteering_cb(long arg);
  extern int adrive_resetAcceleration_cb(long arg);
  extern int adrive_resetTransmission_cb(long arg);
  extern int adrive_resetActuators_cb(long arg);
}
