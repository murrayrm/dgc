/*!
 * \file TurnSignalModule.cc
 * \brief GcModule for controlling Alice's turn signals
 * 
 * \author Richard Murray
 * \date 18 May 2007
 *
 * This is the GcModule that controls Alice's turn signals.  It receives
 * commands from the ActuatorInterface module (router) and sends them
 * to the turn signal controller.  The initial release assumes
 * correct operation and does minimal checking for errors.
 */

#include <assert.h> 
#include <sys/stat.h>
#include "dgcutils/DGCutils.hh"

#include "TurnSignalModule.hh"

using namespace std;

/*
 * TurnSignalModule::TurnSignalModule
 *
 * The constructor initializes the GcModule interface, the turn signal
 * port and other class data.
 */

TurnSignalModule::TurnSignalModule(int skynetKey,
				       ActuationInterface *actuationInterface,
				       int verbose_flag)
  : GcModule("Adrive_TurnSignalModule", &m_controlStatus, &m_mergedDirective,
	      SLOW_SLEEP, SLOW_SLEEP)
{
  /* Set up the log file if log level is greater than 0 */
  if (actuationInterface->options->log_level_arg > 0) {
    ostringstream oss;
    oss << actuationInterface->options->log_file_arg << "-turnsignal";
    string logFileName = oss.str();

    /* This is how we use GcModuleLogger. First we tell it to create a log file.
       Then, set the log level of the module so when we use 
       gclog(n) below, the message will be logged if the log level is
       greater than or equal to n. */
    this->addLogfile(logFileName);
    this->setLogLevel(actuationInterface->options->log_level_arg);
  }

  verbose = verbose_flag;
  if (verbose > 8) cerr << "TurnSignalModule: verbose = " << verbose << endl;

  /*
   * GcModule initialization
   *
   * The first thing that we do is initialize all of the variables
   * that are required for communicating with other GcModules,
   * including the ActuationInterface.
   */

  /* Get the interface to the transControl module */
  adriveTurnInterfaceNF = 
    AdriveTurnInterface::generateNorthface(skynetKey, this);
  adriveTurnInterfaceNF->setStaleThreshold(10);

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * TurnSignalModule initialization
   *
   * Initialize the member variables used by the control and arbitrate
   * functions.
   */

  /* Initialize the IDs so that we can tell when we get new commands */
  m_latestID = m_currentID = 0;

  /* Initialize the control state to keep from returning a response */
  m_controlStatus.status = ControlStatus::RUNNING;
  controlCount = 0;

  /*
   * Hardware/simulation initailization
   *
   * Now that we have initalized the software interfaces, we
   * initialize the hardware interface.  This consists of opening up
   * the serial port so that we can talk to the motor controller and
   * then initializing the motor controller.  If we can't open the
   * serial port, we abort with an error.  It is possible for the
   * motor controller initialization to fail if turn signal is not
   * currently enabled in Alice.  If this fails, we mark the interface
   * as uninitialized and continue on.  This allows us to continue to
   * receive status and also to rest the turn signal module manually from
   * sparrow.
   *
   * If the turn signal interface is disabled (using the command line
   * flag), then we still try to open the port but we don't generate
   * abort if we can't do it.  This allows the turn signal status
   * information to still be run in the case that we are in Alice and
   * just don't want to use turn signal.  If this turn signal is disabled,
   * we set the inialized flag to true so that it appears to modules
   * that turn signal is being commanded.  This is useful for testing.
   *
   * If we are running in simulation mode, we don't bother trying to
   * talk to the hardware. 
   */

  if (adrive->options->simulate_given) {
    /* Simulation mode - no action required */
    initialized = 1;
    connected = 1;

  } else if (adrive->options->disable_turnsig_flag) {
    /* Interface is disabled; don't bother trying to talk to hardware */
    connected = 0;
    initialized = 1;

  } else {
    turnsignal = new Turnsignal(verbose);
    if (verbose) cerr << "TurnSignalModule: opening igntrans" << endl;
    if (turnsignal->ts_open(adrive->options->turnsig_port_arg) < 0) {
      /* Allow operation, but warn the user someting is wrong */
      cerr << "TurnSignalModule: ts_open failed" << endl;
      connected = 0;
      initialized = 0;
    } else if (turnsignal->init() < 0) {
      /* We connected, but couldn't initialize */
      connected = 1;
      initialized = 0;
    } else {
      /* Keep track of the fact that we are connected */
      connected = 1;
      initialized = 1;
    }
  }

  /*
   * Status thread startup
   *
   * As part of the TurnSignalModule function, we start up a thread to
   * read the status of the actuator on a periodic basis.  This thread
   * fills in data in the TurnSignalModule class, so we have easy
   * access to this information.  
   */

  statusCount = 0;
  statusSleepTime = STATUS_SLEEP*4;
  DGCstartMemberFunctionThread(this, &TurnSignalModule::status);

  /* Mutex for controlling access to serial port and varaibles */
  DGCcreateMutex(&actuatorMutex);
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void TurnSignalModule::initSparrow(DD_IDENT *tbl, int label_id)
{
  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the motor controller (turn signal disabled) */
      dd_setcolor_tbl(label_id, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to motor controller, but couldn't initialize */
      dd_setcolor_tbl(label_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(label_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("turn.command_loop_counter", &controlCount, tbl);
    dd_rebind_tbl("turn.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("turn.command", &commandDir, tbl);
    dd_rebind_tbl("turn.position", &currentDir, tbl);
  }
}

/*
 * TurnSignalModule::arbitrate
 *
 * Arbitrator for TurnSignalModule.  The artibiter receives turn signal
 * commands from the actuationInterface and sends them to control.  At
 * present, the only directives that are recognized are new turn signal
 * commands, which replace old turn signal commands.
 *
 */

void TurnSignalModule::arbitrate(ControlStatus* cs, MergedDirective* md)
{
  TurnControlStatus *controlResponse =
    dynamic_cast<TurnControlStatus *>(cs);

  TurnMergedDirective *mergedDirective =
    dynamic_cast<TurnMergedDirective *>(md);

  /* 
   * Check on the status of the last command acted on by control 
   *
   * Since we operate a FIFO queue, we can just need to pass the
   * information from execution back to the actuation interface.  The
   * directive IDs will make sure that everyone stays in sync.
   */
  AdriveResponse response;

  switch (controlResponse->status) {
  case ControlStatus::STOPPED:
    /* We have finished processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.source = controlResponse->source;
    response.status = GcInterfaceDirectiveStatus::COMPLETED;
    adriveTurnInterfaceNF->sendResponse( &response, response.id );
    break;

  case ControlStatus::FAILED:
    /* A failure occured in processing the directive; return a response */
    response.id = controlResponse->parent_id;
    response.source = controlResponse->source;
    response.status = GcInterfaceDirectiveStatus::FAILED;
    response.reason = controlResponse->reason;
    adriveTurnInterfaceNF->sendResponse( &response, response.id );
    gclog(1) << response.toString() << endl;
    break;

  case ControlStatus::RUNNING:
    /* No action required */
    break;
  }

  /*
   * Compute the next mergedDirective
   *
   * This code determines whether or not there is a new directive to
   * be sent to the execution unit.  We use the directive ID to keep
   * to keep track of whether we have pass something new down.  Note
   * that the ID for the mergedDirective is just a counter => we have
   * to keep track of the parent ID separately.
   * 
   */

  /* Check to see if we have rec'd a new directive from actuation interface */
  if (adriveTurnInterfaceNF->haveNewDirective()) {
    AdriveDirective newDirective;
    adriveTurnInterfaceNF->getNewDirective( &newDirective );
    struct timeval tv;
    gettimeofday(&tv, NULL);
    uint64_t t = uint64_t(tv.tv_usec) + uint64_t(1000000) * tv.tv_sec;
    gclog(2) << t << "\tReceived turn command: id = " 
	     << newDirective.id << endl;
 
    /* !! Make sure that the directive has a unique ID !! */
#   warning need to add check for unique directive ID

    /* Save the ID to pass back when we are done */
    mergedDirective->parent_id = newDirective.id;
    mergedDirective->source = ExternalSource;

    switch (newDirective.command) {
    case SetPosition:
      /* Make sure we are properly initalized */
      if (!initialized) {
	/* Respond with a failure */
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::FAILED;
	response.reason = NotInitialized;
	adriveTurnInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Check the range of the command */
      if (newDirective.arg < -1 || newDirective.arg > 1) {
	response.id = newDirective.id;
	response.status = GcInterfaceDirectiveStatus::REJECTED;
	response.reason = OutOfRange;
	adriveTurnInterfaceNF->sendResponse( &response, response.id );
	gclog(1) << response.toString() << endl;
	return;
      }

      /* Generate the directive to set the signals */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = 0;
      if (newDirective.arg < 0) mergedDirective->arg = -1;
      if (newDirective.arg > 0) mergedDirective->arg = 1;
      mergedDirective->id = ++m_latestID;
      break;

    case Reset:
      /* !! Check to make sure we are stopped !! */
#     warning missing check for stop on Turn Reset
      mergedDirective->command = Reset;
      mergedDirective->id = ++m_latestID;
      break;
    }
  }

  /* Check to see if we should switch the signals off */
  else if (steerLimitFlag && 
      fabs(adrive->state.m_steerpos) < TURN_STEER_RESET) {

    if (steerLimitFlag * currentDir > 0) {
      /* Steering has gone past the limit and back again; reset signal */
      mergedDirective->command = SetPosition;
      mergedDirective->arg = 0;
      mergedDirective->id = ++m_latestID;
      mergedDirective->source = InternalSource;
    }

    /* Reset the flag */
    steerLimitFlag = 0;

  } else if (fabs(adrive->state.m_steerpos) > TURN_STEER_LIMIT) {
    /* Steering has turned far enough to set the limit flag */
    steerLimitFlag = adrive->state.m_steerpos > 0 ? 1 : -1;
  }
}

/*
 * TurnSignalModule::control
 *
 * Control for TurnSignalModule.  The artibiter receives merged
 * directives the TurnSignalModule arbitrator.  For turn signal, this is
 * just the latest turn signal command, which is sent on to the hardware.
 * The m_currentID tag is used to keep track of which directive we are
 * currently excuting.
 *
 */

void TurnSignalModule::control(ControlStatus *cs, MergedDirective *md)
{
  TurnControlStatus* controlResponse =
    dynamic_cast<TurnControlStatus *>(cs);
  TurnMergedDirective* mergedDirective =
    dynamic_cast<TurnMergedDirective *>(md);

  /* First check to see if we have anything new to do */
  if (mergedDirective->id == m_currentID) {
    /* Tell the arbiter we are still running */
    controlResponse->status = ControlStatus::RUNNING;
    return;
  }

  /* New directive to process: update IDs and counters (for sparrow) */
  m_currentID = mergedDirective->id;
  ++controlCount;

  switch (mergedDirective->command) {
  case SetPosition:
    /* If we are in simulation mode, just send the command down */
    if (adrive->options->simulate_given) {
      /* Just set the commanded direction; no action required */

    } else {
      /* Figure out what type of turn to signal */
      TurnType turntype = HOME;
      if (mergedDirective->arg < 0) turntype = LEFT;
      if (mergedDirective->arg > 0) turntype = RIGHT;

      /* Send the command to the turn signal controller */
      int status;
      if (verbose >= 9) cerr << "Commanding turn type " << turntype << endl;
      pthread_mutex_lock(&actuatorMutex);
      if (connected) status = turnsignal->setpos(turntype);
      pthread_mutex_unlock(&actuatorMutex);
      if (verbose >= 9) cerr << "setpos returned " << status << endl;
    }

#   warning no check for failure on turnsigal->setpos

    controlResponse->id = m_currentID;
    controlResponse->status = ControlStatus::STOPPED;
    controlResponse->parent_id = mergedDirective->parent_id;
    controlResponse->source = mergedDirective->source;
    commandDir = mergedDirective->arg;
    break;

  case Reset:
    /* Initialize the turn signal actuator (if connected) */
    pthread_mutex_lock(&actuatorMutex);
    int status = !connected || (turnsignal->init() == 0);
    pthread_mutex_unlock(&actuatorMutex);

    /* Make sure that it worked */
    if (status) {
      initialized = 1;
      controlResponse->status = ControlStatus::STOPPED;

    } else {
      initialized = 0;
      controlResponse->status = ControlStatus::FAILED;
      controlResponse->reason = InitializationError;
    }      
    controlResponse->id = m_currentID;
    controlResponse->parent_id = mergedDirective->parent_id;
    break;
  }
}

/*
 * TurnSignalModule::status
 *
 * This function is called as a thread and is used to read the status
 * of the actuator.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void TurnSignalModule::status()
{
  while (1) {
    if (adrive->options->simulate_given) {
      /* For now, just do whatever we are told */
      currentDir = commandDir;
      update_time = DGCgettime();

    } else {
      if (!connected || !initialized) {
	/* TurnSignal is currently disabled, so just return zero */
	currentDir = 0;

      } else {
	/* Read the current status */
	pthread_mutex_lock(&actuatorMutex);
	TurnType turntype;
	int retval = turnsignal->getpos(&turntype);
	pthread_mutex_unlock(&actuatorMutex);

	/* Check the status and set state appropriately */
	if (retval < 0) {
	  /* Error condition */
#         warning turn signal status error not yet handled
	  cerr << "TurnSignalModule::status error reading turn signal position"
	       << endl;
	  sleep(1);

	} else {
	  if (turntype == HOME) currentDir = 0;
	  if (turntype == LEFT) currentDir = -1;
	  if (turntype == RIGHT) currentDir = 1;
	  if (turntype == HAZARD) currentDir = 2;
	}
      }
      update_time = DGCgettime();
    }

    /* Now fill up the actuatorState struct with current state */
#   warning turn signals not represented in actuator state
#ifdef UNUSED
    adrive->state.m_transpos = currentGear;
    adrive->state.m_transcmd = commandGear;
    adrive->state.m_transstatus = initialized && connected;
    adrive->state.m_trans_update_time = update_time;
#endif

    /* Now that we have the state, check to see if there are any problems */
    /* TODO: implement something here */

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}
