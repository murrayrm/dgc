/*!
 * \file turnsignals.cc
 * \brief Interface to turn signal hardware
 *
 * \author Jeremy Ma, Dominic Rizzo
 * \date June 2007
 *
 */

#include "turnsignals.hh"

/// Constructor
Turnsignal::Turnsignal(int verbose_flag) 
{
  tl.tv_sec = SMARTRELAY_MSG_TIMEOUT;
  tl.tv_usec = 0;
  ctrl_sock = 0;
  verbose = verbose_flag;
}

/// Destructor
Turnsignal::~Turnsignal() 
{
  if (ctrl_sock!= 0)
    ts_close();	

}

/// Make socket connection
int Turnsignal::_MakeSocket(int port, int &sock, fd_set &sock_fd) 
{ 
  struct sockaddr_in localAddr, serverAddr;
  struct hostent *serverHostInfo;
  int listen_socket,tempInt; 
  
  if (port == 0) 
    return(ERROR("port number not set!"));
  
  serverHostInfo = gethostbyname(host);
  if (serverHostInfo == NULL) 
    return(ERROR("Could not get host name!"));
  
  //close passed socket if opened
  _CloseSocket(sock);
  
  // Create socket
  serverAddr.sin_family = serverHostInfo->h_addrtype;
  memcpy((char *) &serverAddr.sin_addr.s_addr, serverHostInfo->h_addr_list[0], serverHostInfo->h_length);
  serverAddr.sin_port = htons(port);
  listen_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (listen_socket < 0) 
    return(ERROR("Could not listen to socket!"));
  
  // Bind any port number
  localAddr.sin_family = AF_INET;
  localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  localAddr.sin_port = htons(0);
  memset(&(localAddr.sin_zero), '\0', 8);
  
  // connect to server
  tempInt = connect(listen_socket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
  if (tempInt < 0) {
    sock = 0;
    return(ERROR("Could not connect to socket!"));
  }
  FD_ZERO(&sock_fd);  	 
  FD_SET(listen_socket, &sock_fd);
  sock = listen_socket; 
  return 0;
}


/// Close socket connection
void Turnsignal::_CloseSocket(int &socket) 
{ 
  if (socket!= 0)
    close(socket);
 
  socket = 0; 
}

/// Send message
int Turnsignal::_SendCtrlMsg(char *msg, int size) {		
    return send(ctrl_sock, msg, size, 0);
}

// GetCtrlMsg
int Turnsignal::_GetCtrlMsg(char *msg, int size) {

  int nbytes;
  timeval tmp; 
  
  tmp = tl; 
  if ((nbytes = select(ctrl_sock +1, &ctrl_fdset, NULL, NULL, &tmp)) == -1)
    return(ERROR("Could not select socket"));
  
  if (nbytes == 0)
    return(ERROR("Warning: Timeout Waiting for Message"));
  
  if ((nbytes = recv(ctrl_sock, msg, size , 0)) == -1)
    return(ERROR("Could not receive message"));
  
  if (nbytes ==0) 
    return(ERROR("Could not receive message"));

  return nbytes;

}

/// Open connection
int Turnsignal::ts_open(char *hostname) 
{
  strncpy(host, hostname == NULL ? SMARTRELAY_DEFAULT_IP : hostname, 
	  SMARTRELAY_MAX_CHAR);
  
  if (_MakeSocket(SMARTRELAY_PORT, ctrl_sock, ctrl_fdset)<0)
    return(ERROR("Could not create socket!"));
  
  return 0;	
}


/// Initialize turn signals to home position
int Turnsignal::init()
{
  // Home the device; initialize
  if(_SendCtrlMsg(TURNSIGNAL_LF_OFF, strlen(TURNSIGNAL_LF_OFF))<0)
    return(ERROR("Could not turn off left-front signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing left signal!"));
 
 if(_SendCtrlMsg(TURNSIGNAL_LR_OFF, strlen(TURNSIGNAL_LR_OFF))<0)
    return(ERROR("Could not turn off left-rear signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing left signal!"));

  if(_SendCtrlMsg(TURNSIGNAL_RF_OFF, strlen(TURNSIGNAL_RF_OFF))<0)
    return(ERROR("Could not turn off right-front signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing right signal!"));

  if(_SendCtrlMsg(TURNSIGNAL_RR_OFF, strlen(TURNSIGNAL_RR_OFF))<0)
    return(ERROR("Could not turn off right-rear signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing right signal!"));


  return(0);

}

/// Close connection
int Turnsignal::ts_close() 
{
  // Home the device before closing
  if(_SendCtrlMsg(TURNSIGNAL_LF_OFF, strlen(TURNSIGNAL_LF_OFF))<0)
    return(ERROR("Could not turn off left-front signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing left signal!"));
 
 if(_SendCtrlMsg(TURNSIGNAL_LR_OFF, strlen(TURNSIGNAL_LR_OFF))<0)
    return(ERROR("Could not turn off left-rear signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing left signal!"));

  if(_SendCtrlMsg(TURNSIGNAL_RF_OFF, strlen(TURNSIGNAL_RF_OFF))<0)
    return(ERROR("Could not turn off right-front signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing right signal!"));

  if(_SendCtrlMsg(TURNSIGNAL_RR_OFF, strlen(TURNSIGNAL_RR_OFF))<0)
    return(ERROR("Could not turn off right-rear signal!"));
  if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
    return(ERROR("Could not get acknowledgement on homing right signal!"));
 
 
  _CloseSocket(ctrl_sock);  

  MSG("Disconnected from SmartRelay.");

  return(0);

}


/// Set Turn signal
int Turnsignal::setpos(TurnType turn)
{
  if(turn==RIGHT)
    {
      if(_SendCtrlMsg(TURNSIGNAL_LF_OFF, strlen(TURNSIGNAL_LF_OFF))<0)
	return(ERROR("Could not turn off left-front signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on turning off left signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_LR_OFF, strlen(TURNSIGNAL_LR_OFF))<0)
	return(ERROR("Could not turn off left-rear signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on turning off left signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_RF_ON, strlen(TURNSIGNAL_RF_ON))<0)
	return(ERROR("Could not set right-front signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting right signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_RR_ON, strlen(TURNSIGNAL_RR_ON))<0)
	return(ERROR("Could not set right-rear signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting right signal!"));

    }
  else if(turn==LEFT)
    {
      if(_SendCtrlMsg(TURNSIGNAL_RF_OFF, strlen(TURNSIGNAL_RF_OFF))<0)
	return(ERROR("Could not turn off right-front signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on turning off right signal!"));

     if(_SendCtrlMsg(TURNSIGNAL_RR_OFF, strlen(TURNSIGNAL_RR_OFF))<0)
	return(ERROR("Could not turn off right-rear signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on turning off right signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_LF_ON, strlen(TURNSIGNAL_LF_ON))<0)
	return(ERROR("Could not set left-front signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting left signal!"));
 
      if(_SendCtrlMsg(TURNSIGNAL_LR_ON, strlen(TURNSIGNAL_LR_ON))<0)
	return(ERROR("Could not set left-rear signal!"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting left signal!"));

    }
  else if(turn==HAZARD)
    {
      if(_SendCtrlMsg(TURNSIGNAL_LF_ON, strlen(TURNSIGNAL_LF_ON))<0)
	return(ERROR("Could not set hazards"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting right-hazard signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_LR_ON, strlen(TURNSIGNAL_LR_ON))<0)
	return(ERROR("Could not set hazards"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting right-hazard signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_RF_ON, strlen(TURNSIGNAL_RF_ON))<0)
	return(ERROR("Could not set hazards"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting left-hazard signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_RR_ON, strlen(TURNSIGNAL_RR_ON))<0)
	return(ERROR("Could not set hazards"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on setting left-hazard signal!"));

    }
  else if(turn==HOME)
    {
      if(_SendCtrlMsg(TURNSIGNAL_LF_OFF, strlen(TURNSIGNAL_LF_OFF))<0)
	return(ERROR("Could not set home position"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on homing right signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_LR_OFF, strlen(TURNSIGNAL_LR_OFF))<0)
	return(ERROR("Could not set home position"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on homing right signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_RF_OFF, strlen(TURNSIGNAL_RF_OFF))<0)
	return(ERROR("Could not set home position"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on homing left signal!"));

      if(_SendCtrlMsg(TURNSIGNAL_RR_OFF, strlen(TURNSIGNAL_RR_OFF))<0)
	return(ERROR("Could not set home position"));
      if(_GetCtrlMsg(reply_buf, sizeof(reply_buf))<0)
	return(ERROR("Could not get acknowledgement on homing left signal!"));

    }
  return 0;
}

// Get Turnsignal status
int Turnsignal::getpos(TurnType *turn)
{

  int rf_status, lf_status;
  int rr_status, lr_status;
  int left_status, right_status;
  int i=0;

  // Check right-front signal
  for (i = 0; i < MAX_NUM_CHECKS; ++i) {    
    if(_SendCtrlMsg(TURNSIGNAL_RF_STATUS, strlen(TURNSIGNAL_RF_STATUS))<0)
      return(ERROR("Could not set query for right signal status!"));

    if(_GetCtrlMsg(reply_buf, sizeof(reply_buf)) < 0)
      return(ERROR("Could not get status of right front signal"));

    if (strncmp(reply_buf, TURNSIGNAL_RF_ON_STATUS, 2) == 0) break;
  }

  /* Set the status flag based on  what we read */
  if (strncmp(reply_buf, TURNSIGNAL_RF_ON_STATUS, 3) == 0) rf_status = 1;
  else if (strncmp(reply_buf, TURNSIGNAL_RF_OFF_STATUS, 3) == 0) rf_status = 0;
  else {
    reply_buf[3] = '\0';
    if (verbose >= 3) 
      cerr << "Unknown reply from right front: " << reply_buf << endl;
    rf_status = -1;
  }

  // Check right-rear signal
  for (i = 0; i < MAX_NUM_CHECKS; ++i) {    
    if(_SendCtrlMsg(TURNSIGNAL_RR_STATUS, strlen(TURNSIGNAL_RR_STATUS))<0)
      return(ERROR("Could not set query for right signal status!"));

    if(_GetCtrlMsg(reply_buf, sizeof(reply_buf)) < 0)
      return(ERROR("Could not get status of right rear signal"));

    if (strncmp(reply_buf, TURNSIGNAL_RR_ON_STATUS, 2) == 0) break;
  }

  /* Set the status flag based on  what we read */
  if (strncmp(reply_buf, TURNSIGNAL_RR_ON_STATUS, 3) == 0) rr_status = 1;
  else if (strncmp(reply_buf, TURNSIGNAL_RR_OFF_STATUS, 3) == 0) rr_status = 0;
  else {
    reply_buf[3] = '\0';
    if (verbose >= 3) 
      cerr << "Unknown reply from right rear: " << reply_buf << endl;
    rr_status = -1;
  }

  // Check left-front signal
  for (i = 0; i < MAX_NUM_CHECKS; ++i) {    
    if(_SendCtrlMsg(TURNSIGNAL_LF_STATUS, strlen(TURNSIGNAL_LF_STATUS))<0)
      return(ERROR("Could not set query for right signal status!"));

    if(_GetCtrlMsg(reply_buf, sizeof(reply_buf)) < 0)
      return(ERROR("Could not get status of left front signal"));

    if (strncmp(reply_buf, TURNSIGNAL_LF_ON_STATUS, 2) == 0) break;
  }

  /* Set the status flag based on  what we read */
  if (strncmp(reply_buf, TURNSIGNAL_LF_ON_STATUS, 3) == 0) lf_status = 1;
  else if (strncmp(reply_buf, TURNSIGNAL_LF_OFF_STATUS, 3) == 0) lf_status = 0;
  else {
    reply_buf[3] = '\0';
    if (verbose >= 3) 
      cerr << "Unknown reply from left front: " << reply_buf << endl;
    lf_status = -1;
  }

  
  // Check left-rear signal
  for (i = 0; i < MAX_NUM_CHECKS; ++i) {    
    if(_SendCtrlMsg(TURNSIGNAL_LR_STATUS, strlen(TURNSIGNAL_LR_STATUS))<0)
      return(ERROR("Could not set query for right signal status!"));

    if(_GetCtrlMsg(reply_buf, sizeof(reply_buf)) < 0)
      return(ERROR("Could not get status of left rear signal"));

    if (strncmp(reply_buf, TURNSIGNAL_LR_ON_STATUS, 2) == 0) break;
  }

  /* Set the status flag based on  what we read */
  if (strncmp(reply_buf, TURNSIGNAL_LR_ON_STATUS, 3) == 0) lr_status = 1;
  else if (strncmp(reply_buf, TURNSIGNAL_LR_OFF_STATUS, 3) == 0) lr_status = 0;
  else {
    reply_buf[3] = '\0';
    if (verbose >= 3) 
      cerr << "Unknown reply from left rear: " << reply_buf << endl;
    lr_status = -1;
  }

  /* Now sort out the status of the signals themselves */
  if (lf_status==-1 && lr_status==-1) left_status = -1;
  else left_status = (lf_status || lr_status);

  if(rf_status==-1 && rr_status==-1) right_status = -1;
  else right_status = (rf_status || rr_status);

  /* Now determine the state of the signals */
  if(left_status==-1 || right_status==-1) {
    if (verbose >= 2)
      cerr << "Unrecognized status of turns signals: "
	   << "L = " << lf_status << ", " << lr_status << "; "
	   << "R = " << rf_status << ", " << rr_status <<endl;
    // WARN("Unrecognized status of turnsignals");
    *turn = UNKNOWN;

  } else if (left_status == 1 && right_status == 1) {
    *turn = HAZARD;

  } else if (left_status == 1 && right_status == 0) {
    *turn = LEFT;

  } else if (left_status == 0 && right_status == 1) {
    *turn = RIGHT;

  } else if (left_status == 0 && right_status == 0) {
    *turn = HOME;
  }

  return 0;
} 
