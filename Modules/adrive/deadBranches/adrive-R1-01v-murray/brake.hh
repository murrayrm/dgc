#ifndef BRAKE_H
#define BRAKE_H

// I hoped these would be defined somewhere else:
//#define TRUE    1
//#define FALSE   0

///////////////////////////////////////////////////////////////////////////////
//              PROTOTYPES
///////////////////////////////////////////////////////////////////////////////
int brake_open(int, int verbose = 1);
int brake_init();
int brake_close(void);
int brake_setposition(double);
double brake_getposition(void);
double brake_getpressure();
int brake_pause(void);
int brake_disable(void);
int brake_off(void);

void simulator_brake_open(int); //Simulation (Gazebo)

///////////////////////////////////////////////////////////////////////////////
//              CONSTANTS
///////////////////////////////////////////////////////////////////////////////
#define DGC_ALICE_BRAKE_MINPOS  0
#define DGC_ALICE_BRAKE_MAXPOS  15
#define DGC_ALICE_BRAKE_ON      DGC_ALICE_BRAKE_MAXPOS
#define DGC_ALICE_BRAKE_OFF     DGC_ALICE_BRAKE_MINPOS
#define DGC_ALICE_BRAKE_READ    'R'
#define DGC_ALICE_BRAKE_PRESSURE_READ 'P'
#define DGC_ALICE_BRAKE_COMMAND 'C'
#define DGC_ALICE_BRAKE_MAXDELAY        100000  /* in microseconds */
#define DGC_ALICE_BRAKE_EOT     '\n'

#define BRAKE_SERIAL

#ifndef BRAKE_SERIAL
#define BRAKE_SDS
#endif

#endif
