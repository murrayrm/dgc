/*!
 * \file OBDIISensor.cc
 * \brief Read OBD-II hardware
 *
 * \author Richard Murray
 * \date 22 April 2007
 *
 * \ingroup gcsensor
 *
 * This file contains the routines required to read and update the
 * OBD-II status.  This is not a gcmodule, just a thread that keeps
 * track of the output from the OBD-II unit (roughly the status loop
 * part of an actuator module).
 *
 */

#include "alice/AliceConstants.h"

#include "OBDIISensor.hh"
#include "obdii.h"
#include "ActuationInterface.hh"	// for sleep times

#define US_FOOT_POUNDS_TO_NEWTON_METERS (12*2.54/100/2.2*9.8)

/*
 * OBDIISensor::OBDIISensor
 *
 * This constructor initializes the OBDIISensor class, including
 * establishing connection to the hardware.
 *
 */

OBDIISensor::OBDIISensor(ActuationInterface *actuationInterface, int sn_key, 
			 int verbose_flag)
{
  verbose = verbose_flag;

  /* Save the actuation interface so we can talk to other actuators */
  adrive = actuationInterface;

  /*
   * Hardware/simulation initailization
   *
   * All we have to do here is open up the serial port, since the
   * obdii box continuously sends out commands.  If we are running in
   * simulation mode, we don't bother trying to talk to the hardware.
   *
   * If we are using vehicle state to fake the OBD-II, we initialize a
   * state client here.  We check for this case before simulation so
   * that we can test --fake-obd.
   */

  if (adrive->options->fake_obd_flag) {
    /* Use veh state to infer OBD II settings; start thread to read state */  
    if (verbose >= 2) cerr << "OBDII: running with fake OBD" << endl;
    DGCstartMemberFunctionThread(this, &OBDIISensor::getVehicleState);
    initialized = 1;
    connected = 1;

  } else if (adrive->options->simulate_flag) {
    /* Simulation mode - no action required */
    initialized = 1;
    connected = 1;
    
  } else if (adrive->options->disable_obd_flag) {
    connected = 0;
    initialized = 1;

  } else {
    if (verbose >= 3) cout << "OBDII: initializing OBD" << endl;
    if (obdii_open(verbose) < 0) {
      cerr << "OBDII: obdii open failed" << endl;
      exit (1);
    }

    connected = 1;

    if (obdii_init() < 0) {     /* vehicle had better be on... */
      cerr << "OBDII: init failed; is vehicle on?" << endl;
      /* This isn't fatal; it'll still probably work even if we can't init,
         thanks to a few PPs set on the ELM chip. */
    } else {
      initialized = 1;
    }

    /* See if we want to take the wheel speed for lower latency */
    if (adrive->options->fake_wheelspeed_flag) {
      /* Start up the thread to read vehicle state */
      DGCstartMemberFunctionThread(this, &OBDIISensor::getVehicleState);
    }
  }

  /*
   * Status thread startup
   *
   * As part of the OBDIISensor function, we start up a thread to read
   * the status of the obdii box on a periodic basis.  This thread
   * fills in data in the OBDIISensor class, so we have easy access to
   * this information.
   */

  /* Start up the thread */
  statusCount = 0;
  statusSleepTime = STATUS_SLEEP*2;
  DGCstartMemberFunctionThread(this, &OBDIISensor::status);
}

/*!
 * Sparrow initialization
 *
 * Set up variables that are used by sparrow to keep track of the
 * status of this device.  Note that all of these variables should be
 * pointing to memory in the current class.  We also set the color of
 * the actuator label to reflect the current status.
 */

void OBDIISensor::initSparrow(DD_IDENT *tbl, int label_id)
{
  /* Save the label for later usage */
  sparrowLabel = label_id;
  sparrowTable = tbl;

  if (!adrive->options->disable_console_given) {
    /* Set the color of the label to reflect our current status */
    if (!connected) {
      /* No connection to the OBDII hardware */
      dd_setcolor_tbl(label_id, BLACK, RED, tbl);

    } else if (!initialized) {
      /* We connect to OBDII interface, but couldn't initialize */
      dd_setcolor_tbl(label_id, BLACK, YELLOW, tbl);

    } else {
      /* Everything is good to go */
      dd_setcolor_tbl(label_id, BLACK, GREEN, tbl);
    }

    /* Set up variable we want to display on the screen */
    dd_rebind_tbl("obdii.status_loop_counter", &statusCount, tbl);
    dd_rebind_tbl("obdii.fake_loop_counter", &fakeCount, tbl);
    dd_rebind_tbl("obdii.engine_rpm", &engineRPM, tbl);
    dd_rebind_tbl("obdii.engine_temp", &engineCoolantTemp, tbl);
    dd_rebind_tbl("obdii.wheel_speed", &vehicleWheelSpeed, tbl);
    dd_rebind_tbl("obdii.gear_ratio", &currentGearRatio, tbl);
    dd_rebind_tbl("obdii.throttle_position", &throttlePosition, tbl);
  }
}

/*
 * OBDIISensor::status
 *
 * This function is called as a thread and is used to read the status
 * of the OBDII hardware.  It puts its information in a state struct that is
 * part of the adrive interface.
 *
 */

void OBDIISensor::status()
{
  int obdii_is_on = 0;
  while (1) {
    /* Check for OBD faking first (so that we can test in simulation) */
    if (adrive->options->fake_obd_flag) {
      obdii_is_on = 1;
      /* Use vehicle state to infer OBD II settings */
      // Should put a mutex here to avoid updates (not critical)

      // Gear ratio: set based on currently commanded gear
      double gear_ratio;
      switch (adrive->state.m_transpos) {
      case -1:	gear_ratio = -R_TRANS1; break;	// reverse
      case 1:	gear_ratio = R_TRANS3;	break;	// forward
      default:	gear_ratio = 0;		break;	// park, neutral
      }
      currentGearRatio = gear_ratio;

      // Wheel speed: based on current velocity, converted to rad/sec
      vehicleWheelSpeed = 
	sqrt( pow(vehState.utmNorthVel, 2) + pow(vehState.utmEastVel, 2) ) /
	VEHICLE_TIRE_RADIUS;

      // Engine RPM: if running, base on gear ratio, otherwise 2000 rpm
      double gearboxRPM = gear_ratio == 0 ? 0 :
	vehicleWheelSpeed * 60 *
	(fabs(gear_ratio) * R_DIFF * R_TRANSFER * R_TC) / (2 * M_PI);
      engineRPM = fmax(gearboxRPM, W_IDLE * 60 / (2 * M_PI));

      // Wheel force: who the hell cares?
      currentWheelForce = 0.0;
      
      // Assume throttle and trans are perfect and change instantaneously
      throttlePosition = adrive->state.m_gaspos;
      transmissionPosition = adrive->state.m_transpos;

    } else if (adrive->options->simulate_flag) {
      /* In simulation mode, read state from asim */
      obdii_is_on = 1;
      if (adrive->asim_status) {
	engineRPM = adrive->asim_state.m_engineRPM;
	engineCoolantTemp = adrive->asim_state.m_EngineCoolantTemp;
	vehicleWheelSpeed = adrive->asim_state.m_VehicleWheelSpeed;
	currentGearRatio = adrive->asim_state.m_CurrentGearRatio;
        currentWheelForce = -1.0;
        throttlePosition = adrive->state.m_gaspos;
        transmissionPosition = adrive->state.m_transpos;
      }

    } else if (connected) {
      int errs;

      /* Read the current status */
      engineRPM = obdii_get_rpm();
      engineCoolantTemp = obdii_get_engine_coolant_temp();
      if (adrive->options->fake_wheelspeed_flag) {
	/* Compute the wheel speed from state for lower latency */
	vehicleWheelSpeed = 
	  sqrt( pow(vehState.utmNorthVel, 2) + pow(vehState.utmEastVel, 2) ) /
	  VEHICLE_TIRE_RADIUS;
      } else {
	vehicleWheelSpeed = obdii_get_vehicle_speed();
      }
      currentGearRatio  = obdii_get_current_gear_ratio();
      int torque = obdii_get_torque();
      char trans = obdii_get_transmission_position();
      throttlePosition = obdii_get_throttle_position();

      errs = (engineRPM < 0) + (engineCoolantTemp < 0) + 
	(vehicleWheelSpeed < 0) + (currentGearRatio < 0) +
        (torque < 0) + (throttlePosition < 0) +
        (trans == '!' || trans == 'X');

      if (errs && verbose) {
        cerr << "OBDIISensor::status: " << errs 
	     << " errors reading sensors" << endl;
      }

      /* Stolen from actuators.cc (doesn't use gear ratio?): */
      currentWheelForce = torque * TORQUE_CONVERTER_EFFICIENCY * 
	DIFFERENTIAL_RATIO / VEHICLE_TIRE_RADIUS * 
	US_FOOT_POUNDS_TO_NEWTON_METERS;

      /* Throttle position is in degrees of pedal depression; max is 15.25. */
      throttlePosition /= 15.25;
      if (throttlePosition > 1.0) throttlePosition = 1.0;

      /* Convert trans pos from char to int */
      if (trans == 'P') transmissionPosition = 0;
      if (trans == 'N') transmissionPosition = 2;
      if (trans == 'D') transmissionPosition = 1;
      if (trans == 'R') transmissionPosition = -1;

      obdii_is_on = !errs;
    }

    /* Now fill up the actuatorState struct with current state */
    adrive->state.m_obdiistatus = obdii_is_on;
    adrive->state.m_engineRPM = engineRPM;
    adrive->state.m_EngineCoolantTemp = engineCoolantTemp;
    adrive->state.m_VehicleWheelSpeed = vehicleWheelSpeed;
    adrive->state.m_CurrentGearRatio = currentGearRatio;
    adrive->state.m_WheelForce = currentWheelForce;
    adrive->state.m_ThrottlePosition = throttlePosition;
    adrive->state.m_TransmissionPosition = transmissionPosition;
    DGCgettime(adrive->state.m_obdii_update_time);

    ++statusCount;
    DGCusleep(statusSleepTime);
  }
}

void OBDIISensor::getVehicleState()
{
  int statesocket = adrive->m_skynet->listen(SNstate, ALLMODULES);

  while (1) {
    /* Read the state data from skynet */
    int count =
      adrive->m_skynet->get_msg(statesocket, &vehState, sizeof(vehState), 0);

    /* Make sure we got the right number of bytes */
    if (count != sizeof(vehState)) fakeCount = -1;

    /* Keep track of the number of times we run */
    ++fakeCount;
  }
}

/* Local Variables: */
/* c-basic-offset: 2 */
/* End: */
