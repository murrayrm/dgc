///
/// \file AttentionModule.cc
/// \brief 
/// 
/// \author Jeremy Ma Vanessa Carson
/// \date 15 August 2007
/// 


#include "AttentionModule.hh"
#include <gcinterfaces/AttentionPlannerState.hh>
#include <dgcutils/DGCutils.hh>


AttentionModule::AttentionModule()
  : GcModule("AttentionModule", &m_controlStatus, &m_mergedDirective, 20500, 20500), 
    m_planningMode(PlanningState::NOMINAL) 
{
  m_attention = new Attention::Attention();
}


AttentionModule::~AttentionModule() {
  PlannerStateInterface::releaseNorthface(m_plannerStateNF);
}


void AttentionModule::arbitrate(ControlStatus*, MergedDirective*) {
  /* We do not need to pass control status and merged directives between arbitrate and control for now */
  PlannerState plannerState; 

  if (m_plannerStateNF->haveNewDirective()) {    
    m_plannerStateNF->getNewDirective( &plannerState, true);
         
    m_planningMode = plannerState.mode;
    m_planningExitPoint = plannerState.exitPoint;
    //cout<<"Receiving planningMode "<<m_planningMode << ", exit " << m_planningExitPoint<<endl;
  }
}

void AttentionModule::control(ControlStatus*, MergedDirective*) 
{  
  /* We do not need to pass control status and merged directives between arbitrate and control for now */
  /* We simply now use m_planningMode */    

  if(!this->m_attention->quit)
    {  
            // Do heartbeat occasionally
      if(!this->m_attention->options.disable_process_control_flag)
	{
	  if(this->m_attention->heartCount % 15 == 0)
	    this->m_attention->getProcessState();           
	}    

      // Update the console
      if (!this->m_attention->options.disable_console_flag)
	cotk_update(this->m_attention->console);


      if(this->m_attention->pause)
	return;

      if(this->m_attention->updateMap(m_planningMode, m_planningExitPoint) != 0)
	WARN("Could not update map!");

      if(this->m_attention->attend() != 0)
	WARN("Could not attend to desired location!");

    }
  else
    {
      MSG("Quitting program now!");
      this->m_attention->finiConsole();
      this->m_attention->finiSensnet();
      this->m_attention->finiSkynet();
      this->Stop();
    }

  return;

}


int AttentionModule::initialize(int argc, char* argv[]) {

  // parse command line options
  if(this->m_attention->parseCmdLine(argc, argv) != 0)
    return -1;
  
  cout << "SKYNET_KEY: " << this->m_attention->skynetKey << endl;
  
  m_plannerStateNF = PlannerStateInterface::generateNorthface(this->m_attention->skynetKey, this);

  // Initialize logging
  if(this->m_attention->logging)
    {  
      ostringstream oss;             
      string logFileName;
      struct stat st;
      char timestr[64];
      time_t t = time(NULL);
      strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t)); 
      oss << "attention-" << "." << timestr << ".log";
      logFileName = oss.str();
      string suffix = "";
      
      /* if it exists already, append .1, .2, .3 ... */
      for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) 
	{
	  ostringstream tmp;
	  tmp << '.' << i;
	  suffix = tmp.str();
	}
      logFileName += suffix;
      this->m_attention->log_filename = logFileName;
    }
    
  /* Activates the gcmodule logging mechanism */
  if (this->m_attention->log_level > 0) {
    ostringstream str;
    str << this->m_attention->log_path << "attention-gcmodule";
    this->setLogLevel(this->m_attention->log_level);
    this->addLogfile(str.str().c_str());
  }


  // Initialize sensnet
  if (this->m_attention->initSensnet() != 0)
    return -1;

  // Initialize skynet
  if (this->m_attention->initSkynet() != 0)
    return -1;

  // Initialize map
  if(this->m_attention->initMap(this->m_attention->defaultConfigPath, this->m_planningMode, this->m_planningExitPoint) != 0)
    return -1;

  // Initialize console
  if (!this->m_attention->options.disable_console_flag)
  {
    if (this->m_attention->initConsole() != 0)
      return -1;
  }

  // Initialize visualization (if applicable)
  if (this->m_attention->options.view_flag)
    {
      this->m_attention->initVisualizer(&argc, argv);
      DGCstartMemberFunctionThread(this->m_attention, &Attention::v_startVisualizerThread);
    } 
  
  return(0);

}
