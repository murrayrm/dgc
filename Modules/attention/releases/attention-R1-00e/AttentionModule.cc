///
/// \file AttentionModule.cc
/// \brief 
/// 
/// \author Jeremy Ma Vanessa Carson
/// \date 15 August 2007
/// 


#include "AttentionModule.hh"
#include "gcinterfaces/AttentionPlannerState.hh"
#include <dgcutils/DGCutils.hh>
#include "CmdArgs.hh"


AttentionModule::AttentionModule()
  : GcModule("AttentionModule", &m_controlStatus, &m_mergedDirective, 10000, 1000) 
  , m_planningMode(PlanningState::NOMINAL) 
{

  m_plannerStateNF = PlannerStateInterface::generateNorthface(CmdArgs::sn_key, this);

  cout<<"Skynet key"<<CmdArgs::sn_key<<endl;

  /* Activates the gcmodule logging mechanism */
  if (CmdArgs::log_level > 0) {
    ostringstream str;
    str << CmdArgs::log_path << "planner-gcmodule";
    this->setLogLevel(CmdArgs::log_level);
    this->addLogfile(str.str().c_str());
  }

}


AttentionModule::~AttentionModule() {
  PlannerStateInterface::releaseNorthface(m_plannerStateNF);
}


void AttentionModule::arbitrate(ControlStatus*, MergedDirective*) {
  /* We do not need to pass control status and merged directives between arbitrate and control for now */
  PlannerState plannerState; 

  if (m_plannerStateNF->haveNewDirective()) {    
    m_plannerStateNF->getNewDirective( &plannerState, true);
    
    m_planningMode = plannerState.mode;
    cout<<plannerState.toString()<<endl;
  }
}

void AttentionModule::control(ControlStatus*, MergedDirective*) {
  
  /* We do not need to pass control status and merged directives between arbitrate and control for now */
  /* We simply now use m_planningMode */

  switch(m_planningMode) {
  case PlanningState::NOMINAL:
    //planNominal();
    break;
  case PlanningState::INTERSECT_LEFT:
    //planIntersection();
    break;
  case PlanningState::INTERSECT_RIGHT:
    //planIntersection();
    break;
  case PlanningState::INTERSECT_STRAIGHT:
    //planIntersection();
    break;
  case PlanningState::BACKUP:
    //planBackup();
    break;
  case PlanningState::DRIVE:
    //planDrive();
    break;
  case PlanningState::STOP_OBS:
    //planStopObs();
    break;
  case PlanningState::PASS_LEFT:
    //planPassLeft();
    break;
  case PlanningState::PASS_RIGHT:
    //planPassRight();
    break;
  case PlanningState::UTURN:
    //planUturn();
    break;
  case PlanningState::ZONE:
    //planZone();
    break;
  default:
    break;
  }
}

