Fri Aug 31  1:37:59 2007	Jeremy Ma (jerma)

	* version R1-00k
	BUGS:  
	FILES: AttentionModule.cc(36770)
	modified some sleep times so that this module doesn't run at 95Hz.

Fri Aug 31  0:23:32 2007	Jeremy Ma (jerma)

	* version R1-00j
	BUGS:  
	Deleted files: emapWrapper.cc emapWrapper.hh
	FILES: Makefile.yam(36758), WorldWin.cc(36758),
		attention.cc(36758), attention.hh(36758)
	fixed a few small bugs that would cause the PTU to fixate on an old
	maximum cell which wasn't the peak cell it should have been
	attending to; if it receives a change in state from planner, it
	immediately sends a new command to the PTU. The only potential
	pitfall here is if planner frequently flickers between two very
	different states (like INTERSECT_LEFT or INTERSECT_RIGHT) in which
	case a flood of messages would be generated to the PTU and possibly
	cause it to fail. If planner flickers between STOP_OBS and DRIVE
	(which seems more the case), that won't cause any problems because
	the PTU will simply just sweep in either mode. 

	FILES: Makefile.yam(36760)
	fixed a small bug in the makefile.

Wed Aug 29 20:12:08 2007	Jeremy Ma (jerma)

	* version R1-00i
	BUGS:  
	Deleted files: test.rndf
	FILES: ATTENTION.CFG(36372), AttentionModule.cc(36372),
		attention.cc(36372), attention.hh(36372)
	added a 4 second wait to the PTU for the case when alice's velocity
	is less than ALICE_STOP_SPEED (currently set to 0.5m/s); this is so
	the radar can establish a feasible track.

	FILES: ATTENTION.CFG(36409), attention.cc(36409),
		attention.hh(36409), cmdline.ggo(36409)
	now have the option to control how long the ptu waits before
	processing the next scan

	FILES: AttentionModule.cc(36012), attention.cc(36012),
		attention.hh(36012)
	fixed INTERSECT_STRAIGHT handling as well as situations when the
	desired turning lane or entry lane is not found

	FILES: attention.cc(36386), attention.hh(36386)
	moved various valued variables to constants defined in the header
	file 

Mon Aug 27 22:17:45 2007	Jeremy Ma (jerma)

	* version R1-00h
	BUGS:  
	FILES: ATTENTION.CFG(35819), attention.cc(35819),
		attention.hh(35819), cmdline.ggo(35819)
	When alice now slows to a stop (has a velocity < 1.0m), and the
	planning state is either INTERSECT_LEFT, INTERSECT_RIGHT,
	INTERSECT_STRAIGHT, PASS_LEFT, or PASS_RIGHT, the PTU holds its
	position long enough to allow the radar to establish a track

	FILES: attention.cc(35714), attention.hh(35714), cmdline.ggo(35714)
	sensor coverage layer now can be simulated (a big 35m ring around
	Alice) or free space from bumper ladars

Sun Aug 26 22:56:09 2007	Jeremy Ma (jerma)

	* version R1-00g
	BUGS:  
	FILES: attention.cc(35592), attention.hh(35592)
	added PTU control for the remaining planner states (NOMINAL, DRIVE,
	UTURN, STOP_OBS, and ZONE). They all do the exact same thing which
	is go through a temporary cycled list of panning and tilting
	commands to create a sweeping pattern. This is more or less a place
	holder until more intelligent functionality is found and/or
	implemented. 

Sun Aug 26 14:36:05 2007	Jeremy Ma (jerma)

	* version R1-00f
	BUGS:  
	New files: attention.hh attentionMain.cc emapWrapper.cc
		emapWrapper.hh
	Deleted files: RndfPainter.cc RndfPainter.hh
	FILES: ATTENTION.CFG(35555), AttentionMain.cc(35555),
		AttentionModule.cc(35555), AttentionModule.hh(35555),
		Makefile.yam(35555), WorldWin.cc(35555),
		WorldWin.hh(35555), attention.cc(35555),
		cmdline.ggo(35555), send_test.cc(35555)
	Merging in my code into the AttentionModule shell that Vanessa
	wrote. Cleaning up some unused files and adding new files I wrote.
	Everything seems to work fine except the console QUIT button don't
	quite let you exit cleanly (mainly because I don't know how to exit
	the gcmodule arbitrate-control loop cleanly). Other than that,
	looks pretty good. 

Thu Aug 23 17:20:34 2007	vcarson (vcarson)

	* version R1-00e
	BUGS:  
	New files: AttentionMain.cc CmdArgs.cc CmdArgs.hh
Thu Aug 23 13:33:59 2007	vcarson (vcarson)

	* version R1-00d
	BUGS:  
	FILES: AttentionModule.cc(35166), AttentionModule.hh(35166),
		Makefile.yam(35166)
	Fixed communication between Planner and AttentionModule. 

Mon Aug 20 13:39:57 2007	vcarson (vcarson)

	* version R1-00c
	BUGS:  
	New files: AttentionModule.cc AttentionModule.hh
	FILES: Makefile.yam(34232), attention.cc(34232)
	Fixed init warnings in attention.cc.  Added AttentionModule to
	establish planner state communication. AttentionModule is a gc
	module.  Changed Makefile to compile these files.      

Fri Aug 17  5:23:23 2007	Jeremy Ma (jerma)

	* version R1-00b
	BUGS:  
	FILES: Makefile.yam(34051), attention.cc(34051)
	cleaned up some unnecessary include files.

	FILES: attention.cc(34050), send_test.cc(34050)
	modified the PlanningModes to be consistent with what planner
	actually is capable of sending. Also got rid of gaussian costs and
	used long blocks instead.Works much better. Forgetting constant is
	also incorporated.  Still needs a bit more debugging before ramping
	the speed on the PTU.

Thu Aug 16 14:31:27 2007	Jeremy Ma (jerma)

	* version R1-00a
	BUGS:  
	New files: ATTENTION.CFG RndfPainter.cc RndfPainter.hh WorldWin.cc
		WorldWin.hh attention.cc cmdline.ggo glUtils.cc glUtils.hh
		send_test.cc test.rndf
	FILES: Makefile.yam(30257)
	Added visualization capabilities within this module. Visualization
	is done without passing deltas; direct access to local variables
	from within the module. I basically stripped down a bunch of files
	in sensviewer and added them here.

	FILES: Makefile.yam(31418)
	adding gist-layer generation

	FILES: Makefile.yam(32094)
	added right lane change; also did pass through of planning mode
	commands to load gist layers.

	FILES: Makefile.yam(33886)
	added panning/tilting control as well as cleaned up a bunch of
	debug messages.

Tue Jun 26 15:39:48 2007	Jeremy Ma (jerma)

	* version R1-00
	Created attention module.











