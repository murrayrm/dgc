#ifndef __ATTENTION_HH__
#define __ATTENTION_HH__

#include <assert.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <iostream>

// dgc basic header files
#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/PTUStateBlob.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/PTUCommand.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <vector>

// State talker
#include <skynettalker/SkynetTalker.hh>

// Map header file
#include <map/Map.hh>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>

// Time stamp header file
#include <sys/time.h>

// Frames header files
#include <frames/pose3.h>
#include <frames/mat44.h>

// cotk display
#include <cotk/cotk.h>

// custom header files
#include "cmdline.h"
#include "WorldWin.hh"

// Planning mode interface
#include <gcinterfaces/AttentionPlannerState.hh>

// Bad macro defined
#undef border

// for visualizer
#include <GL/glut.h>
#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>

// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 1 : 1)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

#define BASE_TO_TILTAXIS              0.115 // m
#define TILTAXIS_TO_TOOLFRAME         0.071 //m

#define ALICE_STOP_SPEED              1.75  //m/s

#define LOOK_UP_LEFT_LANE_DIST        50    // m; how far to look up the left lane
#define LOOK_UP_RIGHT_LANE_DIST       50    // m; how far to look up the right lane
#define LOOK_UP_OTHER_LANE_DIST       50    // m; how far to look up the other lanes for a straight intersection

#define LANE_NO_STOP_COST_LEVEL1      5
#define LANE_NO_STOP_COST_LEVEL2      7
#define LANE_NO_STOP_COST_LEVEL3      9

#define LANE_NO_STOP_COST_MAX         9     // highest cost to give lane when that lane has no stop sign
#define LANE_NO_STOP_COST             8     // cost in a given lane when that lane has no stop sign
#define TRAVEL_ACROSS_LANE_COST       10    // cost for travelling across a lane when making a left turn that has no stop sign
#define LEFT_TURN_LANE_UP_COST        7.5   // cost up left turn lane when making left turn
#define RIGHT_TURN_LANE_UP_COST       10    // cost up right turn lane when making a right turn
#define OTHER_LANE_UP_COST            5     // cost of other lanes when making a turn

#define LOOK_BACK_DIST                50    // m; how far to look back when performing a lane change
#define LOOK_AHEAD_DIST               50    // m; how far to look ahead when performing a lane change
#define PASS_LANE_HIGH_COST           10    //    high cost value for a lane change
#define PASS_LANE_LOW_COST            5     //    low cost value for a lane change
#define SHIFT_RIGHT_DIST              5     //    how far to shift the current center line if the desired lane to change to is not a known lane
#define SHIFT_LEFT_DIST               5     //    how far to shift the current center line if the desired lane to change to is not a known lane

#define MAXPAN                        170   // deg; maximum allowable pan angle
#define MINPAN                       -170   // deg; minimum allowable pan angle
#define MAXTILT                        20   // deg; maximum allowable tilt angle    
#define MINTILT                       -20   // deg; minimum allowable tilt angle

#define DIST_TO_INTERSECTION_THRESH   30    // distance threshold to intersection

// all wait times below must be different from each other
#define RADAR_WAIT_TIME               35    // s; how long to wait for radar to initialize tracks
#define SWEEP_WAIT_TIME               0.25  // s; how long to wait between sweep modes 
#define FIXED_WAIT_TIME               1     // s; how long to wait between fixed transition; this number really has no affect other than that it has to be different than the other two

#define FIXED_TILT                    -3    // deg; fixed tilt angle when driving above ALICE_STOP_SPEED   
#define FIXED_PAN                     7.5   // deg; corresponds to the fixed offset in pan; this is home position                
#define FIXED_PANSPEED                20    // deg/s; panspeed for moving to fixed position
#define FIXED_TILTSPEED               10    // deg/s; tiltspeed for moving to fixed position

#define DP_FIXED_PAN                  112
#define DP_FIXED_TILT                 0
#define DP_FIXED_PANSPEED             30
#define DP_FIXED_TILTSPEED            30

#define SWEEP_PANSPEED                20    // deg/s; panspeed for sweeping behavior
#define SWEEP_TILTSPEED               15    // deg/s; tiltspeed for sweeping behavior
#define SWEEP_PANMAX                  67.5  // deg; max pan angle for sweeping behavior
#define SWEEP_PANMIN                 -52.5  // deg; min pan angle for sweeping behavior
#define SWEEP_TILTMAX                  0.0  // deg; max tilt angle for sweeping behavior
#define SWEEP_TILTMIN                -30.0  // deg; min tilt angle for sweeping behavior
#define SWEEP_HOME                     7.5  // deg; home pan angle for sweeping behavior

#define RADAR_PANSPEED                30    // deg/s; panspeed for moving into radar look position
#define RADAR_TILTSPEED               25    // deg/s; tiltspeed for moving into radar look position

#define MIN_COST_THRESH               1     // minimum cost needed to command an attend movement
#define STOP_LINE_DIST_THRESH         20    // expect adjacent stop line to be within this radius of planningPt
#define CELL_DIST_THRESH              5     // distance threshold to decide when we've arrived at a desired cell
using namespace std;

class Attention
{
public:

  //! Default constructor
  Attention();
  
  //! Default destructor
  ~Attention();

  //! Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  //! Parse the config file
  int parseConfigFile(const char *configPath);

  //! Initialize attention map
  int initMap(const char *configPath, PlanningState::Mode l_planningMode, PlanningState::ExitPoint l_planningExitPoint);

  //! Update the map
  int updateMap(PlanningState::Mode l_planningMode, PlanningState::ExitPoint l_planningExitPoint);

  //! Initialize sensnet -- will need sensnet for PTU state
  int initSensnet();

  //! Finalize sensnet -- will need sensnet for PTU state
  int finiSensnet();

  //! Initialize skynet -- will need skynet for receiving gist and sending ptucommand
  int initSkynet();

  //! Finalize skynet -- will need skynet for receiving gist and sending ptucommand
  int finiSkynet();

  //! Do the attending based on current map
  int attend();

  //! Get alice and ptu state
  int getState();

  //! Get process state
  int getProcessState();
  
  //! Initialize console display
  int initConsole();

  //! Finalize console display
  int finiConsole();

  //! Console button callback
  static int onUserQuit(cotk_t *console, Attention *self, const char *token);

  //! Console button callback
  static int onUserPause(cotk_t *console, Attention *self, const char *token);

  //! Initialize visualizer
  void initVisualizer(int *argc, char** argv);

  //! Visualization thread
  void v_startVisualizerThread();

  //! Action callback
  static void v_onAction(Fl_Widget *w, int option);
  
  //! Exit callback
  static void v_onExit(Fl_Widget *w, int option);
  
public:

  //! Default configuration path
  char *defaultConfigPath;

  //! Spread settings
  char *spreadDaemon;
  int skynetKey;

  //! SensNet handle
  sensnet_t *sensnet;

  //! CSS logging settings
  bool debug;
  int verbose_level;
  bool logging;
  string log_path;
  string log_filename;
  int log_level;
  
  //! Console text display
  cotk_t *console;

  //! Current vehicle state data
  VehicleState m_state;

  //! PTU blob for ptu state
  PTUStateBlob m_ptublob;
  
  //! Skynet object
  skynet *m_skynet;

  //! Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  //! Top-level window
  Fl_Window *mainwin;
  
  //! Top-level menu
  Fl_Menu_Bar *menubar;

  //! 3D window
  WorldWin *worldwin;
  
  //! Program options
  gengetopt_args_info options;  
  
  vector<CellData> m_gistvec;

  vector<CellData> m_gistvec_dp;

  vector<CellData> m_costvec;

  vector<CellData> m_costvec_dp;

  vector<MapElement> m_priorMapData;

  pthread_mutex_t m_stateMutex;

  pthread_mutex_t m_ptuStateMutex;

  pthread_mutex_t m_currentCellMutex;

  pthread_mutex_t m_desiredCellMutex;

  pthread_mutex_t m_gistvecMutex;

  pthread_mutex_t m_costvecMutex;

  CellData currentAttendedCell;

  CellData desiredAttendedCell;

  //! Counter for heartbeat
  int heartCount;

  //! cycle count
  int cycleCount;

  double cycleAvg;

private:
  
  //! Grab lane lines from RNDF
  int loadMapPriorData();

  //! load appropriate gist map based on directive
  int loadGistMap();

  //! load fused cost map
  int loadFusedCostMap();

  //! Assigns the given cost to the cell in the array that is furthest from Alice yet still in the map
  int getCellCost(const point2arr &laneCenterLine, vector<CellData> &cellVec, double cost);

  //! Densifies the given line to the given resolution up to a distance along that line
  //! --- if the distance is longer than the line, then it just goes to the end of the array
  void densifyLine(const point2arr &ptarr, point2arr &newptarr, double res, double distance);

  //! Finds the intersection of the Line of Site vector with the ground plane
  int findLineOfSiteIntersect(float *x, float *y, float *z);

  //! Converts a line of site pose to corresponding pan-tilt values
  void lineOfSiteToPanTilt(float *pan, float *tilt, float const &localx, float const &localy, float const &localz);

  //! Get intersection stop lines
  int getIntersectStopLines(const PointLabel startPtLabel, vector<PointLabel> &stopLinePoints);

  bool isPlanningPtTooFar(const PointLabel planningPtLabel, const LaneLabel currLaneLabel, const VehicleState state);

  int getDistanceToIntersection(const PointLabel planningPtLabel, const LaneLabel currLaneLabel, const VehicleState state);

  //! Send PTU command
  int sendPTUcommand(PTUCommand command);

  //! World map -- vector based
  Map worldMap;
  
  //! receive subgroup
  int recvSubGroup;

  //! rndf filename
  string rndfFilename;

  //! Our module id (SkyNet)
  modulename moduleId;
    
  //! No data value for gist layer
  double gistNoDataVal;

  //! No data value for cost layer
  double costNoDataVal;
    
  //! command Socket for commanding the PTU
  int ptuCommandSocket;

  //! planning mode
  PlanningState::Mode m_planningMode;
  PointLabel m_planningPtLabel;
  point2_uncertain m_planningPt;

  //! Counter for sent PTU commands
  int ptuCount;

  //! Counter for received gist commands
  int gistCount;

  //! Time stamp for last sent ptu command
  uint64_t ptuTime;

  //! how long should we stay still to look down a lane
  double lookTime; 
  double lastLookTime;

  double stopTime;
  bool stopTimeStamped;

  double dist2int;

  //! command to PTU
  PTUCommand command; // this is the outgoing command

  struct timeval stv;

  double lasttime;
  double currenttime;

  CellData maxCostCell;
  int maxCostIndex;

  CellData maxCostCell_dp;
  int maxCostIndex_dp;

  //! the time constant that will be used
  double timeConstant;

  // sweeping
  bool sweep;
  int sweepMode;

  // fixed nod
  bool fixed;  

  double groundSpeed;

};

#endif
