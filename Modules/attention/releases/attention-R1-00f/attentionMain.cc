#include "attention.hh"

int main(int argc, char* argv[])
{
  // Initialize attention module
  Attention *attention;
  attention = new Attention();
  PlanningState::Mode planningMode;
  

  int loopCount = 0;
  planningMode = PlanningState::NOMINAL;
  int modeSocket;
  int modeReceived;
  skynet *m_skynet;

  // Parse command line options
  if (attention->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize sensnet
  if (attention->initSensnet() != 0)
    return -1;

  // Initialize skynet
  if (attention->initSkynet() != 0)
    return -1;

  //--------------------------------
  //--------------------------------
  m_skynet = new skynet(MODattention, atoi(getenv("SKYNET_KEY")), NULL);
  modeSocket = m_skynet->listen(SNgist,MODattention);
  //--------------------------------
  //--------------------------------
  
  
  // Initialize map
  if(attention->initMap(attention->defaultConfigPath, planningMode) != 0)
    return -1;

  // Initialize console
  if (!attention->options.disable_console_flag)
  {
    if (attention->initConsole() != 0)
      return -1;
  }

  // Initialize visualization (if applicable)
  if (attention->options.view_flag)
    {
      attention->initVisualizer(&argc, argv);
      DGCstartMemberFunctionThread(attention, &Attention::v_startVisualizerThread);
    } 
 
  // Start Processing
  while(!attention->quit)
    {
      usleep(1000);

      // Do heartbeat occasionally
      if(!attention->options.disable_process_control_flag)
	{
	  if(attention->heartCount % 15 == 0)
	    attention->getProcessState();           
	}    

      // Update the console
      if (!attention->options.disable_console_flag)
	cotk_update(attention->console);
      
      // If paused, give up our time slice.
      if (attention->pause)
	{      
	  usleep(0);
	  continue;
	}
      
      //--------------------------------
      //--------------------------------
      if(m_skynet->is_msg(modeSocket))       
	modeReceived = m_skynet->get_msg(modeSocket, &planningMode, sizeof(planningMode), 0);	
      //--------------------------------
      //--------------------------------

      // Update map -- includes listening for gist directive            
      if(attention->updateMap(planningMode) != 0)
	break;		      
           
      // Attend -- includes sending PTU command directive
      // FIX: currently attend every 100 times through the loop; too many PTUcommands
      if(attention->attend() != 0)
	break;

      loopCount++;
    }     

  attention->finiConsole();
  attention->finiSensnet();
  attention->finiSkynet();

  delete attention;

  cmdline_parser_free(&attention->options);

  MSG("program exited cleanly");

  return 0;

  

}
