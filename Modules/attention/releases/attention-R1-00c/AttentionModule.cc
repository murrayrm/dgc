///
/// \file AttentionModule.cc
/// \brief 
/// 
/// \author Jeremy Ma Vanessa Carson
/// \date 15 August 2007
/// 


#include "AttentionModule.hh"
#include "gcinterfaces/AttentionPlannerState.hh"
#include <dgcutils/DGCutils.hh>
#include "CmdArgs.hh"


AttentionModule::AttentionModule()
  : GcModule("AttentionModule", &m_controlStatus, &m_mergedDirective, 10000, 1000) 
  , m_planningMode(NOMINAL) 
{

  m_plannerStateNF = PlannerStateInterface::generateNorthface(CmdArgs::sn_key, this);

  /* Activates the gcmodule logging mechanism */
  if (CmdArgs::log_level > 0) {
    ostringstream str;
    str << CmdArgs::log_path << "planner-gcmodule";
    this->setLogLevel(CmdArgs::log_level);
    this->addLogfile(str.str().c_str());
  }

}


AttentionModule::~AttentionModule() {
  PlannerStateInterface::releaseNorthface(m_plannerStateNF);

}


void AttentionModule::arbitrate(ControlStatus*, MergedDirective*) {
  /* We do not need to pass control status and merged directives between arbitrate and control for now */
  while (m_plannerStateNF->haveNewDirective()) {    
    PlannerState plannerState; 
    m_plannerStateNF->getNewDirective( &plannerState );
    m_planningMode = plannerState.mode;
  }
  cout<<"Planning mode "<<m_planningMode<<endl;
}

void AttentionModule::control(ControlStatus*, MergedDirective*) {
  /* We do not need to pass control status and merged directives between arbitrate and control for now */
  /* We simply now use m_planningMode */
  
}

