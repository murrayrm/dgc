/*!
 * \file CmdArgs.cc
 * \brief Header for the command line arguments object that gets passed around in the planner stack
 *
 * \author Vanessa Carson 
 * \date 22 August 2007
 *
 * \ingroup attention
 *
 */

#ifndef CMDARGS_HH_
#define CMDARGS_HH_

#include <string>

using namespace std;

class CmdArgs {
public:
  static int sn_key;
  static bool debug;
  static int verbose_level;
  static bool logging;
  static bool console;
  static bool lane_cost;
  static string log_path;
  static string log_filename;
  static int log_level;
};

#endif
