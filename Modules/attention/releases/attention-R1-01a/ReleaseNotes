              Release Notes for "attention" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "attention" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "attention" module can be found in
the ChangeLog file.

Release R1-01a (Wed Oct 17  0:13:48 2007):
	With the new location of the radar, there shouldn't be need to look left anymore. Hasn't been tested in 
simulation so we'll see what happens in the field tomorrow.

Release R1-01 (Sun Oct 14  0:59:19 2007):
	recompile with latest gcmodule

Release R1-00z (Sat Oct 13 17:03:16 2007):
	added a distance dependent check on intersections so that if we're close enough, we start panning/tilting right away; 
what was happening before was I was doing a speed based check (i.e. if our ground speed was less than 1.5m/s, then start 
looking) but that didn't allow us to start looking early enough; with this release, we start looking 30m before the 
intersection. 

Release R1-00y (Thu Oct 11 19:41:37 2007):
	More changes:
1) reverted back to local frame since the PTUfeeder accepts line of site 
commands in the local frame
2) increased the pan and tilt nodding speeds since it fares quite well 
with the new RAMP motion commands to the PTU
3) added a distance to intersection check because of certain situations 
where Alice would get stuck several meters before an intersection due to 
some obstacle while the reported state from planner would be 
INTERSECT_LEFT or INTERSECT_RIGHT and alice would be "looking" rather than 
scanning. 
4) cleaned up some code to be consistent with coding standards.

Release R1-00x (Tue Oct  9  4:26:52 2007):
	A couple of changes:
1) only paint cost for radar-looking for lanes with no stop lines
2) nod faster
3) use of Alice site frame pose
4) line of site correction threshold reduced

Release R1-00w (Sun Oct  7  0:07:10 2007):
	A couple of changes here:
1) removed emap and cmap dependencies 
2) improved algorithm for checking which lanes to point the radar down, 
taking into account which lanes have stop lines
3) increased radar wait time
4) changed sweeping pattern to help groundstrike filtering get more 
coverage

Release R1-00v (Sat Oct  6 23:58:01 2007):
	added frame transformation to updateMap function to handle floating
	local frame

Release R1-00u (Thu Oct  4  2:52:59 2007):
	Adding some unit test files; cleaned up some unnecessary code 
that was slowing stuff down; reduced CPU% from 56% to 14%; implemented 
longer waits for the radar to establish a track; i ran into some 
difficulty in testing in simulation because somehow over the past few 
days of numerous planner releases, the attention communication may have 
been broken. Until i can get it resolved, I've put together 
"temp-attention" binary which will get us through the el toro runs 
tomorrow with groundstrike filtering and all that jazz (just no radar 
testing). 

Release R1-00t (Thu Sep 27  0:38:45 2007):
	In complying with andrew's request, SVN rm-ing some files which are no longer used and were poorly named (i.e. differentiated by case only). 

Release R1-00s (Fri Sep 21 11:58:46 2007):
	updated to  work with renamed setLocalToGlobalOffset map function

Release R1-00r (Tue Sep 18 11:11:11 2007):
	added a nodding mode for situations when Alice is in NOMINAL or DRIVE state. All other cases, typical sweeping behavior 
is executed. 

Release R1-00q (Mon Sep 17  8:46:11 2007):
	Updated map calls to allow for non-static local to global frame
	transform

Release R1-00p (Fri Sep 14  1:34:48 2007):
	Changed the tilt speed so that's a constant value. I before had 
it so that the tiltspeed would be adjusted such that the panning and 
tilting would end at the exact same moment the desired location was 
achieved -- but this didn't seem to always work and the tilting would 
end up not being commanded. This fix should always ensure some tilting 
motion. 

Release R1-00o (Tue Sep 11 18:59:29 2007):
	Vastly simplified the logic and computations within; it was too complicated before considering what Alice's current capabilites are. 
Basic logic now is to sweep continuously in a figure-8 when Alice is moving above some nominal speed; as she approaches an intersection, she 
does your typical "look-left / look-right" for all potential entering lanes to allow the radar on top to establish a track. Once all lanes 
are checked, ptu goes back to sweeping until one of two things happens: 
1) the cost of a "watched" lane grows above some threshold 
2) alice moves again at a speed above the nominal threshold.

Looks pretty good in simulation. 

Release R1-00n (Mon Sep 10 17:36:38 2007):
	Tweaked costs so that preference is given to certain lanes over others when attending and approaching intersections. Also fixed a few 
bugs here and there (minor fixes). 

Release R1-00m (Thu Sep  6 19:44:06 2007):
	Integrated the exitPt info coming from planner; increased the 
distance to look down lanes when doing intersection handling

Release R1-00l (Wed Sep  5 13:21:47 2007):
	Added the receipt of an exit point in the planner state information. 

Release R1-00k (Fri Aug 31  1:38:02 2007):
	Changed some sleep times in the gcmodule instantiation so that the module doesn't run at 95Hz 
and eat up 70% of CPU. It's about 40Hz now and 19% of CPU.

Release R1-00j (Fri Aug 31  0:23:39 2007):
	Fixed a small bug that was causing the PTU to fixate on a max cell from a previous planner mode 
which wasn't exactly the peak cell to be attending to. Changed it so that if planner ever sends a 
significant change in state (like NOMINAL to INTERSECT_RIGHT or INTERSECT_LEFT), then a new command is 
issued. The only potential pitfall is if planner flickers continuously between two very different 
states which could end up sending a pluthera of PTU commands which could possibly result in failure 
(though nothing a power cycle couldn't fix).

Release R1-00i (Wed Aug 29 20:12:19 2007):
	Adjusted the way PTU commands are sent such that if alice's speed is less than ALICE_STOP_SPEED, a time-out variable is increased 
so that the PTU spends more time scanning down lanes to give the radar enough time to initialize. Also moved a bunch of variables to 
defined constants in the header file and increased the sleep time on the gcmodule loop since the attention module was running faster than 
the ptu state could be reported. 

Release R1-00h (Mon Aug 27 22:17:52 2007):
	Sensor coverage map now reads in from bumper ladars only by default (use the --sim-sensor options to disable that). Also, added functionality that causes the PTU to wait a certain amount of time for the radar to establish a track before moving to the next peak point in the map (currently set to 4 seconds). This timed wait only gets implemented if Alice's velocity is < 1.0m/s. If Alice is moving > 1.0m/s, then the PTU only waits 0.5sec before moving to the next peak  cell. It's quite possible that if the PTU is moving, the radar returned data is quite unreliable (yet to be verified, however).

Release R1-00g (Sun Aug 26 22:56:13 2007):
	Added attentional control of the PTU for the remaining planner states (NOMINAL, DRIVE, UTURN, ZONE, and STOP_OBS). Currently, these all the 
do the same thing in that the PTU will simply sweep through a figure-8 and cover quite a bit of ground. This behavior may not be optimal but 
it'll serve as a place-holder until more intelligent functionality is implemented (if necessary). 

Also, I haven't converted my comments to doxygen format yet but will in the next release (which should be out by tomorrow morning).

Release R1-00f (Sun Aug 26 14:36:20 2007):
	A couple of major commits happening here so I'll list them one by one:
* Removed the cmap class altogether and implemented the emap class which draws polygons much faster (no more latency issues)
** to implement the above change, an emapWrapper class has been written (existing only within this module) that implements a handful of useful cmap 
class functions yet without the overhead of deltas and so forth -- will eventually move it to the emap module
* Re-implemented all three layers that consitute the attention architecture with the new map structure (i.e. a sensor-coverage layer, a gist-layer 
based on what planning's current mode is, and a final cost layer which is a fused version of the above two)
** drawing polygons is no longer a blocker
** the following planner states now affect the movement of the PTU: INTERSECT_RIGHT, INTERSECT_LEFT, INTERSECT_STRAIGHT, PASS_LEFT, PASS_RIGHT
** still have yet to implement the NOMINAL, DRIVE, STOP_OBS, and ZONE planning modes (will come in the next release)
** control of the PTU is based on the peak value in the fused cost layer; some tweeking still needed to prevent the PTU from 
swiveling around in a jerky motion
* code has been merged in with Vanessa's AttentionModule shells
** in the process of doing the above change, the CmdArgs.hh/cc have become obsolete (though still svn watched since I need to check with Vanessa 
before 'svn rm'-ing them)


Release R1-00e (Thu Aug 23 17:20:38 2007):
	Added AttentionMain and CmdArgs for running AttentionModule. 
Release R1-00d (Thu Aug 23 13:34:07 2007):
	Changed AttentionModule to get latest directive from planner (vs. oldest one).  AttentionModule now receives the NOMINAL
state as well.    

Release R1-00c (Mon Aug 20 13:40:00 2007):
	Added AttentionModule class.  It derives from gcmodule and servs as a communication 
shell for state updates from planner.  Changed Makefile to account for gcmodule 
dependencies.  Cleaned some warnings in attention.cc.  
	

Release R1-00b (Fri Aug 17  5:23:28 2007):
	Fixed the planning modes to be consistent with what planner actually uses (according to Kristian and 
Vanessa). This version of the attention module uses blocks for cost in the costlayer of the map (as opposed 
to Gaussian curves in the previous release). Panning and tilting of the PTU seem a lot smoother now. Error 
handling still needs to be implemented so this is a fragile release.

Release R1-00a (Thu Aug 16 14:31:30 2007):
	this release has a lot updates yet the module is not in a feasible 
working condition (the PTU swings too much -- almost out of control in 
simulation). 
* The sensor coverage layer is in place but problems with the drawPolygon 
function have been a blocker. 
* The gist layers have been added for most planning states.
* Panning and Tilting commands are currently being sent to the PTU based 
on peak cost cell
* cotk display implemented. 
all that's left now is fine tuning to get the PTU to move smoothly and not 
so jerky. Making a release now so Vanessa can add her AttentionModule shell 
for CSS implementation. 


Release R1-00 (Tue Jun 26 15:39:48 2007):
	Created.






























