#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <emap/emap.hh>
#include <iostream>

/*
 * /!* This is a wrapper around the basic emap class functions.
 * /!* It allows one to make calls to the augment the map similar to how cmap does it.
 */

using namespace std;

class EMapWrapper
{
public: 
  // Default constructor
  EMapWrapper();

  // Default destructor
  ~EMapWrapper();

  // initializes the emap and some other variables
  void initMap(EMap* m_emap, double m_localX, double m_localY, double m_rowRes, double m_colRes, emap_t m_initValue);
    
  // clears the emap
  void clearMap(EMap* m_emap, emap_t m_clearValue);

  // updates the map to the new vehicle location
  void updateVehicleLoc(EMap* m_emap, double m_localX, double m_localY, emap_t m_clearValue);

  // converts the UTM coordinates to window coordinates
  void UTM2Win(EMap* m_emap, double m_localX, double m_localY, int* m_winRow, int* m_winCol);

  // converts the window coordinates to UTM coordinates
  void Win2UTM(EMap* m_emap, int m_winRow, int m_winCol, double* m_localX, double* m_localY);

  // gets the data at the appropriate location specified in window coordinates
  int getDataWin(EMap* m_emap, int m_winRow, int m_winCol, emap_t *m_val);

  // sets the data in the window coordinates
  void setDataWin(EMap* m_emap, int m_winRow, int m_winCol, emap_t m_val);

  // gets the data at the appropriate location specified in UTM coordinates
  int getDataUTM(EMap* m_emap, double m_localX, double m_localY, emap_t *m_val);

  // sets the data in UTM coordinates
  void setDataUTM(EMap* m_emap, double m_localX, double m_localY, emap_t m_val);

  // fill area enclosed by triangle to desired value
  void fillTriangle(EMap* m_emap, int x1, int y1, int x2, int y2, int x3, int y3, emap_t m_val);

  // checks whether given UTM point is inside the map
  bool isInsideMap(EMap* m_emap, double m_localX, double m_localY);

private:

  // last known vehicle state
  double lastLocalX;  
  double lastLocalY;
  
  // row resolution of the map in meters
  double rowRes;

  // column resolution of the map in meters
  double colRes;

  // size of the map in number of cells specified by one side of the square map
  int size;

};
