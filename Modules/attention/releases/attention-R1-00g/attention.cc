#include "attention.hh"

// Default constructor
Attention::Attention()
{
  //memset(this, 0, sizeof(*this));
  this->ptuCount = 0;
  this->gistCount = 0;
  this->heartCount = 0;
  this->ptuTime = DGCgettime();
  this->gistTime = DGCgettime();
  this->mapTime = DGCgettime();
  this->sweep = true;

  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  DGCcreateMutex(&m_stateMutex);
  DGCcreateMutex(&m_ptuStateMutex);
  DGCcreateMutex(&m_currentCellMutex);
  DGCcreateMutex(&m_desiredCellMutex);
  DGCcreateMutex(&m_emapMutex);

  return;
}

// Default destructor
Attention::~Attention()
{
  DGCdeleteMutex(&m_stateMutex);
  DGCdeleteMutex(&m_ptuStateMutex);
  DGCdeleteMutex(&m_currentCellMutex);
  DGCdeleteMutex(&m_desiredCellMutex);
  DGCdeleteMutex(&m_emapMutex);
  return;
}

// Command line parser
int Attention::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return ERROR("Could not parse command line!");
  
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("attention");
 
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;  

  // Fill out CSS logging options
  if(this->options.debug_flag) 
    this->debug = true;
  else
    this->debug = false;
  this->verbose_level = this->options.verbose_level_arg;
  if(this->options.logging_flag)
    this->logging = true;
  else
    this->logging = false;
  this->log_path = this->options.log_path_arg;
  this->log_level = this->options.log_level_arg;

  // RNDF stuff
  if(this->options.rndf_given)
    this->rndfFilename.assign(this->options.rndf_arg);
  else
    this->rndfFilename = "test.rndf";

  return 0;
}


// Parse the config file
int Attention::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  int tmpVal;

  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, "ATTENTION");

  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Grab map details
  this->numRows = this->options.num_rows_arg;
  this->numCols = this->options.num_cols_arg;
  this->rowRes = this->options.row_resolution_arg;
  this->colRes = this->options.col_resolution_arg;

  tmpVal = (long int)floor(this->options.sens_no_data_value_arg*1000);
  this->sensNoDataVal_emapt = (emap_t)tmpVal;
  this->sensNoDataVal = this->options.sens_no_data_value_arg;

  tmpVal = (long int)floor(this->options.sens_data_value_arg*1000);
  this->sensDataVal_emapt = (emap_t)tmpVal;
  this->sensDataVal = this->options.sens_data_value_arg;

  tmpVal = (long int)floor(this->options.gist_no_data_value_arg*1000);
  this->gistNoDataVal_emapt = (emap_t)tmpVal;
  this->gistNoDataVal = this->options.gist_no_data_value_arg;

  // set the forgetting constant; the larger this value, the slower the cost grows back
  this->timeConstantFast = this->options.time_constant_fast_arg;
  this->timeConstantSlow = this->options.time_constant_slow_arg;

  //currently setting these values to max value
  this->costNoDataVal_emapt = 65535;

  //currently setting these to 0
  this->costNoDataTime_emapt = 0;

  // Grab map details
  this->recvSubGroup = this->options.receive_subgroup_arg;

  return 0;
}


/// Initialize map
int Attention::initMap(const char *configPath, PlanningState::Mode m_planningMode)
{
  // No need for mutexes here because the other thread hasn't started yet


  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;
  
  // grab state first and center map at alice's pose
  while(true)
    {
      if(this->getState() < 0)
	return ERROR("initMap failed.");
      else if(this->getState() > 0)
	continue;
      else if(this->getState() == 0)
	break;
    }

  //--------------------------------------------------
  // Initiliaze worldMap and load prior data
  //--------------------------------------------------
  // Initialize worldMap
  this->worldMapTalker.initRecvMapElement(this->skynetKey, this->recvSubGroup);

  // load rndf
  this->worldMap.data.clear();
  if(!this->worldMap.loadRNDF(this->rndfFilename.c_str()))    
    return ERROR("Error loading rndf!");

  point2 statedelta, stateNE, statelocal;
  statelocal.set(this->state.localX,this->state.localY);
  stateNE.set(this->state.utmNorthing,this->state.utmEasting);
  statedelta = stateNE-statelocal;

  this->worldMap.setTransform(statedelta); 

  if(this->loadMapPriorData()<0)
    return ERROR("Error loading lane lines!");

  //--------------------------------------------------
  // Initialize the attention maps and associated layers
  //--------------------------------------------------
  // EMap initialization
  emapSensor = new EMap(this->numRows);
  this->sensorMapWrapper.initMap(emapSensor, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->sensNoDataVal_emapt);

  emapGist = new EMap(this->numRows);
  this->gistMapWrapper.initMap(emapGist, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->gistNoDataVal_emapt);

  emapCostTime = new EMap(this->numRows);
  this->timeMapWrapper.initMap(emapCostTime, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->costNoDataTime_emapt);

  emapCostVal = new EMap(this->numRows);
  this->costMapWrapper.initMap(emapCostVal, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->costNoDataVal_emapt);

  // set default plannerMode
  this->planningMode = m_planningMode;
  
  // grab timestamp for module process speed
  gettimeofday(&stv, 0);
  this->lasttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  this->currentAttendedCell.x = 0;
  this->currentAttendedCell.y = 0;
  this->currentAttendedCell.cellVal = 0;

  this->desiredAttendedCell.x = 0;
  this->desiredAttendedCell.y = 0;
  this->desiredAttendedCell.cellVal = 0;

  return 0;
}

/// Update the map
int Attention::updateMap(PlanningState::Mode m_planningMode)
{
  // -------------------------------
  // Update state and map locations
  // -------------------------------

  VehicleState m_state;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);

  // grab state first and update vehicle location
  if(this->getState()<0)
    printf("error getting state! \n");

  DGClockMutex(&m_emapMutex);
  this->gistMapWrapper.updateVehicleLoc(emapGist, m_state.localX, m_state.localY, this->gistNoDataVal_emapt);
  this->sensorMapWrapper.updateVehicleLoc(emapSensor, m_state.localX, m_state.localY, this->sensNoDataVal_emapt);
  this->timeMapWrapper.updateVehicleLoc(emapCostTime, m_state.localX, m_state.localY, this->costNoDataTime_emapt);
  this->costMapWrapper.updateVehicleLoc(emapCostVal, m_state.localX, m_state.localY, this->costNoDataVal_emapt);
  DGCunlockMutex(&m_emapMutex);

  // ---------------------------------
  // Update the planning state
  //----------------------------------
  if(m_planningMode!=this->planningMode)
    {
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      this->costMapWrapper.clearMap(emapCostVal, this->costNoDataVal_emapt);
      this->timeMapWrapper.clearMap(emapCostTime, this->costNoDataTime_emapt);
      DGCunlockMutex(&m_emapMutex);      

      this->planningMode = m_planningMode;

      // for nominal driving, uturn, zone, or stop_obs, do sweeping and reset the sweeping mode
      if(this->planningMode==PlanningState::NOMINAL || this->planningMode==PlanningState::DRIVE || this->planningMode==PlanningState::UTURN ||
	 this->planningMode==PlanningState::UTURN || this->planningMode==PlanningState::ZONE || this->planningMode==PlanningState::STOP_OBS)
	{
	  this->sweep = true;
	  this->sweepMode=0;
	}
      else
	{
	  this->sweep = false;
	}

      this->gistTime = DGCgettime();
    }

	
  // load the gistmap layer
  this->loadGistMap(this->planningMode);

  // load the sensor coverage layer
  this->loadSensorCovMap();

  // now fuse the two layers
  this->loadFusedCostMap();

  
  // update heartbeat
  this->heartCount += 1;
  this->mapTime = DGCgettime();
  
  // update current time
  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  // -------------------------------
  // Update the console display
  // -------------------------------
  if(!this->options.disable_console_flag)
    {
      // update process diagnostics
      cotk_printf(this->console, "%hcap%", A_NORMAL, "%5d ",this->heartCount);
      cotk_printf(this->console, "%mcycle%", A_NORMAL, "%+03.2f ", 1/(currenttime - lasttime));


      // update vehicle state display
      if (m_state.timestamp > 0)
	cotk_printf(this->console, "%slat%", A_NORMAL,
		    "%+06dms", (int) (m_state.timestamp - this->mapTime) / 1000);
      
      cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
		  fmod((double) m_state.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%spos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  m_state.localX, m_state.localY, m_state.localZ);
      cotk_printf(this->console, "%srot%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  m_state.localRoll*180/M_PI,
		  m_state.localPitch*180/M_PI,
		  m_state.localYaw*180/M_PI);
      
      DGClockMutex(&m_ptuStateMutex);

      // update ptu state display
      cotk_printf(this->console, "%lpan%", A_NORMAL, "%+03.2f",(double)(this->ptublob.currpan));
      cotk_printf(this->console, "%ltilt%", A_NORMAL, "%+03.2f",(double)(this->ptublob.currtilt));

      DGCunlockMutex(&m_ptuStateMutex);

      // update latency displays
      cotk_printf(this->console, "%plat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->ptuTime) / 1000);
      cotk_printf(this->console, "%glat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->gistTime) / 1000);     

      // update planning mode display
      switch(this->planningMode)
	{
	case PlanningState::NOMINAL:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NOMINAL           ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_LEFT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_LEFT    ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_RIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_RIGHT   ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_STRAIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_STRAIGHT");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::BACKUP:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "BACKUP            ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::DRIVE:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "DRIVE             ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::STOP_OBS:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "STOP_OBS          ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::PASS_LEFT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "PASS_LEFT         ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::PASS_RIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "PASS_RIGHT        ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::UTURN:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "UTURN             ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::ZONE:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "ZONE              ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	}
    }

  this->lasttime = this->currenttime;
  
  return 0;
  
}


///Weight Map with prior info from RNDF
int Attention::loadMapPriorData()
{    
  // No need for mutexes here because this function is only called in the initMap function,
  // which is before the other thread is created
  MapElement tmpEl;
  for(int i=0; i< (int)this->worldMap.prior.data.size(); i++)
    {
      this->worldMap.prior.getElFull(tmpEl,i);
      this->priorMapData.push_back(tmpEl);
    }

  return 0;

}


/// Load the appropriate gist map
/// No need to worry about map mutex locks here; handled above; do need to worry about state mutex though
int Attention::loadGistMap(PlanningState::Mode m_planningMode)
{
  LaneLabel currLaneLabel;
  point2 posePt; 
  double heading;
  double minDist;
  VehicleState m_state;
  LaneLabel otherLaneLabel;
  vector<PointLabel> currLaneExitPtLabels;
  vector<PointLabel> otherLaneEntryPtLabels;
  vector<LaneLabel> opposingLaneLabels;
  vector<PointLabel> oppLaneExitPtLabels;
  point2arr oppLaneCenterLine_up;
  point2arr oppLaneCenterLine_up_dense;
  point2arr otherLaneCenterLine;
  point2arr otherLaneCenterLine_dense;
  double lookUpOppLaneDist = 20;
  double oppLaneUpCost = 10;
  int oppLaneIndex;

  point2_uncertain exitPt;
  point2_uncertain entryPt;
  point2_uncertain oppExitPt;
  bool exitPtFound = false;
  bool entryPtFound = false;
  bool oppLaneExitPtFound = false;

  point2_uncertain exitPt_tmp;
  point2_uncertain entryPt_tmp;
  point2_uncertain lanePerpVec;
  point2_uncertain tmpVec;
  double minDistToExitPt = 9e10;
  double minDistToEntryPt = 9e10;
  bool entryPtIsAhead;
  bool exitPtIsAhead;

  double laneHeading;
  
  // LEFT TURN VARIABLES
  point2arr leftTurnLaneCenterLine_down;
  point2arr leftTurnLaneCenterLine_up;
  point2arr leftTurnLaneCenterLine_down_dense;
  point2arr leftTurnLaneCenterLine_up_dense;
  double lookDownLeftLaneDist = 10;
  double lookUpLeftLaneDist = 20;
  double leftTurnLaneDownCost = 5;
  double leftTurnLaneUpCost = 10;

  // RIGHT TURN VARIABLES
  point2arr rightTurnLaneCenterLine_down;
  point2arr rightTurnLaneCenterLine_up;
  point2arr rightTurnLaneCenterLine_down_dense;
  point2arr rightTurnLaneCenterLine_up_dense;
  double lookDownRightLaneDist = 10;
  double lookUpRightLaneDist = 20;
  double rightTurnLaneDownCost = 5;
  double rightTurnLaneUpCost = 10;

  // STRAIGHT THROUGH INTERSECTIONS
  double lookUpOtherLaneDist = 20;
  double otherLaneCost = 5;

  // PASSING VARIABLES
  bool passingLaneFound;
  bool passLaneIsSameDir;
  LaneLabel passLaneLabel;
  point2arr passLaneCenterLine;
  point2arr passLaneCenterLine_dense;
  point2arr passLaneBehindCenterLine;
  point2arr passLaneAheadCenterLine;
  point2arr passLaneCenterLine_shifted;
  point2 projPosePt, startPt, endPt;
  point2 vPt, lPt;
  double projPoseDist;
  double lookBackDist=20;
  double lookAheadDist=20;
  double projLookBackDist;
  double projLookAheadDist;
  double passLaneHighCost=10;
  double passLaneLowCost = 5;
  double shiftRightDist = 5;
  double shiftLeftDist = 5;

  minDist = 9e10;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);
  
  posePt.x = m_state.localX;
  posePt.y = m_state.localY;
  heading = m_state.localYaw;

  // find my lane first
  this->worldMap.getLane(currLaneLabel,posePt);
  
  if(!this->options.disable_console_flag)
    {
      cotk_printf(this->console, "%seg%", A_NORMAL, "%5d",currLaneLabel.segment);
      cotk_printf(this->console, "%lane%", A_NORMAL, "%5d",currLaneLabel.lane);
    }

  switch(m_planningMode)
    {
    case PlanningState::NOMINAL:
      // for nominal driving, we don't use a gistMap (not yet at least) so just clear old map
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      break;
    case PlanningState::INTERSECT_LEFT:      

      // clear the map
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);


      // get exit and entry waypoint labels
      this->worldMap.getLaneExits(currLaneExitPtLabels, otherLaneEntryPtLabels, currLaneLabel);
      
      // now find the closest exit point that is AHEAD of us
      for(int i=0; i<(int)currLaneExitPtLabels.size(); i++)
	{
	  this->worldMap.getWayPoint(exitPt_tmp, currLaneExitPtLabels[i]);
	  exitPtIsAhead = ((10*cos(heading)*(exitPt_tmp.x-posePt.x)+10*sin(heading)*(exitPt_tmp.y-posePt.y)) > 0);
	  
	  if(exitPtIsAhead && 
	     sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2)) < minDistToExitPt)
	    {
	      minDistToExitPt = sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2));
	      exitPt = exitPt_tmp;
	      exitPtFound = true;
	    }
	}
      
      
      if(exitPtFound)
	{
	  //now generate a vector that is perpendicular to direction of lane at the exit pt
	  this->worldMap.getHeading(laneHeading, exitPt);
	  lanePerpVec.x = 10*cos(laneHeading+M_PI/2); //this is +M_PI/2 because we have an upside down coordinate system
	  lanePerpVec.y = 10*sin(laneHeading+M_PI/2); //this is +M_PI/2 because we have an upside down coordinate system
	  
	  //now find closest entry point to exit pt that allows for a left turn 
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      this->worldMap.getWayPoint(entryPt_tmp, otherLaneEntryPtLabels[i]);
	      if(otherLaneEntryPtLabels[i].segment != currLaneLabel.segment)
		{
		  tmpVec.x = entryPt_tmp.x - exitPt.x;
		  tmpVec.y = entryPt_tmp.y - exitPt.y;
		  
		  entryPtIsAhead = ( (tmpVec.x*lanePerpVec.x + tmpVec.y*lanePerpVec.y) < 0 );
		  
		  if(entryPtIsAhead &&
		     sqrt(pow(tmpVec.x,2) + pow(tmpVec.y,2)) < minDistToEntryPt)
		    {
		      minDistToEntryPt = sqrt(pow(tmpVec.x,2) + pow(tmpVec.y,2));
		      entryPt = entryPt_tmp;
		      entryPtFound = true;
		    }
		}
	    }

	  //now find closest exit pt in oncoming traffic lane	  
	  minDistToExitPt = 9e10;
	  if(this->worldMap.getOppDirLanes(opposingLaneLabels, currLaneLabel)>0)
	    {
	      // now find closest exit point in the opposing lane(s) that is AHEAD of us
	      for(int i=0; i< (int)opposingLaneLabels.size(); i++)
		{
		  this->worldMap.getLaneExits(oppLaneExitPtLabels, otherLaneEntryPtLabels, opposingLaneLabels[i]);

		  // now find the closest exit point in opposing lane that is AHEAD of us
		  for(int j=0; j<(int)oppLaneExitPtLabels.size(); j++)
		    {
		      this->worldMap.getWayPoint(exitPt_tmp, oppLaneExitPtLabels[j]);
		      exitPtIsAhead = ((10*cos(heading)*(exitPt_tmp.x-posePt.x)+10*sin(heading)*(exitPt_tmp.y-posePt.y)) > 0);
		      
		      if(exitPtIsAhead && 
			 sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2)) < minDistToExitPt)
			{
			  minDistToExitPt = sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2));
			  oppExitPt = exitPt_tmp;
			  oppLaneIndex = i;
			  oppLaneExitPtFound = true;
			}	    	   
		    }     
		}
	    }                             	  	  	  
	}
      
      if(exitPtFound && entryPtFound)
	{
	  this->worldMap.getLane(otherLaneLabel,entryPt);
	  this->worldMap.getLaneCenterLine(leftTurnLaneCenterLine_down, otherLaneLabel);
	  this->worldMap.getLaneCenterLine(leftTurnLaneCenterLine_up, otherLaneLabel);
	  leftTurnLaneCenterLine_down.cut_back(entryPt);
	  leftTurnLaneCenterLine_up.cut_front(entryPt);
	  leftTurnLaneCenterLine_up.reverse();

	  densifyLine(leftTurnLaneCenterLine_down, leftTurnLaneCenterLine_down_dense, this->rowRes, lookDownLeftLaneDist);
	  densifyLine(leftTurnLaneCenterLine_up, leftTurnLaneCenterLine_up_dense, this->rowRes, lookUpLeftLaneDist);

	  if(leftTurnLaneCenterLine_down_dense.size()>0)	    
	    drawLaneCost(leftTurnLaneCenterLine_down_dense, leftTurnLaneDownCost);	    
	         
	  if(leftTurnLaneCenterLine_up_dense.size()>0)
	    drawLaneCost(leftTurnLaneCenterLine_up_dense, leftTurnLaneUpCost);	 

	  // now if we found exit point in opposing lane, let's fill that in too
	  if(oppLaneExitPtFound)
	    {
	      this->worldMap.getLane(otherLaneLabel, oppExitPt);
	      this->worldMap.getLaneCenterLine(oppLaneCenterLine_up, otherLaneLabel);
	      oppLaneCenterLine_up.cut_front(oppExitPt);
	      oppLaneCenterLine_up.reverse();

	      densifyLine(oppLaneCenterLine_up,oppLaneCenterLine_up_dense, this->rowRes, lookUpOppLaneDist);
	      
	      if(oppLaneCenterLine_up_dense.size()>0)
		drawLaneCost(oppLaneCenterLine_up_dense, oppLaneUpCost);	      
	      
	    }	  	  
	}
      else	
	WARN("Could not find the left turn lane!");
      
      break; 
      
    case PlanningState::INTERSECT_RIGHT:    
 
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      
      // get exit and entry waypoint labels
      this->worldMap.getLaneExits(currLaneExitPtLabels, otherLaneEntryPtLabels, currLaneLabel);

      // now find the closest exit point that is AHEAD of us
      for(int i=0; i<(int)currLaneExitPtLabels.size(); i++)
	{
	  this->worldMap.getWayPoint(exitPt_tmp, currLaneExitPtLabels[i]);
	  exitPtIsAhead = ((10*cos(heading)*(exitPt_tmp.x-posePt.x)+10*sin(heading)*(exitPt_tmp.y-posePt.y)) > 0);
	  
	  if(exitPtIsAhead && 
	     sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2)) < minDistToExitPt)
	    {
	      minDistToExitPt = sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2));
	      exitPt = exitPt_tmp;
	      exitPtFound = true;
	    }	    	   
	}

      //now find closest entry point to exit pt that allows for a right turn
      if(exitPtFound)
	{
	  //now generate a vector that is perpendicular to direction of lane at the exit pt
	  this->worldMap.getHeading(laneHeading, exitPt);
	  lanePerpVec.x = 10*cos(laneHeading+M_PI/2); //this is +M_PI/2 because we have an upside down coordinate system
	  lanePerpVec.y = 10*sin(laneHeading+M_PI/2); //this is +M_PI/2 because we have an upside down coordinate system

	  //now find closest entry point to exit pt that yields a positive dot product with lanePerpVec	  
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      this->worldMap.getWayPoint(entryPt_tmp, otherLaneEntryPtLabels[i]);
	      if(otherLaneEntryPtLabels[i].segment != currLaneLabel.segment)
		{
		  tmpVec.x = entryPt_tmp.x - exitPt.x;
		  tmpVec.y = entryPt_tmp.y - exitPt.y;
		  
		  entryPtIsAhead = ( (tmpVec.x*lanePerpVec.x + tmpVec.y*lanePerpVec.y) > 0 );
		  
		  if(entryPtIsAhead &&
		     sqrt(pow(tmpVec.x,2) + pow(tmpVec.y,2)) < minDistToEntryPt)
		    {
		      minDistToEntryPt = sqrt(pow(tmpVec.x,2) + pow(tmpVec.y,2));
		      entryPt = entryPt_tmp;
		      entryPtFound = true;
		    }
		}
	    }     
	  

	  //now find closest exit pt in oncoming traffic lane	  
	  minDistToExitPt = 9e10;
	  if(this->worldMap.getOppDirLanes(opposingLaneLabels, currLaneLabel)>0)
	    {
	      // now find closest exit point in the opposing lane(s) that is AHEAD of us
	      for(int i=0; i< (int)opposingLaneLabels.size(); i++)
		{
		  this->worldMap.getLaneExits(oppLaneExitPtLabels, otherLaneEntryPtLabels, opposingLaneLabels[i]);

		  // now find the closest exit point in opposing lane that is AHEAD of us
		  for(int j=0; j<(int)oppLaneExitPtLabels.size(); j++)
		    {
		      this->worldMap.getWayPoint(exitPt_tmp, oppLaneExitPtLabels[j]);
		      exitPtIsAhead = ((10*cos(heading)*(exitPt_tmp.x-posePt.x)+10*sin(heading)*(exitPt_tmp.y-posePt.y)) > 0);
		      
		      if(exitPtIsAhead && 
			 sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2)) < minDistToExitPt)
			{
			  minDistToExitPt = sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2));
			  oppExitPt = exitPt_tmp;
			  oppLaneIndex = i;
			  oppLaneExitPtFound = true;
			}	    	   
		    }     
		}
	    }                             	  	  	  
	}	
	  
      //if exitPt of current lane and entryPt of right-turn-lane is found
      if(exitPtFound && entryPtFound) 
	{	
	  this->worldMap.getLane(otherLaneLabel,entryPt);
	  this->worldMap.getLaneCenterLine(rightTurnLaneCenterLine_down, otherLaneLabel);
	  this->worldMap.getLaneCenterLine(rightTurnLaneCenterLine_up, otherLaneLabel);
	  rightTurnLaneCenterLine_down.cut_back(entryPt);
	  rightTurnLaneCenterLine_up.cut_front(entryPt);	  	  
	  rightTurnLaneCenterLine_up.reverse();

	  densifyLine(rightTurnLaneCenterLine_down, rightTurnLaneCenterLine_down_dense, this->rowRes, lookDownRightLaneDist);
	  densifyLine(rightTurnLaneCenterLine_up, rightTurnLaneCenterLine_up_dense, this->rowRes, lookUpRightLaneDist);
	  
	  if(rightTurnLaneCenterLine_down_dense.size()>0)	    
	    drawLaneCost(rightTurnLaneCenterLine_down_dense, rightTurnLaneDownCost);	  	    	  

	  if(rightTurnLaneCenterLine_up_dense.size()>0)	    	    
	    drawLaneCost(rightTurnLaneCenterLine_up_dense, rightTurnLaneUpCost);


	  // now if we found exit point in opposing lane, let's fill that in too
	  if(oppLaneExitPtFound)
	    {
	      this->worldMap.getLane(otherLaneLabel, oppExitPt);
	      this->worldMap.getLaneCenterLine(oppLaneCenterLine_up, otherLaneLabel);
	      oppLaneCenterLine_up.cut_front(oppExitPt);
	      oppLaneCenterLine_up.reverse();

	      densifyLine(oppLaneCenterLine_up,oppLaneCenterLine_up_dense, this->rowRes, lookUpOppLaneDist);
	      
	      if(oppLaneCenterLine_up_dense.size()>0)
	        drawLaneCost(oppLaneCenterLine_up_dense, oppLaneUpCost);	      
	      
	    }	  	  	   
	}
      else	
	WARN("Could not find the right turn lane!");            

      break;
    case PlanningState::INTERSECT_STRAIGHT: 

      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      // get exit and entry waypoint labels
      this->worldMap.getLaneExits(currLaneExitPtLabels, otherLaneEntryPtLabels, currLaneLabel);

      // now find my own closest exit point that is AHEAD of us
      for(int i=0; i<(int)currLaneExitPtLabels.size(); i++)
	{
	  this->worldMap.getWayPoint(exitPt_tmp, currLaneExitPtLabels[i]);
	  exitPtIsAhead = ((10*cos(heading)*(exitPt_tmp.x-posePt.x)+10*sin(heading)*(exitPt_tmp.y-posePt.y)) > 0);
	  
	  if(exitPtIsAhead && 
	     sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2)) < minDistToExitPt)
	    {
	      minDistToExitPt = sqrt(pow(posePt.x-exitPt_tmp.x,2)+pow(posePt.y-exitPt_tmp.y,2));
	      exitPt = exitPt_tmp;
	      exitPtFound = true;
	    }	    	   
	}

      if(exitPtFound)
	{
	  // want to fill all exit pts ahead of us (within some nominal distance to my own exit pt) up to some nominal cost
	  for(int i=0; i<(int)currLaneExitPtLabels.size(); i++)
	    {
	      this->worldMap.getWayPoint(exitPt_tmp, currLaneExitPtLabels[i]);
	      exitPtIsAhead = ((10*cos(heading)*(exitPt_tmp.x-posePt.x)+10*sin(heading)*(exitPt_tmp.y-posePt.y)) > 0);	  
	      
	      if(exitPtIsAhead && 
		 sqrt(pow(exitPt.x-exitPt_tmp.x,2)+pow(exitPt.y-exitPt_tmp.y,2)) < 25) //within 25m
		{
		  this->worldMap.getLane(otherLaneLabel,exitPt_tmp);
		  this->worldMap.getLaneCenterLine(otherLaneCenterLine, otherLaneLabel);
		  otherLaneCenterLine.cut_front(exitPt_tmp);
		  otherLaneCenterLine.reverse();
		  densifyLine(otherLaneCenterLine, otherLaneCenterLine_dense, this->rowRes, lookUpOtherLaneDist);
		  if(otherLaneCenterLine_dense.size()>0)
		    drawLaneCost(otherLaneCenterLine_dense, otherLaneCost);
		}	    	   
	    }
	}
      else	
	WARN("Could not find exit Pt in lane!");            

      break;
    case PlanningState::PASS_LEFT:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      // find my neighboring lane
      this->worldMap.getNeighborLane(passLaneLabel, currLaneLabel,-1);
      if(passLaneLabel.segment==0 && passLaneLabel.lane==0)
	passingLaneFound = false;	
      else	
	passingLaneFound = true;

      if(passingLaneFound)
	{
	  //check if the passing lane is in the same direction or not
	  passLaneIsSameDir = this->worldMap.isLaneSameDir(currLaneLabel,passLaneLabel);

	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, passLaneLabel);
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  if(!passLaneIsSameDir)	    
	    passLaneCenterLine_dense.reverse();	    

	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);

	  projLookBackDist = projPoseDist-lookBackDist;
	  if(projLookBackDist<0)
	    projLookBackDist=0;

	  projLookAheadDist = projPoseDist+lookAheadDist;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();

	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);

	  passLaneBehindCenterLine = passLaneCenterLine_dense;
	  passLaneBehindCenterLine.cut_back(startPt);
	  passLaneBehindCenterLine.cut_front(projPosePt);

	  passLaneAheadCenterLine = passLaneCenterLine_dense;
	  passLaneAheadCenterLine.cut_back(projPosePt);
	  passLaneAheadCenterLine.cut_front(endPt);

	  if(passLaneIsSameDir)
	    {
	      if(passLaneBehindCenterLine.size()>0)
		drawLaneCost(passLaneBehindCenterLine,passLaneHighCost);
	      if(passLaneAheadCenterLine.size()>0)
		drawLaneCost(passLaneAheadCenterLine,passLaneLowCost);
	    }
	  else
	    {
	      if(passLaneBehindCenterLine.size()>0)
		drawLaneCost(passLaneBehindCenterLine,passLaneLowCost);
	      if(passLaneAheadCenterLine.size()>0)
		drawLaneCost(passLaneAheadCenterLine,passLaneHighCost);	      
	    }	      
	}
      else
       	{	  
	  WARN("could not find passing lane!");	  
	  // if we go out of lane, then we take our current centerline, truncate it and shift 
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, currLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);
	  
	  projLookBackDist = projPoseDist-lookBackDist;
	  if(projLookBackDist<0)
	    projLookBackDist=0;
	  
	  projLookAheadDist = projPoseDist+lookAheadDist;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();
	  
	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);
	  
	  passLaneCenterLine_dense.cut_back(startPt);
	  passLaneCenterLine_dense.cut_front(endPt);
	  passLaneCenterLine_shifted.clear();
	  
	  // now we need to shift this truncated center line to our right
	  for(uint i=0; i<passLaneCenterLine_dense.size(); i++)
	    {
	      //bring pt to vehicle frame
	      vPt.x = cos(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + sin(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      vPt.y = -sin(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + cos(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      
	      //shift it by shiftDist to the right
	      vPt.y = vPt.y + shiftLeftDist;
	      
	      //bring it back to local frame
	      lPt.x = cos(heading)*vPt.x - sin(heading)*vPt.y + posePt.x;
	      lPt.y = sin(heading)*vPt.x + cos(heading)*vPt.y + posePt.y;

	      passLaneCenterLine_shifted.push_back(lPt);

	    }

	  if(passLaneCenterLine_shifted.size()>0)
	    drawLaneCost(passLaneCenterLine_shifted,passLaneHighCost);	 

	}

       
      break;
    case PlanningState::PASS_RIGHT:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      // find my neighboring lane
      this->worldMap.getNeighborLane(passLaneLabel, currLaneLabel,1);
      if(passLaneLabel.segment==0 && passLaneLabel.lane==0)
	passingLaneFound = false;	
      else	
	passingLaneFound = true;

      if(passingLaneFound)
	{
	  //check if the passing lane is in the same direction or not
	  passLaneIsSameDir = this->worldMap.isLaneSameDir(currLaneLabel,passLaneLabel);

	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, passLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  if(!passLaneIsSameDir)	    
	    passLaneCenterLine_dense.reverse();	    

	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);

	  projLookBackDist = projPoseDist-lookBackDist;
	  if(projLookBackDist<0)
	    projLookBackDist=0;

	  projLookAheadDist = projPoseDist+lookAheadDist;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();

	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);

	  passLaneBehindCenterLine = passLaneCenterLine_dense;
	  passLaneBehindCenterLine.cut_back(startPt);
	  passLaneBehindCenterLine.cut_front(projPosePt);

	  passLaneAheadCenterLine = passLaneCenterLine_dense;
	  passLaneAheadCenterLine.cut_back(projPosePt);
	  passLaneAheadCenterLine.cut_front(endPt);

	  if(passLaneIsSameDir)
	    {
	      if(passLaneBehindCenterLine.size()>0)
		drawLaneCost(passLaneBehindCenterLine,passLaneHighCost);
	      if(passLaneAheadCenterLine.size()>0)
		drawLaneCost(passLaneAheadCenterLine,passLaneLowCost);
	    }
	  else
	    {
	      if(passLaneBehindCenterLine.size()>0)
		drawLaneCost(passLaneBehindCenterLine,passLaneLowCost);
	      if(passLaneAheadCenterLine.size()>0)
		drawLaneCost(passLaneAheadCenterLine,passLaneHighCost);	      
	    }	      
	}
      else
	{	  
	  WARN("could not find passing lane!");	  
	  // if we go out of lane, then we take our current centerline, truncate it and shift 
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, currLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);

	  projLookBackDist = projPoseDist-lookBackDist;
	  if(projLookBackDist<0)
	    projLookBackDist=0;

	  projLookAheadDist = projPoseDist+lookAheadDist;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();

	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);

	  passLaneCenterLine_dense.cut_back(startPt);
	  passLaneCenterLine_dense.cut_front(endPt);
	  passLaneCenterLine_shifted.clear();

	  // now we need to shift this truncated center line to our right
	  for(uint i=0; i<passLaneCenterLine_dense.size(); i++)
	    {
	      //bring pt to vehicle frame
	      vPt.x = cos(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + sin(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      vPt.y = -sin(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + cos(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      
	      //shift it by shiftDist to the right
	      vPt.y = vPt.y + shiftRightDist; 

	      //bring it back to local frame
	      lPt.x = cos(heading)*vPt.x - sin(heading)*vPt.y + posePt.x;
	      lPt.y = sin(heading)*vPt.x + cos(heading)*vPt.y + posePt.y;

	      passLaneCenterLine_shifted.push_back(lPt);

	    }

	  if(passLaneCenterLine_shifted.size()>0)
	    drawLaneCost(passLaneCenterLine_shifted,passLaneHighCost);	 

	}

      break;
    case PlanningState::STOP_OBS: 
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      break;
    case PlanningState::DRIVE:      
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      break; 
    case PlanningState::BACKUP:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      break;
    case PlanningState::UTURN:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      break;
    case PlanningState::ZONE:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      break;
    } 
  
  return 0; 
  
} 


int Attention::loadSensorCovMap()
{  
  double theta = 0;
  double theta_next = 0;
  double x, y, R;
  double x_next, y_next;
  int vRow1, vCol1;
  int vRow2, vCol2;
  int vRow3, vCol3;

  VehicleState m_state;

  R = 30;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);

  DGClockMutex(&m_emapMutex);
  this->sensorMapWrapper.clearMap(emapSensor, this->sensNoDataVal_emapt);
  for(int i=0; i<360; i++)
    {
      theta = theta + i*2*M_PI/360;
      theta_next = theta + i*2*M_PI/360;
      x = R*cos(theta)+m_state.localX;
      y = R*sin(theta)+m_state.localY;
      x_next = R*cos(theta_next)+m_state.localX;
      y_next = R*sin(theta_next)+m_state.localY;
      this->sensorMapWrapper.UTM2Win(emapSensor, x, y, &vRow1, &vCol1);
      this->sensorMapWrapper.UTM2Win(emapSensor, m_state.localX, m_state.localY, &vRow2, &vCol2);
      this->sensorMapWrapper.UTM2Win(emapSensor, x_next, y_next, &vRow3, &vCol3);

      this->sensorMapWrapper.fillTriangle(emapSensor, vCol1, vRow1, vCol2, vRow2, vCol3, vRow3, this->sensDataVal_emapt);
    }
  DGCunlockMutex(&m_emapMutex);

  return 0;
}

// fuses the two layers and finds the max cell (UTM) at the same time
int Attention::loadFusedCostMap()
{
  double sensVal;
  emap_t sensVal_emapt;
  double gistVal;
  emap_t gistVal_emapt;
  double fusedVal_cost;
  emap_t fusedVal_cost_emapt;
  emap_t fusedVal_time_emapt;
  double last_fusedVal_cost;
  emap_t last_fusedVal_cost_emapt;
  emap_t last_fusedVal_time_emapt;

  double m_maxCostval = 0;
  double m_maxCostx;
  double m_maxCosty;

  double tau;
  double deltaT;
  double newCost, desiredCost;
  int m_maxRow = 0; 
  int m_maxCol = 0;
  double cellX,cellY;
  int tmpVal;

  DGClockMutex(&m_emapMutex);
  for(int i=0; i < (this->numRows); i++)
    {
      for(int j=0; j < (this->numCols); j++)
	{
	  this->sensorMapWrapper.getDataWin(emapSensor, i, j, &sensVal_emapt);
	  this->gistMapWrapper.getDataWin(emapGist, i, j, &gistVal_emapt);
	  this->timeMapWrapper.getDataWin(emapCostTime, i, j, &last_fusedVal_time_emapt);
	  this->costMapWrapper.getDataWin(emapCostVal, i, j, &last_fusedVal_cost_emapt);	
	  this->costMapWrapper.Win2UTM(emapCostVal, i, j, &cellX, &cellY);

	  sensVal = (double)sensVal_emapt/1000;
	  gistVal = (double)gistVal_emapt/1000;
	  last_fusedVal_cost = (double)last_fusedVal_cost_emapt/1000;

	  desiredCost = sensVal*gistVal;

	  // we should really only pay attention to the overlap of the gistmap and the sensCov map

	  // we should really only grow a cell that's been touched by the PTU

	  //first : if it's a simple map shift, no need to grow
	  //second: otherwise check if cell was just cleared due to new directive
	  //third : otherwise checkif cell was just pushed down, then grow
	  //last  : otherwise give it the desired cost 
	  // if our sensor coverage has moved, no need to grow, just augment	 

	  if( (last_fusedVal_cost==0 || last_fusedVal_cost_emapt==65535) &&
	     desiredCost!=(this->sensNoDataVal*this->gistNoDataVal) &&
	     desiredCost!=(this->sensDataVal*this->gistNoDataVal))	     
	    {		 
	      // we must have moved the map into a new area
	      fusedVal_cost = desiredCost;
	      fusedVal_time_emapt = 65535;
	    }
	  else if(desiredCost==(this->sensNoDataVal*this->gistNoDataVal) ||
		  desiredCost==(this->sensDataVal*this->gistNoDataVal))
	    {
	      fusedVal_cost = 0;
	      fusedVal_time_emapt = 65535;
	    }
	  else
	    {
	      tau = this->timeConstantFast;
	      deltaT = (double)last_fusedVal_time_emapt;
	      newCost = desiredCost*(1-exp(-deltaT/tau)) + 0.01;
	      
	      if(fabs(desiredCost-newCost)<0.25)
		{
		  fusedVal_cost = desiredCost;
		  fusedVal_time_emapt = 65535;
		}
	      else
		{
		  fusedVal_cost = newCost;
		  if(last_fusedVal_time_emapt==65535)
		    fusedVal_time_emapt = 65535;
		  else
		    fusedVal_time_emapt = last_fusedVal_time_emapt + 1;
		}      
	    }
	  
	  // now update what the max cell is
	  if(fusedVal_cost >= m_maxCostval)
	    {
	      m_maxCostval = fusedVal_cost;
	      m_maxRow = i;
	      m_maxCol = j;
	    }	        	  

	  tmpVal = (long int)floor(fusedVal_cost*1000);
	  fusedVal_cost_emapt = (emap_t)tmpVal;
	  this->timeMapWrapper.setDataWin(this->emapCostTime, i, j, fusedVal_time_emapt);
	  this->costMapWrapper.setDataWin(this->emapCostVal, i, j, fusedVal_cost_emapt);  

	}
    }

  this->costMapWrapper.Win2UTM(this->emapCostVal, m_maxRow, m_maxCol, &m_maxCostx, &m_maxCosty);
  this->maxCostCell.x = m_maxCostx;
  this->maxCostCell.y = m_maxCosty;
  this->maxCostCell.cellVal = m_maxCostval;
  
  DGCunlockMutex(&m_emapMutex);  

  return(0);

}


/// Draws a block in the specified lane up to some following distance along a provided heading
/// -- if length is too long, then it gets truncated at end of lane segment
void Attention::drawBlockInLane(LaneLabel m_laneLabel, double blockHeight, point2 startPt, double heading, double length)
{
  point2 tmpPt;  
  point2 tmpPt_next;
  double fillDist;
  double tmpDist;
  int winRow, winCol;
  point2 lanePt;
  int tmpVal;

  fillDist = 0;

  tmpPt = startPt;

  DGClockMutex(&m_emapMutex);
  while(true)
    {
      for(int i=-2; i<3; i++)
	{
	  for(int j=-2; j<3; j++)
	    {
	      lanePt.x = tmpPt.x + i*(this->rowRes);
	      lanePt.y = tmpPt.y + j*(this->colRes);
	      tmpDist = sqrt(pow(startPt.x-lanePt.x,2)+pow(startPt.y-lanePt.y,2));

	      if( this->worldMap.isPointInLane(lanePt, m_laneLabel) &&
		  (10*cos(heading)*(lanePt.x-startPt.x)+10*sin(heading)*(lanePt.y-startPt.y))>0)
		{
		  this->gistMapWrapper.UTM2Win(emapGist, lanePt.x, lanePt.y, &winRow, &winCol);
		  tmpVal = (long int)floor(blockHeight*1000);		  
		  this->gistMapWrapper.setDataWin(emapGist, winRow, winCol, (emap_t)tmpVal);
		  
		  if(tmpDist>fillDist)
		    {
		      fillDist = tmpDist;
		      tmpPt_next = lanePt;
		    }
		  
		}	  
	    }
	}

      tmpPt = tmpPt_next;

      
      if( fabs(length-fillDist)< (this->rowRes) )
	break;

    }
  DGCunlockMutex(&m_emapMutex);
  
}


/// Draws a block in the lane given the lane center line, the cost, and the distance along the lane
void Attention::drawLaneCost(const point2arr &m_laneCenterLine, double m_cost)
{
  int winRow, winCol;
  int tmpVal;

  if(m_laneCenterLine.size()>0)
    {
      DGClockMutex(&m_emapMutex);
      for(int i=0; i < (int)m_laneCenterLine.size(); i++ )
	{
	  this->gistMapWrapper.UTM2Win(emapGist, m_laneCenterLine[i].x, m_laneCenterLine[i].y, &winRow, &winCol);
	  if(winRow>0 && winCol>0)
	    {	      
	      tmpVal = (long int)floor(m_cost*1000);
	      this->gistMapWrapper.setDataWin(emapGist, winRow, winCol, (emap_t)tmpVal);
	      //DEBUG
	      //printf("x: %f  y: %f  winRow: %d  winCol: %d  tmpVal: %d\n", m_laneCenterLine[i].x, m_laneCenterLine[i].y, winRow, winCol, tmpVal);
	    }
	  else
	    {
	      //DEBUG
	      //printf("x: %f  y: %f  winRow: %d  winCol: %d\n", m_laneCenterLine[i].x, m_laneCenterLine[i].y, winRow, winCol);
	    }
	}
      DGCunlockMutex(&m_emapMutex);
    }

  return;
}


/// Densifies the given line to the given resolution up to a distance along that line
/// --- if the distance is longer than the line, then it just goes to the end of the array
void Attention::densifyLine(const point2arr &ptarr, point2arr &newptarr, double res, double distance)
{
  double actualDistance;
  double ptHeading;
  point2 nextPt;

  newptarr.clear();
  newptarr.push_back(ptarr[0]);

  //check to make sure the desired distance is not longer than the measured length of the line
  if(distance>=ptarr.linelength())
    actualDistance = ptarr.linelength();
  else
    actualDistance = distance;

  //now densify the line
  for(int i=0; i<(int)(ptarr.size()-1); i++)
    {
      // if next pt is within res distance, add it to the vector list
      if(ptarr[i].dist(ptarr[i+1])<res)	
	{

	  newptarr.push_back(ptarr[i+1]);
	  //check distance
	  if(newptarr.linelength()>=actualDistance)
	    return;
	}
      else
	{
	  nextPt = ptarr[i];
	  ptHeading = atan2(ptarr[i+1].y-ptarr[i].y,ptarr[i+1].x-ptarr[i].x);
	  for(int j=0; j<(long int)floor(ptarr[i].dist(ptarr[i+1])/res); j++)
	    {
	      nextPt.x = res*cos(ptHeading)+nextPt.x;
	      nextPt.y = res*sin(ptHeading)+nextPt.y;

	      newptarr.push_back(nextPt);	      
	      //check distance
	      if(newptarr.linelength()>=actualDistance)
		return;

	    }

	  newptarr.push_back(ptarr[i+1]);
	  //check distance
	  if(newptarr.linelength()>=actualDistance)
	    return;
	}
    }
  return;
  
}

/// Finds the intersection of the Line of Site vector with the ground plane
int Attention::findLineOfSiteIntersect(float *x, float *y, float *z)
{
  vec3_t pointInPlane;
  vec3_t normalToPlane;
  vec3_t linePt1, linePt2;
  float px,py,pz;
  float vx,vy,vz;
  float lx,ly,lz;

  float d,t;
  float numerator, denominator;

  PTUStateBlob *m_ptublob;
  VehicleState m_state;

  DGClockMutex(&this->m_ptuStateMutex);
  m_ptublob = &this->ptublob;
  DGCunlockMutex(&this->m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);

  PTUStateBlobToolToPTU(m_ptublob,0,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(m_ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(m_ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt1 = vec3_set(lx,ly,lz);
  
  PTUStateBlobToolToPTU(m_ptublob,50,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(m_ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(m_ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt2 = vec3_set(lx,ly,lz);

  pointInPlane = vec3_set(m_state.localX, m_state.localY, m_state.localZ+VEHICLE_TIRE_RADIUS);
  normalToPlane = vec3_set(0, 0, 1);

  d = vec3_dot(pointInPlane, normalToPlane);
  
  denominator = normalToPlane.x*(linePt2.x-linePt1.x) + normalToPlane.y*(linePt2.y-linePt1.y) + normalToPlane.z*(linePt2.z-linePt1.z);
  if(denominator==0) // the intersection does not exist    
    return(-1);
  else
    {
      numerator = d - normalToPlane.x*linePt1.x - normalToPlane.y*linePt1.y - normalToPlane.z*linePt1.z;
      t = numerator/denominator;
      *x = linePt1.x + t*(linePt2.x - linePt1.x);
      *y = linePt1.y + t*(linePt2.y - linePt1.y);
      *z = linePt1.z + t*(linePt2.z - linePt1.z);
      return 0;
    }
}


/// Initialize sensnet 
int Attention::initSensnet()
{  
  // Initialize SensNet 
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, MODattention) != 0) 
    return ERROR("unable to connect to sensnet");
  
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to the PTU state messages
  if (sensnet_join(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(this->ptublob)) != 0)
    return ERROR("unable to join PTU state group");

  // Subscribe to process state messages
  if(!this->options.disable_process_control_flag)
    {
      if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
	return ERROR("unable to join process group");
    }

  return 0;
}

/// Finalize sensnet
int Attention::finiSensnet()
{
  // Leave the state group
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);

  // Leave the PTU state group 
  sensnet_leave(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB);

  // Leave the ProcessControl group
  if(!this->options.disable_process_control_flag)
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);

  // Disconnect
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}

/// Initialize skynet
int Attention::initSkynet()
{
  // Initialize sockets for receiving gistmap and sending ptucommands
  this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);

  this->ptuCommandSocket = this->m_skynet->get_send_sock(SNptuCommand);
  if(this->ptuCommandSocket < 0)
    return ERROR("attention::initSkynet(): skynet get_send_sock returned error");

  return 0;
}

/// Finalise skynet
int Attention::finiSkynet()
{
  delete this->m_skynet;
  return 0;
}

/// Do the attending based on current map
int Attention::attend()
{
  float localx, localy, localz;
  int m_row, m_col;
  int tmpRow, tmpCol;
  double m_panspeed, m_tiltspeed;
  double timeElapsed;
  double distToMaxCell;
  VehicleState m_state;

  DGClockMutex(&m_ptuStateMutex);
  m_panspeed = this->ptublob.currpanspeed;
  m_tiltspeed = this->ptublob.currtiltspeed;
  DGCunlockMutex(&m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);
  
  if(sweep)
    {
      switch(this->sweepMode)
	{
	case 0:
	  this->command.type = RAW;
	  this->command.pan = 0;
	  this->command.tilt = -5;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;
	  break;
	case 1:
	  this->command.type = RAW;
	  this->command.pan = -60;
	  this->command.tilt = -20;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;
	  break;
	case 2:
	  this->command.type = RAW;
	  this->command.pan = 0;
	  this->command.tilt = -5;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;	  
	  break;
	case 3:
	  this->command.type = RAW;
	  this->command.pan = 60;
	  this->command.tilt = -20;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;	  
	  break;
	default:	  
	  this->command.type = RAW;
	  this->command.pan = 0;
	  this->command.tilt = -5;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;
	  break;
	}

      // find cell location of current line of site
      if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
	{
	  DGClockMutex(&m_currentCellMutex);
	  this->currentAttendedCell.x = localx;
	  this->currentAttendedCell.y = localy;
	  this->currentAttendedCell.cellVal = 0;
	  DGCunlockMutex(&m_currentCellMutex);
	}
      
      // since the nominal driving is a fixed set of sweeping modes, we'll 
      // just send the attended cell to plot to be where alice is
      DGClockMutex(&m_desiredCellMutex);
      this->desiredAttendedCell.x = m_state.localX;
      this->desiredAttendedCell.y = m_state.localY;
      this->desiredAttendedCell.cellVal = 0;
      DGCunlockMutex(&m_desiredCellMutex);	  
      
      timeElapsed = this->currenttime - (double)(this->ptuTime)/1000000;

      if(m_panspeed == 0 && m_tiltspeed == 0 && timeElapsed > 2.5)
	{	  	  
	  // Send PTU command
	  if(this->sendPTUcommand(this->command) <= 0)
	    return ERROR("PTU command not sent!");

	  this->sweepMode++;
	  if(this->sweepMode==4)
	    this->sweepMode=0;
	  
	  // Update ptu command count
	  this->ptuCount += 1;  
	  this->ptuTime = DGCgettime();
	  
	  if(this->console)
	    {
	      cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	      cotk_printf(this->console, "%plos%", A_NORMAL, "sweepMode %d",
			  this->sweepMode);
	    }      
	}            
    }
  else
    {
      this->command.type = LINEOFSITE;
      this->command.localx = this->maxCostCell.x;
      this->command.localy = this->maxCostCell.y;
      this->command.localz = m_state.localZ+VEHICLE_TIRE_RADIUS;
      this->command.panspeed = 25;
      this->command.tiltspeed = 10;    
            
      //DEBUG
      //printf("this->maxCostCell.x: %f    y: %f \n", this->maxCostCell.x, maxCostCell.y);
      
      if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
	{
	  DGClockMutex(&m_currentCellMutex);
	  this->currentAttendedCell.x = localx;
	  this->currentAttendedCell.y = localy;
	  this->currentAttendedCell.cellVal = 0;
	  DGCunlockMutex(&m_currentCellMutex);
	  
	  DGClockMutex(&m_emapMutex);
	  this->costMapWrapper.UTM2Win(this->emapCostVal,(double)localx, (double)localy, &m_row, &m_col);
	  
	  for(int i=-3; i<4; i++)
	    {
	      for(int j=-3; j<4; j++)
		{
		  tmpRow = m_row+i;
		  tmpCol = m_col+j;
		  
		  if(tmpRow >= this->numRows-1)
		    tmpRow = this->numRows-1;
		  if(tmpRow <= 0)
		    tmpRow = 0;
		  
		  if(tmpCol >= this->numCols-1)
		    tmpCol = this->numCols-1;
		  if(tmpCol <= 0)
		    tmpCol = 0;
		  
		  this->costMapWrapper.setDataWin(this->emapCostVal, tmpRow, tmpCol, 10);
		  this->timeMapWrapper.setDataWin(this->emapCostTime, tmpRow, tmpCol, 0);
		}
	    }
	  DGCunlockMutex(&m_emapMutex);
	  
	  distToMaxCell = sqrt(pow(this->maxCostCell.x-localx,2) + pow(this->maxCostCell.y-localy,2));
	  timeElapsed = this->currenttime - (double)(this->ptuTime)/1000000;
	  
	  //if(distToMaxCell < 0.5 && timeElapsed > 1) //wait at least two seconds between sending commands
	  if(distToMaxCell < 0.25 && timeElapsed > 0.5)
	    {      
	      DGClockMutex(&m_desiredCellMutex);
	      this->desiredAttendedCell.x = this->command.localx;
	      this->desiredAttendedCell.y = this->command.localy;
	      this->desiredAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_desiredCellMutex);	  
	      
	      // Send PTU command
	      if(this->sendPTUcommand(this->command) <= 0)
		return ERROR("PTU command not sent!");
	      
	      // Update ptu command count
	      this->ptuCount += 1;  
	      this->ptuTime = DGCgettime();
	      
	      if(this->console)
		{
		  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
		  cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
			      this->command.localx, this->command.localy, this->command.localz);
		}      
	    }      
	  
	  // this is if it gets stuck trying to get an infeasible cell location
	  if(m_panspeed == 0 && m_tiltspeed == 0 && timeElapsed > 1)
	    {
	      DGClockMutex(&m_desiredCellMutex);
	      this->desiredAttendedCell.x = this->command.localx;
	      this->desiredAttendedCell.y = this->command.localy;
	      this->desiredAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_desiredCellMutex);	  
	      
	      // Send PTU command
	      if(this->sendPTUcommand(this->command) <= 0)
		return ERROR("PTU command not sent!");
	      
	      // Update ptu command count
	      this->ptuCount += 1;  
	      this->ptuTime = DGCgettime();
	      
	      if(this->console)
		{
		  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
		  cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
			      this->command.localx, this->command.localy, this->command.localz);
		}      
	    }      
	}
    }
  
  return 0;
}

/// Get Alice and PTU state
int Attention::getState()
{
  int blobId;
  int ptublobId;

  DGClockMutex(&m_stateMutex);
  memset(&this->state, 0, sizeof(this->state));  
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state data");
  DGCunlockMutex(&m_stateMutex);

  if (blobId <= 0)
    return WARN("state is invalid: ignoring");
  

  DGClockMutex(&m_ptuStateMutex);
  memset(&this->ptublob, 0, sizeof(this->ptublob));
  if (sensnet_read(this->sensnet, SENSNET_MF_PTU, 
		   SENSNET_PTU_STATE_BLOB, &ptublobId, 
		   sizeof(this->ptublob), &this->ptublob) != 0)
    return ERROR("Could not read ptu state");
  DGCunlockMutex(&m_ptuStateMutex);

  return 0;
}


// Get the process state
int Attention::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


/// Send PTU command
int Attention::sendPTUcommand(PTUCommand p_command)
{
  int numBytesSent;
  numBytesSent = m_skynet->send_msg(this->ptuCommandSocket, &p_command, sizeof(p_command), 0);
  return numBytesSent;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"Attention $Revision$                MODULE RATE                            \n"
"                                    Hz: %mcycle%                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"ALICE STATE                         PTU STATE                              \n"
"Time    : %stime%                   rawpan  (deg): %lpan%                  \n"
"Pos     : %spos%                    rawtilt (deg): %ltilt%                 \n"
"Rot     : %srot%                                                           \n"
"Latency : %slat%                                                           \n"
"                                                                           \n"
"PLANNING MODE                       PTU COMMANDS                           \n"
"Mode      : %gmode%                 Number sent  : %pcap%                  \n"
"Num recvd : %gcap%                  Command      : %plos%                  \n"
"Latency   : %glat%                  Latency      : %plat%                  \n"
"                                                                           \n" 
"                                                                           \n" 
"                                                                           \n" 
"HEARTBEAT : %hcap%                  CURRENT LANE                           \n" 
"                                    Segment:  %seg%                        \n" 
"                                    Lane:     %lane%                       \n" 
"                                                                           \n" 
"%stderr%                                                                   \n" 
"%stderr%                                                                   \n" 
"%stderr%                                                                   \n" 
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n"; 
  
// Initialize console display
int Attention::initConsole()
{   
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  if (cotk_open(this->console, "attention.msg") != 0)
    return -1;
  
  // Display spread id
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
 
  // Display some initial values
  cotk_printf(this->console, "%gcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%pcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NO MODE        ");
  return 0;
}

// Finalize console display
int Attention::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}

/// Console button callback
int Attention::onUserQuit(cotk_t *console, Attention *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

/// Console button callback
int Attention::onUserPause(cotk_t *console, Attention *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

void idle(void* param)
{ 

  Attention* self = (Attention*) param;

  CellData tempData;
  CellData tmpCurrAttCell, tmpGoalAttCell;

  VehicleState m_state;
  PTUStateBlob m_ptublob;

  emap_t gistVal_emapt;
  emap_t sensVal_emapt;
  emap_t costVal_emapt;


  DGClockMutex(&self->m_stateMutex);
  m_state = self->state;
  DGCunlockMutex(&self->m_stateMutex);

  DGClockMutex(&self->m_ptuStateMutex);
  m_ptublob = self->ptublob;
  DGCunlockMutex(&self->m_ptuStateMutex);

  self->sensvec.clear();
  self->gistvec.clear();
  self->costvec.clear();

  DGClockMutex(&self->m_emapMutex);
  for(int i=0; i<self->numRows; i++)
    {
      for(int j=0; j<self->numCols; j++)
	{
	  self->sensorMapWrapper.Win2UTM(self->emapSensor, i, j, &tempData.x, &tempData.y);
	  self->sensorMapWrapper.getDataWin(self->emapSensor, i, j, &sensVal_emapt);
	  tempData.cellVal = ((double)sensVal_emapt)/1000;
	  self->sensvec.push_back(tempData);


	  self->gistMapWrapper.Win2UTM(self->emapGist, i, j, &tempData.x, &tempData.y);
	  self->gistMapWrapper.getDataWin(self->emapGist, i, j, &gistVal_emapt);
	  tempData.cellVal = ((double)gistVal_emapt)/1000;
	  self->gistvec.push_back(tempData);

	  self->costMapWrapper.Win2UTM(self->emapCostVal, i, j, &tempData.x, &tempData.y);
	  self->costMapWrapper.getDataWin(self->emapCostVal, i, j, &costVal_emapt);
	  if(costVal_emapt==65535)
	    costVal_emapt = 0;
	  tempData.cellVal = ((double)costVal_emapt)/1000;
	  
	  self->costvec.push_back(tempData);
	}
    }  
  DGCunlockMutex(&self->m_emapMutex);

  DGClockMutex(&self->m_currentCellMutex);
  tmpCurrAttCell.x = self->currentAttendedCell.x;
  tmpCurrAttCell.y = self->currentAttendedCell.y;
  tmpCurrAttCell.cellVal = self->currentAttendedCell.cellVal;
  DGCunlockMutex(&self->m_currentCellMutex);

  DGClockMutex(&self->m_desiredCellMutex);
  tmpGoalAttCell.x = self->desiredAttendedCell.x;
  tmpGoalAttCell.y = self->desiredAttendedCell.y;
  tmpGoalAttCell.cellVal = self->desiredAttendedCell.cellVal;
  DGCunlockMutex(&self->m_desiredCellMutex);

  // update the map
  self->worldwin->update(m_state, m_ptublob, self->sensvec, self->gistvec, self->costvec, self->priorMapData, tmpCurrAttCell, tmpGoalAttCell);

}

void Attention::initVisualizer(int *argc, char** argv)
{
  glutInit(argc, argv);
}

/// Start visualizer thread
void Attention::v_startVisualizerThread()
{
  int cols = 1024;
  int rows = 768;

  Fl_Menu_Item menuitems[] =
    {
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"&Pause", FL_CTRL + 'p', (Fl_Callback*) Attention::v_onAction, (void*) 0x1000, FL_MENU_TOGGLE},
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) Attention::v_onExit},
      {0},
      {0},     
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Attention Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new WorldWin(0, 30, cols, rows - 30, 30);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Initialize image window
  // -- no need for mutex around options because it's not used after this call in other thread
  this->worldwin->init(this->options.col_resolution_arg, this->options.row_resolution_arg);

  // Idle callback
  Fl::add_idle(idle, this);

  // Run
  this->mainwin->show();
  while (!this->quit)
    Fl::wait();
   
  return;

}

// Handle menu callbacks
void Attention::v_onExit(Fl_Widget *w, int option)
{
  Attention *self;

  self = (Attention*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void Attention::v_onAction(Fl_Widget *w, int option)
{
  Attention *self;  

  self = (Attention*) w->user_data();
  if (option == 0x1000)
    self->pause = !self->pause;
  
  return;
}
