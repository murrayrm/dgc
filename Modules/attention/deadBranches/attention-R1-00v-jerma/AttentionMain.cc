#include "AttentionModule.hh"
//#include "dgcutils/DGCutils.hh"
//#include "dgcutils/cfgfile.h"

int main(int argc, char **argv)              
{  

  /* Initializes Planner and start the gcmodule */
  AttentionModule* attention = new AttentionModule();
  attention->initialize(argc, argv);
  attention->Start();
  /* Refreshing the console */
  unsigned int cnt = 0;
  while (true) {
    sleep(5);

     /* Refresh console every 100 changes
      * Refreshing every changes would create flickers on the
      * terminal (not very nice for the eye) */

     cnt++;
     if (cnt > 100) {
       cnt = 0;
     }
  }

  return 0;
}
