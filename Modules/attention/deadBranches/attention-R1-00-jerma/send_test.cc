#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <interfaces/GistMapMsg.h>

using namespace std;

int main(int argc, char* argv[])
{
 
  int modeCommandSocket;
  int sendSuccess;
  int type;
  PlanningMode modeCommand;
 
  skynet m_skynet = skynet(217, atoi(getenv("SKYNET_KEY")), NULL); 
  modeCommandSocket = m_skynet.get_send_sock(SNgist);
  if(modeCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }

  while(true)
    {

      cout << "What would you like to do? " << endl;
      cout << "0: LEFT_TURN" << endl;
      cout << "1: RIGHT_TURN " << endl;
      cout << "2: LEFT_LANE_CHANGE" << endl;
      cout << "3: RIGHT_LANE_CHANGE" << endl;
      cout << "4: FOLLOWING" << endl;
      cout << "5: U_TURN" << endl;
      cout << "6: CRUISE" << endl;
      cin >> type;

      if(type==0)
	{
	  modeCommand = LEFT_TURN;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==1)
	{
	  modeCommand = RIGHT_TURN;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==2)
      	{
	  modeCommand = LEFT_LANE_CHANGE;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==3)
	{
	  modeCommand = RIGHT_LANE_CHANGE;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==4)
	{
	  modeCommand = FOLLOWING;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==5)
	{
	  modeCommand = U_TURN;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==6)
	{
	  modeCommand = CRUISE;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else
	printf("Unrecognized type. Please try again\n");
    }
      
  return(0);
}
