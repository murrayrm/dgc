#include <assert.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <iostream>

// dgc basic header files
#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/PTUStateBlob.h>
#include <interfaces/PTUCommand.h>
#include <interfaces/GistMapMsg.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <vector>
#include <emap/emap.hh>


// CMap header file
#include <cmap/CMapPlus.hh>

// Map header file
#include <map/Map.hh>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>

// Frames header files
#include <frames/pose3.h>
#include <frames/mat44.h>

// cotk display
#include <cotk/cotk.h>

// custom header files
#include "cmdline.h"
#include "WorldWin.hh"


// Bad macro defined
#undef border

// for visualizer
#include <GL/glut.h>
#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <jplv/Fl_Glv_Window.H>
//#include <jplv/jplv_image.h>

// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

using namespace std;

struct attentionCost{
  double cost;
  double timestamp;
};

class Attention
{
public:

  /// Default constructor
  Attention();
  
  /// Default destructor
  ~Attention();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize attention cmap
  int initMap(const char *configPath);

  /// Grab lane lines from RNDF
  int loadMapPriorData();

  /// Update the map
  int updateMap();

  /// load appropriate gist map based on directive
  int loadGistMap(PlanningMode m_planningMode);
 
  /// load sensor coverage map
  int loadSensorCovMap(vector<NEcoord> m_sensCovPoly);

  /// load fused cost map
  int loadFusedCostMap();

  /// Fills the adjacent lane specified by m_laneLabel with cost up to some look behind distance 
  /// --- var is used as a blending of the cost
  void fillLaneUpToDistBehind(LaneLabel m_laneLabel, double m_lookBehindDist, point2 m_posePt, double m_var);

  /// Draws a gaussian at a specified point
  void drawGaussianAtPt(point2 gPt, double m_var);

  /// Draws a gaussian in the specified lane at some distance from where vehicle is
  /// --- if dist is negative, we draw the gaussian behind us;
  void drawGaussianInLane(LaneLabel m_laneLabel, VehicleState m_state, double m_lookDist, double var, double weight, bool behind);

  /// Finds the intersection of the Line of Site vector with the ground plane
  int findLineOfSiteIntersect(float *x, float *y, float *z);

 /// Initialize sensnet -- will need sensnet for PTU state
  int initSensnet();

  /// Finalize sensnet -- will need sensnet for PTU state
  int finiSensnet();

  /// Initialize skynet -- will need skynet for receiving gist and sending ptucommand
  int initSkynet();

  /// Finalise skynet -- will need skynet for receiving gist and sending ptucommang
  int finiSkynet();

  /// Do the attending based on current map
  int attend();

  /// Get alice and ptu state
  int getState();

  /// Get process state
  int getProcessState();

  /// Send PTU command
  int sendPTUcommand(PTUCommand command);
  
  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, Attention *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, Attention *self, const char *token);

  /// Initialize visualizer
  void initVisualizer(int *argc, char** argv);

  /// Visualization thread
  void v_startVisualizerThread();

  // Action callback
  static void v_onAction(Fl_Widget *w, int option);
  
  // Exit callback
  static void v_onExit(Fl_Widget *w, int option);
  
public:

  // World map -- vector based
  Map worldMap;

  // Map talker for worldMap
  CMapElementTalker worldMapTalker;
  
  // receive subgroup
  int recvSubGroup;

  // Attention map
  CMap attentionMap;

  EMap *emap;

  // Number of rows
  int numRows;
  
  // Number of cols
  int numCols;

  // Resolution of rows in map
  double rowRes;
  
  // Resolution of columns in map
  double colRes;

  // No data value for sensor layer
  double sensOutsideMapVal;
  
  // Outside map value for sensor layer
  double sensNoDataVal;

  // No data value for gist layer
  double gistOutsideMapVal;
  
  // Outside map value for gist layer
  double gistNoDataVal;

  // Sensor coverage layer id
  int sensCovLayerId;
  
  // Gist layer id
  int gistLayerId;

  // Cost layer id
  int costLayerId;

  // Program options
  gengetopt_args_info options;

  // Default configuration path
  char *defaultConfigPath;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // rndf filename
  string rndfFilename;

  // Our module id (SkyNet)
  modulename moduleId;

  // SensNet handle
  sensnet_t *sensnet;
  
  // Console text display
  cotk_t *console;

  // Current vehicle state data
  VehicleState state;
  
  // Skynet object
  skynet *m_skynet;

  // command Socket for commanding the PTU
  int ptuCommandSocket;

  // socket for receiving gistLayer from planner
  int gistSocket;
  int gistReceived;
  PlanningMode planningMode;

  // Counter for sent PTU commands
  int ptuCount;

  // Counter for received gist commands
  int gistCount;

  // Counter for heartbeat
  int heartCount;

  // Time stamp for last received gist command
  uint64_t gistTime;

  // Time stamp for last sent ptu command
  uint64_t ptuTime;

  // Time stamp for latency computation of one map update
  uint64_t mapTime;

  // command to PTU
  PTUCommand command; // this is the outgoing command

  // PTU blob for ptu state
  PTUStateBlob ptublob;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Top-level window
  Fl_Window *mainwin;
  
  // Top-level menu
  Fl_Menu_Bar *menubar;

  // 3D window
  WorldWin *worldwin;

  vector<CellData> sensvec;
  
  vector<CellData> gistvec;

  vector<CellData> costvec;

  vector<CellData> attentionCellVec;

  vector<NEcoord> sensCovPoly;

  vector<MapElement> priorMapData;

  //TODO: REMOVE
  NEcoord tmpCoord;
  vector<double> theta;

  point2arr_uncertain centerLine;

  vec3_t maxCostPt;

  // a forgetting constant; the larger the value, the slower the cost grows back
  double tau; 

public:
 
  pthread_mutex_t m_mapMutex;

  pthread_mutex_t m_stateMutex;

  pthread_mutex_t m_ptuStateMutex;

  pthread_mutex_t m_attentionCellMutex;

private:
  
  struct timeval stv;

  double lasttime;

  double currenttime;

};

// Default constructor
Attention::Attention()
{
  //memset(this, 0, sizeof(*this));
  this->ptuCount = 0;
  this->gistCount = 0;
  this->heartCount = 0;
  this->ptuTime = DGCgettime();
  this->gistTime = DGCgettime();
  this->mapTime = DGCgettime();

  DGCcreateMutex(&m_mapMutex);
  DGCcreateMutex(&m_stateMutex);
  DGCcreateMutex(&m_ptuStateMutex);
  DGCcreateMutex(&m_attentionCellMutex);


  return;
}

// Default destructor
Attention::~Attention()
{
  DGCdeleteMutex(&m_mapMutex);
  DGCdeleteMutex(&m_stateMutex);
  DGCdeleteMutex(&m_ptuStateMutex);
  DGCdeleteMutex(&m_attentionCellMutex);
  return;
}

// Command line parser
int Attention::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return ERROR("Could not parse command line!");
  
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("attention");
 
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;  

  if(this->options.rndf_given)
    this->rndfFilename.assign(this->options.rndf_arg);
  else
    this->rndfFilename = "test.rndf";

  return 0;
}


// Parse the config file
int Attention::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, "ATTENTION");

  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Grab cmap details
  this->numRows = this->options.num_rows_arg;
  this->numCols = this->options.num_cols_arg;
  this->rowRes = this->options.row_resolution_arg;
  this->colRes = this->options.col_resolution_arg;
  this->sensNoDataVal = this->options.sens_no_data_value_arg;
  this->sensOutsideMapVal = this->options.sens_outside_map_value_arg;
  this->gistNoDataVal = this->options.gist_no_data_value_arg;
  this->gistOutsideMapVal = this->options.gist_outside_map_value_arg;

  emap = new EMap(this->numRows);

  // Grab map details
  this->recvSubGroup = this->options.receive_subgroup_arg;

  return 0;
}


/// Initialize map
int Attention::initMap(const char *configPath)
{
  // No need for mutexes here because the other thread hasn't started yet

  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;
  
  // grab state first and center map at alice's pose
  if(this->getState() != 0)
    return ERROR("initMap failed.");

  if(this->attentionMap.initMap(this->state.localX, this->state.localY, this->numRows, this->numCols,
				this->rowRes, this->colRes, 0) != 0)
    return ERROR("initMap failed.");

  // add sensor coverage layer (false to indicate NOT using deltas)
  this->sensCovLayerId = this->attentionMap.addLayer<double>(this->sensNoDataVal,this->sensOutsideMapVal, true);

  // add gist layer (false to indicate NOT using deltas)
  this->gistLayerId = this->attentionMap.addLayer<double>(this->gistNoDataVal,this->gistOutsideMapVal, true);

  // add final cost layer (false to indicate NOT using deltas)
  attentionCost initCost;
  initCost.cost = -1;
  gettimeofday(&stv, 0);
  initCost.timestamp = stv.tv_sec + (stv.tv_usec/1000000.0);
  this->costLayerId = this->attentionMap.addLayer<attentionCost>(initCost,initCost, true);

  // Initialize worldMap
  this->worldMapTalker.initRecvMapElement(this->skynetKey, this->recvSubGroup);

  // load rndf
  if(!this->worldMap.loadRNDF(this->rndfFilename.c_str()))    
    return ERROR("Error loading rndf!");

  if(this->loadMapPriorData()<0)
    return ERROR("Error loading lane lines!");


  // set default plannerMode
  this->planningMode = NO_MODE;
  
  // grab timestamp for module process speed
  gettimeofday(&stv, 0);
  this->lasttime = stv.tv_sec + (stv.tv_usec/1000000.0);


  // set the forgetting constant; the larger this value, the slower the cost grows back
  this->tau = 15;

  // TODO:REMOVE -- for the sensor coverage layer
  this->theta.clear();
  for(int i=0; i<540; i++)
    {
      //CCW
      //this->theta.push_back(i*2*M_PI/540);

      //CW
      this->theta.push_back(2*M_PI-i*2*M_PI/540);
    }

  return 0;
}

/// Update the map
int Attention::updateMap()
{
  usleep(100);

  NEcoord tmpCoord;
  VehicleState m_state;

  // grab state first and update vehicle location
  if(this->getState()<0)
    printf("error getting state! \n");
  DGClockMutex(&m_mapMutex);
  DGClockMutex(&m_stateMutex);
  this->attentionMap.updateVehicleLoc(this->state.localX, this->state.localY);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);
  DGCunlockMutex(&m_mapMutex);

  // listen for gist directive
  if(this->m_skynet->is_msg(this->gistSocket))
    {
      this->gistReceived = this->m_skynet->get_msg(this->gistSocket, &this->planningMode, 
						   sizeof(this->planningMode), 0);      
      if(this->gistReceived > 0)
	{
	  // process gist into map layer
	  // update display if applicable
	  this->gistCount += 1;

	  DGClockMutex(&m_mapMutex);
	  if(this->planningMode==LEFT_TURN)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "LEFT TURN         ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else if(this->planningMode==RIGHT_TURN)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "RIGHT TURN        ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else if(this->planningMode==LEFT_LANE_CHANGE)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "LEFT LANE CHANGE  ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else if(this->planningMode==RIGHT_LANE_CHANGE)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "RIGHT LANE CHANGE  ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else if(this->planningMode==FOLLOWING)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "FOLLOWING         ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else if(this->planningMode==U_TURN)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "U TURN            ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else if(this->planningMode==CRUISE)
	    {
	      if(this->console)
		{
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "CRUISE            ");
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	      this->attentionMap.clearLayer(this->gistLayerId);
	      this->attentionMap.clearLayer(this->costLayerId);
	    }
	  else		
	    {
	      if(this->console)
		{ 
		  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "UNKNOWN           ");		
		  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);	  
		}
	    }
	  DGCunlockMutex(&m_mapMutex);

	  this->gistTime = DGCgettime();
	  
	}      
    }

  this->loadGistMap(this->planningMode);


  // process sensor coverage into map layer
  // -- grab free space coverage from sensors

  // TODO: replace this section
  this->sensCovPoly.clear();
  double R = 30;

  for(uint i=0; i < theta.size(); i++)
    {     
      tmpCoord.N = m_state.localX+R*cos(theta[i]);
      tmpCoord.E = m_state.localY+R*sin(theta[i]);
      this->sensCovPoly.push_back(tmpCoord);
    }

  DGClockMutex(&m_mapMutex);
  this->attentionMap.clearLayer(this->sensCovLayerId);
  DGCunlockMutex(&m_mapMutex);

  this->loadSensorCovMap(this->sensCovPoly);
  
  // now fuse map
  this->loadFusedCostMap();

  // update heartbeat
  this->heartCount += 1;
  this->mapTime = DGCgettime();
  
  // update vehicle state display
  if(this->console)
    {

      if (m_state.timestamp > 0)
	cotk_printf(this->console, "%slat%", A_NORMAL,
		    "%+06dms", (int) (m_state.timestamp - this->mapTime) / 1000);
      cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
		  fmod((double) m_state.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%spos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  m_state.localX, m_state.localY, m_state.localZ);
      cotk_printf(this->console, "%srot%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  m_state.localRoll*180/M_PI,
		  m_state.localPitch*180/M_PI,
		  m_state.localYaw*180/M_PI);
      
      DGClockMutex(&m_ptuStateMutex);

      // update ptu state display
      cotk_printf(this->console, "%lpan%", A_NORMAL, "%+03.2f",(double)(this->ptublob.currpan));
      cotk_printf(this->console, "%ltilt%", A_NORMAL, "%+03.2f",(double)(this->ptublob.currtilt));

      DGCunlockMutex(&m_ptuStateMutex);

      // update latency displays
      cotk_printf(this->console, "%plat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->ptuTime) / 1000);
      cotk_printf(this->console, "%glat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->gistTime) / 1000);
      
      // update process diagnostics
      gettimeofday(&stv, 0);
      this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);
      cotk_printf(this->console, "%hcap%", A_NORMAL, "%5d ",this->heartCount);
      cotk_printf(this->console, "%mcycle%", A_NORMAL, "%+03.2f ", 1/(currenttime - lasttime));
      this->lasttime = this->currenttime;

    }
  
  return 0;
  
}


///Weight Map with prior info from RNDF
int Attention::loadMapPriorData()
{    
  // No need for mutexes here because this function is only called in the initMap function,
  // which is before the other thread is created
  MapElement tmpEl;
  for(int i=0; i< (int)this->worldMap.prior.data.size(); i++)
    {
      this->worldMap.prior.getElFull(tmpEl,i);
      this->priorMapData.push_back(tmpEl);
    }

  return 0;

}


/// Load the appropriate gist map
/// No need to worry about map mutex locks here; handled above; do need to worry about state mutex though
int Attention::loadGistMap(PlanningMode m_planningMode)
{
  LaneLabel currLaneLabel;
  point2 posePt;
  double scale;
  double heading;
  double minDist, tmpDist;
  int winRow, winCol;
  int winRow_next, winCol_next;

  VehicleState m_state;

  double northing, easting;
  double cost;
  double dist;

  // RIGHT TURN VARIABLES
  LaneLabel otherLaneLabel;
  vector<PointLabel> currLaneExitPtLabels;
  vector<PointLabel> otherLaneEntryPtLabels;
  point2_uncertain exitPt;
  point2_uncertain entryPt;
  point2_uncertain exitPt_tmp;
  point2_uncertain entryPt_tmp;
  int exitPt_row, exitPt_col;
  double varRightTurn = 3;
  double lookDownRightLaneDist = 5;
  double minDistToExitPt = 9e10;
  double minDistToEntryPt = 9e10;
  int closestExitPtIndex = -1;
  int closestEntryPtIndex = -1;

  bool rightTurnIsAhead;
 
  // RIGHT LANE CHANGE VARIABLES
  LaneLabel rightLaneLabel;
  point2 rightLanePt;
  double rlcLookRightDist = 3;
  double rlcLookBehindDist = 20;
  double varRightChange = 3;
  double varRightLaneChange = 10;
  double laneHeading;
  point2 lanePerpVec;
  point2 tmpVec;

  // LEFT LANE CHANGE VARIABLES
  LaneLabel leftLaneLabel;
  point2 leftLanePt;
  double llcLookLeftDist = 3;
  double llcLookBehindDist = 20;
  double varLeftChange = 3;
  double varLeftLaneChange = 10;


  // FOLLOWING VARIABLES
  double followLookAheadDist = 20;
  double followLookBehindDist = 20;    
  double frontWeight = 1;
  double backWeight = 0.60;
  double varFollowing = 10;
  
  minDist = 9e10;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);
  
  posePt.x = m_state.localX;
  posePt.y = m_state.localY;
  heading = m_state.localYaw;

  // find my lane first
  this->worldMap.getLane(currLaneLabel,posePt);
  
  if(this->console)
    {
      cotk_printf(this->console, "%seg%", A_NORMAL, "%5d",currLaneLabel.segment);
      cotk_printf(this->console, "%lane%", A_NORMAL, "%5d",currLaneLabel.lane);
    }

  switch(m_planningMode)
    {
    case LEFT_TURN:
      // fill gistMapLayer
      break;
    case RIGHT_TURN:

      // get exit and entry waypoint labels
      this->worldMap.getLaneExits(currLaneExitPtLabels, otherLaneEntryPtLabels, currLaneLabel);

      // now find the closest exit point that is AHEAD of us
      for(int i=0; i<(int)currLaneExitPtLabels.size(); i++)
	{
	  this->worldMap.getWayPoint(exitPt, currLaneExitPtLabels[i]);
	  rightTurnIsAhead = ((10*cos(heading)*(exitPt.x-posePt.x)+10*sin(heading)*(exitPt.y-posePt.y)) > 0);
	  
	  if(rightTurnIsAhead && 
	     sqrt(pow(posePt.x-exitPt.x,2)+pow(posePt.y-exitPt.y,2)) < minDistToExitPt)
	    {
	      minDistToExitPt = sqrt(pow(posePt.x-exitPt.x,2)+pow(posePt.y-exitPt.y,2));
	      closestExitPtIndex = i;
	    }	    	   
	}

      //now generate a vector that is perpendicular to direction of lane
      this->worldMap.getHeading(laneHeading, currLaneExitPtLabels[closestExitPtIndex]);
      lanePerpVec.x = 10*cos(laneHeading+M_PI/2); //this is +M_PI/2 because we have an upside down coordinate system
      lanePerpVec.y = 10*sin(laneHeading+M_PI/2); //this is +M_PI/2 because we have an upside down coordinate system
      
      //now find closest entry point to exit pt that yields a positive dot product with lanePerpVec               
      this->worldMap.getWayPoint(exitPt, currLaneExitPtLabels[closestExitPtIndex]);	  
      for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	{
	  this->worldMap.getWayPoint(entryPt, otherLaneEntryPtLabels[i]);
	  if(otherLaneEntryPtLabels[i].segment != currLaneLabel.segment)
	    {
	      tmpVec.x = entryPt.x - exitPt.x;
	      tmpVec.y = entryPt.y - exitPt.y;	      	      	    

	      rightTurnIsAhead = ( (tmpVec.x*lanePerpVec.x + tmpVec.y*lanePerpVec.y) > 0 );
	      
	      if(rightTurnIsAhead &&
		 sqrt(pow(tmpVec.x,2) + pow(tmpVec.y,2)) < minDistToEntryPt)
		{
		  minDistToEntryPt = sqrt(pow(tmpVec.x,2) + pow(tmpVec.y,2));
		  closestEntryPtIndex = i;
		}
	    }
	}
      
      // now for the closest exit pt, draw gaussian at exit pt
      if(closestExitPtIndex<0)
	{
	  // no exit point ahead of us so draw nothing
	}
      else
	{
	  this->worldMap.getWayPoint(exitPt, currLaneExitPtLabels[closestExitPtIndex]);	  
	  DGClockMutex(&m_mapMutex);
	  this->attentionMap.UTM2Win(exitPt.x, exitPt.y, &exitPt_row, &exitPt_col);
	  DGCunlockMutex(&m_mapMutex);
	  
	  dist = sqrt(pow(exitPt.x-posePt.x,2) + pow(exitPt.y-posePt.y,2));
	  this->drawGaussianInLane(currLaneLabel, m_state, dist, varRightTurn, 1, false);
	  
	}

      // now for the closest entry pt, draw long gaussian stretch
      if(closestEntryPtIndex<0)
	{
	  // no entry point for a right turn ahead of us so draw nothing
	}
      else
	{
	  this->worldMap.getWayPoint(entryPt, otherLaneEntryPtLabels[closestEntryPtIndex]);
	  this->worldMap.getLane(otherLaneLabel, entryPt);
	  
	  if(!(otherLaneLabel==currLaneLabel))
	    {

	      this->centerLine.clear();
	      this->worldMap.getLaneCenterLine(this->centerLine, otherLaneLabel);

	      DGClockMutex(&m_mapMutex);
	      for(int i=0; i<(int)this->centerLine.size(); i++)
		{
		  if( i==((int)this->centerLine.size()-1) )				  
		    break;
		  
		  minDist = sqrt(pow(this->centerLine[i+1].x-this->centerLine[i].x,2)+pow(this->centerLine[i+1].y-this->centerLine[i].y,2));

		  this->attentionMap.UTM2Win(this->centerLine[i].x,this->centerLine[i].y, &winRow, &winCol);
		  
		  //Check bounds
		  if(winRow < (this->numRows-1) &&
		     winRow >= 0 &&
		     winCol < (this->numCols-1) &&
		     winCol >= 0)
		    {
		      this->attentionMap.Win2UTM(winRow,winCol, &northing, &easting);		    
		      
		      scale = sqrt(2*M_PI)*varRightTurn*10;
		      
		      //check orientation; want to weight what's to the left by more <--- INSERT HERE
		      // if(left side)
		      //   cost = high value
		      // else(right side)
		      //   cost = lower value
		      
		      dist = sqrt(pow(entryPt.x-northing,2)+pow(entryPt.y-easting,2));
		      if(dist>=lookDownRightLaneDist)					    
			cost = 10;		    
		      else			
			cost =  1/(sqrt(2*M_PI)*varRightTurn)*exp(-pow(dist-lookDownRightLaneDist,2)/(2*varRightTurn*varRightTurn))*scale;			
		      
		      this->attentionMap.setDataWin<double>(this->gistLayerId, winRow, winCol, cost);
		    }
		  
		  while(minDist > this->rowRes)
		    {
		      for(int u=-1; u<2; u++)
			{
			  for(int v=-1; v<2; v++)
			    {			
			      this->attentionMap.Win2UTM(winRow+u,winCol+v, &northing, &easting);		       

			      tmpDist = sqrt(pow(this->centerLine[i+1].x-northing,2)+pow(this->centerLine[i+1].y-easting,2));
			      if(tmpDist < minDist)
				{
				  minDist = tmpDist;
				  winRow_next = winRow+u;
				  winCol_next = winCol+v;
				}		     
			    }
			}
		      
		      //Check bounds
		      if(winRow_next < (this->numRows-1) &&
			 winRow_next >= 0 &&
			 winCol_next < (this->numCols-1) &&
			 winCol_next >= 0)
			{
			  this->attentionMap.Win2UTM(winRow_next,winCol_next, &northing, &easting);			
			  
			  scale = sqrt(2*M_PI)*varRightTurn*10;		     
			  
			  //check orientation; want to weight what's to the left by more <--- INSERT HERE
			  // if(left side)
			  //   cost = high value
			  // else(right side)
			  //   cost = lower value
			  
			  dist = sqrt(pow(entryPt.x-northing,2)+pow(entryPt.y-easting,2));
			  if(dist>=lookDownRightLaneDist)		       
			    cost = 10;			
			  else			    
			    cost =  1/(sqrt(2*M_PI)*varRightTurn)*exp(-pow(dist-lookDownRightLaneDist,2)/(2*varRightTurn*varRightTurn))*scale;			  

			  this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next, winCol_next, cost);		 			  
			  for(int u=-1; u<2; u++)
			    { 
			      for(int v=-1; v<2; v++) 
				{ 				    
				  this->attentionMap.Win2UTM(winRow_next+u,winCol_next+v,&entryPt_tmp.x, &entryPt_tmp.y); 
				  
				  if(this->worldMap.isPointInLane(entryPt_tmp, otherLaneLabel))
				    {			      
				      if(winRow_next+u < (this->numRows-1) &&
					 winRow_next+u >= 0 &&
					 winCol_next+v < (this->numCols-1) &&
					 winCol_next+v >= 0)			      							    				   
					this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next+u, winCol_next+v, cost);
				      
				    }			 
				} 
			    } 		 		 

			}
		      winRow = winRow_next;
		      winCol = winCol_next;	     
		    }	 
		}
	      DGCunlockMutex(&m_mapMutex); 
	    }
	}
      break;
    case LEFT_LANE_CHANGE:                 

      // if my left lane doesn't exist
      if(this->worldMap.getNeighborLane(leftLaneLabel, currLaneLabel,-1)<=0)
	{	  
	  //draw a guassian directly to the left and shifted back 
	  leftLanePt.x = cos(heading)*(-llcLookBehindDist)-sin(heading)*(-llcLookLeftDist)+posePt.x;
	  leftLanePt.y = sin(heading)*(-llcLookBehindDist)+cos(heading)*(-llcLookLeftDist)+posePt.y;	
	  this->drawGaussianAtPt(leftLanePt, varLeftChange);
	}
      else //left lane does exist	
	fillLaneUpToDistBehind(leftLaneLabel, llcLookBehindDist, posePt, varLeftLaneChange);	         
      break;
    case RIGHT_LANE_CHANGE:      

      // find my lane first
      this->worldMap.getLane(currLaneLabel,posePt);

      // if my right lane doesn't exist
      if(this->worldMap.getNeighborLane(rightLaneLabel, currLaneLabel,1)<=0)      
	{	  
	  //draw a guassian directly to the right and shifted back 
	  rightLanePt.x = cos(heading)*(-rlcLookBehindDist)-sin(heading)*(rlcLookRightDist)+posePt.x;
	  rightLanePt.y = sin(heading)*(-rlcLookBehindDist)+cos(heading)*(rlcLookRightDist)+posePt.y;
	  
	  this->drawGaussianAtPt(rightLanePt, varRightChange);       
	}
      else //right lane does exist	
	fillLaneUpToDistBehind(rightLaneLabel, rlcLookBehindDist, posePt, varRightLaneChange);
      break;
    case FOLLOWING:      
      // fill gistMapLayer     

      this->drawGaussianInLane(currLaneLabel, m_state, followLookAheadDist, varFollowing, frontWeight, false);
      //this->drawGaussianInLane(currLaneLabel, m_state, followLookBehindDist, varFollowing, backWeight, true);

      break; 
    case U_TURN:
      // fill gistMapLayer 
      break; 
    case CRUISE: 
      // fill gistMapLayer       
      break; 
    } 
  
  return 0; 
  
} 


int Attention::loadSensorCovMap(vector<NEcoord> m_sensCovPoly)
{  
  
  vector<int> x_vec;
  vector<int> y_vec;
  vector<double> sens_vec;
  vector<NEcoord> pts_vec;
  NEcoord pt_tmp;
  int winRow, winCol;


  x_vec.clear();
  y_vec.clear();
  sens_vec.clear();

  DGClockMutex(&m_mapMutex);
  for(uint i=0; i<m_sensCovPoly.size(); i++)
    {
      this->attentionMap.UTM2Win(m_sensCovPoly[i].N,m_sensCovPoly[i].E,&winRow, &winCol);
      if(i==0)
	{	  	  
	  x_vec.push_back(winCol);
	  y_vec.push_back(winRow);
	  sens_vec.push_back(0);	
	}
      if(i>0)
	{            
	  if(winCol!=x_vec[x_vec.size()-1] && winRow!=y_vec[y_vec.size()-1])
	    {
	      x_vec.push_back(winCol);
	      y_vec.push_back(winRow);
	      sens_vec.push_back(0);
	    }
	}
    }

  //this->attentionMap.drawPolygonWin<double>(this->sensCovLayerId, x_vec, y_vec, sens_vec, false);

  DGCunlockMutex(&m_mapMutex);   


  /*
  x_vec.clear();
  y_vec.clear();
  sens_vec.clear();
  pts_vec.clear();

  DGClockMutex(&m_mapMutex);
  for(uint i=0; i<m_sensCovPoly.size(); i++)
    {
      this->attentionMap.UTM2Win(m_sensCovPoly[i].N,m_sensCovPoly[i].E,&winRow, &winCol);
      if(i==0)
	{	  	  
	  x_vec.push_back(winCol);
	  y_vec.push_back(winRow);

	  this->attentionMap.Win2UTM(winRow, winCol, &pt_tmp.N, &pt_tmp.E);
	  pts_vec.push_back(pt_tmp);

	  sens_vec.push_back(0);
	}
      if(i>0)
	{            
	  if(winCol!=x_vec[x_vec.size()-1] && winRow!=y_vec[y_vec.size()-1])
	    {
	      x_vec.push_back(winCol);
	      y_vec.push_back(winRow);

	      this->attentionMap.Win2UTM(winRow, winCol, &pt_tmp.N, &pt_tmp.E);
	      pts_vec.push_back(pt_tmp);

	      sens_vec.push_back(0);
	    }
	}
    }

  this->attentionMap.drawPolygonUTM<double>(this->sensCovLayerId, pts_vec, 0);  

  DGCunlockMutex(&m_mapMutex);  
  */  

  return 0;
}

// fuses the two layers and finds the max cell (UTM) at the same time
int Attention::loadFusedCostMap()
{
  double sensVal;
  double gistVal;
  attentionCost fusedVal;
  attentionCost last_fusedVal;  

  double m_maxCostval = 0;
  double m_maxCostx;
  double m_maxCosty;

  double t, deltaT;
  double newCost, desiredCost;
  int m_maxRow, m_maxCol;

  DGClockMutex(&m_mapMutex);
  for(int i=0; i<this->numRows; i++)
    {
      for(int j=0; j<this->numCols; j++)
	{
	  sensVal = this->attentionMap.getDataWin<double>(this->sensCovLayerId, i, j);
	  gistVal = this->attentionMap.getDataWin<double>(this->gistLayerId, i, j);
	  desiredCost = sensVal*gistVal;
	  gettimeofday(&stv, 0);
	  t = stv.tv_sec + (stv.tv_usec/1000000.0);

	  last_fusedVal = this->attentionMap.getDataWin<attentionCost>(this->costLayerId, i, j);

	  if(last_fusedVal.cost==-1)
	    {
	      // this means the layer was just cleared
	      fusedVal.cost = desiredCost;
	      fusedVal.timestamp = t;
	    }
	  else if(last_fusedVal.cost < desiredCost)
	    {	      
	      deltaT = t - last_fusedVal.timestamp;
	      newCost = desiredCost*(1-exp(-deltaT/this->tau));

	      if(fabs(desiredCost-newCost)<0.25)
		{
		  fusedVal.cost = desiredCost;
		  fusedVal.timestamp = t;
		}
	      else
		{
		  fusedVal.cost = newCost;
		  fusedVal.timestamp = last_fusedVal.timestamp;
		}	    
	    }
	  else
	    {
	      fusedVal.cost = desiredCost;
	      fusedVal.timestamp = t;
	    }
	  
	  if(fusedVal.cost > m_maxCostval)
	    {
	      m_maxCostval = fusedVal.cost;
	      m_maxRow = i;
	      m_maxCol = j;
	    }	        	  

	  this->attentionMap.setDataWin<attentionCost>(this->costLayerId, i, j, fusedVal);
	}
    }

  this->attentionMap.Win2UTM(m_maxRow, m_maxCol, &m_maxCostx, &m_maxCosty);
  this->maxCostPt = vec3_set(m_maxCostx,m_maxCosty,m_maxCostval);
  
  DGCunlockMutex(&m_mapMutex);  

  return(0);

}

void Attention::drawGaussianAtPt(point2 gPt, double m_var)
{
  int winRow, winCol;
  int winRow_tmp, winCol_tmp;
  double dist;
  double scale;
  double cost;

  DGClockMutex(&m_mapMutex);
  this->attentionMap.UTM2Win(gPt.x, gPt.y, &winRow, &winCol);	  
  for(int i=-10; i<10; i++)
    {
      for(int j=-10; j<10; j++)
	{		  
	  winRow_tmp = winRow+i;
	  winCol_tmp = winCol+j;
	  
	  dist = sqrt(pow((double)i,2)+pow((double)j,2));
	  scale = sqrt(2*M_PI)*m_var*10;
	  cost = 1/(sqrt(2*M_PI)*m_var)*exp(-pow(dist,2)/(2*m_var*m_var))*scale;

	  this->attentionMap.setDataWin<double>(this->gistLayerId,winRow_tmp,winCol_tmp,cost);
	}
    }

  DGCunlockMutex(&m_mapMutex);
}

/// Draws a gaussian in the specified lane at some distance from where vehicle is
/// --- if dist is negative, we draw the gaussian behind us;
void Attention::drawGaussianInLane(LaneLabel m_laneLabel, VehicleState m_state, double m_lookDist, double var, double weight, bool behind)
{
  point2arr_uncertain m_centerLine;
  point2 lanePt;
  point2 posePt;
  double heading;
  double minDist, dist, tmpDist;
  double scale;
  double cost;
  int winRow, winCol;
  int winRow_next, winCol_next;
  double northing, easting;
  bool cellisAhead;

  heading = m_state.localYaw;
  posePt.x = m_state.localX;
  posePt.y = m_state.localY;

  DGClockMutex(&m_mapMutex);  
  this->worldMap.getLaneCenterLine(m_centerLine, m_laneLabel);
  for(int i=0; i<(int)m_centerLine.size(); i++)
    {
      if( i==((int)m_centerLine.size()-1) )
	break;
      
      minDist = sqrt(pow(m_centerLine[i+1].x-m_centerLine[i].x,2)+pow(m_centerLine[i+1].y-m_centerLine[i].y,2));
      this->attentionMap.UTM2Win(m_centerLine[i].x,m_centerLine[i].y, &winRow, &winCol);
	  
      //Check bounds
      if(winRow < (this->numRows-1) &&
	 winRow >= 0 &&
	 winCol < (this->numCols-1) &&
	 winCol >= 0)
	{	  
	  if( (10*cos(heading)*(m_centerLine[i].x-posePt.x)+10*sin(heading)*(m_centerLine[i].y-posePt.y)) >= 0 )
	    cellisAhead = true;	    
	  else	    
	    cellisAhead = false;

	  if(cellisAhead && !behind > 0)
	    {
	      dist = sqrt(pow(posePt.x-m_centerLine[i].x,2)+pow(posePt.y-m_centerLine[i].y,2));
	      if(dist<7)
		cost=0; // 7 is roughly the length of alice
	      else
		{
		  scale = sqrt(2*M_PI)*var*10;
		  cost = 1/(sqrt(2*M_PI)*var)*exp(-pow(dist-m_lookDist,2)/(2*var*var))*scale*weight;
		}
	      
	      this->attentionMap.setDataWin<double>(this->gistLayerId, winRow, winCol, cost);
	    }
	  else if(!cellisAhead && behind)
	    {
	      dist = sqrt(pow(posePt.x-m_centerLine[i].x,2)+pow(posePt.y-m_centerLine[i].y,2));	    
	      if(dist<7)
		cost=0; // 7 is roughly the length of alice
	      else
		{
		  scale = sqrt(2*M_PI)*var*10;
		  cost = 1/(sqrt(2*M_PI)*var)*exp(-pow(dist-m_lookDist,2)/(2*var*var))*scale*weight;	      
		}

	      this->attentionMap.setDataWin<double>(this->gistLayerId, winRow, winCol, cost);
	    }	 	  
	}
      
      while(minDist > this->rowRes)
	{
	  for(int u=-1; u<2; u++)
	    {
	      for(int v=-1; v<2; v++)
		{
		  this->attentionMap.Win2UTM(winRow+u,winCol+v, &northing, &easting);
		  tmpDist = sqrt(pow(m_centerLine[i+1].x-northing,2)+pow(m_centerLine[i+1].y-easting,2));
		  if(tmpDist < minDist)
		    {
		      minDist = tmpDist;
		      winRow_next = winRow+u;
		      winCol_next = winCol+v;
		    }		     
		}
	    }
	  
	  //Check bounds
	  if(winRow_next < (this->numRows-1) &&
	     winRow_next >= 0 &&
	     winCol_next < (this->numCols-1) &&
	     winCol_next >= 0)
	    {
	      this->attentionMap.Win2UTM(winRow_next,winCol_next, &northing, &easting);
	      if( (10*cos(heading)*(northing-posePt.x)+10*sin(heading)*(easting-posePt.y)) >= 0 )	    
		cellisAhead = true;	    
	      else	    
		cellisAhead = false;
	      
	      if(cellisAhead && !behind)
		{
		  dist = sqrt(pow(posePt.x-northing,2)+pow(posePt.y-easting,2));
		  if(dist<7)
		    cost=0; //7 is roughly the length of Alice
		  else
		    {
		      scale = sqrt(2*M_PI)*var*10;
		      cost = 1/(sqrt(2*M_PI)*var)*exp(-pow(dist-m_lookDist,2)/(2*var*var))*scale*weight;	      
		    }

		  
		  this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next, winCol_next, cost);
		  
		  for(int u=-1; u<2; u++)
		    { 
		      for(int v=-1; v<2; v++) 
			{ 
			  this->attentionMap.Win2UTM(winRow_next+u,winCol_next+v,&lanePt.x, &lanePt.y); 
			  
			  if(this->worldMap.isPointInLane(lanePt, m_laneLabel))
			    {
			      
			      if(winRow_next+u < (this->numRows-1) &&
				 winRow_next+u >= 0 &&
				 winCol_next+v < (this->numCols-1) &&
				 winCol_next+v >= 0)			      			       
				this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next+u, winCol_next+v, cost);
			    }			 
			} 
		    } 		 		 
		}
	      else if(!cellisAhead && behind)
		{
		  this->attentionMap.Win2UTM(winRow_next,winCol_next, &northing, &easting);
		  dist = sqrt(pow(posePt.x-northing,2)+pow(posePt.y-easting,2));
		  if(dist<7)
		    cost=0; //7 is roughly the length of Alice
		  else
		    {
		      scale = sqrt(2*M_PI)*var*10;
		      cost = 1/(sqrt(2*M_PI)*var)*exp(-pow(dist-m_lookDist,2)/(2*var*var))*scale*weight;	      
		    }
		  
		  this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next, winCol_next, cost);

		  for(int u=-1; u<2; u++)
		    { 
		      for(int v=-1; v<2; v++) 
			{ 
			  this->attentionMap.Win2UTM(winRow_next+u,winCol_next+v,&lanePt.x, &lanePt.y); 
			  
			  if(this->worldMap.isPointInLane(lanePt, m_laneLabel))
			    {
			      
			      if(winRow_next+u < (this->numRows-1) &&
				 winRow_next+u >= 0 &&
				 winCol_next+v < (this->numCols-1) &&
				 winCol_next+v >= 0)			      			       
				this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next+u, winCol_next+v, cost);
			    }			 
			} 
		    } 		 		 
		}		      
	    }
	  winRow = winRow_next;
	  winCol = winCol_next;	     
	}	 
    }            
  DGCunlockMutex(&m_mapMutex);  
}


/// Finds the intersection of the Line of Site vector with the ground plane
int Attention::findLineOfSiteIntersect(float *x, float *y, float *z)
{
  vec3_t pointInPlane;
  vec3_t normalToPlane;
  vec3_t linePt1, linePt2;

  float px,py,pz;
  float vx,vy,vz;
  float lx,ly,lz;

  float d,t;
  float numerator, denominator;

  PTUStateBlob *m_ptublob;
  VehicleState m_state;

  DGClockMutex(&this->m_ptuStateMutex);
  m_ptublob = &this->ptublob;
  DGCunlockMutex(&this->m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);

  PTUStateBlobToolToPTU(m_ptublob,0,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(m_ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(m_ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt1 = vec3_set(lx,ly,lz);
  
  PTUStateBlobToolToPTU(m_ptublob,50,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(m_ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(m_ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt2 = vec3_set(lx,ly,lz);

  pointInPlane = vec3_set(m_state.localX, m_state.localY, m_state.localZ+VEHICLE_TIRE_RADIUS);
  normalToPlane = vec3_set(0, 0, 1);

  d = vec3_dot(pointInPlane, normalToPlane);
  
  denominator = normalToPlane.x*(linePt2.x-linePt1.x) + normalToPlane.y*(linePt2.y-linePt1.y) + normalToPlane.z*(linePt2.z-linePt1.z);
  if(denominator==0) // the intersection does not exist    
    return(-1);
  else
    {
      numerator = d - normalToPlane.x*linePt1.x - normalToPlane.y*linePt1.y - normalToPlane.z*linePt1.z;
      t = numerator/denominator;
      *x = linePt1.x + t*(linePt2.x - linePt1.x);
      *y = linePt1.y + t*(linePt2.y - linePt1.y);
      *z = linePt1.z + t*(linePt2.z - linePt1.z);
      return 0;
    }
}


void Attention::fillLaneUpToDistBehind(LaneLabel m_laneLabel, double m_lookBehindDist, point2 m_posePt, double m_var)
{
  double minDist, dist, tmpDist;
  int winRow, winCol;
  int winRow_next, winCol_next;
  double northing, easting;
  double heading;
  double cost;
  double scale;
  point2 lanePt;
  point2arr_uncertain m_centerLine;

  this->worldMap.getLaneCenterLine(m_centerLine, m_laneLabel);

  DGClockMutex(&m_stateMutex);
  heading = this->state.localYaw;
  DGCunlockMutex(&m_stateMutex);

  DGClockMutex(&m_mapMutex);
  for(int i=0; i<(int)m_centerLine.size(); i++)
    {
      if( i==((int)m_centerLine.size()-1) )
	break;
      
      minDist = sqrt(pow(m_centerLine[i+1].x-m_centerLine[i].x,2)+pow(m_centerLine[i+1].y-m_centerLine[i].y,2));
      this->attentionMap.UTM2Win(m_centerLine[i].x,m_centerLine[i].y, &winRow, &winCol);
      
      //Check bounds
      if(winRow < (this->numRows-1) &&
	 winRow >= 0 &&
	 winCol < (this->numCols-1) &&
	 winCol >= 0)
	{
	  this->attentionMap.Win2UTM(winRow,winCol, &northing, &easting);
	  
	  dist = sqrt(pow(m_posePt.x-northing,2)+pow(m_posePt.y-easting,2));
	  
	  //check orientation; want to weight what's behind us by more
	  if( (10*cos(heading)*(northing-m_posePt.x)+10*sin(heading)*(easting-m_posePt.y)) < 0)
	    {
	      if(dist>=m_lookBehindDist)		   
		cost = 10;		   
	      else		   
		{
		  scale = sqrt(2*M_PI)*m_var*10;
		  cost = 1/(sqrt(2*M_PI)*m_var)*exp(-pow(dist-m_lookBehindDist,2)/(2*m_var*m_var))*scale;
		  if(cost<5)
		    cost = 5;
		}		  
	    }
	  else
	    cost = 5;
	  
	  this->attentionMap.setDataWin<double>(this->gistLayerId, winRow, winCol, cost);
	}
      
      while(minDist > this->rowRes)
	{
	  for(int u=-1; u<2; u++)
	    {
	      for(int v=-1; v<2; v++)
		{
		  this->attentionMap.Win2UTM(winRow+u,winCol+v, &northing, &easting);
		  tmpDist = sqrt(pow(m_centerLine[i+1].x-northing,2)+pow(m_centerLine[i+1].y-easting,2));
		  if(tmpDist < minDist)
		    {
		      minDist = tmpDist;
		      winRow_next = winRow+u;
		      winCol_next = winCol+v;
		    }		     
		}
	    }
	  
	  //Check bounds
	  if(winRow_next < (this->numRows-1) &&
	     winRow_next >= 0 &&
	     winCol_next < (this->numCols-1) &&
	     winCol_next >= 0)
	    {
	      this->attentionMap.Win2UTM(winRow_next,winCol_next, &northing, &easting);
	      
	      dist = sqrt(pow(m_posePt.x-northing,2)+pow(m_posePt.y-easting,2));
	      
	      //check orientation; want to weight what's behind us by more
	      if( (10*cos(heading)*(northing-m_posePt.x)+10*sin(heading)*(easting-m_posePt.y)) < 0)
		{
		  if(dist>=m_lookBehindDist)		   
		    cost = 10;		   
		  else		   
		    {
		      scale = sqrt(2*M_PI)*m_var*10;
		      cost = 1/(sqrt(2*M_PI)*m_var)*exp(-pow(dist-m_lookBehindDist,2)/(2*m_var*m_var))*scale;
		      if(cost < 5)
			cost = 5;
		    }		      
		}
	      else
		cost = 5;
	      
	      this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next, winCol_next, cost);		 
	      
	      for(int u=-1; u<2; u++)
		{ 
		  for(int v=-1; v<2; v++) 
		    { 
		      this->attentionMap.Win2UTM(winRow_next+u,winCol_next+v,&lanePt.x, &lanePt.y); 
		      
		      if(this->worldMap.isPointInLane(lanePt, m_laneLabel))
			{			      
			  if(winRow_next+u < (this->numRows-1) &&
			     winRow_next+u >= 0 &&
			     winCol_next+v < (this->numCols-1) &&
			     winCol_next+v >= 0)			      			       
			    this->attentionMap.setDataWin<double>(this->gistLayerId, winRow_next+u, winCol_next+v, cost);
			}			 
		    } 
		} 		 		 
	    }
	  winRow = winRow_next;
	  winCol = winCol_next;	     
	}	 
    }
  DGCunlockMutex(&m_mapMutex);	    
  
}



/// Initialize sensnet 
int Attention::initSensnet()
{  
  // Initialize SensNet 
  this->sensnet = sensnet_alloc();
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, MODattention) != 0) 
    return ERROR("unable to connect to sensnet");
  
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to the PTU state messages
  if (sensnet_join(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(this->ptublob)) != 0)
    return ERROR("unable to join PTU state group");

  // Subscribe to process state messages
  if(!this->options.disable_process_control_flag)
    {
      if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
	return ERROR("unable to join process group");
    }

  return 0;
}

/// Finalize sensnet
int Attention::finiSensnet()
{
  // Leave the state group
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);

  // Leave the PTU state group 
  sensnet_leave(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB);

  // Leave the ProcessControl group
  if(!this->options.disable_process_control_flag)
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);

  // Disconnect
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}

/// Initialize skynet
int Attention::initSkynet()
{
  // Initialize sockets for receiving gistmap and sending ptucommands
  this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);

  this->ptuCommandSocket = this->m_skynet->get_send_sock(SNptuCommand);
  if(this->ptuCommandSocket < 0)
    return ERROR("attention::initSkynet(): skynet get_send_sock returned error");

  this->gistSocket =  this->m_skynet->listen(SNgist, this->moduleId);
  if(this->gistSocket < 0)
    return ERROR("attention::initSkynet(): skynet get_send_sock returned error");

  return 0;
}

/// Finalise skynet
int Attention::finiSkynet()
{
  delete this->m_skynet;
  return 0;
}

/// Do the attending based on current map
int Attention::attend()
{
  float localx, localy, localz;
  int m_row, m_col;
  int tmpRow, tmpCol;
  double m_panspeed, m_tiltspeed;
  double t;
  double timeElapsed;

  attentionCost tmpCost;
  CellData tmpCell;

  if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
    {
      DGClockMutex(&m_attentionCellMutex);
      attentionCellVec.clear();
      tmpCell.x = localx;
      tmpCell.y = localy;      
      tmpCell.cellVal = 0;
      attentionCellVec.push_back(tmpCell);
      DGCunlockMutex(&m_attentionCellMutex);

      DGClockMutex(&m_mapMutex);
      this->attentionMap.UTM2Win((double)localx, (double)localy, &m_row, &m_col);

      gettimeofday(&stv, 0);
      t = stv.tv_sec + (stv.tv_usec/1000000.0);

      for(int i=-3; i<4; i++)
	{
	  for(int j=-3; j<4; j++)
	    {
	      tmpRow = m_row+i;
	      tmpCol = m_col+j;

	      if(tmpRow >= this->numRows-1)
		tmpRow = this->numRows-1;
	      if(tmpRow <= 0)
		tmpRow = 0;

	      if(tmpCol >= this->numCols-1)
		tmpCol = this->numCols-1;
	      if(tmpCol <= 0)
		tmpCol = 0;

	      tmpCost.cost = 0;
	      tmpCost.timestamp = t;

	      this->attentionMap.setDataWin<attentionCost>(this->costLayerId, tmpRow, tmpCol, tmpCost);
	    }
	}
      DGCunlockMutex(&m_mapMutex);
    }

  if(this->maxCostPt.z < 1)
    {
      this->command.type = RAW;
      this->command.pan = 0;
      this->command.tilt = -15;
      this->command.panspeed = 25;
      this->command.tiltspeed = 25;
    }
  else
    {
      DGClockMutex(&m_stateMutex);
      this->command.type = LINEOFSITE;
      this->command.localx = this->maxCostPt.x;
      this->command.localy = this->maxCostPt.y;
      this->command.localz = this->state.localZ+VEHICLE_TIRE_RADIUS;
      this->command.panspeed = 25;
      this->command.tiltspeed = 25;
      DGCunlockMutex(&m_stateMutex);
    }

  DGClockMutex(&m_attentionCellMutex); 
  tmpCell.x = this->command.localx;
  tmpCell.y = this->command.localy;      
  tmpCell.cellVal = 0;
  attentionCellVec.push_back(tmpCell);
  DGCunlockMutex(&m_attentionCellMutex);
  
  DGClockMutex(&m_ptuStateMutex);
  m_panspeed = this->ptublob.currpanspeed;
  m_tiltspeed = this->ptublob.currtiltspeed;
  DGCunlockMutex(&m_ptuStateMutex);

  timeElapsed = this->currenttime - (double)(this->ptuTime)/1000000;

  if(m_panspeed==0 && m_tiltspeed==0 && timeElapsed > 2) //wait at least two seconds between sending commands
    {      
      // Send PTU command
      if(this->sendPTUcommand(this->command) <= 0)
	return ERROR("PTU command not sent!");
      
      // Update ptu command count
      this->ptuCount += 1;  
      this->ptuTime = DGCgettime();
      
      if(this->console)
	{
	  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	  cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		      this->command.localx, this->command.localy, this->command.localz);
	}      
    }

  return 0;
}

/// Get Alice and PTU state
int Attention::getState()
{
  int blobId;
  int ptublobId;

  DGClockMutex(&m_stateMutex);
  memset(&this->state, 0, sizeof(this->state));  
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &blobId, sizeof(this->state), &this->state) != 0)
    return ERROR("unable to read state data");
  DGCunlockMutex(&m_stateMutex);

  if (blobId <= 0)
    return WARN("state is invalid: ignoring");
  

  DGClockMutex(&m_ptuStateMutex);
  memset(&this->ptublob, 0, sizeof(this->ptublob));
  if (sensnet_read(this->sensnet, SENSNET_MF_PTU, 
		   SENSNET_PTU_STATE_BLOB, &ptublobId, 
		   sizeof(this->ptublob), &this->ptublob) != 0)
    return ERROR("Could not read ptu state");
  DGCunlockMutex(&m_ptuStateMutex);

  return 0;
}


// Get the process state
int Attention::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


/// Send PTU command
int Attention::sendPTUcommand(PTUCommand p_command)
{
  int numBytesSent;
  numBytesSent = m_skynet->send_msg(this->ptuCommandSocket, &p_command, sizeof(p_command), 0);
  return numBytesSent;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"Attention $Revision$           MODULE RATE                            \n"
"                               Hz: %mcycle%                           \n"
"Skynet: %spread%                                                      \n"
"                                                                      \n"
"ALICE STATE                    PTU STATE                              \n"
"Time    : %stime%              rawpan  (deg): %lpan%                  \n"
"Pos     : %spos%               rawtilt (deg): %ltilt%                 \n"
"Rot     : %srot%                                                      \n"
"Latency : %slat%                                                      \n"
"                                                                      \n"
"GIST COMMANDS                  PTU COMMANDS                           \n"
"Mode      : %gmode%            Number sent  : %pcap%                  \n"
"Num recvd : %gcap%             Command      : %plos%                  \n"
"Latency   : %glat%             Latency      : %plat%                  \n"
"                                                                      \n" 
"                                                                      \n" 
"                                                                      \n" 
"HEARTBEAT : %hcap%             CURRENT LANE                           \n" 
"                               Segment:  %seg%                        \n" 
"%stderr%                       Lane:     %lane%                       \n" 
"%stderr%                                                              \n" 
"%stderr%                                                              \n" 
"                                                                      \n" 
"[%QUIT%|%PAUSE%]                                                      \n"; 
  
// Initialize console display
int Attention::initConsole()
{   
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  if (cotk_open(this->console, "attention.msg") != 0)
    return -1;
  
  // Display spread id
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
 
  // Display some initial values
  cotk_printf(this->console, "%gcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%pcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NO MODE        ");
  return 0;
}

// Finalize console display
int Attention::finiConsole()
{
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}

/// Console button callback
int Attention::onUserQuit(cotk_t *console, Attention *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

/// Console button callback
int Attention::onUserPause(cotk_t *console, Attention *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}

void idle(void* param)
{ 

  Attention* self = (Attention*) param;
  CellData tempData;

  VehicleState m_state;
  PTUStateBlob m_ptublob;

  DGClockMutex(&self->m_stateMutex);
  m_state = self->state;
  DGCunlockMutex(&self->m_stateMutex);

  DGClockMutex(&self->m_ptuStateMutex);
  m_ptublob = self->ptublob;
  DGCunlockMutex(&self->m_ptuStateMutex);

  self->sensvec.clear();
  self->gistvec.clear();
  self->costvec.clear();
  
  DGClockMutex(&self->m_mapMutex);
  for(int i=0; i<self->numRows; i++)
    {
      for(int j=0; j<self->numCols; j++)
	{
	  self->attentionMap.Win2UTM(i, j, &tempData.x, &tempData.y);
	  tempData.cellVal = self->attentionMap.getDataWin<double>(self->sensCovLayerId, i, j);	  
	  self->sensvec.push_back(tempData);
	  
	  self->attentionMap.Win2UTM(i, j, &tempData.x, &tempData.y);
	  tempData.cellVal = self->attentionMap.getDataWin<double>(self->gistLayerId, i, j);
	  self->gistvec.push_back(tempData);
	  
	  self->attentionMap.Win2UTM(i, j, &tempData.x, &tempData.y);
	  tempData.cellVal = self->attentionMap.getDataWin<double>(self->costLayerId, i, j);
	  self->costvec.push_back(tempData);
	}
    }  
  DGCunlockMutex(&self->m_mapMutex);

  // update the map
  DGClockMutex(&self->m_attentionCellMutex);
  self->worldwin->update(m_state, m_ptublob, self->sensvec, self->gistvec, self->costvec, self->priorMapData, self->attentionCellVec);
  DGCunlockMutex(&self->m_attentionCellMutex);

}

void Attention::initVisualizer(int *argc, char** argv)
{
  glutInit(argc, argv);
}

/// Start visualizer thread
void Attention::v_startVisualizerThread()
{
  int cols = 1024;
  int rows = 768;

  Fl_Menu_Item menuitems[] =
    {
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"&Pause", FL_CTRL + 'p', (Fl_Callback*) Attention::v_onAction, (void*) 0x1000, FL_MENU_TOGGLE},
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) Attention::v_onExit},
      {0},
      {0},     
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Attention Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Crate the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new WorldWin(0, 30, cols, rows - 30, 30);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Initialize image window
  // -- no need for mutex around options because it's not used after this call in other thread
  this->worldwin->init(this->options.col_resolution_arg, this->options.row_resolution_arg);

  // Idle callback
  Fl::add_idle(idle, this);

  // Run
  this->mainwin->show();
  while (!this->quit)
    Fl::wait();
   
  return;

}

// Handle menu callbacks
void Attention::v_onExit(Fl_Widget *w, int option)
{
  Attention *self;

  self = (Attention*) w->user_data();
  self->quit = true;

  return;
}


// Handle menu callbacks
void Attention::v_onAction(Fl_Widget *w, int option)
{
  Attention *self;  

  self = (Attention*) w->user_data();
  if (option == 0x1000)
    self->pause = !self->pause;
  
  return;
}


int main(int argc, char* argv[])
{
  // Initialize attention module
  Attention *attention;
  attention = new Attention();
  assert(attention);

  int loopCount = 0;

  // Parse command line options
  if (attention->parseCmdLine(argc, argv) != 0)
    return -1;

  // Initialize sensnet
  if (attention->initSensnet() != 0)
    return -1;

  // Initialize skynet
  if (attention->initSkynet() != 0)
    return -1;

  // Initialize map
  if(attention->initMap(attention->defaultConfigPath) != 0)
    return -1;

  // Initialize console
  if (!attention->options.disable_console_flag)
  {
    if (attention->initConsole() != 0)
      return -1;
  }

  // Initialize visualization (if applicable)
  if (attention->options.view_flag)
    {
      attention->initVisualizer(&argc, argv);
      DGCstartMemberFunctionThread(attention, &Attention::v_startVisualizerThread);
    }
 
 
  // Start Processing
  while(!attention->quit)
    {
      // Do heartbeat occasionally
      if(!attention->options.disable_process_control_flag)
	{
	  if(attention->heartCount % 15 == 0)
	    attention->getProcessState();           
	}    

      // Update the console
      if (attention->console)
	cotk_update(attention->console);
      
      // If paused, give up our time slice.
      if (attention->pause)
	{      
	  usleep(0);
	  continue;
	}
      
      // Update map -- includes listening for gist directive            
      if(attention->updateMap() != 0)
	break;		      
           
      // Attend -- includes sending PTU command directive
      // FIX: currently attend every 100 times through the loop; too many PTUcommands
      if(attention->attend() != 0)
	break;

      loopCount++;
    }     

  attention->finiConsole();
  attention->finiSensnet();
  attention->finiSkynet();

  cmdline_parser_free(&attention->options);
  delete attention;

  MSG("program exited cleanly");

  return 0;

  

}
