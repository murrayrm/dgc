#ifndef __ATTENTION_HH__
#define __ATTENTION_HH__

#include <assert.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <iostream>

// dgc basic header files
#include <alice/AliceConstants.h>
#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <interfaces/PTUStateBlob.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/PTUCommand.h>
#include <skynet/sn_msg.hh>
#include <sensnet/sensnet.h>
#include <vector>

// State talker
#include <skynettalker/SkynetTalker.hh>
#include <skynettalker/StateClient.hh>

// Map header file
#include <map/Map.hh>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>

// EMap header file
#include <emap/emap.hh>
#include "emapWrapper.hh"

// Time stamp header file
#include <sys/time.h>

// Frames header files
#include <frames/pose3.h>
#include <frames/mat44.h>

// cotk display
#include <cotk/cotk.h>

// custom header files
#include "cmdline.h"
#include "WorldWin.hh"

// Planning mode interface
#include <gcinterfaces/AttentionPlannerState.hh>

// Bad macro defined
#undef border

// for visualizer
#include <GL/glut.h>
#include <FL/Fl.H>
#include <FL/fl_draw.h>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>

// Error handling 
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)
#define WARN(fmt, ...) \
  (fprintf(stderr, "warn: %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 1 : 1)
#define MSG(fmt, ...) \
  (fprintf(stderr, "msg  : %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

#define NUMSCANPOINTS 181

using namespace std;

/// Wraps a state client into an object
class CStateWrapper : public CStateClient
{
public:
  CStateWrapper(int snkey) : CSkynetContainer(MODattention,snkey),CStateClient(true) { };
  ~CStateWrapper() {};
  
  VehicleState getstate() 
  {
    UpdateState();
    return m_state;
  }

};

class Attention
{
public:

  /// Default constructor
  Attention();
  
  /// Default destructor
  ~Attention();

  /// Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  /// Parse the config file
  int parseConfigFile(const char *configPath);

  /// Initialize attention map
  int initMap(const char *configPath, PlanningState::Mode m_planningMode);

  /// Update the map
  int updateMap(PlanningState::Mode m_planningMode);

 /// Initialize sensnet -- will need sensnet for PTU state
  int initSensnet();

  /// Finalize sensnet -- will need sensnet for PTU state
  int finiSensnet();

  /// Initialize skynet -- will need skynet for receiving gist and sending ptucommand
  int initSkynet();

  /// Finalise skynet -- will need skynet for receiving gist and sending ptucommang
  int finiSkynet();

  /// Do the attending based on current map
  int attend();

  /// Get alice and ptu state
  int getState();

  /// Get process state
  int getProcessState();
  
  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  /// Console button callback
  static int onUserQuit(cotk_t *console, Attention *self, const char *token);

  /// Console button callback
  static int onUserPause(cotk_t *console, Attention *self, const char *token);

  /// Initialize visualizer
  void initVisualizer(int *argc, char** argv);

 /// Visualization thread
  void v_startVisualizerThread();

  // Action callback
  static void v_onAction(Fl_Widget *w, int option);
  
  // Exit callback
  static void v_onExit(Fl_Widget *w, int option);
  
public:

  // Default configuration path
  char *defaultConfigPath;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;

  // SensNet handle
  sensnet_t *sensnet;

  // CSS logging settings
  bool debug;
  int verbose_level;
  bool logging;
  string log_path;
  string log_filename;
  int log_level;
  
  // Console text display
  cotk_t *console;

  // Wrapper around state client
  CStateWrapper * stateTalker;

  // Current vehicle state data
  VehicleState state;

  // PTU blob for ptu state
  PTUStateBlob ptublob;

  // Ladar blob for ladar data 
  LadarRangeBlob ladarblob;

  // Number of rows
  int numRows;
  
  // Number of cols
  int numCols;
  
  // Skynet object
  skynet *m_skynet;

  // Should we quit?
  bool quit;

  // Should we pause?
  bool pause;

  // Top-level window
  Fl_Window *mainwin;
  
  // Top-level menu
  Fl_Menu_Bar *menubar;

  // 3D window
  WorldWin *worldwin;

  // Sensor coverage layer id
  int sensCovLayerId;
  
  // Gist layer id
  int gistLayerId;

  // Cost layer id
  int costLayerId;

  // Program options
  gengetopt_args_info options;

  // Emap attention map
  EMap *emapSensor;
  EMap *emapGist;
  EMap *emapCostVal;
  EMap *emapCostTime;
  
  EMapWrapper sensorMapWrapper;
  EMapWrapper gistMapWrapper;
  EMapWrapper costMapWrapper;
  EMapWrapper timeMapWrapper;
  

  vector<CellData> sensvec;
  
  vector<CellData> gistvec;

  vector<CellData> costvec;

  vector<MapElement> priorMapData;

  pthread_mutex_t m_stateMutex;

  pthread_mutex_t m_ptuStateMutex;

  pthread_mutex_t m_currentCellMutex;

  pthread_mutex_t m_desiredCellMutex;

  pthread_mutex_t m_emapMutex;

  CellData currentAttendedCell;

  CellData desiredAttendedCell;

  // Counter for heartbeat
  int heartCount;

private:
  
  /// Grab lane lines from RNDF
  int loadMapPriorData();

  /// load appropriate gist map based on directive
  int loadGistMap(PlanningState::Mode m_planningMode);
 
  /// load sensor coverage map
  int loadSensorCovMap();

  /// load fused cost map
  int loadFusedCostMap();

  /// Fills the adjacent lane specified by m_laneLabel with cost up to some look behind distance 
  /// --- var is used as a blending of the cost
  void fillLaneUpToDistBehind(LaneLabel m_laneLabel, double m_lookBehindDist, point2 m_posePt, double m_var);

  /// Draws a block in the specified lane at some following distance
  void drawBlockInLane(LaneLabel m_laneLabel, double blockHeight, point2 startPt, double heading, double length);

  /// Draws a block in the lane given the lane center line, the cost, and the distance along the lane
  void drawLaneCost(const point2arr &m_laneCenterLine, double m_cost);

  /// Assigns the given cost to the cell in the array that is furthest from Alice yet still in the map
  void drawCellCost(const point2arr &m_laneCenterLine, double m_cost);

  /// Densifies the given line to the given resolution up to a distance along that line
  /// --- if the distance is longer than the line, then it just goes to the end of the array
  void densifyLine(const point2arr &ptarr, point2arr &newptarr, double res, double distance);

  /// Finds the intersection of the Line of Site vector with the ground plane
  int findLineOfSiteIntersect(float *x, float *y, float *z);

  /// Send PTU command
  int sendPTUcommand(PTUCommand command);

  // World map -- vector based
  Map worldMap;

  // Map talker for worldMap
  CMapElementTalker worldMapTalker;
  
  // receive subgroup
  int recvSubGroup;

  // rndf filename
  string rndfFilename;

  // Our module id (SkyNet)
  modulename moduleId;

  // Resolution of rows in map
  double rowRes;
  
  // Resolution of columns in map
  double colRes;
  
  // No data value for sensor layer
  // No data value for sensor layer
  double sensNoDataVal;
  emap_t sensNoDataVal_emapt;

  // Data value to fill sensor coverage layer
  double sensDataVal;
  emap_t sensDataVal_emapt;
  
  // No data value for gist layer
  double gistNoDataVal;
  emap_t gistNoDataVal_emapt;

  // No data value for cost layer
  emap_t costNoDataVal_emapt;
    
  // No data value for cost layer
  emap_t costNoDataTime_emapt;

  // command Socket for commanding the PTU
  int ptuCommandSocket;

  // planning mode
  PlanningState::Mode planningMode;

  // Counter for sent PTU commands
  int ptuCount;

  // Counter for received gist commands
  int gistCount;

  // Time stamp for last received gist command
  uint64_t gistTime;

  // Time stamp for last sent ptu command
  uint64_t ptuTime;

  // Time stamp for latency computation of one map update
  uint64_t mapTime;

  // how long should we stay still to look down a lane
  double lookTime; 

  // command to PTU
  PTUCommand command; // this is the outgoing command
  PTUCommand lastCommand; // this is the last command sent

  struct timeval stv;

  double lasttime;
  double currenttime;

  CellData maxCostCell;

  CellData last_maxCostCell;

  // a time constant for fast growth rate
  double timeConstantFast;  

  // a time constant for slow growth rate
  double timeConstantSlow;

  // the time constant that will be used
  double timeConstant;

  // for NOMINAL, DRIVE, STOP_OBS, UTURN, or ZONE mode, we want to do a typical sweeping pattern (for now at least)
  // which consists of four modes in itself
  bool sweep;
  int sweepMode;

  sensnet_id_t sensorIds[16];
  int numSensors;

};

#endif
