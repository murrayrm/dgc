
/* 
 * Desc: Window for displaying images
 * Date: 19 June 2007
 * Author: Jeremy Ma (adapted from Sensviewer written by Andrew Howard)
 * CVS: $Id$
*/

#undef border

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <vector>

#include <map/Map.hh>
#include <map/MapElementTalker.hh>
#include <map/MapElement.hh>

#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Multiline_Output.H>
#include <frames/pose3.h>
#include <dgcutils/Fl_Glv_Window.H>
#include <alice/AliceConstants.h>
#include <interfaces/PTUStateBlob.h>

#include "cmdline.h"
#include "glUtils.hh"

enum
  {
    ATTENTION_SENS_LAYER = 0x1000,
    ATTENTION_GIST_LAYER,
    ATTENTION_COST_LAYER,
    
    ATTENTION_VIEW_CENTER_ON_ALICE,
    ATTENTION_VIEW_ALICE_FRAME,
    ATTENTION_VIEW_LOCAL_FRAME,
    
    ATTENTION_LAST,  
  };

struct CellData
{
  double x;
  double y;  
  double cellVal;
};

using std::vector;

// Widget for displaying images
class WorldWin : public Fl_Group
{
  public:

  // Constructor
  WorldWin(int x, int y, int w, int h, int menuh);

  public:

  // Initialize
  int init(double m_rowRes, double m_colRes);

  // Update blob data
  int update(VehicleState m_state, PTUStateBlob m_ptublob, vector<CellData> m_sensvec, vector<CellData> m_gistvec, vector<CellData> m_costvec, vector<MapElement> m_priorMapData, CellData m_currentAttendedCell, CellData m_desiredAttendedCell);

  // Enable sensor Coverage Layer
  static void sensCovLayerEnable(Fl_Widget *w, int option);

  // Enable gist Layer
  static void gistLayerEnable(Fl_Widget *w, int option);

  // Enable cost Layer
  static void costLayerEnable(Fl_Widget *w, int option);

  public:

  // View options
  static void onView(Fl_Widget *w, int option);

  // Prepare for drawing
  static void onPrepare(Fl_Glv_Window *win, WorldWin *self);

  // Draw window
  static void onDraw(Fl_Glv_Window *win, WorldWin *self);

  private:

  // Switch to given frame (homogeneous transform)
  void pushFrame(float m[4][4]);
  
  // Switch to the given frame.
  void pushFrame(pose3_t pose);

  // Revert to previous frame
  void popFrame();

// Switch to the given local frame.
  void pushFrameLocal(VehicleState state);

  // Switch to viewport projection.
  void pushViewport();

  // Revert to non-viewport projection
  void popViewport();

  private:

  // Predraw a ground grid
  void predrawGrid();

  // Predraw the robot
  void predrawAlice();

  // Predraw some layer
  void predrawLayer(vector<CellData> m_vec, int* cellList);

  // Predraw the line of site of the PTU
  void predrawLineOfSite();

  // Predraw lanePolygons
  void predrawPriorMapData(vector<MapElement> m_priorMapData);

  // Predraw attention cells
  void predrawAttentionCells(CellData m_currentAttendedCell, CellData m_desiredAttendedCell);

  // Predraw state data as text
  void drawStateText();

  public:

  // Local menu
  Fl_Menu_Bar *menubar;
  
  // GL window
  Fl_Glv_Window *glwin;

  // Command-line options
  const struct gengetopt_args_info *options;    

  // Most recent state data
  VehicleState state;

  // PTU state
  PTUStateBlob ptublob;

  double colRes;

  double rowRes;

  // Rainbow color scale
  uint8_t rainbow[0x10000][3];

  // Opacity
  uint8_t opacity;

  // Does anything need predrawing?
  bool dirty;  

  bool enableSensCovLayer;
  bool enableGistLayer;
  bool enableCostLayer;

  bool viewAlice;
  bool viewCenterOnAlice;

  // Some display lists
  int aliceList, gridList, lineofsiteList;
  int sensCellList, gistCellList, costCellList;
  int priorMapList;
  int attentionCellList;

};
