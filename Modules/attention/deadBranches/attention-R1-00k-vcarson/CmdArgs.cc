/*!
 * \file CmdArgs.cc
 * \brief Source for the command line arguments object that gets passed around in the planner stack
 *
 * \author Vanessa Carson
 * \date 22 August 2007
 *
 * \ingroup attention
 *
 */

#include "CmdArgs.hh"

int CmdArgs::sn_key = 0;
bool CmdArgs::debug = false;
int CmdArgs::verbose_level = 0;
bool CmdArgs::logging = false;
bool CmdArgs::console = false;
string CmdArgs::log_path = "";
string CmdArgs::log_filename = "tplanner.log";
int CmdArgs::log_level = 0;
