
/* 
 * Desc: Window for displaying images
 * Date: 19 June 2007
 * Author: Jeremy Ma (adapted from Sensviewer written by Andrew Howard)
 * CVS: $Id$
*/

#include "WorldWin.hh"

// Error handling
#define MSG(fmt, ...) \
  (fprintf(stdout, fmt "\n", ##__VA_ARGS__) ? -1 : 0)
#define ERROR(fmt, ...) \
  (fprintf(stderr, "error %s:%d " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : 0)

// Constructor
WorldWin::WorldWin(int x, int y, int w, int h, int menuh)
    : Fl_Group(x, y, w, h)
{
  begin();
  this->menubar = new Fl_Menu_Bar(x, y, w, menuh);
  this->menubar->user_data(this);
  this->glwin = new Fl_Glv_Window(x, y + menuh, w, h - menuh, this,
                                  (Fl_Callback*) onPrepare, (Fl_Callback*) onDraw);
  this->glwin->user_data(this);  
  this->resizable(this->glwin);  
  end();  
  
  // Use DGC convention
  this->glwin->set_up("-z");
  
  // Things are both near and far away, so set a generous clipping range
  this->glwin->set_clip(1, 500);

  // Set initial camera position
  this->glwin->eye_x = -20;
  this->glwin->eye_y = -20;
  this->glwin->eye_z = -20;

  this->menubar->add("&Layer/Sensor Coverage Layer", 0, (Fl_Callback*) WorldWin::sensCovLayerEnable, 
		     (void*)ATTENTION_SENS_LAYER, FL_MENU_RADIO);
  this->menubar->add("&Layer/Gist Layer", FL_CTRL + 'g', (Fl_Callback*) WorldWin::gistLayerEnable, 
		     (void*)ATTENTION_GIST_LAYER, FL_MENU_RADIO);

  this->menubar->add("&Layer/Cost Layer", 0, (Fl_Callback*) WorldWin::costLayerEnable, 
		     (void*)ATTENTION_COST_LAYER, FL_MENU_RADIO | FL_MENU_VALUE);
  this->enableCostLayer = true;

  this->menubar->add("&View/Center on Alice", 'a', (Fl_Callback*) WorldWin::onView, 
		     (void*)ATTENTION_VIEW_CENTER_ON_ALICE, FL_MENU_RADIO | FL_MENU_VALUE);
  this->viewCenterOnAlice = true;

  this->menubar->add("&View/Alice Frame", FL_CTRL + 'a', (Fl_Callback*) WorldWin::onView, 
		     (void*)ATTENTION_VIEW_ALICE_FRAME, FL_MENU_RADIO);

  this->aliceList = 0;
  this->gridList = 0;
  this->lineofsiteList = 0;
  this->sensCellList = 0;
  this->gistCellList = 0;
  this->costCellList = 0;
  this->priorMapList = 0;
  this->attentionCellList = 0;


  return;
}


// Do some initialization
int WorldWin::init(double m_rowRes, double m_colRes)
{
  // Initialize state
  memset(&this->state, 0, sizeof(this->state));

  int i;
  float d;

  // Rainbow color scale
  for (i = 0; i < 0x10000; i++)
    {
      d = 4 * ((double) i / 0x10000);
      
      if (d >= 0 && d < 1.0)
	{
	  this->rainbow[i][0] = 0x00;
	  this->rainbow[i][1] = (int) (d * 0xFF);
	  this->rainbow[i][2] = 0xFF;
	}
      else if (d < 2.0)
	{
	  d -= 1.0;
	  this->rainbow[i][0] = 0x00;
	  this->rainbow[i][1] = 0xFF;
	  this->rainbow[i][2] = (int) ((1 - d) * 0xFF);
	}
      else if (d < 3.0)
	{
	  d -= 2.0;
	  this->rainbow[i][0] = (int) (d * 0xFF);
	  this->rainbow[i][1] = 0xFF;
	  this->rainbow[i][2] = 0x00;
	}
      else if (d < 4.0)
	{
	  d -= 3.0;
	  this->rainbow[i][0] = 0xFF;
	  this->rainbow[i][1] = (int) ((1 - d) * 0xFF);
	  this->rainbow[i][2] = 0x00;
	}
    else
      {
	this->rainbow[i][0] = 0xFF;
	this->rainbow[i][1] = 0x00;
	this->rainbow[i][2] = 0x00;
      }
    }

  this->opacity = 0x6F;
  
  // Store row and column resolution
  this->rowRes = m_rowRes;
  this->colRes = m_colRes;
  
  return 0;
}


// Update blob data using sensnet
int WorldWin::update(VehicleState m_state, PTUStateBlob m_ptublob, vector<CellData> m_sensvec, vector<CellData> m_gistvec, vector<CellData> m_costvec, vector<MapElement> m_priorMapData, CellData m_currentAttendedCell, CellData m_desiredAttendedCell)
{
  this->state = m_state;
  this->ptublob = m_ptublob;

  // If the sensCovLayer is enabled, then draw that
  if (enableSensCovLayer)
    this->predrawLayer(m_sensvec, &this->sensCellList);

  // If the gistLayer is enabled, then draw that
  if (enableGistLayer)
    this->predrawLayer(m_gistvec, &this->gistCellList);

  // If the costLayer is enabled, then draw that
  if (enableCostLayer)
    this->predrawLayer(m_costvec, &this->costCellList);

  this->predrawPriorMapData(m_priorMapData);
  
  this->predrawAttentionCells(m_currentAttendedCell, m_desiredAttendedCell);

  // If the state has changed, update the pose history  
  if (true)      
    this->glwin->redraw();  


  return 0;

}


// Handle menu callbacks
void WorldWin::sensCovLayerEnable(Fl_Widget *w, int option)
{
  WorldWin *self;  

  self = (WorldWin*) w->user_data();
  if (option == ATTENTION_SENS_LAYER)
    self->enableSensCovLayer = !self->enableSensCovLayer;  

  if(self->enableSensCovLayer)
    {
      self->enableGistLayer = false;
      self->enableCostLayer = false;
    }

  return;
}

// Handle menu callbacks
void WorldWin::gistLayerEnable(Fl_Widget *w, int option)
{
  WorldWin *self;  

  self = (WorldWin*) w->user_data();
  if (option == ATTENTION_GIST_LAYER)
    self->enableGistLayer = !self->enableGistLayer;  
  
  if(self->enableGistLayer)
    {
      self->enableSensCovLayer = false;
      self->enableCostLayer = false;
    }

  return;
}

// Handle menu callbacks
void WorldWin::costLayerEnable(Fl_Widget *w, int option)
{
  WorldWin *self;  

  self = (WorldWin*) w->user_data();
  if (option == ATTENTION_COST_LAYER)
    self->enableCostLayer = !self->enableCostLayer;  
  
  if(self->enableCostLayer)
    {
      self->enableSensCovLayer = false;
      self->enableGistLayer = false;
    }

  return;
}

// Prepare for drawing
void WorldWin::onPrepare(Fl_Glv_Window *win, WorldWin *self)
{
  double dx, dy, dz, dr;

  // Compute distance from eye to at point
  dx = win->eye_x - win->at_x;
  dy = win->eye_y - win->at_y;
  dz = win->eye_z - win->at_z;  
  dr = sqrt(dx * dx + dy * dy + dz * dz);

  // Use a heuristic to set the clip planes, based on the distance
  // from the "at" point.
  if (dr < 10)
    win->set_clip(0.1, 50);
  else if (dr < 100)
    win->set_clip(1, 500);
  else
    win->set_clip(10, dr * 10);

  return;
}

// View options
void WorldWin::onView(Fl_Widget *w, int option)
{
  WorldWin *self = (WorldWin*) w->user_data();
  vec3_t eye, at;
  
  if (option == ATTENTION_VIEW_ALICE_FRAME)
  {
    // Default camera viewpoint
    eye = vec3_set(-5, 0, -5);
    at = vec3_set(+10, 0, 0);

    self->glwin->set_lookat(eye.x, eye.y, eye.z, at.x, at.y, at.z, 0, 0, -1);
    self->viewAlice = true;
    self->viewCenterOnAlice = false;
  }

  if (option == ATTENTION_VIEW_CENTER_ON_ALICE)
  {
    eye = vec3_set(-20, -20, -20);
    at = vec3_set(+20, +20, +20);
    
    self->glwin->set_lookat(eye.x, eye.y, eye.z, at.x, at.y, at.z, 0, 0, -1);
    self->viewAlice = false;
    self->viewCenterOnAlice = true;
  }
  
  self->glwin->redraw();
  
  return;
}


// Draw window
void WorldWin::onDraw(Fl_Glv_Window *win, WorldWin *self)
{
  pose3_t pose_local;

  pose_local.pos = vec3_set(self->state.localX,
			    self->state.localY,
			    self->state.localZ);
  pose_local.rot = quat_from_rpy(0,
				 0,
				 self->state.localYaw);

  // Predraw static stuff
  if (self->aliceList == 0)
    self->predrawAlice();

  /*
  if (self->gridList == 0)
    self->predrawGrid();   
  */

  // preDraw Line of site
  if(true)    
    self->predrawLineOfSite();    
  

  // move view to right ref frame
  if(self->viewAlice || self->viewCenterOnAlice)    
    self->pushFrame(pose3_inv(pose_local));

  // Draw Alice (vehicle frame)
  if (true)
    {
      self->pushFrameLocal(self->state);
      glCallList(self->aliceList);
      self->popFrame();
    }
  
  // Draw PTU and line of site
  if(true)
    {      
      self->pushFrameLocal(self->ptublob.state);
      self->pushFrame(self->ptublob.ptu2veh);
      glDrawAxes(0.50);
      self->pushFrame(self->ptublob.tool2ptu);
      glDrawAxes(0.50);

      if(true)
	glCallList(self->lineofsiteList);
      self->popFrame();
      self->popFrame();
      self->popFrame();
    }
  
  // Draw translucent grid
  if (false)
    {      
      glPushMatrix();
      glTranslatef(20 * (int) (self->state.localX/20),
		   20 * (int) (self->state.localY/20),
		   (self->state.localZ + VEHICLE_TIRE_RADIUS));
      glCallList(self->gridList);
      glPopMatrix();
    }
  
  // Switch to viewport frame and draw state data
  if (true)
    {
      self->pushViewport();
      self->drawStateText();
      self->popViewport();
    } 

  // If the sensCovLayer is enabled, then draw that
  if (self->enableSensCovLayer)   
    glCallList(self->sensCellList);
  
  // If the gistLayer is enabled, then draw that
  if (self->enableGistLayer)
    glCallList(self->gistCellList);

  // If the costLayer is enabled, then draw that
  if (self->enableCostLayer)
    glCallList(self->costCellList);
  
  // draw priorMapData
  if(true)
    glCallList(self->priorMapList);

  // draw attention cells
  if(true)
    glCallList(self->attentionCellList);

  if(self->viewAlice || self->viewCenterOnAlice) 
    self->popFrame();

  return;
}


// Switch to given frame (homogeneous transform)
void WorldWin::pushFrame(float m[4][4])
{
  int i, j;
  float t[16];

  // Transpose to column-major order for GL
  for (j = 0; j < 4; j++)
    for (i = 0; i < 4; i++)
      t[i*4+j] = m[j][i];  
  glPushMatrix();
  glMultMatrixf(t);

  return;
}


// Switch to the given frame.
void WorldWin::pushFrame(pose3_t pose)
{
  float m[4][4];
  pose3_to_mat44f(pose, m);  
  return pushFrame(m);
}



// Switch to the given local frame.
void WorldWin::pushFrameLocal(VehicleState state)
{
  pose3_t pose;
  pose.pos = vec3_set(state.localX, state.localY, state.localZ);
  pose.rot = quat_from_rpy(state.localRoll, state.localPitch, state.localYaw);
  return pushFrame(pose);
}


// Revert to previous frame
void WorldWin::popFrame()
{
  glPopMatrix();  
  return;
}


// Switch to viewport projection.
void WorldWin::pushViewport()
{
  int vp[4];
      
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Get the viewport dimensions
  glGetIntegerv(GL_VIEWPORT, vp);
  
  // Shift and rescale such that (-cols/2, 0) is the top-left corner
  // and (+cols/2, rows) is the bottom-right corner.
  glTranslatef(0, 1.0, 0);
  glScalef(2.0/vp[2], -2.0/vp[3], 1.0);
  
  return;
}


// Revert to non-viewport projection
void WorldWin::popViewport()
{
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  return;
}


// Predraw a ground grid
void WorldWin::predrawGrid()
{
  int i, j;

  // Generate display list 
  if (this->gridList == 0)
    this->gridList = glGenLists(1);
  glNewList(this->gridList, GL_COMPILE);
  glPushMatrix();

  glScalef(10, 10, 10);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glBegin(GL_QUADS);
  for (j = -10; j < +10; j++)
  {
    for (i = -10; i < +10; i++)
    {
      glColor3f(0.5, 0.5, 0.5);
      glVertex3f(i + 0, j + 0, 0);
      glVertex3f(i + 1, j + 0, 0);
      glVertex3f(i + 1, j + 1, 0);
      glVertex3f(i + 0, j + 1, 0);
    }
  }
  glEnd();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);
  glBegin(GL_QUADS);
  for (j = -10; j < +10; j++)
  {
    for (i = -10; i < +10; i++)
    {
      glColor4f((i + j + 100) % 2, (i + j + 100) % 2, (i + j + 100) % 2, 0.2);
      glVertex3f(i + 0, j + 0, 0);
      glVertex3f(i + 1, j + 0, 0);
      glVertex3f(i + 1, j + 1, 0);
      glVertex3f(i + 0, j + 1, 0);
    }
  }
  glEnd();
  glDisable(GL_BLEND);
  
  glPopMatrix();

  glEndList();

  return;
}


// Predraw the robot
void WorldWin::predrawAlice()
{
  // Generate display list 
  if (this->aliceList == 0)
    this->aliceList = glGenLists(1);
  glNewList(this->aliceList, GL_COMPILE);

  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
  
  // Draw frame axes
  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 1);
  glEnd();
  
  // Draw outer vehicle dimensions
  glPushMatrix();
  glTranslatef(+(VEHICLE_LENGTH/2-DIST_REAR_TO_REAR_AXLE), 0, -VEHICLE_HEIGHT/2);
  glScalef(VEHICLE_LENGTH, VEHICLE_WIDTH, VEHICLE_HEIGHT - VEHICLE_TIRE_RADIUS);
  glColor3f(0, 0.70, 0);
  glutWireCube(1.0);
  glPopMatrix();

  // Rear left wheel
  glPushMatrix();
  glTranslatef(0, -VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Rear right wheel
  glPushMatrix();
  glTranslatef(0, +VEHICLE_REAR_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();
  
  // Front left wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, -VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  // Front right wheel
  glPushMatrix();
  glTranslatef(VEHICLE_AXLE_DISTANCE, +VEHICLE_FRONT_TRACK/2, 0);
  glRotatef(90, 1, 0, 0);
  glutWireTorus(0.15, VEHICLE_TIRE_RADIUS - 0.15, 16, 64);
  glPopMatrix();

  glEndList();
    
  return;
}

// Predraw the sensor coverage layer
void WorldWin::predrawLayer(vector<CellData> m_vec, int* cellList)
{
  float x_bottomleft, y_bottomleft;
  float x_bottomright, y_bottomright;
  float x_topleft, y_topleft;
  float x_topright, y_topright;
  float groundLevel;
  float cost;
  
  int k;
  
  if (*cellList == 0)
    *cellList = glGenLists(1);
  glNewList(*cellList, GL_COMPILE);
  
  // Draw cubes over each cell
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
  
  for(uint i=0; i<m_vec.size(); i++)
    {
      // Figure out where the nominal ground should be
      groundLevel = this->state.localZ + VEHICLE_TIRE_RADIUS;
      
      cost = -m_vec[i].cellVal + groundLevel; //need the minus sign since -z is up

      // See if there is any data in this cell before we draw it
      if (m_vec[i].cellVal == 0)
        continue;          
      
      // Select color based on elevation relative to vehicle
      // 10 is the color height limit
      k = (int) (0x10000 * (float)(m_vec[i].cellVal) / 10.0); // MAGIC
      if (k < 0x0000) k = 0x0000;
      if (k > 0xFFFF) k = 0xFFFF;

      glColor4ub(this->rainbow[k][0], this->rainbow[k][1], this->rainbow[k][2], this->opacity);     

      /* =================================================================================
       * This function draws a block with the following properties
       * HEIGHT OF BLOCK : meanElevation
       * POSITION OF BLOCK : bottom left base of block is positioned at (northing, easting)
       * WIDTH OF BLOCK : row_resolution
       * LENGTH OF BLOCK : col_resolution
       *
       * THE FOLLOWING DIAGRAM DESCRIBES THE BASE OF THE BLOCK
       *
       *      (x_topleft, y_topleft)               (x_topright, y_topright)
       *               ||================================||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||                                ||
       *               ||================================||
       *      (x_botleft, y_botleft)               (x_botright, y_botright)
       *
       * =================================================================================	   
       */
	  
      x_bottomleft = m_vec[i].x;
      y_bottomleft = m_vec[i].y;
      x_bottomright = m_vec[i].x+this->colRes;
      y_bottomright = m_vec[i].y;
      x_topleft = m_vec[i].x;
      y_topleft = m_vec[i].y+this->rowRes;
      x_topright = m_vec[i].x+this->colRes;
      y_topright = m_vec[i].y+this->rowRes;

      // FRONT FACE
      /*
      glBegin(GL_QUADS);
      glVertex3f(x_bottomleft,y_bottomleft,groundLevel);
      glVertex3f(x_bottomleft,y_bottomleft,cost);
      glVertex3f(x_bottomright,y_bottomright,cost);
      glVertex3f(x_bottomright,y_bottomright,groundLevel);	
      glEnd();
      */	
  
      //LEFT FACE
      /*
      glBegin(GL_QUADS);
      glVertex3f(x_bottomleft,y_bottomleft,groundLevel);
      glVertex3f(x_bottomleft,y_bottomleft,cost);
      glVertex3f(x_topleft,y_topleft,cost);
      glVertex3f(x_topleft,y_topleft,groundLevel);
      glEnd();
      */
	  
      //BACK FACE
      /*
      glBegin(GL_QUADS);
      glVertex3f(x_topleft,y_topleft,groundLevel);
      glVertex3f(x_topleft,y_topleft,cost);
      glVertex3f(x_topright,y_topright,cost);
      glVertex3f(x_topright,y_topright,groundLevel);
      glEnd();
      */
	  
      //RIGHT FACE
      /*
      glBegin(GL_QUADS);
      glVertex3f(x_topright,y_topright,groundLevel);
      glVertex3f(x_topright,y_topright,cost);
      glVertex3f(x_bottomright,y_bottomright,cost);
      glVertex3f(x_bottomright,y_bottomright,groundLevel);
      glEnd();
      */      

  
      //TOP FACE
      glBegin(GL_QUADS);	
      glVertex3f(x_bottomleft,y_bottomleft,cost);
      glVertex3f(x_topleft,y_topleft,cost);
      glVertex3f(x_topright,y_topright,cost);
      glVertex3f(x_bottomright,y_bottomright,cost);	
      glEnd();

	
    }
  
  glEndList();
  
  return;
}


// Generate range point cloud (local frame)
void WorldWin::predrawLineOfSite()
{

  PTUStateBlob *blob;

  blob = &this->ptublob;
  
  if (this->lineofsiteList == 0)
    this->lineofsiteList = glGenLists(1);
  
  glNewList(this->lineofsiteList, GL_COMPILE);

  // Desire to draw a line along the forward pointing unit vector (x-axis) 
  // to it's intersection in the ground plane

  // Draw lines 
  glColor3f(1, 0, 0.5);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glLineWidth(1);
  
  glBegin(GL_LINES);  
  glVertex3f(0,0,0);
  glVertex3f(50,0,0);
  glEnd();

  glEndList();
    
  return;
}

// Predraw pts
void WorldWin::predrawPriorMapData(vector<MapElement> m_priorMapData)
{
  // Figure out where the nominal ground should be
  double groundLevel;
  groundLevel = this->state.localZ + VEHICLE_TIRE_RADIUS;

  MapElement tmpEl;
  // Generate display list 
  if (this->priorMapList == 0)
    this->priorMapList = glGenLists(1);

  glNewList(this->priorMapList, GL_COMPILE);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  if(m_priorMapData.size()>0)
    {
      for(uint i=0; i<m_priorMapData.size(); i++)
	{
	  tmpEl = m_priorMapData[i];
	  if(tmpEl.type==ELEMENT_LANELINE)
	    {
	      glBegin(GL_LINE_STRIP);
	      glColor3f(0, 0, 1);	  
	      for(uint j=0; j<tmpEl.geometry.size(); j++)
		{
		  glVertex3f(tmpEl.geometry[j].x, tmpEl.geometry[j].y,groundLevel);
		}
	      glEnd();
	    }
	}
    }
    
  glEndList();



  return;
}


// Predraw pts
void WorldWin::predrawAttentionCells(CellData m_currentAttendedCell, CellData m_desiredAttendedCell)
{
  // Figure out where the nominal ground should be
  double groundLevel;
  double x_bottomleft, y_bottomleft;
  double x_bottomright, y_bottomright;
  double x_topleft, y_topleft;
  double x_topright, y_topright;
  groundLevel = this->state.localZ + VEHICLE_TIRE_RADIUS;

  // Generate display list 
  if (this->attentionCellList == 0)
    this->attentionCellList = glGenLists(1);
  
  glNewList(this->attentionCellList, GL_COMPILE);

  // paint currently attended cell
  glColor3f(1.0, 1.0, 0.0);   
  x_bottomleft = m_currentAttendedCell.x;
  y_bottomleft = m_currentAttendedCell.y;
  x_bottomright = m_currentAttendedCell.x+this->colRes;
  y_bottomright = m_currentAttendedCell.y;
  x_topleft = m_currentAttendedCell.x;
  y_topleft = m_currentAttendedCell.y+this->rowRes;
  x_topright = m_currentAttendedCell.x+this->colRes;
  y_topright = m_currentAttendedCell.y+this->rowRes;
  
  //BOTTOM FACE
  glBegin(GL_QUADS);	
  glVertex3f(x_bottomleft,y_bottomleft,groundLevel);
  glVertex3f(x_topleft,y_topleft,groundLevel);
  glVertex3f(x_topright,y_topright,groundLevel);
  glVertex3f(x_bottomright,y_bottomright,groundLevel);	
  glEnd();

  // paint goal cell
  glColor3f(1.0, 0.0, 0.0);   
  x_bottomleft = m_desiredAttendedCell.x;
  y_bottomleft = m_desiredAttendedCell.y;
  x_bottomright = m_desiredAttendedCell.x+this->colRes;
  y_bottomright = m_desiredAttendedCell.y;
  x_topleft = m_desiredAttendedCell.x;
  y_topleft = m_desiredAttendedCell.y+this->rowRes;
  x_topright = m_desiredAttendedCell.x+this->colRes;
  y_topright = m_desiredAttendedCell.y+this->rowRes;
  
  //BOTTOM FACE
  glBegin(GL_QUADS);	
  glVertex3f(x_bottomleft,y_bottomleft,groundLevel);
  glVertex3f(x_topleft,y_topleft,groundLevel);
  glVertex3f(x_topright,y_topright,groundLevel);
  glVertex3f(x_bottomright,y_bottomright,groundLevel);	
  glEnd();
	
  glEndList();



  return;
}


// Draw state data as text
void WorldWin::drawStateText()
{
  char text[256];
  char temp[] =
    "Time %.3f\n"
    "Pos %+07.1f %+07.1f %+07.1f\n"
    "Rot  %+06.1f  %+06.1f  %+06.1f\n";

  int vp[4];
  glGetIntegerv(GL_VIEWPORT, vp);

  // Put text in the bottom left corner
  glTranslatef(-vp[2]/2, vp[3] - 64, 0);

  // Hmmm, somewhat hacky way to set the text size.
  // Account for viewport dimensions to preserve 1:1 aspect ratio.
  glScalef(12, -12, 1);
    
  // Construct string
  snprintf(text, sizeof(text), temp,
           fmod((double) this->state.timestamp * 1e-6, 1e4),
           this->state.localX, this->state.localY, this->state.localZ,
           this->state.localRoll * 180/M_PI,
           this->state.localPitch * 180/M_PI,
           this->state.localYaw * 180/M_PI);

  glColor3f(0.7, 0, 0);
  glDrawText(1, text);  

  return;
}
