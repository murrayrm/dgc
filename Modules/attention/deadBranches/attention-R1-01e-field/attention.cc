#include "attention.hh"

//! Default constructor
Attention::Attention()
{
  //memset(this, 0, sizeof(*this));
  this->ptuCount = 0;
  this->gistCount = 0;
  this->heartCount = 0;
  this->ptuTime = DGCgettime();
  this->sweep = false;
  this->fixed = false;
  this->sweepMode = 0;
  this->cycleCount = 0;
  this->cycleAvg = 0;
  this->stopTimeStamped = false;
  this->lastLookTime = 0;

  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  DGCcreateMutex(&m_stateMutex);
  DGCcreateMutex(&m_ptuStateMutex);
  DGCcreateMutex(&m_currentCellMutex);
  DGCcreateMutex(&m_desiredCellMutex);
  DGCcreateMutex(&m_gistvecMutex);
  DGCcreateMutex(&m_costvecMutex);

  return;
}

//! Default destructor
Attention::~Attention()
{
  DGCdeleteMutex(&m_stateMutex);
  DGCdeleteMutex(&m_ptuStateMutex);
  DGCdeleteMutex(&m_currentCellMutex);
  DGCdeleteMutex(&m_desiredCellMutex);
  DGCdeleteMutex(&m_gistvecMutex);
  DGCdeleteMutex(&m_costvecMutex);

  return;
}

//! Command line parser
int Attention::parseCmdLine(int argc, char **argv)
{
 // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return ERROR("Could not parse command line!"); 

  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("attention");

 // Load configuration file second
  if (this->parseConfigFile(this->defaultConfigPath) != 0)
    return -1; 
  
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;  

  // Fill out CSS logging options
  if(this->options.debug_flag) 
    this->debug = true;
  else
    this->debug = false;
  this->verbose_level = this->options.verbose_level_arg;
  if(this->options.logging_flag)
    this->logging = true;
  else
    this->logging = false;
  this->log_path = this->options.log_path_arg;
  this->log_level = this->options.log_level_arg;

  // RNDF stuff
  if(this->options.rndf_given)
    this->rndfFilename.assign(this->options.rndf_arg);
  else
    return ERROR("no rndf specified!!");

  return 0;
}


//! Parse the config file
int Attention::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];

  if(this->options.mini_flag)
    {
      snprintf(filename, sizeof(filename), "%s/%s.CFG",
	       configPath, "ATTENTION_MINI");
    }
  else
    {
      snprintf(filename, sizeof(filename), "%s/%s.CFG",
	       configPath, "ATTENTION");
    }

  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  cout << "parseConfigFile -- moduleId: " << this->moduleId << endl;
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  this->gistNoDataVal = this->options.gist_no_data_value_arg;

  // set the forgetting constant; the larger this value, the slower the cost grows back
  this->timeConstant = this->options.time_constant_arg;

  // Grab map details
  this->recvSubGroup = this->options.receive_subgroup_arg;

  return 0;
}


//! Initialize map
int Attention::initMap(const char *configPath, PlanningState::Mode l_planningMode, PlanningState::ExitPoint l_planningExitPoint)
{  
  // grab state first and center map at alice's pose
  int stateCount = 0;
  while(true)
    {
      sleep(1);
      stateCount++;

      if(stateCount>100)
	return ERROR("Could not get state. initMap failed.");

      if(this->getState() < 0)	
	continue;
      else if(this->getState()==0)
	break;
    }

  //--------------------------------------------------
  // Initiliaze worldMap and load prior data
  //--------------------------------------------------
  // load rndf 
  this->worldMap.data.clear();
  if(!this->worldMap.loadRNDF(this->rndfFilename.c_str()))    
    return ERROR("Error loading rndf!");

  this->worldMap.prior.setLocalToGlobalOffset(this->m_state); 

  if(this->loadMapPriorData()<0)
    return ERROR("Error loading lane lines!");

  //--------------------------------------------------
  // Initialize the attention maps and associated layers
  //--------------------------------------------------
  this->m_gistvec.clear();
  this->m_costvec.clear();

  // set default plannerMode
  this->m_planningMode = l_planningMode;
  this->m_planningPtLabel.segment = l_planningExitPoint.segment;
  this->m_planningPtLabel.lane = l_planningExitPoint.lane;
  this->m_planningPtLabel.point = l_planningExitPoint.waypoint;
  this->worldMap.getWayPoint(this->m_planningPt, this->m_planningPtLabel);

  // grab timestamp for module process speed
  gettimeofday(&stv, 0);
  this->lasttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  this->currentAttendedCell.x = 0;
  this->currentAttendedCell.y = 0;
  this->currentAttendedCell.cellVal = 0;

  this->desiredAttendedCell.x = 0;
  this->desiredAttendedCell.y = 0;
  this->desiredAttendedCell.cellVal = 0;

  return 0;
}

//! Update the map
int Attention::updateMap(PlanningState::Mode l_planningMode, PlanningState::ExitPoint l_planningExitPoint)
{
  // -------------------------------
  // Update state and map locations
  // -------------------------------
  bool recvdNewPlanningPt;

  VehicleState state;

  DGClockMutex(&m_stateMutex);
  state = this->m_state;
  DGCunlockMutex(&m_stateMutex);

  // grab state first and update vehicle location
  if(this->getState()<0)
    printf("error getting state! \n");

  // update map frame offset
  // this also handles site frame offset if needed
  this->worldMap.prior.setLocalToGlobalOffset(state); 

  // ---------------------------------
  // Update the planning state
  //----------------------------------
  if(this->m_planningPtLabel.segment==l_planningExitPoint.segment &&
     this->m_planningPtLabel.lane==l_planningExitPoint.lane &&
     this->m_planningPtLabel.point==l_planningExitPoint.waypoint)
    recvdNewPlanningPt = false;
  else
    recvdNewPlanningPt = true;
  
  if(l_planningMode!=this->m_planningMode || recvdNewPlanningPt)
    {
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);

      DGClockMutex(&m_costvecMutex);
      this->m_costvec.clear();
      DGCunlockMutex(&m_costvecMutex);
      
      this->m_planningMode = l_planningMode;
      this->m_planningPtLabel.segment = l_planningExitPoint.segment;
      this->m_planningPtLabel.lane = l_planningExitPoint.lane;
      this->m_planningPtLabel.point = l_planningExitPoint.waypoint;    
      this->worldMap.getWayPoint(this->m_planningPt, this->m_planningPtLabel);

      this->gistCount++;
      
      sweep = false;    
    }
  
  // load the gistmap layer
  this->loadGistMap();              

  // now fuse the two layers
  this->loadFusedCostMap();
  
  // update heartbeat
  this->heartCount += 1;
  
  // update current time
  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);
  this->cycleAvg = (1/(currenttime-lasttime) + cycleAvg*cycleCount)/(cycleCount+1);
  this->cycleCount++;

  // -------------------------------
  // Update the console display
  // -------------------------------
  if(!this->options.disable_console_flag)
    {
      // update process diagnostics
      cotk_printf(this->console, "%hcap%", A_NORMAL, "%5d ",this->heartCount);
      cotk_printf(this->console, "%mcycle%", A_NORMAL, "%+03.2f ", 1/(currenttime - lasttime));
      cotk_printf(this->console, "%mcycleavg%", A_NORMAL, "%+02.1f ", this->cycleAvg);

      // update vehicle state display      
      //cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
      //		  fmod((double) m_state.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%spos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  state.localX, state.localY, state.localZ);
      cotk_printf(this->console, "%srot%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  state.localRoll*180/M_PI,
		  state.localPitch*180/M_PI,
		  state.localYaw*180/M_PI);
      cotk_printf(this->console, "%gspeed%", A_NORMAL, "%+03.2f", this->groundSpeed);
      
      DGClockMutex(&m_ptuStateMutex);

      // update ptu state display
      cotk_printf(this->console, "%lpan%", A_NORMAL, "%+03.2f",(double)(this->m_ptublob.currpan));
      cotk_printf(this->console, "%ltilt%", A_NORMAL, "%+03.2f",(double)(this->m_ptublob.currtilt));
      cotk_printf(this->console, "%lpanspeed%", A_NORMAL, "%+03.2f", (double)(this->m_ptublob.currpanspeed));
      cotk_printf(this->console, "%ltiltspeed%", A_NORMAL, "%+03.2f", (double)(this->m_ptublob.currtiltspeed));      

      DGCunlockMutex(&m_ptuStateMutex);

      // update latency displays
      cotk_printf(this->console, "%plat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->ptuTime) / 1000);

      // update planning mode display
      switch(this->m_planningMode)
	{
	case PlanningState::NOMINAL:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NOMINAL           ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_LEFT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_LEFT    ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_RIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_RIGHT   ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_STRAIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_STRAIGHT");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::BACKUP:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "BACKUP            ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::DRIVE:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "DRIVE             ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::STOP_OBS:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "STOP_OBS          ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::PASS_LEFT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "PASS_LEFT         ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::PASS_RIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "PASS_RIGHT        ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::UTURN:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "UTURN             ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::ZONE:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "ZONE              ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	}

      cotk_printf(this->console, "%gpoint%", A_NORMAL, "%2d . %2d . %2d  ", 
		  this->m_planningPtLabel.segment,
		  this->m_planningPtLabel.lane,
		  this->m_planningPtLabel.point);
    }

  this->lasttime = this->currenttime; 

  return 0;
  
}


//! Weight Map with prior info from RNDF
int Attention::loadMapPriorData()
{    
  // No need for mutexes here because this function is only called in the initMap function,
  // which is before the other thread is created
  MapElement tmpEl;
  for(int i=0; i< (int)this->worldMap.prior.data.size(); i++)
    {
      this->worldMap.prior.getElFull(tmpEl,i);
      this->m_priorMapData.push_back(tmpEl);
    }

  return 0;

}


//! Load the appropriate gist map
// No need to worry about map mutex locks here; handled above; do need to worry about state mutex though
int Attention::loadGistMap()
{
  double projPoseDist;

  LaneLabel currLaneLabel;
  LaneLabel otherLaneLabel;
  LaneLabel tmpLaneLabel;
  LaneLabel passLaneLabel;

  PointLabel startPtLabel;

  vector<LaneLabel> onComingLaneLabels;
  vector<PointLabel> otherLaneEntryPtLabels;
  vector<PointLabel> stopLinePoints;

  point2arr turnLaneCenterLine_up;
  point2arr turnLaneCenterLine_up_dense;
  point2arr passLaneCenterLine;
  point2arr passLaneCenterLine_dense;
  point2arr passLaneBehindCenterLine;
  point2arr passLaneAheadCenterLine;
  point2arr passLaneCenterLine_shifted;
  point2arr otherLaneCenterLine_up;
  point2arr otherLaneCenterLine_up_dense;

  point2 projPosePt;
  point2 startPt;
  point2 endPt;
  point2 vPt;
  point2 lPt;
  point2 otherPt_tmp;
  point2 adjOppPt;
  point2 posePt; 
  point2 tmpPt;


  VehicleState state;

  bool passingLaneFound;
  bool passLaneIsSameDir;
  bool mapError = false;
  bool adjOppPtIsStopLine = false;

  double heading;
  double cost = 0;
  double projLookBackDist;
  double projLookAheadDist;
  double minDist, ptDist;
  double dist2int;

  int ret = -1;

  DGClockMutex(&m_stateMutex);
  state = this->m_state;
  DGCunlockMutex(&m_stateMutex);
  
  posePt.x = state.localX;
  posePt.y = state.localY;
  heading = state.localYaw;


  // find my lane first
  this->worldMap.getLane(currLaneLabel,posePt);

  // We are given the waypoint to which we are turning to
  // point is m_planningPt and PointLabel is m_planningPtLabel      	   
  if(!this->options.disable_console_flag)
    {
      cotk_printf(this->console, "%seg%", A_NORMAL, "%5d",currLaneLabel.segment);
      cotk_printf(this->console, "%lane%", A_NORMAL, "%5d",currLaneLabel.lane);
    }

  switch(this->m_planningMode)
    {
    case PlanningState::NOMINAL:
      // for nominal driving, we don't use a gistMap so just clear old map
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);
      
      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;	
      this->fixed = false;

      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a     ");

      break;
    case PlanningState::INTERSECT_LEFT:            
      // clear the map
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);

      this->lookTime = RADAR_WAIT_TIME;     
      this->sweep = false;
      this->fixed = false;
      
      // ----------------------------------
      // Get distance to intersection
      // ----------------------------------
      if(this->getDistanceToIntersection(this->m_planningPtLabel, currLaneLabel, state)<0)
	{
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;	
	  this->fixed = false;
	  
	  if(!this->options.disable_console_flag)		
	    cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s","failed!");

	  break;
	}

      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%+03.2f",this->dist2int);
      
      // ----------------------------------------------
      // Check if intersection is too far to be looking
      // ----------------------------------------------
      if(this->dist2int>DIST_TO_INTERSECTION_THRESH)
	{	
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;	
	  this->fixed = false;
	  
	  break;	  
	}

      if(!this->options.mini_flag)
	{
	  // -----------------------------------------------------------------
	  // find all potential lanes that could turn into the desired entryPt and draw cost in those lanes
	  // -----------------------------------------------------------------
	  if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, this->m_planningPtLabel)>0)
	    {
	      
	      for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
		{
		  // want to draw cost for all possible entry lanes other than my own and
		  // don't need to paint up lane for which m_planningPt resides in (that will be done later)
		  if(otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane)
		    {
		      startPtLabel = otherLaneEntryPtLabels[i];
		      continue;
		    }
		  else
		    {
		      // these are lanes which could potentially allow for traffic to turn into my m_planningPt through their exitPts
		      this->worldMap.getWayPoint(otherPt_tmp, otherLaneEntryPtLabels[i]);
		      this->worldMap.getLane(otherLaneLabel,otherPt_tmp);
		      this->worldMap.getLaneCenterLine(otherLaneCenterLine_up, otherLaneLabel);
		      
		      // only paint cost if no stop lines in other lanes
		      if(!this->worldMap.isStopLine(otherLaneEntryPtLabels[i]))
			{
			  // if there is no stop line, we need to paint cost
			  if(otherLaneEntryPtLabels[i].segment==this->m_planningPtLabel.segment && otherLaneEntryPtLabels[i].lane==this->m_planningPtLabel.lane)
			    {
			      otherLaneCenterLine_up.cut_front(this->m_planningPt);			    
			      cost = LANE_NO_STOP_COST_LEVEL2; //give slight preference to area leading up to m_planningPt when that lane has no stop
			    }
			  else
			    {
			      otherLaneCenterLine_up.cut_front(otherPt_tmp);
			      cost = LANE_NO_STOP_COST_LEVEL1;
			    }
			  
			  otherLaneCenterLine_up.reverse();
			  
			  // densify the lane
			  densifyLine(otherLaneCenterLine_up, otherLaneCenterLine_up_dense, 0.50, LOOK_UP_OTHER_LANE_DIST); //resolution of 50cm
			  
			  // paint up the lane
			  if(otherLaneCenterLine_up_dense.size()>0)	     
			    {
			      DGClockMutex(&m_gistvecMutex);
			      ret = getCellCost(otherLaneCenterLine_up_dense, this->m_gistvec, cost);
			      DGCunlockMutex(&m_gistvecMutex);
			    }		      
			}		  
		    }
		}
	    }
	  else
	    {
	      WARN("Could not find other entering lanes!");
	      mapError =true;
	    }            
	}
      
      if(this->options.mini_flag)
	{
	  // --------------------------------
	  // find the starting pt of our turn
	  // --------------------------------
	  if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, this->m_planningPtLabel)>0)
	    {	      
	      for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
		{		  
		  if(otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane)
		    {
		      startPtLabel = otherLaneEntryPtLabels[i];
		      break;
		    }
		}	      
	    }
	  
	  // --------------------------------------------------------------
	  // find stop lines at intersection and store them in stopLinePoints
	  // --------------------------------------------------------------
	  stopLinePoints.clear();      
	  if(this->getIntersectStopLines(startPtLabel, stopLinePoints)!=0)
	    {
	      WARN("Could not get intersect stop lines!");
	      mapError = true;
	    }      
	  
	  // -----------------------------------------------------------------
	  // fill cost up the adjacent lane which the m_planningPt resides
	  // -- by abuse of notation, let "turnLane" refer to the opposing lane(s) adjacent to the lane we are turning into	  
	  // -----------------------------------------------------------------
	  if(this->worldMap.getLane(tmpLaneLabel,this->m_planningPt)<0)
	    {
	      WARN("Could not convert waypoint to lane!");
	      mapError = true;
	    }
	  if(this->worldMap.getOppDirLanes(onComingLaneLabels,tmpLaneLabel)<0)
	    {
	      WARN("Could not get lane labels for adjacent opposiong lanes to waypoint!");
	      mapError = true;
	    }
	  
	  for(int v=0; v < (int)onComingLaneLabels.size(); v++)
	    {
	      otherLaneLabel = onComingLaneLabels[v];
	      turnLaneCenterLine_up.clear();
	      turnLaneCenterLine_up_dense.clear();
	      if(this->worldMap.getLaneCenterLine(turnLaneCenterLine_up, otherLaneLabel)<0) 
		{
		  WARN("Could not get center line for opp lane adjacent to turning lane!");
		  mapError = true;
		}	     	      
	      
	      adjOppPtIsStopLine = false;
	      
	      for(int u=0; u < (int)stopLinePoints.size(); u++)
		{
		  if(stopLinePoints[u].lane==otherLaneLabel.lane &&
		     stopLinePoints[u].segment==otherLaneLabel.segment)
		    {
		      adjOppPtIsStopLine = true;
		      this->worldMap.getWayPoint(adjOppPt, stopLinePoints[u]);
		      break;
		    }
		  
		}
	      
	      // only paint cost if no stop line in other lanes
	      if(!adjOppPtIsStopLine)
		{	   
		  if(this->worldMap.getProjectToLine(adjOppPt, turnLaneCenterLine_up, this->m_planningPt)!=0)
		    {
		      WARN("Could not project point to centerline!");
		      mapError = true;
		    }
		  
		  // give highest cost to the adjacent lanes
		  cost = LANE_NO_STOP_COST_LEVEL3;
		  
		  turnLaneCenterLine_up.cut_front(adjOppPt);
		  turnLaneCenterLine_up.reverse();	      
		  densifyLine(turnLaneCenterLine_up, turnLaneCenterLine_up_dense, 0.50, LOOK_UP_LEFT_LANE_DIST);  //resolution of 50cm
		  if(turnLaneCenterLine_up_dense.size()>0)
		    {
		      DGClockMutex(&m_gistvecMutex);
		      ret = getCellCost(turnLaneCenterLine_up_dense, this->m_gistvec, cost);		      
		      DGCunlockMutex(&m_gistvecMutex);
		    }	      
		}	      	    
	    }
	}

      if(mapError)	
	{
	  WARN("Problem loading gist map for left turn!");
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;
	  this->fixed = false;
	}   
      
      break; 
      
    case PlanningState::INTERSECT_RIGHT:    
      // clear the map
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);
      
      this->lookTime = RADAR_WAIT_TIME;     
      this->sweep = false;
      this->fixed = false;
            
      // ----------------------------------
      // Get distance to intersection
      // ----------------------------------
      if(this->getDistanceToIntersection(this->m_planningPtLabel, currLaneLabel, state)<0)
	{
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;	
	  this->fixed = false;
	  
	  if(!this->options.disable_console_flag)		
	    cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s","failed!");

	  break;
	}

      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%+03.2f",this->dist2int);

      // ----------------------------------------------
      // Check if intersection is too far to be looking
      // ----------------------------------------------
      if(this->dist2int>DIST_TO_INTERSECTION_THRESH)
	{	
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;	
	  this->fixed = false;
	  
	  break;	  
	}

      // -----------------------------------------------------------------
      // find all potential lanes that could turn into the desired entryPt and draw cost in those lanes
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, this->m_planningPtLabel)>0)
	{
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      // want to draw cost for all possible entry lanes other than my own and
	      // don't need to paint up lane for which this->m_planningPt resides in (that will be done later)
	      if(otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane)
		continue;
	      else
		{
		  // these are lanes which could potentially allow for traffic to turn into my this->m_planningPt through their exitPts
		  this->worldMap.getWayPoint(otherPt_tmp, otherLaneEntryPtLabels[i]);
		  this->worldMap.getLane(otherLaneLabel,otherPt_tmp);
		  this->worldMap.getLaneCenterLine(otherLaneCenterLine_up, otherLaneLabel);
		  
		  // only paint cost for lanes with no stop lines
		  if(!this->worldMap.isStopLine(otherLaneEntryPtLabels[i]))
		    {
		      if(otherLaneEntryPtLabels[i].segment==this->m_planningPtLabel.segment && otherLaneEntryPtLabels[i].lane==this->m_planningPtLabel.lane)
			{
			  // give highest cost for lane leading into my turning lane
			  otherLaneCenterLine_up.cut_front(this->m_planningPt);
			  cost = LANE_NO_STOP_COST_LEVEL3;
			}
		      else
			{
			  otherLaneCenterLine_up.cut_front(otherPt_tmp);
			  cost = LANE_NO_STOP_COST_LEVEL1;
			}
		      
		      otherLaneCenterLine_up.reverse();
		      
		      // densify the lane
		      densifyLine(otherLaneCenterLine_up, otherLaneCenterLine_up_dense, 0.50, LOOK_UP_OTHER_LANE_DIST);  //resolution of 50cm
		      
		      // paint up the lane
		      if(otherLaneCenterLine_up_dense.size()>0)		
			{
			  DGClockMutex(&m_gistvecMutex);
			  ret = getCellCost(otherLaneCenterLine_up_dense, this->m_gistvec, cost);
			  DGCunlockMutex(&m_gistvecMutex);
			}
		    }
		  
		}
	    }
	}
      else
	{
	  WARN("Could not find other entering lanes!");
	  mapError =true;
	}      	  	 
      
      
      if(mapError)	
	{
	  WARN("Problem loading gist map for right turn!");
	  this->sweep = true;
	  this->lookTime = SWEEP_WAIT_TIME;

	  this->fixed = false;
	}
      
      break;
      
    case PlanningState::INTERSECT_STRAIGHT:      
      //look for entry point in my current lane segment
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);
      
      this->lookTime = RADAR_WAIT_TIME;
      this->sweep = false;
      this->fixed = false;
            
      // ----------------------------------
      // Get distance to intersection
      // ----------------------------------
      if(this->getDistanceToIntersection(this->m_planningPtLabel, currLaneLabel, state)<0)
	{
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;	
	  this->fixed = false;
	  
	  if(!this->options.disable_console_flag)		
	    cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s","failed!");

	  break;
	}

      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%+03.2f",this->dist2int);

      // ----------------------------------------------
      // Check if intersection is too far to be looking
      // ----------------------------------------------
      if(this->dist2int>DIST_TO_INTERSECTION_THRESH)
	{	
	  this->lookTime = SWEEP_WAIT_TIME;
	  this->sweep = true;	
	  this->fixed = false;
	  
	  break;	  
	}

      // -----------------------------------------------------------------
      // find all potential lanes that could turn into the desired entryPt and draw cost in those lanes
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, this->m_planningPtLabel)>0)
	{
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      // want to draw cost for all possible entry lanes other than my own and	     
	      if( (otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane) || 
		  (otherLaneEntryPtLabels[i].segment==this->m_planningPtLabel.segment && otherLaneEntryPtLabels[i].lane==this->m_planningPtLabel.lane) )
		continue;
	      else
		{
		  // these are lanes which could potentially allow for traffic to turn into my m_planningPt through their exitPts
		  this->worldMap.getWayPoint(otherPt_tmp, otherLaneEntryPtLabels[i]);
		  this->worldMap.getLane(otherLaneLabel,otherPt_tmp);
		  this->worldMap.getLaneCenterLine(otherLaneCenterLine_up, otherLaneLabel);
		  
		  otherLaneCenterLine_up.cut_front(otherPt_tmp);
		  otherLaneCenterLine_up.reverse();
		  
		  // only paint cost for those lanes with no stop lines
		  if(!this->worldMap.isStopLine(otherLaneEntryPtLabels[i]))
		    {
		      double dx =  cos(state.localYaw)*(otherPt_tmp.x - state.localX) + sin(state.localYaw)*(otherPt_tmp.y - state.localY);
		      double dy = -sin(state.localYaw)*(otherPt_tmp.x - state.localX) + cos(state.localYaw)*(otherPt_tmp.y - state.localY);
		      double dTh = atan2(dy,dx);		       

		      cost = LANE_NO_STOP_COST_LEVEL1;
		      // densify the lane
		      densifyLine(otherLaneCenterLine_up, otherLaneCenterLine_up_dense, 0.50, LOOK_UP_OTHER_LANE_DIST);  //resolution of 50cm
		      
		      // paint up the lane
		      if(otherLaneCenterLine_up_dense.size()>0)			
			{
			  if(this->options.mini_flag)
			    {
			      MSG("dx: %f  dy: %f  dTh: %f", dx, dy, dTh);
			      if(dTh <= 0)				
				ret = getCellCost(otherLaneCenterLine_up_dense, this->m_gistvec, cost);				
			    }
			  else
			    {
			      if(dTh > 0)
				{
				  DGClockMutex(&m_gistvecMutex);
				  ret = getCellCost(otherLaneCenterLine_up_dense, this->m_gistvec, cost);
				  DGCunlockMutex(&m_gistvecMutex);
				}
			    }
			}
		    }
		}
	    }
	}
      else
	{
	  WARN("Could not find other entering lanes!");
	  mapError =true;
	}
    
      
      if(mapError)	
	{
	  WARN("Problem loading gist map for straight turn!");
	  this->sweep = true;
	  this->lookTime = SWEEP_WAIT_TIME;

	  this->fixed = false;
	}
      
      break;
      
    case PlanningState::PASS_LEFT:
      // do nominal driving for now
      // for nominal driving, we don't use a gistMap (not yet at least) so just clear old map
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);
            
      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;	
      this->fixed = false;     
      
      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a       ");
      
      break;     
      
      /*
      this->lookTime = RADAR_WAIT_TIME;
      this->sweep = false;
      this->fixed = false;
      
      // find my neighboring lane
      if(this->worldMap.getNeighborLane(passLaneLabel, currLaneLabel,-1) > 0)
	{
	  if(passLaneLabel.segment==0 && passLaneLabel.lane==0)
	    passingLaneFound = false;	
	  else	
	    passingLaneFound = true;
	}
      else
	{ 
	  WARN("Could not find other entering lanes!");
	  mapError =true;
	}    	
      
      if(passingLaneFound)
	{
	  //check if the passing lane is in the same direction or not
	  passLaneIsSameDir = this->worldMap.isLaneSameDir(currLaneLabel,passLaneLabel);
	  
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, passLaneLabel);
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, 0.50, passLaneCenterLine.linelength());  //resolution of 50cm
	  
	  if(!passLaneIsSameDir)	    
	    passLaneCenterLine_dense.reverse();	    
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);	 
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);
	  
	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;
	  
	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();
	  
	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);
	  
	  passLaneBehindCenterLine = passLaneCenterLine_dense;
	  passLaneBehindCenterLine.cut_back(startPt);
	  passLaneBehindCenterLine.cut_front(projPosePt);
	  
	  passLaneAheadCenterLine = passLaneCenterLine_dense;
	  passLaneAheadCenterLine.cut_back(projPosePt);
	  passLaneAheadCenterLine.cut_front(endPt);
	  
	  if(passLaneIsSameDir)
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneBehindCenterLine, this->m_gistvec, PASS_LANE_HIGH_COST);		    
		  DGCunlockMutex(&m_gistvecMutex);
		}
	      
	      if(passLaneAheadCenterLine.size()>0)
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneAheadCenterLine, this->m_gistvec, PASS_LANE_LOW_COST);
		  DGCunlockMutex(&m_gistvecMutex);
		}
	    }
	  else
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneBehindCenterLine, this->m_gistvec, PASS_LANE_LOW_COST);
		  DGCunlockMutex(&m_gistvecMutex);
		}
	      
	      if(passLaneAheadCenterLine.size()>0)
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneAheadCenterLine, this->m_gistvec, PASS_LANE_HIGH_COST);
		  DGCunlockMutex(&m_gistvecMutex);
		}
	    }	      
	}
      else
	{	  
	  WARN("could not find passing lane!");	  
	  // if we go out of lane, then we take our current centerline, truncate it and shift 
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, currLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, 0.50, passLaneCenterLine.linelength());  //resolution of 50cm
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);
	  
	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;
	  
	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();
	  
	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);
	  
	  passLaneCenterLine_dense.cut_back(startPt);
	  passLaneCenterLine_dense.cut_front(endPt);
	  passLaneCenterLine_shifted.clear();
	  
	  // now we need to shift this truncated center line to our right
	  for(uint i=0; i<passLaneCenterLine_dense.size(); i++)
	    {
	      //bring pt to vehicle frame
	      vPt.x = cos(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + sin(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      vPt.y = -sin(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + cos(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      
	      //shift it by shiftDist to the right
	      vPt.y = vPt.y + SHIFT_LEFT_DIST;
	      
	      //bring it back to local frame
	      lPt.x = cos(heading)*vPt.x - sin(heading)*vPt.y + posePt.x;
	      lPt.y = sin(heading)*vPt.x + cos(heading)*vPt.y + posePt.y;
	      
	      passLaneCenterLine_shifted.push_back(lPt);
	      
	    }
	  
	  if(passLaneCenterLine_shifted.size()>0)
	    {
	      DGClockMutex(&m_gistvecMutex);
	      ret = getCellCost(passLaneCenterLine_shifted, this->m_gistvec, PASS_LANE_HIGH_COST);	 					      
	      DGCunlockMutex(&m_gistvecMutex);
	    }	  
	}   	  
      
      break;     

      */

    case PlanningState::PASS_RIGHT:      
      // do nominal driving for now
      // for nominal driving, we don't use a gistMap (not yet at least) so just clear old map
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);
                  
      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;
      this->fixed = false;
      
      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a       ");
      
      break;        
      
      /*
      this->lookTime = RADAR_WAIT_TIME; 
      this->sweep = false;
      this->fixed = false;       
      
      // find my neighboring lane
      this->worldMap.getNeighborLane(passLaneLabel, currLaneLabel,1);
      if(passLaneLabel.segment==0 && passLaneLabel.lane==0) 
	passingLaneFound = false;	
      else	 
	passingLaneFound = true; 
      
      if(passingLaneFound)
	{
	  //check if the passing lane is in the same direction or not
	  passLaneIsSameDir = this->worldMap.isLaneSameDir(currLaneLabel,passLaneLabel);
	  
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, passLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, 0.50, passLaneCenterLine.linelength());  //resolution of 50cm
	  
	  if(!passLaneIsSameDir)	    
	    passLaneCenterLine_dense.reverse();	    
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);
	  
	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;
	  
	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();
	  
	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);
	  
	  passLaneBehindCenterLine = passLaneCenterLine_dense;
	  passLaneBehindCenterLine.cut_back(startPt);
	  passLaneBehindCenterLine.cut_front(projPosePt);
	  
	  passLaneAheadCenterLine = passLaneCenterLine_dense;
	  passLaneAheadCenterLine.cut_back(projPosePt);
	  passLaneAheadCenterLine.cut_front(endPt);
	  
	  if(passLaneIsSameDir)
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneBehindCenterLine, this->m_gistvec, PASS_LANE_HIGH_COST);
		  DGCunlockMutex(&m_gistvecMutex);
		}
	      
	      if(passLaneAheadCenterLine.size()>0)	       
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneAheadCenterLine, this->m_gistvec, PASS_LANE_LOW_COST);
		  DGCunlockMutex(&m_gistvecMutex);
		}
	      
	    }
	  else
	    {
	      if(passLaneBehindCenterLine.size()>0)		
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneBehindCenterLine, this->m_gistvec, PASS_LANE_LOW_COST);
		  DGCunlockMutex(&m_gistvecMutex);
		}
	      
	      if(passLaneAheadCenterLine.size()>0)
		{
		  DGClockMutex(&m_gistvecMutex);
		  ret = getCellCost(passLaneAheadCenterLine, this->m_gistvec, PASS_LANE_HIGH_COST); 
		  DGCunlockMutex(&m_gistvecMutex);
		}
	      
	    }	      
	}
      else
	{	  
	  WARN("could not find passing lane!");	  
	  // if we go out of lane, then we take our current centerline, truncate it and shift 
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, currLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, 0.50, passLaneCenterLine.linelength());  //resolution of 50cm
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);
	  
	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;
	  
	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();
	  
	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);
	  
	  passLaneCenterLine_dense.cut_back(startPt);
	  passLaneCenterLine_dense.cut_front(endPt);
	  passLaneCenterLine_shifted.clear();
	  
	  // now we need to shift this truncated center line to our right
	  for(uint i=0; i<passLaneCenterLine_dense.size(); i++)
	    {
	      //bring pt to vehicle frame
	      vPt.x = cos(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + sin(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      vPt.y = -sin(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + cos(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      
	      //shift it by shiftDist to the right
	      vPt.y = vPt.y + SHIFT_RIGHT_DIST; 
	      
	      //bring it back to local frame
	      lPt.x = cos(heading)*vPt.x - sin(heading)*vPt.y + posePt.x;
	      lPt.y = sin(heading)*vPt.x + cos(heading)*vPt.y + posePt.y;
	      
	      passLaneCenterLine_shifted.push_back(lPt);
	      
	    }
	  
	  if(passLaneCenterLine_shifted.size()>0)
	    {
	      DGClockMutex(&m_gistvecMutex);
	      ret = getCellCost(passLaneCenterLine_shifted, this->m_gistvec, PASS_LANE_HIGH_COST);
	      DGCunlockMutex(&m_gistvecMutex);
	    }
	  
	}
            
      break;

      */     
     
    case PlanningState::STOP_OBS: 
      
      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);
      
      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;
      this->fixed = false;
      
      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a      ");

      break;

    case PlanningState::DRIVE:      

      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);

      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;
      this->fixed = false;

      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a      ");

      break; 

    case PlanningState::BACKUP:

      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);

      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;
      this->fixed = false;

      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a      ");

      break; 

    case PlanningState::UTURN:

      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);

      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;
      this->fixed = false;

      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a     ");

      break; 

    case PlanningState::ZONE:

      DGClockMutex(&m_gistvecMutex);
      this->m_gistvec.clear();
      DGCunlockMutex(&m_gistvecMutex);

      this->lookTime = SWEEP_WAIT_TIME;
      this->sweep = true;
      this->fixed = false;

      // FIX!
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a     ");

      break; 
	}
  
  return 0; 
  
}

//! Fuse the two layers and find the max cell (UTM) at the same time
int Attention::loadFusedCostMap()
{
  double lastfusedVal;
  double newfusedVal;
  double newfusedTime;
  double deltaT;
  double tau;
  double desiredCost;
  double maxCostVal = 0;
  double maxCostx = 0;
  double maxCosty = 0;
  this->maxCostIndex = 0;

  DGClockMutex(&m_gistvecMutex);
  DGClockMutex(&m_costvecMutex);
  if(this->m_costvec.size()==0)
    {
      for(int i=0; i<(int)this->m_gistvec.size(); i++)
	{
	  this->m_costvec.push_back(this->m_gistvec[i]);
	  this->m_costvec[i].time = 1e9;

	  if(this->m_costvec[i].cellVal > maxCostVal)
	    {
	      maxCostVal = this->m_costvec[i].cellVal;
	      maxCostx = this->m_costvec[i].x;
	      maxCosty = this->m_costvec[i].y;
	      this->maxCostIndex = i;
	    }	  
	}
    }
  else
   {
     for(int i=0; i<(int)this->m_costvec.size(); i++)
       {
	 desiredCost = this->m_gistvec[i].cellVal;
	 lastfusedVal = this->m_costvec[i].cellVal;
	 if(lastfusedVal < desiredCost)
	   {
	     tau = this->timeConstant;
	     deltaT = this->m_costvec[i].time;
	     newfusedVal = desiredCost*(1-exp(-deltaT/tau))+0.01;
	     newfusedTime = this->m_costvec[i].time + 1;
	     
	     // if within a threshold of the final cost, then just set equal
	     if(fabs(desiredCost-newfusedVal)<0.25)
	       {
		 newfusedVal = desiredCost;
		 newfusedTime = 1e9;
	       }	      
	   }
	 else
	   {
	     newfusedVal = desiredCost;
	     newfusedTime = 1e9;
	   }	  
	 
	 this->m_costvec[i].time = newfusedTime;
	 this->m_costvec[i].cellVal = newfusedVal;

	 if(newfusedVal > maxCostVal)
	   {
	     maxCostVal = newfusedVal;
	     maxCostx = this->m_costvec[i].x;
	     maxCosty = this->m_costvec[i].y;
	     this->maxCostIndex = i;
	   }
       }
   }
  DGCunlockMutex(&m_costvecMutex);
  DGCunlockMutex(&m_gistvecMutex);
  
  this->maxCostCell.x = maxCostx;
  this->maxCostCell.y = maxCosty;
  this->maxCostCell.cellVal = maxCostVal;      
  
  return(0);

}


//! Do the attending based on current map
int Attention::attend()
{
  float localx, localy, localz;
  float tmpDist;
  double c_to_d_offset;

  float c_pan, c_tilt;
  float c_height;
  bool maxCostCellTooFar = false;

  double panspeed, tiltspeed;
  double currpan, currtilt;
  double timeElapsed;
  bool needLOSCorrection = false;

  VehicleState state;

  DGClockMutex(&m_ptuStateMutex);
  panspeed = this->m_ptublob.currpanspeed;
  tiltspeed = this->m_ptublob.currtiltspeed;
  currpan = this->m_ptublob.currpan;
  currtilt = this->m_ptublob.currtilt;
  DGCunlockMutex(&m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  state = this->m_state;
  DGCunlockMutex(&m_stateMutex);

  // for special cases of intersection, we want to do a pre-look before arriving at the intersection
  if(this->m_planningMode==PlanningState::INTERSECT_LEFT ||
     this->m_planningMode==PlanningState::INTERSECT_RIGHT ||
     this->m_planningMode==PlanningState::INTERSECT_STRAIGHT )
    {
      if(this->dist2int < DIST_TO_INTERSECTION_THRESH && this->dist2int > 0)
	{
	  this->sweep = false;
	  this->fixed = false;	 	  
	  this->lookTime = RADAR_WAIT_TIME;

	  // next check if all cells are below a nominal threshold, then go back to sweeping/fixed
	  if(this->maxCostCell.cellVal < MIN_COST_THRESH && !this->stopTimeStamped)
	    {
	      this->sweep = true;
	      this->fixed = false;
	      this->lookTime = SWEEP_WAIT_TIME;	     
	    }
	  else 
	    {
	      // do a check to make sure max cell is within bounds
	      c_height = (float)state.localZ+VEHICLE_TIRE_RADIUS;
	      this->lineOfSiteToPanTilt(&c_pan, &c_tilt, this->maxCostCell.x, this->maxCostCell.y, c_height);
	      
	      if( (c_pan>MAXPAN || c_pan<MINPAN) || 
		  (c_tilt>MAXTILT || c_tilt<MINTILT) )
		{
		  MSG("LOS pan-tilt out of bounds! maxCell too far");
		  MSG("pan: %f  tilt: %f",c_pan, c_tilt);
		  maxCostCellTooFar = true;
		  
		  this->sweep = true;
		  this->fixed = false;
		  this->lookTime = SWEEP_WAIT_TIME;       	      	 
		}  
	    }
	}
      else if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->sweep = false;
	  this->fixed = true;	  	  
	  this->lookTime = FIXED_WAIT_TIME;
	}
      else
	{
	  this->sweep = true;
	  this->fixed = false;
	  this->lookTime = SWEEP_WAIT_TIME;
	}
    }
  // for non intersection cases, check if alice's speed is above nominal speed; 
  // if so, go into fixed pose
  else 
    {
      if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->sweep = false;
	  this->fixed = true;
	  this->lookTime = FIXED_WAIT_TIME;	 
	}      
    }

  if(!this->options.disable_console_flag)
    {
      if(this->options.mini_flag)
	{
	  if(sweep || fixed)
	    cotk_printf(this->console, "%amode%", A_NORMAL, "%5s", "FIXED  ");	    
	  else	    
	    cotk_printf(this->console, "%amode%",A_NORMAL, "%5s", "LOOKING");
	}
      else
	{
	  if(sweep)	
	    cotk_printf(this->console, "%amode%", A_NORMAL, "%5s", "SWEEPING");	
	  else if(fixed)
	    cotk_printf(this->console, "%amode%", A_NORMAL, "%5s", "FIXED   ");
	  else	
	    cotk_printf(this->console, "%amode%", A_NORMAL, "%5s", "LOOKING ");
	}
      cotk_printf(this->console, "%maxcell%", A_NORMAL, "%+03.2f", this->maxCostCell.cellVal);
    }

  if(sweep)
    {
      // By abuse of notation, "sweep" means fixed for the mini PTU
      if(this->options.mini_flag)
	{
	  this->command.type = RAW;
	  this->command.pan = DP_FIXED_PAN;
	  this->command.tilt = DP_FIXED_TILT;
	  this->command.panspeed = DP_FIXED_PANSPEED;
	  this->command.tiltspeed = DP_FIXED_TILTSPEED;
	}
      else
	{
	  switch(this->sweepMode)
	    {
	    case 0:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_HOME;
	      this->command.tilt = SWEEP_TILTMAX;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;
	      break;
	    case 1:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_HOME;
	      this->command.tilt = SWEEP_TILTMIN;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;
	      break;
	    case 2:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_HOME;
	      this->command.tilt = SWEEP_TILTMAX;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 3:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_PANMAX;
	      this->command.tilt = SWEEP_TILTMIN;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 4:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_PANMAX;
	      this->command.tilt = SWEEP_TILTMAX;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 5:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_PANMAX;
	      this->command.tilt = SWEEP_TILTMIN;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 6:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_HOME;
	      this->command.tilt = SWEEP_TILTMAX;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 7:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_HOME;
	      this->command.tilt = SWEEP_TILTMIN;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 8:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_HOME;
	      this->command.tilt = SWEEP_TILTMAX;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 9:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_PANMIN;
	      this->command.tilt = SWEEP_TILTMIN;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 10:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_PANMIN;
	      this->command.tilt = SWEEP_TILTMAX;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    case 11:
	      this->command.type = RAW;
	      this->command.pan = SWEEP_PANMIN;
	      this->command.tilt = SWEEP_TILTMIN;
	      this->command.panspeed = SWEEP_PANSPEED;
	      this->command.tiltspeed = SWEEP_TILTSPEED;	  
	      break;
	    default:	  
	      this->command.type = RAW;
	      this->command.pan = FIXED_PAN;
	      this->command.tilt = FIXED_TILT;
	      this->command.panspeed = FIXED_PANSPEED;
	      this->command.tiltspeed = FIXED_TILTSPEED;
	      break;
	    }
	}

      // if we've switched states, send new command immediately
      // -- kind of a hack; it uses the look time to determine whether a state has changed; 
      // -- this just requires different states to have different look times
      if(this->lookTime!=this->lastLookTime)
	{
	  // Send PTU command
	  if(this->sendPTUcommand(this->command) <= 0)
	    return ERROR("PTU command not sent!");
	  
	  this->sweepMode++;
	  if(this->sweepMode==12)
	    this->sweepMode=0;
	  
	  // Update ptu command count
	  this->ptuCount += 1;  
	  this->ptuTime = DGCgettime();
	  this->stopTimeStamped = false;
	  
	}
      else
	{	  
	  if(this->options.mini_flag)
	    {
	      //Send a new command if we're not at the deisred pan angle and it's been more than 5 seconds since the last ptu command was sent
	      if((fabs(currpan - DP_FIXED_PAN)> 0.1) &&
		 (this->currenttime - (double)this->ptuTime/1000000) > 5) 
		{	  	  	  
		  // Send PTU command
		  if(this->sendPTUcommand(this->command) <= 0)
		    return ERROR("PTU command not sent!");	      
		  		  
		  // Update ptu command count
		  this->ptuCount += 1;  
		  this->ptuTime = DGCgettime();
		  this->stopTimeStamped = false;		  		
		}
	    }
	  else
	    {
	      if(fabs(panspeed)<= 0.001 && fabs(tiltspeed) <= 0.001 &&
		 (this->currenttime - (double)this->ptuTime/1000000) > SWEEP_TIMEOUT) 
		{	  	  	  
		  // Send PTU command
		  if(this->sendPTUcommand(this->command) <= 0)
		    return ERROR("PTU command not sent!");	      
		  
		  this->sweepMode++;
		  if(this->sweepMode==12)
		    this->sweepMode=0;
		  
		  // Update ptu command count
		  this->ptuCount += 1;  
		  this->ptuTime = DGCgettime();
		  this->stopTimeStamped = false;		  		  
		}
	    }       	  

	  if(!this->options.mini_flag)
	    {
	      // find cell location of current line of site
	      if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
		{
		  DGClockMutex(&m_currentCellMutex);
		  this->currentAttendedCell.x = localx;
		  this->currentAttendedCell.y = localy;
		  this->currentAttendedCell.cellVal = 0;
		  DGCunlockMutex(&m_currentCellMutex);
		}
	      
	      // since the nominal driving is a fixed set of sweeping modes, we'll 
	      // just send the attended cell to plot to be where alice is
	      DGClockMutex(&m_desiredCellMutex);
	      this->desiredAttendedCell.x = state.localX;
	      this->desiredAttendedCell.y = state.localY;
	      this->desiredAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_desiredCellMutex);	  	      
	    }	  
	}

      if(!this->options.disable_console_flag)
	{
	  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	  cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", this->lookTime);
	  cotk_printf(this->console, "%loserror%", A_NORMAL, "%7s", "n/a       ");	     	     
	  cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", this->currenttime-(double)this->ptuTime/1000000);	      
	  if(this->options.mini_flag)
	    cotk_printf(this->console, "%asubmode%", A_NORMAL, "%5s", "n/a  ");
	  else
	    cotk_printf(this->console, "%asubmode%", A_NORMAL, "%5d", this->sweepMode);

	}
      
      //reset sweep to false
      this->sweep = false;
      
    }
  else if(fixed)
    { 
      if(this->options.mini_flag)
	{
	  this->command.type = RAW;
	  this->command.pan = DP_FIXED_PAN;
	  this->command.tilt = DP_FIXED_TILT;
	  this->command.panspeed = DP_FIXED_PANSPEED;
	  this->command.tiltspeed = DP_FIXED_TILTSPEED;
	}
      else
	{
	  this->command.type = RAW;
	  this->command.pan = FIXED_PAN;
	  this->command.tilt = FIXED_TILT;
	  this->command.panspeed = FIXED_PANSPEED;
	  this->command.tiltspeed = FIXED_TILTSPEED;
	}

      // if we've switched states, send new command immediately
      // -- kind of a hack; it uses the look time to determine whether a state has changed; 
      // -- this just requires different states to have different look times
      if(this->lookTime!=this->lastLookTime)
	{
	  // Send PTU command
	  if(this->sendPTUcommand(this->command) <= 0)
	    return ERROR("PTU command not sent!");
	  	  
	  // Update ptu command count
	  this->ptuCount += 1;  
	  this->ptuTime = DGCgettime();
	  this->stopTimeStamped = false;
	  
	}
      else
	{
	  // if for some reason we're not moving but we're still not at desired fixed pose, 
	  // send a command; i doubt we'll ever enter this section, but just to be safe
	  if(this->options.mini_flag)
	    {
	      if(fabs(currpan - DP_FIXED_PAN)> 0.1 &&
		 (this->currenttime - (double)this->ptuTime/1000000) > 5) 
		{
		  if(this->sendPTUcommand(this->command) <= 0)
		    return ERROR("PTU command not sent!");	      
		  
		  // Update ptu command count
		  this->ptuCount += 1;  
		  this->ptuTime = DGCgettime();
		  this->stopTimeStamped = false;
		  
		}
	    }
	  else
	    {
	      if(fabs(panspeed)<=0.001 && fabs(tiltspeed)<=0.001 &&
		 (this->currenttime - (double)this->ptuTime/1000000) > 3)
		{
		  // Send PTU command
		  if(this->sendPTUcommand(this->command) <= 0)
		    return ERROR("PTU command not sent!");
		  
		  // Update ptu command count
		  this->ptuCount += 1;  
		  this->ptuTime = DGCgettime();
		  this->stopTimeStamped = false;		  
		}
	    }

	  // find cell location of current line of site
	  if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
	    {
	      DGClockMutex(&m_currentCellMutex);
	      this->currentAttendedCell.x = localx;
	      this->currentAttendedCell.y = localy;
	      this->currentAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_currentCellMutex);
	    }
	  
	  // since we're at a fixed pose, we'll 
	  // just send the attended cell to plot to be where alice is
	  DGClockMutex(&m_desiredCellMutex);
	  this->desiredAttendedCell.x = state.localX;
	  this->desiredAttendedCell.y = state.localY;
	  this->desiredAttendedCell.cellVal = 0;
	  DGCunlockMutex(&m_desiredCellMutex);	  	  	
	}
      
      if(!this->options.disable_console_flag)
	{
	  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	  cotk_printf(this->console, "%alooktime%", A_NORMAL, "%5s", "INF   ");
	  cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", this->currenttime-(double)this->ptuTime/1000000);
	  cotk_printf(this->console, "%asubmode%", A_NORMAL, "%5s", "n/a  ");
	  cotk_printf(this->console, "%loserror%", A_NORMAL, "%7s", "n/a     ");		     
	}

      //reset fixed to false
      this->fixed = false;
    }
  else
    {
      // if we've gotten this far, then we're not going to be doing any sweeping or fixed pose
      this->command.type = LINEOFSITE;
      this->command.localx = this->maxCostCell.x;
      this->command.localy = this->maxCostCell.y;
      this->command.localz = state.localZ+VEHICLE_TIRE_RADIUS;
      this->command.panspeed = RADAR_PANSPEED;
      this->command.tiltspeed = RADAR_TILTSPEED;    
               
      // if we've switched states, send new command immediately
      if(this->lookTime!=this->lastLookTime)
	{
	  DGClockMutex(&m_desiredCellMutex);
	  this->desiredAttendedCell.x = this->command.localx;
	  this->desiredAttendedCell.y = this->command.localy;
	  this->desiredAttendedCell.cellVal = 0;
	  DGCunlockMutex(&m_desiredCellMutex);	  
	  
	  // Send PTU command
	  if(this->sendPTUcommand(this->command) <= 0)
	    return ERROR("PTU command not sent!");	 
	  
	  // Update ptu command count
	  this->ptuCount += 1;  
	  this->ptuTime = DGCgettime();
	  this->stopTimeStamped = false;
	  
	  if(!this->options.disable_console_flag)
	    {
	      cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	      cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
			  this->command.localx, this->command.localy, this->command.localz);
	      cotk_printf(this->console, "%asubmode%", A_NORMAL, "%7s", "n/a     ");
	      cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
	    }      
	}
      else
	{
	  if(findLineOfSiteIntersect(&localx, &localy, &localz)==0 || this->options.mini_flag)
	    {
	      // at this point, there should have already been sent a command to the ptu to move
	      
	      DGClockMutex(&m_currentCellMutex);
	      this->currentAttendedCell.x = localx;
	      this->currentAttendedCell.y = localy;
	      this->currentAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_currentCellMutex);	      	    
	      	    
	      // ---------------------------------------------------------------
	      // to handle the case when we've just arrived at our look position
	      // ----------------------------------------------------------------
	      // if 1) we're slow enough
	      //    2) we haven't started counting our stopped wait time
	      //    3) and it has been more than 1/10 of a second since our last sent PTU command
	      //    4) we're withint DIST_THRESH of the line of site commanded pose

	      DGClockMutex(&this->m_desiredCellMutex);
	      tmpDist = sqrt(pow(this->desiredAttendedCell.x - localx,2) + 
			     pow(this->desiredAttendedCell.y - localy,2));
	      DGCunlockMutex(&this->m_desiredCellMutex);

	      if(panspeed == 0 && tiltspeed == 0 && !stopTimeStamped &&
		 (this->currenttime - (double)this->ptuTime/1000000) > 0.1 &&
		 tmpDist <= CELL_DIST_THRESH) //it's been more than 0.1s since last command
		{
		  stopTimeStamped = true;
		  this->stopTime = (double)(DGCgettime())/1000000;
		}
	      
	      // -----------------------------------------------------------------------------------------
	      // to handle the case when we've stopped panning/tilting but we're looking at the wrong spot
	      // -----------------------------------------------------------------------------------------
	      // sometimes the commanded cell location is unreached because we plan the cell location
	      // from where alice was a split second ago which is enough to offset the lineofsite intersect
	      // and the desired cell; i could either increase the sensing cone that pushes down cell costs,
	      // or i could re-issue a correction to the lineofsite look command; i'll do the latter
	      DGClockMutex(&this->m_desiredCellMutex);
	      c_to_d_offset  = sqrt(pow(localx-this->desiredAttendedCell.x,2)+
				    pow(localy-this->desiredAttendedCell.y,2));
	      DGCunlockMutex(&this->m_desiredCellMutex);

	      if(c_to_d_offset> CELL_DIST_THRESH)		    
		needLOSCorrection = true;
	      else
		needLOSCorrection = false;

	      if(!this->options.disable_console_flag)
		{
		  cotk_printf(this->console, "%loserror%", A_NORMAL, "%+03.2f", c_to_d_offset);
		}	      
	      
	      if(needLOSCorrection && //panspeed == 0 && tiltspeed == 0
		 (this->currenttime - (double)this->ptuTime/1000000) > 1) //send every second
		{		      
		  DGClockMutex(&this->m_desiredCellMutex);
		  this->command.type = LINEOFSITE;
		  this->command.localx = this->desiredAttendedCell.x;
		  this->command.localy = this->desiredAttendedCell.y;
		  this->command.localz = state.localZ+VEHICLE_TIRE_RADIUS;
		  this->command.panspeed = RADAR_PANSPEED;
		  this->command.tiltspeed = RADAR_TILTSPEED;
		  DGCunlockMutex(&this->m_desiredCellMutex);
		  
		  // print message to screen about needLOSCorrection
		  MSG("issuing a LOS correction; error: %f", c_to_d_offset);
		  
		  // Send PTU command
		  if(this->sendPTUcommand(this->command) <= 0)
		    return ERROR("PTU command not sent!");		  	      
		  
		  // Update ptu command count
		  this->ptuCount += 1;  
		  this->ptuTime = DGCgettime();		      
		  
		  if(!this->options.disable_console_flag)
		    {
		      cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
		      cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
				  this->command.localx, this->command.localy, this->command.localz);
		      cotk_printf(this->console, "%asubmode%", A_NORMAL, "%7s", "n/a      ");
		      cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
		      cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", timeElapsed);
		    }      
		} // end if (needLOScorrection)


	      // -------------------------------------------------------------
	      // to handle the case when we've stopped and started counting our stop timer
	      // -------------------------------------------------------------
	      // if we've stamped the stop time
	      if(stopTimeStamped)	    
		{
		  timeElapsed = this->currenttime - this->stopTime;		
		  
		  DGClockMutex(&m_costvecMutex);
		  this->m_costvec[this->maxCostIndex].cellVal = 0.01;
		  this->m_costvec[this->maxCostIndex].time = 0;
		  DGCunlockMutex(&m_costvecMutex);
		  
		  if(!this->options.disable_console_flag)
		    {		
			  cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
			  cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", timeElapsed);
		    }		  
		}
	      else //we haven't stamped the stop time because we're still panning/tilting
		timeElapsed = -1;

	      // this is when we're done waiting; we need to move on to next max cell
	      if(panspeed == 0 && tiltspeed == 0 && timeElapsed > lookTime && stopTimeStamped)
		{
		  if(this->maxCostCell.cellVal < MIN_COST_THRESH)
		    {		      
		      this->sweep = true;
		      this->lookTime = SWEEP_WAIT_TIME;
		      this->stopTimeStamped = false;
		    }
		  else
		    {
		      DGClockMutex(&m_desiredCellMutex);
		      this->desiredAttendedCell.x = this->command.localx;
		      this->desiredAttendedCell.y = this->command.localy;
		      this->desiredAttendedCell.cellVal = 0;
		      DGCunlockMutex(&m_desiredCellMutex);
		      
		      // Send PTU command
		      if(this->sendPTUcommand(this->command) <= 0)
			return ERROR("PTU command not sent!");		  	      
		      
		      // Update ptu command count
		      this->ptuCount += 1;  
		      this->ptuTime = DGCgettime();
		      this->stopTimeStamped = false;
		      
		      if(!this->options.disable_console_flag)
			{
			  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
			  cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
				      this->command.localx, this->command.localy, this->command.localz);
			  cotk_printf(this->console, "%asubmode%", A_NORMAL, "%7s", "n/a      ");
			  cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
			  cotk_printf(this->console, "%astoptime%", A_NORMAL, "%7s", "n/a     ");
			}      
		    }      
		}	      	
	    } // end if (findLineOfSiteIntersect)	    		
	} // end if (lookTime!=lastLookTime) else
    } // end if not sweep
  
  this->lastLookTime = this->lookTime;
 
  return 0;
}



//! Assigns the given cost to the cell in the array that is furthest from Alice yet still in the map
int Attention::getCellCost(const point2arr &laneCenterLine, vector<CellData> &cellVec, double cost)
{
  float furthestCellX, furthestCellY;
  double poseX, poseY;
  double maxDist = 0;
  double tmpDist;
  CellData tmpCell;
  memset(&tmpCell, 0, sizeof(tmpCell));
  
  DGClockMutex(&this->m_stateMutex);
  poseX = this->m_state.localX;
  poseY = this->m_state.localY;
  DGCunlockMutex(&this->m_stateMutex);  

  if(laneCenterLine.size()>1)
    {
      for(int i=0; i < (int)laneCenterLine.size(); i++ )
	{	  
	  tmpDist = sqrt(pow(poseX-laneCenterLine[i].x,2) + pow(poseY-laneCenterLine[i].y,2));
	  if(tmpDist > maxDist)
	    {
	      furthestCellX = (float)laneCenterLine[i].x;
	      furthestCellY = (float)laneCenterLine[i].y;
	      maxDist = tmpDist;
	    }    
	}
      
      // should do some checking on bounds here
      tmpCell.x = furthestCellX;
      tmpCell.y = furthestCellY;
      tmpCell.cellVal = cost;
      cellVec.push_back(tmpCell);
    }

  return (int)cellVec.size();
}


//! Densifies the given line to the given resolution up to a distance along that line
//! --- if the distance is longer than the line, then it just goes to the end of the array
void Attention::densifyLine(const point2arr &ptarr, point2arr &newptarr, double res, double distance)
{
  double actualDistance;
  double ptHeading;
  point2 nextPt;

  newptarr.clear();
  newptarr.push_back(ptarr[0]);

  //check to make sure the desired distance is not longer than the measured length of the line
  if(distance>=ptarr.linelength())
    actualDistance = ptarr.linelength();
  else
    actualDistance = distance;

  //now densify the line
  for(int i=0; i<(int)(ptarr.size()-1); i++)
    {
      // if next pt is within res distance, add it to the vector list
      if(ptarr[i].dist(ptarr[i+1])<res)	
	{

	  newptarr.push_back(ptarr[i+1]);
	  //check distance
	  if(newptarr.linelength()>=actualDistance)
	    return;
	}
      else
	{
	  nextPt = ptarr[i];
	  ptHeading = atan2(ptarr[i+1].y-ptarr[i].y,ptarr[i+1].x-ptarr[i].x);
	  for(int j=0; j<(long int)floor(ptarr[i].dist(ptarr[i+1])/res); j++)
	    {
	      nextPt.x = res*cos(ptHeading)+nextPt.x;
	      nextPt.y = res*sin(ptHeading)+nextPt.y;

	      newptarr.push_back(nextPt);	      
	      //check distance
	      if(newptarr.linelength()>=actualDistance)
		return;

	    }

	  newptarr.push_back(ptarr[i+1]);
	  //check distance
	  if(newptarr.linelength()>=actualDistance)
	    return;
	}
    }
  return;
  
}

//! Finds the intersection of the Line of Site vector with the ground plane
int Attention::findLineOfSiteIntersect(float *x, float *y, float *z)
{
  vec3_t pointInPlane;
  vec3_t normalToPlane;
  vec3_t linePt1, linePt2;
  float px,py,pz;
  float vx,vy,vz;
  float lx,ly,lz;

  float d,t;
  float numerator, denominator;

  PTUStateBlob *ptublob;
  VehicleState state;

  DGClockMutex(&this->m_ptuStateMutex);
  ptublob = &this->m_ptublob;
  DGCunlockMutex(&this->m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  state = this->m_state;
  DGCunlockMutex(&m_stateMutex);

  PTUStateBlobToolToPTU(ptublob,0,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt1 = vec3_set(lx,ly,lz);
  
  PTUStateBlobToolToPTU(ptublob,50,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt2 = vec3_set(lx,ly,lz);

  pointInPlane = vec3_set(state.localX, state.localY, state.localZ+VEHICLE_TIRE_RADIUS);
  normalToPlane = vec3_set(0, 0, 1);

  d = vec3_dot(pointInPlane, normalToPlane);
  
  denominator = normalToPlane.x*(linePt2.x-linePt1.x) + normalToPlane.y*(linePt2.y-linePt1.y) + normalToPlane.z*(linePt2.z-linePt1.z);
  if(denominator==0) // the intersection does not exist    
    return(-1);
  else
    {
      numerator = d - normalToPlane.x*linePt1.x - normalToPlane.y*linePt1.y - normalToPlane.z*linePt1.z;
      t = numerator/denominator;
      *x = linePt1.x + t*(linePt2.x - linePt1.x);
      *y = linePt1.y + t*(linePt2.y - linePt1.y);
      *z = linePt1.z + t*(linePt2.z - linePt1.z);
      return 0;
    }
}

//! Converts a line of site pose to corresponding pan-tilt values
void Attention::lineOfSiteToPanTilt(float *pan, float *tilt, float const &localx, float const &localy, float const &localz)
{
  PTUStateBlob *ptublob;
  float vx,vy,vz;
  float px,py,pz;
  float L, X, zeta, beta;

  DGClockMutex(&this->m_ptuStateMutex);
  ptublob = &this->m_ptublob;
  DGCunlockMutex(&this->m_ptuStateMutex);
    
  PTUStateBlobLocalToVehicle(ptublob, 
			     localx, localy, localz,
			     &vx, &vy, &vz);
  
  PTUStateBlobVehicleToPTU(ptublob,
			   vx,vy,vz,
			   &px, &py, &pz);
  
  L = sqrt(pow(px,2)+pow(py,2));
  X = sqrt(pow(L,2)+pow(pz+BASE_TO_TILTAXIS,2));  
  zeta = atan2(pz, L);
  beta = 0;
   
  *pan = (atan2(py, px))*180/M_PI; //must be in degrees 
  *tilt = -(zeta+beta)*180/M_PI;

  return;

}

//! Get intersection stop lines
int Attention::getIntersectStopLines(const PointLabel startPtLabel, vector<PointLabel> &stopLinePoints)
{
  vector<PointLabel> WayPointExits, WayPoint;
  vector<PointLabel> EntryPoints;

  // Obtain all WayPointExits for the initial startPtLabel
  this->worldMap.getWayPointExits(WayPointExits, startPtLabel);
  
  for (unsigned int i=0; i<WayPointExits.size(); i++) 
    {
      this->worldMap.getWayPointEntries(WayPoint,WayPointExits[i]);
      
      // Look whether WayPointEntry already exists in list
      for (unsigned int j=0; j<WayPoint.size(); j++) 
	{
	  bool found=false;
	  for (unsigned int k=0; k<EntryPoints.size(); k++) 
	    {
	      if (WayPoint[j]==EntryPoints[k]) 
		{
		  found=true;
		  break;
		}
	    }
	  
	  // If not, add it to list and call function recursively
	  if (!found)
	    EntryPoints.push_back(WayPoint[j]);	      

	}
    }

  for(unsigned int u=0; u < EntryPoints.size(); u++)
    {
      if(this->worldMap.isStopLine(EntryPoints[u]))	
	stopLinePoints.push_back(EntryPoints[u]);	
    }

  return 0;
}

//! Check is planning pt is too far
bool Attention::isPlanningPtTooFar(const PointLabel planningPtLabel, const LaneLabel currLaneLabel, const VehicleState state)
{
  // will send true only if planned intersection is found and is too far
  // will send false all other times 
  vector<PointLabel> otherLaneEntryPtLabels;  
  PointLabel startPtLabel;
  bool foundStart = false;
  point2 planningPt;
  point2 statePt;
  point2arr laneCenterLine;
  double dist1, dist2;
  double dist2int;

  statePt.x = state.localX;
  statePt.y = state.localY;

  if(this->worldMap.getLaneCenterLine(laneCenterLine, currLaneLabel)<=0)
    {
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%s","get centerline failed!");

      return false;
    }

  if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, planningPtLabel)>0)
    {      
      for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	{
	  if(otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane)
	    {
	      startPtLabel = otherLaneEntryPtLabels[i];
	      foundStart = true;
	      break;	      
	    }
	}      
      if(foundStart)
	{
	  if(this->worldMap.getWayPoint(planningPt, planningPtLabel)==0)
	    {
	      this->worldMap.getDistAlongLine(dist1, laneCenterLine, planningPt);
	      this->worldMap.getDistAlongLine(dist2, laneCenterLine, statePt);
	      dist2int = fabs(dist1-dist2);
	      
	      if(!this->options.disable_console_flag)		
		cotk_printf(this->console, "%dist2int%", A_NORMAL, "%+03.2f",dist2int);
	      
	      if(dist2int > DIST_TO_INTERSECTION_THRESH)		
		return true;		 		
	      else
		return false;
	    }
	  else	    
	    {
	      if(!this->options.disable_console_flag)		
		cotk_printf(this->console, "%dist2int%", A_NORMAL, "%s","get waypt failed!");
	      return false;	    	    
	    }
	}
      else
	{
	  if(!this->options.disable_console_flag)		
	    cotk_printf(this->console, "%dist2int%", A_NORMAL, "%s","found start failed!");
	  return false;	
	}
    } 
  else
    {
      if(!this->options.disable_console_flag)		
	cotk_printf(this->console, "%dist2int%", A_NORMAL, "%s","found start failed!");
      return false;
    }

}


//! Get distance to upcoming intersection
int Attention::getDistanceToIntersection(const PointLabel planningPtLabel, const LaneLabel currLaneLabel, const VehicleState state)
{
  // will send true only if planned intersection is found and is too far
  // will send false all other times 
  vector<PointLabel> otherLaneEntryPtLabels;  
  PointLabel startPtLabel;
  bool foundStart = false;
  point2 planningPt;
  point2 statePt;
  point2arr laneCenterLine;
  double dist1, dist2;

  statePt.x = state.localX;
  statePt.y = state.localY;

  // set to -1 by default; 
  this->dist2int = -1;

  if(this->worldMap.getLaneCenterLine(laneCenterLine, currLaneLabel)<=0)
    return -1;
    

  if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, planningPtLabel)>0)
    {      
      for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	{
	  if(otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane)
	    {
	      startPtLabel = otherLaneEntryPtLabels[i];
	      foundStart = true;
	      break;	      
	    }
	}      
      if(foundStart)
	{
	  if(this->worldMap.getWayPoint(planningPt, planningPtLabel)==0)
	    {
	      this->worldMap.getDistAlongLine(dist1, laneCenterLine, planningPt);
	      this->worldMap.getDistAlongLine(dist2, laneCenterLine, statePt);
	      this->dist2int = fabs(dist1-dist2);
	      return 0;	      	      
	    }
	  else	    
	    return -1;	    	    	    
	}
      else
	return -1;		
    } 
  else
    return -1;
}

//! Initialize sensnet 
int Attention::initSensnet()
{  
  // Initialize SensNet 
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  cout << "initSensnet -- moduleId: " <<  this->moduleId << endl;
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0) 
    return ERROR("unable to connect to sensnet"); 
  
  // Subscribe to the PTU state messages
  if(this->options.mini_flag)
    {
      if (sensnet_join(this->sensnet, SENSNET_LF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(PTUStateBlob)) != 0)
	return ERROR("unable to join PTU state group");
    }
  else
    {
      if (sensnet_join(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(PTUStateBlob)) != 0)
	return ERROR("unable to join PTU state group");
    }

  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to process state messages
  if(!this->options.disable_process_control_flag)
    {
      if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
	return ERROR("unable to join process group");
    }

  return 0;
}

//! Finalize sensnet
int Attention::finiSensnet()
{
  // Leave the state group
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);

  // Leave the PTU state group 
  if(this->options.mini_flag)    
    sensnet_leave(this->sensnet, SENSNET_LF_PTU, SENSNET_PTU_STATE_BLOB);
  else
    sensnet_leave(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB);

  // Leave the ProcessControl group
  if(!this->options.disable_process_control_flag)
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);

  // Disconnect
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}

//! Initialize skynet
int Attention::initSkynet()
{
  // Initialize sockets for receiving gistmap and sending ptucommands
  this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);

  if(this->options.mini_flag)
    this->ptuCommandSocket = this->m_skynet->get_send_sock(SNptuMiniCommand);
  else
    this->ptuCommandSocket = this->m_skynet->get_send_sock(SNptuCommand);

  if(this->ptuCommandSocket < 0)
    return ERROR("attention::initSkynet(): skynet get_send_sock returned error");

  return 0;
}

//! Finalise skynet
int Attention::finiSkynet()
{
  delete this->m_skynet;
  return 0;
}

//! Get Alice and PTU state
int Attention::getState()
{
  int ptublobId, stateblobId;

  DGClockMutex(&m_stateMutex);
  // Default to all zeros in state
  memset(&this->m_state, 0, sizeof(this->m_state));
  // Get the current state value
  if (sensnet_read(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate,
                   &stateblobId, sizeof(this->m_state), &this->m_state) != 0)
    return ERROR("unable to read state data");
  this->groundSpeed = sqrt(pow(this->m_state.utmNorthVel,2) + pow(this->m_state.utmEastVel,2));
  DGCunlockMutex(&m_stateMutex);  

  DGClockMutex(&m_ptuStateMutex);
  memset(&this->m_ptublob, 0, sizeof(this->m_ptublob));
  if(this->options.mini_flag)
    {
      if (sensnet_read(this->sensnet, SENSNET_LF_PTU, 
		       SENSNET_PTU_STATE_BLOB, &ptublobId, 
		       sizeof(this->m_ptublob), &this->m_ptublob) != 0)
	return ERROR("Could not read ptu state");
    }
  else
    {
      if (sensnet_read(this->sensnet, SENSNET_MF_PTU, 
		       SENSNET_PTU_STATE_BLOB, &ptublobId, 
		       sizeof(this->m_ptublob), &this->m_ptublob) != 0)
	return ERROR("Could not read ptu state");
    }
  DGCunlockMutex(&m_ptuStateMutex);

  return 0;
}


//! Get the process state
int Attention::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


//! Send PTU command
int Attention::sendPTUcommand(PTUCommand l_command)
{
  int numBytesSent;
  numBytesSent = m_skynet->send_msg(this->ptuCommandSocket, &l_command, sizeof(l_command), 0);
  return numBytesSent;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"Attention $SKYNET$ :: %spread%                                           \n"
"-------------------------------------------------------------------------\n"
"MODULE RATE                         |   CURRENT LANE                     \n"
"Hz: %mcycle% (%mcycleavg%)          |   Segment : %seg%                  \n"
"Heartbeat: %hcap%                   |   Lane    : %lane%                 \n"
"------------------------------------|------------------------------------\n"
"PLANNING MODE                       |   ATTENDING STATES                 \n"
"Mode      : %gmode%                 |   Mode      : %amode%              \n"
"Exit Pt   : %gpoint%                |   subMode   : %asubmode%           \n"          
"Num recvd : %gcap%                  |   LookTime  : %alooktime%          \n"
"------------------------------------|   StopTime  : %astoptime%          \n"
"PTU STATE                           |>> MAXCELL   : %maxcell%            \n"
"rawpan      (deg): %lpan%           |   LOS error : %loserror%           \n"
"rawtilt     (deg): %ltilt%          |   Dist2Int  : %dist2int%           \n"
"panspeed  (deg/s): %lpanspeed%      |------------------------------------\n" 
"tiltspeed (deg/s): %ltiltspeed%     |   ALICE STATE (LOCAL FRAME)        \n"
"------------------------------------|   Pos    : %spos%                  \n" 
"PTU COMMANDS                        |   Rot    : %srot%                  \n" 
"Number Sent  : %pcap%               |   Speed  : %gspeed%                \n"
"Command      : %plos%               |------------------------------------\n"
"Latency      : %plat%               |       [%QUIT% | %PAUSE%]           \n"
"-------------------------------------------------------------------------\n"
"%stderr%                                                                 \n"   
"%stderr%                                                                 \n";  
  

//! Initialize console display
int Attention::initConsole()
{   
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  if (cotk_open(this->console, "attention.msg") != 0)
    return -1;
  
  // Display spread id
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
 
  // Display some initial values
  cotk_printf(this->console, "%gcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%pcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NO MODE        ");
  cotk_printf(this->console, "%gpoint%", A_NORMAL, "%2d . %2d . %2d  ", 0, 0, 0);

  // FIX!
  cotk_printf(this->console, "%dist2int%", A_NORMAL, "%7s", "n/a    ");

  return 0;
}

//! Finalize console display
int Attention::finiConsole()
{
  if (!this->options.disable_console_flag)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}

//! Console button callback
int Attention::onUserQuit(cotk_t *console, Attention *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

//! Console button callback
int Attention::onUserPause(cotk_t *console, Attention *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


void idle(void* param)
{ 

  Attention* self = (Attention*) param;

  CellData tmpCurrAttCell, tmpGoalAttCell;

  VehicleState state;
  PTUStateBlob ptublob;

  DGClockMutex(&self->m_stateMutex);
  state = self->m_state;
  DGCunlockMutex(&self->m_stateMutex);

  DGClockMutex(&self->m_ptuStateMutex);
  ptublob = self->m_ptublob;
  DGCunlockMutex(&self->m_ptuStateMutex);

  DGClockMutex(&self->m_currentCellMutex);
  tmpCurrAttCell.x = self->currentAttendedCell.x;
  tmpCurrAttCell.y = self->currentAttendedCell.y;
  tmpCurrAttCell.cellVal = self->currentAttendedCell.cellVal;
  DGCunlockMutex(&self->m_currentCellMutex);

  DGClockMutex(&self->m_desiredCellMutex);
  tmpGoalAttCell.x = self->desiredAttendedCell.x;
  tmpGoalAttCell.y = self->desiredAttendedCell.y;
  tmpGoalAttCell.cellVal = self->desiredAttendedCell.cellVal;
  DGCunlockMutex(&self->m_desiredCellMutex);

  // update the map
  DGClockMutex(&self->m_gistvecMutex);
  DGClockMutex(&self->m_costvecMutex);
  self->worldwin->update(state, ptublob, self->m_gistvec, self->m_costvec, self->m_priorMapData, tmpCurrAttCell, tmpGoalAttCell);
  DGCunlockMutex(&self->m_costvecMutex);
  DGCunlockMutex(&self->m_gistvecMutex);
}

//! Initialize the visualizer
void Attention::initVisualizer(int *argc, char** argv)
{
  glutInit(argc, argv);
}

//! Start visualizer thread
void Attention::v_startVisualizerThread()
{
  int cols = 512;
  int rows = 384;

  Fl_Menu_Item menuitems[] =
    {
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"&Pause", FL_CTRL + 'p', (Fl_Callback*) Attention::v_onAction, (void*) 0x1000, FL_MENU_TOGGLE},
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) Attention::v_onExit},
      {0},
      {0},     
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Attention Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Create the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new WorldWin(0, 30, cols, rows - 30, 30);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Initialize image window
  // -- no need for mutex around options because it's not used after this call in other thread
  this->worldwin->init(this->options.col_resolution_arg, this->options.row_resolution_arg);

  // Idle callback
  Fl::add_idle(idle, this);

  // Run
  this->mainwin->show();
  while (!this->quit)
    Fl::wait();
   
  return;

}

//! Handle menu callbacks
void Attention::v_onExit(Fl_Widget *w, int option)
{
  Attention *self;

  self = (Attention*) w->user_data();
  self->quit = true;

  return;
}


//! Handle menu callbacks
void Attention::v_onAction(Fl_Widget *w, int option)
{
  Attention *self;  

  self = (Attention*) w->user_data();
  if (option == 0x1000)
    self->pause = !self->pause;
  
  return;
}
