///
/// \file AttentionModule.hh
/// \brief 
/// 
/// \author Jeremy Ma Vanessa Carson
/// \date 15 August 2007
/// 


#ifndef ATTENTIONMODULE_HH_
#define ATTENTIONMODULE_HH_



#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include <gcinterfaces/AttentionPlannerState.hh>
#include <sys/stat.h>
#include "attention.hh"

class AttentionModule : public GcModule
{


public:
  /// Constructor of AttentionModule
  AttentionModule();

  /// Destructor of AttentionModule
  ~AttentionModule();

  /// Arbitration for the AttentionModule. It computes the next
  /// merged directive based on the directives from the latest
  /// received traj and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);

  /// Control for the AttentionModule. It computes and sends
  /// directives to the PTU based on the 
  /// merged directive */
  void control(ControlStatus*, MergedDirective*);

  /// Initialize communication with the PTU to receive its state and also to send commands
  int initialize(int argc, char* argv[]);

private:

  /// GcInterface variable for sending responses to planner
  PlannerStateInterface::Northface* m_plannerStateNF;

 /*!\param control status sent from control to arbiter */
  ControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  MergedDirective m_mergedDirective;

  /*!\param planning mode  */
  PlanningState::Mode m_planningMode;

  Attention *m_attention;

};

#endif //ATTENTIONMODULE_HH
