#include "AttentionModule.hh"
#include "CmdArgs.hh"
#include "dgcutils/DGCutils.hh"
#include "dgcutils/cfgfile.h"
#include <getopt.h>
#include "cmdline.h"
#include <sys/stat.h>

int main(int argc, char **argv)              
{  
  gengetopt_args_info cmdline;      
  string logFileName; 

  /* Get the skynet key */
  CmdArgs::sn_key = skynet_findkey(argc, argv);
 
  /* Parse command-line arguments */
  if (cmdline_parser(argc, argv, &cmdline) != 0)
    exit (1);

  /* Is console enabled? */
  CmdArgs::console = !cmdline.disable_console_given;


  struct stat st;

  /* Generate a unique filename to log to */
  if(CmdArgs::logging) {
    string tmpRNDFname;
  
    ostringstream oss; 

    char timestr[64];
    time_t t = time(NULL);
    strftime(timestr, sizeof(timestr), "%F-%a-%H-%M", localtime(&t)); 
    oss << "attention-" << "." << timestr << ".log";
    logFileName = oss.str();
    string suffix = "";

    /* if it exists already, append .1, .2, .3 ... */
    for (int i = 1; stat((logFileName + suffix).c_str(), &st) == 0; i++) {
      ostringstream tmp;
      tmp << '.' << i;
      suffix = tmp.str();
    }
    logFileName += suffix;
    CmdArgs::log_filename = logFileName;
  }

   /* Display that console is not enabled */
   if (!CmdArgs::console){
     cout << "No display" << endl;
     cout << "Constructing skynet with KEY = " << CmdArgs::sn_key << endl;
     cout << "debug = " << CmdArgs::debug << endl;
   }

  /* Initializes Planner and start the gcmodule */
  AttentionModule* attention = new AttentionModule();
  attention->Start();
  /* Refreshing the console */
  unsigned int cnt = 0;
  while (true) {
    sleep(5);

     /* Refresh console every 100 changes
      * Refreshing every changes would create flickers on the
      * terminal (not very nice for the eye) */

     cnt++;
     if (cnt > 100) {
       cnt = 0;
     }
  }

  return 0;
}
