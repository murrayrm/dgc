#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <interfaces/GistMapMsg.h>

using namespace std;

int main(int argc, char* argv[])
{
 
  int modeCommandSocket;
  int sendSuccess;
  int type;
  PlanningMode modeCommand;
 
  skynet m_skynet = skynet(217, atoi(getenv("SKYNET_KEY")), NULL); 
  modeCommandSocket = m_skynet.get_send_sock(SNgist);
  if(modeCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }

  while(true)
    {

      cout << "What would you like to do? " << endl;
      cout << "0: INTERSECT_LEFT" << endl;
      cout << "1: INTERSECT_RIGHT " << endl;
      cout << "2: INTERSECT_STRAIGHT" << endl;
      cout << "3: BACKUP" << endl;
      cout << "4: DRIVE" << endl;
      cout << "5: STOP_OBS" << endl;
      cout << "6: ZONE" << endl;
      cin >> type;

      if(type==0)
	{
	  modeCommand = INTERSECT_LEFT;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==1)
	{
	  modeCommand = INTERSECT_RIGHT;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==2)
      	{
	  modeCommand = INTERSECT_STRAIGHT;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==3)
	{
	  modeCommand = BACKUP;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==4)
	{
	  modeCommand = DRIVE;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==5)
	{
	  modeCommand = STOP_OBS;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==6)
	{
	  modeCommand = ZONE;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else
	printf("Unrecognized type. Please try again\n");
    }
      
  return(0);
}
