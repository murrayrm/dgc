#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>

#include <dgcutils/cfgfile.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <gcinterfaces/AttentionPlannerState.hh>

using namespace std;

int main(int argc, char* argv[])
{
 
  int modeCommandSocket;
  int pointCommandSocket;
  int sendSuccess;
  int type;
  int segment, lane, waypoint;
  PlanningState::Mode modeCommand;
  PlanningState::ExitPoint exitPoint;
 
  skynet m_skynet = skynet(217, atoi(getenv("SKYNET_KEY")), NULL); 
  modeCommandSocket = m_skynet.get_send_sock(SNgist);
  pointCommandSocket = m_skynet.get_send_sock(SNmark1);
  if(modeCommandSocket < 0 || pointCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }

  while(true)
    {

      cout << "What would you like to do? " << endl;
      cout << "0: INTERSECT_LEFT" << endl;
      cout << "1: INTERSECT_RIGHT " << endl;
      cout << "2: INTERSECT_STRAIGHT" << endl;
      cout << "3: PASS_LEFT" << endl;
      cout << "4: PASS_RIGHT " << endl;
      cout << "5: UTURN" << endl;
      cout << "6: BACKUP" << endl;
      cout << "7: DRIVE" << endl;
      cout << "8: STOP_OBS" << endl;
      cout << "9: ZONE" << endl;
      cin >> type;

      if(type==0)
	{
	  modeCommand = PlanningState::INTERSECT_LEFT;	  
	  cout << "what exit waypoint ? " << endl;
	  cin >> segment >> lane >> waypoint;
	  exitPoint.segment = segment;
	  exitPoint.lane = lane;
	  exitPoint.waypoint = waypoint;
	  sendSuccess = m_skynet.send_msg(pointCommandSocket, &exitPoint, sizeof(exitPoint), 0);
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==1)
	{
	  modeCommand = PlanningState::INTERSECT_RIGHT;	  
	  cout << "what exit waypoint ? " << endl;
	  cin >> segment >> lane >> waypoint;
	  exitPoint.segment = segment;
	  exitPoint.lane = lane;
	  exitPoint.waypoint = waypoint;
	  sendSuccess = m_skynet.send_msg(pointCommandSocket, &exitPoint, sizeof(exitPoint), 0);
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==2)
      	{
	  modeCommand = PlanningState::INTERSECT_STRAIGHT;	  
	  cout << "what exit waypoint ? " << endl;
	  cin >> segment >> lane >> waypoint;
	  exitPoint.segment = segment;
	  exitPoint.lane = lane;
	  exitPoint.waypoint = waypoint;
	  sendSuccess = m_skynet.send_msg(pointCommandSocket, &exitPoint, sizeof(exitPoint), 0);
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==3)
	{
	  modeCommand = PlanningState::PASS_LEFT;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==4)
	{
	  modeCommand = PlanningState::PASS_RIGHT;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==5)
	{
	  modeCommand = PlanningState::UTURN;
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==6)
	{
	  modeCommand = PlanningState::BACKUP;
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==7)
	{
	  modeCommand = PlanningState::DRIVE;
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==8)
	{
	  modeCommand = PlanningState::STOP_OBS;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else if(type==9)
	{
	  modeCommand = PlanningState::ZONE;	  
	  sendSuccess = m_skynet.send_msg(modeCommandSocket, &modeCommand, sizeof(modeCommand), 0);
	}
      else
	printf("Unrecognized type. Please try again\n");
    }
      
  return(0);
}
