              Release Notes for "attention" module

This file documents API, usage, portability etc. changes that have been
introduced in new versions of the "attention" module. This
information should be kept in mind when upgrading to newer versions of
the module. This file may also documment major bug fixes in so far as
they may impact upgrade decisions. More complete and detailed
information on changes to the "attention" module can be found in
the ChangeLog file.

Release R1-00e-jerma (Sun Aug 26 14:36:20 2007):
	A couple of major commits happening here so I'll list them one by one:
* Removed the cmap class altogether and implemented the emap class which draws polygons much faster (no more latency issues)
** to implement the above change, an emapWrapper class has been written (existing only within this module) that implements a handful of useful cmap 
class functions yet without the overhead of deltas and so forth -- will eventually move it to the emap module
* Re-implemented all three layers that consitute the attention architecture with the new map structure (i.e. a sensor-coverage layer, a gist-layer 
based on what planning's current mode is, and a final cost layer which is a fused version of the above two)
** drawing polygons is no longer a blocker
** the following planner states now affect the movement of the PTU: INTERSECT_RIGHT, INTERSECT_LEFT, INTERSECT_STRAIGHT, PASS_LEFT, PASS_RIGHT
** still have yet to implement the NOMINAL, DRIVE, STOP_OBS, and ZONE planning modes (will come in the next release)
** control of the PTU is based on the peak value in the fused cost layer; some tweeking still needed to prevent the PTU from 
swiveling around in a jerky motion
* code has been merged in with Vanessa's AttentionModule shells
** in the process of doing the above change, the CmdArgs.hh/cc have become obsolete (though still svn watched since I need to check with Vanessa 
before 'svn rm'-ing them)


Release R1-00e (Thu Aug 23 17:20:38 2007):
	Added AttentionMain and CmdArgs for running AttentionModule. 
Release R1-00d (Thu Aug 23 13:34:07 2007):
	Changed AttentionModule to get latest directive from planner (vs. oldest one).  AttentionModule now receives the NOMINAL
state as well.    

Release R1-00c (Mon Aug 20 13:40:00 2007):
	Added AttentionModule class.  It derives from gcmodule and servs as a communication 
shell for state updates from planner.  Changed Makefile to account for gcmodule 
dependencies.  Cleaned some warnings in attention.cc.  
	

Release R1-00b (Fri Aug 17  5:23:28 2007):
	Fixed the planning modes to be consistent with what planner actually uses (according to Kristian and 
Vanessa). This version of the attention module uses blocks for cost in the costlayer of the map (as opposed 
to Gaussian curves in the previous release). Panning and tilting of the PTU seem a lot smoother now. Error 
handling still needs to be implemented so this is a fragile release.

Release R1-00a (Thu Aug 16 14:31:30 2007):
	this release has a lot updates yet the module is not in a feasible 
working condition (the PTU swings too much -- almost out of control in 
simulation). 
* The sensor coverage layer is in place but problems with the drawPolygon 
function have been a blocker. 
* The gist layers have been added for most planning states.
* Panning and Tilting commands are currently being sent to the PTU based 
on peak cost cell
* cotk display implemented. 
all that's left now is fine tuning to get the PTU to move smoothly and not 
so jerky. Making a release now so Vanessa can add her AttentionModule shell 
for CSS implementation. 


Release R1-00 (Tue Jun 26 15:39:48 2007):
	Created.





