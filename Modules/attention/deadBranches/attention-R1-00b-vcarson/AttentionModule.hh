///
/// \file AttentionModule.hh
/// \brief 
/// 
/// \author Jeremy Ma Vanessa Carson
/// \date 15 August 2007
/// 


#ifndef ATTENTIONMODULE_HH_
#define ATTENTIONMODULE_HH_



#include <gcmodule/GcModule.hh>
#include <gcmodule/GcInterface.hh>
#include <gcinterfaces/AdriveCommand.hh>
#include <gcinterfaces/AttentionPlannerState.hh>



class AttentionModule : public GcModule
{


public:
  /// Constructor of AttentionModule
  AttentionModule();

  /// Destructor of AttentionModule
  ~AttentionModule();

  /// Arbitration for the AttentionModule. It computes the next
  /// merged directive based on the directives from the latest
  /// received traj and latest control status */
  void arbitrate(ControlStatus*, MergedDirective*);

  /// Control for the AttentionModule. It computes and sends
  /// directives to gcdrive based on the 
  /// merged directive and outputs the control status
  /// based on the latest status from gcdrive. */
  void control(ControlStatus*, MergedDirective*);

private:

  /// GcInterface variable for sending responses to planner
  PlannerStateInterface::Northface* m_plannerStateNF;

 /*!\param control status sent from control to arbiter */
  ControlStatus m_controlStatus;

  /*!\param merged directive sent from arbiter to control */
  MergedDirective m_mergedDirective;

  /*!\param planning mode  */
  PlanningMode m_planningMode;
};

#endif //ATTENTIONMODULE_HH
