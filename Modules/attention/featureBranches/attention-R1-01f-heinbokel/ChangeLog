Mon Nov 12  1:54:34 2007	Jeremy Ma (jerma)

	* version R1-01f
	BUGS:  
	FILES: Makefile.yam(46878), attention.cc(46878),
		attention.hh(46878)
	field fix; looks longer at intersections now (about 300 seconds)

	FILES: attention.hh(46931)
	NQE run 3

Thu Oct 25  4:11:19 2007	Jeremy Ma (jerma)

	* version R1-01e
	BUGS:  
	FILES: AttentionModule.cc(46551), attention.cc(46551),
		attention.hh(46551)
	field commits.

Tue Oct 23 10:48:42 2007	Jeremy Ma (jerma)

	* version R1-01d
	BUGS:  
	New files: ATTENTION_MINI.CFG
	FILES: Makefile.yam(46267), attention.cc(46267),
		attention.hh(46267), cmdline.ggo(46267)
	changes to account for control of new PTU unit; also had the
	parseConfigFile and parseCmdLine functions out of order; it's a
	surprise it hasn't caused any spread disconnects.

Sat Oct 20  2:00:24 2007	Jeremy Ma (jerma)

	* version R1-01c
	BUGS:  
	FILES: Makefile.yam(45611)
	fixing makefile to include the -lrndf library for compatibility
	with latest map release.

Fri Oct 19  3:09:21 2007	Jeremy Ma (jerma)

	* version R1-01b
	BUGS:  
	FILES: attention.hh(45519)
	increasing pan tilt speeds.

Wed Oct 17  0:13:45 2007	Jeremy Ma (jerma)

	* version R1-01a
	BUGS:  
	FILES: attention.cc(45136)
	minor changes; no longer need to look left since the new radar
	location takes care of that.

Sun Oct 14  2:00:37 2007	Joshua Doubleday (jdoubled)

	* version R1-01
	BUGS:  
Sat Oct 13 17:03:10 2007	Jeremy Ma (jerma)

	* version R1-00z
	BUGS:  
	FILES: WorldWin.cc(44402), attention.cc(44402), attention.hh(44402)
	more changes to move back to local frame (site frame doesn't work
	since PTUfeeder works in local frame). Added a pre-look status that
	checks oncoming traffic earlier on (i.e. if we're within some
	threshold distance of the intersection) so that we don't waste time
	panning and tilting when we arrive at the intersection.

	FILES: attention.hh(44403)
	increased threshold distance to intersection to 30m

Thu Oct 11 19:41:29 2007	Jeremy Ma (jerma)

	* version R1-00y
	BUGS:  
	FILES: AttentionModule.cc(44200), attention.cc(44200),
		attention.hh(44200), temp-attention.cc(44200)
	reverted back to using the local frame (the PTUfeeder works in the
	local frame so the line of site commands were getting all out of
	whack when specified in the site frame); added a distance to
	intersection check

	FILES: attention.cc(44203), attention.hh(44203)
	increased tilt and pan nodding speed and also added
	map.prior.setLocalToGlobalOffset() to the loop.

Tue Oct  9  4:26:46 2007	Jeremy Ma (jerma)

	* version R1-00x
	BUGS:  
	FILES: WorldWin.cc(43733), attention.cc(43733), attention.hh(43733)
	a couple of changes: 1) only command looking of the radar for
	intersections that have lanes with no stop lines that could effect
	our current planned path 2) nod faster 3) use alice site frame pose
	4) reduced the line of site correction threshold

	FILES: attention.cc(43734), attention.hh(43734)
	minor change in header file

Sun Oct  7  0:15:58 2007	Jeremy Ma (jerma)

	* version R1-00w
	BUGS:  
	FILES: UTattention.cc(43402), attention.cc(43402),
		attention.hh(43402)
	merged

	FILES: UTattention.cc(43394), attention.cc(43394),
		attention.hh(43394)
	added better algorithm for determining which lanes need to be
	looked down with the radar (dependent on whether a given lane has a
	stop line or not); removed emap dependencies; increased wait time
	for radar track initialization; changed sweeping behavior to give
	more ground strike coverage

Sat Oct  6 23:57:57 2007	Sam Pfister (sam)

	* version R1-00v
	BUGS:  
	FILES: attention.cc(43386)
	added frame transformation to updateMap function to handle floating
	local frame

Thu Oct  4  2:52:48 2007	Jeremy Ma (jerma)

	* version R1-00u
	BUGS:  
	New files: UTattention.cc UTsend.cc temp-attention.cc
	FILES: ATTENTION.CFG(42535), WorldWin.cc(42535),
		WorldWin.hh(42535), attention.cc(42535),
		attention.hh(42535)
	a couple of modifications: took out the emap class; adding nodding
	instead of sweeping behavior for the PTU; added error checking for
	commanded pan-tilt angles that might result in a hard limit

	FILES: Makefile.yam(42576), attention.cc(42576),
		attention.hh(42576)
	removed more emap stuff and slowed down the CPU time significantly

	FILES: Makefile.yam(42753), WorldWin.cc(42753),
		attention.cc(42753), attention.hh(42753)
	fixed bug that prevented updating growing cost; implmented now.

	FILES: Makefile.yam(42799)
	adding some unit test files and a temp-attention script to give the
	groundstrike filtering what it needs until I can get the broken
	communication with planner fixed.

	FILES: attention.hh(42603)
	increasing some threshold values

Thu Sep 27  0:38:42 2007	Jeremy Ma (jerma)

	* version R1-00t
	BUGS:  
	Deleted files: attentionMain.cc send_test.cc
	FILES: Makefile.yam(40696)
	removing some poorly named files that were distinguished by case
	only. 

Fri Sep 21 11:58:43 2007	Sam Pfister (sam)

	* version R1-00s
	BUGS:  
	FILES: attention.cc(39808)
	updated to  work with renamed setLocalToGlobalOffset map function

Tue Sep 18 11:11:05 2007	Jeremy Ma (jerma)

	* version R1-00r
	BUGS:  
	FILES: AttentionModule.cc(39281), attention.cc(39281),
		attention.hh(39281)
	adding nodding behavior for nominal driving above speeds of 0.50m/s

	FILES: attention.cc(39282)
	typo.

Mon Sep 17  8:46:08 2007	Sam Pfister (sam)

	* version R1-00q
	BUGS:  
	FILES: attention.cc(39107)
	Updated map calls to allow for non-static local to global frame
	transform

Fri Sep 14  1:34:45 2007	Jeremy Ma (jerma)

	* version R1-00p
	BUGS:  
	FILES: attention.cc(38714)
	made tilt speed a constant value. minor change. 

Tue Sep 11 18:59:23 2007	Jeremy Ma (jerma)

	* version R1-00o
	BUGS:  
	New files: beta/ATTENTION.CFG beta/AttentionModule.cc
		beta/AttentionModule.hh beta/attention.cc beta/attention.hh
		beta/attentionMain.cc beta/cmdline.ggo
	FILES: ATTENTION.CFG(38389), attention.cc(38389),
		attention.hh(38389), cmdline.ggo(38389)
	vastly simplified the logic in this module; it was too complicated
	for what alice is currently capable of doing; now just does the
	basic sweeping and "look left, look right"-type behavior at
	intersections.

Mon Sep 10 17:36:30 2007	Jeremy Ma (jerma)

	* version R1-00n
	BUGS:  
	FILES: AttentionModule.cc(38182), attention.cc(38182),
		attention.hh(38182), attentionMain.cc(38182),
		send_test.cc(38182)
	tweeked costs so that preference is given to specific lanes that
	are most important in certain intersection scenarios

	FILES: attention.cc(37791)
	cleaned up some unused variables

Thu Sep  6 19:43:59 2007	Jeremy Ma (jerma)

	* version R1-00m
	BUGS:  
	FILES: AttentionModule.cc(37684), attention.cc(37684),
		attention.hh(37684), attentionMain.cc(37684)
	implemented changes to incorporate the given exit pt, as opposed to
	trying to compute it.

	FILES: AttentionModule.cc(37757), attention.cc(37757),
		attention.hh(37757)
	adjusted some threshold variables, increased how far down specific
	lanes the PTU should look, and integrated the exitPt specification
	given by planner now.

	FILES: attention.cc(37759), attention.hh(37759)
	added doxygen comments

Wed Sep  5 13:21:43 2007	vcarson (vcarson)

	* version R1-00l
	BUGS:  
	FILES: AttentionModule.cc(37629), AttentionModule.hh(37629)
	Added the receipt of an exit waypoint in a message of type
	PlannerState. 

Fri Aug 31  1:37:59 2007	Jeremy Ma (jerma)

	* version R1-00k
	BUGS:  
	FILES: AttentionModule.cc(36770)
	modified some sleep times so that this module doesn't run at 95Hz.

Fri Aug 31  0:23:32 2007	Jeremy Ma (jerma)

	* version R1-00j
	BUGS:  
	Deleted files: emapWrapper.cc emapWrapper.hh
	FILES: Makefile.yam(36758), WorldWin.cc(36758),
		attention.cc(36758), attention.hh(36758)
	fixed a few small bugs that would cause the PTU to fixate on an old
	maximum cell which wasn't the peak cell it should have been
	attending to; if it receives a change in state from planner, it
	immediately sends a new command to the PTU. The only potential
	pitfall here is if planner frequently flickers between two very
	different states (like INTERSECT_LEFT or INTERSECT_RIGHT) in which
	case a flood of messages would be generated to the PTU and possibly
	cause it to fail. If planner flickers between STOP_OBS and DRIVE
	(which seems more the case), that won't cause any problems because
	the PTU will simply just sweep in either mode. 

	FILES: Makefile.yam(36760)
	fixed a small bug in the makefile.

Wed Aug 29 20:12:08 2007	Jeremy Ma (jerma)

	* version R1-00i
	BUGS:  
	Deleted files: test.rndf
	FILES: ATTENTION.CFG(36372), AttentionModule.cc(36372),
		attention.cc(36372), attention.hh(36372)
	added a 4 second wait to the PTU for the case when alice's velocity
	is less than ALICE_STOP_SPEED (currently set to 0.5m/s); this is so
	the radar can establish a feasible track.

	FILES: ATTENTION.CFG(36409), attention.cc(36409),
		attention.hh(36409), cmdline.ggo(36409)
	now have the option to control how long the ptu waits before
	processing the next scan

	FILES: AttentionModule.cc(36012), attention.cc(36012),
		attention.hh(36012)
	fixed INTERSECT_STRAIGHT handling as well as situations when the
	desired turning lane or entry lane is not found

	FILES: attention.cc(36386), attention.hh(36386)
	moved various valued variables to constants defined in the header
	file 

Mon Aug 27 22:17:45 2007	Jeremy Ma (jerma)

	* version R1-00h
	BUGS:  
	FILES: ATTENTION.CFG(35819), attention.cc(35819),
		attention.hh(35819), cmdline.ggo(35819)
	When alice now slows to a stop (has a velocity < 1.0m), and the
	planning state is either INTERSECT_LEFT, INTERSECT_RIGHT,
	INTERSECT_STRAIGHT, PASS_LEFT, or PASS_RIGHT, the PTU holds its
	position long enough to allow the radar to establish a track

	FILES: attention.cc(35714), attention.hh(35714), cmdline.ggo(35714)
	sensor coverage layer now can be simulated (a big 35m ring around
	Alice) or free space from bumper ladars

Sun Aug 26 22:56:09 2007	Jeremy Ma (jerma)

	* version R1-00g
	BUGS:  
	FILES: attention.cc(35592), attention.hh(35592)
	added PTU control for the remaining planner states (NOMINAL, DRIVE,
	UTURN, STOP_OBS, and ZONE). They all do the exact same thing which
	is go through a temporary cycled list of panning and tilting
	commands to create a sweeping pattern. This is more or less a place
	holder until more intelligent functionality is found and/or
	implemented. 

Sun Aug 26 14:36:05 2007	Jeremy Ma (jerma)

	* version R1-00f
	BUGS:  
	New files: attention.hh attentionMain.cc emapWrapper.cc
		emapWrapper.hh
	Deleted files: RndfPainter.cc RndfPainter.hh
	FILES: ATTENTION.CFG(35555), AttentionMain.cc(35555),
		AttentionModule.cc(35555), AttentionModule.hh(35555),
		Makefile.yam(35555), WorldWin.cc(35555),
		WorldWin.hh(35555), attention.cc(35555),
		cmdline.ggo(35555), send_test.cc(35555)
	Merging in my code into the AttentionModule shell that Vanessa
	wrote. Cleaning up some unused files and adding new files I wrote.
	Everything seems to work fine except the console QUIT button don't
	quite let you exit cleanly (mainly because I don't know how to exit
	the gcmodule arbitrate-control loop cleanly). Other than that,
	looks pretty good. 

Thu Aug 23 17:20:34 2007	vcarson (vcarson)

	* version R1-00e
	BUGS:  
	New files: AttentionMain.cc CmdArgs.cc CmdArgs.hh
Thu Aug 23 13:33:59 2007	vcarson (vcarson)

	* version R1-00d
	BUGS:  
	FILES: AttentionModule.cc(35166), AttentionModule.hh(35166),
		Makefile.yam(35166)
	Fixed communication between Planner and AttentionModule. 

Mon Aug 20 13:39:57 2007	vcarson (vcarson)

	* version R1-00c
	BUGS:  
	New files: AttentionModule.cc AttentionModule.hh
	FILES: Makefile.yam(34232), attention.cc(34232)
	Fixed init warnings in attention.cc.  Added AttentionModule to
	establish planner state communication. AttentionModule is a gc
	module.  Changed Makefile to compile these files.      

Fri Aug 17  5:23:23 2007	Jeremy Ma (jerma)

	* version R1-00b
	BUGS:  
	FILES: Makefile.yam(34051), attention.cc(34051)
	cleaned up some unnecessary include files.

	FILES: attention.cc(34050), send_test.cc(34050)
	modified the PlanningModes to be consistent with what planner
	actually is capable of sending. Also got rid of gaussian costs and
	used long blocks instead.Works much better. Forgetting constant is
	also incorporated.  Still needs a bit more debugging before ramping
	the speed on the PTU.

Thu Aug 16 14:31:27 2007	Jeremy Ma (jerma)

	* version R1-00a
	BUGS:  
	New files: ATTENTION.CFG RndfPainter.cc RndfPainter.hh WorldWin.cc
		WorldWin.hh attention.cc cmdline.ggo glUtils.cc glUtils.hh
		send_test.cc test.rndf
	FILES: Makefile.yam(30257)
	Added visualization capabilities within this module. Visualization
	is done without passing deltas; direct access to local variables
	from within the module. I basically stripped down a bunch of files
	in sensviewer and added them here.

	FILES: Makefile.yam(31418)
	adding gist-layer generation

	FILES: Makefile.yam(32094)
	added right lane change; also did pass through of planning mode
	commands to load gist layers.

	FILES: Makefile.yam(33886)
	added panning/tilting control as well as cleaned up a bunch of
	debug messages.

Tue Jun 26 15:39:48 2007	Jeremy Ma (jerma)

	* version R1-00
	Created attention module.





































