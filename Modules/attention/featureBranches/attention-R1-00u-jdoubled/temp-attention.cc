#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <sensnet/sensnet.h>
#include <interfaces/VehicleState.h>
#include <interfaces/ProcessState.h>
#include <dgcutils/DGCutils.hh>
#include <skynet/sn_msg.hh>
#include <interfaces/sn_types.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/PTUCommand.h>
#include <pthread.h>

using namespace std;

bool quit = false;
sensnet_t *sensnet;
pthread_mutex_t sensnetMutex = PTHREAD_MUTEX_INITIALIZER;

void *processControlFunc(void *ptr);

int main(int argc, char* argv[])
{

  int ptuCommandSocket;
  int sendSuccess;
  float pan_max, pan_min, home;
  float tilt_max, tilt_min;
  float panspeed, tiltspeed;
  PTUCommand ptuCommand;

  bool marked = false;

  char* spreadDaemon;
  int skynetKey;

  pthread_t processControlThread;

 
  // get environment variables
  if (getenv("SPREAD_DAEMON"))    
    spreadDaemon = getenv("SPREAD_DAEMON");
  else
    {
      printf("Could not get Spread Daemon. Aborting.\n");
      return(-1);
    }

  if (getenv("SKYNET_KEY"))
    skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    {
      printf("Could not find skynet key! Aborting.\n");
      return(-1);
    }

  // initialize sensnet
  sensnet = sensnet_alloc();
  assert(sensnet);
  if (sensnet_connect(sensnet, spreadDaemon, skynetKey, MODattention) != 0) 
    {
      printf("ERROR: unable to connect to sensnet\n");
      return -1;
    }

  // join state group
  if (sensnet_join(sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    {
      printf("ERROR: unable to join state group\n");
      return -1;
    }

  // join the process control group
  if (sensnet_join(sensnet, MODattention, SNprocessRequest, sizeof(ProcessRequest)) != 0)
    {
      printf("ERROR: unable to join process group\n");
      return -1;
    }

  // initialize skynet messages
  skynet m_skynet = skynet(MODattention, skynetKey, NULL); 

  ptuCommandSocket = m_skynet.get_send_sock(SNptuCommand);
  if(ptuCommandSocket < 0)
    {
      printf("skynet listen returned error!\n");
      return(-1);
    }
  
  // Start process control thread
  int ret;
  ret = pthread_create(&processControlThread, NULL, processControlFunc, NULL);

  VehicleState state;
  int stateblobId;
    
  panspeed = 15;
  tiltspeed = 5;
  pan_max = 27.5;
  pan_min = -12.5;
  tilt_max = -5;
  tilt_min = -15;
  home = 7.5;
  

  while(true)
    {
  
      memset(&state, 0, sizeof(state));
      // Get the current state value
      pthread_mutex_lock(&sensnetMutex);
      if (sensnet_read(sensnet, SENSNET_SKYNET_SENSOR, SNstate,
		       &stateblobId, sizeof(state), &state) != 0)
	{
	  printf("ERROR: unable to read state data\n");
	  return(-1);
	}
      pthread_mutex_unlock(&sensnetMutex);
    
      printf("main thread1\n");
      
      printf("velocity = %f \n", state.vehSpeed);
      //alice is stopped/going slow, so sweep down and back up
      if(state.vehSpeed < 1.0) 
	{	
	  marked = false;
	  ptuCommand.type = RAW;
	  ptuCommand.pan = home;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	
	  usleep(1500000);
	  
	  
	  ptuCommand.type = RAW;
	  ptuCommand.pan = home;
	  ptuCommand.tilt = tilt_min;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);

	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = home;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan_max;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);
	  
	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan_max;
	  ptuCommand.tilt = tilt_min;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan_max;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);
	  
	  ptuCommand.type = RAW;
	  ptuCommand.pan = home;
	  ptuCommand.tilt = tilt_min;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);
	  
	  ptuCommand.type = RAW;
	  ptuCommand.pan = home;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = home;
	  ptuCommand.tilt = tilt_min;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan_min;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan_min;
	  ptuCommand.tilt = tilt_min;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(1500000);

	  ptuCommand.type = RAW;
	  ptuCommand.pan = pan_min;
	  ptuCommand.tilt = tilt_max;
	  ptuCommand.panspeed = panspeed;
	  ptuCommand.tiltspeed = tiltspeed;
	  sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), 0);
	  
	  usleep(750000);
	} 
      else 
	{ 
	  if(!marked)
	    {
	      //alice is moving. stay at -5
	      ptuCommand.type = RAW;
	      ptuCommand.pan = home;
	      ptuCommand.tilt = tilt_max;
	      ptuCommand.panspeed = panspeed;
	      ptuCommand.tiltspeed = tiltspeed;
	      sendSuccess = m_skynet.send_msg(ptuCommandSocket, &ptuCommand, sizeof(ptuCommand), home);
	      marked = true;
	    }	 
	}

      if(quit)
	break;

    }

  // clean up sensnet
  sensnet_leave(sensnet, SENSNET_SKYNET_SENSOR, SNstate);
  sensnet_leave(sensnet, MODattention, SNprocessRequest);
  sensnet_disconnect(sensnet);
  sensnet_free(sensnet);
  sensnet = NULL;
  
  return(0);
}

void *processControlFunc(void *ptr)
{
  int count = 0;
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  printf("starting process control thread!\n");

  while(true)
    {
      sleep(2);
      if( (count % 15) == 0)
	{
	  // Send heart-beat message
	  memset(&response, 0, sizeof(response));  
	  response.moduleId = MODattention;
	  response.timestamp = DGCgettime();
	  response.healthStatus = 2; //healthy	

	  pthread_mutex_lock(&sensnetMutex);
	  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
			MODattention, SNprocessResponse, 0, sizeof(response), &response);	 
	  // Read process request
	  if (sensnet_read(sensnet, MODattention, SNprocessRequest,
			   &blobId, sizeof(request), &request) != 0)
	    return 0;
	  pthread_mutex_unlock(&sensnetMutex);

	  if (request.quit)
	    {
	      printf("remote quit request\n");
	      quit = true;
	      break;	    
	    }	  
	}
      count++;
    }

}

