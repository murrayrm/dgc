#include "attention.hh"

//! Default constructor
Attention::Attention()
{
  //memset(this, 0, sizeof(*this));
  this->ptuCount = 0;
  this->gistCount = 0;
  this->heartCount = 0;
  this->ptuTime = DGCgettime();
  this->gistTime = DGCgettime();
  this->mapTime = DGCgettime();
  this->sweep = false;
  this->sweepMode = 0;
  this->cycleCount = 0;
  this->cycleAvg = 0;
  this->stopTimeStamped = false;
  this->lastCommandMode = PlanningState::NOMINAL;
  this->lastLookTime = 0;

  numSensors = 0;

  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  DGCcreateMutex(&m_stateMutex);
  DGCcreateMutex(&m_ptuStateMutex);
  DGCcreateMutex(&m_currentCellMutex);
  DGCcreateMutex(&m_desiredCellMutex);
  DGCcreateMutex(&m_emapMutex);

  return;
}

//! Default destructor
Attention::~Attention()
{
  DGCdeleteMutex(&m_stateMutex);
  DGCdeleteMutex(&m_ptuStateMutex);
  DGCdeleteMutex(&m_currentCellMutex);
  DGCdeleteMutex(&m_desiredCellMutex);
  DGCdeleteMutex(&m_emapMutex);
  return;
}

//! Command line parser
int Attention::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return ERROR("Could not parse command line!");
  
  // Fill out the default config path
  this->defaultConfigPath = dgcFindConfigDir("attention");
 
  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;  

  // Fill out CSS logging options
  if(this->options.debug_flag) 
    this->debug = true;
  else
    this->debug = false;
  this->verbose_level = this->options.verbose_level_arg;
  if(this->options.logging_flag)
    this->logging = true;
  else
    this->logging = false;
  this->log_path = this->options.log_path_arg;
  this->log_level = this->options.log_level_arg;

  // RNDF stuff
  if(this->options.rndf_given)
    this->rndfFilename.assign(this->options.rndf_arg);
  else
    this->rndfFilename = "test.rndf";

  return 0;
}


//! Parse the config file
int Attention::parseConfigFile(const char *configPath)
{  
  // Load options from the configuration file
  char filename[256];
  int tmpVal;

  snprintf(filename, sizeof(filename), "%s/%s.CFG",
           configPath, "ATTENTION");

  MSG("loading %s", filename);
  if (cmdline_parser_configfile(filename, &this->options, false, false, false) != 0)
    MSG("unable to process configuration file %s", filename);

  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // Grab map details
  this->numRows = this->options.num_rows_arg;
  this->numCols = this->options.num_cols_arg;
  this->rowRes = this->options.row_resolution_arg;
  this->colRes = this->options.col_resolution_arg;

  tmpVal = (long int)floor(this->options.sens_no_data_value_arg*1000);
  this->sensNoDataVal_emapt = (emap_t)tmpVal;
  this->sensNoDataVal = this->options.sens_no_data_value_arg;

  tmpVal = (long int)floor(this->options.sens_data_value_arg*1000);
  this->sensDataVal_emapt = (emap_t)tmpVal;
  this->sensDataVal = this->options.sens_data_value_arg;

  tmpVal = (long int)floor(this->options.gist_no_data_value_arg*1000);
  this->gistNoDataVal_emapt = (emap_t)tmpVal;
  this->gistNoDataVal = this->options.gist_no_data_value_arg;

  // set the forgetting constant; the larger this value, the slower the cost grows back
  this->timeConstantFast = this->options.time_constant_fast_arg;
  this->timeConstantSlow = this->options.time_constant_slow_arg;

  //currently setting these values to max value
  this->costNoDataVal_emapt = 65535;

  //currently setting these to 0
  this->costNoDataTime_emapt = 0;

  // Grab map details
  this->recvSubGroup = this->options.receive_subgroup_arg;

  return 0;
}


//! Initialize map
int Attention::initMap(const char *configPath, PlanningState::Mode m_planningMode, PlanningState::ExitPoint m_planningExitPoint)
{
  // No need for mutexes here because the other thread hasn't started yet


  // Load configuration file
  if (this->parseConfigFile(configPath) != 0)
    return -1;
  
  // grab state first and center map at alice's pose
  while(true)
    {
      if(this->getState() < 0)
	return ERROR("initMap failed.");
      else if(this->getState() > 0)
	continue;
      else if(this->getState() == 0)
	break;
    }

  //--------------------------------------------------
  // Initiliaze worldMap and load prior data
  //--------------------------------------------------
  // Initialize worldMap
  this->worldMapTalker.initRecvMapElement(this->skynetKey, this->recvSubGroup);

  // load rndf
  this->worldMap.data.clear();
  if(!this->worldMap.loadRNDF(this->rndfFilename.c_str()))    
    return ERROR("Error loading rndf!");

  point2 statedelta, stateNE, statelocal;
  statelocal.set(this->state.localX,this->state.localY);
  stateNE.set(this->state.utmNorthing,this->state.utmEasting);
  statedelta = stateNE-statelocal;

  this->worldMap.setTransform(statedelta); 

  if(this->loadMapPriorData()<0)
    return ERROR("Error loading lane lines!");

  //--------------------------------------------------
  // Initialize the attention maps and associated layers
  //--------------------------------------------------
  // EMap initialization
  emapSensor = new EMap(this->numRows);
  this->sensorMapWrapper.initMap(emapSensor, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->sensNoDataVal_emapt);

  emapGist = new EMap(this->numRows);
  this->gistMapWrapper.initMap(emapGist, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->gistNoDataVal_emapt);

  emapCostTime = new EMap(this->numRows);
  this->timeMapWrapper.initMap(emapCostTime, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->costNoDataTime_emapt);

  emapCostVal = new EMap(this->numRows);
  this->costMapWrapper.initMap(emapCostVal, this->state.localX, this->state.localY, this->rowRes, this->colRes, this->costNoDataVal_emapt);

  // set default plannerMode
  this->planningMode = m_planningMode;
  this->planningPtLabel.segment = m_planningExitPoint.segment;
  this->planningPtLabel.lane = m_planningExitPoint.lane;
  this->planningPtLabel.point = m_planningExitPoint.waypoint;
  
  // grab timestamp for module process speed
  gettimeofday(&stv, 0);
  this->lasttime = stv.tv_sec + (stv.tv_usec/1000000.0);

  this->currentAttendedCell.x = 0;
  this->currentAttendedCell.y = 0;
  this->currentAttendedCell.cellVal = 0;

  this->desiredAttendedCell.x = 0;
  this->desiredAttendedCell.y = 0;
  this->desiredAttendedCell.cellVal = 0;

  return 0;
}

//! Update the map
int Attention::updateMap(PlanningState::Mode m_planningMode, PlanningState::ExitPoint m_planningExitPoint)
{
  // -------------------------------
  // Update state and map locations
  // -------------------------------
  bool recvdNewPlanningPt;

  VehicleState m_state;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);

  // grab state first and update vehicle location
  if(this->getState()<0)
    printf("error getting state! \n");

  DGClockMutex(&m_emapMutex);
  this->gistMapWrapper.updateVehicleLoc(emapGist, m_state.localX, m_state.localY, this->gistNoDataVal_emapt);
  this->sensorMapWrapper.updateVehicleLoc(emapSensor, m_state.localX, m_state.localY, this->sensNoDataVal_emapt);
  this->timeMapWrapper.updateVehicleLoc(emapCostTime, m_state.localX, m_state.localY, this->costNoDataTime_emapt);
  this->costMapWrapper.updateVehicleLoc(emapCostVal, m_state.localX, m_state.localY, this->costNoDataVal_emapt);
  DGCunlockMutex(&m_emapMutex);

  // ---------------------------------
  // Update the planning state
  //----------------------------------
  if(this->planningPtLabel.segment==m_planningExitPoint.segment &&
     this->planningPtLabel.lane==m_planningExitPoint.lane &&
     this->planningPtLabel.point==m_planningExitPoint.waypoint)
    recvdNewPlanningPt = false;
  else
    recvdNewPlanningPt = true;
  
  if(m_planningMode!=this->planningMode || recvdNewPlanningPt)
    {
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      this->costMapWrapper.clearMap(emapCostVal, this->costNoDataVal_emapt);
      this->timeMapWrapper.clearMap(emapCostTime, this->costNoDataTime_emapt);
      DGCunlockMutex(&m_emapMutex);      

      this->planningMode = m_planningMode;
      this->planningPtLabel.segment = m_planningExitPoint.segment;
      this->planningPtLabel.lane = m_planningExitPoint.lane;
      this->planningPtLabel.point = m_planningExitPoint.waypoint;
      
      this->gistCount++;

      sweep = false;

      this->gistTime = DGCgettime();
    }

	
  // load the gistmap layer
  this->loadGistMap(this->planningMode, this->planningPtLabel);

  // load the sensor coverage layer
  this->loadSensorCovMap();

  // now fuse the two layers
  this->loadFusedCostMap();

  
  // update heartbeat
  this->heartCount += 1;
  this->mapTime = DGCgettime();
  
  // update current time
  gettimeofday(&stv, 0);
  this->currenttime = stv.tv_sec + (stv.tv_usec/1000000.0);
  this->cycleAvg = (1/(currenttime-lasttime) + cycleAvg*cycleCount)/(cycleCount+1);
  this->cycleCount++;

  // -------------------------------
  // Update the console display
  // -------------------------------
  if(!this->options.disable_console_flag)
    {
      // update process diagnostics
      cotk_printf(this->console, "%hcap%", A_NORMAL, "%5d ",this->heartCount);
      cotk_printf(this->console, "%mcycle%", A_NORMAL, "%+03.2f ", 1/(currenttime - lasttime));
      cotk_printf(this->console, "%mcycleavg%", A_NORMAL, "%+03.2f ", this->cycleAvg);

      // update vehicle state display
      if (m_state.timestamp > 0)
	cotk_printf(this->console, "%slat%", A_NORMAL,
		    "%+06dms", (int) (m_state.timestamp - this->mapTime) / 1000);
      
      cotk_printf(this->console, "%stime%", A_NORMAL, "%9.3f",
		  fmod((double) m_state.timestamp * 1e-6, 10000));
      cotk_printf(this->console, "%spos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  m_state.localX, m_state.localY, m_state.localZ);
      cotk_printf(this->console, "%srot%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
		  m_state.localRoll*180/M_PI,
		  m_state.localPitch*180/M_PI,
		  m_state.localYaw*180/M_PI);
      
      DGClockMutex(&m_ptuStateMutex);

      // update ptu state display
      cotk_printf(this->console, "%lpan%", A_NORMAL, "%+03.2f",(double)(this->ptublob.currpan));
      cotk_printf(this->console, "%ltilt%", A_NORMAL, "%+03.2f",(double)(this->ptublob.currtilt));

      DGCunlockMutex(&m_ptuStateMutex);

      // update latency displays
      cotk_printf(this->console, "%plat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->ptuTime) / 1000);
      cotk_printf(this->console, "%glat%", A_NORMAL, "%+06dms ",(int) (DGCgettime() - this->gistTime) / 1000);     

      // update planning mode display
      switch(this->planningMode)
	{
	case PlanningState::NOMINAL:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NOMINAL           ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_LEFT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_LEFT    ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_RIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_RIGHT   ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::INTERSECT_STRAIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "INTERSECT_STRAIGHT");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::BACKUP:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "BACKUP            ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::DRIVE:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "DRIVE             ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::STOP_OBS:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "STOP_OBS          ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::PASS_LEFT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "PASS_LEFT         ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::PASS_RIGHT:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "PASS_RIGHT        ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::UTURN:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "UTURN             ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	case PlanningState::ZONE:
	  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "ZONE              ");
	  cotk_printf(this->console, "%gcap%", A_NORMAL, "%5d",this->gistCount);
	  break;
	}

      cotk_printf(this->console, "%gpoint%", A_NORMAL, "%d . %d . %d", 
		  this->planningPtLabel.segment,
		  this->planningPtLabel.lane,
		  this->planningPtLabel.point);
    }

  this->lasttime = this->currenttime; 

  return 0;
  
}


//! Weight Map with prior info from RNDF
int Attention::loadMapPriorData()
{    
  // No need for mutexes here because this function is only called in the initMap function,
  // which is before the other thread is created
  MapElement tmpEl;
  for(int i=0; i< (int)this->worldMap.prior.data.size(); i++)
    {
      this->worldMap.prior.getElFull(tmpEl,i);
      this->priorMapData.push_back(tmpEl);
    }

  return 0;

}


//! Load the appropriate gist map
// No need to worry about map mutex locks here; handled above; do need to worry about state mutex though
int Attention::loadGistMap(PlanningState::Mode m_planningMode, PointLabel m_planningPtLabel)
{
  double projPoseDist;

  LaneLabel currLaneLabel;
  LaneLabel otherLaneLabel;
  LaneLabel passLaneLabel;

  vector<PointLabel> otherLaneEntryPtLabels;

  point2arr turnLaneCenterLine_down;
  point2arr turnLaneCenterLine_up;
  point2arr turnLaneCenterLine_down_dense;
  point2arr turnLaneCenterLine_up_dense;

  point2arr passLaneCenterLine;
  point2arr passLaneCenterLine_dense;
  point2arr passLaneBehindCenterLine;
  point2arr passLaneAheadCenterLine;
  point2arr passLaneCenterLine_shifted;
  point2arr otherLaneCenterLine_up;
  point2arr otherLaneCenterLine_up_dense;

  point2 projPosePt;
  point2 startPt;
  point2 endPt;
  point2 vPt;
  point2 lPt;
  point2 m_planningPt;
  point2 otherPt_tmp;

  bool passingLaneFound;
  bool passLaneIsSameDir;
  bool mapError = false;

  point2 posePt; 
  double heading;
  VehicleState m_state;

  // PASSING VARIABLES
  double projLookBackDist;
  double projLookAheadDist;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);
  
  posePt.x = m_state.localX;
  posePt.y = m_state.localY;
  heading = m_state.localYaw;

  // find my lane first
  this->worldMap.getLane(currLaneLabel,posePt);
  
  if(!this->options.disable_console_flag)
    {
      cotk_printf(this->console, "%seg%", A_NORMAL, "%5d",currLaneLabel.segment);
      cotk_printf(this->console, "%lane%", A_NORMAL, "%5d",currLaneLabel.lane);
    }

  switch(m_planningMode)
    {
    case PlanningState::NOMINAL:
      // for nominal driving, we don't use a gistMap (not yet at least) so just clear old map
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      this->sweep = true;

      // load which time constant to use for growth
      this->timeConstant = this->timeConstantFast;
      this->lookTime = SWEEP_WAIT_TIME;

      break;
    case PlanningState::INTERSECT_LEFT:            
      // clear the map
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      
      // We are given the waypoint to which we are turning to
      // assume that point is m_planningPt and PointLabel is m_planningPtLabel      

      // -----------------------------------------------------------------
      // fill cost up and down for the lane which the m_planningPt resides
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPoint(m_planningPt, m_planningPtLabel)<0)
	{
	  WARN("Could not convert label to waypoint!");
	  mapError = true;
	}
      if(this->worldMap.getLane(otherLaneLabel,m_planningPt)<0)
	{
	  WARN("Could not get lane label for waypoint!");
	  mapError = true;
	}
      if(this->worldMap.getLaneCenterLine(turnLaneCenterLine_down, otherLaneLabel)<0)
	{
	  WARN("Could not get center line for turning lane!");
	  mapError = true;
	}
      if(this->worldMap.getLaneCenterLine(turnLaneCenterLine_up, otherLaneLabel)<0) 
	{
	  WARN("Could not get center line for turning lane!");
	  mapError = true;
	}

      turnLaneCenterLine_down.cut_back(m_planningPt);
      turnLaneCenterLine_up.cut_front(m_planningPt);
      turnLaneCenterLine_up.reverse();
      
      densifyLine(turnLaneCenterLine_down, turnLaneCenterLine_down_dense, this->rowRes, LOOK_DOWN_LEFT_LANE_DIST);
      densifyLine(turnLaneCenterLine_up, turnLaneCenterLine_up_dense, this->rowRes, LOOK_UP_LEFT_LANE_DIST);
		      
      // paint down left lane 
      if(turnLaneCenterLine_down_dense.size()>0)
	{
	  if(this->groundSpeed>ALICE_STOP_SPEED)
	    drawLaneCost(turnLaneCenterLine_down_dense, LEFT_TURN_LANE_DOWN_COST);
	  else		
	    drawCellCost(turnLaneCenterLine_down_dense, LEFT_TURN_LANE_DOWN_COST);	      	      		
	}
		      
      // paint up left lane
      if(turnLaneCenterLine_up_dense.size()>0)
	{
	  if(this->groundSpeed>ALICE_STOP_SPEED)
	    drawLaneCost(turnLaneCenterLine_up_dense, LEFT_TURN_LANE_UP_COST);	 
	  else		
	    drawCellCost(turnLaneCenterLine_up_dense, LEFT_TURN_LANE_UP_COST);
	}

      // -----------------------------------------------------------------
      // find all potential lanes that could turn into the desired entryPt and draw cost in those lanes
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, m_planningPtLabel)>0)
	{
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      // want to draw cost for all possible entry lanes other than my own and
	      // don't need to paint lane for which m_planningPt resides in (that was already done earlier)
	      if( (otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane) || 
		  (otherLaneEntryPtLabels[i].segment==m_planningPtLabel.segment && otherLaneEntryPtLabels[i].lane==m_planningPtLabel.lane) )
		continue;
	      else
		{
		  // these are lanes which could potentially allow for traffic to turn into my m_planningPt through their exitPts
		  this->worldMap.getWayPoint(otherPt_tmp, otherLaneEntryPtLabels[i]);
		  this->worldMap.getLane(otherLaneLabel,otherPt_tmp);
		  this->worldMap.getLaneCenterLine(otherLaneCenterLine_up, otherLaneLabel);
		  
		  otherLaneCenterLine_up.cut_front(otherPt_tmp);
		  otherLaneCenterLine_up.reverse();
		  
		  // densify the lane
		  densifyLine(otherLaneCenterLine_up, otherLaneCenterLine_up_dense, this->rowRes, LOOK_UP_OTHER_LANE_DIST);
		  
		  // paint up the lane
		  if(otherLaneCenterLine_up_dense.size()>0)
		    {
		      if(this->groundSpeed>ALICE_STOP_SPEED)
			drawLaneCost(otherLaneCenterLine_up_dense, OTHER_LANE_UP_COST);	 //may consider different cost
		      else		
			drawCellCost(turnLaneCenterLine_up_dense, OTHER_LANE_UP_COST); //may consider different cost
		    }
		}
	    }
	}
      else
	{
	  WARN("Could not find other entering lanes!");
	  mapError =true;
	}

      // load which time constant to use for growth
      if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SCAN_WAIT_TIME;
	}
      else
	{
	  this->timeConstant = this->timeConstantSlow;
	  this->lookTime = RADAR_WAIT_TIME;
	}

      if(mapError)	
	{
	  WARN("Problem loading gist map for left turn!");
	  this->sweep = true;
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SWEEP_WAIT_TIME;
	}

      break; 
      
    case PlanningState::INTERSECT_RIGHT:    
      
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      // We are given the waypoint to which we are turning to
      // assume that point is m_planningPt and PointLabel is m_planningPtLabel      

      // -----------------------------------------------------------------
      // fill cost up and down for the lane which the m_planningPt resides
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPoint(m_planningPt, m_planningPtLabel)<0)
	mapError = true;
      if(this->worldMap.getLane(otherLaneLabel,m_planningPt)<0)
	mapError = true;
      if(this->worldMap.getLaneCenterLine(turnLaneCenterLine_down, otherLaneLabel)<0)
	mapError = true;
      if(this->worldMap.getLaneCenterLine(turnLaneCenterLine_up, otherLaneLabel)<0)
	mapError = true;

      turnLaneCenterLine_down.cut_back(m_planningPt);
      turnLaneCenterLine_up.cut_front(m_planningPt);
      turnLaneCenterLine_up.reverse();
      
      densifyLine(turnLaneCenterLine_down, turnLaneCenterLine_down_dense, this->rowRes, LOOK_DOWN_RIGHT_LANE_DIST);
      densifyLine(turnLaneCenterLine_up, turnLaneCenterLine_up_dense, this->rowRes, LOOK_UP_RIGHT_LANE_DIST);
		      
      // paint down lane 
      if(turnLaneCenterLine_down_dense.size()>0)
	{
	  if(this->groundSpeed>ALICE_STOP_SPEED)
	    drawLaneCost(turnLaneCenterLine_down_dense, RIGHT_TURN_LANE_DOWN_COST);
	  else		
	    drawCellCost(turnLaneCenterLine_down_dense, RIGHT_TURN_LANE_DOWN_COST);	      	      		
	}
		      
      // paint up lane
      if(turnLaneCenterLine_up_dense.size()>0)
	{
	  if(this->groundSpeed>ALICE_STOP_SPEED)
	    drawLaneCost(turnLaneCenterLine_up_dense, RIGHT_TURN_LANE_UP_COST);	 
	  else		
	    drawCellCost(turnLaneCenterLine_up_dense, RIGHT_TURN_LANE_UP_COST);
	}

      // -----------------------------------------------------------------
      // find all potential lanes that could turn into the desired entryPt and draw cost in those lanes
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, m_planningPtLabel)>0)
	{
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      // want to draw cost for all possible entry lanes other than my own and
	      // don't need to paint lane for which m_planningPt resides in (that was already done earlier)
	      if( (otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane) || 
		  (otherLaneEntryPtLabels[i].segment==m_planningPtLabel.segment && otherLaneEntryPtLabels[i].lane==m_planningPtLabel.lane) )
		continue;
	      else
		{
		  // these are lanes which could potentially allow for traffic to turn into my m_planningPt through their exitPts
		  this->worldMap.getWayPoint(otherPt_tmp, otherLaneEntryPtLabels[i]);
		  this->worldMap.getLane(otherLaneLabel,otherPt_tmp);
		  this->worldMap.getLaneCenterLine(otherLaneCenterLine_up, otherLaneLabel);
		  
		  otherLaneCenterLine_up.cut_front(otherPt_tmp);
		  otherLaneCenterLine_up.reverse();
		  
		  // densify the lane
		  densifyLine(otherLaneCenterLine_up, otherLaneCenterLine_up_dense, this->rowRes, LOOK_UP_OTHER_LANE_DIST);
		  
		  // paint up the lane
		  if(otherLaneCenterLine_up_dense.size()>0)
		    {
		      if(this->groundSpeed>ALICE_STOP_SPEED)
			drawLaneCost(otherLaneCenterLine_up_dense, OTHER_LANE_UP_COST);	 //may consider different cost
		      else		
			drawCellCost(turnLaneCenterLine_up_dense, OTHER_LANE_UP_COST); //may consider different cost
		    }
		}
	    }
	}
      else
	{
	  WARN("Could not find other entering lanes!");
	  mapError = true;
	}

      // load which time constant to use for growth
      if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SCAN_WAIT_TIME;
	}
      else
	{
	  this->timeConstant = this->timeConstantSlow;
	  this->lookTime = RADAR_WAIT_TIME;
	}
      
      if(mapError)	
	{
	  WARN("Problem loading gist map for right turn!");
	  this->sweep = true;
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SWEEP_WAIT_TIME;
	}
          
      break;

    case PlanningState::INTERSECT_STRAIGHT: 

      //look for entry point in my current lane segment

      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);
      
      // We are given the waypoint to which we are turning to
      // assume that point is m_planningPt and PointLabel is m_planningPtLabel      
 
      // ----------------------------------------------------
      // fill cost up down lane which the m_planningPt resides
      // ----------------------------------------------------     
      // I'm calling it a turnLane because the way Nok defines it, there's such a thing as a "straight turn"      
      if(this->worldMap.getWayPoint(m_planningPt, m_planningPtLabel)<0)
	mapError = true;
      if(this->worldMap.getLane(otherLaneLabel,m_planningPt)<0)
	mapError = true;
      if(this->worldMap.getLaneCenterLine(turnLaneCenterLine_down, otherLaneLabel)<0)
	mapError = true;

      turnLaneCenterLine_down.cut_back(m_planningPt);
      
      densifyLine(turnLaneCenterLine_down, turnLaneCenterLine_down_dense, this->rowRes, LOOK_DOWN_STRAIGHT_LANE_DIST);
		      
      // paint down lane 
      if(turnLaneCenterLine_down_dense.size()>0)
	{
	  if(this->groundSpeed>ALICE_STOP_SPEED)
	    drawLaneCost(turnLaneCenterLine_down_dense, STRAIGHT_LANE_DOWN_COST);
	  else		
	    drawCellCost(turnLaneCenterLine_down_dense, STRAIGHT_LANE_DOWN_COST);
	}		      

      // -----------------------------------------------------------------
      // find all potential lanes that could turn into the desired entryPt and draw cost in those lanes
      // -----------------------------------------------------------------
      if(this->worldMap.getWayPointEntries(otherLaneEntryPtLabels, m_planningPtLabel)>0)
	{
	  for(int i=0; i<(int)otherLaneEntryPtLabels.size(); i++)
	    {
	      // want to draw cost for all possible entry lanes other than my own and
	      // don't need to paint lane for which m_planningPt resides in (that was already done earlier)
	      if( (otherLaneEntryPtLabels[i].segment==currLaneLabel.segment && otherLaneEntryPtLabels[i].lane==currLaneLabel.lane) || 
		  (otherLaneEntryPtLabels[i].segment==m_planningPtLabel.segment && otherLaneEntryPtLabels[i].lane==m_planningPtLabel.lane) )
		continue;
	      else
		{
		  // these are lanes which could potentially allow for traffic to turn into my m_planningPt through their exitPts
		  this->worldMap.getWayPoint(otherPt_tmp, otherLaneEntryPtLabels[i]);
		  this->worldMap.getLane(otherLaneLabel,otherPt_tmp);
		  this->worldMap.getLaneCenterLine(otherLaneCenterLine_up, otherLaneLabel);
		  
		  otherLaneCenterLine_up.cut_front(otherPt_tmp);
		  otherLaneCenterLine_up.reverse();
		  
		  // densify the lane
		  densifyLine(otherLaneCenterLine_up, otherLaneCenterLine_up_dense, this->rowRes, LOOK_UP_OTHER_LANE_DIST);
		  
		  // paint up the lane
		  if(otherLaneCenterLine_up_dense.size()>0)
		    {
		      if(this->groundSpeed>ALICE_STOP_SPEED)
			drawLaneCost(otherLaneCenterLine_up_dense, OTHER_LANE_UP_COST);	 //may consider different cost
		      else		
			drawCellCost(turnLaneCenterLine_up_dense, OTHER_LANE_UP_COST); //may consider different cost
		    }
		}
	    }
	}
      else
	{
	  WARN("Could not find other entering lanes!");
	  mapError =true;
	}

      // load which time constant to use for growth
      if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SCAN_WAIT_TIME;
	}
      else
	{
	  this->timeConstant = this->timeConstantSlow;
	  this->lookTime = RADAR_WAIT_TIME;
	}                    
   
      if(mapError)	
	{
	  WARN("Problem loading gist map for straight turn!");
	  this->sweep = true;
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SWEEP_WAIT_TIME;
	}

      break;

    case PlanningState::PASS_LEFT:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      // find my neighboring lane
      this->worldMap.getNeighborLane(passLaneLabel, currLaneLabel,-1);
      if(passLaneLabel.segment==0 && passLaneLabel.lane==0)
	passingLaneFound = false;	
      else	
	passingLaneFound = true;

      if(passingLaneFound)
	{
	  //check if the passing lane is in the same direction or not
	  passLaneIsSameDir = this->worldMap.isLaneSameDir(currLaneLabel,passLaneLabel);

	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, passLaneLabel);
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  if(!passLaneIsSameDir)	    
	    passLaneCenterLine_dense.reverse();	    

	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);

	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;

	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();

	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);

	  passLaneBehindCenterLine = passLaneCenterLine_dense;
	  passLaneBehindCenterLine.cut_back(startPt);
	  passLaneBehindCenterLine.cut_front(projPosePt);

	  passLaneAheadCenterLine = passLaneCenterLine_dense;
	  passLaneAheadCenterLine.cut_back(projPosePt);
	  passLaneAheadCenterLine.cut_front(endPt);

	  if(passLaneIsSameDir)
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneBehindCenterLine,PASS_LANE_HIGH_COST);
		  else
		    drawCellCost(passLaneBehindCenterLine,PASS_LANE_HIGH_COST);		    
		    
		}
	      if(passLaneAheadCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneAheadCenterLine,PASS_LANE_LOW_COST);
		  else		    
		    drawCellCost(passLaneAheadCenterLine,PASS_LANE_LOW_COST);
		}
	    }
	  else
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneBehindCenterLine,PASS_LANE_LOW_COST);
		  else
		    drawCellCost(passLaneBehindCenterLine,PASS_LANE_LOW_COST);
		    
		}
	      if(passLaneAheadCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneAheadCenterLine,PASS_LANE_HIGH_COST);	      
		  else
		    drawCellCost(passLaneAheadCenterLine,PASS_LANE_HIGH_COST);	      		    
		}
	    }	      
	}
      else
       	{	  
	  WARN("could not find passing lane!");	  
	  // if we go out of lane, then we take our current centerline, truncate it and shift 
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, currLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);
	  
	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;
	  
	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();
	  
	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);
	  
	  passLaneCenterLine_dense.cut_back(startPt);
	  passLaneCenterLine_dense.cut_front(endPt);
	  passLaneCenterLine_shifted.clear();
	  
	  // now we need to shift this truncated center line to our right
	  for(uint i=0; i<passLaneCenterLine_dense.size(); i++)
	    {
	      //bring pt to vehicle frame
	      vPt.x = cos(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + sin(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      vPt.y = -sin(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + cos(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      
	      //shift it by shiftDist to the right
	      vPt.y = vPt.y + SHIFT_LEFT_DIST;
	      
	      //bring it back to local frame
	      lPt.x = cos(heading)*vPt.x - sin(heading)*vPt.y + posePt.x;
	      lPt.y = sin(heading)*vPt.x + cos(heading)*vPt.y + posePt.y;

	      passLaneCenterLine_shifted.push_back(lPt);

	    }

	  if(passLaneCenterLine_shifted.size()>0)
	    {
	      if(this->groundSpeed>ALICE_STOP_SPEED)
		drawLaneCost(passLaneCenterLine_shifted,PASS_LANE_HIGH_COST);	 
	      else
		drawCellCost(passLaneCenterLine_shifted,PASS_LANE_HIGH_COST);	 				
	    }

	}   

     // load which time constant to use for growth
      if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SCAN_WAIT_TIME;
	}
      else
	{
	  this->timeConstant = this->timeConstantSlow;
	  this->lookTime = RADAR_WAIT_TIME;
	}

      break;

    case PlanningState::PASS_RIGHT:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      // find my neighboring lane
      this->worldMap.getNeighborLane(passLaneLabel, currLaneLabel,1);
      if(passLaneLabel.segment==0 && passLaneLabel.lane==0)
	passingLaneFound = false;	
      else	
	passingLaneFound = true;

      if(passingLaneFound)
	{
	  //check if the passing lane is in the same direction or not
	  passLaneIsSameDir = this->worldMap.isLaneSameDir(currLaneLabel,passLaneLabel);

	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, passLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  if(!passLaneIsSameDir)	    
	    passLaneCenterLine_dense.reverse();	    

	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);

	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;

	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();

	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);

	  passLaneBehindCenterLine = passLaneCenterLine_dense;
	  passLaneBehindCenterLine.cut_back(startPt);
	  passLaneBehindCenterLine.cut_front(projPosePt);

	  passLaneAheadCenterLine = passLaneCenterLine_dense;
	  passLaneAheadCenterLine.cut_back(projPosePt);
	  passLaneAheadCenterLine.cut_front(endPt);

	  if(passLaneIsSameDir)
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneBehindCenterLine,PASS_LANE_HIGH_COST);
		  else		    
		    drawCellCost(passLaneBehindCenterLine,PASS_LANE_HIGH_COST);
		}
	      if(passLaneAheadCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneAheadCenterLine,PASS_LANE_LOW_COST);
		  else
		    drawCellCost(passLaneAheadCenterLine,PASS_LANE_LOW_COST);
		    
		}
	    }
	  else
	    {
	      if(passLaneBehindCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)		    
		    drawLaneCost(passLaneBehindCenterLine,PASS_LANE_LOW_COST);
		  else
		    drawCellCost(passLaneBehindCenterLine,PASS_LANE_LOW_COST);
		    
		}
	      if(passLaneAheadCenterLine.size()>0)
		{
		  if(this->groundSpeed>ALICE_STOP_SPEED)
		    drawLaneCost(passLaneAheadCenterLine,PASS_LANE_HIGH_COST);	      
		  else
		    drawCellCost(passLaneAheadCenterLine,PASS_LANE_HIGH_COST);
		    
		}
	    }	      
	}
      else
	{	  
	  WARN("could not find passing lane!");	  
	  // if we go out of lane, then we take our current centerline, truncate it and shift 
	  //get the centerline of the passing lane
	  this->worldMap.getLaneCenterLine(passLaneCenterLine, currLaneLabel);
	  
	  densifyLine(passLaneCenterLine, passLaneCenterLine_dense, this->rowRes, passLaneCenterLine.linelength());
	  
	  this->worldMap.getProjectToLine(projPosePt, passLaneCenterLine_dense, posePt);
	  this->worldMap.getDistAlongLine(projPoseDist, passLaneCenterLine_dense, projPosePt);

	  projLookBackDist = projPoseDist-LOOK_BACK_DIST;
	  if(projLookBackDist<0)
	    projLookBackDist=0;

	  projLookAheadDist = projPoseDist+LOOK_AHEAD_DIST;
	  if(projLookAheadDist>passLaneCenterLine.linelength())
	    projLookAheadDist = passLaneCenterLine.linelength();

	  this->worldMap.getPointAlongLine(startPt,passLaneCenterLine_dense,projLookBackDist);
	  this->worldMap.getPointAlongLine(endPt,passLaneCenterLine_dense,projLookAheadDist);

	  passLaneCenterLine_dense.cut_back(startPt);
	  passLaneCenterLine_dense.cut_front(endPt);
	  passLaneCenterLine_shifted.clear();

	  // now we need to shift this truncated center line to our right
	  for(uint i=0; i<passLaneCenterLine_dense.size(); i++)
	    {
	      //bring pt to vehicle frame
	      vPt.x = cos(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + sin(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      vPt.y = -sin(heading)*(passLaneCenterLine_dense[i].x-posePt.x) + cos(heading)*(passLaneCenterLine_dense[i].y-posePt.y);
	      
	      //shift it by shiftDist to the right
	      vPt.y = vPt.y + SHIFT_RIGHT_DIST; 

	      //bring it back to local frame
	      lPt.x = cos(heading)*vPt.x - sin(heading)*vPt.y + posePt.x;
	      lPt.y = sin(heading)*vPt.x + cos(heading)*vPt.y + posePt.y;

	      passLaneCenterLine_shifted.push_back(lPt);

	    }

	  if(passLaneCenterLine_shifted.size()>0)
	    {
	      if(this->groundSpeed>ALICE_STOP_SPEED)		
		drawLaneCost(passLaneCenterLine_shifted,PASS_LANE_HIGH_COST);	 
	      else
		drawCellCost(passLaneCenterLine_shifted,PASS_LANE_HIGH_COST);
	    }
	}

     // load which time constant to use for growth
      if(this->groundSpeed > ALICE_STOP_SPEED)
	{
	  this->timeConstant = this->timeConstantFast;
	  this->lookTime = SCAN_WAIT_TIME;
	}
      else
	{
	  this->timeConstant = this->timeConstantSlow;
	  this->lookTime = RADAR_WAIT_TIME;
	}

      break;

    case PlanningState::STOP_OBS: 
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      this->sweep = true;

      // load which time constant to use for growth
      this->timeConstant = this->timeConstantFast;
      this->lookTime = SWEEP_WAIT_TIME;

      break;
    case PlanningState::DRIVE:      
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      this->sweep = true;

      // load which time constant to use for growth
      this->timeConstant = this->timeConstantFast;
      this->lookTime = SWEEP_WAIT_TIME;

      break; 
    case PlanningState::BACKUP:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      this->sweep = true;

      // load which time constant to use for growth
      this->timeConstant = this->timeConstantFast;
      this->lookTime = SWEEP_WAIT_TIME;

      break;
    case PlanningState::UTURN:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      this->sweep = true;

      // load which time constant to use for growth
      this->timeConstant = this->timeConstantFast;
      this->lookTime = SWEEP_WAIT_TIME;

      break;
    case PlanningState::ZONE:
      DGClockMutex(&m_emapMutex);
      this->gistMapWrapper.clearMap(emapGist, this->gistNoDataVal_emapt);
      DGCunlockMutex(&m_emapMutex);

      this->sweep = true;

      // load which time constant to use for growth
      this->timeConstant = this->timeConstantFast;
      this->lookTime = SWEEP_WAIT_TIME;

      break;
    } 
  
  return 0; 
  
} 

//! Load the sensor coverage map
int Attention::loadSensorCovMap()
{  
  double theta = 0;
  double next_theta = 0;
  double ptx, pty, R;
  double next_ptx, next_pty;
  double sensor_ptx, sensor_pty;
  int vRow1, vCol1;
  int vRow2, vCol2;
  int vRow3, vCol3;
  VehicleState m_state;

  float sfx, sfy, sfz; //sensor frame vars
  float vfx, vfy, vfz; //vehicle frame vars
  float lfx, lfy, lfz; //local frame vars
  float tmpRange, tmpAngle;
  R = 30;

  int ladarblobId;

  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);


  if(this->options.sim_sensor_flag)
    {
      DGClockMutex(&m_emapMutex);
      this->sensorMapWrapper.clearMap(emapSensor, this->sensNoDataVal_emapt);
      for(int i=0; i<360; i++)
	{
	  theta = theta + i*2*M_PI/360;
	  next_theta = theta + i*2*M_PI/360;
	  ptx = R*cos(theta)+m_state.localX;
	  pty = R*sin(theta)+m_state.localY;
	  next_ptx = R*cos(next_theta)+m_state.localX;
	  next_pty = R*sin(next_theta)+m_state.localY;
	  this->sensorMapWrapper.UTM2Win(emapSensor, ptx, pty, &vRow1, &vCol1);
	  this->sensorMapWrapper.UTM2Win(emapSensor, m_state.localX, m_state.localY, &vRow2, &vCol2);
	  this->sensorMapWrapper.UTM2Win(emapSensor, next_ptx, next_pty, &vRow3, &vCol3);
	  
	  this->sensorMapWrapper.fillTriangle(emapSensor, vCol1, vRow1, vCol2, vRow2, vCol3, vRow3, this->sensDataVal_emapt);
	}
      DGCunlockMutex(&m_emapMutex);
    }
  else
    {
      DGClockMutex(&m_emapMutex);
      this->sensorMapWrapper.clearMap(emapSensor, this->sensNoDataVal_emapt);

      memset(&this->ladarblob, 0, sizeof(this->ladarblob));     
      for(int i=0; i<this->numSensors; i++)
	{
	  if (sensnet_read(this->sensnet, this->sensorIds[i], 
			   SENSNET_LADAR_BLOB, &ladarblobId, 
			   sizeof(this->ladarblob), &this->ladarblob) != 0)
	    {	      
	      WARN("Could not read ladar data");      	  
	      continue;
	    }

	  //find origin of sensor in localframe
	  //transformed to sensor frame
	  LadarRangeBlobScanToSensor(&this->ladarblob, 0, 0, &sfx, &sfy, &sfz);
	  //transformed to vehicle frame	      
	  LadarRangeBlobSensorToVehicle(&this->ladarblob, sfx, sfy, sfz, &vfx, &vfy, &vfz);	      
	  //transformed to local frame	      
	  LadarRangeBlobVehicleToLocal(&this->ladarblob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
	  sensor_ptx = (double)lfx;
	  sensor_pty = (double)lfy;

	  for(int j=0; j< NUMSCANPOINTS - 1; j++)
	    {

	      tmpRange = this->ladarblob.points[j][1];
	      tmpAngle = this->ladarblob.points[j][0];
	      if(tmpRange >=45)
		tmpRange = 45;

	      //transformed to sensor frame
	      LadarRangeBlobScanToSensor(&this->ladarblob, tmpAngle, tmpRange, &sfx, &sfy, &sfz);
	      //transformed to vehicle frame	      
	      LadarRangeBlobSensorToVehicle(&this->ladarblob, sfx, sfy, sfz, &vfx, &vfy, &vfz);	      
	      //transformed to local frame	      
	      LadarRangeBlobVehicleToLocal(&this->ladarblob, vfx, vfy, vfz,&lfx, &lfy, &lfz);
	      ptx = (double)lfx;
	      pty = (double)lfy;

	      tmpRange = this->ladarblob.points[j+1][1];
	      tmpAngle = this->ladarblob.points[j+1][0];
	      if(tmpRange >=45)
		tmpRange = 45;

	      //transformed to sensor frame
	      LadarRangeBlobScanToSensor(&this->ladarblob, tmpAngle, tmpRange,&sfx, &sfy, &sfz);
	      //transformed to vehicle frame	      
	      LadarRangeBlobSensorToVehicle(&this->ladarblob, sfx, sfy, sfz, &vfx, &vfy, &vfz);	      
	      //transformed to local frame	      
	      LadarRangeBlobVehicleToLocal(&this->ladarblob, vfx, vfy, vfz,&lfx, &lfy, &lfz);	      
	      next_ptx = (double)lfx;
	      next_pty = (double)lfy;

	      this->sensorMapWrapper.UTM2Win(emapSensor, ptx, pty, &vRow1, &vCol1);
	      this->sensorMapWrapper.UTM2Win(emapSensor, sensor_ptx, sensor_pty, &vRow2, &vCol2);
	      this->sensorMapWrapper.UTM2Win(emapSensor, next_ptx, next_pty, &vRow3, &vCol3);
	      
	      this->sensorMapWrapper.fillTriangle(emapSensor, vCol1, vRow1, vCol2, vRow2, vCol3, vRow3, this->sensDataVal_emapt);
	    }	  	  
	}    
      DGCunlockMutex(&m_emapMutex);    
    }

  return 0;
}

//! Fuse the two layers and find the max cell (UTM) at the same time
int Attention::loadFusedCostMap()
{
  double sensVal;
  emap_t sensVal_emapt;
  double gistVal;
  emap_t gistVal_emapt;
  double fusedVal_cost;
  emap_t fusedVal_cost_emapt;
  emap_t fusedVal_time_emapt;
  double last_fusedVal_cost;
  emap_t last_fusedVal_cost_emapt;
  emap_t last_fusedVal_time_emapt;

  double m_maxCostval = 0;
  double m_maxCostx;
  double m_maxCosty;

  double tau;
  double deltaT;
  double newCost, desiredCost;
  int m_maxRow = 0; 
  int m_maxCol = 0;
  double cellX,cellY;
  int tmpVal;

  //DEBUG
  DGClockMutex(&m_emapMutex);
  for(int i=0; i < (this->numRows); i++)
    {
      for(int j=0; j < (this->numCols); j++)
	{
	  this->sensorMapWrapper.getDataWin(emapSensor, i, j, &sensVal_emapt);
	  this->gistMapWrapper.getDataWin(emapGist, i, j, &gistVal_emapt);
	  this->timeMapWrapper.getDataWin(emapCostTime, i, j, &last_fusedVal_time_emapt);
	  this->costMapWrapper.getDataWin(emapCostVal, i, j, &last_fusedVal_cost_emapt);	
	  this->costMapWrapper.Win2UTM(emapCostVal, i, j, &cellX, &cellY);

	  sensVal = (double)sensVal_emapt/1000;
	  gistVal = (double)gistVal_emapt/1000;
	  last_fusedVal_cost = (double)last_fusedVal_cost_emapt/1000;

	  desiredCost = sensVal*gistVal;

	  // we should really only pay attention to the overlap of the gistmap and the sensCov map
	  // we should really only grow a cell that's been touched by the PTU

	  //if we have a gistValue and the map was just shifted or cleared
	  if( (last_fusedVal_cost_emapt==0 || last_fusedVal_cost_emapt==65535) &&
	      gistVal != gistNoDataVal)
	    {		 
	      // we must have moved the map into a new area
	      fusedVal_cost = desiredCost;
	      fusedVal_time_emapt = 65535;
	    }	  
	  else if(gistVal==gistNoDataVal)
	    {
	      // if we don't have a gistValue and it wasn't just touched by the PTU
	      fusedVal_cost = 0;
	      fusedVal_time_emapt = 65535;	     
	    }
	  else
	    {		      
	      // otherwise, we have a gist value and the map didn't shift or clear and the PTU just looked over it
	      tau = this->timeConstant;
	      deltaT = (double)last_fusedVal_time_emapt;
	      newCost = desiredCost*(1-exp(-deltaT/tau)) + 0.01;	    
 
	      if(fabs(desiredCost-newCost)<0.25)
		{
		  fusedVal_cost = desiredCost;
		  fusedVal_time_emapt = 65535;
		}
	      else
		{
		  fusedVal_cost = newCost;
		  if(last_fusedVal_time_emapt==65535)
		    fusedVal_time_emapt = 65535;
		  else
		    fusedVal_time_emapt = last_fusedVal_time_emapt + 1;
		}      
	    }	  
	  
	  // now update what the max cell is
	  if(fusedVal_cost >= m_maxCostval)
	    {
	      m_maxCostval = fusedVal_cost;
	      m_maxRow = i;
	      m_maxCol = j;
	    }	        	  
	  
	  tmpVal = (long int)floor(fusedVal_cost*1000);
	  fusedVal_cost_emapt = (emap_t)tmpVal;
	  this->timeMapWrapper.setDataWin(this->emapCostTime, i, j, fusedVal_time_emapt);
	  this->costMapWrapper.setDataWin(this->emapCostVal, i, j, fusedVal_cost_emapt);  
	  
	}
    }
  
  this->costMapWrapper.Win2UTM(this->emapCostVal, m_maxRow, m_maxCol, &m_maxCostx, &m_maxCosty);
  this->maxCostCell.x = m_maxCostx;
  this->maxCostCell.y = m_maxCosty;
  this->maxCostCell.cellVal = m_maxCostval;
  
  DGCunlockMutex(&m_emapMutex);  

  return(0);

}


//! Draws a block in the specified lane up to some following distance along a provided heading
//! -- if length is too long, then it gets truncated at end of lane segment
void Attention::drawBlockInLane(LaneLabel m_laneLabel, double blockHeight, point2 startPt, double heading, double length)
{
  point2 tmpPt;  
  point2 tmpPt_next;
  double fillDist;
  double tmpDist;
  int winRow, winCol;
  point2 lanePt;
  int tmpVal;

  fillDist = 0;

  tmpPt = startPt;

  DGClockMutex(&m_emapMutex);
  while(true)
    {
      for(int i=-2; i<3; i++)
	{
	  for(int j=-2; j<3; j++)
	    {
	      lanePt.x = tmpPt.x + i*(this->rowRes);
	      lanePt.y = tmpPt.y + j*(this->colRes);
	      tmpDist = sqrt(pow(startPt.x-lanePt.x,2)+pow(startPt.y-lanePt.y,2));

	      if( this->worldMap.isPointInLane(lanePt, m_laneLabel) &&
		  (10*cos(heading)*(lanePt.x-startPt.x)+10*sin(heading)*(lanePt.y-startPt.y))>0)
		{
		  this->gistMapWrapper.UTM2Win(emapGist, lanePt.x, lanePt.y, &winRow, &winCol);
		  tmpVal = (long int)floor(blockHeight*1000);		  
		  this->gistMapWrapper.setDataWin(emapGist, winRow, winCol, (emap_t)tmpVal);
		  
		  if(tmpDist>fillDist)
		    {
		      fillDist = tmpDist;
		      tmpPt_next = lanePt;
		    }
		  
		}	  
	    }
	}

      tmpPt = tmpPt_next;

      
      if( fabs(length-fillDist)< (this->rowRes) )
	break;

    }
  DGCunlockMutex(&m_emapMutex);
  
}


//! Draws a block in the lane given the lane center line, the cost, and the distance along the lane
void Attention::drawLaneCost(const point2arr &m_laneCenterLine, double m_cost)
{
  int winRow, winCol;
  int tmpVal;

  if(m_laneCenterLine.size()>1)
    {
      DGClockMutex(&m_emapMutex);      
      for(int i=0; i < (int)m_laneCenterLine.size(); i++ )
	{
	  this->gistMapWrapper.UTM2Win(emapGist, m_laneCenterLine[i].x, m_laneCenterLine[i].y, &winRow, &winCol);
	  if(winRow>0 && winCol>0)
	    {	      
	      tmpVal = (long int)floor(m_cost*1000);
	      this->gistMapWrapper.setDataWin(emapGist, winRow, winCol, (emap_t)tmpVal);	      
	    }
	}
      DGCunlockMutex(&m_emapMutex);
    }

  return;
}


//! Assigns the given cost to the cell in the array that is furthest from Alice yet still in the map
void Attention::drawCellCost(const point2arr &m_laneCenterLine, double m_cost)
{
  double furthestCellX, furthestCellY;
  double poseX, poseY;
  double maxDist = 0;
  double tmpDist;
  int winRow, winCol;
  int tmpVal;
  
  DGClockMutex(&this->m_stateMutex);
  poseX = this->state.localX;
  poseY = this->state.localY;
  DGCunlockMutex(&this->m_stateMutex);  

  if(m_laneCenterLine.size()>1)
    {
      DGClockMutex(&m_emapMutex);
      for(int i=0; i < (int)m_laneCenterLine.size(); i++ )
	{	  
	  if(this->gistMapWrapper.isInsideMap(emapGist, m_laneCenterLine[i].x, m_laneCenterLine[i].y))
	    {
	      tmpDist = sqrt(pow(poseX-m_laneCenterLine[i].x,2) + pow(poseY-m_laneCenterLine[i].y,2));
	      if(tmpDist > maxDist)
		{
		  furthestCellX = m_laneCenterLine[i].x;
		  furthestCellY = m_laneCenterLine[i].y;
		  maxDist = tmpDist;
		}
	    }
	}
      
      this->gistMapWrapper.UTM2Win(emapGist, furthestCellX, furthestCellY, &winRow, &winCol);
      if(winRow>0 && winCol>0)
	{	      
	  tmpVal = (long int)floor(m_cost*1000);
	  this->gistMapWrapper.setDataWin(emapGist, winRow, winCol, (emap_t)tmpVal);	     
	}

      DGCunlockMutex(&m_emapMutex);
    }

  return;
}


//! Densifies the given line to the given resolution up to a distance along that line
//! --- if the distance is longer than the line, then it just goes to the end of the array
void Attention::densifyLine(const point2arr &ptarr, point2arr &newptarr, double res, double distance)
{
  double actualDistance;
  double ptHeading;
  point2 nextPt;

  newptarr.clear();
  newptarr.push_back(ptarr[0]);

  //check to make sure the desired distance is not longer than the measured length of the line
  if(distance>=ptarr.linelength())
    actualDistance = ptarr.linelength();
  else
    actualDistance = distance;

  //now densify the line
  for(int i=0; i<(int)(ptarr.size()-1); i++)
    {
      // if next pt is within res distance, add it to the vector list
      if(ptarr[i].dist(ptarr[i+1])<res)	
	{

	  newptarr.push_back(ptarr[i+1]);
	  //check distance
	  if(newptarr.linelength()>=actualDistance)
	    return;
	}
      else
	{
	  nextPt = ptarr[i];
	  ptHeading = atan2(ptarr[i+1].y-ptarr[i].y,ptarr[i+1].x-ptarr[i].x);
	  for(int j=0; j<(long int)floor(ptarr[i].dist(ptarr[i+1])/res); j++)
	    {
	      nextPt.x = res*cos(ptHeading)+nextPt.x;
	      nextPt.y = res*sin(ptHeading)+nextPt.y;

	      newptarr.push_back(nextPt);	      
	      //check distance
	      if(newptarr.linelength()>=actualDistance)
		return;

	    }

	  newptarr.push_back(ptarr[i+1]);
	  //check distance
	  if(newptarr.linelength()>=actualDistance)
	    return;
	}
    }
  return;
  
}

//! Finds the intersection of the Line of Site vector with the ground plane
int Attention::findLineOfSiteIntersect(float *x, float *y, float *z)
{
  vec3_t pointInPlane;
  vec3_t normalToPlane;
  vec3_t linePt1, linePt2;
  float px,py,pz;
  float vx,vy,vz;
  float lx,ly,lz;

  float d,t;
  float numerator, denominator;

  PTUStateBlob *m_ptublob;
  VehicleState m_state;

  DGClockMutex(&this->m_ptuStateMutex);
  m_ptublob = &this->ptublob;
  DGCunlockMutex(&this->m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);

  PTUStateBlobToolToPTU(m_ptublob,0,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(m_ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(m_ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt1 = vec3_set(lx,ly,lz);
  
  PTUStateBlobToolToPTU(m_ptublob,50,0,0,
			&px, &py, &pz);
  PTUStateBlobPTUToVehicle(m_ptublob,px,py,pz,
			   &vx, &vy, &vz);
  PTUStateBlobVehicleToLocal(m_ptublob, vx,vy,vz,
			 &lx,&ly,&lz);
  linePt2 = vec3_set(lx,ly,lz);

  pointInPlane = vec3_set(m_state.localX, m_state.localY, m_state.localZ+VEHICLE_TIRE_RADIUS);
  normalToPlane = vec3_set(0, 0, 1);

  d = vec3_dot(pointInPlane, normalToPlane);
  
  denominator = normalToPlane.x*(linePt2.x-linePt1.x) + normalToPlane.y*(linePt2.y-linePt1.y) + normalToPlane.z*(linePt2.z-linePt1.z);
  if(denominator==0) // the intersection does not exist    
    return(-1);
  else
    {
      numerator = d - normalToPlane.x*linePt1.x - normalToPlane.y*linePt1.y - normalToPlane.z*linePt1.z;
      t = numerator/denominator;
      *x = linePt1.x + t*(linePt2.x - linePt1.x);
      *y = linePt1.y + t*(linePt2.y - linePt1.y);
      *z = linePt1.z + t*(linePt2.z - linePt1.z);
      return 0;
    }
}

//! Converts a line of site pose to corresponding pan-tilt values
void Attention::lineOfSiteToPanTilt(float *m_pan, float *m_tilt, float const &localx, float const &localy, float const &localz)
{
  PTUStateBlob *m_ptublob;
  float vx,vy,vz;
  float px,py,pz;
  double L, X, zeta, beta;

  DGClockMutex(&this->m_ptuStateMutex);
  m_ptublob = &this->ptublob;
  DGCunlockMutex(&this->m_ptuStateMutex);
    
  PTUStateBlobLocalToVehicle(m_ptublob, 
			     localx, localy, localz,
			     &vx, &vy, &vz);
  
  PTUStateBlobVehicleToPTU(m_ptublob,
			   vx,vy,vz,
			   &px, &py, &pz);
  
  L = (double)sqrt(pow(px,2)+pow(py,2));
  X = (double)sqrt(pow(L,2)+pow(pz+BASE_TO_TILTAXIS,2));
  zeta = atan2((double)pz+BASE_TO_TILTAXIS, L);
  beta = asin(TILTAXIS_TO_TOOLFRAME/X);	      
  
  *m_pan = (atan2(py, px))*180/M_PI; //must be in degrees 
  *m_tilt = -(zeta+beta)*180/M_PI;

  return;

}


//! Initialize sensnet 
int Attention::initSensnet()
{  
  // Initialize SensNet 
  this->sensnet = sensnet_alloc();
  assert(this->sensnet);
  if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, MODattention) != 0) 
    return ERROR("unable to connect to sensnet");
  
  // Subscribe to vehicle state messages
  if (sensnet_join(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate, sizeof(VehicleState)) != 0)
    return ERROR("unable to join state group");
  
  // Subscribe to the PTU state messages
  if (sensnet_join(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB, sizeof(PTUStateBlob)) != 0)
    return ERROR("unable to join PTU state group");

  this->sensorIds[this->numSensors++] = SENSNET_LF_BUMPER_LADAR;
  this->sensorIds[this->numSensors++] = SENSNET_RF_BUMPER_LADAR;
  this->sensorIds[this->numSensors++] = SENSNET_MF_BUMPER_LADAR;
  this->sensorIds[this->numSensors++] = SENSNET_REAR_BUMPER_LADAR;

  for(int i=0; i<this->numSensors; i++)
    {
      if (sensnet_join(this->sensnet, this->sensorIds[i], SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
	return ERROR("unable to join ladar group");
    }  

  // Subscribe to process state messages
  if(!this->options.disable_process_control_flag)
    {
      if (sensnet_join(this->sensnet, this->moduleId, SNprocessRequest, sizeof(ProcessRequest)) != 0)
	return ERROR("unable to join process group");
    }

  return 0;
}

//! Finalize sensnet
int Attention::finiSensnet()
{
  // Leave the state group
  sensnet_leave(this->sensnet, SENSNET_SKYNET_SENSOR, SNstate);

  // Leave the PTU state group 
  sensnet_leave(this->sensnet, SENSNET_MF_PTU, SENSNET_PTU_STATE_BLOB);

  // Leave the ProcessControl group
  if(!this->options.disable_process_control_flag)
    sensnet_leave(this->sensnet, this->moduleId, SNprocessRequest);

  // Disconnect
  sensnet_disconnect(this->sensnet);
  sensnet_free(this->sensnet);
  this->sensnet = NULL;
  
  return 0;
}

//! Initialize skynet
int Attention::initSkynet()
{
  // Initialize sockets for receiving gistmap and sending ptucommands
  this->m_skynet = new skynet(this->moduleId, this->skynetKey, NULL);

  this->ptuCommandSocket = this->m_skynet->get_send_sock(SNptuCommand);
  if(this->ptuCommandSocket < 0)
    return ERROR("attention::initSkynet(): skynet get_send_sock returned error");

  this->stateTalker = new CStateWrapper(this->skynetKey);

  return 0;
}

//! Finalise skynet
int Attention::finiSkynet()
{
  delete this->m_skynet;
  return 0;
}

//! Do the attending based on current map
int Attention::attend()
{
  float localx, localy, localz;
  int m_row, m_col;
  int tmpRow, tmpCol;
  double m_panspeed, m_tiltspeed;
  double m_currpan, m_currtilt;
  double timeElapsed;
  bool maxCostCellOutsideMap = false;

  VehicleState m_state;

  DGClockMutex(&m_ptuStateMutex);
  m_panspeed = this->ptublob.currpanspeed;
  m_tiltspeed = this->ptublob.currtiltspeed;
  m_currpan = this->ptublob.currpan;
  m_currtilt = this->ptublob.currtilt;
  DGCunlockMutex(&m_ptuStateMutex);
  
  DGClockMutex(&m_stateMutex);
  m_state = this->state;
  DGCunlockMutex(&m_stateMutex);
  
  if(this->gistMapWrapper.isInsideMap(this->emapGist, this->maxCostCell.x, this->maxCostCell.y))
    maxCostCellOutsideMap = false;
  else
    maxCostCellOutsideMap = true;

  if(sweep || maxCostCellOutsideMap)
    {
      switch(this->sweepMode)
	{
	case 0:
	  this->command.type = RAW;
	  this->command.pan = 0;
	  this->command.tilt = -5;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;
	  break;
	case 1:
	  this->command.type = RAW;
	  this->command.pan = -60;
	  this->command.tilt = -20;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;
	  break;
	case 2:
	  this->command.type = RAW;
	  this->command.pan = 0;
	  this->command.tilt = -5;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;	  
	  break;
	case 3:
	  this->command.type = RAW;
	  this->command.pan = 60;
	  this->command.tilt = -20;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;	  
	  break;
	default:	  
	  this->command.type = RAW;
	  this->command.pan = 0;
	  this->command.tilt = -5;
	  this->command.panspeed = 25;
	  this->command.tiltspeed = 10;
	  break;
	}

      // if we've switched states, send new command immediately
      if((this->lastCommandMode!=PlanningState::NOMINAL) ||
	 (this->lookTime!=this->lastLookTime))
	{
	  // Send PTU command
	  if(this->sendPTUcommand(this->command) <= 0)
	    return ERROR("PTU command not sent!");
	  
	  this->sweepMode++;
	  if(this->sweepMode==4)
	    this->sweepMode=0;
	  
	  // despite what the current planning state is, the lastCommandMode
	  // indicates what mode we were really in when commanding the PTU
	  lastCommandMode = PlanningState::NOMINAL;
	  // Update ptu command count
	  this->ptuCount += 1;  
	  this->ptuTime = DGCgettime();
	  this->stopTimeStamped = false;
	  
	  if(!this->options.disable_console_flag)
	    {
	      cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	      cotk_printf(this->console, "%plos%", A_NORMAL, "SWEEPMODE %d         ",
			  this->sweepMode);
	      cotk_printf(this->console, "%alooktime%", A_NORMAL, "%5s", "n/a   ");
	      cotk_printf(this->console, "%astoptime%", A_NORMAL, "%5s", "n/a   ");
	    }      	 	  	  
	}
      else
	{
	  // find cell location of current line of site
	  if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
	    {
	      DGClockMutex(&m_currentCellMutex);
	      this->currentAttendedCell.x = localx;
	      this->currentAttendedCell.y = localy;
	      this->currentAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_currentCellMutex);
	    }
	  
	  // since the nominal driving is a fixed set of sweeping modes, we'll 
	  // just send the attended cell to plot to be where alice is
	  DGClockMutex(&m_desiredCellMutex);
	  this->desiredAttendedCell.x = m_state.localX;
	  this->desiredAttendedCell.y = m_state.localY;
	  this->desiredAttendedCell.cellVal = 0;
	  DGCunlockMutex(&m_desiredCellMutex);	  
	  
	  //if 1) we're slow enough
	  //   2) we haven't started counting our stopped wait time
	  //   3) and it has been more than 1/10 of a second since our last sent PTU command
	  if(m_panspeed == 0 && m_tiltspeed == 0 && !stopTimeStamped &&
	     (this->currenttime - (double)this->ptuTime/1000000) > 0.1) 
	    {
	      stopTimeStamped = true;
	      this->stopTime = (double)(DGCgettime()/1000000);
	    }
	  
	  if(stopTimeStamped)	    
	    {
	      timeElapsed = this->currenttime - this->stopTime;
	      
	      if(!this->options.disable_console_flag)
		{		
		  cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
		  cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", timeElapsed);
		}		  
	    }
	  else
	    timeElapsed = -1;
	  	  
	  if(m_panspeed == 0 && m_tiltspeed == 0 && timeElapsed > this->lookTime)
	    {	  	  	  
	      // Send PTU command
	      if(this->sendPTUcommand(this->command) <= 0)
		return ERROR("PTU command not sent!");
	      
	      this->sweepMode++;
	      if(this->sweepMode==4)
		this->sweepMode=0;
	      
	      // despite what the current planning state is, the lastCommandMode
	      // indicates what mode we were really in when commanding the PTU
	      lastCommandMode = PlanningState::NOMINAL;
	      // Update ptu command count
	      this->ptuCount += 1;  
	      this->ptuTime = DGCgettime();
	      this->stopTimeStamped = false;
	      
	      if(!this->options.disable_console_flag)
		{
		  cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
		  cotk_printf(this->console, "%plos%", A_NORMAL, "SWEEPMODE %d         ",
			      this->sweepMode);
		  cotk_printf(this->console, "%alooktime%", A_NORMAL, "%5s", "n/a   ");
		  cotk_printf(this->console, "%astoptime%", A_NORMAL, "%5s", "n/a   ");
		}      
	    }
	}

      //reset sweep to false
      this->sweep = false;

    }
  else
    {
      // if we've gotten this far, then we're not going to be doing any sweeping 
      this->command.type = LINEOFSITE;
      this->command.localx = this->maxCostCell.x;
      this->command.localy = this->maxCostCell.y;
      this->command.localz = m_state.localZ+VEHICLE_TIRE_RADIUS;
      this->command.panspeed = 25;
      //this->command.tiltspeed = 5;    
               
      // if we've switched states, send new command immediately
      if((this->planningMode!=this->lastCommandMode) ||
	 (this->lookTime!=this->lastLookTime ))
	{
	  DGClockMutex(&m_desiredCellMutex);
	  this->desiredAttendedCell.x = this->command.localx;
	  this->desiredAttendedCell.y = this->command.localy;
	  this->desiredAttendedCell.cellVal = 0;
	  DGCunlockMutex(&m_desiredCellMutex);	  

	  float g_pan, g_tilt;
	  double t, tiltrate;
	  this->lineOfSiteToPanTilt(&g_pan, &g_tilt, this->command.localx, this->command.localy, this->command.localz);
	  t = fabs((double)g_pan-m_currpan)/this->command.panspeed;
	  tiltrate = fabs((double)g_tilt-m_currtilt)/t;

	  if(tiltrate>15)
	    tiltrate = 15;	  
	  if(tiltrate<=0)
	    tiltrate = 1;

	  this->command.tiltspeed = tiltrate;
	  
	  
	  // Send PTU command
	  if(this->sendPTUcommand(this->command) <= 0)
	    return ERROR("PTU command not sent!");
	  
	  memcpy(&this->lastCommand, &this->command, sizeof(PTUCommand));
	  lastCommandMode = this->planningMode;
	  
	  // Update ptu command count
	  this->ptuCount += 1;  
	  this->ptuTime = DGCgettime();
	  this->stopTimeStamped = false;
	  
	  if(!this->options.disable_console_flag)
	    {
	      cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
	      cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
			  this->command.localx, this->command.localy, this->command.localz);
	    }      
	}
      else //
	{
	  if(findLineOfSiteIntersect(&localx, &localy, &localz)==0)
	    {
	      DGClockMutex(&m_currentCellMutex);
	      this->currentAttendedCell.x = localx;
	      this->currentAttendedCell.y = localy;
	      this->currentAttendedCell.cellVal = 0;
	      DGCunlockMutex(&m_currentCellMutex);
	      
	      DGClockMutex(&m_emapMutex);
	      this->costMapWrapper.UTM2Win(this->emapCostVal,(double)localx, (double)localy, &m_row, &m_col);
	      
	      for(int i=-3; i<4; i++)
		{
		  for(int j=-3; j<4; j++)
		    {
		      tmpRow = m_row+i;
		      tmpCol = m_col+j;
		      
		      if(tmpRow >= this->numRows-1)
			tmpRow = this->numRows-1;
		      if(tmpRow <= 0)
			tmpRow = 0;
		      
		      if(tmpCol >= this->numCols-1)
			tmpCol = this->numCols-1;
		      if(tmpCol <= 0)
			tmpCol = 0;
		      
		      this->costMapWrapper.setDataWin(this->emapCostVal, tmpRow, tmpCol, 10);
		      this->timeMapWrapper.setDataWin(this->emapCostTime, tmpRow, tmpCol, 0);
		    }
		}
	      DGCunlockMutex(&m_emapMutex);
	      	    
	      //if 1) we're slow enough
	      //   2) we haven't started counting our stopped wait time
	      //   3) and it has been more than 1/10 of a second since our last sent PTU command
	      if(m_panspeed == 0 && m_tiltspeed == 0 && !stopTimeStamped &&
		 (this->currenttime - (double)this->ptuTime/1000000) > 0.1) //it's been more than 0.1s since last command
		{
		  stopTimeStamped = true;
		  this->stopTime = (double)(DGCgettime()/1000000);
		}
	      
	      if(stopTimeStamped)	    
		{
		  timeElapsed = this->currenttime - this->stopTime;
		  
		  if(!this->options.disable_console_flag)
		    {		
		      cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
		      cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", timeElapsed);
		    }		  
		}
	      else
		timeElapsed = -1;
	      
	      
	      // this is if it gets stuck trying to get an infeasible cell location
	      if(m_panspeed == 0 && m_tiltspeed == 0 && timeElapsed > lookTime)
		{
		  DGClockMutex(&m_desiredCellMutex);
		  this->desiredAttendedCell.x = this->command.localx;
		  this->desiredAttendedCell.y = this->command.localy;
		  this->desiredAttendedCell.cellVal = 0;
		  DGCunlockMutex(&m_desiredCellMutex);
	
		  float g_pan, g_tilt;
		  double t, tiltrate;
		  this->lineOfSiteToPanTilt(&g_pan, &g_tilt, this->command.localx, this->command.localy, this->command.localz);
		  t = fabs((double)g_pan-m_currpan)/this->command.panspeed;
		  tiltrate = fabs((double)g_tilt-m_currtilt)/t;
		  
		  if(tiltrate>15)
		    tiltrate = 15;	  
		  if(tiltrate<=0)
		    tiltrate = 1;
		  
		  this->command.tiltspeed = tiltrate;	
	  		  
		  // Send PTU command
		  if(this->sendPTUcommand(this->command) <= 0)
		    return ERROR("PTU command not sent!");
		  	       
		  memcpy(&this->lastCommand, &this->command, sizeof(PTUCommand));
		  lastCommandMode = this->planningMode;
		  
		  // Update ptu command count
		  this->ptuCount += 1;  
		  this->ptuTime = DGCgettime();
		  this->stopTimeStamped = false;
		  
		  if(!this->options.disable_console_flag)
		    {
		      cotk_printf(this->console, "%pcap%", A_NORMAL, "%5d",this->ptuCount);
		      cotk_printf(this->console, "%plos%", A_NORMAL, "%+03.2f %+03.2f %+03.2f",
				  this->command.localx, this->command.localy, this->command.localz);
		      cotk_printf(this->console, "%alooktime%", A_NORMAL, "%+03.2f", lookTime);
		      cotk_printf(this->console, "%astoptime%", A_NORMAL, "%+03.2f", 0.00);
		    }      
		}      
	    }	    		
	}
    }
 
  this->lastLookTime = this->lookTime;
 
  return 0;
}

//! Get Alice and PTU state
int Attention::getState()
{
  int ptublobId;

  DGClockMutex(&m_stateMutex);
  this->state = this->stateTalker->getstate();
  this->groundSpeed = sqrt(pow(this->state.utmNorthVel,2) + pow(this->state.utmEastVel,2));
  DGCunlockMutex(&m_stateMutex);  

  DGClockMutex(&m_ptuStateMutex);
  memset(&this->ptublob, 0, sizeof(this->ptublob));
  if (sensnet_read(this->sensnet, SENSNET_MF_PTU, 
		   SENSNET_PTU_STATE_BLOB, &ptublobId, 
		   sizeof(this->ptublob), &this->ptublob) != 0)
    return ERROR("Could not read ptu state");
  DGCunlockMutex(&m_ptuStateMutex);

  return 0;
}


//! Get the process state
int Attention::getProcessState()
{
  int blobId;
  ProcessRequest request;
  ProcessResponse response;

  // Send heart-beat message
  memset(&response, 0, sizeof(response));  
  response.moduleId = this->moduleId;
  response.timestamp = DGCgettime();
  sensnet_write(sensnet, SENSNET_METHOD_CHUNK,
                this->moduleId, SNprocessResponse, 0, sizeof(response), &response);
  
  // Read process request
  if (sensnet_read(this->sensnet, this->moduleId, SNprocessRequest,
                   &blobId, sizeof(request), &request) != 0)
    return 0;
  if (blobId < 0)
    return 0;

  // If we have request data, override the console values
  this->quit = request.quit;

  if (request.quit)
    MSG("remote quit request");
  
  return 0;
}


//! Send PTU command
int Attention::sendPTUcommand(PTUCommand p_command)
{
  int numBytesSent;
  numBytesSent = m_skynet->send_msg(this->ptuCommandSocket, &p_command, sizeof(p_command), 0);
  return numBytesSent;
}

// Template for console
//234567890123456789012345678901234567890123456789012345678901234567890123456789
static char *consoleTemplate =
"Attention $Revision$                MODULE RATE                            \n"
"                                    Hz: %mcycle% (%mcycleavg%)             \n"
"Skynet: %spread%                    Heartbeat: %hcap%                      \n"
"                                                                           \n"
"ALICE STATE                         PTU STATE                              \n"
"Time    : %stime%                   rawpan  (deg): %lpan%                  \n"
"Pos     : %spos%                    rawtilt (deg): %ltilt%                 \n"
"Rot     : %srot%                                                           \n"
"Latency : %slat%                                                           \n"
"                                                                           \n"
"PLANNING MODE                       PTU COMMANDS                           \n"
"Mode      : %gmode%                 Number sent  : %pcap%                  \n"
"Exit Pt   : %gpoint%                Command      : %plos%                  \n"
"Num recvd : %gcap%                  Latency      : %plat%                  \n"
"Latency   : %glat%                                                         \n" 
"                                                                           \n" 
"ATTENDING MODE                      CURRENT LANE                           \n"
"StopTime :  %astoptime%             Segment: %seg%                         \n"
"LookTime :  %alooktime%             Lane   : %lane%                        \n"
"                                                                           \n" 
"%stderr%                                                                   \n" 
"%stderr%                                                                   \n" 
"%stderr%                                                                   \n" 
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n"; 
  

//! Initialize console display
int Attention::initConsole()
{   
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  if (cotk_open(this->console, "attention.msg") != 0)
    return -1;
  
  // Display spread id
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));
 
  // Display some initial values
  cotk_printf(this->console, "%gcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%pcap%", A_NORMAL, "%d",0);
  cotk_printf(this->console, "%gmode%", A_NORMAL, "%s", "NO MODE        ");
  cotk_printf(this->console, "%gpoint%", A_NORMAL, "%d . %d . %d", 0, 0, 0);
  return 0;
}

//! Finalize console display
int Attention::finiConsole()
{
  if (!this->options.disable_console_flag)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}

//! Console button callback
int Attention::onUserQuit(cotk_t *console, Attention *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}

//! Console button callback
int Attention::onUserPause(cotk_t *console, Attention *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


void idle(void* param)
{ 

  Attention* self = (Attention*) param;

  CellData tempData;
  CellData tmpCurrAttCell, tmpGoalAttCell;

  VehicleState m_state;
  PTUStateBlob m_ptublob;

  emap_t gistVal_emapt;
  emap_t sensVal_emapt;
  emap_t costVal_emapt;


  DGClockMutex(&self->m_stateMutex);
  m_state = self->state;
  DGCunlockMutex(&self->m_stateMutex);

  DGClockMutex(&self->m_ptuStateMutex);
  m_ptublob = self->ptublob;
  DGCunlockMutex(&self->m_ptuStateMutex);

  self->sensvec.clear();
  self->gistvec.clear();
  self->costvec.clear();

  DGClockMutex(&self->m_emapMutex);
  for(int i=0; i<self->numRows; i++)
    {
      for(int j=0; j<self->numCols; j++)
	{
	  self->sensorMapWrapper.Win2UTM(self->emapSensor, i, j, &tempData.x, &tempData.y);
	  self->sensorMapWrapper.getDataWin(self->emapSensor, i, j, &sensVal_emapt);
	  tempData.cellVal = ((double)sensVal_emapt)/1000;
	  self->sensvec.push_back(tempData);


	  self->gistMapWrapper.Win2UTM(self->emapGist, i, j, &tempData.x, &tempData.y);
	  self->gistMapWrapper.getDataWin(self->emapGist, i, j, &gistVal_emapt);
	  tempData.cellVal = ((double)gistVal_emapt)/1000;
	  self->gistvec.push_back(tempData);

	  self->costMapWrapper.Win2UTM(self->emapCostVal, i, j, &tempData.x, &tempData.y);
	  self->costMapWrapper.getDataWin(self->emapCostVal, i, j, &costVal_emapt);
	  if(costVal_emapt==65535)
	    costVal_emapt = 0;
	  tempData.cellVal = ((double)costVal_emapt)/1000;
	  
	  self->costvec.push_back(tempData);
	}
    }  
  DGCunlockMutex(&self->m_emapMutex);

  DGClockMutex(&self->m_currentCellMutex);
  tmpCurrAttCell.x = self->currentAttendedCell.x;
  tmpCurrAttCell.y = self->currentAttendedCell.y;
  tmpCurrAttCell.cellVal = self->currentAttendedCell.cellVal;
  DGCunlockMutex(&self->m_currentCellMutex);

  DGClockMutex(&self->m_desiredCellMutex);
  tmpGoalAttCell.x = self->desiredAttendedCell.x;
  tmpGoalAttCell.y = self->desiredAttendedCell.y;
  tmpGoalAttCell.cellVal = self->desiredAttendedCell.cellVal;
  DGCunlockMutex(&self->m_desiredCellMutex);

  // update the map
  self->worldwin->update(m_state, m_ptublob, self->sensvec, self->gistvec, self->costvec, self->priorMapData, tmpCurrAttCell, tmpGoalAttCell);

}

//! Initialize the visualizer
void Attention::initVisualizer(int *argc, char** argv)
{
  glutInit(argc, argv);
}

//! Start visualizer thread
void Attention::v_startVisualizerThread()
{
  int cols = 512;
  int rows = 384;

  Fl_Menu_Item menuitems[] =
    {
      {"&Action", 0, 0, 0, FL_SUBMENU},    
      {"&Pause", FL_CTRL + 'p', (Fl_Callback*) Attention::v_onAction, (void*) 0x1000, FL_MENU_TOGGLE},
      {"E&xit", FL_CTRL + 'q', (Fl_Callback*) Attention::v_onExit},
      {0},
      {0},     
    };

  // Create top-level window
  this->mainwin = new Fl_Window(cols, rows, "DGC Attention Viewer");
  this->mainwin->user_data(this);

  this->mainwin->begin();

  // Create the menu bar
  this->menubar = new Fl_Menu_Bar(0, 0, cols, 30);
  this->menubar->user_data(this);
  this->menubar->copy(menuitems);

  // Create world window
  this->worldwin = new WorldWin(0, 30, cols, rows - 30, 30);

  this->mainwin->end();

  // Make world window resizable 
  this->mainwin->resizable(this->worldwin);

  // Initialize image window
  // -- no need for mutex around options because it's not used after this call in other thread
  this->worldwin->init(this->options.col_resolution_arg, this->options.row_resolution_arg);

  // Idle callback
  Fl::add_idle(idle, this);

  // Run
  this->mainwin->show();
  while (!this->quit)
    Fl::wait();
   
  return;

}

//! Handle menu callbacks
void Attention::v_onExit(Fl_Widget *w, int option)
{
  Attention *self;

  self = (Attention*) w->user_data();
  self->quit = true;

  return;
}


//! Handle menu callbacks
void Attention::v_onAction(Fl_Widget *w, int option)
{
  Attention *self;  

  self = (Attention*) w->user_data();
  if (option == 0x1000)
    self->pause = !self->pause;
  
  return;
}
