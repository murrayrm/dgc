/**********************************************************
 **
 **  TEMPLATELADARPERCEPTOR.CC
 **
 **    Time-stamp: <2007-06-17 11:47:54 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Apr  4 09:15:25 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "LadarCurbPerceptor.hh"

// Default constructor
LadarCurbPerceptor::LadarCurbPerceptor()
{
  memset(this, 0, sizeof(*this));

  rt = new UD_Ladar_RoadTracker(0.5, 2.0, 100, 0.05);  // 100 was 1000

  return;
}


// Default destructor
LadarCurbPerceptor::~LadarCurbPerceptor()
{
}


// Parse the command line
int LadarCurbPerceptor::parseCmdLine(int argc, char **argv)
{
  // Load options
  if (cmdline_parser(argc, argv, &this->options) < 0)
    return -1;

  // Fill out the spread name
  if (this->options.spread_daemon_given)
    this->spreadDaemon = this->options.spread_daemon_arg;
  else if (getenv("SPREAD_DAEMON"))
    this->spreadDaemon = getenv("SPREAD_DAEMON");
  else
    return ERROR("unknown Spread daemon: please set SPREAD_DAEMON");
  
  // Fill out the skynet key
  if (this->options.skynet_key_given)
    this->skynetKey = this->options.skynet_key_arg;
  else if (getenv("SKYNET_KEY"))
    this->skynetKey = atoi(getenv("SKYNET_KEY"));
  else
    this->skynetKey = 0;
  
  // Fill out module id
  this->moduleId = modulenamefromString(this->options.module_id_arg);
  if (this->moduleId <= 0)
    return ERROR("invalid module id: %s", this->options.module_id_arg);

  // If the user gives us some log files on the command line, run in
  // replay mode.
  if (this->options.inputs_num > 0)
    this->mode = modeReplay;
  else
    this->mode = modeLive;
  
  return 0;
}



// Initialize sensnet
int LadarCurbPerceptor::initSensnet()
{
  int i;
  int numSensorIds;  
  sensnet_id_t sensorIds[16];
  Ladar *ladar;

  if (this->mode == modeLive)
  {
    // Create sensnet interface
    this->sensnet = sensnet_alloc();
    assert(this->sensnet);
    if (sensnet_connect(this->sensnet, this->spreadDaemon, this->skynetKey, this->moduleId) != 0)
      return -1;
  }
  else
  {
    // Create replay interface
    this->replay = sensnet_replay_alloc();
    assert(this->replay);    
    if (sensnet_replay_open(this->replay, this->options.inputs_num, this->options.inputs) != 0)
      return ERROR("unable to open log");
  }

  // Default ladar set
  numSensorIds = 0;
  //  sensorIds[numSensorIds++] = SENSNET_LF_ROOF_LADAR;
  //  sensorIds[numSensorIds++] = SENSNET_RF_ROOF_LADAR;
  //  sensorIds[numSensorIds++] = SENSNET_LF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_MF_BUMPER_LADAR;
  sensorIds[numSensorIds++] = SENSNET_PTU_LADAR;
  //  sensorIds[numSensorIds++] = SENSNET_RF_BUMPER_LADAR;
  // sensorIds[numSensorIds++] = SENSNET_RIEGL;

  // Initialize ladar list
  for (i = 0; i < numSensorIds; i++)
  {
    assert((size_t) this->numLadars < sizeof(this->ladars) / sizeof(this->ladars[0]));
    ladar = this->ladars + this->numLadars++;

    // Initialize ladar data
    ladar->sensorId = sensorIds[i];

    // Join the ladar data group
    if (this->sensnet)
    {
      if (sensnet_join(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, sizeof(LadarRangeBlob)) != 0)
        return ERROR("unable to join %d", ladar->sensorId);
    }
  }

  initSendMapElement(skynetKey);
  lastSentSize.resize(this->numLadars);
  for (i=0;i<this->numLadars;++i){
    lastSentSize[i] = 0;
  }
  return 0;
}


// Clean up sensnet
int LadarCurbPerceptor::finiSensnet()
{
  if (this->sensnet)
  {
    int i;
    Ladar *ladar;
    for (i = 0; i < this->numLadars; i++)
    {
      ladar = this->ladars + i;
      sensnet_leave(this->sensnet, ladar->sensorId, SENSNET_LADAR_BLOB);
    }
    sensnet_free(this->sensnet);
    this->sensnet = NULL;
  }
  else if (this->replay)
  {
    sensnet_replay_close(this->replay);
    sensnet_replay_free(this->replay);
    this->replay = NULL;
  }
  
  return 0;
}


// Update the map with new range data
int LadarCurbPerceptor::update()
{
  //  fprintf(stderr, "entering update() \n");

  int i;
  Ladar *ladar;
  int blobId, blobLen;
  LadarRangeBlob blob;

  // Why is this necessary? AH
  usleep(100000);
  
  if (this->sensnet)
  {
    // In live mode wait for new data, but timeout if
    // we dont get anything new for a while.
    if (sensnet_wait(this->sensnet, 200) != 0)
      return 0;
  }
  else if (this->replay)
  {
    // In replay mode, advance the log to the next record.
    if (sensnet_replay_next(this->replay, 0) != 0)
      return 0;
  }
  
  for (i = 0; i < this->numLadars; i++)
  {
    ladar = this->ladars + i;

    if (this->sensnet)
    {
      // Check the latest blob id
      if (sensnet_peek(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &blobId, &blobLen) != 0)
        continue;

      // Is this a new blob?
      if (blobId == ladar->blobId || blobId <0 )
        continue;

      // If this is a new blob, read it    
      if (sensnet_read(this->sensnet, ladar->sensorId,
                       SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }
    else if (this->replay)
    {
      // Advance log one step
      if (sensnet_replay_next(this->replay, 0) != 0)
        return -1;
 
      // Read new blob
      if (sensnet_replay_read(this->replay, ladar->sensorId,
                              SENSNET_LADAR_BLOB, &ladar->blobId, sizeof(blob), &blob) != 0)
        break;
    }

    fprintf(stderr, "got data %s %d %d %lld\n",
            sensnet_id_to_name(ladar->sensorId), ladar->blobId,
            blob.scanId, blob.timestamp);

    // we've got a new ladar blob--queue it up
    
    rt->filter_new_scan(&blob, blob.state);
    
  } //end cycling through ladars

  // update curb tracker

  rt->update(blob.state);

  // send points in queue to map

  point2 statept(blob.state.localX, blob.state.localY);

  MapElement mappts;

  mappts.setTypePoints();
  mappts.setId((int)MODladarRoadPerceptor,1);
  mappts.setColor(MAP_COLOR_YELLOW);
  mappts.setGeometry(rt->ladar_pts_local); 
  sendMapElement(&mappts,-2); 

  // -2 to visualize on alice, 0 to get listened to, something else for private debugging purposes

  // MODladarCurbPerceptor -- check out latest branch of interfaces

  // send vehicle position to map

  MapElement mapstate;
  point2arr state_ptarr;

  state_ptarr.clear();
  state_ptarr.push_back(statept);

  mapstate.setTypeAlice();
  mapstate.setId((int)MODladarRoadPerceptor,2);
  mapstate.setState(blob.state);
  mapstate.setColor(MAP_COLOR_RED);
  mapstate.setGeometry(state_ptarr); 
  sendMapElement(&mapstate,-2); 
  
  // send estimated curb line to map

  MapElement mapcurb;

  if (rt->right_good) {

    point2arr curb_ptarr;
    point2 frontpt, rearpt;
    
    frontpt.x = rt->filter_box_front;
    rearpt.x = rt->filter_box_rear;
    frontpt.y = rearpt.y = rt->right_x;
    
    curb_ptarr.clear();
    curb_ptarr.push_back(statept + frontpt.rot(blob.state.localYaw));
    curb_ptarr.push_back(statept + rearpt.rot(blob.state.localYaw));
    
    mapcurb.setTypePoly();
    mapcurb.setId((int)MODladarRoadPerceptor,3);
    mapcurb.setColor(MAP_COLOR_GREEN);
    mapcurb.setGeometry(curb_ptarr); 
    sendMapElement(&mapcurb,-2); 
  }
  else {
    mapcurb.setTypeClear();
    mapcurb.setId((int)MODladarRoadPerceptor,3);
    sendMapElement(&mapcurb,-2); 
  }



  return 0;
} 



// Template for console
/*
01234567890123456789012345678901234567890123456789012345678901234567890123456789
*/
static char *consoleTemplate =
"LadarCurbPerceptor $Revision$                                          \n"
"                                                                           \n"
"Skynet: %spread%                                                           \n"
"                                                                           \n"
"Ladar[0]: %ladar0%                                                         \n"
"Ladar[1]: %ladar1%                                                         \n"
"Ladar[2]: %ladar2%                                                         \n"
"Ladar[3]: %ladar3%                                                         \n"
"Ladar[4]: %ladar4%                                                         \n"
"Ladar[5]: %ladar5%                                                         \n"
"                                                                           \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"%stderr%                                                                   \n"
"                                                                           \n"
"[%QUIT%|%PAUSE%]                                                           \n";


// Initialize console display
int LadarCurbPerceptor::initConsole()
{
  // Initialize console
  this->console = cotk_alloc();
  assert(this->console);

  // Set the console template
  cotk_bind_template(this->console, ::consoleTemplate);

  // Bind buttons and toggles
  cotk_bind_button(this->console, "%QUIT%", " QUIT ", "Qq",
                   (cotk_callback_t) onUserQuit, this);
  cotk_bind_toggle(this->console, "%PAUSE%", " PAUSE ", "Pp",
                   (cotk_callback_t) onUserPause, this);
    
  // Initialize the display
  cotk_open(this->console,NULL);
  
  // Display some fixed values
  cotk_printf(this->console, "%spread%", A_NORMAL, "%s:%d:%s",
              this->spreadDaemon, this->skynetKey, modulename_asString(this->moduleId));

  return 0;
}


// Finalize sparrow display
int LadarCurbPerceptor::finiConsole()
{
  // Clean up the CLI
  if (this->console)
  {
    cotk_close(this->console);
    cotk_free(this->console);
    this->console = NULL;
  }
  
  return 0;
}


// Handle button callbacks
int LadarCurbPerceptor::onUserQuit(cotk_t *console, LadarCurbPerceptor *self, const char *token)
{
  MSG("user quit");
  self->quit = true;
  return 0;
}


// Handle button callbacks
int LadarCurbPerceptor::onUserPause(cotk_t *console, LadarCurbPerceptor *self, const char *token)
{
  self->pause = !self->pause;
  MSG("pause %s", (self->pause ? "on" : "off"));
  return 0;
}


// Main program thread
int main(int argc, char **argv)
{
  LadarCurbPerceptor *percept;
  
  percept = new LadarCurbPerceptor();
  assert(percept);

  // Parse command line options
  if (percept->parseCmdLine(argc, argv) != 0)
    return -1;
  
  // Initialize sensnet
  if (percept->initSensnet() != 0)
    return -1;

  // Initialize cotk display
  if (!percept->options.disable_console_flag)
    if (percept->initConsole() != 0)
      return -1;

  percept->send_debug = percept->options.send_debug_flag;
  if (percept->send_debug){
    cout << "Sending data to debugging channel -2" << endl;
  }else{
     cout << "Debugging map element messages off" << endl;
  }




  fprintf(stderr, "entering main thread of LadarCurbPerceptor \n");

  percept->loopCount =0;
  
  while (!percept->quit)
  {
    int tempCount = percept->loopCount;
    percept->loopCount = tempCount+1;

    // Update the console
    if (percept->console)
      cotk_update(percept->console);

    // If we are paused, dont do anything
    if (percept->pause)
    {
      usleep(0);
      continue;
    }

    // Get new data and update our perceptor
    if (percept->update() != 0)
      break;
  }

  if (percept->console)
    percept->finiConsole();
  percept->finiSensnet();

  MSG("exited cleanly");
  
  return 0;
}

