//----------------------------------------------------------------------------
//
//  Ladar road edge tracking code 
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2007, University of Delaware
//
//----------------------------------------------------------------------------

#include "UD_Ladar_RoadTracker.hh"

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// ./sensviewer /dgc/test_logs/elToro_visit1/*PTU* /dgc/test_logs/elToro_visit1/*13*MF_BUMPER* --rndf=eltoro_field_demo1_short.rndf --ladar-scans=100

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

// numbering lanes from 0 at far right to num_lanes_with + num_lanes_against - 1 at far left

// for all testing of Aug. 7 El Toro logs, these numbers were 4.0, 1, 1, 0, 2
// assuming an extra two lane-widths are to searched on each side

// current lane would change with a lane change, of course

void UD_Ladar_RoadTracker::set_lateral_filter_box(double lane_width, int num_lanes_with, int num_lanes_against, int current_lane, int num_shoulder_lanes)
{
  double lanes_to_right, lanes_to_left, lanes_to_centerline;

  // limits of box in vehicle coordinates for filtering out ladar points

  //  cTL[0] = cBL[0] = baselineL[0] = -14.0;
  //  cTR[0] = cBR[0] = baselineR[0] = 10.0;

  lanes_to_right = 0.5 + (double) (current_lane + num_shoulder_lanes);
  lanes_to_left = 0.5 + (double) (num_lanes_with + num_lanes_against - current_lane - 1 + num_shoulder_lanes); 
  lanes_to_centerline = 0.5 * (double) (num_lanes_with - current_lane - 1);
 
  filter_box_left = -lanes_to_left * lane_width;
  filter_box_right = lanes_to_right * lane_width;
  centerline_x = -lanes_to_centerline * lane_width;

  cTL[0] = cBL[0] = baselineL[0] = filter_box_left;
  cTR[0] = cBR[0] = baselineR[0] = filter_box_right;
  
  VectCross(cBL, cTL, eL);
  VectCross(cBR, cTR, eR);
  VectCross(cTL, cTR, eT);
  VectCross(cBL, cBR, eB);

  VectCross(baselineL, baselineR, baseline);
}

//----------------------------------------------------------------------------

// constructor for ladar road edge tracker class 

// max_scans sets maximum size of queue of scans for EACH ladar...so if PTU and MF_BUMPER are being used,
// there will be two non-empty buffers of scans with up to max_scans entries each

UD_Ladar_RoadTracker::UD_Ladar_RoadTracker(double ransac_inlier_good_frac_thresh, double ransac_dist_thresh, 
					   int max_sca, double min_obs_height)
{
  int i;

  ransac_inlier_good_frac_threshold = ransac_inlier_good_frac_thresh;
  ransac_dist_threshold = ransac_dist_thresh;

  max_scans = max_sca;

  min_obstacle_height = min_obs_height;

  //  allscans_local = (deque<point2arr> *) malloc((MAX_LADAR_SENSOR_ID + 1) * sizeof(deque<point2arr>));
  allscans_local.resize(MAX_LADAR_SENSOR_ID + 1);
  for (i = 0; i <= MAX_LADAR_SENSOR_ID; i++) 
    allscans_local[i].clear();

  filter_box_front = 20;
  filter_box_rear = -10;
  num_edge_bins = filter_box_front - filter_box_rear;

  left_edge.resize(num_edge_bins);
  right_edge.resize(num_edge_bins);

  left_count = (int *) malloc(num_edge_bins*sizeof(int));
  right_count = (int *) malloc(num_edge_bins*sizeof(int));

  cTL[1] = cTR[1] = filter_box_front;
  cTL[2] = cTR[2] = 1.0;

  cBL[1] = cBR[1] = filter_box_rear;
  cBL[2] = cBR[2] = 1.0;

  baselineL[1] = baselineR[1] = 0.0;
  baselineL[2] = baselineR[2] = 1.0;

  //  set_lateral_filter_box(4.0, 1, 1, 0, 2);
  set_lateral_filter_box(4.0, 1, 1, 0, 1);
}

//----------------------------------------------------------------------------

// decide whether to save this scan (on principle of maintaining a decent density of points
// in history buffer regardless of vehicle speed) and if so include just the apparent obstacle
// points based on edge detection

// [this was based on predrawPointCloud in sensviewer's LadarSink.cc]

void UD_Ladar_RoadTracker::filter_new_scan(LadarRangeBlob *blob, VehicleState &state)
{
  int i;
  float pa, pr, px, py, pz;
  float vx, vy, vz;
  float lx, ly, lz;
  point2 pt_local;
  float last_vz, diff_vz;
  bool last_good;
  int max_size, excess;

  if (blob->sensorId < 0 || blob->sensorId > MAX_LADAR_SENSOR_ID) {
    fprintf(stderr, "sensorId %i out of range\n", blob->sensorId);
    return;
  }

  //  fprintf(stderr, "blob %i with %i points\n", blob->sensorId, blob->numPoints);

  // test whether this scan should be entered based on timestamp, vehicle speed, and desired sweep density

  // if no...

  if (state.vehSpeed < 1.0) {  // this test is too crude

    // don't queue any scanlines if we're not really moving

    //    fprintf(stderr, "too slow to queue\n");

  }

  // if yes...get points from this scan into local coordinates, look for edge points

  else {
    
    onescan_local.clear();

    for (i = 0, last_good = false; i < blob->numPoints; i++) {
    
      pa = blob->points[i][0];
      pr = blob->points[i][1];
      
      if (pr > 0 && pr < 80.0) {   // in range
	
	// Compute points in sensor frame
	LadarRangeBlobScanToSensor(blob, pa, pr, &px, &py, &pz);
	
	// Transform to vehicle frame
	LadarRangeBlobSensorToVehicle(blob, px, py, pz, &vx, &vy, &vz);
	
	// Transform to local frame
	LadarRangeBlobVehicleToLocal(blob, vx, vy, vz, &lx, &ly, &lz);
	
	if (last_good) {
	  diff_vz = fabs(vz - last_vz);
	  if (diff_vz > min_obstacle_height && 
	      vy < baselineR[0] && vy > baselineL[0]) {   // 4 m. lanes, driving on right + search 2 more lane-widths on either side
	    pt_local.x = lx;
	    pt_local.y = ly;
	    onescan_local.push_back(pt_local);
	  }
	}
	
	last_vz = vz;
	last_good = true;
      }
    }

    // now put filtered scan into history buffer
    
    allscans_local[blob->sensorId].push_front(onescan_local);
    max_size = (int) rint(1000.0 / state.vehSpeed);
    if (max_size > max_scans)
      max_size = max_scans;
    excess = allscans_local[blob->sensorId].size() - max_size;
    if (excess > 0) 
      allscans_local[blob->sensorId].resize(max_size);
  }
}

//----------------------------------------------------------------------------

// is this particular point close enough to the candidate RIGHT road edge line 
// and on the correct side (the outside) to be an inlier?

bool UD_Ladar_RoadTracker::right_test(double distance)
{	
  return (distance >= 0 && distance <= ransac_dist_threshold);
}

//----------------------------------------------------------------------------

// is this particular point close enough to the candidate LEFT road edge line 
// and on the correct side (the outside) to be an inlier?

bool UD_Ladar_RoadTracker::left_test(double distance)
{	
  return (distance < 0 && distance >= -ransac_dist_threshold);
}
    
//----------------------------------------------------------------------------

// intersection of road edge line (in homogeneous form and in vehicle coordinates)
// with forward and rear edges of "window" used to filter ladar points

void UD_Ladar_RoadTracker::get_homogeneous_line_endpts(Vect l, Vect T, Vect B)
{
  VectCross(l, eT, T); VectDeHomogenize(T);
  VectCross(l, eB, B); VectDeHomogenize(B);
}

//----------------------------------------------------------------------------

// find strongest RIGHT vertical (i.e., straight forward in vehicle coordinates)
// line in ladar data--ladar points should be to right of it

bool UD_Ladar_RoadTracker::right_vertical_RANSAC_line(point2arr points, Vect &lbest, double &num_inliers)
{
  int i, u, num_rays, best_u;
  float cur_num_inliers, distance, lnormfactor;
  Vect pu, pv, l, interbaseline;

  num_rays = points.size();
  if (num_rays < 1)
    return false;

  num_inliers = -1;

  pu[2] = pv[2] = 1.0;

  for (u = 0; u < num_rays - 1; u++) {
    
    pv[0] = points[u].x;
    pv[1] = points[u].y - 10;
    
    pu[0] = points[u].x;
    pu[1] = points[u].y;

    VectCross(pu, pv, l);

    // make sure l intersects baseline to RIGHT of robot; else continue;

    VectCross(l, baseline, interbaseline);  VectDeHomogenize(interbaseline);  
    if (interbaseline[0] < 0) 
      continue;

    lnormfactor = 1.0 / sqrt(l[0]*l[0] + l[1]*l[1]);
    l[0] *= lnormfactor;
    l[1] *= lnormfactor;
    l[2] *= lnormfactor;

    // count line support
    
    for (i = 0, cur_num_inliers = 0; i < num_rays; i++) {

      if (points[i].x >= filter_box_right) 
	continue;

      pu[0] = points[i].x;
      pu[1] = points[i].y;
	
      distance = VectDotProd(pu, l);
      
      if (right_test(distance)) 
	cur_num_inliers++;
    }

    // update max support and best model
    
    // either more inliers or the same number, but closer to the road center

    if (cur_num_inliers > num_inliers || 
	(cur_num_inliers == num_inliers && points[u].x < points[best_u].x)) {

      num_inliers = cur_num_inliers;

      lbest[0] = l[0];
      lbest[1] = l[1];
      lbest[2] = l[2];

      best_u = u;
    }
  }

  return true;  
}

//----------------------------------------------------------------------------

// primary update function to find right and possibly left road edges from
// ladar cues

void UD_Ladar_RoadTracker::update(VehicleState &state)
{
  point2 pt_vehicle, pt_local;
  vec3_t p;
  int i, j, k, bin;

  current_state = state;

  // transfer ladar points from multiple deques of point2arrs to one big point2arr in LOCAL coordinates
      
  ladar_pts_local.clear();

  for (k = 0; k <= MAX_LADAR_SENSOR_ID; k++) 
    if (allscans_local[k].size() > 0) {                       // if this is a ladar we're using
      // fprintf(stderr, "got a ladar at %i\n", k);

      for (i = 0; i < allscans_local[k].size(); i++)          // iterate over each scan in its history
	for (j = 0; j < allscans_local[k][i].size(); j++) {   // iterate over each point in this scan
      
	  pt_local.x = allscans_local[k][i][j].x;
	  pt_local.y = allscans_local[k][i][j].y;
	  ladar_pts_local.push_back(pt_local);  
	}
    }

  // convert LOCAL to VEHICLE coords

  current_pose_stable.pos = vec3_set(state.localX, state.localY, state.localZ);
  current_pose_stable.rot = quat_from_rpy(0, 0, state.localYaw);

  ladar_pts_vehicle.resize(ladar_pts_local.size());

  fprintf(stderr, "%i total points\n", ladar_pts_vehicle.size());

  for (i = 0; i < ladar_pts_local.size(); i++) {
    p.x = ladar_pts_local[i].x;
    p.y = ladar_pts_local[i].y;
    p.z = state.localZ; 
    p = vec3_transform(pose3_inv(current_pose_stable), p);
    ladar_pts_vehicle[i].y = p.x;
    ladar_pts_vehicle[i].x = p.y; 
  }

  // do analysis

  for (i = 0; i < num_edge_bins; i++) {
    left_edge[i].y = filter_box_rear + i;
    right_edge[i].y = filter_box_rear + i;
    left_edge[i].x = baselineL[0];
    right_edge[i].x = baselineR[0];

    left_count[i] = 0;
    right_count[i] = 0;
  }

  // project points to either right edge histogram or left edge depending on which
  // side of nominal lane centerline they are on

  for (i = 0; i < ladar_pts_vehicle.size(); i++) {

    if (ladar_pts_vehicle[i].y >= filter_box_rear && ladar_pts_vehicle[i].y < filter_box_front) { 

      bin = (int) floor(ladar_pts_vehicle[i].y - filter_box_rear);

      if (ladar_pts_vehicle[i].x < centerline_x && ladar_pts_vehicle[i].x > filter_box_left) {   
	left_count[bin]++;
	if (ladar_pts_vehicle[i].x > left_edge[bin].x) 
	  left_edge[bin].x = ladar_pts_vehicle[i].x;
      }

      else if (ladar_pts_vehicle[i].x > centerline_x && ladar_pts_vehicle[i].x < filter_box_right) {
	right_count[bin]++;
	if (ladar_pts_vehicle[i].x < right_edge[bin].x) 
	  right_edge[bin].x = ladar_pts_vehicle[i].x;
      }
    }
  }
  
  // find right line via RANSAC; if good enough, filter

  if (right_vertical_RANSAC_line(right_edge, rans_right_l, right_inliers)) {

    right_in.clear();
    right_out.clear();

    right_in =  get_right_vertical_inliers(rans_right_l,  right_edge);
    right_out =  get_right_vertical_outliers(rans_right_l,  right_edge);

    if ((double) right_in.size() / (double) right_edge.size() < ransac_inlier_good_frac_threshold)   
      right_good = false;
    else {

      right_good = true;

      // closest to vehicle (i.e., sampled point)

      for (i = 0, right_x = filter_box_right; i < right_in.size(); i++) {
	if (right_in[i].x < right_x)
	  right_x = right_in[i].x;
      }

      // mean of inliers

      /*
      for (i = 0, right_x = 0.0; i < right_in.size(); i++)
	right_x += right_in[i].x;
      right_x /= (double) right_in.size();
      */

      get_homogeneous_line_endpts(rans_right_l, right_T, right_B);
    }
  }

  // do same for left line if option is enabled

  // ...

  return;


    /*
    frac_good = fraction_ladar_good(ladar_test, 30);
    
    if (do_histogram && frac_good >= 0.9) 
      do_histogram = false;
    else if (!do_histogram && frac_good < 0.1) 
      do_histogram = true;
    */
  
  //  else {
  //    frac_good = 1.0;
  //    do_histogram = false;
  //  }

}

//----------------------------------------------------------------------------

void UD_Ladar_RoadTracker::draw()
{
  /*
  int ii, jj;
  vec3_t p, cTL3, cTR3, cBL3, cBR3, left_T3, left_B3, right_T3, right_B3;
  float z_offset = -1.0;

  glPointSize(2);
  glColor3f(1, 1, 0);
  
  glBegin(GL_POINTS);
  for (ii = 0; ii <  ladar_pts_local.size(); ii++)
    glVertex3f( ladar_pts_local[ii].x,  ladar_pts_local[ii].y,  current_state.localZ);
  glEnd();
  
  glPointSize(8);
  
  glBegin(GL_POINTS);

  if (!right_good) {

    glColor3f(1, 0, 0);
    for (ii = 0; ii <  num_edge_bins; ii++) {
      if ( right_count[ii]) {
	p.y =  right_edge[ii].x;
	p.x =  right_edge[ii].y;
	p.z = 0.0;
	p = vec3_transform(current_pose_stable, p);
	glVertex3f(p.x, p.y, p.z+z_offset);
      }
    }
  }
  else {
  
    glColor3f(0, 1, 0);
    for (ii = 0; ii < right_in.size(); ii++) {
      p.y = right_in[ii].x;
      p.x = right_in[ii].y;
      p.z = 0.0;
      p = vec3_transform(current_pose_stable, p);
      glVertex3f(p.x, p.y, p.z+z_offset);
    }
    
    glColor3f(1, 0, 0);
    for (ii = 0; ii < right_out.size(); ii++) {
      if (right_out[ii].x < filter_box_right) {
	p.y = right_out[ii].x;
	p.x = right_out[ii].y;
	p.z = 0.0;
	p = vec3_transform(current_pose_stable, p);
	glVertex3f(p.x, p.y, p.z+z_offset);
      }
    }
  }

  glEnd();

  glColor3f(0, 0, 1);

  glBegin(GL_LINES);
    
  cTL3.y =  cTL[0];
  cTL3.x =  cTL[1];
  cTL3.z =  cTL[2];
  
  cTR3.y =  cTR[0];
  cTR3.x =  cTR[1];
  cTR3.z =  cTR[2];
  
  cBL3.y =  cBL[0];
  cBL3.x =  cBL[1];
  cBL3.z =  cBL[2];
  
  cBR3.y =  cBR[0];
  cBR3.x =  cBR[1];
  cBR3.z =  cBR[2];
  
  left_T3.y =  left_T[0];
  left_T3.x =  left_T[1];
  left_T3.z =  left_T[2];
  
  left_B3.y =  left_B[0];
  left_B3.x =  left_B[1];
  left_B3.z =  left_B[2];

  right_T3.y =  right_x;  // right_T[0];
  right_T3.x =  right_T[1];
  right_T3.z =  right_T[2];
  
  right_B3.y =  right_x;  // right_B[0];
  right_B3.x =  right_B[1];
  right_B3.z =  right_B[2];
  
  cTL3 = vec3_transform(current_pose_stable, cTL3);
  cTR3 = vec3_transform(current_pose_stable, cTR3);
  cBL3 = vec3_transform(current_pose_stable, cBL3);
  cBR3 = vec3_transform(current_pose_stable, cBR3);
  
  left_T3 = vec3_transform(current_pose_stable, left_T3);
  left_B3 = vec3_transform(current_pose_stable, left_B3);

  right_T3 = vec3_transform(current_pose_stable, right_T3);
  right_B3 = vec3_transform(current_pose_stable, right_B3);
  
  glVertex3f(cTL3.x, cTL3.y, current_state.localZ+z_offset);
  glVertex3f(cTR3.x, cTR3.y, current_state.localZ+z_offset);
  
  glVertex3f(cTR3.x, cTR3.y, current_state.localZ+z_offset);
  glVertex3f(cBR3.x, cBR3.y, current_state.localZ+z_offset);
  
  glVertex3f(cBR3.x, cBR3.y, current_state.localZ+z_offset);
  glVertex3f(cBL3.x, cBL3.y, current_state.localZ+z_offset);
  
  glVertex3f(cBL3.x, cBL3.y, current_state.localZ+z_offset);
  glVertex3f(cTL3.x, cTL3.y, current_state.localZ+z_offset);

  glEnd();

  if (right_good) {

    //    printf("%lf %lf %lf %lf %lf\n", right_x, right_T[0], right_T[1], left_T[0], left_T[1]);

    glLineWidth(5);

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);

    glVertex3f(right_T3.x, right_T3.y, current_state.localZ+z_offset);
    glVertex3f(right_B3.x, right_B3.y, current_state.localZ+z_offset);

    //    glVertex3f(right_T3.x, right_x, current_state.localZ+z_offset);
    //glVertex3f(right_B3.x, right_x, current_state.localZ+z_offset);


    glEnd();

    glLineWidth(1);

  }
  */

}

//----------------------------------------------------------------------------

// return every point closer than RANSAC threshold distance to vertical line

point2arr UD_Ladar_RoadTracker::get_left_vertical_inliers(Vect line, point2arr points)
{
  point2arr inliers;
  point2 p;
  double distance;
  Vect pu;
  int count = 0;

  printf("not yet ready\n");
  exit(1);

  inliers.clear();

  pu[2] = 1.0;

  //  printf("get line "); VectPrint(line);

  for (int i = 0; i < points.size(); i++) {

    pu[0] = points[i].x;
    pu[1] = points[i].y;
	
    distance = VectDotProd(pu, line);
	
    if (left_test(distance)) {
      
      p.x = points[i].x;
      p.y = points[i].y;
      inliers.push_back(p);

      count++;
    }
  }
  
  //  printf("got in %i\n", count);

  return inliers;
}

//----------------------------------------------------------------------------

// return every point farther than RANSAC threshold distance from vertical line

point2arr UD_Ladar_RoadTracker::get_left_vertical_outliers(Vect line, point2arr points)
{
  point2arr outliers;
  point2 p;
  double distance;
  Vect pu;
  int count = 0;

  printf("not yet ready\n");
  exit(1);

  outliers.clear();

  pu[2] = 1.0;

  for (int i = 0; i < points.size(); i++) {

    pu[0] = points[i].x;
    pu[1] = points[i].y;
    
    distance = VectDotProd(pu, line);
    
    if (!left_test(distance)) {
      
      p.x = points[i].x;
      p.y = points[i].y;
      outliers.push_back(p);
      
      count++;
    }
  }

  //  printf("got out %i\n", count);

  return outliers;
}

//----------------------------------------------------------------------------

// return every point closer than RANSAC threshold distance to vertical line

point2arr UD_Ladar_RoadTracker::get_right_vertical_inliers(Vect line, point2arr points)
{
  point2arr inliers;
  point2 p;
  double distance;
  Vect pu;
  int count = 0;

  inliers.clear();

  pu[2] = 1.0;

  //  printf("get line "); VectPrint(line);

  for (int i = 0; i < points.size(); i++) {

    if (points[i].x >= filter_box_right)
      continue;

    pu[0] = points[i].x;
    pu[1] = points[i].y;
    
    distance = VectDotProd(pu, line);
    
    if (right_test(distance)) {
      
      p.x = points[i].x;
      p.y = points[i].y;
      inliers.push_back(p);
      
      count++;
    }
  }
  
  //  printf("got in %i\n", count);

  return inliers;
}

//----------------------------------------------------------------------------

// return every point farther than RANSAC threshold distance from vertical line

point2arr UD_Ladar_RoadTracker::get_right_vertical_outliers(Vect line, point2arr points)
{
  point2arr outliers;
  point2 p;
  double distance;
  Vect pu;
  int count = 0;

  outliers.clear();

  pu[2] = 1.0;

  for (int i = 0; i < points.size(); i++) {

    pu[0] = points[i].x;
    pu[1] = points[i].y;
    
    distance = VectDotProd(pu, line);
    
    if (points[i].x >= filter_box_right || !right_test(distance)) {
      
      p.x = points[i].x;
      p.y = points[i].y;
      outliers.push_back(p);
      
      count++;
    }
  }

  return outliers;
}

//----------------------------------------------------------------------------

void VectPrint(Vect v)
{
  printf("%.2f %.2f %.2f\n", v[0], v[1], v[2]);
}

//----------------------------------------------------------------------------

void VectSwap(Vect v1, Vect v2)
{
  Vect t;

  t[0] = v1[0];
  t[1] = v1[1];
  t[2] = v1[2];

  v1[0] = v2[0];
  v1[1] = v2[1];
  v1[2] = v2[2];

  v2[0] = t[0];
  v2[1] = t[1];
  v2[2] = t[2];
}

//----------------------------------------------------------------------------

void VectCopy(Vect src, Vect dst)
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
}

//----------------------------------------------------------------------------

void VectCross(Vect v1, Vect v2, Vect v3)
{
  v3[0] = v1[1]*v2[2] - v1[2]*v2[1];
  v3[1] = v1[2]*v2[0] - v1[0]*v2[2];
  v3[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

//----------------------------------------------------------------------------

double VectDotProd(Vect v1, Vect v2)
{
  return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

//----------------------------------------------------------------------------

void VectDeHomogenize(Vect v)
{
  v[0] /= v[2];
  v[1] /= v[2];
  v[2] = 1.0;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
