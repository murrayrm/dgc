//----------------------------------------------------------------------------
//
//  Ladar road edge tracking code 
//
//  Christopher E. Rasmussen, cer@cis.udel.edu
// 
//  Copyright 2007, University of Delaware
//
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// UD_Ladar_RoadTracker
//----------------------------------------------------------------------------

#ifndef UD_LADAR_ROADTRACKER_DECS

//----------------------------------------------------------------------------

#define UD_LADAR_ROADTRACKER_DECS

//-------------------------------------------------
// includes
//-------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <deque>

#include <frames/pose3.h>
#include <frames/point2.hh>
#include "interfaces/LadarRangeBlob.h"

#include <iostream>
#include <vector.h>

//#include <GL/glut.h>

//-------------------------------------------------
// defines
//-------------------------------------------------

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

#define SQUARE(x)     ((x) * (x))

#define DEGS_PER_RADIAN     57.29578
#define DEG2RAD(x)          ((x) / DEGS_PER_RADIAN)
#define RAD2DEG(x)          ((x) * DEGS_PER_RADIAN)

#define MAX2(A, B)            (((A) > (B)) ? (A) : (B))
#define MIN2(A, B)            (((A) < (B)) ? (A) : (B))


#define MAX_LADAR_SENSOR_ID      32    // highest sensor id we're expecting (with 0 being lowest)

typedef float Vect[3];

//-------------------------------------------------
// class
//-------------------------------------------------

class UD_Ladar_RoadTracker 
{
  
 public:

  // variables

  int max_scans;                // maximum size of "history" of previous scans to store from EACH ladar
  double ransac_inlier_good_frac_threshold;  // what percentage of bins in fitting window (see below) must be inliers for there to be a road
  double ransac_dist_threshold; // how far away from a candidate line can a point be to be considered an inlier
  double min_obstacle_height;   // threshold on differencing of adjacent ladar point heights to be considered an edge candidate (in meters)

  point2arr onescan_local;      // all points from one scan of one ladar, in local coordinates
  //vector< deque<point2arr> > allscans_local(MAX_LADAR_SENSOR_ID + 1, deque<point2arr>); // array of histories of scans from ladars, in local coordinates (each entry corresponds to a particular ladar)
  vector< deque<point2arr> > allscans_local; // array of histories of scans from ladars, in local coordinates (each entry corresponds to a particular ladar)
  //  deque<point2arr> *allscans_local; // array of histories of scans from ladars, in local coordinates (each entry corresponds to a particular ladar)
  point2arr ladar_pts_local;    // flattened version of allscans_local--all ladars X all times in history => one big point2arr
  point2arr ladar_pts_vehicle;  // ladar_pts_local in vehicle coordinates for state closest to update

  int filter_box_front;         // forward limit of window for road fitting (in meters)
  int filter_box_rear;          // backward limit of window for road fitting 
  double filter_box_left;       // left limit of same window
  double filter_box_right;      // right limit
  double centerline_x;          // lateral position of nominal road centerline within filter box
  int num_edge_bins;            // how many forward-backward bins this range is discretized into (ATM assuming 1 m bin width)
  point2arr left_edge;          // measured nearest road edges on left side over range [edge_rear, edge_front] (in meters)
  point2arr right_edge;         // measured nearest road edges on right side over range [edge_rear, edge_front] (in meters)
  int *left_count;              // how many ladar points projected from left side of vehicle into a particular bin
  int *right_count;             // how many ladar points projected from right side of vehicle into a particular bin

  point2arr right_in, right_out;// inliers and outliers for RANSAC right road edge
  point2arr left_in, left_out;  // the same for left edge
  bool right_good, left_good;   // is there a road edge detected or not?
  double right_x, left_x;       // estimated lateral position of road edge

  Vect rans_right_l;            // fitted right road edge line in homogeneous coordinates (not very exciting, since it must be vertical)
  double right_inliers;         // how many right edge points fit the line we found

  Vect rans_left_l;             // fitted left road edge line in homogeneous coordinates (not very exciting, since it must be vertical)
  double left_inliers;          // how many left edge points fit the line we found

  VehicleState current_state;   // copy of state passed to update()
  pose3_t current_pose_stable;  // derived from current_state

  double frac_good;             // for temporal filtering of decision on whether there is a road edge or not

  // variety of utility variables for lines and points relating to found road edge lines

  Vect left_T, left_B, right_T, right_B;
  Vect eT, eB, eL, eR;
  Vect baselineL, baselineR, baselineC, baseline;
  Vect cTL, cTR, cBL, cBR;

  // functions

  UD_Ladar_RoadTracker(double, double, int, double);              // constructor

  void filter_new_scan(LadarRangeBlob *, VehicleState &);         // filter and push new scan into queue--call for each ladar, call before update()

  void update(VehicleState &);                                    // main update function

  void set_lateral_filter_box(double, int, int, int, int);        // set left/right boundaries of filter window based on a priori lane width, num. lanes, etc.

  void draw();                                                    // assuming its being called from onDraw in WorldWin.cc in sensviewer

  bool right_vertical_RANSAC_line(point2arr, Vect &, double &);   // find best right road edge (exhaustive)
  bool left_vertical_RANSAC_line(point2arr, Vect &, double &);    // find best left road edge (exhaustive)

  bool right_test(double);                                        // is signed distance from right line to point within the inlier threshold?
  bool left_test(double);                                         // same for left line

  point2arr get_left_vertical_inliers(Vect, point2arr);           // pull out all points that are inliers for left road edge
  point2arr get_left_vertical_outliers(Vect, point2arr);          // pull out all points that are outliers for left road edge

  point2arr get_right_vertical_inliers(Vect, point2arr);          // pull out all points that are inliers for right road edge
  point2arr get_right_vertical_outliers(Vect, point2arr);         // pull out all points that are outliers for right road edge

  void get_homogeneous_line_endpts(Vect, Vect, Vect);             // intersection line with front and rear "window" edges in vehicle coordinates

};

//----------------------------------------------------------------------------

void VectPrint(Vect);
void VectSwap(Vect, Vect);
void VectCopy(Vect, Vect);
void VectCross(Vect, Vect, Vect);
double VectDotProd(Vect, Vect);
void VectDeHomogenize(Vect);

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#endif

    
