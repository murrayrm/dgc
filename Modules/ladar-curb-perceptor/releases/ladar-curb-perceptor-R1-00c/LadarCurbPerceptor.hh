/**********************************************************
 **
 **  LADARCURBPERCEPTOR.HH
 **
 **    Time-stamp: <2007-05-22 13:42:15 sam> 
 **
 **    Author: Christopher Rasmussen (modified version of Sam Pfister's TemplateLadarPerceptor)
 **    Created: Wed Apr   14:52:04 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef LADARCURBPERCEPTOR_H
#define LADARCURBPERCEPTOR_H

using namespace std;

#include <alice/AliceConstants.h>
#include <assert.h>
#include <ctype.h>
#include <frames/pose3.h>
#include <frames/coords.hh>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <unistd.h>
#include <vector>

//openGL support
#include <GL/glut.h>

// Sensnet/Skynet support
#include <interfaces/sn_types.h>
#include <sensnet/sensnet.h>
#include <sensnet/sensnet_replay.h>
#include <interfaces/SensnetTypes.h>
#include <interfaces/LadarRangeBlob.h>
#include <interfaces/ProcessState.h>
#include "dgcutils/DGCutils.hh" 
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "interfaces/MapElementMsg.h"

// Console support
#include <ncurses.h>
#include <cotk/cotk.h>

// Cmd-line handling
#include "cmdline.h"

#include "UD_Ladar_RoadTracker.hh"

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define NUMSCANPOINTS 181

//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class LadarCurbPerceptor :public CMapElementTalker
{
  
public:
   // Constructor
  LadarCurbPerceptor();

  // Destructor
  ~LadarCurbPerceptor();
  
  // Parse the command line
  int parseCmdLine(int argc, char **argv);
  
  // Initialize sensnet
  int initSensnet();

  // Clean up sensnet
  int finiSensnet();

  // Update the map
  int update();

  // Initialize console display
  int initConsole();

  // Finalize console display
  int finiConsole();

  //send heartbeat to process-control
  int updateProcessState();
  
  // Console button callback
  static int onUserQuit(cotk_t *console, LadarCurbPerceptor *self, const char *token);

  // Console button callback
  static int onUserPause(cotk_t *console, LadarCurbPerceptor *self, const char *token);

 // Program options
  gengetopt_args_info options;

  // Spread settings
  char *spreadDaemon;
  int skynetKey;
  modulename moduleId;
 
  // Sensnet module
  sensnet_t *sensnet;

  // Sensnet replay module
  sensnet_replay_t *replay;
 
  // Operation mode
  enum {modeLive, modeReplay} mode;

  // Console interface
  cotk_t *console;

  int useDisplay;
  
  // Should we quit?
  bool quit;
  
  // Should we pause?
  bool pause;

  //how many times we've looped in the main program
  int loopCount;

  unsigned long long lastSentHeartbeat;

  // Individual ladar data
  struct Ladar
  {
    // Sensor id
    sensnet_id_t sensorId;

    // Latest blob id
    int blobId;
  };

  // List of currently subscribed ladars
  int numLadars;
  Ladar ladars[16];

  vector<int> lastSentSize;

  bool send_debug;

  int outputSubgroup, debugSubgroup; 

  UD_Ladar_RoadTracker *rt;
  
};
#endif


// Useful message macro
#define MSG(fmt, ...) \
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0)

// Useful error macro
#define ERROR(fmt, ...) \
  (fprintf(stderr, "%s:%d error " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? -1 : -1)


