              Release Notes for "map" module

Release R1-02o (Mon May 28 13:03:10 2007):
	Changed color for polytopes so they wouldn't be confused w/visible obstacles

Release R1-02n (Sat May 26  0:21:32 2007):
	updated getBounds and getBoundsReverse functions to return the
	interpolated data around some turns.  This is a temporary fix, as
	in the future, funtionality should be built on top of the
	getLaneBounds functions. Note that the range argument of the
	funcitons specify the minimum total length of the lines to be
	returned, and the last optional argument, backrange, specifies the
	distance behind state to start the line.  If range and backrange
	are both 20 for example, then the lane lines will start around 20
	meters behind state and extend up to the state point given.  To
	specify lanes that start 20 meters behind and extend 20 meters in
	front, specify range=40 and backrange=20.  This function will
	return left and right lane lines of equal size and will trim to the 
	nearest waypoint.

Release R1-02m (Fri May 25 14:05:11 2007):
	updated interpolation to make it sparser for now to help with the
	polytope creation of planning

Release R1-02l (Wed May 23 13:18:33 2007):
	Updated dist functions in MapElement which calculate the min dist
	to other map elements as well as points or polylines.  Can be used
	to check for partial blocking of lanes.  These functions will
	return 0 if there is overlap between the geometries.  They haven't
	been fully tested but the interface shouldn't change.

Release R1-02k (Sat May 19 17:54:25 2007):
	Added polytope and planner traj elements

Release R1-02j (Sat May 19 10:26:55 2007):
	Fixed some other issues with getTransitionBounds.  Added some new
	transition handling functionality but it's not used yet.  

Release R1-02i (Fri May 18 15:42:31 2007):
	changed retired getTransitionBounds function to not use ptarr
	assignment operator.  May fix a bug in tplanner.  Some intermediate
	work is done on a priori transition bounds calculations.

Release R1-02h (Fri May 18 10:16:54 2007):
	updated getTransitionBounds to work a bit better

Release R1-02g (Fri May 18  9:46:41 2007):
	quick fix for issue getTransitionBounds function in Map.cc has with
	santa anita rndf which loops back on itself.  need a better fix for
	long term and that function itself is going to be retired.


Release R1-02f (Thu May 17 17:31:20 2007):
	reorganized MapPrior RNDF parsing, fixed bug in old function getObstacleDist and GetobstaclePoint.


Release R1-02e (Thu May 17 15:27:39 2007):
	updated interpolation functions in MapPrior.  Updated the
	getWayPointEntries and getWayPointExits functions to return the
	possible transition in the same lane across

Release R1-02d (Wed May 16 23:20:39 2007):
  Very minor tweak to includes for cross-platform build compatability.

Release R1-02c (Tue May 15 23:34:49 2007):
	Fixed bug in getObs functions which would return sensed lanes as
	well as obstacles and vehicles.  Added functions isObstacle() and
	isLine() to MapElement.  Other small bug fixes.

Release R1-02b (Mon May 14 23:29:43 2007):
	Added RDDF, steering, and commanded steering splines

Release R1-02a (Mon May 14 11:42:24 2007):
	Updated the map interface to a cleaner, more uniform structure. 
	Had to change the format of some map interface functions, but will
	migrate all modules which use map to the new version. Implemented a
	cleaner more complete library of RNDF and obstacle query functions
	for the planners.  Cleaned up MapElement object interface which
	might also break some code, but will be easy to fix and easier to
	use from now on.  Implemented some MapElement overlap checking
	functions which use the improved bounding box functionality on the
	MapElement.  Added an elevation term to the MapElement structure to
	handle obstacles like overhanging trees which have freespace
	underneath.  Added additional timestamp to MapElement and
	additional max and min bound terms.

Release R1-01w (Thu May 10 13:52:11 2007):
	Added StateTalker, a small program to send alice info to the mapviewer. Right now it only sends alice's location and traversed path, but eventually it will send actuator state, such as steering commands. Also added a bunch of comments and cleaned up MapElement a bit.

Release R1-01v (Mon Apr 30 12:25:45 2007):
	Updated MapElementTalker to take advantage of new skynet
	functionality to send messages on a sub channel.  Implemented
	variable length skynet messages.  For modules which send map
	elements, no code change is needed, just a recompile against the
	updated map and frames modules.  For modules which receive map
	elements, add the intended subnet group to the initRecvMapElement
	function as follows : initRecvMapElement(skynetKey,recvChannel);
	Without this, change only elements on channel 0 will be received.  
	  Added map interface functions getLaneDistToWaypoint which
	calculate distance along corridor between two points.  Updated
	getBounds, getBoundsReverse and getTransitionBounds to return equal
	sized left and right bounds.

Release R1-01u (Sun Apr 29  0:12:00 2007):
	Fixing MapElement::print_state(), broken by the recent change to
	the VehicleState structure.

Release R1-01t (Wed Apr 25 17:19:24 2007):
	added getLaneID, getSegmentID, checkLaneID functions to tplanner
	interface with map.

Release R1-01s (Fri Mar 30 20:12:18 2007):
	test functions for ellipse class

Release R1-01r (Fri Mar 30  2:16:51 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-01q (Sat Mar 17 11:21:09 2007):
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

Release R1-01p (Sat Mar 17  0:47:19 2007):
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

Release R1-01o (Fri Mar 16  0:27:27 2007):
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

Release R1-01n (Thu Mar 15 13:59:49 2007):
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

Release R1-01m (Wed Mar 14 15:57:56 2007):
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

Release R1-01l (Wed Mar 14 10:14:55 2007):
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount. Added more geometric test functions

Release R1-01k (Tue Mar 13 15:45:32 2007):
		Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

Release R1-01j (Mon Mar 12 15:22:34 2007):
	added getNextStopline function.  Need to debug an RNDF mirroring
	issue but this should work for short term planner testing.
	
Release R1-01i (Sun Mar 11 11:49:20 2007):
	Added clear and state messages to map elements. added test programs testMapElement, and testMapId.

Release R1-01h (Sat Mar 10 17:22:19 2007):
	Fixed bug in getStopline.

Release R1-01g (Fri Mar  9 16:33:35 2007):
	added isStop and isExit query functions to map.

Release R1-01f (Fri Mar  9 12:15:10 2007):
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

Release R1-01e (Thu Mar  8 18:33:06 2007):
	Updated some map functions for use in new tplanner (Sam).

Release R1-01d (Sat Mar  3 15:55:33 2007):
	Updated getStopline function to take only a point in space not a
	stopline label.

Release R1-01c (Thu Mar  1 19:34:02 2007):
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results.	Implemented the getLeftBound and getRightBound functions.

Release R1-01b (Tue Feb 27  8:39:07 2007):
	Complete overhaul of map module.  Moved new map structure from mapper into map module.  Map now parses RNDF files with an internal method and has a very different internal structure.  Still need to implement query functions. 

Release R1-01a (Sun Feb 25 12:56:28 2007):
	Updated to work with new SkynetContainer and interfaces paths

Release R1-00b (Thu Feb  1 19:27:02 2007):
	Fixed links in Makefile.yam and updated Map object for short
	term use in tplanner.

Release R1-00a (Sat Jan 27 18:25:30 2007):
	Added Map class and LocalMapTalker.

Release R1-00 (Sat Jan 27 18:14:18 2007):
	Created.










































