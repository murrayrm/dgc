#include <iostream>
#include <string>
#include "MapElement.hh"
#include "frames/point2_uncertain.hh"

using namespace std;

int main()
{


point2_uncertain a, b, c, d, e, f, g, h;

 a = point2_uncertain(-893.929, 994.459, 0.103636, 0.359817, -0.333664);
 b = point2_uncertain(-892.566, 997.049, 0.103636, 0.359817, -0.333664);
 c = point2_uncertain(-892.127, 997.607, 0.103636, 0.359817, -0.333664);
 d = point2_uncertain(-891.85, 996.931, 0.103636, 0.359817, -0.333664);
 e = point2_uncertain(-891.497, 997.44, 0.103636, 0.359817, -0.333664);
 f = point2_uncertain(-891.415, 997.221, 0.103636, 0.359817, -0.333664);
 g = point2_uncertain(-892.398, 992.643,0.103636, 0.359817, -0.333664);
 h = point2_uncertain(-893.929, 994.459, 0.103636, 0.359817, -0.333664);


 MapElement el;
 
 point2arr_uncertain array1;

 array1.push_back(a); 
 array1.push_back(b);
 array1.push_back(c);
 array1.push_back(d);
 array1.push_back(e);
 array1.push_back(f);
 array1.push_back(g);
 array1.push_back(h);

 el.setGeometry(array1);
 el.setExpandGeo(1.645);

 cout << "geometry: " << endl;
 cout << el.geometry << endl;
 cout << "expandGeo: " << endl;
 cout << el.expandGeo << endl;



  return 0;

}
