/**********************************************************
 **
 **  MAP.CC
 **
 **    Time-stamp: <2007-08-27 15:51:26 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:45 2007
 **
 **
 **********************************************************
 **
 ** Map Class  
 **
 **********************************************************/

#include <cassert>
#include <list>

#include "Map.hh"

using namespace std;


// Useful message macro
//#ifdef __GNUC__
//#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " \
//                  << __PRETTY_FUNCTION__ << ":" << arg << endl)
//#else
//// do we ever use anything else other than gcc? just in case ...
//#define MSG(arg) (cerr << __FILE__ << ':' << __LINE__ << ": " << arg << endl)
//#endif

#define MSG(fmt, ...)\
  (fprintf(stderr, "%s:%d msg   " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__) ? 0 : 0 )

// Useful error macro
#define ERRMSG(arg) MSG("*** ERROR: " << arg)


Map::Map(bool _lineFusion)
    : lineFusion(_lineFusion)
{}

Map::~Map()
{}


void Map::init()
{
  MSG("initializing map");
  numMapObjects = 0;

  usedIndices.clear();
  openIndices.clear();
  for(int i=MAXNUMOBJECTS-1; i >= 0; i--) {
    openIndices.push_back(i);
  }

  memset(IDtoINDEX, -1, NUM_PERCEPTORS*MAXNUMPERCEPTOROBJECTS*sizeof(int));

  //TODO:
  //initialize everything in newMap to 0...

}


bool Map::loadRNDF(string filename) 
{
  bool retval = prior.loadRNDF(filename);
  if (!retval){
    cerr << "in Map::loadRNDF() error loading " << filename << endl;
  }
  this->init();
  return retval;

  
}


int Map::addEl(const MapElement &el)
{
  int index = 0;

  ///////NEW DATA STRUCTURE - only used for obstacles right now....ask sam what to do about lines////////
  int originatingPerceptorID = el.id.dat[0];
  int idFromPerceptor = el.id.dat[1];
  int originatingPerceptor;


  switch(originatingPerceptorID) {

  case 87: //MODladarCarPerceptorLeft: 
    originatingPerceptor = LF_LADAR;
    break;

  case 88: //MODladarCarPerceptorCenter: 
    originatingPerceptor = MF_LADAR;
    break;

  case 89: //MODladarCarPerceptorRight: 
    originatingPerceptor = RF_LADAR;
    break;

  case 90: //MODladarCarPerceptorRear: 
    originatingPerceptor = REAR_LADAR;
    break;

  case 95: //MODladarCarPerceptorRiegl: 
    originatingPerceptor = RIEGL;
    break;

  case 79: //MODradarObsPerceptor: 
    originatingPerceptor = MF_RADAR;
    break;

  case 83: //MODstereoObsPerceptorLong: 
    originatingPerceptor = LONG_STEREO_OBS;
    break;

  case 82: //MODstereoObsPerceptorMedium: 
    originatingPerceptor = MEDIUM_STEREO_OBS;
    break;

  default:
    //unrecognized perceptor
    originatingPerceptor = NUM_PERCEPTORS;
  }

  //take care of clearing obstacles
  //warning: this assumes that anything coming from the ladar/radar/stereo-obs
  //  perceptors IS an obstacle...
  if((ELEMENT_CLEAR == el.type) && (NUM_PERCEPTORS != originatingPerceptor)) {
    index = IDtoINDEX[originatingPerceptor][idFromPerceptor];
    //    MSG("received clear message: index %d, originating Perceptor %d, id from perceptor %d", index, originatingPerceptor,idFromPerceptor);
    if(index != -1) {
	//reset entry in lookup table
	IDtoINDEX[originatingPerceptor][idFromPerceptor] = -1; 
	//in object data, note that this track is dead
	newData[index].trackAlive[originatingPerceptor] = 0;
	//	MSG("processing clear");
    } // else, do nothing for type clear that's not already in the map

  }


  if(el.isObstacle()) {

    if(NUM_PERCEPTORS == originatingPerceptor) {
      MSG("unrecognized perceptor - ID = %d", originatingPerceptorID);
      return -1;
    }

    if(idFromPerceptor < 0 || idFromPerceptor >= MAXNUMPERCEPTOROBJECTS) {
      MSG("object ID out of range - could not add el");
      return -1;
    }

    index = IDtoINDEX[originatingPerceptor][idFromPerceptor];
    //    MSG("for originatingPerceptor %d, w/ object ID %d, index = %d, geometryType = %d", originatingPerceptor, idFromPerceptor, index, el.type );


    //      MSG("processing element. originating Perceptor: %d, elemID: %d, index: %d geometry type: %d, geometry size: %d \n   element timestamp: %llu", originatingPerceptor, idFromPerceptor, index, el.type, el.geometry.size(), el.timestamp);


    if(index == -1) { //haven't seen this ID before; check for overlaps
    //	MSG("checking overlaps for association");
      for(unsigned int i = 0; i < usedIndices.size(); i++) {
        unsigned int j = usedIndices.at(i);
        //TODO: BETTER ASSOCIATION CHECK
        if(el.isOverlap(newData[j].mergedGeometry) || newData[j].mergedMapElement.isOverlap(el.geometry)) {
          index = j;
          IDtoINDEX[originatingPerceptor][idFromPerceptor] = index;
          //	    MSG("found overlap w/ id %d - perceptor %d, ID %d", newData[j].id, originatingPerceptor, idFromPerceptor);
          break; //found match, can stop searching
        }
      }
    }

    //check if we need to add object (if index still =-1, then no overlap found)
    if(index == -1) {
      //	  MSG("adding new object");
      //	    MSG("adding new object w/ id %d - perceptor %d, ID %d", numMapObjects, originatingPerceptor, idFromPerceptor);
      //      MSG("processing element. originating Perceptor: %d, elemID: %d, index: %d geometry type: %d, geometry size: %d \n   element timestamp: %llu", originatingPerceptor, idFromPerceptor, index, el.type, el.geometry.size(), el.timestamp);
      if(openIndices.size() > 0) {
        int newIndex = openIndices.back();
        openIndices.pop_back();
        usedIndices.push_back(newIndex);
        index = newIndex;

        //fill in lookup table
        IDtoINDEX[originatingPerceptor][idFromPerceptor] = newIndex;

        //initialize appropriate fields in newData
	newData[index].inputData[originatingPerceptor].timesSeen = 0; 
	memset(newData[index].trackAlive, 0, NUM_PERCEPTORS*sizeof(int));
	//      MSG("processing element. originating Perceptor: %d, elemID: %d, index: %d geometry type: %d, geometry size: %d \n   element timestamp: %llu", originatingPerceptor, idFromPerceptor, index, el.type, el.geometry.size(), el.timestamp);
	//when adding new element, set merged element to the input element
        newData[index].mergedMapElement = el;
        newData[index].mergedGeometry = el.geometry;
        newData[index].mergedType = el.type;
        newData[index].mergedVelocity = el.velocity;
	//      MSG("processing element. originating Perceptor: %d, elemID: %d, index: %d geometry type: %d, geometry size: %d \n   element timestamp: %llu", originatingPerceptor, idFromPerceptor, index, el.type, el.geometry.size(), el.timestamp);

	//set object id
	newData[index].id = numMapObjects;
        newData[index].mergedMapElement.id.clear();
        newData[index].mergedMapElement.setId(originatingPerceptorID, numMapObjects);
	numMapObjects ++;

      } else {
        MSG("No more space in map, not adding object");
        return -1;
      }
    }

    //      MSG("processing element. originating Perceptor: %d, elemID: %d, index: %d geometry type: %d, geometry size: %d \n   element timestamp: %llu", originatingPerceptor, idFromPerceptor, index, el.type, el.geometry.size(), el.timestamp);
    //copy data over
    newData[index].inputData[originatingPerceptor].timesSeen ++;
    newData[index].inputData[originatingPerceptor].lastSeen = el.timestamp; 
    //      MSG("updating w/ lastSeen = %llu   %llu ", newData[index].inputData[originatingPerceptor].lastSeen, el.timestamp);
    newData[index].inputData[originatingPerceptor].geometry.clear();
    newData[index].inputData[originatingPerceptor].geometry = point2arr(el.geometry);
    newData[index].inputData[originatingPerceptor].velocity = el.velocity;
    newData[index].inputData[originatingPerceptor].type = el.type;

    //note that track is still updating
    newData[index].trackAlive[originatingPerceptor] = 1;
  
  } else { //if not type obstacle, need to use old map for now

    MapId thisid = el.id;
    MapId tmpid;
    unsigned int i;
    for (i = 0; i < data.size(); ++i){
      tmpid = data[i].id;
      if (tmpid == thisid){
	if (el.type==ELEMENT_CLEAR){
	  data.erase(data.begin()+i);
	}else{
	  data[i] = el;
	}
	break;
      }
    }

    if(el.type != ELEMENT_CLEAR) {
      if (i == data.size()) { // not found, add as new
	data.push_back(el);
      }
      if (lineFusionEnabled() && el.isLine())
	{
	  fuseLaneLine(el);
	}
    }
  } //end checking if not type obstacle

  /////////////END NEW DATA STRUCTURE ///////////////
  if(el.isObstacle()) { //if we used new dataStructure
    int tmp = index;
    return tmp;
  } else {
    return 0;
  }
}


void Map::fuseAllLines()
{
  for (unsigned int i = 0; i < data.size(); i++)
  {
    if (data[i].type == ELEMENT_LANELINE) {
      fuseLaneLine(data[i]);
    }
  }
}


float DIST_THRES = 2; // 1m
const float DECAY = 0.9; // decay speed to update prior

static int nfull = 0;
static int nfast = 0;

/// Returns the number of "votes" for the association of the given
/// prior line with the given sensed line. A vote is a sensed
/// point within 'thres' distance of a point in the prior.
/// The actual value returned may be a function of the votes, the
/// actual distance and the number of points in the lines.
/// In any case, higher is better match.
static float getVotes(const MapElement& prior,
        const MapElement& sensed, float thres)
{
  // check bounding boxes first
  // enlarge bounding boxes by the given threshold
  // TODO: use uncertainty ...
  point2 min1 = prior.geometryMin - point2(thres/2, thres/2);
  point2 max1 = prior.geometryMax + point2(thres/2, thres/2);
  point2 min2 = sensed.geometryMin - point2(thres/2, thres/2);
  point2 max2 = sensed.geometryMax + point2(thres/2, thres/2);
  
  if (max1.x < min2.x || min1.x > max2.x
      || max1.y < min2.y || min1.y > max2.y)
  {
    nfast++;
    return 0;
  }

  nfull++;
  // bounding boxes overlap, stop being smart and do the full check
  
  const point2arr_uncertain& priorGeom = prior.geometry;
  const point2arr_uncertain& sensedGeom = sensed.geometry;
  float rss = -1;
  int votes = 0;
  unsigned int i, j;
  for (i = 0; i < sensedGeom.size(); i++)
  {
      for (j = 0; j < priorGeom.size(); j++)
      {
          point2 v = priorGeom[j] - sensedGeom[i];
          float dist2 = v.x*v.x + v.y*v.y;
          if (dist2 < thres*thres) {
              votes++;
              rss += dist2;
          }
      }
  }
  //if (rss < 0) return 0.0;
  //return 1.0/(2.0 + log(rss/votes));
  //return 1.0/(log(rss/votes) + (sensedGeom.size() - votes)/sensedGeom.size() + 1.0);
  return votes;
}

//const float VOTE_THRES = 1.0/(2.0 + log(DIST_THRES*DIST_THRES/4));
//const float VOTE_THRES = 1.0/(1.0 + log(DIST_THRES*DIST_THRES));
const float VOTE_THRES = 4; // minimum absolute number of votes required
const float VOTE_RATIO_THRES = 0.66; // ratio between votes and total sensed points


void Map::fuseLaneLine(const MapElement& laneEl)
{

  //MSG("begin: el.id=" << laneEl.id);

  MapElement laneElMod = laneEl;
  laneElMod.setMinMaxFromGeometry(); // make sure it has bouding box info

  const point2arr_uncertain& sensedOrig = laneEl.geometry;
  point2arr_uncertain& sensed = laneElMod.geometry;
  sensed.clear();
  sensed.arr.reserve(sensedOrig.size()*2);

  // linearly interpolate sensed line, so each point is distant no more than
  // DIST_THRES from the previous
  for (int i = 0; i < int(sensedOrig.size()) - 1; i++)
  {
      point2_uncertain vec = sensedOrig[i + 1] - sensedOrig[i];
      float length = vec.norm();
      if (length > DIST_THRES) {
          int nparts = int(ceil(length / DIST_THRES));
          for (int k = 0; k < nparts; k++) {
              sensed.push_back(sensedOrig[i] + vec * k / nparts);
          }
      } else {
          sensed.push_back(sensedOrig[i]);
      }
  }
  if (sensedOrig.size() > 0)
      sensed.push_back(sensedOrig.arr.back());



  // get the current segment
  int minidx = -1;
  float mindist = 1e20;
  point2 here(laneEl.state.localX, laneEl.state.localY);
  for (unsigned int i = 0; i < prior.lanes.size(); i++)
  {
    MapElement el;
    prior.getElFull(el, prior.lanes[i].waypoint_elindex);
    for (unsigned int j = 0; j < el.geometry.size(); j++)
    {
      float dist = (el.geometry[j] - here).norm();
      if (dist < mindist) {
        minidx = i;
        mindist = dist;
      }
    }
  }

  int idx[2] = {
    prior.lanes[minidx].leftbound_elindex,
    prior.lanes[minidx].rightbound_elindex
  };
  int which = -1;
  float maxvote = VOTE_RATIO_THRES;
  float vote, voteRatio;
  MapElement elem[2];
  prior.getElFull(elem[0], idx[0]);
  prior.getElFull(elem[1], idx[1]);
  for (int j = 0; j < 2; j++)
  {
    point2arr lane = elem[j].geometry; // convert from point2arr_uncertain to point2arr
    //float dist = lane.get_min_dist_line(laneEl.geometry);
    //float dist;
    vote = getVotes(laneElMod, elem[j], DIST_THRES);
    voteRatio = vote / sensed.size();
    if (vote > VOTE_THRES && voteRatio > maxvote)
    {
      which = j;
      maxvote = voteRatio;
      //MSG("Vote for " << (j == 0 ? " (left)" : " (right)")
      //    << " lane " << prior.lanes[minidx].label
      //    << " = " << vote << ">= " << VOTE_THRES << ", may be fused");
    } else if (vote > 0) {
      //MSG("Vote for " << (j == 0 ? " (left)" : " (right)")
      //  << " lane " << prior.lanes[minidx].label
      //  << " = " << vote << "< " << VOTE_THRES << ", not fusing");
    }
  }

  if (which != -1)
  {
    //MSG("Fusing " << (which == 0 ? "left" : "right") << " lane "
    //    << prior.lanes[minidx].label << " with sensed line "
    //    << laneEl.id << ", vote was = " << maxvote);
    MapElement rndfEl;
    prior.getEl(rndfEl, idx[which]);
    fuseLaneLine(elem[which], laneElMod, rndfEl);

    // update center line and the other lane, enforcing the lane width
    int other = (which == 0) ? 1 : 0;
    MapElement center;
    prior.getElFull(center, prior.lanes[minidx].waypoint_elindex);
    double width = prior.lanes[minidx].width;

    point2arr_uncertain& geom1 = elem[which].geometry;
    point2arr_uncertain& geom2 = elem[other].geometry;
    point2arr_uncertain& geomC = center.geometry;
    
    assert(geomC.size() == geom1.size());
    assert(geomC.size() == geom2.size());
    assert(geomC.size() >= 2);

    for (unsigned int i = 0; i < center.geometry.size(); i++)
    {
        point2 c = 0.5 * (geom1[i] + geom2[i]);
        point2 vec = geom2[i] - geom1[i];
        point2 prev = geomC[i == 0 ? i : (i - 1)];
        point2 next = geomC[i == (geomC.size()-1) ? i : (i + 1)];
        point2 n = next - prev;
        if (which == 1) /* 1 == right */
            n = point2(n.y, -n.x); // rotate by 90°, get the normal pointing to the left
        else
            n = point2(-n.y, n.x); // rotate by 90°, get the normal pointing to the right
        double nl = n.norm();
        double w = vec.norm();
        if (w < width) // enforce minimum width
            w = width;
        // enforce points to stay on the normal
        geom1[i] = c - n * 0.5 * width / nl;
        geom2[i] = c + n * 0.5 * width / nl;
        geomC[i] = c;
    }

    elem[0].updateFromGeometry();
    elem[1].updateFromGeometry();
    center.updateFromGeometry();
    prior.setElFull(elem[0], idx[0]);
    prior.setElFull(elem[1], idx[1]);
    prior.setElFull(center, prior.lanes[minidx].waypoint_elindex);
  }

  //MSG("Did full check " << nfull << "/" << nfull+nfast << " times");
  //MSG("Did fast bbox check " << nfast << "/" << nfull+nfast << " times");
  //MSG("end");
}

void Map::fuseLaneLine(MapElement& priorEl, const MapElement& sensEl,
                       const MapElement& rndfEl)
{
  assert(sensEl.isLine());
  // associate points in laneEl with points in elem
  point2arr_uncertain& lane = priorEl.geometry;
  const point2arr_uncertain& sensed = sensEl.geometry;

  // list of associations of the type: (prior index, (list of sensed indexes))
  typedef pair<int, list<int> > assoc_pair_t;
  typedef list<assoc_pair_t> assoc_t;
  assoc_t assoc;
  assoc_t::iterator iter;
  assoc_pair_t* curass;
  vector<bool> isAssoc(lane.size(), false); // true = lane[j] was associated with something
  for (unsigned int j = 0; j < sensed.size(); j++)
  {
    float mindist = 10000;
    int minidx = -1;
    int count = 0;
    // find all prior points within threshold distance and put them into 'assoc'
    for (unsigned int k = 0; k < lane.size(); k++)
    {
      assoc.push_back(assoc_pair_t(k, list<int>()));
      curass = &assoc.back();
      point2 vec = sensed[j] - lane[k];
      float distSq = vec.x*vec.x + vec.y*vec.y; // distance squared
      if (distSq < DIST_THRES*DIST_THRES)
      {
        curass->second.push_back(j); // associate k with j
        isAssoc[k] = true;
        count++;
      } else if (distSq < mindist * mindist) {
	minidx = k;
	mindist = sqrt(distSq);
      }
    }

    if (count == 0) {
      count = 1;
      isAssoc[minidx] = true;
      assoc.push_back(assoc_pair_t(minidx, list<int>()));
      curass = &assoc.back();
      curass->second.push_back(j); // associate k with j
    }

  } // end for (all sensed points)

  // use assoc array to correct prior data
  for (iter = assoc.begin(); iter != assoc.end(); ++iter)
  {
    point2_uncertain& ptlane = lane[iter->first];
    point2_uncertain ptsens(0, 0);

    // let the point only move along the normal
    point2 prev = lane[iter->first == 0 ? iter->first : iter->first-1];
    point2 next = lane[iter->first == (int(lane.size())-1) ? iter->first : iter->first+1];
    point2 normal = (next - prev);
    normal = point2(normal.y, -normal.x); // rotate by 90deg
    normal.normalize();

    // let ptsens be the average of all the sensed points associated with ptlane
    // projected on the normal of the line
    list<int>& sensAssoc = iter->second;
    if (sensAssoc.size() > 0)
    {
      //MSG("sensed points associated with " << ptlane << ":");
      for (list<int>::iterator sensIter = sensAssoc.begin();
           sensIter != sensAssoc.end();
           ++sensIter)
      {
        ptsens = ptsens + sensed[*sensIter];
        //cerr << sensed[*sensIter] << " ";
      }
      ptsens = ptsens / sensAssoc.size();
      ptsens = ptlane + normal * normal.dot(ptsens - ptlane);
      //MSG("sensed average: " << ptsens);
      // let the prior be updated using an exponential filter
      // (first order single pole filter, low pass filter, or whatever you want to call it)
      ptlane = ptlane*DECAY + ptsens*(1-DECAY);
    }
  }

  const point2arr_uncertain& rndflane = rndfEl.geometry;
  vector<point2_uncertain>::const_iterator it = rndflane.arr.begin();
  vector<point2_uncertain>::const_iterator next = it;
  ++next;

  // simple FIR filter, but use original rndf data as input
  // for not associated points
  const float w[3] = {1, 2, 1};
  const float invsum = 1.0 / (w[0] + w[1] + w[2]);
  assert(lane.size() > 0);
  point2 prev = lane[0];
  for (int k = 1; k < int(lane.size()) - 1; k++)
  { 
      point2 old = lane[k];
      lane[k] = (prev * w[0] + lane[k] * w[1] + lane[k + 1] * w[2]) * invsum;

      point2 rndfvec = *it - lane[k];
      float dist = rndfvec.norm();

      // check if the next point in the rndf is closer, and if so advance
      if (next != rndflane.arr.end()) {
          point2 rndfvec2 = *next - lane[k];
          float dist2 = rndfvec2.norm();
          if (dist2 < dist) {
              dist = dist2;
              rndfvec = rndfvec2;
              ++it, ++next;
          }
      }

      if (!isAssoc[k] &&  dist < DIST_THRES * 10)
      {
          point2 normal = lane[k + 1] - lane[k - 1];
          normal = point2(normal.y, -normal.x); // rotate by 90deg
          normal.normalize();
          //float p = 0.25 / (dist * dist + 0.1); // sum 0.1 to avoid div by zero
          float p = 0.5 * exp(- 0.5 * dist * dist / (DIST_THRES*DIST_THRES));
          lane[k] = lane[k] + p / (p + 1.0) * normal * normal.dot(rndfvec);
      }

      prev = old;
  }

  // special case for the first and last point
  if (!isAssoc[0])
  {
      float p = 0.1;
      lane[0] = (1 - p) * lane[0] + p * rndflane[0];
  }
  int k = lane.size() - 1;
  if (!isAssoc[k])
  {
      float p = 0.1;
      lane[k] = (1 - p) * lane[k] + p * rndflane.arr.back();
  }


}


int Map::getLane(LaneLabel & lanelabel, const point2& pt)
{
  point2arr centerline;
  unsigned int sze = prior.lanes.size();
  int retval;
  int index = -1;
  double dist=0; 
  double mindist = -1;
  LaneLabel thislabel,minlabel;
  point2 dpt,tmppt;
  for (unsigned int i = 0; i<sze; ++i){
    thislabel = prior.lanes[i].label;
    retval = getLaneCenterLine(centerline,thislabel);
    if (retval<0)
      continue;
    tmppt = centerline.project(pt);
    dpt = pt-tmppt;
    dist = dpt.norm();
    if (mindist<0){
      mindist = dist;
      minlabel = thislabel;
      index = i;
    }else if (dist<mindist){
      minlabel = thislabel;
      mindist=dist;
      index = i;
    }
    
  }
  lanelabel = minlabel;
  return index;
}

int Map::getStopLineSensed(point2_uncertain &pt, 
                           const PointLabel &ptlabelin)
{

  double alongthresh = 2;
  double edgethresh = 4;
  point2_uncertain priorpt;

  point2 stoppt,thispt,projectpt;
  point2arr midline,leftline,rightline;
  int i,numels,retval;
  
  //  getAllStopLines(labelarr,ptarr);
  if (!isStopLine(ptlabelin)){
    cerr <<"in Map::getStopLineSensed point label given " << ptlabelin << " is not a stopline" << endl;
    return -1;
  }
  retval = getWayPoint(priorpt,ptlabelin);
  numels = (int)data.size();

  // get the lane for the desired stopline
  LaneLabel lanelabel(ptlabelin.segment,ptlabelin.lane);
  getLaneCenterLine(midline,lanelabel);
  getLaneBounds(leftline,rightline,lanelabel);

  midline.set_extend(2*alongthresh);
  midline.set_extend(-2*alongthresh);

  leftline.set_extend(2*alongthresh);
  leftline.set_extend(-2*alongthresh);

  rightline.set_extend(2*alongthresh);
  rightline.set_extend(-2*alongthresh);

  

  int minindex = -1;
  double leftdist,rightdist;
  point2arr stopline;

  double distalong = alongthresh+1;
  // check all map elements for candidate stoplines
  for (i=0;i<numels;++i){
    if (data[i].type!=ELEMENT_STOPLINE)
      continue;

    stopline.set(data[i].geometry);
    stoppt.set(data[i].center);
    thispt.set(priorpt);

    //project the stopline into the lane midpoint
    projectpt = midline.project(stoppt);

    leftdist = leftline.get_min_dist_line(stopline);
    rightdist = rightline.get_min_dist_line(stopline);
    // check if stopline spans the lane within a threshold
    if (leftdist>edgethresh || rightdist>edgethresh)
      continue;

    // if the stopline is close enouch to the prior
    // line, then set this as a candidate stopline
    if(thispt.dist(projectpt)<distalong){
      distalong = thispt.dist(projectpt);
      minindex = i;
    }
   
  }
  
  // if a candidate sensed stopline was found, return it
  if (minindex>=0){
    pt = projectpt;//data[minindex].center;
    return 0;
  }
  
  pt = priorpt;
  return 1;
  
}

int Map::getIndexAlongLine(int &index, const point2arr & line, const double dist)
{
  cerr <<"getIndexAlongLine not implemented yet" << endl;
  index =0;
  return -1;
}   

int Map::getPointAlongLine(point2 &ptout, const point2arr & line, const double dist)
{
  ptout = line.get_point_along(dist);
  return 0;
}

int Map::getDistAlongLine(double &dist, const point2arr & line, const point2& pt)
{
  dist = line.project_along(pt);
  return 0;
}

int Map::getDistAlongLine(double &dist,const point2arr & line, const point2& pta, const point2& ptb) 
{
  dist = line.project_along(pta)-line.project_along(ptb);
  return 0;
}

int Map::getProjectToLine(point2 &projpt, const point2arr & line, const point2& pt)
{
  projpt = line.project(pt);
  return 0;
}

int Map::extendLine(point2arr &lineout, const point2arr & line, double dist)
{
  int sze = line.size();
  if (sze<2)
    return -1;
  
  lineout.set(line);
  point2 pta,ptb, dpt,newpt;
  double thisdist;
  if (dist<0){
    pta = line[0];
    ptb = line[1];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta+dpt*dist/thisdist;
    lineout.insert(0,newpt);
  }else{
    pta = line[sze-1];
    ptb = line[sze-2];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta-dpt*dist/thisdist;
    lineout.push_back(newpt);
  }
  return 0;
}


int Map::getObsNearby(vector<MapElement> &elarr,const point2 &pt, const double dist)
{
  MapElement thisEl;
  double thisdist;
  unsigned int sze = data.size();
  elarr.clear();
  point2 tmppt,dpt;
  point2arr ptarr;  

  //this is temporary, until I get spatial look-up working
  for(unsigned int i = 0; i < usedIndices.size(); i++) {
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;

    //no need to check isObstacle (all in newData are...)

    if (thisEl.isOverlap(pt)){
      elarr.push_back(thisEl);
      continue;
    }
    ptarr.set(thisEl.geometry);    
    dpt = pt-ptarr.project(pt);
    thisdist = dpt.norm();
    if (thisdist<dist){
      elarr.push_back(thisEl);
    }

  }

  return elarr.size();
}


int Map::getObsInLane(vector<MapElement> &elarr,const LaneLabel &lanelabelin)
{
  MapElement thisEl;
  unsigned int sze = data.size();
  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  int retval = getLaneBoundsPoly(bounds,lanelabelin);
  if (retval<0) {
    //    MSG("getLaneBoundsPoly returned %d. exiting getObsInLane", retval);
    return retval;
  }

  //  MSG("using new data structure in getObsInLane");
  for (unsigned int i=0; i<usedIndices.size(); i++){
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;
    //    MSG("in getObsInlane, obj %d has area %f ", j, point2arr(newData[j].mergedMapElement.geometry).get_poly_area());
    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }

  return elarr.size(); 
}


int Map::getObsInBounds(vector<MapElement> &elarr, const point2arr &leftbound, const point2arr &rightbound)
{
  MapElement thisEl;

  unsigned int sze = data.size();
  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  point2arr tmpbound;
  tmpbound = rightbound;
  tmpbound.reverse();
  bounds = leftbound;
  bounds.connect(tmpbound);


  //this is temporary, until I get spatial look-up working
  for(unsigned int i = 0; i < usedIndices.size(); i++) {
    int j = usedIndices.at(i);
    thisEl = newData[j].mergedMapElement;

    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }

  return elarr.size(); 

}



int Map::getEl(MapElement &el, const MapId &id)
{
  MapElement thisEl;
  unsigned int sze = data.size();
  
  for (unsigned int i=0; i<sze; ++i){
    thisEl = data[i];
    
    if (thisEl.id==id){
      el =thisEl;
      return 0;
    }
  }
  return -1;
    
 
}

 
//--------------------------------------------------
//  These functions need to be updated and the interface
// may change slightly
//--------------------------------------------------


int Map::getHeading(double & ang,const PointLabel &label)
{
  int retval;
  point2 pt;
  retval = getWayPoint(pt,label);
  if (retval<0){
    cerr << "in Map::getHeading, point label " << label << " not found" << endl;
  }
  
  return getHeading(ang,pt);

}
int Map::getHeading(double & ang,const point2 &pt)
{

  double delta = .5;
  point2arr centerline;

  LaneLabel lanelabel;
  
  getLane(lanelabel,pt);

  getLaneCenterLine(centerline,lanelabel);


  point2 projpt,frontpt,backpt;

  projpt = centerline.project(pt);

  double dist,newdist;

  getDistAlongLine(dist,centerline,projpt);

  point2 newpt,dpt;

  getPointAlongLine(newpt,centerline,dist+delta);
  getDistAlongLine(newdist,centerline,newpt);

  if (newdist-dist < delta/2) {
    getPointAlongLine(newpt,centerline,dist-delta);
    frontpt = projpt;
    backpt = newpt;
  }else{
    frontpt = newpt;
    backpt = projpt;
  }    

  dpt = frontpt-backpt;

  ang = dpt.heading();
  return 0;
}

int Map::getHeading(double & ang, point2 &ptout, const LaneLabel &label, const point2 &pt)
{
  double delta = .5;
  point2arr centerline;

  getLaneCenterLine(centerline,label);


  point2 projpt,frontpt,backpt;

  projpt = centerline.project(pt);

  double dist,newdist;

  getDistAlongLine(dist,centerline,projpt);

  point2 newpt,dpt;

  getPointAlongLine(newpt,centerline,dist+delta);
  getDistAlongLine(newdist,centerline,newpt);

  if (newdist-dist < delta/2) {
    getPointAlongLine(newpt,centerline,dist-delta);
    frontpt = projpt;
    backpt = newpt;
  }else{
    frontpt = newpt;
    backpt = projpt;
  }    

  dpt = frontpt-backpt;

  ang = dpt.heading();
  ptout = newpt;
  return 0;
}

int Map::getLeftBoundType(string& type, const LaneLabel &label)
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label==label){
      type = prior.lanes[i].leftbound_type;
      return 0;
    }
  }
  return -1;
}
int Map::getRightBoundType(string& type, const LaneLabel &label )
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      type = prior.lanes[i].rightbound_type;
      return 0;
    }
  }
  return -1;

}










 

int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel, const point2 state, const double range, const double backrange)
{
  unsigned int i,j;
  int exit_elindex =-1;
  int exit_leftelindex =-1;
  int exit_rightelindex =-1;
  int exit_ptindex =-1;
  int enter_elindex =-1;
  int enter_leftelindex =-1;
  int enter_rightelindex =-1;

  int enter_ptindex =-1;
  
  point2 exitpt;
  point2 enterpt;
      
  bool samelaneflag = false;
  
  bool isvalid = false;
      

  LaneLabel exit_lanelabel, enter_lanelabel;

  exit_lanelabel.segment = exitlabel.segment;
  exit_lanelabel.lane = exitlabel.lane;

  enter_lanelabel.segment = enterlabel.segment;
  enter_lanelabel.lane = enterlabel.lane;

  PointLabel ptlabel(exitlabel);
  ptlabel.point = ptlabel.point+1;
      
  if (exit_lanelabel == enter_lanelabel){
    samelaneflag = true;
  }
  //      cout << "TRANSITION FROM " << exitlabel << " TO " <<ptlabel << endl;
  if (enterlabel == ptlabel){
    int retval = getBounds(leftbound, rightbound, enter_lanelabel,state, range,backrange);
    //cout << "returning current lane transition" << endl;
    return retval;
  }

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== exitlabel){
    
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==exitlabel &&
            prior.lanes[i].exit_link[j]==enterlabel){
          

          isvalid = true;
        }
      }
      if (!isvalid)
        continue;
      
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
        exit_leftelindex = prior.lanes[i].leftbound_elindex;
        exit_rightelindex = prior.lanes[i].rightbound_elindex;
        exit_ptindex =j;
        
        break;
        }
      }
    }
  }
  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;

  }
  isvalid = false;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== enterlabel){
    
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==enterlabel){
          enter_elindex = prior.lanes[i].waypoint_elindex;
          enter_leftelindex = prior.lanes[i].leftbound_elindex;
          enter_rightelindex = prior.lanes[i].rightbound_elindex;
          enter_ptindex =j;
          isvalid = true;
          break;
        }
      }
    }
  }

  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;
  }

  MapElement enter_leftel, enter_rightel;
  MapElement exit_leftel, exit_rightel;
  point2arr enter_leftarr, enter_rightarr;
  point2arr exit_leftarr, exit_rightarr;
  point2 enterpt_left, enterpt_right;
  point2 exitpt_left, exitpt_right;


  prior.getEl(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getEl(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getEl(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getEl(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);


      
  exitpt_left = exit_leftarr[exit_ptindex];  
  exitpt_right = exit_rightarr[exit_ptindex];  

  enterpt_left = enter_leftarr[enter_ptindex];  
  enterpt_right = enter_rightarr[enter_ptindex];  
  

  prior.getElFull(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getElFull(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getElFull(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getElFull(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);
 

  point2arr new_leftarr;
  point2arr new_rightarr;


  point2arr tmpexleft(exit_leftarr);
  point2arr tmpexright(exit_rightarr);
  point2arr tmpenleft(enter_leftarr);
  point2arr tmpenright(enter_rightarr);


      
  exit_leftarr.cut_front(exitpt_left);
  exit_rightarr.cut_front(exitpt_right);
     
  samelaneflag =false;

  if (samelaneflag){
    exit_leftarr.cut_back(enterpt_left);
    exit_rightarr.cut_back(enterpt_right);
  }else{
    exit_leftarr.cut_back_at_index(10);
    exit_rightarr.cut_back_at_index(10);

  }


  enter_leftarr.cut_back(enterpt_left);
  enter_rightarr.cut_back(enterpt_right);
      
  if (samelaneflag){
    enter_leftarr.cut_front(exitpt_left);
    enter_rightarr.cut_front(exitpt_right);
  }else{
    enter_leftarr.cut_front_at_index(10);
    enter_rightarr.cut_front_at_index(10);

  }

  new_leftarr.set(exit_leftarr);
  new_rightarr.set(exit_rightarr);

  double distleft = exitpt_left.dist(enterpt_left);
  double distright = exitpt_right.dist(enterpt_right);
  point2 midpt;


  //--------------------------------------------------
  // merging lane lines
  //--------------------------------------------------
  //      double distlr = fabs(distleft-distright);
  //      double estang = 2*atan2(distlr/2,exitpt_left.dist(exitpt_right));
  //cout << "est ang = " << estang << endl;

  int szeexit = new_leftarr.size();
  //      int szeenter = enter_leftarr.size();
  point2 dptexit, dptenter;
  dptexit= new_leftarr[szeexit-1] - new_leftarr[szeexit-2];
  dptenter= enter_leftarr[1]-enter_leftarr[0];
      
  double angexit = dptexit.heading();
  double angenter = dptenter.heading();

  double dang = fabs(acos(cos(angexit-angenter)));

      
  if (dang < .25){
    //cout << "GOING STRAIGHT" << endl;
    new_leftarr.connect(enter_leftarr);
    new_rightarr.connect(enter_rightarr);
        
  }else{
    if (distleft<distright){
      if (enter_rightarr.size()>0){
        midpt = (new_leftarr.back()+enter_leftarr[0])/2;
        new_leftarr.push_back(midpt);
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect_intersect(enter_rightarr);
      }
    }else{
      if (enter_leftarr.size()>0){
        midpt = (new_rightarr.back()+enter_rightarr[0])/2;
        new_rightarr.push_back(midpt);
        new_leftarr.connect_intersect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
      }
    }
  }
  //  waypts = waypt_el.geometry;
  //  if (left_neardist<0)
  //     left_neardist = 0;

  // if (right_neardist<0)
  //     right_neardist = 0;
  //      double BACK_RANGE = 5;  

      
  double thisrange  = range;
  double thisbackrange = backrange;
  if (range<0)
    thisrange = 1000000;
  if (backrange<0)
    thisbackrange = 1000000;
      

  int cutleftbacknum = 0;
  int cutrightbacknum = 0;
  int cutleftfrontnum = 0;
  int cutrightfrontnum = 0;
      

     
     
  cutleftbacknum = new_leftarr.cut_back_at_index(state,thisbackrange);
  cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);
     
   
  
  cutleftfrontnum = new_leftarr.cut_front_at_index(state,thisrange);
  cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
   
  //       if (distleft>distright){
  //     new_leftarr.cut_front(state,thisrange);
  // new_rightarr.cut_front(new_leftarr.back());
  // }else{
  //  new_rightarr.cut_front(state,thisrange);
  //  new_leftarr.cut_front(new_rightarr.back());
  // }

      
  leftbound.set(new_leftarr);
  rightbound.set(new_rightarr);
  return 0; 

 
}




 
int Map::getShortTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel)
{
  unsigned int i,j;
  int exit_elindex =-1;
  int exit_leftelindex =-1;
  int exit_rightelindex =-1;
  int exit_ptindex =-1;
  int enter_elindex =-1;
  int enter_leftelindex =-1;
  int enter_rightelindex =-1;

  int enter_ptindex =-1;
  
  point2 exitpt;
  point2 enterpt;
      
  bool samelaneflag = false;
  
  bool isvalid = false;
      
  point2arr new_leftarr;
  point2arr new_rightarr;

  MapElement enter_leftel, enter_rightel;
  MapElement exit_leftel, exit_rightel;
  point2arr enter_leftarr, enter_rightarr;
  point2arr exit_leftarr, exit_rightarr;
  point2 enterpt_left, enterpt_right;
  point2 exitpt_left, exitpt_right;

  int cutleftbacknum = 0;
  int cutrightbacknum = 0;
  int cutleftfrontnum = 0;
  int cutrightfrontnum = 0;


  LaneLabel exit_lanelabel, enter_lanelabel;

  exit_lanelabel.segment = exitlabel.segment;
  exit_lanelabel.lane = exitlabel.lane;

  enter_lanelabel.segment = enterlabel.segment;
  enter_lanelabel.lane = enterlabel.lane;

  PointLabel ptlabel(exitlabel);
  ptlabel.point = ptlabel.point+1;
      
  if (exit_lanelabel == enter_lanelabel){
    samelaneflag = true;
  }
  //      cout << "TRANSITION FROM " << exitlabel << " TO " <<ptlabel << endl;
     

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== exitlabel){
    
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==exitlabel &&
            prior.lanes[i].exit_link[j]==enterlabel){
          

          isvalid = true;
        }
      }
      if (!isvalid)
        continue;
      
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
        exit_leftelindex = prior.lanes[i].leftbound_elindex;
        exit_rightelindex = prior.lanes[i].rightbound_elindex;
        exit_ptindex =j;
        
        break;
        }
      }
    }
  }
  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;

  }
  isvalid = false;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label== enterlabel){
    
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==enterlabel){
          enter_elindex = prior.lanes[i].waypoint_elindex;
          enter_leftelindex = prior.lanes[i].leftbound_elindex;
          enter_rightelindex = prior.lanes[i].rightbound_elindex;
          enter_ptindex =j;
          isvalid = true;
          break;
        }
      }
    }
  }

  if (!isvalid){
    cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
    return -1;
  }

      


  prior.getEl(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getEl(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getEl(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getEl(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);


      
  exitpt_left = exit_leftarr[exit_ptindex];  
  exitpt_right = exit_rightarr[exit_ptindex];  

  enterpt_left = enter_leftarr[enter_ptindex];  
  enterpt_right = enter_rightarr[enter_ptindex];  
  

  prior.getElFull(enter_leftel,enter_leftelindex);
  enter_leftarr.set(enter_leftel.geometry);

  prior.getElFull(enter_rightel,enter_rightelindex);
  enter_rightarr.set(enter_rightel.geometry);

  prior.getElFull(exit_leftel,exit_leftelindex);
  exit_leftarr.set(exit_leftel.geometry);

  prior.getElFull(exit_rightel,exit_rightelindex);
  exit_rightarr.set(exit_rightel.geometry);
 

  if (enterlabel == ptlabel){
    int retval = getLaneBounds(new_leftarr, new_rightarr, enter_lanelabel);
        
        

    cutleftbacknum =new_leftarr.cut_back_at_index(enterpt_left,1);
    cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);      
        
        
    cutleftfrontnum = new_leftarr.cut_front_at_index(exitpt_left,1);
    cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
    leftbound.set(new_leftarr);
    rightbound.set(new_rightarr);
    //cout << "returning current lane transition" << endl;

    return retval;
  }


  point2arr tmpexleft(exit_leftarr);
  point2arr tmpexright(exit_rightarr);
  point2arr tmpenleft(enter_leftarr);
  point2arr tmpenright(enter_rightarr);


      
  exit_leftarr.cut_front(exitpt_left);
  exit_rightarr.cut_front(exitpt_right);
     
  samelaneflag =false;

  if (samelaneflag){
    exit_leftarr.cut_back(enterpt_left);
    exit_rightarr.cut_back(enterpt_right);
  }else{
    exit_leftarr.cut_back_at_index(10);
    exit_rightarr.cut_back_at_index(10);

  }


  enter_leftarr.cut_back(enterpt_left);
  enter_rightarr.cut_back(enterpt_right);
      
  if (samelaneflag){
    enter_leftarr.cut_front(exitpt_left);
    enter_rightarr.cut_front(exitpt_right);
  }else{
    enter_leftarr.cut_front_at_index(10);
    enter_rightarr.cut_front_at_index(10);

  }

  new_leftarr.set(exit_leftarr);
  new_rightarr.set(exit_rightarr);

  double distleft = exitpt_left.dist(enterpt_left);
  double distright = exitpt_right.dist(enterpt_right);
  point2 midpt;


    

  int szeexit = new_leftarr.size();
  //int szeenter = enter_leftarr.size();
  point2 dptexit, dptenter;
  dptexit= new_leftarr[szeexit-1] - new_leftarr[szeexit-2];
  dptenter= enter_leftarr[1]-enter_leftarr[0];
      
  double angexit = dptexit.heading();
  double angenter = dptenter.heading();

  double dang = fabs(acos(cos(angexit-angenter)));

  if (dang < .25  || dang >2.5){
    //cout << "GOING STRAIGHT" << endl;
    new_leftarr.connect(enter_leftarr);
    new_rightarr.connect(enter_rightarr);
        
  }else{
    if (distleft<distright){
      if (enter_rightarr.size()>0){
        midpt = (new_leftarr.back()+enter_leftarr[0])/2;
        new_leftarr.push_back(midpt);
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect_intersect(enter_rightarr);
      }
    }else{
      if (enter_leftarr.size()>0){
        midpt = (new_rightarr.back()+enter_rightarr[0])/2;
        new_rightarr.push_back(midpt);
        new_leftarr.connect_intersect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
      }
    }
  }

 

      
  point2arr tmpptarr(new_leftarr);
      
     
  cutleftbacknum =tmpptarr.cut_back_at_index(enterpt_left,1);
  //cout << "cutleftbacvk =  " <<cutleftbacknum << endl;     
  if (dang < 0.25 || dang >2.5)
    cutleftbacknum = new_leftarr.cut_back_at_index(cutleftbacknum);
  else 
    cutleftbacknum = new_leftarr.cut_back_at_index(cutleftbacknum+1);
  //cout << "cutleftbacvk2 =  " <<cutleftbacknum << endl;    
  cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);      
     

  tmpptarr.set(new_leftarr);
  cutleftfrontnum = tmpptarr.cut_front_at_index(exitpt_left);
  if (dang <0.25 || dang >2.5)
    cutleftfrontnum = new_leftarr.cut_front_at_index(cutleftfrontnum);
  else
    cutleftfrontnum = new_leftarr.cut_front_at_index(cutleftfrontnum+1);

  //cout << "cutleftfrontk =  " <<cutleftfrontnum << endl;  
  cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
   

      
  leftbound.set(new_leftarr);
  rightbound.set(new_rightarr);
  return 0; 

 
}














//--------------------------------------------------
//  These functions have reasonable replacements using
// the newer functions
//--------------------------------------------------

bool Map::checkLaneID(const point2 pt, const LaneLabel &label)
{  return checkLaneID(pt, label.segment, label.lane);}

bool Map::checkLaneID(const point2 pt, const int segment, const int lane)
{
  LaneLabel labelin(segment,lane);
  point2_uncertain ptunc(pt);
  return isPointInLane(ptunc,labelin);

}


int Map::getSegmentID(const point2 pt)
{
  PointLabel label;
  int retval = getNextPointID(label,pt);
  if (retval<0){
    cerr << "in Map::getSegmentID : no matches found for point " << pt << endl;
    return -1;
  }
  
  return label.segment;

}

int Map::getLaneID(const point2 pt)
{

  PointLabel label;
  int retval = getNextPointID(label,pt);
  if (retval<0){
    cerr << "in Map::getLaneID : no matches found for point " << pt << endl;
    return -1;
  }
  
  return label.lane;

}

int Map::getClosestPointID(PointLabel &label, const point2 &pt)
{
  unsigned int i,j;
  MapElement el;
  int elindex =-1;
  point2 thispt,minpt;
  PointLabel minlabel;
  double mindist = -1;
  double thisdist = 0;
  for (i=0;i<prior.lanes.size(); ++i){
    elindex = prior.lanes[i].waypoint_elindex;
    prior.getEl(el,elindex);
    for (j=0;j<el.geometry.size();++j){
      thispt = el.geometry[j];
      thisdist = pt.dist(thispt);
      if ((thisdist < mindist) || mindist<0){
        minlabel = prior.lanes[i].waypoint_label[j];
        minpt = thispt;
        mindist = thisdist;
      }
    }
  }

  for (i=0;i<prior.zones.size(); ++i){
    elindex = prior.zones[i].perimeter_elindex;
    prior.getEl(el,elindex);
    for (j=0;j<el.geometry.size();++j){
      thispt = el.geometry[j];
      thisdist = pt.dist(thispt);
      if ((thisdist < mindist) || mindist<0){
        minlabel = prior.zones[i].perimeter_label[j];
        minpt = thispt;
        mindist = thisdist;
      }
    }
  }

  if (mindist<0){
    cerr << "in Map::getClosestPointID no point found " << endl;
    return -1;
  }

  label = minlabel;
  return 0;

}

        
int Map::getNextPointID(PointLabel &label,const point2& pt)
{
  unsigned int i;
  int left_index;
  int right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  int ptindex;
  double distalong;
  

  for (i=0;i<prior.lanes.size();++i){
   
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);

    if (left_side!=right_side){
      //  cout << "LANE NUMBER " << i << " label =  " << prior.lanes[i].label << endl;
      // cout << "left_side = " << left_side << " right_side = " << right_side << endl;
      // cout << "left_index = " << left_index << " right_index = " << right_index << endl;
      // cout << "left_size = " << left_lane.size() << " right_size = " << right_lane.size() << endl;

      //  if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      //cout << "leftindex = " << left_index 
      //     << " rightindex = " << right_index << endl;
      if (left_index==0 &&right_index==0){
    

        //--------------------------------------------------
        // should use center lane
        //--------------------------------------------------
        distalong =left_lane.project_along(pt);
        ptindex = left_lane.get_index_next(distalong);
        //       if (left_index>right_index)
        //   ptindex = (int)ceil(left_index);
        // else
        //  ptindex = (int)ceil(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return 0;
      }
    }
    
  }
  // cerr << "in Map::getNextPointID no point found " << endl;
  return -1;
}

int Map::getLastPointID(PointLabel &label, const point2& pt)
{
  unsigned int i;
  int left_index;
  int right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  int ptindex;
  double distalong ;
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);
    if (left_side!=right_side){
      //      if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){
        //--------------------------------------------------
        // should use center lane
        //--------------------------------------------------
        distalong =left_lane.project_along(pt);
        ptindex = left_lane.get_index_next(distalong)-1;
        //   if (left_index>right_index)
        //   ptindex = (int)floor(left_index);
        // else
        //  ptindex = (int)floor(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return 0;
      }
    }
    
  }
  // cerr << "in Map::getLastPointID no point found " << endl;
  return -1;
}


int Map::getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

  unsigned int i;
  MapElement el;
  //  double left_index;
  //double right_index;
  //  double left_index2;
  // double right_index2;
  //int left_side;
  //int right_side;
  int waypt_elindex;
  MapElement left_el;
  MapElement right_el;
  MapElement waypt_el;
  point2arr left_lane, right_lane, waypts;

  bool validlabel = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      waypt_elindex = prior.lanes[i].waypoint_elindex;
      prior.getEl(waypt_el,waypt_elindex);
      waypts=waypt_el.geometry;
      validlabel = true;
      break;
    }
  }
  if(!validlabel){
    cerr << "in Map::getLaneCenterPoint() passed bad lane label "<<label <<endl;
    return -1;
  }
  


  waypts = waypt_el.geometry;
  double statedist = waypts.project_along(pt);

  double newdist = statedist+offset;

  waypts.cut_front(newdist);//waypts.linelength()-neardist);
  cpt = waypts.back();

  return 0;

}



int Map::getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

  unsigned int i;
  MapElement el;
  int right_elindex;
  MapElement right_el;
  point2arr rightpts;

  bool validlabel = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      right_elindex = prior.lanes[i].rightbound_elindex;
      prior.getEl(right_el,right_elindex);
      validlabel = true;
      break;
    }
  }
  if(!validlabel){
    cerr << "in Map::getLaneRightPoint() passed bad lane label "<<label <<endl;
    return -1;
  }
  
  rightpts = right_el.geometry;
  double statedist = rightpts.project_along(pt);

  double newdist = statedist+offset;

  rightpts.cut_front(newdist);
  cpt = rightpts.back();

  return 0;
}



int Map::getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

  unsigned int i;
  MapElement el;
  int left_elindex;
  MapElement left_el;
  point2arr leftpts;

  bool validlabel = false;
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      left_elindex = prior.lanes[i].leftbound_elindex;
      prior.getEl(left_el,left_elindex);
      validlabel = true;
      break;
    }
  }
  if(!validlabel){
    cerr << "in Map::getLaneLeftPoint() passed bad lane label "<<label <<endl;
    return -1;
  }
  
  leftpts = left_el.geometry;
  double statedist = leftpts.project_along(pt);

  double newdist = statedist+offset;

  leftpts.cut_front(newdist);
  cpt = leftpts.back();

  return 0;
}





int Map::getLeftBound(point2arr& ptarr, const LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].leftbound_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
    }
  }
  return 0;
}

int Map::getRightBound(point2arr& ptarr, const LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].rightbound_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
    }
  }
  return 0;
}


int Map::getZonePerimeter(point2arr& ptarr, int zoneLabel)
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.zones.size(); ++i){
    if (prior.zones[i].label == zoneLabel){
      elindex = prior.zones[i].perimeter_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
      return 0;
    }
  }
  return -1; // not found -- inexistent label
}

int Map::getSpotWaypoints(point2& first, point2& second, const SpotLabel& spotLabel)
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.spots.size(); ++i){
    if (prior.spots[i].label == spotLabel){
      elindex = prior.spots[i].waypt_elindex;
      prior.getElFull(el,elindex);
      assert(el.geometry.size() >= 2);
      if (el.geometry.size() > 2)
          cerr << "Map::getSpotBound():: WARNING: More than 2 waypoints for a parking spot" << endl;
      first = el.geometry[0];
      second = el.geometry[1];
      return 0;
    }
  }
  return -1; // not found -- inexistent label
}


bool Map::isExit(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //bool isexit = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

bool Map::isStop(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //  bool isstop = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

int Map::getStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          isstop = true;
        }
      }
      if (!isstop)
        continue;

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}
int Map::getStopline(point2& pt, point2 state)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel label, stoplabel;
  
  int retval = getNextPointID(label,state);
  if (retval){
    return retval;
  }
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}

int Map::getNextStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel stoplabel;
  

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          label = stoplabel;
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getNextStopline, stop line not found" << label << endl;
  
  return -1;
}



int Map::getWaypoint(point2& pt, const PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];

          return 0;
        }
      }
    }
  }
  cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
  return -1;

}



//   dpt = newpt-projpt;
//   if (dpt.norm()<delta/4)
  
  


//   getPointAlongLine(newpt,centerline,dist+delta);
  
  

//   PointLabel nextlabel,lastlabel;
  
//   int nextval = getNextPointID(nextlabel,pt);
//   int lastval = getLastPointID(lastlabel,pt);

//   //cout << "nextval = " << nextval
//   //    << " lastval = "<< lastval
//   //     <<endl;
//   if (nextval<0 ||lastval <0){
//     cerr << "in Map::getHeading, couldn't find surrounding waypts" << endl;
//     return -1;
//   }

//   point2 nextpt,lastpt,dpt;

//   nextval = getWaypoint(nextpt,nextlabel);
//   lastval = getWaypoint(lastpt,lastlabel);

//   if (nextval<0 ||lastval <0){
//     cerr << "in Map::getHeading, bad getWaypt call" << endl;
//     return -1;
//   }
  
//   dpt = nextpt-lastpt;
  
//   double tmpang = dpt.heading();
//   ang = tmpang;
//   return 0;
// }


int Map::getBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range, const double backrange )
{

      
  //unsigned int i;
  //MapElement el;
  //  double left_index;
  //double right_index;
  //  double left_index2;
  // double right_index2;
  //int left_side;
  //int right_side;
  //int left_elindex,right_elindex, waypt_elindex;
  //MapElement left_el;
  //MapElement right_el;
  //MapElement waypt_el;
  point2arr left_lane, right_lane, waypts;

  //       bool validlabel = false;
  
  //       for (i=0;i<prior.lanes.size(); ++i){
  //         if (prior.lanes[i].label == label){
  //           left_elindex = prior.lanes[i].leftbound_elindex;
  //           right_elindex = prior.lanes[i].rightbound_elindex;
  //           waypt_elindex = prior.lanes[i].waypoint_elindex;
  //           prior.getEl(left_el,left_elindex);
  //           prior.getEl(right_el,right_elindex);
  //           prior.getEl(waypt_el,waypt_elindex);
  //           left_lane=left_el.geometry;
  //           right_lane=right_el.geometry;
  //           waypts=waypt_el.geometry;
  //           validlabel = true;
  //           break;
  //         }
  //       }
  //       if(!validlabel){
  //         cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
  //         return -1;
  //       }
  int retval;
  point2arr_uncertain tmpleft_unc, tmpright_unc, waypts_unc;
     
  retval= prior.getLaneBoundsFull(tmpleft_unc,tmpright_unc,label);
  retval= prior.getLaneCenterLineFull(waypts_unc,label);

  left_lane.set(tmpleft_unc);
  right_lane.set(tmpright_unc);
  waypts.set(waypts_unc);

  if(retval<0){
    cerr << "in Map::getBounds() passed bad lane label "<<label <<endl;
    return -1;
  }
      
      
  double thisrange  = range;
  double thisbackrange = backrange;
  if (range<0)
    thisrange = 1000000;
  if (backrange<0)
    thisbackrange = 1000000;

     
    
  int cutleftbacknum = 0;
  int cutrightbacknum = 0;
  int cutleftfrontnum = 0;
  int cutrightfrontnum = 0;
      
  // left_lane.cut_front_at_index(10);
  //  right_lane.cut_front_at_index(10);


  //  left_lane.cut_back_at_index(10);
  // right_lane.cut_back_at_index(10);
     
     
  cutleftbacknum = left_lane.cut_back_at_index(state,thisbackrange);
  cutrightbacknum = right_lane.cut_back_at_index(cutleftbacknum);
     
   
  
  cutleftfrontnum = left_lane.cut_front_at_index(state,thisrange);
  cutrightfrontnum =right_lane.cut_front_at_index(cutleftfrontnum);

       
  //         left_lane.cut_back_at_index(nextindex);
  //         left_lane.cut_front_at_index(lastindex);

  //       right_lane.cut_back_at_index(nextindex);
  //       right_lane.cut_front_at_index(lastindex);
      

  //      right_lane.cut_back(nearpt);
  //      right_lane.cut_front(farpt);

  leftbound = left_lane;
  rightbound = right_lane;
  return 0;
} 



 
int Map::getBoundsReverse(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range, const double backrange )
{
  //     unsigned int i;
  //       MapElement el;
  //       //  double left_index;
  //       //double right_index;
  //       //  double left_index2;
  //       // double right_index2;
  //       //int left_side;
  //       //int right_side;
  //       int left_elindex,right_elindex, waypt_elindex;
  //       MapElement left_el;
  //       MapElement right_el;
  //       MapElement waypt_el;
  point2arr left_lane, right_lane, waypts;

  //       bool validlabel = false;
  
  //       for (i=0;i<prior.lanes.size(); ++i){
  //         if (prior.lanes[i].label == label){
  //           left_elindex = prior.lanes[i].leftbound_elindex;
  //           right_elindex = prior.lanes[i].rightbound_elindex;
  //           waypt_elindex = prior.lanes[i].waypoint_elindex;
  //           prior.getEl(left_el,left_elindex);
  //           prior.getEl(right_el,right_elindex);
  //           prior.getEl(waypt_el,waypt_elindex);
  //           left_lane=left_el.geometry;
  //           right_lane=right_el.geometry;
  //           waypts=waypt_el.geometry;
  //           validlabel = true;
  //           break;
  //         }
  //       }
  //       if(!validlabel){
  //         cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
  //         return -1;
  //       }
  //       waypts = waypt_el.geometry;

  int retval;
  point2arr_uncertain tmpleft_unc, tmpright_unc, waypts_unc;
     
  retval= prior.getLaneBoundsFull(tmpleft_unc,tmpright_unc,label);
  retval= prior.getLaneCenterLine(waypts_unc,label);

  left_lane.set(tmpleft_unc);
  right_lane.set(tmpright_unc);
  waypts.set(waypts_unc);

  if(retval<0){
    cerr << "in Map::getBounds() passed bad lane label "<<label <<endl;
    return -1;
  }
      


  //--------------------------------------------------
  // reversal
  //--------------------------------------------------
  left_lane.reverse();
  right_lane.reverse();
  waypts.reverse();
  point2arr tmp;
  tmp = left_lane;
  left_lane = right_lane;
  right_lane = tmp;
  

  double thisrange  = range;
  double thisbackrange = backrange;
  if (range<0)
    thisrange = 1000000;
  if (backrange<0)
    thisbackrange = 1000000;
     
  
  double statedist = waypts.project_along(state);

  double neardist = statedist-thisbackrange;

  if (neardist<0)
    neardist = 0;


  waypts.cut_back(waypts.linelength()-neardist);
  waypts.cut_front(thisrange);

  point2 nearpt,farpt;
  nearpt = waypts[0];
  farpt = waypts.back();


  // int cutleftbacknum = 0;
  //int cutrightbacknum = 0;
  // int cutleftfrontnum = 0;
  // int cutrightfrontnum = 0;

  int valback = left_lane.cut_back_at_index(nearpt);
  int valfront= left_lane.cut_front_at_index(farpt);

  right_lane.cut_back_at_index(valback);
  right_lane.cut_front_at_index(valfront);

  // left_lane.cut_back(nearpt);
  //left_lane.cut_front(farpt);

  //right_lane.cut_back(nearpt);
  //right_lane.cut_front(farpt);

  leftbound = left_lane;
  rightbound = right_lane;
  return 0;
} 
 










int Map::getWaypointArr(point2arr& ptarr,const PointLabel &label)
{ 
  int waypt_elindex;
  MapElement waypt_el;
  for (uint i=0;i<prior.lanes.size();++i){
    if(label == prior.lanes[i].label){
      waypt_elindex = prior.lanes[i].waypoint_elindex;
      prior.getEl(waypt_el, waypt_elindex);  
      ptarr.set(waypt_el.geometry);
      return (0);
      break;
    }
  }
  
  cerr << "in Map::getWaypointArr, Failed finding a matching lane " << label <<endl;
  return -1;
}

int Map::getLaneDistToWaypoint(double &dist, const point2 state,const PointLabel &label)
{
  point2 waypt;
  int retval;
  retval = getWaypoint(waypt, label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << label <<endl;
    return retval;
  }
  point2arr ptarr;
  retval = getWaypointArr(ptarr,label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint array from label " << label <<endl;
    return retval;
  }

  

  double d1 = ptarr.project_along(waypt);
  double d2 = ptarr.project_along(state);

  dist = d1-d2;
  
  return 0;
}

int Map::getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label)
{
  point2 waypt;
  int retval;
  retval = getWaypoint(waypt, label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << label <<endl;
    return retval;
  }

  point2 statewaypt;
  retval = getWaypoint(statewaypt, statelabel);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << statelabel <<endl;
    return retval;
  }


  point2arr ptarr;
  retval = getWaypointArr(ptarr,label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint array from label " << label <<endl;
    return retval;
  }

  

  double d1 = ptarr.project_along(waypt);
  double d2 = ptarr.project_along(statewaypt);

  dist = d1-d2;
  
  return 0;
}





int Map::getObstacleGeometry(point2arr& ptarr)
{
  cerr <<" Map::getObstacleGeometry() not yet implemented!" << endl;
  return -1;
}



double Map::getObstacleDist(const point2& state, const int lanedelta)
{
  double mindist= 1000000;
  double thisdist;
  unsigned int i;
  MapElement el;
  int left_index;
  int right_index;
  int left_index2;
  int right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  bool gotlane = false;
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      //      if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){    
        label = prior.lanes[i].label;
        gotlane = true;
        break;
      }
    }
    
  }
  if (!gotlane) {
    cerr << "in Map::getObstacleDist() error, not in any lane at point " << state << endl;
    return -1;
  }
  //  cout << "in Map::getObstacleDist() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      if (left_index2==0 &&right_index2==0){    
        //if (left_index2>left_index &&right_index2>right_index &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
        
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
        }
      }
    }
    
  }
  if (obsid >=0){
    //cout << "in Map::getObstacleDist() found obstacle "
    //     << obsid << " at distance " << mindist 
    //     << " in lane " << label<< endl;
    return mindist;
  }
  //  else{
  //cout << "in Map::getObstacleDist() found no obstacles in lane " << label<< endl;}
  return -1;
}



point2 Map::getObstaclePoint(const point2& state, const double offset)
{
  double mindist= 1000000;
  double thisdist;
  unsigned int i;
  MapElement el;
  int left_index;
  int right_index;
  int left_index2;
  int right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  int laneindex;
  bool gotlane = false;
  point2 outpt;

  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      // if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){     
        label = prior.lanes[i].label;
        laneindex = i;
        gotlane = true;
        break;
      }
    }
    
  }
  if (!gotlane) {
    //cout << "in Map::getObstaclePoint() warning, not in any lane at point " << state<< endl;
    return outpt;
  }

  //  cout << "in Map::getObstaclePoint() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt,obspt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      //      if (left_index2>left_index &&right_index2>right_index &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index2==0 &&right_index2==0){
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
          obspt = cpt;
        }
      }
    }
    
  }
  //if (obsid >=0)
  //cout << "in Map::getObstaclePoint() found obstacle "
  //     << obsid << " at distance " << mindist 
  //    << " in lane " << label<< endl;
  //else
  // cout << "in Map::getObstaclePoint() found no obstacles in lane " << label<< endl;



  if (obsid >=0){
    int waypt_elindex = prior.lanes[laneindex].waypoint_elindex;
    MapElement waypt_el;
    prior.getEl(waypt_el, waypt_elindex);  
    point2arr wayptarr;
    wayptarr.set(waypt_el.geometry);



    double wayptdist = wayptarr.cut_front(obspt);
    if (wayptdist>offset){
      wayptarr.cut_front(wayptdist-offset);
      outpt = wayptarr.back();
    }
    else {
      //cout << "in Map::getObstaclePoint , offset pushes us out of lane" << endl;
      outpt = state;
    }
  }
  return outpt;
}




//  int Map::getRoadBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range )
//     {

//       cerr << "WARNING getRoadBounds not implemented.  Use getBounds to return lane bounds." << endl;
      
//       unsigned int i;
//       MapElement el;
//       //  double left_index;
//       //double right_index;
//       //  double left_index2;
//       // double right_index2;
//       //int left_side;
//       //int right_side;
//       int left_elindex,right_elindex, waypt_elindex;
//       MapElement left_el;
//       MapElement right_el;
//       MapElement waypt_el;
//       point2arr left_lane, right_lane, waypts;

//       bool validlabel = false;
  
//       int lanenum = label.lane;
//       int segnum = label.segment;

//       LaneLabel maxlabel;
//       LaneLabel minlabel;
      
//       int numlanes = 0;
//       int segindex = i;
//       int laneindex = lanenum;

//       for (i=0;i<prior.segments.size();++i){
//         if (prior.segments[i].label == label.segment){
//           segindex = i;
//           numlanes = prior.segments[i].lane_label.size();
//         }

//       }

//       double thisang = 0;
//       if (getHeading(thisang,label)!=0)
//         cerr << "in getRoadBounds - cant find this label heading" << endl;

//       double tmpang = 0;
//       double dang;
//       vector<bool> sameheading; 
//       LaneLabel tmplanelabel;
//       vector<LaneLabel>

//       for (i=0;i<numlanes;++i){
//         tmplanelabel = prior.segment[segindex].lane_label[i];
//         getHeading(tmpang, tmplanelabel);
//         dang = tmpang-thisang;
//         if (fabs(dang)>(M_PI/2) && fabs(dang)<(3*M_PI/2)){
//           sameheading.push_back(false);
//         }
//         else{
//           sameheading.push_back(true);
//         }
//       }

//       for (i=0;i<numlanes;++i)
//         tmplanelabel = prior.segment[segindex].lane_label[i];
//       if (sameheading[i]){
//         val = getBounds(tmpleftptarr,tmprightptarr,tmplanelabel,state,range);
//       }

   
//       int val;
//       double minang, maxang, thisang;
//       if (getHeading(minang,minlabel)!=0)
//         cerr << "in getRoadBounds - cant find minlabel heading" << endl;
//       if (getHeading(maxang,maxlabel)!=0)
//         cerr << "in getRoadBounds - cant find maxlabel heading" << endl;
      
      
//       if (

//         for (i=0;i<prior.lanes.size(); ++i){
         
//           if (prior.lanes[i].label == templabel){ 
            
//           }

//         }

//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label == label){
//           left_elindex = prior.lanes[i].leftbound_elindex;
//           right_elindex = prior.lanes[i].rightbound_elindex;
//           waypt_elindex = prior.lanes[i].waypoint_elindex;
//           prior.getEl(left_elindex,left_el);
//           prior.getEl(right_elindex,right_el);
//           prior.getEl(waypt_elindex,waypt_el);
//           left_lane=left_el.geometry;
//           right_lane=right_el.geometry;
//           waypts=waypt_el.geometry;
//           validlabel = true;
//           break;
//         }
//       }
//       if(!validlabel){
//         cout << "in Map::getRoadBounds() passed bad lane label "<<label <<endl;
//         return -1;
//       }
  


//       double BACK_RANGE = 5;
//       waypts = waypt_el.geometry;
//       double statedist = waypts.project_along(state);

//       double neardist = statedist-BACK_RANGE;

//       if (neardist<0)
//         neardist = 0;


//       waypts.cut_back(waypts.linelength()-neardist);
//       waypts.cut_front(range);

//       point2 nearpt,farpt;
//       nearpt = waypts[0];
//       farpt = waypts.back();
//       left_lane.cut_back(nearpt);
//       left_lane.cut_front(farpt);

//       right_lane.cut_back(nearpt);
//       right_lane.cut_front(farpt);

//       leftbound = left_lane;
//       rightbound = right_lane;
//      return -1;
//    } 




 

//  int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel)
//     {
//       unsigned int i,j;
//       int exit_elindex =-1;
//       int exit_leftelindex =-1;
//       int exit_rightelindex =-1;
//       int exit_ptindex =-1;
//       int enter_elindex =-1;
//       int enter_leftelindex =-1;
//       int enter_rightelindex =-1;

//       int enter_ptindex =-1;
  
//       point2 exitpt;
//       point2 enterpt;
//       MapElement el;

//       bool isvalid = false;

//       LaneLabel tmplane(enterlabel.segment,enterlabel.lane);
//       if ((exitlabel.segment==enterlabel.segment) && (exitlabel.lane==enterlabel.lane)){
//         getLeftBound(leftbound, tmplane);
//         int retval = getRightBound(rightbound, tmplane);
//         cout << "returning current lane transition" << endl;
//         return retval;
//       }

//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label== exitlabel){
    
//           for (j=0;j<prior.lanes[i].exit_label.size();++j){
//             if (prior.lanes[i].exit_label[j]==exitlabel &&
//                 prior.lanes[i].exit_link[j]==enterlabel){
        

//               isvalid = true;
//             }
//           }
//           if (!isvalid)
//             continue;
      
//           for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
//             if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
//             exit_leftelindex = prior.lanes[i].leftbound_elindex;
//             exit_rightelindex = prior.lanes[i].rightbound_elindex;
//             exit_ptindex =j;
//             break;
//             }
//           }
//         }
//       }
//       if (isvalid){
//         for (i=0;i<prior.lanes.size(); ++i){
//           if (prior.lanes[i].label== enterlabel){
    
//             for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
//               if (prior.lanes[i].waypoint_label[j]==enterlabel){
//                 enter_elindex = prior.lanes[i].waypoint_elindex;
//                 enter_leftelindex = prior.lanes[i].leftbound_elindex;
//                 enter_rightelindex = prior.lanes[i].rightbound_elindex;
//                 enter_ptindex =j;
//                 break;
//               }
//             }
//           }
//         }
//         prior.getEl(enter_leftel,elindex);
//         enterpt = el.geometry[enter_ptindex];  
//         prior.getEl(exit_leftel,elindex);
//         exitpt = el.geometry[exit_ptindex];  

//         leftbound.clear();
//         leftbound.push_back(enterpt);
//         leftbound.push_back(exitpt);

//         prior.getEl(enter_rightel,elindex);
//         enterpt = el.geometry[enter_ptindex];  
//         prior.getEl(exit_rightel,elindex);
//         exitpt = el.geometry[exit_ptindex];  

//         rightbound.clear();
//         rightbound.push_back(enterpt);
//         rightbound.push_back(exitpt);
  
//         return 0;
//       }


//       cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
//       return -1;
//     }

