/**********************************************************
 **
 **  MAP.CC
 **
 **    Time-stamp: <2007-06-07 09:53:40 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:45 2007
 **
 **
 **********************************************************
 **
 ** Map Class  
 **
 **********************************************************/

#include "Map.hh"

using namespace std;


Map::Map()
{}

Map::~Map()
{}

 /**
   * Load an RNDF file into the prior data structure.
   */
bool Map::loadRNDF(string filename) 
{
  bool retval = prior.loadRNDF(filename);
  if (!retval){
    cerr << "in Map::loadRNDF() error loading " << filename << endl;
  }
  
  return retval;

  
}

  /**
   * Adds an element to the map. If the id exists then that element is
   * updated.  If the element has type ELEMENT_CLEAR then the element
   * with that ID (if it exists) is removed
   */

int Map::addEl(const MapElement &el)
{
  MapId thisid = el.id;
	MapId tmpid;

	for (unsigned int i = 0; i < data.size(); ++i){
		tmpid = data[i].id;
		if (tmpid == thisid){
      if (el.type==ELEMENT_CLEAR){
        data.erase(data.begin()+i);
      }else{
        data[i] = el;
      }
			return 0;
		}
	}
  if(el.type != ELEMENT_CLEAR)
    data.push_back(el);
	return 0;
}


 int Map::getLane(LaneLabel & lanelabel, const point2& pt)
{
  point2arr centerline;
  unsigned int sze = prior.lanes.size();
  int retval;
  int index = -1;
  double dist=0; 
    double mindist = -1;
  LaneLabel thislabel,minlabel;
  point2 dpt,tmppt;
  for (unsigned int i = 0; i<sze; ++i){
    thislabel = prior.lanes[i].label;
    retval = getLaneCenterLine(centerline,thislabel);
    if (retval<0)
      continue;
    tmppt = centerline.project(pt);
    dpt = pt-tmppt;
    dist = dpt.norm();
    if (mindist<0){
      mindist = dist;
      minlabel = thislabel;
      index = i;
    }else if (dist<mindist){
      minlabel = thislabel;
       mindist=dist;
      index = i;
    }
    
  }
  lanelabel = minlabel;
  return index;
}

int Map::getStopLineSensed(point2_uncertain &pt, 
                           const PointLabel &ptlabelin)
{

  double alongthresh = 2;
  double edgethresh = 4;
  point2_uncertain priorpt;

  point2 stoppt,thispt,projectpt;
  point2arr midline,leftline,rightline;
  int i,numels,retval;
  
  //  getAllStopLines(labelarr,ptarr);
  if (!isStopLine(ptlabelin)){
    cout <<"in Map::getStopLineSensed point label given " << ptlabelin << " is not a stopline" << endl;
    return -1;
  }
  retval = getWayPoint(priorpt,ptlabelin);
  numels = (int)data.size();

  //--------------------------------------------------
  // get the lane for the desired stopline
  //--------------------------------------------------
  LaneLabel lanelabel(ptlabelin.segment,ptlabelin.lane);
  getLaneCenterLine(midline,lanelabel);
  getLaneBounds(leftline,rightline,lanelabel);

  midline.set_extend(2*alongthresh);
  midline.set_extend(-2*alongthresh);

  leftline.set_extend(2*alongthresh);
  leftline.set_extend(-2*alongthresh);

  rightline.set_extend(2*alongthresh);
  rightline.set_extend(-2*alongthresh);

  

  int minindex = -1;
  double leftdist,rightdist;
  point2arr stopline;

  double distalong = alongthresh+1;
  for (i=0;i<numels;++i){
    if (data[i].type!=ELEMENT_STOPLINE)
      continue;

    stopline.set(data[i].geometry);
    
    stoppt.set(data[i].center);
    thispt.set(priorpt);

    projectpt = midline.project(stoppt);
    //distnormal = projectpt.dist(stoppt);
    //if (distnormal > normalthresh){
    // continue;
    //}


    leftdist = leftline.get_min_dist_line(stopline);
    rightdist = rightline.get_min_dist_line(stopline);


    if (leftdist>edgethresh || rightdist>edgethresh)
      continue;



    

    if(thispt.dist(projectpt)<distalong){
      distalong = thispt.dist(projectpt);
      minindex = i;
    }



    
   
    }
  
  if (minindex>=0){
    pt = projectpt;//data[minindex].center;
    return 0;
  }
  
  pt = priorpt;
  return 1;
  
}

int Map::getIndexAlongLine(int &index, const point2arr & line, const double dist)
{
  cout <<"getIndexAlongLine not implemented yet" << endl;
  index =0;
  return 0;
}   

int Map::getPointAlongLine(point2 &ptout, const point2arr & line, const double dist)
{
  ptout = line.get_point_along(dist);
  return 0;
}   

int Map::getDistAlongLine(double &dist, const point2arr & line, const point2& pt)
{
  dist = line.project_along(pt);
  return 0;
}

int Map::getDistAlongLine(double &dist,const point2arr & line, const point2& pta, const point2& ptb) 
{
  dist = line.project_along(pta)-line.project_along(ptb);
  return 0;
}

int Map::getProjectToLine(point2 &projpt, const point2arr & line, const point2& pt)
{
  projpt = line.project(pt);
  return 0;
}

int Map::extendLine(point2arr &lineout, const point2arr & line, double dist)
{
  int sze = line.size();
  if (sze<2)
    return -1;
  
  lineout.set(line);
  point2 pta,ptb, dpt,newpt;
  double thisdist;
  if (dist<0){
    pta = line[0];
    ptb = line[1];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta+dpt*dist/thisdist;
    lineout.insert(0,newpt);
  }else{
    pta = line[sze-1];
    ptb = line[sze-2];
    dpt = ptb-pta;
    thisdist = dpt.norm();
    newpt = pta-dpt*dist/thisdist;
    lineout.push_back(newpt);
   }
  return 0;
 }



int Map::getObsNearby(vector<MapElement> &elarr,const point2 &pt, const double dist)
{
  MapElement thisEl;
  double thisdist;
  unsigned int sze = data.size();
  elarr.clear();
  point2 tmppt,dpt;
  point2arr ptarr;  
  for (unsigned int i=0; i<sze; ++i){
    thisEl = data[i];
    if (!thisEl.isObstacle()){
      continue;
    }
    if (thisEl.isOverlap(pt)){
      elarr.push_back(thisEl);
      continue;
    }
    ptarr.set(thisEl.geometry);    
    dpt = pt-ptarr.project(pt);
    thisdist = dpt.norm();
    if (thisdist<dist){
      elarr.push_back(thisEl);
    }
  }
  return elarr.size();
}

int Map::getObsInLane(vector<MapElement> &elarr,const LaneLabel &lanelabelin)
{
  MapElement thisEl;
  unsigned int sze = data.size();
  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  int retval = getLaneBoundsPoly(bounds,lanelabelin);
  if (retval<0)
    return retval;

  for (unsigned int i=0; i<sze; ++i){
    thisEl = data[i];
    if (!thisEl.isObstacle()){
      continue;
    }
    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }
  return elarr.size(); 
}


int Map::getObsInBounds(vector<MapElement> &elarr, const point2arr &leftbound, const point2arr &rightbound)
{
  MapElement thisEl;

  unsigned int sze = data.size();
  elarr.clear();
  point2 tmppt,dpt;
  point2arr bounds;
  point2arr tmpbound;
  tmpbound = rightbound;
  tmpbound.reverse();
  bounds = leftbound;
  bounds.connect(tmpbound);

  for (unsigned int i=0; i<sze; ++i){
    thisEl = data[i];
    if (thisEl.isOverlap(bounds)){
      elarr.push_back(thisEl);
      continue;
    }
  }
  return elarr.size(); 

}

int Map::getEl(MapElement &el, const MapId &id)
{
  MapElement thisEl;
  unsigned int sze = data.size();
  
  for (unsigned int i=0; i<sze; ++i){
    thisEl = data[i];
    
    if (thisEl.id==id){
      el =thisEl;
      return 0;
    }
  }
    return -1;
    
 
}

/** 
 *   Checks if the given point lies geometrically within a lane
 *   specified by segment and lane id.  Returns true or false.
 */
bool Map::checkLaneID(const point2 pt, const LaneLabel &label)
{  return checkLaneID(pt, label.segment, label.lane);}

bool Map::checkLaneID(const point2 pt, const int segment, const int lane)
{
  LaneLabel labelin(segment,lane);
  point2_uncertain ptunc(pt);
  return isPointInLane(ptunc,labelin);

}
 












int Map::getSegmentID(const point2 pt)
{
  PointLabel label;
  int retval = getNextPointID(label,pt);
  if (retval<0){
  cout << "in Map::getSegmentID : no matches found for point " << pt << endl;
  return -1;
  }
  
  return label.segment;

}

int Map::getLaneID(const point2 pt)
{

 PointLabel label;
  int retval = getNextPointID(label,pt);
  if (retval<0){
  cout << "in Map::getLaneID : no matches found for point " << pt << endl;
  return -1;
  }
  
  return label.lane;

}

int Map::getClosestPointID(PointLabel &label, const point2 &pt)
{
  unsigned int i,j;
  MapElement el;
  int elindex =-1;
  point2 thispt,minpt;
  PointLabel minlabel;
  double mindist = -1;
  double thisdist = 0;
  for (i=0;i<prior.lanes.size(); ++i){
    elindex = prior.lanes[i].waypoint_elindex;
    prior.getEl(el,elindex);
    for (j=0;j<el.geometry.size();++j){
      thispt = el.geometry[j];
      thisdist = pt.dist(thispt);
      if ((thisdist < mindist) || mindist<0){
        minlabel = prior.lanes[i].waypoint_label[j];
        minpt = thispt;
        mindist = thisdist;
      }
    }
  }
  if (mindist<0){
    cout << "in Map::getClosestPointID no point found " << endl;
    return -1;
  }

  label = minlabel;
  return 0;

}

        
int Map::getNextPointID(PointLabel &label,const point2& pt)
{
  unsigned int i;
  int left_index;
  int right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  int ptindex;
  double distalong;
  

  for (i=0;i<prior.lanes.size();++i){
   
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);

    if (left_side!=right_side){
      //  cout << "LANE NUMBER " << i << " label =  " << prior.lanes[i].label << endl;
      // cout << "left_side = " << left_side << " right_side = " << right_side << endl;
      // cout << "left_index = " << left_index << " right_index = " << right_index << endl;
      // cout << "left_size = " << left_lane.size() << " right_size = " << right_lane.size() << endl;

      //  if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      //cout << "leftindex = " << left_index 
      //     << " rightindex = " << right_index << endl;
if (left_index==0 &&right_index==0){
    

       //--------------------------------------------------
       // should use center lane
       //--------------------------------------------------
       distalong =left_lane.project_along(pt);
       ptindex = left_lane.get_index_next(distalong);
       //       if (left_index>right_index)
       //   ptindex = (int)ceil(left_index);
       // else
       //  ptindex = (int)ceil(right_index);
      
        label = prior.lanes[i].waypoint_label[ptindex];
        return 0;
      }
    }
    
  }
  cout << "in Map::getNextPointID no point found " << endl;
  return -1;
}

int Map::getLastPointID(PointLabel &label, const point2& pt)
{
  unsigned int i;
  int left_index;
  int right_index;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  //  bool found=false;
  int ptindex;
  double distalong ;
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(pt,left_side,left_index);
    right_lane.get_side(pt,right_side,right_index);
    if (left_side!=right_side){
      //      if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
    if (left_index==0 &&right_index==0){
 //--------------------------------------------------
       // should use center lane
       //--------------------------------------------------
       distalong =left_lane.project_along(pt);
       ptindex = left_lane.get_index_next(distalong)-1;
       //   if (left_index>right_index)
       //   ptindex = (int)floor(left_index);
       // else
       //  ptindex = (int)floor(right_index);
      
          label = prior.lanes[i].waypoint_label[ptindex];
        return 0;
      }
    }
    
  }
  cout << "in Map::getLastPointID no point found " << endl;
  return -1;
}


int Map::getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

    unsigned int i;
      MapElement el;
      //  double left_index;
      //double right_index;
      //  double left_index2;
      // double right_index2;
      //int left_side;
      //int right_side;
      int waypt_elindex;
      MapElement left_el;
      MapElement right_el;
      MapElement waypt_el;
      point2arr left_lane, right_lane, waypts;

      bool validlabel = false;
  
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label == label){
          waypt_elindex = prior.lanes[i].waypoint_elindex;
          prior.getEl(waypt_el,waypt_elindex);
          waypts=waypt_el.geometry;
          validlabel = true;
          break;
        }
      }
      if(!validlabel){
        cout << "in Map::getLaneCenterPoint() passed bad lane label "<<label <<endl;
        return -1;
      }
  


      waypts = waypt_el.geometry;
      double statedist = waypts.project_along(pt);

      double newdist = statedist+offset;

      waypts.cut_front(newdist);//waypts.linelength()-neardist);
      cpt = waypts.back();

      return 0;

}



int Map::getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

    unsigned int i;
      MapElement el;
      int right_elindex;
      MapElement right_el;
      point2arr rightpts;

      bool validlabel = false;
  
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label == label){
          right_elindex = prior.lanes[i].rightbound_elindex;
          prior.getEl(right_el,right_elindex);
          validlabel = true;
          break;
        }
      }
      if(!validlabel){
        cout << "in Map::getLaneRightPoint() passed bad lane label "<<label <<endl;
        return -1;
      }
  
      rightpts = right_el.geometry;
      double statedist = rightpts.project_along(pt);

      double newdist = statedist+offset;

      rightpts.cut_front(newdist);
      cpt = rightpts.back();

      return 0;
}



int Map::getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset)
{

    unsigned int i;
      MapElement el;
      int left_elindex;
      MapElement left_el;
      point2arr leftpts;

      bool validlabel = false;
  
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label == label){
          left_elindex = prior.lanes[i].leftbound_elindex;
          prior.getEl(left_el,left_elindex);
          validlabel = true;
          break;
        }
      }
      if(!validlabel){
        cout << "in Map::getLaneLeftPoint() passed bad lane label "<<label <<endl;
        return -1;
      }
  
      leftpts = left_el.geometry;
      double statedist = leftpts.project_along(pt);

      double newdist = statedist+offset;

      leftpts.cut_front(newdist);
      cpt = leftpts.back();

      return 0;
}





int Map::getLeftBound(point2arr& ptarr, const LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].leftbound_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
    }
  }
  return 0;
}

int Map::getRightBound(point2arr& ptarr, const LaneLabel &label )
{
  unsigned int i;
  MapElement el;
  int elindex =-1;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].rightbound_elindex;
      prior.getElFull(el,elindex);
      ptarr.set(el.geometry);
    }
  }
  return 0;
}


int Map::getLeftBoundType(string& type, const LaneLabel &label)
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label==label){
      type = prior.lanes[i].leftbound_type;
      return 0;
    }
  }
  return -1;
}
int Map::getRightBoundType(string& type, const LaneLabel &label )
{
  unsigned int i;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      type = prior.lanes[i].rightbound_type;
      return 0;
    }
  }
  return -1;

}


bool Map::isExit(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //bool isexit = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].exit_label.size();++j){
        if (prior.lanes[i].exit_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

bool Map::isStop(PointLabel &label){
  unsigned int i,j;
  //  int elindex =-1;
  //  int ptindex =-1;
  //  bool isstop = false;
  //MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      //elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          return true;
        }
      }
    }
  }
  return false;
}

int Map::getStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  bool isstop = false;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j]==label){
          isstop = true;
        }
      }
      if (!isstop)
        continue;

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}
int Map::getStopline(point2& pt, point2 state)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel label, stoplabel;
  
  int retval = getNextPointID(label,state);
  if (retval){
    return retval;
  }
  
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getStopline, stop line not found" << label << endl;
  
  return -1;
}

int Map::getNextStopline(point2& pt, PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  //  bool isstop = false;
  MapElement el;

  PointLabel stoplabel;
  

  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].stop_label.size();++j){
        if (prior.lanes[i].stop_label[j].point>=label.point){
          stoplabel = prior.lanes[i].stop_label[j];
          break;
        }
      }

      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==stoplabel){
          
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];
          label = stoplabel;
          return 0;
        }
      }
    }
  }
  cerr << "in Map::getNextStopline, stop line not found" << label << endl;
  
  return -1;
}



int Map::getWaypoint(point2& pt, const PointLabel &label)
{
  unsigned int i,j;
  int elindex =-1;
  int ptindex =-1;
  MapElement el;
  for (i=0;i<prior.lanes.size(); ++i){
    if (prior.lanes[i].label == label){
      elindex = prior.lanes[i].waypoint_elindex;
      for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
        if (prior.lanes[i].waypoint_label[j]==label){
          ptindex =j;
          prior.getEl(el,elindex);
          pt = el.geometry[ptindex];

          return 0;
        }
      }
    }
  }
  cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
  return -1;

}

int Map::getHeading(double & ang,const PointLabel &label)
{
  int retval;
  point2 pt;
  retval = getWayPoint(pt,label);
  if (retval<0){
    cerr << "in Map::getHeading, point label " << label << " not found" << endl;
  }
  
  return getHeading(ang,pt);

}
 int Map::getHeading(double & ang,const point2 &pt)
{

  double delta = .5;
  point2arr centerline;

  LaneLabel lanelabel;
  
  getLane(lanelabel,pt);

  getLaneCenterLine(centerline,lanelabel);


  point2 projpt,frontpt,backpt;

  projpt = centerline.project(pt);

  double dist,newdist;

  getDistAlongLine(dist,centerline,projpt);

  point2 newpt,dpt;

  getPointAlongLine(newpt,centerline,dist+delta);
  getDistAlongLine(newdist,centerline,newpt);

  if (newdist-dist < delta/2) {
    getPointAlongLine(newpt,centerline,dist-delta);
    frontpt = projpt;
    backpt = newpt;
  }else{
    frontpt = newpt;
    backpt = projpt;
  }    

  dpt = frontpt-backpt;

  ang = dpt.heading();
  return 0;
}

//   dpt = newpt-projpt;
//   if (dpt.norm()<delta/4)
  
  


//   getPointAlongLine(newpt,centerline,dist+delta);
  
  

//   PointLabel nextlabel,lastlabel;
  
//   int nextval = getNextPointID(nextlabel,pt);
//   int lastval = getLastPointID(lastlabel,pt);

//   //cout << "nextval = " << nextval
//   //    << " lastval = "<< lastval
//   //     <<endl;
//   if (nextval<0 ||lastval <0){
//     cerr << "in Map::getHeading, couldn't find surrounding waypts" << endl;
//     return -1;
//   }

//   point2 nextpt,lastpt,dpt;

//   nextval = getWaypoint(nextpt,nextlabel);
//   lastval = getWaypoint(lastpt,lastlabel);

//   if (nextval<0 ||lastval <0){
//     cerr << "in Map::getHeading, bad getWaypt call" << endl;
//     return -1;
//   }
  
//   dpt = nextpt-lastpt;
  
//   double tmpang = dpt.heading();
//   ang = tmpang;
//   return 0;
// }


  int Map::getBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range, const double backrange )
    {

      
      //unsigned int i;
      //MapElement el;
      //  double left_index;
      //double right_index;
      //  double left_index2;
      // double right_index2;
      //int left_side;
      //int right_side;
      //int left_elindex,right_elindex, waypt_elindex;
      //MapElement left_el;
      //MapElement right_el;
      //MapElement waypt_el;
      point2arr left_lane, right_lane, waypts;

//       bool validlabel = false;
  
//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label == label){
//           left_elindex = prior.lanes[i].leftbound_elindex;
//           right_elindex = prior.lanes[i].rightbound_elindex;
//           waypt_elindex = prior.lanes[i].waypoint_elindex;
//           prior.getEl(left_el,left_elindex);
//           prior.getEl(right_el,right_elindex);
//           prior.getEl(waypt_el,waypt_elindex);
//           left_lane=left_el.geometry;
//           right_lane=right_el.geometry;
//           waypts=waypt_el.geometry;
//           validlabel = true;
//           break;
//         }
//       }
//       if(!validlabel){
//         cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
//         return -1;
//       }
      int retval;
      point2arr_uncertain tmpleft_unc, tmpright_unc, waypts_unc;
     
      retval= prior.getLaneBoundsFull(tmpleft_unc,tmpright_unc,label);
      retval= prior.getLaneCenterLineFull(waypts_unc,label);

      left_lane.set(tmpleft_unc);
      right_lane.set(tmpright_unc);
      waypts.set(waypts_unc);

      if(retval<0){
        cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
        return -1;
      }
      
      
      double thisrange  = range;
      double thisbackrange = backrange;
      if (range<0)
        thisrange = 1000000;
      if (backrange<0)
        thisbackrange = 1000000;

     
    
 int cutleftbacknum = 0;
      int cutrightbacknum = 0;
      int cutleftfrontnum = 0;
      int cutrightfrontnum = 0;
      
      // left_lane.cut_front_at_index(10);
      //  right_lane.cut_front_at_index(10);


      //  left_lane.cut_back_at_index(10);
      // right_lane.cut_back_at_index(10);
     
     
      cutleftbacknum = left_lane.cut_back_at_index(state,thisbackrange);
      cutrightbacknum = right_lane.cut_back_at_index(cutleftbacknum);
     
   
  
      cutleftfrontnum = left_lane.cut_front_at_index(state,thisrange);
      cutrightfrontnum =right_lane.cut_front_at_index(cutleftfrontnum);

       
//         left_lane.cut_back_at_index(nextindex);
//         left_lane.cut_front_at_index(lastindex);

//       right_lane.cut_back_at_index(nextindex);
//       right_lane.cut_front_at_index(lastindex);
      

      //      right_lane.cut_back(nearpt);
      //      right_lane.cut_front(farpt);

      leftbound = left_lane;
      rightbound = right_lane;
      return 0;
    } 



 
  int Map::getBoundsReverse(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range, const double backrange )
    {
  //     unsigned int i;
//       MapElement el;
//       //  double left_index;
//       //double right_index;
//       //  double left_index2;
//       // double right_index2;
//       //int left_side;
//       //int right_side;
//       int left_elindex,right_elindex, waypt_elindex;
//       MapElement left_el;
//       MapElement right_el;
//       MapElement waypt_el;
      point2arr left_lane, right_lane, waypts;

//       bool validlabel = false;
  
//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label == label){
//           left_elindex = prior.lanes[i].leftbound_elindex;
//           right_elindex = prior.lanes[i].rightbound_elindex;
//           waypt_elindex = prior.lanes[i].waypoint_elindex;
//           prior.getEl(left_el,left_elindex);
//           prior.getEl(right_el,right_elindex);
//           prior.getEl(waypt_el,waypt_elindex);
//           left_lane=left_el.geometry;
//           right_lane=right_el.geometry;
//           waypts=waypt_el.geometry;
//           validlabel = true;
//           break;
//         }
//       }
//       if(!validlabel){
//         cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
//         return -1;
//       }
//       waypts = waypt_el.geometry;

  int retval;
      point2arr_uncertain tmpleft_unc, tmpright_unc, waypts_unc;
     
      retval= prior.getLaneBoundsFull(tmpleft_unc,tmpright_unc,label);
      retval= prior.getLaneCenterLine(waypts_unc,label);

      left_lane.set(tmpleft_unc);
      right_lane.set(tmpright_unc);
      waypts.set(waypts_unc);

      if(retval<0){
        cout << "in Map::getBounds() passed bad lane label "<<label <<endl;
        return -1;
      }
      


      //--------------------------------------------------
      // reversal
      //--------------------------------------------------
      left_lane.reverse();
      right_lane.reverse();
      waypts.reverse();
      point2arr tmp;
      tmp = left_lane;
      left_lane = right_lane;
      right_lane = tmp;
  

  double thisrange  = range;
      double thisbackrange = backrange;
      if (range<0)
        thisrange = 1000000;
      if (backrange<0)
        thisbackrange = 1000000;
     
  
      double statedist = waypts.project_along(state);

      double neardist = statedist-thisbackrange;

      if (neardist<0)
        neardist = 0;


      waypts.cut_back(waypts.linelength()-neardist);
      waypts.cut_front(thisrange);

      point2 nearpt,farpt;
      nearpt = waypts[0];
      farpt = waypts.back();


      // int cutleftbacknum = 0;
      //int cutrightbacknum = 0;
      // int cutleftfrontnum = 0;
      // int cutrightfrontnum = 0;

      int valback = left_lane.cut_back_at_index(nearpt);
      int valfront= left_lane.cut_front_at_index(farpt);

      right_lane.cut_back_at_index(valback);
      right_lane.cut_front_at_index(valfront);

      // left_lane.cut_back(nearpt);
      //left_lane.cut_front(farpt);

      //right_lane.cut_back(nearpt);
      //right_lane.cut_front(farpt);

      leftbound = left_lane;
      rightbound = right_lane;
      return 0;
    } 
 

  int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel, const point2 state, const double range, const double backrange)
    {
      unsigned int i,j;
      int exit_elindex =-1;
      int exit_leftelindex =-1;
      int exit_rightelindex =-1;
      int exit_ptindex =-1;
      int enter_elindex =-1;
      int enter_leftelindex =-1;
      int enter_rightelindex =-1;

      int enter_ptindex =-1;
  
      point2 exitpt;
      point2 enterpt;
      
      bool samelaneflag = false;
  
      bool isvalid = false;
      

      LaneLabel exit_lanelabel, enter_lanelabel;

      exit_lanelabel.segment = exitlabel.segment;
      exit_lanelabel.lane = exitlabel.lane;

      enter_lanelabel.segment = enterlabel.segment;
      enter_lanelabel.lane = enterlabel.lane;

      PointLabel ptlabel(exitlabel);
      ptlabel.point = ptlabel.point+1;
      
      if (exit_lanelabel == enter_lanelabel){
        samelaneflag = true;
      }
      //      cout << "TRANSITION FROM " << exitlabel << " TO " <<ptlabel << endl;
      if (enterlabel == ptlabel){
        int retval = getBounds(leftbound, rightbound, enter_lanelabel,state, range,backrange);
        cout << "returning current lane transition" << endl;
        return retval;
      }

      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== exitlabel){
    
          for (j=0;j<prior.lanes[i].exit_label.size();++j){
            if (prior.lanes[i].exit_label[j]==exitlabel &&
                prior.lanes[i].exit_link[j]==enterlabel){
          

              isvalid = true;
            }
          }
          if (!isvalid)
            continue;
      
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
            exit_leftelindex = prior.lanes[i].leftbound_elindex;
            exit_rightelindex = prior.lanes[i].rightbound_elindex;
            exit_ptindex =j;
        
            break;
            }
          }
        }
      }
      if (!isvalid){
        cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
        return -1;

      }
      isvalid = false;
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== enterlabel){
    
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==enterlabel){
              enter_elindex = prior.lanes[i].waypoint_elindex;
              enter_leftelindex = prior.lanes[i].leftbound_elindex;
              enter_rightelindex = prior.lanes[i].rightbound_elindex;
              enter_ptindex =j;
              isvalid = true;
              break;
            }
          }
        }
      }

      if (!isvalid){
        cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
        return -1;
      }

      MapElement enter_leftel, enter_rightel;
      MapElement exit_leftel, exit_rightel;
      point2arr enter_leftarr, enter_rightarr;
      point2arr exit_leftarr, exit_rightarr;
      point2 enterpt_left, enterpt_right;
      point2 exitpt_left, exitpt_right;


      prior.getEl(enter_leftel,enter_leftelindex);
      enter_leftarr.set(enter_leftel.geometry);

      prior.getEl(enter_rightel,enter_rightelindex);
      enter_rightarr.set(enter_rightel.geometry);

      prior.getEl(exit_leftel,exit_leftelindex);
      exit_leftarr.set(exit_leftel.geometry);

      prior.getEl(exit_rightel,exit_rightelindex);
      exit_rightarr.set(exit_rightel.geometry);


      
      exitpt_left = exit_leftarr[exit_ptindex];  
      exitpt_right = exit_rightarr[exit_ptindex];  

      enterpt_left = enter_leftarr[enter_ptindex];  
      enterpt_right = enter_rightarr[enter_ptindex];  
  

      prior.getElFull(enter_leftel,enter_leftelindex);
      enter_leftarr.set(enter_leftel.geometry);

      prior.getElFull(enter_rightel,enter_rightelindex);
      enter_rightarr.set(enter_rightel.geometry);

      prior.getElFull(exit_leftel,exit_leftelindex);
      exit_leftarr.set(exit_leftel.geometry);

      prior.getElFull(exit_rightel,exit_rightelindex);
      exit_rightarr.set(exit_rightel.geometry);
 

      point2arr new_leftarr;
      point2arr new_rightarr;


      point2arr tmpexleft(exit_leftarr);
      point2arr tmpexright(exit_rightarr);
      point2arr tmpenleft(enter_leftarr);
      point2arr tmpenright(enter_rightarr);


      
      exit_leftarr.cut_front(exitpt_left);
      exit_rightarr.cut_front(exitpt_right);
     
      samelaneflag =false;

      if (samelaneflag){
      exit_leftarr.cut_back(enterpt_left);
      exit_rightarr.cut_back(enterpt_right);
      }else{
      exit_leftarr.cut_back_at_index(10);
      exit_rightarr.cut_back_at_index(10);

      }


      enter_leftarr.cut_back(enterpt_left);
      enter_rightarr.cut_back(enterpt_right);
      
      if (samelaneflag){
        enter_leftarr.cut_front(exitpt_left);
        enter_rightarr.cut_front(exitpt_right);
      }else{
        enter_leftarr.cut_front_at_index(10);
        enter_rightarr.cut_front_at_index(10);

      }

      new_leftarr.set(exit_leftarr);
      new_rightarr.set(exit_rightarr);

      double distleft = exitpt_left.dist(enterpt_left);
      double distright = exitpt_right.dist(enterpt_right);
      point2 midpt;


      //--------------------------------------------------
      // merging lane lines
      //--------------------------------------------------
      double distlr = fabs(distleft-distright);
      double estang = 2*atan2(distlr/2,exitpt_left.dist(exitpt_right));
 cout << "est ang = " << estang << endl;
      
      if (estang < .1){
        cout << "GOING STRAIGHT" << endl;
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
        
      }else{
      if (distleft<distright){
        if (enter_rightarr.size()>0){
          midpt = (new_leftarr.back()+enter_leftarr[0])/2;
          new_leftarr.push_back(midpt);
          new_leftarr.connect(enter_leftarr);
          new_rightarr.connect_intersect(enter_rightarr);
        }
      }else{
        if (enter_leftarr.size()>0){
          midpt = (new_rightarr.back()+enter_rightarr[0])/2;
          new_rightarr.push_back(midpt);
          new_leftarr.connect_intersect(enter_leftarr);
          new_rightarr.connect(enter_rightarr);
        }
      }
      }
      //  waypts = waypt_el.geometry;
      //  if (left_neardist<0)
      //     left_neardist = 0;

      // if (right_neardist<0)
      //     right_neardist = 0;
      //      double BACK_RANGE = 5;  

      
      double thisrange  = range;
      double thisbackrange = backrange;
      if (range<0)
        thisrange = 1000000;
      if (backrange<0)
        thisbackrange = 1000000;
      

      int cutleftbacknum = 0;
      int cutrightbacknum = 0;
      int cutleftfrontnum = 0;
      int cutrightfrontnum = 0;
      

     
     
      cutleftbacknum = new_leftarr.cut_back_at_index(state,thisbackrange);
      cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);
     
   
  
      cutleftfrontnum = new_leftarr.cut_front_at_index(state,thisrange);
      cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
   
      //       if (distleft>distright){
      //     new_leftarr.cut_front(state,thisrange);
      // new_rightarr.cut_front(new_leftarr.back());
      // }else{
      //  new_rightarr.cut_front(state,thisrange);
      //  new_leftarr.cut_front(new_rightarr.back());
      // }

      
      leftbound.set(new_leftarr);
      rightbound.set(new_rightarr);
      return 0; 

 
    }





  int Map::getShortTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel)
    {
      unsigned int i,j;
      int exit_elindex =-1;
      int exit_leftelindex =-1;
      int exit_rightelindex =-1;
      int exit_ptindex =-1;
      int enter_elindex =-1;
      int enter_leftelindex =-1;
      int enter_rightelindex =-1;

      int enter_ptindex =-1;
  
      point2 exitpt;
      point2 enterpt;
      
      bool samelaneflag = false;
  
      bool isvalid = false;
      
      point2arr new_leftarr;
      point2arr new_rightarr;

      MapElement enter_leftel, enter_rightel;
      MapElement exit_leftel, exit_rightel;
      point2arr enter_leftarr, enter_rightarr;
      point2arr exit_leftarr, exit_rightarr;
      point2 enterpt_left, enterpt_right;
      point2 exitpt_left, exitpt_right;

        int cutleftbacknum = 0;
        int cutrightbacknum = 0;
        int cutleftfrontnum = 0;
        int cutrightfrontnum = 0;


      LaneLabel exit_lanelabel, enter_lanelabel;

      exit_lanelabel.segment = exitlabel.segment;
      exit_lanelabel.lane = exitlabel.lane;

      enter_lanelabel.segment = enterlabel.segment;
      enter_lanelabel.lane = enterlabel.lane;

      PointLabel ptlabel(exitlabel);
      ptlabel.point = ptlabel.point+1;
      
      if (exit_lanelabel == enter_lanelabel){
        samelaneflag = true;
      }
      //      cout << "TRANSITION FROM " << exitlabel << " TO " <<ptlabel << endl;
     

      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== exitlabel){
    
          for (j=0;j<prior.lanes[i].exit_label.size();++j){
            if (prior.lanes[i].exit_label[j]==exitlabel &&
                prior.lanes[i].exit_link[j]==enterlabel){
          

              isvalid = true;
            }
          }
          if (!isvalid)
            continue;
      
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
            exit_leftelindex = prior.lanes[i].leftbound_elindex;
            exit_rightelindex = prior.lanes[i].rightbound_elindex;
            exit_ptindex =j;
        
            break;
            }
          }
        }
      }
      if (!isvalid){
        cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
        return -1;

      }
      isvalid = false;
      for (i=0;i<prior.lanes.size(); ++i){
        if (prior.lanes[i].label== enterlabel){
    
          for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
            if (prior.lanes[i].waypoint_label[j]==enterlabel){
              enter_elindex = prior.lanes[i].waypoint_elindex;
              enter_leftelindex = prior.lanes[i].leftbound_elindex;
              enter_rightelindex = prior.lanes[i].rightbound_elindex;
              enter_ptindex =j;
              isvalid = true;
              break;
            }
          }
        }
      }

      if (!isvalid){
        cerr << "in Map::getTransitionBounds, invalid transition for an unexpected reason.  Likely error in RNDF parsing.  exit = " << exitlabel << " enter = " << enterlabel <<endl;
        return -1;
      }

      


      prior.getEl(enter_leftel,enter_leftelindex);
      enter_leftarr.set(enter_leftel.geometry);

      prior.getEl(enter_rightel,enter_rightelindex);
      enter_rightarr.set(enter_rightel.geometry);

      prior.getEl(exit_leftel,exit_leftelindex);
      exit_leftarr.set(exit_leftel.geometry);

      prior.getEl(exit_rightel,exit_rightelindex);
      exit_rightarr.set(exit_rightel.geometry);


      
      exitpt_left = exit_leftarr[exit_ptindex];  
      exitpt_right = exit_rightarr[exit_ptindex];  

      enterpt_left = enter_leftarr[enter_ptindex];  
      enterpt_right = enter_rightarr[enter_ptindex];  
  

      prior.getElFull(enter_leftel,enter_leftelindex);
      enter_leftarr.set(enter_leftel.geometry);

      prior.getElFull(enter_rightel,enter_rightelindex);
      enter_rightarr.set(enter_rightel.geometry);

      prior.getElFull(exit_leftel,exit_leftelindex);
      exit_leftarr.set(exit_leftel.geometry);

      prior.getElFull(exit_rightel,exit_rightelindex);
      exit_rightarr.set(exit_rightel.geometry);
 

      if (enterlabel == ptlabel){
        int retval = getLaneBounds(new_leftarr, new_rightarr, enter_lanelabel);
        
        

        cutleftbacknum =new_leftarr.cut_back_at_index(enterpt_left,1);
        cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);      
        
        
        cutleftfrontnum = new_leftarr.cut_front_at_index(exitpt_left,1);
        cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
 leftbound.set(new_leftarr);
      rightbound.set(new_rightarr);
        cout << "returning current lane transition" << endl;

        return retval;
      }


      point2arr tmpexleft(exit_leftarr);
      point2arr tmpexright(exit_rightarr);
      point2arr tmpenleft(enter_leftarr);
      point2arr tmpenright(enter_rightarr);


      
      exit_leftarr.cut_front(exitpt_left);
      exit_rightarr.cut_front(exitpt_right);
     
      samelaneflag =false;

      if (samelaneflag){
      exit_leftarr.cut_back(enterpt_left);
      exit_rightarr.cut_back(enterpt_right);
      }else{
      exit_leftarr.cut_back_at_index(10);
      exit_rightarr.cut_back_at_index(10);

      }


      enter_leftarr.cut_back(enterpt_left);
      enter_rightarr.cut_back(enterpt_right);
      
      if (samelaneflag){
        enter_leftarr.cut_front(exitpt_left);
        enter_rightarr.cut_front(exitpt_right);
      }else{
        enter_leftarr.cut_front_at_index(10);
        enter_rightarr.cut_front_at_index(10);

      }

      new_leftarr.set(exit_leftarr);
      new_rightarr.set(exit_rightarr);

      double distleft = exitpt_left.dist(enterpt_left);
      double distright = exitpt_right.dist(enterpt_right);
      point2 midpt;


      //--------------------------------------------------
      // merging lane lines
      //--------------------------------------------------
      double distlr = fabs(distleft-distright);
      double estang = 2*atan2(distlr/2,exitpt_left.dist(exitpt_right));
 cout << "est ang = " << estang << endl;
      
      if (estang < .1){
        cout << "GOING STRAIGHT" << endl;
        new_leftarr.connect(enter_leftarr);
        new_rightarr.connect(enter_rightarr);
        
      }else{
      if (distleft<distright){
        if (enter_rightarr.size()>0){
          midpt = (new_leftarr.back()+enter_leftarr[0])/2;
          new_leftarr.push_back(midpt);
          new_leftarr.connect(enter_leftarr);
          new_rightarr.connect_intersect(enter_rightarr);
        }
      }else{
        if (enter_leftarr.size()>0){
          midpt = (new_rightarr.back()+enter_rightarr[0])/2;
          new_rightarr.push_back(midpt);
          new_leftarr.connect_intersect(enter_leftarr);
          new_rightarr.connect(enter_rightarr);
        }
      }
      }

      
      //  waypts = waypt_el.geometry;
      //  if (left_neardist<0)
      //     left_neardist = 0;

      // if (right_neardist<0)
      //     right_neardist = 0;
      //      double BACK_RANGE = 5;  

      
     point2arr tmpptarr(new_leftarr);
      
     
     cutleftbacknum =tmpptarr.cut_back_at_index(enterpt_left,1);
     cout << "cutleftbacvk =  " <<cutleftbacknum << endl;     
     cutleftbacknum = new_leftarr.cut_back_at_index(cutleftbacknum+1);
      cout << "cutleftbacvk2 =  " <<cutleftbacknum << endl;    
 cutrightbacknum = new_rightarr.cut_back_at_index(cutleftbacknum);      
     

 tmpptarr.set(new_leftarr);
      cutleftfrontnum = tmpptarr.cut_front_at_index(exitpt_left);
      cutleftfrontnum = new_leftarr.cut_front_at_index(cutleftfrontnum+1);
 cout << "cutleftfrontk =  " <<cutleftfrontnum << endl;  
      cutrightfrontnum = new_rightarr.cut_front_at_index(cutleftfrontnum);
   

      
      leftbound.set(new_leftarr);
      rightbound.set(new_rightarr);
      return 0; 

 
    }











int Map::getWaypointArr(point2arr& ptarr,const PointLabel &label)
{ 
  int waypt_elindex;
  MapElement waypt_el;
  for (uint i=0;i<prior.lanes.size();++i){
    if(label == prior.lanes[i].label){
      waypt_elindex = prior.lanes[i].waypoint_elindex;
      prior.getEl(waypt_el, waypt_elindex);  
      ptarr.set(waypt_el.geometry);
      return (0);
      break;
    }
  }
  
    cerr << "in Map::getWaypointArr, Failed finding a matching lane " << label <<endl;
    return -1;
}

int Map::getLaneDistToWaypoint(double &dist, const point2 state,const PointLabel &label)
{
  point2 waypt;
  int retval;
  retval = getWaypoint(waypt, label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << label <<endl;
    return retval;
  }
  point2arr ptarr;
  retval = getWaypointArr(ptarr,label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint array from label " << label <<endl;
    return retval;
  }

  

  double d1 = ptarr.project_along(waypt);
  double d2 = ptarr.project_along(state);

  dist = d1-d2;
  
  return 0;
}

int Map::getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label)
{
  point2 waypt;
  int retval;
  retval = getWaypoint(waypt, label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << label <<endl;
    return retval;
  }

  point2 statewaypt;
  retval = getWaypoint(statewaypt, statelabel);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint from label " << statelabel <<endl;
    return retval;
  }


  point2arr ptarr;
  retval = getWaypointArr(ptarr,label);
  if (retval<0){
    cerr << "in Map::getDistToWaypoint, Failed getting waypoint array from label " << label <<endl;
    return retval;
  }

  

  double d1 = ptarr.project_along(waypt);
  double d2 = ptarr.project_along(statewaypt);

  dist = d1-d2;
  
  return 0;
}





int Map::getObstacleGeometry(point2arr& ptarr)
{
  cout <<" Map::getObstacleGeometry() not yet implemented!" << endl;
  return 0;
}



double Map::getObstacleDist(const point2& state, const int lanedelta)
{
  double mindist= 1000000;
  double thisdist;
  unsigned int i;
  MapElement el;
  int left_index;
  int right_index;
  int left_index2;
  int right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  bool gotlane = false;
  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      //      if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
      if (left_index==0 &&right_index==0){    
        label = prior.lanes[i].label;
        gotlane = true;
        break;
      }
    }
    
  }
  if (!gotlane) {
    cout << "in Map::getObstacleDist() warning, not in any lane at point " << state << endl;
  return -1;
  }
  cout << "in Map::getObstacleDist() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
        if (left_index2==0 &&right_index2==0){    
          //if (left_index2>left_index &&right_index2>right_index &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
        
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
        }
      }
    }
    
  }
  if (obsid >=0){
    cout << "in Map::getObstacleDist() found obstacle "
         << obsid << " at distance " << mindist 
         << " in lane " << label<< endl;
    return mindist;
  }
  else{
    cout << "in Map::getObstacleDist() found no obstacles in lane " << label<< endl;}
  return -1;
}



point2 Map::getObstaclePoint(const point2& state, const double offset)
{
  double mindist= 1000000;
  double thisdist;
  unsigned int i;
  MapElement el;
  int left_index;
  int right_index;
  int left_index2;
  int right_index2;
  int left_side;
  int right_side;
  int left_elindex,right_elindex;
  MapElement left_el;
  MapElement right_el;
  point2arr left_lane, right_lane;
  LaneLabel label;
  int laneindex;
  bool gotlane = false;
  point2 outpt;

  for (i=0;i<prior.lanes.size();++i){
    left_elindex = prior.lanes[i].leftbound_elindex;
    right_elindex = prior.lanes[i].rightbound_elindex;
    prior.getEl(left_el,left_elindex);
    prior.getEl(right_el,right_elindex);
    left_lane=left_el.geometry;
    right_lane=right_el.geometry;
    left_lane.get_side(state,left_side,left_index);
    right_lane.get_side(state,right_side,right_index);
    if (left_side!=right_side){
      // if (left_index>0 &&right_index>0 &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
        if (left_index==0 &&right_index==0){     
   label = prior.lanes[i].label;
        laneindex = i;
        gotlane = true;
        break;
      }
    }
    
  }
  if (!gotlane) {
    cout << "in Map::getObstaclePoint() warning, not in any lane at point " << state<< endl;
    return outpt;
  }

  cout << "in Map::getObstaclePoint() getting obstacles in lane "<< label << endl;
  
  int obsid = -1;
  point2 cpt,obspt;
  
  for (i=0;i<data.size();++i){
    el = data[i];
    if (el.height<=0)
      continue;
    cpt = el.center;
    left_lane.get_side(cpt,left_side,left_index2);
    right_lane.get_side(cpt,right_side,right_index2);
    if (left_side!=right_side){
      //      if (left_index2>left_index &&right_index2>right_index &&left_index<(double)(left_lane.size()-1) && right_index< (double) (right_lane.size()-1)){
        if (left_index2==0 &&right_index2==0){
        thisdist = cpt.dist(state);
        if (thisdist < mindist){
          mindist = thisdist;
          obsid = i;
          obspt = cpt;
        }
      }
    }
    
  }
  if (obsid >=0)
    cout << "in Map::getObstaclePoint() found obstacle "
         << obsid << " at distance " << mindist 
         << " in lane " << label<< endl;
  else
    cout << "in Map::getObstaclePoint() found no obstacles in lane " << label<< endl;



  if (obsid >=0){
    int waypt_elindex = prior.lanes[laneindex].waypoint_elindex;
    MapElement waypt_el;
    prior.getEl(waypt_el, waypt_elindex);  
    point2arr wayptarr;
    wayptarr.set(waypt_el.geometry);



    double wayptdist = wayptarr.cut_front(obspt);
    if (wayptdist>offset){
      wayptarr.cut_front(wayptdist-offset);
      outpt = wayptarr.back();
    }
    else {
      cout << "in Map::getObstaclePoint , offset pushes us out of lane" << endl;
      outpt = state;
    }
  }
  return outpt;
}




 //  int Map::getRoadBounds(point2arr& leftbound, point2arr& rightbound,const LaneLabel &label, const point2 state, const double range )
//     {

//       cerr << "WARNING getRoadBounds not implemented.  Use getBounds to return lane bounds." << endl;
      
//       unsigned int i;
//       MapElement el;
//       //  double left_index;
//       //double right_index;
//       //  double left_index2;
//       // double right_index2;
//       //int left_side;
//       //int right_side;
//       int left_elindex,right_elindex, waypt_elindex;
//       MapElement left_el;
//       MapElement right_el;
//       MapElement waypt_el;
//       point2arr left_lane, right_lane, waypts;

//       bool validlabel = false;
  
//       int lanenum = label.lane;
//       int segnum = label.segment;

//       LaneLabel maxlabel;
//       LaneLabel minlabel;
      
//       int numlanes = 0;
//       int segindex = i;
//       int laneindex = lanenum;

//       for (i=0;i<prior.segments.size();++i){
//         if (prior.segments[i].label == label.segment){
//           segindex = i;
//           numlanes = prior.segments[i].lane_label.size();
//         }

//       }

//       double thisang = 0;
//       if (getHeading(thisang,label)!=0)
//         cerr << "in getRoadBounds - cant find this label heading" << endl;

//       double tmpang = 0;
//       double dang;
//       vector<bool> sameheading; 
//       LaneLabel tmplanelabel;
//       vector<LaneLabel>

//       for (i=0;i<numlanes;++i){
//         tmplanelabel = prior.segment[segindex].lane_label[i];
//         getHeading(tmpang, tmplanelabel);
//         dang = tmpang-thisang;
//         if (fabs(dang)>(M_PI/2) && fabs(dang)<(3*M_PI/2)){
//           sameheading.push_back(false);
//         }
//         else{
//           sameheading.push_back(true);
//         }
//       }

//       for (i=0;i<numlanes;++i)
//         tmplanelabel = prior.segment[segindex].lane_label[i];
//       if (sameheading[i]){
//         val = getBounds(tmpleftptarr,tmprightptarr,tmplanelabel,state,range);
//       }

   
//       int val;
//       double minang, maxang, thisang;
//       if (getHeading(minang,minlabel)!=0)
//         cerr << "in getRoadBounds - cant find minlabel heading" << endl;
//       if (getHeading(maxang,maxlabel)!=0)
//         cerr << "in getRoadBounds - cant find maxlabel heading" << endl;
      
      
//       if (

//         for (i=0;i<prior.lanes.size(); ++i){
         
//           if (prior.lanes[i].label == templabel){ 
            
//           }

//         }

//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label == label){
//           left_elindex = prior.lanes[i].leftbound_elindex;
//           right_elindex = prior.lanes[i].rightbound_elindex;
//           waypt_elindex = prior.lanes[i].waypoint_elindex;
//           prior.getEl(left_elindex,left_el);
//           prior.getEl(right_elindex,right_el);
//           prior.getEl(waypt_elindex,waypt_el);
//           left_lane=left_el.geometry;
//           right_lane=right_el.geometry;
//           waypts=waypt_el.geometry;
//           validlabel = true;
//           break;
//         }
//       }
//       if(!validlabel){
//         cout << "in Map::getRoadBounds() passed bad lane label "<<label <<endl;
//         return -1;
//       }
  


//       double BACK_RANGE = 5;
//       waypts = waypt_el.geometry;
//       double statedist = waypts.project_along(state);

//       double neardist = statedist-BACK_RANGE;

//       if (neardist<0)
//         neardist = 0;


//       waypts.cut_back(waypts.linelength()-neardist);
//       waypts.cut_front(range);

//       point2 nearpt,farpt;
//       nearpt = waypts[0];
//       farpt = waypts.back();
//       left_lane.cut_back(nearpt);
//       left_lane.cut_front(farpt);

//       right_lane.cut_back(nearpt);
//       right_lane.cut_front(farpt);

//       leftbound = left_lane;
//       rightbound = right_lane;
//      return -1;
//    } 




 

 //  int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel)
//     {
//       unsigned int i,j;
//       int exit_elindex =-1;
//       int exit_leftelindex =-1;
//       int exit_rightelindex =-1;
//       int exit_ptindex =-1;
//       int enter_elindex =-1;
//       int enter_leftelindex =-1;
//       int enter_rightelindex =-1;

//       int enter_ptindex =-1;
  
//       point2 exitpt;
//       point2 enterpt;
//       MapElement el;

//       bool isvalid = false;

//       LaneLabel tmplane(enterlabel.segment,enterlabel.lane);
//       if ((exitlabel.segment==enterlabel.segment) && (exitlabel.lane==enterlabel.lane)){
//         getLeftBound(leftbound, tmplane);
//         int retval = getRightBound(rightbound, tmplane);
//         cout << "returning current lane transition" << endl;
//         return retval;
//       }

//       for (i=0;i<prior.lanes.size(); ++i){
//         if (prior.lanes[i].label== exitlabel){
    
//           for (j=0;j<prior.lanes[i].exit_label.size();++j){
//             if (prior.lanes[i].exit_label[j]==exitlabel &&
//                 prior.lanes[i].exit_link[j]==enterlabel){
        

//               isvalid = true;
//             }
//           }
//           if (!isvalid)
//             continue;
      
//           for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
//             if (prior.lanes[i].waypoint_label[j]==exitlabel){  exit_elindex = prior.lanes[i].waypoint_elindex;
//             exit_leftelindex = prior.lanes[i].leftbound_elindex;
//             exit_rightelindex = prior.lanes[i].rightbound_elindex;
//             exit_ptindex =j;
//             break;
//             }
//           }
//         }
//       }
//       if (isvalid){
//         for (i=0;i<prior.lanes.size(); ++i){
//           if (prior.lanes[i].label== enterlabel){
    
//             for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
//               if (prior.lanes[i].waypoint_label[j]==enterlabel){
//                 enter_elindex = prior.lanes[i].waypoint_elindex;
//                 enter_leftelindex = prior.lanes[i].leftbound_elindex;
//                 enter_rightelindex = prior.lanes[i].rightbound_elindex;
//                 enter_ptindex =j;
//                 break;
//               }
//             }
//           }
//         }
//         prior.getEl(enter_leftel,elindex);
//         enterpt = el.geometry[enter_ptindex];  
//         prior.getEl(exit_leftel,elindex);
//         exitpt = el.geometry[exit_ptindex];  

//         leftbound.clear();
//         leftbound.push_back(enterpt);
//         leftbound.push_back(exitpt);

//         prior.getEl(enter_rightel,elindex);
//         enterpt = el.geometry[enter_ptindex];  
//         prior.getEl(exit_rightel,elindex);
//         exitpt = el.geometry[exit_ptindex];  

//         rightbound.clear();
//         rightbound.push_back(enterpt);
//         rightbound.push_back(exitpt);
  
//         return 0;
//       }


//       cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
//       return -1;
//     }

