/**********************************************************
 **
 **  TESTMAP.CC
 **
 **    Time-stamp: <2007-03-14 10:00:06 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Mar  1 12:53:36 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/
 
#include "Map.hh"
#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"

using namespace std;


int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testMap [rndf filename] [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  if(argc >1)
    fname = argv[1];
  if(argc >2)
    testnum = atoi(argv[2]);
  if(argc >3)
    sendSubGroup = atoi(argv[3]);
  if(argc >4)
    recvSubGroup = atoi(argv[4]);
  if(argc >5)
    skynetKey = atoi(argv[5]);
  
  cout << "RNDF filename = " << fname 
       << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey);

  Map mapdata;
  //  mapdata.loadRNDF("rn.txt");

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };
 
  MapElement el;
	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;  
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getEl(i,el);
    //TEMP 
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,sendSubGroup);
	}
  cout <<"========================================" << endl;
  testname = "Testing getClosestWaypoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl;
    
    sendEl.set_id(-1);
    sendEl.set_points();
    sendEl.plot_color = COLOR_MAGENTA;

    int retval;
  
    PointLabel label;
    point2 statept;
    point2 pt;
    point2arr ptarr;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        cout << "ptlabel = " << label << endl;   
        retval = mapdata.getWaypoint(pt,label);
        cout << "getWaypoint retval = " << retval << " pt = " << pt << "  label " << label << endl;
        ptarr.clear();
        ptarr.push_back(pt);
        sendEl.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing getStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;  
    int retval;
    PointLabel label;
    point2 statept;
    point2 stoppt;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        cout << "ptlabel = " << label << endl;  
        retval =mapdata.getStopline(stoppt,statept);
        cout << "get stopline retval = " << retval << " point = " << stoppt << endl;
   
      }
      else{ 
        
        usleep (100000);

      }
    }
  }
  cout <<"========================================" << endl;
  testname = "Testing getNextStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;

    int retval;
  
    PointLabel label;
    point2 statept;
    point2 stoppt;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getNextPointID(statept);
        cout << "ptlabel = " << label << endl;  
        retval =mapdata.getNextStopline(stoppt,label);
        cout << "get stopline retval = " << retval << " point = " << stoppt << "  stop label " << label << endl;
   
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  cout <<"========================================" << endl;
  testname = "Testing getLeftBound(), getRightBound() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl2.set_id(-2);
    sendEl1.set_laneline();
    sendEl2.set_laneline();
    sendEl1.plot_color = COLOR_GREEN;
    sendEl2.plot_color = COLOR_RED;

     

    int retval;
  
    PointLabel label;
    LaneLabel lanelabel;
    point2 statept;
    point2 stoppt;
    point2arr ptarr;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        lanelabel.segment = label.segment;
        lanelabel.lane = label.lane;
        cout << "ptlabel = " << label << endl;  
        cout << "lanelabel = " << lanelabel << endl;  
        retval =mapdata.getLeftBound(ptarr,lanelabel);
        cout << "getLeftBound retval = " << retval << " ptarr = " << ptarr << "  label " << label << endl;
        sendEl1.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl1,sendSubGroup);

        retval =mapdata.getRightBound(ptarr,lanelabel);
        cout << "getRightBound retval = " << retval << " ptarr = " << ptarr << "  label " << label << endl;
        sendEl2.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl2,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing getTransitionBounds() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl2.set_id(-2);
    sendEl1.set_laneline();
    sendEl2.set_laneline();
    sendEl1.plot_color = COLOR_GREEN;
    sendEl2.plot_color = COLOR_RED;

     

    int retval;
  
    PointLabel label1;
    PointLabel label2;
    LaneLabel lanelabel;
    point2 statept;
    point2 stoppt;
    point2arr ptarr1;
    point2arr ptarr2;
    bool exitflag = true;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (exitflag){
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          label1=  mapdata.getClosestPointID(statept);
          cout << "label exit segment = " << label1 << endl;
          exitflag = false;
        }else{
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          label2=  mapdata.getClosestPointID(statept);
          cout << "label enter segment = " << label2 << endl;  
        
          retval =mapdata.getTransitionBounds(ptarr1,ptarr2,label1,label2);
          cout << "getTransitionBounds retval = " << retval << endl              << " ptarr1 = " << ptarr1 
               << " ptarr2 = " << ptarr2 << endl
               << " exit label " << label1 
               << " enter label " << label2 << endl;
          sendEl1.set_geometry(ptarr1);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);

          sendEl2.set_geometry(ptarr2);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);
          exitflag = true;
        }
      }
      else{ 
        usleep (100000);
      }
    }
  }
  
  cout <<"========================================" << endl;
  testname = "Testing getObstacleDist(), getObstaclePoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1, sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_circle_obs();
    sendEl1.plot_color = COLOR_MAGENTA;


    sendEl2.set_id(-2);
    sendEl2.set_points();
    sendEl2.plot_color = COLOR_RED;

    int retval;
  
    PointLabel label;
    point2 statept;
    point2 pt;
    point2arr ptarr;
    
    bool initflag = true;
    double dist;
    while (true){

       
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (initflag){
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          sendEl1.set_geometry(statept,2);
          cout << "Setting obstacle = " << endl;   
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          mapdata.data.push_back(sendEl1); 
          initflag = false;
        }
        else {
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          dist=  mapdata.getObstacleDist(statept);
          cout << "DIST = " << dist << endl;
          pt = mapdata.getObstaclePoint(statept,10);
          sendEl2.set_geometry(pt);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);

        }
      }  
      else{  
        usleep (100000); 
      }
    }
  }


  //   bool bretval = mapdata.isStop(stoplabel);
  //   cout << "isStop for " << stoplabel << " = " << bretval << endl;

  //   bretval = mapdata.isStop(enterlabel);
  //   cout << "isStop for " << enterlabel << " = " << bretval << endl;

  //   bretval = mapdata.isExit(enterlabel);
  //   cout << "isExit for " << enterlabel << " = " << bretval << endl;
  //   bretval = mapdata.isExit(exitlabel);
  // cout << "isExit for " << exitlabel << " = " << bretval << endl;

  //  bretval = mapdata.isExit(stoplabel);
  // cout << "isExit for " << stoplabel << " = " << bretval << endl;
 
  //   val = mapdata.getStopline(pt,stoplabel);
  //   cout << "Stopline at " << stoplabel << " = " << pt << endl;
  //   el.clear();
  //   id.clear();
  //   id.push_back(stoplabel.segment);
  //   id.push_back(stoplabel.lane);
  //   id.push_back(stoplabel.point);
  //   rawptarr.clear();
  //   rawptarr.push_back(pt);
  //   el.set_stopline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //   val = mapdata.getLeftBound(ptarr,lanelabel);
  //   cout << "Left lane at " << lanelabel << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel.segment);
  //   id.push_back(lanelabel.lane);
  //   id.push_back(0);
 
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //     val = mapdata.getRightBound(ptarr,lanelabel);
  //   cout << "Right lane at " << lanelabel << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel.segment);
  //   id.push_back(lanelabel.lane);
  //   id.push_back(1);
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  
  //   val = mapdata.getLeftBound(ptarr,lanelabel2);
  //   cout << "Left lane at " << lanelabel2 << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel2.segment);
  //   id.push_back(lanelabel2.lane);
  //   id.push_back(0);
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //     val = mapdata.getRightBound(ptarr,lanelabel2);
  //   cout << "Right lane at " << lanelabel2 << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel2.segment);
  //   id.push_back(lanelabel2.lane);
  //   id.push_back(1);

  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);



  //   val = mapdata.getLeftBoundType(lanetype,lanelabel);
  //   cout << "Left lane type at " << lanelabel << " = " << lanetype << endl;

  //   val = mapdata.getRightBoundType(lanetype,lanelabel);
  //   cout << "Right lane type at " << lanelabel << " = " << lanetype << endl;

  //   val = mapdata.getTransitionBounds(lptarr,rptarr,exitlabel,enterlabel);
  //   cout << "Transition from " << exitlabel 
  //        << " to " << enterlabel 
  //        << ", left: " << lptarr << endl
  //        << "right: " << rptarr<<endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(exitlabel.segment);
  //   id.push_back(exitlabel.lane);
  //   id.push_back(exitlabel.point);
  //   id.push_back(enterlabel.segment);
  //   id.push_back(enterlabel.lane);
  //   id.push_back(enterlabel.point);
  //   id.push_back(0);
  
  //   rawptarr.clear();
  //   rawptarr = lptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //   el.clear();
  //   id.clear();  
  //   id.push_back(exitlabel.segment);
  //   id.push_back(exitlabel.lane);
  //   id.push_back(exitlabel.point);
  //   id.push_back(enterlabel.segment);
  //   id.push_back(enterlabel.lane);
  //   id.push_back(enterlabel.point);
  //   id.push_back(1);
  
  //   rawptarr.clear();
  //   rawptarr = rptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

   
    

  
 
 


 
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
