/**********************************************************
 **
 **  MAPID.CC
 **
 **    Time-stamp: <2007-03-10 12:42:06 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Mar 10 09:00:22 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapId.hh"

using namespace std;


unsigned int MapId::insert(unsigned int n, int id)
{
  unsigned int index=n;
  if (n > arr.size())
    index = arr.size();
  arr.insert(arr.begin()+index,id);
  return index;
}

int &MapId::operator[](unsigned int n) 
{
  if (n>=size()){
    cerr << "Error in MapId::operator[]  bad index = " 
         << n << " for MapId of size " << size() << endl;
    }
  return arr[n];
}
const int &MapId::operator[](unsigned int n) const 
{
  if (n>=size()){
    cerr << "Error in MapId::operator[]  bad index = " << n << " for MapId of size " << size() << endl;
  }
  return arr[n];
}


bool MapId::operator== (const MapId& id) const
{
  if (id.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (id[i]!=this->arr[i])
        return false;
    }
    return true;
  }
  return false;
}

bool MapId::operator!= (const MapId& id) const
{
  if (id.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (id[i]!=this->arr[i])
        return true;
    }
    return false;
  }
  return true;
}

bool MapId::match(const MapId &id) const
{
  unsigned minsize = id.size();
  if (this->size()<minsize) 
    minsize = this->size();

  for (unsigned i = 0; i<minsize;++i){
    if (id[i]!=this->arr[i])
      return false;
  }
  return true;
  
}

ostream &operator<<(ostream &os, const MapId &id) {
  unsigned int i;
  os << "[";
  for (i=0;i<id.size();++i){
    os << id[i];
    if (i<(id.size()-1))
      os << ", ";
  }
  os << "]";
  return os;
}
