#include <iostream>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "interfaces/sn_types.h"
#include "skynettalker/StateClient.hh"

using namespace std;

/** The CStateTalker class gets info from CState, actuator state, etc,
 * and sends it to the mapviewer for visualization 
 *
 * This should probably be moved to a more appropriate location eventually,
 * but I'm not really sure where that is. */
class CStateTalker : public CStateClient, CMapElementTalker {

public:

  int skynetKey;

  /*! Constructor */
  CStateTalker(int skynet_key);
  
  /*! Standard destructor */
  ~CStateTalker() {}
  
  /*! Gets state info and send the alice object to mapviewer */
  void sendAlice();

  /*! Sends the traversed path spline to mapviewer */
  void sendTraversedPath();

private:
  
  vector<int> travID;
  vector<point2> travPath;
  int count;

};

CStateTalker::CStateTalker(int skynet_key)
  : CSkynetContainer(MODmapping, skynet_key)
{
  skynetKey = skynet_key;
  travID.push_back(1);
  count = 0;
  cout << "Constructing skynet with KEY = " << skynetKey << endl;
}

void CStateTalker::sendAlice()
{
  UpdateState();
 
  MapElement obj;
  obj.set_alice(m_state);
  initSendMapElement(skynetKey);
  sendMapElement(&obj);

  // add another point to trav path (not every cycle though)
  count++;
  if (count==20) {
    count = 0;
    travPath.push_back(point2(m_state.localX,m_state.localY));
  }
}

void CStateTalker::sendTraversedPath()
{
  MapElement obj;
  obj.set_trav_path(travID,travPath);
  initSendMapElement(skynetKey);
  sendMapElement(&obj);
}

/** The main function sets up a CAliceTalker and uses state data
 * to construct various map elements that visualize various alice data
 */
int main(int argc, char **argv)
{
  
  int skynetKey = 0;
  int subgroup = 0;
  
  skynetKey = atoi(getenv("SKYNET_KEY"));  

  if(argc >1)
    subgroup = atoi(argv[1]);
  if(argc >2)
    skynetKey = atoi(argv[2]);
  
  cout << "Skynet Key = " << skynetKey << 
    "  Subgroup = "  << subgroup << endl;

  int bytesSent = 0;
  
  // create the talker
  CStateTalker talker(skynetKey);

  // send the alice object
  while (true) {
    talker.sendAlice();
    talker.sendTraversedPath();
    usleep(10);
  }

  return(0);
}

