/**********************************************************
 **
 **  MAPID.HH
 **
 **    Time-stamp: <2007-03-12 13:08:18 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Mar 10 08:31:31 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPID_H
#define MAPID_H

#include <math.h>
#include <vector>
#include <iostream>

using namespace std;
//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class MapId
{
  
public:

  vector<int> arr;
  //! A Constructor 
  /*! DETAILS */

  MapId() {arr.clear();}
  MapId(const MapId& id) {arr = id.arr;}
  MapId(const vector<int> id) {arr = id;}
  MapId(const int id) {arr.clear(); arr.push_back(id);}
  MapId(const int id1,const int id2) {arr.clear(); arr.push_back(id1); arr.push_back(id2);}
  MapId(const int id1,const int id2,const int id3) {arr.clear(); arr.push_back(id1); arr.push_back(id2); arr.push_back(id3);}
  MapId(const int id1,const int id2,const int id3,const int id4) {arr.clear(); arr.push_back(id1); arr.push_back(id2); arr.push_back(id3); arr.push_back(id4);}

    
  //! A Destructor 
  /*! DETAILS */
  ~MapId() {}
  
  void resize(unsigned int size) {arr.resize(size);}
	void clear() {arr.clear();}
	void push_back(const int &id) {arr.push_back(id);}
	unsigned int insert(unsigned int n, int id);

	int &operator[](unsigned int n);
	const int &operator[](unsigned int n) const;
	unsigned int size() const {return arr.size();}

	int back() {return this->arr.back();}
  int front() {return this->arr.front();}

	bool operator== (const MapId& id) const;
	bool operator!= (const MapId& id) const ;
	bool operator== (const int id) const;
	bool operator!= (const int id) const ;

  bool match(const MapId &id) const;
  bool match(const int id) const;
  bool match(const int id1,const int id2) const;
  bool match(const int id1,const int id2,const int id3) const;
  bool match(const int id1, const int id2, const int id3, const int id4) const;

	MapId &operator=(int id1){
    this->clear();
    this->push_back(id1);
		return *this;
	}

	MapId &operator=(const MapId &id){
		if (this!= &id){
			this->arr = id.arr;
		}
		return *this;
	}


	MapId &operator=(const vector<int> &id){
		this->arr = id;
		return *this;
  }

	void set(int id1){
    clear();
    push_back(id1);
	}
	void set(int id1,int id2){
    clear();
    push_back(id1);
    push_back(id2);
	}
	void set(int id1,int id2,int id3){
    clear();
    push_back(id1);
    push_back(id2);
    push_back(id3);
	}
	void set(int id1,int id2,int id3,int id4){
    clear();
    push_back(id1);
    push_back(id2);
    push_back(id3);
    push_back(id4);
	}

	void set(const MapId& id){
    arr = id.arr;
	}

	void set(const vector<int> id){
    arr = id;
	}




};

ostream &operator<<(ostream &os, const MapId &id);

#endif
