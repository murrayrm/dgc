/**********************************************************
 **
 **  MAPID.CC
 **
 **    Time-stamp: <2007-05-18 18:50:49 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Mar 10 09:00:22 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapId.hh"

using namespace std;


unsigned int MapId::insert(unsigned int n, int id)
{
  unsigned int index=n;
  if (n > dat.size())
    index = dat.size();
  dat.insert(dat.begin()+index,id);
  return index;
}

int &MapId::operator[](unsigned int n) 
{
  if (n>=size()){
    cerr << "Error in MapId::operator[]  bad index = " 
         << n << " for MapId of size " << size() << endl;
  }
  return dat[n];
}
const int &MapId::operator[](unsigned int n) const 
{
  if (n>=size()){
    cerr << "Error in MapId::operator[]  bad index = " << n << " for MapId of size " << size() << endl;
  }
  return dat[n];
}


bool MapId::operator== (const int id) const
{
  if (size()==1){
    if (dat[0]==id)
      return true;
  }
  return false;
}


bool MapId::operator!= (const int id) const
{
  if (size()==1){
    if (dat[0]==id)
      return false;
  }
  return true;
}


bool MapId::operator== (const MapId& id) const
{
  if (id.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (id[i]!=this->dat[i])
        return false;
    }
    return true;
  }
  return false;
}

bool MapId::operator!= (const MapId& id) const
{
  if (id.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (id[i]!=this->dat[i])
        return true;
    }
    return false;
  }
  return true;
}

bool MapId::match(const MapId &id) const
{
  unsigned minsize = id.size();
  if (this->size()<minsize) 
    minsize = this->size();

  for (unsigned i = 0; i<minsize;++i){
    if (id[i]!=this->dat[i])
      return false;
  }
  return true;
  
}

bool MapId::match(const int id) const
{
  MapId mapid(id);
  return( match(mapid));
}

bool MapId::match(const int id1, const int id2) const
{
  MapId mapid(id1,id2);
  return( match(mapid));
}

bool MapId::match(const int id1, const int id2, const int id3) const
{
  MapId mapid(id1,id2,id3);
  return( match(mapid));
}

bool MapId::match(const int id1, const int id2, const int id3, const int id4) const
{
  MapId mapid(id1,id2,id3,id4);
  return( match(mapid));
}
bool MapId::match(const int id1, const int id2, const int id3, const int id4,const int id5) const
{
  MapId mapid(id1,id2,id3,id4,id5);
  return( match(mapid));
}



ostream &operator<<(ostream &os, const MapId &id) {
  unsigned int i;
  os << "[";
  for (i=0;i<id.size();++i){
    os << id[i];
    if (i<(id.size()-1))
      os << ", ";
  }
  os << "]";
  return os;
}





unsigned int MapIdArr::insert(unsigned int n, MapId &id)
{
  unsigned int index=n;
  if (n > arr.size())
    index = arr.size();
  arr.insert(arr.begin()+index,id);
  return index;
} 

MapId &MapIdArr::operator[](unsigned int n)
{
  if (n>=size()){
    cerr << "Error in MapIdArr::operator[]  bad index = " << n << " for MapId of size " << size() << endl;
  }
  return arr[n];
}

const MapId &MapIdArr::operator[](unsigned int n) const
{
  if (n>=size()){
    cerr << "Error in MapIdArr::operator[]  bad index = " << n << " for MapId of size " << size() << endl;
  }
  return arr[n];
}



bool MapIdArr::operator== (const MapIdArr& idarr) const
{
  if (idarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (idarr[i]!=this->arr[i])
        return false;
    }
    return true;
  }
  return false;
}


bool MapIdArr::operator!= (const MapIdArr& idarr) const
{
  if (idarr.size()==this->size()){
    for (unsigned i = 0; i<this->size();++i){
      if (idarr[i]!=this->arr[i])
        return true;
    }
    return false;
  }
  return true;
}

bool MapIdArr::getIndex(int &index, const MapId & id) const
{
  int thissize = size();
  int i;
  MapId thisid;
  for (i = 0; i < thissize; ++i){
    thisid = arr[i];
    if (thisid==id){
      index = i;
      return true;
    }
  }
  index = -1;
  return false;
}

bool MapIdArr::getIndex(MapIdArr &indexarr, const MapIdArr & idarr) const
{
  int thissize = size();
  int thatsize = idarr.size();
  int i,j;
  MapId thisid,thatid;
  bool retval = false;
  for (i = 0; i < thissize; ++i){
    thisid = arr[i];
    for (j=0; j<thatsize;j++){
      thatid = idarr[j];
      if (thisid==thatid){
        retval = true;
        indexarr.push_back(i);
        break;
      }
    }
  }
  return retval;
}



