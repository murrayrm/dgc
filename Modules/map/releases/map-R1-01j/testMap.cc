/**********************************************************
 **
 **  TEST_MAP.CC
 **
 **    Time-stamp: <2007-03-12 15:03:54 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Mar  1 12:53:36 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "Map.hh"
#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"

using namespace std;


int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testMap [rndf filename] [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  if(argc >1)
    fname = argv[1];
  if(argc >2)
    testnum = atoi(argv[2]);
  if(argc >3)
    sendSubGroup = atoi(argv[3]);
  if(argc >4)
    recvSubGroup = atoi(argv[4]);
  if(argc >5)
    skynetKey = atoi(argv[5]);
  
  cout << "RNDF filename = " << fname 
       << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey);

  Map mapdata;
  //  mapdata.loadRNDF("rn.txt");

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };

  MapElement el;
	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getEl(i,el);
    //TEMP
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,sendSubGroup);
	}

  cout <<"========================================" << endl;
  testname = "Testing getStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    
    vector<int> id;
    vector<point2> rawptarr;

    PointLabel exitlabel(1,1,4);
    PointLabel enterlabel(1,1,5);
    //  PointLabel stoplabel(10,1,7);
    PointLabel stoplabel(1,2,8);
    LaneLabel lanelabel(1,1);
    LaneLabel lanelabel2(4,2);
    string lanetype;
    int val;

    point2 pt;
    point2arr ptarr, lptarr, rptarr;

    int retval;
    rawptarr.clear();
    PointLabel label;
    point2 statept;
    point2 stoppt;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getNextPointID(statept);
        cout << "ptlabel = " << label << endl;  
        retval =mapdata.getStopline(stoppt,statept);
        cout << "get stopline retval = " << retval << " point = " << stoppt << endl;
   
      }
      else{ 
        
        usleep (100000);

      }
    }
  }
  cout <<"========================================" << endl;
  testname = "Testing getNextStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    
    vector<int> id;
    vector<point2> rawptarr;

    PointLabel exitlabel(1,1,4);
    PointLabel enterlabel(1,1,5);
    //  PointLabel stoplabel(10,1,7);
    PointLabel stoplabel(1,2,8);
    LaneLabel lanelabel(1,1);
    LaneLabel lanelabel2(4,2);
    string lanetype;
    int val;

    point2 pt;
    point2arr ptarr, lptarr, rptarr;

    int retval;
    rawptarr.clear();
    PointLabel label;
    point2 statept;
    point2 stoppt;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getNextPointID(statept);
        cout << "ptlabel = " << label << endl;  
        retval =mapdata.getNextStopline(stoppt,label);
        cout << "get stopline retval = " << retval << " point = " << stoppt << "  stop label " << label << endl;
   
      } 
      else{ 
        
        usleep (100000);

      }
    }
  }
  //   bool bretval = mapdata.isStop(stoplabel);
  //   cout << "isStop for " << stoplabel << " = " << bretval << endl;

  //   bretval = mapdata.isStop(enterlabel);
  //   cout << "isStop for " << enterlabel << " = " << bretval << endl;

  //   bretval = mapdata.isExit(enterlabel);
  //   cout << "isExit for " << enterlabel << " = " << bretval << endl;
  //   bretval = mapdata.isExit(exitlabel);
  // cout << "isExit for " << exitlabel << " = " << bretval << endl;

  //  bretval = mapdata.isExit(stoplabel);
  // cout << "isExit for " << stoplabel << " = " << bretval << endl;
 
  //   val = mapdata.getStopline(pt,stoplabel);
  //   cout << "Stopline at " << stoplabel << " = " << pt << endl;
  //   el.clear();
  //   id.clear();
  //   id.push_back(stoplabel.segment);
  //   id.push_back(stoplabel.lane);
  //   id.push_back(stoplabel.point);
  //   rawptarr.clear();
  //   rawptarr.push_back(pt);
  //   el.set_stopline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //   val = mapdata.getLeftBound(ptarr,lanelabel);
  //   cout << "Left lane at " << lanelabel << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel.segment);
  //   id.push_back(lanelabel.lane);
  //   id.push_back(0);
 
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //     val = mapdata.getRightBound(ptarr,lanelabel);
  //   cout << "Right lane at " << lanelabel << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel.segment);
  //   id.push_back(lanelabel.lane);
  //   id.push_back(1);
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  
  //   val = mapdata.getLeftBound(ptarr,lanelabel2);
  //   cout << "Left lane at " << lanelabel2 << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel2.segment);
  //   id.push_back(lanelabel2.lane);
  //   id.push_back(0);
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //     val = mapdata.getRightBound(ptarr,lanelabel2);
  //   cout << "Right lane at " << lanelabel2 << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel2.segment);
  //   id.push_back(lanelabel2.lane);
  //   id.push_back(1);

  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);



  //   val = mapdata.getLeftBoundType(lanetype,lanelabel);
  //   cout << "Left lane type at " << lanelabel << " = " << lanetype << endl;

  //   val = mapdata.getRightBoundType(lanetype,lanelabel);
  //   cout << "Right lane type at " << lanelabel << " = " << lanetype << endl;

  //   val = mapdata.getTransitionBounds(lptarr,rptarr,exitlabel,enterlabel);
  //   cout << "Transition from " << exitlabel 
  //        << " to " << enterlabel 
  //        << ", left: " << lptarr << endl
  //        << "right: " << rptarr<<endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(exitlabel.segment);
  //   id.push_back(exitlabel.lane);
  //   id.push_back(exitlabel.point);
  //   id.push_back(enterlabel.segment);
  //   id.push_back(enterlabel.lane);
  //   id.push_back(enterlabel.point);
  //   id.push_back(0);
  
  //   rawptarr.clear();
  //   rawptarr = lptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //   el.clear();
  //   id.clear();  
  //   id.push_back(exitlabel.segment);
  //   id.push_back(exitlabel.lane);
  //   id.push_back(exitlabel.point);
  //   id.push_back(enterlabel.segment);
  //   id.push_back(enterlabel.lane);
  //   id.push_back(enterlabel.point);
  //   id.push_back(1);
  
  //   rawptarr.clear();
  //   rawptarr = rptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

   
    

  
 
 


 
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
