#include "Map.hh"
#include "LogicalRoadObject.hh"
#include "RoadObject.hh"
#include <iostream>

using namespace std;

namespace Mapper
{
/* Basic constructor */
Map::Map(vector<Segment> initialSegs)
{
	segments = initialSegs;
}
/* Default, static map constructor */
Map::Map()
{

	// TODO: Finish this after Noel sends his map.
	// Create the correct number of blank lane objects.
	// Use the spoof method to populate the lanes.
	// Construct the segment.
	// Populate the map.
	
	// Create the waypoints
	Location loc0 = {396422, 3778147}; // first seg 
	Location loc1 = {396432, 3778147};
	Location loc2 = {396442, 3778147};
	Location loc3 = {396452, 3778147}; // second seg
	Location loc4 = {396462, 3778147};
	Location loc5 = {396472, 3778147};
	
	// Build the centerlines 
	vector<Location> centerline0;
	centerline0.push_back(loc0);
	centerline0.push_back(loc1);
	centerline0.push_back(loc2);
	vector<Location> centerline1;
	centerline1.push_back(loc3);
	centerline1.push_back(loc4);
	centerline1.push_back(loc5);
	
	// Build the lanes, only one per segment
	Lane lane1_1 = Lane(1, centerline0);
	Lane lane2_1 = Lane(1, centerline1);
	vector<Lane> seg1_lanes;
	vector<Lane> seg2_lanes;
	seg1_lanes.push_back(lane1_1);
	seg1_lanes.push_back(lane1_1);
	seg2_lanes.push_back(lane2_1);
	seg2_lanes.push_back(lane2_1);
	
	// Finally, build the segments and add them to the map
	Segment seg1 = Segment(1, seg1_lanes);
	Segment seg2 = Segment(2, seg2_lanes);
	segments.push_back(seg1);
	segments.push_back(seg1); // duplication is not a mistake, has to do with 0 vs 1-indexing
	segments.push_back(seg2);

}
/* RNDF Constructor */
// Constructs a map out of an RNDF object.
// RNDF contains segments, segments contain lanes, lanes contain waypoints 
// Map contains segments, segments contain lanes.
Map::Map(std::RNDF* rndfObj_p)
{
  //std::doodles * dood = new std::doodles();
  //doodles * dood = new doodles();
  //int knood = dood->getKanoodles()
  
  int waypoints = 0;
  int i = 0;
  int j = 0;
  int k = 0;
  std::Waypoint *rndfWaypoint_p;
  std::Segment *rndfSegment_p;
  std::Lane *rndfLane_p;

  //  Segment *segment_p = NULL;
  //  Lane *lane_p = NULL;
  StopLine *stopLine_p = NULL;
  
  for (i=1; i<=rndfObj_p->getNumOfSegments(); i++) {
    cout << "segment " << i << endl;
    vector<Lane> seg_lanes; // Make a new segment
    rndfSegment_p = rndfObj_p->getSegment(i); //Get the segment
    
    for (j=1; j<=rndfSegment_p->getNumOfLanes(); j++) {
      cout << "lane " << j << endl;
      rndfLane_p = rndfSegment_p->getLane(j);
      waypoints = rndfLane_p->getNumOfWaypoints();
      
      vector<Location> centerline;
      vector<StopLine> stopLines;
      for (k=1; k<=waypoints; k++) {
	cout << "waypoint " << k << endl; 
	rndfWaypoint_p = rndfLane_p->getWaypoint(k);
	Location loc = { rndfWaypoint_p->getNorthing(),rndfWaypoint_p->getEasting() };
	centerline.push_back(loc);

	/* If the waypoint is a stopline, create that as well. */
	/* Push the newly made stopline into a vector. */
	if (rndfWaypoint_p->isStopSign()) {
	  stopLine_p = new StopLine("StopLine", rndfWaypoint_p->getNorthing(), rndfWaypoint_p->getEasting());
	  stopLines.push_back(*stopLine_p);
	}
      }
      /* The centerline and stoplines have been constructed. */
      Lane lane = Lane(j, centerline, stopLines);  //Make a new lane object
      seg_lanes.push_back(lane); //put all of the lanes in a segment
      
      if(j==1) {//compensating for 1-indexing
	Lane lane = Lane(j, centerline, stopLines);
	seg_lanes.push_back(lane);
      }
    }
    
    //push that segment onto the vector of segments
    Segment seg = Segment(i, seg_lanes);
    //push that segment on the vector of segments
    segments.push_back(seg);
    
    if (i==1) {//compensating for 1-indexing
      Segment seg = Segment(i, seg_lanes);
      segments.push_back(seg);
    }
  }
  print();
  cout << "Finished constructing the map from an RNDF." << endl;
}

/********************************/
/* Pretty print the map object. */
/********************************/
void Map::print() const
{
  unsigned int i;
	
	std::cout << "Object Map: \n";
	for (i=0; i<segments.size(); i++) 
	{
		segments[i].print();
	}
}

/* Retrieve a segment given its ID. */
Segment Map::getSegment(int segmentID) const 
{
	Segment reqSegment = segments[segmentID]; //The segment requested.
	int reqSegID = reqSegment.getID();
	
	/* Check that the segment ID matches the requested
	 * segment ID. */
	if (reqSegID != segmentID)
		throw "The requested segment ID does not match the actual ID of the segment returned.\n";
		
	return reqSegment;
}
vector<Segment> Map::getAllSegs() const
{
	return segments;
}

/* Destructor */
Map::~Map()
{
}

}
