/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-03-16 23:27:17 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "MapElement.hh"


class Map
{
  
public:
  
  Map();
  
  ~Map();

  bool loadRNDF(string filename) {return prior.loadRNDF(filename);}

  vector<MapElement> data;
  MapPrior prior;

  int addEl(const MapElement &el);
  void initLaneLines();

  int getSegmentID(point2 pt);
  int getLaneID(point2 pt);
  int getZoneID(point2 pt);
  int getNextPointID(PointLabel &label,const point2& pt);
  int getLastPointID(PointLabel &label,const point2& pt);
  int getClosestPointID(PointLabel &label, const point2& pt);

  int getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);

  PointLabel getNextPointID(const point2& pt);
  PointLabel getClosestPointID(const point2& pt);
  
  int getLeftBound(point2arr& ptarr, LaneLabel &label );
  int getRightBound(point2arr& ptarr, LaneLabel &label );

  int getLeftBoundType(string& type, LaneLabel &label);
  int getRightBoundType(string& type, LaneLabel &label);

  int getStopline(point2& pt, PointLabel &label);
  int getNextStopline(point2& pt, PointLabel &label);
  int getStopline(point2& pt, point2 state);
  
  int getWaypoint(point2& pt, PointLabel &label);

  int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel);

  int getRoadBounds(point2arr& leftbound, point2arr& rightbound, const LaneLabel &label,const point2 state, const double range);
  int getBounds(point2arr& leftbound, point2arr& rightbound, const LaneLabel &label,const point2 state, const double range);
 int getBoundsReverse(point2arr& leftbound, point2arr& rightbound, const LaneLabel &label,const point2 state, const double range);
  
  int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel, const point2 pt, const double range);


  int getHeading(double & ang, const PointLabel &label);
 int getHeading(double & ang, const point2 &pt);


  double getObstacleDist(const point2& state,const int lanedelta=0);
  point2 getObstaclePoint(const point2& state, const double offset=0);
  int getObstacleGeometry(point2arr& ptarr);

  bool isStop(PointLabel &label);
  bool isExit(PointLabel &label);


};



#endif
 
