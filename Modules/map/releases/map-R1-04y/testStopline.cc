/**********************************************************
 **
 **  TESTSTOPLINE.CC
 **
 **    Time-stamp: <2007-10-26 09:31:45 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Oct 25 21:15:23 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "map/Map.hh"
#include <iostream>
#include <string>
#include "map/MapElementTalker.hh"
#include "map/MapElement.hh"
#include "skynettalker/StateClient.hh"
#include "interfaces/sn_types.h"
#include "dgcutils/DGCutils.hh"

using namespace std;

class CStateWrapper : public CStateClient
{
public:
  CStateWrapper(int snkey) : CSkynetContainer(MODmapping,snkey),CStateClient(false) { };
  ~CStateWrapper() {};
  
  VehicleState getstate() {UpdateState();return m_state;}

};

int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroupViewer = -2;
  int recvSubGroup = -1;
  string fname;


  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testStopline [RNDF name] [seg id] [lane id] [pt id] [p1 dx] [p1 dy]  [p2 dx] [p2 dy]" << endl << endl;

  int segid,laneid,ptid;
  double p1x,p1y,p2x,p2y;

  if(argc <= 8) {
    cerr << "invalid args " << endl; return 0;
  }
    
  fname = argv[1];
  segid = atoi(argv[2]);
  laneid = atoi(argv[3]);
  ptid = atoi(argv[4]);
  p1y = atof(argv[5]);
  p2y = atof(argv[6]);
  p1x = atof(argv[7]);
  p2x = atof(argv[8]);


  cout << "RNDF filename = " << fname 
       << "  Send Subgroup Viewer = "  << sendSubGroupViewer 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  cout << "Connecting to State " << endl;
  CStateWrapper stateclient(skynetKey);
  cout << "Got State " << endl;


  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);

  
	maptalker.initRecvMapElement(skynetKey, recvSubGroup);

  Map mapdata(false);

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };

  
  VehicleState state;
  MapElement el;
  
 
  usleep(100000);

  state = stateclient.getstate();
 
  point2 statedelta, stateNE, statelocal;
  cout << "tstamp" <<state.timestamp << endl;
  if (state.timestamp!=0){

    statelocal.set(state.localX,state.localY);
    stateNE.set(state.utmNorthing,state.utmEasting);
    statedelta = stateNE-statelocal;
    cout << "state delta = " << statedelta << " northing = " << stateNE << " local = " << statelocal << endl;
    cout << "predelta = " << mapdata.prior.delta << endl;
    mapdata.setLocalToGlobalOffset(state);
		cout << "postdelta = " << mapdata.prior.delta << endl;
  }    

	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;  
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getElFull(el,i);
    //TEMP 
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,-2);
	}


	MapElement sendEl, dbgEl;

  PointLabel ptlabel(segid,laneid,ptid);

  point2 basept, dpt1,dpt2;
  point2 retpt;
  point2arr points;
  
  double heading;
  int status = mapdata.getStopLineSensed(basept,ptlabel);

  if (status <0){
    cerr << "Couldn't find stopline at " << ptlabel << endl;
    return 0;
  }
  cout << "basept = " << basept << endl;
  basept.x = basept.x-mapdata.loc2site.x;
  basept.y = basept.y-mapdata.loc2site.y;
  cout << "basepo = " << basept << endl;


  mapdata.getHeading(heading,ptlabel);
  cout << "heading = " << heading << endl;

  dpt1.set(p1x,p1y);
  dpt2.set(p2x,p2y);
  dpt1 = dpt1.rot(heading);
  dpt2 = dpt2.rot(heading);


  points.clear();
  points.push_back(basept+dpt1);
  points.push_back(basept+dpt2);

  sendEl.clear();
  sendEl.setGeometry(points);
  sendEl.height = 0;
  sendEl.plotColor = MAP_COLOR_RED;
  sendEl.geometryType = GEOMETRY_LINE;
  sendEl.type = ELEMENT_STOPLINE;
  sendEl.setId(-1980,1);
  sendEl.state = state;

  //send
  maptalker.sendMapElement(&sendEl, 0);



  mapdata.addEl(sendEl,true);

  status = mapdata.getStopLineSensed(retpt,ptlabel);
  cout << "retpt = " << retpt << endl;
  dbgEl.setGeometry(retpt);
  dbgEl.setColor(MAP_COLOR_GREEN);
  dbgEl.setTypePoints();
  dbgEl.setId(-1980,-10);
  maptalker.sendMapElement(&dbgEl,-2);


};
