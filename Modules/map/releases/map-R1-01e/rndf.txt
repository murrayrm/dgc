RNDF_name	St_Lukes_RNDF_Full_Map	
num_segments	5	
num_zones	0	
format_version	1.0	
creation_date	3/1/2007

segment	1	
num_lanes	2	
segment_name	Fast_Lane	
lane	1.1	
num_waypoints	8	
lane_width	12	
left_boundary	double_yellow
right_boundary	solid_white	
checkpoint 1.1.2		1
checkpoint 1.1.6		2
stop	1.1.4
stop	1.1.8
exit	1.1.4	4.2.1
exit	1.1.4	1.1.5
exit	1.1.6	2.2.1
exit	1.1.6	1.1.7
1.1.1	34.167452	-118.095284
1.1.2	34.167442	-118.095699
1.1.3	34.167441	-118.095927
1.1.4	34.167437	-118.096022
1.1.5	34.167435	-118.096162
1.1.6	34.167421	-118.096882
1.1.7	34.167420	-118.097017
1.1.8	34.167418	-118.097081
end_lane		
lane	1.2	
num_waypoints	8	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white
checkpoint	1.2.4		3
checkpoint	1.2.7		4
stop	1.2.8
exit	1.2.2	2.2.1
exit	1.2.2	1.2.3
exit	1.2.4	4.2.1
exit	1.2.4	1.2.5
exit	1.2.8	5.2.2
1.2.1	34.167381	-118.097077
1.2.2	34.167381	-118.097019
1.2.3	34.167383	-118.096885
1.2.4	34.167398	-118.096158
1.2.5	34.167400	-118.096019
1.2.6	34.167403	-118.095926
1.2.7	34.167405	-118.095702
1.2.8	34.167415	-118.095279
end_lane		
end_segment		

segment	2
num_lanes	2	
segment_name	North_Street	
lane	2.1	
num_waypoints	3	
lane_width	12	
left_boundary	double_yellow	
stop	2.1.3	
exit	2.1.3	1.1.7
exit	2.1.3	1.2.3
2.1.1	34.167657	-118.096984
2.1.2	34.167569	-118.096983
2.1.3	34.167453	-118.096983
end_lane		
lane	2.2	
num_waypoints	3
lane_width	12	
left_boundary	double_yellow	
exit	2.2.3	3.1.3
exit	2.2.3	3.2.3
2.2.1	34.167453	-118.096928
2.2.2	34.167568	-118.096929
2.2.3	34.167658	-118.096927
end_lane		
end_segment		

segment	3	
num_lanes	2	
segment_name	Murray_Road	
lane	3.1	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
checkpoint	3.1.2		5
stop	3.1.2
exit	3.1.2	2.1.1
exit	3.1.2	3.1.3
3.1.1	34.167735	-118.096182
3.1.2	34.167723	-118.096882
3.1.3	34.167724	-118.097020
3.1.4	34.167724	-118.097081
end_lane		
lane	3.2	
num_waypoints	4	
lane_width	12	
left_boundary	double_yellow	
right_boundary	solid_white	
checkpoint 3.2.4		6
stop	3.2.2
stop	3.2.4	
exit	3.2.2	2.1.1
exit	3.2.2	3.2.3
exit	3.2.4	4.1.9
exit	3.2.4	4.2.5
3.2.1	34.167688	-118.097086
3.2.2	34.167688	-118.097023
3.2.3	34.167694	-118.096882
3.2.4	34.167702	-118.096180
end_lane		
end_segment		

segment	4
num_lanes	2	
segment_name	St_Luke_Lane	
lane	4.1	
num_waypoints	12	
lane_width	12	
left_boundary	double_yellow	
checkpoint	4.1.3		7
checkpoint	4.1.10		8
stop	4.1.8
stop	4.1.12	
exit	4.1.8	3.1.1
exit	4.1.8	4.1.9
exit	4.1.12	1.1.5
exit	4.1.12	1.2.5
4.1.1	34.168000	-118.095329
4.1.2	34.167996	-118.095642
4.1.3	34.167996	-118.095742
4.1.4	34.167979	-118.096027
4.1.5	34.167924	-118.096100
4.1.6	34.167896	-118.096115
4.1.7	34.167818	-118.096135
4.1.8	34.167766	-118.096135
4.1.9	34.167672	-118.096134
4.1.10	34.167579	-118.096131
4.1.11	34.167498	-118.096126
4.1.12	34.167479	-118.096126
end_lane		
lane	4.2	
num_waypoints	12	
lane_width	12	
left_boundary	double_yellow	
stop	4.2.4
checkpoint 	4.2.3		9
checkpoint 	4.2.7		10
exit	4.2.4	3.1.1
exit	4.2.4	4.2.5
exit	4.2.12	5.1.1
4.2.1	34.167480	-118.096069
4.2.2	34.167497	-118.096069
4.2.3	34.167581	-118.096072
4.2.4	34.167671	-118.096073
4.2.5	34.167765	-118.096077
4.2.6	34.167822	-118.096078
4.2.7	34.167892	-118.096064
4.2.8	34.167920	-118.096041
4.2.9	34.167962	-118.095956
4.2.10	34.167976	-118.095753
4.2.11	34.167977	-118.095634
4.2.12	34.167981	-118.095346
end_lane		
end_segment		

segment	5	
num_lanes	2	
segment_name	Winding_Way	
lane	5.1	
num_waypoints	11	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	5.1.6		11
stop 	5.1.10
exit	5.1.10	1.1.1
5.1.1	34.167949	-118.095312
5.1.2	34.167883	-118.095312
5.1.3	34.167822	-118.095324
5.1.4	34.167769	-118.095351
5.1.5	34.167692	-118.095374
5.1.6	34.167634	-118.095375
5.1.7	34.167584	-118.095342
5.1.8	34.167553	-118.095303
5.1.9	34.167521	-118.095263
5.1.10	34.167488	-118.095226
5.1.11	34.167354	-118.095204
end_lane		
lane	5.2	
num_waypoints	11	
lane_width	12	
left_boundary	broken_white	
right_boundary	solid_white	
checkpoint	5.2.7		12
exit	5.2.11	4.1.1
5.2.1	34.167353	-118.095156
5.2.2	34.167486	-118.095175
5.2.3	34.167520	-118.095204
5.2.4	34.167558	-118.095251
5.2.5	34.167593	-118.095292
5.2.6	34.167636	-118.095325
5.2.7	34.167692	-118.095332
5.2.8	34.167769	-118.095304
5.2.9	34.167816	-118.095287
5.2.10	34.167882	-118.095279
5.2.11	34.167948	-118.095280
end_lane		
end_segment		
end_file