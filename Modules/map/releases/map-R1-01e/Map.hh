/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-03-08 16:45:53 ndutoit> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "interfaces/MapElement.hh"


class Map
{
	
public:
	
	Map();
	
	~Map();

	bool loadRNDF(string filename) {return prior.loadRNDF(filename);}

	vector<MapElement> data;
	MapPrior prior;

	void initLaneLines();

	int getSegmentID(point2 pt);
	int getLaneID(point2 pt);
	int getZoneID(point2 pt);
	PointLabel getNextPointID(point2 pt);


	int getLeftBound(point2arr& ptarr, LaneLabel &label );
	int getRightBound(point2arr& ptarr, LaneLabel &label );

int getLeftBoundType(string& type, LaneLabel &label);
int getRightBoundType(string& type, LaneLabel &label);

int getStopline(point2& pt, PointLabel &label);
int getStopline(point2& pt, point2 state);
	
int getWaypoint(point2& pt, PointLabel &label);

int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel);


int getObstacleGeometry(point2arr& ptarr);

};



#endif
 
