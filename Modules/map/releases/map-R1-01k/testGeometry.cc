/**********************************************************
 **
 **  TESTGEOMETRY.CC
 **
 **    Time-stamp: <2007-03-13 14:11:23 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Mar 13 13:29:56 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "frames/point2.hh"
#include "frames/point2_uncertain.hh"

using namespace std;


int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testGeometry [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  int argnum = 1;
  if(argc >argnum)
    testnum = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    sendSubGroup = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    recvSubGroup = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    skynetKey = atoi(argv[argnum]);
  argnum++;
  
  cout << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey);


  cout <<"========================================" << endl;
  testname = "Testing Point2";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl;
    
    sendEl.set_id(-1);
    sendEl.set_points();
    sendEl.plot_color = COLOR_MAGENTA;

    int retval =0;
  
    point2 statept;
    point2 pt;
    point2arr ptarr;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        ptarr.clear();
        ptarr.push_back(statept);
        sendEl.set_geometry(ptarr);
        cout << "sent geometry " << endl;
        sendEl.print();
        maptalker.sendMapElement(&sendEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  
  cout <<"========================================" << endl;
  testname = "Testing point2arr.project(const point2& pt)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.set_points();
    sendEl2.plot_color = COLOR_GREEN;

    int retval =0;
  
    point2 statept;
    point2 pt;
    point2arr ptarr;

    ptarr.push_back(point2(1,-1));
    ptarr.push_back(point2(2,-3));
    ptarr.push_back(point2(3,-4));
    ptarr.push_back(point2(4,-2));
    ptarr.push_back(point2(5,-6));
    sendEl1.set_geometry(ptarr);
    
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        pt =ptarr.project(statept);
        
        sendEl2.set_geometry(pt);
        maptalker.sendMapElement(&sendEl1,sendSubGroup);
        maptalker.sendMapElement(&sendEl2,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  



  
 
 


 
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
