/**********************************************************
 **
 **  MAP.CC
 **
 **    Time-stamp: <2007-03-01 14:47:25 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:45 2007
 **
 **
 **********************************************************
 **
 ** Map Class  
 **
 **********************************************************/

#include "Map.hh"

using namespace std;
 
Map::Map()
{}

Map::~Map()
{}

void initLaneLines()
{
	
}

int Map::getSegmentID(point2 pt)
{
	

	return -1;
}

int Map::getLaneID(point2 pt)
{
	return -1;
}

int Map::getZoneID(point2 pt)
{
	return -1;
}


int Map::getLeftBound(point2arr& ptarr, LaneLabel &label )
{
	unsigned int i,j;
	MapElement el;
	int elindex =-1;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label == label){
			elindex = prior.lanes[i].leftbound_elindex;
			prior.getEl(elindex,el);
			ptarr = el.geometry;
		}
	}
	return 0;
}

int Map::getRightBound(point2arr& ptarr, LaneLabel &label )
{
	unsigned int i,j;
	MapElement el;
	int elindex =-1;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label == label){
			elindex = prior.lanes[i].rightbound_elindex;
			prior.getEl(elindex,el);
			ptarr = el.geometry;
		}
	}
	return 0;
}


int Map::getLeftBoundType(string& type, LaneLabel &label)
{
	unsigned int i,j;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label==label){
			type = prior.lanes[i].leftbound_type;
			return 0;
		}
	}
	return -1;
}
int Map::getRightBoundType(string& type, LaneLabel &label )
{
	unsigned int i,j;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label == label){
			type = prior.lanes[i].rightbound_type;
			return 0;
		}
	}
	return -1;

}

int Map::getStopline(point2& pt, PointLabel &label)
{
	unsigned int i,j;
	int elindex =-1;
	int ptindex =-1;
	bool isstop = false;
	MapElement el;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label == label){
			elindex = prior.lanes[i].waypoint_elindex;
			for (j=0;j<prior.lanes[i].stop_label.size();++j){
				if (prior.lanes[i].stop_label[j]==label){
					isstop = true;
				}
			}
			if (!isstop)
				continue;

			for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
				if (prior.lanes[i].waypoint_label[j]==label){
					
					ptindex =j;
					prior.getEl(elindex,el);
					pt = el.geometry[ptindex];
					return 0;
				}
			}
		}
	}
	cerr << "in Map::getStopline, stop line not found" << label << endl;
	
	return -1;
}

int Map::getWaypoint(point2& pt, PointLabel &label)
{
	unsigned int i,j;
	int elindex =-1;
	int ptindex =-1;
	MapElement el;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label == label){
			elindex = prior.lanes[i].waypoint_elindex;
			for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
				if (prior.lanes[i].waypoint_label[j]==label){
					ptindex =j;
					prior.getEl(elindex,el);
					pt = el.geometry[ptindex];

					return 0;
				}
			}
		}
	}
	cerr << "in Map::getWaypoint, waypoint not found" << label <<endl;
	return -1;

}

int Map::getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel)
{
	unsigned int i,j;
	int exit_elindex =-1;
	int exit_leftelindex =-1;
	int exit_rightelindex =-1;
	int exit_ptindex =-1;
	int enter_elindex =-1;
	int enter_leftelindex =-1;
	int enter_rightelindex =-1;

	int enter_ptindex =-1;
	
	point2 exitpt;
	point2 enterpt;
	MapElement el;

	bool isvalid = false;
	for (i=0;i<prior.lanes.size(); ++i){
		if (prior.lanes[i].label== exitlabel){
		
			for (j=0;j<prior.lanes[i].exit_label.size();++j){
				if (prior.lanes[i].exit_label[j]==exitlabel &&
						prior.lanes[i].exit_link[j]==enterlabel){
				

					isvalid = true;
				}
			}
			if (!isvalid)
				continue;
			
			for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
				if (prior.lanes[i].waypoint_label[j]==exitlabel){	exit_elindex = prior.lanes[i].waypoint_elindex;
				exit_leftelindex = prior.lanes[i].leftbound_elindex;
				exit_rightelindex = prior.lanes[i].rightbound_elindex;
				exit_ptindex =j;
				break;
				}
			}
		}
	}
	if (isvalid){
		for (i=0;i<prior.lanes.size(); ++i){
			if (prior.lanes[i].label== enterlabel){
		
				for (j=0;j<prior.lanes[i].waypoint_label.size();++j){
					if (prior.lanes[i].waypoint_label[j]==enterlabel){
						enter_elindex = prior.lanes[i].waypoint_elindex;
						enter_leftelindex = prior.lanes[i].leftbound_elindex;
						enter_rightelindex = prior.lanes[i].rightbound_elindex;
						enter_ptindex =j;
						break;
					}
				}
			}
		}
		prior.getEl(enter_leftelindex,el);
		enterpt = el.geometry[enter_ptindex];	
		prior.getEl(exit_leftelindex,el);
		exitpt = el.geometry[exit_ptindex];	

		leftbound.clear();
		leftbound.push_back(enterpt);
		leftbound.push_back(exitpt);

		prior.getEl(enter_rightelindex,el);
		enterpt = el.geometry[enter_ptindex];	
		prior.getEl(exit_rightelindex,el);
		exitpt = el.geometry[exit_ptindex];	

		rightbound.clear();
		rightbound.push_back(enterpt);
		rightbound.push_back(exitpt);
	
		return 0;
	}


	cerr << "in Map::getTransitionBounds, invalid transition, exit = " << exitlabel << " enter = " << enterlabel <<endl;
	return -1;
}





int Map::getObstacleGeometry(point2arr& ptarr)
{
	return 0;
}
