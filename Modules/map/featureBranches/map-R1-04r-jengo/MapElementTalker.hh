/**********************************************************
 **
 **  CMAPELEMENTTALKER.HH
 **
 **    Time-stamp: <2007-09-30 01:23:39 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:05:08 2007
 **
 **
 **********************************************************
 **
 **  Implements a talker for a map element
 **
 **********************************************************/


#ifndef MAPELEMENTTALKER_HH
#define MAPELEMENTTALKER_HH

#include <iostream>
#include "skynet/sn_msg.hh"//"SkynetContainer.hh"
#include "interfaces/sn_types.h"
#include "MapElement.hh"
#include "interfaces/MapElementMsg.h"
#include <sys/poll.h>
#include <sp.h>				// Spread header file

using namespace std;
//! Implements a talker for a map element
class CMapElementTalker
{
  
public:
  
  //! A Constructor 
  CMapElementTalker();
  
  //! A Destructor 
  ~CMapElementTalker() {}

  int initSendMapElement(int snkey, modulename snname = MODmapping);
  int initRecvMapElement(int snkey, modulename snname = MODmapping);
  int initRecvMapElement(int snkey, int subgroup, modulename snname = MODmapping);

  int sendMapElement(MapElement* pMapElement, int subgroup = 0);
    
  int recvMapElementBlock(MapElement* pMapElement,int subgroup = 0);
  int recvMapElementNoBlock(MapElement* pMapElement,int subgroup = 0);
	int recvMapElementTimedBlock(MapElement* pMapElement, int delay, int subgroup = 0);
  int recvMapElementTimedBlockLossy(MapElement* pMapElement, int delay, int subgroup = 0);

  int getDropCount() {return dropcount;}

private:
  int setMapElementMsgOut(MapElement* pMapElement);  
  int setMapElementMsgIn(MapElement* pMapElement);  

  // Spread mail box
  int mbox;  
	struct pollfd fd;
  
  int mapElementSendSocket;
  MapElementMsg mapElementMsgOut;

  int mapElementRecvSocket;
  MapElementMsg mapElementMsgIn;

  skynet * skynet_ptr;
  int basemsgsize;
  int baseptsize;

  long dropcount;
  
  int group;
  int key;
  modulename name;
  point2_uncertain pt;
  point2_uncertain_struct ptmsg;
};
#endif
