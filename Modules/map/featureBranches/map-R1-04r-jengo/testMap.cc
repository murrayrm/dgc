/**********************************************************
 ** 
 **  TESTMAP.CC
 **
 **    Time-stamp: <2007-10-07 14:43:04 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Mar  1 12:53:36 2007
 **
 **
 **********************************************************
 **
 **      
 **
 **********************************************************/
                                  
#include "Map.hh"
#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"

using namespace std;
 
 
int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;
  int count = 0;
  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testMap [rndf filename] [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;
  
  if(argc >1)
    fname = argv[1];
  if(argc >2)
    testnum = atoi(argv[2]);
  if(argc >3)
    sendSubGroup = atoi(argv[3]);
  if(argc >4)
    recvSubGroup = atoi(argv[4]);
  if(argc >5)
    skynetKey = atoi(argv[5]);
  
  cout << "RNDF filename = " << fname 
       << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey,recvSubGroup);

  Map mapdata;
  //  mapdata.loadRNDF("rn.txt");

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };
 
  MapElement el;
	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;  
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getEl(el,i);
    //TEMP 
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,sendSubGroup);
	}

  MapElement recvEl;
  MapElement clearEl;

  MapElement pointEl, pointEl1, pointEl2, 
    lineEl, lineEl1, lineEl2, polyEl, polyEl1;
  
  int baseID = 88;

  pointEl.setId(baseID,1);
  pointEl.setTypePoints();
  pointEl.plotColor = MAP_COLOR_MAGENTA;
  
  pointEl1.setId(baseID,2);
  pointEl1.setTypePoints();
  pointEl1.plotColor = MAP_COLOR_RED;
  
  pointEl2.setId(baseID,3);
  pointEl2.setTypePoints();
  pointEl2.plotColor = MAP_COLOR_GREEN;
  
  lineEl.setId(baseID,4);
  lineEl.setTypeLine();
  lineEl.plotColor = MAP_COLOR_RED;
   
  lineEl1.setId(baseID,5);      
  lineEl1.setTypeLine();
  lineEl1.plotColor = MAP_COLOR_CYAN;
  
  lineEl2.setId(baseID,6);
  lineEl2.setTypeLine();
  lineEl2.plotColor = MAP_COLOR_GREEN;
  
  polyEl.setId(baseID,7);
  polyEl.setTypePoly();
  polyEl.plotColor = MAP_COLOR_YELLOW;
  
  polyEl1.setId(baseID,8);
  polyEl1.setTypePoly();
  polyEl1.plotColor = MAP_COLOR_CYAN;
    
    
  clearEl.setTypeClear();

  int retval;
  double val;
  PointLabel label,label1,label2,statelabel;

  LaneLabel llabel,llabelout,lanelabel;
  point2 statept,stoppt,pt;
  point2_uncertain selectpt;

  point2arr ptarr,ptarr1,ptarr2;
  point2arr_uncertain ptarr_unc,ptarr1_unc,ptarr2_unc;
  vector<PointLabel> ptlabelarr, ptlabelarr2;
  vector<LaneLabel> llabelarr;
  vector<int> intarr;
  
 

  cout <<"========================================" << endl;
  testname = "Testing Map::is EntryPoint ExitPoint StopLine CheckPoint ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    while (true){ 
 
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdatagetPointEntryLabels(ptlabelarr,label);
        //retval = mapdatagetPointArr(ptarr,ptlabelarr);
        pointEl.setGeometry(selectpt);
        maptalker.sendMapElement(&pointEl,sendSubGroup);
       
        if(mapdata.isEntryPoint(label)){
          cout <<" isEntryPoint() returned true for "<< label  << endl;
        }else{
          cout <<" isEntryPoint() returned FALSE for "<< label  << endl;
        }
       
        if(mapdata.isExitPoint(label)){
          cout <<" isExitPoint() returned true for "<< label  << endl;
        }else{
          cout <<" isExitPoint() returned FALSE for "<< label  << endl;
        }

        if(mapdata.isStopLine(label)){
          cout <<" isStopLine() returned true for "<< label  << endl;
        }else{
          cout <<" isStopLine() returned FALSE for "<< label  << endl;
        }

        if(mapdata.isCheckPoint(label)){
          cout <<" isCheckPoint() returned true for "<< label  << endl;
        }else{
          cout <<" isCheckPoint() returned FALSE for "<< label  << endl;
        }

       
 
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
  cout <<"========================================" << endl;
  testname = "Testing Map::getWayPoint Entries Exits";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getPointArr(ptarr,ptlabelarr);
        pointEl.setGeometry(selectpt);
        maptalker.sendMapElement(&pointEl,sendSubGroup);
 
        retval = mapdata.getWayPointEntries(ptlabelarr,label);
        
        if (retval>0){
          cout <<  "Point " << label << " is an entry from labels : " << ptlabelarr <<  endl;
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "entry from points : " << ptarr << endl;
          pointEl2.setGeometry(ptarr);
          maptalker.sendMapElement(&pointEl2,sendSubGroup);
        }else{
          clearEl.setId(pointEl2.id);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
        
        
        retval = mapdata.getWayPointExits(ptlabelarr,label);
        if (retval>0){

          cout << "Point " << label << " can exit to labels : " << ptlabelarr <<  endl;
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "exit to points : " << ptarr << endl;
          pointEl1.setGeometry(ptarr);
          maptalker.sendMapElement(&pointEl1,sendSubGroup);
        }else{
          clearEl.setId(pointEl1.id);  
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
      } 
      else{   
        usleep (100000); 
      }
    } 
  } 
    
  cout <<"========================================" << endl;
  testname = "Testing Map::getLane LeftBound RightBound WayPoints";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        pointEl.setGeometry(selectpt);

        maptalker.sendMapElement(&pointEl,sendSubGroup);
 
        retval = mapdata.getLaneLeftBound(ptarr,llabel);
        cout << "left bound : "<< ptarr <<  "retval = " << retval <<endl;
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_GREEN;
          lineEl.setGeometry(ptarr);
          lineEl.setId(baseID,2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(baseID,2);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }


        retval = mapdata.getLaneRightBound(ptarr,llabel);
        cout << "right bound : "<< ptarr <<  "retval = " << retval <<endl;
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_RED;
          lineEl.setGeometry(ptarr);
          lineEl.setId(baseID,3);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(baseID,3);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
         
        retval = mapdata.getLaneCenterLine(ptarr,llabel);
        cout << "center line : "<< ptarr <<  "retval = " << retval <<endl;
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_YELLOW;
          lineEl.setGeometry(ptarr);
          lineEl.setId(baseID,4);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(baseID,4);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
 

      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
   
  cout <<"========================================" << endl;
  testname = "Testing Map::getLane ExitLabels ExitToLabels EntryLabels EntryFromLabels";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    count = 0;
    unsigned int lastsize=0;;
    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        pointEl.plotColor = MAP_COLOR_MAGENTA;
        pointEl.setGeometry(selectpt);
        pointEl.setId(baseID,1);

        maptalker.sendMapElement(&pointEl,sendSubGroup);
 
        if (count==0){
          count = 1;
        
          retval = mapdata.getLaneExits(ptlabelarr,ptlabelarr2,llabel);
          cout << "Exit labels : "<< ptlabelarr << endl;         
          cout << "Exit To labels : "<< ptlabelarr2 <<  "retval = " << retval <<endl;  
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "Exit points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.getWayPoint(ptarr2,ptlabelarr2);
          cout << "Exit To points : " << ptarr2 << "retval = " << retval <<endl;

          if (ptarr.size() != ptarr2.size()){
            cerr << "SIZE MISMATCH FOR EXIT LABELS - ERROR PARSING" << endl;
            continue;
          }
        }else{
          count = 0;

          retval = mapdata.getLaneEntries(ptlabelarr,ptlabelarr2,llabel);
          cout << "Entry labels : "<< ptlabelarr << endl;         
          cout << "Entry To labels : "<< ptlabelarr2 <<  "retval = " << retval <<endl;  
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "Entry points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.getWayPoint(ptarr2,ptlabelarr2);
          cout << "Entry To points : " << ptarr2 << "retval = " << retval <<endl;

        }
        
        for (unsigned int j =ptarr.size(); j<lastsize;++j){
          clearEl.setId(baseID,15+j);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
        cout<< "cleared" << endl;
        lastsize = ptarr.size();
        for (unsigned int j =0; j<ptarr.size();++j){
          lineEl.plotColor = MAP_COLOR_YELLOW;
          lineEl.setGeometry(ptarr[j],ptarr2[j]);
          lineEl.setId(baseID,15+j);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }
        pointEl1.setGeometry(ptarr);
        maptalker.sendMapElement(&pointEl1,sendSubGroup);

        pointEl2.setGeometry(ptarr2);
        maptalker.sendMapElement(&pointEl2,sendSubGroup);
        cout << "Select a point" << endl;
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
   
  cout <<"========================================" << endl;
  testname = "Testing Map::getZone ExitLabels ExitToLabels EntryLabels EntryFromLabels";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    count = 0;
    int zonelabel;
    unsigned int lastsize=0;;
    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getWayPointArr(ptarr,ptlabelarr);

        zonelabel = label.segment;

        pointEl.plotColor = MAP_COLOR_MAGENTA;
        pointEl.setGeometry(selectpt);
        pointEl.setId(baseID,1);

        maptalker.sendMapElement(&pointEl,sendSubGroup);
 
        if (count==0){
          count = 1;
        
          retval = mapdata.getZoneExits(ptlabelarr,ptlabelarr2,zonelabel);
          cout << "Exit labels : "<< ptlabelarr << endl;         
          cout << "Exit To labels : "<< ptlabelarr2 <<  "retval = " << retval <<endl;  
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "Exit points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.getWayPoint(ptarr2,ptlabelarr2);
          cout << "Exit To points : " << ptarr2 << "retval = " << retval <<endl;

          if (ptarr.size() != ptarr2.size()){
            cerr << "SIZE MISMATCH FOR EXIT LABELS - ERROR PARSING" << endl;
            continue;
          }
        }else{
          count = 0;

          retval = mapdata.getZoneEntries(ptlabelarr,ptlabelarr2,zonelabel);
          cout << "Entry labels : "<< ptlabelarr << endl;         
          cout << "Entry To labels : "<< ptlabelarr2 <<  "retval = " << retval <<endl;  
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "Entry points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.getWayPoint(ptarr2,ptlabelarr2);
          cout << "Entry To points : " << ptarr2 << "retval = " << retval <<endl;

        }
        
        for (unsigned int j =ptarr.size(); j<lastsize;++j){
          clearEl.setId(baseID,15+j);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
        cout<< "cleared" << endl;
        lastsize = ptarr.size();
        for (unsigned int j =0; j<ptarr.size();++j){
          lineEl.plotColor = MAP_COLOR_YELLOW;
          lineEl.setGeometry(ptarr[j],ptarr2[j]);
          lineEl.setId(baseID,15+j);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }
        pointEl1.setGeometry(ptarr);
        maptalker.sendMapElement(&pointEl1,sendSubGroup);

        pointEl2.setGeometry(ptarr2);
        maptalker.sendMapElement(&pointEl2,sendSubGroup);
        cout << "Select a point" << endl;
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 

  cout <<"========================================" << endl;
  testname = "Testing Map::getLane StopLineLabels CheckPointLabels CheckPointNumbers";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        pointEl.setGeometry(selectpt);
        maptalker.sendMapElement(&pointEl,sendSubGroup);
 
        if (count==0){
          count = 1;
        
          retval = mapdata.getLaneStopLines(ptlabelarr,llabel);
          cout << "Stop line labels : "<< ptlabelarr <<  "retval = " << retval <<endl;          
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "Stop line points : " << ptarr << "retval = " << retval <<endl;
          pointEl1.plotColor = MAP_COLOR_RED;              
          
        }else{
          count = 0; 
          
          retval = mapdata.getLaneCheckPoints(ptlabelarr,llabel);
          cout << "Check point labels : "<< ptlabelarr <<  "retval = " << retval <<endl;          
          retval = mapdata.getWayPoint(ptarr,ptlabelarr);
          cout << "Check point points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.getLaneCheckPointNumbers(intarr,llabel);
          for (unsigned int j=0;j<intarr.size();++j){
            cout << "label " <<ptlabelarr[j] << "  num " << intarr[j] << endl;
          }
          pointEl1.plotColor = MAP_COLOR_GREEN;
        }
        if (ptarr.size()>0){
          pointEl1.setGeometry(ptarr);
          maptalker.sendMapElement(&pointEl1,sendSubGroup);
        }else{
          clearEl.setId(pointEl1.id);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }

      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
  

  cout <<"========================================" << endl;
  testname = "Testing Map::getLaneLabelNeighbor";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        pointEl.setGeometry(selectpt);

        maptalker.sendMapElement(&pointEl,sendSubGroup);
 
        if (count==0){  
          //count = 1;
         
          retval = mapdata.getNeighborLane(llabelout,llabel,-1);
          cout << "lane label : "<< llabelout <<  "retval = " << retval <<endl;          
          retval = mapdata.getLaneBounds(ptarr1,ptarr2,llabelout);
          cout <<"retval " << retval <<endl;    
          cout << "left bound = " << ptarr1
               << " right bound = " << ptarr2 << endl;

          
        }else{
          count = 0; 
            
        }
        // retval = -1;    
        if (retval>=0){
          lineEl2.setGeometry(ptarr1);
          maptalker.sendMapElement(&lineEl2,sendSubGroup);
          lineEl.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(lineEl2.id);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
          clearEl.setId(lineEl.id); 
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
      }   
      else{ 
        usleep (100000);  
      }
    }   
  }  
   


  cout <<"========================================" << endl;
  testname = "Testing Map::getLane DirLabels Bounds";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    while (true){ 
 
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        pointEl.setGeometry(selectpt);

        maptalker.sendMapElement(&pointEl,sendSubGroup);
 

         
        retval = mapdata.getSameDirLanes(llabelarr,llabel);
        cout << "Same Dir label arr : "<< llabelarr <<  "retval = " << retval <<endl;          

        retval = mapdata.getOppDirLanes(llabelarr,llabel);
        cout << "Opposite Dir label arr : "<< llabelarr <<  "retval = " << retval <<endl;          

        retval = mapdata.getAllDirLanes(llabelarr,llabel);
        cout << "Any Dir label arr : "<< llabelarr <<  "retval = " << retval <<endl;          
 
        if (count==0){ 
          count = 1;                                   
          retval = mapdata.getSegmentBounds(ptarr1,ptarr2,llabel);
          cout << "Get Segment bounds retval = " << retval <<endl;          
        } else if (count==1){
          count = 2;                                   
          retval = mapdata.getSameDirLaneBounds(ptarr1,ptarr2,llabel);
          cout << "Get Same Direction bounds retval = " << retval <<endl;          
       
        }else{
          count = 0; 
          retval = mapdata.getOppDirLaneBounds(ptarr1,ptarr2,llabel);
          cout << "Get Opposite Direction bounds retval = " << retval <<endl;          
          
        }
        if (retval>=0){
          lineEl.setGeometry(ptarr1);
          lineEl.setId(baseID,2);
          lineEl.plotColor = MAP_COLOR_GREEN;
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          lineEl.setGeometry(ptarr2);
          lineEl.plotColor = MAP_COLOR_RED;
          lineEl.setId(baseID,3);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(baseID,2);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
          clearEl.setId(baseID,3);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
  
  cout <<"========================================" << endl;
  testname = "Testing Map::getLane()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    cout << "getting closest lane to selected point" <<endl;    
    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout  <<"For point " << statept << endl;
        retval=  mapdata.getLane(llabel,statept);
        cout << "getLane returns label = " << llabel << endl;   
        retval = mapdata.getLaneCenterLine(ptarr,llabel);
       
        lineEl.setGeometry(ptarr);
        maptalker.sendMapElement(&lineEl,sendSubGroup);
        cout << endl <<"getting closest lane to selected point" <<endl;
      } 
      else{ 
        usleep (100000);
      }
    }
  }
      
  cout <<"========================================" << endl;
  testname = "Testing Map::getDistAlongLine()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    double val;

    cout << "Getting closest lane to selected point" <<endl;    
    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout  <<"For point " << statept << endl;
        retval=  mapdata.getLane(llabel,statept);
        cout << "getLane returns label = " << llabel << endl;   
        retval = mapdata.getLaneCenterLine(ptarr,llabel);

        retval= mapdata.getDistAlongLine(val,ptarr,statept);
        cout << "DistAlongLine returned  " <<val << endl;

        retval = mapdata.getPointAlongLine(pt,ptarr,val+10);
        cout << "getPointAlongLine returned " << pt << endl;

        pointEl1.setGeometry(pt);
        cout <<"preptarr = " << ptarr << endl;
        retval = mapdata.extendLine(ptarr2,ptarr,-30);
        cout <<"postptarr = " << ptarr2 << endl;
        lineEl1.setGeometry(ptarr2);

        retval = mapdata.extendLine(ptarr2,ptarr,20);
        cout <<"postptarr = " << ptarr2 << endl;
        

        lineEl2.setGeometry(ptarr2);
        lineEl.setGeometry(ptarr);
        maptalker.sendMapElement(&lineEl,sendSubGroup);
        maptalker.sendMapElement(&lineEl1,sendSubGroup);
        maptalker.sendMapElement(&lineEl2,sendSubGroup);
        maptalker.sendMapElement(&pointEl1,sendSubGroup);
        cout << endl <<"Getting closest lane to selected point" <<endl;
      } 
      else{ 
        usleep (100000);
      }
    }
  }   
  

  cout <<"========================================" << endl;
  testname = "Testing Map::getObsNearby()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    //    double val;
    vector<MapElement> elarr;
    count = 5;
    cout << "Getting closest lane to selected point" <<endl;    
    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout  <<"For point " << statept << endl;
        
        if (count>0){
          ptarr.clear();
          ptarr.push_back(statept+point2(2,2));
          ptarr.push_back(statept+point2(2,-1));
          ptarr.push_back(statept+point2(-3,-2));
          ptarr.push_back(statept+point2(-4,2));

          polyEl.setId(baseID,100+count);
          polyEl.setGeometry(ptarr);
          polyEl.setTypeObstacle();
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          mapdata.addEl(polyEl);
          count = count-1;
        }
        else{
          retval = mapdata.getObsNearby(elarr,statept,30);
          for(unsigned int j=0; j<elarr.size();++j){
            cout << "Got el " << elarr[j].id << endl;
            polyEl1.setId(elarr[j].id);
            polyEl1.setGeometry(elarr[j].geometry);
            maptalker.sendMapElement(&polyEl1,sendSubGroup);
          }
        }
        cout << endl <<"Getting closest lane to selected point" <<endl;
      } 
      else{ 
        usleep (100000);
      }
    }
  }   


  cout <<"========================================" << endl;
  testname = "Testing Map::getObsInLane()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    //    double val;
    vector<MapElement> elarr;
    count = 5;
    cout << "Getting closest lane to selected point" <<endl;    
    mapdata.loc2site.x=-10;
    mapdata.loc2site.y=-20;
    mapdata.loc2site.ang=1;



    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout  <<"For point " << statept << endl;
        
        if (count>0){
          ptarr.clear();
          ptarr.push_back(statept+point2(2,2));
          ptarr.push_back(statept+point2(2,-1));
          ptarr.push_back(statept+point2(-3,-2));
          ptarr.push_back(statept+point2(-4,2));

          polyEl.setId(baseID,100+count);
          polyEl.setGeometry(ptarr);
          polyEl.setTypeVehicle();
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          mapdata.addEl(polyEl);
          count = count-1;
        }
        else{
          retval=  mapdata.getLane(llabel,statept);
          cout << "getLane returns label = " << llabel << endl;   
          retval = mapdata.getLaneCenterLine(ptarr,llabel);

          lineEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);

          retval = mapdata.getObsInLane(elarr,llabel);
          for(unsigned int j=0; j<elarr.size();++j){
            cout << "Got el " << elarr[j].id << endl;
            polyEl1.setId(elarr[j].id);
            polyEl1.setGeometry(elarr[j].geometry);
            maptalker.sendMapElement(&polyEl1,sendSubGroup);
          }
        }
        cout << endl <<"Getting closest lane to selected point" <<endl;
      } 
      else{ 
        usleep (100000);
      }
    }
  }   


  cout <<"========================================" << endl;
  testname = "Testing MapElement::rotate()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    //    double val;
    vector<MapElement> elarr;
    count = 5;

    int boundsze;
    cout<<"enter size of bound: ";
    cin>>boundsze;
    ptarr1.clear();
    ptarr2.clear();
    cout<<endl<<"enter "<<boundsze<<" points"<<endl;

    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        if(boundsze > 0) {
          ptarr1.push_back(statept);
          cout<<"entered bounding point: "<<statept.x<<", "<<statept.y<<endl;
          boundsze--;
          if(boundsze == 0) {
            cout << "now, Place 5 obstacles" <<endl;    
          }
          polyEl.setId(baseID);
          polyEl.setGeometry(ptarr1);
          polyEl.plotColor=MAP_COLOR_BLUE;
          maptalker.sendMapElement(&polyEl,sendSubGroup);


        } else{
          polyEl.rotate(.1);
          maptalker.sendMapElement(&polyEl,sendSubGroup);
        }

      } 
      else{ 
        usleep (100000);
      }
    }
  }   


  cout <<"========================================" << endl;
  testname = "Testing Map::getObsInBounds()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    //    double val;
    vector<MapElement> elarr;
    count = 5;

    int boundsze;
    cout<<"enter size of bound: ";
    cin>>boundsze;
    ptarr1.clear();
    ptarr2.clear();
    cout<<endl<<"enter "<<boundsze<<" points"<<endl;

    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        if(boundsze > 0) {
          ptarr1.push_back(statept);
          cout<<"entered bounding point: "<<statept.x<<", "<<statept.y<<endl;
          boundsze--;
          if(boundsze == 0) {
            cout << "now, Place 5 obstacles" <<endl;    
          }
          polyEl.setId(baseID);
          polyEl.setGeometry(ptarr1);
          polyEl.plotColor=MAP_COLOR_BLUE;
          maptalker.sendMapElement(&polyEl,sendSubGroup);


        } else if (count>0){
          ptarr2.clear();
          ptarr2.push_back(statept+point2(2,2));
          ptarr2.push_back(statept+point2(2,-1));
          ptarr2.push_back(statept+point2(-3,-2));
          ptarr2.push_back(statept+point2(-4,2));

          polyEl.setId(baseID,100+count);
          polyEl.setGeometry(ptarr2);
          polyEl.setTypeVehicle();
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          mapdata.addEl(polyEl);
          count = count-1;
        }
        else{
          point2arr fooarr = point2arr();
          retval = mapdata.getObsInBounds(elarr, ptarr1, fooarr);
          for(unsigned int j=0; j<elarr.size();++j){
            cout << "Got el " << elarr[j].id << endl;
            polyEl1.setId(elarr[j].id);
            polyEl1.setGeometry(elarr[j].geometry);
            maptalker.sendMapElement(&polyEl1,sendSubGroup);
          }
        }
      } 
      else{ 
        usleep (100000);
      }
    }
  }   


  
  cout <<"========================================" << endl;
  testname = "Testing Map::getObsInZone()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    //    double val;
    vector<MapElement> elarr;
    count = 5;
    int zonelabel = 0;
    cout << "Getting closest lane to selected point" <<endl;    
    while (true){
 
    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout  <<"For point " << statept << endl;
        
        if (count>0){
          ptarr.clear();
          ptarr.push_back(statept+point2(2,2));
          ptarr.push_back(statept+point2(2,-1));
          ptarr.push_back(statept+point2(-3,-2));
          ptarr.push_back(statept+point2(-4,2));

          polyEl.setId(baseID,100+count);
          polyEl.setGeometry(ptarr);
          polyEl.setTypeVehicle();
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          mapdata.addEl(polyEl);
          count = count-1;
        }
        else{
          retval=  mapdata.getClosestPointID(label,statept);
          zonelabel = label.segment;
          cout << "zonelabel = " << zonelabel << endl;
           
          retval = mapdata.getZonePerimeter(ptarr1,zonelabel);
          if (retval <0)
            continue;
           
          polyEl.setGeometry(ptarr1);
          maptalker.sendMapElement(&polyEl,sendSubGroup);

          retval = mapdata.getObsInZone(elarr,zonelabel);
          for(unsigned int j=0; j<elarr.size();++j){
            cout << "Got el " << elarr[j].id << endl;
            polyEl1.setId(elarr[j].id);
            polyEl1.setGeometry(elarr[j].geometry);
            maptalker.sendMapElement(&polyEl1,sendSubGroup);
          }
        }
        cout << endl <<"Select a zone point" <<endl;
      } 
      else{ 
        usleep (100000);
      }
    }
  }   


  
 


  cout <<"========================================" << endl;
  testname = "Testing getHeading()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    point2 pt,angpt,dpt;
    double ang;
    int headingpt = 0;
    
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout << recvEl << endl;;
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label,statept);
        cout << "ptlabel = " << label << endl;   
        retval = mapdata.getWaypoint(pt,label);
        lanelabel.segment = label.segment;
        lanelabel.lane = label.lane;
        cout << "lanelabel = " << lanelabel << endl;   
        cout << "getWaypoint retval = " << retval << " pt = " << pt << "  label " << label << endl;
        
        if (headingpt==0){
          retval = mapdata.getHeading(ang,statept);
          cout << "getHeading POINT retval = " << retval << " ang = " << ang << endl;
          dpt = statept;
          angpt = statept;
          headingpt++;
        }else if (headingpt==1){
 
          retval = mapdata.getHeading(ang,label);
          cout << "getHeading LABEL retval = " << retval << " ang = " << ang << endl;
          dpt = pt;
          angpt = pt;
          headingpt++;;
        }else{
          retval = mapdata.getHeading(ang,pt,lanelabel,statept);
          cout << "getHeading LANELABEL POINT retval = " << retval << " ang = " << ang << endl;
          dpt = pt;
          angpt = pt;
          

          headingpt=5;
        }


        dpt.x = angpt.x+10*cos(ang);
        dpt.y = angpt.y+10*sin(ang);

        ptarr.clear();
        ptarr.push_back(angpt);
        ptarr.push_back(dpt);
        lineEl.setGeometry(ptarr);
        maptalker.sendMapElement(&lineEl,sendSubGroup);
      }  
      else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing getDistToWaypoint() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool initflag = true;
    cout << "Select the waypoint" << endl;
    double dist = 0;
    while (true){

       
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        
        if (initflag){
          cout <<"For point " << statept << endl;
          retval =mapdata.getClosestPointID(label,statept);
          cout << "ptlabel = " << label << endl;  
          initflag = false;
        }else{
          retval =mapdata.getLaneDistToWaypoint(dist,statept, label);
          cout << "getDistToWaypoint retval = " << retval << " dist = " << dist << endl;
          retval =mapdata.getClosestPointID(statelabel,statept);          retval =mapdata.getLaneDistToWaypoint(dist,statelabel, label);
          cout <<"getDistToWaypt retval = " << retval << " dist = " << dist << endl;
          cout <<"From label "<<label << " to label " << statelabel << endl;
          

        }
        cout <<endl<< "Now select the state point" << endl;
      }
      else{ 
        
        usleep (100000);

      }  
    } 
  } 

  cout <<"========================================" << endl;
  testname = "Testing getStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label,statept);
        cout << "ptlabel = " << label << endl;  
        retval =mapdata.getStopline(stoppt,statept);
        cout << "get stopline retval = " << retval << " point = " << stoppt << endl;
    
      }
      else{ 
        
        usleep (100000);

      }
    }
  }
  cout <<"========================================" << endl;
  testname = "Testing getLaneID getSegmentID checkLaneID() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool inflag = false;
    bool switchflag = true;
    int laneid = 0;
    int segid = 0;
    cout << "Enter a point" << endl;    
    while (true){

    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        // recvEl.print();
        cout << endl <<"For point " << statept << endl;
        if (switchflag){

          laneid=  mapdata.getLaneID(statept);
          cout << "lane id = " << laneid << endl;  

          segid=  mapdata.getSegmentID(statept);
          cout << "segment id = " << segid << endl;  
          switchflag = !switchflag;
          cout << "Enter a point to test checkLaneID for this lane and segment id" << endl;
        }else{
          inflag=  mapdata.checkLaneID(statept, segid, laneid);
          if (inflag)
            cout << "IDs MATCHED - segment = " << segid << " lane = " << laneid << endl;
          else
            cout << "IDs no match - segment = " << segid << " lane = " << laneid << endl;

          switchflag = !switchflag;
          cout << endl<< "Enter a point" << endl;
        }    


      } 
      else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing getNextPointID getNextStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getNextPointID(label, statept);
        cout << "ptlabel = " << label << endl;  
 
        retval =mapdata.getNextStopline(stoppt,label);
        cout << "get stopline retval = " << retval << " point = " << stoppt << "  stop label " << label << endl;
    
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
  cout <<"========================================" << endl;
  testname = "Testing getLeftBound(), getRightBound() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout <<recvEl << endl;
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label,statept);
        lanelabel.segment = label.segment;
        lanelabel.lane = label.lane;
        cout << "ptlabel = " << label << endl;  
        cout << "lanelabel = " << lanelabel << endl;  
        retval =mapdata.getLeftBound(ptarr,lanelabel);
        cout << "getLeftBound retval = " << retval << " ptarr = " << ptarr << "  label " << label << endl;
        lineEl2.setGeometry(ptarr);
        maptalker.sendMapElement(&lineEl2,sendSubGroup);

        retval =mapdata.getRightBound(ptarr,lanelabel);
        cout << "getRightBound retval = " << retval << " ptarr = " << ptarr << "  label " << label << endl;
        lineEl.setGeometry(ptarr);
        maptalker.sendMapElement(&lineEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }

  
  cout <<"========================================" << endl;
  testname = "Testing getObstacleDist(), getObstaclePoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    bool initflag = true;
    double dist;
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (initflag){
          statept = recvEl.center;
          cout << recvEl << endl;
          cout << endl <<"For point " << statept << endl;
          polyEl.setGeometry(statept,2);
          polyEl.height = 10;
          polyEl.setTypeObstacle();
          cout << "Setting obstacle = " << endl;   
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          mapdata.data.push_back(polyEl); 
          initflag = false;
        }
        else {  
          statept = recvEl.center;
          cout << recvEl << endl;
          cout << endl <<"For point " << statept << endl;
          dist=  mapdata.getObstacleDist(statept);
          cout << "DIST = " << dist << endl;
          pt = mapdata.getObstaclePoint(statept,10);
          pointEl.setGeometry(pt);
          maptalker.sendMapElement(&pointEl,sendSubGroup);

        }
      }     
      else{  
        usleep (100000);   
      }   
    }  
  }
  cout <<"========================================" << endl;
  testname = "Testing getStopLineSenseed()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    bool initflag = true;
    bool firstpoint = true;
   
    MapElement stopline;
    point2 stoppt;
  

    // double dist;


    stopline.clear();
    stopline.setId(baseID,150,0);
    stopline.setTypeStopLine();
    //stopline.setGeometry(points);
    //stopline.setState(this->stereoBlob.state);
    stopline.plotColor = MAP_COLOR_RED;


    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (initflag){
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          retval= mapdata.getClosestPointID(label,statept);
          cout << "ptlabel = " << label << endl;  

          retval = mapdata.getWayPoint(selectpt,label);
          cout << "selected point coords " << selectpt << endl;  
          //retval = mapdatagetPointEntryLabels(ptlabelarr,label);
          //retval = mapdatagetPointArr(ptarr,ptlabelarr);
          pointEl.setGeometry(selectpt);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
          initflag = false;
        }
        else {  
          if (firstpoint){
            ptarr.clear();
            statept = recvEl.center;
            ptarr.push_back(statept);
            pointEl2.setGeometry(ptarr);
            maptalker.sendMapElement(&pointEl2,sendSubGroup);
            
            firstpoint=false;
          }else{
            statept = recvEl.center;
            ptarr.push_back(statept);
            stopline.setGeometry(ptarr);
            
            mapdata.addEl(stopline);

            mapdata.getStopLineSensed(stoppt,label);

            pointEl.setGeometry(stoppt);
            maptalker.sendMapElement(&stopline,sendSubGroup);
            maptalker.sendMapElement(&pointEl,sendSubGroup);

            firstpoint=true;
          }

        }
      }      
      else{  
        usleep (100000);   
      }    
    }     
  }
  
  cout <<"========================================" << endl;
  testname = "Testing getBounds() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool forward = true;
    while (true){
    
          
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //  cout <<recvEl << endl;
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getLane(lanelabel,statept);
        cout << "lanelabel = " << lanelabel << endl;
          
        int retval=0; 
        if (forward){
          retval =mapdata.getBounds(ptarr1,ptarr2,lanelabel,statept,40,10);
          //cout << "getBounds retval = " << retval << " ptarr1 = " << ptarr1 << "  ptarr2 = " << ptarr2 << endl;
          //forward = false; 
        }else{
          retval =mapdata.getBoundsReverse(ptarr1,ptarr2,lanelabel,statept,40,10);
          //   cout << "getBoundsReverse retval = " << retval << " ptarr1 = " << ptarr1 << "  ptarr2 = " << ptarr2 << endl;
          forward = true;
        }
      
        lineEl2.setGeometry(ptarr1);
        maptalker.sendMapElement(&lineEl2,sendSubGroup);
        lineEl.setGeometry(ptarr2);
        maptalker.sendMapElement(&lineEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      } 
    }   
  }
 

  cout <<"========================================" << endl;
  testname = "Testing getZonePerimeter, getZoneParkingSpots() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

		int zonelabel =0;
		double width;
		unsigned int i;
		vector<SpotLabel> spotarr;
    while (true){
    
          
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //  cout <<recvEl << endl;
        cout << endl <<"For point " << statept << endl;
				retval=  mapdata.getClosestPointID(label, statept);
				zonelabel = label.segment;
				cout << "zonelabel = " << zonelabel << endl;
          
				retval = mapdata.getZonePerimeter(ptarr1,zonelabel);
				if (retval <0)
					continue;
				 
        polyEl.setGeometry(ptarr1);
        maptalker.sendMapElement(&polyEl,sendSubGroup);

				retval = mapdata.getZoneParkingSpots(spotarr,zonelabel);
				if (retval <0)
					continue;
				 
				for (i = 0; i<spotarr.size();++i){
					width = -1;
					retval = mapdata.getSpotWidth(width, spotarr[i]);
					cout << spotarr[i] << " w=" << width <<endl;
				}

				
        polyEl.setGeometry(ptarr1);
        maptalker.sendMapElement(&polyEl,sendSubGroup);

      } 
      else{ 
        usleep (100000);
      } 
    }   
  }
 



  cout <<"========================================" << endl;
  testname = "Testing getTransitionBounds( NEW ) ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool exitflag = true;
    bool getptflag = true;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (getptflag){
          if (exitflag){
            statept = recvEl.center;
            cout <<recvEl << endl;
            cout << endl <<"For point " << statept << endl;
            retval=  mapdata.getClosestPointID(label1,statept);
            cout << "label exit segment = " << label1 << endl;
            exitflag = false;
          }else{
            statept = recvEl.center;
            cout <<recvEl << endl;
            cout << endl <<"For point " << statept << endl;
            retval=  mapdata.getClosestPointID(label2,statept);
            cout << "label enter segment = " << label2 << endl;  
            getptflag = false;
          }
        }else{
          statept = recvEl.center;
          cout <<recvEl << endl; 
          cout << endl <<"For point " << statept << endl;         
              
          retval =mapdata.getTransitionBounds(ptarr1,ptarr2,label1,label2,statept,80,40);
          cout << "getTransitionBounds retval = " << retval << endl              << " ptarr1 = " << ptarr1 
               << " ptarr2 = " << ptarr2 << endl
               << " exit label " << label1 
               << " enter label " << label2 << endl;
          lineEl2.setGeometry(ptarr1);    
          maptalker.sendMapElement(&lineEl2,sendSubGroup);

          lineEl.setGeometry(ptarr2); 
          maptalker.sendMapElement(&lineEl,sendSubGroup); 
          exitflag = true;
        }         
               
      }        
      else{     
        usleep (100000);
      }   
    }
  }
     
   
  cout <<"========================================" << endl;
  testname = "Testing getShortTransitionBounds() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool exitflag = true;
    // bool getptflag = true;
    while (true){
 
       
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (exitflag){
          statept = recvEl.center;
          cout <<recvEl << endl;
          cout << endl <<"For point " << statept << endl;
          retval=  mapdata.getClosestPointID(label1,statept);
          cout << "label exit segment = " << label1 << endl;
          exitflag = false;
        }else{
          statept = recvEl.center;
          cout <<recvEl << endl;
          cout << endl <<"For point " << statept << endl;
          retval=  mapdata.getClosestPointID(label2,statept);
          cout << "label enter segment = " << label2 << endl;  
          exitflag = true;
          statept = recvEl.center;
          cout <<recvEl << endl; 
          cout << endl <<"For point " << statept << endl;         
              
          retval =mapdata.getShortTransitionBounds(ptarr1,ptarr2,label1,label2);
          cout << "getTransitionBounds retval = " << retval << endl              << " ptarr1 = " << ptarr1 
               << " ptarr2 = " << ptarr2 << endl
               << " exit label " << label1 
               << " enter label " << label2 << endl;
          lineEl2.setGeometry(ptarr1);    
          maptalker.sendMapElement(&lineEl2,sendSubGroup);

          lineEl.setGeometry(ptarr2); 
          maptalker.sendMapElement(&lineEl,sendSubGroup); 
         
        }         
               
             
      }else{     
        usleep (100000);
      }   
    }  
  }   
   
     

  cout <<"========================================" << endl;
  testname = "Testing getLaneCenterPoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    bool initflag = true;
    while (true){

       
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (initflag){
          statept = recvEl.center;
          cout <<recvEl << endl;
          cout << endl <<"For point " << statept << endl;
          retval=  mapdata.getClosestPointID(label,statept);
          lanelabel.segment = label.segment;
          lanelabel.lane = label.lane;
          cout << "lanelabel = " << lanelabel << endl;
       
          initflag = false;
        }
        else {
          statept = recvEl.center;
          cout <<recvEl << endl;
          cout << endl <<"For point " << statept << endl;

          cout << "Getting Center " << endl;
          mapdata.getLaneCenterPoint(pt,lanelabel,statept);
          mapdata.getHeading(val,pt);
          cout << "Center ang = " << val << endl;
     
          pointEl.setGeometry(pt);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
     
          cout << "Getting Right " << endl;
          mapdata.getLaneRightPoint(pt,lanelabel,statept,-10);
          mapdata.getHeading(val,pt);
          cout << "Right ang = " << val << endl;

          pointEl1.setGeometry(pt);
          maptalker.sendMapElement(&pointEl1,sendSubGroup);

          cout << "Getting Left " << endl;
          mapdata.getLaneLeftPoint(pt,lanelabel,statept,15);
          mapdata.getHeading(val,pt);
          cout << "Left ang = " << val << endl;

          pointEl2.setGeometry(pt);
          maptalker.sendMapElement(&pointEl2,sendSubGroup);

        }
      }     
      else{  
        usleep (100000);   
      }   
    }  
  } 
      
  

   
    

  
 
 


 
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
