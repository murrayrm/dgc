/**********************************************************
 **
 **  TESTMAPID.CC
 **
 **    Time-stamp: <2007-03-12 13:12:58 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sun Mar 11 11:46:03 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#include <iostream>
#include <string>
#include "MapId.hh"

using namespace std;



int main(int argc, char **argv)
{

   int testnum = -1;
  int thistest=0;
  bool testflag =0;
  string  testname;

 
  cout << endl << "Usage :" << endl;
  cout << " testMapId [test number, 0 : none, -1 : all] " << endl << endl;


  if(argc >1)
    testnum = atoi(argv[1]);
  
  cout << "  Test Number = " << testnum << endl << endl; ;
       
  cout <<"========================================" << endl;
  testname = "Testing Constructors";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;
        vector<int> vec;
        vec.push_back(4);
        vec.push_back(2);
        vec.push_back(3);

        MapId id_empty;
        cout << "id_empty = " << id_empty << endl;
        MapId id_1(12);
        cout << "id_1 = " << id_1 << endl;
        MapId id_2(74,2);
        cout << "id_2 = " << id_2 << endl;
        MapId id_3(2,1,4);
        cout << "id_3 = " << id_3 << endl;
        MapId id_4(1,5,3,2);
        cout << "id_4 = " << id_4 << endl;
        MapId id_copy(id_3);
        cout << "id_copy = " << id_copy << endl;
        MapId id_vec(vec);
        cout << "id_vec = " << id_vec << endl;
                
      }


      cout <<"========================================" << endl;
      testname = "Testing assignment =, clear, set";
      thistest++;
      testflag = (thistest==testnum || 
                  (testnum<0 &&-thistest<=testnum));
      cout <<"Test #" << thistest << "  " << testname;
      if (!testflag)
        cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;

        MapId id_1(12);
        cout << "id_1 = " << id_1 << endl;
        MapId id_2(74,2);
        cout << "id_2 = " << id_2 << endl;


        id_1=id_2;
        cout << "id_1=id_2 -> id_1 = "  << id_1 << endl;

        id_1=78;
        cout << "id_1=78 -> id_1 = "  << id_1 << endl;

        id_1.set(24);
        cout << "id_1.set(24) -> id_1 = " << id_1 << endl;

        id_1.set(24,2);
        cout << "id_1.set(24,2) -> id_1 = " << id_1 << endl;

        id_1.set(1,2,3);
        cout << "id_1.set(1,2,3) -> id_1 = " << id_1 << endl;

        id_1.set(1,2,2,4);
        cout << "id_1.set(1,2,2,4) -> id_1 = " << id_1 << endl;

        id_1.clear();
        cout << "id_1.clear() -> id_1 = "  << id_1 << endl;
        

      }

      cout <<"========================================" << endl;
      testname = "Testing index operator";
      thistest++;
      testflag = (thistest==testnum || 
                  (testnum<0 &&-thistest<=testnum));
      cout <<"Test #" << thistest << "  " << testname;
      if (!testflag)
        cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;
        
        MapId id_1(2,4,6,8);
        cout << "id_1 = " << id_1 << endl;
        cout << "id_1[2] = " << id_1[2] << endl;

        
        id_1[0] = 99;
        cout << "id_1[0] = 99 -> id_1 = "  << id_1 << endl;

        cout << "testing invalid index" << endl;
        id_1[4] = 9;
        cout << "id_1[4] = 9 -> id_1 = "  << id_1 << endl;
        
      }

      cout <<"========================================" << endl;
      testname = "Testing back, front, push_back, resize";
      thistest++;
      testflag = (thistest==testnum || 
                  (testnum<0 &&-thistest<=testnum));
      cout <<"Test #" << thistest << "  " << testname;
      if (!testflag)
        cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;
        MapId id_1(2,4,6);
        cout << "id_1 = " << id_1 << endl; 
        id_1.push_back(31);
        cout << "id_1.push_back(31) -> id_1 = " << id_1 << endl; 
        id_1.push_back(23);
        cout << "id_1.push_back(23) -> id_1 = " << id_1 << endl; 

        cout << "id_1.back() = " << id_1.back() <<endl;
        cout << "id_1.front() = " << id_1.front() <<endl;
        
        id_1.resize(2);
        cout << "id_1.resize(2) -> id_1 = " << id_1 << endl;

        id_1.resize(6);
        cout << "id_1.resize(6) -> id_1 = " << id_1 << endl;


      }

      cout <<"========================================" << endl;
      testname = "Testing ==, !=, match";
      thistest++;
      testflag = (thistest==testnum || 
                  (testnum<0 &&-thistest<=testnum));
      cout <<"Test #" << thistest << "  " << testname;
      if (!testflag)
        cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;
        MapId id_1(12,23);
        cout << "id_1 = " << id_1 << endl;
        MapId id_2(12,23);
        cout << "id_2 = " << id_2 << endl;
        MapId id_3(12,23,32);
        cout << "id_3 = " << id_3 << endl;
        MapId id_4(21,12,23);
        cout << "id_4 = " << id_4 << endl;
        MapId id_5(12);
        cout << "id_5 = " << id_5 << endl;
        
        cout << "id_1==id_2 returns " << (id_1==id_2) << endl;
        cout << "id_1!=id_2 returns " << (id_1!=id_2) << endl;

        cout << "id_1==id_3 returns " << (id_1==id_3) << endl;
        cout << "id_1!=id_3 returns " << (id_1!=id_3) << endl;

        cout << "id_1==id_4 returns " << (id_1==id_4) << endl;
        cout << "id_1!=id_4 returns " << (id_1!=id_4) << endl;

        cout << "id_1.match(id_2) returns " << id_1.match(id_2) << endl;
        cout << "id_1.match(id_3) returns " << id_1.match(id_3) << endl;
        cout << "id_1.match(id_4) returns " << id_1.match(id_4) << endl;
        
        cout << "id_1==12 returns " << (id_1==12) << endl;
        cout << "id_1!=12 returns " << (id_1!=12) << endl;

        cout << "id_5==12 returns " << (id_5==12) << endl;
        cout << "id_5!=12 returns " << (id_5!=12) << endl;

         cout << "id_2.match(12) returns " << id_2.match(12) << endl;
        cout << "id_2.match(12,23) returns " << id_2.match(12,23) << endl;
        cout << "id_2.match(12,32) returns " << id_2.match(12,32) << endl;
        cout << "id_2.match(12,23,42) returns " << id_2.match(12,23,42) << endl;
         
      }

      cout <<"========================================" << endl;
      testname = "Testing >, <, operators";
      thistest++;
      testflag = (thistest==testnum || 
                  (testnum<0 &&-thistest<=testnum));
      cout <<"Test #" << thistest << "  " << testname;
      if (!testflag)
        cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;
        MapId id_1(12,23);
        cout << "id_1 = " << id_1 << endl;
        MapId id_2(12,23);
        cout << "id_2 = " << id_2 << endl;
        MapId id_3(12,23,32);
        cout << "id_3 = " << id_3 << endl;
        MapId id_4(21,12,23);
        cout << "id_4 = " << id_4 << endl;
        MapId id_5(12);
        cout << "id_5 = " << id_5 << endl;
        
        cout << "id_1<id_2 returns " << (id_1<id_2) << endl;
        cout << "id_1>id_2 returns " << (id_1>id_2) << endl;

        cout << "id_1<id_3 returns " << (id_1<id_3) << endl;
        cout << "id_1>id_3 returns " << (id_1>id_3) << endl;

        cout << "id_1<id_4 returns " << (id_1<id_4) << endl;
        cout << "id_1>id_4 returns " << (id_1>id_4) << endl;

        cout << "id_1<id_5 returns " << (id_1<id_5) << endl;
        cout << "id_1>id_5 returns " << (id_1>id_5) << endl;
         
      }


    if (0){ 
      //--------------------------------------------------
      // starttemplate for new test
      //--------------------------------------------------
   
      cout <<"========================================" << endl;
      testname = "Testing ";
      thistest++;
      testflag = (thistest==testnum || 
                  (testnum<0 &&-thistest<=testnum));
      cout <<"Test #" << thistest << "  " << testname;
      if (!testflag)
        cout << "  NOT RUN" << endl;
      else{
        cout << "  RUNNING" << endl;
          
      }

      //--------------------------------------------------
      // end template for new test
      //--------------------------------------------------   
    }
 
 
 
    cout <<"========================================" 
         << endl << endl;
    return 0;

}
