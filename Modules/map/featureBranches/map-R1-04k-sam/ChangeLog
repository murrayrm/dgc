Sun Sep 30 10:08:40 2007	Sam Pfister (sam)

	* version R1-04k
	BUGS:  
	FILES: MapElementTalker.cc(41808), MapElementTalker.hh(41808),
		testRecvMapElement.cc(41808), testSendMapElement.cc(41808)

	Added function to mapElementTalker which can recover from a spread
	overflow error, but will lose the data in the spread buffer when
	doing so.  It also implements a timed blocking read and should only
	be used in situations where data transfer is non-critical as in
	visualization with mapviewer.  

	The new funciton is called
	recvMapElementTimedBlockLossy(*el,delay,group) and it will return
	>0 for read element ==0 for timed out nothing read, -1 for a
	recovered overflow error, and should call exit(0) for other spread
	errors or a failed recovery.  

	Also added function getDropCount() to the talker which will return
	the number of times the talker has disconnected and reconnected.

Fri Sep 28  6:35:54 2007	Sam Pfister (sam)

	* version R1-04j
	BUGS:  
	FILES: Map.cc(40959), Map.hh(40959), MapElement.cc(40959),
		MapElement.hh(40959), MapElementTalker.cc(40959),
		MapElementTalker.hh(40959), MapPrior.cc(40959),
		MapPrior.hh(40959), testRecvMapElement.cc(40959),
		testSendMapElement.cc(40959)
	Updated MapElementTalker to include a blocking read with timeout :
	recvMapElementTimedBlock.  Added ELEMENT_DEBUG map element type
	which can be used to send formatted debugging data.  Added function
	Map::copyMapData which is a fast method to copy only the data to a
	second map which is accessed by the map queries and by planner. 
	Secondary data used for fusion is not copied, and non-updated
	lane-lines are not copied.  This scales better with large RNDFs.   
	 Updated line fusion option in Map such that lane lines are not
	densified with interpolation if line fusioin is not turned on. 
	Commented out MSG calls which can slow down the receive loop.  

Thu Sep 27  1:44:49 2007	Laura Lindzey (lindzey)

	* version R1-04i
	BUGS:  
	FILES: Map.cc(40734), Map.hh(40734)
	adding rear stereo obs perceptor and ladar obs perceptor to enum

Fri Sep 21 10:29:32 2007	datamino (datamino)

	* version R1-04h
	BUGS:  
	New files: DebugWindow.cc DebugWindow.hh testLineFus.cc
	FILES: Makefile.yam(39739), Map.cc(39739), Map.hh(39739),
		MapPrior.cc(39739)
	Merged changes with Sam (yam sync)

	New files: DebugWindow.cc DebugWindow.hh testLineFus.cc
	FILES: Makefile.yam(39344), Map.cc(39344)
	Added a hack to favour association with the current lane. Commented
	out debug MSG()'s. Added a optimization-based way to smooth the
	fused line (not working well, commented out, could just be
	dropped).

	FILES: Map.cc(39018), MapPrior.cc(39018)
	- Implemented association of lane lines with the whole segment, not
	just the current lane.
	- Now taking into consideration if a line should be present or not,
	as specified by the rndf file (left/right_boundary).
	- Fixed getVotes(), was returning incorrect values which sometimes 
	caused wrong associations.
	- Moved smoothing into its own function.
	- Added some preliminary (non-working) association routines for a
	better association algorithm, commented out.

	NOTE: yam forgot to add some important commit messages, adding them now:
	r39569 | datamino | 2007-09-20 02:45:03 -0700 (Thu, 20 Sep 2007) | 2 lines
	Changed paths:
	   M /Modules/map/featureBranches/map-R1-04f-datamino/Makefile.yam
	   M /Modules/map/featureBranches/map-R1-04f-datamino/Map.cc

	Various bug fixes in fuseCurb(), now seems to work.

	------------------------------------------------------------------------
	r39568 | datamino | 2007-09-20 02:44:24 -0700 (Thu, 20 Sep 2007) | 2 lines
	Changed paths:
	   M /Modules/map/featureBranches/map-R1-04f-datamino/DebugWindow.cc

	Printing selected element as mapviewer does.

	------------------------------------------------------------------------
	r39537 | datamino | 2007-09-19 21:06:34 -0700 (Wed, 19 Sep 2007) | 8 lines
	Changed paths:
	   M /Modules/map/featureBranches/map-R1-04f-datamino/Map.cc
	   M /Modules/map/featureBranches/map-R1-04f-datamino/Map.hh

	Implemented proper curb handling (untested, no avaliable
	log file to use!!!).
	Moved lane postprocessing code (smoothing and width
	enforcement) into its own fuction, Map::postProcessLane().
	Optimized this procedure, don't use getElFull but do all
	the processing in place directly on prior.fulldata (no
	more allocation and copying of big point2arr's).

	------------------------------------------------------------------------
	r39536 | datamino | 2007-09-19 21:02:49 -0700 (Wed, 19 Sep 2007) | 3 lines
	Changed	paths:
	   M /Modules/map/featureBranches/map-R1-04f-datamino/DebugWindow.cc
	   M /Modules/map/featureBranches/map-R1-04f-datamino/DebugWindow.hh
	   M /Modules/map/featureBranches/map-R1-04f-datamino/testLineFus.cc

	Proper handling of ELEMENT_ALICE, and proper update
	of the latest state trying to be smart wrt the timestamp.

	------------------------------------------------------------------------
	r39445 | datamino | 2007-09-18 21:46:56 -0700 (Tue, 18 Sep 2007) | 6 lines
	Changed paths:
	   M /Modules/map/featureBranches/map-R1-04f-datamino/Map.cc
	   M /Modules/map/featureBranches/map-R1-04f-datamino/Map.hh

	   Added Map::fuseAllLines(state), which is faster because it
	   determines the current segment only once instead of for each
	   sensed line (using its MapElement::state).
	   Now smoothing the center of the lane, instead of whichever
	   side was updated, giving better results (more symmetric).

	
Fri Sep 14  5:18:50 2007	Laura Lindzey (lindzey)
Fri Sep 21  4:54:46 2007	Sam Pfister (sam)

	* version R1-04g
	BUGS:  
	FILES: Map.cc(39699), Map.hh(39699), MapElement.cc(39699),
		MapElement.hh(39699), MapPrior.cc(39699),
		MapPrior.hh(39699)
	Added functionality to maintain an RNDF frame offset.  Added
	getElGlobal and getElRndf methods to return elements in those
	frames. 

Tue Sep 18 11:32:15 2007	Sam Pfister (sam)

	* version R1-04f
	BUGS:  
	FILES: Map.cc(39290), Map.hh(39290)
	added getObsInPoly function which checks for obstacles overlapping
	a given polygon

Mon Sep 17  8:25:10 2007	Sam Pfister (sam)

	* version R1-04e
	BUGS:  
	FILES: Map.cc(39103), Map.hh(39103), MapPrior.cc(39103),
		MapPrior.hh(39103)

	Implemented functions to support allowing the local frame to drift
	with respectto global frame.  The default query functions still return
	everything in local frame for now, so adjustments are still needed
	to the planner interface for full integration of a drifting
	local-global transformation.

	Added functionality to allow for the updating of the fused lane
	information from an external source which should enable lane
	fusion to run separately from planner and obstacle fusion if
	desired.  Fused lane map elements added to the map which match ids
	with a lane element in the map prior can now just replace the
	corresponding prior element instead of having to recompute the
	line fusion in the loop.

	
Fri Sep 14  5:18:50 2007	Laura Lindzey (lindzey)

	* version R1-04d BUGS: FILES: Map.cc(38745), Map.hh(38745),
	MapElement.cc(38745), MapElement.hh(38745) adding special handling
	of elements from prediction new .setGeometry(int type) function in
	MapElement

	FILES: Map.cc(38758), MapElement.cc(38758)
	committing so I can yam save - only trivial changes

Thu Sep 13 16:09:56 2007	Laura Lindzey (lindzey)

	* version R1-04c
	BUGS:  
	FILES: Map.cc(38595), Map.hh(38595)
	Cleaning up after quick fix made w/ Vanessa. So long as all
	perceptors use their module ID as first # in element ID, addEl will
	properly handle clears. (I've checked with everybody responsible
	for a perceptor)

	FILES: Map.cc(38613), MapElement.cc(38613), MapElement.hh(38613)
	adding isClear accessor

Thu Sep 13 13:30:42 2007	vcarson (vcarson)

	* version R1-04b
	BUGS:  
	FILES: Map.cc(38537)
	Fixed the order in which line elements are added to the map.  This
	was causing a problem with clearing map elements. 

Tue Sep 11  2:19:46 2007	Laura Lindzey (lindzey)

	* version R1-04a
	BUGS:  
	FILES: Map.cc(38261), MapElement.cc(38261), testGeometry.cc(38261),
		testMap.cc(38261)
	merging my changes into Sam's...

	FILES: Map.cc(38239)
	switching order of line/obstacle handling in addEl  calling
	updateFromGeometry()

	FILES: Map.cc(38254), MapElement.cc(38254), testMap.cc(38254)
	code cleanup now isObstacle actually handles min/max incorrectly 

	FILES: MapElement.cc(38244), testGeometry.cc(38244)
	more changes to testGeometry: getting rid of deprecated functions
	(don't know how it compiled before...), testing self-overlap of
	elements  in MapElement.cc, isOverlap, checking that
	geometryMin/geometryMax have been initialized before they're used. 

	FILES: testMap.cc(38175)
	adding test function for getObsInBounds

Mon Sep 10 15:37:46 2007	Sam Pfister (sam)

	* version R1-04
	BUGS:  
	FILES: Map.hh(38142), MapPrior.cc(38142), MapPrior.hh(38142),
		testMap.cc(38142), testMapPrior.cc(38142)
	added getZoneEntries and getZoneExits function to map query
	function list.	Works the same as getLaneEntries/Exits but for
	zones.

Mon Sep 10 15:02:45 2007	Sam Pfister (sam)

	* version R1-03z
	BUGS:  
	FILES: Makefile.yam(38117), MapElementTalker.cc(38117),
		testRecvMapElement.cc(38117), testSendMapElement.cc(38117)
	Added mapElementTalker diagnostic functionality to
	testSendMapElement and testRecvMapElement.  Running either with no
	command line prints options for various tests which can be run to
	send and receive map elements.	Test 1 in testRecvMapElement
	outputs timing statistics for groups of received messages.  Test 2
	in testRecvMapElement outputs general statistics of the rates
	modules are sending map elements on a channel.	Test 1 in
	testSendMapElement sends a flood of a given number of map elements
	of a given size at a given rate.  All times are in useconds unless
	otherwise stated.  

Mon Sep 10 10:56:13 2007	Sam Pfister (sam)

	* version R1-03y
	BUGS:  
	FILES: Map.cc(38016), Map.hh(38016)
	added getObsInZone function to map interface

	FILES: testMap.cc(38048)
	added getObsInZone test to testMap

Sun Sep  9 16:47:03 2007	Tamas Szalay (tamas)

	* version R1-03x
	BUGS:  
	FILES: MapElement.cc(37973), MapElement.hh(37973)
	Updated isOverlap functions to use new is_poly_overlap test.

	FILES: MapElement.cc(37982)
	Some more speed updates for isOverlap.

Fri Sep  7  7:55:15 2007	Jessica Gonzalez (jengo)

	* version R1-03w
	BUGS:  
	FILES: MapElementTalker.cc(37798)
	added timesSeen to maptalker msg

Wed Sep  5 13:05:43 2007	Jessica Gonzalez (jengo)

	* version R1-03v
	BUGS:  
	FILES: MapElement.cc(37617), MapElement.hh(37617)
	Added timesSeen member to MapElement class

Wed Sep  5 11:46:03 2007	Laura Lindzey (lindzey)

	* version R1-03u
	BUGS:  
	FILES: Map.cc(37554)
	Changing addEl to properly update the IDtoINDEX array for
	mapper->planner messages

Tue Sep  4  0:17:05 2007	Laura Lindzey (lindzey)

	* version R1-03t
	BUGS:  
	FILES: Map.cc(37371), Map.hh(37371)
	merging my changes into sam's release

	FILES: Map.cc(37278), Map.hh(37278)
	now properly handles the time stopped field in map elements, and
	gets rid of duplicate fields in the objectData struct

	FILES: Map.cc(37363), Map.hh(37363)
	Changes made and tested in field on Mon evening: * addEl returns -1
	on failure (same as before) but returns 1-3 to signify whether the
	element was added using the IDtoINDEX lookup, was associated w/ a
	pre-existing element, or if it created a new element. This is
	useful for the debugging display in mapper.  *changing the order of
	an enum

Mon Sep  3 15:24:53 2007	Sam Pfister (sam)

	* version R1-03s
	BUGS:  
	FILES: Map.cc(37287), Map.hh(37287), MapPrior.cc(37287),
		MapPrior.hh(37287), testMap.cc(37287),
		testMapPrior.cc(37287)
	Added new zone parking spot access functions.  getZoneParkingSpots
	updates by reference an array of parking spot labels in a given
	zone.  getSpotWidth updates by reference the width of the spot in
	meters.  Both functions return -1 if the given label is invalid and
	0 if there is no error

Sun Sep  2 16:03:55 2007	Laura Lindzey (lindzey)

	* version R1-03r
	BUGS:  
	FILES: Map.cc(37048), Map.hh(37048)
	adding left roof ladar to perceptor enum

	FILES: Map.cc(37151), Map.hh(37151)
	adding special handling of messages from mapper, so planner's map
	and mapper's map can be kept identical.

Sat Sep  1  7:57:14 2007	Laura Lindzey (lindzey)

	* version R1-03q
	BUGS:  
	FILES: Map.cc(36958), Map.hh(36958)
	Merging my changes w/ current release 

	FILES: Map.cc(36814), Map.hh(36814)
	first attempt at new data structure - still doesn't work,
	committing so I can work on my mac

	FILES: Map.cc(36817)
	fixing segfault

	FILES: Map.cc(36951), Map.hh(36951)
	handles clear messages properly

Thu Aug 30 18:25:20 2007	datamino (datamino)

	* version R1-03p
	BUGS:  
	FILES: MapElementTalker.cc(36653)
	Fixed: now correctly sending the z coordinate for position, center,
	velocity, geometryMin, geometryMax in MapElementTalker.

Wed Aug 29 19:51:58 2007	datamino (datamino)

	* version R1-03o
	BUGS:  3450
	FILES: Map.cc(36231)
	Merged changes with latest release

	FILES: Map.cc(36390)
	Use both absolute number of votes and ratio votes/total points for
	association. Now each segment connecting corresponding left and
	right points is always orthogonal to the center of the lane. When
	postfiltering, also make the first and last points converge to the
	original rndf when not associated with measures.

	FILES: Map.cc(36226)
	Voting threshold on ration between votes and total points instead
	of absolute vote number

Tue Aug 28 17:19:00 2007	Jessica Gonzalez (jengo)

	* version R1-03n
	BUGS:  
	FILES: MapId.cc(36025)
	oops.. fixed bug with operator> and operator<

Tue Aug 28 14:43:41 2007	Jessica Gonzalez (jengo)

	* version R1-03m
	BUGS:  
	FILES: MapId.cc(35980), MapId.hh(35980), testMapId.cc(35980)
	added operator> and operator< so that MapId's can be used as keys
	for std::map

Mon Aug 27 17:18:56 2007	Sam Pfister (sam)

	* version R1-03l
	BUGS:  
	FILES: Map.cc(35751), MapElement.cc(35751), MapElement.hh(35751),
		MapPrior.cc(35751), testMapPrior.cc(35751),
		testRecvMapElement.cc(35751)
	Fixed bugs related to returning zone entrance and exits in the map
	query functions getWayPointExits/Entries and getLaneExits/Entries. 
	Changed default MapElement creation behavior remove bounding box
	calculation to reduce computational complexity.

Thu Aug 23 11:27:54 2007	Jessica Gonzalez (jengo)

	* version R1-03k
	BUGS:  
	FILES: MapElement.cc(35132), MapElement.hh(35132)
	Adding new color for traversed path

Wed Aug 22 14:49:02 2007	datamino (datamino)

	* version R1-03j
	BUGS:  
	FILES: Map.cc(34678)
	Fixed bug when sensed line has size 0 (this may be the cause of
	what we've seen in
	http://gc.caltech.edu/wiki07/index.php/Spread_debugging_2007-08-18#
	Run_5_-_18:34even though I think nobody should send 0-sized lines
	...). Asserted that prior lane line has size > 0.

Mon Aug 20 16:52:18 2007	Laura Lindzey (lindzey)

	* version R1-03i
	BUGS:  
	FILES: MapElementTalker.cc(34275)
	changing the MapElementTalker to actually copy over the z
	coordinates of the point2arr

Wed Aug 15 23:45:21 2007	datamino (datamino)

	* version R1-03h
	BUGS:  
	FILES: Map.cc(33834)
	Field changes: interpolating sensed line before association, since
	I use votes instead of just minimum distance.


Tue Aug 14 23:08:58 2007	datamino (datamino)

	* version R1-03g
	BUGS:  
	FILES: Map.cc(31778), Map.hh(31778)
	Implementation of a simple lane line fusion. Given a new sensed
	lane line MapElement, it updates the prior information with that.
	It's probably not good enough, not tested at all, and will just
	segfault to say hello, but will be better soon ...

	FILES: Map.cc(31972), Map.hh(31972), MapPrior.cc(31972),
		MapPrior.hh(31972)
	Some preliminary fusion of lane lines with prior data. Not working
	well.

	FILES: Map.cc(32221), MapPrior.cc(32221)
	Interpolating prior and sensed lane line to get better association.

	FILES: Map.cc(32356), MapPrior.cc(32356)
	Field changes (moving points along the normal, some other hacks)

	FILES: Map.cc(32676)
	Added a basic smoothing filter, that just updates the points on a
	lane not associated with a measure, but next to an associated one,
	using the average of the two neighbors. This simple change makes a
	LOT of difference ... Also, removed some not-working code.

	FILES: Map.cc(32682)
	lane fusion parameter tuning (parameters are hard coded atm)

	FILES: Map.cc(32897)
	Replaced minimum distance with maximum "votes" for association
	purposes (it's actually some kind of likelihood function), which
	gives better results and has the interesting side effect of being a
	lot faster (or, get_min_dist_line() is very slow).

	FILES: Map.cc(33049), Map.hh(33049), MapPrior.cc(33049)
	Line fusion: only associating sensed line with left or right bound
	of the current lane we're in (need to extend to the current
	segment), now it's much faster. Also, doing a moving average
	filtering on the whole fused line.

	FILES: Map.cc(33180), Map.hh(33180)
	Modified the postprocessing filter to use original rndf waypoints
	as input, so in the absence of data the fused line will converge to
	the original RNDF line (and not to a straight line!).

	FILES: Map.cc(33228)
	Enforcing minimum lane width constraint (the minimum width is the
	one specified in the rndf). Removed old code.

	FILES: Map.cc(33525), Map.hh(33525), MapPrior.cc(33525)
	Added Map::fuseAllLines(), removed some old code in MapPrior.

	FILES: Map.cc(33609), Map.hh(33609)
	Adding a flag to enable or disable line fusion.

Thu Aug  2 13:33:15 2007	datamino (datamino)

	* version R1-03f
	BUGS:  
	FILES: Map.cc(31541), Map.hh(31541)
	Implemented Map::getZonePerimeter() and Map::getSpotWaypoints().

Tue Jul 31 10:48:07 2007	Laura Lindzey (lindzey)

	* version R1-03e
	BUGS:  
	FILES: Map.cc(30975), MapElement.cc(30975), MapElement.hh(30975)
	adding new map element types to help with fusion

	FILES: MapElement.hh(31024)
	committing trivial changes (neater spacing) so I can release

Tue Jul 17  7:58:51 2007	Sam Pfister (sam)

	* version R1-03d
	BUGS:  
	FILES: Map.cc(29230), Map.hh(29230), MapElement.cc(29230),
		MapElement.hh(29230), MapElementTalker.cc(29230),
		MapPrior.cc(29230), MapPrior.hh(29230),
		testGeometry.cc(29230)
	added timeStopped field to MapElement class.  This will maintain
	the amount of time the obstacle has not been perceived to have
	moved which is useful for planning decisions.	Also cleaned up a
	few comments

Wed Jul 11  0:59:49 2007	Sam Pfister (sam)

	* version R1-03c
	BUGS:  
	FILES: Map.cc(28768), testMap.cc(28768)
	Rolled in field changes which fixed transition bounds for uturn
	transitions in lane.

Thu Jun 28  2:36:18 2007	Sam Pfister (sam)

	* version R1-03b
	BUGS:  
	FILES: Map.cc(28382), Map.hh(28382), testMap.cc(28382)
	Added new version of getHeading function which takes a lane label
	and a point and returns the point projected into the lane and the
	heading of the lane at that projected point

Fri Jun 22 14:03:19 2007	Sam Pfister (sam)

	* version R1-03a
	BUGS:  
	FILES: Map.cc(28245), MapPrior.cc(28245), testMap.cc(28245)
	removed cout comments which mess up tplanner console.  Cerr
	comments are still there, but nothing that outputs text in nominal
	operation.

Thu Jun 21 17:05:08 2007	Sam Pfister (sam)

	* version R1-03
	BUGS:  
	FILES: Map.cc(28209)
	updated get transition bounds function to be more robust across
	straight but offset transitions.

Sun Jun 10 10:35:58 2007	Sam Pfister (sam)

	* version R1-02z
	BUGS:  
	FILES: testGeometry.cc(27650, 27652)
	updated some test functions for the point2arr class

Sat Jun  9  9:24:01 2007	Sam Pfister (sam)

	* version R1-02y
	BUGS:  
	FILES: MapElement.cc(27414), MapElement.hh(27414),
		MapPrior.cc(27414)
	Added explicit copy constructor and assignment operator for the
	MapElement class.  Added ELEMENT_FREE_SPACE type to MapElement
	class which will represent empty space.

Thu Jun  7 18:21:20 2007	Sam Pfister (sam)

	* version R1-02x
	BUGS:  
	FILES: Map.cc(27139)
	Fixed bugs in getShortTransitionBounds and in getObsInBounds

Thu Jun  7  9:58:19 2007	Sam Pfister (sam)

	* version R1-02w
	BUGS:  
	FILES: Map.cc(27056), Map.hh(27056), testMap.cc(27056)
	Added function getShortTransitionBounds which just returns the
	portion of the transition bound inside the intersection.

Wed Jun  6 19:50:07 2007	Sam Pfister (sam)

	* version R1-02v
	BUGS:  
	FILES: Map.cc(26916)
	Updated existing map stopline functions used by tplanner to call
	the function setStopLineSensed so that if a stopline is detected,
	and within 2 meters of the rndf stopline, the sensed stopline point
	will be returned.  Nothing will change if the stopline perceptors
	are not on.

Tue Jun  5 16:33:41 2007	Sven Gowal (sgowal)

	* version R1-02u
	BUGS:  
	FILES: MapPrior.cc(26647)
	Fixed isPointInLane function

Mon Jun  4  9:00:18 2007	Sam Pfister (sam)

	* version R1-02t
	BUGS:  
	FILES: MapElement.cc(26294)
	fixed bug in function in MapElement which sets the geometry given a
	centerpoint, length, width and orientation.  This function is
	unused in the current software (probably because it was broken and
	there were alternatives) but it works and has been tested now.

Sat Jun  2  0:44:24 2007	Sam Pfister (sam)

	* version R1-02s
	BUGS:  
	FILES: Map.cc(26131), testMap.cc(26131)
	updated getTransitionBounds function to return interpolated corners

Fri Jun  1  5:03:47 2007	Sam Pfister (sam)

	* version R1-02r
	BUGS:  
	FILES: MapPrior.cc(25983)
	Small update to lane checking functions to move overlap queries to
	use interpolated lanes

Fri Jun  1  4:35:39 2007	Sam Pfister (sam)

	* version R1-02q
	BUGS:  
	FILES: Map.cc(25932), Map.hh(25932), MapPrior.cc(25932),
		MapPrior.hh(25932), testGeometry.cc(25932),
		testMap.cc(25932)
	updated lane functions to all return full interpolated lane.  Fixed
	some problems in old functions which should be retired but are still being used

	FILES: Map.cc(25955), Map.hh(25955), testGeometry.cc(25955),
		testMap.cc(25955)
	added getStopLineSensed function to get the sensed stopline if we
	see one

	FILES: Map.hh(25937), MapPrior.cc(25937), MapPrior.hh(25937),
		testMap.cc(25937)
	updated some map functions to help mapper send obstacles to the
	mplanner in a more robust way.
	



	
Tue May 29 23:37:09 2007	Jessica Gonzalez (jengo)

	* version R1-02p
	BUGS:  
	FILES: MapElement.cc(25474), MapElement.hh(25474)
	added elements for initial and final conditions from ocpspecs

Mon May 28 13:03:04 2007	Jessica Gonzalez (jengo)

	* version R1-02o
	BUGS:  
	FILES: MapElement.cc(25232), MapElement.hh(25232)
	changed color for polytopes

Sat May 26  0:21:29 2007	Sam Pfister (sam)

	* version R1-02n
	BUGS:  
	FILES: Map.cc(25028), testMap.cc(25028)
	updated getBounds and getBoundsReverse functions to return the
	interpolated data around some turns.  This is a temporary fix, as
	in the future, funtionality should be built on top of the
	getLaneBounds functions. Note that the range argument of the
	funcitons specify the minimum total length of the lines to be
	returned, and the last optional argument, backrange, specifies the
	distance behind state to start the line.  If range and backrange
	are both 20 for example, then the lane lines will start around 20
	meters behind state and extend up to the state point given.  To
	specify lanes that start 20 meters behind and extend 20 meters in
	front, specify range=40 and backrange=20.  This function will
	return left and right lane lines of equal size and will trim to
	the nearest waypoint.

Fri May 25 14:05:07 2007	Sam Pfister (sam)

	* version R1-02m
	BUGS:  
	FILES: MapPrior.cc(24989), testMapPrior.cc(24989)
	updated interpolation to make it sparser for now to help with the
	polytope creation of planning

Wed May 23 13:18:22 2007	Sam Pfister (sam)

	* version R1-02l
	BUGS:  
	FILES: Map.cc(24668), Map.hh(24668), MapElement.cc(24668),
		MapElement.hh(24668), testGeometry.cc(24668),
		testMapPrior.cc(24668)
	Updated dist functions in MapElement which calculate the min dist
	to other map elements as well as points or polylines.  Can be used
	to check for partial blocking of lanes.  These functions will
	return 0 if there is overlap between the geometries.  They haven't
	been fully tested but the interface shouldn't change.

Sat May 19 17:54:20 2007	Jessica Gonzalez (jengo)

	* version R1-02k
	BUGS:  
	FILES: MapElement.cc(24051), MapElement.hh(24051)
	added polytope and planner traj elements

Sat May 19 10:26:41 2007	Sam Pfister (sam)

	* version R1-02j
	BUGS:  
	FILES: Map.cc(23943), Map.hh(23943), MapElement.hh(23943),
		MapId.cc(23943), MapId.hh(23943), MapPrior.cc(23943),
		MapPrior.hh(23943), testMap.cc(23943),
		testMapPrior.cc(23943)
	Fixed some other issues with getTransitionBounds.  Added some new
	transition handling functionality but it's not used yet.  

Fri May 18 15:42:25 2007	Sam Pfister (sam)

	* version R1-02i
	BUGS:  
	FILES: Map.cc(23818), MapPrior.cc(23818), MapPrior.hh(23818),
		testMapPrior.cc(23818)
	changed retired getTransitionBounds function to not use ptarr
	assignment operator.  May fix a bug in tplanner.  Some intermediate
	work is done on a priori transition bounds calculations.

Fri May 18 10:16:50 2007	Sam Pfister (sam)

	* version R1-02h
	BUGS:  
	FILES: Map.cc(23757), testMap.cc(23757)
	updated getTransitionBounds to work a bit better

Fri May 18  9:46:29 2007	Sam Pfister (sam)

	* version R1-02g
	BUGS:  
	FILES: Map.cc(23747), Map.hh(23747), MapId.hh(23747),
		MapPrior.cc(23747), MapPrior.hh(23747), testMap.cc(23747),
		testMapPrior.cc(23747)
	quick fix for issue getTransitionBounds function in Map.cc has with
	santa anita rndf which loops back on itself.  need a better fix for
	long term and that function itself is going to be retired.

Thu May 17 17:31:06 2007	Sam Pfister (sam)

	* version R1-02f
	BUGS:  
	FILES: Map.cc(23586), Map.hh(23586), MapPrior.cc(23586),
		MapPrior.hh(23586), testMap.cc(23586),
		testMapPrior.cc(23586)
	reorganized MapPrior RNDF parsing, fixed bug in old function
	getObstacleDist and GetobstaclePoint.

Thu May 17 15:37:18 2007	Sam Pfister (sam)

	* version R1-02e
	BUGS:  
	FILES: MapPrior.cc(23536), MapPrior.hh(23536),
		testGeometry.cc(23536), testMap.cc(23536),
		testMapPrior.cc(23536)
	Updated MapPrior to incorporate waypoint and lane line
	interpolation.	Updated getWayPointExits and getWayPointEntries to
	return entry and exit possibilities in same lane.

	FILES: MapPrior.cc(23528), MapPrior.hh(23528),
		testGeometry.cc(23528), testMap.cc(23528),
		testMapPrior.cc(23528)
	updated interpolation functions in MapPrior.  Updated the
	getWayPointEntries and getWayPointExits functions to return the
	possible transition in the same lane across

Wed May 16 23:20:28 2007	 (ahoward)

	* version R1-02d
	BUGS:  
	FILES: MapElement.hh(23404)
	Tweaked for cross-platform compatability

Tue May 15 23:34:40 2007	Sam Pfister (sam)

	* version R1-02c
	BUGS:  
	New files: testMapPrior.cc
	FILES: Makefile.yam(23258), Map.cc(23258), Map.hh(23258),
		MapElement.cc(23258), MapElement.hh(23258),
		testMap.cc(23258)
	Fixed bug in getObs functions which would return sensed lanes as
	well as obstacles and vehicles.  Added functions isObstacle() and
	isLine() to MapElement.  Other small bug fixes.

Mon May 14 23:29:36 2007	Jessica Gonzalez (jengo)

	* version R1-02b
	BUGS:  
	FILES: Makefile.yam(23128)
	removed MapPrior

	FILES: MapElement.cc(23123), MapElement.hh(23123)
	added elements for steer commands, rddf, etc

Mon May 14 11:42:04 2007	Sam Pfister (sam)

	* version R1-02a
	BUGS:  
	FILES: Makefile.yam(22998), Map.cc(22998), Map.hh(22998),
		MapElement.cc(22998), MapElement.hh(22998),
		MapElementTalker.cc(22998), MapElementTalker.hh(22998),
		MapId.cc(22998), MapId.hh(22998), MapPrior.cc(22998),
		MapPrior.hh(22998), testGeometry.cc(22998),
		testMap.cc(22998), testRecvMapElement.cc(22998),
		testSendMapElement.cc(22998)
	Updated the map interface to a cleaner, more uniform structure. 
	Had to change the format of some map interface functions, but will
	migrate all modules which use map to the new version. Implemented a
	cleaner more complete library of RNDF and obstacle query functions
	for the planners.  Cleaned up MapElement object interface which
	might also break some code, but will be easy to fix and easier to
	use from now on.  Implemented some MapElement overlap checking
	functions which use the improved bounding box functionality on the
	MapElement.  Added an elevation term to the MapElement structure to
	handle obstacles like overhanging trees which have freespace
	underneath.  Added additional timestamp to MapElement and
	additional max and min bound terms.

Thu May 10 13:52:05 2007	Jessica Gonzalez (jengo)

	* version R1-01w
	BUGS:  
	New files: StateTalker.cc
	FILES: Makefile.yam(22690), MapElement.hh(22690)
	added program to send alice info to mapviewer

	FILES: MapElement.cc(22627), MapElement.hh(22627)
	commented everything

	FILES: MapElement.cc(22693), MapElement.hh(22693)
	added traversed path

Mon Apr 30 12:25:29 2007	Sam Pfister (sam)

	* version R1-01v
	BUGS:  
	FILES: Map.cc(21706), Map.hh(21706), MapElement.cc(21706),
		MapElementTalker.cc(21706), MapElementTalker.hh(21706),
		MapPrior.cc(21706), MapPrior.hh(21706),
		testGeometry.cc(21706), testMap.cc(21706),
		testRecvMapElement.cc(21706), testSendMapElement.cc(21706)
	Updated MapElementTalker to take advantage of new skynet
	functionality to send messages on a sub channel.  Implemented
	variable length skynet messages.  For modules which send map
	elements, no code change is needed, just a recompile against the
	updated map and frames modules.  For modules which receive map
	elements, add the intended subnet group to the initRecvMapElement
	function as follows : initRecvMapElement(skynetKey,recvChannel);
	Without this, change only elements on channel 0 will be received.  
	  Added map interface functions getLaneDistToWaypoint which
	calculate distance along corridor between two points.  Updated
	getBounds, getBoundsReverse and getTransitionBounds to return equal
	sized left and right bounds.

Sun Apr 29  0:11:57 2007	datamino (datamino)

	* version R1-01u
	BUGS:  
	FILES: MapElement.cc(21506)
	Fixing MapElement::print_state(), broken by the recent change to
	the VehicleState structure.

Wed Apr 25 17:19:17 2007	Sam Pfister (sam)

	* version R1-01t
	BUGS:  
	FILES: Map.cc(20714), Map.hh(20714), testMap.cc(20714)
	added getLaneID, getSegmentID, checkLaneID functions to tplanner
	interface with map.

Fri Mar 30 20:12:14 2007	Laura Lindzey (lindzey)

	* version R1-01s
	BUGS: 
	FILES: testGeometry.cc(19038)
	test functions for ellipse class

Fri Mar 30  2:16:48 2007	Laura Lindzey (lindzey)

	* version R1-01r
	BUGS: 
	FILES: testGeometry.cc(18403)
	code for testing point2 average 

	FILES: testGeometry.cc(18531)
	adding test for fitline

	FILES: testGeometry.cc(18533)
	added get_inside function

	FILES: testGeometry.cc(18560)
	test functions for merge and get_overlap

	FILES: testGeometry.cc(18976)
	changes to test functions

Sat Mar 17 11:21:01 2007	Sam Pfister (sam)

	* version R1-01q
	BUGS: 
	FILES: MapElement.cc(18258), MapElement.hh(18258),
		MapPrior.cc(18258), testGeometry.cc(18258),
		testMap.cc(18258)
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

Sat Mar 17  0:47:13 2007	Sam Pfister (sam)

	* version R1-01p
	BUGS: 
	FILES: Map.cc(18177), Map.hh(18177), testGeometry.cc(18177),
		testMap.cc(18177)
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

Fri Mar 16  0:27:21 2007	Sam Pfister (sam)

	* version R1-01o
	BUGS: 
	FILES: Map.cc(17906), Map.hh(17906), testMap.cc(17906)
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

Thu Mar 15 13:59:39 2007	Sam Pfister (sam)

	* version R1-01n
	BUGS: 
	FILES: Map.cc(17802), Map.hh(17802)
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

Wed Mar 14 15:57:50 2007	Sam Pfister (sam)

	* version R1-01m
	BUGS: 
	FILES: Map.cc(17594), Map.hh(17594), testMap.cc(17594)
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

Wed Mar 14 10:14:48 2007	Sam Pfister (sam)

	* version R1-01l
	BUGS: 
	FILES: Map.cc(17568), Map.hh(17568), testGeometry.cc(17568),
		testMap.cc(17568)
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount

	FILES: testGeometry.cc(17548), testMap.cc(17548)
	more geometric test functions

Tue Mar 13 15:45:16 2007	Sam Pfister (sam)

	* version R1-01k
	BUGS: 
	New files: testGeometry.cc
	FILES: Makefile.yam(17469), Map.cc(17469), Map.hh(17469),
		MapElement.cc(17469), MapElement.hh(17469),
		MapPrior.cc(17469), rndf.txt(17469), testMap.cc(17469)
	Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

Mon Mar 12 15:22:21 2007	Sam Pfister (sam)

	* version R1-01j
	BUGS: 
	FILES: Map.cc(17247), Map.hh(17247), MapElement.cc(17247),
		MapPrior.cc(17247), testMap.cc(17247)
	added getNextStopline function.  Need to debug an RNDF reversing
	issue

	FILES: MapId.cc(17226), MapId.hh(17226), testMapId.cc(17226)
	updated testMapId program and MapId class functionality

Sun Mar 11 11:49:13 2007	Sam Pfister (sam)

	* version R1-01i
	BUGS: 
	New files: testMapElement.cc testMapId.cc
	FILES: MapElement.cc(17098), MapElement.hh(17098),
		MapElementTalker.cc(17098), testSendMapElement.cc(17098)
	Added clear and alice state messages to map element.

Sat Mar 10 17:22:05 2007	Sam Pfister (sam)

	* version R1-01h
	BUGS: 
	New files: MapId.cc MapId.hh testMap.cc
	Deleted files: test_map.cc
	FILES: Makefile.yam(17025), Map.cc(17025), Map.hh(17025),
		MapElement.hh(17025), MapElementTalker.cc(17025),
		MapElementTalker.hh(17025), MapPrior.cc(17025),
		MapPrior.hh(17025), testRecvMapElement.cc(17025),
		testSendMapElement.cc(17025)
	removed tabs in place of spaces.  Fixed bug in getStopline
	function.  Moved test_map.cc to testMap.cc.

Fri Mar  9 16:33:29 2007	Sam Pfister (sam)

	* version R1-01g
	BUGS: 
	FILES: Map.cc(16900), Map.hh(16900), test_map.cc(16900)
	added isStop and isExit query functions to map.

Fri Mar  9 12:15:02 2007	Sam Pfister (sam)

	* version R1-01f
	BUGS: 
	New files: MapElement.cc MapElement.hh MapElementTalker.cc
		MapElementTalker.hh testRecvMapElement.cc
		testSendMapElement.cc
	FILES: Makefile.yam(16849), Map.hh(16849), MapPrior.hh(16849),
		test_map.cc(16849)
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

Thu Mar  8 18:32:56 2007	Noel duToit (ndutoit)

	* version R1-01e
	BUGS: 
	FILES: Map.cc(16816), Map.hh(16816), rndf.txt(16816),
		test_map.cc(16816)
	Some updates in map to interface with new tplanner.

	FILES: MapPrior.cc(16751), test_map.cc(16751)
	local changes by Sam

Sat Mar  3 15:55:28 2007	Sam Pfister (sam)

	* version R1-01d
	BUGS: 
	FILES: Map.cc(16394), Map.hh(16394), test_map.cc(16394)
	Updated getStopline function to take only a point in space not a
	stopline label

Thu Mar  1 19:33:50 2007	Sam Pfister (sam)

	* version R1-01c
	BUGS: 
	New files: rndf.txt test_map.cc
	FILES: Makefile.yam(16013), Map.cc(16013), Map.hh(16013),
		MapPrior.cc(16013), MapPrior.hh(16013)
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results

	FILES: Map.cc(15922), Map.hh(15922)
	added base query function declarations to Map.hh .

	FILES: Map.cc(15999), Map.hh(15999), MapPrior.cc(15999),
		MapPrior.hh(15999)
	Implemented the getLeftBound and getRightBound functions.  Still
	need intersection transition.

Tue Feb 27  8:39:02 2007	Sam Pfister (sam)

	* version R1-01b
	BUGS: 
	New files: MapPrior.cc MapPrior.hh
	Deleted files: LocalMapTalker.cc LocalMapTalker.hh
		LogicalRoadObject.cc LogicalRoadObject.hh RoadObject.cc
		RoadObject.hh
	FILES: Makefile.yam(15900), Map.cc(15900), Map.hh(15900)
	Moved new map structure from mapper into map module.  Map now
	parses RNDF files with an internal method.

Sun Feb 25 12:56:24 2007	murray (murray)

	* version R1-01a
	BUGS: 
	FILES: LocalMapTalker.cc(15691), LocalMapTalker.hh(15691)
	updated to work with new interfaces release

2007-02-25  murray  <murray@gclab.dgc.caltech.edu>

	* LocalMapTalker.hh: updated SkynetContainer path
	* LocalMapTalker.cc: added interfaces/sn_types.h
	(CSkynetContainer): updated modName to type int

Thu Feb  1 19:26:58 2007	Sam Pfister (sam)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(13982)
	commented out LIB_MODULE_LINKS := libmap line in Makefile.yam.	It
	was making a broken link.

	FILES: Map.hh(13981)
	Adding simpler endpoint data structure to map for short term use.  

Sat Jan 27 18:25:27 2007	Nok Wongpiromsarn (nok)

	* version R1-00a
	BUGS: 
	New files: LocalMapTalker.cc LocalMapTalker.hh LogicalRoadObject.cc
		LogicalRoadObject.hh Map.cc Map.hh RoadObject.cc
		RoadObject.hh
	FILES: Makefile.yam(13230)
	Added Map class and LocalMapTalker

Sat Jan 27 18:14:18 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created map module.





