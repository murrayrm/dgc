/**********************************************************
 **
 **  MAPELEMENT.CC
 **
 **    Time-stamp: <2007-05-15 17:58:31 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Mar  9 10:57:28 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapElement.hh"

using namespace std;

MapElement::MapElement()
{
  // Note: do we need this??
  clear();
}


void  MapElement::clear(){
  subgroup = 0;
  id.clear();
  conf = 0;
  type = ELEMENT_UNDEF;
  typeConf = 0;
  position.clear();
  center.clear();
  length = -1;
  width = -1;
  orientation =0;
  lengthVar = 0;
  widthVar = 0;
  orientationVar = 0;
  height =  -1;
  heightVar = 0;
  elevation =  -1;
  elevationVar = 0;
  velocity.clear();
  peakSpeed=-1;
  peakSpeedVar=0;
  peakAccel=-1;
  peakAccelVar=0;

  frameType = FRAME_UNDEF;
  timestamp=0;
  memset(&state, 0, sizeof(state));
  plotColor = MAP_COLOR_GREY;
  plotValue = 100;
  label.clear();

  geometryMin.clear();
  geometryMax.clear();
  geometryType = GEOMETRY_UNDEF;
  geometry.clear();
}

void MapElement::setTypeClear()
{
  type=ELEMENT_CLEAR;
}

void MapElement::setTypeUndef()
{
  geometryType=GEOMETRY_UNDEF;
  type=ELEMENT_UNDEF;
}
void MapElement::setTypeAlice()
{
  geometryType=GEOMETRY_POLY;
  type=ELEMENT_ALICE;
}


void MapElement::setTypePoints()
{
  geometryType = GEOMETRY_POINTS;
  type=ELEMENT_POINTS;
}
void MapElement::setTypeWayPoints()
{
  geometryType = GEOMETRY_ORDERED_POINTS;
  type=ELEMENT_WAYPOINTS;
  plotColor = MAP_COLOR_BLUE;
  plotValue = 50;     
}
void MapElement::setTypeCheckPoints()
{
  geometryType = GEOMETRY_ORDERED_POINTS;
  type=ELEMENT_CHECKPOINTS;
}


void MapElement::setTypeLine()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_LINE;
}
void MapElement::setTypeStopLine()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_STOPLINE;
}
void MapElement::setTypeLaneLine()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_LANELINE;
  plotColor = MAP_COLOR_BLUE;
  plotValue = 100;     
}


void MapElement::setTypeParkingSpot()
{
  geometryType = GEOMETRY_ORDERED_POINTS;
  type=ELEMENT_PARKING_SPOT;
}
void MapElement::setTypePerimeter()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_PERIMETER;
}


void MapElement::setTypeObstacle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_OBSTACLE;
}
void MapElement::setTypeObstacleEdge()
{
  geometryType = GEOMETRY_EDGE;
  type=ELEMENT_OBSTACLE_EDGE;
}
void MapElement::setTypeVehicle()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_VEHICLE;
}

void MapElement::setTypePoly()
{
  geometryType = GEOMETRY_POLY;
  type=ELEMENT_POLY;
}

void MapElement::setTypeTravPath()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_TRAV_PATH;
  plotColor = MAP_COLOR_RED;
}

void MapElement::setTypePlanningTraj()
{
  geometryType = GEOMETRY_LINE;
  type=ELEMENT_PLANNING_TRAJ;
  plotColor=MAP_COLOR_MAGENTA;
}

void MapElement::setTypeRDDF()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_RDDF;
  plotColor = MAP_COLOR_LIGHT_BLUE;
}

void MapElement::setTypeSteerCmd()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_STEER_CMD;
  plotColor = MAP_COLOR_GREEN;
}

void MapElement::setTypeSteer()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_VEH_STEER;
  plotColor = MAP_COLOR_BLUE_2;
}

void MapElement::setTypePolytope()
{
  geometryType = GEOMETRY_LINE;
  type = ELEMENT_POLYTOPE;
  plotColor = MAP_COLOR_YELLOW;
}

void MapElement::setGeometry(point2arr_uncertain &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}
 
void MapElement::setGeometry(point2arr &ptarr)
{
  geometry=ptarr;
  updateFromGeometry();
}

void MapElement::setGeometry(point2 &pt)
{
  point2arr ptarr(pt);
  geometry = ptarr;
  updateFromGeometry();
}
void MapElement::setGeometry(point2_uncertain &pt)
{
  geometry.clear();
  geometry.push_back(pt);
  updateFromGeometry();
}

void MapElement::setGeometry(point2 &pt, double radius)
{
  point2_uncertain pt_unc(pt);
  setGeometry(pt_unc,radius);
}
void MapElement::setGeometry(point2_uncertain &pt, double radius)
{
  geometry.clear();
  int numpts = 20;
  point2 tmppt(radius,0);
  for (int i=0;i<numpts;++i){
    tmppt =tmppt.rot(2*M_PI/(numpts));
    geometry.push_back(pt+tmppt);
  }
  updateFromGeometry();
}

void MapElement::setGeometry(point2 &pt, double len, double wid, double orient)
{
  point2_uncertain pt_unc(pt);
  setGeometry(pt_unc,len,wid,orient);
}
void MapElement::setGeometry(point2_uncertain &pt, double len, double wid, double orient)
{
  point2_uncertain tmppt;
 
  geometry.clear();
 tmppt.set(len/2,wid/2);
 geometry.push_back(pt+tmppt);

 tmppt.set(len/2,-wid/2);
geometry.push_back(pt+tmppt);
   
 tmppt.set(-len/2,-wid/2);
geometry.push_back(pt+tmppt);

 tmppt.set(-len/2,wid/2);
 geometry.push_back(pt+tmppt);
 geometry = geometry.rot(orient);
 updateFromGeometry();
}



void MapElement::setGeometry(point2 &pta,point2 &ptb)
{
  geometry.clear();
  geometry.push_back(pta);
  geometry.push_back(ptb);
  updateFromGeometry();
}
void MapElement::setGeometry(point2_uncertain &pta,point2_uncertain &ptb)
{
  geometry.clear();
  geometry.push_back(pta);
  geometry.push_back(ptb);
  updateFromGeometry();
}


void MapElement::setGeometry(vector<point2> &ptarr){
  point2arr tmpptarr(ptarr);
  geometry = tmpptarr;
  updateFromGeometry();  
}
void MapElement::setGeometry(vector<point2_uncertain> &ptarr)
{
 geometry=ptarr;
  updateFromGeometry();  
}

void MapElement::setGeometry(vector<double> &xarr, vector<double> &yarr)
{
  int sze1 = xarr.size();
  int sze2 = yarr.size();
  if (sze1!=sze2){
    cerr << "in MapElement::setGeometry mismatched sizes of x, y arrays given"<<endl;
    return;
  }
  geometry.clear();
  point2_uncertain pt;
  for (int i=0;i<sze1; ++i){
    pt.x = xarr[i];
    pt.y = yarr[i];
    geometry.push_back(pt);
  }
  updateFromGeometry();  
}


void MapElement::setPosition(point2 cpt)
{
  position.clear();
  position = cpt;
}

void MapElement::setPosition(point2_uncertain cpt)
{
  position.clear();
  position = cpt;
}
 
void MapElement::setPosition(double x, double y)
{
  position.clear();
  position.x = x;
  position.y = y;
}
 

void MapElement::setCenterBoundsFromGeometry(){
  point2arr ptarr(geometry);
  point2 cpt;
  // cout <<"getting bounds " << ptarr << endl;
  ptarr.get_bound_box(cpt,length,width,orientation);
  // cout << "cnetr " << cpt 
  //      << "  len " << length
  //     << "  wid " << width
  //     << "  orin " << orientation << endl;
  center = cpt;

  //--------------------------------------------------
  // still need to define the variance terms using point uncertainty
  //--------------------------------------------------
  return;
}

void MapElement::setPositionFromGeometry(){
  int i;
  int size = (int)geometry.size();
  if (size==0)
    return;
  
  double fulldist = 0;
  double dist = 0;
  point2 dpt;
  double ratio = 1;
  point2 sum(0,0);
  int lowindex =0,highindex=0;

  
  switch (geometryType) {
  case GEOMETRY_UNDEF:
  case GEOMETRY_POINTS:
    position = geometry[0];
    return;
  case GEOMETRY_ORDERED_POINTS:
  case GEOMETRY_LINE:
  case GEOMETRY_EDGE:
    if (size==1){
      position = geometry[0];
      return;
    }
    for (i = 0; i < size-1; ++i){
      dpt = geometry[i+1]-geometry[i];
      fulldist = fulldist+dpt.norm();
    }
      
    for (i = 0; i < size-1; ++i){
      dpt = geometry[i+1]-geometry[i];
        
      if (dist < fulldist/2 &&
          dist+dpt.norm() >= fulldist/2){
        lowindex = i;
        highindex = i+1;
        
        break;
      }
      dist = dist+dpt.norm();
    }
    ratio = (fulldist/2-dist)/dpt.norm();
    position = geometry[lowindex]+dpt*ratio;
    return;
  case GEOMETRY_POLY:
    for (i = 0; i < size; ++i){
      sum = sum+geometry[i];
    }
    position = sum/size;
    return;
    
  }
}
void MapElement::setMinMax(double minx, double miny, double maxx, double maxy)
{
  geometryMin.x = minx;
  geometryMin.y = miny;
  geometryMax.x = maxx;
  geometryMax.y = maxy;

}

void MapElement::setMinMaxFromGeometry()
{

  int size = (int)geometry.size();
  if (size==0){
    geometryMin.clear();
    geometryMax.clear();
    return;
  }

  geometryMin = geometry.min();
  geometryMax = geometry.max();

  return;
}

void MapElement::updateFromGeometry()
{
  setCenterBoundsFromGeometry();
  setPositionFromGeometry();
  setMinMaxFromGeometry();
}

bool MapElement::isOverlap(const MapElement& el) const
{
  if (geometryMax.x < el.geometryMin.x) 
    return false;

  if (geometryMin.x > el.geometryMax.x) 
    return false;

  if (geometryMax.y < el.geometryMin.y) 
    return false;

  if (geometryMin.y > el.geometryMax.y) 
    return false;

  point2arr thisptarr,thatptarr;
  thisptarr = geometry;
  thatptarr = el.geometry;

  if (geometryType == GEOMETRY_LINE){
    if (el.geometryType ==GEOMETRY_LINE){
      return (thisptarr.is_intersect(thatptarr));
    }else if (el.geometryType == GEOMETRY_POLY){
      return (thisptarr.is_line_poly_overlap(thatptarr));
    }else if (el.geometryType == GEOMETRY_POINTS||
              el.geometryType == GEOMETRY_ORDERED_POINTS){
      return false;
    } else if (el.geometryType == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_intersect(thatptarr));
    }
    //doesnt handle GEOMETRY_UNDEF here
  }else if (geometryType == GEOMETRY_POLY){
    if (el.geometryType ==GEOMETRY_LINE){
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }else if (el.geometryType == GEOMETRY_POLY){
      return (thisptarr.is_poly_overlap(thatptarr));
    }else if (el.geometryType == GEOMETRY_POINTS||
              el.geometryType == GEOMETRY_ORDERED_POINTS){
      return (thisptarr.is_poly_point_overlap(thatptarr));
    }else if (el.geometryType == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }


  }else if (geometryType == GEOMETRY_POINTS ||
            geometryType == GEOMETRY_ORDERED_POINTS){
    if (el.geometryType ==GEOMETRY_LINE){
      return false;
    }else if (el.geometryType == GEOMETRY_POLY){
      return (thisptarr.is_point_poly_overlap(thatptarr));
    }else if (el.geometryType == GEOMETRY_POINTS||
              el.geometryType == GEOMETRY_ORDERED_POINTS){
      return false;
    }else if (el.geometryType == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_intersect(thatptarr));
    }


  }else if (geometryType == GEOMETRY_EDGE){
    if (el.geometryType ==GEOMETRY_LINE){
      return (thisptarr.is_intersect(thatptarr));
    }else if (el.geometryType == GEOMETRY_POLY){
      return (thisptarr.is_line_poly_overlap(thatptarr));
    }else if (el.geometryType == GEOMETRY_POINTS||
              el.geometryType == GEOMETRY_ORDERED_POINTS){
      return false;
    }else if (el.geometryType == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return false;
    }

  }

  cout <<"in MapElement::check_overlap, checking element overlap with UNDEF geometry in map element(s). Returning false" << endl;
  return false;
}

bool MapElement::isOverlap(const point2 &pt) const
{
  if (geometryType != GEOMETRY_POLY)
    return false;

  if (geometryMax.x < pt.x) 
    return false;
  if (geometryMin.x > pt.x) 
    return false;
  if (geometryMax.y < pt.y) 
    return false;
  if (geometryMin.y > pt.y) 
    return false;

  point2arr ptarr(geometry);
    return (ptarr.is_poly_point_overlap(pt));
}
bool MapElement::isOverlap(const point2_uncertain &pt) const
{
  point2 tmppt(pt);
  return isOverlap(tmppt);
}

bool MapElement::isOverlap(const point2arr &ptarr, const MapElementGeometryType type) const 
{
  point2 minpt;
  point2 maxpt;
  minpt = ptarr.min();
  maxpt = ptarr.max();
  if (geometryMax.x < minpt.x) 
    return false;

  if (geometryMin.x > maxpt.x) 
    return false;

  if (geometryMax.y < minpt.y) 
    return false;

  if (geometryMin.y > maxpt.y) 
    return false;

  point2arr thisptarr,thatptarr;
  thisptarr = geometry;
  thatptarr = ptarr;

  if (geometryType == GEOMETRY_LINE){
    if (type ==GEOMETRY_LINE){
      return (thisptarr.is_intersect(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_line_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return false;
    } else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_intersect(thatptarr));
    }
    //doesnt handle GEOMETRY_UNDEF here
  }else if (geometryType == GEOMETRY_POLY){
    if (type ==GEOMETRY_LINE){
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return (thisptarr.is_poly_point_overlap(thatptarr));
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_poly_line_overlap(thatptarr));
    }


  }else if (geometryType == GEOMETRY_POINTS ||
            geometryType == GEOMETRY_ORDERED_POINTS){
    if (type ==GEOMETRY_LINE){
      return false;
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_point_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return false;
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return (thisptarr.is_intersect(thatptarr));
    }


  }else if (geometryType == GEOMETRY_EDGE){
    if (type ==GEOMETRY_LINE){
      return (thisptarr.is_intersect(thatptarr));
    }else if (type == GEOMETRY_POLY){
      return (thisptarr.is_line_poly_overlap(thatptarr));
    }else if (type == GEOMETRY_POINTS||
              type == GEOMETRY_ORDERED_POINTS){
      return false;
    }else if (type == GEOMETRY_EDGE){
      cout <<"in MapElement::check_overlap, need to handle GEOMETRY_EDGE better" << endl;
      return false;
    }

  }

  cout <<"in MapElement::check_overlap, checking element overlap with UNDEF geometry in map element(s). Returning false" << endl;
  return false;
}

bool MapElement::isOverlap(const point2arr_uncertain &ptarr, const MapElementGeometryType type) const  
{
  point2arr tmpptarr(ptarr);
  return (isOverlap(tmpptarr,type));
}



ostream &operator<<(ostream &os, const MapElement &el)
{
  unsigned int i;
  os << endl<<"------Start Printing MapElement------" << endl;
  os << "subgroup= " << el.subgroup << endl;
  os << "id= " << el.id << endl;
  os << " conf= " << el.conf 
     << " type= " << el.type
     << " typeConf= " << el.typeConf << endl;
  os << "isObstacle= " << el.isObstacle() 
     << " isLine= " << el.isLine() << endl;
  os << "position: " << el.position << endl;
  os << "center:  " << el.center << endl;
  os << "length= " << el.length
     << " width= " << el.width
     << " orientation= " << el.orientation << endl;
  os << "lengthVar= " << el.lengthVar
     << " widthVar= " << el.widthVar
     << " orientationVar= " << el.orientationVar << endl;
  os << "geometryType= " << el.geometryType <<endl;
  os << "geometry :  " << el.geometry << endl;
  os << "geometryMin :  " << el.geometryMin << endl;
  os << "geometryMax :  " << el.geometryMax << endl;
   

  os <<"height= " << el.height 
       << " heightVar= " << el.heightVar << endl;
  os <<"elevation= " << el.elevation 
       << " elevationVar= " << el.elevationVar << endl;

  os << "velocity:  " << el.velocity << endl ;
os <<"peakSpeed= " << el.peakSpeed 
       << " peakSpeedVar= " << el.peakSpeedVar << endl;
os <<"peakAccel= " << el.peakAccel 
       << " peakAccelVar= " << el.peakAccelVar << endl;
  

  os <<"frame= " << el.frameType << endl;
  os <<"timestamp= " << el.timestamp << endl;
  

  os <<"plotColor= " << el.plotColor << endl;
  os <<"plotValue= " << el.plotValue << endl;
  
  
  for (i = 0; i < el.label.size(); ++i){
    os << "label line " << i << ":  " << el.label[i]<< endl;
  }
  os << "------End Printing MapElement--------" << endl;
  return os;
}
 ostream &operator<<(ostream &os, const VehicleState &state)
{
  
  os << endl<<"------Start Printing VehicleState------" << endl;
  os << "timestamp= " << state.timestamp << endl;
  os << "utmNorthing= " << state.utmNorthing 
       << " utmEasting= " << state.utmEasting 
       << " utmAltitude= " << state.utmAltitude 
       << endl;

  os << "GPS_Northing_deprecated= " << state.GPS_Northing_deprecated
       << " GPS_Easting_deprecated= " << state.GPS_Easting_deprecated 
       << endl;

  os << "utmNorthVel= " << state.utmNorthVel 
       << " utmEastVel= " << state.utmEastVel 
       << " utmAltitudeVel= " << state.utmAltitudeVel 
       << endl;

  os << "Acc_N_deprecated= " << state.Acc_N_deprecated
       << "Acc_E_deprecated= " << state.Acc_E_deprecated
       << "Acc_D_deprecated= " << state.Acc_D_deprecated
       << endl;

  os << "utmRoll= " << state.utmRoll 
       << " utmPitch= " << state.utmPitch 
       << " utmYaw= " << state.utmYaw 
       << endl;

  os << "utmRollRate= " << state.utmRollRate 
       << " utmPitchRate= " << state.utmPitchRate 
       << " utmYawRate= " << state.utmYawRate 
       << endl;

  os << "raw_YawRate_deprecated= " << state.raw_YawRate_deprecated 
       << " RollAcc_deprecated= " << state.RollAcc_deprecated 
       << endl;
 
  
  os << "PitchAcc_deprecated= " << state.PitchAcc_deprecated
       << " YawAcc_deprecated= " << state.YawAcc_deprecated 
       << endl;

  os << "utmNorthConfidence= " << state.utmNorthConfidence  
       << " utmEastConfidence= " << state.utmEastConfidence 
       << " utmAltitudeConfidence= " <<  state.utmAltitudeConfidence  
       << endl;

  os << "rollConfidence= " << state.rollConfidence 
       << " pitchConfidence= " << state.pitchConfidence
       << " yawConfidence= " << state.yawConfidence
       << endl;

  os << "gpsGamma= " << state.vehSpeed
       << " utmZone= " << state.utmZone 
       << " utmLetter= " << state.utmLetter
       << endl;

  os << "vehXVel= " << state.vehXVel 
       << " vehYVel= " << state.vehYVel 
       << " vehZVel= " << state.vehZVel
       << endl;
  
  os << "vehRollRate= " << state.vehRollRate 
       << " vehPitchRate= " << state.vehPitchRate 
       << " vehYawRate= " << state.vehYawRate
       << endl;


  os << "localX= " << state.localX 
       << " localY= " << state.localY 
       << " localZ= " << state.localZ
       << endl;

  os << "localRoll= " << state.localRoll 
       << " localPitch= " << state.localPitch 
       << " localYaw= " << state.localYaw
       << endl;

  os << "localXVel= " << state.localXVel 
       << " localYVel= " << state.localYVel 
       << " localZVel= " << state.localZVel
       << endl;

  os << "localRollRate= " << state.localRollRate 
       << " localPitchRate= " << state.localPitchRate 
       << " localYawRate= " << state.localYawRate
       << endl; 

  os << "------End Printing VehicleState--------" << endl;
  return os;
}

















//--------------------------------------------------
// old stuff
//--------------------------------------------------


void MapElement::set_geometry(point2arr_uncertain &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}
 
void MapElement::set_geometry(point2arr &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}

void MapElement::set_geometry(point2 &pt)
{
  geometry.clear();
  geometry.push_back(pt);
  updateFromGeometry();

}

void MapElement::set_geometry(point2 &pt, double radius)
{
  geometry.clear();
  geometry.push_back(pt);
  updateFromGeometry();
  width = radius;
  length = radius;

}

void MapElement::set_geometry(vector<point2> &ptarr){
  point2arr tmparr;
  tmparr = ptarr;
  geometry = tmparr;
  updateFromGeometry();
}
void MapElement::set_geometry(vector<point2_uncertain> &ptarr)
{
  geometry = ptarr;
  updateFromGeometry();
}

void MapElement::set_geometry(vector<double> &xarr, vector<double> &yarr)
{
  // NOT IMPLEMENTED!!
}

void  MapElement::set_clear(vector<int>& ident)
{
  clear();
  id = ident;
  type = ELEMENT_CLEAR;
  
}
void  MapElement::set_alice(VehicleState &statein)
{
  clear();
  id = 0;
  type = ELEMENT_ALICE;
  state = statein;
}
void  MapElement::set_circle_obs(vector<int>& ident, point2 cpt, double radius){
  id = ident;
  center = cpt;
  length = radius;
  width = radius;
}
void  MapElement::set_circle_obs()
{
  height = 10;
  type = ELEMENT_OBSTACLE;
}

void  MapElement::set_poly_obs(vector<int>& ident, vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 10;
  type = ELEMENT_OBSTACLE;
  //center_type = CENTER_BOUND_BOX;
  geometryType = GEOMETRY_POLY;
  updateFromGeometry();
}
void  MapElement::set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid ){
  id = ident;
  center.x = cpt.x;
  center.y = cpt.y;
  orientation = ang;
  length = len;
  width = wid;
  height = 10;
  type = ELEMENT_OBSTACLE;
   geometryType = GEOMETRY_UNDEF;
}
void  MapElement::set_line(vector<int>& ident,  vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 0;
  set_line();
  updateFromGeometry();
}
void  MapElement::set_line(){
  type = ELEMENT_LINE;
  geometryType = GEOMETRY_LINE;

}

void  MapElement::set_stopline(vector<int>& ident, vector<point2>& ptarr){
  clear();
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }  
  height = 0;
  type = ELEMENT_STOPLINE;
  geometryType = GEOMETRY_LINE;
  updateFromGeometry();
}



void  MapElement::set_laneline(vector<int>& ident, vector<point2>& ptarr){
  
  setId(ident);
  set_geometry(ptarr);
  set_laneline();

  plotColor = MAP_COLOR_BLUE;

  
}

void  MapElement::set_laneline(){
 
  height = 0;
  type = ELEMENT_LANELINE;
  geometryType = GEOMETRY_LINE;
  //  plot_color = MAP_COLOR_BLUE;
  //updateFromGeometry();
  
}

void  MapElement::set_points(vector<int>& ident, vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }  
  type = ELEMENT_POINTS;
  geometryType = GEOMETRY_POINTS;
  updateFromGeometry();

}
void  MapElement::set_points(){
  height = 0;
  type = ELEMENT_POINTS;
  geometryType = GEOMETRY_POINTS;
}

void  MapElement::set_trav_path(vector<int>& ident,  vector<point2>& ptarr){

  type = ELEMENT_TRAV_PATH;
  geometryType = GEOMETRY_LINE;
  plotColor = MAP_COLOR_RED;
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 0;
  set_line();
  updateFromGeometry();
}
