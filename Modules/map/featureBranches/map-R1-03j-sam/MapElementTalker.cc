/**********************************************************
 **
 **  MAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-07-17 04:11:44 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:05:13 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapElementTalker.hh"
using namespace std;

CMapElementTalker::CMapElementTalker()
{
  skynet_ptr = NULL;
  basemsgsize = sizeof(this->mapElementMsgOut);
  baseptsize = sizeof(this->mapElementMsgOut.geometry[0]);
}

int CMapElementTalker::initSendMapElement(  int snkey,
  modulename snname)
{
  if (!skynet_ptr)
    skynet_ptr = new skynet(snname, snkey);

  mapElementSendSocket = this->skynet_ptr->get_send_sock(SNmapElement);
 basemsgsize = sizeof(this->mapElementMsgOut);
  baseptsize = sizeof(this->mapElementMsgOut.geometry[0]);

  return(0);
}

int CMapElementTalker::initRecvMapElement(int snkey, modulename snname)
{
  if (!skynet_ptr)
    skynet_ptr = new skynet(snname, snkey);

  mapElementRecvSocket= this->skynet_ptr->listen(SNmapElement,snname);
 basemsgsize = sizeof(this->mapElementMsgOut);
  baseptsize = sizeof(this->mapElementMsgOut.geometry[0]);

  return(mapElementRecvSocket);
}

int CMapElementTalker::initRecvMapElement(int snkey, int subgroup, modulename snname)
{
  if (!skynet_ptr)
    skynet_ptr = new skynet(snname, snkey);

  mapElementRecvSocket= this->skynet_ptr->listen_channel(SNmapElement,subgroup);
  return(mapElementRecvSocket);
}

int CMapElementTalker::sendMapElement(MapElement* pMapElement, int subgroup)
{
  if (!skynet_ptr){
    cerr << "in CMapElementTalker - Need to initialize talker" << endl;
    return 0;
  }
  pMapElement->subgroup = subgroup;
  setMapElementMsgOut(pMapElement);

   int numpts = pMapElement->geometry.size();
   int newsize = basemsgsize-(MAP_ELEMENT_NUMPTS-numpts)*baseptsize;
 
  return(this->skynet_ptr->send_msg_channel(this->mapElementSendSocket,
                                 subgroup,&this->mapElementMsgOut,
                                 newsize,0));
  //                                sizeof(this->mapElementMsgOut),0));
}


int CMapElementTalker::recvMapElementBlock(MapElement* pMapElement, int subgroup)
{
 if (!skynet_ptr){
    cerr << "in CMapElementTalker - Need to initialize talker" << endl;
    return 0;
  }
  int readval = 0;
  int size = 0;
  
  size = this->skynet_ptr->get_msg_size(this->mapElementRecvSocket);
  // cout << "in MapElementTalker - size recv = " << size << endl;
       readval = this->skynet_ptr->get_msg(this->mapElementRecvSocket,
                                     &this->mapElementMsgIn,
                                     size,0);
    
       //  cout << "in MapElementTalker - readval = " << size << endl;

  setMapElementMsgIn(pMapElement);

  return readval;
}


int CMapElementTalker::recvMapElementNoBlock(MapElement* pMapElement, int subgroup)
{
  if (!skynet_ptr){
    cerr << "in MapElementTalker - Need to initialize talker" << endl;
    return 0;
  }
  int size = 0;
  int readval = 0;
   //  while(1){
  if (this->skynet_ptr->is_msg(this->mapElementRecvSocket)){
    size = this->skynet_ptr->get_msg_size(this->mapElementRecvSocket);
    //cout << "in MapElementTalker - size recv = " << size << endl; 
   readval = this->skynet_ptr->get_msg(this->mapElementRecvSocket,
                                        &this->mapElementMsgIn,
                                        size,0);
   setMapElementMsgIn(pMapElement);
 
  }
  return(readval);
}




int CMapElementTalker::setMapElementMsgOut(MapElement* pMapElement)
{
  int i,j;

  //--------------------------------------------------
  // truncate id if needed
  //--------------------------------------------------
  if (pMapElement->id.size() > MAP_ELEMENT_ID_LENGTH){
    this->mapElementMsgOut.numIds = MAP_ELEMENT_ID_LENGTH;
    cerr << "in MapElementTalker.cc, Truncating Map Element id length from "
         << pMapElement->id.size() << " to " << MAP_ELEMENT_ID_LENGTH<< endl;
  } 
  else
    this->mapElementMsgOut.numIds = pMapElement->id.size();

  for (i = 0; i<mapElementMsgOut.numIds;++i){
    this->mapElementMsgOut.id[i] = pMapElement->id[i];
  }


  this->mapElementMsgOut.subgroup = pMapElement->subgroup;
  this->mapElementMsgOut.conf = pMapElement->conf;
  this->mapElementMsgOut.type = pMapElement->type;
  this->mapElementMsgOut.typeConf = pMapElement->typeConf;
this->mapElementMsgOut.position.x = pMapElement->position.x;
  this->mapElementMsgOut.position.y = pMapElement->position.y;
  this->mapElementMsgOut.position.min_var = pMapElement->position.min_var;
  this->mapElementMsgOut.position.max_var = pMapElement->position.max_var;
  this->mapElementMsgOut.position.axis = pMapElement->position.axis;


this->mapElementMsgOut.center.x = pMapElement->center.x;
  this->mapElementMsgOut.center.y = pMapElement->center.y;
  this->mapElementMsgOut.center.min_var = pMapElement->center.min_var;
  this->mapElementMsgOut.center.max_var = pMapElement->center.max_var;
  this->mapElementMsgOut.center.axis = pMapElement->center.axis;
  
  this->mapElementMsgOut.length = pMapElement->length;
  this->mapElementMsgOut.width = pMapElement->width;
  this->mapElementMsgOut.orientation = pMapElement->orientation;
  this->mapElementMsgOut.lengthVar = pMapElement->lengthVar;
  this->mapElementMsgOut.widthVar = pMapElement->widthVar;
  this->mapElementMsgOut.orientationVar = pMapElement->orientationVar;



  this->mapElementMsgOut.height = pMapElement->height;
  this->mapElementMsgOut.heightVar = pMapElement->heightVar;

 this->mapElementMsgOut.elevation = pMapElement->elevation;
  this->mapElementMsgOut.elevationVar = pMapElement->elevationVar;

  this->mapElementMsgOut.velocity.x = pMapElement->velocity.x;
  this->mapElementMsgOut.velocity.y = pMapElement->velocity.y;
  this->mapElementMsgOut.velocity.max_var = pMapElement->velocity.max_var;
  this->mapElementMsgOut.velocity.min_var = pMapElement->velocity.min_var;
  this->mapElementMsgOut.velocity.axis = pMapElement->velocity.axis;
this->mapElementMsgOut.timeStopped = pMapElement->timeStopped;  
this->mapElementMsgOut.peakSpeed = pMapElement->peakSpeed;
  this->mapElementMsgOut.peakSpeedVar = pMapElement->peakSpeedVar;
  this->mapElementMsgOut.peakAccel = pMapElement->peakAccel;     
  this->mapElementMsgOut.peakAccelVar = pMapElement->peakAccelVar;     

  this->mapElementMsgOut.frameType = pMapElement->frameType;
  this->mapElementMsgOut.timestamp = pMapElement->timestamp;
  this->mapElementMsgOut.state = pMapElement->state;

  this->mapElementMsgOut.plotColor= pMapElement->plotColor;
  this->mapElementMsgOut.plotValue= pMapElement->plotValue;

  //--------------------------------------------------
  // truncate label if needed
  //--------------------------------------------------
  if (pMapElement->label.size() > MAP_ELEMENT_LABEL_NUMLINES){
    this->mapElementMsgOut.numLines = MAP_ELEMENT_LABEL_NUMLINES;
    cerr << "in MapElementTalker.cc, Truncating Map Element label vector length from "
         << pMapElement->label.size() << " to " 
         << MAP_ELEMENT_LABEL_NUMLINES<< endl;
  } 
  else
    this->mapElementMsgOut.numLines = pMapElement->label.size();

  ///cout << "label size = " << pMapElement->label.size() << endl;
  for (i = 0; i<mapElementMsgOut.numLines;++i){
    ///cout << " out line " << i << " = " << pMapElement->label[i] << endl;
    this->mapElementMsgOut.label[i][MAP_ELEMENT_LABEL_LENGTH-1] = '\0';
    for (j = 0; j<(MAP_ELEMENT_LABEL_LENGTH-1); ++j){
      if (j>=(int)pMapElement->label[i].size()){
        this->mapElementMsgOut.label[i][j] = '\0';
        break;
      }
      this->mapElementMsgOut.label[i][j] = pMapElement->label[i][j];
    }
    
  }


this->mapElementMsgOut.geometryMin.x = pMapElement->geometryMin.x;
  this->mapElementMsgOut.geometryMin.y = pMapElement->geometryMin.y;
  this->mapElementMsgOut.geometryMin.max_var = pMapElement->geometryMin.max_var;
  this->mapElementMsgOut.geometryMin.min_var = pMapElement->geometryMin.min_var;
  this->mapElementMsgOut.geometryMin.axis = pMapElement->geometryMin.axis;

this->mapElementMsgOut.geometryMax.x = pMapElement->geometryMax.x;
  this->mapElementMsgOut.geometryMax.y = pMapElement->geometryMax.y;
  this->mapElementMsgOut.geometryMax.max_var = pMapElement->geometryMax.max_var;
  this->mapElementMsgOut.geometryMax.min_var = pMapElement->geometryMax.min_var;
  this->mapElementMsgOut.geometryMax.axis = pMapElement->geometryMax.axis;


  this->mapElementMsgOut.geometryType = pMapElement->geometryType;
  //--------------------------------------------------
  // truncate geometry if needed
  //--------------------------------------------------
  if (pMapElement->geometry.size() > MAP_ELEMENT_NUMPTS){
    this->mapElementMsgOut.numPts = MAP_ELEMENT_NUMPTS;
    cerr << "in MapElementTalker.cc, Truncating Map Element geometry vector length from "
         << pMapElement->geometry.size() << " to " << MAP_ELEMENT_NUMPTS<< endl;
  } 
  else
    this->mapElementMsgOut.numPts = pMapElement->geometry.size();
  
  for (i = 0; i<mapElementMsgOut.numPts;++i){
    pt =pMapElement->geometry[i];
    ptmsg.x = pt.x;
    ptmsg.y = pt.y;
    ptmsg.z = pt.z;
    ptmsg.max_var = pt.max_var;
    ptmsg.min_var = pt.min_var;
    ptmsg.axis = pt.axis;
    this->mapElementMsgOut.geometry[i] = ptmsg;
  }
  
  return 0;
}

int CMapElementTalker::setMapElementMsgIn(MapElement* pMapElement)
{
  pMapElement->id.clear();
  for (int i = 0; i<mapElementMsgIn.numIds;++i){
    //cout << i;
    pMapElement->id.push_back(this->mapElementMsgIn.id[i]);
  }

  pMapElement->subgroup = this->mapElementMsgIn.subgroup;
  pMapElement->conf = this->mapElementMsgIn.conf;
  pMapElement->type = (MapElementType)this->mapElementMsgIn.type;
  pMapElement->typeConf = this->mapElementMsgIn.typeConf;
pMapElement->position.x = this->mapElementMsgIn.position.x;
  pMapElement->position.y = this->mapElementMsgIn.position.y;
  pMapElement->position.max_var = this->mapElementMsgIn.position.max_var;
  pMapElement->position.min_var = this->mapElementMsgIn.position.min_var;
  pMapElement->position.axis = this->mapElementMsgIn.position.axis;
  
  
  pMapElement->center.x = this->mapElementMsgIn.center.x;
  pMapElement->center.y = this->mapElementMsgIn.center.y;
  pMapElement->center.max_var = this->mapElementMsgIn.center.max_var;
  pMapElement->center.min_var = this->mapElementMsgIn.center.min_var;
  pMapElement->center.axis = this->mapElementMsgIn.center.axis;
  
  pMapElement->length = this->mapElementMsgIn.length;
  pMapElement->width = this->mapElementMsgIn.width;
  pMapElement->orientation = this->mapElementMsgIn.orientation;
  pMapElement->lengthVar = this->mapElementMsgIn.lengthVar;
  pMapElement->widthVar = this->mapElementMsgIn.widthVar;
  pMapElement->orientationVar = this->mapElementMsgIn.orientationVar;


  pMapElement->height = this->mapElementMsgIn.height;
  pMapElement->heightVar = this->mapElementMsgIn.heightVar;
  pMapElement->elevation = this->mapElementMsgIn.elevation;
  pMapElement->elevationVar = this->mapElementMsgIn.elevationVar;


  pMapElement->velocity.x = this->mapElementMsgIn.velocity.x;
  pMapElement->velocity.y = this->mapElementMsgIn.velocity.y;
  pMapElement->velocity.max_var = this->mapElementMsgIn.velocity.max_var;
  pMapElement->velocity.min_var = this->mapElementMsgIn.velocity.min_var;
  pMapElement->velocity.axis = this->mapElementMsgIn.velocity.axis;

  pMapElement->timeStopped = this->mapElementMsgIn.timeStopped;
  pMapElement->peakSpeed = this->mapElementMsgIn.peakSpeed;
pMapElement->peakSpeedVar = this->mapElementMsgIn.peakSpeedVar;
  pMapElement->peakAccel = this->mapElementMsgIn.peakAccel;
pMapElement->peakAccelVar = this->mapElementMsgIn.peakAccelVar;

     


  pMapElement->frameType = (MapElementFrameType)this->mapElementMsgIn.frameType;
  pMapElement->timestamp = this->mapElementMsgIn.timestamp;
  pMapElement->state = this->mapElementMsgIn.state;

  pMapElement->plotColor = (MapElementColorType)this->mapElementMsgIn.plotColor;
  pMapElement->plotValue = this->mapElementMsgIn.plotValue;

  pMapElement->label.clear();
  ///cout << "label recv " << mapElementMsgIn.num_lines <<endl;
  //  string tmprecv ;
  for (int i = 0; i<mapElementMsgIn.numLines;++i){
    ///cout << " in line " << i << " = " << this->mapElementMsgIn.label[i] << endl;
    //tmprecv = this->mapElementMsgIn.label[i];
    //pMapElement->label.push_back(tmprecv);
    pMapElement->label.push_back(this->mapElementMsgIn.label[i]);
  }

 pMapElement->geometryMin.x = this->mapElementMsgIn.geometryMin.x;
  pMapElement->geometryMin.y = this->mapElementMsgIn.geometryMin.y;
  pMapElement->geometryMin.max_var = this->mapElementMsgIn.geometryMin.max_var;
  pMapElement->geometryMin.min_var = this->mapElementMsgIn.geometryMin.min_var;
  pMapElement->geometryMin.axis = this->mapElementMsgIn.geometryMin.axis;
     
 pMapElement->geometryMax.x = this->mapElementMsgIn.geometryMax.x;
  pMapElement->geometryMax.y = this->mapElementMsgIn.geometryMax.y;
  pMapElement->geometryMax.max_var = this->mapElementMsgIn.geometryMax.max_var;
  pMapElement->geometryMax.min_var = this->mapElementMsgIn.geometryMax.min_var;
  pMapElement->geometryMax.axis = this->mapElementMsgIn.geometryMax.axis;
     

  pMapElement->geometryType = (MapElementGeometryType)this->mapElementMsgIn.geometryType;
  pMapElement->geometry.clear();
  for (int i = 0; i<mapElementMsgIn.numPts;++i){
    ptmsg = this->mapElementMsgIn.geometry[i];
    pt.x = ptmsg.x;
    pt.y = ptmsg.y;
    pt.z = ptmsg.z;
    pt.max_var = ptmsg.max_var;
    pt.min_var = ptmsg.min_var;
    pt.axis = ptmsg.axis;

    pMapElement->geometry.push_back(pt);
  }


  return 0;
}
