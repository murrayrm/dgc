/**********************************************************
 **
 **  TESTMAPELEMENTLISTENER.CC
 **
 **    Time-stamp: <2007-04-27 19:27:49 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "interfaces/sn_types.h"

using namespace std;


class CTestRecvMapElement : public CMapElementTalker{

public:
  /*! Constructor */
  CTestRecvMapElement() {}
      
  /*! Standard destructor */
  ~CTestRecvMapElement() {}

};

int main(int argc, char **argv)
{

  int skynetKey = 0;
  int subgroup = 0;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  if(argc >1)
    subgroup = atoi(argv[1]);
  if(argc >2)
    skynetKey = atoi(argv[2]);

  int bytesRecv = 0;

  cout << "Skynet Key = " << skynetKey << 
    "  Subgroup = "  << subgroup << endl;
  

  CTestRecvMapElement testtalker;
  testtalker.initRecvMapElement(skynetKey,subgroup);
  MapElement el;
  
  cout << "Testing blocking read " << endl;


  bytesRecv = testtalker.recvMapElementBlock(&el,subgroup);
  cout << "bytes received = " << bytesRecv << endl;
  el.print(); 
 


  cout << "Testing non-blocking read" << endl;
  while(1){
  
    bytesRecv = testtalker.recvMapElementNoBlock(&el,subgroup);
    if (bytesRecv>0){
      cout << "bytes received = " << bytesRecv << endl;
      el.print();
    }else{
      usleep(100000);      
    }
  }
  return(0);
}

