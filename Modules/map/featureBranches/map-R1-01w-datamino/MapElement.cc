/**********************************************************
 **
 **  MAPELEMENT.CC
 **
 **    Time-stamp: <2007-04-28 10:33:44 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Mar  9 10:57:28 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "MapElement.hh"

using namespace std;

MapElement::MapElement()
{
  // Note: do we need this??
  clear();
}


void  MapElement::clear(){
  subgroup = 0;
  id.clear();
  conf = 0;
  type = ELEMENT_UNDEF;
  type_conf = 0;
  center_type = CENTER_UNDEF;
  center.clear();
  length = -1;
  width = -1;
  orientation =0;
  length_var = 0;
  width_var = 0;
  orientation_var = 0;
  geometry_type = GEOMETRY_UNDEF;
  geometry.clear();
  height =  -1;
  height_var = 0;
  velocity.clear();
  frame_type = FRAME_UNDEF;
  memset(&state, 0, sizeof(state));
  label.clear();
  plot_color = MAP_COLOR_GREY;
  plot_value = 100;
}

void MapElement::set_geometry(point2arr_uncertain &ptarr)
{
  geometry = ptarr;
  set_center_from_geometry();
}
 
void MapElement::set_geometry(point2arr &ptarr)
{
  geometry = ptarr;
  set_center_from_geometry();
}

void MapElement::set_geometry(point2 &pt)
{
  geometry.clear();
  geometry.push_back(pt);
  set_center_from_geometry();

}

void MapElement::set_geometry(point2 &pt, double radius)
{
  geometry.clear();
  geometry.push_back(pt);
  set_center_from_geometry();
  width = radius;
  length = radius;

}

void MapElement::set_geometry(vector<point2> &ptarr){
  point2arr tmparr;
  tmparr = ptarr;
  geometry = tmparr;
  set_center_from_geometry();
}
void MapElement::set_geometry(vector<point2_uncertain> &ptarr)
{
  geometry = ptarr;
  set_center_from_geometry();
}

void MapElement::set_geometry(vector<double> &xarr, vector<double> &yarr)
{
  // NOT IMPLEMENTED!!
}

void  MapElement::set_clear(vector<int>& ident)
{
  clear();
  id = ident;
  type = ELEMENT_CLEAR;
  
}
void  MapElement::set_alice(VehicleState &statein)
{
  clear();
  id = 0;
  type = ELEMENT_ALICE;
  state = statein;
}
void  MapElement::set_circle_obs(vector<int>& ident, point2 cpt, double radius){
  id = ident;
  center = cpt;
  length = radius;
  width = radius;
}
void  MapElement::set_circle_obs()
{
  height = 10;
  type = ELEMENT_OBSTACLE;
  center_type = CENTER_BOUND_RADIUS;
}

void  MapElement::set_poly_obs(vector<int>& ident, vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 10;
  type = ELEMENT_OBSTACLE;
  //center_type = CENTER_BOUND_BOX;
  geometry_type = GEOMETRY_POLY;
  set_center_from_geometry();
}
void  MapElement::set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid ){
  id = ident;
  center.x = cpt.x;
  center.y = cpt.y;
  orientation = ang;
  length = len;
  width = wid;
  height = 10;
  type = ELEMENT_OBSTACLE;
  center_type = CENTER_BOUND_BOX;
  geometry_type = GEOMETRY_UNDEF;
}
void  MapElement::set_line(vector<int>& ident,  vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 0;
  set_line();
  set_center_from_geometry();
}
void  MapElement::set_line(){
  type = ELEMENT_LINE;
  geometry_type = GEOMETRY_LINE;

}

void  MapElement::set_stopline(vector<int>& ident, vector<point2>& ptarr){
  clear();
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }  
  height = 0;
  type = ELEMENT_STOPLINE;
  geometry_type = GEOMETRY_LINE;
  set_center_from_geometry();
}



void  MapElement::set_laneline(vector<int>& ident, vector<point2>& ptarr){
  
  set_id(ident);
  set_geometry(ptarr);
  set_laneline();

  plot_color = MAP_COLOR_BLUE;

  
}

void  MapElement::set_laneline(){
 
  height = 0;
  type = ELEMENT_LANELINE;
  geometry_type = GEOMETRY_LINE;
  //  plot_color = MAP_COLOR_BLUE;
  //set_center_from_geometry();
  
}

void  MapElement::set_points(vector<int>& ident, vector<point2>& ptarr){
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }  
  type = ELEMENT_POINTS;
  geometry_type = GEOMETRY_POINTS;
  set_center_from_geometry();

}
void  MapElement::set_points(){
  height = 0;
  type = ELEMENT_POINTS;
  geometry_type = GEOMETRY_POINTS;
}

void  MapElement::set_trav_path(vector<int>& ident,  vector<point2>& ptarr){

  type = ELEMENT_TRAV_PATH;
  geometry_type = GEOMETRY_LINE;
  plot_color = MAP_COLOR_RED;
  id = ident;
  geometry.clear();
  for (int i = 0; i < (int)ptarr.size(); ++i){
    geometry.push_back(ptarr[i]);
  }
  height = 0;
  set_line();
  set_center_from_geometry();
}

void  MapElement::print() const
{
  unsigned int i;
  cout << "------Start Printing MapElement------" << endl;
  cout << "subgroup= " << subgroup << endl;
  cout << "id= " ;
  for (i = 0; i < id.size(); ++i){
    cout << id[i] << "  ";
  }
  cout << " conv= " << conf 
       << " type= " << type
       << " type_conf= " << type_conf << endl;
  cout << "center:  " ;
  cout << "center_type= " << center_type <<endl;
  center.print();
  cout << "length= " << length
       << " width= " << width
       << " orientation= " << orientation << endl;
  cout << "length_var= " << length_var
       << " width_var= " << width_var
       << " orientation_var= " << orientation_var << endl;
  cout << "geometry_type= " << geometry_type <<endl;
  for (i = 0; i < geometry.size(); ++i){
    cout << "geometry point " << i << ":  ";
    geometry[i].print();
  }
  cout <<"height= " << height 
       << " height_var= " << height_var << endl;
  cout << "velocity:  " ;
  velocity.print();
  cout <<"frame= " << frame_type << endl;
  cout <<"plot_color= " << plot_color << endl;
  cout <<"plot_value= " << plot_value << endl;
  
  
  for (i = 0; i < label.size(); ++i){
    cout << "label line " << i << ":  " << label[i]<< endl;
  }
  cout << "------End Printing MapElement--------" << endl;
}
void MapElement::print_state() const {
  
  cout << "------Start Printing VehicleState------" << endl;
  cout << "timestamp= " << state.timestamp << endl;
  cout << "utmNorthing= " << state.utmNorthing 
       << " utmEasting= " << state.utmEasting 
       << " utmAltitude= " << state.utmAltitude 
       << endl;

  cout << "GPS_Northing_deprecated= " << state.GPS_Northing_deprecated
       << " GPS_Easting_deprecated= " << state.GPS_Easting_deprecated 
       << endl;

  cout << "utmNorthVel= " << state.utmNorthVel 
       << " utmEastVel= " << state.utmEastVel 
       << " utmAltitudeVel= " << state.utmAltitudeVel 
       << endl;

  cout << "Acc_N_deprecated= " << state.Acc_N_deprecated
       << "Acc_E_deprecated= " << state.Acc_E_deprecated
       << "Acc_D_deprecated= " << state.Acc_D_deprecated
       << endl;

  cout << "utmRoll= " << state.utmRoll 
       << " utmPitch= " << state.utmPitch 
       << " utmYaw= " << state.utmYaw 
       << endl;

  cout << "utmRollRate= " << state.utmRollRate 
       << " utmPitchRate= " << state.utmPitchRate 
       << " utmYawRate= " << state.utmYawRate 
       << endl;

  cout << "raw_YawRate_deprecated= " << state.raw_YawRate_deprecated 
       << " RollAcc_deprecated= " << state.RollAcc_deprecated 
       << endl;
 
  
  cout << "PitchAcc_deprecated= " << state.PitchAcc_deprecated
       << " YawAcc_deprecated= " << state.YawAcc_deprecated 
       << endl;

  cout << "utmNorthConfidence= " << state.utmNorthConfidence  
       << " utmEastConfidence= " << state.utmEastConfidence 
       << " utmAltitudeConfidence= " <<  state.utmAltitudeConfidence  
       << endl;

  cout << "rollConfidence= " << state.rollConfidence 
       << " pitchConfidence= " << state.pitchConfidence
       << " yawConfidence= " << state.yawConfidence
       << endl;

  cout << "gpsGamma= " << state.vehSpeed
       << " utmZone= " << state.utmZone 
       << " utmLetter= " << state.utmLetter
       << endl;

  cout << "vehXVel= " << state.vehXVel 
       << " vehYVel= " << state.vehYVel 
       << " vehZVel= " << state.vehZVel
       << endl;
  
  cout << "vehRollRate= " << state.vehRollRate 
       << " vehPitchRate= " << state.vehPitchRate 
       << " vehYawRate= " << state.vehYawRate
       << endl;


  cout << "localX= " << state.localX 
       << " localY= " << state.localY 
       << " localZ= " << state.localZ
       << endl;

  cout << "localRoll= " << state.localRoll 
       << " localPitch= " << state.localPitch 
       << " localYaw= " << state.localYaw
       << endl;

  cout << "localXVel= " << state.localXVel 
       << " localYVel= " << state.localYVel 
       << " localZVel= " << state.localZVel
       << endl;

  cout << "localRollRate= " << state.localRollRate 
       << " localPitchRate= " << state.localPitchRate 
       << " localYawRate= " << state.localYawRate
       << endl; 

  cout << "------End Printing VehicleState--------" << endl;
}

void MapElement::set_center(point2 cpt)
{
  center.clear();
  center = cpt;
}

void MapElement::set_center(point2_uncertain cpt)
{
  center.clear();
  center = cpt;
}
 
void MapElement::set_center(double x, double y)
{
  center.clear();
  center.x = x;
  center.y = y;
}

void MapElement::set_center_from_geometry(){
  int i;
  int size = (int)geometry.size();
  if (size==0)
    return;
  
  double fulldist = 0;
  double dist = 0;
  point2 dpt;
  double ratio = 1;
  point2 sum(0,0);
  int lowindex =0,highindex=0;

  
  switch (geometry_type) {
  case GEOMETRY_UNDEF:
  case GEOMETRY_POINTS:
    center = geometry[0];
    return;
  case GEOMETRY_ORDERED_POINTS:
  case GEOMETRY_LINE:
  case GEOMETRY_EDGE:
    if (size==1){
      center = geometry[0];
      return;
    }
    for (i = 0; i < size-1; ++i){
      dpt = geometry[i+1]-geometry[i];
      fulldist = fulldist+dpt.norm();
    }
      
    for (i = 0; i < size-1; ++i){
      dpt = geometry[i+1]-geometry[i];
        
      if (dist < fulldist/2 &&
          dist+dpt.norm() >= fulldist/2){
        lowindex = i;
        highindex = i+1;
        
        break;
      }
      dist = dist+dpt.norm();
    }
    ratio = (fulldist/2-dist)/dpt.norm();
    center = geometry[lowindex]+dpt*ratio;
    return;
  case GEOMETRY_POLY:
    for (i = 0; i < size; ++i){
      sum = sum+geometry[i];
    }
    center = sum/size;
    return;
    
  }
}
