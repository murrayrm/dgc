/**********************************************************
 **
 **  MAPELEMENT.HH
 **
 **    Time-stamp: <2007-03-17 11:12:29 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:44:48 2007
 **
 **
 **********************************************************
 **
 **  A class to describe a geometric map element
 **
 **********************************************************/


#ifndef MAPELEMENT_HH
#define MAPELEMENT_HH

#include <vector>
#include <string>
#include <iostream>
#include "interfaces/VehicleState.h"
#include "frames/point2_uncertain.hh"
#include "MapId.hh"

using namespace std;

/** The object's geometry */
enum MapElementGeometryType {
  GEOMETRY_UNDEF,  
  GEOMETRY_POINTS,
  GEOMETRY_ORDERED_POINTS,
  GEOMETRY_LINE,
  GEOMETRY_EDGE,
  GEOMETRY_POLY
};

/** How the object's center is defined */
enum MapElementCenterType {
  CENTER_UNDEF,  
  CENTER_POINT,
  CENTER_POSE,
  CENTER_BOUND_RADIUS,
  CENTER_BOUND_BOX,
  CENTER_BOUND_LINES
};
    
/** The frame of the object (global, local, etc */
enum MapElementFrameType {
  FRAME_UNDEF,
  FRAME_LOCAL, /*!< Alice's local frame */
  FRAME_GLOBAL, /*!< Global (UTM coords) */
  FRAME_VEHICLE /*!< Not sure what this is... ask Sam */
};

/** What color to draw the object */
enum MapElementColorType {
  MAP_COLOR_GREY,
  MAP_COLOR_RED,
  MAP_COLOR_BLUE,
  MAP_COLOR_LIGHT_BLUE,
  MAP_COLOR_GREEN,
  MAP_COLOR_CYAN,
  MAP_COLOR_YELLOW,
  MAP_COLOR_MAGENTA
};

/* What kind of object we're representing */
enum MapElementType {
  ELEMENT_UNDEF, /*!< Undefined object */
  ELEMENT_CLEAR, /*!< ? */
  ELEMENT_ALICE, /*!< Alice */

  ELEMENT_POINTS, /*!< A set of points */
  ELEMENT_WAYPOINT, /*!< A waypoint */
  ELEMENT_CHECKPOINT, /*!< A checkpoint */

  ELEMENT_LINE, /*!< A generic road line */
  ELEMENT_STOPLINE, /*!< A stop line */
  ELEMENT_LANELINE, /*!< A lane line */

  ELEMENT_PARKING_SPOT, /*!< A parking spot */
  ELEMENT_PERIMETER, /*!< The perimeter of a zone, such as a parking lot */

  ELEMENT_OBSTACLE,  /*!< An obstacle */
  ELEMENT_OBSTACLE_EDGE, /*!< The edge of an obstacle */
  //  ELEMENT_VEHICLE /*!< A vehicle */

  //  ELEMENT_TPLAN_PATH /*!< A spline describing Alice's planned path */
  //  ELEMENT_RDDF /*!< An RDDF */
  //  ELEMENT_POLYTOPE /*!< A polytope, for example like what is sent to dplanner */
  ELEMENT_TRAV_PATH /*!< Alice's traversed path */
};



/** \brief Describes a geometric map element
 *
 * A MapElement represents information, such as vehicles, road lines,
 * trajectory splines, etc that we wanted to represent in the map.
 *
 * To send a MapElement, use MapElementTalker
 */
class MapElement
{
  
public:
  
  /** Default Constructor. Clears whatever is in the mapelement */
  MapElement();
  
  /** Destructor. Does nothing */
  ~MapElement() {}

  /// Object id
  MapId id;
  
  /** Subgroup id. only changed and used by talker */
  int subgroup;

  /// Confidence that the object exists 
  double conf;

  /// Object classification
  MapElementType type;
  // TODO: Need type list and enumeration
  
  /// Type classification confidence bounds
  double type_conf;
  
  /// The object's center type
  MapElementCenterType center_type;
  /// \brief Estimated object center position and covariance
  ///
  /// point2_uncertain struct members (defined in frames/point2_uncertain.hh)
  ///
  /// center.x                x position
  /// center.y                y position
  /// center.max_var        max variance
  /// center.min_var        min variance
  /// center.axis             angle of max variance
  point2_uncertain center;
  
  /** Estimated bounding box parameter
   *
   *  Not meaningful for all objects, but can be useful */
  double length;  
  /** Estimated bounding box parameter
   *
   *  Not meaningful for all objects, but can be useful */
  double width;
  /** Estimated bounding box parameter
   *
   *  Not meaningful for all objects, but can be useful 
   * Ranges from -pi to pi */
  double orientation;
  

  /** bounding box parameter variance
   *
   *  Not meaningful for all objects, but can be useful */
  double length_var;
  /** bounding box parameter variance
   *
   *  Not meaningful for all objects, but can be useful */
  double width_var;
  /** bounding box parameter variance
   *
   *  Not meaningful for all objects, but can be useful */
  double orientation_var;

  /// Object geometry type
  MapElementGeometryType geometry_type;
  /** Estimated object geometry 
   *
   * geometry represents object contour
   * -list of ordered connected points for a line 
   * -list of ordered vertices for an obstacle
   * If the geometry is not specified, bounding box info only is used
   */
  point2arr_uncertain geometry;
  //vector<point2_uncertain> geometry;

  /// Estimated object height. 
  /// height for road markings should be zero
  double height;

  /// Object height variance
  double height_var;
  

  /// Object velocity
  point2_uncertain velocity;
  // The velocity x,y coordinates define a velocity vector
  // for static map elements x,y should be 0 

    
  /// Frame of reference
  /// All map element coordinates should be given in local frame, 
  /// not GPS global or vehicle frame.
  /// Shouldn't need this eventually.
  /// TODO: define frame enumeration
  MapElementFrameType frame_type;

  /// Vehicle state data when element was detected
  VehicleState state;

  /// The color of the object
  /// Note: the colors are declared in MapElement.hh and 
  /// interpreted in MapWindow.cc
  MapElementColorType plot_color;
  int plot_value;

  /// Text label
  /// Not critical but could be useful initially for debugging
  vector<string> label;

  /// Resets everything to zero, undefined, etc
  void clear();

  /// Set the object ID using a MapID
  void set_id(MapId& id_in) {id = id_in;}
  /// Set the object id using an array of ints
  void set_id(vector<int>& id_in) {id = id_in;}
  /// set the object ID using an integer
  void set_id(int id_in) {id = id_in;}
  /// set the object ID using integers
  void set_id(int id1_in,int id2_in) {id.set(id1_in,id2_in);}
  /// set the object ID using integers
  void set_id(int id1_in,int id2_in,int id3_in) {id.set(id1_in,id2_in,id3_in);}
  /// set the object ID using integers
  void set_id(int id1_in,int id2_in,int id3_in, int id4_in) {id.set( id1_in,id2_in,id3_in,id4_in);}

  /// Set the boundary of an object
  void set_geometry(point2arr &ptarr);
  /// Set the boundary of an object (single point) 
  void set_geometry(point2 &pt);
  /// Set the boundary of an object
  void set_geometry(point2arr_uncertain &ptarr);
  /// Set the boundary of an object
  void set_geometry(vector<point2> &ptarr);
  /// Set the boundary of an object
  void set_geometry(vector<point2_uncertain> &ptarr);
  /// Set the boundary of an object - NOT IMPLEMENTED!!!
  void set_geometry(vector<double> &xarr, vector<double> &yarr);
  /// Set the boundary of a circular object by giving its center
  /// and radius
  void set_geometry(point2 &pt, double radius);

  /// Set the center of the object
  void set_center(point2 cpt);
  /// Set the center of the object
  void set_center(point2_uncertain cpt);
  /// Set the center of the object
  void set_center(double x, double y);
  /// Uses the object's geometry to find its center
  void set_center_from_geometry();

  //  void set_frame_local();
  //  void set_frame_global();
  
  void set_clear(vector<int>& ident);
  /// Sets an Alice object with a given state
  void set_alice(VehicleState &statein);
  /// Sets a circle obstacle with a given center and radius
  void set_circle_obs(vector<int>& ident, point2 cpt, double radius);
  /// Sets the given obstacle as a circle (default height: 10 meters)
  void set_circle_obs();
  /// Set a polygonal obstacle with the given boundary
  void set_poly_obs(vector<int>& ident, vector<point2>& ptarr);
  /// Set a block obstacle 
  /// \param cpt the center point
  /// \param ang the angle (-pi to pi)
  /// \param len the length
  /// \param wid the width
  void set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid );
  /// Set a line with the given points
  void set_line(vector<int>& ident,  vector<point2>& ptarr);
  /// Set the current obstacle as a line (does not modify points)
  void set_line();
  /// Set the obstacle as a stopline with the given poitns
  void set_stopline(vector<int>& ident, vector<point2>& ptarr);
  /// Set the obstacle as a lane line with the given points
  void set_laneline(vector<int>& ident, vector<point2>& ptarr);
  /// Set the current obstacle as a lane line
  void set_laneline();
  /// Set the obstacle as a series of points
  void set_points(vector<int>& ident, vector<point2>& ptarr);
  /// Set the obstacle as a series of points
  void set_points();
  /// Set's alice's traversed path
  void set_trav_path(vector<int>& ident, vector<point2>& ptarr);

  /// Print the element information
  void print() const ;
  /// Print alice's state info
  void print_state() const ;

};
#endif
