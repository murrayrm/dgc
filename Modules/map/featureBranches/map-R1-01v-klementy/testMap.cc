/**********************************************************
 **
 **  TESTMAP.CC
 **
 **    Time-stamp: <2007-04-30 11:46:19 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Mar  1 12:53:36 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/
                     
#include "Map.hh"
#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"

using namespace std;
 
 
int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testMap [rndf filename] [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  if(argc >1)
    fname = argv[1];
  if(argc >2)
    testnum = atoi(argv[2]);
  if(argc >3)
    sendSubGroup = atoi(argv[3]);
  if(argc >4)
    recvSubGroup = atoi(argv[4]);
  if(argc >5)
    skynetKey = atoi(argv[5]);
  
  cout << "RNDF filename = " << fname 
       << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey,recvSubGroup);

  Map mapdata;
  //  mapdata.loadRNDF("rn.txt");

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };
 
  MapElement el;
	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;  
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getEl(i,el);
    //TEMP 
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,sendSubGroup);
	}
  cout <<"========================================" << endl;
  testname = "Testing getClosestWaypoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl;
    
    sendEl.set_id(-1);
    sendEl.set_points();
    sendEl.plot_color = MAP_COLOR_MAGENTA;

    int retval;
  
    PointLabel label;
    point2 statept;
    point2 pt;
    point2arr ptarr;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        cout << "ptlabel = " << label << endl;   
        retval = mapdata.getWaypoint(pt,label);
        cout << "getWaypoint retval = " << retval << " pt = " << pt << "  label " << label << endl;
        ptarr.clear();
        ptarr.push_back(pt);
        sendEl.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  } 


 cout <<"========================================" << endl;
  testname = "Testing getHeading()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl;
    
    sendEl.set_id(-1);
    sendEl.set_line();
    sendEl.plot_color = MAP_COLOR_MAGENTA;

    int retval; 
  
    PointLabel label;
    point2 statept;
    point2 pt,angpt,dpt;
    point2arr ptarr;
    double ang;
    bool headingpt = true;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        cout << "ptlabel = " << label << endl;   
        retval = mapdata.getWaypoint(pt,label);
        cout << "getWaypoint retval = " << retval << " pt = " << pt << "  label " << label << endl;
        
        if (headingpt){
        retval = mapdata.getHeading(ang,statept);
        cout << "getHeading POINT retval = " << retval << " ang = " << ang << endl;
        dpt = statept;
        angpt = statept;
        headingpt = false;
        }else{
 
        retval = mapdata.getHeading(ang,label);
        cout << "getHeading LABEL retval = " << retval << " ang = " << ang << endl;
        dpt = pt;
        angpt = pt;
        headingpt = true;
        }


        dpt.x = angpt.x+10*cos(ang);
        dpt.y = angpt.y+10*sin(ang);

        ptarr.clear();
        ptarr.push_back(angpt);
        ptarr.push_back(dpt);
        sendEl.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }

 cout <<"========================================" << endl;
  testname = "Testing getDistToWaypoint() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;  
    int retval;
    PointLabel label;
    PointLabel statelabel;
    point2 statept;
    point2 stoppt;
    bool initflag = true;
    cout << "Select the waypoint" << endl;
    double dist = 0;
    while (true){

     
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        
        if (initflag){
          cout <<"For point " << statept << endl;
          retval =mapdata.getClosestPointID(label,statept);
          cout << "ptlabel = " << label << endl;  
          initflag = false;
        }else{
          retval =mapdata.getLaneDistToWaypoint(dist,statept, label);
          cout << "getDistToWaypoint retval = " << retval << " dist = " << dist << endl;
          retval =mapdata.getClosestPointID(statelabel,statept);          retval =mapdata.getLaneDistToWaypoint(dist,statelabel, label);
          cout <<"getDistToWaypt retval = " << retval << " dist = " << dist << endl;
          cout <<"From label "<<label << " to label " << statelabel << endl;
          

        }
        cout <<endl<< "Now select the state point" << endl;
      }
      else{ 
        
        usleep (100000);

      }  
    } 
  } 

  cout <<"========================================" << endl;
  testname = "Testing getStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;  
    int retval;
    PointLabel label;
    point2 statept;
    point2 stoppt;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        cout << "ptlabel = " << label << endl;  
        retval =mapdata.getStopline(stoppt,statept);
        cout << "get stopline retval = " << retval << " point = " << stoppt << endl;
    
      }
      else{ 
        
        usleep (100000);

      }
    }
  }
 cout <<"========================================" << endl;
  testname = "Testing getLaneID getSegmentID checkLaneID() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;

    int retval;
  
    PointLabel label;
    point2 statept;
    point2 stoppt;
    bool inflag = false;
    bool switchflag = true;
    int laneid = 0;
    int segid = 0;
  cout << "Enter a point" << endl;    
while (true){

    
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        // recvEl.print();
        cout << endl <<"For point " << statept << endl;
        if (switchflag){

        laneid=  mapdata.getLaneID(statept);
        cout << "lane id = " << laneid << endl;  

        segid=  mapdata.getSegmentID(statept);
        cout << "segment id = " << segid << endl;  
        switchflag = !switchflag;
              cout << "Enter a point to test checkLaneID for this lane and segment id" << endl;
        }else{
          inflag=  mapdata.checkLaneID(statept, segid, laneid);
          if (inflag)
            cout << "IDs MATCHED - segment = " << segid << " lane = " << laneid << endl;
          else
            cout << "IDs no match - segment = " << segid << " lane = " << laneid << endl;

          switchflag = !switchflag;
          cout << endl<< "Enter a point" << endl;
        }    


      } 
      else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing getNextPointID getNextStopline() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;

    int retval;
  
    PointLabel label;
    point2 statept;
    point2 stoppt;
    int laneid = 0;
    int segid = 0;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getNextPointID(statept);
        cout << "ptlabel = " << label << endl;  
 
        retval =mapdata.getNextStopline(stoppt,label);
        cout << "get stopline retval = " << retval << " point = " << stoppt << "  stop label " << label << endl;
    
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
  cout <<"========================================" << endl;
  testname = "Testing getLeftBound(), getRightBound() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl2.set_id(-2);
    sendEl1.set_laneline();
    sendEl2.set_laneline();
    sendEl1.plot_color = MAP_COLOR_GREEN;
    sendEl2.plot_color = MAP_COLOR_RED;

     

    int retval;
  
    PointLabel label;
    LaneLabel lanelabel;
    point2 statept;
    point2 stoppt;
    point2arr ptarr;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        lanelabel.segment = label.segment;
        lanelabel.lane = label.lane;
        cout << "ptlabel = " << label << endl;  
        cout << "lanelabel = " << lanelabel << endl;  
        retval =mapdata.getLeftBound(ptarr,lanelabel);
        cout << "getLeftBound retval = " << retval << " ptarr = " << ptarr << "  label " << label << endl;
        sendEl1.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl1,sendSubGroup);

        retval =mapdata.getRightBound(ptarr,lanelabel);
        cout << "getRightBound retval = " << retval << " ptarr = " << ptarr << "  label " << label << endl;
        sendEl2.set_geometry(ptarr);
        maptalker.sendMapElement(&sendEl2,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }

  
  cout <<"========================================" << endl;
  testname = "Testing getObstacleDist(), getObstaclePoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1, sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_circle_obs();
    sendEl1.plot_color = MAP_COLOR_MAGENTA;


    sendEl2.set_id(-2);
    sendEl2.set_points();
    sendEl2.plot_color = MAP_COLOR_RED;

  
    PointLabel label;
    point2 statept;
    point2 pt;
    point2arr ptarr;
    
    bool initflag = true;
    double dist;
    while (true){

       
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (initflag){
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          sendEl1.set_geometry(statept,2);
          cout << "Setting obstacle = " << endl;   
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          mapdata.data.push_back(sendEl1); 
          initflag = false;
        }
        else {  
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          dist=  mapdata.getObstacleDist(statept);
          cout << "DIST = " << dist << endl;
          pt = mapdata.getObstaclePoint(statept,10);
          sendEl2.set_geometry(pt);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);

        }
      }     
      else{  
        usleep (100000);   
      }   
    }  
  }
  

 cout <<"========================================" << endl;
  testname = "Testing getBounds() ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl2.set_id(-2);
    sendEl1.set_laneline();
    sendEl2.set_laneline();
    sendEl1.plot_color = MAP_COLOR_GREEN;
    sendEl2.plot_color = MAP_COLOR_RED;

     

    PointLabel label;
    LaneLabel lanelabel;
    point2 statept;
    point2 stoppt;
    point2arr ptarr1,ptarr2;

    bool forward = true;

    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        label=  mapdata.getClosestPointID(statept);
        lanelabel.segment = label.segment;
        lanelabel.lane = label.lane;
        cout << "ptlabel = " << label << endl;  
        cout << "lanelabel = " << lanelabel << endl;
          
        int retval=0;
        if (forward){
        retval =mapdata.getBounds(ptarr1,ptarr2,lanelabel,statept,20,0);
cout << "getBounds retval = " << retval << " ptarr1 = " << ptarr1 << "  ptarr2 = " << ptarr2 << endl;
 forward = false; 
        }else{
          retval =mapdata.getBoundsReverse(ptarr1,ptarr2,lanelabel,statept,20,0);
          cout << "getBoundsReverse retval = " << retval << " ptarr1 = " << ptarr1 << "  ptarr2 = " << ptarr2 << endl;
          forward = true;
        }

        sendEl1.set_geometry(ptarr1);
        maptalker.sendMapElement(&sendEl1,sendSubGroup);
        sendEl2.set_geometry(ptarr2);
        maptalker.sendMapElement(&sendEl2,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      } 
    }   
  }
 



  cout <<"========================================" << endl;
  testname = "Testing getTransitionBounds( NEW ) ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl2.set_id(-2);
    sendEl1.set_laneline();
    sendEl2.set_laneline();
    sendEl1.plot_color = MAP_COLOR_GREEN;
    sendEl2.plot_color = MAP_COLOR_RED;

     

    int retval;
  
    PointLabel label1;
    PointLabel label2;
    LaneLabel lanelabel;
    point2 statept;
    point2 stoppt;
    point2arr ptarr1;
    point2arr ptarr2;
    bool exitflag = true;
    bool getptflag = true;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (getptflag){
        if (exitflag){
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          label1=  mapdata.getClosestPointID(statept);
          cout << "label exit segment = " << label1 << endl;
          exitflag = false;
        }else{
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
          label2=  mapdata.getClosestPointID(statept);
          cout << "label enter segment = " << label2 << endl;  
          getptflag = false;
        }
        }else{
          statept = recvEl.center;
          recvEl.print(); 
          cout << endl <<"For point " << statept << endl;         
              
          retval =mapdata.getTransitionBounds(ptarr1,ptarr2,label1,label2,statept,50,20);
          cout << "getTransitionBounds retval = " << retval << endl              << " ptarr1 = " << ptarr1 
               << " ptarr2 = " << ptarr2 << endl
               << " exit label " << label1 
               << " enter label " << label2 << endl;
          sendEl1.set_geometry(ptarr1);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);

          sendEl2.set_geometry(ptarr2); 
          maptalker.sendMapElement(&sendEl2,sendSubGroup); 
          exitflag = true;
        }         
               
      }      
      else{    
        usleep (100000);
      }
    }
  }
  
   
    

 cout <<"========================================" << endl;
  testname = "Testing getLaneCenterPoint()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1, sendEl2, sendEl3;
    
    sendEl1.set_id(-1);
    sendEl1.set_points();
    sendEl1.plot_color = MAP_COLOR_BLUE;


    sendEl2.set_id(-2);
    sendEl2.set_points();
    sendEl2.plot_color = MAP_COLOR_RED;

    sendEl3.set_id(-3);
    sendEl3.set_points();
    sendEl3.plot_color = MAP_COLOR_GREEN;

  
    point2 statept;
    point2 pt;
    point2arr ptarr;
    
    bool initflag = true;
    double val;
    LaneLabel lanelabel;
    PointLabel label;
    while (true){

       
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (initflag){
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;
  label=  mapdata.getClosestPointID(statept);
        lanelabel.segment = label.segment;
        lanelabel.lane = label.lane;
        cout << "lanelabel = " << lanelabel << endl;
       
          initflag = false;
        }
        else {
          statept = recvEl.center;
          recvEl.print();
          cout << endl <<"For point " << statept << endl;

          cout << "Getting Center " << endl;
     mapdata.getLaneCenterPoint(pt,lanelabel,statept);
     mapdata.getHeading(val,pt);
     cout << "Center ang = " << val << endl;
     
     sendEl1.set_geometry(pt);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
     
          cout << "Getting Right " << endl;
mapdata.getLaneRightPoint(pt,lanelabel,statept,-10);
     mapdata.getHeading(val,pt);
     cout << "Right ang = " << val << endl;

     sendEl2.set_geometry(pt);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);

          cout << "Getting Left " << endl;
      mapdata.getLaneLeftPoint(pt,lanelabel,statept,15);
     mapdata.getHeading(val,pt);
     cout << "Left ang = " << val << endl;

     sendEl3.set_geometry(pt);
          maptalker.sendMapElement(&sendEl3,sendSubGroup);

        }
      }     
      else{  
        usleep (100000);   
      }   
    }  
  } 
      
  
  //   bool bretval = mapdata.isStop(stoplabel);
  //   cout << "isStop for " << stoplabel << " = " << bretval << endl;

  //   bretval = mapdata.isStop(enterlabel);
  //   cout << "isStop for " << enterlabel << " = " << bretval << endl;

  //   bretval = mapdata.isExit(enterlabel);
  //   cout << "isExit for " << enterlabel << " = " << bretval << endl;
  //   bretval = mapdata.isExit(exitlabel);
  // cout << "isExit for " << exitlabel << " = " << bretval << endl;

  //  bretval = mapdata.isExit(stoplabel);
  // cout << "isExit for " << stoplabel << " = " << bretval << endl;
 
  //   val = mapdata.getStopline(pt,stoplabel);
  //   cout << "Stopline at " << stoplabel << " = " << pt << endl;
  //   el.clear();
  //   id.clear();
  //   id.push_back(stoplabel.segment);
  //   id.push_back(stoplabel.lane);
  //   id.push_back(stoplabel.point);
  //   rawptarr.clear();
  //   rawptarr.push_back(pt);
  //   el.set_stopline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //   val = mapdata.getLeftBound(ptarr,lanelabel);
  //   cout << "Left lane at " << lanelabel << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel.segment);
  //   id.push_back(lanelabel.lane);
  //   id.push_back(0);
 
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //     val = mapdata.getRightBound(ptarr,lanelabel);
  //   cout << "Right lane at " << lanelabel << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel.segment);
  //   id.push_back(lanelabel.lane);
  //   id.push_back(1);
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  
  //   val = mapdata.getLeftBound(ptarr,lanelabel2);
  //   cout << "Left lane at " << lanelabel2 << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel2.segment);
  //   id.push_back(lanelabel2.lane);
  //   id.push_back(0);
  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //     val = mapdata.getRightBound(ptarr,lanelabel2);
  //   cout << "Right lane at " << lanelabel2 << " = " << ptarr << endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(lanelabel2.segment);
  //   id.push_back(lanelabel2.lane);
  //   id.push_back(1);

  //   rawptarr.clear();
  //   rawptarr = ptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);



  //   val = mapdata.getLeftBoundType(lanetype,lanelabel);
  //   cout << "Left lane type at " << lanelabel << " = " << lanetype << endl;

  //   val = mapdata.getRightBoundType(lanetype,lanelabel);
  //   cout << "Right lane type at " << lanelabel << " = " << lanetype << endl;

  //   val = mapdata.getTransitionBounds(lptarr,rptarr,exitlabel,enterlabel);
  //   cout << "Transition from " << exitlabel 
  //        << " to " << enterlabel 
  //        << ", left: " << lptarr << endl
  //        << "right: " << rptarr<<endl;
  //   el.clear();
  //   id.clear();  
  //   id.push_back(exitlabel.segment);
  //   id.push_back(exitlabel.lane);
  //   id.push_back(exitlabel.point);
  //   id.push_back(enterlabel.segment);
  //   id.push_back(enterlabel.lane);
  //   id.push_back(enterlabel.point);
  //   id.push_back(0);
  
  //   rawptarr.clear();
  //   rawptarr = lptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

  //   el.clear();
  //   id.clear();  
  //   id.push_back(exitlabel.segment);
  //   id.push_back(exitlabel.lane);
  //   id.push_back(exitlabel.point);
  //   id.push_back(enterlabel.segment);
  //   id.push_back(enterlabel.lane);
  //   id.push_back(enterlabel.point);
  //   id.push_back(1);
  
  //   rawptarr.clear();
  //   rawptarr = rptarr.arr;
  //   el.set_laneline(id,rawptarr);
  //   maptalker.sendMapElement(&el,subgroup);

   
    

  
 
 


 
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
