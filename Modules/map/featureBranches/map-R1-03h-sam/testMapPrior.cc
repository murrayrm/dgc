    
/**********************************************************
 **
 **  TESTMAPPRIOR.CC
 **
 **    Time-stamp: <2007-05-25 13:59:01 sam> 
 **
 **    Author: Sam Pfister 
 **    Created: Wed May  9 09:24:03 2007
 **
 **
 **********************************************************
 **
 **  
 **  
 **********************************************************/
     
                     
#include "Map.hh"
#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"

using namespace std;
 
 
int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testMap [rndf filename] [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  if(argc >1)
    fname = argv[1];
  if(argc >2)
    testnum = atoi(argv[2]);
  if(argc >3)
    sendSubGroup = atoi(argv[3]);
  if(argc >4)
    recvSubGroup = atoi(argv[4]);
  if(argc >5)
    skynetKey = atoi(argv[5]);
  
  cout << "RNDF filename = " << fname 
       << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey,recvSubGroup);

  Map mapdata;
  //  mapdata.loadRNDF("rn.txt");

  if (!mapdata.loadRNDF(fname.c_str())){
    cout << "Error loading " << fname << endl;
    return 0;
  };
 
  MapElement el;
	int numelements = mapdata.prior.data.size();
	cout << "elements = " << numelements << endl;  
	for (int i = 0 ; i < numelements; ++i){
		mapdata.prior.getEl(el,i);
    //TEMP 
    //el = mapper->map.prior.data[i];
	
    maptalker.sendMapElement(&el,sendSubGroup);
	}

 
  MapElement recvEl;
  MapElement sendEl;
  MapElement lineEl;
  MapElement clearEl;

  sendEl.setId(-1);
  sendEl.setTypePoints();
  sendEl.plotColor = MAP_COLOR_MAGENTA;

  lineEl.setId(-2);
  lineEl.setTypeLine();
    
  clearEl.setTypeClear();

  int retval;
  
  PointLabel label,label2;
  LaneLabel llabel,llabelout;
  point2 statept;
  point2_uncertain selectpt;
  point2arr_uncertain ptarr,ptarr1,ptarr2;
  vector<PointLabel> ptlabelarr, ptlabelarr2;
  vector<LaneLabel> llabelarr;
  vector<int> intarr;
  int count = 0;
  unsigned int lastsize=0;



  cout <<"========================================" << endl;
  testname = "Testing MapPrior::is EntryPoint ExitPoint StopLine CheckPoint ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    while (true){ 
 
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getPointArr(ptarr,ptlabelarr);
        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        maptalker.sendMapElement(&sendEl,sendSubGroup);
       
        if(mapdata.prior.isEntryPoint(label)){
          cout <<" isEntryPoint() returned true for "<< label  << endl;
        }else{
          cout <<" isEntryPoint() returned FALSE for "<< label  << endl;
        }
       
        if(mapdata.prior.isExitPoint(label)){
          cout <<" isExitPoint() returned true for "<< label  << endl;
        }else{
          cout <<" isExitPoint() returned FALSE for "<< label  << endl;
        }

        if(mapdata.prior.isStopLine(label)){
          cout <<" isStopLine() returned true for "<< label  << endl;
        }else{
          cout <<" isStopLine() returned FALSE for "<< label  << endl;
        }

        if(mapdata.prior.isCheckPoint(label)){
          cout <<" isCheckPoint() returned true for "<< label  << endl;
        }else{
          cout <<" isCheckPoint() returned FALSE for "<< label  << endl;
        }

       
 
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
  cout <<"========================================" << endl;
  testname = "Testing MapPrior::getWayPoint Entries Exits";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getPointArr(ptarr,ptlabelarr);
        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        sendEl.setId(-1);

        maptalker.sendMapElement(&sendEl,sendSubGroup);
 
        retval = mapdata.prior.getWayPointEntries(ptlabelarr,label);
        
        if (retval>0){
          cout <<  "Point " << label << " is an entry from labels : " << ptlabelarr <<  endl;
          retval = mapdata.prior.getWayPoint(ptarr,ptlabelarr);
          cout << "entry from points : " << ptarr << endl;
          sendEl.plotColor = MAP_COLOR_GREEN;
          sendEl.setGeometry(ptarr);
          sendEl.setId(-2);
          maptalker.sendMapElement(&sendEl,sendSubGroup);
        }else{
          clearEl.setId(-2);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
        
        
        retval = mapdata.prior.getWayPointExits(ptlabelarr,label);
        if (retval>0){

          cout << "Point " << label << " can exit to labels : " << ptlabelarr <<  endl;
          retval = mapdata.prior.getWayPoint(ptarr,ptlabelarr);
          cout << "exit to points : " << ptarr << endl;
          sendEl.plotColor = MAP_COLOR_YELLOW; 
          sendEl.setGeometry(ptarr);
          sendEl.setId(-3);
          maptalker.sendMapElement(&sendEl,sendSubGroup);
        }else{
          clearEl.setId(-3);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
  cout <<"========================================" << endl;
  testname = "Testing MapPrior::getLane LeftBound RightBound WayPoints";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        sendEl.setId(-1);

        maptalker.sendMapElement(&sendEl,sendSubGroup);
 
        retval = mapdata.prior.getLaneLeftBoundFull(ptarr,llabel);
        cout << "left bound : "<< ptarr <<  "retval = " << retval <<endl;
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_GREEN;
          lineEl.setGeometry(ptarr);
          lineEl.setId(-2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(-2); 
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
     
 
        retval = mapdata.prior.getLaneRightBoundFull(ptarr,llabel);
        cout << "right bound : "<< ptarr <<  "retval = " << retval <<endl;       
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_RED;
          lineEl.setGeometry(ptarr);
          lineEl.setId(-3);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{  
          clearEl.setId(-3); 
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
         
        retval = mapdata.prior.getLaneCenterLineFull(ptarr,llabel);
        cout << "center line : "<< ptarr <<  "retval = " << retval <<endl;
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_YELLOW;
          lineEl.setGeometry(ptarr);
          lineEl.setId(-4);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(-4);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
     retval = mapdata.prior.getLaneBoundsFull(ptarr,ptarr1,llabel);
     cout << "bounds line : "<< ptarr <<  " and "<< ptarr1 << "retval = " << retval <<endl;
        if (retval>=0){
          lineEl.plotColor = MAP_COLOR_MAGENTA;
          lineEl.setGeometry(ptarr);
          lineEl.setId(-6);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          lineEl.plotColor = MAP_COLOR_MAGENTA;
          lineEl.setGeometry(ptarr1);
          lineEl.setId(-7);
          maptalker.sendMapElement(&lineEl,sendSubGroup);

        }else{
          clearEl.setId(-6);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
          clearEl.setId(-7);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
   
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
   
  cout <<"========================================" << endl;
  testname = "Testing MapPrior::getLane ExitLabels ExitToLabels EntryLabels EntryFromLabels";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        sendEl.setId(-1);

        maptalker.sendMapElement(&sendEl,sendSubGroup);
 
        if (count==0){
          count = 1;
        
          retval = mapdata.prior.getLaneExits(ptlabelarr,ptlabelarr2,llabel);
          cout << "Exit labels : "<< ptlabelarr << endl;         
          cout << "Exit To labels : "<< ptlabelarr2 <<  "retval = " << retval <<endl;  
          retval = mapdata.prior.getWayPoint(ptarr,ptlabelarr);
          cout << "Exit points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.prior.getWayPoint(ptarr2,ptlabelarr);
          cout << "Exit To points : " << ptarr2 << "retval = " << retval <<endl;

          if (ptarr.size() != ptarr2.size()){
            cerr << "SIZE MISMATCH FOR EXIT LABELS - ERROR PARSING" << endl;
            continue;
          }
        }else{
          count = 0;

          retval = mapdata.prior.getLaneEntries(ptlabelarr,ptlabelarr2,llabel);
          cout << "Entry labels : "<< ptlabelarr << endl;         
          cout << "Entry To labels : "<< ptlabelarr2 <<  "retval = " << retval <<endl;  
          retval = mapdata.prior.getWayPoint(ptarr,ptlabelarr);
          cout << "Entry points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.prior.getWayPoint(ptarr2,ptlabelarr);
          cout << "Entry To points : " << ptarr2 << "retval = " << retval <<endl;

        }
        
        for (unsigned int j =ptarr.size(); j<lastsize;++j){
          clearEl.setId(-4-j);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
        lastsize = ptarr.size();
        for (unsigned int j =0; j<ptarr.size();++j){
          lineEl.plotColor = MAP_COLOR_YELLOW;
          lineEl.setGeometry(ptarr[j],ptarr2[j]);
          lineEl.setId(-4-j);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }

        sendEl.plotColor = MAP_COLOR_RED;
        sendEl.setGeometry(ptarr);
        sendEl.setId(-2);
        maptalker.sendMapElement(&sendEl,sendSubGroup);

        sendEl.plotColor = MAP_COLOR_GREEN;
        sendEl.setGeometry(ptarr2);
        sendEl.setId(-3);
        maptalker.sendMapElement(&sendEl,sendSubGroup);

      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
    
   


  cout <<"========================================" << endl;
  testname = "Testing MapPrior::getLane StopLineLabels CheckPointLabels CheckPointNumbers";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        sendEl.setId(-1);

        maptalker.sendMapElement(&sendEl,sendSubGroup);
 
        if (count==0){
          count = 1;
        
          retval = mapdata.prior.getLaneStopLines(ptlabelarr,llabel);
          cout << "Stop line labels : "<< ptlabelarr <<  "retval = " << retval <<endl;          
          retval = mapdata.prior.getWayPoint(ptarr,ptlabelarr);
          cout << "Stop line points : " << ptarr << "retval = " << retval <<endl;
          sendEl.plotColor = MAP_COLOR_RED;              
          
        }else{
          count = 0; 
          
          retval = mapdata.prior.getLaneCheckPoints(ptlabelarr,llabel);
          cout << "Check point labels : "<< ptlabelarr <<  "retval = " << retval <<endl;          
          retval = mapdata.prior.getWayPoint(ptarr,ptlabelarr);
          cout << "Check point points : " << ptarr << "retval = " << retval <<endl;
          retval = mapdata.prior.getLaneCheckPointNumbers(intarr,llabel);
          for (unsigned int j=0;j<intarr.size();++j){
            cout << "label " <<ptlabelarr[j] << "  num " << intarr[j] << endl;
          }
          sendEl.plotColor = MAP_COLOR_GREEN;
        }
        if (ptarr.size()>0){
          sendEl.setGeometry(ptarr);
          sendEl.setId(-2);
          maptalker.sendMapElement(&sendEl,sendSubGroup);
        }else{
          clearEl.setId(-2);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }

      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
  

  cout <<"========================================" << endl;
  testname = "Testing MapPrior::getLaneLabelNeighbor";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    while (true){ 

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        sendEl.setId(-1);

        maptalker.sendMapElement(&sendEl,sendSubGroup);
 
        if (count==0){
          //count = 1;
         
          retval = mapdata.prior.getNeighborLane(llabelout,llabel,-1);
          cout << "lane label : "<< llabelout <<  "retval = " << retval <<endl;          
          retval = mapdata.prior.getLaneBounds(ptarr1,ptarr2,llabelout);
                       
          
        }else{
          count = 0; 
            
        }
        if (retval>=0){
          lineEl.setGeometry(ptarr1);
          lineEl.setId(-2);
          lineEl.plotColor = MAP_COLOR_GREEN;
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          lineEl.setGeometry(ptarr2);
          lineEl.plotColor = MAP_COLOR_RED;
          lineEl.setId(-3);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(-2);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
          clearEl.setId(-3);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
      } 
      else{ 
        usleep (100000); 
      }
    } 
  }  
   


  cout <<"========================================" << endl;
  testname = "Testing MapPrior::getLane DirLabels Bounds";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    while (true){ 
 
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        statept = recvEl.center;
        //        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        retval=  mapdata.getClosestPointID(label, statept);
        cout << "ptlabel = " << label << endl;  

        retval = mapdata.prior.getWayPoint(selectpt,label);
        cout << "selected point coords " << selectpt << endl;  
        //retval = mapdata.prior.getWayPointEntryLabels(ptlabelarr,label);
        //retval = mapdata.prior.getWayPointArr(ptarr,ptlabelarr);
        llabel.segment = label.segment; 
        llabel.lane = label.lane;

        sendEl.plotColor = MAP_COLOR_MAGENTA;
        sendEl.setGeometry(selectpt);
        sendEl.setId(-1);

        maptalker.sendMapElement(&sendEl,sendSubGroup);
 

         
        retval = mapdata.prior.getSameDirLanes(llabelarr,llabel);
        cout << "Same Dir label arr : "<< llabelarr <<  "retval = " << retval <<endl;          

        retval = mapdata.prior.getOppDirLanes(llabelarr,llabel);
        cout << "Opposite Dir label arr : "<< llabelarr <<  "retval = " << retval <<endl;          

        retval = mapdata.prior.getAllDirLanes(llabelarr,llabel);
        cout << "Any Dir label arr : "<< llabelarr <<  "retval = " << retval <<endl;          

        if (count==0){
          count = 1;                                   
          retval = mapdata.prior.getSegmentBounds(ptarr1,ptarr2,llabel);
          cout << "Get Segment bounds retval = " << retval <<endl;          
        } else if (count==1){
          count = 2;                                   
          retval = mapdata.prior.getSameDirLaneBounds(ptarr1,ptarr2,llabel);
          cout << "Get Same Direction bounds retval = " << retval <<endl;          
       
        }else{
          count = 0; 
          retval = mapdata.prior.getOppDirLaneBounds(ptarr1,ptarr2,llabel);
          cout << "Get Opposite Direction bounds retval = " << retval <<endl;          
          
        }
        if (retval>=0){
          lineEl.setGeometry(ptarr1);
          lineEl.setId(-2);
          lineEl.plotColor = MAP_COLOR_GREEN;
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          lineEl.setGeometry(ptarr2);
          lineEl.plotColor = MAP_COLOR_RED;
          lineEl.setId(-3);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        }else{
          clearEl.setId(-2);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
          clearEl.setId(-3);
          maptalker.sendMapElement(&clearEl,sendSubGroup);
        }
      } 
      else{ 
        usleep (100000);
      }
    } 
  } 
  

  cout <<"========================================" << endl;
  testname = "Testing ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool firstval = true;
    
    while(1){

      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){

        if (firstval){
          statept = recvEl.center;
          //        recvEl.print();
          cout << endl <<"For point " << statept << endl;
          retval=  mapdata.getClosestPointID(label, statept);
          cout << "exit from ptlabel = " << label << endl;  
          firstval =false;
        }else{
          statept = recvEl.center;
          //        recvEl.print();
          cout << endl <<"For point " << statept << endl;
          retval=  mapdata.getClosestPointID(label2, statept);
          cout << "enter to ptlabel = " << label << endl;  

          retval = mapdata.prior.getTransitionBounds(ptarr1,ptarr2,label,label2);
          if (retval<0){
            cout <<"NO TRANSITION FOUND" << endl;
            clearEl.setId(-2);
            maptalker.sendMapElement(&clearEl,sendSubGroup);
            clearEl.setId(-3);
            maptalker.sendMapElement(&clearEl,sendSubGroup);
          }
          else{
            lineEl.setGeometry(ptarr1);
            lineEl.setId(-2);
            lineEl.plotColor = MAP_COLOR_GREEN;
            maptalker.sendMapElement(&lineEl,sendSubGroup);
            lineEl.setGeometry(ptarr2);
            lineEl.plotColor = MAP_COLOR_RED;
            lineEl.setId(-3);
            maptalker.sendMapElement(&lineEl,sendSubGroup);
          }

          firstval = true;
        }

        
      }      
    }
  } 

  


 
  
  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  

