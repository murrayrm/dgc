/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-05-08 17:20:09 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "MapElement.hh"


class Map
{
  
public:
  
  Map();
  
  ~Map();

  bool loadRNDF(string filename) {return prior.loadRNDF(filename);}

  vector<MapElement> data;
  MapPrior prior;

  int addEl(const MapElement &el);
  void initLaneLines();




  //--------------------------------------------------
  // tested functions
  //--------------------------------------------------
  bool checkLaneID(const point2 pt, const int segment, const int lane);
  bool checkLaneID(const point2 pt, const LaneLabel &label);

  

  int getSegmentID(const point2 pt);
  int getLaneID(const point2 pt);
  //  int getZoneID(const point2 pt);
  int getNextPointID(PointLabel &label,const point2& pt);
  int getLastPointID(PointLabel &label,const point2& pt);
  int getClosestPointID(PointLabel &label, const point2& pt);

  int getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);

  // PointLabel getNextPointID(const point2& pt);
  // PointLabel getClosestPointID(const point2& pt);

  
  int getLeftBound(point2arr& ptarr, const LaneLabel &label );
  int getRightBound(point2arr& ptarr, const LaneLabel &label );

  int getLeftBoundType(string& type, const LaneLabel &label);
  int getRightBoundType(string& type, const LaneLabel &label);

  int getStopline(point2& pt, PointLabel &label);
  int getNextStopline(point2& pt, PointLabel &label);
  int getStopline(point2& pt, point2 state);
  
  int getWaypoint(point2& pt, const PointLabel &label);

  
  int getBounds(point2arr& leftbound, point2arr& rightbound, 
                const LaneLabel &label,const point2 state, 
                const double range , const double backrange=10);
 int getBoundsReverse(point2arr& leftbound, point2arr& rightbound, 
                      const LaneLabel &label,const point2 state, 
                      const double range, const double backrange=10);
  
  int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , 
                          const PointLabel &exitlabel, 
                          const PointLabel &enterlabel, 
                          const point2 pt, const double range, 
                          const double backrange=10);

//   int getMatchedBounds(point2arr& leftbound, point2arr& rightbound, 
//                        const LaneLabel &label,const point2 state, 
//                        const double range , const double backrange=10);
//   int getMatchedBoundsReverse(point2arr& leftbound, 
//                               point2arr& rightbound, 
//                               const LaneLabel &label,
//                               const point2 state, 
//                               const double range, 
//                               const double backrange=10);
  
//   int getMatchedTransitionBounds(point2arr &leftbound ,
//                                  point2arr &rightbound , 
//                                  const PointLabel &exitlabel, 
//                                  const PointLabel &enterlabel, 
//                                  const point2 pt, const double range, 
//                                  const double backrange=10);



  int getHeading(double & ang, const PointLabel &label);
  int getHeading(double & ang, const point2 &pt);


 

  bool isStop(PointLabel &label);
  bool isExit(PointLabel &label);

  int getWaypointArr(point2arr& ptarr, const PointLabel &label);
  int getLaneDistToWaypoint(double& dist,const point2 state,const PointLabel &label);
  int getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label);

 double getObstacleDist(const point2& state,const int lanedelta=0);
  point2 getObstaclePoint(const point2& state, const double offset=0);
  int getObstacleGeometry(point2arr& ptarr);

};



#endif
 
