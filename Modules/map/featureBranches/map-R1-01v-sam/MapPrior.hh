/**********************************************************
 **
 **  MAPPRIOR.HH
 **
 **    Time-stamp: <2007-05-08 23:41:00 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Feb 19 12:51:26 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPRIOR_H
#define MAPPRIOR_H

#include <math.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

#include "dgcutils/ggis.h"

#include "MapElement.hh"
#include "frames/point2_uncertain.hh"

class LaneLabel;

class PointLabel
{
public:
  PointLabel() {}
  PointLabel(int s, int l, int p){segment = s; lane = l; point = p;}
  ~PointLabel() {}

  bool operator==(const PointLabel &label) const{ return (segment==label.segment && lane==label.lane && point==label.point);}
  bool operator==(const LaneLabel &label) const;

  friend ostream &operator<<(ostream &os, const PointLabel &label);
void print()  { cout << segment <<" "<< lane <<" " << point << endl; }
  int segment, lane, point;
};




class LaneLabel
{
public:
  LaneLabel() {}
  LaneLabel(int s, int l) {segment = s; lane = l;}
  ~LaneLabel() {}

  bool operator==(const LaneLabel &label) const { return (segment==label.segment && lane==label.lane);}
  bool operator==(const PointLabel &label) const;
  friend ostream &operator<<(ostream &os, const LaneLabel &label);


  void print() { cout << segment <<" "<< lane <<endl; }  
  int segment, lane;
};

class SpotLabel
{
public:
  SpotLabel() {}
  SpotLabel(int z, int s) {zone = z; spot = s;}
  ~SpotLabel() {}

  bool operator==(const SpotLabel &label) { return (zone==label.zone && spot==label.spot);}
  void print() { cout << zone <<" "<< spot <<endl; }  
  int zone, spot;
};



class RNDFsegment
{
public:
  RNDFsegment() {}
  RNDFsegment(int val) {label=val;}

  ~RNDFsegment() {}
  int label;
  string name;
  vector<int> laneindex;
  vector<LaneLabel> lane_label;
};

class RNDFlane
{
public:
  RNDFlane() {}
  RNDFlane(LaneLabel& val) {label=val;}
  ~RNDFlane() {}
  LaneLabel label;
  string name;
  double width;
  
  int waypoint_elindex;
  int leftbound_elindex;
  int rightbound_elindex;
  vector<PointLabel> waypoint_label;
  
  string leftbound_type;
  string rightbound_type;
  
  int leftneighbor_laneindex;
  int rightneighbor_laneindex;
  
  //vector<int> exit_ptindex;
  vector<PointLabel> exit_label;
  vector<PointLabel> exit_link;
  //vector<int> exit_intindex;

  // vector<int> entry_ptindex;
  vector<PointLabel> entry_label;
  vector<PointLabel> entry_link;
  // vector<int> entry_intindex;

  //  vector<int> stop_ptindex;
  vector<PointLabel> stop_label;
 
  //vector<int> check_ptindex;
  vector<PointLabel> checkpt_label;
  vector<int> checkpt_num;

};


class RNDFzone
{
public:
  RNDFzone() {}
  RNDFzone(int val) {label=val;}
  ~RNDFzone() {}
  int label;  
  string name;
  int perimeter_elindex;
  vector<PointLabel> perimeter_label;

  //  SpotLabel perimeter_label;

  vector<int> spotindex;
  vector<SpotLabel> spot_label;


  //vector<int> exit_ptindex;
  vector<PointLabel> exit_link;
  vector<PointLabel> exit_label;
  //  vector<int> exit_intindex;

  // vector<int> entry_ptindex;
  vector<PointLabel> entry_label;
  vector<PointLabel> entry_link;
  // vector<int> entry_intindex;

  
};

class RNDFspot
{
public:
  RNDFspot() {}
  RNDFspot(SpotLabel& val) {label=val;}
  ~RNDFspot() {}
  SpotLabel label;
  string name;
  
  double width;
  
  int waypt_elindex;
  vector<PointLabel> waypoint_label;

  int leftbound_elindex;
  int rightbound_elindex;
  
  vector<int> check_ptindex;
  vector<PointLabel> checkpt_label;
  vector<int> checkpt_num;

};


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class MapPrior
{
  
public:
  
  //! A Constructor 
  /*! DETAILS */
  MapPrior();
  
  //! A Destructor 
  /*! DETAILS */
  ~MapPrior();

  vector<MapElement> data;

  bool loadRNDF(string filename);
  bool parseRNDF(vector<string>& strfile);
  //  bool parseHeader(vector<string>& strfile);
  //  bool parseSegments(vector<string>& strfile);
  //  bool parseZones(vector<string>& strfile);
  bool parsePointLabel(string&, PointLabel& label);
  bool parseLaneLabel(string&, LaneLabel& label);
  bool parseSpotLabel(string&, SpotLabel& label);


  bool getEl(int index, MapElement & el);


  vector<RNDFsegment> segments;
  vector<RNDFlane> lanes;
  vector<RNDFzone> zones;  
  vector<RNDFspot> spots;
  vector<PointLabel> checkpoints;
  
  int numSegments;
  int numZones;
  string RNDFname;
  string RNDFdate;
  double RNDFversion;


  //--------------------------------------------------
  // 
  //--------------------------------------------------
private:
  int getLaneIndex(const LaneLabel &label);
int getLaneIndex(const PointLabel &label);
int getLaneIndex(const int segnum, const int lanenum);

  int getLanePointIndex(const PointLabel &label);
  int getLanePointIndex(const int segnum, const int lanenum, const int ptnum);


public:


  //general queries
  bool isEntryPoint(const PointLabel &ptlabelin);
  bool isExitPoint(const PointLabel &ptlabelin);
  bool isStopLine(const PointLabel &ptlabelin);
  bool isCheckPoint(const PointLabel &ptlabelin);



  int getLaneLeftBound(point2arr_uncertain &bound, 
                       const LaneLabel &lanelabelin);
  int getLaneRightBound(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin);
  int getLaneBounds(point2arr_uncertain &leftbound, 
                    point2arr_uncertain &rightbound, 
                    const LaneLabel &lanelabelin);
  int getLaneWayPoints(point2arr_uncertain &waypoints, 
                       const LaneLabel &lanelabelin);

  int getPoint(point2_uncertain &pt, 
               const PointLabel &ptlabelin);
  int getPointArr(point2arr_uncertain &ptarr, 
                  const vector<PointLabel> &ptlabelarrin);


  //intersection queries
  int getPointEntryLabels(vector<PointLabel>& ptlabelarr,
                          const PointLabel &ptlabelin);
  int getPointExitLabels(vector<PointLabel>& ptlabelarr,
                         const PointLabel &ptlabelin);


  int getLaneExitLabels(vector<PointLabel>& ptlabelarr, 
                        const LaneLabel &lanelabelin);
  int getLaneExitToLabels(vector<PointLabel>& ptlabelarr, 
                          const LaneLabel &lanelabelin);

  int getLaneEntryLabels(vector<PointLabel>& ptlabelarr,
                         const LaneLabel &lanelabelin);
  int getLaneEntryFromLabels(vector<PointLabel>& ptlabelarr,
                             const LaneLabel &lanelabelin);

  int getLaneStopLineLabels(vector<PointLabel>& ptlabelarr,
                            const LaneLabel &lanelabelin);
  int getLaneCheckPointLabels(vector<PointLabel>& ptlabelarr,
                              const LaneLabel &lanelabelin);

  int getLaneToLeft(LaneLabel& lanelabel, 
                    const LaneLabel &lanelabelin);
  int getLaneToRight(LaneLabel& lanelabel, 
                     const LaneLabel &lanelabelin);
  int getLaneName(string &name, 
                  const LaneLabel &lanelabelin);


  int getSegmentLanes(vector<LaneLabel>& lanelabelarr,
                      const int seglabelin);
  int getSegmentLanesSameDirection(vector<LaneLabel>& lanelabelarr,
                                   const int seglabelin);
  int getSegmentLanesOtherDirection(vector<LaneLabel>& lanelabelarr,
                                    const int seglabelin);

  int getSegmentName(string &name, const int seglabelin);
  int getSegmentEntryFromLabels(vector<PointLabel>& lanelabelarr,
                                const int seglabelin);
  

  point2_uncertain delta; // global to local delta

};
#endif
