/**********************************************************
 **
 **  MAPPRIOR.CC
 **
 **    Time-stamp: <2007-05-08 23:42:25 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Feb 19 12:51:18 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#include "MapPrior.hh"

using namespace std;

bool PointLabel::operator==(const LaneLabel &label) const
{
  return (segment==label.segment && lane==label.lane);
}

bool LaneLabel::operator==(const PointLabel &label) const
{ return (segment==label.segment && lane==label.lane);}

ostream &operator<<(ostream &os, const PointLabel &label)
{
  os << "(" << label.segment <<", " << label.lane << ", " << label.point <<")" ;
  return os;
}

ostream &operator<<(ostream &os, const LaneLabel &label)
{
  os << "(" << label.segment <<", " << label.lane <<")" ;
  return os;
}


MapPrior::MapPrior()
{}

MapPrior::~MapPrior()
{}

bool MapPrior::loadRNDF(string filename)
{
  ifstream ifs;
  vector<string> strfile;
  string line;

  segments.clear();
  lanes.clear();
  zones.clear();
  spots.clear();
  checkpoints.clear();

  numSegments= -1;
  numZones = -1;
  RNDFversion = -1;

  ifs.open(filename.c_str(),ios::in);
  if (!ifs){
    cerr << "Error in MapPrior.cc, " << filename << " not found." << endl;
    return false;
  }
  
  while(getline(ifs,line)) {
    strfile.push_back(line);
  }
  
  cout << "Parsing RNDF" << endl;
  if (!parseRNDF(strfile)){
    cerr << "MapPrior.cc: Error parsing RNDF in " << filename << endl;
    return false;
  }

  
  return true;
}

bool MapPrior::parseRNDF(vector<string>& strfile)
{
  int i, j;
  int filesize = strfile.size();
  string word;
  int thisSegment = -1;
  int thisSegmentIndex = -1;
 
  int thisNumLanes = -1;
  int thisNumPoints = -1;
  int thisNumSpots = -1;

  double FOOT_PER_METER = 3.2808399;
  //  int thisLane = -1;
  int thisLaneIndex = -1;

  int thisDataIndex = -1;

  int thisCheckPoint = -1;

  string laneString, pointString, spotString;
  point2 thisPoint;
 
  MapElement el;
  LaneLabel laneLabel;
  PointLabel pointLabel;
  SpotLabel spotLabel;
  
  int thisZone = -1;
  int thisZoneIndex = -1;
  int thisSpotIndex = -1;
  
  int parsemode = 0;
  GisCoordLatLon latlon;
  GisCoordUTM utm;

  point2arr ptarr;
  point2arr_uncertain ptarr_unc, cptarr_unc;

  for (i=0; i<filesize; ++i){
    istringstream lstream(strfile[i]);    
    lstream >> word;  



    if (parsemode ==0) {// base mode
    
      if (word =="RNDF_name"){ lstream >> RNDFname; continue;}
      if (word =="num_segments"){ lstream >> numSegments;continue;}
      if (word == "num_zones"){  lstream >> numZones;continue;  }
      if (word == "format_version"){lstream >> RNDFversion;  continue;}
      if (word == "creation_date"){lstream >> RNDFdate;continue;}

      if (word =="segment"){ 
        lstream >> thisSegment;
        thisSegmentIndex = segments.size();
        segments.push_back(RNDFsegment(thisSegment));
        parsemode = 1;// switch to segment mode
        continue;
      }
      if (word =="zone"){ 
        lstream >> thisZone;
        thisZoneIndex = zones.size();
        zones.push_back(RNDFzone(thisZone));
        parsemode = 3; //switch to zone mode
        continue;
      }
    } //end base mode
    if (parsemode == 1){ // segment mode
      if (word =="num_lanes"){ lstream >> thisNumLanes; continue;}
      if (word =="segment_name"){ lstream >> segments[thisSegmentIndex].name; continue;}
      if (word =="end_segment"){
        if (thisNumLanes != (int)segments[thisSegmentIndex].laneindex.size()){
          cerr << "size mismatch in segment " << thisSegmentIndex << endl;
          return false;
        }
        thisSegment = -1; 
        thisNumLanes = -1;
        parsemode = 0;  // switch to base mode
        continue;
      }
      if (word =="lane"){ 
        lstream >> laneString;
        if (!parseLaneLabel(laneString,laneLabel)) return false;
        thisLaneIndex = lanes.size();
        lanes.push_back(RNDFlane(laneLabel));
        segments[thisSegmentIndex].laneindex.push_back(thisLaneIndex);
        segments[thisSegmentIndex].lane_label.push_back(laneLabel);
        thisDataIndex = data.size();
        el.clear();
        el.id.push_back(laneLabel.segment);
        el.id.push_back(laneLabel.lane);
        el.type = ELEMENT_WAYPOINT;
        el.geometry_type = GEOMETRY_ORDERED_POINTS;
        el.frame_type = FRAME_GLOBAL;
        el.plot_color = MAP_COLOR_BLUE;
        el.plot_value = 50;
       
        data.push_back(el);
        lanes[thisLaneIndex].waypoint_elindex = thisDataIndex;
        parsemode = 2; // switch to lane mode;
        
        continue;
      }
    }



    if (parsemode ==2){ //lane mode
      if (word =="num_waypoints"){ lstream >> thisNumPoints; continue;}
      if (word =="lane_width"){ lstream >> lanes[thisLaneIndex].width;
      lanes[thisLaneIndex].width = lanes[thisLaneIndex].width/FOOT_PER_METER;
continue;}
      if (word =="left_boundary"){ lstream >> lanes[thisLaneIndex].leftbound_type; continue;}
      if (word =="right_boundary"){ lstream >> lanes[thisLaneIndex].rightbound_type; continue;}
      if (word =="checkpoint"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].checkpt_label.push_back(pointLabel);
        lstream >> thisCheckPoint;
        lanes[thisLaneIndex].checkpt_num.push_back(thisCheckPoint);
        
        thisCheckPoint = -1;
        continue;
      }
      if (word =="stop"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].stop_label.push_back(pointLabel);
        continue;
      }    
      if (word =="exit"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].exit_label.push_back(pointLabel);
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        lanes[thisLaneIndex].exit_link.push_back(pointLabel);
        continue;
      }
      if (word[0] >='0' && word[0] <='9'){
        if (!parsePointLabel(word,pointLabel)) return false;
        lstream >> latlon.latitude;
        lstream >> latlon.longitude;
        gis_coord_latlon_to_utm(&latlon,&utm,GEODETIC_MODEL);
        thisPoint.y = utm.e;
        thisPoint.x = utm.n;
        lanes[thisLaneIndex].waypoint_label.push_back(pointLabel);
        
        data[thisDataIndex].geometry.push_back(thisPoint);
        
        continue;
      } 
      if (word =="end_lane"){
        if (thisNumPoints != (int)data[lanes[thisLaneIndex].waypoint_elindex].geometry.size()){
          cerr << "size mismatch in lane " << thisLaneIndex << endl;
          return false;
        }
        data[thisDataIndex].set_center_from_geometry();

        lanes[thisLaneIndex].waypoint_elindex = thisDataIndex;

        cptarr_unc = data[thisDataIndex].geometry;

        ptarr = cptarr_unc;
        if (lanes[thisLaneIndex].width>0)
          ptarr = ptarr.get_offset(-lanes[thisLaneIndex].width/2);
        else
          ptarr = ptarr.get_offset(-6);
        
        ptarr_unc = ptarr;

        thisDataIndex = data.size();
        el.clear();
        el.id.push_back(laneLabel.segment);
        el.id.push_back(laneLabel.lane);
        el.id.push_back(0);
        el.type = ELEMENT_LANELINE;
        el.geometry_type = GEOMETRY_LINE;
        el.frame_type = FRAME_GLOBAL;
        el.geometry = ptarr_unc.arr;
        el.set_center_from_geometry();
        el.plot_color = MAP_COLOR_BLUE;
        el.plot_value = 100;
        data.push_back(el);
        lanes[thisLaneIndex].leftbound_elindex = thisDataIndex;


        ptarr = cptarr_unc;
        if (lanes[thisLaneIndex].width>0)
          ptarr = ptarr.get_offset(lanes[thisLaneIndex].width/2);
        else
          ptarr = ptarr.get_offset(6);
        
        ptarr_unc = ptarr;

        thisDataIndex = data.size();
        el.clear();
        el.id.push_back(laneLabel.segment);
        el.id.push_back(laneLabel.lane);
        el.id.push_back(1);
        el.type = ELEMENT_LANELINE;
        el.geometry_type = GEOMETRY_LINE;
        el.frame_type = FRAME_GLOBAL;
        el.geometry = ptarr_unc.arr;
        el.set_center_from_geometry();
        el.plot_color = MAP_COLOR_BLUE;
        el.plot_value = 100;
        data.push_back(el);
        lanes[thisLaneIndex].rightbound_elindex = thisDataIndex;


        parsemode = 1; //switch to segment mode
        continue;
      }
    } // end if lane mode
    


    if (parsemode ==3){ //zone mode
      if (word =="num_spots"){ lstream >> thisNumSpots; continue;}      
      if (word =="zone_name"){ lstream >> zones[thisZoneIndex].name; continue;}
      if (word =="perimeter"){ 
        lstream >> spotString;
        if (!parseSpotLabel(spotString,spotLabel)) return false;
        //zones[thisZoneIndex].perimeter_label = spotLabel;
        thisDataIndex= data.size();

        el.clear();
        el.id.push_back(spotLabel.zone);
        el.id.push_back(spotLabel.spot);
        el.type=ELEMENT_PERIMETER;
        el.geometry_type = GEOMETRY_POLY;
        el.frame_type = FRAME_GLOBAL;
        data.push_back(el);
        zones[thisZoneIndex].perimeter_elindex = thisDataIndex;
        parsemode =4; // switch to perimeter mode
        continue;
      }
      if (word =="spot"){ 
       
        lstream >> spotString;
        if (!parseSpotLabel(spotString,spotLabel)) return false;
        //  cout << "spot = " << spotString << endl;
        thisSpotIndex = spots.size();
        spots.push_back(RNDFspot(spotLabel));
        zones[thisZoneIndex].spotindex.push_back(thisSpotIndex);
        zones[thisZoneIndex].spot_label.push_back(spotLabel);

        thisDataIndex= data.size();
        el.clear();
        el.id.push_back(spotLabel.zone);
        el.id.push_back(spotLabel.spot);
        el.type=ELEMENT_PARKING_SPOT;
        el.geometry_type = GEOMETRY_ORDERED_POINTS;
        el.frame_type = FRAME_GLOBAL;
        data.push_back(el);
        spots[thisSpotIndex].waypt_elindex=(thisDataIndex);
        
        parsemode =5; // switch to spot mode
        continue;
      }


      if (word =="end_zone"){ 
        if (thisNumSpots != (int)zones[thisZoneIndex].spotindex.size()){
          cerr << "size mismatch in zone " << thisZoneIndex << endl;
          return false;
        }
        parsemode = 0; //switch to base mode
        continue;
      }
    } //end zone mode


    if (parsemode ==4){ //perimeter mode
      if (word =="num_perimeterpoints"){ lstream >> thisNumPoints; continue;}
      if (word =="exit"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        zones[thisZoneIndex].exit_label.push_back(pointLabel);
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        zones[thisZoneIndex].exit_link.push_back(pointLabel);
        continue;
      }
      if (word[0] >='0' && word[0] <='9'){
        if (!parsePointLabel(word,pointLabel)) return false;
        lstream >> latlon.latitude;
        lstream >> latlon.longitude;
        gis_coord_latlon_to_utm(&latlon,&utm,GEODETIC_MODEL);
        thisPoint.y = utm.e;
        thisPoint.x = utm.n;
        zones[thisZoneIndex].perimeter_label.push_back(pointLabel);
        data[thisDataIndex].geometry.push_back(thisPoint);
        
        continue;
      }
      if (word =="end_perimeter"){
        if (thisNumPoints != (int)data[zones[thisZoneIndex].perimeter_elindex].geometry.size()){
           cerr << "size mismatch in zone " << thisZoneIndex << endl;
          return false;
        }
        data[thisDataIndex].set_center_from_geometry();
  
        parsemode = 3; //switch to zone mode
        continue;
      }

    } // end perimeter mode


    if (parsemode ==5){ //spot mode
      if (word =="spot_width"){ lstream >> spots[thisSpotIndex].width;
spots[thisSpotIndex].width = spots[thisSpotIndex].width/FOOT_PER_METER;
 continue;}
      if (word =="checkpoint"){ 
        lstream >> pointString;
        if (!parsePointLabel(pointString,pointLabel)) return false;
        spots[thisSpotIndex].checkpt_label.push_back(pointLabel);
        lstream >> thisCheckPoint;
        spots[thisSpotIndex].checkpt_num.push_back(thisCheckPoint);
        thisCheckPoint = -1;
        continue;
      }

      if (word[0] >='0' && word[0] <='9'){
        if (!parsePointLabel(word,pointLabel)) return false;
        lstream >> latlon.latitude;
        lstream >> latlon.longitude;
        gis_coord_latlon_to_utm(&latlon,&utm,GEODETIC_MODEL);
        thisPoint.y = utm.e;
        thisPoint.x = utm.n;
        spots[thisSpotIndex].waypoint_label.push_back(pointLabel);
        data[thisDataIndex].geometry.push_back(thisPoint);
        continue;
      }
      if (word =="end_spot"){
        if (data[thisDataIndex].geometry.size() != 2){
          cerr << "size mismatch in spot " << thisSpotIndex << endl;
          return false;
        }
        data[thisDataIndex].set_center_from_geometry();
        parsemode = 3; //switch to zone mode
        continue;
      }
     
    } // end spot mode
    
  }


  int exitsize= 0;
  //  bool checkval = false;
  PointLabel thisptlabel, thislinklabel;
  int entrylaneindex, entrypointindex;
  
  cout << "Parsing exit points " << endl;
  for (i=0;i<(int)lanes.size();++i){
    //    cout << "lane " << lanes[i].label << endl;
    exitsize = lanes[i].exit_label.size();
    if (exitsize!=(int)lanes[i].exit_link.size()){
        cerr << "in MapPrior::parseRNDF size mismatch in parsed exit labels for lane index " << i<< endl;
    }
    for (j=0;j<exitsize;++j){
      thisptlabel = lanes[i].exit_label[j];
      thislinklabel =  lanes[i].exit_link[j];
      // cout << "exit " << thisptlabel << " link to " << thislinklabel << endl;      

      entrylaneindex = getLaneIndex(thislinklabel);
      entrypointindex = getLanePointIndex(thislinklabel);
      if (entrylaneindex <0 || entrypointindex <0){
        cerr << "in MapPrior::parseRNDF, bad link specified from "<< thisptlabel << " to " << thislinklabel << ".   Target point or lane not found" << endl;
        break;
      }


      //lanes[entrylaneindex].entry_ptindex.push_back(entry_ptindex);
      lanes[entrylaneindex].entry_label.push_back(thislinklabel);
      lanes[entrylaneindex].entry_link.push_back(thisptlabel);
    }
  }

  cout << "Loaded RNDF header " << endl <<
    "    RNDF name = " <<RNDFname << endl <<
    "    RNDF date = " <<RNDFdate << endl <<
    "    RNDF version = " <<RNDFversion << endl <<
    "    RNDF num segments = " << numSegments << endl <<
    "    RNDF num zones = " << numZones << endl;

  if (data.size()>0){
    if (data[0].geometry.size()>0){
      delta = data[0].geometry[0];
    }
  }

  return true;
}


bool MapPrior::parseLaneLabel(string& str, LaneLabel& label)
{
  int startpos= 0;
  int endpos = 0;
  string tmpstr;
  
  startpos = endpos;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){
    cout<<"error parsing lane label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.segment = atoi(tmpstr.c_str());
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  tmpstr = str.substr(startpos,endpos);
  label.lane = atoi(tmpstr.c_str());  

  return true;
}
 
bool MapPrior::parsePointLabel(string& str, PointLabel& label)
{
  int startpos= 0;
  int endpos = 0;
  string tmpstr;
  startpos = endpos;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){ 
    cout<<"error parsing point label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.segment = atoi(tmpstr.c_str());
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){
    cout<<"error parsing point label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.lane = atoi(tmpstr.c_str());
  
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  tmpstr = str.substr(startpos,endpos);
  label.point = atoi(tmpstr.c_str());  

  return true;
}

bool MapPrior::parseSpotLabel(string& str, SpotLabel& label)
{
  int startpos= 0;
  int endpos = 0;
  string tmpstr;
  
  startpos = endpos;
  endpos = str.find_first_of('.',startpos);
  if (endpos==-1){
    cout<<"error parsing spot label "<<str<<endl;
    return false;}
  tmpstr = str.substr(startpos,endpos);
  label.zone = atoi(tmpstr.c_str());
  startpos = endpos+1;
  endpos = str.find_first_of('.',startpos);
  tmpstr = str.substr(startpos,endpos);
  label.spot = atoi(tmpstr.c_str());  

  return true;
}


bool MapPrior::getEl(int index, MapElement & el)
{
  if (index <0 || index > (int)data.size()){
    cerr << "in MapPrior::getEl(), invalid index " << index << endl;
    return false;
  }
  el = data[index];
  el.center.x = el.center.x - delta.x;
  el.center.y = el.center.y - delta.y;
  
  for (int i = 0; i < (int) el.geometry.size(); ++i){
    el.geometry[i].x = el.geometry[i].x - delta.x;
    el.geometry[i].y = el.geometry[i].y - delta.y;
  }
  return true;

}


/** 
 * Gets the internal vector index of the lane given a lane label
 */
 int MapPrior::getLaneIndex(const LaneLabel &label)
{
  return (getLaneIndex(label.segment,label.lane));
}
 int MapPrior::getLaneIndex(const PointLabel &label)
{
  return (getLaneIndex(label.segment,label.lane));
}
 int MapPrior::getLaneIndex(const int segnum, const int lanenum)
{
  unsigned int i;

  for (i=0; i<lanes.size();++i){
    if (segnum == lanes[i].label.segment &&
        lanenum ==lanes[i].label.lane){
      
      return i;
    }
  } 
  // cerr << "Error in MapPrior::getLaneIndex, Segment " << segnum <<
  //  " lane " << lanenum << " not found" <<endl;
  return -1;
}


 int MapPrior::getLanePointIndex(const PointLabel &label)
{
  return (getLanePointIndex(label.segment,label.lane,label.point));
}
 int MapPrior::getLanePointIndex(const int segnum, const int lanenum, const int ptnum)
{
  unsigned int i;

  int index = getLaneIndex(segnum,lanenum);
 if (index<0)
   return false;

  PointLabel thislabel; 

  for (i=0; i<lanes[index].waypoint_label.size();++i){
    thislabel = lanes[index].waypoint_label[i];
    if (segnum == thislabel.segment &&
        lanenum ==thislabel.lane &&
        ptnum == thislabel.point){
      
      return i;
    }
  } 
  //cerr << "Error in MapPrior::getLanePointIndex, Segment " << segnum <<
  //  " lane " << lanenum << " point " << ptnum <<" not found" <<endl;
  return -1;
}


/** 
 *
 */
  bool MapPrior::isEntryPoint(const PointLabel &ptlabelin)
{
  int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].entry_label.size();++i){
    if (ptlabelin==lanes[index].entry_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */

  bool MapPrior::isExitPoint(const PointLabel &ptlabelin)
{
int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].exit_label.size();++i){
    if (ptlabelin==lanes[index].exit_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */

  bool MapPrior::isStopLine(const PointLabel &ptlabelin)
{
int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].stop_label.size();++i){
    if (ptlabelin==lanes[index].stop_label[i]){
      return true;
    }
  }

  return false;
}

/** 
 *
 */

  bool MapPrior::isCheckPoint(const PointLabel &ptlabelin)
{
int index = getLaneIndex(ptlabelin);
  if (index<0)
    return false;

  unsigned int i;

  for (i=0;i<lanes[index].checkpt_label.size();++i){
    if (ptlabelin==lanes[index].checkpt_label[i]){
      return true;
    }
  }

  return false;
}





/** 
 *
 */
  int MapPrior::getLaneLeftBound(point2arr_uncertain &bound, 
                       const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;
  bound = data[elindex].geometry;

  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneRightBound(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].rightbound_elindex;
  bound = data[elindex].geometry;

  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneBounds(point2arr_uncertain &leftbound, 
                    point2arr_uncertain &rightbound, 
                    const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int elindex = lanes[index].leftbound_elindex;
  leftbound = data[elindex].geometry;
  elindex = lanes[index].rightbound_elindex;
  rightbound = data[elindex].geometry;

  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneWayPoints(point2arr_uncertain &waypoints, 
                       const LaneLabel &lanelabelin)
{
  int index = getLaneIndex(lanelabelin);
  if (index<0)
    return -1;
  int wpindex = lanes[index].waypoint_elindex;
  waypoints = data[wpindex].geometry;


  return 0;

}

/** 
 *
 */
  int MapPrior::getPoint(point2_uncertain &pt, 
               const PointLabel &ptlabelin)
{
  int index = getLaneIndex(ptlabelin);

  int ptindex = getLanePointIndex(ptlabelin);
  if (ptindex<0 || index<0){
     cerr << "in MapPrior::getPoint(), point " << ptlabelin << " not found "<< endl;
    return -1;  
  }

  //  unsigned int i;
  
  point2arr_uncertain waypts;
  waypts = data[index].geometry;
  pt = waypts[ptindex];

  return 0;
}


/** 
 *
 */
  int MapPrior::getPointArr(point2arr_uncertain &ptarr, 
                  const vector<PointLabel> &ptlabelarrin)
{
  ptarr.clear();
  point2_uncertain pt;
  int retval;
  for (unsigned int i=0; i< ptlabelarrin.size();++i){
    retval = getPoint(pt,ptlabelarrin[i]);
    if (retval>=0){
      ptarr.push_back(pt);
    }else{
      ptarr.clear();
      cerr << "in MapPrior::getPointArr(), point " << ptlabelarrin[i] << " not found "<< endl;
      return retval;
    }
  }
  return 0;
}



  //intersection queries
/** 
 *
 */
  int MapPrior::getPointEntryLabels(vector<PointLabel>& ptlabelarr,
                          const PointLabel &ptlabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getPointExitLabels(vector<PointLabel>& ptlabelarr,
                         const PointLabel &ptlabelin)
{
  return 0;
}



/** 
 *
 */
  int MapPrior::getLaneExitLabels(vector<PointLabel>& ptlabelarr, 
                        const LaneLabel &lanelabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneExitToLabels(vector<PointLabel>& ptlabelarr, 
                          const LaneLabel &lanelabelin)
{
  return 0;
}


/** 
 *
 */
  int MapPrior::getLaneEntryLabels(vector<PointLabel>& ptlabelarr,
                         const LaneLabel &lanelabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneEntryFromLabels(vector<PointLabel>& ptlabelarr,
                             const LaneLabel &lanelabelin)
{
  return 0;
}


/** 
 *
 */
  int MapPrior::getLaneStopLineLabels(vector<PointLabel>& ptlabelarr,
                            const LaneLabel &lanelabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneCheckPointLabels(vector<PointLabel>& ptlabelarr,
                              const LaneLabel &lanelabelin)
{
  return 0;
}


/** 
 *
 */
  int MapPrior::getLaneToLeft(LaneLabel& lanelabel, 
                    const LaneLabel &lanelabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneToRight(LaneLabel& lanelabel, 
                     const LaneLabel &lanelabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getLaneName(string &name, 
                  const LaneLabel &lanelabelin)
{
  return 0;
}



/** 
 *
 */
  int MapPrior::getSegmentLanes(vector<LaneLabel>& lanelabelarr,
                      const int seglabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getSegmentLanesSameDirection(vector<LaneLabel>& lanelabelarr,
                                   const int seglabelin)
{
  return 0;
}

/** 
 *
 */
  int MapPrior::getSegmentLanesOtherDirection(vector<LaneLabel>& lanelabelarr,
                                    const int seglabelin)
{
  return 0;
}


/** 
 *
 */
  int MapPrior::getSegmentName(string &name, const int seglabelin)
{
  return 0;
}


/** 
 *
 */
  int MapPrior::getSegmentEntryFromLabels(vector<PointLabel>& lanelabelarr,
                                const int seglabelin)
{
  return 0;
}

  
