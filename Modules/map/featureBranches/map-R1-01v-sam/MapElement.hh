/**********************************************************
 **
 **  MAPELEMENT.HH
 **
 **    Time-stamp: <2007-05-08 15:00:32 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 12:44:48 2007
 **
 **
 **********************************************************
 **
 **  A class to describe a geometric map element
 **
 **********************************************************/


#ifndef MAPELEMENT_HH
#define MAPELEMENT_HH

#include <vector>
#include <string>
#include <iostream>
#include "interfaces/VehicleState.h"
#include "frames/point2_uncertain.hh"
#include "MapId.hh"

using namespace std;

enum MapElementGeometryType {
  GEOMETRY_UNDEF,  
  GEOMETRY_POINTS,
  GEOMETRY_ORDERED_POINTS,
  GEOMETRY_LINE,
  GEOMETRY_EDGE,
  GEOMETRY_POLY
};
enum MapElementCenterType {
  CENTER_UNDEF,  
  CENTER_POINT,
  CENTER_POSE,
  CENTER_BOUND_RADIUS,
  CENTER_BOUND_BOX,
  CENTER_BOUND_LINES
};
    
enum MapElementFrameType {
  FRAME_UNDEF,
  FRAME_LOCAL,
  FRAME_GLOBAL,
  FRAME_VEHICLE
};
enum MapElementColorType {
  MAP_COLOR_GREY,
  MAP_COLOR_RED,
  MAP_COLOR_BLUE,
  MAP_COLOR_GREEN,
  MAP_COLOR_CYAN,
  MAP_COLOR_YELLOW,
  MAP_COLOR_MAGENTA
};

enum MapElementType {
  ELEMENT_UNDEF,
  ELEMENT_CLEAR,
  ELEMENT_ALICE,

  ELEMENT_POINTS,
  ELEMENT_WAYPOINT,
  ELEMENT_CHECKPOINT,

  ELEMENT_LINE,
  ELEMENT_STOPLINE,
  ELEMENT_LANELINE,

  ELEMENT_PARKING_SPOT,
  ELEMENT_PERIMETER,

  ELEMENT_OBSTACLE,  
  ELEMENT_OBSTACLE_EDGE,
  ELEMENT_VEHICLE
};



// A class to describe a geometric map element
class MapElement
{
  
public:
  
  // A Constructor 
  MapElement();
  
  // A Destructor 
  ~MapElement() {}
  
  // Subgroup id. only changed and used by talker;
  int subgroup;

  // Object id
  //  vector<int> id; 
  MapId id;

  // Confidence that the object exists 
  double conf;

  // Object classification
  MapElementType type;
  // TODO: Need type list and enumeration
  
  // Type classification confidence bounds
  double type_conf;
  

  MapElementCenterType center_type;
  // Estimated object center position and covariance
  point2_uncertain center;
  //
  // point2_uncertain struct members (defined in frames/point2_uncertain.hh)
  //
  // center.x                x position
  // center.y                y position
  // center.max_var        max variance
  // center.min_var        min variance
  // center.axis             angle of max variance
  
  
  // Estimated bounding box parameter
  double length;  
  double width;
  double orientation;
  // Not meaningful for all objects, but can be useful
  

  // bounding box parameter variance
  double length_var;
  double width_var;
  double orientation_var;

  MapElementGeometryType geometry_type;
  // Estimated object geometry 
  point2arr_uncertain geometry;
  //vector<point2_uncertain> geometry;
  // geometry represents object contour
  // -list of ordered connected points for a line 
  // -list of ordered vertices for an obstacle
  // If the geometry is not specified, bounding box info only is used


  // Estimated object height
  double height;
  // height for road markings should be zero

  // Object height variance
  double height_var;
  

  // Object velocity
  point2_uncertain velocity;
  // The velocity x,y coordinates define a velocity vector
  // for static map elements x,y should be 0 

    
  // Frame of reference
  MapElementFrameType frame_type;

  // All map element coordinates should be given in local frame, 
  // not GPS global or vehicle frame.
  // Shouldn't need this eventually.
  // TODO: define frame enumeration

  /// Vehicle state data when element was detected
  VehicleState state;


MapElementColorType plot_color;
  int plot_value;

  // Text label
  // Not critical but could be useful initially for debugging
  vector<string> label;

  
  void clear();

  void set_id(MapId& id_in) {id = id_in;}
  void set_id(vector<int>& id_in) {id = id_in;}
  void set_id(int id_in) {id = id_in;}
  void set_id(int id1_in,int id2_in) {id.set(id1_in,id2_in);}
  void set_id(int id1_in,int id2_in,int id3_in) {id.set(id1_in,id2_in,id3_in);}
  void set_id(int id1_in,int id2_in,int id3_in, int id4_in) {id.set( id1_in,id2_in,id3_in,id4_in);}

  
  void set_geometry(point2arr &ptarr);
  void set_geometry(point2 &pt);
  void set_geometry(point2arr_uncertain &ptarr);
  void set_geometry(vector<point2> &ptarr);
  void set_geometry(vector<point2_uncertain> &ptarr);
  void set_geometry(vector<double> &xarr, vector<double> &yarr);
  void set_geometry(point2 &pt, double radius);

  void set_center(point2 cpt);
  void set_center(point2_uncertain cpt);
  void set_center(double x, double y);

  void set_frame_local();
  void set_frame_global();

  void set_clear(vector<int>& ident);
  void set_alice(VehicleState &statein);
  void set_circle_obs(vector<int>& ident, point2 cpt, double radius);
  void set_circle_obs();
  void set_poly_obs(vector<int>& ident, vector<point2>& ptarr);
  void set_block_obs(vector<int>& ident, point2 cpt, double ang, double len, double wid );
  void set_line(vector<int>& ident,  vector<point2>& ptarr);
  void set_line();
  void set_stopline(vector<int>& ident, vector<point2>& ptarr);
  void set_laneline(vector<int>& ident, vector<point2>& ptarr);
  void set_laneline();
 
 void set_points(vector<int>& ident, vector<point2>& ptarr);
 void set_points();
  void print() const ;
void print_state() const ;
  void set_center_from_geometry();
};
#endif
