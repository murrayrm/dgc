              Release Notes for "map" module

Release R1-01w (Thu May 10 13:52:11 2007):
	Added StateTalker, a small program to send alice info to the mapviewer. Right now it only sends alice's location and traversed path, but eventually it will send actuator state, such as steering commands. Also added a bunch of comments and cleaned up MapElement a bit.

Release R1-01v (Mon Apr 30 12:25:45 2007):
	Updated MapElementTalker to take advantage of new skynet
	functionality to send messages on a sub channel.  Implemented
	variable length skynet messages.  For modules which send map
	elements, no code change is needed, just a recompile against the
	updated map and frames modules.  For modules which receive map
	elements, add the intended subnet group to the initRecvMapElement
	function as follows : initRecvMapElement(skynetKey,recvChannel);
	Without this, change only elements on channel 0 will be received.  
	  Added map interface functions getLaneDistToWaypoint which
	calculate distance along corridor between two points.  Updated
	getBounds, getBoundsReverse and getTransitionBounds to return equal
	sized left and right bounds.

Release R1-01u (Sun Apr 29  0:12:00 2007):
	Fixing MapElement::print_state(), broken by the recent change to
	the VehicleState structure.

Release R1-01t (Wed Apr 25 17:19:24 2007):
	added getLaneID, getSegmentID, checkLaneID functions to tplanner
	interface with map.

Release R1-01s (Fri Mar 30 20:12:18 2007):
	test functions for ellipse class

Release R1-01r (Fri Mar 30  2:16:51 2007):
	<* Pleaase insert a release notes entry here. If none, then please delete these lines *>

Release R1-01q (Sat Mar 17 11:21:09 2007):
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

Release R1-01p (Sat Mar 17  0:47:19 2007):
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

Release R1-01o (Fri Mar 16  0:27:27 2007):
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

Release R1-01n (Thu Mar 15 13:59:49 2007):
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

Release R1-01m (Wed Mar 14 15:57:56 2007):
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

Release R1-01l (Wed Mar 14 10:14:55 2007):
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount. Added more geometric test functions

Release R1-01k (Tue Mar 13 15:45:32 2007):
		Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

Release R1-01j (Mon Mar 12 15:22:34 2007):
	added getNextStopline function.  Need to debug an RNDF mirroring
	issue but this should work for short term planner testing.
	
Release R1-01i (Sun Mar 11 11:49:20 2007):
	Added clear and state messages to map elements. added test programs testMapElement, and testMapId.

Release R1-01h (Sat Mar 10 17:22:19 2007):
	Fixed bug in getStopline.

Release R1-01g (Fri Mar  9 16:33:35 2007):
	added isStop and isExit query functions to map.

Release R1-01f (Fri Mar  9 12:15:10 2007):
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

Release R1-01e (Thu Mar  8 18:33:06 2007):
	Updated some map functions for use in new tplanner (Sam).

Release R1-01d (Sat Mar  3 15:55:33 2007):
	Updated getStopline function to take only a point in space not a
	stopline label.

Release R1-01c (Thu Mar  1 19:34:02 2007):
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results.	Implemented the getLeftBound and getRightBound functions.

Release R1-01b (Tue Feb 27  8:39:07 2007):
	Complete overhaul of map module.  Moved new map structure from mapper into map module.  Map now parses RNDF files with an internal method and has a very different internal structure.  Still need to implement query functions. 

Release R1-01a (Sun Feb 25 12:56:28 2007):
	Updated to work with new SkynetContainer and interfaces paths

Release R1-00b (Thu Feb  1 19:27:02 2007):
	Fixed links in Makefile.yam and updated Map object for short
	term use in tplanner.

Release R1-00a (Sat Jan 27 18:25:30 2007):
	Added Map class and LocalMapTalker.

Release R1-00 (Sat Jan 27 18:14:18 2007):
	Created.


























