/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-04-28 11:42:13 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "MapElement.hh"


class Map
{
  
public:
  
  Map();
  
  ~Map();

  bool loadRNDF(string filename) {return prior.loadRNDF(filename);}

  vector<MapElement> data;
  MapPrior prior;

  int addEl(const MapElement &el);
  void initLaneLines();

  bool checkLaneID(point2 pt, int segment, int lane);

  int getSegmentID(point2 pt);
  int getLaneID(point2 pt);
  int getZoneID(point2 pt);
  int getNextPointID(PointLabel &label,const point2& pt);
  int getLastPointID(PointLabel &label,const point2& pt);
  int getClosestPointID(PointLabel &label, const point2& pt);

  int getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
 int getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);

  PointLabel getNextPointID(const point2& pt);
  PointLabel getClosestPointID(const point2& pt);

  
  int getLeftBound(point2arr& ptarr, LaneLabel &label );
  int getRightBound(point2arr& ptarr, LaneLabel &label );

  int getLeftBoundType(string& type, LaneLabel &label);
  int getRightBoundType(string& type, LaneLabel &label);

  int getStopline(point2& pt, PointLabel &label);
  int getNextStopline(point2& pt, PointLabel &label);
  int getStopline(point2& pt, point2 state);
  
  int getWaypoint(point2& pt, const PointLabel &label);

  // int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , PointLabel &exitlabel, PointLabel &enterlabel);

  //  int getRoadBounds(point2arr& leftbound, point2arr& rightbound, const LaneLabel &label,const point2 state, const double range, const double backrange = 10;);
  int getBounds(point2arr& leftbound, point2arr& rightbound, const LaneLabel &label,const point2 state, const double range , const double backrange=10);
 int getBoundsReverse(point2arr& leftbound, point2arr& rightbound, const LaneLabel &label,const point2 state, const double range, const double backrange=10);
  
  int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , const PointLabel &exitlabel, const PointLabel &enterlabel, const point2 pt, const double range, const double backrange=10);


  int getHeading(double & ang, const PointLabel &label);
 int getHeading(double & ang, const point2 &pt);


 

  bool isStop(PointLabel &label);
  bool isExit(PointLabel &label);

  int getWaypointArr(point2arr& ptarr, const PointLabel &label);
  int getLaneDistToWaypoint(double& dist,const point2 state,const PointLabel &label);
  int getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label);

 double getObstacleDist(const point2& state,const int lanedelta=0);
  point2 getObstaclePoint(const point2& state, const double offset=0);
  int getObstacleGeometry(point2arr& ptarr);

};



#endif
 
