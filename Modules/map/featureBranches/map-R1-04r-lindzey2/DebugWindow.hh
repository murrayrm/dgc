#ifndef __DEBUG_WINDOW_HH__
#define __DEBUG_WINDOW_HH__

#include <vector>
#include <string>
#include <sstream>

#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>
#include "Map.hh"

// Reduced version of the MapWindow found in MapViewer, just for debugging

class DebugWindow : public Fl_Gl_Window
{
public:
    DebugWindow(int X,int Y,int W,int H,const char*L=0);
    
    // overridden Fl_Gl_Window methods
    void draw();
    void reshape();

    // utilities

    void DrawGrid();
    void DrawEllipse(point2_uncertain pt);
    void DrawText(string txt, double x, double y , double width);
    void DrawElement(const MapElement& el, bool isSel = false);

    void DrawVehicle(const VehicleState &state);

    void DrawElements();
    void DrawShape();

    void DrawDebugData();

    bool isElementVisible(const MapElement & el);
    void select_element(double x, double y);

    void set_color(MapElementColorType color, int value =100);

    double scale_to_mult(double s);
    
    int set_view_screen_delta(double dx, double dy);

    int set_view(double x, double y);

    int set_scale_delta(double ds, double cx, double cy);

    int set_scale(double s);
  
    void reset_map();

    int handle(int event) ;
    string id_to_string(const MapId& id);

    void setMap(Map* map) { this->map = map; }
    Map* getMap() { return map; }
    const Map* getMap() const { return map; }

    void updateState(const VehicleState& state);

private:

    Map* map;
    
    double mouse_local_x;
    double mouse_local_y;
    double mouse_x;
    double mouse_y;
    double mouse_x_press;
    double mouse_y_press;
    double mouse_button;
    double center_x;
    double center_y;
    double scale;
  
    int elrate;  

    int selected;
    int selectedRndf;
    std::vector<int> selectedarr;
    int selectedarr_index;
    point2arr selectedptarr;
    point2 selectedpt;

    
    int ymult;
    bool centerOnAlice;
    bool xAxisRight;

    // used to represent the most recent element drawn in the map which has state info
    VehicleState latestState;
    point2 stateDelta; // uset when following alice
    bool drawStateFlag;

    int debugLevel;
    bool showIDs; //whether to print the mapelement IDs on the display
    bool showUncertainty;
    bool showTrav; //whether or not to show the trav path
    bool showTimeStopped;
};

template<class T>
inline std::string to_string(const T& x)
{
    std::ostringstream o;
    o << x;
    return o.str();
}


#endif
