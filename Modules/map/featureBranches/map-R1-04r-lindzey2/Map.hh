/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-10-07 15:33:29 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "MapElement.hh"
#include "frames/pose2.hh"

#define MAXNUMOBJECTS 500
#define MAXNUMPERCEPTOROBJECTS 500

//important to have ladars first, then stereo, then radar
enum {
  LF_LADAR,
  MF_LADAR,
  RF_LADAR,
  REAR_LADAR,
  LF_ROOF_LADAR,
  LADAR_OBS,
  RIEGL,
  LONG_STEREO_OBS,
  MEDIUM_STEREO_OBS,
  REAR_STEREO_OBS,
  MF_RADAR,
  FUSED_FROM_MAPPER,
  PRIOR_FROM_MAPPER,
  PREDICTION,
  NUM_PERCEPTORS
};

//used to store data from one perceptor
struct perceptorData {
  int timesSeen;
  unsigned long long lastSeen;
  point2arr geometry;
  point2 velocity;
  MapElementType type;
  double timeStopped;
};


//used to store information about the objects
//tracked by map
struct objectData {
  int id;

  MapElement mergedMapElement;

  unsigned long long lastFused;
  unsigned long long lastAssoc;

  int trackAlive[NUM_PERCEPTORS];
  perceptorData inputData[NUM_PERCEPTORS];

};



class Map
{
  
public:
  
  /// Default constructor.
  /// \param lineFusion enable internal line fusion at all
  /// \param lineFusionOnAddEl fuse lane lines as they are added with addEl (old behavior).
  /// Set this to false if you call fuseAllLines() in the main loop instead.
  Map(bool lineFusion = true, bool lineFusOnAddEl = false);
  
  ~Map();

  /// core representation of sensed map data
  vector<MapElement> data;

  /// most recent free-space estimate 
  point2arr freespace;


  /// new data structure to store all sensed data
  objectData newData[MAXNUMOBJECTS];
  vector<int> usedIndices;
  vector<int> openIndices;
  int IDtoINDEX[NUM_PERCEPTORS][MAXNUMPERCEPTOROBJECTS];
  int numMapObjects; //always incrementing # (how many objs map has dealt w/)

  // initializes the indices arrays, and the ID-element lookup table
  void init();

  /// representation of prior map information derived from the RNDF
  MapPrior prior;

  /// loc2rndf holds the offset between the local frame and the RNDF frame as 
  /// sent by fused-perceptor
  point2_uncertain loc2rndf;


  /// loc2rndf holds the offset between the local frame and the site frame
  pose2 loc2site;
 
  bool lineFusion;
  uint64_t lastStateTimestamp;

  // debug stuff, if not NULL it will be used
  LineFusionDebugData* lineFusDebug;



  /// Efficiently copy map data returned by queries
  /// only.  Won't copy fusion support data
  void copyMapData(Map *targetmap);


  //--------------------------------------------------
  // Map queries
  //--------------------------------------------------

  /// Checks if a point label is an entry point
  bool isEntryPoint(const PointLabel &ptlabelin)
  {return prior.isEntryPoint(ptlabelin);}
  /// Checks if a point label is an exit point
  bool isExitPoint(const PointLabel &ptlabelin)
  {return prior.isExitPoint(ptlabelin);}
  /// Checks if a point label is a stop line
  bool isStopLine(const PointLabel &ptlabelin)
  {return prior.isStopLine(ptlabelin);}
  /// Checks if a point label is a check point
  bool isCheckPoint(const PointLabel &ptlabelin)
  {return prior.isCheckPoint(ptlabelin);}
  /// Checks if the two given lanes run the same direction
  bool isLaneSameDir(const LaneLabel &lanelabel1,const LaneLabel &lanelabel2)
  {return prior.isLaneSameDir(lanelabel1,lanelabel2);}
  /// Checks if a point is geometrically in the lane
  bool isPointInLane(const point2_uncertain &pt, const LaneLabel &lanelabelin)
  {return prior.isPointInLane(pt,lanelabelin);}  
  bool isPointInLane(const point2 &pt, const LaneLabel &lanelabelin)
  {return prior.isPointInLane(pt,lanelabelin);}  
  
  /// Checks if a map element is geometrically in the lane
  bool isElementInLane(const MapElement &el, const LaneLabel &lanelabelin)
  {return prior.isElementInLane(el,lanelabelin);}  

  /// Returns the lanes which have any overlap with the given map element
  int getLanesInElement(vector<LaneLabel> labelarr, const MapElement &el)
  {return prior.getLanesInElement(labelarr,el);}  
  

  /// Given a point, gets the best guess of what lane you're in.  Not
  /// guaranteed to return the correct lane especially in
  /// intersections.  Will return lane which has the shortest
  /// projected distance to that lane's centerline.
  int getLane(LaneLabel & lanelabel, const point2& pt);
  //int getLane(LaneLabel & lanelabel, const point2& pt,double heading);


  //  int getLane(LaneLabel & lanelabel, const point2& pt, const double heading);
   

  /// Gets the left bound geometry of the given lane
  int getLaneLeftBound(point2arr_uncertain &bound, 
                       const LaneLabel &lanelabelin)
  { return prior.getLaneLeftBoundFull(bound,lanelabelin);}
  int getLaneLeftBound(point2arr &bound, 
                       const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneLeftBoundFull(tmppt,lanelabelin);
    bound.set(tmppt);
    return retval;
  }

  
  /// Gets the right bound geometry of the given lane
  int getLaneRightBound(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin)
  { return prior.getLaneRightBoundFull(bound,lanelabelin);}
  int getLaneRightBound(point2arr &bound, 
                        const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneRightBoundFull(tmppt,lanelabelin);
    bound.set(tmppt);
    return retval;
  }
  
  /// Gets both left and right bound geometry of the given lane
  int getLaneBounds(point2arr_uncertain &leftbound, 
                    point2arr_uncertain &rightbound, 
                    const LaneLabel &lanelabelin)
  { return prior.getLaneBoundsFull(leftbound,rightbound,lanelabelin);}
  int getLaneBounds(point2arr &leftbound, 
                    point2arr &rightbound, 
                    const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getLaneBoundsFull(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }
  

  /// Gets both left and right bound geometry of the given lane in a single polygon
  /// bounds is ordered clockwise starting at the first point of the left lane
  /// The return value is the size of the left lane
  int getLaneBoundsPoly(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin)
  { return prior.getLaneBoundsPolyFull(bound,lanelabelin);}
  int getLaneBoundsPoly(point2arr &bound, 
                        const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneBoundsPolyFull(tmppt,lanelabelin);
    bound.set(tmppt);
    return retval;
  }

  
  /// Gets the centerline of the given lane
  int getLaneCenterLine(point2arr_uncertain &centerline, 
                        const LaneLabel &lanelabelin)
  { return prior.getLaneCenterLineFull(centerline,lanelabelin);}
  int getLaneCenterLine(point2arr &centerline, 
                        const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneCenterLineFull(tmppt,lanelabelin);
    centerline.set(tmppt);
    return retval;
  }

  /// Gets the waypoints of the given lane
  int getLaneWayPoints(point2arr_uncertain &centerline, 
                       const LaneLabel &lanelabelin)
  { return prior.getLaneCenterLine(centerline,lanelabelin);}
  int getLaneWayPoints(point2arr &centerline, 
                       const LaneLabel &lanelabelin){
    point2arr_uncertain tmppt;
    int retval = prior.getLaneCenterLine(tmppt,lanelabelin);
    centerline.set(tmppt);
    return retval;
  }


  
  /// Gets the coordinates (local frame) of the given point label
  int getWayPoint(point2_uncertain &pt, 
                  const PointLabel &ptlabelin)
  {return prior.getWayPoint(pt,ptlabelin);}
  int getWayPoint(point2 &pt, 
                  const PointLabel &ptlabelin){
    point2_uncertain tmppt;
    int retval = prior.getWayPoint(tmppt,ptlabelin);
    pt.set(tmppt);
    return retval;
  }
  
  /// Gets the coordinate array (local frame) of the given point labels
  int getWayPoint(point2arr_uncertain &ptarr, 
                  const vector<PointLabel> &ptlabelarrin)
  {return prior.getWayPoint(ptarr,ptlabelarrin);}
  int getWayPoint(point2arr &ptarr, 
                  const vector<PointLabel> &ptlabelarrin){
    point2arr_uncertain tmppt;
    int retval = prior.getWayPoint(tmppt,ptlabelarrin);
    ptarr.set(tmppt);
    return retval;
  }
 
  /// Gets the coordinates (local frame) of the given point label
  int getStopLineSensed(point2_uncertain &pt, 
                        const PointLabel &ptlabelin);

  int getStopLineSensed(point2 &pt, 
                        const PointLabel &ptlabelin){
    point2_uncertain tmppt;
    int retval = getStopLineSensed(tmppt,ptlabelin);
    pt.set(tmppt);
    return retval;
  }

  /// Gets the left and right outer bounds of the lanes going in the same
  /// direction as the given lane label
  int getSameDirLaneBounds(point2arr_uncertain &leftbound, 
                           point2arr_uncertain &rightbound, 
                           const LaneLabel &lanelabelin)
  {return prior.getSameDirLaneBounds(leftbound, rightbound, lanelabelin);}
  int getSameDirLaneBounds(point2arr &leftbound, 
                           point2arr &rightbound, 
                           const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getSameDirLaneBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }


  /// Gets the left and right outer bounds of the lanes going in the opposite
  /// direction as the given lane label
  int getOppDirLaneBounds(point2arr_uncertain &leftbound, 
                          point2arr_uncertain &rightbound, 
                          const LaneLabel &lanelabelin)
  {return prior.getOppDirLaneBounds(leftbound, rightbound, lanelabelin);}
  int getOppDirLaneBounds(point2arr &leftbound, 
                          point2arr &rightbound, 
                          const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getOppDirLaneBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }

 
  /// Gets the left and right outer bounds of the full segment
  int getSegmentBounds(point2arr_uncertain &leftbound, 
                       point2arr_uncertain &rightbound, 
                       const LaneLabel &lanelabelin)
  {return prior.getSegmentBounds(leftbound, rightbound, lanelabelin);}
  int getSegmentBounds(point2arr &leftbound, 
                       point2arr &rightbound, 
                       const LaneLabel &lanelabelin){
    point2arr_uncertain lefttmppt,righttmppt;
    int retval = prior.getSegmentBounds(lefttmppt,righttmppt,lanelabelin);
    leftbound.set(lefttmppt); rightbound.set(righttmppt);
    return retval;
  }



  /// Gets the waypoint labels which from which traffic can enter the given waypoint 
  int getWayPointEntries(vector<PointLabel>& ptlabelarr,
                         const PointLabel &ptlabelin)
  {return prior.getWayPointEntries(ptlabelarr,ptlabelin);}
  /// Gets the waypoint labels which from which traffic can exit the given waypoint 
  int getWayPointExits(vector<PointLabel>& ptlabelarr,
                       const PointLabel &ptlabelin)
  {return prior.getWayPointExits(ptlabelarr,ptlabelin);}

  /// Gets the waypoint labels which from which traffic can exit the given lane  
  int getLaneExits(vector<PointLabel>& thislabelarr, 
                   vector<PointLabel>& otherlabelarr,
                   const LaneLabel &lanelabelin)
  {return prior.getLaneExits(thislabelarr,otherlabelarr,lanelabelin);}

  /// Gets the waypoint labels which from which traffic can enter the given lane 
  int getLaneEntries(vector<PointLabel>& thislabelarr,
                     vector<PointLabel>& otherlabelarr,
                     const LaneLabel &lanelabelin)
  {return prior.getLaneEntries(thislabelarr,otherlabelarr,lanelabelin);}

  /// Gets the waypoint labels which from which traffic can exit the given zone  
  int getZoneExits(vector<PointLabel>& thislabelarr, 
                   vector<PointLabel>& otherlabelarr,
                   const int zonelabelin)
  {return prior.getZoneExits(thislabelarr,otherlabelarr,zonelabelin);}

  /// Gets the waypoint labels which from which traffic can enter the given zone 
  int getZoneEntries(vector<PointLabel>& thislabelarr,
                     vector<PointLabel>& otherlabelarr,
                     const int zonelabelin)
  {return prior.getZoneEntries(thislabelarr,otherlabelarr,zonelabelin);}


  /// Gets the stopline labels in the given lane
  int getLaneStopLines(vector<PointLabel>& ptlabelarr,
                       const LaneLabel &lanelabelin)
  {return prior.getLaneStopLines(ptlabelarr,lanelabelin);}

  /// Gets the checkpoint labels in the given lane
  int getLaneCheckPoints(vector<PointLabel>& ptlabelarr,
                         const LaneLabel &lanelabelin)
  {return prior.getLaneCheckPoints(ptlabelarr,lanelabelin);}

  /// Gets the checkpoint numbers in the given lane
  int getLaneCheckPointNumbers(vector<int>& ptnumarr,
                               const LaneLabel &lanelabelin)
  {return prior.getLaneCheckPointNumbers(ptnumarr,lanelabelin);}

  /// Gets the name string of the given lane
  int getLaneName(string &name, 
                  const LaneLabel &lanelabelin)
  {return prior.getLaneName(name,lanelabelin);}


  /// Returns neighboring lane label offset from the given lane.  
  ///
  /// A negative value of the offset parameter will return a lane
  ///lable on the left with respect to nominal driving direction of
  ///the given lane.  A positive value of offset will return a lane to
  ///the right.  An offset of 0 will return the current lane.
  int getNeighborLane(LaneLabel& lanelabel, 
                      const LaneLabel &lanelabelin,
                      const int offset)
  {return prior.getNeighborLane(lanelabel,lanelabelin,offset);}

  /// Gets the labels of lanes which are in the same direction and 
  /// same segment as the given lane 
  int getSameDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
  {return prior.getSameDirLanes(lanelabelarr,lanelabelin);}

  /// Gets the labels of lanes which are in the opposite direction and 
  /// same segment as the given lane 
  int getOppDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
  {return prior.getOppDirLanes(lanelabelarr,lanelabelin);}

  /// Gets the labels of lanes which are in the same segment 
  /// as the given lane and have any direction
  int getAllDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin)
  {return prior.getAllDirLanes(lanelabelarr,lanelabelin);}

  /// Gets the name of the segment
  int getSegmentName(string &name, const int seglabelin)
  {return prior.getSegmentName(name,seglabelin);}

  /// Gets the number of lanes in the segment which contains the given point
  int getSegmentNumLanes(const PointLabel & pointlabelin)
  {return prior.getSegmentNumLanes(pointlabelin);}
  /// Gets the number of lanes in the segment which contains the given lane
  int getSegmentNumLanes(const LaneLabel & lanelabelin)
  {return prior.getSegmentNumLanes(lanelabelin);}
  /// Gets the number of lanes in the given segment
  int getSegmentNumLanes(const int seglabelin) 
  {return prior.getSegmentNumLanes(seglabelin);}


  /// Gets the point along the given line which is offset from the 
  /// start of the line by the given distance.  The distance is
  /// measured along the path of the line
  int getPointAlongLine(point2 &ptout, const point2arr & line, const double dist);
  int getPointAlongLine(point2_uncertain &ptout, const point2arr_uncertain & line, const double dist){ 
    point2arr tmpline(line); point2 tmpptout;
    int retval= getPointAlongLine(tmpptout,tmpline,dist);
    ptout.set(tmpptout);
    return retval;
  }

  /// Returns the index of the point which is closest to the given
  /// distance along the line from the start point.  If dist is
  /// negative, the first index will be returned.  If dist is longer
  /// than the line length, the last index will be returned.
  int getIndexAlongLine(int &index, const point2arr & line, const double dist);
  int getIndexAlongLine(int &index, const point2arr_uncertain & line, const double dist){ 
    point2arr tmpline(line);
    return getIndexAlongLine(index,tmpline,dist);
  }

  /// Given a line and a point, gets the distance along the line to 
  /// the point's projection into the line.  The distance is
  /// measured along the path of the line
  int getDistAlongLine(double &dist, const point2arr & line, const point2& pt);
  int getDistAlongLine(double &dist, const point2arr_uncertain & line, const point2_uncertain& pt){ 
    point2arr tmpline; point2 tmppt;
    return getDistAlongLine(dist,tmpline,tmppt);
  }
 
  /// Returns the distance along the given line between the two given
  /// points.  If pta is further along the line than ptb the resulting
  /// dist will be positive.  Otherwise it will be negative
  int getDistAlongLine(double &dist, const point2arr & line, const point2& pta, const point2& ptb);
  int getDistAlongLine(double &dist, const point2arr_uncertain & line, const point2_uncertain& pta, const point2_uncertain& ptb){ 
    point2arr tmpline; point2 tmppta, tmpptb;
    return getDistAlongLine(dist,tmpline,tmppta,tmpptb);
  } 
 
  /// Given a line and a point, returns the projection of the point
  /// along the line
  int getProjectToLine(point2 &projpt, const point2arr & line, const point2& pt);
  int getProjectToLine(point2_uncertain &projpt, const point2arr_uncertain & line, const point2_uncertain& pt){ 
    point2arr tmpline; point2 tmppt, tmpprojpt;
    int retval = getProjectToLine(tmpprojpt,tmpline,tmppt);
    projpt.set(tmpprojpt);
    return (retval);
  }

  /// Extend a line by a given amount
  int extendLine(point2arr &lineout, const point2arr & line, const double dist);
  int extendLine(point2arr_uncertain & lineout, const point2arr_uncertain & line, const double dist){
    point2arr tmpline(line), tmplineout;
    int retval=extendLine(tmplineout,tmpline,dist);
    lineout.set(tmplineout);
    return retval;
  }


  /// Get all obstacles within a given distance from the given point
  int getObsNearby(vector<MapElement> &elarr,const point2 &pt, const double dist);
  int getObsNearby(vector<MapElement> &elarr,const point2_uncertain &pt, const double dist){
    point2 tmppt(pt);
    return getObsNearby(elarr,tmppt,dist);
  }

  /// Get all obstacles in the given lane bounds
  int getObsInLane(vector<MapElement> &elarr,const LaneLabel &lanelabelin);

  /// Get all obstacles in the given lane bounds
  int getObsInZone(vector<MapElement> &elarr,int zonelabelin);


  /// Get all obstacles in between the given bounding lines
  int getObsInBounds(vector<MapElement> &elarr, const point2arr &leftbound, const point2arr &rightbound);
  int getObsInBounds(vector<MapElement> &elarr, const point2arr_uncertain &leftbound, const point2arr_uncertain &rightbound){
    point2arr tmpleft(leftbound),tmpright(rightbound);
    return getObsInBounds(elarr,tmpleft,tmpright);
    
  }

  /// Return the union of the given polygon and free space
  int getFreeSpaceInBounds(vector<point2arr> &freespaceOverlap,const point2arr &poly);


  /// Get all obstacles in the given polygon
  int getObsInPoly(vector<MapElement> &elarr, const point2arr &poly);





  /// Loads the RNDF from a file
  bool loadRNDF(string filename);



 
  ///Adds an element to the map. If the id exists then that element is
  /// updated.  If the element has type ELEMENT_CLEAR then the element
  /// with that ID (if it exists) is removed
  int addEl(const MapElement &el, bool fusionDisabled = false);
 
  /// Enable line fusion when adding a LANELINE MapElement to the map
  /// via addEl(). You can still call fuseAllLines() to do the fusion
  /// at a later time.
  void enableLineFusion(bool enable = true) { lineFusion = enable; }

  /// Disables line fusion, equivalent to enableLineFusion(false).
  void disableLineFusion() { enableLineFusion(false); }

  /// Returns true if line fusion is enabled. This only affects addEl(),
  /// if you call one of the fuse* methods, fusion will happen anyway.
  bool lineFusionEnabled() { return lineFusion; }

  /// Fuse sensed lane line with prior data (use prior.fulldata for this, so
  /// the original data in prior.data is still available).
  /// Returns true if the line was fused, false otherwise.
  bool fuseLaneLine(const MapElement& laneEl);

  /// Fuse sensed lane line with prior data from the given segment and lane
  /// (in the format returned by getClosestSegment()).
  /// Returns true if the line was fused, false otherwise.
  bool fuseLaneLine(const MapElement& inputEl, int currSeg, int currLane, int currPoint);

  /// Given the sensed element representing a curb (right side only),
  /// update the lane lines accordingly (all of them must be on the left
  /// of the curb.
  void fuseCurb(const MapElement& curbEl);

  /// Fuse the curb with the given segment and lane. Faster than the
  /// other version, that has to guess the current segment and lane
  /// from the state in the MapElement.
  void fuseCurb(const MapElement& inputEl, int currSeg, int currLane, int currPoint);

  /// Fuse all the sensed lane lines currently in the map with the prior
  /// (updates prior.fulldata, keeps the original rndf data in prior.data).
  void fuseAllLines();

  /// Fuse all the sensed lane lines currently in the map with the prior,
  /// using the given state as the current state (used to find the closest
  /// segment and lane). Faster than fuseAllLines() without parameters.
  void fuseAllLines(const VehicleState& state);

private:

  int lastSeg, lastLane, lastPoint;
  // internal line fusion functions

  /// Fuse sensed lane line with the given MapElement. The prior data
  /// in priorEl will be updated in place. isUpd will contain a bool
  /// for each point in the fused lane, true if the point was updated
  /// and false if not.
  /// begin and and specify the range of points in priorEl to be associated
  /// with laneEl, with priorEl.geometry[begin] being associated with
  /// the first point in laneEl, and priorEl.geometry[end] with the last point.
  void fuseLaneLineInternal(MapElement& priorEl, const MapElement& laneEl,
                    int begin, int end, vector<bool>& isUpd);

  /// Using the given state, determines the closest segment and lane,
  /// and stores it into currSeg, currLane and currPoint. Used by line
  /// fusion. currSeg and currLane will be indexes into the prior.segments
  /// and prior.segments[currSeg].laneindex vectors, respectively, and
  /// currPoint will be an index into the geometry of the lane center
  /// MapElement retrieved with getElFull().
  void getClosestSegment(const VehicleState& state,
                         int* currSeg, int* currLane = NULL,
                         int* currPoint = NULL);

  /// Do any post-processing after updating the prior with new sensed
  /// data (smoothing, constraint enforcement).
  /// seg and lane are in the format returned by getClosestSegment.
  void postProcessLane(int seg, int lane, const vector<bool>& isUpd)
  {
      prior.postProcessLane(seg, lane, isUpd);
  }

  void postProcessSegment(int seg)
  {
      prior.postProcessSegment(seg);
  }

public:


  
  /// Gets an element from the map by index in the local frame or site frame if the loc2site transform is non-zero
  void getFusedEl(MapElement &el, const unsigned int index) {
    el= newData[usedIndices.at(index)].mergedMapElement;
    transformElLocalToSite(el);
  }
  
    
  /// Gets an element from the map by index in the local frame, or site frame if the transform is non-zero
  void getEl(MapElement &el, const unsigned int index) 
  {el= data[index]; transformElLocalToSite(el); }
  
  




  /// Transforms a given element from local to RNDF frame
  void transformElLocalToSite(MapElement &el)
  {
    if (!el.isFrameTypeSite()){
      el.translate(loc2site.x,loc2site.y);     
      el.rotate(-loc2site.ang);
      el.setFrameTypeSite();
    }
  }

  

  void setLocalToGlobalOffset( const VehicleState &state)
  {setLocalToSiteOffset(state);}

/// sets the local to global frame offset
  void setLocalToGlobalOffset( const point2_uncertain &pt)
  {prior.delta =pt;}
  void setLocalToGlobalOffset( const point2 &pt)
  {prior.delta =pt;}

  point2_uncertain getLocalToGlobalOffset()
  {return prior.delta;}

  double getLocalToGlobalYawOffset()
  {return prior.deltaYaw;}

  void setLocalToGlobalYawOffset( const double ang)
  {prior.deltaYaw =ang;}

  int setLocalToSiteOffset( const VehicleState &state)
  {
     if (state.timestamp<=0)
    return -1;

     loc2site = pose2(state.siteNorthing-state.localX, state.siteEasting-state.localY,acos(cos(state.siteYaw-state.localYaw)));
     
 
   return  (prior.setSiteToGlobalOffset(state));
    
  }

  void clearLocalToSiteOffset()
  {
    loc2site.x=0; loc2site.y=0;loc2site.ang=0;
  }




 





  
  //--------------------------------------------------
  //  These functions need to be updated and the interface
  // may change slightly
  //--------------------------------------------------

 

  /// Gets the heading angle of the lane at the given point
  int getHeading(double & ang, const PointLabel &label);
  /// Gets the heading angle of the lane at the given point
  int getHeading(double & ang, const point2 &pt);
  /// Gets the heading angle and the projected into the given lane of
  /// the given point
  int getHeading(double & ang,  point2 &ptout, const LaneLabel &label, const point2 &pt);

  /// Gets the type of the lane bound
  int getLeftBoundType(string& type, const LaneLabel &label);
  /// Gets the type of the lane bound  
  int getRightBoundType(string& type, const LaneLabel &label);


  /// Gets the bounds for an intersection transition between the given
  /// exit and enter point labels.  The bounds are cut to the length 
  /// given by 'range' measured from a point that is a distance 
  /// 'backrange' behind the given point 'pt' 
  int getTransitionBounds(point2arr &leftbound ,point2arr &rightbound , 
                          const PointLabel &exitlabel, 
                          const PointLabel &enterlabel, 
                          const point2 pt, const double range, 
                          const double backrange=10);
  /// Gets the transition bounds for an intersection transition cut
  /// to just return the bounds inside the intersection.
  int getShortTransitionBounds(point2arr &leftbound ,point2arr &rightbound , 
                               const PointLabel &exitlabel, 
                               const PointLabel &enterlabel);



  //--------------------------------------------------
  //  These functions have reasonable replacements using
  // the newer functions
  //--------------------------------------------------

  /// checks if the given point lies within the given lane
  bool checkLaneID(const point2 pt, const int segment, const int lane);
  bool checkLaneID(const point2 pt, const LaneLabel &label);

  int getSegmentID(const point2 pt);
  int getLaneID(const point2 pt);
  //  int getZoneID(const point2 pt);
  int getNextPointID(PointLabel &label,const point2& pt);
  int getLastPointID(PointLabel &label,const point2& pt);
  int getClosestPointID(PointLabel &label, const point2& pt);

  int getLaneCenterPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
  int getLaneLeftPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
  int getLaneRightPoint(point2 &cpt, const LaneLabel &label, const point2 &pt, const double offset=0);
  
  int getLeftBound(point2arr& ptarr, const LaneLabel &label );
  int getRightBound(point2arr& ptarr, const LaneLabel &label );

  /** Stores the perimeter of the zone with label 'zoneLabel' into ptarr.
   * \return 0 if everything is ok, <0 in case of error (e.g. invalid label)
   */
  int getZonePerimeter(point2arr& ptarr, int zoneLabel);  
        
  /** Gets a vector of spot labels associated with the given zone 
   */
  int getZoneParkingSpots(vector<SpotLabel> & spotlabelarrp, int zoneLabel);
  

  /**
   * Populates the two waypoints 'first' and 'second' associated with the pasking 
   * spot with label 'spotLabel'.
   * \return 0 if everything is ok, <0 in case of error (e.g. invalid label)
   */
  int getSpotWaypoints(point2& first, point2& second, const SpotLabel &spotLabel);

  /** returns width of the parking spot
   */
  int getSpotWidth(double &width, const SpotLabel& spotLabel);

  int getStopline(point2& pt, PointLabel &label);
  int getNextStopline(point2& pt, PointLabel &label);
  int getStopline(point2& pt, point2 state);
  
  int getWaypoint(point2& pt, const PointLabel &label);

  int getBounds(point2arr& leftbound, point2arr& rightbound, 
                const LaneLabel &label,const point2 state, 
                const double range , const double backrange=10);
  int getBoundsReverse(point2arr& leftbound, point2arr& rightbound, 
                       const LaneLabel &label,const point2 state, 
                       const double range, const double backrange=10);
  

  bool isStop(PointLabel &label);
  bool isExit(PointLabel &label);

  int getWaypointArr(point2arr& ptarr, const PointLabel &label);
  int getLaneDistToWaypoint(double& dist,const point2 state,const PointLabel &label);
  int getLaneDistToWaypoint(double &dist, const PointLabel &statelabel,const PointLabel &label);

  double getObstacleDist(const point2& state,const int lanedelta=0);

  point2 getObstaclePoint(const point2& state, const double offset=0);
  int getObstacleGeometry(point2arr& ptarr);

};



#endif
 
