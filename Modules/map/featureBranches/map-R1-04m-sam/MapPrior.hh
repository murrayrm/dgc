/**********************************************************
 **
 **  MAPPRIOR.HH
 **
 **    Time-stamp: <2007-09-27 21:37:01 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Feb 19 12:51:26 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPRIOR_H
#define MAPPRIOR_H

#include <math.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

#include "dgcutils/ggis.h"

#include "MapElement.hh"
#include "frames/point2_uncertain.hh"

/// This class is used to store information computed while the
/// algorithm is running to be shown later on the debug display
/// (DebugWindow).
struct LineFusionDebugData
{
  // list of couples of points that were associated (sensed, prior), (sensed, prior), ...
  point2arr assocLines;
  point2arr equalLines;
  // more?
};
#ifdef NDEBUG
#define DBG_RESET(dbg)
#define DBG_ASSOC_RESET(dbg)
#define DBG_EQUAL_RESET(dbg)
#define DBG_ASSOC_ADD(dbg, pPrior, pSens)
#define DBG_EQUAL_ADD(dbg, pPrior, pSens)
#else
#define DBG_ASSOC_RESET(dbg) if ((dbg) != NULL) { (dbg)->assocLines.clear(); }
#define DBG_EQUAL_RESET(dbg) if ((dbg) != NULL) { (dbg)->equalLines.clear(); }
#define DBG_RESET(dbg) { DBG_ASSOC_RESET(dbg); DBG_EQUAL_RESET(dbg); }
#define __DBG_ADD_HELPER(dbg, name, p1, p2) \
    if ((dbg) != NULL) { \
        (dbg)->name.push_back(p1); \
        (dbg)->name.push_back(p2); \
    }
#define DBG_ASSOC_ADD(dbg, pPrior, pSens) __DBG_ADD_HELPER(dbg, assocLines, pPrior, pSens)
#define DBG_EQUAL_ADD(dbg, p1, p2) __DBG_ADD_HELPER(dbg, equalLines, p1, p2)
#endif


class LaneLabel;

/// Simple struct to hold an RNDF point label
class PointLabel
{
public:
  PointLabel() {}
  PointLabel(int s, int l, int p){segment = s; lane = l; point = p;}
/*// no need for this, the automatically generated copy constructor is fine
  PointLabel(const PointLabel &label){segment = label.segment; lane = label.lane; point = label.point;}
*/
  ~PointLabel() {}

  bool operator==(const PointLabel &label) const{ return (segment==label.segment && lane==label.lane && point==label.point);}
  bool operator==(const LaneLabel &label) const;
/*
  // not needed at all, the compiler will generate this by itself
  // (and it will be faster and bulletproof)
  PointLabel &operator=(const PointLabel &label){
    if (this!= &label){
      segment = label.segment;
      lane = label.lane;
      point= label.point;
    }
    return *this;
  }
*/

  friend ostream &operator<<(ostream &os, const PointLabel &label);
  friend ostream &operator<<(ostream &os, const vector<PointLabel> &label);
  void print()  { cout << segment <<" "<< lane <<" " << point << endl; }
  int segment, lane, point;
};


/// Simple struct to hold an RNDF lane label
class LaneLabel
{
public:
  LaneLabel() {}
  LaneLabel(int s, int l) {segment = s; lane = l;}
  ~LaneLabel() {}

  bool operator==(const LaneLabel &label) const { return (segment==label.segment && lane==label.lane);}
  bool operator==(const PointLabel &label) const;
  LaneLabel &operator=(const LaneLabel &label){
    if (this!= &label){
      segment = label.segment;
      lane = label.lane;
    }
    return *this;
  }
  friend ostream &operator<<(ostream &os, const LaneLabel &label);
  friend ostream &operator<<(ostream &os, const vector<LaneLabel> &label);
  void print() { cout << segment <<" "<< lane <<endl; }  
  int segment, lane;
};


/// Simple struct to hold an RNDF spot label
class SpotLabel
{
public:
  SpotLabel() {}
  SpotLabel(int z, int s) {zone = z; spot = s;}
  ~SpotLabel() {}

  bool operator==(const SpotLabel &label) { return (zone==label.zone && spot==label.spot);}
  friend ostream &operator<<(ostream &os, const SpotLabel &label);
  friend ostream &operator<<(ostream &os, const vector<SpotLabel> &label);
  void print() { cout << zone <<" "<< spot <<endl; }  
  int zone, spot;
};


/// Internal representation for the MapPrior class of an RNDF segment
class RNDFsegment
{
public:
  RNDFsegment() {}
  RNDFsegment(int val) {label=val;}

  ~RNDFsegment() {}
  int label;
  string name;
  /// array of indices of the lanes which belong to this segment
  vector<int> laneindex;
  vector<LaneLabel> lane_label;
  vector<int> lanedirection;

  // Number of points in this segment.
  // It doesn't necessarily mean that this number of points are stored in memory
  // for every lane, because lanes may start later (resp. earlier) than the
  // start (resp. end) of the segment. See RNDFlane::pointLabelToIndex().
  int numPoints;
};

/// Internal representation for the MapPrior class of an RNDF lane
class RNDFlane
{
public:
  RNDFlane() {}
  RNDFlane(LaneLabel& val) {label=val;}
  ~RNDFlane() {}
  LaneLabel label;
  string name;
  double width;
  int direction;

  int waypoint_elindex;
  int leftbound_elindex;
  int rightbound_elindex;
  vector<PointLabel> waypoint_label;
  

  string leftbound_type;
  string rightbound_type;
  
  LaneLabel leftneighbor_label;
  LaneLabel rightneighbor_label;

  vector<PointLabel> exit_label;
  vector<PointLabel> exit_link;
  vector<int> exit_leftbound_elindex;
  vector<int> exit_rightbound_elindex;
  vector<int> exit_waypoint_elindex;

  vector<PointLabel> entry_label;
  vector<PointLabel> entry_link;
  vector<int> entry_leftbound_elindex;
  vector<int> entry_rightbound_elindex;
  vector<int> entry_bound_elindex;

  vector<PointLabel> stop_label;
  vector<int> stop_datindex;

  vector<PointLabel> checkpt_label;
  vector<int> checkpt_num;

  // line fusion related stuff (but could be useful elsewhere)
  // this data is initialized only if line fusion is enabled though

  // 0 if the i-th point is an interpolated one, or the waypoint label
  // (index in the RNDF geometry) if it corresponds to a RNDF waypoint.
  vector<int> isWaypoint;

  /// Maps segment linear indexes (position along the length of the segment)
  /// into actual lane geometry indexes. The first corresponds to geomStart
  /// and the last to geomEnd.
  vector<int> segIndex;
    
  /// Start and end indexes of the geometry, in case this lane doesn start
  /// and/or end at the same relative position of the longest lane.
  int geomStart, geomEnd;

  /// Given the lane and the point label, returns the index of the point in the geometry.
  int pointLabelToIndex(int point) const
  {
    if (point < geomStart || point > geomEnd) return -1;
    return segIndex[point - geomStart];
  }

};

/// Internal representation for the MapPrior class of an RNDF zone
class RNDFzone
{
public:
  RNDFzone() {}
  RNDFzone(int val) {label=val;}
  ~RNDFzone() {}
  int label;  
  string name;
  int perimeter_elindex;
  vector<PointLabel> perimeter_label;

  vector<int> spotindex;
  vector<SpotLabel> spot_label;

  vector<PointLabel> exit_link;
  vector<PointLabel> exit_label;
  vector<PointLabel> entry_label;
  vector<PointLabel> entry_link;
  
};

/// Internal representation for the MapPrior class of an RNDF spot
class RNDFspot
{
public:
  RNDFspot() {}
  RNDFspot(SpotLabel& val) {label=val;}
  ~RNDFspot() {}
  SpotLabel label;
  string name;
  
  double width;
  
  int waypt_elindex;
  vector<PointLabel> waypoint_label;

  int leftbound_elindex;
  int rightbound_elindex;
  
  vector<int> check_ptindex;
  vector<PointLabel> checkpt_label;
  vector<int> checkpt_num;

};


/// Defines the data structure which holds prior data derived from an RNDF file
class MapPrior
{

  friend class DebugWindow; // allow debugging code to peek inside
  
public:
  
  MapPrior();
  
  ~MapPrior();

  // This structure holds the raw parsed rndf data
  vector<MapElement> data;

  // This structure holds the parsed rndf data with full interpolation
  vector<MapElement> fulldata;
  
  // This structure holds interpolated transition bounds data
  vector<MapElement> transdata;

  vector<bool> fulldataUpdateFlag; 
 
  /// Load the RNDF file given by the filename into the map prior struct
  bool loadRNDF(string filename);

  /// Access the prior raw data in local frame
  bool getEl(MapElement & el, int index);
  /// Access the prior interpolated data in local frame
  bool getElFull(MapElement & el, int index);

  /// Sets the prior raw data in local frame
  bool setEl(const MapElement & el, int index);
  /// Sets the prior interpolated (or fused) data in local frame
  bool setElFull(const MapElement & el, int index);


  ///Representation of RNDF segment structure
  vector<RNDFsegment> segments;
  ///Representation of RNDF lane structure
  vector<RNDFlane> lanes;
  ///Representation of RNDF zone structure
  vector<RNDFzone> zones;  
  ///Representation of RNDF parking spot structure
  vector<RNDFspot> spots;
  ///Representation of RNDF checkpoint structure
  vector<PointLabel> checkpoints;



  int numSegments;
  int numZones;
  string RNDFname;
  string RNDFdate;
  double RNDFversion;

  bool internalLineFusion;

private:

  /// Main internal parser of RNDF file
  bool parseRNDF(vector<string>& strfile);
  /// Adds additional lane interpolation to the RNDF parsing
  bool parseLaneInterpolation();
  /// Sets links between neighboring lanes in the RNDF parser
  bool parseLaneNeighbors();
  /// Checks and labels lane direction in the RNDF parser
  bool parseLaneDirection();
  /// Establishes links from exit to entry points for transitions in the RNDF parser
  bool parseExitPoints();
  /// Generates interpolated paths for transitions in the RNDF parser
  bool parseTransitions();

  /// Low level point label parser
  bool parsePointLabel(string&, PointLabel& label);
  /// Low level lane label parser
  bool parseLaneLabel(string&, LaneLabel& label);
  /// Low level spot label parser
  bool parseSpotLabel(string&, SpotLabel& label);


  

  int getLaneIndex(const LaneLabel &label);
  int getLaneIndex(const PointLabel &label);
  int getLaneIndex(const int segnum, const int lanenum);

  int getLanePointIndex(const PointLabel &label);
  int getLanePointIndex(const int segnum, const int lanenum, const int ptnum);
  
  int getZoneIndex(const LaneLabel &label);
  int getZoneIndex(const PointLabel &label);
  int getZoneIndex(const SpotLabel &label);
  int getZoneIndex(const int zonenum);

  int getZoneSpotIndex(const SpotLabel &label);
  int getZoneSpotIndex(const int zonenum, const int spotnum);
  int getZonePointIndex(const PointLabel &label);
  int getZonePointIndex(const int segnum, const int lanenum, const int ptnum);


  int getSegmentIndex(const LaneLabel &label);
  int getSegmentIndex(const PointLabel &label);
  int getSegmentIndex(const int segnum);

public:


  //general queries
  bool isEntryPoint(const PointLabel &ptlabelin);
  bool isExitPoint(const PointLabel &ptlabelin);
  bool isStopLine(const PointLabel &ptlabelin);
  bool isCheckPoint(const PointLabel &ptlabelin);

  bool isLaneSameDir(const LaneLabel &lanelabel1,const LaneLabel &lanelabel2);
  bool isPointInLane(const point2_uncertain &pt, const LaneLabel &lanelabelin);
  bool isPointInLane(const point2arr_uncertain &pt, const LaneLabel &lanelabelin);
  bool isPolyInLane(const point2arr_uncertain &pt, const LaneLabel &lanelabelin);
  bool isLineInLane(const point2arr_uncertain &pt, const LaneLabel &lanelabelin);
  bool isElementInLane(const MapElement &el, const LaneLabel &lanelabelin);

  int getLanesInElement(vector<LaneLabel> labelarr, const MapElement &el);

  int getTransitionBounds(point2arr_uncertain &leftbound, 
                          point2arr_uncertain &rightbound, 
                          const PointLabel &ptlabelfrom,
                          const PointLabel &ptlabelto);

  int getLaneLeftBound(point2arr_uncertain &bound, 
                       const LaneLabel &lanelabelin);
  int getLaneLeftBoundFull(point2arr_uncertain &bound, 
                           const LaneLabel &lanelabelin);
  int getLaneRightBound(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin);
  int getLaneRightBoundFull(point2arr_uncertain &bound, 
                            const LaneLabel &lanelabelin);
  int getLaneBounds(point2arr_uncertain &leftbound, 
                    point2arr_uncertain &rightbound, 
                    const LaneLabel &lanelabelin);
  int getLaneBoundsFull(point2arr_uncertain &leftbound, 
                        point2arr_uncertain &rightbound, 
                        const LaneLabel &lanelabelin);

  int getLaneBoundsPoly(point2arr_uncertain &bound, 
                        const LaneLabel &lanelabelin);
  int getLaneBoundsPolyFull(point2arr_uncertain &bound, 
                            const LaneLabel &lanelabelin);


  int getLaneCenterLine(point2arr_uncertain &centerline, 
                        const LaneLabel &lanelabelin);
  int getLaneCenterLineFull(point2arr_uncertain &centerline,
                                const LaneLabel &lanelabelin);

  
  int getWayPoint(point2_uncertain &pt, 
                  const PointLabel &ptlabelin);
  int getWayPoint(point2arr_uncertain &ptarr, 
                  const vector<PointLabel> &ptlabelarrin);

  int getSameDirLaneBounds(point2arr_uncertain &leftbound, 
                           point2arr_uncertain &rightbound, 
                           const LaneLabel &lanelabelin);

  int getOppDirLaneBounds(point2arr_uncertain &leftbound, 
                          point2arr_uncertain &rightbound, 
                          const LaneLabel &lanelabelin);
 
  int getSegmentBounds(point2arr_uncertain &leftbound, 
                       point2arr_uncertain &rightbound, 
                       const LaneLabel &lanelabelin);

  


  //intersection queries
  int getWayPointEntries(vector<PointLabel>& ptlabelarr,
                         const PointLabel &ptlabelin);
  int getWayPointExits(vector<PointLabel>& ptlabelarr,
                       const PointLabel &ptlabelin);


  int getLaneExits(vector<PointLabel>& thisplabelarr,
                   vector<PointLabel>& otherlabelarr, 
                   const LaneLabel &lanelabelin);

  int getLaneEntries(vector<PointLabel>& thislabelarr,
                     vector<PointLabel>& otherlabelarr, 
                     const LaneLabel &lanelabelin);
  int getZoneExits(vector<PointLabel>& thisplabelarr,
                   vector<PointLabel>& otherlabelarr, 
                   const int zonelabelin);

  int getZoneEntries(vector<PointLabel>& thislabelarr,
                     vector<PointLabel>& otherlabelarr, 
                     const int zonelabelin);



  int getLaneStopLines(vector<PointLabel>& ptlabelarr,
                       const LaneLabel &lanelabelin);
  int getLaneCheckPoints(vector<PointLabel>& ptlabelarr,
                         const LaneLabel &lanelabelin);
  int getLaneCheckPointNumbers(vector<int>& ptnumarr,
                               const LaneLabel &lanelabelin);
  int getLaneName(string &name, 
                  const LaneLabel &lanelabelin);



  int getNeighborLane(LaneLabel& lanelabel, 
                      const LaneLabel &lanelabelin,
                      const int offset);

  int getSameDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin);

  int getOppDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin);

  int getAllDirLanes(vector<LaneLabel>& lanelabelarr, const LaneLabel &lanelabelin);




  



 
  int getSegmentLanes(vector<LaneLabel>& lanelabelarr, const int seglabelin);

  int getSegmentName(string &name, const int seglabelin);
  //  int getSegmentEntryFromLabels(vector<PointLabel>& ptlabelarr,
  //                             const int seglabelin);
 

  int getSegmentNumLanes(const PointLabel & pointlabelin);
  int getSegmentNumLanes(const LaneLabel & lanelabelin);
  int getSegmentNumLanes(const int seglabelin); 


	
  //   int getLaneLeftIndex(const LaneLabel &lanelabelin);
  //   int getLaneRightIndex(const LaneLabel &lanelabelin);
  //   int getLaneIndex(const LaneLabel &lanelabelin);
  //   int getPointIndex(const PointLabel &ptlabelin);
  //   int getLaneSameDirLeftIndex(const LaneLabel &lanelabelin);
  //   int getLaneSameDirRightIndex(const LaneLabel &lanelabelin);
  //   int getLaneOppDirLeftIndex(const LaneLabel &lanelabelin);
  //   int getLaneOppDirRightIndex(const LaneLabel &lanelabelin);
  //     int getLaneSegmentLeftIndex(const int seglabelin);
  //    int getLaneSegmentRightIndex(const int seglabelin);
  
  int setLocalToGlobalOffset(const VehicleState &state);

  // line fusion related stuff

  LineFusionDebugData* lineFusDebug;

  /// Do any post-processing after updating the prior with new sensed
  /// data (smoothing, constraint enforcement).
  /// if enforce is false, only smoothing is done. Faster, but left and right
  /// bounds are not recalculated. Useful to so more smoothing steps before
  /// one full step.
  /// seg and lane are in the format returned by getClosestSegment.
  void postProcessLane(int seg, int lane, const vector<bool>& isUpd = vector<bool>(),
                       bool enforce = true, bool smooth = true);

  /// Make sure that lanes don't overlap after performing line fusion, and do any other
  /// post-processing on the resulting segment
  void postProcessSegment(int seg/*, int point*/);

  /// Given the full point label, returns the index of the point in the geometry.
  int pointLabelToIndex(const PointLabel& pt) const
  {
    const RNDFlane& lane = lanes[segments[pt.segment].laneindex[pt.lane]];
    return lane.pointLabelToIndex(pt.point);
  }

  /// Initialize the data to be used by the line fusion. This means, in pratice,
  /// matching points in adjacent lanes and finding out when a lane starts
  /// and ends, relative to the linear position in the segment.
  void initLineFusion();


  /// Returns the label, made of (segment, lane, point index) of the point
  /// in the next lane (west to east or north to south order) corresponding
  /// to the specified one.
  /// The 'point label' here is a value that identifies the position within
  /// the segment, and is equivalent to RNDFlane::geomStart + i, where i is
  /// the actual index within MapElement::geometry.
  PointLabel getNextLaneAt(const PointLabel& currPt) const;

  /// Same as getNextLaneAt(), but returns the point in the previous
  /// lane, in the same ordering (west to east, north to south).
  PointLabel getPrevLaneAt(const PointLabel& currPt) const;


  point2_uncertain delta; // global to local delta
  double deltaYaw; // global to local delta

};
#endif
