/**********************************************************
 **
 **  TEST_MAP.CC
 **
 **    Time-stamp: <2007-03-03 15:14:26 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Thu Mar  1 12:53:36 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include "map/Map.hh"
#include <iostream>
#include <string>
#include "skynettalker/MapElementTalker.hh"
#include "interfaces/MapElement.hh"

using namespace std;


class CTestSendMapElement : public CMapElementTalker{

public:
	/*! Constructor */
  CTestSendMapElement(){}
      
  /*! Standard destructor */
  ~CTestSendMapElement() {}

};

int main(int argc, char **argv)
{
	int skynetKey = 0;
	int subgroup = 0;
	
	skynetKey = atoi(getenv("SKYNET_KEY"));	

	cout << endl << "Usage :" << endl;
	cout << " testSendMapElement [skynet key] [subgroup]" << endl << endl;

	if(argc >1)
		subgroup = atoi(argv[1]);
	if(argc >2)
		skynetKey = atoi(argv[2]);
	
	cout << "Skynet Key = " << skynetKey << 
		"  Subgroup = "  << subgroup << endl;

	CTestSendMapElement testtalker;
	testtalker.initSendMapElement(skynetKey);

	Map mapdata;
	mapdata.loadRNDF("rndf.txt");
	MapElement el;
	vector<int> id;
vector<point2> rawptarr;

	PointLabel exitlabel(10,1,5);
	PointLabel enterlabel(3,1,8);
	PointLabel stoplabel(10,1,7);
	LaneLabel lanelabel(10,1);
	LaneLabel lanelabel2(3,1);
	string lanetype;
	int val;

	point2 pt;
	point2arr ptarr, lptarr, rptarr;

	val = mapdata.getStopline(pt,stoplabel);
	cout << "Stopline at " << stoplabel << " = " << pt << endl;
	el.clear();
	id.clear();
	id.push_back(stoplabel.segment);
	id.push_back(stoplabel.lane);
	id.push_back(stoplabel.point);
	rawptarr.clear();
	rawptarr.push_back(pt);
	el.set_stopline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);

	val = mapdata.getLeftBound(ptarr,lanelabel);
	cout << "Left lane at " << lanelabel << " = " << ptarr << endl;
	el.clear();
	id.clear();	
	id.push_back(lanelabel.segment);
	id.push_back(lanelabel.lane);
	id.push_back(0);

	rawptarr.clear();
	rawptarr = ptarr.arr;
	el.set_laneline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);

		val = mapdata.getRightBound(ptarr,lanelabel);
	cout << "Right lane at " << lanelabel << " = " << ptarr << endl;
	el.clear();
	id.clear();	
	id.push_back(lanelabel.segment);
	id.push_back(lanelabel.lane);
	id.push_back(1);
	rawptarr.clear();
	rawptarr = ptarr.arr;
	el.set_laneline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);

	
	val = mapdata.getLeftBound(ptarr,lanelabel2);
	cout << "Left lane at " << lanelabel2 << " = " << ptarr << endl;
	el.clear();
	id.clear();	
	id.push_back(lanelabel2.segment);
	id.push_back(lanelabel2.lane);
	id.push_back(0);
	rawptarr.clear();
	rawptarr = ptarr.arr;
	el.set_laneline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);

		val = mapdata.getRightBound(ptarr,lanelabel2);
	cout << "Right lane at " << lanelabel2 << " = " << ptarr << endl;
	el.clear();
	id.clear();	
	id.push_back(lanelabel2.segment);
	id.push_back(lanelabel2.lane);
	id.push_back(1);

	rawptarr.clear();
	rawptarr = ptarr.arr;
	el.set_laneline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);



	val = mapdata.getLeftBoundType(lanetype,lanelabel);
	cout << "Left lane type at " << lanelabel << " = " << lanetype << endl;

	val = mapdata.getRightBoundType(lanetype,lanelabel);
	cout << "Right lane type at " << lanelabel << " = " << lanetype << endl;

	val = mapdata.getTransitionBounds(lptarr,rptarr,exitlabel,enterlabel);
	cout << "Transition from " << exitlabel 
			 << " to " << enterlabel 
			 << ", left: " << lptarr << endl
			 << "right: " << rptarr<<endl;
	el.clear();
	id.clear();	
	id.push_back(exitlabel.segment);
	id.push_back(exitlabel.lane);
	id.push_back(exitlabel.point);
	id.push_back(enterlabel.segment);
	id.push_back(enterlabel.lane);
	id.push_back(enterlabel.point);
	id.push_back(0);
	
	rawptarr.clear();
	rawptarr = lptarr.arr;
	el.set_laneline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);

	el.clear();
	id.clear();	
	id.push_back(exitlabel.segment);
	id.push_back(exitlabel.lane);
	id.push_back(exitlabel.point);
	id.push_back(enterlabel.segment);
	id.push_back(enterlabel.lane);
	id.push_back(enterlabel.point);
	id.push_back(1);
	
	rawptarr.clear();
	rawptarr = rptarr.arr;
	el.set_laneline(id,rawptarr);
	testtalker.sendMapElement(&el,subgroup);

	int retval;
	rawptarr.clear();
	PointLabel label;
	point2 statept;
	point2 stoppt;
	for (int i = 660; i<671; ++i){
		statept = point2(i,-540);
		cout << endl <<"For point " << statept << endl;
	label=	mapdata.getNextPointID(statept);
	cout << "ptlabel = " << label << endl;	
	retval =mapdata.getStopline(stoppt,statept);
	cout << "retval = " << retval << " point = " << stoppt << endl;
	
}
	
	for (int i = 805; i<812; ++i){
		statept = point2(i,-252);
		cout << endl <<"For point " << statept << endl;
	label=	mapdata.getNextPointID(statept);
	cout << "ptlabel = " << label << endl;
	retval =mapdata.getStopline(stoppt,statept);
	cout << "retval = " << retval << " point = " << stoppt << endl;
	}
	
	

	return 0;


}	
