/**********************************************************
 **
 **  MAP.HH
 **
 **    Time-stamp: <2007-02-26 15:59:19 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Fri Feb 16 21:11:33 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAP_H
#define MAP_H

#include <math.h>
#include <string>
#include <sstream>
#include <vector>

#include "MapPrior.hh"
#include "interfaces/MapElement.hh"


class Map
{
	
public:
	
	Map();
	
	~Map();

	bool loadRNDF(string filename) {return prior.loadRNDF(filename);}

	vector<MapElement> data;
	MapPrior prior;


};



#endif
