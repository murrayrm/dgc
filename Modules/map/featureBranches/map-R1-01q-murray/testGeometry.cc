/**********************************************************
 **
 **  TESTGEOMETRY.CC
 **
 **    Time-stamp: <2007-03-17 11:13:56 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Mar 13 13:29:56 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "frames/point2.hh"
#include "frames/point2_uncertain.hh"

using namespace std;


int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testGeometry [testnum] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  int argnum = 1;
  if(argc >argnum)
    testnum = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    sendSubGroup = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    recvSubGroup = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    skynetKey = atoi(argv[argnum]);
  argnum++;
  
  cout << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey);


  cout <<"========================================" << endl;
  testname = "Testing Point2";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl;
    
    sendEl.set_id(-1);
    sendEl.set_points();
    sendEl.plot_color = MAP_COLOR_MAGENTA;

  
    point2 statept;
    point2 pt;
    point2arr ptarr;
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        recvEl.print();
        cout << endl <<"For point " << statept << endl;
        ptarr.clear();
        ptarr.push_back(statept);
        sendEl.set_geometry(ptarr);
        cout << "sent geometry " << endl;
        sendEl.print();
        maptalker.sendMapElement(&sendEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  cout <<"========================================" << endl;
  testname = "Testing line2 dist, is_normal)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.set_points();
    sendEl2.plot_color = MAP_COLOR_GREEN;

  
    point2 statept;
    point2 pt;
    point2arr ptarr,baseptarr;
    line2 line;

    int inputnum = 2;
    
    while (true){

      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum==2){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          line.ptA = statept;
          sendEl1.set_geometry(statept);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          inputnum--;
        }
        else if (inputnum==1){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          line.ptB = statept;
          ptarr = line;
          sendEl1.set_geometry(ptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          inputnum--;
        
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;
        
          int retval = line.is_normal(statept);
          cout << "line.is_normal = " << retval << endl;
        
          double dist = line.dist(statept);
          cout << "line.dist = " << dist << endl;

          pt =line.project(statept);
        
          sendEl2.set_geometry(pt);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
  
  cout <<"========================================" << endl;
  testname = "Testing point2arr.project(const point2& pt)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.set_points();
    sendEl2.plot_color = MAP_COLOR_GREEN;

  
    point2 statept;
    point2 pt;
    point2arr ptarr,baseptarr;

    int inputnum = 5;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          sendEl1.set_geometry(baseptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;
          pt =ptarr.project(statept);
        
          sendEl2.set_geometry(pt);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
  



  
  cout <<"========================================" << endl;
  testname = "Testing line intersect (line), is_intersect(line)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2,sendEl3;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.set_line();
    sendEl2.plot_color = MAP_COLOR_GREEN;

    sendEl3.set_id(-3);
    sendEl3.set_points();
    sendEl3.plot_color = MAP_COLOR_BLUE;

  
    point2 statept;
    point2 pt;
    point2arr ptarr;

    line2 line1,line2;
    bool firstline = true;
    bool firstpoint = true;
    
    while (true){
 
        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstline){
          if (firstpoint){
            statept = recvEl.center;
            line1.ptA = statept;
            sendEl1.set_geometry(statept);
            maptalker.sendMapElement(&sendEl1,sendSubGroup);

            firstpoint = false;
          }else{
            statept = recvEl.center;
            line1.ptB = statept;
            ptarr = line1;
            sendEl1.set_geometry(ptarr);
            maptalker.sendMapElement(&sendEl1,sendSubGroup);
            firstline = false;
            firstpoint = true;
          }


        }else {
          if (firstpoint){
            statept = recvEl.center;
            line2.ptA = statept;
            sendEl2.set_geometry(statept);
            maptalker.sendMapElement(&sendEl2,sendSubGroup);
            
            firstpoint = false;
          }else{
            statept = recvEl.center;
            line2.ptB = statept;
            ptarr = line2;
            sendEl2.set_geometry(ptarr);
            maptalker.sendMapElement(&sendEl2,sendSubGroup);
            
            pt = line1.intersect(line2);
            cout << "INTERSECTION point at " << pt << endl;
            bool val = line1.is_intersect(line2);
            cout << "IS_INTERSECT = " << val << endl;
            sendEl3.set_geometry(pt);
            maptalker.sendMapElement(&sendEl3,sendSubGroup);
          }


        }
       
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  
 
  cout <<"========================================" << endl;
  testname = "Testing point2arr.cut front( double)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.set_line();
    sendEl2.plot_color = MAP_COLOR_GREEN;

    point2 statept;
    point2 pt;
    point2arr ptarr,baseptarr;

    bool cutfront = true;
    int inputnum = 5;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          sendEl1.set_geometry(baseptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          double val = statept.x;
          ptarr = baseptarr;        
          if (cutfront){
            cout << "CUTTING FRONT val = " << val  << endl;
            ptarr.cut_front(val);
            cutfront=false;
          }else{
            cout << "CUTTING BACK val = " << val  << endl;
            ptarr.cut_back(val);
            cutfront = true;
          }
          sendEl2.set_geometry(ptarr);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
    

   
  cout <<"========================================" << endl;
  testname = "Testing point2arr.cut front( point)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;
 
    sendEl2.set_id(-2);
    sendEl2.set_line();
    sendEl2.plot_color = MAP_COLOR_GREEN;
 
    point2 statept; 
    point2 pt;
    point2arr ptarr,baseptarr;

    bool cutfront = true;
    int inputnum = 5;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          sendEl1.set_geometry(baseptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;        
          if (cutfront){
            cout << "CUTTING FRONT point = " << statept  << endl;
            ptarr.cut_front(statept);
            cutfront=false;
          }else{
            cout << "CUTTING BACK point = " << statept  << endl;
            ptarr.cut_back(statept);
            cutfront = true;
          }
          sendEl2.set_geometry(ptarr);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
    

   
 
  cout <<"========================================" << endl;
  testname = "Testing ptarr connect, connect_intersect (ptarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2,sendEl3;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.set_line();
    sendEl2.plot_color = MAP_COLOR_GREEN;

    sendEl3.set_id(-3);
    sendEl3.set_line();
    sendEl3.plot_color = MAP_COLOR_BLUE;

  
    point2 statept;
    point2 pt;
    point2arr ptarr,ptarr1,ptarr2;

    bool firstptarr = true;
    bool firstpoint = true;
    bool connectflag = true;
    while (true){
 
        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstptarr){
          if (firstpoint){
            statept = recvEl.center;
            ptarr1.push_back(statept);
            sendEl1.set_geometry(statept);
            maptalker.sendMapElement(&sendEl1,sendSubGroup);

            firstpoint = false;  
          }else{
            statept = recvEl.center;
            ptarr1.push_back(statept);
            sendEl1.set_geometry(ptarr1);
            maptalker.sendMapElement(&sendEl1,sendSubGroup);
            firstptarr = false;
            firstpoint = true;
          }

 
        }else {
          if (firstpoint){
            statept = recvEl.center;
            ptarr2.clear();
            ptarr2.push_back(statept);
            sendEl2.set_geometry(statept);
            maptalker.sendMapElement(&sendEl2,sendSubGroup);
            
            firstpoint = false;
          }else{
            statept = recvEl.center;
            ptarr2.push_back(statept);
            
            sendEl2.set_geometry(ptarr2);
            maptalker.sendMapElement(&sendEl2,sendSubGroup);
            
            ptarr = ptarr1;
            if (connectflag){
              cout << "connect" << endl;
              ptarr.connect(ptarr2);
              connectflag = false;
            }
            else{ 
              cout << "connect_intersect" << endl;
              ptarr.connect_intersect(ptarr2);
              connectflag = true;
 
            }
            sendEl3.set_geometry(ptarr);
            maptalker.sendMapElement(&sendEl3,sendSubGroup);
            

            firstpoint = true;
          }


        }
       
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  
 
  cout <<"========================================" << endl;
  testname = "Testing point2arr.get_bound_box(const point2arr& ptarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.type = ELEMENT_OBSTACLE;
    sendEl2.geometry_type = GEOMETRY_POLY;   
    sendEl2.plot_color = MAP_COLOR_GREEN;

  
    point2 statept;
    point2 pt;
    point2arr ptarr,baseptarr;

    int inputnum = 5;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          sendEl1.set_geometry(baseptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr =baseptarr.get_bound_box();
        
          sendEl2.set_geometry(ptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          maptalker.sendMapElement(&sendEl2,sendSubGroup);
          baseptarr.clear();
          inputnum =5;
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  } 
  

 

  cout <<"========================================" << endl;
  testname = "Testing point2arr.split(double )";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    MapElement recvEl;
    MapElement sendEl1,sendEl2;
    
    sendEl1.set_id(-1);
    sendEl1.set_line();
    sendEl1.plot_color = MAP_COLOR_RED;

    sendEl2.set_id(-2);
    sendEl2.type = ELEMENT_OBSTACLE;
    sendEl2.geometry_type = GEOMETRY_POLY;   
    sendEl2.plot_color = MAP_COLOR_GREEN;

  
    point2 statept;
    point2 pt;
    point2arr ptarr,baseptarr;
    vector<point2arr> ptarrarr;
    int inputnum = 8;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          sendEl1.set_geometry(baseptarr);
          maptalker.sendMapElement(&sendEl1,sendSubGroup);
          
          inputnum--;
        }else{
          
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          cout << "splitting based on " << statept.x << endl;
          ptarrarr =baseptarr.split(statept.x);
          cout << "got size " << ptarrarr.size() << endl;
          for (unsigned j = 0; j< ptarrarr.size();++j){
            ptarr = ptarrarr[j];
            sendEl2.set_geometry(ptarr);
            sendEl2.set_id(-2-j);
            maptalker.sendMapElement(&sendEl2,sendSubGroup);
          }
       

        } 
      }
      else{ 
        usleep (100000);
      }
    }
  } 
  







 
 

  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
