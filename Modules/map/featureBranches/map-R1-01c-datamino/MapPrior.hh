/**********************************************************
 **
 **  MAPPRIOR.HH
 **
 **    Time-stamp: <2007-03-01 13:09:53 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Mon Feb 19 12:51:26 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPPRIOR_H
#define MAPPRIOR_H

#include <math.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

#include "dgcutils/ggis.h"

#include "interfaces/MapElement.hh"

class LaneLabel;

class PointLabel
{
public:
	PointLabel() {}
	PointLabel(int s, int l, int p){segment = s; lane = l; point = p;}
	~PointLabel() {}

	bool operator==(const PointLabel &label) { return (segment==label.segment && lane==label.lane && point==label.point);}
	bool operator==(const LaneLabel &label);

	friend ostream &operator<<(ostream &os, const PointLabel &label);
void print()	{ cout << segment <<" "<< lane <<" " << point << endl; }
	int segment, lane, point;
};




class LaneLabel
{
public:
	LaneLabel() {}
	LaneLabel(int s, int l) {segment = s; lane = l;}
	~LaneLabel() {}

	bool operator==(const LaneLabel &label) { return (segment==label.segment && lane==label.lane);}
	bool operator==(const PointLabel &label);
	friend ostream &operator<<(ostream &os, const LaneLabel &label);


	void print() { cout << segment <<" "<< lane <<endl; }	
	int segment, lane;
};

class SpotLabel
{
public:
	SpotLabel() {}
	SpotLabel(int z, int s) {zone = z; spot = s;}
	~SpotLabel() {}

	bool operator==(const SpotLabel &label) { return (zone==label.zone && spot==label.spot);}
	void print() { cout << zone <<" "<< spot <<endl; }	
	int zone, spot;
};



class RNDFsegment
{
public:
	RNDFsegment() {}
	RNDFsegment(int val) {label=val;}

	~RNDFsegment() {}
	int label;
	string name;
	vector<int> laneindex;
	vector<LaneLabel> lane_label;
};

class RNDFlane
{
public:
	RNDFlane() {}
	RNDFlane(LaneLabel& val) {label=val;}
	~RNDFlane() {}
	LaneLabel label;
	string name;
	double width;
	
	int waypoint_elindex;
	int leftbound_elindex;
	int rightbound_elindex;
	vector<PointLabel> waypoint_label;
	
	string leftbound_type;
	string rightbound_type;
	
	int leftneighbor_laneindex;
	int rightneighbor_laneindex;
	
	vector<int> exit_ptindex;
	vector<PointLabel> exit_label;
	vector<PointLabel> exit_link;
	vector<int> exit_intindex;

	vector<int> entry_ptindex;
	vector<PointLabel> entry_label;
	vector<PointLabel> entry_link;
	vector<int> entry_intindex;

	vector<int> stop_ptindex;
	vector<PointLabel> stop_label;
	vector<int> check_ptindex;
	vector<PointLabel> checkpt_label;
	vector<int> checkpt_num;

};


class RNDFzone
{
public:
	RNDFzone() {}
	RNDFzone(int val) {label=val;}
	~RNDFzone() {}
	int label;	
	string name;
	int perimeter_elindex;
	vector<PointLabel> perimeter_label;

	//	SpotLabel perimeter_label;

	vector<int> spotindex;
	vector<SpotLabel> spot_label;


	vector<int> exit_ptindex;
	vector<PointLabel> exit_link;
	vector<PointLabel> exit_label;
	vector<int> exit_intindex;

	vector<int> entry_ptindex;
	vector<PointLabel> entry_label;
	vector<PointLabel> entry_link;
	vector<int> entrypt_intindex;

	
};

class RNDFspot
{
public:
	RNDFspot() {}
	RNDFspot(SpotLabel& val) {label=val;}
	~RNDFspot() {}
	SpotLabel label;
	string name;
	
	double width;
	
	int waypt_elindex;
	vector<PointLabel> waypoint_label;

	int leftbound_elindex;
	int rightbound_elindex;
	
	vector<int> check_ptindex;
	vector<PointLabel> checkpt_label;
	vector<int> checkpt_num;

};


//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class MapPrior
{
	
public:
	
	//! A Constructor 
	/*! DETAILS */
	MapPrior();
	
	//! A Destructor 
	/*! DETAILS */
	~MapPrior();

	vector<MapElement> data;

	bool loadRNDF(string filename);
	bool parseRNDF(vector<string>& strfile);
	//	bool parseHeader(vector<string>& strfile);
	//	bool parseSegments(vector<string>& strfile);
	//	bool parseZones(vector<string>& strfile);
	bool parsePointLabel(string&, PointLabel& label);
	bool parseLaneLabel(string&, LaneLabel& label);
	bool parseSpotLabel(string&, SpotLabel& label);


	bool getEl(int index, MapElement & el);


	vector<RNDFsegment> segments;
	vector<RNDFlane> lanes;
	vector<RNDFzone> zones;	
	vector<RNDFspot> spots;
	vector<PointLabel> checkpoints;
	
	int numSegments;
	int numZones;
	string RNDFname;
	string RNDFdate;
	double RNDFversion;

	

	point2_uncertain delta; // global to local delta

};
#endif
