/**********************************************************
 **
 **  TESTGEOMETRY.CC
 **
 **    Time-stamp: <2007-06-10 10:35:13 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Tue Mar 13 13:29:56 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#include <iostream>
#include <string>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "frames/point2.hh"
#include "frames/ellipse.hh"
#include "frames/point2_uncertain.hh"

using namespace std;


int main(int argc, char **argv)
{
  int skynetKey = 0;
  int sendSubGroup = 0;
  int recvSubGroup = -1;
  string fname;
  int testnum = 0;
  int testarg = 0;
  int thistest=0;
  bool testflag =0;
  string  testname;

  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testGeometry [testnum] [test argument] [send subgroup] [recv subgroup] [skynet key] " << endl << endl;

  int argnum = 1;
  if(argc >argnum)
    testnum = atoi(argv[argnum]);
  argnum++;
  if(argc > argnum)
    testarg = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    sendSubGroup = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    recvSubGroup = atoi(argv[argnum]);
  argnum++;
  if(argc >argnum)
    skynetKey = atoi(argv[argnum]);
  argnum++;
  
  cout << "  Test Number = " << testnum << endl 
       << "  Send Subgroup = "  << sendSubGroup 
       << "  Recv Subgroup = "  << recvSubGroup 
       << "  Skynet Key = " << skynetKey << endl;

  CMapElementTalker maptalker;
  maptalker.initSendMapElement(skynetKey);
  maptalker.initRecvMapElement(skynetKey,recvSubGroup);

    MapElement recvEl;
    MapElement pointEl, pointEl1, lineEl, lineEl1, lineEl2,polyEl, polyEl1, polyEl2, clearEl;
 clearEl.setTypeClear();
    pointEl.setId(-1);
    pointEl.setTypePoints();
    pointEl.plotColor = MAP_COLOR_MAGENTA;

    pointEl1.setId(-2);
    pointEl1.setTypePoints();
    pointEl1.plotColor = MAP_COLOR_GREY;

    lineEl.setId(-3);
    lineEl.setTypeLine();
    lineEl.plotColor = MAP_COLOR_RED;
   
    lineEl1.setId(-4);
    lineEl1.setTypeLine();
    lineEl1.plotColor = MAP_COLOR_BLUE;
   
    lineEl2.setId(-5);
    lineEl2.setTypeLine();
    lineEl2.plotColor = MAP_COLOR_GREEN;

    polyEl.setId(-6);
    polyEl.setTypePoly();
    polyEl.plotColor = MAP_COLOR_YELLOW;

    polyEl1.setId(-7);
    polyEl1.setTypePoly();
    polyEl1.plotColor = MAP_COLOR_CYAN;
  
   polyEl2.setId(-8);
    polyEl2.setTypePoly();
    polyEl2.plotColor = MAP_COLOR_MAGENTA;
    
    point2 statept;
    point2 pt,pt1,pt2,pt3;
    point2arr ptarr, ptarr1, ptarr2, ptarr3,baseptarr,ptarroff;
    line2 line,line1,line2;
    
 

  cout <<"========================================" << endl;
  testname = "Testing Point2";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    while (true){

      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        statept = recvEl.center;
        cout<<recvEl<< endl;
        cout << endl <<"For point " << statept << endl;
        ptarr.clear();
        ptarr.push_back(statept);
        pointEl.setGeometry(ptarr);
        cout << "sent geometry " << endl;
        cout << pointEl << endl;
        maptalker.sendMapElement(&pointEl,sendSubGroup);
      } 
      else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing line2 dist, is_normal)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
    int inputnum = 2;
    while (true){
      
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum==2){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          line.ptA = statept;
          lineEl.setGeometry(statept);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
        }
        else if (inputnum==1){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          line.ptB = statept;
          ptarr = line;
          lineEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
        
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;
        
          int retval = line.is_normal(statept);
          cout << "line.is_normal = " << retval << endl;
        
          double dist = line.dist(statept);
          cout << "line.dist = " << dist << endl;

          pt =line.project(statept);
        
          pointEl.setGeometry(pt);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
  
  cout <<"========================================" << endl;
  testname = "Testing point2arr.project(const point2& pt)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    int inputnum = 5;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          lineEl.setGeometry(baseptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;
          pt =ptarr.project(statept);
        
          pointEl.setGeometry(pt);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
  

  cout <<"========================================" << endl;
  testname = "Testing point2arr.get_side(const point2& pt, int& side, double& index)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     
   
    int sideval, indexval;
    int inputnum = 3; 
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
          ptarroff = baseptarr.get_offset(-3);        

          lineEl.setGeometry(baseptarr);
          lineEl1.setGeometry(ptarroff);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;
          ptarroff = baseptarr.get_offset(-3);

          ptarr.get_side(statept,sideval, indexval);
          cout <<"-----ORIG Sideval = " << sideval<< "  Indexval = " << indexval << endl;
          //   ptarroff.get_side(statept,sideval, indexval);
          //   cout <<"-----OFFSET Sideval = " << sideval<< "  Indexval = " << indexval << endl;


          pointEl.setGeometry(statept);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
      
  
  
  cout <<"========================================" << endl;
  testname = "Testing line intersect (line), is_intersect(line)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool firstline = true;
    bool firstpoint = true;
    
    while (true){
 
        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstline){
          if (firstpoint){
            statept = recvEl.center;
            line1.ptA = statept;
            lineEl.setGeometry(statept);
            maptalker.sendMapElement(&lineEl,sendSubGroup);

            firstpoint = false;
          }else{
            statept = recvEl.center;
            line1.ptB = statept;
            ptarr = line1;
            lineEl.setGeometry(ptarr);
            maptalker.sendMapElement(&lineEl,sendSubGroup);
            firstline = false;
            firstpoint = true;
          }


        }else {
          if (firstpoint){
            statept = recvEl.center;
            line2.ptA = statept;
            lineEl1.setGeometry(statept);
            maptalker.sendMapElement(&lineEl1,sendSubGroup);
            
            firstpoint = false;
          }else{
            statept = recvEl.center;
            line2.ptB = statept;
            ptarr = line2;
            lineEl1.setGeometry(ptarr);
            maptalker.sendMapElement(&lineEl1,sendSubGroup);
            
            pt = line1.intersect(line2);
            cout << "INTERSECTION point at " << pt << endl;
            bool val = line1.is_intersect(line2);
            cout << "IS_INTERSECT = " << val << endl;
            pointEl.setGeometry(pt);
            maptalker.sendMapElement(&pointEl,sendSubGroup);
          }
        }
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  
 
  cout <<"========================================" << endl;
  testname = "Testing point2arr.cut front( double)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool cutfront = true;
    int inputnum = 5;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          lineEl.setGeometry(baseptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          double val = statept.x;
          ptarr = baseptarr;        
          if (cutfront){
            cout << "CUTTING FRONT val = " << val  << endl;
            ptarr.cut_front(val);
            cutfront=false;
          }else{
            cout << "CUTTING BACK val = " << val  << endl;
            ptarr.cut_back(val);
            cutfront = true;
          }
          lineEl1.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
    

   
  cout <<"========================================" << endl;
  testname = "Testing point2arr.cut front( point)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool cutfront = true;
    int inputnum = 5;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          lineEl.setGeometry(baseptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr = baseptarr;        
          if (cutfront){
            cout << "CUTTING FRONT point = " << statept  << endl;
            ptarr.cut_front(statept);
            cutfront=false;
          }else{
            cout << "CUTTING BACK point = " << statept  << endl;
            ptarr.cut_back(statept);
            cutfront = true;
          }
          lineEl1.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  }
    

   
 
  cout <<"========================================" << endl;
  testname = "Testing ptarr connect, connect_intersect (ptarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    bool firstptarr = true;
    bool firstpoint = true;
    bool connectflag = true;
    while (true){
 
        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstptarr){
          if (firstpoint){
            statept = recvEl.center;
            ptarr1.push_back(statept);
            lineEl.setGeometry(statept);
            maptalker.sendMapElement(&lineEl,sendSubGroup);

            firstpoint = false;  
          }else{
            statept = recvEl.center;
            ptarr1.push_back(statept);
            lineEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&lineEl,sendSubGroup);
            firstptarr = false;
            firstpoint = true;
          }

 
        }else {
          if (firstpoint){
            statept = recvEl.center;
            ptarr2.clear();
            ptarr2.push_back(statept);
            lineEl1.setGeometry(statept);
            maptalker.sendMapElement(&lineEl1,sendSubGroup);
            
            firstpoint = false;
          }else{
            statept = recvEl.center;
            ptarr2.push_back(statept);
            
            lineEl1.setGeometry(ptarr2);
            maptalker.sendMapElement(&lineEl1,sendSubGroup);
            
            ptarr = ptarr1;
            if (connectflag){
              cout << "connect" << endl;
              ptarr.connect(ptarr2);
              connectflag = false;
            }
            else{ 
              cout << "connect_intersect" << endl;
              ptarr.connect_intersect(ptarr2);
              connectflag = true;
 
            }
            lineEl2.setGeometry(ptarr);
            maptalker.sendMapElement(&lineEl2,sendSubGroup);
            firstpoint = true;
          }
        }
      } 
      else{ 
        usleep (100000);
      }
    }
  }
  
 
  cout <<"========================================" << endl;
  testname = "Testing point2arr.get_bound_box(const point2arr& ptarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    int inputnum = 5;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          lineEl.setGeometry(baseptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          
          inputnum--;
        }else{
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          ptarr =baseptarr.get_bound_box();
        
          polyEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          baseptarr.clear();
          inputnum =5;
        } 
      }
      else{ 
        usleep (100000);
      }
    }
  } 
  

 

  cout <<"========================================" << endl;
  testname = "Testing point2arr.split(double )";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

    vector<point2arr> ptarrarr;
    int inputnum = 8;
    
    while (true){

        
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (inputnum>0){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          baseptarr.push_back(statept);
        
          lineEl.setGeometry(baseptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          
          inputnum--;
        }else{
          
          statept = recvEl.center;
          cout << endl <<"For point " << statept << endl;
          cout << "splitting based on " << statept.x << endl;
          ptarrarr =baseptarr.split(statept.x);
          cout << "got size " << ptarrarr.size() << endl;
          for (unsigned j = 0; j< ptarrarr.size();++j){
            ptarr = ptarrarr[j];
            polyEl.setGeometry(ptarr);
            polyEl.setId(-2-j);
            maptalker.sendMapElement(&polyEl,sendSubGroup);
          }
       

        } 
      }
      else{ 
        usleep (100000);
      }
    }
  } 
  

  cout <<"========================================" << endl;
  testname = "Testing point2arr.is_intersect(pointarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 5;

    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Point " << statept << endl;
            ptarr1.push_back(statept);
            lineEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&lineEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
          
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          lineEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          
          cout << endl <<"CHECKING INTERSECTION" << endl;
          
          if (ptarr1.is_intersect(ptarr2)){
            cout << "intersect 1->2 = TRUE " << endl;
          }else{
            cout << "intersect 1->2 = FALSE " << endl;
          }
          if (ptarr2.is_intersect(ptarr1)){
            cout << "intersect 2->1 = TRUE " << endl;
          }else{
            cout << "intersect 2->1 = FALSE " << endl;
          }
          
          
          tmpinputnum--;
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear();
          }
        }
      }
      else{ 
        usleep (100000);
      }
    }
  } 
        
   
  cout <<"========================================" << endl;
  testname = "Testing point2arr.is_poly_overlap(pointarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 7;

    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Point " << statept << endl;
            ptarr1.push_back(statept);
            polyEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&polyEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
           
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          polyEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&polyEl1,sendSubGroup);
          
          cout << endl <<"CHECKING OVERLAP" << endl;
       
          if (ptarr1.is_poly_overlap(ptarr2)){
            cout << "overlap 1->2 = TRUE " << endl;
          }else{
            cout << "overlap 1->2 = FALSE " << endl;
          }
          if (ptarr2.is_poly_overlap(ptarr1)){
            cout << "overlap 2->1 = TRUE " << endl;
          }else{
            cout << "overlap 2->1 = FALSE " << endl;
          }
          
          tmpinputnum--;
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear();
          }
        }
      }
      else{ 
        usleep (100000);
      }
    }
  }
     
 cout <<"========================================" << endl;
  testname = "Testing point2arr.is_line_poly_overlap is_poly_line_overlap";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 7;

    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Point " << statept << endl;
            ptarr1.push_back(statept);
            polyEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&polyEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
           
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          lineEl.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          
          cout << endl <<"CHECKING OVERLAP" << endl;
       
          if (ptarr1.is_poly_line_overlap(ptarr2)){
            cout << "overlap 1->2 = TRUE " << endl;
          }else{
            cout << "overlap 1->2 = FALSE " << endl;
          }
          if (ptarr2.is_line_poly_overlap(ptarr1)){
            cout << "overlap 2->1 = TRUE " << endl;
          }else{
            cout << "overlap 2->1 = FALSE " << endl;
          }
          
          tmpinputnum--;
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear();
          }
        }
      }
      else{ 
        usleep (100000);
      }
    } 
  }
     
 cout <<"========================================" << endl;
  testname = "Testing point2arr.is_point_poly_overlap is_poly_point_overlap";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 7;

    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Point " << statept << endl;
            ptarr1.push_back(statept);
            polyEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&polyEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
           
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          pointEl.setGeometry(ptarr2);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
          
          cout << endl <<"CHECKING OVERLAP" << endl;
       
          if (ptarr1.is_poly_point_overlap(ptarr2)){
            cout << "overlap 1->2 = TRUE " << endl;
          }else{
            cout << "overlap 1->2 = FALSE " << endl;
          }
          if (ptarr2.is_point_poly_overlap(ptarr1)){
            cout << "overlap 2->1 = TRUE " << endl;
          }else{
            cout << "overlap 2->1 = FALSE " << endl;
          }
          
          tmpinputnum--;
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear();
          }
        }
      }
      else{ 
        usleep (100000);
      }
    }
  }

 cout <<"========================================" << endl;
  testname = "Testing point2arr.get_min_dist_points";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 7;
    double dist;
    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Point " << statept << endl;
            ptarr1.push_back(statept);
            pointEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&pointEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
           
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          pointEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&pointEl1,sendSubGroup);
          
          cout << endl <<"CHECKING MIN DIST" << endl;
          dist = ptarr1.get_min_dist_points(ptarr2);
          cout << "dist 1->2 = " << dist <<endl; 
          dist = ptarr2.get_min_dist_points(ptarr1);
          cout << "dist 2->1 = " << dist <<endl; 
          
          tmpinputnum--;
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear();
          }
        }
      }
      else{ 
        usleep (100000);
      }
    }
  }


 cout <<"========================================" << endl;
  testname = "Testing point2arr.get_min_dist_poly_line";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 7;
    double dist;
    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Line Point " << statept << endl;
            ptarr1.push_back(statept);
            lineEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&lineEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
           
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Poly Point " << statept << endl;
          ptarr2.push_back(statept);
          polyEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&polyEl1,sendSubGroup);
          
          cout << endl <<"CHECKING MIN DIST " << endl;
          dist = ptarr2.get_min_dist_poly_line(ptarr1);
          cout << "dist 1->2 = " << dist <<endl; 
          dist = ptarr1.get_min_dist_line_poly(ptarr2);
          cout << "dist 2->1 = " << dist <<endl; 
          
          tmpinputnum--;
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear();
          }
        }
      }
      else{ 
        usleep (100000);
      }
    }
  }
 cout <<"========================================" << endl;
  testname = "Testing point2arr.get_max_dist_points";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    int inputnum = 4;
    double dist;
    int inputnum2 = 4;
    int tmpinputnum = inputnum2;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        if (firstpoly){
          
            statept = recvEl.center;
            cout << endl <<"Input Point " << statept << endl;
            ptarr1.push_back(statept);
            polyEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&polyEl,sendSubGroup);
            lineEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&lineEl,sendSubGroup);
            pointEl.setGeometry(ptarr1);
            maptalker.sendMapElement(&pointEl,sendSubGroup);
            inputnum--;
            if (inputnum<=0){          
              firstpoly = false;
            } 
            
        }else{
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          polyEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&polyEl1,sendSubGroup);
          lineEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          pointEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&pointEl1,sendSubGroup);
          
          cout << endl <<"CHECKING MAX DIST POINTS" << endl;
          dist = ptarr1.get_max_dist_points(ptarr2);
          cout << "dist 1->2 = " << dist <<endl; 
          dist = ptarr2.get_max_dist_points(ptarr1);
          cout << "dist 2->1 = " << dist <<endl; 
           cout << endl <<"CHECKING MAX DIST LINE" << endl;
          dist = ptarr1.get_max_dist_line(ptarr2);
          cout << "dist 1->2 = " << dist <<endl; 
          dist = ptarr2.get_max_dist_line(ptarr1);
          cout << "dist 2->1 = " << dist <<endl; 
           cout << endl <<"CHECKING MAX DIST POLY" << endl;
          dist = ptarr1.get_max_dist_poly(ptarr2);
          cout << "dist 1->2 = " << dist <<endl; 
          dist = ptarr2.get_max_dist_poly(ptarr1); 
          cout << "dist 2->1 = " << dist <<endl; 
           
 

          tmpinputnum--; 
          if (tmpinputnum<=0){
            tmpinputnum =inputnum2;
            ptarr2.clear(); 
          }
        } 
      }
      else{ 
        usleep (100000); 
      }
    } 
  } 
 



 cout <<"========================================" << endl;
  testname = "Testing point2arr.get_poly_ difference,intersection,xor,union ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    bool firstpoly = true;
    bool secondpoly = false;
    int clipval=0;
    int inputnum = 7;
    int inputnum2 = 3;
    int tmpinputnum = inputnum2;
    int retval = 0;
    vector<point2arr> ptarrarr;
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){ 
        if (firstpoly){ 
          
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr1.push_back(statept);
          polyEl.setGeometry(ptarr1);
          maptalker.sendMapElement(&polyEl,sendSubGroup);
          inputnum--;
          if (inputnum<=0){          
            firstpoly = false;
            secondpoly=true;
          } 
          
        }else if (secondpoly){
          statept = recvEl.center;
          cout << endl <<"Input Point " << statept << endl;
          ptarr2.push_back(statept);
          polyEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&polyEl1,sendSubGroup);    
          inputnum2--;
            if (inputnum2<=0){          
              secondpoly=false;
            } 
          
        }else{
          switch (clipval){
          case 0:
            cout << endl <<"CHECKING POLY DIFFERENCE" << endl;
            retval = ptarr1.get_poly_difference(ptarr2,ptarrarr);
          clipval++;
            break;
          case 1:
            cout << endl <<"CHECKING POLY INTERSECTION" << endl;
            retval = ptarr1.get_poly_intersection(ptarr2,ptarrarr);
          clipval++;
            break;
          case 2:
            cout << endl <<"CHECKING POLY XOR" << endl;
            retval = ptarr1.get_poly_xor(ptarr2,ptarrarr);
          clipval++;
            break;
          case 3:
            cout << endl <<"CHECKING POLY UNION" << endl;
            retval = ptarr1.get_poly_union(ptarr2,ptarrarr);
            clipval = 0; 
            break;
           
          }
 
          for (int i = 0; i<10; ++i){
            polyEl2.setId(-8,i);
            if (i<retval){
              polyEl2.setGeometry(ptarrarr[i]);
              maptalker.sendMapElement(&polyEl2,sendSubGroup);
            }else{
              clearEl.setId(-8,i);
              maptalker.sendMapElement(&clearEl,sendSubGroup);
            }
            
          }
        }       
      }
      else{ 
        usleep (100000);
      }
    }
  }



 cout <<"========================================" << endl;
  testname = "Testing point2arr.get_poly_area ";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
     int clipval=0;
    int baseinputnum = 7;
    int inputnum = baseinputnum;
    double area;
     int retval = 0;
     while (true){
       if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){ 
         statept = recvEl.center;
      
         ptarr1.push_back(statept);
         polyEl.setGeometry(ptarr1);
         maptalker.sendMapElement(&polyEl,sendSubGroup);
         
         area = ptarr1.get_poly_area();
         cout << "POLY AREA = " << area << endl;
         
         inputnum--;
         if (inputnum<=0){          
           inputnum = baseinputnum;
           ptarr1.clear();
         } 
         
       }
       else{ 
         usleep (100000);
       }
     }
  }
   
  


  cout <<"========================================" << endl;
  testname = "Testing point2arr.set_arc_curve(pt,pt,pt,int)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

     while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){


        statept = recvEl.center;
        cout << endl <<"Input Point " << statept << endl;
        ptarr1.push_back(statept);
        pointEl.setGeometry(ptarr1);
        maptalker.sendMapElement(&pointEl,sendSubGroup);
        if (ptarr1.size()>=3){          
          ptarr.set_arc_curve(ptarr1[0],ptarr1[1],ptarr1[2],25);
          cout <<"ptarr arc = " <<  ptarr << endl;
          ptarr3.connect(ptarr);

          lineEl.setGeometry(ptarr3);

          ptarr2 = ptarr3.get_offset(5);
          lineEl1.setGeometry(ptarr2);
          ptarr2.set_trim_line_loops(10);
          lineEl2.setGeometry(ptarr2);
 
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          maptalker.sendMapElement(&lineEl2,sendSubGroup);

          
          pt = ptarr1[2];
          ptarr1.clear()  ; 
          ptarr1.push_back(pt);
        } 
      }
      else{ 
        usleep (100000);
      }
    } 
  } 
 
   
  
  
  //--------------------------------------------------
  // these need some cleanup 
  //--------------------------------------------------

  cout <<"========================================" << endl;
  testname = "Testing point2arr.average()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    point2 avept, tmppt;
    double ssxx, ssxy, ssyy;

    int runtimes = max(2,testarg);
    int inputnum = runtimes;
    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > 0) {
          ptarr.push_back(pt);
          lineEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	} else {
	  avept = ptarr.average();
	  tmppt = ptarr[0];

	  cout<< "Sum Squares "<<ssxx<<' '<<ssxy<<' '<<ssyy<<endl;
	  ptarr.sum_squares(ssxx, ssxy, ssyy);
	  cout<< "Sum Squares "<<ssxx<<' '<<ssxy<<' '<<ssyy<<endl;

	  ptarr.push_back(tmppt);
	  pointEl.setGeometry(avept);
	  lineEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&pointEl,sendSubGroup);
	  ptarr.clear();
	  inputnum = runtimes;
	}
      }else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing point2arr.fitline()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;

  
    point2 end1, end2;
    point2arr linearr;

    double a,b, MSE;

    int runtimes = max(2,testarg);
    cout<<"pick "<<runtimes<<" points"<<endl;
    int inputnum = runtimes;
    //    int inputnum = 2;    

    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > 0) {
          ptarr.push_back(pt);
          lineEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	} else {
	  MSE = ptarr.fit_line(a, b, end1, end2);
	  linearr.push_back(end1);
	  linearr.push_back(end2);
	  cout<<"Error: "<<MSE<<endl;
	  lineEl1.setGeometry(linearr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);

	  ptarr.clear();
	  linearr.clear();
	  inputnum =runtimes;
	}
      }else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing point2arr.get_inside()";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    int objsize = max(3,testarg);
    cout<<"pick "<<objsize<<" points"<<endl;
    int inputnum = objsize;

    int inside;

    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > 0) {
          ptarr.push_back(pt);
	  if(inputnum == 1)
	    ptarr.push_back(ptarr[0]);

          lineEl.setGeometry(ptarr);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	} else {
	  inside = ptarr.get_inside(pt);
	  cout<<"get_inside returned inside = "<<inside<<endl;
	  lineEl1.setGeometry(pt);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);

          inputnum--;
	}
      }else{ 
        usleep (100000);
      }
    }
  }

  cout <<"========================================" << endl;
  testname = "Testing point2arr.merge(ptarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    int objsize = max(3,testarg);
    cout<<"pick "<<objsize<<" points"<<endl;
    int inputnum = 2*objsize;

    
    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > objsize) {
          ptarr1.push_back(pt);
	  ptarr3.push_back(pt);
	  if(inputnum == objsize + 1)
	    ptarr1.push_back(ptarr1[0]);

          lineEl.setGeometry(ptarr1);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	} else if(inputnum > 0) {
          ptarr2.push_back(pt);

          lineEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          inputnum--;
	 } else {
	   ptarr3.merge(ptarr2);
	   ptarr3.push_back(ptarr3[0]);
	  lineEl2.setGeometry(ptarr3);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          maptalker.sendMapElement(&lineEl2,sendSubGroup);

	  for(int i = 0; i < objsize; i++) {
	    int foo = ptarr1.get_inside(ptarr2[i]);
	    int bar = ptarr1.get_closest_side(ptarr2[i]);
	    cout<<"for pt "<<i<<" of arr2, ptarr1.get_inside returns: "<<foo<<" for index: "<<bar<<endl;
	  }

	  ptarr1.clear();
	  ptarr2.clear();
	  ptarr3.clear();
	  inputnum = 2*objsize;
	}
      }else{ 
        usleep (100000);
      }
    }
  }


  cout <<"========================================" << endl;
  testname = "Testing point2arr.overlap(ptarr)";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    int objsize = max(3,testarg);
    cout<<"pick "<<objsize<<" points"<<endl;
    int inputnum = 2*objsize;

    int overlap1;

    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > objsize) {
          ptarr1.push_back(pt);

          lineEl.setGeometry(ptarr1);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	} else if(inputnum > 0) {
          ptarr2.push_back(pt);

          lineEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          inputnum--;
	 } else {
	   ptarr1.push_back(ptarr1[0]);
	   ptarr2.push_back(ptarr2[0]);
	   overlap1 = ptarr1.get_overlap(ptarr2);
	   cout<<"overlaps: "<<overlap1<<endl;
	  ptarr1.clear();
	  ptarr2.clear();
	  inputnum = 2*objsize;
	}
      }else{ 
        usleep (100000);
      }
    }
  }


  cout <<"========================================" << endl;
  testname = "Testing ellipse initialization";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
    
    int objsize = max(3,testarg);
    cout<<"pick "<<objsize<<" points to fit ellipse"<<endl;
    int inputnum = 2*objsize;

    point2arr elpts;
    ellipse el;

    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > objsize) {
          ptarr1.push_back(pt);

          lineEl.setGeometry(ptarr1);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	  if(inputnum == objsize) {
	    el = ellipse(ptarr1);
	    cout<<"new ellipse. center = "<<el.center.x<<' '<<el.center.y<<" major axis = "<<el.a<<" minor axis = "<<el.b<<" rotation = "<<el.theta<<endl;
	    elpts.clear();
	    elpts = el.border();
	    lineEl2.setGeometry(elpts);
	    maptalker.sendMapElement(&lineEl2, sendSubGroup);
	  }
	} else if(inputnum > 0) {
          ptarr2.push_back(pt);
	  ptarr1.push_back(pt);
          lineEl1.setGeometry(ptarr2);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          inputnum--;

	  if(inputnum == 0) {
	    el.add_points(ptarr2);
	    cout<<"added points. new center = "<<el.center.x<<' '<<el.center.y<<" major axis = "<<el.a<<" minor axis = "<<el.b<<" rotation = "<<el.theta<<endl;
	    elpts = el.border();
	    lineEl2.setGeometry(elpts);
	    maptalker.sendMapElement(&lineEl2, sendSubGroup);
  	    ptarr2.clear();
	    inputnum = objsize;
	  }

	}
      }else{ 
        usleep (100000);
      }
    }
  }


  cout <<"========================================" << endl;
  testname = "Testing inside ellipse";
  thistest++;
  testflag = (thistest==testnum || 
              (testnum<0 &&-thistest<=testnum));
  cout <<"Test #" << thistest << "  " << testname;
  if (!testflag)
    cout << "  NOT RUN" << endl;
  else{
    cout << "  RUNNING" << endl;
   
    int objsize = max(3,testarg);
    cout<<"pick "<<objsize<<" points to fit ellipse"<<endl;
    int inputnum = objsize;

   
    point2arr  elpts;
    ellipse el;

    while (true){
      if (maptalker.recvMapElementNoBlock(&recvEl,recvSubGroup)){
        pt = recvEl.center;
        cout << endl <<"Input Point " << pt << endl;

	if(inputnum > 0) {
          ptarr1.push_back(pt);

          lineEl.setGeometry(ptarr1);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          inputnum--;
	  if(inputnum == 0) {
	    el = ellipse(ptarr1);
	    cout<<"new ellipse. center = "<<el.center.x<<' '<<el.center.y<<" major axis = "<<el.a<<" minor axis = "<<el.b<<" rotation = "<<el.theta<<endl;
	    elpts.clear();
	    elpts = el.border();
	    lineEl2.setGeometry(elpts);
	    maptalker.sendMapElement(&lineEl2, sendSubGroup);
	  }
	} else {
	  int inside = el.inside(pt);
	  cout<<"inside returns "<<inside<<" for new pt"<<endl;

          lineEl1.setGeometry(pt);
          maptalker.sendMapElement(&lineEl,sendSubGroup);
          maptalker.sendMapElement(&lineEl1,sendSubGroup);
          inputnum--;

	}
      }else{ 
        usleep (100000);
      }
    }
  }





 
 

  
  if (0){ 
    //--------------------------------------------------
    // starttemplate for new test
    //--------------------------------------------------
   
    cout <<"========================================" << endl;
    testname = "Testing ";
    thistest++;
    testflag = (thistest==testnum || 
                (testnum<0 &&-thistest<=testnum));
    cout <<"Test #" << thistest << "  " << testname;
    if (!testflag)
      cout << "  NOT RUN" << endl;
    else{
      cout << "  RUNNING" << endl;
          
    }

    //--------------------------------------------------
    // end template for new test
    //--------------------------------------------------   
  }
 
 
 
  cout <<"========================================" 
       << endl << endl;
  return 0;


}  
