/**********************************************************
 **
 **  MAPID.HH
 **
 **    Time-stamp: <2007-05-18 18:50:16 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Sat Mar 10 08:31:31 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/


#ifndef MAPID_H
#define MAPID_H

#include <math.h>
#include <vector>
#include <iostream>

using namespace std;
//! CLASS DESCRIPTION
/*! CLASS DETAILS */
class MapId
{
  
public:

  vector<int> dat;
  //! A Constructor 
  /*! DETAILS */

  MapId() {dat.clear();}
  MapId(const MapId& id) {dat = id.dat;}
  MapId(const vector<int> id) {dat = id;}
  MapId(const int id) {dat.clear(); dat.push_back(id);}
  MapId(const int id1,const int id2) {dat.clear(); dat.push_back(id1); dat.push_back(id2);}
  MapId(const int id1,const int id2,const int id3) {dat.clear(); dat.push_back(id1); dat.push_back(id2); dat.push_back(id3);}
  MapId(const int id1,const int id2,const int id3,const int id4) {dat.clear(); dat.push_back(id1); dat.push_back(id2); dat.push_back(id3); dat.push_back(id4);}

  MapId(const int id1,const int id2,const int id3,const int id4, const int id5) {dat.clear(); dat.push_back(id1); dat.push_back(id2); dat.push_back(id3); dat.push_back(id4);dat.push_back(id5);}

    
  //! A Destructor 
  /*! DETAILS */
  ~MapId() {}
  
  void resize(unsigned int size) {dat.resize(size);}
	void clear() {dat.clear();}
	void push_back(const int &id) {dat.push_back(id);}
  void pop_back() {dat.pop_back();}
	unsigned int insert(unsigned int n, int id);

	int &operator[](unsigned int n);
	const int &operator[](unsigned int n) const;
	unsigned int size() const {return dat.size();}

	int back() {return this->dat.back();}
  int front() {return this->dat.front();}

	bool operator== (const MapId& id) const;
	bool operator!= (const MapId& id) const ;
	bool operator== (const int id) const;
	bool operator!= (const int id) const ;

  bool match(const MapId &id) const;
  bool match(const int id) const;
  bool match(const int id1,const int id2) const;
  bool match(const int id1,const int id2,const int id3) const;
  bool match(const int id1, const int id2, const int id3, const int id4) const;
  bool match(const int id1, const int id2, const int id3, const int id4, const int id5) const;



	MapId &operator=(int id1){
    this->clear();
    this->push_back(id1);
		return *this;
	}

	MapId &operator=(const MapId &id){
		if (this!= &id){
			this->dat = id.dat;
		}
		return *this;
	}


	MapId &operator=(const vector<int> &id){
		this->dat = id;
		return *this;
  }

	void set(int id1){
    clear();
    push_back(id1);
	}
	void set(int id1,int id2){
    clear();
    push_back(id1);
    push_back(id2);
	}
	void set(int id1,int id2,int id3){
    clear();
    push_back(id1);
    push_back(id2);
    push_back(id3);
	}
	void set(int id1,int id2,int id3,int id4){
    clear();
    push_back(id1);
    push_back(id2);
    push_back(id3);
    push_back(id4);
	}
void set(int id1,int id2,int id3,int id4,int id5){
    clear();
    push_back(id1);
    push_back(id2);
    push_back(id3);
    push_back(id4);
    push_back(id5);
	}


	void set(const MapId& id){
    dat = id.dat;
	}

	void set(const vector<int> id){
    dat = id;
	}




};

ostream &operator<<(ostream &os, const MapId &id);

class MapIdArr
{
public:
  vector<MapId> arr;

  MapIdArr() {arr.clear();}
  MapIdArr(const MapIdArr &idarr){
    arr = idarr.arr;
  }
  MapIdArr(const vector<MapId> &idarr){
    arr =idarr;
  }

  MapIdArr(const MapId &ida, const MapId &idb){
    arr.push_back(ida);
    arr.push_back(idb);
  }

  MapIdArr(const MapId &ida){
    arr.push_back(ida);
  }

  ~MapIdArr() {}
  
  void resize(unsigned int size) {arr.resize(size);}
  void clear() {arr.clear();}
  void push_back(const MapId &id) {arr.push_back(id);}
  unsigned int insert(unsigned int n, MapId &id); 

  MapId &operator[](unsigned int n);
  const MapId &operator[](unsigned int n) const;
  unsigned int size() const {return arr.size();}
  MapId& back() {
    return this->arr.back();
  }


  MapIdArr &operator=(const MapIdArr &idarr){
    if (this!= &idarr){
      this->arr = idarr.arr;
    }
    return *this;
  }

  MapIdArr &operator=(const vector<MapId> &idarr){
    this->arr = idarr;
    return *this;
  }

  bool operator== (const MapIdArr& idarr) const;

  bool operator!= (const MapIdArr& idarr) const ;

  bool getIndex(int &index, const MapId & id) const;

  bool getIndex(MapIdArr &indexarr, const MapIdArr & idarr) const;



};

#endif
