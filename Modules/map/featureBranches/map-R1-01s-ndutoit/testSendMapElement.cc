/**********************************************************
 **
 **  TESTMAPELEMENTTALKER.CC
 **
 **    Time-stamp: <2007-03-11 11:29:04 sam> 
 **
 **    Author: Sam Pfister
 **    Created: Wed Feb  7 13:39:57 2007
 **
 **
 **********************************************************
 **
 **  
 **
 **********************************************************/

#include <iostream>
#include "MapElementTalker.hh"
#include "MapElement.hh"
#include "interfaces/sn_types.h"


using namespace std;


class CTestSendMapElement : public CMapElementTalker{

public:
  /*! Constructor */
  CTestSendMapElement(){}
      
  /*! Standard destructor */
  ~CTestSendMapElement() {}

};

int main(int argc, char **argv)
{
  int skynetKey = 0;
  int subgroup = 0;
  
  skynetKey = atoi(getenv("SKYNET_KEY"));  

  cout << endl << "Usage :" << endl;
  cout << " testSendMapElement [skynet key] [subgroup]" << endl << endl;

  if(argc >1)
    subgroup = atoi(argv[1]);
  if(argc >2)
    skynetKey = atoi(argv[2]);
  
  cout << "Skynet Key = " << skynetKey << 
    "  Subgroup = "  << subgroup << endl;

  int bytesSent = 0;


  CTestSendMapElement testtalker;
  testtalker.initSendMapElement(skynetKey);
  MapElement el;


  int id_input;
  vector<int> id;
  point2 cpoint(14,25);

  
  vector<point2> ptarr;
  ptarr.push_back(cpoint+point2(-3,3));
  ptarr.push_back(cpoint+point2(-2,0));
  ptarr.push_back(cpoint+point2(-1,-2));
  ptarr.push_back(cpoint+point2(1,-1));
  ptarr.push_back(cpoint+point2(3,3));
  
  double angle = .2;
  double radius = 3;
  double length = 4;
  double width = 2;
  
  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending obstacle, radius bound" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  
  id.push_back(id_input);
  el.clear();
  el.set_circle_obs(id,cpoint,radius);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending obstacle, block bound" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_block_obs(id,cpoint,angle,length,width);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending obstacle, polygon bound" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_poly_obs(id,ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending generic line" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_line(id,ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending laneline" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_laneline(id,ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;


  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending stop line" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_stopline(id,ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending points" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_points(id,ptarr);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending manually built element" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;

  el.clear();
  el.id.push_back(id_input);
  el.id.push_back(12);
  el.id.push_back( 1);
  el.center = cpoint;
  el.center.max_var = 2;
  el.center.min_var = 1;
  el.center.axis = 2*angle;
  el.length = length;
  el.width = width;
  el.orientation =angle;
  el.label.push_back("MapElement created in testSendMapElement");
  el.label.push_back("to be sent over skynet");
  
  for (int i = 0; i < 20 ; ++i){
    el.geometry.push_back(point2_uncertain(el.center.x+0.5*((double)i-10),
                                            el.center.y-2+.05*pow((double)i-9.5,2), 4, 2, .2));
  }
  el.state.timestamp = 4;
  

  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();


  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending Alice" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  VehicleState state;
  memset(&state, 0, sizeof(state));
  state.localX = 10;
  state.localY = 15;
  state.localYaw = 1;
  

  el.clear();
  el.set_alice(state);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;

  /////////////////////////////////////
  cout << "====================================" << endl;
  cout << "Sending clear" << endl;
  cout << "Enter id number and hit enter to send" << endl;
  cin>>id_input;
  id.clear();
  id.push_back(id_input);

  el.clear();
  el.set_clear(id);
  bytesSent = testtalker.sendMapElement(&el,subgroup);
  el.print();
  cout << "bytes sent = " << bytesSent << endl;


  return(0);
}

