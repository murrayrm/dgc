Sat Mar 17 11:21:01 2007	Sam Pfister (sam)

	* version R1-01p-sam
	BUGS: 
	FILES: MapElement.cc(18258), MapElement.hh(18258),
		MapPrior.cc(18258), testGeometry.cc(18258),
		testMap.cc(18258)
	changed color enums from COLOR_RED to MAP_COLOR_RED to avoid some
	collision when compiling lineperceptor

Sat Mar 17  0:47:13 2007	Sam Pfister (sam)

	* version R1-01p
	BUGS: 
	FILES: Map.cc(18177), Map.hh(18177), testGeometry.cc(18177),
		testMap.cc(18177)
	Added getLaneCenterPoint, getLaneRightPoint, and getLaneLeftPoint
	functions for tplanner queries.  Added functions to test map
	queries and geometry operations.

Fri Mar 16  0:27:21 2007	Sam Pfister (sam)

	* version R1-01o
	BUGS: 
	FILES: Map.cc(17906), Map.hh(17906), testMap.cc(17906)
	added new getHeading function which gets an estimate of the lane
	heading at some point if that point is inside valid lane bounds.

Thu Mar 15 13:59:39 2007	Sam Pfister (sam)

	* version R1-01n
	BUGS: 
	FILES: Map.cc(17802), Map.hh(17802)
	added addElement function which adds an element if the id is new
	and updates the element if it has been seen.

Wed Mar 14 15:57:50 2007	Sam Pfister (sam)

	* version R1-01m
	BUGS: 
	FILES: Map.cc(17594), Map.hh(17594), testMap.cc(17594)
	added getTransitionBounds and getBounds functions which are more
	functional and useful for tplanner than existing versions.  Added
	getHeading to get the heading angle at a waypoint.  Added
	getBoundsReverse for getting lanebounds in reverse.

Wed Mar 14 10:14:48 2007	Sam Pfister (sam)

	* version R1-01l
	BUGS: 
	FILES: Map.cc(17568), Map.hh(17568), testGeometry.cc(17568),
		testMap.cc(17568)
	Added getObstaclePoint(offset) function for tplanner use which
	returns a point offset from closest obstacle in lane by specified
	amount

	FILES: testGeometry.cc(17548), testMap.cc(17548)
	more geometric test functions

Tue Mar 13 15:45:16 2007	Sam Pfister (sam)

	* version R1-01k
	BUGS: 
	New files: testGeometry.cc
	FILES: Makefile.yam(17469), Map.cc(17469), Map.hh(17469),
		MapElement.cc(17469), MapElement.hh(17469),
		MapPrior.cc(17469), rndf.txt(17469), testMap.cc(17469)
	Added testGeometry function to test and visualize 2D geometric
	functions.  Implemented getObstacleDist() which given a point in
	local frame returns the distance to nearest obstacle in current
	lane in front of Alice.

Mon Mar 12 15:22:21 2007	Sam Pfister (sam)

	* version R1-01j
	BUGS: 
	FILES: Map.cc(17247), Map.hh(17247), MapElement.cc(17247),
		MapPrior.cc(17247), testMap.cc(17247)
	added getNextStopline function.  Need to debug an RNDF reversing
	issue

	FILES: MapId.cc(17226), MapId.hh(17226), testMapId.cc(17226)
	updated testMapId program and MapId class functionality

Sun Mar 11 11:49:13 2007	Sam Pfister (sam)

	* version R1-01i
	BUGS: 
	New files: testMapElement.cc testMapId.cc
	FILES: MapElement.cc(17098), MapElement.hh(17098),
		MapElementTalker.cc(17098), testSendMapElement.cc(17098)
	Added clear and alice state messages to map element.

Sat Mar 10 17:22:05 2007	Sam Pfister (sam)

	* version R1-01h
	BUGS: 
	New files: MapId.cc MapId.hh testMap.cc
	Deleted files: test_map.cc
	FILES: Makefile.yam(17025), Map.cc(17025), Map.hh(17025),
		MapElement.hh(17025), MapElementTalker.cc(17025),
		MapElementTalker.hh(17025), MapPrior.cc(17025),
		MapPrior.hh(17025), testRecvMapElement.cc(17025),
		testSendMapElement.cc(17025)
	removed tabs in place of spaces.  Fixed bug in getStopline
	function.  Moved test_map.cc to testMap.cc.

Fri Mar  9 16:33:29 2007	Sam Pfister (sam)

	* version R1-01g
	BUGS: 
	FILES: Map.cc(16900), Map.hh(16900), test_map.cc(16900)
	added isStop and isExit query functions to map.

Fri Mar  9 12:15:02 2007	Sam Pfister (sam)

	* version R1-01f
	BUGS: 
	New files: MapElement.cc MapElement.hh MapElementTalker.cc
		MapElementTalker.hh testRecvMapElement.cc
		testSendMapElement.cc
	FILES: Makefile.yam(16849), Map.hh(16849), MapPrior.hh(16849),
		test_map.cc(16849)
	Moved MapElementTalker, testSendMapElement, testRecvMapElement, and
	MapElement.hh to map module.  This is part of a general
	restructuring which moves MapElement functionality into map.  All
	users of the MapElementTalker will need to include
	map/MapElementTalker.hh and map/MapElement.hh instead of
	skynettalker/MapElementTalker and interfaces/MapElement.hh.  Also
	-lmap should be added to LIBS in the Makefile.yam if it isn't there
	already 

Thu Mar  8 18:32:56 2007	Noel duToit (ndutoit)

	* version R1-01e
	BUGS: 
	FILES: Map.cc(16816), Map.hh(16816), rndf.txt(16816),
		test_map.cc(16816)
	Some updates in map to interface with new tplanner.

	FILES: MapPrior.cc(16751), test_map.cc(16751)
	local changes by Sam

Sat Mar  3 15:55:28 2007	Sam Pfister (sam)

	* version R1-01d
	BUGS: 
	FILES: Map.cc(16394), Map.hh(16394), test_map.cc(16394)
	Updated getStopline function to take only a point in space not a
	stopline label

Thu Mar  1 19:33:50 2007	Sam Pfister (sam)

	* version R1-01c
	BUGS: 
	New files: rndf.txt test_map.cc
	FILES: Makefile.yam(16013), Map.cc(16013), Map.hh(16013),
		MapPrior.cc(16013), MapPrior.hh(16013)
	implemented first cut of getTransitionBounds and added test_map
	program to test map queries and plot the results

	FILES: Map.cc(15922), Map.hh(15922)
	added base query function declarations to Map.hh .

	FILES: Map.cc(15999), Map.hh(15999), MapPrior.cc(15999),
		MapPrior.hh(15999)
	Implemented the getLeftBound and getRightBound functions.  Still
	need intersection transition.

Tue Feb 27  8:39:02 2007	Sam Pfister (sam)

	* version R1-01b
	BUGS: 
	New files: MapPrior.cc MapPrior.hh
	Deleted files: LocalMapTalker.cc LocalMapTalker.hh
		LogicalRoadObject.cc LogicalRoadObject.hh RoadObject.cc
		RoadObject.hh
	FILES: Makefile.yam(15900), Map.cc(15900), Map.hh(15900)
	Moved new map structure from mapper into map module.  Map now
	parses RNDF files with an internal method.

Sun Feb 25 12:56:24 2007	murray (murray)

	* version R1-01a
	BUGS: 
	FILES: LocalMapTalker.cc(15691), LocalMapTalker.hh(15691)
	updated to work with new interfaces release

2007-02-25  murray  <murray@gclab.dgc.caltech.edu>

	* LocalMapTalker.hh: updated SkynetContainer path
	* LocalMapTalker.cc: added interfaces/sn_types.h
	(CSkynetContainer): updated modName to type int

Thu Feb  1 19:26:58 2007	Sam Pfister (sam)

	* version R1-00b
	BUGS: 
	FILES: Makefile.yam(13982)
	commented out LIB_MODULE_LINKS := libmap line in Makefile.yam.	It
	was making a broken link.

	FILES: Map.hh(13981)
	Adding simpler endpoint data structure to map for short term use.  

Sat Jan 27 18:25:27 2007	Nok Wongpiromsarn (nok)

	* version R1-00a
	BUGS: 
	New files: LocalMapTalker.cc LocalMapTalker.hh LogicalRoadObject.cc
		LogicalRoadObject.hh Map.cc Map.hh RoadObject.cc
		RoadObject.hh
	FILES: Makefile.yam(13230)
	Added Map class and LocalMapTalker

Sat Jan 27 18:14:18 2007	Nok Wongpiromsarn (nok)

	* version R1-00
	Created map module.


















